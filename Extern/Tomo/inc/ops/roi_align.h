// Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
#pragma once

#include "../macros.h"
#include <ATen/ATen.h>

namespace tomo {
    VISION_API at::Tensor roi_align_3d(
        const at::Tensor& input,
        const at::Tensor& rois,
        const double spatial_scale,
        const int64_t pooled_depth,
        const int64_t pooled_height,
        const int64_t pooled_width,
        const int64_t sampling_ratio,
        const bool aligned);

namespace detail {
    at::Tensor _roi_align_3d_backward(
        const at::Tensor& grad,
        const at::Tensor& rois,
        const double spatial_scale,
        const int64_t pooled_depth,
        const int64_t pooled_height,
        const int64_t pooled_width,
        const int64_t batch_size,
        const int64_t channels,
        const int64_t depth,
        const int64_t height,
        const int64_t width,
        const int64_t sampling_ratio,
        const bool aligned);

} // namespace detail
} // namespace tomo
