#pragma once

#include <ATen/ATen.h>
#include "../macros.h"

namespace tomo {

VISION_API std::tuple<at::Tensor, at::Tensor> morph_pool(
    const at::Tensor& input,
    const at::Tensor& mask);

namespace detail {

at::Tensor _morph_pool_backward(
    const at::Tensor& grad,
    const at::Tensor& mask,
    const at::Tensor& input_indices,
    const int64_t batch,
    const int64_t channel,
    const int64_t depth,
    const int64_t height,
    const int64_t width);

} // namespace detail
} // namespace tomo
