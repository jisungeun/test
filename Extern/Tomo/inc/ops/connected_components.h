#pragma once

#include <ATen/ATen.h>
#include "../macros.h"


namespace tomo {
    VISION_API at::Tensor cc_3d(const at::Tensor &input);
}
