#pragma once

#include <ATen/ATen.h>
#include "../macros.h"

namespace tomo {

VISION_API at::Tensor nms_3d(
    const at::Tensor& dets,
    const at::Tensor& scores,
    double iou_threshold);

} // namespace vision
