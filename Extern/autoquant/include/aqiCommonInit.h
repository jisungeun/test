// aqiCommonInit.h
// Declares the unlocking function for the common initialization DLL.
// Copyright 2001, AutoQuant Imaging, Inc.

// DLL Unlocking Function
//   Processes the key pair indicated by this function's parameters -
//   if the pair is valid, the DLL will be unlocked
// Parameters:
//   lpstrProgramKey: The program part of the DLL unlocking key; hexadecimal string passed
//                    directly from the calling program
//   lpstrRegKeyLocation: The fully-qualified registry subkey underneath HKEY_CURRENT_USER
//                        where the registry-stored part of the DLL-unlocking key can be found
//   lpstrRegKeyValueName: The name of the registry variable that stores the registry part of
//                         the DLL key
// Returns:
//   Type short
//   AQI_STATUS_DONE if the DLL was successfully unlocked
//   AQI_STATUS_DLLLOCKED if the DLL was not successfully unlocked
extern "C" short _stdcall aqiUnlockCommonInitDll(const char* lpstrProgramKey,
												 const char* lpstrRegKeyLocation,
												 const char* lpstrRegKeyValueName);
