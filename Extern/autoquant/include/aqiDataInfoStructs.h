// aqiDataInfoStructs.h
// Dataset Information Structures - Definitions and Initialization Routines
// Copyright 2001-2003, AutoQuant Imaging, Inc.
// This file may not be modified or redistributed without the permission
// of AutoQuant Imaging, Inc.

// Note: This file contains structure definitions used across all
// AutoQuant deconvolution libraries. Some of these structures may not be
// needed by a given library. Check the programming manual for a
// specific library to determine which structure definitions are relevant to
// that library.

#ifndef AQIDATAINFOSTRUCTS_H
#define AQIDATAINFOSTRUCTS_H

////////////////////////////////////////////////////////////////////
////////////////////// STRUCTURE DEFINITIONS ///////////////////////
////////////////////////////////////////////////////////////////////
typedef float IMGTYPE;	// Image type
typedef unsigned char IMGTYPE_8; //8 bit (char) data type

// Structure with members that define the properties of the
// input image
struct aqiStructImgInfo
{
	char* lpstrImgFile;                     // Image filename (absolute)
	char* lpstrImgInFile;                   // Image input filename (absolute) - DEPRECATED
	char* lpstrRealName;                    // Log file entry filename (absolute)
	bool* lpbBadSlice;                      // Mask list that indicates slices which contain unusable data
	IMGTYPE* lpfImgData;                    // Pointer to 32-bit FP image data (used in place of filename)
	enum enumDataTypes enDataType;          // Input image data type
	enum enumModalities enScopeModality;    // Modality of scope used to collect image
	short shWidth;                          // Image width
	short shHeight;                         // Image height
	short shNumSlices;                      // Image depth (number of optical slices)
	short shTotalChannels;                  // Total channels in the dataset
	float fPixelSizeX;                      // Scale factor between image width and sample width
	float fPixelSizeY;                      // Scale factor between image height and sample height
	float fPixelSizeZ;                      // Scale factor between image depth and sample depth
	float fNumericAperture;                 // Lens numerical aperture
	float fRefractiveIndex;                 // Refractive index of lens immersion medium
	float fEmmWavelength;                   // Emmisive wavelength of dye used on sample
	float fSphereAberrationFactor;          // Spherical aberration correction factor
	float fConfocalPinholeSize;				// Size of Confocal Pinhole in Airy disk units
	char* lpstrImgFirstGuess;				// Filename for image first guess
	float fEmbeddingRefractiveIndex;		// Embedding medium RI
	float fSampleDepth;						// Sample depth from coverslip (um)
	float fSphereAberrationFactorO2;        // Second-order spherical aberration correction factor
	long dwReserved2;                       // Reserved for future use
	long dwReserved3;                       // Reserved for future use
	long dwReserved4;                       // Reserved for future use
};

// Structure with members that define requested manipulations to be made to the
// image data contained in an associated aqiStructImgInfo when displaying
// that information
struct aqiStructImgDispParams
{
	float* lpfRotationMatrix;               // Pointer to a 3x3 rotation matrix
	enum enumProjectionTypes enProjType;    // Desired projection
	float fMinThreshold;                    // Intensity percentage below which data will not be displayed
	float fMaxThreshold;                    // Intensity percentage above which data will not be displayed
	float fGamma;                           // Gamma correction factor
	float fBrightness;                      // Intensity percentage above which data will be displayed at max value
	float fDarkness;                        // Intensity percentage below which data will be displayed at min value
	float fAlpha;                           // Alpha weight (only applicable to alpha blend projections)

	int iUseTextureCaching;              // Cache 2D textures (time series only)
	int iCurrentTimePoint;                  // Informs texture cacher of current time point
	int iCurrentChannel;                    // Informs texture cacher of current channel

	long dwReserved1;                       // Reserved for future use
	long dwReserved2;                       // Reserved for future use
	long dwReserved3;                       // Reserved for future use
	long dwReserved4;                       // Reserved for future use
	long dwReserved5;                       // Reserved for future use
};

// Structure with members that define the properties of the
// point-spread function (PSF)
struct aqiStructPsfInfo
{
	char* lpstrPsfFile;                     // PSF filename (absolute)
	char* lpstrPsfOutFile;                  // Filename to which PSF will be saved (absolute) - DEPRECATED
	float* lpfPsfEstimate;                  // Pointer to 32-bit FP PSF estimate (used in place of filename)
	short shWidth;                          // PSF width
	short shHeight;                         // PSF height
	short shNumSlices;                      // PSF depth (number of optical slices)
	float fPixelSizeX;                      // Scale factor between PSF width and sample width
	float fPixelSizeY;                      // Scale factor between PSF height and sample height
	float fPixelSizeZ;                      // Scale factor between PSF depth and sample depth
	enum enumPsfSources enPsfSource;        // Manner in which PSF was generated
	long dwReserved1;                       // Reserved for future use
	long dwReserved2;                       // Reserved for future use
	long dwReserved3;                       // Reserved for future use
	long dwReserved4;                       // Reserved for future use
	long dwReserved5;                       // Reserved for future use
	long dwReserved6;                       // Reserved for future use
	long dwReserved7;                       // Reserved for future use
	long dwReserved8;                       // Reserved for future use
};


////////////////////////////////////////////////////////////////////
////////////////////// FUNCTION DECLARATIONS ///////////////////////
////////////////////////////////////////////////////////////////////

// aqiInitStructImgInfo(...)
//   Image Parameter Structure Initialization Routine
//   Initializes an input image information structure to its default values
//   (specified in the programming manual)
// Parameter:
//   pstructImgInfo: Structure that defines the properties of the image to be processed
// Returns:
//   Type short
//   AQI_STATUS_NOERROR if the operation completes successfully
//   AQI_STATUS_ERROR otherwise
extern "C" short _stdcall aqiInitStructImgInfo(struct aqiStructImgInfo* pstructImgInfo);

// aqiInitStructImgDispParams
//   Image Display Structure Initialization Routine
//   Assigns each member of the image manipulation parameter structure its default
//   value (as listed in the programming manual)
// Parameter:
//   pstructImgDispParams: Structure that defines the manipulations requested on
//                         an accompanying image
// Returns:
//   Type short
//   AQI_STATUS_NOERROR if the operation completes succesfully
//   AQI_STATUS_ERROR otherwise
extern "C" short _stdcall aqiInitStructImgDispParams(struct aqiStructImgDispParams* pstructImgDispParams);

// aqiInitStructPsfInfo(...)
//   PSF Property Structure Initialization Routine
//   Initializes a PSF information structure to its default values
//   (specified in the programming manual)
// Parameter:
//   pstructPsfInfo: Structure that defines the properties of the PSF to be processed
// Returns:
//   Type short
//   AQI_STATUS_NOERROR if the operation completes successfully
//   AQI_STATUS_ERROR otherwise
extern "C" short _stdcall aqiInitStructPsfInfo(struct aqiStructPsfInfo* pstructPsfInfo);

#endif // #ifndef AQIDATAINFOSTRUCTS_H