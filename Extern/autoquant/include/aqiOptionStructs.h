// aqiOptionStructs.h
// Deconvolution Option Structures - Definitions and Initialization Routines
// Copyright 2006-2012, Media Cybernetics, Inc.
// This file may not be modified or redistributed without the permission
// of Media Cybernetics, Inc.

// Note: This file contains structure definitions used across all
// AutoQuant deconvolution libraries. Some of these structures may not be
// needed by a given library. Check the programming manual for a
// specific library to determine which structure definitions are relevant to
// that library.

#ifndef __AQI_OPTIONSTRUCTS_H__
#define __AQI_OPTIONSTRUCTS_H__

////////////////////////////////////////////////////////////////////
////////////////////// STRUCTURE DEFINITIONS ///////////////////////
////////////////////////////////////////////////////////////////////

// Structure with members that specify standard control
// parameters for 3D Deconvolution
struct aqiStructDeconOpsStd
{
	char* lpstrImgOutFile;								   // Filename to which output image will be saved (absolute)
	char* lpstrTempDir;									   // Directory in which to store temporary files
	enum enumDeconMeths enDeconMeth;					   // Deconvolution method to use
	enum enumDarkCurMeths enDarkCurMeth;				   // Darkcurrent calculation method to use
	float fDarkCurrent;									   // Darkcurrent value (if manual calculation method is used)
	short shNumIterations;								   // Number of iterations through which to process the image
	short shSaveInterval;								   // Iteration interval at which to save processing "so far"
	short shBinFactorXY;								   // Binning factor to apply to the XY plane
	short shBinFactorZ;									   // Binning factor to apply to the Z (optical slice) dimension
	bool bEnableGpuProcessing;							   // Determines whether to permit GPU processing for the dataset
	long dwReserved1;									   // Reserved for future use
	long dwReserved2;									   // Reserved for future use
	long dwReserved3;									   // Reserved for future use
	long dwReserved4;									   // Reserved for future use
	long dwReserved5;									   // Reserved for future use
	long dwReserved6;									   // Reserved for future use
	long dwReserved7;									   // Reserved for future use
};

// Structure with members that specify expert control
// parameters for 3D Deconvolution
struct aqiStructDeconOpsExp
{
	enum enumImgGuessMeths enImgGuessMeth;				   // Image initial guess calculation method
	enum enumPsfGuessMeths enPsfGuessMeth;				   // PSF initial guess calculation method
	enum enumFreqConsMeths enFreqConsMeth;				   // Frequency bandlimit calculation method
	enum enumSubVolMeths enSubVolMeth;					   // Subvolume size calculation method
	short shGuardBand;									   // Padding size to add to XY image border (in pixels)
	short shGuardBandZ;									   // Padding size to add to Z image border (in pixels)
	short shSubVolOverlap;								   // Number of common pixels between adjacent subvolumes
	bool bMontageXY;									   // Toggles subvolume decomposition in XY plane
	bool bMontageZ;										   // Toggles subvolume decomposition in Z dimension
	bool bDetectBlanks;									   // Toggles blank region detection and skipping
	bool bSuppressNoise;								   // Toggles noise compensation
	bool bDenseSample;									   // Toggles dense sample processing
	bool bEnablePsfCons;								   // Toggles use of theoretical constraints on PSF
	bool bEnableGuidedDecon;							   // Toggles guided deconvolution
    bool bSuppressMessages;								   // Toggles interactive message boxes
    float fSuppressNoiseFactor;							   // Value for noise suppression factor (2=low, 20=medium, 100=high)
    float fPsfStretchFactor;							   // Stretch factor to apply to calculated PSF
    float fPsfCentralRadius;							   // Radius (in pixels) of initial PSF hourglass "waist"
	short shGoldsSmoothIteration;						   // Iteration interval for applying smoothing with Golds Method
	float fGoldsSmoothGauss;							   // FWHM of Gaussian applied with Golds Method
	short shSubpixelXYFactor;							   // Subpixel factor in XY
	short shSubpixelZFactor;							   // Subpixel factor in Z  (reserved for future use)
	short shIntensityCorrection;						   // Flicker/attenuation correction
	float fMaxMemoryUsage;								   // Maximum permitted memory usage as a percentage of available memory
	bool bEnableClassicConfocalAlgorithm;				   // Determines whether to use "classic" confocal algorithm
	bool bReserved1;									   // Reserved for future use
	short shReserved2;									   // Reserved for future use
	enum enumTheoreticalPsfGenMeths enPsfGenMeth;          // Algorithm used for generation of the theoretical PSF
	long dwReserved3;									   // Reserved for future use
	long dwReserved4;									   // Reserved for future use
};


// Structure with members that contain validation flags for 3D Deconvolution
struct aqiStructDecon3dValidationFlags
{
	long lImgInfoValidationFlags;                          // Validation flags for aqiStructImgInfo (AQ_IMGINFO values)
	long lPsfInfoInputValidationFlags;                     // Validation flags for input aqiStructPsfInfo (AQ_PSFINFO values)
	long lPsfInfoOutputValidationFlags;                    // Validation flags for output aqiStructPsfInfo (AQ_PSFINFO values)
	long lOpsStdValidationFlags;                           // Validation flags for aqiStructDeconOpsStd (AQ_STDOPS values)
	long lOpsExpValidationFlags;						   // Validation flags for aqiStructDeconOpsExp (AQ_EXPOPS values)
	long lDeconMixedValidationFlags;                       // Additional validation flags for interactions between structures
};

////////////////////////////////////////////////////////////////////
////////////////////// FUNCTION DECLARATIONS ///////////////////////
////////////////////////////////////////////////////////////////////

// aqiInitStructDeconOpsStd(...)
//   Standard Control Parameter Structure Initialization Routine
//   Initializes a standard deconvolution parameter structure to its default values
//   (specified in the programming manual)
// Parameter:
//   pstructDeconOpsStd: Structure that specifies standard deconvolution control parameters
// Returns:
//   Type short
//   AQI_STATUS_NOERROR if the operation completes successfully
//   AQI_STATUS_ERROR otherwise
extern "C" short _stdcall aqiInitStructDeconOpsStd(struct aqiStructDeconOpsStd* pstructDeconOpsStd);


// aqiInitStructDeconOpsExp(...)
//   Expert Control Parameter Structure Initialization Routine
//   Initializes an expert deconvolution parameter structure to its default values
//   (specified in the programming manual)
// Parameter:
//   pstructDeconOpsExp: Structure that specifies expert deconvolution control parameters
// Returns:
//   Type short
//   AQI_STATUS_NOERROR if the operation completes successfully
//   AQI_STATUS_ERROR otherwise
extern "C" short _stdcall aqiInitStructDeconOpsExp(struct aqiStructDeconOpsExp* pstructDeconOpsExp);

#endif // #ifndef __AQI_OPTIONSTRUCTS_H__