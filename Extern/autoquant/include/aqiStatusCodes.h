#ifndef STATUSCODES__H
#define STATUSCODES__H

	////////////////////////////////////////////////////////////////////
	//////////// AUTOQUANT ERROR/STATUS CODE DEFINITIONS ///////////////
	////////////////////////////////////////////////////////////////////

	//////////////////// RETURN CODE DEFINITIONS ///////////////////////
	const short AQI_STATUS_DONE                            = 0;     // Success (no error)
	const short AQI_STATUS_NOERROR                         = 0;     // Success (no error) 
	const short AQI_STATUS_ERROR                           = 1;     // General error
	const short AQI_STATUS_INVALIDINPUT                    = 2;     // Invalid input parameter 
	const short AQI_STATUS_INVALIDIMAGE                    = 3;     // Invalid input image 
	const short AQI_STATUS_OUTOFMEMORY                     = 4;     // Out of memory
	const short AQI_STATUS_OUTOFDISK                       = 5;     // Out of disk space
	const short AQI_STATUS_FILEOPENERROR                   = 6;     // Error during file open
	const short AQI_STATUS_FILEREADERROR                   = 7;     // Error during file reading
	const short AQI_STATUS_FILEWRITEERROR                  = 8;     // Error during file writing
	const short AQI_STATUS_DATAFORMATERROR                 = 9;     // Data format cannot be recognized
	const short AQI_STATUS_FFTERROR                        = 10;    // FFT error
	const short AQI_STATUS_TYPEMISMATCH                    = 11;    // The input perspective (XY, XZ, or ZY) is not supported
	const short AQI_STATUS_BLANKCHANNEL                    = 12;    // Input channel contains constant-value data (cannot be deblurred)
	const short AQI_STATUS_LARGE_GUARDBAND                 = 13;    // The input guardband is too large
	const short AQI_STATUS_LARGE_OVERLAP                   = 14;    // The overlap between subvolumes is too large
	const short AQI_STATUS_EW_OUTSIDERANGE                 = 15;    // The emision wavelength (EW) is outside the acceptable range (10-10000nm)
	const short AQI_STATUS_NA_RI_OUTSIDERANGE              = 16;    // Either the NA or the RI are outside the acceptable range
	const short AQI_STATUS_XY_UNDERSAMPLING                = 17;    // Undersampling in the x and/or y dimensions
	const short AQI_STATUS_XY_OVERSAMPLING                 = 18;    // Oversampling in the x and/or y dimensions
	const short AQI_STATUS_Z_UNDERSAMPLING                 = 19;    // Undersampling in the z dimension (very large z-step size)
	const short AQI_STATUS_Z_OVERSAMPLING                  = 20;    // Oversampling in the z dimension (too small z-step size)
	const short AQI_STATUS_DLLLOCKED                       = 99;    // DLL is locked


	//////////////// PROCESSING  STATUS  CODE DEFINITIONS ////////////////
	const short AQI_STATUS_BEGIN                           = 100;   // Beginning algorithm

	const short AQI_STATUS_START_PREPROCESS                = 101;   // Initializing pre-processsing phase
	const short AQI_STATUS_PREPROCESSING                   = 102;   // Performing pre-processing phase
	const short AQI_STATUS_START_ROTATE                    = 103;   // Initializing data rotation operation
	const short AQI_STATUS_ROTATING                        = 104;   // Rotating data

	const short AQI_STATUS_START_PROCESS                   = 105;   // Initializing main processing phase (occurs for each
	                                                                // subvolume  if data has been decomposed to subvolumes)
	const short AQI_STATUS_PROCESSING                      = 106;   // Processing (message does not need to be updated)
	const short AQI_STATUS_SUBVOLUME                       = 107;   // Processing subvolume
	const short AQI_STATUS_BLENDSUBVOLUME                  = 108;   // Blending sub-volume

	const short AQI_STATUS_START_POSTPROCESS               = 109;   // Initializing post-processing phase
	const short AQI_STATUS_POSTPROCESSING                  = 110;   // Performing post-processing phase
	const short AQI_STATUS_START_FINALPROCESS              = 111;   // Initializing completion phase
	const short AQI_STATUS_FINALPROCESSING                 = 112;   // Cleaning up and finishing processing

	const short AQI_STATUS_ALLOCATEMEMORY                  = 113;   // Allocating memory
	const short AQI_STATUS_FFT                             = 114;   // Performing FFT
	const short AQI_STATUS_INVERTDOC                       = 115;   // Inverting data set intensity
	const short AQI_STATUS_DATACORRECTION                  = 116;   // Performing data correction
	const short AQI_STATUS_GENPSF                          = 117;   // Generating PSF   

	const short AQI_STATUS_STARTITERATION                  = 120;   // Starting iteration
	const short AQI_STATUS_EMSTEP                          = 121;   // Common part of MLE iteration
	const short AQI_STATUS_OBJESTIMATE                     = 122;   // Calculating object estimate
	const short AQI_STATUS_PSFESTIMATE                     = 123;   // Calculating PSF estimate
	const short AQI_STATUS_POWERACCEL                      = 124;   // Performing power acceleration
	const short AQI_STATUS_STEPACCEL                       = 125;   // Performing step acceleration
	const short AQI_STATUS_NOISESUPPRESSION                = 126;   // Applying noise suppression
	const short AQI_STATUS_PSFCONSTRAINTS                  = 127;   // Applying PSF constraints
	const short AQI_STATUS_WRITETEMPRESULTS                = 128;   // Writing intermediate object and psf results into files
	const short AQI_STATUS_DETECT_SA                       = 129;   // Detecting Magnitude of Spherical Aberrations
                                                                    // Here, the time left will actually be the SA, and the percentage is the iteration number
	const short AQI_STATUS_SLICES_SEGMENT                  = 130;   // Slice by slice segmentation
	const short AQI_STATUS_GLOBAL_SEGMENT                  = 131;   // Global volume segmentation

	const short AQI_STATUS_TRACKING                        = 132;   // Tracking algorithm
	const short AQI_STATUS_FEATURE_EXTRACTION              = 133;   // Feature extraction
	const short AQI_STATUS_OBJECT_RECOGNITION              = 134;   // Object Recognition
	const short AQI_STATUS_SEGMENTATION                    = 135;   // Segmentation (Watershed)
	const short AQI_STATUS_UPDATE_3DDECON_PREVIEW          = 136;   // Update preview image

	const short AQI_STATUS_BLOB_SEPARATION                 = 137;   // Blob separation
	const short AQI_STATUS_GENERATE_TRACKING_VIEWS         = 138;   // Generation of different views of tracked objects
	const short AQI_STATUS_GENERATE_MERGED_VIEWS           = 139;   // Generation of merged views

	const short AQI_STATUS_NOISE_REDUCTION                 = 140;   // Noise reduction in Segmentation
	const short AQI_STATUS_BACKGROUND_EQUALIZATION         = 141;   // Background equalization
	const short AQI_STATUS_THRESHOLDING                    = 142;   // Thresholding
	const short AQI_STATUS_CLEANING                        = 143;   // Cleaning after thresholding
	const short AQI_STATUS_BRIGHT_FIELD_INVERSION          = 144;   // Bright field inversion
	const short AQI_STATUS_AUTOMATIC_THRESHOLD             = 145;   // Automatic threshold calculation
	const short AQI_STATUS_CONNECTED_COMP                  = 146;
	const short AQI_STATUS_SUPPRESS_NOISE                  = 147;
	const short AQI_STATUS_SAT_LOAD                        = 148;
	const short AQI_STATUS_SAT_SAVE                        = 149;
	const short AQI_STATUS_2D_METRICS_CALCULATION          = 150;
	const short AQI_STATUS_3D_METRICS_CALCULATION          = 151;
	const short AQI_STATUS_CONVEX_HULL_CALCULATION         = 152;
	const short AQI_STATUS_WAVELET_NOISE_REDUCTION         = 153;
	const short AQI_STATUS_BACKGRND_EQU_REASONABLE         = 154;
	const short AQI_UPDATE_ITERATION_ONLY                  = 155;

	///////////////// CALLBACK = RETURN  CODE DEFINITIONS /////////////////      
	const short AQI_STATUS_CANCEL                          = 200;   // Abort or cancel processing (returned by call-back function)
	const short AQI_STATUS_CONTINUE                        = 201;   // Continue processing (returned by call-back function)  
	const short AQI_STATUS_PAUSE                           = 202;   // Pause processing (returned by call-back function)  
	const short AQI_STATUS_RESUME                          = 203;   // Resume a paused process (returned by call-back function)

#endif