#ifndef AQIDATAINFOVALIDATIONFLAGS__H
#define AQIDATAINFOVALIDATIONFLAGS__H


//////////////////// IMAGE INFORMATION VALIDATION FLAGS ///////////////////////

const long AQ_IMGINFO_NOERROR                      = 0x00000000;     // No invalid fields
const long AQ_IMGINFO_ERR_FILENAME                 = 0x00000001;     // The image filename is invalid
const long AQ_IMGINFO_ERR_IMGMEMORY                = 0x00000002;     // The image memory buffer is invalid
const long AQ_IMGINFO_ERR_REALNAME                 = 0x00000004;     // The lpstrRealName description field has not been specified
const long AQ_IMGINFO_ERR_DATATYPE	               = 0x00000008;     // DataType value is invalid
const long AQ_IMGINFO_ERR_MODALITY                 = 0x00000010;     // ScopeModality value is invalid
const long AQ_IMGINFO_ERR_TOTALCHANNELS            = 0x00000020;     // TotalChannels value is out of range
const long AQ_IMGINFO_ERR_WIDTH                    = 0x00000040;     // Width value is out of range
const long AQ_IMGINFO_ERR_HEIGHT                   = 0x00000080;     // Height value is out of range
const long AQ_IMGINFO_ERR_NUMSLICES                = 0x00000100;     // NumSlices value is out of range
const long AQ_IMGINFO_ERR_PIXELSIZE_X              = 0x00000200;     // PixelSizeX value is out of range
const long AQ_IMGINFO_ERR_PIXELSIZE_Y              = 0x00000400;     // PixelSizeY value is out of range
const long AQ_IMGINFO_ERR_PIXELSIZE_Z              = 0x00000800;     // PixelSizeZ value is out of range
const long AQ_IMGINFO_ERR_LENS_RI                  = 0x00001000;     // RefractiveIndex value is invalid
const long AQ_IMGINFO_ERR_LENS_NA                  = 0x00002000;     // NumericAperture value is invalid
const long AQ_IMGINFO_ERR_EMW                      = 0x00004000;     // EmmWavelength value is invalid

///////////////////// PSF INFORMATION VALIDATION FLAGS ////////////////////////

const long AQ_PSFINFO_NOERROR                      = 0x00000000;     // No invalid fields
const long AQ_PSFINFO_ERR_FILENAME                 = 0x00000001;     // The filename specified for the PSF data was invalid
const long AQ_PSFINFO_ERR_PSFMEMORY                = 0x00000002;     // The image memory buffer is invalid
const long AQ_PSFINFO_ERR_PSFSOURCE                = 0x00000004;     // PsfSource value is invalid
const long AQ_PSFINFO_ERR_WIDTH                    = 0x00000008;     // Width value is out of range
const long AQ_PSFINFO_ERR_HEIGHT                   = 0x00000010;     // Height value is out of range
const long AQ_PSFINFO_ERR_NUMSLICES                = 0x00000020;     // NumSlices value is out of range
const long AQ_PSFINFO_ERR_PIXELSIZE_X              = 0x00000040;     // PixelSizeX value is out of range
const long AQ_PSFINFO_ERR_PIXELSIZE_Y              = 0x00000080;     // PixelSizeY value is out of range
const long AQ_PSFINFO_ERR_PIXELSIZE_Z              = 0x00000100;     // PixelSizeZ value is out of range


#endif // AQIDATAINFOVALIDATIONFLAGS__H