#ifndef AQIDECONOPSVALIDATIONFLAGS__H
#define AQIDECONOPSVALIDATIONFLAGS__H


///////////////////// STANDARD OPTIONS VALIDATION FLAGS ///////////////////////

const long AQ_STDOPS_NOERROR                      = 0x00000000;     // No invalid fields
const long AQ_STDOPS_ERR_FILENAME                 = 0x00000001;     // The image output base filename is invalid (one or more output files cannot be written to)
const long AQ_STDOPS_ERR_TEMPDIR                  = 0x00000002;     // The temporary folder value is invalid (fails write test)
const long AQ_STDOPS_ERR_DECONMETH                = 0x00000004;     // Deconvolution method value is invalid
const long AQ_STDOPS_ERR_DARKCURRENTMETH          = 0x00000008;     // Darkcurrent detection method value is invalid
const long AQ_STDOPS_ERR_DARKCURRENT	          = 0x00000010;     // Darkcurrent value is invalid
const long AQ_STDOPS_ERR_NUMITERATIONS            = 0x00000020;     // NumIterations value is invalid
const long AQ_STDOPS_ERR_SAVEINTERVAL             = 0x00000040;     // SaveInterval value is out of range
const long AQ_STDOPS_ERR_BINFACTOR_XY             = 0x00000080;     // BinFactorXY value is invalid
const long AQ_STDOPS_ERR_BINFACTOR_Z              = 0x00000100;     // BinFactorZ value is invalid


////////////////////// EXPERT OPTIONS VALIDATION FLAGS ////////////////////////

const long AQ_EXPOPS_NOERROR                      = 0x00000000;     // No invalid fields
const long AQ_EXPOPS_ERR_IMGGUESSMETH             = 0x00000001;     // Image first-guess method is invalid
const long AQ_EXPOPS_ERR_PSFGUESSMETH             = 0x00000002;     // PSF first-guess method is invalid
const long AQ_EXPOPS_ERR_FREQCONSMETH             = 0x00000004;     // Frequency bandlimit calculation method is invalid
const long AQ_EXPOPS_ERR_SUBVOLMETH               = 0x00000008;     // Subvolume size calculation method is invalid
const long AQ_EXPOPS_ERR_GUARDBANDXY              = 0x00000010;     // GuardBand value is out of range
const long AQ_EXPOPS_ERR_GUARDBANDZ               = 0x00000020;     // GuardBandZ value is out of range
const long AQ_EXPOPS_ERR_SUBVOLOVERLAP            = 0x00000040;     // SubVolOverlap value is out of range
const long AQ_EXPOPS_ERR_NOISEFACTOR              = 0x00000080;     // SuppressNoiseFactor value is out of range
const long AQ_EXPOPS_ERR_PSFSTRETCHFACTOR         = 0x00000100;     // PsfStretchFactor value is out of range
const long AQ_EXPOPS_ERR_PSFCENTRALRADIUS         = 0x00000400;     // PsfCentralRadius value is out of range
const long AQ_EXPOPS_ERR_GOLDSSMOOTH_ITERATION    = 0x00000800;     // GoldsSmoothIteration value is out of range
const long AQ_EXPOPS_ERR_GOLDSSMOOTH_FWHM         = 0x00001000;     // GoldsSmoothGauss value is out of range
const long AQ_EXPOPS_ERR_SUBPIXELFACTOR_XY        = 0x00002000;     // SubpixelXYFactor value is out of range
const long AQ_EXPOPS_ERR_SUBPIXELFACTOR_Z         = 0x00004000;     // SubpixelZFactor value is out of range
const long AQ_EXPOPS_ERR_MAX_MEMORY               = 0x00008000;     // MaxMemoryUsage value is out of range
const long AQ_EXPOPS_ERR_PSFGENMETH               = 0x00010000;     // Theoretical PSF generation method is invalid


#endif // AQIDECONOPSVALIDATIONFLAGS__H