// aqidllcst.h
// Definition File for Media Cybernetics' AutoQuant DLL Status Codes and Callback Routines
// Copyright 2000-2012, Media Cybernetics, Inc.

#ifndef __AQI_DLL_CONSTANT__
#define __AQI_DLL_CONSTANT__

#include "aqiDataInfoStructs.h"

#include "aqiStatusCodes.h"

////////////////////////////////////////////////////////////////////
////////////////// CALLBACK FUNCTION SUPPORT ///////////////////////
////////////////////////////////////////////////////////////////////

//////////////// PROGRESS STRUCTURE DEFINITIONS ////////////////////
struct aqiStructProgressInfo
{
	float fPercentComplete;                 // Percent completion of current part of operation
	float fMinutesRemaining;                // Time remaining until completion of full operation
	short shCurIteration;                   // Current computational iteration being executed
	short shTotalIterations;                // Total number of iterations that will be executed
	short shCurSubVolume;                   // Current data subvolume being processed
	short shTotalSubVolumes;                // Total number of subvolumes in the overall volume
	short shStatusCode;                     // One of the status codes listed above
	bool bReinitialize;                     // Flag for reinitializing cancellation status
};

struct aqiStructProgressImage
{
	float* lpfObjectEstimate;               // Pointer to intermediate image data
	float* lpfPsfEstimate;                  // Pointer to intermediate PSF data
	short shImageWidth;                     // Image width
	short shImageHeight;                    // Image height
	short shImageDepth;                     // Image depth
	float fPixelSum;                        // sum of all pixel intensities (the image is 
	                                        // internally normalized to 1.0)
};

//////////////// CALLBACK FUNCTION DECLARATIONS ////////////////////

// Status Callback Specification
//   The callback function is written by the user of the DLL, and placed in
//   the calling program. DLL functions that supply a parameter for a callback
//   pointer may then take a pointer to that function.
// Function Specification:
//   short _stdcall <MyCallback>(aqiStructProgressInfo* pstructProgressInfo,
//                               void* pCallerInfo);
// Parameters:
//   pstructProgressInfo: Structure that describes the progress of a lengthy operation
//   pCallerInfo: Pointer to a block of memory that was passed to the DLL, and is here
//                passed to the callback function (this memory may contain any information
//                about the calling program that the user of this function wants)
// Returns:
//   Type short
//   AQI_STATUS_CONTINUE if the DLL function should keep processing
//   AQI_STATUS_CANCEL if the DLL function should terminate without finishing
typedef short (_stdcall* aqiCbfnDllStatus)(aqiStructProgressInfo* pstructProgressInfo,
										   void* pCallerInfo);


// Image Update Callback Specification
//   The callback function is written by the user of the DLL, and placed in
//   the calling program. DLL functions that supply a parameter for a callback
//   pointer may then take a pointer to that function. This callback will receive
//   intermediate image data results (to show a work-in-progress, if desired)
// Function Specification:
//   short _stdcall <MyCallback>(aqiStructProgressImage* pstructProgressImage,
//                               void* pCallerInfo);
// Parameters:
//   pstructProgressImage: Structure that contains the images produced during the operation
//   pCallerInfo: Pointer to a block of memory that was passed to the DLL, and is here
//                passed to the callback function (this memory may contain any information
//                about the calling program that the user of this function wants)
// Returns:
//    undefined at this time
typedef short (_stdcall* aqiCbfnDllImage)(aqiStructProgressImage* pstructProgressImage,
										  void* pCallerInfo);


// Data Storage Callback Specification
//   The callback function is written by the user of the DLL, and placed in
//   the calling program. DLL functions that supply a parameter for a callback
//   pointer may then take a pointer to that function. This callback will receive
//   image data and a set of manipulations requested to be made to that data, for storage within
//   the calling application, rather than (or in addition to) storage within the DLL
// Function Specification:
//   short _stdcall <MyCallback>(aqiStructImgInfo* pstructImgInfo,
//                               aqiStructImgDispParams* pstructImgDispParams,
//                               short shNumChannels,
//                               void* pCallerInfo);
// Parameters:
//   pstructImgInfo: Array of structures that contain information about the image data to be stored;
//                   each element contains one channel's worth of data
//   pstructImgDispParms: Array of stuctures that contains a set of manipulations that the user has requested
//                        be done to the data prior to storage (the calling application must perform
//                        these manipulations; they have not been performed by the DLL);
//                        each element contains manipulations for one channel, and cooresponds to its counterpart
//                        element in pstructImgInfo
//   shNumChannels: Total number of channels (i.e., number of elements in each of the structure arrays passed)
//   pCallerInfo: Pointer to a block of memory that was passed to the DLL, and is here
//                passed to the callback function (this memory may contain any information
//                about the calling program that the user of this function wants)
// Returns:
//    undefined at this time
typedef short (_stdcall* aqiCbfnDllStoreData)(aqiStructImgInfo* pstructImgInfo,
                                              aqiStructImgDispParams* pstructImgDispParams,
                                              short shNumChannels,
                                              void* pCallerInfo);


// Additional Processing Request Callback Specification
// USED BY 2D DECONVOLUTION MODULE ONLY
//   The callback function is written by the user of the DLL, and placed in
//   the calling program. DLL functions that supply a parameter for a callback
//   pointer may then take a pointer to that function. This callback will allow
//   a calling program to specify additional iterations to be completed before
//   terminating processing.
// Functions Specification:
//	 short _stdcall<Mycallback>(const int TotalIterations, void* pCallerInfo);
//
// Parameters:
//	 TotalIterations: The total number of iterations currently done by the deconvolution
//					  program
//   pCallerInfo: Pointer to a block of memory that was passed to the DLL, and is here
//                passed to the callback function (this memory may contain any information
//                about the calling program that the user of this function wants)
// Returns: The number of further iterations for the deconvolution program to do
typedef short (_stdcall* aqiCbfnMoreIter)(const int TotalIterations, void* pCallerInfo);


// Compatibility Specification
// Specification of the callback function for the following DLL packages:
// ------------------
// Nearest Neighbors
// ------------------
//
// The callback function is written by the author of the calling program and not
// supplied with the AutoQuant library.  It must satisfy the following specifications.
//
// Your call-back function calling structure must be defined according to the following example.
// We recommend that you this example as a template to write your own callback function.
// 
//  short _stdcall MyCallBack(void* hTask,short nStatus,float fPercentage,short total_sub, short current_sub, float time_left)
//  {
//      ... program that manages a message box with a "cancel" button and progress bar. 
//      return AQI_STATUS_CONTINUE;
//  }
//
// The parameters indicated in the 1st line of this example program are defined as follows.
//
//  hTask			The identifier of the task specific to this callback instance, which
//                  may be used for multitasking or multithreading.
//                  Passed both ways.
//
//  nStatus         Status indicator as specified above under "Definitions of status codes..."
//                  Passed both ways.
//                  For example, if the code AQI_STATUS_DEBLURRING is passed from 
//                  NearNeighbors... to the call-back function, it indicates to the callback
//                  function that the the "delburring" is in progress and the 
//                  hypothetical callback function
//                  will hypothetically display a message "deblurring in progress" on the 
//                  message box to the user.  For example if the code AQI_DLLSTATUS_CANCEL
//                  is passed from the hypothetical callback function to NearNeighbors...,
//                  this will inform NearNeighbors that the user wants to "cancel" the
//                  operation and then the NearNeighbors function will do so and exit.
//
//  fPercentage     Progress indicator, which informs the callback function the percentage
//                  of the program that has executed.  This will be number between 0 and 100.
//                  Passed from the NearNeighbors... function to the callback function.
typedef short (_stdcall* AQIDLLStatus)(void* hTask, short nStatus, float fPercentage);

// Compatibility Specification
// Specification of the callback function for the following DLL packages:
// ------------------
// Inverse Filter
// ------------------
//
// Your call-back function calling structure must be defined according to the following example.
// We recommend that you this example as a template to write your own callback function.
//
//  short _stdcall MyCallBack(void* hTask,short nStatus,float fPercentage,short total_sub, short current_sub, float time_left)
//  {
//     ... program that manages a message box with a "cancel" button and progress bar. 
//     return AQI_DLLSTATUS_CONTINUE;
//  }
//
// The parameters indicated in the 1st line of this example program are defined as follows.
//
//  hTask           The identifier of the task specific to this callback instance, which
//					may be used for multitasking or multithreading.
//					Passed both ways.
//
//  nStatus         Status indicator as specified above under "Definitions of status codes..."
//                  Passed both ways.
//                  For example, if the code AQI_DLLSTATUS_DEBLURRING is passed from 
//                  NearNeighbors... to the call-back function, it indicates to the callback
//                  function that the the "delburring" is in progress and the 
//                  hypothetical callback function
//                  will hypothetically display a message "deblurring in progress" on the 
//                  message box to the user.  For example if the code AQI_DLLSTATUS_CANCEL
//                  is passed from the hypothetical callback function to NearNeighbors...,
//                  this will inform NearNeighbors that the user wants to "cancel" the
//                  operation and then the NearNeighbors function will do so and exit.
//
//  fPercentage     Progress indicator, which informs the callback function the percentage
//                  of the program that has executed.  This will be number between 0 and 100.
//                  Passed from the NearNeighbors... function to the callback function.
//
//  total_sub       The total number of sub-volumes
//  current_sub     The sub-volume that is being deblurred currently
//  time_left       The time left to finish deblurring process in terms of minutes
typedef short (_stdcall* AQIDLLStatusSub06)(void* hTask, short nStatus, float fPercentage, short total_sub, short current_sub, float time_left);

#endif // #ifndef __AQI_DLL_CONSTANT__
