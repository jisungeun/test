// aqiEnumerations.h
// Enumerated Type Definitions Used by AutoQuant Software
// Copyright 2000-2003, AutoQuant Imaging, Inc.
// This file may not be modified or redistributed without the permission
// of AutoQuant Imaging, Inc.

// Note: This file contains enumerations used across all AutoQuant
// libraries. Some of these enumerations may not be
// needed by a given library. Check the programming manual for a
// specific library to determine which enumerations are relevant to
// that library.

#ifndef __AQI_ENUMERATIONS_H__
#define __AQI_ENUMERATIONS_H__

////////////////////////////////////////////////////////////////////
/////////////////// ENUMERATED TYPE DEFINITIONS ////////////////////
////////////////////////////////////////////////////////////////////

// Enumeration of recognized data types
enum enumDataTypes
{
	DT_UINT_8BIT = 0,              // 8 bits-per-pixel unsigned integer data
	DT_UINT_16BIT,                 // 16 bits-per-pixel unsigned integer data
	DT_FLOAT_32BIT                 // 32 bits-per-pixel floating-point data
};

// Enumeration of supported microscope modalities
enum enumModalities
{
	MOD_INVALID_VALUE = 0,         // Indeterminate modality - not a valid value
	MOD_CONFOCAL,                  // Confocal modality
	MOD_SPIN_DISK_CF,              // Spinning-disk confocal modality
	MOD_TWO_PHOTON,                // Two-photon modality
	MOD_WF_FLUORESCENCE,           // Widefield-fluorescence modality
	MOD_TL_BRIGHTFIELD,            // Transmitted-light brightfield modality
	MOD_SLO_ICG_FA,                // Scanning Laser Ophthalmoscope (Indo-Cyanine Green/Fluorescence Angiogram)
	MOD_SLO_REFLIGHT,              // Scanning Laser Ophthalmoscope (Reflected Light) modality
	MOD_GENERIC					   // Non-specific modality (e.g., camera lens)
};

// Enumeration of deconvolution methods used
enum enumDeconMeths
{
	DM_EXPECTATION_MAX = 0,         // Expectation maximization method
	DM_POWER_ACCEL,                 // Power acceleration method
	DM_EXTRAPOLATION_ACCEL,         // PSF extrapolation acceleration method
	DM_GOLDS_METHOD					// Golds Method for non-blind
};

// Enumeration of initial image guess generation methods
enum enumImgGuessMeths
{
	IGM_ORIGINAL_DATA = 0,         // Original input image data
	IGM_FILTERED_ORIGINAL,         // Linear-filtered input image data
	IGM_FLAT_SHEET,                // Constant-value data
	IGM_OBJECT_INPUT			   // User specified input
};

// Enumeration of initial PSF guess generation methods
enum enumPsfGuessMeths
{
	PGM_THEORETICAL_EST = 0,       // Theoretical function estimate
	PGM_FLAT_SHEET,                // Constant value function
	PGM_AUTOCORRELATION,		   // Autocorrelation of image
	PGM_PSF_INPUT				   // User specified input
};

// Enumeration of PSF generation sources
enum enumPsfSources
{
	PSFSRC_CALCULATED = 0,         // Calculated (synthetic) PSF
	PSFSRC_MEASURED                // Measured PSF
};

// Enumeration of frequency bandlimit determination methods
enum enumFreqConsMeths
{
	FCM_AUTO_SELECTION = 0,        // Automatically choose best method
	FCM_THEORETICAL_LIMIT,         // Use theoretical frequency limits
	FCM_DETECTED_LIMIT             // Use detected frequency limits (confocal modalities only)
};

// Enumeration of methods that can be used to calculate
// the sizes of subvolumes within the overall volume
enum enumSubVolMeths
{
	SVM_STATIC = 0,                // Use predetermined, fixed values
	SVM_DYNAMIC                    // Determine values based on available memory
};

// Enumeration of methods that can be used to calculate
// the average background intensity of an image
enum enumDarkCurMeths
{
    DCM_AUTO_CALCULATION = 0,      // Scan the image and automatically calculate the darkcurrent
    DCM_MANUAL_INPUT               // Take a manually set input value as the darkcurrent
};

// Enumeration of algorithms available to generate a theoretical PSF
enum enumTheoreticalPsfGenMeths
{
	TPM_AUTOQUANT = 0,             // Legacy AutoQuant PSF generation algorithm
	TPM_GIBSON_LANNI,              // PSF generation algorithm based on Gibson-Lanni PSF model
    TPM_ZERNIKE                    // PSF generation algorithm using Zernike spherical aberration terms
};

// Enumeration of feature comparison methods used to align an image
enum enumAlignMethods
{
	AM_ADAPTIVE_METHOD = 0,        // Adaptive method
	AM_NEAREST_SLICE,              // Compare to nearest slice
	AM_COMPARE_TO_BASE             // Always compare to base slice
};

// Enumeration of fill options (how to fill the void space)
enum enumFillOptions
{
	FO_MIN_DATA_SET = 0,           // Fill the void with the minimum of the data set
	FO_RANDOM_NUMBER,              // Fill the void with random values
	FO_MAX_DATA_SET                // Fill the void with the maximum of the data set
};

// Enumeration of the Alignment direction(s)
enum enumAlignDirection
{	
	AD_XY = 0,                     // Align with respect to both X and Y axes
	AD_X_ONLY,                     // Align with respect to X axis only
	AD_Y_ONLY                      // Align with respect to Y axis only
};


// Enumeration for warping seriousness degree
enum enumSeriousDegree
{
	SD_TINY = 0,
	SD_NORMAL,
	SD_SERIOUS,
	SD_VERY_SERIOUS
};

enum enumProjectionTypes
{
	PT_MAX_INTENSITY = 0,          // Maximum intensity projection
	PT_MIN_INTENSITY,              // Minimum intensity projection
	PT_SUM_INTENSITY,              // Projection of intensity summation along optical axis
	PT_VOXEL_GRADIENT,             // Surface gradient shading
	PT_ALPHA_BLEND,                // Alpha-weighted translucent-surface shading
	PT_BEST_FOCUS,                 // Projection of most in-focus values along optical axis
	PT_SURFACE_SLICE               // Projection of topmost slice within selected region
};

// enumeration for setting the state of the window
enum enumWindowState
{
	WS_WINDOW_UNKNOWN = 0,
	WS_WINDOW_MINIMIZE,				// minimize window
	WS_WINDOW_MAXIMIZE,				// maximize window
	WS_WINDOW_NORMAL,				// show window
	WS_WINDOW_HIDE					// hide window
};
#endif // #ifndef __AQI_ENUMERATIONS_H__