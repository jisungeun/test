// aqi3dDecon.h
// Entry Point for 3D Deconvolution Routines
// Copyright 2006-2012, Media Cybernetics, Inc.
// This file may not be modified or redistributed without the permission
// of Media Cybernetics, Inc.

#ifndef __AQI_3DDECON_H__
#define __AQI_3DDECON_H__

#ifndef __AQI_DLL_CONSTANT__
#include "aqidllcst.h"
#endif

#include "aqiEnumerations.h"
#include "aqiOptionStructs.h"


////////////////////////////////////////////////////////////////////
//////////////////// DECONVOLUTION API METHODS /////////////////////
////////////////////////////////////////////////////////////////////

// aqi3dBlindDeconvolution(...)
//   3D Blind Deconvolution Entry Point
//   Validates input parameters, and begins the deconvolution process
// Parameters:
//   pstructImgInfo: Structure that defines the properties of the image to be processed
//   pstructPsfInInfo: Structure that defines the properties of the input point-spread function
//   pstructPsfOutInfo: Structure that defines the properties of the point-spread function to be saved
//   pstructDeconOpsStd: Structure that specifies standard deconvolution control parameters
//   pstructDeconOpsExp: Structure that specifies expert deconvolution control parameters
//   pfnCallback: Pointer to a function that will be called back to deliver status updates
//   pCallerInfo: Pointer to a block of memory that will be passed to pfnCallback (this memory
//                may contain any information about the calling program that the user of this
//                function wants)
// Returns:
//   Type short
//   Returns one of the codes defined in aqidllcst.h (and in the 3D Deconvolution
//   programming manual) to indicate the reason for the function's termination
extern "C" short _stdcall aqi3dBlindDeconvolution(struct aqiStructImgInfo* pstructImgInfo,
                                                  struct aqiStructPsfInfo* pstructPsfInInfo,
                                                  struct aqiStructPsfInfo* pstructPsfOutInfo,
                                                  struct aqiStructDeconOpsStd* pstructDeconOpsStd,
                                                  struct aqiStructDeconOpsExp* pstructDeconOpsExp,
                                                  aqiCbfnDllStatus pfnCallback = 0,
                                                  void* pCallerInfo = 0
                                                  );

// aqi3dBlindDeconDiskSpaceChk(...)
//   Disk Space Checking Routine
//   Checks the drives selected for output from the deconvolution algorithm and
//   verifies that those drives have enough space for the output
// Parameters:
//   pstructImgInfo: Structure that defines the properties of the image to be processed
//   pstructPsfInInfo: Structure that defines the properties of the input point-spread function
//   pstructPsfOutInfo: Structure that defines the properties of the point-spread function to be saved
//   pstructDeconOpsStd: Structure that specifies standard deconvolution control parameters
//   pstructDeconOpsExp: Structure that specifies expert deconvolution control parameters
//   lpImgOutSpaceNeeded: Output parameter that will be assigned the additional number of megabytes
//                        needed to store the output image
//   lpPsfOutSpaceNeeded: Output parameter that will be assigned the additional number of megabytes
//                        needed to store the output point-spread function
//   lpTempSpaceNeeded: Output parameter that will be assigned the additional number of megabytes
//                      needed to store the temporary files created during deconvolution
// Returns:
//   Type short
//   AQI_STATUS_NOERROR if there is enough disk space to run the deconvolution
//   AQI_STATUS_OUTOFDISK if there is not enough disk space to run the deconvolution
extern "C" short _stdcall aqi3dBlindDeconDiskSpaceChk(struct aqiStructImgInfo* pstructImgInfo,
                                                      struct aqiStructPsfInfo* pstructPsfInInfo,
                                                      struct aqiStructPsfInfo* pstructPsfOutInfo,
													  struct aqiStructDeconOpsStd* pstructDeconOpsStd,
													  struct aqiStructDeconOpsExp* pstructDeconOpsExp,
                                                      long* lpImgOutSpaceNeeded,
													  long* lpPsfOutSpaceNeeded,
													  long* lpTempSpaceNeeded
													  );

// aqi3dNonBlindDeconvolution(...)
//   3D NonBlind Deconvolution Entry Point
//   Validates input parameters, and begins the deconvolution process
// Parameters:
//   pstructImgInfo: Structure that defines the properties of the image to be processed
//   pstructPsfInInfo: Structure that defines the properties of the input point-spread function
//   pstructPsfOutInfo: Structure that defines the properties of the point-spread function to be saved
//   pstructDeconOpsStd: Structure that specifies standard deconvolution control parameters
//   pstructDeconOpsExp: Structure that specifies expert deconvolution control parameters
//   pfnCallback: Pointer to a function that will be called back to deliver status updates
//   pCallerInfo: Pointer to a block of memory that will be passed to pCallback (this memory
//                may contain any information about the calling program that the user of this
//                function wants)
// Returns:
//   Type short
//   Returns one of the codes defined in aqidllcst.h (and in the 3D Deconvolution
//   programming manual) to indicate the reason for the function's termination
extern "C" short _stdcall aqi3dNonBlindDeconvolution(struct aqiStructImgInfo* pstructImgInfo,
                                                     struct aqiStructPsfInfo* pstructPsfInInfo,
													 struct aqiStructPsfInfo* pstructPsfOutInfo,
													 struct aqiStructDeconOpsStd* pstructDeconOpsStd,
													 struct aqiStructDeconOpsExp* pstructDeconOpsExp,
													 aqiCbfnDllStatus pfnCallback,
													 void* pCallerInfo
													 );

// aqi3dNonBlindDeconDiskSpaceChk(...)
//   Disk Space Checking Routine
//   Checks the drives selected for output from the deconvolution algorithm and
//   verifies that those drives have enough space for the output
// Parameters:
//   pstructImgInfo: Structure that defines the properties of the image to be processed
//   pstructPsfInInfo: Structure that defines the properties of the input point-spread function
//   pstructPsfOutInfo: Structure that defines the properties of the point-spread function to be saved
//   pstructDeconOpsStd: Structure that specifies standard deconvolution control parameters
//   pstructDeconOpsExp: Structure that specifies expert deconvolution control parameters
//   lpImgOutSpaceNeeded: Output parameter that will be assigned the additional number of megabytes
//                        needed to store the output image
//   lpPsfOutSpaceNeeded: Output parameter that will be assigned the additional number of megabytes
//                        needed to store the output point-spread function
//   lpTempSpaceNeeded: Output parameter that will be assigned the additional number of megabytes
//                      needed to store the temporary files created during deconvolution
// Returns:
//   Type short
//   AQI_STATUS_NOERROR if there is enough disk space to run the deconvolution
//   AQI_STATUS_OUTOFDISK if there is not enough disk space to run the deconvolution
extern "C" short _stdcall aqi3dNonBlindDeconDiskSpaceChk(struct aqiStructImgInfo* pstructImgInfo,
                                                         struct aqiStructPsfInfo* pstructPsfInInfo,
														 struct aqiStructPsfInfo* pstructPsfOutInfo,
														 struct aqiStructDeconOpsStd* pstructDeconOpsStd,
														 struct aqiStructDeconOpsExp* pstructDeconOpsExp,
                                                         long* lpImgOutSpaceNeeded,
                                                         long* lpPsfOutSpaceNeeded,
                                                         long* lpTempSpaceNeeded
														 );

// aqi3dDeconValidateInputs(...)
//   Deconvolution parameter validation routine
//   Checks validity of deconvolution parameters in the structures used by the deconvolution API, and flags
//   any fields that fall outside of their valid ranges
// Parameters:
//   pstructImgInfo: Structure that defines the properties of the image to be processed
//   pstructPsfInInfo: Structure that defines the properties of the input point-spread function
//   pstructPsfOutInfo: Structure that defines the properties of the point-spread function to be saved
//   pstructDeconOpsStd: Structure that specifies standard deconvolution control parameters
//   pstructDeconOpsExp: Structure that specifies expert deconvolution control parameters
//   pstructDeconValidationFlags: Structure whose members may be loaded with bitwise flags indicating parameters that
//                                fall outside of their valid ranges
// Returns:
//   Type short
//   AQI_STATUS_NOERROR if there is enough disk space to run the deconvolution
//   AQI_STATUS_INVALIDINPUT if any parameters there is not enough disk space to run the deconvolution
extern "C" short _stdcall aqi3dDeconValidateInputs(struct aqiStructImgInfo* pstructImgInfo,
   												   struct aqiStructPsfInfo* pstructPsfInInfo,
												   struct aqiStructPsfInfo* pstructPsfOutInfo,
												   struct aqiStructDeconOpsStd* pstructDeconOpsStd,
												   struct aqiStructDeconOpsExp* pstructDeconOpsExp,
												   struct aqiStructDecon3dValidationFlags* pstructDeconValidationFlags
												   );


// aqi3dDeconTlbColorShift(...)
//   Transmitted-Light Brightfield Color Correction
//   Rescales transmitted-light brightfield data so that all channels have matching
//   maximums and minimums; this is meant to solve coloring errors in deconvolved
//   transmitted-light brightfield data
// Parameters:
//   pstructImgInfo: Array of three image information structures, which describe the
//     channels to be corrected
//   pstructDeconOpsStd: Used to identify the temp directory in which to store intermediate
//     results
//   shNumChannels: Identifies the number of channels in the pstructImgInfo array;
//     at present, this value must be 3
// Returns:
//   Type short
//   AQI_STATUS_NOERROR if the correction is successfully completed
//   AQI_STATUS_ERROR otherwise
extern "C" short _stdcall aqi3dDeconTlbColorShift(aqiStructImgInfo* pstructImgInfo,
									              aqiStructDeconOpsStd* pstructDeconOpsStd,
												  short shNumChannels
												  );

// aqi3dDeconDetectSA(...)
//   3D Spherical Aberration Detection
//   Detects spherical aberrations within the input image and store the number into the
//   fSphereAberrationFactor image parameter
//   This detection routine is relevant for the AutoQuant and Zernike PSF models. If this method is called
//   with the pstructDeconOpsExp->enPsfGenMeth field set to TPM_GIBSON_LANNI, the field will be altered to
//   TPM_ZERNIKE.
// Parameters:
//   pstructImgInfo: Structure that defines the properties of the image to be analyzed
//     -fSphereAberrationFactor will store the spherical aberration that is detected
//   pstructPsfOutInfo: Reserved for future use, currently unused
//   pstructDeconOpsStd: Structure that specifies standard deconvolution control parameters
//   pstructDeconOpsExp: Structure that specifies expert deconvolution control parameters
//   fLBoundSA: The lower bound of the SA search region (presently only used for AutoQuant PSF model)
//   fUBoundSA: The upper bound of the SA search region (presently only used for AutoQuant PSF model)
//   shAccelerated: A flag that, if nonzero, will indicate that a faster but less accurate SA search should be used
//   pfnCallback: Pointer to a function that will be called back to deliver status updates
//   pCallerInfo: Pointer to a block of memory that will be passed to pCallback (this memory
//                may contain any information about the calling program that the user of this
//                function wants)
// Returns:
//   Type short
//   Returns one of the codes defined in aqidllcst.h (and in the 3D Deconvolution
//   programming manual) to indicate the reason for the function's termination
extern "C" short _stdcall aqi3dDeconDetectSA(aqiStructImgInfo* pstructImgInfo,
											 aqiStructPsfInfo* pstructPsfOutInfo,
											 aqiStructDeconOpsStd* pstructDeconOpsStd,
											 aqiStructDeconOpsExp* pstructDeconOpsExp,
											 float fLBoundSA,
											 float fUBoundSA,
											 short shAccelerated = 1,
											 aqiCbfnDllStatus pfnCallback = 0,
											 void* pCallerInfo = 0
											 );

// aqi3dDeconDetectSampleRI(...)
//   3D Sample Embedding Refractive Index Detection
//   Given known optical parameters and image data, determines the likely refractive index of the
//   sample embedding medium, and stores the value in the fEmbeddingRefractiveIndex image parameter.
//   This detection routine is only relevant for the Gibson-Lanni PSF model, and the
//   pstructDeconOpsExp->enPsfGenMeth field will be set to TPM_GIBSON_LANNI when this method is called
// Parameters:
//   pstructImgInfo: Structure that defines the properties of the image to be analyzed
//     -fEmbeddingRefractiveIndex will store the sample refractive index that is detected
//   pstructPsfOutInfo: Reserved for future use, currently unused
//   pstructDeconOpsStd: Structure that specifies standard deconvolution control parameters
//   pstructDeconOpsExp: Structure that specifies expert deconvolution control parameters
//   fLBound: The lower bound of the parameter search region (presently not used)
//   fUBound: The upper bound of the parameter search region (presently not used)
//   shAccelerated: A flag that, if nonzero, will indicate that a faster but less accurate search should be used
//   pfnCallback: Pointer to a function that will be called back to deliver status updates
//   pCallerInfo: Pointer to a block of memory that will be passed to pCallback (this memory
//                may contain any information about the calling program that the user of this
//                function wants)
// Returns:
//   Type short
//   Returns one of the codes defined in aqidllcst.h (and in the 3D Deconvolution
//   programming manual) to indicate the reason for the function's termination
extern "C" short _stdcall aqi3dDeconDetectSampleRI(aqiStructImgInfo* pstructImgInfo,
											 aqiStructPsfInfo* pstructPsfOutInfo,
											 aqiStructDeconOpsStd* pstructDeconOpsStd,
											 aqiStructDeconOpsExp* pstructDeconOpsExp,
											 float fLBound,
											 float fUBound,
											 short shAccelerated = 1,
											 aqiCbfnDllStatus pfnCallback = 0,
											 void* pCallerInfo = 0
											 );

// aqi3dDeconDetectSampleDepth(...)
//   3D Sample Depth Detection
//   Given known optical parameters and image data, determines the likely distance from coverslip of the
//   sample, and stores the value in the fSampleDepth image parameter.
//   This detection routine is only relevant for the Gibson-Lanni PSF model, and the
//   pstructDeconOpsExp->enPsfGenMeth field will be set to TPM_GIBSON_LANNI when this method is called
// Parameters:
//   pstructImgInfo: Structure that defines the properties of the image to be analyzed
//     -fSampleDepth will store the sample depth that is detected
//   pstructPsfOutInfo: Reserved for future use, currently unused
//   pstructDeconOpsStd: Structure that specifies standard deconvolution control parameters
//   pstructDeconOpsExp: Structure that specifies expert deconvolution control parameters
//   fLBound: The lower bound of the parameter search region (presently not used)
//   fUBound: The upper bound of the parameter search region (presently not used)
//   shAccelerated: A flag that, if nonzero, will indicate that a faster but less accurate search should be used
//   pfnCallback: Pointer to a function that will be called back to deliver status updates
//   pCallerInfo: Pointer to a block of memory that will be passed to pCallback (this memory
//                may contain any information about the calling program that the user of this
//                function wants)
// Returns:
//   Type short
//   Returns one of the codes defined in aqidllcst.h (and in the 3D Deconvolution
//   programming manual) to indicate the reason for the function's termination
extern "C" short _stdcall aqi3dDeconDetectSampleDepth(aqiStructImgInfo* pstructImgInfo,
											 aqiStructPsfInfo* pstructPsfOutInfo,
											 aqiStructDeconOpsStd* pstructDeconOpsStd,
											 aqiStructDeconOpsExp* pstructDeconOpsExp,
											 float fLBound,
											 float fUBound,
											 short shAccelerated = 1,
											 aqiCbfnDllStatus pfnCallback = 0,
											 void* pCallerInfo = 0
											 );

// aqi3dDeconGetResultMaxProjection(...)
//   Retrieves the maximum projection of the volume in its current deconvolution state;
//   used for providing a preview image of a deconvolution in progress. When a deconvolution
//   callback indicates a status of AQI_STATUS_UPDATE_3DDECON_PREVIEW, the image retrievable from
//   this method has been updated.
// Parameters:
//   fImage: Image array in which to store the preview. Array size is sWidth*sHeight. Ignored if sInit is 2.
//   nWidth: Pointer to the width of the image. Set by this function when sInit is 0 or 2.
//   nHeight: Pointer to the height of the image. Set by this function when sInit is 0 or 2.
//   fXOffset: Pointer to the X offset for current subvolume. Set by function when sInit is 0. Used to determine
//             where to place the image for the current subvolume into a larger overall preview image.
//   fYOffset: Pointer to the Y offset for current subvolume. Set by function when sInit is 0. Used to determine
//             where to place the image for the current subvolume into a larger overall preview image.
//   fMax: Pointer to the maximum intensity value in the image array. Set by this function when sInit is 0.
//   fMin: Pointer to the minimum intensity value in the image array. Set by this function when sInit is 0.
//   sOpMode: Used to define how the function responds.  When the value is 2, the function just sets the nWidth and nHeight variables,
//            defining the size of the image.  You can then allocate an array of size nWidth*nHeight.
//            When the value is 0, the preview image is copied to the image array, and the other variables are set.
//            The value of 1 is used for internal purposes.  Do not use this value.
extern "C" void _stdcall aqi3dDeconGetResultMaxProjection(float* fImage,
														  int* nWidth,
														  int* nHeight,
														  float* fXOffset,
														  float* fYOffset,
														  float* fMax,
														  float* fMin,
														  short sOpMode);


// aqi3dDeconGetLastError(...)
//   Retrieves a description string that, when AQI_STATUS_ERROR is returned from a
//   deconvolution routine, may provide additional information about the cause of the error.
//
//   This routine operates in two modes. A first call to this routine with nBufferSize set to -1
//   (or any insufficiently small size) will cause the necessary buffer size to be stored in
//   nBufferSize, and will leave strErrorInfo untouched. Note that if nBufferSize is set to 0, there
//   if no error information available to retrieve.
//
//   When nBufferSize meets or exceeds the storage requirements for the error string, the last recorded
//   error string will be copied to strErrorInfo.
// Parameters:
//   strErrorInfo: A string buffer that will contain the last recorded error information when nBufferSize indicates
//                 that the buffer is large enough to receive the information
//   nBufferSize: The number of single-byte characters allocated to strErrorInfo. When set smaller than the
//                buffer size needed (e.g., -1), the necessary size will be stored to this parameter
// Returns:
//   Type short
//   Returns one of the codes defined in aqidllcst.h (and in the 3D Deconvolution
//   programming manual) to indicate the reason for the function's termination
extern "C" short _stdcall aqi3dDeconGetLastError(char* strErrorInfo, int* nBufferSize);


// aqi3dDeconSetMaxNumThreads(...)
//   Sets the maximum number of threads into which the 3D Deconvolution may split itself. As deconvolution is
//   a processor-intensive operation, each thread will often fully utilize a processor core.
//   In practical terms, this method therefore sets the number of logical processor cores that the deconvolution
//   process is permitted to use. By default, it will use as many cores as are available on the host machine.
//
// Parameters:
//   nMaxThreads: The maximum number of threads into which a single 3D Deconvolution job is allowed
//                to divide itself.
//
// Returns:
//   Type short
//   AQI_STATUS_NOERROR when the new value is set
//   AQI_STATUS_ERROR if an exception occurs
extern "C" short _stdcall aqi3dDeconSetMaxNumThreads(int nMaxThreads);


// DLL Unlocking Function
//   Processes the key pair indicated by this function's parameters -
//   if the pair is valid, the DLL will be unlocked
// Parameters:
//   lpstrProgramKey: The program part of the DLL unlocking key; hexadecimal string passed
//                    directly from the calling program
//   lpstrRegKeyLocation: The fully-qualified registry subkey underneath HKEY_CURRENT_USER
//                        where the registry-stored part of the DLL-unlocking key can be found
//   lpstrRegKeyValueName: The name of the registry variable that stores the registry part of
//                         the DLL key
// Returns:
//   Type short
//   AQI_STATUS_DONE if the DLL was successfully unlocked
//   AQI_STATUS_DLLLOCKED if the DLL was not successfully unlocked
extern "C" short _stdcall aqiUnlock3dBlindDeconDll(const char* lpstrProgramKey,
                                                   const char* lpstrRegKeyLocation,
												   const char* lpstrRegKeyValueName);

extern "C" short _stdcall aqi3dDeconFlag(short flag);

extern "C" void _stdcall aqi3dDeconSetLogName(const char * strName);

#endif // #ifndef __AQI_3DDECON_H__