#pragma once

#include <MaskData.h>
#include <IMaskWriter.h>
#include "CBCResearchMaskDataIOExport.h"

namespace CBCResearch::Plugins {
    class CBCResearchMaskDataIO_API MaskDataWriter : public UseCase::IMaskWriter {
    public:
        MaskDataWriter();
        virtual ~MaskDataWriter();

        auto Write(Entity::MaskData::Pointer data, const QString& path)->bool override;
        auto UpdateRow(int index, Entity::MaskData::Pointer data, const QString& path)->bool override;
    };
}