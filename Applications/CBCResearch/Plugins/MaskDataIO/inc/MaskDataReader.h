#pragma once

#include <QString>

#include <IMaskReader.h>
#include "CBCResearchMaskDataIOExport.h"

namespace CBCResearch::Plugins {
    class CBCResearchMaskDataIO_API MaskDataReader : public UseCase::IMaskReader {
    public:
        MaskDataReader();
        virtual ~MaskDataReader();

        auto Read(const QString& path, const bool loadMaskVolume = false) const -> Entity::MaskData::Pointer override;
    };
}