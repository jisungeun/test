#include <TCMaskReader.h>
#include "MaskDataReader.h"

namespace CBCResearch::Plugins {
    MaskDataReader::MaskDataReader() {
    }

    MaskDataReader::~MaskDataReader() {
    }

    auto MaskDataReader::Read(const QString& path, const bool loadMaskVolume) const -> Entity::MaskData::Pointer {
        auto reader = TC::IO::TCMaskReader(path);
        if (!reader.Exist()) return nullptr;

        Entity::MaskData::Pointer maskData(new Entity::MaskData());

        const auto blobs = reader.GetBlobCount("HT", 0);
        for (int idx = 0; idx < blobs; idx++) {
            auto data = reader.ReadBlob("HT", 0, idx);
            auto index = std::get<0>(data);
            auto bbox = std::get<1>(data);
            auto code = std::get<2>(data);
            auto offset = bbox.GetOffset();
            auto size = bbox.GetSize();

            Entity::MaskBlob blob;
            blob.SetBoundingBox(offset.x0, offset.y0, offset.z0, offset.x0+size.d0-1, offset.y0+size.d1-1, offset.z0+size.d2-1);
            blob.SetCode(code);
            maskData->AppendBlob(index, blob);

            if (!loadMaskVolume) continue;

            auto maskVolume = reader.ReadMask("HT", 0, idx);
            maskData->AppendMaskVolume(index, maskVolume);
        }

        return maskData;
    }
}