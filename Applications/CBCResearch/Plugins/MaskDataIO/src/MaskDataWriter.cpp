#include <TCMaskWriter.h>
#include "MaskDataWriter.h"

namespace CBCResearch::Plugins {
    MaskDataWriter::MaskDataWriter() {
    }

    MaskDataWriter::~MaskDataWriter() {
    }

    auto MaskDataWriter::Write(Entity::MaskData::Pointer data, const QString& path) -> bool {
        const auto counts = data->CountBlobs();
        TC::IO::TCMaskWriter writer(path, counts);

        const auto indexes = data->GetBlobIndexes();

        for(const auto index : indexes) {
            auto blob = data->GetBlob(index);
            auto inBox = blob.GetBoundingBox();
            auto code = blob.GetCode();

            TC::IO::BoundingBox bbox;
            bbox.SetOffset(std::get<0>(inBox), std::get<1>(inBox), std::get<2>(inBox));
            bbox.SetSize(std::get<3>(inBox) - std::get<0>(inBox) + 1,
                std::get<4>(inBox) - std::get<1>(inBox) + 1,
                std::get<5>(inBox) - std::get<2>(inBox) + 1);
            if (!writer.WriteBlob("HT", 0, index, bbox, code)) return false;

            auto maskVolume = blob.GetMaskVolume();
            if (!writer.WriteMask("HT", 0, index, bbox, maskVolume)) return false;
        }

        return true;
    }

    auto MaskDataWriter::UpdateRow(int index, Entity::MaskData::Pointer data, const QString& path) -> bool {
        TC::IO::TCMaskWriter writer(path);

        auto blob = data->GetBlob(index);
        auto inBox = blob.GetBoundingBox();
        auto code = blob.GetCode();

        TC::IO::BoundingBox bbox;
        bbox.SetOffset(std::get<0>(inBox), std::get<1>(inBox), std::get<2>(inBox));
        bbox.SetSize(std::get<3>(inBox) - std::get<0>(inBox) + 1,
            std::get<4>(inBox) - std::get<1>(inBox) + 1,
            std::get<5>(inBox) - std::get<2>(inBox) + 1);

        if (!writer.UpdateBlob("HT", 0, index, bbox, code)) return false;

        return true;
    }
}
