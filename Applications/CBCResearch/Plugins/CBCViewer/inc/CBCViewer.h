#pragma once

#include <memory>
#include <QWidget>
#include <enum.h>
#include "CBCMask.h"
#include "CBCResearchCBCViewerExport.h"

namespace CBCResearch::Plugins {

    BETTER_ENUM(CBCViewerColormap, int,
                Turbo               = 10,
                Jet                 = 20,
                Hot                 = 30,
                Rainbow             = 40,
                InverseGreyscale    = 50
    )

    class CBCResearchCBCViewer_API CBCViewer : public QWidget {
        Q_OBJECT
    public:
        CBCViewer(QWidget* parent=nullptr);
        virtual ~CBCViewer();

        auto SetTCFPath(const QString& strPath)->void;
        auto SetMask(CBCMaskData::Pointer& mask)->void;
        auto ShowBoundingBox(bool show)->void;
        auto ShowMask(bool show)->void;
        auto IsMaskLoaded(void) const->bool;
        auto SetRIThresholdForGray(double minValue, double maxValue)->void;
        auto SetRIThresholdForColor(double minValue, double maxValue)->void;
        auto SetBFAsDefault()->void;
        auto SetHTAsDefault()->void;
        auto SetColormap(CBCViewerColormap cmap)->void;

    protected:
        auto UpdateImages(int slice, bool force=false)->void;
        auto UpdateMask(int slice)->void;

    signals:
        void mouseClicked(int index, QPoint point);
        void mouseDoubleClicked(int index, QPoint point);
        void RIThresholdChanged(int index, double minValue, double maxValue);
        void BBoxSelected(int index);

    protected slots:
        void onChangeZSlice(int slice);
        void onResetZSlice();
        void onLeftSliderMinValueChanged(int value);
        void onLeftSliderMaxValueChanged(int value);
        void onRightSliderMinValueChanged(int value);
        void onRightSliderMaxValueChanged(int value);
        void onLeftSpinMinValueChanged(double value);
        void onLeftSpinMaxValueChanged(double value);
        void onRightSpinMinValueChanged(double value);
        void onRightSpinMaxValueChanged(double value);
        void onLeftViewClicked(QPoint point);
        void onLeftViewDoubleClicked(QPoint point);
        void onRightViewClicked(QPoint point);
        void onRightViewDoubleClicked(QPoint point);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}