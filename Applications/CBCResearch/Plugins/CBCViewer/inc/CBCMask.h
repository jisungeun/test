#pragma once

#include <memory>
#include <QColor>
#include "CBCResearchCBCViewerExport.h"

namespace CBCResearch::Plugins {
    class CBCResearchCBCViewer_API CBCMaskBlob {
    public:
        typedef std::shared_ptr<uint8_t> VolumePtr;

    public:
        CBCMaskBlob();
        CBCMaskBlob(const CBCMaskBlob& other);
        CBCMaskBlob(int x0, int y0, int z0, int d0, int d1, int d2, QColor color, int index);
        virtual ~CBCMaskBlob();

        auto GetBoundingBox() const->std::tuple<int, int, int, int, int, int>;
        auto GetColor() const->QColor;
        auto GetIndex() const->int;

        auto SetMaskVolume(VolumePtr volume)->void;
        auto GetMaskVolume()->VolumePtr;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class CBCResearchCBCViewer_API CBCMaskData {
    public:
        typedef CBCMaskData Self;
        typedef std::shared_ptr<Self> Pointer;

    public:
        CBCMaskData();
        virtual ~CBCMaskData();

        auto AppendBlob(const CBCMaskBlob& blob)->void;
        auto GetBlob(int index) const->CBCMaskBlob;
        auto CountBlobs() const->int;

        auto AppendMaskVolume(int index, CBCMaskBlob::VolumePtr maskVolume)->void;
        auto GetMaskVolume(int index) const->CBCMaskBlob::VolumePtr;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}