#include <QCoreApplication>
#include <QBoxLayout>
#include <QMenuBar>
#include <QFileDialog>
#include <QStatusBar>
#include <QImage>
#include <QSettings>

#include <CBCViewer.h>

#include "mainwindow.h"

struct MainWindow::Impl {
    CBCResearch::Plugins::CBCViewer* imageview{ nullptr };
};

MainWindow::MainWindow(QWidget* parent, Qt::WindowFlags flags) : QMainWindow(parent, flags), d{ new Impl } {
    setWindowTitle("CBCViewer Example");
    setMinimumSize(QSize(1400, 700));

    d->imageview = new CBCResearch::Plugins::CBCViewer();
    setCentralWidget(d->imageview);

    auto fileMenu = menuBar()->addMenu(tr("&File"));
    {
        auto openAct = new QAction("&Open");
        fileMenu->addAction(openAct);
        fileMenu->addSeparator();
        auto quitAct = new QAction("&Quit");
        fileMenu->addAction(quitAct);

        connect(openAct, SIGNAL(triggered()), this, SLOT(onOpen()));
        connect(quitAct, SIGNAL(triggered()), qApp, SLOT(quit()));
    }
}

MainWindow::~MainWindow() {
}

void MainWindow::onOpen() {
    auto prev = QSettings().value("recentFile", "").toString();
    auto filename = QFileDialog::getOpenFileName(this, "Select TCF", prev, "*.TCF");
    if (filename.isEmpty()) return;
    QSettings().setValue("recentFile", filename);

    d->imageview->SetTCFPath(filename);
}
