#include <QList>
#include "CBCMask.h"

namespace CBCResearch::Plugins {

    struct CBCMaskBlob::Impl {
        Impl() = default;
        Impl(const Impl& other) {
            offset = other.offset;
            size = other.size;
            color = other.color;
            index = other.index;
            maskVolume = other.maskVolume;
        }

        struct {
            int x0, y0, z0;
        } offset{ 0, 0, 0 };

        struct {
            int d0, d1, d2;
        } size{ 1, 1, 1};

        QColor color;
        int index;

        VolumePtr maskVolume{ nullptr };
    };

    CBCMaskBlob::CBCMaskBlob() : d{ new Impl() } {
    }

    CBCMaskBlob::CBCMaskBlob(const CBCMaskBlob& other) : d{ new Impl(*(other.d)) } {
    }

    CBCMaskBlob::CBCMaskBlob(int x0, int y0, int z0, int d0, int d1, int d2, QColor color, int index) : d{ new Impl } {
        d->offset.x0 = x0;
        d->offset.y0 = y0;
        d->offset.z0 = z0;
        d->size.d0 = d0;
        d->size.d1 = d1;
        d->size.d2 = d2;
        d->color = color;
        d->index = index;
    }

    CBCMaskBlob::~CBCMaskBlob() {
    }

    auto CBCMaskBlob::GetBoundingBox() const -> std::tuple<int, int, int, int, int, int> {
        return std::make_tuple(d->offset.x0, d->offset.y0, d->offset.z0, d->size.d0, d->size.d1, d->size.d2);
    }

    auto CBCMaskBlob::GetColor() const -> QColor {
        return d->color;
    }

    auto CBCMaskBlob::GetIndex() const -> int {
        return d->index;
    }

    auto CBCMaskBlob::SetMaskVolume(VolumePtr volume) -> void {
        d->maskVolume = volume;
    }

    auto CBCMaskBlob::GetMaskVolume() -> VolumePtr {
        return d->maskVolume;
    }

    struct CBCMaskData::Impl {
        QList<CBCMaskBlob> blobs;
    };

    CBCMaskData::CBCMaskData() : d{ new Impl } {
    }

    CBCMaskData::~CBCMaskData() {
    }

    auto CBCMaskData::AppendBlob(const CBCMaskBlob& blob) -> void {
        d->blobs.push_back(blob);
    }

    auto CBCMaskData::GetBlob(int index) const -> CBCMaskBlob {
        return d->blobs.at(index);
    }

    auto CBCMaskData::CountBlobs() const -> int {
        return d->blobs.size();
    }

    auto CBCMaskData::AppendMaskVolume(int index, CBCMaskBlob::VolumePtr maskVolume) -> void {
        d->blobs[index].SetMaskVolume(maskVolume);
    }

    auto CBCMaskData::GetMaskVolume(int index) const -> CBCMaskBlob::VolumePtr {
        return d->blobs[index].GetMaskVolume();
    }
}
