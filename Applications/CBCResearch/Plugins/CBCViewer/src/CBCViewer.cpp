#include <QGraphicsItem>
#include <QBitmap>

#include <SimpleImageView.h>
#include <TCFSimpleReader.h>
#include <VolumeData.h>
#include <Colorize.h>

#include "ui_CBCViewer.h"
#include "CBCViewer.h"

namespace CBCResearch::Plugins {
    auto ConvertToTCColormap(CBCViewerColormap cmap) -> TC::ColorMap {
        TC::ColorMap colormap = TC::ColorMap::Turbo;

        switch (cmap) {
        case CBCViewerColormap::Turbo:
            colormap = TC::ColorMap::Turbo;
            break;
        case CBCViewerColormap::Jet:
            colormap = TC::ColorMap::Jet;
            break;
        case CBCViewerColormap::Hot:
            colormap = TC::ColorMap::Hot;
            break;
        case CBCViewerColormap::Rainbow:
            colormap = TC::ColorMap::Rainbow;
            break;
        case CBCViewerColormap::InverseGreyscale:
            colormap = TC::ColorMap::Inverse_Greyscale;
            break;
        }

        return colormap;
    }

    struct CBCViewer::Impl {
        Ui::CBCViewer* ui{ nullptr };
        TC::IO::VolumeData::Pointer volumeData{ nullptr };
        TC::IO::VolumeData::Pointer bfData{ nullptr };
        CBCMaskData::Pointer maskData{ nullptr };
        bool hasBFData{ false };
        bool showBoundingBox{ false };
        bool showMask{ false };
        QColor selectionColor;

        struct {
            uint32_t x, y, z;
        } dims{ 1, 1, 1 };

        struct {
            uint32_t x, y;
        } bfDims{ 1, 1 };

        struct {
            double grayMin, grayMax;
            double colorMin, colorMax;
        } threshold{ 1.337, 1.4500, 1.337, 1.4500 };

        enum class ImageMode {
            BF,
            HT
        } rightImageMode{ ImageMode::BF };

        CBCViewerColormap rightColormap{ CBCViewerColormap::Turbo };

        QMap<int, std::shared_ptr<std::vector<uint8_t>>> sliceImagesGray;
        QMap<int, std::shared_ptr<std::vector<uint8_t>>> sliceImagesColor;
        std::shared_ptr<std::vector<uint32_t>> sliceImageBF;
        bool firstUpdate{ true };
        int lastSlice{ 0 };

        auto leftView() { return ui->leftImageView; }
        auto rightView() { return ui->rightImageView; }
        auto zSlider() { return ui->zSlider; }

        const int indexKey = 0; //for graphicsitem
    };

    CBCViewer::CBCViewer(QWidget* parent) : d{ new Impl } {
        d->ui = new Ui::CBCViewer();
        d->ui->setupUi(this);
        
        d->zSlider()->setSingleStep(1);
        d->zSlider()->setPageStep(1);

        const double minValue = 1.3300;
        const double maxValue = 1.4700;

        d->ui->leftRISlider->SetRange(minValue*10000, maxValue*10000);
        d->ui->rightRISlider->SetRange(minValue*10000, maxValue*10000);
        d->ui->leftRIMinSpin->setRange(minValue, maxValue);
        d->ui->leftRIMaxSpin->setRange(minValue, maxValue);
        d->ui->rightRIMinSpin->setRange(minValue, maxValue);
        d->ui->rightRIMaxSpin->setRange(minValue, maxValue);
        d->ui->leftRIMinSpin->setSingleStep(0.001);
        d->ui->leftRIMaxSpin->setSingleStep(0.001);
        d->ui->rightRIMinSpin->setSingleStep(0.001);
        d->ui->rightRIMaxSpin->setSingleStep(0.001);

        connect(d->zSlider(), SIGNAL(valueChanged(int)), this, SLOT(onChangeZSlice(int)));
        connect(d->ui->resetButton, SIGNAL(clicked()), this, SLOT(onResetZSlice()));

        connect(d->leftView(), SIGNAL(mouseClicked(QPoint)), this, SLOT(onLeftViewClicked(QPoint)));
        connect(d->leftView(), SIGNAL(mouseDoubleClicked(QPoint)), this, SLOT(onLeftViewDoubleClicked(QPoint)));
        connect(d->rightView(), SIGNAL(mouseClicked(QPoint)), this, SLOT(onRightViewClicked(QPoint)));
        connect(d->rightView(), SIGNAL(mouseDoubleClicked(QPoint)), this, SLOT(onRightViewDoubleClicked(QPoint)));
        connect(d->ui->leftRISlider, SIGNAL(lowerValueChanged(int)), this, SLOT(onLeftSliderMinValueChanged(int)));
        connect(d->ui->leftRISlider, SIGNAL(upperValueChanged(int)), this, SLOT(onLeftSliderMaxValueChanged(int)));
        connect(d->ui->rightRISlider, SIGNAL(lowerValueChanged(int)), this, SLOT(onRightSliderMinValueChanged(int)));
        connect(d->ui->rightRISlider, SIGNAL(upperValueChanged(int)), this, SLOT(onRightSliderMaxValueChanged(int)));
        connect(d->ui->leftRIMinSpin, SIGNAL(valueChanged(double)), this, SLOT(onLeftSpinMinValueChanged(double)));
        connect(d->ui->leftRIMaxSpin, SIGNAL(valueChanged(double)), this, SLOT(onLeftSpinMaxValueChanged(double)));
        connect(d->ui->rightRIMinSpin, SIGNAL(valueChanged(double)), this, SLOT(onRightSpinMinValueChanged(double)));
        connect(d->ui->rightRIMaxSpin, SIGNAL(valueChanged(double)), this, SLOT(onRightSpinMaxValueChanged(double)));
    }

    CBCViewer::~CBCViewer() {
    }

    auto CBCViewer::SetTCFPath(const QString& strPath) -> void {
        d->maskData = nullptr;
        d->sliceImagesGray.clear();
        d->sliceImagesColor.clear();
        d->sliceImageBF = nullptr;
        d->firstUpdate = true;

        auto reader = TC::IO::TCFSimpleReader(strPath);
        {
            d->volumeData = std::make_shared<TC::IO::VolumeData>(reader.ReadHT3D(0, true));
            if (!d->volumeData->IsValid()) {
                d->volumeData = nullptr;
                return;
            }
        }

        auto bfReader = TC::IO::TCFSimpleReader(strPath);
        {
            d->hasBFData = bfReader.BFExists();
            if (d->hasBFData) {
                d->bfData = std::make_shared<TC::IO::VolumeData>(bfReader.ReadBF());
                if(!d->bfData->IsValid()) {
                    d->hasBFData = false;
                    return;
                }

                auto bfDims = d->bfData->GetDimension();
                d->bfDims.x = std::get<0>(bfDims);
                d->bfDims.y = std::get<1>(bfDims);
            }
            else {
                d->bfData = nullptr;
            }
        }

        d->ui->rightRIMinSpin->setEnabled(!d->hasBFData || (d->rightImageMode == Impl::ImageMode::HT));
        d->ui->rightRIMaxSpin->setEnabled(!d->hasBFData || (d->rightImageMode == Impl::ImageMode::HT));
        d->ui->rightRISlider->setEnabled(!d->hasBFData || (d->rightImageMode == Impl::ImageMode::HT));

        auto dims = d->volumeData->GetDimension();
        d->dims.x = std::get<0>(dims);
        d->dims.y = std::get<1>(dims);
        d->dims.z = std::get<2>(dims);
        d->zSlider()->setRange(0, d->dims.z - 1);

        auto center = static_cast<int>(round(d->dims.z / 2.0));
        d->zSlider()->setValue(center);

        UpdateImages(center);
    }

    auto CBCViewer::SetMask(CBCMaskData::Pointer& mask) -> void {
        d->maskData = mask;
        UpdateImages(d->lastSlice, true);
    }

    auto CBCViewer::ShowBoundingBox(bool show) -> void {
        d->showBoundingBox = show;
        UpdateImages(d->lastSlice, true);
    }

    auto CBCViewer::ShowMask(bool show) -> void {
        d->showMask = show;
        UpdateImages(d->lastSlice, true);
    }

    auto CBCViewer::IsMaskLoaded() const -> bool {
        const auto count = d->maskData->CountBlobs();
        if (count == 0) return true;

        const auto maskVolume = d->maskData->GetMaskVolume(0);
        return (maskVolume.get() != nullptr);
    }

    auto CBCViewer::SetRIThresholdForGray(double minValue, double maxValue) -> void {
        if ((d->threshold.grayMin == minValue) && (d->threshold.grayMax == maxValue)) return;

        d->threshold.grayMin = minValue;
        d->threshold.grayMax = maxValue;
        d->sliceImagesGray.clear();
        UpdateImages(d->lastSlice, true);

        blockSignals(true);
        onLeftSliderMinValueChanged(minValue * 10000);
        onLeftSliderMaxValueChanged(maxValue * 10000);
        blockSignals(false);
    }

    auto CBCViewer::SetRIThresholdForColor(double minValue, double maxValue) -> void {
        if ((d->threshold.colorMin == minValue) && (d->threshold.colorMax == maxValue)) return;

        d->threshold.colorMin = minValue;
        d->threshold.colorMax = maxValue;
        d->sliceImagesColor.clear();
        UpdateImages(d->lastSlice, true);

        blockSignals(true);
        onRightSliderMinValueChanged(minValue * 10000);
        onRightSliderMaxValueChanged(maxValue * 10000);
        blockSignals(false);
    }

    auto CBCViewer::SetBFAsDefault() -> void {
        d->rightImageMode = Impl::ImageMode::BF;

        d->ui->rightRIMinSpin->setEnabled(!d->hasBFData);
        d->ui->rightRIMaxSpin->setEnabled(!d->hasBFData);
        d->ui->rightRISlider->setEnabled(!d->hasBFData);
    }

    auto CBCViewer::SetHTAsDefault() -> void {
        d->rightImageMode = Impl::ImageMode::HT;

        d->ui->rightRIMinSpin->setEnabled(!d->hasBFData);
        d->ui->rightRIMaxSpin->setEnabled(!d->hasBFData);
        d->ui->rightRISlider->setEnabled(!d->hasBFData);
    }

    auto CBCViewer::SetColormap(CBCViewerColormap cmap) -> void {
        d->rightColormap = cmap;
        UpdateImages(d->lastSlice, true);
    }

    auto CBCViewer::UpdateImages(int slice, bool force) -> void {
        if (d->volumeData.get() == nullptr) return;

        const auto range = d->volumeData->GetRange();
        auto rawImage = d->volumeData->GetDataAsFloat();
        if (rawImage.get() == nullptr) return;

        if (!force && !d->firstUpdate && (slice == d->lastSlice)) return;

        auto sliceImageGray = [&]() {
            if(d->sliceImagesGray.contains(slice)) {
                return d->sliceImagesGray[slice];
            } else {
                const auto elements = d->dims.x * d->dims.y;
                auto image = std::make_shared<std::vector<uint8_t>>(elements);
                const auto offset = elements * slice;
                const auto minValue = d->threshold.grayMin; // std::max<double>(d->threshold.grayMin, std::get<0>(range));
                const auto maxValue = d->threshold.grayMax; // std::min<double>(d->threshold.grayMax, std::get<1>(range));
                const auto range = maxValue - minValue;

                for (uint32_t idx = 0; idx < elements; ++idx) {
                    const auto original = rawImage.get()[offset + idx];
                    auto value = std::max<double>(original, minValue);
                    value = std::min<double>(value, maxValue);
                    const auto delta = value - minValue;
                    
                    image->data()[idx] = static_cast<uint8_t>( delta / range * 255);
                }
                d->sliceImagesGray[slice] = image;
                return image;
            }
        }();

        auto qImageGray = QImage(sliceImageGray->data(), d->dims.x, d->dims.y, d->dims.x, QImage::Format_Indexed8);
        d->leftView()->ShowImage(qImageGray, d->firstUpdate);

        if(d->hasBFData && (d->rightImageMode == Impl::ImageMode::BF)) {
            auto bfRawImage = d->bfData->GetDataAsUInt32();
            if (bfRawImage.get() == nullptr) return;

            auto sliceImageBF = [&]() {
                if(d->sliceImageBF.get()) {
                    return d->sliceImageBF;
                } else {
                    const auto elements = d->bfDims.x * d->bfDims.y;
                    auto image = std::make_shared<std::vector<uint32_t>>(elements);
                    for(uint32_t idx=0; idx<elements; ++idx) {
                        image->data()[idx] = bfRawImage.get()[idx];
                    }
                    d->sliceImageBF = image;
                    return image;
                }
            }();

            auto qImageColor = QImage(reinterpret_cast<uchar*>(sliceImageBF->data()), d->bfDims.y, d->bfDims.x, QImage::Format_RGB32);
            d->rightView()->ShowImage(qImageColor, d->firstUpdate);
        } else {
            auto sliceImageColor = [&]() {
                if (d->sliceImagesColor.contains(slice)) {
                    return d->sliceImagesColor[slice];
                } else {
                    const auto elements = d->dims.x * d->dims.y;
                    auto image = std::make_shared<std::vector<uint8_t>>(elements);
                    auto offset = elements * slice;
                    const auto minValue = d->threshold.colorMin; // std::max<double>(d->threshold.colorMin, std::get<0>(range));
                    const auto maxValue = d->threshold.colorMax; // std::min<double>(d->threshold.colorMax, std::get<1>(range));
                    const auto range = maxValue - minValue;

                    for (uint32_t idx = 0; idx < elements; ++idx) {
                        const auto original = rawImage.get()[offset + idx];
                        auto value = std::max<double>(original, minValue);
                        value = std::min<double>(value, maxValue);
                        const auto delta = value - minValue;

                        image->data()[idx] = static_cast<uint8_t>(delta / range * 255);
                    }
                    d->sliceImagesColor[slice] = image;
                    return image;
                }
            }();

            auto qImageColor = QImage(sliceImageColor->data(), d->dims.x, d->dims.y, d->dims.x, QImage::Format_Indexed8);
            d->rightView()->ShowImage(TC::Colorize(qImageColor, ConvertToTCColormap(d->rightColormap)), d->firstUpdate);
        }

        UpdateMask(slice);

        d->firstUpdate = false;
        d->lastSlice = slice;
    }

    auto CBCViewer::UpdateMask(int slice) -> void {
        d->leftView()->ClearItems();

        if (d->maskData.get() == nullptr) return;
        if (!d->showBoundingBox && !d->showMask) return;

        const auto blobs = d->maskData->CountBlobs();
        for (int idx = 0; idx < blobs; ++idx) {
            const auto blob = d->maskData->GetBlob(idx);
            const auto maskVolume = d->maskData->GetMaskVolume(idx);

            const auto bbox = blob.GetBoundingBox();
            const auto z0 = std::min<int>(std::get<2>(bbox), std::get<5>(bbox));
            const auto z1 = std::max<int>(std::get<2>(bbox), std::get<5>(bbox));

            if ((slice < z0) || (slice > z1)) continue;

            const auto x0 = static_cast<qreal>(std::min<int>(std::get<0>(bbox), std::get<3>(bbox)));
            const auto x1 = static_cast<qreal>(std::max<int>(std::get<0>(bbox), std::get<3>(bbox)));
            const auto y0 = static_cast<qreal>(std::min<int>(std::get<1>(bbox), std::get<4>(bbox)));
            const auto y1 = static_cast<qreal>(std::max<int>(std::get<1>(bbox), std::get<4>(bbox)));
            const auto xSize = x1 - x0 + 1;
            const auto ySize = y1 - y0 + 1;
            
            if(d->showMask && (maskVolume.get() != nullptr)) {
                const auto width = static_cast<size_t>(xSize);
                const auto height = static_cast<size_t>(ySize);
                const auto offset = (slice-z0) * width * height;

                auto* ptr = maskVolume.get() + offset;
                auto image = QImage(static_cast<int>(width), static_cast<int>(height), QImage::Format_Mono);

                auto setPixel = [&image](const int x, const int y, const int pixel)->void {
                    const uchar mask = 0x80 >> (x % 8);
                    if (pixel) {
                        image.scanLine(y)[x / 8] |= mask;
                    } else {
                        image.scanLine(y)[x / 8] &= ~mask;
                    }
                };

                for (size_t y = 0; y < height; y++) {
                    for (size_t x = 0; x < width; x++) {
                        setPixel(static_cast<int>(x), static_cast<int>(y), *(ptr + (y * width) + x) == 0);
                    }
                }

                auto pixmap = QPixmap(image.size());
                pixmap.fill(blob.GetColor());
                pixmap.setMask(QBitmap::fromImage(image));

                auto pixmapItem = new QGraphicsPixmapItem(pixmap);
                pixmapItem->setOffset(x0, y0);
                d->leftView()->AddGraphicsItem(pixmapItem);
            } else {
                auto rectItem = new QGraphicsRectItem(x0, y0, xSize, ySize);
                rectItem->setPen(QPen(blob.GetColor()));
                rectItem->setData(d->indexKey, blob.GetIndex());
                d->leftView()->AddGraphicsItem(rectItem);
            }
        }
    }

    void CBCViewer::onChangeZSlice(int slice) {
        UpdateImages(slice);
    }

    void CBCViewer::onResetZSlice() {
        d->zSlider()->setValue(static_cast<int>(round(d->dims.z / 2.0)));
    }

    void CBCViewer::onLeftSliderMinValueChanged(int value) {
        double threshold = value / 10000.0;
        d->ui->leftRIMinSpin->setValue(threshold);
    }

    void CBCViewer::onLeftSliderMaxValueChanged(int value) {
        double threshold = value / 10000.0;
        d->ui->leftRIMaxSpin->setValue(threshold);
    }

    void CBCViewer::onRightSliderMinValueChanged(int value) {
        double threshold = value / 10000.0;
        d->ui->rightRIMinSpin->setValue(threshold);
    }

    void CBCViewer::onRightSliderMaxValueChanged(int value) {
        double threshold = value / 10000.0;
        d->ui->rightRIMaxSpin->setValue(threshold);
    }

    void CBCViewer::onLeftSpinMinValueChanged(double value) {
        d->ui->leftRISlider->blockSignals(true);
        d->ui->leftRISlider->SetLowerValue(static_cast<int>(value * 10000));
        d->ui->leftRISlider->blockSignals(false);

        const double maxValue = d->ui->leftRIMaxSpin->value();
        SetRIThresholdForGray(value, maxValue);
        emit RIThresholdChanged(0, value, maxValue);
    }

    void CBCViewer::onLeftSpinMaxValueChanged(double value) {
        d->ui->leftRISlider->blockSignals(true);
        d->ui->leftRISlider->SetUpperValue(static_cast<int>(value * 10000));
        d->ui->leftRISlider->blockSignals(false);

        const double minValue = d->ui->leftRIMinSpin->value();
        SetRIThresholdForGray(minValue, value);
        emit RIThresholdChanged(0, minValue, value);
    }

    void CBCViewer::onRightSpinMinValueChanged(double value) {
        d->ui->rightRISlider->blockSignals(true);
        d->ui->rightRISlider->SetLowerValue(static_cast<int>(value * 10000));
        d->ui->rightRISlider->blockSignals(false);

        const double maxValue = d->ui->rightRIMaxSpin->value();
        SetRIThresholdForColor(value, maxValue);
        emit RIThresholdChanged(1, value, maxValue);
    }

    void CBCViewer::onRightSpinMaxValueChanged(double value) {
        d->ui->rightRISlider->blockSignals(true);
        d->ui->rightRISlider->SetUpperValue(static_cast<int>(value * 10000));
        d->ui->rightRISlider->blockSignals(false);

        const double minValue = d->ui->rightRIMinSpin->value();
        SetRIThresholdForColor(minValue, value);
        emit RIThresholdChanged(1, minValue, value);
    }

    void CBCViewer::onLeftViewClicked(QPoint point) {
        if (d->showBoundingBox) {
            auto items = d->leftView()->GraphicsItemsAt(point);
            if (items.size() == 0) return;

            for (auto item : items) {
                if (item->type() != QGraphicsRectItem::Type) continue;

                auto rectItem = qgraphicsitem_cast<QGraphicsRectItem*>(item);
                if (rectItem == nullptr) continue;

                const auto index = rectItem->data(d->indexKey).toInt();
                emit BBoxSelected(index);

                break;
            }
        } else {
            emit mouseClicked(0, point);
        }
    }

    void CBCViewer::onLeftViewDoubleClicked(QPoint point) {
        emit mouseDoubleClicked(0, point);
    }

    void CBCViewer::onRightViewClicked(QPoint point) {
        emit mouseClicked(1, point);
    }

    void CBCViewer::onRightViewDoubleClicked(QPoint point) {
        emit mouseDoubleClicked(1, point);
    }
}
