#include <QFileInfo>
#include <CellType.h>
#include <TCFSimpleReader.h>
#include <TCMaskReader.h>
#include "CBCCalculator.h"

namespace CBCResearch::Plugins {
    CBCCalculator::CBCCalculator() {
    }

    CBCCalculator::~CBCCalculator() {
    }

    auto CBCCalculator::Calculate(ListType& list) -> Entity::CBCData::Pointer {
        Entity::CBCData::Pointer result{ new Entity::CBCData() };

        if (!CountCells(list, result)) return result;
        if (!CalculateRBC(list, result)) return result;
        if (!CalculatePLT(list, result)) return result;

        return result;
    }

    auto CBCCalculator::CountCells(ListType& list, Entity::CBCData::Pointer& data) -> bool {

        using CellType = Entity::CellType;
        using CountEntry = Entity::CBCData::CountEntry;

        for (const auto& item : list) {
            const auto& tcfPath = item.first;
            const auto& maskPath = item.second;
            if (!QFile::exists(maskPath)) continue;

            auto maskReader = TC::IO::TCMaskReader(maskPath);
            if (!maskReader.Exist()) continue;

            const auto blobs = maskReader.GetBlobCount("HT", 0);
            for (int idx = 0; idx < blobs; ++idx) {
                const auto blob = maskReader.ReadBlob("HT", 0, idx);
                const auto codeValue = std::get<2>(blob);
                const auto typeCode = CellType::Convert(codeValue, CellType::TypeCode::Excluded);
                const auto celltype = CellType(typeCode);

                if (celltype.IsRBC()) {
                    data->AddCellCount(CountEntry::rbcCount);
                } else if (celltype.IsPlatelet()) {
                    data->AddCellCount(CountEntry::pltCount);
                } else if (celltype.IsWBC()) {
                    data->AddCellCount(CountEntry::wbcCount);
                    data->AddWBCCount(typeCode);
                }
            }
        }

        return true;
    }

    auto CBCCalculator::CalculateRBC(ListType& list, Entity::CBCData::Pointer& data) -> bool {
        using CellType = Entity::CellType;
        using CountEntry = Entity::CBCData::CountEntry;

        for (const auto& item : list) {
            const auto& tcfPath = item.first;
            const auto& maskPath = item.second;
            if (!QFile::exists(maskPath)) continue;

            auto maskReader = TC::IO::TCMaskReader(maskPath);
            if (!maskReader.Exist()) continue;

            auto volumeReader = TC::IO::TCFSimpleReader(tcfPath);
            auto volumeData = volumeReader.ReadHT3D(0, false);

            const auto blobs = maskReader.GetBlobCount("HT", 0);
            for (int idx = 0; idx < blobs; ++idx) {
                const auto blob = maskReader.ReadBlob("HT", 0, idx);
                const auto codeValue = std::get<2>(blob);
                const auto typeCode = CellType::Convert(codeValue, CellType::TypeCode::Excluded);
                const auto celltype = CellType(typeCode);

                if (!celltype.IsRBC()) continue;


            }
        }

        return true;
    }

    auto CBCCalculator::CalculatePLT(ListType& list, Entity::CBCData::Pointer& data) -> bool {
        return true;
    }
}
