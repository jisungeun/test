#pragma once

#include <ICBCCalculator.h>
#include "CBCResearchCBCCalculatorExport.h"

namespace CBCResearch::Plugins {

    class CBCResearchCBCCalculator_API CBCCalculator : public UseCase::ICBCCalculator {
    public:
        explicit CBCCalculator();
        virtual ~CBCCalculator();

        auto Calculate(ListType& list) -> Entity::CBCData::Pointer override;

    private:
        auto CountCells(ListType& list, Entity::CBCData::Pointer& data)->bool;
        auto CalculateRBC(ListType& list, Entity::CBCData::Pointer& data)->bool;
        auto CalculatePLT(ListType& list, Entity::CBCData::Pointer& data)->bool;
    };
}
