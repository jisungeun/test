#include <QMap>
#include <QComboBox>
#include <QIcon>

#include "ui_TCFListPanel.h"
#include "TCFListPanel.h"

namespace CBCResearch::Plugins {
    struct TCFListPanel::Impl {
        Ui::TCFListPanel* ui{ nullptr };
        QMap<int, TC::StatusListWidget::Index> index2ItemMap;
        QMap<Status, QIcon> icons{
            {Status::Loaded, QIcon(":/icons/dot-lightgray-16-10.png")},
            {Status::Segmented, QIcon(":/icons/dot-dodgeblue-16-10.png")},
            {Status::Annotated, QIcon(":/icons/dot-limegreen-16-10.png")}
        };
    };

    TCFListPanel::TCFListPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::TCFListPanel();
        d->ui->setupUi(this);

        for(auto itr=d->icons.begin(); itr!=d->icons.end(); ++itr) {
            d->ui->listWidget->SetStatusIcon(static_cast<int>(itr.key()), itr.value());
        }

        connect(d->ui->listWidget, SIGNAL(itemClicked(int)), this, SLOT(onItemClicked(int)));
        connect(d->ui->filterCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onFilterChanged(int)));
    }

    TCFListPanel::~TCFListPanel() {
    }

    auto TCFListPanel::AddFilter(const QString& title, int value) -> void {
        d->ui->filterCombo->setDisabled(true);
        d->ui->filterCombo->addItem(title, value);
        d->ui->filterCombo->setDisabled(false);
    }

    auto TCFListPanel::AddItem(const int index, const QString& label, Status status) -> void {
        const auto itemIndex = d->ui->listWidget->AddItem(label, static_cast<int>(status));
        d->ui->listWidget->SetUserData(itemIndex, index);
        d->index2ItemMap[index] = itemIndex;
    }

    auto TCFListPanel::SetStatus(int index, Status status) -> void {
        if (!d->index2ItemMap.contains(index)) return;
        d->ui->listWidget->UpdateStatus(d->index2ItemMap[index], static_cast<int>(status));
    }

    auto TCFListPanel::BeginUpdate() -> void {
        d->ui->listWidget->BeginUpdate();
    }

    auto TCFListPanel::EndUpdate() -> void {
        d->ui->listWidget->EndUpdate();
    }

    auto TCFListPanel::ClearItems() -> void {
        d->ui->listWidget->ClearItems();
    }

    auto TCFListPanel::Selected() const -> int {
        return d->ui->listWidget->Selected();
    }

    auto TCFListPanel::AllItems() const -> QList<int> {
        return d->index2ItemMap.keys();
    }

    void TCFListPanel::onItemClicked(int listIndex) {
        const auto userIndex = d->ui->listWidget->GetUserData(listIndex).toInt();
        emit itemSelected(userIndex);
    }

    void TCFListPanel::onFilterChanged(int index) {
        const auto value = d->ui->filterCombo->currentData().toInt();
        emit filterChanged(value);
    }
}
