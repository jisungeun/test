#include <QBoxLayout>
#include <QFrame>
#include <QPushButton>

#include <TCFListPanel.h>

#include "mainwindow.h"

namespace TCFPanelListExample {
    struct MainWindow::Impl {
        CBCResearch::Plugins::TCFListPanel* panel{ nullptr };
    };

    MainWindow::MainWindow(QWidget* parent, Qt::WindowFlags flags) : QMainWindow(parent, flags), d{ new Impl } {
        setWindowTitle("TCFListPanel Example");
        setMinimumSize(QSize(300, 400));

        auto frame = new QFrame();
        {
            auto vbox = new QVBoxLayout();
            {
                d->panel = new CBCResearch::Plugins::TCFListPanel();
                vbox->addWidget(d->panel);

                auto hbox = new QHBoxLayout();
                {
                    auto* btn = new QPushButton("Segmented");
                    hbox->addWidget(btn);
                    connect(btn, SIGNAL(clicked()), this, SLOT(onSegmented()));

                    btn = new QPushButton("Annotated");
                    hbox->addWidget(btn);
                    connect(btn, SIGNAL(clicked()), this, SLOT(onAnnotated()));
                }
                vbox->addLayout(hbox);
            }
            frame->setLayout(vbox);
        }
        setCentralWidget(frame);

        CreateItems();
    }

    MainWindow::~MainWindow() {
    }

    auto MainWindow::CreateItems() -> void {
        d->panel->AddItem(100, "Test01");
        d->panel->AddItem(101, "Test02");
        d->panel->AddItem(102, "Test03");
        d->panel->AddItem(103, "Test04");
    }

    void MainWindow::onSegmented() {
        using namespace CBCResearch::Plugins;
        d->panel->SetStatus(100, TCFListPanel::Status::Segmented);
        d->panel->SetStatus(101, TCFListPanel::Status::Segmented);
    }

    void MainWindow::onAnnotated() {
        using namespace CBCResearch::Plugins;
        d->panel->SetStatus(101, TCFListPanel::Status::Annotated);
        d->panel->SetStatus(102, TCFListPanel::Status::Annotated);
    }
}
