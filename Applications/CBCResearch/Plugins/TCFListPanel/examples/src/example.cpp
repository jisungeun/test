#include <QApplication>
#include "mainwindow.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    app.setApplicationName("CBCViewer Example");
    app.setOrganizationName("Tomocube, Inc.");
    app.setOrganizationDomain("www.tomocube.com");

    TCFPanelListExample::MainWindow window;
    window.show();

    return app.exec();
}