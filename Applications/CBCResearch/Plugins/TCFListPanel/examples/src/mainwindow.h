#pragma once

#include <memory>
#include <QMainWindow>

namespace TCFPanelListExample {
    class MainWindow : public QMainWindow {
        Q_OBJECT

    public:
        MainWindow(QWidget* parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());
        virtual ~MainWindow();

    protected:
        auto CreateItems()->void;

    protected slots:
        void onSegmented();
        void onAnnotated();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}