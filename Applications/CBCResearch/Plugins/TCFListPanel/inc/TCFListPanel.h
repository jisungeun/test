#pragma once

#include <memory>
#include <StatusListWidget.h>
#include "CBCResearchTCFListPanelExport.h"

namespace CBCResearch::Plugins {
    class CBCResearchTCFListPanel_API TCFListPanel : public QWidget {
        Q_OBJECT
    public:
        enum class Status {
            Loaded,
            Segmented,
            Annotated
        };

    public:
        TCFListPanel(QWidget* parent=nullptr);
        virtual ~TCFListPanel();

        auto AddFilter(const QString& title, int value)->void;

        auto AddItem(int index, const QString& label, Status status = Status::Loaded)->void;
        auto SetStatus(int index, Status status)->void;

        auto BeginUpdate()->void;
        auto EndUpdate()->void;
        auto ClearItems()->void;

        auto Selected() const->int;
        auto AllItems() const->QList<int>;

    protected slots:
        void onItemClicked(int listIndex);
        void onFilterChanged(int index);

    signals:
        void itemSelected(int index);
        void filterChanged(int value);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}