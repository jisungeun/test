#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

#include "ui_CBCReportPanel.h"
#include "CBCReportPanel.h"

namespace CBCResearch::Plugins {
    struct CBCReportPanel::Impl {
        Ui::CBCReportPanel* ui{ nullptr };
        uint32_t itemCount{ 0 };
    };

    CBCReportPanel::CBCReportPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::CBCReportPanel();
        d->ui->setupUi(this);

        SetupTable();

        connect(d->ui->saveButton, SIGNAL(clicked()), this, SLOT(onSave()));
        connect(d->ui->calcButton, SIGNAL(clicked()), this, SIGNAL(calculate()));
    }

    CBCReportPanel::~CBCReportPanel() {
    }

    auto CBCReportPanel::Clear() -> void {
        d->ui->resultTable->clearContents();
        d->itemCount = 0;
    }

    auto CBCReportPanel::AddItem(const Item& item) -> void {
        auto table = d->ui->resultTable;

        const auto rowIndex = d->itemCount;
        d->itemCount = d->itemCount + 1;
        table->setRowCount(d->itemCount);

        auto tableItem = [&](int row, int col)->QTableWidgetItem* {
            auto item = table->item(row, col);
            if (!item) {
                item = new QTableWidgetItem();
                table->setItem(row, col, item);
            }
            return item;
        };

        tableItem(rowIndex, 0)->setText(item.title);
        tableItem(rowIndex, 1)->setText(QString::number(item.value, 'f', 1));
        tableItem(rowIndex, 2)->setText(item.unit);

        tableItem(rowIndex, 1)->setTextAlignment(Qt::AlignmentFlag::AlignRight | Qt::AlignmentFlag::AlignVCenter);
    }
    
    auto CBCReportPanel::SetupTable() -> void {
        auto table = d->ui->resultTable;
        table->setColumnCount(3);
        const QStringList headers(QString("Test;Result;Units").split(';'));
        table->setHorizontalHeaderLabels(headers);
        table->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    }

    void CBCReportPanel::onSave() {
        static QString prevPath;
        auto path = QFileDialog::getSaveFileName(this, "Save CBC Result", prevPath, "CSV (*.csv)");
        if (path.isEmpty()) {
            QMessageBox::warning(this, "Save CBC Result", "You should select a file to save the result");
            return;
        } else {
            prevPath = path;
        }

        QStringList contents;

        auto table = d->ui->resultTable;
        const auto columns = table->columnCount();
        const auto rows = table->rowCount();

        QString line;
        for (auto idx = 0; idx < columns; idx++) {
            line.push_back(table->horizontalHeaderItem(idx)->text());
            if(idx<(columns-1)) line.push_back(",");
        }
        contents.push_back(line);

        for (auto row = 0; row < rows; row++) {
            line.clear();
            for (auto idx = 0; idx < columns; idx++) {
                line.push_back(table->item(row, idx)->text());
                if (idx < (columns - 1)) line.push_back(",");
            }
            contents.push_back(line);
        }

        QFile file(path);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QMessageBox::warning(this, "Save CBC Result", "Failed to write the result");
            return;
        }

        QTextStream out(&file);
        for (const auto content : contents) {
            out << content << "\n";
        }
    }
}
