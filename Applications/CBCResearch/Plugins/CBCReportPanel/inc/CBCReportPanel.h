#pragma once

#include <memory>
#include <QWidget>
#include <QString>

#include "CBCResearchReportPanelExport.h"

namespace CBCResearch::Plugins {
    class CBCResearchReportPanel_API CBCReportPanel : public QWidget {
        Q_OBJECT

    public:
        struct Item {
            QString title;
            double value;
            QString unit;
        };

    public:
        CBCReportPanel(QWidget* parent = nullptr);
        virtual ~CBCReportPanel();

        auto Clear()->void;
        auto AddItem(const Item& item)->void;

    protected:
        auto SetupTable()->void;

    protected slots:
        void onSave();

    signals:
        void calculate();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}