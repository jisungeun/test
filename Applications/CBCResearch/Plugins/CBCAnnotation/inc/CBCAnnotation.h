#pragma once

#include <memory>
#include <QString>
#include <QColor>
#include <CellType.h>
#include "CBCResearchCBCAnnotationExport.h"

namespace CBCResearch::Plugins {

    class CBCResearchCBCAnnotation_API CBCAnnotation {
    public:
        typedef Entity::CellType CellType;
        typedef CellType::TypeCode TypeCode;

    public:
        explicit CBCAnnotation(const Entity::CellType type);
        virtual ~CBCAnnotation();

        auto ShortName() const->QString;
        auto FullName() const->QString;
        auto Description() const->QString;
        auto Color() const->QColor;

        static auto TypeCount()->uint32_t;
        static auto TypeByIndex(uint32_t index)->TypeCode;
        static auto ShortName(TypeCode typeCode)->QString;
        static auto FullName(TypeCode typeCode)->QString;
        static auto Description(TypeCode typeCode)->QString;
        static auto Color(TypeCode typeCode)->QColor;
        static auto Convert(int typeValue, TypeCode defaultTypeCode)->TypeCode;
        static auto Convert(TypeCode typeCode)->int;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}