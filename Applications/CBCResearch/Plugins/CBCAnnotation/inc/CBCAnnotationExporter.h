#pragma once

#include <memory>
#include <IAnnotationExporter.h>
#include "CBCResearchCBCAnnotationExport.h"

namespace CBCResearch::Plugins {
    class CBCResearchCBCAnnotation_API CBCAnnotationExporter : public UseCase::IAnnotationExporter {
    public:
        CBCAnnotationExporter();
        virtual ~CBCAnnotationExporter();
        
        auto Export(const QStringList& pahtList)->bool override;
    };
}