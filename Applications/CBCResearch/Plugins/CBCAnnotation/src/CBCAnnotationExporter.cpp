#include <QFileInfo>
#include <QTextStream>

#include <IMaskReader.h>
#include <CellType.h>

#include "CBCAnnotationExporter.h"

namespace CBCResearch::Plugins {
    Plugins::CBCAnnotationExporter::CBCAnnotationExporter() {
    }
    
    CBCAnnotationExporter::~CBCAnnotationExporter() {
    }
    
    auto CBCAnnotationExporter::Export(const QStringList& pathList)->bool {
        auto outPath = GetPath();
        auto reader = GetMaskReader();
        auto wbcOnly = IsWBCOnly();

        if (!reader) return false;

        QStringList outLines;

        for (const auto path : pathList) {
            auto data = reader->Read(path, false);
            if (data.get() == nullptr) continue;

            const auto blobs = data->CountBlobs();
            for (auto idx = 0; idx < blobs; ++idx) {
                const auto code = data->GetBlob(idx).GetCode();
                const auto bbox = data->GetBlob(idx).GetBoundingBox();
                const auto isWBC = Entity::CellType(code).IsWBC();

                if (wbcOnly && !isWBC) continue;

                //name,x0,y0,z0,x1,y1,z1,code
                outLines.push_back(QString("%1,%2,%3,%4,%5,%6,%7,%8")
                                   .arg(QFileInfo(path).completeBaseName())
                                   .arg(std::get<0>(bbox))
                                   .arg(std::get<1>(bbox))
                                   .arg(std::get<2>(bbox))
                                   .arg(std::get<3>(bbox))
                                   .arg(std::get<4>(bbox))
                                   .arg(std::get<5>(bbox))
                                   .arg(Entity::CellType(code).toString()));
            }
        }

        QFile file(outPath);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return false;

        QTextStream out(&file);
        out << "File,X0,Y0,Z0,X1,Y1,Z1,Type\n";

        for (auto line : outLines) {
            out << line << "\n";
        }

        return true;
    }
}