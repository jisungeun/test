#include <QMap>
#include "CBCAnnotation.h"

namespace CBCResearch::Plugins {
    struct TypeProp {
        QString shortName;
        QString fullName;
        QString description;
        QColor color;
    };

    static const QMap<CBCAnnotation::CellType::TypeCode, TypeProp> types{
            {CBCAnnotation::TypeCode::RBC, {"RBC", "RBC", "Red Blood Cell", 0xFFC0392B}},
            {CBCAnnotation::TypeCode::PLT, {"Platelet", "Platelet", "Platelet", Qt::yellow}},
            {CBCAnnotation::TypeCode::SegmentedNeutrophil, {"S.Neutorphil", "Segmented Neutrophil", "Segmented Neutrophil", 0xFF9B59B6}},
            {CBCAnnotation::TypeCode::BandNeutrophil, {"B.Neutrophil", "Band Neutrophil", "Band Neutrophil", 0xFF7D3C98}},
            {CBCAnnotation::TypeCode::Lymphocyte, {"Lympocyte", "Lympocyte", "Lympocyte", 0xFF3498DB}},
            {CBCAnnotation::TypeCode::Monocyte, {"Monocyte", "Monocyte", "Monocyte", 0xFF1ABC9C}},
            {CBCAnnotation::TypeCode::Eosinophil, {"Esoinophil", "Eosinophil", "Eosinophil", 0xFFF39C12}},
            {CBCAnnotation::TypeCode::Basophil, {"Basophil", "Basophil", "Bashophil", 0xFFD35400}},
            {CBCAnnotation::TypeCode::UnknownCell, {"Unknown", "Unknown", "Unknown Cell Type", 0xFFFADBD8}},
            {CBCAnnotation::TypeCode::Excluded, {"Exclude", "Exclude", "Exclude", 0xFFD0ECE7}}
    };

    struct CBCAnnotation::Impl {
        CellType type;
    };

    CBCAnnotation::CBCAnnotation(const CellType type) : d{ new Impl} {
        d->type = type;
    }

    CBCAnnotation::~CBCAnnotation() {
    }

    auto CBCAnnotation::ShortName() const -> QString {
        return types[d->type.GetType()].shortName;
    }

    auto CBCAnnotation::FullName() const -> QString {
        return types[d->type.GetType()].fullName;
    }

    auto CBCAnnotation::Description() const -> QString {
        return types[d->type.GetType()].description;
    }

    auto CBCAnnotation::Color() const -> QColor {
        return types[d->type.GetType()].color;
    }

    auto CBCAnnotation::TypeCount() -> uint32_t {
        return types.size();
    }

    auto CBCAnnotation::TypeByIndex(uint32_t index) -> TypeCode {
        return CellType::TypeByIndex(index);
    }

    auto CBCAnnotation::ShortName(TypeCode typeCode) -> QString {
        return types[typeCode].shortName;
    }

    auto CBCAnnotation::FullName(TypeCode typeCode) -> QString {
        return types[typeCode].fullName;
    }

    auto CBCAnnotation::Description(TypeCode typeCode) -> QString {
        return types[typeCode].description;
    }

    auto CBCAnnotation::Color(TypeCode typeCode) -> QColor {
        return types[typeCode].color;
    }

    auto CBCAnnotation::Convert(int typeValue, TypeCode defaultTypeCode) -> TypeCode {
        return CellType::Convert(typeValue, defaultTypeCode);
    }

    auto CBCAnnotation::Convert(TypeCode typeCode) -> int {
        return CellType::Convert(typeCode);
    }
}
