#pragma once

#include <memory>
#include <ISegment.h>
#include "CBCResearchCellSegmentExport.h"

namespace CBCResearch::Plugins {
    class CBCResearchCellSegment_API SegmentCells : public UseCase::ISegment {
    public:
        SegmentCells(uint16_t offset = 0);
        virtual ~SegmentCells();

        auto Run() -> Entity::MaskData::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}