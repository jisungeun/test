#include <TCFSimpleReader.h>
#include <TCLabelMap.h>
#include <Resize3D.h>
#include <TCOtsuThreshold.h>

#include "SegmentCells.h"

namespace CBCResearch::Plugins {
    struct SegmentCells::Impl {
        QString modelPath;
        uint16_t offset{ 0 };
    };

    SegmentCells::SegmentCells(uint16_t offset) : d{ new Impl } {
        d->offset = offset;
    }

    SegmentCells::~SegmentCells() {
    }

    auto SegmentCells::Run() -> Entity::MaskData::Pointer {
        const auto source = GetSource();
        const auto reader = TC::IO::TCFSimpleReader(source);
        auto volumeData = reader.ReadHT3D(0);
        if (!volumeData.IsValid()) return nullptr;

        auto dims = volumeData.GetDimension();
        uint32_t volumeSize[3];
        volumeSize[0] = std::get<0>(dims);
        volumeSize[1] = std::get<1>(dims);
        volumeSize[2] = std::get<2>(dims);

        auto outputBuf = TC::OtsuThreshold(volumeData.GetDataAsUInt16().get(), volumeSize, d->offset);

        auto labelMap = TC::LabelMap();
        labelMap.SetCountLimit(true, 100);
        labelMap.SetRemoveInclusion(true, TC::LabelMap::InclusionMode::LateralInclusion);
        labelMap.SetInput(outputBuf.get(), volumeSize);

        Entity::MaskData::Pointer maskData{ new Entity::MaskData() };

        const auto blobs = labelMap.GetCount();
        for (uint32_t idx = 0; idx < blobs; ++idx) {
            const auto index = labelMap.GetIndex(idx);
            const auto volume = labelMap.GetMaskVolume(idx);
            const auto blob = labelMap.GetBoundingBox(idx);
            const auto offset = blob.GetOffset();
            const auto size = blob.GetSize();

            auto maskblob = Entity::MaskBlob();
            maskblob.SetBoundingBox(offset.x0, offset.y0, offset.z0, 
                                    offset.x0+size.d0-1, offset.y0+size.d1-1, offset.z0+size.d2-1);
            maskblob.SetCode(GetDefaultCode());
            maskblob.SetMaskVolume(volume);
            maskData->AppendBlob(index, maskblob);
        }

        return maskData;
    }
}