project(CBCResearchCellSegment)

#Header files for external use
set(INTERFACE_HEADERS
    inc/SegmentCells.h
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES
    src/SegmentCells.cpp
)

add_library(${PROJECT_NAME} SHARED
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

add_library(CBCResearch::Plugins::CellSegment ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<INSTALL_INTERFACE:inc>
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_libraries(${PROJECT_NAME}
	PUBLIC
		Extern::BetterEnums
        Qt5::Core
    PRIVATE
        CBCResearch::BusinessRule::UseCase
        TC::Components::OtsuThreshold
        TC::Components::TCFIO
        TC::Components::TCLabelMap
        TC::Components::ImageProcessing
)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PUBLIC
		/wd4251
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/CBCResearch/Plugins")

generate_export_header(${PROJECT_NAME}
    EXPORT_MACRO_NAME ${PROJECT_NAME}_API
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

# 'make install' to the correct locations
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR}
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR})
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

# This makes the project importable from the install directory
# Put config file in per-project dir (name MUST match), can also
# just go into 'cmake'.
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake)

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)

add_subdirectory(test)