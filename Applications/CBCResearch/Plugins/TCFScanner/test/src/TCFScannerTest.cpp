#include <catch2/catch.hpp>

#include <ScanDirectory.h>

using namespace CBCResearch;

namespace _Test {
    class DirectoryScanner : public UseCase::IDirectoryScanner {
    public:
        DirectoryScanner(const QStringList& dirs) : dirs{ dirs } {
        }

        auto Scan(const QString& topDir) const->QStringList {
            return dirs;
        }

    private:
        QStringList dirs;
    };

    class ScanDirectoryPort : public UseCase::IScanDirectoryPort {
    public:
        ScanDirectoryPort() {
        }

        auto Update(const UseCase::SourceList::Pointer& list)->void {
            sources = list;
        }

        auto GetDirectories(void) const->UseCase::SourceList::Pointer {
            return sources;
        }

    private:
        UseCase::SourceList::Pointer sources;
    };
    
}

TEST_CASE("ScanDirectory", "ScanDirectory") {
    namespace test = _Test;
    
    SECTION("Request to scan") {
        QStringList dirs{
            "D:/Temp/X1.tcf",
            "D:/Temp/X2.tcf",
            "D:/Temp/X3.tcf"
        };

        test::DirectoryScanner scanner(dirs);
        test::ScanDirectoryPort port;

        UseCase::ScanDirectory usecase(&scanner, &port);
        auto res = usecase.Request("D:/Temp");
        REQUIRE(res == true);

        auto sources = port.GetDirectories();
        REQUIRE(dirs.length() == sources->GetCount());

        for(int idx=0; idx<sources->GetCount(); ++idx) {
            auto source = sources->GetSource(idx);

            REQUIRE(dirs.contains(source.path));
        }
    }
}