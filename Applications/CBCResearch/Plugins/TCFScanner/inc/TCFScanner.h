#pragma once

#include <QString>

#include <IDirectoryScanner.h>
#include "CBCResearchTCFScannerExport.h"

namespace CBCResearch::Plugins {
    class CBCResearchTCFScanner_API TCFScanner : public UseCase::IDirectoryScanner {
    public:
        TCFScanner();
        virtual ~TCFScanner();

        auto Scan(const QString& topDir) const->QStringList override;
    };
}