#include <FileUtility.h>
#include "TCFScanner.h"

namespace CBCResearch::Plugins {
    TCFScanner::TCFScanner() {
    }

    TCFScanner::~TCFScanner() {
    }

    auto TCFScanner::Scan(const QString& topDir) const -> QStringList {
        return TC::FindFiles(topDir, "*.tcf");
    }
}