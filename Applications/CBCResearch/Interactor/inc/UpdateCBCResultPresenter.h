#pragma once

#include <memory>
#include <IUpdateCBCResultPort.h>
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class ICBCResultWidget;

    class CBCResearchInteractor_API UpdateCBCResultPresenter : public UseCase::IUpdateCBCResultPort {
    public:
        UpdateCBCResultPresenter(ICBCResultWidget* widget = nullptr);
        virtual ~UpdateCBCResultPresenter();

        auto Update(Entity::CBCData::Pointer data) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}