#pragma once

#include <memory>

#include <IVolumeDataPort.h>
#include "IVolumeDataWidget.h"

#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API VolumeDataPresenter : public UseCase::IVolumeDataPort {
    public:
        VolumeDataPresenter(IVolumeDataWidget* widget=nullptr);
        virtual ~VolumeDataPresenter();

        auto Update(const QString& dataPath) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}