#pragma once

#include <memory>
#include <IMaskReader.h>
#include "MaskDataPresenter.h"
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API LoadMaskData {
    public:
        LoadMaskData(UseCase::IMaskReader* reader, MaskDataPresenter* presenter = nullptr);
        virtual ~LoadMaskData();

        auto Request(const int index, bool loadMaskVolume = false)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}