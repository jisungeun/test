#pragma once

#include <memory>
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::UseCase {
    class IMaskReader;
    class IAnnotationExporter;
}

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API ExportAnnotation {
    public:
        ExportAnnotation(UseCase::IMaskReader* reader, UseCase::IAnnotationExporter* exporter);
        virtual ~ExportAnnotation();

        auto Request(const QString& path, bool exportWBCOnly = false)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}