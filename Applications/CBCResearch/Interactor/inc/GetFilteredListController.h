#pragma once

#include <memory>

#include <CellType.h>

#include "TCFListPresenter.h"
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API GetFilteredList {
    public:
        using TypeCode = Entity::CellType::TypeCode;

    public:
        GetFilteredList(TCFListPresenter* presenter = nullptr);
        virtual ~GetFilteredList();

        auto Request(bool applyFilter, TypeCode type = TypeCode::RBC)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}