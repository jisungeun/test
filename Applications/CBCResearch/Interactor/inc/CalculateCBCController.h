#pragma once

#include <memory>
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::UseCase {
    class ICBCCalculator;
    class IUpdateCBCResultPort;
}

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API CalcaulateCBCController {
    public:
        CalcaulateCBCController(UseCase::ICBCCalculator* calculator, UseCase::IUpdateCBCResultPort* port = nullptr);
        virtual ~CalcaulateCBCController();

        auto Request()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}