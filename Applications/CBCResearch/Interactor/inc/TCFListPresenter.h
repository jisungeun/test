#pragma once

#include <memory>
#include <IScanDirectoryPort.h>

#include "ITCFListWidget.h"
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API TCFListPresenter : public UseCase::IScanDirectoryPort {
    public:
        TCFListPresenter(ITCFListWidget* widget=nullptr);
        virtual ~TCFListPresenter();

        auto Update(const UseCase::SourceList::Pointer& list) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}