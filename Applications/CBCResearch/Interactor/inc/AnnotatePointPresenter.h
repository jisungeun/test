#pragma once

#include <memory>
#include <IAnnotatePointPort.h>
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class IMaskDataWidget;

    class CBCResearchInteractor_API AnnotatePointPresenter : public UseCase::IAnnotatePointPort {
    public:
        AnnotatePointPresenter(IMaskDataWidget* widget = nullptr);
        virtual ~AnnotatePointPresenter();

        auto Update(const int index, Entity::MaskData::Pointer& mask)->void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}