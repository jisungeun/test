#pragma

#include <QString>
#include <MaskData.h>
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API IMaskDataWidget {
    public:
        IMaskDataWidget();
        virtual ~IMaskDataWidget();

        virtual auto SetItem(int index, Entity::MaskData::Pointer& mask)->void = 0;
        virtual auto SetAnnotated(int index, Entity::MaskData::Pointer& mask)->void = 0;
    };
}