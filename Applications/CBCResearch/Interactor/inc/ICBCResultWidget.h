#pragma once

#include <QList>
#include <QString>
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API ICBCResultWidget {
    public:
        struct Item {
            QString title;
            double value;
            QString unit;
        };

        typedef QList<Item> List;

    public:
        ICBCResultWidget();
        virtual ~ICBCResultWidget();

        virtual auto Update(List& list)->void = 0;
    };
}