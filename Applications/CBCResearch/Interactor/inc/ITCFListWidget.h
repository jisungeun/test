#pragma once

#include <QString>
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API ITCFListWidget {
    public:
        enum class Status {
            Loaded,
            Segmented,
            Annotated
        };

    public:
        ITCFListWidget();
        virtual ~ITCFListWidget();

        virtual auto BeginUpdate()->void = 0;
        virtual auto EndUpdate()->void = 0;
        virtual auto Clear()->void = 0;
        virtual auto AddItem(int index, const QString& path, Status status = Status::Loaded)->void = 0;
    };
}