#pragma once

#include <memory>
#include <ISegmentedCellsPort.h>
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class IMaskDataWidget;

    class CBCResearchInteractor_API MaskDataPresenter : public UseCase::ISegmentedCellsPort {
    public:
        MaskDataPresenter(IMaskDataWidget* widget=nullptr);
        virtual ~MaskDataPresenter();

        auto Update(const int index, Entity::MaskData::Pointer& mask)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}