#pragma once

#include <ISegment.h>
#include <IMaskWriter.h>

#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class MaskDataPresenter;

    class CBCResearchInteractor_API SegmentCellsController {
    public:
        SegmentCellsController(UseCase::ISegment* segment, UseCase::IMaskWriter* writer, MaskDataPresenter* presenter = nullptr);
        virtual ~SegmentCellsController();

        auto Request(const int index)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}