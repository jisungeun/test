#pragma once

#include <memory>
#include <AnnotatePoint.h>
#include <IMaskWriter.h>
#include "AnnotatePointPresenter.h"
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API AnnotatePointController {
    public:
        AnnotatePointController(UseCase::IMaskWriter* writer, AnnotatePointPresenter* port = nullptr);
        virtual ~AnnotatePointController();

        auto Request(int index, int code)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}