#pragma once

#include <QString>
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API IVolumeDataWidget {
    public:
        IVolumeDataWidget();
        virtual ~IVolumeDataWidget();

        virtual auto Update(const QString& dataPath)->void = 0;
    };
}