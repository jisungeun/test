#pragma once

#include <memory>
#include "VolumeDataPresenter.h"
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API LoadVolumeData {
    public:
        LoadVolumeData(VolumeDataPresenter* presenter = nullptr);
        virtual ~LoadVolumeData();

        auto Request(const int index)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}