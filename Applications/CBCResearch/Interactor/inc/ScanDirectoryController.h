#pragma once

#include <memory>
#include <QString>

#include <IDirectoryScanner.h>
#include <IMaskReader.h>

#include "TCFListPresenter.h"
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API ScanDirectory {
    public:
        ScanDirectory(UseCase::IDirectoryScanner* scanner, TCFListPresenter* presenter = nullptr, UseCase::IMaskReader* reader = nullptr);
        virtual ~ScanDirectory();

        auto Request(const QString& sourceDirectory)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}