#pragma once

#include <QString>
#include "CBCResearchInteractorExport.h"

namespace CBCResearch::Interactor {
    class CBCResearchInteractor_API CreateWorkset {
    public:
        CreateWorkset();
        virtual ~CreateWorkset();

        auto Request(const QString& destDirectory)->void;
    };
}