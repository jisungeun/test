#include <CreateWorkset.h>
#include "CreateWorksetController.h"

namespace CBCResearch::Interactor {
    CreateWorkset::CreateWorkset() {
    }

    CreateWorkset::~CreateWorkset() {
    }

    auto CreateWorkset::Request(const QString& destDirectory) -> void {
        auto usecase = UseCase::CreateWorkset();
        usecase.Request(destDirectory);
    }

}