#include <GetFilteredList.h>
#include "GetFilteredListController.h"

namespace CBCResearch::Interactor {
    struct GetFilteredList::Impl {
        TCFListPresenter* presenter{ nullptr };
    };

    GetFilteredList::GetFilteredList(TCFListPresenter* presenter) : d{ new Impl } {
        d->presenter = presenter;
    }

    GetFilteredList::~GetFilteredList() {
        
    }

    auto GetFilteredList::Request(const bool applyFilter, const TypeCode type) -> bool {
        auto usecase = UseCase::GetFilteredList(d->presenter);
        return usecase.Request(applyFilter, type);
    }



}