#include <ICBCCalculator.h>
#include <IUpdateCBCResultPort.h>
#include <CalculateCBC.h>

#include "CalculateCBCController.h"

namespace CBCResearch::Interactor {
    struct CalcaulateCBCController::Impl{
        UseCase::ICBCCalculator* calculator{ nullptr };
        UseCase::IUpdateCBCResultPort* port{ nullptr };
    };

    CalcaulateCBCController::CalcaulateCBCController(UseCase::ICBCCalculator* calculator, UseCase::IUpdateCBCResultPort* port)
        : d{ new Impl } {
        d->calculator = calculator;
        d->port = port;
    }

    CalcaulateCBCController::~CalcaulateCBCController() {
    }

    auto CalcaulateCBCController::Request() -> bool {
        auto usecase = UseCase::CalculateCBC(d->calculator, d->port);
        return usecase.Request();
    }
}