#include "MakeMaskData.h"
#include "MaskDataPresenter.h"
#include "SegmentCellsController.h"

namespace CBCResearch::Interactor {
    struct SegmentCellsController::Impl {
        UseCase::ISegment* segment{ nullptr };
        UseCase::IMaskWriter* writer{ nullptr };
        MaskDataPresenter* presenter{ nullptr };
    };

    SegmentCellsController::SegmentCellsController(UseCase::ISegment* segment, UseCase::IMaskWriter* writer, MaskDataPresenter* presenter) : d{ new Impl } {
        d->segment = segment;
        d->writer = writer;
        d->presenter = presenter;
    }

    SegmentCellsController::~SegmentCellsController() {
    }

    auto SegmentCellsController::Request(const int index) -> bool {
        if (!d->segment) return false;

        auto usecase = UseCase::MakeMaskData(d->segment, d->writer, d->presenter);
        return usecase.Request(index);
    }
}
