#include <IMaskReader.h>
#include <IAnnotationExporter.h>
#include <ExportAnnotation.h>
#include "ExportAnnotationController.h"

namespace CBCResearch::Interactor {
    struct ExportAnnotation::Impl {
        UseCase::IMaskReader* reader{ nullptr };
        UseCase::IAnnotationExporter* exporter{ nullptr };
    };

    ExportAnnotation::ExportAnnotation(UseCase::IMaskReader* reader, UseCase::IAnnotationExporter* exporter) : d{ new Impl } {
        d->reader = reader;
        d->exporter = exporter;
    }

    ExportAnnotation::~ExportAnnotation() {
    }

    auto ExportAnnotation::Request(const QString& path, bool exportWBCOnly) -> bool {
        auto usecase = UseCase::ExportAnnotation(d->reader, d->exporter);
        return usecase.Request(path, exportWBCOnly);
    }
}
