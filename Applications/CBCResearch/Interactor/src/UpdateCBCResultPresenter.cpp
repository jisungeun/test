#include <QMap>

#include "ICBCResultWidget.h"
#include "UpdateCBCResultPresenter.h"

namespace CBCResearch::Interactor {
    struct UpdateCBCResultPresenter::Impl {
        ICBCResultWidget* widget{ nullptr };

        struct FieldInfo {
            QString name;
            QString unit;
        };

        using CBCData = Entity::CBCData;
        using CellTypeCode = Entity::CellType::TypeCode;

        QMap<CBCData::CountEntry, FieldInfo> countFields{
            {CBCData::CountEntry::rbcCount, {"RBC Count", "ea"}},
            {CBCData::CountEntry::wbcCount, {"WBC Count", "ea"}},
            {CBCData::CountEntry::pltCount, {"PLT Count", "ea"}}
        };

        QMap<CBCData::RBCEntry, FieldInfo> rbcFields{
            {CBCData::RBCEntry::Hb,   {"Hb",   "g/dL"}},
            {CBCData::RBCEntry::Hct,  {"Hct",  "%"}},
            {CBCData::RBCEntry::MCV,  {"MCV",  "fL"}},
            {CBCData::RBCEntry::MCH,  {"MCH",  "pg"}},
            {CBCData::RBCEntry::MCHC, {"MCHC", "%"}},
            {CBCData::RBCEntry::RDW,  {"RDW",  "%"}},
            {CBCData::RBCEntry::HDW,  {"HDW",  "g/dL"}}
        };

        QMap<CBCData::PlateletEntry, FieldInfo> pltFields{
            {CBCData::PlateletEntry::PCT, {"PCT", "%"}},
            {CBCData::PlateletEntry::MPV, {"MPV", "fL"}},
            {CBCData::PlateletEntry::PDW, {"RDW", "fL"}}
        };

        QMap< CellTypeCode, FieldInfo> wbcFields{
            {CellTypeCode::SegmentedNeutrophil, {"S. Neutrophil", "%"}},
            {CellTypeCode::BandNeutrophil,      {"B. Neutrophil", "%"}},
            {CellTypeCode::Lymphocyte,          {"Lymphocyte",    "%"}},
            {CellTypeCode::Monocyte,            {"Monocyte",      "%"}},
            {CellTypeCode::Eosinophil,          {"Eosinophil",    "%"}},
            {CellTypeCode::Basophil,            {"Basophil",      "%"}}
        };
    };

    UpdateCBCResultPresenter::UpdateCBCResultPresenter(ICBCResultWidget* widget) : d{ new Impl } {
        d->widget = widget;
    }

    UpdateCBCResultPresenter::~UpdateCBCResultPresenter() {
    }

    auto UpdateCBCResultPresenter::Update(Entity::CBCData::Pointer data) -> void {
        if (!d->widget) return;

        using CBCData = Entity::CBCData;

        auto item = [](const QString& title, double value, const QString& unit)->ICBCResultWidget::Item {
            ICBCResultWidget::Item item;
            item.title = title;
            item.value = value;
            item.unit = unit;
            return item;
        };

        auto list = ICBCResultWidget::List();

        for (auto itr = d->countFields.begin(); itr != d->countFields.end(); ++itr) {
            const auto value = data->GetCellCount(itr.key());
            list.append(item(itr->name, value, itr->unit));
        }

        for (auto itr = d->rbcFields.begin(); itr != d->rbcFields.end(); ++itr) {
            const auto value = data->GetRBCParameter(itr.key());
            list.append(item(itr->name, value, itr->unit));
        }

        for (auto itr = d->pltFields.begin(); itr != d->pltFields.end(); ++itr) {
            const auto value = data->GetPlateletParameter(itr.key());
            list.append(item(itr->name, value, itr->unit));
        }

        for (auto itr = d->wbcFields.begin(); itr != d->wbcFields.end(); ++itr) {
            const auto value = data->GetWBCParameter(itr.key());
            list.append(item(itr->name, value, itr->unit));
        }

        d->widget->Update(list);
    }
}