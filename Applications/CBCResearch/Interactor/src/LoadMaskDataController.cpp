#include "LoadMaskData.h"
#include "LoadMaskDataController.h"

namespace CBCResearch::Interactor {
    struct LoadMaskData::Impl {
        MaskDataPresenter* presenter{ nullptr };
        UseCase::IMaskReader* reader{ nullptr };
    };

    LoadMaskData::LoadMaskData(UseCase::IMaskReader* reader, MaskDataPresenter* presenter) : d{ new Impl } {
        d->presenter = presenter;
        d->reader = reader;
    }

    LoadMaskData::~LoadMaskData() {
    }

    auto LoadMaskData::Request(const int index, bool loadMaskVolume) -> bool {
        auto usecase = UseCase::LoadMaskData(d->reader, d->presenter);
        return usecase.Request(index, loadMaskVolume);
    }
}
