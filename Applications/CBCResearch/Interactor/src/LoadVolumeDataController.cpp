#include "LoadVolumeData.h"
#include "LoadVolumeDataController.h"

namespace CBCResearch::Interactor {
    struct LoadVolumeData::Impl {
        VolumeDataPresenter* presenter{ nullptr };
    };

    LoadVolumeData::LoadVolumeData(VolumeDataPresenter* presenter) : d{ new Impl } {
        d->presenter = presenter;
    }

    LoadVolumeData::~LoadVolumeData() {
    }

    auto LoadVolumeData::Request(const int index) -> bool {
        auto usecase = UseCase::LoadVolumeData(d->presenter);
        return usecase.Request(index);
    }
}
