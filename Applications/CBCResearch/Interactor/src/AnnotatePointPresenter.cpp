#include "IMaskDataWidget.h"
#include "AnnotatePointPresenter.h"

namespace CBCResearch::Interactor {
    struct AnnotatePointPresenter::Impl {
        IMaskDataWidget* widget{ nullptr };
    };

    AnnotatePointPresenter::AnnotatePointPresenter(IMaskDataWidget* widget) : d{ new Impl } {
        d->widget = widget;
    }

    AnnotatePointPresenter::~AnnotatePointPresenter() {
    }

    auto AnnotatePointPresenter::Update(const int index, Entity::MaskData::Pointer& mask) -> void {
        if (d->widget) {
            d->widget->SetAnnotated(index, mask);
        }
    }
}