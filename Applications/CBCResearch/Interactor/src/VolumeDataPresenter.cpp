#include "VolumeDataPresenter.h"

namespace CBCResearch::Interactor {
    struct VolumeDataPresenter::Impl {
        IVolumeDataWidget* widget{ nullptr };
    };

    VolumeDataPresenter::VolumeDataPresenter(IVolumeDataWidget* widget) : d{ new Impl } {
        d->widget = widget;
    }

    VolumeDataPresenter::~VolumeDataPresenter() {
    }

    auto VolumeDataPresenter::Update(const QString& dataPath) -> void {
        if (d->widget) d->widget->Update(dataPath);
    }
}
