#include "ITCFListWidget.h"
#include "TCFListPresenter.h"

namespace CBCResearch::Interactor {
    struct TCFListPresenter::Impl {
        ITCFListWidget* widget{ nullptr };
    };

    TCFListPresenter::TCFListPresenter(ITCFListWidget* widget) : d{ new Impl } {
        d->widget = widget;
    }

    TCFListPresenter::~TCFListPresenter() {
    }

    auto TCFListPresenter::Update(const UseCase::SourceList::Pointer& list) -> void {
        if (!d->widget) return;

        auto convert = [](UseCase::SourceList::Status status)->ITCFListWidget::Status {
            using SourceStatus = UseCase::SourceList::Status;
            using TargetStatus = ITCFListWidget::Status;

            auto retStatus = TargetStatus::Loaded;

            switch (status) {
            case SourceStatus::Loaded:
                retStatus = TargetStatus::Loaded;
                break;
            case SourceStatus::Segmented:
                retStatus = TargetStatus::Segmented;
                break;
            case SourceStatus::Annotated:
                retStatus = TargetStatus::Annotated;
                break;
            }

            return retStatus;
        };

        d->widget->BeginUpdate();

        d->widget->Clear();

        const auto count = list->GetCount();
        for(int idx=0; idx<count; idx++) {
            auto item = list->GetSource(idx);

            d->widget->AddItem(item.index, item.path, convert(item.status));
        }

        d->widget->EndUpdate();
    }
}
