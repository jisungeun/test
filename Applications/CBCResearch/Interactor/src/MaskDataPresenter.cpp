#include "IMaskDataWidget.h"
#include "MaskDataPresenter.h"

namespace CBCResearch::Interactor {
    struct MaskDataPresenter::Impl {
        IMaskDataWidget* widget{ nullptr };
    };

    MaskDataPresenter::MaskDataPresenter(IMaskDataWidget* widget) : d{ new Impl } {
        d->widget = widget;
    }

    MaskDataPresenter::~MaskDataPresenter() {
    }

    auto MaskDataPresenter::Update(const int index, Entity::MaskData::Pointer& mask) -> void {
        if(d->widget) {
            d->widget->SetItem(index, mask);
        }
    }
}