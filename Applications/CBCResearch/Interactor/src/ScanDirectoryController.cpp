#include "ScanDirectory.h"
#include "ScanDirectoryController.h"

namespace CBCResearch::Interactor {
    struct ScanDirectory::Impl {
        TCFListPresenter* presenter{ nullptr };
        UseCase::IDirectoryScanner* scanner{ nullptr };
        UseCase::IMaskReader* reader{ nullptr };
    };

    ScanDirectory::ScanDirectory(UseCase::IDirectoryScanner* scanner, TCFListPresenter* presenter, UseCase::IMaskReader* reader) : d{ new Impl } {
        d->scanner = scanner;
        d->presenter = presenter;
        d->reader = reader;
    }

    ScanDirectory::~ScanDirectory() {
    }

    auto ScanDirectory::Request(const QString& sourceDirectory) -> bool {
        auto usecase = UseCase::ScanDirectory(d->scanner, d->presenter, d->reader);
        return usecase.Request(sourceDirectory);
    }
}