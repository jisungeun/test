#include "AnnotatePointController.h"

namespace CBCResearch::Interactor {
    struct AnnotatePointController::Impl {
        UseCase::IMaskWriter* writer{ nullptr };
        AnnotatePointPresenter* port{ nullptr };
    };

    AnnotatePointController::AnnotatePointController(UseCase::IMaskWriter* writer, AnnotatePointPresenter* port) : d{ new Impl } {
        d->writer = writer;
        d->port = port;
    }

    AnnotatePointController::~AnnotatePointController() {
    }

    auto AnnotatePointController::Request(int index, int code) -> bool {
        auto usecase = UseCase::AnnotatePoint(d->writer, d->port);
        return usecase.Request(index, code);
    }
}