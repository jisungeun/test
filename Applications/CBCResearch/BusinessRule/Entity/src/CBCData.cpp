#include <QMap>
#include "CBCData.h"

namespace CBCResearch::Entity {

    struct CBCData::Impl {
        QMap<CountEntry, uint32_t> count{
            {CountEntry::rbcCount, 0},
            {CountEntry::wbcCount, 0},
            {CountEntry::pltCount, 0}
        };

        QMap<RBCEntry, double> rbcParams{
            {RBCEntry::Hb, 0},
            {RBCEntry::Hct, 0},
            {RBCEntry::MCV, 0},
            {RBCEntry::MCH, 0},
            {RBCEntry::MCHC, 0},
            {RBCEntry::RDW, 0},
            {RBCEntry::HDW, 0}
        };

        QMap<PlateletEntry, double> pltParams{
            {PlateletEntry::PCT, 0},
            {PlateletEntry::MPV, 0},
            {PlateletEntry::PDW, 0}
        };

        QMap<CellType::TypeCode, double> wbcParams{
            {CellType::TypeCode::SegmentedNeutrophil, 0},
            {CellType::TypeCode::BandNeutrophil, 0},
            {CellType::TypeCode::Lymphocyte, 0},
            {CellType::TypeCode::Monocyte, 0},
            {CellType::TypeCode::Eosinophil, 0},
            {CellType::TypeCode::Basophil, 0},
        };

        QMap<CellType::TypeCode, uint32_t> wbcCountParams{
            {CellType::TypeCode::SegmentedNeutrophil, 0},
            {CellType::TypeCode::BandNeutrophil, 0},
            {CellType::TypeCode::Lymphocyte, 0},
            {CellType::TypeCode::Monocyte, 0},
            {CellType::TypeCode::Eosinophil, 0},
            {CellType::TypeCode::Basophil, 0},
        };
    };

    CBCData::CBCData() : d{ new Impl } {
    }

    CBCData::CBCData(const CBCData& other) : d{ new Impl } {
        *d = *other.d;
    }

    CBCData::~CBCData() {
    }

    auto CBCData::Init() -> void {
        for (auto& count : d->count) count = 0;
        for (auto& rbcParam : d->rbcParams) rbcParam = 0;
        for (auto& pltParam : d->pltParams) pltParam = 0;
        for (auto& wbcParam : d->wbcParams) wbcParam = 0;
        for (auto& wbcParam : d->wbcCountParams) wbcParam = 0;
    }

    auto CBCData::SetCellCount(CountEntry entry, uint32_t count) -> void {
        d->count[entry] = count;
    }

    auto CBCData::GetCellCount(CountEntry entry) const -> uint32_t {
        return d->count[entry];
    }

    auto CBCData::AddCellCount(CountEntry entry) -> void {
        d->count[entry] += 1;
    }

    auto CBCData::SetRBCParameter(RBCEntry entry, double value) -> void {
        d->rbcParams[entry] = value;
    }

    auto CBCData::GetRBCParameter(RBCEntry entry) const -> double {
        return d->rbcParams[entry];
    }

    auto CBCData::SetPlateletParameter(PlateletEntry entry, double value) -> void {
        d->pltParams[entry] = value;
    }

    auto CBCData::GetPlateletParameter(PlateletEntry entry) const -> double {
        return d->pltParams[entry];
    }

    auto CBCData::SetWBCParameter(CellType::TypeCode entry, double value) -> void {
        if (!CellType(entry).IsWBC()) return;
        d->wbcParams[entry] = value;

        const auto totalWBC = d->count[CountEntry::wbcCount];
        d->wbcCountParams[entry] = static_cast<uint32_t>(d->wbcParams[entry] * totalWBC);
    }

    auto CBCData::GetWBCParameter(CellType::TypeCode entry) const -> double {
        if (!CellType(entry).IsWBC()) return 0;
        return d->wbcParams[entry];
    }

    auto CBCData::AddWBCCount(CellType::TypeCode entry) -> void {
        if (!CellType(entry).IsWBC()) return;
        d->wbcCountParams[entry] += 1;

        const auto totalWBC = static_cast<double>(d->count[CountEntry::wbcCount]);
        if (totalWBC != 0) {
            for (auto itr = d->wbcParams.begin(); itr != d->wbcParams.end(); ++itr) {
                const auto wbcEntry = itr.key();
                *itr = d->wbcCountParams[wbcEntry] / totalWBC * 100;
            }
            d->wbcParams[entry] = d->wbcCountParams[entry] / totalWBC * 100;
        }
    }

    CBCData& CBCData::operator=(const CBCData other) {
        *d = *(other.d);
        return *this;
    }
}
