#include "CellType.h"

namespace CBCResearch::Entity {

    struct CellType::Impl {
        TypeCode code{ TypeCode::Excluded };
    };

    CellType::CellType() :d{ new Impl } {
    }

    CellType::CellType(const CellType& type) : d{ new Impl } {
        d->code = type.d->code;
    }

    CellType::CellType(TypeCode type) : d{ new Impl } {
        d->code = type;
    }

    CellType::CellType(int code) : d{ new Impl } {
        if (TypeCode::_is_valid(code)) {
            d->code = TypeCode::_from_integral(code);
        } else {
            d->code = TypeCode::Excluded;
        }
    }

    CellType::~CellType() {
    }

    auto CellType::operator==(const CellType& type) const-> bool {
        return (d->code == type.d->code);
    }

    auto CellType::operator=(const CellType& type) -> CellType& {
        d->code = type.d->code;
        return *this;
    }

    auto CellType::GetType() const-> TypeCode {
        return d->code;
    }

    auto CellType::IsRBC() const -> bool {
        return d->code._to_integral() == TypeCode::RBC;
    }

    auto CellType::IsWBC() const -> bool {
        const auto codeVal = d->code._to_integral();
        return ((codeVal >= 300) && (codeVal < 400));
    }

    auto CellType::IsPlatelet() const -> bool {
        return (d->code._to_integral() == TypeCode::PLT);
    }

    auto CellType::toString() const -> QString {
        return QString::fromLatin1(d->code._to_string());
    }

    auto CellType::TypeCount() -> uint32_t {
        return static_cast<uint32_t>(TypeCode::_size());
    }

    auto CellType::TypeByIndex(uint32_t index) -> TypeCode {
        if (index >= TypeCount()) return TypeCode::Excluded;
        return TypeCode::_values()[index];
    }

    auto CellType::Convert(int typeValue, TypeCode defaultType) -> TypeCode {
        if (TypeCode::_is_valid(typeValue)) {
            return TypeCode::_from_integral(typeValue);
        }

        return defaultType;
    }

    auto CellType::Convert(TypeCode type) -> int {
        return type._to_integral();
    }
}
