#include <QMap>

#include "MaskData.h"

namespace CBCResearch::Entity {
    struct MaskBlob::Impl {
        struct {
            int x, y, z;
        } center{ 0, 0, 0 };

        struct {
            int x0, y0, z0;
            int x1, y1, z1;
        } bbox{ 0, 0, 0, 1, 1, 1};

        int code{ 0 };

        VolumePtr maskVolume{ nullptr };
    };

    MaskBlob::MaskBlob() : d{ new Impl } {
    }

    MaskBlob::MaskBlob(const MaskBlob& other) : d{ new Impl } {
        *d = *other.d;
    }

    MaskBlob::~MaskBlob() {
    }

    MaskBlob& MaskBlob::operator=(const MaskBlob& other) {
        *d = *other.d;
        return *this;
    }

    auto MaskBlob::GetCenter() const -> std::tuple<int, int, int> {
        return std::make_tuple(d->center.x, d->center.y, d->center.z);
    }

    auto MaskBlob::SetBoundingBox(int x0, int y0, int z0, int x1, int y1, int z1) -> void {
        d->bbox.x0 = x0;
        d->bbox.y0 = y0;
        d->bbox.z0 = z0;
        d->bbox.x1 = x1;
        d->bbox.y1 = y1;
        d->bbox.z1 = z1;

        d->center.x = (d->bbox.x0 + d->bbox.x1) / 2;
        d->center.y = (d->bbox.y0 + d->bbox.y1) / 2;
        d->center.z = (d->bbox.z0 + d->bbox.z1) / 2;
    }

    auto MaskBlob::GetBoundingBox() const -> std::tuple<int, int, int, int, int, int> {
        return std::make_tuple(d->bbox.x0, d->bbox.y0, d->bbox.z0, d->bbox.x1, d->bbox.y1, d->bbox.z1);
    }

    auto MaskBlob::SetCode(int code) -> void {
        d->code = code;
    }

    auto MaskBlob::GetCode() const -> int {
        return d->code;
    }

    auto MaskBlob::SetMaskVolume(VolumePtr volume) -> void {
        d->maskVolume = volume;
    }

    auto MaskBlob::GetMaskVolume() -> VolumePtr {
        return d->maskVolume;
    }

    struct MaskData::Impl {
        QMap<int, MaskBlob> blobs;
        bool valid{ false };
    };

    MaskData::MaskData() : d{ new Impl } {
    }

    MaskData::~MaskData() {
    }

    auto MaskData::AppendBlob(int index, MaskBlob& blob) -> void {
        d->blobs[index] = blob;
        SetValid(true);
    }

    auto MaskData::GetBlobIndexes() const -> QList<int> {
        return d->blobs.keys();
    }

    auto MaskData::GetBlob(int index) const -> MaskBlob {
        if (!d->blobs.contains(index)) return MaskBlob();
        return d->blobs[index];
    }

    auto MaskData::CountBlobs() const -> int {
        return d->blobs.size();
    }

    auto MaskData::IsValid() const -> bool {
        return d->valid;
    }

    auto MaskData::SetValid(bool valid) -> void {
        d->valid = valid;
    }

    auto MaskData::UpdateCode(int index, int code) -> bool {
        if(!d->blobs.contains(index)) return false;
        d->blobs[index].SetCode(code);
        return true;
    }

    auto MaskData::GetMaskVolume(int index) const -> std::shared_ptr<uint8_t> {
        if (!d->blobs.contains(index)) return nullptr;
        return d->blobs[index].GetMaskVolume();
    }

    auto MaskData::AppendMaskVolume(int index, std::shared_ptr<uint8_t> maskVolume)->void {
        d->blobs[index].SetMaskVolume(maskVolume);
    }
}
