#include <QMap>
#include "CellData.h"

namespace CBCResearch::Entity {
    struct CellData::Impl {
        QMap<Entry, double> data{
            {Entry::volume, 0},
            {Entry::surfaceArea, 0},
            {Entry::projectedArea, 0},
            {Entry::sphericity, 0},
            {Entry::meanRI, 0},
            {Entry::dryMass, 0},
            {Entry::concentration, 0}
        };
    };

    CellData::CellData() : d{ new Impl } {
    }

    CellData::CellData(const CellData& other) : d{ new Impl } {
        *d = *(other.d);
    }

    CellData::~CellData() {
    }

    CellData& CellData::operator=(const CellData& other) {
        *d = *(other.d);
        return *this;
    }

    auto CellData::SetData(Entry entry, double value) -> void {
        d->data[entry] = value;
    }

    auto CellData::GetData(Entry entry) const -> double {
        return d->data[entry];
    }
}
