#include "WorkingSet.h"

#include <QFileInfo>
#include <QList>
#include <QMap>

namespace CBCResearch::Entity {
    struct WorkingSet::Impl {
        QString sourceDirectory;
        QString workingDirectory;

        QList<QString> tcfList;
        QMap<QString, TCFIndex> tcfIndexMap;
        QMap<TCFIndex, QString> maskFileMap;
        QMap<TCFIndex, QString> cellDataFileMap;
        QMap<TCFIndex, TCFStatus::Status> tcfStatus;

        TCFIndex maskIndex;
        MaskData::Pointer maskData;
        CBCData::Pointer cbcData;

        QMap<CellType::TypeCode, QList<TCFIndex>> tcfListPerType;
    };

    WorkingSet::WorkingSet() : d{ new Impl } {
    }

    WorkingSet::~WorkingSet() {
    }

    auto WorkingSet::GetInstance() -> Pointer {
        static Pointer theInstance{ new WorkingSet() };
        return theInstance;
    }

    auto WorkingSet::SetSourceDirectory(const QString& path)->void {
        d->sourceDirectory = path;
        Clear();
    }

    auto WorkingSet::GetSourceDirectory(void) const->QString {
        return d->sourceDirectory;
    }

    auto WorkingSet::SetWorkingDirectory(const QString& path)->void {
        d->workingDirectory = path;
        Clear();
    }

    auto WorkingSet::GetWorkingDirectory(void) const->QString {
        return d->workingDirectory;
    }

    auto WorkingSet::AppendTCFPath(const QString& path)->TCFIndex {
        d->tcfList.push_back(path);
        TCFIndex index = d->tcfList.size() - 1;
        d->tcfIndexMap[path] = index;
        d->tcfStatus[index] = TCFStatus::Status::Loaded;

        return index;
    }

    auto WorkingSet::TCFCount(void) const->unsigned int {
        return d->tcfList.size();
    }

    auto WorkingSet::GetTCFIndex(const QString& path) const->int {
        if (d->tcfIndexMap.contains(path)) return d->tcfIndexMap[path];
        return -1;
    }

    auto WorkingSet::GetTCFPath(const TCFIndex index) const->QString {
        if (d->tcfList.size() <= index) return "";
        return d->tcfList.at(index);
    }

    auto WorkingSet::GetTCFIndexList() const -> QList<TCFIndex> {
        QList<TCFIndex> list;
        for (const auto path : d->tcfList) {
            list.push_back(d->tcfIndexMap[path]);
        }
        return list;
    }

    auto WorkingSet::GetTCFStatus(TCFIndex index) const -> TCFStatus::Status {
        return d->tcfStatus[index];
    }

    auto WorkingSet::SetMaskPath(TCFIndex index, const QString& path) -> void {
        d->maskFileMap[index] = path;
    }

    auto WorkingSet::GetMaskPath(TCFIndex index) const -> QString {
        if (d->maskFileMap.contains(index)) return d->maskFileMap[index];
        const auto tcfPath = GetTCFPath(index);
        auto name = QFileInfo(tcfPath).fileName();
        return QString("%1/%2.MSK").arg(GetWorkingDirectory()).arg(name.remove(".TCF", Qt::CaseInsensitive));
    }

    auto WorkingSet::GetMaskPathList() const -> QStringList {
        QStringList path;

        auto indexList = GetTCFIndexList();
        for (auto index : indexList) {
            path.push_back(GetMaskPath(index));
        }

        return path;
    }

    auto WorkingSet::SetCurrentMaskData(TCFIndex index, MaskData::Pointer data) -> void {
        d->maskIndex = index;
        d->maskData = data;
    }

    auto WorkingSet::GetCurrentMaskIndex() -> TCFIndex {
        return d->maskIndex;
    }

    auto WorkingSet::GetCurrentMaskData() -> MaskData::Pointer {
        return d->maskData;
    }

    auto WorkingSet::SetCellDataPath(TCFIndex index, const QString& path) -> void {
        d->cellDataFileMap[index] = path;
    }

    auto WorkingSet::GetCellDataPath(TCFIndex index) const -> QString {
        if (d->cellDataFileMap.contains(index)) return d->cellDataFileMap[index];
        return "";
    }

    auto WorkingSet::SetCBCData(const CBCData::Pointer data) -> void {
        d->cbcData = data;
    }

    auto WorkingSet::GetCBCData() -> CBCData::Pointer {
        if (d->cbcData.get() == nullptr) d->cbcData.reset(new CBCData);
        return d->cbcData;
    }

    auto WorkingSet::AddTCFIndex(const CellType cellType, TCFIndex index, TCFStatus::Status status) -> void {
        auto& list = d->tcfListPerType[cellType.GetType()];
        if (!list.contains(index)) {
            list.append(index);
        }
        d->tcfStatus[index] = status;
    }

    auto WorkingSet::GetTCFIndexList(const CellType cellType) const -> QList<TCFIndex> {
        const auto typeCode = cellType.GetType();
        return GetTCFIndexList(typeCode);
    }

    auto WorkingSet::GetTCFPathList(const CellType cellType) const -> QStringList {
        const auto typeCode = cellType.GetType();
        return GetTCFPathList(typeCode);
    }

    auto WorkingSet::GetTCFIndexList(const CellType::TypeCode typeCode) const -> QList<TCFIndex> {
        return d->tcfListPerType[typeCode];
    }

    auto WorkingSet::GetTCFPathList(const CellType::TypeCode typeCode) const -> QStringList {
        QStringList list;

        for (auto index : d->tcfListPerType[typeCode]) {
            list.append(d->tcfList.at(index));
        }

        return list;
    }

    auto WorkingSet::Clear()->void {
        d->tcfList.clear();
        d->tcfIndexMap.clear();
        d->maskFileMap.clear();
        d->cellDataFileMap.clear();
        d->maskIndex = -1;
        d->maskData = nullptr;
        d->cbcData = nullptr;
        d->tcfListPerType.clear();
    }
}