#pragma once

#include <memory>
#include <QString>

#include "CellType.h"
#include "CBCResearchEntityExport.h"


namespace CBCResearch::Entity {

    class CBCResearchEntity_API CBCData {
    public:
        typedef CBCData Self;
        typedef std::shared_ptr<Self> Pointer;

        enum class CountEntry {
            rbcCount,
            wbcCount,
            pltCount,
        };

        enum class RBCEntry {
            Hb,
            Hct,
            MCV,
            MCH,
            MCHC,
            RDW,
            HDW
        };

        enum class PlateletEntry {
            PCT,
            MPV,
            PDW
        };

    public:
        CBCData();
        CBCData(const CBCData& other);
        virtual ~CBCData();

        auto Init()->void;

        auto SetCellCount(CountEntry entry, uint32_t count)->void;
        auto GetCellCount(CountEntry entry) const->uint32_t;
        auto AddCellCount(CountEntry entry)->void;

        auto SetRBCParameter(RBCEntry entry, double value)->void;
        auto GetRBCParameter(RBCEntry entry) const->double;

        auto SetPlateletParameter(PlateletEntry entry, double value)->void;
        auto GetPlateletParameter(PlateletEntry entry) const->double;

        auto SetWBCParameter(CellType::TypeCode entry, double value)->void;
        auto GetWBCParameter(CellType::TypeCode entry) const->double;
        auto AddWBCCount(CellType::TypeCode entry)->void;

        CBCData& operator=(const CBCData other);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}