#pragma once

#include "CBCResearchEntityExport.h"

namespace CBCResearch::Entity {

    class CBCResearchEntity_API TCFStatus {
    public:
        enum class Status {
            Loaded,
            Segmented,
            Annotated
        };
        
    protected:
        TCFStatus();
    };
}