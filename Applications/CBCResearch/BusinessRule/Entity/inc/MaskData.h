#pragma once

#include <memory>
#include <QString>
#include <QList>

#include "CBCResearchEntityExport.h"

namespace CBCResearch::Entity {

    class CBCResearchEntity_API MaskBlob {
    public:
        typedef std::shared_ptr<uint8_t> VolumePtr;

    public:
        MaskBlob();
        MaskBlob(const MaskBlob& other);
        ~MaskBlob();

        MaskBlob& operator=(const MaskBlob& other);

        auto GetCenter(void) const->std::tuple<int, int, int>;
        auto SetBoundingBox(int x0, int y0, int z0, int x1, int y1, int z1)->void;
        auto GetBoundingBox(void) const->std::tuple<int, int, int, int, int, int>;

        auto SetCode(int code)->void;
        auto GetCode() const->int;

        auto SetMaskVolume(VolumePtr volume)->void;
        auto GetMaskVolume()->VolumePtr;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class CBCResearchEntity_API MaskData {
    public:
        typedef MaskData Self;
        typedef std::shared_ptr<Self> Pointer;

    public:
        MaskData();
        virtual ~MaskData();

        auto AppendBlob(int index, MaskBlob& blob)->void;
        auto GetBlobIndexes() const->QList<int>;
        auto GetBlob(int index) const->MaskBlob;
        auto CountBlobs(void) const->int;

        auto IsValid() const->bool;
        auto SetValid(bool valid)->void;

        auto UpdateCode(int index, int code)->bool;

        auto AppendMaskVolume(int index, MaskBlob::VolumePtr maskVolume)->void;
        auto GetMaskVolume(int index) const->MaskBlob::VolumePtr;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}