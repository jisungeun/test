#pragma once

#include <memory>
#include <QStringList>

#include "CellType.h"
#include "CBCData.h"
#include "MaskData.h"
#include "TCFStatus.h"
#include "CBCResearchEntityExport.h"

namespace CBCResearch::Entity {
    typedef int TCFIndex;

    class CBCResearchEntity_API WorkingSet {
    public:
        typedef WorkingSet Self;
        typedef std::shared_ptr<Self> Pointer;
        
    public:
        virtual ~WorkingSet();

        static auto GetInstance(void)->Pointer;

        auto SetSourceDirectory(const QString& path)->void;
        auto GetSourceDirectory(void) const->QString;
        
        auto SetWorkingDirectory(const QString& path)->void;
        auto GetWorkingDirectory(void) const->QString;
        
        auto AppendTCFPath(const QString& path)->TCFIndex;
        auto TCFCount(void) const->unsigned int;
        auto GetTCFIndex(const QString& path) const->int;
        auto GetTCFPath(const TCFIndex index) const->QString;
        auto GetTCFIndexList() const->QList<TCFIndex>;

        auto GetTCFStatus(TCFIndex index) const->TCFStatus::Status;

        auto SetMaskPath(TCFIndex index, const QString& path)->void;
        auto GetMaskPath(TCFIndex index) const->QString;
        auto GetMaskPathList() const->QStringList;

        auto SetCurrentMaskData(TCFIndex index, MaskData::Pointer data)->void;
        auto GetCurrentMaskIndex()->TCFIndex;
        auto GetCurrentMaskData()->MaskData::Pointer;

        auto SetCellDataPath(TCFIndex index, const QString& path)->void;
        auto GetCellDataPath(TCFIndex index) const->QString;

        auto SetCBCData(const CBCData::Pointer data)->void;
        auto GetCBCData(void)->CBCData::Pointer;

        auto AddTCFIndex(CellType cellType, TCFIndex index, TCFStatus::Status status = TCFStatus::Status::Segmented)->void;
        auto GetTCFIndexList(CellType cellType) const->QList<TCFIndex>;
        auto GetTCFPathList(CellType cellType) const->QStringList;
        auto GetTCFIndexList(CellType::TypeCode typeCode) const->QList<TCFIndex>;
        auto GetTCFPathList(CellType::TypeCode typeCode) const->QStringList;

    protected:
        WorkingSet();
        
        auto Clear()->void;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}