#pragma once

#include <memory>
#include <QString>

#include "CBCResearchEntityExport.h"


namespace CBCResearch::Entity {
    class CBCResearchEntity_API CellData {
    public:
        enum class Entry {
            volume,             //um^3 = 1fL
            surfaceArea,        //um^2
            projectedArea,      //um^2
            sphericity,
            meanRI,
            dryMass,            //pg
            concentration       //pg/um^3 = pg/fL
        };
    public:
        CellData();
        CellData(const CellData& other);
        virtual ~CellData();

        CellData& operator=(const CellData& other);

        auto SetData(Entry entry, double value)->void;
        auto GetData(Entry entry) const->double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}