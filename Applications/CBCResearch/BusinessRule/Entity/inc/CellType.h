#pragma once

#include <memory>
#include <enum.h>
#include <QString>
#include "CBCResearchEntityExport.h"

namespace CBCResearch::Entity {
    
    BETTER_ENUM(CellTypeEnum, int,
        RBC                     = 100,  //RBC
        PLT                     = 200,  //Platelet
        SegmentedNeutrophil     = 300,  //WBC
        BandNeutrophil          = 301,
        Lymphocyte              = 302,
        Monocyte                = 303,
        Eosinophil              = 304,
        Basophil                = 305,
        UnknownCell             = 400,  //Unknown
        Excluded                = 500   //Excluded
    )
    
    class CBCResearchEntity_API CellType {
    public:
        typedef CellTypeEnum TypeCode;
        
    public:
        CellType();
        explicit CellType(const CellType& type);
        explicit CellType(TypeCode type);
        explicit CellType(int code);
        virtual ~CellType();
        
        auto operator==(const CellType& type) const->bool;
        auto operator=(const CellType& type)->CellType&;

        auto GetType() const->TypeCode;
        auto IsRBC() const->bool;
        auto IsWBC() const->bool;
        auto IsPlatelet() const->bool;
        auto toString() const->QString;

        static auto TypeCount()->uint32_t;
        static auto TypeByIndex(uint32_t index)->TypeCode;
        static auto Convert(int typeValue, TypeCode defaultType)->TypeCode;
        static auto Convert(TypeCode type)->int;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}