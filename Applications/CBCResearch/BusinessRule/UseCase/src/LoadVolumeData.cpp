#include "WorkingSet.h"
#include "LoadVolumeData.h"

namespace CBCResearch::UseCase {
    struct LoadVolumeData::Impl {
        IVolumeDataPort* port{ nullptr };
    };

    LoadVolumeData::LoadVolumeData(IVolumeDataPort* port) : d{ new Impl } {
        d->port = port;
    }

    LoadVolumeData::~LoadVolumeData() {
        
    }

    auto LoadVolumeData::Request(const int index) -> bool {
        auto workset = Entity::WorkingSet::GetInstance();
        auto path = workset->GetTCFPath(index);

        if (d->port) d->port->Update(path);

        return true;
    }
}