#include <WorkingSet.h>

#include "SourceList.h"
#include "ScanDirectory.h"

namespace CBCResearch::UseCase {
    struct ScanDirectory::Impl {
        IDirectoryScanner* scanner{ nullptr };
        IScanDirectoryPort* port{ nullptr };
        IMaskReader* maskReader{ nullptr };
    };

    ScanDirectory::ScanDirectory(IDirectoryScanner* scanner, IScanDirectoryPort* port, IMaskReader* reader) : d{ new Impl } {
        d->scanner = scanner;
        d->port = port;
        d->maskReader = reader;
    }

    ScanDirectory::~ScanDirectory() {
    }

    auto ScanDirectory::Request(const QString& topDir) const -> bool {
        if (!d->scanner) return false;

        using Status = SourceList::Status;

        SourceList::Pointer sources{ new SourceList() };

        auto wset = Entity::WorkingSet::GetInstance();
        wset->SetSourceDirectory(topDir);

        auto paths = d->scanner->Scan(topDir);
        for(const auto& path : paths) {
            const auto tcfIndex =  wset->AppendTCFPath(path);

            Status status = Status::Loaded;

            const auto maskPath = wset->GetMaskPath(tcfIndex);
            if (!maskPath.isEmpty() && d->maskReader) {
                const auto maskData = d->maskReader->Read(maskPath);
                if (maskData.get() != nullptr) {
                    for (auto blobIdx = 0; blobIdx < maskData->CountBlobs(); ++blobIdx) {
                        const auto blob = maskData->GetBlob(blobIdx);
                        wset->AddTCFIndex(Entity::CellType(blob.GetCode()), tcfIndex, Status::Segmented);
                    }

                    status = Status::Segmented;
                }
            }

            sources->SetSource(tcfIndex, path, status);
        }

        if(d->port) {
            d->port->Update(sources);
        }

        return true;
    }
}