#include "IMaskReader.h"
#include "IAnnotationExporter.h"

namespace CBCResearch::UseCase {
    struct IAnnotationExporter::Impl {
        IMaskReader* reader{ nullptr };
        QString path;
        bool wbcOnly{ false };
    };

    IAnnotationExporter::IAnnotationExporter() : d{ new Impl } {
    }

    IAnnotationExporter::~IAnnotationExporter() {
    }

    auto IAnnotationExporter::SetPath(const QString& path)->void {
        d->path = path;
    }

    auto IAnnotationExporter::SetMaskReader(IMaskReader* reader)->void {
        d->reader = reader;
    }

    auto IAnnotationExporter::SetWBCOnly(const bool wbcOnly) -> void {
        d->wbcOnly = wbcOnly;
    }

    auto IAnnotationExporter::GetPath() const -> QString {
        return d->path;
    }

    auto IAnnotationExporter::GetMaskReader() const -> IMaskReader* {
        return d->reader;
    }

    auto IAnnotationExporter::IsWBCOnly() const -> bool {
        return d->wbcOnly;
    }
}
