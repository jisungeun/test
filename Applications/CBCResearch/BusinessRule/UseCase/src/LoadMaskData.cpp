#include <QFileInfo>
#include "WorkingSet.h"
#include "LoadMaskData.h"

namespace CBCResearch::UseCase {
    struct LoadMaskData::Impl {
        ISegmentedCellsPort* port{ nullptr };
        IMaskReader* reader{ nullptr };
    };

    LoadMaskData::LoadMaskData(IMaskReader* reader, ISegmentedCellsPort* port) : d{ new Impl } {
        d->port = port;
        d->reader = reader;
    }

    LoadMaskData::~LoadMaskData() {
        
    }

    auto LoadMaskData::Request(const int index, const bool loadMaskVolume) -> bool {
        auto workset = Entity::WorkingSet::GetInstance();
        auto maskfile = workset->GetMaskPath(index);

        if (d->reader) {
            auto mask = d->reader->Read(maskfile, loadMaskVolume);
            if (mask.get() == nullptr) return false;

            workset->SetCurrentMaskData(index, mask);
            if (d->port) {
                d->port->Update(index, mask);
            }
        }

        return true;
    }
}