#include <WorkingSet.h>

#include "ICBCCalculator.h"
#include "IUpdateCBCResultPort.h"
#include "CalculateCBC.h"

namespace CBCResearch::UseCase {
    struct CalculateCBC::Impl {
        ICBCCalculator* calculator{ nullptr };
        IUpdateCBCResultPort* port{ nullptr };
    };

    CalculateCBC::CalculateCBC(ICBCCalculator* calculator, IUpdateCBCResultPort* port) : d{ new Impl } {
        d->calculator = calculator;
        d->port = port;
    }

    CalculateCBC::~CalculateCBC() {
    }

    auto CalculateCBC::Request() -> bool {
        if (!d->calculator) return false;

        auto wset = Entity::WorkingSet::GetInstance();
        const auto tcfIndexList = wset->GetTCFIndexList();

        QList<QPair<QString, QString>> list;
        for (const auto index : tcfIndexList) {
            const auto tcfPath = wset->GetTCFPath(index);
            const auto maskPath = wset->GetMaskPath(index);
            list.append(QPair(tcfPath, maskPath));
        }

        auto cbcData = d->calculator->Calculate(list);

        wset->SetCBCData(cbcData);

        if (d->port) {
            d->port->Update(wset->GetCBCData());
        }

        return true;
    }

}
