#include "SourceList.h"

#include <QList>

namespace CBCResearch::UseCase {
    struct SourceList::Impl {
        QList<Item> sources;
    };

    SourceList::SourceList() : d{ new Impl } {
    }

    SourceList::SourceList(const SourceList& other) : d{ new Impl } {
        *d = *(other.d);
    }

    SourceList::~SourceList() {
    }

    auto SourceList::SetSource(const int index, const QString& path, Status status) -> void {
        Item item;
        item.index = index;
        item.path = path;
        item.status = status;

        d->sources.append(item);
    }

    auto SourceList::GetSource(const int index) const -> Item {
        if (index >= d->sources.size()) return Item();
        return d->sources.at(index);
    }

    auto SourceList::GetCount() const -> int {
        return d->sources.size();
    }
}
