#pragma once

#include <WorkingSet.h>
#include <CellType.h>

#include "IAnnotatePointPort.h"
#include "IMaskWriter.h"
#include "AnnotatePoint.h"

namespace CBCResearch::UseCase {
    struct AnnotatePoint::Impl {
        IMaskWriter* writer{ nullptr };
        IAnnotatePointPort* port{ nullptr };
    };
    
    AnnotatePoint::AnnotatePoint(IMaskWriter* writer, IAnnotatePointPort* port) : d{ new Impl } {
        d->writer = writer;
        d->port = port;
    }
    
    AnnotatePoint::~AnnotatePoint() {
    }
    
    auto AnnotatePoint::Request(int index, int code)->bool {
        auto ws = Entity::WorkingSet::GetInstance();
        auto maskData = ws->GetCurrentMaskData();
        if (!maskData.get()) return false;
        if (!maskData->UpdateCode(index, code)) return false;

        const auto tcfIndex = ws->GetCurrentMaskIndex();
        ws->AddTCFIndex(Entity::CellType(code), tcfIndex);

        if (d->writer) {
            if (!d->writer->UpdateRow(index, maskData, ws->GetMaskPath(tcfIndex))) return false;
        }

        if (d->port) {
            d->port->Update(tcfIndex, maskData);
        }

        return true;
    }
}
