#include <QList>

#include <WorkingSet.h>

#include "GetFilteredList.h"

namespace CBCResearch::UseCase {
    struct GetFilteredList::Impl {
        IScanDirectoryPort* port{ nullptr };
    };

    GetFilteredList::GetFilteredList(IScanDirectoryPort* port) : d{ new Impl } {
        d->port = port;
    }

    GetFilteredList::~GetFilteredList() {
    }

    auto GetFilteredList::Request(const bool applyFilter, const TypeCode type) -> bool {
        if (!d->port) return false;

        SourceList::Pointer sources{ new SourceList() };

        auto wset = Entity::WorkingSet::GetInstance();

        QList<Entity::TCFIndex> indexList;
        if (applyFilter) {
            indexList = wset->GetTCFIndexList(type);
        } else {
            indexList = wset->GetTCFIndexList();
        }

        for (auto index : indexList) {
            sources->SetSource(index, wset->GetTCFPath(index), wset->GetTCFStatus(index));
        }

        if (d->port) {
            d->port->Update(sources);
        }

        return true;
    }
}
