#include <WorkingSet.h>
#include "CreateWorkset.h"

namespace CBCResearch::UseCase {
    CreateWorkset::CreateWorkset() {
    }

    CreateWorkset::~CreateWorkset() {
    }

    auto CreateWorkset::Request(const QString& destDirectory) -> void {
        auto workset = Entity::WorkingSet::GetInstance();
        workset->SetWorkingDirectory(destDirectory);
    }

}