#include "ISegment.h"

namespace CBCResearch::UseCase {
    struct ISegment::Impl {
        QString srcPath;
        int defaultCode{ 0 };
    };

    ISegment::ISegment() : d{new Impl} {
    }

    ISegment::~ISegment() {
    }

    auto ISegment::SetSource(const QString& path) -> void {
        d->srcPath = path;
    }

    auto ISegment::SetDefaultCode(int code) -> void {
        d->defaultCode = code;
    }

    auto ISegment::GetSource() const -> QString {
        return d->srcPath;
    }

    auto ISegment::GetDefaultCode() const -> int {
        return d->defaultCode;
    }
}
