#include <WorkingSet.h>

#include "IMaskReader.h"
#include "IAnnotationExporter.h"

#include "ExportAnnotation.h"

namespace CBCResearch::UseCase {
    struct ExportAnnotation::Impl {
        IMaskReader* reader{ nullptr };
        IAnnotationExporter* exporter{ nullptr };
    };

    ExportAnnotation::ExportAnnotation(IMaskReader* reader, IAnnotationExporter* exporter) : d{ new Impl } {
        d->reader = reader;
        d->exporter = exporter;
    }

    ExportAnnotation::~ExportAnnotation() {
    }

    auto ExportAnnotation::Request(const QString& path, const bool exportWBCOnly)->bool {
        if (!d->exporter) return false;

        d->exporter->SetPath(path);
        d->exporter->SetMaskReader(d->reader);
        d->exporter->SetWBCOnly(exportWBCOnly);

        auto ws = Entity::WorkingSet::GetInstance();
        auto pathList = ws->GetMaskPathList();

        return d->exporter->Export(pathList);
    }
}