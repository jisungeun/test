#include <QStringList>
#include <CellType.h>
#include "ISegmentedCellsPort.h"
#include "ISegment.h"
#include "IMaskWriter.h"
#include "WorkingSet.h"
#include "MakeMaskData.h"

namespace CBCResearch::UseCase {

    auto GetPathName(const QString& strFullPath)->QString {
        auto strIn = strFullPath;
        strIn = strIn.replace("\\", "/");
        const auto tokens = strIn.split('/');
        return tokens.at(tokens.length() - 1);
    }

    struct MakeMaskData::Impl {
        ISegment* segment{ nullptr };
        IMaskWriter* writer{ nullptr };
        ISegmentedCellsPort* port{ nullptr };
    };

    MakeMaskData::MakeMaskData(ISegment* segment, IMaskWriter* writer, ISegmentedCellsPort* port) : d{new Impl} {
        d->segment = segment;
        d->writer = writer;
        d->port = port;
    }

    MakeMaskData::~MakeMaskData() {
        
    }

    auto MakeMaskData::Request(const int index) -> bool {
        if (!d->segment) return false;

        auto wset = Entity::WorkingSet::GetInstance();
        const auto sourcePath = wset->GetTCFPath(index);
        const auto fileName = GetPathName(sourcePath).replace(".TCF", ".MSK", Qt::CaseInsensitive);
        const auto outputPath = QString("%1/%2").arg(wset->GetWorkingDirectory()).arg(fileName);

        d->segment->SetSource(sourcePath);
        d->segment->SetDefaultCode(Entity::CellType::TypeCode::RBC);
        auto mask = d->segment->Run();
        if (mask.get() == nullptr) return false;

        if(d->writer->Write(mask, outputPath)) {
            wset->SetMaskPath(index, outputPath);
        } else {
            return false;
        }

        if(d->port) {
            d->port->Update(index, mask);
        }

        return true;
    }

}