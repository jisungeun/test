#pragma once

#include <QList>
#include <QPair>
#include <QString>

#include <CBCData.h>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {

    class CBCResearchUseCase_API ICBCCalculator {
    public:
        typedef QPair<QString, QString> ItemType;
        typedef QList<ItemType> ListType;

    public:
        ICBCCalculator();
        virtual ~ICBCCalculator();

        virtual auto Calculate(ListType& list)->Entity::CBCData::Pointer = 0;
    };
};