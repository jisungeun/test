#pragma once

#include <QString>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API IVolumeDataPort {
    public:
        IVolumeDataPort();
        virtual ~IVolumeDataPort();

        virtual auto Update(const QString& dataPath)->void = 0;
    };
}