#pragma once

#include "MaskData.h"
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API ISegmentedCellsPort {
    public:
        ISegmentedCellsPort();
        virtual ~ISegmentedCellsPort();

        virtual auto Update(const int index, Entity::MaskData::Pointer& mask)->void = 0;
    };
}