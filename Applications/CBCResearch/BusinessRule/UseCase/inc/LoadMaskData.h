#pragma once

#include <memory>

#include "ISegmentedCellsPort.h"
#include "IMaskReader.h"
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API LoadMaskData {
    public:
        LoadMaskData(IMaskReader* reader, ISegmentedCellsPort* port = nullptr);
        virtual ~LoadMaskData();

        auto Request(const int index, const bool loadMaskVolume = false)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}