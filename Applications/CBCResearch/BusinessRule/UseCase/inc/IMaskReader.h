#pragma once

#include <QString>
#include <MaskData.h>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API IMaskReader {
    public:
        IMaskReader();
        virtual ~IMaskReader();

        virtual auto Read(const QString& path, const bool loadMaskVolume = false) const->Entity::MaskData::Pointer = 0;
    };
}
