#pragma once

#include <MaskData.h>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API IAnnotatePointPort {
    public:
        IAnnotatePointPort();
        virtual ~IAnnotatePointPort();

        virtual auto Update(const int index, Entity::MaskData::Pointer& mask)->void = 0;
    };
}