#pragma once

#include <QString>
#include <MaskData.h>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API IMaskWriter {
    public:
        IMaskWriter();
        virtual ~IMaskWriter();

        virtual auto Write(Entity::MaskData::Pointer data, const QString& path)->bool = 0;
        virtual auto UpdateRow(int index, Entity::MaskData::Pointer data, const QString& path)->bool = 0;
    };
}
