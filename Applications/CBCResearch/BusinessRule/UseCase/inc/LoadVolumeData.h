#pragma once

#include <memory>

#include "IVolumeDataPort.h"
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API LoadVolumeData {
    public:
        LoadVolumeData(IVolumeDataPort* port = nullptr);
        virtual ~LoadVolumeData();

        auto Request(const int index)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}