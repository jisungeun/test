#pragma once

#include <CBCData.h>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {

    class CBCResearchUseCase_API IUpdateCBCResultPort {
    public:
        IUpdateCBCResultPort();
        virtual ~IUpdateCBCResultPort();

        virtual auto Update(Entity::CBCData::Pointer data)->void = 0;
    };
}