#pragma once

#include <memory>

#include <CellType.h>

#include "IScanDirectoryPort.h"
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API GetFilteredList {
    public:
        using TypeCode = Entity::CellType::TypeCode;

        GetFilteredList(IScanDirectoryPort* port = nullptr);
        virtual ~GetFilteredList();

        auto Request(bool applyFilter, TypeCode type = TypeCode::RBC)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}