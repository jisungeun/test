#pragma once

#include "SourceList.h"
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API IScanDirectoryPort {
    public:
        IScanDirectoryPort();
        virtual ~IScanDirectoryPort();

        virtual auto Update(const SourceList::Pointer& list)->void = 0;
    };
}
