#pragma once

#include <memory>
#include <QString>
#include <QPair>

#include "TCFStatus.h"

#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API SourceList {
    public:
        typedef SourceList Self;
        typedef std::shared_ptr<Self> Pointer;
        typedef Entity::TCFStatus::Status Status;

        struct Item {
            int index{ -1 };
            QString path;
            Status status{ Status::Loaded };
        };

    public:
        SourceList();
        SourceList(const SourceList& other);
        ~SourceList();

        auto SetSource(const int index, const QString& path, Status status = Status::Loaded)->void;
        auto GetSource(const int index) const->Item;
        auto GetCount(void) const->int;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

    };
}