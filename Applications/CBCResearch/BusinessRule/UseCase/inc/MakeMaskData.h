#pragma once

#include <memory>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class ISegmentedCellsPort;
    class ISegment;
    class IMaskWriter;

    class CBCResearchUseCase_API MakeMaskData {
    public:
        MakeMaskData(ISegment* segment, IMaskWriter* writer, ISegmentedCellsPort* port = nullptr);
        virtual ~MakeMaskData();

        auto Request(const int index)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}