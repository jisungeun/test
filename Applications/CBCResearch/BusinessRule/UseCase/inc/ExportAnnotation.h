#pragma once

#include <memory>
#include <QString>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class IMaskReader;
    class IAnnotationExporter;

    class CBCResearchUseCase_API ExportAnnotation {
    public:
        ExportAnnotation(IMaskReader* reader, IAnnotationExporter* exporter);
        virtual ~ExportAnnotation();

        auto Request(const QString& path, const bool exportWBCOnly = false)->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}