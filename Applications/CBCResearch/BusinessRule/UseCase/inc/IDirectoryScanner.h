#pragma once

#include <QStringList>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API IDirectoryScanner {
    public:
        IDirectoryScanner();
        virtual ~IDirectoryScanner();

        virtual auto Scan(const QString& topDir) const->QStringList = 0;
    };
}

