#pragma once

#include <QString>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API CreateWorkset {
    public:
        CreateWorkset();
        virtual ~CreateWorkset();

        auto Request(const QString& destDirectory)->void;
    };
}