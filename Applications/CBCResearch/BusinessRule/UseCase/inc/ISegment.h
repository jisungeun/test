#pragma once

#include <memory>
#include <QString>
#include <MaskData.h>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API ISegment {
    public:
        ISegment();
        virtual ~ISegment();

        auto SetSource(const QString& path)->void;
        auto SetDefaultCode(int code)->void;

        virtual auto Run()->Entity::MaskData::Pointer = 0;

    protected:
        auto GetSource() const->QString;
        auto GetDefaultCode() const->int;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
