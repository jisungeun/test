#pragma once

#include <memory>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class ICBCCalculator;
    class IUpdateCBCResultPort;

    class CBCResearchUseCase_API CalculateCBC {
    public:
        CalculateCBC(ICBCCalculator* calculator, IUpdateCBCResultPort* port = nullptr);
        virtual ~CalculateCBC();

        auto Request()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}