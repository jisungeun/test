#pragma once

#include <memory>

#include "IMaskReader.h"
#include "IDirectoryScanner.h"
#include "IScanDirectoryPort.h"
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class CBCResearchUseCase_API ScanDirectory {
    public:
        ScanDirectory(IDirectoryScanner* scanner = nullptr, IScanDirectoryPort* port = nullptr, IMaskReader* reader = nullptr);
        virtual ~ScanDirectory();

        auto Request(const QString& topDir) const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}