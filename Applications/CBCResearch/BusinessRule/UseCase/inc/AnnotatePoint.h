#pragma once

#include <memory>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class IAnnotatePointPort;
    class IMaskWriter;
    
    class CBCResearchUseCase_API AnnotatePoint {
    public:
        AnnotatePoint(IMaskWriter* writer, IAnnotatePointPort* port = nullptr);
        virtual ~AnnotatePoint();
        
        auto Request(int index, int code)->bool;
    
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
