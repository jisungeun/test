#pragma once

#include <memory>
#include <QString>
#include "CBCResearchUseCaseExport.h"

namespace CBCResearch::UseCase {
    class IMaskReader;

    class CBCResearchUseCase_API IAnnotationExporter {
    public:
        IAnnotationExporter();
        virtual ~IAnnotationExporter();

        auto SetPath(const QString& path)->void;
        auto SetMaskReader(IMaskReader* reader)->void;
        auto SetWBCOnly(const bool wbcOnly)->void;

        virtual auto Export(const QStringList& pahtList)->bool = 0;

    protected:
        auto GetPath() const->QString;
        auto GetMaskReader() const->IMaskReader*;
        auto IsWBCOnly() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}