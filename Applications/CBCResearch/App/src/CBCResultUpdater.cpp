#include "CBCResultUpdater.h"

namespace CBCResearch {
    struct CBCResultUpdater::Impl {
        Plugins::CBCReportPanel* widget{ nullptr };
    };

    CBCResultUpdater::CBCResultUpdater(Plugins::CBCReportPanel* widget) : d{ new Impl } {
        d->widget = widget;
    }

    CBCResultUpdater::~CBCResultUpdater() {
        
    }

    auto CBCResultUpdater::Update(List& list) -> void {
        if (!d->widget) return;
        d->widget->Clear();

        auto convert = [](List::value_type item)->Plugins::CBCReportPanel::Item {
            auto output = Plugins::CBCReportPanel::Item();
            output.title = item.title;
            output.value = item.value;
            output.unit = item.unit;
            return output;
        };

        for (const auto& item : list) {
            d->widget->AddItem(convert(item));
        }
    }
}