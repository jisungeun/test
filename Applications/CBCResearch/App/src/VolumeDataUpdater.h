#pragma once

#include <memory>
#include "CBCViewer.h"
#include "IVolumeDataWidget.h"

namespace CBCResearch {
    class VolumeDataUpdater : public Interactor::IVolumeDataWidget {
    public:
        VolumeDataUpdater(Plugins::CBCViewer* widget = nullptr);
        virtual ~VolumeDataUpdater();

        auto Update(const QString& dataPath) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}