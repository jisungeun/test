#pragma once

#include <QMainWindow>

namespace CBCResearch {
	class MainWindow : public QMainWindow
	{
		Q_OBJECT

	public:
		explicit MainWindow(QWidget* parent = nullptr);
		virtual ~MainWindow();

	protected:
		auto SetupToolBar()->void;
		auto SetupListFilter()->void;
		
	protected slots:
		void onCreateWorkSet();
		void onLoadProject();
		void onFileSelected(int index);
		void onListFilterChanged(int filter);
		void onQuit();
		void onRIThresholdChanged(int index, double minValue, double maxValue);
		void onBoundingBoxSelected(int index);
		void onSelectBFAsDefault();
		void onSelectHTAsDefault();
		void onSetTriggerMode(QAction* action);
		void onSetMaskMode(QAction* action);
		void onCalculateCBC();
		void onExportAnnotationAll();
		void onExportAnnotationWBC();
		void onSelectColormapTurbo();
		void onSelectColormapJet();
		void onSelectColormapHot();
		void onSelectColormapRainbow();
		void onSelectColormapInverseGreyscale();
		void onSetThresholdOffset();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
