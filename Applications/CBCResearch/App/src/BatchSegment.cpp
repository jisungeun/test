#include <QtConcurrent/QtConcurrent>
#include <QFutureWatcher>
#include <QProgressDialog>
#include <QThread>

#define LOGGER_TAG "BatchSegment"
#include <TCLogger.h>

#include <MaskDataReader.h>
#include <LoadMaskDataController.h>
#include <SegmentCellsController.h>
#include <SegmentCells.h>
#include <MaskDataWriter.h>
#include "BatchSegment.h"

#include <iostream>

namespace CBCResearch {
    class Item {
    public:
        int index;
        uint16_t offset;
    };

    void Process(Item item);

    struct BatchSegment::Impl {
        uint16_t offset = 0;
    };

    BatchSegment::BatchSegment(uint16_t offset) : d{ new Impl } {
        d->offset = offset;
    }

    BatchSegment::~BatchSegment() {
    }

    auto BatchSegment::Run(QList<int> indexes) -> bool {
        bool bSuccess = true;

        const auto maxCount = QThread::idealThreadCount();
        const auto usedCount = std::min<int>(5, maxCount * 0.5);
        QThreadPool::globalInstance()->setMaxThreadCount(usedCount);

        QProgressDialog dialog;
        dialog.setLabelText(QString("Progressing %1 files using %2 threads").arg(indexes.length()).arg(usedCount));

        QFutureWatcher<void> futureWatcher;
        QObject::connect(&futureWatcher, SIGNAL(finished()), &dialog, SLOT(reset()));
        QObject::connect(&dialog, SIGNAL(canceled()), &futureWatcher, SLOT(cancel()));
        QObject::connect(&futureWatcher, SIGNAL(progressRangeChanged(int, int)), &dialog, SLOT(setRange(int, int)));
        QObject::connect(&futureWatcher, SIGNAL(progressValueChanged(int)), &dialog, SLOT(setValue(int)));

        QList<Item> items;
        foreach(auto index, indexes) {
            Item item;
            item.index = index;
            item.offset = d->offset;
            items.push_back(item);
        }

        futureWatcher.setFuture(QtConcurrent::map(items, Process));

        dialog.exec();
        futureWatcher.waitForFinished();

        return !futureWatcher.future().isCanceled();
    }

    void Process(Item item) {
        QLOG_TRACE() << "Start process [index=" << item.index << "] with offset=" << item.offset;
        auto maskReader = Plugins::MaskDataReader();
        auto loadMaskInteractor = Interactor::LoadMaskData(&maskReader, nullptr);
        if (!loadMaskInteractor.Request(item.index, false)) {
            auto segment = Plugins::SegmentCells(item.offset);
            auto writer = Plugins::MaskDataWriter();
            auto segmentInteractor = Interactor::SegmentCellsController(&segment, &writer, nullptr);
            if (!segmentInteractor.Request(item.index)) {
                QLOG_ERROR() << "Failed to process [index=" << item.index << "]";
            }
        }
        QLOG_TRACE() << "End process [index=" << item.index << "]";
    }
}
