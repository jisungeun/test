#include <QSettings>
#include <QFile>
#include "CBCProjectFile.h"

namespace CBCResearch {
    struct ProjectFile::Impl {
        QString file;

        QString workingPath;
        QString sourcePath;

        struct {
            const QString workingPath{ "Working Directory" };
            const QString sourcePath{ "Source Directory" };
        } entry;
    };

    ProjectFile::ProjectFile() : d{ new Impl } {
    }

    ProjectFile::~ProjectFile() {
    }

    auto ProjectFile::GetInstance() -> Pointer {
        static Pointer theInstance{ new ProjectFile() };
        return theInstance;
    }

    auto ProjectFile::Create(const QString& path) -> void {
        d->file = path;
    }

    auto ProjectFile::Load(const QString& path) -> bool {
        if (path.isEmpty()) return false;
        d->file = path;

        return LoadInternal();
    }

    auto ProjectFile::Save(const QString& path) -> bool {
        d->file = path;
        return SaveInternal();
    }

    auto ProjectFile::SetWorkingDirectory(const QString& path) -> bool {
        d->workingPath = path;
        return SaveInternal();
    }

    auto ProjectFile::GetWorkingDirectory() -> QString {
        if (!LoadInternal()) return "";
        return d->workingPath;
    }

    auto ProjectFile::SetSourceDirectory(const QString& path) -> bool {
        d->sourcePath = path;
        return SaveInternal();
    }

    auto ProjectFile::GetSourceDirectory() -> QString {
        if (!LoadInternal()) return "";
        return d->sourcePath;
    }

    auto ProjectFile::SaveInternal() -> bool {
        if (d->file.isEmpty()) return false;

        QSettings qs(d->file, QSettings::Format::IniFormat);
        qs.setValue(d->entry.workingPath, d->workingPath);
        qs.setValue(d->entry.sourcePath, d->sourcePath);

        return true;
    }

    auto ProjectFile::LoadInternal() -> bool {
        if (d->file.isEmpty()) return false;
        if (!QFile::exists(d->file)) return false;

        QSettings qs(d->file, QSettings::Format::IniFormat);
        d->workingPath = qs.value(d->entry.workingPath).toString();
        d->sourcePath = qs.value(d->entry.sourcePath).toString();

        return true;
    }
};