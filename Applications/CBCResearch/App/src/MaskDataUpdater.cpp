#include "CBCAnnotation.h"
#include "MaskDataUpdater.h"

namespace CBCResearch {
    struct MaskDataUpdater::Impl {
        Plugins::TCFListPanel* listWidget{ nullptr };
        Plugins::CBCViewer* viewer{ nullptr };

        QColor code2color(const int code) {
            using namespace CBCResearch::Plugins;
            auto type = CBCAnnotation::Convert(code, CBCAnnotation::TypeCode::Excluded);
            return CBCAnnotation::Color(type);
        }
    };

    MaskDataUpdater::MaskDataUpdater() : d{ new Impl } {
    }

    MaskDataUpdater::~MaskDataUpdater() {
    }

    auto MaskDataUpdater::SetListWidget(Plugins::TCFListPanel* widget) -> void {
        d->listWidget = widget;
    }

    auto MaskDataUpdater::SetCBCViewer(Plugins::CBCViewer* widget) -> void {
        d->viewer = widget;
    }

    auto MaskDataUpdater::SetItem(int index, Entity::MaskData::Pointer& mask) -> void {
        using namespace CBCResearch::Plugins;

        if (!d->listWidget || !d->viewer) return;
        d->listWidget->SetStatus(index, TCFListPanel::Status::Segmented);

        CBCMaskData::Pointer cbcmask{ new  CBCMaskData() };
        const auto blobs = mask->CountBlobs();
        for (int idx = 0; idx < blobs; ++idx) {
            const auto blob = mask->GetBlob(idx);
            const auto bbox = blob.GetBoundingBox();
            const auto color = d->code2color(blob.GetCode());
            const auto cbcblob = CBCMaskBlob(
                std::get<0>(bbox), std::get<1>(bbox), std::get<2>(bbox),
                std::get<3>(bbox), std::get<4>(bbox), std::get<5>(bbox),
                color,
                idx
            );
            cbcmask->AppendBlob(cbcblob);

            auto maskVolume = mask->GetMaskVolume(idx);
            cbcmask->AppendMaskVolume(idx, maskVolume);
        }

        if(index == d->listWidget->Selected()) {
            d->viewer->SetMask(cbcmask);
        }
    }

    auto MaskDataUpdater::SetAnnotated(int index, Entity::MaskData::Pointer& mask) -> void {
        using namespace CBCResearch::Plugins;

        if (!d->listWidget || !d->viewer) return;
        d->listWidget->SetStatus(index, TCFListPanel::Status::Annotated);

        CBCMaskData::Pointer cbcmask{ new  CBCMaskData() };
        const auto blobs = mask->CountBlobs();
        for (int idx = 0; idx < blobs; ++idx) {
            const auto blob = mask->GetBlob(idx);
            const auto bbox = blob.GetBoundingBox();
            const auto color = d->code2color(blob.GetCode());
            const auto cbcblob = CBCMaskBlob(
                std::get<0>(bbox), std::get<1>(bbox), std::get<2>(bbox),
                std::get<3>(bbox), std::get<4>(bbox), std::get<5>(bbox),
                color,
                idx
            );
            cbcmask->AppendBlob(cbcblob);
        }

        if (index == d->listWidget->Selected()) {
            d->viewer->SetMask(cbcmask);
        }
    }
}
