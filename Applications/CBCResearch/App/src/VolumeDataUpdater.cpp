#include "VolumeDataUpdater.h"

namespace CBCResearch {
    struct VolumeDataUpdater::Impl {
        CBCResearch::Plugins::CBCViewer* widget{ nullptr };
    };

    VolumeDataUpdater::VolumeDataUpdater(CBCResearch::Plugins::CBCViewer* widget) : d{ new Impl } {
        d->widget = widget;
    }

    VolumeDataUpdater::~VolumeDataUpdater() {
    }

    auto VolumeDataUpdater::Update(const QString& dataPath) -> void {
        if (d->widget) d->widget->SetTCFPath(dataPath);
    }
}