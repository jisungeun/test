#include <FileUtility.h>
#include "TCFListUpdater.h"

namespace CBCResearch {
    struct TCFListUpdater::Impl {
        Plugins::TCFListPanel* widget{ nullptr };
    };

    TCFListUpdater::TCFListUpdater(Plugins::TCFListPanel* widget) : d{ new Impl } {
        d->widget = widget;
    }

    TCFListUpdater::~TCFListUpdater() {
    }

    auto TCFListUpdater::BeginUpdate() -> void {
        if (!d->widget) return;
        d->widget->BeginUpdate();
    }

    auto TCFListUpdater::EndUpdate() -> void {
        if (!d->widget) return;
        d->widget->EndUpdate();
    }

    auto TCFListUpdater::Clear() -> void {
        if (!d->widget) return;
        d->widget->ClearItems();
    }

    auto TCFListUpdater::AddItem(int index, const QString& path, Status status) -> void {
        if (!d->widget) return;
        auto fileName = TC::GetPathName(path).remove(".TCF", Qt::CaseInsensitive);

        auto convert = [](Status status)->Plugins::TCFListPanel::Status {
            auto retStatus = Plugins::TCFListPanel::Status::Loaded;

            switch (status) {
            case Status::Loaded:
                retStatus = Plugins::TCFListPanel::Status::Loaded;
                break;
            case Status::Segmented:
                retStatus = Plugins::TCFListPanel::Status::Segmented;
                break;
            case Status::Annotated:
                retStatus = Plugins::TCFListPanel::Status::Annotated;
                break;
            }

            return retStatus;
        };

        d->widget->AddItem(index, fileName, convert(status));
    }
}
