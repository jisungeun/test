#pragma once

#include <memory>
#include <QString>

namespace CBCResearch {
    class ProjectFile {
    public:
        typedef std::shared_ptr<ProjectFile> Pointer;

    public:
        virtual ~ProjectFile();
        static auto GetInstance()->Pointer;

        auto Create(const QString& path)->void;

        auto Load(const QString& path)->bool;
        auto Save(const QString& path)->bool;

        auto SetWorkingDirectory(const QString& path)->bool;
        auto GetWorkingDirectory()->QString;

        auto SetSourceDirectory(const QString& path)->bool;
        auto GetSourceDirectory()->QString;

    protected:
        ProjectFile();

        auto SaveInternal()->bool;
        auto LoadInternal()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}