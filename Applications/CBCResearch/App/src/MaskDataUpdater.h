#pragma once

#include <memory>
#include <TCFListPanel.h>
#include <CBCViewer.h>
#include <IMaskDataWidget.h>

namespace CBCResearch {
    class MaskDataUpdater : public Interactor::IMaskDataWidget {
    public:
        MaskDataUpdater();
        virtual ~MaskDataUpdater();

        auto SetListWidget(Plugins::TCFListPanel* widget)->void;
        auto SetCBCViewer(Plugins::CBCViewer* widget)->void;

        auto SetItem(int index, Entity::MaskData::Pointer& mask) -> void override;
        auto SetAnnotated(int index, Entity::MaskData::Pointer& mask) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}