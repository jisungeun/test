#include <QCoreApplication>
#include <QFileDialog>
#include <QMessageBox>
#include <QDir>
#include <QSettings>
#include <QInputDialog>
#include <QToolBar>
#include <QPixmap>
#include <QVariant>

#define LOGGER_TAG	"Main"
#include <TCLogger.h>

#include <FileUtility.h>
#include <CreateWorksetController.h>
#include <ScanDirectoryController.h>
#include <LoadVolumeDataController.h>
#include <SegmentCellsController.h>
#include <LoadMaskDataController.h>
#include <AnnotatePointController.h>
#include <GetFilteredListController.h>
#include <CalculateCBCController.h>
#include <ExportAnnotationController.h>
#include <TCFListPresenter.h>
#include <VolumeDataPresenter.h>
#include <MaskDataPresenter.h>
#include <AnnotatePointPresenter.h>
#include <UpdateCBCResultPresenter.h>
#include <TCFScanner.h>
#include <MaskDataReader.h>
#include <MaskDataWriter.h>
#include <CBCAnnotation.h>
#include <CBCAnnotationExporter.h>
#include <CBCCalculator.h>

#include "TCFListUpdater.h"
#include "VolumeDataUpdater.h"
#include "CBCResultUpdater.h"
#include "MaskDataUpdater.h"
#include "SegmentCells.h"
#include "BatchSegment.h"

#include "CBCProjectFile.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"

namespace CBCResearch {
	struct MainWindow::Impl {
		Ui::MainWindow* ui{ nullptr };
		TCFListUpdater* listUpdater{ nullptr };
		VolumeDataUpdater* volumeDataUpdater{ nullptr };
		MaskDataUpdater* maskDataUpdater{ nullptr };
		CBCResultUpdater* resultUpdater{ nullptr };

		QActionGroup* annotationActionGroup{ nullptr };
		QAction* annotationOffAction{ nullptr };

		QActionGroup* maskActionGroup{ nullptr };
		QAction* hidMaskAction{ nullptr };

		int currenIndex{ -1 };

		//registry keys
		struct {
			const char* riThresholdGrayMin = "RI Threshold/GrayMin";
			const char* riThresholdGrayMax = "RI Threshold/GrayMax";
			const char* riThresholdColorMin = "RI Threshold/ColorMin";
			const char* riThresholdColorMax = "RI Threshold/ColorMax";
			const char* viewModeRightImage = "View Configuration/Right Image";
			const char* colormapRightImage = "Colormap/Right Image";
			const char* recentWorkingFolder = "Recent/WorkingFolder";
			const char* recentSourceFolder = "Recent/SourceFolder";
			const char* modelFolder = "Model Folder";
			const char* thresholdOffset = "Parameters/ThresholdOffset";
		} entry;

		enum class MaskVisiblity {
			HIDE,
			SHOW
		};
	};

	MainWindow::MainWindow(QWidget* parent)
		: QMainWindow(parent)
		, d(new Impl())
	{
		d->ui = new Ui::MainWindow();
		d->ui->setupUi(this);

		QLOG_INFO() << "CBC Research is started";
		
		setWindowTitle("CBC Research");
		d->listUpdater = new TCFListUpdater(d->ui->filesView);
		d->volumeDataUpdater = new VolumeDataUpdater(d->ui->cbcview);
		d->maskDataUpdater = new MaskDataUpdater();
		d->maskDataUpdater->SetListWidget(d->ui->filesView);
		d->maskDataUpdater->SetCBCViewer(d->ui->cbcview);
		d->resultUpdater = new CBCResultUpdater(d->ui->reportPanel);

		const auto grayThresholdMin = QSettings().value(d->entry.riThresholdGrayMin, 1.337).toDouble();
		const auto grayThresholdMax = QSettings().value(d->entry.riThresholdGrayMax, 1.400).toDouble();
		const auto colorThresholdMin = QSettings().value(d->entry.riThresholdColorMin, 1.337).toDouble();
		const auto colorThresholdMax = QSettings().value(d->entry.riThresholdColorMax, 1.400).toDouble();
		d->ui->cbcview->SetRIThresholdForGray(grayThresholdMin, grayThresholdMax);
		d->ui->cbcview->SetRIThresholdForColor(colorThresholdMin, colorThresholdMax);

		auto* actGroup = new QActionGroup(this);
	    {
			actGroup->addAction(d->ui->actionBF);
			actGroup->addAction(d->ui->actionHT);
		}
		actGroup->setExclusive(true);

		actGroup = new QActionGroup(this);
		{
			actGroup->addAction(d->ui->actionTurbo);
			actGroup->addAction(d->ui->actionJet);
			actGroup->addAction(d->ui->actionHot);
			actGroup->addAction(d->ui->actionRainbow);
			actGroup->addAction(d->ui->actionInverse_Greyscale);
		}
		actGroup->setExclusive(true);

		const auto mode = QSettings().value(d->entry.viewModeRightImage, 0);
		if (mode == 0) {
			d->ui->cbcview->SetBFAsDefault();
			d->ui->actionBF->setChecked(true);
		} else {
			d->ui->cbcview->SetHTAsDefault();
			d->ui->actionHT->setChecked(true);
		}

		const auto defaultColormapInt = (+CBCResearch::Plugins::CBCViewerColormap::Turbo)._to_integral();
		const auto colormapInt = QSettings().value(d->entry.colormapRightImage, defaultColormapInt).toInt();
		const auto colormap = CBCResearch::Plugins::CBCViewerColormap::_from_integral(colormapInt);
		d->ui->cbcview->SetColormap(colormap);
		switch (colormap) {
		case CBCResearch::Plugins::CBCViewerColormap::Turbo:
			d->ui->actionTurbo->setChecked(true);
			break;
		case CBCResearch::Plugins::CBCViewerColormap::Jet:
			d->ui->actionJet->setChecked(true);
			break;
		case CBCResearch::Plugins::CBCViewerColormap::Hot:
			d->ui->actionHot->setChecked(true);
			break;
		case CBCResearch::Plugins::CBCViewerColormap::Rainbow:
			d->ui->actionRainbow->setChecked(true);
			break;
		case CBCResearch::Plugins::CBCViewerColormap::InverseGreyscale:
			d->ui->actionInverse_Greyscale->setChecked(true);
			break;
		}

		SetupToolBar();
		SetupListFilter();

		connect(d->ui->actionNew, SIGNAL(triggered()), this, SLOT(onCreateWorkSet()));
		connect(d->ui->actionLoad, SIGNAL(triggered()), this, SLOT(onLoadProject()));
		connect(d->ui->actionQuit, SIGNAL(triggered()), this, SLOT(onQuit()));
		connect(d->ui->actionAll_Annotation_Data, SIGNAL(triggered()), this, SLOT(onExportAnnotationAll()));
		connect(d->ui->actionWBC_Annotation_Data, SIGNAL(triggered()), this, SLOT(onExportAnnotationWBC()));
		connect(d->ui->filesView, SIGNAL(itemSelected(int)), this, SLOT(onFileSelected(int)));
		connect(d->ui->cbcview, SIGNAL(RIThresholdChanged(int, double, double)), this, SLOT(onRIThresholdChanged(int, double, double)));
		connect(d->ui->cbcview, SIGNAL(BBoxSelected(int)), this, SLOT(onBoundingBoxSelected(int)));
		connect(d->ui->actionBF, SIGNAL(triggered()), this, SLOT(onSelectBFAsDefault()));
		connect(d->ui->actionHT, SIGNAL(triggered()), this, SLOT(onSelectHTAsDefault()));
		connect(d->ui->reportPanel, SIGNAL(calculate()), this, SLOT(onCalculateCBC()));
		connect(d->ui->actionTurbo, SIGNAL(triggered()), this, SLOT(onSelectColormapTurbo()));
		connect(d->ui->actionJet, SIGNAL(triggered()), this, SLOT(onSelectColormapJet()));
		connect(d->ui->actionHot, SIGNAL(triggered()), this, SLOT(onSelectColormapHot()));
		connect(d->ui->actionRainbow, SIGNAL(triggered()), this, SLOT(onSelectColormapRainbow()));
		connect(d->ui->actionInverse_Greyscale, SIGNAL(triggered()), this, SLOT(onSelectColormapInverseGreyscale()));
		connect(d->ui->actionThreshold_Offset, SIGNAL(triggered()), this, SLOT(onSetThresholdOffset()));
	}

	MainWindow::~MainWindow() {
		QLOG_INFO() << "CBC Research is finished";
	}

    auto MainWindow::SetupToolBar() -> void {
		auto toolbar = addToolBar("Toolbar");
		toolbar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

		toolbar->addAction(d->ui->actionNew);
		toolbar->addAction(d->ui->actionLoad);

		toolbar->addSeparator();

		// right-side image
		d->ui->actionBF->setToolTip("Show bright-field image as default");
		d->ui->actionHT->setToolTip("Show holotographic image as default");

		toolbar->addAction(d->ui->actionBF);
		toolbar->addAction(d->ui->actionHT);

		toolbar->addSeparator();

		// on/off annotation
		auto actionGroup = new QActionGroup(toolbar);
		actionGroup->setExclusive(true);
		{
			auto pixmap = QPixmap(12, 12);
			pixmap.fill(Qt::white);
			
			auto action = toolbar->addAction(QIcon(pixmap), "Off");
			action->setToolTip("Disable Annotation Tool");
			action->setCheckable(true);
			action->setChecked(true);
			action->setData(-1);
			actionGroup->addAction(action);

			d->annotationOffAction = action;
		}

		const auto types = Plugins::CBCAnnotation::TypeCount();
		for (uint32_t idx = 0; idx < types; ++idx) {
			auto type = Plugins::CBCAnnotation::TypeByIndex(idx);

			auto pixmap = QPixmap(12, 12);
			pixmap.fill(Plugins::CBCAnnotation::Color(type));

			auto action = toolbar->addAction(QIcon(pixmap), Plugins::CBCAnnotation::ShortName(type));
			action->setToolTip(Plugins::CBCAnnotation::Description(type));
			action->setCheckable(true);
			action->setData(Plugins::CBCAnnotation::Convert(type));
			actionGroup->addAction(action);
		}

		d->annotationActionGroup = actionGroup;

		connect(actionGroup, SIGNAL(triggered(QAction*)), this, SLOT(onSetTriggerMode(QAction*)));

		// hide/show mask
		toolbar->addSeparator();

		actionGroup = new QActionGroup(toolbar);
		actionGroup->setExclusive(true);
		{
			auto pixmapHide = QPixmap(12, 12);
			pixmapHide.fill(Qt::white);

			auto action = toolbar->addAction(QIcon(pixmapHide), "Hide");
			action->setToolTip("Hide Mask");
			action->setCheckable(true);
			action->setChecked(true);
			action->setData(static_cast<int>(Impl::MaskVisiblity::HIDE));
			actionGroup->addAction(action);

			d->hidMaskAction = action;

			auto pixmapShow = QPixmap(12, 12);
			pixmapShow.fill(Qt::green);

			action = toolbar->addAction(QIcon(pixmapShow), "Show");
			action->setToolTip("Show Mask");
			action->setCheckable(true);
			action->setData(static_cast<int>(Impl::MaskVisiblity::SHOW));
			actionGroup->addAction(action);
		}

		d->maskActionGroup = actionGroup;

		connect(actionGroup, SIGNAL(triggered(QAction*)), this, SLOT(onSetMaskMode(QAction*)));
    }

    auto MainWindow::SetupListFilter() -> void {
		d->ui->filesView->AddFilter("All", -1);

		const auto types = Plugins::CBCAnnotation::TypeCount();
		for (uint32_t idx = 0; idx < types; ++idx) {
			auto type = Plugins::CBCAnnotation::TypeByIndex(idx);

			d->ui->filesView->AddFilter(Plugins::CBCAnnotation::ShortName(type), Plugins::CBCAnnotation::Convert(type));
		}

		connect(d->ui->filesView, SIGNAL(filterChanged(int)), this, SLOT(onListFilterChanged(int)));
    }

    void MainWindow::onCreateWorkSet() {
		auto prevWorking = QSettings().value(d->entry.recentWorkingFolder).toString();

		auto workingDirectory = [&]()->QString {
			while (1) {
				auto directory = QFileDialog::getExistingDirectory(this, "Select an empty directory to save results", prevWorking);
				if (directory.isEmpty()) return "";

				if (!TC::IsEmptyDirectory(directory)) {
					QMessageBox::information(this, "Select Working Directory", "This is not empty directory. Select another one");
					continue;
				}

				return directory;
			}
		}();

		if (workingDirectory.isEmpty()) {
			QMessageBox::warning(this, "Select Working Directory", "No directory is selected");
			return;
		}

		QSettings().setValue(d->entry.recentWorkingFolder, workingDirectory);

		auto createInteractor = CBCResearch::Interactor::CreateWorkset();
		createInteractor.Request(workingDirectory);

		auto prevSource = QSettings().value(d->entry.recentSourceFolder).toString();
		auto sourceDirectory = QFileDialog::getExistingDirectory(this, "Select a directory contains TCF files", prevSource);
		if (sourceDirectory.isEmpty()) {
			QMessageBox::warning(this, "Select Source Directory", "No directory is selected");
			return;
		}

		QSettings().setValue(d->entry.recentSourceFolder, sourceDirectory);

		auto scanPresenter = CBCResearch::Interactor::TCFListPresenter(d->listUpdater);
		auto scanner = CBCResearch::Plugins::TCFScanner();
		auto maskReader = Plugins::MaskDataReader();

		auto scanInteractor = CBCResearch::Interactor::ScanDirectory(&scanner, &scanPresenter, &maskReader);
		scanInteractor.Request(sourceDirectory);
		
		auto project = ProjectFile::GetInstance();
		project->Create(QString("%1/CBCResearch.prj").arg(workingDirectory));
		project->SetWorkingDirectory(workingDirectory);
		project->SetSourceDirectory(sourceDirectory);

		auto items = d->ui->filesView->AllItems();

		const auto thOffset = static_cast<uint16_t>(QSettings().value(d->entry.thresholdOffset, 0).toDouble() * 10000);

		QLOG_INFO() << "Start batch segment for " << items.size() << " data";
		auto batch = BatchSegment(thOffset);
		if (!batch.Run(items)) {
			QMessageBox::warning(this, "Batch Processing", "Some of data may not be processed");
		}
		QLOG_INFO() << "Batch segment is finished";
	}

    void MainWindow::onLoadProject() {
		auto prevWorking = QSettings().value(d->entry.recentWorkingFolder).toString();
		auto projectFile = [&]()->QString {
			while (1) {
				auto file = QFileDialog::getOpenFileName(this, "Select a project file", prevWorking, "Project (*.prj)");
				if (file.isEmpty()) return "";
				return file;
			}
		}();

		if (projectFile.isEmpty()) {
			QMessageBox::warning(this, "Open Project File", "No project file is selected");
			return;
		}

		const auto workingDirectory = QFileInfo(projectFile).absolutePath();
		QSettings().setValue(d->entry.recentWorkingFolder, workingDirectory);

		auto createInteractor = CBCResearch::Interactor::CreateWorkset();
		createInteractor.Request(workingDirectory);

		auto project = ProjectFile::GetInstance();
		project->Load(projectFile);

		auto sourceDirectory = project->GetSourceDirectory();
		QSettings().setValue(d->entry.recentSourceFolder, sourceDirectory);

		auto scanPresenter = CBCResearch::Interactor::TCFListPresenter(d->listUpdater);
		auto scanner = CBCResearch::Plugins::TCFScanner();
		auto maskReader = Plugins::MaskDataReader();

		auto scanInteractor = CBCResearch::Interactor::ScanDirectory(&scanner, &scanPresenter, &maskReader);
		scanInteractor.Request(sourceDirectory);
    }

    void MainWindow::onFileSelected(int index) {
		d->currenIndex = index;
		d->hidMaskAction->setChecked(true);

		auto volumePresenter = Interactor::VolumeDataPresenter(d->volumeDataUpdater);
		auto loadVolumeInteractor = Interactor::LoadVolumeData(&volumePresenter);
		if (loadVolumeInteractor.Request(index)) {
			auto maskReader = Plugins::MaskDataReader();
			auto maskPresenter = Interactor::MaskDataPresenter(d->maskDataUpdater);
			auto loadMaskInteractor = Interactor::LoadMaskData(&maskReader, &maskPresenter);
			if(!loadMaskInteractor.Request(index, false)) {
				auto segment = Plugins::SegmentCells();
				auto writer = Plugins::MaskDataWriter();
				auto segmentInteractor = Interactor::SegmentCellsController(&segment, &writer, &maskPresenter);
				if (!segmentInteractor.Request(index)) {
					QMessageBox::warning(this, "Segmentation", "It fails to segment cells");
				}
			}
		} else {
			QMessageBox::warning(this, "Load data", "It fails to load a volume data");
		}
	}

    void MainWindow::onListFilterChanged(int filter) {
		auto presenter = CBCResearch::Interactor::TCFListPresenter(d->listUpdater);
		auto interactor = CBCResearch::Interactor::GetFilteredList(&presenter);

		if (filter < 0) {
			interactor.Request(false);
		} else {
			auto type = Plugins::CBCAnnotation::Convert(filter, Plugins::CBCAnnotation::TypeCode::Excluded);
			interactor.Request(true, type);
		}
    }

    void MainWindow::onQuit() {
		qApp->quit();
	}

	void MainWindow::onRIThresholdChanged(int index, double minValue, double maxValue) {
		if (index == 0) {
			QSettings().setValue(d->entry.riThresholdGrayMin, minValue);
			QSettings().setValue(d->entry.riThresholdGrayMax, maxValue);
		}
		else {
			QSettings().setValue(d->entry.riThresholdColorMin, minValue);
			QSettings().setValue(d->entry.riThresholdColorMax, maxValue);
		}
	}

    void MainWindow::onBoundingBoxSelected(int index) {
		if (d->annotationActionGroup == nullptr) return;

		auto writer = Plugins::MaskDataWriter();
		auto presenter = Interactor::AnnotatePointPresenter(d->maskDataUpdater);
		auto controller = Interactor::AnnotatePointController(&writer, &presenter);

		auto action = d->annotationActionGroup->checkedAction();

		auto type = Plugins::CBCAnnotation::Convert(action->data().toInt(), Plugins::CBCAnnotation::TypeCode::Excluded);
		auto code = Plugins::CBCAnnotation::Convert(type);

		if (!controller.Request(index, code)) {
			QMessageBox::warning(this, "Annotation", "It fails to annotate the cell");
		}
    }

    void MainWindow::onSelectBFAsDefault() {
		QSettings().setValue(d->entry.viewModeRightImage, 0);
		d->ui->cbcview->SetBFAsDefault();
    }

    void MainWindow::onSelectHTAsDefault() {
		QSettings().setValue(d->entry.viewModeRightImage, 1);
		d->ui->cbcview->SetHTAsDefault();
    }

    void MainWindow::onSetTriggerMode(QAction* action) {
		const auto data = action->data().toInt();
		if (data == -1) {
			d->ui->cbcview->ShowBoundingBox(false);
		} else {
			d->ui->cbcview->ShowBoundingBox(true);
		}
    }

    void MainWindow::onSetMaskMode(QAction* action) {
		const auto data = action->data().toInt();
		const auto visibility = static_cast<Impl::MaskVisiblity>(data);

		if (visibility == Impl::MaskVisiblity::HIDE) {
			d->ui->cbcview->ShowMask(false);
		} else {
			if (!d->ui->cbcview->IsMaskLoaded()) {
				auto maskReader = Plugins::MaskDataReader();
				auto maskPresenter = Interactor::MaskDataPresenter(d->maskDataUpdater);
				auto loadMaskInteractor = Interactor::LoadMaskData(&maskReader, &maskPresenter);
				if (!loadMaskInteractor.Request(d->currenIndex, true)) {
					d->ui->cbcview->ShowMask(false);
				}
			}

			d->ui->cbcview->ShowMask(true);
		}
    }

    void MainWindow::onCalculateCBC() {
		auto calculator = Plugins::CBCCalculator();
		auto presenter = Interactor::UpdateCBCResultPresenter(d->resultUpdater);
		auto controller = Interactor::CalcaulateCBCController(&calculator, &presenter);
		if (!controller.Request()) {
			QMessageBox::warning(this, "CBC Calculation", "It fails to calculate CBC data");
			return;
		}

		statusBar()->showMessage("CBC Calculation is finished", 10000);
    }

    void MainWindow::onExportAnnotationAll() {
		auto path = QFileDialog::getSaveFileName(this, "Export all annotation result", "", "CSV (*.csv)");
		if (path.isEmpty()) return;

		auto maskReader = Plugins::MaskDataReader();
		auto exporter = Plugins::CBCAnnotationExporter();
		auto controller = Interactor::ExportAnnotation(&maskReader, &exporter);
		if (!controller.Request(path, false)) {
			QMessageBox::warning(this, "Export Annotation Result", "Failed to export annotation result");
			return;
		}

		statusBar()->showMessage(QString("Exported to %1").arg(path), 10000);
    }

    void MainWindow::onExportAnnotationWBC() {
		auto path = QFileDialog::getSaveFileName(this, "Export all annotation result", "", "CSV (*.csv)");
		if (path.isEmpty()) return;

		auto maskReader = Plugins::MaskDataReader();
		auto exporter = Plugins::CBCAnnotationExporter();
		auto controller = Interactor::ExportAnnotation(&maskReader, &exporter);
		if (!controller.Request(path, true)) {
			QMessageBox::warning(this, "Export Annotation Result", "Failed to export annotation result");
			return;
		}

		statusBar()->showMessage(QString("Exported to %1").arg(path), 10000);
    }

	void MainWindow::onSelectColormapTurbo() {
		const CBCResearch::Plugins::CBCViewerColormap colormap = CBCResearch::Plugins::CBCViewerColormap::Turbo;
		QSettings().setValue(d->entry.colormapRightImage, colormap._to_integral());
		d->ui->cbcview->SetColormap(colormap);
	}

	void MainWindow::onSelectColormapJet() {
		const CBCResearch::Plugins::CBCViewerColormap colormap = CBCResearch::Plugins::CBCViewerColormap::Jet;
		QSettings().setValue(d->entry.colormapRightImage, colormap._to_integral());
		d->ui->cbcview->SetColormap(colormap);
	}

	void MainWindow::onSelectColormapHot() {
		const CBCResearch::Plugins::CBCViewerColormap colormap = CBCResearch::Plugins::CBCViewerColormap::Hot;
		QSettings().setValue(d->entry.colormapRightImage, colormap._to_integral());
		d->ui->cbcview->SetColormap(colormap);
	}

	void MainWindow::onSelectColormapRainbow() {
		const CBCResearch::Plugins::CBCViewerColormap colormap = CBCResearch::Plugins::CBCViewerColormap::Rainbow;
		QSettings().setValue(d->entry.colormapRightImage, colormap._to_integral());
		d->ui->cbcview->SetColormap(colormap);
	}

	void MainWindow::onSelectColormapInverseGreyscale() {
		const CBCResearch::Plugins::CBCViewerColormap colormap = CBCResearch::Plugins::CBCViewerColormap::InverseGreyscale;
		QSettings().setValue(d->entry.colormapRightImage, colormap._to_integral());
		d->ui->cbcview->SetColormap(colormap);
	}

	void MainWindow::onSetThresholdOffset() {
		const auto offset = QSettings().value(d->entry.thresholdOffset, 0).toDouble();
		bool ok = false;
		const auto newOffset = QInputDialog::getDouble(this, "Threshold Offset", "Offset", offset, 0, 0.1, 4, &ok);
		if (ok && newOffset != offset) {
			QSettings().setValue(d->entry.thresholdOffset, newOffset);
		}
	}
}
