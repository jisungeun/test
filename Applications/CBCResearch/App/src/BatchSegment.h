#pragma once

#include <memory>
#include <QList>

namespace CBCResearch {
    class BatchSegment {
    public:
        BatchSegment(uint16_t offset = 0);
        virtual ~BatchSegment();

        auto Run(QList<int> indexes)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}