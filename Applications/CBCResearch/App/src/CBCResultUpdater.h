#pragma once

#include <memory>
#include <ICBCResultWidget.h>
#include <CBCReportPanel.h>

namespace CBCResearch {
    class CBCResultUpdater : public Interactor::ICBCResultWidget {
    public:
        CBCResultUpdater(Plugins::CBCReportPanel* widget = nullptr);
        virtual ~CBCResultUpdater();

        auto Update(List& list) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}