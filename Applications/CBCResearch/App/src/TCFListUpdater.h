#pragma once

#include <memory>
#include <TCFListPanel.h>
#include <ITCFListWidget.h>

namespace CBCResearch {
    class TCFListUpdater : public Interactor::ITCFListWidget {
    public:
        TCFListUpdater(Plugins::TCFListPanel* widget = nullptr);
        virtual ~TCFListUpdater();

        auto BeginUpdate() -> void override;
        auto EndUpdate() -> void override;
        auto Clear() -> void override;
        auto AddItem(int index, const QString& path, Status status) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}