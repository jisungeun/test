#pragma once

#include <memory>

#include <QMainWindow>

namespace Utilities {
	class SystemID : public QMainWindow {
	public:
		explicit SystemID(QWidget* parent = nullptr);
		~SystemID() override;

	protected slots:
		auto OnButtonClicked() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}