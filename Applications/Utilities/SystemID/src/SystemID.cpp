#include <QClipboard>

#include "SystemID.h"
#include "VolumeSerialNumber.h"

#include "ui_SystemID.h"

namespace Utilities {
	struct SystemID::Impl {
		Ui::SystemID ui;
	};

	SystemID::SystemID(QWidget* parent) : QMainWindow(parent), d(new Impl) {
		setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::MSWindowsFixedSizeDialogHint);

		d->ui.setupUi(this);
		char driveLetter = 'C';

		if (const auto* env = getenv("SystemDrive"); env && std::strlen(env) > 0)
			driveLetter = env[0];

		if (const auto number = TC::VolumeSerialNumber::Read(driveLetter))
			d->ui.lineEdit->setText(*number);
		else
			d->ui.lineEdit->setText("NaN");

		connect(d->ui.pushButton, &QPushButton::clicked, this, &SystemID::OnButtonClicked);
	}

	SystemID::~SystemID() = default;

	auto SystemID::OnButtonClicked() -> void {
		QApplication::clipboard()->setText(d->ui.lineEdit->text());
	}
}
