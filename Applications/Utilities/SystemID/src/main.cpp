#include <QApplication>

#include "SystemID.h"

int main(int argc, char** argv) {
	QApplication app(argc, argv);

	Utilities::SystemID reader;
	reader.show();

	QApplication::exec();
}