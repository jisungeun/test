#include <QApplication>

#include "TsxOtpGenerator.h"

int main(int argc, char** argv) {
	QApplication app(argc, argv);

	Utilities::TsxOtpGenerator gen;
	gen.show();

	app.exec();
}