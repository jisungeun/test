#include <QApplication>
#include <QDate>
#include <QFrame>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>

#include "TsxOtpGenerator.h"

#include "OtpGenerator.h"
#include "ui_tsxotpgenerator.h"

namespace Utilities {
	struct TsxOtpGenerator::Impl {
		Ui::TsxOtpGenerator ui;

		const int visibleOtpCount = 1;
		const int otpLength = 8;
		const QString otpKey = "9z$C&F)J@NcRfUjXn2r5u7x!A%D*G-KaPdSgVkYp3s6v9y/B?E(H+MbQeThWmZq4";

		auto AddOtp(const QString& year, const QString& otp) -> void {
			auto *frame = new QFrame;
			auto* layout = new QHBoxLayout;
			auto* label = new QLabel(year);
			auto* line = new QLineEdit(otp);
			
			line->setReadOnly(true);
			frame->setLayout(layout);
			layout->addWidget(label);
			layout->addWidget(line);

			ui.frame->layout()->addWidget(frame);
		}
	};

	TsxOtpGenerator::TsxOtpGenerator(QWidget* parent) : QMainWindow(parent), d(new Impl) {
		d->ui.setupUi(this);

		const auto year = QDate::currentDate().year();
		TC::OtpGenerator generator(d->otpLength, d->otpKey);

		for(int i = year - d->visibleOtpCount; i < year + d->visibleOtpCount + 1; i++) {
			auto plain = QString::number(i);
			auto otp = generator.Generate(plain);

			d->AddOtp(plain, otp);
		}
	}

	TsxOtpGenerator::~TsxOtpGenerator() = default;
}