#pragma once

#include <memory>

#include <QMainWindow>

namespace Utilities {
	class TsxOtpGenerator : public QMainWindow {
	public:
		TsxOtpGenerator(QWidget* parent = nullptr);
		~TsxOtpGenerator() override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}