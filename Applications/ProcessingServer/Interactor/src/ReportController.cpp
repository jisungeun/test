#include "ReportController.h"

#include "ProgressConsolePresenter.h"
#include "ProgressHostPresenter.h"
#include "ProgressReporter.h"

namespace processing_server::Interactor {
    struct ReportController::Impl {
        Impl() = default;
        ~Impl() = default;
    };

    ReportController::ReportController()
        :d(new Impl()) {
    }

    ReportController::~ReportController() = default;

    auto ReportController::Report() const -> ProgressConsoleList {
        const auto progressConsolePresenter = std::make_shared<ProgressConsolePresenter>();
        UseCase::ProgressReporter progressReporter(progressConsolePresenter);
        progressReporter.Report();

        const auto progressConsoleList = progressConsolePresenter->GetProgressConsoleList();

        return progressConsoleList;
    }

    auto ReportController::ReportHost() const -> ProgressList {
        const auto progressHostPresenter = std::make_shared<ProgressHostPresenter>();

        UseCase::ProgressReporter progressReporter(progressHostPresenter);
        progressReporter.Report();

        const auto progressConsoleList = progressHostPresenter->GetProgressHostList();

        return progressConsoleList;
    }
}
