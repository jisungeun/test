#include "ProgressList.h"

#include <QList>
#include <QMap>

namespace processing_server::Interactor {
    struct ProgressList::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        QList<QString> dataPaths;
        QList<float> progresses;
    };

    ProgressList::ProgressList()
        : d(new Impl()) {
    }

    ProgressList::ProgressList(const ProgressList& other)
        : d(std::make_unique<Impl>(*other.d)) {
    }

    ProgressList::~ProgressList() = default;

    auto ProgressList::operator=(const ProgressList & other) -> ProgressList& {
        d = std::make_unique<Impl>(*other.d);
        return *this;
    }

    auto ProgressList::PushBack(const QString & dataPath, const float& progress) -> void {
        d->dataPaths.push_back(dataPath);
        d->progresses.push_back(progress);
    }

    auto ProgressList::Size() const -> size_t {
        return d->dataPaths.size();
    }

    auto ProgressList::GetDataPath(const int32_t & index) const -> QString {
        return d->dataPaths.at(index);
    }

    auto ProgressList::GetProgress(const int32_t & index) const -> float {
        return d->progresses.at(index);
    }

    auto ProgressList::UpdateProgress(const QString& dataPath, const float& progress) -> void {
        const auto dataPathIndex = FindDataPathIndex(dataPath);
        if (dataPathIndex >= 0) {
            d->progresses.replace(dataPathIndex, progress);
        } else {
            PushBack(dataPath, progress); 
        }
    }

    auto ProgressList::RemoveProgress(const QString& dataPath) -> void {
        const auto dataPathIndex = FindDataPathIndex(dataPath);
        d->progresses.removeAt(dataPathIndex);
        d->dataPaths.removeAt(dataPathIndex);
    }

    auto ProgressList::FindDataPathIndex(const QString& dataPath) -> int32_t {
        auto& dataPathList = d->dataPaths;

        for (auto index = 0; index < dataPathList.size(); ++index) {
            if(dataPathList.at(index) == dataPath) {
                return index;
            }
        }
        return -1;
    }
}
