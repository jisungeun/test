#include "ProgressConsolePresenter.h"

namespace processing_server::Interactor {
    struct ProgressConsolePresenter::Impl {
        Impl() = default;
        ~Impl() = default;

        UseCase::ProgressData progressData;
    };

    ProgressConsolePresenter::ProgressConsolePresenter()
        : d(new Impl()) {
    }

    ProgressConsolePresenter::~ProgressConsolePresenter() = default;

    auto ProgressConsolePresenter::SetProgressData(const UseCase::ProgressData& progressData) -> void {
        d->progressData = progressData;
    }

    auto ProgressConsolePresenter::GetProgressData() -> UseCase::ProgressData {
        return d->progressData;
    }

    auto ProgressConsolePresenter::GetProgressConsoleList() const -> ProgressConsoleList {
        const auto numberOfData = d->progressData.Size();

        ProgressConsoleList progressConsoleList;
        for(auto i = 0; i< numberOfData;++i) {
            const auto dataPath = d->progressData.GetDataPath(i);
            const auto progress = d->progressData.GetProgress(i);
            progressConsoleList.PushBack(dataPath, progress);
        }
        return progressConsoleList;
    }
}
