#define LOGGER_TAG "ProcessingServerInteractor::AddController"

#include <TCLogger.h>

#include "AddController.h"

#include "JobAdder.h"

namespace processing_server::Interactor {
    AddController::AddController() = default;
    AddController::~AddController() = default;

    auto AddController::AddOneJob(const UseCase::InputJobContents& inputJobContents)->void {
        const auto dataPath = inputJobContents.GetDataPath();
        const auto dataType = QString(inputJobContents.GetDataType()._to_string());
        const auto timeFrameIndex = QString("%1").arg(inputJobContents.GetTimeFrameIndex(), 6, 10, QChar('0'));
        QLOG_INFO() << QString("Job Added : %1 %2 %3").arg(dataPath).arg(dataType).arg(timeFrameIndex);

        UseCase::JobAdder jobAdder;
        jobAdder.Add(inputJobContents);
    }
}
