#include "ProgressConsoleList.h"

#include <deque>

namespace processing_server::Interactor {
    struct ProgressConsoleList::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        std::deque<QString> dataPaths;
        std::deque<float> progresses;
    };

    ProgressConsoleList::ProgressConsoleList()
        : d(new Impl()) {
    }

    ProgressConsoleList::ProgressConsoleList(const ProgressConsoleList& other) 
        : d(std::make_unique<Impl>(*other.d)) {
    }

    ProgressConsoleList::~ProgressConsoleList() = default;

    auto ProgressConsoleList::operator=(const ProgressConsoleList& other) -> ProgressConsoleList& {
        d = std::make_unique<Impl>(*other.d);
        return *this;
    }

    auto ProgressConsoleList::PushBack(const QString& dataPath, const float& progress) -> void {
        d->dataPaths.push_back(dataPath);
        d->progresses.push_back(progress);
    }

    auto ProgressConsoleList::Size() const -> size_t {
        return d->dataPaths.size();
    }

    auto ProgressConsoleList::GetDataPath(const int32_t& index) const -> QString {
        return d->dataPaths.at(index);
    }

    auto ProgressConsoleList::GetProgress(const int32_t& index) const -> float {
        return d->progresses.at(index);
    }
}
