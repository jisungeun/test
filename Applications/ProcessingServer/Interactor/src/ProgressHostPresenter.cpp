#include "ProgressHostPresenter.h"

namespace processing_server::Interactor {
    struct ProgressHostPresenter::Impl {
        Impl() = default;
        ~Impl() = default;

        UseCase::ProgressData progressData;
    };

    ProgressHostPresenter::ProgressHostPresenter()
        : d(new Impl()) {
    }

    ProgressHostPresenter::~ProgressHostPresenter() = default;

    auto ProgressHostPresenter::SetProgressData(const UseCase::ProgressData & progressData) -> void {
        d->progressData = progressData;
    }

    auto ProgressHostPresenter::GetProgressData() -> UseCase::ProgressData {
        return d->progressData;
    }

    auto ProgressHostPresenter::GetProgressHostList() const -> ProgressList {
        const auto numberOfData = d->progressData.Size();

        ProgressList progressHostList;
        for (auto i = 0; i < numberOfData; ++i) {
            const auto dataPath = d->progressData.GetDataPath(i);
            const auto progress = d->progressData.GetProgress(i);
            progressHostList.PushBack(dataPath, progress);
        }
        return progressHostList;
    }
}
