#pragma once

#include "IProgressOutputPort.h"

#include "ProcessingServerInteractorExport.h"
#include "ProgressList.h"

namespace processing_server::Interactor {
    class ProcessingServerInteractor_API ProgressHostPresenter : public UseCase::IProgressOutputPort {
    public:
        ProgressHostPresenter();
        ~ProgressHostPresenter();

        auto SetProgressData(const UseCase::ProgressData& progressData) -> void override;
        auto GetProgressData()->UseCase::ProgressData override;

        auto GetProgressHostList() const->ProgressList;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
