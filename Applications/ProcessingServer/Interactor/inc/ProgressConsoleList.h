#pragma once
#include <memory>

#include <QString>

#include "ProcessingServerInteractorExport.h"

namespace processing_server::Interactor {
    class ProcessingServerInteractor_API ProgressConsoleList {
    public:
        ProgressConsoleList();
        ProgressConsoleList(const ProgressConsoleList& other);
        ~ProgressConsoleList();

        auto operator=(const ProgressConsoleList& other)->ProgressConsoleList&;

        auto PushBack(const QString& dataPath, const float& progress)->void;

        auto Size() const->size_t;
        auto GetDataPath(const int32_t& index) const->QString;
        auto GetProgress(const int32_t& index) const ->float;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
