#pragma once
#include <memory>

#include "IProgressOutputPort.h"

#include "ProcessingServerInteractorExport.h"
#include "ProgressConsoleList.h"

namespace processing_server::Interactor {
    class ProcessingServerInteractor_API ProgressConsolePresenter final : public UseCase::IProgressOutputPort {
    public:
        ProgressConsolePresenter();
        ~ProgressConsolePresenter();

        auto SetProgressData(const UseCase::ProgressData& progressData) -> void override;
        auto GetProgressData() -> UseCase::ProgressData override;

        auto GetProgressConsoleList() const->ProgressConsoleList;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

