#pragma once
#include <memory>

#include "ProcessingServerInteractorExport.h"
#include "ProgressConsoleList.h"
#include "ProgressList.h"

namespace processing_server::Interactor {
    class ProcessingServerInteractor_API ReportController {
    public:
        ReportController();
        ~ReportController();

        auto Report() const ->ProgressConsoleList;
        auto ReportHost() const->ProgressList;

    private:
        struct Impl;
        std::shared_ptr<Impl> d;
    };
}
