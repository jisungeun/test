#pragma once

#include <memory>
#include <QString>

#include "ProcessingServerInteractorExport.h"

namespace processing_server::Interactor {
    class ProcessingServerInteractor_API ProgressList {
    public:
        ProgressList();
        ProgressList(const ProgressList& other);
        ~ProgressList();

        auto operator=(const ProgressList& other)->ProgressList&;

        auto PushBack(const QString& dataPath, const float& progress)->void;

        auto Size() const->size_t;
        auto GetDataPath(const int32_t& index) const->QString;
        auto GetProgress(const int32_t& index) const ->float;
        auto UpdateProgress(const QString& dataPath, const float& progress)->void;
        auto RemoveProgress(const QString& dataPath)->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        auto FindDataPathIndex(const QString& dataPath)->int32_t;
    };
}
