#pragma once

#include "JobAdder.h"
#include "InputJobContents.h"

#include "ProcessingServerInteractorExport.h"

namespace processing_server::Interactor {
    class ProcessingServerInteractor_API AddController {
    public:
        AddController();
        ~AddController();
        static auto AddOneJob(const UseCase::InputJobContents& inputJobContents)->void;
    };
}
