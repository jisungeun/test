function Component()
{
}

Component.prototype.createOperations = function()
{
	//call default implementation
    component.createOperations();

	//add shortcuts on start menu
    component.addOperation("CreateShortcut",
						   "@TargetDir@/bin/TomoProcessingServer.exe",
						   "@StartMenuDir@/TomoProcessingServer.lnk",
						   "workingDirectory=@TargetDir@/bin");
			
	component.addOperation("CreateShortcut",
						   "@TargetDir@/MaintenanceTool.exe",
						   "@StartMenuDir@/Uninstall.lnk",
						   " -- uninstall");
			
	component.addOperation("CreateShortcut",
						   "@TargetDir@/bin/Tomocube.url",
						   "@StartMenuDir@/Tomocube Inc.lnk");
                          
    //add shortcuts on desktop                          
    component.addOperation("CreateShortcut",
						   "@TargetDir@/bin/TomoProcessingServer.exe",
						   "@DesktopDir@/TomoProcessingServer.lnk",
						   "workingDirectory=@TargetDir@/bin");
}
