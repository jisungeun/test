#include <QTextCodec>
#include <TCLogger.h>

#include "RemoteClient.h"

RemoteClient::RemoteClient(QObject* parent)
	: QThread(parent)
	, m_nPort(0)
	, m_pSocket(0)
	, m_bRunning(true)
	, m_bNewDataReady(false)
{
	openServer();
	start();
}

RemoteClient::RemoteClient(int nPort, QObject* parent)
	: QThread(parent)
	, m_nPort(nPort)
	, m_pSocket(0)
	, m_bRunning(true)
	, m_bNewDataReady(false)
{
	openServer();
	start();
}

RemoteClient::~RemoteClient(void)
{
	m_bRunning = false;
	m_wait.wakeAll();
	wait();
}

RemoteClient::Pointer RemoteClient::GetInstance(int nPort, QObject* parent)
{
	static RemoteClient::Pointer theInstance(new RemoteClient(parent));
	return theInstance;
}

void RemoteClient::openServer(void)
{
	m_pServer = new QTcpServer(this);
	connect(m_pServer, SIGNAL(newConnection()), this, SLOT(reqMakeConnection()));

	QString ipAddress = QHostAddress(QHostAddress::LocalHost).toString();

	if (m_nPort != 0)
	{
		if (!m_pServer->listen(QHostAddress::Any, m_nPort))
		{
			QLOG_INFO() << "Remote : Port (" << m_nPort << ") is already occupied.";

			if (!m_pServer->listen())
			{
				QLOG_ERROR() << "Remote : Server fails to listen";
				return;
			}
		}
	}
	else if (!m_pServer->listen())
	{
		QLOG_ERROR() << "Remote : Server fails to listen";
		return;
	}

	QLOG_INFO() << "Remote : The server is running on IP: " << ipAddress << " port: " << m_pServer->serverPort();
}

bool RemoteClient::isReady(void) const
{
	return m_pServer->isListening();
}

unsigned short RemoteClient::getPort(void)
{
	return m_pServer->serverPort();
}

bool RemoteClient::isConnected(void) const
{
	if (!m_pSocket) return false;
	return m_pSocket->isOpen();
}

bool RemoteClient::sendResponse(const QString& strResponse)
{
	if (!m_pSocket) return false;

	const int nToBeSent = strResponse.length();
	int nSent = 0;
	while (nSent < nToBeSent)
	{
		QString strRespNow = strResponse.mid(nSent, (nToBeSent - nSent));
		int nSentNow = m_pSocket->write(QTextCodec::codecForName("UTF-8")->fromUnicode(strRespNow));
		if (nSentNow == -1) break;

		nSent = nSent + nSentNow;
		m_pSocket->flush();
	}

	return true;
}

bool RemoteClient::isCompletedPacket(QByteArray& line)
{
	return line.contains('}');
}

void RemoteClient::run(void)
{
	while (m_bRunning)
	{
		QMutexLocker locker(&m_mutex);
		if (!m_bNewDataReady) m_wait.wait(&m_mutex, 500);
		if (m_bNewDataReady)
		{
			parse(m_baNewData);
			m_bNewDataReady = false;
		}
	}
}

void RemoteClient::parse(QByteArray& line)
{
	QString strLine = QTextCodec::codecForName("UTF-8")->toUnicode(line);

	int idx = strLine.lastIndexOf(QChar('{'), -1);	// use lastIndexOf instead of indexOf. e.g. {status}{status}{status}
	strLine = strLine.remove(0, idx + 1);

	QString strCmd, strOption;
	idx = strLine.indexOf(QChar(' '), 0);
	if (idx != -1)
	{
		strCmd = strLine.mid(0, idx);
		strLine.remove(0, idx + 1);
		idx = strLine.indexOf(QChar('}'), 0);
		strOption = strLine.mid(0, idx);
	}
	else
	{
		strLine.remove('}');
		strCmd = strLine;
	}

	bool bValidCommand = true;

	if (strCmd.compare("status", Qt::CaseInsensitive) != 0) QLOG_INFO() << "RemoteClient: " << strLine;

	do
	{
		if (strCmd.compare("status", Qt::CaseInsensitive) == 0)
		{
			emit sigReportStatus();
		}
		else if (strCmd.compare("stop", Qt::CaseInsensitive) == 0)
		{
			emit sigStopProcess();
		}
		else if (strCmd.compare("process", Qt::CaseInsensitive) == 0)
		{
			emit sigProcess(strOption);
		}
		else if (strCmd.compare("client", Qt::CaseInsensitive) == 0)
		{
			QStringList toks = strOption.split('|');
			if (toks.size() < 1) break;

			QStringList param = toks.at(0).split('=');
			if (param.size() != 2) continue;
			const bool isBusy = (param.at(1).compare("busy", Qt::CaseInsensitive) == 0);

			emit sigClientStatus(isBusy);
		}
		else if (strCmd.compare("update_list", Qt::CaseInsensitive) == 0)
		{
			emit sigUpdateList();
		}
		else if (strCmd.compare("show", Qt::CaseInsensitive) == 0)
		{
			emit sigShow();
		}
		else
		{
			bValidCommand = false;
		}
	} while (0);
	

	if(!bValidCommand)
	{
		QLOG_ERROR() << "Invalid: " << line.toStdString().c_str();
	}
}

void RemoteClient::reqMakeConnection(void)
{
	if (m_pSocket)
	{
		QLOG_INFO() << "There is existing connection";
		return;
	}

	QLOG_INFO() << "New remote client is connected.";

	m_pSocket = m_pServer->nextPendingConnection();
	m_pSocket->setSocketOption(QAbstractSocket::LowDelayOption, 1);

	connect(m_pSocket, SIGNAL(disconnected()), this, SLOT(reqDisconnected()));
	connect(m_pSocket, SIGNAL(readyRead()), this, SLOT(reqReadData()), Qt::DirectConnection);

	emit sigConnected(true);
}

void RemoteClient::reqDisconnected(void)
{
	QLOG_INFO() << "Remote : Client is disconnected";

	m_pSocket->disconnectFromHost();
	m_pSocket->deleteLater();
	m_pSocket = 0;

	emit sigConnected(false);
}

void RemoteClient::reqReadData(void)
{
	static QByteArray line;

	while (m_pSocket->bytesAvailable())
	{
		line += m_pSocket->readAll();
	}

	if (isCompletedPacket(line))
	{
		m_bNewDataReady = true;
		m_baNewData = line;
		line.clear();

		m_wait.wakeAll();
	}
}