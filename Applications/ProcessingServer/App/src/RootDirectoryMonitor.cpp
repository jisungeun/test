#include "RootDirectoryMonitor.h"

#include <QFileSystemWatcher>
#include <QMap>
#include <QDir>

#include "DataDirectoryMonitorHt.h"
#include "DataDirectoryMonitorFl.h"
#include "DataDirectoryMonitorBf.h"
#include "DataDirectoryMonitorPhase.h"

#include "JobParameter.h"
#include "TomoBuilderUtility.h"

namespace processing_server {
    struct RootDirectoryMonitor::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;
        QString rootDirectoryPath{};
        DataDirectoryMonitorHt::Pointer dataHtMonitor{};
        DataDirectoryMonitorFl::Pointer dataFlMonitor{};
        DataDirectoryMonitorBf::Pointer dataBfMonitor{};
        DataDirectoryMonitorPhase::Pointer dataPhaseMonitor{};

        bool htIncluded{ false };
        bool flBlueIncluded{ false };
        bool flGreenIncluded{ false };
        bool flRedIncluded{ false };
        bool bfIncluded{ false };
        bool phaseIncluded{ false };

        QMap<QString, QString> configMap;
        JobParameter jobParameter;
        int32_t timeFrameNumber{ 0 };
    };

    RootDirectoryMonitor::RootDirectoryMonitor(const QString& rootDirectoryPath)
        : d(new Impl()) {
        Initialize(rootDirectoryPath);
    }

    RootDirectoryMonitor::RootDirectoryMonitor(const RootDirectoryMonitor& other)
        : d(new Impl(*other.d)) {
    }

    RootDirectoryMonitor::~RootDirectoryMonitor() = default;

    auto RootDirectoryMonitor::IsChanged() const -> bool {
        auto htChanged{ false }, flChanged{ false }, bfChanged{ false }, phaseChanged{ false };

        if (d->htIncluded) {
            htChanged = d->dataHtMonitor->ScanChanged();
        }
        if (d->bfIncluded) {
            bfChanged = d->dataBfMonitor->ScanChanged();
        }
        const auto flIncluded = d->flBlueIncluded || d->flGreenIncluded || d->flRedIncluded;
        if (flIncluded) {
            flChanged = d->dataFlMonitor->ScanChanged();
        }
        if (d->phaseIncluded) {
            phaseChanged = d->dataPhaseMonitor->ScanChanged();
        }

        return htChanged || flChanged || bfChanged || phaseChanged;
    }

    auto RootDirectoryMonitor::GetNewInputJobContents() const -> QList<UseCase::InputJobContents> {
        const auto dataDirectoryContentsList = GetNewDataDirectoryContents();
        const auto inputJobContentsList = ConvertDataToJobContents(dataDirectoryContentsList);
        return inputJobContentsList;
    }

    auto RootDirectoryMonitor::IsCompleted() const -> bool {
        auto htComplete{ true }, flComplete{ true }, bfComplete{ true }, phaseComplete{ true };

        if (d->htIncluded) {
            htComplete = d->dataHtMonitor->IsCompleted();
        }
        if (d->bfIncluded) {
            bfComplete = d->dataBfMonitor->IsCompleted();
        }
        const auto flIncluded = d->flBlueIncluded || d->flGreenIncluded || d->flRedIncluded;
        if (flIncluded) {
            flComplete = d->dataFlMonitor->IsCompleted();
        }
        if (d->phaseIncluded) {
            phaseComplete = d->dataPhaseMonitor->IsCompleted();
        }

        return htComplete && flComplete && bfComplete && phaseComplete;
    }

    auto RootDirectoryMonitor::Initialize(const QString& rootDirectoryPath) -> void {
        d->rootDirectoryPath = rootDirectoryPath;

        SetConfigMapAndJobParameter();
        d->timeFrameNumber = ReadTimeFrameNumber();
        SetDataIncluded();

        SetDataDirectoryMonitors();
    }

    auto RootDirectoryMonitor::SetConfigMapAndJobParameter() -> void {
        const auto configPath = QString("%1/config.dat").arg(d->rootDirectoryPath);
        const auto jobParameterPath = QString("%1/JobParameter.tcp").arg(d->rootDirectoryPath);

        TC::LoadFullParameter(configPath, d->configMap);
        d->jobParameter = JobParameter(jobParameterPath);
    }

    auto RootDirectoryMonitor::ReadTimeFrameNumber() const -> int32_t {
        const auto htDataNumber = DataNumber("Images HT3D");
        const auto flDataNumber = DataNumber("Images FL3D");
        const auto bfDataNumber = DataNumber("Images BF");
        const auto phaseDataNumber = DataNumber("Images HT2D");

        //TODO : apply TimeFrameNumber for each DataType
        const auto timeFrameNumber =
            std::max(std::max(htDataNumber, flDataNumber), std::max(bfDataNumber, phaseDataNumber));

        return timeFrameNumber;
    }

    auto RootDirectoryMonitor::DataNumber(const QString& dataString) const -> int32_t {
        auto dataNumber{ -1 };
        if (d->configMap.contains(dataString)) {
            dataNumber = d->configMap[dataString].toInt();
        }
        return dataNumber;
    }

    auto RootDirectoryMonitor::SetDataIncluded() -> void {
        d->htIncluded = DataIncluded("Images HT3D");
        d->bfIncluded = DataIncluded("Images BF");
        d->phaseIncluded = DataIncluded("Images HT2D");

        d->flBlueIncluded = FlIncluded(0);
        d->flGreenIncluded = FlIncluded(1);
        d->flRedIncluded = FlIncluded(2);
    }

    auto RootDirectoryMonitor::DataIncluded(const QString& dataString) const -> bool {
        const auto dataNumber = DataNumber(dataString);
        const auto dataIncluded = dataNumber > 0;
        return dataIncluded;
    }

    auto RootDirectoryMonitor::FlIncluded(const int32_t& channelIndex) const -> bool {
        auto dataIncluded{ false };
        const auto outBoundedChannelIndex = (channelIndex < 0) || (channelIndex > 2);
        if (outBoundedChannelIndex) {
            return dataIncluded;
        }

        const auto flNumber = DataNumber("Images FL3D");
        const auto flExists = flNumber > 0;

        if (flExists) {
            if (channelIndex == 0) {
                dataIncluded = d->jobParameter.FLEnableChannel(TC::FLChannel::CHANNEL_BLUE);
            } else if (channelIndex == 1) {
                dataIncluded = d->jobParameter.FLEnableChannel(TC::FLChannel::CHANNEL_GREEN);
            } else if (channelIndex == 2) {
                dataIncluded = d->jobParameter.FLEnableChannel(TC::FLChannel::CHANNEL_RED);
            }
        }

        return dataIncluded;
    }

    auto RootDirectoryMonitor::GetNewDataDirectoryContents() const -> QList<DataDirectoryContents> {
        QList<DataDirectoryContents> dataDirectoryContentsList;

        if (d->htIncluded) {
            const auto dataDirectoryContentsHt = d->dataHtMonitor->GetNewDataDirectoryContents();
            dataDirectoryContentsList.append(dataDirectoryContentsHt);
        }
        if (d->bfIncluded) {
            const auto dataDirectoryContentsBf = d->dataBfMonitor->GetNewDataDirectoryContents();
            dataDirectoryContentsList.append(dataDirectoryContentsBf);
        }
        const auto flIncluded = d->flBlueIncluded || d->flGreenIncluded || d->flRedIncluded;
        if (flIncluded) {
            const auto dataDirectoryContentsFl = d->dataFlMonitor->GetNewDataDirectoryContents();
            dataDirectoryContentsList.append(dataDirectoryContentsFl);
        }
        if (d->phaseIncluded) {
            const auto dataDirectoryContentsPhase = d->dataPhaseMonitor->GetNewDataDirectoryContents();
            dataDirectoryContentsList.append(dataDirectoryContentsPhase);
        }

        return dataDirectoryContentsList;
    }

    auto RootDirectoryMonitor::ConvertDataToJobContents(
        const QList<DataDirectoryContents>& dataDirectoryContentsList) const -> QList<UseCase::InputJobContents> {
        QList<UseCase::InputJobContents> inputJobContentsList;

        for (const auto& dataDirectoryContents : dataDirectoryContentsList) {
            auto inputJobContents = GenerateCommonInputJobContents();
            inputJobContents.SetDataType(dataDirectoryContents.GetDataType());
            inputJobContents.SetTimeFrames(d->timeFrameNumber, dataDirectoryContents.GetTimeFrameIndex());

            inputJobContentsList.push_back(inputJobContents);
        }
        return inputJobContentsList;
    }

    auto RootDirectoryMonitor::SetDataDirectoryMonitors() -> void {
        if (d->htIncluded) {
            const auto htDataDirectory = QString("%1/data3d").arg(d->rootDirectoryPath);
            d->dataHtMonitor = std::make_shared<DataDirectoryMonitorHt>(htDataDirectory, d->timeFrameNumber);
        }
        if (d->bfIncluded) {
            const auto bfDataDirectory = QString("%1/databf").arg(d->rootDirectoryPath);
            d->dataBfMonitor = std::make_shared<DataDirectoryMonitorBf>(bfDataDirectory, d->timeFrameNumber);
        }
        const auto flIncluded = d->flBlueIncluded || d->flGreenIncluded || d->flRedIncluded;
        if (flIncluded) {
            DataDirectoryMonitorFl::FlRequirement requirement;
            requirement.expectedNumberOfData = d->timeFrameNumber;
            requirement.directoryPath = QString("%1/data3dfl").arg(d->rootDirectoryPath);
            requirement.blueIncluded = d->flBlueIncluded;
            requirement.greenIncluded = d->flGreenIncluded;
            requirement.redIncluded = d->flRedIncluded;

            d->dataFlMonitor = std::make_shared<DataDirectoryMonitorFl>(requirement);
        }

        if (d->phaseIncluded) {
            const auto phaseDataDirectory = QString("%1/data2d").arg(d->rootDirectoryPath);
            d->dataPhaseMonitor = std::make_shared<DataDirectoryMonitorPhase>(phaseDataDirectory, d->timeFrameNumber);
        }
    }

    auto RootDirectoryMonitor::GenerateCommonInputJobContents() const -> UseCase::InputJobContents {
        const auto fileName = QDir(d->rootDirectoryPath).dirName();
        const auto outputPath = QString("%1/%2.TCF").arg(d->rootDirectoryPath).arg(fileName);

        UseCase::InputJobContents commonInputJobContents;
        commonInputJobContents.SetPaths(d->rootDirectoryPath, outputPath);
        commonInputJobContents.SetProcessingFlags(true, true);
        commonInputJobContents.SetDataIncludedFlags(d->htIncluded, d->flBlueIncluded, d->flGreenIncluded,
            d->flRedIncluded, d->phaseIncluded, d->bfIncluded);
        return commonInputJobContents;
    }

}
