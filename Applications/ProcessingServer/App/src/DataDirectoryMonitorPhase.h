#pragma once
#include <memory>
#include <QObject>
#include <QList>

#include "DataDirectoryContents.h"

namespace processing_server {
    class DataDirectoryMonitorPhase : public QObject {
        Q_OBJECT
    public:
        typedef DataDirectoryMonitorPhase Self;
        typedef std::shared_ptr<Self> Pointer;

        DataDirectoryMonitorPhase(const QString& directoryPath, const int32_t& expectedNumberOfData);
        ~DataDirectoryMonitorPhase();

        auto ScanChanged() const ->bool;
        auto GetNewDataDirectoryContents()->QList<DataDirectoryContents>;
        auto IsCompleted() const ->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        static auto GenerateDataDirectoryContents(const QString& timeFramePath)->DataDirectoryContents;
    };
}
