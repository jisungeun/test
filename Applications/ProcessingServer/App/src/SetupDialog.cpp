#include <QtWidgets>
#include <QSettings>

#include "SetupDialog.h"

SetupDialog::SetupDialog(QWidget* parent)
    : QDialog(parent) {
    setupUi(this);

    UpdateDevices();

    connect(m_pGPUModes, SIGNAL(buttonToggled(QAbstractButton*, bool)), this, SLOT(reqModeSelected(QAbstractButton*, bool)));
    connect(m_pEnableBGProc, SIGNAL(stateChanged(int)), this, SLOT(reqEnableBGProc(int)));
}

void SetupDialog::UpdateConfig(void) {
}

void SetupDialog::UpdateDevices(void) {
}

void SetupDialog::reqModeSelected(QAbstractButton* pButton, bool bChecked) {
    const bool isSingleMode = (pButton == m_pSingleGPU) && (bChecked == true);

    if (isSingleMode) {
        m_pGPULabel->setEnabled(true);
        m_pGPUCombo->setEnabled(true);
    } else {
        m_pGPULabel->setEnabled(false);
        m_pGPUCombo->setEnabled(false);
    }
}

void SetupDialog::reqEnableBGProc(int state) {
    const bool isEnabled = (state == Qt::Checked);
    m_pEnableBGProcBusy->setEnabled(isEnabled);
}
