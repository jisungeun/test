#include <QBoxLayout>

#include "SelfProgressDialog.h"

SelfProgressDialog::SelfProgressDialog(QWidget* parent, Qt::WindowFlags flags)
    : QDialog(parent, flags) {
    createWidgets();
}

SelfProgressDialog::SelfProgressDialog(const QString& labelText, const QString& cancelButtonText, int minimum, int maximum, QWidget* parent, Qt::WindowFlags flags)
    : QDialog(parent, flags) {
    createWidgets();

    m_pLabel->setText(labelText);
    m_pProgress->setRange(minimum, maximum);
}

SelfProgressDialog::~SelfProgressDialog(void) {
    Stop();
}

void SelfProgressDialog::createWidgets(void) {
    QVBoxLayout* pLayout = new QVBoxLayout();
    {
        m_pLabel = new QLabel("Progress");
        pLayout->addWidget(m_pLabel);

        m_pProgress = new QProgressBar();
        pLayout->addWidget(m_pProgress);
    }
    setLayout(pLayout);

    connect(&m_timer, SIGNAL(timeout()), this, SLOT(reqTimeout()));
}

void SelfProgressDialog::Start(bool bModal) {
    setModal(bModal);
    if (!isVisible()) show();

    m_pProgress->setValue(0);
    m_timer.start(200);
}

void SelfProgressDialog::Stop(void) {
    m_timer.stop();
    hide();
}

void SelfProgressDialog::reqTimeout(void) {
    m_pProgress->setValue(m_pProgress->value() + 1);
}