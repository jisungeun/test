#pragma once

#include <QString>
#include <QMap>
#include <QueueTableWidget.h>

class CompletedQueueTableWidget : public QueueTableWidget {
    Q_OBJECT

public:
    CompletedQueueTableWidget(QWidget* parent = 0);
    ~CompletedQueueTableWidget(void);

    void Update(void) override;
};
