#include <iostream>

#include <QtWidgets>
#include <QDir>
#include <QStorageInfo>
#include <QCoreApplication>

#include <TCLogger.h>

#include "Version.h"
#include "MainWindow.h"
#include "SetupDialog.h"
#include "RemoteClient.h"
#include "SelfProgressDialog.h"
#include "ProcessingQueue.h"
#include "ProcessingPerfTimer.h"

#include "ProcessSelectionWidget.h"


MainWindow::MainWindow(QWidget* parent)
    : QDialog(parent, Qt::WindowSystemMenuHint | Qt::WindowTitleHint)
{
    constructor();
}

MainWindow::MainWindow(bool isManual, processing_server::GuiClient* guiClient, QWidget* parent)
    : QDialog(parent, Qt::WindowSystemMenuHint | Qt::WindowTitleHint)
{
    bool isLicensed = true;

    SetGuiClient(guiClient);

    constructor();

    if (isLicensed) {
        if (isManual) {
            show();
        } else {
            hide();
            Start();
        }
    }

    int prevPort = QSettings().value("ServerPort", 0).toInt();

    RemoteClient::Pointer remote = RemoteClient::GetInstance(prevPort, this);
    if (!remote->isReady()) {
        QLOG_ERROR() << "Remote client is not ready.";
        show();
        QMessageBox::warning(this, "Remote connection", "It can't be connected to TomoStudio");
        return;
    } else {
        unsigned short port = remote->getPort();
        QSettings().setValue("ServerPort", port);
    }

    connect(remote, SIGNAL(sigReportStatus()), this, SLOT(reqReportStatus()));
    //connect(remote, SIGNAL(sigStopProcess()), this, SLOT(reqFinishProcess()));
    connect(remote, SIGNAL(sigProcess(const QString&)), this, SLOT(reqRequestProcess(const QString&)));
    connect(remote, SIGNAL(sigConnected(bool)), this, SLOT(reqClientConnected(bool)));
    //connect(remote, SIGNAL(sigClientStatus(bool)), this, SLOT(reqUpdateClientStatus(bool)));
    connect(remote, SIGNAL(sigUpdateList()), this, SLOT(reqUpdateList()));
    connect(remote, SIGNAL(sigShow()), this, SLOT(showNormal()));

    m_sysStatTimer.start(60000);
}

void MainWindow::constructor() {
    setupUi(this);

    setWindowTitle(QString("TomoProcessing Server - %1").arg(PROJECT_REVISION));
    setWindowFlags(windowFlags() | Qt::WindowStaysOnTopHint);

    createTrayIcon();
    setIcon(STATUS_IDLE);
    trayIcon->show();

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
    connect(m_pBtnQuit, SIGNAL(pressed()), this, SLOT(reqQuit()));
    connect(m_pBtnHide, SIGNAL(pressed()), this, SLOT(hide()));

    connect(addJob, SIGNAL(clicked()), this, SLOT(AddJobClicked()));

    StartPeriodicProgressTimer(1000);
}

MainWindow::~MainWindow() {
    QMessageBox::information(this, "TomoProcessingServer", "Processing server is finished.");
}

void MainWindow::setVisible(bool visible) {
    minimizeAction->setEnabled(visible);
    restoreAction->setEnabled(isMaximized() || !visible);
    QDialog::setVisible(visible);
}

void MainWindow::setIcon(int index) {
    QIcon icon = m_trayIcons[index];
    trayIcon->setIcon(icon);
    setWindowIcon(icon);

    trayIcon->setToolTip(m_trayTooltips[index]);
}

void MainWindow::createTrayIcon() {
    createTrayActions();

    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);

    m_trayIcons[STATUS_IDLE] = QIcon(":/idle.png");
    m_trayIcons[STATUS_BUSY] = QIcon(":/busy.png");
    m_trayIcons[STATUS_ERROR] = QIcon(":/error.png");

    m_statusIcons[STATUS_IDLE] = QPixmap(":/ball_ok.png");
    m_statusIcons[STATUS_BUSY] = QPixmap(":/ball_busy.png");
    m_statusIcons[STATUS_ERROR] = QPixmap(":/ball_error.png");

    m_trayTooltips[STATUS_IDLE] = QString("Idle");
    m_trayTooltips[STATUS_BUSY] = QString("Busy");
    m_trayTooltips[STATUS_ERROR] = QString("Error");
}

void MainWindow::createTrayActions() {
    minimizeAction = new QAction(tr("&Minimize"), this);
    connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, SIGNAL(triggered()), this, SLOT(reqQuit()));
}

void MainWindow::Start(void) {
    setIcon(STATUS_BUSY);
    m_updateTimer.start(1000);
}

void MainWindow::Stop(bool showProgressDialog) {
    m_status.Clear();

    setIcon(STATUS_IDLE);

    m_updateTimer.stop();

    RemoteClient::GetInstance()->sendResponse(ResponseDone(QString("stop"), true).toString());
}

void MainWindow::SetGuiClient(processing_server::GuiClient* guiClient) {
    this->guiClient = guiClient;
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason) {
    switch (reason) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
        showNormal();
        break;
    default:
        ;
    }
}

void MainWindow::reqRunStateChanged(int state) {
    if (state == 0) Start();
    else Stop(true);
}

void MainWindow::reqSetup(void) {
    SetupDialog dlg(this);
    if (dlg.exec() != QDialog::Accepted) return;

    dlg.UpdateConfig();
}

void MainWindow::reqQuit(void) {
    //if (RemoteClient::GetInstance()->isConnected()) {
    //    QMessageBox::information(this, "Quit", "Can't quit HoloProcessingServer while TomoStudio is running");
    //    return;
    //}

    Stop();
    qApp->quit();
}

void MainWindow::reqChangeFolder(void) {
    QString strPath = QFileDialog::getExistingDirectory(this, "Choose a data folder");
    if (strPath.isEmpty()) return;
}

void MainWindow::reqReportStatus() {
    const QString resp = m_status.toString();
    RemoteClient::GetInstance()->sendResponse(resp);
}

void MainWindow::reqFinishProcess() {
    RemoteClient::GetInstance()->sendResponse(ResponseDone(QString("stop"), true).toString());
}

void MainWindow::reqRequestProcess(const QString& strOption) {
    QString strPath;
    auto reprocessingFlag{ false };
    auto deconvolutionFlag{ false };

    QStringList tokens = strOption.split('|');
    foreach(auto token, tokens) {
        QStringList param = token.split('=');
        if (param.size() != 2) continue;

        const QString cmd = param.at(0);
        const QString val = param.at(1);

        if (cmd.compare("path", Qt::CaseInsensitive) == 0) {
            strPath = val;
        } else if (cmd.compare("is_reproc", Qt::CaseInsensitive) == 0) {
            reprocessingFlag = val.toInt() == 1;
        } else if (cmd.compare("incl_decon", Qt::CaseInsensitive) == 0) {
            deconvolutionFlag = val.toInt() == 1;
        }
    }

    if (reprocessingFlag) {
        guiClient->AddOneTcfReprocessing(strPath, deconvolutionFlag);
    } else {
        guiClient->AddOneTcf(strPath, deconvolutionFlag);
    }

    RemoteClient::GetInstance()->sendResponse(ResponseDone(QString("process"), true).toString());
}

void MainWindow::AddJobClicked() {
    const QString titleName = "Select directory path to add online processing";
    const auto tcfFolderPath = QFileDialog::getExistingDirectory(this, titleName, "C:/");
    if (tcfFolderPath.isEmpty()) return;

    guiClient->AddOnlineProcessingPath(tcfFolderPath);
}

auto MainWindow::UpdateProgress() -> void {
    const auto updatedProgressList = guiClient->UpdateProgress();

    auto queue = ProcessingQueue::GetInstance();

    for (auto i = 0; i < updatedProgressList.Size(); ++i) {
        const auto updatedDataPath = updatedProgressList.GetDataPath(i);
        const auto updatedProgress = updatedProgressList.GetProgress(i);

        const auto listHasDataPath = (progressMap.find(updatedDataPath) != progressMap.end());

        if (listHasDataPath) {
            const auto progressChanged = (progressMap[updatedDataPath] != updatedProgress);

            if (progressChanged) {
                const auto progressBefore = progressMap.take(updatedDataPath);
                if (progressBefore == 0) {
                    elapsedTimerMap[updatedDataPath].start();
                }

                progressMap[updatedDataPath] = updatedProgress;
            }
        } else {
            progressMap[updatedDataPath] = 0;
            elapsedTimerMap[updatedDataPath] = QElapsedTimer{};
            m_status.ClearCompleted(updatedDataPath);
            queue->AddTask(updatedDataPath);
        }
    }

    for (auto iterator = progressMap.begin(); iterator != progressMap.end(); ) {
        const auto dataPath = iterator.key();
        const auto progress = iterator.value();

        auto elapsedTime{ 0 };
        if (elapsedTimerMap[dataPath].isValid()) {
            elapsedTime = elapsedTimerMap[dataPath].elapsed() / 1000;
        }

        const auto completeCase = (progress == 1);
        if (completeCase) {
            const auto success = true;
            queue->UpdateCompleted(dataPath, elapsedTime, success);
            elapsedTimerMap.remove(dataPath);
            iterator = progressMap.erase(iterator);

            m_status.SetCompleted(dataPath, success);
        } else {
            queue->UpdateProgress(dataPath, elapsedTime, static_cast<int32_t>(progress * 100));
            ++iterator;
        }
    }

    ProcessingPerfTimer::Pointer perfTimer = ProcessingPerfTimer::GetInstance();

    int elapsed = perfTimer->GetTimeElapsed();
    int remaining = perfTimer->GetTimeRemaining();
    double progress = 0;
    if (elapsed + remaining != 0) {
        progress = (elapsed * 100) / (elapsed + remaining);
    }


    if (remaining != 0) m_status.SetTotalProgress(progress, secs2str(elapsed));
    else m_status.SetTotalProgress(0, secs2str(elapsed));
}

void MainWindow::reqClientConnected(bool bConnected) {
    //if (bConnected) {
    //    m_pBtnQuit->hide();
    //} else {
    //    m_pBtnQuit->show();
    //}
}

void MainWindow::reqUpdateList(void) {
    QStringList pathes;
    ProcessingQueue::GetInstance()->GetProcessingList(pathes);
    RemoteClient::GetInstance()->sendResponse(ResponseList(pathes).toString());
}

QString MainWindow::secs2str(qint64 sec) {
    const int hours = sec / 3600;
    const int mins = (sec - (hours * 3600)) / 60;
    const int secs = sec - (hours * 3600) - (mins * 60);

    return QString("%1:%2:%3")
        .arg(hours, 2, 10, QChar('0'))
        .arg(mins, 2, 10, QChar('0'))
        .arg(secs, 2, 10, QChar('0'));
}

int MainWindow::find(const QString& strTop, bool isReprocess, QStringList& pathes) {
    QStringList ltPathes;

    QDirIterator itr(strTop, QStringList("config.dat"), QDir::Files, QDirIterator::Subdirectories);
    while (itr.hasNext())
        ltPathes.append(itr.next());

    pathes.clear();
    foreach(auto path, ltPathes) {
        path = path.replace("\\", "/");
        path = path.remove("/config.dat");
        pathes.append(path);
    }

    if (!isReprocess) {
        QStringList tcfPathes;

        ltPathes.clear();

        QDirIterator itr(strTop, QStringList("*.TCF"), QDir::Files, QDirIterator::Subdirectories);
        while (itr.hasNext())
            ltPathes.append(itr.next());

        foreach(auto path, ltPathes) {
            QFileInfo info(path);
            path = info.dir().absolutePath();
            path = path.replace("\\", "/");
            tcfPathes.append(path);
        }

        QStringList allPathes(pathes);

        pathes.clear();
        foreach(auto path, allPathes) {
            const int idx = tcfPathes.indexOf(path);
            if (-1 != idx) {
                continue;
            }
            pathes.push_back(path);
        }
    }

    return pathes.size();
}

auto MainWindow::StartPeriodicProgressTimer(const uint32_t& msec) -> void {
    connect(&progressTimer, SIGNAL(timeout()), this, SLOT(UpdateProgress()));
    progressTimer.start(msec);
}