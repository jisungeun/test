#include <QMutexLocker>

#include <TCLogger.h>

#include "Response.h"

QString ResponseDone::toString(void)
{
	QMutexLocker locker(&m_mutex);

	QString ret("{\n");

	ret.append(QString("cmd=%1\n").arg(m_strCmd));
	ret.append(QString("result=%1\n").arg(m_bOK));
	ret.append("}");

	return ret;
}

void ResponseStatus::SetTotalProgress(int nProgress, const QString& strElapsed)
{
	QMutexLocker locker(&m_mutex);
	m_nTotalProgress = nProgress;
	m_strTotalElapsed = strElapsed;
}

void ResponseStatus::SetCompleted(const QString& strPath, bool Success)
{
	QMutexLocker locker(&m_mutex);

	Result res;
	res.m_strPath = strPath;
	res.m_bCompleted = Success;
	m_ltCompleted.push_back(res);

	if (m_ltCompleted.size() > 5)
		m_ltCompleted.pop_front();
}

void ResponseStatus::ClearCompleted(const QString& strPath)
{
	QMutexLocker locker(&m_mutex);
	foreach(auto item, m_ltCompleted)
	{
		if (item.m_strPath.compare(strPath) == 0)
		{
			m_ltCompleted.removeOne(item);
			return;
		}
	}
}

void ResponseStatus::Clear(void)
{
	QMutexLocker locker(&m_mutex);
	m_nTotalProgress = 0;
	m_ltCompleted.clear();
}


QString ResponseStatus::toString(void)
{
	QMutexLocker locker(&m_mutex);

	QString ret("{\n");

	ret.append(QString("cmd=status\n"));
	ret.append(QString("total_progress=%1\n").arg(m_nTotalProgress));
	ret.append(QString("total_elapsed=%1\n").arg(m_strTotalElapsed));
	ret.append(QString("latest_completed_count=%1\n").arg(m_ltCompleted.size()));
	for (int i = 0; i < m_ltCompleted.size(); i++)
	{
		QString strCompletedPath = QString("latest_path_%1").arg(i + 1);
		ret.append(QString("%1=%2\n").arg(strCompletedPath).arg(m_ltCompleted.at(i).m_strPath));
	}
	for (int i = 0; i < m_ltCompleted.size(); i++)
	{
		QString strResult = QString("latest_result_%1").arg(i + 1);
		ret.append(QString("%1=%2\n").arg(strResult).arg(m_ltCompleted.at(i).m_bCompleted));
	}

	ret.append("}");

	return ret;
}

QString ResponseList::toString(void)
{
	QMutexLocker locker(&m_mutex);

	QString ret("{\n");

	ret.append(QString("cmd=update_list\n"));
	ret.append(QString("list=%1\n").arg(m_strPathes.join(",")));
	ret.append("}");

	return ret;
}