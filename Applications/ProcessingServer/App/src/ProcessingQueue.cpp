#include <QMutexLocker>
#include <QDir>

#include "QueueWidgetInterface.h"
#include "ProcessingPerfTimer.h"
#include "ProcessingQueue.h"
#include <TCLogger.h>

ProcessingQueue::ProcessingQueue(void) {
}

ProcessingQueue::~ProcessingQueue(void) {
}

ProcessingQueue::Pointer ProcessingQueue::GetInstance(void) {
    static ProcessingQueue::Pointer theInstance(new ProcessingQueue());
    return theInstance;
}

void ProcessingQueue::AddTask(const QString& strPath) {
    AddTask(QStringList(strPath));
}

void ProcessingQueue::AddTask(const QStringList& strPathes) {
    QMutexLocker locker(&m_mutex);

    foreach(auto strPath, strPathes) {
        m_waitingList.push_back(strPath);
        m_failedList.removeOne(strPath);

        Item item;
        item.elements = CountElements(strPath);
        item.status = WAITING;
        item.progress = 0;

        m_map[strPath] = item;
        Update(strPath, WAITING);
    }

    ProcessingPerfTimer::Pointer perfTimer = ProcessingPerfTimer::GetInstance();
    perfTimer->AddTask(strPathes);

    UpdateWidgets();
}

void ProcessingQueue::UpdateProgress(const QString& strPath, int elapsed, int progress) {
    QMutexLocker locker(&m_mutex);

    auto iter = m_map.find(strPath);
    if (iter == m_map.end()) return;
    if ((iter->status == COMPLETED) || (iter->status == FAILED)) return;

    if (iter->status == WAITING) {
        iter->status = PROCESSING;
        m_processingList.push_back(strPath);
        m_waitingList.removeOne(strPath);
    }

    Update(strPath, PROCESSING);

    iter->elapsed = elapsed;
    iter->progress = progress;

    ProcessingPerfTimer::Pointer perfTimer = ProcessingPerfTimer::GetInstance();
    perfTimer->Update(strPath);

    UpdateWidgets();
}

void ProcessingQueue::UpdateCompleted(const QString& strPath, int elapsed, bool bCompleted) {
    QMutexLocker locker(&m_mutex);

    auto iter = m_map.find(strPath);
    if (iter == m_map.end()) return;
    if ((iter->status == COMPLETED) || (iter->status == FAILED)) return;

    if (bCompleted) {
        iter->status = COMPLETED;
        iter->elapsed = elapsed;
        iter->progress = 100;
        m_completedList.push_back(strPath);
        Update(strPath, COMPLETED);
    } else {
        iter->status = FAILED;
        iter->elapsed = elapsed;
        iter->progress = 0;
        m_failedList.push_back(strPath);
        Update(strPath, FAILED);
    }

    m_processingList.removeOne(strPath);

    ProcessingPerfTimer::Pointer perfTimer = ProcessingPerfTimer::GetInstance();
    perfTimer->Complete(strPath, bCompleted);

    UpdateWidgets();
}

void ProcessingQueue::ResetWaitingAll(void) {
    QMutexLocker locker(&m_mutex);

    QStringList tmpList;

    foreach(auto item, m_waitingList) {
        tmpList.push_back(item);
        Update(item, DELETED);
    }

    foreach(auto item, m_processingList) {
        tmpList.push_back(item);
        Update(item, DELETED);
    }

    m_waitingList.clear();
    m_processingList.clear();

    UpdateWidgets();

    for (auto iter = tmpList.begin(); iter != tmpList.end(); iter++) m_map.remove(*iter);
}

void ProcessingQueue::ResetFailed(const QString& strPath) {
    AddTask(strPath);
}

void ProcessingQueue::ResetFailedAll(void) {
    QStringList all = m_failedList;
    AddTask(all);
}

bool ProcessingQueue::IsEmpty(void) {
    QMutexLocker locker(&m_mutex);
    return (m_waitingList.size() + m_processingList.size()) == 0;
}

void ProcessingQueue::UpdateWidgets(void) {
    foreach(auto widget, m_widgets) {
        widget->Update();
    }

    m_updated.clear();
    m_updatedList.clear();
}

int ProcessingQueue::CountElements(const QString& strPath) {
    int count = 0;

    auto infoList = QDir(strPath).entryInfoList(QStringList("data*"), QDir::Dirs);
    foreach(auto data_dir, infoList) {
        QDir dir(data_dir.absoluteFilePath());
        auto dataList = dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
        count = dataList.size();
    }

    return count;
}
