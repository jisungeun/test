#pragma once

#include <QDialog>
#include <QProgressBar>
#include <QTimer>
#include <QLabel>
#include <QProgressBar>

class SelfProgressDialog : public QDialog {
    Q_OBJECT
public:
    SelfProgressDialog(QWidget* parent = Q_NULLPTR, Qt::WindowFlags flags = Qt::WindowFlags());
    SelfProgressDialog(const QString& labelText, const QString& cancelButtonText,
        int minimum, int maximum, QWidget* parent = Q_NULLPTR,
        Qt::WindowFlags flags = Qt::WindowFlags());
    ~SelfProgressDialog();

    void Start(bool bModal = false);
    void Stop(void);

private:
    void createWidgets(void);

protected slots:
    void reqTimeout(void);

private:
    QTimer m_timer;
    QLabel* m_pLabel;
    QProgressBar* m_pProgress;
};