#include <QBoxLayout>
#include "SwitchButton.h"

SwitchButton::SwitchButton(QWidget* parent, Qt::WindowFlags f)
    : QWidget(parent, f)
    , m_nState(0) {
    PixmapButton* btn1 = new PixmapButton(this);
    PixmapButton* btn2 = new PixmapButton(this);

    QHBoxLayout* pLayout = new QHBoxLayout();
    pLayout->addWidget(btn1);
    pLayout->addWidget(btn2);
    pLayout->setSpacing(0);
    pLayout->setContentsMargins(0, 0, 0, 0);
    setLayout(pLayout);

    m_buttons.push_back(btn1);
    m_buttons.push_back(btn2);

    for (int i = 0; i < 2; i++) {
        m_iconStates.push_back(QPixmap(10, 10));
        m_iconActions.push_back(QPixmap(10, 10));
    }

    connect(m_buttons[0], SIGNAL(pressed()), this, SLOT(reqAction1()));
    connect(m_buttons[1], SIGNAL(pressed()), this, SLOT(reqAction2()));
}

SwitchButton::~SwitchButton() {
}

void SwitchButton::setState(int index) {
    m_nState = index;
    updateButtons();
}

void SwitchButton::setupState(int index, QPixmap& stateIcon, QPixmap& actionIcon) {
    if ((index < 0) || (index > 1)) return;

    m_iconStates[index] = stateIcon;
    m_iconActions[index] = actionIcon;
    m_buttons[index]->setMinimumSize(stateIcon.rect().size());
}

void SwitchButton::updateButtons(void) {
    const int sidx = m_nState;	//state
    const int aidx = (1 - m_nState); //action

    m_buttons[sidx]->setPixmap(m_iconStates.at(sidx));
    m_buttons[aidx]->setPixmap(m_iconActions.at(aidx));
}

void SwitchButton::reqAction1() {
    if (m_nState == 0) return;

    m_nState = 0;
    updateButtons();
    emit sigStateChanged(m_nState);
}

void SwitchButton::reqAction2() {
    if (m_nState == 1) return;

    m_nState = 1;
    updateButtons();
    emit sigStateChanged(m_nState);
}
