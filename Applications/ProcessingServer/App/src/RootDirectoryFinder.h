#pragma once
#include <memory>
#include <QString>
#include <QList>

namespace processing_server {
    class RootDirectoryFinder {
    public:
        struct RootDirectory {
            QString rootDirectoryPath{};
            bool tcfExists{};
            bool tempExists{};
        };

        RootDirectoryFinder(const QString& targetPath);
        ~RootDirectoryFinder();

        auto Find() const->QList<RootDirectory>;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        static auto FindInDirectory(const QString& directoryPath)->QList<RootDirectory>;
        static auto DirectoryIsRootDirectory(const QString& directoryPath)->bool;

    };
}
