#pragma once

#include <memory>
#include <QList>
#include <QMap>
#include <QString>
#include <QStringList>
#include <QMutex>

class QueueWidgetInterface;

class ProcessingQueue {
public:
    typedef ProcessingQueue Self;
    typedef std::shared_ptr<ProcessingQueue> Pointer;

    enum {
        WAITING,
        PROCESSING,
        COMPLETED,
        FAILED,
        DELETED
    };

    struct Item {
        int status;
        int elapsed;
        int elements;
        int progress;
    };

public:
    static Pointer GetInstance(void);
    ~ProcessingQueue(void);

    void AddTask(const QString& strPath);
    void AddTask(const QStringList& strPathes);

    void UpdateProgress(const QString& strPath, int elapsed, int progress);
    void UpdateCompleted(const QString& strPath, int elapsed, bool bCompleted);

    void ResetWaitingAll(void);
    void ResetFailed(const QString& strPath);
    void ResetFailedAll(void);

    bool IsEmpty(void);

    void GetProcessingList(QStringList& strList) {
        QMutexLocker locker(&m_mutex);
        strList.append(m_processingList);
        strList.append(m_waitingList);
    }

    inline Item& GetStatus(const QString& strPath) { return m_map[strPath]; }

protected:
    ProcessingQueue(void);

    void AddWidget(QueueWidgetInterface* pWidget) { m_widgets.push_front(pWidget); }

    inline QStringList& waitingList(void) { return m_waitingList; }
    inline QStringList& processingList(void) { return m_processingList; }
    inline QStringList& completedList(void) { return m_completedList; }
    inline QStringList& failedList(void) { return m_failedList; }
    inline QStringList& updatedList(void) { return m_updatedList; }
    inline QMap<QString, int>& updatedStatus(void) { return m_updated; }

    void UpdateWidgets(void);
    int CountElements(const QString& strPath);

    inline void Update(const QString& strPath, int status) {
        m_updated[strPath] = status;
        m_updatedList.push_back(strPath);
    }

private:
    QStringList m_waitingList;
    QStringList m_processingList;
    QStringList m_completedList;
    QStringList m_failedList;

    QMap<QString, Item> m_map;
    QMap<QString, int> m_updated;
    QStringList m_updatedList;

    QMutex m_mutex;

    QList<QueueWidgetInterface*> m_widgets;

    friend class QueueWidgetInterface;
};