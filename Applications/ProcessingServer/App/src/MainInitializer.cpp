#define LOGGER_TAG "ProcessingServer::MainInitializer"
#include "MainInitializer.h"

#include <QFile>
#include <QStandardPaths>

#include <TCLogger.h>

#include "JobManagerCaller.h"
#include "ProcessorCreator.h"
#include "ProcessorRegistryCaller.h"
#include "TCFDataValidityChecker.h"
#include "TCFEntryWriter.h"
#include "TCFWriterPolicy.h"

#include "Version.h"

namespace processing_server {
    auto MainInitializer::ApplyStyle(QApplication& qApplication) -> void {
        QFile styleFile(":qdarkstyle/style.qss");
        styleFile.open(QFile::ReadOnly);
        qApplication.setStyleSheet(styleFile.readAll());
    }

    auto MainInitializer::Initialize() -> void {
        SetupLogger();
        SetJobManager();
        SetProcessorRegistry();
    }

    auto MainInitializer::SetupLogger() -> void {
        const auto applicationDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
        const auto logFilePath = QString("%1/TomoProcessingServer.log").arg(applicationDataPath);
        const auto errorLogFilePath = QString("%1/TomoProcessingServer_Error.log").arg(applicationDataPath);

        TC::Logger::Initialize(logFilePath, errorLogFilePath);
    }

    auto MainInitializer::SetJobManager()->void {
        auto jobManager = Entity::JobManagerCaller::GetJobManager();

        const auto tcfWriterPointer = std::make_shared<Plugins::TCFEntryWriter>();
        tcfWriterPointer->SetSoftwareVersion(QString("TomoProcessing Server - %1").arg(PROJECT_REVISION));


        const auto tcfWriterPolicyPointer = std::make_shared<Plugins::TCFWriterPolicy>();
        const size_t gb = 1024 * 1024 * 1024;
        const auto limitMemorySize = 20 * gb;
        tcfWriterPolicyPointer->SetLimitMemorySizeInBytes(limitMemorySize);
        
        const auto tcfValidityCheckerPointer = std::make_shared<Plugins::TCFDataValidityChecker>();
        
        jobManager->SetFileWriter(tcfWriterPointer);
        jobManager->SetWriterPolicy(tcfWriterPolicyPointer);
        jobManager->SetValidityChecker(tcfValidityCheckerPointer);
    }

    auto MainInitializer::SetProcessorRegistry()->void {
        auto processorRegistryPointer = Entity::ProcessorRegistryCaller::GetProcessorRegistry();

        Plugins::ProcessorCreator processorCreator;
        const auto systemProcessorsList = processorCreator.Create();

        for (const auto& processor : systemProcessorsList) {
            processorRegistryPointer->Register(processor);
        }
    }
}
