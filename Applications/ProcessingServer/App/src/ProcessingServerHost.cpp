#include "ProcessingServerHost.h"

#include <iostream>
#include <QDir>
#include <QTimer>

#include "AddController.h"
#include "DataScanner.h"
#include "DirectoryMonitor.h"
#include "InputJobScanner.h"
#include "ProgressHostPresenter.h"
#include "ReportController.h"
#include "TCLogger.h"

namespace processing_server {
    struct ProcessingServerHost::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        QList<IProcessingServerClient*> clients{};
        QMap<IProcessingServerClient*, QMap<QString, CompleteProcessFlag>> clientsCompleteProgressFlagMap{};

        DirectoryMonitor::Pointer monitor{};

        Interactor::ProgressList progressList{};

        QTimer* scanTimer{};
        QTimer* progressTimer{};
    };

    ProcessingServerHost::ProcessingServerHost()
        : d(new Impl()) {
        d->monitor = std::make_shared<DirectoryMonitor>();

        const uint32_t sec = 1000;
        StartPeriodicProgressTimer(1 * sec);
        StartPeriodicMonitorTimer(1 * sec);
    }

    ProcessingServerHost::ProcessingServerHost(const ProcessingServerHost& other)
        : d(new Impl(*other.d)) {
    }

    ProcessingServerHost::~ProcessingServerHost() = default;

    auto ProcessingServerHost::RegisterClient(IProcessingServerClient* client) -> void {
        d->clients.push_back(client);
        client->SetHost(this);

        d->clientsCompleteProgressFlagMap[client] = QMap<QString, CompleteProcessFlag>();
    }

    auto ProcessingServerHost::DeregisterClient(IProcessingServerClient* client) -> void {
        d->clients.removeOne(client);
        d->clientsCompleteProgressFlagMap.remove(client);
    }

    auto ProcessingServerHost::AddOneTcf(const QString& rootPath, const bool& deconvolutionFlag) -> void {
        if (IsProcessingRequired(rootPath)) {
            auto inputJobScanner = InputJobScanner(rootPath);
            auto inputJobContentsList = inputJobScanner.Scan();

            const auto reprocessingFlag = true;
            for (auto& inputJobContents : inputJobContentsList) {
                inputJobContents.SetProcessingFlags(reprocessingFlag, deconvolutionFlag);
                Interactor::AddController::AddOneJob(inputJobContents);
            }
        }
    }

    auto ProcessingServerHost::AddOneTcfReprocessing(const QString& rootPath, const bool& deconvolutionFlag)-> void {
        RemovePreprocessedData(rootPath);
        AddOneTcf(rootPath, deconvolutionFlag);
    }

    auto ProcessingServerHost::AddDirectory(const QString& directoryPath, const bool& deconvolutionFlag) -> void {
        const auto subFolderList = GetSubFolderList(directoryPath);
        for (auto& subFolderPath : subFolderList) {
            const auto rootPath = QString("%1/%2").arg(directoryPath).arg(subFolderPath);
            AddOneTcf(rootPath, deconvolutionFlag);
        }
    }

    auto ProcessingServerHost::AddDirectoryReprocessing(const QString& directoryPath, const bool& deconvolutionFlag)
        -> void {
        const auto subFolderList = GetSubFolderList(directoryPath);
        for (auto& subFolderPath : subFolderList) {
            const auto rootPath = QString("%1/%2").arg(directoryPath).arg(subFolderPath);
            AddOneTcfReprocessing(rootPath, deconvolutionFlag);
        }
    }

    auto ProcessingServerHost::AddOnlineProcessingPath(const QString& path) -> void {
        d->monitor->AddPathToMonitor(path);
    }

    auto ProcessingServerHost::UpdateProgressToClient(IProcessingServerClient* client) -> void {
        auto& clientProgressFlagMap = d->clientsCompleteProgressFlagMap[client];
        const auto& progressList = d->progressList;

        Interactor::ProgressList updatedProgressList;
        for (auto i = 0; i < progressList.Size(); ++i) {
            const auto dataPath = progressList.GetDataPath(i);
            const auto progressValue = progressList.GetProgress(i);

            const auto progressIsCompleted = (progressValue == 1);

            const auto progressIsUpdatedBefore =
                clientProgressFlagMap.find(dataPath) != clientProgressFlagMap.end();

            if (progressIsUpdatedBefore) {
                if (progressIsCompleted) {
                    const auto clientAlreadyHasProgress = clientProgressFlagMap[dataPath];
                    if (!clientAlreadyHasProgress) {
                        clientProgressFlagMap.remove(dataPath);
                        clientProgressFlagMap[dataPath] = progressIsCompleted;
                        updatedProgressList.PushBack(dataPath, progressValue);
                    }
                } else {
                    clientProgressFlagMap.remove(dataPath);
                    clientProgressFlagMap[dataPath] = progressIsCompleted;
                    updatedProgressList.PushBack(dataPath, progressValue);
                }
            } else {
                clientProgressFlagMap[dataPath] = progressIsCompleted;
                updatedProgressList.PushBack(dataPath, progressValue);
            }
        }

        client->SetProgressList(updatedProgressList);

        RemoveUpdatedCompletedProgress();
    }

    void ProcessingServerHost::UpdateMonitor() {
        const auto newInputJobContentsList = d->monitor->GetNewInputJobContents();
        for (const auto& inputJobContents : newInputJobContentsList) {
            Interactor::AddController::AddOneJob(inputJobContents);
        }
    }

    void ProcessingServerHost::UpdateProgress() {
        const Interactor::ReportController reportController;
        const auto updatedProgress = reportController.ReportHost();

        for (auto i = 0; i < updatedProgress.Size(); ++i) {
            d->progressList.UpdateProgress(updatedProgress.GetDataPath(i), updatedProgress.GetProgress(i));
        }
    }

    auto ProcessingServerHost::IsProcessingRequired(const QString& rootPath) -> bool {
        return !ProcessingResultExists(rootPath);
    }

    auto ProcessingServerHost::ProcessingResultExists(const QString& rootPath) -> bool {
        const QStringList processedFileFilters{ "*.TCF" };
        const auto preProcessedList = QDir(rootPath).entryList(processedFileFilters, QDir::Files);
        return !preProcessedList.isEmpty();
    }

    auto ProcessingServerHost::RemovePreprocessedData(const QString& rootPath) -> void {
        const QStringList preprocessedFileFilters{ "*.tctmp", "*.TCF", "*-BF.TIFF", "*-MIP.TIFF" };
        const auto preProcessedFileList = QDir(rootPath).entryList(preprocessedFileFilters, QDir::Files);
        for (const auto& preProcessedFile : preProcessedFileList) {
            QFile::remove(QString("%1/%2").arg(rootPath).arg(preProcessedFile));
        }
    }

    auto ProcessingServerHost::GetSubFolderList(const QString& topFolderPath) -> QStringList {
        const QDir dir(topFolderPath);
        const auto subFolderList = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);
        return subFolderList;
    }

    auto ProcessingServerHost::StartPeriodicMonitorTimer(const uint32_t& msec) -> void {
        d->scanTimer = new QTimer(this);
        connect(d->scanTimer, SIGNAL(timeout()), this, SLOT(UpdateMonitor()));
        d->scanTimer->start(msec);
    }

    auto ProcessingServerHost::StartPeriodicProgressTimer(const uint32_t& msec) -> void {
        d->progressTimer = new QTimer(this);
        connect(d->progressTimer, SIGNAL(timeout()), this, SLOT(UpdateProgress()));
        d->progressTimer->start(msec);
    }

    auto ProcessingServerHost::RemoveUpdatedCompletedProgress() -> void {
        const auto& hostList = d->progressList;

        QStringList removeList;

        for (auto i = 0; i < hostList.Size(); ++i) {
            const auto dataPath = hostList.GetDataPath(i);
            const auto hostComplete = (hostList.GetProgress(i) == 1);

            if (hostComplete) {
                auto allClientProgressesAreCompleted = true;

                for (const auto& clientPointer : d->clients) {
                    const auto& clientProgressMap = d->clientsCompleteProgressFlagMap[clientPointer];

                    const auto progressExistsInClient =
                        clientProgressMap.find(dataPath) != clientProgressMap.end();

                    if (progressExistsInClient) {
                        allClientProgressesAreCompleted = clientProgressMap[dataPath];
                    } else {
                        allClientProgressesAreCompleted = false;
                    }
                }

                if (allClientProgressesAreCompleted) {
                    removeList.push_back(dataPath);
                }
            }
        }

        for (const auto& removeDataPath : removeList) {
            d->progressList.RemoveProgress(removeDataPath);
            for (const auto& clientPointer : d->clients) {
                auto& clientProgressMap = d->clientsCompleteProgressFlagMap[clientPointer];
                clientProgressMap.remove(removeDataPath);
            }
        }
    }
}
