#include "RootDirectoryFinder.h"

#include <QDir>

namespace processing_server {
    struct RootDirectoryFinder::Impl {
        Impl() = default;
        ~Impl() = default;

        QString targetPath{};
    };

    RootDirectoryFinder::RootDirectoryFinder(const QString& targetPath)
        : d(new Impl()) {
        auto path = targetPath;
        d->targetPath = path.replace("\\", "/");
    }

    RootDirectoryFinder::~RootDirectoryFinder() = default;

    auto RootDirectoryFinder::Find() const -> QList<RootDirectory> {
        return FindInDirectory(d->targetPath);
    }

    auto RootDirectoryFinder::FindInDirectory(const QString& directoryPath) -> QList<RootDirectory> {
        const QDir directoryDir(directoryPath);
        QList<RootDirectory> rootDirectoryList;

        if (DirectoryIsRootDirectory(directoryPath)) {
            RootDirectory rootDirectory;

            const QString tcfPattern = "*.TCF";
            const QStringList tcfFilter(tcfPattern);
            const auto tcfEntry = directoryDir.entryList(tcfFilter, QDir::Files);

            const QString tempPattern = "*.tctmp";
            const QStringList tempFilter(tempPattern);
            const auto tempEntry = directoryDir.entryList(tempFilter, QDir::Files);

            rootDirectory.tcfExists = !tcfEntry.empty();
            rootDirectory.tempExists = !tempEntry.empty();
            rootDirectory.rootDirectoryPath = directoryPath;

            rootDirectoryList.push_back(rootDirectory);
        } else {
            const auto directoryEntry = directoryDir.entryList(QDir::NoDotAndDotDot | QDir::Dirs);

            for (const auto& directoryName : directoryEntry) {
                const auto subDirectoryPath = QString("%1/%2").arg(directoryPath).arg(directoryName);
                const auto subRootDirectoryList = FindInDirectory(subDirectoryPath);
                rootDirectoryList.append(subRootDirectoryList);
            }
        }

        return rootDirectoryList;
    }

    auto RootDirectoryFinder::DirectoryIsRootDirectory(const QString& directoryPath) -> bool {
        const QDir directoryDir(directoryPath);

        const auto configFileExists = directoryDir.exists("config.dat");
        const auto jobParameterFileExists = directoryDir.exists("JobParameter.tcp");

        return configFileExists && jobParameterFileExists;
    }
}
