#pragma once
#include <memory>

#include <QObject>
#include <QString>
#include <QList>

#include "InputJobContents.h"

namespace processing_server {
    class DirectoryMonitor : public QObject {
        Q_OBJECT
    public:
        typedef DirectoryMonitor Self;
        typedef std::shared_ptr<Self> Pointer;

        DirectoryMonitor();
        explicit DirectoryMonitor(const QString& topDirectoryPath);
        ~DirectoryMonitor();

        auto AddPathToMonitor(const QString& topDirectoryPath)->void;
        auto GetNewInputJobContents()->QList<UseCase::InputJobContents>;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        auto UpdateDirectories(const QString& directoryPath)->void;
    };
}
