#pragma once
#include <memory>

#include <QString>

#include "DataType.h"

namespace processing_server {
    class DataDirectoryContents {
    public:
        DataDirectoryContents(const DataDirectoryContents& other);
        DataDirectoryContents(const Entity::DataType& dataType, const int32_t& timeFrameIndex);
        ~DataDirectoryContents();

        auto GetDataType() const->Entity::DataType;
        auto GetTimeFrameIndex() const->int32_t;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
