#include <TCLogger.h>

#include "ProcessingQueueTableWidget.h"
#include "CompletedQueueTableWidget.h"
#include "FailedQueueTableWidget.h"

#include "ProcessingQueueTabWidget.h"
#define BTNWIDHT (23)

ProcessingQueueTabWidget::ProcessingQueueTabWidget(QWidget* parent)
    : QTabWidget(parent)
    , QueueWidgetInterface() {
    m_titles[PROCESSING] = "Processing";
    m_titles[COMPLETED] = "Completed";
    m_titles[FAILED] = "Failed";

    QueueTableWidget* pProcessing = new ProcessingQueueTableWidget(this);
    addTab(pProcessing, m_titles[PROCESSING]);
    m_tables[PROCESSING] = pProcessing;

    QueueTableWidget* pCompleted = new CompletedQueueTableWidget(this);
    addTab(pCompleted, m_titles[COMPLETED]);
    m_tables[COMPLETED] = pCompleted;

    QueueTableWidget* pFailed = new FailedQueueTableWidget(this);
    addTab(pFailed, m_titles[FAILED]);
    m_tables[FAILED] = pFailed;

    ////Add action buttons...
    //QPushButton* pMoveTop = new QPushButton(QIcon(":/top.png"), "", this);
    //pMoveTop->setFixedWidth(BTNWIDHT);
    //pMoveTop->setToolTip("Move selected items to the top");
    //pMoveTop->hide();
    //m_pButtons.push_back(pMoveTop);

    //QPushButton* pMoveBottom = new QPushButton(QIcon(":/down.png"), "", this);
    //pMoveBottom->setFixedWidth(BTNWIDHT);
    //pMoveBottom->setToolTip("Move selected items to the bottom");
    //pMoveBottom->hide();
    //m_pButtons.push_back(pMoveBottom);

    Update();

    connect(this, SIGNAL(currentChanged(int)), this, SLOT(reqTabChanged(int)));
    connect(pProcessing, SIGNAL(itemSelectionChanged()), this, SLOT(reqProcessingSelected()));
    //connect(pMoveTop, SIGNAL(pressed(void)), this, SLOT(reqMoveTop(void)));
    //connect(pMoveBottom, SIGNAL(pressed(void)), this, SLOT(reqMoveBottom(void)));
}

ProcessingQueueTabWidget::~ProcessingQueueTabWidget(void) {
}

void ProcessingQueueTabWidget::Update(void) {
    QTabBar* pTabBar = tabBar();
    for (int idx = 0; idx < TABLE_COUNTS; idx++) {
        const int counts = m_tables[idx]->rowCount();
        pTabBar->setTabText(idx, QString("%1 (%2)").arg(m_titles[idx]).arg(counts));
    }

    updateActionWidgets();
}

void ProcessingQueueTabWidget::updateActionWidgets(void) {
    const int tabh = std::max<int>(20, tabBar()->size().height());
    const int w = width();
    const int acts = m_pButtons.size();
    const int btn_width = BTNWIDHT;
    const int btn_margin = 2;

    for (int i = 0; i < acts; i++) {
        const int xoffset = w - (acts - i) * btn_width - (acts - i - 1) * btn_margin;
        m_pButtons.at(i)->setFixedHeight(tabh);
        m_pButtons.at(i)->move(xoffset, 0);
    }
}

void ProcessingQueueTabWidget::updateActionWidgetsStatus(void) {
    const int index = currentIndex();
    const bool enable = (index == 0);
    const bool show = (index == 0) && (m_tables[PROCESSING]->selectedItems().size() > 0);

    foreach(auto pBtn, m_pButtons) {
        pBtn->setEnabled(enable);
        if (show) pBtn->show();
        else pBtn->hide();
    }
}

void ProcessingQueueTabWidget::tabInserted(int index) {
    QTabWidget::tabInserted(index);
    updateActionWidgets();
}

void ProcessingQueueTabWidget::tabRemoved(int index) {
    QTabWidget::tabRemoved(index);
    updateActionWidgets();
}

void ProcessingQueueTabWidget::resizeEvent(QResizeEvent* e) {
    QTabWidget::resizeEvent(e);
    updateActionWidgets();
}

void ProcessingQueueTabWidget::showEvent(QShowEvent* e) {
    QTabWidget::showEvent(e);
    updateActionWidgets();
}

void ProcessingQueueTabWidget::reqTabChanged(int index) {
    updateActionWidgetsStatus();
}

void ProcessingQueueTabWidget::reqProcessingSelected(void) {
    updateActionWidgetsStatus();
}

void ProcessingQueueTabWidget::reqMoveTop(void) {
    auto selected = m_tables[PROCESSING]->selectedItems();

    QStringList strList;
    foreach(auto item, selected) {
        if (item->column() != 0) continue;

        const QString strPath = item->text();
        strList.push_back(strPath);
    }

    m_tables[PROCESSING]->Reorder(strList, true);
    m_tables[PROCESSING]->clearSelection();
}

void ProcessingQueueTabWidget::reqMoveBottom(void) {
    auto selected = m_tables[PROCESSING]->selectedItems();
    QStringList strList;
    foreach(auto item, selected) {
        if (item->column() != 0) continue;

        const QString strPath = item->text();
        strList.push_back(strPath);
    }

    m_tables[PROCESSING]->Reorder(strList, false);
    m_tables[PROCESSING]->clearSelection();
}
