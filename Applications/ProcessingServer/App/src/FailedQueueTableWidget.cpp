#include <QHeaderView>

#include "ProcessingQueue.h"
#include "FailedQueueTableWidget.h"

FailedQueueTableWidget::FailedQueueTableWidget(QWidget* parent)
    : QueueTableWidget(parent) {
    SetMaximumRows(2000);
}

FailedQueueTableWidget::~FailedQueueTableWidget(void) {
}


void FailedQueueTableWidget::Update(void) {
    auto list = updatedStatus();

    blockSignals(true);

    auto iter = list.begin();
    for (; iter != list.end(); iter++) {
        const QString path = iter.key();

        if (iter.value() != ProcessingQueue::FAILED) {
            RemoveRow(path);
        } else {
            const QString path = iter.key();

            auto item_iter = m_ItemMap.find(path);
            if (item_iter == m_ItemMap.end())		//new item
            {
                const int row_idx = CreateRow(path);
                auto item_status = getStatus(path);

                QTableWidgetItem* colitem = new QTableWidgetItem(sec2str(item_status.elapsed));
                colitem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                setItem(row_idx, 1, colitem);

                colitem = new QTableWidgetItem(QIcon(":/process_failed.png"), "");
                colitem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                setItem(row_idx, 2, colitem);

                scrollToItem(colitem, QAbstractItemView::PositionAtBottom);
            }
        }
    }

    blockSignals(false);
}

