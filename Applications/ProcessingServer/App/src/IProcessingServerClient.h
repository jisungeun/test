#pragma once
#include <memory>

#include <QObject>
#include <QString>

#include "ProcessingServerHost.h"
#include "ProgressList.h"

namespace processing_server {
    class ProcessingServerHost;
    class IProcessingServerClient : public QObject {
        Q_OBJECT
    public:
        IProcessingServerClient();
        IProcessingServerClient(const IProcessingServerClient& other);
        virtual ~IProcessingServerClient();

        auto SetHost(ProcessingServerHost* processingServerHost)->void;
        auto SetProgressList(const Interactor::ProgressList& progressList)->void;
    protected:
        auto HostAddOneTcf(const QString& dataPath, const bool& deconvolutionFlag) const ->void;
        auto HostAddOneTcfReprocessing(const QString& dataPath, const bool& deconvolutionFlag) const ->void;
        auto HostAddDirectory(const QString& directoryPath, const bool& deconvolutionFlag) const->void;
        auto HostAddDirectoryReprocessing(const QString& directoryPath, const bool& deconvolutionFlag) const->void;
        auto HostAddOnlineProcessingPath(const QString& path) const ->void;
        auto HostUpdateProgress()->void;
        auto GetProgressList() const->Interactor::ProgressList;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
