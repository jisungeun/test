#pragma once

#include <QAbstractButton>
#include <QPainter>

class PixmapButton : public QAbstractButton {
    Q_OBJECT

public:
    PixmapButton(QWidget* parent = 0) : QAbstractButton(parent) {}

    void setPixmap(const QPixmap& pm) { m_pixmap = pm; update(); }
    QSize sizeHint() const { return m_pixmap.size(); }

protected:
    void paintEvent(QPaintEvent* e) {
        QPainter p(this);
        p.drawPixmap(0, 0, m_pixmap);
    }

private:
    QPixmap m_pixmap;
};