#pragma once

#include <QString>
#include <QMap>
#include <QueueTableWidget.h>

class ProcessingQueueTableWidget : public QueueTableWidget {
    Q_OBJECT

public:
    ProcessingQueueTableWidget(QWidget* parent = 0);
    ~ProcessingQueueTableWidget(void);

    void Update(void) override;
};
