#pragma once

#include <QWidget>
#include <QString>

#include "QueueWidgetInterface.h"
#include "ui_ProcessingQueueProgressWidget.h"

class ProcessingQueueProgressWidget : public QWidget, public Ui::ProcessingQueueProgressWidget, public QueueWidgetInterface {
    Q_OBJECT

public:
    ProcessingQueueProgressWidget(QWidget* parent = 0);
    ~ProcessingQueueProgressWidget(void);

    void Update(void) override;

protected:
    QString sec2str(const int sec);

private:
    int m_nProcessed;
};