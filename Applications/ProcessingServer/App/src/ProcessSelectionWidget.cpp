#include "ProcessSelectionWidget.h"

#include <QFileDialog>

namespace processing_server {
    struct ProcessSelectionWidget::Impl {
        Impl() = default;
        ~Impl() = default;

        ProcessMode processMode{ ProcessMode::None };
        QString selectedPath{};
    };

    ProcessSelectionWidget::ProcessSelectionWidget(QWidget* parent)
        : QDialog(parent), d(new Impl) {
        setupUi(this);
        SetupConnection();
    }

    ProcessSelectionWidget::~ProcessSelectionWidget() = default;

    auto ProcessSelectionWidget::GetSelectedPath() const -> QString {
        return d->selectedPath;
    }

    auto ProcessSelectionWidget::GetProcessMode() const -> ProcessMode {
        return d->processMode;
    }

    auto ProcessSelectionWidget::SetupConnection() const -> void {
        connect(oneTcfButton, SIGNAL(clicked()), this, SLOT(OneTcfButtonClicked()));
        connect(oneTcfReprocessButton, SIGNAL(clicked()), this, SLOT(OneTcfReprocessButtonClicked()));
        connect(directoryButton, SIGNAL(clicked()), this, SLOT(DirectoryButtonClicked()));
        connect(directoryReprocessButton, SIGNAL(clicked()), this, SLOT(DirectoryReprocessButtonClicked()));
        connect(directoryOnlineButton, SIGNAL(clicked()), this, SLOT(DirectoryOnlineButtonClicked()));
    }

    auto ProcessSelectionWidget::GetPath(const QString& titleName)-> QString {
        const auto tcfFolderPath = QFileDialog::getExistingDirectory(this, titleName, "C:/");
        return tcfFolderPath;
    }

    void ProcessSelectionWidget::OneTcfButtonClicked() {
        const auto path = GetPath("Select directory path to process one TCF");
        if (path.isEmpty()) return;
        d->processMode = ProcessMode::OneTCF;
        d->selectedPath = path;
        close();
    }

    void ProcessSelectionWidget::OneTcfReprocessButtonClicked() {
        const auto path = GetPath("Select directory path to reprocess one TCF");
        if (path.isEmpty()) return;
        d->processMode = ProcessMode::OneTCFRe;
        d->selectedPath = path;
        close();
    }

    void ProcessSelectionWidget::DirectoryButtonClicked() {
        const auto path = GetPath("Select directory path to process multiple TCF");
        if (path.isEmpty()) return;
        d->processMode = ProcessMode::Directory;
        d->selectedPath = path;
        close();
    }

    void ProcessSelectionWidget::DirectoryReprocessButtonClicked() {
        const auto path = GetPath("Select directory path to reprocess multiple TCF");
        if (path.isEmpty()) return;
        d->processMode = ProcessMode::DirectoryRe;
        d->selectedPath = path;
        close();
    }

    void ProcessSelectionWidget::DirectoryOnlineButtonClicked() {
        const auto path = GetPath("Select directory path to add online processing");
        if (path.isEmpty()) return;
        d->processMode = ProcessMode::DirectoryOnline;
        d->selectedPath = path;
        close();
    }
}
