#pragma once

#include <memory>
#include "enum.h"

#include <QString>
#include <QDialog>

#include "ui_ProcessSelectionWidget.h"

namespace processing_server {
    BETTER_ENUM(ProcessMode, uint8_t, OneTCF, OneTCFRe, Directory, DirectoryRe, DirectoryOnline, None)
    class ProcessSelectionWidget : public QDialog, public Ui::ProcessSelectionWidget {
        Q_OBJECT
    public:
        ProcessSelectionWidget(QWidget* parent = nullptr);
        ~ProcessSelectionWidget();

        auto GetSelectedPath() const ->QString;
        auto GetProcessMode() const ->ProcessMode;
    private:
        struct Impl;
        std::shared_ptr<Impl> d;
    private:
        auto SetupConnection() const ->void;
        auto GetPath(const QString& titleName)->QString;
    private slots:
        void OneTcfButtonClicked();
        void OneTcfReprocessButtonClicked();
        void DirectoryButtonClicked();
        void DirectoryReprocessButtonClicked();
        void DirectoryOnlineButtonClicked();
    };
}