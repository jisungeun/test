#pragma once

#include <memory>
#include <QList>

#include "InputJobContents.h"

namespace processing_server {
    class InputJobScanner {
    public:
        explicit InputJobScanner(const QString& dataPath);
        ~InputJobScanner();

        auto Scan()->QList<UseCase::InputJobContents>;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        auto ScanCommonInfo()->void;

        auto PushBackInputJobContents(const Entity::DataType& dataType)->void;

        auto GenerateOutputPath()->QString;
        auto GenerateTotalFrameNumber()->int32_t;

        auto ScanHtIncluded()->bool;
        auto ScanFlBlueIncluded()->bool;
        auto ScanFlGreenIncluded()->bool;
        auto ScanFlRedIncluded()->bool;
        auto ScanPhaseIncluded()->bool;
        auto ScanBfIncluded()->bool;
    };
}

