#pragma once
#include <memory>

#include <QObject>
#include <QString>
#include <QList>

#include "DataDirectoryContents.h"
#include "InputJobContents.h"

namespace processing_server {
    class RootDirectoryMonitor : public QObject {
        Q_OBJECT
    public:
        typedef RootDirectoryMonitor Self;
        typedef std::shared_ptr<Self> Pointer;

        explicit RootDirectoryMonitor(const QString& rootDirectoryPath);
        RootDirectoryMonitor(const RootDirectoryMonitor& other);
        ~RootDirectoryMonitor();

        auto IsChanged() const ->bool;
        auto GetNewInputJobContents() const->QList<UseCase::InputJobContents>;
        auto IsCompleted() const ->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        auto Initialize(const QString& rootDirectoryPath)->void;
        auto SetConfigMapAndJobParameter()->void;
        auto ReadTimeFrameNumber() const->int32_t;
        auto SetDataIncluded()->void;
        auto SetDataDirectoryMonitors()->void;

        auto GenerateCommonInputJobContents() const->UseCase::InputJobContents;
        auto DataIncluded(const QString& dataString) const ->bool;
        auto DataNumber(const QString& dataString) const->int32_t;
        auto FlIncluded(const int32_t& channelIndex) const ->bool;
        auto GetNewDataDirectoryContents() const->QList<DataDirectoryContents>;
        auto ConvertDataToJobContents(const  QList<DataDirectoryContents>& dataDirectoryContentsList) const->QList<UseCase::InputJobContents>;
    };
}
