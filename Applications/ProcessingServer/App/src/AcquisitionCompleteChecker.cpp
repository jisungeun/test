#include "AcquisitionCompleteChecker.h"

#include <QDir>

namespace processing_server {
    auto AcquisitionCompleteChecker::Check(const QString& acquisitionPath) -> bool {
        const QDir acquisitionDir(acquisitionPath);
        const auto timeFrameEntry = acquisitionDir.entryList(QDir::Files | QDir::NoDotAndDotDot);

        if (!timeFrameEntry.contains("images.dat")) {
            return false;
        }

        if (!timeFrameEntry.contains("position.txt")) {
            return false;
        }

        if (!timeFrameEntry.contains("save_timestamp.txt")) {
            return false;
        }

        if (!timeFrameEntry.contains("acquisition_timestamp.txt")) {
            return false;
        }

        return true;
    }
}
