#include <ProcessingPerfTimer.h>

#include "ProcessingQueue.h"

static bool isFirstItem = true;

ProcessingPerfTimer::ProcessingPerfTimer(void)
    : m_nAverageTime(30)
    , m_nRemainTotal(0)
    , m_nCompletedTotal(0) {
    m_timer.start();
}

ProcessingPerfTimer::~ProcessingPerfTimer(void) {
}

ProcessingPerfTimer::Pointer ProcessingPerfTimer::GetInstance(void) {
    static ProcessingPerfTimer::Pointer theInstance(new ProcessingPerfTimer());
    return theInstance;
}

void ProcessingPerfTimer::AddTask(const QString& strPath) {
    Start();

    ProcessingQueue::Pointer queue = ProcessingQueue::GetInstance();
    const int nElements = queue->GetStatus(strPath).elements;
    m_remain[strPath] = nElements;
    m_nRemainTotal += nElements;

    CalcAverageTime();
}

void ProcessingPerfTimer::AddTask(const QStringList& strPathes) {
    Start();

    ProcessingQueue::Pointer queue = ProcessingQueue::GetInstance();
    foreach(auto strPath, strPathes) {
        const int nElements = queue->GetStatus(strPath).elements;
        m_remain[strPath] = nElements;
        m_nRemainTotal += nElements;
    }

    CalcAverageTime();
}

void ProcessingPerfTimer::Update(const QString& strPath) {
    ProcessingQueue::Pointer queue = ProcessingQueue::GetInstance();
    m_processing[strPath] = (queue->GetStatus(strPath).elements * queue->GetStatus(strPath).progress / 100.0);

    CalcAverageTime();
}

void ProcessingPerfTimer::Complete(const QString& strPath, bool bSuccess) {
    const int nElements = m_remain[strPath];

    m_remain.remove(strPath);
    m_processing.remove(strPath);

    m_nRemainTotal -= nElements;
    if (bSuccess) m_nCompletedTotal += nElements;

    isFirstItem = false;

    CalcAverageTime();
}

int ProcessingPerfTimer::GetTimeElapsed(void) {
    return m_timer.elapsed() / 1000;
}

int ProcessingPerfTimer::GetTimeRemaining(void) {
    if (isFirstItem) {
        const int remains = m_nRemainTotal - m_processing.size();
        const int estimated = 90 - (m_timer.elapsed() / 1000) + (remains * m_nAverageTime);
        return (estimated < 0) ? 10 : estimated;
    }

    float processed_now = 0;
    foreach(auto item, m_processing)
        processed_now += item;

    return std::max<int>(0, (m_nRemainTotal - processed_now) * m_nAverageTime);
}

void ProcessingPerfTimer::Start(void) {
    if (m_remain.size() == 0) {
        m_timer.restart();
        m_nRemainTotal = 0;
        m_nCompletedTotal = 0;
    }
}

void ProcessingPerfTimer::CalcAverageTime(void) {
    if (isFirstItem) return;
    else if (m_nCompletedTotal < 1) return;

    float processed_now = m_nCompletedTotal;
    foreach(auto item, m_processing)
        processed_now += item;

    m_nAverageTime = m_timer.elapsed() / processed_now / 1000;
}

