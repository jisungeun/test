#pragma once

#include <QString>
#include <QMap>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QueueWidgetInterface.h>

class QueueTableWidget : public QTableWidget, public QueueWidgetInterface {
    Q_OBJECT

public:
    QueueTableWidget(QWidget* parent = 0);
    ~QueueTableWidget(void);

    void Reorder(const QString& strPath, bool bFirst);
    void Reorder(const QStringList& strPathes, bool bFirst);

protected:
    void SetMaximumRows(int nRows);
    void Clear(void);
    int CreateRow(const QString& strPath);
    void RemoveRow(const QString& strPath);

protected:
    QMap<QString, QTableWidgetItem*> m_ItemMap;
    int m_nMaximumRows;
};
