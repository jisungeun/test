#include <TCLogger.h>
#include <QHeaderView>

#include "ProcessingQueue.h"
#include "ProcessingQueueTableWidget.h"

ProcessingQueueTableWidget::ProcessingQueueTableWidget(QWidget* parent)
    : QueueTableWidget(parent) {
}

ProcessingQueueTableWidget::~ProcessingQueueTableWidget(void) {
}

void ProcessingQueueTableWidget::Update(void) {
    QStringList list = updatedList();
    QMap<QString, int> statusList = updatedStatus();

    blockSignals(true);

    auto iter = list.begin();
    for (; iter != list.end(); iter++) {
        const QString& path = *(iter);
        const int status = statusList[path];

        if ((status != ProcessingQueue::WAITING) && (status != ProcessingQueue::PROCESSING)) {
            RemoveRow(path);
        } else {
            auto item_iter = m_ItemMap.find(path);

            if (item_iter == m_ItemMap.end())		//new item
            {
                const int row_idx = CreateRow(path);

                QTableWidgetItem* colitem = new QTableWidgetItem("00:00:00");
                colitem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                setItem(row_idx, 1, colitem);

                colitem = new QTableWidgetItem(QString("%1%").arg(0, 3));
                colitem->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
                setItem(row_idx, 2, colitem);
            } else {
                const int row_idx = item_iter.value()->row();
                auto item_status = getStatus(path);

                QTableWidgetItem* colitem = item(row_idx, 1);
                colitem->setText(sec2str(item_status.elapsed));

                colitem = item(row_idx, 2);
                colitem->setText(QString("%1%").arg(item_status.progress, 3));
            }
        }
    }

    blockSignals(false);
}
