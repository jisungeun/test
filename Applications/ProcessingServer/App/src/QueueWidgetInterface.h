#pragma once

#include "ProcessingQueue.h"

class QueueWidgetInterface {
public:
    QueueWidgetInterface(void);
    ~QueueWidgetInterface(void);

    virtual void Update(void) = 0;

protected:
    QStringList& waitingList(void);
    QStringList& processingList(void);
    QStringList& completedList(void);
    QStringList& failedList(void);
    QStringList& updatedList(void);
    QMap<QString, int>& updatedStatus(void);

    ProcessingQueue::Item& getStatus(const QString& strPath);

    QString sec2str(int elapsed);
};