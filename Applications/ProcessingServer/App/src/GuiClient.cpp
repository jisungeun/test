#include "GuiClient.h"

#include <QTimer>

namespace processing_server {
    struct GuiClient::Impl {
        Impl() = default;
        ~Impl() = default;
    };

    GuiClient::GuiClient()
        : d(new Impl()) {
    }

    GuiClient::~GuiClient() = default;

    auto GuiClient::AddOneTcf(const QString& dataPath, const bool& deconvolutionFlag) const -> void {
        if (dataPath.isEmpty()) return;
        HostAddOneTcf(dataPath, deconvolutionFlag);
    }

    auto GuiClient::AddOneTcfReprocessing(const QString& dataPath, const bool& deconvolutionFlag) const -> void {
        if (dataPath.isEmpty()) return;
        HostAddOneTcfReprocessing(dataPath, deconvolutionFlag);
    }

    auto GuiClient::AddOnlineProcessingPath(const QString& path) const -> void {
        if (path.isEmpty()) return;
        HostAddOnlineProcessingPath(path);
    }

    auto GuiClient::UpdateProgress() ->Interactor::ProgressList {
        HostUpdateProgress();
        return GetProgressList();
    }
}
