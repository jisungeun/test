#include <QHeaderView>
#include <TCLogger.h>

#include "ProcessingQueue.h"
#include "QueueTableWidget.h"

QueueTableWidget::QueueTableWidget(QWidget* parent)
    : QTableWidget(parent)
    , QueueWidgetInterface()
    , m_nMaximumRows(0) {
    setColumnCount(3);
    setHorizontalHeaderLabels(QStringList() << "Path" << "Sec" << "");
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setColumnWidth(1, 70);
    setColumnWidth(2, 45);

    QHeaderView* pHoriHeader = horizontalHeader();
    pHoriHeader->setSectionResizeMode(0, QHeaderView::Stretch);
    pHoriHeader->setSectionResizeMode(1, QHeaderView::Fixed);
    pHoriHeader->setSectionResizeMode(2, QHeaderView::Fixed);
}

QueueTableWidget::~QueueTableWidget(void) {
}

void QueueTableWidget::Reorder(const QString& strPath, bool bFirst) {
    Reorder(QStringList(strPath), bFirst);
}

void QueueTableWidget::Reorder(const QStringList& strPathes, bool bFirst) {
    blockSignals(true);

    QStringList strNewList;

    if (bFirst) strNewList = strPathes;

    const int rows = rowCount();
    for (int idx = 0; idx < rows; idx++) {
        const QString strPath = item(idx, 0)->text();
        if (strPathes.contains(strPath, Qt::CaseInsensitive)) continue;
        strNewList.push_back(strPath);
    }

    if (!bFirst) strNewList.append(strPathes);

    int row_idx = 0;
    foreach(auto strPath, strNewList) {
        item(row_idx, 0)->setText(strPath);
        item(row_idx, 1)->setText("00:00:00");
        item(row_idx, 2)->setText("0%");
        m_ItemMap[strPath] = item(row_idx, 0);
        row_idx++;
    }

    blockSignals(false);
}

void QueueTableWidget::SetMaximumRows(int nRows) {
    m_nMaximumRows = nRows;
}

void QueueTableWidget::Clear(void) {
    blockSignals(true);
    clearContents();
    blockSignals(false);

    m_ItemMap.clear();
}

int QueueTableWidget::CreateRow(const QString& strPath) {
    if ((m_nMaximumRows > 0) && (rowCount() > m_nMaximumRows)) {
        removeRow(0);
    }

    const int row_idx = rowCount();
    setRowCount(row_idx + 1);

    QTableWidgetItem* colitem = new QTableWidgetItem(strPath);
    setItem(row_idx, 0, colitem);
    m_ItemMap[strPath] = colitem;

    //QLOG_INFO() << "Table [" << row_idx << "] " << strPath;

    return row_idx;
}

void QueueTableWidget::RemoveRow(const QString& strPath) {
    auto item_iter = m_ItemMap.find(strPath);
    if (item_iter != m_ItemMap.end()) {
        const int row_idx = item_iter.value()->row();
        removeRow(row_idx);

        m_ItemMap.remove(strPath);
    }
}