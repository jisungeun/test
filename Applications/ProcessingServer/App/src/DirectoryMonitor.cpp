#define LOGGER_TAG "TomoProcessingServer::DirectoryMonitor"
#include "DirectoryMonitor.h"

#include <QList>

#include <TCLogger.h>

#include "RootDirectoryFinder.h"
#include "RootDirectoryMonitor.h"

namespace processing_server {
    struct DirectoryMonitor::Impl {
        Impl() = default;
        ~Impl() = default;

        QStringList topDirectoryPaths{};

        QStringList handlingRootDirectories{};
        QList<RootDirectoryMonitor::Pointer> rootDirectoryMonitorList{};
    };

    DirectoryMonitor::DirectoryMonitor()
        : d(new Impl()) {
    }

    DirectoryMonitor::DirectoryMonitor(const QString& topDirectoryPath)
        : DirectoryMonitor() {
        AddPathToMonitor(topDirectoryPath);
    }

    DirectoryMonitor::~DirectoryMonitor() = default;

    auto DirectoryMonitor::AddPathToMonitor(const QString& topDirectoryPath)->void {
        d->topDirectoryPaths.push_back(topDirectoryPath);
        QLOG_INFO() << "Directory Path is added : " << topDirectoryPath;
    }

    auto DirectoryMonitor::GetNewInputJobContents() -> QList<UseCase::InputJobContents> {
        QList<UseCase::InputJobContents> newInputJobContentsList;

        for (const auto& topDirectoryPath : d->topDirectoryPaths) {
            UpdateDirectories(topDirectoryPath);
        }

        for (auto iter = d->rootDirectoryMonitorList.begin();
            iter != d->rootDirectoryMonitorList.end();
            ) {
            const auto rootDirectoryMonitor = *iter;
            if (rootDirectoryMonitor->IsChanged()) {
                newInputJobContentsList.append(rootDirectoryMonitor->GetNewInputJobContents());
            }

            if (rootDirectoryMonitor->IsCompleted()) {
                iter = d->rootDirectoryMonitorList.erase(iter);
            } else {
                ++iter;
            }
        }

        return newInputJobContentsList;
    }

    auto DirectoryMonitor::UpdateDirectories(const QString& directoryPath) -> void {
        const auto rootDirectoryList = RootDirectoryFinder(directoryPath).Find();
        for (const auto& rootDirectory : rootDirectoryList) {
            const auto rootDirectoryPath = rootDirectory.rootDirectoryPath;
            const auto rootDirectoryIsNotBeingHandled = !d->handlingRootDirectories.contains(rootDirectoryPath);
            if (rootDirectoryIsNotBeingHandled) {

                if (!rootDirectory.tcfExists) {
                    RootDirectoryMonitor::Pointer rootDirectoryMonitor(new RootDirectoryMonitor(rootDirectoryPath));
                    d->rootDirectoryMonitorList.push_back(rootDirectoryMonitor);
                    d->handlingRootDirectories.push_back(rootDirectoryPath);
                }
            }
        }
    }
}
