#pragma once
#include <memory>
#include <QObject>
#include <QList>

#include "DataDirectoryContents.h"

namespace processing_server {
    class DataDirectoryMonitorFl : public QObject {
        Q_OBJECT
    public:
        typedef DataDirectoryMonitorFl Self;
        typedef std::shared_ptr<Self> Pointer;

        struct FlRequirement {
            QString directoryPath{};
            int32_t expectedNumberOfData{ 0 };
            bool blueIncluded{ false };
            bool greenIncluded{ false };
            bool redIncluded{ false };
        };

        explicit DataDirectoryMonitorFl(const FlRequirement& flRequirement);
        ~DataDirectoryMonitorFl();

        auto ScanChanged() const ->bool;
        auto GetNewDataDirectoryContents()->QList<DataDirectoryContents>;
        auto IsCompleted() const ->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        static auto GenerateDataDirectoryContents(const QString& channelPath)->DataDirectoryContents;
    };
}
