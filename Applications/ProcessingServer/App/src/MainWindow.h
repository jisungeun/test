#pragma once

#include <QSystemTrayIcon>
#include <QDialog>
#include <QMap>
#include <QElapsedTimer>
#include <QTimer>
#include <QProgressDialog>

#include "Processor.h"
#include "Response.h"

#include "GuiClient.h"

#include "ui_MainPanel.h"

class MainWindow : public QDialog, public Ui::MainPanel {
    Q_OBJECT

public:
    enum {
        STATUS_IDLE,
        STATUS_BUSY,
        STATUS_ERROR
    };

public:
    MainWindow(QWidget* parent = 0);
    MainWindow(bool isManual, processing_server::GuiClient* guiClient, QWidget* parent = 0);
    ~MainWindow(void);

    void setVisible(bool visible);

protected:
    void Start(void);
    void Stop(bool showProgressDialog = false);
    void SetGuiClient(processing_server::GuiClient* guiClient);

private slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void reqRunStateChanged(int state);
    void reqSetup(void);
    void reqQuit(void);
    void reqChangeFolder(void);
    void reqReportStatus();
    void reqFinishProcess();
    void reqRequestProcess(const QString& strOption);
    void reqClientConnected(bool bConnected);
    void reqUpdateList(void);
    void AddJobClicked();
    void UpdateProgress();

private:
    void constructor();
    void setIcon(int index);
    void createTrayActions();
    void createTrayIcon();
    QString secs2str(qint64 sec);
    int find(const QString& strTop, bool isReprocess, QStringList& pathes);

    auto StartPeriodicProgressTimer(const uint32_t& msec)->void;

private:
    QMap<int, QIcon> m_trayIcons;
    QMap<int, QPixmap> m_statusIcons;
    QMap<int, QString> m_trayTooltips;

    QAction* minimizeAction;
    QAction* restoreAction;
    QAction* quitAction;

    QSystemTrayIcon* trayIcon;
    QMenu* trayIconMenu;

    QTimer m_updateTimer;

    ResponseStatus m_status;

    QTimer m_stopTimer;
    int m_nStopCount;

    QTimer m_sysStatTimer;
    QTimer progressTimer;

    processing_server::GuiClient* guiClient{};

    QMap<QString, float> progressMap{};
    QMap<QString, QElapsedTimer> elapsedTimerMap{};
};
