#pragma once

#include <QDialog>

#include "ui_SetupDialog.h"

class SetupDialog : public QDialog, public Ui::SetupDialog {
    Q_OBJECT

public:
    SetupDialog(QWidget* parent = nullptr);

    void UpdateConfig(void);

protected:
    void UpdateDevices(void);

protected slots:
    void reqModeSelected(QAbstractButton* pButton, bool bChecked);
    void reqEnableBGProc(int state);
};
