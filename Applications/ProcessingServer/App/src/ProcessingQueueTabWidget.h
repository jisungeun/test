#pragma once

#include <QTabWidget>
#include <QMap>
#include <QPushButton>

#include "QueueWidgetInterface.h"
#include "QueueTableWidget.h"

class ProcessingQueueTabWidget : public QTabWidget, public QueueWidgetInterface {
    Q_OBJECT

protected:
    enum {
        PROCESSING,
        COMPLETED,
        FAILED,
        TABLE_COUNTS
    };

public:
    ProcessingQueueTabWidget(QWidget* parent = 0);
    ~ProcessingQueueTabWidget(void);

    void Update(void) override;

protected:
    void updateActionWidgets(void);
    void updateActionWidgetsStatus(void);

    void tabInserted(int index) override;
    void tabRemoved(int index) override;
    void resizeEvent(QResizeEvent* e) override;
    void showEvent(QShowEvent* e) override;

protected slots:
    void reqTabChanged(int index);
    void reqProcessingSelected(void);
    void reqMoveTop(void);
    void reqMoveBottom(void);

private:
    QMap<int, QueueTableWidget*> m_tables;
    QMap<int, QString> m_titles;

    QList<QPushButton*> m_pButtons;
};
