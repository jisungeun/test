#pragma once

#include <memory>
#include <QMutex>
#include <QString>
#include <QDateTime>

class Response
{
public:
	virtual QString toString(void) = 0;

protected:
	QMutex m_mutex;
};

class ResponseDone : public Response
{
public:
	ResponseDone(const QString& strCmd, bool bOK) : m_strCmd(strCmd), m_bOK(bOK) {}
	QString toString(void) override;

private:
	QString m_strCmd;
	bool m_bOK;
};

class ResponseStatus : public Response
{
	class Result
	{
	public:
		inline bool operator==(const Result& other)
		{
			return (m_strPath == other.m_strPath);
		}
	public:
		QString m_strPath;
		bool m_bCompleted;
	};

public:
	ResponseStatus() {}

	void SetTotalProgress(int nProgress, const QString& strElapsed = "00:00:00");
	void SetCompleted(const QString& strPath, bool bSuccess);
	void ClearCompleted(const QString& strPath);

	void Clear();

	QString toString(void) override;

private:
	int m_nTotalProgress;
	QString m_strTotalElapsed;

	QList<Result> m_ltCompleted;
};

class ResponseList : public Response
{
public:
	ResponseList(const QStringList& pathes) : m_strPathes(pathes) {}
	QString toString(void) override;

private:
	QStringList m_strPathes;
};