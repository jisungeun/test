#include "ProcessingQueue.h"
#include "QueueWidgetInterface.h"

QueueWidgetInterface::QueueWidgetInterface(void) {
    ProcessingQueue::GetInstance()->AddWidget(this);
}

QueueWidgetInterface::~QueueWidgetInterface(void) {
}

QStringList& QueueWidgetInterface::waitingList(void) {
    ProcessingQueue::Pointer queue = ProcessingQueue::GetInstance();
    return queue->waitingList();
}

QStringList& QueueWidgetInterface::processingList(void) {
    ProcessingQueue::Pointer queue = ProcessingQueue::GetInstance();
    return queue->processingList();
}

QStringList& QueueWidgetInterface::completedList(void) {
    ProcessingQueue::Pointer queue = ProcessingQueue::GetInstance();
    return queue->completedList();
}

QStringList& QueueWidgetInterface::failedList(void) {
    ProcessingQueue::Pointer queue = ProcessingQueue::GetInstance();
    return  queue->failedList();
}

QStringList& QueueWidgetInterface::updatedList(void) {
    ProcessingQueue::Pointer queue = ProcessingQueue::GetInstance();
    return queue->updatedList();
}

QMap<QString, int>& QueueWidgetInterface::updatedStatus(void) {
    ProcessingQueue::Pointer queue = ProcessingQueue::GetInstance();
    return queue->updatedStatus();
}

ProcessingQueue::Item& QueueWidgetInterface::getStatus(const QString& strPath) {
    ProcessingQueue::Pointer queue = ProcessingQueue::GetInstance();
    return queue->GetStatus(strPath);
}

QString QueueWidgetInterface::sec2str(int elapsed) {
    const int hours = elapsed / 3600;
    const int mins = (elapsed - (hours * 3600)) / 60;
    const int secs = (elapsed - (hours * 3600) - (mins * 60));

    return (QString("%1:%2:%3").arg(hours, 2, 10, QLatin1Char('0')).arg(mins, 2, 10, QLatin1Char('0')).arg(secs, 2, 10, QLatin1Char('0')));
}