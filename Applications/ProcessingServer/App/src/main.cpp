#define LOGGER_TAG "ProcessingServer::main"

#include <QtGui>
#include <QFile>
#include <QMessageBox>

#include <TCLogger.h>

#include "MainInitializer.h"
#include "MainWindow.h"
#include "ProcessingServerHost.h"

int main(int argc, char* argv[]) {
    const auto hMutex = OpenMutex(MUTEX_ALL_ACCESS, 0, "TomoProcessingServer");
    if (!hMutex) {
        CreateMutex(0, 0, "TomoProcessingServer");
    } else {
        QApplication app(argc, argv);
        processing_server::MainInitializer::ApplyStyle(app);

        QMessageBox::warning(nullptr, "ProcessingServer Overlap", "ProcessingServer is already opened, please close the ProcessingServer");
        return 0;
    }

    processing_server::MainInitializer::Initialize();

    QApplication app(argc, argv);
    processing_server::MainInitializer::ApplyStyle(app);
    app.setApplicationName("TomoProcessingServer");
    app.setOrganizationName("TomoCube, Inc.");
    app.setOrganizationDomain("www.tomocube.com");

    QLOG_INFO() << "TomoProcessingServer is started";

    processing_server::GuiClient guiClient;
    processing_server::ProcessingServerHost processingServerHost;
    processingServerHost.RegisterClient(&guiClient);

    const auto manualMode = false;
    MainWindow window(manualMode, &guiClient);
    window.show();

    return app.exec();
}
