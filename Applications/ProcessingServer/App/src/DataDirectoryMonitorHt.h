#pragma once
#include <memory>
#include <QObject>
#include <QList>

#include "DataDirectoryContents.h"

namespace processing_server {
    class DataDirectoryMonitorHt : public QObject {
        Q_OBJECT
    public:
        typedef DataDirectoryMonitorHt Self;
        typedef std::shared_ptr<Self> Pointer;

        DataDirectoryMonitorHt(const QString& directoryPath, const int32_t& expectedNumberOfData);
        ~DataDirectoryMonitorHt();

        auto ScanChanged() const ->bool;
        auto GetNewDataDirectoryContents()->QList<DataDirectoryContents>;
        auto IsCompleted() const ->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        static auto GenerateDataDirectoryContents(const QString& timeFramePath)->DataDirectoryContents;
    };
}
