#include "ProcessingQueue.h"
#include "ProcessingPerfTimer.h"
#include "ProcessingQueueProgressWidget.h"

ProcessingQueueProgressWidget::ProcessingQueueProgressWidget(QWidget* parent)
    : QWidget(parent)
    , QueueWidgetInterface()
    , m_nProcessed(0) {
    setupUi(this);
}

ProcessingQueueProgressWidget::~ProcessingQueueProgressWidget(void) {
}

void ProcessingQueueProgressWidget::Update(void) {
    ProcessingQueue::Pointer queue = ProcessingQueue::GetInstance();

    auto list = updatedStatus();

    for (auto iter = list.begin(); iter != list.end(); iter++) {
        const QString path = iter.key();

        auto status_item = getStatus(path);

        switch (iter.value()) {
        case ProcessingQueue::WAITING:;
            break;
        case ProcessingQueue::PROCESSING:
            break;
        case ProcessingQueue::COMPLETED:
            m_nProcessed++;
        case ProcessingQueue::FAILED:
            break;
        case ProcessingQueue::DELETED:
            break;
        }
    }

    const int remain_items = processingList().size() + waitingList().size();
    m_pProcessingCountEdit->setText(QString("%1").arg(remain_items));
    m_pProcessedCountEdit->setText(QString("%1").arg(m_nProcessed));

    ProcessingPerfTimer::Pointer perfTimer = ProcessingPerfTimer::GetInstance();
    m_pElapsedTimeEdit->setText(sec2str(perfTimer->GetTimeElapsed()));
    m_pRemainTimeEdit->setText(sec2str(perfTimer->GetTimeRemaining()));
}

QString ProcessingQueueProgressWidget::sec2str(const int total_sec) {
    const int hours = (total_sec / 3600);
    const int mins = (total_sec - (hours * 3600)) / 60;
    const int secs = (total_sec - (hours * 3600) - (mins * 60));

    return QString("%1:%2:%3").arg(hours, 2, 10, QLatin1Char('0')).arg(mins, 2, 10, QLatin1Char('0')).arg(secs, 2, 10, QLatin1Char('0'));
}