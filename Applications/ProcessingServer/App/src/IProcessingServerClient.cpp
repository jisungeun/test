#include "IProcessingServerClient.h"

namespace processing_server {
    struct IProcessingServerClient::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        ProcessingServerHost* processingServerHost{};
        Interactor::ProgressList progressList;
    };

    IProcessingServerClient::IProcessingServerClient()
        : d(new Impl()) {
    }

    IProcessingServerClient::IProcessingServerClient(const IProcessingServerClient& other)
        : d(new Impl(*other.d)) {
    }

    IProcessingServerClient::~IProcessingServerClient() = default;

    auto IProcessingServerClient::SetHost(ProcessingServerHost* processingServerHost)->void {
        d->processingServerHost = processingServerHost;
    }

    auto IProcessingServerClient::SetProgressList(const Interactor::ProgressList& progressList)->void {
        d->progressList = progressList;
    }

    auto IProcessingServerClient::HostAddOneTcf(const QString& dataPath, const bool& deconvolutionFlag) const -> void {
        d->processingServerHost->AddOneTcf(dataPath, deconvolutionFlag);
    }

    auto IProcessingServerClient::HostAddOneTcfReprocessing(const QString& dataPath, const bool& deconvolutionFlag)
        const -> void {
        d->processingServerHost->AddOneTcfReprocessing(dataPath, deconvolutionFlag);
    }

    auto IProcessingServerClient::HostAddDirectory(const QString& directoryPath, const bool& deconvolutionFlag) const
        -> void {
        d->processingServerHost->AddDirectory(directoryPath, deconvolutionFlag);
    }

    auto IProcessingServerClient::HostAddDirectoryReprocessing(const QString& directoryPath,
        const bool& deconvolutionFlag) const -> void {
        d->processingServerHost->AddDirectoryReprocessing(directoryPath, deconvolutionFlag);
    }

    auto IProcessingServerClient::HostAddOnlineProcessingPath(const QString& path) const -> void {
        d->processingServerHost->AddOnlineProcessingPath(path);
    }

    auto IProcessingServerClient::HostUpdateProgress() -> void {
        d->processingServerHost->UpdateProgressToClient(this);
    }

    auto IProcessingServerClient::GetProgressList() const -> Interactor::ProgressList {
        return d->progressList;
    }
}
