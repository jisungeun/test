#include "InputJobScanner.h"

#include <QDir>

#include "FileUtility.h"
#include "TomoBuilderUtility.h"

namespace processing_server {
    struct InputJobScanner::Impl {
        Impl() = default;
        ~Impl() = default;

        QString dataPath{};
        bool htIncluded{ false };
        bool flBlueIncluded{ false };
        bool flGreenIncluded{ false };
        bool flRedIncluded{ false };
        bool bfIncluded{ false };
        bool phaseIncluded{ false };
        int32_t totalFrameNumber{ 0 };

        QString outputPath{};

        QList<UseCase::InputJobContents> inputJobContentsList;
    };

    InputJobScanner::InputJobScanner(const QString& dataPath)
        : d(new Impl()) {
        auto formattedDataPath = dataPath;
        d->dataPath = formattedDataPath.replace("\\", "/");;
    }

    InputJobScanner::~InputJobScanner() = default;

    auto InputJobScanner::Scan() -> QList<UseCase::InputJobContents> {
        ScanCommonInfo();
        d->outputPath = GenerateOutputPath();

        if (d->htIncluded) {
            PushBackInputJobContents(Entity::DataType::HT);
        }

        if (d->flBlueIncluded) {
            PushBackInputJobContents(Entity::DataType::FLBLUE);
        }

        if (d->flGreenIncluded) {
            PushBackInputJobContents(Entity::DataType::FLGREEN);
        }

        if (d->flRedIncluded) {
            PushBackInputJobContents(Entity::DataType::FLRED);
        }

        if (d->bfIncluded) {
            PushBackInputJobContents(Entity::DataType::BF);
        }

        if (d->phaseIncluded) {
            PushBackInputJobContents(Entity::DataType::PHASE);
        }

        return d->inputJobContentsList;
    }

    auto InputJobScanner::ScanCommonInfo() -> void {
        d->htIncluded = ScanHtIncluded();
        d->flBlueIncluded = ScanFlBlueIncluded();
        d->flGreenIncluded = ScanFlGreenIncluded();
        d->flRedIncluded = ScanFlRedIncluded();
        d->phaseIncluded = ScanPhaseIncluded();
        d->bfIncluded = ScanBfIncluded();
        d->totalFrameNumber = GenerateTotalFrameNumber();
    }

    auto InputJobScanner::PushBackInputJobContents(const Entity::DataType& dataType) -> void {

        for (auto timeFrameIndex = 0; timeFrameIndex < d->totalFrameNumber; ++timeFrameIndex) {
            UseCase::InputJobContents contents;
            contents.SetPaths(d->dataPath, d->outputPath);
            contents.SetDataType(dataType);
            contents.SetTimeFrames(d->totalFrameNumber, timeFrameIndex);
            contents.SetDataIncludedFlags(d->htIncluded, d->flBlueIncluded, d->flGreenIncluded, d->flRedIncluded,
                d->phaseIncluded, d->bfIncluded);

            d->inputJobContentsList.push_back(contents);
        }
    }

    auto InputJobScanner::GenerateOutputPath() -> QString {
        QString fileName = TC::GetPathName(d->dataPath);
        const auto outputPath = QString("%1/%2.TCF").arg(d->dataPath).arg(fileName);
        return outputPath;
    }

    auto InputJobScanner::GenerateTotalFrameNumber() -> int32_t {
        const auto configPath = QString("%1/config.dat").arg(d->dataPath);
        QMap<QString, QString> configMap;
        TC::LoadFullParameter(configPath, configMap);

        const QStringList nameFilter("*");

        QStringList subDirList{};
        QString directoryPath{};
        if (d->htIncluded) {
            directoryPath = QString("%1/data3d").arg(d->dataPath);
        } else if (d->flBlueIncluded) {
            directoryPath = QString("%1/data3dfl").arg(d->dataPath);
        } else if (d->flGreenIncluded) {
            directoryPath = QString("%1/data3dfl").arg(d->dataPath);
        } else if (d->flRedIncluded) {
            directoryPath = QString("%1/data3dfl").arg(d->dataPath);
        } else if (d->phaseIncluded) {
            directoryPath = QString("%1/data2d").arg(d->dataPath);
        } else if (d->bfIncluded) {
            directoryPath = QString("%1/databf").arg(d->dataPath);
        }
        subDirList = QDir(directoryPath).entryList(nameFilter, QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

        const auto totalFrameNumber = static_cast<int32_t>(subDirList.size());


        return totalFrameNumber;
    }

    auto InputJobScanner::ScanHtIncluded() -> bool {
        return !TC::FindFolders(d->dataPath, "data3d").isEmpty();
    }

    auto InputJobScanner::ScanFlBlueIncluded() -> bool {
        return !TC::FindFolders(d->dataPath, "ch0").isEmpty();

    }

    auto InputJobScanner::ScanFlGreenIncluded() -> bool {
        return !TC::FindFolders(d->dataPath, "ch1").isEmpty();
    }

    auto InputJobScanner::ScanFlRedIncluded() -> bool {
        return !TC::FindFolders(d->dataPath, "ch2").isEmpty();
    }

    auto InputJobScanner::ScanPhaseIncluded() -> bool {
        return !TC::FindFolders(d->dataPath, "data2d").isEmpty();
    }

    auto InputJobScanner::ScanBfIncluded() -> bool {
        return !TC::FindFolders(d->dataPath, "databf").isEmpty();
    }
}
