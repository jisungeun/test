#pragma once
#include <memory>

#include <QObject>
#include <QString>


namespace processing_server {
    class TemporalWidgetLogic : public QObject {
        Q_OBJECT
    public:
        TemporalWidgetLogic();
        ~TemporalWidgetLogic();

        auto AddOneTcf(const QString& dataPath)->void;
        auto AddDirectory(const QString& directoryPath)->void;
        auto AddOneTcfReprocess(const QString& dataPath)->void;
        auto AddDirectoryReprocess(const QString& directoryPath)->void;
        auto AddMonitor(const QString& directoryPath)->void;
    private:
        auto Initialization() const ->void;
        auto JobManagerSetting() const ->void;
        auto ProcessorRegistrySetting() const->void;
        auto StartPeriodicTimer(const uint32_t& msec)->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private slots:
        void Update();

    };
}
