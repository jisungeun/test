#pragma once

#include <QString>

namespace processing_server {
    class AcquisitionCompleteChecker {
    public:
        static auto Check(const QString& acquisitionPath)->bool;
    };
}
