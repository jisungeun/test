#include "DataDirectoryMonitorFl.h"

#include "AcquisitionCompleteChecker.h"

#include <QFileSystemWatcher>
#include <QMap>
#include <QDir>

namespace processing_server {
    BETTER_ENUM(Channel, uint8_t, Blue, Green, Red);

    QMap<QString, Channel::_enumerated> channelNameToChannel{ {"ch0",Channel::Blue},{"ch1",Channel::Green},{"ch2",Channel::Red} };
    QMap<Channel::_enumerated, QString> channelToChannelName{ {Channel::Blue,"ch0"},{Channel::Green,"ch1"},{Channel::Red,"ch2"} };

    struct DataDirectoryMonitorFl::Impl {
        Impl() = default;
        ~Impl() = default;
        QString dataDirectoryPath{};

        int32_t expectedNumberOfData{ 0 };
        int32_t completedDataNumber[Channel::_size()]{ 0, 0, 0 };
        bool channelExist[Channel::_size()]{ false, false, false };

        QMap<Channel::_enumerated, QStringList> changedChannelPathListMap{
            {Channel::Blue,QStringList{}},
            {Channel::Green,QStringList{}},
            {Channel::Red,QStringList{}} };
    };

    DataDirectoryMonitorFl::DataDirectoryMonitorFl(const FlRequirement& flRequirement)
        : d(new Impl()) {
        d->dataDirectoryPath = flRequirement.directoryPath;
        d->expectedNumberOfData = flRequirement.expectedNumberOfData;
        if (flRequirement.blueIncluded) {
            d->channelExist[Channel::Blue] = true;
        }
        if (flRequirement.greenIncluded) {
            d->channelExist[Channel::Green] = true;
        }
        if (flRequirement.redIncluded) {
            d->channelExist[Channel::Red] = true;
        }

    }

    DataDirectoryMonitorFl::~DataDirectoryMonitorFl() = default;

    auto DataDirectoryMonitorFl::ScanChanged() const -> bool {
        int32_t numberOfAcquisitionData[Channel::_size()]{ 0,0,0 };
        const auto timeFrameEntry =
            QDir(d->dataDirectoryPath).entryList(QDir::NoDotAndDotDot | QDir::Dirs, QDir::Name);

        for (const auto& timeFrameName : timeFrameEntry) {
            const auto timeFramePath = QString("%1/%2").arg(d->dataDirectoryPath).arg(timeFrameName);

            const auto channelEntry = QDir(timeFramePath).entryList(QDir::NoDotAndDotDot | QDir::Dirs, QDir::Name);

            for (const auto& channelName : channelEntry) {
                const auto channelPath = QString("%1/%2").arg(timeFramePath).arg(channelName);
                if (AcquisitionCompleteChecker::Check(channelPath)) {
                    ++numberOfAcquisitionData[channelNameToChannel[channelName]];
                }
            }
        }

        for (auto channelIndex = 0; channelIndex < Channel::_size(); ++channelIndex) {
            const auto channel = Channel::_from_index(channelIndex);
            for (auto timeFrameIndex = d->completedDataNumber[channel];
                timeFrameIndex < numberOfAcquisitionData[channel];
                ++timeFrameIndex) {
                const auto timeFrameString = QString("%1").arg(timeFrameIndex, 6, 10, QChar('0'));
                const auto timeFramePath = QString("%1/%2").arg(d->dataDirectoryPath).arg(timeFrameString);
                const auto channelPath = QString("%1/%2").arg(timeFramePath).arg(channelToChannelName[channel]);

                auto& changedTimeFramePathList = d->changedChannelPathListMap[channel];
                changedTimeFramePathList.push_back(channelPath);
            }
        }

        const auto blueIsChanged = numberOfAcquisitionData[Channel::Blue] != d->completedDataNumber[Channel::Blue];
        const auto greenIsChanged = numberOfAcquisitionData[Channel::Green] != d->completedDataNumber[Channel::Green];
        const auto redIsChanged = numberOfAcquisitionData[Channel::Red] != d->completedDataNumber[Channel::Red];

        return blueIsChanged || greenIsChanged || redIsChanged;
    }

    auto DataDirectoryMonitorFl::GetNewDataDirectoryContents() -> QList<DataDirectoryContents> {
        QList<DataDirectoryContents> dataDirectoryContentsList;
        for (auto channelIndex = 0; channelIndex < Channel::_size(); ++channelIndex) {
            const auto channel = Channel::_from_index(channelIndex);
            for (auto& channelPath : d->changedChannelPathListMap[channel]) {
                const auto dataDirectoryContents = GenerateDataDirectoryContents(channelPath);
                dataDirectoryContentsList.push_back(dataDirectoryContents);
                ++d->completedDataNumber[channel];
            }

            d->changedChannelPathListMap[channel].clear();
        }
        return dataDirectoryContentsList;
    }

    auto DataDirectoryMonitorFl::IsCompleted() const -> bool {
        auto blueCompleted{ true }, greenCompleted{ true }, redCompleted{ true };

        if (d->channelExist[Channel::Blue]) {
            blueCompleted = d->completedDataNumber[Channel::Blue] == d->expectedNumberOfData;
        }
        if (d->channelExist[Channel::Green]) {
            greenCompleted = d->completedDataNumber[Channel::Green] == d->expectedNumberOfData;
        }
        if (d->channelExist[Channel::Red]) {
            redCompleted = d->completedDataNumber[Channel::Red] == d->expectedNumberOfData;
        }

        return blueCompleted && greenCompleted && redCompleted;
    }

    auto DataDirectoryMonitorFl::GenerateDataDirectoryContents(const QString& channelPath) -> DataDirectoryContents {
        const QDir channelDir(channelPath);
        const auto channelName = channelDir.dirName();

        Entity::DataType dataType{ Entity::DataType::FLBLUE };
        if (channelName == "ch0") {
            dataType = Entity::DataType::FLBLUE;
        } else if (channelName == "ch1") {
            dataType = Entity::DataType::FLGREEN;
        } else if (channelName == "ch2") {
            dataType = Entity::DataType::FLRED;
        }

        const auto timeFrameDirectoryPathDotDot = QString("%1/..").arg(channelPath);
        const auto timeFramePath = QDir(timeFrameDirectoryPathDotDot).absolutePath();
        const auto timeFrameIndex = QDir(timeFramePath).dirName().toInt();

        DataDirectoryContents dataDirectoryContents{ dataType, timeFrameIndex };
        return dataDirectoryContents;
    }
}
