#pragma once

#include <QString>
#include <QMap>
#include <QueueTableWidget.h>

class FailedQueueTableWidget : public QueueTableWidget {
    Q_OBJECT

public:
    FailedQueueTableWidget(QWidget* parent = 0);
    ~FailedQueueTableWidget(void);

    void Update(void) override;
};
