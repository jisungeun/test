#pragma once

#include <memory>
#include <QTcpServer>
#include <QTcpSocket>
#include <QThread>
#include <QMutex>
#include <QByteArray>
#include <QWaitCondition>

class RemoteClient : public QThread
{
	Q_OBJECT
public:
	typedef RemoteClient* Pointer;	//DO NOT USE Smart pointer because the parent will takes its ownership....

public:
	static Pointer GetInstance(int nPort=0, QObject* parent = nullptr);
	~RemoteClient(void);

	bool isReady(void) const;
	unsigned short getPort(void);

	bool isConnected(void) const;

	bool sendResponse(const QString& strResponse);

protected:
	RemoteClient(QObject* parent = nullptr);
	RemoteClient(int nPort, QObject* parent = nullptr);
	void openServer(void);
	bool isCompletedPacket(QByteArray& line);

	void run(void) override;
	void parse(QByteArray& line);

protected slots:
	void reqMakeConnection(void);
	void reqDisconnected(void);
	void reqReadData(void);

signals:
	void sigReportStatus();
	void sigStopProcess();
	void sigProcess(const QString& strOption);
	void sigConnected(bool bConnected);
	void sigClientStatus(bool busy);
	void sigUpdateList();
	void sigShow();

private:
	QTcpServer* m_pServer;
	int m_nPort;

	bool m_bRunning;
	QMutex m_mutex;
	QWaitCondition m_wait;

	QTcpSocket* m_pSocket;
	QByteArray m_baNewData;
	bool m_bNewDataReady;
};