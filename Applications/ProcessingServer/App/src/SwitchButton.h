#pragma once

#include <QPushButton>
#include <QStringList>
#include <QList>

#include "PixmapButton.h"

class SwitchButton : public QWidget {
    Q_OBJECT
public:
    SwitchButton(QWidget* parent = 0, Qt::WindowFlags f = 0);
    ~SwitchButton();

    void setState(int index);
    void setupState(int index, QPixmap& stateIcon, QPixmap& actionIcon);

protected:
    void updateButtons(void);

protected slots:
    void reqAction1();
    void reqAction2();

signals:
    void sigStateChanged(int state);

private:
    QList<QPixmap> m_iconStates;
    QList<QPixmap> m_iconActions;

    QList<PixmapButton*> m_buttons;
    int m_nState;
};