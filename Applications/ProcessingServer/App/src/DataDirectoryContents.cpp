#include "DataDirectoryContents.h"

namespace processing_server {
    struct DataDirectoryContents::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        Entity::DataType dataType{ Entity::DataType::HT };
        int32_t timeFrameIndex{ -1 };
    };

    DataDirectoryContents::DataDirectoryContents(const DataDirectoryContents& other)
        : d(new Impl(*other.d)) {
    }

    DataDirectoryContents::DataDirectoryContents(const Entity::DataType& dataType, const int32_t& timeFrameIndex)
        : d(new Impl()) {
        d->dataType = dataType;
        d->timeFrameIndex = timeFrameIndex;
    }

    DataDirectoryContents::~DataDirectoryContents() = default;

    auto DataDirectoryContents::GetDataType() const -> Entity::DataType {
        return d->dataType;
    }

    auto DataDirectoryContents::GetTimeFrameIndex() const -> int32_t {
        return d->timeFrameIndex;
    }
}
