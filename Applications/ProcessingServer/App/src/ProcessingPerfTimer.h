#pragma once

#include <memory>

#include <QMap>
#include <QString>
#include <QElapsedTimer>

class ProcessingPerfTimer {
public:
    typedef ProcessingPerfTimer Self;
    typedef std::shared_ptr<Self> Pointer;

public:
    ~ProcessingPerfTimer(void);

    static Pointer GetInstance(void);

    void AddTask(const QString& strPath);
    void AddTask(const QStringList& strPathes);
    void Update(const QString& strPath);
    void Complete(const QString& strPath, bool bSuccess);

    int GetTimeElapsed(void);
    int GetTimeRemaining(void);

protected:
    ProcessingPerfTimer(void);
    void Start(void);
    void CalcAverageTime(void);

private:
    QMap<QString, int> m_remain;
    QMap<QString, float> m_processing;

    int m_nAverageTime;
    int m_nRemainTotal;
    int m_nCompletedTotal;
    QElapsedTimer m_timer;
};