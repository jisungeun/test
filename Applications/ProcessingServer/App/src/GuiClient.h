#pragma once
#include "IProcessingServerClient.h"

namespace processing_server {
    class GuiClient final : public IProcessingServerClient {
        Q_OBJECT
    public:
        GuiClient();
        ~GuiClient();

        auto AddOneTcf(const QString& dataPath, const bool& deconvolutionFlag) const ->void;
        auto AddOneTcfReprocessing(const QString& dataPath, const bool& deconvolutionFlag) const ->void;
        auto AddOnlineProcessingPath(const QString& path) const ->void;
    public slots:
        Interactor::ProgressList UpdateProgress();
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
