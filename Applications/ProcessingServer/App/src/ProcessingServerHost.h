#pragma once
#include <memory>

#include <QObject>

#include "IProcessingServerClient.h"

namespace processing_server {
    class IProcessingServerClient;
    class ProcessingServerHost final : public QObject {
        Q_OBJECT
    public:
        ProcessingServerHost();
        ProcessingServerHost(const ProcessingServerHost& other);
        ~ProcessingServerHost();

        auto RegisterClient(IProcessingServerClient* client)->void;
        auto DeregisterClient(IProcessingServerClient* client)->void;
        static auto AddOneTcf(const QString& rootPath, const bool& deconvolutionFlag)->void;
        static auto AddOneTcfReprocessing(const QString& rootPath, const bool& deconvolutionFlag)->void;
        static auto AddDirectory(const QString& directoryPath, const bool& deconvolutionFlag)->void;
        static auto AddDirectoryReprocessing(const QString& directoryPath, const bool& deconvolutionFlag)->void;
        auto AddOnlineProcessingPath(const QString& path)->void;
        auto UpdateProgressToClient(IProcessingServerClient* client)->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
        typedef bool CompleteProcessFlag;
    private slots:
        void UpdateMonitor();
        void UpdateProgress();
    private:
        static auto IsProcessingRequired(const QString& rootPath)->bool;
        static auto ProcessingResultExists(const QString& rootPath)->bool;
        static auto RemovePreprocessedData(const QString& rootPath)->void;
        static auto GetSubFolderList(const QString& topFolderPath)->QStringList;
        auto StartPeriodicMonitorTimer(const uint32_t& msec) -> void;
        auto StartPeriodicProgressTimer(const uint32_t& msec) -> void;
        auto RemoveUpdatedCompletedProgress()->void;
    };
}
