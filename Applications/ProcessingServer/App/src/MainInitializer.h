#pragma once
#include <QApplication>

namespace processing_server {
    class MainInitializer {
    public:
        static auto ApplyStyle(QApplication& qApplication)->void;
        static auto Initialize()->void;
    private:
        static auto SetupLogger()->void;
        static auto SetJobManager()->void;
        static auto SetProcessorRegistry()->void;
    };
}
