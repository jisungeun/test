#define LOGGER_TAG "ProcessingServerInteractor::TemporalWidgetLogic"

#include "TemporalWidgetLogic.h"

#include <TCLogger.h>

#include <iostream>
#include <QDir>
#include <QTimer>

#include "AddController.h"
#include "DataScanner.h"
#include "JobManagerCaller.h"
#include "Processor.h"
#include "ProcessorCreator.h"
#include "ProcessorRegistryCaller.h"
#include "TCFDataValidityChecker.h"
#include "TCFEntryWriter.h"
#include "TCFWriterPolicy.h"

#include "DirectoryMonitor.h"
#include "InputJobScanner.h"

namespace processing_server {
    struct TemporalWidgetLogic::Impl {
        Impl() = default;
        ~Impl() = default;

        DirectoryMonitor* monitor;
        QTimer* timer;
    };

    TemporalWidgetLogic::TemporalWidgetLogic() : d(new Impl()) {
        Initialization();
    }

    TemporalWidgetLogic::~TemporalWidgetLogic() = default;

    auto TemporalWidgetLogic::AddOneTcf(const QString& dataPath) -> void {
        if (dataPath.isEmpty()) return;

        const QStringList resultFilters{ "*.TCF" };
        const auto preProcessedList = QDir(dataPath).entryList(resultFilters, QDir::Files);

        if (preProcessedList.isEmpty()) {
            Interactor::AddController addController;
            const auto reprocessingFlag = true;
            const auto deconvolutionFlag = true;

            auto inputJobScanner = InputJobScanner(dataPath);
            auto inputJobContentsList = inputJobScanner.Scan();

            for (auto& i : inputJobContentsList) {
                auto& inputJobContents = i;
                inputJobContents.SetProcessingFlags(reprocessingFlag, deconvolutionFlag);
                addController.AddOneJob(inputJobContents);
            }
        }
    }

    auto TemporalWidgetLogic::AddDirectory(const QString& directoryPath) -> void {
        if (directoryPath.isEmpty()) return;

        QDir dir(directoryPath);
        QStringList dirList = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

        for (auto& dirIteration : dirList) {
            AddOneTcf(QString("%1/%2").arg(directoryPath).arg(dirIteration));
        }
    }

    auto TemporalWidgetLogic::AddOneTcfReprocess(const QString& dataPath) -> void {
        if (dataPath.isEmpty()) return;

        const QStringList resultFilters{ "*.tctmp", "*.TCF", "*-BF.TIFF", "*-MIP.TIFF" };
        const auto preProcessedList = QDir(dataPath).entryList(resultFilters, QDir::Files);
        for (const auto& preProcessed : preProcessedList) {
            QFile::remove(QString("%1/%2").arg(dataPath).arg(preProcessed));
        }

        Interactor::AddController addController;
        const auto reprocessingFlag = true;
        const auto deconvolutionFlag = true;

        auto inputJobScanner = InputJobScanner(dataPath);
        auto inputJobContentsList = inputJobScanner.Scan();

        for (auto& i : inputJobContentsList) {
            auto& inputJobContents = i;
            inputJobContents.SetProcessingFlags(reprocessingFlag, deconvolutionFlag);
            addController.AddOneJob(inputJobContents);
        }
    }

    auto TemporalWidgetLogic::AddDirectoryReprocess(const QString& directoryPath) -> void {
        if (directoryPath.isEmpty()) return;

        QDir dir(directoryPath);
        QStringList dirList = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

        for (auto& dirIteration : dirList) {
            AddOneTcfReprocess(QString("%1/%2").arg(directoryPath).arg(dirIteration));
        }
    }

    auto TemporalWidgetLogic::AddMonitor(const QString& directoryPath) -> void {
        if (directoryPath.isEmpty()) return;

        if (d->monitor == nullptr) {
            d->monitor = new DirectoryMonitor(directoryPath);
            StartPeriodicTimer(1000);
        }
    }

    auto TemporalWidgetLogic::Initialization() const -> void {
        JobManagerSetting();
        ProcessorRegistrySetting();
    }

    auto TemporalWidgetLogic::JobManagerSetting() const -> void {
        auto jobManager = Entity::JobManagerCaller::GetJobManager();

        auto tcfWriter = std::make_shared<Plugins::TCFEntryWriter>();
        jobManager->SetFileWriter(tcfWriter);

        auto tcfWriterPolicy = std::make_shared<Plugins::TCFWriterPolicy>();
        const size_t GB = 1024 * 1024 * 1024;
        const auto limitMemorySize = 20 * GB;
        tcfWriterPolicy->SetLimitMemorySizeInBytes(limitMemorySize);
        jobManager->SetWriterPolicy(tcfWriterPolicy);


        auto tcfValidityChecker = std::make_shared<Plugins::TCFDataValidityChecker>();
        jobManager->SetValidityChecker(tcfValidityChecker);
    }

    auto TemporalWidgetLogic::ProcessorRegistrySetting() const -> void {
        auto processorRegistry = Entity::ProcessorRegistryCaller::GetProcessorRegistry();

        Plugins::ProcessorCreator processorCreator;
        const auto systemProcessorsList = processorCreator.Create();

        for (const auto& processor : systemProcessorsList) {
            processorRegistry->Register(processor);
        }
    }

    auto TemporalWidgetLogic::StartPeriodicTimer(const uint32_t& msec) -> void {
        d->timer = new QTimer(this);
        connect(d->timer, SIGNAL(timeout()), this, SLOT(Update()));
        d->timer->start(msec);
    }

    void TemporalWidgetLogic::Update() {
        const auto newInputJobContentsList = d->monitor->GetNewInputJobContents();
        for (const auto& inputJobContents : newInputJobContentsList) {
            std::cout << inputJobContents.GetDataPath().toLocal8Bit().constData() << "  ";
            std::cout << inputJobContents.GetDataType()._to_string() << "  ";
            std::cout << inputJobContents.GetTimeFrameIndex() << "  ";
            std::cout << std::endl << "======================================" << std::endl;

            const auto dataPath = inputJobContents.GetDataPath();
            const auto dataType = QString(inputJobContents.GetDataType()._to_string());
            const auto timeFrameIndex = QString("%1").arg(inputJobContents.GetTimeFrameIndex(), 6, 10, QChar('0'));

            QLOG_INFO() << QString("Job Added : %1 %2 %3").arg(dataPath).arg(dataType).arg(timeFrameIndex);

            Interactor::AddController::AddOneJob(inputJobContents);
        }
    }
}
