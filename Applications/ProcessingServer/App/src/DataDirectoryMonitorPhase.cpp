#include "DataDirectoryMonitorPhase.h"

#include <QFileSystemWatcher>
#include <QMap>
#include <QDir>

#include "AcquisitionCompleteChecker.h"

namespace processing_server {
    const bool added = true;
    const bool notAdded = !added;

    struct DataDirectoryMonitorPhase::Impl {
        Impl() = default;
        ~Impl() = default;
        QString dataDirectoryPath{};

        int32_t expectedNumberOfData{ 0 };
        int32_t completedDataNumber{ 0 };

        QStringList changedTimeFramePathList;
    };

    DataDirectoryMonitorPhase::DataDirectoryMonitorPhase(const QString& directoryPath,
        const int32_t& expectedNumberOfData)
        : d(new Impl()) {
        d->dataDirectoryPath = directoryPath;
        d->expectedNumberOfData = expectedNumberOfData;
    }

    DataDirectoryMonitorPhase::~DataDirectoryMonitorPhase() = default;

    auto DataDirectoryMonitorPhase::ScanChanged() const -> bool {
        const auto timeFrameEntry =
            QDir(d->dataDirectoryPath).entryList(QDir::NoDotAndDotDot | QDir::Dirs, QDir::Name);
        const auto numberOfAcquisitionData = static_cast<int32_t>(timeFrameEntry.size());

        for (auto timeFrameIndex = d->completedDataNumber;
            timeFrameIndex < numberOfAcquisitionData;
            ++timeFrameIndex) {

            const auto timeFrameString = QString("%1").arg(timeFrameIndex, 6, 10, QChar('0'));
            const auto timeFramePath = QString("%1/%2").arg(d->dataDirectoryPath).arg(timeFrameString);

            if (AcquisitionCompleteChecker::Check(timeFramePath)) {
                d->changedTimeFramePathList.push_back(timeFramePath);
            }
        }

        const auto dataIsChanged = numberOfAcquisitionData != d->completedDataNumber;
        return dataIsChanged;
    }

    auto DataDirectoryMonitorPhase::GetNewDataDirectoryContents() -> QList<DataDirectoryContents> {
        QList<DataDirectoryContents> dataDirectoryContentsList;
        for (auto& timeFramePath : d->changedTimeFramePathList) {
            const auto dataDirectoryContents = GenerateDataDirectoryContents(timeFramePath);
            dataDirectoryContentsList.push_back(dataDirectoryContents);
            ++d->completedDataNumber;
        }

        d->changedTimeFramePathList.clear();

        return dataDirectoryContentsList;
    }

    auto DataDirectoryMonitorPhase::IsCompleted() const -> bool {
        return d->expectedNumberOfData == d->completedDataNumber;
    }

    auto DataDirectoryMonitorPhase::GenerateDataDirectoryContents(const QString& timeFramePath) -> DataDirectoryContents {
        const QDir timeFrameDir(timeFramePath);
        const auto timeFrameIndex = timeFrameDir.dirName().toInt();

        DataDirectoryContents dataDirectoryContents{ Entity::DataType::PHASE, timeFrameIndex };
        return dataDirectoryContents;
    }
}
