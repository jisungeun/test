#include <catch2/catch.hpp>

#include <ProgressEntry.h>

using namespace processing_server;

namespace _TestProgressEntry {
    auto GetHtFlag() ->Entity::Job::DataTypeFlag {
        Entity::Job::DataTypeFlag htFlag;
        htFlag.existence[Entity::DataType::HT] = true;
        return htFlag;
    }

    auto MakeProgressUpdateParameter(const Entity::DataType& dataType, const uint32_t& timeFrameIndex,
        const QString dataPath, const float& completedProgressValue)->Entity::Progress::Parameter {

        Entity::Job::Info jobInfo;
        jobInfo.dataPath = dataPath;
        jobInfo.dataType = dataType;
        jobInfo.index = timeFrameIndex;

        Entity::Progress::Parameter parameter;
        parameter.job = std::make_shared<Entity::Job>(jobInfo);
        parameter.completeProgressValue = completedProgressValue;
        return parameter;
    }

    SCENARIO("Progress Exists") {
        GIVEN("Emtpy ProgressEntry instance") {
            Entity::ProgressEntry progressEntry;
            WHEN("Nothing happens") {
                const QString nonCreatedDataPath = "NonCreatedPath.path";
                THEN("Progress doesn't exist") {
                    const auto result = progressEntry.ProgressExists(nonCreatedDataPath);
                    CHECK(result == false);
                }
            }
        }
    }

    SCENARIO("Create progress") {
        GIVEN("Emtpy ProgressEntry instance") {
            Entity::ProgressEntry progressEntry;
            WHEN("Single Progress is created") {
                const QString createdDataPath = "DataPath/path.path";
                const uint32_t totalNumberOfTimeFrame = 20;
                progressEntry.CreateProgress(createdDataPath, GetHtFlag(), totalNumberOfTimeFrame);

                THEN("Progress exists") {
                    const auto result = progressEntry.ProgressExists(createdDataPath);
                    CHECK(result == true);
                }

                AND_WHEN("More Progresses are created") {
                    const QString createdDataPath2 = "DataPath/path.path2";
                    const QString createdDataPath3 = "DataPath/path.path3";

                    progressEntry.CreateProgress(createdDataPath2, GetHtFlag(), totalNumberOfTimeFrame);
                    progressEntry.CreateProgress(createdDataPath3, GetHtFlag(), totalNumberOfTimeFrame);

                    THEN("First created progress exists") {
                        const auto result = progressEntry.ProgressExists(createdDataPath);
                        CHECK(result == true);
                    }
                }
            }
        }
    }

    SCENARIO("Remove progress") {
        GIVEN("ProgressEntry instance") {
            Entity::ProgressEntry progressEntry;
            AND_GIVEN("Progress is created") {
                const QString createdDataPath = "DataPath/path.path";
                const uint32_t totalNumberOfTimeFrame = 20;
                progressEntry.CreateProgress(createdDataPath, GetHtFlag(), totalNumberOfTimeFrame);

                WHEN("Progress is removed") {
                    progressEntry.RemoveProgress(createdDataPath);
                    THEN("Progress doesn't exist") {
                        const auto result = progressEntry.ProgressExists(createdDataPath);
                        CHECK(result == false);
                    }

                    AND_WHEN("Progress is created again") {
                        progressEntry.CreateProgress(createdDataPath, GetHtFlag(), totalNumberOfTimeFrame);
                        THEN("Progress exists") {
                            const auto result = progressEntry.ProgressExists(createdDataPath);
                            CHECK(result == true);
                        }
                    }
                }
            }
        }
    }

    SCENARIO("Update progresses") {
        GIVEN("ProgressEntry instance") {
            Entity::ProgressEntry progressEntry;
            AND_GIVEN("Two Progresses are created") {
                const uint32_t totalNumberOfTimeFrame = 20;
                const QString createdDataPath1 = "DataPath/path.path1";
                const QString createdDataPath2 = "DataPath/path.path2";
                progressEntry.CreateProgress(createdDataPath1, GetHtFlag(), totalNumberOfTimeFrame);
                progressEntry.CreateProgress(createdDataPath2, GetHtFlag(), totalNumberOfTimeFrame);
                WHEN("Nothing happens") {
                    THEN("Progress is zero") {
                        const auto resultProgress = progressEntry.GetProgressInDecimal(createdDataPath1);
                        CHECK(resultProgress == 0.f);
                    }
                }

                WHEN("ProcessProgress is updated") {
                    const auto processProgressValue = 0.2f;
                    const auto processProgressParameter = 
                        MakeProgressUpdateParameter(Entity::DataType::HT, 0, createdDataPath1,
                            processProgressValue);
                    progressEntry.UpdateProcessProgress(processProgressParameter);
                    THEN("Progress is correct value") {
                        const auto resultProgress = progressEntry.GetProgressInDecimal(createdDataPath1);
                        const auto answerProgress = 
                            processProgressValue / static_cast<float>(2 * totalNumberOfTimeFrame);
                        CHECK(resultProgress == answerProgress);
                    }

                    THEN("The other Progress is zero") {
                        const auto resultProgress = progressEntry.GetProgressInDecimal(createdDataPath2);
                        CHECK(resultProgress == 0.f);
                    }

                    AND_WHEN("WritingProgress is updated") {
                        const auto writingProgressValue = 0.5f;
                        const auto writingProgressParameter =
                            MakeProgressUpdateParameter(Entity::DataType::HT, 0, createdDataPath1,
                                writingProgressValue);
                        progressEntry.UpdateWritingProgress(writingProgressParameter);
                        THEN("Progress is correct value") {
                            const auto resultProgress = progressEntry.GetProgressInDecimal(createdDataPath1);
                            const auto answerProgress = (processProgressValue + writingProgressValue)
                                / static_cast<float>(2 * totalNumberOfTimeFrame);
                            CHECK(resultProgress == answerProgress);
                        }

                        THEN("The other Progress is zero") {
                            const auto resultProgress = progressEntry.GetProgressInDecimal(createdDataPath2);
                            CHECK(resultProgress == 0.f);
                        }
                    }
                }
            }

        }
    }
}