#include <catch2/catch.hpp>

#include "JobManagerCaller.h"

using namespace processing_server;

namespace _TestJobManagerCaller {
    SCENARIO("JobmanagerCaller acts like singleton") {
        GIVEN("JobManager instance from JobManagerCaller") {
            const auto jobManager1 = Entity::JobManagerCaller::GetJobManager();
            WHEN("another jobManager is called") {
                const auto jobManager2 = Entity::JobManagerCaller::GetJobManager();
                THEN("memory addresses of two jobManagers are same") {
                    CHECK(jobManager1.get() == jobManager2.get());
                }
            }
        }
    }
}
