#include <catch2/catch.hpp>

#include <IJobValidityChecker.h>

using namespace processing_server;

namespace _TestIJobValidityChecker {
    namespace _Test {
        class DummyChecker : public Entity::IJobValidityChecker {
        public:
            DummyChecker() = default;
            ~DummyChecker() = default;
            auto Check(const Entity::Job::Pointer& job) -> bool override {
                return true;
            }
        };
    }

    TEST_CASE("IJobValidityChecker") {
        _Test::DummyChecker checker;
        SECTION("Check()") {
            Entity::Job::Pointer job = std::make_shared<Entity::Job>();
            CHECK(checker.Check(job) == true);
        }
    }
}