#include <catch2/catch.hpp>
#include <deque>

#include "JobManager.h"
#include "ProcessorRegistryCaller.h"

using namespace processing_server;

namespace _TestJobManager {
    namespace _Test {
        class DummyWriterPolicy : public Entity::IWriterPolicy {
        public:
            ~DummyWriterPolicy() = default;
            DummyWriterPolicy() = default;

            auto SetLimitMemorySizeInBytes(const size_t& limitMemorySizeInBytes) -> void override {
                this->limitMemorySizeInBytes = limitMemorySizeInBytes;
            }

            auto Accept(const size_t& requiredMemorySizeInBytes, const size_t& usedMemorySizeInBytes)
                ->bool override {
                return (requiredMemorySizeInBytes + usedMemorySizeInBytes) < limitMemorySizeInBytes;
            }
        private:
            size_t limitMemorySizeInBytes{0};
        };

        class DummyFileWaitingWriter : public Entity::IFileWriter {
        public:
            DummyFileWaitingWriter() = default;
            ~DummyFileWaitingWriter() = default;
            auto GetUsedMemoryInBytes() -> size_t override {
                const size_t oneGigaBytes = 1 * 1024 * 1024 * 1024;
                return oneGigaBytes;
            }
            auto SetUpdater(const Entity::IProgressUpdater::Pointer& progressUpdater) -> void override {
                updater = progressUpdater;
            }
            auto RequestToWrite(const Entity::WritingData::Pointer& writingData) -> void override {
                requestedWritingData.push_back(writingData);
                ++requestedNumber;
            }
            auto Clear() -> void override {
                requestedWritingData.clear();
            }
            auto GetRequestedNumber() const -> uint32_t {
                return requestedNumber;
            }

            auto SetSoftwareVersion(const QString& softwareVersion) -> void override {
            }
        private:
            Entity::IProgressUpdater::Pointer updater{};
            std::deque<Entity::WritingData::Pointer> requestedWritingData;
            uint32_t requestedNumber{ 0 };
        };

        class DummyFileWritingWriter : public Entity::IFileWriter {
        public:
            DummyFileWritingWriter() = default;
            ~DummyFileWritingWriter() = default;
            auto GetUsedMemoryInBytes() -> size_t override {
                const size_t oneGigaBytes = 1 * 1024 * 1024 * 1024;
                return oneGigaBytes;
            }
            auto SetUpdater(const Entity::IProgressUpdater::Pointer& progressUpdater) -> void override {
                updater = progressUpdater;
            }
            auto RequestToWrite(const Entity::WritingData::Pointer& writingData) -> void override {
                requestedWritingData.push_back(writingData);
                ++requestedNumber;

                Entity::Progress::Parameter parameter;
                parameter.job = writingData->GetJobPointer();
                parameter.completeProgressValue = 1.f;

                ++writtenNumber;
                updater->Update(parameter);

            }
            auto Clear() -> void override {
                requestedWritingData.clear();
            }
            auto GetRequestedNumber() const -> uint32_t {
                return requestedNumber;
            }

            auto GetWrittenNumber()const->uint32_t {
                return writtenNumber;
            }

            auto SetSoftwareVersion(const QString& softwareVersion) -> void override {
            }
        private:
            Entity::IProgressUpdater::Pointer updater{};
            std::deque<Entity::WritingData::Pointer> requestedWritingData;
            uint32_t requestedNumber{ 0 };
            uint32_t writtenNumber{ 0 };
        };

        class DummyValidReturnChecker : public Entity::IJobValidityChecker {
        public:
            DummyValidReturnChecker() = default;
            ~DummyValidReturnChecker() = default;
            auto Check(const Entity::Job::Pointer& job) -> bool override {
                return true;
            }
        };

        class DummyInValidReturnChecker : public Entity::IJobValidityChecker {
        public:
            DummyInValidReturnChecker() = default;
            ~DummyInValidReturnChecker() = default;
            auto Check(const Entity::Job::Pointer& job) -> bool override {
                ++checkedNumber;
                return false;
            }

            auto GetCheckedNumber()->uint32_t {
                return checkedNumber;
            }
        private:
            uint32_t checkedNumber{ 0 };
        };

        class DummyProcessor : public Entity::IProcessor {
        public:
            DummyProcessor(const Entity::ProcessorType& type, const Entity::ProcessorPriority& priority)
                : processorType(type), priority(priority) {
            }
            ~DummyProcessor() = default;

            auto IsWorking() const -> bool override {
                const auto statusIsIdle = (GetStatus()._value == Entity::ProcessorStatus::IDLE);
                return !statusIsIdle;
            }
            auto SetJobPointer(const Entity::Job::Pointer job) -> void override {
                this->job = job;
            }
            auto SetUpdater(const Entity::IProgressUpdater::Pointer updater) -> void override {
                this->updater = updater;
            }
            auto StartProcess() -> void override {
                status = Entity::ProcessorStatus::PROCESSING;
                Entity::Progress::Parameter parameter;
                parameter.job = job;
                parameter.completeProgressValue = 0.1f;
                updater->Update(parameter);

                parameter.completeProgressValue = 0.3f;
                updater->Update(parameter);

                parameter.completeProgressValue = 0.7f;
                updater->Update(parameter);

                processedData = std::make_shared<Entity::ProcessedData>(job->GetDataType());
                Entity::ProcessedData::Dimension dimension(800,800,200);
                Entity::ProcessedData::Resolution resolution(0.1f, 0.1f, 0.2f);
                processedData->SetDimension(dimension);
                processedData->SetResolution(resolution);

                parameter.completeProgressValue = 1.f;
                ++processedDataNumber;

                status = Entity::ProcessorStatus::COMPLETE;
                updater->Update(parameter);
            }
            auto GetProcessedData() -> Entity::ProcessedData::Pointer override {
                status = Entity::ProcessorStatus::IDLE;
                return processedData;
            }
            auto GetProcessedDataNumber() const ->uint32_t {
                return processedDataNumber;
            }

            auto SetType(const Entity::ProcessorType& type) -> void override {
                this->processorType = type;
            }
            auto GetType() const -> Entity::ProcessorType override {
                return processorType;
            }
            auto SetPriority(const Entity::ProcessorPriority& priority) -> void override {
                this->priority = priority;
            }
            auto GetPriority() const -> Entity::ProcessorPriority override {
                return priority;
            }
            auto GetStatus() const -> Entity::ProcessorStatus override {
                return status;
            }

            auto GetProcessedDataMemoryInBytes() -> size_t override {
                return 10;
            }
        private:
            Entity::ProcessorStatus status{ Entity::ProcessorStatus::IDLE };
            Entity::Job::Pointer job{};
            Entity::IProgressUpdater::Pointer updater{};
            Entity::ProcessedData::Pointer processedData{};
            uint32_t processedDataNumber{ 0 };
            Entity::ProcessorType processorType{ Entity::ProcessorType::LOCAL_GPU };
            Entity::ProcessorPriority priority{ Entity::ProcessorPriority::MAIN };
        };

        class ConcreteJobManager : public Entity::JobManager {
        public:
            auto WaitingQueue()->Entity::JobQueue& {
                return JobManager::WaitingQueue();
            }
            auto ProcessingQueue()->Entity::JobQueue& {
                return JobManager::ProcessingQueue();
            }
            auto WritingQueue()->Entity::JobQueue& {
                return JobManager::WritingQueue();
            }
        };
    }

    auto AllQueueAreEmpty(_Test::ConcreteJobManager& jobManager)->bool {
        const auto waitingQueueIsEmpty = jobManager.WaitingQueue().Empty();
        const auto processingQueueIsEmpty = jobManager.ProcessingQueue().Empty();
        const auto writingQueueIsEmpty = jobManager.WritingQueue().Empty();
        return waitingQueueIsEmpty && processingQueueIsEmpty && writingQueueIsEmpty;
    }

    auto WaitingProcessingQueueAreEmpty(_Test::ConcreteJobManager& jobManager)->bool {
        const auto waitingQueueIsEmpty = jobManager.WaitingQueue().Empty();
        const auto processingQueueIsEmpty = jobManager.ProcessingQueue().Empty();
        return waitingQueueIsEmpty && processingQueueIsEmpty;

    }

    auto MakeAnyProcessorRegistry()->_Test::DummyProcessor* {
        const _Test::DummyProcessor::Pointer processor = std::make_shared<_Test::DummyProcessor>(
            Entity::ProcessorType::ANY, Entity::ProcessorPriority::ANY);

        auto registry = Entity::ProcessorRegistryCaller::GetProcessorRegistry();
        registry->Register(processor);
        return dynamic_cast<_Test::DummyProcessor*>(processor.get());
    }

    auto ClearAllProcessorInRegistry() {
        auto processorRegistry = Entity::ProcessorRegistryCaller::GetProcessorRegistry();
        const auto anyType = std::tuple<Entity::ProcessorType, Entity::ProcessorPriority>(
            Entity::ProcessorType::ANY, Entity::ProcessorPriority::ANY);
        while (processorRegistry->CheckIfMatchedProcessorExists(anyType)) {
            processorRegistry->Unregister(processorRegistry->RequestProcessor(anyType));
        }
    }

    auto MakeMainGpuProcessorRegistry()->_Test::DummyProcessor* {
        const _Test::DummyProcessor::Pointer processor = std::make_shared<_Test::DummyProcessor>(
            Entity::ProcessorType::LOCAL_GPU, Entity::ProcessorPriority::MAIN);

        auto registry = Entity::ProcessorRegistryCaller::GetProcessorRegistry();
        registry->Register(processor);
        return dynamic_cast<_Test::DummyProcessor*>(processor.get());
    }

    auto MakeSubGpuProcessorRegistry()->_Test::DummyProcessor* {
        const _Test::DummyProcessor::Pointer processor = std::make_shared<_Test::DummyProcessor>(
            Entity::ProcessorType::LOCAL_GPU, Entity::ProcessorPriority::SUB);

        auto registry = Entity::ProcessorRegistryCaller::GetProcessorRegistry();
        registry->Register(processor);
        return dynamic_cast<_Test::DummyProcessor*>(processor.get());
    }

    auto AddManyHtJobWhichIsInSameDataSet(_Test::ConcreteJobManager& jobManager,
        const uint32_t totalNumberOfTimeFrame) {
        for (uint32_t i = 0; i < totalNumberOfTimeFrame; ++i) {
            Entity::Job::Info info;
            info.dataType = Entity::DataType::HT;
            info.dataPath = "DataPath/Data.Path";
            info.outputPath = "OutputPath/Output.Path";
            info.index = i;
            info.dataTypeFlag.existence[Entity::DataType::HT] = true;
            info.reprocessingFlag = false;
            info.totalFrameNumber = totalNumberOfTimeFrame;
            info.uniqueID = i + 1;
            Entity::Job job(info);
            
            jobManager.AddJob(std::make_shared<Entity::Job>(job));
        }
    }

    auto AddManyFlJobWhichIsInSameDataSet(_Test::ConcreteJobManager& jobManager,
        const uint32_t& totalNumberOfTimeFrame)->void {
        for (uint32_t i = 0; i < totalNumberOfTimeFrame; ++i) {
            Entity::Job::Info info;
            info.dataType = Entity::DataType::FLGREEN;
            info.dataPath = "DataPath/Data.Path";
            info.outputPath = "OutputPath/Output.Path";
            info.index = i;
            info.dataTypeFlag.existence[Entity::DataType::FLGREEN] = true;
            info.reprocessingFlag = false;
            info.totalFrameNumber = totalNumberOfTimeFrame;
            info.uniqueID = i + 1;
            Entity::Job job(info);

            jobManager.AddJob(std::make_shared<Entity::Job>(job));
        }
    }

    auto AddHtFlJobsInMixedOrder(_Test::ConcreteJobManager& jobManager, const uint32_t& numberOfJobs)->uint32_t {
        uint32_t numberOfHtJobs{0};
        for (uint32_t i = 0; i < numberOfJobs; ++i) {
            const auto randNumber = std::rand();

            if(randNumber%2 == 1) {
                Entity::Job::Info info;
                info.dataType = Entity::DataType::FLGREEN;
                info.dataPath = "DataPath/Data.Path";
                info.outputPath = "OutputPath/Output.Path";
                info.index = i;
                info.dataTypeFlag.existence[Entity::DataType::HT] = true;
                info.dataTypeFlag.existence[Entity::DataType::FLGREEN] = true;
                info.reprocessingFlag = false;
                info.totalFrameNumber = numberOfJobs;
                info.uniqueID = i + 1;
                Entity::Job job(info);

                jobManager.AddJob(std::make_shared<Entity::Job>(job));
            } else {
                Entity::Job::Info info;
                info.dataType = Entity::DataType::HT;
                info.dataPath = "DataPath/Data.Path";
                info.outputPath = "OutputPath/Output.Path";
                info.index = i;
                info.dataTypeFlag.existence[Entity::DataType::HT] = true;
                info.dataTypeFlag.existence[Entity::DataType::FLGREEN] = true;
                info.reprocessingFlag = false;
                info.totalFrameNumber = numberOfJobs;
                info.uniqueID = i + 1;
                Entity::Job job(info);

                jobManager.AddJob(std::make_shared<Entity::Job>(job));
                ++numberOfHtJobs;
            }
        }

        return numberOfHtJobs;
    }

    SCENARIO("Add job") {
        GIVEN("JobManager Instance") {
            _Test::ConcreteJobManager jobManager;
            const auto limitMemorySizeInBytes = 32ULL * 1024ULL * 1024ULL * 1024ULL;

            const auto fileWriter = std::make_shared<_Test::DummyFileWaitingWriter>();
            const auto writerPolicy =
                std::make_shared<_Test::DummyWriterPolicy>();
            writerPolicy->SetLimitMemorySizeInBytes(limitMemorySizeInBytes);
            const auto validityChecker = std::make_shared<_Test::DummyValidReturnChecker>();

            jobManager.SetFileWriter(fileWriter);
            jobManager.SetWriterPolicy(writerPolicy);
            jobManager.SetValidityChecker(validityChecker);
            ClearAllProcessorInRegistry();
            const auto processor = MakeAnyProcessorRegistry();
            
            WHEN("A job is added") {
                const uint32_t numberOfJob = 1;
                AddManyHtJobWhichIsInSameDataSet(jobManager, numberOfJob);

                THEN("Processed Data Number of processor is 1") {
                    CHECK(processor->GetProcessedDataNumber() == numberOfJob);
                }

                THEN("Requested Data Number of writer is 1") {
                    CHECK(fileWriter->GetRequestedNumber() == numberOfJob);
                }

                THEN("Waiting and Processing Queue are empty") {
                    CHECK(WaitingProcessingQueueAreEmpty(jobManager) == true);
                }

                THEN("Size of Writing Queue is numberOfJob") {
                    CHECK(jobManager.WritingQueue().Size() == numberOfJob);
                }
            }

            WHEN("Many jobs in same data set are added") {
                const uint32_t numberOfJob = 10;
                AddManyHtJobWhichIsInSameDataSet(jobManager, numberOfJob);

                THEN("Processed Data Number of processor is the number of Job") {
                    CHECK(processor->GetProcessedDataNumber() == numberOfJob);
                }

                THEN("Requested Data Number of writer is the number of Job") {
                    CHECK(fileWriter->GetRequestedNumber() == numberOfJob);
                }

                THEN("Waiting and Processing Queue are empty") {
                    CHECK(WaitingProcessingQueueAreEmpty(jobManager) == true);
                }

                THEN("Size of Writing Queue is numberOfJob") {
                    CHECK(jobManager.WritingQueue().Size() == numberOfJob);
                }
            }
        }
    }

    SCENARIO("Add FL job, which means it need main GPU processor") {
        GIVEN("JobManager") {
            _Test::ConcreteJobManager jobManager;
            const auto limitMemorySizeInBytes = 32ULL * 1024ULL * 1024ULL * 1024ULL;

            const auto fileWriter = std::make_shared<_Test::DummyFileWaitingWriter>();
            const auto writerPolicy =
                std::make_shared<_Test::DummyWriterPolicy>();
            writerPolicy->SetLimitMemorySizeInBytes(limitMemorySizeInBytes);
            const auto validityChecker = std::make_shared<_Test::DummyValidReturnChecker>();

            jobManager.SetFileWriter(fileWriter);
            jobManager.SetWriterPolicy(writerPolicy);
            jobManager.SetValidityChecker(validityChecker);
            ClearAllProcessorInRegistry();

            AND_GIVEN("Main gpu processor") {
                const auto mainGpuProcessor = MakeMainGpuProcessorRegistry();
                
                WHEN("A Fl job is added") {
                    const uint32_t numberOfJob = 1;
                    AddManyFlJobWhichIsInSameDataSet(jobManager, numberOfJob);

                    THEN("Processed Data Number of processor is 1") {
                        CHECK(mainGpuProcessor->GetProcessedDataNumber() == numberOfJob);
                    }

                    THEN("Requested Data Number of writer is 1") {
                        CHECK(fileWriter->GetRequestedNumber() == numberOfJob);
                    }

                    THEN("Waiting and Processing Queue are empty") {
                        CHECK(WaitingProcessingQueueAreEmpty(jobManager) == true);
                    }

                    THEN("Size of Writing Queue is numberOfJob") {
                        CHECK(jobManager.WritingQueue().Size() == numberOfJob);
                    }
                }
            }

            AND_GIVEN("sub gpu processor") {
                const auto mainGpuProcessor = MakeSubGpuProcessorRegistry();
                
                WHEN("A Fl job is added") {
                    const uint32_t numberOfJob = 1;
                    AddManyFlJobWhichIsInSameDataSet(jobManager, numberOfJob);

                    THEN("Processed Data Number of processor is 0") {
                        CHECK(mainGpuProcessor->GetProcessedDataNumber() == 0);
                    }

                    THEN("Requested Data Number of writer is 0") {
                        CHECK(fileWriter->GetRequestedNumber() == 0);
                    }

                    THEN("Size of Waiting Queue is number of job") {
                        CHECK(jobManager.WaitingQueue().Size() == numberOfJob);
                    }

                    THEN("Processing Queue is empty") {
                        CHECK(jobManager.ProcessingQueue().Empty() == true);
                    }

                    THEN("Writing Queue is empty") {
                        CHECK(jobManager.WritingQueue().Empty() == true);
                    }
                }
            }
        }
    }

    SCENARIO("Ht and Fl jobs are added In mixed order") {
        GIVEN("JobManager") {
            _Test::ConcreteJobManager jobManager;
            const auto limitMemorySizeInBytes = 32ULL * 1024ULL * 1024ULL * 1024ULL;

            const auto fileWriter = std::make_shared<_Test::DummyFileWaitingWriter>();
            const auto writerPolicy =
                std::make_shared<_Test::DummyWriterPolicy>();
            writerPolicy->SetLimitMemorySizeInBytes(limitMemorySizeInBytes);
            const auto validityChecker = std::make_shared<_Test::DummyValidReturnChecker>();

            jobManager.SetFileWriter(fileWriter);
            jobManager.SetWriterPolicy(writerPolicy);
            jobManager.SetValidityChecker(validityChecker);
            ClearAllProcessorInRegistry();

            const auto subProcessor = MakeSubGpuProcessorRegistry();
            const auto mainProcessor = MakeMainGpuProcessorRegistry();

            WHEN("Ht and FL jobs are added") {
                const uint32_t numberOfJob = 30;
                const auto numberOfHtJobs = AddHtFlJobsInMixedOrder(jobManager, numberOfJob);
                THEN("processedNumber of mainProcessor is numberOfFljobs") {
                    CHECK(mainProcessor->GetProcessedDataNumber() == numberOfJob - numberOfHtJobs);
                }

                THEN("processedNumber of subProcessor is numberOfHtjobs") {
                    CHECK(subProcessor->GetProcessedDataNumber() == numberOfHtJobs);
                }

            }

        }
    }

    SCENARIO("When writer Memory is short, jobs remain in processing queue") {
        GIVEN("JobManager whose memory is full") {
            _Test::ConcreteJobManager jobManager;
            const auto limitMemorySizeInBytes = 1ULL * 1024ULL * 1024ULL * 1024ULL;

            const auto fileWriter = std::make_shared<_Test::DummyFileWaitingWriter>();
            const auto writerPolicy =
                std::make_shared<_Test::DummyWriterPolicy>();
            writerPolicy->SetLimitMemorySizeInBytes(limitMemorySizeInBytes);
            const auto validityChecker = std::make_shared<_Test::DummyValidReturnChecker>();

            jobManager.SetFileWriter(fileWriter);
            jobManager.SetWriterPolicy(writerPolicy);
            jobManager.SetValidityChecker(validityChecker);
            ClearAllProcessorInRegistry();

            CHECK(fileWriter->GetUsedMemoryInBytes() == limitMemorySizeInBytes);

            const auto numberOfProcessor = 1;
            const auto mainProcessor = MakeMainGpuProcessorRegistry();

            WHEN("Ht and Fl jobs are added") {
                const uint32_t numberOfJob = 30;
                AddHtFlJobsInMixedOrder(jobManager, numberOfJob);

                THEN("Processed Data Number of processor is numberOfJob") {
                    CHECK(mainProcessor->GetProcessedDataNumber() == 1);
                }

                THEN("Requested Data Number of writer is 0") {
                    CHECK(fileWriter->GetRequestedNumber() == 0);
                }

                THEN("size of WaitingQueue is numberOfJob - 1") {
                    CHECK(jobManager.WaitingQueue().Size() == numberOfJob - 1);
                }

                THEN("size Processing Queue is 1") {
                    CHECK(jobManager.ProcessingQueue().Size() == 1);
                }

                THEN("Writing Queue is empty") {
                    CHECK(jobManager.WritingQueue().Empty() == true);
                }

                THEN("Status of Processor is Completed") {
                    CHECK(mainProcessor->GetStatus()._value == Entity::ProcessorStatus::COMPLETE);
                }
            }
        }
    }

    SCENARIO("Data Writing will be finished, then all jobs are not in Queue") {
        GIVEN("JobManager") {
            _Test::ConcreteJobManager jobManager;
            const auto limitMemorySizeInBytes = 32ULL * 1024ULL * 1024ULL * 1024ULL;

            const auto fileWriter = std::make_shared<_Test::DummyFileWritingWriter>();
            const auto writerPolicy =
                std::make_shared<_Test::DummyWriterPolicy>();
            writerPolicy->SetLimitMemorySizeInBytes(limitMemorySizeInBytes);
            const auto validityChecker = std::make_shared<_Test::DummyValidReturnChecker>();

            jobManager.SetFileWriter(fileWriter);
            jobManager.SetWriterPolicy(writerPolicy);
            jobManager.SetValidityChecker(validityChecker);

            ClearAllProcessorInRegistry();
            const auto mainProcessor = MakeMainGpuProcessorRegistry();

            WHEN("Ht and FL jobs are added") {
                const uint32_t numberOfJob = 30;
                AddHtFlJobsInMixedOrder(jobManager, numberOfJob);

                THEN("processedNumber is numberOfJob") {
                    CHECK(mainProcessor->GetProcessedDataNumber() == numberOfJob);
                }

                THEN("All Queue are empty") {
                    CHECK(AllQueueAreEmpty(jobManager) == true);
                }

                THEN("RequestedNumber is numberOfJob") {
                    CHECK(fileWriter->GetRequestedNumber() == numberOfJob);
                }

                THEN("WrittenNumber is  numberOfJob") {
                    CHECK(fileWriter->GetWrittenNumber() == numberOfJob);
                }

            }

        }
    }

    SCENARIO("JobValidityChecker always returns false") {
        GIVEN("JobManager") {
            _Test::ConcreteJobManager jobManager;
            const auto limitMemorySizeInBytes = 32ULL * 1024ULL * 1024ULL * 1024ULL;

            const auto fileWriter = std::make_shared<_Test::DummyFileWritingWriter>();
            const auto writerPolicy =
                std::make_shared<_Test::DummyWriterPolicy>();
            writerPolicy->SetLimitMemorySizeInBytes(limitMemorySizeInBytes);
            const auto validityChecker = std::make_shared<_Test::DummyInValidReturnChecker>();

            jobManager.SetFileWriter(fileWriter);
            jobManager.SetWriterPolicy(writerPolicy);
            jobManager.SetValidityChecker(validityChecker);

            ClearAllProcessorInRegistry();
            const auto mainProcessor = MakeMainGpuProcessorRegistry();

            WHEN("Ht and FL jobs are added") {
                const uint32_t numberOfJob = 30;
                AddHtFlJobsInMixedOrder(jobManager, numberOfJob);

                THEN("processedNumber is 0") {
                    CHECK(mainProcessor->GetProcessedDataNumber() == 0);
                }

                THEN("All Queue are empty") {
                    CHECK(AllQueueAreEmpty(jobManager) == true);
                }

                THEN("RequestedNumber is 0") {
                    CHECK(fileWriter->GetRequestedNumber() == 0);
                }

                THEN("WrittenNumber is  0") {
                    CHECK(fileWriter->GetWrittenNumber() == 0);
                }
            }
        }
    }

}
