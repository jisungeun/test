#include <catch2/catch.hpp>

#include <Progress.h>

using namespace processing_server;

namespace _TestProgress {
    auto CheckProperProgressValue(const Entity::Progress& progress,const float& answerProgress)->bool {
        const auto currentProgressValue = progress.GetProgressInDecimal();
        const auto progressIsNotZero = (currentProgressValue != 0.f);
        const auto progressValueIsCorrect = (currentProgressValue == answerProgress);
        return progressIsNotZero && progressValueIsCorrect;
    }

    auto MakeProgressUpdateParameter(const Entity::DataType& dataType, const uint32_t& timeFrameIndex,
        const float& completedProgressValue)->Entity::Progress::Parameter {

        Entity::Job::Info jobInfo;
        jobInfo.dataType = dataType;
        jobInfo.index = timeFrameIndex;

        Entity::Progress::Parameter parameter;
        parameter.job = std::make_shared<Entity::Job>(jobInfo);
        parameter.completeProgressValue = completedProgressValue;
        return parameter;
    }

    SCENARIO("ProgressException is working well") {
        GIVEN("Exception contents") {
            const std::string exceptionContents = "testContents";
            WHEN("ProgressException is created with contents") {
                Entity::Progress::ProgressException progressException(exceptionContents);
                THEN("What() method returns exceptionContents") {
                    const auto resultContents = progressException.What();
                    CHECK(resultContents == exceptionContents);
                }
            }
        }
    }

    SCENARIO("Progress class is working properly") {
        GIVEN("Total Number To process") {
            const auto totalFrameNumber = 100;
            const Entity::DataType updatedDataType = Entity::DataType::HT;

            AND_GIVEN("Progress Instance with parameter") {
                Entity::Job::DataTypeFlag flag;
                flag.existence[updatedDataType] = true;
                Entity::Progress progress(flag, totalFrameNumber);

                WHEN("Nothing happens") {
                    THEN("Progress is zero") {
                        const auto resultProgress = progress.GetProgressInDecimal();
                        CHECK(resultProgress == 0.f);
                    }
                }
                WHEN("Process progress is updated(One process is completed)") {
                    const auto processingCompletedValue = 1.f;
                    auto parameter =
                        MakeProgressUpdateParameter(updatedDataType, 0, processingCompletedValue);

                    progress.ProcessUpdate(parameter);
                    const auto answerProgress =
                        processingCompletedValue / static_cast<float>(2 * totalFrameNumber);

                    THEN("Progress is not zero and proper value") {
                        CHECK(CheckProperProgressValue(progress,answerProgress) == true);
                    }
                }
                WHEN("Writing progress is updated(One writing is completed)") {
                    const auto writingCompletedValue = 1.f;
                    auto parameter =
                        MakeProgressUpdateParameter(updatedDataType, 0, writingCompletedValue);

                    progress.WritingUpdate(parameter);
                    const auto answerProgress =
                        writingCompletedValue / static_cast<float>(2 * totalFrameNumber);

                    THEN("Progress is not zero and proper value") {
                        CHECK(CheckProperProgressValue(progress, answerProgress) == true);
                    }
                }

                WHEN("Process progress is updated(just updated, not complete)") {
                    const float processingCompletedValue = 0.3f;
                    auto parameter =
                        MakeProgressUpdateParameter(updatedDataType, 0, processingCompletedValue);

                    progress.ProcessUpdate(parameter);
                    const auto answerProgress =
                        processingCompletedValue / static_cast<float>(2 * totalFrameNumber);

                    THEN("Progress is not zero and proper value") {
                        CHECK(CheckProperProgressValue(progress, answerProgress) == true);
                    }
                }

                WHEN("Writing progress is updated(just updated, not complete)") {
                    const auto writingCompletedValue = 0.3f;
                    auto parameter =
                        MakeProgressUpdateParameter(updatedDataType, 0, writingCompletedValue);

                    progress.WritingUpdate(parameter);
                    const auto answerProgress =
                        writingCompletedValue / static_cast<float>(2 * totalFrameNumber);

                    THEN("Progress is not zero and proper value") {
                        CHECK(CheckProperProgressValue(progress, answerProgress) == true);
                    }
                }

                WHEN("Out-bound progress in process updating is updated") {
                    const auto outBoundedCompletedValue = 1.1f;
                    auto parameter =
                        MakeProgressUpdateParameter(updatedDataType, 0, outBoundedCompletedValue);

                    auto exceptionThrown = false;
                    try {
                        progress.ProcessUpdate(parameter);
                    }
                    catch (Entity::Progress::ProgressException) {
                        exceptionThrown = true;
                    }

                    THEN("ProgressException is Thrown") {
                        CHECK(exceptionThrown == true);
                    }
                }

                WHEN("Out-bound progress in writing updating is updated") {
                    const auto outBoundedCompletedValue = 1.1f;
                    auto parameter =
                        MakeProgressUpdateParameter(updatedDataType, 0, outBoundedCompletedValue);

                    auto exceptionThrown = false;
                    try {
                        progress.WritingUpdate(parameter);
                    }
                    catch (Entity::Progress::ProgressException) {
                        exceptionThrown = true;
                    }

                    THEN("ProgressException is Thrown") {
                        CHECK(exceptionThrown == true);
                    }
                }

                WHEN("Multiple progress is updated") {
                    const auto updatedProcessNumber = 40;
                    const auto updatedWritingNumber = 33;
                    CHECK(updatedWritingNumber < totalFrameNumber);
                    CHECK(updatedWritingNumber < totalFrameNumber);

                    auto processProgressValueSum = 0.f;
                    auto writingProgressValueSum = 0.f;

                    for(auto i =0; i<updatedProcessNumber;++i) {
                        const auto updatedProcessValue = 
                            static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX);
                        const auto parameter =
                            MakeProgressUpdateParameter(updatedDataType, static_cast<uint32_t>(i),
                                updatedProcessValue);
                        progress.ProcessUpdate(parameter);
                        processProgressValueSum += updatedProcessValue;
                    }

                    for (auto i = 0; i < updatedWritingNumber; ++i) {
                        const auto updatedWritingValue =
                            static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX);
                        const auto parameter =
                            MakeProgressUpdateParameter(updatedDataType, static_cast<uint32_t>(i),
                                updatedWritingValue);
                        progress.WritingUpdate(parameter);
                        processProgressValueSum += updatedWritingValue;
                    }

                    const auto answerProgress = (processProgressValueSum + writingProgressValueSum)
                        / static_cast<float>(2 * totalFrameNumber);

                    THEN("Progress is not zero and proper value") {
                        CHECK(CheckProperProgressValue(progress, answerProgress) == true);
                    }
                }

                WHEN("Multiple progress is updated in same timeframe") {
                    const auto processingCompletedValue1 = 0.1f;
                    auto parameter1 =
                        MakeProgressUpdateParameter(updatedDataType, 0, processingCompletedValue1);
                    progress.ProcessUpdate(parameter1);

                    const auto processingCompletedValue2 = 0.2f;
                    auto parameter2 =
                        MakeProgressUpdateParameter(updatedDataType, 0, processingCompletedValue2);
                    progress.ProcessUpdate(parameter2);

                    const auto processingCompletedValue3 = 0.3f;
                    auto parameter3 =
                        MakeProgressUpdateParameter(updatedDataType, 0, processingCompletedValue3);
                    progress.ProcessUpdate(parameter3);

                    const auto answerProgress =
                        processingCompletedValue3 / static_cast<float>(2 * totalFrameNumber);

                    THEN("Progress is not zero and proper value") {
                        CHECK(CheckProperProgressValue(progress, answerProgress) == true);
                    }

                    AND_WHEN("Progress is going reversely") {
                        const auto processingCompletedValue = 0.1f;
                        auto parameter =
                            MakeProgressUpdateParameter(updatedDataType, 0, processingCompletedValue);
                        auto exceptionThrown = false;
                        try {
                            progress.ProcessUpdate(parameter);
                        }
                        catch (Entity::Progress::ProgressException) {
                            exceptionThrown = true;
                        }
                        THEN("ProgressException is Thrown") {
                            CHECK(exceptionThrown == true);
                        }
                    }
                }
            }
        }
    }
}
