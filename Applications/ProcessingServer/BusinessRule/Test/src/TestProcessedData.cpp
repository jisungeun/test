#include <catch2/catch.hpp>

#include "ProcessedData.h"

using namespace processing_server;

namespace _TestProcessedData {
    TEST_CASE("ProcessedData::Resolution") {
        SECTION("Resolution()") {
            Entity::ProcessedData::Resolution resolution;
            CHECK(resolution.GetResolutionX() == 0);
            CHECK(resolution.GetResolutionY() == 0);
            CHECK(resolution.GetResolutionZ() == 0);
        }

        SECTION("Resolution(x,y,z)") {
            Entity::ProcessedData::Resolution resolution(1, 2, 3);
            CHECK(resolution.GetResolutionX() == 1);
            CHECK(resolution.GetResolutionY() == 2);
            CHECK(resolution.GetResolutionZ() == 3);
        }

        SECTION("Resolution(Resolution)") {
            Entity::ProcessedData::Resolution srcResolution(1, 2, 3);
            auto resolution(srcResolution);
            CHECK(resolution.GetResolutionX() == 1);
            CHECK(resolution.GetResolutionY() == 2);
            CHECK(resolution.GetResolutionZ() == 3);
        }

        SECTION("operator =") {
            Entity::ProcessedData::Resolution srcResolution(1, 2, 3);
            auto resolution = srcResolution;
            CHECK(resolution.GetResolutionX() == 1);
            CHECK(resolution.GetResolutionY() == 2);
            CHECK(resolution.GetResolutionZ() == 3);
        }

        SECTION("operator ==") {
            Entity::ProcessedData::Resolution resolution1(1, 2, 3);
            Entity::ProcessedData::Resolution resolution2(1, 2, 3);
            CHECK(resolution1 == resolution2);
        }

        SECTION("operator !=") {
            Entity::ProcessedData::Resolution resolution1(1, 2, 3);
            Entity::ProcessedData::Resolution resolution2(3, 2, 1);
            CHECK(resolution1 != resolution2);
        }

        SECTION("SetResolution(), GetResolution()") {
            Entity::ProcessedData::Resolution resolution;
            resolution.SetResolution(1, 2, 3);
            CHECK(resolution.GetResolutionX() == 1);
            CHECK(resolution.GetResolutionY() == 2);
            CHECK(resolution.GetResolutionZ() == 3);
        }
    }

    TEST_CASE("ProcessedData::Dimension") {
        SECTION("Dimension()") {
            Entity::ProcessedData::Dimension dimension;
            CHECK(dimension.GetDimensionX() == 0);
            CHECK(dimension.GetDimensionY() == 0);
            CHECK(dimension.GetDimensionZ() == 0);
        }

        SECTION("Dimension(x,y,z)") {
            Entity::ProcessedData::Dimension dimension(1, 2, 3);
            CHECK(dimension.GetDimensionX() == 1);
            CHECK(dimension.GetDimensionY() == 2);
            CHECK(dimension.GetDimensionZ() == 3);
        }

        SECTION("Dimension(Dimension)") {
            Entity::ProcessedData::Dimension srcDimension(1, 2, 3);
            auto dimension(srcDimension);
            CHECK(dimension.GetDimensionX() == 1);
            CHECK(dimension.GetDimensionY() == 2);
            CHECK(dimension.GetDimensionZ() == 3);
        }

        SECTION("operator =") {
            Entity::ProcessedData::Dimension srcDimension(1, 2, 3);
            auto dimension = srcDimension;
            CHECK(dimension.GetDimensionX() == 1);
            CHECK(dimension.GetDimensionY() == 2);
            CHECK(dimension.GetDimensionZ() == 3);
        }

        SECTION("operator ==") {
            Entity::ProcessedData::Dimension dimension1(1, 2, 3);
            Entity::ProcessedData::Dimension dimension2(1, 2, 3);
            CHECK(dimension1 == dimension2);
        }

        SECTION("operator !=") {
            Entity::ProcessedData::Dimension dimension1(1, 2, 3);
            Entity::ProcessedData::Dimension dimension2(3, 2, 1);
            CHECK(dimension1 != dimension2);
        }

        SECTION("SetDimension(), GetDimensionXYZ()") {
            Entity::ProcessedData::Dimension dimension;
            dimension.SetDimension(1, 2, 3);
            CHECK(dimension.GetDimensionX() == 1);
            CHECK(dimension.GetDimensionY() == 2);
            CHECK(dimension.GetDimensionZ() == 3);
        }
    }

    TEST_CASE("ProcessedData::MinMax") {
        SECTION("MinMax()") {
            Entity::ProcessedData::MinMax minMax;
            CHECK(minMax.GetMinValue() == 0);
            CHECK(minMax.GetMaxValue() == 0);
        }

        SECTION("MinMax(minValue, maxValue)") {
            Entity::ProcessedData::MinMax minMax(1, 2);
            CHECK(minMax.GetMinValue() == 1);
            CHECK(minMax.GetMaxValue() == 2);
        }

        SECTION("MinMax(MinMax)") {
            Entity::ProcessedData::MinMax srcMinMax(1, 2);
            auto minMax(srcMinMax);
            CHECK(minMax.GetMinValue() == 1);
            CHECK(minMax.GetMaxValue() == 2);
        }

        SECTION("operator=") {
            Entity::ProcessedData::MinMax srcMinMax(1, 2);
            auto minMax = srcMinMax;
            CHECK(minMax.GetMinValue() == 1);
            CHECK(minMax.GetMaxValue() == 2);
        }

        SECTION("operator ==") {
            Entity::ProcessedData::MinMax minMax1(1, 2);
            Entity::ProcessedData::MinMax minMax2(1, 2);
            CHECK(minMax1 == minMax2);
        }

        SECTION("operator !=") {
            Entity::ProcessedData::MinMax minMax1(1, 2);
            Entity::ProcessedData::MinMax minMax2(1, 3);
            CHECK(minMax1 != minMax2);
        }

        SECTION("SetMinMax() / GetMinMaxValue()") {
            Entity::ProcessedData::MinMax minMax;
            minMax.SetMinMax(1, 2);
            CHECK(minMax.GetMinValue() == 1);
            CHECK(minMax.GetMaxValue() == 2);
        }
    }

    TEST_CASE("ProcessedData") {
        SECTION("ProcessedData(dataType) / GetDataType()") {
            Entity::ProcessedData processedData(Entity::DataType::BF);
            CHECK(processedData.GetDataType()._value == Entity::DataType::BF);
        }

        SECTION("SetResolution() / GetResolution()") {
            Entity::ProcessedData processedData(Entity::DataType::HT);
            const auto resolution = Entity::ProcessedData::Resolution{ 1,2,3 };
            processedData.SetResolution(resolution);
            const auto outputResolution = processedData.GetResolution();
            CHECK(outputResolution == resolution);
        }

        SECTION("SetDimension() / GetDimension()") {
            Entity::ProcessedData processedData(Entity::DataType::HT);
            const auto dimension = Entity::ProcessedData::Dimension{ 1,2,3 };
            processedData.SetDimension(dimension);
            const auto outputDimension = processedData.GetDimension();
            CHECK(outputDimension == dimension);
        }

        SECTION("SetMinMax3d() / GetMinMax3d()") {
            Entity::ProcessedData processedData(Entity::DataType::HT);
            const auto minMax3d = Entity::ProcessedData::MinMax{ 1,2 };
            processedData.SetMinMax3d(minMax3d);
            const auto outputMinMax3d = processedData.GetMinMax3d();
            CHECK(outputMinMax3d == minMax3d);
        }

        SECTION("SetMinMaxMip() / GetMinMaxMip()") {
            Entity::ProcessedData processedData(Entity::DataType::HT);
            const auto minMaxMip = Entity::ProcessedData::MinMax{ 1,2 };
            processedData.SetMinMaxMip(minMaxMip);
            const auto outputMinMaxMip = processedData.GetMinMaxMip();
            CHECK(outputMinMaxMip == minMaxMip);
        }

        SECTION("SetData3d() / GetData3d()") {
            Entity::ProcessedData processedData(Entity::DataType::HT);
            const std::any data3d = double(1);
            processedData.SetData3d(data3d);
            const auto outputData3d = processedData.GetData3d();
            CHECK(*outputData3d._Cast<double>() == 1);
        }

        SECTION("SetDataMip() / GetDataMip()") {
            Entity::ProcessedData processedData(Entity::DataType::HT);
            const std::any dataMip = double(1);
            processedData.SetDataMip(dataMip);
            const auto outputDataMip = processedData.GetDataMip();
            CHECK(*outputDataMip._Cast<double>() == 1);
        }
    }
}