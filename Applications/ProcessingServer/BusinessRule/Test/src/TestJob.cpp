#include <catch2/catch.hpp>

#include <Job.h>

using namespace processing_server;

const QString answerDataPath = "testDataPath.input";
const QString answerOutputPath = "testOutputPath.output";
const auto answerDataType = Entity::DataType::BF;
const auto answerTimeFrameIndex = 10;
const auto answerUniqueID = 100;

auto MakeReferenceJobWithJobInfo()-> Entity::Job {
    Entity::Job::Info jobInfo;

    jobInfo.dataPath = answerDataPath;
    jobInfo.outputPath = answerOutputPath;
    jobInfo.dataType = answerDataType;
    jobInfo.index = answerTimeFrameIndex;
    jobInfo.uniqueID = answerUniqueID;
    Entity::Job referenceJob(jobInfo);

    return referenceJob;
}

auto ContentsOfThisJobAreAnswer(const Entity::Job& job)->bool {
    const bool dataPathIsAnswer = job.GetDataPath() == answerDataPath;
    const bool outputPathIsAnswer = job.GetOutputPath() == answerOutputPath;
    const bool dataTypeIsAnswer = static_cast<uint8_t>(job.GetDataType()) == static_cast<uint8_t>(answerDataType);
    const bool timeFrameIndexIsAnswer = job.GetTimeFrameIndex() == answerTimeFrameIndex;
    const bool uniqueIdIsAnswer = job.GetUniqueID() == answerUniqueID;

    return dataPathIsAnswer && outputPathIsAnswer && dataTypeIsAnswer && timeFrameIndexIsAnswer &&
        uniqueIdIsAnswer;
}

SCENARIO("The access method functions are working well.") {
    GIVEN("Custom JobInfo ") {
        auto job = MakeReferenceJobWithJobInfo();

        WHEN("Job is created with custom JobInfo") {
            THEN("GetDataPath() is doing well") {
                CHECK(job.GetDataPath() == answerDataPath);
            }
            THEN("GetOutputPath() is doing well") {
                CHECK(job.GetOutputPath() == answerOutputPath);
            }
            THEN("GetDataType() is doing well") {
                CHECK(static_cast<uint8_t>(job.GetDataType()) == static_cast<uint8_t>(answerDataType));
            }
            THEN("GetTimeFrameIndex() is doing well") {
                CHECK(job.GetTimeFrameIndex() == answerTimeFrameIndex);
            }
            THEN("GetUniqueID() is doing well") {
                CHECK(job.GetUniqueID() == answerUniqueID);
            }
        }
    }
}

SCENARIO("The copy constructions of Job work differently") {
    GIVEN("A new Job instance is created by copying with '= job' ") {

        auto job = MakeReferenceJobWithJobInfo();
        {
            auto newJob = job;
            CHECK(ContentsOfThisJobAreAnswer(newJob));
        }

        WHEN("newJob is destructed") {
            THEN("Member variables of original job are survived") {
                CHECK(ContentsOfThisJobAreAnswer(job));
            }
        }
    }

    GIVEN("A new Job instance is created by copying with 'job(originalJob)'") {
        auto job = MakeReferenceJobWithJobInfo();
        {
            auto newJob(job);
            CHECK(ContentsOfThisJobAreAnswer(newJob));
        }
        WHEN("newJob is destructed") {
            THEN("Member variables of original job are survived") {
                CHECK(ContentsOfThisJobAreAnswer(job));
            }
        }

    }
}


