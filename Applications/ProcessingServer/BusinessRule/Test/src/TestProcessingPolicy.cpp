#include <catch2/catch.hpp>

#include "ProcessingPolicy.h"

using namespace processing_server;

namespace _TestProcessingPolicy {
    SCENARIO("ProcessingPolicy returns proper requestType") {
        GIVEN("job whose datatype is HT") {
            Entity::Job::Info jobInfo;
            jobInfo.dataType = Entity::DataType::HT;
            const auto job = std::make_shared<Entity::Job>(jobInfo);

            WHEN("GetRequestType() is called") {
                const auto requestType = Entity::ProcessingPolicy::GetRequestType(job);
                THEN("ProcessorType is LOCAL_GPU") {
                    const auto processorType = std::get<0>(requestType);
                    CHECK(processorType._value == Entity::ProcessorType::LOCAL_GPU);
                }
                THEN("PriorityType is ANY") {
                    const auto processorPriority = std::get<1>(requestType);
                    CHECK(processorPriority._value == Entity::ProcessorPriority::ANY);
                }
            }
        }

        GIVEN("job whose datatype is FLBLUE") {
            Entity::Job::Info jobInfo;
            jobInfo.dataType = Entity::DataType::FLBLUE;
            const auto job = std::make_shared<Entity::Job>(jobInfo);

            WHEN("GetRequestType() is called") {
                const auto requestType = Entity::ProcessingPolicy::GetRequestType(job);
                THEN("ProcessorType is LOCAL_GPU") {
                    const auto processorType = std::get<0>(requestType);
                    CHECK(processorType._value == Entity::ProcessorType::LOCAL_GPU);
                }
                THEN("PriorityType is MAIN") {
                    const auto processorPriority = std::get<1>(requestType);
                    CHECK(processorPriority._value == Entity::ProcessorPriority::MAIN);
                }
            }
        }

        GIVEN("job whose datatype is FLGREEN") {
            Entity::Job::Info jobInfo;
            jobInfo.dataType = Entity::DataType::FLGREEN;
            const auto job = std::make_shared<Entity::Job>(jobInfo);

            WHEN("GetRequestType() is called") {
                const auto requestType = Entity::ProcessingPolicy::GetRequestType(job);
                THEN("ProcessorType is LOCAL_GPU") {
                    const auto processorType = std::get<0>(requestType);
                    CHECK(processorType._value == Entity::ProcessorType::LOCAL_GPU);
                }
                THEN("PriorityType is MAIN") {
                    const auto processorPriority = std::get<1>(requestType);
                    CHECK(processorPriority._value == Entity::ProcessorPriority::MAIN);
                }
            }
        }

        GIVEN("job whose datatype is FLRED") {
            Entity::Job::Info jobInfo;
            jobInfo.dataType = Entity::DataType::FLRED;
            const auto job = std::make_shared<Entity::Job>(jobInfo);

            WHEN("GetRequestType() is called") {
                const auto requestType = Entity::ProcessingPolicy::GetRequestType(job);
                THEN("ProcessorType is LOCAL_GPU") {
                    const auto processorType = std::get<0>(requestType);
                    CHECK(processorType._value == Entity::ProcessorType::LOCAL_GPU);
                }
                THEN("PriorityType is MAIN") {
                    const auto processorPriority = std::get<1>(requestType);
                    CHECK(processorPriority._value == Entity::ProcessorPriority::MAIN);
                }
            }
        }

        GIVEN("job whose datatype is PHASE") {
            Entity::Job::Info jobInfo;
            jobInfo.dataType = Entity::DataType::PHASE;
            const auto job = std::make_shared<Entity::Job>(jobInfo);

            WHEN("GetRequestType() is called") {
                const auto requestType = Entity::ProcessingPolicy::GetRequestType(job);
                THEN("ProcessorType is LOCAL_GPU") {
                    const auto processorType = std::get<0>(requestType);
                    CHECK(processorType._value == Entity::ProcessorType::LOCAL_GPU);
                }
                THEN("PriorityType is ANY") {
                    const auto processorPriority = std::get<1>(requestType);
                    CHECK(processorPriority._value == Entity::ProcessorPriority::ANY);
                }
            }
        }
    }
}