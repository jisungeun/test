#include <catch2/catch.hpp>

#include <JobQueue.h>

using namespace processing_server;

namespace _TestJobQueue {


    auto CreateJobByUniqueId(const Entity::Job::ID& id) -> Entity::Job::Pointer {
        Entity::Job::Info jobInfo;
        jobInfo.uniqueID = id;

        return std::make_shared<Entity::Job>(jobInfo);
    }

    auto AddJobsToJobQueueInOrder(Entity::JobQueue& jobQueue, const int32_t numberOfJobs,
        const int32_t startIdNumber = 1) ->void {
        for (auto i = startIdNumber; i <= startIdNumber + numberOfJobs - 1; ++i) {
            auto job = CreateJobByUniqueId(i);
            jobQueue.Add(job);
        }
    }

    auto GetOrderInJobQueue(const Entity::Job::ID& targetId, Entity::JobQueue& jobQueue) ->int32_t {
        auto order = 0;
        while (true) {
            const auto nextJob = jobQueue.GetNextJob();
            const auto nextJobId = nextJob->GetUniqueID();
            if (nextJobId == targetId) {
                break;
            }
            else {
                jobQueue.Remove(nextJobId);
                ++order;
            }
        }
        return order;
    }

    SCENARIO("JobQueue is created as an empty form") {
        GIVEN("JobQueue is created") {
            Entity::JobQueue jobQueue;
            WHEN("No Job is added to JobQueue") {
                THEN("JobQueue is empty") {
                    CHECK(jobQueue.Empty() == true);
                }
            }
        }
    }

    SCENARIO("Size method is working well") {
        GIVEN("Empty JobQueue") {
            Entity::JobQueue jobQueue;
            WHEN("100 jobs are added to empty JobQueue") {
                const auto numberOfAddedJob = 100;
                AddJobsToJobQueueInOrder(jobQueue, numberOfAddedJob);
                THEN("Size of jobQueue is 100") {
                    CHECK(jobQueue.Size() == numberOfAddedJob);
                }
            }
        }

        GIVEN("JobQueue which has 100 jobs") {
            Entity::JobQueue jobQueue;
            const auto numberOfAddedJob = 100;
            AddJobsToJobQueueInOrder(jobQueue, numberOfAddedJob);
            CHECK(jobQueue.Size() == numberOfAddedJob);

            WHEN("200 jobs are added to jobQueue(not empty)") {
                const auto extraNumberOfAddedJob = 200;
                AddJobsToJobQueueInOrder(jobQueue, extraNumberOfAddedJob, numberOfAddedJob + 1);
                THEN("Size of jobQueue is 300") {
                    const auto totalNumberOfJobs = numberOfAddedJob + extraNumberOfAddedJob;
                    CHECK(jobQueue.Size() == totalNumberOfJobs);
                }
            }

        }

    }


    SCENARIO("Addition of Job is working well") {
        GIVEN("Empty jobQueue is created") {
            Entity::JobQueue jobQueue;

            WHEN("One Job is added to JobQueue") {
                const auto jobId = 1;
                const auto job = CreateJobByUniqueId(jobId);
                jobQueue.Add(job);
                THEN("JobQueue is not empty") {
                    CHECK(jobQueue.Empty() == false);
                }
            }

            WHEN("First job is added to JobQueue") {
                const auto firstJobId = 10;
                auto firstJob = CreateJobByUniqueId(firstJobId);
                jobQueue.Add(firstJob);

                THEN("NextJob of JobQueue is the first job") {
                    const auto nextJob = jobQueue.GetNextJob();
                    CHECK(nextJob->GetUniqueID() == firstJobId);
                }
            }

            WHEN("First and second job are added to JobQueue in order") {
                const auto firstJobId = 10;
                const auto secondJobId = 11;

                auto firstJob = CreateJobByUniqueId(firstJobId);
                auto secondJob = CreateJobByUniqueId(secondJobId);

                jobQueue.Add(firstJob);
                jobQueue.Add(secondJob);

                THEN("NextJob of JobQueue is the first job") {
                    const auto nextJob = jobQueue.GetNextJob();
                    CHECK(nextJob->GetUniqueID() == firstJobId);
                }
            }

            WHEN("100 jobs are added to JobQueue in order") {
                const auto firstJobId = 1;

                const auto numberOfJobs = 100;
                AddJobsToJobQueueInOrder(jobQueue, numberOfJobs);

                THEN("Size of JobQueue is 100") {
                    const auto jobQueueSize = jobQueue.Size();
                    CHECK(jobQueueSize == numberOfJobs);
                }

                THEN("NextJob of JobQueue is the first job") {
                    const auto nextJob = jobQueue.GetNextJob();
                    CHECK(nextJob->GetUniqueID() == firstJobId);
                }
            }
        }
    }

    SCENARIO("Removal of Job from JobQueue is working well") {
        GIVEN("Empty JobQueue") {
            Entity::JobQueue jobQueue;

            WHEN("Remove job from emtpy JobQueue") {
                const auto targetId = 1;
                bool jobQueueExceptionIsThrown;
                try {
                    jobQueue.Remove(targetId);
                    jobQueueExceptionIsThrown = false;
                }
                catch (const Entity::JobQueue::JobQueueException&) {
                    jobQueueExceptionIsThrown = true;
                }
                THEN("JobQueueException is thrown") {
                    CHECK(jobQueueExceptionIsThrown == true);
                }
            }
        }
        GIVEN("JobQueue filled with 100 jobs whose id are 1~100 in order") {
            Entity::JobQueue jobQueue;
            const auto numberOfJobs = 100;
            AddJobsToJobQueueInOrder(jobQueue, numberOfJobs);

            WHEN("Remove first job") {
                const auto firstJobId = 1;
                CHECK(firstJobId < numberOfJobs);

                jobQueue.Remove(firstJobId);

                THEN("Size of JobQueue is reduced") {
                    const auto jobQueueSize = jobQueue.Size();
                    const auto answerJobQueueSize = numberOfJobs - 1;
                    CHECK(jobQueueSize == answerJobQueueSize);
                }

                THEN("Next job id in JobQueue is firstJobId + 1") {
                    const auto nextJob = jobQueue.GetNextJob();
                    const auto nextJobId = nextJob->GetUniqueID();
                    const auto answerJobId = firstJobId + 1;
                    CHECK(nextJobId == answerJobId);
                }
            }

            WHEN("Remove out-bounded-job") {
                const auto outBoundedJobId = 200;
                CHECK(outBoundedJobId > numberOfJobs);

                bool jobQueueExceptionIsThrown;
                try {
                    jobQueue.Remove(outBoundedJobId);
                    jobQueueExceptionIsThrown = false;
                }
                catch (const Entity::JobQueue::JobQueueException&) {
                    jobQueueExceptionIsThrown = true;
                }
                THEN("JobQueueException is thrown") {
                    CHECK(jobQueueExceptionIsThrown == true);
                }
            }

            WHEN("Remove first several jobs") {
                const auto numberOfRemovalJobs = 20;
                for (auto i = 1; i <= numberOfRemovalJobs; ++i) {
                    jobQueue.Remove(i);
                }

                THEN("Size of JobQueue is Reduced") {
                    const auto answerNumber = numberOfJobs - numberOfRemovalJobs;
                    CHECK(jobQueue.Size() == answerNumber);
                }

                THEN("Next job id in JobQueue is number of removal jobs + 1") {
                    const auto nextJob = jobQueue.GetNextJob();
                    const auto nextJobId = nextJob->GetUniqueID();
                    const auto answerJobId = numberOfRemovalJobs + 1;
                    CHECK(nextJobId == answerJobId);
                }
            }

            WHEN("Remove all jobs") {
                const auto numberOfRemovalJobs = numberOfJobs;
                for (auto i = 1; i <= numberOfRemovalJobs; ++i) {
                    jobQueue.Remove(i);
                }
                THEN("jobQueue is empty") {
                    CHECK(jobQueue.Empty() == true);
                }

                THEN("Size of jobQueue is zero") {
                    CHECK(jobQueue.Size() == 0);
                }
            }
        }
    }

    SCENARIO("Clear is working well") {
        GIVEN("Empty JobQueue") {
            Entity::JobQueue jobQueue;
            CHECK(jobQueue.Empty() == true);

            WHEN("clear empty JobQueue") {
                jobQueue.Clear();
                THEN("jobQueue is empty") {
                    CHECK(jobQueue.Empty() == true);
                }

                THEN("Size of jobQueue is zero") {
                    CHECK(jobQueue.Size() == 0);
                }
            }
        }

        GIVEN("Not empty JobQueue") {
            Entity::JobQueue jobQueue;
            const auto numberOfAddedJobs = 100;
            AddJobsToJobQueueInOrder(jobQueue, numberOfAddedJobs);
            CHECK(jobQueue.Empty() == false);

            WHEN("clear not empty JobQueue") {
                jobQueue.Clear();
                THEN("jobQueue is empty") {
                    CHECK(jobQueue.Empty() == true);
                }

                THEN("Size of jobQueue is zero") {
                    CHECK(jobQueue.Size() == 0);
                }
            }

        }
    }

    SCENARIO("GetNextJob of Empty jobQueue throws JobQueueException") {
        GIVEN("Empty JobQueue") {
            Entity::JobQueue jobQueue;
            CHECK(jobQueue.Empty() == true);

            WHEN("GetNextJob method is called from empty jobQueue") {
                bool jobQueueExceptionIsThrown;
                try {
                    const auto nextJob = jobQueue.GetNextJob();
                    jobQueueExceptionIsThrown = false;
                }
                catch (const Entity::JobQueue::JobQueueException&) {
                    jobQueueExceptionIsThrown = true;
                }
                THEN("JobQueueException is thrown") {
                    CHECK(jobQueueExceptionIsThrown == true);
                }
            }
        }
    }

    SCENARIO("MoveFront is working well") {
        GIVEN("Empty JobQueue") {
            Entity::JobQueue jobQueue;
            CHECK(jobQueue.Empty() == true);

            WHEN("MoveFront method is called from empty jobQueue") {
                bool jobQueueExceptionIsThrown;
                try {
                    jobQueue.MoveFront(0);
                    jobQueueExceptionIsThrown = false;
                }
                catch (const Entity::JobQueue::JobQueueException&) {
                    jobQueueExceptionIsThrown = true;
                }
                THEN("JobQueueException is thrown") {
                    CHECK(jobQueueExceptionIsThrown == true);
                }
            }
        }

        GIVEN("Not Empty JobQueue") {
            Entity::JobQueue jobQueue;
            const auto numberOfAddedJobs = 100;
            AddJobsToJobQueueInOrder(jobQueue, numberOfAddedJobs);

            WHEN("MoveFront(in-bound-id) is called from jobQueue") {
                const auto inBoundTargetId = 30;
                CHECK(inBoundTargetId < numberOfAddedJobs);

                jobQueue.MoveFront(inBoundTargetId);

                THEN("ID of NextJob of jobQueue is targetId") {
                    const auto nextJob = jobQueue.GetNextJob();
                    CHECK(nextJob->GetUniqueID() == inBoundTargetId);
                }

                THEN("Size of JobQueue remains") {
                    CHECK(jobQueue.Size() == numberOfAddedJobs);
                }
            }

            WHEN("MoveFront(out-bound-id) is called from jobQueue") {
                const auto outBoundId = 200;
                CHECK(outBoundId > numberOfAddedJobs);

                bool jobQueueExceptionIsThrown;
                try {
                    jobQueue.MoveFront(outBoundId);
                    jobQueueExceptionIsThrown = false;
                }
                catch (const Entity::JobQueue::JobQueueException&) {
                    jobQueueExceptionIsThrown = true;
                }
                THEN("JobQueueException is thrown") {
                    CHECK(jobQueueExceptionIsThrown == true);
                }

                THEN("Size of JobQueue remains") {
                    CHECK(jobQueue.Size() == numberOfAddedJobs);
                }
            }
        }
    }

    SCENARIO("MoveBack is working well") {
        GIVEN("Empty JobQueue") {
            Entity::JobQueue jobQueue;
            CHECK(jobQueue.Empty() == true);

            WHEN("MoveFront method is called from empty jobQueue") {
                bool jobQueueExceptionIsThrown;
                try {
                    jobQueue.MoveBack(0);
                    jobQueueExceptionIsThrown = false;
                }
                catch (const Entity::JobQueue::JobQueueException&) {
                    jobQueueExceptionIsThrown = true;
                }
                THEN("JobQueueException is thrown") {
                    CHECK(jobQueueExceptionIsThrown == true);
                }
            }
        }

        GIVEN("Not Empty JobQueue") {
            Entity::JobQueue jobQueue;
            const auto numberOfAddedJobs = 100;
            AddJobsToJobQueueInOrder(jobQueue, numberOfAddedJobs);

            WHEN("MoveBack(in-bound-id) is called from jobQueue") {
                const auto inBoundTargetId = 30;
                CHECK(inBoundTargetId < numberOfAddedJobs);

                jobQueue.MoveBack(inBoundTargetId);

                THEN("Size of JobQueue remains") {
                    CHECK(jobQueue.Size() == numberOfAddedJobs);
                }

                THEN("Order of targetId is last") {
                    auto order = GetOrderInJobQueue(inBoundTargetId, jobQueue);
                    CHECK(order == (numberOfAddedJobs - 1));
                }

            }

            WHEN("MoveBack(out-bound-id) is called from jobQueue") {
                const auto outBoundId = 200;
                CHECK(outBoundId > numberOfAddedJobs);

                bool jobQueueExceptionIsThrown;
                try {
                    jobQueue.MoveBack(outBoundId);
                    jobQueueExceptionIsThrown = false;
                }
                catch (const Entity::JobQueue::JobQueueException&) {
                    jobQueueExceptionIsThrown = true;
                }
                THEN("JobQueueException is thrown") {
                    CHECK(jobQueueExceptionIsThrown == true);
                }

                THEN("Size of JobQueue remains") {
                    CHECK(jobQueue.Size() == numberOfAddedJobs);
                }
            }
        }
    }

    SCENARIO("GetJobByIndex is working well") {
        GIVEN("JobQueue inserted two job in order") {
            Entity::JobQueue jobQueue;
            CHECK(jobQueue.Empty() == true);

            Entity::Job::Info info1;
            info1.dataPath = "first";

            Entity::Job::Info info2;
            info2.dataPath = "second";

            jobQueue.Add(std::make_shared<Entity::Job>(info1));
            jobQueue.Add(std::make_shared<Entity::Job>(info2));

            WHEN("Second job is called from jobQueue by GetJobByIndex") {
                const auto job = jobQueue.GetJobByIndex(1);
                THEN("The job's dataPath is 'second'") {
                    CHECK(job->GetDataPath() == "second");
                }
            }

            WHEN("Index is outbounded") {
                bool jobQueueExceptionIsThrown = false;
                const auto boutBoundIndex = 2;
                try {
                    const auto job = jobQueue.GetJobByIndex(boutBoundIndex);
                    jobQueueExceptionIsThrown = false;
                }
                catch (const Entity::JobQueue::JobQueueException&) {
                    jobQueueExceptionIsThrown = true;
                }
                THEN("exception is thrown") {
                    CHECK(jobQueueExceptionIsThrown == true);
                }
            }
        }
    }
}