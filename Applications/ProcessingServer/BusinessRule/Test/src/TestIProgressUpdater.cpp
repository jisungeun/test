#include <catch2/catch.hpp>

#include <IProgressUpdater.h>

using namespace processing_server;

namespace _Test {
    class DummyProgressUpdater : public Entity::IProgressUpdater {
    public:
        DummyProgressUpdater() = default;
        ~DummyProgressUpdater() = default;
        auto Update(const Entity::Progress::Parameter& parameter)-> void override {
            this->dataPath = parameter.job->GetDataPath();
            this->dataType = parameter.job->GetDataType();
            this->timeFrameIndex = parameter.job->GetTimeFrameIndex();
            this->completeProgressValue = parameter.completeProgressValue;
        }

        auto GetDataPath()->QString {
            return dataPath;
        }

        auto GetDataType()->Entity::DataType {
            return dataType;
        }

        auto GetTimeFrameIndex()->uint32_t {
            return timeFrameIndex;
        }

        auto GetCompleteProgressValue()->float {
            return completeProgressValue;
        }
    private:
        QString dataPath;
        Entity::DataType dataType{ Entity::DataType::HT };
        uint32_t timeFrameIndex{ 0 };
        float completeProgressValue{ 0 };
    };
}

namespace _TestIProgressUpdater {
    const QString answerDataPath = "DataPath/Path.path";
    const Entity::DataType answerDataType = Entity::DataType::BF;
    const uint32_t answerTimeFrameIndex = 10;
    const float answerProgressValue = 1.f;

    SCENARIO("Update is working well") {
        GIVEN("ProgressUpdater instance") {
            _Test::DummyProgressUpdater updater;
            AND_GIVEN("ProgressUpdateParameter") {

                Entity::Job::Info jobInfo;
                jobInfo.dataPath = answerDataPath;
                jobInfo.dataType = answerDataType;
                jobInfo.index = answerTimeFrameIndex;

                Entity::Progress::Parameter parameter;
                parameter.job = std::make_shared<Entity::Job>(jobInfo);
                parameter.completeProgressValue = answerProgressValue;

                WHEN("update with parameter") {
                    updater.Update(parameter);
                    THEN("dataPath is updated") {
                        CHECK(updater.GetDataPath() == answerDataPath);
                    }
                    THEN("dataType is updated") {
                        CHECK(updater.GetDataType() == answerDataType);
                    }
                    THEN("timeFrameIndex is updated") {
                        CHECK(updater.GetTimeFrameIndex() == answerTimeFrameIndex);
                    }
                    THEN("completeProgressValue is updated") {
                        CHECK(updater.GetCompleteProgressValue() == answerProgressValue);
                    }
                }
            }
        }

    }
}
