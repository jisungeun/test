#include <catch2/catch.hpp>

#include "WritingData.h"

using namespace processing_server;

namespace _TestWritingData {
    SCENARIO("WritingData is created as a empty state") {
        GIVEN("WritingData instance") {
            const auto job = std::make_shared<Entity::Job>();
            const auto processedData = std::make_shared<Entity::ProcessedData>(Entity::DataType::HT);

            Entity::WritingData writingData(job, processedData);

            WHEN("Nothing happens") {
                THEN("dataPath is emtpy") {
                    CHECK(writingData.GetJobPointer()->GetDataPath().isEmpty());
                }
                THEN("outputPath is empty") {
                    CHECK(writingData.GetJobPointer()->GetOutputPath().isEmpty());
                }
            }
        }
    }

    SCENARIO("ProcessedData is set well") {

        AND_GIVEN("processedData") {
            auto processedData
                = (std::make_shared<Entity::ProcessedData>(Entity::DataType::BF));
            GIVEN("WritingData instance") {
                const auto job = std::make_shared<Entity::Job>();

                Entity::WritingData writingData(job, processedData);
                WHEN("processedData is set to WritingData") {
                    THEN("processedData is set well") {
                        CHECK(writingData.GetProcessedData().get() == processedData.get());
                    }
                }
            }
        }
    }
}
