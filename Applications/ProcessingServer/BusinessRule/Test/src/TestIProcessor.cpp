#include <catch2/catch.hpp>

#include <IProcessor.h>

using namespace processing_server;
namespace _TestIProcessor {

    namespace _Test {
        class DummyProcessor : public Entity::IProcessor {
        public:
            DummyProcessor() = default;
            ~DummyProcessor() = default;
            auto SetType(const Entity::ProcessorType& type) -> void override {
                this->type = type;
            }
            auto GetType() const -> Entity::ProcessorType override {
                return type;
            }

            auto SetPriority(const Entity::ProcessorPriority& priority) -> void override {
                this->priority = priority;
            }
            auto GetPriority() const -> Entity::ProcessorPriority override {
                return priority;
            }
            auto GetStatus() const -> Entity::ProcessorStatus override {
                return status;
            }

            auto IsWorking() const -> bool override {
                return true;
            }

            auto SetJobPointer(const Entity::Job::Pointer job)-> void override {
                this->job = job;
            }
            auto GetJob() const ->Entity::Job::Pointer {
                return job;
            }

            auto SetUpdater(const Entity::IProgressUpdater::Pointer updater)-> void override {
                this->updater = updater;
            }
            auto GetUpdater()->Entity::IProgressUpdater::Pointer {
                return updater;
            }

            auto StartProcess()-> void override {
                processIsStarted = true;
            }

            auto GetProcessIsStarted()->bool {
                return processIsStarted;
            }

            auto SetProcessedData(const Entity::ProcessedData::Pointer& processedData)->void {
                this->processedData = processedData;
            }

            auto GetProcessedData() -> Entity::ProcessedData::Pointer override {
                return processedData;
            }

            auto GetProcessedDataMemoryInBytes() -> size_t override {
                return 10;
            }
        private:
            Entity::ProcessorStatus status{ Entity::ProcessorStatus::IDLE };
            Entity::ProcessedData::Pointer processedData{};
            Entity::Job::Pointer job{};
            Entity::IProgressUpdater::Pointer updater{};
            Entity::ProcessorType type{ Entity::ProcessorType::LOCAL_GPU };
            Entity::ProcessorPriority priority{ Entity::ProcessorPriority::MAIN };
            bool processIsStarted{ false };
        };

        class DummyUpdater : public Entity::IProgressUpdater {
        public:
            DummyUpdater() = default;
            ~DummyUpdater() = default;
            auto Update(const Entity::Progress::Parameter& parameter) -> void override {
            }
        };
    }

        TEST_CASE("IProcessor") {
            _Test::DummyProcessor processor;
            SECTION("SetType(), GetType()") {
                processor.SetType(Entity::ProcessorType::REMOTE_GPU);
                CHECK(processor.GetType()._value == Entity::ProcessorType::REMOTE_GPU);
            }
            SECTION("SetPriority(), GetPriority()") {
                processor.SetPriority(Entity::ProcessorPriority::SUB);
                CHECK(processor.GetPriority()._value == Entity::ProcessorPriority::SUB);
            }
            SECTION("SetStatus(), GetStatus()") {
                CHECK(processor.GetStatus()._value == Entity::ProcessorStatus::IDLE);
            }
            SECTION("IsWorking()") {
                CHECK(processor.IsWorking() == true);
            }
            SECTION("SetJob()") {
                auto job = std::make_shared<Entity::Job>();
                processor.SetJobPointer(job);
                CHECK(processor.GetJob().get() == job.get());
            }
            SECTION("SetUpdater()") {
                _Test::DummyUpdater::Pointer updater = std::make_shared<_Test::DummyUpdater>();
                processor.SetUpdater(updater);
                CHECK(processor.GetUpdater().get() == updater.get());
            }
            SECTION("StartProcess()") {
                processor.StartProcess();
                CHECK(processor.GetProcessIsStarted() == true);
            }
            SECTION("GetProcessedData()") {
                auto data = std::make_shared<Entity::ProcessedData>(Entity::DataType::HT);
                processor.SetProcessedData(data);
                CHECK(processor.GetProcessedData().get() == data.get());
            }
            SECTION("GetProcessedDataMemoryInBytes()") {
                auto memorySize = processor.GetProcessedDataMemoryInBytes();
                CHECK(memorySize == 10);
            }
        }


}