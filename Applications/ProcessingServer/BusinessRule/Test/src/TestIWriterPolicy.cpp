#include <catch2/catch.hpp>

#include "IWriterPolicy.h"

using namespace processing_server;

namespace _Test {
    class DummyWriterPolicy : Entity::IWriterPolicy {
    public:
        DummyWriterPolicy() = default;
        ~DummyWriterPolicy() = default;

        auto SetLimitMemorySizeInBytes(const size_t& limitMemorySizeInBytes) -> void override {
            this->limitMemorySizeInBytes = limitMemorySizeInBytes;
        }

        auto Accept(const size_t& requiredMemorySizeInBytes, const size_t& usedMemorySizeInBytes) -> bool override {
            return (requiredMemorySizeInBytes + usedMemorySizeInBytes) < limitMemorySizeInBytes;
        }

    private:
        size_t limitMemorySizeInBytes{ 0 };
    };
}

namespace _TestIWriterPolicy {
    SCENARIO("WriterPolicy returns proper Acceptance flag") {
        GIVEN("Limit memory value") {
            const size_t limitMemorySizeInBytes = static_cast<size_t>(32 * std::pow(1024,3));
            auto writerPolicy = _Test::DummyWriterPolicy();
            writerPolicy.SetLimitMemorySizeInBytes(limitMemorySizeInBytes);
            WHEN("Empty memory space is enough") {
                const size_t usedMemorySizeInBytes = 0;
                const size_t requiredMemorySize = static_cast<size_t>(100 * std::pow(1024, 2));

                THEN("Accept() returns true") {
                    CHECK(writerPolicy.Accept(requiredMemorySize, usedMemorySizeInBytes) == true);
                }
            }

            WHEN("Empty memory space is short") {
                const size_t usedMemorySizeInBytes = limitMemorySizeInBytes;
                const size_t requiredMemorySize = static_cast<size_t>(100 * std::pow(1024, 2));

                THEN("Accept() returns false") {
                    CHECK(writerPolicy.Accept(requiredMemorySize, usedMemorySizeInBytes) == false);
                }
            }
        }
    }


}
