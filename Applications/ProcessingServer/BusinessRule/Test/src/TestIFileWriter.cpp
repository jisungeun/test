#include <catch2/catch.hpp>

#include "IFileWriter.h"

using namespace processing_server;

namespace _TestIFileWriter {
    namespace _Test {
        class DummyFileWriter : public Entity::IFileWriter {
        public:
            DummyFileWriter() = default;
            ~DummyFileWriter() = default;
            auto GetUsedMemoryInBytes() -> size_t override {
                return 10;
            }

            auto SetUpdater(const Entity::IProgressUpdater::Pointer& progressUpdater) -> void override {
                this->updater = progressUpdater;
            }

            auto GetUpdater() const ->Entity::IProgressUpdater::Pointer {
                return updater;
            }

            auto RequestToWrite(const Entity::WritingData::Pointer& writingData) -> void override {
                this->writingData = writingData;
            }

            auto GetWritingData() const -> Entity::WritingData::Pointer {
                return writingData;
            }

            auto Clear() -> void override {
                cleared = true;
            }

            auto GetCleared() const -> bool {
                return cleared;
            }

            auto SetSoftwareVersion(const QString& softwareVersion) -> void override {
            }
        private:
            Entity::IProgressUpdater::Pointer updater;
            Entity::WritingData::Pointer writingData;
            bool cleared{ false };
        };

        class DummyProgressUpdater : public Entity::IProgressUpdater {
        public:
            DummyProgressUpdater() = default;
            ~DummyProgressUpdater() = default;
            auto Update(const Entity::Progress::Parameter& parameter) -> void override {
            }
        };
    }

    TEST_CASE("IFileWriter") {
        _Test::DummyFileWriter writer;

        SECTION("GetUsedMemoryInBytes()") {
            CHECK(writer.GetUsedMemoryInBytes() == 10);
        }

        SECTION("SetUpdater()") {
            _Test::DummyProgressUpdater::Pointer updater = std::make_shared<_Test::DummyProgressUpdater>();
            writer.SetUpdater(updater);
            CHECK(writer.GetUpdater().get() == updater.get());
        }

        SECTION("RequestToWrite()") {
            auto data = std::make_shared<Entity::WritingData>();
            writer.RequestToWrite(data);
            CHECK(writer.GetWritingData().get() == data.get());
        }

        SECTION("Clear") {
            writer.Clear();
            CHECK(writer.GetCleared() == true);
        }
    }
}

