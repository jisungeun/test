#include <catch2/catch.hpp>

#include "ProcessorRegistry.h"

namespace _Test {
    class DummyProcessor;
}

using namespace processing_server;

namespace _TestProcessorRegistry {
    namespace _Test {
        class DummyProcessor:public Entity::IProcessor {
        public:
            DummyProcessor(const Entity::ProcessorType& type, const Entity::ProcessorPriority& priority)
                : processorType(type), priority(priority){
            }
            ~DummyProcessor() = default;
            auto IsWorking() const -> bool override {
                return status._value != Entity::ProcessorStatus::IDLE;
            }
            auto SetJobPointer(const Entity::Job::Pointer job) -> void override {
            }
            auto SetUpdater(const Entity::IProgressUpdater::Pointer updater) -> void override {
            }
            auto StartProcess() -> void override {
                status = Entity::ProcessorStatus::PROCESSING;
            }
            auto GetProcessedData() -> Entity::ProcessedData::Pointer override {
                return Entity::ProcessedData::Pointer{};
            }

            auto SetType(const Entity::ProcessorType& type) -> void override {
                this->processorType = type;
            }
            auto GetType() const -> Entity::ProcessorType override {
                return processorType;
            }
            auto SetPriority(const Entity::ProcessorPriority& priority) -> void override {
                this->priority = priority;
            }
            auto GetPriority() const -> Entity::ProcessorPriority override {
                return priority;
            }
            auto GetStatus() const -> Entity::ProcessorStatus override {
                return status;
            }

            auto GetProcessedDataMemoryInBytes() -> size_t override {
                return 10;
            }
        private:
            Entity::ProcessorStatus status{ Entity::ProcessorStatus::IDLE };
            Entity::Job::Pointer job{};
            Entity::IProgressUpdater::Pointer updater{};
            Entity::ProcessedData::Pointer processedData{};
            uint32_t processedDataNumber{ 0 };
            Entity::ProcessorType processorType{ Entity::ProcessorType::LOCAL_GPU };
            Entity::ProcessorPriority priority{ Entity::ProcessorPriority::MAIN };
        };
    }

    auto AddAllPossibleProcessor(const Entity::ProcessorRegistry::Pointer& registry) {
        for (auto i = 0; i < Entity::ProcessorType::_size(); ++i) {
            for (auto j = 0; j < Entity::ProcessorPriority::_size(); ++j) {
                _Test::DummyProcessor::Pointer dummyProcessor =
                    std::make_shared<_Test::DummyProcessor>(Entity::ProcessorType::_from_index(i),
                        Entity::ProcessorPriority::_from_index(j));
                registry->Register(dummyProcessor);
            }
        }
    }

    auto CheckTypeMatch(const Entity::ProcessorType& condition, const Entity::ProcessorType& result)->bool {
        if(condition._value == Entity::ProcessorType::ANY) {
            return true;
        }
        if(result == condition) {
            return true;
        } else {
            return false;
        }
    }

    auto CheckPriorityMatch(const Entity::ProcessorPriority& condition, const Entity::ProcessorPriority& result)
        ->bool {
        if (condition._value == Entity::ProcessorPriority::ANY) {
            return true;
        }
        if (result == condition) {
            return true;
        }
        else {
            return false;
        }
    }

    auto CheckRightProcessor(const Entity::ProcessorRegistry::Pointer& registry,
        const Entity::ProcessorType& type, const Entity::ProcessorPriority& priority) {
        const Entity::ProcessorRequestType requestType(type, priority);

        THEN("There is idle processor") {
            CHECK(registry->IdleProcessorExist() == true);
        }
        THEN("There is matched processor") {
            CHECK(registry->CheckIfMatchedProcessorExists(requestType) == true);
        }
        THEN("The MatchedProcess is right processor") {
            const auto matchedProcessor = registry->RequestProcessor(requestType);
            const auto processorType = matchedProcessor->GetType();
            const auto processorPriority = matchedProcessor->GetPriority();

            const auto typeMatch = CheckTypeMatch(type, processorType);
            const auto priorityMatch = CheckPriorityMatch(priority, processorPriority);

            const auto rightProcessor = typeMatch && priorityMatch;
            CHECK(rightProcessor == true);
        }
    }

    SCENARIO("ProcessorRegistry returns proper processor") {
        GIVEN("ProcessorRegistry which has all possible idle processor") {
            const auto registry = std::make_shared<Entity::ProcessorRegistry>();
            AddAllPossibleProcessor(registry);
            WHEN("All possible cases") {
                CheckRightProcessor(registry, Entity::ProcessorType::LOCAL_CPU, Entity::ProcessorPriority::MAIN);
                CheckRightProcessor(registry, Entity::ProcessorType::LOCAL_CPU, Entity::ProcessorPriority::SUB);
                CheckRightProcessor(registry, Entity::ProcessorType::LOCAL_CPU, Entity::ProcessorPriority::ANY);

                CheckRightProcessor(registry, Entity::ProcessorType::LOCAL_GPU, Entity::ProcessorPriority::MAIN);
                CheckRightProcessor(registry, Entity::ProcessorType::LOCAL_GPU, Entity::ProcessorPriority::SUB);
                CheckRightProcessor(registry, Entity::ProcessorType::LOCAL_GPU, Entity::ProcessorPriority::ANY);

                CheckRightProcessor(registry, Entity::ProcessorType::REMOTE_CPU, Entity::ProcessorPriority::MAIN);
                CheckRightProcessor(registry, Entity::ProcessorType::REMOTE_CPU, Entity::ProcessorPriority::SUB);
                CheckRightProcessor(registry, Entity::ProcessorType::REMOTE_CPU, Entity::ProcessorPriority::ANY);

                CheckRightProcessor(registry, Entity::ProcessorType::REMOTE_GPU, Entity::ProcessorPriority::MAIN);
                CheckRightProcessor(registry, Entity::ProcessorType::REMOTE_CPU, Entity::ProcessorPriority::SUB);
                CheckRightProcessor(registry, Entity::ProcessorType::REMOTE_CPU, Entity::ProcessorPriority::ANY);

                CheckRightProcessor(registry, Entity::ProcessorType::ANY, Entity::ProcessorPriority::MAIN);
                CheckRightProcessor(registry, Entity::ProcessorType::ANY, Entity::ProcessorPriority::SUB);
                CheckRightProcessor(registry, Entity::ProcessorType::ANY, Entity::ProcessorPriority::ANY);
            }
        }
    }
}

