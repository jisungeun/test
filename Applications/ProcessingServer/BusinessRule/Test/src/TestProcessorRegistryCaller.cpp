#include <catch2/catch.hpp>

#include "ProcessorRegistryCaller.h"

using namespace processing_server;

namespace _TestProcessingRegistryCaller {
    namespace _Test {
        class DummyProcessor : public Entity::IProcessor {
        public:
            DummyProcessor(const Entity::ProcessorType& type, const Entity::ProcessorPriority& priority)
                {
            }
            auto IsWorking() const -> bool override {
                return false;
            }
            auto SetJobPointer(const Entity::Job::Pointer job) -> void override {
            }
            auto SetUpdater(const Entity::IProgressUpdater::Pointer updater) -> void override {
            }
            auto StartProcess() -> void override {
            }
            auto GetProcessedData() -> Entity::ProcessedData::Pointer override {
                return nullptr;
            }

            auto SetType(const Entity::ProcessorType& type) -> void override {
                this->processorType = type;
            }
            auto GetType() const -> Entity::ProcessorType override {
                return processorType;
            }
            auto SetPriority(const Entity::ProcessorPriority& priority) -> void override {
                this->priority = priority;
            }
            auto GetPriority() const -> Entity::ProcessorPriority override {
                return priority;
            }
            auto GetStatus() const -> Entity::ProcessorStatus override {
                return status;
            }

            auto GetProcessedDataMemoryInBytes() -> size_t override {
                return 10;
            }
        private:
            Entity::ProcessorStatus status{ Entity::ProcessorStatus::IDLE };
            Entity::Job::Pointer job{};
            Entity::IProgressUpdater::Pointer updater{};
            Entity::ProcessedData::Pointer processedData{};
            uint32_t processedDataNumber{ 0 };
            Entity::ProcessorType processorType{ Entity::ProcessorType::LOCAL_GPU };
            Entity::ProcessorPriority priority{ Entity::ProcessorPriority::MAIN };
        };
    }

    auto ClearAllProcessorInRegistry(const Entity::ProcessorRegistry::Pointer& processorRegistry) {
        const auto anyType = std::tuple<Entity::ProcessorType, Entity::ProcessorPriority>(
            Entity::ProcessorType::ANY, Entity::ProcessorPriority::ANY);
        while (processorRegistry->CheckIfMatchedProcessorExists(anyType)) {
            processorRegistry->Unregister(processorRegistry->RequestProcessor(anyType));
        }
    }

    SCENARIO("Caller offers the singleton pattern") {
        GIVEN("ProcessorRegistry instance from caller") {
            auto processorRegistry = Entity::ProcessorRegistryCaller::GetProcessorRegistry();
            ClearAllProcessorInRegistry(processorRegistry);
            CHECK(processorRegistry->IdleProcessorExist() == false);

            WHEN("Processor is registered") {
                _Test::DummyProcessor::Pointer processor =
                    std::make_shared<_Test::DummyProcessor>(
                        Entity::ProcessorType::ANY, Entity::ProcessorPriority::ANY);
                processorRegistry->Register(processor);
                THEN("Idle processor exists") {
                    CHECK(processorRegistry->IdleProcessorExist() == true);
                }
            }
        }

        GIVEN("ProcessorRegistry instance from caller") {
            auto processorRegistry = Entity::ProcessorRegistryCaller::GetProcessorRegistry();
            WHEN("Processor is already registerd at previous test") {
                THEN("Idle processor exists") {
                    CHECK(processorRegistry->IdleProcessorExist() == true);
                }
            }
        }
    }
}

