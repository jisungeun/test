#pragma once
#include <memory>
#include <any>

#include "DataType.h"

#include "ProcessingServerEntityExport.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API ProcessedData {
    public:
        class ProcessingServerEntity_API Resolution {
        public:
            Resolution();
            Resolution(const Resolution& other);
            Resolution(const double& x, const double& y, const double& z);
            ~Resolution();
            auto operator=(const Resolution& other)->Resolution&;
            auto operator==(const Resolution& other) const->bool;
            auto operator!= (const Resolution& other) const->bool;

            auto SetResolution(const double& x, const double& y, const double& z)->void;
            auto GetResolutionX() const ->double;
            auto GetResolutionY() const ->double;
            auto GetResolutionZ() const ->double;

        private:
            struct Impl;
            std::unique_ptr<Impl> d;
        };

        class ProcessingServerEntity_API Dimension {
        public:
            Dimension();
            Dimension(const Dimension& other);
            Dimension(const size_t& x, const size_t& y, const size_t& z);
            ~Dimension();
            auto operator=(const Dimension& other)->Dimension&;
            auto operator==(const Dimension& other) const->bool;
            auto operator!=(const Dimension& other) const->bool;

            auto SetDimension(const size_t& x, const size_t& y, const size_t& z)->void;
            auto GetDimensionX() const->size_t;
            auto GetDimensionY() const->size_t;
            auto GetDimensionZ() const->size_t;

        private:
            struct Impl;
            std::unique_ptr<Impl> d;
        };

        class ProcessingServerEntity_API MinMax {
        public:
            MinMax();
            MinMax(const MinMax& other);
            MinMax(const double& minValue, const double& maxValue);
            ~MinMax();
            auto operator=(const MinMax& other)->MinMax&;
            auto operator==(const MinMax& other) const->bool;
            auto operator!=(const MinMax& other) const->bool;

            auto SetMinMax(const double& minValue, const double& maxValue)->void;
            auto GetMinValue() const -> double;
            auto GetMaxValue() const -> double;

        private:
            struct Impl;
            std::unique_ptr<Impl> d;
        };

        typedef ProcessedData Self;
        typedef std::shared_ptr<Self> Pointer;

        explicit ProcessedData(const DataType& dataType);
        ~ProcessedData();

        auto SetResolution(const Resolution& resolution)->void;
        auto SetDimension(const Dimension& dimension)->void;
        auto SetMinMax3d(const MinMax& minMax3d)->void;
        auto SetMinMaxMip(const MinMax& minMaxMip)->void;
        auto SetData3d(const std::any& data3d)->void;
        auto SetDataMip(const std::any& dataMip)->void;

        auto GetResolution() const->Resolution;
        auto GetDimension() const->Dimension;
        auto GetMinMax3d() const->MinMax;
        auto GetMinMaxMip() const->MinMax;
        auto GetData3d()->std::any;
        auto GetDataMip()->std::any;

        auto GetDataType() const->DataType;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
