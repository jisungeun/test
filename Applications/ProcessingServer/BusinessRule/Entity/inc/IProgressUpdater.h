#pragma once
#include <memory>

#include "Progress.h"

#include "ProcessingServerEntityExport.h"
namespace processing_server::Entity {
    class ProcessingServerEntity_API IProgressUpdater {
    public:
        typedef IProgressUpdater Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual auto Update(const Progress::Parameter& parameter)->void = 0;
    };
}
