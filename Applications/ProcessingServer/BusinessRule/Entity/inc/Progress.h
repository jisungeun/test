#pragma once
#include <memory>
#include <string>

#include "Job.h"
#include "ProcessingServerEntityExport.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API Progress {
    public:
        struct Parameter {
            Job::Pointer job;
            double completeProgressValue{ 0 };
        };

        typedef Progress Self;
        typedef std::shared_ptr<Self> Pointer;

        class ProcessingServerEntity_API ProgressException {
        public:
            explicit ProgressException(const std::string& contents);
            ~ProgressException();
            auto What()->std::string;
        private:
            std::string exceptionContents{};
        };

        Progress(const Job::DataTypeFlag& flag, const uint32_t& totalFrameNumber);
        ~Progress();

        auto ProcessUpdate(const Parameter& parameter)->void;
        auto WritingUpdate(const Parameter& parameter)->void;

        auto GetProgressInDecimal() const ->double;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        auto CreateInstantProgressMap()->void;
        auto BoundedCheck(const double& progress)const->void;
        static auto OutBounded(const double& progress) ->bool;
    };
}
