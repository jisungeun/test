#pragma once
#include <string>
#include <memory>

#include "Job.h"

#include "ProcessingServerEntityExport.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API JobQueue {
    public:
        class ProcessingServerEntity_API JobQueueException {
        public:
            explicit JobQueueException(const std::exception& e);
            explicit JobQueueException(const std::string& contents);
            ~JobQueueException() = default;
            auto What() const ->std::string;
        private:
            std::string exceptionContents;
        };

        JobQueue();
        ~JobQueue();

        auto Add(const Job::Pointer& job)->void;
        auto Remove(const Job::ID& id)->void;
        auto Clear()->void;
        auto GetNextJob() const->Job::Pointer;
        auto GetJobByID(const Job::ID& id) const->Job::Pointer;
        auto GetJobByIndex(const uint32_t& index) const->Job::Pointer;
        auto MoveFront(const Job::ID& id)->void;
        auto MoveBack(const Job::ID& id)->void;
        auto Empty() const ->bool;
        auto Size() const ->size_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        auto EraseJobIdInDeque(const Job::ID& id) const ->void;
        auto FindMatchedIndexInDeque(const Job::ID& id) const->int32_t;
        auto CheckMatchedAndThrowExceptionWhenNotMatched(const bool& matchedFound, const Job::ID& id) const ->void;
    };
}
