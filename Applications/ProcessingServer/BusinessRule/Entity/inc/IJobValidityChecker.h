#pragma once
#include "Job.h"
#include "ProcessingServerEntityExport.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API IJobValidityChecker {
    public:
        virtual ~IJobValidityChecker() = default;
        typedef IJobValidityChecker Self;
        typedef std::shared_ptr<Self> Pointer;
        virtual auto Check(const Job::Pointer& job)->bool = 0;
    };
}
