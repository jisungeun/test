#pragma once
#include "ProcessorRegistry.h"
#include "ProcessingServerEntityExport.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API ProcessorRegistryCaller {
    public:
        static auto GetProcessorRegistry()->ProcessorRegistry::Pointer;
    };
}
