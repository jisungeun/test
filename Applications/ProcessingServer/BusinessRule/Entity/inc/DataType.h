#pragma once

#include <enum.h>

namespace processing_server::Entity {
    BETTER_ENUM(DataType, uint8_t, HT, FLBLUE, FLGREEN, FLRED, PHASE, BF)
}
