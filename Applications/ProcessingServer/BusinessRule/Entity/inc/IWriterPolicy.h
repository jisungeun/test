#pragma once
#include <memory>

#include "ProcessingServerEntityExport.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API IWriterPolicy {
    public:
        virtual ~IWriterPolicy() = default;
        typedef IWriterPolicy Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual auto SetLimitMemorySizeInBytes(const size_t& limitMemorySizeInBytes)->void = 0;
        virtual auto Accept(const size_t& requiredMemorySizeInBytes, const size_t& usedMemorySizeInBytes)->bool = 0;
    };
}
