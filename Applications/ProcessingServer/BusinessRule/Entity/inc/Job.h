#pragma once
#include <memory>

#include <enum.h>
#include <QString>

#include "DataType.h"

#include "ProcessingServerEntityExport.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API Job final {
    public:
        typedef int32_t ID;
        typedef int32_t TimeFrameIndex;
        typedef Job Self;
        typedef std::shared_ptr<Self> Pointer;

        struct DataTypeFlag {
            bool existence[DataType::_size()]{};
        };

        struct Info { 
            ID uniqueID{ -1 };
            QString dataPath{ "" };
            QString outputPath{ "" };
            DataType dataType{ DataType::HT };
            TimeFrameIndex index{ -1 };

            int32_t totalFrameNumber{ -1 };
            bool reprocessingFlag{ false };
            bool deconvolutionFlag{ true };
            DataTypeFlag dataTypeFlag{};
        };

        explicit Job();
        Job(const Job& other);
        explicit Job(const Info& jobInfo);
        ~Job();

        auto GetUniqueID() const->ID;
        auto GetDataPath() const->QString;
        auto GetOutputPath() const->QString;
        auto GetDataType() const->DataType;
        auto GetTimeFrameIndex() const->TimeFrameIndex;
        auto GetTotalTimeFrameNumber() const->int32_t;
        auto GetReprocessingFlag() const->bool;
        auto GetDataTypeFlag() const->DataTypeFlag;
        auto GetDeconvolutionFlag() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
