#pragma once

#include <Job.h>
#include <map>

#include "IWriterPolicy.h"
#include "IFileWriter.h"
#include "IJobValidityChecker.h"
#include "IProcessor.h"
#include "JobQueue.h"
#include "ProcessingServerEntityExport.h"
#include "ProgressEntry.h"
#include "IProgressEntityOutputPort.h"

namespace processing_server::Entity {
    class WritingProgressUpdater;
    class ProcessProgressUpdater;

    class ProcessingServerEntity_API JobManager {
    public:
        typedef JobManager Self;
        typedef std::shared_ptr<Self> Pointer;

    public:
        JobManager();
        ~JobManager();

        auto SetWriterPolicy(const IWriterPolicy::Pointer& writerPolicy)->void;
        auto SetFileWriter(const IFileWriter::Pointer& fileWriter)->void;
        auto SetValidityChecker(const IJobValidityChecker::Pointer& validityChecker)->void;

        auto GetProgressEntry(const IProgressEntityOutputPort::Pointer& outputPort) const ->ProgressEntry::Pointer;

        auto AddJob(const Job::Pointer& job)->void;
        //auto Pause()->void;
        //auto Resume()->void;
        auto Cancel()->void;

    protected:
        auto WaitingQueue()->JobQueue&;
        auto ProcessingQueue()->JobQueue&;
        auto WritingQueue()->JobQueue&;
  

    private:
        auto CheckAndCreateProgress(const Job::Pointer& job)->void;
        auto CheckJobValidity(const Job::Pointer& job) const ->bool;
        auto UpdateCompleteJobProgress(const Job::Pointer& job)->void;

        auto GetAllPossibleProcessorAndStartProcessing()->void;
        auto StartProcessingInWaitingQueue()->bool;
        auto MoveJobFromWaitingQueueToProcessingQueue(const Job::Pointer& job)->void;

        auto GetMatchedIdleProcessor(const Job::Pointer& job)->IProcessor::Pointer;
        auto CreateUpdaterAndStartProcess(const Job::Pointer& job, const IProcessor::Pointer& processor)->void;

        auto UpdateProcessProgress(const Progress::Parameter& parameter)
            ->void;
        auto UpdateWritingProgress(const Progress::Parameter& parameter)
            ->void;

        auto GetAllCompleteProcessorsAndRequestToWriter()->void;
        static auto IsProcessorStatusComplete(const IProcessor::Pointer& processor)->bool;
        bool DoesWriterHaveMemorySpace(const IProcessor::Pointer& processor);
        auto CreateWritingData(const Job::ID& jobId, const IProcessor::Pointer& processor)->WritingData::Pointer;
        auto MoveJobFromProcessingQueueToWritingQueue(const Job::Pointer& job)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend ProcessProgressUpdater;
        friend WritingProgressUpdater;
    };
}