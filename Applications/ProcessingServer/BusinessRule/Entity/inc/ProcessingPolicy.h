#pragma once
#include "Job.h"
#include "IProcessor.h"
#include "ProcessingServerEntityExport.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API ProcessingPolicy {
    public:
        static auto GetRequestType(const Job::Pointer& job)->ProcessorRequestType;
    private:
        static auto IsDataTypeIsFl(const DataType& dataType)->bool;
        static auto GetProcessorType(const DataType& dataType)->ProcessorType;
        static auto GetProcessorPriority(const DataType& dataType)->ProcessorPriority;
    };
}
