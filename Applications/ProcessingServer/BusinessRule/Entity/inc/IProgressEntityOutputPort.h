#pragma once

#include "Progress.h"

#include "ProcessingServerEntityExport.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API IProgressEntityOutputPort {
    public:
        virtual ~IProgressEntityOutputPort() = default;
        typedef IProgressEntityOutputPort Self;
        typedef std::shared_ptr<Self> Pointer;
        virtual auto AddProgress(const QString& dataPath, const double& progress)->void = 0;
    };
}
