#pragma once
#include "JobManager.h"
#include "ProcessingServerEntityExport.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API JobManagerCaller {
    public:
        static auto GetJobManager()->JobManager::Pointer;
    };
}
