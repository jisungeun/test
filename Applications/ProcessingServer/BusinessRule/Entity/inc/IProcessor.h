#pragma once

#include <Job.h>

#include "ProcessedData.h"
#include "ProcessingServerEntityExport.h"
#include "IProgressUpdater.h"

namespace processing_server::Entity {
    BETTER_ENUM(ProcessorType, uint8_t, LOCAL_GPU, LOCAL_CPU, REMOTE_GPU, REMOTE_CPU, ANY)
    BETTER_ENUM(ProcessorPriority, uint8_t, MAIN, SUB, ANY)
    BETTER_ENUM(ProcessorStatus, uint8_t, IDLE, PROCESSING, COMPLETE)

    typedef std::tuple<ProcessorType, ProcessorPriority> ProcessorRequestType;

    class ProcessingServerEntity_API IProcessor {
    public:
        typedef IProcessor Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual ~IProcessor() = default;

        virtual auto SetType(const ProcessorType& type)->void = 0;
        virtual auto GetType() const ->ProcessorType = 0;

        virtual auto SetPriority(const ProcessorPriority& priority)->void = 0;
        virtual auto GetPriority() const ->ProcessorPriority = 0;

        virtual auto GetStatus() const->ProcessorStatus = 0;

        virtual auto IsWorking() const ->bool = 0;

        virtual auto SetJobPointer(const Job::Pointer job)->void = 0;
        virtual auto SetUpdater(const IProgressUpdater::Pointer updater)->void = 0;
        virtual auto StartProcess()->void = 0;

        virtual auto GetProcessedData()->ProcessedData::Pointer = 0;
        virtual auto GetProcessedDataMemoryInBytes()->size_t = 0;
    };
}
