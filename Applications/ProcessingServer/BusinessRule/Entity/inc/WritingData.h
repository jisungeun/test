#pragma once
#include "Job.h"
#include "ProcessedData.h"

#include "ProcessingServerEntityExport.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API WritingData {
    public:
        typedef WritingData Self;
        typedef std::shared_ptr<Self> Pointer;

        WritingData();
        WritingData(const Job::Pointer& job, const ProcessedData::Pointer processedData);
        ~WritingData();

        auto GetProcessedData() const ->ProcessedData::Pointer;
        auto GetJobPointer() const ->Job::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
