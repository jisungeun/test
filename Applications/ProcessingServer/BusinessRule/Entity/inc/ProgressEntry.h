#pragma once
#include <map>
#include <memory>
#include <QString>

#include "Progress.h"

#include "ProcessingServerEntityExport.h"

#include "Job.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API ProgressEntry {
    public:
        typedef ProgressEntry Self;
        typedef std::shared_ptr<Self> Pointer;

        ProgressEntry();
        ~ProgressEntry();

        auto CreateProgress(const QString& dataPath, const Job::DataTypeFlag& flag,
            const uint32_t& totalNumberOfTimeFrame)->void;
        auto RemoveProgress(const QString& dataPath)->void;

        auto ProgressExists(const QString& dataPath) const ->bool;
        auto UpdateProcessProgress(const Progress::Parameter& parameter)->void;
        auto UpdateWritingProgress(const Progress::Parameter& parameter)->void;
        auto GetProgressInDecimal(const QString& dataPath)->float;
        auto Clear()->void;

        auto GetProgressMap() const ->std::map<QString, Progress::Pointer>;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
