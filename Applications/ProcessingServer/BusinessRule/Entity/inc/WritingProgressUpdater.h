#pragma once

#include "JobManager.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API WritingProgressUpdater : public IProgressUpdater {
    public:
        typedef WritingProgressUpdater Self;
        typedef std::shared_ptr<Self> Pointer;

        explicit WritingProgressUpdater(JobManager* jobManager);
        ~WritingProgressUpdater();

        auto Update(const Progress::Parameter& parameter)-> void override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
