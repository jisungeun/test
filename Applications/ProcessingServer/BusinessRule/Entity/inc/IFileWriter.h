#pragma once

#include "ProcessingServerEntityExport.h"

#include "IProgressUpdater.h"
#include "WritingData.h"

#include <QString>

namespace processing_server::Entity{
    class ProcessingServerEntity_API IFileWriter {
    public:
        virtual ~IFileWriter() = default;
        typedef IFileWriter Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual auto GetUsedMemoryInBytes()->size_t = 0;
        virtual auto SetUpdater(const IProgressUpdater::Pointer& progressUpdater)->void = 0;
        virtual auto RequestToWrite(const WritingData::Pointer& writingData)->void = 0;
        virtual auto Clear()->void = 0;
        virtual auto SetSoftwareVersion(const QString& softwareVersion)->void = 0;
    };
}
