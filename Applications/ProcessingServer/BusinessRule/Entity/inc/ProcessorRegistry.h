#pragma once
#include <memory>

#include "IProcessor.h"
#include "ProcessingServerEntityExport.h"

namespace processing_server::Entity {
    class ProcessingServerEntity_API ProcessorRegistry {
    public:
        typedef ProcessorRegistry Self;
        typedef std::shared_ptr<Self> Pointer;

        ProcessorRegistry();
        ~ProcessorRegistry();

        auto Register(const IProcessor::Pointer& processor)->void;
        auto Unregister(const IProcessor::Pointer& processor)->void;

        auto IdleProcessorExist() const ->bool;
        auto CheckIfMatchedProcessorExists(const ProcessorRequestType& requestType) const ->bool;
        auto RequestProcessor(const ProcessorRequestType& requestType) const ->IProcessor::Pointer;

        private:
        struct Impl;
        std::unique_ptr<Impl> d;
        static auto MatchProcessorType(const ProcessorType& requestType, const ProcessorType& processorType) -> bool;
        static auto MatchProcessorPriority(const ProcessorPriority& requestPriority,
            const ProcessorPriority& processorPriority)->bool;
    };
}
