#include "WritingData.h"

namespace processing_server::Entity {
    struct WritingData::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        Job::Pointer job{};
        ProcessedData::Pointer processedData{};
    };

    WritingData::WritingData()
        : d(new Impl()) {
    }

    WritingData::WritingData(const Job::Pointer& job, const ProcessedData::Pointer processedData)
        : d(new Impl()) {
        d->job = job;
        d->processedData = processedData;
    }

    WritingData::~WritingData() = default;

    auto WritingData::GetProcessedData() const -> ProcessedData::Pointer {
        return d->processedData;
    }

    auto WritingData::GetJobPointer() const -> Job::Pointer {
        return d->job;
    }

}
