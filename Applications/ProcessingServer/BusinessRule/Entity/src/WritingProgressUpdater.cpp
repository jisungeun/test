#include "WritingProgressUpdater.h"

namespace processing_server::Entity {
    struct WritingProgressUpdater::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        JobManager* jobManager;
    };

    WritingProgressUpdater::WritingProgressUpdater(JobManager* jobManager)
        : d(new Impl()) {
        d->jobManager = jobManager;
    }

    WritingProgressUpdater::~WritingProgressUpdater() = default;

    auto WritingProgressUpdater::Update(const Progress::Parameter& parameter) -> void {
        d->jobManager->UpdateWritingProgress(parameter);
    }
}
