#include "JobManagerCaller.h"

namespace processing_server::Entity {
    auto JobManagerCaller::GetJobManager() -> JobManager::Pointer {
        static auto jobManager = std::make_shared<JobManager>();
        return jobManager;
    }
}
