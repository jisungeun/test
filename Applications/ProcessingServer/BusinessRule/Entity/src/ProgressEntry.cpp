#include "ProgressEntry.h"

namespace processing_server::Entity {
    struct ProgressEntry::Impl {
        Impl() = default;
        ~Impl() = default;

        std::map<QString, Progress::Pointer> progressMap;
    };

    ProgressEntry::ProgressEntry()
        : d(new Impl()){
    }

    ProgressEntry::~ProgressEntry() = default;

    auto ProgressEntry::CreateProgress(const QString& dataPath, const Job::DataTypeFlag& flag,
        const uint32_t& totalNumberOfTimeFrame) -> void {
        d->progressMap[dataPath] = std::make_shared<Progress>(flag, totalNumberOfTimeFrame);
    }

    auto ProgressEntry::RemoveProgress(const QString& dataPath) -> void {
        d->progressMap.erase(dataPath);
    }

    auto ProgressEntry::ProgressExists(const QString& dataPath) const -> bool {
        return d->progressMap.count(dataPath) != 0;
    }

    auto ProgressEntry::UpdateProcessProgress(const Progress::Parameter& parameter) -> void {
        auto progress = d->progressMap[parameter.job->GetDataPath()];
        progress->ProcessUpdate(parameter);
    }

    auto ProgressEntry::UpdateWritingProgress(const Progress::Parameter& parameter) -> void {
        auto progress = d->progressMap[parameter.job->GetDataPath()];
        progress->WritingUpdate(parameter);
    }

    auto ProgressEntry::GetProgressInDecimal(const QString& dataPath)-> float {
        const auto progress = d->progressMap[dataPath];
        return progress->GetProgressInDecimal();
    }

    auto ProgressEntry::Clear() -> void {
        d->progressMap.clear();
    }

    auto ProgressEntry::GetProgressMap() const -> std::map<QString, Progress::Pointer> {
        return d->progressMap;
    }
}
