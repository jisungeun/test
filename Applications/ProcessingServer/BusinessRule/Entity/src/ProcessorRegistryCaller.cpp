#include "ProcessorRegistryCaller.h"

namespace processing_server::Entity {
    auto ProcessorRegistryCaller::GetProcessorRegistry() -> ProcessorRegistry::Pointer {
        static auto processorRegistry = std::make_shared<ProcessorRegistry>();
        return processorRegistry;
    }

}
