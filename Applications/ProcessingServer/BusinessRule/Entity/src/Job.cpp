#include "Job.h"

namespace processing_server::Entity {
    struct Job::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        Info info{};
    };

    Job::Job()
        : d(new Impl()) {
    };

    Job::Job(const Job& other)
        : d(new Impl(*other.d)) {
    }

    Job::Job(const Info& jobInfo)
        : Job() {
        d->info = jobInfo;
    }

    Job::~Job() = default;

    auto Job::GetUniqueID() const ->ID {
        return d->info.uniqueID;
    }

    auto Job::GetDataPath() const ->QString {
        return d->info.dataPath;
    }

    auto Job::GetOutputPath() const ->QString {
        return d->info.outputPath;
    }

    auto Job::GetDataType() const ->DataType {
        return d->info.dataType;
    }

    auto Job::GetTimeFrameIndex() const ->TimeFrameIndex {
        return d->info.index;
    }

    auto Job::GetTotalTimeFrameNumber() const -> int32_t {
        return d->info.totalFrameNumber;
    }

    auto Job::GetReprocessingFlag() const -> bool {
        return d->info.reprocessingFlag;
    }

    auto Job::GetDataTypeFlag() const -> DataTypeFlag {
        return d->info.dataTypeFlag;
    }

    auto Job::GetDeconvolutionFlag() const -> bool {
        return d->info.deconvolutionFlag;
    }
}
