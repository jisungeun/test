#include "ProcessorRegistry.h"

#include <deque>
#include <map>

#include "Job.h"

namespace processing_server::Entity {
    struct ProcessorRegistry::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        std::deque<IProcessor::Pointer> processors;
    };

    ProcessorRegistry::ProcessorRegistry()
        : d(new Impl()) {
    }

    ProcessorRegistry::~ProcessorRegistry() = default;

    auto ProcessorRegistry::Register(const IProcessor::Pointer& processor) -> void {
        d->processors.push_back(processor);
    }

    auto ProcessorRegistry::Unregister(const IProcessor::Pointer& processor) -> void {
        for(auto iter = d->processors.begin();iter!=d->processors.end();++iter) {
            if( (*iter).get() == processor.get()) {
                d->processors.erase(iter);
                break;
            }
        }
    }

    auto ProcessorRegistry::IdleProcessorExist() const -> bool {
        auto idleProcessorExist{ false };
        for (auto& processor : d->processors) {
            idleProcessorExist = (idleProcessorExist || !processor->IsWorking());
        }
        return idleProcessorExist;
    }


    auto ProcessorRegistry::CheckIfMatchedProcessorExists(const ProcessorRequestType& requestType) const -> bool {
        auto enableProcessorExists = false;

        const auto requestProcessorType = std::get<0>(requestType);
        const auto requestProcessorPriority = std::get<1>(requestType);

        for (const auto& processor : d->processors) {
            const auto typeMatched =
                MatchProcessorType(requestProcessorType, processor->GetType());
            const auto priorityMatched =
                MatchProcessorPriority(requestProcessorPriority, processor->GetPriority());
            enableProcessorExists = typeMatched && priorityMatched;

            if(enableProcessorExists) {
                break;
            }
        }

        return enableProcessorExists;
    }

    auto ProcessorRegistry::RequestProcessor(const ProcessorRequestType& requestType) const -> IProcessor::Pointer {
        IProcessor::Pointer resultProcessor{nullptr};

        const auto requestProcessorType = std::get<0>(requestType);
        const auto requestProcessorPriority = std::get<1>(requestType);

        for (const auto& processor : d->processors) {
            const auto typeMatched =
                MatchProcessorType(requestProcessorType, processor->GetType());
            const auto priorityMatched =
                MatchProcessorPriority(requestProcessorPriority, processor->GetPriority());
            const auto idle = !processor->IsWorking();
            if (typeMatched && priorityMatched && idle) {
                resultProcessor = processor;
                break;
            }
        }

        return resultProcessor;
    }

    auto ProcessorRegistry::MatchProcessorType(const ProcessorType& requestType, const ProcessorType& processorType)
        -> bool {
        const ProcessorType anyType = ProcessorType::ANY;

        const auto processorTypeIsAny = (processorType == anyType);
        const auto requestTypeIsAny = (requestType == anyType);
        const auto typeMatched = (requestType == processorType);

        return processorTypeIsAny || requestTypeIsAny || typeMatched;
    }

    auto ProcessorRegistry::MatchProcessorPriority(const ProcessorPriority& requestPriority,
        const ProcessorPriority& processorPriority) -> bool {
        const ProcessorPriority anyPriority = ProcessorPriority::ANY;

        const auto processorPriorityIsAny = (processorPriority == anyPriority);
        const auto requestPriorityIsAny = (requestPriority == anyPriority);
        const auto priorityMatched = requestPriority == processorPriority;

        return processorPriorityIsAny|| requestPriorityIsAny || priorityMatched;
    }
}
