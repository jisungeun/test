#include "ProcessedData.h"

namespace processing_server::Entity {
    struct ProcessedData::Resolution::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        double resolutionX{ 0.f };
        double resolutionY{ 0.f };
        double resolutionZ{ 0.f };
    };

    struct ProcessedData::Dimension::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        size_t dimensionX{ 0 };
        size_t dimensionY{ 0 };
        size_t dimensionZ{ 0 };
    };

    struct ProcessedData::MinMax::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        double minValue{ 0 };
        double maxValue{ 0 };
    };

    struct ProcessedData::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        std::any data3d{};
        std::any dataMip{};

        MinMax minMax3d{};
        MinMax minMaxMip{};

        Resolution resolution{};
        Dimension dimension{};
        DataType dataType{ DataType::HT };
    };

    ProcessedData::Resolution::Resolution()
        : d(new Impl()) {
    }

    ProcessedData::Resolution::Resolution(const Resolution& other)
        : d(new Impl(*other.d)) {
    }

    ProcessedData::Resolution::Resolution(const double& x, const double& y, const double& z)
        : Resolution() {
        SetResolution(x, y, z);
    }

    ProcessedData::Resolution::~Resolution() = default;

    auto ProcessedData::Resolution::operator=(const Resolution& other) -> ProcessedData::Resolution& {
        *d = *other.d;
        return *this;
    }

    auto ProcessedData::Resolution::operator==(const Resolution& other) const -> bool {
        const auto xValueIsSame = (d->resolutionX == other.d->resolutionX);
        const auto yValueIsSame = (d->resolutionY == other.d->resolutionY);
        const auto zValueIsSame = (d->resolutionZ == other.d->resolutionZ);
        return xValueIsSame && yValueIsSame && zValueIsSame;
    }

    auto ProcessedData::Resolution::operator!=(const Resolution& other) const -> bool {
        return !(*this == other);
    }

    auto ProcessedData::Resolution::SetResolution(const double& x, const double& y, const double& z)->void {
        d->resolutionX = x;
        d->resolutionY = y;
        d->resolutionZ = z;
    }

    auto ProcessedData::Resolution::GetResolutionX() const -> double {
        return d->resolutionX;
    }

    auto ProcessedData::Resolution::GetResolutionY() const -> double {
        return d->resolutionY;
    }

    auto ProcessedData::Resolution::GetResolutionZ() const -> double {
        return d->resolutionZ;
    }

    ProcessedData::Dimension::Dimension()
        : d(new Impl()) {
    }

    ProcessedData::Dimension::Dimension(const Dimension& other)
        : d(new Impl(*other.d)) {
    }

    ProcessedData::Dimension::Dimension(const size_t& x, const size_t& y, const size_t& z)
        : Dimension() {
        SetDimension(x, y, z);
    }

    auto ProcessedData::Dimension::SetDimension(const size_t& x, const size_t& y, const size_t& z)->void {
        d->dimensionX = x;
        d->dimensionY = y;
        d->dimensionZ = z;
    }

    auto ProcessedData::Dimension::GetDimensionX() const -> size_t {
        return d->dimensionX;
    }

    auto ProcessedData::Dimension::GetDimensionY() const -> size_t {
        return d->dimensionY;
    }

    auto ProcessedData::Dimension::GetDimensionZ() const -> size_t {
        return d->dimensionZ;
    }

    ProcessedData::Dimension::~Dimension() = default;

    auto ProcessedData::Dimension::operator=(const Dimension& other) -> ProcessedData::Dimension& {
        *d = *other.d;
        return *this;
    }

    auto ProcessedData::Dimension::operator==(const Dimension& other) const -> bool {
        const auto xValueIsSame = (d->dimensionX == other.d->dimensionX);
        const auto yValueIsSame = (d->dimensionY == other.d->dimensionY);
        const auto zValueIsSame = (d->dimensionZ == other.d->dimensionZ);
        return xValueIsSame && yValueIsSame && zValueIsSame;
    }

    auto ProcessedData::Dimension::operator!=(const Dimension& other) const -> bool {
        return !(*this == other);
    }

    ProcessedData::MinMax::MinMax()
        : d (new Impl()) {
    }

    ProcessedData::MinMax::MinMax(const MinMax& other)
        : d(new Impl(*other.d)) {
    }

    ProcessedData::MinMax::MinMax(const double& minValue, const double& maxValue)
        : MinMax() {
        d->minValue = minValue;
        d->maxValue = maxValue;
    }

    ProcessedData::MinMax::~MinMax() = default;

    auto ProcessedData::MinMax::operator=(const MinMax& other) -> MinMax& {
        *d = *other.d;
        return *this;
    }

    auto ProcessedData::MinMax::operator==(const MinMax& other) const -> bool {
        const auto minValueIsSame = (d->minValue == other.d->minValue);
        const auto maxValueIsSame = (d->maxValue == other.d->maxValue);
        return maxValueIsSame && minValueIsSame;
    }

    auto ProcessedData::MinMax::operator!=(const MinMax& other) const -> bool {
        return !(*this == other);
    }

    auto ProcessedData::MinMax::SetMinMax(const double& minValue, const double& maxValue) -> void {
        d->minValue = minValue;
        d->maxValue = maxValue;
    }

    auto ProcessedData::MinMax::GetMinValue() const -> double {
        return d->minValue;
    }

    auto ProcessedData::MinMax::GetMaxValue() const -> double {
        return d->maxValue;
    }

    ProcessedData::ProcessedData(const DataType& dataType)
        : d(std::make_unique<Impl>()) {
        d->dataType = dataType;
    }

    ProcessedData::~ProcessedData() = default;

    auto ProcessedData::SetResolution(const Resolution& resolution) -> void {
        d->resolution = resolution;
    }

    auto ProcessedData::SetDimension(const Dimension& dimension) -> void {
        d->dimension = dimension;
    }

    auto ProcessedData::SetMinMax3d(const MinMax& minMax3d) -> void {
        d->minMax3d = minMax3d;
    }

    auto ProcessedData::SetMinMaxMip(const MinMax& minMaxMip) -> void {
        d->minMaxMip = minMaxMip;
    }

    auto ProcessedData::GetMinMax3d() const -> MinMax {
        return d->minMax3d;
    }

    auto ProcessedData::GetMinMaxMip() const -> MinMax {
        return d->minMaxMip;
    }

    auto ProcessedData::GetResolution() const -> Resolution {
        return d->resolution;
    }

    auto ProcessedData::GetDimension() const -> Dimension {
        return d->dimension;
    }

    auto ProcessedData::SetData3d(const std::any& data3d) -> void {
        d->data3d = data3d;
    }

    auto ProcessedData::SetDataMip(const std::any& dataMip) -> void {
        d->dataMip = dataMip;
    }

    auto ProcessedData::GetData3d() -> std::any {
        return d->data3d;
    }

    auto ProcessedData::GetDataMip() -> std::any {
        return d->dataMip;
    }

    auto ProcessedData::GetDataType() const -> DataType {
        return d->dataType;
    }
}
