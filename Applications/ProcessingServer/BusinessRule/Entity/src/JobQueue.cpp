#include <deque>
#include <map>

#include "JobQueue.h"

namespace processing_server::Entity {
    struct JobQueue::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        std::map<Job::ID, Job::Pointer> jobPointerByIndexMap;
        std::deque<Job::ID> jobIdDeque;
    };

    JobQueue::JobQueueException::JobQueueException(const std::exception& e) {
        exceptionContents = e.what();
    }

    JobQueue::JobQueueException::JobQueueException(const std::string& contents) {
        exceptionContents = contents;
    }

    auto JobQueue::JobQueueException::What() const -> std::string {
        return exceptionContents;
    }

    JobQueue::JobQueue()
        : d(new Impl()){
    }
    JobQueue::~JobQueue() = default;

    auto JobQueue::Add(const Job::Pointer& job)->void {
        try {
            const auto jobId = job->GetUniqueID();
            d->jobPointerByIndexMap[jobId] = job;
            d->jobIdDeque.push_back(jobId);
        } catch (const std::exception& e) {
            throw JobQueueException(e);
        }
    }

    auto JobQueue::Remove(const Job::ID& id)->void {
        try {
            const auto matchedIndex = FindMatchedIndexInDeque(id);
            const auto jobId = d->jobIdDeque.at(matchedIndex);
            d->jobPointerByIndexMap.erase(jobId);
            EraseJobIdInDeque(id);
        } catch (const std::exception& e) {
            throw JobQueueException(e);
        }
    }

    auto JobQueue::Clear()->void {
        try {
            d->jobPointerByIndexMap.clear();
            d->jobIdDeque.clear();
        } catch (const std::exception& e) {
            throw JobQueueException(e);
        }
    }

    auto JobQueue::GetNextJob() const ->Job::Pointer {
        Job::Pointer nextJobPointer;
        try {
            if(d->jobIdDeque.empty()) {
                const auto exceptionContents = "GetNextJob() method is called from empty jobIdDeque.";
                throw JobQueueException(exceptionContents);
            }
            const auto nextJobQueueIndex = d->jobIdDeque.front();
            nextJobPointer = d->jobPointerByIndexMap[nextJobQueueIndex];
        } catch (const std::exception& e) {
            throw JobQueueException(e);
        }

        return nextJobPointer;
    }

    auto JobQueue::GetJobByID(const Job::ID& id) const -> Job::Pointer {
        try {
            return d->jobPointerByIndexMap[id];
        }
        catch (const std::exception & e) {
            throw JobQueueException(e);
        }
    }

    auto JobQueue::GetJobByIndex(const uint32_t& index) const -> Job::Pointer {
        try {
            const auto jobId = d->jobIdDeque.at(index);
            return d->jobPointerByIndexMap[jobId];
        }
        catch (const std::exception& e) {
            throw JobQueueException(e);
        }
    }

    auto JobQueue::MoveFront(const Job::ID& id)-> void {
        try {
            const auto matchedIndex = FindMatchedIndexInDeque(id);
            EraseJobIdInDeque(matchedIndex);
            d->jobIdDeque.push_front(id);
        } catch (const std::exception& e) {
            throw JobQueueException(e);
        }
    }

    auto JobQueue::MoveBack(const Job::ID& id)-> void {
        try {
            const auto matchedIndex = FindMatchedIndexInDeque(id);
            EraseJobIdInDeque(id);
            d->jobIdDeque.push_back(id);
        } catch (const std::exception& e) {
            throw JobQueueException(e);
        }
    }

    auto JobQueue::Empty() const ->bool {
        bool jobIdDequeIsEmpty, mapIsEmpty;
        try {
            jobIdDequeIsEmpty = d->jobIdDeque.empty();
            mapIsEmpty = d->jobPointerByIndexMap.empty();
        } catch (const std::exception& e) {
            throw JobQueueException(e);
        }
        return jobIdDequeIsEmpty && mapIsEmpty;
    }

    auto JobQueue::Size() const ->size_t {
        const auto size = size_t{};
        try {
            return static_cast<size_t>(d->jobIdDeque.size());
        }
        catch (const std::exception& e) {
            throw JobQueueException(e);
        }
    }

    auto JobQueue::EraseJobIdInDeque(const Job::ID& id) const -> void {
        for (auto iter = d->jobIdDeque.begin(); iter != d->jobIdDeque.end(); ++iter) {
            if(*iter == id) {
                d->jobIdDeque.erase(iter);
                break;
            }
        }
    }

    auto JobQueue::FindMatchedIndexInDeque(const Job::ID& id) const->int32_t {
        int32_t matchedIndex = 0;
        bool matchedIndexFound = false;
        for (auto iter = d->jobIdDeque.begin(); iter != d->jobIdDeque.end(); ++iter) {
            if (*iter == id) {
                matchedIndexFound = true;
                break;
            } else {
                ++matchedIndex;
            }
        }
        CheckMatchedAndThrowExceptionWhenNotMatched(matchedIndexFound, id);
        return matchedIndex;
    }

    auto JobQueue::CheckMatchedAndThrowExceptionWhenNotMatched(const bool& matchedFound, const Job::ID& id) const
        ->void {
        const auto notMatched = (matchedFound == false);
        if (notMatched) {
            const auto exceptionContents = "There is no matched Job ID for jobIdDeque: "
                + std::string("Job::ID -> (") + std::string(std::to_string(id));
            throw JobQueueException(exceptionContents);
        }
    }

}
