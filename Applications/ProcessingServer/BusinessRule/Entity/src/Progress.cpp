#include "Progress.h"

#include <map>
#include <vector>

namespace processing_server::Entity {
    struct Progress::Impl {
        Impl() = default;
        ~Impl() = default;

        uint32_t totalNumberOfTimeFrame{ 0 };
        uint32_t dataTypeNumber{ 0 };
        Job::DataTypeFlag dataTypeFlag{};

        std::map<DataType, std::vector<double>> instantProcessingProgressMap;
        std::map<DataType, std::vector<double>> instantWritingProgressMap;

        double completeProcessingProgress{ 0 };
        double completeWritingProgress{ 0 };
    };

    Progress::ProgressException::ProgressException(const std::string& contents) {
        exceptionContents = contents;
    }
    Progress::ProgressException::~ProgressException() = default;

    auto Progress::ProgressException::What() -> std::string {
        return exceptionContents;
    }

    Progress::Progress(const Job::DataTypeFlag& flag, const uint32_t& totalFrameNumber)
        : d(new Impl()){
        d->totalNumberOfTimeFrame = totalFrameNumber;
        d->dataTypeFlag = flag;
        CreateInstantProgressMap();
    }


    Progress::~Progress() = default;

    auto Progress::ProcessUpdate(const Parameter& parameter)->void {
        auto& progressValuesVector = d->instantProcessingProgressMap[parameter.job->GetDataType()];
        auto& recordedProgressValue = progressValuesVector.at(parameter.job->GetTimeFrameIndex());
        const auto incrementedProgress = (parameter.completeProgressValue - recordedProgressValue);
        BoundedCheck(parameter.completeProgressValue);
        BoundedCheck(incrementedProgress);

        recordedProgressValue = parameter.completeProgressValue;

        d->completeProcessingProgress += incrementedProgress;
    }

    auto Progress::WritingUpdate(const Parameter& parameter)->void {
        auto& progressValuesVector = d->instantWritingProgressMap[parameter.job->GetDataType()];
        auto& recordedProgressValue = progressValuesVector.at(parameter.job->GetTimeFrameIndex());
        const auto incrementedProgress = (parameter.completeProgressValue - recordedProgressValue);
        BoundedCheck(parameter.completeProgressValue);
        BoundedCheck(incrementedProgress);

        recordedProgressValue = parameter.completeProgressValue;

        d->completeWritingProgress += incrementedProgress;
    }

    auto Progress::GetProgressInDecimal() const -> double {
        const auto totalNumber = 2 * d->totalNumberOfTimeFrame * d->dataTypeNumber;
        const auto totalCompleted = d->completeProcessingProgress + d->completeWritingProgress;
        const auto progressInDecimal = totalCompleted / static_cast<double>(totalNumber);

        return progressInDecimal;
    }

    auto Progress::CreateInstantProgressMap() -> void {
        for (auto i = 0; i < DataType::_size(); i++) {
            const auto targetDataType = DataType::_from_index(i);
            const auto targetDataTypeExistence = d->dataTypeFlag.existence[i];

            if(targetDataTypeExistence) {
                const std::vector<double> progressValues(d->totalNumberOfTimeFrame, 0);
                d->instantProcessingProgressMap[targetDataType] = progressValues;
                d->instantWritingProgressMap[targetDataType] = progressValues;
                d->dataTypeNumber++;
            }
        }
    }

    auto Progress::BoundedCheck(const double& progress) const -> void {
        if (OutBounded(progress)) {
            const auto contents = "OutBounded(0<=value>=1) : progress value is " + std::to_string(progress);
            throw ProgressException(contents);
        }
    }

    auto Progress::OutBounded(const double& progress) ->bool {
        const auto lowBound = 0;
        const auto highBound = 1;

        const auto lowerThanLowBound = progress < lowBound;
        const auto higherThanHighBound = progress > highBound;

        return lowerThanLowBound || higherThanHighBound;
    }
}
