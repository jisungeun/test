#include "ProcessingPolicy.h"

namespace processing_server::Entity {
    auto ProcessingPolicy::GetProcessorType(const DataType& dataType) -> ProcessorType {
        return ProcessorType::LOCAL_GPU;
    }

    auto ProcessingPolicy::GetProcessorPriority(const DataType& dataType) -> ProcessorPriority {
        ProcessorPriority resultProcessorPriority{ ProcessorPriority::MAIN };

        if(IsDataTypeIsFl(dataType)) {
            resultProcessorPriority = ProcessorPriority::MAIN;
        } else {
            resultProcessorPriority = ProcessorPriority::ANY;
        }

        return resultProcessorPriority;
    }

    auto ProcessingPolicy::GetRequestType(const Job::Pointer& job) ->ProcessorRequestType {
        const auto dataType = job->GetDataType();
        auto processorType = GetProcessorType(dataType);
        auto processorPriority = GetProcessorPriority(dataType);

        return std::make_tuple(processorType, processorPriority);
    }

    auto ProcessingPolicy::IsDataTypeIsFl(const DataType& dataType) -> bool {
        const auto dataTypeIsFlBlue = (dataType._value == DataType::FLBLUE);
        const auto dataTypeIsFlGreen = (dataType._value == DataType::FLGREEN);
        const auto dataTypeIsFlRed = (dataType._value == DataType::FLRED);

        return dataTypeIsFlBlue || dataTypeIsFlGreen || dataTypeIsFlRed;
    }

}
