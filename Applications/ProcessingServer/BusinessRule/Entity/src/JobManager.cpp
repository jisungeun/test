#include "JobManager.h"

#include <map>

#include "ProcessorRegistry.h"
#include "ProcessingPolicy.h"
#include "ProcessorRegistryCaller.h"

#include "ProcessProgressUpdater.h"
#include "WritingProgressUpdater.h"

#include "QMutex"

namespace processing_server::Entity {
    struct JobManager::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        JobQueue waitingQueue{};
        JobQueue processingQueue{};
        JobQueue writingQueue{};

        ProgressEntry::Pointer progressEntry = std::make_shared<ProgressEntry>();
        IJobValidityChecker::Pointer jobValidityChecker{};
        ProcessorRegistry::Pointer processorRegistry = ProcessorRegistryCaller::GetProcessorRegistry();

        IWriterPolicy::Pointer writerPolicy{};
        IFileWriter::Pointer fileWriter{};

        std::map<Job::ID, IProcessor::Pointer> workingProcessorsMap;
        QMutex mutex;
    };

    JobManager::JobManager()
        : d(new Impl()) {
    }

    JobManager::~JobManager() = default;

    auto JobManager::SetWriterPolicy(const IWriterPolicy::Pointer& writerPolicy)->void {
        d->writerPolicy = writerPolicy;
    }

    auto JobManager::SetFileWriter(const IFileWriter::Pointer& fileWriter)->void{
        d->fileWriter = fileWriter;

        const auto writingProgressUpdater = std::make_shared<WritingProgressUpdater>(this);
        d->fileWriter->SetUpdater(writingProgressUpdater);
    }

    auto JobManager::SetValidityChecker(const IJobValidityChecker::Pointer& validityChecker) -> void {
        d->jobValidityChecker = validityChecker;
    }

    auto JobManager::GetProgressEntry(const IProgressEntityOutputPort::Pointer& outputPort) const
        ->ProgressEntry::Pointer {
        QMutexLocker locker(&d->mutex);

        const auto progressMap = d->progressEntry->GetProgressMap();

        for (const auto& progressIterator : progressMap) {
            const auto dataPath = progressIterator.first;
            const auto progress = progressIterator.second->GetProgressInDecimal();
            outputPort->AddProgress(dataPath, progress);

            if(progress == 1) {
                d->progressEntry->RemoveProgress(dataPath);
            }
        }

        return d->progressEntry;
    }

    auto JobManager::AddJob(const Job::Pointer& job) -> void {
        QMutexLocker locker(&d->mutex);

        CheckAndCreateProgress(job);

        if(CheckJobValidity(job)) {
            WaitingQueue().Add(job);
            GetAllPossibleProcessorAndStartProcessing();
        } else {
            UpdateCompleteJobProgress(job);
        }
    }

    auto JobManager::CheckAndCreateProgress(const Job::Pointer& job) -> void {
        const auto progressDoesNotExist = !d->progressEntry->ProgressExists(job->GetDataPath());
        const auto dataPath = job->GetDataPath();
        const auto dataTypeFlag = job->GetDataTypeFlag();
        const auto totalTimeFrameNumber = job->GetTotalTimeFrameNumber();
        if (progressDoesNotExist) {
            d->progressEntry->CreateProgress(dataPath, dataTypeFlag, totalTimeFrameNumber);
        } else {
            const auto processingCompleted = (d->progressEntry->GetProgressInDecimal(dataPath) >= 1);
            if (processingCompleted) {
                d->progressEntry->RemoveProgress(dataPath);
                d->progressEntry->CreateProgress(dataPath, dataTypeFlag, totalTimeFrameNumber);
            }
        }
    }

    auto JobManager::CheckJobValidity(const Job::Pointer& job) const -> bool {
        const auto jobIsValid = d->jobValidityChecker->Check(job);
        return jobIsValid;
    }

    auto JobManager::UpdateCompleteJobProgress(const Job::Pointer& job) -> void {
        Progress::Parameter parameter;
        parameter.job = job;
        parameter.completeProgressValue = 1;
        d->progressEntry->UpdateProcessProgress(parameter);
        d->progressEntry->UpdateWritingProgress(parameter);
    }

    auto JobManager::GetAllPossibleProcessorAndStartProcessing() -> void {
        while (d->processorRegistry->IdleProcessorExist() && !WaitingQueue().Empty()) {
            if (StartProcessingInWaitingQueue()) {
                break;
            }
        }
    }

    auto JobManager::StartProcessingInWaitingQueue() -> bool {
        auto allJobsAreTried = true;

        auto queueSize = WaitingQueue().Size();
        auto queueIndex = 0;

        while (queueIndex < queueSize) {
            const auto job = WaitingQueue().GetJobByIndex(queueIndex);

            auto processor = GetMatchedIdleProcessor(job);

            if (processor == nullptr) {
                ++queueIndex;
                continue;
            }

            d->workingProcessorsMap[job->GetUniqueID()] = processor;

            MoveJobFromWaitingQueueToProcessingQueue(job);
            CreateUpdaterAndStartProcess(job, processor);
            allJobsAreTried = false;

            queueIndex = 0;

            queueSize = WaitingQueue().Size();
        }

        return allJobsAreTried;
    }

    auto JobManager::MoveJobFromWaitingQueueToProcessingQueue(const Job::Pointer& job) -> void {
        WaitingQueue().Remove(job->GetUniqueID());
        ProcessingQueue().Add(job);
    }

    auto JobManager::GetMatchedIdleProcessor(const Job::Pointer& job) -> IProcessor::Pointer {
        const auto requestType = ProcessingPolicy::GetRequestType(job);
        const auto matchedProcessor = d->processorRegistry->RequestProcessor(requestType);
        return matchedProcessor;
    }

    auto JobManager::CreateUpdaterAndStartProcess(const Job::Pointer& job, const IProcessor::Pointer& processor)
        -> void {
        const auto processProgressUpdater = std::make_shared<ProcessProgressUpdater>(this);
        processor->SetJobPointer(job);
        processor->SetUpdater(processProgressUpdater);
        processor->StartProcess();
    }

    //auto JobManager::Pause() -> void {

    //}

    //auto JobManager::Resume() -> void {

    //}

    auto JobManager::Cancel() -> void {
        WaitingQueue().Clear();
        ProcessingQueue().Clear();
        WritingQueue().Clear();

        d->progressEntry->Clear();
        d->fileWriter->Clear();

        d->workingProcessorsMap.clear();
    }

    auto JobManager::WaitingQueue() -> JobQueue& {
        return d->waitingQueue;
    }

    auto JobManager::ProcessingQueue() -> JobQueue& {
        return d->processingQueue;
    }

    auto JobManager::WritingQueue() -> JobQueue& {
        return d->writingQueue;
    }

    auto JobManager::UpdateProcessProgress(const Progress::Parameter& parameter) -> void {
        QMutexLocker locker(&d->mutex);
        d->progressEntry->UpdateProcessProgress(parameter);
        const auto processComplete = (parameter.completeProgressValue == 1);

        if (processComplete) {
            GetAllCompleteProcessorsAndRequestToWriter();
            GetAllPossibleProcessorAndStartProcessing();
        }
    }

    auto JobManager::UpdateWritingProgress(const Progress::Parameter& parameter) -> void {
        QMutexLocker locker(&d->mutex);
        d->progressEntry->UpdateWritingProgress(parameter);
        const auto writingComplete = (parameter.completeProgressValue == 1);
        if (writingComplete) {
            WritingQueue().Remove(parameter.job->GetUniqueID());
            GetAllCompleteProcessorsAndRequestToWriter();
        }
    }

    auto JobManager::GetAllCompleteProcessorsAndRequestToWriter() -> void {
        for (auto iter = d->workingProcessorsMap.begin();
            iter != d->workingProcessorsMap.end(); ++iter) {
            const auto jobId = iter->first;
            const auto processor = iter->second;
            if (IsProcessorStatusComplete(iter->second)) {

                if (DoesWriterHaveMemorySpace(processor)) {
                    MoveJobFromProcessingQueueToWritingQueue(ProcessingQueue().GetJobByID(jobId));
                    d->workingProcessorsMap.erase(jobId);

                    const auto writingData = CreateWritingData(jobId, processor);
                    d->fileWriter->RequestToWrite(writingData);
                }
            }

            if(d->workingProcessorsMap.empty()) {
                break;
            }
        }
    }

    auto JobManager::IsProcessorStatusComplete(const IProcessor::Pointer& processor)->bool{
        return processor->GetStatus()._value == ProcessorStatus::COMPLETE;
    }

    bool JobManager::DoesWriterHaveMemorySpace(const IProcessor::Pointer& processor) {
        const auto usedMemoryInBytes = d->fileWriter->GetUsedMemoryInBytes();
        const auto requiredMemoryInBytes = processor->GetProcessedDataMemoryInBytes();

        const auto writerHasMemorySpace = d->writerPolicy->Accept(usedMemoryInBytes, requiredMemoryInBytes);

        return writerHasMemorySpace;
    }

    auto JobManager::CreateWritingData(const Job::ID& jobId,
        const IProcessor::Pointer& processor)-> WritingData::Pointer {
        const auto job = WritingQueue().GetJobByID(jobId);
        const auto processedData = processor->GetProcessedData();
        auto writingData = std::make_shared<WritingData>(job, processedData);
        return writingData;
    }

    auto JobManager::MoveJobFromProcessingQueueToWritingQueue(const Job::Pointer& job) -> void {
        ProcessingQueue().Remove(job->GetUniqueID());
        WritingQueue().Add(job);
    }
}
