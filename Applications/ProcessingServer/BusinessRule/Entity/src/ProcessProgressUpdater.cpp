#include "ProcessProgressUpdater.h"

namespace processing_server::Entity {
    struct ProcessProgressUpdater::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        JobManager* jobManager;
    };

    ProcessProgressUpdater::ProcessProgressUpdater(JobManager* jobManager)
        :d(new Impl()) {
        d->jobManager = jobManager;
    }

    ProcessProgressUpdater::~ProcessProgressUpdater() = default;

    auto ProcessProgressUpdater::Update(const Progress::Parameter& parameter) -> void {
        d->jobManager->UpdateProcessProgress(parameter);
    }
}
