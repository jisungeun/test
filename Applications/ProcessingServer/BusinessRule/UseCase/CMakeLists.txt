project(ProcessingServerUseCase)

#Header files for external use
set(INTERFACE_HEADERS
    inc/JobAdder.h
    inc/InputJobContents.h
    inc/ProgressReporter.h
    inc/IProgressOutputPort.h
    inc/ProgressData.h
    inc/EntityProgressData.h
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES
    src/JobAdder.cpp
    src/InputJobContents.cpp
    src/ProgressReporter.cpp
    src/IProgressOutputPort.cpp
    src/ProgressData.cpp
    src/EntityProgressData.cpp
)

add_library(${PROJECT_NAME} SHARED
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

add_library(ProcessingServer::BusinessRule::UseCase ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<INSTALL_INTERFACE:inc>
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_libraries(${PROJECT_NAME}
	PUBLIC
		ProcessingServer::BusinessRule::Entity
        Qt5::Core
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/ProcessingServer/BusinessRule")

generate_export_header(${PROJECT_NAME}
    EXPORT_MACRO_NAME ${PROJECT_NAME}_API
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

# 'make install' to the correct locations
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR}
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR})
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

# This makes the project importable from the install directory
# Put config file in per-project dir (name MUST match), can also
# just go into 'cmake'.
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake)

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)


