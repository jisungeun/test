#pragma once
#include <memory>

#include <QString>

#include "ProcessingServerUseCaseExport.h"
#include "InputJobContents.h"
#include "Job.h"

namespace processing_server::UseCase {
    class ProcessingServerUseCase_API JobAdder {
    public:
        explicit JobAdder();
        ~JobAdder();

        auto Add(const InputJobContents& inputJobContents)->void;
    private:
        auto GenerateJobInfo(const InputJobContents& inputJobContents)->Entity::Job::Info;
    };
}
