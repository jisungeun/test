#pragma once

#include "ProcessingServerUseCaseExport.h"

#include "IProgressEntityOutputPort.h"
#include "ProgressData.h"

namespace processing_server::UseCase {
    class ProcessingServerUseCase_API EntityProgressData : public Entity::IProgressEntityOutputPort {
    public:
        EntityProgressData();
        ~EntityProgressData();

        auto AddProgress(const QString& dataPath, const double& progress) -> void override;
        auto GetProgress() const ->ProgressData;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
