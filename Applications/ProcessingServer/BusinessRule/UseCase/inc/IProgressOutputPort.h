#pragma once
#include <memory>

#include "ProcessingServerUseCaseExport.h"
#include "ProgressData.h"

namespace processing_server::UseCase {
    class ProcessingServerUseCase_API IProgressOutputPort {
    public:
        typedef IProgressOutputPort Self;
        typedef std::shared_ptr<Self> Pointer;

        virtual auto SetProgressData(const ProgressData& progressData)->void = 0;
        virtual auto GetProgressData()->ProgressData = 0;
    };

}
