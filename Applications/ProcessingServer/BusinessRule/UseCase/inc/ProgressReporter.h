#pragma once
#include <memory>

#include "IProgressOutputPort.h"
#include "ProcessingServerUseCaseExport.h"

namespace processing_server::UseCase {
    class ProcessingServerUseCase_API ProgressReporter {
    public:
        explicit ProgressReporter(const IProgressOutputPort::Pointer& progressOutputPort);
        ~ProgressReporter();

        auto Report() -> void;
    private:
        struct Impl;
        std::shared_ptr<Impl> d;
        static auto GenerateProgressData()->ProgressData;
    };
}
