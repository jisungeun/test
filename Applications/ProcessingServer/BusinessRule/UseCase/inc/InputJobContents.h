#pragma once
#include <memory>

#include <QString>

#include "DataType.h"
#include "ProcessingServerUseCaseExport.h"

namespace processing_server::UseCase {
    class ProcessingServerUseCase_API InputJobContents {
    public:
        InputJobContents();
        InputJobContents(const InputJobContents& other);
        ~InputJobContents();

        auto SetPaths(const QString& dataPath, const QString& outputPath) -> void;
        auto SetDataType(const Entity::DataType& dataType) -> void;
        auto SetTimeFrames(const int32_t& totalFrameNumber, const int32_t& timeFrameIndex) -> void;
        auto SetProcessingFlags(const bool& reprocessingFlag, const bool& deconvolutionFlag) -> void;
        auto SetDataIncludedFlags(const bool& ht, const bool& flBlue, const bool& flGreen, const bool& flRed,
            const bool& phase, const bool& bf) -> void;

        auto GetDataPath() const->QString;
        auto GetOutputPath() const->QString;
        auto GetDataType() const->Entity::DataType;
        auto GetTotalFrameNumber() const->int32_t;
        auto GetTimeFrameIndex() const->int32_t;

        auto GetReprocessingFlag() const->bool;
        auto GetDeconvolutionFlag() const->bool;

        auto GetHtIncludedFlag() const->bool;
        auto GetFlBlueIncludedFlag() const->bool;
        auto GetFlGreenIncludedFlag() const->bool;
        auto GetFlRedIncludedFlag() const->bool;
        auto GetPhaseIncludedFlag() const->bool;
        auto GetBfIncludedFlag() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
