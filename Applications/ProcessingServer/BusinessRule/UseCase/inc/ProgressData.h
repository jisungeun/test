#pragma once
#include <memory>
#include <QString>

#include "ProcessingServerUseCaseExport.h"

namespace processing_server::UseCase {
    class ProcessingServerUseCase_API ProgressData {
    public:
        ProgressData();
        ProgressData(const ProgressData& other);
        ~ProgressData();
        auto operator=(const ProgressData& other)->ProgressData&;

        auto PushBack(const QString& dataPath, const float& progress)->void;

        auto Size() const ->size_t;
        auto GetDataPath(const int32_t& index) const ->QString;
        auto GetProgress(const int32_t& index) const ->float;
    private:
        struct Impl;
        std::shared_ptr<Impl> d;
    };
}
