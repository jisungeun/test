#include "JobAdder.h"

#include "JobManagerCaller.h"

static int32_t generatedJobId = 0;

namespace processing_server::UseCase {
    JobAdder::JobAdder() = default;
    JobAdder::~JobAdder() = default;

    auto JobAdder::Add(const InputJobContents& inputJobContents) -> void {
        const auto info = GenerateJobInfo(inputJobContents);
        const auto job = std::make_shared<Entity::Job>(info);

        auto jobManager = Entity::JobManagerCaller::GetJobManager();
        jobManager->AddJob(job);
    }

    auto JobAdder::GenerateJobInfo(const InputJobContents& inputJobContents) -> Entity::Job::Info {
        Entity::Job::Info info;
        info.uniqueID = ++(generatedJobId);
        info.dataPath = inputJobContents.GetDataPath();
        info.outputPath = inputJobContents.GetOutputPath();
        info.dataType = inputJobContents.GetDataType();
        info.index = inputJobContents.GetTimeFrameIndex();

        info.totalFrameNumber = inputJobContents.GetTotalFrameNumber();
        info.reprocessingFlag = inputJobContents.GetReprocessingFlag();
        info.deconvolutionFlag = inputJobContents.GetDeconvolutionFlag();

        Entity::Job::DataTypeFlag dataTypeFlag;
        dataTypeFlag.existence[Entity::DataType::HT] = inputJobContents.GetHtIncludedFlag();
        dataTypeFlag.existence[Entity::DataType::FLBLUE] = inputJobContents.GetFlBlueIncludedFlag();
        dataTypeFlag.existence[Entity::DataType::FLGREEN] = inputJobContents.GetFlGreenIncludedFlag();
        dataTypeFlag.existence[Entity::DataType::FLRED] = inputJobContents.GetFlRedIncludedFlag();
        dataTypeFlag.existence[Entity::DataType::PHASE] = inputJobContents.GetPhaseIncludedFlag();
        dataTypeFlag.existence[Entity::DataType::BF] = inputJobContents.GetBfIncludedFlag();

        info.dataTypeFlag = dataTypeFlag;

        return info;
    }
}
