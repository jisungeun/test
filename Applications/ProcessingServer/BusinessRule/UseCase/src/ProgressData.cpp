#include "ProgressData.h"

#include <deque>

namespace processing_server::UseCase {
    struct ProgressData::Impl {
        Impl() = default;
        ~Impl() = default;

        std::deque<QString> dataPaths;
        std::deque<float> progresses;
    };

    ProgressData::ProgressData()
        : d(new Impl()) {
    }

    ProgressData::ProgressData(const ProgressData& other)
        : d(std::make_unique<Impl>(*other.d) ) {
    }

    ProgressData::~ProgressData() = default;

    auto ProgressData::operator=(const ProgressData& other) -> ProgressData& {
        d = std::make_unique<Impl>(*other.d);
        return *this;
    }

    auto ProgressData::PushBack(const QString& dataPath, const float& progress) -> void {
        d->dataPaths.push_back(dataPath);
        d->progresses.push_back(progress);
    }

    auto ProgressData::Size() const -> size_t {
        return d->dataPaths.size();
    }

    auto ProgressData::GetDataPath(const int32_t& index) const -> QString {
        return d->dataPaths.at(index);
    }

    auto ProgressData::GetProgress(const int32_t& index) const -> float {
        return d->progresses.at(index);
    }
}
