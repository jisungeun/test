#include "ProgressReporter.h"

#include "JobManagerCaller.h"
#include "IProgressOutputPort.h"
#include "EntityProgressData.h"

namespace processing_server::UseCase {
    struct ProgressReporter::Impl {
        Impl() = default;
        ~Impl() = default;

        IProgressOutputPort::Pointer progressOutputPort;
    };

    ProgressReporter::ProgressReporter(const IProgressOutputPort::Pointer& progressOutputPort)
        : d(new Impl()) {
        d->progressOutputPort = progressOutputPort;
    }

    ProgressReporter::~ProgressReporter() = default;

    auto ProgressReporter::Report() -> void {
        d->progressOutputPort->SetProgressData(GenerateProgressData());
    }

    auto ProgressReporter::GenerateProgressData() -> ProgressData {
        const auto jobManager = Entity::JobManagerCaller::GetJobManager();

        EntityProgressData::Pointer entityProgressDataPointer(new EntityProgressData);

        const auto progressEntry = jobManager->GetProgressEntry(entityProgressDataPointer);

        const auto entityProgressData = dynamic_cast<EntityProgressData*>(entityProgressDataPointer.get());

        return entityProgressData->GetProgress();
    }
}
