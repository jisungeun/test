#include "InputJobContents.h"

namespace processing_server::UseCase {
    struct InputJobContents::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        QString dataPath{};
        QString outputPath{};
        Entity::DataType dataType{ Entity::DataType::HT };
        int32_t timeFrameIndex{ -1 };

        int32_t totalFrameNumber{ -1 };
        bool reprocessingFlag{ false };
        bool deconvolutionFlag{ true };

        bool htIncluded{ false };
        bool flBlueIncluded{ false };
        bool flGreenIncluded{ false };
        bool flRedIncluded{ false };
        bool phaseIncluded{ false };
        bool bfIncluded{ false };
    };

    InputJobContents::InputJobContents()
        : d(new Impl()) {
    }

    InputJobContents::InputJobContents(const InputJobContents& other)
        : d(std::make_unique<Impl>(*other.d)) {
    }

    InputJobContents::~InputJobContents() = default;

    auto InputJobContents::SetPaths(const QString& dataPath, const QString& outputPath) -> void {
        d->dataPath = dataPath;
        d->outputPath = outputPath;
    }

    auto InputJobContents::SetDataType(const Entity::DataType& dataType) -> void {
        d->dataType = dataType;
    }

    auto InputJobContents::SetTimeFrames(const int32_t& totalFrameNumber, const int32_t& timeFrameIndex) -> void {
        d->totalFrameNumber = totalFrameNumber;
        d->timeFrameIndex = timeFrameIndex;
    }

    auto InputJobContents::SetProcessingFlags(const bool& reprocessingFlag, const bool& deconvolutionFlag) -> void {
        d->reprocessingFlag = reprocessingFlag;
        d->deconvolutionFlag = deconvolutionFlag;
    }

    auto InputJobContents::SetDataIncludedFlags(const bool& ht, const bool& flBlue, const bool& flGreen,
        const bool& flRed, const bool& phase, const bool& bf) -> void {
        d->htIncluded = ht;
        d->flBlueIncluded = flBlue;
        d->flGreenIncluded = flGreen;
        d->flRedIncluded = flRed;
        d->phaseIncluded = phase;
        d->bfIncluded = bf;
    }

    auto InputJobContents::GetDataPath() const -> QString {
        return d->dataPath;
    }

    auto InputJobContents::GetOutputPath() const -> QString {
        return d->outputPath;
    }

    auto InputJobContents::GetDataType() const -> Entity::DataType {
        return d->dataType;
    }

    auto InputJobContents::GetTotalFrameNumber() const -> int32_t {
        return d->totalFrameNumber;
    }

    auto InputJobContents::GetTimeFrameIndex() const -> int32_t {
        return d->timeFrameIndex;
    }

    auto InputJobContents::GetReprocessingFlag() const -> bool {
        return d->reprocessingFlag;
    }

    auto InputJobContents::GetDeconvolutionFlag() const -> bool {
        return d->deconvolutionFlag;
    }

    auto InputJobContents::GetHtIncludedFlag() const -> bool {
        return d->htIncluded;
    }

    auto InputJobContents::GetFlBlueIncludedFlag() const -> bool {
        return d->flBlueIncluded;
    }

    auto InputJobContents::GetFlGreenIncludedFlag() const -> bool {
        return d->flGreenIncluded;
    }

    auto InputJobContents::GetFlRedIncludedFlag() const -> bool {
        return d->flRedIncluded;
    }

    auto InputJobContents::GetPhaseIncludedFlag() const -> bool {
        return d->phaseIncluded;
    }

    auto InputJobContents::GetBfIncludedFlag() const -> bool {
        return d->bfIncluded;
    }
}
