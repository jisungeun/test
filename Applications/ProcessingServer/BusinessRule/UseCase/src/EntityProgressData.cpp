#include "EntityProgressData.h"

namespace processing_server::UseCase {
    struct EntityProgressData::Impl {
        Impl() = default;
        ~Impl() = default;

        ProgressData progressData;
    };

    EntityProgressData::EntityProgressData()
        : d(new Impl()) {
    }

    EntityProgressData::~EntityProgressData() = default;

    auto EntityProgressData::AddProgress(const QString& dataPath, const double& progress) -> void {
        d->progressData.PushBack(dataPath, static_cast<float>(progress));
    }

    auto EntityProgressData::GetProgress() const -> ProgressData {
        return d->progressData;
    }
}
