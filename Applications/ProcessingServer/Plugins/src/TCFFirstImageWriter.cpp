#define LOGGER_TAG "ProcessingServerPlugins::TCFFirstImageWriter"

#include "TCFFirstImageWriter.h"

#include <QFileInfo>
#include <TCLogger.h>

#include "HDF5Mutex.h"

#include "H5Cpp.h"

#include "arrayfire.h"

namespace processing_server::Plugins {
    struct TCFFirstImageWriter::Impl {
        Impl() = default;
        ~Impl() = default;

        QString tcfPath{};
        std::string stdStringTcfPath{};
    };

    TCFFirstImageWriter::TCFFirstImageWriter(const QString& tcfPath)
        : d(new Impl()) {
        d->tcfPath = tcfPath;
        d->stdStringTcfPath = std::string(d->tcfPath.toLocal8Bit().constData());
    }

    TCFFirstImageWriter::~TCFFirstImageWriter() = default;

    auto TCFFirstImageWriter::Write() -> void {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());

        if(HasHtMip()) {
            const auto htMipRequirements = LoadFirstHtMipRequirements();
            const auto mipPath = MakeHtMipPath();
            WriteHtFirstMipImage(mipPath, htMipRequirements);
        }

        if(HasBf()) {
            const auto bfRequirements = LoadFirstBfRequirements();
            const auto bfPath = MakeBfPath();
            WriteBfFirstImage(bfPath, bfRequirements);
        }
    }

    auto TCFFirstImageWriter::HasHtMip() const -> bool {
        const auto htMipGroupPath = std::string("/Data/2DMIP");
        return HasData(htMipGroupPath);
    }

    auto TCFFirstImageWriter::HasBf() const -> bool {
        const auto bfGroupPath = std::string("/Data/BF");
        return HasData(bfGroupPath);
    }

    auto TCFFirstImageWriter::HasData(const std::string& groupPath) const -> bool {
        H5::H5File file{};
        auto hasData{ false };

        try {
            file = H5::H5File(d->stdStringTcfPath, H5F_ACC_RDONLY);
            if (file.exists(groupPath)) {
                auto htMipGroup = file.openGroup(groupPath);

                if (htMipGroup.exists("000000")) {
                    hasData = true;
                }

                htMipGroup.close();
            }
        } catch (H5::Exception & e) {
            std::cout << e.getCDetailMsg() << std::endl;
            QLOG_ERROR() << QString("Fail to check first data in TCF. TCFPath : %1 groupPath : %2")
                .arg(QString::fromStdString(d->stdStringTcfPath))
                .arg(QString::fromStdString(groupPath));
            QLOG_ERROR() << QString::fromStdString(std::string(e.getCDetailMsg()));
        }

        file.close();

        return hasData;
    }

    auto TCFFirstImageWriter::LoadFirstHtMipRequirements() const -> HtMipImageRequirements {
        H5::H5File file{};
        HtMipImageRequirements htMipImageRequirements;
        try {
            file = H5::H5File(d->stdStringTcfPath, H5F_ACC_RDONLY);
            auto htMipGroup = file.openGroup("/Data/2DMIP");

            auto htMipFirstDataSet = htMipGroup.openDataSet("000000");

            auto dataSpace = htMipFirstDataSet.getSpace();
            hsize_t dims[2]{};
            dataSpace.getSimpleExtentDims(dims);
            dataSpace.close();

            const auto rawDataLength = dims[0] * dims[1];
            const std::shared_ptr<uint16_t[]> rawData(new uint16_t[rawDataLength]());

            htMipFirstDataSet.read(rawData.get(), H5::PredType::NATIVE_UINT16);
            htMipFirstDataSet.close();

            htMipImageRequirements.imageSizes =
                ImageSizes{ static_cast<size_t>(dims[1]), static_cast<size_t>(dims[0]) };
            htMipImageRequirements.data = rawData;

            htMipGroup.close();
        } catch (H5::Exception & e) {
            std::cout << e.getCDetailMsg() << std::endl;
            QLOG_ERROR() << QString("Fail to check first HT Mip in TCF. TCFPath : %1")
                .arg(QString::fromStdString(d->stdStringTcfPath));
            QLOG_ERROR() << QString::fromStdString(std::string(e.getCDetailMsg()));
        }

        file.close();

        return htMipImageRequirements;
    }

    auto TCFFirstImageWriter::LoadFirstBfRequirements() const -> BfImageRequirements {
        H5::H5File file{};
        BfImageRequirements bfImageRequirements;
        try {
            file = H5::H5File(d->stdStringTcfPath, H5F_ACC_RDONLY);
            auto bfGroup = file.openGroup("/Data/BF");

            auto bfFirstDataSet = bfGroup.openDataSet("000000");

            auto dataSpace = bfFirstDataSet.getSpace();
            hsize_t dims[3]{};
            dataSpace.getSimpleExtentDims(dims);
            dataSpace.close();

            const auto rawDataLength = dims[0] * dims[1] * dims[2];
            const std::shared_ptr<uint8_t[]> rawData(new uint8_t[rawDataLength]());

            bfFirstDataSet.read(rawData.get(), H5::PredType::NATIVE_UINT8);
            bfFirstDataSet.close();

            bfImageRequirements.imageSizes =
                ImageSizes{ static_cast<size_t>(dims[2]), static_cast<size_t>(dims[1]) };
            bfImageRequirements.data = rawData;

            bfGroup.close();
        } catch (H5::Exception & e) {
            std::cout << e.getCDetailMsg() << std::endl;
            QLOG_ERROR() << QString("Fail to check first BF in TCF. TCFPath : %1")
                .arg(QString::fromStdString(d->stdStringTcfPath));
            QLOG_ERROR() << QString::fromStdString(std::string(e.getCDetailMsg()));
        }

        file.close();

        return bfImageRequirements;
    }

    auto TCFFirstImageWriter::MakeHtMipPath() const -> std::string {
        const auto mipPath = EditPathChangingSuffix("-MIP.TIFF");
        return std::string(mipPath.toLocal8Bit().constData());
    }

    auto TCFFirstImageWriter::MakeBfPath() const -> std::string {
        const auto bfPath = EditPathChangingSuffix("-BF.TIFF");
        return std::string(bfPath.toLocal8Bit().constData());
    }

    auto TCFFirstImageWriter::EditPathChangingSuffix(const QString& suffix) const -> QString {
        const QFileInfo tcf(d->tcfPath);
        const auto tcfName = tcf.fileName();
        const auto editedName = tcf.completeBaseName().append(suffix);

        auto editedPath = d->tcfPath;
        editedPath.replace(tcfName, editedName);

        return editedPath;
    }

    auto TCFFirstImageWriter::WriteHtFirstMipImage(const std::string& imagePath,
        const HtMipImageRequirements& htMipImageRequirements) -> void {
        try {
            af::setDevice(0);
            const auto xSize = htMipImageRequirements.imageSizes.xSize;
            const auto ySize = htMipImageRequirements.imageSizes.ySize;
            const auto data = htMipImageRequirements.data;

            af::array mipImg(xSize, ySize, data.get());
            mipImg = mipImg.as(f32) / 10000;

            auto minValue = af::min<float>(mipImg);
            auto maxValue = af::max<float>(mipImg);
            maxValue = std::max<float>(maxValue, minValue + 0.040);

            mipImg = af::floor((mipImg - minValue) / (maxValue - minValue) * 255);

            mipImg = mipImg.as(u8);
            mipImg.eval();

            af::saveImageNative(imagePath.c_str(), mipImg.T());
        }
        catch (af::exception& e) {
            QLOG_ERROR() << QString("Fail to write HT Mip image. imagePath : %1")
                .arg(QString::fromStdString(imagePath));
            QLOG_ERROR() << QString::fromStdString(std::string(e.what()));
        }
        
    }

    auto TCFFirstImageWriter::WriteBfFirstImage(const std::string& imagePath,
        const BfImageRequirements& bfImageRequirements) -> void {
        try {
            af::setDevice(0);
            const auto xSize = bfImageRequirements.imageSizes.xSize;
            const auto ySize = bfImageRequirements.imageSizes.ySize;
            const auto data = bfImageRequirements.data;
            const af::array bfImage(xSize, ySize, 3, data.get());
            bfImage.eval();

            af::saveImageNative(imagePath.c_str(), bfImage.T());
        }
        catch (af::exception& e) {
            QLOG_ERROR() << QString("Fail to write BF image. imagePath : %1")
                .arg(QString::fromStdString(imagePath));
            QLOG_ERROR() << QString::fromStdString(std::string(e.what()));
        }

    }
}
