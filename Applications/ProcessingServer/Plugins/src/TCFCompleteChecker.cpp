#define LOGGER_TAG "ProcessingServerPlugins::TCFCompleteChecker"

#include "TCFCompleteChecker.h"

#include <TCLogger.h>
#include <PathUtility.h>

#include <QFile>

#include <H5Cpp.h>

#include "HDF5Mutex.h"


namespace processing_server::Plugins {
    struct TCFCompleteChecker::Impl {
        Impl() = default;
        ~Impl() = default;

        Parameter parameter;
        std::string targetPath{};
    };

    TCFCompleteChecker::TCFCompleteChecker(const Parameter& parameter)
        : d(new Impl()) {
        d->parameter = parameter;
        d->targetPath = QStringToStdString(parameter.targetPath);
    }

    TCFCompleteChecker::~TCFCompleteChecker() = default;

    auto TCFCompleteChecker::TcfIsComplete() -> bool {
        if (!CheckFileExists()) {
            return false;
        }

        if (!CheckHdf5Format()) {
            return false;
        }

        if (!CheckTcfStructure()) {
            return false;
        }

        if (!CheckData()) {
            return false;
        }

        return true;
    }

    auto TCFCompleteChecker::QStringToStdString(const QString& qString) -> std::string {
        return std::string(qString.toLocal8Bit().constData());
    }

    auto TCFCompleteChecker::CheckFileExists() const -> bool {
        return QFile(QString::fromStdString(d->targetPath)).exists();
    }

    auto TCFCompleteChecker::CheckHdf5Format() const -> bool {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());
        auto isHdf5Format{ false };
        try {
            isHdf5Format = H5::H5File::isHdf5(d->targetPath);
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << "Checking hdf5 format failed : target (" << d->targetPath.c_str() << ")";
            QLOG_ERROR() << ex.getCDetailMsg();
        }
        return isHdf5Format;
    }

    auto TCFCompleteChecker::CheckTcfStructure() const -> bool {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());

        auto tcfStructureIsCompleted{ false };

        H5::H5File file{};
        try {
            file = H5::H5File(d->targetPath, H5F_ACC_RDONLY);

            const auto dataGroupExists = file.exists("Data");
            const auto infoGroupExists = file.exists("Info");
            const auto annotationGroupExists = file.exists("/Info/Annotation");
            const auto deviceGroupExists = file.exists("/Info/Device");
            const auto imagingGroupExists = file.exists("/Info/Imaging");

            tcfStructureIsCompleted = dataGroupExists && infoGroupExists
                && annotationGroupExists && deviceGroupExists && imagingGroupExists;
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << "Checking tcf structure failed : target (" << d->targetPath.c_str() << ")";
            QLOG_ERROR() << ex.getCDetailMsg();
        }

        file.close();

        return tcfStructureIsCompleted;
    }

    auto TCFCompleteChecker::CheckData() -> bool {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());

        const auto htIncluded = d->parameter.dataTypeFlag.existence[Entity::DataType::HT];
        const auto flBlueIncluded = d->parameter.dataTypeFlag.existence[Entity::DataType::FLBLUE];
        const auto flGreenIncluded = d->parameter.dataTypeFlag.existence[Entity::DataType::FLGREEN];
        const auto flRedIncluded = d->parameter.dataTypeFlag.existence[Entity::DataType::FLRED];
        const auto bfIncluded = d->parameter.dataTypeFlag.existence[Entity::DataType::BF];
        const auto phaseIncluded = d->parameter.dataTypeFlag.existence[Entity::DataType::PHASE];

        if (htIncluded) {
            if (!CheckHt()) {
                return false;
            }
        }
        if (flBlueIncluded) {
            if (!CheckFlBlue()) {
                return false;
            }
        }
        if (flGreenIncluded) {
            if (!CheckFlGreen()) {
                return false;
            }
        }
        if (flRedIncluded) {
            if (!CheckFlRed()) {
                return false;
            }
        }
        if (bfIncluded) {
            if (!CheckBf()) {
                return false;
            }
        }
        if (phaseIncluded) {
            if (!CheckPhase()) {
                return false;
            }
        }

        return true;
    }

    auto TCFCompleteChecker::CheckHt() -> bool {
        const auto ht3dGroupPath = "/Data/3D";
        const auto htMipGroupPath = "/Data/2DMIP";

        return Check3dAndMipGroups(ht3dGroupPath, htMipGroupPath);
    }

    auto TCFCompleteChecker::CheckFlBlue() -> bool {
        const auto flBlue3dGroupPath = "/Data/3DFL/CH0";
        const auto flBlueMipGroupPath = "/Data/2DFLMIP/CH0";

        return Check3dAndMipGroups(flBlue3dGroupPath, flBlueMipGroupPath);
    }

    auto TCFCompleteChecker::CheckFlGreen() -> bool {
        const auto flGreen3dGroupPath = "/Data/3DFL/CH1";
        const auto flGreenMipGroupPath = "/Data/2DFLMIP/CH1";

        return Check3dAndMipGroups(flGreen3dGroupPath, flGreenMipGroupPath);
    }

    auto TCFCompleteChecker::CheckFlRed() -> bool {
        const auto flRed3dGroupPath = "/Data/3DFL/CH2";
        const auto flRedMipGroupPath = "/Data/2DFLMIP/CH2";

        return Check3dAndMipGroups(flRed3dGroupPath, flRedMipGroupPath);
    }

    auto TCFCompleteChecker::CheckBf() -> bool {
        const auto bf3dGroupPath = "/Data/BF";
        return Check3dAndMipGroups(bf3dGroupPath, bf3dGroupPath);
    }

    auto TCFCompleteChecker::CheckPhase() -> bool {
        const auto phaseGroupPath = "/Data/2D";
        return Check3dAndMipGroups(phaseGroupPath, phaseGroupPath);
    }

    auto TCFCompleteChecker::Check3dAndMipGroups(const std::string& group3dPath,
        const std::string& groupMipPath) -> bool {
        auto groupsCompleted{ false };

        H5::H5File file;
        try {
            file = H5::H5File(d->targetPath, H5F_ACC_RDONLY);

            const auto group3dCompleted = CheckGroupAndDataSetNumber(file, group3dPath);
            const auto groupMipCompleted = CheckGroupAndDataSetNumber(file, groupMipPath);

            groupsCompleted = group3dCompleted && groupMipCompleted;
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << "Checking 3d and Mip groups failed : target file (" << d->targetPath.c_str() << ")"
                << "\n target 3d (" << group3dPath.c_str() << ")"
                << "\n target Mip (" << group3dPath.c_str() << ")";
            QLOG_ERROR() << ex.getCDetailMsg();
        }

        file.close();

        return groupsCompleted;
    }

    auto TCFCompleteChecker::CheckGroupAndDataSetNumber(const H5::H5File& file, const std::string& groupPath) const -> bool {
        auto groupAndDataSetIsCompleted = true;
        try {
            if (CheckGroupExists(file, groupPath)) {
                auto targetGroup = file.openGroup(groupPath);
                const auto objectsNumber = targetGroup.getNumObjs();

                if (static_cast<int32_t>(objectsNumber) != d->parameter.totalTimeFrameNumber) {
                    groupAndDataSetIsCompleted = false;
                }
                targetGroup.close();
            } else {
                groupAndDataSetIsCompleted = false;
            }
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << "Checking group and dataset number : target file (" << d->targetPath.c_str() << ")"
                << "\n target group (" << groupPath.c_str() << ")";
            QLOG_ERROR() << ex.getCDetailMsg();
        }
        return groupAndDataSetIsCompleted;
    }

    auto TCFCompleteChecker::CheckGroupExists(const H5::Group& group, const std::string& groupPath) -> bool {
        const auto pathList = TC::DividePath(QString::fromStdString(groupPath));
        const auto subPath = pathList[0];
        QString subGroupPath{};
        for (auto i = 1; i < pathList.size(); ++i) {
            subGroupPath += "/" + pathList[i];
        }

        auto groupExists{ false };
        try {
            if (group.exists(QStringToStdString(subPath))) {
                if (pathList.size() == 1) {
                    groupExists = true;
                } else {
                    auto subGroup = group.openGroup(QStringToStdString(subPath));
                    groupExists = CheckGroupExists(subGroup, QStringToStdString(subGroupPath));
                    subGroup.close();
                }
            }
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << "CheckGroupExists fails : groupName (" << groupPath.c_str() << ")";
            QLOG_ERROR() << ex.getCDetailMsg();
        }

        return groupExists;
    }
}
