#define LOGGER_TAG "ProcessingServerPlugins::TCFWritingDataGenerator"
#include <TCLogger.h>

#include "TCFWritingDataGenerator.h"

#include <string>
#include <QFile>

#include "TCFWritingDataConverter.h"

#include "H5Cpp.h"

#include "HDF5Mutex.h"
#include "PathUtility.h"

namespace processing_server::Plugins {
    struct TCFWritingDataGenerator::Impl {
        Impl() = default;
        ~Impl() = default;

        QString outputPath{};

        Entity::WritingData::Pointer writingData{};
        TC::IO::TCFWritingData::Pointer tcfWritingData{};
        Entity::DataType dataType{ Entity::DataType::HT };
        QString softwareVersion{};
    };

    TCFWritingDataGenerator::TCFWritingDataGenerator(const QString& outputPath,
        const Entity::WritingData::Pointer& writingData, const QString& softwareVersion)
        : d(new Impl()) {
        d->outputPath = outputPath;
        d->writingData = writingData;
        d->dataType = d->writingData->GetJobPointer()->GetDataType();
        d->softwareVersion = softwareVersion;
    }

    TCFWritingDataGenerator::~TCFWritingDataGenerator() = default;

    auto TCFWritingDataGenerator::GenerateTcfWritingData() -> TC::IO::TCFWritingData {
        TCFWritingDataConverter tcfWritingDataConverter(d->writingData, d->softwareVersion);
        d->tcfWritingData = tcfWritingDataConverter.ConvertTcfWritingData();

        GroupMinMaxCorrection();
        return *d->tcfWritingData.get();
    }

    auto TCFWritingDataGenerator::QStringToStdString(const QString& qString) -> std::string {
        return std::string(qString.toLocal8Bit().constData());
    }

    auto TCFWritingDataGenerator::GroupMinMaxCorrection() -> void {
        const auto dataSetMinMax3dValues = d->tcfWritingData->GetDataInfo().GetMinMax3d();
        const auto dataSetMinMaxMipValues = d->tcfWritingData->GetDataInfo().GetMinMaxMip();

        const auto groupMinMax3dValues = ReadGroup3dMinMax();
        const auto groupMinMaxMipValues = ReadGroupMipMinMax();

        const TC::IO::DataInfo::MinMax minMax3dValues{
            std::min(dataSetMinMax3dValues.min,groupMinMax3dValues.min),
            std::max(dataSetMinMax3dValues.max,groupMinMax3dValues.max) };

        const TC::IO::DataInfo::MinMax minMaxMipValues{
            std::min(dataSetMinMaxMipValues.min,groupMinMaxMipValues.min),
            std::max(dataSetMinMaxMipValues.max,groupMinMaxMipValues.max) };

        d->tcfWritingData->GetDataInfo().SetMinMax3d(minMax3dValues);
        d->tcfWritingData->GetDataInfo().SetMinMaxMip(minMaxMipValues);
    }

    auto TCFWritingDataGenerator::ReadGroup3dMinMax() -> TC::IO::DataInfo::MinMax {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());
        auto groupMinValue = std::numeric_limits<double>::max();
        auto groupMaxValue = std::numeric_limits<double>::lowest();

        if (OutputFileExists()) {
            H5::H5File file;
            H5::Group group;
            H5::Attribute minAttribute, maxAttribute;

            try {
                const auto filePath = QStringToStdString(d->outputPath);
                file = H5::H5File(filePath, H5F_ACC_RDONLY);

                const auto attributePathAndName = Get3dAttributePathAndName();

                const auto path = QStringToStdString(std::get<0>(attributePathAndName));
                const auto minName = QStringToStdString(std::get<1>(attributePathAndName));
                const auto maxName = QStringToStdString(std::get<2>(attributePathAndName));

                if (CheckGroupExists(file, path)) {
                    group = file.openGroup(path);
                    minAttribute = group.openAttribute(minName);
                    maxAttribute = group.openAttribute(maxName);

                    auto minValue{ std::numeric_limits<double>::max() };
                    auto maxValue{ std::numeric_limits<double>::lowest() };

                    minAttribute.read(H5::PredType::NATIVE_DOUBLE, &minValue);
                    maxAttribute.read(H5::PredType::NATIVE_DOUBLE, &maxValue);

                    groupMinValue = std::min(groupMinValue, minValue);
                    groupMaxValue = std::max(groupMaxValue, maxValue);
                }
            } catch (std::exception & e) {
                QLOG_ERROR() << e.what();
            } catch (H5::Exception & e) {
                QLOG_ERROR() << e.getCDetailMsg();
            }

            group.close();
            minAttribute.close();
            maxAttribute.close();
            file.close();
        }

        return TC::IO::DataInfo::MinMax{ groupMinValue,groupMaxValue };
    }

    auto TCFWritingDataGenerator::ReadGroupMipMinMax() -> TC::IO::DataInfo::MinMax {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());
        auto groupMinValue = std::numeric_limits<double>::max();
        auto groupMaxValue = std::numeric_limits<double>::lowest();

        if (OutputFileExists()) {
            H5::H5File file;
            H5::Group group;
            H5::Attribute minAttribute, maxAttribute;

            try {
                const auto filePath = QStringToStdString(d->outputPath);
                file = H5::H5File(filePath, H5F_ACC_RDONLY);

                const auto attributePathAndName = GetMipAttributePathAndName();

                const auto path = QStringToStdString(std::get<0>(attributePathAndName));
                const auto minName = QStringToStdString(std::get<1>(attributePathAndName));
                const auto maxName = QStringToStdString(std::get<2>(attributePathAndName));

                if (CheckGroupExists(file, path)) {
                    group = file.openGroup(path);
                    minAttribute = group.openAttribute(minName);
                    maxAttribute = group.openAttribute(maxName);

                    auto minValue{ std::numeric_limits<double>::max() };
                    auto maxValue{ std::numeric_limits<double>::lowest() };

                    minAttribute.read(H5::PredType::NATIVE_DOUBLE, &minValue);
                    maxAttribute.read(H5::PredType::NATIVE_DOUBLE, &maxValue);

                    groupMinValue = std::min(groupMinValue, minValue);
                    groupMaxValue = std::max(groupMaxValue, maxValue);
                }
            } catch (std::exception & e) {
                QLOG_ERROR() << e.what();
            } catch (H5::Exception & e) {
                QLOG_ERROR() << e.getCDetailMsg();
            }

            group.close();
            minAttribute.close();
            maxAttribute.close();
            file.close();
        }

        return TC::IO::DataInfo::MinMax{ groupMinValue,groupMaxValue };
    }

    auto TCFWritingDataGenerator::OutputFileExists() const -> bool {
        return QFile(d->outputPath).exists();
    }

    auto TCFWritingDataGenerator::Get3dAttributePathAndName() const
        -> std::tuple<AttributePath, MinAttributeName, MaxAttributeName> {
        AttributePath attributePath{};
        MinAttributeName minAttributeName{};
        MaxAttributeName maxAttributeName{};

        const auto dataTypeIsHt = d->dataType._value == Entity::DataType::HT;
        const auto dataTypeIsFl = d->dataType._value == Entity::DataType::FLBLUE
            || d->dataType._value == Entity::DataType::FLGREEN
            || d->dataType._value == Entity::DataType::FLRED;
        const auto dataTypeIsBf = d->dataType._value == Entity::DataType::BF;
        const auto dataTypeIsPhase = d->dataType._value == Entity::DataType::PHASE;

        if (dataTypeIsHt) {
            attributePath = "/Data/3D";
            minAttributeName = "RIMin";
            maxAttributeName = "RIMax";
        } else if (dataTypeIsFl) {
            attributePath = "/Data/3DFL";
            minAttributeName = "MinIntensity";
            maxAttributeName = "MaxIntensity";
        } else if (dataTypeIsBf) {
            throw std::exception("Invalid DataType(BF) in TCFWritingDataGenerator::Get3dAttributePathAndName()");
        } else if (dataTypeIsPhase) {
            attributePath = "/Data/2D";
            minAttributeName = "MinPhase";
            maxAttributeName = "MaxPhase";
        }

        return std::tuple<AttributePath, MinAttributeName, MaxAttributeName>
        {attributePath, minAttributeName, maxAttributeName };
    }

    auto TCFWritingDataGenerator::GetMipAttributePathAndName() const
        -> std::tuple<AttributePath, MinAttributeName, MaxAttributeName> {
        AttributePath attributePath{};
        MinAttributeName minAttributeName{};
        MaxAttributeName maxAttributeName{};

        const auto dataTypeIsHt = d->dataType._value == Entity::DataType::HT;
        const auto dataTypeIsFl = d->dataType._value == Entity::DataType::FLBLUE
            || d->dataType._value == Entity::DataType::FLGREEN
            || d->dataType._value == Entity::DataType::FLRED;
        const auto dataTypeIsBf = d->dataType._value == Entity::DataType::BF;
        const auto dataTypeIsPhase = d->dataType._value == Entity::DataType::PHASE;

        if (dataTypeIsHt) {
            attributePath = "/Data/2DMIP";
            minAttributeName = "RIMin";
            maxAttributeName = "RIMax";
        } else if (dataTypeIsFl) {
            attributePath = "/Data/2DFLMIP";
            minAttributeName = "MinIntensity";
            maxAttributeName = "MaxIntensity";
        } else if (dataTypeIsBf) {
            throw std::exception("Invalid DataType(BF) in TCFWritingDataGenerator::GetMipAttributePathAndName()");
        } else if (dataTypeIsPhase) {
            throw std::exception("Invalid DataType(Phase) in TCFWritingDataGenerator::GetMipAttributePathAndName()");
        }

        return std::tuple<AttributePath, MinAttributeName, MaxAttributeName>
        {attributePath, minAttributeName, maxAttributeName };
    }

    auto TCFWritingDataGenerator::CheckGroupExists(const H5::Group& group, const std::string& groupPath) -> bool {
        const auto pathList = TC::DividePath(QString::fromStdString(groupPath));
        const auto subPath = pathList[0];
        QString subGroupPath{};
        for (auto i = 1; i < pathList.size(); ++i) {
            subGroupPath += "/" + pathList[i];
        }

        auto groupExists{ false };
        try {
            if (group.exists(QStringToStdString(subPath))) {
                if (pathList.size() == 1) {
                    groupExists = true;
                } else {
                    auto subGroup = group.openGroup(QStringToStdString(subPath));
                    groupExists = CheckGroupExists(subGroup, QStringToStdString(subGroupPath));
                    subGroup.close();
                }
            }
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << "CheckGroupExists fails : groupName (" << groupPath.c_str() << ")";
            QLOG_ERROR() << ex.getCDetailMsg();
        }

        return groupExists;
    }
}
