#define LOGGER_TAG "ProcessingServerPlugins::ProcessorCreator"
#include "ProcessorCreator.h"

#include <TCLogger.h>

#include "arrayfire.h"

namespace processing_server::Plugins {
    auto ProcessorCreator::Create() -> QList<Processor::Pointer> {
        const auto deviceCount = ScanDeviceCount();

        QList<Processor::Pointer> processorList;
        for (auto deviceIndex = 0; deviceIndex < deviceCount; ++deviceIndex) {
            LogDeviceInfoByIndex(deviceIndex);
            processorList.push_back(GenerateProcessor(deviceIndex));
        }

        return processorList;
    }

    auto ProcessorCreator::ScanDeviceCount() -> int32_t {
        auto deviceCount{ -1 };
        try {
            deviceCount = af::getDeviceCount();
        } catch (const af::exception& afException) {
            QLOG_INFO() << QString(afException.what());
        }
        return deviceCount;
    }

    auto ProcessorCreator::LogDeviceInfoByIndex(const int32_t& deviceIndex) -> void {
        try {
            af::setDevice(deviceIndex);
            char deviceName[64], devicePlatform[10], deviceToolkit[64], deviceCompute[10];
            af::deviceInfo(deviceName, devicePlatform, deviceToolkit, deviceCompute);
            QLOG_INFO() << "Device Index :" << QString::number(deviceIndex);
            QLOG_INFO() << "Device Name :" << QString(deviceName);
            QLOG_INFO() << "Device Platform :" << QString(devicePlatform);
            QLOG_INFO() << "Device Toolkit :" << QString(deviceToolkit);
            QLOG_INFO() << "Device Compute :" << QString(deviceCompute);
        } catch (af::exception & afException) {
            QLOG_INFO() << QString(afException.what());
        }
    }

    auto ProcessorCreator::GenerateProcessor(const int32_t deviceIndex) -> Processor::Pointer {
        const auto processorType = Entity::ProcessorType::LOCAL_GPU;
        const auto priority = (deviceIndex == 0) ? Entity::ProcessorPriority::MAIN : Entity::ProcessorPriority::SUB;
        return std::make_shared<Processor>(processorType, priority, deviceIndex);
    }
}
