#define LOGGER_TAG "ProcessingServerPlugins::Processor"

#include "Processor.h"

#include <QStandardPaths>
#include <QImage>
#include <QDir>
#include <QCoreApplication>
#include <QWaitCondition>
#include <TCLogger.h>

#include "arrayfire.h"
#include "DataArchive.h"
#include "HoloProcessorArrayFire.h"
#include "HoloscopeUtility.h"
#include "SystemConfig.h"

namespace processing_server::Plugins {
    class HoloProcessorReporter : public ProgressReporter {
    public:
        HoloProcessorReporter(Processor* processor)
            : ProgressReporter()
            , processor(processor) {
        }

        void notify(int value) override {
            processor->ReportProgress(value);
        }

    private:
        Processor* processor;
    };

    struct Processor::Impl {
        Impl() = default;
        ~Impl() = default;

        int32_t deviceIndex{ -1 };

        Entity::ProcessorStatus status{ Entity::ProcessorStatus::IDLE };
        Entity::Job::Pointer job{};
        Entity::IProgressUpdater::Pointer updater{};
        Entity::ProcessedData::Pointer processedData{};
        Entity::ProcessorType processorType{ Entity::ProcessorType::LOCAL_GPU };
        Entity::ProcessorPriority priority{ Entity::ProcessorPriority::MAIN };

        QMutex mutex;
        QWaitCondition waitCondition;
        bool runFlag{ true };
    };

    Processor::Processor(QObject* parent) : QThread(parent), d(new Impl()) {
        start();
    }

    Processor::Processor(const Entity::ProcessorType& type, const Entity::ProcessorPriority& priority,
        const int32_t& device, QObject* parent)
        : Processor(parent) {
        d->deviceIndex = device;
        d->processorType = type;
        d->priority = priority;
        SetProcessingDirectory();
    }

    Processor::~Processor() {
        d->mutex.lock();
        d->runFlag = false;
        d->waitCondition.wakeOne();
        d->mutex.unlock();

        wait(10000);
    };

    auto Processor::IsWorking() const -> bool {
        const auto isIdle = (GetStatus()._value == Entity::ProcessorStatus::IDLE);
        return !isIdle;
    }

    auto Processor::SetJobPointer(const Entity::Job::Pointer job) -> void {
        QMutexLocker locker(&d->mutex);
        d->job = job;
    }

    auto Processor::SetUpdater(const Entity::IProgressUpdater::Pointer updater) -> void {
        QMutexLocker locker(&d->mutex);
        d->updater = updater;
    }

    auto Processor::StartProcess() -> void {
        QMutexLocker locker(&d->mutex);
        if (d->status._value == Entity::ProcessorStatus::IDLE) {
            d->status = Entity::ProcessorStatus::PROCESSING;
            d->waitCondition.wakeOne();
        }
    }

    auto Processor::GetProcessedData() -> Entity::ProcessedData::Pointer {
        QMutexLocker locker(&d->mutex);
        d->status = Entity::ProcessorStatus::IDLE;
        return d->processedData;
    }

    auto Processor::SetType(const Entity::ProcessorType& type) -> void {
        d->processorType = type;
    }

    auto Processor::GetType() const -> Entity::ProcessorType {
        return d->processorType;
    }

    auto Processor::SetPriority(const Entity::ProcessorPriority& priority) -> void {
        QMutexLocker locker(&d->mutex);
        d->priority = priority;
    }

    auto Processor::GetPriority() const -> Entity::ProcessorPriority {
        return d->priority;
    }

    auto Processor::GetStatus() const -> Entity::ProcessorStatus {
        return d->status;
    }

    auto Processor::GetProcessedDataMemoryInBytes() -> size_t {
        QMutexLocker locker(&d->mutex);
        const auto dimension = d->processedData->GetDimension();

        const auto voxelSizeX = dimension.GetDimensionX();
        const auto voxelSizeY = dimension.GetDimensionY();
        const auto voxelSizeZ = dimension.GetDimensionZ();

        const auto dataUnitSizeInBytes = GetDataUnitSizeInBytes(d->processedData);

        const auto requiredMemorySizeInBytes =
            voxelSizeX * voxelSizeY * voxelSizeZ * dataUnitSizeInBytes;

        return requiredMemorySizeInBytes;
    }

    auto Processor::GetDataUnitSizeInBytes(const Entity::ProcessedData::Pointer processedData) -> size_t {
        //TODO change implementation using processedData->GetData().Type().name();
        const auto dataType = processedData->GetDataType();

        const auto unitIs2Bytes =
            dataType._value == Entity::DataType::HT || dataType._value == Entity::DataType::FLBLUE ||
            dataType._value == Entity::DataType::FLGREEN || dataType._value == Entity::DataType::FLRED;
        const auto unitIs4Bytes = dataType._value == Entity::DataType::PHASE;
        const auto unitIs1Bytes = dataType._value == Entity::DataType::BF;

        size_t unit{};

        if (unitIs4Bytes) {
            unit = 4;
        } else if (unitIs2Bytes) {
            unit = 2;
        } else if (unitIs1Bytes) {
            unit = 1;
        }

        return unit;
    }

    auto Processor::SetProcessingDirectory() -> void {
        const auto baseDirectory = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
        const auto processingDirPath = QString("%1/.TSSProcessor").arg(baseDirectory);
        auto sysConfig = SystemConfig::GetInstance(processingDirPath, true);
    }

    void Processor::run() {
        while(true) {
            d->mutex.lock();
            if(!d->runFlag) {
                d->mutex.unlock();
                break;
            }

            if(d->status._value == Entity::ProcessorStatus::IDLE) {
                d->waitCondition.wait(&d->mutex);
                d->mutex.unlock();
                continue;
            }

            af::setDevice(d->deviceIndex);

            InitializeProcessedData();
            Process();
            d->mutex.unlock();
            ReportProgress(100);
        }
    }

    auto Processor::InitializeProcessedData() -> void {
        const auto processingDataType = d->job->GetDataType();
        d->processedData = std::make_shared<Entity::ProcessedData>(processingDataType);
    }

    auto Processor::Process() -> void {
        LogStartProcess();

        const auto dataPath = d->job->GetDataPath();
        const auto reprocessingFlag = d->job->GetReprocessingFlag();
        const auto restoredFlag = !reprocessingFlag;

        const auto archiveFlag = TC::restore_background(dataPath, GetTempSubFolderString(), restoredFlag);

        CheckCalibration(dataPath, archiveFlag);

        const auto dataType = d->job->GetDataType();
        ProcessTargetDataType(dataType);

        TC::clear_background(GetTempSubFolderString());

        d->status = Entity::ProcessorStatus::COMPLETE;
        LogEndProcess();
    }

    auto Processor::CheckCalibration(const QString& strPath, bool bArchive) -> bool {
        SystemConfig::Pointer sysConfig = SystemConfig::GetInstance();

        const auto backgroundPath = TC::get_background_path(QString::number(d->job->GetUniqueID()));
        QString bgImgPath = QString("%1/001.png").arg(backgroundPath);

        const auto dataPath = d->job->GetDataPath();
        if (!bArchive) bgImgPath = QString("%1/bgImages/001.png").arg(dataPath);

        QString imgPath;

        if (CheckFirstSampleImage(dataPath, imgPath)) {
            QImage src(imgPath);
            QImage bg(bgImgPath);
        }

        //clear sample file
        QFile(imgPath).remove();

        return true;
    }

    auto Processor::CheckFirstSampleImage(const QString& strPath, QString& imgPath) -> bool {
        const auto m_nIndex = d->job->GetUniqueID();
        const auto dataType = d->job->GetDataType();

        SystemConfig::Pointer sysConfig = SystemConfig::GetInstance();

        bool bArchExist = false;

        QStringList pathList;
        pathList.push_back(QString("%1/data3d/000000/images.dat").arg(strPath));
        pathList.push_back(QString("%1/data2d/000000/images.dat").arg(strPath));
        pathList.push_back(QString("%1/data3dfl/000000/ch0/images.dat").arg(strPath));
        pathList.push_back(QString("%1/data3dfl/000000/ch1/images.dat").arg(strPath));
        pathList.push_back(QString("%1/data3dfl/000000/ch2/images.dat").arg(strPath));

        QString imgArcPath;
        foreach(QString aPath, pathList) {
            if (QFileInfo(aPath).exists()) {
                imgArcPath = aPath;
                bArchExist = true;
                break;
            }
        }

        if (bArchExist) //if archive file exists...
        {
            DataArchive archive;

            archive.setFilename(imgArcPath);

            imgPath = QString("%1/tid_%2_sample.png").arg(sysConfig->getTempPath()).arg(m_nIndex);

            if(dataType._value == Entity::DataType::FLGREEN) {
                if (!archive.restore("/images", "000001", imgPath)) return false;
            } else if (dataType._value == Entity::DataType::FLRED) {
                if (!archive.restore("/images", "000002", imgPath)) return false;
            } else {
                if (!archive.restore("/images", "000000", imgPath)) return false;
            }
            return true;
        }

        pathList.clear();

        //if archive file does not exist...
        bool bImgExist = false;

        pathList.push_back(QString("%1/data3d/000000/001.png").arg(strPath));
        pathList.push_back(QString("%1/data2d/000000/001.png").arg(strPath));
        pathList.push_back(QString("%1/data3dfl/000000/ch00/001.png").arg(strPath));
        pathList.push_back(QString("%1/data3dfl/000000/ch01/001.png").arg(strPath));
        pathList.push_back(QString("%1/data3dfl/000000/ch02/001.png").arg(strPath));

        QString srcImgPath;
        foreach(QString aPath, pathList) {
            if (QFile(aPath).exists()) {
                srcImgPath = aPath;
                bImgExist = true;
            }
        }

        if (bImgExist) //if image file exists...
        {
            imgPath = QString("%1/tid_%2_sample.png").arg(sysConfig->getTempPath()).arg(m_nIndex);

            QFile(imgPath).remove();

            if (!QFile(srcImgPath).copy(imgPath)) return false;

            return true;
        }

        return false;
    }

    auto Processor::ProcessTargetDataType(const Entity::DataType& dataType) -> void {
        if (dataType._value == Entity::DataType::HT) {
            ProcessHt();
        } else if (dataType._value == Entity::DataType::FLBLUE) {
            ProcessFlBlue();
        } else if (dataType._value == Entity::DataType::FLGREEN) {
            ProcessFlGreen();
        } else if (dataType._value == Entity::DataType::FLRED) {
            ProcessFlRed();
        } else if (dataType._value == Entity::DataType::PHASE) {
            ProcessPhase();
        } else if (dataType._value == Entity::DataType::BF) {
            ProcessBf();
        }
    }

    auto Processor::ProcessHt() -> void {
        SystemConfig::Pointer sysConfig = SystemConfig::GetInstance();

        const auto strTopPath = d->job->GetDataPath();
        const auto reprocessingFlag = d->job->GetReprocessingFlag();

        const auto strConfigPath = QString("%1/config.dat").arg(strTopPath);
        auto strDataPath =
            QString("%1/data3d/%2").arg(strTopPath).arg(TimeFrameIndexString());
        auto strBgPath = QString("%1/bgImages").arg(strTopPath);
        const auto strOutPath =
            QString("%1/3dRecon/%2").arg(strTopPath).arg(TimeFrameIndexString());

        if (TC::restore_sample_images(strDataPath, GetTempSubFolderString())) {
            strDataPath = TC::get_sample_images_path(GetTempSubFolderString());
            strBgPath = TC::get_background_path(GetTempSubFolderString());
        }

        std::shared_ptr<HoloProcessorReporter> reporter(new HoloProcessorReporter(this));
        auto holoProcessorArrayFire =
            HoloProcessorArrayFire::create(reporter.get(), QCoreApplication::instance(), 1);

        const auto processSuccess = holoProcessorArrayFire->procHologram3D(strConfigPath, strDataPath, strBgPath,
            strOutPath, reprocessingFlag);

        SetProcessedDataHt(holoProcessorArrayFire);

        TC::clear_sample_images(GetTempSubFolderString());
    }

    auto Processor::ProcessFlBlue() -> void {
        ProcessFlByChannelIndex(0);
    }

    auto Processor::ProcessFlGreen() -> void {
        ProcessFlByChannelIndex(1);
    }

    auto Processor::ProcessFlRed() -> void {
        ProcessFlByChannelIndex(2);
    }

    auto Processor::ProcessFlByChannelIndex(const int32_t& channelIndex) -> void {
        SystemConfig::Pointer sysConfig = SystemConfig::GetInstance();

        const auto strTopPath = d->job->GetDataPath();
        const auto deconvolutionFlag = d->job->GetDeconvolutionFlag();

        const bool isFirstFrameOfChannel = d->job->GetTimeFrameIndex() == 0;

        const auto strConfigPath = QString("%1/config.dat").arg(strTopPath);
        const auto jobParameterPath = QString("%1/JobParameter.tcp").arg(strTopPath);
        const auto strDarkpixelPath = QString("%1/darkpixel.dat").arg(strTopPath);
        auto strDataPath =
            QString("%1/data3dfl/%2").arg(strTopPath).arg(TimeFrameIndexString());
        auto strBgPath = QString("%1/bgImages").arg(strTopPath);
        const auto strOutPath =
            QString("%1/3dFluorescence/%2").arg(strTopPath).arg(TimeFrameIndexString());

        if (TC::restore_flsample_images(strDataPath, GetTempSubFolderString())) {
            strDataPath = TC::get_sample_images_path(GetTempSubFolderString());
            strBgPath = TC::get_background_path(GetTempSubFolderString());
        }

        std::shared_ptr<HoloProcessorReporter> reporter(new HoloProcessorReporter(this));
        auto processorArrayFire =
            HoloProcessorArrayFire::create(reporter.get(), QCoreApplication::instance(), 1);

        const auto htConsider = d->job->GetDataTypeFlag().existence[Entity::DataType::HT];

        const auto processSuccess = processorArrayFire->procHologram3DFL(strConfigPath, strDarkpixelPath,
            strDataPath, jobParameterPath, deconvolutionFlag, isFirstFrameOfChannel, channelIndex, htConsider);

        SetProcessedDataFl(processorArrayFire);

        TC::clear_flsample_images(GetTempSubFolderString());
    }

    auto Processor::ProcessPhase() -> void {
        SystemConfig::Pointer sysConfig = SystemConfig::GetInstance();

        const auto strTopPath = d->job->GetDataPath();
        const auto reprocessingFlag = d->job->GetReprocessingFlag();

        const auto jobParameterPath = QString("%1/JobParameter.tcp").arg(strTopPath);
        const QString strConfigPath = QString("%1/config.dat").arg(strTopPath);
        QString strDataPath = QString("%1/data2d/%2").arg(strTopPath).arg(TimeFrameIndexString());
        QString strBgPath = QString("%1/bgImages").arg(strTopPath);
        const QString strOutPath = QString("%1/2dPhase/%2").arg(strTopPath).arg(TimeFrameIndexString());

        if (TC::restore_sample_images(strDataPath, GetTempSubFolderString())) {
            strDataPath = TC::get_sample_images_path(GetTempSubFolderString());
            strBgPath = TC::get_background_path(GetTempSubFolderString());
        }

        std::shared_ptr<HoloProcessorReporter> reporter(new HoloProcessorReporter(this));
        auto processorArrayFire =
            HoloProcessorArrayFire::create(reporter.get(), QCoreApplication::instance(), 1);

        const auto htConsider = d->job->GetDataTypeFlag().existence[Entity::DataType::HT];

        const auto processSuccess = processorArrayFire->procHologram2D(strConfigPath, strDataPath, strBgPath,
            strOutPath, jobParameterPath, htConsider, reprocessingFlag);

        SetProcessedDataPhase(processorArrayFire);

        TC::clear_sample_images(GetTempSubFolderString());
    }

    auto Processor::ProcessBf() -> void {
        SystemConfig::Pointer sysConfig = SystemConfig::GetInstance();

        const auto strTopPath = d->job->GetDataPath();
        const auto reprocessingFlag = d->job->GetReprocessingFlag();

        const QString strConfigPath = QString("%1/config.dat").arg(strTopPath);
        QString strDataPath = QString("%1/databf/%2").arg(strTopPath).arg(TimeFrameIndexString());
        const QString strOutPath = QString("%1/brightfield/%2").arg(strTopPath).arg(TimeFrameIndexString());

        if (TC::restore_sample_images(strDataPath, GetTempSubFolderString())) {
            strDataPath = TC::get_sample_images_path(GetTempSubFolderString());
        }

        std::shared_ptr<HoloProcessorReporter> reporter(new HoloProcessorReporter(this));
        auto processorArrayFire =
            HoloProcessorArrayFire::create(reporter.get(), QCoreApplication::instance(), 1);

        const auto processSuccess = processorArrayFire->procBrightfield(strConfigPath, strDataPath, strOutPath, reprocessingFlag);

        SetProcessedDataBf(processorArrayFire);

        TC::clear_sample_images(GetTempSubFolderString());
    }

    auto Processor::SetProcessedDataHt(const std::shared_ptr<HoloProcessor> holoProcessor) -> void {
        const auto processorArrayFire = dynamic_cast<HoloProcessorArrayFire*>(holoProcessor.get());

        const auto outputSize = processorArrayFire->GetSizes();
        const auto outputResolution = processorArrayFire->GetResolution();
        const auto outputMinMax = processorArrayFire->GetMinMax();
        const auto outputMinMaxMip = processorArrayFire->GetMinMaxMip();

        Entity::ProcessedData::Dimension dataDimension;
        dataDimension.SetDimension(outputSize.sizeX, outputSize.sizeY, outputSize.sizeZ);

        Entity::ProcessedData::Resolution dataResolution;
        dataResolution.SetResolution(outputResolution.resolutionX, outputResolution.resolutionY,
            outputResolution.resolutionZ);

        Entity::ProcessedData::MinMax minMax3d, minMaxMip;
        minMax3d.SetMinMax(outputMinMax.minValue, outputMinMax.maxValue);
        minMaxMip.SetMinMax(outputMinMaxMip.minValue, outputMinMaxMip.maxValue);

        d->processedData->SetData3d(processorArrayFire->GetRawData());
        d->processedData->SetDataMip(processorArrayFire->GetRawDataMip());
        d->processedData->SetMinMax3d(minMax3d);
        d->processedData->SetMinMaxMip(minMaxMip);
        d->processedData->SetDimension(dataDimension);
        d->processedData->SetResolution(dataResolution);
    }

    auto Processor::SetProcessedDataFl(const std::shared_ptr<HoloProcessor> holoProcessor) -> void {
        const auto processorArrayFire = dynamic_cast<HoloProcessorArrayFire*>(holoProcessor.get());

        const auto outputSize = processorArrayFire->GetSizes();
        const auto outputResolution = processorArrayFire->GetResolution();
        const auto outputMinMax = processorArrayFire->GetMinMax();
        const auto outputMinMaxMip = processorArrayFire->GetMinMaxMip();

        Entity::ProcessedData::Dimension dataDimension;
        dataDimension.SetDimension(outputSize.sizeX, outputSize.sizeY, outputSize.sizeZ);

        Entity::ProcessedData::Resolution dataResolution;
        dataResolution.SetResolution(outputResolution.resolutionX, outputResolution.resolutionY,
            outputResolution.resolutionZ);

        Entity::ProcessedData::MinMax minMax3d, minMaxMip;
        minMax3d.SetMinMax(outputMinMax.minValue, outputMinMax.maxValue);
        minMaxMip.SetMinMax(outputMinMaxMip.minValue, outputMinMaxMip.maxValue);

        d->processedData->SetData3d(processorArrayFire->GetRawData());
        d->processedData->SetDataMip(processorArrayFire->GetRawDataMip());
        d->processedData->SetMinMax3d(minMax3d);
        d->processedData->SetMinMaxMip(minMaxMip);
        d->processedData->SetDimension(dataDimension);
        d->processedData->SetResolution(dataResolution);
    }

    auto Processor::SetProcessedDataPhase(const std::shared_ptr<HoloProcessor> holoProcessor) -> void {
        const auto processorArrayFire = dynamic_cast<HoloProcessorArrayFire*>(holoProcessor.get());

        const auto outputSize = processorArrayFire->GetSizes();
        const auto outputResolution = processorArrayFire->GetResolution();
        const auto outputMinMax = processorArrayFire->GetMinMax();

        Entity::ProcessedData::Dimension dataDimension;
        dataDimension.SetDimension(outputSize.sizeX, outputSize.sizeY, outputSize.sizeZ);

        Entity::ProcessedData::Resolution dataResolution;
        dataResolution.SetResolution(outputResolution.resolutionX, outputResolution.resolutionY,
            outputResolution.resolutionZ);

        Entity::ProcessedData::MinMax minMax3d;
        minMax3d.SetMinMax(outputMinMax.minValue, outputMinMax.maxValue);

        d->processedData->SetData3d(processorArrayFire->GetRawData());
        d->processedData->SetMinMax3d(minMax3d);
        d->processedData->SetDimension(dataDimension);
        d->processedData->SetResolution(dataResolution);
    }

    auto Processor::SetProcessedDataBf(const std::shared_ptr<HoloProcessor> holoProcessor) -> void {
        const auto processorArrayFire = dynamic_cast<HoloProcessorArrayFire*>(holoProcessor.get());

        const auto outputSize = processorArrayFire->GetSizes();
        const auto outputResolution = processorArrayFire->GetResolution();

        Entity::ProcessedData::Dimension dataDimension;
        dataDimension.SetDimension(outputSize.sizeX, outputSize.sizeY, 0);

        Entity::ProcessedData::Resolution dataResolution;
        dataResolution.SetResolution(outputResolution.resolutionX, outputResolution.resolutionY,
            0);

        d->processedData->SetData3d(processorArrayFire->GetRawData());
        d->processedData->SetDimension(dataDimension);
        d->processedData->SetResolution(dataResolution);
    }

    auto Processor::ReportProgress(int value) const -> void {
        Entity::Progress::Parameter parameter;
        parameter.completeProgressValue = float(value) / 100;
        parameter.job = d->job;
        d->updater->Update(parameter);
    }

    auto Processor::GetTempSubFolderString() const -> QString {
        const auto jobId = d->job->GetUniqueID();
        return QString::number(jobId);
    }

    auto Processor::TimeFrameIndexString() -> QString {
        const auto timeFrameIndex = d->job->GetTimeFrameIndex();
        return QString("%1").arg(timeFrameIndex, 6, 10, QChar('0'));
    }

    auto Processor::LogStartProcess() -> void {
        const auto name = d->job->GetDataPath();
        const auto dataTypeString = QString(d->job->GetDataType()._to_string());
        const auto timeFrameIndex = TimeFrameIndexString();

        const auto startLogContents =
            QString("Process Start : %1 %2 %3").arg(name).arg(dataTypeString).arg(timeFrameIndex);

        QLOG_INFO() << "GPU(" << d->deviceIndex << ")" << startLogContents;
    }

    auto Processor::LogEndProcess() -> void {
        const auto name = d->job->GetDataPath();
        const auto dataTypeString = QString(d->job->GetDataType()._to_string());
        const auto timeFrameIndex = TimeFrameIndexString();

        const auto startLogContents =
            QString("Process End : %1 %2 %3").arg(name).arg(dataTypeString).arg(timeFrameIndex);

        QLOG_INFO() << "GPU(" << d->deviceIndex << ")" << startLogContents;
    }
}
