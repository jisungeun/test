#define LOGGER_TAG "ProcessingServerPlugins::TCFEntryWriter"

#include "TCFEntryWriter.h"

#include <TCLogger.h>

#include <iostream>
#include <QFileInfo>
#include <QQueue>
#include <QMutex>
#include <QWaitCondition>
#include <QDir>

#include "TCFWritingDataConverter.h"
#include "TCFWritingAlgorithm.h"
#include "TCFWritingDataGenerator.h"
#include "TCFCompleteChecker.h"
#include "TCFFirstImageWriter.h"

namespace processing_server::Plugins {
    struct TCFEntryWriter::Impl {
        Impl() = default;
        ~Impl() = default;

        size_t usedMemorySize{ 0 };
        Entity::IProgressUpdater::Pointer progressUpdater;
        QQueue<Entity::WritingData::Pointer> dataQueue;
        QMutex mutex;
        QWaitCondition waitCondition;
        bool runFlag{ true };

        QString softwareVersion{};
    };

    TCFEntryWriter::TCFEntryWriter()
        : d(new Impl()) {
        start();
    }

    TCFEntryWriter::~TCFEntryWriter() {
        d->runFlag = false;

        d->mutex.lock();
        d->waitCondition.wakeOne();
        d->mutex.unlock();

        wait(10000);
    }

    auto TCFEntryWriter::GetUsedMemoryInBytes() -> size_t {
        return d->usedMemorySize;
    }

    auto TCFEntryWriter::SetUpdater(const Entity::IProgressUpdater::Pointer& progressUpdater) -> void {
        d->progressUpdater = progressUpdater;
    }

    auto TCFEntryWriter::RequestToWrite(const Entity::WritingData::Pointer& writingData) -> void {
        QMutexLocker locker(&d->mutex);
        d->dataQueue.enqueue(writingData);
        d->usedMemorySize += CalMemorySize(writingData);
        d->waitCondition.wakeOne();
    }

    auto TCFEntryWriter::Clear() -> void {
        d->dataQueue.clear();
        d->usedMemorySize = 0;
    }

    auto TCFEntryWriter::SetSoftwareVersion(const QString& softwareVersion) ->void {
        d->softwareVersion = softwareVersion;
    }

    void TCFEntryWriter::run() {
        while (d->runFlag) {
            d->mutex.lock();
            if (d->dataQueue.isEmpty()) {
                d->waitCondition.wait(&d->mutex);
                d->mutex.unlock();
                continue;
            }
            auto writingData = d->dataQueue.dequeue();
            d->mutex.unlock();

            Write(writingData);
        }
    }

    auto TCFEntryWriter::Write(const Entity::WritingData::Pointer& writingData) -> void {
        LogStartWriting(writingData);
        const auto outputPath = writingData->GetJobPointer()->GetOutputPath();

        const auto tcfTempPath = MakeTempPath(outputPath, "tctmp");

        TCFWritingDataGenerator tcfWritingDataGenerator(tcfTempPath, writingData, d->softwareVersion);
        const auto tcfWritingData = tcfWritingDataGenerator.GenerateTcfWritingData();

        TC::IO::TCFWritingAlgorithm tcfWritingAlgorithm;
        tcfWritingAlgorithm.Write(tcfTempPath, tcfWritingData);
        d->usedMemorySize -= CalMemorySize(writingData);


        TCFCompleteChecker::Parameter parameter;
        parameter.dataTypeFlag = writingData->GetJobPointer()->GetDataTypeFlag();
        parameter.totalTimeFrameNumber = writingData->GetJobPointer()->GetTotalTimeFrameNumber();
        parameter.targetPath = tcfTempPath;

        TCFCompleteChecker tcfCompleteChecker(parameter);

        LogEndWriting(writingData);

        if (tcfCompleteChecker.TcfIsComplete()) {
            QFile file(tcfTempPath);
            file.rename(outputPath);

            TCFFirstImageWriter tcfMipImageWriter(outputPath);
            tcfMipImageWriter.Write();

            QLOG_INFO() << "Complete: " << outputPath;

            std::cout << "Complete: " << QFileInfo(outputPath).fileName().toLocal8Bit().constData() << std::endl;
        }
        
        d->progressUpdater->Update(Entity::Progress::Parameter{ writingData->GetJobPointer(),1 });
    }

    auto TCFEntryWriter::MakeTempPath(const QString& path, const QString& targetExtension) -> QString {
        const QFileInfo qFileInfo(path);
        const auto extension = qFileInfo.suffix();

        auto tempPath = path;
        tempPath = tempPath.left(tempPath.length() - (extension.length() + 1));
        tempPath = tempPath.append(".").append(targetExtension);

        return tempPath;
    }

    auto TCFEntryWriter::CalMemorySize(const Entity::WritingData::Pointer& writingData) -> size_t {
        const auto processedData = writingData->GetProcessedData();
        const auto dimension = processedData->GetDimension();

        const auto voxelSizeX = dimension.GetDimensionX();
        const auto voxelSizeY = dimension.GetDimensionY();
        const auto voxelSizeZ = dimension.GetDimensionZ();

        const auto dataUnitSizeInBytes = GetDataUnitSizeInBytes(processedData);

        const auto requiredMemorySizeInBytes =
            voxelSizeX * voxelSizeY * voxelSizeZ * dataUnitSizeInBytes;

        return requiredMemorySizeInBytes;
    }

    auto TCFEntryWriter::GetDataUnitSizeInBytes(const Entity::ProcessedData::Pointer& processedData) -> size_t {
        const auto nameOfDataType = QString(processedData->GetData3d().type().name());

        const auto unitIs8Bytes = nameOfDataType.contains("double");
        const auto unitIs4Bytes = nameOfDataType.contains("int") || nameOfDataType.contains("float");
        const auto unitIs2Bytes = nameOfDataType.contains("short");
        const auto unitIs1Bytes = nameOfDataType.contains("char");

        size_t unit{};

        if (unitIs8Bytes) {
            unit = 8;
        } else if (unitIs4Bytes) {
            unit = 4;
        } else if (unitIs2Bytes) {
            unit = 2;
        } else if (unitIs1Bytes) {
            unit = 1;
        }

        return unit;
    }

    auto TCFEntryWriter::LogStartWriting(const Entity::WritingData::Pointer& writingData) -> void {
        const auto job = writingData->GetJobPointer();
        const auto name = job->GetDataPath();
        const auto dataTypeString = QString(job->GetDataType()._to_string());
        const auto timeFrameIndex = QString("%1").arg(job->GetTimeFrameIndex(), 6, 10, QChar('0'));

        const auto startLogContents =
            QString("Writing Start : %1 %2 %3").arg(name).arg(dataTypeString).arg(timeFrameIndex);

        QLOG_INFO() << startLogContents;
    }

    auto TCFEntryWriter::LogEndWriting(const Entity::WritingData::Pointer& writingData) -> void {
        const auto job = writingData->GetJobPointer();
        const auto name = job->GetDataPath();
        const auto dataTypeString = QString(job->GetDataType()._to_string());
        const auto timeFrameIndex = QString("%1").arg(job->GetTimeFrameIndex(), 6, 10, QChar('0'));

        const auto startLogContents =
            QString("Writing End : %1 %2 %3").arg(name).arg(dataTypeString).arg(timeFrameIndex);

        QLOG_INFO() << startLogContents;
    }
}
