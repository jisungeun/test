#include "TCFWriterPolicy.h"

namespace processing_server::Plugins {
    struct TCFWriterPolicy::Impl {
        Impl() = default;
        ~Impl() = default;

        size_t limitMemorySizeInBytes{ 0 };
    };

    TCFWriterPolicy::TCFWriterPolicy()
        : d(new Impl()) {
    }

    TCFWriterPolicy::~TCFWriterPolicy() = default;

    auto TCFWriterPolicy::SetLimitMemorySizeInBytes(const size_t& limitMemorySizeInBytes) -> void {
        d->limitMemorySizeInBytes = limitMemorySizeInBytes;
    }

    auto TCFWriterPolicy::Accept(const size_t& requiredMemorySizeInBytes,
        const size_t& usedMemorySizeInBytes) -> bool {
        return (requiredMemorySizeInBytes + usedMemorySizeInBytes) < d->limitMemorySizeInBytes;
    }
}
