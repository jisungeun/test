#define LOGGER_TAG "ProcessingServerPlugins::TCFDataValidityChecker"

#include "TCFDataValidityChecker.h"

#include <QFileInfo>

#include "HDF5Mutex.h"

#include "TCLogger.h"
#include "PathUtility.h"

namespace processing_server::Plugins {
    struct TCFDataValidityChecker::Impl {
        Impl() = default;
        ~Impl() = default;
    };

    TCFDataValidityChecker::TCFDataValidityChecker()
        : d(new Impl()) {
    }

    TCFDataValidityChecker::~TCFDataValidityChecker() = default;

    auto TCFDataValidityChecker::Check(const Entity::Job::Pointer& job) -> bool {
        const auto tempPath = MakeTempPath(job->GetOutputPath(), "tctmp");

        if (!CheckFileExists(tempPath)) {
            return true;
        }

        if (!CheckFileIsHdf5Format(tempPath)) {
            return true;
        }

        const auto dataType = job->GetDataType();
        const auto timeFrameIndex = job->GetTimeFrameIndex();

        if (!CheckGroupAndDataSet(tempPath, dataType, timeFrameIndex)) {
            return true;
        }

        return false;
    }

    auto TCFDataValidityChecker::QStringToStdString(const QString& qString) -> std::string {
        return std::string(qString.toLocal8Bit().constData());
    }

    auto TCFDataValidityChecker::MakeTempPath(const QString& path, const QString& targetExtension) -> QString {
        const QFileInfo qFileInfo(path);
        const auto extension = qFileInfo.suffix();

        auto tempPath = path;
        tempPath = tempPath.left(tempPath.length() - (extension.length() + 1));
        tempPath = tempPath.append(".").append(targetExtension);

        return tempPath;
    }

    auto TCFDataValidityChecker::CheckFileExists(const QString& filePath) -> bool {
        return QFile(filePath).exists();
    }

    auto TCFDataValidityChecker::CheckFileIsHdf5Format(const QString& filePath) const -> bool {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());
        try {
            return H5::H5File::isHdf5(filePath.toLocal8Bit().constData());
        }
        catch (H5::Exception& h5Exception) {
            QLOG_ERROR() << h5Exception.getCDetailMsg();
            return false;
        }
    }

    auto TCFDataValidityChecker::CheckGroupAndDataSet(const QString& filePath, const Entity::DataType& dataType,
        const int32_t& timeFrameIndex) -> bool {
        TC::IO::HDF5MutexLocker locker(TC::IO::HDF5Mutex::GetInstance());

        auto groupAndDataSetExist = false;

        H5::H5File file;
        const auto timeFrameString = QString("%1").arg(timeFrameIndex, 6, 10, QChar('0'));

        try {
            file = H5::H5File(filePath.toLocal8Bit().constData(), H5F_ACC_RDONLY);

            if (dataType._value == Entity::DataType::HT) {
                const auto path3d = QString("Data/3D/%1").arg(timeFrameString);
                const auto pathMip = QString("Data/2DMIP/%1").arg(timeFrameString);

                const auto group3dExists = CheckGroupExists(file, QStringToStdString(path3d));
                const auto group2dMipExists = CheckGroupExists(file, QStringToStdString(pathMip));

                groupAndDataSetExist = group3dExists && group2dMipExists;
            } else if (dataType._value == Entity::DataType::FLBLUE) {
                const auto path3d = QString("Data/3DFL/CH0/%1").arg(timeFrameString);
                const auto pathMip = QString("Data/2DFLMIP/CH0/%1").arg(timeFrameString);

                const auto group3dExists = CheckGroupExists(file, QStringToStdString(path3d));
                const auto group2dMipExists = CheckGroupExists(file, QStringToStdString(pathMip));

                groupAndDataSetExist = group3dExists && group2dMipExists;
            } else if (dataType._value == Entity::DataType::FLGREEN) {
                const auto path3d = QString("Data/3DFL/CH1/%1").arg(timeFrameString);
                const auto pathMip = QString("Data/2DFLMIP/CH1/%1").arg(timeFrameString);

                const auto group3dExists = CheckGroupExists(file, QStringToStdString(path3d));
                const auto group2dMipExists = CheckGroupExists(file, QStringToStdString(pathMip));

                groupAndDataSetExist = group3dExists && group2dMipExists;
            } else if (dataType._value == Entity::DataType::FLRED) {
                const auto path3d = QString("Data/3DFL/CH2/%1").arg(timeFrameString);
                const auto pathMip = QString("Data/2DFLMIP/CH2/%1").arg(timeFrameString);

                const auto group3dExists = CheckGroupExists(file, QStringToStdString(path3d));
                const auto group2dMipExists = CheckGroupExists(file, QStringToStdString(pathMip));

                groupAndDataSetExist = group3dExists && group2dMipExists;
            } else if (dataType._value == Entity::DataType::PHASE) {
                const auto path2d = QString("Data/2D/%1").arg(timeFrameIndex);
                groupAndDataSetExist = CheckGroupExists(file, QStringToStdString(path2d));
            } else if (dataType._value == Entity::DataType::BF) {
                const auto pathBf = QString("Data/BF/%1").arg(timeFrameIndex);
                groupAndDataSetExist = CheckGroupExists(file, QStringToStdString(pathBf));
            }
        }
        catch (H5::Exception& h5Exception) {
            QLOG_ERROR() << h5Exception.getCDetailMsg();
            groupAndDataSetExist = false;
        }
        file.close();
        return groupAndDataSetExist;
    }

    auto TCFDataValidityChecker::CheckGroupExists(const H5::Group& group, const std::string& groupPath) -> bool {
        const auto pathList = TC::DividePath(QString::fromStdString(groupPath));
        const auto subPath = pathList[0];
        QString subGroupPath{};
        for (auto i = 1; i < pathList.size(); ++i) {
            subGroupPath += "/" + pathList[i];
        }

        auto groupExists{ false };
        try {
            if (group.exists(QStringToStdString(subPath))) {
                if (pathList.size() == 1) {
                    groupExists = true;
                } else {
                    auto subGroup = group.openGroup(QStringToStdString(subPath));
                    groupExists = CheckGroupExists(subGroup, QStringToStdString(subGroupPath));
                    subGroup.close();
                }
            }
        } catch (H5::Exception & ex) {
            QLOG_ERROR() << "CheckGroupExists fails : groupName (" << groupPath.c_str() << ")";
            QLOG_ERROR() << ex.getCDetailMsg();
        }

        return groupExists;
    }
}
