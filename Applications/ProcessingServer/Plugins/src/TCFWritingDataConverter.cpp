#include "TCFWritingDataConverter.h"

#include <QDir>
#include <QMap>

#include "JobParameter.h"
#include "TomoBuilderUtility.h"

namespace processing_server::Plugins {
    struct TCFWritingDataConverter::Impl {
        Impl() = default;
        ~Impl() = default;

        Entity::WritingData::Pointer writingData{};
        Entity::ProcessedData::Pointer processedData{};
        Entity::Job::Pointer job{};
        QString dataPath{};
        QMap<QString, QString> configMap;
        Entity::DataType dataType{Entity::DataType::HT};
        QString softwareVersion{};
    };

    TCFWritingDataConverter::TCFWritingDataConverter(const Entity::WritingData::Pointer& writingData,
        const QString& softwareVersion)
        : d(new Impl()){
        d->writingData = writingData;
        d->softwareVersion = softwareVersion;
        Initialize();
    }

    TCFWritingDataConverter::~TCFWritingDataConverter() = default;

    auto TCFWritingDataConverter::ConvertTcfWritingData() -> TC::IO::TCFWritingData::Pointer {
        const auto metaInfo = GenerateMetaInfo();
        const auto dataInfo = GenerateDataInfo();

        TC::IO::TCFWritingData::Pointer tcfWritingData(new TC::IO::TCFWritingData);
        tcfWritingData->SetInfo(dataInfo, metaInfo);

        return tcfWritingData;
    }

    auto TCFWritingDataConverter::Initialize() -> void {
        d->job = d->writingData->GetJobPointer();
        d->processedData = d->writingData->GetProcessedData();
        d->configMap = SetConfigMap(d->job);
        d->dataPath = d->job->GetDataPath();
        d->dataType = d->job->GetDataType();
    }

    auto TCFWritingDataConverter::SetConfigMap(const Entity::Job::Pointer& job) -> QMap<QString, QString> {
        const auto configPath = QString("%1/config.dat").arg(job->GetDataPath());
        QMap<QString, QString> configMap;
        TC::LoadFullParameter(configPath, configMap);
        return configMap;
    }

    auto TCFWritingDataConverter::GenerateMetaInfo() -> TC::IO::TCFMetaInfo {
        TC::IO::TCFMetaInfo metaInfo;

        metaInfo.SetCommon(GenerateCommonInfo());
        metaInfo.SetAnnotation(GenerateAnnotationInfo());
        metaInfo.SetDevice(GenerateDeviceInfo());
        metaInfo.SetImaging(GenerateImagingInfo());

        const auto tilePath = QString("%1/tiles.dat").arg(d->dataPath);
        if (TC::FileExists(tilePath)) {
            metaInfo.SetTile(GenerateTileInfo());
        }

        return metaInfo;
    }

    auto TCFWritingDataConverter::GenerateCommonInfo() const -> TC::IO::TCFMetaInfo::Common {
        const auto configPath = QString("%1/config.dat").arg(d->dataPath);
        const auto commentPath = QString("%1/Comment.txt").arg(d->dataPath);
        const auto timeStampPath = QString("%1/timestamp.txt").arg(d->dataPath);
        const auto jobParameterPath = QString("%1/JobParameter.tcp").arg(d->dataPath);

        const auto timeStampNumber = TC::loadTimestamp(timeStampPath);
        const auto timeStampString = TC::TimeStamp2String(timeStampNumber);
        const auto currentTimeString = TC::CurrentDateTime();

        const JobParameter jobParameter(jobParameterPath);

        TC::IO::TCFMetaInfo::Common common;
        if (StringMapHasParameter(d->configMap,"Serial")) {
            common.ids.dataID = TC::GenDataID(d->configMap["Serial"], timeStampString);
            common.ids.uniqueID = TC::GenUniqueID(d->configMap["Serial"], timeStampString, currentTimeString);
            common.devices.deviceSerial = d->configMap["Serial"];
        }

        if (StringMapHasParameter(d->configMap, "SW Version")) {
            common.devices.deviceSoftwareVersion = d->configMap["SW Version"];
        }

        if (StringMapHasParameter(d->configMap, "Computer")) {
            common.devices.deviceHost = d->configMap["Computer"];
        }

        common.ids.userID = " ";
        common.formatVersion = "not recorded";
        common.softwareVersion = d->softwareVersion;
        common.createDate = currentTimeString;
        common.description = TC::LoadComment(commentPath);
        if(common.description == " ") {
            common.description = QString("%1").arg(QChar(' '), 4096);
        }

        common.recordingTime = timeStampString;
        common.title = jobParameter.title();

        return common;
    }

    auto TCFWritingDataConverter::GenerateAnnotationInfo() const -> TC::IO::TCFMetaInfo::Annotation {
        const auto dataPath = d->job->GetDataPath();
        const auto annotationPath = QString("%1/Annotation.ini").arg(dataPath);

        TC::IO::TCFMetaInfo::Annotation annotation;
        annotation.annotations = TC::LoadAnnotation(annotationPath);
        return annotation;
    }

    auto TCFWritingDataConverter::GenerateDeviceInfo() const -> TC::IO::TCFMetaInfo::Device {
        TC::IO::TCFMetaInfo::Device device;

        if (StringMapHasParameter(d->configMap, "iteration")) {
            device.iteration = d->configMap["iteration"].toDouble();
        }
        if (StringMapHasParameter(d->configMap, "M")) {
            device.magnification = d->configMap["M"].toDouble();
        }
        if (StringMapHasParameter(d->configMap, "NA")) {
            device.na = d->configMap["NA"].toDouble();
        }
        if (StringMapHasParameter(d->configMap, "n_m")) {
            device.ri = d->configMap["n_m"].toDouble();
        }
        if (StringMapHasParameter(d->configMap, "Lambda")) {
            device.waveLength = d->configMap["Lambda"].toDouble();
        }

        // TODO device rawSize, zp, zp2, zp3
        device.rawSize = 0;
        device.zps.zp = 0;
        device.zps.zp2 = 0;
        device.zps.zp3 = 0;

        return device;
    }

    auto TCFWritingDataConverter::GenerateImagingInfo() const -> TC::IO::TCFMetaInfo::Imaging {
        TC::IO::TCFMetaInfo::Imaging imaging;

        if (StringMapHasParameter(d->configMap, "Camera Shutter")) {
            imaging.cameraShutter = d->configMap["Camera Shutter"].toDouble();
        }
        if (StringMapHasParameter(d->configMap, "Camera Gain")) {
            imaging.cameraGain = d->configMap["Camera Gain"].toDouble();
        }

        return imaging;
    }

    auto TCFWritingDataConverter::GenerateTileInfo() -> TC::IO::TCFMetaInfo::Tile {
        const auto tilePath = QString("%1/tiles.dat").arg(d->dataPath);

        const auto tileMap = TC::LoadTile(tilePath);

        TC::IO::TCFMetaInfo::Tile tile;
        if (StringMapHasParameter(tileMap, "Row")) {
            tile.tilePositions.rowIndex = static_cast<int64_t>(tileMap["Row"].toInt());
        }
        if (StringMapHasParameter(tileMap, "Column")) {
            tile.tilePositions.columnIndex = static_cast<int64_t>(tileMap["Column"].toInt());
        }
        if (StringMapHasParameter(tileMap, "Overlap_V")) {
            tile.overlapLengths.verticalLengthInMicroMeter = static_cast<int64_t>(tileMap["Overlap_V"].toDouble());
        }
        if (StringMapHasParameter(tileMap, "Overlap_H")) {
            tile.overlapLengths.horizontalLengthInMicroMeter = static_cast<int64_t>(tileMap["Overlap_H"].toDouble());
        }
        if (StringMapHasParameter(tileMap, "TilesV")) {
            tile.tileSizes.tileVertical = static_cast<int64_t>(tileMap["TilesV"].toInt());
        }
        if (StringMapHasParameter(tileMap, "TilesH")) {
            tile.tileSizes.tileHorizontal = static_cast<int64_t>(tileMap["TilesH"].toInt());
        }
        if (StringMapHasParameter(tileMap, "Tiles")) {
            tile.tileSizes.tileNumber = static_cast<int64_t>(tileMap["Tiles"].toInt());
        }
        if (StringMapHasParameter(tileMap, "CPos")) {
            tile.positions.c = static_cast<int64_t>(tileMap["CPos"].toDouble());
        }
        if (StringMapHasParameter(tileMap, "XPos")) {
            tile.positions.x = static_cast<int64_t>(tileMap["XPos"].toDouble());
        }
        if (StringMapHasParameter(tileMap, "YPos")) {
            tile.positions.y = static_cast<int64_t>(tileMap["YPos"].toDouble());
        }
        if (StringMapHasParameter(tileMap, "ZPos")) {
            tile.positions.z = static_cast<int64_t>(tileMap["ZPos"].toDouble());
        }
        return tile;
    }

    auto TCFWritingDataConverter::StringMapHasParameter(const QMap<QString, QString>& stringMap,
        const QString& parameterName) -> bool {
        if (stringMap.find(parameterName) == stringMap.end()) {
            return false;
        } else {
            return true;
        }
    }

    auto TCFWritingDataConverter::GenerateDataInfo() const -> TC::IO::DataInfo {
        TC::IO::DataInfo dataInfo;
        dataInfo.SetImageType(GenerateImageType());
        dataInfo.SetRawData(GenerateRawData());
        dataInfo.SetTimelapseIndex(d->job->GetTimeFrameIndex());
        dataInfo.SetSizes(GenerateSizes());

        dataInfo.SetMinMax3d(GenerateMinMax3d());
        dataInfo.SetMinMaxMip(GenerateMinMaxMip());
        dataInfo.SetPositions(GeneratePositions());
        dataInfo.SetRecordingTime(GenerateRecordingTime());
        dataInfo.SetChannels(GenerateChannels());
        dataInfo.SetOffsetZs(GenerateOffsetZs());
        dataInfo.SetDataCount(d->job->GetTotalTimeFrameNumber());
        dataInfo.SetResolutions(GenerateResolutions());
        dataInfo.SetTimeInterval(GenerateTimeInterval());

        return dataInfo;
    }

    auto TCFWritingDataConverter::GenerateImageType() const -> TC::IO::ImageType {
        TC::IO::ImageType imageType{ TC::IO::ImageType::HT };
        switch (d->dataType) {
        case Entity::DataType::HT:
            imageType = TC::IO::ImageType::HT;
            break;
        case Entity::DataType::FLBLUE:
            imageType = TC::IO::ImageType::FLBLUE;
            break;
        case Entity::DataType::FLGREEN:
            imageType = TC::IO::ImageType::FLGREEN;
            break;
        case Entity::DataType::FLRED:
            imageType = TC::IO::ImageType::FLRED;
            break;
        case Entity::DataType::PHASE:
            imageType = TC::IO::ImageType::PHASE;
            break;
        case Entity::DataType::BF:
            imageType = TC::IO::ImageType::BF;
            break;
        default:
            break;
        }
        return imageType;
    }

    auto TCFWritingDataConverter::GenerateRawData() const -> TC::IO::DataInfo::RawData {
        TC::IO::DataInfo::RawData rawData;
        rawData.data3d = d->processedData->GetData3d();
        rawData.dataMip = d->processedData->GetDataMip();

        return rawData;
    }

    auto TCFWritingDataConverter::GenerateSizes() const -> TC::IO::DataInfo::Sizes {
        const auto dimension = d->processedData->GetDimension();

        TC::IO::DataInfo::Sizes sizes;
        sizes.sizeX = dimension.GetDimensionX();
        sizes.sizeY = dimension.GetDimensionY();
        sizes.sizeZ = dimension.GetDimensionZ();
        sizes.sizeT = dimension.GetDimensionZ();

        return sizes;
    }

    auto TCFWritingDataConverter::GenerateMinMax3d() const -> TC::IO::DataInfo::MinMax {
        TC::IO::DataInfo::MinMax minMax3d;
        minMax3d.min = d->processedData->GetMinMax3d().GetMinValue();
        minMax3d.max = d->processedData->GetMinMax3d().GetMaxValue();
        return minMax3d;
    }

    auto TCFWritingDataConverter::GenerateMinMaxMip() const -> TC::IO::DataInfo::MinMax {
        TC::IO::DataInfo::MinMax minMaxMip;
        minMaxMip.min = d->processedData->GetMinMaxMip().GetMinValue();
        minMaxMip.max = d->processedData->GetMinMaxMip().GetMaxValue();
        return minMaxMip;
    }

    auto TCFWritingDataConverter::GeneratePositions() const -> TC::IO::DataInfo::Positions {
        const auto timeFrameString =
            QString("%1").arg(d->job->GetTimeFrameIndex(), 6, 10, QLatin1Char('0'));

        QString positionFilePath;

        if (d->dataType._value == Entity::DataType::HT) {
            positionFilePath = QString("%1/data3d/%2/position.txt").arg(d->dataPath).arg(timeFrameString);
        } else if (d->dataType._value == Entity::DataType::FLBLUE) {
            positionFilePath = QString("%1/data3dfl/%2/ch0/position.txt").arg(d->dataPath).arg(timeFrameString);
        } else if (d->dataType._value == Entity::DataType::FLGREEN) {
            positionFilePath = QString("%1/data3dfl/%2/ch1/position.txt").arg(d->dataPath).arg(timeFrameString);
        } else if (d->dataType._value == Entity::DataType::FLRED) {
            positionFilePath = QString("%1/data3dfl/%2/ch2/position.txt").arg(d->dataPath).arg(timeFrameString);
        } else if (d->dataType._value == Entity::DataType::BF) {
            positionFilePath = QString("%1/databf/%2/position.txt").arg(d->dataPath).arg(timeFrameString);
        } else if (d->dataType._value == Entity::DataType::PHASE) {
            positionFilePath = QString("%1/data2d/%2/position.txt").arg(d->dataPath).arg(timeFrameString);
        }

        const auto tcPosition = TC::loadPosition(positionFilePath);

        TC::IO::DataInfo::Positions positions;
        positions.positionX = tcPosition.x;
        positions.positionY = tcPosition.y;
        positions.positionZ = tcPosition.z;
        positions.positionC = tcPosition.c;

        return positions;
    }

    auto TCFWritingDataConverter::GenerateRecordingTime() const -> QString {
        const auto timeFrameString = QString("%1").arg(d->job->GetTimeFrameIndex(), 6, 10, QChar('0'));
        const QString timeStampFile = "acquisition_timestamp.txt";
        QString recordingTimeFilePath;
        if (d->dataType._value == Entity::DataType::HT) {
            recordingTimeFilePath = 
                QString("%1/data3d/%2/%3").arg(d->dataPath).arg(timeFrameString).arg(timeStampFile);
        } else if (d->dataType._value == Entity::DataType::FLBLUE) {
            recordingTimeFilePath = 
                QString("%1/data3dfl/%2/ch0/%3").arg(d->dataPath).arg(timeFrameString).arg(timeStampFile);
        } else if (d->dataType._value == Entity::DataType::FLGREEN) {
            recordingTimeFilePath = 
                QString("%1/data3dfl/%2/ch1/%3").arg(d->dataPath).arg(timeFrameString).arg(timeStampFile);
        } else if (d->dataType._value == Entity::DataType::FLRED) {
            recordingTimeFilePath = 
                QString("%1/data3dfl/%2/ch2/%3").arg(d->dataPath).arg(timeFrameString).arg(timeStampFile);
        } else if (d->dataType._value == Entity::DataType::BF) {
            recordingTimeFilePath = 
                QString("%1/databf/%2/%3").arg(d->dataPath).arg(timeFrameString).arg(timeStampFile);
        } else if (d->dataType._value == Entity::DataType::PHASE) {
            recordingTimeFilePath = 
                QString("%1/data2d/%2/%3").arg(d->dataPath).arg(timeFrameString).arg(timeStampFile);
        }
        const auto recordingTimeString = TC::TimeStamp2String(TC::loadTimestamp(recordingTimeFilePath));

        return recordingTimeString;
    }

    auto TCFWritingDataConverter::GenerateChannels() const -> int64_t {
        const auto flDirectoryPath = 
            QString("%1/data3dfl/%2").arg(d->dataPath).arg(d->job->GetTimeFrameIndex(), 6, 10, QChar('0'));

        const QStringList nameFilter("ch*");
        const auto chDirs =
            QDir(flDirectoryPath).entryList(nameFilter, QDir::Dirs, QDir::Name);

        return static_cast<int64_t>(chDirs.size());
    }

    auto TCFWritingDataConverter::GenerateOffsetZs() const -> TC::IO::DataInfo::OffsetZs {
        const auto dataTypeIsFL = d->dataType._value == Entity::DataType::FLBLUE
            || d->dataType._value == Entity::DataType::FLGREEN
            || d->dataType._value == Entity::DataType::FLRED;

        double offsetZ{ 0 }, offsetZCompensation{ 0 };

        if(dataTypeIsFL) {
            const auto configPath = QString("%1/config.dat").arg(d->dataPath);
            const auto jobParameterPath = QString("%1/JobParameter.tcp").arg(d->dataPath);

            const auto lengthZ = TC::GetHtZWorldLength(configPath, jobParameterPath);

            if (StringMapHasParameter(d->configMap, "Acquisition Z Offset")) {
                offsetZ = d->configMap["Acquisition Z Offset"].toDouble();
            }
            if (StringMapHasParameter(d->configMap, "Acquisition Z Offset Compensation")) {
                offsetZCompensation = d->configMap["Acquisition Z Offset Compensation"].toDouble() / 1000.0;
            }

            offsetZ += lengthZ / 2 + offsetZCompensation;
        }


        TC::IO::DataInfo::OffsetZs offsetZs;
        offsetZs.offsetZ = offsetZ;
        offsetZs.offsetZCompensation = offsetZCompensation;

        return offsetZs;
    }

    auto TCFWritingDataConverter::GenerateResolutions() const -> TC::IO::DataInfo::Resolutions {
        const auto resolution = d->processedData->GetResolution();
        const double timeInterval = GenerateTimeInterval();

        TC::IO::DataInfo::Resolutions resolutions;
        resolutions.resolutionX = resolution.GetResolutionX();
        resolutions.resolutionY = resolution.GetResolutionY();
        resolutions.resolutionZ = resolution.GetResolutionZ();
        resolutions.resolutionT = timeInterval;

        return resolutions;
    }

    auto TCFWritingDataConverter::GenerateTimeInterval() const -> double {
        double timeInterval{ 0 };
        if (d->dataType._value == Entity::DataType::PHASE) {
            if (StringMapHasParameter(d->configMap, "Acquisition interval 2D")) {
                timeInterval = d->configMap["Acquisition interval 2D"].toDouble();
            }
        } else {
            if (StringMapHasParameter(d->configMap, "Acquisition Time Interval")) {
                timeInterval = d->configMap["Acquisition Time Interval"].toDouble();
            } else if (StringMapHasParameter(d->configMap, "Acquisition interval 3D")) {
                timeInterval = d->configMap["Acquisition interval 3D"].toDouble();
            }
        }

        return timeInterval;
    }
}
