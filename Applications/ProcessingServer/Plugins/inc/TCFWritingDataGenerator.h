#pragma once
#include <memory>

#include "ProcessingServerPluginsExport.h"

#include <tuple>

#include "TCFWritingData.h"
#include "WritingData.h"

#include "H5Cpp.h"

namespace processing_server::Plugins {
    class ProcessingServerPlugins_API TCFWritingDataGenerator {
    public:
        TCFWritingDataGenerator(const QString& outputPath, const Entity::WritingData::Pointer& writingData, 
            const QString& softwareVersion);
        ~TCFWritingDataGenerator();

        auto GenerateTcfWritingData()->TC::IO::TCFWritingData;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        typedef QString MinAttributeName;
        typedef QString MaxAttributeName;
        typedef QString AttributePath;

        static auto QStringToStdString(const QString& qString)->std::string;

        auto GroupMinMaxCorrection()->void;
        auto ReadGroup3dMinMax()->TC::IO::DataInfo::MinMax;
        auto ReadGroupMipMinMax()->TC::IO::DataInfo::MinMax;
        auto OutputFileExists() const ->bool;
        auto Get3dAttributePathAndName() const
            ->std::tuple<AttributePath, MinAttributeName, MaxAttributeName>;
        auto GetMipAttributePathAndName() const
            ->std::tuple<AttributePath, MinAttributeName, MaxAttributeName>;
        static auto CheckGroupExists(const H5::Group& group, const std::string& groupPath) ->bool;

    };
}
