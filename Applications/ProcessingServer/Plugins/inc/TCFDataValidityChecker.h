#pragma once
#include "ProcessingServerPluginsExport.h"
#include "IJobValidityChecker.h"

#include <H5Cpp.h>

namespace processing_server::Plugins {
    class ProcessingServerPlugins_API TCFDataValidityChecker final : public Entity::IJobValidityChecker{
    public:
        TCFDataValidityChecker();
        ~TCFDataValidityChecker();
        
        auto Check(const Entity::Job::Pointer& job) -> bool override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        static auto QStringToStdString(const QString& qString)->std::string;

        static auto MakeTempPath(const QString& path, const QString& targetExtension)->QString;
        static auto CheckFileExists(const QString& filePath)->bool;
        auto CheckFileIsHdf5Format(const QString& filePath) const ->bool;
        auto CheckGroupAndDataSet(const QString& filePath, const Entity::DataType& dataType,
            const int32_t& timeFrameIndex)->bool;
        static auto CheckGroupExists(const H5::Group& group, const std::string& groupPath) ->bool;

    };
}
