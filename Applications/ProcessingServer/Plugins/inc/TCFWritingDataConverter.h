#pragma once
#include <memory>

#include "ProcessingServerPluginsExport.h"

#include "WritingData.h"
#include "TCFWritingData.h"


namespace processing_server::Plugins {
    class ProcessingServerPlugins_API TCFWritingDataConverter {
    public:
        TCFWritingDataConverter(const Entity::WritingData::Pointer& writingData, const QString& softwareVersion);
        ~TCFWritingDataConverter();

        auto ConvertTcfWritingData()->TC::IO::TCFWritingData::Pointer;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        auto Initialize()->void;
        static auto SetConfigMap(const Entity::Job::Pointer& job)->QMap<QString, QString>;

        auto GenerateMetaInfo()->TC::IO::TCFMetaInfo;
        auto GenerateCommonInfo() const ->TC::IO::TCFMetaInfo::Common;
        auto GenerateAnnotationInfo() const ->TC::IO::TCFMetaInfo::Annotation;
        auto GenerateDeviceInfo() const ->TC::IO::TCFMetaInfo::Device;
        auto GenerateImagingInfo() const ->TC::IO::TCFMetaInfo::Imaging;
        auto GenerateTileInfo()->TC::IO::TCFMetaInfo::Tile;
        static auto StringMapHasParameter(const QMap<QString,QString>& stringMap, const QString& parameterName) ->bool;

        auto GenerateDataInfo() const ->TC::IO::DataInfo;

        auto GenerateImageType() const ->TC::IO::ImageType;
        auto GenerateRawData() const ->TC::IO::DataInfo::RawData;
        auto GenerateSizes() const ->TC::IO::DataInfo::Sizes;
        auto GenerateMinMax3d() const ->TC::IO::DataInfo::MinMax;
        auto GenerateMinMaxMip() const ->TC::IO::DataInfo::MinMax;
        auto GeneratePositions() const ->TC::IO::DataInfo::Positions;
        auto GenerateRecordingTime() const ->QString;
        auto GenerateChannels() const ->int64_t;
        auto GenerateOffsetZs() const ->TC::IO::DataInfo::OffsetZs;
        auto GenerateResolutions() const ->TC::IO::DataInfo::Resolutions;
        auto GenerateTimeInterval() const ->double;
    };
}
