#pragma once
#include "ProcessingServerPluginsExport.h"

#include <QThread>

#include "DataInfo.h"
#include "IFileWriter.h"
#include "TCFMetaInfo.h"

namespace processing_server::Plugins {
    class ProcessingServerPlugins_API TCFEntryWriter final : public Entity::IFileWriter, public QThread {
    public:
        TCFEntryWriter();
        ~TCFEntryWriter();

        auto GetUsedMemoryInBytes() -> size_t override;
        auto SetUpdater(const Entity::IProgressUpdater::Pointer& progressUpdater) -> void override;
        auto RequestToWrite(const Entity::WritingData::Pointer& writingData) -> void override;
        auto Clear() -> void override;
        auto SetSoftwareVersion(const QString& softwareVersion)->void override;
    protected:
        void run() override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    private:
        auto Write(const Entity::WritingData::Pointer& writingData)->void;
        static auto MakeTempPath(const QString& path, const QString& targetExtension)->QString;
        static auto CalMemorySize(const Entity::WritingData::Pointer& writingData)->size_t;
        static auto GetDataUnitSizeInBytes(const Entity::ProcessedData::Pointer& processedData)->size_t;
        auto LogStartWriting(const Entity::WritingData::Pointer& writingData)->void;
        auto LogEndWriting(const Entity::WritingData::Pointer& writingData)->void;

    };
}
