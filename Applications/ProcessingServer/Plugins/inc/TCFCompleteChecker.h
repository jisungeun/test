#pragma once
#include <memory>
#include <string>
#include <QString>

#include <H5Cpp.h>

#include "ProcessingServerPluginsExport.h"

#include "Job.h"

namespace processing_server::Plugins {
    class ProcessingServerPlugins_API TCFCompleteChecker {
    public:
        struct Parameter {
            QString targetPath{};
            int32_t totalTimeFrameNumber{ 0 };
            Entity::Job::DataTypeFlag dataTypeFlag;
        };

        TCFCompleteChecker(const Parameter& parameter);
        ~TCFCompleteChecker();

        auto TcfIsComplete()->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;

    private:
        static auto QStringToStdString(const QString& qString)->std::string;

        auto CheckFileExists() const ->bool;
        auto CheckHdf5Format() const ->bool;
        auto CheckTcfStructure() const ->bool;
        auto CheckData()->bool;
        auto CheckHt()->bool;
        auto CheckFlBlue()->bool;
        auto CheckFlGreen()->bool;
        auto CheckFlRed()->bool;
        auto CheckBf()->bool;
        auto CheckPhase()->bool;
        auto Check3dAndMipGroups(const std::string& group3dPath, const std::string& groupMipPath)->bool;
        auto CheckGroupAndDataSetNumber(const H5::H5File& file, const std::string& groupPath) const ->bool;
        static auto CheckGroupExists(const H5::Group& group, const std::string& groupPath) ->bool;
    };

}