#pragma once
#include <QThread>

#include "ProcessingServerPluginsExport.h"
#include "IProcessor.h"
#include "HoloProcessor.h"

namespace processing_server::Plugins {
    class HoloProcessorReporter;

    class ProcessingServerPlugins_API Processor : public QThread, public Entity::IProcessor {
        Q_OBJECT
    public:
        Processor(QObject* parent = nullptr);
        Processor(const Entity::ProcessorType& type, const Entity::ProcessorPriority& priority,
            const int32_t& device = 0, QObject* parent = nullptr);
        ~Processor();

        auto IsWorking() const -> bool override;
        auto SetJobPointer(const Entity::Job::Pointer job) -> void override;
        auto SetUpdater(const Entity::IProgressUpdater::Pointer updater) -> void override;
        auto StartProcess() -> void override;
        auto GetProcessedData()->Entity::ProcessedData::Pointer override;
        auto SetType(const Entity::ProcessorType& type) -> void override;
        auto GetType() const->Entity::ProcessorType override;
        auto SetPriority(const Entity::ProcessorPriority& priority) -> void override;
        auto GetPriority() const->Entity::ProcessorPriority override;
        auto GetStatus() const->Entity::ProcessorStatus override;
        auto GetProcessedDataMemoryInBytes()->size_t override;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class HoloProcessorReporter;
    private:
        static auto GetDataUnitSizeInBytes(const Entity::ProcessedData::Pointer processedData)->size_t;

        auto SetProcessingDirectory()->void;
        auto InitializeProcessedData()->void;
        auto Process()->void;

        auto CheckCalibration(const QString& strPath, bool bArchive)->bool;
        auto CheckFirstSampleImage(const QString& strPath, QString& imgPath)->bool;

        auto ProcessTargetDataType(const Entity::DataType& dataType)->void;

        auto ProcessHt()->void;
        auto ProcessFlBlue()->void;
        auto ProcessFlGreen()->void;
        auto ProcessFlRed()->void;
        auto ProcessFlByChannelIndex(const int32_t& channelIndex) -> void;
        auto ProcessPhase()->void;
        auto ProcessBf()->void;

        auto SetProcessedDataHt(const std::shared_ptr<HoloProcessor> holoProcessor)->void;
        auto SetProcessedDataFl(const std::shared_ptr<HoloProcessor> holoProcessor)->void;
        auto SetProcessedDataPhase(const std::shared_ptr<HoloProcessor> holoProcessor)->void;
        auto SetProcessedDataBf(const std::shared_ptr<HoloProcessor> holoProcessor)->void;

        auto ReportProgress(int value) const ->void;
        auto GetTempSubFolderString() const->QString;
        auto TimeFrameIndexString()->QString;

        auto LogStartProcess()->void;
        auto LogEndProcess()->void;
    };
}
