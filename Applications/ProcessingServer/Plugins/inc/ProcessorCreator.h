#pragma once
#include <memory>

#include <QList>

#include "ProcessingServerPluginsExport.h"
#include "Processor.h"

namespace processing_server::Plugins {
    class ProcessingServerPlugins_API ProcessorCreator {
    public:
        static auto Create()->QList<Processor::Pointer>;
    private:
        static auto ScanDeviceCount()->int32_t;
        static auto LogDeviceInfoByIndex(const int32_t& deviceIndex)->void;
        static auto GenerateProcessor(const int32_t deviceIndex)->Processor::Pointer;
    };
}
