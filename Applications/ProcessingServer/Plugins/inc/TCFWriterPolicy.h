#pragma once
#include "ProcessingServerPluginsExport.h"
#include "IWriterPolicy.h"

namespace processing_server::Plugins {
    class ProcessingServerPlugins_API TCFWriterPolicy final : public Entity::IWriterPolicy {
    public:
        TCFWriterPolicy();
        ~TCFWriterPolicy();

        auto SetLimitMemorySizeInBytes(const size_t& limitMemorySizeInBytes) -> void override;
        auto Accept(const size_t& requiredMemorySizeInBytes, const size_t& usedMemorySizeInBytes) -> bool override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
