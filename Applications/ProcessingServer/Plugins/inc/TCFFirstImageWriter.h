#pragma once

#include <memory>

#include <QString>

#include "ProcessingServerPluginsExport.h"

namespace processing_server::Plugins {
    class ProcessingServerPlugins_API TCFFirstImageWriter {
    public:
        TCFFirstImageWriter(const QString& tcfPath);
        ~TCFFirstImageWriter();

        auto Write()->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        struct ImageSizes {
            size_t xSize{};
            size_t ySize{};
        };

        struct HtMipImageRequirements {
            std::shared_ptr<uint16_t[]> data{};
            ImageSizes imageSizes{};
        };

        struct BfImageRequirements {
            std::shared_ptr<uint8_t[]> data{};
            ImageSizes imageSizes{};
        };
    private:
        auto HasHtMip() const ->bool;
        auto HasBf() const ->bool;
        auto HasData(const std::string& groupPath) const ->bool;
        auto LoadFirstHtMipRequirements() const ->HtMipImageRequirements;
        auto LoadFirstBfRequirements() const ->BfImageRequirements;
        auto MakeHtMipPath() const->std::string;
        auto MakeBfPath() const->std::string;
        auto EditPathChangingSuffix(const QString& suffix) const->QString;
        static auto WriteHtFirstMipImage(const std::string& imagePath,
            const HtMipImageRequirements& htMipImageRequirements)->void;
        static auto WriteBfFirstImage(const std::string& imagePath,
            const BfImageRequirements& bfImageRequirements)->void;
    };
}
