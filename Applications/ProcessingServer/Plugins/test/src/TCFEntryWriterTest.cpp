#include <catch2/catch.hpp>

#include "TCFEntryWriter.h"

using namespace processing_server;

namespace _TestTCFEntryWriter {
    namespace _Test {
        class DummyWritingUpdater : public Entity::IProgressUpdater {
        public:
            DummyWritingUpdater() = default;
            ~DummyWritingUpdater() = default;

            auto Update(const Entity::Progress::Parameter& parameter) -> void override {
                
            }
        };
    }

    auto GenerateWritingData()-> Entity::WritingData::Pointer {
        Entity::Job::Info info;
        info.dataPath = "C:/Users/HanghunJo/Desktop/Sample/Comparison/ProcessingServer";
        info.outputPath = "test.h5";

        Entity::Job::Pointer job(new Entity::Job(info));

        const std::any data3d = std::shared_ptr<uint16_t>(new uint16_t[10], std::default_delete<uint16_t[]>());
        const std::any dataMip = std::shared_ptr<uint16_t>(new uint16_t[10], std::default_delete<uint16_t[]>());

        Entity::ProcessedData::Pointer processedData(new Entity::ProcessedData(Entity::DataType::HT));
        processedData->SetData3d(data3d);
        processedData->SetDataMip(dataMip);
        processedData->SetDimension(Entity::ProcessedData::Dimension{ 10,1,1 });

        Entity::WritingData::Pointer writingData(new Entity::WritingData(job, processedData));

        return writingData;
    }


    TEST_CASE("TCFEntryWriter") {
        Plugins::TCFEntryWriter entryWriter;

        _Test::DummyWritingUpdater::Pointer updater(new _Test::DummyWritingUpdater());
        entryWriter.SetUpdater(updater);

        const auto writingData = GenerateWritingData();

        entryWriter.RequestToWrite(writingData);

        while (true);
    }
}