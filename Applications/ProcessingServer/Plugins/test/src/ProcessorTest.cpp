#include <catch2/catch.hpp>
#include <iostream>

#include "Processor.h"

#include <QCoreApplication>
#include <QDir>

using namespace processing_server;

namespace _TestProcessor {
    class DummyUpdater : public Entity::IProgressUpdater {
    public:
        DummyUpdater() = default;
        ~DummyUpdater() = default;
        auto Update(const Entity::Progress::Parameter& parameter) -> void override {
            progress = parameter.completeProgressValue;
        }
    private:
        float progress = 0;
    };

    auto GenerateHtJob(const QString& dataPath) -> Entity::Job::Pointer {
        Entity::Job::Info info;
        info.dataPath = dataPath;
        info.dataType = Entity::DataType::HT;
        info.dataTypeFlag.existence[Entity::DataType::HT] = true;
        info.index = 0;
        info.outputPath = "";
        info.reprocessingFlag = true;
        info.totalFrameNumber = 1;
        info.uniqueID = 1;
        info.deconvolutionFlag = true;

        return std::make_shared<Entity::Job>(info);
    }

    TEST_CASE("Processor HT Memory Leak test") {
        const QString topPath = "E:/00_Data/20200708_HTFL/Crop";

        const QDir dir(topPath);
        auto dirList = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name);

        //SECTION("Processor is not initialized") {
        //    Plugins::Processor processor(Entity::ProcessorType::LOCAL_GPU, Entity::ProcessorPriority::MAIN);
        //    Entity::IProgressUpdater::Pointer updater(new DummyUpdater);
        //    processor.SetUpdater(updater);

        //    int argc = 0;
        //    char** argv = nullptr;

        //    QCoreApplication coreApplication(argc, argv);
        //    auto i = 0;
        //    for (const auto& folder : dirList) {
        //        const auto dataPath = QString("%1/%2").arg(topPath).arg(folder);
        //        const auto job = GenerateHtJob(dataPath);

        //        if (processor.GetStatus()._value == Entity::ProcessorStatus::IDLE) {
        //            processor.SetJobPointer(job);
        //            processor.StartProcess();

        //            std::cout << "Start : " << i;
        //        }


        //        while (true) {
        //            coreApplication.processEvents();
        //            if (processor.GetStatus()._value == Entity::ProcessorStatus::COMPLETE) {
        //                processor.GetProcessedData();
        //                std::cout << " End" << std::endl;
        //                i++;
        //                break;
        //            }
        //        }
        //    }
        //}

        SECTION("Processor is initialized") {

            const auto job = GenerateHtJob(topPath + "/HT_001");
            const Entity::IProgressUpdater::Pointer updater(new DummyUpdater);
            Plugins::Processor processor(Entity::ProcessorType::LOCAL_GPU, Entity::ProcessorPriority::MAIN);
            auto i = 0;
            for (const auto& folder : dirList) {
                processor.SetUpdater(updater);

                const auto dataPath = QString("%1/%2").arg(topPath).arg(folder);
                //const auto job = GenerateHtJob(dataPath);

                if (processor.GetStatus()._value == Entity::ProcessorStatus::IDLE) {
                    processor.SetJobPointer(job);
                    processor.StartProcess();

                    std::cout << "Start : " << i;
                }


                while (true) {
                    if (processor.GetStatus()._value == Entity::ProcessorStatus::COMPLETE) {
                        processor.GetProcessedData();
                        std::cout << " End" << std::endl;
                        i++;
                        break;
                    }
                }
            }
        }




    }
}

