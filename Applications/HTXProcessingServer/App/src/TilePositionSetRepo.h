#pragma once

#include <memory>
#include <QString>

#include "TilePositionSet.h"

class TilePositionSetRepo {
public:
    using Pointer = std::shared_ptr<TilePositionSetRepo>;

    ~TilePositionSetRepo();

    static auto GetInstance()->Pointer;

    auto Clear()->void;

    auto AddHTTilePositionSet(const QString& rootFolderPath, const int32_t& elapsedTime, const TilePositionSet& tilePositionSet)->void;
    auto AddHTVoxelSizeXY(const QString& rootFolderPath, const double& voxelSizeXY)->void;

    auto HTTilePositionSetExists(const QString& rootFolderPath, const int32_t& elapsedTime)->bool;
    auto GetHTTilePositionSet(const QString& rootFolderPath, const int32_t& elapsedTime)->TilePositionSet;
    auto GetHTVoxelSizeXY(const QString& rootFolderPath)const->const double&;

protected:
    TilePositionSetRepo();

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
