#pragma once
#include <memory>
#include <QTableWidget>

#include "SampleObject.h"

class SampleTableWidget final : public QTableWidget {
    Q_OBJECT
public:
    explicit SampleTableWidget(QWidget* parent = nullptr);
    ~SampleTableWidget();
    auto UpdateSampleTableWidget(const QList<SampleObject>& sampleObjectList, const QString& topDirectory, const bool& onProcessing)-> void;
signals:
    void OpenFolder(const int32_t& rowIndex);
private:
    struct Impl;
    std::unique_ptr<Impl> d;
private slots:
    void ShowContextMenu(const QPoint& point);
    void OpenFolderTriggered();
};
