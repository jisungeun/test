#pragma once

#include <memory>
#include <QThread>

#include "SampleObject.h"

class SampleObjectUpdater final : public QThread {
    Q_OBJECT
public:
    explicit SampleObjectUpdater(QObject* parent = nullptr);
    ~SampleObjectUpdater();

    auto SetTopDirectory(const QString& topDirectory)->void;
    auto SetSystemBackgroundFolderPath(const QString& backgroundFolderPath)->void;

    auto GetSampleObjectList()const -> const QList<SampleObject>&;

    auto Start()->void;
    auto Stop()->void;
    auto Close()->void;

    auto IsStopped() const->bool;
protected:
    void run() override;

private slots:
    void WakeThread();

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
