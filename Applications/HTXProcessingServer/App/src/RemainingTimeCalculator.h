#pragma once

#include <memory>

#include "AcquisitionSequenceInfo.h"
#include "RemainingTaskCount.h"
#include "TakenUnitTimeToProcess.h"

class RemainingTimeCalculator {
public:
    RemainingTimeCalculator();
    ~RemainingTimeCalculator();

    auto SetRemainingTaskCount(const RemainingTaskCount& count)->void;
    auto SetTakenUnitTimeToProcess(const TakenUnitTimeToProcess& unitTime)->void;
    auto SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo)->void;


    auto Calculate()->bool;

    auto GetRemainingTime(const TimeUnit& unit)->double;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
