#pragma once

#include <memory>
#include <QString>

#include "AcquisitionConfig.h"

class ProcessorBF {
public:
    ProcessorBF();
    ~ProcessorBF();

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto SetAcquisitionConfig(const AcquisitionConfig& acquisitionConfig)->void;

    auto Process(const int32_t& tileIndex, const int32_t& timeIndex)->bool;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
