#pragma once
#include <memory>
#include <QString>


class IdGenerator {
public:
    IdGenerator(const QString& configFilePath, const QString& timeStampFilePath = "");
    ~IdGenerator();

    auto GenerateDataId() const->QString;
    auto GenerateUniqueId() const->QString;
private:
    struct Impl;
    std::unique_ptr<Impl> d;
};
