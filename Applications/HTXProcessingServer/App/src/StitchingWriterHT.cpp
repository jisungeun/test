#define LOGGER_TAG "[StitchingWriterHT]"

#include "StitchingWriterHT.h"

#include <QDateTime>
#include <QDir>

#include "arrayfire.h"
#include "ElapsedTimeReader.h"
#include "H5Cpp.h"

#include "HDF5Mutex.h"
#include "HTDataSetGetterStitchingResult.h"
#include "PiercedRectangle.h"
#include "PiercedRectangleMerger.h"
#include "RecordingTimeReader.h"
#include "ResultFilePathGenerator.h"
#include "TCLogger.h"
#include "SIUnit.h"
#include "TCFDataSetRecordedPosition.h"
#include "TCFHTSectionDataSetWriter.h"

using namespace TC::TCFWriter;

using SequenceModality = AcquisitionSequenceInfo::Modality;

struct WritingStartIndex {
    int32_t x{};
    int32_t y{};
    int32_t z{};
};

struct CenterPosition {
    int32_t x{};
    int32_t y{};
};

auto ReadHTDataSet(const QString& processedDataFilePath, const QString& stitchingDataFilePath)->TCFHTDataSet {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    float pixelWorldSizeX{}, pixelWorldSizeY{}, pixelWorldSizeZ{};

    const H5::H5File processedDataFile(processedDataFilePath.toStdString(), H5F_ACC_RDONLY);
    const auto processedDataSet = processedDataFile.openDataSet("Data");

    const auto attrPixelWorldSizeX = processedDataSet.openAttribute("pixelWorldSizeX");
    const auto attrPixelWorldSizeY = processedDataSet.openAttribute("pixelWorldSizeY");
    const auto attrPixelWorldSizeZ = processedDataSet.openAttribute("pixelWorldSizeZ");

    attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
    attrPixelWorldSizeY.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeY);
    attrPixelWorldSizeZ.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeZ);

    const H5::H5File stitchingDataFile(stitchingDataFilePath.toStdString(), H5F_ACC_RDONLY);
    const auto stitchingDataSet3D = stitchingDataFile.openDataSet("StitchingData3D");
    const auto stitchingDataSetMIP = stitchingDataFile.openDataSet("StitchingDataMIP");

    int32_t stitchingDataSizeX{}, stitchingDataSizeY{}, stitchingDataSizeZ{};
    const auto attrDataSizeX = stitchingDataSet3D.openAttribute("dataSizeX");
    const auto attrDataSizeY = stitchingDataSet3D.openAttribute("dataSizeY");
    const auto attrDataSizeZ = stitchingDataSet3D.openAttribute("dataSizeZ");

    attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
    attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);
    attrDataSizeZ.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeZ);

    float data3dMinValue{}, data3dMaxValue{}, dataMIPMinValue{}, dataMIPMaxValue{};
    const auto attrData3DMinValue = stitchingDataSet3D.openAttribute("minValue");
    const auto attrData3DMaxValue = stitchingDataSet3D.openAttribute("maxValue");
    const auto attrDataMIPMinValue = stitchingDataSetMIP.openAttribute("minValue");
    const auto attrDataMIPMaxValue = stitchingDataSetMIP.openAttribute("maxValue");

    attrData3DMinValue.read(H5::PredType::NATIVE_FLOAT, &data3dMinValue);
    attrData3DMaxValue.read(H5::PredType::NATIVE_FLOAT, &data3dMaxValue);
    attrDataMIPMinValue.read(H5::PredType::NATIVE_FLOAT, &dataMIPMinValue);
    attrDataMIPMaxValue.read(H5::PredType::NATIVE_FLOAT, &dataMIPMaxValue);

    const IHTDataSetGetter::Pointer htDataSetGetter{ new HTDataSetGetterStitchingResult };
    std::dynamic_pointer_cast<HTDataSetGetterStitchingResult>(htDataSetGetter)->SetResultFilePath(stitchingDataFilePath);

    TCFHTDataSetMetaInfo metaInfo;
    metaInfo.SetDataSize(stitchingDataSizeX, stitchingDataSizeY, stitchingDataSizeZ);
    metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);
    metaInfo.SetDataMinMaxValue(data3dMinValue, data3dMaxValue);
    metaInfo.SetMIPDataMinMaxValue(dataMIPMinValue, dataMIPMaxValue);

    TCFHTDataSet tcfHTDataSet;
    tcfHTDataSet.SetDataSetGetter(htDataSetGetter);
    tcfHTDataSet.SetMetaInfo(metaInfo);

    return tcfHTDataSet;
}

auto ReadHTCenterPosition(const QString& stitchingResultFilePath)->CenterPosition {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);

    int32_t centerPositionX{}, centerPositionY{};

    const auto attCenterPositionX = stitchingDataFile.openAttribute("centerPositionX");
    const auto attCenterPositionY = stitchingDataFile.openAttribute("centerPositionY");

    attCenterPositionX.read(H5::PredType::NATIVE_INT32, &centerPositionX);
    attCenterPositionY.read(H5::PredType::NATIVE_INT32, &centerPositionY);

    return { centerPositionX , centerPositionY };
}

class StitchingWriterHT::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    QString tempTCFFilePath{};
    AcquisitionSequenceInfo acquisitionSequenceInfo{};
    AcquisitionConfig acquisitionConfig{};
    
    auto CalculateStitchingPiercedRect()->PiercedRectangle;
    auto CalculateWritingStartIndex(const int32_t& mergedCenterPositionX, const int32_t& mergedCenterPositionY, 
        const int32_t& centerPositionX, const int32_t& centerPositionY)->WritingStartIndex;
};

auto StitchingWriterHT::Impl::CalculateStitchingPiercedRect() -> PiercedRectangle {
    const auto timeFrameCount = this->acquisitionSequenceInfo.GetTimeFrameCount();
    
    QList<PiercedRectangle> stitchingRectList;
    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(SequenceModality::HT, timeIndex)) {
            continue;
        }

        const auto stitchingResultFilePath = QString("%1/temp/HT/Stitching_time%2").arg(this->rootFolderPath)
            .arg(timeIndex, 4, 10, QChar('0'));
        const auto [centerPositionX, centerPositionY] = ReadHTCenterPosition(stitchingResultFilePath);

        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto stitchingDataSet3D = stitchingDataFile.openDataSet("StitchingData3D");

        int32_t stitchingDataSizeX{}, stitchingDataSizeY{};
        const auto attrDataSizeX = stitchingDataSet3D.openAttribute("dataSizeX");
        const auto attrDataSizeY = stitchingDataSet3D.openAttribute("dataSizeY");

        attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
        attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);

        PiercedRectangle piercedRectangle;
        piercedRectangle.SetSize(stitchingDataSizeX, stitchingDataSizeY);
        piercedRectangle.SetPiercingIndex(centerPositionX, centerPositionY);

        stitchingRectList.push_back(piercedRectangle);
    }

    PiercedRectangleMerger piercedRectangleMerger;
    piercedRectangleMerger.SetPiercedRectangleList(stitchingRectList);
    piercedRectangleMerger.Merge();

    return piercedRectangleMerger.GetMergedPiercedRectangle();
}

auto StitchingWriterHT::Impl::CalculateWritingStartIndex(const int32_t& mergedCenterPositionX, 
    const int32_t& mergedCenterPositionY, const int32_t& centerPositionX, const int32_t& centerPositionY)
    -> WritingStartIndex {

    const auto writingStartIndexX = centerPositionX - mergedCenterPositionX;
    const auto writingStartIndexY = centerPositionY - mergedCenterPositionY;

    return WritingStartIndex{ writingStartIndexX, writingStartIndexY , 0 };
}

StitchingWriterHT::StitchingWriterHT() : d(new Impl()) {
}

StitchingWriterHT::~StitchingWriterHT() = default;

auto StitchingWriterHT::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto StitchingWriterHT::SetTempTCFFilePath(const QString& tempTCFFilePath) -> void {
    d->tempTCFFilePath = tempTCFFilePath;
}

auto StitchingWriterHT::SetAcquisitionConfig(const AcquisitionConfig& acquisitionConfig) -> void {
    d->acquisitionConfig = acquisitionConfig;
}

auto StitchingWriterHT::SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo) -> void {
    d->acquisitionSequenceInfo = acquisitionSequenceInfo;
}

auto StitchingWriterHT::Write(const int32_t& timeIndex) -> bool {
    const auto& acquisitionPosition = d->acquisitionConfig.GetAcquisitionPosition();

    const auto positionX = acquisitionPosition.positionXMillimeter;
    const auto positionY = acquisitionPosition.positionYMillimeter;
    const auto positionZ = acquisitionPosition.positionZMillimeter;
    const auto positionC = acquisitionPosition.positionCMillimeter;
    constexpr auto positionUnit = LengthUnit::Millimenter;

    TCFDataSetRecordedPosition position;
    position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

    RecordingTimeReader recordingTimeReader;
    ElapsedTimeReader elapsedTimeReader;

    const auto stitchingPiercedRect = d->CalculateStitchingPiercedRect();

    QLOG_INFO() << "stitchingWriter HT time(" << timeIndex << ")";

    const QString htProcessedDataFilePath = ResultFilePathGenerator::GetHTProcessedFilePath(d->rootFolderPath, 0, timeIndex);
    const auto stitchingResultFilePath = ResultFilePathGenerator::GetHTStitchingFilePath(d->rootFolderPath, timeIndex);

    const QString recordedTimeFilePath = QString("%1/data/0000/HT3D/%2/acquisition_timestamp.txt")
        .arg(d->rootFolderPath).arg(timeIndex, 4, 10, QChar('0'));
    const QString elapsedTimeFilePath = QString("%1/data/0000/HT3D/%2/logical_timestamp.txt")
        .arg(d->rootFolderPath).arg(timeIndex, 4, 10, QChar('0'));

    recordingTimeReader.SetFilePath(recordedTimeFilePath);
    const auto recordingTimeReadResult = recordingTimeReader.Read();

    elapsedTimeReader.SetFilePath(elapsedTimeFilePath);
    const auto elapsedTimeReadResult = elapsedTimeReader.Read();

    QDateTime recordedTime;
    if (recordingTimeReadResult == true) {
        recordedTime = recordingTimeReader.GetRecordingTime();
    } else {
        recordedTime = QDateTime::currentDateTime();
    }

    double elapsedTime;
    constexpr auto elapsedTimeUnit = TimeUnit::Second;
    if (elapsedTimeReadResult == true) {
        elapsedTime = elapsedTimeReader.GetElapsedTime(elapsedTimeUnit);
    } else {
        elapsedTime = 0;
    }

    const auto tcfDataIndex = d->acquisitionSequenceInfo.GetOrderIndex(SequenceModality::HT, timeIndex);

    auto tcfHTDataSet = ReadHTDataSet(htProcessedDataFilePath, stitchingResultFilePath);

    auto metaInfo = tcfHTDataSet.GetMetaInfo();
    metaInfo.SetRecordedTime(recordedTime);
    metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
    metaInfo.SetTimeFrameIndex(tcfDataIndex);
    metaInfo.SetPosition(position);

    tcfHTDataSet.SetMetaInfo(metaInfo);

    const auto [centerPositionX, centerPositionY] = ReadHTCenterPosition(stitchingResultFilePath);
    const auto [writingStartIndexX, writingStartIndexY, writingStartIndexZ] = 
        d->CalculateWritingStartIndex(stitchingPiercedRect.GetPiercingIndexX(), stitchingPiercedRect.GetPiercingIndexY(), 
            centerPositionX, centerPositionY);

    TCFHTSectionDataSetWriter tcfHTSectionDataSetWriter;
    tcfHTSectionDataSetWriter.SetTargetFilePath(d->tempTCFFilePath);
    tcfHTSectionDataSetWriter.SetTCFHTDataSet(tcfHTDataSet);
    tcfHTSectionDataSetWriter.SetWholeDataSize(stitchingPiercedRect.GetSizeX(), stitchingPiercedRect.GetSizeY(), metaInfo.GetSizeZ());
    tcfHTSectionDataSetWriter.SetWritingStartIndex(writingStartIndexX, writingStartIndexY, writingStartIndexZ);

    if (!tcfHTSectionDataSetWriter.Write()) {
        QLOG_ERROR() << "tcfHTDataSetWriter.Write() fails";
        return false;
    }
    return true;
}
