#pragma once

#include <memory>

#include "SIUnit.h"

class TaskInfo {
public:
    TaskInfo();
    TaskInfo(const TaskInfo& other);
    ~TaskInfo();

    auto operator=(const TaskInfo& other)->TaskInfo&;

    auto SetProcessedTime(const double& processedTime, const TimeUnit& unit)->void;
    auto GetProcessedTime(const TimeUnit& unit)const->double;

    auto SetProcessedProgress(const double& processedProgress)->void;
    auto GetProcessedProgress()const->const double&;

    auto SetOnProcessing(const bool& onProcessing)->void;
    auto GetOnProcessing()const->const bool&;

    auto SetFailed(const bool& failed)->void;
    auto GetFailed()const->const bool&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};

auto operator==(const TaskInfo& taskInfo1, const TaskInfo& taskInfo2)->bool;
auto operator!=(const TaskInfo& taskInfo1, const TaskInfo& taskInfo2)->bool;
