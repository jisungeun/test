#pragma once
#include <memory>
#include <QString>

class TimeStampReader {
public:
    explicit TimeStampReader(const QString& timeStampFilePath);
    ~TimeStampReader();

    auto ReadTimeStampString() const->QString;
private:
    struct Impl;
    std::unique_ptr<Impl> d;
};
