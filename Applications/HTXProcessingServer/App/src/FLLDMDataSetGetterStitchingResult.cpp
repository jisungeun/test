#include "FLLDMDataSetGetterStitchingResult.h"

#include "Hdf5DataSetHybridSamplerReader.h"
#include "StorageOnePickSampler.h"

using namespace TC::IO::LdmCore;
using namespace TC::IO::LdmReading;
using namespace TC::IO::LdmWriting;

class FLLDMDataSetGetterStitchingResult::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    H5::DataSet* srcDataSet{ nullptr };
    H5::Group* destGroup{ nullptr };

    H5::DataSet* srcMIPDataSet{ nullptr };
    H5::Group* destMIPGroup{ nullptr };

    int32_t readingOffsetX{};
    int32_t readingOffsetY{};
    int32_t readingOffsetZ{};

    LdmConfiguration ldmConfiguration3D{};
    LdmConfiguration ldmConfigurationMIP{};
};

FLLDMDataSetGetterStitchingResult::FLLDMDataSetGetterStitchingResult() : d{ std::make_unique<Impl>() } {
}

FLLDMDataSetGetterStitchingResult::~FLLDMDataSetGetterStitchingResult() = default;

auto FLLDMDataSetGetterStitchingResult::SetSourceDataSet(H5::DataSet* dataSet) -> void {
    d->srcDataSet = dataSet;
}

auto FLLDMDataSetGetterStitchingResult::SetDestLDMGroup(H5::Group* group) -> void {
    d->destGroup = group;
}

auto FLLDMDataSetGetterStitchingResult::SetSourceMIPDataSet(H5::DataSet* mipDataSet) -> void {
    d->srcMIPDataSet = mipDataSet;
}

auto FLLDMDataSetGetterStitchingResult::SetDestLDMMIPGroup(H5::Group* mipGroup) -> void {
    d->destMIPGroup = mipGroup;
}

auto FLLDMDataSetGetterStitchingResult::SetReadingOffset(const int32_t& offsetX, const int32_t& offsetY,
    const int32_t& offsetZ) -> void {
    d->readingOffsetX = offsetX;
    d->readingOffsetY = offsetY;
    d->readingOffsetZ = offsetZ;
}

auto FLLDMDataSetGetterStitchingResult::SetLdmConfiguration(const LdmConfiguration& ldmConfiguration3D,
    const LdmConfiguration& ldmConfigurationMIP) -> void {
    d->ldmConfiguration3D = ldmConfiguration3D;
    d->ldmConfigurationMIP = ldmConfigurationMIP;
}

auto FLLDMDataSetGetterStitchingResult::GetTileData(const int32_t& tileIndex) -> std::shared_ptr<float[]> {
    auto samplerReader = std::make_shared<Hdf5DataSetHybridSamplerReader>(*d->srcDataSet, *d->destGroup);
    samplerReader->SetOffsetPoint({ d->readingOffsetX, d->readingOffsetY, d->readingOffsetZ });

    auto storageOnePickSampler = std::make_shared<StorageOnePickSampler>();
    storageOnePickSampler->SetSamplerReader(samplerReader);

    const auto ldmSampledMemoryChunk = storageOnePickSampler->Sample(d->ldmConfiguration3D, tileIndex);

    const auto numberOfElements = ldmSampledMemoryChunk->GetDimension().GetNumberOfElements();

    const auto flTileDataFloat = std::shared_ptr<float[]>{ new float[numberOfElements]() };

    if (ldmSampledMemoryChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type) {
        const auto flTileDataUint16 = static_cast<uint16_t*>(ldmSampledMemoryChunk->GetDataPointer());

        for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
            flTileDataFloat.get()[dataIndex] = static_cast<float>(flTileDataUint16[dataIndex]);
        }
    } else {
        const auto flTileDataUint8 = static_cast<uint8_t*>(ldmSampledMemoryChunk->GetDataPointer());

        for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
            flTileDataFloat.get()[dataIndex] = static_cast<float>(flTileDataUint8[dataIndex]);
        }
    }

    return flTileDataFloat;
}

auto FLLDMDataSetGetterStitchingResult::GetMipTileData(const int32_t& tileIndex) -> std::shared_ptr<float[]> {
    auto samplerReader = std::make_shared<Hdf5DataSetHybridSamplerReader>(*d->srcMIPDataSet, *d->destMIPGroup);
    samplerReader->SetOffsetPoint({ d->readingOffsetX, d->readingOffsetY });

    auto storageOnePickSampler = std::make_shared<StorageOnePickSampler>();
    storageOnePickSampler->SetSamplerReader(samplerReader);

    const auto ldmSampledMemoryChunk = storageOnePickSampler->Sample(d->ldmConfigurationMIP, tileIndex);

    const auto numberOfElements = ldmSampledMemoryChunk->GetDimension().GetNumberOfElements();

    const auto flTileDataFloat = std::shared_ptr<float[]>{ new float[numberOfElements]() };

    if (ldmSampledMemoryChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type) {
        const auto flTileDataUint16 = static_cast<uint16_t*>(ldmSampledMemoryChunk->GetDataPointer());

        for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
            flTileDataFloat.get()[dataIndex] = static_cast<float>(flTileDataUint16[dataIndex]);
        }
    } else {
        const auto flTileDataUint8 = static_cast<uint8_t*>(ldmSampledMemoryChunk->GetDataPointer());

        for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
            flTileDataFloat.get()[dataIndex] = static_cast<float>(flTileDataUint8[dataIndex]);
        }
    }

    return flTileDataFloat;
}
