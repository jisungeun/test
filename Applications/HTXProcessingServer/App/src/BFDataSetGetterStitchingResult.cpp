#include "BFDataSetGetterStitchingResult.h"

#include "H5Cpp.h"

class BFDataSetGetterStitchingResult::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString resultFilePath{};
};

BFDataSetGetterStitchingResult::BFDataSetGetterStitchingResult() : d(std::make_unique<Impl>()) {
}

BFDataSetGetterStitchingResult::~BFDataSetGetterStitchingResult() = default;

auto BFDataSetGetterStitchingResult::SetResultFilePath(const QString& resultFilePath) -> void {
    d->resultFilePath = resultFilePath;
}

auto BFDataSetGetterStitchingResult::GetData() const -> std::shared_ptr<uint8_t[]> {
    const H5::H5File resultFile{ d->resultFilePath.toStdString(),H5F_ACC_RDONLY };

    const auto dataSet = resultFile.openDataSet("StitchingData");
    const auto dataSpace = dataSet.getSpace();

    hsize_t dims[3];
    dataSpace.getSimpleExtentDims(dims);

    const auto dataSizeX = dims[2];
    const auto dataSizeY = dims[1];
    const auto dataSizeZ = dims[0];

    const auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

    const std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements] };
    dataSet.read(data.get(), dataSet.getDataType());

    return data;
}

auto BFDataSetGetterStitchingResult::GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0,
    const int32_t& y1) const -> std::shared_ptr<uint8_t[]> {
    const H5::H5File resultFile{ d->resultFilePath.toStdString(),H5F_ACC_RDONLY };

    const auto dataSet = resultFile.openDataSet("StitchingData");
    const auto wholeDataSpace = dataSet.getSpace();

    hsize_t dims[3];
    wholeDataSpace.getSimpleExtentDims(dims);

    const auto dataSizeX = static_cast<hsize_t>(x1 - x0 + 1);
    const auto dataSizeY = static_cast<hsize_t>(y1 - y0 + 1);
    const auto dataSizeZ = dims[0];
    const auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

    hsize_t count[3] = { dataSizeZ, dataSizeY, dataSizeX };
    hsize_t start[3] = { static_cast<hsize_t>(0), static_cast<hsize_t>(y0), static_cast<hsize_t>(x0) };

    const auto sectionDataSpace = H5::DataSpace{ 3,count };

    const auto selectedDataSpace = wholeDataSpace;
    selectedDataSpace.selectHyperslab(H5S_SELECT_SET, count, start);

    const std::shared_ptr<uint8_t[]> data{ new uint8_t[numberOfElements] };
    dataSet.read(data.get(), dataSet.getDataType(), sectionDataSpace, selectedDataSpace);

    return data;
}

auto BFDataSetGetterStitchingResult::GetColorChannelCount() const -> int32_t {
    return 1;
}

auto BFDataSetGetterStitchingResult::GetDataMemoryOrder() const -> MemoryOrder3D {
    return MemoryOrder3D::XYZ;
}
