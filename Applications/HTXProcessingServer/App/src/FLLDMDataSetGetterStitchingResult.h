#pragma once

#include <memory>

#include <IFLLDMDataSetGetter.h>

#include <H5Cpp.h>

class FLLDMDataSetGetterStitchingResult : public TC::TCFWriter::IFLLDMDataSetGetter {
public:
    FLLDMDataSetGetterStitchingResult();
    ~FLLDMDataSetGetterStitchingResult();

    auto SetSourceDataSet(H5::DataSet* dataSet)->void;
    auto SetDestLDMGroup(H5::Group* group)->void;

    auto SetSourceMIPDataSet(H5::DataSet* mipDataSet)->void;
    auto SetDestLDMMIPGroup(H5::Group* mipGroup)->void;

    auto SetReadingOffset(const int32_t& offsetX, const int32_t& offsetY, const int32_t& offsetZ)->void;

    auto SetLdmConfiguration(const TC::IO::LdmCore::LdmConfiguration& ldmConfiguration3D,
        const TC::IO::LdmCore::LdmConfiguration& ldmConfigurationMIP) -> void override;

    auto GetTileData(const int32_t& tileIndex)->std::shared_ptr<float[]> override;
    auto GetMipTileData(const int32_t& tileIndex)->std::shared_ptr<float[]> override;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};