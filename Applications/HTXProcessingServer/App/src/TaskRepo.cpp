#include "TaskRepo.h"

#include <QMap>
#include <QMutexLocker>
#include <QMutex>

class TaskRepo::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QStringList rootFolderPathAdded{};
    QStringList rootFolderPathProcessAlreadyDone{};
    QStringList rootFolderPathProcessDone{};
    QStringList rootFolderPathProcessNotDone{};
    QStringList rootFolderPathProcessFailed{};
    
    QMap<QString, AcquiredDataFlag> acquiredDataFlagMap{};
    QMap<QString, TaskInfoSet> taskInfoSetMap{};
    QMap<QString, AcquisitionCount> acquisitionCountMap{};

    QMutex mutex{};
};

TaskRepo::~TaskRepo() = default;

auto TaskRepo::GetInstance() -> Pointer {
    static Pointer instance{ new TaskRepo() };
    return instance;
}

auto TaskRepo::AddAlreadyDone(const QString& rootFolderPath) -> void {
    QMutexLocker locker{ &d->mutex };
    d->rootFolderPathAdded.push_back(rootFolderPath);
    d->rootFolderPathProcessAlreadyDone.push_back(rootFolderPath);
}

auto TaskRepo::AddNotDone(const QString& rootFolderPath, const AcquiredDataFlag& acquiredDataFlag,
    const TaskInfoSet& taskInfoSet, const AcquisitionCount& acquisitionCount) -> void {
    QMutexLocker locker{ &d->mutex };
    d->rootFolderPathAdded.push_back(rootFolderPath);
    d->rootFolderPathProcessNotDone.push_back(rootFolderPath);

    d->acquiredDataFlagMap.remove(rootFolderPath);
    d->taskInfoSetMap.remove(rootFolderPath);
    d->acquisitionCountMap.remove(rootFolderPath);

    d->acquiredDataFlagMap[rootFolderPath] = acquiredDataFlag;
    d->taskInfoSetMap[rootFolderPath] = taskInfoSet;
    d->acquisitionCountMap[rootFolderPath] = acquisitionCount;
}

auto TaskRepo::IsAdded(const QString& rootFolderPath) -> bool {
    QMutexLocker locker{ &d->mutex };
    return d->rootFolderPathAdded.contains(rootFolderPath);
}

auto TaskRepo::IsAlreadyDone(const QString& rootFolderPath) -> bool {
    QMutexLocker locker{ &d->mutex };
    return d->rootFolderPathProcessAlreadyDone.contains(rootFolderPath);
}

auto TaskRepo::IsDone(const QString& rootFolderPath) -> bool {
    QMutexLocker locker{ &d->mutex };
    return d->rootFolderPathProcessDone.contains(rootFolderPath);
}

auto TaskRepo::IsFailed(const QString& rootFolderPath) -> bool {
    QMutexLocker locker{ &d->mutex };
    return d->rootFolderPathProcessFailed.contains(rootFolderPath);
}

auto TaskRepo::Update(const QString& rootFolderPath, const AcquiredDataFlag& acquiredDataFlag) -> void {
    QMutexLocker locker{ &d->mutex };
    d->acquiredDataFlagMap.remove(rootFolderPath);
    d->acquiredDataFlagMap[rootFolderPath] = acquiredDataFlag;
}

auto TaskRepo::Update(const QString& rootFolderPath, const TaskInfoSet& taskInfoSet) -> void {
    QMutexLocker locker{ &d->mutex };
    d->taskInfoSetMap.remove(rootFolderPath);
    d->taskInfoSetMap[rootFolderPath] = taskInfoSet;
}

auto TaskRepo::UpdateToDone(const QString& rootFolderPath) -> void {
    QMutexLocker locker{ &d->mutex };
    d->rootFolderPathProcessNotDone.removeAll(rootFolderPath);
    d->rootFolderPathProcessDone.push_back(rootFolderPath);
}

auto TaskRepo::UpdateToFailed(const QString& rootFolderPath) -> void {
    QMutexLocker locker{ &d->mutex };
    d->rootFolderPathProcessNotDone.removeAll(rootFolderPath);
    d->rootFolderPathProcessFailed.push_back(rootFolderPath);
}

auto TaskRepo::GetRootFolderPathList() const -> QStringList {
    QMutexLocker locker{ &d->mutex };
    return d->rootFolderPathAdded;
}

auto TaskRepo::GetAcquiredDataFlag(const QString& rootFolderPath) const -> AcquiredDataFlag {
    QMutexLocker locker{ &d->mutex };
    return d->acquiredDataFlagMap[rootFolderPath];
}

auto TaskRepo::GetTaskInfoSet(const QString& rootFolderPath) const -> TaskInfoSet {
    QMutexLocker locker{ &d->mutex };
    return d->taskInfoSetMap[rootFolderPath];
}

auto TaskRepo::GetAcquisitionCount(const QString& rootFolderPath) const -> AcquisitionCount {
    QMutexLocker locker{ &d->mutex };
    return d->acquisitionCountMap[rootFolderPath];
}

auto TaskRepo::Clear() -> void {
    QMutexLocker locker{ &d->mutex };
    d->rootFolderPathAdded.clear();
    d->rootFolderPathProcessAlreadyDone.clear();
    d->rootFolderPathProcessDone.clear();
    d->rootFolderPathProcessNotDone.clear();
    d->rootFolderPathProcessFailed.clear();

    d->acquiredDataFlagMap.clear();
    d->taskInfoSetMap.clear();
    d->acquisitionCountMap.clear();
}

TaskRepo::TaskRepo() : d(new Impl()) {
}
