#pragma once

#include <memory>
#include <QString>

class PositionIniFileReader {
public:
    PositionIniFileReader();
    ~PositionIniFileReader();

    auto SetIniFilePath(const QString& iniFilePath)->void;
    auto Read()->bool;

    auto GetX()->double;
    auto GetY()->double;
    auto GetZ()->double;
    auto GetFocus()->double;
    auto GetScanStart()->double;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};