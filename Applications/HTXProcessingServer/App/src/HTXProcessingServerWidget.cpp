#define LOGGER_TAG "[HTXProcessingServerWidget]"

#include "HTXProcessingServerWidget.h"

#include <iostream>

#include "Version.h"
#include "HTXProcessingServerWidgetControl.h"
#include <QFileDialog>
#include <QMimeData>
#include <QDropEvent>
#include <QMessageBox>

#include "TCLogger.h"

struct HTXProcessingServerWidget::Impl {
    Impl() = default;
    ~Impl() = default;

    HTXProcessingServerWidgetControl htxProcessingServerWidgetControl{};
    bool changedByUser{ true };
};

HTXProcessingServerWidget::HTXProcessingServerWidget(QWidget* parent)
    : QWidget(parent), d(new Impl()) {
    setupUi(this);
    setAcceptDrops(true);

    auto windowTitle = this->windowTitle();
    windowTitle = QString("%1 - %2").arg(windowTitle).arg(QString(PROJECT_REVISION));
    this->setWindowTitle(windowTitle);

    QLOG_INFO() << windowTitle << " starts";

    connect(topDirectoryButton, SIGNAL(clicked()), this, SLOT(TopDirectoryButtonClicked()));
    connect(processButton, SIGNAL(clicked()), this, SLOT(ProcessButtonClicked()));
    connect(scanPreiodSpinBox, SIGNAL(valueChanged(int)), this, SLOT(ScanPeriodChanged(int)));

    connect(sampleTableWidget, SIGNAL(OpenFolder(const int32_t&)), this, SLOT(OpenSelectedItemFolder(const int32_t&)));
        
    connect(&d->htxProcessingServerWidgetControl, SIGNAL(HumbleObjectIsUpdated(HTXProcessingServerHumbleObject)),
        this, SLOT(UpdateStatus(HTXProcessingServerHumbleObject)));

    UpdateStatus(d->htxProcessingServerWidgetControl.GetHumbleObject());
}

HTXProcessingServerWidget::~HTXProcessingServerWidget() = default;

void HTXProcessingServerWidget::dragEnterEvent(QDragEnterEvent* dragEnterEvent) {
    const auto mimeData = dragEnterEvent->mimeData();
    if (mimeData->hasUrls()) {
        const auto url = mimeData->urls().first();

        if (url.isLocalFile()) {
            const auto path = url.toLocalFile();
            if (QFileInfo(path).isDir()) {
                dragEnterEvent->setDropAction(Qt::DropAction::LinkAction);
                dragEnterEvent->accept();
            }
        }
    }
}

void HTXProcessingServerWidget::dropEvent(QDropEvent* dropEvent) {
    const auto mimeData = dropEvent->mimeData();
    if (mimeData->hasUrls()) {
        const auto url = mimeData->urls().first();

        if (url.isLocalFile()) {
            const auto path = url.toLocalFile();
            if (QFileInfo(path).isDir()) {
                const auto humbleObject = d->htxProcessingServerWidgetControl.TopDirectoryChanged(path);
                UpdateStatus(humbleObject);
            }
        }
    }
}

void HTXProcessingServerWidget::closeEvent(QCloseEvent* event) {
    QMessageBox qMessageBox(this);
    qMessageBox.setWindowTitle(tr("Processing server closing"));
    qMessageBox.setText(tr("Please wait"));
    qMessageBox.show();
    d->htxProcessingServerWidgetControl.CloseWidget();
}

void HTXProcessingServerWidget::TopDirectoryButtonClicked() {
    const QString titleName = tr("Select folder to process");
    const auto topDirectoryPath = QFileDialog::getExistingDirectory(this, titleName);
    if (topDirectoryPath.isEmpty()) return;

    const auto humbleObject = d->htxProcessingServerWidgetControl.TopDirectoryChanged(topDirectoryPath);
    UpdateStatus(humbleObject);
}

void HTXProcessingServerWidget::ProcessButtonClicked() {
    const auto humbleObject = d->htxProcessingServerWidgetControl.ProcessButtonIsClicked();
    UpdateStatus(humbleObject);
}

void HTXProcessingServerWidget::OpenSelectedItemFolder(const int32_t& dataIndex) {
    d->htxProcessingServerWidgetControl.OpenFolder(dataIndex);
}

void HTXProcessingServerWidget::ScanPeriodChanged(int periodInSec) {
    if (d->changedByUser) {
        const auto humbleObject = d->htxProcessingServerWidgetControl.PeriodChanged(periodInSec);
        UpdateStatus(humbleObject);
    }
}

void HTXProcessingServerWidget::UpdateStatus(HTXProcessingServerHumbleObject humbleObject) {
    d->changedByUser = false;

    const auto topDirectory = humbleObject.GetTopDirectory();
    topDirectoryLine->setText(topDirectory);

    const auto isProcessing = humbleObject.GetIsProcessingFlag();

    const auto sampleObjectList = humbleObject.GetSampleObjectList();
    sampleTableWidget->UpdateSampleTableWidget(sampleObjectList, topDirectory, isProcessing);
    if (isProcessing) {
        processButton->setStyleSheet("background-color: rgb(0, 210, 0)"); //green
        processButton->setText("Processing");
        auto font = processButton->font();
        font.setBold(true);
        processButton->setFont(font);

        topDirectoryButton->setDisabled(true);
        scanPreiodSpinBox->setDisabled(true);
    } else {
        processButton->setStyleSheet("background-color: #505F69"); //default
        processButton->setText("Process");
        auto font = processButton->font();
        font.setBold(false);
        processButton->setFont(font);

        topDirectoryButton->setDisabled(false);
        scanPreiodSpinBox->setDisabled(false);
    }

    const auto scanPeriodInSec = humbleObject.GetScanPeriodInSec();
    scanPreiodSpinBox->setValue(static_cast<int32_t>(scanPeriodInSec));

    d->changedByUser = true;
}
