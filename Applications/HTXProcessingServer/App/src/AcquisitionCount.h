#pragma once

#include <memory>
#include <tuple>

#include "AcquisitionSequenceInfo.h"

class AcquisitionCount {
public:
    AcquisitionCount();
    AcquisitionCount(const AcquisitionCount& other);
    ~AcquisitionCount();

    auto operator=(const AcquisitionCount& other)->AcquisitionCount&;

    auto SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo)->void;
    auto GetAcquisitionSequenceInfo()const->AcquisitionSequenceInfo;

    using TileNumberX = int32_t;
    using TileNumberY = int32_t;

    auto SetTileNumber(const TileNumberX& tileNumberX, const TileNumberY& tileNumberY)->void;
    auto GetTileNumber()const->std::tuple<TileNumberX, TileNumberY>;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
