#pragma once

#include <memory>
#include <QString>

#include "AcquisitionConfig.h"

class AcquisitionConfigReader {
public:
    AcquisitionConfigReader();
    ~AcquisitionConfigReader();

    auto SetFilePath(const QString& filePath)->void;
    auto Read()->bool;

    auto GetAcquisitionConfig() const ->const AcquisitionConfig&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};