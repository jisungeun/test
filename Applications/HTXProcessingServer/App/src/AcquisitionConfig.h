#pragma once

#include <memory>
#include <QString>

class AcquisitionConfig {
public:
    struct RGB {
        uint8_t r{};
        uint8_t g{};
        uint8_t b{};
    };

    struct JobInfo {
        QString title{};
        QString userID{};
    };

    struct AcquisitionCount {
        int32_t ht3D{};
        int32_t ht2D{};

        int32_t fl3DCH0{};
        int32_t fl2DCH0{};

        int32_t fl3DCH1{};
        int32_t fl2DCH1{};

        int32_t fl3DCH2{};
        int32_t fl2DCH2{};

        int32_t ht3DZ{};
        int32_t fl3DZ{};

        int32_t bf{};
        int32_t bfChannel{};
    };

    struct AcquisitionSetting {
        double htZStepLengthMicrometer{};
        double flZStepLengthMicrometer{};

        double flCH0ExcitationWaveLengthMicrometer{};
        double flCH1ExcitationWaveLengthMicrometer{};
        double flCH2ExcitationWaveLengthMicrometer{};

        double flCH0EmissionWaveLengthMicrometer{};
        double flCH1EmissionWaveLengthMicrometer{};
        double flCH2EmissionWaveLengthMicrometer{};

        RGB flCH0Color{};
        RGB flCH1Color{};
        RGB flCH2Color{};

        int32_t flCH0Intensity{};
        int32_t flCH1Intensity{};
        int32_t flCH2Intensity{};

        int32_t flCH0ExposureTimeMillisecond{};
        int32_t flCH1ExposureTimeMillisecond{};
        int32_t flCH2ExposureTimeMillisecond{};
    };

    struct AcquisitionPosition {
        int32_t wellIndex{};
        double positionXMillimeter{};
        double positionYMillimeter{};
        double positionZMillimeter{};
        double positionCMillimeter{};
        double flAcquisitionZOffsetMicrometer{};
    };

    struct AcquisitionSize {
        double acquisitionSizeXMicrometer{};
        double acquisitionSizeYMicrometer{};
    };

    struct ImageInfo {
        int32_t roiSizeXPixels{};
        int32_t roiSizeYPixels{};
        int32_t roiOffsetXPixels{};
        int32_t roiOffsetYPixels{};
    };

    struct DeviceInfo {
        double pixelSizeMicrometer{};
        double magnification{};
        double objectiveNA{};
        double condenserNA{};
        double mediumRI{};
        QString deviceSerial{};
        QString deviceHost{};
        QString deviceSoftwareVersion{};
        bool sampleStageEncoderSupported{false};
    };

    struct TileInfo {
        int32_t tileNumberX{};
        int32_t tileNumberY{};
    };

    AcquisitionConfig();
    AcquisitionConfig(const AcquisitionConfig& other);
    ~AcquisitionConfig();

    auto operator=(const AcquisitionConfig& other)->AcquisitionConfig&;

    auto SetJobInfo(const JobInfo& jobInfo)->void;
    auto GetJobInfo()const->const JobInfo&;
    auto IsJobInfoValid()const->const bool&;

    auto SetAcquisitionCount(const AcquisitionCount& acquisitionCount)->void;
    auto GetAcquisitionCount()const->const AcquisitionCount&;
    auto IsAcquisitionCountValid()const->const bool&;

    auto SetAcquisitionSetting(const AcquisitionSetting& acquisitionSetting)->void;
    auto GetAcquisitionSetting()const->const AcquisitionSetting&;
    auto IsAcquisitionSettingValid()const->const bool&;

    auto SetAcquisitionPosition(const AcquisitionPosition& acquisitionPosition)->void;
    auto GetAcquisitionPosition()const->const AcquisitionPosition&;
    auto IsAcquisitionPositionValid()const->const bool&;

    auto SetAcquisitionSize(const AcquisitionSize& acquisitionSize)->void;
    auto GetAcquisitionSize()const->const AcquisitionSize&;
    auto IsAcquisitionSizeValid()const->const bool&;

    auto SetDeviceInfo(const DeviceInfo& deviceInfo)->void;
    auto GetDeviceInfo()const->const DeviceInfo&;
    auto IsDeviceInfoValid()const->const bool&;

    auto SetImageInfo(const ImageInfo& imageInfo)->void;
    auto GetImageInfo()const->const ImageInfo&;
    auto IsImageInfoValid()const->const bool&;

    auto SetTileInfo(const TileInfo& tileInfo)->void;
    auto GetTileInfo()const->const TileInfo&;
    auto IsTileInfoValid()const->const bool&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
