#define LOGGER_TAG "[StitcherBFStaged]"

#include "StitcherBFStaged.h"

#include <QFile>
#include <QStringList>
#include <QMap>

#include <TCLogger.h>

#include <H5Cpp.h>
#include <HDF5Mutex.h>

#include <StitchingWriterHDF5BF.h>
#include <TilePositionCalculatorStaged.h>

#include "HTVoxelSizeReader.h"
#include "ResultFilePathGenerator.h"
#include "TileConfigurationReaderBF.h"
#include "TilePositionSetReader.h"
#include "TileSetGeneratorBF.h"

class StitcherBFStaged::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    int32_t tileNumberX{};
    int32_t tileNumberY{};

    double limitedSizeXInMicrometer{};
    double limitedSizeYInMicrometer{};
    bool sizeLimited{ false };

    auto GetProcessedDataFilePathList(const int32_t& tileNumber, const int32_t& timeFrameIndex)->QStringList;
    auto GetPositionFilePathList(const int32_t& tileNumber)->QStringList;
    auto MergeToMap(const QStringList& processedDataFilePathList, const QStringList& positionFilePathList)->QMap<QString, QString>;
    auto ReadVoxelSizeXY(const QString& processedDataFilePath)->double;
    auto CalculateBFTilePositionMap(const TilePositionSet& htTilePositionSet,
        const double& htResolutionXY, const double& bfResolutionXY)->TilePositionSet;
};


auto StitcherBFStaged::Impl::GetProcessedDataFilePathList(const int32_t& tileNumber,
    const int32_t& timeFrameIndex) -> QStringList {
    QStringList processedDataFilePathList{};
    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        const QString processedDataFilePath =
            ResultFilePathGenerator::GetBFProcessedFilePath(this->rootFolderPath, tileIndex, timeFrameIndex);
        processedDataFilePathList.push_back(processedDataFilePath);
    }

    return processedDataFilePathList;
}

auto StitcherBFStaged::Impl::GetPositionFilePathList(const int32_t& tileNumber) -> QStringList {
    QStringList positionFilePathList{};

    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        auto positionFilePath = QString("%1/data/%2/position.ini").arg(this->rootFolderPath).arg(tileIndex, 4, 10, QChar('0'));
        if (!QFile::exists(positionFilePath)) {
            positionFilePath = QString("%1/data/%2/position.ini").arg(this->rootFolderPath).arg(tileIndex);
        }
        positionFilePathList.push_back(positionFilePath);
    }

    return positionFilePathList;
}

auto StitcherBFStaged::Impl::MergeToMap(const QStringList& processedDataFilePathList,
    const QStringList& positionFilePathList) -> QMap<QString, QString> {
    const auto numberOfData = processedDataFilePathList.size();

    QMap<QString, QString> dataPositionFilePathMap{};
    for (auto index = 0; index < numberOfData; ++index) {
        dataPositionFilePathMap[processedDataFilePathList.at(index)] = positionFilePathList.at(index);
    }

    return dataPositionFilePathMap;
}

auto StitcherBFStaged::Impl::ReadVoxelSizeXY(const QString& processedDataFilePath) -> double {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    float pixelWorldSizeX{};

    const H5::H5File file(processedDataFilePath.toStdString(), H5F_ACC_RDONLY);
    const auto dataSet = file.openDataSet("Data");
    const auto attrPixelWorldSizeX = dataSet.openAttribute("pixelWorldSizeX");
    attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);

    return pixelWorldSizeX;
}

auto StitcherBFStaged::Impl::CalculateBFTilePositionMap(const TilePositionSet& htTilePositionSet,
    const double& htResolutionXY, const double& bfResolutionXY) -> TilePositionSet {
    const auto htToBFRatio = htResolutionXY / bfResolutionXY;

    TilePositionSet bfTilePositionSet;
    bfTilePositionSet.SetTileNumber(this->tileNumberX, this->tileNumberY);
    for (auto tileIndexX = 0; tileIndexX < this->tileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < this->tileNumberY; ++tileIndexY) {
            const auto tilePosition = htTilePositionSet.GetTilePosition(tileIndexX, tileIndexY);

            const auto calculatedTilePositionX = std::round(tilePosition.GetTilePositionX() * htToBFRatio);
            const auto calculatedTilePositionY = std::round(tilePosition.GetTilePositionY() * htToBFRatio);
            const auto calculatedTilePositionZ = std::round(tilePosition.GetTilePositionZ() * 1);

            TilePosition calculatedTilePosition;
            calculatedTilePosition.SetPositions(calculatedTilePositionX, calculatedTilePositionY, calculatedTilePositionZ);

            bfTilePositionSet.InsertTilePosition(tileIndexX, tileIndexY, calculatedTilePosition);
        }
    }

    return bfTilePositionSet;
}

StitcherBFStaged::StitcherBFStaged() : d{ std::make_unique<Impl>() } {
}

StitcherBFStaged::~StitcherBFStaged() {
}

auto StitcherBFStaged::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto StitcherBFStaged::SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY) -> void {
    d->tileNumberX = tileNumberX;
    d->tileNumberY = tileNumberY;
}

auto StitcherBFStaged::SetLimitSize(const double& sizeXInMicrometer, const double& sizeYInMicrometer) -> void {
    d->limitedSizeXInMicrometer = sizeXInMicrometer;
    d->limitedSizeYInMicrometer = sizeYInMicrometer;

    d->sizeLimited = true;
}

auto StitcherBFStaged::Stitch(const int32_t& timeIndex) -> bool {
    const auto tileNumber = d->tileNumberX * d->tileNumberY;
    const auto positionFilePathList = d->GetPositionFilePathList(tileNumber);

    const QString writingTempFolderPath = d->rootFolderPath + "/temp/BF";

    QLOG_INFO() << "BF Staged Stitcher time(" << timeIndex << ")";

    const auto processedDataFilePathList = d->GetProcessedDataFilePathList(tileNumber, timeIndex);

    TileConfigurationReaderBF tileConfigurationReaderBF;
    tileConfigurationReaderBF.SetPositionFileList(positionFilePathList);
    tileConfigurationReaderBF.SetProcessedFilePathList(processedDataFilePathList);

    tileConfigurationReaderBF.Read();

    const auto tileConfiguration = tileConfigurationReaderBF.GetTileConfiguration();

    const auto dataPositionFilePathMap = d->MergeToMap(processedDataFilePathList, positionFilePathList);

    TileSetGeneratorBF tileSetGeneratorBF;
    tileSetGeneratorBF.SetTileConfiguration(tileConfiguration);
    tileSetGeneratorBF.SetDataPositionFilePathMap(dataPositionFilePathMap);
    tileSetGeneratorBF.Generate();

    const auto tileSet = tileSetGeneratorBF.GetTileSet();

    auto htTilePositionSetExists = true;
    const auto htProcessingFilePath = ResultFilePathGenerator::GetHTProcessedFilePath(d->rootFolderPath, 0, timeIndex);
    HTVoxelSizeReader htVoxelSizeReader;
    if (QFile::exists(htProcessingFilePath)) {
        htVoxelSizeReader.SetProcessedFilePath(htProcessingFilePath);
        const auto voxelSizeReadResult = htVoxelSizeReader.Read();

        htTilePositionSetExists &= voxelSizeReadResult;
    }

    const auto htStitchingFilePath = ResultFilePathGenerator::GetHTStitchingFilePath(d->rootFolderPath, timeIndex);
    TilePositionSetReader tilePositionSetReader;
    if (QFile::exists(htStitchingFilePath)) {
        tilePositionSetReader.SetTargetFilePath(htStitchingFilePath);
        const auto tilePositionSetReadResult = tilePositionSetReader.Read();

        htTilePositionSetExists &= tilePositionSetReadResult;
    }

    const auto bfVoxelSizeXY = d->ReadVoxelSizeXY(processedDataFilePathList.first());

    TilePositionSet tilePositionSet;
    if (htTilePositionSetExists) {
        const auto htTilePositionSet = tilePositionSetReader.GetTilePositionSet();
        const auto htVoxelSizeXY = htVoxelSizeReader.GetVoxelSizeXY();

        tilePositionSet = d->CalculateBFTilePositionMap(htTilePositionSet, htVoxelSizeXY, bfVoxelSizeXY);
    } else {
        TilePositionCalculatorStaged tilePositionCalculatorStaged;
        tilePositionCalculatorStaged.SetTileConfiguration(tileConfiguration);
        tilePositionSet = tilePositionCalculatorStaged.Calculate();
    }
    
    const auto stitchingResultFilePath =
        ResultFilePathGenerator::GetBFStitchingFilePath(d->rootFolderPath, timeIndex);

    StitchingWriterHDF5BF stitchingWriterHdf5BF;
    stitchingWriterHdf5BF.SetTileSet(tileSet);
    stitchingWriterHdf5BF.SetTilePositionSet(tilePositionSet);
    stitchingWriterHdf5BF.SetTileConfiguration(tileConfiguration);
    stitchingWriterHdf5BF.SetHDF5FilePath(stitchingResultFilePath);
    stitchingWriterHdf5BF.SetBoundaryCropFlag(true);
    if (d->sizeLimited == true) {
        const auto limitedSizeX = static_cast<int32_t>(std::round(d->limitedSizeXInMicrometer / bfVoxelSizeXY));
        const auto limitedSizeY = static_cast<int32_t>(std::round(d->limitedSizeYInMicrometer / bfVoxelSizeXY));

        stitchingWriterHdf5BF.SetDataSizeLimit(limitedSizeX, limitedSizeY);
    }
    if (!stitchingWriterHdf5BF.Run()) { return false; }

    return true;
}
