#pragma once
#include <memory>
#include <QString>
#include "IImageIntegrityChecker.h"

class ImageIntegrityCheckerPng final : public IImageIntegrityChecker {
public:
    ImageIntegrityCheckerPng();
    ~ImageIntegrityCheckerPng();

    auto SetImageFolderPath(const QString& imageFolderPath)->void;
    auto SetNumberOfImages(const uint32_t& numberOfImages)->void;

    auto CheckSameCount() -> bool override;
    auto CheckMoreThanCount()->bool;
private:
    struct Impl;
    std::unique_ptr<Impl> d;
};
