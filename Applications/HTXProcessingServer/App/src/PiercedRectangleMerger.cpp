#include "PiercedRectangleMerger.h"

class PiercedRectangleMerger::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QList<PiercedRectangle> piercedRectangleList{};

    PiercedRectangle mergedPiercedRectangle{};
};

PiercedRectangleMerger::PiercedRectangleMerger() : d(new Impl()) {
}

PiercedRectangleMerger::~PiercedRectangleMerger() = default;

auto PiercedRectangleMerger::SetPiercedRectangleList(const QList<PiercedRectangle>& piercedRectangleList) -> void {
    d->piercedRectangleList = piercedRectangleList;
}

auto PiercedRectangleMerger::Merge() -> bool {
    if (d->piercedRectangleList.isEmpty()) {
        return false;
    }

    int32_t minLeftPixelCount{ std::numeric_limits<int32_t>::max() };
    int32_t minUpPixelCount{ std::numeric_limits<int32_t>::max() }; 
    int32_t minRightPixelCount{ std::numeric_limits<int32_t>::max() };
    int32_t minDownPixelCount{ std::numeric_limits<int32_t>::max() };

    for (const auto& piercedRectangle : d->piercedRectangleList) {
        const auto& rectSizeX = piercedRectangle.GetSizeX();
        const auto& rectSizeY = piercedRectangle.GetSizeY();

        const auto& piercingIndexX = piercedRectangle.GetPiercingIndexX();
        const auto& piercingIndexY = piercedRectangle.GetPiercingIndexY();

        const auto leftPixelCount = piercingIndexX;
        const auto rightPixelCount = rectSizeX - piercingIndexX - 1;
        const auto upPixelCount = piercingIndexY;
        const auto downPixelCount = rectSizeY - piercingIndexY - 1;

        minLeftPixelCount = std::min(minLeftPixelCount, leftPixelCount);
        minRightPixelCount = std::min(minRightPixelCount, rightPixelCount);
        minUpPixelCount = std::min(minUpPixelCount, upPixelCount);
        minDownPixelCount = std::min(minDownPixelCount, downPixelCount);
    }

    const auto mergedRectSizeX = minLeftPixelCount + minRightPixelCount + 1;
    const auto mergedRectSizeY = minUpPixelCount + minDownPixelCount + 1;

    const auto mergedRectPiercingIndexX = minLeftPixelCount;
    const auto mergedRectPiercingIndexY = minUpPixelCount;

    PiercedRectangle mergedPiercedRectangle;
    mergedPiercedRectangle.SetSize(mergedRectSizeX, mergedRectSizeY);
    mergedPiercedRectangle.SetPiercingIndex(mergedRectPiercingIndexX, mergedRectPiercingIndexY);

    d->mergedPiercedRectangle = mergedPiercedRectangle;

    return true;
}

auto PiercedRectangleMerger::GetMergedPiercedRectangle() const -> const PiercedRectangle& {
    return d->mergedPiercedRectangle;
}
