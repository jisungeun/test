#include "TimeStampReader.h"

#include <QTextStream>
#include <QFile>
#include <QDateTime>

struct TimeStampReader::Impl {
    Impl() = default;
    ~Impl() = default;

    QString timeStampFilePath{};

    static auto ReadTimeStampFile(const QString& timeStampFilePath)->QString;
    static auto TimeStampNumberToString(const uint64_t& timeStampNumber)->QString;
    static auto CheckFileValidity(const QString& filePath)->bool;
};

auto TimeStampReader::Impl::ReadTimeStampFile(const QString& timeStampFilePath) -> QString {
    QFile timeStampFile(timeStampFilePath);
    timeStampFile.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream in(&timeStampFile);
    const auto line = in.readLine();

    return line;
}

auto TimeStampReader::Impl::TimeStampNumberToString(const uint64_t& timeStampNumber) -> QString {
    const auto rawString = QString::number(timeStampNumber);

    const auto year = rawString.mid(0, 4);
    const auto month = rawString.mid(4, 2);
    const auto day = rawString.mid(6, 2);
    const auto hour = rawString.mid(8, 2);
    const auto minute = rawString.mid(10, 2);
    const auto second = rawString.mid(12, 2);
    const auto millisecond = rawString.mid(14, 3);

    const QDate date(year.toInt(), month.toInt(), day.toInt());
    const QTime time(hour.toInt(), minute.toInt(), second.toInt(), millisecond.toInt());
    const QDateTime dateTime(date, time);

    return dateTime.toString("yyyy-MM-dd hh:mm:ss.zzz");
}

auto TimeStampReader::Impl::CheckFileValidity(const QString& filePath) -> bool {
    auto fileIsValid{ false };

    const auto pathIsNotEmtpy = !filePath.isEmpty();
    const auto fileExists = QFile::exists(filePath);

    if (pathIsNotEmtpy && fileExists) {
        fileIsValid = true;
    }

    return fileIsValid;
}

TimeStampReader::TimeStampReader(const QString& timeStampFilePath)
    : d(new Impl()) {
    d->timeStampFilePath = timeStampFilePath;
}

TimeStampReader::~TimeStampReader() = default;

auto TimeStampReader::ReadTimeStampString() const -> QString {
    QString timeStampString;
    if (d->CheckFileValidity(d->timeStampFilePath)) {
        const auto timeStamp = d->ReadTimeStampFile(d->timeStampFilePath);
        const auto timeStampNumber = timeStamp.toULongLong();
        timeStampString = d->TimeStampNumberToString(timeStampNumber);
    } else {
        timeStampString = "1997-01-01 00:00:00.000";
    }
    return timeStampString;
}
