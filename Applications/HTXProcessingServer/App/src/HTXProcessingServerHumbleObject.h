#pragma once
#include <memory>

#include <QString>
#include <QList>

#include "enum.h"
#include "SampleObject.h"

class HTXProcessingServerHumbleObject {
public:
    HTXProcessingServerHumbleObject();
    HTXProcessingServerHumbleObject(const HTXProcessingServerHumbleObject& other);
    ~HTXProcessingServerHumbleObject();

    auto operator=(const HTXProcessingServerHumbleObject& other)->HTXProcessingServerHumbleObject&;

    auto SetSampleObjectList(const QList<SampleObject>& sampleObjectList)->void;
    auto SetTopDirectory(const QString& topDirectory)->void;
    auto SetSystemBackgroundFolderPath(const QString& systemBackgroundFolderPath)->void;
    auto SetIsProcessingFlag(const bool& isProcessingFlag)->void;
    auto SetScanPeriodInSec(const uint32_t& scanPeriodInSec)->void;
    auto SetPsfFolderPath(const QString& psfFolderPath)->void;
    auto SetMatlabModuleFolderPath(const QString& matlabModuleFolderPath)->void;

    auto GetSampleObjectList() const->QList<SampleObject>;
    auto GetTopDirectory() const ->QString;
    auto GetSystemBackgroundFolderPath() const->QString;
    auto GetIsProcessingFlag()const->bool;
    auto GetScanPeriodInSec()const->uint32_t;
    auto GetPsfFolderPath()const->const QString&;
    auto GetMatlabModuleFolderPath()const->const QString&;

private:
    struct Impl;
    std::unique_ptr<Impl> d;
};
