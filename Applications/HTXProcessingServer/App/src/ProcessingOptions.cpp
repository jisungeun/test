#include "ProcessingOptions.h"

class ProcessingOptions::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    bool ldmFlag{ false };
};

ProcessingOptions::ProcessingOptions() : d{ std::make_unique<Impl>() } {
}

ProcessingOptions::ProcessingOptions(const ProcessingOptions& other) : d{ std::make_unique<Impl>(*other.d) } {
}

auto ProcessingOptions::operator=(const ProcessingOptions& other) -> ProcessingOptions& {
    *(this->d) = *(other.d);
    return *this;
}

ProcessingOptions::~ProcessingOptions() = default;

auto ProcessingOptions::SetLDMFlag(const bool& ldmFlag) -> void {
    d->ldmFlag = ldmFlag;
}

auto ProcessingOptions::GetLDMFlag() const -> bool {
    return d->ldmFlag;
}
