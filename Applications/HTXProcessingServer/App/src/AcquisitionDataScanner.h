#pragma once

#include <memory>

#include "AcquiredDataFlag.h"
#include "AcquisitionDataInfo.h"

class AcquisitionDataScanner {
public:
    AcquisitionDataScanner();
    ~AcquisitionDataScanner();

    auto SetAcquisitionDataInfo(const AcquisitionDataInfo& acquisitionDataInfo)->void;
    auto Scan()->bool;
    auto GetAcquiredDataFlag()const -> const AcquiredDataFlag&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
