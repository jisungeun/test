#include "MultiModalProcessorThread.h"

#include <QDir>
#include <QMutexLocker>
#include <QTimer>
#include <QWaitCondition>
#include <chrono>

#include "H5Cpp.h"

#include "HDF5Mutex.h"

#include <LicenseManager.h>

#include "ModalityType.h"

#include "MultiModalProcessor.h"
#include "ProcessType.h"
#include "TaskRepo.h"
#include "ResultFilePathGenerator.h"
#include "TCFSkeletonConfig.h"
#include "TCFSkeletonWriter.h"
#include "NextProcessTaskFinder.h"
#include "ProcessTask.h"

#include "Version.h"

#include "RegistryVerifier.h"

using SequenceModality = AcquisitionSequenceInfo::Modality;

struct MultiModalProcessorThread::Impl {
    Impl() = default;
    ~Impl() = default;

    QString psfFolderPath{};
    QString systemBackgroundFolderPath{};

    QString matlabModuleFolderPath{};

    QMutex mutex;
    QWaitCondition waitCondition;
    bool runFlag{ true };
    bool processStopFlag{ false };
    
    bool processingIsStopped{ false };

    auto ToSec(const std::chrono::time_point<std::chrono::system_clock>& start, const std::chrono::time_point<std::chrono::system_clock>& end)->double;
    auto WriteTempFolder(const QString& rootFolderPath)->void;
    auto GetKeepRawDataFlag()const->bool;
};

auto MultiModalProcessorThread::Impl::ToSec(const std::chrono::time_point<std::chrono::system_clock>& start,
    const std::chrono::time_point<std::chrono::system_clock>& end) -> double {
    const auto duration = end - start;
    return static_cast<double>(std::chrono::duration_cast<std::chrono::milliseconds>(duration).count()) / 1000;
}

auto MultiModalProcessorThread::Impl::WriteTempFolder(const QString& rootFolderPath) -> void {
    const auto tempFolderPath = ResultFilePathGenerator::GetTempFolderPath(rootFolderPath);

    if(!QFile::exists(tempFolderPath)) {
        QDir().mkdir(tempFolderPath);
    }
}

auto MultiModalProcessorThread::Impl::GetKeepRawDataFlag()const->bool {
    const auto keepRawImageByLicenseFlag = LicenseManager::CheckFeature("Misc-Keep_Raw_Images");

    const QString organization{"Tomocube, Inc."};
    const QString application{"TomoStudioX"};
    const QString path{"signatures/service"};
    const QString key{ PROJECT_REVISION };
    TC::RegistryVerifier verifier(organization, application, path, key);
    
    const auto keepRawImageByServiceEngineer = verifier.Verify();

    const auto keepRawImage = keepRawImageByLicenseFlag || keepRawImageByServiceEngineer;
    return keepRawImage;
}

MultiModalProcessorThread::MultiModalProcessorThread(QObject* parent)
    : d(new Impl()), QThread(parent) {
    start();
}

MultiModalProcessorThread::~MultiModalProcessorThread() {
    Close();
    wait();
}

auto MultiModalProcessorThread::Reset() -> void {
    QMutexLocker locker(&d->mutex);
    d->psfFolderPath.clear();
    d->matlabModuleFolderPath.clear();
    d->systemBackgroundFolderPath.clear();
    d->processStopFlag = false;
}

auto MultiModalProcessorThread::Stop() -> void {
    QMutexLocker locker(&d->mutex);
    d->processStopFlag = true;
}

auto MultiModalProcessorThread::Close() -> void {
    d->mutex.lock();
    d->runFlag = false;
    d->waitCondition.wakeOne();
    d->mutex.unlock();
}

auto MultiModalProcessorThread::TriggerProcess() -> void {
    QMutexLocker locker(&d->mutex);
    d->waitCondition.wakeOne();
}

auto MultiModalProcessorThread::IsStopped() const -> bool {
    QMutexLocker locker(&d->mutex);
    return d->processingIsStopped;
}

auto MultiModalProcessorThread::SetPsfFolderPath(const QString& psfFolderPath) -> void {
    d->psfFolderPath = psfFolderPath;
}

auto MultiModalProcessorThread::SetSystemBackgroundFolderPath(const QString& systemBackgroundFolderPath) -> void {
    d->systemBackgroundFolderPath = systemBackgroundFolderPath;
}

auto MultiModalProcessorThread::SetMatlabModuleFolderPath(const QString& matlabModuleFolderPath) -> void {
    d->matlabModuleFolderPath = matlabModuleFolderPath;
}

void MultiModalProcessorThread::run() {
    while (true) {
        QMutexLocker locker(&d->mutex);

        if (d->runFlag) {
            d->processingIsStopped = true;
            d->waitCondition.wait(locker.mutex());
            d->processingIsStopped = false;
            if (!d->runFlag) {
                break;
            }
        } else {
            break;
        }
        locker.unlock();
        Process();
    }
}

auto MultiModalProcessorThread::Process() -> void {
    if (d->processStopFlag == true) { return; }

    NextProcessTaskFinder nextProcessTaskFinder;
    if (!nextProcessTaskFinder.Find()) { return; }
    if (!nextProcessTaskFinder.NextProcessTaskExists()) { return; }

    const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();
    const auto& rootFolderPath = nextProcessTask.GetRootFolderPath();

    MultiModalProcessor multiModalProcessor;
    multiModalProcessor.SetMatlabModuleFolderPath(d->matlabModuleFolderPath);
    multiModalProcessor.SetPsfFolderPath(d->psfFolderPath);
    multiModalProcessor.SetSystemBackgroundFolderPath(d->systemBackgroundFolderPath);
    multiModalProcessor.SetRootFolderPath(rootFolderPath);

    auto taskRepoInstance = TaskRepo::GetInstance();
    auto taskInfoSet = taskRepoInstance->GetTaskInfoSet(rootFolderPath);

    const auto acquisitionCount = taskRepoInstance->GetAcquisitionCount(rootFolderPath);
    const auto acquisitionSequenceInfo = acquisitionCount.GetAcquisitionSequenceInfo();

    d->WriteTempFolder(rootFolderPath);

    const auto& modalityType = nextProcessTask.GetModalityType();
    const auto& processType = nextProcessTask.GetProcessType();
    const auto& timeIndex = nextProcessTask.GetTimeFrameIndex();
    const auto& tileIndex = nextProcessTask.GetTileIndex();

    const auto modalityName = modalityType.GetName();

    multiModalProcessor.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);
    multiModalProcessor.Initialize();
    if (modalityName == ModalityType::Name::HT) {
        auto taskInfo = taskInfoSet.GetTaskInfo(timeIndex, tileIndex, modalityType, processType);
        taskInfo.SetOnProcessing(true);
        taskInfoSet.SetTaskInfo(timeIndex, tileIndex, modalityType, processType, taskInfo);
        taskRepoInstance->Update(rootFolderPath, taskInfoSet);

        bool processResult = false;
        const auto start = std::chrono::system_clock::now();
        if (processType == +ProcessType::TileProcessing) {
            processResult = multiModalProcessor.HTProcess(tileIndex, timeIndex);
        } else if (processType == +ProcessType::Stitching) {
            processResult = multiModalProcessor.HTStitch(timeIndex);
            multiModalProcessor.HTWriteThumbnail(timeIndex, acquisitionSequenceInfo);
        } else if (processType == +ProcessType::StitchingWriting) {
            processResult = multiModalProcessor.HTStitchWrite(timeIndex, acquisitionSequenceInfo);
        } else if (processType == +ProcessType::LDMConversion) {
            processResult = true;
        }
        const auto end = std::chrono::system_clock::now();
        const auto processingTime = d->ToSec(start, end);

        taskInfo.SetOnProcessing(false);

        if (processResult) {
            taskInfo.SetProcessedProgress(1);
            taskInfo.SetProcessedTime(processingTime, TimeUnit::Second);
            taskInfo.SetFailed(false);
            taskInfoSet.SetTaskInfo(timeIndex, tileIndex, modalityType, processType, taskInfo);
            taskRepoInstance->Update(rootFolderPath, taskInfoSet);
        } else {
            taskInfo.SetFailed(true);
            taskInfoSet.SetTaskInfo(timeIndex, tileIndex, modalityType, processType, taskInfo);
            taskRepoInstance->Update(rootFolderPath, taskInfoSet);
            taskRepoInstance->UpdateToFailed(rootFolderPath);
        }

    } else if (modalityName == ModalityType::Name::FL) {
        int32_t lastFLChannelIndex{ -1 };
        QList<int32_t> flChannelIndexList;
        for (auto flChannelIndex = 0; flChannelIndex < 3; ++flChannelIndex) {
            const auto flSequenceModality = ConvertSequenceModality(flChannelIndex);
            if (acquisitionSequenceInfo.AcquisitionExists(flSequenceModality, timeIndex)) {
                lastFLChannelIndex = flChannelIndex;
                flChannelIndexList.push_back(flChannelIndex);
            }
        }

        const auto flChannelIndex = modalityType.GetChannelIndex();

        auto taskInfo = taskInfoSet.GetTaskInfo(timeIndex, tileIndex, modalityType, processType);
        taskInfo.SetOnProcessing(true);
        taskInfoSet.SetTaskInfo(timeIndex, tileIndex, modalityType, processType, taskInfo);
        taskRepoInstance->Update(rootFolderPath, taskInfoSet);
        
        bool processResult = false;
        const auto start = std::chrono::system_clock::now();
        if (processType == +ProcessType::TileProcessing) {
            processResult = multiModalProcessor.FLProcess(flChannelIndex, tileIndex, timeIndex);
        } else if (processType == +ProcessType::Stitching) {
            processResult = multiModalProcessor.FLStitch(flChannelIndex, timeIndex);
            if (lastFLChannelIndex == flChannelIndex) {
                multiModalProcessor.FLWriteThumbnail(flChannelIndexList, timeIndex, acquisitionSequenceInfo);
            }
        } else if (processType == +ProcessType::StitchingWriting) {
            processResult = multiModalProcessor.FLStitchWrite(flChannelIndex, timeIndex, acquisitionSequenceInfo);
        } else if (processType == +ProcessType::LDMConversion) {
            processResult = true;
        }
        const auto end = std::chrono::system_clock::now();
        const auto processingTime = d->ToSec(start, end);

        taskInfo.SetOnProcessing(false);

        if (processResult) {
            taskInfo.SetProcessedProgress(1);
            taskInfo.SetProcessedTime(processingTime, TimeUnit::Second);
            taskInfo.SetFailed(false);
            taskInfoSet.SetTaskInfo(timeIndex, tileIndex, modalityType, processType, taskInfo);
            taskRepoInstance->Update(rootFolderPath, taskInfoSet);
        }
        else {
            taskInfo.SetFailed(true);
            taskInfoSet.SetTaskInfo(timeIndex, tileIndex, modalityType, processType, taskInfo);
            taskRepoInstance->Update(rootFolderPath, taskInfoSet);
            taskRepoInstance->UpdateToFailed(rootFolderPath);
        }

    } else if (modalityName == ModalityType::Name::BF) {
        auto taskInfo = taskInfoSet.GetTaskInfo(timeIndex, tileIndex, modalityType, processType);
        taskInfo.SetOnProcessing(true);
        taskInfoSet.SetTaskInfo(timeIndex, tileIndex, modalityType, processType, taskInfo);
        taskRepoInstance->Update(rootFolderPath, taskInfoSet);

        bool processResult = false;
        const auto start = std::chrono::system_clock::now();
        if (processType == +ProcessType::TileProcessing) {
            processResult = multiModalProcessor.BFProcess(tileIndex, timeIndex);

        } else if (processType == +ProcessType::Stitching) {
            processResult = multiModalProcessor.BFStitch(timeIndex);
            multiModalProcessor.BFWriteThumbnail(timeIndex, acquisitionSequenceInfo);
        } else if (processType == +ProcessType::StitchingWriting) {
            processResult = multiModalProcessor.BFStitchWrite(timeIndex, acquisitionSequenceInfo);
        } else if (processType == +ProcessType::LDMConversion) {
            processResult = true;
        }
        const auto end = std::chrono::system_clock::now();
        const auto processingTime = d->ToSec(start, end);

        taskInfo.SetOnProcessing(false);

        if (processResult) {
            taskInfo.SetProcessedProgress(1);
            taskInfo.SetProcessedTime(processingTime, TimeUnit::Second);
            taskInfo.SetFailed(false);
            taskInfoSet.SetTaskInfo(timeIndex, tileIndex, modalityType, processType, taskInfo);
            taskRepoInstance->Update(rootFolderPath, taskInfoSet);
        } else {
            taskInfo.SetFailed(true);
            taskInfoSet.SetTaskInfo(timeIndex, tileIndex, modalityType, processType, taskInfo);
            taskRepoInstance->Update(rootFolderPath, taskInfoSet);
            taskRepoInstance->UpdateToFailed(rootFolderPath);
        }
    } else if (processType == +ProcessType::TCFWriting) {
        auto tcfWrittenTaskInfo = taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::None }, ProcessType::TCFWriting);
        const auto tcfWrittenProcessedProgress = tcfWrittenTaskInfo.GetProcessedProgress();

        if (tcfWrittenProcessedProgress != 1) {
            tcfWrittenTaskInfo.SetOnProcessing(true);
            taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::None }, ProcessType::TCFWriting, tcfWrittenTaskInfo);
            taskRepoInstance->Update(rootFolderPath, taskInfoSet);

            const auto start = std::chrono::system_clock::now();
            const auto tcfWritingResult = multiModalProcessor.TCFWrite(acquisitionSequenceInfo);
            const auto end = std::chrono::system_clock::now();
            const auto writingTime = d->ToSec(start, end);

            tcfWrittenTaskInfo.SetOnProcessing(false);

            if (tcfWritingResult) {
                tcfWrittenTaskInfo.SetProcessedProgress(1);
                tcfWrittenTaskInfo.SetProcessedTime(writingTime, TimeUnit::Second);
                tcfWrittenTaskInfo.SetFailed(false);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::None }, ProcessType::TCFWriting, tcfWrittenTaskInfo);
                taskRepoInstance->Update(rootFolderPath, taskInfoSet);
            }
            else {
                tcfWrittenTaskInfo.SetFailed(true);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::None }, ProcessType::TCFWriting, tcfWrittenTaskInfo);
                taskRepoInstance->Update(rootFolderPath, taskInfoSet);
                taskRepoInstance->UpdateToFailed(rootFolderPath);
            }
        }
    } else if (processType == +ProcessType::DataRemoving) {
        const QString thumbnailFolderPath = ResultFilePathGenerator::GetThumbnailFolderPath(rootFolderPath);
        //QDir(thumbnailFolderPath).removeRecursively();

        const QString tempFolderPath = ResultFilePathGenerator::GetTempFolderPath(rootFolderPath);
        QDir(tempFolderPath).removeRecursively();

        const auto keepRawImage = d->GetKeepRawDataFlag();
        if (!keepRawImage) {
            const QString dataFolderPath = rootFolderPath + "/data";
            QDir(dataFolderPath).removeRecursively();
        }
        taskRepoInstance->UpdateToDone(rootFolderPath);
    }
}
