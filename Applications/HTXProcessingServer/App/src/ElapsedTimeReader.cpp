#include "ElapsedTimeReader.h"

#include <QFile>
#include <QTextStream>

class ElapsedTimeReader::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString filePath{};

    double elapsedTime{};
    TimeUnit timeUnit{ TimeUnit::Second };
};

ElapsedTimeReader::ElapsedTimeReader() : d(std::make_unique<Impl>()) {
}

ElapsedTimeReader::~ElapsedTimeReader() = default;

auto ElapsedTimeReader::SetFilePath(const QString& filePath) -> void {
    d->filePath = filePath;
}

auto ElapsedTimeReader::Read() -> bool {
    if (d->filePath.isEmpty()) {
        return false;
    }
    if (!QFile::exists(d->filePath)) {
        return false;
    }

    QFile file(d->filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return false;
    }

    QTextStream inputStream(&file);
    QString timeText = inputStream.readLine();

    d->elapsedTime = timeText.toDouble();
    d->timeUnit = TimeUnit::Second;

    return true;
}

auto ElapsedTimeReader::GetElapsedTime(const TimeUnit& unit) const -> double {
    return ConvertUnit(d->elapsedTime, d->timeUnit, unit);
}
