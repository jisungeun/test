#define LOGGER_TAG "[StitcherFLStaged]"

#include "StitcherFLStaged.h"

#include <QFile>

#include <TCLogger.h>

#include <H5Cpp.h>
#include <HDF5Mutex.h>

#include <StitchingWriterHDF5FL.h>
#include <TilePositionCalculatorStaged.h>

#include "HTVoxelSizeReader.h"
#include "ResultFilePathGenerator.h"
#include "TileConfigurationReaderFL.h"
#include "TilePositionSetReader.h"
#include "TileSetGeneratorFL.h"

class StitcherFLStaged::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    int32_t channelIndex{};

    int32_t tileNumberX{};
    int32_t tileNumberY{};

    double limitedSizeXInMicrometer{};
    double limitedSizeYInMicrometer{};
    bool sizeLimited{ false };

    auto GetPositionFilePathList(const int32_t& tileNumber)->QStringList;
    auto GetProcessedDataFilePathList(const int32_t& tileNumber, const int32_t& timeFrameIndex, const int32_t& channelIndex)->QStringList;
    auto MergeToMap(const QStringList& processedDataFilePathList, const QStringList& positionFilePathList)->QMap<QString, QString>;
    auto ReadVoxelSizeXY(const QString& processedDataFilePath)->double;
    auto CalculateFLTilePositionMap(const TilePositionSet& htTilePositionSet, 
        const double& htResolutionXY, const double& flResolutionXY)->TilePositionSet;
};

auto StitcherFLStaged::Impl::GetPositionFilePathList(const int32_t& tileNumber) -> QStringList {
    QStringList positionFilePathList{};

    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        auto positionFilePath = QString("%1/data/%2/position.ini").arg(this->rootFolderPath).arg(tileIndex, 4, 10, QChar('0'));
        if (!QFile::exists(positionFilePath)) {
            positionFilePath = QString("%1/data/%2/position.ini").arg(this->rootFolderPath).arg(tileIndex);
        }

        positionFilePathList.push_back(positionFilePath);
    }

    return positionFilePathList;
}

auto StitcherFLStaged::Impl::GetProcessedDataFilePathList(const int32_t& tileNumber, const int32_t& timeFrameIndex,
    const int32_t& channelIndex) -> QStringList {
    const QString writingTempFolderPath = this->rootFolderPath + "/temp/FL";

    QStringList processedDataFilePathList{};

    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        const auto processedDataFilePath = QString("%1/fl_Processed_CH%2_tile%3_time%4")
            .arg(writingTempFolderPath).arg(channelIndex).arg(tileIndex, 4, 10, QChar('0'))
            .arg(timeFrameIndex, 4, 10, QChar('0'));

        processedDataFilePathList.push_back(processedDataFilePath);
    }

    return processedDataFilePathList;
}

auto StitcherFLStaged::Impl::MergeToMap(const QStringList& processedDataFilePathList,
    const QStringList& positionFilePathList) -> QMap<QString, QString> {
    const auto numberOfData = processedDataFilePathList.size();

    QMap<QString, QString> dataPositionFilePathMap{};
    for (auto index = 0; index < numberOfData; ++index) {
        dataPositionFilePathMap[processedDataFilePathList.at(index)] = positionFilePathList.at(index);
    }

    return dataPositionFilePathMap;
}

auto StitcherFLStaged::Impl::ReadVoxelSizeXY(const QString& processedDataFilePath) -> double {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    float pixelWorldSizeX{};

    const H5::H5File file(processedDataFilePath.toStdString(), H5F_ACC_RDONLY);
    const auto dataSet = file.openDataSet("Data");
    const auto attrPixelWorldSizeX = dataSet.openAttribute("pixelWorldSizeX");
    attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);

    return pixelWorldSizeX;
}

auto StitcherFLStaged::Impl::CalculateFLTilePositionMap(const TilePositionSet& htTilePositionSet,
    const double& htResolutionXY, const double& flResolutionXY) -> TilePositionSet {
    const auto htToFLRatio = htResolutionXY / flResolutionXY;

    TilePositionSet flTilePositionSet;
    flTilePositionSet.SetTileNumber(this->tileNumberX, this->tileNumberY);
    for (auto tileIndexX = 0; tileIndexX < this->tileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < this->tileNumberY; ++tileIndexY) {
            const auto tilePosition = htTilePositionSet.GetTilePosition(tileIndexX, tileIndexY);

            const auto calculatedTilePositionX = std::round(tilePosition.GetTilePositionX() * htToFLRatio);
            const auto calculatedTilePositionY = std::round(tilePosition.GetTilePositionY() * htToFLRatio);
            const auto calculatedTilePositionZ = std::round(tilePosition.GetTilePositionZ() * 1);

            TilePosition calculatedTilePosition;
            calculatedTilePosition.SetPositions(calculatedTilePositionX, calculatedTilePositionY, calculatedTilePositionZ);

            flTilePositionSet.InsertTilePosition(tileIndexX, tileIndexY, calculatedTilePosition);
        }
    }

    return flTilePositionSet;
}

StitcherFLStaged::StitcherFLStaged() : d{ std::make_unique<Impl>() } {
}

StitcherFLStaged::~StitcherFLStaged() {
}

auto StitcherFLStaged::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto StitcherFLStaged::SetChannelIndex(const int32_t& channelIndex) -> void {
    d->channelIndex = channelIndex;
}

auto StitcherFLStaged::SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY) -> void {
    d->tileNumberX = tileNumberX;
    d->tileNumberY = tileNumberY;
}

auto StitcherFLStaged::SetLimitSize(const double& sizeXInMicrometer, const double& sizeYInMicrometer) -> void {
    d->limitedSizeXInMicrometer = sizeXInMicrometer;
    d->limitedSizeYInMicrometer = sizeYInMicrometer;

    d->sizeLimited = true;
}

auto StitcherFLStaged::Stitch(const int32_t& timeIndex) -> bool {
    const auto tileNumber = d->tileNumberX * d->tileNumberY;

    const auto positionFilePathList = d->GetPositionFilePathList(tileNumber);

    const QString writingTempFolderPath = d->rootFolderPath + "/temp/FL";

    QLOG_INFO() << "FL Staged Stitcher time(" << timeIndex << ")" << "CH" << d->channelIndex;

    const auto processedDataFilePathList = d->GetProcessedDataFilePathList(tileNumber, timeIndex, d->channelIndex);

    TileConfigurationReaderFL tileConfigurationReaderFL;
    tileConfigurationReaderFL.SetPositionFileList(positionFilePathList);
    tileConfigurationReaderFL.SetProcessedFilePathList(processedDataFilePathList);

    tileConfigurationReaderFL.Read();

    const auto tileConfiguration = tileConfigurationReaderFL.GetTileConfiguration();

    const auto dataPositionFilePathMap = d->MergeToMap(processedDataFilePathList, positionFilePathList);

    TileSetGeneratorFL tileSetGeneratorFL;
    tileSetGeneratorFL.SetTileConfiguration(tileConfiguration);
    tileSetGeneratorFL.SetDataPositionFilePathMap(dataPositionFilePathMap);
    tileSetGeneratorFL.Generate();

    const auto tileSet = tileSetGeneratorFL.GetTileSet();

    auto htTilePositionSetExists = true;
    const auto htProcessingFilePath = ResultFilePathGenerator::GetHTProcessedFilePath(d->rootFolderPath, 0, timeIndex);
    HTVoxelSizeReader htVoxelSizeReader;
    if (QFile::exists(htProcessingFilePath)) {
        htVoxelSizeReader.SetProcessedFilePath(htProcessingFilePath);
        const auto voxelSizeReadResult = htVoxelSizeReader.Read();

        htTilePositionSetExists &= voxelSizeReadResult;
    }

    const auto htStitchingFilePath = ResultFilePathGenerator::GetHTStitchingFilePath(d->rootFolderPath, timeIndex);
    TilePositionSetReader tilePositionSetReader;
    if (QFile::exists(htStitchingFilePath)) {
        tilePositionSetReader.SetTargetFilePath(htStitchingFilePath);
        const auto tilePositionSetReadResult = tilePositionSetReader.Read();

        htTilePositionSetExists &= tilePositionSetReadResult;
    }

    const auto flVoxelSizeXY = d->ReadVoxelSizeXY(processedDataFilePathList.first());

    TilePositionSet tilePositionSet;
    if (htTilePositionSetExists) {
        const auto htTilePositionSet = tilePositionSetReader.GetTilePositionSet();
        const auto htVoxelSizeXY = htVoxelSizeReader.GetVoxelSizeXY();

        tilePositionSet = d->CalculateFLTilePositionMap(htTilePositionSet, htVoxelSizeXY, flVoxelSizeXY);
    } else {
        TilePositionCalculatorStaged tilePositionCalculatorStaged;
        tilePositionCalculatorStaged.SetTileConfiguration(tileConfiguration);
        tilePositionSet = tilePositionCalculatorStaged.Calculate();
    }

    const auto stitchingResultFilePath =
        ResultFilePathGenerator::GetFLStitchingFilePath(d->rootFolderPath, d->channelIndex, timeIndex);

    StitchingWriterHDF5FL stitchingWriterHdf5FL;
    stitchingWriterHdf5FL.SetTileSet(tileSet);
    stitchingWriterHdf5FL.SetTilePositionSet(tilePositionSet);
    stitchingWriterHdf5FL.SetTileConfiguration(tileConfiguration);
    stitchingWriterHdf5FL.SetHDF5FilePath(stitchingResultFilePath);
    stitchingWriterHdf5FL.SetBoundaryCropFlag(true);
    if (d->sizeLimited == true) {
        const auto limitedSizeX = static_cast<int32_t>(std::round(d->limitedSizeXInMicrometer / flVoxelSizeXY));
        const auto limitedSizeY = static_cast<int32_t>(std::round(d->limitedSizeYInMicrometer / flVoxelSizeXY));

        stitchingWriterHdf5FL.SetDataSizeLimit(limitedSizeX, limitedSizeY);
    }
    if (!stitchingWriterHdf5FL.Run()) { return false; }

    return true;
}
