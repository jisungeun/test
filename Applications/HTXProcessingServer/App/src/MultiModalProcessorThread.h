#pragma once

#include <QThread>

class MultiModalProcessorThread final : public QThread {
public:
    explicit MultiModalProcessorThread(QObject* parent = nullptr);
    ~MultiModalProcessorThread();

    auto Reset()->void;
    auto Stop()->void;
    auto Close()->void;

    auto TriggerProcess()->void;

    auto IsStopped() const ->bool;

    auto SetPsfFolderPath(const QString& psfFolderPath)->void;
    auto SetSystemBackgroundFolderPath(const QString& systemBackgroundFolderPath)->void;

    auto SetMatlabModuleFolderPath(const QString& matlabModuleFolderPath)->void;

protected:
    void run() override;
private:
    struct Impl;
    std::shared_ptr<Impl> d;
private:
    auto Process()->void;
};
