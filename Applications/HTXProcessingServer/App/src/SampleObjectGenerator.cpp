#include "SampleObjectGenerator.h"

#include "ModalityType.h"
#include "ProcessType.h"
#include "TaskRepo.h"
#include "RemainingTimeCalculator.h"

using SequenceModality = AcquisitionSequenceInfo::Modality;

constexpr double defaultHTProcessingTimePerSlice = 2.5;
constexpr double defaultHTStitchingTimePerTileSlice = 0.25;
constexpr double defaultHTStitchingWrittenTimePerTileSlice = 0.25;
constexpr double defaultHTLDMConvertingTimePerTileSlice = 0; // Change

constexpr double defaultFLProcessingTimePerSlice = 0.5;
constexpr double defaultFLStitchingTimePerTileSlice = 0.5;
constexpr double defaultFLStitchingWrittenTimePerTileSlice = 0.5;
constexpr double defaultFLLDMConvertingTimePerTileSlice = 0; // Change

constexpr double defaultBFProcessingTimePerSlice = 0.5;
constexpr double defaultBFStitchingTimePerTileSlice = 0.5;
constexpr double defaultBFStitchingWrittenTimePerTileSlice = 0.5;
constexpr double defaultBFLDMConvertingTimePerTileSlice = 0; // Change

constexpr auto second = TimeUnit::Second;

static bool isGenerating = false;

auto GetAverageTime(const double& sum, const double& count, const double& defaultValue)->double {
    if (sum == 0) {
        return defaultValue;
    }

    if (count == 0) {
        return defaultValue;
    }

    return sum / count;
}

auto CalculateRemainingTimeInSec(const TaskInfoSet& taskInfoSet, const AcquisitionCount& acquisitionCount)->double {
    using ZSliceCount = int32_t;
    
    const auto [tileNumberX, tileNumberY] = acquisitionCount.GetTileNumber();
    const auto tileNumber = tileNumberX * tileNumberY;

    RemainingTaskCount remainingTaskCount;
    TakenUnitTimeToProcess takenUnitTimeToProcess;

    const auto acquisitionSequenceInfo = acquisitionCount.GetAcquisitionSequenceInfo();
    const auto& timeFrameCount = acquisitionSequenceInfo.GetTimeFrameCount();

    QList<ZSliceCount> htZSliceCountList, flZSliceCountList, bfZSliceCountList;
    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        const auto htZSliceCount =
            acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::HT, timeIndex);
        if (htZSliceCount > 0) {
            if (!htZSliceCountList.contains(htZSliceCount)) {
                htZSliceCountList.push_back(htZSliceCount);
            }
        }

        const auto flCh0ZSliceCount =
            acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::FLCH0, timeIndex);
        if (flCh0ZSliceCount > 0) {
            if (!flZSliceCountList.contains(flCh0ZSliceCount)) {
                flZSliceCountList.push_back(flCh0ZSliceCount);
            }
        }

        const auto flCh1ZSliceCount =
            acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::FLCH1, timeIndex);
        if (flCh1ZSliceCount > 0) {
            if (!flZSliceCountList.contains(flCh1ZSliceCount)) {
                flZSliceCountList.push_back(flCh1ZSliceCount);
            }
        }

        const auto flCh2ZSliceCount =
            acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::FLCH2, timeIndex);
        if (flCh2ZSliceCount > 0) {
            if (!flZSliceCountList.contains(flCh2ZSliceCount)) {
                flZSliceCountList.push_back(flCh2ZSliceCount);
            }
        }

        const auto bfZSliceCount =
            acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::BF, timeIndex);
        if (bfZSliceCount > 0) {
            if (!bfZSliceCountList.contains(bfZSliceCount)) {
                bfZSliceCountList.push_back(bfZSliceCount);
            }
        }
    }

    {
        QMap<ZSliceCount, double> htProcessedTimeSum{}, htStitchingTimeSum{}, htStitchingWrittenTimeSum{}, htLDMConvertingTimeSum{};
        QMap<ZSliceCount, int32_t> htProcessedTimeNumber{}, htStitchingTimeNumber{}, htStitchingWrittenTimeNumber{}, htLDMConvertingTimeNumber{};
        QMap<ZSliceCount, int32_t> htProcessingRemainCount{}, htStitchingRemainCount{}, htStitchingWrittenRemainCount{}, htLDMConvertingRemainCount{};

        for (const auto& zSliceCount : htZSliceCountList) {
            htProcessedTimeSum[zSliceCount] = 0;
            htStitchingTimeSum[zSliceCount] = 0;
            htStitchingWrittenTimeSum[zSliceCount] = 0;
            htLDMConvertingTimeSum[zSliceCount] = 0;

            htProcessedTimeNumber[zSliceCount] = 0;
            htStitchingTimeNumber[zSliceCount] = 0;
            htStitchingWrittenTimeNumber[zSliceCount] = 0;
            htLDMConvertingTimeNumber[zSliceCount] = 0;

            htProcessingRemainCount[zSliceCount] = 0;
            htStitchingRemainCount[zSliceCount] = 0;
            htStitchingWrittenRemainCount[zSliceCount] = 0;
            htLDMConvertingRemainCount[zSliceCount] = 0;
        }

        for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
            if (!acquisitionSequenceInfo.AcquisitionExists(SequenceModality::HT, timeIndex)) {
                continue;
            }

            const auto zSliceCount = 
                acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::HT, timeIndex);

            for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
                const auto htProcessedTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, tileIndex, 
                    ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing);
                const auto htProcessedTime = htProcessedTaskInfo.GetProcessedTime(second);

                if (htProcessedTaskInfo.GetProcessedProgress() == 1) {
                    if (htProcessedTime > 0) {
                        htProcessedTimeSum[zSliceCount] += htProcessedTime;
                        htProcessedTimeNumber[zSliceCount]++;
                    }
                } else {
                    htProcessingRemainCount[zSliceCount]++;
                }
            }

            const auto htStitchingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, 
                ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching);
            const auto htStitchingTime = htStitchingTaskInfo.GetProcessedTime(second);

            if (htStitchingTaskInfo.GetProcessedProgress() == 1) {
                if (htStitchingTime > 0) {
                    htStitchingTimeSum[zSliceCount] += htStitchingTime;
                    htStitchingTimeNumber[zSliceCount]++;
                } else {
                    htStitchingRemainCount[zSliceCount]++;
                }
            }

            const auto htStitchingWrittenTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, 
                ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting);
            const auto htStitchingWrittenTime = htStitchingWrittenTaskInfo.GetProcessedTime(second);

            if (htStitchingWrittenTaskInfo.GetProcessedProgress() == 1) {
                if (htStitchingWrittenTime > 0) {
                    htStitchingWrittenTimeSum[zSliceCount] += htStitchingWrittenTime;
                    htStitchingWrittenTimeNumber[zSliceCount]++;
                } else {
                    htStitchingWrittenRemainCount[zSliceCount]++;
                }
            }

            const auto htLDMConvertingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion);
            const auto htLDMConvertingTime = htLDMConvertingTaskInfo.GetProcessedTime(second);

            if (htLDMConvertingTaskInfo.GetProcessedProgress() == 1) {
                if (htLDMConvertingTime > 0) {
                    htLDMConvertingTimeSum[zSliceCount] += htLDMConvertingTime;
                    htLDMConvertingTimeNumber[zSliceCount]++;
                }
                htLDMConvertingRemainCount[zSliceCount]++;
            }
        }
        for (const auto& zSliceCount : htZSliceCountList) {
            remainingTaskCount.SetHTProcessingCount(zSliceCount, htProcessingRemainCount[zSliceCount]);
            remainingTaskCount.SetHTStitchingCount(zSliceCount, htStitchingRemainCount[zSliceCount]);
            remainingTaskCount.SetHTStitchingWrittenCount(zSliceCount, htStitchingWrittenRemainCount[zSliceCount]);
            remainingTaskCount.SetHTLDMConvertingCount(zSliceCount, htLDMConvertingRemainCount[zSliceCount]);

            const auto averageProcessedTime = GetAverageTime(htProcessedTimeSum[zSliceCount], htProcessedTimeNumber[zSliceCount], defaultHTProcessingTimePerSlice * zSliceCount);
            const auto averageStitchingTime = GetAverageTime(htStitchingTimeSum[zSliceCount], htStitchingTimeNumber[zSliceCount], defaultHTStitchingTimePerTileSlice * tileNumber * zSliceCount);
            const auto averageStitchingWrittenTime = GetAverageTime(htStitchingWrittenTimeSum[zSliceCount], htStitchingWrittenTimeNumber[zSliceCount], defaultHTStitchingWrittenTimePerTileSlice * tileNumber * zSliceCount);
            const auto averageLDMConvertingTime = GetAverageTime(htLDMConvertingTimeSum[zSliceCount], htLDMConvertingTimeNumber[zSliceCount], defaultHTLDMConvertingTimePerTileSlice * tileNumber * zSliceCount);

            takenUnitTimeToProcess.SetHTProcessingTime(zSliceCount, averageProcessedTime, second);
            takenUnitTimeToProcess.SetHTStitchingTime(zSliceCount, averageStitchingTime, second);
            takenUnitTimeToProcess.SetHTStitchingWrittenTime(zSliceCount, averageStitchingWrittenTime, second);
            takenUnitTimeToProcess.SetHTLDMConvertingTime(zSliceCount, averageLDMConvertingTime, second);
        }
    }

    {
        QMap<int32_t, double> flProcessedTimeSum{}, flStitchingTimeSum{}, flStitchingWrittenTimeSum{}, flLDMConvertingTimeSum{};
        QMap<int32_t, int32_t> flProcessedTimeNumber{}, flStitchingTimeNumber{}, flStitchingWrittenTimeNumber{}, flLDMConvertingTimeNumber{};
        QMap<int32_t, int32_t> flProcessingRemainCount{}, flStitchingRemainCount{}, flStitchingWrittenRemainCount{}, flLDMConvertingRemainCount{};

        for (const auto& zSliceCount : flZSliceCountList) {
            flProcessedTimeSum[zSliceCount] = 0;
            flStitchingTimeSum[zSliceCount] = 0;
            flStitchingWrittenTimeSum[zSliceCount] = 0;
            flLDMConvertingTimeSum[zSliceCount] = 0;

            flProcessedTimeNumber[zSliceCount] = 0;
            flStitchingTimeNumber[zSliceCount] = 0;
            flStitchingWrittenTimeNumber[zSliceCount] = 0;
            flLDMConvertingTimeNumber[zSliceCount] = 0;

            flProcessingRemainCount[zSliceCount] = 0;
            flStitchingRemainCount[zSliceCount] = 0;
            flStitchingWrittenRemainCount[zSliceCount] = 0;
            flLDMConvertingRemainCount[zSliceCount] = 0;
        }

        for (auto flChannelIndex = 0; flChannelIndex < 3; ++flChannelIndex) {
            const auto flSequenceModality = ConvertSequenceModality(flChannelIndex);
            for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
                if (!acquisitionSequenceInfo.AcquisitionExists(flSequenceModality, timeIndex)) {
                    continue;
                }

                const auto zSliceCount = acquisitionSequenceInfo.GetAcquisitionZSliceCount(flSequenceModality, timeIndex);

                for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
                    const auto flProcessedTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, tileIndex,
                        ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::TileProcessing);
                    const auto flProcessedTime = flProcessedTaskInfo.GetProcessedTime(second);

                    if (flProcessedTaskInfo.GetProcessedProgress() == 1) {
                        if (flProcessedTime > 0) {
                            flProcessedTimeSum[zSliceCount] += flProcessedTime;
                            flProcessedTimeNumber[zSliceCount]++;
                        }
                    } else {
                        flProcessingRemainCount[zSliceCount]++;
                    }
                }

                const auto flStitchingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                    ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::Stitching);
                const auto flStitchingTime = flStitchingTaskInfo.GetProcessedTime(second);

                if (flStitchingTaskInfo.GetProcessedProgress() == 1) {
                    if (flStitchingTime > 0) {
                        flStitchingTimeSum[zSliceCount] += flStitchingTime;
                        flStitchingTimeNumber[zSliceCount]++;
                    }
                } else {
                    flStitchingRemainCount[zSliceCount]++;
                }

                const auto flStitchingWrittenTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                    ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::StitchingWriting);
                const auto flStitchingWrittenTime = flStitchingWrittenTaskInfo.GetProcessedTime(second);

                if (flStitchingWrittenTaskInfo.GetProcessedProgress() == 1) {
                    if (flStitchingWrittenTime > 0) {
                        flStitchingWrittenTimeSum[zSliceCount] += flStitchingWrittenTime;
                        flStitchingWrittenTimeNumber[zSliceCount]++;
                    }
                } else {
                    flStitchingWrittenRemainCount[zSliceCount]++;
                }

                const auto flLDMConvertingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                    ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::LDMConversion);
                const auto flLDMConvertingTime = flLDMConvertingTaskInfo.GetProcessedTime(second);

                if (flLDMConvertingTaskInfo.GetProcessedProgress() == 1) {
                    if (flLDMConvertingTime > 0) {
                        flLDMConvertingTimeSum[zSliceCount] += flLDMConvertingTime;
                        flLDMConvertingTimeNumber[zSliceCount]++;
                    }
                } else {
                    flLDMConvertingRemainCount[zSliceCount]++;
                }
            }
        }

        for (const auto& zSliceCount : flZSliceCountList) {
            remainingTaskCount.SetFLProcessingCount(zSliceCount, flProcessingRemainCount[zSliceCount]);
            remainingTaskCount.SetFLStitchingCount(zSliceCount, flStitchingRemainCount[zSliceCount]);
            remainingTaskCount.SetFLStitchingWrittenCount(zSliceCount, flStitchingWrittenRemainCount[zSliceCount]);
            remainingTaskCount.SetFLLDMConvertingCount(zSliceCount, flLDMConvertingRemainCount[zSliceCount]);

            const auto averageProcessedTime = GetAverageTime(flProcessedTimeSum[zSliceCount], flProcessedTimeNumber[zSliceCount], defaultFLProcessingTimePerSlice * zSliceCount);
            const auto averageStitchingTime = GetAverageTime(flStitchingTimeSum[zSliceCount], flStitchingTimeNumber[zSliceCount], defaultFLStitchingTimePerTileSlice * zSliceCount);
            const auto averageStitchingWrittenTime = GetAverageTime(flStitchingWrittenTimeSum[zSliceCount], flStitchingWrittenTimeNumber[zSliceCount], defaultFLStitchingWrittenTimePerTileSlice * tileNumber * zSliceCount);
            const auto averageLDMConvertingTime = GetAverageTime(flLDMConvertingTimeSum[zSliceCount], flLDMConvertingTimeNumber[zSliceCount], defaultFLLDMConvertingTimePerTileSlice * tileNumber * zSliceCount);

            takenUnitTimeToProcess.SetFLProcessingTime(zSliceCount, averageProcessedTime, second);
            takenUnitTimeToProcess.SetFLStitchingTime(zSliceCount, averageStitchingTime, second);
            takenUnitTimeToProcess.SetFLStitchingWrittenTime(zSliceCount, averageStitchingWrittenTime, second);
            takenUnitTimeToProcess.SetFLLDMConvertingTime(zSliceCount, averageLDMConvertingTime, second);
        }
    }

    {
        QMap<ZSliceCount, double> bfProcessedTimeSum{}, bfStitchingTimeSum{}, bfStitchingWrittenTimeSum{}, bfLDMConvertingTimeSum{};
        QMap <ZSliceCount, int32_t> bfProcessedTimeNumber{}, bfStitchingTimeNumber{}, bfStitchingWrittenTimeNumber{}, bfLDMConvertingTimeNumber{};
        QMap <ZSliceCount, int32_t> bfProcessingRemainCount{}, bfStitchingRemainCount{}, bfStitchingWrittenRemainCount{}, bfLDMConvertingRemainCount{};

        for (const auto& zSliceCount : htZSliceCountList) {
            bfProcessedTimeSum[zSliceCount] = 0;
            bfStitchingTimeSum[zSliceCount] = 0;
            bfStitchingWrittenTimeSum[zSliceCount] = 0;
            bfLDMConvertingTimeSum[zSliceCount] = 0;

            bfProcessedTimeNumber[zSliceCount] = 0;
            bfStitchingTimeNumber[zSliceCount] = 0;
            bfStitchingWrittenTimeNumber[zSliceCount] = 0;
            bfLDMConvertingTimeNumber[zSliceCount] = 0;

            bfProcessingRemainCount[zSliceCount] = 0;
            bfStitchingRemainCount[zSliceCount] = 0;
            bfStitchingWrittenRemainCount[zSliceCount] = 0;
            bfLDMConvertingRemainCount[zSliceCount] = 0;
        }

        for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
            if (!acquisitionSequenceInfo.AcquisitionExists(SequenceModality::BF, timeIndex)) {
                continue;
            }
            const auto zSliceCount = acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::BF, timeIndex);

            for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
                const auto bfProcessedTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, tileIndex, 
                    ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing);
                const auto bfProcessedTime = bfProcessedTaskInfo.GetProcessedTime(second);

                if (bfProcessedTaskInfo.GetProcessedProgress() == 1) {
                    if (bfProcessedTime > 0) {
                        bfProcessedTimeSum[zSliceCount] += bfProcessedTime;
                        bfProcessedTimeNumber[zSliceCount]++;
                    }
                } else {
                    bfProcessingRemainCount[zSliceCount]++;
                }
            }

            const auto bfStitchingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                ModalityType{ ModalityType::Name::BF }, ProcessType::Stitching);
            const auto bfStitchingTime = bfStitchingTaskInfo.GetProcessedTime(second);

            if (bfStitchingTaskInfo.GetProcessedProgress() == 1) {
                if (bfStitchingTime > 0) {
                    bfStitchingTimeSum[zSliceCount] += bfStitchingTime;
                    bfStitchingTimeNumber[zSliceCount]++;
                } else {
                    bfStitchingRemainCount[zSliceCount]++;
                }

            }

            const auto bfStitchingWrittenTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                ModalityType{ ModalityType::Name::BF }, ProcessType::StitchingWriting);
            const auto bfStitchingWrittenTime = bfStitchingWrittenTaskInfo.GetProcessedTime(second);

            if (bfStitchingWrittenTaskInfo.GetProcessedProgress() == 1) {
                if (bfStitchingWrittenTime > 0) {
                    bfStitchingWrittenTimeSum[zSliceCount] += bfStitchingWrittenTime;
                    bfStitchingWrittenTimeNumber[zSliceCount]++;
                } else {
                    bfStitchingWrittenRemainCount[zSliceCount]++;
                }
            }

            const auto bfLDMConvertingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                ModalityType{ ModalityType::Name::BF }, ProcessType::LDMConversion);
            const auto bfLDMConvertingTime = bfLDMConvertingTaskInfo.GetProcessedTime(second);

            if (bfLDMConvertingTaskInfo.GetProcessedProgress() == 1) {
                if (bfLDMConvertingTime > 0) {
                    bfLDMConvertingTimeSum[zSliceCount] += bfLDMConvertingTime;
                    bfLDMConvertingTimeNumber[zSliceCount]++;
                } else {
                    bfLDMConvertingRemainCount[zSliceCount]++;
                }
            }
        }

        for (const auto& zSliceCount : bfZSliceCountList) {
            remainingTaskCount.SetBFProcessingCount(zSliceCount, bfProcessingRemainCount[zSliceCount]);
            remainingTaskCount.SetBFStitchingCount(zSliceCount, bfStitchingRemainCount[zSliceCount]);
            remainingTaskCount.SetBFStitchingWrittenCount(zSliceCount, bfStitchingWrittenRemainCount[zSliceCount]);
            remainingTaskCount.SetBFLDMConvertingCount(zSliceCount, bfLDMConvertingRemainCount[zSliceCount]);

            const auto averageProcessedTime = GetAverageTime(bfProcessedTimeSum[zSliceCount], bfProcessedTimeNumber[zSliceCount], defaultBFProcessingTimePerSlice * zSliceCount);
            const auto averageStitchingTime = GetAverageTime(bfStitchingTimeSum[zSliceCount], bfStitchingTimeNumber[zSliceCount], defaultBFStitchingTimePerTileSlice * tileNumber * zSliceCount);
            const auto averageStitchingWrittenTime = GetAverageTime(bfStitchingWrittenTimeSum[zSliceCount], bfStitchingWrittenTimeNumber[zSliceCount], defaultBFStitchingWrittenTimePerTileSlice * tileNumber * zSliceCount);
            const auto averageLDMConvertingTime = GetAverageTime(bfLDMConvertingTimeSum[zSliceCount], bfLDMConvertingTimeNumber[zSliceCount], defaultBFLDMConvertingTimePerTileSlice * tileNumber * zSliceCount);

            takenUnitTimeToProcess.SetBFProcessingTime(zSliceCount, averageProcessedTime, second);
            takenUnitTimeToProcess.SetBFStitchingTime(zSliceCount, averageStitchingTime, second);
            takenUnitTimeToProcess.SetBFStitchingWrittenTime(zSliceCount, averageStitchingWrittenTime, second);
            takenUnitTimeToProcess.SetBFLDMConvertingTime(zSliceCount, averageLDMConvertingTime, second);
        }
    }

    RemainingTimeCalculator remainingTimeCalculator;
    remainingTimeCalculator.SetRemainingTaskCount(remainingTaskCount);
    remainingTimeCalculator.SetTakenUnitTimeToProcess(takenUnitTimeToProcess);
    remainingTimeCalculator.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

    remainingTimeCalculator.Calculate();

    return remainingTimeCalculator.GetRemainingTime(TimeUnit::Second);
}
auto FigureOutAcquisitionStatus(const AcquiredDataFlag& acquiredDataFlag, const AcquisitionCount& acquisitionCount)->DataAcquisitionStatus {
    const auto [tileNumberX, tileNumberY] = acquisitionCount.GetTileNumber();
    const auto tileNumber = tileNumberX * tileNumberY;

    const auto acquisitionSequenceInfo = acquisitionCount.GetAcquisitionSequenceInfo();
    const auto& timeFrameCount = acquisitionSequenceInfo.GetTimeFrameCount();

    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        for (auto tileIndex = 0; tileIndex < tileNumber;++tileIndex) {
            { // HT
                if (acquisitionSequenceInfo.AcquisitionExists(SequenceModality::HT, timeIndex)) {
                    if (!acquiredDataFlag.GetBGAcquiredFlag()) {
                        return DataAcquisitionStatus::BackgroundFault;
                    }
                    if (!acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::HT })) {
                        return DataAcquisitionStatus::NotFullyAcquired;
                    }
                }
            }
            
            { // FLCH0
                constexpr auto flChannelIndex = 0;
                if (acquisitionSequenceInfo.AcquisitionExists(SequenceModality::FLCH0, timeIndex)) {
                    if (!acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, flChannelIndex })) {
                        return DataAcquisitionStatus::NotFullyAcquired;
                    }
                }
            }
            { // FLCH1
                constexpr auto flChannelIndex = 1;
                if (acquisitionSequenceInfo.AcquisitionExists(SequenceModality::FLCH1, timeIndex)) {
                    if (!acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, flChannelIndex })) {
                        return DataAcquisitionStatus::NotFullyAcquired;
                    }
                }
            }
            { // FLCH2
                constexpr auto flChannelIndex = 2;
                if (acquisitionSequenceInfo.AcquisitionExists(SequenceModality::FLCH2, timeIndex)) {
                    if (!acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, flChannelIndex })) {
                        return DataAcquisitionStatus::NotFullyAcquired;
                    }
                }
            }

            { // BF
                if (acquisitionSequenceInfo.AcquisitionExists(SequenceModality::BF, timeIndex)) {
                    if (!acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::BF })) {
                        return DataAcquisitionStatus::NotFullyAcquired;
                    }
                }
            }
        }
    }
    return DataAcquisitionStatus::FullyAcquired;
}
auto FigureOutProcessingStatus(const TaskInfoSet& taskInfoSet, const AcquiredDataFlag& acquiredDataFlag, const AcquisitionCount& acquisitionCount)->ProcessingStatus {
    const auto [tileNumberX, tileNumberY] = acquisitionCount.GetTileNumber();
    const auto tileNumber = tileNumberX * tileNumberY;

    const auto acquisitionSequenceInfo = acquisitionCount.GetAcquisitionSequenceInfo();
    const auto& timeFrameCount = acquisitionSequenceInfo.GetTimeFrameCount();

    auto remainingTaskExists = false;
    auto missingDataExists = false;
    auto isProcessing = false;

    {
        for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
            if (!acquisitionSequenceInfo.AcquisitionExists(SequenceModality::HT, timeIndex)) {
                continue;
            }

            auto tileRemainingTaskExists = false;
            auto tileMissingDataExists = false;
            auto tileIsProcessing = false;

            for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
                const auto processingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, tileIndex, 
                    ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing);

                if (processingTaskInfo.GetOnProcessing()) {
                    tileIsProcessing = true;
                    break;
                }

                const auto dataAcquiredDone = acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::HT });
                const auto processingDone = (processingTaskInfo.GetProcessedProgress() == 1);

                if (dataAcquiredDone) {
                    if (!processingDone) {
                        tileRemainingTaskExists = true;
                        break;
                    }
                } else {
                    tileMissingDataExists = true;
                    break;
                }
            }

            const auto tileProcessingDone = !tileRemainingTaskExists && !tileMissingDataExists && !tileIsProcessing;
            if (tileProcessingDone) {
                const auto stitchingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, 
                    ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching);
                const auto stitchingWrittenTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, 
                    ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting);
                const auto ldmConvertingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, 
                    ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion);

                if (stitchingTaskInfo.GetOnProcessing()) { isProcessing |= true; }
                if (stitchingWrittenTaskInfo.GetOnProcessing()) { isProcessing |= true; }
                if (ldmConvertingTaskInfo.GetOnProcessing()) { isProcessing |= true; }

                const auto stitchingDone = (stitchingTaskInfo.GetProcessedProgress() == 1);
                const auto stitchingWrittenDone = (stitchingWrittenTaskInfo.GetProcessedProgress() == 1);
                const auto ldmConvertingDone = (ldmConvertingTaskInfo.GetProcessedProgress() == 1);

                if (!stitchingDone) { remainingTaskExists |= true; }
                if (stitchingDone && !stitchingWrittenDone) { remainingTaskExists |= true; }
                if (stitchingWrittenDone && !ldmConvertingDone) { remainingTaskExists |= true; }
            } else {
                remainingTaskExists |= tileRemainingTaskExists;
                missingDataExists |= tileMissingDataExists;
                isProcessing |= tileIsProcessing;
                break;
            }
        }
    }
    {
        for (auto flChannelIndex = 0; flChannelIndex < 3; ++flChannelIndex) {
            const auto sequenceModality = ConvertSequenceModality(flChannelIndex);
            for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
                if (!acquisitionSequenceInfo.AcquisitionExists(sequenceModality, timeIndex)) {
                    continue;
                }

                auto tileRemainingTaskExists = false;
                auto tileMissingDataExists = false;
                auto tileIsProcessing = false;

                for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
                    const auto processingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, tileIndex, 
                        ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::TileProcessing);

                    if (processingTaskInfo.GetOnProcessing()) {
                        tileIsProcessing = true;
                        break;
                    }

                    const auto dataAcquiredDone = acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, flChannelIndex });
                    const auto processingDone = (processingTaskInfo.GetProcessedProgress() == 1);

                    if (dataAcquiredDone) {
                        if (!processingDone) {
                            tileRemainingTaskExists = true;
                            break;
                        }
                    } else {
                        tileRemainingTaskExists = true;
                        break;
                    }
                }

                const auto tileProcessingDone = !tileRemainingTaskExists && !tileMissingDataExists && !tileIsProcessing;
                if (tileProcessingDone) {
                    const auto stitchingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, 
                        ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::Stitching);
                    const auto stitchingWrittenTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, 
                        ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::StitchingWriting);
                    const auto ldmConvertingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, 
                        ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::LDMConversion);

                    if (stitchingTaskInfo.GetOnProcessing()) { isProcessing |= true; }
                    if (stitchingWrittenTaskInfo.GetOnProcessing()) { isProcessing |= true; }
                    if (ldmConvertingTaskInfo.GetOnProcessing()) { isProcessing |= true; }

                    const auto stitchingDone = (stitchingTaskInfo.GetProcessedProgress() == 1);
                    const auto stitchingWrittenDone = (stitchingWrittenTaskInfo.GetProcessedProgress() == 1);
                    const auto ldmConvertingDone = (ldmConvertingTaskInfo.GetProcessedProgress() == 1);

                    if (!stitchingDone) { remainingTaskExists |= true; }
                    if (stitchingDone && !stitchingWrittenDone) { remainingTaskExists |= true; }
                    if (stitchingWrittenDone && !ldmConvertingDone) { remainingTaskExists |= true; }
                } else {
                    remainingTaskExists |= tileRemainingTaskExists;
                    missingDataExists |= tileMissingDataExists;
                    isProcessing |= tileIsProcessing;
                    break;
                }
            }
        }
    }
    {
        for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
            if (!acquisitionSequenceInfo.AcquisitionExists(SequenceModality::BF, timeIndex)) {
                continue;
            }

            auto tileRemainingTaskExists = false;
            auto tileMissingDataExists = false;
            auto tileIsProcessing = false;

            for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
                const auto processingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, tileIndex, ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing);

                if (processingTaskInfo.GetOnProcessing()) {
                    tileIsProcessing = true;
                    break;
                }

                const auto dataAcquiredDone = acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::BF });
                const auto processingDone = (processingTaskInfo.GetProcessedProgress() == 1);

                if (dataAcquiredDone) {
                    if (!processingDone) {
                        tileRemainingTaskExists = true;
                        break;
                    }
                } else {
                    tileMissingDataExists = true;
                    break;
                }
            }

            const auto tileProcessingDone = !tileRemainingTaskExists && !tileMissingDataExists && !tileIsProcessing;
            if (tileProcessingDone) {
                const auto stitchingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, 
                    ModalityType{ModalityType::Name::BF}, ProcessType::Stitching);
                const auto stitchingWrittenTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, 
                    ModalityType{ ModalityType::Name::BF }, ProcessType::StitchingWriting);
                const auto ldmConvertingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, 
                    ModalityType{ ModalityType::Name::BF }, ProcessType::LDMConversion);

                if (stitchingTaskInfo.GetOnProcessing()) { isProcessing |= true; }
                if (stitchingWrittenTaskInfo.GetOnProcessing()) { isProcessing |= true; }
                if (ldmConvertingTaskInfo.GetOnProcessing()) { isProcessing |= true; }

                const auto stitchingDone = (stitchingTaskInfo.GetProcessedProgress() == 1);
                const auto stitchingWrittenDone = (stitchingWrittenTaskInfo.GetProcessedProgress() == 1);
                const auto ldmConvertingDone = (ldmConvertingTaskInfo.GetProcessedProgress() == 1);

                if (!stitchingDone) { remainingTaskExists |= true; }
                if (stitchingDone && !stitchingWrittenDone) { remainingTaskExists |= true; }
                if (stitchingWrittenDone && !ldmConvertingDone) { remainingTaskExists |= true; }
            } else {
                remainingTaskExists |= tileRemainingTaskExists;
                missingDataExists |= tileMissingDataExists;
                isProcessing |= tileIsProcessing;
                break;
            }
        }
    }

    {
        const auto tcfWrittenTaskInfo = taskInfoSet.GetTaskInfo(0, 0, 
            ModalityType{ ModalityType::Name::None }, ProcessType::TCFWriting);

        const auto tcfWrittenIsOnProcessing = tcfWrittenTaskInfo.GetOnProcessing();
        if (tcfWrittenIsOnProcessing) {
            isProcessing |= true;
        }
        const auto writtenDone = (tcfWrittenTaskInfo.GetProcessedProgress() == 1);
        if (!writtenDone) {
            remainingTaskExists |= true;
        }
    }
    
    if (isProcessing) {
        return ProcessingStatus::IsProcessing;
    } else if (missingDataExists) {
        return ProcessingStatus::WaitForAcquisition;
    } else if (remainingTaskExists) {
        return ProcessingStatus::WaitForProcess;
    } else {
        return ProcessingStatus::UnhandledStatus;
    }
}

auto SampleObjectGenerator::IsGenerating() -> bool {
    return isGenerating;
}

auto SampleObjectGenerator::Generate() -> QList<SampleObject> {
    isGenerating = true;

    const auto taskRepoInstance = TaskRepo::GetInstance();

    const auto rootFolderPathList = taskRepoInstance->GetRootFolderPathList();

    QList<SampleObject> sampleObjectList;
    for (const auto& rootFolderPath : rootFolderPathList) {
        const auto acquisitionDataFlag = taskRepoInstance->GetAcquiredDataFlag(rootFolderPath);
        const auto taskInfoSet = taskRepoInstance->GetTaskInfoSet(rootFolderPath);
        const auto acquisitionCount = taskRepoInstance->GetAcquisitionCount(rootFolderPath);

        const auto dataAcquisitionStatus = FigureOutAcquisitionStatus(acquisitionDataFlag, acquisitionCount);

        SampleObject sampleObject;
        sampleObject.rootPath = rootFolderPath;
        sampleObject.dataAcquisitionStatus = dataAcquisitionStatus;

        if (taskRepoInstance->IsAlreadyDone(rootFolderPath)) {
            sampleObject.tcfExist = true;
            sampleObject.remainingTime = 0;
            sampleObject.processingStatus = ProcessingStatus::AlreadyProcessed;
        } else if (taskRepoInstance->IsFailed(rootFolderPath)) {
            sampleObject.tcfExist = false;
            sampleObject.remainingTime = 0;
            sampleObject.processingStatus = ProcessingStatus::ProcessingFail;
        } else if (taskRepoInstance->IsDone(rootFolderPath)) {
            sampleObject.tcfExist = true;
            sampleObject.remainingTime = 0;
            sampleObject.processingStatus = ProcessingStatus::ProperlyProcessed;
        } else if (taskRepoInstance->IsAdded(rootFolderPath)) {
            const auto remainingTime = CalculateRemainingTimeInSec(taskInfoSet, acquisitionCount);
            const auto processingStatus = FigureOutProcessingStatus(taskInfoSet, acquisitionDataFlag, acquisitionCount);

            sampleObject.tcfExist = false;
            sampleObject.remainingTime = remainingTime;
            sampleObject.processingStatus = processingStatus; // one of WaitForProcess, WaitForAcquisition, IsProcessing
        }

        sampleObjectList.push_back(sampleObject);
    }

    isGenerating = false;

    return sampleObjectList;
}
