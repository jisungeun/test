#pragma once

#include <memory>
#include <QString>
#include <QMap>

#include "TileConfiguration.h"
#include "TileSet.h"

class TileSetGeneratorHT {
public:
    TileSetGeneratorHT();
    ~TileSetGeneratorHT();

    auto SetTileConfiguration(const TileConfiguration& tileConfiguration)->void;
    auto SetDataPositionFilePathMap(const QMap<QString, QString>& dataPositionFilePathMap)->void;

    auto Generate()->bool;

    auto GetTileSet() const ->const TileSet&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
