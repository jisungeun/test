#pragma once

#include <memory>

class AcquisitionSequenceInfo {
public:
    enum class Modality { HT, FLCH0, FLCH1, FLCH2, BF, Count = 5 };

    AcquisitionSequenceInfo();
    AcquisitionSequenceInfo(const AcquisitionSequenceInfo& other);
    ~AcquisitionSequenceInfo();

    auto operator=(const AcquisitionSequenceInfo& other)->AcquisitionSequenceInfo&;

    auto Initialize(const int32_t& timeFrameCount)->void;
    auto IsInitialized()const->const bool&;
    auto GetTimeFrameCount()const->const int32_t&;

    auto SetAcquisitionZSliceCount(const Modality& modality, const int32_t& timeIndex, const int32_t& zSliceCount)->void;
    auto GetAcquisitionZSliceCount(const Modality& modality, const int32_t& timeIndex)const->int32_t;

    auto AcquisitionExists(const Modality& modality, const int32_t& timeIndex)const->bool;
    auto GetOrderIndex(const Modality& modality, const int32_t& timeIndex)const->int32_t;

    auto GetFirstTimeFrameIndex(const Modality& modality)const->int32_t;
    auto GetLastTimeFrameIndex(const Modality& modality)const->int32_t;
    auto GetTimeFrameIndex(const Modality& modality, const int32_t& orderIndex)const->int32_t;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};

auto ConvertSequenceModality(const int32_t& flChannelIndex)->AcquisitionSequenceInfo::Modality;