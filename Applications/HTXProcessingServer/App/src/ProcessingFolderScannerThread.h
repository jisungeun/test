#pragma once

#include <memory>
#include <QThread>

#include "SampleObject.h"

class ProcessingFolderScannerThread final : public QThread {
    Q_OBJECT
public:
    explicit ProcessingFolderScannerThread(QObject* parent = nullptr);
    ~ProcessingFolderScannerThread();

    auto SetScanFolderPath(const QString& scanFolderPath)->void;
    auto SetPeriodInSec(const int32_t& periodInSec)->void;
    auto SetSystemBackgroundFolderPath(const QString& systemBackgroundImageFolderPath)->void;

    auto StartScan()->void;
    auto StopScan()->void;

    auto Close()->void;

    auto IsStopped() const ->bool;
protected:
    void run() override;
private:
    struct Impl;
    std::shared_ptr<Impl> d;
private slots:
    void WakeThread();
};
