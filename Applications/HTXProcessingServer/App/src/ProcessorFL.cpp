#define NOMINMAX
#define LOGGER_TAG "[ProcessorFL]"

#include "ProcessorFL.h"

#include <QDir>
#include <QStandardPaths>

#include "arrayfire.h"
#include "DataFolderPathGetter.h"

#include "FLProcessorAutoQuant.h"
#include "FLWriterHDF5.h"
#include "HDF5Mutex.h"
#include "IFLProcessorOutput.h"
#include "IFLWriterOutput.h"
#include "ResultFilePathGenerator.h"
#include "TCFDataSetRecordedPosition.h"
#include "TCLogger.h"

using namespace TC::TCFWriter;

class FLProcessorOutput final : public IFLProcessorOutput {
public:
    FLProcessorOutput() = default;
    ~FLProcessorOutput() = default;

    auto SetFLProcessorResult(const FLProcessorResult& flProcessorResult) -> void override {
        this->result = flProcessorResult;
    }

    auto GetFLWriterInput()->FLWriterInput {
        const auto data = result.GetData();
        const auto dataSizeX = result.GetDataSizeX();
        const auto dataSizeY = result.GetDataSizeY();
        const auto dataSizeZ = result.GetDataSizeZ();

        const auto pixelWorldSizeX = result.GetPixelWorldSizeX(LengthUnit::Micrometer);
        const auto pixelWorldSizeY = result.GetPixelWorldSizeY(LengthUnit::Micrometer);
        const auto pixelWorldSizeZ = result.GetPixelWorldSizeZ(LengthUnit::Micrometer);

        FLWriterInput flWriterInput;
        flWriterInput.SetData(data);
        flWriterInput.SetDataSize(dataSizeX, dataSizeY, dataSizeZ);
        flWriterInput.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);

        return flWriterInput;
    }

private:
    FLProcessorResult result;
};

class FLWriterOutput final : public IFLWriterOutput {
public:
    FLWriterOutput() = default;
    ~FLWriterOutput() = default;

    auto SetFLWriterResult(const FLWriterResult& htWriterResult) -> void override {
        this->result = htWriterResult;
    }

    FLWriterResult result;
};

class ProcessorFL::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};

    TCFFLDataSet tcfFLDataSet{};
    AcquisitionConfig acquisitionConfig{};
};

ProcessorFL::ProcessorFL() : d(new Impl()) {
}

ProcessorFL::~ProcessorFL() = default;

auto ProcessorFL::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto ProcessorFL::SetAcquisitionConfig(const AcquisitionConfig& acquisitionConfig) -> void {
    d->acquisitionConfig = acquisitionConfig;
}

auto ProcessorFL::Process(const int32_t& flChannelIndex, const int32_t& tileIndex, const int32_t& timeIndex) -> bool {
    const auto& deviceInfo = d->acquisitionConfig.GetDeviceInfo();
    const auto& acquisitionSetting = d->acquisitionConfig.GetAcquisitionSetting();

    float waveLength{};
    if (flChannelIndex == 0) {
        waveLength = acquisitionSetting.flCH0EmissionWaveLengthMicrometer;
    } else if (flChannelIndex == 1) {
        waveLength = acquisitionSetting.flCH1EmissionWaveLengthMicrometer;
    } else if (flChannelIndex == 2) {
        waveLength = acquisitionSetting.flCH2EmissionWaveLengthMicrometer;
    }

    const ModalityType modalityType{ ModalityType::Name::FL, flChannelIndex };

    DataFolderPathGetter dataFolderPathGetter;
    dataFolderPathGetter.SetRootFolderPath(d->rootFolderPath);
    dataFolderPathGetter.SetModalityType(modalityType);
    dataFolderPathGetter.SetTileIndex(tileIndex);
    dataFolderPathGetter.SetTimeIndex(timeIndex);

    if (!dataFolderPathGetter.ExistTimeFrameFolder()) {
        QLOG_ERROR() << "dataFolderPathGetter.ExistTimeFrameFolder() fails in Process()";
        return false;
    }

    const auto sampleDataFolderPath = dataFolderPathGetter.GetTimeFrameFolderPath();

    QLOG_INFO() << "FL processing tile(" << tileIndex << ") time(" << timeIndex << ")";
    QLOG_INFO() << sampleDataFolderPath;

    const auto flZStepLengthMicrometer = std::max(acquisitionSetting.flZStepLengthMicrometer, 0.001);

    FLProcessorInput flProcessorInput;
    flProcessorInput.SetSampleDataFolderPath(sampleDataFolderPath);
    flProcessorInput.SetObjectiveNA(deviceInfo.objectiveNA);
    flProcessorInput.SetCondenserNA(deviceInfo.condenserNA);
    flProcessorInput.SetPixelSizeOfImagingSensor(deviceInfo.pixelSizeMicrometer, LengthUnit::Micrometer);
    flProcessorInput.SetMagnificationOfSystem(deviceInfo.magnification);
    flProcessorInput.SetZStepLength(flZStepLengthMicrometer, LengthUnit::Micrometer);
    flProcessorInput.SetMediumRI(deviceInfo.mediumRI);
    flProcessorInput.SetWaveLength(waveLength, LengthUnit::Micrometer);

    constexpr float sampleDepth = 3.0f;
    const QString processingTempFolderPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation) + "/temp/FL";
    if (!QDir().exists(processingTempFolderPath)) {
        QDir().mkpath(processingTempFolderPath);
    }

    IFLProcessorOutput::Pointer outputPort{ new FLProcessorOutput };

    FLProcessorAutoQuant flProcessorAutoQuant;
    flProcessorAutoQuant.SetDllUnlockRegistryKeyLocation("HKEY_CURRENT_USER\\Software\\TomoCube, Inc.\\HTXProcessingServer\\Strings");
    flProcessorAutoQuant.SetFLProcessorInput(flProcessorInput);
    flProcessorAutoQuant.SetOutputPort(outputPort);
    flProcessorAutoQuant.SetSampleDepth(sampleDepth, LengthUnit::Micrometer);
    flProcessorAutoQuant.SetProcessingTempFolderPath(processingTempFolderPath);

    if (!flProcessorAutoQuant.Process()) {
        QLOG_ERROR() << "flProcessorAutoQuant.Process() fails";
        return false;
    }

    const auto flWriterInput = std::dynamic_pointer_cast<FLProcessorOutput>(outputPort)->GetFLWriterInput();

    const QString writingTempFolderPath = d->rootFolderPath + "/temp/FL";
    if (!QDir().mkpath(writingTempFolderPath)) {
        return false;
    }

    const auto flProcessedDataFilePath = 
        ResultFilePathGenerator::GetFLProcessedFilePath(d->rootFolderPath, flChannelIndex, tileIndex, timeIndex);

    const IFLWriterOutput::Pointer flWriterOutputPointer{ new FLWriterOutput };

    FLWriterHDF5 flWriterHdf5;
    flWriterHdf5.SetTargetFilePath(flProcessedDataFilePath);
    flWriterHdf5.SetInput(flWriterInput);
    flWriterHdf5.SetOutputPort(flWriterOutputPointer);

    {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        if (!flWriterHdf5.Write()) {
            QLOG_ERROR() << "flWriterHdf5.Write() fails";
            return false;
        }
    }

    return true;
}
