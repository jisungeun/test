#pragma once

#include <memory>
#include <QString>

#include "IBFDataSetGetter.h"

class BFDataSetGetterStitchingResult final : public IBFDataSetGetter {
public:
    BFDataSetGetterStitchingResult();
    ~BFDataSetGetterStitchingResult();

    auto SetResultFilePath(const QString& resultFilePath)->void;

    auto GetData() const -> std::shared_ptr<uint8_t[]> override;
    auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1) const
        -> std::shared_ptr<uint8_t[]> override;
    auto GetColorChannelCount() const -> int32_t override;
    auto GetDataMemoryOrder() const -> MemoryOrder3D override;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};