#pragma once

#include <QString>
#include <memory>

#include "SIUnit.h"

class AcquisitionPositionReader {
public:
    AcquisitionPositionReader();
    ~AcquisitionPositionReader();

    auto SetFilePath(const QString& filePath)->void;
    auto Read()->bool;

    auto GetPositionX(const LengthUnit& unit)const ->double;
    auto GetPositionY(const LengthUnit& unit)const ->double;
    auto GetPositionZ(const LengthUnit& unit)const ->double;
    auto GetPositionC(const LengthUnit& unit)const ->double;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};