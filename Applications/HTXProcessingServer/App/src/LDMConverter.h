#pragma once

#include <memory>
#include <QString>

#include "AcquisitionConfig.h"

class LDMConverter {
public:
    LDMConverter();
    ~LDMConverter();

    auto SetTCFFilePath(const QString& tcfFilePath)->void;
    auto SetOutputTCFFilePath(const QString& outputTCFFilePath)->void;
    auto SetAcquisitionConfig(const AcquisitionConfig& config)->void;

    auto Convert()->bool;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
