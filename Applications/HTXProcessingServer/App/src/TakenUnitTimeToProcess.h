#pragma once

#include <memory>
#include "SIUnit.h"

class TakenUnitTimeToProcess {
public:
    TakenUnitTimeToProcess();
    TakenUnitTimeToProcess(const TakenUnitTimeToProcess& other);
    ~TakenUnitTimeToProcess();

    auto operator=(const TakenUnitTimeToProcess& other)->TakenUnitTimeToProcess&;

    auto SetHTProcessingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit)->void;
    auto SetFLProcessingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit)->void;
    auto SetBFProcessingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit)->void;

    auto SetHTStitchingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit)->void;
    auto SetFLStitchingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit)->void;
    auto SetBFStitchingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit)->void;

    auto SetHTStitchingWrittenTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit)->void;
    auto SetFLStitchingWrittenTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit)->void;
    auto SetBFStitchingWrittenTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit)->void;

    auto SetHTLDMConvertingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit)->void;
    auto SetFLLDMConvertingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit)->void;
    auto SetBFLDMConvertingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit)->void;

    auto GetHTProcessingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit)->double;
    auto GetFLProcessingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit)->double;
    auto GetBFProcessingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit)->double;

    auto GetHTStitchingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit)->double;
    auto GetFLStitchingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit)->double;
    auto GetBFStitchingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit)->double;

    auto GetHTStitchingWrittenTime(const int32_t& zSliceCount, const TimeUnit& timeUnit)->double;
    auto GetFLStitchingWrittenTime(const int32_t& zSliceCount, const TimeUnit& timeUnit)->double;
    auto GetBFStitchingWrittenTime(const int32_t& zSliceCount, const TimeUnit& timeUnit)->double;

    auto GetHTLDMConvertingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit)->double;
    auto GetFLLDMConvertingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit)->double;
    auto GetBFLDMConvertingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit)->double;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};