#pragma once

#include <memory>
#include <QString>

class ThumbnailImageWriter {
public:
    struct RGB {
        uint8_t r{};
        uint8_t g{};
        uint8_t b{};
    };

    ThumbnailImageWriter();
    ~ThumbnailImageWriter();

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto WriteHT(const std::shared_ptr<uint8_t[]>& imageData, const int64_t& sizeX, const int64_t& sizeY, const int32_t& timeIndex)->bool;
    auto WriteFL(const std::shared_ptr<uint8_t[]>& imageData, const int64_t& sizeX, const int64_t& sizeY, const int32_t& timeIndex)->bool;
    auto WriteBF(const std::shared_ptr<uint8_t[]>& imageData, const int64_t& sizeX, const int64_t& sizeY, const int32_t& channels, const int32_t& timeIndex)->bool;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};