#pragma once

#include <memory>
#include <QString>
#include <QStringList>

#include "AcquiredDataFlag.h"
#include "TaskInfoSet.h"

class TaskRepo {
public:
    using Pointer = std::shared_ptr<TaskRepo>;

    ~TaskRepo();

    static auto GetInstance()->Pointer;

    auto AddAlreadyDone(const QString& rootFolderPath)->void;
    auto AddNotDone(const QString& rootFolderPath, const AcquiredDataFlag& acquiredDataFlag, const TaskInfoSet& taskInfoSet, const AcquisitionCount& acquisitionCount)->void;

    auto IsAdded(const QString& rootFolderPath)->bool;
    auto IsAlreadyDone(const QString& rootFolderPath)->bool;
    auto IsDone(const QString& rootFolderPath)->bool;
    auto IsFailed(const QString& rootFolderPath)->bool;

    auto Update(const QString& rootFolderPath, const AcquiredDataFlag& acquiredDataFlag)->void;
    auto Update(const QString& rootFolderPath, const TaskInfoSet& taskInfoSet)->void;
    auto UpdateToDone(const QString& rootFolderPath)->void;
    auto UpdateToFailed(const QString& rootFolderPath)->void;

    auto GetRootFolderPathList()const->QStringList;

    auto GetAcquiredDataFlag(const QString& rootFolderPath)const->AcquiredDataFlag;
    auto GetTaskInfoSet(const QString& rootFolderPath)const->TaskInfoSet;
    auto GetAcquisitionCount(const QString& rootFolderPath)const->AcquisitionCount;

    auto Clear()->void;

protected:
    TaskRepo();

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
