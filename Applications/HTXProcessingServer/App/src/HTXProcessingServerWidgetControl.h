#pragma once

#include <QObject>
#include <QStringList>
#include "HTXProcessingServerHumbleObject.h"

class HTXProcessingServerWidgetControl final : public QObject {
    Q_OBJECT
public:
    HTXProcessingServerWidgetControl();
    ~HTXProcessingServerWidgetControl();

    auto TopDirectoryChanged(const QString& topDirectoryPath)->HTXProcessingServerHumbleObject;

    auto OpenFolder(const int32_t& dataIndex) const ->void;
    auto ProcessButtonIsClicked()->HTXProcessingServerHumbleObject;
    auto PeriodChanged(const int32_t& periodInSec)->HTXProcessingServerHumbleObject;

    auto GetHumbleObject()const->const HTXProcessingServerHumbleObject&;

    auto CloseWidget()->void;
signals:
    void HumbleObjectIsUpdated(HTXProcessingServerHumbleObject humbleObject);
private slots:
    void UpdateProgressByTimer();
    void UpdateSampleObjectByTimer();
    void ProcessByTimer();
private:
    struct Impl;
    std::unique_ptr<Impl> d;
private:
    auto StopTimerAndThreads()->void;
    auto WaitForStopThreads()->void;
};
