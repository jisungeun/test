#include "ProcessingFolderScannerThread.h"

#include <QMutexLocker>
#include <QTimer>
#include <QWaitCondition>

#include "ProcessingFolderScanner.h"

struct ProcessingFolderScannerThread::Impl {
    Impl() = default;
    ~Impl() = default;

    QString scanFolderPath{};
    QString systemBackgroundImageFolderPath{};

    int32_t periodInSec{ 5 };
    QTimer periodicTimer;

    QMutex scannerMutex;

    QWaitCondition waitCondition;
    bool runFlag{ true };

    bool scanIsStopped{ false };
};

ProcessingFolderScannerThread::ProcessingFolderScannerThread(QObject* parent)
    : d(new Impl), QThread(parent) {
    connect(&d->periodicTimer, SIGNAL(timeout()), this, SLOT(WakeThread()));
    start();
}

ProcessingFolderScannerThread::~ProcessingFolderScannerThread() {
    Close();
    wait();
}

auto ProcessingFolderScannerThread::SetScanFolderPath(const QString& scanFolderPath) ->void {
    d->scanFolderPath = scanFolderPath;
}

auto ProcessingFolderScannerThread::SetPeriodInSec(const int32_t& periodInSec) -> void {
    d->periodInSec = periodInSec;
}

auto ProcessingFolderScannerThread::SetSystemBackgroundFolderPath(const QString& systemBackgroundImageFolderPath) -> void {
    d->systemBackgroundImageFolderPath = systemBackgroundImageFolderPath;
}

auto ProcessingFolderScannerThread::StartScan() -> void {
    const auto periodInMilliseconds = d->periodInSec * 1000;
    d->periodicTimer.start(periodInMilliseconds);
}

auto ProcessingFolderScannerThread::StopScan() -> void {
    QMutexLocker locker(&d->scannerMutex);
    d->periodicTimer.stop();
}

auto ProcessingFolderScannerThread::Close() -> void {
    d->periodicTimer.stop();

    d->scannerMutex.lock();
    d->runFlag = false;
    d->waitCondition.wakeOne();
    d->scannerMutex.unlock();
}

auto ProcessingFolderScannerThread::IsStopped() const -> bool {
    QMutexLocker locker(&d->scannerMutex);
    return d->scanIsStopped;
}

void ProcessingFolderScannerThread::run() {
    while (true) {
        QMutexLocker locker(&d->scannerMutex);
        if (d->runFlag) {
            d->scanIsStopped = true;
            d->waitCondition.wait(locker.mutex());
            d->scanIsStopped = false;
            if (!d->runFlag) {
                break;
            }
        } else {
            break;
        }

        ProcessingFolderScanner::Scan(d->scanFolderPath, d->systemBackgroundImageFolderPath);
    }
}

void ProcessingFolderScannerThread::WakeThread() {
    QMutexLocker locker(&d->scannerMutex);
    d->waitCondition.wakeOne();
}
