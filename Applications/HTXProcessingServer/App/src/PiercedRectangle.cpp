#include "PiercedRectangle.h"

class PiercedRectangle::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    int32_t sizeX{};
    int32_t sizeY{};

    int32_t piercingIndexX{};
    int32_t piercingIndexY{};
};

PiercedRectangle::PiercedRectangle() : d(new Impl()) {
}

PiercedRectangle::PiercedRectangle(const PiercedRectangle& other) : d(new Impl(*other.d)) {
}

PiercedRectangle::~PiercedRectangle() = default;

auto PiercedRectangle::operator=(const PiercedRectangle& other) -> PiercedRectangle& {
    *(this->d) = *(other.d);
    return *this;
}

auto PiercedRectangle::SetSize(const int32_t& sizeX, const int32_t& sizeY) -> void {
    d->sizeX = sizeX;
    d->sizeY = sizeY;
}

auto PiercedRectangle::GetSizeX() const -> const int32_t& {
    return d->sizeX;
}

auto PiercedRectangle::GetSizeY() const -> const int32_t& {
    return d->sizeY;
}

auto PiercedRectangle::SetPiercingIndex(const int32_t& piercingIndexX, const int32_t& piercingIndexY) -> void {
    d->piercingIndexX = piercingIndexX;
    d->piercingIndexY = piercingIndexY;
}

auto PiercedRectangle::GetPiercingIndexX() const -> const int32_t& {
    return d->piercingIndexX;
}

auto PiercedRectangle::GetPiercingIndexY() const -> const int32_t& {
    return d->piercingIndexY;
}
