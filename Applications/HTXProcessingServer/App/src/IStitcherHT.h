#pragma once

#include <memory>
#include <QString>

class IStitcherHT {
public:
    using Pointer = std::shared_ptr<IStitcherHT>;

    virtual ~IStitcherHT();

    virtual auto SetRootFolderPath(const QString& rootFolderPath)->void = 0;
    virtual auto SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY)->void = 0;
    virtual auto SetLimitSize(const double& sizeXInMicrometer, const double& sizeYInMicrometer)->void = 0;

    virtual auto Stitch(const int32_t& timeIndex)->bool = 0;
};