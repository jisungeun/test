#include "StitchingWriterHTLDM.h"

#include <TCLogger.h>
#include <H5Cpp.h>
#include <HDF5Mutex.h>
#include <QFile>

#include <TCFHTLDMDataSet.h>

#include "HTLDMDataSetGetterStitchingResult.h"
#include "ElapsedTimeReader.h"
#include "Octree2DConfigurator.h"
#include "Octree3DConfigurator.h"
#include "RecordingTimeReader.h"
#include "ResultFilePathGenerator.h"
#include "PiercedRectangle.h"
#include "PiercedRectangleMerger.h"
#include "TCFHTLDMDataSetWriter.h"

using namespace TC::TCFWriter;
using namespace TC::IO;
using namespace TC::IO::LdmCore;

using SequenceModality = AcquisitionSequenceInfo::Modality;

class StitchingWriterHTLDM::Impl {
public:
    struct WritingStartIndex {
        int32_t x{};
        int32_t y{};
        int32_t z{};
    };

    struct CenterPosition {
        int32_t x{};
        int32_t y{};
    };

    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    QString tempTCFFilePath{};

    AcquisitionConfig acquisitionConfig{};
    AcquisitionSequenceInfo acquisitionSequenceInfo{};

    auto CheckTempTCFFile()->bool;

    auto ReadHTDataSetMetaInfo(const int32_t& timeIndex)->TCFHTDataSetMetaInfo;

    auto CalculateStitchingPiercedRect()->PiercedRectangle;
    auto CalculateWritingStartIndex(const int32_t& mergedCenterPositionX, const int32_t& mergedCenterPositionY,
        const int32_t& centerPositionX, const int32_t& centerPositionY)->WritingStartIndex;
    auto ReadHTCenterPosition(const int32_t& timeIndex)->CenterPosition;
};

auto StitchingWriterHTLDM::Impl::CheckTempTCFFile() -> bool {
    if (!QFile::exists(this->tempTCFFilePath)) {
        return false;
    }
    if (!H5::H5File::isHdf5(this->tempTCFFilePath.toStdString())) {
        return false;
    }

    const H5::H5File file(this->tempTCFFilePath.toStdString(), H5F_ACC_RDWR);
    if (!file.nameExists("/Data/3D")) {
        return false;
    }
    if (!file.nameExists("/Data/2DMIP")) {
        return false;
    }
    return true;
}

auto StitchingWriterHTLDM::Impl::ReadHTDataSetMetaInfo(const int32_t& timeIndex) -> TCFHTDataSetMetaInfo {
    const QString htProcessedDataFilePath = ResultFilePathGenerator::GetHTProcessedFilePath(this->rootFolderPath, 0, timeIndex);
    const auto stitchingResultFilePath = ResultFilePathGenerator::GetHTStitchingFilePath(this->rootFolderPath, timeIndex);

    // stitching data size
    int32_t stitchingDataSizeX{}, stitchingDataSizeY{}, stitchingDataSizeZ{};
    {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto stitchingDataSet3D = stitchingDataFile.openDataSet("StitchingData3D");

        const auto attrDataSizeX = stitchingDataSet3D.openAttribute("dataSizeX");
        const auto attrDataSizeY = stitchingDataSet3D.openAttribute("dataSizeY");
        const auto attrDataSizeZ = stitchingDataSet3D.openAttribute("dataSizeZ");

        attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
        attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);
        attrDataSizeZ.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeZ);
    }

    // pixel world size
    float pixelWorldSizeX{}, pixelWorldSizeY{}, pixelWorldSizeZ{};
    {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        const H5::H5File processedDataFile(htProcessedDataFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto processedDataSet = processedDataFile.openDataSet("Data");

        const auto attrPixelWorldSizeX = processedDataSet.openAttribute("pixelWorldSizeX");
        const auto attrPixelWorldSizeY = processedDataSet.openAttribute("pixelWorldSizeY");
        const auto attrPixelWorldSizeZ = processedDataSet.openAttribute("pixelWorldSizeZ");

        attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
        attrPixelWorldSizeY.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeY);
        attrPixelWorldSizeZ.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeZ);
    }

    // min max values
    float data3dMinValue{}, data3dMaxValue{}, dataMIPMinValue{}, dataMIPMaxValue{};
    {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };

        const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto stitchingDataSet3D = stitchingDataFile.openDataSet("StitchingData3D");
        const auto stitchingDataSetMIP = stitchingDataFile.openDataSet("StitchingDataMIP");

        const auto attrData3DMinValue = stitchingDataSet3D.openAttribute("minValue");
        const auto attrData3DMaxValue = stitchingDataSet3D.openAttribute("maxValue");
        const auto attrDataMIPMinValue = stitchingDataSetMIP.openAttribute("minValue");
        const auto attrDataMIPMaxValue = stitchingDataSetMIP.openAttribute("maxValue");

        attrData3DMinValue.read(H5::PredType::NATIVE_FLOAT, &data3dMinValue);
        attrData3DMaxValue.read(H5::PredType::NATIVE_FLOAT, &data3dMaxValue);
        attrDataMIPMinValue.read(H5::PredType::NATIVE_FLOAT, &dataMIPMinValue);
        attrDataMIPMaxValue.read(H5::PredType::NATIVE_FLOAT, &dataMIPMaxValue);
    }

    // recordedTime
    QDateTime recordedTime;
    { 
        RecordingTimeReader recordingTimeReader;
        const QString recordedTimeFilePath = QString("%1/data/0000/HT3D/%2/acquisition_timestamp.txt")
            .arg(this->rootFolderPath).arg(timeIndex, 4, 10, QChar('0'));

        recordingTimeReader.SetFilePath(recordedTimeFilePath);
        const auto recordingTimeReadResult = recordingTimeReader.Read();

        
        if (recordingTimeReadResult == true) {
            recordedTime = recordingTimeReader.GetRecordingTime();
        } else {
            recordedTime = QDateTime::currentDateTime();
        }
    }

    // elapsedTime
    double elapsedTime;
    constexpr auto elapsedTimeUnit = TimeUnit::Second;
    {
        ElapsedTimeReader elapsedTimeReader;

        const QString elapsedTimeFilePath = QString("%1/data/0000/HT3D/%2/logical_timestamp.txt")
            .arg(this->rootFolderPath).arg(timeIndex, 4, 10, QChar('0'));

        elapsedTimeReader.SetFilePath(elapsedTimeFilePath);
        const auto elapsedTimeReadResult = elapsedTimeReader.Read();

        if (elapsedTimeReadResult == true) {
            elapsedTime = elapsedTimeReader.GetElapsedTime(elapsedTimeUnit);
        } else {
            elapsedTime = 0;
        }
    }

    // tcfDataIndex
    const auto tcfDataIndex = this->acquisitionSequenceInfo.GetOrderIndex(SequenceModality::HT, timeIndex);

    // position
    TCFDataSetRecordedPosition position;
    {
        const auto& acquisitionPosition = this->acquisitionConfig.GetAcquisitionPosition();

        const auto positionX = acquisitionPosition.positionXMillimeter;
        const auto positionY = acquisitionPosition.positionYMillimeter;
        const auto positionZ = acquisitionPosition.positionZMillimeter;
        const auto positionC = acquisitionPosition.positionCMillimeter;
        constexpr auto positionUnit = LengthUnit::Millimenter;

        position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);
    }

    TCFHTDataSetMetaInfo metaInfo;
    metaInfo.SetDataSize(stitchingDataSizeX, stitchingDataSizeY, stitchingDataSizeZ);
    metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);
    metaInfo.SetDataMinMaxValue(data3dMinValue, data3dMaxValue);
    metaInfo.SetMIPDataMinMaxValue(dataMIPMinValue, dataMIPMaxValue);
    metaInfo.SetRecordedTime(recordedTime);
    metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
    metaInfo.SetTimeFrameIndex(tcfDataIndex);
    metaInfo.SetPosition(position);

    return metaInfo;
}

auto StitchingWriterHTLDM::Impl::CalculateStitchingPiercedRect() -> PiercedRectangle {
    const auto timeFrameCount = this->acquisitionSequenceInfo.GetTimeFrameCount();

    QList<PiercedRectangle> stitchingRectList;
    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(SequenceModality::HT, timeIndex)) {
            continue;
        }

        const auto stitchingResultFilePath = ResultFilePathGenerator::GetHTStitchingFilePath(this->rootFolderPath, timeIndex);
        const auto [centerPositionX, centerPositionY] = ReadHTCenterPosition(timeIndex);

        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto stitchingDataSet3D = stitchingDataFile.openDataSet("StitchingData3D");

        int32_t stitchingDataSizeX{}, stitchingDataSizeY{};
        const auto attrDataSizeX = stitchingDataSet3D.openAttribute("dataSizeX");
        const auto attrDataSizeY = stitchingDataSet3D.openAttribute("dataSizeY");

        attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
        attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);

        PiercedRectangle piercedRectangle;
        piercedRectangle.SetSize(stitchingDataSizeX, stitchingDataSizeY);
        piercedRectangle.SetPiercingIndex(centerPositionX, centerPositionY);

        stitchingRectList.push_back(piercedRectangle);
    }

    PiercedRectangleMerger piercedRectangleMerger;
    piercedRectangleMerger.SetPiercedRectangleList(stitchingRectList);
    piercedRectangleMerger.Merge();

    return piercedRectangleMerger.GetMergedPiercedRectangle();
}

auto StitchingWriterHTLDM::Impl::CalculateWritingStartIndex(const int32_t& mergedCenterPositionX,
    const int32_t& mergedCenterPositionY, const int32_t& centerPositionX,
    const int32_t& centerPositionY) -> WritingStartIndex {

    const auto writingStartIndexX = centerPositionX - mergedCenterPositionX;
    const auto writingStartIndexY = centerPositionY - mergedCenterPositionY;

    return WritingStartIndex{ writingStartIndexX, writingStartIndexY , 0 };
}

auto StitchingWriterHTLDM::Impl::ReadHTCenterPosition(const int32_t& timeIndex) -> CenterPosition {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    const auto stitchingResultFilePath = ResultFilePathGenerator::GetHTStitchingFilePath(this->rootFolderPath, timeIndex);
    const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);

    int32_t centerPositionX{}, centerPositionY{};

    const auto attCenterPositionX = stitchingDataFile.openAttribute("centerPositionX");
    const auto attCenterPositionY = stitchingDataFile.openAttribute("centerPositionY");

    attCenterPositionX.read(H5::PredType::NATIVE_INT32, &centerPositionX);
    attCenterPositionY.read(H5::PredType::NATIVE_INT32, &centerPositionY);

    return { centerPositionX , centerPositionY };
}

StitchingWriterHTLDM::StitchingWriterHTLDM() : d(std::make_unique<Impl>()) {
}

StitchingWriterHTLDM::~StitchingWriterHTLDM() = default;

auto StitchingWriterHTLDM::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto StitchingWriterHTLDM::SetTempTCFFilePath(const QString& tempTCFFilePath) -> void {
    d->tempTCFFilePath = tempTCFFilePath;
}

auto StitchingWriterHTLDM::SetAcquisitionConfig(const AcquisitionConfig& acquisitionConfig) -> void {
    d->acquisitionConfig = acquisitionConfig;
}

auto StitchingWriterHTLDM::SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo) -> void {
    d->acquisitionSequenceInfo = acquisitionSequenceInfo;
}

auto StitchingWriterHTLDM::Write(const int32_t& timeIndex) -> bool {
    QLOG_INFO() << "stitchingWriter HT LDM time(" << timeIndex << ")";

    const auto htDataSetMetaInfo = d->ReadHTDataSetMetaInfo(timeIndex);

    const auto stitchingPiercedRect = d->CalculateStitchingPiercedRect();
    const auto [centerPositionX, centerPositionY] = d->ReadHTCenterPosition(timeIndex);
    const auto [writingStartIndexX, writingStartIndexY, writingStartIndexZ] =
        d->CalculateWritingStartIndex(stitchingPiercedRect.GetPiercingIndexX(), stitchingPiercedRect.GetPiercingIndexY(),
            centerPositionX, centerPositionY);

    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    if (!d->CheckTempTCFFile()) {
        return false;
    }

    const auto stitchingResultFilePath = ResultFilePathGenerator::GetHTStitchingFilePath(d->rootFolderPath, timeIndex);
    const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);

    auto srcDataSet = stitchingDataFile.openDataSet("StitchingData3D");
    auto srcMIPDataSet = stitchingDataFile.openDataSet("StitchingDataMIP");

    const auto tcfDataIndex = d->acquisitionSequenceInfo.GetOrderIndex(SequenceModality::HT, timeIndex);
    const auto timeFrameName = QString("%1").arg(tcfDataIndex, 6, 10, QChar('0'));

    const H5::H5File file(d->tempTCFFilePath.toStdString(), H5F_ACC_RDWR);
    const auto data3dGroup = file.openGroup("/Data/3D");
    auto destLDMGroup = data3dGroup.createGroup(timeFrameName.toStdString());

    const auto data2dMIPGroup = file.openGroup("/Data/2DMIP");
    auto destLDMMIPGroup = data2dMIPGroup.createGroup(timeFrameName.toStdString());

    const auto dataSizeX = static_cast<size_t>(stitchingPiercedRect.GetSizeX());
    const auto dataSizeY = static_cast<size_t>(stitchingPiercedRect.GetSizeY());
    const auto dataSizeZ = static_cast<size_t>(htDataSetMetaInfo.GetSizeZ());

    constexpr auto tileSize = 128;

    const Dimension dimension3D{ dataSizeX, dataSizeY, dataSizeZ };
    const Dimension tileDimension3D{ tileSize, tileSize, tileSize };

    const Dimension dimensionMIP{ dataSizeX, dataSizeY };
    const Dimension tileDimensionMIP{ tileSize, tileSize };

    Octree3DConfigurator octree3DConfigurator{ dimension3D, tileDimension3D };
    const auto ldmConfiguration3D = octree3DConfigurator.Configure();

    Octree2DConfigurator octree2DConfigurator{ dimensionMIP, tileDimensionMIP };
    const auto ldmConfigurationMip = octree2DConfigurator.Configure();

    const auto minValue3D = htDataSetMetaInfo.GetDataMinValue();
    const auto minValueMIP = htDataSetMetaInfo.GetDataMIPMinValue();

    std::shared_ptr<HTLDMDataSetGetterStitchingResult> dataGetter{ new HTLDMDataSetGetterStitchingResult };
    dataGetter->SetReadingOffset(writingStartIndexX, writingStartIndexY, writingStartIndexZ);
    dataGetter->SetSourceDataSet(&srcDataSet);
    dataGetter->SetDestLDMGroup(&destLDMGroup);
    dataGetter->SetSourceMIPDataSet(&srcMIPDataSet);
    dataGetter->SetDestLDMMIPGroup(&destLDMMIPGroup);
    dataGetter->SetLdmConfiguration(ldmConfiguration3D, ldmConfigurationMip);
    dataGetter->SetDataMinValue(minValue3D, minValueMIP);

    TCFHTLDMDataSet tcfHTLDMDataSet;
    tcfHTLDMDataSet.SetMetaInfo(htDataSetMetaInfo);
    tcfHTLDMDataSet.SetDataGetter(dataGetter);
    tcfHTLDMDataSet.SetLdmConfiguration(ldmConfiguration3D, ldmConfigurationMip);
        
    locker.Unlock();

    TCFHTLDMDataSetWriter writer;
    writer.SetDestLDMGroup(&destLDMGroup, &destLDMMIPGroup);
    writer.SetTCFHTLDMDataSet(tcfHTLDMDataSet);
    writer.SetWholeDataSize(dataSizeX, dataSizeY, dataSizeZ);
    if (!writer.Write()) {
        QLOG_ERROR() << "tcfHTLDMDataSetWriter.Write() fails";
        return false;
    }

    return true;
}
