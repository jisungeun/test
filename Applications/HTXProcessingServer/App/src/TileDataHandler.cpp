#include "TileDataHandler.h"

#include <QMap>
#include <QVector>

struct TileMemorySize {
    double memorySize{};
    DataSizeUnit dataSizeUnit{ DataSizeUnit::Mebibyte };
};

class TileDataHandler::Impl {
public:
    Impl() : maximumMemorySize(20), maximumMemorySizeUnit(DataSizeUnit::Gibibyte) {
    }
    ~Impl() = default;

    double maximumMemorySize;
    DataSizeUnit maximumMemorySizeUnit;

    double handlingMemorySize{};
    const DataSizeUnit handlingMemorySizeUnit{ DataSizeUnit::Mebibyte };

    QMap<int32_t, std::shared_ptr<float[]>> tileDataMap{};
    QMap<int32_t, TileMemorySize> tileDataSizeMap{};
    QVector<int32_t> addedTileIndices;
};

TileDataHandler::~TileDataHandler() = default;

auto TileDataHandler::GetInstance() -> Pointer {
    static auto instance = Pointer{ new TileDataHandler};
    return instance;
}

auto TileDataHandler::SetMaximumMemorySize(const double& maximumMemorySize, const DataSizeUnit& unit) -> void {
    d->maximumMemorySize = maximumMemorySize;
    d->maximumMemorySizeUnit = unit;
}

auto TileDataHandler::HasSpace(const double& memorySize, const DataSizeUnit& unit) -> bool {
    const auto maximumMemorySize = ConvertUnit(d->maximumMemorySize, d->maximumMemorySizeUnit, DataSizeUnit::Mebibyte);
    const auto handlingMemorySize = ConvertUnit(d->handlingMemorySize, d->handlingMemorySizeUnit, DataSizeUnit::Mebibyte);
    const auto requestedMemorySize = ConvertUnit(memorySize, unit, DataSizeUnit::Mebibyte);

    const auto emptySpace = maximumMemorySize - (handlingMemorySize + requestedMemorySize);
    if (emptySpace > 0) {
        return true;
    } else {
        return false;
    }
}

auto TileDataHandler::AddTileData(const std::shared_ptr<float[]>& tileData, const int32_t& dataIndex,
    const double& memorySize, const DataSizeUnit& unit) -> void {
    d->tileDataMap[dataIndex] = tileData;
    d->tileDataSizeMap[dataIndex] = TileMemorySize{memorySize, unit};
    d->addedTileIndices.push_back(dataIndex);

    d->handlingMemorySize += ConvertUnit(memorySize, unit, d->handlingMemorySizeUnit);
}

auto TileDataHandler::RemoveTileData(const int32_t& dataIndex) -> void {
    if (d->tileDataMap.contains(dataIndex)) {
        d->tileDataMap.remove(dataIndex);
    }

    if (d->tileDataSizeMap.contains(dataIndex)) {
        const auto [memorySize, memorySizeUnit] = d->tileDataSizeMap[dataIndex];

        d->handlingMemorySize -= ConvertUnit(memorySize, memorySizeUnit, d->handlingMemorySizeUnit);

        if (d->handlingMemorySize < 0) {
            d->handlingMemorySize = 0;
        }
    }

    if (d->addedTileIndices.contains(dataIndex)) {
        d->addedTileIndices.removeAll(dataIndex);
    }
}

auto TileDataHandler::RemoveOldestData() -> void {
    if (d->addedTileIndices.size() != 0) {
        RemoveTileData(d->addedTileIndices.first());
    }
}

auto TileDataHandler::GetHandlingDataSize(const DataSizeUnit& unit) const -> double {
    return ConvertUnit(d->handlingMemorySize, d->handlingMemorySizeUnit, unit);
}

auto TileDataHandler::HasData(const int32_t& dataIndex) const -> bool {
    return d->tileDataMap.contains(dataIndex);
}

auto TileDataHandler::GetTileData(const int32_t& dataIndex) const -> std::shared_ptr<float[]> {
    if (HasData(dataIndex)) {
        return d->tileDataMap[dataIndex];
    } else {
        return nullptr;
    }
}

auto TileDataHandler::Clear() -> void {
    d->tileDataMap.clear();
    d->tileDataSizeMap.clear();
    d->addedTileIndices.clear();
    d->handlingMemorySize = 0;
    d->maximumMemorySize = 20;
    d->maximumMemorySizeUnit = DataSizeUnit::Gibibyte;
}

TileDataHandler::TileDataHandler() : d(new Impl()) {
}
