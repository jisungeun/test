#include "HTXProcessingServerWidgetControl.h"

#include <chrono>
#include <QApplication>
#include <QFileDialog>
#include <QMessageBox>
#include <QMap>
#include <QProcess>
#include <QMutex>
#include <QSettings>
#include <QStandardPaths>
#include <QTimer>
#include <QtConcurrent/QtConcurrent>

#include "AcquisitionConfigReader.h"
#include "ProcessingFolderScanner.h"
#include "ProcessingFolderScannerThread.h"
#include "MultiModalProcessorThread.h"

#include "TaskRepo.h"
#include "SampleObjectGenerator.h"
#include "SampleObjectUpdater.h"

#include "DataFolderPathGetter.h"

struct HTXProcessingServerWidgetControl::Impl {
    Impl() = default;
    ~Impl() = default;

    HTXProcessingServerHumbleObject htxProcessingServerHumbleObject{};
    QMutex controlMutex{};

    QTimer sampleObjectUpdatingTimer;
    QTimer humbleObjectUpdatingTimer;
    QTimer processTimer;

    ProcessingFolderScannerThread processingFolderScannerThread;
    MultiModalProcessorThread multiModalProcessorThread;
    SampleObjectUpdater sampleObjectUpdater;

    auto GetPsfFolderPath()->QString;
    auto GetSystemBackgroundFolderPath()->QString;

    auto GetMatlabModuleFolderPath()->QString;

    auto UpdatedSampleObjectList()->void;
};

auto HTXProcessingServerWidgetControl::Impl::GetPsfFolderPath() -> QString {
    auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
    const auto tomoStudioXAppPath = appDataPath.replace("HTXProcessingServer", "TomoStudioX");

    const auto psfFolderPath = QString("%1/PSF").arg(tomoStudioXAppPath);
    return psfFolderPath;
}

auto HTXProcessingServerWidgetControl::Impl::GetSystemBackgroundFolderPath() -> QString {
    auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
    const auto tomoStudioXAppPath = appDataPath.replace("HTXProcessingServer", "TomoStudioX");

    const auto systemBackgroundFolderPath = QString("%1/backgrounds").arg(tomoStudioXAppPath);
    return systemBackgroundFolderPath;
}

auto HTXProcessingServerWidgetControl::Impl::GetMatlabModuleFolderPath() -> QString {
    const auto applicationFolderPath = QCoreApplication::applicationDirPath();
    const auto matlabModuleFolderPath = QString("%1/HTProc").arg(applicationFolderPath);
    return matlabModuleFolderPath;
}

auto HTXProcessingServerWidgetControl::Impl::UpdatedSampleObjectList() -> void {
    const auto topDirectoryPath = this->htxProcessingServerHumbleObject.GetTopDirectory();
    const auto systemBackgroundImageFolderPath = this->htxProcessingServerHumbleObject.GetSystemBackgroundFolderPath();
    ProcessingFolderScanner::Scan(topDirectoryPath, systemBackgroundImageFolderPath);

    if (!SampleObjectGenerator::IsGenerating()) {
        const auto sampleObjectList = SampleObjectGenerator::Generate();
        this->htxProcessingServerHumbleObject.SetSampleObjectList(sampleObjectList);
    }
}

HTXProcessingServerWidgetControl::HTXProcessingServerWidgetControl()
    : d(new Impl()) {
    d->htxProcessingServerHumbleObject.SetMatlabModuleFolderPath(d->GetMatlabModuleFolderPath());

    const auto systemBackgroundFolderPath = d->GetSystemBackgroundFolderPath();
    if (!QDir().exists(systemBackgroundFolderPath)) {
        QDir().mkpath(systemBackgroundFolderPath);
    }
    d->htxProcessingServerHumbleObject.SetSystemBackgroundFolderPath(systemBackgroundFolderPath);

    const auto psfFolderPath = d->GetPsfFolderPath();
    if (!QDir().exists(psfFolderPath)) {
        QDir().mkpath(psfFolderPath);
    }
    d->htxProcessingServerHumbleObject.SetPsfFolderPath(d->GetPsfFolderPath());


    connect(&d->sampleObjectUpdatingTimer, SIGNAL(timeout()), this, SLOT(UpdateSampleObjectByTimer()));
    connect(&d->humbleObjectUpdatingTimer, SIGNAL(timeout()), this, SLOT(UpdateProgressByTimer()));
    connect(&d->processTimer, SIGNAL(timeout()), this, SLOT(ProcessByTimer()));
}

HTXProcessingServerWidgetControl::~HTXProcessingServerWidgetControl() = default;

auto HTXProcessingServerWidgetControl::TopDirectoryChanged(const QString& topDirectoryPath)
    -> HTXProcessingServerHumbleObject {
    QMutexLocker locker(&d->controlMutex);
    TaskRepo::GetInstance()->Clear();

    if (!d->htxProcessingServerHumbleObject.GetIsProcessingFlag()) {
        d->htxProcessingServerHumbleObject.SetTopDirectory(topDirectoryPath);
        d->UpdatedSampleObjectList();
    }
    return d->htxProcessingServerHumbleObject;
}

auto HTXProcessingServerWidgetControl::OpenFolder(const int32_t& dataIndex) const -> void {
    QMutexLocker locker(&d->controlMutex);
    const auto sampleObjectList = d->htxProcessingServerHumbleObject.GetSampleObjectList();

    if (sampleObjectList.size() < dataIndex + 1) {
        return;
    }

    const auto& sampleObject = sampleObjectList.at(dataIndex);

    auto rootPath = sampleObject.rootPath;
    rootPath.replace("/", "\\");
    const QString explorerPath = "C:/Windows/explorer.exe";

    if (QFile::exists(explorerPath)) {
        QProcess::startDetached(explorerPath, { rootPath });
    }
}

auto HTXProcessingServerWidgetControl::ProcessButtonIsClicked() -> HTXProcessingServerHumbleObject {
    QMutexLocker locker(&d->controlMutex);

    const auto& humbleObj = d->htxProcessingServerHumbleObject;
    const auto isProcessingFlag = humbleObj.GetIsProcessingFlag();
    if (!isProcessingFlag) {
        const auto scanPeriodInSec = d->htxProcessingServerHumbleObject.GetScanPeriodInSec();
        const auto scanFolderPath = d->htxProcessingServerHumbleObject.GetTopDirectory();
        const auto systemBackgroundImageFolderPath = d->htxProcessingServerHumbleObject.GetSystemBackgroundFolderPath();

        TaskRepo::GetInstance()->Clear();
        d->UpdatedSampleObjectList();

        d->multiModalProcessorThread.Reset();
        d->multiModalProcessorThread.SetPsfFolderPath(humbleObj.GetPsfFolderPath());
        d->multiModalProcessorThread.SetMatlabModuleFolderPath(humbleObj.GetMatlabModuleFolderPath());
        d->multiModalProcessorThread.SetSystemBackgroundFolderPath(humbleObj.GetSystemBackgroundFolderPath());
        d->multiModalProcessorThread.TriggerProcess();
        
        constexpr auto updatePeriodInMilliseconds = 1 * 1000;
        d->humbleObjectUpdatingTimer.start(updatePeriodInMilliseconds);
        
        d->processingFolderScannerThread.SetPeriodInSec(scanPeriodInSec);
        d->processingFolderScannerThread.SetScanFolderPath(scanFolderPath);
        d->processingFolderScannerThread.SetSystemBackgroundFolderPath(systemBackgroundImageFolderPath);
        d->processingFolderScannerThread.StartScan();

        d->sampleObjectUpdater.SetTopDirectory(scanFolderPath);
        d->sampleObjectUpdater.SetSystemBackgroundFolderPath(systemBackgroundImageFolderPath);
        d->sampleObjectUpdater.Start();

        d->processTimer.start(100);

        const auto scanPeriodInMilliseconds = scanPeriodInSec * 1000;
        d->sampleObjectUpdatingTimer.start(scanPeriodInMilliseconds);
    } else {
        QMessageBox qMessageBox("Processing is stopping in few seconds", "please wait until this message is disappeared",
            QMessageBox::Information, QMessageBox::NoButton, QMessageBox::NoButton, QMessageBox::NoButton);
        qMessageBox.show();
        StopTimerAndThreads();
        WaitForStopThreads();
        qMessageBox.close();
    }

    d->htxProcessingServerHumbleObject.SetIsProcessingFlag(!humbleObj.GetIsProcessingFlag());

    return d->htxProcessingServerHumbleObject;
}

auto HTXProcessingServerWidgetControl::PeriodChanged(const int32_t& periodInSec) -> HTXProcessingServerHumbleObject {
    QMutexLocker locker(&d->controlMutex);
    d->htxProcessingServerHumbleObject.SetScanPeriodInSec(periodInSec);
    return d->htxProcessingServerHumbleObject;
}

auto HTXProcessingServerWidgetControl::GetHumbleObject() const -> const HTXProcessingServerHumbleObject& {
    return d->htxProcessingServerHumbleObject;
}

auto HTXProcessingServerWidgetControl::CloseWidget() -> void {
    QMutexLocker locker(&d->controlMutex);
    if (d->htxProcessingServerHumbleObject.GetIsProcessingFlag()) {
        StopTimerAndThreads();

        d->processingFolderScannerThread.Close();
        d->multiModalProcessorThread.Close();
        d->sampleObjectUpdater.Close();

        d->processingFolderScannerThread.wait();
        d->multiModalProcessorThread.wait();
        d->sampleObjectUpdater.wait();
    }
}

auto HTXProcessingServerWidgetControl::StopTimerAndThreads() -> void {
    d->humbleObjectUpdatingTimer.stop();
    d->sampleObjectUpdatingTimer.stop();
    d->processTimer.stop();
    d->processingFolderScannerThread.StopScan();
    d->sampleObjectUpdater.Stop();
    d->multiModalProcessorThread.Stop();
}

auto HTXProcessingServerWidgetControl::WaitForStopThreads() -> void {
    while (true) {
        const auto scannerStop = d->processingFolderScannerThread.IsStopped();
        const auto updaterStop = d->sampleObjectUpdater.IsStopped();
        const auto processorStop = d->multiModalProcessorThread.IsStopped();

        QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);

        const auto allStop = scannerStop && updaterStop && processorStop;
        if (allStop) {
            d->UpdatedSampleObjectList();
            break;
        }
    }
}

void HTXProcessingServerWidgetControl::UpdateProgressByTimer() {
    QMutexLocker locker(&d->controlMutex);
    emit HumbleObjectIsUpdated(d->htxProcessingServerHumbleObject);
}

void HTXProcessingServerWidgetControl::UpdateSampleObjectByTimer() {
    QMutexLocker locker(&d->controlMutex);
    d->htxProcessingServerHumbleObject.SetSampleObjectList(d->sampleObjectUpdater.GetSampleObjectList());
}

void HTXProcessingServerWidgetControl::ProcessByTimer() {
    d->multiModalProcessorThread.TriggerProcess();
}

