#include "TileDataGetterProcessedData.h"

#include <iostream>

#include "arrayfire.h"
#include "H5Cpp.h"
#include "HDF5Mutex.h"

#include "TileDataHandler.h"

static int32_t numberOfTileData = 0;

struct DataSize {
    int32_t x{};
    int32_t y{};
    int32_t z{};
};

class TileDataGetterProcessedData::Impl {
public:
    Impl() : dataIndex(numberOfTileData++) {}
    ~Impl() {
        TileDataHandler::GetInstance()->RemoveTileData(this->dataIndex);
    }

    QString processedDataFilePath{};

    const int32_t dataIndex;

    auto ReadDataSize(const H5::H5Object& dataSet)->DataSize;
};

auto TileDataGetterProcessedData::Impl::ReadDataSize(const H5::H5Object& dataSet) -> DataSize {
    int32_t dataSizeX{};
    int32_t dataSizeY{};
    int32_t dataSizeZ{};

    auto dataSizeXAttr = dataSet.openAttribute("dataSizeX");
    auto dataSizeYAttr = dataSet.openAttribute("dataSizeY");

    dataSizeXAttr.read(H5::PredType::NATIVE_INT32, &dataSizeX);
    dataSizeYAttr.read(H5::PredType::NATIVE_INT32, &dataSizeY);

    dataSizeXAttr.close();
    dataSizeYAttr.close();

    if (dataSet.attrExists("dataSizeZ")) {
        const auto dataSizeZAttr = dataSet.openAttribute("dataSizeZ");
        dataSizeZAttr.read(H5::PredType::NATIVE_INT32, &dataSizeZ);
    } else {
        dataSizeZ = 1;
    }

    if (dataSet.attrExists("channelCount")) {
        const auto channelCountAttr = dataSet.openAttribute("channelCount");
        channelCountAttr.read(H5::PredType::NATIVE_INT32, &dataSizeZ);
    }

    return { dataSizeX, dataSizeY, dataSizeZ };
}

TileDataGetterProcessedData::TileDataGetterProcessedData() : d(new Impl()) {
}

TileDataGetterProcessedData::~TileDataGetterProcessedData() = default;

auto TileDataGetterProcessedData::SetProcessedDataFilePath(const QString& processedDataFilePath) -> void {
    d->processedDataFilePath = processedDataFilePath;
}

auto TileDataGetterProcessedData::GetData() const -> std::shared_ptr<float[]> {
    std::shared_ptr<float[]> tileData{};

    auto tileDataHandler = TileDataHandler::GetInstance();
    if (tileDataHandler->HasData(d->dataIndex)) {
        tileData = tileDataHandler->GetTileData(d->dataIndex);
    } else {
        try {
            const H5::H5File file(d->processedDataFilePath.toStdString(), H5F_ACC_RDONLY);

            const auto dataSet = file.openDataSet("Data");
            const auto [dataSizeX, dataSizeY, dataSizeZ] = d->ReadDataSize(dataSet);
            const auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

            tileData = std::shared_ptr<float[]>{ new float[numberOfElements]() };

            double tileDataMemorySize{};
            constexpr auto tileDataMemorySizeUnit = DataSizeUnit::Mebibyte;

            const auto dataType = dataSet.getDataType();
            if(dataType == H5::PredType::NATIVE_UINT8) {
                std::shared_ptr<uint8_t[]> readData{ new uint8_t[numberOfElements]() };
                dataSet.read(readData.get(), dataType);

                for (auto index = 0; index < numberOfElements; ++index) {
                    tileData.get()[index] = static_cast<float>(readData.get()[index]);
                }

                tileDataMemorySize = ConvertUnit(numberOfElements, DataSizeUnit::Byte, tileDataMemorySizeUnit);
            } else if (dataType == H5::PredType::NATIVE_FLOAT) {
                dataSet.read(tileData.get(), dataType);

                tileDataMemorySize = ConvertUnit(numberOfElements * 4, DataSizeUnit::Byte, tileDataMemorySizeUnit);
            }


            while (!tileDataHandler->HasSpace(tileDataMemorySize, tileDataMemorySizeUnit) && 
                (tileDataHandler->GetHandlingDataSize(DataSizeUnit::Byte) != 0)) {
                tileDataHandler->RemoveOldestData();
            }

            tileDataHandler->AddTileData(tileData, d->dataIndex, tileDataMemorySize, tileDataMemorySizeUnit);

        } catch (const H5::Exception& exception) {
            tileData = std::shared_ptr<float[]>{ nullptr };
            std::cout << exception.getCDetailMsg() << std::endl;
        }
    }
    return tileData;
}

auto TileDataGetterProcessedData::GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1,
    const int32_t& z0, const int32_t& z1) const -> std::shared_ptr<float[]> {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    int32_t tileSizeX, tileSizeY, tileSizeZ;
    try {
        const H5::H5File file(d->processedDataFilePath.toStdString(), H5F_ACC_RDONLY);

        const auto dataSet = file.openDataSet("Data");
        const auto dataSize = d->ReadDataSize(dataSet);

        tileSizeX = dataSize.x;
        tileSizeY = dataSize.y;
        tileSizeZ = dataSize.z;
    } catch (const H5::Exception& exception) {
        std::cout << exception.getCDetailMsg() << std::endl;
        return nullptr;
    }

    std::shared_ptr<float[]> tileData;

    auto tileDataHandler = TileDataHandler::GetInstance();
    if (tileDataHandler->HasData(d->dataIndex)) {
        tileData = tileDataHandler->GetTileData(d->dataIndex);
    } else {
        tileData = GetData();
    }

    std::shared_ptr<float[]> croppedTileData;
    try {
        af::array tileDataArray{ tileSizeY, tileSizeX, tileSizeZ, tileData.get() };

        const af::seq xRange{ static_cast<double>(x0), static_cast<double>(x1) };
        const af::seq yRange{ static_cast<double>(y0), static_cast<double>(y1) };

        const auto targetZSize = z1 - z0 + 1;

        af::seq zRange;
        if (tileSizeZ > targetZSize) {
            zRange = af::seq{ static_cast<double>(z0), static_cast<double>(z1) };
        } else {
            zRange = af::seq(0, static_cast<double>(tileSizeZ - 1));
        }
        

        const auto tileCropDataArray = tileDataArray(yRange, xRange, zRange).copy();
        const auto numberOfElements = tileCropDataArray.elements();

        croppedTileData = std::shared_ptr<float[]>{ new float[numberOfElements]() };
        tileCropDataArray.host(croppedTileData.get());
    }
    catch (const af::exception& exception) {
        std::cout << exception.what() << std::endl;
        return nullptr;
    }
    
    return croppedTileData;
}
