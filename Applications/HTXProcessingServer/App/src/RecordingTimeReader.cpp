#include "RecordingTimeReader.h"

#include <QFile>
#include <QTextStream>

#include "SIUnit.h"

class RecordingTimeReader::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString filePath{};
    QDateTime recordingTime{};

    auto TimeStampToDateTime(const QString& timeStampString)->QDateTime;
};

auto RecordingTimeReader::Impl::TimeStampToDateTime(const QString& timeStampString) -> QDateTime {
    const QString year = timeStampString.mid(0, 4);
    const QString month = timeStampString.mid(4, 2);
    const QString day = timeStampString.mid(6, 2);
    const QString hour = timeStampString.mid(8, 2);
    const QString minute = timeStampString.mid(10, 2);
    const QString second = timeStampString.mid(12, 2);
    const QString millisecond = timeStampString.mid(14, 3);

    const QDate date(year.toInt(), month.toInt(), day.toInt());
    const QTime time(hour.toInt(), minute.toInt(), second.toInt(), millisecond.toInt());

    return QDateTime(date, time);
}

RecordingTimeReader::RecordingTimeReader() : d(new Impl()) {
}

RecordingTimeReader::~RecordingTimeReader() = default;

auto RecordingTimeReader::SetFilePath(const QString& filePath) -> void {
    d->filePath = filePath;
}

auto RecordingTimeReader::Read() -> bool {
    if (d->filePath.isEmpty()) {
        return false;
    }
    if (!QFile::exists(d->filePath)) {
        return false;
    }

    QFile file(d->filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return false;
    }

    QTextStream inputStream(&file);
    QString timeText = inputStream.readLine();

    d->recordingTime = d->TimeStampToDateTime(timeText);

    return true;
}

auto RecordingTimeReader::GetRecordingTime() const -> const QDateTime& {
    return d->recordingTime;
}
