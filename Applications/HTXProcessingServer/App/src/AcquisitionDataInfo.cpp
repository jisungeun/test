#include "AcquisitionDataInfo.h"

class AcquisitionDataInfo::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    QString rootFolderPath{};
    QString backgroundFolderPath{};
    bool backgroundPlacedInRootFolder{ false };
    AcquisitionCount acquisitionCount{};
};

AcquisitionDataInfo::AcquisitionDataInfo() : d(std::make_unique<Impl>()) {
}

AcquisitionDataInfo::AcquisitionDataInfo(const AcquisitionDataInfo& other) : d(std::make_unique<Impl>(*other.d)) {
}

AcquisitionDataInfo::~AcquisitionDataInfo() = default;

auto AcquisitionDataInfo::operator=(const AcquisitionDataInfo& other) -> AcquisitionDataInfo& {
    *(this->d) = *(other.d);
    return *this;
}

auto AcquisitionDataInfo::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto AcquisitionDataInfo::GetRootFolderPath() const -> const QString& {
    return d->rootFolderPath;
}

auto AcquisitionDataInfo::SetBackgroundFolderPath(const QString& backgroundFolderPath,
    const bool& placeInRootFolder) -> void {
    d->backgroundFolderPath = backgroundFolderPath;
    d->backgroundPlacedInRootFolder = placeInRootFolder;
}

auto AcquisitionDataInfo::GetBackgroundFolderPath() const -> const QString& {
    return d->backgroundFolderPath;
}

auto AcquisitionDataInfo::IsBackgroundPlacedInRootFolder() const -> const bool& {
    return d->backgroundPlacedInRootFolder;
}

auto AcquisitionDataInfo::SetAcquisitionCount(const AcquisitionCount& acquisitionCount) -> void {
    d->acquisitionCount = acquisitionCount;
}

auto AcquisitionDataInfo::GetAcquisitionCount() const -> const AcquisitionCount& {
    return d->acquisitionCount;
}
