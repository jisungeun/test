#include "TakenUnitTimeToProcess.h"

#include <QMap>

const TimeUnit takenTimeUnit{ TimeUnit::Second };
using ZSliceCount = int32_t;

class TakenUnitTimeToProcess::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    QMap<ZSliceCount, double> htProcessingTimeMap{};
    QMap<ZSliceCount, double> flProcessingTimeMap{};
    QMap<ZSliceCount, double> bfProcessingTimeMap{};

    QMap<ZSliceCount, double> htStitchingTimeMap{};
    QMap<ZSliceCount, double> flStitchingTimeMap{};
    QMap<ZSliceCount, double> bfStitchingTimeMap{};

    QMap<ZSliceCount, double> htStitchingWrittenTimeMap{};
    QMap<ZSliceCount, double> flStitchingWrittenTimeMap{};
    QMap<ZSliceCount, double> bfStitchingWrittenTimeMap{};

    QMap<ZSliceCount, double> htLDMConvertingTimeMap{};
    QMap<ZSliceCount, double> flLDMConvertingTimeMap{};
    QMap<ZSliceCount, double> bfLDMConvertingTimeMap{};
};

TakenUnitTimeToProcess::TakenUnitTimeToProcess() : d(new Impl()) {
}

TakenUnitTimeToProcess::TakenUnitTimeToProcess(const TakenUnitTimeToProcess& other) : d(new Impl(*other.d)) {
}

TakenUnitTimeToProcess::~TakenUnitTimeToProcess() = default;

auto TakenUnitTimeToProcess::operator=(const TakenUnitTimeToProcess& other) -> TakenUnitTimeToProcess& {
    *(this->d) = *(other.d);
    return *this;
}

auto TakenUnitTimeToProcess::SetHTProcessingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit) -> void {
    if (d->htProcessingTimeMap.contains(zSliceCount)) {
        d->htProcessingTimeMap.remove(zSliceCount);
    }
    d->htProcessingTimeMap[zSliceCount] = ConvertUnit(time, timeUnit, takenTimeUnit);
}

auto TakenUnitTimeToProcess::SetFLProcessingTime(const int32_t& zSliceCount, const double& time,
    const TimeUnit& timeUnit) -> void {
    if (d->flProcessingTimeMap.contains(zSliceCount)) {
        d->flProcessingTimeMap.remove(zSliceCount);
    }
    d->flProcessingTimeMap[zSliceCount] = ConvertUnit(time, timeUnit, takenTimeUnit);
}

auto TakenUnitTimeToProcess::SetBFProcessingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit) -> void {
    if (d->bfProcessingTimeMap.contains(zSliceCount)) {
        d->bfProcessingTimeMap.remove(zSliceCount);
    }
    d->bfProcessingTimeMap[zSliceCount] = ConvertUnit(time, timeUnit, takenTimeUnit);
}

auto TakenUnitTimeToProcess::SetHTStitchingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit) -> void {
    if (d->htStitchingTimeMap.contains(zSliceCount)) {
        d->htStitchingTimeMap.remove(zSliceCount);
    }
    d->htStitchingTimeMap[zSliceCount] = ConvertUnit(time, timeUnit, takenTimeUnit);
}

auto TakenUnitTimeToProcess::SetFLStitchingTime(const int32_t& zSliceCount, const double& time,
    const TimeUnit& timeUnit) -> void {
    if (d->flStitchingTimeMap.contains(zSliceCount)) {
        d->flStitchingTimeMap.remove(zSliceCount);
    }
    d->flStitchingTimeMap[zSliceCount] = ConvertUnit(time, timeUnit, takenTimeUnit);
}

auto TakenUnitTimeToProcess::SetBFStitchingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit) -> void {
    if (d->bfStitchingTimeMap.contains(zSliceCount)) {
        d->bfStitchingTimeMap.remove(zSliceCount);
    }
    d->bfStitchingTimeMap[zSliceCount] = ConvertUnit(time, timeUnit, takenTimeUnit);
}

auto TakenUnitTimeToProcess::SetHTStitchingWrittenTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit) -> void {
    if (d->htStitchingWrittenTimeMap.contains(zSliceCount)) {
        d->htStitchingWrittenTimeMap.remove(zSliceCount);
    }
    d->htStitchingWrittenTimeMap[zSliceCount] = ConvertUnit(time, timeUnit, takenTimeUnit);
}

auto TakenUnitTimeToProcess::SetFLStitchingWrittenTime(const int32_t& zSliceCount, const double& time,
    const TimeUnit& timeUnit) -> void {
    if (d->flStitchingWrittenTimeMap.contains(zSliceCount)) {
        d->flStitchingWrittenTimeMap.remove(zSliceCount);
    }

    d->flStitchingWrittenTimeMap[zSliceCount] = ConvertUnit(time, timeUnit, takenTimeUnit);
}

auto TakenUnitTimeToProcess::SetBFStitchingWrittenTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit) -> void {
    if (d->bfStitchingWrittenTimeMap.contains(zSliceCount)) {
        d->bfStitchingWrittenTimeMap.remove(zSliceCount);
    }
    d->bfStitchingWrittenTimeMap[zSliceCount] = ConvertUnit(time, timeUnit, takenTimeUnit);
}

auto TakenUnitTimeToProcess::SetHTLDMConvertingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit) -> void {
    if (d->htLDMConvertingTimeMap.contains(zSliceCount)) {
        d->htLDMConvertingTimeMap.remove(zSliceCount);
    }
    d->htLDMConvertingTimeMap[zSliceCount] = ConvertUnit(time, timeUnit, takenTimeUnit);
}

auto TakenUnitTimeToProcess::SetFLLDMConvertingTime(const int32_t& zSliceCount, const double& time,
    const TimeUnit& timeUnit) -> void {
    if (d->flLDMConvertingTimeMap.contains(zSliceCount)) {
        d->flLDMConvertingTimeMap.remove(zSliceCount);
    }
    d->flLDMConvertingTimeMap[zSliceCount] = ConvertUnit(time, timeUnit, takenTimeUnit);
}

auto TakenUnitTimeToProcess::SetBFLDMConvertingTime(const int32_t& zSliceCount, const double& time, const TimeUnit& timeUnit) -> void {
    if (d->bfLDMConvertingTimeMap.contains(zSliceCount)) {
        d->bfLDMConvertingTimeMap.remove(zSliceCount);
    }
    d->bfLDMConvertingTimeMap[zSliceCount] = ConvertUnit(time, timeUnit, takenTimeUnit);
}

auto TakenUnitTimeToProcess::GetHTProcessingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit) -> double {
    if (d->htProcessingTimeMap.contains(zSliceCount)) {
        return ConvertUnit(d->htProcessingTimeMap[zSliceCount], takenTimeUnit, timeUnit);
    } else {
        return 0;
    }
}

auto TakenUnitTimeToProcess::GetFLProcessingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit) -> double {
    if (d->flProcessingTimeMap.contains(zSliceCount)) {
        return ConvertUnit(d->flProcessingTimeMap[zSliceCount], takenTimeUnit, timeUnit);
    } else {
        return 0;
    }
}

auto TakenUnitTimeToProcess::GetBFProcessingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit) -> double {
    if (d->bfProcessingTimeMap.contains(zSliceCount)) {
        return ConvertUnit(d->bfProcessingTimeMap[zSliceCount], takenTimeUnit, timeUnit);
    } else {
        return 0;
    }
}

auto TakenUnitTimeToProcess::GetHTStitchingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit) -> double {
    if (d->htStitchingTimeMap.contains(zSliceCount)) {
        return ConvertUnit(d->htStitchingTimeMap[zSliceCount], takenTimeUnit, timeUnit);
    } else {
        return 0;
    }
}

auto TakenUnitTimeToProcess::GetFLStitchingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit) -> double {
    if (d->flStitchingTimeMap.contains(zSliceCount)) {
        return ConvertUnit(d->flStitchingTimeMap[zSliceCount], takenTimeUnit, timeUnit);
    } else {
        return 0;
    }
}

auto TakenUnitTimeToProcess::GetBFStitchingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit) -> double {
    if (d->bfStitchingTimeMap.contains(zSliceCount)) {
        return ConvertUnit(d->bfStitchingTimeMap[zSliceCount], takenTimeUnit, timeUnit);
    } else {
        return 0;
    }
}

auto TakenUnitTimeToProcess::GetHTStitchingWrittenTime(const int32_t& zSliceCount, const TimeUnit& timeUnit) -> double {
    if (d->htStitchingWrittenTimeMap.contains(zSliceCount)) {
        return ConvertUnit(d->htStitchingWrittenTimeMap[zSliceCount], takenTimeUnit, timeUnit);
    } else {
        return 0;
    }
}

auto TakenUnitTimeToProcess::GetFLStitchingWrittenTime(const int32_t& zSliceCount, const TimeUnit& timeUnit) -> double {
    if (d->flStitchingWrittenTimeMap.contains(zSliceCount)) {
        return ConvertUnit(d->flStitchingWrittenTimeMap[zSliceCount], takenTimeUnit, timeUnit);
    } else {
        return 0;
    }
}

auto TakenUnitTimeToProcess::GetBFStitchingWrittenTime(const int32_t& zSliceCount, const TimeUnit& timeUnit) -> double {
    if (d->bfStitchingWrittenTimeMap.contains(zSliceCount)) {
        return ConvertUnit(d->bfStitchingWrittenTimeMap[zSliceCount], takenTimeUnit, timeUnit);
    } else {
        return 0;
    }
}

auto TakenUnitTimeToProcess::GetHTLDMConvertingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit) -> double {
    if (d->htLDMConvertingTimeMap.contains(zSliceCount)) {
        return ConvertUnit(d->htLDMConvertingTimeMap[zSliceCount], takenTimeUnit, timeUnit);
    } else {
        return 0;
    }
}

auto TakenUnitTimeToProcess::GetFLLDMConvertingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit) -> double {
    if (d->flLDMConvertingTimeMap.contains(zSliceCount)) {
        return ConvertUnit(d->flLDMConvertingTimeMap[zSliceCount], takenTimeUnit, timeUnit);
    } else {
        return 0;
    }
}

auto TakenUnitTimeToProcess::GetBFLDMConvertingTime(const int32_t& zSliceCount, const TimeUnit& timeUnit) -> double {
    if (d->bfLDMConvertingTimeMap.contains(zSliceCount)) {
        return ConvertUnit(d->bfLDMConvertingTimeMap[zSliceCount], takenTimeUnit, timeUnit);
    } else {
        return 0;
    }
}
