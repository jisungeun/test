#define NOMINMAX
#define LOGGER_TAG "[ProcessorHT]"

#include "ProcessorHT.h"

#include <iostream>
#include <QDir>
#include <QCoreApplication>
#include <QStandardPaths>

#include <AlgorithmVersionReader.h>

#include "arrayfire.h"
#include "DataFolderPathGetter.h"
#include "HDF5Mutex.h"
#include "HTProcessingProfileVersion.h"
#include "PSFProfileVersion.h"

#include "HTProcessorInput.h"
#include "HTProcessorMatlabApp.h"
#include "HTWriterHDF5.h"
#include "HTWriterInput.h"
#include "IHTProcessorOutput.h"
#include "IHTWriterOutput.h"
#include "ResultFilePathGenerator.h"
#include "TCFDataSetRecordedPosition.h"
#include "TCLogger.h"

#include "ProfileFileFinder.h"

using namespace TC::PSFProfile;
using namespace TC::HTProcessingProfile;
using namespace TC::IO::ProfileIO;

const QString moduleFolderName = "HTProc";
const QString matlabAppProgram = "TCHTProcessingMatlabApp.exe";
const QString htLatestModuleFile = "HTModule_v1_4_1_d.ctf";
const QString psfLatestModuleFile = "PSFModule_v1_4_1_c.ctf";

struct CropIndex {
    int32_t startIndexX{};
    int32_t endIndexX{};
    int32_t startIndexY{};
    int32_t endIndexY{};
};

auto CalculateCropIndex(const AcquisitionConfig::ImageInfo& imageInfo)->CropIndex {
    const auto startIndexX = imageInfo.roiOffsetXPixels;
    const auto endIndexX = imageInfo.roiOffsetXPixels + imageInfo.roiSizeXPixels - 1;

    const auto startIndexY = imageInfo.roiOffsetYPixels;
    const auto endIndexY = imageInfo.roiOffsetYPixels + imageInfo.roiSizeYPixels - 1;

    return { startIndexX, endIndexX, startIndexY , endIndexY };
}

class HTProcessorOutput final : public IHTProcessorOutput {
public:
    HTProcessorOutput() = default;
    ~HTProcessorOutput() = default;
    auto SetHTProcessorResult(const HTProcessorResult& htProcessorResult) -> void override {
        this->htProcessorResult = htProcessorResult;
    }

    auto GetHTWriterInput()->HTWriterInput {
        const auto data = htProcessorResult.GetData();
        const auto sizeX = htProcessorResult.GetDataSizeX();
        const auto sizeY = htProcessorResult.GetDataSizeY();
        const auto sizeZ = htProcessorResult.GetDataSizeZ();

        const auto pixelWorldSizeX = htProcessorResult.GetPixelWorldSizeX(LengthUnit::Micrometer);
        const auto pixelWorldSizeY = htProcessorResult.GetPixelWorldSizeY(LengthUnit::Micrometer);
        const auto pixelWorldSizeZ = htProcessorResult.GetPixelWorldSizeZ(LengthUnit::Micrometer);

        HTWriterInput input;
        input.SetData(data);
        input.SetDataSize(sizeX, sizeY, sizeZ);
        input.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);

        return input;
    }

private:
    HTProcessorResult htProcessorResult;
};

class HTWriterOutput final : public IHTWriterOutput {
public:
    HTWriterOutput() = default;
    ~HTWriterOutput() = default;

    auto SetHTWriterResult(const HTWriterResult& htWriterResult) -> void override {
        this->result = htWriterResult;
    }

    HTWriterResult result;
};

auto GenerateMIPHT(const std::shared_ptr<float[]>& data3d, const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ)->std::shared_ptr<float[]> {
    af::array data3dArray{ sizeY, sizeX, sizeZ, data3d.get() };

    const auto mipData = af::max(data3dArray, 2);
    mipData.eval();

    std::shared_ptr<float[]> dataMip{ new float[sizeX * sizeY]() };

    mipData.host(dataMip.get());

    return dataMip;
}

class ProcessorHT::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    QString backgroundFolderPath{};
    QString psfFolderPath{};

    QString matlabModuleFolderPath{};

    AcquisitionConfig acquisitionConfig;
    bool sampleImageCropFlag{ false };

    auto GetMatchedHTModuleFilePath(const QString& moduleFolderPath, const QString& htProcessingProfileFilePath)->QString;
    auto GetMatchedPSFModuleFilePath(const QString& moduleFolderPath, const QString& psfProfileFilePath)->QString;
};

auto ProcessorHT::Impl::GetMatchedHTModuleFilePath(const QString& moduleFolderPath, 
    const QString& htProcessingProfileFilePath) -> QString {
    const auto latestModuleFilePath = moduleFolderPath + "/" + htLatestModuleFile;

    if (htProcessingProfileFilePath.isEmpty()) {
        return latestModuleFilePath;
    }

    AlgorithmVersionReader versionReader;
    versionReader.SetFilePath(htProcessingProfileFilePath);
    if (!versionReader.Read()) {
        return latestModuleFilePath;
    }
    const auto version = versionReader.GetVersion();

    QString htModuleFilePath;
    if (version == "1.4.1d") {
        htModuleFilePath = moduleFolderPath + "/HTModule_v1_4_1_d.ctf";
    } else {
        htModuleFilePath = latestModuleFilePath;
    }

    return htModuleFilePath;
}

auto ProcessorHT::Impl::GetMatchedPSFModuleFilePath(const QString& moduleFolderPath, 
    const QString& psfProfileFilePath) -> QString {
    const auto latestModuleFilePath = moduleFolderPath + "/" + psfLatestModuleFile;

    if (psfProfileFilePath.isEmpty()) {
        return latestModuleFilePath;
    }

    AlgorithmVersionReader versionReader;
    versionReader.SetFilePath(psfProfileFilePath);
    if (!versionReader.Read()) {
        return latestModuleFilePath;
    }
    const auto version = versionReader.GetVersion();

    QString psfModuleFilePath;
    if (version == "1.4.1c") {
        psfModuleFilePath = moduleFolderPath + "/PSFModule_v1_4_1_c.ctf";
    } else {
        psfModuleFilePath = latestModuleFilePath;
    }

    return psfModuleFilePath;
}

ProcessorHT::ProcessorHT() : d(new Impl()) {
}

ProcessorHT::~ProcessorHT() = default;

auto ProcessorHT::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto ProcessorHT::SetBackgroundFolderPath(const QString& backgroundFolderPath) -> void {
    d->backgroundFolderPath = backgroundFolderPath;
}

auto ProcessorHT::SetMatlabModuleFolderPath(const QString& matlabModuleFolderPath) -> void {
    d->matlabModuleFolderPath = matlabModuleFolderPath;
}

auto ProcessorHT::SetPsfFolderPath(const QString& psfFolderPath) -> void {
    d->psfFolderPath = psfFolderPath;
}

auto ProcessorHT::SetAcquisitionConfig(const AcquisitionConfig& acquisitionConfig) -> void {
    d->acquisitionConfig = acquisitionConfig;
}

auto ProcessorHT::SetSampleImageCropFlag(const bool& sampleImageCropFlag) -> void {
    d->sampleImageCropFlag = sampleImageCropFlag;
}

auto ProcessorHT::Process(const int32_t& tileIndex, const int32_t& timeIndex) -> bool {
    try {
        const auto& deviceInfo = d->acquisitionConfig.GetDeviceInfo();
        const auto& acquisitionSetting = d->acquisitionConfig.GetAcquisitionSetting();
        const auto& imageInfo = d->acquisitionConfig.GetImageInfo();

        const QString processingModuleFolderPath = QCoreApplication::applicationDirPath() + "/" + moduleFolderName;
        QString appPath = processingModuleFolderPath + "/" + matlabAppProgram;
        if (!QFile::exists(appPath)) {
            appPath = QCoreApplication::applicationDirPath() + "/" + matlabAppProgram;
        }

        if (!QFile::exists(appPath)) {
            QLOG_ERROR() << "processing application doesn't exists : " << appPath;
            return false;
        }

        constexpr auto sampleStartIndexX = 0;
        constexpr auto sampleStartIndexY = 0;
        const auto backgroundStartIndexX = imageInfo.roiOffsetXPixels;
        const auto backgroundStartIndexY = imageInfo.roiOffsetYPixels;

        const auto cropSizeX = imageInfo.roiSizeXPixels;
        const auto cropSizeY = imageInfo.roiSizeYPixels;

        const HTProcessorInput::CropInfo sampleCropInfo{ sampleStartIndexX, sampleStartIndexY, cropSizeX, cropSizeY };
        const HTProcessorInput::CropInfo backgroundCropInfo{ backgroundStartIndexX, backgroundStartIndexY, cropSizeX, cropSizeY };

        const ModalityType modalityType{ ModalityType::Name::HT };

        DataFolderPathGetter dataFolderPathGetter;
        dataFolderPathGetter.SetRootFolderPath(d->rootFolderPath);
        dataFolderPathGetter.SetModalityType(modalityType);
        dataFolderPathGetter.SetTileIndex(tileIndex);
        dataFolderPathGetter.SetTimeIndex(timeIndex);

        if (!dataFolderPathGetter.ExistTimeFrameFolder()) {
            QLOG_ERROR() << "dataFolderPathGetter.ExistTimeFrameFolder() fails in Process()";
            return false;
        }

        const auto sampleDataFolderPath = dataFolderPathGetter.GetTimeFrameFolderPath();

        QLOG_INFO() << "HT processing tile(" << tileIndex << ") time(" << timeIndex << ")";
        QLOG_INFO() << sampleDataFolderPath;

        const QString processingTempFolderPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation) + "/temp/HT";
        if (!QDir().exists(processingTempFolderPath)) {
            QDir().mkpath(processingTempFolderPath);
        }

        HTProcessorInput htProcessorInput;
        htProcessorInput.SetSampleDataFolderPath(sampleDataFolderPath);
        htProcessorInput.SetBackgroundDataFolderPath(d->backgroundFolderPath);
        htProcessorInput.SetObjectiveNA(deviceInfo.objectiveNA);
        htProcessorInput.SetCondenserNA(deviceInfo.condenserNA);
        htProcessorInput.SetPixelSizeOfImagingSensor(deviceInfo.pixelSizeMicrometer, LengthUnit::Micrometer);
        htProcessorInput.SetMagnificationOfSystem(deviceInfo.magnification);
        htProcessorInput.SetZStepLength(acquisitionSetting.htZStepLengthMicrometer, LengthUnit::Micrometer);
        htProcessorInput.SetMediumRI(deviceInfo.mediumRI);
        htProcessorInput.SetCropInfo(sampleCropInfo, backgroundCropInfo);

        const IHTProcessorOutput::Pointer htProcessorOutputPointer{ new HTProcessorOutput };

        ProfileFileFinder profileFileFinder;
        profileFileFinder.SetRootFolderPath(d->rootFolderPath);

        QString htProcessingProfileFilePath{}, psfProfileFilePath{};
        if (profileFileFinder.Find()) {
            htProcessingProfileFilePath = profileFileFinder.GetHTProcessingProfileFilePath();
            psfProfileFilePath = profileFileFinder.GetPSFProfileFilePath();
        } 

        const auto htProcessingModuleFilePath = d->GetMatchedHTModuleFilePath(processingModuleFolderPath, htProcessingProfileFilePath);
        const auto psfModuleFilePath = d->GetMatchedPSFModuleFilePath(processingModuleFolderPath, psfProfileFilePath);

        HTProcessorMatlabApp htProcessorMatlabApp;
        htProcessorMatlabApp.SetHTProcessingModuleFilePath(htProcessingModuleFilePath);
        htProcessorMatlabApp.SetPSFModuleFilePath(psfModuleFilePath);
        htProcessorMatlabApp.SetPsfFolderPath(d->psfFolderPath);
        htProcessorMatlabApp.SetHTProcessorInput(htProcessorInput);
        htProcessorMatlabApp.SetOutputPort(htProcessorOutputPointer);
        htProcessorMatlabApp.SetProcessingAppPath(appPath);
        htProcessorMatlabApp.SetProcessingTempFolderPath(processingTempFolderPath);
        htProcessorMatlabApp.SetHTProcessingProfileFilePath(htProcessingProfileFilePath);
        htProcessorMatlabApp.SetPSFProfileFilePath(psfProfileFilePath);

        if (!htProcessorMatlabApp.Process()) {
            QLOG_ERROR() << "htProcessorMatlabApp.Process() fails";
            return false;
        }
        // Processed Data Writing
        HTWriterInput htWriterInput = std::dynamic_pointer_cast<HTProcessorOutput>(htProcessorOutputPointer)->GetHTWriterInput();

        const IHTWriterOutput::Pointer htWriterOutputPointer{ new HTWriterOutput };

        const QString writingTempFolderPath = d->rootFolderPath + "/temp/HT";
        if (!QDir().mkpath(writingTempFolderPath)) {
            return false;
        }

        const QString htProcessedDataFilePath = 
            ResultFilePathGenerator::GetHTProcessedFilePath(d->rootFolderPath, tileIndex, timeIndex);

        HTWriterHDF5 writer;
        writer.SetTargetFilePath(htProcessedDataFilePath);
        writer.SetInput(htWriterInput);
        writer.SetOutputPort(htWriterOutputPointer);

        {
            TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
            if (!writer.Write()) {
                QLOG_ERROR() << "writer.Write() fails";
                return false;
            }
        }

        return true;
    } catch (const std::exception& exception) {
        QLOG_ERROR() << exception.what();
        return false;
    }
}
