#include "ProfileFileFinder.h"

#include <QDir>
#include <QSet>

#include <ProfileFileNamer.h>

using namespace TC::ProfileIO;
using namespace TC::PSFProfile;
using namespace TC::HTProcessingProfile;

class ProfileFileFinder::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};

    QString htProcessingProfileFilePath{};
    QString psfProfileFilePath{};

    auto GetPSFExtensionList()->QStringList;
    auto GetHTProcessingExtensionList()->QStringList;
};

auto ProfileFileFinder::Impl::GetPSFExtensionList() -> QStringList {
    constexpr auto psfVersionCount = static_cast<int32_t>(PSFProfileVersion::_size());

    QSet<QString> extensionSet;
    for (auto versionIndex = 0; versionIndex < psfVersionCount; ++versionIndex) {
        const auto psfVersion = PSFProfileVersion::_from_index(versionIndex);
        const auto extension = ProfileFileNamer::GetExtension(psfVersion);
        if (!extension.isEmpty()) {
            extensionSet.insert(extension);
        }
    }

    return extensionSet.values();
}

auto ProfileFileFinder::Impl::GetHTProcessingExtensionList() -> QStringList {
    constexpr auto htProcessingVersionCount = static_cast<int32_t>(HTProcessingProfileVersion::_size());

    QSet<QString> extensionSet;
    for (auto versionIndex = 0; versionIndex < htProcessingVersionCount; ++versionIndex) {
        const auto htProcessingVersion = HTProcessingProfileVersion::_from_index(versionIndex);
        const auto extension = ProfileFileNamer::GetExtension(htProcessingVersion);
        if (!extension.isEmpty()) {
            extensionSet.insert(extension);
        }
    }

    return extensionSet.values();
}

ProfileFileFinder::ProfileFileFinder() : d{ std::make_unique<Impl>() } {
}

ProfileFileFinder::~ProfileFileFinder() = default;

auto ProfileFileFinder::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->htProcessingProfileFilePath.clear();
    d->psfProfileFilePath.clear();

    d->rootFolderPath = rootFolderPath;
}

auto ProfileFileFinder::Find() -> bool {
    if (d->rootFolderPath.isEmpty()) {
        return false;
    }
    if (!QDir(d->rootFolderPath).exists()) {
        return false;
    }
    const auto profileFolderPath = QString("%1/profile").arg(d->rootFolderPath);
    if (!QDir(profileFolderPath).exists()) {
        return false;
    }

    const QDir profileFolder(profileFolderPath);

    const auto psfExtensionList = d->GetPSFExtensionList();
    QStringList psfProfileFileFilterList;
    for(const auto& psfExtension : psfExtensionList) {
        const auto filter = QString("*.%1").arg(psfExtension);
        psfProfileFileFilterList.push_back(filter);
    }

    const auto htProcessingExtensionList = d->GetHTProcessingExtensionList();
    QStringList htProcessingProfileFileFilterList;
    for (const auto& htProcessingExtension : htProcessingExtensionList) {
        const auto filter = QString("*.%1").arg(htProcessingExtension);
        htProcessingProfileFileFilterList.push_back(filter);
    }

    const auto psfProfileInfoList = profileFolder.entryInfoList(psfProfileFileFilterList);
    const auto htProcessingProfileInfoList = profileFolder.entryInfoList(htProcessingProfileFileFilterList);

    if (psfProfileInfoList.size() != 1) { return false; }
    if (htProcessingProfileInfoList.size() != 1) { return false; }

    d->psfProfileFilePath = psfProfileInfoList.first().absoluteFilePath();
    d->htProcessingProfileFilePath = htProcessingProfileInfoList.first().absoluteFilePath();

    return true;
}

auto ProfileFileFinder::GetHTProcessingProfileFilePath() const -> QString {
    return d->htProcessingProfileFilePath;
}

auto ProfileFileFinder::GetPSFProfileFilePath() const -> QString {
    return d->psfProfileFilePath;
}
