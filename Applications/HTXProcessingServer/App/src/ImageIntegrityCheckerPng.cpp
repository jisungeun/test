#include "ImageIntegrityCheckerPng.h"
#include <QDir>
struct ImageIntegrityCheckerPng::Impl {
    Impl() = default;
    ~Impl() = default;

    QString imageFolderPath{};
    uint32_t numberOfImages{0};
};

ImageIntegrityCheckerPng::ImageIntegrityCheckerPng()
    : d(new Impl()) {
}

ImageIntegrityCheckerPng::~ImageIntegrityCheckerPng() = default;

auto ImageIntegrityCheckerPng::SetImageFolderPath(const QString& imageFolderPath) -> void {
    d->imageFolderPath = imageFolderPath;
}

auto ImageIntegrityCheckerPng::SetNumberOfImages(const uint32_t& numberOfImages) -> void {
    d->numberOfImages = numberOfImages;
}

auto ImageIntegrityCheckerPng::CheckSameCount() -> bool {
    const QDir folderDir(d->imageFolderPath);
    const auto pngNameList = folderDir.entryList(QStringList{ "*.png" }, QDir::Files);
    const auto imageIntegrity = pngNameList.size() == static_cast<int32_t>(d->numberOfImages);

    return imageIntegrity;
}

auto ImageIntegrityCheckerPng::CheckMoreThanCount() -> bool {
    const QDir folderDir(d->imageFolderPath);
    const auto pngNameList = folderDir.entryList(QStringList{ "*.png" }, QDir::Files);
    const auto imageIntegrity = pngNameList.size() >= static_cast<int32_t>(d->numberOfImages);

    return imageIntegrity;
}
