#pragma once    
#include <QString>
#include <QMap>
#include "enum.h"

#include "SIUnit.h"

BETTER_ENUM(ProcessingStatus, uint8_t, WaitForProcess, WaitForAcquisition, IsProcessing, AlreadyProcessed, 
    ProcessingFail, ProperlyProcessed, UnhandledStatus)
BETTER_ENUM(DataAcquisitionStatus, uint8_t, NotFullyAcquired, FullyAcquired, BackgroundFault)

class SampleObject {
public:
    bool tcfExist{ false };

    QString rootPath{};

    double remainingTime{};
    const TimeUnit remainingTimeUnit{ TimeUnit::Second };

    DataAcquisitionStatus dataAcquisitionStatus{ DataAcquisitionStatus::NotFullyAcquired };
    ProcessingStatus processingStatus{ ProcessingStatus::WaitForProcess };
};
