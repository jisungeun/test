#pragma once

#include <memory>
#include <QString>
#include <QStringList>

class ProfileFileFinder {
public:
    ProfileFileFinder();
    ~ProfileFileFinder();

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto Find()->bool;

    auto GetHTProcessingProfileFilePath()const->QString;
    auto GetPSFProfileFilePath()const->QString;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};