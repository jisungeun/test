#include "TilePositionSetReader.h"

#include <QFile>

#include "H5Cpp.h"
#include <QMap>

class TilePositionSetReader::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString filePath{};

    TilePositionSet tilePositionSet{};

};

TilePositionSetReader::TilePositionSetReader() : d(new Impl()) {
}

TilePositionSetReader::~TilePositionSetReader() = default;

auto TilePositionSetReader::SetTargetFilePath(const QString& filePath) -> void {
    d->filePath = filePath;
}

auto TilePositionSetReader::Read() -> bool {
    if (d->filePath.isEmpty()) { return false; }
    if (!QFile::exists(d->filePath)) { return false; }
    if (!H5::H5File::isHdf5(d->filePath.toStdString())) { return false; }
    
    const H5::H5File file{ d->filePath.toStdString(), H5F_ACC_RDONLY };

    const auto numberOfAttributes = file.getNumAttrs();
    if (numberOfAttributes == 0) { return false; }

    QMap<QString, int32_t> tilePositionXAttrMap;
    QMap<QString, int32_t> tilePositionYAttrMap;

    for (auto attrIndex = 0; attrIndex < numberOfAttributes; ++attrIndex) {
        const auto attribute = file.openAttribute(attrIndex);
        const auto attributeName = QString(attribute.getName().c_str());

        if (!attributeName.contains("tilePosition")) { continue; }

        int32_t attributeValue;
        attribute.read(attribute.getDataType(), &attributeValue);

        if (attributeName.contains("tilePositionX")) { tilePositionXAttrMap[attributeName] = attributeValue; }
        if (attributeName.contains("tilePositionY")) { tilePositionYAttrMap[attributeName] = attributeValue; }
    }

    if (tilePositionXAttrMap.isEmpty()) { return false; }
    if (tilePositionYAttrMap.isEmpty()) { return false; }
    if (tilePositionXAttrMap.size() != tilePositionYAttrMap.size()) { return false; }

    int32_t maxTileIndexX{ -1 }, maxTileIndexY{ -1 };
    for (auto xMapIterator = tilePositionXAttrMap.begin(); xMapIterator != tilePositionXAttrMap.end(); ++xMapIterator) {
        const auto& tilePositionXName = xMapIterator.key();

        const auto nameSplitList = tilePositionXName.split("_");
        const auto& xIndexString = nameSplitList.at(1);
        const auto& yIndexString = nameSplitList.at(2);

        const auto tileIndexX = xIndexString.right(xIndexString.size() - 1).toInt();
        const auto tileIndexY = yIndexString.right(yIndexString.size() - 1).toInt();

        maxTileIndexX = std::max<int32_t>(tileIndexX, maxTileIndexX);
        maxTileIndexY = std::max<int32_t>(tileIndexY, maxTileIndexY);
    }

    if (maxTileIndexX == -1) { return false; }
    if (maxTileIndexY == -1) { return false; }

    const auto tileNumberX = maxTileIndexX + 1;
    const auto tileNumberY = maxTileIndexY + 1;
    const auto tileNumber = tileNumberX * tileNumberY;

    if (tilePositionXAttrMap.size() != tileNumber) { return false; }
    if (tilePositionYAttrMap.size() != tileNumber) { return false; }

    TilePositionSet tilePositionSet;
    tilePositionSet.SetTileNumber(tileNumberX, tileNumberY);

    for (auto tileIndexX = 0; tileIndexX < tileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < tileNumberY; ++tileIndexY) {
            const auto tilePositionXString = QString("tilePositionX_x%1_y%2").arg(tileIndexX).arg(tileIndexY);
            const auto tilePositionYString = QString("tilePositionY_x%1_y%2").arg(tileIndexX).arg(tileIndexY);

            if (!tilePositionXAttrMap.contains(tilePositionXString)) { return false; }
            if (!tilePositionYAttrMap.contains(tilePositionYString)) { return false; }

            const auto tilePositionX = static_cast<float>(tilePositionXAttrMap[tilePositionXString]);
            const auto tilePositionY = static_cast<float>(tilePositionYAttrMap[tilePositionYString]);
            
            TilePosition tilePosition;
            tilePosition.SetPositions(tilePositionX, tilePositionY, 0);
            tilePositionSet.InsertTilePosition(tileIndexX, tileIndexY, tilePosition);
        }
    }

    d->tilePositionSet = tilePositionSet;

    return true;
}

auto TilePositionSetReader::GetTilePositionSet() const -> TilePositionSet {
    return d->tilePositionSet;
}
