#include "TaskInfoSet.h"

#include <vector>
#include <QMap>

#include "ModalityType.h"
#include "ProcessType.h"

using FLChannelIndex = int32_t;

class TaskInfoSet::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    std::vector<std::vector<TaskInfo>> htProcessedTaskInfoVector{};
    QMap<FLChannelIndex, std::vector<std::vector<TaskInfo>>> flProcessedTaskInfoMap{};
    std::vector<std::vector<TaskInfo>> bfProcessedTaskInfoVector{};

    std::vector<TaskInfo> htStitchingDoneTaskInfoVector{};
    QMap<FLChannelIndex, std::vector<TaskInfo>> flStitchingDoneTaskInfoMap{};
    std::vector<TaskInfo> bfStitchingDoneTaskInfoVector{};

    std::vector<TaskInfo> htThumbnailWrittenDoneTaskInfoVector{};
    QMap<FLChannelIndex, std::vector<TaskInfo>> flThumbnailWrittenDoneTaskInfoMap{};
    std::vector<TaskInfo> bfThumbnailWrittenDoneTaskInfoVector{};

    std::vector<TaskInfo> htStitchingWrittenDoneTaskInfoVector{};
    QMap<FLChannelIndex, std::vector<TaskInfo>> flStitchingWrittenDoneTaskInfoMap{};
    std::vector<TaskInfo> bfStitchingWrittenDoneTaskInfoVector{};

    std::vector<TaskInfo> htLDMConvertingDoneTaskInfoVector{};
    QMap<FLChannelIndex, std::vector<TaskInfo>> flLDMConvertingDoneTaskInfoMap{};
    std::vector<TaskInfo> bfLDMConvertingDoneTaskInfoVector{};

    TaskInfo tcfWrittenTaskInfo{};
    TaskInfo dataRemovingTaskInfo{};
};

TaskInfoSet::TaskInfoSet() : d(new Impl()) {
}

TaskInfoSet::TaskInfoSet(const TaskInfoSet& other) : d(new Impl(*other.d)) {
}

TaskInfoSet::~TaskInfoSet() = default;

auto TaskInfoSet::operator=(const TaskInfoSet& other) -> TaskInfoSet& {
    *(this->d) = *(other.d);
    return *this;
}

auto TaskInfoSet::Initialize(const AcquisitionCount& acquisitionCount) -> void {
    d = std::make_unique<Impl>();

    const auto [tileNumberX, tileNumberY] = acquisitionCount.GetTileNumber();
    const auto tileNumber = tileNumberX * tileNumberY;

    const auto acquisitionSequenceInfo = acquisitionCount.GetAcquisitionSequenceInfo();

    const auto timeFrameCount = acquisitionSequenceInfo.GetTimeFrameCount();
    
    const std::vector defaultTimeTaskInfoVector(timeFrameCount, TaskInfo{});
    const std::vector defaultTileTaskInfoVector(tileNumber, defaultTimeTaskInfoVector);

    d->htProcessedTaskInfoVector = defaultTileTaskInfoVector;
    d->htStitchingDoneTaskInfoVector = defaultTimeTaskInfoVector;
    d->htThumbnailWrittenDoneTaskInfoVector = defaultTimeTaskInfoVector;
    d->htStitchingWrittenDoneTaskInfoVector = defaultTimeTaskInfoVector;
    d->htLDMConvertingDoneTaskInfoVector = defaultTimeTaskInfoVector;

    for (auto flChannelIndex = 0; flChannelIndex < 3; ++flChannelIndex) {
        d->flProcessedTaskInfoMap[flChannelIndex] = defaultTileTaskInfoVector;
        d->flStitchingDoneTaskInfoMap[flChannelIndex] = defaultTimeTaskInfoVector;
        d->flThumbnailWrittenDoneTaskInfoMap[flChannelIndex] = defaultTimeTaskInfoVector;
        d->flStitchingWrittenDoneTaskInfoMap[flChannelIndex] = defaultTimeTaskInfoVector;
        d->flLDMConvertingDoneTaskInfoMap[flChannelIndex] = defaultTimeTaskInfoVector;
    }

    d->bfProcessedTaskInfoVector = defaultTileTaskInfoVector;
    d->bfStitchingDoneTaskInfoVector = defaultTimeTaskInfoVector;
    d->bfThumbnailWrittenDoneTaskInfoVector = defaultTimeTaskInfoVector;
    d->bfStitchingWrittenDoneTaskInfoVector = defaultTimeTaskInfoVector;
    d->bfLDMConvertingDoneTaskInfoVector = defaultTimeTaskInfoVector;
}

auto TaskInfoSet::SetTaskInfo(const int32_t& timeIndex, const int32_t& tileIndex, const ModalityType& modalityType,
    const ProcessType& processType, const TaskInfo& taskInfo) -> void {
    if (modalityType.GetName() == ModalityType::Name::HT) {
        if (processType == +ProcessType::TileProcessing) {
            d->htProcessedTaskInfoVector[tileIndex][timeIndex] = taskInfo;
            return;
        }

        if (processType == +ProcessType::Stitching) {
            d->htStitchingDoneTaskInfoVector[timeIndex] = taskInfo;
            return;
        }

        if (processType == +ProcessType::StitchingWriting) {
            d->htStitchingWrittenDoneTaskInfoVector[timeIndex] = taskInfo;
            return;
        }

        if (processType == +ProcessType::ThumbnailWriting) {
            //TODO 
            return;
        }

        if (processType == +ProcessType::LDMConversion) {
            d->htLDMConvertingDoneTaskInfoVector[timeIndex] = taskInfo;
            return;
        }
    }

    const auto flChannelIndex = modalityType.GetChannelIndex();
    if (modalityType.GetName() == ModalityType::Name::FL) {
        if (processType == +ProcessType::TileProcessing) {
            d->flProcessedTaskInfoMap[flChannelIndex][tileIndex][timeIndex] = taskInfo;
            return;
        }

        if (processType == +ProcessType::Stitching) {
            d->flStitchingDoneTaskInfoMap[flChannelIndex][timeIndex] = taskInfo;
            return;
        }

        if (processType == +ProcessType::StitchingWriting) {
            d->flStitchingWrittenDoneTaskInfoMap[flChannelIndex][timeIndex] = taskInfo;
            return;
        }

        if (processType == +ProcessType::ThumbnailWriting) {
            // TODO
            return;
        }

        if (processType == +ProcessType::LDMConversion) {
            d->flLDMConvertingDoneTaskInfoMap[flChannelIndex][timeIndex] = taskInfo;
            return;
        }
    }

    if (modalityType.GetName() == ModalityType::Name::BF) {
        if (processType == +ProcessType::TileProcessing) {
            d->bfProcessedTaskInfoVector[tileIndex][timeIndex] = taskInfo;
            return;
        }

        if (processType == +ProcessType::Stitching) {
            d->bfStitchingDoneTaskInfoVector[timeIndex] = taskInfo;
            return;
        }

        if (processType == +ProcessType::StitchingWriting) {
            d->bfStitchingWrittenDoneTaskInfoVector[timeIndex] = taskInfo;
            return;
        }

        if (processType == +ProcessType::ThumbnailWriting) {
            // TODO
            return;
        }

        if (processType == +ProcessType::LDMConversion) {
            d->bfLDMConvertingDoneTaskInfoVector[timeIndex] = taskInfo;
            return;
        }
    }

    if (processType == +ProcessType::TCFWriting) {
        d->tcfWrittenTaskInfo = taskInfo;
    }

    if (processType == +ProcessType::DataRemoving) {
        d->dataRemovingTaskInfo = taskInfo;
    }
}

auto TaskInfoSet::GetTaskInfo(const int32_t& timeIndex, const int32_t& tileIndex, const ModalityType& modalityType,
    const ProcessType& processType) const -> TaskInfo {
    TaskInfo errorTaskInfo;
    errorTaskInfo.SetFailed(true);
    errorTaskInfo.SetOnProcessing(false);
    errorTaskInfo.SetProcessedProgress(1);
    errorTaskInfo.SetProcessedTime(0, TimeUnit::Second);

    if (modalityType.GetName() == ModalityType::Name::HT) {
        if (processType == +ProcessType::TileProcessing) {
            return d->htProcessedTaskInfoVector[tileIndex][timeIndex];
        }

        if (processType == +ProcessType::Stitching) {
            return d->htStitchingDoneTaskInfoVector[timeIndex];
        }

        if (processType == +ProcessType::StitchingWriting) {
            return d->htStitchingWrittenDoneTaskInfoVector[timeIndex];
        }

        if (processType == +ProcessType::ThumbnailWriting) {
            //TODO
            return errorTaskInfo;
        }

        if (processType == +ProcessType::LDMConversion) {
            return d->htLDMConvertingDoneTaskInfoVector[timeIndex];
        }


    }

    const auto flChannelIndex = modalityType.GetChannelIndex();
    if (modalityType.GetName() == ModalityType::Name::FL) {
        if (processType == +ProcessType::TileProcessing) {
            return d->flProcessedTaskInfoMap[flChannelIndex][tileIndex][timeIndex];
        }

        if (processType == +ProcessType::Stitching) {
            return d->flStitchingDoneTaskInfoMap[flChannelIndex][timeIndex];
        }

        if (processType == +ProcessType::StitchingWriting) {
            return d->flStitchingWrittenDoneTaskInfoMap[flChannelIndex][timeIndex];
        }

        if (processType == +ProcessType::ThumbnailWriting) {
            // TODO
            return errorTaskInfo;
        }

        if (processType == +ProcessType::LDMConversion) {
            return d->flLDMConvertingDoneTaskInfoMap[flChannelIndex][timeIndex];
        }
    }

    if (modalityType.GetName() == ModalityType::Name::BF) {
        if (processType == +ProcessType::TileProcessing) {
            return d->bfProcessedTaskInfoVector[tileIndex][timeIndex];
        }

        if (processType == +ProcessType::Stitching) {
            return d->bfStitchingDoneTaskInfoVector[timeIndex];
        }

        if (processType == +ProcessType::StitchingWriting) {
            return d->bfStitchingWrittenDoneTaskInfoVector[timeIndex];
        }

        if (processType == +ProcessType::ThumbnailWriting) {
            // TODO
            return errorTaskInfo;
        }

        if (processType == +ProcessType::LDMConversion) {
            return d->bfLDMConvertingDoneTaskInfoVector[timeIndex];
        }
    }

    if (processType == +ProcessType::TCFWriting) {
        return d->tcfWrittenTaskInfo;
    }

    if (processType == +ProcessType::DataRemoving) {
        return d->dataRemovingTaskInfo;
    }

    return errorTaskInfo;
}