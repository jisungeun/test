#pragma once

#include <QList>
#include "SampleObject.h"

class SampleObjectGenerator {
public:
    static auto IsGenerating()->bool;
    static auto Generate()->QList<SampleObject>;
};