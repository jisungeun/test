#pragma once

class IImageIntegrityChecker {
public:
    virtual ~IImageIntegrityChecker() = default;
    virtual auto CheckSameCount()->bool = 0;
};