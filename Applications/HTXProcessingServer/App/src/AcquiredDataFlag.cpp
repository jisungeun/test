#include "AcquiredDataFlag.h"

#include <QMap>

using ExistFlag = bool;
using FLChannelIndex = int32_t;

class AcquiredDataFlag::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    bool bgAcquiredFlag{ false };
    std::vector<std::vector<ExistFlag>> htAcquiredFlagVector{};
    QMap<FLChannelIndex, std::vector<std::vector<ExistFlag>>> flAcquiredFlagMap{};
    std::vector<std::vector<ExistFlag>> bfAcquiredFlagVector{};
};

AcquiredDataFlag::AcquiredDataFlag() : d(std::make_unique<Impl>()) {
}

AcquiredDataFlag::AcquiredDataFlag(const AcquiredDataFlag& other) : d(std::make_unique<Impl>(*other.d)) {
}

AcquiredDataFlag::~AcquiredDataFlag() = default;

auto AcquiredDataFlag::operator=(const AcquiredDataFlag& other) -> AcquiredDataFlag& {
    *(this->d) = *(other.d);
    return *this;
}

auto AcquiredDataFlag::Initialize(const AcquisitionCount& acquisitionCount) -> void {
    d->htAcquiredFlagVector.clear();
    d->flAcquiredFlagMap.clear();
    d->bfAcquiredFlagVector.clear();

    const auto [tileNumberX, tileNumberY] = acquisitionCount.GetTileNumber();
    const auto tileNumber = tileNumberX * tileNumberY;

    const auto acquisitionSequenceInfo = acquisitionCount.GetAcquisitionSequenceInfo();

    const auto& timeFrameCount = acquisitionSequenceInfo.GetTimeFrameCount();

    const std::vector defaultTimeFlagVector(timeFrameCount, false);
    const std::vector defaultTileFlagVector(tileNumber, defaultTimeFlagVector);

    d->htAcquiredFlagVector = defaultTileFlagVector;
    d->bfAcquiredFlagVector = defaultTileFlagVector;
    d->flAcquiredFlagMap[0] = defaultTileFlagVector;
    d->flAcquiredFlagMap[1] = defaultTileFlagVector;
    d->flAcquiredFlagMap[2] = defaultTileFlagVector;
}

auto AcquiredDataFlag::SetBGAcquiredFlag(const bool& flag) -> void {
    d->bgAcquiredFlag = flag;
}

auto AcquiredDataFlag::GetBGAcquiredFlag() const -> bool {
    return d->bgAcquiredFlag;
}

auto AcquiredDataFlag::SetAcquiredFlag(const int32_t& timeIndex, const int32_t& tileIndex,
    const ModalityType& modalityType, const bool& flag) -> void {
    if (modalityType.GetName() == ModalityType::Name::HT) {
        d->htAcquiredFlagVector[tileIndex][timeIndex] = flag;
        return;
    }

    if (modalityType.GetName() == ModalityType::Name::FL) {
        const auto flChannelIndex = modalityType.GetChannelIndex();
        d->flAcquiredFlagMap[flChannelIndex][tileIndex][timeIndex] = flag;
        return;
    }

    if (modalityType.GetName() == ModalityType::Name::BF) {
        d->bfAcquiredFlagVector[tileIndex][timeIndex] = flag;
    }
}

auto AcquiredDataFlag::GetAcquiredFlag(const int32_t& timeIndex, const int32_t& tileIndex,
    const ModalityType& modalityType) const -> bool {
    if (modalityType.GetName() == ModalityType::Name::HT) {
        return d->htAcquiredFlagVector[tileIndex][timeIndex];
    }

    if (modalityType.GetName() == ModalityType::Name::FL) {
        const auto flChannelIndex = modalityType.GetChannelIndex();
        return d->flAcquiredFlagMap[flChannelIndex][tileIndex][timeIndex];
    }

    if (modalityType.GetName() == ModalityType::Name::BF) {
        return  d->bfAcquiredFlagVector[tileIndex][timeIndex];
    }

    return false;
}
