#include "ProcessingOptionManager.h"

#include <optional>
#include <QSettings>

constexpr auto defaultLDMOption = false;
const QString optionRegistryPath = "HKEY_CURRENT_USER\\Software\\TomoCube, Inc.\\HTXProcessingServer\\Options";

const QString ldmFlagString = "LDMFlag";

class ProcessingOptionManager::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    ProcessingOptions options{};

    auto WriteDefaultOptions()->void;
    auto ReadOptions()->std::optional<ProcessingOptions>;
};

auto ProcessingOptionManager::Impl::ReadOptions() -> std::optional<ProcessingOptions> {
    QSettings settings(optionRegistryPath, QSettings::NativeFormat);

    if (!settings.contains(ldmFlagString)) {
        return std::nullopt;
    }

    const auto ldmFlag = settings.value(ldmFlagString, defaultLDMOption ? (1) : (0)).toBool();

    ProcessingOptions processingOptions;
    processingOptions.SetLDMFlag(ldmFlag);

    return processingOptions;
}

auto ProcessingOptionManager::GetInstance() -> std::shared_ptr<ProcessingOptionManager> {
    static auto processingOptionManager = std::shared_ptr<ProcessingOptionManager>{new ProcessingOptionManager};
    return processingOptionManager;
}

ProcessingOptionManager::~ProcessingOptionManager() = default;

auto ProcessingOptionManager::ProcessingOptionsExists() -> bool {
    const auto processingOptionResult = d->ReadOptions();
    return processingOptionResult.has_value();
}

auto ProcessingOptionManager::WriteDefaultOptions() -> void {
    QSettings settings(optionRegistryPath, QSettings::NativeFormat);
    settings.setValue(ldmFlagString, defaultLDMOption ? (1) : (0));
}

auto ProcessingOptionManager::ReadProcessingOptions() -> ProcessingOptions {
    const auto processingOptionResult = d->ReadOptions();
    return processingOptionResult.value();
}

auto ProcessingOptionManager::SetProcessingOptions(const ProcessingOptions& options) -> void {
    d->options = options;
}

auto ProcessingOptionManager::GetProcessingOptions() -> ProcessingOptions {
    return d->options;
}

ProcessingOptionManager::ProcessingOptionManager() : d{ std::make_unique<Impl>() } {
}
