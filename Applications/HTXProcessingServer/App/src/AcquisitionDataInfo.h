#pragma once

#include <memory>
#include <QString>

#include "AcquisitionCount.h"

class AcquisitionDataInfo {
public:
    AcquisitionDataInfo();
    AcquisitionDataInfo(const AcquisitionDataInfo& other);
    ~AcquisitionDataInfo();

    auto operator=(const AcquisitionDataInfo& other)->AcquisitionDataInfo&;

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto GetRootFolderPath()const->const QString&;

    auto SetBackgroundFolderPath(const QString& backgroundFolderPath, const bool& placeInRootFolder)->void;
    auto GetBackgroundFolderPath()const->const QString&;
    auto IsBackgroundPlacedInRootFolder()const->const bool&;

    auto SetAcquisitionCount(const AcquisitionCount& acquisitionCount)->void;
    auto GetAcquisitionCount()const->const AcquisitionCount&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
