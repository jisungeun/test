#include "DataFolderPathGetter.h"

#include <QDir>
#include <QFile>

class DataFolderPathGetter::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    ModalityType modalityType{ModalityType::Name::HT};
    int32_t tileIndex{};
    int32_t timeIndex{};

    auto ToNameString(const ModalityType::Name& name)->QString;
    auto ToModalityString(const ModalityType& modalityType)->QString;

    auto GetTileFolderPath(const bool& newVersion = false) const ->QString;
    auto GetModalityFolderPath(const bool& newVersion = false)->QString;
    auto GetTimeFrameFolderPath(const bool& newVersion)->QString;
};

auto DataFolderPathGetter::Impl::GetTileFolderPath(const bool& newVersion) const -> QString {
    QString tileIndexString;
    if (newVersion) {
        tileIndexString = QString("%1").arg(tileIndex, 4, 10, QChar('0'));
    } else {
        tileIndexString = QString("%1").arg(tileIndex);
    }

    return QString("%1/data/%2").arg(rootFolderPath).arg(tileIndexString);
}

auto DataFolderPathGetter::Impl::GetModalityFolderPath(const bool& newVersion) -> QString {
    const auto tileFolderPath = GetTileFolderPath(newVersion);
    const auto modalityString = ToModalityString(modalityType);

    return QString("%1/%2").arg(tileFolderPath).arg(modalityString);
}

auto DataFolderPathGetter::Impl::GetTimeFrameFolderPath(const bool& newVersion) -> QString {
    const auto modalityFolderPath = this->GetModalityFolderPath(newVersion);

    if (!QDir().exists(modalityFolderPath)) {
        return "";
    }

    if (newVersion) {
        return QString("%1/%2").arg(modalityFolderPath).arg(this->timeIndex, 4, 10, QChar('0')); 
    } else {
        return QString("%1/%2").arg(modalityFolderPath).arg(this->timeIndex);
    }
}

auto DataFolderPathGetter::Impl::ToNameString(const ModalityType::Name& name) -> QString {
    if (name == ModalityType::Name::HT) {
        return "HT";
    } else if (name == ModalityType::Name::FL) {
        return "FL";
    } else if (name == ModalityType::Name::BF) {
        return "BF";
    }
    return "";
}

auto DataFolderPathGetter::Impl::ToModalityString(const ModalityType& modalityType) -> QString {
    const auto name = modalityType.GetName();
    const auto nameString = this->ToNameString(name);

    const auto channelIndex = modalityType.GetChannelIndex();

    if (name == ModalityType::Name::FL) {
        return QString("%1%2/CH%3").arg(nameString).arg("3D").arg(channelIndex);
    } else if (name == ModalityType::Name::HT) {
        return QString("%1%2").arg(nameString).arg("3D");
    } else if (name == ModalityType::Name::BF) {
        return QString("%1").arg(nameString);
    } else {
        return "";
    }
}

DataFolderPathGetter::DataFolderPathGetter() : d(std::make_unique<Impl>()) {
}

DataFolderPathGetter::~DataFolderPathGetter() = default;

auto DataFolderPathGetter::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto DataFolderPathGetter::SetModalityType(const ModalityType& modalityType) -> void {
    d->modalityType = modalityType;
}

auto DataFolderPathGetter::SetTileIndex(const int32_t& tileIndex) -> void {
    d->tileIndex = tileIndex;
}

auto DataFolderPathGetter::SetTimeIndex(const int32_t& timeIndex) -> void {
    d->timeIndex = timeIndex;
}

auto DataFolderPathGetter::ExistTileFolder() const -> bool {
    if (QFile::exists(d->GetTileFolderPath(false))) {
        return true;
    } else if (QFile::exists(d->GetTileFolderPath(true))) {
        return true;
    } else {
        return false;
    }
}

auto DataFolderPathGetter::ExistModalityFolder() const -> bool {
    if (QFile::exists(d->GetModalityFolderPath(false))) {
        return true;
    } else if (QFile::exists(d->GetModalityFolderPath(true))) {
        return true;
    } else {
        return false;
    }
}

auto DataFolderPathGetter::ExistTimeFrameFolder() const -> bool {
    if (QFile::exists(d->GetTimeFrameFolderPath(false))) {
        return true;
    } else if (QFile::exists(d->GetTimeFrameFolderPath(true))) {
        return true;
    } else {
        return false;
    }
}

auto DataFolderPathGetter::GetTileFolderPath() const -> QString {
    if (this->ExistTileFolder()) {
        if (QFile::exists(d->GetTileFolderPath(false))) {
            return d->GetTileFolderPath(false);
        } else {
            return d->GetTileFolderPath(true);
        }
    } else {
        return "";
    }
}

auto DataFolderPathGetter::GetModalityFolderPath() const -> QString {
    if (this->ExistModalityFolder()) {
        if (QFile::exists(d->GetModalityFolderPath(false))) {
            return d->GetModalityFolderPath(false);
        } else {
            return d->GetModalityFolderPath(true);
        }
    } else {
        return "";
    }
}

auto DataFolderPathGetter::GetTimeFrameFolderPath() const -> QString {
    if (this->ExistTimeFrameFolder()) {
        if (QFile::exists(d->GetTimeFrameFolderPath(false))) {
            return d->GetTimeFrameFolderPath(false);
        } else {
            return d->GetTimeFrameFolderPath(true);
        }
    } else {
        return "";
    }
}

auto DataFolderPathGetter::GetProfileFolderPath() const -> QString {
    return QString("%1/profile").arg(d->rootFolderPath);
}
