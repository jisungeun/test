#define LOGGER_TAG "[ThumbnailWriterBF]"

#include "ThumbnailWriterBF.h"
#include "H5Cpp.h"
#include "HDF5Mutex.h"
#include "ResultFilePathGenerator.h"
#include "TCFThumbnailDataSet.h"
#include "TCFThumbnailWriter.h"
#include "TCLogger.h"
#include "ThumbnailBF.h"

class ThumbnailWriterBF::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    QString tempTCFFilePath{};
    int32_t timeFrameIndex{};
    AcquisitionSequenceInfo acquisitionSequenceInfo{};

    std::shared_ptr<uint8_t[]> thumbnailData{};
    int32_t thumbnailSizeX{};
    int32_t thumbnailSizeY{};

    auto ReadBFVoxelSize()->std::tuple<bool, float, float>;
    auto ReadBFData()->std::tuple<bool, std::shared_ptr<uint8_t[]>, int32_t, int32_t>;
    auto GenerateThumbnailData(const std::shared_ptr<uint8_t[]>& bfData, const int32_t& bfDataSizeX,
        const int32_t& bfDataSizeY)->std::tuple<bool, std::shared_ptr<uint8_t[]>, int32_t, int32_t>;
};

auto ThumbnailWriterBF::Impl::ReadBFVoxelSize() -> std::tuple<bool, float, float> {
    const QString bfProcessedDataFilePath = ResultFilePathGenerator::GetBFProcessedFilePath(this->rootFolderPath, 0,
        this->timeFrameIndex);
    try {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        float pixelWorldSizeX{}, pixelWorldSizeY{};

        const H5::H5File processedDataFile(bfProcessedDataFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto processedDataSet = processedDataFile.openDataSet("Data");

        const auto attrPixelWorldSizeX = processedDataSet.openAttribute("pixelWorldSizeX");
        const auto attrPixelWorldSizeY = processedDataSet.openAttribute("pixelWorldSizeY");

        attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
        attrPixelWorldSizeY.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeY);

        return { true, pixelWorldSizeX, pixelWorldSizeY };
    }
    catch (const H5::Exception&) {
        return { false, 0,0 };
    }
}

auto ThumbnailWriterBF::Impl::ReadBFData() -> std::tuple<bool, std::shared_ptr<uint8_t[]>, int32_t, int32_t> {
    const auto stitchingFilePath = ResultFilePathGenerator::GetBFStitchingFilePath(this->rootFolderPath, this->timeFrameIndex);

    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    try {
        const H5::H5File stitchingDataFile(stitchingFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto stitchingDataSet = stitchingDataFile.openDataSet("StitchingData");

        int32_t stitchingDataSizeX{}, stitchingDataSizeY{}, stitchingDataSizeZ{};
        const auto attrDataSizeX = stitchingDataSet.openAttribute("dataSizeX");
        const auto attrDataSizeY = stitchingDataSet.openAttribute("dataSizeY");
        const auto attrDataSizeZ = stitchingDataSet.openAttribute("dataSizeZ");

        attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
        attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);
        attrDataSizeZ.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeZ);

        const auto numberOfElements = static_cast<int64_t>(stitchingDataSizeX) * static_cast<int64_t>(stitchingDataSizeY) *
            static_cast<int64_t>(stitchingDataSizeZ);

        std::shared_ptr<uint8_t[]> bfData{ new uint8_t[numberOfElements] };
        stitchingDataSet.read(bfData.get(), H5::PredType::NATIVE_UINT8);

        std::shared_ptr<uint8_t[]> reorderedBFData;

        if (stitchingDataSizeZ == 1) {
            reorderedBFData = std::shared_ptr<uint8_t[]>{ new uint8_t[numberOfElements * 3] };

            for (int64_t xIndex = 0; xIndex < static_cast<int64_t>(stitchingDataSizeX); ++xIndex) {
                for (int64_t yIndex = 0; yIndex < static_cast<int64_t>(stitchingDataSizeY); ++yIndex) {
                    for (int64_t zIndex = 0; zIndex < 3; ++zIndex) {
                        const auto dataIndex = xIndex + yIndex * static_cast<int64_t>(stitchingDataSizeX);
                        const auto reorderedIndex = yIndex + xIndex * static_cast<int64_t>(stitchingDataSizeY) +
                            zIndex * static_cast<int64_t>(stitchingDataSizeX) * static_cast<int64_t>(stitchingDataSizeY);

                        reorderedBFData.get()[reorderedIndex] = bfData.get()[dataIndex];
                    }
                }
            }
        }
        else if (stitchingDataSizeZ == 3) {
            reorderedBFData = std::shared_ptr<uint8_t[]>{ new uint8_t[numberOfElements] };

            for (int64_t xIndex = 0; xIndex < static_cast<int64_t>(stitchingDataSizeX); ++xIndex) {
                for (int64_t yIndex = 0; yIndex < static_cast<int64_t>(stitchingDataSizeY); ++yIndex) {
                    for (int64_t zIndex = 0; zIndex < 3; ++zIndex) {
                        const auto dataIndex = xIndex + yIndex * static_cast<int64_t>(stitchingDataSizeX) +
                            zIndex * static_cast<int64_t>(stitchingDataSizeX) * static_cast<int64_t>(stitchingDataSizeY);
                        const auto reorderedIndex = yIndex + xIndex * static_cast<int64_t>(stitchingDataSizeY) +
                            zIndex * static_cast<int64_t>(stitchingDataSizeX) * static_cast<int64_t>(stitchingDataSizeY);

                        reorderedBFData.get()[reorderedIndex] = bfData.get()[dataIndex];
                    }
                }
            }
        }

        return { true, reorderedBFData, stitchingDataSizeX, stitchingDataSizeY };
    }
    catch (const H5::Exception&) {
        return { false, nullptr, 0,0 };
    }
}

auto ThumbnailWriterBF::Impl::GenerateThumbnailData(const std::shared_ptr<uint8_t[]>& bfData, 
    const int32_t& bfDataSizeX, const int32_t& bfDataSizeY)
    -> std::tuple<bool, std::shared_ptr<uint8_t[]>, int32_t, int32_t> {

    TC::Processing::ThumbnailGenerator::ThumbnailBF thumbnailBF;
    thumbnailBF.SetDataSize(bfDataSizeX, bfDataSizeY);
    thumbnailBF.SetBFData(bfData);
    if (!thumbnailBF.Generate()) { return { false, nullptr, 0,0 }; }
    
    const auto thumbnailData = thumbnailBF.GetThumbnailData();
    const auto thumbnailSizeX = thumbnailBF.GetThumbnailSizeX();
    const auto thumbnailSizeY = thumbnailBF.GetThumbnailSizeY();

    return { true, thumbnailData, thumbnailSizeX, thumbnailSizeY };
}

ThumbnailWriterBF::ThumbnailWriterBF() : d(new Impl()) {
}

ThumbnailWriterBF::~ThumbnailWriterBF() = default;

auto ThumbnailWriterBF::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto ThumbnailWriterBF::SetTempTCFFilePath(const QString& tempTCFFilePath) -> void {
    d->tempTCFFilePath = tempTCFFilePath;
}

auto ThumbnailWriterBF::SetTimeFrameIndex(const int32_t& timeFrameIndex) -> void {
    d->timeFrameIndex = timeFrameIndex;
}

auto ThumbnailWriterBF::SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo) -> void {
    d->acquisitionSequenceInfo = acquisitionSequenceInfo;
}

auto ThumbnailWriterBF::Write() -> bool {
    auto [readingResult, bfData, bfDataSizeX, bfDataSizeY] = d->ReadBFData();
    if (readingResult == false) {
        QLOG_ERROR() << "ReadBFData() fails";
        return false;
    }

    auto [voxelSizeReadingResult, voxelSizeX, voxelSizeY] = d->ReadBFVoxelSize();
    if (voxelSizeReadingResult == false) {
        QLOG_ERROR() << "ReadBFVoxelSize() fails";
        return false;
    }

    auto [thumbnailGeneratingResult, thumbnailData, thumbnailSizeX, thumbnailSizeY] =
        d->GenerateThumbnailData(bfData, bfDataSizeX, bfDataSizeY);

    if (thumbnailGeneratingResult == false) {
        QLOG_ERROR() << "GenerateThumbnailData() fails";
        return false;
    }

    d->thumbnailData = thumbnailData;
    d->thumbnailSizeX = thumbnailSizeX;
    d->thumbnailSizeY = thumbnailSizeY;

    const auto thumbnailVoxelSizeX = voxelSizeX * static_cast<float>(bfDataSizeX) / static_cast<float>(thumbnailSizeX);
    const auto thumbnailVoxelSizeY = voxelSizeY * static_cast<float>(bfDataSizeY) / static_cast<float>(thumbnailSizeY);

    const auto tcfDataIndex =
        d->acquisitionSequenceInfo.GetOrderIndex(AcquisitionSequenceInfo::Modality::BF, d->timeFrameIndex);

    TC::IO::TCFWriter::TCFThumbnailDataSet tcfThumbnailDataSet;
    tcfThumbnailDataSet.SetData(thumbnailData);
    tcfThumbnailDataSet.SetDataMemoryOrder(MemoryOrder3D::YXZ);
    tcfThumbnailDataSet.SetDataSize(thumbnailSizeX, thumbnailSizeY);
    tcfThumbnailDataSet.SetColorFlag(true);
    tcfThumbnailDataSet.SetVoxelSize(thumbnailVoxelSizeX, thumbnailVoxelSizeY, LengthUnit::Micrometer);
    tcfThumbnailDataSet.SetTimeFrameIndex(tcfDataIndex);

    TC::IO::TCFWriter::TCFThumbnailWriter tcfThumbnailWriter;
    tcfThumbnailWriter.SetModality(TC::IO::TCFWriter::Modality::BF);
    tcfThumbnailWriter.SetTCFThumbnailDataSet(tcfThumbnailDataSet);
    tcfThumbnailWriter.SetTargetFilePath(d->tempTCFFilePath);
    if (!tcfThumbnailWriter.Write()) {
        QLOG_ERROR() << "tcfThumbnailWriter.Write() fails";
        return false;
    }

    return true;
}

auto ThumbnailWriterBF::GetThumbnailData() const -> std::tuple<std::shared_ptr<uint8_t[]>, int32_t, int32_t> {
    return { d->thumbnailData, d->thumbnailSizeX, d->thumbnailSizeY };
}
