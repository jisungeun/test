#include "HTLDMDataSetGetterStitchingResult.h"

#include "Hdf5DataSetHybridSamplerReader.h"
#include "StorageOnePickSampler.h"

using namespace TC::IO::LdmCore;
using namespace TC::IO::LdmReading;
using namespace TC::IO::LdmWriting;

class HTLDMDataSetGetterStitchingResult::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    H5::DataSet* srcDataSet{ nullptr };
    H5::Group* destGroup{ nullptr };

    H5::DataSet* srcMIPDataSet{ nullptr };
    H5::Group* destMIPGroup{ nullptr };

    int32_t readingOffsetX{};
    int32_t readingOffsetY{};
    int32_t readingOffsetZ{};

    LdmConfiguration ldmConfiguration3D{};
    LdmConfiguration ldmConfigurationMIP{};

    float minValue3D{};
    float minValueMIP{};
};

HTLDMDataSetGetterStitchingResult::HTLDMDataSetGetterStitchingResult() : d(std::make_unique<Impl>()) {
}

HTLDMDataSetGetterStitchingResult::~HTLDMDataSetGetterStitchingResult() = default;

auto HTLDMDataSetGetterStitchingResult::SetSourceDataSet(H5::DataSet* dataSet) -> void {
    d->srcDataSet = dataSet;
}

auto HTLDMDataSetGetterStitchingResult::SetDestLDMGroup(H5::Group* group) -> void {
    d->destGroup = group;
}

auto HTLDMDataSetGetterStitchingResult::SetSourceMIPDataSet(H5::DataSet* mipDataSet) -> void {
    d->srcMIPDataSet = mipDataSet;
}

auto HTLDMDataSetGetterStitchingResult::SetDestLDMMIPGroup(H5::Group* mipGroup) -> void {
    d->destMIPGroup = mipGroup;
}

auto HTLDMDataSetGetterStitchingResult::SetDataMinValue(const float& minValue3D, const float& minValueMIP) -> void {
    d->minValue3D = minValue3D;
    d->minValueMIP = minValueMIP;
}

auto HTLDMDataSetGetterStitchingResult::SetReadingOffset(const int32_t& offsetX, const int32_t& offsetY,
    const int32_t& offsetZ) -> void {
    d->readingOffsetX = offsetX;
    d->readingOffsetY = offsetY;
    d->readingOffsetZ = offsetZ;
}

auto HTLDMDataSetGetterStitchingResult::SetLdmConfiguration(const LdmConfiguration& ldmConfiguration3D,
    const LdmConfiguration& ldmConfigurationMIP) -> void {
    d->ldmConfiguration3D = ldmConfiguration3D;
    d->ldmConfigurationMIP = ldmConfigurationMIP;
}

auto HTLDMDataSetGetterStitchingResult::GetTileData(const int32_t& tileIndex) -> std::shared_ptr<float[]> {
    auto samplerReader = std::make_shared<Hdf5DataSetHybridSamplerReader>(*d->srcDataSet, *d->destGroup);
    samplerReader->SetOffsetPoint({ d->readingOffsetX, d->readingOffsetY, d->readingOffsetZ });

    auto storageOnePickSampler = std::make_shared<StorageOnePickSampler>();
    storageOnePickSampler->SetSamplerReader(samplerReader);

    const auto ldmSampledMemoryChunk = storageOnePickSampler->Sample(d->ldmConfiguration3D, tileIndex);

    const auto numberOfElements = ldmSampledMemoryChunk->GetDimension().GetNumberOfElements();

    const auto htTileDataFloat = std::shared_ptr<float[]>{ new float[numberOfElements]() };

    if (ldmSampledMemoryChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type) {
        const auto htTileDataUint16 = static_cast<uint16_t*>(ldmSampledMemoryChunk->GetDataPointer());

        for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
            htTileDataFloat.get()[dataIndex] = static_cast<float>(htTileDataUint16[dataIndex]) / 10000;
        }
    } else {
        const auto htTileDataUint8 = static_cast<uint8_t*>(ldmSampledMemoryChunk->GetDataPointer());

        for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
            htTileDataFloat.get()[dataIndex] = static_cast<float>(htTileDataUint8[dataIndex]) / 1000 + d->minValue3D;
        }
    }

    return htTileDataFloat;
}

auto HTLDMDataSetGetterStitchingResult::GetMipTileData(const int32_t& tileIndex) -> std::shared_ptr<float[]> {
    auto samplerReader = std::make_shared<Hdf5DataSetHybridSamplerReader>(*d->srcMIPDataSet, *d->destMIPGroup);
    samplerReader->SetOffsetPoint({ d->readingOffsetX, d->readingOffsetY});

    auto storageOnePickSampler = std::make_shared<StorageOnePickSampler>();
    storageOnePickSampler->SetSamplerReader(samplerReader);

    const auto ldmSampledMemoryChunk = storageOnePickSampler->Sample(d->ldmConfigurationMIP, tileIndex);

    const auto numberOfElements = ldmSampledMemoryChunk->GetDimension().GetNumberOfElements();

    const auto htTileDataFloat = std::shared_ptr<float[]>{ new float[numberOfElements]() };

    if (ldmSampledMemoryChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type) {
        const auto htTileDataUint16 = static_cast<uint16_t*>(ldmSampledMemoryChunk->GetDataPointer());

        for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
            htTileDataFloat.get()[dataIndex] = static_cast<float>(htTileDataUint16[dataIndex]) / 10000;
        }
    } else {
        const auto htTileDataUint8 = static_cast<uint8_t*>(ldmSampledMemoryChunk->GetDataPointer());

        for (size_t dataIndex = 0; dataIndex < numberOfElements; ++dataIndex) {
            htTileDataFloat.get()[dataIndex] = static_cast<float>(htTileDataUint8[dataIndex]) / 1000 + d->minValueMIP;;
        }
    }

    return htTileDataFloat;

}
