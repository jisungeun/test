#include "StitchingWriterBFLDM.h"

#include <TCLogger.h>
#include <H5Cpp.h>
#include <HDF5Mutex.h>
#include <QFile>

#include <TCFBFLDMDataSet.h>

#include "BFLDMDataSetGetterStitchingResult.h"
#include "ElapsedTimeReader.h"
#include "Octree2DStackConfigurator.h"
#include "RecordingTimeReader.h"
#include "ResultFilePathGenerator.h"
#include "PiercedRectangle.h"
#include "PiercedRectangleMerger.h"
#include "TCFBFLDMDataSetWriter.h"

using namespace TC::TCFWriter;
using namespace TC::IO;
using namespace TC::IO::LdmCore;

using SequenceModality = AcquisitionSequenceInfo::Modality;

class StitchingWriterBFLDM::Impl {
public:
    struct WritingStartIndex {
        int32_t x{};
        int32_t y{};
        int32_t z{};
    };

    struct CenterPosition {
        int32_t x{};
        int32_t y{};
    };

    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    QString tempTCFFilePath{};

    AcquisitionConfig acquisitionConfig{};
    AcquisitionSequenceInfo acquisitionSequenceInfo{};

    auto CheckTempTCFFile()->bool;

    auto ReadBFDataSetMetaInfo(const int32_t& timeIndex)->TCFBFDataSetMetaInfo;

    auto CalculateStitchingPiercedRect()->PiercedRectangle;
    auto CalculateWritingStartIndex(const int32_t& mergedCenterPositionX, const int32_t& mergedCenterPositionY,
        const int32_t& centerPositionX, const int32_t& centerPositionY)->WritingStartIndex;
    auto ReadBFCenterPosition(const int32_t& timeIndex)->CenterPosition;
};

auto StitchingWriterBFLDM::Impl::CheckTempTCFFile() -> bool {
    try {
        if (!H5::H5File::isHdf5(this->tempTCFFilePath.toStdString())) {
            return false;
        }
        const H5::H5File file(this->tempTCFFilePath.toStdString(), H5F_ACC_RDWR);
        if (!file.nameExists("/Data/BF")) {
            return false;
        }
    } catch(const H5::Exception&) {
        return false;
    }
    return true;
}

auto StitchingWriterBFLDM::Impl::ReadBFDataSetMetaInfo(const int32_t& timeIndex) -> TCFBFDataSetMetaInfo {
    const QString bfProcessedDataFilePath = ResultFilePathGenerator::GetBFProcessedFilePath(this->rootFolderPath, 0, timeIndex);
    const auto stitchingResultFilePath = ResultFilePathGenerator::GetBFStitchingFilePath(this->rootFolderPath, timeIndex);

    // stitching data size
    int32_t stitchingDataSizeX{}, stitchingDataSizeY{}, stitchingDataSizeZ{};
    {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto stitchingDataSet3D = stitchingDataFile.openDataSet("StitchingData");

        const auto attrDataSizeX = stitchingDataSet3D.openAttribute("dataSizeX");
        const auto attrDataSizeY = stitchingDataSet3D.openAttribute("dataSizeY");
        const auto attrDataSizeZ = stitchingDataSet3D.openAttribute("dataSizeZ");

        attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
        attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);
        attrDataSizeZ.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeZ);
    }

    // pixel world size
    float pixelWorldSizeX{}, pixelWorldSizeY{}, pixelWorldSizeZ{};
    {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        const H5::H5File processedDataFile(bfProcessedDataFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto processedDataSet = processedDataFile.openDataSet("Data");

        const auto attrPixelWorldSizeX = processedDataSet.openAttribute("pixelWorldSizeX");
        const auto attrPixelWorldSizeY = processedDataSet.openAttribute("pixelWorldSizeY");

        attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
        attrPixelWorldSizeY.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeY);
    }

    // recordedTime
    QDateTime recordedTime;
    {
        RecordingTimeReader recordingTimeReader;
        const QString recordedTimeFilePath = QString("%1/data/0000/BF/%2/acquisition_timestamp.txt")
            .arg(this->rootFolderPath).arg(timeIndex, 4, 10, QChar('0'));

        recordingTimeReader.SetFilePath(recordedTimeFilePath);
        const auto recordingTimeReadResult = recordingTimeReader.Read();


        if (recordingTimeReadResult == true) {
            recordedTime = recordingTimeReader.GetRecordingTime();
        } else {
            recordedTime = QDateTime::currentDateTime();
        }
    }

    // elapsedTime
    double elapsedTime;
    constexpr auto elapsedTimeUnit = TimeUnit::Second;
    {
        ElapsedTimeReader elapsedTimeReader;

        const QString elapsedTimeFilePath = QString("%1/data/0000/BF/%2/logical_timestamp.txt")
            .arg(this->rootFolderPath).arg(timeIndex, 4, 10, QChar('0'));

        elapsedTimeReader.SetFilePath(elapsedTimeFilePath);
        const auto elapsedTimeReadResult = elapsedTimeReader.Read();

        if (elapsedTimeReadResult == true) {
            elapsedTime = elapsedTimeReader.GetElapsedTime(elapsedTimeUnit);
        } else {
            elapsedTime = 0;
        }
    }

    // tcfDataIndex
    const auto tcfDataIndex = this->acquisitionSequenceInfo.GetOrderIndex(SequenceModality::BF, timeIndex);

    // position
    TCFDataSetRecordedPosition position;
    {
        const auto& acquisitionPosition = this->acquisitionConfig.GetAcquisitionPosition();

        const auto positionX = acquisitionPosition.positionXMillimeter;
        const auto positionY = acquisitionPosition.positionYMillimeter;
        const auto positionZ = acquisitionPosition.positionZMillimeter;
        const auto positionC = acquisitionPosition.positionCMillimeter;
        constexpr auto positionUnit = LengthUnit::Millimenter;

        position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);
    }

    TCFBFDataSetMetaInfo metaInfo;
    metaInfo.SetDataSize(stitchingDataSizeX, stitchingDataSizeY);
    metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, LengthUnit::Micrometer);
    metaInfo.SetRecordedTime(recordedTime);
    metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
    metaInfo.SetTimeFrameIndex(tcfDataIndex);
    metaInfo.SetPosition(position);

    return metaInfo;
}

auto StitchingWriterBFLDM::Impl::CalculateStitchingPiercedRect() -> PiercedRectangle {
    const auto timeFrameCount = this->acquisitionSequenceInfo.GetTimeFrameCount();

    QList<PiercedRectangle> stitchingRectList;
    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(SequenceModality::BF, timeIndex)) {
            continue;
        }

        const auto stitchingResultFilePath = ResultFilePathGenerator::GetBFStitchingFilePath(this->rootFolderPath, timeIndex);
        const auto [centerPositionX, centerPositionY] = ReadBFCenterPosition(timeIndex);

        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto stitchingDataSet = stitchingDataFile.openDataSet("StitchingData");

        int32_t stitchingDataSizeX{}, stitchingDataSizeY{};
        const auto attrDataSizeX = stitchingDataSet.openAttribute("dataSizeX");
        const auto attrDataSizeY = stitchingDataSet.openAttribute("dataSizeY");

        attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
        attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);

        PiercedRectangle piercedRectangle;
        piercedRectangle.SetSize(stitchingDataSizeX, stitchingDataSizeY);
        piercedRectangle.SetPiercingIndex(centerPositionX, centerPositionY);

        stitchingRectList.push_back(piercedRectangle);
    }

    PiercedRectangleMerger piercedRectangleMerger;
    piercedRectangleMerger.SetPiercedRectangleList(stitchingRectList);
    piercedRectangleMerger.Merge();

    return piercedRectangleMerger.GetMergedPiercedRectangle();
}

auto StitchingWriterBFLDM::Impl::CalculateWritingStartIndex(const int32_t& mergedCenterPositionX,
    const int32_t& mergedCenterPositionY, const int32_t& centerPositionX,
    const int32_t& centerPositionY) -> WritingStartIndex {

    const auto writingStartIndexX = centerPositionX - mergedCenterPositionX;
    const auto writingStartIndexY = centerPositionY - mergedCenterPositionY;

    return WritingStartIndex{ writingStartIndexX, writingStartIndexY , 0 };
}

auto StitchingWriterBFLDM::Impl::ReadBFCenterPosition(const int32_t& timeIndex) -> CenterPosition {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    const auto stitchingResultFilePath = ResultFilePathGenerator::GetBFStitchingFilePath(this->rootFolderPath, timeIndex);
    const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);

    int32_t centerPositionX{}, centerPositionY{};

    const auto attCenterPositionX = stitchingDataFile.openAttribute("centerPositionX");
    const auto attCenterPositionY = stitchingDataFile.openAttribute("centerPositionY");

    attCenterPositionX.read(H5::PredType::NATIVE_INT32, &centerPositionX);
    attCenterPositionY.read(H5::PredType::NATIVE_INT32, &centerPositionY);

    return { centerPositionX , centerPositionY };
}

StitchingWriterBFLDM::StitchingWriterBFLDM() : d(std::make_unique<Impl>()) {
}

StitchingWriterBFLDM::~StitchingWriterBFLDM() = default;

auto StitchingWriterBFLDM::SetRootFolderPath(const QString & rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto StitchingWriterBFLDM::SetTempTCFFilePath(const QString & tempTCFFilePath) -> void {
    d->tempTCFFilePath = tempTCFFilePath;
}

auto StitchingWriterBFLDM::SetAcquisitionConfig(const AcquisitionConfig & acquisitionConfig) -> void {
    d->acquisitionConfig = acquisitionConfig;
}

auto StitchingWriterBFLDM::SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo & acquisitionSequenceInfo) -> void {
    d->acquisitionSequenceInfo = acquisitionSequenceInfo;
}

auto StitchingWriterBFLDM::Write(const int32_t & timeIndex) -> bool {
    QLOG_INFO() << "stitchingWriter BF LDM time(" << timeIndex << ")";

    const auto bfDataSetMetaInfo = d->ReadBFDataSetMetaInfo(timeIndex);

    const auto stitchingPiercedRect = d->CalculateStitchingPiercedRect();
    const auto [centerPositionX, centerPositionY] = d->ReadBFCenterPosition(timeIndex);
    const auto [writingStartIndexX, writingStartIndexY, writingStartIndexZ] =
        d->CalculateWritingStartIndex(stitchingPiercedRect.GetPiercingIndexX(), stitchingPiercedRect.GetPiercingIndexY(),
            centerPositionX, centerPositionY);

    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    if (!d->CheckTempTCFFile()) {
        return false;
    }

    const auto stitchingResultFilePath = ResultFilePathGenerator::GetBFStitchingFilePath(d->rootFolderPath, timeIndex);
    const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);

    auto srcDataSet = stitchingDataFile.openDataSet("StitchingData");
    int32_t stitchingDataSizeZ{};
    const auto attrDataSizeZ = srcDataSet.openAttribute("dataSizeZ");
    attrDataSizeZ.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeZ);

    const auto tcfDataIndex = d->acquisitionSequenceInfo.GetOrderIndex(SequenceModality::BF, timeIndex);
    const auto timeFrameName = QString("%1").arg(tcfDataIndex, 6, 10, QChar('0'));

    const H5::H5File file(d->tempTCFFilePath.toStdString(), H5F_ACC_RDWR);
    const auto dataGroup = file.openGroup("/Data/BF");
    auto destLDMGroup = dataGroup.createGroup(timeFrameName.toStdString());

    const auto dataSizeX = static_cast<size_t>(stitchingPiercedRect.GetSizeX());
    const auto dataSizeY = static_cast<size_t>(stitchingPiercedRect.GetSizeY());
    const auto dataSizeZ = static_cast<size_t>(stitchingDataSizeZ);

    constexpr auto tileSize = 128;

    const Dimension dimensionBF{ dataSizeX, dataSizeY, dataSizeZ };
    const Dimension tileDimensionBF{ tileSize, tileSize };

    Octree2DStackConfigurator octree2DStackConfigurator{ dimensionBF, tileDimensionBF };
    const auto ldmConfiguration = octree2DStackConfigurator.Configure();
    
    std::shared_ptr<BFLDMDataSetGetterStitchingResult> dataGetter{ new BFLDMDataSetGetterStitchingResult };
    dataGetter->SetReadingOffset(writingStartIndexX, writingStartIndexY);
    dataGetter->SetSourceDataSet(&srcDataSet);
    dataGetter->SetDestLDMGroup(&destLDMGroup);
    dataGetter->SetLdmConfiguration(ldmConfiguration);
    dataGetter->SetChannelCount(stitchingDataSizeZ);

    TCFBFLDMDataSet tcfBFLDMDataSet;
    tcfBFLDMDataSet.SetMetaInfo(bfDataSetMetaInfo);
    tcfBFLDMDataSet.SetDataGetter(dataGetter);
    tcfBFLDMDataSet.SetLdmConfiguration(ldmConfiguration);

    locker.Unlock();

    TCFBFLDMDataSetWriter writer;
    writer.SetDestLDMGroup(&destLDMGroup);
    writer.SetTCFBFLDMDataSet(tcfBFLDMDataSet);
    writer.SetWholeDataSize(dataSizeX, dataSizeY);
    if (!writer.Write()) {
        QLOG_ERROR() << "tcfHTLDMDataSetWriter.Write() fails";
        return false;
    }

    return true;
}
