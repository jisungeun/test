#pragma once

#include <memory>
#include <QString>

#include "IStitcherFL.h"

class StitcherFL : public IStitcherFL {
public:
    StitcherFL();
    ~StitcherFL();

    auto SetRootFolderPath(const QString& rootFolderPath)->void override;
    auto SetChannelIndex(const int32_t& channelIndex)->void override;
    auto SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY)->void override;
    auto SetLimitSize(const double& sizeXInMicrometer, const double& sizeYInMicrometer)->void override;

    auto Stitch(const int32_t& timeIndex)->bool override;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
