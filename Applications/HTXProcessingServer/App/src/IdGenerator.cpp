#include "IdGenerator.h"
#include <QFile>
#include <QDateTime>
#include <QFileInfo>
#include <QSettings>
#include <QTextStream>
#include <QUuid>

#include "TimeStampReader.h"

struct IdGenerator::Impl {
    Impl() = default;
    ~Impl() = default;

    QString configFilePath{};
    QString timeStampFilePath{};

    auto GetTimeStampString() const->QString;
    static auto CheckFileValidity(const QString& filePath)->bool;
    static auto GetFileBirthTimeString(const QString& filePath)->QString;

    auto GetDeviceSerialString() const->QString;
    auto ReadDeviceSerialString(const QString& configFilePath) const->QString;
    auto GetDeviceSerial(const QString& configFilePath)const->QString;

    auto GetCurrentTimeString() const->QString;
};

auto IdGenerator::Impl::GetTimeStampString() const -> QString {
    const auto configFileIsValid = CheckFileValidity(configFilePath);
    const auto timeStampFileIsValid = CheckFileValidity(timeStampFilePath);

    QString timeStampString;

    if (timeStampFileIsValid) {
        TimeStampReader timeStampReader(timeStampFilePath);
        timeStampString = timeStampReader.ReadTimeStampString();
    } else if (configFileIsValid) {
        const auto configFileGeneratedTime = GetFileBirthTimeString(configFilePath);
        timeStampString = configFileGeneratedTime;
    } else {
        const QString defaultTimeStampString = "1997-01-01 00:00:00.000";
        timeStampString = defaultTimeStampString;
    }

    return timeStampString;
}

auto IdGenerator::Impl::CheckFileValidity(const QString& filePath) -> bool {
    auto fileIsValid{ false };

    const auto pathIsNotEmtpy = !filePath.isEmpty();
    const auto fileExists = QFile::exists(filePath);

    if (pathIsNotEmtpy && fileExists) {
        fileIsValid = true;
    }

    return fileIsValid;
}

auto IdGenerator::Impl::GetFileBirthTimeString(const QString& filePath) -> QString {
    const QFileInfo fileInfo(filePath);

    const auto birthTime = fileInfo.birthTime();
    const auto fileBirthTimeString = birthTime.toString("yyyy-MM-dd hh:mm:ss.zzz");
    return fileBirthTimeString;
}

auto IdGenerator::Impl::GetDeviceSerialString() const -> QString {
    const auto deviceSerialString = ReadDeviceSerialString(configFilePath);
    return deviceSerialString;
}

auto IdGenerator::Impl::ReadDeviceSerialString(const QString& configFilePath) const -> QString {
    const auto configFileIsValid = CheckFileValidity(configFilePath);

    QString deviceSerialString;
    if (configFileIsValid) {
        deviceSerialString = GetDeviceSerial(configFilePath);
    } else {
        deviceSerialString = "DummyDeviceSerial";
    }
    return deviceSerialString;
}

auto IdGenerator::Impl::GetDeviceSerial(const QString& configFilePath) const -> QString {
    QSettings configFile(configFilePath, QSettings::IniFormat);
    const auto deviceSerial = configFile.value("MetaInfo/serial", "DummyDeviceSerial").toString();
    return deviceSerial;
}

auto IdGenerator::Impl::GetCurrentTimeString() const -> QString {
    return QDateTime(QDateTime::currentDateTime()).toString("yyyy-MM-dd HH:mm:ss");
}

IdGenerator::IdGenerator(const QString& configFilePath, const QString& timeStampFilePath)
    : d(new Impl()) {
    d->configFilePath = configFilePath;
    d->timeStampFilePath = timeStampFilePath;
}

IdGenerator::~IdGenerator() = default;

auto IdGenerator::GenerateDataId() const -> QString {
    auto timestampString = d->GetTimeStampString();
    const auto deviceSerialString = d->GetDeviceSerialString();

    timestampString.replace(QRegExp("[- .]"), ":");

    QStringList strDataID;

    strDataID << deviceSerialString << timestampString;

    return strDataID.join("-");
}

auto IdGenerator::GenerateUniqueId() const -> QString {
    auto timestampString = d->GetTimeStampString();
    const auto deviceSerialString = d->GetDeviceSerialString();
    auto currentTimeString = d->GetCurrentTimeString();

    timestampString.replace(QRegExp("[- .]"), ":");
    currentTimeString.replace(QRegExp("[- ]"), ":");
    QString uuid = QUuid::createUuid().toString();
    uuid.replace(QRegExp("[\\{\\-\\}]"), "");

    QStringList strUUID;

    strUUID << deviceSerialString
        << timestampString
        << currentTimeString
        << uuid;

    return strUUID.join("-");
}
