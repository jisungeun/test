#include "TileConfigurationReaderBF.h"

#include <QSet>

#include "AcquisitionPositionReader.h"
#include "H5Cpp.h"
#include "HDF5Mutex.h"
#include "SIUnit.h"

struct TileSizeXYZ {
    int32_t tileSizeX{};
    int32_t tileSizeY{};
    int32_t tileSizeZ{};
};

struct TilePixelWorldSizeXY {
    float tileWorldSizeX{};
    float tileWorldSizeY{};
    LengthUnit unit{ LengthUnit::Micrometer };
};

struct AcquisitionPosition {
    double x;
    double y;
    LengthUnit unit{ LengthUnit::Millimenter };
};

struct PositionList {
    QList<int32_t> xPositionList{};
    QList<int32_t> yPositionList{};
    LengthUnit unit{ LengthUnit::Nanometer };
};

struct OverlapLength {
    int32_t x;
    int32_t y;
};

class TileConfigurationReaderBF::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QStringList positionFileList{};
    QStringList processedFilePathList{};

    TileConfiguration tileConfiguration;

    auto ReadTileSize()->TileSizeXYZ;
    auto ReadTilePixelWorldSize()->TilePixelWorldSizeXY;
    auto GetAcquisitionPositionList(const QStringList& positionFilePathList)->QList<AcquisitionPosition>;
    auto SortAndDivideAcquisitionPosition(const QList<AcquisitionPosition>& acquisitionPositionList)
        ->PositionList;

    auto CalculateOverlapLength(const int32_t& tileSizeX, const int32_t& tileSizeY,
        const QList<int32_t>& xPositionList, const QList<int32_t>& yPositionList, const LengthUnit& positionUnit)
        ->OverlapLength;
};

auto TileConfigurationReaderBF::Impl::ReadTileSize() -> TileSizeXYZ {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    const auto processedFilePath = this->processedFilePathList.first();

    int32_t tileSizeX{}, tileSizeY{}, tileSizeZ{};

    const H5::H5File file(processedFilePath.toStdString(), H5F_ACC_RDONLY);
    const auto dataSet = file.openDataSet("Data");

    const auto attrSizeX = dataSet.openAttribute("dataSizeX");
    const auto attrSizeY = dataSet.openAttribute("dataSizeY");
    const auto attrChannelCount = dataSet.openAttribute("channelCount");

    attrSizeX.read(H5::PredType::NATIVE_INT32, &tileSizeX);
    attrSizeY.read(H5::PredType::NATIVE_INT32, &tileSizeY);
    attrChannelCount.read(H5::PredType::NATIVE_INT32, &tileSizeZ);

    return { tileSizeX, tileSizeY, tileSizeZ };
}

auto TileConfigurationReaderBF::Impl::ReadTilePixelWorldSize() -> TilePixelWorldSizeXY {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    const auto processedFilePath = this->processedFilePathList.first();

    float pixelWorldSizeX{}, pixelWorldSizeY{};

    const H5::H5File file(processedFilePath.toStdString(), H5F_ACC_RDONLY);
    const auto dataSet = file.openDataSet("Data");

    const auto attrPixelWorldSizeX = dataSet.openAttribute("pixelWorldSizeX");
    const auto attrPixelWorldSizeY = dataSet.openAttribute("pixelWorldSizeY");

    attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
    attrPixelWorldSizeY.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeY);

    return { pixelWorldSizeX, pixelWorldSizeY, LengthUnit::Micrometer };
}

auto TileConfigurationReaderBF::Impl::GetAcquisitionPositionList(const QStringList& positionFilePathList)
-> QList<AcquisitionPosition> {
    QList<AcquisitionPosition> acquisitionPositionList;

    constexpr auto positionUnit = LengthUnit::Millimenter;

    for (const auto& positionFilePath : positionFilePathList) {
        AcquisitionPositionReader acquisitionPositionReader;
        acquisitionPositionReader.SetFilePath(positionFilePath);
        acquisitionPositionReader.Read();

        const auto x = acquisitionPositionReader.GetPositionX(positionUnit);
        const auto y = acquisitionPositionReader.GetPositionY(positionUnit);

        acquisitionPositionList.push_back({ x,y,positionUnit });
    }

    return acquisitionPositionList;
}

auto TileConfigurationReaderBF::Impl::SortAndDivideAcquisitionPosition(
    const QList<AcquisitionPosition>& acquisitionPositionList) -> PositionList {
    QSet<int32_t> xPositionSet{}, yPositionSet{};

    for (const auto& acquisitionPosition : acquisitionPositionList) {
        const auto xPosition = static_cast<int32_t>(std::round(ConvertUnit(acquisitionPosition.x, acquisitionPosition.unit, LengthUnit::Nanometer)));
        const auto yPosition = static_cast<int32_t>(std::round(ConvertUnit(acquisitionPosition.y, acquisitionPosition.unit, LengthUnit::Nanometer)));

        xPositionSet.insert(xPosition);
        yPositionSet.insert(yPosition);
    }

    QList xPositionSortAndDividedSet = xPositionSet.values();
    QList yPositionSortAndDividedSet = yPositionSet.values();

    std::sort(xPositionSortAndDividedSet.begin(), xPositionSortAndDividedSet.end());
    std::sort(yPositionSortAndDividedSet.begin(), yPositionSortAndDividedSet.end());

    return { xPositionSortAndDividedSet, yPositionSortAndDividedSet, LengthUnit::Nanometer };
}

auto TileConfigurationReaderBF::Impl::CalculateOverlapLength(const int32_t& tileSizeX, const int32_t& tileSizeY,
    const QList<int32_t>& xPositionList, const QList<int32_t>& yPositionList, const LengthUnit& positionUnit)
    -> OverlapLength {
    const auto [tilePixelWorldSizeX, tilePixelWorldSizeY, tilePixelWorldSizeUnit] = this->ReadTilePixelWorldSize();

    const auto tilePixelWorldSizeXNanometer = ConvertUnit(tilePixelWorldSizeX, tilePixelWorldSizeUnit, LengthUnit::Nanometer);
    const auto tilePixelWorldSizeYNanometer = ConvertUnit(tilePixelWorldSizeY, tilePixelWorldSizeUnit, LengthUnit::Nanometer);

    const auto tileWorldSizeXNanometer = static_cast<double>(tileSizeX) * tilePixelWorldSizeXNanometer;
    const auto tileWorldSizeYNanometer = static_cast<double>(tileSizeY) * tilePixelWorldSizeYNanometer;

    int32_t nonOverlapLengthXNanometer, nonOverlapLengthYNanometer;

    if (xPositionList.size() == 1) {
        nonOverlapLengthXNanometer = static_cast<int32_t>(tileWorldSizeXNanometer);
    } else {
        nonOverlapLengthXNanometer =
            std::abs(xPositionList.last() - xPositionList.first()) / (xPositionList.size() - 1);
    }

    if (yPositionList.size() == 1) {
        nonOverlapLengthYNanometer = static_cast<int32_t>(tileWorldSizeYNanometer);
    } else {
        nonOverlapLengthYNanometer =
            std::abs(yPositionList.last() - yPositionList.first()) / (yPositionList.size() - 1);
    }

    const auto overlapLengthXNanometer = tileWorldSizeXNanometer - nonOverlapLengthXNanometer;
    const auto overlapLengthYNanometer = tileWorldSizeYNanometer - nonOverlapLengthYNanometer;

    const auto overlapLengthXPixel = static_cast<int32_t>(std::round(overlapLengthXNanometer / tilePixelWorldSizeXNanometer));
    const auto overlapLengthYPixel = static_cast<int32_t>(std::round(overlapLengthYNanometer / tilePixelWorldSizeYNanometer));

    return { overlapLengthXPixel, overlapLengthYPixel };
}

TileConfigurationReaderBF::TileConfigurationReaderBF() : d(new Impl()) {
}

TileConfigurationReaderBF::~TileConfigurationReaderBF() = default;

auto TileConfigurationReaderBF::SetPositionFileList(const QStringList & positionFileList) -> void {
    d->positionFileList = positionFileList;
}

auto TileConfigurationReaderBF::SetProcessedFilePathList(const QStringList & processedFilePathList) -> void {
    d->processedFilePathList = processedFilePathList;
}

auto TileConfigurationReaderBF::Read() -> bool {
    const auto [tileSizeX, tileSizeY, tileSizeZ] = d->ReadTileSize();
    const auto [xPositionList, yPositionList, positionUnit] = d->SortAndDivideAcquisitionPosition(d->GetAcquisitionPositionList(d->positionFileList));

    const auto tileNumberX = xPositionList.size();
    const auto tileNumberY = yPositionList.size();

    const auto [overlapLengthX, overlapLengthY] = d->CalculateOverlapLength(tileSizeX, tileSizeY, xPositionList, yPositionList, positionUnit);

    TileConfiguration tileConfiguration;
    tileConfiguration.SetOverlapLengthInPixel(overlapLengthX, overlapLengthY);
    tileConfiguration.SetTileNumber(tileNumberX, tileNumberY);
    tileConfiguration.SetTileSizeInPixel(tileSizeX, tileSizeY, tileSizeZ);

    d->tileConfiguration = tileConfiguration;

    return true;
}

auto TileConfigurationReaderBF::GetTileConfiguration() const -> const TileConfiguration& {
    return d->tileConfiguration;
}
