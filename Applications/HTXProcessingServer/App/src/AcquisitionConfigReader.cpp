#define LOGGER_TAG "[AcquisitionConfigReader]"

#include "AcquisitionConfigReader.h"

#include <QFile>
#include <QSettings>

#include "TCLogger.h"

auto ConvertStringToRGBValue(const QString& rgbString)->AcquisitionConfig::RGB {
    const auto rgbStringList = rgbString.split(",");

    const auto r = static_cast<uint8_t>(rgbStringList.at(0).toUInt());
    const auto g = static_cast<uint8_t>(rgbStringList.at(1).toUInt());
    const auto b = static_cast<uint8_t>(rgbStringList.at(2).toUInt());

    return AcquisitionConfig::RGB{ r, g, b };
}

class AcquisitionConfigReader::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString filePath{};

    AcquisitionConfig acquisitionConfig{};

    auto ReadJobInfo()->AcquisitionConfig::JobInfo;
    auto ReadAcquisitionCount()->AcquisitionConfig::AcquisitionCount;
    auto ReadAcquisitionSetting()->AcquisitionConfig::AcquisitionSetting;
    auto ReadAcquisitionPosition()->AcquisitionConfig::AcquisitionPosition;
    auto ReadAcquisitionSize()->AcquisitionConfig::AcquisitionSize;
    auto ReadDeviceInfo()->AcquisitionConfig::DeviceInfo;
    auto ReadImageInfo()->AcquisitionConfig::ImageInfo;
    auto ReadTileInfo()->AcquisitionConfig::TileInfo;
};

auto AcquisitionConfigReader::Impl::ReadJobInfo() -> AcquisitionConfig::JobInfo {
    AcquisitionConfig::JobInfo jobInfo;

    QSettings file(this->filePath, QSettings::IniFormat);
    file.beginGroup("JobInfo");

    jobInfo.title = file.value("Title", "").toString();
    jobInfo.userID = file.value("User_ID", "").toString();

    file.endGroup();

    return jobInfo;
}

auto AcquisitionConfigReader::Impl::ReadAcquisitionCount() -> AcquisitionConfig::AcquisitionCount {
    AcquisitionConfig::AcquisitionCount acquisitionCount;

    QSettings file(this->filePath, QSettings::IniFormat);
    file.beginGroup("AcquisitionCount");

    acquisitionCount.ht3D = file.value("HT_3D_Count", "0").toInt();
    acquisitionCount.ht2D = file.value("HT_2D_Count", "0").toInt();
    acquisitionCount.fl3DCH0 = file.value("FL_3D_CH0_Count", "0").toInt();
    acquisitionCount.fl2DCH0 = file.value("FL_2D_CH0_Count", "0").toInt();
    acquisitionCount.fl3DCH1 = file.value("FL_3D_CH1_Count", "0").toInt();
    acquisitionCount.fl2DCH1 = file.value("FL_2D_CH1_Count", "0").toInt();
    acquisitionCount.fl3DCH2 = file.value("FL_3D_CH2_Count", "0").toInt();
    acquisitionCount.fl2DCH2 = file.value("FL_2D_CH2_Count", "0").toInt();

    acquisitionCount.ht3DZ = file.value("HT_3D_Z_Count", "0").toInt();
    acquisitionCount.fl3DZ = file.value("FL_3D_Z_Count", "0").toInt();

    acquisitionCount.bf = file.value("BF_Count", "0").toInt();
    acquisitionCount.bfChannel = file.value("BF_Channel", "0").toInt();

    file.endGroup();

    return acquisitionCount;
}

auto AcquisitionConfigReader::Impl::ReadAcquisitionSetting() -> AcquisitionConfig::AcquisitionSetting {
    AcquisitionConfig::AcquisitionSetting acquisitionSetting;

    QSettings file(this->filePath, QSettings::IniFormat);
    file.beginGroup("AcquisitionSetting");

    acquisitionSetting.htZStepLengthMicrometer = file.value("HT_Z_Step_Length_Micrometer", "0").toDouble();
    acquisitionSetting.flZStepLengthMicrometer = file.value("FL_Z_Step_Length_Micrometer", "0").toDouble();

    acquisitionSetting.flCH0ExcitationWaveLengthMicrometer = file.value("FL_CH0_Excitation_Wavelength_Micrometer", "0").toDouble();
    acquisitionSetting.flCH1ExcitationWaveLengthMicrometer = file.value("FL_CH1_Excitation_Wavelength_Micrometer", "0").toDouble();
    acquisitionSetting.flCH2ExcitationWaveLengthMicrometer = file.value("FL_CH2_Excitation_Wavelength_Micrometer", "0").toDouble();

    acquisitionSetting.flCH0EmissionWaveLengthMicrometer = file.value("FL_CH0_Emission_Wavelength_Micrometer", "0").toDouble();
    acquisitionSetting.flCH1EmissionWaveLengthMicrometer = file.value("FL_CH1_Emission_Wavelength_Micrometer", "0").toDouble();
    acquisitionSetting.flCH2EmissionWaveLengthMicrometer = file.value("FL_CH2_Emission_Wavelength_Micrometer", "0").toDouble();

    const auto flCH0ColorString = file.value("FL_CH0_Color").toString();
    const auto flCH1ColorString = file.value("FL_CH1_Color").toString();
    const auto flCH2ColorString = file.value("FL_CH2_Color").toString();

    if (!flCH0ColorString.isEmpty()) {
        acquisitionSetting.flCH0Color = ConvertStringToRGBValue(flCH0ColorString);
    }

    if (!flCH1ColorString.isEmpty()) {
        acquisitionSetting.flCH1Color = ConvertStringToRGBValue(flCH1ColorString);
    }

    if (!flCH2ColorString.isEmpty()) {
        acquisitionSetting.flCH2Color = ConvertStringToRGBValue(flCH2ColorString);
    }

    acquisitionSetting.flCH0Intensity = file.value("FL_CH0_Intensity", "0").toInt();
    acquisitionSetting.flCH1Intensity = file.value("FL_CH1_Intensity", "0").toInt();
    acquisitionSetting.flCH2Intensity = file.value("FL_CH2_Intensity", "0").toInt();

    acquisitionSetting.flCH0ExposureTimeMillisecond = file.value("FL_CH0_Exposure_Time_Millisecond", "0").toInt();
    acquisitionSetting.flCH1ExposureTimeMillisecond = file.value("FL_CH1_Exposure_Time_Millisecond", "0").toInt();
    acquisitionSetting.flCH2ExposureTimeMillisecond = file.value("FL_CH2_Exposure_Time_Millisecond", "0").toInt();

    file.endGroup();

    return acquisitionSetting;
}

auto AcquisitionConfigReader::Impl::ReadAcquisitionPosition() -> AcquisitionConfig::AcquisitionPosition {
    AcquisitionConfig::AcquisitionPosition acquisitionPosition;

    QSettings file(this->filePath, QSettings::IniFormat);
    file.beginGroup("AcquisitionPosition");

    acquisitionPosition.wellIndex = file.value("WellIndex", "0").toInt();
    acquisitionPosition.positionXMillimeter = file.value("Position_X_Millimeter", "0").toDouble();
    acquisitionPosition.positionYMillimeter = file.value("Position_Y_Millimeter", "0").toDouble();
    acquisitionPosition.positionZMillimeter = file.value("Position_Z_Millimeter", "0").toDouble();
    acquisitionPosition.positionCMillimeter = file.value("Position_C_Millimeter", "0").toDouble();
    acquisitionPosition.flAcquisitionZOffsetMicrometer = file.value("FL_Acquisition_Z_Offset_Micrometer", "0").toDouble();

    file.endGroup();

    return acquisitionPosition;
}

auto AcquisitionConfigReader::Impl::ReadAcquisitionSize() -> AcquisitionConfig::AcquisitionSize {
    AcquisitionConfig::AcquisitionSize acquisitionSize;

    QSettings file(this->filePath, QSettings::IniFormat);
    file.beginGroup("AcquisitionSize");

    acquisitionSize.acquisitionSizeXMicrometer = file.value("Acquisition_Size_X_Micrometer", "0").toDouble();
    acquisitionSize.acquisitionSizeYMicrometer = file.value("Acquisition_Size_Y_Micrometer", "0").toDouble();

    file.endGroup();

    return acquisitionSize;
}

auto AcquisitionConfigReader::Impl::ReadDeviceInfo() -> AcquisitionConfig::DeviceInfo {
    AcquisitionConfig::DeviceInfo deviceInfo;

    QSettings file(this->filePath, QSettings::IniFormat);
    file.beginGroup("DeviceInfo");

    deviceInfo.pixelSizeMicrometer = file.value("Pixel_Size_Micrometer", "0").toDouble();
    deviceInfo.magnification = file.value("Magnification", "0").toDouble();
    deviceInfo.objectiveNA = file.value("Objective_NA", "0").toDouble();
    deviceInfo.condenserNA = file.value("Condenser_NA", "0").toDouble();
    deviceInfo.mediumRI = file.value("Medium_RI", "0").toDouble();
    deviceInfo.deviceSerial = file.value("Device_Serial", "0").toString();
    deviceInfo.deviceHost = file.value("Device_Host", "0").toString();
    deviceInfo.deviceSoftwareVersion = file.value("Device_Software_Version", "0").toString();
    deviceInfo.sampleStageEncoderSupported = file.value("SampleStage_Encoder_Supported", false).toBool();

    file.endGroup();

    return deviceInfo;
}

auto AcquisitionConfigReader::Impl::ReadImageInfo() -> AcquisitionConfig::ImageInfo {
    AcquisitionConfig::ImageInfo imageInfo;

    QSettings file(this->filePath, QSettings::IniFormat);
    file.beginGroup("ImageInfo");

    imageInfo.roiSizeXPixels = file.value("ROI_Size_X_Pixels", "0").toInt();
    imageInfo.roiSizeYPixels = file.value("ROI_Size_Y_Pixels", "0").toInt();
    imageInfo.roiOffsetXPixels = file.value("ROI_Offset_X_Pixels", "0").toInt();
    imageInfo.roiOffsetYPixels = file.value("ROI_Offset_Y_Pixels", "0").toInt();

    file.endGroup();

    return imageInfo;
}

auto AcquisitionConfigReader::Impl::ReadTileInfo() -> AcquisitionConfig::TileInfo {
    AcquisitionConfig::TileInfo tileInfo;

    QSettings file(this->filePath, QSettings::IniFormat);
    file.beginGroup("TileInfo");
    tileInfo.tileNumberX = file.value("Tile_Number_X", "0").toInt();
    tileInfo.tileNumberY = file.value("Tile_Number_Y", "0").toInt();
    file.endGroup();

    return tileInfo;
}

AcquisitionConfigReader::AcquisitionConfigReader() : d(std::make_unique<Impl>()) {
}

AcquisitionConfigReader::~AcquisitionConfigReader() = default;

auto AcquisitionConfigReader::SetFilePath(const QString& filePath) -> void {
    d->filePath = filePath;
}

auto AcquisitionConfigReader::Read() -> bool {
    if (!QFile::exists(d->filePath)) {
        QLOG_ERROR() << "config file doesn't exist :" << d->filePath.toStdString().c_str();
        return false;
    }

    QSettings file(d->filePath, QSettings::IniFormat);
    const auto groupList = file.childGroups();
    if (groupList.isEmpty()) {
        QLOG_ERROR() << "config file has no groups : " << d->filePath.toStdString().c_str();
        return false;
    }

    AcquisitionConfig acquisitionConfig;
    if (groupList.contains("JobInfo")) {
        acquisitionConfig.SetJobInfo(d->ReadJobInfo());
    }

    if (groupList.contains("AcquisitionCount")) {
        acquisitionConfig.SetAcquisitionCount(d->ReadAcquisitionCount());
    }

    if (groupList.contains("AcquisitionSetting")) {
        acquisitionConfig.SetAcquisitionSetting(d->ReadAcquisitionSetting());
    }

    if (groupList.contains("AcquisitionPosition")) {
        acquisitionConfig.SetAcquisitionPosition(d->ReadAcquisitionPosition());
    }

    if (groupList.contains("AcquisitionSize")) {
        acquisitionConfig.SetAcquisitionSize(d->ReadAcquisitionSize());
    }

    if (groupList.contains("ImageInfo")) {
        acquisitionConfig.SetImageInfo(d->ReadImageInfo());
    }

    if (groupList.contains("TileInfo")) {
        acquisitionConfig.SetTileInfo(d->ReadTileInfo());
    }

    if (groupList.contains("DeviceInfo")) {
        acquisitionConfig.SetDeviceInfo(d->ReadDeviceInfo());
    }

    d->acquisitionConfig = acquisitionConfig;

    return true;
}

auto AcquisitionConfigReader::GetAcquisitionConfig() const -> const AcquisitionConfig& {
    return d->acquisitionConfig;
}
