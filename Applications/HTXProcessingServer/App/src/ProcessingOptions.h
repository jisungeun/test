#pragma once

#include <memory>

class ProcessingOptions {
public:
    ProcessingOptions();
    ProcessingOptions(const ProcessingOptions& other);

    auto operator=(const ProcessingOptions& other)->ProcessingOptions&;

    ~ProcessingOptions();
    
    auto SetLDMFlag(const bool& ldmFlag)->void;
    auto GetLDMFlag() const ->bool;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};