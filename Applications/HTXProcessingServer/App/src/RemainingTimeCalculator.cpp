#include "RemainingTimeCalculator.h"

using ZSliceCount = int32_t;

class RemainingTimeCalculator::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    RemainingTaskCount count{};
    TakenUnitTimeToProcess takenTime{};
    AcquisitionSequenceInfo acquisitionSequenceInfo{};

    double remainingTime{};
    TimeUnit remainingTimeUnit{TimeUnit::Second};
};

RemainingTimeCalculator::RemainingTimeCalculator() : d(new Impl()) {
}

RemainingTimeCalculator::~RemainingTimeCalculator() = default;

auto RemainingTimeCalculator::SetRemainingTaskCount(const RemainingTaskCount& count) -> void {
    d->count = count;
}

auto RemainingTimeCalculator::SetTakenUnitTimeToProcess(const TakenUnitTimeToProcess& takenTime) -> void {
    d->takenTime = takenTime;
}

auto RemainingTimeCalculator::SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo)
    -> void {
    d->acquisitionSequenceInfo = acquisitionSequenceInfo;
}

auto RemainingTimeCalculator::Calculate() -> bool {
    const auto timeFrameCount = d->acquisitionSequenceInfo.GetTimeFrameCount();

    QList<ZSliceCount> htZSliceCountList, flZSliceCountList, bfZSliceCountList;
    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        const auto htZSliceCount =
            d->acquisitionSequenceInfo.GetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, timeIndex);
        if (htZSliceCount > 0) {
            if (!htZSliceCountList.contains(htZSliceCount)) {
                htZSliceCountList.push_back(htZSliceCount);
            }
        }

        const auto flCh0ZSliceCount =
            d->acquisitionSequenceInfo.GetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, timeIndex);
        if (flCh0ZSliceCount > 0) {
            if (!flZSliceCountList.contains(flCh0ZSliceCount)) {
                flZSliceCountList.push_back(flCh0ZSliceCount);
            }
        }

        const auto flCh1ZSliceCount =
            d->acquisitionSequenceInfo.GetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH1, timeIndex);
        if (flCh1ZSliceCount > 0) {
            if (!flZSliceCountList.contains(flCh1ZSliceCount)) {
                flZSliceCountList.push_back(flCh1ZSliceCount);
            }
        }

        const auto flCh2ZSliceCount =
            d->acquisitionSequenceInfo.GetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH2, timeIndex);
        if (flCh2ZSliceCount > 0) {
            if (!flZSliceCountList.contains(flCh2ZSliceCount)) {
                flZSliceCountList.push_back(flCh2ZSliceCount);
            }
        }

        const auto bfZSliceCount =
            d->acquisitionSequenceInfo.GetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::BF, timeIndex);
        if (bfZSliceCount > 0) {
            if (!bfZSliceCountList.contains(bfZSliceCount)) {
                bfZSliceCountList.push_back(bfZSliceCount);
            }
        }
    }

    constexpr auto timeUnit = TimeUnit::Second;
    double htRemainingTime{ 0 };
    for (const auto& htZSliceCount : htZSliceCountList) {
        const auto htProcessingCount = d->count.GetHTProcessingCount(htZSliceCount);
        const auto htProcessingTakenTime = d->takenTime.GetHTProcessingTime(htZSliceCount,timeUnit);
        const auto htProcessingTotalTime = htProcessingCount * htProcessingTakenTime;

        const auto htStitchingCount = d->count.GetHTStitchingCount(htZSliceCount);
        const auto htStitchingTakenTime = d->takenTime.GetHTStitchingTime(htZSliceCount, timeUnit);
        const auto htStitchingTotalTime = htStitchingCount * htStitchingTakenTime;

        const auto htStitchingWrittenCount = d->count.GetHTStitchingWrittenCount(htZSliceCount);
        const auto htStitchingWrittenTakenTime = d->takenTime.GetHTStitchingWrittenTime(htZSliceCount, timeUnit);
        const auto htStitchingWrittenTotalTime = htStitchingWrittenCount * htStitchingWrittenTakenTime;

        const auto htLDMConvertingCount = d->count.GetHTLDMConvertingCount(htZSliceCount);
        const auto htLDMConvertingTakenTime = d->takenTime.GetHTLDMConvertingTime(htZSliceCount, timeUnit);
        const auto htLDMConvertingTotalTime = htLDMConvertingCount * htLDMConvertingTakenTime;

        htRemainingTime += htProcessingTotalTime + htStitchingTotalTime + htStitchingWrittenTotalTime + htLDMConvertingTotalTime;
    }

    double flRemainingTime{ 0 };
    for (const auto& flZSliceCount : flZSliceCountList) {
        const auto flProcessingCount = d->count.GetFLProcessingCount(flZSliceCount);
        const auto flProcessingTakenTime = d->takenTime.GetFLProcessingTime(flZSliceCount, timeUnit);
        const auto flProcessingTotalTime = flProcessingCount * flProcessingTakenTime;

        const auto flStitchingCount = d->count.GetFLStitchingCount(flZSliceCount);
        const auto flStitchingTakenTime = d->takenTime.GetFLStitchingTime(flZSliceCount, timeUnit);
        const auto flStitchingTotalTime = flStitchingCount * flStitchingTakenTime;

        const auto flStitchingWrittenCount = d->count.GetFLStitchingWrittenCount(flZSliceCount);
        const auto flStitchingWrittenTakenTime = d->takenTime.GetFLStitchingWrittenTime(flZSliceCount, timeUnit);
        const auto flStitchingWrittenTotalTime = flStitchingWrittenCount * flStitchingWrittenTakenTime;

        const auto flLDMConvertingCount = d->count.GetFLLDMConvertingCount(flZSliceCount);
        const auto flLDMConvertingTakenTime = d->takenTime.GetFLLDMConvertingTime(flZSliceCount, timeUnit);
        const auto flLDMConvertingTotalTime = flLDMConvertingCount * flLDMConvertingTakenTime;

        flRemainingTime += flProcessingTotalTime + flStitchingTotalTime + flStitchingWrittenTotalTime + flLDMConvertingTotalTime;
    }

    double bfRemainingTime{ 0 };
    for (const auto& bfZSliceCount : bfZSliceCountList) {
        const auto bfProcessingCount = d->count.GetBFProcessingCount(bfZSliceCount);
        const auto bfProcessingTakenTime = d->takenTime.GetBFProcessingTime(bfZSliceCount, timeUnit);
        const auto bfProcessingTotalTime = bfProcessingCount * bfProcessingTakenTime;

        const auto bfStitchingCount = d->count.GetBFStitchingCount(bfZSliceCount);
        const auto bfStitchingTakenTime = d->takenTime.GetBFStitchingTime(bfZSliceCount, timeUnit);
        const auto bfStitchingTotalTime = bfStitchingCount * bfStitchingTakenTime;

        const auto bfStitchingWrittenCount = d->count.GetBFStitchingWrittenCount(bfZSliceCount);
        const auto bfStitchingWrittenTakenTime = d->takenTime.GetBFStitchingWrittenTime(bfZSliceCount, timeUnit);
        const auto bfStitchingWrittenTotalTime = bfStitchingWrittenCount * bfStitchingWrittenTakenTime;

        const auto bfLDMConvertingCount = d->count.GetBFLDMConvertingCount(bfZSliceCount);
        const auto bfLDMConvertingTakenTime = d->takenTime.GetBFLDMConvertingTime(bfZSliceCount, timeUnit);
        const auto bfLDMConvertingTotalTime = bfLDMConvertingCount * bfLDMConvertingTakenTime;

        bfRemainingTime = bfProcessingTotalTime + bfStitchingTotalTime + bfStitchingWrittenTotalTime + bfLDMConvertingTotalTime;
    }

    d->remainingTime = htRemainingTime + flRemainingTime + bfRemainingTime;
    d->remainingTimeUnit = timeUnit;

    return true;
}

auto RemainingTimeCalculator::GetRemainingTime(const TimeUnit& unit) -> double {
    return ConvertUnit(d->remainingTime, d->remainingTimeUnit, unit);
}
