#pragma once

#include <QString>

class ResultFilePathGenerator {
public:
    static auto GetTempFolderPath(const QString& rootFolderPath)->QString;
    static auto GetThumbnailFolderPath(const QString& rootFolderPath)->QString;
    static auto GetTempFilePath(const QString& rootFolderPath)->QString;

    static auto GetHTProcessedFilePath(const QString& rootFolderPath, const int32_t& tileIndex, const int32_t& timeIndex)->QString;
    static auto GetFLProcessedFilePath(const QString& rootFolderPath, const int32_t& flChannelIndex, const int32_t& tileIndex, const int32_t& timeIndex)->QString;
    static auto GetBFProcessedFilePath(const QString& rootFolderPath, const int32_t& tileIndex, const int32_t& timeIndex)->QString;

    static auto GetHTStitchingFilePath(const QString& rootFolderPath, const int32_t& timeIndex)->QString;
    static auto GetFLStitchingFilePath(const QString& rootFolderPath, const int32_t& flChannelIndex, const int32_t& timeIndex)->QString;
    static auto GetBFStitchingFilePath(const QString& rootFolderPath, const int32_t& timeIndex)->QString;

    static auto GetTCFFilePath(const QString& rootFolderPath)->QString;
    static auto GetHTMIPImageFilePath(const QString& rootFolderPath)->QString;
    static auto GetBFImageFilePath(const QString& rootFolderPath)->QString;
    static auto GetLDMTCFFilePath(const QString& rootFolderPath)->QString;
};