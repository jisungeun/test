#pragma once

#include <memory>

#include <QString>

#include "AcquisitionConfig.h"
#include "TaskInfoSet.h"

class TaskInfoSetScanner {
public:
    TaskInfoSetScanner();
    ~TaskInfoSetScanner();

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto SetAcquisitionCount(const AcquisitionCount& acquisitionCount)->void;

    auto Scan()->bool;

    auto GetTaskInfoSet()const ->const TaskInfoSet&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
