#pragma once

#include <memory>

class PiercedRectangle {
public:
    PiercedRectangle();
    PiercedRectangle(const PiercedRectangle& other);
    ~PiercedRectangle();

    auto operator=(const PiercedRectangle& other)->PiercedRectangle&;

    auto SetSize(const int32_t& sizeX, const int32_t& sizeY)->void;
    auto GetSizeX()const ->const int32_t&;
    auto GetSizeY()const ->const int32_t&;

    auto SetPiercingIndex(const int32_t& piercingIndexX, const int32_t& piercingIndexY)->void;
    auto GetPiercingIndexX()const ->const int32_t&;
    auto GetPiercingIndexY()const ->const int32_t&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};