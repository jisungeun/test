#include "HTXProcessingServerHumbleObject.h"

struct HTXProcessingServerHumbleObject::Impl {
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    QString topDirectory{};
    QList<SampleObject> sampleObjectList{};
    bool isProcessingFlag{ false };
    uint32_t scanPeriodInSec{ 5 };

    QString psfFolderPath{};
    QString systemBackgroundFolderPath{};

    QString matlabModuleFolderPath{};
};

HTXProcessingServerHumbleObject::HTXProcessingServerHumbleObject()
    : d(new Impl()) {
}

HTXProcessingServerHumbleObject::HTXProcessingServerHumbleObject(const HTXProcessingServerHumbleObject& other)
    : d(new Impl(*other.d)) {
}

HTXProcessingServerHumbleObject::~HTXProcessingServerHumbleObject() = default;

auto HTXProcessingServerHumbleObject::operator=(
    const HTXProcessingServerHumbleObject& other) -> HTXProcessingServerHumbleObject& {
    *d = *other.d;
    return *this;
}

auto HTXProcessingServerHumbleObject::SetSampleObjectList(const QList<SampleObject>& sampleObjectList) -> void {
    d->sampleObjectList = sampleObjectList;
}

auto HTXProcessingServerHumbleObject::SetTopDirectory(const QString& topDirectory) -> void {
    d->topDirectory = topDirectory;
}

auto HTXProcessingServerHumbleObject::SetSystemBackgroundFolderPath(const QString& systemBackgroundFolderPath) -> void {
    d->systemBackgroundFolderPath = systemBackgroundFolderPath;
}

auto HTXProcessingServerHumbleObject::SetIsProcessingFlag(const bool& isProcessingFlag) -> void {
    d->isProcessingFlag = isProcessingFlag;
}

auto HTXProcessingServerHumbleObject::SetScanPeriodInSec(const uint32_t& scanPeriodInSec) -> void {
    d->scanPeriodInSec = scanPeriodInSec;
}

auto HTXProcessingServerHumbleObject::SetPsfFolderPath(const QString& psfFolderPath) -> void {
    d->psfFolderPath = psfFolderPath;
}

auto HTXProcessingServerHumbleObject::SetMatlabModuleFolderPath(const QString& matlabModuleFolderPath) -> void {
    d->matlabModuleFolderPath = matlabModuleFolderPath;
}

auto HTXProcessingServerHumbleObject::GetIsProcessingFlag() const -> bool {
    return d->isProcessingFlag;
}

auto HTXProcessingServerHumbleObject::GetScanPeriodInSec() const -> uint32_t {
    return d->scanPeriodInSec;
}

auto HTXProcessingServerHumbleObject::GetPsfFolderPath() const -> const QString& {
    return d->psfFolderPath;
}

auto HTXProcessingServerHumbleObject::GetMatlabModuleFolderPath() const -> const QString& {
    return d->matlabModuleFolderPath;
}

auto HTXProcessingServerHumbleObject::GetSampleObjectList() const -> QList<SampleObject> {
    return d->sampleObjectList;
}

auto HTXProcessingServerHumbleObject::GetTopDirectory() const -> QString {
    return d->topDirectory;
}

auto HTXProcessingServerHumbleObject::GetSystemBackgroundFolderPath() const -> QString {
    return d->systemBackgroundFolderPath;
}