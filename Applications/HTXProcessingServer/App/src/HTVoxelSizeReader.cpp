#include "HTVoxelSizeReader.h"

#include <QFile>
#include "H5Cpp.h"

class HTVoxelSizeReader::Impl {
public:
	Impl() = default;
	~Impl() = default;

	QString processedFilePath{};

	double voxelSizeXY{};
};

HTVoxelSizeReader::HTVoxelSizeReader() : d(new Impl()) {
}

HTVoxelSizeReader::~HTVoxelSizeReader() = default;

auto HTVoxelSizeReader::SetProcessedFilePath(const QString& processedFilePath) -> void {
	d->processedFilePath = processedFilePath;
}

auto HTVoxelSizeReader::Read() -> bool {
	if (d->processedFilePath.isEmpty()) { return false; }
	if (!QFile::exists(d->processedFilePath)) { return false; }
	if (!H5::H5File::isHdf5(d->processedFilePath.toStdString())) { return false; }

	const H5::H5File file{ d->processedFilePath.toStdString(), H5F_ACC_RDONLY };
	if (!file.exists("Data")) { return false; }

	const auto dataSet = file.openDataSet("Data");

	if (!dataSet.attrExists("pixelWorldSizeX")) { return false; }

	const auto pixelWorldSizeXAttribute = dataSet.openAttribute("pixelWorldSizeX");

	float pixelWorldSizeX{ 0 };
	pixelWorldSizeXAttribute.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);

	if (pixelWorldSizeX == 0) { return false; }
	
	d->voxelSizeXY = static_cast<double>(pixelWorldSizeX);

	return true;
}

auto HTVoxelSizeReader::GetVoxelSizeXY() const -> double {
	return d->voxelSizeXY;
}
