#pragma once

#include <memory>
#include <QString>

class HTVoxelSizeReader {
public:
	HTVoxelSizeReader();
	~HTVoxelSizeReader();

	auto SetProcessedFilePath(const QString& processedFilePath)->void;
	auto Read()->bool;

	auto GetVoxelSizeXY()const ->double;

private:
	class Impl;
	std::unique_ptr<Impl> d;
};
