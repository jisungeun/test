#include "PositionIniFileReader.h"

#include <QFile>
#include <QSettings>

class PositionIniFileReader::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString iniFilePath{};

    double x;
    double y;
    double z;
    double focus;
    double scanStart;
};

PositionIniFileReader::PositionIniFileReader() : d(new Impl()) {
}

PositionIniFileReader::~PositionIniFileReader() = default;

auto PositionIniFileReader::SetIniFilePath(const QString& iniFilePath) -> void {
    d->iniFilePath = iniFilePath;
}

auto PositionIniFileReader::Read() -> bool {
    if (!QFile::exists(d->iniFilePath)) {
        return false;
    }

    auto iniFile = QSettings(d->iniFilePath, QSettings::IniFormat);
    d->x = iniFile.value("X", 0).toDouble();
    d->y = iniFile.value("Y", 0).toDouble();
    d->z = iniFile.value("Z", 0).toDouble();
    d->focus = iniFile.value("Z Focus", 0).toDouble();
    d->scanStart = iniFile.value("Z Scan Start").toDouble();

    return true;
}

auto PositionIniFileReader::GetX() -> double {
    return d->x;
}

auto PositionIniFileReader::GetY() -> double {
    return d->y;
}

auto PositionIniFileReader::GetZ() -> double {
    return d->z;
}

auto PositionIniFileReader::GetFocus() -> double {
    return d->focus;
}

auto PositionIniFileReader::GetScanStart() -> double {
    return d->scanStart;
}
