#pragma once

#include <memory>
#include "TaskInfo.h"
#include "AcquisitionCount.h"

class ProcessType;
class ModalityType;

class TaskInfoSet {
public:
    TaskInfoSet();
    TaskInfoSet(const TaskInfoSet& other);
    ~TaskInfoSet();

    auto operator=(const TaskInfoSet& other)->TaskInfoSet&;

    auto Initialize(const AcquisitionCount& acquisitionCount)->void;

    auto SetTaskInfo(const int32_t& timeIndex, const int32_t& tileIndex, const ModalityType& modalityType, const ProcessType& processType, const TaskInfo& taskInfo)->void;
    auto GetTaskInfo(const int32_t& timeIndex, const int32_t& tileIndex, const ModalityType& modalityType, const ProcessType& processType)const->TaskInfo;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
