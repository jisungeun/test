#pragma once

#include <memory>

#include "AcquiredDataFlag.h"
#include "AcquisitionDataInfo.h"

class AcquisitionDataTargetScanner {
public:
    AcquisitionDataTargetScanner();
    ~AcquisitionDataTargetScanner();

    auto SetAcquisitionDataInfo(const AcquisitionDataInfo& acquisitionDataInfo)->void;
    auto SetAcquiredDataFlag(const AcquiredDataFlag& acquiredDataFlag)->void;
    auto SetTarget(const bool& bgScanFlag, const int32_t& timeIndex, const int32_t& tileIndex, const ModalityType& type)->void;
    auto Scan()->bool;
    auto GetAcquiredDataFlag()const -> const AcquiredDataFlag&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
