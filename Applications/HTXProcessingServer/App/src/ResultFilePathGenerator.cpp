#include "ResultFilePathGenerator.h"

#include <QDir>

auto ResultFilePathGenerator::GetTempFolderPath(const QString& rootFolderPath) -> QString {
    const auto tempFolderPath = rootFolderPath + "/temp";
    return tempFolderPath;
}

auto ResultFilePathGenerator::GetThumbnailFolderPath(const QString& rootFolderPath) -> QString {
    const QString thumbnailFolderPath = rootFolderPath + "/thumbnail";
    return thumbnailFolderPath;
}

auto ResultFilePathGenerator::GetTempFilePath(const QString& rootFolderPath) -> QString {
    const auto tempFolderPath = GetTempFolderPath(rootFolderPath);
    const QString tcfTempFilePath = tempFolderPath + "/result.tctmp";
    return tcfTempFilePath;
}

auto ResultFilePathGenerator::GetHTProcessedFilePath(const QString& rootFolderPath, const int32_t& tileIndex,
    const int32_t& timeIndex) -> QString {
    const QString htProcessedFilePath = QString("%1/temp/HT/ht_Processed_tile%2_time%3")
        .arg(rootFolderPath).arg(tileIndex, 4, 10, QChar('0'))
        .arg(timeIndex, 4, 10, QChar('0'));
    return htProcessedFilePath;
}

auto ResultFilePathGenerator::GetFLProcessedFilePath(const QString& rootFolderPath, const int32_t& flChannelIndex,
    const int32_t& tileIndex, const int32_t& timeIndex) -> QString {
    const auto flProcessedFilePath = QString("%1/temp/FL/fl_Processed_CH%2_tile%3_time%4")
        .arg(rootFolderPath).arg(flChannelIndex).arg(tileIndex, 4, 10, QChar('0'))
        .arg(timeIndex, 4, 10, QChar('0'));
    return flProcessedFilePath;
}

auto ResultFilePathGenerator::GetBFProcessedFilePath(const QString& rootFolderPath, const int32_t& tileIndex,
    const int32_t& timeIndex) -> QString {
    const auto bfProcessedDataFilePath = QString("%1/temp/BF/bf_Processed_tile%2_time%3")
        .arg(rootFolderPath).arg(tileIndex, 4, 10, QChar('0')).arg(timeIndex, 4, 10, QChar('0'));
    return bfProcessedDataFilePath;
}

auto ResultFilePathGenerator::GetHTStitchingFilePath(const QString& rootFolderPath, const int32_t& timeIndex)
    -> QString {
    const auto htStitchingResultFilePath = QString("%1/temp/HT/Stitching_time%2").arg(rootFolderPath)
        .arg(timeIndex, 4, 10, QChar('0'));
    return htStitchingResultFilePath;
}

auto ResultFilePathGenerator::GetFLStitchingFilePath(const QString& rootFolderPath, const int32_t& flChannelIndex,
    const int32_t& timeIndex) -> QString {
    const auto flStitchingResultFilePath = QString("%1/temp/FL/Stitching_CH%2_time%3")
        .arg(rootFolderPath).arg(flChannelIndex).arg(timeIndex, 4, 10, QChar('0'));
    return flStitchingResultFilePath;
}

auto ResultFilePathGenerator::GetBFStitchingFilePath(const QString& rootFolderPath, const int32_t& timeIndex)
    -> QString {
    const auto bfStitchingResultFilePath = QString("%1/temp/BF/Stitching_time%2")
        .arg(rootFolderPath).arg(timeIndex, 4, 10, QChar('0'));
    return bfStitchingResultFilePath;
}

auto ResultFilePathGenerator::GetTCFFilePath(const QString& rootFolderPath) -> QString {
    const auto rootName = QDir(rootFolderPath).dirName();
    const QString tcfFilePath = rootFolderPath + "/" + rootName + ".TCF";
    return tcfFilePath;
}

auto ResultFilePathGenerator::GetHTMIPImageFilePath(const QString& rootFolderPath) -> QString {
    const auto rootName = QDir(rootFolderPath).dirName();
    const QString htMIPFilePath = rootFolderPath + "/" + rootName + "-MIP.PNG";
    return htMIPFilePath;
}

auto ResultFilePathGenerator::GetBFImageFilePath(const QString& rootFolderPath) -> QString {
    const auto rootName = QDir(rootFolderPath).dirName();
    const QString bfImageFilePath = rootFolderPath + "/" + rootName + "-BF.PNG";
    return bfImageFilePath;
}

auto ResultFilePathGenerator::GetLDMTCFFilePath(const QString& rootFolderPath) -> QString {
    const auto rootName = QDir(rootFolderPath).dirName();
    const QString ldmTCFFilePath = rootFolderPath + "/" + rootName + "_LDM.TCF";
    return ldmTCFFilePath;
}
