#include "ModalityType.h"

class ModalityType::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    auto operator==(const Impl& other)const->bool;
    auto operator!=(const Impl& other)const->bool;

    Name name{};
    int32_t channelIndex{};
};

auto ModalityType::Impl::operator==(const Impl& other) const -> bool {
    if (this->name != other.name) { return false; }
    if (this->channelIndex != other.channelIndex) { return false; }
    return true;
}

auto ModalityType::Impl::operator!=(const Impl& other) const -> bool {
    return !(*this == other);
}

ModalityType::ModalityType(const Name& name) : d(new Impl()) {
    d->name = name;
    d->channelIndex = -1;
}

ModalityType::ModalityType(const Name& name, const int32_t& channelIndex)
    : d(new Impl()) {
    d->name = name;
    d->channelIndex = channelIndex;
}

ModalityType::ModalityType(const ModalityType& other) : d(new Impl(*other.d)) {
}

ModalityType::~ModalityType() = default;

auto ModalityType::operator=(const ModalityType& other) -> ModalityType& {
    *(this->d) = *(other.d);
    return *this;
}

auto ModalityType::operator==(const ModalityType& other) const -> bool {
    return *(this->d) == *(other.d);
}

auto ModalityType::operator!=(const ModalityType& other) const -> bool {
    return *(this->d) != *(other.d);
}

auto ModalityType::operator==(const Name& name) const -> bool {
    return d->name == name;
}

auto ModalityType::operator!=(const Name& name) const -> bool {
    return d->name != name;
}

auto ModalityType::GetName() const -> const Name& {
    return d->name;
}

auto ModalityType::GetChannelIndex() const -> const int32_t& {
    return d->channelIndex;
}
