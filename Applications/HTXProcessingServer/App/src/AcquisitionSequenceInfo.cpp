#include "AcquisitionSequenceInfo.h"

#include <QMap>
#include <QList>

using ZSliceCount = int32_t;

class AcquisitionSequenceInfo::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    bool initialized{ false };
    int32_t timeFrameCount{};

    QMap<Modality, QList<ZSliceCount>> acquisitionZSliceCountMap{};
};

AcquisitionSequenceInfo::AcquisitionSequenceInfo() : d(std::make_unique<Impl>()) {
}

AcquisitionSequenceInfo::AcquisitionSequenceInfo(const AcquisitionSequenceInfo& other) : d(std::make_unique<Impl>(*other.d)) {
}

AcquisitionSequenceInfo::~AcquisitionSequenceInfo() = default;

auto AcquisitionSequenceInfo::operator=(const AcquisitionSequenceInfo& other) -> AcquisitionSequenceInfo& {
    *(this->d) = *(other.d);
    return *this;
}

auto AcquisitionSequenceInfo::Initialize(const int32_t& timeFrameCount) -> void {
    d->acquisitionZSliceCountMap.clear();

    QList<ZSliceCount> emptyZSliceCountList;
    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        emptyZSliceCountList.push_back(0);
    }

    constexpr auto modalityCount = 5;
    for (auto modalityIndex = 0; modalityIndex < modalityCount; ++modalityIndex) {
        d->acquisitionZSliceCountMap[static_cast<Modality>(modalityIndex)] = emptyZSliceCountList;
    }
    
    d->initialized = true;
    d->timeFrameCount = timeFrameCount;
}

auto AcquisitionSequenceInfo::IsInitialized() const -> const bool& {
    return d->initialized;
}

auto AcquisitionSequenceInfo::GetTimeFrameCount() const -> const int32_t& {
    return d->timeFrameCount;
}

auto AcquisitionSequenceInfo::SetAcquisitionZSliceCount(const Modality& modality, const int32_t& timeIndex,
    const int32_t& zSliceCount) -> void {
    if (timeIndex >= d->timeFrameCount) { return; }
    if (timeIndex < 0) { return; }
    d->acquisitionZSliceCountMap[modality][timeIndex] = zSliceCount;
}

auto AcquisitionSequenceInfo::GetAcquisitionZSliceCount(const Modality& modality, const int32_t& timeIndex) const
    -> int32_t {
    if (timeIndex >= d->timeFrameCount) { return 0; }
    if (timeIndex < 0) { return 0; }
    return d->acquisitionZSliceCountMap[modality][timeIndex];
}

auto AcquisitionSequenceInfo::AcquisitionExists(const Modality& modality, const int32_t& timeIndex) const -> bool {
    const auto zSliceCount = this->GetAcquisitionZSliceCount(modality, timeIndex);
    return zSliceCount > 0;
}

auto AcquisitionSequenceInfo::GetOrderIndex(const Modality& modality, const int32_t& timeIndex) const -> int32_t {
    auto orderIndex = -1;

    if (!this->AcquisitionExists(modality, timeIndex)) {
        return orderIndex;
    }

    for (auto timeFrameIndex = 0; timeFrameIndex <= timeIndex; ++timeFrameIndex) {
        if (this->AcquisitionExists(modality, timeFrameIndex)) { orderIndex++; }
    }
    return orderIndex;
}

auto AcquisitionSequenceInfo::GetFirstTimeFrameIndex(const Modality& modality) const -> int32_t {
    for (auto timeFrameIndex = 0; timeFrameIndex < d->timeFrameCount; timeFrameIndex++) {
        if (this->AcquisitionExists(modality, timeFrameIndex)) {
            return timeFrameIndex;
        }
    }
    return -1;
}

auto AcquisitionSequenceInfo::GetLastTimeFrameIndex(const Modality& modality) const -> int32_t {
    int32_t lastTimeFrameIndex = -1;
    for (auto timeFrameIndex = 0; timeFrameIndex < d->timeFrameCount; timeFrameIndex++) {
        if (this->AcquisitionExists(modality, timeFrameIndex)) {
            lastTimeFrameIndex = timeFrameIndex;
        }
    }
    return lastTimeFrameIndex;
}

auto AcquisitionSequenceInfo::GetTimeFrameIndex(const Modality& modality, const int32_t& orderIndex) const -> int32_t {
    int32_t orderCount = 0;
    for (auto timeFrameIndex = 0; timeFrameIndex < d->timeFrameCount; timeFrameIndex++) {
        if (this->AcquisitionExists(modality, timeFrameIndex)) {
            orderCount++;
        }

        if (orderCount - 1 == orderIndex) {
            return timeFrameIndex;
        }
    }
    return -1;
}

auto ConvertSequenceModality(const int32_t& flChannelIndex) -> AcquisitionSequenceInfo::Modality {
    if (flChannelIndex == 0) { return AcquisitionSequenceInfo::Modality::FLCH0; }
    if (flChannelIndex == 1) { return AcquisitionSequenceInfo::Modality::FLCH1; }
    if (flChannelIndex == 2) { return AcquisitionSequenceInfo::Modality::FLCH2; }
    return AcquisitionSequenceInfo::Modality::FLCH0; //TODO Error Handling
}
