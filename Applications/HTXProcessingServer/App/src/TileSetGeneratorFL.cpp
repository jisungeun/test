#include "TileSetGeneratorFL.h"

#include <QSet>

#include "AcquisitionPositionReader.h"
#include "TileDataGetterProcessedData.h"

struct TilePositionXYInNanometer {
    int32_t positionX{};
    int32_t positionY{};
};

struct TilePositionXYNanometerSetList {
    QList<int32_t> tilePositionXSetList{};
    QList<int32_t> tilePositionYSetList{};
};

using TilePositionNanometer = int32_t;
using TilePositionIndex = int32_t;

struct TilePositionIndexMap {
    QMap<TilePositionNanometer, TilePositionIndex> tilePositionIndexX{};
    QMap<TilePositionNanometer, TilePositionIndex> tilePositionIndexY{};
};

class TileSetGeneratorFL::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QMap<QString, QString> dataPositionFilePathMap{};
    TileConfiguration tileConfiguration{};

    TileSet tileSet{};

    auto ReadTilePositionXYZList()const->QList<TilePositionXYInNanometer>;
    auto GenerateTilePositionXYSetList(const QList<TilePositionXYInNanometer>& tilePositionXYList)
        ->TilePositionXYNanometerSetList;
    static auto GenerateTilePositionIndexMap(const TilePositionXYNanometerSetList& tilePositionXYList)
        ->TilePositionIndexMap;

    auto GenerateTile(const QString& processedDataFilePath)->Tile;
};

auto TileSetGeneratorFL::Impl::ReadTilePositionXYZList() const -> QList<TilePositionXYInNanometer> {
    QList<TilePositionXYInNanometer> tilePositionXYList;

    for (auto mapIter = this->dataPositionFilePathMap.begin(); mapIter != this->dataPositionFilePathMap.end(); ++mapIter) {
        const auto& positionFilePath = mapIter.value();

        AcquisitionPositionReader acquisitionPositionReader;
        acquisitionPositionReader.SetFilePath(positionFilePath);
        acquisitionPositionReader.Read();
        const auto positionX = static_cast<int32_t>(std::round(acquisitionPositionReader.GetPositionX(LengthUnit::Nanometer)));
        const auto positionY = static_cast<int32_t>(std::round(acquisitionPositionReader.GetPositionY(LengthUnit::Nanometer)));

        tilePositionXYList.push_back({ positionX, positionY });
    }

    return tilePositionXYList;
}

auto TileSetGeneratorFL::Impl::GenerateTilePositionXYSetList(
    const QList<TilePositionXYInNanometer>& tilePositionXYList) -> TilePositionXYNanometerSetList {

    QSet<int32_t> tilePositionXSet{}, tilePositionYSet{};

    for (const auto& tilePositionXY : tilePositionXYList) {
        const auto& [positionXNanometer, positionYNanometer] = tilePositionXY;

        tilePositionXSet.insert(positionXNanometer);
        tilePositionYSet.insert(positionYNanometer);
    }

    const auto tilePositionXList = tilePositionXSet.values();
    const auto tilePositionYList = tilePositionYSet.values();

    return TilePositionXYNanometerSetList{ tilePositionXList, tilePositionYList };
}

auto TileSetGeneratorFL::Impl::GenerateTilePositionIndexMap(const TilePositionXYNanometerSetList& tilePositionXYList)
-> TilePositionIndexMap {
    auto [tilePositionXSetList, tilePositionYSetList] = tilePositionXYList;

    //x coordinate left to right direction is (+)
    std::sort(tilePositionXSetList.begin(), tilePositionXSetList.end());

    //y coordinate bottom to up direction is (+)
    std::sort(tilePositionYSetList.begin(), tilePositionYSetList.end());
    std::reverse(tilePositionYSetList.begin(), tilePositionYSetList.end());

    QMap<TilePositionNanometer, TilePositionIndex> tilePositionIndexXMap{};
    {
        TilePositionIndex tilePositionIndexX{ 0 };

        for (const auto& tilePositionX : tilePositionXSetList) {
            tilePositionIndexXMap[tilePositionX] = tilePositionIndexX;
            tilePositionIndexX++;
        }
    }

    QMap<TilePositionNanometer, TilePositionIndex> tilePositionIndexYMap{};
    {
        TilePositionIndex tilePositionIndexY{ 0 };
        for (const auto& tilePositionY : tilePositionYSetList) {
            tilePositionIndexYMap[tilePositionY] = tilePositionIndexY;
            tilePositionIndexY++;
        }
    }

    return TilePositionIndexMap{ tilePositionIndexXMap, tilePositionIndexYMap };
}

auto TileSetGeneratorFL::Impl::GenerateTile(const QString& processedDataFilePath) -> Tile {
    const auto tileSizeX = this->tileConfiguration.GetTileSizeXInPixel();
    const auto tileSizeY = this->tileConfiguration.GetTileSizeYInPixel();
    const auto tileSizeZ = this->tileConfiguration.GetTileSizeZInPixel();

    const auto overlapSizeX = this->tileConfiguration.GetOverlapLengthXInPixel();
    const auto overlapSizeY = this->tileConfiguration.GetOverlapLengthYInPixel();
    
    const auto tileDataGetterPointer = ITileDataGetter::Pointer{ new TileDataGetterProcessedData };
    std::dynamic_pointer_cast<TileDataGetterProcessedData>(tileDataGetterPointer)->SetProcessedDataFilePath(processedDataFilePath);

    Tile tile;
    tile.SetTileSize(tileSizeX, tileSizeY, tileSizeZ);
    tile.SetOverlapSize(overlapSizeX, overlapSizeY);
    tile.SetTileDataGetter(tileDataGetterPointer);

    return tile;
}

TileSetGeneratorFL::TileSetGeneratorFL() : d(new Impl()) {
}

TileSetGeneratorFL::~TileSetGeneratorFL() = default;

auto TileSetGeneratorFL::SetTileConfiguration(const TileConfiguration & tileConfiguration) -> void {
    d->tileConfiguration = tileConfiguration;
}

auto TileSetGeneratorFL::SetDataPositionFilePathMap(const QMap<QString, QString>&dataPositionFilePathMap) -> void {
    d->dataPositionFilePathMap = dataPositionFilePathMap;
}

auto TileSetGeneratorFL::Generate() -> bool {
    const auto tilePositionXYZList = d->ReadTilePositionXYZList();
    const auto tilePositionXYSetList = d->GenerateTilePositionXYSetList(tilePositionXYZList);
    const auto tilePositionIndexMap = d->GenerateTilePositionIndexMap(tilePositionXYSetList);

    const auto& [tilePositionIndexXMap, tilePositionIndexYMap] = tilePositionIndexMap;

    const auto& tileNumberX = d->tileConfiguration.GetTileNumberX();
    const auto& tileNumberY = d->tileConfiguration.GetTileNumberY();

    TileSet tileSet;
    tileSet.SetTileNumber(tileNumberX, tileNumberY);

    for (auto mapIter = d->dataPositionFilePathMap.begin(); mapIter != d->dataPositionFilePathMap.end(); ++mapIter) {
        const auto& processedDataFilePath = mapIter.key();
        const auto& positionFilePath = mapIter.value();
        const auto tile = d->GenerateTile(processedDataFilePath);

        AcquisitionPositionReader acquisitionPositionReader;
        acquisitionPositionReader.SetFilePath(positionFilePath);
        acquisitionPositionReader.Read();
        const auto positionX = static_cast<int32_t>(std::round(acquisitionPositionReader.GetPositionX(LengthUnit::Nanometer)));
        const auto positionY = static_cast<int32_t>(std::round(acquisitionPositionReader.GetPositionY(LengthUnit::Nanometer)));

        const auto& tileIndexX = tilePositionIndexXMap[positionX];
        const auto& tileIndexY = tilePositionIndexYMap[positionY];

        tileSet.InsertTile(tile, tileIndexX, tileIndexY);
    }

    d->tileSet = tileSet;

    return true;
}

auto TileSetGeneratorFL::GetTileSet() const -> const TileSet& {
    return d->tileSet;
}

