#include "ProcessingFolderScanner.h"

#include "RootDirectoryFinder.h"
#include <QDir>

#include "AcquisitionConfigReader.h"

#include "TaskRepo.h"
#include "AcquisitionDataScanner.h"
#include "AcquisitionDataTargetScanner.h"
#include "AcquisitionScanTargetFinder.h"
#include "ResultFilePathGenerator.h"
#include "TaskInfoSetScanner.h"

#include "AcquisitionSequenceReaderIni.h"
static bool isScanning = false;

struct AcquisitionConfigReadingResult {
    AcquisitionConfigReadingResult() = default;
    AcquisitionConfigReadingResult(AcquisitionConfigReadingResult const& other) = default;
    AcquisitionConfigReadingResult(AcquisitionConfigReadingResult&& other) noexcept = default;

    ~AcquisitionConfigReadingResult() = default;
    AcquisitionConfigReadingResult& operator=(AcquisitionConfigReadingResult const& other) = default;
    AcquisitionConfigReadingResult& operator=(AcquisitionConfigReadingResult&& other) noexcept = default;

    bool validConfig{ false };
    AcquisitionConfig acquisitionConfig;
};

auto ReadAcquisitionConfig(const QString& rootFolderPath)->AcquisitionConfigReadingResult {
    AcquisitionConfigReadingResult readingResult;
    
    const auto configFilePath = QString("%1/config.dat").arg(rootFolderPath);
    AcquisitionConfigReader reader;
    reader.SetFilePath(configFilePath);

    if (!reader.Read()) { return readingResult; }
    
    auto config = reader.GetAcquisitionConfig();

    return { true, config };
}
auto EditAcquisitionConfig(const AcquisitionConfig& acquisitionConfig)->AcquisitionConfig {
    AcquisitionConfig editedAcquisitionConfig{ acquisitionConfig };

    if (!acquisitionConfig.IsTileInfoValid()) {
        AcquisitionConfig::TileInfo tileInfo;
        tileInfo.tileNumberX = 1;
        tileInfo.tileNumberY = 1;

        editedAcquisitionConfig.SetTileInfo(tileInfo);
    }

    return editedAcquisitionConfig;
}

auto CheckValidAcquisitionConfig(const AcquisitionConfig& acquisitionConfig)->bool {
    if (!acquisitionConfig.IsJobInfoValid()) { return false; }
    if (!acquisitionConfig.IsAcquisitionCountValid()) { return false; }
    if (!acquisitionConfig.IsAcquisitionSettingValid()) { return false; }
    if (!acquisitionConfig.IsAcquisitionPositionValid()) { return false; }
    if (!acquisitionConfig.IsDeviceInfoValid()) { return false; }
    if (!acquisitionConfig.IsImageInfoValid()) { return false; }
    if (!acquisitionConfig.IsTileInfoValid()) { return false; }
    return true;
}

auto ConvertSequenceInfoModality(const TC::IO::AcquisitionSequence::Modality& modality)->AcquisitionSequenceInfo::Modality {
    using namespace TC::IO::AcquisitionSequence;
    if (modality == +Modality::HT) {
        return AcquisitionSequenceInfo::Modality::HT;
    } else if (modality == +Modality::FLCH0) {
        return AcquisitionSequenceInfo::Modality::FLCH0;
    } else if (modality == +Modality::FLCH1) {
        return AcquisitionSequenceInfo::Modality::FLCH1;
    } else if (modality == +Modality::FLCH2) {
        return AcquisitionSequenceInfo::Modality::FLCH2;
    } else if (modality == +Modality::BF) {
        return AcquisitionSequenceInfo::Modality::BF;
    } else {
        return AcquisitionSequenceInfo::Modality::HT; //TODO Error Handling
    }
}

auto GetZSliceCount(const TC::IO::AcquisitionSequence::Modality& modality, 
    const TC::IO::AcquisitionSequence::AcquisitionType& type, 
    const AcquisitionConfig::AcquisitionCount& acquisitionCount)->int32_t {
    using namespace TC::IO::AcquisitionSequence;

    if (type == +AcquisitionType::Dimension2) {
        return 1;
    } else if (type == +AcquisitionType::Dimension3) {
        if (modality == +Modality::HT) {
            return acquisitionCount.ht3DZ;
        } else if (modality == +Modality::FLCH0) {
            return acquisitionCount.fl3DZ;
        } else if (modality == +Modality::FLCH1) {
            return acquisitionCount.fl3DZ;
        } else if (modality == +Modality::FLCH2) {
            return acquisitionCount.fl3DZ;
        } else if (modality == +Modality::FLCH3) {
            return acquisitionCount.fl3DZ;
        }
    } else if (type == +AcquisitionType::Color) {
        return 3;
    } else if (type == +AcquisitionType::Gray) {
        return 1;
    }

    return 0;
}

auto ReadAcquisitionSequenceInfo(const QString& rootFolderPath, const AcquisitionConfig& acquisitionConfig)->AcquisitionSequenceInfo {
    const auto acquisitionSequenceIniFilePath = QString("%1/sequence.dat").arg(rootFolderPath);
    const auto configAcquisitionCount = acquisitionConfig.GetAcquisitionCount();

    AcquisitionSequenceInfo acquisitionSequenceInfo;
    if (!QFile::exists(acquisitionSequenceIniFilePath)) {
        return acquisitionSequenceInfo;
    }

    TC::IO::AcquisitionSequence::AcquisitionSequenceReaderIni reader;
    reader.SetIniFilePath(acquisitionSequenceIniFilePath);
    if (!reader.Read()) {
        return acquisitionSequenceInfo;
    }

    const auto acquisitionSequence = reader.GetAcquisitionSequence();
    const auto timeFrameCount = acquisitionSequence.GetTimeFrameCount();
    acquisitionSequenceInfo.Initialize(timeFrameCount);

    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        if (!acquisitionSequence.ExistFrame(timeIndex)) { continue; }

        const auto acquisitionFrame = acquisitionSequence.GetFrame(timeIndex);
        const auto& frameInfoList = acquisitionFrame.GetInfoList();

        for (const auto& frameInfo : frameInfoList) {
            const auto modality = frameInfo.modality;
            const auto type = frameInfo.type;

            const auto sequenceInfoModality = ConvertSequenceInfoModality(modality);
            const auto zSliceCount = GetZSliceCount(modality, type, configAcquisitionCount);

            acquisitionSequenceInfo.SetAcquisitionZSliceCount(sequenceInfoModality, timeIndex, zSliceCount);
        }
    }

    return acquisitionSequenceInfo;
}

auto GenerateAcquisitionSequenceInfo(const AcquisitionConfig& acquisitionConfig)->AcquisitionSequenceInfo {
    AcquisitionSequenceInfo acquisitionSequenceInfo;

    const auto configAcquisitionCount = acquisitionConfig.GetAcquisitionCount();
    
    const auto htCount = configAcquisitionCount.ht3D;
    const auto fl3DCH0Count = configAcquisitionCount.fl3DCH0;
    const auto fl3DCH1Count = configAcquisitionCount.fl3DCH1;
    const auto fl3DCH2Count = configAcquisitionCount.fl3DCH2;
    const auto fl2DCH0Count = configAcquisitionCount.fl2DCH0;
    const auto fl2DCH1Count = configAcquisitionCount.fl2DCH1;
    const auto fl2DCH2Count = configAcquisitionCount.fl2DCH2;
    const auto bfCount = configAcquisitionCount.bf;
    const auto fl3DCount = std::max(fl3DCH0Count, std::max(fl3DCH1Count, fl3DCH2Count));
    const auto fl2DCount = std::max(fl2DCH0Count, std::max(fl2DCH1Count, fl2DCH2Count));

	const auto timeFrameCount = std::max(htCount, std::max(std::max(fl3DCount, fl2DCount), bfCount));
    acquisitionSequenceInfo.Initialize(timeFrameCount);

    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        if (htCount > 0) {
            const auto zSliceCount = configAcquisitionCount.ht3DZ;
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, timeIndex, zSliceCount);
        }

        if (fl3DCount > 0) {
            const auto zSliceCount = configAcquisitionCount.fl3DZ;
            if (fl3DCH0Count > 0) {
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, timeIndex, zSliceCount);
            }
            if (fl3DCH1Count > 0) {
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH1, timeIndex, zSliceCount);
            }
            if (fl3DCH2Count > 0) {
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH2, timeIndex, zSliceCount);
            }
        }

        if (fl2DCount>0) {
            constexpr auto zSliceCount = 1;
            if (fl2DCH0Count > 0) {
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, timeIndex, zSliceCount);
            }
            if (fl2DCH1Count > 0) {
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH1, timeIndex, zSliceCount);
            }
            if (fl2DCH2Count > 0) {
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH2, timeIndex, zSliceCount);
            }
        }

        if (bfCount > 0) {
            const auto zSliceCount = configAcquisitionCount.bfChannel;
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::BF, timeIndex, zSliceCount);
        }
    }

    return acquisitionSequenceInfo;
}

auto SelectBackgroundFolderPath(const QString& rootFolderPath, const QString& systemBackgroundImageFolderPath, 
    const double& condenserNA)->std::tuple<QString, bool> {
    const auto backgroundFolderPathInRootFolder = QString("%1/bgImages").arg(rootFolderPath);

    if (QDir().exists(backgroundFolderPathInRootFolder)) {
        return { backgroundFolderPathInRootFolder, true };
    } 

    const QString backgroundNAFolderPath = QString("%1/%2").arg(systemBackgroundImageFolderPath).arg(condenserNA, 0, 'f', 2);
    return { backgroundNAFolderPath, false };
}

auto GetAcquisitionDataInfo(const QString& rootFolderPath, const QString& systemBackgroundImageFolderPath, const AcquisitionConfig& acquisitionConfig)->AcquisitionDataInfo {
    auto acquisitionSequenceInfo = ReadAcquisitionSequenceInfo(rootFolderPath, acquisitionConfig);

    if (!acquisitionSequenceInfo.IsInitialized()) {
        acquisitionSequenceInfo = GenerateAcquisitionSequenceInfo(acquisitionConfig);
    }

    const auto condenserNA = acquisitionConfig.GetDeviceInfo().condenserNA;
    const auto [backgroundFolderPath, placedInRootFolder] = 
        SelectBackgroundFolderPath(rootFolderPath, systemBackgroundImageFolderPath, condenserNA);
    
    const int32_t tileNumberX = acquisitionConfig.GetTileInfo().tileNumberX;
    const int32_t tileNumberY = acquisitionConfig.GetTileInfo().tileNumberY;
    
    AcquisitionCount acquisitionCount;
    acquisitionCount.SetTileNumber(tileNumberX, tileNumberY);
    acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

    AcquisitionDataInfo acquisitionDataInfo;
    acquisitionDataInfo.SetRootFolderPath(rootFolderPath);
    acquisitionDataInfo.SetBackgroundFolderPath(backgroundFolderPath, placedInRootFolder);
    acquisitionDataInfo.SetAcquisitionCount(acquisitionCount);
    
    return acquisitionDataInfo;
}

auto ProcessingFolderScanner::Scan(const QString& topFolderPath, const QString& systemBackgroundImageFolderPath) -> bool {
	if (isScanning) { return true; }

    isScanning = true;
    const auto rootDirectoryList = RootDirectoryFinder::GetRootDirectoryList(topFolderPath);

    auto repoInstance = TaskRepo::GetInstance();

    for (const auto& rootFolderPath : rootDirectoryList) {
        const auto [validConfig, readAcquisitionConfig] = ReadAcquisitionConfig(rootFolderPath);
        if (!validConfig) { continue; }

        const auto acquisitionConfig = EditAcquisitionConfig(readAcquisitionConfig);
        if (!CheckValidAcquisitionConfig(acquisitionConfig)) { continue; }

        const auto acquisitionDataInfo = GetAcquisitionDataInfo(rootFolderPath, systemBackgroundImageFolderPath, acquisitionConfig);
        const auto& acquisitionCount = acquisitionDataInfo.GetAcquisitionCount();

        const auto isNewRootScanned = !repoInstance->IsAdded(rootFolderPath);
        if (isNewRootScanned) {
            const auto tcfFilePath = ResultFilePathGenerator::GetTCFFilePath(rootFolderPath);

            if (QFile::exists(tcfFilePath)) {
                repoInstance->AddAlreadyDone(rootFolderPath);
            } else {
                AcquisitionDataScanner acquisitionDataScanner;
                acquisitionDataScanner.SetAcquisitionDataInfo(acquisitionDataInfo);
                acquisitionDataScanner.Scan();

                const auto acquiredDataFlag = acquisitionDataScanner.GetAcquiredDataFlag();

                TaskInfoSetScanner taskInfoSetScanner;
                taskInfoSetScanner.SetRootFolderPath(rootFolderPath);
                taskInfoSetScanner.SetAcquisitionCount(acquisitionCount);
                taskInfoSetScanner.Scan();

                const auto taskInfoSet = taskInfoSetScanner.GetTaskInfoSet();

                repoInstance->AddNotDone(rootFolderPath, acquiredDataFlag, taskInfoSet, acquisitionCount);
            }
        } else {
            const auto isDone = repoInstance->IsDone(rootFolderPath);
            const auto isAlreadyDone = repoInstance->IsAlreadyDone(rootFolderPath);
            const auto isFailed = repoInstance->IsFailed(rootFolderPath);

            const auto dataScanNotNeeded = isDone || isAlreadyDone || isFailed;
            if (dataScanNotNeeded) { continue; }

            AcquisitionScanTargetFinder acquisitionScanTargetFinder;
            acquisitionScanTargetFinder.SetRootFolderPath(rootFolderPath);
            if (!acquisitionScanTargetFinder.Find()) { continue; }

            const auto bgScanFlag = acquisitionScanTargetFinder.IsNextScanBG();
            const auto timeIndex = acquisitionScanTargetFinder.GetNextScanTimeIndex();
            const auto tileIndex = acquisitionScanTargetFinder.GetNextScanTileIndex();
            const auto modalityType = acquisitionScanTargetFinder.GetNextScanModalityType();

            AcquisitionDataTargetScanner acquisitionDataTargetScanner;
            acquisitionDataTargetScanner.SetAcquiredDataFlag(repoInstance->GetAcquiredDataFlag(rootFolderPath));
            acquisitionDataTargetScanner.SetAcquisitionDataInfo(acquisitionDataInfo);
            acquisitionDataTargetScanner.SetTarget(bgScanFlag, timeIndex, tileIndex, modalityType);

            if (!acquisitionDataTargetScanner.Scan()) { continue; }
            const auto acquiredDataFlag = acquisitionDataTargetScanner.GetAcquiredDataFlag();

            repoInstance->Update(rootFolderPath, acquiredDataFlag);
        }
    }

    isScanning = false;
    return true;
}