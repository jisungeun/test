#pragma once

#include <memory>
#include <QString>

#include "AcquisitionConfig.h"

class ProcessorHT {
public:
    ProcessorHT();
    ~ProcessorHT();

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto SetBackgroundFolderPath(const QString& backgroundFolderPath)->void;
    auto SetMatlabModuleFolderPath(const QString& matlabModuleFolderPath)->void;
    auto SetPsfFolderPath(const QString& psfFolderPath)->void;
    auto SetAcquisitionConfig(const AcquisitionConfig& acquisitionConfig)->void;
    auto SetSampleImageCropFlag(const bool& sampleImageCropFlag)->void;

    auto Process(const int32_t& tileIndex, const int32_t& timeIndex)->bool;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
