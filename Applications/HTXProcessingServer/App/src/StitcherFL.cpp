#define LOGGER_TAG "[StitcherFL]"

#include "StitcherFL.h"

#include <QFile>
#include <QStringList>

#include "H5Cpp.h"
#include "HDF5Mutex.h"
#include "HTVoxelSizeReader.h"
#include "OverlapRelationCalculatorPhaseCorrelation.h"
#include "ResultFilePathGenerator.h"
#include "StitchingWriterHDF5FL.h"
#include "TakenUnitTimeToProcess.h"
#include "TCLogger.h"
#include "TileConfigurationReaderFL.h"
#include "TilePositionCalculatorBestOverlap.h"
#include "TileSetGeneratorFL.h"
#include "TilePositionSetReader.h"

auto CalculateFLTilePositionMap(const TilePositionSet& htTilePositionSet, const int32_t& tileNumberX,
    const int32_t& tileNumberY, const double& htResolutionXY, const double& flResolutionXY) ->TilePositionSet {
    const auto htToFLRatio = htResolutionXY / flResolutionXY;

    TilePositionSet flTilePositionSet;
    flTilePositionSet.SetTileNumber(tileNumberX, tileNumberY);
    for (auto tileIndexX = 0; tileIndexX < tileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < tileNumberY; ++tileIndexY) {
            const auto tilePosition = htTilePositionSet.GetTilePosition(tileIndexX, tileIndexY);

            const auto calculatedTilePositionX = std::round(tilePosition.GetTilePositionX() * htToFLRatio);
            const auto calculatedTilePositionY = std::round(tilePosition.GetTilePositionY() * htToFLRatio);
            const auto calculatedTilePositionZ = std::round(tilePosition.GetTilePositionZ() * 1);

            TilePosition calculatedTilePosition;
            calculatedTilePosition.SetPositions(calculatedTilePositionX, calculatedTilePositionY, calculatedTilePositionZ);

            flTilePositionSet.InsertTilePosition(tileIndexX, tileIndexY, calculatedTilePosition);
        }
    }

    return flTilePositionSet;
}

class StitcherFL::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    int32_t channelIndex{};

    int32_t tileNumberX{};
    int32_t tileNumberY{};

    double limitedSizeXInMicrometer{};
    double limitedSizeYInMicrometer{};
    bool sizeLimited{ false };

    auto GetProcessedDataFilePathList(const int32_t& tileNumber, const int32_t& timeFrameIndex, const int32_t& channelIndex)->QStringList;
    auto GetPositionFilePathList(const int32_t& tileNumber)->QStringList;
    auto MergeToMap(const QStringList& processedDataFilePathList, const QStringList& positionFilePathList)->QMap<QString, QString>;
    auto FilterOverlapRelationSet(const OverlapRelationSet& overlapRelationSet, const TileConfiguration& tileConfiguration, const float& voxelSize)->OverlapRelationSet;
    auto ReadVoxelSizeXY(const QString& processedDataFilePath)->double;
};

auto StitcherFL::Impl::GetProcessedDataFilePathList(const int32_t& tileNumber, const int32_t& timeFrameIndex, const int32_t& channelIndex) -> QStringList {
    const QString writingTempFolderPath = this->rootFolderPath + "/temp/FL";

    QStringList processedDataFilePathList{};

    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        const auto processedDataFilePath = QString("%1/fl_Processed_CH%2_tile%3_time%4")
            .arg(writingTempFolderPath).arg(channelIndex).arg(tileIndex, 4, 10, QChar('0'))
            .arg(timeFrameIndex, 4, 10, QChar('0'));

        processedDataFilePathList.push_back(processedDataFilePath);
    }

    return processedDataFilePathList;
}

auto StitcherFL::Impl::GetPositionFilePathList(const int32_t& tileNumber) -> QStringList {
    QStringList positionFilePathList{};

    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        auto positionFilePath = QString("%1/data/%2/position.ini").arg(this->rootFolderPath).arg(tileIndex, 4, 10, QChar('0'));
        if (!QFile::exists(positionFilePath)) {
            positionFilePath = QString("%1/data/%2/position.ini").arg(this->rootFolderPath).arg(tileIndex);
        }

        positionFilePathList.push_back(positionFilePath);
    }

    return positionFilePathList;
}

auto StitcherFL::Impl::MergeToMap(const QStringList& processedDataFilePathList,
    const QStringList& positionFilePathList) -> QMap<QString, QString> {
    const auto numberOfData = processedDataFilePathList.size();

    QMap<QString, QString> dataPositionFilePathMap{};
    for (auto index = 0; index < numberOfData; ++index) {
        dataPositionFilePathMap[processedDataFilePathList.at(index)] = positionFilePathList.at(index);
    }

    return dataPositionFilePathMap;
}

auto StitcherFL::Impl::FilterOverlapRelationSet(const OverlapRelationSet& overlapRelationSet,
    const TileConfiguration& tileConfiguration, const float& voxelSize) -> OverlapRelationSet {
    const auto tileNumberX = tileConfiguration.GetTileNumberX();
    const auto tileNumberY = tileConfiguration.GetTileNumberY();

    const auto overlapLengthX = tileConfiguration.GetOverlapLengthXInPixel();
    const auto overlapLengthY = tileConfiguration.GetOverlapLengthYInPixel();

    const auto overlapGridNumberX = 2 * tileNumberX - 1;
    const auto overlapGridNumberY = 2 * tileNumberY - 1;

    constexpr float filteringThresholdCorrelation = 0.7f;
    constexpr float filteringThresholdXShift = 4.f;
    constexpr float filteringThresholdYShift = 4.f;

    OverlapRelationSet filteredOverlapRelationSet;
    filteredOverlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
    for (auto overlapIndexX = 0; overlapIndexX < overlapGridNumberX; ++overlapIndexX) {
        for (auto overlapIndexY = 0; overlapIndexY < overlapGridNumberY; ++overlapIndexY) {
            if ((overlapIndexX + overlapIndexY) % 2 == 0) { continue; }
            const auto overlapRelation = overlapRelationSet.GetOverlapRelation(overlapIndexX, overlapIndexY);

            const auto reliability = overlapRelation.GetReliability();
            const auto shiftX = overlapRelation.GetShiftValueX();
            const auto shiftY = overlapRelation.GetShiftValueY();

            bool tooBigX{ false }, tooBigY{ false };

            //const auto tooWeakReliability = reliability <= 0.2;
            if (overlapLengthX > 0) {
                tooBigX = std::abs(shiftX) * voxelSize >= filteringThresholdXShift;
            }

            if (overlapLengthY > 0) {
                tooBigY = std::abs(shiftY) * voxelSize >= filteringThresholdYShift;
            }

            //const auto filtered = tooWeakReliability || tooBigX || tooBigY;
            const auto filtered = tooBigX || tooBigY;


            OverlapRelation filteredOverlapRelation;
            if (filtered) {
                filteredOverlapRelation.SetReliability(0);
                filteredOverlapRelation.SetShiftValue(0, 0, 0);
            } else {
                filteredOverlapRelation = overlapRelation;
            }

            filteredOverlapRelationSet.InsertOverlapRelation(overlapIndexX, overlapIndexY, filteredOverlapRelation);
        }
    }

    return filteredOverlapRelationSet;
}

auto StitcherFL::Impl::ReadVoxelSizeXY(const QString& processedDataFilePath) -> double {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    float pixelWorldSizeX{};

    const H5::H5File file(processedDataFilePath.toStdString(), H5F_ACC_RDONLY);
    const auto dataSet = file.openDataSet("Data");
    const auto attrPixelWorldSizeX = dataSet.openAttribute("pixelWorldSizeX");
    attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);

    return pixelWorldSizeX;
}

StitcherFL::StitcherFL() : d(new Impl()) {
}

StitcherFL::~StitcherFL() = default;

auto StitcherFL::SetRootFolderPath(const QString & rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto StitcherFL::SetChannelIndex(const int32_t& channelIndex) -> void {
    d->channelIndex = channelIndex;
}

auto StitcherFL::SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY) -> void {
    d->tileNumberX = tileNumberX;
    d->tileNumberY = tileNumberY;
}

auto StitcherFL::SetLimitSize(const double& sizeXInMicrometer, const double& sizeYInMicrometer) -> void {
    d->limitedSizeXInMicrometer = sizeXInMicrometer;
    d->limitedSizeYInMicrometer = sizeYInMicrometer;

    d->sizeLimited = true;
}

auto StitcherFL::Stitch(const int32_t& timeIndex) -> bool {
    const auto tileNumber = d->tileNumberX * d->tileNumberY;

    const auto positionFilePathList = d->GetPositionFilePathList(tileNumber);

    const QString writingTempFolderPath = d->rootFolderPath + "/temp/FL";

	QLOG_INFO() << "FL Stitcher time(" << timeIndex << ")" << "CH" << d->channelIndex;

    const auto processedDataFilePathList = d->GetProcessedDataFilePathList(tileNumber, timeIndex, d->channelIndex);

    TileConfigurationReaderFL tileConfigurationReaderFL;
    tileConfigurationReaderFL.SetPositionFileList(positionFilePathList);
    tileConfigurationReaderFL.SetProcessedFilePathList(processedDataFilePathList);

    tileConfigurationReaderFL.Read();

    const auto tileConfiguration = tileConfigurationReaderFL.GetTileConfiguration();

    const auto dataPositionFilePathMap = d->MergeToMap(processedDataFilePathList, positionFilePathList);

    TileSetGeneratorFL tileSetGeneratorFL;
    tileSetGeneratorFL.SetTileConfiguration(tileConfiguration);
    tileSetGeneratorFL.SetDataPositionFilePathMap(dataPositionFilePathMap);
    tileSetGeneratorFL.Generate();

    const auto tileSet = tileSetGeneratorFL.GetTileSet();

    auto htTilePositionSetExists = true;
	const auto htProcessingFilePath = ResultFilePathGenerator::GetHTProcessedFilePath(d->rootFolderPath, 0, timeIndex);
    HTVoxelSizeReader htVoxelSizeReader;
    if (QFile::exists(htProcessingFilePath)) {
        htVoxelSizeReader.SetProcessedFilePath(htProcessingFilePath);
        const auto voxelSizeReadResult = htVoxelSizeReader.Read();

        htTilePositionSetExists &= voxelSizeReadResult;
    }
    
    const auto htStitchingFilePath = ResultFilePathGenerator::GetHTStitchingFilePath(d->rootFolderPath, timeIndex);
    TilePositionSetReader tilePositionSetReader;
    if (QFile::exists(htStitchingFilePath)) {
        tilePositionSetReader.SetTargetFilePath(htStitchingFilePath);
        const auto tilePositionSetReadResult = tilePositionSetReader.Read();

        htTilePositionSetExists &= tilePositionSetReadResult;
    }

    const auto flVoxelSizeXY = d->ReadVoxelSizeXY(processedDataFilePathList.first());

    TilePositionSet tilePositionSet;
    if (htTilePositionSetExists){
        const auto htTilePositionSet = tilePositionSetReader.GetTilePositionSet();
        const auto htVoxelSizeXY = htVoxelSizeReader.GetVoxelSizeXY();
       
        tilePositionSet = CalculateFLTilePositionMap(htTilePositionSet, d->tileNumberX, d->tileNumberY, htVoxelSizeXY, flVoxelSizeXY);
    } else {
        OverlapRelationCalculatorPhaseCorrelation overlapRelationCalculatorPhaseCorrelation;
        overlapRelationCalculatorPhaseCorrelation.SetTileConfiguration(tileConfiguration);
        overlapRelationCalculatorPhaseCorrelation.SetTileSet(tileSet);
        const auto overlapRelationSet = overlapRelationCalculatorPhaseCorrelation.Calculate();

        const auto filteredOverlapRelationSet = d->FilterOverlapRelationSet(overlapRelationSet, tileConfiguration, flVoxelSizeXY);

        TilePositionCalculatorBestOverlap tilePositionCalculatorBestOverlap;
        tilePositionCalculatorBestOverlap.SetTileConfiguration(tileConfiguration);
        tilePositionCalculatorBestOverlap.SetOverlapRelationSet(filteredOverlapRelationSet);
        tilePositionSet = tilePositionCalculatorBestOverlap.Calculate();
    }

    const auto stitchingResultFilePath = 
        ResultFilePathGenerator::GetFLStitchingFilePath(d->rootFolderPath, d->channelIndex, timeIndex);

    StitchingWriterHDF5FL stitchingWriterHdf5FL;
    stitchingWriterHdf5FL.SetTileSet(tileSet);
    stitchingWriterHdf5FL.SetTilePositionSet(tilePositionSet);
    stitchingWriterHdf5FL.SetTileConfiguration(tileConfiguration);
    stitchingWriterHdf5FL.SetHDF5FilePath(stitchingResultFilePath);
    stitchingWriterHdf5FL.SetBoundaryCropFlag(true);
    if (d->sizeLimited == true) {
        const auto limitedSizeX = static_cast<int32_t>(std::round(d->limitedSizeXInMicrometer / flVoxelSizeXY));
        const auto limitedSizeY = static_cast<int32_t>(std::round(d->limitedSizeYInMicrometer / flVoxelSizeXY));

        stitchingWriterHdf5FL.SetDataSizeLimit(limitedSizeX, limitedSizeY);
    }
    if (!stitchingWriterHdf5FL.Run()) { return false; }

    return true;
}
