#include "AcquisitionDataTargetScanner.h"

#include <QFile>
#include <QMutex>

#include "DataFolderPathGetter.h"
#include "ImageIntegrityCheckerPng.h"
#include "ModalityType.h"

using SequenceModality = AcquisitionSequenceInfo::Modality;

class AcquisitionDataTargetScanner::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    AcquisitionDataInfo acquisitionDataInfo{};
    AcquiredDataFlag acquiredDataFlag{};
    QString rootFolderPath{};

    AcquisitionSequenceInfo acquisitionSequenceInfo{};

    QMutex mutex{};

    bool targetSet{ false };

    bool bgScanFlag{ false };
    int32_t timeIndex{};
    int32_t tileIndex{};
    ModalityType type{ ModalityType::Name::None };

    auto ScanBackgroundImageAcquisition()->void;
    auto ScanHTAcquisition()->void;
    auto ScanFLAcquisition()->void;
    auto ScanBFAcquisition()->void;

    auto CheckImageIntegrity(const QString& imageFolderPath, const uint32_t& numberOfImages,
        const bool& moreThanCount = false) const ->bool;
};

auto AcquisitionDataTargetScanner::Impl::ScanBackgroundImageAcquisition() -> void {
    const auto backgroundFolderPath = this->acquisitionDataInfo.GetBackgroundFolderPath();

    if (this->acquisitionDataInfo.IsBackgroundPlacedInRootFolder()) {
        constexpr int32_t numberOfImages = 4;
        const auto backgroundImageDataAcquired = this->CheckImageIntegrity(backgroundFolderPath, numberOfImages);
        this->acquiredDataFlag.SetBGAcquiredFlag(backgroundImageDataAcquired);
    } else {
        constexpr int32_t numberOfImages = 80;
        const auto backgroundImageDataAcquired = this->CheckImageIntegrity(backgroundFolderPath, numberOfImages, true);
        this->acquiredDataFlag.SetBGAcquiredFlag(backgroundImageDataAcquired);
    }
}

auto AcquisitionDataTargetScanner::Impl::ScanHTAcquisition() -> void {
    DataFolderPathGetter dataFolderPathGetter;
    dataFolderPathGetter.SetRootFolderPath(this->rootFolderPath);
    dataFolderPathGetter.SetModalityType(this->type);

    const auto zSliceCount = this->acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::HT, this->timeIndex);
    const auto numberOfImages = zSliceCount * 4;
    dataFolderPathGetter.SetTimeIndex(this->timeIndex);
    dataFolderPathGetter.SetTileIndex(this->tileIndex);

    const auto htFolderPath = dataFolderPathGetter.GetTimeFrameFolderPath();
    const bool htDataAcquired = this->CheckImageIntegrity(htFolderPath, numberOfImages);
    this->acquiredDataFlag.SetAcquiredFlag(this->timeIndex, this->tileIndex, ModalityType{ ModalityType::Name::HT }, htDataAcquired);
}

auto AcquisitionDataTargetScanner::Impl::ScanFLAcquisition() -> void {
    DataFolderPathGetter dataFolderPathGetter;
    dataFolderPathGetter.SetRootFolderPath(this->rootFolderPath);
    dataFolderPathGetter.SetModalityType(this->type);

    const auto channelIndex = this->type.GetChannelIndex();

    SequenceModality modality;
    if (channelIndex == 0) {
        modality = SequenceModality::FLCH0;
    } else if (channelIndex == 1) {
        modality = SequenceModality::FLCH1;
    } else if (channelIndex == 2) {
        modality = SequenceModality::FLCH2;
    } else {
        modality = SequenceModality::FLCH0; //error;
    }

    const auto zSliceCount = this->acquisitionSequenceInfo.GetAcquisitionZSliceCount(modality, this->timeIndex);
    
    const auto numberOfImages = zSliceCount;
    dataFolderPathGetter.SetTimeIndex(this->timeIndex);
    dataFolderPathGetter.SetTileIndex(this->tileIndex);

    const auto flFolderPath = dataFolderPathGetter.GetTimeFrameFolderPath();
    const bool flDataAcquired = this->CheckImageIntegrity(flFolderPath, numberOfImages);
    this->acquiredDataFlag.SetAcquiredFlag(this->timeIndex, this->tileIndex, ModalityType{ ModalityType::Name::FL, channelIndex }, flDataAcquired);
}

auto AcquisitionDataTargetScanner::Impl::ScanBFAcquisition() -> void {
    DataFolderPathGetter dataFolderPathGetter;
    dataFolderPathGetter.SetRootFolderPath(this->rootFolderPath);
    dataFolderPathGetter.SetModalityType(this->type);

    const auto zSliceCount = this->acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::BF, this->timeIndex);
    const auto numberOfImages = zSliceCount;
    dataFolderPathGetter.SetTimeIndex(this->timeIndex);
    dataFolderPathGetter.SetTileIndex(this->tileIndex);

    const auto bfFolderPath = dataFolderPathGetter.GetTimeFrameFolderPath();
    const bool bfDataAcquired = this->CheckImageIntegrity(bfFolderPath, numberOfImages);
    this->acquiredDataFlag.SetAcquiredFlag(this->timeIndex, this->tileIndex, ModalityType{ ModalityType::Name::BF }, bfDataAcquired);
}

auto AcquisitionDataTargetScanner::Impl::CheckImageIntegrity(const QString& imageFolderPath,
    const uint32_t& numberOfImages, const bool& moreThanCount) const -> bool {
    if (QFile::exists(imageFolderPath)) {
        ImageIntegrityCheckerPng imageIntegrityCheckerPng;
        imageIntegrityCheckerPng.SetImageFolderPath(imageFolderPath);
        imageIntegrityCheckerPng.SetNumberOfImages(numberOfImages);

        if (moreThanCount) {
            return imageIntegrityCheckerPng.CheckMoreThanCount();
        }

        return imageIntegrityCheckerPng.CheckSameCount();
    }
    return false;
}

AcquisitionDataTargetScanner::AcquisitionDataTargetScanner() : d{ std::make_unique<Impl>() } {
}

AcquisitionDataTargetScanner::~AcquisitionDataTargetScanner() = default;

auto AcquisitionDataTargetScanner::SetAcquisitionDataInfo(const AcquisitionDataInfo& acquisitionDataInfo) -> void {
    QMutexLocker locker{ &d->mutex };
    d->acquisitionDataInfo = acquisitionDataInfo;
    d->targetSet = false;
}

auto AcquisitionDataTargetScanner::SetAcquiredDataFlag(const AcquiredDataFlag& acquiredDataFlag) -> void {
    QMutexLocker locker{ &d->mutex };
    d->acquiredDataFlag = acquiredDataFlag;
}

auto AcquisitionDataTargetScanner::SetTarget(const bool& bgScanFlag, const int32_t& timeIndex, const int32_t& tileIndex,
    const ModalityType& type) -> void {
    QMutexLocker locker{ &d->mutex };
    d->bgScanFlag = bgScanFlag;
    d->timeIndex = timeIndex;
    d->tileIndex = tileIndex;
    d->type = type;

    d->targetSet = true;
}

auto AcquisitionDataTargetScanner::Scan() -> bool {
    QMutexLocker locker{ &d->mutex };

    const auto& acquisitionCount = d->acquisitionDataInfo.GetAcquisitionCount();

    d->rootFolderPath = d->acquisitionDataInfo.GetRootFolderPath();
    d->acquisitionSequenceInfo = acquisitionCount.GetAcquisitionSequenceInfo();

    if (d->bgScanFlag) {
        d->ScanBackgroundImageAcquisition();
        return true;
    }

    const auto modalityName = d->type.GetName();

    if (modalityName == ModalityType::Name::HT) {
        d->ScanHTAcquisition();
        return true;
    }

    if (modalityName == ModalityType::Name::FL) {
        d->ScanFLAcquisition();
        return true;
    }

    if (modalityName == ModalityType::Name::BF) {
        d->ScanBFAcquisition();
        return true;
    }

    return false;
}

auto AcquisitionDataTargetScanner::GetAcquiredDataFlag() const -> const AcquiredDataFlag& {
    QMutexLocker locker{ &d->mutex };
    return d->acquiredDataFlag;
}
