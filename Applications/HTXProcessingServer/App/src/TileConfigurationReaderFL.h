#pragma once

#include <memory>
#include <QStringList>

#include "TileConfiguration.h"

class TileConfigurationReaderFL {
public:
    TileConfigurationReaderFL();
    ~TileConfigurationReaderFL();

    auto SetPositionFileList(const QStringList& positionFileList)->void;
    auto SetProcessedFilePathList(const QStringList& processedFilePathList)->void;

    auto Read()->bool;
    auto GetTileConfiguration()const->const TileConfiguration&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
