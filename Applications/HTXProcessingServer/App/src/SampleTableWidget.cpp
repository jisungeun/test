#include "SampleTableWidget.h"
#include <QHeaderView>
#include <QMenu>
#include <QTime>

constexpr auto numberOfColumn = 4;

constexpr auto columnIndexPath = 0;
constexpr auto columnIndexStatus = 1;
constexpr auto columnIndexRemainingTime = 2;
constexpr auto columnIndexTCF = 3;

const QMap<ProcessingStatus, Qt::GlobalColor> processingColorMap{
    {ProcessingStatus::WaitForProcess, Qt::white},
    {ProcessingStatus::WaitForAcquisition, Qt::white},
    {ProcessingStatus::IsProcessing, Qt::white},
    {ProcessingStatus::AlreadyProcessed, Qt::gray},
    {ProcessingStatus::ProcessingFail, Qt::red},
    {ProcessingStatus::ProperlyProcessed, Qt::green},
    {ProcessingStatus::UnhandledStatus, Qt::red}};

const QMap<ProcessingStatus, QString> processingStringMap{
    {ProcessingStatus::WaitForProcess, "Please wait"},
    {ProcessingStatus::WaitForAcquisition, "Please wait"},
    {ProcessingStatus::IsProcessing, "Processing."},
    {ProcessingStatus::AlreadyProcessed, "Processed"},
    {ProcessingStatus::ProcessingFail, "Processing failed"},
    {ProcessingStatus::ProperlyProcessed, "Processed."},
    {ProcessingStatus::UnhandledStatus, "Unhandled Status"} };

auto GetTextColor(const SampleObject& sampleObject, const bool& onProcessing)->Qt::GlobalColor {
    if (sampleObject.tcfExist) {
        return processingColorMap[sampleObject.processingStatus];
    } else {
        if (onProcessing) {
            return processingColorMap[sampleObject.processingStatus];
        } else {
            const auto& dataAcquisitionStatus = sampleObject.dataAcquisitionStatus;
            const auto& processingStatus = sampleObject.processingStatus;

            if (processingStatus == +ProcessingStatus::ProcessingFail) {
                return processingColorMap[ProcessingStatus::ProcessingFail];
            }

            if (dataAcquisitionStatus == +DataAcquisitionStatus::NotFullyAcquired) {
                return Qt::yellow;
            } else if (dataAcquisitionStatus == +DataAcquisitionStatus::FullyAcquired) {
                return Qt::white;
            } else if (dataAcquisitionStatus == +DataAcquisitionStatus::BackgroundFault) {
                return Qt::yellow;
            }
            return Qt::white;
        }
    }
}

auto GetStatusText(const SampleObject& sampleObject, const bool& onProcessing)->QString {
    if (sampleObject.tcfExist) {
        return processingStringMap[sampleObject.processingStatus];
    } else {
        if (onProcessing) {
            return processingStringMap[sampleObject.processingStatus];
        } else {
            const auto& dataAcquisitionStatus = sampleObject.dataAcquisitionStatus;

            if (dataAcquisitionStatus == +DataAcquisitionStatus::NotFullyAcquired) {
                return "Data count does not match";
            } else if (dataAcquisitionStatus == +DataAcquisitionStatus::FullyAcquired) {
                return processingStringMap[sampleObject.processingStatus];
            } else if (dataAcquisitionStatus == +DataAcquisitionStatus::BackgroundFault) {
                return "Background image count does not match.";
            }

            return "Programming Error";
        }
    }
}

auto GetTextFont(const SampleObject& sampleObject, const bool& onProcessing)->QFont {
    QFont font;

    if (onProcessing) {
        if (sampleObject.processingStatus == +ProcessingStatus::IsProcessing) {
            font.setBold(true);
        }
    }
    
    return font;
}

struct SampleTableWidget::Impl {
    Impl() = default;
    ~Impl() = default;

    int32_t recentRowIndex{};
};

SampleTableWidget::SampleTableWidget(QWidget* parent)
    : QTableWidget(parent), d(new Impl()) {
    setContextMenuPolicy(Qt::CustomContextMenu);
    setColumnCount(numberOfColumn);
    setHorizontalHeaderLabels(QStringList{ "Path", "Status", "remains", "TCF" });
    setSelectionMode(NoSelection);
    setEditTriggers(NoEditTriggers);

    setColumnWidth(columnIndexStatus, 240);
    setColumnWidth(columnIndexRemainingTime, 80);
    setColumnWidth(columnIndexTCF, 40);

    const auto horizontalHeaderPointer = horizontalHeader();
    horizontalHeaderPointer->setSectionResizeMode(0, QHeaderView::Stretch);

    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(ShowContextMenu(const QPoint&)));
}

SampleTableWidget::~SampleTableWidget() = default;

auto SampleTableWidget::UpdateSampleTableWidget(const QList<SampleObject>& sampleObjectList, 
    const QString& topDirectory, const bool& onProcessing)-> void {
    const auto numberOfObject = sampleObjectList.size();
    setRowCount(numberOfObject);

    for (auto rowIndex = 0; rowIndex < numberOfObject; ++rowIndex) {
        const auto& sampleObject = sampleObjectList.at(rowIndex);

        const auto textColor = GetTextColor(sampleObject, onProcessing);
        const auto statusText = GetStatusText(sampleObject, onProcessing);
        const auto textFont = GetTextFont(sampleObject, onProcessing);

        { // path cell
            auto rootPath = sampleObject.rootPath;
            auto path = rootPath.replace(topDirectory + "/", "");
            if (path.isEmpty()) { path = topDirectory; }

            const auto rootPathItem = new QTableWidgetItem(path);
            rootPathItem->setForeground(textColor);
            rootPathItem->setFont(textFont);
            setItem(rowIndex, columnIndexPath, rootPathItem);
        }

        { // status cell
            const auto statusItem = new QTableWidgetItem(statusText);
            statusItem->setForeground(textColor);
            statusItem->setTextAlignment(Qt::AlignCenter);
            statusItem->setFont(textFont);
            setItem(rowIndex, columnIndexStatus, statusItem);
        }

        { // remainingTime cell
            QTime remainingTime(0, 0, 0);
            const auto remainingTimeInHours =
                static_cast<int32_t>(ConvertUnit(sampleObject.remainingTime, sampleObject.remainingTimeUnit, TimeUnit::Hour));
            const auto remainingTimeInSec =
                static_cast<int32_t>(ConvertUnit(sampleObject.remainingTime, sampleObject.remainingTimeUnit, TimeUnit::Second));

            const auto remainingTimeInDays = remainingTimeInHours / 24;

            QString remainingTimeString;
            if (remainingTimeInDays > 1) {
                const auto remainingTimeInSecExcludingDay = remainingTimeInSec - remainingTimeInDays * 86400;
                remainingTime = remainingTime.addSecs(remainingTimeInSecExcludingDay);

                remainingTimeString = QString("%1d %2").arg(remainingTimeInDays).arg(remainingTime.toString("hh:mm:ss"));
            } else {
                remainingTime = remainingTime.addSecs(remainingTimeInSec);
                remainingTimeString = remainingTime.toString("hh:mm:ss");
            }
            
            const auto remainingTimeItem = new QTableWidgetItem(remainingTimeString);
            remainingTimeItem->setForeground(textColor);
            remainingTimeItem->setTextAlignment(Qt::AlignCenter);
            remainingTimeItem->setFont(textFont);
            setItem(rowIndex, columnIndexRemainingTime, remainingTimeItem);
        }

        { // tcf cell
            QTableWidgetItem* tcfExistItem;
            if (sampleObject.tcfExist) {
                tcfExistItem = new QTableWidgetItem("O");
            } else {
                tcfExistItem = new QTableWidgetItem("X");
            }
            tcfExistItem->setForeground(textColor);

            tcfExistItem->setTextAlignment(Qt::AlignCenter);
            tcfExistItem->setFont(textFont);
            setItem(rowIndex, columnIndexTCF, tcfExistItem);
        }
    }

}

void SampleTableWidget::ShowContextMenu(const QPoint& point) {
    const auto item = this->itemAt(point);

    if (item != nullptr) {
        const auto rowIndex = item->row();
        const auto rowCount = this->rowCount();

        if (rowIndex < rowCount) {
            const auto columnIndex = item->column();
            if (columnIndex == 0) {
                d->recentRowIndex = rowIndex;

                QMenu contextMenu(tr("Context menu"), this);

                QAction openFolderAction("Open folder", this);
                connect(&openFolderAction, SIGNAL(triggered()), this, SLOT(OpenFolderTriggered()));
                contextMenu.addAction(&openFolderAction);

                contextMenu.exec(mapToGlobal(point));
            }
        }
    }
}

void SampleTableWidget::OpenFolderTriggered() {
    emit OpenFolder(d->recentRowIndex);
}
