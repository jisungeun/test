#pragma once

#include <memory>

#include <QList>

class RemainingTaskCount {
public:
    RemainingTaskCount();
    RemainingTaskCount(const RemainingTaskCount& other);
    ~RemainingTaskCount();

    auto operator=(const RemainingTaskCount& other)->RemainingTaskCount&;

    auto SetHTProcessingCount(const int32_t& zSliceCount, const int32_t& htProcessingCount)->void;
    auto SetFLProcessingCount(const int32_t& zSliceCount, const int32_t& flProcessingCount)->void;
    auto SetBFProcessingCount(const int32_t& zSliceCount, const int32_t& bfProcessingCount)->void;

    auto SetHTStitchingCount(const int32_t& zSliceCount, const int32_t& htStitchingCount)->void;
    auto SetFLStitchingCount(const int32_t& zSliceCount, const int32_t& flStitchingCount)->void;
    auto SetBFStitchingCount(const int32_t& zSliceCount, const int32_t& bfStitchingCount)->void;

    auto SetHTStitchingWrittenCount(const int32_t& zSliceCount, const int32_t& htStitchingWrittenCount)->void;
    auto SetFLStitchingWrittenCount(const int32_t& zSliceCount, const int32_t& flStitchingWrittenCount)->void;
    auto SetBFStitchingWrittenCount(const int32_t& zSliceCount, const int32_t& bfStitchingWrittenCount)->void;

    auto SetHTLDMConvertingCount(const int32_t& zSliceCount, const int32_t& htLDMConvertingCount)->void;
    auto SetFLLDMConvertingCount(const int32_t& zSliceCount, const int32_t& flLDMConvertingCount)->void;
    auto SetBFLDMConvertingCount(const int32_t& zSliceCount, const int32_t& bfLDMConvertingCount)->void;

    auto GetHTProcessingCount(const int32_t& zSliceCount)->int32_t;
    auto GetFLProcessingCount(const int32_t& zSliceCount)->int32_t;
    auto GetBFProcessingCount(const int32_t& zSliceCount)->int32_t;

    auto GetHTStitchingCount(const int32_t& zSliceCount)->int32_t;
    auto GetFLStitchingCount(const int32_t& zSliceCount)->int32_t;
    auto GetBFStitchingCount(const int32_t& zSliceCount)->int32_t;

    auto GetHTStitchingWrittenCount(const int32_t& zSliceCount)->int32_t;
    auto GetFLStitchingWrittenCount(const int32_t& zSliceCount)->int32_t;
    auto GetBFStitchingWrittenCount(const int32_t& zSliceCount)->int32_t;

    auto GetHTLDMConvertingCount(const int32_t& zSliceCount)->int32_t;
    auto GetFLLDMConvertingCount(const int32_t& zSliceCount)->int32_t;
    auto GetBFLDMConvertingCount(const int32_t& zSliceCount)->int32_t;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};