#pragma once
#include <QMap>
#include <memory>

#include <tuple>

#include "AcquisitionConfig.h"
#include "SampleObject.h"
#include "AcquisitionSequenceInfo.h"

class MultiModalProcessor {
public:
    MultiModalProcessor();
    ~MultiModalProcessor();

    auto Initialize()->bool;

    auto HTProcess(const int32_t& tileIndex, const int32_t& timeIndex)->bool;
    auto FLProcess(const int32_t& flChannelIndex, const int32_t& tileIndex, const int32_t& timeIndex)->bool;
    auto BFProcess(const int32_t& tileIndex, const int32_t& timeIndex)->bool;

    auto HTStitch(const int32_t& timeIndex)->bool;
    auto FLStitch(const int32_t& flChannelIndex, const int32_t& timeIndex)->bool;
    auto BFStitch(const int32_t& timeIndex)->bool;

    auto HTWriteThumbnail(const int32_t& timeIndex, const AcquisitionSequenceInfo& acquisitionSequenceInfo) const ->bool;
    auto FLWriteThumbnail(const QList<int32_t>& flChannelIndexList, const int32_t& timeIndex, 
        const AcquisitionSequenceInfo& acquisitionSequenceInfo) const ->bool;
    auto BFWriteThumbnail(const int32_t& timeIndex, const AcquisitionSequenceInfo& acquisitionSequenceInfo) const ->bool;

    auto HTStitchWrite(const int32_t& timeIndex, const AcquisitionSequenceInfo& acquisitionSequenceInfo)->bool;
    auto FLStitchWrite(const int32_t& flChannelIndex, const int32_t& timeIndex, const AcquisitionSequenceInfo& acquisitionSequenceInfo)->bool;
    auto BFStitchWrite(const int32_t& timeIndex, const AcquisitionSequenceInfo& acquisitionSequenceInfo)->bool;

    auto HTLDMConvert(const int32_t& timeIndex)->bool;
    auto FLLDMConvert(const int32_t& flChannelIndex, const int32_t& timeIndex)->bool;
    auto BFLDMConvert(const int32_t& timeIndex)->bool;

    auto TCFWrite(const AcquisitionSequenceInfo& acquisitionSequenceInfo)->bool;

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo)->void;
    auto SetPsfFolderPath(const QString& psfFolderPath)->void;
    auto SetSystemBackgroundFolderPath(const QString& systemBackgroundFolderPath)->void;

    auto SetMatlabModuleFolderPath(const QString& matlabModuleFolderPath)->void;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
