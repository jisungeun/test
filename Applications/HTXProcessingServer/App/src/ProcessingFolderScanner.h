#pragma once
#include <QList>
#include "SampleObject.h"

class ProcessingFolderScanner {
public:
    static auto Scan(const QString& topFolderPath, const QString& systemBackgroundImageFolderPath)->bool;
};