#define LOGGER_TAG "[LDMConverter]"

#include "LDMConverter.h"

#include "HDF5Mutex.h"
#include "TcfLdmConverter.h"

#include "TCLogger.h"

class LDMConverter::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString tcfFilePath{};
    QString outputTCFFilePath{};
    AcquisitionConfig config{};

    auto CalculateMaximumTileSize(const int32_t& number)->int32_t;
};

auto LDMConverter::Impl::CalculateMaximumTileSize(const int32_t& number) -> int32_t {
    const auto power = std::floor(std::log2(static_cast<float>(number)));
    const auto maximumTileSize = static_cast<int32_t>(std::pow(2, power));

    return maximumTileSize;
}

LDMConverter::LDMConverter() : d(new Impl()) {
}

LDMConverter::~LDMConverter() = default;

auto LDMConverter::SetTCFFilePath(const QString& tcfFilePath) -> void {
    d->tcfFilePath = tcfFilePath;
}

auto LDMConverter::SetOutputTCFFilePath(const QString& outputTCFFilePath) -> void {
    d->outputTCFFilePath = outputTCFFilePath;
}

auto LDMConverter::SetAcquisitionConfig(const AcquisitionConfig& config) -> void {
    d->config = config;
}

auto LDMConverter::Convert() -> bool {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    TC::IO::LdmConverting::TcfLdmConverter tcfLdmConverter;
    tcfLdmConverter.SetInOutTcfPaths(d->tcfFilePath, d->outputTCFFilePath);

    const auto ht3DExists = d->config.GetAcquisitionCount().ht3D > 0;
    const auto fl3DExists = (d->config.GetAcquisitionCount().fl3DCH0 + d->config.GetAcquisitionCount().fl3DCH1 
        + d->config.GetAcquisitionCount().fl3DCH2) > 0;

    QLOG_INFO() << "LDM Convert Input : " << d->tcfFilePath;
    QLOG_INFO() << "LDM Convert Output : " << d->outputTCFFilePath;
    
    if (ht3DExists) {
        //const auto maximumZTileSize = d->CalculateMaximumTileSize(d->config.GetAcquisitionCount().ht3DZ);
        constexpr auto htTileSize = 64;
        tcfLdmConverter.SetHtTileSize(htTileSize, htTileSize, htTileSize);
    }

    if (fl3DExists) {
        //const auto maximumZTileSize = d->CalculateMaximumTileSize(d->config.GetAcquisitionCount().fl3DZ);
        constexpr auto flTileSize = 64;
        tcfLdmConverter.SetFlTileSize(flTileSize, flTileSize, flTileSize);
    }

    tcfLdmConverter.Write();

    return true;
}
