#include "ProcessTask.h"

class ProcessTask::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    QString rootFolderPath{};
    ProcessType processType{ ProcessType::None };
    ModalityType modalityType{ ModalityType::Name::None };
    int32_t timeFrameIndex{};
    int32_t tileIndex{};
};

ProcessTask::ProcessTask() : d(new Impl()) {
}

ProcessTask::ProcessTask(const ProcessTask& other) : d(new Impl(*other.d)) {
}

ProcessTask::~ProcessTask() = default;

auto ProcessTask::operator=(const ProcessTask& other) -> ProcessTask& {
    *(this->d) = *(other.d);
    return *this;
}

auto ProcessTask::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto ProcessTask::SetProcessType(const ProcessType& processType) -> void {
    d->processType = processType;
}

auto ProcessTask::SetModalityType(const ModalityType& modalityType) -> void {
    d->modalityType = modalityType;
}

auto ProcessTask::SetTimeFrameIndex(const int32_t& timeFrameIndex) -> void {
    d->timeFrameIndex = timeFrameIndex;
}

auto ProcessTask::SetTileIndex(const int32_t& tileIndex) -> void {
    d->tileIndex = tileIndex;
}

auto ProcessTask::GetRootFolderPath() const -> const QString& {
    return d->rootFolderPath;
}

auto ProcessTask::GetProcessType() const -> const ProcessType& {
    return d->processType;
}

auto ProcessTask::GetModalityType() const -> const ModalityType& {
    return d->modalityType;
}

auto ProcessTask::GetTimeFrameIndex() const -> const int32_t& {
    return d->timeFrameIndex;
}

auto ProcessTask::GetTileIndex() const -> const int32_t& {
    return d->tileIndex;
}
