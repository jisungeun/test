#define LOGGER_TAG "[StitcherHTStaged]"

#include "StitcherHTStaged.h"

#include <QFile>
#include <QStringList>

#include <TCLogger.h>

#include <StitchingWriterHDF5HT.h>
#include <TilePositionCalculatorStaged.h>

#include <H5Cpp.h>
#include <HDF5Mutex.h>

#include "ResultFilePathGenerator.h"
#include "TileConfigurationReaderHT.h"
#include "TileSetGeneratorHT.h"

class StitcherHTStaged::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    int32_t tileNumberX{};
    int32_t tileNumberY{};

    double limitedSizeXInMicrometer{};
    double limitedSizeYInMicrometer{};

    bool sizeLimited{false};

    auto GetPositionFilePathList(const int32_t& tileNumber)->QStringList;
    auto GetProcessedDataFilePathList(const int32_t& tileNumber, const int32_t& timeFrameIndex)->QStringList;
    auto MergeToMap(const QStringList& processedDataFilePathList, const QStringList& positionFilePathList)->QMap<QString, QString>;
    auto ReadVoxelSizeXY(const QString& processedDataFilePath)->double;
};

auto StitcherHTStaged::Impl::GetPositionFilePathList(const int32_t& tileNumber) -> QStringList {
    QStringList positionFilePathList{};

    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        auto positionFilePath = QString("%1/data/%2/position.ini").arg(this->rootFolderPath).arg(tileIndex, 4, 10, QChar('0'));
        if (!QFile::exists(positionFilePath)) {
            positionFilePath = QString("%1/data/%2/position.ini").arg(this->rootFolderPath).arg(tileIndex);
        }
        positionFilePathList.push_back(positionFilePath);
    }

    return positionFilePathList;
}

auto StitcherHTStaged::Impl::GetProcessedDataFilePathList(const int32_t& tileNumber,
    const int32_t& timeFrameIndex) -> QStringList {
    QStringList processedDataFilePathList{};
    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        const QString processedDataFilePath =
            ResultFilePathGenerator::GetHTProcessedFilePath(this->rootFolderPath, tileIndex, timeFrameIndex);
        processedDataFilePathList.push_back(processedDataFilePath);
    }

    return processedDataFilePathList;
}

auto StitcherHTStaged::Impl::MergeToMap(const QStringList& processedDataFilePathList,
    const QStringList& positionFilePathList) -> QMap<QString, QString> {
    const auto numberOfData = processedDataFilePathList.size();

    QMap<QString, QString> dataPositionFilePathMap{};
    for (auto index = 0; index < numberOfData; ++index) {
        dataPositionFilePathMap[processedDataFilePathList.at(index)] = positionFilePathList.at(index);
    }

    return dataPositionFilePathMap;
}

auto StitcherHTStaged::Impl::ReadVoxelSizeXY(const QString& processedDataFilePath) -> double {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    float pixelWorldSizeX{};

    const H5::H5File file(processedDataFilePath.toStdString(), H5F_ACC_RDONLY);
    const auto dataSet = file.openDataSet("Data");
    const auto attrPixelWorldSizeX = dataSet.openAttribute("pixelWorldSizeX");
    attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);

    return pixelWorldSizeX;
}

StitcherHTStaged::StitcherHTStaged() : d{ std::make_unique<Impl>() } {
}

StitcherHTStaged::~StitcherHTStaged() {
}

auto StitcherHTStaged::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto StitcherHTStaged::SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY) -> void {
    d->tileNumberX = tileNumberX;
    d->tileNumberY = tileNumberY;
}

auto StitcherHTStaged::SetLimitSize(const double& sizeXInMicrometer, const double& sizeYInMicrometer) -> void {
    d->limitedSizeXInMicrometer = sizeXInMicrometer;
    d->limitedSizeYInMicrometer = sizeYInMicrometer;

    d->sizeLimited = true;
}

auto StitcherHTStaged::Stitch(const int32_t& timeIndex) -> bool {
    const auto tileNumber = d->tileNumberX * d->tileNumberY;
    const auto positionFilePathList = d->GetPositionFilePathList(tileNumber);

    QLOG_INFO() << "HT Staged Stitcher time(" << timeIndex << ")";

    const auto processedDataFilePathList = d->GetProcessedDataFilePathList(tileNumber, timeIndex);

    TileConfigurationReaderHT tileConfigurationReaderHT;
    tileConfigurationReaderHT.SetPositionFileList(positionFilePathList);
    tileConfigurationReaderHT.SetProcessedFilePathList(processedDataFilePathList);

    if (!tileConfigurationReaderHT.Read()) { return false; }

    const auto tileConfiguration = tileConfigurationReaderHT.GetTileConfiguration();

    const auto dataPositionFilePathMap = d->MergeToMap(processedDataFilePathList, positionFilePathList);

    TileSetGeneratorHT tileSetGeneratorHT;
    tileSetGeneratorHT.SetTileConfiguration(tileConfiguration);
    tileSetGeneratorHT.SetDataPositionFilePathMap(dataPositionFilePathMap);
    tileSetGeneratorHT.Generate();

    const auto tileSet = tileSetGeneratorHT.GetTileSet();

    TilePositionCalculatorStaged tilePositionCalculatorStaged;
    tilePositionCalculatorStaged.SetTileConfiguration(tileConfiguration);
    const auto tilePositionSet = tilePositionCalculatorStaged.Calculate();

    const auto stitchingResultFilePath = ResultFilePathGenerator::GetHTStitchingFilePath(d->rootFolderPath, timeIndex);

    StitchingWriterHDF5HT stitchingWriterHdf5HT;
    stitchingWriterHdf5HT.SetTileSet(tileSet);
    stitchingWriterHdf5HT.SetTilePositionSet(tilePositionSet);
    stitchingWriterHdf5HT.SetTileConfiguration(tileConfiguration);
    stitchingWriterHdf5HT.SetHDF5FilePath(stitchingResultFilePath);
    stitchingWriterHdf5HT.SetBoundaryCropFlag(true);

    if (d->sizeLimited == true) {
        const QString htProcessedDataFilePath = ResultFilePathGenerator::GetHTProcessedFilePath(d->rootFolderPath, 0, timeIndex);
        const auto voxelSize = d->ReadVoxelSizeXY(htProcessedDataFilePath);

        const auto limitedSizeX = static_cast<int32_t>(std::round(d->limitedSizeXInMicrometer / voxelSize));
        const auto limitedSizeY = static_cast<int32_t>(std::round(d->limitedSizeYInMicrometer / voxelSize));

        stitchingWriterHdf5HT.SetDataSizeLimit(limitedSizeX, limitedSizeY);
    }
    if (!stitchingWriterHdf5HT.Run()) { return false; }

    return true;
}
