#pragma once
#include <QString>
#include <QStringList>

class RootDirectoryFinder {
public:
    static auto GetRootDirectoryList(const QString& topPath) ->QStringList;
private:
    static auto FindInDirectory(const QString& directoryPath)->QStringList;
    static auto IsRootDirectory(const QString& directoryPath)->bool;
};
