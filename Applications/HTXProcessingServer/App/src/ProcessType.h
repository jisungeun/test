#pragma once

#include <enum.h>

BETTER_ENUM(ProcessType, int32_t, None, TileProcessing, Stitching, ThumbnailWriting, StitchingWriting,
    LDMConversion, TCFWriting, DataRemoving);
