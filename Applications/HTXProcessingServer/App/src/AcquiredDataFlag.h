#pragma once

#include <memory>

#include "AcquisitionCount.h"
#include "ModalityType.h"

class AcquiredDataFlag {
public:
    AcquiredDataFlag();
    AcquiredDataFlag(const AcquiredDataFlag& other);
    ~AcquiredDataFlag();

    auto operator=(const AcquiredDataFlag& other)->AcquiredDataFlag&;

    auto Initialize(const AcquisitionCount& acquisitionCount)->void;

    auto SetBGAcquiredFlag(const bool& flag)->void;
    auto GetBGAcquiredFlag()const ->bool;

    auto SetAcquiredFlag(const int32_t& timeIndex, const int32_t& tileIndex, const ModalityType& modalityType, 
        const bool& flag)->void;
    auto GetAcquiredFlag(const int32_t& timeIndex, const int32_t& tileIndex, const ModalityType& modalityType)const
        ->bool;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
