#define LOGGER_TAG "[StitchingWriterFL]"

#include "StitchingWriterFL.h"

#include <QDateTime>
#include <QDir>

#include "arrayfire.h"
#include "ElapsedTimeReader.h"
#include "FLDataSetGetterStitchingResult.h"
#include "H5Cpp.h"

#include "HDF5Mutex.h"
#include "PiercedRectangleMerger.h"
#include "RecordingTimeReader.h"
#include "ResultFilePathGenerator.h"
#include "TCLogger.h"
#include "SIUnit.h"
#include "TCFDataSetRecordedPosition.h"

#include "TCFFLSectionDataSetWriter.h"

using namespace TC::TCFWriter;

struct WritingStartIndex {
    int32_t x{};
    int32_t y{};
    int32_t z{};
};

struct CenterPosition {
    int32_t x{};
    int32_t y{};
};

auto ReadFLDataSet(const QString& processedDataFilePath, const QString& stitchingDataFilePath)->TCFFLDataSet {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    float pixelWorldSizeX{}, pixelWorldSizeY{}, pixelWorldSizeZ{};

    const H5::H5File processedDataFile(processedDataFilePath.toStdString(), H5F_ACC_RDONLY);
    const auto processedDataSet = processedDataFile.openDataSet("Data");

    const auto attrPixelWorldSizeX = processedDataSet.openAttribute("pixelWorldSizeX");
    const auto attrPixelWorldSizeY = processedDataSet.openAttribute("pixelWorldSizeY");
    const auto attrPixelWorldSizeZ = processedDataSet.openAttribute("pixelWorldSizeZ");

    attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
    attrPixelWorldSizeY.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeY);
    attrPixelWorldSizeZ.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeZ);

    const H5::H5File stitchingDataFile(stitchingDataFilePath.toStdString(), H5F_ACC_RDONLY);
    const auto stitchingDataSet3D = stitchingDataFile.openDataSet("StitchingData3D");
    const auto stitchingDataSetMIP = stitchingDataFile.openDataSet("StitchingDataMIP");

    int32_t stitchingDataSizeX{}, stitchingDataSizeY{}, stitchingDataSizeZ{};
    const auto attrDataSizeX = stitchingDataSet3D.openAttribute("dataSizeX");
    const auto attrDataSizeY = stitchingDataSet3D.openAttribute("dataSizeY");
    const auto attrDataSizeZ = stitchingDataSet3D.openAttribute("dataSizeZ");

    attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
    attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);
    attrDataSizeZ.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeZ);

    float data3dMinValue{}, data3dMaxValue{}, dataMIPMinValue{}, dataMIPMaxValue{};
    const auto attrData3DMinValue = stitchingDataSet3D.openAttribute("minValue");
    const auto attrData3DMaxValue = stitchingDataSet3D.openAttribute("maxValue");
    const auto attrDataMIPMinValue = stitchingDataSetMIP.openAttribute("minValue");
    const auto attrDataMIPMaxValue = stitchingDataSetMIP.openAttribute("maxValue");

    attrData3DMinValue.read(H5::PredType::NATIVE_FLOAT, &data3dMinValue);
    attrData3DMaxValue.read(H5::PredType::NATIVE_FLOAT, &data3dMaxValue);
    attrDataMIPMinValue.read(H5::PredType::NATIVE_FLOAT, &dataMIPMinValue);
    attrDataMIPMaxValue.read(H5::PredType::NATIVE_FLOAT, &dataMIPMaxValue);

    const IFLDataSetGetter::Pointer flDataSetGetter{ new FLDataSetGetterStitchingResult };
    std::dynamic_pointer_cast<FLDataSetGetterStitchingResult>(flDataSetGetter)->SetResultFilePath(stitchingDataFilePath);

    TCFFLDataSetMetaInfo metaInfo;
    metaInfo.SetDataSize(stitchingDataSizeX, stitchingDataSizeY, stitchingDataSizeZ);
    metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);
    metaInfo.SetDataMinMaxValue(data3dMinValue, data3dMaxValue);
    metaInfo.SetMIPDataMinMaxValue(dataMIPMinValue, dataMIPMaxValue);

    TCFFLDataSet tcfFLDataSet;
    tcfFLDataSet.SetDataSetGetter(flDataSetGetter);
    tcfFLDataSet.SetMetaInfo(metaInfo);

    return tcfFLDataSet;
}

auto ReadFLCenterPosition(const QString& stitchingResultFilePath)->CenterPosition {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);

    int32_t centerPositionX{}, centerPositionY{};

    const auto attCenterPositionX = stitchingDataFile.openAttribute("centerPositionX");
    const auto attCenterPositionY = stitchingDataFile.openAttribute("centerPositionY");

    attCenterPositionX.read(H5::PredType::NATIVE_INT32, &centerPositionX);
    attCenterPositionY.read(H5::PredType::NATIVE_INT32, &centerPositionY);

    return { centerPositionX , centerPositionY };
}

class StitchingWriterFL::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    QString tempTCFFilePath{};

    TCFFLDataSet firstFLDataSet{};
    AcquisitionConfig acquisitionConfig{};
    AcquisitionSequenceInfo acquisitionSequenceInfo{};

    PiercedRectangle stitchingPiercedRect{};

    auto CalculateStitchingPiercedRect(const int32_t& channelIndex)->PiercedRectangle;
    auto CalculateWritingStartIndex(const int32_t& mergedCenterPositionX, const int32_t& mergedCenterPositionY,
        const int32_t& centerPositionX, const int32_t& centerPositionY)->WritingStartIndex;
};

auto StitchingWriterFL::Impl::CalculateStitchingPiercedRect(const int32_t& channelIndex)-> PiercedRectangle {
    QList<PiercedRectangle> stitchingRectList;

    const auto timeFrameCount = this->acquisitionSequenceInfo.GetTimeFrameCount();

    const auto flSequenceModality = ConvertSequenceModality(channelIndex);
    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(flSequenceModality, timeIndex)) {
            continue;
        }

        const auto stitchingResultFilePath = 
            ResultFilePathGenerator::GetFLStitchingFilePath(rootFolderPath, channelIndex, timeIndex);

        const auto [centerPositionX, centerPositionY] = ReadFLCenterPosition(stitchingResultFilePath);

        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto stitchingDataSet3D = stitchingDataFile.openDataSet("StitchingData3D");

        int32_t stitchingDataSizeX{}, stitchingDataSizeY{};
        const auto attrDataSizeX = stitchingDataSet3D.openAttribute("dataSizeX");
        const auto attrDataSizeY = stitchingDataSet3D.openAttribute("dataSizeY");

        attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
        attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);

        PiercedRectangle piercedRectangle;
        piercedRectangle.SetSize(stitchingDataSizeX, stitchingDataSizeY);
        piercedRectangle.SetPiercingIndex(centerPositionX, centerPositionY);

        stitchingRectList.push_back(piercedRectangle);
    }

    PiercedRectangleMerger piercedRectangleMerger;
    piercedRectangleMerger.SetPiercedRectangleList(stitchingRectList);
    piercedRectangleMerger.Merge();

    return piercedRectangleMerger.GetMergedPiercedRectangle();
}

auto StitchingWriterFL::Impl::CalculateWritingStartIndex(const int32_t& mergedCenterPositionX,
    const int32_t& mergedCenterPositionY, const int32_t& centerPositionX,
    const int32_t& centerPositionY) -> WritingStartIndex {
    const auto writingStartIndexX = centerPositionX - mergedCenterPositionX;
    const auto writingStartIndexY = centerPositionY - mergedCenterPositionY;

    return WritingStartIndex{ writingStartIndexX, writingStartIndexY , 0 };
}

StitchingWriterFL::StitchingWriterFL() : d(new Impl()) {
}

StitchingWriterFL::~StitchingWriterFL() = default;

auto StitchingWriterFL::SetRootFolderPath(const QString & rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto StitchingWriterFL::SetTempTCFFilePath(const QString & tempTCFFilePath) -> void {
    d->tempTCFFilePath = tempTCFFilePath;
}

auto StitchingWriterFL::SetAcquisitionConfig(const AcquisitionConfig& acquisitionConfig) -> void {
    d->acquisitionConfig = acquisitionConfig;
}

auto StitchingWriterFL::SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo) -> void {
    d->acquisitionSequenceInfo = acquisitionSequenceInfo;
}

auto StitchingWriterFL::Write(const int32_t& timeIndex, const int32_t& channelIndex) -> bool {
    const auto& acquisitionPosition = d->acquisitionConfig.GetAcquisitionPosition();

    const auto positionX = acquisitionPosition.positionXMillimeter;
    const auto positionY = acquisitionPosition.positionYMillimeter;
    const auto positionZ = acquisitionPosition.positionZMillimeter;
    const auto positionC = acquisitionPosition.positionCMillimeter;
    constexpr auto positionUnit = LengthUnit::Millimenter;

    TCFDataSetRecordedPosition position;
    position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);

    RecordingTimeReader recordingTimeReader;
    ElapsedTimeReader elapsedTimeReader;

    const auto stitchingPiercedRect = d->CalculateStitchingPiercedRect(channelIndex);

    QLOG_INFO() << "stitchingWriter FL time(" << timeIndex << ")" << "Channel (" << channelIndex << ")";

    const auto flProcessedDataFilePath = 
        ResultFilePathGenerator::GetFLProcessedFilePath(d->rootFolderPath, channelIndex, 0, timeIndex);
    const auto stitchingResultFilePath = 
        ResultFilePathGenerator::GetFLStitchingFilePath(d->rootFolderPath, channelIndex, timeIndex);

    const QString recordedTimeFilePath = QString("%1/data/0000/FL3D/CH%2/%3/acquisition_timestamp.txt")
        .arg(d->rootFolderPath).arg(channelIndex).arg(timeIndex, 4, 10, QChar('0'));
    const QString elapsedTimeFilePath = QString("%1/data/0000/FL3D/CH%2/%3/logical_timestamp.txt")
        .arg(d->rootFolderPath).arg(channelIndex).arg(timeIndex, 4, 10, QChar('0'));

    recordingTimeReader.SetFilePath(recordedTimeFilePath);
    const auto recordingTimeReadResult = recordingTimeReader.Read();

    elapsedTimeReader.SetFilePath(elapsedTimeFilePath);
    const auto elapsedTimeReadResult = elapsedTimeReader.Read();

    QDateTime recordedTime;
    if (recordingTimeReadResult == true) {
        recordedTime = recordingTimeReader.GetRecordingTime();
    } else {
        recordedTime = QDateTime::currentDateTime();
    }

    double elapsedTime;
    constexpr auto elapsedTimeUnit = TimeUnit::Second;
    if (elapsedTimeReadResult == true) {
        elapsedTime = elapsedTimeReader.GetElapsedTime(elapsedTimeUnit);
    } else {
        elapsedTime = 0;
    }

    const auto sequenceModality = ConvertSequenceModality(channelIndex);

    const auto tcfDataIndex = d->acquisitionSequenceInfo.GetOrderIndex(sequenceModality, timeIndex);

    auto tcfFLDataSet = ReadFLDataSet(flProcessedDataFilePath, stitchingResultFilePath);

    auto metaInfo = tcfFLDataSet.GetMetaInfo();
    metaInfo.SetRecordedTime(recordedTime);
    metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
    metaInfo.SetTimeFrameIndex(tcfDataIndex);
    metaInfo.SetPosition(position);
    metaInfo.SetChannelIndex(channelIndex);

    tcfFLDataSet.SetMetaInfo(metaInfo);

    const auto [centerPositionX, centerPositionY] = ReadFLCenterPosition(stitchingResultFilePath);
    const auto [writingStartIndexX, writingStartIndexY, writingStartIndexZ] =
        d->CalculateWritingStartIndex(stitchingPiercedRect.GetPiercingIndexX(), stitchingPiercedRect.GetPiercingIndexY(),
            centerPositionX, centerPositionY);

    TCFFLSectionDataSetWriter tcfFLDataSetWriter;
    tcfFLDataSetWriter.SetTargetFilePath(d->tempTCFFilePath);
    tcfFLDataSetWriter.SetTCFFLDataSet(tcfFLDataSet);
    tcfFLDataSetWriter.SetWholeDataSize(stitchingPiercedRect.GetSizeX(), stitchingPiercedRect.GetSizeY(), metaInfo.GetSizeZ());
    tcfFLDataSetWriter.SetWritingStartIndex(writingStartIndexX, writingStartIndexY, writingStartIndexZ);

    if (!tcfFLDataSetWriter.Write()) {
        QLOG_ERROR() << "tcfFLDataSetWriter.Write() fails";
        return false;
    }
    return true;
}
