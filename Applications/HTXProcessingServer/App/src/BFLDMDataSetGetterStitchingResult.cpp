#include "BFLDMDataSetGetterStitchingResult.h"

#include "Hdf5DataSetHybridSamplerReader.h"
#include "StorageOnePickSampler.h"

using namespace TC::IO::LdmCore;
using namespace TC::IO::LdmReading;
using namespace TC::IO::LdmWriting;

class BFLDMDataSetGetterStitchingResult::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    H5::DataSet* srcDataSet{ nullptr };
    H5::Group* destGroup{ nullptr };

    int32_t readingOffsetX{};
    int32_t readingOffsetY{};

    LdmConfiguration ldmConfiguration;

    int32_t channelCount{};
};

BFLDMDataSetGetterStitchingResult::BFLDMDataSetGetterStitchingResult() : d{ std::make_unique<Impl>() } {
}

BFLDMDataSetGetterStitchingResult::~BFLDMDataSetGetterStitchingResult() = default;

auto BFLDMDataSetGetterStitchingResult::SetSourceDataSet(H5::DataSet* dataSet) -> void {
    d->srcDataSet = dataSet;
}

auto BFLDMDataSetGetterStitchingResult::SetDestLDMGroup(H5::Group* group) -> void {
    d->destGroup = group;
}

auto BFLDMDataSetGetterStitchingResult::SetReadingOffset(const int32_t& offsetX, const int32_t& offsetY) -> void {
    d->readingOffsetX = offsetX;
    d->readingOffsetY = offsetY;
}

auto BFLDMDataSetGetterStitchingResult::SetLdmConfiguration(const LdmConfiguration& ldmConfiguration) -> void {
    d->ldmConfiguration = ldmConfiguration;
}

auto BFLDMDataSetGetterStitchingResult::GetTileData(const int32_t& tileIndex) -> std::shared_ptr<uint8_t[]> {
    auto samplerReader = std::make_shared<Hdf5DataSetHybridSamplerReader>(*d->srcDataSet, *d->destGroup);
    samplerReader->SetOffsetPoint({ d->readingOffsetX, d->readingOffsetY, 0 });

    auto storageOnePickSampler = std::make_shared<StorageOnePickSampler>();
    storageOnePickSampler->SetSamplerReader(samplerReader);

    const auto ldmSampledMemoryChunk = storageOnePickSampler->Sample(d->ldmConfiguration, tileIndex);
    const auto numberOfElements = ldmSampledMemoryChunk->GetDimension().GetNumberOfElements();

    std::shared_ptr<uint8_t[]> bfTileData{ new uint8_t[numberOfElements] };
    std::copy_n(ldmSampledMemoryChunk->GetDataPointerAs<uint8_t>(), numberOfElements, bfTileData.get());

    return bfTileData;
}

auto BFLDMDataSetGetterStitchingResult::SetChannelCount(const int32_t& channelCount) -> void {
    d->channelCount = channelCount;
}

auto BFLDMDataSetGetterStitchingResult::GetChannelCount() -> int32_t {
    return d->channelCount;
}
