#define LOGGER_TAG "[StitcherBF]"

#include "StitcherBF.h"

#include <QFile>

#include "H5Cpp.h"
#include "HDF5Mutex.h"
#include "HTVoxelSizeReader.h"
#include "OverlapRelationCalculatorPhaseCorrelation.h"
#include "OverlapRelationSet.h"
#include "ResultFilePathGenerator.h"
#include "StitchingWriterHDF5BF.h"
#include "TCLogger.h"
#include "TileConfigurationReaderBF.h"
#include "TilePositionCalculatorBestOverlap.h"
#include "TilePositionSetReader.h"
#include "TileSetGeneratorBF.h"

auto CalculateBFTilePositionMap(const TilePositionSet& htTilePositionSet, const int32_t& tileNumberX,
    const int32_t& tileNumberY, const double& htResolutionXY, const double& bfResolutionXY) ->TilePositionSet {
    const auto htToBFRatio = htResolutionXY / bfResolutionXY;

    TilePositionSet bfTilePositionSet;
    bfTilePositionSet.SetTileNumber(tileNumberX, tileNumberY);
    for (auto tileIndexX = 0; tileIndexX < tileNumberX; ++tileIndexX) {
        for (auto tileIndexY = 0; tileIndexY < tileNumberY; ++tileIndexY) {
            const auto tilePosition = htTilePositionSet.GetTilePosition(tileIndexX, tileIndexY);

            const auto calculatedTilePositionX = std::round(tilePosition.GetTilePositionX() * htToBFRatio);
            const auto calculatedTilePositionY = std::round(tilePosition.GetTilePositionY() * htToBFRatio);
            const auto calculatedTilePositionZ = std::round(tilePosition.GetTilePositionZ() * 1);

            TilePosition calculatedTilePosition;
            calculatedTilePosition.SetPositions(calculatedTilePositionX, calculatedTilePositionY, calculatedTilePositionZ);

            bfTilePositionSet.InsertTilePosition(tileIndexX, tileIndexY, calculatedTilePosition);
        }
    }

    return bfTilePositionSet;
}

class StitcherBF::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    int32_t tileNumberX{};
    int32_t tileNumberY{};

    double limitedSizeXInMicrometer{};
    double limitedSizeYInMicrometer{};
    bool sizeLimited{ false };

    auto GetProcessedDataFilePathList(const int32_t& tileNumber, const int32_t& timeFrameIndex)->QStringList;
    auto GetPositionFilePathList(const int32_t& tileNumber)->QStringList;
    auto MergeToMap(const QStringList& processedDataFilePathList, const QStringList& positionFilePathList)->QMap<QString, QString>;
    auto FilterOverlapRelationSet(const OverlapRelationSet& overlapRelationSet, const TileConfiguration& tileConfiguration, const float& voxelSize)->OverlapRelationSet;
    auto ReadVoxelSizeXY(const QString& processedDataFilePath)->double;
};

auto StitcherBF::Impl::GetProcessedDataFilePathList(const int32_t& tileNumber, const int32_t& timeFrameIndex)
    -> QStringList {
    QStringList processedDataFilePathList{};
    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        const QString processedDataFilePath =
            ResultFilePathGenerator::GetBFProcessedFilePath(this->rootFolderPath, tileIndex, timeFrameIndex);
        processedDataFilePathList.push_back(processedDataFilePath);
    }

    return processedDataFilePathList;
}

auto StitcherBF::Impl::GetPositionFilePathList(const int32_t& tileNumber) -> QStringList {
    QStringList positionFilePathList{};

    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        auto positionFilePath = QString("%1/data/%2/position.ini").arg(this->rootFolderPath).arg(tileIndex, 4, 10, QChar('0'));
        if (!QFile::exists(positionFilePath)) {
            positionFilePath = QString("%1/data/%2/position.ini").arg(this->rootFolderPath).arg(tileIndex);
        }
        positionFilePathList.push_back(positionFilePath);
    }

    return positionFilePathList;
}

auto StitcherBF::Impl::MergeToMap(const QStringList& processedDataFilePathList,
    const QStringList& positionFilePathList) -> QMap<QString, QString> {
    const auto numberOfData = processedDataFilePathList.size();

    QMap<QString, QString> dataPositionFilePathMap{};
    for (auto index = 0; index < numberOfData; ++index) {
        dataPositionFilePathMap[processedDataFilePathList.at(index)] = positionFilePathList.at(index);
    }

    return dataPositionFilePathMap;
}

auto StitcherBF::Impl::FilterOverlapRelationSet(const OverlapRelationSet& overlapRelationSet,
    const TileConfiguration& tileConfiguration, const float& voxelSize) -> OverlapRelationSet {
    const auto tileNumberX = tileConfiguration.GetTileNumberX();
    const auto tileNumberY = tileConfiguration.GetTileNumberY();

    const auto overlapLengthX = tileConfiguration.GetOverlapLengthXInPixel();
    const auto overlapLengthY = tileConfiguration.GetOverlapLengthYInPixel();

    const auto overlapGridNumberX = 2 * tileNumberX - 1;
    const auto overlapGridNumberY = 2 * tileNumberY - 1;

    constexpr float filteringThresholdCorrelation = 0.7f;
    constexpr float filteringThresholdXShift = 4.f;
    constexpr float filteringThresholdYShift = 4.f;

    OverlapRelationSet filteredOverlapRelationSet;
    filteredOverlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
    for (auto overlapIndexX = 0; overlapIndexX < overlapGridNumberX; ++overlapIndexX) {
        for (auto overlapIndexY = 0; overlapIndexY < overlapGridNumberY; ++overlapIndexY) {
            if ((overlapIndexX + overlapIndexY) % 2 == 0) { continue; }
            const auto overlapRelation = overlapRelationSet.GetOverlapRelation(overlapIndexX, overlapIndexY);

            const auto reliability = overlapRelation.GetReliability();
            const auto shiftX = overlapRelation.GetShiftValueX();
            const auto shiftY = overlapRelation.GetShiftValueY();


            bool tooBigX{ false }, tooBigY{ false };

            //const auto tooWeakReliability = reliability <= 0.2;
            if (overlapLengthX > 0) {
                tooBigX = std::abs(shiftX) * voxelSize >= filteringThresholdXShift;
            }

            if (overlapLengthY > 0) {
                tooBigY = std::abs(shiftY) * voxelSize >= filteringThresholdYShift;
            }

            //const auto filtered = tooWeakReliability || tooBigX || tooBigY;
            const auto filtered = tooBigX || tooBigY;


            OverlapRelation filteredOverlapRelation;
            if (filtered) {
                filteredOverlapRelation.SetReliability(0);
                filteredOverlapRelation.SetShiftValue(0, 0, 0);
            } else {
                filteredOverlapRelation = overlapRelation;
            }

            filteredOverlapRelationSet.InsertOverlapRelation(overlapIndexX, overlapIndexY, filteredOverlapRelation);
        }
    }

    return filteredOverlapRelationSet;

}

auto StitcherBF::Impl::ReadVoxelSizeXY(const QString& processedDataFilePath) -> double {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    float pixelWorldSizeX{};

    const H5::H5File file(processedDataFilePath.toStdString(), H5F_ACC_RDONLY);
    const auto dataSet = file.openDataSet("Data");
    const auto attrPixelWorldSizeX = dataSet.openAttribute("pixelWorldSizeX");
    attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);

    return pixelWorldSizeX;
}

StitcherBF::StitcherBF() : d(new Impl()) {
    
}

StitcherBF::~StitcherBF() = default;

auto StitcherBF::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto StitcherBF::SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY) -> void {
    d->tileNumberX = tileNumberX;
    d->tileNumberY = tileNumberY;
}

auto StitcherBF::SetLimitSize(const double& sizeXInMicrometer, const double& sizeYInMicrometer) -> void {
    d->limitedSizeXInMicrometer = sizeXInMicrometer;
    d->limitedSizeYInMicrometer = sizeYInMicrometer;

    d->sizeLimited = true;
}

auto StitcherBF::Stitch(const int32_t& timeIndex) -> bool {
    const auto tileNumber = d->tileNumberX * d->tileNumberY;
    const auto positionFilePathList = d->GetPositionFilePathList(tileNumber);

    const QString writingTempFolderPath = d->rootFolderPath + "/temp/BF";

    QLOG_INFO() << "BF Stitcher time(" << timeIndex << ")";

    const auto processedDataFilePathList = d->GetProcessedDataFilePathList(tileNumber, timeIndex);

    TileConfigurationReaderBF tileConfigurationReaderBF;
    tileConfigurationReaderBF.SetPositionFileList(positionFilePathList);
    tileConfigurationReaderBF.SetProcessedFilePathList(processedDataFilePathList);

    tileConfigurationReaderBF.Read();

    const auto tileConfiguration = tileConfigurationReaderBF.GetTileConfiguration();

    const auto dataPositionFilePathMap = d->MergeToMap(processedDataFilePathList, positionFilePathList);

    TileSetGeneratorBF tileSetGeneratorBF;
    tileSetGeneratorBF.SetTileConfiguration(tileConfiguration);
    tileSetGeneratorBF.SetDataPositionFilePathMap(dataPositionFilePathMap);
    tileSetGeneratorBF.Generate();

    const auto tileSet = tileSetGeneratorBF.GetTileSet();

    auto htTilePositionSetExists = true;
    const auto htProcessingFilePath = ResultFilePathGenerator::GetHTProcessedFilePath(d->rootFolderPath, 0, timeIndex);
    HTVoxelSizeReader htVoxelSizeReader;
    if (QFile::exists(htProcessingFilePath)) {
        htVoxelSizeReader.SetProcessedFilePath(htProcessingFilePath);
        const auto voxelSizeReadResult = htVoxelSizeReader.Read();

        htTilePositionSetExists &= voxelSizeReadResult;
    }

    const auto htStitchingFilePath = ResultFilePathGenerator::GetHTStitchingFilePath(d->rootFolderPath, timeIndex);
    TilePositionSetReader tilePositionSetReader;
    if (QFile::exists(htStitchingFilePath)) {
        tilePositionSetReader.SetTargetFilePath(htStitchingFilePath);
        const auto tilePositionSetReadResult = tilePositionSetReader.Read();

        htTilePositionSetExists &= tilePositionSetReadResult;
    }

    const auto bfVoxelSizeXY = d->ReadVoxelSizeXY(processedDataFilePathList.first());

    TilePositionSet tilePositionSet;
    if (htTilePositionSetExists) {
        const auto htTilePositionSet = tilePositionSetReader.GetTilePositionSet();
        const auto htVoxelSizeXY = htVoxelSizeReader.GetVoxelSizeXY();

        tilePositionSet = CalculateBFTilePositionMap(htTilePositionSet, d->tileNumberX, d->tileNumberY, htVoxelSizeXY, bfVoxelSizeXY);
    } else {
        OverlapRelationCalculatorPhaseCorrelation overlapRelationCalculatorPhaseCorrelation;
        overlapRelationCalculatorPhaseCorrelation.SetTileConfiguration(tileConfiguration);
        overlapRelationCalculatorPhaseCorrelation.SetTileSet(tileSet);
        const auto overlapRelationSet = overlapRelationCalculatorPhaseCorrelation.Calculate();

        const auto filteredOverlapRelationSet = d->FilterOverlapRelationSet(overlapRelationSet, tileConfiguration, bfVoxelSizeXY);

        TilePositionCalculatorBestOverlap tilePositionCalculatorBestOverlap;
        tilePositionCalculatorBestOverlap.SetTileConfiguration(tileConfiguration);
        tilePositionCalculatorBestOverlap.SetOverlapRelationSet(filteredOverlapRelationSet);
        tilePositionSet = tilePositionCalculatorBestOverlap.Calculate();
    }


    const auto stitchingResultFilePath = 
        ResultFilePathGenerator::GetBFStitchingFilePath(d->rootFolderPath, timeIndex);

    StitchingWriterHDF5BF stitchingWriterHdf5BF;
    stitchingWriterHdf5BF.SetTileSet(tileSet);
    stitchingWriterHdf5BF.SetTilePositionSet(tilePositionSet);
    stitchingWriterHdf5BF.SetTileConfiguration(tileConfiguration);
    stitchingWriterHdf5BF.SetHDF5FilePath(stitchingResultFilePath);
    stitchingWriterHdf5BF.SetBoundaryCropFlag(true);
    if (d->sizeLimited == true) {
        const auto limitedSizeX = static_cast<int32_t>(std::round(d->limitedSizeXInMicrometer / bfVoxelSizeXY));
        const auto limitedSizeY = static_cast<int32_t>(std::round(d->limitedSizeYInMicrometer / bfVoxelSizeXY));

        stitchingWriterHdf5BF.SetDataSizeLimit(limitedSizeX, limitedSizeY);
    }
    if (!stitchingWriterHdf5BF.Run()) { return false; }
    
    return true;
}
