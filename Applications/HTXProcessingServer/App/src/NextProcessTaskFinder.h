#pragma once

#include <memory>

class ProcessTask;

class NextProcessTaskFinder {
public:
    NextProcessTaskFinder();
    ~NextProcessTaskFinder();

    auto Find()->bool;

    auto NextProcessTaskExists()const->bool;
    auto GetNextProcessTask()const->ProcessTask;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
