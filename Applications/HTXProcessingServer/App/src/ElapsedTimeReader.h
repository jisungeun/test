#pragma once

#include <memory>
#include <QString>

#include "SIUnit.h"

class ElapsedTimeReader {
public:
    ElapsedTimeReader();
    ~ElapsedTimeReader();

    auto SetFilePath(const QString& filePath)->void;
    auto Read()->bool;

    auto GetElapsedTime(const TimeUnit& unit) const ->double;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
