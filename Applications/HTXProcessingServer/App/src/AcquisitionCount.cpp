#include "AcquisitionCount.h"

class AcquisitionCount::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    AcquisitionSequenceInfo acquisitionSequenceInfo{};
    TileNumberX tileNumberX{};
    TileNumberY tileNumberY{};
};

AcquisitionCount::AcquisitionCount() : d(std::make_unique<Impl>()) {
}

AcquisitionCount::AcquisitionCount(const AcquisitionCount& other) : d(std::make_unique<Impl>(*other.d)) {
}

AcquisitionCount::~AcquisitionCount() = default;

auto AcquisitionCount::operator=(const AcquisitionCount& other) -> AcquisitionCount& {
    *(this->d) = *(other.d);
    return *this;
}

auto AcquisitionCount::SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo) -> void {
    d->acquisitionSequenceInfo = acquisitionSequenceInfo;
}

auto AcquisitionCount::GetAcquisitionSequenceInfo() const -> AcquisitionSequenceInfo {
    return d->acquisitionSequenceInfo;
}

auto AcquisitionCount::SetTileNumber(const TileNumberX& tileNumberX, const TileNumberY& tileNumberY) -> void {
    d->tileNumberX = tileNumberX;
    d->tileNumberY = tileNumberY;
}

auto AcquisitionCount::GetTileNumber() const -> std::tuple<TileNumberX, TileNumberY> {
    return { d->tileNumberX, d->tileNumberY };
}

