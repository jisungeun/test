#include "NextProcessTaskFinder.h"

#include "ModalityType.h"
#include "ProcessTask.h"
#include "TaskRepo.h"

class NextProcessTaskFinder::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    bool nextProcessTaskExists{ false };
    ProcessTask nextProcessTask{};

    auto GetExistFLChannelIndexList(const AcquisitionSequenceInfo& acquisitionSequenceInfo)->QList<int32_t>;
    auto GetModalityExistence(const AcquisitionSequenceInfo& acquisitionSequenceInfo)->std::tuple<bool, bool, bool>;
    auto GetModalityDone(const AcquisitionSequenceInfo& acquisitionSequenceInfo, const TaskInfoSet& taskInfoSet)
        ->std::tuple<bool, bool, bool>;
};

auto NextProcessTaskFinder::Impl::GetExistFLChannelIndexList(const AcquisitionSequenceInfo& acquisitionSequenceInfo)
    -> QList<int32_t> {
    const auto timeFrameCount = acquisitionSequenceInfo.GetTimeFrameCount();

    QList<int32_t> flChannelIndexList;
    for (auto flChannelIndex = 0; flChannelIndex < 3; ++flChannelIndex) {
        for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
            const auto flSequenceModality = ConvertSequenceModality(flChannelIndex);
            if (acquisitionSequenceInfo.AcquisitionExists(flSequenceModality, timeIndex)) {
                if (!flChannelIndexList.contains(flChannelIndex)) {
                    flChannelIndexList.push_back(flChannelIndex);
                }
            }
        }
    }

    return flChannelIndexList;
}

auto NextProcessTaskFinder::Impl::GetModalityExistence(const AcquisitionSequenceInfo& acquisitionSequenceInfo)
    -> std::tuple<bool, bool, bool> {
    bool htExists{ false }, flExists{ false }, bfExists{ false };

    const auto timeFrameCount = acquisitionSequenceInfo.GetTimeFrameCount();
    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        if (acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::HT, timeIndex)) {
            htExists = true;
        }

        if (acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::FLCH0, timeIndex)) {
            flExists = true;
        }

        if (acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::FLCH1, timeIndex)) {
            flExists = true;
        }

        if (acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::FLCH2, timeIndex)) {
            flExists = true;
        }

        if (acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::BF, timeIndex)) {
            bfExists = true;
        }

        if (htExists && flExists && bfExists) { break; }
    }

    return { htExists, flExists, bfExists };
}

auto NextProcessTaskFinder::Impl::GetModalityDone(const AcquisitionSequenceInfo& acquisitionSequenceInfo, 
    const TaskInfoSet& taskInfoSet)-> std::tuple<bool, bool, bool> {
    const auto [htExists, flExists, bfExists] = this->GetModalityExistence(acquisitionSequenceInfo);
    const auto timeFrameCount = acquisitionSequenceInfo.GetTimeFrameCount();

    bool htDone{ true }, flDone{ true }, bfDone{ true };

    if (htExists) {
        for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
            if (!acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::HT, timeIndex)) {
                continue;
            }

            const auto taskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion);
            if (taskInfo.GetProcessedProgress() != 1) {
                htDone = false;
                break;
            }
        }
    }
    if (flExists) {
        const auto existFLChannelIndexList = this->GetExistFLChannelIndexList(acquisitionSequenceInfo);

        for (const auto& flChannelIndex : existFLChannelIndexList) {
            for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
                if (!acquisitionSequenceInfo.AcquisitionExists(ConvertSequenceModality(flChannelIndex), timeIndex)) {
                    continue;
                }

                const auto taskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::LDMConversion);
                if (taskInfo.GetProcessedProgress() != 1) {
                    flDone = false;
                    break;
                }
            }

            if (!flDone) {
                break;
            }
        }

    }
    if (bfExists) {
        for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
            if (!acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::BF, timeIndex)) {
                continue;
            }

            const auto taskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::LDMConversion);
            if (taskInfo.GetProcessedProgress() != 1) {
                bfDone = false;
                break;
            }
        }
    }

    return { htDone, flDone, bfDone };
}

NextProcessTaskFinder::NextProcessTaskFinder() : d(new Impl()) {
}

NextProcessTaskFinder::~NextProcessTaskFinder() = default;

auto NextProcessTaskFinder::Find() -> bool {
    d->nextProcessTaskExists = false;

    auto taskRepoInstance = TaskRepo::GetInstance();

    const auto rootFolderPathList = taskRepoInstance->GetRootFolderPathList();
    if (rootFolderPathList.isEmpty()) { return true; }

    ProcessTask nextProcessTask;

    bool taskFound = false;
    for (auto rootIndex = 0; (rootIndex < rootFolderPathList.size()) && (!taskFound); ++rootIndex) {
        const auto& rootFolderPath = rootFolderPathList.at(rootIndex);
        if (taskRepoInstance->IsDone(rootFolderPath) || taskRepoInstance->IsAlreadyDone(rootFolderPath) 
            || taskRepoInstance->IsFailed(rootFolderPath)) {
            continue;
        }
        nextProcessTask.SetRootFolderPath(rootFolderPath);

        const auto acquisitionDataFlag = taskRepoInstance->GetAcquiredDataFlag(rootFolderPath);
        const auto taskInfoSet = taskRepoInstance->GetTaskInfoSet(rootFolderPath);
        const auto acquisitionCount = taskRepoInstance->GetAcquisitionCount(rootFolderPath);

        const auto acquisitionSequenceInfo = acquisitionCount.GetAcquisitionSequenceInfo();

        const auto& timeFrameCount = acquisitionSequenceInfo.GetTimeFrameCount();

        const auto [tileNumberX, tileNumberY] = acquisitionCount.GetTileNumber();
        const auto tileNumber = tileNumberX * tileNumberY;

        const auto [htExists, flExists, bfExists] = d->GetModalityExistence(acquisitionSequenceInfo);

        if (htExists){
            const auto htLastTimeFrameIndex = acquisitionSequenceInfo.GetLastTimeFrameIndex(AcquisitionSequenceInfo::Modality::HT);
            bool stitchingCompleted = false;
            for (auto timeIndex = 0; (timeIndex < timeFrameCount) && (!taskFound); ++timeIndex) {
                if (!acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::HT, timeIndex)) {
                    continue;
                }
                
                bool tileProcessingCompleted = false;
                for (auto tileIndex = 0; (tileIndex < tileNumber) && (!taskFound); ++tileIndex) {

                    if (!acquisitionDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::HT })) {
                        break;
                    }

                    const auto tileTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, tileIndex, 
                        ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing);
                    const auto tileProgress = tileTaskInfo.GetProcessedProgress();

                    if (tileProgress != 1) {
                        taskFound = true;
                        nextProcessTask.SetTimeFrameIndex(timeIndex);
                        nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::HT });
                        nextProcessTask.SetProcessType(ProcessType::TileProcessing);
                        nextProcessTask.SetTileIndex(tileIndex);
                        break;
                    }

                    if (tileIndex == tileNumber - 1) {
                        tileProcessingCompleted = true;
                    }
                }

                if (tileProcessingCompleted) {
                    const auto stitchingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                        ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching);
                    const auto stitchingProgress = stitchingTaskInfo.GetProcessedProgress();

                    if (stitchingProgress != 1) {
                        taskFound = true;
                        nextProcessTask.SetTimeFrameIndex(timeIndex);
                        nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::HT });
                        nextProcessTask.SetProcessType(ProcessType::Stitching);
                        break;
                    }

                    if (timeIndex == htLastTimeFrameIndex) {
                        stitchingCompleted = true;
                    }
                }
            }

            bool stitchingWritingCompleted = false;
            if (stitchingCompleted) {
                for (auto timeIndex = 0; (timeIndex < timeFrameCount) && (!taskFound); ++timeIndex) {
                    if (!acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::HT, timeIndex)) {
                        continue;
                    }

                    const auto stitchingWrittenTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                        ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting);
                    const auto progress = stitchingWrittenTaskInfo.GetProcessedProgress();

                    if (progress != 1) {
                        taskFound = true;
                        nextProcessTask.SetTimeFrameIndex(timeIndex);
                        nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::HT });
                        nextProcessTask.SetProcessType(ProcessType::StitchingWriting);
                        break;
                    }

                    if (timeIndex == htLastTimeFrameIndex) {
                        stitchingWritingCompleted = true;
                    }
                }
            }

            if (stitchingWritingCompleted) {
                //TODO Implement Thumbnail writing;
            }

            if (stitchingWritingCompleted) {
                for (auto timeIndex = 0; (timeIndex < timeFrameCount) && (!taskFound); ++timeIndex) {
                    if (!acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::HT, timeIndex)) {
                        continue;
                    }

                    const auto ldmConversionTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                        ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion);
                    const auto progress = ldmConversionTaskInfo.GetProcessedProgress();

                    if (progress != 1) {
                        taskFound = true;
                        nextProcessTask.SetTimeFrameIndex(timeIndex);
                        nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::HT });
                        nextProcessTask.SetProcessType(ProcessType::LDMConversion);
                        break;
                    }
                }
            }
        }

        if (flExists) {
            const auto existFLChannelIndexList = d->GetExistFLChannelIndexList(acquisitionSequenceInfo);
            for (auto flIndex = 0; (flIndex < existFLChannelIndexList.size()) && (!taskFound); ++flIndex) {
                const auto flChannelIndex = existFLChannelIndexList.at(flIndex);
                const auto flSequenceModality = ConvertSequenceModality(flChannelIndex);
                const auto flLastTimeFrameIndex = acquisitionSequenceInfo.GetLastTimeFrameIndex(flSequenceModality);
                bool stitchingCompleted = false;
                for (auto timeIndex = 0; (timeIndex < timeFrameCount) && (!taskFound); ++timeIndex) {
                    if (!acquisitionSequenceInfo.AcquisitionExists(flSequenceModality, timeIndex)) {
                        continue;
                    }

                    bool tileProcessingCompleted = false;
                    for (auto tileIndex = 0; (tileIndex < tileNumber) && (!taskFound); ++tileIndex) {
                        if (!acquisitionDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, flChannelIndex })) {
                            break;
                        }

                        const auto tileTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, tileIndex,
                            ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::TileProcessing);
                        const auto tileProgress = tileTaskInfo.GetProcessedProgress();

                        if (tileProgress != 1) {
                            taskFound = true;
                            nextProcessTask.SetTimeFrameIndex(timeIndex);
                            nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::FL, flChannelIndex });
                            nextProcessTask.SetProcessType(ProcessType::TileProcessing);
                            nextProcessTask.SetTileIndex(tileIndex);
                            break;
                        }

                        if (tileIndex == tileNumber - 1) {
                            tileProcessingCompleted = true;
                        }
                    }

                    if (tileProcessingCompleted) {
                        const auto stitchingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                            ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::Stitching);

                        const auto stitchingProgress = stitchingTaskInfo.GetProcessedProgress();

                        if (stitchingProgress != 1) {
                            const auto htStitchingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                                ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching);
                            const auto htStitchingProgress = htStitchingTaskInfo.GetProcessedProgress();

                            if ((tileNumber == 1) || (htStitchingProgress == 1)) {
                                taskFound = true;
                                nextProcessTask.SetTimeFrameIndex(timeIndex);
                                nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::FL, flChannelIndex });
                                nextProcessTask.SetProcessType(ProcessType::Stitching);
                                break;
                            }
                        }

                        if (timeIndex == flLastTimeFrameIndex) {
                            stitchingCompleted = true;
                        }
                    }
                }

                bool stitchingWritingCompleted = false;
                if (stitchingCompleted) {
                    for (auto timeIndex = 0; (timeIndex < timeFrameCount) && (!taskFound); ++timeIndex) {
                        if (!acquisitionSequenceInfo.AcquisitionExists(flSequenceModality, timeIndex)) {
                            continue;
                        }

                        const auto stitchingWrittenTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                            ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::StitchingWriting);
                        const auto progress = stitchingWrittenTaskInfo.GetProcessedProgress();

                        if (progress != 1) {
                            taskFound = true;
                            nextProcessTask.SetTimeFrameIndex(timeIndex);
                            nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::FL, flChannelIndex });
                            nextProcessTask.SetProcessType(ProcessType::StitchingWriting);
                            break;
                        }


                        if (timeIndex == flLastTimeFrameIndex) {
                            stitchingWritingCompleted = true;
                        }
                    }
                }

                if (stitchingWritingCompleted) {
                    //TODO Implement Thumbnail writing;
                }

                if (stitchingWritingCompleted) {
                    for (auto timeIndex = 0; (timeIndex < timeFrameCount) && (!taskFound); ++timeIndex) {
                        if (!acquisitionSequenceInfo.AcquisitionExists(flSequenceModality, timeIndex)) {
                            continue;
                        }

                        const auto ldmConversionTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                            ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::LDMConversion);
                        const auto progress = ldmConversionTaskInfo.GetProcessedProgress();

                        if (progress != 1) {
                            taskFound = true;
                            nextProcessTask.SetTimeFrameIndex(timeIndex);
                            nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::FL, flChannelIndex });
                            nextProcessTask.SetProcessType(ProcessType::LDMConversion);
                            break;
                        }
                    }
                }
            }
        }
        if (bfExists) {
            const auto bfLastTimeFrameIndex = acquisitionSequenceInfo.GetLastTimeFrameIndex(AcquisitionSequenceInfo::Modality::BF);
            bool stitchingCompleted = false;
            for (auto timeIndex = 0; (timeIndex < timeFrameCount) && (!taskFound); ++timeIndex) {
                if (!acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::BF, timeIndex)) {
                    continue;
                }

                bool tileProcessingCompleted = false;
                for (auto tileIndex = 0; (tileIndex < tileNumber) && (!taskFound); ++tileIndex) {
                    if (!acquisitionDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::BF })) {
                        break;
                    }

                    const auto tileTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, tileIndex,
                        ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing);
                    const auto tileProgress = tileTaskInfo.GetProcessedProgress();

                    if (tileProgress != 1) {
                        taskFound = true;
                        nextProcessTask.SetTimeFrameIndex(timeIndex);
                        nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::BF });
                        nextProcessTask.SetProcessType(ProcessType::TileProcessing);
                        nextProcessTask.SetTileIndex(tileIndex);
                        break;
                    }

                    if (tileIndex == tileNumber - 1) {
                        tileProcessingCompleted = true;
                    }
                }

                if (tileProcessingCompleted) {
                    const auto stitchingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                        ModalityType{ ModalityType::Name::BF}, ProcessType::Stitching);

                    const auto stitchingProgress = stitchingTaskInfo.GetProcessedProgress();

                    if (stitchingProgress != 1) {
                        const auto htStitchingTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                            ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching);
                        const auto htStitchingProgress = htStitchingTaskInfo.GetProcessedProgress();

                        if ((tileNumber == 1) || (htStitchingProgress == 1)) {
                            taskFound = true;
                            nextProcessTask.SetTimeFrameIndex(timeIndex);
                            nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::BF});
                            nextProcessTask.SetProcessType(ProcessType::Stitching);
                            break;
                        }
                    }

                    if (timeIndex == bfLastTimeFrameIndex) {
                        stitchingCompleted = true;
                    }
                }
            }

            bool stitchingWritingCompleted = false;
            if (stitchingCompleted) {
                for (auto timeIndex = 0; (timeIndex < timeFrameCount) && (!taskFound); ++timeIndex) {
                    if (!acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::BF, timeIndex)) {
                        continue;
                    }

                    const auto stitchingWrittenTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                        ModalityType{ ModalityType::Name::BF }, ProcessType::StitchingWriting);
                    const auto progress = stitchingWrittenTaskInfo.GetProcessedProgress();

                    if (progress != 1) {
                        taskFound = true;
                        nextProcessTask.SetTimeFrameIndex(timeIndex);
                        nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::BF });
                        nextProcessTask.SetProcessType(ProcessType::StitchingWriting);
                        break;
                    }

                    if (timeIndex == bfLastTimeFrameIndex) {
                        stitchingWritingCompleted = true;
                    }
                }
            }

            if (stitchingWritingCompleted) {
                //TODO Implement Thumbnail writing;
            }

            if (stitchingWritingCompleted) {
                for (auto timeIndex = 0; (timeIndex < timeFrameCount) && (!taskFound); ++timeIndex) {
                    if (!acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::BF, timeIndex)) {
                        continue;
                    }

                    const auto ldmConversionTaskInfo = taskInfoSet.GetTaskInfo(timeIndex, 0,
                        ModalityType{ ModalityType::Name::BF }, ProcessType::LDMConversion);
                    const auto progress = ldmConversionTaskInfo.GetProcessedProgress();

                    if (progress != 1) {
                        taskFound = true;
                        nextProcessTask.SetTimeFrameIndex(timeIndex);
                        nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::BF });
                        nextProcessTask.SetProcessType(ProcessType::LDMConversion);
                        break;
                    }
                }
            }
        }

        const auto [htDone, flDone, bfDone] = d->GetModalityDone(acquisitionSequenceInfo, taskInfoSet);
        if (htDone && flDone && bfDone) {
            taskFound = true;
            nextProcessTask.SetTimeFrameIndex(-1);
            nextProcessTask.SetTileIndex(-1);
            nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::None });
            nextProcessTask.SetProcessType(ProcessType::TCFWriting);
        }


        const auto tcfDone = 
            (taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::None }, ProcessType::TCFWriting).GetProcessedProgress() == 1);
        if (tcfDone) {
            taskFound = true;
            nextProcessTask.SetTimeFrameIndex(-1);
            nextProcessTask.SetTileIndex(-1);
            nextProcessTask.SetModalityType(ModalityType{ ModalityType::Name::None });
            nextProcessTask.SetProcessType(ProcessType::DataRemoving);
        }
    }

    if (nextProcessTask.GetProcessType() != +ProcessType::None) {
        d->nextProcessTaskExists = true;
        d->nextProcessTask = nextProcessTask;
    }

    return true;
}

auto NextProcessTaskFinder::NextProcessTaskExists() const -> bool {
    return d->nextProcessTaskExists;
}

auto NextProcessTaskFinder::GetNextProcessTask() const -> ProcessTask {
    return d->nextProcessTask;
}
