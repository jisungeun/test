#define LOGGER_TAG "[ThumbnailWriterHT]"

#include "H5Cpp.h"

#include "ThumbnailWriterHT.h"

#include "HDF5Mutex.h"
#include "ResultFilePathGenerator.h"
#include "ThumbnailHT.h"

#include "TCFThumbnailDataSet.h"
#include "TCFThumbnailWriter.h"
#include "TCLogger.h"

class ThumbnailWriterHT::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    QString tempTCFFilePath{};
    int32_t timeFrameIndex{};
    AcquisitionSequenceInfo acquisitionSequenceInfo{};

    std::shared_ptr<uint8_t[]> thumbnailData{};
    int32_t thumbnailSizeX{};
    int32_t thumbnailSizeY{};

    auto ReadHTVoxelSize()->std::tuple<bool, float, float>;
    auto ReadHTMIPData()->std::tuple<bool, std::shared_ptr<uint16_t[]>, int32_t, int32_t>;
    auto GenerateThumbnailData(const std::shared_ptr<uint16_t[]>& htData, const int32_t& htDataSizeX, 
        const int32_t& htDataSizeY)->std::tuple<bool, std::shared_ptr<uint8_t[]>, int32_t, int32_t>;
};

auto ThumbnailWriterHT::Impl::ReadHTVoxelSize() -> std::tuple<bool, float, float> {
    const QString htProcessedDataFilePath = ResultFilePathGenerator::GetHTProcessedFilePath(this->rootFolderPath, 0, 
        this->timeFrameIndex);
    try {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        float pixelWorldSizeX{}, pixelWorldSizeY{};

        const H5::H5File processedDataFile(htProcessedDataFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto processedDataSet = processedDataFile.openDataSet("Data");

        const auto attrPixelWorldSizeX = processedDataSet.openAttribute("pixelWorldSizeX");
        const auto attrPixelWorldSizeY = processedDataSet.openAttribute("pixelWorldSizeY");

        attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
        attrPixelWorldSizeY.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeY);

        return { true, pixelWorldSizeX, pixelWorldSizeY };
    } catch (const H5::Exception&) {
        return { false, 0,0 };
    }
}

auto ThumbnailWriterHT::Impl::ReadHTMIPData() -> std::tuple<bool, std::shared_ptr<uint16_t[]>, int32_t, int32_t> {
    const auto stitchingFilePath = ResultFilePathGenerator::GetHTStitchingFilePath(this->rootFolderPath, this->timeFrameIndex);

    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    try {
        const H5::H5File stitchingDataFile(stitchingFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto stitchingDataSetMIP = stitchingDataFile.openDataSet("StitchingDataMIP");

        int32_t stitchingDataSizeX{}, stitchingDataSizeY{};
        const auto attrDataSizeX = stitchingDataSetMIP.openAttribute("dataSizeX");
        const auto attrDataSizeY = stitchingDataSetMIP.openAttribute("dataSizeY");

        attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
        attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);

        const auto numberOfElements = static_cast<int64_t>(stitchingDataSizeX) * static_cast<int64_t>(stitchingDataSizeY);

        std::shared_ptr<uint16_t[]> mipData{ new uint16_t[numberOfElements]() };
        stitchingDataSetMIP.read(mipData.get(), H5::PredType::NATIVE_UINT16);

        return { true, mipData, stitchingDataSizeX, stitchingDataSizeY };
    } catch (const H5::Exception&) {
        return { false, nullptr, 0,0 };
    }
}

auto ThumbnailWriterHT::Impl::GenerateThumbnailData(const std::shared_ptr<uint16_t[]>& htData, const int32_t& htDataSizeX, 
    const int32_t& htDataSizeY)-> std::tuple<bool, std::shared_ptr<uint8_t[]>, int32_t, int32_t> {

    const auto numberOfElements = static_cast<uint64_t>(htDataSizeX) * static_cast<uint64_t>(htDataSizeY);
    std::shared_ptr<uint16_t[]> convertedData{ new uint16_t[numberOfElements] };

    for (int64_t xIndex = 0; xIndex < static_cast<int64_t>(htDataSizeX); ++xIndex) {
        for (int64_t yIndex = 0; yIndex < static_cast<int64_t>(htDataSizeY); ++yIndex) {
            const auto srcIndex = xIndex + yIndex * static_cast<int64_t>(htDataSizeX);
            const auto destIndex = yIndex + xIndex * static_cast<int64_t>(htDataSizeY);

            convertedData.get()[destIndex] = htData.get()[srcIndex];
        }
    }

    TC::Processing::ThumbnailGenerator::ThumbnailHT thumbnailHT;
    thumbnailHT.SetDataSize(htDataSizeX, htDataSizeY);
    thumbnailHT.SetHTData(convertedData);
    if (!thumbnailHT.Generate()) { return { false, nullptr, 0,0 }; }

    const auto thumbnailData = thumbnailHT.GetThumbnailData();
    const auto thumbnailSizeX = thumbnailHT.GetThumbnailSizeX();
    const auto thumbnailSizeY = thumbnailHT.GetThumbnailSizeY();

    return { true, thumbnailData, thumbnailSizeX, thumbnailSizeY };
}

ThumbnailWriterHT::ThumbnailWriterHT() : d(new Impl()) {
}

ThumbnailWriterHT::~ThumbnailWriterHT() = default;

auto ThumbnailWriterHT::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto ThumbnailWriterHT::SetTempTCFFilePath(const QString& tempTCFFilePath) -> void {
    d->tempTCFFilePath = tempTCFFilePath;
}

auto ThumbnailWriterHT::SetTimeFrameIndex(const int32_t& timeFrameIndex) -> void {
    d->timeFrameIndex = timeFrameIndex;
}

auto ThumbnailWriterHT::SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo)->void {
    d->acquisitionSequenceInfo = acquisitionSequenceInfo;
}

auto ThumbnailWriterHT::Write() -> bool {
    auto [readingResult, htData, htDataSizeX, htDataSizeY] = d->ReadHTMIPData();
    if (readingResult == false) {
        QLOG_ERROR() << "ReadHTMIPData() fails";
        return false;
    }

    auto [voxelSizeReadingResult, voxelSizeX, voxelSizeY] = d->ReadHTVoxelSize();
    if (voxelSizeReadingResult == false) {
        QLOG_ERROR() << "ReadHTVoxelSize() fails";
        return false;
    }

    auto [thumbnailGeneratingResult, thumbnailData, thumbnailSizeX, thumbnailSizeY] =
        d->GenerateThumbnailData(htData, htDataSizeX, htDataSizeY);

    if (thumbnailGeneratingResult == false) {
        QLOG_ERROR() << "GenerateThumbnailData() fails";
        return false;
    }

    d->thumbnailData = thumbnailData;
    d->thumbnailSizeX = thumbnailSizeX;
    d->thumbnailSizeY = thumbnailSizeY;

    const auto thumbnailVoxelSizeX = voxelSizeX * static_cast<float>(htDataSizeX) / static_cast<float>(thumbnailSizeX);
    const auto thumbnailVoxelSizeY = voxelSizeX * static_cast<float>(htDataSizeY) / static_cast<float>(thumbnailSizeY);

    const auto tcfDataIndex = 
        d->acquisitionSequenceInfo.GetOrderIndex(AcquisitionSequenceInfo::Modality::HT, d->timeFrameIndex);

    TC::IO::TCFWriter::TCFThumbnailDataSet tcfThumbnailDataSet;
    tcfThumbnailDataSet.SetData(thumbnailData);
    tcfThumbnailDataSet.SetDataMemoryOrder(MemoryOrder3D::YXZ);
    tcfThumbnailDataSet.SetDataSize(thumbnailSizeX, thumbnailSizeY);
    tcfThumbnailDataSet.SetColorFlag(false);
    tcfThumbnailDataSet.SetVoxelSize(thumbnailVoxelSizeX, thumbnailVoxelSizeY, LengthUnit::Micrometer);
    tcfThumbnailDataSet.SetTimeFrameIndex(tcfDataIndex);
    
    TC::IO::TCFWriter::TCFThumbnailWriter tcfThumbnailWriter;
    tcfThumbnailWriter.SetModality(TC::IO::TCFWriter::Modality::HT);
    tcfThumbnailWriter.SetTCFThumbnailDataSet(tcfThumbnailDataSet);
    tcfThumbnailWriter.SetTargetFilePath(d->tempTCFFilePath);
    if (!tcfThumbnailWriter.Write()) {
        QLOG_ERROR() << "tcfThumbnailWriter.Write() fails";
        return false;
    }

    return true;
}

auto ThumbnailWriterHT::GetThumbnailData() const -> std::tuple<std::shared_ptr<uint8_t[]>, int32_t, int32_t> {
    return { d->thumbnailData, d->thumbnailSizeX, d->thumbnailSizeY };
}
