#include <QDir>

#include "RootDirectoryFinder.h"

auto RootDirectoryFinder::GetRootDirectoryList(const QString& topPath) -> QStringList {
    return FindInDirectory(topPath);
}

auto RootDirectoryFinder::FindInDirectory(const QString& directoryPath) -> QStringList {
    const QDir directoryDir(directoryPath);
    QStringList rootDirectoryList;

    if (IsRootDirectory(directoryPath)) {
        rootDirectoryList.push_back(directoryPath);
    } else {
        const auto directoryEntry = directoryDir.entryList(QDir::NoDotAndDotDot | QDir::Dirs);

        for (const auto& directoryName : directoryEntry) {
            const auto subDirectoryPath = QString("%1/%2").arg(directoryPath).arg(directoryName);
            const auto subRootDirectoryList = FindInDirectory(subDirectoryPath);
            rootDirectoryList.append(subRootDirectoryList);
        }
    }

    return rootDirectoryList;
}

auto RootDirectoryFinder::IsRootDirectory(const QString& directoryPath) -> bool {
    const QDir directoryDir(directoryPath);

    const auto configFileExists = directoryDir.exists("config.dat");

    return configFileExists;
}
