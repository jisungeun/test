#define LOGGER_TAG "HTXProcessingServer::main"

#include <BugSplat.h>
#include <iostream>

#include <QDir>
#include <QMessageBox>
#include <QStandardPaths>
#include <QString>
#include <QApplication>
#include <QElapsedTimer>
#include <QProcess>
#include <QSettings>
#include <QSharedMemory>

#include <LicenseManager.h>
#include <LicenseDialog.h>
#include <MessageDialog.h>

#include "TCLogger.h"
#include "Version.h"

#include "ProcessingOptionManager.h"

#include "HTXProcessingServerWidget.h"

const std::wstring PRODUCT_DATA = L"MjBFMkRGNjVGQzM0QkUyMEVDRTUzMzEwQzc0NkM0REQ=.zopfgOjNZQguckHR4H8c8mjyi9aypppF6p9KUqQADEjVShHpKd0ZzO3hH6Yea72iEL0dqP9El3u8PVmg8R8MWwZKj1IGGpN0KJF3KMbkYjHGJXLBnRmp1OfUw+pDtjNVZ7om6cZjLsndRAEiO+sBgcCqoQoHG5a0sqpJp4mV9cLFNrZpsqIDDUjK85/KSz/P1TGPazID+lB0wIYeNJTye9edDxg37gZdXLDqYvJkCo8D/GitK5Gv+qNsjsonI/rJ2w2zD1yJFbUWRd5v5MZ24athOe/HKcMznOphTElieEe9jmSuMTNDXWw30Ij9FPgG30TmmDKx1b6kbWqLxIi7NjRLptv9LwZYfyH2bEEOJ0TVQ4IdSiWMhfyxigJSC++VWwsQ6pQVqKiH9YH7RYu3bFvlCIojy1pAApU5RCLRlc1PhrUS5l/Fk3FqHx8DmF34goTeiUnzR9enq04RX4mvZwFbXEfJ95IHv7+rQAdEPlEvD/+QtdFXpsbZgwXwWn49xaxKFAslHvjHa70LAh6/xV4cU/7m8/z4wj8VZunN9fKFCqbGRecEOIHawLPlSNVT5YpyzTo4R7HfYU835j9iUO7ScIZM5CeUI7IuJVFF1u804q1utVeI25blHiMRtQj7DzPBhx8CjQt6pDA921ssTNGZHWeTX4uJ3+HDU6HbIKz/ytOW9IPWYk0u/a/PCJAReJixYys4UUM3sdRt+Zt0dXxoaMDIMGRuvHLdmWxuDyQ=";
const std::string PRODUCT_ID = "17f43a07-dfb6-43db-8897-a27c5120dc50";

std::shared_ptr<MiniDmpSender> miniDmpSender{};

auto CrashCallback(UINT nCode, LPVOID lpVal1, LPVOID lpVal2)->bool;

int main(int argc, char* argv[]) {
    QSharedMemory sharedMemory{ "HTXProcessingServer" };
    if (!sharedMemory.create(512, QSharedMemory::ReadWrite)) { return 0; }

    QProcess htProcessingApp;
    htProcessingApp.start("C:/Windows/System32/taskkill.exe", { "/f","/im","TCHTProcessingMatlabApp.exe" });
    htProcessingApp.waitForFinished();

    QApplication app(argc, argv);

    app.setApplicationName("HTXProcessingServer");
    app.setOrganizationName("Tomocube, Inc.");
    app.setOrganizationDomain("www.tomocube.com");

#ifndef DEBUG
    miniDmpSender = std::make_shared<MiniDmpSender>(L"tomostudio_x_v1",
        L"TomoStudio X",
        PROJECT_REVISION_W,
        nullptr,
        MDSF_USEGUARDMEMORY | MDSF_LOGFILE | MDSF_PREVENTHIJACKING);

    SetGlobalCRTExceptionBehavior();
    SetPerThreadCRTExceptionBehavior();

    miniDmpSender->setGuardByteBufferSize(20 * 1024 * 1024);
    miniDmpSender->setDefaultUserEmail(L"user@youremail.com");
    miniDmpSender->setDefaultUserName(L"User Name");
    miniDmpSender->setDefaultUserDescription(L"Describe the situation in which the SW crash occurred.");
    miniDmpSender->setCallback(CrashCallback);
#endif

    QSettings settings("HKEY_CURRENT_USER\\Software\\TomoCube, Inc.\\HTXProcessingServer\\Strings", QSettings::NativeFormat);
    settings.setValue("0", QString("BC86 3219 4926 1418 105A 66B2 A052 A2C0"));
    settings.setValue("1", QString("8318 6214 EB09 3A1B 0623 A881 0C23 11B8"));
    settings.setValue("2", QString("C280 662D E180 323B BC80 0209 0183 90C8"));
    settings.setValue("3", QString("1D80 1698 4102 1619 085A 61B2 82D2 B2C0"));
    settings.setValue("4", QString("1640 A219 9236 18A8 3291 B2DD D123 C2A1"));
    settings.setValue("5", QString("A823 1B20 52B1 D920 82A1 70B1 C0C1 9918"));
    settings.setValue("6", QString("2301 AB10 9012 E83E 2322 44A3 C125 125E"));
    settings.setValue("7", QString("BA12 90D1 105A 66B2 1131 B123 12A2 9012"));
    settings.setValue("8", QString("C120 5312 3320 A029 01AB 43B2 C087 5420"));
    settings.setValue("9", QString("19B0 B6D2 4E20 1B13 06C2 81DD B238 A289"));

    QFile f(":qdarkstyle/style.qss");

    if (!f.exists()) {
        printf("Unable to set stylesheet, file not found\n");
    } else {
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
    }

    auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
    TC::Logger::Initialize(QString("%1/log/HTXProcessingServer.log").arg(appDataPath), QString("%1/log/HTXProcessingServerErrors.log").arg(appDataPath));

    QElapsedTimer timer;
    timer.start();

    //License
    LicenseManager::SetLicenseInfo(QString::fromStdWString(PRODUCT_DATA), QString::fromStdString(PRODUCT_ID));
    while (timer.elapsed() < 1000) qApp->processEvents();

    if (!LicenseManager::CheckLicense()) {
        TC::MessageDialog::warning(0, "License", LicenseManager::GetErrorAsString(), QDialogButtonBox::StandardButton::Ok);

        LicenseDialog licenseDialog;
        licenseDialog.exec();
    }

    if (!LicenseManager::CheckLicense()) {
        TC::MessageDialog::warning(0, "License", LicenseManager::GetErrorAsString(), QDialogButtonBox::StandardButton::Ok);
        return EXIT_SUCCESS;
    }

    auto processingOptionManager = ProcessingOptionManager::GetInstance();

    if (!processingOptionManager->ProcessingOptionsExists()) {
        processingOptionManager->WriteDefaultOptions();
    }
    const auto processingOptions = processingOptionManager->ReadProcessingOptions();
    processingOptionManager->SetProcessingOptions(processingOptions);
    
    HTXProcessingServerWidget htxProcessingServerWidget{};
    htxProcessingServerWidget.show();
    return app.exec();
}

auto CrashCallback(UINT nCode, LPVOID lpVal1, LPVOID lpVal2) ->bool {
    //destroy logger
    QsLogging::Logger& logger = QsLogging::Logger::instance();
    logger.destroyInstance();
    Sleep(2000);

    wchar_t tempPath[MAX_PATH];
    GetTempPathW(MAX_PATH, tempPath);
    const auto tempFolderPath = QString::fromStdWString(tempPath);


    const auto processingServerDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
    const auto processingServerLogFolderPath = processingServerDataPath + "/log";

    auto htProcessingAppDataPath = processingServerDataPath;
    htProcessingAppDataPath.replace("HTXProcessingServer", "TCHTProcessingMatlabApp");

    QStringList logDataFilePathList;
    logDataFilePathList.push_back(processingServerLogFolderPath + "/HTXProcessingServer.log");
    logDataFilePathList.push_back(processingServerLogFolderPath + "/HTXProcessingServer.log.1");
    logDataFilePathList.push_back(processingServerLogFolderPath + "/HTXProcessingServer.log.2");
    logDataFilePathList.push_back(processingServerLogFolderPath + "/HTXProcessingServer.log.3");
    logDataFilePathList.push_back(htProcessingAppDataPath + "/TCHTProcessingMatlabApp.log");
    logDataFilePathList.push_back(htProcessingAppDataPath + "/TCHTProcessingMatlabApp.log.1");
    logDataFilePathList.push_back(htProcessingAppDataPath + "/TCHTProcessingMatlabApp.log.2");
    logDataFilePathList.push_back(htProcessingAppDataPath + "/TCHTProcessingMatlabApp.log.3");
    
    for (const auto& logDataFilePath : logDataFilePathList) {
        if (!QFile::exists(logDataFilePath)) { continue; }
        const auto logDataFileName = QFileInfo(logDataFilePath).fileName();
        const QString tempFilePath = QString("%1%2").arg(tempFolderPath).arg(logDataFileName);
        QFile::copy(logDataFilePath, tempFilePath);

        miniDmpSender->sendAdditionalFile(tempFilePath.toStdWString().c_str());
    }

    return true;
}