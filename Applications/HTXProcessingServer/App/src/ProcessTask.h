#pragma once

#include <memory>
#include <QString>

#include "ModalityType.h"
#include "ProcessType.h"

class ProcessTask {
public:
    ProcessTask();
    ProcessTask(const ProcessTask& other);
    ~ProcessTask();

    auto operator=(const ProcessTask& other)->ProcessTask&;

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto SetProcessType(const ProcessType& processType)->void;
    auto SetModalityType(const ModalityType& modalityType)->void;
    auto SetTimeFrameIndex(const int32_t& timeFrameIndex)->void;
    auto SetTileIndex(const int32_t& tileIndex)->void;

    auto GetRootFolderPath()const->const QString&;
    auto GetProcessType()const->const ProcessType&;
    auto GetModalityType()const->const ModalityType&;
    auto GetTimeFrameIndex()const->const int32_t&;
    auto GetTileIndex()const->const int32_t&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};