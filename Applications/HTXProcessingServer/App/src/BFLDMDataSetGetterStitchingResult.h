#pragma once

#include <memory>

#include <IBFLDMDataSetGetter.h>

#include <H5Cpp.h>

class BFLDMDataSetGetterStitchingResult : public TC::TCFWriter::IBFLDMDataSetGetter {
public:
    BFLDMDataSetGetterStitchingResult();
    ~BFLDMDataSetGetterStitchingResult();

    auto SetSourceDataSet(H5::DataSet* dataSet)->void;
    auto SetDestLDMGroup(H5::Group* group)->void;

    auto SetReadingOffset(const int32_t& offsetX, const int32_t& offsetY)->void;

    auto SetLdmConfiguration(const TC::IO::LdmCore::LdmConfiguration& ldmConfiguration) -> void override;

    auto GetTileData(const int32_t& tileIndex) -> std::shared_ptr<uint8_t[]> override;

    auto SetChannelCount(const int32_t& channelCount)->void;
    auto GetChannelCount() -> int32_t override;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};