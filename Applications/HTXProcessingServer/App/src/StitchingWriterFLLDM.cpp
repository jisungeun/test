#include "StitchingWriterFLLDM.h"

#include <TCLogger.h>
#include <H5Cpp.h>
#include <HDF5Mutex.h>
#include <QFile>

#include <TCFFLLDMDataSet.h>

#include "FLLDMDataSetGetterStitchingResult.h"
#include "ElapsedTimeReader.h"
#include "Octree2DConfigurator.h"
#include "Octree3DConfigurator.h"
#include "RecordingTimeReader.h"
#include "ResultFilePathGenerator.h"
#include "PiercedRectangle.h"
#include "PiercedRectangleMerger.h"
#include "TCFFLLDMDataSetWriter.h"

using namespace TC::TCFWriter;
using namespace TC::IO;
using namespace TC::IO::LdmCore;

using SequenceModality = AcquisitionSequenceInfo::Modality;

class StitchingWriterFLLDM::Impl {
public:
    struct WritingStartIndex {
        int32_t x{};
        int32_t y{};
        int32_t z{};
    };

    struct CenterPosition {
        int32_t x{};
        int32_t y{};
    };

    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    QString tempTCFFilePath{};

    AcquisitionConfig acquisitionConfig{};
    AcquisitionSequenceInfo acquisitionSequenceInfo{};

    auto CheckTempTCFFile(const int32_t& channelIndex)->bool;

    auto ReadFLDataSetMetaInfo(const int32_t& timeIndex, const int32_t& channelIndex)->TCFFLDataSetMetaInfo;

    auto CalculateStitchingPiercedRect(const int32_t& channelIndex)->PiercedRectangle;
    auto CalculateWritingStartIndex(const int32_t& mergedCenterPositionX, const int32_t& mergedCenterPositionY,
        const int32_t& centerPositionX, const int32_t& centerPositionY)->WritingStartIndex;
    auto ReadFLCenterPosition(const int32_t& timeIndex, const int32_t& channelIndex)->CenterPosition;
};

auto StitchingWriterFLLDM::Impl::CheckTempTCFFile(const int32_t& channelIndex) -> bool {
    try {
        if (!H5::H5File::isHdf5(this->tempTCFFilePath.toStdString())) {
            return false;
        }
        const H5::H5File file(this->tempTCFFilePath.toStdString(), H5F_ACC_RDWR);
        if (!file.nameExists(QString("/Data/3DFL").toStdString())) {
            return false;
        }
        if (!file.nameExists(QString("/Data/2DFLMIP").toStdString())) {
            return false;
        }
    } catch (const H5::Exception&) {
        return false;
    }
    return true;
}

auto StitchingWriterFLLDM::Impl::ReadFLDataSetMetaInfo(const int32_t& timeIndex, const int32_t& channelIndex)
    -> TCFFLDataSetMetaInfo {
    const auto flProcessedDataFilePath = 
        ResultFilePathGenerator::GetFLProcessedFilePath(this->rootFolderPath, channelIndex, 0, timeIndex);
    const auto stitchingResultFilePath = 
        ResultFilePathGenerator::GetFLStitchingFilePath(this->rootFolderPath, channelIndex, timeIndex);

    // stitching data size
    int32_t stitchingDataSizeX{}, stitchingDataSizeY{}, stitchingDataSizeZ{};
    {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto stitchingDataSet3D = stitchingDataFile.openDataSet("StitchingData3D");

        const auto attrDataSizeX = stitchingDataSet3D.openAttribute("dataSizeX");
        const auto attrDataSizeY = stitchingDataSet3D.openAttribute("dataSizeY");
        const auto attrDataSizeZ = stitchingDataSet3D.openAttribute("dataSizeZ");

        attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
        attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);
        attrDataSizeZ.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeZ);
    }

    // pixel world size
    float pixelWorldSizeX{}, pixelWorldSizeY{}, pixelWorldSizeZ{};
    {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        const H5::H5File processedDataFile(flProcessedDataFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto processedDataSet = processedDataFile.openDataSet("Data");

        const auto attrPixelWorldSizeX = processedDataSet.openAttribute("pixelWorldSizeX");
        const auto attrPixelWorldSizeY = processedDataSet.openAttribute("pixelWorldSizeY");
        const auto attrPixelWorldSizeZ = processedDataSet.openAttribute("pixelWorldSizeZ");

        attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
        attrPixelWorldSizeY.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeY);
        attrPixelWorldSizeZ.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeZ);
    }

    // min max values
    float data3dMinValue{}, data3dMaxValue{}, dataMIPMinValue{}, dataMIPMaxValue{};
    {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };

        const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto stitchingDataSet3D = stitchingDataFile.openDataSet("StitchingData3D");
        const auto stitchingDataSetMIP = stitchingDataFile.openDataSet("StitchingDataMIP");

        const auto attrData3DMinValue = stitchingDataSet3D.openAttribute("minValue");
        const auto attrData3DMaxValue = stitchingDataSet3D.openAttribute("maxValue");
        const auto attrDataMIPMinValue = stitchingDataSetMIP.openAttribute("minValue");
        const auto attrDataMIPMaxValue = stitchingDataSetMIP.openAttribute("maxValue");

        attrData3DMinValue.read(H5::PredType::NATIVE_FLOAT, &data3dMinValue);
        attrData3DMaxValue.read(H5::PredType::NATIVE_FLOAT, &data3dMaxValue);
        attrDataMIPMinValue.read(H5::PredType::NATIVE_FLOAT, &dataMIPMinValue);
        attrDataMIPMaxValue.read(H5::PredType::NATIVE_FLOAT, &dataMIPMaxValue);
    }

    // recordedTime
    QDateTime recordedTime;
    {
        RecordingTimeReader recordingTimeReader;
        const auto recordedTimeFilePath = QString("%1/data/0000/FL3D/CH%2/%3/acquisition_timestamp.txt")
            .arg(this->rootFolderPath).arg(channelIndex).arg(timeIndex, 4, 10, QChar('0'));

        recordingTimeReader.SetFilePath(recordedTimeFilePath);
        const auto recordingTimeReadResult = recordingTimeReader.Read();


        if (recordingTimeReadResult == true) {
            recordedTime = recordingTimeReader.GetRecordingTime();
        } else {
            recordedTime = QDateTime::currentDateTime();
        }
    }

    // elapsedTime
    double elapsedTime;
    constexpr auto elapsedTimeUnit = TimeUnit::Second;
    {
        ElapsedTimeReader elapsedTimeReader;
        const QString elapsedTimeFilePath = QString("%1/data/0000/FL3D/CH%2/%3/logical_timestamp.txt")
            .arg(this->rootFolderPath).arg(channelIndex).arg(timeIndex, 4, 10, QChar('0'));

        elapsedTimeReader.SetFilePath(elapsedTimeFilePath);
        const auto elapsedTimeReadResult = elapsedTimeReader.Read();

        if (elapsedTimeReadResult == true) {
            elapsedTime = elapsedTimeReader.GetElapsedTime(elapsedTimeUnit);
        } else {
            elapsedTime = 0;
        }
    }

    // tcfDataIndex
    const auto sequenceModality = ConvertSequenceModality(channelIndex);
    const auto tcfDataIndex = this->acquisitionSequenceInfo.GetOrderIndex(sequenceModality, timeIndex);

    // position
    TCFDataSetRecordedPosition position;
    {
        const auto& acquisitionPosition = this->acquisitionConfig.GetAcquisitionPosition();

        const auto positionX = acquisitionPosition.positionXMillimeter;
        const auto positionY = acquisitionPosition.positionYMillimeter;
        const auto positionZ = acquisitionPosition.positionZMillimeter;
        const auto positionC = acquisitionPosition.positionCMillimeter;
        constexpr auto positionUnit = LengthUnit::Millimenter;

        position.SetPositions(positionX, positionY, positionZ, positionC, positionUnit);
    }

    TCFFLDataSetMetaInfo metaInfo;
    metaInfo.SetDataSize(stitchingDataSizeX, stitchingDataSizeY, stitchingDataSizeZ);
    metaInfo.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);
    metaInfo.SetDataMinMaxValue(data3dMinValue, data3dMaxValue);
    metaInfo.SetMIPDataMinMaxValue(dataMIPMinValue, dataMIPMaxValue);
    metaInfo.SetRecordedTime(recordedTime);
    metaInfo.SetElapsedTime(elapsedTime, elapsedTimeUnit);
    metaInfo.SetTimeFrameIndex(tcfDataIndex);
    metaInfo.SetPosition(position);

    return metaInfo;
}

auto StitchingWriterFLLDM::Impl::CalculateStitchingPiercedRect(const int32_t& channelIndex) -> PiercedRectangle {
    const auto timeFrameCount = this->acquisitionSequenceInfo.GetTimeFrameCount();

    QList<PiercedRectangle> stitchingRectList;

    const auto flSequenceModality = ConvertSequenceModality(channelIndex);
    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(flSequenceModality, timeIndex)) {
            continue;
        }

        const auto stitchingResultFilePath =
            ResultFilePathGenerator::GetFLStitchingFilePath(rootFolderPath, channelIndex, timeIndex);

        const auto [centerPositionX, centerPositionY] = ReadFLCenterPosition(timeIndex, channelIndex);

        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto stitchingDataSet3D = stitchingDataFile.openDataSet("StitchingData3D");

        int32_t stitchingDataSizeX{}, stitchingDataSizeY{};
        const auto attrDataSizeX = stitchingDataSet3D.openAttribute("dataSizeX");
        const auto attrDataSizeY = stitchingDataSet3D.openAttribute("dataSizeY");

        attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
        attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);

        PiercedRectangle piercedRectangle;
        piercedRectangle.SetSize(stitchingDataSizeX, stitchingDataSizeY);
        piercedRectangle.SetPiercingIndex(centerPositionX, centerPositionY);

        stitchingRectList.push_back(piercedRectangle);
    }

    PiercedRectangleMerger piercedRectangleMerger;
    piercedRectangleMerger.SetPiercedRectangleList(stitchingRectList);
    piercedRectangleMerger.Merge();

    return piercedRectangleMerger.GetMergedPiercedRectangle();
}

auto StitchingWriterFLLDM::Impl::CalculateWritingStartIndex(const int32_t& mergedCenterPositionX,
    const int32_t& mergedCenterPositionY, const int32_t& centerPositionX,
    const int32_t& centerPositionY) -> WritingStartIndex {

    const auto writingStartIndexX = centerPositionX - mergedCenterPositionX;
    const auto writingStartIndexY = centerPositionY - mergedCenterPositionY;

    return WritingStartIndex{ writingStartIndexX, writingStartIndexY , 0 };
}

auto StitchingWriterFLLDM::Impl::ReadFLCenterPosition(const int32_t& timeIndex, const int32_t& channelIndex)
    -> CenterPosition {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    const auto stitchingResultFilePath = 
        ResultFilePathGenerator::GetFLStitchingFilePath(this->rootFolderPath, channelIndex, timeIndex);
    const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);

    int32_t centerPositionX{}, centerPositionY{};

    const auto attCenterPositionX = stitchingDataFile.openAttribute("centerPositionX");
    const auto attCenterPositionY = stitchingDataFile.openAttribute("centerPositionY");

    attCenterPositionX.read(H5::PredType::NATIVE_INT32, &centerPositionX);
    attCenterPositionY.read(H5::PredType::NATIVE_INT32, &centerPositionY);

    return { centerPositionX , centerPositionY };
}

StitchingWriterFLLDM::StitchingWriterFLLDM() : d(std::make_unique<Impl>()) {
}

StitchingWriterFLLDM::~StitchingWriterFLLDM() = default;

auto StitchingWriterFLLDM::SetRootFolderPath(const QString & rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto StitchingWriterFLLDM::SetTempTCFFilePath(const QString & tempTCFFilePath) -> void {
    d->tempTCFFilePath = tempTCFFilePath;
}

auto StitchingWriterFLLDM::SetAcquisitionConfig(const AcquisitionConfig & acquisitionConfig) -> void {
    d->acquisitionConfig = acquisitionConfig;
}

auto StitchingWriterFLLDM::SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo & acquisitionSequenceInfo) -> void {
    d->acquisitionSequenceInfo = acquisitionSequenceInfo;
}

auto StitchingWriterFLLDM::Write(const int32_t & timeIndex, const int32_t& channelIndex) -> bool {
    QLOG_INFO() << "stitchingWriter FL LDM time(" << timeIndex << ")" << "Channel (" << channelIndex << ")";

    const auto flDataSetMetaInfo = d->ReadFLDataSetMetaInfo(timeIndex, channelIndex);

    const auto stitchingPiercedRect = d->CalculateStitchingPiercedRect(channelIndex);
    const auto [centerPositionX, centerPositionY] = d->ReadFLCenterPosition(timeIndex, channelIndex);
    const auto [writingStartIndexX, writingStartIndexY, writingStartIndexZ] =
        d->CalculateWritingStartIndex(stitchingPiercedRect.GetPiercingIndexX(), stitchingPiercedRect.GetPiercingIndexY(),
            centerPositionX, centerPositionY);

    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    if (!d->CheckTempTCFFile(channelIndex)) {
        QLOG_ERROR() << "CheckTempTCFFile channel(" << channelIndex << ") fails";
        return false;
    }

    const auto stitchingResultFilePath = ResultFilePathGenerator::GetFLStitchingFilePath(d->rootFolderPath, channelIndex, timeIndex);
    const H5::H5File stitchingDataFile(stitchingResultFilePath.toStdString(), H5F_ACC_RDONLY);

    auto srcDataSet = stitchingDataFile.openDataSet("StitchingData3D");
    auto srcMIPDataSet = stitchingDataFile.openDataSet("StitchingDataMIP");

    const auto sequenceModality = ConvertSequenceModality(channelIndex);
    const auto tcfDataIndex = d->acquisitionSequenceInfo.GetOrderIndex(sequenceModality, timeIndex);
    const auto timeFrameName = QString("%1").arg(tcfDataIndex, 6, 10, QChar('0'));

    const H5::H5File file(d->tempTCFFilePath.toStdString(), H5F_ACC_RDWR);

    const auto channelName = QString("CH%1").arg(channelIndex);

    const auto data3dGroup = file.openGroup("/Data/3DFL");
    if (!data3dGroup.nameExists(channelName.toStdString())) {
        data3dGroup.createGroup(channelName.toStdString());
    }
    const auto channel3DGroup = data3dGroup.openGroup(channelName.toStdString());
    auto destLDMGroup = channel3DGroup.createGroup(timeFrameName.toStdString());

    const auto data2dMIPGroup = file.openGroup("/Data/2DFLMIP");
    if (!data2dMIPGroup.nameExists(channelName.toStdString())) {
        data2dMIPGroup.createGroup(channelName.toStdString());
    }
    const auto channel2DMIPGroup = data2dMIPGroup.openGroup(channelName.toStdString());
    auto destLDMMIPGroup = channel2DMIPGroup.createGroup(timeFrameName.toStdString());

    const auto dataSizeX = static_cast<size_t>(stitchingPiercedRect.GetSizeX());
    const auto dataSizeY = static_cast<size_t>(stitchingPiercedRect.GetSizeY());
    const auto dataSizeZ = static_cast<size_t>(flDataSetMetaInfo.GetSizeZ());

    constexpr auto tileSize = 128;

    const Dimension dimension3D{ dataSizeX, dataSizeY, dataSizeZ };
    const Dimension tileDimension3D{ tileSize, tileSize, tileSize };

    const Dimension dimensionMIP{ dataSizeX, dataSizeY };
    const Dimension tileDimensionMIP{ tileSize, tileSize };

    Octree3DConfigurator octree3DConfigurator{ dimension3D, tileDimension3D };
    const auto ldmConfiguration3D = octree3DConfigurator.Configure();

    Octree2DConfigurator octree2DConfigurator{ dimensionMIP, tileDimensionMIP };
    const auto ldmConfigurationMip = octree2DConfigurator.Configure();

    std::shared_ptr<FLLDMDataSetGetterStitchingResult> dataGetter{ new FLLDMDataSetGetterStitchingResult };
    dataGetter->SetReadingOffset(writingStartIndexX, writingStartIndexY, writingStartIndexZ);
    dataGetter->SetSourceDataSet(&srcDataSet);
    dataGetter->SetDestLDMGroup(&destLDMGroup);
    dataGetter->SetSourceMIPDataSet(&srcMIPDataSet);
    dataGetter->SetDestLDMMIPGroup(&destLDMMIPGroup);
    dataGetter->SetLdmConfiguration(ldmConfiguration3D, ldmConfigurationMip);

    TCFFLLDMDataSet tcfFLLDMDataSet;
    tcfFLLDMDataSet.SetMetaInfo(flDataSetMetaInfo);
    tcfFLLDMDataSet.SetDataGetter(dataGetter);
    tcfFLLDMDataSet.SetLdmConfiguration(ldmConfiguration3D, ldmConfigurationMip);

    locker.Unlock();

    TCFFLLDMDataSetWriter writer;
    writer.SetDestLDMGroup(&destLDMGroup, &destLDMMIPGroup);
    writer.SetTCFFLLDMDataSet(tcfFLLDMDataSet);
    writer.SetWholeDataSize(dataSizeX, dataSizeY, dataSizeZ);
    if (!writer.Write()) {
        QLOG_ERROR() << "tcfFLLDMDataSetWriter.Write() fails";
        return false;
    }

    return true;
}
