#include "SampleObjectUpdater.h"

#include <QMutex>
#include <QTimer>
#include <QWaitCondition>

#include "ProcessingFolderScanner.h"
#include "SampleObjectGenerator.h"

class SampleObjectUpdater::Impl {
public:
    Impl() = default;
    ~Impl() {
        delete periodicTimer;
    }
    
    int32_t periodInSec{ 1 };
    QTimer* periodicTimer{ new QTimer };

    QMutex mutex;

    QWaitCondition waitCondition;
    bool runFlag{ true };

    QString topDirectory{};
    QString backgroundFolderPath{};

    bool runningIsStopped{ false };

    QList<SampleObject> sampleObjectList;
};

SampleObjectUpdater::SampleObjectUpdater(QObject* parent)
    : d(new Impl()), QThread(parent) {
    connect(d->periodicTimer, SIGNAL(timeout()), this, SLOT(WakeThread()));
    start();
}

SampleObjectUpdater::~SampleObjectUpdater() {
    Close();
    wait();
}

auto SampleObjectUpdater::SetTopDirectory(const QString& topDirectory) -> void {
    d->topDirectory = topDirectory;
}

auto SampleObjectUpdater::SetSystemBackgroundFolderPath(const QString& backgroundFolderPath) -> void {
    d->backgroundFolderPath = backgroundFolderPath;
}

auto SampleObjectUpdater::GetSampleObjectList() const -> const QList<SampleObject>& {
    QMutexLocker locker(&d->mutex);
    return d->sampleObjectList;
}

auto SampleObjectUpdater::Start() -> void {
    ProcessingFolderScanner::Scan(d->topDirectory, d->backgroundFolderPath);

    if (!SampleObjectGenerator::IsGenerating()) {
        d->sampleObjectList = SampleObjectGenerator::Generate();
    }

    const auto periodInMilliseconds = d->periodInSec * 1000;
    d->periodicTimer->start(periodInMilliseconds);
}

auto SampleObjectUpdater::Stop() -> void {
    QMutexLocker locker(&d->mutex);
    d->periodicTimer->stop();
}

auto SampleObjectUpdater::Close() -> void {
    d->periodicTimer->stop();

    d->mutex.lock();
    d->runFlag = false;
    d->waitCondition.wakeOne();
    d->mutex.unlock();
}

auto SampleObjectUpdater::IsStopped() const -> bool {
    QMutexLocker locker(&d->mutex);
    return d->runningIsStopped;
}

void SampleObjectUpdater::run() {
    while (true) {
        QMutexLocker locker(&d->mutex);
        if (d->runFlag) {
            d->runningIsStopped = true;
            d->waitCondition.wait(locker.mutex());
            d->runningIsStopped = false;
            if (!d->runFlag) {
                break;
            }
        } else {
            break;
        }

        const auto topDirectory = d->topDirectory;
        const auto backgroundFolderPath = d->backgroundFolderPath;

        ProcessingFolderScanner::Scan(topDirectory, backgroundFolderPath);

        if (!SampleObjectGenerator::IsGenerating()) {
            const auto sampleObjectList = SampleObjectGenerator::Generate();
            d->sampleObjectList = sampleObjectList;
        }
    }
}

void SampleObjectUpdater::WakeThread() {
    QMutexLocker locker(&d->mutex);
    d->waitCondition.wakeOne();
}
