#pragma once

#include <memory>
#include "SIUnit.h"

class TileDataHandler {
public:
    using Pointer = std::shared_ptr<TileDataHandler>;

    ~TileDataHandler();

    static auto GetInstance()->Pointer;
    auto SetMaximumMemorySize(const double& maximumMemorySize, const DataSizeUnit& unit)->void;

    auto HasSpace(const double& memorySize, const DataSizeUnit& unit)->bool;

    auto AddTileData(const std::shared_ptr<float[]>& tileData, const int32_t& dataIndex, const double& memorySize, 
        const DataSizeUnit& unit)->void;
    auto RemoveTileData(const int32_t& dataIndex)->void;
    auto RemoveOldestData()->void;

    auto GetHandlingDataSize(const DataSizeUnit& unit) const ->double;

    auto HasData(const int32_t& dataIndex) const ->bool;
    auto GetTileData(const int32_t& dataIndex)const ->std::shared_ptr<float[]>;

    auto Clear()->void;

protected:
    TileDataHandler();

private:
    class Impl;
    std::unique_ptr<Impl> d;
};