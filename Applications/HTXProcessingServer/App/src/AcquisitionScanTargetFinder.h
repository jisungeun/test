#pragma once

#include <memory>
#include <QString>

#include "ModalityType.h"

class AcquisitionScanTargetFinder {
public:
    AcquisitionScanTargetFinder();
    ~AcquisitionScanTargetFinder();

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto Find()->bool;

    auto IsNextScanBG() const->bool;
    auto GetNextScanTimeIndex() const->int32_t;
    auto GetNextScanTileIndex() const->int32_t;
    auto GetNextScanModalityType() const->ModalityType;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
