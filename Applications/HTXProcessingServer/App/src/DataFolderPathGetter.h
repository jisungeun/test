#pragma once

#include <memory>
#include <QString>

#include "ModalityType.h"

class DataFolderPathGetter {
public:
    DataFolderPathGetter();
    ~DataFolderPathGetter();

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto SetModalityType(const ModalityType& modalityType)->void;
    auto SetTileIndex(const int32_t& tileIndex)->void;
    auto SetTimeIndex(const int32_t& timeIndex)->void;

    auto ExistTileFolder()const->bool;
    auto ExistModalityFolder()const->bool;
    auto ExistTimeFrameFolder()const->bool;

    auto GetTileFolderPath()const->QString;
    auto GetModalityFolderPath()const->QString;
    auto GetTimeFrameFolderPath()const->QString;

    auto GetProfileFolderPath()const->QString;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
