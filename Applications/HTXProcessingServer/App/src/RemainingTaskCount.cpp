#include "RemainingTaskCount.h"

#include <QMap>

using ZSliceCount = int32_t;

class RemainingTaskCount::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl& = default;

    int32_t tileNumberX{};
    int32_t tileNumberY{};

    QMap<ZSliceCount, int32_t> htProcessingCountMap{};
    QMap<ZSliceCount, int32_t> flProcessingCountMap{};
    QMap<ZSliceCount, int32_t> bfProcessingCountMap{};

    QMap<ZSliceCount, int32_t> htStitchingCountMap{};
    QMap<ZSliceCount, int32_t> flStitchingCountMap{};
    QMap<ZSliceCount, int32_t> bfStitchingCountMap{};

    QMap<ZSliceCount, int32_t> htStitchingWrittenCountMap{};
    QMap<ZSliceCount, int32_t> flStitchingWrittenCountMap{};
    QMap<ZSliceCount, int32_t> bfStitchingWrittenCountMap{};

    QMap<ZSliceCount, int32_t> htLDMConvertingCountMap{};
    QMap<ZSliceCount, int32_t> flLDMConvertingCountMap{};
    QMap<ZSliceCount, int32_t> bfLDMConvertingCountMap{};
};

RemainingTaskCount::RemainingTaskCount() : d(new Impl()) {
}

RemainingTaskCount::RemainingTaskCount(const RemainingTaskCount& other) : d(new Impl(*other.d)) {
}

RemainingTaskCount::~RemainingTaskCount() = default;

auto RemainingTaskCount::operator=(const RemainingTaskCount& other) -> RemainingTaskCount& {
    *(this->d) = *(other.d);
    return *this;
}

auto RemainingTaskCount::SetHTProcessingCount(const int32_t& zSliceCount, const int32_t& htProcessingCount) -> void {
    if (d->htProcessingCountMap.contains(zSliceCount)) {
        d->htProcessingCountMap.remove(zSliceCount);
    }

    d->htProcessingCountMap[zSliceCount] = htProcessingCount;
}

auto RemainingTaskCount::SetFLProcessingCount(const int32_t& zSliceCount, const int32_t& flProcessingCount) -> void {
    if (d->flProcessingCountMap.contains(zSliceCount)) {
        d->flProcessingCountMap.remove(zSliceCount);
    }
    d->flProcessingCountMap[zSliceCount] = flProcessingCount;
}

auto RemainingTaskCount::SetBFProcessingCount(const int32_t& zSliceCount, const int32_t& bfProcessingCount) -> void {
    if (d->bfProcessingCountMap.contains(zSliceCount)) {
        d->bfProcessingCountMap.remove(zSliceCount);
    }
    d->bfProcessingCountMap[zSliceCount] = bfProcessingCount;
}

auto RemainingTaskCount::SetHTStitchingCount(const int32_t& zSliceCount, const int32_t& htStitchingCount) -> void {
    if (d->htStitchingCountMap.contains(zSliceCount)) {
        d->htStitchingCountMap.remove(zSliceCount);
    }
    d->htStitchingCountMap[zSliceCount] = htStitchingCount;
}

auto RemainingTaskCount::SetFLStitchingCount(const int32_t& zSliceCount, const int32_t& flStitchingCount) -> void {
    if (d->flStitchingCountMap.contains(zSliceCount)) {
        d->flStitchingCountMap.remove(zSliceCount);
    }

    d->flStitchingCountMap[zSliceCount] = flStitchingCount;
}

auto RemainingTaskCount::SetBFStitchingCount(const int32_t& zSliceCount, const int32_t& bfStitchingCount) -> void {
    if (d->bfStitchingCountMap.contains(zSliceCount)) {
        d->bfStitchingCountMap.remove(zSliceCount);
    }
    d->bfStitchingCountMap[zSliceCount] = bfStitchingCount;
}

auto RemainingTaskCount::SetHTStitchingWrittenCount(const int32_t& zSliceCount, const int32_t& htStitchingWrittenCount) -> void {
    if (d->htStitchingWrittenCountMap.contains(zSliceCount)) {
        d->htStitchingWrittenCountMap.remove(zSliceCount);
    }
    d->htStitchingWrittenCountMap[zSliceCount] = htStitchingWrittenCount;
}

auto RemainingTaskCount::SetFLStitchingWrittenCount(const int32_t& zSliceCount, 
    const int32_t& flStitchingWrittenCount) -> void {
    if (d->flStitchingWrittenCountMap.contains(zSliceCount)) {
        d->flStitchingWrittenCountMap.remove(zSliceCount);
    }
    d->flStitchingWrittenCountMap[zSliceCount] = flStitchingWrittenCount;
}

auto RemainingTaskCount::SetBFStitchingWrittenCount(const int32_t& zSliceCount, const int32_t& bfStitchingWrittenCount) -> void {
    if (d->bfStitchingWrittenCountMap.contains(zSliceCount)) {
        d->bfStitchingWrittenCountMap.remove(zSliceCount);
    }
    d->bfStitchingWrittenCountMap[zSliceCount] = bfStitchingWrittenCount;
}

auto RemainingTaskCount::SetHTLDMConvertingCount(const int32_t& zSliceCount, const int32_t& htLDMConvertingCount) -> void {
    if (d->htLDMConvertingCountMap.contains(zSliceCount)) {
        d->htLDMConvertingCountMap.remove(zSliceCount);
    }
    d->htLDMConvertingCountMap[zSliceCount] = htLDMConvertingCount;
}

auto RemainingTaskCount::SetFLLDMConvertingCount(const int32_t& zSliceCount, const int32_t& flLDMConvertingCount)
    -> void {
    if (d->flLDMConvertingCountMap.contains(zSliceCount)) {
        d->flLDMConvertingCountMap.remove(zSliceCount);
    }
    d->flLDMConvertingCountMap[zSliceCount] = flLDMConvertingCount;
}

auto RemainingTaskCount::SetBFLDMConvertingCount(const int32_t& zSliceCount, const int32_t& bfLDMConvertingCount) -> void {
    if (d->bfLDMConvertingCountMap.contains(zSliceCount)) {
        d->bfLDMConvertingCountMap.remove(zSliceCount);
    }
    d->bfLDMConvertingCountMap[zSliceCount] = bfLDMConvertingCount;
}

auto RemainingTaskCount::GetHTProcessingCount(const int32_t& zSliceCount) -> int32_t {
    if (d->htProcessingCountMap.contains(zSliceCount)) {
        return d->htProcessingCountMap[zSliceCount];
    } else {
        return 0;
    }
}

auto RemainingTaskCount::GetFLProcessingCount(const int32_t& zSliceCount) -> int32_t {
    if (d->flProcessingCountMap.contains(zSliceCount)) {
        return d->flProcessingCountMap[zSliceCount];
    } else {
        return 0;
    }
}

auto RemainingTaskCount::GetBFProcessingCount(const int32_t& zSliceCount) -> int32_t {
    if (d->bfProcessingCountMap.contains(zSliceCount)) {
        return d->bfProcessingCountMap[zSliceCount];
    } else {
        return 0;
    }
}

auto RemainingTaskCount::GetHTStitchingCount(const int32_t& zSliceCount) -> int32_t {
    if (d->htStitchingCountMap.contains(zSliceCount)) {
        return d->htStitchingCountMap[zSliceCount];
    } else {
        return 0;
    }
}

auto RemainingTaskCount::GetFLStitchingCount(const int32_t& zSliceCount) -> int32_t {
    if (d->flStitchingCountMap.contains(zSliceCount)) {
        return d->flStitchingCountMap[zSliceCount];
    } else {
        return 0;
    }
}

auto RemainingTaskCount::GetBFStitchingCount(const int32_t& zSliceCount) -> int32_t {
    if (d->bfStitchingCountMap.contains(zSliceCount)) {
        return d->bfStitchingCountMap[zSliceCount];
    } else {
        return 0;
    }
}

auto RemainingTaskCount::GetHTStitchingWrittenCount(const int32_t& zSliceCount) -> int32_t {
    if (d->htStitchingWrittenCountMap.contains(zSliceCount)) {
        return d->htStitchingWrittenCountMap[zSliceCount];
    } else {
        return 0;
    }
}

auto RemainingTaskCount::GetFLStitchingWrittenCount(const int32_t& zSliceCount) -> int32_t {
    if (d->flStitchingWrittenCountMap.contains(zSliceCount)) {
        return d->flStitchingWrittenCountMap[zSliceCount];
    } else {
        return 0;
    }
}

auto RemainingTaskCount::GetBFStitchingWrittenCount(const int32_t& zSliceCount) -> int32_t {
    if (d->bfStitchingWrittenCountMap.contains(zSliceCount)) {
        return d->bfStitchingWrittenCountMap[zSliceCount];
    } else {
        return 0;
    }
}

auto RemainingTaskCount::GetHTLDMConvertingCount(const int32_t& zSliceCount) -> int32_t {
    if (d->htLDMConvertingCountMap.contains(zSliceCount)) {
        return d->htLDMConvertingCountMap[zSliceCount];
    } else {
        return 0;
    }
}

auto RemainingTaskCount::GetFLLDMConvertingCount(const int32_t& zSliceCount) -> int32_t {
    if (d->flLDMConvertingCountMap.contains(zSliceCount)) {
        return d->flLDMConvertingCountMap[zSliceCount];
    } else {
        return 0;
    }
}

auto RemainingTaskCount::GetBFLDMConvertingCount(const int32_t& zSliceCount) -> int32_t {
    if (d->bfLDMConvertingCountMap.contains(zSliceCount)) {
        return d->bfLDMConvertingCountMap[zSliceCount];
    } else {
        return 0;
    }
}
