#pragma once

#include <memory>
#include <QString>

#include "AcquisitionConfig.h"
#include "AcquisitionSequenceInfo.h"

class ThumbnailWriterFL {
public:
    ThumbnailWriterFL();
    ~ThumbnailWriterFL();

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto SetTempTCFFilePath(const QString& tempTCFFilePath)->void;
    auto SetTimeFrameIndex(const int32_t& timeFrameIndex)->void;
    auto SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo)->void;
    auto SetAcquisitionConfig(const AcquisitionConfig& acquisitionConfig)->void;
    auto SetFLChannelIndexList(const QList<int32_t>& flChannelIndexList)->void;

    auto Write()->bool;

    auto GetThumbnailData()const->std::tuple<std::shared_ptr<uint8_t[]>, int32_t, int32_t>;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
