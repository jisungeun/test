#pragma once

#include <memory>

#include "ProcessingOptions.h"

class ProcessingOptionManager {
public:
    static auto GetInstance()->std::shared_ptr<ProcessingOptionManager>;
    ~ProcessingOptionManager();

    auto ProcessingOptionsExists()->bool;
    auto WriteDefaultOptions()->void;
    auto ReadProcessingOptions()->ProcessingOptions;

    auto SetProcessingOptions(const ProcessingOptions& options)->void;
    auto GetProcessingOptions()->ProcessingOptions;

protected:
    ProcessingOptionManager();

private:
    class Impl;
    std::unique_ptr<Impl> d;
};