#define LOGGER_TAG "[StitcherHT]"

#include "StitcherHT.h"

#include <QFile>
#include <QStringList>

#include "H5Cpp.h"
#include "HDF5Mutex.h"
#include "OverlapRelationCalculatorPhaseCorrelation.h"
#include "ResultFilePathGenerator.h"
#include "StitchingWriterHDF5HT.h"
#include "TileConfigurationReaderHT.h"
#include "TilePositionCalculatorBestOverlap.h"
#include "TileSetGeneratorHT.h"
#include "TCLogger.h"

class StitcherHT::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    int32_t tileNumberX{};
    int32_t tileNumberY{};

    double limitedSizeXInMicrometer{};
    double limitedSizeYInMicrometer{};
    bool sizeLimited{ false };

    auto GetProcessedDataFilePathList(const int32_t& tileNumber, const int32_t& timeFrameIndex)->QStringList;
    auto GetPositionFilePathList(const int32_t& tileNumber)->QStringList;
    auto MergeToMap(const QStringList& processedDataFilePathList, const QStringList& positionFilePathList)->QMap<QString, QString>;
    auto FilterOverlapRelationSet(const OverlapRelationSet& overlapRelationSet, const TileConfiguration& tileConfiguration, const float& voxelSize)->OverlapRelationSet;
    auto ReadVoxelSizeXY(const QString& processedDataFilePath)->double;
};

auto StitcherHT::Impl::GetProcessedDataFilePathList(const int32_t& tileNumber, const int32_t& timeFrameIndex) -> QStringList {
    QStringList processedDataFilePathList{};
    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        const QString processedDataFilePath = 
            ResultFilePathGenerator::GetHTProcessedFilePath(this->rootFolderPath, tileIndex, timeFrameIndex);
        processedDataFilePathList.push_back(processedDataFilePath);
    }

    return processedDataFilePathList;
}

auto StitcherHT::Impl::GetPositionFilePathList(const int32_t& tileNumber) -> QStringList {
    QStringList positionFilePathList{};

    for (auto tileIndex = 0; tileIndex < tileNumber; ++tileIndex) {
        auto positionFilePath = QString("%1/data/%2/position.ini").arg(this->rootFolderPath).arg(tileIndex, 4, 10, QChar('0'));
        if (!QFile::exists(positionFilePath)) {
            positionFilePath = QString("%1/data/%2/position.ini").arg(this->rootFolderPath).arg(tileIndex);
        }
        positionFilePathList.push_back(positionFilePath);
    }

    return positionFilePathList;
}

auto StitcherHT::Impl::MergeToMap(const QStringList& processedDataFilePathList, 
    const QStringList& positionFilePathList) -> QMap<QString, QString> {
    const auto numberOfData = processedDataFilePathList.size();

    QMap<QString, QString> dataPositionFilePathMap{};
    for (auto index = 0; index < numberOfData; ++index) {
        dataPositionFilePathMap[processedDataFilePathList.at(index)] = positionFilePathList.at(index);
    }

    return dataPositionFilePathMap;
}

auto StitcherHT::Impl::FilterOverlapRelationSet(const OverlapRelationSet& overlapRelationSet, const TileConfiguration& tileConfiguration, const float& voxelSize) -> OverlapRelationSet {
    const auto tileNumberX = tileConfiguration.GetTileNumberX();
    const auto tileNumberY = tileConfiguration.GetTileNumberY();

    const auto overlapLengthX = tileConfiguration.GetOverlapLengthXInPixel();
    const auto overlapLengthY = tileConfiguration.GetOverlapLengthYInPixel();

    const auto overlapGridNumberX = 2 * tileNumberX - 1;
    const auto overlapGridNumberY = 2 * tileNumberY - 1;

    constexpr float filteringThresholdCorrelation = 0.7f;
    constexpr float filteringThresholdXShift = 6.f;
    constexpr float filteringThresholdYShift = 6.f;

    OverlapRelationSet filteredOverlapRelationSet;
    filteredOverlapRelationSet.SetTileNumber(tileNumberX, tileNumberY);
    for (auto overlapIndexX = 0; overlapIndexX < overlapGridNumberX; ++overlapIndexX) {
        for (auto overlapIndexY = 0; overlapIndexY < overlapGridNumberY; ++overlapIndexY) {
            if ((overlapIndexX + overlapIndexY) % 2 == 0) { continue; }
            const auto overlapRelation = overlapRelationSet.GetOverlapRelation(overlapIndexX, overlapIndexY);

            const auto reliability = overlapRelation.GetReliability();
            const auto shiftX = overlapRelation.GetShiftValueX();
            const auto shiftY = overlapRelation.GetShiftValueY();
            
            bool tooBigX{ false }, tooBigY{ false };

            //const auto tooWeakReliability = reliability < filteringThresholdCorrelation;
            if (overlapLengthX > 0) {
                tooBigX = std::abs(shiftX) * voxelSize >= filteringThresholdXShift;
            }

            if (overlapLengthY > 0) {
                tooBigY = std::abs(shiftY) * voxelSize >= filteringThresholdYShift;
            }

            //const auto filtered = tooWeakReliability || tooBigX || tooBigY;
            const auto filtered = tooBigX || tooBigY;

            OverlapRelation filteredOverlapRelation;
            if (filtered) {
                filteredOverlapRelation.SetReliability(0);
                filteredOverlapRelation.SetShiftValue(0, 0, 0);
            } else {
                filteredOverlapRelation = overlapRelation;
            }

            filteredOverlapRelationSet.InsertOverlapRelation(overlapIndexX, overlapIndexY, filteredOverlapRelation);
        }
    }

    return filteredOverlapRelationSet;
}

auto StitcherHT::Impl::ReadVoxelSizeXY(const QString& processedDataFilePath) -> double {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    float pixelWorldSizeX{};

    const H5::H5File file(processedDataFilePath.toStdString(), H5F_ACC_RDONLY);
    const auto dataSet = file.openDataSet("Data");
    const auto attrPixelWorldSizeX = dataSet.openAttribute("pixelWorldSizeX");
    attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);

    return pixelWorldSizeX;
}

StitcherHT::StitcherHT() : d(new Impl()) {
}

StitcherHT::~StitcherHT() = default;

auto StitcherHT::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto StitcherHT::SetTileNumber(const int32_t& tileNumberX, const int32_t& tileNumberY) -> void {
    d->tileNumberX = tileNumberX;
    d->tileNumberY = tileNumberY;
}

auto StitcherHT::SetLimitSize(const double& sizeXInMicrometer, const double& sizeYInMicrometer) -> void {
    d->limitedSizeXInMicrometer = sizeXInMicrometer;
    d->limitedSizeYInMicrometer = sizeYInMicrometer;

    d->sizeLimited = true;
}

auto StitcherHT::Stitch(const int32_t& timeIndex) -> bool {
    const auto tileNumber = d->tileNumberX * d->tileNumberY;
    const auto positionFilePathList = d->GetPositionFilePathList(tileNumber);

    QLOG_INFO() << "HT Stitcher time(" << timeIndex << ")";

    const auto processedDataFilePathList = d->GetProcessedDataFilePathList(tileNumber, timeIndex);

    TileConfigurationReaderHT tileConfigurationReaderHT;
    tileConfigurationReaderHT.SetPositionFileList(positionFilePathList);
    tileConfigurationReaderHT.SetProcessedFilePathList(processedDataFilePathList);

    if (!tileConfigurationReaderHT.Read()) { return false; }

    const auto tileConfiguration = tileConfigurationReaderHT.GetTileConfiguration();

    const auto dataPositionFilePathMap = d->MergeToMap(processedDataFilePathList, positionFilePathList);

    TileSetGeneratorHT tileSetGeneratorHT;
    tileSetGeneratorHT.SetTileConfiguration(tileConfiguration);
    tileSetGeneratorHT.SetDataPositionFilePathMap(dataPositionFilePathMap);
    tileSetGeneratorHT.Generate();

    const auto tileSet = tileSetGeneratorHT.GetTileSet();

    OverlapRelationCalculatorPhaseCorrelation overlapRelationCalculatorPhaseCorrelation;
    overlapRelationCalculatorPhaseCorrelation.SetTileConfiguration(tileConfiguration);
    overlapRelationCalculatorPhaseCorrelation.SetTileSet(tileSet);
    const auto overlapRelationSet = overlapRelationCalculatorPhaseCorrelation.Calculate();

    const QString htProcessedDataFilePath = ResultFilePathGenerator::GetHTProcessedFilePath(d->rootFolderPath, 0, timeIndex);
    const auto voxelSize = d->ReadVoxelSizeXY(htProcessedDataFilePath);

    const auto filteredOverlapRelationSet = d->FilterOverlapRelationSet(overlapRelationSet, tileConfiguration, voxelSize);

    TilePositionCalculatorBestOverlap tilePositionCalculatorBestOverlap;
    tilePositionCalculatorBestOverlap.SetTileConfiguration(tileConfiguration);
    tilePositionCalculatorBestOverlap.SetOverlapRelationSet(filteredOverlapRelationSet);
    const auto tilePositionSet = tilePositionCalculatorBestOverlap.Calculate();

    const auto stitchingResultFilePath = ResultFilePathGenerator::GetHTStitchingFilePath(d->rootFolderPath, timeIndex);

    StitchingWriterHDF5HT stitchingWriterHdf5HT;
    stitchingWriterHdf5HT.SetTileSet(tileSet);
    stitchingWriterHdf5HT.SetTilePositionSet(tilePositionSet);
    stitchingWriterHdf5HT.SetTileConfiguration(tileConfiguration);
    stitchingWriterHdf5HT.SetHDF5FilePath(stitchingResultFilePath);
    stitchingWriterHdf5HT.SetBoundaryCropFlag(true);
    if (d->sizeLimited == true) {
        const auto limitedSizeX = static_cast<int32_t>(std::round(d->limitedSizeXInMicrometer / voxelSize));
        const auto limitedSizeY = static_cast<int32_t>(std::round(d->limitedSizeYInMicrometer / voxelSize));

        stitchingWriterHdf5HT.SetDataSizeLimit(limitedSizeX, limitedSizeY);
    }
    if (!stitchingWriterHdf5HT.Run()) { return false; }
    
    return true;
}
