#define NOMINMAX
#define LOGGER_TAG "[MultiModalProcessor]"

#include "MultiModalProcessor.h"

#include "TCLogger.h"

#include "H5Cpp.h"

#include <iostream>
#include <QDir>
#include <QFile>
#include <QImageReader>
#include <QUuid>

#include <TcfH5GroupRoiLdmDataReader.h>

#include "AcquisitionConfig.h"
#include "AcquisitionConfigReader.h"
#include "Version.h"

#include "TCFCompleter.h"

#include "arrayfire.h"
#include "DataFolderPathGetter.h"
#include "ElapsedTimeReader.h"
#include "HDF5Mutex.h"
#include "MultiModalProcessorThread.h"
#include "ProcessorBF.h"
#include "ProcessorFL.h"
#include "ProcessorHT.h"
#include "ResultFilePathGenerator.h"
#include "StitcherBF.h"
#include "StitcherFL.h"
#include "StitcherFLStaged.h"
#include "StitcherHT.h"
#include "StitcherHTStaged.h"
#include "StitchingWriterBF.h"
#include "StitchingWriterFL.h"
#include "StitchingWriterHT.h"
#include "TCFBFCompleter.h"
#include "TCFBFDataSet.h"
#include "TCFFLCompleter.h"
#include "TCFHTCompleter.h"
#include "ThumbnailImageWriter.h"
#include "RecordingTimeReader.h"
#include "StitcherBFStaged.h"
#include "StitchingWriterBFLDM.h"
#include "StitchingWriterFLLDM.h"
#include "StitchingWriterHTLDM.h"
#include "TCFSkeletonConfig.h"
#include "TCFSkeletonWriter.h"
#include "TCFThumbnailCompleter.h"
#include "ThumbnailWriterBF.h"
#include "ThumbnailWriterFL.h"
#include "ThumbnailWriterHT.h"
#include "ProcessingOptionManager.h"

auto GetRecordingTime(const QString& rootFolderPath, const AcquisitionSequenceInfo& acquisitionSequenceInfo)
    -> QDateTime {
    RecordingTimeReader reader;
    
	if (acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::HT, 0)) {
        const QString recordedTimeFilePath = QString("%1/data/0000/HT3D/%2/acquisition_timestamp.txt")
            .arg(rootFolderPath).arg(0, 4, 10, QChar('0'));
        reader.SetFilePath(recordedTimeFilePath);

        if (reader.Read()) {
            return reader.GetRecordingTime();
        }
	}
	if (acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::FLCH0, 0)) {
        const QString recordedTimeFilePath = QString("%1/data/0000/FL3D/CH%2/%3/acquisition_timestamp.txt")
            .arg(rootFolderPath).arg(0).arg(0, 4, 10, QChar('0'));
        reader.SetFilePath(recordedTimeFilePath);

        if (reader.Read()) {
            return reader.GetRecordingTime();
        }
	}
	if (acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::FLCH1, 0)) {
        const QString recordedTimeFilePath = QString("%1/data/0000/FL3D/CH%2/%3/acquisition_timestamp.txt")
            .arg(rootFolderPath).arg(1).arg(0, 4, 10, QChar('0'));
        reader.SetFilePath(recordedTimeFilePath);

        if (reader.Read()) {
            return reader.GetRecordingTime();
        }
	}
	if (acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::FLCH2, 0)) {
        const QString recordedTimeFilePath = QString("%1/data/0000/FL3D/CH%2/%3/acquisition_timestamp.txt")
            .arg(rootFolderPath).arg(2).arg(0, 4, 10, QChar('0'));
        reader.SetFilePath(recordedTimeFilePath);

        if (reader.Read()) {
            return reader.GetRecordingTime();
        }
	}
	if (acquisitionSequenceInfo.AcquisitionExists(AcquisitionSequenceInfo::Modality::BF, 0)) {
        const QString recordedTimeFilePath = QString("%1/data/0000/BF/%2/acquisition_timestamp.txt")
            .arg(rootFolderPath).arg(0, 4, 10, QChar('0'));
        reader.SetFilePath(recordedTimeFilePath);

        if (reader.Read()) {
            return reader.GetRecordingTime();
        }
	}

    return QDateTime::currentDateTime();
}

auto GenerateUniqueID(const QString& deviceName, const QDateTime& dataRecordedDateTime,
    const QDateTime& dataWritingDateTime) -> QString {
    const auto dataRecordedDateTimeString = dataRecordedDateTime.toString("yyyy:MM:dd:HH:mm:ss:zzz");
    const auto dataWritingDateTimeString = dataWritingDateTime.toString("yyyy:MM:dd:HH:mm:ss:zzz");

    QString uuid = QUuid::createUuid().toString();
    uuid.replace(QRegExp("[\\{\\-\\}]"), "");

    const QStringList uniqueIDComponents{ deviceName, dataRecordedDateTimeString, dataWritingDateTimeString, uuid };

    return uniqueIDComponents.join("-");
}

auto GenerateDataID(const QString& deviceName, const QDateTime& dataRecordedDateTime) -> QString {
    const auto dataRecordedDateTimeString = dataRecordedDateTime.toString("yyyy:MM:dd:HH:mm:ss:zzz");
    const QStringList dataIDComponents{ deviceName, dataRecordedDateTimeString };

    return dataIDComponents.join("-");
}

auto CalculateOffsetZ(const AcquisitionConfig& acquisitionConfig)->double {
    const auto& deviceInfo = acquisitionConfig.GetDeviceInfo();
    const auto& acquisitionCount = acquisitionConfig.GetAcquisitionCount();
    const auto& acquisitionSetting = acquisitionConfig.GetAcquisitionSetting();
    const auto& acquisitionPosition = acquisitionConfig.GetAcquisitionPosition();

    const auto htAcquisitionZCount = acquisitionCount.ht3DZ;
    const auto zStepLength = acquisitionSetting.htZStepLengthMicrometer;
    const auto mediumRI = deviceInfo.mediumRI;

    const auto flAcquisitionZOffset = acquisitionPosition.flAcquisitionZOffsetMicrometer; // HT Center to FL Center Offset

    const auto htReconZLength = static_cast<double>(htAcquisitionZCount) * zStepLength * mediumRI;
    const auto flZOffset = flAcquisitionZOffset * mediumRI;

    const auto finalOffsetZ = htReconZLength / 2 + flZOffset; // HT Bottom To FL Center Offset

    return finalOffsetZ;
}

auto WriteMIP(const std::shared_ptr<float[]>& mipData, const int32_t& sizeX, const int32_t& sizeY, const QString& pngFilePath)->void {
    af::array mipArray(sizeX, sizeY, mipData.get());

    const auto minValue = af::min<float>(mipArray);
    const auto maxValue = af::max<float>(mipArray);

    mipArray = af::reorder(mipArray, 1, 0);
    mipArray = af::floor((mipArray - minValue) / (maxValue - minValue) * 255);
    mipArray = mipArray.as(u8);
    mipArray.eval();

    af::saveImageNative(pngFilePath.toStdString().c_str(), mipArray);
}

auto WriteBF(const std::shared_ptr<uint8_t[]>& bfData, const int32_t& sizeX, const int32_t& sizeY, const QString& pngFilePath)->void {
    af::array bfArray(sizeX, sizeY, 3, bfData.get());

    const double elementLimitation = std::pow(2, 32) - 1;
    const auto numberOfElements = static_cast<double>(sizeX) * static_cast<double>(sizeY) * 3;
    
    if (numberOfElements > elementLimitation) {
        const auto ratio = std::sqrt(elementLimitation / numberOfElements);
        const auto resizeX = static_cast<int32_t>(static_cast<double>(sizeX) * ratio);
        const auto resizeY = static_cast<int32_t>(static_cast<double>(sizeY) * ratio);

        af::array resizedBfArray(resizeX, resizeY, 3, u8);

        resizedBfArray(af::span, af::span, 0) = af::resize(bfArray(af::span, af::span, 0), resizeX, resizeY);
        resizedBfArray(af::span, af::span, 1) = af::resize(bfArray(af::span, af::span, 1), resizeX, resizeY);
        resizedBfArray(af::span, af::span, 2) = af::resize(bfArray(af::span, af::span, 2), resizeX, resizeY);
        resizedBfArray.eval();

        resizedBfArray = af::reorder(resizedBfArray, 1, 0, 2);
        af::saveImageNative(pngFilePath.toStdString().c_str(), resizedBfArray);
    } else {
        bfArray = af::reorder(bfArray, 1, 0, 2);
        af::saveImageNative(pngFilePath.toStdString().c_str(), bfArray);
    }
}

auto GetImageSizeXY(const QString& imageFolderPath) -> std::tuple<int32_t, int32_t> {
    const QDir pngFolderDir(imageFolderPath);
    const auto pngNameList = pngFolderDir.entryList(QStringList{ "*.png" }, QDir::Files);

    const auto firstPngFilePath = QString("%1/%2").arg(imageFolderPath).arg(pngNameList.first());

    const QImageReader reader(firstPngFilePath);
    const QSize sizeOfImage = reader.size();

    const auto sizeX = sizeOfImage.width();
    const auto sizeY = sizeOfImage.height();

    return { sizeX, sizeY };
}

struct MinMaxValue {
    double minValue{};
    double maxValue{};
};

auto ReadHTFirstMIPMinMaxValue(const QString& tcfFilePath) ->MinMaxValue {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    const H5::H5File file{ tcfFilePath.toStdString(), H5F_ACC_RDONLY };
    const auto dataGroup = file.openGroup("/Data/2DMIP");
    const auto dataSet = dataGroup.openDataSet("000000");

    const auto riMaxAttribute = dataSet.openAttribute("RIMax");
    const auto riMinAttribute = dataSet.openAttribute("RIMin");

    double riMax{}, riMin{};

    riMaxAttribute.read(H5::PredType::NATIVE_DOUBLE, &riMax);
    riMinAttribute.read(H5::PredType::NATIVE_DOUBLE, &riMin);

    return { riMin, riMax };
}

auto ReadHTMIPData(const QString& tcfTempFilePath, const int64_t& sizeX, const int64_t& sizeY)->std::shared_ptr<float[]> {
    using namespace TC::IO;

    HDF5MutexLocker locker{ HDF5Mutex::GetInstance() };
    const H5::H5File file{ tcfTempFilePath.toStdString(), H5F_ACC_RDONLY };
    const auto dataGroup = file.openGroup("/Data/2DMIP");

    const auto ldmOptionFlag = ProcessingOptionManager::GetInstance()->GetProcessingOptions().GetLDMFlag();

    std::shared_ptr<float[]> floatMipData{ new float[sizeX * sizeY]() };

    if (ldmOptionFlag) {
        auto ldmGroup = dataGroup.openGroup("000000");

        const auto sizeXAttr = ldmGroup.openAttribute("DataSizeX");
        const auto sizeYAttr = ldmGroup.openAttribute("DataSizeY");

        const DataRange roiDataRange{ Point{0,0}, Point{sizeX - 1, sizeY - 1} };

        using namespace TC::IO::LdmReading;
        TcfH5GroupRoiLdmDataReader ldmDataReader;
        ldmDataReader.SetLdmDataGroup(ldmGroup);
        ldmDataReader.SetReadingRoi(roiDataRange);
        ldmDataReader.SetSamplingStep(1);
        ldmDataReader.SetLdmDataIs2DStack(false);

        const auto mipData = ldmDataReader.Read();

        if (mipData->GetDataType() == +ChunkDataType::UInt16Type) {
            const auto srcData = mipData->GetDataPointerAs<uint16_t>();
            for (auto index = 0; index < sizeX * sizeY; ++index) {
                floatMipData[index] = static_cast<float>(srcData[index]);
            }
        } else {
            const auto srcData = mipData->GetDataPointerAs<uint8_t>();
            for (auto index = 0; index < sizeX * sizeY; ++index) {
                floatMipData[index] = static_cast<float>(srcData[index]);
            }
        }
    } else {
        const auto dataSet = dataGroup.openDataSet("000000");

        if (dataSet.getDataType() == H5::PredType::NATIVE_UINT16) {
            std::shared_ptr<uint16_t[]> mipData{ new uint16_t[sizeX * sizeY]() };
            dataSet.read(mipData.get(), H5::PredType::NATIVE_UINT16);

            for (auto index = 0; index < sizeX * sizeY; ++index) {
                floatMipData.get()[index] = static_cast<float>(mipData.get()[index]) / 10000;
            }
        } else if (dataSet.getDataType() == H5::PredType::NATIVE_UINT8) {
            locker.Unlock();
            const auto [minValue, maxValue] = ReadHTFirstMIPMinMaxValue(tcfTempFilePath);

            std::shared_ptr<uint8_t[]> mipData{ new uint8_t[sizeX * sizeY]() };
            dataSet.read(mipData.get(), H5::PredType::NATIVE_UINT8);

            for (auto index = 0; index < sizeX * sizeY; ++index) {
                floatMipData.get()[index] = static_cast<float>(mipData.get()[index]) / 1000 + minValue;
            }
        }
    }

    return floatMipData;
}

auto ReadBFData(const QString& tcfTempFilePath, const int64_t& sizeX, const int64_t& sizeY)->std::shared_ptr<uint8_t[]> {
    using namespace TC::IO;
    HDF5MutexLocker locker{ HDF5Mutex::GetInstance() };
    const H5::H5File file{ tcfTempFilePath.toStdString(), H5F_ACC_RDONLY };
    const auto dataGroup = file.openGroup("/Data/BF");

    const auto ldmOptionFlag = ProcessingOptionManager::GetInstance()->GetProcessingOptions().GetLDMFlag();

    std::shared_ptr<uint8_t[]> bfData{ new uint8_t[sizeX * sizeY * 3]() };
    
    if (ldmOptionFlag) {
        auto ldmGroup = dataGroup.openGroup("000000");
        const DataRange roiDataRange{ Point{0,0,0}, Point{sizeX - 1, sizeY - 1, 2} };

        using namespace TC::IO::LdmReading;
        TcfH5GroupRoiLdmDataReader ldmDataReader;
        ldmDataReader.SetLdmDataGroup(ldmGroup);
        ldmDataReader.SetReadingRoi(roiDataRange);
        ldmDataReader.SetSamplingStep(1);
        ldmDataReader.SetLdmDataIs2DStack(true);

        const auto bfDataChunk = ldmDataReader.Read();
        const auto numberOfElements = bfDataChunk->GetDimension().GetNumberOfElements();

        std::copy_n(bfDataChunk->GetDataPointerAs<uint8_t>(), numberOfElements, bfData.get());
    } else {
        const auto dataSet = dataGroup.openDataSet("000000");

        dataSet.read(bfData.get(), H5::PredType::NATIVE_UINT8);
    }

    return bfData;
}

struct DataSize {
    int64_t sizeX{};
    int64_t sizeY{};
    int64_t sizeZ{};
};

auto ReadHTDataSize(const QString& tcfFilePath)->DataSize {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    const H5::H5File file{ tcfFilePath.toStdString(), H5F_ACC_RDONLY };
    const auto dataGroup = file.openGroup("/Data/3D");

    int64_t sizeX, sizeY, sizeZ;

    const auto ldmOptionFlag = ProcessingOptionManager::GetInstance()->GetProcessingOptions().GetLDMFlag();

    if (ldmOptionFlag) {
        const auto timeFrameGroup = dataGroup.openGroup("000000");

        const auto sizeXAttr = timeFrameGroup.openAttribute("DataSizeX");
        const auto sizeYAttr = timeFrameGroup.openAttribute("DataSizeY");
        const auto sizeZAttr = timeFrameGroup.openAttribute("DataSizeZ");

        sizeXAttr.read(H5::PredType::NATIVE_UINT64, &sizeX);
        sizeYAttr.read(H5::PredType::NATIVE_UINT64, &sizeY);
        sizeZAttr.read(H5::PredType::NATIVE_UINT64, &sizeZ);
    } else {
        const auto dataSet = dataGroup.openDataSet("000000");
        const auto dataSetSpace = dataSet.getSpace();

        hsize_t dims[3];
        dataSetSpace.getSimpleExtentDims(dims);

        sizeX = static_cast<int64_t>(dims[2]);
        sizeY = static_cast<int64_t>(dims[1]);
        sizeZ = static_cast<int64_t>(dims[0]);
    }

    return { sizeX, sizeY, sizeZ };
}

auto ReadFLDataSize(const QString& tcfFilePath, const int32_t& flChannelIndex)->DataSize {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    const H5::H5File file{ tcfFilePath.toStdString(), H5F_ACC_RDONLY };
    const auto dataGroup = file.openGroup("/Data/3DFL");
    const auto channelGroup = dataGroup.openGroup(QString("CH%1").arg(flChannelIndex).toStdString());

    int64_t sizeX, sizeY, sizeZ;
    
    const auto ldmOptionFlag = ProcessingOptionManager::GetInstance()->GetProcessingOptions().GetLDMFlag();

    if (ldmOptionFlag) {
        const auto timeFrameGroup = channelGroup.openGroup("000000");

        const auto sizeXAttr = timeFrameGroup.openAttribute("DataSizeX");
        const auto sizeYAttr = timeFrameGroup.openAttribute("DataSizeY");
        const auto sizeZAttr = timeFrameGroup.openAttribute("DataSizeZ");

        sizeXAttr.read(H5::PredType::NATIVE_UINT64, &sizeX);
        sizeYAttr.read(H5::PredType::NATIVE_UINT64, &sizeY);
        sizeZAttr.read(H5::PredType::NATIVE_UINT64, &sizeZ);
    } else {
        const auto dataSet = channelGroup.openDataSet("000000");
        const auto dataSetSpace = dataSet.getSpace();

        hsize_t dims[3];
        dataSetSpace.getSimpleExtentDims(dims);

        sizeX = static_cast<int64_t>(dims[2]);
        sizeY = static_cast<int64_t>(dims[1]);
        sizeZ = static_cast<int64_t>(dims[0]);
    }

    return { sizeX, sizeY, sizeZ };
}

auto ReadBFDataSize(const QString& tcfFilePath)->DataSize {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };

    const H5::H5File file{ tcfFilePath.toStdString(), H5F_ACC_RDONLY };
    const auto dataGroup = file.openGroup("/Data/BF");

    int64_t sizeX, sizeY, sizeZ;

    const auto ldmOptionFlag = ProcessingOptionManager::GetInstance()->GetProcessingOptions().GetLDMFlag();

    if (ldmOptionFlag) {
        const auto timeFrameGroup = dataGroup.openGroup("000000");

        const auto sizeXAttr = timeFrameGroup.openAttribute("DataSizeX");
        const auto sizeYAttr = timeFrameGroup.openAttribute("DataSizeY");
        const auto sizeZAttr = timeFrameGroup.openAttribute("DataSizeZ");

        sizeXAttr.read(H5::PredType::NATIVE_UINT64, &sizeX);
        sizeYAttr.read(H5::PredType::NATIVE_UINT64, &sizeY);
        sizeZAttr.read(H5::PredType::NATIVE_UINT64, &sizeZ);
    } else {
        const auto dataSet = dataGroup.openDataSet("000000");
        const auto dataSetSpace = dataSet.getSpace();

        hsize_t dims[3];
        dataSetSpace.getSimpleExtentDims(dims);

        sizeX = static_cast<int64_t>(dims[2]);
        sizeY = static_cast<int64_t>(dims[1]);
        sizeZ = static_cast<int64_t>(dims[0]);
    }
    
    return { sizeX, sizeY, sizeZ };
}

struct WorldSize {
    double sizeX{};
    double sizeY{};
    double sizeZ{};
};

auto ReadWorldSize(const QString& hdf5FilePath)->WorldSize {
    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    float pixelWorldSizeX{}, pixelWorldSizeY{}, pixelWorldSizeZ{};

    const H5::H5File processedDataFile(hdf5FilePath.toStdString(), H5F_ACC_RDONLY);
    const auto processedDataSet = processedDataFile.openDataSet("Data");

    const auto attrPixelWorldSizeX = processedDataSet.openAttribute("pixelWorldSizeX");
    const auto attrPixelWorldSizeY = processedDataSet.openAttribute("pixelWorldSizeY");

    attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
    attrPixelWorldSizeY.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeY);


    if(processedDataSet.attrExists("pixelWorldSizeZ")) {
        const auto attrPixelWorldSizeZ = processedDataSet.openAttribute("pixelWorldSizeZ");
        attrPixelWorldSizeZ.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeZ);
    } else {
        pixelWorldSizeZ = 0;
    }
    
    return { pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ };
}


class MultiModalProcessor::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    static auto ReadAcquisitionConfig(const QString& rootFolderPath)->AcquisitionConfig;
    static auto InsertInfoToInvalidConfigValues(const QString& rootFolderPath, const QString& backgroundFolderPath,
        const AcquisitionConfig& config)->AcquisitionConfig;
    static auto GetExistDataFolderPath(const QString& rootFolderPath)->QString;
    auto GetSampleImageCropFlag(const QString& rootPath, const QString& backgroundFolderNAPath)->bool;

    auto GetChannelColorMap()->QMap<TCFFLCompleter::ChannelIndex, TCFFLCompleter::RGB>;
    auto GetWaveLengthMap()->QMap<TCFFLCompleter::ChannelIndex, TCFFLCompleter::EmissionExcitationWaveLength>;
    auto GetIntensityMap()->QMap<TCFFLCompleter::ChannelIndex, int32_t>;
    auto GetExposureTimeMap()->QMap<TCFFLCompleter::ChannelIndex, int32_t>;
    auto SelectBackgroundFolderPath(const double& condenserNA)->QString;
    auto WriteTempFile(const AcquisitionSequenceInfo& acquisitionSequenceInfo)->void;

    QString softwareVersion{ QString("%1").arg(QString(PROJECT_REVISION)) };
    int32_t bestFocusZIndex{0};

    QString psfFolderPath{};
    QString systemBackgroundFolderPath{};

    QString matlabModuleFolderPath{};

    AcquisitionConfig acquisitionConfig;
    AcquisitionSequenceInfo acquisitionSequenceInfo;
    QString rootFolderPath{};
};

auto MultiModalProcessor::Impl::ReadAcquisitionConfig(const QString& rootFolderPath) -> AcquisitionConfig {
    const auto configFilePath = QString("%1/config.dat").arg(rootFolderPath);

    AcquisitionConfigReader reader;
    reader.SetFilePath(configFilePath);
    reader.Read();

    return reader.GetAcquisitionConfig();
}

auto MultiModalProcessor::Impl::InsertInfoToInvalidConfigValues(const QString& rootFolderPath, 
    const QString& backgroundFolderPath, const AcquisitionConfig& config) -> AcquisitionConfig {
    auto validAcquisitionConfig = config;

    if (!config.IsTileInfoValid()) {
        AcquisitionConfig::TileInfo tileInfo;
        tileInfo.tileNumberX = 1;
        tileInfo.tileNumberY = 1;

        validAcquisitionConfig.SetTileInfo(tileInfo);
    }

    const auto dataFolderPath = GetExistDataFolderPath(rootFolderPath);

    if (!config.IsImageInfoValid()) {
        if (config.GetAcquisitionCount().ht3D > 0) {
            const auto [sampleImageSizeX, sampleImageSizeY] = GetImageSizeXY(dataFolderPath);
            const auto [backgroundImageSizeX, backgroundImageSizeY] = GetImageSizeXY(backgroundFolderPath);

            const auto squareSize = std::min(sampleImageSizeX, sampleImageSizeY);

            AcquisitionConfig::ImageInfo imageInfo;
            imageInfo.roiSizeXPixels = squareSize;
            imageInfo.roiSizeYPixels = squareSize;
            imageInfo.roiOffsetXPixels = ((backgroundImageSizeX / 2)) - (squareSize / 2);
            imageInfo.roiOffsetYPixels = ((backgroundImageSizeY / 2)) - (squareSize / 2);

            validAcquisitionConfig.SetImageInfo(imageInfo);
        }
    } else {
        if (config.GetAcquisitionCount().ht3D > 0) {
            const auto [sampleImageSizeX, sampleImageSizeY] = GetImageSizeXY(dataFolderPath);
            const auto [backgroundImageSizeX, backgroundImageSizeY] = GetImageSizeXY(backgroundFolderPath);

            const auto sampleSquare = sampleImageSizeX == sampleImageSizeY;
            const auto backgroundSquare = backgroundImageSizeX == backgroundImageSizeY;

            if (sampleSquare && backgroundSquare) {
                const auto sameSize = sampleImageSizeX == sampleImageSizeY;

                if (sameSize) {
                    const auto squareSize = sampleImageSizeX;

                    AcquisitionConfig::ImageInfo imageInfo;
                    imageInfo.roiSizeXPixels = squareSize;
                    imageInfo.roiSizeYPixels = squareSize;
                    imageInfo.roiOffsetXPixels = 0;
                    imageInfo.roiOffsetYPixels = 0;

                    validAcquisitionConfig.SetImageInfo(imageInfo);
                }
            }
        }
    }

    return validAcquisitionConfig;
}

auto MultiModalProcessor::Impl::GetExistDataFolderPath(const QString& rootFolderPath) -> QString {
    DataFolderPathGetter dataFolderPathGetter;
    dataFolderPathGetter.SetRootFolderPath(rootFolderPath);
    dataFolderPathGetter.SetTileIndex(0);
    dataFolderPathGetter.SetTimeIndex(0);

    dataFolderPathGetter.SetModalityType(ModalityType{ ModalityType::Name::HT });
    if (dataFolderPathGetter.ExistTimeFrameFolder()) {
        return dataFolderPathGetter.GetTimeFrameFolderPath();
    }
    dataFolderPathGetter.SetModalityType({ ModalityType::Name::FL,0 });
    if (dataFolderPathGetter.ExistTimeFrameFolder()) {
        return dataFolderPathGetter.GetTimeFrameFolderPath();
    }
    dataFolderPathGetter.SetModalityType({ ModalityType::Name::FL,1 });
    if (dataFolderPathGetter.ExistTimeFrameFolder()) {
        return dataFolderPathGetter.GetTimeFrameFolderPath();
    }
    dataFolderPathGetter.SetModalityType({ ModalityType::Name::FL,2 });
    if (dataFolderPathGetter.ExistTimeFrameFolder()) {
        return dataFolderPathGetter.GetTimeFrameFolderPath();
    }
    dataFolderPathGetter.SetModalityType(ModalityType{ ModalityType::Name::BF });
    if (dataFolderPathGetter.ExistTimeFrameFolder()) {
        return dataFolderPathGetter.GetTimeFrameFolderPath();
    }

    return "";
}

auto MultiModalProcessor::Impl::GetSampleImageCropFlag(const QString& rootPath, const QString& backgroundFolderNAPath) -> bool {
    const auto dataFolderPath = GetExistDataFolderPath(rootFolderPath);
    
    if (this->acquisitionConfig.GetAcquisitionCount().ht3D > 0) {
        const auto [sampleImageSizeX, sampleImageSizeY] = GetImageSizeXY(dataFolderPath);
        const auto [backgroundImageSizeX, backgroundImageSizeY] = GetImageSizeXY(backgroundFolderNAPath);

        if ((sampleImageSizeX == backgroundImageSizeX) && (sampleImageSizeY == backgroundImageSizeY)) {
            if (sampleImageSizeX != sampleImageSizeY) {
                return true;
            }
        }
    } 

    return false;
}

auto MultiModalProcessor::Impl::GetChannelColorMap() -> QMap<TCFFLCompleter::ChannelIndex, TCFFLCompleter::RGB> {
    auto flCH0Color = this->acquisitionConfig.GetAcquisitionSetting().flCH0Color;
    auto flCH1Color = this->acquisitionConfig.GetAcquisitionSetting().flCH1Color;
    auto flCH2Color = this->acquisitionConfig.GetAcquisitionSetting().flCH2Color;

    QMap<TCFFLCompleter::ChannelIndex, TCFFLCompleter::RGB> channelColorMap;
    if ((flCH0Color.r == 0) && (flCH0Color.g == 0) && (flCH0Color.b == 0)) {
        flCH0Color.r = 255;
        flCH0Color.g = 255;
        flCH0Color.b = 255;
    } 
    if ((flCH1Color.r == 0) && (flCH1Color.g == 0) && (flCH1Color.b == 0)) {
        flCH1Color.r = 255;
        flCH1Color.g = 255;
        flCH1Color.b = 255;
    }
    if ((flCH2Color.r == 0) && (flCH2Color.g == 0) && (flCH2Color.b == 0)) {
        flCH2Color.r = 255;
        flCH2Color.g = 255;
        flCH2Color.b = 255;
    }

    channelColorMap[0] = { flCH0Color.r, flCH0Color.g, flCH0Color.b };
    channelColorMap[1] = { flCH1Color.r, flCH1Color.g, flCH1Color.b };
    channelColorMap[2] = { flCH2Color.r, flCH2Color.g, flCH2Color.b };

    return channelColorMap;
}

auto MultiModalProcessor::Impl::GetWaveLengthMap()
    -> QMap<TCFFLCompleter::ChannelIndex, TCFFLCompleter::EmissionExcitationWaveLength> {
    auto ch0Emission = this->acquisitionConfig.GetAcquisitionSetting().flCH0EmissionWaveLengthMicrometer;
    auto ch0Excitation = this->acquisitionConfig.GetAcquisitionSetting().flCH0ExcitationWaveLengthMicrometer;
    auto ch1Emission = this->acquisitionConfig.GetAcquisitionSetting().flCH1EmissionWaveLengthMicrometer;
    auto ch1Excitation = this->acquisitionConfig.GetAcquisitionSetting().flCH1ExcitationWaveLengthMicrometer;
    auto ch2Emission = this->acquisitionConfig.GetAcquisitionSetting().flCH2EmissionWaveLengthMicrometer;
    auto ch2Excitation = this->acquisitionConfig.GetAcquisitionSetting().flCH2ExcitationWaveLengthMicrometer;

    QMap<TCFFLCompleter::ChannelIndex, TCFFLCompleter::EmissionExcitationWaveLength> waveLengthMap;
    waveLengthMap[0] = { ch0Emission, ch0Excitation };
    waveLengthMap[1] = { ch1Emission, ch1Excitation };
    waveLengthMap[2] = { ch2Emission, ch2Excitation };

    return waveLengthMap;
}

auto MultiModalProcessor::Impl::GetIntensityMap() -> QMap<TCFFLCompleter::ChannelIndex, int32_t> {
    const auto ch0Intensity = this->acquisitionConfig.GetAcquisitionSetting().flCH0Intensity;
    const auto ch1Intensity = this->acquisitionConfig.GetAcquisitionSetting().flCH1Intensity;
    const auto ch2Intensity = this->acquisitionConfig.GetAcquisitionSetting().flCH2Intensity;

    QMap<TCFFLCompleter::ChannelIndex, int32_t> intensityMap;
    intensityMap[0] = ch0Intensity;
    intensityMap[1] = ch1Intensity;
    intensityMap[2] = ch2Intensity;

    return intensityMap;
}

auto MultiModalProcessor::Impl::GetExposureTimeMap() -> QMap<TCFFLCompleter::ChannelIndex, int32_t> {
    const auto ch0ExposureTimeMillisecond = this->acquisitionConfig.GetAcquisitionSetting().flCH0ExposureTimeMillisecond;
    const auto ch1ExposureTimeMillisecond = this->acquisitionConfig.GetAcquisitionSetting().flCH1ExposureTimeMillisecond;
    const auto ch2ExposureTimeMillisecond = this->acquisitionConfig.GetAcquisitionSetting().flCH2ExposureTimeMillisecond;

    QMap<TCFFLCompleter::ChannelIndex, int32_t> exposureTimeMap;
    exposureTimeMap[0] = ch0ExposureTimeMillisecond;
    exposureTimeMap[1] = ch1ExposureTimeMillisecond;
    exposureTimeMap[2] = ch2ExposureTimeMillisecond;

    return exposureTimeMap;
}

auto MultiModalProcessor::Impl::SelectBackgroundFolderPath(const double& condenserNA) -> QString {
    const auto backgroundFolderPathInRootFolder = QString("%1/bgImages").arg(this->rootFolderPath);

    if (QDir().exists(backgroundFolderPathInRootFolder)) {
        return backgroundFolderPathInRootFolder;
    }
    else {
        const QString backgroundNAFolderPath = QString("%1/%2").arg(this->systemBackgroundFolderPath).arg(condenserNA, 0, 'f', 2);
        return backgroundNAFolderPath;
    }
}

auto MultiModalProcessor::Impl::WriteTempFile(const AcquisitionSequenceInfo& acquisitionSequenceInfo) -> void {
    using SequenceModality = AcquisitionSequenceInfo::Modality;
    const auto tempFilePath = ResultFilePathGenerator::GetTempFilePath(this->rootFolderPath);

    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
    if (QFile::exists(tempFilePath)) { return; }

    bool htExists{ false }, flExists{ false }, bfExists{ false };


    const auto timeFrameCount = acquisitionSequenceInfo.GetTimeFrameCount();

    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        const auto htZSliceCount = acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::HT, timeIndex);
        const auto flCH0ZSliceCount = acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::FLCH0, timeIndex);
        const auto flCH1ZSliceCount = acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::FLCH1, timeIndex);
        const auto flCH2ZSliceCount = acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::FLCH2, timeIndex);
        const auto bfZSliceCount = acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::BF, timeIndex);

        htExists |= (htZSliceCount > 0);
        flExists |= (flCH0ZSliceCount > 0) || (flCH1ZSliceCount > 0) || (flCH2ZSliceCount > 0);
        bfExists |= (bfZSliceCount > 0);
    }

    TCFSkeletonConfig skeletonConfig;
    skeletonConfig.SetDataIncludingFlag(htExists, flExists, bfExists);

    TCFSkeletonWriter skeletonWriter;
    skeletonWriter.SetConfig(skeletonConfig);
    skeletonWriter.SetTargetFilePath(tempFilePath);
    skeletonWriter.Write();
}

MultiModalProcessor::MultiModalProcessor()
    : d(new Impl()) {
}

MultiModalProcessor::~MultiModalProcessor() = default;

auto MultiModalProcessor::Initialize() -> bool {
    const auto acquisitionConfig = d->ReadAcquisitionConfig(d->rootFolderPath);
    const auto backgroundFolderPath = d->SelectBackgroundFolderPath(acquisitionConfig.GetDeviceInfo().condenserNA);

    d->acquisitionConfig = d->InsertInfoToInvalidConfigValues(d->rootFolderPath, backgroundFolderPath, acquisitionConfig);

    d->WriteTempFile(d->acquisitionSequenceInfo);
    return true;
}

auto MultiModalProcessor::HTProcess(const int32_t& tileIndex, const int32_t& timeIndex) -> bool {
    const auto condenserNA = d->acquisitionConfig.GetDeviceInfo().condenserNA;
    const auto backgroundFolderPath = d->SelectBackgroundFolderPath(condenserNA);

    ProcessorHT processorHT;
    processorHT.SetRootFolderPath(d->rootFolderPath);
    processorHT.SetBackgroundFolderPath(backgroundFolderPath);
    processorHT.SetMatlabModuleFolderPath(d->matlabModuleFolderPath);
    processorHT.SetPsfFolderPath(d->psfFolderPath);
    processorHT.SetAcquisitionConfig(d->acquisitionConfig);
    processorHT.SetSampleImageCropFlag(d->GetSampleImageCropFlag(d->rootFolderPath, backgroundFolderPath));

    const auto processingResult = processorHT.Process(tileIndex, timeIndex);
    if (!processingResult) {
        QLOG_ERROR() << "processorHT.Process() fails";
        return false;
    }

    return true;
}

auto MultiModalProcessor::FLProcess(const int32_t& flChannelIndex, const int32_t& tileIndex, const int32_t& timeIndex)
    -> bool {
    ProcessorFL processorFL;
    processorFL.SetRootFolderPath(d->rootFolderPath);
    processorFL.SetAcquisitionConfig(d->acquisitionConfig);

    const auto processingResult = processorFL.Process(flChannelIndex, tileIndex, timeIndex);
    if (!processingResult) {
        QLOG_ERROR() << "processorFL.Process() fails";
        return false;
    }
    return true;
}

auto MultiModalProcessor::BFProcess(const int32_t& tileIndex, const int32_t& timeIndex) -> bool {
    ProcessorBF processorBF;
    processorBF.SetRootFolderPath(d->rootFolderPath);
    processorBF.SetAcquisitionConfig(d->acquisitionConfig);

    const auto processingResult = processorBF.Process(tileIndex, timeIndex);
    if (!processingResult) {
        QLOG_ERROR() << "processorBF.Process() fails";
        return false;
    }
    return true;
}

auto MultiModalProcessor::HTStitch(const int32_t& timeIndex) -> bool {
    const auto tileInfo = d->acquisitionConfig.GetTileInfo();

    const auto& tileNumberX = tileInfo.tileNumberX;
    const auto& tileNumberY = tileInfo.tileNumberY;

    //const bool stagedHasEncoder = d->acquisitionConfig.GetDeviceInfo().sampleStageEncoderSupported;
    constexpr bool stagedHasEncoder = false; // temporally false;

    IStitcherHT::Pointer stitcherHT;
    if (stagedHasEncoder) {
        stitcherHT = IStitcherHT::Pointer{ new StitcherHTStaged };
    } else {
        stitcherHT = IStitcherHT::Pointer{ new StitcherHT };
    }

    stitcherHT->SetRootFolderPath(d->rootFolderPath);
    stitcherHT->SetTileNumber(tileNumberX, tileNumberY);

    if (tileNumberX * tileNumberY != 1) {
        if (d->acquisitionConfig.IsAcquisitionSizeValid()) {
            const auto limitSizeXInMicrometer = d->acquisitionConfig.GetAcquisitionSize().acquisitionSizeXMicrometer;
            const auto limitSizeYInMicrometer = d->acquisitionConfig.GetAcquisitionSize().acquisitionSizeYMicrometer;
            stitcherHT->SetLimitSize(limitSizeXInMicrometer, limitSizeYInMicrometer);
        }
    }

    const auto stitchingResult = stitcherHT->Stitch(timeIndex);
    if (!stitchingResult) {
        if (stagedHasEncoder) {
            QLOG_ERROR() << "stitcherHTStaged.Stitch() fails";
        } else {
            QLOG_ERROR() << "stitcherHT.Stitch() fails";
        }
        return false;
    }

    return true;
}

auto MultiModalProcessor::FLStitch(const int32_t& flChannelIndex, const int32_t& timeIndex) -> bool {
    const auto tileInfo = d->acquisitionConfig.GetTileInfo();

    const auto& tileNumberX = tileInfo.tileNumberX;
    const auto& tileNumberY = tileInfo.tileNumberY;

    //const bool stagedHasEncoder = d->acquisitionConfig.GetDeviceInfo().sampleStageEncoderSupported;
    constexpr bool stagedHasEncoder = false; // temporally false;

    IStitcherFL::Pointer stitcherFL;
    if (stagedHasEncoder) {
        stitcherFL = IStitcherFL::Pointer{ new StitcherFLStaged };
    } else {
        stitcherFL = IStitcherFL::Pointer{ new StitcherFL };
    }

    stitcherFL->SetRootFolderPath(d->rootFolderPath);
    stitcherFL->SetChannelIndex(flChannelIndex);
    stitcherFL->SetTileNumber(tileNumberX, tileNumberY);

    if (tileNumberX * tileNumberY != 1) {
        if (d->acquisitionConfig.IsAcquisitionSizeValid()) {
            const auto limitSizeXInMicrometer = d->acquisitionConfig.GetAcquisitionSize().acquisitionSizeXMicrometer;
            const auto limitSizeYInMicrometer = d->acquisitionConfig.GetAcquisitionSize().acquisitionSizeYMicrometer;
            stitcherFL->SetLimitSize(limitSizeXInMicrometer, limitSizeYInMicrometer);
        }
    }

    const auto stitchingResult = stitcherFL->Stitch(timeIndex);
    if (!stitchingResult) {
        if (stagedHasEncoder) {
            QLOG_ERROR() << "stitcherFLStaged.Stitch() fails";
        } else {
            QLOG_ERROR() << "stitcherFL.Stitch() fails";
        }
        return false;
    }

    return true;
}

auto MultiModalProcessor::BFStitch(const int32_t& timeIndex) -> bool {
    const auto tileInfo = d->acquisitionConfig.GetTileInfo();

    const auto& tileNumberX = tileInfo.tileNumberX;
    const auto& tileNumberY = tileInfo.tileNumberY;

    //const bool stagedHasEncoder = d->acquisitionConfig.GetDeviceInfo().sampleStageEncoderSupported;
    constexpr bool stagedHasEncoder = false; // temporally false;

    IStitcherBF::Pointer stitcherBF;
    if (stagedHasEncoder) {
        stitcherBF = IStitcherBF::Pointer{ new StitcherBFStaged };
    } else {
        stitcherBF = IStitcherBF::Pointer{ new StitcherBF };
    }

    stitcherBF->SetRootFolderPath(d->rootFolderPath);
    stitcherBF->SetTileNumber(tileNumberX, tileNumberY);

    if (tileNumberX * tileNumberY != 1) {
        if (d->acquisitionConfig.IsAcquisitionSizeValid()) {
            const auto limitSizeXInMicrometer = d->acquisitionConfig.GetAcquisitionSize().acquisitionSizeXMicrometer;
            const auto limitSizeYInMicrometer = d->acquisitionConfig.GetAcquisitionSize().acquisitionSizeYMicrometer;
            stitcherBF->SetLimitSize(limitSizeXInMicrometer, limitSizeYInMicrometer);
        }
    }

    const auto stitchingResult = stitcherBF->Stitch(timeIndex);
    if (!stitchingResult) {
        if (stagedHasEncoder) {
            QLOG_ERROR() << "stitcherBFStaged.stitch() fails";
        } else {
            QLOG_ERROR() << "stitcherBF.stitch() fails";
        }
        
        return false;
    }

    return true;
}

auto MultiModalProcessor::HTWriteThumbnail(const int32_t& timeIndex, const AcquisitionSequenceInfo& acquisitionSequenceInfo) const -> bool {
    const auto tempFilePath = ResultFilePathGenerator::GetTempFilePath(d->rootFolderPath);

    ThumbnailWriterHT thumbnailWriterHT;
    thumbnailWriterHT.SetTimeFrameIndex(timeIndex);
    thumbnailWriterHT.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);
    thumbnailWriterHT.SetRootFolderPath(d->rootFolderPath);
    thumbnailWriterHT.SetTempTCFFilePath(tempFilePath);

    if (!thumbnailWriterHT.Write()) {
        QLOG_ERROR() << "thumbnailWriterHT.Write() fails";
        return false;
    }

    const auto [thumbnailData, thumbnailSizeX, thumbnailSizeY] = thumbnailWriterHT.GetThumbnailData();

    ThumbnailImageWriter thumbnailImageWriter;
    thumbnailImageWriter.SetRootFolderPath(d->rootFolderPath);
    if (!thumbnailImageWriter.WriteHT(thumbnailData, thumbnailSizeX, thumbnailSizeY, timeIndex)) {
        QLOG_ERROR() << "thumbnailImageWriter.WriteHT() fails";
        return false;
    }

    return true;
}

auto MultiModalProcessor::FLWriteThumbnail(const QList<int32_t>& flChannelIndexList, const int32_t& timeIndex,
    const AcquisitionSequenceInfo& acquisitionSequenceInfo) const -> bool {
    const auto tempFilePath = ResultFilePathGenerator::GetTempFilePath(d->rootFolderPath);

    ThumbnailWriterFL thumbnailWriterFL;
    thumbnailWriterFL.SetTimeFrameIndex(timeIndex);
    thumbnailWriterFL.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);
    thumbnailWriterFL.SetRootFolderPath(d->rootFolderPath);
    thumbnailWriterFL.SetTempTCFFilePath(tempFilePath);
    thumbnailWriterFL.SetAcquisitionConfig(d->acquisitionConfig);
    thumbnailWriterFL.SetFLChannelIndexList(flChannelIndexList);

    if (!thumbnailWriterFL.Write()) {
        QLOG_ERROR() << "thumbnailWriterFL.Write() fails";
        return false;
    }

    const auto [thumbnailData, thumbnailDataSizeX, thumbnailDataSizeY] = thumbnailWriterFL.GetThumbnailData();

    ThumbnailImageWriter thumbnailImageWriter;
    thumbnailImageWriter.SetRootFolderPath(d->rootFolderPath);
    if (!thumbnailImageWriter.WriteFL(thumbnailData, thumbnailDataSizeX, thumbnailDataSizeY, timeIndex)) {
        QLOG_ERROR() << "ThumbnailImageWriter.WriteFL() fails";
        return false;
    }

    return true;
}

auto MultiModalProcessor::BFWriteThumbnail(const int32_t& timeIndex, const AcquisitionSequenceInfo& acquisitionSequenceInfo) const -> bool {
    const auto tempFilePath = ResultFilePathGenerator::GetTempFilePath(d->rootFolderPath);

    ThumbnailWriterBF thumbnailWriterBF;
    thumbnailWriterBF.SetTimeFrameIndex(timeIndex);
    thumbnailWriterBF.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);
    thumbnailWriterBF.SetRootFolderPath(d->rootFolderPath);
    thumbnailWriterBF.SetTempTCFFilePath(tempFilePath);

    if (!thumbnailWriterBF.Write()) {
        QLOG_ERROR() << "thumbnailWriterBF.Write() fails";
        return false;
    }

    const auto [thumbnailData, thumbnailSizeX, thumbnailSizeY] = thumbnailWriterBF.GetThumbnailData();

    ThumbnailImageWriter thumbnailImageWriter;
    thumbnailImageWriter.SetRootFolderPath(d->rootFolderPath);
    if (!thumbnailImageWriter.WriteBF(thumbnailData, thumbnailSizeX, thumbnailSizeY, 3, timeIndex)) {
        QLOG_ERROR() << "thumbnailImageWriter.WriteBF() fails";
        return false;
    }

    return true;
}

auto MultiModalProcessor::HTStitchWrite(const int32_t& timeIndex, const AcquisitionSequenceInfo& acquisitionSequenceInfo) -> bool {
    const auto tempFilePath = ResultFilePathGenerator::GetTempFilePath(d->rootFolderPath);

    const auto ldmOptionFlag = ProcessingOptionManager::GetInstance()->GetProcessingOptions().GetLDMFlag();

    if (ldmOptionFlag) {
        StitchingWriterHTLDM stitchingWriterHTLDM;
        stitchingWriterHTLDM.SetTempTCFFilePath(tempFilePath);
        stitchingWriterHTLDM.SetRootFolderPath(d->rootFolderPath);
        stitchingWriterHTLDM.SetAcquisitionConfig(d->acquisitionConfig);
        stitchingWriterHTLDM.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

        const auto stitchingWriterHTLDMResult = stitchingWriterHTLDM.Write(timeIndex);
        if (!stitchingWriterHTLDMResult) {
            QLOG_ERROR() << "StitchingWriterHTLDM.Write() fails";
            return false;
        }
    } else {
        StitchingWriterHT stitchingWriterHT;
        stitchingWriterHT.SetRootFolderPath(d->rootFolderPath);
        stitchingWriterHT.SetTempTCFFilePath(tempFilePath);
        stitchingWriterHT.SetAcquisitionConfig(d->acquisitionConfig);
        stitchingWriterHT.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

        const auto stitchingWriterHTResult = stitchingWriterHT.Write(timeIndex);
        if (!stitchingWriterHTResult) {
            QLOG_ERROR() << "StitchingWriterHT.Write() fails";
            return false;
        }
    }
    
    return true;
}

auto MultiModalProcessor::FLStitchWrite(const int32_t& flChannelIndex, const int32_t& timeIndex, 
    const AcquisitionSequenceInfo& acquisitionSequenceInfo) -> bool {
    const auto tempFilePath = ResultFilePathGenerator::GetTempFilePath(d->rootFolderPath);

    const auto ldmOptionFlag = ProcessingOptionManager::GetInstance()->GetProcessingOptions().GetLDMFlag();

    if (ldmOptionFlag) {
        StitchingWriterFLLDM stitchingWriterFLLDM;
        stitchingWriterFLLDM.SetTempTCFFilePath(tempFilePath);
        stitchingWriterFLLDM.SetRootFolderPath(d->rootFolderPath);
        stitchingWriterFLLDM.SetAcquisitionConfig(d->acquisitionConfig);
        stitchingWriterFLLDM.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

        const auto stitchingWriterFLLDMResult = stitchingWriterFLLDM.Write(timeIndex, flChannelIndex);
        if (!stitchingWriterFLLDMResult) {
            QLOG_ERROR() << "stitchingWriterFLLDM.Write() fails";
            return false;
        }
    } else {
        StitchingWriterFL stitchingWriterFL;
        stitchingWriterFL.SetRootFolderPath(d->rootFolderPath);
        stitchingWriterFL.SetTempTCFFilePath(tempFilePath);
        stitchingWriterFL.SetAcquisitionConfig(d->acquisitionConfig);
        stitchingWriterFL.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

        const auto stitchingWriterFLResult = stitchingWriterFL.Write(timeIndex, flChannelIndex);
        if (!stitchingWriterFLResult) {
            QLOG_ERROR() << "stitchingWriterFL.Write() fails";
            return false;
        }
    }

    return true;
}

auto MultiModalProcessor::BFStitchWrite(const int32_t& timeIndex, const AcquisitionSequenceInfo& acquisitionSequenceInfo) -> bool {
    const auto tempFilePath = ResultFilePathGenerator::GetTempFilePath(d->rootFolderPath);

    const auto ldmOptionFlag = ProcessingOptionManager::GetInstance()->GetProcessingOptions().GetLDMFlag();

    if (ldmOptionFlag) {
        StitchingWriterBFLDM stitchingWriterBFLDM;
        stitchingWriterBFLDM.SetTempTCFFilePath(tempFilePath);
        stitchingWriterBFLDM.SetRootFolderPath(d->rootFolderPath);
        stitchingWriterBFLDM.SetAcquisitionConfig(d->acquisitionConfig);
        stitchingWriterBFLDM.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

        const auto stitchingWriterResult = stitchingWriterBFLDM.Write(timeIndex);
        if (!stitchingWriterResult) {
            QLOG_ERROR() << "stitchingWriterBFLDM.Write() fails";
            return false;
        }
    } else {
        StitchingWriterBF stitchingWriterBF;
        stitchingWriterBF.SetRootFolderPath(d->rootFolderPath);
        stitchingWriterBF.SetTempTCFFilePath(tempFilePath);
        stitchingWriterBF.SetAcquisitionConfig(d->acquisitionConfig);
        stitchingWriterBF.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

        const auto stitchingWriterBFResult = stitchingWriterBF.Write(timeIndex);
        if (!stitchingWriterBFResult) {
            QLOG_ERROR() << "stitchingWriterBF.Write() fails";
            return false;
        }
    }

    return true;
}

auto MultiModalProcessor::HTLDMConvert(const int32_t& timeIndex) -> bool {
    //TODO Implement
    return true;
}

auto MultiModalProcessor::FLLDMConvert(const int32_t& flChannelIndex, const int32_t& timeIndex) -> bool {
    //TODO Implement
    return true;
}

auto MultiModalProcessor::BFLDMConvert(const int32_t& timeIndex) -> bool {
    //TODO Implement
    return true;
}

auto MultiModalProcessor::TCFWrite(const AcquisitionSequenceInfo& acquisitionSequenceInfo) -> bool {
    const auto tempFilePath = ResultFilePathGenerator::GetTempFilePath(d->rootFolderPath);

    const auto ldmOptionFlag = ProcessingOptionManager::GetInstance()->GetProcessingOptions().GetLDMFlag();

    const auto htExists = d->acquisitionConfig.GetAcquisitionCount().ht3D > 0;
    if (htExists) {
        const auto [dataSizeX, dataSizeY, dataSizeZ] = ReadHTDataSize(tempFilePath);
        const auto htFirstTimeFrameIndex = acquisitionSequenceInfo.GetFirstTimeFrameIndex(AcquisitionSequenceInfo::Modality::HT);

        const auto stitchingFilePath = ResultFilePathGenerator::GetHTProcessedFilePath(d->rootFolderPath, 0, htFirstTimeFrameIndex);
        const auto [pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ] = ReadWorldSize(stitchingFilePath);

        TCFHTCompleter tcfHTCompleter;
        tcfHTCompleter.SetTargetTCFFilePath(tempFilePath);
        tcfHTCompleter.SetSize(dataSizeX, dataSizeY, dataSizeZ);
        tcfHTCompleter.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);
        tcfHTCompleter.SetLdmOptionFlag(ldmOptionFlag);

        if (!tcfHTCompleter.MakeCompleteGroup()) {
            QLOG_ERROR() << "tcfHTCompleter.MakeCompleteGroup() fails";
            return false;
        }

        const auto mipData = ReadHTMIPData(tempFilePath, dataSizeX, dataSizeY);
        const QString mipFilePath = ResultFilePathGenerator::GetHTMIPImageFilePath(d->rootFolderPath);

        WriteMIP(mipData, dataSizeX, dataSizeY, mipFilePath);
    }

    const auto fl3DCH0Exists = d->acquisitionConfig.GetAcquisitionCount().fl3DCH0;
    const auto fl3DCH1Exists = d->acquisitionConfig.GetAcquisitionCount().fl3DCH1;
    const auto fl3DCH2Exists = d->acquisitionConfig.GetAcquisitionCount().fl3DCH2;
    const auto fl3DExists = fl3DCH0Exists || fl3DCH1Exists || fl3DCH2Exists;

    const auto fl2DCH0Exists = d->acquisitionConfig.GetAcquisitionCount().fl2DCH0;
    const auto fl2DCH1Exists = d->acquisitionConfig.GetAcquisitionCount().fl2DCH1;
    const auto fl2DCH2Exists = d->acquisitionConfig.GetAcquisitionCount().fl2DCH2;
    const auto fl2DExists = fl2DCH0Exists || fl2DCH1Exists || fl2DCH2Exists;
    
    const auto flExists = fl3DExists || fl2DExists;

    if (flExists) {
        int32_t flChannelIndex{ -1 }, flFirstTimeFrameIndex{ -1 };
        if (fl3DCH0Exists || fl2DCH0Exists) {
            flChannelIndex = 0;
            flFirstTimeFrameIndex = acquisitionSequenceInfo.GetFirstTimeFrameIndex(AcquisitionSequenceInfo::Modality::FLCH0);
        } else if (fl3DCH1Exists || fl2DCH1Exists) {
            flChannelIndex = 1;
            flFirstTimeFrameIndex = acquisitionSequenceInfo.GetFirstTimeFrameIndex(AcquisitionSequenceInfo::Modality::FLCH1);
        } else if (fl3DCH2Exists || fl2DCH2Exists) {
            flChannelIndex = 2;
            flFirstTimeFrameIndex = acquisitionSequenceInfo.GetFirstTimeFrameIndex(AcquisitionSequenceInfo::Modality::FLCH2);
        }

        const auto [dataSizeX, dataSizeY, dataSizeZ] = ReadFLDataSize(tempFilePath, flChannelIndex);

        const auto stitchingFilePath = ResultFilePathGenerator::GetFLProcessedFilePath(d->rootFolderPath, flChannelIndex, 0, flFirstTimeFrameIndex);
        const auto [pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ] = ReadWorldSize(stitchingFilePath);

        const double offsetZ = CalculateOffsetZ(d->acquisitionConfig);
        constexpr double offsetZCompensation = 0;

        const auto channelColorMap = d->GetChannelColorMap();
        const auto waveLengthMap = d->GetWaveLengthMap();
        const auto intensityMap = d->GetIntensityMap();
        const auto exposureTimeMap = d->GetExposureTimeMap();

        TCFFLCompleter tcfFLCompleter;
        tcfFLCompleter.SetTargetTCFFilePath(tempFilePath);
        tcfFLCompleter.SetSize(dataSizeX, dataSizeY, dataSizeZ);
        tcfFLCompleter.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ, LengthUnit::Micrometer);
        tcfFLCompleter.SetOffsetZ(offsetZ, LengthUnit::Micrometer);
        tcfFLCompleter.SetOffsetZCompensation(offsetZCompensation, LengthUnit::Micrometer);
        tcfFLCompleter.SetChannelColorMap(channelColorMap);
        tcfFLCompleter.SetIntensityMap(intensityMap);
        tcfFLCompleter.SetExposureTimeMap(exposureTimeMap);
        tcfFLCompleter.SetWaveLengthMap(waveLengthMap);
        tcfFLCompleter.SetLdmOptionFlag(ldmOptionFlag);

        if (!tcfFLCompleter.MakeCompleteGroup()) {
            QLOG_ERROR() << "tcfFLCompleter.MakeCompleteGroup() fails";
            return false;
        }
    }

    const auto bfExists = d->acquisitionConfig.GetAcquisitionCount().bf > 0;
    if (bfExists) {
        const auto [dataSizeX, dataSizeY, dataSizeZ] = ReadBFDataSize(tempFilePath);
        const auto bfFirstTimeFrameIndex = acquisitionSequenceInfo.GetFirstTimeFrameIndex(AcquisitionSequenceInfo::Modality::BF);

        const auto stitchingFilePath = ResultFilePathGenerator::GetBFProcessedFilePath(d->rootFolderPath, 0, bfFirstTimeFrameIndex);
        const auto [pixelWorldSizeX, pixelWorldSizeY, pixelWorldSizeZ] = ReadWorldSize(stitchingFilePath);

        TCFBFCompleter tcfBFCompleter;
        tcfBFCompleter.SetTargetTCFFilePath(tempFilePath);
        tcfBFCompleter.SetSize(dataSizeX, dataSizeY);
        tcfBFCompleter.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, LengthUnit::Micrometer);

        if (!tcfBFCompleter.MakeCompleteGroup()) {
            QLOG_ERROR() << "tcfBFCompleter.MakeCompleteGroup() fails";
            return false;
        }

        const auto rootName = QDir(d->rootFolderPath).dirName();

        const auto bfData = ReadBFData(tempFilePath, dataSizeX, dataSizeY);
        const QString bfFilePath = d->rootFolderPath + "/" + rootName + "-BF.PNG";
        WriteBF(bfData, dataSizeX, dataSizeY, bfFilePath);
    }

    TC::IO::TCFWriter::TCFThumbnailCompleter tcfThumbnailCompleter;
    tcfThumbnailCompleter.SetTargetTCFFilePath(tempFilePath);
    if (!tcfThumbnailCompleter.MakeCompleteGroup()) {
        QLOG_ERROR() << "tcfThumbnailCompleter.MakeCompleteGroup() fails";
        return false;
    }

    const auto deviceInfo = d->acquisitionConfig.GetDeviceInfo();

    const auto rootName = QDir(d->rootFolderPath).dirName();

    const QString deviceName = deviceInfo.deviceHost + "_" + deviceInfo.deviceSerial;
    const auto dataRecordedDateTime = GetRecordingTime(d->rootFolderPath, acquisitionSequenceInfo);
    const auto dataWritingDateTime = QDateTime::currentDateTime();

    const auto createDate = QDateTime::currentDateTime();
    const auto dataID = GenerateDataID(deviceName, dataRecordedDateTime);
    const QString description = "";
    const QString deviceHost = deviceInfo.deviceHost;
    const QString deviceSerial = deviceInfo.deviceSerial;
    const QString deviceModelType = "HTX";
    const QString deviceSoftwareVersion = deviceInfo.deviceSoftwareVersion;
    const QString formatVersion = "1.6.0";
    const auto& recordingTime = dataRecordedDateTime;
    const QString softwareVersion = "HTX ProcessingServer "+ QString(PROJECT_REVISION);
    const QString& title = rootName;
    const auto uniqueID = GenerateUniqueID(deviceName, dataRecordedDateTime, dataWritingDateTime);
    const auto userID = d->acquisitionConfig.GetJobInfo().userID;

    TCFCommonInfo tcfCommonInfo;
    tcfCommonInfo.SetCreateDate(createDate);
    tcfCommonInfo.SetDataID(dataID);
    tcfCommonInfo.SetDescription(description);
    tcfCommonInfo.SetDeviceHost(deviceHost);
    tcfCommonInfo.SetDeviceSerial(deviceSerial);
    tcfCommonInfo.SetDeviceModelType(deviceModelType);
    tcfCommonInfo.SetDeviceSoftwareVersion(deviceSoftwareVersion);
    tcfCommonInfo.SetFormatVersion(formatVersion);
    tcfCommonInfo.SetRecordingTime(recordingTime);
    tcfCommonInfo.SetSoftwareVersion(softwareVersion);
    tcfCommonInfo.SetTitle(title);
    tcfCommonInfo.SetUniqueID(uniqueID);
    tcfCommonInfo.SetUserID(userID);

    const double magnification = deviceInfo.magnification;
    const double na = deviceInfo.condenserNA;
    const double ri = deviceInfo.mediumRI;

    TCFDeviceInfo tcfDeviceInfo;
    tcfDeviceInfo.SetMagnification(magnification);
    tcfDeviceInfo.SetNA(na);
    tcfDeviceInfo.SetRI(ri);

    TCFStructureInfo tcfStructureInfo;
    if (ldmOptionFlag) {
        tcfStructureInfo.SetStructureType(StructureType::LDM);
    } else {
        tcfStructureInfo.SetStructureType(StructureType::Default);
    }

    TCFCompleter tcfCompleter;
    tcfCompleter.SetTargetTCFFilePath(tempFilePath);
    tcfCompleter.SetTCFCommonInfo(tcfCommonInfo);
    tcfCompleter.SetTCFDeviceInfo(tcfDeviceInfo);
    tcfCompleter.SetTCFStructureInfo(tcfStructureInfo);

    if (!tcfCompleter.MakeTCFComplete()) {
        QLOG_ERROR() << "tcfCompleter.MakeTCFComplete() fails";
        return false;
    }

    const QString tcfFilePath = ResultFilePathGenerator::GetTCFFilePath(d->rootFolderPath);
    QFile::rename(tempFilePath, tcfFilePath);

    QLOG_INFO() << "process success: " << d->rootFolderPath;

    return true;
}

auto MultiModalProcessor::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto MultiModalProcessor::SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo) -> void {
    d->acquisitionSequenceInfo = acquisitionSequenceInfo;
}

auto MultiModalProcessor::SetPsfFolderPath(const QString& psfFolderPath) -> void {
    d->psfFolderPath = psfFolderPath;
}

auto MultiModalProcessor::SetSystemBackgroundFolderPath(const QString& systemBackgroundFolderPath) -> void {
    d->systemBackgroundFolderPath = systemBackgroundFolderPath;
}

auto MultiModalProcessor::SetMatlabModuleFolderPath(const QString& matlabModuleFolderPath) -> void {
    d->matlabModuleFolderPath = matlabModuleFolderPath;
}
