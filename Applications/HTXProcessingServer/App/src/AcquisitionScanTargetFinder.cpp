#include "AcquisitionScanTargetFinder.h"

#include "TaskRepo.h"

class AcquisitionScanTargetFinder::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};

    bool nextIsBgScan{ false };

    int32_t nextModalityIndex{};
    int32_t nextTimeIndex{};
    int32_t nextTileIndex{};
};

AcquisitionScanTargetFinder::AcquisitionScanTargetFinder() : d{ std::make_unique<Impl>() } {
}

AcquisitionScanTargetFinder::~AcquisitionScanTargetFinder() = default;

auto AcquisitionScanTargetFinder::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto AcquisitionScanTargetFinder::Find() -> bool {
    if (d->rootFolderPath.isEmpty()) {
        return false;
    }

    const auto taskRepo = TaskRepo::GetInstance();

    const auto rootAdded = taskRepo->IsAdded(d->rootFolderPath);
    const auto rootNotDone = !(taskRepo->IsAlreadyDone(d->rootFolderPath) || taskRepo->IsDone(d->rootFolderPath));
    const auto rootNotFailed = !taskRepo->IsFailed(d->rootFolderPath);

    const auto taskRemains = rootAdded && rootNotDone && rootNotFailed;

    if (!taskRemains) {
        return false;
    }

    const auto acquisitionCount = taskRepo->GetAcquisitionCount(d->rootFolderPath);
    const auto acquisitionSequenceInfo = acquisitionCount.GetAcquisitionSequenceInfo();

    if (!acquisitionSequenceInfo.IsInitialized()) {
        return false;
    }

    const auto [tileNumberX, tileNumberY] = acquisitionCount.GetTileNumber();
    const auto tileCount = tileNumberX * tileNumberY;
    const auto timeFrameCount = acquisitionSequenceInfo.GetTimeFrameCount();

    constexpr auto modalityCount = static_cast<int32_t>(AcquisitionSequenceInfo::Modality::Count);

    const auto acquiredDataFlag = taskRepo->GetAcquiredDataFlag(d->rootFolderPath);

    for (auto modalityIndex = 0; modalityIndex < modalityCount; ++modalityIndex) {
        for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
            const auto modality = static_cast<AcquisitionSequenceInfo::Modality>(modalityIndex);

            if (!acquisitionSequenceInfo.AcquisitionExists(modality, timeIndex)) { continue; }

            if (modality == AcquisitionSequenceInfo::Modality::HT) {
                if (!acquiredDataFlag.GetBGAcquiredFlag()) {
                    d->nextIsBgScan = true;
                    return true;
                }
            }

            for (auto tileIndex = 0; tileIndex < tileCount; ++tileIndex) {
                bool acquiredFlag = true;

                switch (modality) {
                    case AcquisitionSequenceInfo::Modality::HT:
                        acquiredFlag = acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::HT });
                        break;
                    case AcquisitionSequenceInfo::Modality::FLCH0:
                        acquiredFlag = acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, 0 });
                        break;
                    case AcquisitionSequenceInfo::Modality::FLCH1:
                        acquiredFlag = acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, 1 });
                        break;
                    case AcquisitionSequenceInfo::Modality::FLCH2:
                        acquiredFlag = acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, 2 });
                        break;
                    case AcquisitionSequenceInfo::Modality::BF:
                        acquiredFlag = acquiredDataFlag.GetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::BF });
                        break;
                    case AcquisitionSequenceInfo::Modality::Count:
                    default:;
                }

                if (!acquiredFlag) {
                    d->nextModalityIndex = modalityIndex;
                    d->nextTimeIndex = timeIndex;
                    d->nextTileIndex = tileIndex;
                    return true;
                }
            }

        }
    }
    return false;
}

auto AcquisitionScanTargetFinder::IsNextScanBG() const -> bool {
    return d->nextIsBgScan;
}

auto AcquisitionScanTargetFinder::GetNextScanTimeIndex() const -> int32_t {
    return d->nextTimeIndex;
}

auto AcquisitionScanTargetFinder::GetNextScanTileIndex() const -> int32_t {
    return d->nextTileIndex;
}

auto AcquisitionScanTargetFinder::GetNextScanModalityType() const -> ModalityType {
    const auto modality = static_cast<AcquisitionSequenceInfo::Modality>(d->nextModalityIndex);
    
    switch (modality) {
    case AcquisitionSequenceInfo::Modality::HT:
        return ModalityType{ ModalityType::Name::HT };
    case AcquisitionSequenceInfo::Modality::FLCH0:
        return ModalityType{ ModalityType::Name::FL, 0 };
    case AcquisitionSequenceInfo::Modality::FLCH1:
        return ModalityType{ ModalityType::Name::FL, 1 };
    case AcquisitionSequenceInfo::Modality::FLCH2:
        return ModalityType{ ModalityType::Name::FL, 2 };
    case AcquisitionSequenceInfo::Modality::BF:
        return ModalityType{ ModalityType::Name::BF };
    case AcquisitionSequenceInfo::Modality::Count: //error case
    default:; // error case
    }

    return ModalityType{ ModalityType::Name::None }; //error case
}
