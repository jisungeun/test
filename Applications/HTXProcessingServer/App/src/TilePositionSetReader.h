#pragma once

#include <memory>
#include <QString>

#include "TilePositionSet.h"

class TilePositionSetReader {
public:
    TilePositionSetReader();
    ~TilePositionSetReader();

    auto SetTargetFilePath(const QString& filePath)->void;
    auto Read()->bool;

    auto GetTilePositionSet()const->TilePositionSet;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};