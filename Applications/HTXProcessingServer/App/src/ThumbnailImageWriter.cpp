#include "ThumbnailImageWriter.h"

#include <QDir>

#include "arrayfire.h"

#include "ResultFilePathGenerator.h"

class ThumbnailImageWriter::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};

    auto WriteThumbnailFolder(const QString& thumbnailFolderPath)->void;
    auto ApplyColor(const std::shared_ptr<uint8_t[]>& data, const int64_t& sizeX, const int64_t& sizeY,
        const RGB& color)->std::shared_ptr<uint8_t[]>;
    auto WriteImage(const QString& imageFilePath, const std::shared_ptr<uint8_t[]>& data, const int64_t& sizeX,
        const int64_t& sizeY, const int32_t& channels)->bool;
};

auto ThumbnailImageWriter::Impl::WriteThumbnailFolder(const QString& thumbnailFolderPath) -> void {
    if (!QDir().exists(thumbnailFolderPath)) {
        QDir().mkpath(thumbnailFolderPath);
    }
}

auto ThumbnailImageWriter::Impl::WriteImage(const QString& imageFilePath, const std::shared_ptr<uint8_t[]>& data,
    const int64_t& sizeX, const int64_t& sizeY, const int32_t& channels) -> bool {
    try {
        if (channels == 3) {
            const af::array imageData(sizeY, sizeX, channels, data.get());
            af::saveImageNative(imageFilePath.toStdString().c_str(), imageData);
        } else {
            const af::array imageData(sizeY, sizeX, data.get());
            af::saveImageNative(imageFilePath.toStdString().c_str(), imageData);
        }
    } catch (const af::exception&) {
        return false;
    }

    return true;
}

ThumbnailImageWriter::ThumbnailImageWriter() : d(new Impl()) {
}

ThumbnailImageWriter::~ThumbnailImageWriter() = default;

auto ThumbnailImageWriter::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto ThumbnailImageWriter::WriteHT(const std::shared_ptr<uint8_t[]>& imageData, const int64_t& sizeX,
    const int64_t& sizeY, const int32_t& timeIndex) -> bool {
    const auto thumbnailFolderPath = ResultFilePathGenerator::GetThumbnailFolderPath(d->rootFolderPath);
    d->WriteThumbnailFolder(thumbnailFolderPath);
    const auto imageFilePath = QString("%1/HT_%2.png").arg(thumbnailFolderPath).arg(timeIndex, 4, 10, QChar('0'));

    return d->WriteImage(imageFilePath, imageData, sizeX, sizeY, 1);
}

auto ThumbnailImageWriter::WriteFL(const std::shared_ptr<uint8_t[]>& imageData, const int64_t& sizeX,
    const int64_t& sizeY, const int32_t& timeIndex) -> bool {
    const auto thumbnailFolderPath = ResultFilePathGenerator::GetThumbnailFolderPath(d->rootFolderPath);
    d->WriteThumbnailFolder(thumbnailFolderPath);
    const auto imageFilePath = QString("%1/FL_%2.png").arg(thumbnailFolderPath).arg(timeIndex, 4, 10, QChar('0'));

    return d->WriteImage(imageFilePath, imageData, sizeX, sizeY, 3);
}

auto ThumbnailImageWriter::WriteBF(const std::shared_ptr<uint8_t[]>& imageData, const int64_t& sizeX,
    const int64_t& sizeY, const int32_t& channels, const int32_t& timeIndex) -> bool {
    const auto thumbnailFolderPath = ResultFilePathGenerator::GetThumbnailFolderPath(d->rootFolderPath);
    d->WriteThumbnailFolder(thumbnailFolderPath);
    const auto imageFilePath = QString("%1/BF_%2.png").arg(thumbnailFolderPath).arg(timeIndex, 4, 10, QChar('0'));

    return d->WriteImage(imageFilePath, imageData, sizeX, sizeY, channels);
}
