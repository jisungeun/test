#pragma once

#include <memory>
#include <QString>

#include "AcquisitionConfig.h"
#include "AcquisitionSequenceInfo.h"

class StitchingWriterFL {
public:
    StitchingWriterFL();
    ~StitchingWriterFL();

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto SetTempTCFFilePath(const QString& tempTCFFilePath)->void;
    auto SetAcquisitionConfig(const AcquisitionConfig& acquisitionConfig)->void;
    auto SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo)->void;

    auto Write(const int32_t& timeIndex, const int32_t& channelIndex)->bool;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};
