#pragma once

#include <memory>

class ModalityType {
public:
    enum class Name { None, HT, FL, BF };

    explicit ModalityType(const Name& name);
    ModalityType(const Name& name, const int32_t& channelIndex);
    ModalityType(const ModalityType& other);
    ~ModalityType();

    auto operator=(const ModalityType& other)->ModalityType&;

    auto operator==(const ModalityType& other)const->bool;
    auto operator!=(const ModalityType& other)const->bool;

    auto operator==(const Name& name)const->bool;
    auto operator!=(const Name& name)const->bool;

    auto GetName()const->const Name&;
    auto GetChannelIndex()const->const int32_t&;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};