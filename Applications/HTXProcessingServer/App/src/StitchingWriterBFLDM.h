#pragma once

#include <memory>
#include <QString>

#include "AcquisitionConfig.h"
#include "AcquisitionSequenceInfo.h"

class StitchingWriterBFLDM {
public:
    StitchingWriterBFLDM();
    ~StitchingWriterBFLDM();

    auto SetRootFolderPath(const QString& rootFolderPath)->void;
    auto SetTempTCFFilePath(const QString& tempTCFFilePath)->void;
    auto SetAcquisitionConfig(const AcquisitionConfig& acquisitionConfig)->void;
    auto SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo)->void;

    auto Write(const int32_t& timeIndex)->bool;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};