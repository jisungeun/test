#pragma once

#include <memory>
#include <QString>

#include "IFLDataSetGetter.h"

class FLDataSetGetterStitchingResult final : public IFLDataSetGetter {
public:
    FLDataSetGetterStitchingResult();
    ~FLDataSetGetterStitchingResult();

    auto SetResultFilePath(const QString& resultFilePath)->void;
    auto GetData() const -> std::shared_ptr<float[]> override;
    auto GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1, const int32_t& z0,
        const int32_t& z1) const -> std::shared_ptr<float[]> override;
    auto GetDataMemoryOrder() const -> MemoryOrder3D override;
    auto GetMIPData() const -> std::shared_ptr<float[]> override;
    auto GetMIPData(const int32_t& x0, const int32_t& x1, const int32_t& y0,
        const int32_t& y1) const -> std::shared_ptr<float[]> override;
    auto GetMIPDataMemoryOrder() const -> MemoryOrder2D override;
private:
    class Impl;
    std::unique_ptr<Impl> d;
};