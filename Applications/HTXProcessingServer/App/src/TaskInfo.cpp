#include "TaskInfo.h"

class TaskInfo::Impl {
public:
    Impl() = default;
    Impl(const Impl& other) = default;
    ~Impl() = default;

    auto operator=(const Impl& other)->Impl & = default;

    double processedTime{};
    TimeUnit processedTimeUnit{ TimeUnit::Second };

    double processedProgress{};

    bool onProcessing{ false };
    bool failed{ false };
};

TaskInfo::TaskInfo() : d(new Impl()) {
}

TaskInfo::TaskInfo(const TaskInfo& other) : d(new Impl(*other.d)) {
}

TaskInfo::~TaskInfo() = default;

auto TaskInfo::operator=(const TaskInfo& other) -> TaskInfo& {
    *(this->d) = *(other.d);
    return *this;
}

auto TaskInfo::SetProcessedTime(const double& processedTime, const TimeUnit& unit) -> void {
    d->processedTime = processedTime;
    d->processedTimeUnit = unit;
}

auto TaskInfo::GetProcessedTime(const TimeUnit& unit) const -> double {
    return ConvertUnit(d->processedTime, d->processedTimeUnit, unit);
}

auto TaskInfo::SetProcessedProgress(const double& processedProgress) -> void {
    d->processedProgress = processedProgress;
}

auto TaskInfo::GetProcessedProgress() const -> const double& {
    return d->processedProgress;
}

auto TaskInfo::SetOnProcessing(const bool& onProcessing) -> void {
    d->onProcessing = onProcessing;
}

auto TaskInfo::GetOnProcessing() const -> const bool& {
    return d->onProcessing;
}

auto TaskInfo::SetFailed(const bool& failed) -> void {
    d->failed = failed;
}

auto TaskInfo::GetFailed() const -> const bool& {
    return d->failed;
}

auto operator==(const TaskInfo& taskInfo1, const TaskInfo& taskInfo2) -> bool {
    if (taskInfo1.GetProcessedProgress() != taskInfo2.GetProcessedProgress()) {
        return false;
    }
    if (taskInfo1.GetProcessedTime(TimeUnit::Second) != taskInfo2.GetProcessedTime(TimeUnit::Second)) {
        return false;
    }
    if (taskInfo1.GetOnProcessing() != taskInfo2.GetOnProcessing()) {
        return false;
    }
    if (taskInfo1.GetFailed()!=taskInfo2.GetFailed()) {
        return false;
    }
    return true;
}

auto operator!=(const TaskInfo& taskInfo1, const TaskInfo& taskInfo2) -> bool {
    return !operator==(taskInfo1, taskInfo2);
}
