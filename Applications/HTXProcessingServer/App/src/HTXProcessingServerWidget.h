#pragma once

#include <memory>
#include "ui_HTXProcessingServerWidget.h"
#include "HTXProcessingServerHumbleObject.h"

class HTXProcessingServerWidget final : public QWidget, Ui::HTXProcessingServerWidget {
    Q_OBJECT
public:
    explicit HTXProcessingServerWidget(QWidget* parent = nullptr);
    ~HTXProcessingServerWidget();
protected:
    void dragEnterEvent(QDragEnterEvent* dragEnterEvent) override;
    void dropEvent(QDropEvent* dropEvent) override;
    void closeEvent(QCloseEvent* event) override;
private:
    struct Impl;
    std::unique_ptr<Impl> d;
private slots:
    void TopDirectoryButtonClicked();
    void ProcessButtonClicked();
    void OpenSelectedItemFolder(const int32_t& dataIndex);
    void ScanPeriodChanged(int periodInSec);

    void UpdateStatus(HTXProcessingServerHumbleObject humbleObject);
};
