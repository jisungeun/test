#define LOGGER_TAG "[ThumbnailWriterFL]"

#include "ThumbnailWriterFL.h"
#include <QMap>
#include "H5Cpp.h"

#include "HDF5Mutex.h"
#include "ResultFilePathGenerator.h"
#include "TCFThumbnailDataSet.h"
#include "TCFThumbnailWriter.h"
#include "TCLogger.h"
#include "ThumbnailFL.h"

class ThumbnailWriterFL::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    QString tempTCFFilePath{};
    int32_t timeFrameIndex{};
    AcquisitionSequenceInfo acquisitionSequenceInfo{};
    AcquisitionConfig acquisitionConfig{};
    QList<int32_t> flChannelIndexList{};

    std::shared_ptr<uint8_t[]> thumbnailData{};
    int32_t thumbnailSizeX{};
    int32_t thumbnailSizeY{};

    auto ReadFLVoxelSize(const int32_t& flChannelIndex)->std::tuple<bool, float, float>;
    auto ReadFLMIPData(const QList<int32_t>& flChannelIndexList)->std::tuple<bool, QMap<int32_t, std::tuple<std::shared_ptr<uint16_t[]>, int32_t, int32_t>>>;
    auto GenerateThumbnailData(const QMap<int32_t, std::shared_ptr<uint16_t[]>>& flDataMap, const int32_t& flDataSizeX,
        const int32_t& flDataSizeY)->std::tuple<bool, std::shared_ptr<uint8_t[]>, int32_t, int32_t>;
};

auto ThumbnailWriterFL::Impl::ReadFLVoxelSize(const int32_t& flChannelIndex) -> std::tuple<bool, float, float> {
    const QString htProcessedDataFilePath = ResultFilePathGenerator::GetFLProcessedFilePath(this->rootFolderPath, 
        flChannelIndex, 0, this->timeFrameIndex);
    try {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        float pixelWorldSizeX{}, pixelWorldSizeY{};

        const H5::H5File processedDataFile(htProcessedDataFilePath.toStdString(), H5F_ACC_RDONLY);
        const auto processedDataSet = processedDataFile.openDataSet("Data");

        const auto attrPixelWorldSizeX = processedDataSet.openAttribute("pixelWorldSizeX");
        const auto attrPixelWorldSizeY = processedDataSet.openAttribute("pixelWorldSizeY");

        attrPixelWorldSizeX.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
        attrPixelWorldSizeY.read(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeY);

        return { true, pixelWorldSizeX, pixelWorldSizeY };
    }
    catch (const H5::Exception&) {
        return { false, 0,0 };
    }
}

auto ThumbnailWriterFL::Impl::ReadFLMIPData(const QList<int32_t>& flChannelIndexList)
    -> std::tuple<bool, QMap<int32_t, std::tuple<std::shared_ptr<uint16_t[]>, int32_t, int32_t>>> {

    QMap<int32_t, std::tuple<std::shared_ptr<uint16_t[]>, int32_t, int32_t>> flMipData;

    for (const auto& flChannelIndex : flChannelIndexList) {
        const auto stitchingFilePath = 
            ResultFilePathGenerator::GetFLStitchingFilePath(this->rootFolderPath, flChannelIndex, this->timeFrameIndex);

        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        try {
            const H5::H5File stitchingDataFile(stitchingFilePath.toStdString(), H5F_ACC_RDONLY);
            const auto stitchingDataSetMIP = stitchingDataFile.openDataSet("StitchingDataMIP");

            int32_t stitchingDataSizeX{}, stitchingDataSizeY{};
            const auto attrDataSizeX = stitchingDataSetMIP.openAttribute("dataSizeX");
            const auto attrDataSizeY = stitchingDataSetMIP.openAttribute("dataSizeY");

            attrDataSizeX.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeX);
            attrDataSizeY.read(H5::PredType::NATIVE_INT32, &stitchingDataSizeY);

            const auto numberOfElements = 
                static_cast<int64_t>(stitchingDataSizeX) * static_cast<int64_t>(stitchingDataSizeY);

            std::shared_ptr<uint16_t[]> mipData{ new uint16_t[numberOfElements]() };
            stitchingDataSetMIP.read(mipData.get(), H5::PredType::NATIVE_UINT16);

            flMipData[flChannelIndex] = { mipData, stitchingDataSizeX, stitchingDataSizeY };
        }
        catch (const H5::Exception&) {
            return { false, {} };
        }
    }

    return { true, flMipData };
}

auto ThumbnailWriterFL::Impl::GenerateThumbnailData(const QMap<int32_t, std::shared_ptr<uint16_t[]>>& flDataMap,
    const int32_t& flDataSizeX, const int32_t& flDataSizeY)
    -> std::tuple<bool, std::shared_ptr<uint8_t[]>, int32_t, int32_t> {

    TC::Processing::ThumbnailGenerator::ThumbnailFL thumbnailFL;

    for (const auto& flChannelIndex : this->flChannelIndexList) {
        AcquisitionConfig::RGB flRGB;
        if (flChannelIndex == 0) {
            flRGB = this->acquisitionConfig.GetAcquisitionSetting().flCH0Color;
        }
        else if (flChannelIndex == 1) {
            flRGB = this->acquisitionConfig.GetAcquisitionSetting().flCH1Color;
        }
        else if (flChannelIndex == 2) {
            flRGB = this->acquisitionConfig.GetAcquisitionSetting().flCH2Color;
        }

        const auto [r, g, b] = flRGB;

        const auto flData = flDataMap[flChannelIndex];
        const auto numberOfElements = static_cast<uint64_t>(flDataSizeX) * static_cast<uint64_t>(flDataSizeY);
        std::shared_ptr<uint16_t[]> convertedData{ new uint16_t[numberOfElements] };

        for (int64_t xIndex = 0; xIndex < static_cast<int64_t>(flDataSizeX); ++xIndex) {
            for (int64_t yIndex = 0; yIndex < static_cast<int64_t>(flDataSizeY); ++yIndex) {
                const auto srcIndex = xIndex + yIndex * static_cast<int64_t>(flDataSizeX);
                const auto destIndex = yIndex + xIndex * static_cast<int64_t>(flDataSizeY);

                convertedData.get()[destIndex] = flData.get()[srcIndex];
            }
        }

        if (flChannelIndex == 0) {
            thumbnailFL.SetFLCH0Data(convertedData, r, g, b);
        } else if (flChannelIndex == 1) {
            thumbnailFL.SetFLCH1Data(convertedData, r, g, b);
        } else if (flChannelIndex == 2) {
            thumbnailFL.SetFLCH2Data(convertedData, r, g, b);
        }
    }

    thumbnailFL.SetDataSize(flDataSizeX, flDataSizeY);
    if (!thumbnailFL.Generate()) { return { false, nullptr, 0,0 }; }

    const auto thumbnailData = thumbnailFL.GetThumbnailData();
    const auto thumbnailDataSizeX = thumbnailFL.GetThumbnailSizeX();
    const auto thumbnailDataSizeY = thumbnailFL.GetThumbnailSizeY();

    return { true, thumbnailData, thumbnailDataSizeX, thumbnailDataSizeY };
}

ThumbnailWriterFL::ThumbnailWriterFL() :d(new Impl()) {
}

ThumbnailWriterFL::~ThumbnailWriterFL() = default;

auto ThumbnailWriterFL::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto ThumbnailWriterFL::SetTempTCFFilePath(const QString& tempTCFFilePath) -> void {
    d->tempTCFFilePath = tempTCFFilePath;
}

auto ThumbnailWriterFL::SetTimeFrameIndex(const int32_t& timeFrameIndex) -> void {
    d->timeFrameIndex = timeFrameIndex;
}

auto ThumbnailWriterFL::SetAcquisitionSequenceInfo(const AcquisitionSequenceInfo& acquisitionSequenceInfo) -> void {
    d->acquisitionSequenceInfo = acquisitionSequenceInfo;
}

auto ThumbnailWriterFL::SetAcquisitionConfig(const AcquisitionConfig& acquisitionConfig) -> void {
    d->acquisitionConfig = acquisitionConfig;
}

auto ThumbnailWriterFL::SetFLChannelIndexList(const QList<int32_t>& flChannelIndexList) -> void {
    d->flChannelIndexList = flChannelIndexList;
}

auto ThumbnailWriterFL::Write() -> bool {
    if (d->flChannelIndexList.isEmpty()) {
        QLOG_ERROR() << "flChannelIndexList is empty";
        return false;
    }

    auto [readingResult, flDataMap] = d->ReadFLMIPData(d->flChannelIndexList);
    if (readingResult == false) {
        QLOG_ERROR() << "ReadFLMIPData() fails";
        return false;
    }

    auto [voxelSizeReadingResult, voxelSizeX, voxelSizeY] = d->ReadFLVoxelSize(d->flChannelIndexList.first());
    if (voxelSizeReadingResult == false) {
        QLOG_ERROR() << "ReadFLVoxelSize fails";
        return false;
    }

    int32_t flDataSizeX{}, flDataSizeY{};
    QMap<int32_t, std::shared_ptr<uint16_t[]>> onlyFLDataMap;
    for (auto iterator = flDataMap.begin(); iterator != flDataMap.end(); ++iterator) {
        const auto flChannelIndex = iterator.key();
        const auto [flData, dataSizeX, dataSizeY] = iterator.value();

        onlyFLDataMap[flChannelIndex] = flData;
        flDataSizeX = dataSizeX;
        flDataSizeY = dataSizeY;
    }

    auto [thumbnailGeneratingResult, thumbnailData, thumbnailSizeX, thumbnailSizeY] =
        d->GenerateThumbnailData(onlyFLDataMap, flDataSizeX, flDataSizeY);

    if (thumbnailGeneratingResult == false) {
        QLOG_ERROR() << "GenerateThumbnailData() fails";
        return false;
    }

    d->thumbnailData = thumbnailData;
    d->thumbnailSizeX = thumbnailSizeX;
    d->thumbnailSizeY = thumbnailSizeY;

    const auto thumbnailVoxelSizeX = voxelSizeX * static_cast<float>(flDataSizeX) / static_cast<float>(thumbnailSizeX);
    const auto thumbnailVoxelSizeY = voxelSizeY * static_cast<float>(flDataSizeY) / static_cast<float>(thumbnailSizeY);


    int32_t tcfDataIndex{-1};
    if (d->flChannelIndexList.contains(0)) {
        tcfDataIndex = d->acquisitionSequenceInfo.GetOrderIndex(AcquisitionSequenceInfo::Modality::FLCH0, d->timeFrameIndex);
    } else if (d->flChannelIndexList.contains(1)) {
        tcfDataIndex = d->acquisitionSequenceInfo.GetOrderIndex(AcquisitionSequenceInfo::Modality::FLCH1, d->timeFrameIndex);
    } else if (d->flChannelIndexList.contains(2)) {
        tcfDataIndex = d->acquisitionSequenceInfo.GetOrderIndex(AcquisitionSequenceInfo::Modality::FLCH2, d->timeFrameIndex);
    }

    if (tcfDataIndex == -1) {
        QLOG_ERROR() << "flChannelIndexList fails";
        return false;
    }

    TC::IO::TCFWriter::TCFThumbnailDataSet tcfThumbnailDataSet;
    tcfThumbnailDataSet.SetData(thumbnailData);
    tcfThumbnailDataSet.SetDataMemoryOrder(MemoryOrder3D::YXZ);
    tcfThumbnailDataSet.SetDataSize(thumbnailSizeX, thumbnailSizeY);
    tcfThumbnailDataSet.SetColorFlag(true);
    tcfThumbnailDataSet.SetVoxelSize(thumbnailVoxelSizeX, thumbnailVoxelSizeY, LengthUnit::Micrometer);
    tcfThumbnailDataSet.SetTimeFrameIndex(tcfDataIndex);

    TC::IO::TCFWriter::TCFThumbnailWriter tcfThumbnailWriter;
    tcfThumbnailWriter.SetModality(TC::IO::TCFWriter::Modality::FL);
    tcfThumbnailWriter.SetTCFThumbnailDataSet(tcfThumbnailDataSet);
    tcfThumbnailWriter.SetTargetFilePath(d->tempTCFFilePath);
    if (!tcfThumbnailWriter.Write()) {
        QLOG_ERROR() << "tcfThumbnailWriter.Write() fails";
        return false;
    }

    return true;
}

auto ThumbnailWriterFL::GetThumbnailData() const -> std::tuple<std::shared_ptr<uint8_t[]>, int32_t, int32_t> {
    return { d->thumbnailData, d->thumbnailSizeX, d->thumbnailSizeY };
}
