#pragma once

#include <memory>
#include <QDateTime>
#include <QString>

class RecordingTimeReader {
public:
    RecordingTimeReader();
    ~RecordingTimeReader();

    auto SetFilePath(const QString& filePath)->void;
    auto Read()->bool;

    auto GetRecordingTime()const->const QDateTime&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};