#include "AcquisitionDataScanner.h"

#include <QFile>
#include <QMutexLocker>

#include "ImageIntegrityCheckerPng.h"

#include "DataFolderPathGetter.h"

using SequenceModality = AcquisitionSequenceInfo::Modality;

class AcquisitionDataScanner::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    AcquisitionDataInfo acquisitionDataInfo{};

    AcquiredDataFlag acquiredDataFlag{};

    QString rootFolderPath{};
    int32_t tileNumber{};
    int32_t timeFrameCount{};
    AcquisitionSequenceInfo acquisitionSequenceInfo{};

    QMutex mutex{};

    auto ScanBackgroundImageAcquisition()->void;
    auto ScanHTAcquisition()->void;
    auto ScanFLAcquisition()->void;
    auto ScanBFAcquisition()->void;

    auto CheckImageIntegrity(const QString& imageFolderPath, const uint32_t& numberOfImages, 
        const bool& moreThanCount = false) const ->bool;

    auto DoesHTExist()const->bool;
};

auto AcquisitionDataScanner::Impl::ScanBackgroundImageAcquisition() -> void {
    if (this->DoesHTExist()) {
        const auto backgroundFolderPath = this->acquisitionDataInfo.GetBackgroundFolderPath();

        if (this->acquisitionDataInfo.IsBackgroundPlacedInRootFolder()) {
            const int32_t numberOfImages = 4;
            const auto backgroundImageDataAcquired = this->CheckImageIntegrity(backgroundFolderPath, numberOfImages);
            this->acquiredDataFlag.SetBGAcquiredFlag(backgroundImageDataAcquired);
        } else {
            const int32_t numberOfImages = 80;
            const auto backgroundImageDataAcquired = this->CheckImageIntegrity(backgroundFolderPath, numberOfImages, true);
            this->acquiredDataFlag.SetBGAcquiredFlag(backgroundImageDataAcquired);
        }
    }
}

auto AcquisitionDataScanner::Impl::ScanHTAcquisition() -> void {
    const ModalityType modalityType{ ModalityType::Name::HT };
    DataFolderPathGetter dataFolderPathGetter;
    dataFolderPathGetter.SetRootFolderPath(this->rootFolderPath);
    dataFolderPathGetter.SetModalityType(modalityType);

    for (auto timeIndex = 0; timeIndex < this->timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(SequenceModality::HT, timeIndex)) { continue; }

        const auto zSliceCount = this->acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::HT, timeIndex);

        const auto numberOfImages = zSliceCount * 4;
        dataFolderPathGetter.SetTimeIndex(timeIndex);

        for (auto tileIndex = 0; tileIndex < this->tileNumber; ++tileIndex) {
            dataFolderPathGetter.SetTileIndex(tileIndex);
            if (!dataFolderPathGetter.ExistTimeFrameFolder()) { continue; }

            const auto htFolderPath = dataFolderPathGetter.GetTimeFrameFolderPath();
            const bool htDataAcquired = this->CheckImageIntegrity(htFolderPath, numberOfImages);
            this->acquiredDataFlag.SetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::HT }, htDataAcquired);
        }
    }
}

auto AcquisitionDataScanner::Impl::ScanFLAcquisition() -> void {
    DataFolderPathGetter dataFolderPathGetter;
    dataFolderPathGetter.SetRootFolderPath(this->rootFolderPath);

    for (auto flChannelIndex = 0; flChannelIndex < 3; ++flChannelIndex) {
        const ModalityType modalityType{ ModalityType::Name::FL, flChannelIndex };
        dataFolderPathGetter.SetModalityType(modalityType);

        const auto sequenceModality = ConvertSequenceModality(flChannelIndex);

        for (auto timeIndex = 0; timeIndex < this->timeFrameCount; ++timeIndex) {
            if (!this->acquisitionSequenceInfo.AcquisitionExists(sequenceModality, timeIndex)) {
                continue;
            }
            const auto zSliceCount = this->acquisitionSequenceInfo.GetAcquisitionZSliceCount(sequenceModality, timeIndex);

            const auto numberOfImages = zSliceCount;
            dataFolderPathGetter.SetTimeIndex(timeIndex);

            for (auto tileIndex = 0; tileIndex < this->tileNumber; ++tileIndex) {
                dataFolderPathGetter.SetTileIndex(tileIndex);
                if (!dataFolderPathGetter.ExistTimeFrameFolder()) { continue; }

                const auto flDataFolderPath = dataFolderPathGetter.GetTimeFrameFolderPath();
                bool flDataAcquired = this->CheckImageIntegrity(flDataFolderPath, numberOfImages);
                this->acquiredDataFlag.SetAcquiredFlag(timeIndex, tileIndex, 
                    ModalityType{ ModalityType::Name::FL, flChannelIndex }, flDataAcquired);
            }
        }
    }
}

auto AcquisitionDataScanner::Impl::ScanBFAcquisition() -> void {
    const ModalityType modalityType{ ModalityType::Name::BF };
    DataFolderPathGetter dataFolderPathGetter;
    dataFolderPathGetter.SetRootFolderPath(this->rootFolderPath);
    dataFolderPathGetter.SetModalityType(modalityType);

    for (auto timeIndex = 0; timeIndex < this->timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(SequenceModality::BF, timeIndex)) {
            continue;
        }

        const auto zSliceCount = this->acquisitionSequenceInfo.GetAcquisitionZSliceCount(SequenceModality::BF, timeIndex);

        const auto numberOfImages = zSliceCount;
        dataFolderPathGetter.SetTimeIndex(timeIndex);

        for (auto tileIndex = 0; tileIndex < this->tileNumber; ++tileIndex) {
            dataFolderPathGetter.SetTileIndex(tileIndex);
            if (!dataFolderPathGetter.ExistTimeFrameFolder()) { continue; }

            const auto bfFolderPath = dataFolderPathGetter.GetTimeFrameFolderPath();
            const bool bfDataAcquired = this->CheckImageIntegrity(bfFolderPath, numberOfImages);
            this->acquiredDataFlag.SetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::BF }, bfDataAcquired);
        }
    }
}

auto AcquisitionDataScanner::Impl::CheckImageIntegrity(const QString& imageFolderPath, const uint32_t& numberOfImages, 
    const bool& moreThanCount) const -> bool {
    if (QFile::exists(imageFolderPath)) {
        ImageIntegrityCheckerPng imageIntegrityCheckerPng;
        imageIntegrityCheckerPng.SetImageFolderPath(imageFolderPath);
        imageIntegrityCheckerPng.SetNumberOfImages(numberOfImages);

        if (moreThanCount) {
            return imageIntegrityCheckerPng.CheckMoreThanCount();
        } else {
            return imageIntegrityCheckerPng.CheckSameCount();
        }
    } else {
        return false;
    }
}

auto AcquisitionDataScanner::Impl::DoesHTExist() const -> bool {
    const auto timeFrameCount = this->acquisitionSequenceInfo.GetTimeFrameCount();
    for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
        if (acquisitionSequenceInfo.AcquisitionExists(SequenceModality::HT, timeIndex)) {
            return true;
        }
    }

    return false;
}

AcquisitionDataScanner::AcquisitionDataScanner() : d(std::make_unique<Impl>()) {
}

AcquisitionDataScanner::~AcquisitionDataScanner() = default;

auto AcquisitionDataScanner::SetAcquisitionDataInfo(const AcquisitionDataInfo& acquisitionDataInfo) -> void {
    QMutexLocker locker{ &d->mutex };
    d->acquisitionDataInfo = acquisitionDataInfo;
}

auto AcquisitionDataScanner::Scan() -> bool {
    QMutexLocker locker{ &d->mutex };
    const auto& acquisitionCount = d->acquisitionDataInfo.GetAcquisitionCount();

    d->acquiredDataFlag = AcquiredDataFlag{};
    d->acquiredDataFlag.Initialize(acquisitionCount);

    d->rootFolderPath = d->acquisitionDataInfo.GetRootFolderPath();

    const auto [tileNumberX, tileNumberY] = acquisitionCount.GetTileNumber();
    d->tileNumber = tileNumberX * tileNumberY;
    d->timeFrameCount = acquisitionCount.GetAcquisitionSequenceInfo().GetTimeFrameCount();

    d->acquisitionSequenceInfo = acquisitionCount.GetAcquisitionSequenceInfo();

    d->ScanBackgroundImageAcquisition();
    d->ScanHTAcquisition();
    d->ScanFLAcquisition();
    d->ScanBFAcquisition();

    return true;
}

auto AcquisitionDataScanner::GetAcquiredDataFlag() const -> const AcquiredDataFlag& {
    QMutexLocker locker{ &d->mutex };
    return d->acquiredDataFlag;
}
