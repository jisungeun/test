#include "TaskInfoSetScanner.h"

#include <QFile>
#include <QMutexLocker>

#include "H5Cpp.h"
#include "HDF5Mutex.h"
#include "ModalityType.h"
#include "ProcessType.h"

#include "ResultFilePathGenerator.h"

using SequenceModality = AcquisitionSequenceInfo::Modality;

class TaskInfoSetScanner::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};
    AcquisitionCount acquisitionCount;
    AcquisitionSequenceInfo acquisitionSequenceInfo;
    TaskInfoSet taskInfoSet{};

    int32_t tileNumber{};
    int32_t timeFrameCount{};

    QMutex mutex{};

    auto ScanHTProcessing()->void;
    auto ScanFLProcessing()->void;
    auto ScanBFProcessing()->void;

    auto ScanHTStitching()->void;
    auto ScanFLStitching()->void;
    auto ScanBFStitching()->void;

    auto ScanHTStitchingWritten(const QString& tempFilePath)->void;
    auto ScanFLStitchingWritten(const QString& tempFilePath)->void;
    auto ScanBFStitchingWritten(const QString& tempFilePath)->void;

    auto ScanHTLDMConverting()->void;
    auto ScanFLLDMConverting()->void;
    auto ScanBFLDMConverting()->void;

    TaskInfo completedTaskInfo{};

    static auto GetDataSetName(const int32_t& dataIndex)->QString;
    static auto CheckTileDoneFlag(const QString& processedFilePath)->bool;
    static auto CheckStitchingDoneFlag(const QString& processedFilePath)->bool;
};

auto TaskInfoSetScanner::Impl::ScanHTProcessing() -> void {
    for (auto timeIndex = 0; timeIndex < this->timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(SequenceModality::HT, timeIndex)) {
            continue;
        }

        for (auto tileIndex = 0; tileIndex < this->tileNumber; ++tileIndex) {
            const auto htProcessedDataFilePath = 
                ResultFilePathGenerator::GetHTProcessedFilePath(this->rootFolderPath, tileIndex, timeIndex);

            if (QFile::exists(htProcessedDataFilePath)) {
                TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
                if (this->CheckTileDoneFlag(htProcessedDataFilePath)) {
                    this->taskInfoSet.SetTaskInfo(timeIndex, tileIndex, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, this->completedTaskInfo);
                }
            }
        }
    }
}

auto TaskInfoSetScanner::Impl::ScanFLProcessing() -> void {
    for (auto flChannelIndex = 0; flChannelIndex < 3; ++flChannelIndex) {
        const auto sequenceModality = ConvertSequenceModality(flChannelIndex);

        for (auto timeIndex = 0; timeIndex < this->timeFrameCount; ++timeIndex) {
            if (!this->acquisitionSequenceInfo.AcquisitionExists(sequenceModality, timeIndex)) {
                continue;
            }

            for (auto tileIndex = 0; tileIndex < this->tileNumber; ++tileIndex) {
                const auto flProcessedDataFilePath =
                    ResultFilePathGenerator::GetFLProcessedFilePath(this->rootFolderPath, flChannelIndex, tileIndex, timeIndex);

                if (QFile::exists(flProcessedDataFilePath)) {
                    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
                    if (this->CheckTileDoneFlag(flProcessedDataFilePath)) {
                        this->taskInfoSet.SetTaskInfo(timeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::TileProcessing, this->completedTaskInfo);
                    }
                }
            }
        }
    }
}

auto TaskInfoSetScanner::Impl::ScanBFProcessing() -> void {
    for (auto timeIndex = 0; timeIndex < this->timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(SequenceModality::BF, timeIndex)) {
            continue;
        }

        for (auto tileIndex = 0; tileIndex < this->tileNumber; ++tileIndex) {
            const auto bfProcessedDataFilePath =
                ResultFilePathGenerator::GetBFProcessedFilePath(this->rootFolderPath, tileIndex, timeIndex);

            if (QFile::exists(bfProcessedDataFilePath)) {
                TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
                if (this->CheckTileDoneFlag(bfProcessedDataFilePath)) {
                    this->taskInfoSet.SetTaskInfo(timeIndex, tileIndex, ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing, this->completedTaskInfo);
                }
            }
        }
    }
}

auto TaskInfoSetScanner::Impl::ScanHTStitching() -> void {
    for (auto timeIndex = 0; timeIndex < this->timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(SequenceModality::HT, timeIndex)) {
            continue;
        }

        const auto htStitchingResultFilePath =
            ResultFilePathGenerator::GetHTStitchingFilePath(this->rootFolderPath, timeIndex);

        if (QFile::exists(htStitchingResultFilePath)) {
            TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
            if (this->CheckStitchingDoneFlag(htStitchingResultFilePath)) {
                this->taskInfoSet.SetTaskInfo(timeIndex, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, this->completedTaskInfo);
            }
        }
    }
}

auto TaskInfoSetScanner::Impl::ScanFLStitching() -> void {
    for (auto flChannelIndex = 0; flChannelIndex < 3; ++flChannelIndex) {
        const auto sequenceModality = ConvertSequenceModality(flChannelIndex);

        for (auto timeIndex = 0; timeIndex < this->timeFrameCount; ++timeIndex) {
            if (!this->acquisitionSequenceInfo.AcquisitionExists(sequenceModality, timeIndex)) {
                continue;
            }

            const auto flStitchingResultFilePath =
                ResultFilePathGenerator::GetFLStitchingFilePath(this->rootFolderPath, flChannelIndex, timeIndex);

            if (QFile::exists(flStitchingResultFilePath)) {
                TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
                if (this->CheckStitchingDoneFlag(flStitchingResultFilePath)) {
                    this->taskInfoSet.SetTaskInfo(timeIndex, 0, ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::Stitching, this->completedTaskInfo);
                }
            }
        }
    }
}

auto TaskInfoSetScanner::Impl::ScanBFStitching() -> void {
    for (auto timeIndex = 0; timeIndex < this->timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(SequenceModality::BF, timeIndex)) {
            continue;
        }
        const auto bfStitchingResultFilePath =
            ResultFilePathGenerator::GetBFStitchingFilePath(this->rootFolderPath, timeIndex);

        if (QFile::exists(bfStitchingResultFilePath)) {
            TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
            if (this->CheckStitchingDoneFlag(bfStitchingResultFilePath)) {
                this->taskInfoSet.SetTaskInfo(timeIndex, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::Stitching, this->completedTaskInfo);
            }
        }
    }
}

auto TaskInfoSetScanner::Impl::ScanHTStitchingWritten(const QString& tempFilePath) -> void {
    const H5::H5File tempFile{ tempFilePath.toStdString(), H5F_ACC_RDONLY };

    if (!tempFile.exists("Data")) { return; }
    const auto dataGroup = tempFile.openGroup("Data");

    if (!dataGroup.exists("3D")) { return; }
    const auto htGroup = dataGroup.openGroup("3D");

    constexpr auto sequenceModality = SequenceModality::HT;
    for (auto timeIndex = 0; timeIndex < this->timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(sequenceModality, timeIndex)) {
            continue;
        }

        const auto dataSetIndex = this->acquisitionSequenceInfo.GetOrderIndex(sequenceModality, timeIndex);
        const auto dataSetName = this->GetDataSetName(dataSetIndex);
        const auto dataSetExists = htGroup.exists(dataSetName.toStdString());

        if (!dataSetExists) { continue; }
        this->taskInfoSet.SetTaskInfo(timeIndex, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, this->completedTaskInfo);
    }
}

auto TaskInfoSetScanner::Impl::ScanFLStitchingWritten(const QString& tempFilePath) -> void {
    const H5::H5File tempFile{ tempFilePath.toStdString(), H5F_ACC_RDONLY };

    if (!tempFile.exists("Data")) { return; }
    const auto dataGroup = tempFile.openGroup("Data");

    if (!dataGroup.exists("3DFL")) { return; }
    const auto flGroup = dataGroup.openGroup("3DFL");

    for (auto flChannelIndex = 0; flChannelIndex < 3; ++flChannelIndex) {
        const auto sequenceModality = ConvertSequenceModality(flChannelIndex);

        for (auto timeIndex = 0; timeIndex < this->timeFrameCount; ++timeIndex) {
            if (!this->acquisitionSequenceInfo.AcquisitionExists(sequenceModality, timeIndex)) {
                continue;
            }

            const auto channelGroupName = QString("CH%1").arg(flChannelIndex);
            if (!flGroup.exists(channelGroupName.toStdString())) { continue; }

            const auto channelGroup = flGroup.openGroup(channelGroupName.toStdString());

            const auto dataSetIndex = this->acquisitionSequenceInfo.GetOrderIndex(sequenceModality, timeIndex);
            const auto dataSetName = this->GetDataSetName(dataSetIndex);
            const auto dataSetExists = channelGroup.exists(dataSetName.toStdString());

            if (!dataSetExists) { continue; }

            this->taskInfoSet.SetTaskInfo(timeIndex, 0, ModalityType{ ModalityType::Name::FL, flChannelIndex }, ProcessType::StitchingWriting, this->completedTaskInfo);
        }
    }
}

auto TaskInfoSetScanner::Impl::ScanBFStitchingWritten(const QString& tempFilePath) -> void {
    const H5::H5File tempFile{ tempFilePath.toStdString(), H5F_ACC_RDONLY };

    if (!tempFile.exists("Data")) { return; }
    const auto dataGroup = tempFile.openGroup("Data");

    if (!dataGroup.exists("BF")) { return; }
    const auto bfGroup = dataGroup.openGroup("BF");

    constexpr auto sequenceModality = SequenceModality::BF;
    for (auto timeIndex = 0; timeIndex < this->timeFrameCount; ++timeIndex) {
        if (!this->acquisitionSequenceInfo.AcquisitionExists(sequenceModality, timeIndex)) {
            continue;
        }

        const auto dataSetIndex = this->acquisitionSequenceInfo.GetOrderIndex(sequenceModality, timeIndex);
        const auto dataSetName = this->GetDataSetName(dataSetIndex);
        const auto dataSetExists = bfGroup.exists(dataSetName.toStdString());

        if (!dataSetExists) { continue; }

        this->taskInfoSet.SetTaskInfo(timeIndex, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::StitchingWriting, this->completedTaskInfo);
    }
}

auto TaskInfoSetScanner::Impl::ScanHTLDMConverting() -> void {
    //TODO Implement
}

auto TaskInfoSetScanner::Impl::ScanFLLDMConverting() -> void {
    //TODO Implement
}

auto TaskInfoSetScanner::Impl::ScanBFLDMConverting() -> void {
    //TODO Implement
}

auto TaskInfoSetScanner::Impl::GetDataSetName(const int32_t& timeIndex) -> QString {
    return QString("%1").arg(timeIndex, 6, 10, QChar('0'));
}

auto TaskInfoSetScanner::Impl::CheckTileDoneFlag(const QString& processedFilePath) -> bool {
    try {
        if (!H5::H5File::isHdf5(processedFilePath.toStdString())) { return false; }

        const H5::H5File file(processedFilePath.toStdString(), H5F_ACC_RDONLY);

        if (!file.exists("Data")) { return false; }
        const auto dataSet = file.openDataSet("Data");

        if (!dataSet.attrExists("done")) { return false; }
    } catch(const H5::Exception& h5Exception) {
        return false;
    }
    
    return true;
}

auto TaskInfoSetScanner::Impl::CheckStitchingDoneFlag(const QString& processedFilePath) -> bool {
    try {
        if (!H5::H5File::isHdf5(processedFilePath.toStdString())) { return false; }

        const H5::H5File file(processedFilePath.toStdString(), H5F_ACC_RDONLY);

        if (!file.attrExists("done")) { return false; }
    }
    catch (const H5::Exception& h5Exception) {
        return false;
    }

    return true;
}

TaskInfoSetScanner::TaskInfoSetScanner() : d(new Impl()) {
    d->completedTaskInfo.SetProcessedProgress(1);
}

TaskInfoSetScanner::~TaskInfoSetScanner() = default;

auto TaskInfoSetScanner::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto TaskInfoSetScanner::SetAcquisitionCount(const AcquisitionCount& acquisitionCount) -> void {
    d->acquisitionCount = acquisitionCount;
}

auto TaskInfoSetScanner::Scan() -> bool {
    QMutexLocker locker{ &d->mutex };

    TaskInfoSet taskInfoSet;
    taskInfoSet.Initialize(d->acquisitionCount);
    
    d->taskInfoSet = taskInfoSet;

    const auto [tileNumberX, tileNumberY] = d->acquisitionCount.GetTileNumber();
    d->tileNumber = tileNumberX * tileNumberY;

    d->acquisitionSequenceInfo = d->acquisitionCount.GetAcquisitionSequenceInfo();
    d->timeFrameCount = d->acquisitionSequenceInfo.GetTimeFrameCount();

    d->ScanHTProcessing();
    d->ScanFLProcessing();
    d->ScanBFProcessing();

    d->ScanHTStitching();
    d->ScanFLStitching();
    d->ScanBFStitching();
    
    const auto tempFilePath = ResultFilePathGenerator::GetTempFilePath(d->rootFolderPath);
    if (QFile::exists(tempFilePath)) {
        if (H5::H5File::isHdf5(tempFilePath.toStdString())) {
            d->ScanHTStitchingWritten(tempFilePath);
            d->ScanFLStitchingWritten(tempFilePath);
            d->ScanBFStitchingWritten(tempFilePath);
        }
    }

    d->ScanHTLDMConverting();
    d->ScanFLLDMConverting();
    d->ScanBFLDMConverting();

    return true;
}

auto TaskInfoSetScanner::GetTaskInfoSet() const -> const TaskInfoSet& {
    QMutexLocker locker{ &d->mutex };
    return d->taskInfoSet;
}
