#include "FLDataSetGetterStitchingResult.h"

#include "H5Cpp.h"

class FLDataSetGetterStitchingResult::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString resultFilePath{};
};

FLDataSetGetterStitchingResult::FLDataSetGetterStitchingResult() : d(std::make_unique<Impl>()) {
}

FLDataSetGetterStitchingResult::~FLDataSetGetterStitchingResult() = default;

auto FLDataSetGetterStitchingResult::SetResultFilePath(const QString& resultFilePath) -> void {
    d->resultFilePath = resultFilePath;
}

auto FLDataSetGetterStitchingResult::GetData() const -> std::shared_ptr<float[]> {
    const H5::H5File resultFile{ d->resultFilePath.toStdString(),H5F_ACC_RDONLY };

    const auto dataSet3D = resultFile.openDataSet("StitchingData3D");
    const auto dataSpace = dataSet3D.getSpace();

    hsize_t dims[3];
    dataSpace.getSimpleExtentDims(dims);

    const auto dataSizeX = dims[2];
    const auto dataSizeY = dims[1];
    const auto dataSizeZ = dims[0];

    const auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

    const std::shared_ptr<uint16_t[]> rawData{ new uint16_t[numberOfElements] };
    dataSet3D.read(rawData.get(), dataSet3D.getDataType());

    const std::shared_ptr<float[]> floatData{ new float[numberOfElements] };
    for (hsize_t index = 0; index < numberOfElements; ++index) {
        floatData.get()[index] = static_cast<float>(rawData.get()[index]);
    }

    return floatData;
}

auto FLDataSetGetterStitchingResult::GetData(const int32_t& x0, const int32_t& x1, const int32_t& y0, const int32_t& y1,
    const int32_t& z0, const int32_t& z1) const -> std::shared_ptr<float[]> {
    const H5::H5File resultFile{ d->resultFilePath.toStdString(),H5F_ACC_RDONLY };

    const auto dataSet3D = resultFile.openDataSet("StitchingData3D");
    const auto wholeDataSpace = dataSet3D.getSpace();

    hsize_t dims[3];
    wholeDataSpace.getSimpleExtentDims(dims);

    const auto dataSizeX = static_cast<hsize_t>(x1 - x0 + 1);
    const auto dataSizeY = static_cast<hsize_t>(y1 - y0 + 1);
    const auto dataSizeZ = static_cast<hsize_t>(z1 - z0 + 1);
    const auto numberOfElements = dataSizeX * dataSizeY * dataSizeZ;

    hsize_t count[3] = { dataSizeZ, dataSizeY, dataSizeX };
    hsize_t start[3] = { static_cast<hsize_t>(z0), static_cast<hsize_t>(y0), static_cast<hsize_t>(x0) };

    const auto sectionDataSpace = H5::DataSpace{ 3,count };

    const auto selectedDataSpace = wholeDataSpace;
    selectedDataSpace.selectHyperslab(H5S_SELECT_SET, count, start);

    const std::shared_ptr<uint16_t[]> rawData{ new uint16_t[numberOfElements] };
    dataSet3D.read(rawData.get(), dataSet3D.getDataType(), sectionDataSpace, selectedDataSpace);

    std::shared_ptr<float[]> floatData{ new float[numberOfElements] };
    for (hsize_t index = 0; index < numberOfElements; ++index) {
        floatData.get()[index] = static_cast<float>(rawData.get()[index]);
    }

    return floatData;
}

auto FLDataSetGetterStitchingResult::GetDataMemoryOrder() const -> MemoryOrder3D {
    return MemoryOrder3D::XYZ;
}

auto FLDataSetGetterStitchingResult::GetMIPData() const -> std::shared_ptr<float[]> {
    const H5::H5File resultFile{ d->resultFilePath.toStdString(),H5F_ACC_RDONLY };

    const auto dataSetMIP = resultFile.openDataSet("StitchingDataMIP");
    const auto dataSpace = dataSetMIP.getSpace();

    hsize_t dims[2];
    dataSpace.getSimpleExtentDims(dims);

    const auto dataSizeX = dims[1];
    const auto dataSizeY = dims[0];

    const auto numberOfElements = dataSizeX * dataSizeY;

    const std::shared_ptr<uint16_t[]> rawData{ new uint16_t[numberOfElements] };
    dataSetMIP.read(rawData.get(), dataSetMIP.getDataType());

    std::shared_ptr<float[]> floatData{ new float[numberOfElements] };
    for (hsize_t index = 0; index < numberOfElements; ++index) {
        floatData.get()[index] = static_cast<float>(rawData.get()[index]);
    }

    return floatData;
}

auto FLDataSetGetterStitchingResult::GetMIPData(const int32_t& x0, const int32_t& x1, const int32_t& y0,
    const int32_t& y1) const -> std::shared_ptr<float[]> {
    const H5::H5File resultFile{ d->resultFilePath.toStdString(),H5F_ACC_RDONLY };

    const auto dataSetMIP = resultFile.openDataSet("StitchingDataMIP");
    const auto wholeDataSpace = dataSetMIP.getSpace();

    hsize_t dims[2];
    wholeDataSpace.getSimpleExtentDims(dims);

    const auto dataSizeX = static_cast<hsize_t>(x1 - x0 + 1);
    const auto dataSizeY = static_cast<hsize_t>(y1 - y0 + 1);
    const auto numberOfElements = dataSizeX * dataSizeY;

    hsize_t count[2] = { dataSizeY, dataSizeX };
    hsize_t start[2] = { static_cast<hsize_t>(y0), static_cast<hsize_t>(x0) };

    const auto sectionDataSpace = H5::DataSpace{ 2,count };

    const auto selectedDataSpace = wholeDataSpace;
    selectedDataSpace.selectHyperslab(H5S_SELECT_SET, count, start);

    const std::shared_ptr<uint16_t[]> rawData{ new uint16_t[numberOfElements] };
    dataSetMIP.read(rawData.get(), dataSetMIP.getDataType(), sectionDataSpace, selectedDataSpace);

    std::shared_ptr<float[]> floatData{ new float[numberOfElements] };
    for (hsize_t index = 0; index < numberOfElements; ++index) {
        floatData.get()[index] = static_cast<float>(rawData.get()[index]);
    }

    return floatData;
}

auto FLDataSetGetterStitchingResult::GetMIPDataMemoryOrder() const -> MemoryOrder2D {
    return MemoryOrder2D::XY;
}
