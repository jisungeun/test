#include "AcquisitionPositionReader.h"

#include <QSettings>

class AcquisitionPositionReader::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString filePath;

    double positionX;
    double positionY;
    double positionZ;
    double positionC;
    const LengthUnit positionUnit{ LengthUnit::Millimenter };
};

AcquisitionPositionReader::AcquisitionPositionReader() : d(std::make_unique<Impl>()) {
}

AcquisitionPositionReader::~AcquisitionPositionReader() = default;

auto AcquisitionPositionReader::SetFilePath(const QString& filePath) -> void {
    d->filePath = filePath;
}

auto AcquisitionPositionReader::Read() -> bool {
    const QSettings positionFile{ d->filePath, QSettings::IniFormat };

    bool conversionPass = true;

    const auto positionX = positionFile.value("X", "0").toString().toDouble(&conversionPass);
    if (!conversionPass) return false;
    const auto positionY = positionFile.value("Y", "0").toString().toDouble(&conversionPass);
    if (!conversionPass) return false;
    const auto positionZ = positionFile.value("Z", "0").toString().toDouble(&conversionPass);
    if (!conversionPass) return false;
    const auto positionC = positionFile.value("C", "0").toString().toDouble(&conversionPass);
    if (!conversionPass) return false;


    d->positionX = ConvertUnit(positionX, LengthUnit::Millimenter, d->positionUnit);
    d->positionY = ConvertUnit(positionY, LengthUnit::Millimenter, d->positionUnit);
    d->positionZ = ConvertUnit(positionZ, LengthUnit::Millimenter, d->positionUnit);
    d->positionC = ConvertUnit(positionC, LengthUnit::Millimenter, d->positionUnit);

    return true;
}

auto AcquisitionPositionReader::GetPositionX(const LengthUnit& unit) const -> double {
    return ConvertUnit(d->positionX, d->positionUnit, unit);
}

auto AcquisitionPositionReader::GetPositionY(const LengthUnit& unit) const -> double {
    return ConvertUnit(d->positionY, d->positionUnit, unit);
}

auto AcquisitionPositionReader::GetPositionZ(const LengthUnit& unit) const -> double {
    return ConvertUnit(d->positionZ, d->positionUnit, unit);
}

auto AcquisitionPositionReader::GetPositionC(const LengthUnit& unit) const -> double {
    return ConvertUnit(d->positionC, d->positionUnit, unit);
}
