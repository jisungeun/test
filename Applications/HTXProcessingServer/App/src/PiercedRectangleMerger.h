#pragma once

#include <memory>
#include <QList>

#include "PiercedRectangle.h"

class PiercedRectangleMerger {
public:
    PiercedRectangleMerger();
    ~PiercedRectangleMerger();

    auto SetPiercedRectangleList(const QList<PiercedRectangle>& piercedRectangleList)->void;

    auto Merge()->bool;

    auto GetMergedPiercedRectangle()const->const PiercedRectangle&;

private:
    class Impl;
    std::unique_ptr<Impl> d;
};
