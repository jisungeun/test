#define LOGGER_TAG "[ProcessorBF]"

#include "ProcessorBF.h"

#include <QDir>
#include <QStandardPaths>

#include "H5Cpp.h"
#include "arrayfire.h"

#include "BFProcessor.h"
#include "BFProcessorInput.h"
#include "BFWriterHDF5.h"
#include "IBFProcessorOutput.h"
#include "IBFWriterOutput.h"
#include "TCFBFCompleter.h"
#include "TCLogger.h"

#include "HDF5Mutex.h"
#include "DataFolderPathGetter.h"
#include "ResultFilePathGenerator.h"

class BFProcessorOutput final : public IBFProcessorOutput {
public:
    BFProcessorOutput() = default;
    ~BFProcessorOutput() = default;

    auto SetBFProcessorResult(const BFProcessorResult& bfProcessorResult) -> void override {
        this->result = bfProcessorResult;
    }

    auto GetBFWriterInput()->BFWriterInput {
        const auto data = result.GetData();
        const auto dataSizeX = result.GetDataSizeX();
        const auto dataSizeY = result.GetDataSizeY();
        const auto channelCount = result.GetChannelCount();

        const auto pixelWorldSizeX = result.GetPixelWorldSizeX(LengthUnit::Micrometer);
        const auto pixelWorldSizeY = result.GetPixelWorldSizeY(LengthUnit::Micrometer);

        BFWriterInput bfWriterInput;
        bfWriterInput.SetData(data);
        bfWriterInput.SetDataSize(dataSizeX, dataSizeY);
        bfWriterInput.SetChannelCount(channelCount);
        bfWriterInput.SetPixelWorldSize(pixelWorldSizeX, pixelWorldSizeY, LengthUnit::Micrometer);

        return bfWriterInput;
    }
private:
    BFProcessorResult result;
};

class BFWriterOutput final : public IBFWriterOutput {
public:
    BFWriterOutput() = default;
    ~BFWriterOutput() = default;

    auto SetBFWriterResult(const BFWriterResult& htWriterResult) -> void override {
        this->result = htWriterResult;
    }

    BFWriterResult result;
};

class ProcessorBF::Impl {
public:
    Impl() = default;
    ~Impl() = default;

    QString rootFolderPath{};

    AcquisitionConfig acquisitionConfig{};
};

ProcessorBF::ProcessorBF() : d(new Impl()) {
}

ProcessorBF::~ProcessorBF() = default;

auto ProcessorBF::SetRootFolderPath(const QString& rootFolderPath) -> void {
    d->rootFolderPath = rootFolderPath;
}

auto ProcessorBF::SetAcquisitionConfig(const AcquisitionConfig& acquisitionConfig) -> void {
    d->acquisitionConfig = acquisitionConfig;
}

auto ProcessorBF::Process(const int32_t& tileIndex, const int32_t& timeIndex) -> bool {
    const auto& acquisitionPosition = d->acquisitionConfig.GetAcquisitionPosition();
    const auto& deviceInfo = d->acquisitionConfig.GetDeviceInfo();

    const auto magnificationOfSystem = deviceInfo.magnification;
    const auto pixelSizeOfImagingSensor = deviceInfo.pixelSizeMicrometer;

    const ModalityType modalityType{ ModalityType::Name::BF };

    DataFolderPathGetter dataFolderPathGetter;
    dataFolderPathGetter.SetRootFolderPath(d->rootFolderPath);
    dataFolderPathGetter.SetModalityType(modalityType);
    dataFolderPathGetter.SetTileIndex(tileIndex);
    dataFolderPathGetter.SetTimeIndex(timeIndex);

    if (!dataFolderPathGetter.ExistTimeFrameFolder()) {
        QLOG_ERROR() << "dataFolderPathGetter.ExistTimeFrameFolder() fails in Process()";
        return false;
    }

    const auto sampleDataFolderPath = dataFolderPathGetter.GetTimeFrameFolderPath();

    QLOG_INFO() << "BF processing tile(" << tileIndex << ") time(" << timeIndex << ")";
    QLOG_INFO() << sampleDataFolderPath;

    BFProcessorInput bfProcessorInput;
    bfProcessorInput.SetMagnificationOfSystem(magnificationOfSystem);
    bfProcessorInput.SetPixelSizeOfImagingSensor(pixelSizeOfImagingSensor, LengthUnit::Micrometer);
    bfProcessorInput.SetSampleDataFolderPath(sampleDataFolderPath);

    const QString processingTempFolderPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation) + "/temp/BF";
    if (!QDir().exists(processingTempFolderPath)) {
        QDir().mkpath(processingTempFolderPath);
    }

    IBFProcessorOutput::Pointer bfProcessorOutputPort{ new BFProcessorOutput };

    BFProcessor bfProcessor;
    bfProcessor.SetBFProcessorInput(bfProcessorInput);
    bfProcessor.SetOutputPort(bfProcessorOutputPort);

    if (!bfProcessor.Process()) {
        QLOG_ERROR() << "bfProcessor.Process() fails";
        return false;
    }

    const auto bfWriterInput = std::dynamic_pointer_cast<BFProcessorOutput>(bfProcessorOutputPort)->GetBFWriterInput();

    IBFWriterOutput::Pointer bfWriterOutputPort{ new BFWriterOutput };
    
    const QString writingTempFolderPath = d->rootFolderPath + "/temp/BF";
    if (!QDir().mkpath(writingTempFolderPath)) {
        return false;
    }

    const auto bfProcessedDataFilePath = 
        ResultFilePathGenerator::GetBFProcessedFilePath(d->rootFolderPath, tileIndex, timeIndex);

    BFWriterHDF5 bfWriterHDF5;
    bfWriterHDF5.SetTargetFilePath(bfProcessedDataFilePath);
    bfWriterHDF5.SetInput(bfWriterInput);
    bfWriterHDF5.SetOutputPort(bfWriterOutputPort);

    {
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        if (!bfWriterHDF5.Write()) {
            QLOG_ERROR() << "bfWriterHDF5.Write() fails";
            return false;
        }
    }

    return true;
}
