#include <catch2/catch.hpp>

#include "AcquisitionCount.h"

namespace AcquisitionCountTest {
    TEST_CASE("AcquisitionCount : unit test") {
        SECTION("AcquisitionCount()") {
            AcquisitionCount acquisitionCount;
            CHECK(&acquisitionCount != nullptr);
        }
        SECTION("AcquisitionCount(other)") {
            AcquisitionCount srcAcquisitionCount;
            srcAcquisitionCount.SetTileNumber(1, 2);

            const AcquisitionCount destAcquisitionCount(srcAcquisitionCount);
            const auto [copyTileNumberX, copyTileNumberY] = destAcquisitionCount.GetTileNumber();
            CHECK(copyTileNumberX == 1);
            CHECK(copyTileNumberY == 2);
        }
        SECTION("operator=()") {
            AcquisitionCount srcAcquisitionCount;
            srcAcquisitionCount.SetTileNumber(1, 2);

            AcquisitionCount destAcquisitionCount;
            destAcquisitionCount = srcAcquisitionCount;
            const auto [copyTileNumberX, copyTileNumberY] = destAcquisitionCount.GetTileNumber();
            CHECK(copyTileNumberX == 1);
            CHECK(copyTileNumberY == 2);
        }
        SECTION("SetAcquisitionSequenceInfo()") {
            AcquisitionCount acquisitionCount;
            acquisitionCount.SetAcquisitionSequenceInfo({});

            CHECK(&acquisitionCount != nullptr);
        }
        SECTION("GetAcquisitionSequence()") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            const auto resultAcquisitionSequenceInfo = acquisitionCount.GetAcquisitionSequenceInfo();
            CHECK(resultAcquisitionSequenceInfo.GetTimeFrameCount() == 1);
        }
        SECTION("SetTileNumber()") {
            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 2);
            CHECK(&acquisitionCount != nullptr);
        }
        SECTION("GetTileNumber()") {
            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 2);

            CHECK(std::get<0>(acquisitionCount.GetTileNumber()) == 1);
            CHECK(std::get<1>(acquisitionCount.GetTileNumber()) == 2);
        }
    }
    TEST_CASE("AcquisitionCount : practical test") {
        AcquisitionSequenceInfo acquisitionSequenceInfo;
        acquisitionSequenceInfo.Initialize(2);

        constexpr auto tileNumberX = 2;
        constexpr auto tileNumberY = 2;

        AcquisitionCount acquisitionCount;
        acquisitionCount.SetTileNumber(tileNumberX, tileNumberY);
        acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

        const auto [resultTileNumberX, resultTileNumberY] = acquisitionCount.GetTileNumber();
        const auto resultAcquisitionSequenceInfo = acquisitionCount.GetAcquisitionSequenceInfo();

        CHECK(resultTileNumberX == 2);
        CHECK(resultTileNumberY == 2);
        CHECK(resultAcquisitionSequenceInfo.IsInitialized() == true);
        CHECK(resultAcquisitionSequenceInfo.GetTimeFrameCount() == 2);
    }
}