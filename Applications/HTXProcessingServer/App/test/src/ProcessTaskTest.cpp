#include <catch2/catch.hpp>

#include "ProcessTask.h"

namespace ProcessTaskTest {
    TEST_CASE("ProcessTask : unit test") {
        SECTION("ProcessTask()") {
            ProcessTask processTask;
            CHECK(&processTask != nullptr);
        }
        SECTION("ProcessTask(other)") {
            ProcessTask srcProcessTask;
            srcProcessTask.SetTimeFrameIndex(1);

            ProcessTask destProcessTask(srcProcessTask);
            CHECK(destProcessTask.GetTimeFrameIndex() == 1);
        }
        SECTION("operator=()") {
            ProcessTask srcProcessTask;
            srcProcessTask.SetTimeFrameIndex(1);

            ProcessTask destProcessTask;
            destProcessTask = srcProcessTask;
            CHECK(destProcessTask.GetTimeFrameIndex() == 1);
        }
        SECTION("SetRootFolderPath()") {
            ProcessTask processTask;
            processTask.SetRootFolderPath("");
            CHECK(&processTask != nullptr);
        }
        SECTION("SetProcessType()") {
            ProcessTask processTask;
            processTask.SetProcessType(ProcessType::TileProcessing);
            CHECK(&processTask != nullptr);
        }
        SECTION("SetModalityType()") {
            ProcessTask processTask;
            processTask.SetModalityType(ModalityType{ ModalityType::Name::HT });
            CHECK(&processTask != nullptr);
        }
        SECTION("SetTimeFrameIndex()") {
            ProcessTask processTask;
            processTask.SetTimeFrameIndex(0);
            CHECK(&processTask != nullptr);
        }
        SECTION("SetTileIndex()") {
            ProcessTask processTask;
            processTask.SetTileIndex(0);
            CHECK(&processTask != nullptr);
        }
        SECTION("GetRootFolderPath()") {
            ProcessTask processTask;
            processTask.SetRootFolderPath("root");
            CHECK(processTask.GetRootFolderPath() == "root");
        }
        SECTION("GetProcessType()") {
            ProcessTask processTask;
            processTask.SetProcessType(ProcessType::TileProcessing);
            CHECK(processTask.GetProcessType() == +ProcessType::TileProcessing);
        }
        SECTION("GetModalityType()"){
            ProcessTask processTask;
            processTask.SetModalityType(ModalityType{ ModalityType::Name::HT });
            CHECK(processTask.GetModalityType() == ModalityType{ ModalityType::Name::HT });
        }
        SECTION("GetTimeFrameIndex()"){
            ProcessTask processTask;
            processTask.SetTimeFrameIndex(1);
            CHECK(processTask.GetTimeFrameIndex() == 1);
        }
        SECTION("GetTileIndex()") {
            ProcessTask processTask;
            processTask.SetTileIndex(1);
            CHECK(processTask.GetTileIndex() == 1);
        }
    }
    TEST_CASE("ProcessTask : practical test") {
        //TODO Implement test
    }
}