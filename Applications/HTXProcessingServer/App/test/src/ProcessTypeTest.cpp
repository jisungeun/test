#include <catch2/catch.hpp>

#include "ProcessType.h"

namespace ProcessTypeTest {
    TEST_CASE("ProcessType : unit test") {
        CHECK(ProcessType::_size() == 8);
        CHECK(ProcessType::_from_index(0) == +ProcessType::None);
        CHECK(ProcessType::_from_index(1) == +ProcessType::TileProcessing);
        CHECK(ProcessType::_from_index(2) == +ProcessType::Stitching);
        CHECK(ProcessType::_from_index(3) == +ProcessType::ThumbnailWriting);
        CHECK(ProcessType::_from_index(4) == +ProcessType::StitchingWriting);
        CHECK(ProcessType::_from_index(5) == +ProcessType::LDMConversion);
        CHECK(ProcessType::_from_index(6) == +ProcessType::TCFWriting);
        CHECK(ProcessType::_from_index(7) == +ProcessType::DataRemoving);
    }
}
