#include <QFile>
#include <catch2/catch.hpp>

#include "ThumbnailImageWriter.h"

#include "BinaryFileIO.h"

namespace ThumbnailImageWriterTest {
    const QString testFolderPath = TEST_DATA_FOLDR_PATH + QString("/HTXProcessingServerTest/ImageData");
    const QString imageDataFilePath = testFolderPath + "/image.dat";

    TEST_CASE("ThumbnailImageWriter : unit test") {
        SECTION("ThumbnailImageWriter()") {
            ThumbnailImageWriter writer;
            CHECK(&writer != nullptr);
        }
        SECTION("SetRootFoldePath()") {
            ThumbnailImageWriter writer;
            writer.SetRootFolderPath("");
            CHECK(&writer != nullptr);
        }
        SECTION("WriteHT()") {
            constexpr auto imageSizeX = 1416;
            constexpr auto imageSizeY = 1416;
            constexpr auto numberOfElements = imageSizeX * imageSizeY;

            const auto imageData = ReadFile_uint8_t(imageDataFilePath.toStdString(), numberOfElements);

            ThumbnailImageWriter writer;
            writer.SetRootFolderPath("D:/temp");
            
            CHECK(writer.WriteHT(imageData, imageSizeX, imageSizeY, 0) == true);
            CHECK(QFile::exists("D:/temp/thumbnail/HT_0000.png") == true);
            QFile::remove("D:/temp/thumbnail/HT_0000.png");

            CHECK(writer.WriteHT(imageData, imageSizeX, imageSizeY, 1) == true);
            CHECK(QFile::exists("D:/temp/thumbnail/HT_0001.png") == true);
            QFile::remove("D:/temp/thumbnail/HT_0001.png");
        }
        SECTION("WriteFL()") {
            constexpr auto imageSizeX = 1416;
            constexpr auto imageSizeY = 1416;
            constexpr auto numberOfElements = imageSizeX * imageSizeY;

            const auto imageData = ReadFile_uint8_t(imageDataFilePath.toStdString(), numberOfElements);

            std::shared_ptr<uint8_t[]> mergedImageData{ new uint8_t[numberOfElements * 3] };

            std::copy_n(imageData.get(), numberOfElements, mergedImageData.get());
            std::copy_n(imageData.get(), numberOfElements, mergedImageData.get() + numberOfElements);
            std::copy_n(imageData.get(), numberOfElements, mergedImageData.get() + 2 * numberOfElements);

            ThumbnailImageWriter writer;
            writer.SetRootFolderPath("D:/temp");

            CHECK(writer.WriteFL(mergedImageData, imageSizeX, imageSizeY, 0) == true);
            CHECK(QFile::exists("D:/temp/thumbnail/FL_0000.png") == true);
            QFile::remove("D:/temp/thumbnail/FL_0000.png");

            CHECK(writer.WriteFL(mergedImageData, imageSizeX, imageSizeY, 1) == true);
            CHECK(QFile::exists("D:/temp/thumbnail/FL_0001.png") == true);
            QFile::remove("D:/temp/thumbnail/FL_0001.png");
        }
        SECTION("WriteBF()") {
            
        }
    }
    TEST_CASE("ThumbnailImageWriter : practical test") {
        constexpr auto imageSizeX = 1416;
        constexpr auto imageSizeY = 1416;
        constexpr auto numberOfElements = imageSizeX * imageSizeY;

        const auto imageData = ReadFile_uint8_t(imageDataFilePath.toStdString(), numberOfElements);

        std::shared_ptr<uint8_t[]> colorImageData{ new uint8_t[numberOfElements * 3]() };

        std::copy_n(imageData.get(), numberOfElements, colorImageData.get());
        std::copy_n(imageData.get(), numberOfElements, colorImageData.get() + numberOfElements);
        std::copy_n(imageData.get(), numberOfElements, colorImageData.get() + 2 * numberOfElements);

        ThumbnailImageWriter writer;
        writer.SetRootFolderPath("D:/temp");

        CHECK(writer.WriteBF(imageData, imageSizeX, imageSizeY, 1, 0));
        CHECK(QFile::exists("D:/temp/thumbnail/BF_0000.png"));
        QFile::remove("D:/temp/thumbnail/BF_0000.png");

        CHECK(writer.WriteBF(colorImageData, imageSizeX, imageSizeY, 3, 1));
        CHECK(QFile::exists("D:/temp/thumbnail/BF_0001.png"));
        QFile::remove("D:/temp/thumbnail/BF_0001.png");
    }
}