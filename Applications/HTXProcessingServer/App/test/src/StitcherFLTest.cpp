#include <catch2/catch.hpp>

#include "StitcherFL.h"

namespace StitcherFLTest {
    TEST_CASE("StitcherFL : unit test") {
        //TODO Implement unit test
        SECTION("StitcherFL()") {}
        SECTION("SetRootFolderPath()") {}
        SECTION("Stitch()") {}
    }
    TEST_CASE("StitcherFL : practical test") {
        //TODO Change the rootFolderPath to read data from test directory
        const QString rootFolderPath = "E:/00_Data/20220406 HTX Tile Square Image Data HT FL/sampling/20220405.193549.TestProject.VAL-B02.002.Hep3B mitoDsRed.Well_1_1.P000";

        StitcherFL stitcherFL;
        stitcherFL.SetRootFolderPath(rootFolderPath);
        stitcherFL.SetChannelIndex(0);
        stitcherFL.Stitch(0);
        
    }
}