#include <catch2/catch.hpp>

#include "AcquisitionDataInfo.h"

namespace AcquisitionDataInfoTest {
    TEST_CASE("AcquisitionDataInfo : unit test") {
        SECTION("AcquisitionDataInfo()") {
            AcquisitionDataInfo acquisitionDataInfo;
            CHECK(&acquisitionDataInfo != nullptr);
        }
        SECTION("AcquisitionDataInfo(other)") {
            AcquisitionDataInfo srcAcquisitionDataInfo;
            srcAcquisitionDataInfo.SetRootFolderPath("root");

            AcquisitionDataInfo destAcquisitionDataInfo(srcAcquisitionDataInfo);
            CHECK(destAcquisitionDataInfo.GetRootFolderPath() == "root");
        }
        SECTION("operator=()") {
            AcquisitionDataInfo srcAcquisitionDataInfo;
            srcAcquisitionDataInfo.SetRootFolderPath("root");

            AcquisitionDataInfo destAcquisitionDataInfo;
            destAcquisitionDataInfo = srcAcquisitionDataInfo;
            CHECK(destAcquisitionDataInfo.GetRootFolderPath() == "root");
        }
        SECTION("SetRootFolderPath()") {
            AcquisitionDataInfo acquisitionDataInfo;
            acquisitionDataInfo.SetRootFolderPath("");
            CHECK(&acquisitionDataInfo != nullptr);
        }
        SECTION("GetRootFolderPath()") {
            AcquisitionDataInfo acquisitionDataInfo;
            acquisitionDataInfo.SetRootFolderPath("root");
            CHECK(acquisitionDataInfo.GetRootFolderPath() == "root");
        }
        SECTION("SetBackgroundFolderPath()") {
            AcquisitionDataInfo acquisitionDataInfo;
            acquisitionDataInfo.SetBackgroundFolderPath("", true);

            CHECK(&acquisitionDataInfo != nullptr);
        }
        SECTION("GetBackgroundFolderPath()") {
            AcquisitionDataInfo acquisitionDataInfo;
            acquisitionDataInfo.SetBackgroundFolderPath("background", true);

            CHECK(acquisitionDataInfo.GetBackgroundFolderPath() == "background");
        }
        SECTION("IsBackgroundPlacedInRootFolder()") {
            AcquisitionDataInfo acquisitionDataInfo;
            acquisitionDataInfo.SetBackgroundFolderPath("background", true);

            CHECK(acquisitionDataInfo.IsBackgroundPlacedInRootFolder() == true);
        }
        SECTION("SetAcquisitionCount()") {
            AcquisitionDataInfo acquisitionDataInfo;
            acquisitionDataInfo.SetAcquisitionCount({});
            CHECK(&acquisitionDataInfo != nullptr);
        }
        SECTION("GetAcquisitionCount()") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(2);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 2);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquisitionDataInfo acquisitionDataInfo;
            acquisitionDataInfo.SetAcquisitionCount(acquisitionCount);
            const auto resultAcquisitionCount = acquisitionDataInfo.GetAcquisitionCount();

            CHECK(std::get<0>(resultAcquisitionCount.GetTileNumber()) == 1);
            CHECK(std::get<1>(resultAcquisitionCount.GetTileNumber()) == 2);
            CHECK(resultAcquisitionCount.GetAcquisitionSequenceInfo().IsInitialized() == true);
            CHECK(resultAcquisitionCount.GetAcquisitionSequenceInfo().GetTimeFrameCount() == 2);
        }
    }
    TEST_CASE("AcquisitionDataInfo : practical test") {
        AcquisitionSequenceInfo acquisitionSequenceInfo;
        acquisitionSequenceInfo.Initialize(2);
        acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 30);
        acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 1, 30);

        constexpr int32_t tileNumberX = 2;
        constexpr int32_t tileNumberY = 2;

        AcquisitionCount acquisitionCount;
        acquisitionCount.SetTileNumber(tileNumberX, tileNumberY);
        acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

        AcquisitionDataInfo acquisitionDataInfo;
        acquisitionDataInfo.SetRootFolderPath("root");
        acquisitionDataInfo.SetAcquisitionCount(acquisitionCount);
        acquisitionDataInfo.SetBackgroundFolderPath("background", true);

        const auto resultAcquisitionCount = acquisitionDataInfo.GetAcquisitionCount();

        CHECK(acquisitionDataInfo.GetRootFolderPath() == "root");
        CHECK(resultAcquisitionCount.GetAcquisitionSequenceInfo().IsInitialized() == true);
        CHECK(resultAcquisitionCount.GetAcquisitionSequenceInfo().GetTimeFrameCount() == 2);
        CHECK(resultAcquisitionCount.GetAcquisitionSequenceInfo().GetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0) == 30);
        CHECK(resultAcquisitionCount.GetAcquisitionSequenceInfo().GetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 1) == 30);
        CHECK(std::get<0>(resultAcquisitionCount.GetTileNumber()) == tileNumberX);
        CHECK(std::get<1>(resultAcquisitionCount.GetTileNumber()) == tileNumberY);
        CHECK(acquisitionDataInfo.GetBackgroundFolderPath() == "background");
        CHECK(acquisitionDataInfo.IsBackgroundPlacedInRootFolder() == true);
    }
}