#include <catch2/catch.hpp>

#include "PiercedRectangleMerger.h"

namespace PiercedRectangleMergerTest {
    TEST_CASE("PiercedRectangleMerger : unit test") {
        SECTION("PiercedRectangleMerger()") {
            PiercedRectangleMerger piercedRectangleMerger;
            CHECK(&piercedRectangleMerger != nullptr);
        }
        SECTION("SetPiercedRectangleList()") {
            PiercedRectangleMerger piercedRectangleMerger;
            piercedRectangleMerger.SetPiercedRectangleList({});
            CHECK(&piercedRectangleMerger != nullptr);
        }
        SECTION("Merge()") {
            PiercedRectangleMerger piercedRectangleMerger;
            SECTION("without setthing list") {
                CHECK(piercedRectangleMerger.Merge() == false);
            }

            SECTION("with setting list") {
                QList<PiercedRectangle> piercedRectangleList;

                PiercedRectangle rect1;
                rect1.SetSize(50, 50);
                rect1.SetPiercingIndex(23, 25);

                PiercedRectangle rect2;
                rect2.SetSize(50, 50);
                rect2.SetPiercingIndex(25, 23);

                piercedRectangleList.push_back(rect1);
                piercedRectangleList.push_back(rect2);

                piercedRectangleMerger.SetPiercedRectangleList(piercedRectangleList);

                CHECK(piercedRectangleMerger.Merge() == true);
            }
        }
        SECTION("GetMergedPiercedRectangle()") {
            QList<PiercedRectangle> piercedRectangleList;

            PiercedRectangle rect1;
            rect1.SetSize(50, 50);
            rect1.SetPiercingIndex(23, 25);

            PiercedRectangle rect2;
            rect2.SetSize(50, 50);
            rect2.SetPiercingIndex(25, 23);

            piercedRectangleList.push_back(rect1);
            piercedRectangleList.push_back(rect2);

            PiercedRectangleMerger piercedRectangleMerger;
            piercedRectangleMerger.SetPiercedRectangleList(piercedRectangleList);
            piercedRectangleMerger.Merge();

            const auto& mergedRect = piercedRectangleMerger.GetMergedPiercedRectangle();

            CHECK(mergedRect.GetSizeX() == 48);
            CHECK(mergedRect.GetSizeY() == 48);
            CHECK(mergedRect.GetPiercingIndexX() == 23);
            CHECK(mergedRect.GetPiercingIndexX() == 23);
        }
    }
    TEST_CASE("PiercedRectangleMerger : practical test") {
        SECTION("Only one rect") {
            QList<PiercedRectangle> piercedRectangleList;

            PiercedRectangle rect1;
            rect1.SetSize(50, 50);
            rect1.SetPiercingIndex(23, 25);

            piercedRectangleList.push_back(rect1);

            PiercedRectangleMerger piercedRectangleMerger;
            piercedRectangleMerger.SetPiercedRectangleList(piercedRectangleList);
            piercedRectangleMerger.Merge();

            const auto& mergedRect = piercedRectangleMerger.GetMergedPiercedRectangle();

            CHECK(mergedRect.GetSizeX() == 50);
            CHECK(mergedRect.GetSizeY() == 50);
            CHECK(mergedRect.GetPiercingIndexX() == 23);
            CHECK(mergedRect.GetPiercingIndexY() == 25);
        }

        SECTION("Multiple rect") {
            QList<PiercedRectangle> piercedRectangleList;

            PiercedRectangle rect1;
            rect1.SetSize(60, 50);
            rect1.SetPiercingIndex(23, 25);

            PiercedRectangle rect2;
            rect2.SetSize(50, 70);
            rect2.SetPiercingIndex(25, 23);

            piercedRectangleList.push_back(rect1);
            piercedRectangleList.push_back(rect2);

            PiercedRectangleMerger piercedRectangleMerger;
            piercedRectangleMerger.SetPiercedRectangleList(piercedRectangleList);
            piercedRectangleMerger.Merge();

            const auto& mergedRect = piercedRectangleMerger.GetMergedPiercedRectangle();

            CHECK(mergedRect.GetSizeX() == 48);
            CHECK(mergedRect.GetSizeY() == 48);
            CHECK(mergedRect.GetPiercingIndexX() == 23);
            CHECK(mergedRect.GetPiercingIndexX() == 23);
        }
        
    }
}