#include <catch2/catch.hpp>

#include <QSettings>

#include "AcquisitionPositionReader.h"

namespace AcquisitionPositionReaderTest {
    auto WritePositionIniFile(const QString& filePath, const double& x, const double& y, const double& z, const double& c)->void {
        QSettings file(filePath, QSettings::IniFormat);

        file.setValue("x", QString::number(x));
        file.setValue("y", QString::number(y));
        file.setValue("z", QString::number(z));
        file.setValue("c", QString::number(c));
    }

    TEST_CASE("AcquisitionPositionReader : unit test") {
        SECTION("AcquisitionPositionReader()") {
            AcquisitionPositionReader acquisitionPositionReader;
            CHECK(&acquisitionPositionReader != nullptr);
        }
        SECTION("SetFilePath()") {
            AcquisitionPositionReader acquisitionPositionReader;
            acquisitionPositionReader.SetFilePath("");
            CHECK(&acquisitionPositionReader != nullptr);
        }
        SECTION("Read()") {
            const QString filePath = "acquisitionPosition.ini";
            constexpr double x = 1;
            constexpr double y = 2;
            constexpr double z = 3;
            constexpr double c = 4;

            WritePositionIniFile(filePath, x, y, z, c);

            AcquisitionPositionReader acquisitionPositionReader;
            acquisitionPositionReader.SetFilePath(filePath);
            CHECK(acquisitionPositionReader.Read() == true);
        }
        SECTION("GetPositionX()") {
            const QString filePath = "acquisitionPosition.ini";
            constexpr double x = 1;
            constexpr double y = 2;
            constexpr double z = 3;
            constexpr double c = 4;

            WritePositionIniFile(filePath, x, y, z, c);

            AcquisitionPositionReader acquisitionPositionReader;
            acquisitionPositionReader.SetFilePath(filePath);
            acquisitionPositionReader.Read();

            CHECK(acquisitionPositionReader.GetPositionX(LengthUnit::Micrometer) == 1000);
        }
        SECTION("GetPositionY()") {
            const QString filePath = "acquisitionPosition.ini";
            constexpr double x = 1;
            constexpr double y = 2;
            constexpr double z = 3;
            constexpr double c = 4;

            WritePositionIniFile(filePath, x, y, z, c);

            AcquisitionPositionReader acquisitionPositionReader;
            acquisitionPositionReader.SetFilePath(filePath);
            acquisitionPositionReader.Read();

            CHECK(acquisitionPositionReader.GetPositionY(LengthUnit::Micrometer) == 2000);
        }
        SECTION("GetPositionZ()") {
            const QString filePath = "acquisitionPosition.ini";
            constexpr double x = 1;
            constexpr double y = 2;
            constexpr double z = 3;
            constexpr double c = 4;

            WritePositionIniFile(filePath, x, y, z, c);

            AcquisitionPositionReader acquisitionPositionReader;
            acquisitionPositionReader.SetFilePath(filePath);
            acquisitionPositionReader.Read();

            CHECK(acquisitionPositionReader.GetPositionZ(LengthUnit::Micrometer) == 3000);
        }
        SECTION("GetPositionC()") {
            const QString filePath = "acquisitionPosition.ini";
            constexpr double x = 1;
            constexpr double y = 2;
            constexpr double z = 3;
            constexpr double c = 4;

            WritePositionIniFile(filePath, x, y, z, c);

            AcquisitionPositionReader acquisitionPositionReader;
            acquisitionPositionReader.SetFilePath(filePath);
            acquisitionPositionReader.Read();

            CHECK(acquisitionPositionReader.GetPositionC(LengthUnit::Micrometer) == 4000);
        }
    }
    TEST_CASE("AcquisitionPositionReader : practical test") {
        const QString filePath = "acquisitionPosition.ini";
        constexpr double x = 1;
        constexpr double y = 2;
        constexpr double z = 3;
        constexpr double c = 4;

        WritePositionIniFile(filePath, x, y, z, c);

        AcquisitionPositionReader acquisitionPositionReader;
        acquisitionPositionReader.SetFilePath(filePath);

        CHECK(acquisitionPositionReader.Read() == true);
        CHECK(acquisitionPositionReader.GetPositionX(LengthUnit::Micrometer) == 1000);
        CHECK(acquisitionPositionReader.GetPositionY(LengthUnit::Micrometer) == 2000);
        CHECK(acquisitionPositionReader.GetPositionZ(LengthUnit::Micrometer) == 3000);
        CHECK(acquisitionPositionReader.GetPositionC(LengthUnit::Micrometer) == 4000);
    }
}