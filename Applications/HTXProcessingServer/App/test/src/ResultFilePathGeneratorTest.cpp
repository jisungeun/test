#include <QDir>
#include <catch2/catch.hpp>

#include "ResultFilePathGenerator.h"

namespace ResultFilePathGeneratorTest {
    TEST_CASE("ResultFilePathGenerator") {
        const auto rootFolderPath = "rootFolder";
        if (!QFile::exists(rootFolderPath)) {
            QDir().mkpath(rootFolderPath);
        }

        SECTION("GetTempFolderPath()") {
            CHECK(ResultFilePathGenerator::GetTempFolderPath(rootFolderPath) == "rootFolder/temp");
        }
        SECTION("GetTempFilePath()"){
            CHECK(ResultFilePathGenerator::GetTempFilePath(rootFolderPath) == "rootFolder/temp/result.tctmp");
        }
        SECTION("GetHTPrcessedFilePath()"){
            constexpr auto tileIndex = 2;
            constexpr auto timeIndex = 3;

            CHECK(ResultFilePathGenerator::GetHTProcessedFilePath(rootFolderPath, tileIndex, timeIndex) 
                == "rootFolder/temp/HT/ht_Processed_tile0002_time0003");
        }
        SECTION("GetFLPrcessedFilePath()") {
            constexpr auto flChannelIndex = 1;
            constexpr auto tileIndex = 2;
            constexpr auto timeIndex = 3;

            CHECK(ResultFilePathGenerator::GetFLProcessedFilePath(rootFolderPath, flChannelIndex, tileIndex, timeIndex)
                == "rootFolder/temp/FL/fl_Processed_CH1_tile0002_time0003");
        }
        SECTION("GetBFPrcessedFilePath()"){
            constexpr auto tileIndex = 2;
            constexpr auto timeIndex = 3;

            CHECK(ResultFilePathGenerator::GetBFProcessedFilePath(rootFolderPath, tileIndex, timeIndex)
                == "rootFolder/temp/BF/bf_Processed_tile0002_time0003");
        }
        SECTION("GetHTStitchingFilePath()"){
            constexpr auto timeIndex = 3;

            CHECK(ResultFilePathGenerator::GetHTStitchingFilePath(rootFolderPath, timeIndex)
                == "rootFolder/temp/HT/Stitching_time0003");
        }
        SECTION("GetFLStitchingFilePath()") {
            constexpr auto flChannelIndex = 1;
            constexpr auto timeIndex = 3;

            CHECK(ResultFilePathGenerator::GetFLStitchingFilePath(rootFolderPath, flChannelIndex, timeIndex)
                == "rootFolder/temp/FL/Stitching_CH1_time0003");
        }
        SECTION("GetBFStitchingFilePath()"){
            constexpr auto timeIndex = 3;

            CHECK(ResultFilePathGenerator::GetBFStitchingFilePath(rootFolderPath, timeIndex)
                == "rootFolder/temp/BF/Stitching_time0003");
        }
        SECTION("GetTCFFilePath()") {
            CHECK(ResultFilePathGenerator::GetTCFFilePath(rootFolderPath) == "rootFolder/rootFolder.TCF");
        }
        SECTION("GetHTMIPImageFilePath()") {
            CHECK(ResultFilePathGenerator::GetHTMIPImageFilePath(rootFolderPath) == "rootFolder/rootFolder-MIP.PNG");
        }
        SECTION("GetBFImageFilePath()") {
            CHECK(ResultFilePathGenerator::GetBFImageFilePath(rootFolderPath) == "rootFolder/rootFolder-BF.PNG");
        }
        SECTION("GetLDMTCFFilePath()") {
            CHECK(ResultFilePathGenerator::GetLDMTCFFilePath(rootFolderPath) == "rootFolder/rootFolder_LDM.TCF");
        }
    }
}