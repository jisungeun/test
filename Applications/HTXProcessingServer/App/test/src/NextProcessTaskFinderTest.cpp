#include <catch2/catch.hpp>

#include "NextProcessTaskFinder.h"
#include "ProcessTask.h"
#include "TaskRepo.h"

namespace NextProcessTaskFinderTest {
    TEST_CASE("NextProcessTaskFinder : unit test") {
        SECTION("NextProcessTaskFinder()") {
            NextProcessTaskFinder nextProcessTaskFinder;
            CHECK(&nextProcessTaskFinder != nullptr);
        }
        SECTION("Find()") {
            NextProcessTaskFinder nextProcessTaskFinder;
            CHECK(nextProcessTaskFinder.Find() == true);
        }
        SECTION("NextProcessTaskExists()") {
            NextProcessTaskFinder nextProcessTaskFinder;
            
            SECTION("Initial Condition") {
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == false);
            }

            SECTION("with data") {
                auto taskRepoInstance = TaskRepo::GetInstance();
                taskRepoInstance->Clear();
                CHECK(taskRepoInstance->GetRootFolderPathList().isEmpty() == true);

                AcquisitionSequenceInfo acquisitionSequenceInfo;
                acquisitionSequenceInfo.Initialize(1);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 10);

                AcquisitionCount acquisitionCount;
                acquisitionCount.SetTileNumber(1, 1);
                acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

                AcquiredDataFlag acquiredDataFlag;
                acquiredDataFlag.Initialize(acquisitionCount);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);

                TaskInfo taskInfo;
                taskInfo.SetFailed(false);
                taskInfo.SetOnProcessing(false);
                taskInfo.SetProcessedProgress(0);
                taskInfo.SetProcessedTime(0, TimeUnit::Second);

                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, taskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, taskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, taskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, taskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                nextProcessTaskFinder.Find();
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                taskRepoInstance->Clear();
            }

        }
        SECTION("GetNextProcessTask()") {
            NextProcessTaskFinder nextProcessTaskFinder;

            SECTION("Initial Condition") {
                const auto processTask = nextProcessTaskFinder.GetNextProcessTask();
                CHECK(processTask.GetModalityType() == ModalityType::Name::None);
                CHECK(processTask.GetProcessType() == +ProcessType::None);
            }

            SECTION("with data") {
                auto taskRepoInstance = TaskRepo::GetInstance();
                taskRepoInstance->Clear();
                CHECK(taskRepoInstance->GetRootFolderPathList().isEmpty() == true);

                AcquisitionSequenceInfo acquisitionSequenceInfo;
                acquisitionSequenceInfo.Initialize(1);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 10);

                AcquisitionCount acquisitionCount;
                acquisitionCount.SetTileNumber(1, 1);
                acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

                AcquiredDataFlag acquiredDataFlag;
                acquiredDataFlag.Initialize(acquisitionCount);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);

                TaskInfo taskInfo;
                taskInfo.SetFailed(false);
                taskInfo.SetOnProcessing(false);
                taskInfo.SetProcessedProgress(0);
                taskInfo.SetProcessedTime(0, TimeUnit::Second);

                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, taskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, taskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, taskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, taskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                nextProcessTaskFinder.Find();
                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TileProcessing);
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::HT);

                taskRepoInstance->Clear();
            }
        }
    }
    TEST_CASE("NextProcessTaskFinder : practical test") {
        TaskInfo notDoneTaskInfo;
        notDoneTaskInfo.SetProcessedProgress(0);

        TaskInfo doneTaskInfo;
        doneTaskInfo.SetProcessedProgress(1);

        auto taskRepoInstance = TaskRepo::GetInstance();
        taskRepoInstance->Clear();
        CHECK(taskRepoInstance->GetRootFolderPathList().isEmpty() == true);

        SECTION("Only HT") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 10);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquiredDataFlag acquiredDataFlag;
            acquiredDataFlag.Initialize(acquisitionCount);
            acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);

            taskRepoInstance->Clear();
            SECTION("TileProcessing") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TileProcessing);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::HT);
            }
            SECTION("Stitching") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::Stitching);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::HT);
            }
            SECTION("StitchingWriting") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::StitchingWriting);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::HT);
            }
            SECTION("ThumbnailWriting") {
                //TODO Implement ThumbnailWriting test
            }
            SECTION("LDMConversion") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::LDMConversion);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::HT);
            }
        }
        SECTION("Only FL CH0") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 0, 10);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquiredDataFlag acquiredDataFlag;
            acquiredDataFlag.Initialize(acquisitionCount);
            acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, true);

            taskRepoInstance->Clear();
            SECTION("TileProcessing") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TileProcessing);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 0);
            }
            SECTION("Stitching") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::Stitching);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 0);
            }
            SECTION("StitchingWriting") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::StitchingWriting);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 0);
            }
            SECTION("ThumbnailWriting") {
                //TODO
            }
            SECTION("LDMConversion") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::LDMConversion);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 0);
            }
        }
        SECTION("Only FL CH1") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH1, 0, 10);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquiredDataFlag acquiredDataFlag;
            acquiredDataFlag.Initialize(acquisitionCount);
            acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, true);

            taskRepoInstance->Clear();
            SECTION("TileProcessing") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::TileProcessing, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TileProcessing);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 1);
            }
            SECTION("Stitching") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::Stitching);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 1);
            }
            SECTION("StitchingWriting") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::StitchingWriting);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 1);
            }
            SECTION("ThumbnailWriting") {
                //TODO
            }
            SECTION("LDMConversion") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::LDMConversion);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 1);
            }
        }
        SECTION("Only FL CH2") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH2, 0, 10);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquiredDataFlag acquiredDataFlag;
            acquiredDataFlag.Initialize(acquisitionCount);
            acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, true);

            taskRepoInstance->Clear();
            SECTION("TileProcessing") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::TileProcessing, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TileProcessing);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 2);
            }
            SECTION("Stitching") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::Stitching);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 2);
            }
            SECTION("StitchingWriting") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::StitchingWriting);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 2);
            }
            SECTION("ThumbnailWriting") {
                //TODO
            }
            SECTION("LDMConversion") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::LDMConversion);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 2);
            }
        }
        SECTION("Only BF") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::BF, 0, 3);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquiredDataFlag acquiredDataFlag;
            acquiredDataFlag.Initialize(acquisitionCount);
            acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::BF }, true);

            taskRepoInstance->Clear();
            SECTION("TileProcessing") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TileProcessing);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::BF);
            }
            SECTION("Stitching") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::Stitching);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::BF);
            }
            SECTION("StitchingWriting") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::StitchingWriting);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::BF);
            }
            SECTION("ThumbnailWriting") {
                //TODO Implement ThumbnailWriting test
            }
            SECTION("LDMConversion") {
                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::LDMConversion);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::BF);
            }
        }

        SECTION("FLCH0") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 0, 10);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquiredDataFlag acquiredDataFlag;
            acquiredDataFlag.Initialize(acquisitionCount);

            taskRepoInstance->Clear();

            SECTION("HT Stitching Not Done, FLCH0 TileProcessing Done") {
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, true);

                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::Stitching);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 0);
            }
        }

        SECTION("HT + FLCH0 + FLCH1 + FLCH2 + BF") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 10);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 0, 10);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH1, 0, 10);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH2, 0, 10);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::BF, 0, 3);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquiredDataFlag acquiredDataFlag;
            acquiredDataFlag.Initialize(acquisitionCount);

            taskRepoInstance->Clear();

            SECTION("No Data Missing"){
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::BF }, true);

                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TileProcessing);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::HT);
            }

            SECTION("HT Data Missing") {
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, false);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::BF }, true);

                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TileProcessing);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 0);
            }

            SECTION("HT Done, FLCH0 Missing") {
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, false);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::BF }, true);

                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TileProcessing);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 1);
            }

            SECTION("HT Done, FLCH0 Missing") {
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, false);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::BF }, true);

                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TileProcessing);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 1);
            }

            SECTION("all process done") {
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, true);
                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::BF }, true);

                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::LDMConversion, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 2 }, ProcessType::LDMConversion, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::LDMConversion, doneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == -1);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TCFWriting);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::None);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == -1);
            }
        }

        SECTION("Multi data") {
            SECTION("Two Data : first data FLCH0 missing") {
                taskRepoInstance->Clear();
                {
                    AcquisitionSequenceInfo acquisitionSequenceInfo1;
                    acquisitionSequenceInfo1.Initialize(1);
                    acquisitionSequenceInfo1.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 10);
                    acquisitionSequenceInfo1.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 0, 10);

                    AcquisitionCount acquisitionCount1;
                    acquisitionCount1.SetTileNumber(1, 1);
                    acquisitionCount1.SetAcquisitionSequenceInfo(acquisitionSequenceInfo1);

                    AcquiredDataFlag acquiredDataFlag1;
                    acquiredDataFlag1.Initialize(acquisitionCount1);

                    acquiredDataFlag1.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);
                    acquiredDataFlag1.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, false);

                    TaskInfoSet taskInfoSet1;
                    taskInfoSet1.Initialize(acquisitionCount1);
                    taskInfoSet1.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                    taskInfoSet1.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                    taskInfoSet1.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                    taskInfoSet1.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                    taskRepoInstance->AddNotDone("rootFolderPath1", acquiredDataFlag1, taskInfoSet1, acquisitionCount1);
                }
                {
                    AcquisitionSequenceInfo acquisitionSequenceInfo2;
                    acquisitionSequenceInfo2.Initialize(1);
                    acquisitionSequenceInfo2.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 10);

                    AcquisitionCount acquisitionCount2;
                    acquisitionCount2.SetTileNumber(1, 1);
                    acquisitionCount2.SetAcquisitionSequenceInfo(acquisitionSequenceInfo2);

                    AcquiredDataFlag acquiredDataFlag2;
                    acquiredDataFlag2.Initialize(acquisitionCount2);

                    acquiredDataFlag2.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);

                    TaskInfoSet taskInfoSet2;
                    taskInfoSet2.Initialize(acquisitionCount2);

                    taskRepoInstance->AddNotDone("rootFolderPath2", acquiredDataFlag2, taskInfoSet2, acquisitionCount2);
                }


                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath2");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TileProcessing);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::HT);
            }

            SECTION("Two Data : first data task done except TCFWriting") {
                taskRepoInstance->Clear();
                {
                    AcquisitionSequenceInfo acquisitionSequenceInfo1;
                    acquisitionSequenceInfo1.Initialize(1);
                    acquisitionSequenceInfo1.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 10);
                    acquisitionSequenceInfo1.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 0, 10);

                    AcquisitionCount acquisitionCount1;
                    acquisitionCount1.SetTileNumber(1, 1);
                    acquisitionCount1.SetAcquisitionSequenceInfo(acquisitionSequenceInfo1);

                    AcquiredDataFlag acquiredDataFlag1;
                    acquiredDataFlag1.Initialize(acquisitionCount1);

                    acquiredDataFlag1.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);
                    acquiredDataFlag1.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, true);

                    TaskInfoSet taskInfoSet1;
                    taskInfoSet1.Initialize(acquisitionCount1);
                    taskInfoSet1.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                    taskInfoSet1.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                    taskInfoSet1.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                    taskInfoSet1.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);
                    taskInfoSet1.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing, doneTaskInfo);
                    taskInfoSet1.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching, doneTaskInfo);
                    taskInfoSet1.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting, doneTaskInfo);
                    taskInfoSet1.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion, doneTaskInfo);

                    taskRepoInstance->AddNotDone("rootFolderPath1", acquiredDataFlag1, taskInfoSet1, acquisitionCount1);
                }
                {
                    AcquisitionSequenceInfo acquisitionSequenceInfo2;
                    acquisitionSequenceInfo2.Initialize(1);
                    acquisitionSequenceInfo2.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 10);

                    AcquisitionCount acquisitionCount2;
                    acquisitionCount2.SetTileNumber(1, 1);
                    acquisitionCount2.SetAcquisitionSequenceInfo(acquisitionSequenceInfo2);

                    AcquiredDataFlag acquiredDataFlag2;
                    acquiredDataFlag2.Initialize(acquisitionCount2);

                    acquiredDataFlag2.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);

                    TaskInfoSet taskInfoSet2;
                    taskInfoSet2.Initialize(acquisitionCount2);

                    taskRepoInstance->AddNotDone("rootFolderPath2", acquiredDataFlag2, taskInfoSet2, acquisitionCount2);
                }


                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath1");
                CHECK(nextProcessTask.GetTimeFrameIndex() == -1);
                CHECK(nextProcessTask.GetTileIndex() == -1);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TCFWriting);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::None);
            }

            SECTION("With Fail data") {
                taskRepoInstance->Clear();
                {
                    AcquisitionSequenceInfo acquisitionSequenceInfo1;
                    acquisitionSequenceInfo1.Initialize(1);
                    acquisitionSequenceInfo1.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 10);
                    acquisitionSequenceInfo1.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 0, 10);

                    AcquisitionCount acquisitionCount1;
                    acquisitionCount1.SetTileNumber(1, 1);
                    acquisitionCount1.SetAcquisitionSequenceInfo(acquisitionSequenceInfo1);

                    AcquiredDataFlag acquiredDataFlag1;
                    acquiredDataFlag1.Initialize(acquisitionCount1);

                    acquiredDataFlag1.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);
                    acquiredDataFlag1.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, true);

                    TaskInfoSet taskInfoSet1;
                    taskInfoSet1.Initialize(acquisitionCount1);

                    taskRepoInstance->AddNotDone("rootFolderPath1", acquiredDataFlag1, taskInfoSet1, acquisitionCount1);
                }
                {
                    AcquisitionSequenceInfo acquisitionSequenceInfo2;
                    acquisitionSequenceInfo2.Initialize(1);
                    acquisitionSequenceInfo2.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 10);

                    AcquisitionCount acquisitionCount2;
                    acquisitionCount2.SetTileNumber(1, 1);
                    acquisitionCount2.SetAcquisitionSequenceInfo(acquisitionSequenceInfo2);

                    AcquiredDataFlag acquiredDataFlag2;
                    acquiredDataFlag2.Initialize(acquisitionCount2);

                    acquiredDataFlag2.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);

                    TaskInfoSet taskInfoSet2;
                    taskInfoSet2.Initialize(acquisitionCount2);

                    taskRepoInstance->AddNotDone("rootFolderPath2", acquiredDataFlag2, taskInfoSet2, acquisitionCount2);
                }

                taskRepoInstance->UpdateToFailed("rootFolderPath1");

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath2");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 0);
                CHECK(nextProcessTask.GetTileIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TileProcessing);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::HT);
            }
        }


        SECTION("HeteroTimelapse") {
            SECTION("fl not done") {
                taskRepoInstance->Clear();
                AcquisitionSequenceInfo acquisitionSequenceInfo;
                acquisitionSequenceInfo.Initialize(5);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 1, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 2, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 3, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 4, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 1, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 3, 10);

                AcquisitionCount acquisitionCount;
                acquisitionCount.SetTileNumber(1, 1);
                acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

                AcquiredDataFlag acquiredDataFlag;
                acquiredDataFlag.Initialize(acquisitionCount);

                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(1, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(2, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(3, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(4, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, true);
                acquiredDataFlag.SetAcquiredFlag(3, 0, ModalityType{ ModalityType::Name::FL, 0 }, true);

                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                taskInfoSet.SetTaskInfo(2, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(2, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(2, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(2, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                taskInfoSet.SetTaskInfo(4, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(4, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(4, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(4, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting, notDoneTaskInfo);
                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion, notDoneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == 1);
                CHECK(nextProcessTask.GetTileIndex() == 0);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::Stitching);
                CHECK(nextProcessTask.GetModalityType().GetName() == ModalityType::Name::FL);
                CHECK(nextProcessTask.GetModalityType().GetChannelIndex() == 0);
            }

            SECTION("all done") {
                taskRepoInstance->Clear();
                AcquisitionSequenceInfo acquisitionSequenceInfo;
                acquisitionSequenceInfo.Initialize(5);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 1, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 2, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 3, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 4, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 1, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 3, 10);

                AcquisitionCount acquisitionCount;
                acquisitionCount.SetTileNumber(1, 1);
                acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

                AcquiredDataFlag acquiredDataFlag;
                acquiredDataFlag.Initialize(acquisitionCount);

                acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(1, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(2, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(3, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(4, 0, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, true);
                acquiredDataFlag.SetAcquiredFlag(3, 0, ModalityType{ ModalityType::Name::FL, 0 }, true);

                TaskInfoSet taskInfoSet;
                taskInfoSet.Initialize(acquisitionCount);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                taskInfoSet.SetTaskInfo(2, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(2, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(2, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(2, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                taskInfoSet.SetTaskInfo(4, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(4, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(4, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(4, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, doneTaskInfo);

                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion, doneTaskInfo);

                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing, doneTaskInfo);
                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching, doneTaskInfo);
                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting, doneTaskInfo);
                taskInfoSet.SetTaskInfo(3, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion, doneTaskInfo);

                taskRepoInstance->AddNotDone("rootFolderPath", acquiredDataFlag, taskInfoSet, acquisitionCount);

                NextProcessTaskFinder nextProcessTaskFinder;
                CHECK(nextProcessTaskFinder.Find() == true);
                CHECK(nextProcessTaskFinder.NextProcessTaskExists() == true);

                const auto nextProcessTask = nextProcessTaskFinder.GetNextProcessTask();

                CHECK(nextProcessTask.GetRootFolderPath() == "rootFolderPath");
                CHECK(nextProcessTask.GetTimeFrameIndex() == -1);
                CHECK(nextProcessTask.GetTileIndex() == -1);
                CHECK(nextProcessTask.GetProcessType() == +ProcessType::TCFWriting);
                CHECK(nextProcessTask.GetModalityType() == ModalityType::Name::None);
            }
        }

        taskRepoInstance->Clear();
    }

}
