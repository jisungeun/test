#include <iostream>
#include <catch2/catch.hpp>

#include "AcquisitionScanTargetFinder.h"
#include "TaskRepo.h"

TEST_CASE("AcquisitionScanTargetFinderTest") {
    SECTION("unit test") {
        const auto startTime = std::chrono::system_clock::now();

        constexpr auto timeFrameCount = 300;
        constexpr auto tileNumberX = 30;
        constexpr auto tileNumberY = 30;
        constexpr auto tileCount = tileNumberX * tileNumberY;

        constexpr auto targetTimeIndex = 299;
        constexpr auto targetTileIndex = 899;

        constexpr auto bgAcquiredFlag = true;
        const QString rootFolderPath = "root1";

        AcquisitionSequenceInfo acquisitionSequenceInfo;
        acquisitionSequenceInfo.Initialize(timeFrameCount);

        for (auto timeIndex = 0; timeIndex < timeFrameCount; ++timeIndex) {
            constexpr auto zSliceCount = 1;
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, timeIndex, zSliceCount);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, timeIndex, zSliceCount);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH1, timeIndex, zSliceCount);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH2, timeIndex, zSliceCount);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::BF, timeIndex, zSliceCount);
        }

        AcquisitionCount acquisitionCount;
        acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);
        acquisitionCount.SetTileNumber(tileNumberX, tileNumberY);
        
        AcquiredDataFlag acquiredDataFlag;
        acquiredDataFlag.Initialize(acquisitionCount);
        acquiredDataFlag.SetBGAcquiredFlag(bgAcquiredFlag);

        for (auto timeIndex = 0; timeIndex < targetTimeIndex; ++timeIndex) {
            for (auto tileIndex = 0; tileIndex < tileCount; ++tileIndex) {
                acquiredDataFlag.SetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::HT }, true);
                acquiredDataFlag.SetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, 0 }, true);
                acquiredDataFlag.SetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, 1 }, true);
                acquiredDataFlag.SetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, 2 }, true);
                acquiredDataFlag.SetAcquiredFlag(timeIndex, tileIndex, ModalityType{ ModalityType::Name::BF }, true);
            }
        }

        for (auto tileIndex = 0; tileIndex < targetTileIndex; ++tileIndex) {
            acquiredDataFlag.SetAcquiredFlag(targetTimeIndex, tileIndex, ModalityType{ ModalityType::Name::HT }, true);
            acquiredDataFlag.SetAcquiredFlag(targetTimeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, 0 }, true);
            acquiredDataFlag.SetAcquiredFlag(targetTimeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, 1 }, true);
            acquiredDataFlag.SetAcquiredFlag(targetTimeIndex, tileIndex, ModalityType{ ModalityType::Name::FL, 2 }, true);
            acquiredDataFlag.SetAcquiredFlag(targetTimeIndex, tileIndex, ModalityType{ ModalityType::Name::BF }, true);
        }

        acquiredDataFlag.SetAcquiredFlag(targetTimeIndex, targetTileIndex, ModalityType{ ModalityType::Name::HT }, true);
        acquiredDataFlag.SetAcquiredFlag(targetTimeIndex, targetTileIndex, ModalityType{ ModalityType::Name::FL, 0 }, true);
        acquiredDataFlag.SetAcquiredFlag(targetTimeIndex, targetTileIndex, ModalityType{ ModalityType::Name::FL, 1 }, true);
        acquiredDataFlag.SetAcquiredFlag(targetTimeIndex, targetTileIndex, ModalityType{ ModalityType::Name::FL, 2 }, true);
        //acquiredDataFlag.SetAcquiredFlag(targetTimeIndex, targetTileIndex, ModalityType{ ModalityType::Name::BF }, true);

        
        TaskInfoSet taskInfoSet;
        taskInfoSet.Initialize(acquisitionCount);

        auto taskRepo = TaskRepo::GetInstance();
        taskRepo->Clear();
        taskRepo->AddNotDone(rootFolderPath, acquiredDataFlag, taskInfoSet, acquisitionCount);
        
        AcquisitionScanTargetFinder scanner;
        scanner.SetRootFolderPath(rootFolderPath);
        CHECK(scanner.Find());
        CHECK(scanner.IsNextScanBG() == !bgAcquiredFlag);
        CHECK(scanner.GetNextScanTileIndex() == targetTileIndex);
        CHECK(scanner.GetNextScanTimeIndex() == targetTimeIndex);
        CHECK(scanner.GetNextScanModalityType().GetName() == ModalityType::Name::BF);
        //CHECK(scanner.GetNextScanModalityType().GetChannelIndex() == 0);

        const auto endTime = std::chrono::system_clock::now();
        const auto duration = endTime - startTime;

        std::cout << static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(duration).count()) / 1000 << std::endl;
    }

    SECTION("practical test") {
        
    }
}