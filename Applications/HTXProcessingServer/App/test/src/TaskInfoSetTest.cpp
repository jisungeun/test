#include <catch2/catch.hpp>

#include "ModalityType.h"
#include "ProcessType.h"
#include "TaskInfo.h"
#include "TaskInfoSet.h"

namespace TaskInfoSetTest {
    const auto emptyTaskInfo = TaskInfo{};

    TEST_CASE("TaskInfoSet : unit test") {
        SECTION("TaskInfoSet()") {
            TaskInfoSet taskInfoSet;
            CHECK(&taskInfoSet != nullptr);
        }
        SECTION("TaskInfoSet(other)") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            TaskInfoSet srcTaskInfoSet;
            srcTaskInfoSet.Initialize(acquisitionCount);

            TaskInfoSet destTaskInfoSet(srcTaskInfoSet);
            CHECK(destTaskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion).GetProcessedProgress() == 0);
        }
        SECTION("operator=()") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            TaskInfoSet srcTaskInfoSet;
            srcTaskInfoSet.Initialize(acquisitionCount);

            TaskInfoSet destTaskInfoSet;
            destTaskInfoSet = srcTaskInfoSet;
            CHECK(destTaskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion).GetProcessedProgress() == 0);
        }
        SECTION("Initialize()") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            TaskInfoSet taskInfoSet;
            taskInfoSet.Initialize(acquisitionCount);
            CHECK(&taskInfoSet != nullptr);
        }
        SECTION("SetTaskInfo()") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            TaskInfoSet taskInfoSet;
            taskInfoSet.Initialize(acquisitionCount);
            taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, emptyTaskInfo);
            CHECK(&taskInfoSet != nullptr);
        }
        SECTION("GetTaskInfo()") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            TaskInfo taskInfo;
            taskInfo.SetProcessedProgress(1);

            TaskInfoSet taskInfoSet;
            taskInfoSet.Initialize(acquisitionCount);
            taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, taskInfo);

            const auto resultTaskInfo = taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing);
            CHECK(resultTaskInfo.GetProcessedProgress() == 1);
        }
    }
    TEST_CASE("ProcessedFlag : practical test") {
        AcquisitionSequenceInfo acquisitionSequenceInfo;
        acquisitionSequenceInfo.Initialize(2);
        acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 30);
        acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 1, 30);
        acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 0, 1);
        acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 1, 1);
        acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH1, 0, 10);
        acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH1, 1, 10);
        acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::BF, 0, 1);
        acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::BF, 1, 1);

        AcquisitionCount acquisitionCount;
        acquisitionCount.SetTileNumber(1, 2);
        acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);
        
        TaskInfo taskInfo;
        taskInfo.SetProcessedProgress(1);
        taskInfo.SetProcessedTime(2, TimeUnit::Second);

        TaskInfoSet taskInfoSet;
        taskInfoSet.Initialize(acquisitionCount);

        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::TileProcessing, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing, taskInfo);

        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::Stitching, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::Stitching, taskInfo);

        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::StitchingWriting, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::StitchingWriting, taskInfo);

        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::LDMConversion, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::LDMConversion, taskInfo);

        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::None }, ProcessType::TCFWriting, taskInfo);
        taskInfoSet.SetTaskInfo(0, 0, ModalityType{ ModalityType::Name::None }, ProcessType::DataRemoving, taskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing) == emptyTaskInfo);
        CHECK(taskInfoSet.GetTaskInfo(0, 1, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing) == emptyTaskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 1, ModalityType{ ModalityType::Name::HT }, ProcessType::TileProcessing) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing) == emptyTaskInfo);
        CHECK(taskInfoSet.GetTaskInfo(0, 1, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing) == emptyTaskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 1, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::TileProcessing) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::TileProcessing) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::TileProcessing) == emptyTaskInfo);
        CHECK(taskInfoSet.GetTaskInfo(0, 1, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::TileProcessing) == emptyTaskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 1, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::TileProcessing) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing) == emptyTaskInfo);
        CHECK(taskInfoSet.GetTaskInfo(0, 1, ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing) == emptyTaskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 1, ModalityType{ ModalityType::Name::BF }, ProcessType::TileProcessing) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::Stitching) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::Stitching) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::Stitching) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::Stitching) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF}, ProcessType::Stitching) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::BF}, ProcessType::Stitching) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::StitchingWriting) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::StitchingWriting) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::StitchingWriting) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::StitchingWriting) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::StitchingWriting) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::StitchingWriting) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::HT }, ProcessType::LDMConversion) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 0 }, ProcessType::LDMConversion) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::LDMConversion) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::FL, 1 }, ProcessType::LDMConversion) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::LDMConversion) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(1, 0, ModalityType{ ModalityType::Name::BF }, ProcessType::LDMConversion) == emptyTaskInfo);

        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::None }, ProcessType::TCFWriting) == taskInfo);
        CHECK(taskInfoSet.GetTaskInfo(0, 0, ModalityType{ ModalityType::Name::None }, ProcessType::DataRemoving) == taskInfo);
    }
}