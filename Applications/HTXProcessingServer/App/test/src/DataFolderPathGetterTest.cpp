#include <QDir>
#include <catch2/catch.hpp>

#include "DataFolderPathGetter.h"

namespace DataFolderPathGetterTest {
    TEST_CASE("DataFolderPathGetter : unit test") {
        SECTION("DataFolderPathGetter()") {
            DataFolderPathGetter dataFolderPathGetter;
            CHECK(&dataFolderPathGetter != nullptr);
        }
        SECTION("SetRootFolderPath()") {
            DataFolderPathGetter dataFolderPathGetter;
            dataFolderPathGetter.SetRootFolderPath({});
            CHECK(&dataFolderPathGetter != nullptr);
        }
        SECTION("SetModalityType()") {
            DataFolderPathGetter dataFolderPathGetter;
            dataFolderPathGetter.SetModalityType(ModalityType{ ModalityType::Name::HT});
            CHECK(&dataFolderPathGetter != nullptr);
        }
        SECTION("SetTileIndex()") {
            DataFolderPathGetter dataFolderPathGetter;
            dataFolderPathGetter.SetTileIndex(0);
            CHECK(&dataFolderPathGetter != nullptr);
        }
        SECTION("SetTimeIndex()") {
            DataFolderPathGetter dataFolderPathGetter;
            dataFolderPathGetter.SetTimeIndex(0);
            CHECK(&dataFolderPathGetter != nullptr);
        }
        SECTION("ExistTileFolder()") {
            const QString rootFolderPath = "root";

            constexpr int32_t tileIndex = 1;
            constexpr int32_t timeIndex = 2;

            DataFolderPathGetter dataFolderPathGetter;
            dataFolderPathGetter.SetRootFolderPath(rootFolderPath);
            dataFolderPathGetter.SetTileIndex(tileIndex);
            dataFolderPathGetter.SetTimeIndex(timeIndex);

            SECTION("HT") {
                ModalityType modalityType{ ModalityType::Name::HT};

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/HT3D/0002");
                    CHECK(dataFolderPathGetter.ExistTileFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/HT3D/2");
                    CHECK(dataFolderPathGetter.ExistTileFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.ExistTileFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH0") {
                ModalityType modalityType{ ModalityType::Name::FL, 0 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH0/0002");
                    CHECK(dataFolderPathGetter.ExistTileFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH0/2");
                    CHECK(dataFolderPathGetter.ExistTileFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.ExistTileFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH1") {
                ModalityType modalityType{ ModalityType::Name::FL, 1 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH1/0002");
                    CHECK(dataFolderPathGetter.ExistTileFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH1/2");
                    CHECK(dataFolderPathGetter.ExistTileFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.ExistTileFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH2") {
                ModalityType modalityType{ ModalityType::Name::FL, 2 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH2/0002");
                    CHECK(dataFolderPathGetter.ExistTileFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH2/2");
                    CHECK(dataFolderPathGetter.ExistTileFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.ExistTileFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
            SECTION("BF") {
                ModalityType modalityType{ ModalityType::Name::BF};

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/BF/0002");
                    CHECK(dataFolderPathGetter.ExistTileFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/BF/2");
                    CHECK(dataFolderPathGetter.ExistTileFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.ExistTileFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
        }
        SECTION("ExistModalityFolder()"){
            const QString rootFolderPath = "root";

            constexpr int32_t tileIndex = 1;
            constexpr int32_t timeIndex = 2;

            DataFolderPathGetter dataFolderPathGetter;
            dataFolderPathGetter.SetRootFolderPath(rootFolderPath);
            dataFolderPathGetter.SetTileIndex(tileIndex);
            dataFolderPathGetter.SetTimeIndex(timeIndex);

            SECTION("HT") {
                ModalityType modalityType{ ModalityType::Name::HT};

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/HT3D/0002");
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/HT3D/2");
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH0") {
                ModalityType modalityType{ ModalityType::Name::FL, 0 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH0/0002");
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH0/2");
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH1") {
                ModalityType modalityType{ ModalityType::Name::FL, 1 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH1/0002");
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH1/2");
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH2") {
                ModalityType modalityType{ ModalityType::Name::FL, 2 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH2/0002");
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH2/2");
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
            SECTION("BF") {
                ModalityType modalityType{ ModalityType::Name::BF };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/BF/0002");
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/BF/2");
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.ExistModalityFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
        }
        SECTION("ExistTimeFrameFolder()"){
            const QString rootFolderPath = "root";

            constexpr int32_t tileIndex = 1;
            constexpr int32_t timeIndex = 2;

            DataFolderPathGetter dataFolderPathGetter;
            dataFolderPathGetter.SetRootFolderPath(rootFolderPath);
            dataFolderPathGetter.SetTileIndex(tileIndex);
            dataFolderPathGetter.SetTimeIndex(timeIndex);

            SECTION("HT") {
                ModalityType modalityType{ ModalityType::Name::HT };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/HT3D/0000");
                    QDir().mkpath("root/data/0001/HT3D/0001");
                    QDir().mkpath("root/data/0001/HT3D/0002");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/HT3D/0");
                    QDir().mkpath("root/data/1/HT3D/1");
                    QDir().mkpath("root/data/1/HT3D/2");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    QDir().mkpath("root/data/0001/HT3D/0000");
                    QDir().mkpath("root/data/0001/HT3D/0001");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH0") {
                ModalityType modalityType{ ModalityType::Name::FL, 0 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH0/0000");
                    QDir().mkpath("root/data/0001/FL3D/CH0/0001");
                    QDir().mkpath("root/data/0001/FL3D/CH0/0002");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH0/0");
                    QDir().mkpath("root/data/1/FL3D/CH0/1");
                    QDir().mkpath("root/data/1/FL3D/CH0/2");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH0/0000");
                    QDir().mkpath("root/data/0001/FL3D/CH0/0001");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH1") {
                ModalityType modalityType{ ModalityType::Name::FL, 1 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH1/0000");
                    QDir().mkpath("root/data/0001/FL3D/CH1/0001");
                    QDir().mkpath("root/data/0001/FL3D/CH1/0002");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH1/0");
                    QDir().mkpath("root/data/1/FL3D/CH1/1");
                    QDir().mkpath("root/data/1/FL3D/CH1/2");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH1/0000");
                    QDir().mkpath("root/data/0001/FL3D/CH1/0001");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH2") {
                ModalityType modalityType{ ModalityType::Name::FL, 2 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH2/0000");
                    QDir().mkpath("root/data/0001/FL3D/CH2/0001");
                    QDir().mkpath("root/data/0001/FL3D/CH2/0002");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH2/0");
                    QDir().mkpath("root/data/1/FL3D/CH2/1");
                    QDir().mkpath("root/data/1/FL3D/CH2/2");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH2/0000");
                    QDir().mkpath("root/data/0001/FL3D/CH2/0001");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
            SECTION("BF") {
                ModalityType modalityType{ ModalityType::Name::BF };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/BF/0000");
                    QDir().mkpath("root/data/0001/BF/0001");
                    QDir().mkpath("root/data/0001/BF/0002");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/BF/0");
                    QDir().mkpath("root/data/1/BF/1");
                    QDir().mkpath("root/data/1/BF/2");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == true);
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    QDir().mkpath("root/data/0001/BF/0000");
                    QDir().mkpath("root/data/0001/BF/0001");
                    CHECK(dataFolderPathGetter.ExistTimeFrameFolder() == false);
                    QDir("root").removeRecursively();
                }
            }
        }
        SECTION("GetTileFolderPath()"){
            const QString rootFolderPath = "root";

            constexpr int32_t tileIndex = 1;
            constexpr int32_t timeIndex = 2;

            DataFolderPathGetter dataFolderPathGetter;
            dataFolderPathGetter.SetRootFolderPath(rootFolderPath);
            dataFolderPathGetter.SetTileIndex(tileIndex);
            dataFolderPathGetter.SetTimeIndex(timeIndex);

            SECTION("HT") {
                ModalityType modalityType{ ModalityType::Name::HT };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/HT3D/0002");
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "root/data/0001");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/HT3D/2");
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "root/data/1");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH0") {
                ModalityType modalityType{ ModalityType::Name::FL, 0 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH0/0002");
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "root/data/0001");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH0/2");
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "root/data/1");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH1") {
                ModalityType modalityType{ ModalityType::Name::FL, 1 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH1/0002");
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "root/data/0001");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH1/2");
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "root/data/1");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH2") {
                ModalityType modalityType{ ModalityType::Name::FL, 2 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH2/0002");
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "root/data/0001");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH2/2");
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "root/data/1");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
            SECTION("BF") {
                ModalityType modalityType{ ModalityType::Name::BF };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/BF/0002");
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "root/data/0001");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/BF/2");
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "root/data/1");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.GetTileFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
        }
        SECTION("GetModalityFolderPath()"){
            const QString rootFolderPath = "root";

            constexpr int32_t tileIndex = 1;
            constexpr int32_t timeIndex = 2;

            DataFolderPathGetter dataFolderPathGetter;
            dataFolderPathGetter.SetRootFolderPath(rootFolderPath);
            dataFolderPathGetter.SetTileIndex(tileIndex);
            dataFolderPathGetter.SetTimeIndex(timeIndex);

            SECTION("HT") {
                ModalityType modalityType{ ModalityType::Name::HT };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/HT3D/0002");
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "root/data/0001/HT3D");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/HT3D/2");
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "root/data/1/HT3D");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH0") {
                ModalityType modalityType{ ModalityType::Name::FL, 0 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH0/0002");
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "root/data/0001/FL3D/CH0");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH0/2");
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "root/data/1/FL3D/CH0");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH1") {
                ModalityType modalityType{ ModalityType::Name::FL, 1 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH1/0002");
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "root/data/0001/FL3D/CH1");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH1/2");
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "root/data/1/FL3D/CH1");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH2") {
                ModalityType modalityType{ ModalityType::Name::FL, 2 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH2/0002");
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "root/data/0001/FL3D/CH2");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH2/2");
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "root/data/1/FL3D/CH2");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
            SECTION("BF") {
                ModalityType modalityType{ ModalityType::Name::BF };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/BF/0002");
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "root/data/0001/BF");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/BF/2");
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "root/data/1/BF");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    CHECK(dataFolderPathGetter.GetModalityFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
        }
        SECTION("GetTimeFrameFolderPath()") {
            const QString rootFolderPath = "root";

            constexpr int32_t tileIndex = 1;
            constexpr int32_t timeIndex = 2;

            DataFolderPathGetter dataFolderPathGetter;
            dataFolderPathGetter.SetRootFolderPath(rootFolderPath);
            dataFolderPathGetter.SetTileIndex(tileIndex);
            dataFolderPathGetter.SetTimeIndex(timeIndex);

            SECTION("HT") {
                ModalityType modalityType{ ModalityType::Name::HT };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/HT3D/0000");
                    QDir().mkpath("root/data/0001/HT3D/0001");
                    QDir().mkpath("root/data/0001/HT3D/0002");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "root/data/0001/HT3D/0002");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/HT3D/0");
                    QDir().mkpath("root/data/1/HT3D/1");
                    QDir().mkpath("root/data/1/HT3D/2");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "root/data/1/HT3D/2");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    QDir().mkpath("root/data/0001/HT3D/0000");
                    QDir().mkpath("root/data/0001/HT3D/0001");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH0") {
                ModalityType modalityType{ ModalityType::Name::FL, 0 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH0/0000");
                    QDir().mkpath("root/data/0001/FL3D/CH0/0001");
                    QDir().mkpath("root/data/0001/FL3D/CH0/0002");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "root/data/0001/FL3D/CH0/0002");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH0/0");
                    QDir().mkpath("root/data/1/FL3D/CH0/1");
                    QDir().mkpath("root/data/1/FL3D/CH0/2");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "root/data/1/FL3D/CH0/2");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH0/0000");
                    QDir().mkpath("root/data/0001/FL3D/CH0/0001");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH1") {
                ModalityType modalityType{ ModalityType::Name::FL, 1 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH1/0000");
                    QDir().mkpath("root/data/0001/FL3D/CH1/0001");
                    QDir().mkpath("root/data/0001/FL3D/CH1/0002");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "root/data/0001/FL3D/CH1/0002");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH1/0");
                    QDir().mkpath("root/data/1/FL3D/CH1/1");
                    QDir().mkpath("root/data/1/FL3D/CH1/2");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "root/data/1/FL3D/CH1/2");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH1/0000");
                    QDir().mkpath("root/data/0001/FL3D/CH1/0001");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
            SECTION("FL CH2") {
                ModalityType modalityType{ ModalityType::Name::FL, 2 };

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH2/0000");
                    QDir().mkpath("root/data/0001/FL3D/CH2/0001");
                    QDir().mkpath("root/data/0001/FL3D/CH2/0002");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "root/data/0001/FL3D/CH2/0002");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/FL3D/CH2/0");
                    QDir().mkpath("root/data/1/FL3D/CH2/1");
                    QDir().mkpath("root/data/1/FL3D/CH2/2");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "root/data/1/FL3D/CH2/2");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    QDir().mkpath("root/data/0001/FL3D/CH2/0000");
                    QDir().mkpath("root/data/0001/FL3D/CH2/0001");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
            SECTION("BF") {
                ModalityType modalityType{ ModalityType::Name::BF};

                dataFolderPathGetter.SetModalityType(modalityType);

                SECTION("exist situation") {
                    QDir().mkpath("root/data/0001/BF/0000");
                    QDir().mkpath("root/data/0001/BF/0001");
                    QDir().mkpath("root/data/0001/BF/0002");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "root/data/0001/BF/0002");
                    QDir("root").removeRecursively();
                }
                SECTION("exist situation (old)") {
                    QDir().mkpath("root/data/1/BF/0");
                    QDir().mkpath("root/data/1/BF/1");
                    QDir().mkpath("root/data/1/BF/2");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "root/data/1/BF/2");
                    QDir("root").removeRecursively();
                }
                SECTION("not exist situation") {
                    QDir().mkpath("root/data/0001/BF/0000");
                    QDir().mkpath("root/data/0001/BF/0001");
                    CHECK(dataFolderPathGetter.GetTimeFrameFolderPath() == "");
                    QDir("root").removeRecursively();
                }
            }
        }
    }
    TEST_CASE("DataFolderPathGetter : practical test") {
        //TODO Implement DataFolderPathGetter : practical test
    }
}