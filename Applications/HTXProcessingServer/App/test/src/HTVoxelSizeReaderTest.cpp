#include <catch2/catch.hpp>

#include "HTVoxelSizeReader.h"
#include "H5Cpp.h"

namespace HTVoxelSizeReaderTest {
    auto WriteTempFile(const QString& tempFilePath)->void {
        const H5::H5File file{ tempFilePath.toStdString(), H5F_ACC_TRUNC };

        constexpr hsize_t dims[3] = { 1,2,3 };
		const auto dataSpace = H5::DataSpace{ 3,dims };
		const auto dataSet = file.createDataSet("Data", H5::PredType::NATIVE_FLOAT, dataSpace);

		const auto pixelWorldSizeXAttribute = dataSet.createAttribute("pixelWorldSizeX", H5::PredType::NATIVE_FLOAT, H5S_SCALAR);
        constexpr float pixelWorldSizeX = 1;

        pixelWorldSizeXAttribute.write(H5::PredType::NATIVE_FLOAT, &pixelWorldSizeX);
    }

    TEST_CASE("HTVoxelSizeReader : unit test") {
        SECTION("HTVoxelSizeReader") {
            HTVoxelSizeReader reader;
            CHECK(&reader != nullptr);
        }
        SECTION("SetProcessedFilePath()") {
            HTVoxelSizeReader reader;
            reader.SetProcessedFilePath("");
            CHECK(&reader != nullptr);
        }
        SECTION("Read()") {
            const QString tempFilePath = "HTVoxelSizeReaderTest.h5";
            WriteTempFile(tempFilePath);

            HTVoxelSizeReader reader;
            reader.SetProcessedFilePath(tempFilePath);

            CHECK(reader.Read() == true);
        }
        SECTION("GetVoxelSizeXY()") {
            const QString tempFilePath = "HTVoxelSizeReaderTest.h5";
            WriteTempFile(tempFilePath);

            HTVoxelSizeReader reader;
            reader.SetProcessedFilePath(tempFilePath);

            reader.Read();

            const auto resultVoxelSize = reader.GetVoxelSizeXY();
            CHECK(resultVoxelSize == 1);
        }
    }
    TEST_CASE("HTVoxelSizeReader : practical test") {
        //TODO Implement test
    }
    
}