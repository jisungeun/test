#include <catch2/catch.hpp>

#include "AcquisitionDataScanner.h"
#include "SampleObjectGenerator.h"
#include "TaskRepo.h"
#include "TaskInfoSetScanner.h"
#include "AcquisitionConfigReader.h"

namespace SampleObjectGeneratorTest {
    TEST_CASE("SampleObjectGenerator : unit test") {
        SECTION("Generate()") {
            //Implement
        }
    }
    TEST_CASE("SampleObjectGenerator : practical test") {
        SECTION("only already done data") {
            auto instance = TaskRepo::GetInstance();
            instance->Clear();

            const QString rootFolderPath = "E:/00_Data/20220428 Processing Fail/20220420.174235.TestProject.PPeT_6well_Hep3B.009.K562.Well_2_2.P000";

            instance->AddAlreadyDone(rootFolderPath);

            const auto sampleObjectList = SampleObjectGenerator::Generate();
            CHECK(sampleObjectList.size() == 1);

            const auto resultSampleObject = sampleObjectList.first();

            CHECK(resultSampleObject.tcfExist == true);
            CHECK(resultSampleObject.rootPath == rootFolderPath);
            CHECK(resultSampleObject.remainingTime == 0);
            CHECK(resultSampleObject.remainingTimeUnit == +TimeUnit::Second);
            CHECK(resultSampleObject.dataAcquisitionStatus == +DataAcquisitionStatus::FullyAcquired);
            CHECK(resultSampleObject.processingStatus == +ProcessingStatus::AlreadyProcessed);
        }

        SECTION("only Failed data") {
            auto instance = TaskRepo::GetInstance();
            instance->Clear();

            const QString rootFolderPath = "E:/00_Data/20220428 Processing Fail/20220420.174235.TestProject.PPeT_6well_Hep3B.009.K562.Well_2_2.P000";
            const QString backgroundFolderPath = "C:/Users/HanghunJo/AppData/Local/TomoCube, Inc/TomoStudioX/backgrounds";

            AcquisitionConfigReader acquisitionConfigReader;
            acquisitionConfigReader.SetFilePath(rootFolderPath + "/config.dat");
            acquisitionConfigReader.Read();
            const auto acquisitionConfig = acquisitionConfigReader.GetAcquisitionConfig();

            //DataCountInfo dataCountInfo;
            //dataCountInfo.SetHTTimelapseCount(acquisitionConfig.GetAcquisitionCount().ht3D);
            ////dataCountInfo.SetFLAcquisitionSequence(acquisitionConfig.GetFLAcquisitionSequence());
            //dataCountInfo.SetBFTimelapseCount(acquisitionConfig.GetAcquisitionCount().bf);
            //dataCountInfo.SetTileNumber(acquisitionConfig.GetTileInfo().tileNumberX, acquisitionConfig.GetTileInfo().tileNumberY);

            //TODO Implement test
            //AcquisitionDataScanner acquisitionDataScanner;
            //acquisitionDataScanner.SetRootFolderPath(rootFolderPath);
            //acquisitionDataScanner.SetBackgroundFolderPath(backgroundFolderPath);
            //acquisitionDataScanner.SetAcquisitionConfig(acquisitionConfig);
            //acquisitionDataScanner.SetDataCountInfo(dataCountInfo);
            //acquisitionDataScanner.Scan();

            //const auto acquiredDataFlag = acquisitionDataScanner.GetAcquiredDataFlag();

            TaskInfoSetScanner taskInfoSetScanner;
            taskInfoSetScanner.SetRootFolderPath(rootFolderPath);
            taskInfoSetScanner.Scan();

            const auto taskInfoSet = taskInfoSetScanner.GetTaskInfoSet();

            //instance->AddNotDone(rootFolderPath, acquiredDataFlag, taskInfoSet, dataCountInfo);
            //instance->UpdateToFailed(rootFolderPath);

            const auto sampleObjectList = SampleObjectGenerator::Generate();
            CHECK(sampleObjectList.size() == 1);

            const auto resultSampleObject = sampleObjectList.first();

            CHECK(resultSampleObject.tcfExist == false);
            CHECK(resultSampleObject.rootPath == rootFolderPath);
            CHECK(resultSampleObject.remainingTime == 0);
            CHECK(resultSampleObject.remainingTimeUnit == +TimeUnit::Second);
            CHECK(resultSampleObject.dataAcquisitionStatus == +DataAcquisitionStatus::FullyAcquired);
            CHECK(resultSampleObject.processingStatus == +ProcessingStatus::ProcessingFail);
        }

        SECTION("only Done data") {
            auto instance = TaskRepo::GetInstance();
            instance->Clear();

            const QString rootFolderPath = "E:/00_Data/20220428 Processing Fail/20220420.174235.TestProject.PPeT_6well_Hep3B.009.K562.Well_2_2.P000";
            const QString backgroundFolderPath = "C:/Users/HanghunJo/AppData/Local/TomoCube, Inc/TomoStudioX/backgrounds";

            AcquisitionConfigReader acquisitionConfigReader;
            acquisitionConfigReader.SetFilePath(rootFolderPath + "/config.dat");
            acquisitionConfigReader.Read();
            const auto acquisitionConfig = acquisitionConfigReader.GetAcquisitionConfig();

            //DataCountInfo dataCountInfo;
            //dataCountInfo.SetHTTimelapseCount(acquisitionConfig.GetAcquisitionCount().ht3D);
            ////dataCountInfo.SetFLAcquisitionSequence(acquisitionConfig.GetFLAcquisitionSequence());
            //dataCountInfo.SetBFTimelapseCount(acquisitionConfig.GetAcquisitionCount().bf);
            //dataCountInfo.SetTileNumber(acquisitionConfig.GetTileInfo().tileNumberX, acquisitionConfig.GetTileInfo().tileNumberY);

            //TODO Implement test
            //AcquisitionDataScanner acquisitionDataScanner;
            //acquisitionDataScanner.SetRootFolderPath(rootFolderPath);
            //acquisitionDataScanner.SetBackgroundFolderPath(backgroundFolderPath);
            //acquisitionDataScanner.SetAcquisitionConfig(acquisitionConfig);
            //acquisitionDataScanner.SetDataCountInfo(dataCountInfo);
            //acquisitionDataScanner.Scan();

            //const auto acquiredDataFlag = acquisitionDataScanner.GetAcquiredDataFlag();

            TaskInfoSetScanner taskInfoSetScanner;
            taskInfoSetScanner.SetRootFolderPath(rootFolderPath);
            taskInfoSetScanner.Scan();

            const auto taskInfoSet = taskInfoSetScanner.GetTaskInfoSet();

            //instance->AddNotDone(rootFolderPath, acquiredDataFlag, taskInfoSet, dataCountInfo);
            //instance->UpdateToDone(rootFolderPath);

            const auto sampleObjectList = SampleObjectGenerator::Generate();
            CHECK(sampleObjectList.size() == 1);

            const auto resultSampleObject = sampleObjectList.first();

            CHECK(resultSampleObject.tcfExist == true);
            CHECK(resultSampleObject.rootPath == rootFolderPath);
            CHECK(resultSampleObject.remainingTime == 0);
            CHECK(resultSampleObject.remainingTimeUnit == +TimeUnit::Second);
            CHECK(resultSampleObject.dataAcquisitionStatus == +DataAcquisitionStatus::FullyAcquired);
            CHECK(resultSampleObject.processingStatus == +ProcessingStatus::ProperlyProcessed);
        }

        SECTION("Processing data : no temp data Exists") {
            auto instance = TaskRepo::GetInstance();
            instance->Clear();

            const QString rootFolderPath = "E:/00_Data/20220428 Processing Fail/20220420.174235.TestProject.PPeT_6well_Hep3B.009.K562.Well_2_2.P000";
            const QString backgroundFolderPath = "C:/Users/HanghunJo/AppData/Local/TomoCube, Inc/TomoStudioX/backgrounds";

            AcquisitionConfigReader acquisitionConfigReader;
            acquisitionConfigReader.SetFilePath(rootFolderPath + "/config.dat");
            acquisitionConfigReader.Read();
            const auto acquisitionConfig = acquisitionConfigReader.GetAcquisitionConfig();

            //DataCountInfo dataCountInfo;
            //dataCountInfo.SetHTTimelapseCount(acquisitionConfig.GetAcquisitionCount().ht3D);
            ////dataCountInfo.SetFLAcquisitionSequence(acquisitionConfig.GetFLAcquisitionSequence());
            //dataCountInfo.SetBFTimelapseCount(acquisitionConfig.GetAcquisitionCount().bf);
            //dataCountInfo.SetTileNumber(acquisitionConfig.GetTileInfo().tileNumberX, acquisitionConfig.GetTileInfo().tileNumberY);

            //TODO Implement test
            //AcquisitionDataScanner acquisitionDataScanner;
            //acquisitionDataScanner.SetRootFolderPath(rootFolderPath);
            //acquisitionDataScanner.SetBackgroundFolderPath(backgroundFolderPath);
            //acquisitionDataScanner.SetAcquisitionConfig(acquisitionConfig);
            //acquisitionDataScanner.SetDataCountInfo(dataCountInfo);
            //acquisitionDataScanner.Scan();

            //const auto acquiredDataFlag = acquisitionDataScanner.GetAcquiredDataFlag();

            //TaskInfoSetScanner taskInfoSetScanner;
            //taskInfoSetScanner.SetRootFolderPath(rootFolderPath);
            //taskInfoSetScanner.SetDataCountInfo(dataCountInfo);
            //taskInfoSetScanner.Scan();

            //const auto taskInfoSet = taskInfoSetScanner.GetTaskInfoSet();

            //instance->AddNotDone(rootFolderPath, acquiredDataFlag, taskInfoSet, dataCountInfo);

            const auto sampleObjectList = SampleObjectGenerator::Generate();
            CHECK(sampleObjectList.size() == 1);

            const auto resultSampleObject = sampleObjectList.first();

            CHECK(resultSampleObject.tcfExist == false);
            CHECK(resultSampleObject.rootPath == rootFolderPath);
            CHECK(resultSampleObject.remainingTime == 180);
            CHECK(resultSampleObject.remainingTimeUnit == +TimeUnit::Second);
            CHECK(resultSampleObject.dataAcquisitionStatus == +DataAcquisitionStatus::FullyAcquired);
            CHECK(resultSampleObject.processingStatus == +ProcessingStatus::WaitForProcess);
        }

        SECTION("Processing data : temp data Exists") {
            auto instance = TaskRepo::GetInstance();
            instance->Clear();

            const QString rootFolderPath = "E:/00_Data/20220425 HTX Timelapse ProcessingFail/HTX-VAL-B11_6well_StitchingFail/20220421.173211.Beta-timelapse.HTX-VAL-B11_6well.091.Jurkat.A2.P008";
            const QString backgroundFolderPath = "C:/Users/HanghunJo/AppData/Local/TomoCube, Inc/TomoStudioX/backgrounds";

            AcquisitionConfigReader acquisitionConfigReader;
            acquisitionConfigReader.SetFilePath(rootFolderPath + "/config.dat");
            acquisitionConfigReader.Read();
            const auto acquisitionConfig = acquisitionConfigReader.GetAcquisitionConfig();

            //DataCountInfo dataCountInfo;
            //dataCountInfo.SetHTTimelapseCount(acquisitionConfig.GetAcquisitionCount().ht3D);
            ////dataCountInfo.SetFLAcquisitionSequence(acquisitionConfig.GetFLAcquisitionSequence());
            //dataCountInfo.SetBFTimelapseCount(acquisitionConfig.GetAcquisitionCount().bf);
            //dataCountInfo.SetTileNumber(acquisitionConfig.GetTileInfo().tileNumberX, acquisitionConfig.GetTileInfo().tileNumberY);

            //TODO Implement test
            //AcquisitionDataScanner acquisitionDataScanner;
            //acquisitionDataScanner.SetRootFolderPath(rootFolderPath);
            //acquisitionDataScanner.SetBackgroundFolderPath(backgroundFolderPath);
            //acquisitionDataScanner.SetAcquisitionConfig(acquisitionConfig);
            //acquisitionDataScanner.SetDataCountInfo(dataCountInfo);
            //acquisitionDataScanner.Scan();

            //const auto acquiredDataFlag = acquisitionDataScanner.GetAcquiredDataFlag();

            //TaskInfoSetScanner taskInfoSetScanner;
            //taskInfoSetScanner.SetRootFolderPath(rootFolderPath);
            //taskInfoSetScanner.SetDataCountInfo(dataCountInfo);
            //taskInfoSetScanner.Scan();

            //const auto taskInfoSet = taskInfoSetScanner.GetTaskInfoSet();

            //instance->AddNotDone(rootFolderPath, acquiredDataFlag, taskInfoSet, dataCountInfo);

            const auto sampleObjectList = SampleObjectGenerator::Generate();
            CHECK(sampleObjectList.size() == 1);

            const auto resultSampleObject = sampleObjectList.first();

            CHECK(resultSampleObject.tcfExist == false);
            CHECK(resultSampleObject.rootPath == rootFolderPath);
            //CHECK(resultSampleObject.remainingTime == 180);
            CHECK(resultSampleObject.remainingTimeUnit == +TimeUnit::Second);
            CHECK(resultSampleObject.dataAcquisitionStatus == +DataAcquisitionStatus::FullyAcquired);
            CHECK(resultSampleObject.processingStatus == +ProcessingStatus::WaitForProcess);
        }

        SECTION("Processing data : temp data Exists 2") {
            auto instance = TaskRepo::GetInstance();
            instance->Clear();

            const QString rootFolderPath = "E:/00_Data/20220425 HTX Timelapse ProcessingFail/HTX-VAL-B11_6well_StitchingFail/20220421.173211.Beta-timelapse.HTX-VAL-B11_6well.091.Jurkat.A2.P008";
            const QString backgroundFolderPath = "C:/Users/HanghunJo/AppData/Local/TomoCube, Inc/TomoStudioX/backgrounds";

            AcquisitionConfigReader acquisitionConfigReader;
            acquisitionConfigReader.SetFilePath(rootFolderPath + "/config.dat");
            acquisitionConfigReader.Read();
            const auto acquisitionConfig = acquisitionConfigReader.GetAcquisitionConfig();

            //DataCountInfo dataCountInfo;
            //dataCountInfo.SetHTTimelapseCount(acquisitionConfig.GetAcquisitionCount().ht3D);
            ////dataCountInfo.SetFLAcquisitionSequence(acquisitionConfig.GetFLAcquisitionSequence());
            //dataCountInfo.SetBFTimelapseCount(acquisitionConfig.GetAcquisitionCount().bf);
            //dataCountInfo.SetTileNumber(acquisitionConfig.GetTileInfo().tileNumberX, acquisitionConfig.GetTileInfo().tileNumberY);

            //TODO Implement test
            //AcquisitionDataScanner acquisitionDataScanner;
            //acquisitionDataScanner.SetRootFolderPath(rootFolderPath);
            //acquisitionDataScanner.SetBackgroundFolderPath(backgroundFolderPath);
            //acquisitionDataScanner.SetAcquisitionConfig(acquisitionConfig);
            //acquisitionDataScanner.SetDataCountInfo(dataCountInfo);
            //acquisitionDataScanner.Scan();

            //const auto acquiredDataFlag = acquisitionDataScanner.GetAcquiredDataFlag();

            //TaskInfoSetScanner taskInfoSetScanner;
            //taskInfoSetScanner.SetRootFolderPath(rootFolderPath);
            //taskInfoSetScanner.SetDataCountInfo(dataCountInfo);
            //taskInfoSetScanner.Scan();

            //const auto taskInfoSet = taskInfoSetScanner.GetTaskInfoSet();

            //instance->AddNotDone(rootFolderPath, acquiredDataFlag, taskInfoSet, dataCountInfo);

            const auto sampleObjectList = SampleObjectGenerator::Generate();
            CHECK(sampleObjectList.size() == 1);

            const auto resultSampleObject = sampleObjectList.first();

            CHECK(resultSampleObject.tcfExist == false);
            CHECK(resultSampleObject.rootPath == rootFolderPath);
            //CHECK(resultSampleObject.remainingTime == 180);
            CHECK(resultSampleObject.remainingTimeUnit == +TimeUnit::Second);
            CHECK(resultSampleObject.dataAcquisitionStatus == +DataAcquisitionStatus::FullyAcquired);
            CHECK(resultSampleObject.processingStatus == +ProcessingStatus::WaitForProcess);
        }

    }
}
