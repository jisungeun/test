#include <catch2/catch.hpp>

#include "PiercedRectangle.h"

namespace PiercedRectangleTest {
    TEST_CASE("PiercedRectangle : unit test") {
        SECTION("PiercedRectangle()") {
            PiercedRectangle piercedRectangle;
            CHECK(&piercedRectangle != nullptr);
        }
        SECTION("PiercedRectangle(other)") {
            PiercedRectangle srcPiercedRectangle;
            srcPiercedRectangle.SetSize(1, 2);

            PiercedRectangle destPiercedRectangle(srcPiercedRectangle);
            CHECK(destPiercedRectangle.GetSizeX() == 1);
            CHECK(destPiercedRectangle.GetSizeY() == 2);
        }
        SECTION("operator=()") {
            PiercedRectangle srcPiercedRectangle;
            srcPiercedRectangle.SetSize(1, 2);

            PiercedRectangle destPiercedRectangle;
            destPiercedRectangle = srcPiercedRectangle;
            CHECK(destPiercedRectangle.GetSizeX() == 1);
            CHECK(destPiercedRectangle.GetSizeY() == 2);
        }
        SECTION("SetSize()") {
            PiercedRectangle piercedRectangle;
            piercedRectangle.SetSize(1, 2);
            CHECK(&piercedRectangle != nullptr);
        }
        SECTION("GetSizeX()"){
            PiercedRectangle piercedRectangle;
            piercedRectangle.SetSize(1, 2);
            CHECK(piercedRectangle.GetSizeX() == 1);
        }
        SECTION("GetSizeY()"){
            PiercedRectangle piercedRectangle;
            piercedRectangle.SetSize(1, 2);
            CHECK(piercedRectangle.GetSizeY() == 2);
        }
        SECTION("SetPiercingIndex()"){
            PiercedRectangle piercedRectangle;
            piercedRectangle.SetPiercingIndex(1, 2);
            CHECK(&piercedRectangle != nullptr);
        }
        SECTION("GetPiercingIndexX()") {
            PiercedRectangle piercedRectangle;
            piercedRectangle.SetPiercingIndex(1, 2);
            CHECK(piercedRectangle.GetPiercingIndexX() == 1);
        }
        SECTION("GetPiercingIndexY()") {
            PiercedRectangle piercedRectangle;
            piercedRectangle.SetPiercingIndex(1, 2);
            CHECK(piercedRectangle.GetPiercingIndexY() == 2);
        }
    }

    TEST_CASE("PiercedRectangle : practical test") {
        PiercedRectangle piercedRectangle;
        piercedRectangle.SetSize(10, 12);
        piercedRectangle.SetPiercingIndex(4, 5);

        CHECK(piercedRectangle.GetSizeX() == 10);
        CHECK(piercedRectangle.GetSizeY() == 12);

        CHECK(piercedRectangle.GetPiercingIndexX() == 4);
        CHECK(piercedRectangle.GetPiercingIndexY() == 5);
    }
}