#include <catch2/catch.hpp>

#include "AcquisitionDataScanner.h"
#include "AcquisitionConfigReader.h"

namespace AcquisitionDataScannerTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/HTXProcessingServerTest/SampleAcquisitionData";

    TEST_CASE("AcquisitionDataScanner : unit test") {
        SECTION("AcquisitionDataScanner()") {
            AcquisitionDataScanner scanner;
            CHECK(&scanner != nullptr);
        }
        SECTION("SetAcquisitionDataInfo()") {
            //TODO Implement test
        }
        SECTION("Scan()") {
            //TODO Implement test
        }
        SECTION("GetAcquiredDataFlag()") {
            //TODO Implement test
        }
    }
    TEST_CASE("AcquisitionDataScanner : practical test") {
        //TODO Implement test
    }
}