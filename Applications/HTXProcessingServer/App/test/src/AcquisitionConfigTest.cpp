#include <catch2/catch.hpp>

#include "AcquisitionConfig.h"

namespace AcquisitionConfigTest {
    TEST_CASE("AcquisitionConfig : unit test") {
        SECTION("AcquisitionConfig()") {
            AcquisitionConfig config;
            CHECK(&config != nullptr);
        }
        SECTION("AcquisitionConfig(other)") {
            AcquisitionConfig srcConfig;
            srcConfig.SetJobInfo({ "test","test" });

            AcquisitionConfig destConfig{ srcConfig };
            CHECK(destConfig.GetJobInfo().title == "test");
            CHECK(destConfig.GetJobInfo().userID == "test");
        }
        SECTION("operator=()") {
            AcquisitionConfig srcConfig;
            srcConfig.SetJobInfo({ "test","test" });

            AcquisitionConfig destConfig;
            destConfig = srcConfig;
            CHECK(destConfig.GetJobInfo().title == "test");
            CHECK(destConfig.GetJobInfo().userID == "test");
        }
        SECTION("SetJobInfo()") {
            AcquisitionConfig config;
            config.SetJobInfo({});
            CHECK(&config != nullptr);
        }
        SECTION("GetJobInfo()") {
            AcquisitionConfig::JobInfo jobInfo;
            jobInfo.userID = "test";
            jobInfo.title = "test";

            AcquisitionConfig config;
            config.SetJobInfo(jobInfo);
            CHECK(config.GetJobInfo().userID == "test");
            CHECK(config.GetJobInfo().title == "test");
        }
        SECTION("IsJobInfoValid()") {
            AcquisitionConfig::JobInfo jobInfo;

            AcquisitionConfig config;
            CHECK(config.IsJobInfoValid() == false);
            config.SetJobInfo(jobInfo);
            CHECK(config.IsJobInfoValid() == true);
        }
        SECTION("SetAcquisitionCount()") {
            AcquisitionConfig config;
            config.SetAcquisitionCount({});
            CHECK(&config != nullptr);
        }
        SECTION("GetAcquisitionCount()") {
            AcquisitionConfig::AcquisitionCount acquisitionCount;
            acquisitionCount.ht3D = 1;
            acquisitionCount.ht2D = 2;
            acquisitionCount.fl3DCH0 = 3;
            acquisitionCount.fl2DCH0 = 4;
            acquisitionCount.fl3DCH1 = 5;
            acquisitionCount.fl2DCH1 = 6;
            acquisitionCount.fl3DCH2 = 7;
            acquisitionCount.fl2DCH2 = 8;
            acquisitionCount.ht3DZ = 9;
            acquisitionCount.fl3DZ = 10;
            acquisitionCount.bf = 11;
            acquisitionCount.bfChannel = 12;

            AcquisitionConfig config;
            config.SetAcquisitionCount(acquisitionCount);
            CHECK(config.GetAcquisitionCount().ht3D == 1);
            CHECK(config.GetAcquisitionCount().ht2D == 2);
            CHECK(config.GetAcquisitionCount().fl3DCH0 == 3);
            CHECK(config.GetAcquisitionCount().fl2DCH0 == 4);
            CHECK(config.GetAcquisitionCount().fl3DCH1 == 5);
            CHECK(config.GetAcquisitionCount().fl2DCH1 == 6);
            CHECK(config.GetAcquisitionCount().fl3DCH2 == 7);
            CHECK(config.GetAcquisitionCount().fl2DCH2 == 8);
            CHECK(config.GetAcquisitionCount().ht3DZ == 9);
            CHECK(config.GetAcquisitionCount().fl3DZ == 10);
            CHECK(config.GetAcquisitionCount().bf == 11);
            CHECK(config.GetAcquisitionCount().bfChannel == 12);
        }
        SECTION("IsAcquisitionCountValid()") {
            AcquisitionConfig::AcquisitionCount acquisitionCount;

            AcquisitionConfig config;
            CHECK(config.IsAcquisitionCountValid() == false);
            config.SetAcquisitionCount(acquisitionCount);
            CHECK(config.IsAcquisitionCountValid() == true);
        }
        SECTION("SetAcquisitionSetting()") {
            AcquisitionConfig config;
            config.SetAcquisitionSetting({});
            CHECK(&config != nullptr);
        }
        SECTION("GetAcquisitionSetting()") {
            AcquisitionConfig::AcquisitionSetting acquisitionSetting;
            acquisitionSetting.htZStepLengthMicrometer = 1;
            acquisitionSetting.flZStepLengthMicrometer = 2;
            acquisitionSetting.flCH0ExcitationWaveLengthMicrometer = 3;
            acquisitionSetting.flCH1ExcitationWaveLengthMicrometer = 4;
            acquisitionSetting.flCH2ExcitationWaveLengthMicrometer = 5;
            acquisitionSetting.flCH0EmissionWaveLengthMicrometer = 6;
            acquisitionSetting.flCH1EmissionWaveLengthMicrometer = 7;
            acquisitionSetting.flCH2EmissionWaveLengthMicrometer = 8;
            acquisitionSetting.flCH0Color = { 9,10,11 };
            acquisitionSetting.flCH1Color = { 12,13,14 };
            acquisitionSetting.flCH2Color = { 15,16,17 };
            acquisitionSetting.flCH0Intensity = 18;
            acquisitionSetting.flCH1Intensity = 19;
            acquisitionSetting.flCH2Intensity = 20;
            acquisitionSetting.flCH0ExposureTimeMillisecond = 21;
            acquisitionSetting.flCH1ExposureTimeMillisecond = 22;
            acquisitionSetting.flCH2ExposureTimeMillisecond = 23;

            AcquisitionConfig config;
            config.SetAcquisitionSetting(acquisitionSetting);
            CHECK(config.GetAcquisitionSetting().htZStepLengthMicrometer == 1);
            CHECK(config.GetAcquisitionSetting().flZStepLengthMicrometer == 2);
            CHECK(config.GetAcquisitionSetting().flCH0ExcitationWaveLengthMicrometer == 3);
            CHECK(config.GetAcquisitionSetting().flCH1ExcitationWaveLengthMicrometer == 4);
            CHECK(config.GetAcquisitionSetting().flCH2ExcitationWaveLengthMicrometer == 5);
            CHECK(config.GetAcquisitionSetting().flCH0EmissionWaveLengthMicrometer == 6);
            CHECK(config.GetAcquisitionSetting().flCH1EmissionWaveLengthMicrometer == 7);
            CHECK(config.GetAcquisitionSetting().flCH2EmissionWaveLengthMicrometer == 8);
            CHECK(config.GetAcquisitionSetting().flCH0Color.r == 9);
            CHECK(config.GetAcquisitionSetting().flCH0Color.g == 10);
            CHECK(config.GetAcquisitionSetting().flCH0Color.b == 11);
            CHECK(config.GetAcquisitionSetting().flCH1Color.r == 12);
            CHECK(config.GetAcquisitionSetting().flCH1Color.g == 13);
            CHECK(config.GetAcquisitionSetting().flCH1Color.b == 14);
            CHECK(config.GetAcquisitionSetting().flCH2Color.r == 15);
            CHECK(config.GetAcquisitionSetting().flCH2Color.g == 16);
            CHECK(config.GetAcquisitionSetting().flCH2Color.b == 17);
            CHECK(config.GetAcquisitionSetting().flCH0Intensity == 18);
            CHECK(config.GetAcquisitionSetting().flCH1Intensity == 19);
            CHECK(config.GetAcquisitionSetting().flCH2Intensity == 20);
            CHECK(config.GetAcquisitionSetting().flCH0ExposureTimeMillisecond == 21);
            CHECK(config.GetAcquisitionSetting().flCH1ExposureTimeMillisecond == 22);
            CHECK(config.GetAcquisitionSetting().flCH2ExposureTimeMillisecond == 23);
        }
        SECTION("IsAcquisitionSettingValid()") {
            AcquisitionConfig::AcquisitionSetting acquisitionSetting;

            AcquisitionConfig config;
            CHECK(config.IsAcquisitionSettingValid() == false);
            config.SetAcquisitionSetting(acquisitionSetting);
            CHECK(config.IsAcquisitionSettingValid() == true);
        }
        SECTION("SetAcquisitionPosition()") {
            AcquisitionConfig config;
            config.SetAcquisitionPosition({});
            CHECK(&config != nullptr);
        }
        SECTION("GetAcquisitionPosition()") {
            AcquisitionConfig::AcquisitionPosition acquisitionPosition;
            acquisitionPosition.positionXMillimeter = 1;
            acquisitionPosition.positionYMillimeter = 2;
            acquisitionPosition.positionZMillimeter = 3;
            acquisitionPosition.flAcquisitionZOffsetMicrometer = 4;
            
            AcquisitionConfig config;
            config.SetAcquisitionPosition(acquisitionPosition);
            CHECK(config.GetAcquisitionPosition().positionXMillimeter == 1);
            CHECK(config.GetAcquisitionPosition().positionYMillimeter == 2);
            CHECK(config.GetAcquisitionPosition().positionZMillimeter == 3);
            CHECK(config.GetAcquisitionPosition().flAcquisitionZOffsetMicrometer == 4);
        }
        SECTION("IsAcquisitionPositionValid()") {
            AcquisitionConfig::AcquisitionPosition acquisitionPosition;

            AcquisitionConfig config;
            CHECK(config.IsAcquisitionPositionValid() == false);
            config.SetAcquisitionPosition(acquisitionPosition);
            CHECK(config.IsAcquisitionPositionValid() == true);
        }
        SECTION("SetAcquisitionSize()") {
            AcquisitionConfig config;
            config.SetAcquisitionSize({});
            CHECK(&config != nullptr);
        }
        SECTION("GetAcquisitionSize()") {
            AcquisitionConfig::AcquisitionSize acquisitionSize;
            acquisitionSize.acquisitionSizeXMicrometer = 1;
            acquisitionSize.acquisitionSizeYMicrometer = 2;

            AcquisitionConfig config;
            config.SetAcquisitionSize(acquisitionSize);
            CHECK(config.GetAcquisitionSize().acquisitionSizeXMicrometer == 1);
            CHECK(config.GetAcquisitionSize().acquisitionSizeYMicrometer == 2);
        }
        SECTION("IsAcquisitionSizeValid()") {
            AcquisitionConfig::AcquisitionSize acquisitionSize;

            AcquisitionConfig config;
            CHECK(config.IsAcquisitionSizeValid() == false);
            config.SetAcquisitionSize(acquisitionSize);
            CHECK(config.IsAcquisitionSizeValid() == true);
        }
        SECTION("SetDeviceInfo()") {
            AcquisitionConfig config;
            config.SetDeviceInfo({});
            CHECK(&config != nullptr);
        }
        SECTION("GetDeviceInfo()") {
            AcquisitionConfig::DeviceInfo deviceInfo;
            deviceInfo.pixelSizeMicrometer = 1;
            deviceInfo.magnification = 2;
            deviceInfo.objectiveNA = 3;
            deviceInfo.condenserNA = 4;
            deviceInfo.mediumRI = 5;
            deviceInfo.deviceSerial = "6";
            deviceInfo.deviceHost = "7";
            deviceInfo.deviceSoftwareVersion = "8";

            AcquisitionConfig config;
            config.SetDeviceInfo(deviceInfo);
            CHECK(config.GetDeviceInfo().pixelSizeMicrometer == 1);
            CHECK(config.GetDeviceInfo().magnification == 2);
            CHECK(config.GetDeviceInfo().objectiveNA == 3);
            CHECK(config.GetDeviceInfo().condenserNA == 4);
            CHECK(config.GetDeviceInfo().mediumRI == 5);
            CHECK(config.GetDeviceInfo().deviceSerial == "6");
            CHECK(config.GetDeviceInfo().deviceHost == "7");
            CHECK(config.GetDeviceInfo().deviceSoftwareVersion == "8");
        }
        SECTION("IsDeviceInfoValid()") {
            AcquisitionConfig::DeviceInfo deviceInfo;

            AcquisitionConfig config;
            CHECK(config.IsDeviceInfoValid() == false);
            config.SetDeviceInfo(deviceInfo);
            CHECK(config.IsDeviceInfoValid() == true);
        }
        SECTION("SetImageInfo()") {
            AcquisitionConfig config;
            config.SetImageInfo({});
            CHECK(&config != nullptr);
        }
        SECTION("GetImageInfo()") {
            AcquisitionConfig::ImageInfo imageInfo;
            imageInfo.roiSizeXPixels = 1;
            imageInfo.roiSizeYPixels = 2;
            imageInfo.roiOffsetXPixels = 3;
            imageInfo.roiOffsetYPixels = 4;

            AcquisitionConfig config;
            config.SetImageInfo(imageInfo);
            CHECK(config.GetImageInfo().roiSizeXPixels == 1);
            CHECK(config.GetImageInfo().roiSizeYPixels == 2);
            CHECK(config.GetImageInfo().roiOffsetXPixels == 3);
            CHECK(config.GetImageInfo().roiOffsetYPixels == 4);
        }
        SECTION("IsImageInfoValid()") {
            AcquisitionConfig::ImageInfo imageInfo;

            AcquisitionConfig config;
            CHECK(config.IsImageInfoValid() == false);
            config.SetImageInfo(imageInfo);
            CHECK(config.IsImageInfoValid() == true);
        }
        SECTION("SetTileInfo()") {
            AcquisitionConfig config;
            config.SetTileInfo({});
            CHECK(&config != nullptr);
        }
        SECTION("GetTileInfo()") {
            AcquisitionConfig::TileInfo tileInfo;
            tileInfo.tileNumberX = 1;
            tileInfo.tileNumberY = 2;

            AcquisitionConfig config;
            config.SetTileInfo(tileInfo);
            CHECK(config.GetTileInfo().tileNumberX == 1);
            CHECK(config.GetTileInfo().tileNumberY == 2);
        }
        SECTION("IsTileInfoValid()") {
            AcquisitionConfig::TileInfo tileInfo;

            AcquisitionConfig config;
            CHECK(config.IsTileInfoValid() == false);
            config.SetTileInfo(tileInfo);
            CHECK(config.IsTileInfoValid() == true);
        }
    }
    TEST_CASE("AcquisitionConfig : practical test") {
        AcquisitionConfig::JobInfo jobInfo;
        jobInfo.userID = "test";
        jobInfo.title = "test";

        AcquisitionConfig::AcquisitionCount acquisitionCount;
        acquisitionCount.ht3D = 1;
        acquisitionCount.ht2D = 2;
        acquisitionCount.fl3DCH0 = 3;
        acquisitionCount.fl2DCH0 = 4;
        acquisitionCount.fl3DCH1 = 5;
        acquisitionCount.fl2DCH1 = 6;
        acquisitionCount.fl3DCH2 = 7;
        acquisitionCount.fl2DCH2 = 8;

        acquisitionCount.ht3DZ = 9;
        acquisitionCount.fl3DZ = 10;

        acquisitionCount.bf = 11;
        acquisitionCount.bfChannel = 12;

        AcquisitionConfig::AcquisitionSetting acquisitionSetting;
        acquisitionSetting.htZStepLengthMicrometer = 13;
        acquisitionSetting.flZStepLengthMicrometer = 14;
        acquisitionSetting.flCH0ExcitationWaveLengthMicrometer = 15;
        acquisitionSetting.flCH1ExcitationWaveLengthMicrometer = 16;
        acquisitionSetting.flCH2ExcitationWaveLengthMicrometer = 17;
        acquisitionSetting.flCH0EmissionWaveLengthMicrometer = 18;
        acquisitionSetting.flCH1EmissionWaveLengthMicrometer = 19;
        acquisitionSetting.flCH2EmissionWaveLengthMicrometer = 20;
        acquisitionSetting.flCH0Color = { 19,20,21 };
        acquisitionSetting.flCH1Color = { 22,23,24 };
        acquisitionSetting.flCH2Color = { 25,26,27 };
        acquisitionSetting.flCH0Intensity = 28;
        acquisitionSetting.flCH1Intensity = 29;
        acquisitionSetting.flCH2Intensity = 30;
        acquisitionSetting.flCH0ExposureTimeMillisecond = 31;
        acquisitionSetting.flCH1ExposureTimeMillisecond = 32;
        acquisitionSetting.flCH2ExposureTimeMillisecond = 33;

        AcquisitionConfig::AcquisitionPosition acquisitionPosition;
        acquisitionPosition.positionXMillimeter = 19;
        acquisitionPosition.positionYMillimeter = 20;
        acquisitionPosition.positionZMillimeter = 21;
        acquisitionPosition.flAcquisitionZOffsetMicrometer = 22;

        AcquisitionConfig::DeviceInfo deviceInfo;
        deviceInfo.pixelSizeMicrometer = 23;
        deviceInfo.magnification = 24;
        deviceInfo.objectiveNA = 25;
        deviceInfo.condenserNA = 26;
        deviceInfo.mediumRI = 27;
        deviceInfo.deviceSerial = "28";
        deviceInfo.deviceHost = "29";
        deviceInfo.deviceSoftwareVersion = "30";

        AcquisitionConfig::ImageInfo imageInfo;
        imageInfo.roiSizeXPixels = 31;
        imageInfo.roiSizeYPixels = 32;
        imageInfo.roiOffsetXPixels = 33;
        imageInfo.roiOffsetYPixels = 34;

        AcquisitionConfig::TileInfo tileInfo;
        tileInfo.tileNumberX = 35;
        tileInfo.tileNumberY = 36;

        AcquisitionConfig config;
        CHECK(config.IsJobInfoValid() == false);
        CHECK(config.IsAcquisitionCountValid() == false);
        CHECK(config.IsAcquisitionSettingValid() == false);
        CHECK(config.IsAcquisitionPositionValid() == false);
        CHECK(config.IsDeviceInfoValid() == false);
        CHECK(config.IsImageInfoValid() == false);
        CHECK(config.IsTileInfoValid() == false);

        config.SetJobInfo(jobInfo);
        config.SetAcquisitionCount(acquisitionCount);
        config.SetAcquisitionSetting(acquisitionSetting);
        config.SetAcquisitionPosition(acquisitionPosition);
        config.SetDeviceInfo(deviceInfo);
        config.SetImageInfo(imageInfo);
        config.SetTileInfo(tileInfo);

        CHECK(config.IsJobInfoValid() == true);
        CHECK(config.IsAcquisitionCountValid() == true);
        CHECK(config.IsAcquisitionSettingValid() == true);
        CHECK(config.IsAcquisitionPositionValid() == true);
        CHECK(config.IsDeviceInfoValid() == true);
        CHECK(config.IsImageInfoValid() == true);
        CHECK(config.IsTileInfoValid() == true);

        CHECK(config.GetJobInfo().userID == "test");
        CHECK(config.GetJobInfo().title == "test");

        CHECK(config.GetAcquisitionCount().ht3D == 1);
        CHECK(config.GetAcquisitionCount().ht2D == 2);
        CHECK(config.GetAcquisitionCount().fl3DCH0 == 3);
        CHECK(config.GetAcquisitionCount().fl2DCH0 == 4);
        CHECK(config.GetAcquisitionCount().fl3DCH1 == 5);
        CHECK(config.GetAcquisitionCount().fl2DCH1 == 6);
        CHECK(config.GetAcquisitionCount().fl3DCH2 == 7);
        CHECK(config.GetAcquisitionCount().fl2DCH2 == 8);
        CHECK(config.GetAcquisitionCount().ht3DZ == 9);
        CHECK(config.GetAcquisitionCount().fl3DZ == 10);
        CHECK(config.GetAcquisitionCount().bf == 11);
        CHECK(config.GetAcquisitionCount().bfChannel == 12);

        CHECK(config.GetAcquisitionSetting().htZStepLengthMicrometer == 13);
        CHECK(config.GetAcquisitionSetting().flZStepLengthMicrometer == 14);
        CHECK(config.GetAcquisitionSetting().flCH0ExcitationWaveLengthMicrometer == 15);
        CHECK(config.GetAcquisitionSetting().flCH1ExcitationWaveLengthMicrometer == 16);
        CHECK(config.GetAcquisitionSetting().flCH2ExcitationWaveLengthMicrometer == 17);
        CHECK(config.GetAcquisitionSetting().flCH0EmissionWaveLengthMicrometer == 18);
        CHECK(config.GetAcquisitionSetting().flCH1EmissionWaveLengthMicrometer == 19);
        CHECK(config.GetAcquisitionSetting().flCH2EmissionWaveLengthMicrometer == 20);
        CHECK(config.GetAcquisitionSetting().flCH0Color.r == 19);
        CHECK(config.GetAcquisitionSetting().flCH0Color.g == 20);
        CHECK(config.GetAcquisitionSetting().flCH0Color.b == 21);
        CHECK(config.GetAcquisitionSetting().flCH1Color.r == 22);
        CHECK(config.GetAcquisitionSetting().flCH1Color.g == 23);
        CHECK(config.GetAcquisitionSetting().flCH1Color.b == 24);
        CHECK(config.GetAcquisitionSetting().flCH2Color.r == 25);
        CHECK(config.GetAcquisitionSetting().flCH2Color.g == 26);
        CHECK(config.GetAcquisitionSetting().flCH2Color.b == 27);
        CHECK(config.GetAcquisitionSetting().flCH0Intensity == 28);
        CHECK(config.GetAcquisitionSetting().flCH1Intensity == 29);
        CHECK(config.GetAcquisitionSetting().flCH2Intensity == 30);
        CHECK(config.GetAcquisitionSetting().flCH0ExposureTimeMillisecond == 31);
        CHECK(config.GetAcquisitionSetting().flCH1ExposureTimeMillisecond == 32);
        CHECK(config.GetAcquisitionSetting().flCH2ExposureTimeMillisecond == 33);

        CHECK(config.GetAcquisitionPosition().positionXMillimeter == 19);
        CHECK(config.GetAcquisitionPosition().positionYMillimeter == 20);
        CHECK(config.GetAcquisitionPosition().positionZMillimeter == 21);
        CHECK(config.GetAcquisitionPosition().flAcquisitionZOffsetMicrometer == 22);

        CHECK(config.GetDeviceInfo().pixelSizeMicrometer == 23);
        CHECK(config.GetDeviceInfo().magnification == 24);
        CHECK(config.GetDeviceInfo().objectiveNA == 25);
        CHECK(config.GetDeviceInfo().condenserNA == 26);
        CHECK(config.GetDeviceInfo().mediumRI == 27);
        CHECK(config.GetDeviceInfo().deviceSerial == "28");
        CHECK(config.GetDeviceInfo().deviceHost == "29");
        CHECK(config.GetDeviceInfo().deviceSoftwareVersion == "30");

        CHECK(config.GetImageInfo().roiSizeXPixels == 31);
        CHECK(config.GetImageInfo().roiSizeYPixels == 32);
        CHECK(config.GetImageInfo().roiOffsetXPixels == 33);
        CHECK(config.GetImageInfo().roiOffsetYPixels == 34);

        CHECK(config.GetTileInfo().tileNumberX == 35);
        CHECK(config.GetTileInfo().tileNumberY == 36);
    }
}