#include <catch2/catch.hpp>

#include "TileDataGetterProcessedData.h"

#include "BinaryFileIO.h"
#include "CompareArray.h"

namespace TileDataGetterProcessedDataTest {
    const QString testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/HTXProcessingServerTest/Stitching/TileDataGetterProcessedDataTest";

    TEST_CASE("TileDataGetterProcessedData : unit test") {
        SECTION("TileDataGetterProcessedData") {
            TileDataGetterProcessedData tileDataGetter;
            CHECK(&tileDataGetter != nullptr);
        }
        SECTION("SetProcessedFilePath") {
            TileDataGetterProcessedData tileDataGetter;
            tileDataGetter.SetProcessedDataFilePath("");
            CHECK(&tileDataGetter != nullptr);
        }
        SECTION("GetData()") {
            constexpr auto sizeX = 256;
            constexpr auto sizeY = 256;
            constexpr auto sizeZ = 3;

            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

            const QString htFileHDF5Path = testFolderPath + "/htDataForTileDataGetterProcessedData.h5";

            TileDataGetterProcessedData tileDataGetter;
            tileDataGetter.SetProcessedDataFilePath(htFileHDF5Path);
            const auto resultData = tileDataGetter.GetData();

            const std::string htAnswerFilePath = testFolderPath.toStdString() + "/htRawData";
            const auto answerData = ReadFile_float(htAnswerFilePath, numberOfElements);

            CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements) == true);
        }
        SECTION("GetData(x0,x1,y0,y1,z0,z1") {
            const QString htFileHDF5Path = testFolderPath + "/htDataForTileDataGetterProcessedData.h5";

            constexpr auto x0 = 9;
            constexpr auto x1 = 18;
            constexpr auto y0 = 29;
            constexpr auto y1 = 38;
            constexpr auto z0 = 0;
            constexpr auto z1 = 1;

            constexpr auto sizeX = 10;
            constexpr auto sizeY = 10;
            constexpr auto sizeZ = 2;

            constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

            TileDataGetterProcessedData tileDataGetter;
            tileDataGetter.SetProcessedDataFilePath(htFileHDF5Path);
            const auto resultData = tileDataGetter.GetData(x0, x1, y0, y1, z0, z1);

            const std::string htAnswerFilePath = testFolderPath.toStdString() + "/htCropData";
            const auto answerData = ReadFile_float(htAnswerFilePath, numberOfElements);

            CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements) == true);
        }
    }
    TEST_CASE("TileDataGetterProcessedData : practical test") {
        SECTION("GetData()") {
            SECTION("HT") {
                constexpr auto sizeX = 256;
                constexpr auto sizeY = 256;
                constexpr auto sizeZ = 3;

                constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

                const QString htFileHDF5Path = testFolderPath + "/htDataForTileDataGetterProcessedData.h5";

                TileDataGetterProcessedData tileDataGetter;
                tileDataGetter.SetProcessedDataFilePath(htFileHDF5Path);
                const auto resultData = tileDataGetter.GetData();

                const std::string htAnswerFilePath = testFolderPath.toStdString() + "/htRawData";
                const auto answerData = ReadFile_float(htAnswerFilePath, numberOfElements);

                CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements) == true);
            }

            SECTION("FL") {
                constexpr auto sizeX = 256;
                constexpr auto sizeY = 256;
                constexpr auto sizeZ = 3;

                constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

                const QString flFileHDF5Path = testFolderPath + "/flDataForTileDataGetterProcessedData.h5";

                TileDataGetterProcessedData tileDataGetter;
                tileDataGetter.SetProcessedDataFilePath(flFileHDF5Path);
                const auto resultData = tileDataGetter.GetData();

                const std::string flAnswerFilePath = testFolderPath.toStdString() + "/flRawData";
                const auto answerData = ReadFile_float(flAnswerFilePath, numberOfElements);

                CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements) == true);
            }

            SECTION("BF") {
                constexpr auto sizeX = 256;
                constexpr auto sizeY = 256;
                constexpr auto sizeZ = 1;

                constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

                const QString bfFileHDF5Path = testFolderPath + "/bfDataForTileDataGetterProcessedData.h5";

                TileDataGetterProcessedData tileDataGetter;
                tileDataGetter.SetProcessedDataFilePath(bfFileHDF5Path);
                const auto resultData = tileDataGetter.GetData();

                const std::string bfAnswerFilePath = testFolderPath.toStdString() + "/bfRawData";
                const auto bfAnswerData_uint8 = ReadFile_uint8_t(bfAnswerFilePath, numberOfElements);

                auto answerData = std::shared_ptr<float>{ new float[numberOfElements]() };

                for (auto index = 0; index < numberOfElements; ++index) {
                    answerData.get()[index] = static_cast<float>(bfAnswerData_uint8.get()[index]);
                }

                CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements) == true);
            }
        }

        SECTION("GetData(x0,x1,y0,y1,z0,z1") {
            SECTION("HT") {
                const QString htFileHDF5Path = testFolderPath + "/htDataForTileDataGetterProcessedData.h5";

                constexpr auto x0 = 9;
                constexpr auto x1 = 18;
                constexpr auto y0 = 29;
                constexpr auto y1 = 38;
                constexpr auto z0 = 0;
                constexpr auto z1 = 1;

                constexpr auto sizeX = 10;
                constexpr auto sizeY = 10;
                constexpr auto sizeZ = 2;

                constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

                TileDataGetterProcessedData tileDataGetter;
                tileDataGetter.SetProcessedDataFilePath(htFileHDF5Path);
                const auto resultData = tileDataGetter.GetData(x0, x1, y0, y1, z0, z1);

                const std::string htAnswerFilePath = testFolderPath.toStdString() + "/htCropData";
                const auto answerData = ReadFile_float(htAnswerFilePath, numberOfElements);

                CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements) == true);
            }
            SECTION("FL") {
                const QString flFileHDF5Path = testFolderPath + "/flDataForTileDataGetterProcessedData.h5";

                constexpr auto x0 = 9;
                constexpr auto x1 = 18;
                constexpr auto y0 = 29;
                constexpr auto y1 = 38;
                constexpr auto z0 = 0;
                constexpr auto z1 = 1;

                constexpr auto sizeX = 10;
                constexpr auto sizeY = 10;
                constexpr auto sizeZ = 2;

                constexpr auto numberOfElements = sizeX * sizeY * sizeZ;

                TileDataGetterProcessedData tileDataGetter;
                tileDataGetter.SetProcessedDataFilePath(flFileHDF5Path);
                const auto resultData = tileDataGetter.GetData(x0, x1, y0, y1, z0, z1);

                const std::string flAnswerFilePath = testFolderPath.toStdString() + "/flCropData";
                const auto answerData = ReadFile_float(flAnswerFilePath, numberOfElements);

                CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements) == true);
            }
            SECTION("BF") {
                const QString flFileHDF5Path = testFolderPath + "/bfDataForTileDataGetterProcessedData.h5";

                constexpr auto x0 = 9;
                constexpr auto x1 = 18;
                constexpr auto y0 = 29;
                constexpr auto y1 = 38;
                constexpr auto z0 = 0;
                constexpr auto z1 = 2;

                constexpr auto sizeX = 10;
                constexpr auto sizeY = 10;

                constexpr auto numberOfElements = sizeX * sizeY;

                TileDataGetterProcessedData tileDataGetter;
                tileDataGetter.SetProcessedDataFilePath(flFileHDF5Path);
                const auto resultData = tileDataGetter.GetData(x0, x1, y0, y1, z0, z1);

                const std::string bfAnswerFilePath = testFolderPath.toStdString() + "/bfCropData";
                const auto bfAnswerData_uint8 = ReadFile_uint8_t(bfAnswerFilePath, numberOfElements);

                auto answerData = std::shared_ptr<float>{ new float[numberOfElements]() };

                for (auto index = 0; index < numberOfElements; ++index) {
                    answerData.get()[index] = static_cast<float>(bfAnswerData_uint8.get()[index]);
                }

                CHECK(CompareArray(resultData.get(), answerData.get(), numberOfElements) == true);
            }
        }
    }
}