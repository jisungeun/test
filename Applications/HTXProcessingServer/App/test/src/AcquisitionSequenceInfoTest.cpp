#include <catch2/catch.hpp>

#include "AcquisitionSequenceInfo.h"

namespace AcquisitionSequenceInfoTest {
    using SequenceModality = AcquisitionSequenceInfo::Modality;

    TEST_CASE("AcquisitionSequenceInfo : unit test") {
        SECTION("AcquisitionSequnece()") {
            AcquisitionSequenceInfo info;
            CHECK(&info != nullptr);
        }
        SECTION("AcquisitionSequnece(other)") {
            AcquisitionSequenceInfo srcInfo;
            srcInfo.Initialize(1);

            AcquisitionSequenceInfo destInfo(srcInfo);
            CHECK(destInfo.IsInitialized() == true);
        }
        SECTION("operator=()") {
            AcquisitionSequenceInfo srcInfo;
            srcInfo.Initialize(1);

            AcquisitionSequenceInfo destInfo;
            destInfo = srcInfo;
            CHECK(destInfo.IsInitialized() == true);
        }
        SECTION("Initialize()") {
            AcquisitionSequenceInfo info;
            info.Initialize(1);
            CHECK(&info != nullptr);
        }
        SECTION("IsInitialized()") {
            AcquisitionSequenceInfo info;
            SECTION("Before Initialization") {
                CHECK(info.IsInitialized() == false);
            }
            SECTION("After Initialization") {
                info.Initialize(1);
                CHECK(info.IsInitialized() == true);
            }
        }
        SECTION("GetTimeFrameCount()") {
            AcquisitionSequenceInfo info;
            SECTION("Before Initialization") {
                CHECK(info.GetTimeFrameCount() == 0);
            }
            SECTION("After Initialization") {
                info.Initialize(1);
                CHECK(info.GetTimeFrameCount() == 1);
            }
        }
        SECTION("SetAcquisitionZSliceCount()") {
            AcquisitionSequenceInfo info;
            info.Initialize(1);

            info.SetAcquisitionZSliceCount(SequenceModality::HT, 0, 30);
            CHECK(&info != nullptr);
        }
        SECTION("GetAcquisitionZSliceCount()") {
            AcquisitionSequenceInfo info;
            info.Initialize(1);

            info.SetAcquisitionZSliceCount(SequenceModality::HT, 0, 30);
            CHECK(info.GetAcquisitionZSliceCount(SequenceModality::HT, 0) == 30);
        }
        SECTION("AcquisitionExists()") {
            AcquisitionSequenceInfo info;
            info.Initialize(1);

            info.SetAcquisitionZSliceCount(SequenceModality::HT, 0, 1);
            CHECK(info.AcquisitionExists(SequenceModality::HT, 0) == true);
            CHECK(info.AcquisitionExists(SequenceModality::HT, 1) == false);
        }
        SECTION("GetOrderIndex()") {
            AcquisitionSequenceInfo info;
            info.Initialize(5);

            info.SetAcquisitionZSliceCount(SequenceModality::HT, 0, 10);
            info.SetAcquisitionZSliceCount(SequenceModality::HT, 2, 10);
            info.SetAcquisitionZSliceCount(SequenceModality::HT, 4, 10);

            CHECK(info.GetOrderIndex(SequenceModality::HT, 0) == 0);
            CHECK(info.GetOrderIndex(SequenceModality::HT, 1) == -1);
            CHECK(info.GetOrderIndex(SequenceModality::HT, 2) == 1);
            CHECK(info.GetOrderIndex(SequenceModality::HT, 3) == -1);
            CHECK(info.GetOrderIndex(SequenceModality::HT, 4) == 2);
        }
        SECTION("GetFirstTimeFrameIndex()") {
            AcquisitionSequenceInfo info;
            info.Initialize(5);

            info.SetAcquisitionZSliceCount(SequenceModality::HT, 1, 10);
            info.SetAcquisitionZSliceCount(SequenceModality::HT, 3, 10);

            CHECK(info.GetFirstTimeFrameIndex(SequenceModality::HT) == 1);
        }
        SECTION("GetLastTimeFrameIndex()") {
            AcquisitionSequenceInfo info;
            info.Initialize(5);

            info.SetAcquisitionZSliceCount(SequenceModality::HT, 1, 10);
            info.SetAcquisitionZSliceCount(SequenceModality::HT, 3, 10);

            CHECK(info.GetLastTimeFrameIndex(SequenceModality::HT) == 3);
        }
        SECTION("GetTimeFrameIndex()") {
            AcquisitionSequenceInfo info;
            info.Initialize(5);

            info.SetAcquisitionZSliceCount(SequenceModality::HT, 1, 10);
            info.SetAcquisitionZSliceCount(SequenceModality::HT, 3, 10);

            CHECK(info.GetTimeFrameIndex(SequenceModality::HT, 0) == 1);
            CHECK(info.GetTimeFrameIndex(SequenceModality::HT, 1) == 3);
        }
    }
    TEST_CASE("AcquisitionSequenceInfo : practical test") {
        constexpr auto timeFrameCount = 2;

        AcquisitionSequenceInfo info;
        info.Initialize(timeFrameCount);

        CHECK(info.IsInitialized() == true);
        CHECK(info.GetTimeFrameCount() == timeFrameCount);

        info.SetAcquisitionZSliceCount(SequenceModality::HT, 0, 30);
        info.SetAcquisitionZSliceCount(SequenceModality::HT, 1, 1);

        info.SetAcquisitionZSliceCount(SequenceModality::FLCH0, 0, 30);
        info.SetAcquisitionZSliceCount(SequenceModality::FLCH0, 1, 1);

        info.SetAcquisitionZSliceCount(SequenceModality::FLCH2, 0, 30);
        info.SetAcquisitionZSliceCount(SequenceModality::FLCH2, 1, 1);

        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::HT, 0) == 30);
        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::HT, 1) == 1);

        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::FLCH0, 0) == 30);
        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::FLCH0, 1) == 1);

        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::FLCH2, 0) == 30);
        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::FLCH2, 1) == 1);

        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::HT, -1) == 0);
        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::FLCH0, -1) == 0);
        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::FLCH2, -1) == 0);

        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::HT, 2) == 0);
        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::FLCH0, 2) == 0);
        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::FLCH2, 2) == 0);

        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::FLCH1, 0) == 0);
        CHECK(info.GetAcquisitionZSliceCount(SequenceModality::FLCH1, 1) == 0);
    }
}