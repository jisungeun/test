#include <catch2/catch.hpp>

#include "TaskInfoSetScanner.h"
#include "AcquisitionConfigReader.h"

#include "HDF5Mutex.h"

namespace TaskInfoSetScannerTest {
    TEST_CASE("TaskInfoSetScanner : unit test") {
        // Implement unit test
    }
    TEST_CASE("TaskInfoSetScanner : practical test") {
        const QString rootFolderPath = "E:/00_Data/20220425 HTX Timelapse ProcessingFail/HTX-VAL-B11_6well_StitchingFail/20220421.173211.Beta-timelapse.HTX-VAL-B11_6well.091.Jurkat.A2.P008";

        AcquisitionConfigReader acquisitionConfigReader;
        acquisitionConfigReader.SetFilePath(rootFolderPath + "/config.dat");
        acquisitionConfigReader.Read();

        const auto acquisitionConfig = acquisitionConfigReader.GetAcquisitionConfig();

        //DataCountInfo dataCountInfo;
        //dataCountInfo.SetHTTimelapseCount(acquisitionConfig.GetAcquisitionCount().ht3D);
        ////dataCountInfo.SetFLAcquisitionSequence(acquisitionConfig.GetFLAcquisitionSequence());
        //dataCountInfo.SetBFTimelapseCount(acquisitionConfig.GetAcquisitionCount().bf);
        //dataCountInfo.SetTileNumber(acquisitionConfig.GetTileInfo().tileNumberX, acquisitionConfig.GetTileInfo().tileNumberY);

        //TaskInfoSetScanner scanner;
        //scanner.SetRootFolderPath(rootFolderPath);
        //scanner.SetDataCountInfo(dataCountInfo);

        //{
        //    TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        //    scanner.Scan();
        //}

        //const auto resultTaskInfoSet = scanner.GetTaskInfoSet();

        //for (auto tileIndex = 0; tileIndex < 9; ++tileIndex) {
        //    for (auto timeIndex = 0; timeIndex < 24; ++timeIndex) {
        //        //CHECK(resultTaskInfoSet.GetHTProcessedTaskInfo(tileIndex, timeIndex).GetProcessedProgress() == 1);
        //    }
        //}

        //for (auto timeIndex = 0; timeIndex < 24; ++timeIndex) {
        //    //CHECK(resultTaskInfoSet.GetHTStitchedTaskInfo(timeIndex).GetProcessedProgress() == 1);
        //    //CHECK(resultTaskInfoSet.GetHTStitchingWrittenTaskInfo(timeIndex).GetProcessedProgress() == 0);
        //}
    }
}