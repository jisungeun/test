#include <catch2/catch.hpp>

#include <QDir>

#include "ProfileFileNamer.h"

#include "ProfileFileFinder.h"
#include "BinaryFileIO.h"

namespace ProfileFileFinderTest {
    using namespace TC::ProfileIO;
    using namespace TC::HTProcessingProfile;
    using namespace TC::PSFProfile;

    auto WriteTempFile(const QString& path)->bool {
        std::shared_ptr<char[]> data{ new char[1] };
        return WriteFile(path.toStdString(), data, 1);
    }

    const QString profileName = "default";

    TEST_CASE("ProfileFileFinderTest") {
        const QString rootFolderPath = "rootFolderPath";
        const QString profileFolderPath = QString("%1/profile").arg(rootFolderPath);

        if (QDir(rootFolderPath).exists()) {
            CHECK(QDir(rootFolderPath).removeRecursively());
        }

        SECTION("no profile folder") {
            ProfileFileFinder finder;
            finder.SetRootFolderPath(rootFolderPath);
            CHECK(finder.Find() == false);
            const auto resultProcessingProfileFilePath = finder.GetHTProcessingProfileFilePath();
            const auto resultPSFProfileFilePath = finder.GetPSFProfileFilePath();

            CHECK(resultProcessingProfileFilePath.isEmpty() == true);
            CHECK(resultPSFProfileFilePath.isEmpty() == true);
        }

        SECTION("no profile files") {
            CHECK(QDir().mkpath(profileFolderPath) == true);

            ProfileFileFinder finder;
            finder.SetRootFolderPath(rootFolderPath);
            CHECK(finder.Find() == false);
            const auto resultProcessingProfileFilePath = finder.GetHTProcessingProfileFilePath();
            const auto resultPSFProfileFilePath = finder.GetPSFProfileFilePath();

            CHECK(resultProcessingProfileFilePath.isEmpty() == true);
            CHECK(resultPSFProfileFilePath.isEmpty() == true);
        }

        SECTION("only htprocessing profile exists") {
            CHECK(QDir().mkpath(profileFolderPath) == true);
            const auto htProcessingProfileFileName = ProfileFileNamer::AppendExtension(profileName, HTProcessingProfileVersion::v1_4_1_d);
            const auto htProcessingProfileFilePath = QString("%1/%2").arg(profileFolderPath).arg(htProcessingProfileFileName);

            CHECK(WriteTempFile(htProcessingProfileFilePath));

            ProfileFileFinder finder;
            finder.SetRootFolderPath(rootFolderPath);
            CHECK(finder.Find() == false);
            const auto resultProcessingProfileFilePath = finder.GetHTProcessingProfileFilePath();
            const auto resultPSFProfileFilePath = finder.GetPSFProfileFilePath();

            CHECK(resultProcessingProfileFilePath.isEmpty() == true);
            CHECK(resultPSFProfileFilePath.isEmpty() == true);
        }

        SECTION("only psf profile exists") {
            CHECK(QDir().mkpath(profileFolderPath) == true);
            const auto psfProfileFileName = ProfileFileNamer::AppendExtension(profileName, PSFProfileVersion::v1_4_1_c);
            const auto psfProfileFilePath = QString("%1/%2").arg(profileFolderPath).arg(psfProfileFileName);
            CHECK(WriteTempFile(psfProfileFilePath));

            ProfileFileFinder finder;
            finder.SetRootFolderPath(rootFolderPath);
            CHECK(finder.Find() == false);
            const auto resultProcessingProfileFilePath = finder.GetHTProcessingProfileFilePath();
            const auto resultPSFProfileFilePath = finder.GetPSFProfileFilePath();

            CHECK(resultProcessingProfileFilePath.isEmpty() == true);
            CHECK(resultPSFProfileFilePath.isEmpty() == true);
        }

        SECTION("both profile exist") {
            CHECK(QDir().mkpath(profileFolderPath) == true);
            const auto htProcessingProfileFileName = ProfileFileNamer::AppendExtension(profileName, HTProcessingProfileVersion::v1_4_1_d);
            const auto psfProfileFileName = ProfileFileNamer::AppendExtension(profileName, PSFProfileVersion::v1_4_1_c);
            const auto htProcessingProfileFilePath = QString("%1/%2").arg(profileFolderPath).arg(htProcessingProfileFileName);
            const auto psfProfileFilePath = QString("%1/%2").arg(profileFolderPath).arg(psfProfileFileName);

            CHECK(WriteTempFile(htProcessingProfileFilePath));
            CHECK(WriteTempFile(psfProfileFilePath));

            ProfileFileFinder finder;
            finder.SetRootFolderPath(rootFolderPath);
            CHECK(finder.Find() == true);
            const auto resultProcessingProfileFilePath = finder.GetHTProcessingProfileFilePath();
            const auto resultPSFProfileFilePath = finder.GetPSFProfileFilePath();

            CHECK(resultProcessingProfileFilePath == QFileInfo(htProcessingProfileFilePath).absoluteFilePath());
            CHECK(resultPSFProfileFilePath == QFileInfo(psfProfileFilePath).absoluteFilePath());
        }

    }
}