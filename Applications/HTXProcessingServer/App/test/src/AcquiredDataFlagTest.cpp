#include <catch2/catch.hpp>

#include "AcquiredDataFlag.h"

namespace AcquiredDataFlagTest {
    TEST_CASE("AcquiredDataFlag : unit test") {
        SECTION("AcquiredDataFlag()") {
            AcquiredDataFlag acquiredDataFlag;
            CHECK(&acquiredDataFlag != nullptr);
        }
        SECTION("AcquiredDataFlag(other)") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 30);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);
            
            AcquiredDataFlag srcAcquiredDataFlag;
            srcAcquiredDataFlag.Initialize(acquisitionCount);
            srcAcquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);

            AcquiredDataFlag destAcquiredDataFlag(srcAcquiredDataFlag);
            CHECK(destAcquiredDataFlag.GetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }) == true);
        }
        SECTION("operator=()") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 30);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquiredDataFlag srcAcquiredDataFlag;
            srcAcquiredDataFlag.Initialize(acquisitionCount);
            srcAcquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);

            AcquiredDataFlag destAcquiredDataFlag;
            destAcquiredDataFlag = srcAcquiredDataFlag;
            CHECK(destAcquiredDataFlag.GetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }) == true);
        }
        SECTION("Initialize()") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 30);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquiredDataFlag acquiredDataFlag;
            acquiredDataFlag.Initialize(acquisitionCount);

            CHECK(&acquiredDataFlag != nullptr);
        }
        SECTION("SetBGAcquiredFlag()") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 30);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquiredDataFlag acquiredDataFlag;
            acquiredDataFlag.Initialize(acquisitionCount);

            acquiredDataFlag.SetBGAcquiredFlag(true);

            CHECK(&acquiredDataFlag != nullptr);
        }
        SECTION("GetBGAcquiredFlag()") {
            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 30);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(1, 1);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquiredDataFlag acquiredDataFlag;
            acquiredDataFlag.Initialize(acquisitionCount);

            acquiredDataFlag.SetBGAcquiredFlag(true);
            CHECK(acquiredDataFlag.GetBGAcquiredFlag() == true);
        }

    }
    TEST_CASE("AcquiredDataFlag : practical test") {
        SECTION("few data") {
            constexpr auto tileNumberX = 2;
            constexpr auto tileNumberY = 2;

            AcquisitionSequenceInfo acquisitionSequenceInfo;
            acquisitionSequenceInfo.Initialize(3);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 30);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 1, 30);

            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::BF, 0, 1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::BF, 1, 1);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::BF, 2, 1);

            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 0, 10);
            acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH1, 0, 10);

            AcquisitionCount acquisitionCount;
            acquisitionCount.SetTileNumber(tileNumberX, tileNumberY);
            acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

            AcquiredDataFlag acquiredDataFlag;
            acquiredDataFlag.Initialize(acquisitionCount);


            acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }, true);
            acquiredDataFlag.SetAcquiredFlag(0, 1, ModalityType{ ModalityType::Name::HT }, true);
            acquiredDataFlag.SetAcquiredFlag(0, 2, ModalityType{ ModalityType::Name::HT }, true);
            acquiredDataFlag.SetAcquiredFlag(0, 3, ModalityType{ ModalityType::Name::HT }, true);
            acquiredDataFlag.SetAcquiredFlag(1, 0, ModalityType{ ModalityType::Name::HT }, true);
            acquiredDataFlag.SetAcquiredFlag(1, 1, ModalityType{ ModalityType::Name::HT }, true);
            acquiredDataFlag.SetAcquiredFlag(1, 2, ModalityType{ ModalityType::Name::HT }, false);
            acquiredDataFlag.SetAcquiredFlag(1, 3, ModalityType{ ModalityType::Name::HT }, false);

            acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::BF }, true);
            acquiredDataFlag.SetAcquiredFlag(0, 1, ModalityType{ ModalityType::Name::BF }, true);
            acquiredDataFlag.SetAcquiredFlag(0, 2, ModalityType{ ModalityType::Name::BF }, true);
            acquiredDataFlag.SetAcquiredFlag(0, 3, ModalityType{ ModalityType::Name::BF }, true);
            acquiredDataFlag.SetAcquiredFlag(1, 0, ModalityType{ ModalityType::Name::BF }, true);
            acquiredDataFlag.SetAcquiredFlag(1, 1, ModalityType{ ModalityType::Name::BF }, true);
            acquiredDataFlag.SetAcquiredFlag(1, 2, ModalityType{ ModalityType::Name::BF }, true);
            acquiredDataFlag.SetAcquiredFlag(1, 3, ModalityType{ ModalityType::Name::BF }, true);
            acquiredDataFlag.SetAcquiredFlag(2, 0, ModalityType{ ModalityType::Name::BF }, true);
            acquiredDataFlag.SetAcquiredFlag(2, 1, ModalityType{ ModalityType::Name::BF }, false);
            acquiredDataFlag.SetAcquiredFlag(2, 2, ModalityType{ ModalityType::Name::BF }, false);
            acquiredDataFlag.SetAcquiredFlag(2, 3, ModalityType{ ModalityType::Name::BF }, false);

            acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 0 }, true);
            acquiredDataFlag.SetAcquiredFlag(0, 1, ModalityType{ ModalityType::Name::FL, 0 }, true);
            acquiredDataFlag.SetAcquiredFlag(0, 2, ModalityType{ ModalityType::Name::FL, 0 }, true);
            acquiredDataFlag.SetAcquiredFlag(0, 3, ModalityType{ ModalityType::Name::FL, 0 }, false);

            acquiredDataFlag.SetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 1 }, true);
            acquiredDataFlag.SetAcquiredFlag(0, 1, ModalityType{ ModalityType::Name::FL, 1 }, true);
            acquiredDataFlag.SetAcquiredFlag(0, 2, ModalityType{ ModalityType::Name::FL, 1 }, false);
            acquiredDataFlag.SetAcquiredFlag(0, 3, ModalityType{ ModalityType::Name::FL, 1 }, false);

            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::HT }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 1, ModalityType{ ModalityType::Name::HT }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 2, ModalityType{ ModalityType::Name::HT }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 3, ModalityType{ ModalityType::Name::HT }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(1, 0, ModalityType{ ModalityType::Name::HT }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(1, 1, ModalityType{ ModalityType::Name::HT }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(1, 2, ModalityType{ ModalityType::Name::HT }) == false);
            CHECK(acquiredDataFlag.GetAcquiredFlag(1, 3, ModalityType{ ModalityType::Name::HT }) == false);

            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::BF }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 1, ModalityType{ ModalityType::Name::BF }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 2, ModalityType{ ModalityType::Name::BF }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 3, ModalityType{ ModalityType::Name::BF }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(1, 0, ModalityType{ ModalityType::Name::BF }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(1, 1, ModalityType{ ModalityType::Name::BF }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(1, 2, ModalityType{ ModalityType::Name::BF }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(1, 3, ModalityType{ ModalityType::Name::BF }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(2, 0, ModalityType{ ModalityType::Name::BF }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(2, 1, ModalityType{ ModalityType::Name::BF }) == false);
            CHECK(acquiredDataFlag.GetAcquiredFlag(2, 2, ModalityType{ ModalityType::Name::BF }) == false);
            CHECK(acquiredDataFlag.GetAcquiredFlag(2, 3, ModalityType{ ModalityType::Name::BF }) == false);

            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 0 }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 1, ModalityType{ ModalityType::Name::FL, 0 }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 2, ModalityType{ ModalityType::Name::FL, 0 }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 3, ModalityType{ ModalityType::Name::FL, 0 }) == false);

            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 0, ModalityType{ ModalityType::Name::FL, 1 }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 1, ModalityType{ ModalityType::Name::FL, 1 }) == true);
            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 2, ModalityType{ ModalityType::Name::FL, 1 }) == false);
            CHECK(acquiredDataFlag.GetAcquiredFlag(0, 3, ModalityType{ ModalityType::Name::FL, 1 }) == false);
        }

        SECTION("Error Case") {
            SECTION("copy constructor check") {
                constexpr auto tileNumberX = 10;
                constexpr auto tileNumberY = 10;

                AcquisitionSequenceInfo acquisitionSequenceInfo;
                acquisitionSequenceInfo.Initialize(1);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::HT, 0, 120);

                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 0, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH1, 0, 10);
                acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH2, 0, 10);

                AcquisitionCount acquisitionCount;
                acquisitionCount.SetTileNumber(tileNumberX, tileNumberY);
                acquisitionCount.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

                AcquiredDataFlag acquiredDataFlag;
                acquiredDataFlag.Initialize(acquisitionCount);

                for (auto i = 0; i < tileNumberX * tileNumberY; ++i) {
                    acquiredDataFlag.SetAcquiredFlag(0, i, ModalityType{ ModalityType::Name::HT }, true);
                    acquiredDataFlag.SetAcquiredFlag(0, i, ModalityType{ ModalityType::Name::FL, 0 }, true);
                    acquiredDataFlag.SetAcquiredFlag(0, i, ModalityType{ ModalityType::Name::FL, 1 }, true);
                    acquiredDataFlag.SetAcquiredFlag(0, i, ModalityType{ ModalityType::Name::FL, 2 }, true);
                }

                constexpr auto iterationNumber = 1000;
                for (auto iter = 0; iter < iterationNumber; ++iter) {
                    const auto copiedAcquisitionDataFlag = acquiredDataFlag;

                    for (auto i = 0; i < tileNumberX * tileNumberY; ++i) {
                        CHECK(copiedAcquisitionDataFlag.GetAcquiredFlag(0, i, ModalityType{ ModalityType::Name::HT }) == true);
                        CHECK(copiedAcquisitionDataFlag.GetAcquiredFlag(0, i, ModalityType{ ModalityType::Name::FL, 0 }) == true);
                        CHECK(copiedAcquisitionDataFlag.GetAcquiredFlag(0, i, ModalityType{ ModalityType::Name::FL, 1 }) == true);
                        CHECK(copiedAcquisitionDataFlag.GetAcquiredFlag(0, i, ModalityType{ ModalityType::Name::FL, 2 }) == true);
                    }
                }
            }
        }
    }
}