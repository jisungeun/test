#include <catch2/catch.hpp>

#include "StitchingWriterHT.h"
#include "TCFSkeletonConfig.h"
#include "TCFSkeletonWriter.h"

namespace StitchingWriterHTTest {
    TEST_CASE("StitchingWriterHT : unit test") {
        //TODO Implement unit test
    }
    TEST_CASE("StitchingWriterHT : practical test") {
        TCFSkeletonConfig tcfSkeletonConfig;
        tcfSkeletonConfig.SetDataIncludingFlag(true, false, false);

        const QString rootFolderPath = "E:/00_Data/20220406 HTX Tile Square Image Data HT FL/sampling/20220405.193549.TestProject.VAL-B02.002.Hep3B mitoDsRed.Well_1_1.P000";
        const QString tempTCFFilePath = "E:/00_Data/20220406 HTX Tile Square Image Data HT FL/sampling/20220405.193549.TestProject.VAL-B02.002.Hep3B mitoDsRed.Well_1_1.P000/temp/20220405.193549.TestProject.VAL-B02.002.Hep3B mitoDsRed.Well_1_1.P000.tctmp";

        TCFSkeletonWriter tcfSkeletonWriter;
        tcfSkeletonWriter.SetConfig(tcfSkeletonConfig);
        tcfSkeletonWriter.SetTargetFilePath(tempTCFFilePath);

        tcfSkeletonWriter.Write();

        StitchingWriterHT stitchingWriterHT;

        stitchingWriterHT.SetRootFolderPath(rootFolderPath);
        stitchingWriterHT.SetTempTCFFilePath(tempTCFFilePath);

        stitchingWriterHT.Write(0);
    }
}
