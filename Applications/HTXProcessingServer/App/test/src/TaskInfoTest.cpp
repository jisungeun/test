#include <catch2/catch.hpp>

#include "TaskInfo.h"

namespace TaskInfoTest {
    TEST_CASE("TaskInfo : unit test") {
        SECTION("TaskInfo()") {
            TaskInfo taskInfo;
            CHECK(&taskInfo != nullptr);
        }
        SECTION("TaskInfo(other)") {
            TaskInfo srcTaskInfo;
            srcTaskInfo.SetProcessedProgress(1);

            const TaskInfo destTaskInfo(srcTaskInfo);
            CHECK(destTaskInfo.GetProcessedProgress() == 1);
        }
        SECTION("operator=()") {
            TaskInfo srcTaskInfo;
            srcTaskInfo.SetProcessedProgress(1);

            TaskInfo destTaskInfo;
            destTaskInfo = srcTaskInfo;
            CHECK(destTaskInfo.GetProcessedProgress() == 1);
        }
        SECTION("operator==()") {
            TaskInfo taskInfo1;
            taskInfo1.SetProcessedProgress(1);
            taskInfo1.SetProcessedTime(1, TimeUnit::Second);

            TaskInfo taskInfo2;
            taskInfo2.SetProcessedProgress(1);
            taskInfo2.SetProcessedTime(1, TimeUnit::Second);

            CHECK((taskInfo1 == taskInfo2) == true);
        }
        SECTION("operator!=()") {
            TaskInfo taskInfo1;
            taskInfo1.SetProcessedProgress(1);
            taskInfo1.SetProcessedTime(1, TimeUnit::Second);

            TaskInfo taskInfo2;
            taskInfo2.SetProcessedProgress(0.1);
            taskInfo2.SetProcessedTime(1, TimeUnit::Second);

            CHECK((taskInfo1 != taskInfo2) == true);
        }
        SECTION("SetProcessedTime()") {
            TaskInfo taskInfo;
            taskInfo.SetProcessedTime(1, TimeUnit::Second);
            CHECK(&taskInfo != nullptr);
        }
        SECTION("GetProcessedTime()") {
            TaskInfo taskInfo;
            taskInfo.SetProcessedTime(1, TimeUnit::Second);
            CHECK(taskInfo.GetProcessedTime(TimeUnit::Millisecond) == 1000);
        }
        SECTION("SetProcessedProgress()") {
            TaskInfo taskInfo;
            taskInfo.SetProcessedProgress(1);
            CHECK(&taskInfo != nullptr);
        }
        SECTION("SetFailed()") {
            TaskInfo taskInfo;
            taskInfo.SetFailed(true);
            CHECK(&taskInfo != nullptr);
        }
        SECTION("GetProcessedProgress()") {
            TaskInfo taskInfo;
            taskInfo.SetProcessedProgress(1);
            CHECK(taskInfo.GetProcessedProgress() == 1);
        }
        SECTION("SetOnProcessing()") {
            TaskInfo taskInfo;
            taskInfo.SetOnProcessing(true);
            CHECK(&taskInfo != nullptr);
        }
        SECTION("GetOnProcessing()") {
            TaskInfo taskInfo;
            taskInfo.SetOnProcessing(true);
            CHECK(taskInfo.GetOnProcessing() == true);
        }
        SECTION("GetFailed()") {
            TaskInfo taskInfo;
            taskInfo.SetFailed(true);
            CHECK(taskInfo.GetFailed() == true);
        }
    }
    TEST_CASE("TaskInfo : practical test") {
        TaskInfo taskInfo;
        taskInfo.SetProcessedProgress(1);
        taskInfo.SetProcessedTime(2, TimeUnit::Second);
        taskInfo.SetOnProcessing(true);

        CHECK(taskInfo.GetProcessedProgress() == 1);
        CHECK(taskInfo.GetProcessedTime(TimeUnit::Millisecond) == 2000);
        CHECK(taskInfo.GetOnProcessing() == true);
    }
}