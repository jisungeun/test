#include <catch2/catch.hpp>

#include "ModalityType.h"

namespace ModalityTypeTest {
    TEST_CASE("ModalityType : unit test") {
        SECTION("ModalityType(modality)") {
            ModalityType modalityType{ ModalityType::Name::HT };
            CHECK(modalityType.GetName() == ModalityType::Name::HT);

            SECTION("default channelIndex") {
                CHECK(modalityType.GetChannelIndex() == -1);
            }
        }
        SECTION("ModalityType(name, channelIndex)") {
            ModalityType modalityType{ ModalityType::Name::FL, 1 };
            CHECK(modalityType.GetName() == ModalityType::Name::FL);
            CHECK(modalityType.GetChannelIndex() == 1);
        }
        SECTION("ModalityType(other)") {
            ModalityType srcModalityType{ ModalityType::Name::FL, 1 };

            const ModalityType destModalityType(srcModalityType);
            CHECK(destModalityType.GetName() == ModalityType::Name::FL);
            CHECK(destModalityType.GetChannelIndex() == 1);
        }
        SECTION("operator=(other)") {
            ModalityType srcModalityType{ ModalityType::Name::FL, 1 };

            ModalityType destModalityType{ModalityType::Name::HT};
            CHECK(destModalityType.GetName() == ModalityType::Name::HT);
            CHECK(destModalityType.GetChannelIndex() == -1);

            destModalityType = srcModalityType;
            CHECK(destModalityType.GetName() == ModalityType::Name::FL);
            CHECK(destModalityType.GetChannelIndex() == 1);
        }
        SECTION("operator==(other)") {
            ModalityType modalityType1{ ModalityType::Name::FL, 1 };
            SECTION("All Same") {
                ModalityType modalityType2{ ModalityType::Name::FL, 1 };
                CHECK((modalityType1 == modalityType2) == true);
            }
            SECTION("Name Different"){
                ModalityType modalityType2{ ModalityType::Name::HT, 1 };
                CHECK((modalityType1 == modalityType2) == false);
            }
            SECTION("ChannelIndex Different"){
                ModalityType modalityType2{ ModalityType::Name::FL, 2 };
                CHECK((modalityType1 == modalityType2) == false);
            }
        }
        SECTION("operator!=(other)") {
            ModalityType modalityType1{ ModalityType::Name::FL, 1 };
            SECTION("All Same") {
                ModalityType modalityType2{ ModalityType::Name::FL, 1 };
                CHECK((modalityType1 != modalityType2) == false);
            }
            SECTION("Name Different") {
                ModalityType modalityType2{ ModalityType::Name::HT, 1 };
                CHECK((modalityType1 != modalityType2) == true);
            }
            SECTION("ChannelIndex Different") {
                ModalityType modalityType2{ ModalityType::Name::FL, 2 };
                CHECK((modalityType1 != modalityType2) == true);
            }
        }
        SECTION("operator==(name)") {
            ModalityType modalityType{ ModalityType::Name::FL, 1 };
            CHECK((modalityType == ModalityType::Name::HT) == false);
            CHECK((modalityType == ModalityType::Name::FL) == true);
            CHECK((modalityType == ModalityType::Name::BF) == false);
        }
        SECTION("operator!=(name)") {
            ModalityType modalityType{ ModalityType::Name::FL, 1 };
            CHECK((modalityType != ModalityType::Name::HT) == true);
            CHECK((modalityType != ModalityType::Name::FL) == false);
            CHECK((modalityType != ModalityType::Name::BF) == true);
        }
        SECTION("GetName()") {
            ModalityType modalityType{ ModalityType::Name::FL, 1 };
            CHECK(modalityType.GetName() == ModalityType::Name::FL);
        }
        SECTION("GetChannelIndex()") {
            ModalityType modalityType{ ModalityType::Name::FL, 1 };
            CHECK(modalityType.GetChannelIndex() == 1);
        }
    }
    TEST_CASE("ModalityType : practical test") {
        //TODO Implement ModalityType : practical test
    }
}