#include <catch2/catch.hpp>

#include "StitchingWriterFL.h"
#include "TCFSkeletonConfig.h"
#include "TCFSkeletonWriter.h"

namespace StitchingWriterFLTest {
    TEST_CASE("StitchingWriterFL : unit test") {
        //TODO Implement unit test
    }
    TEST_CASE("StitchingWriterFL : practical test") {
        TCFSkeletonConfig tcfSkeletonConfig;
        tcfSkeletonConfig.SetDataIncludingFlag(false, true, false);

        const QString rootFolderPath = "E:/00_Data/20220406 HTX Tile Square Image Data HT FL/sampling/20220405.193549.TestProject.VAL-B02.002.Hep3B mitoDsRed.Well_1_1.P000";
        const QString tempTCFFilePath = "E:/00_Data/20220406 HTX Tile Square Image Data HT FL/sampling/20220405.193549.TestProject.VAL-B02.002.Hep3B mitoDsRed.Well_1_1.P000/temp/20220405.193549.TestProject.VAL-B02.002.Hep3B mitoDsRed.Well_1_1.P000.tctmp";

        AcquisitionSequenceInfo acquisitionSequenceInfo;
        acquisitionSequenceInfo.Initialize(1);
        acquisitionSequenceInfo.SetAcquisitionZSliceCount(AcquisitionSequenceInfo::Modality::FLCH0, 0, 23);

        TCFSkeletonWriter tcfSkeletonWriter;
        tcfSkeletonWriter.SetConfig(tcfSkeletonConfig);
        tcfSkeletonWriter.SetTargetFilePath(tempTCFFilePath);

        tcfSkeletonWriter.Write();

        StitchingWriterFL stitchingWriterFL;

        stitchingWriterFL.SetRootFolderPath(rootFolderPath);
        stitchingWriterFL.SetTempTCFFilePath(tempTCFFilePath);
        stitchingWriterFL.SetAcquisitionSequenceInfo(acquisitionSequenceInfo);

        stitchingWriterFL.Write(0, 0);
    }
}
