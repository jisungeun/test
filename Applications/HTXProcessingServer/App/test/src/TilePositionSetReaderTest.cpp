#include <catch2/catch.hpp>

#include "H5Cpp.h"

#include "TilePositionSetReader.h"



namespace TilePositionSetReaderTest {
    auto WriteTilePositionSet(const QString& filePath)->void {
        const H5::H5File file(filePath.toStdString(), H5F_ACC_TRUNC);

        constexpr int32_t tilePositionX00 = 0;
        constexpr int32_t tilePositionX01 = 1;
        constexpr int32_t tilePositionX10 = 2;
        constexpr int32_t tilePositionX11 = 3;

        constexpr int32_t tilePositionY00 = 4;
        constexpr int32_t tilePositionY01 = 5;
        constexpr int32_t tilePositionY10 = 6;
        constexpr int32_t tilePositionY11 = 7;
        
        const auto attributeX00 = file.createAttribute("tilePositionX_x0_y0", H5::PredType::NATIVE_INT32, H5S_SCALAR);
        const auto attributeX01 = file.createAttribute("tilePositionX_x0_y1", H5::PredType::NATIVE_INT32, H5S_SCALAR);
        const auto attributeX10 = file.createAttribute("tilePositionX_x1_y0", H5::PredType::NATIVE_INT32, H5S_SCALAR);
        const auto attributeX11 = file.createAttribute("tilePositionX_x1_y1", H5::PredType::NATIVE_INT32, H5S_SCALAR);
        const auto attributeY00 = file.createAttribute("tilePositionY_x0_y0", H5::PredType::NATIVE_INT32, H5S_SCALAR);
        const auto attributeY01 = file.createAttribute("tilePositionY_x0_y1", H5::PredType::NATIVE_INT32, H5S_SCALAR);
        const auto attributeY10 = file.createAttribute("tilePositionY_x1_y0", H5::PredType::NATIVE_INT32, H5S_SCALAR);
        const auto attributeY11 = file.createAttribute("tilePositionY_x1_y1", H5::PredType::NATIVE_INT32, H5S_SCALAR);
        
        attributeX00.write(H5::PredType::NATIVE_INT32, &tilePositionX00);
        attributeX01.write(H5::PredType::NATIVE_INT32, &tilePositionX01);
        attributeX10.write(H5::PredType::NATIVE_INT32, &tilePositionX10);
        attributeX11.write(H5::PredType::NATIVE_INT32, &tilePositionX11);

        attributeY00.write(H5::PredType::NATIVE_INT32, &tilePositionY00);
        attributeY01.write(H5::PredType::NATIVE_INT32, &tilePositionY01);
        attributeY10.write(H5::PredType::NATIVE_INT32, &tilePositionY10);
        attributeY11.write(H5::PredType::NATIVE_INT32, &tilePositionY11);
    }


    TEST_CASE("TilePositionSetReader : unit test") {
        SECTION("TilePositionSetReader()") {
            TilePositionSetReader reader;
            CHECK(&reader != nullptr);
        }
        SECTION("SetTargetFilePath()") {
            TilePositionSetReader reader;
            reader.SetTargetFilePath("");
            CHECK(&reader != nullptr);
        }
        SECTION("Read()") {
            const QString filePath = "tilePositionSet.h5";

            WriteTilePositionSet(filePath);

            TilePositionSetReader reader;
            reader.SetTargetFilePath(filePath);
            CHECK(reader.Read() == true);
        }
        SECTION("GetTilePositionSet()") {
            const QString filePath = "tilePositionSet.h5";

            WriteTilePositionSet(filePath);

            TilePositionSetReader reader;
            reader.SetTargetFilePath(filePath);

            reader.Read();

            const auto tilePositionSet = reader.GetTilePositionSet();

            CHECK(tilePositionSet.GetTilePosition(0, 0).GetTilePositionX() == 0);
            CHECK(tilePositionSet.GetTilePosition(0, 1).GetTilePositionX() == 1);
            CHECK(tilePositionSet.GetTilePosition(1, 0).GetTilePositionX() == 2);
            CHECK(tilePositionSet.GetTilePosition(1, 1).GetTilePositionX() == 3);

            CHECK(tilePositionSet.GetTilePosition(0, 0).GetTilePositionY() == 4);
            CHECK(tilePositionSet.GetTilePosition(0, 1).GetTilePositionY() == 5);
            CHECK(tilePositionSet.GetTilePosition(1, 0).GetTilePositionY() == 6);
            CHECK(tilePositionSet.GetTilePosition(1, 1).GetTilePositionY() == 7);

            CHECK(tilePositionSet.GetTilePosition(0, 0).GetTilePositionZ() == 0);
            CHECK(tilePositionSet.GetTilePosition(0, 1).GetTilePositionZ() == 0);
            CHECK(tilePositionSet.GetTilePosition(1, 0).GetTilePositionZ() == 0);
            CHECK(tilePositionSet.GetTilePosition(1, 1).GetTilePositionZ() == 0);
        }
    }
    TEST_CASE("TilePositionSetReader : practical test") {
        //TODO Implement test
    }
}