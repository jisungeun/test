#include <catch2/catch.hpp>

#include "TileDataHandler.h"

namespace TileDataHandlerTest {
    TEST_CASE("TileDataHandler : unit test") {
        SECTION("GetInstance()") {
            const auto instance1 = TileDataHandler::GetInstance();
            const auto instance2 = TileDataHandler::GetInstance();
            CHECK(instance1 != nullptr);
            CHECK(instance1 == instance2);
        }
        SECTION("SetMaximumMemorySize()") {
            auto instance = TileDataHandler::GetInstance();
            instance->Clear();

            instance->SetMaximumMemorySize(1, DataSizeUnit::Byte);
            CHECK(instance != nullptr);
        }
        SECTION("HasSpace()") {
            auto instance = TileDataHandler::GetInstance();
            instance->Clear();

            instance->SetMaximumMemorySize(20, DataSizeUnit::Mebibyte);
            CHECK(instance->HasSpace(30, DataSizeUnit::Mebibyte) == false);
            CHECK(instance->HasSpace(10, DataSizeUnit::Mebibyte) == true);
        }
        SECTION("AddTileData()") {
            auto instance = TileDataHandler::GetInstance();
            instance->Clear();

            instance->AddTileData(nullptr, 0, 1, DataSizeUnit::Byte);
            CHECK(instance != nullptr);
        }
        SECTION("RemoveTileData()") {
            auto instance = TileDataHandler::GetInstance();
            instance->Clear();

            instance->RemoveTileData(0);
        }
        SECTION("RemoveOldestData()") {
            auto instance = TileDataHandler::GetInstance();
            instance->Clear();

            instance->RemoveOldestData();
        }
        SECTION("GetHandlingDataSize()") {
            auto instance = TileDataHandler::GetInstance();
            instance->Clear();

            CHECK(instance->GetHandlingDataSize(DataSizeUnit::Byte) == 0);

            instance->AddTileData(nullptr, 0, 4, DataSizeUnit::Byte);

            CHECK(instance->GetHandlingDataSize(DataSizeUnit::Byte) == 4);
        }
        SECTION("HasData()") {
            auto instance = TileDataHandler::GetInstance();
            instance->Clear();

            instance->AddTileData(nullptr, 0, 1, DataSizeUnit::Byte);
            CHECK(instance->HasData(0) == true);
            CHECK(instance->HasData(1) == false);
            instance->RemoveTileData(0);
            CHECK(instance->HasData(0) == false);
        }
        SECTION("GetTileData()") {
            auto instance = TileDataHandler::GetInstance();
            instance->Clear();

            std::shared_ptr<float[]> data{ new float[1]() };

            instance->AddTileData(data, 0, 1, DataSizeUnit::Byte);

            CHECK(data == instance->GetTileData(0));
            CHECK(nullptr == instance->GetTileData(1));
        }
        SECTION("Clear()") {
            auto instance = TileDataHandler::GetInstance();
            instance->Clear();

            std::shared_ptr<float[]> data{ new float[1]() };

            instance->AddTileData(data, 0, 1, DataSizeUnit::Byte);
            instance->SetMaximumMemorySize(10, DataSizeUnit::Mebibyte);

            CHECK(instance->HasSpace(100, DataSizeUnit::Mebibyte) == false);
            CHECK(instance->HasData(0) == true);

            instance->Clear();

            CHECK(instance->HasSpace(10, DataSizeUnit::Gibibyte) == true);
            CHECK(instance->HasData(0) == false);
        }
    }
    TEST_CASE("TileDataHandler : practical test") {
        auto instance = TileDataHandler::GetInstance();
        instance->Clear();

        instance->SetMaximumMemorySize(10, DataSizeUnit::Byte);

        constexpr int32_t dataSize1 = 4;
        constexpr int32_t dataSize2 = 4;
        constexpr int32_t dataSize3 = 4;

        const std::shared_ptr<float[]> data1{ new float[dataSize1]() };
        const std::shared_ptr<float[]> data2{ new float[dataSize2]() };
        const std::shared_ptr<float[]> data3{ new float[dataSize3]() };

        constexpr int32_t dataIndex1 = 1;
        constexpr int32_t dataIndex2 = 2;
        constexpr int32_t dataIndex3 = 3;

        if (instance->HasSpace(dataSize1, DataSizeUnit::Byte)) {
            if (!instance->HasData(dataIndex1)) {
                instance->AddTileData(data1, dataIndex1, dataSize1, DataSizeUnit::Byte);
            }
        }

        CHECK(instance->GetHandlingDataSize(DataSizeUnit::Byte) == 4);

        if (instance->HasSpace(dataSize2, DataSizeUnit::Byte)) {
            if (!instance->HasData(dataIndex2)) {
                instance->AddTileData(data2, dataIndex2, dataSize2, DataSizeUnit::Byte);
            }
        }

        CHECK(instance->GetHandlingDataSize(DataSizeUnit::Byte) == 8);

        CHECK(instance->HasSpace(dataSize3, DataSizeUnit::Byte) == false);

        CHECK(instance->HasData(dataIndex1) == true);
        CHECK(instance->HasData(dataIndex2) == true);
        CHECK(instance->HasData(dataIndex3) == false);

        instance->RemoveTileData(dataIndex1);

        CHECK(instance->GetHandlingDataSize(DataSizeUnit::Byte) == 4);

        if (instance->HasSpace(dataSize3, DataSizeUnit::Byte)) {
            if (!instance->HasData(dataIndex3)) {
                instance->AddTileData(data3, dataIndex3, dataSize3, DataSizeUnit::Byte);
            }
        }

        CHECK(instance->GetHandlingDataSize(DataSizeUnit::Byte) == 8);

        CHECK(instance->HasData(dataIndex1) == false);
        CHECK(instance->HasData(dataIndex2) == true);
        CHECK(instance->HasData(dataIndex3) == true);

        instance->RemoveOldestData();
        CHECK(instance->GetHandlingDataSize(DataSizeUnit::Byte) == 4);
        CHECK(instance->HasData(dataIndex1) == false);
        CHECK(instance->HasData(dataIndex2) == false);
        CHECK(instance->HasData(dataIndex3) == true);
    }
}