#include <catch2/catch.hpp>

#include "AcquisitionConfigReader.h"

namespace ConfigReaderTest {
    const auto testFolderPath = QString(TEST_DATA_FOLDR_PATH) + "/HTXProcessingServerTest";
    TEST_CASE("AcquisitionConfigReader : unit test") {
        SECTION("AcquisitionConfigReader()") {
            AcquisitionConfigReader reader;
            CHECK(&reader != nullptr);
        }
        SECTION("SetFilePath()") {
            AcquisitionConfigReader reader;
            reader.SetFilePath("");
            CHECK(&reader != nullptr);
        }
        SECTION("Read()") {
            SECTION("file doesn't exist") {
                const QString notFilePath = testFolderPath + "/noConfig.dat";
                AcquisitionConfigReader reader;
                reader.SetFilePath(notFilePath);

                CHECK(reader.Read() == false);
            }
            SECTION("empty file") {
                const QString emptyFilePath = testFolderPath + "/emptyConfig.dat";
                AcquisitionConfigReader reader;
                reader.SetFilePath(emptyFilePath);

                CHECK(reader.Read() == false);
            }
            SECTION("normal file") {
                const QString configFilePath = testFolderPath + "/config.dat";
                AcquisitionConfigReader reader;
                reader.SetFilePath(configFilePath);

                CHECK(reader.Read() == true);
            }
        }
        SECTION("GetAcquisitionConfig()") {
            SECTION("full parameter included") {
                const QString configFilePath = testFolderPath + "/config.dat";
                AcquisitionConfigReader reader;
                reader.SetFilePath(configFilePath);
                reader.Read();

                const auto resultAcquisitionConfig = reader.GetAcquisitionConfig();

                const auto& jobInfo = resultAcquisitionConfig.GetJobInfo();
                const auto& acquisitionCount = resultAcquisitionConfig.GetAcquisitionCount();
                const auto& acquisitionSetting = resultAcquisitionConfig.GetAcquisitionSetting();
                const auto& acquisitionPosition = resultAcquisitionConfig.GetAcquisitionPosition();
                const auto& imageInfo = resultAcquisitionConfig.GetImageInfo();
                const auto& deviceInfo = resultAcquisitionConfig.GetDeviceInfo();
                const auto& tileInfo = resultAcquisitionConfig.GetTileInfo();

                CHECK(resultAcquisitionConfig.IsJobInfoValid() == true);
                CHECK(resultAcquisitionConfig.IsAcquisitionCountValid() == true);
                CHECK(resultAcquisitionConfig.IsAcquisitionSettingValid() == true);
                CHECK(resultAcquisitionConfig.IsAcquisitionPositionValid() == true);
                CHECK(resultAcquisitionConfig.IsImageInfoValid() == true);
                CHECK(resultAcquisitionConfig.IsDeviceInfoValid() == true);
                CHECK(resultAcquisitionConfig.IsTileInfoValid() == true);

                CHECK(jobInfo.title == "Title");
                CHECK(jobInfo.userID == "UserID");

                CHECK(acquisitionCount.ht3D == 1);
                CHECK(acquisitionCount.ht2D == 2);
                CHECK(acquisitionCount.fl3DCH0 == 3);
                CHECK(acquisitionCount.fl3DCH1 == 4);
                CHECK(acquisitionCount.fl3DCH2 == 5);
                CHECK(acquisitionCount.fl2DCH0 == 6);
                CHECK(acquisitionCount.fl2DCH1 == 7);
                CHECK(acquisitionCount.fl2DCH2 == 8);
                CHECK(acquisitionCount.bf == 9);
                CHECK(acquisitionCount.ht3DZ == 10);
                CHECK(acquisitionCount.fl3DZ == 11);
                CHECK(acquisitionCount.bfChannel == 12);

                CHECK(acquisitionSetting.htZStepLengthMicrometer == 13);
                CHECK(acquisitionSetting.flZStepLengthMicrometer == 14);
                CHECK(acquisitionSetting.flCH0ExcitationWaveLengthMicrometer == 15);
                CHECK(acquisitionSetting.flCH1ExcitationWaveLengthMicrometer == 16);
                CHECK(acquisitionSetting.flCH2ExcitationWaveLengthMicrometer == 17);
                CHECK(acquisitionSetting.flCH0EmissionWaveLengthMicrometer == 18);
                CHECK(acquisitionSetting.flCH1EmissionWaveLengthMicrometer == 19);
                CHECK(acquisitionSetting.flCH2EmissionWaveLengthMicrometer == 20);
                CHECK(acquisitionSetting.flCH0Color.r == 1);
                CHECK(acquisitionSetting.flCH0Color.g == 2);
                CHECK(acquisitionSetting.flCH0Color.b == 3);
                CHECK(acquisitionSetting.flCH1Color.r == 4);
                CHECK(acquisitionSetting.flCH1Color.g == 5);
                CHECK(acquisitionSetting.flCH1Color.b == 6);
                CHECK(acquisitionSetting.flCH2Color.r == 7);
                CHECK(acquisitionSetting.flCH2Color.g == 8);
                CHECK(acquisitionSetting.flCH2Color.b == 9);
                CHECK(acquisitionSetting.flCH0Intensity == 11);
                CHECK(acquisitionSetting.flCH1Intensity == 22);
                CHECK(acquisitionSetting.flCH2Intensity == 33);
                CHECK(acquisitionSetting.flCH0ExposureTimeMillisecond == 44);
                CHECK(acquisitionSetting.flCH1ExposureTimeMillisecond == 55);
                CHECK(acquisitionSetting.flCH2ExposureTimeMillisecond == 66);

                CHECK(acquisitionPosition.wellIndex == 21);
                CHECK(acquisitionPosition.positionXMillimeter == 22);
                CHECK(acquisitionPosition.positionYMillimeter == 23);
                CHECK(acquisitionPosition.positionZMillimeter == 24);
                CHECK(acquisitionPosition.positionCMillimeter == 25);
                CHECK(acquisitionPosition.flAcquisitionZOffsetMicrometer == 26);

                CHECK(imageInfo.roiSizeXPixels == 27);
                CHECK(imageInfo.roiSizeYPixels == 28);
                CHECK(imageInfo.roiOffsetXPixels == 29);
                CHECK(imageInfo.roiOffsetYPixels == 30);

                CHECK(tileInfo.tileNumberX == 31);
                CHECK(tileInfo.tileNumberY == 32);

                CHECK(deviceInfo.pixelSizeMicrometer == 33);
                CHECK(deviceInfo.magnification == 34);
                CHECK(deviceInfo.objectiveNA == 35);
                CHECK(deviceInfo.condenserNA == 36);
                CHECK(deviceInfo.mediumRI == 37);
                CHECK(deviceInfo.deviceSerial == "38");
                CHECK(deviceInfo.deviceHost == "39");
                CHECK(deviceInfo.deviceSoftwareVersion == "40");
            }

            SECTION("ImageInfo, TileInfo Excluded") {
                const QString configFilePath = testFolderPath + "/imageInfoTileInfoExcludedConfig.dat";
                AcquisitionConfigReader reader;
                reader.SetFilePath(configFilePath);
                reader.Read();

                const auto resultAcquisitionConfig = reader.GetAcquisitionConfig();

                CHECK(resultAcquisitionConfig.IsJobInfoValid() == true);
                CHECK(resultAcquisitionConfig.IsAcquisitionCountValid() == true);
                CHECK(resultAcquisitionConfig.IsAcquisitionSettingValid() == true);
                CHECK(resultAcquisitionConfig.IsAcquisitionPositionValid() == true);
                CHECK(resultAcquisitionConfig.IsImageInfoValid() == false);
                CHECK(resultAcquisitionConfig.IsDeviceInfoValid() == true);
                CHECK(resultAcquisitionConfig.IsTileInfoValid() == false);

            }

            SECTION("rgb Excluded") {
                const QString configFilePath = testFolderPath + "/imageInfoTileInfoExcludedConfig.dat";
                AcquisitionConfigReader reader;
                reader.SetFilePath(configFilePath);
                reader.Read();

                const auto resultAcquisitionConfig = reader.GetAcquisitionConfig();

                CHECK(resultAcquisitionConfig.IsJobInfoValid() == true);
                CHECK(resultAcquisitionConfig.IsAcquisitionCountValid() == true);
                CHECK(resultAcquisitionConfig.IsAcquisitionSettingValid() == true);
                CHECK(resultAcquisitionConfig.IsAcquisitionPositionValid() == true);
                CHECK(resultAcquisitionConfig.IsImageInfoValid() == false);
                CHECK(resultAcquisitionConfig.IsDeviceInfoValid() == true);
                CHECK(resultAcquisitionConfig.IsTileInfoValid() == false);

                CHECK(resultAcquisitionConfig.GetAcquisitionSetting().flCH0Color.r == 0);
                CHECK(resultAcquisitionConfig.GetAcquisitionSetting().flCH0Color.g == 0);
                CHECK(resultAcquisitionConfig.GetAcquisitionSetting().flCH0Color.b == 0);
                CHECK(resultAcquisitionConfig.GetAcquisitionSetting().flCH1Color.r == 0);
                CHECK(resultAcquisitionConfig.GetAcquisitionSetting().flCH1Color.g == 0);
                CHECK(resultAcquisitionConfig.GetAcquisitionSetting().flCH1Color.b == 0);
                CHECK(resultAcquisitionConfig.GetAcquisitionSetting().flCH2Color.r == 0);
                CHECK(resultAcquisitionConfig.GetAcquisitionSetting().flCH2Color.g == 0);
                CHECK(resultAcquisitionConfig.GetAcquisitionSetting().flCH2Color.b == 0);
            }
        }
    }


    TEST_CASE("ConfigReader : practical test") {
        const QString configFilePath = testFolderPath + "/config.dat";
        AcquisitionConfigReader reader;
        reader.SetFilePath(configFilePath);
        reader.Read();

        const auto resultAcquisitionConfig = reader.GetAcquisitionConfig();

        const auto& jobInfo = resultAcquisitionConfig.GetJobInfo();
        const auto& acquisitionCount = resultAcquisitionConfig.GetAcquisitionCount();
        const auto& acquisitionSetting = resultAcquisitionConfig.GetAcquisitionSetting();
        const auto& acquisitionPosition = resultAcquisitionConfig.GetAcquisitionPosition();
        //TODO : Add AcquisitionSize test
        const auto& imageInfo = resultAcquisitionConfig.GetImageInfo();
        const auto& deviceInfo = resultAcquisitionConfig.GetDeviceInfo();
        const auto& tileInfo = resultAcquisitionConfig.GetTileInfo();

        CHECK(resultAcquisitionConfig.IsJobInfoValid() == true);
        CHECK(resultAcquisitionConfig.IsAcquisitionCountValid() == true);
        CHECK(resultAcquisitionConfig.IsAcquisitionSettingValid() == true);
        CHECK(resultAcquisitionConfig.IsAcquisitionPositionValid() == true);
        CHECK(resultAcquisitionConfig.IsImageInfoValid() == true);
        CHECK(resultAcquisitionConfig.IsDeviceInfoValid() == true);
        CHECK(resultAcquisitionConfig.IsTileInfoValid() == true);

        CHECK(jobInfo.title == "Title");
        CHECK(jobInfo.userID == "UserID");

        CHECK(acquisitionCount.ht3D == 1);
        CHECK(acquisitionCount.ht2D == 2);
        CHECK(acquisitionCount.fl3DCH0 == 3);
        CHECK(acquisitionCount.fl3DCH1 == 4);
        CHECK(acquisitionCount.fl3DCH2 == 5);
        CHECK(acquisitionCount.fl2DCH0 == 6);
        CHECK(acquisitionCount.fl2DCH1 == 7);
        CHECK(acquisitionCount.fl2DCH2 == 8);
        CHECK(acquisitionCount.bf == 9);
        CHECK(acquisitionCount.ht3DZ == 10);
        CHECK(acquisitionCount.fl3DZ == 11);
        CHECK(acquisitionCount.bfChannel == 12);

        CHECK(acquisitionSetting.htZStepLengthMicrometer == 13);
        CHECK(acquisitionSetting.flZStepLengthMicrometer == 14);
        CHECK(acquisitionSetting.flCH0ExcitationWaveLengthMicrometer == 15);
        CHECK(acquisitionSetting.flCH1ExcitationWaveLengthMicrometer == 16);
        CHECK(acquisitionSetting.flCH2ExcitationWaveLengthMicrometer == 17);
        CHECK(acquisitionSetting.flCH0EmissionWaveLengthMicrometer == 18);
        CHECK(acquisitionSetting.flCH1EmissionWaveLengthMicrometer == 19);
        CHECK(acquisitionSetting.flCH2EmissionWaveLengthMicrometer == 20);
        CHECK(acquisitionSetting.flCH0Color.r == 1);
        CHECK(acquisitionSetting.flCH0Color.g == 2);
        CHECK(acquisitionSetting.flCH0Color.b == 3);
        CHECK(acquisitionSetting.flCH1Color.r == 4);
        CHECK(acquisitionSetting.flCH1Color.g == 5);
        CHECK(acquisitionSetting.flCH1Color.b == 6);
        CHECK(acquisitionSetting.flCH2Color.r == 7);
        CHECK(acquisitionSetting.flCH2Color.g == 8);
        CHECK(acquisitionSetting.flCH2Color.b == 9);
        CHECK(acquisitionSetting.flCH0Intensity == 11);
        CHECK(acquisitionSetting.flCH1Intensity == 22);
        CHECK(acquisitionSetting.flCH2Intensity == 33);
        CHECK(acquisitionSetting.flCH0ExposureTimeMillisecond == 44);
        CHECK(acquisitionSetting.flCH1ExposureTimeMillisecond == 55);
        CHECK(acquisitionSetting.flCH2ExposureTimeMillisecond == 66);

        CHECK(acquisitionPosition.wellIndex == 21);
        CHECK(acquisitionPosition.positionXMillimeter == 22);
        CHECK(acquisitionPosition.positionYMillimeter == 23);
        CHECK(acquisitionPosition.positionZMillimeter == 24);
        CHECK(acquisitionPosition.positionCMillimeter == 25);
        CHECK(acquisitionPosition.flAcquisitionZOffsetMicrometer == 26);

        CHECK(imageInfo.roiSizeXPixels == 27);
        CHECK(imageInfo.roiSizeYPixels == 28);
        CHECK(imageInfo.roiOffsetXPixels == 29);
        CHECK(imageInfo.roiOffsetYPixels == 30);

        CHECK(tileInfo.tileNumberX == 31);
        CHECK(tileInfo.tileNumberY == 32);

        CHECK(deviceInfo.pixelSizeMicrometer == 33);
        CHECK(deviceInfo.magnification == 34);
        CHECK(deviceInfo.objectiveNA == 35);
        CHECK(deviceInfo.condenserNA == 36);
        CHECK(deviceInfo.mediumRI == 37);
        CHECK(deviceInfo.deviceSerial == "38");
        CHECK(deviceInfo.deviceHost == "39");
        CHECK(deviceInfo.deviceSoftwareVersion == "40");
    }
}