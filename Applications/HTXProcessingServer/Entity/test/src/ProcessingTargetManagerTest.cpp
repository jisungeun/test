#include <catch2/catch.hpp>

#include <chrono>
#include <iostream>

#include "ProcessingTargetManager.h"

namespace ProcessingTargetManagerTest {
    using namespace HTXProcessingServer::Entity;

    TEST_CASE("ProcessingTargetManager : unit test") {
        SECTION("Singleton") {
            SECTION("CHECK Memory Address") {
                ProcessingTargetManager::GetInstance()->Clear();

                void* memoryAddress1 = nullptr;
                {memoryAddress1 = ProcessingTargetManager::GetInstance().get(); }

                void* memoryAddress2 = nullptr;
                {memoryAddress2 = ProcessingTargetManager::GetInstance().get(); }

                CHECK(memoryAddress1 != nullptr);
                CHECK(memoryAddress2 != nullptr);
                CHECK(memoryAddress1 == memoryAddress2);

                ProcessingTargetManager::GetInstance()->Clear();
            }

            SECTION("Check member variables") {
                ProcessingTargetManager::GetInstance()->Clear();
                {
                    auto manager = ProcessingTargetManager::GetInstance();
                    manager->Append(ProcessingTarget{ 1,"2",3,ProcessingStatus::Failed,4,5 });

                    CHECK(manager->GetProcessingTargetsByPriorityOrder().size() == 1);
                }

                {
                    auto manager = ProcessingTargetManager::GetInstance();
                    const auto targets = manager->GetProcessingTargetsByPriorityOrder();
                    CHECK(targets.size() == 1);

                    const auto target = targets.at(0);

                    CHECK(target.GetTargetID() == 1);
                    CHECK(target.GetPath() == "2");
                    CHECK(target.GetPriorityIndex() == 3);
                    CHECK(target.GetStatus() == ProcessingStatus::Failed);
                    CHECK(target.GetRemainingTime() == 4);
                    CHECK(target.GetTakenTime() == 5);
                }

                ProcessingTargetManager::GetInstance()->Clear();
            }
        }

        SECTION("Clear()") {
            ProcessingTargetManager::GetInstance()->Clear();

            auto manager = ProcessingTargetManager::GetInstance();
            manager->Append(ProcessingTarget{ 1,"2",3,ProcessingStatus::Failed,4,5 });

            const auto targets = manager->GetProcessingTargetsByPriorityOrder();

            REQUIRE(targets.size() == 1);

            manager->Clear();

            const auto targetsAfterClear = manager->GetProcessingTargetsByPriorityOrder();
            CHECK(targetsAfterClear.empty());

            ProcessingTargetManager::GetInstance()->Clear();
        }

        SECTION("GetProcessingTargetsByPriorityOrder(id)") {
            ProcessingTargetManager::GetInstance()->Clear();

            SECTION("Append targets in priority order") {
                auto manager = ProcessingTargetManager::GetInstance();
                manager->Append(ProcessingTarget{ 1,"path",-1,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 2,"path",0,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 3,"path",1,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 4,"path",2,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 5,"path",3,ProcessingStatus::Waiting,0,0 });

                const auto processingTargets = manager->GetProcessingTargetsByPriorityOrder();

                CHECK(processingTargets.size() == 5);
                CHECK(processingTargets.at(0).GetTargetID() == 1);
                CHECK(processingTargets.at(1).GetTargetID() == 2);
                CHECK(processingTargets.at(2).GetTargetID() == 3);
                CHECK(processingTargets.at(3).GetTargetID() == 4);
                CHECK(processingTargets.at(4).GetTargetID() == 5);
            }

            SECTION("Append targets in priority reverse-order") {
                auto manager = ProcessingTargetManager::GetInstance();
                manager->Append(ProcessingTarget{ 1,"path",3,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 2,"path",2,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 3,"path",1,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 4,"path",0,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 5,"path",-1,ProcessingStatus::Waiting,0,0 });

                const auto processingTargets = manager->GetProcessingTargetsByPriorityOrder();

                CHECK(processingTargets.size() == 5);
                CHECK(processingTargets.at(0).GetTargetID() == 5);
                CHECK(processingTargets.at(1).GetTargetID() == 4);
                CHECK(processingTargets.at(2).GetTargetID() == 3);
                CHECK(processingTargets.at(3).GetTargetID() == 2);
                CHECK(processingTargets.at(4).GetTargetID() == 1);
            }

            SECTION("Append targets in priority random-order") {
                auto manager = ProcessingTargetManager::GetInstance();
                manager->Append(ProcessingTarget{ 1,"path",-20,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 2,"path",1,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 3,"path",3,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 4,"path",-7,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 5,"path",100,ProcessingStatus::Waiting,0,0 });

                const auto processingTargets = manager->GetProcessingTargetsByPriorityOrder();

                CHECK(processingTargets.size() == 5);
                CHECK(processingTargets.at(0).GetTargetID() == 1);
                CHECK(processingTargets.at(1).GetTargetID() == 4);
                CHECK(processingTargets.at(2).GetTargetID() == 2);
                CHECK(processingTargets.at(3).GetTargetID() == 3);
                CHECK(processingTargets.at(4).GetTargetID() == 5);
            }

            ProcessingTargetManager::GetInstance()->Clear();
        }

        SECTION("ChangeToTopPriority(id)") {
            ProcessingTargetManager::GetInstance()->Clear();

            auto manager = ProcessingTargetManager::GetInstance();
            manager->Append(ProcessingTarget{ 1,"path",-1,ProcessingStatus::Waiting,0,0 });
            manager->Append(ProcessingTarget{ 2,"path",0,ProcessingStatus::Waiting,0,0 });
            manager->Append(ProcessingTarget{ 3,"path",1,ProcessingStatus::Waiting,0,0 });
            manager->Append(ProcessingTarget{ 4,"path",2,ProcessingStatus::Waiting,0,0 });
            manager->Append(ProcessingTarget{ 5,"path",3,ProcessingStatus::Waiting,0,0 });

            constexpr auto changingTargetID = 3;
            manager->ChangeToTopPriority(changingTargetID);

            const auto processingTargets = manager->GetProcessingTargetsByPriorityOrder();

            CHECK(processingTargets.size() == 5);
            CHECK(processingTargets.at(0).GetTargetID() == changingTargetID);
            CHECK(processingTargets.at(1).GetTargetID() == 1);
            CHECK(processingTargets.at(2).GetTargetID() == 2);
            CHECK(processingTargets.at(3).GetTargetID() == 4);
            CHECK(processingTargets.at(4).GetTargetID() == 5);

            ProcessingTargetManager::GetInstance()->Clear();
        }

        SECTION("ChangeToTopPriority(ids)") {
            ProcessingTargetManager::GetInstance()->Clear();

            SECTION("serial ids") {
                auto manager = ProcessingTargetManager::GetInstance();
                manager->Append(ProcessingTarget{ 1,"path",-1,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 2,"path",0,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 3,"path",1,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 4,"path",2,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 5,"path",3,ProcessingStatus::Waiting,0,0 });

                std::vector<TargetID> serialIDs;
                serialIDs.push_back(3);
                serialIDs.push_back(4);
                manager->ChangeToTopPriority(serialIDs);

                const auto processingTargets = manager->GetProcessingTargetsByPriorityOrder();

                CHECK(processingTargets.size() == 5);
                CHECK(processingTargets.at(0).GetTargetID() == 3);
                CHECK(processingTargets.at(1).GetTargetID() == 4);
                CHECK(processingTargets.at(2).GetTargetID() == 1);
                CHECK(processingTargets.at(3).GetTargetID() == 2);
                CHECK(processingTargets.at(4).GetTargetID() == 5);
            }

            SECTION("apart ids") {
                auto manager = ProcessingTargetManager::GetInstance();
                manager->Append(ProcessingTarget{ 1,"path",-1,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 2,"path",0,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 3,"path",1,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 4,"path",2,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 5,"path",3,ProcessingStatus::Waiting,0,0 });

                std::vector<TargetID> apartIDs;
                apartIDs.push_back(5);
                apartIDs.push_back(2);
                manager->ChangeToTopPriority(apartIDs);

                const auto processingTargets = manager->GetProcessingTargetsByPriorityOrder();

                CHECK(processingTargets.size() == 5);
                CHECK(processingTargets.at(0).GetTargetID() == 2);
                CHECK(processingTargets.at(1).GetTargetID() == 5);
                CHECK(processingTargets.at(2).GetTargetID() == 1);
                CHECK(processingTargets.at(3).GetTargetID() == 3);
                CHECK(processingTargets.at(4).GetTargetID() == 4);
            }

            ProcessingTargetManager::GetInstance()->Clear();
        }

        SECTION("ChangeToBottomPriority(id)") {
            ProcessingTargetManager::GetInstance()->Clear();

            auto manager = ProcessingTargetManager::GetInstance();
            manager->Append(ProcessingTarget{ 1,"path",-1,ProcessingStatus::Waiting,0,0 });
            manager->Append(ProcessingTarget{ 2,"path",0,ProcessingStatus::Waiting,0,0 });
            manager->Append(ProcessingTarget{ 3,"path",1,ProcessingStatus::Waiting,0,0 });
            manager->Append(ProcessingTarget{ 4,"path",2,ProcessingStatus::Waiting,0,0 });
            manager->Append(ProcessingTarget{ 5,"path",3,ProcessingStatus::Waiting,0,0 });

            constexpr auto changingTargetID = 3;
            manager->ChangeToBottomPriority(changingTargetID);

            const auto processingTargets = manager->GetProcessingTargetsByPriorityOrder();

            CHECK(processingTargets.size() == 5);
            CHECK(processingTargets.at(0).GetTargetID() == 1);
            CHECK(processingTargets.at(1).GetTargetID() == 2);
            CHECK(processingTargets.at(2).GetTargetID() == 4);
            CHECK(processingTargets.at(3).GetTargetID() == 5);
            CHECK(processingTargets.at(4).GetTargetID() == changingTargetID);

            ProcessingTargetManager::GetInstance()->Clear();
        }

        SECTION("ChangeToBottomPriority(ids)") {
            ProcessingTargetManager::GetInstance()->Clear();

            SECTION("serial ids") {
                auto manager = ProcessingTargetManager::GetInstance();
                manager->Append(ProcessingTarget{ 1,"path",-1,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 2,"path",0,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 3,"path",1,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 4,"path",2,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 5,"path",3,ProcessingStatus::Waiting,0,0 });

                std::vector<TargetID> serialIDs;
                serialIDs.push_back(3);
                serialIDs.push_back(4);
                manager->ChangeToBottomPriority(serialIDs);

                const auto processingTargets = manager->GetProcessingTargetsByPriorityOrder();

                CHECK(processingTargets.size() == 5);
                CHECK(processingTargets.at(0).GetTargetID() == 1);
                CHECK(processingTargets.at(1).GetTargetID() == 2);
                CHECK(processingTargets.at(2).GetTargetID() == 5);
                CHECK(processingTargets.at(3).GetTargetID() == 3);
                CHECK(processingTargets.at(4).GetTargetID() == 4);
            }

            SECTION("apart ids") {
                auto manager = ProcessingTargetManager::GetInstance();
                manager->Append(ProcessingTarget{ 1,"path",-1,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 2,"path",0,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 3,"path",1,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 4,"path",2,ProcessingStatus::Waiting,0,0 });
                manager->Append(ProcessingTarget{ 5,"path",3,ProcessingStatus::Waiting,0,0 });

                std::vector<TargetID> apartIDs;
                apartIDs.push_back(5);
                apartIDs.push_back(2);
                manager->ChangeToBottomPriority(apartIDs);

                const auto processingTargets = manager->GetProcessingTargetsByPriorityOrder();

                CHECK(processingTargets.size() == 5);
                CHECK(processingTargets.at(0).GetTargetID() == 1);
                CHECK(processingTargets.at(1).GetTargetID() == 3);
                CHECK(processingTargets.at(2).GetTargetID() == 4);
                CHECK(processingTargets.at(3).GetTargetID() == 2);
                CHECK(processingTargets.at(4).GetTargetID() == 5);
            }

            ProcessingTargetManager::GetInstance()->Clear();
        }
    }

    TEST_CASE("ProcessingTargetManager : practical test") {
        ProcessingTargetManager::GetInstance()->Clear();

        constexpr auto targetCount = 5000;
        constexpr auto changingTargetCount = 2500;

        auto manager = ProcessingTargetManager::GetInstance();
        for (auto index = 0; index < targetCount; ++index) {
            manager->Append(ProcessingTarget{ index,"path",index,ProcessingStatus::Waiting,0,0 });
        }

        std::vector<TargetID> apartIDs;
        for (auto index = 0; index < changingTargetCount; ++index) {
            apartIDs.push_back(index * 3);
        }

        {
            const auto changingStart = std::chrono::system_clock::now();
            manager->ChangeToBottomPriority(apartIDs);
            const auto changingEnd = std::chrono::system_clock::now();

            const auto changingTime = changingEnd - changingStart;
            //std::cout << "changingTime : " << static_cast<float>(std::chrono::duration_cast<std::chrono::milliseconds>(changingTime).count()) / 1000 << " ms" << std::endl;
            CHECK(std::chrono::duration_cast<std::chrono::milliseconds>(changingTime).count() < 100);
        }

        {
            const auto gettingStart = std::chrono::system_clock::now();
            const auto processingTargets = manager->GetProcessingTargetsByPriorityOrder();
            const auto gettingEnd = std::chrono::system_clock::now();

            const auto gettingTime = gettingEnd - gettingStart;
            //std::cout << "gettingTime : " << static_cast<float>(std::chrono::duration_cast<std::chrono::milliseconds>(gettingTime).count()) / 1000 << " ms" << std::endl;
            CHECK(std::chrono::duration_cast<std::chrono::milliseconds>(gettingTime).count() < 100);
        }
        ProcessingTargetManager::GetInstance()->Clear();
    }

}