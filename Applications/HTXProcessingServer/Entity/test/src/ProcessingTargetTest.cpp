#include <catch2/catch.hpp>

#include "ProcessingTarget.h"

namespace ProcessingTargetTest {
    using namespace HTXProcessingServer::Entity;

    TEST_CASE("ProcessingTargetTest : unit test") {
        SECTION("ProcessingTarget()") {
            ProcessingTarget target;
            CHECK(&target != nullptr);
        }
        SECTION("ProcessingTarget(id, path, index, status, remainingTime, takenTime)") {
            ProcessingTarget target(1, "2", 3, ProcessingStatus::Processing, 4, 5);
            CHECK(target.GetTargetID() == 1);
            CHECK(target.GetPath() == "2");
            CHECK(target.GetPriorityIndex() == 3);
            CHECK(target.GetStatus() == ProcessingStatus::Processing);
            CHECK(target.GetRemainingTime() == 4);
            CHECK(target.GetTakenTime() == 5);
        }
        SECTION("ProcessingTarget(other)") {
            ProcessingTarget srcTarget(1, "2", 3, ProcessingStatus::Processing, 4, 5);
            ProcessingTarget destTarget(srcTarget);
            CHECK(destTarget.GetTargetID() == 1);
            CHECK(destTarget.GetPath() == "2");
            CHECK(destTarget.GetPriorityIndex() == 3);
            CHECK(destTarget.GetStatus() == ProcessingStatus::Processing);
            CHECK(destTarget.GetRemainingTime() == 4);
            CHECK(destTarget.GetTakenTime() == 5);
        }
        SECTION("operator=()") {
            ProcessingTarget srcTarget(1, "2", 3, ProcessingStatus::Processing, 4, 5);
            ProcessingTarget destTarget;
            destTarget = srcTarget;

            CHECK(destTarget.GetTargetID() == 1);
            CHECK(destTarget.GetPath() == "2");
            CHECK(destTarget.GetPriorityIndex() == 3);
            CHECK(destTarget.GetStatus() == ProcessingStatus::Processing);
            CHECK(destTarget.GetRemainingTime() == 4);
            CHECK(destTarget.GetTakenTime() == 5);
        }
        SECTION("Get*()/Set*()") {
            ProcessingTarget target;
            CHECK(target.GetTargetID() == 0);
            CHECK(target.GetPath().empty());
            CHECK(target.GetPriorityIndex() == 0);
            CHECK(target.GetStatus() == ProcessingStatus::Waiting);
            CHECK(target.GetRemainingTime() == 0);
            CHECK(target.GetTakenTime() == 0);

            target.SetTargetID(1);
            target.SetPath("2");
            target.SetPriorityIndex(3);
            target.SetStatus(ProcessingStatus::Completed);
            target.SetRemainingTime(4);
            target.SetTakenTime(5);

            CHECK(target.GetTargetID() == 1);
            CHECK(target.GetPath() == "2");
            CHECK(target.GetPriorityIndex() == 3);
            CHECK(target.GetStatus() == ProcessingStatus::Completed);
            CHECK(target.GetRemainingTime() == 4);
            CHECK(target.GetTakenTime() == 5);
        }
    }
}
