#include <catch2/catch.hpp>

#include "ProcessingTargetRepo.h"

namespace ProcessingTargetTest {
    using namespace HTXProcessingServer::Entity;

    TEST_CASE("ProcessingTargetRepoTest : unit test") {
        SECTION("ProcessingTargetRepo(other)") {
            ProcessingTargetRepo srcRepo;
            srcRepo.Add(ProcessingTarget{ 1,"2",3,ProcessingStatus::Processing,4,5 });

            ProcessingTargetRepo destRepo{ srcRepo };

            CHECK(destRepo.GetCount() == 1);
            CHECK(!destRepo.GetTargets().empty());
        }

        SECTION("operator=()") {
            ProcessingTargetRepo srcRepo;
            srcRepo.Add(ProcessingTarget{ 1,"2",3,ProcessingStatus::Processing,4,5 });

            ProcessingTargetRepo destRepo;
            destRepo = srcRepo;

            CHECK(destRepo.GetCount() == 1);
            CHECK(!destRepo.GetTargets().empty());
        }
    }

    TEST_CASE("ProcessingTargetRepo : practical test") {
        ProcessingTargetRepo repo;
        const ProcessingTarget target1{ 1,"2",3,ProcessingStatus::Processing,4,5 };
        const ProcessingTarget target2{ 6,"7",8,ProcessingStatus::Waiting,9,10 };
        const ProcessingTarget target3{ 11,"12",13,ProcessingStatus::Failed,14,15 };

        repo.Add(target1);
        repo.Add(target2);
        repo.Add(target3);

        CHECK(repo.GetCount() == 3);

        const auto targets = repo.GetTargets();
        CHECK(targets.size() == 3);

        CHECK(targets.at(0).GetTargetID() == 1);
        CHECK(targets.at(1).GetTargetID() == 6);
        CHECK(targets.at(2).GetTargetID() == 11);

        repo.Clear();
        CHECK(repo.GetCount() == 0);
        CHECK(repo.GetTargets().empty());
    }
}
