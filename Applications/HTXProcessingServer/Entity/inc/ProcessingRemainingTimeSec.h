#pragma once
#include <cstdint>

namespace HTXProcessingServer::Entity {
    using ProcessingRemainingTimeSec = int64_t;
}
