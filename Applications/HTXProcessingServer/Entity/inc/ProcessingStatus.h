#pragma once

namespace HTXProcessingServer::Entity {
    enum class ProcessingStatus { Waiting, Processing, Completed, Failed };
}