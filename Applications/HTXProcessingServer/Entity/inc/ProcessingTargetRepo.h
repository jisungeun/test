#pragma once

#include <memory>
#include <vector>

#include "HTXProcessingServerEntityExport.h"

#include "ProcessingTarget.h"

namespace HTXProcessingServer::Entity {
    class HTXProcessingServerEntity_API ProcessingTargetRepo {
    public:
        ProcessingTargetRepo();
        ProcessingTargetRepo(const ProcessingTargetRepo& other);
        ~ProcessingTargetRepo();

        auto operator=(const ProcessingTargetRepo& other)->ProcessingTargetRepo&;

        auto Add(const ProcessingTarget& target)->void;
        auto GetCount()const->size_t;
        auto GetTargets()const->std::vector<ProcessingTarget>;

        auto Clear()->void;
    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}