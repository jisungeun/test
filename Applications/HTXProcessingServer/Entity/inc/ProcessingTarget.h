#pragma once

#include <memory>

#include "HTXProcessingServerEntityExport.h"

#include "TargetID.h"
#include "ProcessingPath.h"
#include "ProcessingPriorityIndex.h"
#include "ProcessingStatus.h"
#include "ProcessingRemainingTimeSec.h"
#include "ProcessingTakenTimeSec.h"

namespace HTXProcessingServer::Entity {
    class HTXProcessingServerEntity_API ProcessingTarget {
    public:
        ProcessingTarget();
        ProcessingTarget(const ProcessingTarget& other);
        ProcessingTarget(const TargetID& id, const ProcessingPath& path, const ProcessingPriorityIndex& index, 
            const ProcessingStatus& status, const ProcessingRemainingTimeSec& remainingTime, const ProcessingTakenTimeSec& takenTime);
        ~ProcessingTarget();

        auto operator=(const ProcessingTarget& other)->ProcessingTarget&;

        auto SetTargetID(const TargetID& id)->void;
        auto SetPath(const ProcessingPath& path)->void;
        auto SetPriorityIndex(const ProcessingPriorityIndex& index)->void;
        auto SetStatus(const ProcessingStatus& status)->void;
        auto SetRemainingTime(const ProcessingRemainingTimeSec& time)->void;
        auto SetTakenTime(const ProcessingTakenTimeSec& time)->void;

        auto GetTargetID()const->TargetID;
        auto GetPath()const->ProcessingPath;
        auto GetPriorityIndex()const->ProcessingPriorityIndex;
        auto GetStatus()const->ProcessingStatus;
        auto GetRemainingTime()const->ProcessingRemainingTimeSec;
        auto GetTakenTime()const->ProcessingTakenTimeSec;

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}