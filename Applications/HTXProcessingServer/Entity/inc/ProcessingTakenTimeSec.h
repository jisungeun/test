#pragma once
#include <cstdint>

namespace HTXProcessingServer::Entity {
    using ProcessingTakenTimeSec = int64_t;
}