#pragma once

#include <memory>
#include <vector>

#include "HTXProcessingServerEntityExport.h"
#include "ProcessingTarget.h"

namespace HTXProcessingServer::Entity {
    class HTXProcessingServerEntity_API ProcessingTargetManager {
    public:
        using Pointer = std::shared_ptr<ProcessingTargetManager>;
        ~ProcessingTargetManager();

        static auto GetInstance()->Pointer;

        auto Append(const ProcessingTarget& target)->void;
        auto Clear()->void;

        auto ChangeToTopPriority(const TargetID& id)->void;
        auto ChangeToTopPriority(const std::vector<TargetID>& ids)->void;

        auto ChangeToBottomPriority(const TargetID& id)->void;
        auto ChangeToBottomPriority(const std::vector<TargetID>& ids)->void;

        auto GetProcessingTargetsByPriorityOrder()const->std::vector<ProcessingTarget>;

    protected:
        ProcessingTargetManager();

    private:
        class Impl;
        std::unique_ptr<Impl> d;
    };
}
