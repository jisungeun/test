project(HTXProcessingServerEntity)

#Header files for external use
set(INTERFACE_HEADERS
	inc/ProcessingPath.h
	inc/ProcessingPriorityIndex.h
	inc/ProcessingRemainingTimeSec.h
	inc/ProcessingStatus.h
	inc/ProcessingTakenTimeSec.h
	inc/ProcessingTarget.h
	inc/ProcessingTargetManager.h
	inc/ProcessingTargetRepo.h
	inc/TargetID.h
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES
	src/ProcessingPath.cpp
	src/ProcessingPriorityIndex.cpp
	src/ProcessingRemainingTimeSec.cpp
	src/ProcessingStatus.cpp
	src/ProcessingTakenTimeSec.cpp
	src/ProcessingTarget.cpp
	src/ProcessingTargetManager.cpp
	src/ProcessingTargetRepo.cpp
	src/TargetID.cpp
)

add_library(${PROJECT_NAME} SHARED
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

add_library(HTXProcessingServer::Entity ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<INSTALL_INTERFACE:inc>
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_libraries(${PROJECT_NAME}
	PUBLIC
)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PUBLIC
		/wd4251
    PRIVATE
        /W4 /WX
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/HTXProcessingServer/Entity")

generate_export_header(${PROJECT_NAME}
    EXPORT_MACRO_NAME ${PROJECT_NAME}_API
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

# 'make install' to the correct locations
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT application_htx_proc)
#install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT dev)

# This makes the project importable from the install directory
# Put config file in per-project dir (name MUST match), can also
# just go into 'cmake'.
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)

add_subdirectory(test)


 	
