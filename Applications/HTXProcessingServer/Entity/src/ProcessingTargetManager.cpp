#include "ProcessingTargetManager.h"

#include <tuple>
#include <algorithm>
#include "ProcessingTargetRepo.h"

namespace HTXProcessingServer::Entity {
    using TargetContainer = std::vector<ProcessingTarget>;
    using IDContainer = std::vector<TargetID>;

    auto ComparePriorityIndex(const ProcessingTarget& a, const ProcessingTarget& b)->bool {
        return a.GetPriorityIndex() < b.GetPriorityIndex();
    }

    class ProcessingTargetManager::Impl {
    public:
        Impl() = default;
        ~Impl() = default;

        ProcessingTargetRepo repo{};

        auto FindFirstPriorityIndex(const TargetContainer& targets)->ProcessingPriorityIndex;
        auto FindLastPriorityIndex(const TargetContainer& targets)->ProcessingPriorityIndex;
        auto Contains(const IDContainer& ids, const TargetID& findingID)->bool;
        auto DivideProcessingTargetsByMatchingIDs(const TargetContainer& targets, const std::vector<TargetID>& ids)->std::tuple<TargetContainer, TargetContainer>;
        auto UpdateRepoTo(const TargetContainer& targets)->void;
    };

    auto ProcessingTargetManager::Impl::FindFirstPriorityIndex(
        const std::vector<ProcessingTarget>& targets) -> ProcessingPriorityIndex {
        ProcessingPriorityIndex firstPriorityIndex = std::numeric_limits<ProcessingPriorityIndex>::max();

        for (const auto& target : targets) {
            const auto targetPriorityIndex = target.GetPriorityIndex();

            if (targetPriorityIndex < firstPriorityIndex) {
                firstPriorityIndex = targetPriorityIndex;
            }
        }

        return firstPriorityIndex;
    }

    auto ProcessingTargetManager::Impl::FindLastPriorityIndex(
        const std::vector<ProcessingTarget>& targets) -> ProcessingPriorityIndex {
        ProcessingPriorityIndex lastPriorityIndex = std::numeric_limits<ProcessingPriorityIndex>::min();

        for (const auto& target : targets) {
            const auto targetPriorityIndex = target.GetPriorityIndex();

            if (targetPriorityIndex > lastPriorityIndex) {
                lastPriorityIndex = targetPriorityIndex;
            }
        }

        return lastPriorityIndex;
    }

    auto ProcessingTargetManager::Impl::Contains(const std::vector<TargetID>& ids, const TargetID& findingID) -> bool {
        for (const auto& id : ids) {
           if (id == findingID) {
               return true;
           }
        }

        return false;
    }

    auto ProcessingTargetManager::Impl::DivideProcessingTargetsByMatchingIDs(const TargetContainer& targets,
        const std::vector<TargetID>& ids) -> std::tuple<TargetContainer, TargetContainer> {
        TargetContainer idMatchedTargets;
        TargetContainer idNotMatchedTargets;

        for (const auto& target : targets) {
            if (idMatchedTargets.size() == ids.size()) {
                idNotMatchedTargets.push_back(target);
                continue;
            }
            
            if (this->Contains(ids, target.GetTargetID())) {
                idMatchedTargets.push_back(target);
            } else {
                idNotMatchedTargets.push_back(target);
            }
        }

        return { idMatchedTargets, idNotMatchedTargets };
    }

    auto ProcessingTargetManager::Impl::UpdateRepoTo(const TargetContainer& targets) -> void {
        this->repo.Clear();
        for (const auto& target : targets) {
            this->repo.Add(target);
        }
    }

    ProcessingTargetManager::~ProcessingTargetManager() = default;

    auto ProcessingTargetManager::GetInstance() -> Pointer {
        static Pointer instance{ new ProcessingTargetManager{} };
        return instance;
    }

    auto ProcessingTargetManager::Append(const ProcessingTarget& target) -> void {
        d->repo.Add(target);
    }

    auto ProcessingTargetManager::Clear() -> void {
        d->repo.Clear();
    }

    auto ProcessingTargetManager::ChangeToTopPriority(const TargetID& id) -> void {
        const auto oldTargets = d->repo.GetTargets();
        const auto firstPriorityIndex = d->FindFirstPriorityIndex(oldTargets);

        TargetContainer priorityChangedTargets;

        for (const auto& oldTarget : oldTargets) {
            ProcessingTarget newTarget = oldTarget;
            if (oldTarget.GetTargetID() == id) {
                newTarget.SetPriorityIndex(firstPriorityIndex - 1);
            }
            priorityChangedTargets.push_back(newTarget);
        }

        d->UpdateRepoTo(priorityChangedTargets);
    }

    auto ProcessingTargetManager::ChangeToTopPriority(const std::vector<TargetID>& ids) -> void {
        const auto oldTargets = d->repo.GetTargets();
        const auto firstPriorityIndex = d->FindFirstPriorityIndex(oldTargets);

        auto [priorityChangingTargets, priorityMaintainingTargets] = d->DivideProcessingTargetsByMatchingIDs(oldTargets, ids);

        auto indexDiff = 1;
        for (auto iterator = priorityChangingTargets.rbegin(); iterator != priorityChangingTargets.rend(); ++iterator) {
            auto& changingTarget = *iterator;
            changingTarget.SetPriorityIndex(firstPriorityIndex - indexDiff);
            indexDiff++;
        }

        TargetContainer priorityChangedTargets;
        priorityChangedTargets.insert(priorityChangedTargets.end(), priorityChangingTargets.begin(), priorityChangingTargets.end());
        priorityChangedTargets.insert(priorityChangedTargets.end(), priorityMaintainingTargets.begin(), priorityMaintainingTargets.end());

        d->UpdateRepoTo(priorityChangedTargets);
    }

    auto ProcessingTargetManager::ChangeToBottomPriority(const TargetID& id) -> void {
        const auto oldTargets = d->repo.GetTargets();
        const auto lastPriorityIndex = d->FindLastPriorityIndex(oldTargets);

        TargetContainer priorityChangedTargets;

        for (const auto& oldTarget : oldTargets) {
            ProcessingTarget newTarget = oldTarget;
            if (oldTarget.GetTargetID() == id) {
                newTarget.SetPriorityIndex(lastPriorityIndex + 1);
            }
            priorityChangedTargets.push_back(newTarget);
        }

        d->UpdateRepoTo(priorityChangedTargets);
    }

    auto ProcessingTargetManager::ChangeToBottomPriority(const std::vector<TargetID>& ids) -> void {
        const auto oldTargets = d->repo.GetTargets();
        const auto lastPriorityIndex = d->FindLastPriorityIndex(oldTargets);

        auto [priorityChangingTargets, priorityMaintainingTargets] = d->DivideProcessingTargetsByMatchingIDs(oldTargets, ids);

        auto indexDiff = 1;
        for (auto iterator = priorityChangingTargets.begin(); iterator != priorityChangingTargets.end(); ++iterator) {
            auto& changingTarget = *iterator;
            changingTarget.SetPriorityIndex(lastPriorityIndex + indexDiff);
            indexDiff++;
        }

        TargetContainer priorityChangedTargets;
        priorityChangedTargets.insert(priorityChangedTargets.end(), priorityMaintainingTargets.begin(), priorityMaintainingTargets.end());
        priorityChangedTargets.insert(priorityChangedTargets.end(), priorityChangingTargets.begin(), priorityChangingTargets.end());
        
        d->UpdateRepoTo(priorityChangedTargets);
    }

    auto ProcessingTargetManager::GetProcessingTargetsByPriorityOrder() const -> std::vector<ProcessingTarget> {
        auto targetsByPriorityOrder = d->repo.GetTargets();
        std::sort(targetsByPriorityOrder.begin(), targetsByPriorityOrder.end(), ComparePriorityIndex);
        return targetsByPriorityOrder;
    }

    ProcessingTargetManager::ProcessingTargetManager() : d{ std::make_unique<Impl>() } {
    }
}
