#include "ProcessingTarget.h"

namespace HTXProcessingServer::Entity {
    class ProcessingTarget::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        TargetID targetID{};
        ProcessingPath processingPath{};
        ProcessingPriorityIndex processingPriorityIndex{};
        ProcessingStatus processingStatus{};
        ProcessingRemainingTimeSec processingRemainingTimeSec{};
        ProcessingTakenTimeSec processingTakenTimeSec{};
    };

    ProcessingTarget::ProcessingTarget() : d{ std::make_unique<Impl>() } {
    }

    ProcessingTarget::ProcessingTarget(const ProcessingTarget& other) : d{ std::make_unique<Impl>(*other.d) } {
    }

    ProcessingTarget::ProcessingTarget(const TargetID& id, const ProcessingPath& path,
        const ProcessingPriorityIndex& index, const ProcessingStatus& status,
        const ProcessingRemainingTimeSec& remainingTime, const ProcessingTakenTimeSec& takenTime) : d{ std::make_unique<Impl>() } {
        this->SetTargetID(id);
        this->SetPath(path);
        this->SetPriorityIndex(index);
        this->SetStatus(status);
        this->SetRemainingTime(remainingTime);
        this->SetTakenTime(takenTime);
    }

    ProcessingTarget::~ProcessingTarget() = default;

    auto ProcessingTarget::operator=(const ProcessingTarget& other) -> ProcessingTarget& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto ProcessingTarget::SetTargetID(const TargetID& id) -> void {
        d->targetID = id;
    }

    auto ProcessingTarget::SetPath(const ProcessingPath& path) -> void {
        d->processingPath = path;
    }

    auto ProcessingTarget::SetPriorityIndex(const ProcessingPriorityIndex& index) -> void {
        d->processingPriorityIndex = index;
    }

    auto ProcessingTarget::SetStatus(const ProcessingStatus& status) -> void {
        d->processingStatus = status;
    }

    auto ProcessingTarget::SetRemainingTime(const ProcessingRemainingTimeSec& time) -> void {
        d->processingRemainingTimeSec = time;
    }

    auto ProcessingTarget::SetTakenTime(const ProcessingTakenTimeSec& time) -> void {
        d->processingTakenTimeSec = time;
    }

    auto ProcessingTarget::GetTargetID() const -> TargetID {
        return d->targetID;
    }

    auto ProcessingTarget::GetPath() const -> ProcessingPath {
        return d->processingPath;
    }

    auto ProcessingTarget::GetPriorityIndex() const -> ProcessingPriorityIndex {
        return d->processingPriorityIndex;
    }

    auto ProcessingTarget::GetStatus() const -> ProcessingStatus {
        return d->processingStatus;
    }

    auto ProcessingTarget::GetRemainingTime() const -> ProcessingRemainingTimeSec {
        return d->processingRemainingTimeSec;
    }

    auto ProcessingTarget::GetTakenTime() const -> ProcessingTakenTimeSec {
        return d->processingTakenTimeSec;
    }
}
