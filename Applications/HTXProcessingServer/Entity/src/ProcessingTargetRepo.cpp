#include "ProcessingTargetRepo.h"

namespace HTXProcessingServer::Entity {
    class ProcessingTargetRepo::Impl {
    public:
        Impl() = default;
        Impl(const Impl& other) = default;
        ~Impl() = default;

        auto operator=(const Impl& other)->Impl & = default;

        std::vector<ProcessingTarget> container;
    };

    ProcessingTargetRepo::ProcessingTargetRepo() : d{ std::make_unique<Impl>() } {
    }

    ProcessingTargetRepo::ProcessingTargetRepo(const ProcessingTargetRepo& other) : d{ std::make_unique<Impl>(*other.d) } {
    }

    ProcessingTargetRepo::~ProcessingTargetRepo() = default;

    auto ProcessingTargetRepo::operator=(const ProcessingTargetRepo& other) -> ProcessingTargetRepo& {
        *(this->d) = *(other.d);
        return *this;
    }

    auto ProcessingTargetRepo::Add(const ProcessingTarget& target) -> void {
        d->container.push_back(target);
    }

    auto ProcessingTargetRepo::GetCount() const -> size_t {
        return d->container.size();
    }

    auto ProcessingTargetRepo::GetTargets() const -> std::vector<ProcessingTarget> {
        return d->container;
    }

    auto ProcessingTargetRepo::Clear() -> void {
        d->container.clear();
    }
}
