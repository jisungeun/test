function generateTileImage(sampleAngle, roiSize, roiDist, roiYOffset)
    baseImg = zeros(2000, 4000, 'double');
    baseImg(:,:) = 255;
    baseImg(1000-50:1000+50,:) = 0;

    %rotation sample
    baseImg = rotateMatrix(baseImg, sampleAngle);
    baseImg(baseImg > 100) = 255;
    
    roiSizeHalf = round(roiSize/2);
    y0 = 1000 - roiSizeHalf;
    y1 = y0 + roiSize - 1;
    
    %figure
    index = 0;
    for yOffset = roiYOffset
        imgLeft = baseImg(y0:y1, 1:roiSize);
        imgRight = baseImg(y0-yOffset:y1-yOffset, roiDist:(roiDist+roiSize-1));
        %subplot(121), imagesc(imgLeft), subplot(122), imagesc(imgRight);
        %pause;

        imwrite(uint8(imgLeft), sprintf('%d_1_%d.png', index, yOffset));
        imwrite(uint8(imgRight), sprintf('%d_2_%d.png', index, yOffset));
        index = index + 1;
    end
end