function rotatedMatrix = rotateMatrix(inputMatrix, angle)
    % Convert the angle from degrees to radians.
    theta = deg2rad(angle);
    
    % Create a 2D rotation matrix.
    R = [cos(theta), -sin(theta); sin(theta), cos(theta)];
    
    % Subtract the mean of the matrix to centralize the rotation.
    matrixMean = mean(inputMatrix(:));
    centralizedMatrix = inputMatrix - matrixMean;
    
    % Calculate the dimensions of the input matrix.
    [rows, cols] = size(inputMatrix);
    
    % Initialize the rotated matrix.
    rotatedMatrix = zeros(rows, cols);
    
    % Rotate each point in the matrix.
    for i = 1:rows
        for j = 1:cols
            % Apply the rotation matrix.
            point = R * [i; j];
            
            % Find the nearest integer coordinates.
            x = round(point(1));
            y = round(point(2));
            
            % Check if the coordinates are within the matrix.
            if x > 0 && x <= rows && y > 0 && y <= cols
                % Transfer the value to the rotated matrix.
                rotatedMatrix(i, j) = centralizedMatrix(x, y);
            end
        end
    end
    
    % Add the mean of the matrix to de-centralize the rotation.
    rotatedMatrix = rotatedMatrix + matrixMean;
end
