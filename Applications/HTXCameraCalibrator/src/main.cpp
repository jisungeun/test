#define LOGGER_TAG "[HTXCameraCalibrator]"

#include <QFile>
#include <QStandardPaths>
#include <QString>
#include <QApplication>

#include <TCLogger.h>

#include "Version.h"
#include "MainWindow.h"

int main(int argc, char* argv[]) {
    QApplication app(argc, argv);

    app.setApplicationName("HTXCameraCalibrator");
    app.setOrganizationName("Tomocube, Inc.");
    app.setOrganizationDomain("www.tomocube.com");

    QFile f(":qdarkstyle/style.qss");

    if (!f.exists()) {
        printf("Unable to set stylesheet, file not found\n");
    } else {
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
    }

    auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
    TC::Logger::Initialize(QString("%1/log/HTXCameraCalibrator.log").arg(appDataPath), QString("%1/log/HTXCameraCalibratorErrors.log").arg(appDataPath));

    TC::HTXCameraCalibrator::MainWindow window;
    window.show();

    return app.exec();
}