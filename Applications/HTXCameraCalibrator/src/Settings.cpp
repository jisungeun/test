#include <QSettings>
#include <QStandardPaths>
#include <QString>

#include "Settings.h"

#define MCU_CONFIG_PATH             "MCUConfigPath"
#define LIVE_CONFIG_PATH            "LiveConfigPath"
#define MOTION_PRESET_PATH          "MotionPresetPath"
#define ROTATION_CALIBRATION_PATH   "RotationCalibrationPath"

namespace TC::HTXCameraCalibrator {
    Settings::Settings() {
        auto qs = QSettings();
        auto allKeys = qs.allKeys();

        if (!allKeys.contains(MCU_CONFIG_PATH)) {
            const auto defaultPath = QString("%1/config/mcuconfig.ini")
                .arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
            qs.setValue(MCU_CONFIG_PATH, defaultPath);
        }

        if (!allKeys.contains(LIVE_CONFIG_PATH)) {
            const auto defaultPath = QString("%1/config/liveconfig.ini")
                .arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
            qs.setValue(LIVE_CONFIG_PATH, defaultPath);
        }

        if (!allKeys.contains(MOTION_PRESET_PATH)) {
            const auto defaultPath = QString("%1/config/motionpreset.ini")
                .arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
            qs.setValue(MOTION_PRESET_PATH, defaultPath);
        }

        if (!allKeys.contains(ROTATION_CALIBRATION_PATH)) {
            const auto defaultPath = QString("%1/config/rotationcalibration.ini")
                .arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
            qs.setValue(ROTATION_CALIBRATION_PATH, defaultPath);
        }
    }

    Settings::~Settings() {
    }

    auto Settings::GetInstance() -> Pointer {
        static Pointer theInstance{ new Settings() };
        return theInstance;
    }

    auto Settings::GetValue(const QString& key) const -> QVariant {
        return QSettings().value(key);
    }

    auto Settings::GetMCUConfigPath() -> QString {
        auto settings = GetInstance();
        return settings->GetValue(MCU_CONFIG_PATH).toString();
    }

    auto Settings::GetLiveConfigPath() -> QString {
        auto settings = GetInstance();
        return settings->GetValue(LIVE_CONFIG_PATH).toString();
    }

    auto Settings::GetMotionPresetPath()->QString {
        auto settings = GetInstance();
        return settings->GetValue(MOTION_PRESET_PATH).toString();
    }

    auto Settings::GetRotationCalibrationPath()->QString {
        auto settings = GetInstance();
        return settings->GetValue(ROTATION_CALIBRATION_PATH).toString();
    }
}
