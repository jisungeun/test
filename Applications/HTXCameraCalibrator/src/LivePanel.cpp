#include <QMessageBox>

#include "LivePanel.h"
#include "LivePanelControl.h"
#include "ui_LivePanel.h"

namespace TC::HTXCameraCalibrator {
    struct LivePanel::Impl {
        Ui::LivePanel ui;
        LivePanelControl control;

        auto UpdateUi(const LivePanelControl::Config& config)->void;
        auto UpdateConfig(LivePanelControl::Config& config)->void;
    };

    auto LivePanel::Impl::UpdateUi(const LivePanelControl::Config& config) -> void {
        ui.sequenceId->setValue(config.sequenceId);
        ui.ledChannel->setValue(config.ledChannel);
        ui.ledIntensity->setValue(config.ledIntensity);
        ui.exposureTime->setValue(config.exposureUSec);
        ui.intervalTime->setValue(config.intervalUSec);
        ui.cameraType->setValue(config.cameraType);
        ui.filter->setValue(config.filter);
    }

    auto LivePanel::Impl::UpdateConfig(LivePanelControl::Config& config) -> void {
        config.sequenceId = ui.sequenceId->value();
        config.ledChannel = ui.ledChannel->value();
        config.ledIntensity = ui.ledIntensity->value();
        config.exposureUSec = ui.exposureTime->value();
        config.intervalUSec = ui.intervalTime->value();
        config.cameraType = ui.cameraType->value();
        config.filter = ui.filter->value();
    }

    LivePanel::LivePanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);

        LivePanelControl::Config config;
        if (!d->control.LoadConfig(config)) {
            setDisabled(true);
            return;
        }
        
        d->UpdateUi(config);

        connect(d->ui.applyBtn, &QPushButton::clicked, this, [this]() {
            LivePanelControl::Config config;
            d->UpdateConfig(config);
            
            if (!d->control.Apply(config)) {
                QMessageBox::warning(this, "Live", "Failed to change MCU pattern sequence for live imaging");
                return;
            }
        });
    }

    LivePanel::~LivePanel() {
    }
}