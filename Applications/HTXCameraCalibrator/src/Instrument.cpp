#define LOGGER_TAG "[Instrument]"

#include <QFile>
#include <QThread>
#include <QElapsedTimer>
#include <QSettings>

#include <TCLogger.h>
#include <CameraControlFactory.h>
#include <ImageSink.h>
#include <MCUFactory.h>

#include "Settings.h"
#include "Instrument.h"


namespace TC::HTXCameraCalibrator {
    using ImageSink = TC::CameraControl::ImageSink;
    using CameraSystem = TC::CameraControl::CameraSystem;
    using CameraControlFactory = TC::CameraControl::CameraControlFactory;
    using DeviceInfo = TC::CameraControl::DeviceInfo;
    using CameraControl = TC::CameraControl::CameraControl;
    using IMCUControl = TC::MCUControl::IMCUControl;
    using MCUFactory = TC::MCUControl::MCUFactory;
    using MotionCommand = TC::MCUControl::MotionCommandType;

    struct Instrument::Impl {
        ImageSink::Pointer sink{ nullptr };
        CameraSystem::Pointer cameraSystem{ nullptr };
        CameraControl::Pointer cameraControl{ nullptr };
        IMCUControl::Pointer mcuControl{ nullptr };

        TC::MCUControl::MCUConfig mcuConfig;
        LiveConfig liveConfig;

        auto CheckMoving(const TC::MCUControl::MCUResponse& response) const -> bool;
        auto WaitForMotionStop(uint32_t msec = 60000, uint32_t interval = 100) const -> bool;
        auto RunMacro(const QList<QList<int32_t>>& macros) -> bool;
        auto StartLiveMacro(const LiveConfig& config)->QList<QList<int32_t>>;
        auto StopLiveMacro(const LiveConfig& config)->QList<QList<int32_t>>;
        auto BuildPacket(MotionCommand cmd, const QVector<int32_t>& params) const->QVector<int32_t>;
    };

    auto Instrument::Impl::CheckMoving(const TC::MCUControl::MCUResponse& response) const -> bool {
        using ResponseCode = TC::MCUControl::Response;
        QVector<ResponseCode> codes{ ResponseCode::AxisXMoving, ResponseCode::AxisYMoving, ResponseCode::AxisZMoving,
                                     ResponseCode::AxisUMoving, ResponseCode::AxisVMoving, ResponseCode::AxisWMoving };
        for (auto code : codes) {
            if (response.GetValueAsFlag(code)) {
                return true;
            }
        }

        if (response.GetValue(ResponseCode::AfOn) == 1) {
            return true;
        }

        return false;
    }

    auto Instrument::Impl::WaitForMotionStop(uint32_t msec, uint32_t interval) const -> bool {
        QElapsedTimer timer;

        timer.start();
        while (timer.elapsed() < msec) {
            TC::MCUControl::MCUResponse status;
            mcuControl->CheckStatus(status);
            if (!CheckMoving(status)) return true;
            QThread::msleep(interval);
        }

        return false;
    }

    auto Instrument::Impl::RunMacro(const QList<QList<int32_t>>& macros) -> bool {
        if (!WaitForMotionStop()) return false;

        if (!mcuControl->StartMacroStreaming()) return false;
        for (auto macro : macros) {
            if (!mcuControl->SendMacrosToStreamBuffer({ macro })) return false;
        }

        TC::MCUControl::MCUResponse status;
        while (mcuControl->CheckStatus(status)) {
            const auto completed = status.GetValue(TC::MCUControl::Response::MacroStreamingIndex);
            const auto error = status.GetValue(TC::MCUControl::Response::MacroStreamingError);

            if (error == 1) return false;
            if (completed == macros.length()) break;

            QThread::msleep(100);
        }

        if (!mcuControl->StopMacroStreaming()) return false;

        return true;
    }

    auto Instrument::Impl::StartLiveMacro(const LiveConfig& config) -> QList<QList<int32_t>> {
        QList<QList<int32_t>> macros;

        const auto changeFilter = BuildPacket(MotionCommand::ChangeFilter, { config.filter });
        macros.push_back(QList<int32_t>::fromVector(changeFilter));

        const auto patternSequence = BuildPacket(MotionCommand::PatternSequence, 
            { 1,
              config.sequenceId,
              config.ledChannel==0 ? config.ledIntensity : 0,
              config.ledChannel==1 ? config.ledIntensity : 0,
              config.ledChannel==2 ? config.ledIntensity : 0,
              config.exposureUSec,
              config.intervalUSec,
              config.cameraType
            }
        );
        macros.push_back(QList<int32_t>::fromVector(patternSequence));

        return macros;
    }

    auto Instrument::Impl::StopLiveMacro(const LiveConfig& config) -> QList<QList<int32_t>> {
        QList<QList<int32_t>> macros;

        const auto patternSequence = BuildPacket(MotionCommand::PatternSequence,
            { 0,
              config.sequenceId,
              config.ledChannel == 0 ? config.ledIntensity : 0,
              config.ledChannel == 1 ? config.ledIntensity : 0,
              config.ledChannel == 2 ? config.ledIntensity : 0,
              config.exposureUSec,
              config.intervalUSec,
              config.cameraType
            }
        );
        macros.push_back(QList<int32_t>::fromVector(patternSequence));

        return macros;
    }

    auto Instrument::Impl::BuildPacket(MotionCommand cmd, const QVector<int32_t>& params) const->QVector<int32_t> {
        QVector<int32_t> packet(6);
        packet[0] = 0;
        packet[1] = 0;
        packet[2] = 0;
        packet[3] = 0;
        packet[4] = cmd._to_integral();
        packet[5] = params.size();
        packet.append(params);
        return packet;
    }

    Instrument::Instrument() : d{ new Impl } {
        d->mcuControl = MCUFactory::CreateControl(TC::MCUControl::Model::HTX);
        d->cameraSystem = CameraControlFactory::Create(TC::CameraControl::CameraControlFactory::Controller::PointGrey);
        d->sink.reset(new ImageSink());
    }

    Instrument::~Instrument() {
    }

    auto Instrument::GetInstance() -> Pointer {
        static Pointer theInstance{ new Instrument() };
        return theInstance;
    }

    auto Instrument::CleanUp() -> void {
        if(d->cameraControl) d->cameraControl->CleanUp();
    }

    auto Instrument::LoadLiveConfig(LiveConfig& config)->bool {
        const auto configPath = Settings::GetLiveConfigPath();
        if (configPath.isEmpty() || !QFile::exists(configPath)) {
            QLOG_ERROR() << "No live configuraion file exists at " << configPath;
            return false;
        }

        QSettings qs(configPath, QSettings::IniFormat);
        config.sequenceId = qs.value("SequenceId", 0).toInt();
        config.ledChannel = qs.value("LEDChannel", 0).toInt();
        config.ledIntensity = qs.value("LEDIntensity", 0).toInt();
        config.exposureUSec = qs.value("ExposureUSec", 0).toInt();
        config.intervalUSec = qs.value("IntervalUSec", 0).toInt();
        config.cameraType = qs.value("CameraType", 0).toInt();
        config.filter = qs.value("Filter", 0).toInt();

        d->liveConfig = config;

        return true;
    }

    auto Instrument::StoreLiveConfig(const LiveConfig& config)->bool {
        const auto configPath = Settings::GetLiveConfigPath();
        if (configPath.isEmpty()) return false;

        QSettings qs(configPath, QSettings::IniFormat);

        qs.setValue("SequenceId", config.sequenceId);
        qs.setValue("LEDChannel", config.ledChannel);
        qs.setValue("LEDIntensity", config.ledIntensity);
        qs.setValue("ExposureUSec", config.exposureUSec);
        qs.setValue("IntervalUSec", config.intervalUSec);
        qs.setValue("CameraType", config.cameraType);
        qs.setValue("Filter", config.filter);

        d->liveConfig = config;

        return true;
    }

    auto Instrument::GetCameras() const -> QList<DeviceInfo> {
        return d->cameraSystem->GetDeviceInfos();
    }

    auto Instrument::InitializeCamera(int32_t cameraIndex) -> bool {
        const auto& infos = d->cameraSystem->GetDeviceInfos();
        if (cameraIndex >= infos.size()) return false;

        if (!LoadLiveConfig(d->liveConfig)) return false;

        const auto& deviceInfo = infos.at(cameraIndex);
        d->cameraControl = d->cameraSystem->GetCamera(deviceInfo.serialNumber, d->sink.get());
        return d->cameraControl->Initialize();
    }

    auto Instrument::LoadMCUConfiguration() -> bool {
        const auto configPath = Settings::GetMCUConfigPath();
        if (configPath.isEmpty() || !QFile::exists(configPath)) return false;

        if (!d->mcuControl->LoadConfig(configPath)) return false;

        d->mcuConfig = d->mcuControl->GetConfig();

        return true;
    }

    auto Instrument::InitializeMCU(bool justOpen, bool& noNeedInitialization) -> bool {
        if (!d->mcuControl->OpenPort()) {
            QLOG_ERROR() << "It fails to connect to MCU";
            return false;
        }

        noNeedInitialization = false;

        if (justOpen) {
            TC::MCUControl::MCUResponse mcuResponse;
            if (!d->mcuControl->CheckStatus(mcuResponse)) return false;

            auto mcuState = mcuResponse.GetState();
            if (mcuState._to_integral() < TC::MCUControl::MCUState::Error) {
                noNeedInitialization = true;
                return true;
            }

            QLOG_ERROR() << "MCU is in " << mcuState._to_string() << " state";
            if (!d->mcuControl->Recover()) {
                QLOG_ERROR() << "It fails to recover MCU";
                return false;
            }

            QElapsedTimer timer;
            timer.start();
            do {
                d->mcuControl->CheckStatus(mcuResponse);
                mcuState = mcuResponse.GetState();
                QThread::msleep(200);
                const auto isReadyState = (mcuState == +TC::MCUControl::MCUState::Ready);
                const auto isMoving = d->CheckMoving(mcuResponse);
                if (isReadyState && !isMoving) break;
            } while (timer.elapsed() < 60000);

            QLOG_INFO() << "MCU State After Recover = " << mcuState._to_string();
        }

        if (!d->mcuControl->StartInitialization()) return false;
        return true;
    }

    auto Instrument::CheckMCUInitialiation(int32_t& progress) -> bool {
        return d->mcuControl->CheckInitialization(progress);
    }

    auto Instrument::GetLatestImage()->TC::CameraControl::Image::Pointer {
        return d->sink->GetLatestImage(true);
    }

    auto Instrument::CaptureImage(int32_t timeoutSec)->TC::CameraControl::Image::Pointer {
        TC::CameraControl::Image::Pointer image;
        d->sink->AllowClear(false);

        QElapsedTimer timer;
        timer.start();
        do {
            image = d->sink->GetLatestImage(false);
            if (image) break;
            QThread::msleep(50);
        } while (timer.elapsed() < (timeoutSec * 1000));

        d->sink->AllowClear(true);

        return image;
    }

    auto Instrument::StartLive(const LiveConfig& config) -> bool {
        if (!d->cameraControl->StopAcquisition()) return false;
        if (!d->mcuControl->SetLEDChannel(config.ledChannel)) return false;
        if (!d->RunMacro(d->StopLiveMacro(config) + d->StartLiveMacro(config))) return false;
        if (!d->cameraControl->StartAcquisition(0)) return false;
        return true;
    }

    auto Instrument::StopLive(const LiveConfig& config) -> bool {
        if (!d->cameraControl->StopAcquisition()) return false;
        if (!d->RunMacro(d->StopLiveMacro(config))) return false;
        return true;
    }

    auto Instrument::ResumeLive() -> bool {
        return StartLive(d->liveConfig);
    }

    auto Instrument::GetPosition() const->Position {
        Position position;

        auto [result, poses] = d->mcuControl->GetPositions();
        if (!result) return position;

        position.pulse.x = poses[Axis::X];
        position.pulse.y = poses[Axis::Y];
        position.pulse.z = poses[Axis::Z];
        position.pulse.c = poses[Axis::U];

        position.mm.x = (position.pulse.x * 1.0) / d->mcuConfig.GetResolution(Axis::X);
        position.mm.y = (position.pulse.y * 1.0) / d->mcuConfig.GetResolution(Axis::Y);
        position.mm.z = (position.pulse.z * 1.0) / d->mcuConfig.GetResolution(Axis::Z);
        position.mm.c = (position.pulse.c * 1.0) / d->mcuConfig.GetResolution(Axis::U);

        return position;
    }

    auto Instrument::MoveAxis(Axis axis, double targetMm) -> bool {
        const auto targetPulse = static_cast<int32_t>(targetMm * d->mcuConfig.GetResolution(axis));
        return d->mcuControl->Move(axis, targetPulse);
    }

    auto Instrument::IsMoving(Axis axis) const -> bool {
        TC::MCUControl::Flag flag{ TC::MCUControl::Flag::NotMoving };
        d->mcuControl->IsMoving(axis, flag);
        return flag == +TC::MCUControl::Flag::Moving;
    }
}