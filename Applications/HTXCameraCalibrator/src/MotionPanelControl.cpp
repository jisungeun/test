#include <QSettings>

#include "Settings.h"
#include "Instrument.h"
#include "MotionPanelControl.h"

namespace TC::HTXCameraCalibrator {
    struct MotionPanelControl::Impl {
    };

    MotionPanelControl::MotionPanelControl() : d{ new Impl } {
    }

    MotionPanelControl::~MotionPanelControl() {
    }

    auto MotionPanelControl::GetPosition() const->Position {
        auto instrument = Instrument::GetInstance();
        return instrument->GetPosition();
    }

    auto MotionPanelControl::MoveAxis(Axis axis, double targetMm)->bool {
        auto instrument = Instrument::GetInstance();
        return instrument->MoveAxis(axis, targetMm);
    }

    auto MotionPanelControl::IsMoving(Axis axis) const->bool {
        auto instrument = Instrument::GetInstance();
        return instrument->IsMoving(axis);
    }

    auto MotionPanelControl::StorePreset(int32_t slotIndex, double xPos, double yPos, double zPos, double cPos) -> void {
        const auto prefix = QString("Preset/%1").arg(slotIndex);

        QSettings qs(Settings::GetMotionPresetPath(), QSettings::IniFormat);
        qs.setValue(QString("%1/X").arg(prefix), xPos);
        qs.setValue(QString("%1/Y").arg(prefix), yPos);
        qs.setValue(QString("%1/Z").arg(prefix), zPos);
        qs.setValue(QString("%1/C").arg(prefix), cPos);
    }

    auto MotionPanelControl::RestorePreset(int32_t slotIndex) -> std::tuple<bool, double, double, double, double> {
        const auto prefix = QString("Preset/%1").arg(slotIndex);
        auto xKey = QString("%1/X").arg(prefix);
        auto yKey = QString("%1/Y").arg(prefix);
        auto zKey = QString("%1/Z").arg(prefix);
        auto cKey = QString("%1/C").arg(prefix);

        QSettings qs(Settings::GetMotionPresetPath(), QSettings::IniFormat);

        if (!qs.contains(xKey)) return std::make_tuple(false, 0, 0, 0, 0);
        if (!qs.contains(yKey)) return std::make_tuple(false, 0, 0, 0, 0);
        if (!qs.contains(zKey)) return std::make_tuple(false, 0, 0, 0, 0);
        if (!qs.contains(cKey)) return std::make_tuple(false, 0, 0, 0, 0);

        return std::make_tuple(true,
            qs.value(xKey).toDouble(),
            qs.value(yKey).toDouble(),
            qs.value(zKey).toDouble(),
            qs.value(cKey).toDouble());
    }
}