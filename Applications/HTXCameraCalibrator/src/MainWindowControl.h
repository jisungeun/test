#pragma once
#include <memory>

#include <CameraSystem.h>
#include <CameraImage.h>

namespace TC::HTXCameraCalibrator {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

        auto GetCameras() const->QList<TC::CameraControl::DeviceInfo>;
        auto InitializeCamera(int32_t cameraIndex) -> bool;
        auto LoadMCUConfiguration() -> bool;
        auto InitializeMCU(bool justOpen, bool& noNeedInitialization) -> bool;
        auto CheckMCUInitialiation(int32_t& progress) -> bool;

        auto StartLive() -> bool;
        auto GetLatestImage()->TC::CameraControl::Image::Pointer;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}