#pragma once
#include <memory>
#include <QImage>
#include <CameraImage.h>

namespace TC::HTXCameraCalibrator {
    class ImageConverter {
    public:
        typedef std::shared_ptr<ImageConverter> Pointer;
    protected:
        ImageConverter();

    public:
        ~ImageConverter();

        static auto GetInstance()->Pointer;
        static auto Image2QImage(TC::CameraControl::Image::Pointer& image)->QImage;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}