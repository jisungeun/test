#include "MotionUpdater.h"
#include "MotionObserver.h"

namespace TC::HTXCameraCalibrator {
    MotionObserver::MotionObserver(QObject* parent) : QObject(parent) {
        MotionUpdater::GetInstance()->Register(this);
    }

    MotionObserver::~MotionObserver() {
        MotionUpdater::GetInstance()->Deregister(this);
    }

    auto MotionObserver::UpdatePosition(double xPos, double yPos, double zPos, double cPos) -> void {
        emit sigUpdatePosition(xPos, yPos, zPos, cPos);
    }
}