#pragma once
#include <memory>

#include "Instrument.h"

namespace TC::HTXCameraCalibrator {
    class MotionPanelControl {
    public:
        using Position = Instrument::Position;
        using Axis = Instrument::Axis;

    public:
        MotionPanelControl();
        ~MotionPanelControl();

        auto GetPosition() const->Position;
        auto MoveAxis(Axis axis, double targetMm)->bool;
        auto IsMoving(Axis axis) const->bool;

        auto StorePreset(int32_t slotIndex, double xPos, double yPos, double zPos, double cPos)->void;
        auto RestorePreset(int32_t slotIndex)->std::tuple<bool, double, double, double, double>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}