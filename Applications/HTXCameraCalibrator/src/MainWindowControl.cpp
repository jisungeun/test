#include "Settings.h"
#include "Instrument.h"
#include "MotionUpdater.h"
#include "MainWindowControl.h"

namespace TC::HTXCameraCalibrator {
    struct MainWindowControl::Impl {
        Instrument::Pointer instrument{ nullptr };
    };

    MainWindowControl::MainWindowControl() : d{ new Impl } {
        d->instrument = Instrument::GetInstance();
    }

    MainWindowControl::~MainWindowControl() {
        d->instrument->CleanUp();
    }

    auto MainWindowControl::GetCameras() const -> QList<TC::CameraControl::DeviceInfo> {
        return d->instrument->GetCameras();
    }

    auto MainWindowControl::InitializeCamera(int32_t cameraIndex) -> bool {
        return d->instrument->InitializeCamera(cameraIndex);
    }

    auto MainWindowControl::LoadMCUConfiguration() -> bool {
        return d->instrument->LoadMCUConfiguration();
    }

    auto MainWindowControl::InitializeMCU(bool justOpen, bool& noNeedInitialization) -> bool {
        const auto res = d->instrument->InitializeMCU(justOpen, noNeedInitialization);
        if (res && noNeedInitialization) {
            auto pos = d->instrument->GetPosition();
            MotionUpdater::GetInstance()->UpdatePosition(pos.mm.x, pos.mm.y, pos.mm.z, pos.mm.c);
        }

        return res;
    }

    auto MainWindowControl::CheckMCUInitialiation(int32_t& progress) -> bool {
        const auto res = d->instrument->CheckMCUInitialiation(progress);
        if (res) {
            auto pos = d->instrument->GetPosition();
            MotionUpdater::GetInstance()->UpdatePosition(pos.mm.x, pos.mm.y, pos.mm.z, pos.mm.c);
        }

        return res;
    }

    auto MainWindowControl::StartLive() -> bool {
        return d->instrument->ResumeLive();
    }

    auto MainWindowControl::GetLatestImage()->TC::CameraControl::Image::Pointer {
        return d->instrument->GetLatestImage();
    }
}