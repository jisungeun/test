#pragma once
#include <memory>
#include <CameraControl.h>

namespace TC::HTXCameraCalibrator {
    class RotationMeasurePanelControl {
    public:
        using Image = TC::CameraControl::Image;

    public:
        RotationMeasurePanelControl();
        ~RotationMeasurePanelControl();

        auto StorePosition()->void;
        auto RestorePosition()->std::tuple<bool, double, double, double, double>;

        auto StoreParameters(double fov, int32_t distance, int32_t pause)->void;
        auto RestoreParameters()->std::tuple<double, int32_t, int32_t>;

        auto Move2Start()->bool;
        auto Move2Left()->bool;
        auto Move2Right()->bool;
        auto GetImage()->Image::Pointer;
        auto Measure(Image::Pointer image1, Image::Pointer image2)->double;

        auto IsReadyToSave() const->bool;
        auto SaveMergedImage(const QString& path)->bool;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}