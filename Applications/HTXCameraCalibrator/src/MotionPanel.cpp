#include <QCoreApplication>
#include <QElapsedTimer>
#include <QInputDialog>
#include <QMessageBox>
#include <QVector>

#include "MotionObserver.h"
#include "MotionPanel.h"
#include "MotionPanelControl.h"
#include "ui_MotionPanel.h"

namespace TC::HTXCameraCalibrator {
    using Position = MotionPanelControl::Position;
    using Axis = MotionPanelControl::Axis;

    struct MotionPanel::Impl {
        MotionPanel* p{ nullptr };
        Ui::MotionPanel ui;
        MotionPanelControl control;
        MotionObserver* motioObserver{ nullptr };

        Impl(MotionPanel* p) : p{ p } {}

        auto UpdatePosition()->void;
        auto MoveAxis(Axis axis, double targetMm, int32_t timeOutSec = 60)->void;
        auto SavePreset()->void;
        auto MoveToPreset(int32_t slotIndex)->void;
        auto UpdatePresetButton(int32_t slotIndex = 0)->void;
    };

    auto MotionPanel::Impl::UpdatePosition()->void {
        const auto position = control.GetPosition();
        ui.xPos->setValue(position.mm.x);
        ui.yPos->setValue(position.mm.y);
        ui.zPos->setValue(position.mm.z);
        ui.cPos->setValue(position.mm.c);
    }

    auto MotionPanel::Impl::MoveAxis(Axis axis, double targetMm, int32_t timeOutSec) -> void {
        QElapsedTimer timer;

        p->setDisabled(true);

        timer.start();
        if (!control.MoveAxis(axis, targetMm)) {
            p->setEnabled(true);
            return;
        }

        do {
            UpdatePosition();

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 100);

            if (!control.IsMoving(axis)) break;
        } while (timer.elapsed() < (timeOutSec * 1000));

        UpdatePosition();

        p->setEnabled(true);
    }

    auto MotionPanel::Impl::SavePreset() -> void {
        bool ok{ false };
        auto slotIndex = QInputDialog::getInt(p, "Preset", "Preset Slot", 1, 1, 5, 1, &ok);
        if (!ok) {
            QMessageBox::warning(p, "Preset", "Preset is not saved");
            return;
        }

        control.StorePreset(slotIndex, ui.xPos->value(), ui.yPos->value(), ui.zPos->value(), ui.cPos->value());
        UpdatePresetButton(slotIndex);
    }

    auto MotionPanel::Impl::MoveToPreset(int32_t slotIndex)->void {
        const auto [res, xPos, yPos, zPos, cPos] = control.RestorePreset(slotIndex);
        if (!res) {
            QMessageBox::warning(p, "Preset", "No preset exists");
            return;
        }

        MoveAxis(Axis::X, xPos);
        MoveAxis(Axis::Y, yPos);
        MoveAxis(Axis::Z, zPos);
        MoveAxis(Axis::U, cPos);
    }

    auto MotionPanel::Impl::UpdatePresetButton(int32_t slotIndex) -> void {
        QVector<int32_t> slotIndexes = (slotIndex == 0) ? QVector({1, 2, 3, 4, 5}) : QVector({slotIndex});
        QVector<QPushButton*> buttons = { ui.preset1Btn, ui.preset2Btn, ui.preset3Btn, ui.preset4Btn, ui.preset5Btn };

        for (auto index : slotIndexes) {
            const auto res = control.RestorePreset(index);
            const auto hasValue = std::get<0>(res);
            buttons.at(index - 1)->setEnabled(hasValue);
        }
    }

    MotionPanel::MotionPanel(QWidget* parent) : QWidget(parent), d{ new Impl(this) } {
        d->ui.setupUi(this);

        d->motioObserver = new MotionObserver(this);

        connect(d->ui.xMoveBtn, &QPushButton::clicked, this, [this]() {
            d->MoveAxis(Axis::X, d->ui.xPos->value());
        });

        connect(d->ui.yMoveBtn, &QPushButton::clicked, this, [this]() {
            d->MoveAxis(Axis::Y, d->ui.yPos->value());
        });

        connect(d->ui.zMoveBtn, &QPushButton::clicked, this, [this]() {
            d->MoveAxis(Axis::Z, d->ui.zPos->value());
        });

        connect(d->ui.cMoveBtn, &QPushButton::clicked, this, [this]() {
            d->MoveAxis(Axis::U, d->ui.cPos->value());
        });

        connect(d->ui.xPlusBtn, &QPushButton::clicked, this, [this]() {
            d->MoveAxis(Axis::X, d->ui.xPos->value() + d->ui.xStep->value());
        });

        connect(d->ui.xMinusBtn, &QPushButton::clicked, this, [this]() {
            d->MoveAxis(Axis::X, d->ui.xPos->value() - d->ui.xStep->value());
        });

        connect(d->ui.yPlusBtn, &QPushButton::clicked, this, [this]() {
            d->MoveAxis(Axis::Y, d->ui.yPos->value() + d->ui.yStep->value());
        });

        connect(d->ui.yMinusBtn, &QPushButton::clicked, this, [this]() {
            d->MoveAxis(Axis::Y, d->ui.yPos->value() - d->ui.yStep->value());
        });

        connect(d->ui.zPlusBtn, &QPushButton::clicked, this, [this]() {
            d->MoveAxis(Axis::Z, d->ui.zPos->value() + d->ui.zStep->value());
        });
        
        connect(d->ui.zMinusBtn, &QPushButton::clicked, this, [this]() {
            d->MoveAxis(Axis::Z, d->ui.zPos->value() - d->ui.zStep->value());
        });

        connect(d->ui.cPlusBtn, &QPushButton::clicked, this, [this]() {
            d->MoveAxis(Axis::U, d->ui.cPos->value() + d->ui.cStep->value());
        });

        connect(d->ui.cMinusBtn, &QPushButton::clicked, this, [this]() {
            d->MoveAxis(Axis::U, d->ui.cPos->value() - d->ui.cStep->value());
        });

        connect(d->ui.savePresetBtn, &QPushButton::clicked, this, [this]() {
            d->SavePreset();
        });

        connect(d->ui.preset1Btn, &QPushButton::clicked, this, [this]() {
            d->MoveToPreset(1);
        });

        connect(d->ui.preset2Btn, &QPushButton::clicked, this, [this]() {
            d->MoveToPreset(2);
        });

        connect(d->ui.preset3Btn, &QPushButton::clicked, this, [this]() {
            d->MoveToPreset(3);
        });

        connect(d->ui.preset4Btn, &QPushButton::clicked, this, [this]() {
            d->MoveToPreset(4);
        });

        connect(d->ui.preset5Btn, &QPushButton::clicked, this, [this]() {
            d->MoveToPreset(5);
        });

        connect(d->motioObserver, &MotionObserver::sigUpdatePosition, this, [this](double xPos, double yPos, double zPos, double cPos) {
            d->ui.xPos->setValue(xPos);
            d->ui.yPos->setValue(yPos);
            d->ui.zPos->setValue(zPos);
            d->ui.cPos->setValue(cPos);
        });

        d->UpdatePresetButton();
    }

    MotionPanel::~MotionPanel() {
    }
}