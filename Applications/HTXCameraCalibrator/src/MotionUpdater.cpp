#include <QList>

#include "MotionObserver.h"
#include "MotionUpdater.h"

namespace TC::HTXCameraCalibrator {
    struct MotionUpdater::Impl {
        QList<MotionObserver*> observers;
    };

    MotionUpdater::MotionUpdater() : d{ new Impl } {
    }

    MotionUpdater::~MotionUpdater() {
    }

    auto MotionUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new MotionUpdater() };
        return theInstance;
    }

    auto MotionUpdater::UpdatePosition(double xPos, double yPos, double zPos, double cPos) -> void {
        for (auto observer : d->observers) {
            observer->UpdatePosition(xPos, yPos, zPos, cPos);
        }
    }

    auto MotionUpdater::Register(MotionObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto MotionUpdater::Deregister(MotionObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}