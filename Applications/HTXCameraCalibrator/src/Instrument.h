#pragma once
#include <memory>

#include <CameraSystem.h>
#include <CameraImage.h>
#include <MCUFactory.h>

namespace TC::HTXCameraCalibrator {
    class Instrument {
    public:
        using Pointer = std::shared_ptr<Instrument>;
        using Axis = TC::MCUControl::Axis;

        struct LiveConfig {
            int32_t sequenceId{ 0 };
            int32_t ledChannel{ 0 };
            int32_t ledIntensity{ 0 };
            int32_t exposureUSec{ 0 };
            int32_t intervalUSec{ 0 };
            int32_t cameraType{ 0 };
            int32_t filter{ 0 };
        };

        struct Position {
            struct {
                double x{ 0 };
                double y{ 0 };
                double z{ 0 };
                double c{ 0 };
            } mm;

            struct {
                int32_t x{ 0 };
                int32_t y{ 0 };
                int32_t z{ 0 };
                int32_t c{ 0 };
            } pulse;
        };

    protected:
        Instrument();

    public:
        ~Instrument();

        static auto GetInstance()->Pointer;
        auto CleanUp()->void;

        auto LoadLiveConfig(LiveConfig& config)->bool;
        auto StoreLiveConfig(const LiveConfig& config)->bool;

        auto GetCameras() const->QList<TC::CameraControl::DeviceInfo>;
        auto InitializeCamera(int32_t cameraIndex) -> bool;
        auto LoadMCUConfiguration() -> bool;
        auto InitializeMCU(bool justOpen, bool& noNeedInitialization) -> bool;
        auto CheckMCUInitialiation(int32_t& progress) -> bool;

        auto GetLatestImage()->TC::CameraControl::Image::Pointer;
        auto CaptureImage(int32_t timeoutSec = 5)->TC::CameraControl::Image::Pointer;

        auto StartLive(const LiveConfig& config)->bool;
        auto StopLive(const LiveConfig& config)->bool;
        auto ResumeLive()->bool;

        auto GetPosition() const->Position;
        auto MoveAxis(Axis axis, double targetMm)->bool;
        auto IsMoving(Axis axis) const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}