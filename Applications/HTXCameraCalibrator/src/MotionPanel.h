#pragma once
#include <memory>

#include <QWidget>

namespace TC::HTXCameraCalibrator {
    class MotionPanel : public QWidget {
        Q_OBJECT

    public:
        explicit MotionPanel(QWidget* parent = nullptr);
        ~MotionPanel() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}