#define LOGGER_TAG "[RotationMeasurement]"

#include <QFile>
#include <QSettings>
#include <QCoreApplication>
#include <QElapsedTimer>

#include <TCLogger.h>
#include <TCPython.h>

#include "MotionUpdater.h"
#include "Instrument.h"
#include "Settings.h"
#include "RotationMeasurePanelControl.h"

namespace TC::HTXCameraCalibrator {
    using Axis = Instrument::Axis;

    struct RotationMeasurePanelControl::Impl {
        TC::PythonModule::Pointer py{ TC::PythonModule::GetInstance() };
        struct {
            const QString measure{ "measureRotation.py" };
            const QString stitch{ "stitchImageSideBySide.py" };
        } scripts;
        struct {
            QString measure;
            QString stitch;
        } fullPath;;

        struct {
            Image::Pointer imageLeft;
            Image::Pointer imageRight;
            double distValue{ 0 };
            double overlap{ 0 };
        } result;
        
        auto SetupScriptPath()->void;

        auto MoveAxis(Axis axis, double targetMm, int32_t timeOutSec = 60)->void;
    };

    auto RotationMeasurePanelControl::Impl::SetupScriptPath() -> void {
        fullPath.measure = QString("%1/pyscript/%2").arg(qApp->applicationDirPath()).arg(scripts.measure);
        fullPath.stitch = QString("%1/pyscript/%2").arg(qApp->applicationDirPath()).arg(scripts.stitch);
        if (QFile::exists(fullPath.measure) && QFile::exists(fullPath.stitch)) return;

        fullPath.measure = QString("%1/%2").arg(_PYSCRIPT_DIR).arg(scripts.measure);
        fullPath.stitch = QString("%1/%2").arg(_PYSCRIPT_DIR).arg(scripts.stitch);
        if (QFile::exists(fullPath.measure) && QFile::exists(fullPath.stitch)) return;

        fullPath.measure.clear();
        fullPath.stitch.clear();
    }

    auto RotationMeasurePanelControl::Impl::MoveAxis(Axis axis, double targetMm, int32_t timeOutSec) -> void {
        auto instrument = Instrument::GetInstance();

        QElapsedTimer timer;

        timer.start();
        if (!instrument->MoveAxis(axis, targetMm)) return;

        do {
            auto pos = instrument->GetPosition();
            MotionUpdater::GetInstance()->UpdatePosition(pos.mm.x, pos.mm.y, pos.mm.z, pos.mm.c);

            QCoreApplication::processEvents(QEventLoop::AllEvents, 200);

            if (!instrument->IsMoving(axis)) break;
        } while (timer.elapsed() < (timeOutSec * 1000));

        auto pos = instrument->GetPosition();
        MotionUpdater::GetInstance()->UpdatePosition(pos.mm.x, pos.mm.y, pos.mm.z, pos.mm.c);
    }

    RotationMeasurePanelControl::RotationMeasurePanelControl() : d{ new Impl } {
        d->SetupScriptPath();

        d->py->SetPythonHome("C:\\TomoPython");
        d->py->InitPython();
        d->py->ExecuteString(QString("sys.path.insert(0, '%1\\pyscript')").arg(qApp->applicationDirPath()));
        d->py->ExecuteString(QString("sys.path.insert(0, '%1')").arg(_PYSCRIPT_DIR));
    }

    RotationMeasurePanelControl::~RotationMeasurePanelControl() {
    }

    auto RotationMeasurePanelControl::StorePosition() -> void {
        auto instrument = Instrument::GetInstance();
        const auto position = instrument->GetPosition();

        QSettings qs(Settings::GetRotationCalibrationPath(), QSettings::IniFormat);
        qs.setValue("Position/X", position.mm.x);
        qs.setValue("Position/Y", position.mm.y);
        qs.setValue("Position/Z", position.mm.z);
        qs.setValue("Position/C", position.mm.c);
    }

    auto RotationMeasurePanelControl::RestorePosition() -> std::tuple<bool, double, double, double, double> {
        const auto xKey = "Position/X";
        const auto yKey = "Position/Y";
        const auto zKey = "Position/Z";
        const auto cKey = "Position/C";

        QSettings qs(Settings::GetRotationCalibrationPath(), QSettings::IniFormat);

        if (!qs.contains(xKey)) return std::make_tuple(false, 0, 0, 0, 0);
        if (!qs.contains(yKey)) return std::make_tuple(false, 0, 0, 0, 0);
        if (!qs.contains(zKey)) return std::make_tuple(false, 0, 0, 0, 0);
        if (!qs.contains(cKey)) return std::make_tuple(false, 0, 0, 0, 0);

        return std::make_tuple(true,
            qs.value(xKey).toDouble(),
            qs.value(yKey).toDouble(),
            qs.value(zKey).toDouble(),
            qs.value(cKey).toDouble());
    }

    auto RotationMeasurePanelControl::StoreParameters(double fov, int32_t distance, int32_t pause)->void {
        QSettings qs(Settings::GetRotationCalibrationPath(), QSettings::IniFormat);
        qs.setValue("Parameter/FOV", fov);
        qs.setValue("Parameter/Distance", distance);
        qs.setValue("Parameter/Pause", pause);
    }

    auto RotationMeasurePanelControl::RestoreParameters()->std::tuple<double, int32_t, int32_t> {
        QSettings qs(Settings::GetRotationCalibrationPath(), QSettings::IniFormat);
        const auto fov = qs.value("Parameter/FOV", 217.8).toDouble();
        const auto distance = qs.value("Parameter/Distance", 150).toInt();
        const auto pause = qs.value("Parameter/Pause", 1).toInt();
        return std::make_tuple(fov, distance, pause);
    }

    auto RotationMeasurePanelControl::Move2Start() -> bool {
        const auto [res, xPos, yPos, zPos, cPos] = RestorePosition();
        if (!res) return false;
        
        QLOG_INFO() << "Move to #1 (X=" << xPos << ", Y=" << yPos << ", Z=" << zPos << ", C=" << cPos << ")";

        d->MoveAxis(Axis::X, xPos);
        d->MoveAxis(Axis::Y, yPos);
        d->MoveAxis(Axis::Z, zPos);
        d->MoveAxis(Axis::U, cPos);

        return true;
    }

    auto RotationMeasurePanelControl::Move2Left() -> bool {
        const auto [res, xPos, yPos, zPos, cPos] = RestorePosition();
        if (!res) return false;

        QLOG_INFO() << "Move to the left (" << xPos << ")";

        d->MoveAxis(Axis::X, xPos);

        return true;
    }

    auto RotationMeasurePanelControl::Move2Right() -> bool {
        const auto [res, xPos, yPos, zPos, cPos] = RestorePosition();
        if (!res) return false;

        const auto [fov, distance, pause] = RestoreParameters();

        QLOG_INFO() << "Move to the right (" << xPos + distance/1000.0 << ")";

        d->MoveAxis(Axis::X, xPos + distance/1000.0);

        return true;
    }

    auto RotationMeasurePanelControl::GetImage()->Image::Pointer {
        auto instrument = Instrument::GetInstance();
        return instrument->CaptureImage(5);
    }

    auto RotationMeasurePanelControl::Measure(Image::Pointer image1, Image::Pointer image2)->double {
        auto sizeX = image1->GetWidth();
        auto sizeY = image1->GetHeight();
        uint32_t dims[] = { 1, sizeY, sizeX };
        
        const auto [fov, distance, pause] = RestoreParameters();
        const double overlapUm = std::abs(distance - fov);
        const auto overlapPx = static_cast<int32_t>((sizeX * 1.0) / fov * overlapUm);
        QLOG_INFO() << "Measure the rotation (overlap=" << overlapPx << " pixels, " << overlapUm << "um)";

        double result{ 0 };

        d->result.imageLeft.reset();
        d->result.imageRight.reset();
        d->result.distValue = 0;
        d->result.overlap = 0;

        try {
            d->py->ExecuteString(QString("overlap = %1;").arg(overlapPx));
            d->py->CopyArrToPy(image1->GetData().get(), 3, dims, "imageLeft", TC::ArrType::arrUBYTE, TC::ArrType::arrUBYTE);
            d->py->CopyArrToPy(image2->GetData().get(), 3, dims, "imageRight", TC::ArrType::arrUBYTE, TC::ArrType::arrUBYTE);

            if (!d->py->ExecutePyFile(d->fullPath.measure)) {
                QLOG_ERROR() << "It fails to run the script : " << d->scripts.measure;
                return 0;
            }

            std::any distValue;
            auto [distDim, distType] = d->py->CopyArrToCpp(distValue, "distValue");
            result = std::any_cast<std::shared_ptr<double[]>>(distValue)[0];

            d->result.imageLeft = image1;
            d->result.imageRight = image2;
            d->result.distValue = result;
            d->result.overlap = overlapPx;

            QLOG_INFO() << "Displacement in Y = " << result << " pixels";
        } catch(std::exception& ex) {
            QLOG_ERROR() << ex.what();
        } catch (...) {
            QLOG_ERROR() << "Exception while loading python module or running python scrip";
        }

        return result;
    }

    auto RotationMeasurePanelControl::IsReadyToSave() const -> bool {
        return (d->result.imageLeft != nullptr) && (d->result.imageRight != nullptr);
    }

    auto RotationMeasurePanelControl::SaveMergedImage(const QString& path) -> bool {
        auto sizeX = d->result.imageLeft->GetWidth();
        auto sizeY = d->result.imageLeft->GetHeight();
        uint32_t dims[] = { 1, sizeY, sizeX };

        try {
            d->py->ExecuteString(QString("overlap = %1;").arg(d->result.overlap));
            d->py->ExecuteString(QString("outputPath = '%1';").arg(path));
            d->py->CopyArrToPy(d->result.imageLeft->GetData().get(), 3, dims, "imageLeft", TC::ArrType::arrUBYTE, TC::ArrType::arrUBYTE);
            d->py->CopyArrToPy(d->result.imageRight->GetData().get(), 3, dims, "imageRight", TC::ArrType::arrUBYTE, TC::ArrType::arrUBYTE);

            if (!d->py->ExecutePyFile(d->fullPath.stitch)) {
                QLOG_ERROR() << "It fails to run the script : " << d->scripts.stitch;
                return false;
            }
        } catch (std::exception& ex) {
            QLOG_ERROR() << ex.what();
            return false;
        } catch (...) {
            QLOG_ERROR() << "Exception while loading python module or running python scrip";
            return false;
        }


        return true;
    }
}