#include <QFile>
#include <QSettings>

#include "Settings.h"
#include "Instrument.h"
#include "LivePanelControl.h"

namespace TC::HTXCameraCalibrator {
    struct LivePanelControl::Impl {
    };

    LivePanelControl::LivePanelControl() : d{ new Impl } {
    }

    LivePanelControl::~LivePanelControl() {
    }

    auto LivePanelControl::LoadConfig(Config& config) -> bool {
        auto instrument = Instrument::GetInstance();
        return instrument->LoadLiveConfig(config);
    }

    auto LivePanelControl::StoreConfig(const Config& config) -> bool {
        auto instrument = Instrument::GetInstance();
        return instrument->StoreLiveConfig(config);
    }

    auto LivePanelControl::Apply(const Config& config) -> bool {
        auto instrument = Instrument::GetInstance();

        if (!StoreConfig(config)) return false;

        return instrument->StartLive(config);
    }
}