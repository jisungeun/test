#pragma once
#include <memory>

#include <QWidget>

namespace TC::HTXCameraCalibrator {
    class RotationMeasurePanel : public QWidget {
        Q_OBJECT

    public:
        explicit RotationMeasurePanel(QWidget* parent = nullptr);
        ~RotationMeasurePanel() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}