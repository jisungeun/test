#pragma once
#include <memory>

namespace TC::HTXCameraCalibrator {
    class MotionObserver;

    class MotionUpdater {
    public:
        using Pointer = std::shared_ptr<MotionUpdater>;

    protected:
        MotionUpdater();

    public:
        ~MotionUpdater();
        static auto GetInstance()->Pointer;

        auto UpdatePosition(double xPos, double yPos, double zPos, double cPos)->void;

    protected:
        auto Register(MotionObserver* observer)->void;
        auto Deregister(MotionObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;


        friend class MotionObserver;
    };
}