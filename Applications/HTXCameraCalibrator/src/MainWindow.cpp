#define LOGGER_TAG "[UI]"
#include <QMessageBox>
#include <QTimer>

#include <TCLogger.h>

#include "Settings.h"
#include "ImageConverter.h"
#include "CameraSelectionDialog.h"
#include "MainWindow.h"
#include "Version.h"
#include "ui_MainWindow.h"
#include "MainWindowControl.h"

namespace TC::HTXCameraCalibrator {
    struct MainWindow::Impl {
        MainWindow* p{ nullptr };
        Ui::MainWindow ui;
        MainWindowControl control;
        QTimer initTimer;
        QTimer liveTimer;

        Impl(MainWindow* p) : p{ p } {}

        auto Initialize() -> bool;
        auto InitializeCamera() -> bool;
        auto InitializeMCU() -> bool;
        auto UpdateInitProgress() -> void;
        auto Initialized() -> void;
    };

    auto MainWindow::Impl::Initialize() -> bool {
        if (!InitializeCamera()) return false;
        if (!InitializeMCU()) return false;

        return true;
    }

    auto MainWindow::Impl::InitializeCamera() -> bool {
        CameraSelectionDialog selectionDialog;
        const auto& deviceInfos = control.GetCameras();
        if (deviceInfos.isEmpty()) {
            QMessageBox::warning(p, "Initialization", "No camera is found");
            return false;
        }

        for (const auto& info : deviceInfos) {
            selectionDialog.AddCamera(info);
        }
        selectionDialog.exec();

        const auto cameraIdx = selectionDialog.GetSelectedDevice();
        if (!control.InitializeCamera(cameraIdx)) {
            QMessageBox::warning(p, "Initialization", "Camera initialization is failed");
            return false;
        }

        return true;
    }

    auto MainWindow::Impl::InitializeMCU() -> bool {
        if (!control.LoadMCUConfiguration()) {
            QMessageBox::warning(p, "Intialization", 
                QString("MCU Configuration fils is not loaded. %1").arg(Settings::GetMCUConfigPath()));
            return false;
        }

        bool justOpen = ui.noMCUInit->isChecked();

        bool noNeedInitialization{ false };
        if (!control.InitializeMCU(justOpen, noNeedInitialization)) {
            QMessageBox::warning(p, "Initialization", tr("Failed to initialize MCU"));
            return false;
        }

        if (noNeedInitialization) {
            Initialized();
        } else {
            ui.initProgressBar->show();
            ui.initProgressBar->setValue(0);
            initTimer.start(50);
        }

        return true;
    }

    auto MainWindow::Impl::UpdateInitProgress() -> void {
        int32_t progress = 0;
        if (!control.CheckMCUInitialiation(progress)) return;

        ui.initProgressBar->setValue(progress);
        if (progress == 100) {
            initTimer.stop();
            Initialized();
        }
    }

    auto MainWindow::Impl::Initialized() -> void {
        ui.initProgressBar->hide();
        ui.initBtn->setDisabled(true);

        ui.controlTab->setEnabled(true);
        ui.calibrationTab->setEnabled(true);

        control.StartLive();
        liveTimer.start();
    }

    MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{ new Impl(this) } {
        d->ui.setupUi(this);

        setWindowTitle(QString("HTX Camera Calibrator").arg(PROJECT_REVISION));

        d->ui.initProgressBar->setValue(0);
        d->ui.initProgressBar->hide();
        d->ui.controlTab->setCurrentIndex(0);
        d->ui.calibrationTab->setCurrentIndex(0);
        d->ui.controlTab->setDisabled(true);
        d->ui.calibrationTab->setDisabled(true);

        connect(d->ui.action_Quit, &QAction::triggered, qApp, &QApplication::quit);

        connect(&d->initTimer, &QTimer::timeout, this, [this]() {
            d->UpdateInitProgress();
        });

        connect(&d->liveTimer, &QTimer::timeout, this, [this]() {
            auto image = d->control.GetLatestImage();
            if (image.get()) {
                d->ui.imageView->ShowImage(ImageConverter::Image2QImage(image));
            }
        });

        connect(d->ui.initBtn, &QPushButton::clicked, this, [this]() {
            d->Initialize();
        });
    }

    MainWindow::~MainWindow() {
        d->liveTimer.stop();
    }
}