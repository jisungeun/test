#include "ImageConverter.h"

namespace TC::HTXCameraCalibrator {
    struct ImageConverter::Impl {
        QVector<QRgb> grayTable;
    };

    ImageConverter::ImageConverter() : d{ new Impl } {
        for (int i = 0; i < 256; i++) {
            d->grayTable.push_back(qRgb(i, i, i));
        }
    }

    ImageConverter::~ImageConverter() {
    }

    auto ImageConverter::GetInstance() -> Pointer {
        static Pointer theInstance{ new ImageConverter() };
        return theInstance;
    }

    auto ImageConverter::Image2QImage(TC::CameraControl::Image::Pointer& image) -> QImage {
        QImage qImage(image->GetData().get(), image->GetWidth(), image->GetHeight(), QImage::Format_Indexed8);
        qImage.setColorTable(GetInstance()->d->grayTable);
        return qImage;
    }
}
