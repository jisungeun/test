#pragma once
#include <memory>

#include <QWidget>

namespace TC::HTXCameraCalibrator {
    class LivePanel : public QWidget {
        Q_OBJECT

    public:
        explicit LivePanel(QWidget* parent = nullptr);
        ~LivePanel() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}