#define LOGGER_TAG "[RotationMeasurement]"
#include <QVector>
#include <QFileDialog>
#include <QMessageBox>
#include <QFileInfo>

#include <TCLogger.h>

#include "RotationMeasurePanel.h"
#include "RotationMeasurePanelControl.h"
#include "ui_RotationMeasurePanel.h"

namespace TC::HTXCameraCalibrator {
    struct RotationMeasurePanel::Impl {
        RotationMeasurePanel* p{ nullptr };
        Ui::RotationMeasurePanel ui;
        RotationMeasurePanelControl control;
        bool stopRequested{ false };

        Impl(RotationMeasurePanel* p) : p{ p } { }
        auto UpdateButtons()->void;
        auto Start()->void;
        auto Stop()->void;
        auto SaveMergedImage()->void;
    };

    auto RotationMeasurePanel::Impl::Start() -> void {
        stopRequested = false;
        ui.errorPixels->setValue(0);
        ui.startBtn->setDisabled(true);
        ui.pauseTime->setDisabled(true);
        ui.stopBtn->setEnabled(true);
        ui.saveImageBtn->setEnabled(false);

        control.StoreParameters(ui.fovUm->value(), ui.distanceUm->value(), ui.pauseTime->value());

        QLOG_INFO() << "Start to measure rotation";

        const auto pauseMSec = ui.pauseTime->value() * 1000;

        control.Move2Start();

        do {
            control.Move2Left();
            if (stopRequested) break;

            auto image1 = control.GetImage();

            if (pauseMSec > 0) {
                QCoreApplication::processEvents(QEventLoop::AllEvents, pauseMSec);
            }

            control.Move2Right();
            if (stopRequested) break;

            auto image2 = control.GetImage();

            auto rotation = control.Measure(image1, image2);
            ui.errorPixels->setValue(rotation);

            if (pauseMSec > 0) {
                QCoreApplication::processEvents(QEventLoop::AllEvents, pauseMSec);
            }
        } while (!stopRequested);
    }

    auto RotationMeasurePanel::Impl::Stop() -> void {
        stopRequested = true;
        ui.startBtn->setEnabled(true);
        ui.pauseTime->setEnabled(true);
        ui.stopBtn->setDisabled(true);
        ui.saveImageBtn->setEnabled(control.IsReadyToSave());
    }

    auto RotationMeasurePanel::Impl::UpdateButtons() -> void {
        const auto res = control.RestorePosition();
        const auto hasValue = std::get<0>(res);
        if (hasValue) {
            ui.goBtn->setEnabled(true);
            ui.startBtn->setEnabled(true);
            ui.fovUm->setEnabled(true);
            ui.pauseTime->setEnabled(true);
            ui.stopBtn->setDisabled(true);
        } else {
            ui.goBtn->setDisabled(true);
            ui.startBtn->setDisabled(true);
            ui.fovUm->setDisabled(true);
            ui.pauseTime->setDisabled(true);
            ui.stopBtn->setDisabled(true);
        }

        ui.saveImageBtn->setEnabled(control.IsReadyToSave());
    }

    auto RotationMeasurePanel::Impl::SaveMergedImage() -> void {
        static QString lastPath;
        auto path = QFileDialog::getSaveFileName(p, "Select a image file", lastPath, "PNG (*.png)");
        if (path.isEmpty()) return;

        lastPath = QFileInfo(path).absolutePath();
        if (!control.SaveMergedImage(path)) {
            QMessageBox::warning(p, "Saving Stitched Image", "It fails to save the stitched image to a file");
        }
    }

    RotationMeasurePanel::RotationMeasurePanel(QWidget* parent) : QWidget(parent), d{ new Impl(this) } {
        d->ui.setupUi(this);

        const auto [fov, distance, pause] = d->control.RestoreParameters();
        d->ui.fovUm->setValue(fov);
        d->ui.distanceUm->setValue(distance);
        d->ui.pauseTime->setValue(pause);

        connect(d->ui.setBtn, &QPushButton::clicked, this, [this]() {
            d->control.StorePosition();
            d->UpdateButtons();
        });

        connect(d->ui.goBtn, &QPushButton::clicked, this, [this]() {
            setDisabled(true);
            d->control.Move2Start();
            setDisabled(false);
        });

        connect(d->ui.startBtn, &QPushButton::clicked, this, [this]() {
            d->Start();
        });

        connect(d->ui.stopBtn, &QPushButton::clicked, this, [this]() {
            d->Stop();
        });

        connect(d->ui.saveImageBtn, &QPushButton::clicked, this, [this]() {
            d->SaveMergedImage();
        });

        d->UpdateButtons();
    }

    RotationMeasurePanel::~RotationMeasurePanel() {
    }
}