#pragma once
#include <memory>

#include <QObject>

namespace TC::HTXCameraCalibrator {
    class MotionObserver : public QObject {
        Q_OBJECT

    public:
        MotionObserver(QObject* parent = nullptr);
        ~MotionObserver() override;

        auto UpdatePosition(double xPos, double yPos, double zPos, double cPos)->void;

    signals:
        void sigUpdatePosition(double xPos, double yPos, double zPos, double cPos);
    };
}