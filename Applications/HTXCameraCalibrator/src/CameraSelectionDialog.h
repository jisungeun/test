#pragma once
#include <memory>
#include <QDialog>

#include <CameraSystem.h>

namespace TC::HTXCameraCalibrator {
    class CameraSelectionDialog : public QDialog {
        Q_OBJECT

    public:
        CameraSelectionDialog(QWidget* parent = nullptr);
        ~CameraSelectionDialog() override;

        auto AddCamera(const TC::CameraControl::DeviceInfo& info)->void;
        auto GetSelectedDevice() const->int32_t;

    protected:
        void closeEvent(QCloseEvent* event) override;

    protected slots:
        void accept();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}