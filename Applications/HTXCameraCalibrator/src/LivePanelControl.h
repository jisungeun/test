#pragma once
#include <memory>

#include "Instrument.h"

namespace TC::HTXCameraCalibrator {
    class LivePanelControl {
    public:
        using Config = Instrument::LiveConfig;

    public:
        LivePanelControl();
        ~LivePanelControl();

        auto LoadConfig(Config& config)->bool;
        auto StoreConfig(const Config& config)->bool;

        auto Apply(const Config& config)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}