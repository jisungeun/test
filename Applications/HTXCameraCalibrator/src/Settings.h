#pragma once
#include <memory>

#include <QVariant>
#include <QString>

namespace TC::HTXCameraCalibrator {
    class Settings {
    public:
        typedef std::shared_ptr<Settings> Pointer;

    public:
        Settings();
        virtual ~Settings();

        static auto GetInstance()->Pointer;

        auto GetValue(const QString& key) const->QVariant;

        static auto GetMCUConfigPath()->QString;
        static auto GetLiveConfigPath()->QString;
        static auto GetMotionPresetPath()->QString;
        static auto GetRotationCalibrationPath()->QString;
    };
}