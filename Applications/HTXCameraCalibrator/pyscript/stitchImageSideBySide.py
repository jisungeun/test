# -*- coding: utf-8 -*-
def stitchImageSideBySide():
    import numpy as np
    from PIL import Image

    global imageLeft
    global imageRight
    global stitched_image
    global outputPath
    global overlap
    
    overlap = int(round(overlap));
    
    imageLeft = np.squeeze(imageLeft);
    imageRight = np.squeeze(imageRight);
    
    min_height = min(imageLeft.shape[0], imageRight.shape[0])
    imageLeft = imageLeft[:min_height, :]
    imageRight = imageRight[:min_height, :]
    
    stitched_image = np.concatenate((imageLeft, imageRight[:,overlap:]), axis=1).astype(np.uint8);
    stitched_image = Image.fromarray(stitched_image, mode='L')
    stitched_image.save(outputPath);
    
    
def test(topDir):
    # -*- coding: utf-8 -*-
    import numpy as np
    import matplotlib.pyplot as plt
    from PIL import Image
    import glob
    
    global imageLeft
    global imageRight
    global stitched_image
    global outputPath
    global overlap
    
    overlap = 50;
    outputPath = 'e:/temp/test.png';
    files = glob.glob(topDir + '/*.png')

    imageLeft = np.array(Image.open(files[0]));
    imageRight = np.array(Image.open(files[1]));

    stitchImageSideBySide();
    
    plt.imshow(stitched_image);
    plt.colorbar();
    plt.show();

if __name__ == '__main__':
    stitchImageSideBySide()
    #test('E:/Git Repo/TSS-HTXAppEntity/Applications/HTXCameraCalibrator/test/simulation')