# -*- coding: utf-8 -*-
def measureRotation():
    import numpy as np
    import matplotlib.pyplot as plt

    global distValue
    global overlap
    global imageLeft
    global imageRight
    
    overlap = int(round(overlap));
    print(overlap);

    imageLeft = np.squeeze(imageLeft);
    imageRight = np.squeeze(imageRight);

    distValue = np.zeros(1, dtype=np.float64);

    image_height = imageLeft.shape[0];
    image_width = imageLeft.shape[1];

    subLeft = imageLeft[:, image_width-overlap:].astype(float);
    subRight = imageRight[:, :overlap].astype(float);

    subLeft = np.max(subLeft) - subLeft;
    subRight = np.max(subRight) - subRight;
    
    #plt.figure;
    #plt.subplot(121);
    #plt.imshow(subLeft);
    #plt.subplot(122);
    #plt.imshow(subRight);
    #plt.show();

    maxIndexes = np.array([0] * overlap);
    for col in range(overlap):
        corr = np.correlate(subLeft[:,col], subRight[:,col], mode='full');
        maxIdx = np.argmax(corr);
        maxIndexes[col] = maxIdx;
    
    #plt.figure;
    #plt.plot(maxIndexes);
    #plt.show();
    
    distValue[0] = np.mean(maxIndexes) - image_height;
    
    print(maxIndexes);
    print(distValue);
    
    
    
def test(topDir):
    # -*- coding: utf-8 -*-
    import numpy as np
    import matplotlib.pyplot as plt
    import glob
    
    global imageLeft
    global imageRight
    global overlap

    overlap = 69;

    fileIdx = 0
    files = glob.glob(topDir + '/*.png')

    stacks = int(len(files)/2)

    for index in range(stacks):
        imageLeft = plt.imread(files[index*2 + 0]);
        imageRight = plt.imread(files[index*2 + 1]);

        measureRotation();

if __name__ == '__main__':
    measureRotation()
    #test('E:/Git Repo/TSS-HTXAppEntity/Applications/HTXCameraCalibrator/test/simulation')
    #test('E:/Git Repo/TSS-HTXAppEntity/Applications/HTXCameraCalibrator/test/real/T1_672Pulse')