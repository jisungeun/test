#pragma once

#include "IVolume.h"

#include "CellAnalyzer.DataModelExport.h"

namespace CellAnalyzer {
	struct Size2D {
		int x = 0;
		int y = 0;
	};

	struct Resolution2D {
		double x = 0.0;
		double y = 0.0;
	};

	using Origin2D = Resolution2D;

	class CellAnalyzer_DataModel_API IVolume2D : public IVolume {
	public:
		virtual auto GetSize() const -> Size2D = 0;
		virtual auto GetResolution() const -> Resolution2D = 0;
		virtual auto GetOrigin() const -> Origin2D = 0;
	};
}
