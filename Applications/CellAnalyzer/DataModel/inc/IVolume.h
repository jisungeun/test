#pragma once

#include <QVector>

#include "IService.h"

#include "CellAnalyzer.DataModelExport.h"

namespace CellAnalyzer {
	enum class DataType {
		UInt8,
		UInt16,
		Float32,
		Float64
	};

	class CellAnalyzer_DataModel_API IVolume : public virtual Tomocube::IService {
	public:
		virtual auto GetDataType() const -> DataType = 0;
		virtual auto GetDataSize() const -> uint64_t = 0;
		virtual auto GetData() const -> void* = 0;

		template <typename T>
		auto GetDataAs() const -> T;
	};

	template <typename T>
	auto IVolume::GetDataAs() const -> T {
		return reinterpret_cast<T>(GetData());
	}
}
