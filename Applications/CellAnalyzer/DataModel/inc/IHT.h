#pragma once

#include "IService.h"

#include "CellAnalyzer.DataModelExport.h"

namespace CellAnalyzer {
	struct RIRange {
		double min = 0;
		double max = 0;
	};

	class CellAnalyzer_DataModel_API IHT : public virtual Tomocube::IService {
	public:
		virtual auto GetRI() const -> RIRange = 0;
	};
}
