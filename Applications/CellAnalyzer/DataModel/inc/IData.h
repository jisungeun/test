#pragma once

#include <memory>

#include <QStringList>

#include "IService.h"

#include "CellAnalyzer.DataModelExport.h"

namespace CellAnalyzer {
	class IData;
	using DataPtr = std::shared_ptr<IData>;
	using DataList = QVector<DataPtr>;
	using DataMap = QMap<QString, DataPtr>;

	enum class DataFlag {
		Null = 0x0000,
		Volume2D = 0x0001,
		Volume3D = 0x0002,
		HT = 0x0004,
		FL = 0x0008,
		BF = 0x0010,
		Binary = 0x0020,
		Label = 0x0040,
		Measure = 0x0080,
		Unknown = 0x0100,

		Volume = Volume2D | Volume3D,
		TCF = HT | FL | BF,
		Mask = Binary | Label,
		All = 0xFFFF
	};

	Q_DECLARE_FLAGS(DataFlags, DataFlag)
	Q_DECLARE_OPERATORS_FOR_FLAGS(DataFlags)

	class CellAnalyzer_DataModel_API IData : public virtual Tomocube::IService {
	public:
		virtual auto GetFlags() const -> DataFlags = 0;
		virtual auto GetTimeStep() const -> int = 0;
	};

	static auto ToString(DataFlag flag) -> QString {
		switch (flag) {
			case DataFlag::Null:
				return "Null";
			case DataFlag::Volume2D:
				return "Volume2D";
			case DataFlag::Volume3D:
				return "Volume3D";
			case DataFlag::HT:
				return "HT";
			case DataFlag::FL:
				return "FL";
			case DataFlag::BF:
				return "BF";
			case DataFlag::Binary:
				return "Binary";
			case DataFlag::Label:
				return "Label";
			case DataFlag::Measure:
				return "Measure";
			case DataFlag::Unknown:
				return "Unknown";
			case DataFlag::Volume:
				return "Volume";
			case DataFlag::TCF:
				return "TCF";
			case DataFlag::Mask:
				return "Mask";
			case DataFlag::All:
				return "All";
			default:
				return {};
		}
	}

	static auto ToStringList(DataFlags flags) -> QStringList {
		QStringList list;

		for (const auto f : { DataFlag::Volume2D, DataFlag::Volume3D, DataFlag::HT, DataFlag::FL, DataFlag::BF, DataFlag::Binary, DataFlag::Label, DataFlag::Measure, DataFlag::Unknown }) {
			if (flags.testFlag(f))
				list.push_back(ToString(f));
		}

		return list;
	}

	static auto ToDataFlag(const QString& flag) -> DataFlag {
		if (flag == "Null")
			return DataFlag::Null;
		if (flag == "Volume2D")
			return DataFlag::Volume2D;
		if (flag == "Volume3D")
			return DataFlag::Volume3D;
		if (flag == "HT")
			return DataFlag::HT;
		if (flag == "FL")
			return DataFlag::FL;
		if (flag == "BF")
			return DataFlag::BF;
		if (flag == "Binary")
			return DataFlag::Binary;
		if (flag == "Label")
			return DataFlag::Label;
		if (flag == "Measure")
			return DataFlag::Measure;
		if (flag == "Volume")
			return DataFlag::Volume;
		if (flag == "TCF")
			return DataFlag::TCF;
		if (flag == "Mask")
			return DataFlag::Mask;
		if (flag == "All")
			return DataFlag::All;
		if (flag == "Unknown")
			return DataFlag::Unknown;

		return DataFlag::Null;
	}

	static auto ToDataFlags(const QStringList& flags) -> DataFlags {
		DataFlags f = DataFlag::Null;

		for (const auto& i : flags) {
			if (i == "Volume2D")
				f = f | DataFlag::Volume2D;
			else if (i == "Volume3D")
				f = f | DataFlag::Volume3D;
			else if (i == "HT")
				f = f | DataFlag::HT;
			else if (i == "FL")
				f = f | DataFlag::FL;
			else if (i == "BF")
				f = f | DataFlag::BF;
			else if (i == "Binary")
				f = f | DataFlag::Binary;
			else if (i == "Label")
				f = f | DataFlag::Label;
			else if (i == "Measure")
				f = f | DataFlag::Measure;
			else if (i == "Volume")
				f = f | DataFlag::Volume;
			else if (i == "TCF")
				f = f | DataFlag::TCF;
			else if (i == "Mask")
				f = f | DataFlag::Mask;
			else if (i == "All")
				f = f | DataFlag::All;
			else if (i == "Unknown")
				f = f | DataFlag::Unknown;
		}

		return f;
	}

	static auto ToIcon(DataFlags flags) -> QString {
		return {};
	}
}
