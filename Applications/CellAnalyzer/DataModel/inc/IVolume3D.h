#pragma once

#include "IVolume.h"

#include "CellAnalyzer.DataModelExport.h"

namespace CellAnalyzer {
	struct Size3D {
		int x = 0;
		int y = 0;
		int z = 0;
	};

	struct Resolution3D {
		double x = 0.0;
		double y = 0.0;
		double z = 0.0;
	};

	using Origin3D = Resolution3D;

	class CellAnalyzer_DataModel_API IVolume3D : public IVolume {
	public:
		virtual auto GetSize() const -> Size3D = 0;
		virtual auto GetResolution() const -> Resolution3D = 0;
		virtual auto GetOrigin() const -> Origin3D = 0;
		virtual auto GetZOffset() const -> double = 0;
	};
}
