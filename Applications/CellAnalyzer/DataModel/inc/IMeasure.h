#pragma once

#include <QVariantMap>

#include "IService.h"

#include "CellAnalyzer.DataModelExport.h"

namespace CellAnalyzer {
	class CellAnalyzer_DataModel_API IMeasure : public virtual Tomocube::IService {
	public:
		virtual auto GetKeys() const -> QStringList = 0;
		virtual auto GetValue(const QString& key) const -> QVariant = 0;

		virtual auto GetMap() const -> QVariantMap = 0;
	};
}
