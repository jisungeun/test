#pragma once

#include <QString>

#include "IService.h"

#include "CellAnalyzer.DataModelExport.h"

namespace CellAnalyzer {
	struct FLColor {
		uint8_t red = 0;
		uint8_t green = 0;
		uint8_t blue = 0;
	};

	struct FLIntensity {
		uint16_t min = 0;
		uint16_t max = 0;
	};

	class CellAnalyzer_DataModel_API IFL : public virtual Tomocube::IService {
	public:
		virtual auto GetIntensity() const -> FLIntensity = 0;

		virtual auto GetChannelIndex() const -> int = 0;
		virtual auto GetChannelName() const -> QString = 0;
		virtual auto GetChannelColor() const -> FLColor = 0;

		virtual auto GetChannelEmission() const -> double = 0;
		virtual auto GetChannelExcitation() const -> double = 0;
		virtual auto GetChannelExposureTime() const -> int = 0;
	};
}
