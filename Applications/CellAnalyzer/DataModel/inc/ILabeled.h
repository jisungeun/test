#pragma once

#include "IService.h"

#include "CellAnalyzer.DataModelExport.h"

namespace CellAnalyzer {
	class CellAnalyzer_DataModel_API ILabeled : public virtual Tomocube::IService {
	public:
		virtual auto GetMaxIndex() const -> int = 0;
	};
}
