#pragma once

#include "IHTReader.h"

#include "CellAnalyzer.IO.File.H5TCFExport.h"

namespace CellAnalyzer::IO::File {
	class CellAnalyzer_IO_File_H5TCF_API HTReader final : public IHTReader {
	public:
		explicit HTReader(const QString& filename);
		~HTReader() override;

		auto GetDataCount() const -> int override;
		auto GetResolution() const -> Resolution3D override;
		auto GetSize() const -> Size3D override;
		auto GetRange() const -> DataRange override;

		auto Read(int index) const -> HTImage3D override;
		auto ReadMip(int index) const -> HTImage2D override;
		auto ReadThumbnail(int index) const -> HTThumbnail override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
