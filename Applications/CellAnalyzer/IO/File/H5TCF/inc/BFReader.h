#pragma once

#include "IBFReader.h"

#include "CellAnalyzer.IO.File.H5TCFExport.h"

namespace CellAnalyzer::IO::File {
	class CellAnalyzer_IO_File_H5TCF_API BFReader final : public IBFReader {
	public:
		explicit BFReader(const QString& filename);
		~BFReader() override;

		auto GetDataCount() const -> int override;
		auto GetResolution() const -> Resolution2D override;
		auto GetSize() const -> Size2D override;

		auto Read(int index) const -> BFImage override;
		auto ReadThumbnail(int index) const -> BFImage override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
