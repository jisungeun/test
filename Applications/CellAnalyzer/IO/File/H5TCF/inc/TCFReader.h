#pragma once

#include "ITCFReader.h"

#include "CellAnalyzer.IO.File.H5TCFExport.h"

namespace CellAnalyzer::IO::File {
	class CellAnalyzer_IO_File_H5TCF_API TCFReader final : public CellAnalyzer::IO::ITCFReader {
	public:
		explicit TCFReader(const QString& filename);
		~TCFReader() override;

		auto GetUrl() const -> QString override;
		auto IsReadable() const -> bool override;

		auto GetCreationDateTime() const -> QDateTime override;
		auto GetDataID() const -> QString override;
		auto GetDescription() const -> QString override;
		auto GetDeviceHost() const -> QString override;
		auto GetDeviceModelType() const -> QString override;
		auto GetDeviceSerial() const -> QString override;
		auto GetDeviceSoftwareVersion() const -> QString override;
		auto GetFormatVersion() const -> QString override;
		auto GetRecordDateTime() const -> QDateTime override;
		auto GetSoftwareVersion() const -> QString override;
		auto GetTitle() const -> QString override;
		auto GetUniqueID() const -> QString override;
		auto GetUserID() const -> QString override;

		auto ContainsHT() const -> bool override;
		auto ContainsFL() const -> bool override;
		auto ContainsBF() const -> bool override;

		auto GetHT() const -> HTReaderPtr override;
		auto GetFL() const -> FLReaderPtr override;
		auto GetBF() const -> BFReaderPtr override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
