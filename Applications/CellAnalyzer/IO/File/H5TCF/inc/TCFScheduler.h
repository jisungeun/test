#pragma once

#include <QThread>
#include <QVariantMap>

#include "StreamWriter.h"
#include "TCFHelper.h"

#include "CellAnalyzer.IO.File.H5TCFExport.h"

namespace CellAnalyzer::IO::File {
	enum class SliceAxis {
		XY,
		XZ,
		YZ
	};

	class CellAnalyzer_IO_File_H5TCF_API TCFScheduler final : public QThread {
	public:
		static auto GetInstance() -> std::shared_ptr<TCFScheduler>;

		auto ReadChildren(const QString& filepath, const QString& path) const -> QStringList;
		auto ReadAttribute(const QString& filepath, const QString& path) const -> QVariantMap;
		auto ReadData8b(const StreamWriterPtr<uint8_t>& stream, const QString& filepath, const QString& path) const -> void;
		auto ReadData16b(const StreamWriterPtr<uint16_t>& stream, const QString& filepath, const QString& path) const -> void;

	protected:
		TCFScheduler();
		~TCFScheduler() override;

		auto run() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;

		friend InstanceDeleter;
	};
}
