#pragma once

#include "IFLReader.h"

#include "CellAnalyzer.IO.File.H5TCFExport.h"

namespace CellAnalyzer::IO::File {
	class CellAnalyzer_IO_File_H5TCF_API FLReader final : public IFLReader {
	public:
		explicit FLReader(const QString& filename);
		~FLReader() override;

		auto GetDataCount(int chIndex) const -> int override;
		auto GetResolution() const -> Resolution3D override;
		auto GetSize() const -> Size3D override;
		auto GetRange() const -> DataRange override;
		auto GetZOffset() const -> double override;

		auto GetChannelList() const -> QVector<int> override;
		auto GetChannelCount() const -> int override;
		auto GetChannelName(int chIndex) const -> QString override;

		auto GetChannelColor(int chIndex) const -> Color override;
		auto GetChannelEmission(int chIndex) const -> double override;
		auto GetChannelExcitation(int chIndex) const -> double override;
		auto GetChannelExposureTime(int chIndex) const -> int override;
		auto GetChannelIntensity(int chIndex) const -> int override;

		auto Read(int chIndex, int tIndex) const -> FLImage3D override;
		auto ReadMip(int chIndex, int tIndex) const -> FLImage2D override;
		auto ReadThumbnail(int index) const -> FLThumbnail override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
