#pragma once

#include <memory>

#include "CellAnalyzer.IO.File.H5TCFExport.h"

namespace CellAnalyzer::IO::File {
	class TCFScheduler;

	class CellAnalyzer_IO_File_H5TCF_API H5Muter {
	public:
		H5Muter();
		~H5Muter();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	struct CellAnalyzer_IO_File_H5TCF_API InstanceDeleter {
		explicit InstanceDeleter(TCFScheduler*& instance, int& ref);
		auto operator()(TCFScheduler* const& obj) const noexcept -> void;

	private:
		TCFScheduler** instance;
		int* ref;
	};
}
