#include <H5Cpp.h>

#include <QQueue>

#include "TCFScheduler.h"
#include "TCFHelper.h"

#include "TcfH5GroupRoiLdmDataReader.h"

namespace {
	CellAnalyzer::IO::File::TCFScheduler* instance = nullptr;
	QMutex instanceMutex;
	auto instanceRefCount = 0;

	auto GetAttrValue(const H5::Attribute& attr) -> QVariant {
		if (attr.getDataType() == H5::PredType::NATIVE_INT64) {
			int64_t value;
			attr.read(H5::PredType::NATIVE_INT64, &value);
			return value;
		}

		if (attr.getDataType() == H5::PredType::NATIVE_DOUBLE) {
			double_t value;
			attr.read(H5::PredType::NATIVE_DOUBLE, &value);
			return value;
		}

		if (attr.getDataType() == H5::PredType::NATIVE_UINT8) {
			uint8_t value;
			attr.read(H5::PredType::NATIVE_UINT8, &value);
			return value;
		}

		std::string value;
		attr.read(attr.getDataType(), value);
		return QString::fromStdString(value);
	}

	auto IsLDM(const H5::H5File& file) -> bool {
		constexpr const auto* Data = "Data";
		constexpr const auto* StructType = "StructureType";

		if (const auto group = file.openGroup(Data); group.attrExists(StructType)) {
			const auto attr = group.openAttribute(StructType);
			int64_t value;
			attr.read(H5::PredType::NATIVE_INT64, &value);
			return value == 1;
		}

		return false;
	}
}

namespace CellAnalyzer::IO::File {
	using H5Bin = QList<std::shared_ptr<H5::H5Object>>;
	using Request = std::function<void()>;

	struct TCFScheduler::Impl {
		QQueue<Request> queue;
		QWaitCondition waiter;
		QMutex mutex;
		bool disposed = false;

		auto RequestBlocking(const Request& request) -> void;
		auto RequestNonblocking(const Request& request) -> void;
	};

	auto TCFScheduler::Impl::RequestBlocking(const Request& request) -> void {
		QMutex localMutex;
		QWaitCondition localWaiter;

		{
			QMutexLocker locker(&mutex);

			if (disposed)
				return;

			queue.enqueue([&request, &localWaiter] {
				request();
				localWaiter.wakeAll();
			});
		}

		waiter.wakeAll();
		QMutexLocker locker(&localMutex);
		localWaiter.wait(&localMutex);
	}

	auto TCFScheduler::Impl::RequestNonblocking(const Request& request) -> void {
		{
			QMutexLocker locker(&mutex);

			if (disposed)
				return;

			queue.enqueue(request);
		}

		waiter.wakeAll();
	}

	TCFScheduler::TCFScheduler() : QThread(), d(new Impl) {}

	TCFScheduler::~TCFScheduler() {
		d->disposed = true;
		d->waiter.wakeAll();

		wait();
	}

	auto TCFScheduler::GetInstance() -> std::shared_ptr<TCFScheduler> {
		if (!instance) {
			QMutexLocker locker(&instanceMutex);

			if (!instance) {
				instance = new TCFScheduler;
				instance->start();
			}
		}

		return { instance, InstanceDeleter(instance, instanceRefCount) };
	}

	auto TCFScheduler::ReadChildren(const QString& filepath, const QString& path) const -> QStringList {
		QStringList list;

		d->RequestBlocking([&filepath, &path, &list] {
			try {
				H5::H5File file;
				file.openFile(filepath.toUtf8(), H5F_ACC_RDONLY);

				auto func = [&list](const H5::H5Object& obj) {
					for (auto i = 0ULL; i < obj.getNumObjs(); i++)
						list.push_back(QString::fromStdString(obj.getObjnameByIdx(i)));
				};

				if (path.isEmpty())
					func(file);
				else
					func(file.openGroup(path.toUtf8()));
			} catch (...) {}
		});

		return list;
	}

	auto TCFScheduler::ReadAttribute(const QString& filepath, const QString& path) const -> QVariantMap {
		QVariantMap map;

		d->RequestBlocking([&filepath, &path, &map] {
			try {
				H5::H5File file;
				file.openFile(filepath.toUtf8(), H5F_ACC_RDONLY);

				auto func = [&map](const H5::H5Object& obj) {
					for (auto i = 0; i < obj.getNumAttrs(); i++) {
						const auto attr = obj.openAttribute(i);
						const auto name = QString::fromStdString(attr.getName());

						map[name] = GetAttrValue(attr);
					}
				};

				const auto utf = path.toUtf8();

				if (path.isEmpty())
					func(file);
				else if (const auto type = H5Iget_type(H5Oopen(file.getLocId(), utf, H5P_DEFAULT)); type == H5I_DATASET)
					func(file.openDataSet(utf));
				else if (type == H5I_GROUP)
					func(file.openGroup(utf));
			} catch (...) {}
		});

		return map;
	}

	auto TCFScheduler::ReadData8b(const StreamWriterPtr<uint8_t>& stream, const QString& filepath, const QString& path) const -> void {
		d->RequestBlocking([wstream = std::weak_ptr(stream), filepath, path] {
			if (const auto stream = wstream.lock()) {
				try {
					H5::H5File file;
					file.openFile(filepath.toUtf8(), H5F_ACC_RDONLY);

					if (auto* buffer = stream->BeginWrite(); !path.contains("Thumbnail") && IsLDM(file)) {
						auto group = file.openGroup(path.toUtf8());
						const auto x = GetAttrValue(group.openAttribute("DataSizeX")).toLongLong();
						const auto y = GetAttrValue(group.openAttribute("DataSizeY")).toLongLong();
						const auto z = group.attrExists("DataSizeZ") ? GetAttrValue(group.openAttribute("DataSizeZ")).toLongLong() : 0LL;

						TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
						reader.SetLdmDataGroup(group);
						reader.SetSamplingStep(1);
						reader.SetReadingRoi((z == 0) ?
												TC::IO::DataRange { { 0, 0 }, TC::IO::Count { static_cast<uint64_t>(x), static_cast<uint64_t>(y) } } :
												TC::IO::DataRange { { 0, 0, 0 }, TC::IO::Count { static_cast<uint64_t>(x), static_cast<uint64_t>(y), static_cast<uint64_t>(z) } });

						if (const auto chunk = reader.Read()) {
							const auto data = std::any_cast<std::shared_ptr<uint8_t[]>>(chunk->GetData());
							std::memcpy(buffer, data.get(), stream->GetSize());
							stream->EndWrite(stream->GetSize());
						} else {
							throw std::exception();
						}
					} else {
						const auto dataset = file.openDataSet(path.toUtf8());
						dataset.read(buffer, H5::PredType::NATIVE_UINT8);
						stream->EndWrite(static_cast<int64_t>(dataset.getStorageSize()));
					}
				} catch (...) {
					stream->EndWrite(INT64_MAX);
				}
			}
		});
	}

	auto TCFScheduler::ReadData16b(const StreamWriterPtr<uint16_t>& stream, const QString& filepath, const QString& path) const -> void {
		d->RequestBlocking([wstream = std::weak_ptr(stream), filepath, path] {
			if (const auto stream = wstream.lock()) {
				try {
					H5::H5File file;
					file.openFile(filepath.toUtf8(), H5F_ACC_RDONLY);

					if (auto* buffer = stream->BeginWrite(); IsLDM(file)) {
						auto group = file.openGroup(path.toUtf8());
						const auto x = GetAttrValue(group.openAttribute("DataSizeX")).toLongLong();
						const auto y = GetAttrValue(group.openAttribute("DataSizeY")).toLongLong();
						const auto z = group.attrExists("DataSizeZ") ? GetAttrValue(group.openAttribute("DataSizeZ")).toLongLong() : 0LL;
						const auto scalar = GetAttrValue(group.openAttribute("ScalarType")).toLongLong();

						TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
						reader.SetLdmDataGroup(group);
						reader.SetSamplingStep(1);
						reader.SetReadingRoi((z == 0) ?
												TC::IO::DataRange { { 0, 0 }, TC::IO::Count { static_cast<uint64_t>(x), static_cast<uint64_t>(y) } } :
												TC::IO::DataRange { { 0, 0, 0 }, TC::IO::Count { static_cast<uint64_t>(x), static_cast<uint64_t>(y), static_cast<uint64_t>(z) } });

						if (const auto chunk = reader.Read()) {
							if (scalar == 1) {
								const auto data = std::any_cast<std::shared_ptr<uint8_t[]>>(chunk->GetData());
								uint16_t min = 0;
								uint16_t multiplier = 1;

								if (group.attrExists("MinIntensity"))
									min = static_cast<uint16_t>(GetAttrValue(group.openAttribute("MinIntensity")).toDouble());
								else if (group.attrExists("RIMin")) {
									min = static_cast<uint16_t>(GetAttrValue(group.openAttribute("RIMin")).toDouble() * 10000.0);
									multiplier = 10;
								}

								for (auto i = 0; i < stream->GetSize(); i++)
									buffer[i] = min + data[i] * multiplier;
							} else {
								const auto data = std::any_cast<std::shared_ptr<uint16_t[]>>(chunk->GetData());
								std::memcpy(buffer, data.get(), stream->GetSize() * sizeof(uint16_t));
							}

							stream->EndWrite(stream->GetSize());
						} else {
							throw std::exception();
						}
					} else {
						const auto dataset = file.openDataSet(path.toUtf8());
						dataset.read(buffer, H5::PredType::NATIVE_UINT16);
						stream->EndWrite(static_cast<int64_t>(dataset.getStorageSize()));
					}
				} catch (...) {
					stream->EndWrite(INT64_MAX);
				}
			}
		});
	}

	auto TCFScheduler::run() -> void {
		while (!d->disposed) {
			Request request;

			{
				QMutexLocker locker(&d->mutex);

				while (d->queue.isEmpty() && !d->disposed)
					d->waiter.wait(&d->mutex);

				request = d->queue.dequeue();
			}

			{
				H5Muter muter;
				request();
			}
		}
	}
}
