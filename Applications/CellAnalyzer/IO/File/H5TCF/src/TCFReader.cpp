#include <QFile>

#include "TCFReader.h"
#include "TCFScheduler.h"

#include "BFReader.h"
#include "FLReader.h"
#include "HTReader.h"

namespace {
	struct TCFAttribute {
		QDateTime createDate;
		QString dataID;
		QString description;
		QString deviceHost;
		QString deviceModelType;
		QString deviceSerial;
		QString deviceSoftwareVersion;
		QString formatVersion;
		QDateTime recordTime;
		QString softwareVersion;
		QString title;
		QString uniqueID;
		QString userID;

		bool ht = false;
		bool fl = false;
		bool bf = false;
	};

	QMap<QString, std::weak_ptr<TCFAttribute>> attrMap;
	QMutex attrMutex;
}

namespace CellAnalyzer::IO::File {
	struct TCFReader::Impl {
		mutable std::shared_ptr<TCFAttribute> attr = nullptr;
		QString filename;

		auto GetAttr() const -> std::shared_ptr<TCFAttribute>;
	};

	auto TCFReader::Impl::GetAttr() const -> std::shared_ptr<TCFAttribute> {
		QMutexLocker locker(&attrMutex);

		if (const auto value = attrMap[filename].lock())
			attr = value;
		else {
			attr = std::make_shared<TCFAttribute>();

			const auto scheduler = TCFScheduler::GetInstance();
			const auto map = scheduler->ReadAttribute(filename, QString());
			const auto children = scheduler->ReadChildren(filename, "Data");

			for (const auto& k : map.keys()) {
				if (k == "CreateDate")
					attr->createDate = QDateTime::fromString(map[k].toString(), "yyyy-MM-dd HH:mm:ss");
				else if (k == "RecordTime")
					attr->recordTime = QDateTime::fromString(map[k].toString(), "yyyy-MM-dd HH:mm:ss.zzz");
				else if (k == "DataID")
					attr->dataID = map[k].toString();
				else if (k == "Description")
					attr->description = map[k].toString();
				else if (k == "DeviceHost")
					attr->deviceHost = map[k].toString();
				else if (k == "DeviceModelType")
					attr->deviceModelType = map[k].toString();
				else if (k == "DeviceSerial")
					attr->deviceSerial = map[k].toString();
				else if (k == "DeviceSoftwareVersion")
					attr->deviceSoftwareVersion = map[k].toString();
				else if (k == "FormatVersion")
					attr->formatVersion = map[k].toString();
				else if (k == "SoftwareVersion")
					attr->softwareVersion = map[k].toString();
				else if (k == "Title")
					attr->title = map[k].toString();
				else if (k == "UniqueID")
					attr->uniqueID = map[k].toString();
				else if (k == "UserID")
					attr->userID = map[k].toString();
			}

			for (const auto& ch : children) {
				if (ch == "3D")
					attr->ht = true;
				else if (ch == "3DFL")
					attr->fl = true;
				else if (ch == "BF")
					attr->bf = true;
			}

			attrMap[filename] = attr;
		}

		return attr;
	}

	TCFReader::TCFReader(const QString& filename) : ITCFReader(), d(new Impl) {
		d->filename = filename;
	}

	TCFReader::~TCFReader() {
		d->attr = nullptr;
		QMutexLocker locker(&attrMutex);

		if (attrMap[d->filename].expired())
			attrMap.remove(d->filename);
	}

	auto TCFReader::GetUrl() const -> QString {
		return d->filename;
	}

	auto TCFReader::IsReadable() const -> bool {
		return QFile::exists(d->filename) && !d->GetAttr()->dataID.isEmpty();
	}

	auto TCFReader::GetCreationDateTime() const -> QDateTime {
		return d->GetAttr()->createDate;
	}

	auto TCFReader::GetDataID() const -> QString {
		return d->GetAttr()->dataID;
	}

	auto TCFReader::GetDescription() const -> QString {
		return d->GetAttr()->description;
	}

	auto TCFReader::GetDeviceHost() const -> QString {
		return d->GetAttr()->deviceHost;
	}

	auto TCFReader::GetDeviceModelType() const -> QString {
		return d->GetAttr()->deviceModelType;
	}

	auto TCFReader::GetDeviceSerial() const -> QString {
		return d->GetAttr()->deviceSerial;
	}

	auto TCFReader::GetDeviceSoftwareVersion() const -> QString {
		return d->GetAttr()->deviceSoftwareVersion;
	}

	auto TCFReader::GetFormatVersion() const -> QString {
		return d->GetAttr()->formatVersion;
	}

	auto TCFReader::GetRecordDateTime() const -> QDateTime {
		return d->GetAttr()->recordTime;
	}

	auto TCFReader::GetSoftwareVersion() const -> QString {
		return d->GetAttr()->softwareVersion;
	}

	auto TCFReader::GetTitle() const -> QString {
		return d->GetAttr()->title;
	}

	auto TCFReader::GetUniqueID() const -> QString {
		return d->GetAttr()->uniqueID;
	}

	auto TCFReader::GetUserID() const -> QString {
		return d->GetAttr()->userID;
	}

	auto TCFReader::ContainsHT() const -> bool {
		return d->GetAttr()->ht;
	}

	auto TCFReader::ContainsFL() const -> bool {
		return d->GetAttr()->fl;
	}

	auto TCFReader::ContainsBF() const -> bool {
		return d->GetAttr()->bf;
	}

	auto TCFReader::GetHT() const -> HTReaderPtr {
		if (d->GetAttr()->ht)
			return std::make_shared<HTReader>(d->filename);

		return {};
	}

	auto TCFReader::GetFL() const -> FLReaderPtr {
		if (d->GetAttr()->fl)
			return std::make_shared<FLReader>(d->filename);

		return {};
	}

	auto TCFReader::GetBF() const -> BFReaderPtr {
		if (d->GetAttr()->bf)
			return std::make_shared<BFReader>(d->filename);

		return {};
	}
}
