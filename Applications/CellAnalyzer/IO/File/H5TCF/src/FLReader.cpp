#include <QMutexLocker>

#include "FLReader.h"
#include "TCFScheduler.h"

namespace {
	struct FLChannelAttribute {
		QString name;
		CellAnalyzer::IO::Color color;

		double emission = 0.0;
		double excitation = 0.0;
		int exposureTime = 0;
		int intensity = 0;
		int dataCount = 0;
	};

	struct FLAttribute {
		CellAnalyzer::IO::DataRange range;
		CellAnalyzer::IO::Resolution3D resolution;
		CellAnalyzer::IO::Size3D size;

		QVector<int> channels;
		double offset = 0;
		bool fl = false;
		bool mip = false;
		bool thumbnail = false;
	};

	QMap<QString, std::weak_ptr<FLAttribute>> attrMap;
	QMap<QString, QMap<int, std::shared_ptr<FLChannelAttribute>>> chMap;
	QMutex attrMutex;
	QMutex chMutex;
}

namespace CellAnalyzer::IO::File {
	struct FLReader::Impl {
		mutable std::shared_ptr<FLAttribute> attr = nullptr;
		QString filename;

		auto GetAttr() const -> std::shared_ptr<FLAttribute>;
		auto GetChannel(int chIndex) const -> std::shared_ptr<FLChannelAttribute>;
	};

	auto FLReader::Impl::GetAttr() const -> std::shared_ptr<FLAttribute> {
		QMutexLocker locker(&attrMutex);

		if (const auto value = attrMap[filename].lock())
			attr = value;
		else {
			attr = std::make_shared<FLAttribute>();

			const auto scheduler = TCFScheduler::GetInstance();
			const auto map = scheduler->ReadAttribute(filename, "Data/3DFL");
			const auto channels = scheduler->ReadChildren(filename, "Data/3DFL");
			const auto children = scheduler->ReadChildren(filename, "Data");
			const auto thumb = scheduler->ReadChildren(filename, "Thumbnail");

			for (const auto& k : map.keys()) {
				if (k == "MinIntensity")
					attr->range.min = map[k].toDouble();
				else if (k == "MaxIntensity")
					attr->range.max = map[k].toDouble();
				else if (k == "ResolutionX")
					attr->resolution.x = map[k].toDouble();
				else if (k == "ResolutionY")
					attr->resolution.y = map[k].toDouble();
				else if (k == "ResolutionZ")
					attr->resolution.z = map[k].toDouble();
				else if (k == "OffsetZ")
					attr->offset = map[k].toDouble();
				else if (k == "SizeX")
					attr->size.x = static_cast<int>(map[k].toLongLong());
				else if (k == "SizeY")
					attr->size.y = static_cast<int>(map[k].toLongLong());
				else if (k == "SizeZ")
					attr->size.z = static_cast<int>(map[k].toLongLong());
			}

			for (const auto& ch : channels) {
				if (ch.startsWith("CH"))
					attr->channels.push_back(ch.mid(2, ch.count() - 2).toInt());
			}

			attr->fl = children.contains("3DFL");
			attr->mip = children.contains("2DFLMIP");
			attr->thumbnail = thumb.contains("FL");
			attrMap[filename] = attr;
		}

		return attr;
	}

	auto FLReader::Impl::GetChannel(int chIndex) const -> std::shared_ptr<FLChannelAttribute> {
		QMutexLocker locker(&chMutex);

		if (chMap[filename].contains(chIndex))
			return chMap[filename][chIndex];

		chMap[filename][chIndex] = std::make_shared<FLChannelAttribute>();

		const auto path = QString("Data/3DFL/CH%1").arg(chIndex);
		const auto scheduler = TCFScheduler::GetInstance();
		const auto map = scheduler->ReadAttribute(filename, path);
		const auto children = scheduler->ReadChildren(filename, path);

		chMap[filename][chIndex]->name = QString("CH%1").arg(chIndex);
		chMap[filename][chIndex]->dataCount = children.count();

		for (const auto& k : map.keys()) {
			if (k == "ColorB")
				chMap[filename][chIndex]->color.blue = static_cast<uint8_t>(map[k].toUInt());
			else if (k == "ColorG")
				chMap[filename][chIndex]->color.green = static_cast<uint8_t>(map[k].toUInt());
			else if (k == "ColorR")
				chMap[filename][chIndex]->color.red = static_cast<uint8_t>(map[k].toUInt());
			else if (k == "Emission")
				chMap[filename][chIndex]->emission = map[k].toDouble();
			else if (k == "Excitation")
				chMap[filename][chIndex]->excitation = map[k].toDouble();
			else if (k == "ExposureTime")
				chMap[filename][chIndex]->exposureTime = static_cast<int>(map[k].toLongLong());
			else if (k == "Intensity")
				chMap[filename][chIndex]->intensity = static_cast<int>(map[k].toLongLong());
		}

		if (auto& [r, g, b] = chMap[filename][chIndex]->color; r == 0 && g == 0 && b == 0) {
			switch (chIndex) {
				case 0:
					b = 255;
					break;
				case 1:
					g = 255;
					break;
				case 2:
					r = 255;
					break;
			}
		}

		return chMap[filename][chIndex];
	}

	FLReader::FLReader(const QString& filename) : IFLReader(), d(new Impl) {
		d->filename = filename;
	}

	FLReader::~FLReader() {
		d->attr = nullptr;

		{
			QMutexLocker locker1(&attrMutex);
			QMutexLocker locker2(&chMutex);

			if (attrMap[d->filename].expired()) {
				attrMap.remove(d->filename);
				chMap.remove(d->filename);
			}
		}
	}

	auto FLReader::GetDataCount(int chIndex) const -> int {
		return 0;
	}

	auto FLReader::GetResolution() const -> Resolution3D {
		return d->GetAttr()->resolution;
	}

	auto FLReader::GetSize() const -> Size3D {
		return d->GetAttr()->size;
	}

	auto FLReader::GetRange() const -> DataRange {
		return d->GetAttr()->range;
	}

	auto FLReader::GetZOffset() const -> double {
		return d->GetAttr()->offset;
	}

	auto FLReader::GetChannelList() const -> QVector<int> {
		return d->GetAttr()->channels;
	}

	auto FLReader::GetChannelCount() const -> int {
		return d->GetAttr()->channels.count();
	}

	auto FLReader::GetChannelName(int chIndex) const -> QString {
		return d->GetChannel(chIndex)->name;
	}

	auto FLReader::GetChannelColor(int chIndex) const -> Color {
		return d->GetChannel(chIndex)->color;
	}

	auto FLReader::GetChannelEmission(int chIndex) const -> double {
		return d->GetChannel(chIndex)->emission;
	}

	auto FLReader::GetChannelExcitation(int chIndex) const -> double {
		return d->GetChannel(chIndex)->excitation;
	}

	auto FLReader::GetChannelExposureTime(int chIndex) const -> int {
		return d->GetChannel(chIndex)->exposureTime;
	}

	auto FLReader::GetChannelIntensity(int chIndex) const -> int {
		return d->GetChannel(chIndex)->intensity;
	}

	auto FLReader::Read(int chIndex, int tIndex) const -> FLImage3D {
		if (const auto attr = d->GetAttr(); attr->fl) {
			const auto scheduler = TCFScheduler::GetInstance();
			const auto path = QString("Data/3DFL/%1/%2").arg(d->GetChannel(chIndex)->name).arg(tIndex, 6, 10, QChar('0'));
			const auto map = scheduler->ReadAttribute(d->filename, path);
			auto range = attr->range;

			for (const auto& k : map.keys()) {
				if (k == "MaxIntensity")
					range.max = map[k].toDouble();
				else if (k == "MinIntensity")
					range.min = map[k].toDouble();
			}

			const auto buffer = std::make_shared<StreamBuffer<uint16_t>>(attr->size.ToSize3D());
			auto stream = std::make_shared<ImageStream3D<uint16_t>>(attr->size, attr->resolution, range, buffer);
			scheduler->ReadData16b(std::make_shared<StreamWriter<uint16_t>>(buffer), d->filename, path);

			return stream;
		}

		return {};
	}

	auto FLReader::ReadMip(int chIndex, int tIndex) const -> FLImage2D {
		if (const auto attr = d->GetAttr(); attr->mip) {
			const auto scheduler = TCFScheduler::GetInstance();
			const auto path = QString("Data/2DFLMIP/%1/%2").arg(d->GetChannel(chIndex)->name).arg(tIndex, 6, 10, QChar('0'));
			const auto map = scheduler->ReadAttribute(d->filename, path);
			auto range = attr->range;

			for (const auto& k : map.keys()) {
				if (k == "MaxIntensity")
					range.max = map[k].toDouble();
				else if (k == "MinIntensity")
					range.min = map[k].toDouble();
			}

			const auto buffer = std::make_shared<StreamBuffer<uint16_t>>(attr->size.ToSize2D());
			auto stream = std::make_shared<ImageStream2D<uint16_t>>(attr->size, attr->resolution, range, buffer);
			scheduler->ReadData16b(std::make_shared<StreamWriter<uint16_t>>(buffer), d->filename, path);

			return stream;
		}

		return {};
	}

	auto FLReader::ReadThumbnail(int index) const -> FLThumbnail {
		if (const auto attr = d->GetAttr(); attr->thumbnail) {
			const auto scheduler = TCFScheduler::GetInstance();
			const auto path = QString("Thumbnail/FL/%1").arg(index, 6, 10, QChar('0'));
			const auto map = scheduler->ReadAttribute(d->filename, path);

			Size2D size;
			Resolution2D resolution;

			size.rgb = map["IsColor"].toLongLong() == 1LL;
			size.x = static_cast<int>(map["SizeX"].toLongLong());
			size.y = static_cast<int>(map["SizeY"].toLongLong());
			resolution.x = map["ResolutionX"].toDouble();
			resolution.y = map["ResolutionY"].toDouble();

			const auto buffer = std::make_shared<StreamBuffer<uint8_t>>(size.ToSize2D());
			auto stream = std::make_shared<ImageStream2D<uint8_t>>(size, resolution, DataRange { 0, 255 }, buffer);
			scheduler->ReadData8b(std::make_shared<StreamWriter<uint8_t>>(buffer), d->filename, path);

			return stream;
		}

		return {};
	}
}
