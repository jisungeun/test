#include <H5Cpp.h>

#include "TCFHelper.h"
#include "TCFScheduler.h"

namespace CellAnalyzer::IO::File {
	struct H5Muter::Impl {
		H5E_auto2_t func = nullptr;
		void* data = nullptr;
	};

	H5Muter::H5Muter() : d(new Impl) {
		H5::Exception::getAutoPrint(d->func, &d->data);
		H5::Exception::dontPrint();
	}

	H5Muter::~H5Muter() {
		H5::Exception::setAutoPrint(d->func, d->data);
	}

	InstanceDeleter::InstanceDeleter(TCFScheduler*& instance, int& ref) : instance(&instance), ref(&++ref) {}

	auto InstanceDeleter::operator()(TCFScheduler* const& obj) const noexcept -> void {
		if (--*ref < 0) {
			*ref = 0;
			*instance = nullptr;
			delete obj;
		}
	}
}
