#include <QMutexLocker>

#include "BFReader.h"
#include "TCFScheduler.h"

namespace {
	struct BFAttribute {
		CellAnalyzer::IO::Resolution2D resolution;
		CellAnalyzer::IO::Size2D size { true };
		CellAnalyzer::IO::DataRange range { 0, 255 };

		int dataCount = 0;
		bool bf = false;
		bool thumbnail = false;
	};

	QMap<QString, std::weak_ptr<BFAttribute>> attrMap;
	QMutex attrMutex;
}

namespace CellAnalyzer::IO::File {
	struct BFReader::Impl {
		mutable std::shared_ptr<BFAttribute> attr = nullptr;
		QString filename;

		auto GetAttr() const -> std::shared_ptr<BFAttribute>;
	};

	auto BFReader::Impl::GetAttr() const -> std::shared_ptr<BFAttribute> {
		QMutexLocker locker(&attrMutex);

		if (const auto value = attrMap[filename].lock())
			attr = value;
		else {
			attr = std::make_shared<BFAttribute>();

			const auto scheduler = TCFScheduler::GetInstance();
			const auto map = scheduler->ReadAttribute(filename, "Data/BF");
			const auto children = scheduler->ReadChildren(filename, "Data");
			const auto thumb = scheduler->ReadChildren(filename, "Thumbnail");

			for (const auto& k : map.keys()) {
				if (k == "DataCount")
					attr->dataCount = static_cast<int>(map[k].toLongLong());
				else if (k == "ResolutionX")
					attr->resolution.x = map[k].toDouble();
				else if (k == "ResolutionY")
					attr->resolution.y = map[k].toDouble();
				else if (k == "SizeX")
					attr->size.x = static_cast<int>(map[k].toLongLong());
				else if (k == "SizeY")
					attr->size.y = static_cast<int>(map[k].toLongLong());
			}

			attr->bf = children.contains("BF");
			attr->thumbnail = thumb.contains("BF");
			attrMap[filename] = attr;
		}

		return attr;
	}

	BFReader::BFReader(const QString& filename) : IBFReader(), d(new Impl) {
		d->filename = filename;
	}

	BFReader::~BFReader() {
		d->attr = nullptr;
		QMutexLocker locker(&attrMutex);

		if (attrMap[d->filename].expired())
			attrMap.remove(d->filename);
	}

	auto BFReader::GetDataCount() const -> int {
		return d->GetAttr()->dataCount;
	}

	auto BFReader::GetResolution() const -> Resolution2D {
		return d->GetAttr()->resolution;
	}

	auto BFReader::GetSize() const -> Size2D {
		return d->GetAttr()->size;
	}

	auto BFReader::Read(int index) const -> BFImage {
		if (const auto attr = d->GetAttr(); attr->bf) {
			const auto scheduler = TCFScheduler::GetInstance();
			const auto path = QString("Data/BF/%1").arg(index, 6, 10, QChar('0'));

			const auto buffer = std::make_shared<StreamBuffer<uint8_t>>(attr->size.ToSize2D());
			auto stream = std::make_shared<ImageStream2D<uint8_t>>(attr->size, attr->resolution, attr->range, buffer);
			scheduler->ReadData8b(std::make_shared<StreamWriter<uint8_t>>(buffer), d->filename, path);

			return stream;
		}

		return {};
	}

	auto BFReader::ReadThumbnail(int index) const -> BFImage {
		if (const auto attr = d->GetAttr(); attr->thumbnail) {
			const auto scheduler = TCFScheduler::GetInstance();
			const auto path = QString("Thumbnail/BF/%1").arg(index, 6, 10, QChar('0'));
			const auto map = scheduler->ReadAttribute(d->filename, path);

			Size2D size;
			Resolution2D resolution;

			size.rgb = map["IsColor"].toLongLong() == 1LL;
			size.x = static_cast<int>(map["SizeX"].toLongLong());
			size.y = static_cast<int>(map["SizeY"].toLongLong());
			resolution.x = map["ResolutionX"].toDouble();
			resolution.y = map["ResolutionY"].toDouble();

			const auto buffer = std::make_shared<StreamBuffer<uint8_t>>(size.ToSize2D());
			auto stream = std::make_shared<ImageStream2D<uint8_t>>(size, resolution, attr->range, buffer);
			scheduler->ReadData8b(std::make_shared<StreamWriter<uint8_t>>(buffer), d->filename, path);

			return stream;
		}

		return {};
	}
}
