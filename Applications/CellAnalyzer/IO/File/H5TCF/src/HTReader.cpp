#include "HTReader.h"
#include "TCFScheduler.h"

namespace {
	struct HTAttribute {
		CellAnalyzer::IO::DataRange range;
		CellAnalyzer::IO::Resolution3D resolution;
		CellAnalyzer::IO::Size3D size;

		int dataCount = 0;
		bool ht = false;
		bool mip = false;
		bool thumbnail = false;
	};

	QMap<QString, std::weak_ptr<HTAttribute>> attrMap;
	QMutex attrMutex;
}

namespace CellAnalyzer::IO::File {
	struct HTReader::Impl {
		mutable std::shared_ptr<HTAttribute> attr = nullptr;
		QString filename;

		auto GetAttr() const -> std::shared_ptr<HTAttribute>;
	};

	auto HTReader::Impl::GetAttr() const -> std::shared_ptr<HTAttribute> {
		QMutexLocker locker(&attrMutex);

		if (const auto value = attrMap[filename].lock())
			attr = value;
		else {
			attr = std::make_shared<HTAttribute>();

			const auto scheduler = TCFScheduler::GetInstance();
			const auto map = scheduler->ReadAttribute(filename, "Data/3D");
			const auto children = scheduler->ReadChildren(filename, "Data");
			const auto thumb = scheduler->ReadChildren(filename, "Thumbnail");

			for (const auto& k : map.keys()) {
				if (k == "DataCount")
					attr->dataCount = static_cast<int>(map[k].toLongLong());
				else if (k == "RIMin")
					attr->range.min = map[k].toDouble();
				else if (k == "RIMax")
					attr->range.max = map[k].toDouble();
				else if (k == "ResolutionX")
					attr->resolution.x = map[k].toDouble();
				else if (k == "ResolutionY")
					attr->resolution.y = map[k].toDouble();
				else if (k == "ResolutionZ")
					attr->resolution.z = map[k].toDouble();
				else if (k == "SizeX")
					attr->size.x = static_cast<int>(map[k].toLongLong());
				else if (k == "SizeY")
					attr->size.y = static_cast<int>(map[k].toLongLong());
				else if (k == "SizeZ")
					attr->size.z = static_cast<int>(map[k].toLongLong());
			}

			attr->ht = children.contains("3D");
			attr->mip = children.contains("2DMIP");
			attr->thumbnail = thumb.contains("HT");
			attrMap[filename] = attr;
		}

		return attr;
	}

	HTReader::HTReader(const QString& filename) : IHTReader(), d(new Impl) {
		d->filename = filename;
	}

	HTReader::~HTReader() {
		d->attr = nullptr;
		QMutexLocker locker(&attrMutex);

		if (attrMap[d->filename].expired())
			attrMap.remove(d->filename);
	}

	auto HTReader::GetDataCount() const -> int {
		return d->GetAttr()->dataCount;
	}

	auto HTReader::GetResolution() const -> Resolution3D {
		return d->GetAttr()->resolution;
	}

	auto HTReader::GetSize() const -> Size3D {
		return d->GetAttr()->size;
	}

	auto HTReader::GetRange() const -> DataRange {
		return d->GetAttr()->range;
	}

	auto HTReader::Read(int index) const -> HTImage3D {
		if (const auto attr = d->GetAttr(); attr->ht) {
			const auto scheduler = TCFScheduler::GetInstance();
			const auto path = QString("Data/3D/%1").arg(index, 6, 10, QChar('0'));
			const auto map = scheduler->ReadAttribute(d->filename, path);
			auto range = attr->range;

			for (const auto& k : map.keys()) {
				if (k == "RIMax")
					range.max = map[k].toDouble() * 10000.0;
				else if (k == "RIMin")
					range.min = map[k].toDouble() * 10000.0;
			}

			const auto buffer = std::make_shared<StreamBuffer<uint16_t>>(attr->size.ToSize3D());
			auto stream = std::make_shared<ImageStream3D<uint16_t>>(attr->size, attr->resolution, range, buffer);
			scheduler->ReadData16b(std::make_shared<StreamWriter<uint16_t>>(buffer), d->filename, path);

			return stream;
		}

		return {};
	}

	auto HTReader::ReadMip(int index) const -> HTImage2D {
		if (const auto attr = d->GetAttr(); attr->mip) {
			const auto scheduler = TCFScheduler::GetInstance();
			const auto path = QString("Data/2DMIP/%1").arg(index, 6, 10, QChar('0'));
			const auto map = scheduler->ReadAttribute(d->filename, path);
			auto range = attr->range;

			for (const auto& k : map.keys()) {
				if (k == "RIMax")
					range.max = map[k].toDouble() * 10000.0;
				else if (k == "RIMin")
					range.min = map[k].toDouble() * 10000.0;
			}

			const auto buffer = std::make_shared<StreamBuffer<uint16_t>>(attr->size.ToSize2D());
			auto stream = std::make_shared<ImageStream2D<uint16_t>>(attr->size, attr->resolution, range, buffer);
			scheduler->ReadData16b(std::make_shared<StreamWriter<uint16_t>>(buffer), d->filename, path);

			return stream;
		}

		return {};
	}

	auto HTReader::ReadThumbnail(int index) const -> HTThumbnail {
		if (const auto attr = d->GetAttr(); attr->thumbnail) {
			const auto scheduler = TCFScheduler::GetInstance();
			const auto path = QString("Thumbnail/HT/%1").arg(index, 6, 10, QChar('0'));
			const auto map = scheduler->ReadAttribute(d->filename, path);

			Size2D size;
			Resolution2D resolution;

			size.rgb = map["IsColor"].toLongLong() == 1LL;
			size.x = static_cast<int>(map["SizeX"].toLongLong());
			size.y = static_cast<int>(map["SizeY"].toLongLong());
			resolution.x = map["ResolutionX"].toDouble();
			resolution.y = map["ResolutionY"].toDouble();

			const auto buffer = std::make_shared<StreamBuffer<uint8_t>>(size.ToSize2D());
			auto stream = std::make_shared<ImageStream2D<uint8_t>>(size, resolution, DataRange { 0, 255 }, buffer);
			scheduler->ReadData8b(std::make_shared<StreamWriter<uint8_t>>(buffer), d->filename, path);

			return stream;
		}

		return {};
	}
}
