#pragma once

#include "StreamBuffer.h"

namespace CellAnalyzer::IO {
	template <typename T>
	class StreamWriter {
	public:
		explicit StreamWriter(StreamBufferPtr<T> buffer) : buffer(std::move(buffer)) {}

		auto GetSize() const -> int64_t {
			return buffer->GetCapacity();
		}

		auto GetPosition() const -> int64_t {
			return buffer->GetFilled();
		}

		auto GetBuffer() const -> StreamBufferPtr<T> {
			return buffer;
		}

		auto BeginWrite() -> T* {
			return buffer->GetIndexOf(buffer->GetFilled());
		}

		auto EndWrite(int64_t writtenSize) -> void {
			buffer->Fill(writtenSize);
		}

	private:
		StreamBufferPtr<T> buffer;
	};

	template <typename T>
	using StreamWriterPtr = std::shared_ptr<StreamWriter<T>>;
}
