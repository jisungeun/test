#pragma once

#include "StreamBuffer.h"

namespace CellAnalyzer::IO {
	enum class SeekOrigin {
		Begin,
		Current,
		End
	};

	template <typename T>
	class StreamReader {
	public:
		explicit StreamReader(StreamBufferPtr<T> buffer) : buffer(std::move(buffer)) {}

		~StreamReader() = default;

		auto Seek(int64_t offset, SeekOrigin origin) -> int64_t {
			switch (const auto length = buffer->GetCapacity(); origin) {
				case SeekOrigin::Begin:
					position = std::max(0LL, std::min(length, offset));
					break;
				case SeekOrigin::Current:
					position = std::max(0LL, std::min(length, position + offset));
					break;
				case SeekOrigin::End:
					position = std::max(0LL, std::min(length, length - position - offset));
					break;
			}

			return position;
		}

		auto GetPosition() const -> int64_t {
			return position;
		}

		auto GetSize() const -> int64_t {
			return buffer->GetCapacity();
		}

		auto GetBuffer() const -> std::shared_ptr<const T[]> {
			return buffer->GetBuffer();
		}

		auto Peek() const -> T {
			return buffer[position];
		}

		auto Read(int64_t size) const -> const T* {
			const auto temp = position;
			const auto length = buffer->GetCapacity();
			const auto pos = std::max(0LL, std::min(length, temp + size));

			if (pos != temp + size)
				return nullptr;

			buffer->WaitForFilled(pos);
			position = pos;
			return buffer->GetIndexOf(temp);
		}

		auto ReadOne() const -> T {
			if (const auto* one = Read(1))
				return *one;

			return {};
		}

		auto ReadAll() const -> const T* {
			return Read(buffer->GetCapacity() - position);
		}

	private:
		StreamBufferPtr<T> buffer;
		mutable int64_t position = 0;
	};

	template <typename T>
	using StreamReaderPtr = std::shared_ptr<StreamReader<T>>;
}
