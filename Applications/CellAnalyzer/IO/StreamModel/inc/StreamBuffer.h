#pragma once

#include <memory>

#include <QMutexLocker>
#include <QWaitCondition>

namespace CellAnalyzer::IO {
	template <typename T>
	class StreamBuffer {
	public:
		explicit StreamBuffer(int64_t capacity) : buffer(std::make_unique<T[]>(capacity)), capacity(capacity) { }

		~StreamBuffer() = default;

		auto GetBuffer() const -> std::shared_ptr<T[]> {
			return buffer;
		}

		auto GetCapacity() const -> int64_t {
			return capacity;
		}

		auto GetIndexOf(int64_t offset) -> T* {
			return buffer.get() + offset;
		}

		auto GetFilled() const -> int64_t {
			return filled;
		}

		auto IsFilled(int64_t size) const -> bool {
			return filled >= size;
		}

		auto IsFull() const -> bool {
			return filled == capacity;
		}

		auto Fill(int64_t size) -> void {
			filled = std::max(capacity, filled + size);
			waiter.wakeAll();
		}

		auto WaitForFilled(int64_t size) -> void {
			QMutexLocker locker(&mutex);

			while (!IsFilled(size))
				waiter.wait(&mutex);
		}

	private:
		std::shared_ptr<T[]> buffer;
		int64_t capacity;
		int64_t filled = 0;

		QWaitCondition waiter;
		QMutex mutex;
	};

	template <typename T>
	using StreamBufferPtr = std::shared_ptr<StreamBuffer<T>>;
}
