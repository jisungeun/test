#pragma once

#include "IExporter.h"

#include "CellAnalyzer.IO.ExportModelExport.h"

namespace CellAnalyzer::IO {
	class CellAnalyzer_IO_ExportModel_API IExportService : public virtual Tomocube::IService {
	public:
		virtual auto GetExporter(const QString& name) const -> ExporterPtr = 0;
		virtual auto GetExporterList() const -> ExporterList = 0;
		virtual auto GetExporterList(const DataFlags& flags) const -> ExporterList = 0;
	};
}
