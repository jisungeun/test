#pragma once

#include <QVariant>

#include "IData.h"

#include "CellAnalyzer.IO.ExportModelExport.h"

namespace CellAnalyzer::IO {
	class IExporter;
	using ExporterPtr = std::shared_ptr<IExporter>;
	using ExporterList = QVector<ExporterPtr>;

	class CellAnalyzer_IO_ExportModel_API IExporter : public virtual Tomocube::IService {
	public:
		virtual auto GetName() const -> QString = 0;
		virtual auto GetFormat() const -> QString = 0;
		virtual auto GetIcon() const -> QString = 0;
		virtual auto GetMetadata(const QString& name) const -> QVariant = 0;
		virtual auto GetMetadataList() const -> QStringList = 0;
		virtual auto IsExportable(const DataFlags& flags) const -> bool = 0;

		virtual auto SetMetadata(const QString& name, const QVariant& value) -> void = 0;
		virtual auto Export(const DataPtr& data, const QString& path) -> bool = 0;
	};
}
