#include <QFileDialog>

#include "TCFDialog.h"

#include "TreeModel.h"
#include "ViewerWidget.h"

#include "IDatabase.h"

#include "ui_TCFDialog.h"

namespace CellAnalyzer::IO::Widget {
	struct TCFDialog::Impl {
		Ui::TCFDialog ui;
		TreeModel model;

		ViewerWidget* viewer = nullptr;

		static auto SetStylesheet(QWidget* parent) -> void;
	};

	auto TCFDialog::Impl::SetStylesheet(QWidget* parent) -> void {
		if (QFile file(":/Stylesheet/CellAnalyzer.qss", parent); file.open(QIODevice::ReadOnly))
			parent->setStyleSheet(file.readAll());
	}

	TCFDialog::TCFDialog() : QDialog(nullptr, Qt::Dialog | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint), IView(), d(new Impl) {
		d->SetStylesheet(this);
		d->ui.setupUi(this);
		d->viewer = new ViewerWidget(this);
		d->ui.splitter->setStretchFactor(0, 1);
		d->ui.splitter->setStretchFactor(1, 3);
		d->ui.progressBar->setVisible(false);
		d->ui.selectBtn->setEnabled(false);
		d->ui.selectBtn->setObjectName("accent_color");
		d->ui.htBtn->setObjectName("quiet_color");
		d->ui.flBtn->setObjectName("quiet_color");
		d->ui.bfBtn->setObjectName("quiet_color");
		d->ui.selectBtn->style()->polish(d->ui.selectBtn);
		d->ui.htBtn->style()->polish(d->ui.htBtn);
		d->ui.flBtn->style()->polish(d->ui.flBtn);
		d->ui.bfBtn->style()->polish(d->ui.bfBtn);
		d->ui.tree->setModel(&d->model);
		d->ui.columnSpin->setValue(d->viewer->GetColumnCount());
		d->ui.galleryLayout->addWidget(d->viewer);

		connect(d->ui.pathBtn, &QPushButton::clicked, this, &TCFDialog::OnPathBtnClicked);
		connect(d->ui.selectBtn, &QPushButton::clicked, this, &TCFDialog::OnSelectBtnClicked);
		connect(d->ui.htBtn, &QPushButton::clicked, this, &TCFDialog::OnHtBtnClicked);
		connect(d->ui.flBtn, &QPushButton::clicked, this, &TCFDialog::OnFlBtnClicked);
		connect(d->ui.bfBtn, &QPushButton::clicked, this, &TCFDialog::OnBfBtnClicked);
		connect(d->ui.columnSpin, QOverload<int>::of(&QSpinBox::valueChanged), this, &TCFDialog::OnCountSpinChanged);
		connect(d->ui.tree->selectionModel(), &QItemSelectionModel::currentChanged, this, &TCFDialog::OnTreeSelected);
		connect(d->viewer, &ViewerWidget::TCFSelected, this, &TCFDialog::OnGallerySelected);

		if (const auto map = IDatabase::GetInstance()->GetMap("TCF\\Gallery")) {
			if (map->contains("LastPath")) {
				const auto path = (*map)["LastPath"].toString();

				if (const auto index = d->model.GetIndex(path); index.isValid()) {
					auto parent = index.parent();

					while (parent.isValid()) {
						d->ui.tree->expand(parent);
						parent = parent.parent();
					}

					d->ui.tree->scrollTo(index);
					d->ui.tree->setCurrentIndex(index);
					d->ui.tree->selectionModel()->select(index, QItemSelectionModel::ClearAndSelect);
				}
			}

			if (map->contains("ColumnCount")) {
				const auto column = (*map)["ColumnCount"].toInt();
				d->ui.columnSpin->setValue(column);
			}

			if (map->contains("Modality")) {
				switch (const auto modality = (*map)["Modality"].toString(); ToModality(modality)) {
					case Modality::HT:
						d->ui.htBtn->click();
						break;
					case Modality::FL:
						d->ui.flBtn->click();
						break;
					case Modality::BF:
						d->ui.bfBtn->click();
						break;
				}
			}

			if (map->contains("Width") && map->contains("Height")) {
				const auto width = (*map)["Width"].toInt();
				const auto height = (*map)["Height"].toInt();

				resize(width, height);
			}
		}
	}

	TCFDialog::~TCFDialog() {
		QVariantMap map;

		if (const auto tcf = IDatabase::GetInstance()->GetMap("TCF\\Gallery"))
			map = *tcf;

		map["Width"] = width();
		map["Height"] = height();

		IDatabase::GetInstance()->Save("TCF\\Gallery", map);
		IDatabase::GetInstance()->Flush();
	}

	auto TCFDialog::SetMultiSelective(bool multi) -> void {
		d->viewer->SetMultiSelective(multi);
	}

	auto TCFDialog::GetSelected() const -> QStringList {
		return d->viewer->GetSelected();
	}

	auto TCFDialog::OnPathBtnClicked() -> void {
		if (const auto path = QFileDialog::getExistingDirectory(this); !path.isEmpty()) {
			const auto index = d->model.GetIndex(path);

			for (auto i = index; i.isValid();) {
				d->ui.tree->expand(i.parent());
				i = i.parent();
			}

			d->ui.pathLine->setText(path);
			d->ui.tree->scrollTo(index);
			d->ui.tree->selectionModel()->setCurrentIndex(index, QItemSelectionModel::ClearAndSelect);
		}
	}

	auto TCFDialog::OnSelectBtnClicked() -> void {
		if (!d->viewer->GetSelected().isEmpty()) {
			QVariantMap map;

			if (const auto tcf = IDatabase::GetInstance()->GetMap("TCF\\Gallery"))
				map = *tcf;

			if (const auto index = d->ui.tree->selectionModel()->currentIndex(); index.isValid())
				map["LastPath"] = d->model.GetPath(index);

			map["ColumnCount"] = d->ui.columnSpin->value();
			map["Modality"] = ToString(d->viewer->GetModality());
			map["Width"] = width();
			map["Height"] = height();

			IDatabase::GetInstance()->Save("TCF\\Gallery", map);
			IDatabase::GetInstance()->Flush();

			accept();
		}
	}

	auto TCFDialog::OnHtBtnClicked() -> void {
		d->ui.htBtn->setChecked(true);
		d->ui.flBtn->setChecked(false);
		d->ui.bfBtn->setChecked(false);
		d->viewer->SetModality(Modality::HT);
	}

	auto TCFDialog::OnFlBtnClicked() -> void {
		d->ui.htBtn->setChecked(false);
		d->ui.flBtn->setChecked(true);
		d->ui.bfBtn->setChecked(false);
		d->viewer->SetModality(Modality::FL);
	}

	auto TCFDialog::OnBfBtnClicked() -> void {
		d->ui.htBtn->setChecked(false);
		d->ui.flBtn->setChecked(false);
		d->ui.bfBtn->setChecked(true);
		d->viewer->SetModality(Modality::BF);
	}

	auto TCFDialog::OnCountSpinChanged(int value) -> void {
		d->viewer->SetColumnCount(value);
	}

	auto TCFDialog::OnTreeSelected(const QModelIndex& current, const QModelIndex& previous) -> void {
		const auto path = d->model.GetPath(current);
		const auto list = d->model.GetTCFList(path);

		d->viewer->SetTCFList(list);
		d->ui.pathLine->setText(path);
		d->ui.selectBtn->setEnabled(false);
		d->ui.countLabel->setText(QString::number(list.count()));

		d->viewer->update();
	}

	auto TCFDialog::OnGallerySelected(const QStringList& files) -> void {
		d->ui.selectBtn->setEnabled(!files.isEmpty());
	}
}
