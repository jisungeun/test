#include <QDir>
#include <QFileIconProvider>
#include <QThreadPool>

#include "TreeModel.h"
#include "TreeNode.h"

namespace CellAnalyzer::IO::Widget {
	struct TreeModel::Impl {
		QThreadPool pool;
		NodeChild root = std::make_shared<Node>();

		QMap<const Node*, NodeChild> ptrMap;

		auto ScanNode(TreeModel* model, const NodeChild& node) -> void;
		auto FindChildren(TreeModel* model, const NodeParent& parent, const QString& path) -> NodeChild;
	};

	auto TreeModel::Impl::ScanNode(TreeModel* model, const NodeChild& node) -> void {
		if (node->GetState() != ScanState::Scannable)
			return;

		emit model->beginRemoveRows(model->createIndex(node->GetIndex(), 0, node.get()), 0, 0);
		emit model->endRemoveRows();

		node->Scan();

		for (const auto& ch : node->GetChildren())
			ptrMap[ch.get()] = ch;

		if (!node->IsChildEmpty()) {
			emit model->beginInsertRows(model->createIndex(node->GetIndex(), 0, node.get()), 0, node->GetChildCount() - 1);
			emit model->endInsertRows();
		}
	}

	auto TreeModel::Impl::FindChildren(TreeModel* model, const NodeParent& parent, const QString& path) -> NodeChild {
		if (!ptrMap.contains(parent))
			return {};

		const auto p = ptrMap[parent];

		if (parent->GetState() == ScanState::Scannable)
			ScanNode(model, p);

		for (const auto& ch : p->GetChildren()) {
			if (ch->GetName() == path)
				return ch;
		}

		return {};
	}

	TreeModel::TreeModel() : QAbstractItemModel(), d(new Impl) {
		d->pool.setMaxThreadCount(1);
		d->ptrMap[d->root.get()] = d->root;
		d->ScanNode(this, d->root);
	}

	TreeModel::~TreeModel() {
		d->pool.waitForDone();
	}

	auto TreeModel::GetPath(const QModelIndex& index) const -> QString {
		if (const auto* node = static_cast<Node*>(index.internalPointer()); d->ptrMap.contains(node))
			return node->GetPath();

		return {};
	}

	auto TreeModel::GetTCFList(const QString& path) const -> QStringList {
		const auto index = GetIndex(path);

		if (const auto* node = static_cast<Node*>(index.internalPointer()); d->ptrMap.contains(node))
			return node->GetTCFList();

		return {};
	}

	auto TreeModel::GetIndex(const QString& path) const -> QModelIndex {
		if (const auto splits = path.split('/'); !splits.isEmpty()) {
			NodeChild child = nullptr;

			for (const auto& i : d->root->GetChildren()) {
				if (i->GetPath().remove('/') == splits.first()) {
					child = i;
					break;
				}
			}

			for (auto i = 1; i < splits.count(); i++) {
				if (const auto ch = d->FindChildren(const_cast<TreeModel*>(this), child.get(), splits[i]))
					child = ch;
				else
					break;
			}

			if (child)
				return createIndex(child->GetIndex(), 0, child.get());
		}

		return {};
	}

	auto TreeModel::index(int row, int column, const QModelIndex& parent) const -> QModelIndex {
		if (const auto* node = static_cast<Node*>(parent.internalPointer()); d->ptrMap.contains(node)) {
			if (row < node->GetChildCount() && row >= 0)
				return createIndex(row, 0, node->GetChild(row).get());
		}

		if (!parent.isValid()) {
			if (row < d->root->GetChildCount() && row >= 0)
				return createIndex(row, 0, d->root->GetChild(row).get());
		}

		return {};
	}

	auto TreeModel::parent(const QModelIndex& child) const -> QModelIndex {
		if (const auto* node = static_cast<Node*>(child.internalPointer()); d->ptrMap.contains(node)) {
			if (const auto parent = node->GetParent())
				return createIndex(parent->GetIndex(), 0, parent);
		}

		return {};
	}

	auto TreeModel::rowCount(const QModelIndex& parent) const -> int {
		if (const auto* node = static_cast<Node*>(parent.internalPointer()); d->ptrMap.contains(node)) {
			if (node->GetState() == ScanState::Scannable) {
				d->pool.start([this, node] {
					d->ScanNode(const_cast<TreeModel*>(this), d->ptrMap[node]);
				});
			}

			if (node->GetState() == ScanState::Scanned)
				return node->GetChildCount();

			return 1;
		}

		if (!parent.isValid()) {
			if (d->root->GetState() == ScanState::Scannable)
				d->ScanNode(const_cast<TreeModel*>(this), d->root);

			if (d->root->GetState() == ScanState::Scanned)
				return d->root->GetChildCount();

			return 1;
		}

		return 0;
	}

	auto TreeModel::columnCount(const QModelIndex& parent) const -> int {
		return 1;
	}

	auto TreeModel::data(const QModelIndex& index, int role) const -> QVariant {
		if (const auto* node = static_cast<Node*>(index.internalPointer()); d->ptrMap.contains(node)) {
			switch (role) {
				case Qt::DisplayRole:
					return node->GetName();
				case Qt::DecorationRole:
					return QFileIconProvider().icon(node->GetPath());
			}
		}

		return {};
	}
}
