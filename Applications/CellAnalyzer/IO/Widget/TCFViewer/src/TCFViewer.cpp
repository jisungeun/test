#include "TCFViewer.h"

#include <QFileDialog>
#include <QStyle>

#include "IAlertHandler.h"
#include "TreeModel.h"

#include "ui_TCFViewer.h"

namespace CellAnalyzer::IO::Widget {
	struct TCFViewer::Impl {
		Ui::TCFViewer ui;
		TreeModel model;

		static auto SetStylesheet(QWidget* parent) -> void;
	};

	auto TCFViewer::Impl::SetStylesheet(QWidget* parent) -> void {
		if (QFile file(":/Stylesheet/CellAnalyzer.qss", parent); file.open(QIODevice::ReadOnly))
			parent->setStyleSheet(file.readAll());
	}

	TCFViewer::TCFViewer() : QDialog(nullptr, Qt::Dialog | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint), IView(), d(new Impl) {
		d->SetStylesheet(this);
		d->ui.setupUi(this);
		d->ui.splitter->setStretchFactor(0, 1);
		d->ui.splitter->setStretchFactor(1, 3);
		d->ui.progressBar->setVisible(false);
		d->ui.selectBtn->setEnabled(false);
		d->ui.selectBtn->setObjectName("accent_color");
		d->ui.htBtn->setObjectName("quiet_color");
		d->ui.flBtn->setObjectName("quiet_color");
		d->ui.bfBtn->setObjectName("quiet_color");
		d->ui.selectBtn->style()->polish(d->ui.selectBtn);
		d->ui.htBtn->style()->polish(d->ui.htBtn);
		d->ui.flBtn->style()->polish(d->ui.flBtn);
		d->ui.bfBtn->style()->polish(d->ui.bfBtn);
		d->ui.tree->setModel(&d->model);
		d->ui.columnSpin->setValue(d->ui.gallery->GetColumnCount());

		connect(d->ui.pathBtn, &QPushButton::clicked, this, &TCFViewer::OnPathBtnClicked);
		connect(d->ui.selectBtn, &QPushButton::clicked, this, &TCFViewer::OnSelectBtnClicked);
		connect(d->ui.htBtn, &QPushButton::clicked, this, &TCFViewer::OnHtBtnClicked);
		connect(d->ui.flBtn, &QPushButton::clicked, this, &TCFViewer::OnFlBtnClicked);
		connect(d->ui.bfBtn, &QPushButton::clicked, this, &TCFViewer::OnBfBtnClicked);
		connect(d->ui.columnSpin, QOverload<int>::of(&QSpinBox::valueChanged), this, &TCFViewer::OnCountSpinChanged);
		connect(d->ui.tree->selectionModel(), &QItemSelectionModel::currentChanged, this, &TCFViewer::OnTreeSelected);
		connect(d->ui.gallery, &Preview::Selected, this, &TCFViewer::OnGallerySelected);
	}

	TCFViewer::~TCFViewer() = default;

	auto TCFViewer::GetTitle() const -> QString {
		return "TCF Viewer";
	}

	auto TCFViewer::GetIcon() const -> QString {
		return {};
	}

	auto TCFViewer::SetMultiSelective(bool multi) -> void {
		d->ui.gallery->SetMultiSelective(multi);
	}

	auto TCFViewer::GetSelected() const -> QStringList {
		return d->ui.gallery->GetSelected();
	}

	auto TCFViewer::OnPathBtnClicked() -> void {
		const auto index = d->model.GetIndex(d->ui.pathLine->text());

		for (auto i = index; i.isValid();) {
			d->ui.tree->expand(i.parent());
			i = i.parent();
		}

		d->ui.tree->scrollTo(index);
		d->ui.tree->selectionModel()->clear();
		d->ui.tree->selectionModel()->select(index, QItemSelectionModel::SelectCurrent);
	}

	auto TCFViewer::OnSelectBtnClicked() -> void {
		if (!d->ui.gallery->GetSelected().isEmpty())
			accept();
	}

	auto TCFViewer::OnHtBtnClicked() -> void {
		d->ui.htBtn->setChecked(true);
		d->ui.flBtn->setChecked(false);
		d->ui.bfBtn->setChecked(false);
		d->ui.gallery->SetModality(Modality::HT);
	}

	auto TCFViewer::OnFlBtnClicked() -> void {
		d->ui.htBtn->setChecked(false);
		d->ui.flBtn->setChecked(true);
		d->ui.bfBtn->setChecked(false);
		d->ui.gallery->SetModality(Modality::FL);
	}

	auto TCFViewer::OnBfBtnClicked() -> void {
		d->ui.htBtn->setChecked(false);
		d->ui.flBtn->setChecked(false);
		d->ui.bfBtn->setChecked(true);
		d->ui.gallery->SetModality(Modality::BF);
	}

	auto TCFViewer::OnCountSpinChanged(int value) -> void {
		d->ui.gallery->SetColumnCount(value);
	}

	auto TCFViewer::OnTreeSelected(const QModelIndex& current, const QModelIndex& previous) -> void {
		const auto path = d->model.GetPath(current);
		const auto list = d->model.GetTCFList(path);

		d->ui.gallery->ClearTCF();
		d->ui.pathLine->setText(path);
		d->ui.selectBtn->setEnabled(false);

		d->ui.gallery->AddTCF(list);
		d->ui.countLabel->setText(QString::number(list.count()));
	}

	auto TCFViewer::OnGallerySelected(const QStringList& files) -> void {
		d->ui.selectBtn->setEnabled(!files.isEmpty());
	}
}
