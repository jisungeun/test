#include <QDir>

#include "TreeNode.h"

namespace CellAnalyzer::IO::Widget {
	struct Node::Impl {
		QString path;
		NodeParent parent = nullptr;
		NodeChildren children;
		ScanState state = ScanState::Scannable;
		QStringList tcfList;

		static auto IsPaired(const QDir& dir, const QFileInfoList& files) -> bool;
		static auto GetTCFList(const QDir& dir) -> QStringList;
	};

	auto Node::Impl::IsPaired(const QDir& dir, const QFileInfoList& files) -> bool {
		if (files.count() > 1)
			return false;

		for (const auto& tcf : files) {
			if (tcf.completeBaseName() == dir.dirName())
				return true;
		}

		return false;
	}

	auto Node::Impl::GetTCFList(const QDir& dir) -> QStringList {
		QStringList list;

		for (const auto& tcf : dir.entryInfoList({ "*.TCF" }, QDir::Files | QDir::NoDotAndDotDot))
			list.push_back(tcf.absoluteFilePath());

		for (const auto& subdir : dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
			if (const auto tcfs = QDir(dir.filePath(subdir)).entryInfoList({ "*.TCF" }, QDir::Files | QDir::NoDotAndDotDot); IsPaired(subdir, tcfs)) {
				for (const auto& t : tcfs)
					list.push_back(t.absoluteFilePath());
			}
		}

		return list;
	}

	Node::Node() : d(new Impl) { }

	Node::Node(const QString& path, const QStringList& tcfList, const NodeParent& parent) : d(new Impl) {
		d->path = path;
		d->tcfList = tcfList;
		d->parent = parent;
	}

	Node::~Node() = default;

	auto Node::GetPath() const -> QString {
		return d->path;
	}

	auto Node::GetParent() const -> NodeParent {
		return d->parent;
	}

	auto Node::GetChild(int index) const -> NodeChild {
		return d->children[index];
	}

	auto Node::GetChildren() const -> NodeChildren {
		return d->children;
	}

	auto Node::GetChildCount() const -> int {
		return d->children.count();
	}

	auto Node::IsChildEmpty() const -> bool {
		return d->children.isEmpty();
	}

	auto Node::GetState() const -> ScanState {
		return d->state;
	}

	auto Node::GetTCFList() const -> QStringList {
		return d->tcfList;
	}

	auto Node::GetIndex() const -> int {
		if (!d->parent)
			return 0;

		for (auto i = 0; i < d->parent->GetChildCount(); i++) {
			if (d->parent->GetChild(i).get() == this)
				return i;
		}

		return -1;
	}

	auto Node::GetName() const -> QString {
		if (auto name = QDir(d->path).dirName(); !name.isEmpty())
			return name;

		return d->path;
	}

	auto Node::Scan() -> void {
		if (d->state != ScanState::Scannable)
			return;

		d->state = ScanState::Scanning;
		NodeChildren children;
		QList<QDir> entry;

		if (d->parent == nullptr) {
			for (const auto& i : QDir::drives())
				entry.push_back(i.absoluteFilePath());
		} else {
			const QDir dir(d->path);

			for (const auto& path : dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
				const auto subpath = QDir(dir.filePath(path));
				const auto tcfList = subpath.entryInfoList({ "*.TCF" }, QDir::Files | QDir::NoDotAndDotDot);

				if (!d->IsPaired(subpath, tcfList))
					entry.push_back(subpath);
			}
		}

		for (const auto& e : entry)
			children.push_back(std::make_shared<Node>(e.absolutePath(), d->GetTCFList(e), this));

		d->children = children;
		d->state = ScanState::Scanned;
	}
}
