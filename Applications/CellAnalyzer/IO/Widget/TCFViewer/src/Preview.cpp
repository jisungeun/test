#include <QIcon>
#include <QPainter>
#include <QThreadPool>

#include "Preview.h"

#include "TCFReader.h"
#include "ui_Preview.h"

namespace CellAnalyzer::IO::Widget {
	struct Preview::Impl {
		Ui::Preview ui;

		QStringList readable;
		QStringList list;
		QMap<QString, QPixmap> pixmap;

		Modality modality = Modality::HT;
		bool multi = false;

		int hoveredIndex = -1;
		int pressedIndex = -1;
		QVector<int> selectedIndex;

		QPixmap empty;
		QPixmap accent;

		QThreadPool pool;

		static auto DrawImage(int size, const QIcon& icon) -> QPixmap;
		static auto DrawColor(const QColor& color) -> QPixmap;

		static auto SwapXYRGB(uint8_t* buffer, int x, int y) -> void;
		static auto ReadThumbnail(const File::TCFReader& reader, Modality modality) -> QPixmap;
		static auto ReadMip(const File::TCFReader& reader, Modality modality) -> QPixmap;
	};

	auto Preview::Impl::DrawImage(int size, const QIcon& icon) -> QPixmap {
		QPixmap pixmap(size, size);
		pixmap.fill(QColor(33, 33, 33));

		QPainter painter(&pixmap);
		painter.drawPixmap(size / 4, size / 4, icon.pixmap(size / 2, size / 2));
		painter.end();

		return pixmap;
	}

	auto Preview::Impl::DrawColor(const QColor& color) -> QPixmap {
		QPixmap pixmap(1, 1);
		pixmap.fill(color);
		return pixmap;
	}

	auto Preview::Impl::SwapXYRGB(uint8_t* buffer, int x, int y) -> void {
		const auto result = std::make_unique<uint8_t[]>(x * y * 3);

		for (uint64_t j = 0; j < x; ++j) {
			for (uint64_t k = 0; k < y; ++k) {
				for (uint64_t z = 0; z < 3; ++z) {
					const auto src = j + (k * x) + (z * x * y);
					const auto dest = k + (j * y) + (z * x * y);

					result[dest] = buffer[src];
				}
			}
		}

		for (uint64_t j = 0; j < x; ++j) {
			for (uint64_t k = 0; k < y; ++k) {
				const auto srcR = k + j * y + 0 * (x * y);
				const auto srcG = k + j * y + 1 * (x * y);
				const auto srcB = k + j * y + 2 * (x * y);

				const auto destR = 3 * (j + k * x) + 0;
				const auto destG = 3 * (j + k * x) + 1;
				const auto destB = 3 * (j + k * x) + 2;

				buffer[destR] = result[srcR];
				buffer[destG] = result[srcG];
				buffer[destB] = result[srcB];
			}
		}
	}

	auto Preview::Impl::ReadThumbnail(const File::TCFReader& reader, Modality modality) -> QPixmap {
		switch (modality) {
			case Modality::HT:
				if (const auto ht = reader.GetHT()) {
					if (const auto thumb = ht->ReadThumbnail(0)) {
						const auto [x, y] = thumb->GetSize();
						return QPixmap::fromImage({ thumb->GetData(), x, y, x, QImage::Format_Grayscale8 });
					}
				}
				break;
			case Modality::FL:
				if (const auto fl = reader.GetFL()) {
					if (const auto thumb = fl->ReadThumbnail(0)) {
						const auto [x, y] = thumb->GetSize();
						return QPixmap::fromImage({ thumb->GetData(), x, y, x, QImage::Format_Grayscale8 });
					}
				}
				break;
			case Modality::BF:
				if (const auto bf = reader.GetBF()) {
					if (const auto thumb = bf->ReadThumbnail(0)) {
						const auto [x, y] = thumb->GetSize();
						SwapXYRGB(thumb->GetData(), x, y);
						return QPixmap::fromImage({ thumb->GetData(), x, y, x * 3, QImage::Format_RGB888 });
					}
				}
				break;
		}

		return {};
	}

	auto Preview::Impl::ReadMip(const File::TCFReader& reader, Modality modality) -> QPixmap {
		switch (modality) {
			case Modality::HT:
				if (const auto ht = reader.GetHT()) {
					if (const auto mip = ht->ReadMip(0); mip && mip->GetData()) {
						const auto [x, y] = mip->GetSize();
						const auto [min, max] = mip->GetRange();

						for (auto i = 0; i < x * y; i++)
							mip->GetData()[i] = static_cast<uint16_t>(std::max(0.0, mip->GetData()[i] - min * 10000.0) / ((max - min) * 10000.0) * UINT16_MAX);

						return QPixmap::fromImage({ reinterpret_cast<uint8_t*>(mip->GetData()), x, y, x * 2, QImage::Format_Grayscale16 });
					}
				}
				break;
			case Modality::FL:
				if (const auto fl = reader.GetFL()) {
					std::unique_ptr<uint8_t[]> buffer = nullptr;
					Size2D size;
					const auto count = fl->GetChannelCount();

					for (const auto& i : fl->GetChannelList()) {
						if (const auto mip = fl->ReadMip(i, 0); mip && mip->GetData()) {
							const auto [r, g, b] = fl->GetChannelColor(i);

							if (!buffer) {
								size = mip->GetSize();
								buffer = std::make_unique<uint8_t[]>(size.x * size.y * 3);
							}

							for (auto j = 0; j < size.x * size.y; j++) {
								const auto value = std::min(static_cast<unsigned short>(255), mip->GetData()[j]);
								buffer[j * 3] = buffer[j * 3] + static_cast<uint8_t>(value * r / 255 / count);
								buffer[j * 3 + 1] = buffer[j * 3 + 1] + static_cast<uint8_t>(value * g / 255 / count);
								buffer[j * 3 + 2] = buffer[j * 3 + 2] + static_cast<uint8_t>(value * b / 255 / count);
							}
						}
					}

					if (buffer)
						return QPixmap::fromImage({ buffer.get(), size.x, size.y, size.x * 3, QImage::Format_RGB888 });
				}
				break;
			case Modality::BF:
				if (const auto bf = reader.GetBF()) {
					if (const auto mip = bf->Read(0); mip && mip->GetData()) {
						const auto [x, y] = mip->GetSize();
						SwapXYRGB(mip->GetData(), x, y);
						return QPixmap::fromImage({ mip->GetData(), x, y, x * 3, QImage::Format_RGB888 });
					}
				}
				break;
		}

		return {};
	}

	Preview::Preview(QWidget* parent) : QWidget(parent), IGalleryReader(), d(new Impl) {
		d->ui.setupUi(this);
		d->ui.gallery->SetVisibleArea(d->ui.scrollArea->verticalScrollBar(), d->ui.scrollWidget);
		d->ui.gallery->SetReader(this);
		d->accent = d->DrawColor(QColor(255, 255, 255, 45));
	}

	Preview::~Preview() {
		d->list.clear();
		d->pool.waitForDone();
	}

	auto Preview::AddTCF(const QString& filepath) -> void {
		d->list.push_back(filepath);

		switch (const File::TCFReader reader(filepath); d->modality) {
			case Modality::HT:
				if (reader.ContainsHT())
					d->readable.push_back(filepath);
				break;
			case Modality::FL:
				if (reader.ContainsFL())
					d->readable.push_back(filepath);
				break;
			case Modality::BF:
				if (reader.ContainsBF())
					d->readable.push_back(filepath);
				break;
		}

		d->ui.gallery->Repaint();
	}

	auto Preview::AddTCF(const QStringList& filelist) -> void {
		for (const auto& f : filelist)
			AddTCF(f);
	}

	auto Preview::RemoveTCF(const QString& filepath) -> void {
		d->list.removeOne(filepath);
		d->readable.removeOne(filepath);
		d->pixmap.remove(filepath);
		d->ui.gallery->Repaint();
	}

	auto Preview::ClearTCF() -> void {
		d->list.clear();
		d->readable.clear();
		d->pixmap.clear();
		d->ui.gallery->Repaint();
	}

	auto Preview::SetModality(Modality modality) -> void {
		d->modality = modality;
		const auto list = d->list;
		ClearTCF();
		AddTCF(list);
	}

	auto Preview::SetMultiSelective(bool multi) -> void {
		d->multi = multi;
	}

	auto Preview::GetSelected() const -> QStringList {
		QStringList list;

		for (auto i = 0; i < d->selectedIndex.count(); i++)
			list.push_back(d->readable[d->selectedIndex[i]]);

		return list;
	}

	auto Preview::GetModality() const -> Modality {
		return d->modality;
	}

	auto Preview::IsMultiSelective() const -> bool {
		return d->multi;
	}

	auto Preview::SetColumnCount(int count) -> void {
		d->ui.gallery->SetColumnCount(count);
	}

	auto Preview::GetColumnCount() const -> int {
		return d->ui.gallery->GetColumnCount();
	}

	auto Preview::GetImageCount() const -> int {
		return d->readable.count();
	}

	auto Preview::GetLayerCount() const -> int {
		return 3;
	}

	auto Preview::ContainsImage(int index) const -> bool {
		return d->pixmap.contains(d->readable[index]) && !d->pixmap[d->readable[index]].isNull();
	}

	auto Preview::ContainsLayer(int imageIndex, int layerIndex) const -> bool {
		switch (layerIndex) {
			case 0:
				return d->hoveredIndex == imageIndex;
			case 1:
				return d->pressedIndex == imageIndex;
			case 2:
				return d->selectedIndex.contains(imageIndex);
		}

		return false;
	}

	auto Preview::GetImage(int index) const -> QPixmap {
		if (d->pixmap.contains(d->readable[index]))
			return d->pixmap[d->readable[index]];

		return {};
	}

	auto Preview::GetLayer(int imageIndex, int layerIndex) const -> QPixmap {
		switch (layerIndex) {
			case 0:
				if (d->hoveredIndex == imageIndex)
					return d->accent;
				break;
			case 1:
				if (d->pressedIndex == imageIndex)
					return d->accent;
				break;
			case 2:
				if (d->selectedIndex.contains(imageIndex))
					return d->accent;
				break;
		}

		return {};
	}

	auto Preview::OnMouseHovered(int index, bool hovered) -> void {
		d->hoveredIndex = (hovered) ? index : -1;
		d->ui.gallery->Repaint();

		if (index > -1 && index < d->list.count() && hovered)
			setToolTip(d->list[index]);
		else
			setToolTip({});
	}

	auto Preview::OnMousePressed(int index, bool pressed, Qt::MouseButton button) -> void {
		if (button == Qt::LeftButton) {
			d->pressedIndex = (pressed) ? index : -1;
			d->ui.gallery->Repaint();
		}
	}

	auto Preview::OnClicked(int index, Qt::MouseButton button) -> void {
		if (button == Qt::LeftButton) {
			const auto keyboard = QGuiApplication::keyboardModifiers();

			if (const auto shift = keyboard.testFlag(Qt::ShiftModifier); d->multi && shift) {
				const auto prev = d->selectedIndex.isEmpty() ? 0 : d->selectedIndex.last();

				for (auto i = std::min(prev, index); i < std::max(prev, index + 1); i++) {
					if (!d->selectedIndex.contains(i))
						d->selectedIndex.push_back(i);
				}
			} else if (const auto ctrl = keyboard.testFlag(Qt::ControlModifier); d->multi && ctrl) {
				if (d->selectedIndex.contains(index))
					d->selectedIndex.removeOne(index);
				else
					d->selectedIndex.push_back(index);
			} else {
				d->selectedIndex.clear();
				d->selectedIndex.push_back(index);
			}

			d->ui.gallery->Repaint();

			QStringList list;
			for (auto i = 0; i < d->selectedIndex.count(); i++)
				list.push_back(d->readable[d->selectedIndex[i]]);
			emit Selected(list);
		}
	}

	auto Preview::OnImageResized(int size) -> void {
		d->empty = d->DrawImage(size, QIcon(":/Flat/NoImage.svg"));
	}

	auto Preview::OnVisibleImageChanged(int from, int to) -> void {
		for (auto i = from; i < to && i < d->readable.count(); i++) {
			if (d->pixmap.contains(d->readable[i]))
				continue;

			d->pixmap[d->readable[i]];

			d->pool.start([this, tcf = d->readable[i]] {
				if (!d->list.contains(tcf))
					return;

				const File::TCFReader reader(tcf);
				if (auto thumbnail = d->ReadThumbnail(reader, d->modality); !thumbnail.isNull())
					d->pixmap[tcf] = std::move(thumbnail);
				else if (auto mip = d->ReadMip(reader, d->modality); !mip.isNull())
					d->pixmap[tcf] = std::move(mip);
				else
					d->pixmap[tcf] = d->empty;

				if (d->pixmap.contains(tcf))
					d->ui.gallery->Repaint();
			});
		}
	}
}
