#include <QGuiApplication>
#include <QIcon>
#include <QPainter>
#include <QThreadPool>

#include "ViewerWidget.h"

#include "ImageHelper.h"
#include "TCFReader.h"

namespace CellAnalyzer::IO::Widget {
	struct ViewerWidget::Impl {
		QThreadPool pool;
		QStringList list;
		QMap<QString, QPixmap> pixmap;
		Modality modality = Modality::HT;
		bool multi = false;

		QVector<int> timelapseIndex;
		QVector<int> selectedIndex;
		int hoveredIndex = -1;
		int pressedIndex = -1;

		QPixmap crashed;
		QPixmap missing;
		QPixmap accent;
		QPixmap timelapse;

		static auto IsTimelapse(const File::TCFReader& reader, Modality modality) -> bool;
		static auto ReadThumbnail(const File::TCFReader& reader, Modality modality) -> QPixmap;
		static auto ReadMip(const File::TCFReader& reader, Modality modality) -> QPixmap;
	};

	auto ViewerWidget::Impl::IsTimelapse(const File::TCFReader& reader, Modality modality) -> bool {
		switch (modality) {
			case Modality::HT:
				if (const auto ht = reader.GetHT())
					return ht->GetDataCount() > 1;
			case Modality::FL:
				if (const auto fl = reader.GetFL()) {
					if (const auto channels = fl->GetChannelList(); !channels.isEmpty())
						return fl->GetDataCount(channels.first()) > 1;
				}
			case Modality::BF:
				if (const auto bf = reader.GetBF())
					return bf->GetDataCount() > 1;
		}

		return false;
	}

	auto ViewerWidget::Impl::ReadThumbnail(const File::TCFReader& reader, Modality modality) -> QPixmap {
		switch (modality) {
			case Modality::HT:
				if (const auto ht = reader.GetHT()) {
					if (const auto thumb = ht->ReadThumbnail(0)) {
						const auto [rgb, x, y] = thumb->GetSize();
						auto* buffer = const_cast<uint8_t*>(thumb->ReadAll());

						return QPixmap::fromImage({ buffer, x, y, x, QImage::Format_Grayscale8 });
					}
				}
				break;
			case Modality::FL:
				if (const auto fl = reader.GetFL()) {
					if (const auto thumb = fl->ReadThumbnail(0)) {
						const auto [rgb, x, y] = thumb->GetSize();
						auto* buffer = const_cast<uint8_t*>(thumb->ReadAll());

						ToQImageOrder(thumb);

						return QPixmap::fromImage({ buffer, x, y, x * 3, QImage::Format_RGB888 });
					}
				}
				break;
			case Modality::BF:
				if (const auto bf = reader.GetBF()) {
					if (const auto thumb = bf->ReadThumbnail(0)) {
						const auto [rgb, x, y] = thumb->GetSize();
						auto* buffer = const_cast<uint8_t*>(thumb->ReadAll());

						ToQImageOrder(thumb);

						return QPixmap::fromImage({ buffer, x, y, x * 3, QImage::Format_RGB888 });
					}
				}
				break;
		}

		return {};
	}

	auto ViewerWidget::Impl::ReadMip(const File::TCFReader& reader, Modality modality) -> QPixmap {
		switch (modality) {
			case Modality::HT:
				if (const auto ht = reader.GetHT()) {
					if (const auto mip = ht->ReadMip(0)) {
						const auto [rgb, x, y] = mip->GetSize();
						const auto buffer = ToUInt8(mip);

						return QPixmap::fromImage({ buffer.get(), x, y, x, QImage::Format_Grayscale8 });
					}
				}
				break;
			case Modality::FL:
				if (const auto fl = reader.GetFL()) {
					std::unique_ptr<uint8_t[]> buffer = nullptr;
					Size2D size;

					for (const auto& i : fl->GetChannelList()) {
						if (const auto mip = fl->ReadMip(i, 0)) {
							const auto [r, g, b] = fl->GetChannelColor(i);

							if (!buffer) {
								size = mip->GetSize();
								buffer = std::make_unique<uint8_t[]>(size.ToSize2D() * 3);
							}

							for (auto j = 0; j < size.ToSize2D(); j++) {
								const auto value = std::min(static_cast<uint16_t>(255), mip->ReadOne());

								buffer[j * 3] = buffer[j * 3] + static_cast<uint8_t>(value * (r / 255.0));
								buffer[j * 3 + 1] = buffer[j * 3 + 1] + static_cast<uint8_t>(value * (g / 255.0));
								buffer[j * 3 + 2] = buffer[j * 3 + 2] + static_cast<uint8_t>(value * (b / 255.0));
							}
						}
					}

					if (buffer)
						return QPixmap::fromImage({ buffer.get(), size.x, size.y, size.x * 3, QImage::Format_RGB888 });
				}
				break;
			case Modality::BF:
				if (const auto bf = reader.GetBF()) {
					if (const auto mip = bf->Read(0)) {
						const auto [rgb, x, y] = mip->GetSize();
						auto* buffer = const_cast<uint8_t*>(mip->ReadAll());

						ToQImageOrder(mip);

						return QPixmap::fromImage({ buffer, x, y, x * 3, QImage::Format_RGB888 });
					}
				}
				break;
		}

		return {};
	}

	ViewerWidget::ViewerWidget(QWidget* parent) : GalleryWidget(parent), d(new Impl) {
		d->accent = QPixmap(1, 1);
		d->accent.fill(QColor(255, 255, 255, 45));
		d->pool.setMaxThreadCount(1);

		SetLayerCount(4);
	}

	ViewerWidget::~ViewerWidget() {
		d->list.clear();
		d->pool.waitForDone();
	}

	auto ViewerWidget::SetTCFList(const QStringList& filelist) -> void {
		d->pixmap.clear();
		d->timelapseIndex.clear();
		d->list = filelist;
		SetImageCount(d->list.count());

		Update();
	}

	auto ViewerWidget::SetModality(Modality modality) -> void {
		d->modality = modality;
		d->timelapseIndex.clear();
		d->pixmap.clear();

		OnImageSizeChanged(GetImageSize());
		Update();
	}

	auto ViewerWidget::SetMultiSelective(bool multi) -> void {
		d->multi = multi;
	}

	auto ViewerWidget::GetSelected() const -> QStringList {
		QStringList list;

		for (const auto i : d->selectedIndex)
			list.push_back(d->list[i]);

		return list;
	}

	auto ViewerWidget::GetModality() const -> Modality {
		return d->modality;
	}

	auto ViewerWidget::IsMultiSelective() const -> bool {
		return d->multi;
	}

	auto ViewerWidget::Update() -> void {
		const auto range = GetVisibleRange();

		for (auto i = range.x(); i < range.y(); i++) {
			if (d->pixmap.contains(d->list[i]))
				continue;

			d->pixmap[d->list[i]];
			d->pool.start([this, tcf = d->list[i], i, modality = d->modality] {
				if (!d->list.contains(tcf) || modality != d->modality)
					return;

				const File::TCFReader reader(tcf);
				QPixmap pixmap;

				if (!reader.IsReadable())
					pixmap = d->crashed;
				else if (auto thumbnail = d->ReadThumbnail(reader, d->modality); !thumbnail.isNull())
					pixmap = std::move(thumbnail);
				else if (auto mip = d->ReadMip(reader, d->modality); !mip.isNull())
					pixmap = std::move(mip);
				else
					pixmap = d->missing;

				if (d->IsTimelapse(reader, d->modality))
					d->timelapseIndex.push_back(i);

				if (d->list.contains(tcf) && modality == d->modality) {
					d->pixmap[tcf] = std::move(pixmap);
					update();
				}
			});
		}
	}

	auto ViewerWidget::OnMousePressed(int index, bool pressed, Qt::MouseButton button) -> void {
		if (button == Qt::LeftButton) {
			d->pressedIndex = (pressed) ? index : -1;
			update();
		}
	}

	auto ViewerWidget::OnMouseHovered(int index, bool hovered) -> void {
		d->hoveredIndex = (hovered) ? index : -1;
		update();

		if (index > -1 && index < d->list.count() && hovered)
			setToolTip(d->list[index]);
		else
			setToolTip({});
	}

	auto ViewerWidget::OnMouseClicked(int index, Qt::MouseButton button) -> void {
		if (button == Qt::LeftButton) {
			const auto keyboard = QGuiApplication::keyboardModifiers();

			if (const auto shift = keyboard.testFlag(Qt::ShiftModifier); d->multi && shift) {
				const auto prev = d->selectedIndex.isEmpty() ? 0 : d->selectedIndex.last();

				for (auto i = std::min(prev, index); i < std::max(prev, index + 1); i++) {
					if (!d->selectedIndex.contains(i))
						d->selectedIndex.push_back(i);
				}
			} else if (const auto ctrl = keyboard.testFlag(Qt::ControlModifier); d->multi && ctrl) {
				if (d->selectedIndex.contains(index))
					d->selectedIndex.removeOne(index);
				else
					d->selectedIndex.push_back(index);
			} else {
				d->selectedIndex.clear();
				d->selectedIndex.push_back(index);
			}

			update();

			QStringList list;

			for (const auto i : d->selectedIndex)
				list.push_back(d->list[i]);

			emit TCFSelected(list);
		}
	}

	auto ViewerWidget::OnVisibleRangeChanged(int from, int to) -> void {
		Update();
	}

	auto ViewerWidget::OnImageSizeChanged(int size) -> void {
		auto font = this->font();
		font.setPixelSize(size / 10);

		{
			QPixmap pixmap(size, size);
			pixmap.fill(QColor(33, 33, 33));

			QPainter painter(&pixmap);
			painter.setPen(Qt::white);
			painter.setFont(font);
			painter.drawPixmap(size / 3, size / 4, size / 3, size / 3, QIcon(":/Flat/Warning.svg").pixmap(size / 3, size / 3));
			painter.drawText(QRect(0, size / 4 + size / 3 + 10, size, size - (size / 4 + size / 3)), Qt::AlignHCenter | Qt::AlignTop, "TCF is crashed!");
			painter.end();

			d->crashed = pixmap;
		}

		{
			QPixmap pixmap(size, size);
			pixmap.fill(QColor(33, 33, 33));

			QPainter painter(&pixmap);
			painter.setPen(Qt::white);
			painter.setFont(font);
			painter.drawPixmap(size / 3, size / 4, size / 3, size / 3, QIcon(":/Flat/Missing.svg").pixmap(size / 3, size / 3));
			painter.drawText(QRect(0, size / 4 + size / 3 + 10, size, size - (size / 4 + size / 3)), Qt::AlignHCenter | Qt::AlignTop, "No " + ToString(d->modality));
			painter.end();

			d->missing = pixmap;
		}

		{
			QPixmap pixmap(size, size);
			pixmap.fill(Qt::transparent);

			const auto iconSize = std::min(30, static_cast<int>(size * 0.2));
			const auto pos = std::min(7, static_cast<int>(size * 0.05));

			QPainter painter(&pixmap);
			painter.setPen(QColor(48, 66, 115));
			painter.setBrush(QColor(97, 133, 232));
			painter.drawRect(pos, pos, iconSize, iconSize);

			auto font2 = painter.font();
			font2.setBold(true);
			font2.setPixelSize(static_cast<int>(iconSize * 0.8));
			painter.setPen(QColor(255, 255, 255));
			painter.setFont(font2);
			painter.drawText(pos, pos, iconSize, iconSize, Qt::AlignCenter, "T");
			painter.end();

			d->timelapse = pixmap;
		}
	}

	bool ViewerWidget::OnImageExists(int index) const {
		return d->pixmap.contains(d->list[index]) && !d->pixmap[d->list[index]].isNull();
	}

	auto ViewerWidget::OnImageRequested(int index) const -> QPixmap {
		if (d->pixmap.contains(d->list[index]))
			return d->pixmap[d->list[index]];

		return {};
	}

	auto ViewerWidget::OnLayerRequested(int index, int depth) const -> QPixmap {
		switch (depth) {
			case 0:
				if (d->hoveredIndex == index)
					return d->accent;
				break;
			case 1:
				if (d->pressedIndex == index)
					return d->accent;
				break;
			case 2:
				if (d->selectedIndex.contains(index))
					return d->accent;
				break;
			case 3:
				if (d->timelapseIndex.contains(index))
					return d->timelapse;
				break;
		}

		return {};
	}
}
