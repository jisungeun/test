#pragma once

#include "GalleryWidget.h"

#include "CellAnalyzer.IO.Widget.TCFDialogExport.h"

namespace CellAnalyzer::IO::Widget {
	enum class Modality {
		HT,
		FL,
		BF
	};

	class CellAnalyzer_IO_Widget_TCFDialog_API ViewerWidget final : public UI::Widget::GalleryWidget {
		Q_OBJECT

	public:
		explicit ViewerWidget(QWidget* parent = nullptr);
		~ViewerWidget() override;

		auto SetTCFList(const QStringList& filelist) -> void;
		auto SetModality(Modality modality) -> void;
		auto SetMultiSelective(bool multi) -> void;

		auto GetSelected() const -> QStringList;
		auto GetModality() const -> Modality;
		auto IsMultiSelective() const -> bool;

		auto Update() -> void;

	signals:
		auto TCFSelected(const QStringList& filelist) -> void;

	protected:
		auto OnMousePressed(int index, bool pressed, Qt::MouseButton button) -> void override;
		auto OnMouseHovered(int index, bool hovered) -> void override;
		auto OnMouseClicked(int index, Qt::MouseButton button) -> void override;
		auto OnVisibleRangeChanged(int from, int to) -> void override;
		auto OnImageSizeChanged(int size) -> void override;

		auto OnImageExists(int index) const -> bool override;
		auto OnImageRequested(int index) const -> QPixmap override;
		auto OnLayerRequested(int index, int depth) const -> QPixmap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	static auto ToString(Modality modality) -> QString {
		switch (modality) {
			case Modality::HT:
				return "HT";
			case Modality::FL:
				return "FL";
			case Modality::BF:
				return "BF";
		}

		return {};
	}

	static auto ToModality(const QString& modality) -> Modality {
		if (modality == "HT")
			return Modality::HT;
		if (modality == "FL")
			return Modality::FL;
		if (modality == "BF")
			return Modality::BF;

		return Modality::HT;
	}
}
