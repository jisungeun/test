#pragma once

#include <QDialog>

#include "IView.h"

#include "CellAnalyzer.IO.Widget.TCFDialogExport.h"

namespace CellAnalyzer::IO::Widget {
	class CellAnalyzer_IO_Widget_TCFDialog_API TCFDialog final : public QDialog, public IView {
	public:
		explicit TCFDialog();
		~TCFDialog() override;

		auto SetMultiSelective(bool multi) -> void;
		auto GetSelected() const -> QStringList;

	protected slots:
		auto OnPathBtnClicked() -> void;
		auto OnSelectBtnClicked() -> void;
		auto OnHtBtnClicked() -> void;
		auto OnFlBtnClicked() -> void;
		auto OnBfBtnClicked() -> void;

		auto OnCountSpinChanged(int value) -> void;
		auto OnTreeSelected(const QModelIndex& current, const QModelIndex& previous) -> void;
		auto OnGallerySelected(const QStringList& files) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
