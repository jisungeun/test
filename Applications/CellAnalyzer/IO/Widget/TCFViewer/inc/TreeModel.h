#pragma once

#include <memory>

#include <QAbstractItemModel>

#include "CellAnalyzer.IO.Widget.TCFDialogExport.h"

namespace CellAnalyzer::IO::Widget {
	class CellAnalyzer_IO_Widget_TCFDialog_API TreeModel final : public QAbstractItemModel {
	public:
		TreeModel();
		~TreeModel() override;

		auto GetPath(const QModelIndex& index) const -> QString;
		auto GetTCFList(const QString& path) const -> QStringList;
		auto GetIndex(const QString& path) const -> QModelIndex;

	protected:
		auto index(int row, int column, const QModelIndex& parent) const -> QModelIndex override;
		auto parent(const QModelIndex& child) const -> QModelIndex override;
		auto rowCount(const QModelIndex& parent) const -> int override;
		auto columnCount(const QModelIndex& parent) const -> int override;
		auto data(const QModelIndex& index, int role) const -> QVariant override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
