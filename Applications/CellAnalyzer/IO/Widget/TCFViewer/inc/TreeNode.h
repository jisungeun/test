#pragma once

#include <memory>

#include <QAbstractItemModel>

#include "CellAnalyzer.IO.Widget.TCFDialogExport.h"

namespace CellAnalyzer::IO::Widget {
	class Node;
	using NodeChild = std::shared_ptr<Node>;
	using NodeChildren = QVector<NodeChild>;
	using NodeParent = Node*;

	enum class ScanState {
		Scannable,
		Scanning,
		Scanned
	};

	class CellAnalyzer_IO_Widget_TCFDialog_API Node {
	public:
		Node();
		Node(const QString& path, const QStringList& tcfList, const NodeParent& parent);
		~Node();

		auto GetPath() const -> QString;
		auto GetParent() const -> NodeParent;
		auto GetChild(int index) const -> NodeChild;
		auto GetChildren() const -> NodeChildren;
		auto GetChildCount() const -> int;
		auto IsChildEmpty() const -> bool;
		auto GetState() const -> ScanState;
		auto GetTCFList() const -> QStringList;

		auto GetIndex() const -> int;
		auto GetName() const -> QString;

		auto Scan() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
