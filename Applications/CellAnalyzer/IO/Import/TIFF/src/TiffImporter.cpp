#include <tiffio.h>

#include "TiffImporter.h"

#include "BinaryMask2D.h"
#include "BinaryMask3D.h"
#include "ExtendedTIFF.h"
#include "FL2D.h"
#include "FL3D.h"
#include "Float2D.h"
#include "Float3d.h"
#include "HT2D.h"
#include "HT3D.h"
#include "LabelMask2D.h"
#include "LabelMask3D.h"

namespace CellAnalyzer::IO::Import {
	struct TiffImporter::Impl { };

	TiffImporter::TiffImporter() : IImporter(), d(new Impl) { }

	TiffImporter::~TiffImporter() = default;

	auto TiffImporter::GetName() const -> QString {
		return "TIFF";
	}

	auto TiffImporter::GetFormat() const -> QString {
		return "tiff";
	}

	auto TiffImporter::Import(const QString& filepath, const DataFlags& flags) -> DataPtr {
		if (auto* tiff = TIFFOpen(filepath.toUtf8(), "r")) {
			uint32_t x = 0;
			uint32_t y = 0;
			uint32_t z = 0;
			auto rx = 0.0f;
			auto ry = 0.0f;
			auto rz = 0.0;
			auto px = 0.0f;
			auto py = 0.0f;
			auto pz = 0.0f;
			uint16_t curPage;
			uint16_t pageCount;
			uint16_t size = 0;
			uint16_t format = 0;
			uint16_t max = 0;
			uint16_t min = 0;
			auto dmax = 0.0;
			auto dmin = 0.0;
			char* desc { nullptr };

			TIFFGetField(tiff, TIFFTAG_BITSPERSAMPLE, &size);
			TIFFGetField(tiff, TIFFTAG_IMAGEWIDTH, &x);
			TIFFGetField(tiff, TIFFTAG_IMAGELENGTH, &y);
			TIFFGetField(tiff, TIFFTAG_IMAGEDESCRIPTION, &desc);
			if (desc) {
				QString descString(desc);
				for (const auto token : descString.split('\n')) {
					if (token.contains("slices") && token.split('=').count() > 1) {
						z = static_cast<uint32_t>(token.split('=')[1].toDouble(nullptr));
						break;
					}
				}
			} else {
				z = 1;
			}
			TIFFGetField(tiff, TIFFTAG_SAMPLEFORMAT, &format);
			TIFFGetField(tiff, TIFFTAG_XRESOLUTION, &rx);
			TIFFGetField(tiff, TIFFTAG_YRESOLUTION, &ry);
			TIFFGetField(tiff, TIFFTAG_ZRESOLUTION, &rz);
			TIFFGetField(tiff, TIFFTAG_XPOSITION, &px);
			TIFFGetField(tiff, TIFFTAG_YPOSITION, &py);
			TIFFGetField(tiff, TIFFTAG_MINSAMPLEVALUE, &min);
			TIFFGetField(tiff, TIFFTAG_MAXSAMPLEVALUE, &max);
			TIFFGetField(tiff, TIFFTAG_SMINSAMPLEVALUE, &dmin);
			TIFFGetField(tiff, TIFFTAG_SMAXSAMPLEVALUE, &dmax);
			TIFFGetField(tiff, TIFFTAG_PAGENUMBER, &curPage, &pageCount);
			size = size / 8;
			rx = 10000.0f / rx;
			ry = 10000.0f / ry;
			rz = 10000.0f / rz;
			dmin = dmin == 0.0 ? min / 10000.0f : dmin / 10000.0f;
			dmax = dmax == 0.0 ? max / 10000.0f : dmax / 10000.0f;

			const std::shared_ptr buffer = std::make_unique<uint8_t[]>(z > 0 ? x * y * z * size : x * y * size);
			pageCount = z;
			for (uint16_t i = 0; i < std::max(pageCount, static_cast<uint16_t>(1)); i++) {
				for (uint16_t j = 0; j < y; j++)
					TIFFReadScanline(tiff, buffer.get() + (j * x * size) + (i * y * x * size), j);

				if (pageCount > 0)
					TIFFReadDirectory(tiff);
			}

			TIFFClose(tiff);

			if (flags.testFlag(DataFlag::Volume2D)) {
				if (flags.testFlag(DataFlag::HT))
					return std::make_shared<Data::HT2D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), Size2D { static_cast<int>(x), static_cast<int>(y) }, Resolution2D { rx, ry }, Origin2D { px, py }, RIRange { dmin, dmax });
				if (flags.testFlag(DataFlag::FL))
					return std::make_shared<Data::FL2D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), 0, QString(), Size2D { static_cast<int>(x), static_cast<int>(y) }, Resolution2D { rx, ry }, Origin2D { px, py }, FLIntensity { min, max }, 0, 0);
				if (flags.testFlag(DataFlag::Binary))
					return std::make_shared<Data::BinaryMask2D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), Size2D { static_cast<int>(x), static_cast<int>(y) }, Resolution2D { rx, ry }, Origin2D { px, py });
				if (flags.testFlag(DataFlag::Label))
					return std::make_shared<Data::LabelMask2D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), max, Size2D { static_cast<int>(x), static_cast<int>(y) }, Resolution2D { rx, ry }, Origin2D { px, py });


				return std::make_shared<Data::Float2D>(std::reinterpret_pointer_cast<float_t[]>(buffer), Size2D { static_cast<int>(x), static_cast<int>(y) }, Resolution2D { rx, ry }, Origin2D { px, py }, std::tuple<float, float>(dmin, dmax), 0);
			}

			if (flags.testFlag(DataFlag::Volume3D)) {
				if (flags.testFlag(DataFlag::HT))
					return std::make_shared<Data::HT3D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), Size3D { static_cast<int>(x), static_cast<int>(y) }, Resolution3D { rx, ry, rz }, Origin3D { px, py, pz }, RIRange { dmin, dmax });
				if (flags.testFlag(DataFlag::FL))
					return std::make_shared<Data::FL3D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), 0, QString(), Size3D { static_cast<int>(x), static_cast<int>(y), static_cast<int>(z) }, Resolution3D { rx, ry, rz }, Origin3D { px, py, pz }, FLIntensity { min, max }, 0, 0);
				if (flags.testFlag(DataFlag::Binary))
					return std::make_shared<Data::BinaryMask3D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), Size3D { static_cast<int>(x), static_cast<int>(y), static_cast<int>(z) }, Resolution3D { rx, ry, rz }, Origin3D { px, py, pz });
				if (flags.testFlag(DataFlag::Label))
					return std::make_shared<Data::LabelMask3D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), max, Size3D { static_cast<int>(x), static_cast<int>(y), static_cast<int>(z) }, Resolution3D { rx, ry, rz }, Origin3D { px, py, pz });

				return std::make_shared<Data::Float3D>(std::reinterpret_pointer_cast<float_t[]>(buffer), Size3D { static_cast<int>(x), static_cast<int>(y), static_cast<int>(z) }, Resolution3D { rx, ry, rz }, Origin3D { px, py, pz }, std::tuple<float, float>(dmin, dmax), 0);
			}
		}

		return {};
	}
}
