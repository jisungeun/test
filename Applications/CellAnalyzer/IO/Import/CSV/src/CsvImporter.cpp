#include <QVariantMap>
#include <QTextStream>
#include <QDir>
#include <fstream>
#include <QFileInfo>

#include "IMeasure.h"
#include "Measure.h"
#include "CsvImporter.h"

namespace CellAnalyzer::IO::Import {
	struct CsvImporter::Impl {
		auto RemoveQuotesIfNeeded(const QString& str) -> QString;
		auto StringToDouble(const QString& str) -> double;
	};

	auto CsvImporter::Impl::StringToDouble(const QString& str) -> double {
		bool ok;
		double value = str.toDouble(&ok);

		if (false == ok) {
			if (str.toLower() == "inf") {
				value = std::numeric_limits<double>::infinity();
			} else {
				value = -1;
			}
		}

		return value;
	}

	auto CsvImporter::Impl::RemoveQuotesIfNeeded(const QString& str) -> QString {
		if (str.startsWith('"') && str.endsWith('"')) {
			return str.mid(1, str.length() - 2);
		}
		return str;
	}

	CsvImporter::CsvImporter() : IImporter(), d(new Impl) {}

	CsvImporter::~CsvImporter() = default;

	auto CsvImporter::GetName() const -> QString {
		return "Csv File";
	}

	auto CsvImporter::GetFormat() const -> QString {
		return "csv";
	}

	auto CsvImporter::Import(const QString& filepath, const DataFlags& flags) -> DataPtr {
		QFile file(filepath);

		if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
			QVariantMap measureData;

			QTextStream in(&file);

			const auto headerLine = in.readLine();
			auto headers = headerLine.split(",");
			auto time_step_idx = -1;
			for (int i = 0; i < headers.size(); ++i) {
				headers[i] = d->RemoveQuotesIfNeeded(headers[i]);
				if (headers[i] == "TimeStep") {
					time_step_idx = i;
				}
			}

			int time_step = 0;
			int measureIdx = 0;
			while (!in.atEnd()) {
				QVariantMap singleData;
				QString line = in.readLine();
				QStringList fields = line.split(",");
				for (auto i = 2; i < fields.count(); i++) {
					const auto value = d->StringToDouble(fields[i]);
					if (i == time_step_idx) {
						time_step = static_cast<int>(value);
					}
					singleData[headers[i]] = value;
				}
				measureData[QString::number(measureIdx++)] = singleData;
			}
			if (headers.contains("-parentidx")) {
				measureData["LabelType"] = "DualLabel";
			} else {
				measureData["LabelType"] = "SingleLabel";
			}
			file.close();
			return std::make_shared<Data::Measure>(measureData, time_step);
		}
		return {};
	}
}
