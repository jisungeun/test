#include "RawImporter.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>

#include "BinaryMask2D.h"
#include "BinaryMask3D.h"
#include "FL2D.h"
#include "FL3D.h"
#include "Float2D.h"
#include "Float3D.h"
#include "HT2D.h"
#include "HT3D.h"
#include "IVolume2D.h"
#include "IVolume3D.h"
#include "LabelMask2D.h"
#include "LabelMask3D.h"

namespace CellAnalyzer::IO::Import {
	struct RawImporter::Impl {
		static auto GetMHD(const QString& filepath) -> QVariantMap;
		static auto GetSize(const QVariantList& list) -> std::tuple<int, int, int>;
		static auto GetResolution(const QVariantList& list) -> std::tuple<double, double, double>;
		static auto GetRangeDouble(const QVariantList& list) -> std::tuple<double, double>;
		static auto GetRange(const QVariantList& list) -> std::tuple<uint16_t, uint16_t>;
	};

	auto RawImporter::Impl::GetMHD(const QString& filepath) -> QVariantMap {
		const QFileInfo info(filepath);
		const auto mhdPath = QDir(info.absolutePath()).filePath(info.completeBaseName()) + ".mhd";
		QVariantMap mhd;

		if (QFile mhdFile(mhdPath); mhdFile.open(QIODevice::ReadOnly)) {
			const auto properties = mhdFile.readAll();
			const auto splits = properties.split('\n');
			const auto splitter = QByteArray(" = ");

			for (auto& s : splits) {
				const auto idx = s.indexOf(splitter);
				const auto key = s.mid(0, idx);
				const auto value = s.trimmed().mid(idx + splitter.count());

				if (const auto array = value.split(' '); array.count() == 1)
					mhd[key] = value;
				else {
					QVariantList list;

					for (const auto& ar : array) {
						auto ok = false;

						if (const auto i = ar.toInt(&ok); ok)
							list.push_back(i);
						else if (const auto j = ar.toDouble(&ok); ok)
							list.push_back(j);
						else if (ar == "True")
							list.push_back(true);
						else if (ar == "False")
							list.push_back(false);
						else
							list.push_back(ar);
					}

					mhd[key] = list;
				}
			}
		}

		return mhd;
	}

	auto RawImporter::Impl::GetSize(const QVariantList& list) -> std::tuple<int, int, int> {
		if (list.count() == 2)
			return { list[0].toInt(), list[1].toInt(), 0 };

		if (list.count() == 3)
			return { list[0].toInt(), list[1].toInt(), list[2].toInt() };

		return {};
	}

	auto RawImporter::Impl::GetResolution(const QVariantList& list) -> std::tuple<double, double, double> {
		if (list.count() == 2)
			return { list[0].toDouble(), list[1].toDouble(), 0.0 };

		if (list.count() == 3)
			return { list[0].toDouble(), list[1].toDouble(), list[2].toDouble() };

		return {};
	}

	auto RawImporter::Impl::GetRangeDouble(const QVariantList& list) -> std::tuple<double, double> {
		if (list.count() == 2)
			return { list[0].toDouble(), list[1].toDouble() };

		return {};
	}

	auto RawImporter::Impl::GetRange(const QVariantList& list) -> std::tuple<uint16_t, uint16_t> {
		if (list.count() == 2)
			return { list[0].toInt(), list[1].toInt() };

		return {};
	}

	RawImporter::RawImporter() : IImporter(), d(new Impl) {}

	RawImporter::~RawImporter() = default;

	auto RawImporter::GetName() const -> QString {
		return "Raw Image";
	}

	auto RawImporter::GetFormat() const -> QString {
		return "raw";
	}

	auto RawImporter::Import(const QString& filepath, const DataFlags& flags) -> DataPtr {
		if (const auto mhd = d->GetMHD(filepath); !mhd.isEmpty()) {
			if (QFile file(filepath); file.open(QIODevice::ReadOnly)) {
				const auto [x, y, z] = d->GetSize(mhd["DimSize"].toList());
				const auto [rx, ry, rz] = d->GetResolution(mhd["ElementSpacing"].toList());
				const auto size = z > 0 ? x * y * z * sizeof(uint16_t) : x * y * sizeof(uint16_t);
				const auto [dmin, dmax] = d->GetRangeDouble(mhd["Range"].toList());
				const auto [min, max] = d->GetRange(mhd["Range"].toList());

				std::shared_ptr<char[]> buffer = nullptr;

				if (mhd["ElementType"] == "MET_USHORT")
					buffer = std::make_unique<char[]>(size);
				else
					return {};

				if (const auto read = file.read(buffer.get(), file.size()); read == file.size()) {
					if (flags.testFlag(DataFlag::Volume2D)) {
						if (flags.testFlag(DataFlag::HT))
							return std::make_shared<Data::HT2D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), Size2D { x, y }, Resolution2D { rx, ry }, Origin2D { 0, 0 }, RIRange { dmin, dmax });
						if (flags.testFlag(DataFlag::FL))
							return std::make_shared<Data::FL2D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), 0, QString(), Size2D { x, y }, Resolution2D { rx, ry }, Origin2D { 0, 0 }, FLIntensity { min, max }, 0, 0);
						if (flags.testFlag(DataFlag::Binary))
							return std::make_shared<Data::BinaryMask2D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), Size2D { x, y }, Resolution2D { rx, ry }, Origin2D { 0, 0 });
						if (flags.testFlag(DataFlag::Label))
							return std::make_shared<Data::LabelMask2D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), max, Size2D { x, y }, Resolution2D { rx, ry }, Origin2D { 0, 0 });


						return std::make_shared<Data::Float2D>(std::reinterpret_pointer_cast<float_t[]>(buffer), Size2D { x, y }, Resolution2D { rx, ry }, Origin2D { 0, 0 }, std::tuple<float, float>(dmin, dmax), 0);
					}

					if (flags.testFlag(DataFlag::Volume3D)) {
						if (flags.testFlag(DataFlag::HT))
							return std::make_shared<Data::HT3D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), Size3D { x, y }, Resolution3D { rx, ry }, Origin3D { 0, 0 }, RIRange { dmin, dmax });
						if (flags.testFlag(DataFlag::FL))
							return std::make_shared<Data::FL3D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), 0, QString(), Size3D { x, y, z }, Resolution3D { rx, ry, rz }, Origin3D { 0, 0, 0 }, FLIntensity { min, max }, 0, 0);
						if (flags.testFlag(DataFlag::Binary))
							return std::make_shared<Data::BinaryMask3D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), Size3D { x, y, z }, Resolution3D { rx, ry, rz }, Origin3D { 0, 0, 0 });
						if (flags.testFlag(DataFlag::Label))
							return std::make_shared<Data::LabelMask3D>(std::reinterpret_pointer_cast<uint16_t[]>(buffer), max, Size3D { x, y, z }, Resolution3D { rx, ry, rz }, Origin3D { 0, 0, 0 });

						return std::make_shared<Data::Float3D>(std::reinterpret_pointer_cast<float_t[]>(buffer), Size3D { x, y, z }, Resolution3D { rx, ry, rz }, Origin3D { 0, 0, 0 }, std::tuple<float, float>(dmin, dmax), 0);
					}
				}
			}
		}

		return {};
	}
}
