#pragma once

#include "IImporter.h"

#include "CellAnalyzer.IO.Import.RAWExport.h"

namespace CellAnalyzer::IO::Import {
	class CellAnalyzer_IO_Import_RAW_API RawImporter final : public IImporter {
	public:
		RawImporter();
		~RawImporter() override;

		auto GetName() const -> QString override;
		auto GetFormat() const -> QString override;

		auto Import(const QString& filepath, const DataFlags& flags) -> DataPtr override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
