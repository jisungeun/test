#include "TCRawWriter.h"

#include "RawExporter.h"

#include <QDir>
#include <QFileInfo>

#include "IFL.h"
#include "IHT.h"
#include "ILabeled.h"
#include "IVolume2D.h"
#include "IVolume3D.h"

namespace CellAnalyzer::IO::Export {
	struct RawExporter::Impl { };

	RawExporter::RawExporter() : IExporter(), d(new Impl) {}

	RawExporter::~RawExporter() = default;

	auto RawExporter::GetName() const -> QString {
		return "Raw Image";
	}

	auto RawExporter::GetFormat() const -> QString {
		return "raw";
	}

	auto RawExporter::GetIcon() const -> QString {
		return {};
	}

	auto RawExporter::GetMetadata(const QString& name) const -> QVariant {
		return {};
	}

	auto RawExporter::GetMetadataList() const -> QStringList {
		return {};
	}

	auto RawExporter::IsExportable(const DataFlags& flags) const -> bool {
		if (flags.testFlag(DataFlag::Volume2D) || flags.testFlag(DataFlag::Volume3D))
			return true;

		return false;
	}

	auto RawExporter::SetMetadata(const QString& name, const QVariant& value) -> void {}

	auto RawExporter::Export(const DataPtr& data, const QString& filepath) -> bool {
		bool ok = false;

		if (const auto v = std::dynamic_pointer_cast<IVolume>(data)) {
			std::shared_ptr<TC::IO::RawWriter::Writer> writer = nullptr;
			auto type = TC::IO::RawWriter::Writer::Type::NOTDEFINED;

			switch (v->GetDataType()) {
				case DataType::UInt8:
					type = TC::IO::RawWriter::Writer::Type::UINT8;
					break;
				case DataType::UInt16:
					type = TC::IO::RawWriter::Writer::Type::UINT16;
					break;
				case DataType::Float32:
					type = TC::IO::RawWriter::Writer::Type::FLOAT;
					break;
				case DataType::Float64:
					type = TC::IO::RawWriter::Writer::Type::DOUBLE;
					break;
			}

			if (const auto v2 = std::dynamic_pointer_cast<IVolume2D>(data)) {
				const auto [x, y] = v2->GetSize();
				const auto [rx, ry] = v2->GetResolution();

				if (type != TC::IO::RawWriter::Writer::Type::NOTDEFINED)
					writer = std::make_shared<TC::IO::RawWriter::Writer>(filepath, std::vector<size_t> { static_cast<uint64_t>(x), static_cast<uint64_t>(y) }, std::vector<float> { static_cast<float_t>(rx), static_cast<float_t>(ry) }, type);
			} else if (const auto v3 = std::dynamic_pointer_cast<IVolume3D>(data)) {
				const auto [x, y, z] = v3->GetSize();
				const auto [rx, ry, rz] = v3->GetResolution();

				if (type != TC::IO::RawWriter::Writer::Type::NOTDEFINED)
					writer = std::make_shared<TC::IO::RawWriter::Writer>(filepath, std::vector<size_t> { static_cast<uint64_t>(x), static_cast<uint64_t>(y), static_cast<uint64_t>(z) }, std::vector<float> { static_cast<float_t>(rx), static_cast<float_t>(ry), static_cast<float_t>(rz) }, type);
			}

			if (writer && writer->Append(v->GetDataAs<const char*>(), v->GetDataSize()))
				ok = true;
		}

		if (ok) {
			const QFileInfo info(filepath);
			const auto mhdPath = QDir(info.absolutePath()).filePath(info.completeBaseName()) + ".mhd";

			if (QFile file(mhdPath); file.open(QIODevice::WriteOnly | QIODevice::Append)) {
				if (const auto ht = std::dynamic_pointer_cast<IHT>(data)) {
					const auto [min, max] = ht->GetRI();
					file.write(QString("Range = %1 %2\r\n").arg(min).arg(max).toUtf8());
				} else if (const auto fl = std::dynamic_pointer_cast<IFL>(data)) {
					const auto [min, max] = fl->GetIntensity();
					file.write(QString("Range = %1 %2\r\n").arg(min).arg(max).toUtf8());
				} else if (const auto mask = std::dynamic_pointer_cast<ILabeled>(data)) {
					const auto max = mask->GetMaxIndex();
					file.write(QString("Range = 0 %1\r\n").arg(max).toUtf8());
				}

				return true;
			}
		}

		return false;
	}
}
