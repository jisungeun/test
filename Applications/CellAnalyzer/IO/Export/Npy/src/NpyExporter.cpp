#include "NpyExporter.h"

#include <QDir>
#include <QFileInfo>
#include <fstream>

#include "IFL.h"
#include "IHT.h"
#include "ILabeled.h"
#include "IVolume2D.h"
#include "IVolume3D.h"

#include <PythonNumpyIO.h>

namespace CellAnalyzer::IO::Export {
	struct NpyExporter::Impl { };

	NpyExporter::NpyExporter() : IExporter(), d(new Impl) {}

	NpyExporter::~NpyExporter() = default;

	auto NpyExporter::GetName() const -> QString {
		return "Npy Image";
	}

	auto NpyExporter::GetFormat() const -> QString {
		return "npy";
	}

	auto NpyExporter::GetIcon() const -> QString {
		return {};
	}

	auto NpyExporter::GetMetadata(const QString& name) const -> QVariant {
		return {};
	}

	auto NpyExporter::GetMetadataList() const -> QStringList {
		return {};
	}

	auto NpyExporter::IsExportable(const DataFlags& flags) const -> bool {
		if (flags.testFlag(DataFlag::Volume2D) || flags.testFlag(DataFlag::Volume3D))
			return true;

		return false;
	}

	auto NpyExporter::SetMetadata(const QString& name, const QVariant& value) -> void {}

	auto NpyExporter::Export(const DataPtr& data, const QString& filepath) -> bool {
		bool ok = false;
		double res[3];
		if (const auto v = std::dynamic_pointer_cast<IVolume>(data)) {			
			const auto writer = std::make_shared<TC::IO::TCNumpyWriter>();
			size_t arrSize = 1;
			std::vector<unsigned long> shape;
			if (const auto v2 = std::dynamic_pointer_cast<IVolume2D>(data)) {
				const auto [x, y] = v2->GetSize();
				arrSize = x * y;
				shape.push_back(x);
				shape.push_back(y);
				const auto [rx,ry] = v2->GetResolution();
				res[0] = rx;
				res[1] = ry;
				res[2] = 1;
			} else if (const auto v3 = std::dynamic_pointer_cast<IVolume3D>(data)) {
				const auto [x, y, z] = v3->GetSize();
				arrSize = x * y * z;
				shape.push_back(z);
				shape.push_back(x);
				shape.push_back(y);
				const auto [rx, ry, rz] = v3->GetResolution();
				res[0] = rx;
				res[1] = ry;
				res[2] = rz;
			}

			auto type = TC::IO::NpyArrType::arrBYTE;
			std::any anyArr;

			if(v->GetDataType() == DataType::UInt8) {
				type = TC::IO::NpyArrType::arrUBYTE;
				const auto* ucharArr = v->GetDataAs<const unsigned char*>();
				anyArr = std::vector(ucharArr, ucharArr + arrSize);				
			}else if (v->GetDataType() == DataType::UInt16) {
				type = TC::IO::NpyArrType::arrUSHORT;
				const auto* ushortArr = v->GetDataAs<const unsigned short*>();
				anyArr = std::vector(ushortArr, ushortArr + arrSize);
			}else if (v->GetDataType() == DataType::Float32) {
				type = TC::IO::NpyArrType::arrFLOAT;
				const auto* floatArr = v->GetDataAs<const float*>();
				anyArr = std::vector(floatArr, floatArr + arrSize);				
			}else if (v->GetDataType() == DataType::Float64) {
				type = TC::IO::NpyArrType::arrDOUBLE;
				const auto doubleArr = v->GetDataAs<const double*>();
				anyArr = std::vector(doubleArr, doubleArr + arrSize);
			}else {
				return false;
			}
			
			if (writer->Write(anyArr, filepath, type, shape)) {
				ok = true;
				QFileInfo fileInfo(filepath);
				QString newFilePath = fileInfo.path() + "/" + fileInfo.completeBaseName() + ".txt";
				std::ofstream outFile(newFilePath.toStdString());
				if (outFile.is_open()) {
					outFile << res[0] << " " << res[1] << " " << res[2] << std::endl;
				}
				outFile.close();
			}
		}

		if (ok) {
			return true;
		}

		return false;
	}
}
