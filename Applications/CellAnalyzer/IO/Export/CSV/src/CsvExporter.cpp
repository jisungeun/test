#include <QVariantMap>
#include <QTextStream>
#include <QDir>
#include <fstream>
#include <QFileInfo>

#include "IMeasure.h"
#include "Measure.h"
#include "CsvExporter.h"

#include <iostream>

namespace CellAnalyzer::IO::Export {
	struct CsvExporter::Impl {
		QString group;
		QString fileName;

		auto SaveSummary(std::shared_ptr<Data::Measure> measure, const QString& filePath, const QString& groupName) -> void;
	};

	auto CsvExporter::Impl::SaveSummary(std::shared_ptr<Data::Measure> measureData, const QString& filePath, const QString& groupName) -> void {
		QFileInfo fileInfo(filePath);
		const auto summaryPath = QFileInfo(filePath).dir().filePath("Summary");
		QDir summaryDir(summaryPath);
		if (false == summaryDir.exists()) {
			if (false == summaryDir.mkdir(summaryPath)) {
				return;
			}
		}

		if (measureData == nullptr || measureData->GetKeys().isEmpty())
			return;

		const auto summaryFilePath = summaryPath + "/" + fileInfo.fileName().chopped(4) + "_summary.csv";
		std::ofstream summary_csv;
		summary_csv.open(summaryFilePath.toStdString());

		const auto singleLabelStats = QStringList { "Count", "Sum", "Average", "STD", "Min", "Max", "D10", "D50", "D90" };


		auto measureMap = measureData->GetMap();
		const auto labelType = measureMap.value("LabelType").toString();
		if (!labelType.isEmpty())
			measureMap.remove("LabelType");

		QList<int> indexes;
		for (const auto& key : measureMap.keys())
			indexes << key.toInt();

		std::sort(indexes.begin(), indexes.end());

		auto measureKeys = measureMap.first().toMap().keys();

		const auto q10 = static_cast<int>(indexes.count() / 10.0);
		const auto q50 = static_cast<int>(indexes.count() / 2.0);
		const auto q90 = static_cast<int>(indexes.count() / 10.0 * 9.0);

		summary_csv << "FileName,Group,";

		if (labelType == "SingleLabel") {
			for (auto i = measureKeys.count() - 1; i >= 0; i--) {
				if (measureKeys[i].contains("-idx"))
					measureKeys.removeAt(i);
			}

			summary_csv << "Stats,";
			for (auto i = 0; i < measureKeys.count() - 1; i++) {
				summary_csv << "\"" << measureKeys[i].toStdString() << "\",";
			}
			summary_csv << "\"" << measureKeys.last().toStdString() << "\"" << "\n";

			QMap<QString, QMap<QString, double>> summary;

			for (auto row = 0; row < singleLabelStats.count(); row++) {
				const auto statKey = singleLabelStats[row];
				summary[statKey] = QMap<QString, double>();
			}

			for (auto i = 0; i < measureKeys.count(); i++) {
				const auto& key = measureKeys[i];

				double sum = 0;
				double stdev = 0;
				double min = INT_MAX;
				double max = INT_MIN;

				QList<double> values;

				for (auto j = 0; j < indexes.count(); j++) {
					const auto value = measureMap[QString::number(j)].toMap()[key].toDouble();
					values.append(value);
					if (min > value)
						min = value;
					if (max < value)
						max = value;

					sum += value;
				}

				std::sort(values.begin(), values.end());

				const auto mean = sum / static_cast<double>(indexes.count());
				for (auto j = 0; j < indexes.count(); j++)
					stdev += pow(values[j] - mean, 2);

				stdev = sqrt(stdev / indexes.count());

				summary["Count"][key] = indexes.count();
				summary["Sum"][key] = sum;
				summary["Average"][key] = mean;
				summary["STD"][key] = stdev;
				summary["Min"][key] = min;
				summary["Max"][key] = max;
				summary["D10"][key] = values[q10];
				summary["D50"][key] = values[q50];
				summary["D90"][key] = values[q90];
			}
			for (auto row = 0; row < singleLabelStats.count(); row++) {
				const auto statKey = singleLabelStats[row];
				summary_csv << "\"" << fileName.toStdString() << "\",";
				summary_csv << "\"" << groupName.toStdString() << "\",";
				summary_csv << "\"" << statKey.toStdString() << "\",";
				for (auto i = 0; i < measureKeys.count(); i++) {
					const auto& key = measureKeys[i];

					const bool isIntValue = key.contains("-idx") || key.contains("TimeStep");
					auto statValue = QString::number(summary[statKey][key], 'f', isIntValue ? 0 : 4);
					summary_csv << statValue.toStdString();
					if (i < measureKeys.count() - 1) {
						summary_csv << ",";
					}
				}
				if (row < singleLabelStats.count() - 1) {
					summary_csv << "\n";
				}
			}
		} else if (labelType == "DualLabel") {
			QMap<QString, QMap<QString, QMap<QString, double>>> summary;

			QList<int> parentIdx;
			for (auto i = 0; i < measureKeys.count(); i++) {
				const auto& key = measureKeys[i];
				if (key.contains("-parentidx")) {
					for (auto j = 0; j < indexes.count(); j++) {
						auto value = measureMap[QString::number(j)].toMap()[key].toDouble();
						parentIdx.append(static_cast<int>(value));
					}
					break;
				}
			}
			for (auto i = measureKeys.count() - 1; i >= 0; i--) {
				if (measureKeys[i].contains("-parentidx") || measureKeys[i].contains("-idx"))
					measureKeys.removeAt(i);
			}
			summary_csv << "ParentIdx,Stats,";
			for (auto i = 0; i < measureKeys.count() - 1; i++) {
				summary_csv << "\"" << measureKeys[i].toStdString() << "\",";
			}
			summary_csv << "\"" << measureKeys.last().toStdString() << "\"" << "\n";

			QMap<int, int> parentCnt;
			for (auto i = 0; i < parentIdx.count(); i++) {
				const auto pIdx = parentIdx[i];
				if (!parentCnt.contains(pIdx))
					parentCnt[pIdx] = 0;
				parentCnt[pIdx]++;
			}
			auto sortedParentKey = parentCnt.keys();
			std::sort(sortedParentKey.begin(), sortedParentKey.end());

			summary["All"] = QMap<QString, QMap<QString, double>>();
			for (auto key : sortedParentKey)
				summary[QString::number(key)] = QMap<QString, QMap<QString, double>>();

			for (auto i = 0; i < singleLabelStats.count(); i++) {
				const auto statKey = singleLabelStats[i];

				summary["All"][statKey] = QMap<QString, double>();

				for (auto k = 0; k < sortedParentKey.count(); k++) {
					const auto key = sortedParentKey[k];
					summary[QString::number(key)][statKey] = QMap<QString, double>();
				}
			}

			for (auto measureIndex = 0; measureIndex < measureKeys.count(); measureIndex++) {
				const auto key = measureKeys[measureIndex];

				double sum = 0;
				double stdev = 0;
				double min = INT_MAX;
				double max = INT_MIN;

				QList<double> values;
				QMap<int, double> sumMap;
				QMap<int, double> stdevMap;
				QMap<int, double> minMap;
				QMap<int, double> maxMap;
				QMap<int, QList<double>> valuesMap;
				QMap<int, int> q10Map;
				QMap<int, int> q50Map;
				QMap<int, int> q90Map;

				for (auto j = 0; j < sortedParentKey.count(); j++) {
					const auto pk = sortedParentKey[j];
					sumMap[pk] = 0;
					stdevMap[pk] = 0;
					minMap[pk] = INT_MAX;
					maxMap[pk] = -INT_MAX;
					valuesMap[pk] = QList<double>();
					q10Map[pk] = static_cast<int>(parentCnt[pk] / 10.0);
					q50Map[pk] = static_cast<int>(parentCnt[pk] / 2.0);
					q90Map[pk] = static_cast<int>(parentCnt[pk] / 10.0 * 9.0);
				}

				for (auto j = 0; j < indexes.count(); j++) {
					auto value = measureMap[QString::number(j)].toMap()[key].toDouble();
					values << value;

					const auto pIdx = parentIdx[j];
					valuesMap[pIdx] << value;
					if (min > value)
						min = value;
					if (minMap[pIdx] > value)
						minMap[pIdx] = value;
					if (max < value)
						max = value;
					if (maxMap[pIdx] < value)
						maxMap[pIdx] = value;

					sum += value;
					sumMap[pIdx] += value;
				}

				std::sort(values.begin(), values.end());

				const auto mean = sum / static_cast<double>(indexes.count());
				for (auto j = 0; j < indexes.count(); j++) {
					stdev += pow(values[j] - mean, 2);
				}

				stdev = sqrt(stdev / indexes.count());

				summary["All"]["Count"][key] = indexes.count();
				summary["All"]["Sum"][key] = sum;
				summary["All"]["Average"][key] = mean;
				summary["All"]["STD"][key] = stdev;
				summary["All"]["Min"][key] = min;
				summary["All"]["Max"][key] = max;
				summary["All"]["D10"][key] = values[q10];
				summary["All"]["D50"][key] = values[q50];
				summary["All"]["D90"][key] = values[q90];

				for (auto j = 0; j < sortedParentKey.count(); j++) {
					const auto pk = sortedParentKey[j];
					const auto cnt = parentCnt[pk];
					const auto kmean = sumMap[pk] / cnt;
					for (auto k = 0; k < cnt; k++)
						stdevMap[pk] += pow(valuesMap[pk][k] - kmean, 2);

					stdevMap[pk] = sqrt(stdevMap[pk] / cnt);

					summary[QString::number(pk)]["Count"][key] = cnt;
					summary[QString::number(pk)]["Sum"][key] = sumMap[pk];
					summary[QString::number(pk)]["Average"][key] = kmean;
					summary[QString::number(pk)]["STD"][key] = stdevMap[pk];
					summary[QString::number(pk)]["Min"][key] = minMap[pk];
					summary[QString::number(pk)]["Max"][key] = maxMap[pk];
					summary[QString::number(pk)]["D10"][key] = valuesMap[pk][q10Map[pk]];
					summary[QString::number(pk)]["D50"][key] = valuesMap[pk][q50Map[pk]];
					summary[QString::number(pk)]["D90"][key] = valuesMap[pk][q90Map[pk]];
				}
			}
			//Save CSV
			//All
			auto countStr = QString::number(indexes.count(), 'f', 0);
			for (auto i = 0; i < singleLabelStats.count(); i++) {
				const auto statKey = singleLabelStats[i];
				summary_csv << "\"" << fileName.toStdString() << "\",";
				summary_csv << "\"" << groupName.toStdString() << "\",";
				summary_csv << "All,";
				summary_csv << "\"" << statKey.toStdString() << "\",";
				//summary_csv << countStr.toStdString() << ",";
				for (auto measureIndex = 0; measureIndex < measureKeys.count(); measureIndex++) {
					const auto key = measureKeys[measureIndex];
					QString statStr;
					if (key.contains("-parentidx") || key.contains("-idx") || key.contains("TimeStep")) {
						statStr = QString::number(summary["All"][statKey][key], 'f', 0);
					} else {
						statStr = QString::number(summary["All"][statKey][key], 'f', 4);
					}
					summary_csv << statStr.toStdString();
					if (measureIndex < measureKeys.count() - 1) {
						summary_csv << ",";
					}
				}
				summary_csv << "\n";
			}
			//Others
			for (auto j = 0; j < sortedParentKey.count(); j++) {
				const auto pk = sortedParentKey[j];
				auto pcountStr = QString::number(parentCnt[pk], 'f', 0);
				for (auto i = 0; i < singleLabelStats.count(); i++) {
					const auto statKey = singleLabelStats[i];
					summary_csv << "\"" << fileName.toStdString() << "\",";
					summary_csv << "\"" << groupName.toStdString() << "\",";
					summary_csv << pk << ",";
					summary_csv << "\"" << statKey.toStdString() << "\",";
					//summary_csv << pcountStr.toStdString() << ",";
					for (auto measureIndex = 0; measureIndex < measureKeys.count(); measureIndex++) {
						const auto key = measureKeys[measureIndex];
						QString statStr;
						if (key.contains("-parentidx") || key.contains("-idx") || key.contains("TimeStep")) {
							statStr = QString::number(summary[QString::number(pk)][statKey][key], 'f', 0);
						} else {
							statStr = QString::number(summary[QString::number(pk)][statKey][key], 'f', 4);
						}
						summary_csv << statStr.toStdString();
						if (measureIndex < measureKeys.count() - 1) {
							summary_csv << ",";
						}
					}
					if (j < sortedParentKey.count() - 1 || i < singleLabelStats.count() - 1) {
						summary_csv << "\n";
					}
				}
			}
		}

		summary_csv.close();
	}

	CsvExporter::CsvExporter() : IExporter(), d(new Impl) {}

	CsvExporter::~CsvExporter() = default;

	auto CsvExporter::GetName() const -> QString {
		return "Csv File";
	}

	auto CsvExporter::GetFormat() const -> QString {
		return "csv";
	}

	auto CsvExporter::GetIcon() const -> QString {
		return {};
	}

	auto CsvExporter::GetMetadata(const QString& name) const -> QVariant {
		return d->group;
	}

	auto CsvExporter::GetMetadataList() const -> QStringList {
		return { "Group" };
	}

	auto CsvExporter::IsExportable(const DataFlags& flags) const -> bool {
		if (flags.testFlag(DataFlag::Measure))
			return true;

		return false;
	}

	auto CsvExporter::SetMetadata(const QString& name, const QVariant& value) -> void {
		if (name == "Group")
			d->group = value.toString();
		if (name == "FilePath") {
			QFileInfo info(value.toString());
			d->fileName = info.fileName().chopped(4);
		}
	}

	auto CsvExporter::Export(const DataPtr& data, const QString& filepath) -> bool {
		if (const auto measure = std::dynamic_pointer_cast<Data::Measure>(data)) {
			auto measureMap = measure->GetMap();

			if (measureMap.isEmpty()) {
				return false;
			}
			const auto labelType = measureMap.value("LabelType").toString();
			if (!labelType.isEmpty())
				measureMap.remove("LabelType");

			QList<int> indexes;
			for (const auto& key : measureMap.keys()) {
				indexes << key.toInt();
			}

			if (measureMap.count() < 1) {
				return false;
			}

			const auto fileName = d->fileName.isEmpty() ? "N/A" : d->fileName;
			const auto group = d->group.isEmpty() ? "N/A" : d->group;

			d->SaveSummary(measure, filepath, group);

			std::ofstream new_csv;
			new_csv.open(filepath.toStdString());

			new_csv << "FileName,Group,";

			const auto firstMap = measureMap.first().toMap();
			const auto measureKeys = firstMap.keys();
			for (auto i = 0; i < measureKeys.count(); i++) {
				new_csv << "\"" << measureKeys[i].toStdString() << "\"";
				if (i < measureKeys.count() - 1) {
					new_csv << ",";
				}
			}
			new_csv << "\n";
			for (auto j = 0; j < indexes.count(); j++) {
				new_csv << "\"" << fileName.toStdString() << "\"" << ",";
				new_csv << "\"" << group.toStdString() << "\"" << ",";
				for (auto i = 0; i < measureKeys.count(); i++) {
					const auto& measureName = measureKeys[i];
					const auto measureValue = measureMap[QString::number(j)].toMap()[measureName].toDouble();

					const bool isIntValue = measureName.contains("-idx") || measureName.contains("-parentidx") || measureName.contains("TimeStep");
					const auto cellValue = QString::number(measureValue, 'f', isIntValue ? 0 : 4);

					new_csv << cellValue.toStdString();
					if (i < measureKeys.count() - 1) {
						new_csv << ",";
					}
				}
				if (j < indexes.count() - 1) {
					new_csv << "\n";
				}
			}
			new_csv.close();
		}

		return false;
	}

	auto CsvExporter::Merge(const QString& mergeTargetDir, const QString& mergedFilePath) -> bool {
		if (mergeTargetDir.isEmpty())
			return false;
		if (mergedFilePath.isEmpty())
			return false;
		if (false == QDir(mergeTargetDir).exists()) {
			return false;
		}
		QFile outputFileObj(mergedFilePath);
		if (!outputFileObj.open(QIODevice::WriteOnly | QIODevice::Text)) {
			return false;
		}
		QTextStream outputStream(&outputFileObj);

		QDir sourceDir(mergeTargetDir);
		QStringList filter;
		filter << "*.csv";
		QStringList csvFiles = sourceDir.entryList(filter, QDir::Files);

		if (csvFiles.isEmpty()) {
			return false;
		}
		QFile firstFile(sourceDir.filePath(csvFiles.first()));
		if (firstFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
			outputStream << firstFile.readAll();
			firstFile.close();
		}

		for (int i = 1; i < csvFiles.size(); ++i) {
			QFile inputFile(sourceDir.filePath(csvFiles.at(i)));
			if (inputFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
				// Skip the first line
				QString fileContent = inputFile.readAll();
				int newlineIndex = fileContent.indexOf('\n');
				if (newlineIndex != -1) {
					fileContent = fileContent.mid(newlineIndex + 1);
				}
				outputStream << "\n";
				outputStream << fileContent;
				inputFile.close();
			}
		}


		outputFileObj.close();
		return true;
	}
}
