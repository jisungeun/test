#include "TiffExporter.h"

#include "IFL.h"
#include "IHT.h"
#include "ILabeled.h"
#include "IMask.h"
#include "IVolume2D.h"
#include "IVolume3D.h"
#include "TiffWriter.h"

namespace CellAnalyzer::IO::Export {
	struct TiffExporter::Impl {
		auto SetTiffMetadata(std::shared_ptr<TiffWriter> writer, int sizeZ, double resZ, double min, double max) const -> void {
			const auto desc = QString("ImageJ=%1\nslices=%2\nunit=%3\nspacing=%4\nloop=%5\nmin=%6\nmax=%7\n")
							.arg("1.54f")
							.arg(sizeZ)
							.arg("cm")
							.arg(resZ / 10000.0, 0, 'f', 12)
							.arg("false")
							.arg(min, 0, 'f', 1)
							.arg(max, 0, 'f', 1);

			writer->SetDescription(desc);
		}
	};

	TiffExporter::TiffExporter() : IExporter(), d(new Impl) { }

	TiffExporter::~TiffExporter() = default;

	auto TiffExporter::GetName() const -> QString {
		return "TIFF";
	}

	auto TiffExporter::GetFormat() const -> QString {
		return "tiff";
	}

	auto TiffExporter::GetIcon() const -> QString {
		return {};
	}

	auto TiffExporter::GetMetadata(const QString& name) const -> QVariant {
		return {};
	}

	auto TiffExporter::GetMetadataList() const -> QStringList {
		return {};
	}

	auto TiffExporter::IsExportable(const DataFlags& flags) const -> bool {
		if (flags.testFlag(DataFlag::Volume2D) || flags.testFlag(DataFlag::Volume3D))
			return true;

		return false;
	}

	auto TiffExporter::SetMetadata(const QString& name, const QVariant& value) -> void {}

	auto TiffExporter::Export(const DataPtr& data, const QString& filepath) -> bool {
		if (const auto v = std::dynamic_pointer_cast<IVolume>(data)) {
			std::shared_ptr<TiffWriter> writer = nullptr;
			int sizeZ { 1 };
			double resZ { 1 };
			double metaMin { 0 };
			double metaMax { 1 };
			if (const auto v2 = std::dynamic_pointer_cast<IVolume2D>(data)) {
				const auto [x, y] = v2->GetSize();
				const auto [rx, ry] = v2->GetResolution();
				const auto [px, py] = v2->GetOrigin();

				writer = std::make_shared<TiffWriter>(filepath, v2->GetDataType(), x, y);
				writer->SetResolution(rx, ry);
				writer->SetPosition(px, py);
			} else if (const auto v3 = std::dynamic_pointer_cast<IVolume3D>(data)) {
				const auto [x, y, z] = v3->GetSize();
				const auto [rx, ry, rz] = v3->GetResolution();
				const auto [px, py, pz] = v3->GetOrigin();
				sizeZ = z;
				resZ = rz;
				writer = std::make_shared<TiffWriter>(filepath, v3->GetDataType(), x, y, z);
				writer->SetResolution(rx, ry, rz);
				writer->SetPosition(px, py, pz);
			}

			if (const auto ht = std::dynamic_pointer_cast<IHT>(data)) {
				const auto [min, max] = ht->GetRI();
				writer->SetIntensity(min * 10000.0, max * 10000.0);
				metaMin = min * 10000.0;
				metaMax = max * 10000.0;
			} else if (const auto fl = std::dynamic_pointer_cast<IFL>(data)) {
				const auto [min, max] = fl->GetIntensity();
				writer->SetIntensity(min, max);
				metaMin = min;
				metaMax = max;
			} else if (const auto label = std::dynamic_pointer_cast<ILabeled>(data)) {
				writer->SetIntensity(0, static_cast<uint16_t>(label->GetMaxIndex()));
				metaMin = 0;
				metaMax = label->GetMaxIndex();
			} else if (const auto mask = std::dynamic_pointer_cast<IMask>(data)) {
				writer->SetIntensity(static_cast<uint16_t>(0), 1);
			}

			if (writer) {
				d->SetTiffMetadata(writer, sizeZ, resZ, metaMin, metaMax);
				return writer->Write(v->GetData());
			}
		}

		return false;
	}
}
