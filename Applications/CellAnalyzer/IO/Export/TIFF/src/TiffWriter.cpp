#include <tiffio.h>

#include "TiffWriter.h"

#include "ExtendedTIFF.h"

namespace CellAnalyzer::IO::Export {
	constexpr uint64_t TiffMaximum = 3500uLL * 1000uLL * 1000uLL;

	struct TiffWriter::Impl {
		std::wstring path;
		int32_t size = 0;
		int32_t x = 0, y = 0, z = 0;
		double_t rx = 0.0, ry = 0.0, rz = 0.0;
		double_t px = 0.0, py = 0.0, pz = 0.0;
		bool rgb = false;
		bool floating = false;

		uint16_t min = 0, max = 0;
		double_t dmin = 0.0, dmax = 0.0;

		bool big = false;
		std::string desc;
		std::string artist;
		std::string host;
		std::string model;
		std::string sw;
		std::string datetime;

		TIFF* tiff = nullptr;
	};

	TiffWriter::TiffWriter(const QString& filepath, DataType type, int x, int y) : d(new Impl) {
		d->path = filepath.toStdWString();
		d->x = x;
		d->y = y;

		switch (type) {
			case DataType::UInt8:
				d->size = sizeof(uint8_t);
				d->floating = false;
				break;
			case DataType::UInt16:
				d->size = sizeof(uint16_t);
				d->floating = false;
				break;
			case DataType::Float32:
				d->size = sizeof(float_t);
				d->floating = true;
				break;
			case DataType::Float64:
				d->size = sizeof(double_t);
				d->floating = true;
				break;
		}

		if (static_cast<uint64_t>(x) * y * d->size > TiffMaximum)
			d->big = true;
	}

	TiffWriter::TiffWriter(const QString& path, DataType type, int x, int y, int z) : d(new Impl) {
		d->path = path.toStdWString();
		d->x = x;
		d->y = y;
		d->z = z;

		switch (type) {
			case DataType::UInt8:
				d->size = sizeof(uint8_t);
				d->floating = false;
				break;
			case DataType::UInt16:
				d->size = sizeof(uint16_t);
				d->floating = false;
				break;
			case DataType::Float32:
				d->size = sizeof(float_t);
				d->floating = true;
				break;
			case DataType::Float64:
				d->size = sizeof(double_t);
				d->floating = true;
				break;
		}

		if (static_cast<uint64_t>(x) * y * z * d->size > TiffMaximum)
			d->big = true;
	}

	TiffWriter::~TiffWriter() {
		Dispose();
	}

	auto TiffWriter::SetRGB(bool rgb) -> void {
		d->rgb = rgb;
	}

	auto TiffWriter::SetResolution(double x, double y) -> void {
		d->rx = x;
		d->ry = y;
	}

	auto TiffWriter::SetResolution(double x, double y, double z) -> void {
		d->rx = x;
		d->ry = y;
		d->rz = z;
	}

	auto TiffWriter::SetPosition(double x, double y) -> void {
		d->px = x;
		d->py = y;
	}

	auto TiffWriter::SetPosition(double x, double y, double z) -> void {
		d->px = x;
		d->py = y;
		d->pz = z;
	}

	auto TiffWriter::SetIntensity(uint16_t min, uint16_t max) -> void {
		d->min = min;
		d->max = max;
	}

	auto TiffWriter::SetIntensity(double_t min, double_t max) -> void {
		d->dmin = min;
		d->dmax = max;
	}

	auto TiffWriter::SetBigTIFF(bool big) -> void {
		d->big = big;
	}

	auto TiffWriter::SetDescription(const QString& desc) -> void {
		d->desc = desc.toStdString();
	}

	auto TiffWriter::SetArtist(const QString& artist) -> void {
		d->artist = artist.toStdString();
	}

	auto TiffWriter::SetHost(const QString& host) -> void {
		d->host = host.toStdString();
	}

	auto TiffWriter::SetModel(const QString& model) -> void {
		d->model = model.toStdString();
	}

	auto TiffWriter::SetSoftware(const QString& sw) -> void {
		d->sw = sw.toStdString();
	}

	auto TiffWriter::SetDateTime(const QDateTime& datetime) -> void {
		d->datetime = datetime.toString("yyyy-MM-dd HH:mm:ss").toStdString();
	}

	auto TiffWriter::Write(void* data) -> bool {
		Dispose();

		d->tiff = TIFFOpenW(d->path.c_str(), (d->big) ? "w8" : "w");

		if (!d->tiff)
			return false;

		auto page = 0;

		do {
			TIFFSetField(d->tiff, TIFFTAG_SUBFILETYPE, 0x000000);
			TIFFSetField(d->tiff, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
			TIFFSetField(d->tiff, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
			TIFFSetField(d->tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
			TIFFSetField(d->tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_CENTIMETER);
			TIFFSetField(d->tiff, TIFFTAG_ROWSPERSTRIP, d->x);
			TIFFSetField(d->tiff, TIFFTAG_IMAGEWIDTH, d->x);
			TIFFSetField(d->tiff, TIFFTAG_IMAGELENGTH, d->y);
			TIFFSetField(d->tiff, TIFFTAG_BITSPERSAMPLE, d->size * 8);
			TIFFSetField(d->tiff, TIFFTAG_PHOTOMETRIC, d->rgb ? PHOTOMETRIC_RGB : PHOTOMETRIC_MINISBLACK);
			TIFFSetField(d->tiff, TIFFTAG_SAMPLESPERPIXEL, d->rgb ? 3 : 1);
			TIFFSetField(d->tiff, TIFFTAG_SAMPLEFORMAT, (d->floating) ? SAMPLEFORMAT_IEEEFP : SAMPLEFORMAT_UINT);

			if (d->z > 0) {
				TIFFSetDirectory(d->tiff, page);
			}

			if (d->rx > 0.0)
				TIFFSetField(d->tiff, TIFFTAG_XRESOLUTION, static_cast<float>(10000 / d->rx));
			if (d->ry > 0.0)
				TIFFSetField(d->tiff, TIFFTAG_YRESOLUTION, static_cast<float>(10000 / d->ry));
			if (d->rz > 0.0)
				TIFFSetField(d->tiff, TIFFTAG_ZRESOLUTION, 10000 / d->rz);
			if (d->px > 0.0)
				TIFFSetField(d->tiff, TIFFTAG_XPOSITION, d->px);
			if (d->py > 0.0)
				TIFFSetField(d->tiff, TIFFTAG_YPOSITION, d->py);
			if (d->min > 0)
				TIFFSetField(d->tiff, TIFFTAG_MINSAMPLEVALUE, d->min);
			if (d->max > 0)
				TIFFSetField(d->tiff, TIFFTAG_MAXSAMPLEVALUE, d->max);
			if (d->dmin > 0.0)
				TIFFSetField(d->tiff, TIFFTAG_SMINSAMPLEVALUE, d->dmin);
			if (d->dmax > 0.0)
				TIFFSetField(d->tiff, TIFFTAG_SMAXSAMPLEVALUE, d->dmax);
			if (!d->desc.empty())
				TIFFSetField(d->tiff, TIFFTAG_IMAGEDESCRIPTION, d->desc.c_str());
			if (!d->artist.empty())
				TIFFSetField(d->tiff, TIFFTAG_ARTIST, d->artist.c_str());
			if (!d->host.empty())
				TIFFSetField(d->tiff, TIFFTAG_HOSTCOMPUTER, d->host.c_str());
			if (!d->model.empty())
				TIFFSetField(d->tiff, TIFFTAG_MODEL, d->model.c_str());
			if (!d->sw.empty())
				TIFFSetField(d->tiff, TIFFTAG_SOFTWARE, d->sw.c_str());
			if (!d->datetime.empty())
				TIFFSetField(d->tiff, TIFFTAG_DATETIME, d->datetime.c_str());

			for (auto i = 0; i < d->y; i++) {
				if (TIFFWriteScanline(d->tiff, static_cast<char*>(data) + (i * d->x * d->size) + (page * d->y * d->x * d->size), i) < 0)
					return false;
			}

			if (d->z > 0)
				TIFFWriteDirectory(d->tiff);
		} while (++page < d->z);

		Dispose();
		return true;
	}

	auto TiffWriter::Dispose() -> void {
		if (d->tiff) {
			TIFFClose(d->tiff);
			d->tiff = nullptr;
		}
	}
}
