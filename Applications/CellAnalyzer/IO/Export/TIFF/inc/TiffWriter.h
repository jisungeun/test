#pragma once

#include <QDateTime>

#include "IExporter.h"
#include "IVolume2D.h"

#include "CellAnalyzer.IO.Export.TIFFExport.h"

namespace CellAnalyzer::IO::Export {
	class CellAnalyzer_IO_Export_TIFF_API TiffWriter {
	public:
		explicit TiffWriter(const QString& filepath, DataType type, int x, int y);
		explicit TiffWriter(const QString& path, DataType type, int x, int y, int z);
		~TiffWriter();

		auto SetRGB(bool rgb) -> void;
		auto SetResolution(double x, double y) -> void;
		auto SetResolution(double x, double y, double z) -> void;
		auto SetPosition(double x, double y) -> void;
		auto SetPosition(double x, double y, double z) -> void;
		auto SetIntensity(uint16_t min, uint16_t max) -> void;
		auto SetIntensity(double_t min, double_t max) -> void;

		auto SetBigTIFF(bool big) -> void;
		auto SetDescription(const QString& desc) -> void;
		auto SetArtist(const QString& artist) -> void;
		auto SetHost(const QString& host) -> void;
		auto SetModel(const QString& model) -> void;
		auto SetSoftware(const QString& sw) -> void;
		auto SetDateTime(const QDateTime& datetime) -> void;

		auto Write(void* data) -> bool;
		auto Dispose() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
