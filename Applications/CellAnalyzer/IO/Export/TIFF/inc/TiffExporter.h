#pragma once

#include "IExporter.h"

#include "CellAnalyzer.IO.Export.TIFFExport.h"

namespace CellAnalyzer::IO::Export {
	class CellAnalyzer_IO_Export_TIFF_API TiffExporter final : public IExporter {
	public:
		TiffExporter();
		~TiffExporter() override;

		auto GetName() const -> QString override;
		auto GetFormat() const -> QString override;
		auto GetIcon() const -> QString override;
		auto GetMetadata(const QString& name) const -> QVariant override;
		auto GetMetadataList() const -> QStringList override;
		auto IsExportable(const DataFlags& flags) const -> bool override;

		auto SetMetadata(const QString& name, const QVariant& value) -> void override;
		auto Export(const DataPtr& data, const QString& filepath) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
