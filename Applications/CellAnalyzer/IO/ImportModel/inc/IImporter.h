#pragma once

#include <QVariant>

#include "IData.h"

#include "CellAnalyzer.IO.ImportModelExport.h"

namespace CellAnalyzer::IO {
	class IImporter;
	using ImporterPtr = std::shared_ptr<IImporter>;
	using ImporterList = QVector<ImporterPtr>;

	class CellAnalyzer_IO_ImportModel_API IImporter : public virtual Tomocube::IService {
	public:
		virtual auto GetName() const -> QString = 0;
		virtual auto GetFormat() const -> QString = 0;

		virtual auto Import(const QString& filepath, const DataFlags& flags) -> DataPtr = 0;
	};
}
