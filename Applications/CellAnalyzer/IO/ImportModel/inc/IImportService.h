#pragma once

#include "IImporter.h"

#include "CellAnalyzer.IO.ImportModelExport.h"

namespace CellAnalyzer::IO {
	class CellAnalyzer_IO_ImportModel_API IImportService : public virtual Tomocube::IService {
	public:
		virtual auto GetImporter(const QString& name) const -> ImporterPtr = 0;
		virtual auto GetImporterList() const -> ImporterList = 0;
	};
}
