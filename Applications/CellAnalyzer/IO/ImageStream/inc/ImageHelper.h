#pragma once

#include "ImageStream2D.h"

namespace CellAnalyzer::IO {
	template <typename T>
	static auto ToQImageOrder(const ImageStream2DPtr<T>& stream) -> void {
		const auto buffer = std::const_pointer_cast<T[]>(stream->GetBuffer());
		const auto size = stream->GetSize();
		const auto temp = std::make_unique<T[]>(size.ToSize2D());

		for (uint64_t j = 0; j < size.x; ++j) {
			for (uint64_t k = 0; k < size.y; ++k) {
				for (uint64_t z = 0; z < 3; ++z) {
					const auto src = j + (k * size.x) + (z * size.x * size.y);
					const auto dest = k + (j * size.y) + (z * size.x * size.y);

					temp[dest] = buffer[src];
				}
			}
		}

		for (uint64_t j = 0; j < size.x; ++j) {
			for (uint64_t k = 0; k < size.y; ++k) {
				const auto srcR = k + j * size.y + 0 * (size.x * size.y);
				const auto srcG = k + j * size.y + 1 * (size.x * size.y);
				const auto srcB = k + j * size.y + 2 * (size.x * size.y);

				const auto destR = 3 * (j + k * size.x) + 0;
				const auto destG = 3 * (j + k * size.x) + 1;
				const auto destB = 3 * (j + k * size.x) + 2;

				buffer[destR] = temp[srcR];
				buffer[destG] = temp[srcG];
				buffer[destB] = temp[srcB];
			}
		}
	}

	static auto ToUInt8(const ImageStream2DPtr<uint16_t>& stream) -> std::shared_ptr<uint8_t[]> {
		const auto data = stream->GetBuffer();
		const auto size = stream->GetSize().ToSize2D();
		const auto [min, max] = stream->GetRange();
		auto buffer = std::make_unique<uint8_t[]>(size);

		for (auto i = 0; i < size; i++)
			buffer[i] = static_cast<uint8_t>((data[i] - min) / (max - min) * std::numeric_limits<uint8_t>::max());

		return buffer;
	}

	template <typename T>
	static auto MergeColor(const ImageStream2DPtr<T>& dest, const ImageStream2DPtr<T>& src, const Color& color, int chCount) -> void {
		const auto size = dest->GetSize();
		const auto to = dest->GetBuffer();
		const auto from = src->GetBuffer();

		for (auto j = 0; j < size.x * size.y; j++) {
			const auto value = std::min(std::numeric_limits<T>::max(), from[j]);

			to[j * 3] = to[j * 3] + static_cast<T>(value * color.red / std::numeric_limits<T>::max() / chCount);
			to[j * 3 + 1] = to[j * 3 + 1] + static_cast<T>(value * color.green / std::numeric_limits<T>::max() / chCount);
			to[j * 3 + 2] = to[j * 3 + 2] + static_cast<T>(value * color.blue / std::numeric_limits<T>::max() / chCount);
		}
	}
}
