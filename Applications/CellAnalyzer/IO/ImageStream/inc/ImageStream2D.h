#pragma once

#include <memory>

#include "ImageModel.h"
#include "StreamReader.h"

namespace CellAnalyzer::IO {
	template <typename T>
	class ImageStream2D : public StreamReader<T> {
	public:
		ImageStream2D(const Size2D& size, const Resolution2D& resolution, const DataRange& range, StreamBufferPtr<T> buffer) : StreamReader<T>(std::move(buffer)), size(size), resolution(resolution), range(range) {}

		~ImageStream2D() = default;

		auto IsRGB() const -> bool {
			return size.rgb;
		}

		auto GetSize() const -> Size2D {
			return size;
		}

		auto GetResolution() const -> Resolution2D {
			return resolution;
		}

		auto GetRange() const -> DataRange {
			return range;
		}

	private:
		Size2D size;
		Resolution2D resolution;
		DataRange range;
	};

	template <typename T>
	using ImageStream2DPtr = std::shared_ptr<ImageStream2D<T>>;
}
