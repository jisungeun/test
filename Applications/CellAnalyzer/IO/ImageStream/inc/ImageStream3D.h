#pragma once

#include <memory>

#include "ImageModel.h"
#include "StreamReader.h"

namespace CellAnalyzer::IO {
	template <typename T>
	class ImageStream3D : public StreamReader<T> {
	public:
		ImageStream3D(const Size3D& size, const Resolution3D& resolution, const DataRange& range, StreamBufferPtr<T> buffer) : StreamReader<T>(std::move(buffer)), size(size), resolution(resolution), range(range) {}

		~ImageStream3D() = default;

		auto IsRGB() const -> bool {
			return size.rgb;
		}

		auto GetSize() const -> Size3D {
			return size;
		}

		auto GetResolution() const -> Resolution3D {
			return resolution;
		}

		auto GetRange() const -> DataRange {
			return range;
		}

	private:
		Size3D size;
		Resolution3D resolution;
		DataRange range;
	};

	template <typename T>
	using ImageStream3DPtr = std::shared_ptr<ImageStream3D<T>>;
}
