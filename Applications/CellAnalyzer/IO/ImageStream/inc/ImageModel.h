#pragma once

namespace CellAnalyzer::IO {
	struct Size2D {
		bool rgb = false;
		int x = 1;
		int y = 1;

		auto ToSize2D() const -> int64_t {
			return rgb ? x * y * 3 : x * y;
		}
	};

	struct Size3D : Size2D {
		int z = 1;

		auto ToSize3D() const -> int64_t {
			return rgb ? x * y * z * 3 : x * y * z;
		}
	};

	struct Resolution2D {
		double x = 0.0;
		double y = 0.0;
	};

	struct Resolution3D : Resolution2D {
		double z = 0.0;
	};

	struct DataRange {
		double min = 0.0;
		double max = 0.0;
	};

	struct Color {
		int red = 0;
		int green = 0;
		int blue = 0;
	};
}
