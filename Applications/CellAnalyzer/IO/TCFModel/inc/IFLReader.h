#pragma once

#include <QString>

#include "ImageStream2D.h"
#include "ImageStream3D.h"

#include "CellAnalyzer.IO.TCFModelExport.h"

namespace CellAnalyzer::IO {
	class IFLReader;
	using FLReaderPtr = std::shared_ptr<IFLReader>;
	using FLImage2D = std::shared_ptr<ImageStream2D<uint16_t>>;
	using FLImage3D = std::shared_ptr<ImageStream3D<uint16_t>>;
	using FLThumbnail = std::shared_ptr<ImageStream2D<uint8_t>>;

	class CellAnalyzer_IO_TCFModel_API IFLReader {
	public:
		virtual ~IFLReader() = default;

		virtual auto GetDataCount(int chIndex) const -> int = 0;
		virtual auto GetResolution() const -> Resolution3D = 0;
		virtual auto GetSize() const -> Size3D = 0;
		virtual auto GetRange() const -> DataRange = 0;
		virtual auto GetZOffset() const -> double = 0;

		virtual auto GetChannelList() const -> QVector<int> = 0;
		virtual auto GetChannelCount() const -> int = 0;
		virtual auto GetChannelName(int chIndex) const -> QString = 0;
		virtual auto GetChannelColor(int chIndex) const -> Color = 0;
		virtual auto GetChannelEmission(int chIndex) const -> double = 0;
		virtual auto GetChannelExcitation(int chIndex) const -> double = 0;
		virtual auto GetChannelExposureTime(int chIndex) const -> int = 0;
		virtual auto GetChannelIntensity(int chIndex) const -> int = 0;

		virtual auto Read(int chIndex, int tIndex) const -> FLImage3D = 0;
		virtual auto ReadMip(int chIndex, int tIndex) const -> FLImage2D = 0;
		virtual auto ReadThumbnail(int index) const -> FLThumbnail = 0;
	};
}
