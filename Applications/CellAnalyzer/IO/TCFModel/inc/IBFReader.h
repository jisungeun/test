#pragma once

#include "ImageStream2D.h"

#include "CellAnalyzer.IO.TCFModelExport.h"

namespace CellAnalyzer::IO {
	class IBFReader;
	using BFReaderPtr = std::shared_ptr<IBFReader>;
	using BFImage = std::shared_ptr<ImageStream2D<uint8_t>>;

	class CellAnalyzer_IO_TCFModel_API IBFReader {
	public:
		virtual ~IBFReader() = default;

		virtual auto GetDataCount() const -> int = 0;
		virtual auto GetResolution() const -> Resolution2D = 0;
		virtual auto GetSize() const -> Size2D = 0;

		virtual auto Read(int index) const -> BFImage = 0;
		virtual auto ReadThumbnail(int index) const -> BFImage = 0;
	};
}
