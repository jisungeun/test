#pragma once

#include <QDateTime>
#include <QMap>

#include "IBFReader.h"
#include "IFLReader.h"
#include "IHTReader.h"

#include "CellAnalyzer.IO.TCFModelExport.h"

namespace CellAnalyzer::IO {
	class ITCFReader;
	using TCFReaderPtr = std::shared_ptr<ITCFReader>;

	class CellAnalyzer_IO_TCFModel_API ITCFReader {
	public:
		virtual ~ITCFReader() = default;

		virtual auto GetUrl() const -> QString = 0;
		virtual auto IsReadable() const -> bool = 0;

		virtual auto GetCreationDateTime() const -> QDateTime = 0;
		virtual auto GetDataID() const -> QString = 0;
		virtual auto GetDescription() const -> QString = 0;
		virtual auto GetDeviceHost() const -> QString = 0;
		virtual auto GetDeviceModelType() const -> QString = 0;
		virtual auto GetDeviceSerial() const -> QString = 0;
		virtual auto GetDeviceSoftwareVersion() const -> QString = 0;
		virtual auto GetFormatVersion() const -> QString = 0;
		virtual auto GetRecordDateTime() const -> QDateTime = 0;
		virtual auto GetSoftwareVersion() const -> QString = 0;
		virtual auto GetTitle() const -> QString = 0;
		virtual auto GetUniqueID() const -> QString = 0;
		virtual auto GetUserID() const -> QString = 0;

		virtual auto ContainsHT() const -> bool = 0;
		virtual auto ContainsFL() const -> bool = 0;
		virtual auto ContainsBF() const -> bool = 0;

		virtual auto GetHT() const -> HTReaderPtr = 0;
		virtual auto GetFL() const -> FLReaderPtr = 0;
		virtual auto GetBF() const -> BFReaderPtr = 0;
	};
}
