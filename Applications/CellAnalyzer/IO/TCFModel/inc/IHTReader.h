#pragma once

#include "ImageStream2D.h"
#include "ImageStream3D.h"

#include "CellAnalyzer.IO.TCFModelExport.h"

namespace CellAnalyzer::IO {
	class IHTReader;
	using HTReaderPtr = std::shared_ptr<IHTReader>;
	using HTImage2D = std::shared_ptr<ImageStream2D<uint16_t>>;
	using HTImage3D = std::shared_ptr<ImageStream3D<uint16_t>>;
	using HTThumbnail = std::shared_ptr<ImageStream2D<uint8_t>>;

	class CellAnalyzer_IO_TCFModel_API IHTReader {
	public:
		virtual ~IHTReader() = default;

		virtual auto GetDataCount() const -> int = 0;
		virtual auto GetResolution() const -> Resolution3D = 0;
		virtual auto GetSize() const -> Size3D = 0;
		virtual auto GetRange() const -> DataRange = 0;

		virtual auto Read(int index) const -> HTImage3D = 0;
		virtual auto ReadMip(int index) const -> HTImage2D = 0;
		virtual auto ReadThumbnail(int index) const -> HTThumbnail = 0;
	};
}
