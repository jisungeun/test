#pragma once

#include "IAppModule.h"
#include "IImportService.h"

#include "CellAnalyzer.IO.Service.ImportExport.h"

namespace CellAnalyzer::IO::Service {
	class CellAnalyzer_IO_Service_Import_API ImportService final : public IAppModule, public IImportService {
	public:
		ImportService();
		~ImportService() override;

		auto Start() -> std::optional<Error> override;
		auto Stop() -> void override;

		auto GetImporter(const QString& name) const -> ImporterPtr override;
		auto GetImporterList() const -> ImporterList override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
