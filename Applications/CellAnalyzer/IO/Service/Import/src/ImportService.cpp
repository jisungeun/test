#include "ServiceCollection.h"

#include "ImportService.h"

#include "CsvImporter.h"
#include "RawImporter.h"
#include "TiffImporter.h"

namespace CellAnalyzer::IO::Service {
	struct ImportService::Impl {
		std::shared_ptr<Tomocube::IServiceProvider> provider = nullptr;
	};

	ImportService::ImportService() : IImportService(), d(new Impl) {}

	ImportService::~ImportService() = default;

	auto ImportService::Start() -> std::optional<Error> {
		const auto collection = std::make_unique<Tomocube::DependencyInjection::ServiceCollection>();

		collection->AddScoped<Import::TiffImporter, IImporter>()
				->AddScoped<Import::RawImporter, IImporter>()
				->AddScoped<Import::CsvImporter, IImporter>();

		d->provider = collection->BuildProvider();

		return {};
	}

	auto ImportService::Stop() -> void {}

	auto ImportService::GetImporter(const QString& name) const -> ImporterPtr {
		for (const auto& ex : GetImporterList()) {
			if (ex->GetFormat() == name)
				return ex;
		}

		return {};
	}

	auto ImportService::GetImporterList() const -> ImporterList {
		return d->provider->GetServices<IImporter>();
	}
}
