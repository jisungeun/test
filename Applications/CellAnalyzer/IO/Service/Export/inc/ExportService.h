#pragma once

#include "IAppModule.h"
#include "IExportService.h"

#include "CellAnalyzer.IO.Service.ExportExport.h"

namespace CellAnalyzer::IO::Service {
	class CellAnalyzer_IO_Service_Export_API ExportService final : public IAppModule, public IExportService {
	public:
		ExportService();
		~ExportService() override;

		auto Start() -> std::optional<Error> override;
		auto Stop() -> void override;

		auto GetExporter(const QString& name) const -> ExporterPtr override;
		auto GetExporterList() const -> ExporterList override;
		auto GetExporterList(const DataFlags& flags) const -> ExporterList override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
