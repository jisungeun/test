#include "ServiceCollection.h"

#include "ExportService.h"

#include "CsvExporter.h"
#include "RawExporter.h"
#include "TiffExporter.h"
#include <NpyExporter.h>

namespace CellAnalyzer::IO::Service {
	struct ExportService::Impl {
		std::shared_ptr<Tomocube::IServiceProvider> provider = nullptr;
	};

	ExportService::ExportService() : IExportService(), d(new Impl) {}

	ExportService::~ExportService() = default;

	auto ExportService::Start() -> std::optional<Error> {
		const auto collection = std::make_unique<Tomocube::DependencyInjection::ServiceCollection>();

		collection->AddScoped<Export::TiffExporter, IExporter>()
				->AddScoped<Export::RawExporter, IExporter>()
				->AddScoped<Export::CsvExporter, IExporter>()
				->AddScoped<Export::NpyExporter, IExporter>();

		d->provider = collection->BuildProvider();

		return {};
	}

	auto ExportService::Stop() -> void {}

	auto ExportService::GetExporter(const QString& name) const -> ExporterPtr {
		for (const auto& ex : GetExporterList()) {
			if (ex->GetName() == name)
				return ex;
		}

		return {};
	}

	auto ExportService::GetExporterList() const -> ExporterList {
		return d->provider->GetServices<IExporter>();
	}

	auto ExportService::GetExporterList(const DataFlags& flags) const -> ExporterList {
		ExporterList list;

		for (const auto& e : GetExporterList()) {
			if (e->IsExportable(flags))
				list.push_back(e);
		}

		return list;
	}
}
