#include <Inventor/Qt/SoQt.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>

#include <OivHdf5Reader.h>
#include <OivXYReader.h>
#include <OivLdmReaderXY.h>

#include <OivLdmReader.h>
#include <OivLdmReaderFL.h>

#include <OivThumbnailReader.h>

#include <OivHoveringLasso.h>
#include <OivRectangleDrawer.h>
#include <OivMultiLineDrawer.h>
#include <OivEllipseDrawer.h>
#include <OivCircleDrawer.h>

#include <QThreadPool>
#include <QMainWindow>

#include <OivActivator.h>

#include "OpenInventorLicenseManager.h"

namespace CellAnalyzer::License {
	constexpr const char* const OIV_STRING = "PACKAGE OpenInventorPackage mcslmd 2024.1 COMPONENTS=\"IvTuneViewer OpenInventor\" SIGN=%1 \nINCREMENT OpenInventorPackage mcslmd 2024.1 permanent uncounted \nVENDOR_STRING=INTERNALKEY HOSTID=DEMO ISSUED=12-mar-2024 \nNOTICE=APP-TOMOANALYSIS SN=20010828-IOIVR START=4-dec-2019 \nTS_OK SIGN=%2";
	constexpr const char* const VOLVIZ_STRING = "PACKAGE VolumeVizLDMPackage mcslmd 2024.1 COMPONENTS=\"VolumeVizLDM VolumeViz LDMWriter\" SIGN=%1 \nINCREMENT VolumeVizLDMPackage mcslmd 2024.1 permanent uncounted \nVENDOR_STRING=INTERNALKEY HOSTID=DEMO ISSUED=12-mar-2024 \nNOTICE=APP-TOMOANALYSIS SN=20010833-IVLDM START=4-dec-2019 \nTS_OK SIGN=%2";

	struct OpenInventorLicenseManager::Impl {
		QStringList oivSign { "12GEI;;?A?>C" };
		QStringList oivKey { "FGHI45=J8@C?" };
		QStringList volVizSign { "72CEFK9J=NBC" };
		QStringList volVizKey { "235E4=J?@;@?" };
	};

	OpenInventorLicenseManager::OpenInventorLicenseManager() : d { new Impl } { }

	OpenInventorLicenseManager::~OpenInventorLicenseManager() = default;

	auto OpenInventorLicenseManager::IsInitialized() const -> bool {
		return SoQt::isInitialized();
	}

	auto OpenInventorLicenseManager::Initialize(void* userData) -> void {
		const auto widget = static_cast<QWidget*>(userData);
		if (!widget) {
			return;
		}
		QMetaObject::invokeMethod(qApp, [this,widget] {
			if (false == SoQt::isInitialized()) {
				QStringList OivKeys;
				OivKeys.append(QString(OIV_STRING).arg(OivActivator::Convert(d->oivSign)).arg(OivActivator::Convert(d->oivKey)));
				OivKeys.append(QString(VOLVIZ_STRING).arg(OivActivator::Convert(d->volVizSign)).arg(OivActivator::Convert(d->volVizKey)));
				OivActivator::Activate(OivKeys);

				SoQt::init(widget);
				SoVolumeRendering::init();
				OivHdf5Reader::initClass();
				OivXYReader::initClass();
				OivLdmReaderXY::initClass();
				OivLdmReader::initClass();
				OivLdmReaderFL::initClass();
				OivThumbnailReader::initClass();

				OivHoveringLasso::initClass();
				OivRectangleDrawer::initClass();
				OivMultiLineDrawer::initClass();
				OivCircleDrawer::initClass();
				OivEllipseDrawer::initClass();
			}
		}, Qt::QueuedConnection);
	}

	auto OpenInventorLicenseManager::Finish() -> void {
		QMetaObject::invokeMethod(qApp, [] {
			if (SoQt::isInitialized()) {
				OivEllipseDrawer::exitClass();
				OivCircleDrawer::exitClass();
				OivMultiLineDrawer::exitClass();
				OivRectangleDrawer::exitClass();
				OivHoveringLasso::exitClass();

				OivThumbnailReader::exitClass();
				OivLdmReaderFL::exitClass();
				OivLdmReader::exitClass();
				OivLdmReaderXY::exitClass();
				OivXYReader::exitClass();
				OivHdf5Reader::exitClass();

				SoVolumeRendering::finish();
				SoQt::finish();
			}
		}, Qt::QueuedConnection);
	}
}
