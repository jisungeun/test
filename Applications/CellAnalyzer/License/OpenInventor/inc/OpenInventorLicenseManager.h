#pragma once

#include <memory>

#include "IExtLicenseManager.h"

#include "CellAnalyzer.License.OpenInventorExport.h"

namespace CellAnalyzer::License {
	class CellAnalyzer_License_OpenInventor_API OpenInventorLicenseManager final : public IExtLicenseManager {
	public:
		OpenInventorLicenseManager();
		~OpenInventorLicenseManager() override;

		auto IsInitialized() const -> bool override;
		auto Initialize(void* userData) -> void override;
		auto Finish() -> void override;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
