#pragma once

#include <memory>

#include "IExtLicenseManager.h"

#include "CellAnalyzer.License.ImageDevExport.h"

namespace CellAnalyzer::License {
	class CellAnalyzer_License_ImageDev_API ImageDevLicenseManager final : public IExtLicenseManager{
	public:
		ImageDevLicenseManager();
		~ImageDevLicenseManager() override;

		auto IsInitialized() const -> bool override;
		auto Initialize(void* userData) -> void override;
		auto Finish() -> void override;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}