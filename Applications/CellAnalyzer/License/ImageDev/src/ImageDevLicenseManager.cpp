#include <QThreadPool>

#include <OivActivator.h>
#include <ImageDev/Exception.h>
#include <ImageDev/Initialization.h>

#include "ImageDevLicenseManager.h"

namespace CellAnalyzer::License {
	constexpr const char* const INCREMENT_STRING = "INCREMENT ImageDev asglmd 2023.2 permanent uncounted ";
	constexpr const char* const VENDER_STRING = "VENDOR_STRING=INTERNALKEY HOSTID=DEMO ISSUED=26-dec-2023 ";
	constexpr const char* const SERIAL_STRING = "NOTICE=TomoAnalysis SN=23022968-IMDEV START=14-feb-2023 ";

	struct ImageDevLicenseManager::Impl {
		QStringList password { "013<", "EC89", "12;I", "E::D", "293I", "EC8G", "8DF8", "A1F9", "E:6E", "CBDF", "7225", "1D2;", "6GG<", "EG3:", "5C65", "F2E7", "4679", "E2:H", "463<", "9FHF", "17E4" };
	};

	ImageDevLicenseManager::ImageDevLicenseManager() : d { new Impl } { }

	ImageDevLicenseManager::~ImageDevLicenseManager() = default;

	auto ImageDevLicenseManager::IsInitialized() const -> bool {
		auto isInit = false;
		try {
			isInit = imagedev::isInitialized();
		} catch (imagedev::Exception&) {
			return false;
		}
		return isInit;
	}

	auto ImageDevLicenseManager::Initialize(void*) -> void {
		if (false == imagedev::isInitialized()) {
			QString keys;
			keys.append(INCREMENT_STRING);
			keys.append(VENDER_STRING);
			keys.append(SERIAL_STRING);
			keys.append("TS_OK SIGN=\"");
			keys.append(OivActivator::Convert(d->password));
			keys.append("\"");

			try {
				imagedev::init(keys.toStdString().c_str());
			} catch (imagedev::Exception&) { }
		}
	}

	auto ImageDevLicenseManager::Finish() -> void {
		imagedev::finish();
	}
}
