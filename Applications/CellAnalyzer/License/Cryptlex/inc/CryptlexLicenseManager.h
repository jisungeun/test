#pragma once

#include <memory>

#include "ILicenseManager.h"

#include "CellAnalyzer.License.CryptlexExport.h"

namespace CellAnalyzer::License {
	class CellAnalyzer_License_Cryptlex_API CryptlexLicenseManager final : public ILicenseManager {
	public:
		CryptlexLicenseManager();
		~CryptlexLicenseManager() override;

		auto AddEvent(const std::shared_ptr<ILicenseEvent>& event) -> void override;
		auto RemoveEvent(const std::shared_ptr<ILicenseEvent>& event) -> void override;

		auto IsActivated() const -> bool override;
		auto GetEdition() const -> const QString& override;
		auto GetUserName() const -> const QString& override;

		auto Activate(const QString& key) -> void override;
		auto ActivateOffline(const QString& key, const QString& filepath) -> void override;
		auto Deactivate() -> void override;
		auto DeactivateOffline(const QString& filepath) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
