#include <optional>

#include <QThreadPool>
#include <QVector>

#include "CryptlexLicenseManager.h"

#include "LicenseManager.h"

namespace CellAnalyzer::License {
	constexpr const wchar_t* const PRODUCT_DATA = L"NTI2MUZCRjY5MkRENDkzNTAxRkY5QTdCMUY2NTcxNEE=.UCOgeuyay9oY4BsGI5pzGM0bGuHbJoXR5Ra09+l2To55bJSvzrmxcR6pVBBAAVT+Ox9DQNdyp5CJGk444Z1j5p0Lmd2Eda4OSVxaIk6aYNCmj4F3yDNmSv+kJWf67R3m97egWVZRO0ZcLtd9KA4tACDMWjAIUfhOq1GJOKlOzuS/8qTfxdpjVhBjyujiFr6wyM8QIhxNuYKwD0gm8axLJ8oFU3kQZ8L+qv7U1Mm618JnNpX0UeEe2slYBYvMoDnCP5tVx7SHKoXaweQckKcRcznCmiOkGdSLvpJlOYPxWYL/z0OdePjAMgvl0pw10F2picR+RIUdpv8xn8OnDrP1+43BWdy+9UIchxaHBl95bQbgTLLEHNoW+IRVZ3wlhEt2W7xjdtc5l2WZmsGPoh5UfHx6/z+GqhwvqP7Y1ML97URNIQSzBQEM3PZhzbHarV7qd0ELXVnOSPMWWblK9WCzPfSq/37zHg7uTUkAjde+Zh0aQKVUfgYyKffjeHRPVpktja7Eg/4kZRUqhGmATFkOtUj3k/GBGzwLospEukB778HEcjINUnhNW66sgYpNqyHPCIlji9SUZ/aJ0Adv6TEnaAp61lDsRG1MpbVPLjkv4MGSSt474HYQQwjrhvMn+72qzlpmoQqVdnBLz3uTVRnWW76QBQxjsvAsoMTXLRdeVqy4jmNq+A1fMtnpjlmbpsJ3bKaW7uAWr51sNLJ81FrPHG2O00k31FPDkZPscHM7sxQ=";
	constexpr const char* const PRODUCT_ID = "8f2e8aba-ce73-4c9e-9a81-948a517a3929";

	struct CryptlexLicenseManager::Impl {
		std::optional<QString> edition = std::nullopt;
		std::optional<QString> username = std::nullopt;

		QThreadPool pool;
		QVector<std::shared_ptr<ILicenseEvent>> events;
	};

	CryptlexLicenseManager::CryptlexLicenseManager() : ILicenseManager(), d(new Impl) {
		d->pool.setMaxThreadCount(1);

		if (static auto init = false; !init) {
			init = true;
			LicenseManager::SetLicenseInfo(QString::fromStdWString(PRODUCT_DATA), QString::fromStdString(PRODUCT_ID));
		}
	}

	CryptlexLicenseManager::~CryptlexLicenseManager() = default;

	auto CryptlexLicenseManager::AddEvent(const std::shared_ptr<ILicenseEvent>& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto CryptlexLicenseManager::RemoveEvent(const std::shared_ptr<ILicenseEvent>& event) -> void {
		d->events.removeOne(event);
	}

	auto CryptlexLicenseManager::IsActivated() const -> bool {
		return LicenseManager::CheckLicense(false);
	}

	auto CryptlexLicenseManager::GetEdition() const -> const QString& {
		if (!d->edition)
			d->edition = LicenseManager::GetProductEdition();

		return *d->edition;
	}

	auto CryptlexLicenseManager::GetUserName() const -> const QString& {
		if (!d->username)
			d->username = LicenseManager::GetUser();

		return *d->username;
	}

	auto CryptlexLicenseManager::Activate(const QString& key) -> void {
		d->pool.start([this, key = key] {
						if (LicenseManager::Activate(key)) {
							d->edition = LicenseManager::GetProductEdition();
							d->username = LicenseManager::GetUser();

							for (const auto& e : d->events)
								e->OnActivated(*d->edition, false);
						} else {
							const auto error = LicenseManager::GetErrorAsString();

							for (const auto& e : d->events)
								e->OnErrorOccured(error);
						}
					}
					);
	}

	auto CryptlexLicenseManager::ActivateOffline(const QString& key, const QString& filepath) -> void {
		d->pool.start([this, filepath = filepath, key = key] {
						if (LicenseManager::ActivateOfflineLicense(key, filepath)) {
							d->edition = LicenseManager::GetProductEdition();
							d->username = LicenseManager::GetUser();

							for (const auto& e : d->events)
								e->OnActivated(*d->edition, true);
						} else {
							const auto error = LicenseManager::GetErrorAsString();

							for (const auto& e : d->events)
								e->OnErrorOccured(error);
						}
					}
					);
	}

	auto CryptlexLicenseManager::Deactivate() -> void {
		d->pool.start([this] {
						if (LicenseManager::Deactivate()) {
							d->edition = LicenseManager::GetProductEdition();
							d->username = LicenseManager::GetUser();

							for (const auto& e : d->events)
								e->OnDeactivated(false);
						} else {
							const auto error = LicenseManager::GetErrorAsString();

							for (const auto& e : d->events)
								e->OnErrorOccured(error);
						}
					}
					);
	}

	auto CryptlexLicenseManager::DeactivateOffline(const QString& filepath) -> void {
		d->pool.start([this, filepath = filepath] {
						if (LicenseManager::RequestOfflineDeactivation(filepath)) {
							d->edition = LicenseManager::GetProductEdition();
							d->username = LicenseManager::GetUser();

							for (const auto& e : d->events)
								e->OnDeactivated(true);
						} else {
							const auto error = LicenseManager::GetErrorAsString();

							for (const auto& e : d->events)
								e->OnErrorOccured(error);
						}
					}
					);
	}
}
