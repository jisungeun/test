#pragma once

#include <memory>

#include <QString>

#include "ILicenseEvent.h"
#include "IService.h"

#include "CellAnalyzer.LicenseModelExport.h"

namespace CellAnalyzer {
	class CellAnalyzer_LicenseModel_API ILicenseManager : public virtual Tomocube::IService {
	public:
		virtual auto AddEvent(const std::shared_ptr<ILicenseEvent>& event) -> void = 0;
		virtual auto RemoveEvent(const std::shared_ptr<ILicenseEvent>& event) -> void = 0;

		virtual auto IsActivated() const -> bool = 0;
		virtual auto GetEdition() const -> const QString& = 0;
		virtual auto GetUserName() const -> const QString& = 0;

		virtual auto Activate(const QString& key) -> void = 0;
		virtual auto ActivateOffline(const QString& key, const QString& filepath) -> void = 0;
		virtual auto Deactivate() -> void = 0;
		virtual auto DeactivateOffline(const QString& filepath) -> void = 0;
	};
}
