#pragma once

#include <QString>

#include "CellAnalyzer.LicenseModelExport.h"

namespace CellAnalyzer {
	class CellAnalyzer_LicenseModel_API ILicenseEvent {
	public:
		virtual ~ILicenseEvent();

		virtual auto OnActivated(const QString& edition, bool offline) -> void;
		virtual auto OnDeactivated(bool offline) -> void;
		virtual auto OnErrorOccured(const QString& message) -> void;
	};
}
