#pragma once

#include <memory>

#include <QString>

#include "IService.h"

#include "CellAnalyzer.LicenseModelExport.h"

namespace CellAnalyzer {
	class CellAnalyzer_LicenseModel_API IExtLicenseManager : public virtual Tomocube::IService {
	public:
		virtual auto IsInitialized() const -> bool = 0;
		virtual auto Initialize(void* userData) -> void = 0;
		virtual auto Finish() -> void = 0;
	};
}
