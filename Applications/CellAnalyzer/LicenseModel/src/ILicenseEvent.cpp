#include "ILicenseEvent.h"

namespace CellAnalyzer {
	ILicenseEvent::~ILicenseEvent() = default;

	auto ILicenseEvent::OnActivated(const QString& edition, bool offline) -> void {}

	auto ILicenseEvent::OnDeactivated(bool offline) -> void {}

	auto ILicenseEvent::OnErrorOccured(const QString& message) -> void {}
}
