#include <BugSplat.h>
#include <QApplication>
#include <FramelessHelper/Widgets/framelesshelperwidgets_global.h>

#include "IApplication.h"
#include "Version.h"

#include "ServiceCollection.h"

#include "CryptlexLicenseManager.h"
#include "EditorService.h"
#include "ExportService.h"
#include "ImportService.h"
#include "JsonStorage.h"
#include "MainWindow.h"
#include "MenuHandler.h"
#include "PipelineService.h"
#include "PresenterService.h"
#include "ProcessorService.h"
#include "ProcessService.h"
#include "ProjectService.h"
#include "ScreenHandler.h"
#include "StatusBarHandler.h"
#include "ToolBarHandler.h"
#include "WindowHandler.h"

#include "ImageDevLicenseManager.h"
#include "OpenInventorLicenseManager.h"

using namespace Tomocube;
using namespace CellAnalyzer;

class AppInfo final : public IApplication {
public:
	auto GetSoftwareName() const -> QString override {
		return "TomoAnalysis";
	}

	auto GetVersion() const -> Version override {
		if (const auto identifier = QString(PROJECT_VERSION_RELEASE_TYPE); identifier.size() == 1)
			return { PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR, PROJECT_VERSION_PATCH, identifier.at(0).toLatin1() };
		return { PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR, PROJECT_VERSION_PATCH };
	}

	auto GetCorporation() const -> QString override {
		return "Tomocube";
	}

	auto GetContact() const -> QString override {
		return "https://www.tomocube.com";
	}

	auto GetOpenSourceLicenseList() const -> QVector<OpenSourceLicense> override {
		return {
			{ "Qt Advanced Docking System", "https://github.com/githubuser0xFFFF/Qt-Advanced-Docking-System", "Copyright (C) 2017 Uwe Kindler", "LGPL-v2.1" }
		};
	}
};

class BugSplat;

auto BuildServiceProvider() -> std::shared_ptr<Tomocube::IServiceProvider> {
	const auto collection = std::make_unique<DependencyInjection::ServiceCollection>();

	collection
		->AddSingleton<AppInfo, IApplication>()
		->AddSingleton<UI::MainWindow, IAlertHandler>()
		->AddSingleton<UI::MenuHandler, IMenuHandler>()
		->AddSingleton<UI::ToolBarHandler, IToolBarHandler>()
		->AddSingleton<UI::StatusBarHandler, IStatusBarHandler>()
		->AddSingleton<UI::WindowHandler, IWindowHandler>()
		->AddSingleton<UI::ScreenHandler, IScreenHandler>()
		->AddSingleton<License::CryptlexLicenseManager, ILicenseManager>()
		->AddSingleton<Project::ProjectService, IProjectService>()
		->AddSingleton<Database::JsonStorage, IDatabase, IAppModule>()
		->AddSingleton<IO::Service::ExportService, IO::IExportService, IAppModule>()
		->AddSingleton<IO::Service::ImportService, IO::IImportService, IAppModule>()
		->AddSingleton<Pipeline::Service::ProcessService, Pipeline::IProcessService, IAppModule>()
		->AddSingleton<Pipeline::Service::PipelineService, Pipeline::IPipelineService, IAppModule>()
		->AddSingleton<Processor::Service::ProcessorService, Processor::IProcessorService, IAppModule>()
		->AddSingleton<Presenter::Service::PresenterService, Presenter::IPresenterService, IAppModule>()
		->AddSingleton<Editor::Service::EditorService, Editor::IEditorService, IAppModule>()
		->AddSingleton<License::OpenInventorLicenseManager, IExtLicenseManager>()
		->AddSingleton<License::ImageDevLicenseManager, IExtLicenseManager>();


	collection->AddSingleton<UI::MainWindow, IAppModule>();

	return collection->BuildProvider();
}

class BugSplat {
public:
	BugSplat() {
		wchar_t pversion[20] = { 0, };
		const AppInfo info;
		const auto version = info.GetVersion();
		version.ToString().toWCharArray(pversion);

		sender = std::make_unique<MiniDmpSender>(L"tomoanalysis_v1", L"TomoAnalysis", pversion, nullptr, MDSF_USEGUARDMEMORY | MDSF_LOGFILE | MDSF_PREVENTHIJACKING);
		sender->setGuardByteBufferSize(20 * 1024 * 1024);
		sender->setDefaultUserEmail(L"user@youremail.com");
		sender->setDefaultUserName(L"User Name");
		sender->setDefaultUserDescription(L"Describe the situation in which the SW crash occurred.");
	}

private:
	std::unique_ptr<MiniDmpSender> sender = nullptr;
};

extern "C" {
	__declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
	__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
}

auto main(int argc, char** argv) -> int {
	wangwenx190::FramelessHelper::FramelessHelper::Widgets::initialize();
	BugSplat splat;
	QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
	QGuiApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::Ceil);
	QApplication app(argc, argv);

	const auto provider = BuildServiceProvider();
	const auto modules = provider->GetServices<IAppModule>();

	for (const auto& m : modules)
		m->Start();
	for (const auto& m : modules)
		m->Stop();

	return 0;
}
