#include "RecentProjectList.h"

namespace CellAnalyzer::UI {
	constexpr int MaxRecentStartUpCount = 20;

	struct RecentProjectList::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		QList<RecentProjectPtr> list;

		auto UpdateList() -> void;
		auto UpdateDatabase() -> void;
	};

	auto RecentProjectList::Impl::UpdateList() -> void {
		if (const auto database = provider->GetService<IDatabase>()) {
			for (const auto& i : database->GetList("Project\\History")) {
				QString name;
				QString url;
				QDateTime datetime;

				if (const auto value = IDatabase::ReadString(i, "Name"))
					name = *value;
				if (const auto value = IDatabase::ReadString(i, "Url"))
					url = *value;
				if (const auto value = IDatabase::ReadNumber(i, "DateTime"))
					datetime = QDateTime::fromMSecsSinceEpoch(*value);

				if (!name.isEmpty() && !url.isEmpty() && datetime.toMSecsSinceEpoch() > 0)
					list.push_back(std::make_shared<RecentProject>(name, url, datetime));
			}
		}
	}

	auto RecentProjectList::Impl::UpdateDatabase() -> void {
		if (const auto database = provider->GetService<IDatabase>()) {
			QVariantList array;

			for (const auto& p : list) {
				QVariantMap map;
				map["Name"] = p->name;
				map["Url"] = p->url;
				map["DateTime"] = p->datetime.toMSecsSinceEpoch();

				array.push_back(map);
			}

			database->Save("Project\\History", array);
		}
	}

	RecentProjectList::RecentProjectList(Tomocube::IServiceProvider* provider) : d(new Impl) {
		d->provider = provider;
		d->UpdateList();
	}

	RecentProjectList::~RecentProjectList() = default;

	auto RecentProjectList::GetList() const -> QList<RecentProjectPtr> {
		return d->list;
	}

	auto RecentProjectList::Add(const QString& name, const QString& url) -> void {
		Remove(name, url);

		d->list.push_front(std::make_shared<RecentProject>(name, url, QDateTime::currentDateTime()));

		if (d->list.count() > MaxRecentStartUpCount)
			d->list = d->list.mid(0, MaxRecentStartUpCount);

		d->UpdateDatabase();
	}

	auto RecentProjectList::Remove(const QString& name, const QString& url) -> void {
		for (const auto& i : d->list) {
			if (i->name == name && i->url == url) {
				d->list.removeOne(i);
				d->UpdateDatabase();
				break;
			}
		}
	}
}
