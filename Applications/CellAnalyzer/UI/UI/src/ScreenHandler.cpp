#include <QBoxLayout>

#include <FramelessHelper/Widgets/FramelessWidgetsHelper>
#include <FramelessHelper/Widgets/StandardTitleBar>

#include <DockAreaWidget.h>
#include <DockManager.h>
#include <DockSplitter.h>

#include "ScreenHandler.h"

#include "IMenuHandler.h"

namespace CellAnalyzer::UI {
	using namespace ads;

	struct ScreenHandler::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		ScreenEventList events;

		std::shared_ptr<CDockManager> manager = nullptr;
		std::shared_ptr<CDockWidget> peekDock = nullptr;
		std::shared_ptr<IView> peekView = nullptr;
		std::shared_ptr<QWidget> peekWidget = nullptr;

		QMap<ViewPtr, std::shared_ptr<CDockWidget>> viewMap;
		ViewPtr focused = nullptr;

		static auto SetStylesheet(const std::shared_ptr<CDockManager>& manager) -> void;
		static auto SetFrameless(QWidget* parent) -> void;
	};

	auto ScreenHandler::Impl::SetStylesheet(const std::shared_ptr<CDockManager>& manager) -> void {
		if (QFile file(":/Stylesheet/QtAdvancedDockingSystem.qss"); file.open(QIODevice::ReadOnly))
			manager->setStyleSheet(file.readAll());
	}

	auto ScreenHandler::Impl::SetFrameless(QWidget* parent) -> void {
		auto* helper = wangwenx190::FramelessHelper::FramelessWidgetsHelper::get(parent);
		auto* title = new wangwenx190::FramelessHelper::StandardTitleBar(parent);
		auto* palette = title->chromePalette();

		helper->setTitleBarWidget(title);
		helper->setSystemButton(title->minimizeButton(), wangwenx190::FramelessHelper::Global::SystemButtonType::Minimize);
		helper->setSystemButton(title->maximizeButton(), wangwenx190::FramelessHelper::Global::SystemButtonType::Maximize);
		helper->setSystemButton(title->closeButton(), wangwenx190::FramelessHelper::Global::SystemButtonType::Close);
		title->maximizeButton()->setVisible(parent->windowFlags().testFlag(Qt::WindowType::WindowMaximizeButtonHint));
		title->minimizeButton()->setVisible(parent->windowFlags().testFlag(Qt::WindowType::WindowMinimizeButtonHint));
		title->setFixedHeight(31);
		palette->setChromeButtonHoverColor(QColor(56, 56, 59));
		palette->setChromeButtonPressColor(QColor(36, 159, 176));
		palette->setCloseButtonHoverColor(QColor(56, 56, 59));
		palette->setTitleBarActiveBackgroundColor(Qt::transparent);
		palette->setTitleBarInactiveBackgroundColor(Qt::transparent);

		if (auto* layout = dynamic_cast<QBoxLayout*>(parent->layout()))
			layout->insertWidget(0, title);
	}

	ScreenHandler::ScreenHandler(Tomocube::IServiceProvider* provider) : QMainWindow(), IScreenHandler(), d(new Impl) {
		d->provider = provider;
		d->peekWidget = std::make_shared<QWidget>(this);
		d->peekWidget->setObjectName("empty");
		d->peekDock = std::make_shared<CDockWidget>(QString(), this);
		d->peekDock->setObjectName("center");
		d->peekDock->setWidget(d->peekWidget.get());
		d->peekDock->setFeature(CDockWidget::DockWidgetDeleteOnClose, false);
		d->peekDock->setFeature(CDockWidget::DeleteContentOnClose, false);
		d->peekDock->setFeature(CDockWidget::NoTab, true);
		d->peekDock->setFeature(CDockWidget::DockWidgetFocusable, true);
		d->manager = std::make_shared<CDockManager>(this);
		d->manager->setCentralWidget(d->peekDock.get());

		d->SetStylesheet(d->manager);

		connect(d->manager.get(), &CDockManager::focusedDockWidgetChanged, this, &ScreenHandler::OnFocusChanged);
		connect(d->manager.get(), &CDockManager::floatingWidgetCreated, this, &ScreenHandler::OnFloatingWidgetCreated);
	}

	ScreenHandler::~ScreenHandler() = default;

	auto ScreenHandler::AddEvent(const ScreenEventPtr& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto ScreenHandler::RemoveEvent(const ScreenEventPtr& event) -> void {
		d->events.removeOne(event);
	}

	auto ScreenHandler::Contains(const ViewPtr& view) const -> bool {
		return d->viewMap.contains(view);
	}

	auto ScreenHandler::GetViewPeeked() const -> ViewPtr {
		return d->peekView;
	}

	auto ScreenHandler::GetViewFocused() const -> ViewPtr {
		return d->focused;
	}

	auto ScreenHandler::GetViewList() const -> ViewList {
		return d->viewMap.keys();
	}

	auto ScreenHandler::Show(const ViewPtr& view) -> void {
		if (!d->viewMap.contains(view)) {
			if (const auto dock = CreateDock(view)) {
				d->manager->addDockWidgetTab(CenterDockWidgetArea, dock.get());
				dock->raise();

				for (const auto& e : d->events)
					e->OnScreenOpened(view);
			}
		}
	}

	auto ScreenHandler::Show(const ViewPtr& view, ScreenPosition position, double ratio) -> void {
		if (!d->viewMap.contains(view)) {
			if (const auto dock = CreateDock(view)) {
				d->manager->addDockWidgetTab((position == ScreenPosition::Vertical) ? BottomDockWidgetArea : RightDockWidgetArea, dock.get());
				ResizeDock(dock, ratio);
				dock->raise();

				for (const auto& e : d->events)
					e->OnScreenOpened(view);
			}
		}
	}

	auto ScreenHandler::ShowFloating(const ViewPtr& view) -> void {
		if (!d->viewMap.contains(view)) {
			if (const auto dock = CreateDock(view)) {
				d->manager->addDockWidgetFloating(dock.get());
				dock->raise();

				for (const auto& e : d->events)
					e->OnScreenOpened(view);
			}
		}
	}

	auto ScreenHandler::Peek(const ViewPtr& view) -> void {
		if (view) {
			if (const auto widget = std::dynamic_pointer_cast<QWidget>(view)) {
				if (d->peekView != view) {
					d->peekView = view;
					d->peekDock->setWindowTitle(widget->windowTitle());
					d->peekDock->setWidget(dynamic_cast<QWidget*>(view.get()));
					d->peekDock->raise();
				}
			}
		} else {
			d->peekDock->setWindowTitle({});
			d->peekDock->setWidget(d->peekWidget.get());
			d->peekView = nullptr;
		}
	}

	auto ScreenHandler::Close(const ViewPtr& view) -> void {
		if (d->viewMap.contains(view)) {
			dynamic_cast<QWidget*>(view.get())->setParent(nullptr);
			d->manager->removeDockWidget(d->viewMap[view].get());
			d->viewMap.remove(view);

			if (d->focused == view)
				d->focused = nullptr;

			for (const auto& e : d->events)
				e->OnScreenClosed(view);
		}
	}

	auto ScreenHandler::CloseAll() -> void {
		for (const auto& view : d->viewMap.keys()) {
			dynamic_cast<QWidget*>(view.get())->setParent(nullptr);
			d->manager->removeDockWidget(d->viewMap[view].get());
		}

		d->viewMap.clear();
		d->focused = nullptr;
		Peek({});
	}

	auto ScreenHandler::IsClosable(const ViewPtr& view) const -> bool {
		if (d->viewMap.contains(view))
			return d->viewMap[view]->features().testFlag(CDockWidget::DockWidgetClosable);

		return false;
	}

	auto ScreenHandler::IsVisible(const ViewPtr& view) const -> bool {
		if (d->viewMap.contains(view))
			return d->viewMap[view]->isVisible();

		return false;
	}

	auto ScreenHandler::SetFocus(const ViewPtr& view) -> void {
		if (d->viewMap.contains(view)) {
			d->manager->setDockWidgetFocused(d->viewMap[view].get());
			d->focused = view;
		}
	}

	auto ScreenHandler::SetClosable(const ViewPtr& view, bool closable) -> void {
		if (d->viewMap.contains(view))
			d->viewMap[view]->setFeature(CDockWidget::DockWidgetClosable, closable);
	}

	auto ScreenHandler::SetVisible(const ViewPtr& view, bool visible) -> void {
		if (d->viewMap.contains(view))
			d->viewMap[view]->toggleView(visible);
	}

	auto ScreenHandler::CreateDock(const ViewPtr& view) const -> std::shared_ptr<CDockWidget> {
		if (const auto widget = std::dynamic_pointer_cast<QWidget>(view)) {
			const auto icon = widget->windowIcon();

			auto dock = std::make_shared<CDockWidget>(std::dynamic_pointer_cast<QWidget>(view)->windowTitle(), d->manager.get());
			dock->setWidget(widget.get());
			dock->setFeature(CDockWidget::DockWidgetDeleteOnClose, false);
			dock->setFeature(CDockWidget::DeleteContentOnClose, false);
			dock->setFeature(CDockWidget::DockWidgetFocusable, true);
			dock->setFeature(CDockWidget::CustomCloseHandling, true);
			dock->setFeature(CDockWidget::DockWidgetPinnable, false);

			connect(dock.get(), &CDockWidget::closeRequested, this, &ScreenHandler::OnCloseRequested, Qt::QueuedConnection);
			connect(dock.get(), &CDockWidget::visibilityChanged, this, &ScreenHandler::OnVisibilityChanged);

			widget->setWindowIcon(icon);
			d->viewMap[view] = dock;

			return dock;
		}

		return {};
	}

	auto ScreenHandler::ResizeDock(const std::shared_ptr<CDockWidget>& dock, double ratio) -> void {
		if (ratio > 0.0 && ratio < 1.0) {
			if (auto* splitter = internal::findParent<CDockSplitter*>(dock->dockAreaWidget())) {
				if (auto sizes = splitter->sizes(); !sizes.isEmpty()) {
					const auto idx = splitter->indexOf(dock->dockAreaWidget());
					const auto size = sizes[idx];
					const double sum = std::accumulate(sizes.begin(), sizes.end(), 0);

					for (auto& s : sizes)
						s = static_cast<int>((sum - (sum * ratio)) * (s / (sum - size)));

					sizes[idx] = static_cast<int>(sum * ratio);
					splitter->setSizes(sizes);
				}
			}
		}
	}

	auto ScreenHandler::OnFocusChanged(const CDockWidget* old, const CDockWidget* now) -> void {
		ViewPtr view = nullptr;

		if (d->peekDock.get() == now) {
			if (d->focused != d->peekView)
				view = d->peekView;
		} else {
			for (const auto& k : d->viewMap.keys()) {
				if (d->viewMap[k].get() == now) {
					if (d->focused != k)
						view = k;
					break;
				}
			}
		}

		if (view) {
			if (view != d->peekView)
				Peek({});

			const auto oldfocus = d->focused;
			d->focused = view;

			for (const auto& e : d->events)
				e->OnFocusChanged(view, oldfocus);
		}
	}

	auto ScreenHandler::OnCloseRequested() const -> void {
		const auto* dock = dynamic_cast<CDockWidget*>(sender());

		for (const auto& view : d->viewMap.keys()) {
			if (d->viewMap[view].get() == dock) {
				if (const auto handler = d->provider->GetService<IMenuHandler>(); handler->ContainsWindow(view)) {
					if (d->viewMap.contains(view))
						d->viewMap[view]->toggleView(false);
				} else {
					std::dynamic_pointer_cast<QWidget>(view)->setParent(nullptr);
					d->manager->removeDockWidget(d->viewMap[view].get());
					d->viewMap.remove(view);
				}
			}
		}
	}

	auto ScreenHandler::OnVisibilityChanged(bool visibility) -> void {
		if (visibility) {
			const auto* dock = dynamic_cast<CDockWidget*>(sender());

			if (auto* container = dock->floatingDockContainer()) {
				const auto* helper = wangwenx190::FramelessHelper::FramelessWidgetsHelper::get(container);
				auto* title = dynamic_cast<wangwenx190::FramelessHelper::StandardTitleBar*>(helper->titleBarWidget());

				IView::Run([this, ico = dock->windowIcon(), container] {
					if (ico.isNull())
						container->setWindowIcon(QIcon(":/System/Icon_DE.png"));
					else
						container->setWindowIcon(ico);
				});

				title->closeButton()->setVisible(dock->features().testFlag(CDockWidget::DockWidgetClosable));
				title->setTitleLabelVisible(true);
				title->setWindowIconVisible(true);
				title->setWindowTitle(dock->windowTitle());
			}
		}
	}

	auto ScreenHandler::OnFloatingWidgetCreated(CFloatingDockContainer* container) -> void {
		d->SetFrameless(container);
	}
}
