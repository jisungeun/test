#include <QMap>
#include <QBoxLayout>

#include <FramelessHelper/Widgets/FramelessWidgetsHelper>
#include <FramelessHelper/Widgets/StandardTitleBar>

#include <AutoHideDockContainer.h>
#include <DockAreaWidget.h>
#include <DockManager.h>
#include <DockSplitter.h>

#include "WindowHandler.h"

#include "IMenuHandler.h"
#include "IProjectService.h"
#include "ScreenHandler.h"

namespace CellAnalyzer::UI {
	using namespace ads;

	struct WindowHandler::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		std::shared_ptr<ScreenHandler> screen = nullptr;

		WindowEventList events;
		std::shared_ptr<CDockWidget> centralDock = nullptr;
		std::shared_ptr<CDockManager> manager = nullptr;
		QMap<ViewPtr, std::shared_ptr<CDockWidget>> viewMap;

		static auto SetCDockManagerConfigs() -> void;
		static auto SetStylesheet(const std::shared_ptr<CDockManager>& manager) -> void;
		static auto SetFrameless(QWidget* parent) -> void;

		auto GetDockArea(DockWidgetArea area) const -> CDockAreaWidget*;
	};

	auto WindowHandler::Impl::SetCDockManagerConfigs() -> void {
		CDockManager::setConfigFlag(CDockManager::RetainTabSizeWhenCloseButtonHidden);
		CDockManager::setConfigFlag(CDockManager::DragPreviewShowsContentPixmap, false);
		CDockManager::setConfigFlag(CDockManager::DockAreaHasUndockButton, false);
		CDockManager::setConfigFlag(CDockManager::HideSingleCentralWidgetTitleBar, false);
		CDockManager::setConfigFlag(CDockManager::DockAreaHasTabsMenuButton);
		CDockManager::setConfigFlag(CDockManager::FloatingContainerHasWidgetIcon);
		CDockManager::setConfigFlag(CDockManager::DockAreaHideDisabledButtons);
		CDockManager::setConfigFlag(CDockManager::FocusHighlighting);
		CDockManager::setConfigFlag(CDockManager::MiddleMouseButtonClosesTab);
		CDockManager::setConfigFlag(CDockManager::EqualSplitOnInsertion);
		CDockManager::setConfigFlag(CDockManager::DockAreaDynamicTabsMenuButtonVisibility);

		CDockManager::setAutoHideConfigFlag(CDockManager::AutoHideFeatureEnabled, false);
	}

	auto WindowHandler::Impl::SetStylesheet(const std::shared_ptr<CDockManager>& manager) -> void {
		if (QFile file(":/Stylesheet/QtAdvancedDockingSystem.qss"); file.open(QIODevice::ReadOnly))
			manager->setStyleSheet(file.readAll());
	}

	auto WindowHandler::Impl::SetFrameless(QWidget* parent) -> void {
		auto* helper = wangwenx190::FramelessHelper::FramelessWidgetsHelper::get(parent);
		auto* title = new wangwenx190::FramelessHelper::StandardTitleBar(parent);
		auto* palette = title->chromePalette();

		helper->setTitleBarWidget(title);
		helper->setSystemButton(title->minimizeButton(), wangwenx190::FramelessHelper::Global::SystemButtonType::Minimize);
		helper->setSystemButton(title->maximizeButton(), wangwenx190::FramelessHelper::Global::SystemButtonType::Maximize);
		helper->setSystemButton(title->closeButton(), wangwenx190::FramelessHelper::Global::SystemButtonType::Close);
		title->maximizeButton()->setVisible(parent->windowFlags().testFlag(Qt::WindowType::WindowMaximizeButtonHint));
		title->minimizeButton()->setVisible(parent->windowFlags().testFlag(Qt::WindowType::WindowMinimizeButtonHint));
		title->setFixedHeight(31);
		palette->setTitleBarActiveForegroundColor({ 255, 255, 255 });
		palette->setTitleBarInactiveForegroundColor({ 128, 128, 128 });
		palette->setTitleBarActiveBackgroundColor(Qt::transparent);
		palette->setTitleBarInactiveBackgroundColor(Qt::transparent);
		palette->setChromeButtonHoverColor(QColor(56, 56, 59));
		palette->setChromeButtonPressColor(QColor(36, 159, 176));
		palette->setCloseButtonHoverColor(QColor(56, 56, 59));

		if (auto* layout = dynamic_cast<QBoxLayout*>(parent->layout()))
			layout->insertWidget(0, title);
	}

	auto WindowHandler::Impl::GetDockArea(DockWidgetArea area) const -> CDockAreaWidget* {
		const auto temp = std::make_unique<CDockWidget>(QString());
		auto* container = manager->addDockWidgetTab(area, temp.get());
		manager->removeDockWidget(temp.get());

		for (auto i = 0; i < manager->dockAreaCount(); i++) {
			if (manager->dockArea(i) == container)
				return container;
		}

		return nullptr;
	}

	WindowHandler::WindowHandler(Tomocube::IServiceProvider* provider) : QMainWindow(), IWindowHandler(), d(new Impl) {
		d->SetCDockManagerConfigs();
		d->provider = provider;
		d->manager = std::make_shared<CDockManager>(this);
		d->screen = d->provider->GetService<ScreenHandler>();
		d->SetStylesheet(d->manager);

		d->centralDock = std::make_shared<CDockWidget>(QString(), this);
		d->centralDock->setObjectName("view");
		d->centralDock->setWidget(d->screen.get());
		d->manager->setCentralWidget(d->centralDock.get());

		connect(d->manager.get(), &CDockManager::floatingWidgetCreated, this, &WindowHandler::OnFloatingWidgetCreated);
	}

	WindowHandler::~WindowHandler() = default;

	auto WindowHandler::AddEvent(const WindowEventPtr& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto WindowHandler::RemoveEvent(const WindowEventPtr& event) -> void {
		d->events.removeOne(event);
	}

	auto WindowHandler::GetViewList() const -> ViewList {
		return d->viewMap.keys();
	}

	auto WindowHandler::Contains(const ViewPtr& view) const -> bool {
		return d->viewMap.contains(view);
	}

	auto WindowHandler::Show(const ViewPtr& view, WindowPosition position, double ratio) -> void {
		if (!d->viewMap.contains(view)) {
			if (const auto dock = CreateDock(view)) {
				const auto size = dock->size();

				switch (position) {
					case WindowPosition::Top:
						d->manager->addDockWidget(TopDockWidgetArea, dock.get());
						break;
					case WindowPosition::Bottom:
						d->manager->addDockWidget(BottomDockWidgetArea, dock.get());
						break;
					case WindowPosition::Left:
						d->manager->addDockWidget(LeftDockWidgetArea, dock.get());
						break;
					case WindowPosition::Right:
						d->manager->addDockWidget(RightDockWidgetArea, dock.get());
						break;
				}

				dock->resize(size);
				ResizeDock(dock, position, ratio);

				for (const auto& e : d->events)
					e->OnWindowShown(view);
			}
		}
	}

	auto WindowHandler::ShowFloating(const ViewPtr& view) -> void {
		if (!d->viewMap.contains(view)) {
			if (const auto dock = CreateDock(view)) {
				d->manager->addDockWidgetFloating(dock.get());

				for (const auto& e : d->events)
					e->OnWindowShown(view);
			}
		}
	}

	//auto WindowHandler::ShowPinned(const ViewPtr& view, WindowPosition position) -> void {
	//	if (!d->viewMap.contains(view)) {
	//		if (const auto dock = CreateDock(view)) {
	//			const auto size = dock->widget()->size();

	//			switch (position) {
	//				case WindowPosition::Top:
	//					d->manager->addAutoHideDockWidget(SideBarTop, dock.get());
	//					dock->autoHideDockContainer()->setSize(size.height());
	//					break;
	//				case WindowPosition::Bottom:
	//					d->manager->addAutoHideDockWidget(SideBarBottom, dock.get());
	//					dock->autoHideDockContainer()->setSize(size.height());
	//					break;
	//				case WindowPosition::Left:
	//					d->manager->addAutoHideDockWidget(SideBarLeft, dock.get());
	//					dock->autoHideDockContainer()->setSize(size.width());
	//					break;
	//				case WindowPosition::Right:
	//					d->manager->addAutoHideDockWidget(SideBarRight, dock.get());
	//					dock->autoHideDockContainer()->setSize(size.width());
	//					break;
	//			}

	//			for (const auto& e : d->events)
	//				e->OnWindowShown(view);
	//		}
	//	}
	//}

	auto WindowHandler::ShowTabbed(const ViewPtr& view, const ViewPtr& target) -> void {
		if (!d->viewMap.contains(view) && d->viewMap.contains(target)) {
			if (const auto dock = CreateDock(view)) {
				d->manager->addDockWidgetTabToArea(dock.get(), d->viewMap[target]->dockAreaWidget());

				for (const auto& e : d->events)
					e->OnWindowShown(view);
			}
		}
	}

	auto WindowHandler::ShowSplit(const ViewPtr& view, const ViewPtr& target, WindowPosition position, double ratio) -> void {
		if (!d->viewMap.contains(view) && d->viewMap.contains(target)) {
			if (const auto dock = CreateDock(view)) {
				const auto minSize = dock->minimumSize();
				const auto maxSize = dock->maximumSize();
				dock->setMinimumSize(d->viewMap[target]->size());
				dock->setMaximumSize(d->viewMap[target]->size());

				switch (auto* area = d->viewMap[target]->dockAreaWidget(); position) {
					case WindowPosition::Top:
						d->manager->addDockWidget(TopDockWidgetArea, dock.get(), area);
						break;
					case WindowPosition::Bottom:
						d->manager->addDockWidget(BottomDockWidgetArea, dock.get(), area);
						break;
					case WindowPosition::Left:
						d->manager->addDockWidget(LeftDockWidgetArea, dock.get(), area);
						break;
					case WindowPosition::Right:
						d->manager->addDockWidget(RightDockWidgetArea, dock.get(), area);
						break;
				}

				dock->setMinimumSize(minSize);
				dock->setMaximumSize(maxSize);
				ResizeDock(dock, position, ratio);

				for (const auto& e : d->events)
					e->OnWindowShown(view);
			}
		}
	}

	auto WindowHandler::Close(const ViewPtr& view) -> void {
		if (d->viewMap.contains(view)) {
			std::dynamic_pointer_cast<QWidget>(view)->setParent(nullptr);
			d->manager->removeDockWidget(d->viewMap[view].get());
			d->viewMap.remove(view);
		}
	}

	auto WindowHandler::CloseAll() -> void {
		for (const auto& window : d->viewMap.keys()) {
			dynamic_cast<QWidget*>(window.get())->setParent(nullptr);
			d->manager->removeDockWidget(d->viewMap[window].get());
		}

		d->viewMap.clear();
	}

	//auto WindowHandler::IsPinned(const ViewPtr& view) const -> bool {
	//	if (d->viewMap.contains(view))
	//		return d->viewMap[view]->isAutoHide();

	//	return false;
	//}

	auto WindowHandler::IsVisible(const ViewPtr& view) const -> bool {
		if (d->viewMap.contains(view))
			return d->viewMap[view]->isVisible();

		return false;
	}

	auto WindowHandler::IsClosable(const ViewPtr& view) const -> bool {
		if (d->viewMap.contains(view))
			return d->viewMap[view]->features().testFlag(CDockWidget::DockWidgetClosable);

		return false;
	}

	auto WindowHandler::SetFocus(const ViewPtr& view) -> void {
		if (d->viewMap.contains(view))
			d->manager->setDockWidgetFocused(d->viewMap[view].get());
	}

	//auto WindowHandler::SetPinned(const ViewPtr& view, bool pinned) -> void {
	//	if (d->viewMap.contains(view))
	//		d->viewMap[view]->setAutoHide(pinned);
	//}

	auto WindowHandler::SetVisible(const ViewPtr& view, bool visible) -> void {
		if (d->viewMap.contains(view))
			d->viewMap[view]->toggleView(visible);
	}

	auto WindowHandler::SetClosable(const ViewPtr& view, bool closable) -> void {
		if (d->viewMap.contains(view))
			d->viewMap[view]->setFeature(CDockWidget::DockWidgetClosable, closable);
	}

	auto WindowHandler::CreateDock(const ViewPtr& view) const -> std::shared_ptr<CDockWidget> {
		if (const auto widget = std::dynamic_pointer_cast<QWidget>(view)) {
			const auto size = widget->size();
			const auto icon = widget->windowIcon();
			const auto minSize = widget->minimumSize();
			const auto maxSize = widget->maximumSize();

			auto dock = std::make_shared<CDockWidget>(widget->windowTitle(), d->manager.get());
			dock->setMinimumSizeHintMode(CDockWidget::MinimumSizeHintFromContentMinimumSize);
			dock->setFeature(CDockWidget::CustomCloseHandling, true);

			connect(dock.get(), &CDockWidget::closeRequested, this, &WindowHandler::OnCloseRequested, Qt::QueuedConnection);
			connect(dock.get(), &CDockWidget::visibilityChanged, this, &WindowHandler::OnVisibilityChanged);

			dock->setWidget(widget.get());
			dock->resize(size);
			dock->setMinimumSize(minSize);
			dock->setMaximumSize(maxSize);
			widget->resize(size);
			widget->setWindowIcon(icon);
			widget->setMinimumSize(minSize);
			widget->setMaximumSize(maxSize);

			d->viewMap[view] = dock;
			return dock;
		}

		return {};
	}

	auto WindowHandler::ResizeDock(const std::shared_ptr<CDockWidget>& dock, WindowPosition position, double ratio) -> void {
		if (auto* splitter = internal::findParent<CDockSplitter*>(dock->dockAreaWidget())) {
			if (auto sizes = splitter->sizes(); !sizes.isEmpty()) {
				const auto idx = splitter->indexOf(dock->dockAreaWidget());

				if (ratio > 0.0 && ratio < 1.0) {
					const double sum = std::accumulate(sizes.begin(), sizes.end(), 0);
					const auto size = sizes[idx];

					for (auto& s : sizes)
						s = static_cast<int>((sum - (sum * ratio)) * (s / (sum - size)));

					sizes[idx] = static_cast<int>(sum * ratio);
				} else {
					switch (position) {
						case WindowPosition::Top:
						case WindowPosition::Bottom:
							sizes[idx] = std::max(sizes[idx], dock->height());
							break;
						case WindowPosition::Left:
						case WindowPosition::Right:
							sizes[idx] = std::max(sizes[idx], dock->width());
							break;
					}
				}

				splitter->setSizes(sizes);
			}
		}
	}

	auto WindowHandler::OnCloseRequested() const -> void {
		const auto* dock = dynamic_cast<CDockWidget*>(sender());

		for (const auto& view : d->viewMap.keys()) {
			if (d->viewMap[view].get() == dock) {
				if (const auto handler = d->provider->GetService<IMenuHandler>(); handler->ContainsWindow(view)) {
					if (d->viewMap.contains(view))
						d->viewMap[view]->toggleView(false);
				} else {
					std::dynamic_pointer_cast<QWidget>(view)->setParent(nullptr);
					d->manager->removeDockWidget(d->viewMap[view].get());
					d->viewMap.remove(view);
				}
			}
		}
	}

	auto WindowHandler::OnVisibilityChanged(bool visibility) -> void {
		if (visibility) {
			const auto* dock = dynamic_cast<CDockWidget*>(sender());

			if (auto* container = dock->floatingDockContainer()) {
				const auto* helper = wangwenx190::FramelessHelper::FramelessWidgetsHelper::get(container);
				auto* title = dynamic_cast<wangwenx190::FramelessHelper::StandardTitleBar*>(helper->titleBarWidget());

				IView::Run([this, dock, container] {
					if (const auto ico = dock->windowIcon(); ico.isNull())
						container->setWindowIcon(QIcon(":/System/Icon_DE.png"));
					else
						container->setWindowIcon(ico);
				});
				
				title->closeButton()->setVisible(dock->features().testFlag(CDockWidget::DockWidgetClosable));
				title->setTitleLabelVisible(true);
				title->setWindowIconVisible(true);
				title->setWindowTitle(dock->windowTitle());
			}
		}
	}

	auto WindowHandler::OnFloatingWidgetCreated(CFloatingDockContainer* container) -> void {
		d->SetFrameless(container);
	}
}
