#include <QDateTime>

#include "MenuHandler.h"
#include "AboutDialog.h"
#include "RecentProjectList.h"
#include "StartUpDialog.h"

#include "IAlertHandler.h"
#include "IProjectService.h"
#include "IScreenHandler.h"
#include "IToolBarHandler.h"
#include "IWindowHandler.h"

#include "ui_MenuHandler.h"

namespace CellAnalyzer::UI {
	struct MenuHandler::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		std::unique_ptr<RecentProjectList> recentProject = nullptr;
		Ui::MenuHandler ui;

		QMap<ViewPtr, std::shared_ptr<QAction>> toolBar;
		QMap<ViewPtr, std::shared_ptr<QAction>> window;
		QMap<ViewPtr, std::shared_ptr<QAction>> screen;
		QMap<MenuPtr, std::shared_ptr<QAction>> menu;
		QMap<MenuPtr, QList<std::shared_ptr<QAction>>> fileMenu;
		QMap<MenuPtr, QList<std::shared_ptr<QAction>>> helpMenu;

		auto UpdateProjectList(MenuHandler* parent) const -> void;
		auto UpdateProjectHistory(MenuHandler* parent) const -> void;
	};

	auto MenuHandler::Impl::UpdateProjectList(MenuHandler* parent) const -> void {
		const auto manager = provider->GetService<IProjectService>();

		for (const auto& p : manager->GetProjectList()) {
			const auto [name, format, description, icon] = manager->GetProjectInfo(p);

			ui.menuOpen->addAction(QIcon(icon), name, [parent, name = name] {
				parent->StartProject(name);
			});
		}
	}

	auto MenuHandler::Impl::UpdateProjectHistory(MenuHandler* parent) const -> void {
		const auto manager = provider->GetService<IProjectService>();

		ui.menuRecent->clear();

		for (const auto& p : recentProject->GetList()) {
			auto title = manager->GetProjectName(p->name, p->url);
			const auto info = manager->GetProjectInfo(p->name);

			if (!p->url.isEmpty()) {
				if (const auto maxSize = 60 - title.count(); p->url.count() > maxSize)
					title.append(QString(" (%1...)").arg(p->url.mid(0, maxSize)));
				else
					title.append(QString(" (%1)").arg(p->url));
			}

			ui.menuRecent->addAction(QIcon(info.icon), title, [parent, name = p->name, url = p->url] {
				parent->StartHistory(name, url);
			});
		}
	}

	MenuHandler::MenuHandler(Tomocube::IServiceProvider* provider) : QMenuBar(), IMenuHandler(), d(new Impl) {
		d->provider = provider;
		d->recentProject = std::make_unique<RecentProjectList>(provider);

		d->ui.setupUi(this);
		d->ui.menuToolBar->menuAction()->setVisible(false);
		d->ui.menuWindow->menuAction()->setVisible(false);
		d->ui.menuScreen->menuAction()->setVisible(false);
		d->UpdateProjectList(this);
		d->UpdateProjectHistory(this);

		connect(d->ui.actionStart, &QAction::triggered, this, &MenuHandler::OnStartTriggered);
		connect(d->ui.actionClose, &QAction::triggered, this, &MenuHandler::OnCloseTriggered);
		connect(d->ui.actionExit, &QAction::triggered, this, &MenuHandler::OnExitTriggered);
		connect(d->ui.actionAbout, &QAction::triggered, this, &MenuHandler::OnAboutTriggered);
	}

	MenuHandler::~MenuHandler() = default;

	auto MenuHandler::ShowStart() -> bool {
		const auto dialog = std::make_shared<View::StartUpDialog>(d->provider, this);
		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto manager = d->provider->GetService<IProjectService>();

		for (const auto& p : manager->GetProjectList()) {
			const auto [name, format, desc, icon] = manager->GetProjectInfo(p);
			dialog->AddProject(name, desc, QIcon(icon));
		}

		for (const auto& p : d->recentProject->GetList()) {
			const auto title = manager->GetProjectName(p->name, p->url);
			const auto info = manager->GetProjectInfo(p->name);

			dialog->AddHistory(p->name, title, p->url, p->datetime, QIcon(info.icon));
		}

		connect(dialog.get(), &View::StartUpDialog::ProjectSelected, this, [this, dialog = dialog.get()](const QString& id) {
			if (StartProject(id)) {
				dialog->accept();
				dialog->close();
			}
		});

		connect(dialog.get(), &View::StartUpDialog::HistorySelected, this, [this, dialog = dialog.get()](const QString& id, const QString& url) {
			if (StartHistory(id, url)) {
				dialog->accept();
				dialog->close();
			} else
				dialog->RemoveHistory(id, url);
		});

		return handler->ShowDialog(dialog) == QDialog::Accepted;
	}

	auto MenuHandler::ContainsToolBar(const ViewPtr& view) const -> bool {
		return d->toolBar.contains(view);
	}

	auto MenuHandler::ContainsWindow(const ViewPtr& view) const -> bool {
		return d->window.contains(view);
	}

	auto MenuHandler::ContainsScreen(const ViewPtr& view) const -> bool {
		return d->screen.contains(view);
	}

	auto MenuHandler::ContainsMenu(const MenuPtr& menu) const -> bool {
		return d->menu.contains(menu);
	}

	auto MenuHandler::AddToolBar(const ViewPtr& view) -> void {
		if (const auto widget = std::dynamic_pointer_cast<QWidget>(view)) {
			d->toolBar[view] = std::shared_ptr<QAction>(d->ui.menuToolBar->addAction(widget->windowIcon(), widget->windowTitle(), [this, view] {
				const auto handler = d->provider->GetService<IToolBarHandler>();
				handler->SetVisible(view, true);
			}));

			d->ui.menuToolBar->menuAction()->setVisible(!d->toolBar.isEmpty());
		}
	}

	auto MenuHandler::AddWindow(const ViewPtr& view) -> void {
		if (const auto widget = std::dynamic_pointer_cast<QWidget>(view)) {
			d->window[view] = std::shared_ptr<QAction>(d->ui.menuWindow->addAction(widget->windowIcon(), widget->windowTitle(), [this, view] {
				const auto handler = d->provider->GetService<IWindowHandler>();
				handler->SetVisible(view, true);
				handler->SetFocus(view);
			}));

			d->ui.menuWindow->menuAction()->setVisible(!d->window.isEmpty());
		}
	}

	auto MenuHandler::AddScreen(const ViewPtr& view) -> void {
		if (const auto widget = std::dynamic_pointer_cast<QWidget>(view)) {
			d->screen[view] = std::shared_ptr<QAction>(d->ui.menuScreen->addAction(widget->windowIcon(), widget->windowTitle(), [this, view] {
				const auto handler = d->provider->GetService<IScreenHandler>();
				handler->SetVisible(view, true);
				handler->SetFocus(view);
			}));

			d->ui.menuScreen->menuAction()->setVisible(!d->screen.isEmpty());
		}
	}

	auto MenuHandler::AddMenu(const MenuPtr& menu) -> void {
		if (const auto m = std::dynamic_pointer_cast<QMenu>(menu)) {
			switch (menu->GetParent()) {
				case MenuParent::File:
					for (auto* action : m->actions()) {
						if (action->isSeparator())
							d->fileMenu[menu].push_back(std::shared_ptr<QAction>(d->ui.menuFile->insertSeparator(d->ui.actionClose)));
						else if (auto* submenu = action->menu())
							d->fileMenu[menu].push_back(std::shared_ptr<QAction>(d->ui.menuFile->insertMenu(d->ui.actionClose, submenu)));
						else
							d->ui.menuFile->insertAction(d->ui.actionClose, action);
					}

					d->fileMenu[menu].push_back(std::shared_ptr<QAction>(d->ui.menuFile->insertSeparator(d->ui.actionClose)));
					break;
				case MenuParent::Help:
					for (auto* action : m->actions()) {
						if (action->isSeparator())
							d->helpMenu[menu].push_back(std::shared_ptr<QAction>(d->ui.menuHelp->insertSeparator(d->ui.actionAbout)));
						else if (auto* submenu = action->menu())
							d->helpMenu[menu].push_back(std::shared_ptr<QAction>(d->ui.menuHelp->insertMenu(d->ui.actionAbout, submenu)));
						else
							d->ui.menuHelp->insertAction(d->ui.actionAbout, action);
					}

					d->fileMenu[menu].push_back(std::shared_ptr<QAction>(d->ui.menuFile->insertSeparator(d->ui.actionClose)));
					break;
				case MenuParent::New:
					if (const auto action = std::shared_ptr<QAction>(addMenu(m.get())))
						d->menu[menu] = action;
			}
		}
	}

	auto MenuHandler::RemoveToolBar(const ViewPtr& view) -> void {
		d->toolBar.remove(view);
		d->ui.menuToolBar->menuAction()->setVisible(!d->toolBar.isEmpty());

		if (const auto handler = d->provider->GetService<IToolBarHandler>(); handler->Contains(view) && !handler->IsVisible(view))
			handler->Close(view);
	}

	auto MenuHandler::RemoveWindow(const ViewPtr& view) -> void {
		d->window.remove(view);
		d->ui.menuWindow->menuAction()->setVisible(!d->window.isEmpty());

		if (const auto handler = d->provider->GetService<IWindowHandler>(); handler->Contains(view) && !handler->IsVisible(view))
			handler->Close(view);
	}

	auto MenuHandler::RemoveScreen(const ViewPtr& view) -> void {
		d->screen.remove(view);
		d->ui.menuScreen->menuAction()->setVisible(!d->screen.isEmpty());

		if (const auto handler = d->provider->GetService<IScreenHandler>(); handler->Contains(view) && !handler->IsVisible(view))
			handler->Close(view);
	}

	auto MenuHandler::RemoveMenu(const MenuPtr& menu) -> void {
		d->menu.remove(menu);
	}

	auto MenuHandler::ClearToolBars() -> void {
		const auto handler = d->provider->GetService<IToolBarHandler>();

		for (const auto& v : d->window.keys()) {
			if (handler->Contains(v) && !handler->IsVisible(v))
				handler->Close(v);
		}

		d->toolBar.clear();
		d->ui.menuToolBar->menuAction()->setVisible(false);
	}

	auto MenuHandler::ClearWindows() -> void {
		const auto handler = d->provider->GetService<IWindowHandler>();

		for (const auto& v : d->window.keys()) {
			if (handler->Contains(v) && !handler->IsVisible(v))
				handler->Close(v);
		}

		d->window.clear();
		d->ui.menuWindow->menuAction()->setVisible(false);
	}

	auto MenuHandler::ClearScreens() -> void {
		const auto handler = d->provider->GetService<IScreenHandler>();

		for (const auto& v : d->screen.keys()) {
			if (handler->Contains(v) && !handler->IsVisible(v))
				handler->Close(v);
		}

		d->screen.clear();
		d->ui.menuScreen->menuAction()->setVisible(false);
	}

	auto MenuHandler::ClearMenu() -> void {
		d->menu.clear();
		d->fileMenu.clear();
	}

	auto MenuHandler::OnStartTriggered() -> void {
		ShowStart();
	}

	auto MenuHandler::OnCloseTriggered() -> void {
		const auto manager = d->provider->GetService<IProjectService>();
		manager->Dispose();
	}

	auto MenuHandler::OnExitTriggered() -> void {
		QApplication::exit();
	}

	auto MenuHandler::OnAboutTriggered() -> void {
		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto about = std::make_shared<View::AboutDialog>(d->provider, this);
		handler->ShowDialog(about);
	}

	auto MenuHandler::StartProject(const QString& project) -> bool {
		const auto manager = d->provider->GetService<IProjectService>();

		if (const auto url = manager->FindUrl(project); !url.isEmpty()) {
			if (manager->Initialize(project, url)) {
				d->recentProject->Add(project, url);
				d->UpdateProjectHistory(this);
				return true;
			}

			const auto alert = d->provider->GetService<IAlertHandler>();
			alert->ShowMessage("Project Error", "Project could not be initialized.", AlertIcon::Error);
		}

		return false;
	}

	auto MenuHandler::StartHistory(const QString& project, const QString& url) -> bool {
		if (const auto manager = d->provider->GetService<IProjectService>(); manager->Initialize(project, url)) {
			d->recentProject->Add(project, url);
			d->UpdateProjectHistory(this);
			return true;
		}

		if (const auto alert = d->provider->GetService<IAlertHandler>(); alert->ShowYesNo("Project Error", "Project could not be initialized. \nDo you want to remove from history?", AlertIcon::Error)) {
			d->recentProject->Remove(project, url);
			d->UpdateProjectHistory(this);
		}

		return false;
	}
}
