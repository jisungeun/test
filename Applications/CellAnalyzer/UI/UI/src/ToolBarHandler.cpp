#include <QHBoxLayout>
#include <QToolBar>

#include "ToolBarHandler.h"
#include "MenuHandler.h"
#include "ScreenHandler.h"
#include "WindowHandler.h"

namespace CellAnalyzer::UI {
	struct ToolBarHandler::Impl {
		Tomocube::IServiceProvider* provider = nullptr;

		QMap<ViewPtr, std::shared_ptr<QToolBar>> toolBar;
	};

	ToolBarHandler::ToolBarHandler(Tomocube::IServiceProvider* provider) : QMainWindow(), IToolBarHandler(), d(new Impl) {
		d->provider = provider;

		auto* widget = new QWidget(this);
		widget->setLayout(new QHBoxLayout(widget));
		widget->layout()->setContentsMargins(9, 0, 9, 10);
		widget->layout()->addWidget(d->provider->GetService<WindowHandler>().get());

		this->setCentralWidget(widget);
	}

	ToolBarHandler::~ToolBarHandler() = default;

	auto ToolBarHandler::Contains(const ViewPtr& view) const -> bool {
		return d->toolBar.contains(view);
	}

	auto ToolBarHandler::Show(const ViewPtr& view, ToolBarPosition position) -> void {
		if (const auto widget = std::dynamic_pointer_cast<QWidget>(view)) {
			const auto toolbar = std::make_shared<QToolBar>(widget->windowTitle(), this);
			toolbar->addWidget(widget.get());
			d->toolBar[view] = toolbar;

			switch (position) {
				case ToolBarPosition::Top:
					addToolBar(Qt::TopToolBarArea, toolbar.get());
					break;
				case ToolBarPosition::Left:
					addToolBar(Qt::LeftToolBarArea, toolbar.get());
					break;
				case ToolBarPosition::Right:
					addToolBar(Qt::RightToolBarArea, toolbar.get());
					break;
			}
		}
	}

	auto ToolBarHandler::Close(const ViewPtr& view) -> void {
		d->toolBar.remove(view);
	}

	auto ToolBarHandler::IsVisible(const ViewPtr& view) const -> bool {
		if (d->toolBar.contains(view))
			return d->toolBar[view]->isVisible();

		return false;
	}

	auto ToolBarHandler::SetVisible(const ViewPtr& view, bool visible) -> void {
		if (d->toolBar.contains(view))
			d->toolBar[view]->setVisible(visible);
	}
}
