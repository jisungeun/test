#include "StatusTask.h"

namespace CellAnalyzer::UI {
	struct StatusTask::Impl {
		QString message;
		QString error;
		TaskStatus status = TaskStatus::Pending;
		int max = 100;
		int progress = 0;
	};

	StatusTask::StatusTask(const QString& status) : QObject(), IStatusTask(), d(new Impl) {
		d->message = status;
	}

	StatusTask::~StatusTask() = default;

	auto StatusTask::SetMessage(const QString& message) -> void {
		d->message = message;

		emit Updated();
	}

	auto StatusTask::SetError(const QString& message) -> void {
		d->status = TaskStatus::Error;
		d->error = message;

		emit Updated();
	}

	auto StatusTask::SetProgress(int value) -> void {
		d->status = TaskStatus::Running;
		d->progress = value;
		d->max = 100;

		emit Updated();
	}

	auto StatusTask::SetProgress(int value, int max) -> void {
		d->status = TaskStatus::Running;
		d->progress = value;
		d->max = max;
		
		emit Updated();
	}

	auto StatusTask::SetIntermediate() -> void {
		d->status = TaskStatus::Intermediate;

		emit Updated();
	}

	auto StatusTask::SetPending() -> void {
		d->status = TaskStatus::Pending;

		emit Updated();
	}

	auto StatusTask::SetRunning() -> void {
		d->status = TaskStatus::Running;

		emit Updated();
	}

	auto StatusTask::AddProrgress() -> void {
		d->status = TaskStatus::Running;
		d->progress++;

		emit Updated();
	}

	auto StatusTask::Finish() -> void {
		d->status = TaskStatus::Finished;
		
		emit Updated();
	}

	auto StatusTask::GetMessage() const -> QString {
		return d->message;
	}

	auto StatusTask::GetError() const -> QString {
		return d->error;
	}

	auto StatusTask::GetStatus() const -> TaskStatus {
		return d->status;
	}

	auto StatusTask::GetProgress() const -> int {
		return static_cast<int>(static_cast<double>(d->progress) / d->max * 100);
	}

	auto StatusTask::IsRunning() const -> bool {
		return d->status == TaskStatus::Intermediate || d->status == TaskStatus::Running;
	}

	auto StatusTask::IsFinished() const -> bool {
		return d->status == TaskStatus::Finished;
	}
}
