#include <QIcon>

#include "StatusTaskWidget.h"
#include "StatusTask.h"

#include "ui_StatusTaskWidget.h"

namespace CellAnalyzer::UI {
	struct StatusTaskWidget::Impl {
		Ui::StatusTaskWidget ui;

		StatusTaskPtr task = nullptr;
	};

	StatusTaskWidget::StatusTaskWidget(const StatusTaskPtr& task, QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);
		d->ui.ico->setVisible(false);
		d->task = task;

		OnUpdated();

		if (const auto ptr = std::dynamic_pointer_cast<StatusTask>(task))
			connect(ptr.get(), &StatusTask::Updated, this, &StatusTaskWidget::OnUpdated);
	}

	StatusTaskWidget::~StatusTaskWidget() = default;

	auto StatusTaskWidget::GetStatus() const -> TaskStatus {
		return d->task->GetStatus();
	}

	auto StatusTaskWidget::OnUpdated() -> void {
		d->ui.message->setText(d->task->GetMessage());

		switch (d->task->GetStatus()) {
			case TaskStatus::Pending:
				d->ui.ico->setVisible(true);
				d->ui.ico->setPixmap(QIcon(":/Flat/Loading.svg").pixmap(25, 25));
				d->ui.progressFrame->setVisible(false);
				d->ui.statusFrame->setVisible(true);
				d->ui.statusLabel->setText("Pending...");
				break;
			case TaskStatus::Intermediate:
				d->ui.ico->setVisible(false);
				d->ui.progressFrame->setVisible(true);
				d->ui.progressBar->setMaximum(0);
				d->ui.progressBar->setValue(0);
				d->ui.progressLabel->clear();
				d->ui.statusFrame->setVisible(false);
				break;
			case TaskStatus::Running:
				d->ui.ico->setVisible(false);
				d->ui.progressFrame->setVisible(true);
				d->ui.progressBar->setMaximum(100);
				d->ui.progressBar->setValue(d->task->GetProgress());
				d->ui.progressLabel->setText(QString("%1%").arg(d->task->GetProgress()));
				d->ui.statusFrame->setVisible(false);
				break;
			case TaskStatus::Finished:
				d->ui.ico->setVisible(true);
				d->ui.ico->setPixmap(QIcon(":/Flat/Ok.svg").pixmap(25, 25));
				d->ui.progressFrame->setVisible(false);
				d->ui.statusFrame->setVisible(true);
				d->ui.statusLabel->setText("Finished");
				break;
			case TaskStatus::Error:
				d->ui.ico->setVisible(true);
				d->ui.ico->setPixmap(QIcon(":/Flat/Warning.svg").pixmap(25, 25));
				d->ui.progressFrame->setVisible(false);
				d->ui.statusFrame->setVisible(true);
				d->ui.statusLabel->setText(d->task->GetError());
				break;
		}
	}
}
