#include <QApplication>
#include <QFile>
#include <QFontDatabase>
#include <QLabel>
#include <QScreen>
#include <QThread>
#include <QVBoxLayout>
#include <QWindow>

#include <FramelessHelper/Widgets/FramelessWidgetsHelper>
#include <FramelessHelper/Widgets/StandardTitleBar>

#include "MainWindow.h"
#include "AboutDialog.h"
#include "MenuHandler.h"
#include "MessageBox.h"
#include "StatusBarHandler.h"
#include "ToolBarHandler.h"

#include "IApplication.h"
#include "IDatabase.h"
#include "IExtLicenseManager.h"
#include "ILicenseManager.h"
#include "IProjectService.h"

#include "ClearOnProjectDisposed.h"

namespace CellAnalyzer::UI {
	struct MainWindow::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		static constexpr int TitleBarHeight = 40;
		static constexpr int DialogTitleBarHeight = 31;
		bool initialized = false;

		static auto SetStylesheet(QWidget* parent) -> void;
		static auto SetFrameless(QMainWindow* parent, MenuHandler* menu, const IApplication* app) -> void;
		static auto SetTitleBar(QDialog* parent, bool showTitle) -> void;
		static auto SetFont(QWidget* parent) -> void;
		static auto GetCenter(const QSize& size) -> QPoint;
	};

	auto MainWindow::Impl::SetStylesheet(QWidget* parent) -> void {
		if (QFile file(":/Stylesheet/CellAnalyzer.qss", parent); file.open(QIODevice::ReadOnly))
			parent->setStyleSheet(file.readAll());
	}

	auto MainWindow::Impl::SetFrameless(QMainWindow* parent, MenuHandler* menu, const IApplication* app) -> void {
		wangwenx190::FramelessHelper::FramelessHelper::Core::setApplicationOSThemeAware();

		auto* helper = wangwenx190::FramelessHelper::FramelessWidgetsHelper::get(parent);
		auto* title = new wangwenx190::FramelessHelper::StandardTitleBar(parent);
		auto* layout = dynamic_cast<QHBoxLayout*>(title->layout());
		auto* palette = title->chromePalette();
		auto* text = new QLabel(app->GetVersion().ToString());

		text->setObjectName("tag");
		helper->extendsContentIntoTitleBar();
		helper->setSystemButton(title->minimizeButton(), wangwenx190::FramelessHelper::Global::SystemButtonType::Minimize);
		helper->setSystemButton(title->maximizeButton(), wangwenx190::FramelessHelper::Global::SystemButtonType::Maximize);
		helper->setSystemButton(title->closeButton(), wangwenx190::FramelessHelper::Global::SystemButtonType::Close);
		helper->setTitleBarWidget(title);
		helper->setHitTestVisible(menu);
		layout->setContentsMargins(38, 0, 0, 0);
		layout->insertWidget(0, text);
		layout->insertWidget(0, menu);
		title->setFixedHeight(TitleBarHeight);
		title->setTitleLabelVisible(false);
		title->setWindowIconVisible(true);
		title->setWindowIconSize({ 28, 28 });
		title->minimizeButton()->setToolTip({});
		title->maximizeButton()->setToolTip({});
		title->closeButton()->setToolTip({});
		palette->setTitleBarActiveForegroundColor({ 255, 255, 255 });
		palette->setTitleBarInactiveForegroundColor({ 128, 128, 128 });
		palette->setTitleBarActiveBackgroundColor(Qt::transparent);
		palette->setTitleBarInactiveBackgroundColor(Qt::transparent);
		palette->setChromeButtonHoverColor(QColor(56, 56, 59));
		palette->setChromeButtonPressColor(QColor(36, 159, 176));
		palette->setCloseButtonHoverColor(QColor(56, 56, 59));
		parent->setMenuWidget(title);
	}

	auto MainWindow::Impl::SetTitleBar(QDialog* parent, bool showTitle) -> void {
		auto* helper = wangwenx190::FramelessHelper::FramelessWidgetsHelper::get(parent);
		auto* title = new wangwenx190::FramelessHelper::StandardTitleBar(parent);
		auto* layout = dynamic_cast<QVBoxLayout*>(parent->layout());
		auto* palette = title->chromePalette();

		helper->setTitleBarWidget(title);
		helper->setSystemButton(title->minimizeButton(), wangwenx190::FramelessHelper::Global::SystemButtonType::Minimize);
		helper->setSystemButton(title->maximizeButton(), wangwenx190::FramelessHelper::Global::SystemButtonType::Maximize);
		helper->setSystemButton(title->closeButton(), wangwenx190::FramelessHelper::Global::SystemButtonType::Close);
		layout->insertWidget(0, title);
		title->maximizeButton()->setVisible(parent->windowFlags().testFlag(Qt::WindowType::WindowMaximizeButtonHint));
		title->minimizeButton()->setVisible(parent->windowFlags().testFlag(Qt::WindowType::WindowMinimizeButtonHint));
		title->setFixedHeight(DialogTitleBarHeight);
		title->setTitleLabelVisible(showTitle && !parent->windowTitle().isEmpty());
		title->setWindowIconVisible(showTitle && !parent->windowTitle().isEmpty());
		palette->setTitleBarActiveForegroundColor({ 255, 255, 255 });
		palette->setTitleBarInactiveForegroundColor({ 128, 128, 128 });
		palette->setTitleBarActiveBackgroundColor(Qt::transparent);
		palette->setTitleBarInactiveBackgroundColor(Qt::transparent);
		palette->setChromeButtonHoverColor(QColor(56, 56, 59));
		palette->setChromeButtonPressColor(QColor(36, 159, 176));
		palette->setCloseButtonHoverColor(QColor(56, 56, 59));
	}

	auto MainWindow::Impl::SetFont(QWidget* parent) -> void {
		for (const auto& i : { "Black", "Bold", "ExtraBold", "ExtraLight", "Light", "Medium", "Regular", "SemiBold", "Thin" }) {
			if (QFile file(QString(":/Font/NotoSansKR-%1.ttf").arg(i)); file.open(QIODevice::ReadOnly))
				QFontDatabase::addApplicationFontFromData(file.readAll());
		}

		auto font = QFontDatabase().font("Noto Sans KR", "Regular", 9);
		font.setHintingPreference(QFont::PreferNoHinting);
		font.setStyleStrategy(QFont::PreferQuality);
		font.setKerning(true);

		QApplication::setFont(font);
	}

	auto MainWindow::Impl::GetCenter(const QSize& size) -> QPoint {
		const auto* screen = QGuiApplication::screenAt(QCursor::pos());
		auto geometry = screen->availableGeometry();
		geometry.setWidth(geometry.width() - size.width());
		geometry.setHeight(geometry.height() - size.height());

		return geometry.center();
	}

	MainWindow::MainWindow(Tomocube::IServiceProvider* provider) : QMainWindow(), IAppModule(), IAlertHandler(), d(new Impl) {
		d->SetStylesheet(this);
		d->provider = provider;

		setCentralWidget(d->provider->GetService<ToolBarHandler>().get());
		setStatusBar(d->provider->GetService<StatusBarHandler>().get());
		setWindowIcon(QIcon(":/System/Icon_DE.png"));
	}

	MainWindow::~MainWindow() = default;

	auto MainWindow::Start() -> std::optional<Error> {
		const auto project = d->provider->GetService<IProjectService>();
		const auto database = d->provider->GetService<IDatabase>();
		const auto license = d->provider->GetService<ILicenseManager>();

		project->AddEvent(std::make_shared<ClearOnProjectDisposed>(d->provider));

		if (!license->IsActivated()) {
			const auto about = std::make_shared<View::AboutDialog>(d->provider, this);
			about->CloseOnActivation(true);

			ShowDialog(about);
		}

		if (license->IsActivated()) {
			const auto extLms = d->provider->GetServices<IExtLicenseManager>();

			for (const auto& extLm : extLms) {
				if (extLm->IsInitialized() == false)
					extLm->Initialize(this);
			}

			showMinimized();

			d->SetFrameless(this, d->provider->GetService<MenuHandler>().get(), d->provider->GetService<IApplication>().get());
			d->SetFont(this);

			QSize wsize(1200, 800);
			auto state = Qt::WindowState::WindowNoState;

			if (const auto data = database->GetMap("GUI\\MainWindow\\Geometry")) {
				if (const auto value = IDatabase::ReadNumber(*data, "Width"))
					wsize.setWidth(*value);
				if (const auto value = IDatabase::ReadNumber(*data, "Height"))
					wsize.setHeight(*value);
				if (const auto value = IDatabase::ReadString(*data, "WindowState")) {
					if (*value == "Maximized")
						state = Qt::WindowMaximized;
				}
			}

			this->resize(wsize);
			this->move(d->GetCenter(wsize));

			if (const auto menu = d->provider->GetService<MenuHandler>(); menu->ShowStart()) {
				d->initialized = true;

				if (state == Qt::WindowMaximized) {
					hide();
					showMaximized();
				} else
					setWindowState(Qt::WindowActive);
			} else {
				IView::Run([] {
					QApplication::exit();
				});
			}

			QApplication::exec();
		}

		return std::nullopt;
	}

	auto MainWindow::Stop() -> void {
		const auto extLms = d->provider->GetServices<IExtLicenseManager>();

		for (const auto& extLm : extLms) {
			if (extLm->IsInitialized())
				extLm->Finish();
		}

		this->close();
	}

	auto MainWindow::ShowMessage(const QString& title, const QString& message, AlertIcon icon) -> void {
		IView::Run([this, title, message, icon] {
			MessageBox box(this);
			d->SetTitleBar(&box, false);

			connect(&box, &MessageBox::ButtonClicked, [&box] {
				box.accept();
			});

			box.SetTitle(title);
			box.SetMessage(message);
			box.SetIcon(icon);
			box.SetButtons({ "Ok" });
			box.SetAccentButton("Ok");
			box.exec();
		}, RunType::Blocking);
	}

	auto MainWindow::ShowYesNo(const QString& title, const QString& message, AlertIcon icon) -> bool {
		auto result = false;

		IView::Run([this, &result, title, message, icon] {
			MessageBox box(this);
			d->SetTitleBar(&box, false);

			connect(&box, &MessageBox::ButtonClicked, [&box, &result](const QString& button) {
				result = button == "Yes";
				box.accept();
			});

			box.SetTitle(title);
			box.SetMessage(message);
			box.SetIcon(icon);
			box.SetButtons({ "Yes", "No" });
			box.SetAccentButton("Yes");
			box.exec();
		}, RunType::Blocking);

		return result;
	}

	auto MainWindow::ShowDialog(const ViewPtr& view) -> int {
		if (const auto dialog = std::dynamic_pointer_cast<QDialog>(view)) {
			QDialog temp(nullptr);
			QVBoxLayout layout;
			temp.setWindowFlags(dialog->windowFlags());
			temp.setWindowTitle(dialog->windowTitle());
			temp.setWindowIcon(dialog->windowIcon());
			temp.setLayout(&layout);
			layout.setContentsMargins(0, 0, 0, 0);
			layout.addWidget(dialog.get());

			d->SetStylesheet(&temp);
			d->SetTitleBar(&temp, true);

			auto max = dialog->maximumSize();
			auto min = dialog->minimumSize();
			auto size = dialog->size();

			if (const auto* helper = wangwenx190::FramelessHelper::FramelessWidgetsHelper::get(&temp)) {
				if (const auto* title = helper->titleBarWidget()) {
					auto padding = title->height();

					if (const auto* l = dialog->layout())
						padding += l->spacing();

					max.setHeight(std::min(1677215, max.height() + padding));
					min.setHeight(std::min(1677215, min.height() + padding));
					size.setHeight(std::min(1677215, size.height() + padding));
				}
			}

			temp.setMaximumSize(max);
			temp.setMinimumSize(min);
			temp.resize(size);
			temp.installEventFilter(this);

			connect(dialog.get(), &QDialog::accepted, this, [&temp] {
				temp.accept();
			});

			connect(dialog.get(), &QDialog::rejected, this, [&temp] {
				temp.reject();
			});

			connect(dialog.get(), &QDialog::finished, this, [&temp](int value) {
				temp.finished(value);
			});

			const auto result = temp.exec();
			dialog->setParent(nullptr);
			return result;
		}

		return 0;
	}

	auto MainWindow::showEvent(QShowEvent* event) -> void {
		QMainWindow::showEvent(event);

		const auto* helper = wangwenx190::FramelessHelper::FramelessWidgetsHelper::get(this);

		if (auto* title = helper->titleBarWidget()) {
			title->setFixedHeight(title->height() + 1);

			IView::Run([title] {
				title->setFixedHeight(Impl::TitleBarHeight);
			});
		}
	}

	auto MainWindow::closeEvent(QCloseEvent* event) -> void {
		QMainWindow::closeEvent(event);

		if (const auto database = d->provider->GetService<IDatabase>(); d->initialized && database) {
			const auto size = normalGeometry().size();
			QVariantMap map;
			map["Width"] = size.width();
			map["Height"] = size.height();

			if (const auto state = windowState(); state.testFlag(Qt::WindowMaximized))
				map["WindowState"] = "Maximized";
			else
				map["WindowState"] = "NoState";

			database->Save("GUI\\MainWindow\\Geometry", map);
			database->Flush();
		}

		QApplication::exit();
	}

	auto MainWindow::changeEvent(QEvent* event) -> void {
		if (event->type() == QEvent::WindowStateChange) {
			if (const auto* title = dynamic_cast<wangwenx190::FramelessHelper::StandardTitleBar*>(menuWidget())) {
				title->minimizeButton()->setToolTip({});
				title->maximizeButton()->setToolTip({});
				title->closeButton()->setToolTip({});
			}
		}
	}

	auto MainWindow::eventFilter(QObject* watched, QEvent* event) -> bool {
		if (event->type() == QEvent::Show) {
			if (auto* dialog = dynamic_cast<QDialog*>(watched)) {
				const auto* helper = wangwenx190::FramelessHelper::FramelessWidgetsHelper::get(dialog);

				if (auto* title = helper->titleBarWidget()) {
					title->setFixedHeight(title->height() + 1);

					IView::Run([title] {
						title->setFixedHeight(Impl::DialogTitleBarHeight);
					});
				}
			}
		}

		return QMainWindow::eventFilter(watched, event);
	}
}
