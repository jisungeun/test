#include "StatusGroup.h"
#include "StatusTask.h"

#include "IView.h"

namespace CellAnalyzer::UI {
	struct StatusGroup::Impl {
		QString name;
		StatusTaskList taskList;
	};

	StatusGroup::StatusGroup(const QString& name) : QObject(), IStatusGroup(), d(new Impl) {
		d->name = name;
	}

	StatusGroup::~StatusGroup() = default;

	auto StatusGroup::SetName(const QString& name) -> void {
		d->name = name;
	}

	auto StatusGroup::AddTask(const QString& status) -> StatusTaskPtr {
		std::shared_ptr<StatusTask> task;

		IView::Run([this, &task, &status] {
			task = std::make_shared<StatusTask>(status);
			d->taskList.push_back(task);

			connect(task.get(), &StatusTask::Updated, this, &StatusGroup::Updated);
			emit Updated();
		}, this, Qt::BlockingQueuedConnection);

		return task;
	}

	auto StatusGroup::GetName() const -> QString {
		return d->name;
	}

	auto StatusGroup::GetTask(int index) const -> StatusTaskPtr {
		return d->taskList[index];
	}

	auto StatusGroup::GetTaskList() const -> StatusTaskList {
		return d->taskList;
	}

	auto StatusGroup::GetTaskCount() const -> int {
		return d->taskList.count();
	}

	auto StatusGroup::GetTaskDoneCount() const -> int {
		auto count = 0;

		for (const auto& t : d->taskList) {
			switch (t->GetStatus()) {
				case TaskStatus::Finished:
				case TaskStatus::Error:
					count++;
					break;
			}
		}

		return count;
	}

	auto StatusGroup::GetPercentage() const -> int {
		auto sum = 0;

		for (const auto& t : d->taskList) {
			switch (t->GetStatus()) {
				case TaskStatus::Running:
					sum += t->GetProgress();
					break;
				case TaskStatus::Finished:
				case TaskStatus::Error:
					sum += 100;
					break;
			}
		}

		return static_cast<int>(static_cast<double>(sum) / (d->taskList.count() * 100) * 100);
	}
}
