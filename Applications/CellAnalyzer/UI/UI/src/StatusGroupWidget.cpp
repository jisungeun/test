#include "StatusGroupWidget.h"
#include "StatusGroup.h"
#include "StatusTaskWidget.h"

#include "ui_StatusGroupWidget.h"

namespace CellAnalyzer::UI {
	struct StatusGroupWidget::Impl {
		Ui::StatusGroupWidget ui;
		StatusGroupPtr group = nullptr;

		QMap<StatusTaskPtr, StatusTaskWidget*> taskMap;
	};

	StatusGroupWidget::StatusGroupWidget(const StatusGroupPtr& group, QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);
		d->group = group;
		d->ui.label->setText(group->GetName());

		OnUpdated();

		if (const auto ptr = std::dynamic_pointer_cast<StatusGroup>(group))
			connect(ptr.get(), &StatusGroup::Updated, this, &StatusGroupWidget::OnUpdated);
	}

	StatusGroupWidget::~StatusGroupWidget() = default;

	auto StatusGroupWidget::OnUpdated() -> void {
		d->ui.progressBar->setValue(d->group->GetPercentage());
		d->ui.curLabel->setText(QString::number(d->group->GetTaskDoneCount()));
		d->ui.maxLabel->setText(QString::number(d->group->GetTaskCount()));
		d->ui.label->setText(d->group->GetName());

		for (const auto& t : d->group->GetTaskList()) {
			if (!d->taskMap.contains(t)) {
				if (d->group->GetTaskCount() < 6 || t->IsRunning()) {
					d->taskMap[t] = new StatusTaskWidget(t, this);
					d->ui.frameLayout->addWidget(d->taskMap[t]);
				}
			}
		}
	}
}
