#include <QMenu>
#include <QThread>
#include <QWidgetAction>

#include "IView.h"

#include "StatusTaskContainer.h"
#include "StatusGroup.h"
#include "StatusGroupWidget.h"

#include "ui_StatusTaskContainer.h"

namespace CellAnalyzer::UI {
	struct StatusTaskContainer::Impl {
		Ui::StatusTaskContainer ui;
		StatusGroupList list;
	};

	StatusTaskContainer::StatusTaskContainer(QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);
		d->ui.detail->setObjectName("quiet_color");
		d->ui.frame->setVisible(false);

		connect(d->ui.detail, &QPushButton::clicked, this, &StatusTaskContainer::OnDetailBtnClicked);
	}

	StatusTaskContainer::~StatusTaskContainer() = default;

	auto StatusTaskContainer::AddGroup(const QString& name) -> StatusGroupPtr {
		std::shared_ptr<StatusGroup> group;

		IView::Run([this, &group, &name] {
			group = std::make_shared<StatusGroup>(name);
			d->list.push_back(group);
			OnUpdated();

			connect(group.get(), &StatusGroup::Updated, this, &StatusTaskContainer::OnUpdated);
		}, this, Qt::BlockingQueuedConnection);

		return group;
	}

	auto StatusTaskContainer::RemoveGroup(const StatusGroupPtr& group) -> void {
		IView::Run([this, group] {
			if (d->list.removeOne(group))
				OnUpdated();
		});
	}

	auto StatusTaskContainer::GetGroupList() const -> StatusGroupList {
		return d->list;
	}

	auto StatusTaskContainer::OnDetailBtnClicked() -> void {
		QMenu menu(this);

		if (d->list.count() > 0) {
			for (const auto& i : d->list) {
				auto* action = new QWidgetAction(&menu);
				action->setDefaultWidget(new StatusGroupWidget(i, &menu));
				action->setEnabled(false);
				menu.addAction(action);
				menu.addSeparator();
			}
		} else {
			auto* action = new QWidgetAction(&menu);
			auto* frame = new QFrame(&menu);
			auto* layout = new QHBoxLayout(frame);
			auto* ico = new QLabel(frame);
			auto* message = new QLabel(frame);
			ico->setFixedSize(32, 32);
			ico->setPixmap(QIcon(":/Flat/Ok.svg").pixmap(32, 32));
			message->setText("Nothing is running now.");
			layout->addWidget(ico);
			layout->addWidget(message);
			frame->setLayout(layout);
			action->setDefaultWidget(frame);
			menu.addAction(action);
		}

		auto pos = mapToGlobal(d->ui.detail->geometry().topLeft());
		pos.setY(pos.y() - menu.sizeHint().height() - 15);

		menu.exec(pos);
	}

	auto StatusTaskContainer::OnUpdated() -> void {
		auto count = 0;
		auto error = false;
		QString status;

		for (const auto& g : d->list) {
			for (const auto& t : g->GetTaskList()) {
				switch (t->GetStatus()) {
					case TaskStatus::Pending:
					case TaskStatus::Intermediate:
					case TaskStatus::Running:
						count++;
						status = t->GetMessage();
						break;
					case TaskStatus::Error:
						error = true;
						break;
				}
			}
		}

		if (count == 0)
			d->ui.label->clear();
		else if (count == 1)
			d->ui.label->setText(QString("'%1' task is running...").arg(status));
		else
			d->ui.label->setText(QString("%1 tasks are remaining...").arg(count));

		if (error)
			emit StateChanged(StatusColor::Error);
		else
			emit StateChanged(!d->list.isEmpty() ? StatusColor::Accent : StatusColor::Normal);

		d->ui.frame->setVisible(!d->list.isEmpty());
	}
}
