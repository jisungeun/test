#include <QLabel>

#include "StatusBarHandler.h"

#include "StatusTaskContainer.h"

namespace CellAnalyzer::UI {
	struct StatusBarHandler::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		StatusTaskContainer* container = nullptr;
	};

	StatusBarHandler::StatusBarHandler(Tomocube::IServiceProvider* provider) : QStatusBar(), IStatusBarHandler(), d(new Impl) {
		d->provider = provider;
		d->container = new StatusTaskContainer(this);

		addWidget(d->container);
		connect(d->container, &StatusTaskContainer::StateChanged, this, &StatusBarHandler::OnStateChanged);
	}

	StatusBarHandler::~StatusBarHandler() = default;

	auto StatusBarHandler::AddGroup(const QString& name) -> StatusGroupPtr {
		return d->container->AddGroup(name);
	}

	auto StatusBarHandler::RemoveGroup(const StatusGroupPtr& group) -> void {
		return d->container->RemoveGroup(group);
	}

	auto StatusBarHandler::GetGroupList() const -> StatusGroupList {
		return d->container->GetGroupList();
	}

	auto StatusBarHandler::OnStateChanged(StatusColor color) -> void {
		QString code;

		if (GetGroupList().count() > 0) {
			switch (color) {
				case StatusColor::Normal:
					code = "#1D1D1F";
					break;
				case StatusColor::Accent:
					code = "#1F8896";
					break;
				case StatusColor::Error:
					code = "#FF5500";
					break;
			}
		} else
			code = "#1D1D1F";

		setStyleSheet(QString("QStatusBar { background-color: %1; }").arg(code));
	}
}
