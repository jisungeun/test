#include <QIcon>
#include <QPushButton>
#include <QScrollBar>

#include <FramelessHelper/Widgets/FramelessWidgetsHelper>

#include "MessageBox.h"

#include "ui_MessageBox.h"

namespace CellAnalyzer::UI {
	struct MessageBox::Impl {
		Ui::MessageBox ui;
	};

	MessageBox::MessageBox(QWidget* parent) : QDialog(parent), d(new Impl) {
		wangwenx190::FramelessHelper::FramelessWidgetsHelper::get(this)->extendsContentIntoTitleBar();

		d->ui.setupUi(this);
		d->ui.icoFrame->setVisible(false);
		d->ui.messageLabel->document()->setDocumentMargin(0);
	}

	MessageBox::~MessageBox() = default;

	auto MessageBox::SetTitle(const QString& title) -> void {
		setWindowTitle(title);
		d->ui.titleLabel->setText(title);
	}

	auto MessageBox::SetMessage(const QString& message) -> void {
		d->ui.messageLabel->setPlainText(message);
		const auto metrics = d->ui.messageLabel->fontMetrics();
		const auto width = metrics.horizontalAdvance(message);
		const auto splits = message.split('\n');
		auto lines = 0;

		for (const auto& s : splits)
			lines += std::max(1, metrics.horizontalAdvance(s) / std::min(width, 580));

		resize(width + 120, lines * metrics.height() + 185);
	}

	auto MessageBox::SetIcon(AlertIcon icon) -> void {
		switch (icon) {
			case AlertIcon::None:
				d->ui.icoFrame->setVisible(false);
				break;
			case AlertIcon::Critical:
				d->ui.icoFrame->setVisible(true);
				d->ui.ico->setPixmap(QIcon(":/Flat/Warning.svg").pixmap(60, 60));
				break;
			case AlertIcon::Error:
				d->ui.icoFrame->setVisible(true);
				d->ui.ico->setPixmap(QIcon(":/Flat/Warning.svg").pixmap(60, 60));
				break;
			case AlertIcon::Information:
				d->ui.icoFrame->setVisible(true);
				d->ui.ico->setPixmap(QIcon(":/Flat/Warning.svg").pixmap(60, 60));
				break;
		}
	}

	auto MessageBox::SetButtons(const QStringList& buttons) -> void {
		qDeleteAll(d->ui.actionFrame->findChildren<QPushButton*>());

		for (const auto& b : buttons) {
			auto* button = new QPushButton(b, this);
			d->ui.actionLayout->addWidget(button);

			connect(button, &QPushButton::clicked, this, &MessageBox::OnButtonClicked);
		}
	}

	auto MessageBox::SetAccentButton(const QString& accent) -> void {
		for (auto* btn : d->ui.actionFrame->findChildren<QPushButton*>()) {
			if (btn->text() == accent)
				btn->setObjectName("accent_color");
		}
	}

	auto MessageBox::OnButtonClicked() -> void {
		if (const auto* button = dynamic_cast<QPushButton*>(sender()))
			emit ButtonClicked(button->text());
	}
}
