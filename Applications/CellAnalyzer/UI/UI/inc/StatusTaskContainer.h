#pragma once

#include "QWidget"

#include "IStatusGroup.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	enum class StatusColor {
		Normal,
		Accent,
		Error
	};

	class CellAnalyzer_UI_API StatusTaskContainer final : public QWidget {
		Q_OBJECT

	public:
		explicit StatusTaskContainer(QWidget* parent = nullptr);
		~StatusTaskContainer() override;

		auto AddGroup(const QString& name) -> StatusGroupPtr;
		auto RemoveGroup(const StatusGroupPtr& group) -> void;
		auto GetGroupList() const -> StatusGroupList;

	protected slots:
		auto OnDetailBtnClicked() -> void;
		auto OnUpdated() -> void;

	signals:
		auto StateChanged(StatusColor color) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
