#pragma once

#include <QDateTime>

#include "IDatabase.h"
#include "IServiceProvider.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	struct RecentProject {
		QString name;
		QString url;
		QDateTime datetime;

		RecentProject(const QString& name, const QString& url, const QDateTime& datetime) : name(name), url(url), datetime(datetime) {}
	};

	using RecentProjectPtr = std::shared_ptr<RecentProject>;

	class CellAnalyzer_UI_API RecentProjectList {
	public:
		explicit RecentProjectList(Tomocube::IServiceProvider* provider);
		~RecentProjectList();

		auto GetList() const -> QList<RecentProjectPtr>;
		auto Add(const QString& name, const QString& url) -> void;
		auto Remove(const QString& name, const QString& url) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
