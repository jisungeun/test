#pragma once

#include <QDialog>

#include "IAlertHandler.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	class CellAnalyzer_UI_API MessageBox final : public QDialog {
		Q_OBJECT

	public:
		explicit MessageBox(QWidget* parent);
		~MessageBox() override;

		auto SetTitle(const QString& title) -> void;
		auto SetMessage(const QString& message) -> void;
		auto SetIcon(AlertIcon icon) -> void;
		auto SetButtons(const QStringList& buttons) -> void;
		auto SetAccentButton(const QString& accent) -> void;

	protected:
		auto OnButtonClicked() -> void;

	signals:
		auto ButtonClicked(const QString& button) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
