#pragma once

#include <QObject>

#include "IStatusTask.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	class CellAnalyzer_UI_API StatusTask final : public QObject, public IStatusTask {
		Q_OBJECT

	public:
		explicit StatusTask(const QString& status);
		~StatusTask() override;

		auto SetMessage(const QString& message) -> void override;
		auto SetError(const QString& message) -> void override;

		auto SetProgress(int value) -> void override;
		auto SetProgress(int value, int max) -> void override;
		auto AddProrgress() -> void override;

		auto SetIntermediate() -> void override;
		auto SetPending() -> void override;
		auto SetRunning() -> void override;
		auto Finish() -> void override;

		auto GetMessage() const -> QString override;
		auto GetError() const -> QString override;
		auto GetStatus() const -> TaskStatus override;
		auto GetProgress() const -> int override;
		auto IsRunning() const -> bool override;
		auto IsFinished() const -> bool override;

	signals:
		auto Updated() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
