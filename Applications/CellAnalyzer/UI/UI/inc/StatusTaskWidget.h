#pragma once

#include <QWidget>

#include "IStatusTask.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	class CellAnalyzer_UI_API StatusTaskWidget final : public QWidget {
	public:
		explicit StatusTaskWidget(const StatusTaskPtr& task, QWidget* parent = nullptr);
		~StatusTaskWidget() override;

		auto GetStatus() const -> TaskStatus;

	protected slots:
		auto OnUpdated() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
