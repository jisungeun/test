#pragma once

#include <QMainWindow>

#include "IServiceProvider.h"
#include "IToolBarHandler.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	class CellAnalyzer_UI_API ToolBarHandler final : public QMainWindow, public IToolBarHandler {
	public:
		explicit ToolBarHandler(Tomocube::IServiceProvider* provider);
		~ToolBarHandler() override;

		auto Contains(const ViewPtr& view) const -> bool override;
		auto Show(const ViewPtr& view, ToolBarPosition position) -> void override;
		auto Close(const ViewPtr& view) -> void override;

		auto IsVisible(const ViewPtr& view) const -> bool override;
		auto SetVisible(const ViewPtr& view, bool visible) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
