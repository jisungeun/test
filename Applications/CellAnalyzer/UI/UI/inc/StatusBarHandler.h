#pragma once

#include <QStatusBar>

#include "IServiceProvider.h"
#include "IStatusBarHandler.h"
#include "StatusTaskContainer.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	class CellAnalyzer_UI_API StatusBarHandler final : public QStatusBar, public IStatusBarHandler {
	public:
		explicit StatusBarHandler(Tomocube::IServiceProvider* provider);
		~StatusBarHandler() override;

		auto AddGroup(const QString& name) -> StatusGroupPtr override;
		auto RemoveGroup(const StatusGroupPtr& group) -> void override;
		auto GetGroupList() const -> StatusGroupList override;

	protected slots:
		auto OnStateChanged(StatusColor color) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
