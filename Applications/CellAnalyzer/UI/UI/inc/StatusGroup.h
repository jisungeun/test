#pragma once

#include <QObject>

#include "IStatusGroup.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	class CellAnalyzer_UI_API StatusGroup final : public QObject, public IStatusGroup {
		Q_OBJECT

	public:
		explicit StatusGroup(const QString& name);
		~StatusGroup() override;

		auto SetName(const QString& name) -> void override;
		auto AddTask(const QString& status = QString()) -> StatusTaskPtr override;

		auto GetName() const -> QString override;
		auto GetTask(int index) const -> StatusTaskPtr override;
		auto GetTaskList() const -> StatusTaskList override;
		auto GetTaskCount() const -> int override;
		auto GetTaskDoneCount() const -> int override;
		auto GetPercentage() const -> int override;

	signals:
		auto Updated() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
