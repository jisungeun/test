#pragma once

#include <QMenuBar>

#include "IMenuHandler.h"
#include "IServiceProvider.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	class CellAnalyzer_UI_API MenuHandler final : public QMenuBar, public IMenuHandler {
	public:
		explicit MenuHandler(Tomocube::IServiceProvider* provider);
		~MenuHandler() override;

		auto ShowStart() -> bool;

		auto ContainsToolBar(const ViewPtr& view) const -> bool override;
		auto ContainsWindow(const ViewPtr& view) const -> bool override;
		auto ContainsScreen(const ViewPtr& view) const -> bool override;
		auto ContainsMenu(const MenuPtr& menu) const -> bool override;

		auto AddToolBar(const ViewPtr& view) -> void override;
		auto AddWindow(const ViewPtr& view) -> void override;
		auto AddScreen(const ViewPtr& view) -> void override;
		auto AddMenu(const MenuPtr& menu) -> void override;

		auto RemoveToolBar(const ViewPtr& view) -> void override;
		auto RemoveWindow(const ViewPtr& view) -> void override;
		auto RemoveScreen(const ViewPtr& view) -> void override;
		auto RemoveMenu(const MenuPtr& menu) -> void override;

		auto ClearToolBars() -> void override;
		auto ClearWindows() -> void override;
		auto ClearScreens() -> void override;
		auto ClearMenu() -> void override;

	protected slots:
		auto OnStartTriggered() -> void;
		auto OnCloseTriggered() -> void;
		auto OnExitTriggered() -> void;
		auto OnAboutTriggered() -> void;

	protected:
		auto StartProject(const QString& project) -> bool;
		auto StartHistory(const QString& project, const QString& url) -> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
