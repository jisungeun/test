#pragma once

#include <QMainWindow>

#include "IAlertHandler.h"
#include "IAppModule.h"
#include "IServiceProvider.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	class CellAnalyzer_UI_API MainWindow final : public QMainWindow, public IAppModule, public IAlertHandler {
	public:
		explicit MainWindow(Tomocube::IServiceProvider* provider);
		~MainWindow() override;

		auto Start() -> std::optional<Error> override;
		auto Stop() -> void override;

		auto ShowMessage(const QString& title, const QString& message, AlertIcon icon) -> void override;
		auto ShowYesNo(const QString& title, const QString& message, AlertIcon icon) -> bool override;
		auto ShowDialog(const ViewPtr& view) -> int override;

	protected:
		auto showEvent(QShowEvent* event) -> void override;
		auto closeEvent(QCloseEvent* event) -> void override;
		auto changeEvent(QEvent* event) -> void override;
		auto eventFilter(QObject* watched, QEvent* event) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
