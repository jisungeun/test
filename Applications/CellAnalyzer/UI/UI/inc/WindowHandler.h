#pragma once

#include <QMainWindow>

#include "IServiceProvider.h"
#include "IWindowHandler.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	class CellAnalyzer_UI_API WindowHandler final : public QMainWindow, public IWindowHandler {
	public:
		explicit WindowHandler(Tomocube::IServiceProvider* provider);
		~WindowHandler() override;

		auto AddEvent(const WindowEventPtr& event) -> void override;
		auto RemoveEvent(const WindowEventPtr& event) -> void override;

		auto GetViewList() const -> ViewList override;
		auto Contains(const ViewPtr& view) const -> bool override;

		auto Show(const ViewPtr& view, WindowPosition position, double ratio) -> void override;
		auto ShowFloating(const ViewPtr& view) -> void override;
		auto ShowTabbed(const ViewPtr& view, const ViewPtr& target) -> void override;
		auto ShowSplit(const ViewPtr& view, const ViewPtr& target, WindowPosition position, double ratio) -> void override;

		auto Close(const ViewPtr& view) -> void override;
		auto CloseAll() -> void override;

		auto IsVisible(const ViewPtr& view) const -> bool override;
		auto IsClosable(const ViewPtr& view) const -> bool override;

		auto SetFocus(const ViewPtr& view) -> void override;
		auto SetVisible(const ViewPtr& view, bool visible) -> void override;
		auto SetClosable(const ViewPtr& view, bool closable) -> void override;

		auto CreateDock(const ViewPtr& view) const -> std::shared_ptr<ads::CDockWidget>;
		auto ResizeDock(const std::shared_ptr<ads::CDockWidget>& dock, WindowPosition position, double ratio) -> void;

	protected slots:
		auto OnCloseRequested() const -> void;
		auto OnVisibilityChanged(bool visibility) -> void;
		auto OnFloatingWidgetCreated(ads::CFloatingDockContainer* container) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
