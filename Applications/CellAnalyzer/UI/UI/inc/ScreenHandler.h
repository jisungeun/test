#pragma once

#include <QMainWindow>

#include "DockWidget.h"
#include "IScreenHandler.h"
#include "IServiceProvider.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	class CellAnalyzer_UI_API ScreenHandler final : public QMainWindow, public IScreenHandler {
	public:
		explicit ScreenHandler(Tomocube::IServiceProvider* provider);
		~ScreenHandler() override;

		auto AddEvent(const ScreenEventPtr& event) -> void override;
		auto RemoveEvent(const ScreenEventPtr& event) -> void override;

		auto Contains(const ViewPtr& view) const -> bool override;
		auto GetViewPeeked() const -> ViewPtr override;
		auto GetViewFocused() const -> ViewPtr override;
		auto GetViewList() const -> ViewList override;

		auto Show(const ViewPtr& view) -> void override;
		auto Show(const ViewPtr& view, ScreenPosition position, double ratio) -> void override;
		auto ShowFloating(const ViewPtr& view) -> void override;

		auto Peek(const ViewPtr& view) -> void override;
		auto Close(const ViewPtr& view) -> void override;
		auto CloseAll() -> void override;

		auto IsClosable(const ViewPtr& view) const -> bool override;
		auto IsVisible(const ViewPtr& view) const -> bool override;

		auto SetFocus(const ViewPtr& view) -> void override;
		auto SetClosable(const ViewPtr& view, bool closable) -> void override;
		auto SetVisible(const ViewPtr& view, bool visible) -> void override;

		auto CreateDock(const ViewPtr& view) const -> std::shared_ptr<ads::CDockWidget>;
		auto ResizeDock(const std::shared_ptr<ads::CDockWidget>& dock, double ratio) -> void;

	protected slots:
		auto OnFocusChanged(const ads::CDockWidget* old, const ads::CDockWidget* now) -> void;
		auto OnCloseRequested() const -> void;
		auto OnVisibilityChanged(bool visibility) -> void;
		auto OnFloatingWidgetCreated(ads::CFloatingDockContainer* container) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
