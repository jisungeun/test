#pragma once

#include <QWidget>

#include "IStatusGroup.h"

#include "CellAnalyzer.UIExport.h"

namespace CellAnalyzer::UI {
	class CellAnalyzer_UI_API StatusGroupWidget final : public QWidget {
	public:
		explicit StatusGroupWidget(const StatusGroupPtr& group, QWidget* parent = nullptr);
		~StatusGroupWidget() override;

	protected slots:
		auto OnUpdated() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
