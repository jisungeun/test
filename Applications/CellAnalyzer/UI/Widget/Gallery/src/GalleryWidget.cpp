#include <QHBoxLayout>
#include <QPainter>
#include <QScrollArea>
#include <QScrollBar>

#include "GalleryWidget.h"
#include "GalleryPainter.h"

namespace CellAnalyzer::UI::Widget {
	struct GalleryWidget::Impl {
		QScrollArea* scrollArea = nullptr;
		QScrollBar* scrollBar = nullptr;
		QHBoxLayout* layout = nullptr;
		GalleryPainter* painter = nullptr;

		auto UpdateVisibleRange(const GalleryWidget* parent) const -> void;
	};

	auto GalleryWidget::Impl::UpdateVisibleRange(const GalleryWidget* parent) const -> void {
		QRect geometry;
		geometry.setWidth(parent->width());
		geometry.setTop(scrollBar->value());
		geometry.setBottom(scrollBar->value() + scrollArea->height());

		const auto from = std::max(0, painter->GetImageIndexNoSpace(geometry.topLeft()));
		const auto to = std::min(painter->GetImageIndexNoSpace(geometry.bottomRight()) + 1, painter->GetImageCount());

		if (const auto range = painter->GetVisibleRange(); from != range.x() || to != range.y()) {
			painter->SetVisibleRange(from, to);
			const_cast<GalleryWidget*>(parent)->OnVisibleRangeChanged(from, to);
		}
	}

	GalleryWidget::GalleryWidget(QWidget* parent) : QWidget(parent), d(new Impl) {
		d->layout = new QHBoxLayout(this);
		d->scrollArea = new QScrollArea(this);
		d->painter = new GalleryPainter(this);
		d->scrollBar = d->scrollArea->verticalScrollBar();

		d->scrollArea->setContentsMargins(0, 0, 0, 0);
		d->scrollArea->setFrameShape(QFrame::Shape::NoFrame);
		d->scrollArea->setWidgetResizable(true);
		d->scrollArea->setWidget(d->painter);
		d->scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

		d->layout->setContentsMargins(0, 0, 0, 0);
		d->layout->addWidget(d->scrollArea);

		setLayout(d->layout);

		connect(d->scrollBar, &QScrollBar::valueChanged, this, &GalleryWidget::OnScrolled);
	}

	GalleryWidget::~GalleryWidget() = default;

	auto GalleryWidget::ScrollTo(int index) -> void {
		if (const auto pos = d->painter->GetImagePoint(index).y(); d->scrollBar->value() > pos)
			d->scrollBar->setValue(pos);
		else if (d->scrollBar->value() + d->scrollBar->height() - d->painter->GetImageSize() < pos)
			d->scrollBar->setValue(pos - d->scrollBar->height() + d->painter->GetImageSize());
	}

	auto GalleryWidget::SetColumnCount(int count) -> void {
		d->painter->SetColumnCount(count);
		d->UpdateVisibleRange(this);
	}

	auto GalleryWidget::SetImageCount(int count) -> void {
		d->painter->SetImageCount(count);
		d->UpdateVisibleRange(this);
	}

	auto GalleryWidget::SetLayerCount(int count) -> void {
		d->painter->SetLayerCount(count);
	}

	auto GalleryWidget::GetImageCount() const -> int {
		return d->painter->GetImageCount();
	}

	auto GalleryWidget::GetLayerCount() const -> int {
		return d->painter->GetLayerCount();
	}

	auto GalleryWidget::GetImageSize() const -> int {
		return d->painter->GetImageSize();
	}

	auto GalleryWidget::GetImageIndex(const QPoint& point) const -> int {
		return d->painter->GetImageIndex(point);
	}

	auto GalleryWidget::GetImagePoint(int index) const -> QPoint {
		return d->painter->GetImagePoint(index);
	}

	auto GalleryWidget::GetVisibleRange() const -> QPoint {
		return d->painter->GetVisibleRange();
	}

	auto GalleryWidget::OnMousePressed(int index, bool pressed, Qt::MouseButton button) -> void {}

	auto GalleryWidget::OnMouseHovered(int index, bool hovered) -> void {}

	auto GalleryWidget::OnMouseClicked(int index, Qt::MouseButton button) -> void {}

	auto GalleryWidget::OnMouseDoubleClicked(int index, Qt::MouseButton button) -> void {}

	auto GalleryWidget::OnImageRequested(int index) const -> QPixmap {
		return {};
	}

	auto GalleryWidget::OnLayerRequested(int index, int depth) const -> QPixmap {
		return {};
	}

	auto GalleryWidget::OnVisibleRangeChanged(int from, int to) -> void {}

	auto GalleryWidget::OnImageSizeChanged(int size) -> void {}

	auto GalleryWidget::OnImageExists(int index) const -> bool {
		return false;
	}

	auto GalleryWidget::GetColumnCount() const -> int {
		return d->painter->GetColumnCount();
	}

	auto GalleryWidget::resizeEvent(QResizeEvent* event) -> void {
		QWidget::resizeEvent(event);
		d->painter->UpdateImageSize();
		d->UpdateVisibleRange(this);
	}

	auto GalleryWidget::OnScrolled(int value) -> void {
		d->UpdateVisibleRange(this);
	}
}
