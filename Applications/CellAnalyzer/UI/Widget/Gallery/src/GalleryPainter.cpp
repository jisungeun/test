#include <QMouseEvent>
#include <QPainter>
#include <QVariantAnimation>

#include "GalleryPainter.h"
#include "GalleryWidget.h"

namespace CellAnalyzer::UI::Widget {
	struct GalleryPainter::Impl {
		GalleryWidget* parent = nullptr;
		QVariantAnimation anime;

		int imageCount = 0;
		int layerCount = 0;

		int column = 5;
		int size = 0;
		int space = 3;
		QPoint range;

		QMap<Qt::MouseButton, int> pressed;
		int hovered = -1;
	};

	GalleryPainter::GalleryPainter(QWidget* parent) : QWidget(parent), d(new Impl) {
		d->parent = dynamic_cast<GalleryWidget*>(parent);
		d->anime.setDuration(1500);
		d->anime.setStartValue(0.0);
		d->anime.setEndValue(1.0);
		d->anime.setEasingCurve(QEasingCurve::OutQuart);
		d->anime.setLoopCount(-1);

		setMouseTracking(true);
		setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

		connect(&d->anime, &QVariantAnimation::valueChanged, this, &GalleryPainter::OnAnimationValueChanged);
	}

	GalleryPainter::~GalleryPainter() = default;

	auto GalleryPainter::SetColumnCount(int count) -> void {
		d->column = count;

		UpdateImageSize();
		updateGeometry();
	}

	auto GalleryPainter::SetImageCount(int count) -> void {
		d->imageCount = count;

		UpdateImageSize();
		updateGeometry();
	}

	auto GalleryPainter::SetLayerCount(int count) -> void {
		d->layerCount = count;

		update();
		updateGeometry();
	}

	auto GalleryPainter::SetVisibleRange(int from, int to) -> void {
		d->range = { from, to };

		update();
		updateGeometry();
	}

	auto GalleryPainter::UpdateImageSize() -> void {
		if (const auto newSize = (d->parent->width() + d->space) / d->column - d->space; d->size != newSize) {
			d->size = newSize;
			d->parent->OnImageSizeChanged(d->size);
			update();
		}

	}

	auto GalleryPainter::GetColumnCount() const -> int {
		return d->column;
	}

	auto GalleryPainter::GetImageCount() const -> int {
		return d->imageCount;
	}

	auto GalleryPainter::GetLayerCount() const -> int {
		return d->layerCount;
	}

	auto GalleryPainter::GetImageSize() const -> int {
		return d->size;
	}

	auto GalleryPainter::GetImagePoint(int index) const -> QPoint {
		if (index > -1) {
			const auto x = (index % d->column) * (d->size + d->space);
			const auto y = (index / d->column) * (d->size + d->space);

			return { x, y };
		}

		return {};
	}

	auto GalleryPainter::GetImageIndex(const QPoint& point) const -> int {
		if (point.x() % (d->size + d->space) < d->size && point.y() % (d->size + d->space) < d->size) {
			const auto x = point.x() / (d->size + d->space);
			const auto y = point.y() / (d->size + d->space);

			return y * d->column + std::min(x, d->column - 1);
		}

		return -1;
	}

	auto GalleryPainter::GetImageIndexNoSpace(const QPoint& point) const -> int {
		const auto x = point.x() / (d->size + d->space);
		const auto y = point.y() / (d->size + d->space);

		return y * d->column + std::min(x, d->column - 1);
	}

	auto GalleryPainter::GetVisibleRange() const -> QPoint {
		return d->range;
	}

	auto GalleryPainter::mousePressEvent(QMouseEvent* event) -> void {
		QWidget::mousePressEvent(event);

		if (const auto idx = GetImageIndex(event->pos()); idx > -1 && idx < d->imageCount) {
			d->pressed[event->button()] = idx;
			d->parent->OnMousePressed(idx, true, event->button());
		}
	}

	auto GalleryPainter::mouseReleaseEvent(QMouseEvent* event) -> void {
		QWidget::mouseReleaseEvent(event);

		if (const auto idx = GetImageIndex(event->pos()); d->pressed.contains(event->button()) && idx < d->imageCount) {
			if (d->pressed[event->button()] == idx) {
				d->parent->OnMouseClicked(idx, event->button());
				d->pressed.remove(event->button());
			}

			d->parent->OnMousePressed(d->pressed[event->button()], false, event->button());
		}
	}

	auto GalleryPainter::mouseDoubleClickEvent(QMouseEvent* event) -> void {
		QWidget::mouseDoubleClickEvent(event);

		if (const auto idx = GetImageIndex(event->pos()); idx > -1 && idx < d->imageCount)
			d->parent->OnMouseDoubleClicked(idx, event->button());
	}

	auto GalleryPainter::mouseMoveEvent(QMouseEvent* event) -> void {
		QWidget::mouseMoveEvent(event);

		if (const auto idx = GetImageIndex(event->pos()); idx != d->hovered && idx < d->imageCount) {
			if (d->hovered > -1)
				d->parent->OnMouseHovered(d->hovered, false);

			d->hovered = idx;

			if (idx > -1)
				d->parent->OnMouseHovered(d->hovered, true);
		}
	}

	auto GalleryPainter::leaveEvent(QEvent* event) -> void {
		QWidget::leaveEvent(event);

		if (d->hovered > -1) {
			d->parent->OnMouseHovered(d->hovered, false);
			d->hovered = -1;
		}
	}

	auto GalleryPainter::showEvent(QShowEvent* event) -> void {
		QWidget::showEvent(event);

		updateGeometry();
	}

	auto GalleryPainter::paintEvent(QPaintEvent* event) -> void {
		QWidget::paintEvent(event);
		QPainter painter(this);
		painter.setPen(Qt::NoPen);

		auto loadedAll = true;

		for (auto i = std::max(0, d->range.x()); i < d->range.y() && i < d->imageCount; i++) {
			const auto pos = GetImagePoint(i);

			if (d->parent->OnImageExists(i))
				painter.drawPixmap(pos.x(), pos.y(), d->size, d->size, d->parent->OnImageRequested(i));
			else {
				loadedAll = false;

				const auto gradWidth = width() / 2.0;
				const auto gradX = (this->width() + gradWidth) * d->anime.currentValue().toDouble() - gradWidth;

				QLinearGradient gradient(gradX, 0, gradX + gradWidth, 0);
				gradient.setColorAt(0, QColor(255, 255, 255, 30));
				gradient.setColorAt(0.4, QColor(255, 255, 255, 45));
				gradient.setColorAt(0.8, QColor(255, 255, 255, 45));
				gradient.setColorAt(0.9, QColor(255, 255, 255, 30));

				painter.setBrush(QBrush(gradient));
				painter.drawRect(pos.x(), pos.y(), d->size, d->size);
			}

			for (auto j = 0; j < d->layerCount; j++) {
				if (auto pixmap = d->parent->OnLayerRequested(i, j); !pixmap.isNull())
					painter.drawPixmap(pos.x(), pos.y(), d->size, d->size, pixmap);
			}
		}

		if (loadedAll)
			d->anime.stop();
		else if (d->anime.state() != QAbstractAnimation::Running)
			d->anime.start();
	}

	auto GalleryPainter::resizeEvent(QResizeEvent* event) -> void {
		QWidget::resizeEvent(event);
		UpdateImageSize();
	}

	auto GalleryPainter::minimumSizeHint() const -> QSize {
		const auto width = (d->column - 1) * d->space + d->column;

		return { width, heightForWidth(width) };
	}

	auto GalleryPainter::heightForWidth(int i) const -> int {
		return std::max(0, (d->imageCount + d->column - 1) / d->column * (d->size + d->space) - d->space);
	}

	auto GalleryPainter::hasHeightForWidth() const -> bool {
		return true;
	}

	auto GalleryPainter::OnAnimationValueChanged(const QVariant& value) -> void {
		update();
	}
}
