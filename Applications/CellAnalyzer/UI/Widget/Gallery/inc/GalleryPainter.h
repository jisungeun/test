#pragma once

#include <memory>

#include <QWidget>

#include "CellAnalyzer.UI.Widget.GalleryExport.h"

namespace CellAnalyzer::UI::Widget {
	class CellAnalyzer_UI_Widget_Gallery_API GalleryPainter final : public QWidget {
	public:
		explicit GalleryPainter(QWidget* parent);
		~GalleryPainter() override;

		auto SetColumnCount(int count) -> void;
		auto SetImageCount(int count) -> void;
		auto SetLayerCount(int count) -> void;
		auto SetVisibleRange(int from, int to) -> void;
		auto UpdateImageSize() -> void;

		auto GetColumnCount() const -> int;
		auto GetImageCount() const -> int;
		auto GetLayerCount() const -> int;

		auto GetImageSize() const -> int;
		auto GetImagePoint(int index) const -> QPoint;
		auto GetImageIndex(const QPoint& point) const -> int;
		auto GetImageIndexNoSpace(const QPoint& point) const -> int;
		auto GetVisibleRange() const -> QPoint;

	protected:
		auto mousePressEvent(QMouseEvent* event) -> void override;
		auto mouseReleaseEvent(QMouseEvent* event) -> void override;
		auto mouseDoubleClickEvent(QMouseEvent* event) -> void override;
		auto mouseMoveEvent(QMouseEvent* event) -> void override;
		auto leaveEvent(QEvent* event) -> void override;

		auto showEvent(QShowEvent* event) -> void override;
		auto paintEvent(QPaintEvent* event) -> void override;
		auto resizeEvent(QResizeEvent* event) -> void override;

		auto minimumSizeHint() const -> QSize override;
		auto heightForWidth(int) const -> int override;
		auto hasHeightForWidth() const -> bool override;

	protected slots:
		auto OnAnimationValueChanged(const QVariant& value) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
