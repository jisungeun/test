#pragma once

#include <QMouseEvent>
#include <QPixmap>
#include <QStringList>

#include "CellAnalyzer.UI.Widget.GalleryExport.h"

namespace CellAnalyzer::UI::Widget {
	class IGalleryReader;
	using GalleryReaderPtr = std::shared_ptr<IGalleryReader>;
	using GalleryReaderList = QList<GalleryReaderPtr>;

	class CellAnalyzer_UI_Widget_Gallery_API IGalleryReader {
	public:
		virtual ~IGalleryReader() = default;

		virtual auto GetImageCount() const -> int = 0;
		virtual auto GetLayerCount() const -> int = 0;

		virtual auto ContainsImage(int index) const -> bool = 0;
		virtual auto ContainsLayer(int imageIndex, int layerIndex) const -> bool = 0;

		virtual auto GetImage(int index) const -> QPixmap = 0;
		virtual auto GetLayer(int imageIndex, int layerIndex) const -> QPixmap = 0;

		virtual auto OnMouseHovered(int index, bool hovered) -> void;
		virtual auto OnMousePressed(int index, bool pressed, Qt::MouseButton button) -> void;
		virtual auto OnClicked(int index, Qt::MouseButton button) -> void;
		virtual auto OnDoubleClicked(int index, Qt::MouseButton button) -> void;
		virtual auto OnImageResized(int size) -> void;
		virtual auto OnVisibleImageChanged(int from, int to) -> void;
	};
}
