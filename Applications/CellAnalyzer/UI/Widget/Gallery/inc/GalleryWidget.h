#pragma once

#include <memory>

#include <QWidget>

#include "GalleryPainter.h"

#include "CellAnalyzer.UI.Widget.GalleryExport.h"

namespace CellAnalyzer::UI::Widget {
	class CellAnalyzer_UI_Widget_Gallery_API GalleryWidget : public QWidget {
	public:
		explicit GalleryWidget(QWidget* parent = nullptr);
		~GalleryWidget() override;

		auto ScrollTo(int index) -> void;

		auto SetColumnCount(int count) -> void;
		auto SetImageCount(int count) -> void;
		auto SetLayerCount(int count) -> void;

		auto GetColumnCount() const -> int;
		auto GetImageCount() const -> int;
		auto GetLayerCount() const -> int;

		auto GetImageSize() const -> int;
		auto GetImageIndex(const QPoint& point) const -> int;
		auto GetImagePoint(int index) const -> QPoint;
		auto GetVisibleRange() const -> QPoint;

	protected:
		virtual auto OnMousePressed(int index, bool pressed, Qt::MouseButton button) -> void;
		virtual auto OnMouseHovered(int index, bool hovered) -> void;
		virtual auto OnMouseClicked(int index, Qt::MouseButton button) -> void;
		virtual auto OnMouseDoubleClicked(int index, Qt::MouseButton button) -> void;
		virtual auto OnVisibleRangeChanged(int from, int to) -> void;
		virtual auto OnImageSizeChanged(int size) -> void;

		virtual auto OnImageExists(int index) const -> bool;
		virtual auto OnImageRequested(int index) const -> QPixmap;
		virtual auto OnLayerRequested(int index, int depth) const -> QPixmap;

		auto resizeEvent(QResizeEvent* event) -> void override;

	protected slots:
		auto OnScrolled(int value) -> void;

	private:
		friend GalleryPainter;
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
