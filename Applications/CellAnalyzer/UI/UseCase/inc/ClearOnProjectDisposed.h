#pragma once

#include <memory>

#include "IProjectEvent.h"
#include "IServiceProvider.h"

#include "CellAnalyzer.UI.UseCaseExport.h"

namespace CellAnalyzer::UI {
	class CellAnalyzer_UI_UseCase_API ClearOnProjectDisposed final : public IProjectEvent {
	public:
		explicit ClearOnProjectDisposed(Tomocube::IServiceProvider* provider);
		~ClearOnProjectDisposed() override;

		auto OnDisposed(const QString& project) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
