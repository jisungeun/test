#include "ClearOnProjectDisposed.h"

#include "IMenuHandler.h"
#include "IScreenHandler.h"
#include "IStatusBarHandler.h"
#include "IWindowHandler.h"

namespace CellAnalyzer::UI {
	struct ClearOnProjectDisposed::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
	};

	ClearOnProjectDisposed::ClearOnProjectDisposed(Tomocube::IServiceProvider* provider) : IProjectEvent(), d(new Impl) {
		d->provider = provider;
	}

	ClearOnProjectDisposed::~ClearOnProjectDisposed() = default;

	auto ClearOnProjectDisposed::OnDisposed(const QString& project) -> void {
		if (const auto handler = d->provider->GetService<IMenuHandler>()) {
			handler->ClearScreens();
			handler->ClearWindows();
			handler->ClearToolBars();
			handler->ClearMenu();
		}

		if (const auto handler = d->provider->GetService<IWindowHandler>())
			handler->CloseAll();

		if (const auto handler = d->provider->GetService<IScreenHandler>())
			handler->CloseAll();

		//if (const auto handler = d->provider->GetService<IStatusBarHandler>())
		//	handler->SetState({});
	}
}
