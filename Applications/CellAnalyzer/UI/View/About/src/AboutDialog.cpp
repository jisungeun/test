#include <QDesktopServices>
#include <QFileDialog>

#include "AboutDialog.h"

#include "IAlertHandler.h"
#include "IApplication.h"
#include "ILicenseManager.h"
#include "IProjectService.h"

#include "ui_AboutDialog.h"

namespace CellAnalyzer::UI::View {
	struct AboutDialog::Impl {
		Ui::AboutDialog ui {};
		Tomocube::IServiceProvider* provider = nullptr;
		bool close = false;

		QTableWidgetItem* activated = nullptr;
		QTableWidgetItem* edition = nullptr;
		QTableWidgetItem* user = nullptr;

		auto SetEnability(bool enabled) -> void;
		auto UpdateLicense() -> void;
	};

	auto AboutDialog::Impl::SetEnability(bool enabled) -> void {
		ui.keyLine->setEnabled(enabled);
		ui.activateBtn->setEnabled(enabled && !ui.keyLine->text().isEmpty());
		ui.activateOfflineBtn->setEnabled(enabled && !ui.keyLine->text().isEmpty());
		ui.deactiveBtn->setEnabled(enabled);
		ui.deactiveOfflineBtn->setEnabled(enabled);
	}

	auto AboutDialog::Impl::UpdateLicense() -> void {
		if (const auto man = provider->GetService<ILicenseManager>()) {
			ui.licenseTable->setItem(0, 0, new QTableWidgetItem(man->IsActivated() ? "Activated" : "Not Activated"));
			ui.licenseTable->setItem(0, 1, new QTableWidgetItem(man->GetEdition()));
			ui.licenseTable->setItem(0, 2, new QTableWidgetItem(man->GetUserName()));
			ui.activeFrame->setVisible(!man->IsActivated());
			ui.deactiveFrame->setVisible(man->IsActivated());
			ui.errorLabel->clear();
			ui.keyLine->clear();

			SetEnability(true);
		} else
			ui.errorLabel->setText("Could not find license information.");
	}

	AboutDialog::AboutDialog(Tomocube::IServiceProvider* provider, QWidget* parent) : QDialog(parent, Qt::WindowMinimizeButtonHint | Qt::WindowCloseButtonHint | Qt::Dialog), std::enable_shared_from_this<AboutDialog>(), IView(), ILicenseEvent(), d(new Impl) {
		d->ui.setupUi(this);
		d->provider = provider;
		d->ui.aboutTable->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
		d->ui.licenseTable->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
		d->ui.activeFrame->setVisible(false);
		d->ui.deactiveFrame->setVisible(false);
		d->ui.browser->setVisible(false);
		d->ui.ossBtn->setObjectName("quiet_color");

		if (const auto app = d->provider->GetService<IApplication>()) {
			d->ui.aboutTable->setItem(0, 0, new QTableWidgetItem(app->GetSoftwareName()));
			d->ui.aboutTable->setItem(0, 1, new QTableWidgetItem(app->GetVersion().ToString()));
			d->ui.aboutTable->setItem(0, 2, new QTableWidgetItem(app->GetCorporation()));
			d->ui.aboutTable->setItem(0, 3, new QTableWidgetItem(app->GetContact()));

			for (const auto& [name, url, copyright, license] : app->GetOpenSourceLicenseList()) {
				d->ui.browser->append(QString("<b>%1</b>").arg(name));
				d->ui.browser->append(QString("<a style='color: #1F8896' href='%1'>%1</a>").arg(url));
				d->ui.browser->append(QString("<span style='color: #bbb'>%1</span>").arg(copyright));
				d->ui.browser->append(QString("<a style='color: #1F8896' href='license://%1.md'>%1</a>").arg(license));
				d->ui.browser->append(QString());
			}
		}

		d->UpdateLicense();
		setFixedSize(this->size());

		connect(d->ui.browser, &QTextBrowser::anchorClicked, this, &AboutDialog::OnBrowserClicked);
		connect(d->ui.ossBtn, &QPushButton::clicked, this, &AboutDialog::OnOssBtnClicked);
		connect(d->ui.keyLine, &QLineEdit::textChanged, this, &AboutDialog::OnKeyTextChanged);
		connect(d->ui.activateBtn, &QPushButton::clicked, this, &AboutDialog::OnActivateBtnClicked);
		connect(d->ui.activateOfflineBtn, &QPushButton::clicked, this, &AboutDialog::OnActivateOfflineBtnClicked);
		connect(d->ui.deactiveBtn, &QPushButton::clicked, this, &AboutDialog::OnDeactivateBtnClicked);
		connect(d->ui.deactiveOfflineBtn, &QPushButton::clicked, this, &AboutDialog::OnDeactivateOfflineBtnClicked);
	}

	AboutDialog::~AboutDialog() = default;

	auto AboutDialog::CloseOnActivation(bool close) -> void {
		d->close = close;
	}

	auto AboutDialog::showEvent(QShowEvent* event) -> void {
		if (const auto man = d->provider->GetService<ILicenseManager>())
			man->AddEvent(shared_from_this());
	}

	auto AboutDialog::closeEvent(QCloseEvent* event) -> void {
		if (const auto man = d->provider->GetService<ILicenseManager>())
			man->RemoveEvent(shared_from_this());

		QDialog::closeEvent(event);
	}

	auto AboutDialog::OnKeyTextChanged(const QString& text) -> void {
		d->ui.activateBtn->setEnabled(!text.isEmpty());
		d->ui.activateOfflineBtn->setEnabled(!text.isEmpty());
	}

	auto AboutDialog::OnOssBtnClicked() -> void {
		d->ui.browser->setVisible(d->ui.ossBtn->isChecked());
	}

	auto AboutDialog::OnBrowserClicked(const QUrl& url) -> void {
		d->ui.browser->setHtml(d->ui.browser->toHtml());

		if (const auto scheme = url.scheme(); scheme.startsWith("http"))
			QDesktopServices::openUrl(QUrl::fromLocalFile(url.toString()));
		else if (scheme.startsWith("license")) {
			if (QFile file(QString(":/License/%1").arg(url.host())); file.open(QIODevice::ReadOnly)) {
				const auto alert = d->provider->GetService<IAlertHandler>();
				alert->ShowMessage(url.host(), file.readAll());
			}
		}
	}

	auto AboutDialog::OnActivateBtnClicked() -> void {
		if (const auto man = d->provider->GetService<ILicenseManager>()) {
			d->SetEnability(false);
			man->Activate(d->ui.keyLine->text());
		}
	}

	auto AboutDialog::OnActivateOfflineBtnClicked() -> void {
		if (const auto man = d->provider->GetService<ILicenseManager>()) {
			QFileDialog dialog;
			dialog.setFileMode(QFileDialog::ExistingFile);
			dialog.setNameFilter("Text (*.txt)");

			if (dialog.exec() == Accepted) {
				if (const auto files = dialog.selectedFiles(); !files.isEmpty()) {
					man->ActivateOffline(d->ui.keyLine->text(), files.first());
					d->SetEnability(false);
				}
			}
		}
	}

	auto AboutDialog::OnDeactivateBtnClicked() -> void {
		if (const auto man = d->provider->GetService<ILicenseManager>()) {
			man->Deactivate();
			d->SetEnability(false);
		}
	}

	auto AboutDialog::OnDeactivateOfflineBtnClicked() -> void {
		if (const auto man = d->provider->GetService<ILicenseManager>()) {
			QFileDialog dialog;
			dialog.setFileMode(QFileDialog::ExistingFile);
			dialog.setAcceptMode(QFileDialog::AcceptSave);
			dialog.selectFile("Deactivation Request");
			dialog.setNameFilter("Text (*.txt)");

			if (dialog.exec() == Accepted) {
				if (const auto files = dialog.selectedFiles(); !files.isEmpty()) {
					man->DeactivateOffline(files.first());
					d->SetEnability(false);
				}
			}
		}
	}

	auto AboutDialog::OnActivated(const QString& edition, bool offline) -> void {
		QMetaObject::invokeMethod(qApp, [this] {
			d->UpdateLicense();

			if (d->close)
				close();
		});
	}

	auto AboutDialog::OnDeactivated(bool offline) -> void {
		QMetaObject::invokeMethod(qApp, [this] {
			if (const auto service = d->provider->GetService<IProjectService>())
				service->Dispose();

			d->UpdateLicense();
		});
	}

	auto AboutDialog::OnErrorOccured(const QString& message) -> void {
		QMetaObject::invokeMethod(qApp, [this, message] {
			d->ui.errorLabel->setText(message);
			d->SetEnability(true);
		});
	}
}
