#pragma once

#include <memory>

#include <QDialog>

#include "ILicenseEvent.h"
#include "IServiceProvider.h"
#include "IView.h"

#include "CellAnalyzer.UI.View.AboutExport.h"

namespace CellAnalyzer::UI::View {
	class CellAnalyzer_UI_View_About_API AboutDialog final : public QDialog, public std::enable_shared_from_this<AboutDialog>, public IView, public ILicenseEvent {
	public:
		explicit AboutDialog(Tomocube::IServiceProvider* provider, QWidget* parent = nullptr);
		~AboutDialog() override;

		auto CloseOnActivation(bool close) -> void;

	protected:
		auto showEvent(QShowEvent* event) -> void override;
		auto closeEvent(QCloseEvent* event) -> void override;

	protected slots:
		auto OnKeyTextChanged(const QString& text) -> void;

		auto OnOssBtnClicked() -> void;
		auto OnBrowserClicked(const QUrl& url) -> void;
		auto OnActivateBtnClicked() -> void;
		auto OnActivateOfflineBtnClicked() -> void;
		auto OnDeactivateBtnClicked() -> void;
		auto OnDeactivateOfflineBtnClicked() -> void;

	public:
		auto OnActivated(const QString& edition, bool offline) -> void override;
		auto OnDeactivated(bool offline) -> void override;
		auto OnErrorOccured(const QString& message) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
