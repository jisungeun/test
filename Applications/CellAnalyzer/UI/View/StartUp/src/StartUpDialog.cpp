#include <QFileInfo>
#include <QProcess>
#include <QScreen>

#include "StartUpDialog.h"
#include "RecentButton.h"
#include "StartButton.h"

#include "IAlertHandler.h"
#include "IDatabase.h"
#include "IProjectService.h"

#include "ui_StartUpDialog.h"

namespace CellAnalyzer::UI::View {
	struct StartUpDialog::Impl {
		Ui::StartUpDialog ui {};
		Tomocube::IServiceProvider* provider = nullptr;

		static auto SetStylesheet(QWidget* parent, QWidget* target) -> void;
		static auto GetCenter(const QSize& size) -> QPoint;

		auto AddLegacies(QDialog* parent) -> void;
	};

	auto StartUpDialog::Impl::SetStylesheet(QWidget* parent, QWidget* target) -> void {
		if (QFile file(":/Stylesheet/CellAnalyzer.qss", parent); file.open(QIODevice::ReadOnly))
			target->setStyleSheet(file.readAll());
	}

	auto StartUpDialog::Impl::GetCenter(const QSize& size) -> QPoint {
		const auto* screen = QGuiApplication::screenAt(QCursor::pos());
		auto geometry = screen->availableGeometry();
		geometry.setWidth(geometry.width() - size.width());
		geometry.setHeight(geometry.height() - size.height());

		return geometry.center();
	}

	auto StartUpDialog::Impl::AddLegacies(QDialog* parent) -> void {
		auto* start = new StartButton("TA Viewer", QIcon(":/Flat/View.svg"), parent);
		start->setEnabled(QFileInfo::exists(QString("%1/%2").arg(QCoreApplication::applicationDirPath()).arg("TomoAnalysis.exe")));
		ui.legacyLayout->addWidget(start);

		connect(start, &QPushButton::clicked, parent, [parent] {
			if (QProcess::startDetached(QString("%1/%2").arg(QCoreApplication::applicationDirPath()).arg("TomoAnalysis.exe"), { "-nav" })) {
				parent->reject();
				parent->close();
				QApplication::exit();
			}
		});
	}

	StartUpDialog::StartUpDialog(Tomocube::IServiceProvider* provider, QWidget* parent) : QDialog(nullptr, Qt::Dialog | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint), IView(), d(new Impl) {
		d->ui.setupUi(this);
		d->ui.startBtn->setObjectName("quiet_color");
		// TODO Update to visible later
		d->ui.startBtn->setVisible(false);
		d->provider = provider;
		d->SetStylesheet(parent, this);
		d->AddLegacies(this);

		connect(d->ui.startBtn, &QPushButton::clicked, this, &StartUpDialog::OnStartBtnClicked);
	}

	StartUpDialog::~StartUpDialog() = default;

	auto StartUpDialog::AddProject(const QString& project, const QString& desc, const QIcon& icon) -> void {
		auto* button = new StartButton(project, desc, icon, this);
		d->ui.actionLayout->addWidget(button);

		connect(button, &QPushButton::clicked, this, [this, project] {
			emit ProjectSelected(project);
		});
	}

	auto StartUpDialog::AddHistory(const QString& project, const QString& title, const QString& url, const QDateTime& datetime, const QIcon& icon) -> void {
		auto* button = new RecentButton(project, title, url, datetime, icon, this);
		d->ui.recentLayout->addWidget(button);

		connect(button, &QPushButton::clicked, this, [this, project, url] {
			emit HistorySelected(project, url);
		});
	}

	auto StartUpDialog::RemoveHistory(const QString& project, const QString& url) -> void {
		for (const auto* ch : d->ui.recentFrame->findChildren<RecentButton*>(QString(), Qt::FindChildrenRecursively)) {
			if (ch->GetProject() == project && ch->GetUrl() == url)
				delete ch;
		}
	}

	auto StartUpDialog::showEvent(QShowEvent* event) -> void {
		QDialog::showEvent(event);

		if (const auto database = d->provider->GetService<IDatabase>(); const auto data = database->GetMap("GUI\\StartUp\\Geometry")) {
			auto size = this->size();
			auto state = Qt::WindowNoState;

			if (const auto value = IDatabase::ReadNumber(*data, "Width"))
				size.setWidth(*value);
			if (const auto value = IDatabase::ReadNumber(*data, "Height"))
				size.setHeight(*value);

			if (const auto value = IDatabase::ReadString(*data, "WindowState")) {
				if (*value == "Maximized")
					state = Qt::WindowMaximized;
			}

			if (state == Qt::WindowNoState) {
				this->resize(size);
				this->move(d->GetCenter(size));
			} else
				this->setWindowState(state);
		}
	}

	auto StartUpDialog::closeEvent(QCloseEvent* close_event) -> void {
		QDialog::closeEvent(close_event);

		if (const auto database = d->provider->GetService<IDatabase>()) {
			QVariantMap map;
			map["Width"] = width();
			map["Height"] = height();

			if (const auto state = windowState(); state.testFlag(Qt::WindowMaximized))
				map["WindowState"] = "Maximized";
			else
				map["WindowState"] = "NoState";

			database->Save("GUI\\StartUp\\Geometry", map);
		}
	}

	auto StartUpDialog::OnStartBtnClicked() -> void {
		accept();
	}
}
