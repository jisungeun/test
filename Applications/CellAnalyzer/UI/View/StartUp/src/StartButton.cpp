#include "StartButton.h"

#include "ui_StartButton.h"

namespace CellAnalyzer::UI::View {
	struct StartButton::Impl {
		Ui::StartButton ui;
	};

	StartButton::StartButton(QWidget* parent) : QPushButton(parent), d(new Impl) {
		d->ui.setupUi(this);
	}

	StartButton::StartButton(const QString& title, const QIcon& ico, QWidget* parent) : StartButton(parent) {
		SetTitle(title);
		SetIcon(ico);
		SetDescription({});
	}

	StartButton::StartButton(const QString& title, const QString& desc, const QIcon& ico, QWidget* parent) : StartButton(parent) {
		SetTitle(title);
		SetIcon(ico);
		SetDescription(desc);
	}

	StartButton::~StartButton() = default;

	auto StartButton::SetTitle(const QString& title) -> void {
		d->ui.titleLabel->setText(title);
	}

	auto StartButton::SetDescription(const QString& desc) -> void {
		d->ui.descLabel->setText(desc);
		d->ui.descLabel->setVisible(!desc.isEmpty());

		this->setMinimumHeight(desc.isEmpty() ? 50 : 75);
		d->ui.icon->setAlignment(desc.isEmpty() ? Qt::AlignVCenter : Qt::AlignTop);
	}

	auto StartButton::SetIcon(const QIcon& icon) -> void {
		d->ui.icon->setPixmap(icon.pixmap(d->ui.icon->size()));
	}

	auto StartButton::GetTitle() const -> QString {
		return d->ui.titleLabel->text();
	}

	auto StartButton::GetDescription() const -> QString {
		return d->ui.descLabel->text();
	}
}
