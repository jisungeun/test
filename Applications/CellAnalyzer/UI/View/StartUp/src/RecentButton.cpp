#include "RecentButton.h"

#include "ui_RecentButton.h"

namespace CellAnalyzer::UI::View {
	struct RecentButton::Impl {
		Ui::RecentButton ui;
		QString project;
	};

	RecentButton::RecentButton(const QString& project, const QString& title, const QString& url, const QDateTime& datetime, const QIcon& icon, QWidget* parent) : QPushButton(parent), d(new Impl) {
		d->ui.setupUi(this);
		d->project = project;
		d->ui.titleLabel->setText(title);
		d->ui.urlLabel->setText(url);
		d->ui.dateLabel->setText(datetime.toString("yyyy/MM/dd HH:mm:ss"));
		d->ui.icon->setPixmap(icon.pixmap(d->ui.icon->size()));
	}

	RecentButton::~RecentButton() = default;

	auto RecentButton::GetProject() const -> QString {
		return d->project;
	}

	auto RecentButton::GetTitle() const -> QString {
		return d->ui.titleLabel->text();
	}

	auto RecentButton::GetUrl() const -> QString {
		return d->ui.urlLabel->text();
	}
}
