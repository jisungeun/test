#pragma once

#include <memory>

#include <QPushButton>

#include "CellAnalyzer.UI.View.StartUpExport.h"

namespace CellAnalyzer::UI::View {
	class CellAnalyzer_UI_View_StartUp_API StartButton final : public QPushButton {
	public:
		explicit StartButton(QWidget* parent = nullptr);
		StartButton(const QString& title, const QIcon& ico, QWidget* parent = nullptr);
		StartButton(const QString& title, const QString& desc, const QIcon& ico, QWidget* parent = nullptr);
		~StartButton() override;

		auto SetTitle(const QString& title) -> void;
		auto SetDescription(const QString& desc) -> void;
		auto SetIcon(const QIcon& icon) -> void;

		auto GetTitle() const -> QString;
		auto GetDescription() const -> QString;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
