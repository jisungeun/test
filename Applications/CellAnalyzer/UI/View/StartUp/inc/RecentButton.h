#pragma once

#include <memory>

#include <QDateTime>
#include <QPushButton>

#include "CellAnalyzer.UI.View.StartUpExport.h"

namespace CellAnalyzer::UI::View {
	class CellAnalyzer_UI_View_StartUp_API RecentButton final : public QPushButton {
	public:
		RecentButton(const QString& project, const QString& title, const QString& url, const QDateTime& datetime, const QIcon& icon, QWidget* parent = nullptr);
		~RecentButton() override;

		auto GetProject() const -> QString;
		auto GetTitle() const -> QString;
		auto GetUrl() const -> QString;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
