#pragma once

#include <memory>

#include <QDialog>

#include "IServiceProvider.h"
#include "IView.h"

#include "CellAnalyzer.UI.View.StartUpExport.h"

namespace CellAnalyzer::UI::View {
	class CellAnalyzer_UI_View_StartUp_API StartUpDialog final : public QDialog, public IView {
		Q_OBJECT

	public:
		explicit StartUpDialog(Tomocube::IServiceProvider* provider, QWidget* parent = nullptr);
		~StartUpDialog() override;

		auto AddProject(const QString& project, const QString& desc, const QIcon& icon) -> void;
		auto AddHistory(const QString& project, const QString& title, const QString& url, const QDateTime& datetime, const QIcon& icon) -> void;
		auto RemoveHistory(const QString& project, const QString& url) -> void;

	protected:
		auto showEvent(QShowEvent* event) -> void override;
		auto closeEvent(QCloseEvent* event) -> void override;

	protected slots:
		auto OnStartBtnClicked() -> void;

	signals:
		auto ProjectSelected(const QString& id) -> void;
		auto HistorySelected(const QString& id, const QString& url) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
