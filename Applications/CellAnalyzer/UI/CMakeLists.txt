add_subdirectory(UI)
add_subdirectory(UseCase)

add_subdirectory(View)
add_subdirectory(Widget)