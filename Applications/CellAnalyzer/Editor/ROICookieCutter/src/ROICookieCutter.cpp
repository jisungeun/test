#include "ROICookieCutter.h"

#include <HT2D.h>
#include <HT3D.h>
#include <FL2D.h>
#include <FL3D.h>
#include <Measure.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <Inventor/Qt/SoQt.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <VolumeViz/nodes/SoVolumeData.h>

#include <ROIReader.h>

namespace CellAnalyzer::Editor::Simple {
	using namespace imagedev;
	using namespace iolink;

	struct ROICookieCutter::Impl {
		InputImageType type { Unknown };

		std::shared_ptr<ImageView> imageView { nullptr };
		int curTimeStep { 0 };
		double curOffset { 0 };
		int curChannel { 0 };
		QString curChannelName;
		DataPtr imageData { nullptr };
		int dim[3] { 0, 0, 0 };
		double res[3] { 0, 0, 0 };
		QString imageName;
		QString roiPath;
		DataPtr pathData { nullptr };
		QString pathName;
		DataPtr resultData { nullptr };

		auto AddHT2D(std::shared_ptr<Data::HT2D> data) -> bool;
		auto AddHT3D(std::shared_ptr<Data::HT3D> data) -> bool;
		auto AddFL2D(std::shared_ptr<Data::FL2D> data) -> bool;
		auto AddFL3D(std::shared_ptr<Data::FL3D> data) -> bool;

		auto PerformCookieCutter() -> void;
		auto Cut2D() -> void;
		auto Cut3D() -> void;
	};

	auto ROICookieCutter::Impl::AddHT2D(std::shared_ptr<Data::HT2D> data) -> bool {
		type = HT2D;
		curTimeStep = data->GetTimeStep();
		const auto res = data->GetResolution();
		const auto size = data->GetSize();
		//Copy Image to imageDev
		auto spatialCalibrationProperty = SpatialCalibrationProperty(
																	{ 0, 0, 0 },	// origin
																	{ res.x, res.y, 1 }	// spacing
																	);
		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
		const VectorXu64 imageShape { static_cast<uint64_t>(size.x), static_cast<uint64_t>(size.y) };
		imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		setDimensionalInterpretation(imageView, ImageTypeId::IMAGE);

		const RegionXu64 imageRegion { { 0, 0 }, imageShape };
		imageView->writeRegion(imageRegion, data->GetData());

		return true;
	}

	auto ROICookieCutter::Impl::AddHT3D(std::shared_ptr<Data::HT3D> data) -> bool {
		type = HT3D;
		curTimeStep = data->GetTimeStep();
		curOffset = data->GetZOffset();
		const auto res = data->GetResolution();
		const auto size = data->GetSize();

		//Copy Image to imageDev
		auto spatialCalibrationProperty = SpatialCalibrationProperty(
																	{ 0, 0, 0 },	// origin
																	{ res.x, res.y, res.z }	// spacing
																	);
		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
		const VectorXu64 imageShape { static_cast<uint64_t>(size.x), static_cast<uint64_t>(size.y), static_cast<uint64_t>(size.z) };
		imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);

		const RegionXu64 imageRegion { { 0, 0, 0 }, imageShape };
		imageView->writeRegion(imageRegion, data->GetData());

		return true;
	}

	auto ROICookieCutter::Impl::AddFL2D(std::shared_ptr<Data::FL2D> data) -> bool {
		type = FL2D;
		curChannel = data->GetChannelIndex();
		curChannelName = data->GetChannelName();
		curTimeStep = data->GetTimeStep();

		const auto res = data->GetResolution();
		const auto size = data->GetSize();

		//Copy Image to imageDev
		auto spatialCalibrationProperty = SpatialCalibrationProperty(
																	{ 0, 0, 0 },	// origin
																	{ res.x, res.y, 1 }	// spacing
																	);
		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
		const VectorXu64 imageShape { static_cast<uint64_t>(size.x), static_cast<uint64_t>(size.y) };
		imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		setDimensionalInterpretation(imageView, ImageTypeId::IMAGE);

		const RegionXu64 imageRegion { { 0, 0 }, imageShape };
		imageView->writeRegion(imageRegion, data->GetData());

		return true;
	}

	auto ROICookieCutter::Impl::AddFL3D(std::shared_ptr<Data::FL3D> data) -> bool {
		type = FL3D;
		curChannel = data->GetChannelIndex();
		curChannelName = data->GetChannelName();
		curTimeStep = data->GetTimeStep();
		curOffset = data->GetZOffset();
		const auto res = data->GetResolution();
		const auto size = data->GetSize();

		//Copy Image to imageDev
		auto spatialCalibrationProperty = SpatialCalibrationProperty(
																	{ 0, 0, 0 },	// origin
																	{ res.x, res.y, res.z }	// spacing
																	);
		const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
		const VectorXu64 imageShape { static_cast<uint64_t>(size.x), static_cast<uint64_t>(size.y), static_cast<uint64_t>(size.z) };
		imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
		setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);

		const RegionXu64 imageRegion { { 0, 0, 0 }, imageShape };
		imageView->writeRegion(imageRegion, data->GetData());

		return true;
	}

	auto ROICookieCutter::Impl::PerformCookieCutter() -> void {
		if (nullptr == imageData) {
			return;
		}
		if (type == HT2D || type == FL2D) {
			Cut2D();
		} else {
			Cut3D();
		}
	}

	auto ROICookieCutter::Impl::Cut2D() -> void {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.00001;
		};
		const auto reader = TC::IO::ROIReader(roiPath);
		const auto roiset = reader.Read();

		//Create empty mask
		auto buffer = new uint16_t[dim[0] * dim[1]]();

		SoVolumeData* MaskVolume = new SoVolumeData;
		MaskVolume->ref();
		MaskVolume->data.setValue(SbVec3i32(dim[0], dim[1], 1), SbDataType::UNSIGNED_SHORT, 16, buffer, SoSFArray::NO_COPY_AND_DELETE);
		MaskVolume->extent.setValue(SbBox3f(-dim[0] * res[0] / 2, -dim[1] * res[1] / 2, -0.5, dim[0] * res[0] / 2, dim[1] * res[1] / 2, 0.5));

		for (const auto roi : roiset.GetROIs()) {
			const auto phyZ = 0.0;
			std::vector<SbVec3f> resultRayCast;
			for (const auto v : roi.GetVertices()) {
				const auto [px, py, pz] = v;
				const auto element = SbVec3f(px, py, phyZ);
				resultRayCast.push_back(element);
			}

			SoVertexProperty* vertexProp = new SoVertexProperty;
			vertexProp->ref();
			vertexProp->vertex.setValues(0, static_cast<int>(resultRayCast.size()), &resultRayCast[0]);

			SoFaceSet* faceSet = new SoFaceSet;
			faceSet->ref();
			faceSet->numVertices.set1Value(0, static_cast<int>(resultRayCast.size()));
			faceSet->vertexProperty.setValue(vertexProp);

			int editionId;
			MaskVolume->startEditing(editionId);
			MaskVolume->editSurfaceShape(faceSet, 1., 1);
			MaskVolume->finishEditing(editionId);

			faceSet->unref();
			faceSet = nullptr;
			vertexProp->unref();
			vertexProp = nullptr;
		}
		MaskVolume->saveEditing();

		try {
			//create mask binary mask view
			std::shared_ptr<iolink::ImageView> maskView { nullptr };
			auto spatialCalibrationProperty = SpatialCalibrationProperty(
																		{ 0, 0, 0 }, // origin
																		{ res[0], res[1], 1 }	// spacing
																		);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
			const VectorXu64 imageShape { static_cast<uint64_t>(dim[0]), static_cast<uint64_t>(dim[1]) };
			maskView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
			setDimensionalInterpretation(maskView, ImageTypeId::IMAGE);

			const RegionXu64 imageRegion { { 0, 0 }, imageShape };
			maskView->writeRegion(imageRegion, MaskVolume->data.getValue());

			const auto imageStat = intensityStatistics(imageView, IntensityStatistics::MIN_MAX, { 0, 1 });
			const auto min = imageStat->minimum();

			const auto minImage = resetImage(imageView, min);

			const auto binaryImage = convertImage(maskView, ConvertImage::BINARY);

			const auto maskedImage = combineByMask(imageView, minImage, binaryImage);

			const auto resultImage = convertImage(maskedImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			const auto resultStat = intensityStatistics(resultImage, IntensityStatistics::MIN_MAX, { 0, 1 });

			std::unique_ptr<uint16_t[]> resultArr(new uint16_t[dim[0] * dim[1]](), std::default_delete<uint16_t[]>());
			memcpy(resultArr.get(), resultImage->bufferReadOnly(), dim[0] * dim[1] * sizeof(uint16_t));

			resultData = nullptr;

			if (type == HT2D) {
				resultData = std::shared_ptr<Data::HT2D>(new Data::HT2D(std::move(resultArr), { dim[0], dim[1] }, { res[0], res[1] }, { -dim[0] * res[0] / 2, -dim[1] * res[1] / 2 }, { resultStat->minimum() / 10000.0, resultStat->maximum() / 10000.0 }, curTimeStep));
			} else if (type == FL2D) {
				FLIntensity inten;
				inten.min = resultStat->minimum();
				inten.max = resultStat->maximum();
				resultData = std::shared_ptr<Data::FL2D>(new Data::FL2D(std::move(resultArr), curChannel, curChannelName, { dim[0], dim[1] }, { res[0], res[1] }, { -dim[0] * res[0] / 2, -dim[1] * res[1] / 2 }, inten, curOffset, curTimeStep));
			}
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		MaskVolume->unref();
		MaskVolume = nullptr;
	}

	auto ROICookieCutter::Impl::Cut3D() -> void {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.00001;
		};
		const auto reader = TC::IO::ROIReader(roiPath);
		const auto roiset = reader.Read();

		//Create empty mask
		auto buffer = new uint16_t[dim[0] * dim[1] * dim[2]]();

		SoVolumeData* MaskVolume = new SoVolumeData;
		MaskVolume->ref();
		MaskVolume->data.setValue(SbVec3i32(dim[0], dim[1], dim[2]), SbDataType::UNSIGNED_SHORT, 16, buffer, SoSFArray::NO_COPY_AND_DELETE);
		MaskVolume->extent.setValue(SbBox3f(-dim[0] * res[0] / 2, -dim[1] * res[1] / 2, -dim[2] * res[2] / 2, dim[0] * res[0] / 2, dim[1] * res[1] / 2, dim[2] * res[2] / 2));

		for (const auto roi : roiset.GetROIs()) {
			const auto [zmin, zmax] = roi.GetZRange();
			int editionId;

			std::vector<SbVec3f> topFace;
			std::vector<SbVec3f> bottomFace;
			std::vector<SbVec3f> totalVertices;
			const auto zPhyMin = zmin * res[2] - dim[2] * res[2] / 2 - res[2] / 2;
			const auto zPhyMax = zmax * res[2] - dim[2] * res[2] / 2 + res[2] / 2;
			for (const auto v : roi.GetVertices()) {
				auto [px, py, pz] = v;
				const auto topElement = SbVec3f(px, py, zPhyMax);
				topFace.push_back(topElement);
				totalVertices.push_back(topElement);
			}
			for (const auto v : roi.GetVertices()) {
				auto [px, py, pz] = v;
				const auto bottomElement = SbVec3f(px, py, zPhyMin);
				bottomFace.push_back(bottomElement);
				totalVertices.push_back(bottomElement);
			}

			//add TopFace
			std::vector<int> faceOrder;
			for (auto i = 0; i < topFace.size(); i++) {
				faceOrder.push_back(i);
			}
			faceOrder.push_back(-1);

			//Add surrounding squares
			for (auto i = 0; i < topFace.size() - 1; i++) {
				faceOrder.push_back(i);
				faceOrder.push_back(i + 1);
				faceOrder.push_back(i + 1 + topFace.size());
				faceOrder.push_back(i + topFace.size());
				faceOrder.push_back(-1);
			}
			//Add Final Square
			faceOrder.push_back(topFace.size() - 1);
			faceOrder.push_back(0);
			faceOrder.push_back(topFace.size());
			faceOrder.push_back(topFace.size() - 1 + topFace.size());
			faceOrder.push_back(-1);

			//add bottom Face
			for (auto i = 0; i < bottomFace.size(); i++) {
				faceOrder.push_back(i + topFace.size());
			}
			faceOrder.push_back(-1);

			SoVertexProperty* vp = new SoVertexProperty;
			vp->ref();
			vp->vertex.setValues(0, totalVertices.size(), totalVertices.data());

			SoIndexedFaceSet* faceSet = new SoIndexedFaceSet;
			faceSet->ref();
			faceSet->coordIndex.setValues(0, faceOrder.size(), faceOrder.data());
			faceSet->vertexProperty.setValue(vp);

			MaskVolume->startEditing(editionId);
			MaskVolume->editSolidShape(faceSet, 1);
			MaskVolume->finishEditing(editionId);

			faceSet->unref();
			faceSet = nullptr;
			vp->unref();
			vp = nullptr;
		}
		MaskVolume->saveEditing();

		try {
			//create mask binary mask view
			std::shared_ptr<iolink::ImageView> maskView { nullptr };
			auto spatialCalibrationProperty = SpatialCalibrationProperty(
																		{ 0, 0, 0 }, // origin
																		{ res[0], res[1], res[2] }	// spacing
																		);

			const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
			const VectorXu64 imageShape { static_cast<uint64_t>(dim[0]), static_cast<uint64_t>(dim[1]), static_cast<uint64_t>(dim[2]) };
			maskView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
			setDimensionalInterpretation(maskView, ImageTypeId::VOLUME);

			const RegionXu64 imageRegion { { 0, 0, 0 }, imageShape };
			maskView->writeRegion(imageRegion, MaskVolume->data.getValue());
			const auto imageStat = intensityStatistics(imageView, IntensityStatistics::MIN_MAX, { 0, 1 });

			const auto min = imageStat->minimum();

			const auto minImage = resetImage(imageView, min);

			const auto binaryImage = convertImage(maskView, ConvertImage::BINARY);
			const auto maskedImage = combineByMask(imageView, minImage, binaryImage);

			const auto resultImage = convertImage(maskedImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			const auto resultStat = intensityStatistics(resultImage, IntensityStatistics::MIN_MAX, { 0, 1 });

			std::unique_ptr<uint16_t[]> resultArr(new uint16_t[dim[0] * dim[1] * dim[2]](), std::default_delete<uint16_t[]>());
			memcpy(resultArr.get(), resultImage->buffer(), dim[0] * dim[1] * dim[2] * sizeof(uint16_t));

			resultData = nullptr;

			if (type == HT3D) {
				resultData = std::shared_ptr<Data::HT3D>(new Data::HT3D(std::move(resultArr), { dim[0], dim[1], dim[2] }, { res[0], res[1], res[2] }, { -dim[0] * res[0] / 2, -dim[1] * res[1] / 2, -dim[2] * res[2] / 2 }, { resultStat->minimum() / 10000.0, resultStat->maximum() / 10000.0 }, curTimeStep));
			} else if (type == FL3D) {
				FLIntensity inten;
				inten.min = resultStat->minimum();
				inten.max = resultStat->maximum();
				resultData = std::shared_ptr<Data::FL3D>(new Data::FL3D(std::move(resultArr), curChannel, curChannelName, { dim[0], dim[1], dim[2] }, { res[0], res[1], res[2] }, { -dim[0] * res[0] / 2, -dim[1] * res[1] / 2, -dim[2] * res[2] / 2 }, inten, curOffset, curTimeStep));
			}
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		MaskVolume->unref();
		MaskVolume = nullptr;
	}

	ROICookieCutter::ROICookieCutter(QWidget* parent) : QWidget(parent), IEditor(), d { new Impl } {
		SoQt::init(this);
	}

	ROICookieCutter::~ROICookieCutter() = default;

	auto ROICookieCutter::SetTitle(const QString& title) -> void {
		setWindowTitle(title);
	}

	auto ROICookieCutter::GetWindowList() const -> ViewList {
		return {};
	}

	auto ROICookieCutter::IsPrimary(const ViewPtr& window) const -> bool {
		return false;
	}

	auto ROICookieCutter::IsAcceptable(const DataPtr& data) const -> bool {
		return false;
	}

	auto ROICookieCutter::GetDataList() const -> QStringList {
		return {};
	}

	auto ROICookieCutter::GetData(const QString& name) const -> DataPtr {
		if (name == d->imageName) {
			return d->imageData;
		}
		if (name == d->pathName) {
			return d->pathData;
		}
		return {};
	}

	auto ROICookieCutter::GetName(const DataPtr& data) const -> QString {
		if (data == d->imageData) {
			return d->imageName;
		}
		if (data == d->pathData) {
			return d->pathName;
		}
		return {};
	}

	auto ROICookieCutter::AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool {
		if (portName == "List of Images") {
			d->curChannel = 0;
			d->curChannelName = "";
			d->curOffset = 0;
			d->curTimeStep = 0;
			d->imageData = nullptr;
			d->imageName = QString();
			d->dim[0] = d->dim[1] = d->dim[2] = 0;
			d->res[0] = d->res[1] = d->res[2] = 0;
			if (const auto ht2d = std::dynamic_pointer_cast<Data::HT2D>(data)) {
				const auto [ix,iy] = ht2d->GetSize();
				const auto [irx, iry] = ht2d->GetResolution();
				d->dim[0] = ix;
				d->dim[1] = iy;
				d->res[0] = irx;
				d->res[1] = iry;
				if (false == d->AddHT2D(ht2d)) {
					return false;
				}
				d->imageData = data;
				d->imageName = name;
			} else if (const auto ht3d = std::dynamic_pointer_cast<Data::HT3D>(data)) {
				const auto [ix, iy,iz] = ht3d->GetSize();
				const auto [irx, iry,irz] = ht3d->GetResolution();
				d->dim[0] = ix;
				d->dim[1] = iy;
				d->dim[2] = iz;
				d->res[0] = irx;
				d->res[1] = iry;
				d->res[2] = irz;
				if (false == d->AddHT3D(ht3d)) {
					return false;
				}
				d->imageData = data;
				d->imageName = name;
			} else if (const auto fl2d = std::dynamic_pointer_cast<Data::FL2D>(data)) {
				const auto [ix, iy] = fl2d->GetSize();
				const auto [irx, iry] = fl2d->GetResolution();
				d->dim[0] = ix;
				d->dim[1] = iy;
				d->res[0] = irx;
				d->res[1] = iry;
				if (false == d->AddFL2D(fl2d)) {
					return false;
				}
				d->imageData = data;
				d->imageName = name;
			} else if (const auto fl3d = std::dynamic_pointer_cast<Data::FL3D>(data)) {
				const auto [ix, iy, iz] = fl3d->GetSize();
				const auto [irx, iry, irz] = fl3d->GetResolution();
				d->dim[0] = ix;
				d->dim[1] = iy;
				d->dim[2] = iz;
				d->res[0] = irx;
				d->res[1] = iry;
				d->res[2] = irz;
				if (false == d->AddFL3D(fl3d)) {
					return false;
				}
				d->imageData = data;
				d->imageName = name;
			}
		}

		if (d->imageData && false == d->roiPath.isEmpty()) {
			d->PerformCookieCutter();
		}
		return true;
	}

	auto ROICookieCutter::RenameData(const DataPtr& data, const QString& name) -> bool {
		return false;
	}

	auto ROICookieCutter::RemoveData(const QString& name) -> void { }

	auto ROICookieCutter::ClearData() -> void { }

	auto ROICookieCutter::SetProperty(const QVariantMap& propMap) -> void {
		if (false == propMap.contains("ROIPath")) {
			d->roiPath = QString();
			return;
		}
		d->roiPath = propMap["ROIPath"].toString();
	}


	auto ROICookieCutter::GetResult(const QString& name) const -> DataPtr {
		return d->resultData;
	}
}
