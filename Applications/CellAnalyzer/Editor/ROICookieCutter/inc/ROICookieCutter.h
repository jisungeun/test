#pragma once

#include <QWidget>

#include "IEditor.h"

namespace CellAnalyzer::Editor::Simple {
	enum InputImageType {
		HT2D,
		HT3D,
		FL2D,
		FL3D,
		Unknown
	};

	class ROICookieCutter final : public QWidget, public IEditor {
	public:
		ROICookieCutter(QWidget* parent = nullptr);
		~ROICookieCutter() override;

		auto SetTitle(const QString& title) -> void override;

		auto GetWindowList() const -> ViewList override;
		auto IsPrimary(const ViewPtr& window) const -> bool override;

		auto IsAcceptable(const DataPtr& data) const -> bool override;
		auto GetDataList() const -> QStringList override;
		auto GetData(const QString& name) const -> DataPtr override;
		auto GetName(const DataPtr& data) const -> QString override;

		auto AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool override;
		auto RenameData(const DataPtr& data, const QString& name) -> bool override;
		auto RemoveData(const QString& name) -> void override;
		auto ClearData() -> void override;

		auto GetResult(const QString& name) const -> DataPtr override;
		auto SetProperty(const QVariantMap& propMap) -> void override;

	protected:
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	DECLARE_EDITOR_START(ROICookieCutter) {
		Q_OBJECT
		DECLARE_EDITOR_END(ROICookieCutter, Simple, DEFAULT_PATH)
	};
}
