#pragma once

#include "IEditor.h"
#include "IEditorType.h"

#include "CellAnalyzer.Editor.ServiceModelExport.h"

namespace CellAnalyzer::Editor {
	class CellAnalyzer_Editor_ServiceModel_API IEditorService : public virtual Tomocube::IService {
	public:
		virtual auto GetIdList() const -> QStringList = 0;
		virtual auto GetType(const QString& id) const -> EditorTypePtr = 0;

		virtual auto CreateEditor(const QString& id) -> EditorPtr = 0;

	protected:
		static auto GetLibMap() -> QMap<QString, QStringList>;
		static auto GetLibPath(const QString& category, const QString& Editor) -> QString;
	};
}
