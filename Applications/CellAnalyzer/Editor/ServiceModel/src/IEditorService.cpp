#include <QCoreApplication>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include "IEditorService.h"

namespace CellAnalyzer::Editor {
	auto IEditorService::GetLibMap() -> QMap<QString, QStringList> {
		if (QFile file(":/EditorList.json"); file.open(QIODevice::ReadOnly)) {
			QMap<QString, QStringList> libMap;
			const auto doc = QJsonDocument::fromJson(file.readAll());
			const auto obj = doc.object();
			const auto map = obj.toVariantMap();

			for (const auto& category : map.keys())
				libMap[category] = map[category].toStringList();

			return libMap;
		}
		
		return {}; 
	}

	auto IEditorService::GetLibPath(const QString& category, const QString& Editor) -> QString {
		const auto appPath = QCoreApplication::applicationDirPath();

		if (category.isEmpty())
			return QString("%1/Editor/CellAnalyzer.Editor.%2.dll").arg(appPath).arg(Editor);

		return QString("%1/Editor/CellAnalyzer.Editor.%2.%3.dll").arg(appPath).arg(category).arg(Editor);
	}
}
