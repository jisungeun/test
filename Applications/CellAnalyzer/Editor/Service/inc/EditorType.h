#pragma once

#include <QVariantMap>

#include "IEditorType.h"

#include "CellAnalyzer.Editor.ServiceExport.h"

namespace CellAnalyzer::Editor::Service {
	class CellAnalyzer_Editor_Service_API EditorType final : public IEditorType {
	public:
		explicit EditorType(const QVariantMap& info);
		~EditorType() override;

		auto IsValid() const -> bool;

		auto GetID() const -> QString override;
		auto GetCategory() const -> QString override;
		auto GetDataPort() const -> DataPort override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}