#pragma once

#include "IAppModule.h"
#include "IEditorService.h"

#include "CellAnalyzer.Editor.ServiceExport.h"

namespace CellAnalyzer::Editor::Service {
	class CellAnalyzer_Editor_Service_API EditorService final : public IAppModule, public IEditorService {
	public:
		EditorService();
		~EditorService() override;

		auto Start() -> std::optional<Error> override;
		auto Stop() -> void override;

		auto GetIdList() const -> QStringList override;
		auto GetType(const QString& id) const -> EditorTypePtr override;
		auto CreateEditor(const QString& id) -> EditorPtr override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
