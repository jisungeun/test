#include <QCoreApplication>
#include <QJsonDocument>
#include <QStringList>

#include "PluginCollection.h"

#include "EditorService.h"
#include "EditorType.h"

#include "IEditorFactory.h"

namespace CellAnalyzer::Editor::Service {
	struct EditorService::Impl {
		std::shared_ptr<Tomocube::IServiceProvider> provider = nullptr;
		QMap<QString, EditorTypePtr> typeMap;

		QList<EditorTypePtr> infoList;
	};

	EditorService::EditorService() : IAppModule(), IEditorService(), d(new Impl) { }

	EditorService::~EditorService() = default;

	auto EditorService::Start() -> std::optional<Error> {
		Tomocube::PluginInjection::PluginCollection collection;
		const auto map = GetLibMap();

		for (const auto& category : map.keys()) {
			for (const auto& process : map[category])
				collection.AddScoped<IEditorFactory>(GetLibPath(category, process));
		}

		d->provider = collection.BuildProvider();

		for (const auto& m : d->provider->GetMetadata<IEditorFactory>()) {
			if (const auto type = std::make_shared<EditorType>(m); type->IsValid())
				d->infoList.push_back(type);
		}

		return std::nullopt;
	}

	auto EditorService::Stop() -> void { }

	auto EditorService::GetIdList() const -> QStringList {
		QStringList list;

		for (const auto& i : d->infoList)
			list.push_back(i->GetID());

		return list;
	}

	auto EditorService::GetType(const QString& id) const -> EditorTypePtr {
		for (const auto& i : d->infoList) {
			if (i->GetID() == id)
				return i;
		}

		return {};
	}

	auto EditorService::CreateEditor(const QString& type) -> EditorPtr {
		for (int i = 0; i < d->infoList.count(); i++) {
			if (d->infoList[i]->GetID() == type) {
				if (const auto factory = d->provider->GetService<IEditorFactory>(i)) {
					return factory->CreateInstance();
				}
			}
		}

		return {};
	}
}
