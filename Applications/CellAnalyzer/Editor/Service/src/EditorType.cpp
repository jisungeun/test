#include "EditorType.h"

namespace CellAnalyzer::Editor::Service {
	struct EditorType::Impl {
		QString id;
		QString category;
		DataPort port;
	};

	EditorType::EditorType(const QVariantMap& info) : IEditorType(), d(new Impl) {
		d->id = info["ID"].toString();
		d->category = info["Category"].toString();
		const auto portList = info["Port"].toList();
		for(auto port : portList) {
			auto layer = false;
			if(port.toMap().contains("Layered")) {
				layer = port.toMap()["Layered"].toBool();
			}
			const auto dataflags = ToDataFlags(port.toMap()["DataFlags"].toStringList());
			const auto PortName = port.toMap()["Name"].toString();
			d->port[PortName] = std::make_tuple(dataflags,layer);
		}
	}

	EditorType::~EditorType() = default;

	auto EditorType::IsValid() const -> bool {
		return !d->id.isEmpty();
	}

	auto EditorType::GetID() const -> QString {
		return d->id;
	}
	auto EditorType::GetCategory() const -> QString {
		return d->category;
	}
	auto EditorType::GetDataPort() const -> DataPort {
		return d->port;
	}

}
