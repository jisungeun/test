#pragma once

#include <memory>

#include <QObject>

#include "IService.h"

#include "CellAnalyzer.Editor.EditorModelExport.h"

namespace CellAnalyzer::Editor {
	class IEditor;

	class CellAnalyzer_Editor_EditorModel_API IEditorFactory : public QObject, public virtual Tomocube::IService {
		Q_OBJECT

	public:
		virtual auto CreateInstance() -> std::shared_ptr<IEditor> = 0;
	};
}
