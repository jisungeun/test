#pragma once

#include "IData.h"

#include "CellAnalyzer.Editor.EditorModelExport.h"

namespace CellAnalyzer::Editor {	
	class IEditorType;
	using EditorTypePtr = std::shared_ptr<IEditorType>;
	using EditorTypeList = QVector<EditorTypePtr>;
	using DataPort = QMap<QString, std::tuple<DataFlags,bool>>;

	class CellAnalyzer_Editor_EditorModel_API IEditorType : public virtual Tomocube::IService {
	public:
		virtual auto GetID() const -> QString = 0;
		virtual auto GetCategory() const->QString = 0;
		virtual auto GetDataPort() const->DataPort = 0;
	};
}
