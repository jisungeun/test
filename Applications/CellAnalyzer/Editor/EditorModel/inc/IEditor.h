#pragma once

#include "IView.h"

#include "IEditorFactory.h"
#include "IEditorType.h"

#include "CellAnalyzer.Editor.EditorModelExport.h"

namespace CellAnalyzer::Editor {
	class IEditor;
	using EditorPtr = std::shared_ptr<IEditor>;
	using EditorList = QVector<EditorPtr>;

	class CellAnalyzer_Editor_EditorModel_API IEditor : public IView {
	public:
		virtual auto GetWindowList() const -> ViewList = 0;
		virtual auto IsPrimary(const ViewPtr& window) const -> bool = 0;
		virtual auto IsAcceptable(const DataPtr& data) const -> bool = 0;
		virtual auto SetTitle(const QString& title) -> void = 0;

		virtual auto GetDataList() const -> QStringList = 0;
		virtual auto GetData(const QString& name) const -> DataPtr = 0;
		virtual auto GetName(const DataPtr& data) const -> QString = 0;

		virtual auto AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool = 0;
		virtual auto RenameData(const DataPtr& data, const QString& name) -> bool = 0;
		virtual auto RemoveData(const QString& name) -> void = 0;
		virtual auto ClearData() -> void = 0;

		virtual auto GetResult(const QString& name) const -> DataPtr = 0;
		virtual auto SetProperty(const QVariantMap& propMap) -> void = 0;
	};
}

Q_DECLARE_INTERFACE(CellAnalyzer::Editor::IEditorFactory, "CellAnalyzer::Editor::IEditorFactory");

#define DEFAULT_PATH "../rsc/TypeInfo.json"

#define DECLARE_EDITOR_START(Name) \
	class Name##Factory final : public CellAnalyzer::Editor::IEditorFactory

#define DECLARE_EDITOR_END(Name, Category, TypeInfo) \
		Q_INTERFACES(CellAnalyzer::Editor::IEditorFactory)\
		Q_PLUGIN_METADATA(IID "CellAnalyzer::Editor::" #Category "::" #Name "Factory" FILE TypeInfo)\
	public:\
		auto CreateInstance() -> std::shared_ptr<IEditor> override {\
			return std::make_shared<CellAnalyzer::Editor::##Category##::##Name>();\
		}
