#include <QDesktopServices>
#include <QFileDialog>
#include <QMouseEvent>

#include "InitAnalysis.h"
#include "PipelineFinder.h"

#include "IAlertHandler.h"
#include "IDatabase.h"
#include "IExporterRepo.h"
#include "IHistoryRepo.h"
#include "IPipelineService.h"
#include "IPresenterRepo.h"
#include "IProject.h"
#include "IProjectRepo.h"
#include "IProjectService.h"
#include "ITaskRepo.h"

#include "ui_InitAnalysis.h"

namespace CellAnalyzer::Project::Analysis::View {
	struct InitAnalysis::Impl {
		Ui::InitAnalysis ui;
		Tomocube::IServiceProvider* provider = nullptr;
	};

	InitAnalysis::InitAnalysis(Tomocube::IServiceProvider* provider) : QDialog(nullptr, Qt::Dialog | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint), IView(), d(new Impl) {
		d->provider = provider;
		d->ui.setupUi(this);
		d->ui.createBtn->setVisible(false);
		d->ui.errorFrame->setVisible(false);
		d->ui.showBtn->setObjectName("quiet_color");
		d->ui.startBtn->setObjectName("accent_color");

		d->ui.tree->setColumnHidden(0, true);
		d->ui.tree->header()->setSectionResizeMode(1, QHeaderView::Stretch);
		d->ui.tree->header()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
		d->ui.tree->header()->setSectionResizeMode(3, QHeaderView::ResizeToContents);

		connect(d->ui.startBtn, &QPushButton::clicked, this, &InitAnalysis::OnStartBtnClicked);
		connect(d->ui.createBtn, &QPushButton::clicked, this, &InitAnalysis::OnCreateBtnClicked);
		connect(d->ui.urlBtn, &QPushButton::clicked, this, &InitAnalysis::OnUrlBtnClicked);
		connect(d->ui.showBtn, &QPushButton::clicked, this, &InitAnalysis::OnShowBtnClicked);
		connect(d->ui.pipBtn, &QPushButton::clicked, this, &InitAnalysis::OnPipBtnClicked);
		connect(d->ui.tree, &QTreeWidget::itemSelectionChanged, this, &InitAnalysis::OnTreeSelectionChanged);

		const auto database = provider->GetService<IDatabase>();
		const auto project = IDatabase::GetInstance()->GetMap(QString("Project\\Analysis"));

		if (const auto path = project.value_or(QVariantMap())["Path"].toString(); !path.isEmpty() && QDir(path).exists())
			SetPath(path);
		else {
			const auto service = provider->GetService<IProjectService>();
			const QDir dir(service->GetBasePath("Analysis"));
			dir.mkpath(".");
			SetPath(service->GetBasePath("Analysis"));
		}

		d->ui.tree->viewport()->installEventFilter(this);
	}

	InitAnalysis::~InitAnalysis() = default;

	auto InitAnalysis::GetUrl() const -> QString {
		const auto repo = d->provider->GetService<IProjectRepo>();
		return repo->GetLocation();
	}

	auto InitAnalysis::GetPath() const -> QString {
		const auto repo = d->provider->GetService<IProjectRepo>();
		return QFileInfo(repo->GetLocation()).absolutePath();
	}

	auto InitAnalysis::SetPath(const QString& path) -> void {
		OnCreateBtnClicked();

		const QDir dir(path);
		const auto list = dir.entryList({ path }, QDir::AllDirs | QDir::NoDotAndDotDot);
		const auto repo = d->provider->GetService<IProjectRepo>();

		d->ui.urlText->setText(path);
		d->ui.tree->clear();

		for (const auto& l : list) {
			if (repo->Open(dir.filePath(l)))
				d->ui.tree->addTopLevelItem(new QTreeWidgetItem(d->ui.tree, { l, repo->GetName(), repo->GetUser(), repo->GetCreationDateTime().toString("yyyy/MM/dd HH:mm:ss") }));
		}
	}

	auto InitAnalysis::OnStartBtnClicked() -> void {
		const QDir dir(d->ui.urlText->text());

		if (d->ui.nameText->text().isEmpty()) {
			d->ui.errorFrame->setVisible(true);
			d->ui.errorLabel->setText("Pipeline name cannot be empty");
		} else {
			const auto repo = d->provider->GetService<IProjectRepo>();
			const auto pRepo = d->provider->GetService<IPresenterRepo>();
			const auto task = d->provider->GetService<ITaskRepo>();
			const auto exp = d->provider->GetService<IExporterRepo>();
			const auto history = d->provider->GetService<IHistoryRepo>();
			const auto service = d->provider->GetService<Pipeline::IPipelineService>();

			if (const auto item = d->ui.tree->currentItem()) {
				if (const auto path = dir.filePath(item->text(0)); !repo->Open(path)) {
					d->ui.errorFrame->setVisible(true);
					d->ui.errorLabel->setText("Project could not be opened");
				} else {
					const auto database = d->provider->GetService<IDatabase>();
					QVariantMap map;
					map["Path"] = dir.absolutePath();
					database->Save("Project\\Analysis", map);

					accept();
					close();
				}
			} else {
				const auto path = dir.filePath(d->ui.nameText->text());
				const auto pip = service->Read(d->ui.pipText->text());

				if (!pip) {
					d->ui.errorFrame->setVisible(true);
					d->ui.errorLabel->setText("Pipeline could not be found");
					return;
				}

				if (repo->Create(path, d->ui.nameText->text()) && repo->Open(path)) {
					repo->SetUser(d->ui.userText->text());
					repo->SetDescription(d->ui.descText->toPlainText());
					repo->Save();
					task->SetPipeline(pip);
					task->Save();

					const auto database = d->provider->GetService<IDatabase>();
					QVariantMap map;
					map["Path"] = dir.absolutePath();
					database->Save("Project\\Analysis", map);
					pRepo->SetPipeline(pip);

					accept();
					close();
				} else {
					d->ui.errorFrame->setVisible(true);
					d->ui.errorLabel->setText("Project could not be created");
				}
			}
		}
	}

	auto InitAnalysis::OnCreateBtnClicked() -> void {
		d->ui.tree->clearSelection();
		d->ui.tree->setCurrentItem(nullptr);
	}

	auto InitAnalysis::OnUrlBtnClicked() -> void {
		if (const auto path = QFileDialog::getExistingDirectory(this, "Find Path", d->ui.urlText->text()); !path.isEmpty())
			SetPath(path);
	}

	auto InitAnalysis::OnShowBtnClicked() -> void {
		QDesktopServices::openUrl(QUrl::fromLocalFile(d->ui.urlText->text()));
	}

	auto InitAnalysis::OnPipBtnClicked() -> void {
		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto proj = d->provider->GetService<IProjectRepo>();
		const auto finder = std::make_shared<PipelineFinder>(d->provider);

		if (handler->ShowDialog(finder) == Accepted)
			d->ui.pipText->setText(finder->GetUrl());
	}

	auto InitAnalysis::OnTreeSelectionChanged() -> void {
		if (const auto items = d->ui.tree->selectedItems(); !items.isEmpty()) {
			if (d->ui.startBtn->text() == "Create" && (!d->ui.nameText->text().isEmpty() || !d->ui.descText->toPlainText().isEmpty() || !d->ui.pipText->text().isEmpty() && !d->ui.userText->text().isEmpty())) {
				if (const auto alert = d->provider->GetService<IAlertHandler>()) {
					if (!alert->ShowYesNo("Select existing project", "Are you sure to discard the current content of the new project?")) {
						d->ui.tree->clearSelection();
						d->ui.tree->setCurrentItem(nullptr);
						return;
					}
				}
			}

			d->ui.errorFrame->setVisible(false);
			d->ui.createBtn->setVisible(true);
			d->ui.nameText->setReadOnly(true);
			d->ui.userText->setReadOnly(true);
			d->ui.descText->setReadOnly(true);
			d->ui.pipBtn->setEnabled(false);
			d->ui.pipText->setReadOnly(true);
			d->ui.startBtn->setText("Open");

			const auto repo = d->provider->GetService<IProjectRepo>();
			const auto task = d->provider->GetService<ITaskRepo>();

			if (repo->Open(QDir(d->ui.urlText->text()).filePath(items.first()->text(0)))) {
				d->ui.nameText->setText(repo->GetName());
				d->ui.userText->setText(repo->GetUser());
				d->ui.descText->setText(repo->GetDescription());

				if (const auto pip = task->GetPipeline())
					d->ui.pipText->setText(pip->GetLocation());
				else
					d->ui.pipText->clear();
			} else {
				d->ui.nameText->clear();
				d->ui.userText->clear();
				d->ui.descText->clear();
				d->ui.pipText->clear();
			}
		} else if (d->ui.startBtn->text() != "Create") {
			d->ui.errorFrame->setVisible(false);
			d->ui.nameText->clear();
			d->ui.nameText->setReadOnly(false);
			d->ui.userText->clear();
			d->ui.userText->setReadOnly(false);
			d->ui.descText->clear();
			d->ui.descText->setReadOnly(false);
			d->ui.pipText->clear();
			d->ui.pipText->setReadOnly(false);
			d->ui.pipBtn->setEnabled(true);
			d->ui.startBtn->setText("Create");
			d->ui.createBtn->setVisible(false);
		}
	}

	auto InitAnalysis::eventFilter(QObject* object, QEvent* event) -> bool {
		if (event->type() == QEvent::MouseButtonRelease) {
			if (const auto* arg = dynamic_cast<QMouseEvent*>(event)) {
				if (arg->button() == Qt::LeftButton && d->ui.tree->itemAt(arg->pos()) == nullptr) {
					d->ui.tree->clearSelection();
					d->ui.tree->setCurrentItem(nullptr);
				}
			}
		}

		return QDialog::eventFilter(object, event);
	}
}
