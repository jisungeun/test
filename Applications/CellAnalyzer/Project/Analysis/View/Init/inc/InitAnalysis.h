#pragma once

#include <QDialog>
#include <QTreeWidgetItem>

#include "IServiceProvider.h"
#include "IView.h"

#include "CellAnalyzer.Project.Analysis.View.InitExport.h"

namespace CellAnalyzer::Project::Analysis::View {
	class CellAnalyzer_Project_Analysis_View_Init_API InitAnalysis final : public QDialog, public IView {
	public:
		explicit InitAnalysis(Tomocube::IServiceProvider* provider);
		~InitAnalysis() override;

		auto GetUrl() const -> QString;
		auto GetPath() const -> QString;
		auto SetPath(const QString& path) -> void;

	protected slots:
		auto OnStartBtnClicked() -> void;
		auto OnCreateBtnClicked() -> void;
		auto OnUrlBtnClicked() -> void;
		auto OnShowBtnClicked() -> void;
		auto OnPipBtnClicked() -> void;
		auto OnTreeSelectionChanged() -> void;

	protected:
		auto eventFilter(QObject*, QEvent*) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
