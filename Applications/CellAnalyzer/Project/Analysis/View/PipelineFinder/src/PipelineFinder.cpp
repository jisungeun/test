#include <QFileDialog>

#include "PipelineFinder.h"

#include "IDatabase.h"
#include "IPipelineService.h"
#include "IProject.h"
#include "IProjectService.h"

#include "ui_PipelineFinder.h"

namespace CellAnalyzer::Project::Analysis::View {
	struct PipelineFinder::Impl {
		Ui::PipelineFinder ui;
		Tomocube::IServiceProvider* provider = nullptr;
		QString url;
	};

	PipelineFinder::PipelineFinder(Tomocube::IServiceProvider* provider) : QDialog(nullptr, Qt::Dialog | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint), IView(), d(new Impl) {
		d->provider = provider;
		d->ui.setupUi(this);
		d->ui.errorFrame->setVisible(false);
		d->ui.startBtn->setObjectName("accent_color");

		d->ui.tree->setColumnHidden(0, true);
		d->ui.tree->header()->setSectionResizeMode(1, QHeaderView::Stretch);
		d->ui.tree->header()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
		d->ui.tree->header()->setSectionResizeMode(3, QHeaderView::ResizeToContents);

		connect(d->ui.startBtn, &QPushButton::clicked, this, &PipelineFinder::OnStartBtnClicked);
		connect(d->ui.urlBtn, &QPushButton::clicked, this, &PipelineFinder::OnUrlBtnClicked);
		connect(d->ui.tree, &QTreeWidget::currentItemChanged, this, &PipelineFinder::OnTreeSelectionChanged);

		const auto database = provider->GetService<IDatabase>();
		const auto project = IDatabase::GetInstance()->GetMap(QString("Project\\Pipeline"));

		if (const auto path = project.value_or(QVariantMap())["Path"].toString(); !path.isEmpty() && QDir(path).exists())
			SetPath(path);
		else {
			const auto service = provider->GetService<IProjectService>();
			SetPath(service->GetBasePath("Pipeline"));
		}
	}

	PipelineFinder::~PipelineFinder() = default;

	auto PipelineFinder::GetUrl() const -> QString {
		return d->url;
	}

	auto PipelineFinder::GetPath() const -> QString {
		return QFileInfo(d->url).absolutePath();
	}

	auto PipelineFinder::SetPath(const QString& path) -> void {
		d->ui.urlText->setText(path);
		d->ui.tree->clear();

		const QDir dir(path);
		const auto list = dir.entryList({ "*.tcap" });
		const auto service = d->provider->GetService<Pipeline::IPipelineService>();

		for (const auto& l : list) {
			if (const auto pipeline = service->Read(dir.filePath(l)))
				d->ui.tree->addTopLevelItem(new QTreeWidgetItem(d->ui.tree, { l, pipeline->GetName(), pipeline->GetAuthor(), pipeline->GetCreationDateTime().toString("yyyy/MM/dd HH:mm:ss") }));
		}
	}

	auto PipelineFinder::OnStartBtnClicked() -> void {
		const auto service = d->provider->GetService<Pipeline::IPipelineService>();

		if (const auto pipeline = service->Read(d->url)) {
			d->url = pipeline->GetLocation();

			const auto database = d->provider->GetService<IDatabase>();
			QVariantMap map;
			map["Path"] = d->ui.urlText->text();
			database->Save("Project\\Pipeline", map);

			accept();
			close();
		}

		d->ui.errorFrame->setVisible(true);
		d->ui.errorLabel->setText("Pipeline could not be found");
	}

	auto PipelineFinder::OnUrlBtnClicked() -> void {
		if (const auto path = QFileDialog::getExistingDirectory(this, "Select the location for pipelines", d->ui.urlText->text()); !path.isEmpty()) {
			d->ui.urlText->setText(path);
			SetPath(path);
		}
	}

	auto PipelineFinder::OnTreeSelectionChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous) -> void {
		if (current) {
			const auto service = d->provider->GetService<Pipeline::IPipelineService>();
			const auto url = QDir(d->ui.urlText->text()).filePath(current->text(0));

			if (const auto pipeline = service->Read(url)) {
				d->ui.nameText->setText(pipeline->GetName());
				d->ui.authorText->setText(pipeline->GetAuthor());
				d->ui.descText->setText(pipeline->GetDescription());
				d->url = url;
				d->ui.startBtn->setEnabled(true);
			}
		} else {
			d->url.clear();
			d->ui.startBtn->setEnabled(false);
		}
	}
}
