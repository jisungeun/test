#pragma once

#include <QDialog>
#include <QTreeWidgetItem>

#include "IServiceProvider.h"
#include "IView.h"

#include "CellAnalyzer.Project.Analysis.View.PipelineFinderExport.h"

namespace CellAnalyzer::Project::Analysis::View {
	class CellAnalyzer_Project_Analysis_View_PipelineFinder_API PipelineFinder final : public QDialog, public IView {
	public:
		explicit PipelineFinder(Tomocube::IServiceProvider* provider);
		~PipelineFinder() override;

		auto GetUrl() const -> QString;
		auto GetPath() const -> QString;
		auto SetPath(const QString& path) -> void;

	protected slots:
		auto OnStartBtnClicked() -> void;
		auto OnUrlBtnClicked() -> void;
		auto OnTreeSelectionChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
