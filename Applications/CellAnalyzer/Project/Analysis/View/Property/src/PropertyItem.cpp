#include "PropertyItem.h"

#include "AttrWidget.h"
#include "IView.h"

namespace CellAnalyzer::Project::Analysis::View {
	struct PropertyItem::Impl {
		Pipeline::Widget::AttrWidget* attr = nullptr;
		Pipeline::AttrCategory category = Pipeline::AttrCategory::Null;
		QString group;

		Pipeline::AttrValue value;
		Pipeline::AttrModel model;
		Pipeline::AttrState state = Pipeline::AttrState::Enabled;
		QString description;
	};

	PropertyItem::PropertyItem(const QString& text, const QString& group, Pipeline::AttrCategory category) : std::enable_shared_from_this<PropertyItem>(), QTreeWidgetItem({ text }), IPropertyItem(), d(new Impl) {
		d->group = group;
		d->category = category;
	}

	PropertyItem::~PropertyItem() = default;

	auto PropertyItem::IsExecutable() const -> bool {
		return d->value != d->attr->GetValue();
	}

	auto PropertyItem::Discard() -> void {
		if (d->attr) {
			d->attr->SetValue(d->value);
			emit d->attr->ValueChanged(d->value);
		}
	}

	auto PropertyItem::GetCategory() const -> Pipeline::AttrCategory {
		return d->category;
	}

	auto PropertyItem::SetAttrWidget(Pipeline::Widget::AttrWidget* widget) -> void {
		widget->SetModel(d->model);
		widget->SetValue(d->value);

		switch (d->state) {
			case Pipeline::AttrState::Enabled:
				widget->setEnabled(true);
				setHidden(false);
				break;
			case Pipeline::AttrState::Disabled:
				widget->setEnabled(false);
				setHidden(false);
				break;
			case Pipeline::AttrState::Hidden:
				setHidden(true);
				break;
			default: ;
		}

		d->attr = widget;
	}

	auto PropertyItem::GetName() const -> QString {
		return text(0);
	}

	auto PropertyItem::GetGroup() const -> QString {
		return d->group;
	}

	auto PropertyItem::GetValue() const -> Pipeline::AttrValue {
		return d->attr->GetValue();
	}

	auto PropertyItem::GetDescription() const -> QString {
		return d->description;
	}

	auto PropertyItem::SetDescription(const QString& desc) -> void {
		IView::Run([parent = std::weak_ptr(shared_from_this()), desc] {
			if (const auto p = parent.lock())
				p->d->description = desc;
		});
	}

	auto PropertyItem::SetValue(const Pipeline::AttrValue& value) -> void {
		IView::Run([parent = std::weak_ptr(shared_from_this()), value] {
			if (const auto p = parent.lock()) {
				p->d->value = value;

				if (p->d->attr)
					p->d->attr->SetValue(value);
			}
		});
	}

	auto PropertyItem::SetModel(const Pipeline::AttrModel& model) -> void {
		IView::Run([parent = std::weak_ptr(shared_from_this()), model] {
			if (const auto p = parent.lock()) {
				p->d->model = model;

				if (p->d->attr)
					p->d->attr->SetModel(model);
			}
		});
	}

	auto PropertyItem::SetState(Pipeline::AttrState state) -> void {
		IView::Run([parent = std::weak_ptr(shared_from_this()), state] {
			if (const auto p = parent.lock()) {
				p->d->state = state;

				if (p->d->attr) {
					switch (state) {
						case Pipeline::AttrState::Enabled:
							p->d->attr->setEnabled(true);
							p->setHidden(false);
							break;
						case Pipeline::AttrState::Disabled:
							p->d->attr->setEnabled(false);
							p->setHidden(false);
							break;
						case Pipeline::AttrState::Hidden:
							p->setHidden(true);
							break;
						default: ;
					}
				}
			}
		});
	}
}
