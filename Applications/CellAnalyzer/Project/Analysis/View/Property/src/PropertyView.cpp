#include <optional>

#include <QLabel>

#include "PropertyView.h"

#include "AttrWidget.h"
#include "IPropertyViewEvent.h"
#include "PropertyItem.h"

#include "ui_PropertyView.h"

namespace CellAnalyzer::Project::Analysis::View {
	struct PropertyView::Impl {
		Ui::PropertyView ui;
		Tomocube::IServiceProvider* provider = nullptr;
		QVector<std::shared_ptr<IPropertyViewEvent>> events;

		Session session = 0;

		std::optional<bool> abortable;
		std::optional<bool> executable;
		std::optional<bool> savable;

		bool executing = false;

		QMap<QString, std::shared_ptr<QTreeWidgetItem>> groupMap;
		QMap<QString, std::shared_ptr<PropertyItem>> attrMap;

		auto UpdateExecutable() const -> void;
	};

	auto PropertyView::Impl::UpdateExecutable() const -> void {
		auto exec = false;

		for (const auto& a : attrMap.values()) {
			exec = a->IsExecutable();

			if (exec)
				break;
		}

		ui.executeBtn->setEnabled(executable.value_or(exec));
		ui.discardBtn->setEnabled(exec);
		ui.saveBtn->setEnabled(exec);
		ui.actionFrame->setVisible(executable.value_or(exec) && !executing);
	}

	PropertyView::PropertyView(Tomocube::IServiceProvider* provider) : QWidget(), IPropertyView(), d(new Impl) {
		d->provider = provider;
		d->ui.setupUi(this);
		d->ui.abortFrame->setVisible(false);
		d->ui.actionFrame->setVisible(false);
		d->ui.executeBtn->setObjectName("accent_color");
		d->ui.tree->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
		d->ui.tree->header()->setSectionResizeMode(1, QHeaderView::Fixed);
		d->ui.tree->header()->setSectionResizeMode(2, QHeaderView::Stretch);
		d->ui.tree->setColumnWidth(1, 12 + 10 /* icon size + right padding(10px) */);

		connect(d->ui.abortBtn, &QPushButton::clicked, this, &PropertyView::OnAbortBtnClicked);
		connect(d->ui.executeBtn, &QPushButton::clicked, this, &PropertyView::OnExecuteBtnClicked);
		connect(d->ui.discardBtn, &QPushButton::clicked, this, &PropertyView::OnDiscardBtnClicked);
		connect(d->ui.saveBtn, &QPushButton::clicked, this, &PropertyView::OnSaveBtnClicked);
	}

	PropertyView::~PropertyView() = default;

	auto PropertyView::AddEvent(const std::shared_ptr<IPropertyViewEvent>& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto PropertyView::RemoveEvent(const std::shared_ptr<IPropertyViewEvent>& event) -> void {
		d->events.removeOne(event);
	}

	auto PropertyView::CreateSession() -> Session {
		Session session = 0;

		Run([this, &session] {
			d->attrMap.clear();
			d->groupMap.clear();
			d->executable = std::nullopt;
			d->abortable = std::nullopt;
			d->savable = std::nullopt;
			d->ui.actionFrame->setVisible(false);
			d->ui.saveBtn->setVisible(false);
			d->UpdateExecutable();

			session = ++d->session;
		}, RunType::Blocking);

		return session;
	}

	auto PropertyView::GetSession() const -> Session {
		return d->session;
	}

	auto PropertyView::GetPropertyList(Session session) const -> PropertyList {
		PropertyList list;

		Run([this, &list, session] {
			if (d->session != session)
				return;

			for (const auto& p : d->attrMap.values())
				list.push_back(p);
		}, RunType::Blocking);

		return list;
	}

	auto PropertyView::GetProperty(Session session, const QString& name) const -> PropertyPtr {
		PropertyPtr property = nullptr;

		Run([this, &property, session, name] {
			if (d->session != session)
				return;

			if (d->attrMap.contains(name))
				property = d->attrMap[name];
		}, RunType::Blocking);

		return property;
	}

	auto PropertyView::CreateProperty(const QString& group, const QString& name, Pipeline::AttrCategory category) -> PropertyPtr {
		std::shared_ptr<PropertyItem> property = nullptr;

		Run([this, &property, group, name, category] {
			property = std::make_shared<PropertyItem>(name, group, category);
			d->ui.executeBtn->setText("Execute");
			d->ui.discardBtn->setVisible(true);
		}, RunType::Blocking);

		return property;
	}

	auto PropertyView::AddProperty(Session session, PropertyPtr&& property) -> void {
		Run([this, session, property = std::move(property)] {
			if (d->session != session)
				return;

			const auto prop = std::dynamic_pointer_cast<PropertyItem>(property);
			const auto group = property->GetGroup();
			const auto name = property->GetName();

			if (!d->groupMap.contains(group)) {
				d->groupMap[group] = std::make_shared<QTreeWidgetItem>(d->ui.tree, QStringList { group });
				d->groupMap[group]->setExpanded(true);
			}

			d->attrMap[name] = prop;
			d->groupMap[group]->addChild(prop.get());

			auto* attr = new Pipeline::Widget::AttrWidget(prop->GetCategory(), this);
			prop->SetAttrWidget(attr);

			if (const auto description = property->GetDescription(); !description.isEmpty()) {
				const auto infoLabel = new QLabel;
				infoLabel->setPixmap(QIcon(":/Flat/Question.svg").pixmap(12, 12));
				infoLabel->setToolTip(description);
				d->ui.tree->setItemWidget(prop.get(), 1, infoLabel);
			}

			d->ui.tree->setItemWidget(prop.get(), 2, attr);

			connect(attr, &Pipeline::Widget::AttrWidget::ValueChanged, this, [this, session, name](const Pipeline::AttrValue& value) {
				if (d->session == session && d->attrMap.contains(name))
					d->UpdateExecutable();

				RunAsync([this, session, name, value] {
					for (const auto& e : d->events)
						e->OnPropertyChanged(session, name, value);
				});
			});
		});
	}

	auto PropertyView::RemoveProperty(Session session, const QString& name) -> void {
		if (session != d->session)
			return;

		d->attrMap.remove(name);
	}

	auto PropertyView::SetExecutable(Session session, bool executable) -> void {
		Run([this, session, executable] {
			if (d->session != session)
				return;

			d->ui.actionFrame->setVisible(!d->executing);
			d->ui.executeBtn->setEnabled(executable);
			d->executable = executable;
		});
	}

	auto PropertyView::SetAbortable(Session session, bool abortable) -> void {
		Run([this, session, abortable] {
			if (d->session != session)
				return;

			d->abortable = abortable;
		});
	}

	auto PropertyView::SetDiscardable(Session session, bool visible) -> void {
		Run([this, session, visible] {
			if (d->session != session)
				return;

			d->ui.discardBtn->setVisible(visible);
		});
	}

	auto PropertyView::SetSavable(Session session, bool savable) -> void {
		Run([this, session, savable] {
			if (d->session != session)
				return;
			
			d->ui.actionFrame->setVisible(!d->executing);
			d->ui.saveBtn->setVisible(savable);
			d->savable = savable;
		});
	}

	auto PropertyView::SetExecuteText(Session session, const QString& text) -> void {
		Run([this, session, text] {
			if (d->session != session)
				return;

			d->ui.executeBtn->setText(text);
		});
	}

	auto PropertyView::Execute() -> void {
		Run([this] {
			if (!d->executing)
				d->ui.executeBtn->click();
		});
	}

	auto PropertyView::OnAbortBtnClicked() -> void {
		d->ui.abortFrame->setEnabled(false);

		RunAsync([this] {
			for (const auto& e : d->events)
				e->OnAborted();
			
			Run([this] {
				d->UpdateExecutable();
			});
		});
	}

	auto PropertyView::OnExecuteBtnClicked() -> void {
		d->executing = true;
		d->ui.actionFrame->setEnabled(false);

		if (d->abortable.value_or(false)) {
			d->ui.abortFrame->setVisible(true);
			d->ui.actionFrame->setVisible(false);
		}

		RunAsync([this, session = d->session] {
			for (const auto& e : d->events)
				e->OnExecuted(session);

			Run([this] {
				for (const auto& a : d->attrMap.values())
					a->SetValue(a->GetValue());

				d->UpdateExecutable();
				d->ui.actionFrame->setEnabled(true);
				d->ui.actionFrame->setVisible(true);
				d->ui.abortFrame->setVisible(false);
				d->ui.abortFrame->setEnabled(true);
				d->executing = false;
			});
		});
	}

	auto PropertyView::OnDiscardBtnClicked() -> void {
		d->ui.actionFrame->setEnabled(false);

		for (const auto& i : d->attrMap.values())
			i->Discard();

		RunAsync([this, session = d->session] {
			for (const auto& e : d->events)
				e->OnDiscarded(session);

			Run([this] {
				d->UpdateExecutable();
				d->ui.actionFrame->setEnabled(true);
			});
		});
	}

	auto PropertyView::OnSaveBtnClicked() -> void {
		d->ui.actionFrame->setEnabled(false);

		RunAsync([this] {
			for (const auto& e : d->events)
				e->OnSaved(d->session);

			Run([this] {
				for (const auto& a : d->attrMap.values())
					a->SetValue(a->GetValue());

				d->ui.actionFrame->setEnabled(true);

				Run([this] {
					d->UpdateExecutable();
				});
			});
		});
	}
}
