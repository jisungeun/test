#pragma once

#include <QWidget>

#include "IServiceProvider.h"

#include "IPropertyView.h"

#include "CellAnalyzer.Project.Analysis.View.PropertyExport.h"

namespace CellAnalyzer::Project::Analysis::View {
	class CellAnalyzer_Project_Analysis_View_Property_API PropertyView final : public QWidget, public IPropertyView {
	public:
		explicit PropertyView(Tomocube::IServiceProvider* provider);
		~PropertyView() override;

		auto AddEvent(const std::shared_ptr<IPropertyViewEvent>& event) -> void override;
		auto RemoveEvent(const std::shared_ptr<IPropertyViewEvent>& event) -> void override;

		auto CreateSession() -> Session override;
		auto GetSession() const -> Session override;

		auto GetPropertyList(Session session) const -> PropertyList override;
		auto GetProperty(Session session, const QString& name) const -> PropertyPtr override;

		auto CreateProperty(const QString& group, const QString& name, Pipeline::AttrCategory category) -> PropertyPtr override;
		auto AddProperty(Session session, PropertyPtr&& property) -> void override;
		auto RemoveProperty(Session session, const QString& name) -> void override;

		auto SetExecutable(Session session, bool executable) -> void override;
		auto SetAbortable(Session session, bool abortable) -> void override;
		auto SetDiscardable(Session session, bool visible) -> void override;
		auto SetSavable(Session session, bool savable) -> void override;
		auto SetExecuteText(Session session, const QString& text) -> void override;

		auto Execute() -> void override;

	protected slots:
		auto OnAbortBtnClicked() -> void;
		auto OnExecuteBtnClicked() -> void;
		auto OnDiscardBtnClicked() -> void;
		auto OnSaveBtnClicked() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
