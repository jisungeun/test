#include <QDesktopServices>
#include <QDropEvent>
#include <QFileDialog>
#include <QMenu>
#include <QSettings>
#include <QStack>
#include <QThread>

#include <RoiEditorDef.h>
#include <RoiEditorDialog.h>
#include <WorksetEditorDialog.h>
#include <MaskEditorDialog.h>

#include "IAlertHandler.h"
#include "IExporterRepo.h"
#include "IExportService.h"
#include "IHistoryRepo.h"
#include "IImportService.h"
#include "IPresenterRepo.h"
#include "IProjectRepo.h"
#include "IPropertyView.h"
#include "ITaskRepo.h"

#include "ProjectView.h"
#include "ExporterFinder.h"
#include "PipelineFinder.h"
#include "PresenterDialog.h"
#include "ROIWriter.h"
#include "TCFDialog.h"

#include "ui_ProjectView.h"

namespace CellAnalyzer::Project::Analysis::View {
	struct ProjectView::Impl {
		Ui::ProjectView ui;
		Tomocube::IServiceProvider* provider = nullptr;
		QVector<std::shared_ptr<IProjectViewEvent>> events;

		QTreeWidgetItem* project = nullptr;
		QTreeWidgetItem* pipeline = nullptr;

		static auto ClearChildren(QTreeWidgetItem* item) -> void;
		static auto GetExpanded(const QTreeWidgetItem* item) -> QStringList;
		static auto GetExpanded(const QTreeWidget* widget) -> QStringList;
		static auto GetCategory(const QTreeWidgetItem* item) -> SelectionCategory;
		static auto ToTSModality(TaskType type) -> Project::View::WorksetEditor::Modality;
		static auto ToDataType(Project::View::WorksetEditor::Modality modality) -> TaskType;

		auto GetName(const QTreeWidgetItem* item) const -> QString;
		auto GetFileGroupName(const QString& base, const QTreeWidgetItem* original = nullptr) const -> QString;
		auto RemoveROI(const QString& filepath) -> void;
		auto RemoveROI(const QString& filepath, int timePoint) -> void;

		auto AddDataToDefaultPresenter(const DataPtr& processData, const QString& dataID)->void;
	};

	auto ProjectView::Impl::AddDataToDefaultPresenter(const DataPtr& processData, const QString& dataID) -> void {
		if (processData) {
			const auto presenterRepo = provider->GetService<IPresenterRepo>();
			const auto defaultRender = presenterRepo->GetDefaultPresenter();
			QMetaObject::invokeMethod(qApp, [this, dataID, processData, defaultRender] {
				defaultRender->AddData(dataID, processData, "List of Images");
				});
		}
	}
	
	auto ProjectView::Impl::RemoveROI(const QString& filepath) -> void {
		const auto taskRepo = provider->GetService<ITaskRepo>();
		const auto file = taskRepo->GetFile(filepath);
		if (!file) {
			return;
		}
		const auto projRepo = provider->GetService<IProjectRepo>();
		const auto roiPath = QDir(projRepo->GetLocation()).filePath("ROI");

		QStringList roiNameCandidates;
		const auto filename = file->GetName();
		for (auto& tp : file->GetTimePointList()) {
			const auto entry = QString("%1_%2.tcroi").arg(filename).arg(tp);
			roiNameCandidates.push_back(entry);
		}

		QDir roiDir(roiPath);
		if (false == roiDir.exists()) {
			return;
		}
		QStringList filters;
		filters << "*.tcroi";
		roiDir.setNameFilters(filters);
		QFileInfoList list = roiDir.entryInfoList();
		for (const auto l : list) {
			if (roiNameCandidates.contains(l.fileName())) {
				QFile::remove(l.filePath());
			}
		}
	}

	auto ProjectView::Impl::RemoveROI(const QString& filepath, int timePoint) -> void {
		const auto taskRepo = provider->GetService<ITaskRepo>();
		const auto file = taskRepo->GetFile(filepath);
		if (!file) {
			return;
		}
		const auto projRepo = provider->GetService<IProjectRepo>();
		const auto roiPath = QDir(projRepo->GetLocation()).filePath("ROI");

		const auto roiFileName = QString("%1_%2.tcroi").arg(file->GetName()).arg(timePoint);

		QDir roiDir(roiPath);
		if (false == roiDir.exists()) {
			return;
		}

		QStringList filters;
		filters << "*.tcroi";
		roiDir.setNameFilters(filters);
		QFileInfoList list = roiDir.entryInfoList();
		for (const auto l : list) {
			if (roiFileName == l.fileName()) {
				QFile::remove(l.filePath());
				break;
			}
		}
	}

	auto ProjectView::Impl::ClearChildren(QTreeWidgetItem* item) -> void {
		qDeleteAll(item->takeChildren());
	}

	auto ProjectView::Impl::GetExpanded(const QTreeWidgetItem* item) -> QStringList {
		QStringList list;
		QStack<const QTreeWidgetItem*> stack;
		stack.push(item);

		while (!stack.isEmpty()) {
			const auto* child = stack.pop();

			for (auto i = 0; i < child->childCount(); i++) {
				if (const auto* ch = child->child(i); ch->isExpanded()) {
					list.push_back(ch->text(0));
				} else if (ch->childCount() > 0)
					stack.push(ch);
			}
		}

		return list;
	}

	auto ProjectView::Impl::GetExpanded(const QTreeWidget* widget) -> QStringList {
		QStringList list;
		QStack<const QTreeWidgetItem*> stack;

		for (auto i = 0; i < widget->topLevelItemCount(); i++)
			stack.push(widget->topLevelItem(i));

		while (!stack.isEmpty()) {
			const auto* child = stack.pop();

			if (child->isExpanded())
				list.push_back(child->text(0));

			for (auto i = 0; i < child->childCount(); i++) {
				if (const auto* ch = child->child(i); ch->childCount() > 0)
					stack.push(ch);
			}
		}

		return list;
	}

	auto ProjectView::Impl::ToTSModality(TaskType type) -> Project::View::WorksetEditor::Modality {
		if (type == TaskType::HT2D)
			return Project::View::WorksetEditor::Modality::Ht2d;
		if (type == TaskType::HT3D)
			return Project::View::WorksetEditor::Modality::Ht3d;
		if (type == TaskType::FL2DCH0)
			return Project::View::WorksetEditor::Modality::Fl2dCh1;
		if (type == TaskType::FL2DCH1)
			return Project::View::WorksetEditor::Modality::Fl2dCh2;
		if (type == TaskType::FL2DCH2)
			return Project::View::WorksetEditor::Modality::Fl2dCh3;
		if (type == TaskType::FL3DCH0)
			return Project::View::WorksetEditor::Modality::Fl3dCh1;
		if (type == TaskType::FL3DCH1)
			return Project::View::WorksetEditor::Modality::Fl3dCh2;
		if (type == TaskType::FL3DCH2)
			return Project::View::WorksetEditor::Modality::Fl3dCh3;

		return Project::View::WorksetEditor::Modality::None;
	}

	auto ProjectView::Impl::ToDataType(Project::View::WorksetEditor::Modality modality) -> TaskType {
		switch (modality) {
			case Project::View::WorksetEditor::Modality::Ht2d:
				return TaskType::HT2D;
			case Project::View::WorksetEditor::Modality::Ht3d:
				return TaskType::HT3D;
			case Project::View::WorksetEditor::Modality::Fl2dCh1:
				return TaskType::FL2DCH0;
			case Project::View::WorksetEditor::Modality::Fl2dCh2:
				return TaskType::FL2DCH1;
			case Project::View::WorksetEditor::Modality::Fl2dCh3:
				return TaskType::FL2DCH2;
			case Project::View::WorksetEditor::Modality::Fl3dCh1:
				return TaskType::FL3DCH0;
			case Project::View::WorksetEditor::Modality::Fl3dCh2:
				return TaskType::FL3DCH1;
			case Project::View::WorksetEditor::Modality::Fl3dCh3:
				return TaskType::FL3DCH2;
			case Project::View::WorksetEditor::Modality::Bf:
				return TaskType::BF;
		}

		return TaskType::Unknown;
	}

	auto ProjectView::Impl::GetCategory(const QTreeWidgetItem* item) -> SelectionCategory {
		if (!item)
			return SelectionCategory::None;

		const auto txt = item->text(1);

		if (txt == "Project")
			return SelectionCategory::Project;
		if (txt == "Pipeline")
			return SelectionCategory::Pipeline;
		if (txt == "Process")
			return SelectionCategory::Process;
		if (txt == "ProcessOutput")
			return SelectionCategory::ProcessOutput;
		if (txt == "SourceList")
			return SelectionCategory::SourceList;
		if (txt == "Source")
			return SelectionCategory::Source;
		if (txt == "FileGroup")
			return SelectionCategory::FileGroup;
		if (txt == "File")
			return SelectionCategory::File;
		if (txt == "Work")
			return SelectionCategory::Work;

		return SelectionCategory::None;
	}

	auto ProjectView::Impl::GetName(const QTreeWidgetItem* item) const -> QString {
		const auto repo = provider->GetService<ITaskRepo>();

		switch (GetCategory(item)) {
			case SelectionCategory::None:
				return {};
			case SelectionCategory::Process:
				if (auto txt = item->text(0); !txt.isEmpty()) {
					if (const auto idx = txt.indexOf(". "); idx > -1)
						return txt.mid(idx + 2, txt.count() - 1);

					return txt;
				}

				break;
			case SelectionCategory::File:
				for (const auto& f : repo->GetFileList()) {
					if (f->GetName() == item->text(0))
						return f->GetFilePath();
				}

				break;
			case SelectionCategory::Work:
				if (const auto* parent = item->parent()) {
					const auto timepoint = item->data(0, Qt::UserRole).toInt();
					QString filepath;

					for (const auto& f : repo->GetFileList()) {
						if (f->GetName() == parent->text(0)) {
							filepath = f->GetFilePath();
							break;
						}
					}

					return QString("%1:%2").arg(filepath).arg(timepoint);
				}

				break;
			default:
				return item->text(0);
		}

		return {};
	}

	auto ProjectView::Impl::GetFileGroupName(const QString& base, const QTreeWidgetItem* original) const -> QString {
		auto text = base;
		auto idx = 1;

		while (true) {
			auto ok = true;


			for (auto i = 0; i < ui.fileTree->topLevelItemCount(); i++) {
				if (const auto* ch = ui.fileTree->topLevelItem(i); GetCategory(ch) == SelectionCategory::FileGroup && ch != original) {
					if (ch->text(0) == text) {
						text = QString("%1_%2").arg(base).arg(idx++);
						ok = false;
						break;
					}
				}
			}

			if (ok)
				return text;
		}
	}

	ProjectView::ProjectView(Tomocube::IServiceProvider* provider) : QWidget(), IProjectView(), IProjectRepoEvent(), ITaskRepoEvent(), d(new Impl) {
		d->provider = provider;
		d->ui.setupUi(this);
		d->ui.addTcfBtn->setObjectName("quiet_color");
		d->ui.addGroupBtn->setObjectName("quiet_color");
		d->ui.addPresenterBtn->setObjectName("quiet_color");
		d->ui.delBtn->setObjectName("quiet_color");
		d->ui.pipBtn->setObjectName("quiet_color");
		d->ui.linkBtn->setObjectName("quiet_color");
		d->ui.exportBtn->setObjectName("quiet_color");
		d->ui.roiBtn->setObjectName("quiet_color");
		d->ui.meBtn->setObjectName("quite_color");
		d->ui.batchBtn->setObjectName("quiet_color");
		d->ui.testBtn->setObjectName("quiet_color");
		d->ui.expandBtn->setObjectName("quiet_color");
		d->ui.expandBtn_2->setObjectName("quiet_color");
		d->ui.collapseBtn->setObjectName("quiet_color");
		d->ui.collapseBtn_2->setObjectName("quiet_color");
		d->ui.historyBtn->setObjectName("quiet_color");
		d->ui.snapshotBtn->setObjectName("quiet_color");
		d->ui.fileTree->viewport()->installEventFilter(this);

		d->ui.addPresenterBtn->setToolTip("New Viewer");
		d->ui.batchBtn->setToolTip("Run Batch");
		d->ui.testBtn->setToolTip("Test run");
		d->ui.pipBtn->setToolTip("Change pipeline");
		d->ui.exportBtn->setToolTip("Data export preferences");

		connect(d->ui.addTcfBtn, &QPushButton::clicked, this, &ProjectView::OnAddTCFClicked);
		connect(d->ui.addGroupBtn, &QPushButton::clicked, this, &ProjectView::OnAddGroupClicked);
		connect(d->ui.addPresenterBtn, &QPushButton::clicked, this, &ProjectView::OnAddPresenterBtnClicked);
		connect(d->ui.delBtn, &QPushButton::clicked, this, &ProjectView::OnDelBtnClicked);
		connect(d->ui.pipBtn, &QPushButton::clicked, this, &ProjectView::OnPipBtnClicked);
		connect(d->ui.linkBtn, &QPushButton::clicked, this, &ProjectView::OnLinkAllBtnClicked);
		connect(d->ui.exportBtn, &QPushButton::clicked, this, &ProjectView::UpdateExporter);
		connect(d->ui.roiBtn, &QPushButton::clicked, this, &ProjectView::OnRoiBtnClicked);
		connect(d->ui.meBtn, &QPushButton::clicked, this, &ProjectView::OnMaskEditorBtnClicked);
		connect(d->ui.batchBtn, &QPushButton::clicked, this, &ProjectView::OnBatchBtnClicked);
		connect(d->ui.expandBtn, &QPushButton::clicked, this, &ProjectView::OnExpandBtnClicked);
		connect(d->ui.expandBtn_2, &QPushButton::clicked, this, &ProjectView::OnExpand2BtnClicked);
		connect(d->ui.collapseBtn, &QPushButton::clicked, this, &ProjectView::OnCollapseBtnClicked);
		connect(d->ui.collapseBtn_2, &QPushButton::clicked, this, &ProjectView::OnCollapse2BtnClicked);
		connect(d->ui.testBtn, &QPushButton::clicked, this, &ProjectView::OnTestBtnClicked);
		connect(d->ui.snapshotBtn, &QPushButton::clicked, this, &ProjectView::OnSnapshotClicked);
		connect(d->ui.historyBtn, &QPushButton::clicked, this, &ProjectView::OnHistoryBtnClicked);

		for (auto* tree : { d->ui.projTree, d->ui.pipTree, d->ui.fileTree }) {
			tree->header()->setSectionResizeMode(0, QHeaderView::Stretch);
			tree->header()->setSectionResizeMode(2, QHeaderView::Fixed);
			tree->setColumnHidden(1, true);
			tree->setColumnWidth(2, 25);

			connect(tree, &QTreeWidget::currentItemChanged, this, &ProjectView::OnTreeSelectionChanged);
			connect(tree, &QTreeWidget::itemChanged, this, &ProjectView::OnTreeItemChanged);
			connect(tree, &QTreeWidget::itemDoubleClicked, this, &ProjectView::OnTreeItemDoubleClicked);
			connect(tree, &QTreeWidget::customContextMenuRequested, this, &ProjectView::OnTreeContextMenuRequested);
		}

		const auto repo = d->provider->GetService<IProjectRepo>();

		d->project = new QTreeWidgetItem(d->ui.projTree, { repo->GetName(), "Project" });
		d->project->setIcon(0, QIcon(":/Project/Analysis.svg"));
		d->project->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		d->project->setExpanded(true);
		d->pipeline = new QTreeWidgetItem(d->ui.pipTree, { "Pipeline", "Pipeline" });
		d->pipeline->setIcon(0, QIcon(":/Flat/Pipeline.svg"));
		d->pipeline->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		d->pipeline->setExpanded(true);

		UpdatePipeline();
		UpdateFileList();
	}

	ProjectView::~ProjectView() = default;

	auto ProjectView::AddEvent(const std::shared_ptr<IProjectViewEvent>& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto ProjectView::RemoveEvent(const std::shared_ptr<IProjectViewEvent>& event) -> void {
		d->events.removeOne(event);
	}

	auto ProjectView::Select(SelectionCategory category, const QString& name) -> void {
		d->ui.projTree->clearSelection();
		d->ui.pipTree->clearSelection();
		d->ui.fileTree->clearSelection();

		switch (category) {
			case SelectionCategory::None:
				break;
			case SelectionCategory::Project:
				d->project->setSelected(true);
				break;
			case SelectionCategory::Pipeline:
				d->pipeline->setSelected(true);
				break;
			case SelectionCategory::Process:
				for (auto i = 0; i < d->pipeline->childCount(); i++) {
					if (d->pipeline->child(i)->text(0) == name)
						d->pipeline->child(i)->setSelected(true);
				}
				break;
			case SelectionCategory::ProcessOutput:
				for (auto i = 0; i < d->pipeline->childCount(); i++) {
					if (d->pipeline->child(i)->text(0) == name)
						d->pipeline->child(i)->setSelected(true);
				}
				break;
			case SelectionCategory::File:
				for (auto i = 0; i < d->ui.fileTree->topLevelItemCount(); i++) {
					if (d->ui.fileTree->topLevelItem(i)->text(0) == name)
						d->ui.fileTree->topLevelItem(i)->setSelected(true);
				}
				break;
		}
	}

	auto ProjectView::UpdateExporter() -> void {
		Run([this] {
			const auto repo = d->provider->GetService<IExporterRepo>();
			const auto task = d->provider->GetService<ITaskRepo>();
			const auto handler = d->provider->GetService<IAlertHandler>();
			const auto finder = std::make_shared<ExporterFinder>(d->provider);

			finder->SetVolume2DExporter(repo->GetExporter(DataFlag::Volume2D));
			finder->SetVolume3DExporter(repo->GetExporter(DataFlag::Volume3D));
			finder->SetMeasureExporter(repo->GetExporter(DataFlag::Measure));

			if (const auto pip = task->GetPipeline()) {
				for (const auto& p : pip->GetProcessList()) {
					const auto proc = pip->GetProcess(p);

					for (const auto& o : proc->GetOutputList()) {
						if (const auto output = proc->GetOutput(o); output->IsAutoSave())
							finder->AddExportableData(output->GetName(), output->GetFlags());
					}
				}
			}

			if (handler->ShowDialog(finder) == QDialog::Accepted) {
				const auto service = d->provider->GetService<IO::IExportService>();

				repo->SetExporter(DataFlag::Volume2D, finder->GetVolume2DExporter());
				repo->SetExporter(DataFlag::Volume3D, finder->GetVolume3DExporter());
				repo->SetExporter(DataFlag::Measure, finder->GetMeasureExporter());

				repo->Save();
			}
		}, RunType::Blocking);
	}

	auto ProjectView::UpdatePipeline() -> void {
		Run([this] {
			const auto repo = d->provider->GetService<ITaskRepo>();
			const auto expanded = d->GetExpanded(d->pipeline);
			QString selection;

			if (const auto* item = d->ui.pipTree->currentItem())
				selection = item->text(0);

			d->ui.pipTree->blockSignals(true);
			d->ClearChildren(d->pipeline);

			if (const auto pip = repo->GetPipeline()) {
				d->pipeline->setText(0, pip->GetName());

				auto* srcItem = new QTreeWidgetItem(d->pipeline, { "Data Sources", "SourceList" });
				srcItem->setIcon(0, QIcon(":/Flat/Folder.svg"));
				srcItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
				srcItem->setExpanded(true);

				if (expanded.contains(srcItem->text(0)))
					srcItem->setExpanded(true);

				for (const auto& s : pip->GetSourceList()) {
					const auto source = pip->GetSource(s);
					auto* item = new QTreeWidgetItem(srcItem, { s, "Source" });
					item->setIcon(0, QIcon(":/Flat/Source.svg"));
					item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

					if (!source->ExistsData())
						item->setIcon(2, QIcon(":/Flat/Warning.svg"));

					srcItem->addChild(item);
				}

				d->pipeline->addChild(srcItem);

				auto procIdx = 1;
				for (const auto& p : pip->GetProcessList()) {
					const auto proc = pip->GetProcess(p);

					auto* procItem = new QTreeWidgetItem(d->pipeline, { QString("%1. %2").arg(procIdx++).arg(p), "Process" });
					procItem->setIcon(0, QIcon(":/Flat/Process.svg"));
					procItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

					if (expanded.contains(procItem->text(0)))
						procItem->setExpanded(true);

					for (const auto& o : proc->GetOutputList()) {
						const auto output = proc->GetOutput(o);

						auto* outItem = new QTreeWidgetItem(procItem, { output->GetName(), "ProcessOutput" });
						outItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

						if (!output->ExistsData())
							outItem->setIcon(2, QIcon(":/Flat/Warning.svg"));

						if (const auto flags = output->GetFlags(); flags.testFlag(DataFlag::Binary) || flags.testFlag(DataFlag::Label))
							outItem->setIcon(0, QIcon(":/Flat/Mask.svg"));
						else if (flags.testFlag(DataFlag::Measure))
							outItem->setIcon(0, QIcon(":/Flat/Measure.svg"));
						else
							outItem->setIcon(0, QIcon(":/Flat/Data.svg"));

						procItem->addChild(outItem);
					}

					d->pipeline->addChild(procItem);
				}
			}

			d->ui.pipTree->blockSignals(false);

			for (auto* i : d->ui.pipTree->findItems(selection, Qt::MatchExactly | Qt::MatchRecursive)) {
				d->ui.pipTree->setCurrentItem(i);
				i->setSelected(true);
				break;
			}
		});
	}

	auto ProjectView::UpdateFileList() -> void {
		Run([this] {
			const auto repo = d->provider->GetService<ITaskRepo>();
			const auto expanded = d->GetExpanded(d->ui.fileTree);
			QString selection;

			if (const auto* item = d->ui.fileTree->currentItem())
				selection = item->text(0);

			d->ui.fileTree->blockSignals(true);
			d->ui.fileTree->clear();

			QMap<QString, QTreeWidgetItem*> map;

			for (const auto& g : repo->GetGroupList()) {
				if (!g.isEmpty() && !map.contains(g)) {
					map[g] = new QTreeWidgetItem(d->ui.fileTree, { g, "FileGroup" });
					map[g]->setIcon(0, QIcon(":/Flat/Folder.svg"));
					map[g]->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsDropEnabled);

					if (expanded.contains(map[g]->text(0)))
						map[g]->setExpanded(true);
				}

				for (const auto& f : repo->GetFileList(g)) {
					auto* file = new QTreeWidgetItem({ f->GetName(), "File" });
					file->setIcon(0, QIcon(":/Flat/File.svg"));
					file->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);

					if (!f->IsValid())
						file->setIcon(2, QIcon(":/Flat/Warning.svg"));

					if (f->IsLinked())
						file->setIcon(2, QIcon(":/Flat/Link.svg"));

					for (const auto t : f->GetTimePointList()) {
						auto* work = new QTreeWidgetItem(file, { IFileItem::ToTimeString(t), "Work" });
						work->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
						work->setData(0, Qt::UserRole, t);
						work->setIcon(0, QIcon(":/Flat/Task.svg"));

						if (!f->IsLinkable(t))
							work->setIcon(2, QIcon(":/Flat/Warning.svg"));
						else if (f->IsLinked(t))
							work->setIcon(2, QIcon(":/Flat/Link.svg"));
						else if (f->IsError(t))
							work->setIcon(2, QIcon(":/Flat/RedCircle.svg"));

						file->addChild(work);
					}

					if (g.isEmpty())
						d->ui.fileTree->addTopLevelItem(file);
					else
						map[g]->addChild(file);

					if (expanded.contains(file->text(0)))
						file->setExpanded(true);
				}
			}

			d->ui.fileTree->blockSignals(false);

			for (auto* i : d->ui.fileTree->findItems(selection, Qt::MatchExactly | Qt::MatchRecursive)) {
				d->ui.fileTree->setCurrentItem(i);
				i->setSelected(true);
				break;
			}
		});
	}

	auto ProjectView::GetSelectionCategory() const -> SelectionCategory {
		for (const auto* tree : { d->ui.projTree, d->ui.pipTree, d->ui.fileTree }) {
			if (const auto* item = tree->currentItem())
				return d->GetCategory(item);
		}

		return {};
	}

	auto ProjectView::GetSelectionName() const -> QString {
		for (const auto* tree : { d->ui.projTree, d->ui.pipTree, d->ui.fileTree }) {
			if (const auto* item = tree->currentItem())
				return d->GetName(item);
		}

		return {};
	}

	auto ProjectView::OnSnapshotClicked() -> void {
		const auto repo = d->provider->GetService<ITaskRepo>();
		const auto preRepo = d->provider->GetService<IPresenterRepo>();
		const auto pipeline = repo->GetPipeline();

		const auto defaultPresenter = preRepo->GetDefaultPresenter();

		if (defaultPresenter->GetDataList().count() < 1) {
			return;
		}

		const auto presenterID = QString("Slice view in 2D");
		auto portMap = QMap<QString, QStringList>();
		const auto currentDateTime = QDateTime::currentDateTime();
		const auto currentTime = currentDateTime.time();
		const auto windowTitle = QString("Snapshot (%1)").arg(currentTime.toString("hh:mm:ss"));

		QStringList originalSourceList = defaultPresenter->GetDataList();
		QStringList htFirst;
		for (const auto& sourceID : originalSourceList) {
			if (const auto data = pipeline->GetSource(sourceID)) {
				if (data->GetFlags().testFlag(DataFlag::HT)) {
					htFirst.append(sourceID);
					originalSourceList.removeOne(sourceID);
				}
			}
		}
		for (const auto& remains : originalSourceList) {
			htFirst.append(remains);
		}

		portMap["List of Images"] = htFirst;

		for (const auto& e : d->events) {
			e->OnPresenterAdded(presenterID, portMap, windowTitle);
		}
	}

	auto ProjectView::OnAddPresenterBtnClicked() -> void {
		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto repo = d->provider->GetService<ITaskRepo>();
		const auto pipeline = repo->GetPipeline();
		const auto dialog = std::make_shared<PresenterDialog>(d->provider, pipeline);

		if (handler->ShowDialog(dialog) == QDialog::Accepted) {
			const auto presenterID = dialog->GetPresenterID();
			const auto portMap = dialog->GetDataLinkMap();
			const auto windowTitle = dialog->GetWindowName();

			for (const auto& e : d->events) {
				e->OnPresenterAdded(presenterID, portMap, windowTitle);
			}
		}
	}

	auto ProjectView::OnAddFileBtnClicked() -> void {
		if (const auto filepath = QFileDialog::getOpenFileName(this, "Add File"); !filepath.isEmpty()) {
			RunAsync([this, filepath] {
				for (const auto& e : d->events)
					e->OnFileAdded(filepath);
			});
		}
	}

	auto ProjectView::OnAddTCFClicked() -> void {
		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto viewer = std::make_shared<IO::Widget::TCFDialog>();

		viewer->SetMultiSelective(true);

		if (handler->ShowDialog(viewer) == QDialog::Accepted) {
			const auto list = viewer->GetSelected();
			QString group;

			if (d->GetCategory(d->ui.fileTree->currentItem()) == SelectionCategory::FileGroup)
				group = d->GetName(d->ui.fileTree->currentItem());

			RunAsync([this, list, group] {
				for (const auto& e : d->events) {
					for (const auto& l : list) {
						e->OnFileAdded(l);
						e->OnFileMoved(l, group);
					}
				}
			});
		}
	}

	auto ProjectView::OnAddGroupClicked() -> void {
		for (const auto& e : d->events)
			e->OnGroupAdded(d->GetFileGroupName("Group"));
	}

	auto ProjectView::OnAddWorkClicked(const QStringList& list) -> void {
		const auto repo = d->provider->GetService<ITaskRepo>();
		const auto pipeline = repo->GetPipeline();

		if (pipeline == nullptr)
			return;

		Project::View::WorksetEditor::Config config;
		QString htSourceName;
		QStringList fl2dSourceNames;
		QStringList fl3dSourceNames;
		for (const auto& sourceName : pipeline->GetSourceList()) {
			const auto source = pipeline->GetSource(sourceName);
			const auto dataFlag = source->GetFlags();

			if (dataFlag & DataFlag::HT && dataFlag & DataFlag::Volume2D) {
				config.sources.insert(Project::View::WorksetEditor::Modality::Ht2d, { source->GetName() });
				htSourceName = source->GetName();
			}
			if (dataFlag & DataFlag::HT && dataFlag & DataFlag::Volume3D) {
				config.sources.insert(Project::View::WorksetEditor::Modality::Ht3d, { source->GetName() });
				htSourceName = source->GetName();
			}
			if (dataFlag & DataFlag::FL && dataFlag & DataFlag::Volume2D) {
				if (config.sources.find(Project::View::WorksetEditor::Modality::Fl2d) == config.sources.end()) {
					config.sources.insert(Project::View::WorksetEditor::Modality::Fl2d, { source->GetName() });
				} else {
					auto sourceNames = config.sources[Project::View::WorksetEditor::Modality::Fl2d];
					sourceNames << source->GetName();
					config.sources[Project::View::WorksetEditor::Modality::Fl2d] = sourceNames;
				}
				fl2dSourceNames.append(sourceName);
			}
			if (dataFlag & DataFlag::FL && dataFlag & DataFlag::Volume3D) {
				if (config.sources.find(Project::View::WorksetEditor::Modality::Fl3d) == config.sources.end()) {
					config.sources.insert(Project::View::WorksetEditor::Modality::Fl3d, { source->GetName() });
				} else {
					auto sourceNames = config.sources[Project::View::WorksetEditor::Modality::Fl3d];
					sourceNames << source->GetName();
					config.sources[Project::View::WorksetEditor::Modality::Fl3d] = sourceNames;
				}
				fl3dSourceNames.append(sourceName);
			}
			if (dataFlag & DataFlag::BF && dataFlag & DataFlag::Volume2D)
				config.sources.insert(Project::View::WorksetEditor::Modality::Bf, { source->GetName() });
		}

		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto timePointDialog = std::make_shared<Project::View::WorksetEditor::TimePointSettingDialog>(config, this);
		timePointDialog->SetTCFs(list);

		if (handler->ShowDialog(timePointDialog) != QDialog::Accepted)
			return;

		const auto timeSelectionOutput = timePointDialog->GetResult();
		if (timeSelectionOutput.isEmpty())
			return;

		RunAsync([this, timeSelectionOutput, htSourceName, fl2dSourceNames, fl3dSourceNames] {
			for (const auto& k : timeSelectionOutput.keys()) {
				const auto output = timeSelectionOutput[k];

				if (output.times.count() != output.realTimes.count())
					continue;

				for (auto i = 0; i < output.times.count(); i++) {
					auto fl2dCnt = 0;
					auto fl3dCnt = 0;
					for (const auto modality : output.modalities) {
						QString sn;
						const auto dtype = d->ToDataType(modality);
						if (dtype == TaskType::HT2D || dtype == TaskType::HT3D)
							sn = htSourceName;
						else if (dtype == TaskType::FL2DCH0 || dtype == TaskType::FL2DCH1 || dtype == TaskType::FL2DCH2) {
							if (fl2dSourceNames.count() > fl2dCnt) {
								sn = fl2dSourceNames[fl2dCnt++];
							}
						} else if (dtype == TaskType::FL3DCH0 || dtype == TaskType::FL3DCH1 || dtype == TaskType::FL3DCH2) {
							if (fl3dSourceNames.count() > fl3dCnt) {
								sn = fl3dSourceNames[fl3dCnt++];
							}
						}

						for (const auto& e : d->events)
							e->OnWorkAdded(k, sn, static_cast<int>(output.realTimes[i]), d->ToDataType(modality), output.modalTimes[modality][output.times[i]]);
					}
				}
			}
		});
	}

	auto ProjectView::OnDelBtnClicked() -> void {
		QStringList files;
		QStringList works;

		for (const auto* i : d->ui.fileTree->selectedItems()) {
			switch (d->GetCategory(i)) {
				case SelectionCategory::File:
					files.push_back(d->GetName(i));
					break;
				case SelectionCategory::Work:
					works.push_back(d->GetName(i));
					break;
			}
		}

		for (const auto& f : files) {
			d->RemoveROI(f);
			for (const auto& e : d->events) {
				e->OnFileRemoved(f);
			}
		}


		for (const auto& w : works) {
			const auto idx = w.lastIndexOf(':');
			d->RemoveROI(w.mid(0, idx), w.mid(idx + 1, -1).toInt());
			for (const auto& e : d->events) {
				e->OnWorkRemoved(w.mid(0, idx), w.mid(idx + 1, -1).toInt());
			}
		}
	}

	auto ProjectView::OnDelAllFilesBtnClicked() -> void {
		QStringList remove;

		for (auto i = 0; i < d->ui.fileTree->topLevelItemCount(); i++) {
			switch (const auto* child = d->ui.fileTree->topLevelItem(i); d->GetCategory(child)) {
				case SelectionCategory::FileGroup:
					for (auto j = 0; j < child->childCount(); j++) {
						if (const auto* gchild = child->child(j); d->GetCategory(gchild) == SelectionCategory::File)
							remove.push_back(d->GetName(gchild));
					}
					break;
				case SelectionCategory::File:
					remove.push_back(d->GetName(child));
					break;
			}
		}

		for (const auto& r : remove) {
			d->RemoveROI(r);
			for (const auto& e : d->events) {
				e->OnFileRemoved(r);
			}
		}
	}

	auto ProjectView::OnDelGroupBtnClicked() -> void {
		QStringList groups;
		QStringList remove;

		for (const auto* item : d->ui.fileTree->selectedItems()) {
			if (d->GetCategory(item) == SelectionCategory::FileGroup) {
				groups.push_back(item->text(0));

				for (int i = 0; i < item->childCount(); i++) {
					const auto* child = item->child(i);

					if (d->GetCategory(child) == SelectionCategory::File)
						remove.push_back(d->GetName(child));
				}
			}
		}

		for (const auto& r : remove) {
			for (const auto& e : d->events)
				e->OnFileMoved(r, QString());
		}

		for (const auto& g : groups) {
			for (const auto& e : d->events)
				e->OnGroupRemoved(g);
		}
	}

	auto ProjectView::OnDelGroupAndFilesBtnClicked() -> void {
		QStringList groups;

		for (const auto* item : d->ui.fileTree->selectedItems()) {
			if (d->GetCategory(item) == SelectionCategory::FileGroup)
				groups.push_back(d->GetName(item));
		}

		for (const auto& g : groups)
			for (const auto& e : d->events)
				e->OnGroupRemoved(g);
	}

	auto ProjectView::OnDelWorkBtnClicked() -> void {
		QStringList works;
		QList<int> tps;

		for (const auto* item : d->ui.fileTree->selectedItems()) {
			if (d->GetCategory(item) == SelectionCategory::Work) {
				works.push_back(d->GetName(item->parent()));
				tps.push_back(d->GetName(item).toInt());
			}
		}

		for (auto i = 0; i < tps.count(); i++) {
			d->RemoveROI(works[i], tps[i]);
			for (const auto& e : d->events) {
				e->OnWorkRemoved(works[i], tps[i]);
			}
		}
	}

	auto ProjectView::OnPipBtnClicked() -> void {
		const auto repo = d->provider->GetService<IProjectRepo>();
		const auto finder = std::make_shared<PipelineFinder>(d->provider);
		const auto handler = d->provider->GetService<IAlertHandler>();

		if (handler->ShowDialog(finder) == QDialog::Accepted) {
			RunAsync([this, path = finder->GetUrl()] {
				for (const auto& e : d->events)
					e->OnPipelineChanged(path);
			});
		}
	}

	auto ProjectView::OnLinkBtnClicked() -> void {
		if (const auto* item = d->ui.fileTree->currentItem(); d->GetCategory(item) == SelectionCategory::File) {
			const auto name = d->GetName(item);
			OnAddWorkClicked({ name });
		}
	}

	auto ProjectView::OnLinkAllBtnClicked() -> void {
		const auto repo = d->provider->GetService<ITaskRepo>();
		QStringList list;

		for (const auto& f : repo->GetFileList())
			list.push_back(f->GetFilePath());

		OnAddWorkClicked(list);
	}

	auto ProjectView::OnLinkWorkClicked() -> void {
		if (const auto* item = d->ui.fileTree->currentItem(); d->GetCategory(item) == SelectionCategory::Work) {
			const auto repo = d->provider->GetService<ITaskRepo>();

			if (const auto file = repo->GetFile(d->GetName(item->parent()))) {
				const auto filePath = file->GetFilePath();
				const auto timePoint = d->GetName(item).toInt();

				for (const auto& e : d->events)
					e->OnWorkLinked(filePath, timePoint);
			}
		}
	}

	auto ProjectView::OnLinkGroupBtnClicked() -> void {
		QStringList list;

		for (const auto& item : d->ui.fileTree->selectedItems()) {
			if (d->GetCategory(item) == SelectionCategory::FileGroup) {
				const auto repo = d->provider->GetService<ITaskRepo>();

				for (auto i = 0; i < item->childCount(); i++) {
					if (const auto* child = item->child(i); d->GetCategory(child) == SelectionCategory::File) {
						if (const auto file = repo->GetFile(d->GetName(child)))
							list.push_back(file->GetFilePath());
					}
				}
			}
		}

		OnAddWorkClicked(list);
	}

	auto ProjectView::OnBatchBtnClicked() -> void {
		d->pipeline->setSelected(true);
		d->ui.pipTree->setCurrentItem(d->pipeline);

		RunAsync([this] {
			QThread::sleep(1);

			const auto prop = d->provider->GetService<IPropertyView>();
			prop->SetAbortable(prop->GetSession(), true);
			prop->Execute();
		});
	}

	auto ProjectView::OnTestBtnClicked() -> void {
		RunAsync([this] {
			QThread::sleep(1);

			//const auto prop = d->provider->GetService<IPropertyView>();
			//prop->SetAbortable(prop->GetSession(), true);

			for (const auto& e : d->events)
				e->OnTestRun();
		});
	}

	auto ProjectView::OnMaskEditorBtnClicked() -> void {
		const auto presenterRepo = d->provider->GetService<IPresenterRepo>();
		const auto taskRepo = d->provider->GetService<ITaskRepo>();
		const auto pipeline = taskRepo->GetPipeline();
		if(!pipeline) {
			return;
		}

		const auto linkedFile = taskRepo->GetLinkedFile();
		if(!linkedFile || !linkedFile->IsValid()) {
			return;
		}

		if (pipeline->GetSourceList().count() < 1) {
			//there is no source
			return;
		}

		auto sourceDataExist = false;
		for (const auto& sn : pipeline->GetSourceList()) {
			if (const auto source = pipeline->GetSource(sn); source->GetData()) {
				sourceDataExist = true;
				break;
			}
		}

		if (false == sourceDataExist) {
			return;
		}

		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto dialog = std::make_shared<Project::View::MaskEditor::MaskEditorDialog>();

		for (const auto& sn : pipeline->GetSourceList()) {
			dialog->AddSource(sn, pipeline->GetSource(sn)->GetData());
		}

		for (const auto& on : pipeline->GetOutputList()) {
			if (const auto output = pipeline->GetOutput(on); output->GetData()) {
				dialog->AddData(on, output->GetData());
			}
		}

		if (handler->ShowDialog(dialog) != QDialog::Accepted) {
			//Do nothing when ME dialog closed
			return;
		}

		const auto [maskID, modifiedMask] = dialog->GetCurrentMask();
		const auto presenter = presenterRepo->GetDefaultPresenter();

		if (const auto targetOutput = pipeline->GetOutput(maskID)) {
			targetOutput->SetData(modifiedMask);
			d->AddDataToDefaultPresenter(targetOutput->GetData(), maskID);
			UpdatePipeline();
		}
	}

	auto ProjectView::OnRoiBtnClicked() -> void {
		const auto repo = d->provider->GetService<ITaskRepo>();
		QList<Project::View::RoiEditor::Config> roiConfig;

		for (const auto& f : repo->GetFileList()) {
			const auto path = f->GetFilePath();
			Project::View::RoiEditor::Config config;
			config.tcfPath = path;
			Project::View::WorksetEditor::TimeSelectionOutput selection;
			const auto file = repo->GetFile(path);

			for (const auto i : file->GetTimePointList()) {
				selection.times.append(i);

				for (const auto& id : file->GetTypeList(i)) {
					const auto type = file->GetType(i, id);
					const auto modal = d->ToTSModality(type);
					selection.modalities.append(modal);
				}
			}

			config.timeSelection = selection;
			roiConfig.append(config);
		}

		const auto handler = d->provider->GetService<IAlertHandler>();

		const auto projRepo = d->provider->GetService<IProjectRepo>();
		const auto roiPath = QDir(projRepo->GetLocation()).filePath("ROI");
		const auto roiDialog = std::make_shared<Project::View::RoiEditor::ROISelectionDialog>();
		roiDialog->SetCurRoiRoot(roiPath);
		roiDialog->SetInput(roiConfig);

		if (handler->ShowDialog(roiDialog) != QDialog::Accepted) {
			return;
		}

		const auto roiResult = roiDialog->GetOutput();

		if (roiResult.count() < 1) {
			return;
		}

		QDir roiDir(roiPath);
		if (false == roiDir.exists()) {
			roiDir.mkdir(roiPath);
		} else {
			QStringList filters;
			filters << "*.tcroi";
			roiDir.setNameFilters(filters);
			QFileInfoList list = roiDir.entryInfoList();
			for (const auto l : list) {
				QFile::remove(l.filePath());
			}
		}

		for (const auto& f : repo->GetFileList()) {
			const auto path = f->GetFilePath();
			if (false == roiResult.contains(path)) {
				continue;
			}

			QFileInfo fileInfo(path);
			for (auto i = 0; i < roiResult[path].count(); i++) {
				const auto timePoint = roiResult[path].keys()[i];

				const auto roiFilePath = roiDir.filePath(QString("%1_%2.tcroi").arg(fileInfo.fileName().chopped(4)).arg(timePoint));
				const auto roi = roiResult[path][timePoint];

				TC::IO::ROIWriter roiWriter(roiFilePath);
				roiWriter.Write(roi);
			}
		}
	}

	auto ProjectView::OnOpenProjectBtnClicked() -> void {
		const auto repo = d->provider->GetService<IProjectRepo>();
		QDesktopServices::openUrl(QUrl::fromLocalFile(repo->GetLocation()));
	}

	auto ProjectView::OnRenameGroupBtnClicked() -> void {
		if (auto* item = d->ui.fileTree->currentItem(); d->GetCategory(item) == SelectionCategory::FileGroup)
			d->ui.fileTree->editItem(item, 0);
	}

	auto ProjectView::OnExpandBtnClicked() -> void {
		const auto topLevelItem = d->ui.pipTree->topLevelItem(0);
		for (auto i = 0; i < topLevelItem->childCount(); i++) {
			const auto child = topLevelItem->child(i);
			d->ui.pipTree->expandItem(child);
		}
	}

	auto ProjectView::OnExpand2BtnClicked() -> void {
		d->ui.fileTree->expandAll();
	}

	auto ProjectView::OnCollapseBtnClicked() -> void {
		const auto topLevelItem = d->ui.pipTree->topLevelItem(0);
		for (auto i = 0; i < topLevelItem->childCount(); i++) {
			const auto child = topLevelItem->child(i);
			d->ui.pipTree->collapseItem(child);
		}
	}

	auto ProjectView::OnCollapse2BtnClicked() -> void {
		d->ui.fileTree->collapseAll();
	}

	auto ProjectView::OnHistoryBtnClicked() -> void {
		const auto project = d->provider->GetService<IProjectRepo>();
		const auto history = d->provider->GetService<IHistoryRepo>();
		const auto task = d->provider->GetService<ITaskRepo>();
		const auto linked = task->GetLinkedFile();

		if (!linked) {
			const auto alert = d->provider->GetService<IAlertHandler>();
			alert->ShowMessage("No Linked File", "To load history data, work must be linked.");
			return;
		}

		if (const auto path = QFileDialog::getExistingDirectory(this, "Find History", project->GetLocation() + "/Export"); !path.isEmpty()) {
			const QDir dir(path);

			for (const auto& h : history->GetHistoryList()) {
				if (h->GetCreationDateTime().toString("yyyyMMdd_HHmmss") == dir.dirName()) {
					if (const auto pip = h->GetPipeline()) {
						if (const auto temp = task->GetPipeline()) {
							for (const auto& s : temp->GetSourceList()) {
								const auto src = temp->GetSource(s);
								const auto dest = pip->GetSource(s);

								if (src && dest)
									dest->SetData(src->GetData());
							}
						}

						task->SetPipeline(pip);

						for (const auto& p : pip->GetProcessList()) {
							const auto proc = pip->GetProcess(p);
							const auto repo = d->provider->GetService<IPresenterRepo>();
							const auto presenter = repo->GetDefaultPresenter();
							const auto report = repo->GetDefaultReport();
							const QDir procDir(dir.filePath(p));

							for (const auto& o : proc->GetOutputList()) {
								const auto output = proc->GetOutput(o);
								const auto filename = QString("%1_%2_%3").arg(output->GetName()).arg(linked->GetName()).arg(linked->GetLinkedTimePoint());

								for (const auto& f : procDir.entryList(QDir::NoDotAndDotDot | QDir::Files)) {
									if (f.startsWith(filename)) {
										const auto service = d->provider->GetService<IO::IImportService>();
										const QFileInfo info(procDir.filePath(f));

										if (const auto importer = service->GetImporter(info.suffix())) {
											if (const auto data = importer->Import(info.absoluteFilePath(), output->GetFlags())) {
												output->SetData(data);

												if (output->GetFlags().testFlag(DataFlag::Volume2D) || output->GetFlags().testFlag(DataFlag::Volume3D)) {
													presenter->AddData(output->GetName(), data, "SlicePort");
												} else if (output->GetFlags().testFlag(DataFlag::Measure)) {
													QString portName = "HT";
													for (const auto& ikey : proc->GetInputList()) {
														if (const auto in = proc->GetInput(ikey);in->GetFlags().testFlag(DataFlag::FL)) {
															portName = "FL";
															break;
														}
													}
													report->AddData(output->GetName(), data, portName);
												}

												UpdatePipeline();
											}
										}
									}
								}
							}
						}
					}

					break;
				}
			}
		}
	}

	auto ProjectView::OnTreeSelectionChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous) -> void {
		if (current) {
			for (auto* tree : { d->ui.projTree, d->ui.pipTree, d->ui.fileTree }) {
				if (tree != current->treeWidget()) {
					tree->setCurrentItem(nullptr);
					tree->clearSelection();
				}
			}
		}

		const auto category = d->GetCategory(current);
		const auto name = d->GetName(current);

		d->ui.delBtn->setEnabled(category == SelectionCategory::File || category == SelectionCategory::Work);

		RunAsync([this, category, name] {
			for (const auto& e : d->events)
				e->OnSelectionChanged(category, name);
		});
	}

	auto ProjectView::OnTreeItemChanged(QTreeWidgetItem* item, int column) -> void {
		if (d->GetCategory(item) == SelectionCategory::FileGroup) {
			const auto repo = d->provider->GetService<ITaskRepo>();
			const auto newName = d->GetFileGroupName(item->text(0), item);

			d->ui.fileTree->blockSignals(true);
			item->setText(0, newName);
			d->ui.fileTree->blockSignals(false);

			for (const auto& g : repo->GetGroupList()) {
				if (g.isEmpty())
					continue;

				const auto items = d->ui.fileTree->findItems(g, Qt::MatchCaseSensitive | Qt::MatchRecursive);
				const auto predicate = [this](const QTreeWidgetItem* child) {
					return d->GetCategory(child) == SelectionCategory::FileGroup;
				};

				if (!std::any_of(items.constBegin(), items.constEnd(), predicate)) {
					for (const auto& e : d->events)
						e->OnGroupRenamed(g, newName);
				}
			}
		}
	}

	auto ProjectView::OnTreeItemDoubleClicked(QTreeWidgetItem* item, int column) -> void {
		if (d->GetCategory(item) == SelectionCategory::Work) {
			const auto repo = d->provider->GetService<ITaskRepo>();

			if (const auto file = repo->GetFile(d->GetName(item->parent()))) {
				const auto filePath = file->GetFilePath();
				const auto timePoint = d->GetName(item).toInt();

				RunAsync([this, filePath, timePoint] {
					for (const auto& e : d->events)
						e->OnWorkLinked(filePath, timePoint);
				});
			}
		}
	}

	auto ProjectView::OnTreeContextMenuRequested(const QPoint& pos) -> void {
		if (const auto* tree = dynamic_cast<QTreeWidget*>(sender())) {
			if (const auto* item = tree->itemAt(pos)) {
				QMenu menu(this);

				switch (d->GetCategory(item)) {
					case SelectionCategory::None:
					case SelectionCategory::SourceList:
					case SelectionCategory::Process:
						break;
					case SelectionCategory::Project:
						menu.addAction(QIcon(":/Flat/Pipeline.svg"), "Change Pipeline", this, &ProjectView::OnPipBtnClicked);
						menu.addAction(QIcon(":/Flat/Export.svg"), "Data Export Preferences", this, &ProjectView::UpdateExporter);
						menu.addSeparator();
						menu.addAction(QIcon(":/Flat/Folder.svg"), "Open the Project Folder", this, &ProjectView::OnOpenProjectBtnClicked);
						break;
					case SelectionCategory::Pipeline:
						menu.addAction(QIcon(":/Flat/Pipeline.svg"), "Change Pipeline", this, &ProjectView::OnPipBtnClicked);
						break;
					case SelectionCategory::ProcessOutput: {
						const auto service = d->provider->GetService<IO::IExportService>();
						const auto repo = d->provider->GetService<ITaskRepo>();

						menu.addAction(QIcon(":/Widget/Add.svg"), "Add Viewer", this, &ProjectView::OnAddPresenterBtnClicked);
						menu.addSeparator();

						if (const auto pip = repo->GetPipeline()) {
							if (const auto data = pip->GetData(d->GetName(item)); data && data->GetData()) {
								for (const auto& l : service->GetExporterList(data->GetFlags()))
									menu.addAction("Export as " + l->GetName(), this, [this, l, data] {
										const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/export", QApplication::applicationDirPath()).toString();
										if (const auto path = QFileDialog::getSaveFileName(this, "Save Data", prev, QString("%1 (*.%2)").arg(l->GetName()).arg(l->GetFormat())); !path.isEmpty()) {
											l->Export(data->GetData(), path);
											QSettings("Tomocube", "TomoAnalysis").setValue("recent/export", QFileInfo(path).dir().path());
										}
									});
							}
						}

						break;
					}
					case SelectionCategory::FileGroup:
						menu.addAction("Load TCF files", this, &ProjectView::OnAddTCFClicked);
						menu.addAction("Rename Group", this, &ProjectView::OnRenameGroupBtnClicked);
						menu.addAction(QIcon(":/Flat/Link.svg"), "Make Worksets in This Group", this, &ProjectView::OnLinkGroupBtnClicked);
						menu.addSeparator();
						menu.addAction(QIcon(":/Flat/Bin.svg"), "Remove the Groups and Files", this, &ProjectView::OnDelGroupAndFilesBtnClicked);
						break;
					case SelectionCategory::File:
						menu.addAction(QIcon(":/Flat/Link.svg"), "Make Worksets", this, &ProjectView::OnLinkBtnClicked);
						menu.addSeparator();
						menu.addAction(QIcon(":/Widget/Remove.svg"), "Remove TCF Files or Worksets", this, &ProjectView::OnDelBtnClicked);
						break;
					case SelectionCategory::Work:
						menu.addAction(QIcon(":/Flat/Link.svg"), "Use as Active Workset", this, &ProjectView::OnLinkWorkClicked);
						menu.addAction(QIcon(":/Widget/Remove.svg"), "Remove Worksets", this, &ProjectView::OnDelBtnClicked);
						break;
				}

				menu.exec(QCursor::pos());
			}
		}
	}

	auto ProjectView::OnNameChanged(const QString& name) -> void {
		Run([this, name] {
			d->project->setText(0, name);
		});
	}

	auto ProjectView::OnPipelineChanged(const Pipeline::PipelinePtr& pipeline) -> void {
		UpdatePipeline();
		UpdateFileList();
	}

	auto ProjectView::OnFileAdded(const FileItemPtr& file) -> void {
		UpdateFileList();
	}

	auto ProjectView::OnFileUpdated(const FileItemPtr& file) -> void {
		UpdateFileList();
	}

	auto ProjectView::OnFileRemoved(const FileItemPtr& file) -> void {
		UpdateFileList();
	}

	auto ProjectView::OnFileLinked(const FileItemPtr& file, int timepoint) -> void {
		UpdateFileList();
		UpdatePipeline();
	}

	auto ProjectView::OnFileUnlinked(const FileItemPtr& file, int timepoint) -> void {
		UpdateFileList();
		UpdatePipeline();
	}

	auto ProjectView::OnGroupAdded(const QString& group) -> void {
		UpdateFileList();
	}

	auto ProjectView::OnGroupRemoved(const QString& group) -> void {
		UpdateFileList();
	}

	auto ProjectView::eventFilter(QObject* watched, QEvent* event) -> bool {
		if (watched == d->ui.fileTree->viewport()) {
			if (event->type() == QEvent::Drop) {
				const auto* drop = dynamic_cast<QDropEvent*>(event);

				QString group;
				QStringList files;

				if (const auto* item = d->ui.fileTree->itemAt(drop->pos())) {
					if (d->GetCategory(item) == SelectionCategory::FileGroup)
						group = d->GetName(item);
				}

				for (const auto* src : d->ui.fileTree->selectedItems())
					files.push_back(d->GetName(src));

				RunAsync([this, files, group] {
					for (const auto& f : files) {
						for (const auto& e : d->events)
							e->OnFileMoved(f, group);
					}
				});
			}
		}

		return QWidget::eventFilter(watched, event);
	}
}
