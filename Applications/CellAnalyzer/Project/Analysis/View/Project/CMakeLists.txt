project(CellAnalyzer.Project.Analysis.View.Project)

set(HEADERS
	inc/ProjectView.h
	inc/PresenterDialog.h
)

set(SOURCES
	src/ProjectView.cpp
	src/ProjectView.ui
	src/PresenterDialog.cpp
	src/PresenterDialog.ui
)

add_library(${PROJECT_NAME} SHARED
	${HEADERS}
	${SOURCES}
)

add_library(CellAnalyzer::Project::Analysis::View::Project ALIAS ${PROJECT_NAME})
set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/CellAnalyzer/Project/Analysis/View")

target_link_libraries(${PROJECT_NAME}
	PUBLIC
		Qt5::Widgets
		
		CellAnalyzer::UIModel
		CellAnalyzer::IO::Widget::TCFDialog
		CellAnalyzer::IO::ImportModel
		CellAnalyzer::Project::Analysis::RepoModel
		CellAnalyzer::Project::Analysis::ViewModel
		CellAnalyzer::Project::Analysis::View::ExporterFinder
		CellAnalyzer::Project::Analysis::View::PipelineFinder
		CellAnalyzer::Pipeline::PipelineModel
		CellAnalyzer::Presenter::ServiceModel

		CellAnalyzer::Project::View::WorksetEditor
		CellAnalyzer::Project::View::RoiEditor
		CellAnalyzer::Project::View::MaskEditor
)

target_include_directories(${PROJECT_NAME}
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
		$<INSTALL_INTERFACE:inc>
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PUBLIC
		/wd4251
)

generate_export_header(${PROJECT_NAME}
	EXPORT_MACRO_NAME ${PROJECT_NAME}_API
	EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
	ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
	LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
	RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT application_ta_de)
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT dev)
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)