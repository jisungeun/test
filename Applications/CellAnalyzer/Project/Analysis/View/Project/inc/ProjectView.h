#pragma once

#include <QTreeWidgetItem>
#include <QWidget>

#include "IServiceProvider.h"

#include "IProjectRepoEvent.h"
#include "IProjectView.h"
#include "ITaskRepoEvent.h"

#include "CellAnalyzer.Project.Analysis.View.ProjectExport.h"

namespace CellAnalyzer::Project::Analysis::View {
	class CellAnalyzer_Project_Analysis_View_Project_API ProjectView final : public QWidget, public IProjectView, public IProjectRepoEvent, public ITaskRepoEvent {
	public:
		explicit ProjectView(Tomocube::IServiceProvider* provider);
		~ProjectView() override;

		auto AddEvent(const std::shared_ptr<IProjectViewEvent>& event) -> void override;
		auto RemoveEvent(const std::shared_ptr<IProjectViewEvent>& event) -> void override;

		auto Select(SelectionCategory category, const QString& name) -> void override;

		auto UpdateExporter() -> void override;
		auto UpdatePipeline() -> void override;
		auto UpdateFileList() -> void override;

		auto GetSelectionCategory() const -> SelectionCategory override;
		auto GetSelectionName() const -> QString override;

	protected slots:
		auto OnAddPresenterBtnClicked() -> void;
		auto OnAddFileBtnClicked() -> void;
		auto OnAddTCFClicked() -> void;
		auto OnAddGroupClicked() -> void;
		auto OnAddWorkClicked(const QStringList& list) -> void;
		auto OnDelBtnClicked() -> void;
		auto OnDelAllFilesBtnClicked() -> void;
		auto OnDelGroupBtnClicked() -> void;
		auto OnDelGroupAndFilesBtnClicked() -> void;
		auto OnDelWorkBtnClicked() -> void;
		auto OnPipBtnClicked() -> void;
		auto OnLinkBtnClicked() -> void;
		auto OnLinkAllBtnClicked() -> void;
		auto OnLinkWorkClicked() -> void;
		auto OnLinkGroupBtnClicked() -> void;
		auto OnRoiBtnClicked() -> void;
		auto OnMaskEditorBtnClicked() -> void;
		auto OnBatchBtnClicked() -> void;
		auto OnTestBtnClicked() -> void;
		auto OnOpenProjectBtnClicked() -> void;
		auto OnRenameGroupBtnClicked() -> void;
		auto OnExpandBtnClicked() -> void;
		auto OnExpand2BtnClicked() -> void;
		auto OnCollapseBtnClicked() -> void;
		auto OnCollapse2BtnClicked() -> void;
		auto OnHistoryBtnClicked() -> void;
		auto OnSnapshotClicked() -> void;

		auto OnTreeSelectionChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous) -> void;
		auto OnTreeItemChanged(QTreeWidgetItem* item, int column) -> void;
		auto OnTreeItemDoubleClicked(QTreeWidgetItem* item, int column) -> void;
		auto OnTreeContextMenuRequested(const QPoint& pos) -> void;

	protected:
		auto OnNameChanged(const QString& name) -> void override;
		auto OnPipelineChanged(const Pipeline::PipelinePtr& pipeline) -> void override;

		auto OnFileAdded(const FileItemPtr& file) -> void override;
		auto OnFileUpdated(const FileItemPtr& file) -> void override;
		auto OnFileRemoved(const FileItemPtr& file) -> void override;
		auto OnFileLinked(const FileItemPtr& file, int timepoint) -> void override;
		auto OnFileUnlinked(const FileItemPtr& file, int timepoint) -> void override;
		auto OnGroupAdded(const QString& group) -> void override;
		auto OnGroupRemoved(const QString& group) -> void override;

		auto eventFilter(QObject* watched, QEvent* event) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
