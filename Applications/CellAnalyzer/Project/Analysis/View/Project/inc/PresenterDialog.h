#pragma once

#include <QDialog>
#include <QTreeWidgetItem>

#include "IServiceProvider.h"

#include "IPipeline.h"
#include "IView.h"

#include "CellAnalyzer.Project.Analysis.View.ProjectExport.h"

namespace CellAnalyzer::Project::Analysis::View {
	class CellAnalyzer_Project_Analysis_View_Project_API PresenterDialog final : public QDialog, public IView {
	public:
		explicit PresenterDialog(Tomocube::IServiceProvider* provider, const Pipeline::PipelinePtr& pipeline);
		~PresenterDialog() override;

		auto GetWindowName() const -> QString;
		auto GetPresenterID() const -> QString;
		auto GetDataLinkMap() const -> QMap<QString, QStringList>;//PortName, DataIDList

	protected slots:
		auto OnPresenterSelected(QTreeWidgetItem* current, QTreeWidgetItem* previous) -> void;
		auto OnSelectBtnClicked() -> void;
		auto OnCheckChanged(QTreeWidgetItem* item, int column) -> void;
		auto OnOkBtnClicked() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
