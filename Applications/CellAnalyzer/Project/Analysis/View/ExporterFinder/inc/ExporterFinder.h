#pragma once

#include <QDialog>
#include <QListWidgetItem>

#include "IExporter.h"
#include "IServiceProvider.h"
#include "IView.h"

#include "CellAnalyzer.Project.Analysis.View.ExporterFinderExport.h"

namespace CellAnalyzer::Project::Analysis::View {
	class CellAnalyzer_Project_Analysis_View_ExporterFinder_API ExporterFinder final : public QDialog, public IView {
	public:
		explicit ExporterFinder(Tomocube::IServiceProvider* provider);
		~ExporterFinder() override;

		auto SetVolume2DExporter(const QString& exporter) -> void;
		auto SetVolume3DExporter(const QString& exporter) -> void;
		auto SetMeasureExporter(const QString& exporter) -> void;

		auto GetVolume2DExporter() -> QString;
		auto GetVolume3DExporter() -> QString;
		auto GetMeasureExporter() -> QString;

		auto AddExportableData(const QString& name, const DataFlags& flag) -> void;

	protected slots:
		auto OnListItemChanged(QListWidgetItem* current, QListWidgetItem* previous) -> void;
		auto OnComboTextChanged(const QString& text) -> void;
		auto OnExecBtnClicked() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
