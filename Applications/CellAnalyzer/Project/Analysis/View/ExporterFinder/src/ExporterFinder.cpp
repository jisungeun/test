#include <QFileDialog>

#include "ExporterFinder.h"
#include "IExportService.h"

#include "ui_ExporterFinder.h"

namespace CellAnalyzer::Project::Analysis::View {
	struct ExporterFinder::Impl {
		Ui::ExporterFinder ui;
		Tomocube::IServiceProvider* provider = nullptr;

		QString volume2D;
		QString volume3D;
		QString measure;

		QMap<QString, DataFlags> dataMap;
	};

	ExporterFinder::ExporterFinder(Tomocube::IServiceProvider* provider) : QDialog(nullptr, Qt::Dialog | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint), IView(), d(new Impl) {
		d->provider = provider;
		d->ui.setupUi(this);
		d->ui.execBtn->setObjectName("accent_color");

		connect(d->ui.list, &QListWidget::currentItemChanged, this, &ExporterFinder::OnListItemChanged);
		connect(d->ui.combo, &QComboBox::currentTextChanged, this, &ExporterFinder::OnComboTextChanged);
		connect(d->ui.execBtn, &QPushButton::clicked, this, &ExporterFinder::OnExecBtnClicked);

		if (auto* item = d->ui.list->item(0))
			item->setSelected(true);

		const auto service = d->provider->GetService<IO::IExportService>();
		QStringList listVolume2D;
		for (const auto& ex : service->GetExporterList(DataFlag::Volume2D))
			listVolume2D.push_back(ex->GetName());
		if (false == listVolume2D.isEmpty()) {
			d->volume2D = listVolume2D.first();
		}

		QStringList listVolume3D;
		for (const auto& ex : service->GetExporterList(DataFlag::Volume3D))
			listVolume3D.push_back(ex->GetName());
		if (false == listVolume3D.isEmpty()) {
			d->volume3D = listVolume3D.first();
		}

		QStringList listMeasure;
		for (const auto& ex : service->GetExporterList(DataFlag::Measure))
			listMeasure.push_back(ex->GetName());
		if (false == listMeasure.isEmpty()) {
			d->measure = listMeasure.first();
		}
	}

	ExporterFinder::~ExporterFinder() = default;

	auto ExporterFinder::SetVolume2DExporter(const QString& exporter) -> void {
		if (false == exporter.isEmpty()) {
			d->volume2D = exporter;
		}
	}

	auto ExporterFinder::SetVolume3DExporter(const QString& exporter) -> void {
		if (false == exporter.isEmpty()) {
			d->volume3D = exporter;
		}
	}

	auto ExporterFinder::SetMeasureExporter(const QString& exporter) -> void {
		if (false == exporter.isEmpty()) {
			d->measure = exporter;
		}
	}

	auto ExporterFinder::GetVolume2DExporter() -> QString {
		return d->volume2D;
	}

	auto ExporterFinder::GetVolume3DExporter() -> QString {
		return d->volume3D;
	}

	auto ExporterFinder::GetMeasureExporter() -> QString {
		return d->measure;
	}

	auto ExporterFinder::AddExportableData(const QString& name, const DataFlags& flag) -> void {
		d->dataMap[name] = flag;
	}

	auto ExporterFinder::OnListItemChanged(QListWidgetItem* current, QListWidgetItem* previous) -> void {
		const auto service = d->provider->GetService<IO::IExportService>();
		const auto idx = d->ui.list->row(current);
		auto flag = DataFlag::Unknown;
		QString exporter;

		switch (idx) {
			case 0:
				flag = DataFlag::Volume2D;
				exporter = d->volume2D;
				break;
			case 1:
				flag = DataFlag::Volume3D;
				exporter = d->volume3D;
				break;
			case 2:
				flag = DataFlag::Measure;
				exporter = d->measure;
				break;
		}

		QStringList list;

		for (const auto& ex : service->GetExporterList(flag))
			list.push_back(ex->GetName());

		d->ui.combo->blockSignals(true);
		d->ui.combo->clear();
		d->ui.dataList->clear();
		d->ui.combo->addItems(list);
		if (list.indexOf(exporter) < 0) {
			if (list.count() > 0) {
				d->ui.combo->setCurrentIndex(0);
			}
		} else {
			d->ui.combo->setCurrentIndex(list.indexOf(exporter));
		}
		d->ui.combo->blockSignals(false);

		for (const auto& k : d->dataMap.keys()) {
			if (d->dataMap[k].testFlag(flag)) {
				if (d->dataMap[k].testFlag(DataFlag::Binary) || d->dataMap[k].testFlag(DataFlag::Label))
					d->ui.dataList->addItem(new QListWidgetItem(QIcon(":/Flat/Mask.svg"), k, d->ui.dataList));
				else if (d->dataMap[k].testFlag(DataFlag::Measure))
					d->ui.dataList->addItem(new QListWidgetItem(QIcon(":/Flat/Measure.svg"), k, d->ui.dataList));
				else
					d->ui.dataList->addItem(new QListWidgetItem(QIcon(":/Flat/Data.svg"), k, d->ui.dataList));
			}
		}
	}

	auto ExporterFinder::OnComboTextChanged(const QString& text) -> void {
		switch (d->ui.list->currentRow()) {
			case 0:
				d->volume2D = text;
				break;
			case 1:
				d->volume3D = text;
				break;
			case 2:
				d->measure = text;
				break;
		}
	}

	auto ExporterFinder::OnExecBtnClicked() -> void {
		accept();
	}
}
