#pragma once

#include "IService.h"

#include "CellAnalyzer.Project.Analysis.RepoModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_RepoModel_API IHistoryRepoEvent : public virtual Tomocube::IService {
	public:
		virtual auto OnHistoryCreated() -> void = 0;
	};
}
