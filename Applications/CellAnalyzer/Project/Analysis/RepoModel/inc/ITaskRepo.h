#pragma once

#include "IService.h"

#include "IFileItem.h"
#include "ITaskRepoEvent.h"

#include "CellAnalyzer.Project.Analysis.RepoModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_RepoModel_API ITaskRepo : public virtual Tomocube::IService {
	public:
		virtual auto AddEvent(const std::shared_ptr<ITaskRepoEvent>& event) -> void = 0;
		virtual auto RemoveEvent(const std::shared_ptr<ITaskRepoEvent>& event) -> void = 0;

		virtual auto Open(const QString& path) -> bool = 0;
		virtual auto Create(const QString& path) -> bool = 0;
		virtual auto Save() -> bool = 0;

		virtual auto GetLocation() const -> QString = 0;

		virtual auto GetPipeline() const -> Pipeline::PipelinePtr = 0;
		virtual auto SetPipeline(const Pipeline::PipelinePtr& pipeline) -> void = 0;

		virtual auto ContainsGroup(const QString& group) const -> bool = 0;
		virtual auto GetGroupList() const -> QStringList = 0;
		virtual auto GetGroup(const FileItemPtr& file) const -> QString = 0;

		virtual auto ContainsFile(const QString& filepath) const -> bool = 0;
		virtual auto GetFile(const QString& filepath) const -> FileItemPtr = 0;
		virtual auto GetFileList() const -> FileItemList = 0;
		virtual auto GetFileList(const QString& group) const -> FileItemList = 0;
		virtual auto GetLinkedFile() const -> FileItemPtr = 0;

		virtual auto AddGroup(const QString& group) -> void = 0;
		virtual auto RemoveGroup(const QString& group) -> void = 0;

		virtual auto AddFile(const QString& filepath) -> FileItemPtr = 0;
		virtual auto AddFile(const QString& group, const QString& filepath) -> FileItemPtr = 0;
		virtual auto RemoveFile(const FileItemPtr& file) -> void = 0;

		virtual auto Link(const FileItemPtr& file, int timepoint) -> bool = 0;
		virtual auto Unlink() -> void = 0;
	};
}