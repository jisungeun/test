#pragma once

#include "IService.h"

#include "IData.h"

#include "CellAnalyzer.Project.Analysis.RepoModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_RepoModel_API IExporterRepoEvent : public virtual Tomocube::IService {
	public:
		virtual auto OnExporterUpdated(DataFlag flag, const QString& exporter) -> void = 0;
	};
}
