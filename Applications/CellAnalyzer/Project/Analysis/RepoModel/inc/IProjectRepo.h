#pragma once

#include "IService.h"

#include "IProjectRepoEvent.h"

#include "CellAnalyzer.Project.Analysis.RepoModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_RepoModel_API IProjectRepo : public virtual Tomocube::IService {
	public:
		virtual auto AddEvent(const std::shared_ptr<IProjectRepoEvent>& event) -> void = 0;
		virtual auto RemoveEvent(const std::shared_ptr<IProjectRepoEvent>& event) -> void = 0;

		virtual auto Open(const QString& path) -> bool = 0;
		virtual auto Create(const QString& path, const QString& name) -> bool = 0;
		virtual auto Save() -> bool = 0;

		virtual auto GetLocation() const -> QString = 0;
		virtual auto GetExporterPath() const -> QString = 0;
		virtual auto GetTaskPath() const -> QString = 0;
		virtual auto GetHistoryPath() const -> QString = 0;

		virtual auto GetName() const -> QString = 0;
		virtual auto GetUser() const -> QString = 0;
		virtual auto GetDescription() const -> QString = 0;
		virtual auto GetCreationDateTime() const -> QDateTime = 0;

		virtual auto SetName(const QString& name) -> void = 0;
		virtual auto SetUser(const QString& user) -> void = 0;
		virtual auto SetDescription(const QString& desc) -> void = 0;
	};
}
