#pragma once

#include <memory>

#include <QString>

#include "CellAnalyzer.Project.Analysis.RepoModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class IFileItem;
	using FileItemPtr = std::shared_ptr<IFileItem>;
	using FileItemList = QVector<FileItemPtr>;

	enum class TaskType {
		HT2D,
		HT3D,
		FL2DCH0,
		FL2DCH1,
		FL2DCH2,
		FL3DCH0,
		FL3DCH1,
		FL3DCH2,
		BF,
		Unknown
	};

	class CellAnalyzer_Project_Analysis_RepoModel_API IFileItem {
	public:
		virtual ~IFileItem() = default;

		virtual auto GetFilePath() const -> QString = 0;
		virtual auto GetName() const -> QString = 0;
		virtual auto GetGroup() const -> QString = 0;
		virtual auto IsValid() const -> bool = 0;

		virtual auto SetGroup(const QString& group) -> void = 0;

		virtual auto ContainsTimePoint(int timepoint) const -> bool = 0;
		virtual auto GetTimePointList() const -> QList<int> = 0;
		virtual auto GetTypeList(int timepoint) const -> QStringList = 0;
		virtual auto GetType(int timepoint, const QString& name) const -> TaskType = 0;
		virtual auto GetTimeStep(int timepoint, const QString& name) -> int = 0;

		virtual auto SetType(int timepoint, const QString& name, TaskType type, int timestep) -> void = 0;

		virtual auto RemoveTimePoint(int timepoint) -> void = 0;
		virtual auto RemoveType(int timepoint, const QString& name) -> void = 0;

		virtual auto SetError(int timepoint, bool error) -> void = 0;
		virtual auto IsError(int timepoint) const -> bool = 0;

		virtual auto IsLinkable(int timepoint) const -> bool = 0;
		virtual auto IsLinked() const -> bool = 0;
		virtual auto IsLinked(int timepoint) const -> bool = 0;
		virtual auto GetLinkedTimePoint() const -> int = 0;

		virtual auto Link(int timepoint) -> bool = 0;
		virtual auto Unlink() -> void = 0;

		static auto ToTimeString(int timepoint) -> QString;
	};

	static auto ToString(TaskType type) -> QString {
		switch (type) {
			case TaskType::HT2D:
				return "HT2D";
			case TaskType::HT3D:
				return "HT3D";
			case TaskType::FL2DCH0:
				return "FL2DCH1";
			case TaskType::FL2DCH1:
				return "FL2DCH2";
			case TaskType::FL2DCH2:
				return "FL2DCH3";
			case TaskType::FL3DCH0:
				return "FL3DCH1";
			case TaskType::FL3DCH1:
				return "FL3DCH2";
			case TaskType::FL3DCH2:
				return "FL3DCH3";
			case TaskType::BF:
				return "BF";
			case TaskType::Unknown:
				return "Unknown";
		}

		return {};
	}

	static auto ToTaskType(const QString& string) -> TaskType {
		if (string == "HT2D")
			return TaskType::HT2D;
		if (string == "HT3D")
			return TaskType::HT3D;
		if (string == "FL2DCH1")
			return TaskType::FL2DCH0;
		if (string == "FL2DCH2")
			return TaskType::FL2DCH1;
		if (string == "FL2DCH3")
			return TaskType::FL2DCH2;
		if (string == "FL3DCH1")
			return TaskType::FL3DCH0;
		if (string == "FL3DCH2")
			return TaskType::FL3DCH1;
		if (string == "FL3DCH3")
			return TaskType::FL3DCH2;
		if (string == "BF")
			return TaskType::BF;

		return TaskType::Unknown;
	}
}
