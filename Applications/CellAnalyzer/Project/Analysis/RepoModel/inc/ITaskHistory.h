#pragma once

#include "IPipeline.h"

#include "CellAnalyzer.Project.Analysis.RepoModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class ITaskHistory;
	using TaskHistoryPtr = std::shared_ptr<ITaskHistory>;
	using TaskHistoryList = QVector<TaskHistoryPtr>;

	class CellAnalyzer_Project_Analysis_RepoModel_API ITaskHistory {
	public:
		virtual ~ITaskHistory() = default;

		virtual auto GetCreationDateTime() const -> QDateTime = 0;
		virtual auto GetPipelineLocation() const -> QString = 0;
		virtual auto GetPipeline() const -> Pipeline::PipelinePtr = 0;
		virtual auto GetLocation() const -> QString = 0;
		virtual auto IsFinished() const -> bool = 0;

		virtual auto ContainsFilePath(const QString& filepath) const -> bool = 0;
		virtual auto GetFilePathList() const -> QStringList = 0;

		virtual auto ContainsTimePoint(const QString& filepath, int timepoint) const -> bool = 0;
		virtual auto GetTimePointList(const QString& filepath) const -> QList<int> = 0;

		virtual auto ContainsProcess(const QString& filepath, int timepoint, const QString& process) const -> bool = 0;
		virtual auto GetProcessList(const QString& filepath, int timepoint) const -> QStringList = 0;

		virtual auto AddHistory(const QString& filepath, int timepoint, const QString& process) -> void = 0;
		virtual auto Finish() -> void = 0;
	};
}
