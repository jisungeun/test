#pragma once

#include "IHistoryRepoEvent.h"
#include "ITaskHistory.h"

#include "CellAnalyzer.Project.Analysis.RepoModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_RepoModel_API IHistoryRepo : public virtual Tomocube::IService {
	public:
		virtual auto AddEvent(const std::shared_ptr<IHistoryRepoEvent>& event) -> void = 0;
		virtual auto RemoveEvent(const std::shared_ptr<IHistoryRepoEvent>& event) -> void = 0;

		virtual auto Open(const QString& path) -> bool = 0;
		virtual auto Create(const QString& path) -> bool = 0;
		virtual auto Save() -> bool = 0;

		virtual auto GetLocation() const -> QString = 0;

		virtual auto GetHistoryList() const -> TaskHistoryList = 0;
		virtual auto GetHistory(const QDateTime& datetime) const -> TaskHistoryPtr = 0;

		virtual auto CreateHistory() -> TaskHistoryPtr = 0;
	};
}
