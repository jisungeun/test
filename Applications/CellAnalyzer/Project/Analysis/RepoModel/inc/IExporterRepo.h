#pragma once

#include "IExporter.h"
#include "IExporterRepoEvent.h"

#include "CellAnalyzer.Project.Analysis.RepoModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_RepoModel_API IExporterRepo : public virtual Tomocube::IService {
	public:
		virtual auto AddEvent(const std::shared_ptr<IExporterRepoEvent>& event) -> void = 0;
		virtual auto RemoveEvent(const std::shared_ptr<IExporterRepoEvent>& event) -> void = 0;

		virtual auto Open(const QString& path) -> bool = 0;
		virtual auto Create(const QString& path) -> bool = 0;
		virtual auto Save() -> bool = 0;

		virtual auto GetLocation() const -> QString = 0;

		virtual auto GetExporter(DataFlag flag) const -> QString = 0;
		virtual auto CreateExporter(DataFlag flag) const -> IO::ExporterPtr = 0;
		virtual auto SetExporter(DataFlag flag, const QString& id) -> void = 0;
	};
}
