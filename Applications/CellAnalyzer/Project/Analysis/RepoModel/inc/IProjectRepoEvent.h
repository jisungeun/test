#pragma once

#include "IService.h"

#include "IPipeline.h"

#include "CellAnalyzer.Project.Analysis.RepoModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_RepoModel_API IProjectRepoEvent : public virtual Tomocube::IService {
	public:
		virtual auto OnNameChanged(const QString& name) -> void;
		virtual auto OnUserChanged(const QString& user) -> void;
		virtual auto OnDescriptionChanged(const QString& desc) -> void;
	};
}
