#pragma once

#include "IService.h"

#include "IFileItem.h"
#include "IPipeline.h"

#include "CellAnalyzer.Project.Analysis.RepoModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_RepoModel_API ITaskRepoEvent : public virtual Tomocube::IService {
	public:
		virtual auto OnPipelineChanged(const Pipeline::PipelinePtr& pipeline) -> void;

		virtual auto OnFileAdded(const FileItemPtr& file) -> void;
		virtual auto OnFileUpdated(const FileItemPtr& file) -> void;
		virtual auto OnFileRemoved(const FileItemPtr& file) -> void;

		virtual auto OnFileLinked(const FileItemPtr& file, int timepoint) -> void;
		virtual auto OnFileUnlinked(const FileItemPtr& file, int timepoint) -> void;

		virtual auto OnGroupAdded(const QString& group) -> void;
		virtual auto OnGroupRemoved(const QString& group) -> void;
	};
}
