#include "ITaskRepoEvent.h"

namespace CellAnalyzer::Project::Analysis {
	auto ITaskRepoEvent::OnPipelineChanged(const Pipeline::PipelinePtr& pipeline) -> void {}

	auto ITaskRepoEvent::OnFileAdded(const FileItemPtr& file) -> void {}

	auto ITaskRepoEvent::OnFileUpdated(const FileItemPtr& file) -> void {}

	auto ITaskRepoEvent::OnFileRemoved(const FileItemPtr& file) -> void {}

	auto ITaskRepoEvent::OnFileLinked(const FileItemPtr& file, int timepoint) -> void {}

	auto ITaskRepoEvent::OnFileUnlinked(const FileItemPtr& file, int timepoint) -> void {}

	auto ITaskRepoEvent::OnGroupAdded(const QString& group) -> void {}

	auto ITaskRepoEvent::OnGroupRemoved(const QString& group) -> void {}
}
