#include "IFileItem.h"

namespace CellAnalyzer::Project::Analysis {
	auto IFileItem::ToTimeString(int timepoint) -> QString {
		const auto hour = timepoint / 3600;
		const auto mins = timepoint % 3600 / 60;
		const auto secs = timepoint % 60;

		return QString("%1:%2:%3").arg(hour, 2, 10, QLatin1Char('0')).arg(mins, 2, 10, QLatin1Char('0')).arg(secs, 2, 10, QLatin1Char('0'));
	}
}
