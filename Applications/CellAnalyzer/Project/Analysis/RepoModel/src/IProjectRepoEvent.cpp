#include "IProjectRepoEvent.h"

namespace CellAnalyzer::Project::Analysis {
	auto IProjectRepoEvent::OnNameChanged(const QString& name) -> void {}

	auto IProjectRepoEvent::OnUserChanged(const QString& user) -> void {}

	auto IProjectRepoEvent::OnDescriptionChanged(const QString& desc) -> void {}
}
