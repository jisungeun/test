#pragma once

#include "IServiceProvider.h"

#include "IFileItem.h"

#include "CellAnalyzer.Project.Analysis.Repo.InfoExport.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	class CellAnalyzer_Project_Analysis_Repo_Info_API FileItem final : public QObject, public std::enable_shared_from_this<FileItem>, public IFileItem {
		Q_OBJECT

	public:
		explicit FileItem(Tomocube::IServiceProvider* provider, const QString& filepath);
		explicit FileItem(Tomocube::IServiceProvider* provider, const QString& filepath, int64_t size);
		~FileItem() override;

		auto GetFileSize() const -> int64_t;

		auto GetFilePath() const -> QString override;
		auto GetName() const -> QString override;
		auto GetGroup() const -> QString override;
		auto IsValid() const -> bool override;

		auto SetGroup(const QString& group) -> void override;

		auto ContainsTimePoint(int timepoint) const -> bool override;
		auto GetTimePointList() const -> QList<int> override;
		auto GetTypeList(int timepoint) const -> QStringList override;
		auto GetType(int timepoint, const QString& name) const -> TaskType override;
		auto GetTimeStep(int timepoint, const QString& name) -> int override;

		auto SetType(int timepoint, const QString& name, TaskType type, int timestep) -> void override;

		auto RemoveTimePoint(int timepoint) -> void override;
		auto RemoveType(int timepoint, const QString& name) -> void override;

		auto SetError(int timepoint, bool error) -> void override;
		auto IsError(int timepoint) const -> bool override;

		auto IsLinkable(int timepoint) const -> bool override;
		auto IsLinked() const -> bool override;
		auto IsLinked(int timepoint) const -> bool override;
		auto GetLinkedTimePoint() const -> int override;

		auto Link(int timepoint) -> bool override;
		auto Unlink() -> void override;

	signals:
		auto GroupUpdated(const QString& group) -> void;
		auto TypeUpdated() -> void;
		auto Linked(int timepoint) -> void;
		auto Unlinked(int timepoint) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
