#pragma once

#include "IServiceProvider.h"

#include "IProjectRepo.h"

#include "CellAnalyzer.Project.Analysis.Repo.InfoExport.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	class CellAnalyzer_Project_Analysis_Repo_Info_API ProjectRepo final : public IProjectRepo {
	public:
		explicit ProjectRepo(Tomocube::IServiceProvider* provider);
		~ProjectRepo() override;

		auto AddEvent(const std::shared_ptr<IProjectRepoEvent>& event) -> void override;
		auto RemoveEvent(const std::shared_ptr<IProjectRepoEvent>& event) -> void override;

		auto Open(const QString& path) -> bool override;
		auto Create(const QString& path, const QString& name) -> bool override;
		auto Save() -> bool override;

		auto GetLocation() const -> QString override;
		auto GetExporterPath() const -> QString override;
		auto GetTaskPath() const -> QString override;
		auto GetHistoryPath() const -> QString override;

		auto GetName() const -> QString override;
		auto GetUser() const -> QString override;
		auto GetDescription() const -> QString override;
		auto GetCreationDateTime() const -> QDateTime override;

		auto SetName(const QString& name) -> void override;
		auto SetUser(const QString& user) -> void override;
		auto SetDescription(const QString& desc) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
