#pragma once

#include "IServiceProvider.h"

#include "IExporterRepo.h"

#include "CellAnalyzer.Project.Analysis.Repo.InfoExport.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	class CellAnalyzer_Project_Analysis_Repo_Info_API ExporterRepo final : public IExporterRepo {
	public:
		explicit ExporterRepo(Tomocube::IServiceProvider* provider);
		~ExporterRepo() override;

		auto AddEvent(const std::shared_ptr<IExporterRepoEvent>& event) -> void override;
		auto RemoveEvent(const std::shared_ptr<IExporterRepoEvent>& event) -> void override;

		auto Open(const QString& path) -> bool override;
		auto Create(const QString& path) -> bool override;
		auto Save() -> bool override;

		auto GetLocation() const -> QString override;

		auto GetExporter(DataFlag flag) const -> QString override;
		auto CreateExporter(DataFlag flag) const -> IO::ExporterPtr override;
		auto SetExporter(DataFlag flag, const QString& id) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
