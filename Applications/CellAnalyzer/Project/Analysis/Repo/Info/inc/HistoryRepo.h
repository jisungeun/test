#pragma once

#include "IServiceProvider.h"

#include "IHistoryRepo.h"

#include "CellAnalyzer.Project.Analysis.Repo.InfoExport.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	class CellAnalyzer_Project_Analysis_Repo_Info_API HistoryRepo final : public IHistoryRepo {
	public:
		explicit HistoryRepo(Tomocube::IServiceProvider* provider);
		~HistoryRepo() override;

		auto AddEvent(const std::shared_ptr<IHistoryRepoEvent>& event) -> void override;
		auto RemoveEvent(const std::shared_ptr<IHistoryRepoEvent>& event) -> void override;

		auto Open(const QString& path) -> bool override;
		auto Create(const QString& path) -> bool override;
		auto Save() -> bool override;

		auto GetLocation() const -> QString override;

		auto GetHistoryList() const -> TaskHistoryList override;
		auto GetHistory(const QDateTime& datetime) const -> TaskHistoryPtr override;

		auto CreateHistory() -> TaskHistoryPtr override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}