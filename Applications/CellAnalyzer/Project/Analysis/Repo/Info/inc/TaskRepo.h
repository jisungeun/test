#pragma once

#include "IServiceProvider.h"

#include "ITaskRepo.h"
#include "ITaskRepoEvent.h"

#include "FileItem.h"

#include "CellAnalyzer.Project.Analysis.Repo.InfoExport.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	class CellAnalyzer_Project_Analysis_Repo_Info_API TaskRepo final : public QObject, public ITaskRepo {
	public:
		explicit TaskRepo(Tomocube::IServiceProvider* provider);
		~TaskRepo() override;

		auto AddEvent(const std::shared_ptr<ITaskRepoEvent>& event) -> void override;
		auto RemoveEvent(const std::shared_ptr<ITaskRepoEvent>& event) -> void override;

		auto Open(const QString& path) -> bool override;
		auto Create(const QString& path) -> bool override;
		auto Save() -> bool override;
		auto Load() -> void;

		auto GetLocation() const -> QString override;

		auto GetPipeline() const -> Pipeline::PipelinePtr override;
		auto SetPipeline(const Pipeline::PipelinePtr& pipeline) -> void override;

		auto ContainsGroup(const QString& group) const -> bool override;
		auto GetGroupList() const -> QStringList override;
		auto GetGroup(const FileItemPtr& file) const -> QString override;

		auto ContainsFile(const QString& filepath) const -> bool override;
		auto GetFile(const QString& filepath) const -> FileItemPtr override;
		auto GetFileList() const -> FileItemList override;
		auto GetFileList(const QString& group) const -> FileItemList override;
		auto GetLinkedFile() const -> FileItemPtr override;

		auto AddGroup(const QString& group) -> void override;
		auto RemoveGroup(const QString& group) -> void override;

		auto AddFile(const QString& filepath) -> FileItemPtr override;
		auto AddFile(const QString& group, const QString& filepath) -> FileItemPtr override;
		auto RemoveFile(const FileItemPtr& file) -> void override;

		auto Link(const FileItemPtr& file, int timepoint) -> bool override;
		auto Unlink() -> void override;

	protected slots:
		auto OnGroupUpdated(const QString& group) -> void;
		auto OnTypeUpdated() -> void;
		auto OnLinked(int timepoint) -> void;
		auto OnUnlinked(int timepoint) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
