#pragma once

#include "IServiceProvider.h"

#include "ITaskHistory.h"

#include "CellAnalyzer.Project.Analysis.Repo.InfoExport.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	class CellAnalyzer_Project_Analysis_Repo_Info_API TaskHistory final : public ITaskHistory {
	public:
		TaskHistory(Tomocube::IServiceProvider* provider, const QString& basePath, const QDateTime& datetime, bool finished);
		~TaskHistory() override;

		auto GetCreationDateTime() const -> QDateTime override;
		auto GetPipelineLocation() const -> QString override;
		auto GetPipeline() const -> Pipeline::PipelinePtr override;
		auto GetLocation() const -> QString override;
		auto IsFinished() const -> bool override;

		auto ContainsFilePath(const QString& filepath) const -> bool override;
		auto GetFilePathList() const -> QStringList override;

		auto ContainsTimePoint(const QString& filepath, int timepoint) const -> bool override;
		auto GetTimePointList(const QString& filepath) const -> QList<int> override;

		auto ContainsProcess(const QString& filepath, int timepoint, const QString& process) const -> bool override;
		auto GetProcessList(const QString& filepath, int timepoint) const -> QStringList override;

		auto AddHistory(const QString& filepath, int timepoint, const QString& process) -> void override;
		auto Finish() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
