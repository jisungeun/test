#include "ProjectRepo.h"

#include <QDir>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include "IExporterRepo.h"
#include "IHistoryRepo.h"
#include "ITaskRepo.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	struct ProjectRepo::Impl {
		QVector<std::shared_ptr<IProjectRepoEvent>> events;
		Tomocube::IServiceProvider* provider = nullptr;

		QString path;
		QString name;
		QString user;
		QString desc;
		QDateTime created;
	};

	ProjectRepo::ProjectRepo(Tomocube::IServiceProvider* provider) : IProjectRepo(), d(new Impl) {
		d->provider = provider;
	}

	ProjectRepo::~ProjectRepo() = default;

	auto ProjectRepo::AddEvent(const std::shared_ptr<IProjectRepoEvent>& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto ProjectRepo::RemoveEvent(const std::shared_ptr<IProjectRepoEvent>& event) -> void {
		d->events.removeOne(event);
	}

	auto ProjectRepo::Open(const QString& path) -> bool {
		d->path.clear();
		d->name.clear();
		d->user.clear();
		d->desc.clear();
		d->created = {};

		const auto tapPath = QFileInfo(path).completeSuffix() == "tap" ?path : QString("%1/.tap").arg(path);

		if (QFile file(tapPath); file.open(QIODevice::ReadOnly)) {
			QJsonParseError error;

			if (const auto doc = QJsonDocument::fromJson(file.readAll(), &error); error.error == QJsonParseError::NoError) {
				const auto obj = doc.object().toVariantMap();

				d->path = QFileInfo(tapPath).absolutePath();
				d->name = obj["Name"].toString();
				d->user = obj["User"].toString();
				d->desc = obj["Description"].toString();
				d->created = QDateTime::fromMSecsSinceEpoch(obj["Created"].toLongLong());

				const auto repoPath = obj["RepoPath"].toMap();

				if (const auto repo = d->provider->GetService<IExporterRepo>(); !repo->Open(repoPath["Exporter"].toString()))
					return false;

				if (const auto repo = d->provider->GetService<ITaskRepo>(); !repo->Open(repoPath["Task"].toString()))
					return false;

				if (const auto repo = d->provider->GetService<IHistoryRepo>(); !repo->Open(repoPath["History"].toString()))
					return false;

				if (!d->name.isEmpty())
					return true;
			}
		}

		return false;
	}

	auto ProjectRepo::Create(const QString& path, const QString& name) -> bool {
		const QDir dir(path);
		dir.mkpath(".");

		if (QFile file(dir.filePath(".tap")); dir.isEmpty() && file.open(QIODevice::WriteOnly | QIODevice::NewOnly)) {
			const auto task = d->provider->GetService<ITaskRepo>();
			const auto exporter = d->provider->GetService<IExporterRepo>();
			const auto history = d->provider->GetService<IHistoryRepo>();

			QJsonObject obj;
			QJsonObject repo;
			obj["Name"] = name;
			obj["Created"] = QDateTime::currentMSecsSinceEpoch();
			repo["Task"] = dir.relativeFilePath(".tctsk");
			repo["Exporter"] = dir.relativeFilePath(".tcexp");
			repo["History"] = dir.relativeFilePath(".tchst");
			obj["RepoPath"] = repo;

			task->Create(dir.absoluteFilePath(".tctsk"));
			exporter->Create(dir.absoluteFilePath(".tcexp"));
			history->Create(dir.absoluteFilePath(".tchst"));

			const QJsonDocument doc(obj);
			file.write(doc.toJson());

			if (file.flush())
				return true;
		}

		return false;
	}

	auto ProjectRepo::Save() -> bool {
		if (QFile file(QString("%1/.tap").arg(d->path)); file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
			QJsonObject obj;
			obj["Name"] = d->name;
			obj["User"] = d->user;
			obj["Description"] = d->desc;
			obj["Created"] = d->created.toMSecsSinceEpoch();

			QJsonObject repoPath;
			repoPath["Exporter"] = GetExporterPath();
			repoPath["Task"] = GetTaskPath();
			repoPath["History"] = GetHistoryPath();
			obj["RepoPath"] = repoPath;

			const QJsonDocument doc(obj);
			file.write(doc.toJson());

			if (file.flush())
				return true;
		}

		return false;
	}

	auto ProjectRepo::GetLocation() const -> QString {
		return d->path;
	}

	auto ProjectRepo::GetExporterPath() const -> QString {
		if (const auto repo = d->provider->GetService<IExporterRepo>())
			return repo->GetLocation();

		return {};
	}

	auto ProjectRepo::GetTaskPath() const -> QString {
		if (const auto repo = d->provider->GetService<ITaskRepo>())
			return repo->GetLocation();

		return {};
	}

	auto ProjectRepo::GetHistoryPath() const -> QString {
		if (const auto repo = d->provider->GetService<IHistoryRepo>())
			return repo->GetLocation();

		return {};
	}

	auto ProjectRepo::GetName() const -> QString {
		return d->name;
	}

	auto ProjectRepo::GetUser() const -> QString {
		return d->user;
	}

	auto ProjectRepo::GetDescription() const -> QString {
		return d->desc;
	}

	auto ProjectRepo::GetCreationDateTime() const -> QDateTime {
		return d->created;
	}

	auto ProjectRepo::SetName(const QString& name) -> void {
		d->name = name;

		for (const auto& e : d->events)
			e->OnNameChanged(name);
	}

	auto ProjectRepo::SetUser(const QString& user) -> void {
		d->user = user;

		for (const auto& e : d->events)
			e->OnUserChanged(user);
	}

	auto ProjectRepo::SetDescription(const QString& desc) -> void {
		d->desc = desc;

		for (const auto& e : d->events)
			e->OnDescriptionChanged(desc);
	}
}
