#include "TaskHistory.h"

#include <QDir>

#include "IPipelineService.h"
#include "ITaskRepo.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	struct TaskHistory::Impl {
		Tomocube::IServiceProvider* provider = nullptr;

		QString basePath;
		QDateTime datetime = QDateTime::currentDateTime();
		bool finished = false;

		QMap<QString, QMap<int, QStringList>> fileMap;
	};

	TaskHistory::TaskHistory(Tomocube::IServiceProvider* provider, const QString& basePath, const QDateTime& datetime, bool finished) : ITaskHistory(), d(new Impl) {
		d->provider = provider;
		d->basePath = QDir(QFileInfo(basePath).absolutePath() + "/Export").filePath(datetime.toString("yyyyMMdd_HHmmss"));
		d->datetime = datetime;
		d->finished = finished;
	}

	TaskHistory::~TaskHistory() = default;

	auto TaskHistory::GetCreationDateTime() const -> QDateTime {
		return d->datetime;
	}

	auto TaskHistory::GetPipelineLocation() const -> QString {
		return QDir(d->basePath).filePath(".tcap");
	}

	auto TaskHistory::GetPipeline() const -> Pipeline::PipelinePtr {
		const auto service = d->provider->GetService<Pipeline::IPipelineService>();

		if (auto pip = service->Read(GetPipelineLocation())) {
			const auto task = d->provider->GetService<ITaskRepo>();
			const auto file = task->GetLinkedFile();

			if (!file)
				return nullptr;

			const auto name = file->GetName();
			const auto tp = file->GetLinkedTimePoint();

			if (tp < 0)
				return nullptr;

			for (const auto& p : pip->GetProcessList()) {
				const auto proc = pip->GetProcess(p);
				QDir dir(QDir(GetLocation()).filePath(p));

				for (const auto& o : proc->GetOutputList()) {
					const auto output = proc->GetOutput(o);
					const auto filename = dir.filePath(QString("%1_%2_%3").arg(output->GetName()).arg(name).arg(tp));

					// Load data from filename (No Suffix)
				}
			}

			return pip;
		}

		return {};
	}

	auto TaskHistory::GetLocation() const -> QString {
		return d->basePath;
	}

	auto TaskHistory::IsFinished() const -> bool {
		return d->finished;
	}

	auto TaskHistory::ContainsFilePath(const QString& filepath) const -> bool {
		return d->fileMap.contains(filepath);
	}

	auto TaskHistory::GetFilePathList() const -> QStringList {
		return d->fileMap.keys();
	}

	auto TaskHistory::ContainsTimePoint(const QString& filepath, int timepoint) const -> bool {
		if (d->fileMap.contains(filepath))
			return d->fileMap[filepath].contains(timepoint);

		return false;
	}

	auto TaskHistory::GetTimePointList(const QString& filepath) const -> QList<int> {
		if (d->fileMap.contains(filepath))
			return d->fileMap[filepath].keys();

		return {};
	}

	auto TaskHistory::ContainsProcess(const QString& filepath, int timepoint, const QString& process) const -> bool {
		if (d->fileMap.contains(filepath)) {
			if (d->fileMap[filepath].contains(timepoint))
				return d->fileMap[filepath][timepoint].contains(process);
		}

		return false;
	}

	auto TaskHistory::GetProcessList(const QString& filepath, int timepoint) const -> QStringList {
		if (d->fileMap.contains(filepath)) {
			if (d->fileMap[filepath].contains(timepoint))
				return d->fileMap[filepath][timepoint];
		}

		return {};
	}

	auto TaskHistory::AddHistory(const QString& filepath, int timepoint, const QString& process) -> void {
		if (!d->fileMap[filepath][timepoint].contains(process))
			d->fileMap[filepath][timepoint].push_back(process);
	}

	auto TaskHistory::Finish() -> void {
		d->finished = true;
	}
}
