#include <QDir>
#include <QFileInfo>

#include "FileItem.h"

#include <QThread>

#include "BF.h"
#include "FL2D.h"
#include "FL3D.h"
#include "HT2D.h"
#include "HT3D.h"

#include "IEditorService.h"
#include "IProjectRepo.h"
#include "ITaskRepo.h"

#include "TCFMetaReader.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	struct TaskStruct {
		TaskType type = TaskType::Unknown;
		int timestep = 0;
	};

	struct FileItem::Impl {
		Tomocube::IServiceProvider* provider = nullptr;

		QString filepath;
		int64_t size = 0;
		int linked = -1;

		QList<int> errorList;
		QMap<int, QMap<QString, TaskStruct>> taskMap;

		auto IsRoiExist(int timepoint) const -> std::tuple<bool, QString>;
		auto ApplyRoi(const QString& roiPath, const QString& name, const DataPtr& data) const -> DataPtr;
	};

	auto FileItem::Impl::IsRoiExist(int timepoint) const -> std::tuple<bool, QString> {
		const auto projRepo = provider->GetService<IProjectRepo>();

		if (!filepath.isEmpty()) {
			const QFileInfo fileInfo(filepath);

			TC::IO::TCFMetaReader reader;
			const auto meta = reader.Read(filepath);
			const auto name = fileInfo.fileName().chopped(4);
			const auto absoluteTime = timepoint;

			if (absoluteTime < 0)
				return std::make_tuple(false, QString());

			const auto roiPath = QDir(projRepo->GetLocation()).filePath("ROI");
			const QDir roiDir(roiPath);
			const auto roiFilePath = roiDir.filePath(QString("%1_%2.tcroi").arg(fileInfo.fileName().chopped(4)).arg(absoluteTime));

			return std::make_tuple(QFileInfo::exists(roiFilePath), roiFilePath);
		}

		return std::make_tuple(false, QString());
	}

	auto FileItem::Impl::ApplyRoi(const QString& roiPath, const QString& name, const DataPtr& data) const -> DataPtr {
		const auto service = provider->GetService<Editor::IEditorService>();

		const auto cutter = service->CreateEditor("ROICookieCutter");
		QVariantMap propMap;
		propMap["ROIPath"] = roiPath;
		cutter->SetProperty(propMap);

		cutter->AddData(name, data, "List of Images");
		auto result = cutter->GetResult("");

		return result;
	}

	FileItem::FileItem(Tomocube::IServiceProvider* provider, const QString& filepath) : QObject(), std::enable_shared_from_this<FileItem>(), IFileItem(), d(new Impl) {
		d->provider = provider;
		d->filepath = filepath;
		d->size = QFile(filepath).size();
	}

	FileItem::FileItem(Tomocube::IServiceProvider* provider, const QString& filepath, int64_t size) : QObject(), std::enable_shared_from_this<FileItem>(), IFileItem(), d(new Impl) {
		d->provider = provider;
		d->filepath = filepath;
		d->size = size;
	}

	FileItem::~FileItem() = default;

	auto FileItem::GetFileSize() const -> int64_t {
		return d->size;
	}

	auto FileItem::GetFilePath() const -> QString {
		return d->filepath;
	}

	auto FileItem::GetName() const -> QString {
		return QFileInfo(d->filepath).completeBaseName();
	}

	auto FileItem::GetGroup() const -> QString {
		const auto repo = d->provider->GetService<ITaskRepo>();
		return repo->GetGroup(std::const_pointer_cast<FileItem>(shared_from_this()));
	}

	auto FileItem::IsValid() const -> bool {
		return QFile(d->filepath).size() == d->size;
	}

	auto FileItem::SetGroup(const QString& group) -> void {
		emit GroupUpdated(group);
	}

	auto FileItem::ContainsTimePoint(int timepoint) const -> bool {
		return d->taskMap.contains(timepoint);
	}

	auto FileItem::GetTimePointList() const -> QList<int> {
		return d->taskMap.keys();
	}

	auto FileItem::GetTypeList(int timepoint) const -> QStringList {
		if (d->taskMap.contains(timepoint))
			return d->taskMap[timepoint].keys();

		return {};
	}

	auto FileItem::GetType(int timepoint, const QString& name) const -> TaskType {
		if (d->taskMap.contains(timepoint) && d->taskMap[timepoint].contains(name))
			return d->taskMap[timepoint][name].type;

		return {};
	}

	auto FileItem::GetTimeStep(int timepoint, const QString& name) -> int {
		if (d->taskMap.contains(timepoint) && d->taskMap[timepoint].contains(name))
			return d->taskMap[timepoint][name].timestep;

		return {};
	}

	auto FileItem::SetType(int timepoint, const QString& name, TaskType type, int timestep) -> void {
		d->taskMap[timepoint][name] = { type, timestep };

		emit TypeUpdated();
	}

	auto FileItem::RemoveTimePoint(int timepoint) -> void {
		if (const auto map = d->taskMap.take(timepoint); !map.isEmpty()) {
			if (d->linked == timepoint) {
				const auto temp = d->linked;
				d->linked = -1;
				emit Unlinked(temp);
			}

			emit TypeUpdated();
		}
	}

	auto FileItem::RemoveType(int timepoint, const QString& name) -> void {
		if (d->taskMap.contains(timepoint) && d->taskMap[timepoint].contains(name)) {
			const auto type = d->taskMap[timepoint].take(name);
			emit TypeUpdated();
		}
	}

	auto FileItem::SetError(int timepoint, bool error) -> void {
		if (error)
			d->errorList.push_back(timepoint);
		else
			d->errorList.removeOne(timepoint);
	}

	auto FileItem::IsError(int timepoint) const -> bool {
		return d->errorList.contains(timepoint);
	}

	auto FileItem::IsLinkable(int timepoint) const -> bool {
		if (IsValid() && d->taskMap.contains(timepoint)) {
			const auto repo = d->provider->GetService<ITaskRepo>();

			if (const auto pipeline = repo->GetPipeline()) {
				if (const auto srcs = pipeline->GetSourceList(); srcs.count() == d->taskMap[timepoint].count()) {
					for (const auto& s : srcs) {
						if (!d->taskMap[timepoint].contains(s))
							return false;

						const auto src = pipeline->GetSource(s);

						switch (const auto flags = src->GetFlags(); d->taskMap[timepoint][s].type) {
							case TaskType::HT2D:
							case TaskType::HT3D:
								if (!flags.testFlag(DataFlag::HT))
									return false;
								break;
							case TaskType::FL2DCH0:
							case TaskType::FL2DCH1:
							case TaskType::FL2DCH2:
							case TaskType::FL3DCH0:
							case TaskType::FL3DCH1:
							case TaskType::FL3DCH2:
								if (!flags.testFlag(DataFlag::FL))
									return false;
								break;
							case TaskType::BF:
								if (!flags.testFlag(DataFlag::BF))
									return false;
								break;
						}

						switch (const auto flags = src->GetFlags(); d->taskMap[timepoint][s].type) {
							case TaskType::HT2D:
							case TaskType::FL2DCH0:
							case TaskType::FL2DCH1:
							case TaskType::FL2DCH2:
							case TaskType::BF:
								if (!flags.testFlag(DataFlag::Volume2D))
									return false;
								break;
							case TaskType::HT3D:
							case TaskType::FL3DCH0:
							case TaskType::FL3DCH1:
							case TaskType::FL3DCH2:
								if (!flags.testFlag(DataFlag::Volume3D))
									return false;
								break;
						}
					}

					return true;
				}
			}
		}

		return false;
	}

	auto FileItem::IsLinked() const -> bool {
		return d->linked > -1;
	}

	auto FileItem::IsLinked(int timepoint) const -> bool {
		return d->linked == timepoint;
	}

	auto FileItem::GetLinkedTimePoint() const -> int {
		return d->linked;
	}

	auto FileItem::Link(int timepoint) -> bool {
		if (!IsLinkable(timepoint))
			return false;

		if (d->linked == timepoint)
			return true;

		if (d->taskMap.contains(timepoint)) {
			const auto repo = d->provider->GetService<ITaskRepo>();

			if (const auto pipeline = repo->GetPipeline()) {
				repo->Unlink();

				for (const auto& s : pipeline->GetSourceList()) {
					const auto src = pipeline->GetSource(s);
					const auto flags = src->GetFlags();

					DataPtr data = nullptr;

					if (flags.testFlag(DataFlag::HT)) {
						if (flags.testFlag(DataFlag::Volume2D))
							data = std::make_shared<Data::HT2D>(d->filepath, d->taskMap[timepoint][s].timestep);
						else if (flags.testFlag(DataFlag::Volume3D))
							data = std::make_shared<Data::HT3D>(d->filepath, d->taskMap[timepoint][s].timestep);
					} else if (flags.testFlag(DataFlag::FL)) {
						auto chIdx = -1;

						switch (d->taskMap[timepoint][s].type) {
							case TaskType::FL2DCH0:
							case TaskType::FL3DCH0:
								chIdx = 0;
								break;
							case TaskType::FL2DCH1:
							case TaskType::FL3DCH1:
								chIdx = 1;
								break;
							case TaskType::FL2DCH2:
							case TaskType::FL3DCH2:
								chIdx = 2;
								break;
						}

						if (chIdx > -1) {
							if (flags.testFlag(DataFlag::Volume2D))
								data = std::make_shared<Data::FL2D>(d->filepath, chIdx, d->taskMap[timepoint][s].timestep);
							else if (flags.testFlag(DataFlag::Volume3D))
								data = std::make_shared<Data::FL3D>(d->filepath, chIdx, d->taskMap[timepoint][s].timestep);
						}
					} else if (flags.testFlag(DataFlag::BF)) {
						data = std::make_shared<Data::BF>(d->filepath, d->taskMap[timepoint][s].timestep);
					}

					if (const auto [exists, path] = d->IsRoiExist(timepoint); exists) {
						if (QThread::currentThread() == thread())
							data = d->ApplyRoi(path, src->GetName(), data);
						else {
							IView::Run([this, path, name = src->GetName(), &data] {
								data = d->ApplyRoi(path, name, data);
							}, Qt::BlockingQueuedConnection);
						}
					}

					src->SetData(data);
				}

				d->linked = timepoint;
				emit Linked(timepoint);
				return true;
			}
		}

		return false;
	}

	auto FileItem::Unlink() -> void {
		if (d->linked > -1) {
			const auto repo = d->provider->GetService<ITaskRepo>();

			if (const auto pipeline = repo->GetPipeline()) {
				for (const auto& s : pipeline->GetSourceList()) {
					const auto src = pipeline->GetSource(s);
					src->SetData(nullptr);
				}
			}

			const auto linked = d->linked;
			d->linked = -1;
			emit Unlinked(linked);
		}
	}
}
