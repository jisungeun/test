#include "ExporterRepo.h"

#include <QDir>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonObject>

#include "IExportService.h"
#include "IProjectRepo.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	struct ExporterRepo::Impl {
		QVector<std::shared_ptr<IExporterRepoEvent>> events;
		Tomocube::IServiceProvider* provider = nullptr;

		QString path;
		QString volume2d;
		QString volume3d;
		QString measure;

		auto GetAbsolutePath(const QString& path) const -> QString;
	};

	auto ExporterRepo::Impl::GetAbsolutePath(const QString& path) const -> QString {
		if (const QFileInfo info(path); info.isRelative()) {
			const auto repo = provider->GetService<IProjectRepo>();
			const QDir dir(repo->GetLocation());
			return dir.filePath(path);
		}

		return path;
	}

	ExporterRepo::ExporterRepo(Tomocube::IServiceProvider* provider) : IExporterRepo(), d(new Impl) {
		d->provider = provider;
	}

	ExporterRepo::~ExporterRepo() = default;

	auto ExporterRepo::AddEvent(const std::shared_ptr<IExporterRepoEvent>& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto ExporterRepo::RemoveEvent(const std::shared_ptr<IExporterRepoEvent>& event) -> void {
		d->events.removeOne(event);
	}

	auto ExporterRepo::Open(const QString& path) -> bool {
		d->path.clear();
		d->volume2d.clear();
		d->volume3d.clear();
		d->measure.clear();

		if (QFile file(d->GetAbsolutePath(path)); file.open(QIODevice::ReadOnly)) {
			QJsonParseError error;

			if (const auto doc = QJsonDocument::fromJson(file.readAll(), &error); error.error == QJsonParseError::NoError) {
				const auto obj = doc.object().toVariantMap();

				d->path = d->GetAbsolutePath(path);
				d->volume2d = obj["Volume2D"].toString();
				d->volume3d = obj["Volume3D"].toString();
				d->measure = obj["Measure"].toString();

				return true;
			}
		}

		return false;
	}

	auto ExporterRepo::Create(const QString& path) -> bool {
		if (QFile file(d->GetAbsolutePath(path)); file.open(QIODevice::WriteOnly | QIODevice::NewOnly)) {
			const QJsonObject obj;
			obj["Volume2D"] = "TIFF";
			obj["Volume3D"] = "TIFF";
			obj["Measure"] = "Csv File";

			const QJsonDocument doc(obj);
			file.write(doc.toJson());

			if (file.flush())
				return true;
		}

		return false;
	}

	auto ExporterRepo::Save() -> bool {
		if (QFile file(d->GetAbsolutePath(d->path)); file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
			QJsonObject obj;
			obj["Volume2D"] = d->volume2d;
			obj["Volume3D"] = d->volume3d;
			obj["Measure"] = d->measure;

			const QJsonDocument doc(obj);
			file.write(doc.toJson());

			if (file.flush())
				return true;
		}

		return false;
	}

	auto ExporterRepo::GetLocation() const -> QString {
		const auto repo = d->provider->GetService<IProjectRepo>();
		return QDir(repo->GetLocation()).relativeFilePath(d->path);
	}

	auto ExporterRepo::GetExporter(DataFlag flag) const -> QString {
		switch (flag) {
			case DataFlag::Volume2D:
				return d->volume2d;
			case DataFlag::Volume3D:
				return d->volume3d;
			case DataFlag::Measure:
				return d->measure;
		}

		return {};
	}

	auto ExporterRepo::CreateExporter(DataFlag flag) const -> IO::ExporterPtr {
		switch (const auto service = d->provider->GetService<IO::IExportService>(); flag) {
			case DataFlag::Volume2D:
				return service->GetExporter(d->volume2d);
			case DataFlag::Volume3D:
				return service->GetExporter(d->volume3d);
			case DataFlag::Measure:
				return service->GetExporter(d->measure);
		}

		return {};
	}

	auto ExporterRepo::SetExporter(DataFlag flag, const QString& id) -> void {
		switch (flag) {
			case DataFlag::Volume2D:
				d->volume2d = id;
				break;
			case DataFlag::Volume3D:
				d->volume3d = id;
				break;
			case DataFlag::Measure:
				d->measure = id;
				break;
			default:
				return;
		}

		for (const auto& e : d->events)
			e->OnExporterUpdated(flag, id);
	}
}
