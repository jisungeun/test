#include <QDir>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMutexLocker>

#include "TaskRepo.h"
#include "FileItem.h"

#include "IPipelineService.h"
#include "IProjectRepo.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	struct TaskRepo::Impl {
		QVector<std::shared_ptr<ITaskRepoEvent>> events;
		Tomocube::IServiceProvider* provider = nullptr;
		Pipeline::PipelinePtr pipeline = nullptr;

		QString path;
		QMap<QString, FileItemList> fileMap;

		QMutex mutex;

		auto GetAbsolutePath(const QString& filename) const -> QString;
		auto GetFileItemPtr(const FileItem* item) const -> FileItemPtr;
	};

	auto TaskRepo::Impl::GetAbsolutePath(const QString& filename) const -> QString {
		if (const QFileInfo info(filename); info.isRelative()) {
			const auto repo = provider->GetService<IProjectRepo>();
			const QDir dir(repo->GetLocation());
			return dir.filePath(filename);
		}

		return filename;
	}

	auto TaskRepo::Impl::GetFileItemPtr(const FileItem* item) const -> FileItemPtr {
		for (const auto& g : fileMap.keys()) {
			for (const auto& f : fileMap[g]) {
				if (f.get() == item)
					return f;
			}
		}

		return {};
	}

	TaskRepo::TaskRepo(Tomocube::IServiceProvider* provider) : QObject(), ITaskRepo(), d(new Impl) {
		d->provider = provider;
	}

	TaskRepo::~TaskRepo() = default;

	auto TaskRepo::AddEvent(const std::shared_ptr<ITaskRepoEvent>& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto TaskRepo::RemoveEvent(const std::shared_ptr<ITaskRepoEvent>& event) -> void {
		d->events.removeOne(event);
	}

	auto TaskRepo::Open(const QString& path) -> bool {
		QMutexLocker locker(&d->mutex);

		d->pipeline = nullptr;
		d->path.clear();
		d->fileMap.clear();

		if (QFile file(d->GetAbsolutePath(path)); file.open(QIODevice::ReadOnly)) {
			QJsonParseError error;

			if (const auto doc = QJsonDocument::fromJson(file.readAll(), &error); error.error == QJsonParseError::NoError) {
				const auto service = d->provider->GetService<Pipeline::IPipelineService>();
				const auto obj = doc.object().toVariantMap();

				d->path = d->GetAbsolutePath(path);

				if (const auto pip = service->Read(obj["Pipeline"].toString()))
					d->pipeline = pip;

				for (const auto& t : obj["TaskList"].toList()) {
					const auto taskMap = t.toMap();
					const auto name = taskMap["FilePath"].toString();
					const auto group = taskMap["Group"].toString();
					const auto size = taskMap["Size"].toLongLong();
					const auto item = std::make_shared<FileItem>(d->provider, name, size);
					d->fileMap[group].push_back(item);

					for (const auto& w : taskMap["WorkList"].toList()) {
						const auto workMap = w.toMap();
						const auto timepoint = workMap["TimePoint"].toInt();

						for (const auto& i : workMap["TypeList"].toList()) {
							const auto typeMap = i.toMap();
							const auto source = typeMap["Name"].toString();
							const auto type = ToTaskType(typeMap["Type"].toString());
							const auto timestep = typeMap["TimeStep"].toInt();

							item->SetType(timepoint, source, type, timestep);
						}
					}

					connect(item.get(), &FileItem::GroupUpdated, this, &TaskRepo::OnGroupUpdated);
					connect(item.get(), &FileItem::TypeUpdated, this, &TaskRepo::OnTypeUpdated);
					connect(item.get(), &FileItem::Linked, this, &TaskRepo::OnLinked);
					connect(item.get(), &FileItem::Unlinked, this, &TaskRepo::OnUnlinked);
				}

				return true;
			}
		}

		return false;
	}

	auto TaskRepo::Create(const QString& path) -> bool {
		QMutexLocker locker(&d->mutex);

		if (QFile file(d->GetAbsolutePath(path)); file.open(QIODevice::WriteOnly | QIODevice::NewOnly)) {
			QJsonObject obj;
			obj["Pipeline"] = QString();
			obj["TaskList"] = QJsonArray();

			const QJsonDocument doc(obj);
			file.write(doc.toJson());

			if (file.flush())
				return true;
		}

		return false;
	}

	auto TaskRepo::Save() -> bool {
		QMutexLocker locker(&d->mutex);

		if (QFile file(d->path); file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
			QJsonObject obj;
			QJsonArray taskList;

			for (const auto& g : d->fileMap.keys()) {
				for (const auto& f : d->fileMap[g]) {
					if (const auto item = std::dynamic_pointer_cast<FileItem>(f)) {
						QJsonObject task;
						QJsonArray workList;

						for (const auto t : item->GetTimePointList()) {
							QJsonObject work;
							QJsonArray typeList;

							for (const auto& n : item->GetTypeList(t)) {
								QJsonObject type;

								type["Name"] = n;
								type["Type"] = ToString(item->GetType(t, n));
								type["TimeStep"] = item->GetTimeStep(t, n);

								typeList.push_back(type);
							}

							work["TimePoint"] = t;
							work["TypeList"] = typeList;
							workList.push_back(work);
						}

						task["FilePath"] = item->GetFilePath();
						task["Size"] = item->GetFileSize();
						task["Group"] = g;
						task["WorkList"] = workList;
						taskList.push_back(task);
					}
				}
			}

			if (d->pipeline)
				obj["Pipeline"] = d->pipeline->GetLocation();

			obj["TaskList"] = taskList;

			const QJsonDocument doc(obj);
			file.write(doc.toJson());

			if (file.flush())
				return true;
		}

		return false;
	}

	auto TaskRepo::Load() -> void {
		if (d->pipeline)
			d->pipeline->Load();
	}

	auto TaskRepo::GetLocation() const -> QString {
		const auto repo = d->provider->GetService<IProjectRepo>();
		return QDir(repo->GetLocation()).relativeFilePath(d->path);
	}

	auto TaskRepo::GetPipeline() const -> Pipeline::PipelinePtr {
		return d->pipeline;
	}

	auto TaskRepo::SetPipeline(const Pipeline::PipelinePtr& pipeline) -> void {
		QMutexLocker locker(&d->mutex);

		d->pipeline = pipeline;
		d->pipeline->Load();

		for (const auto& e : d->events)
			e->OnPipelineChanged(pipeline);
	}

	auto TaskRepo::ContainsGroup(const QString& group) const -> bool {
		return d->fileMap.contains(group);
	}

	auto TaskRepo::GetGroupList() const -> QStringList {
		return d->fileMap.keys();
	}

	auto TaskRepo::GetGroup(const FileItemPtr& file) const -> QString {
		for (const auto& g : d->fileMap.keys()) {
			if (d->fileMap[g].contains(file))
				return g;
		}

		return {};
	}

	auto TaskRepo::ContainsFile(const QString& filepath) const -> bool {
		for (const auto& g : d->fileMap.keys()) {
			for (const auto& f : d->fileMap[g]) {
				if (f->GetFilePath() == filepath)
					return true;
			}
		}

		return false;
	}

	auto TaskRepo::GetFile(const QString& filepath) const -> FileItemPtr {
		for (const auto& g : d->fileMap.keys()) {
			for (const auto& f : d->fileMap[g]) {
				if (f->GetFilePath() == filepath)
					return f;
			}
		}

		return {};
	}

	auto TaskRepo::GetFileList() const -> FileItemList {
		FileItemList list;

		for (const auto& f : d->fileMap.values())
			list.append(f);

		return list;
	}

	auto TaskRepo::GetFileList(const QString& group) const -> FileItemList {
		if (d->fileMap.contains(group))
			return d->fileMap[group];

		return {};
	}

	auto TaskRepo::GetLinkedFile() const -> FileItemPtr {
		for (const auto& g : d->fileMap.keys()) {
			for (const auto& f : d->fileMap[g]) {
				if (f->GetLinkedTimePoint() > -1)
					return f;
			}
		}

		return {};
	}

	auto TaskRepo::AddGroup(const QString& group) -> void {
		QMutexLocker locker(&d->mutex);

		if (!d->fileMap.contains(group)) {
			d->fileMap[group];

			for (const auto& e : d->events)
				e->OnGroupAdded(group);
		}
	}

	auto TaskRepo::RemoveGroup(const QString& group) -> void {
		QMutexLocker locker(&d->mutex);

		if (d->fileMap.contains(group)) {
			d->fileMap.remove(group);

			for (const auto& e : d->events)
				e->OnGroupRemoved(group);
		}
	}

	auto TaskRepo::AddFile(const QString& filepath) -> FileItemPtr {
		QMutexLocker locker(&d->mutex);

		if (!ContainsFile(filepath)) {
			auto item = std::make_shared<FileItem>(d->provider, filepath);
			d->fileMap[QString()].push_back(item);

			connect(item.get(), &FileItem::GroupUpdated, this, &TaskRepo::OnGroupUpdated);
			connect(item.get(), &FileItem::TypeUpdated, this, &TaskRepo::OnTypeUpdated);
			connect(item.get(), &FileItem::Linked, this, &TaskRepo::OnLinked);
			connect(item.get(), &FileItem::Unlinked, this, &TaskRepo::OnUnlinked);

			for (const auto& e : d->events)
				e->OnFileAdded(item);

			return item;
		}

		return {};
	}

	auto TaskRepo::AddFile(const QString& group, const QString& filepath) -> FileItemPtr {
		QMutexLocker locker(&d->mutex);

		if (!ContainsFile(filepath)) {
			AddGroup(group);

			auto item = std::make_shared<FileItem>(d->provider, filepath);
			d->fileMap[group].push_back(item);

			connect(item.get(), &FileItem::GroupUpdated, this, &TaskRepo::OnGroupUpdated);
			connect(item.get(), &FileItem::TypeUpdated, this, &TaskRepo::OnTypeUpdated);
			connect(item.get(), &FileItem::Linked, this, &TaskRepo::OnLinked);
			connect(item.get(), &FileItem::Unlinked, this, &TaskRepo::OnUnlinked);

			for (const auto& e : d->events)
				e->OnFileAdded(item);

			return item;
		}

		return {};
	}

	auto TaskRepo::RemoveFile(const FileItemPtr& file) -> void {
		QMutexLocker locker(&d->mutex);

		if (const auto group = file->GetGroup(); d->fileMap.contains(group)) {
			if (d->fileMap[group].removeOne(file)) {
				for (const auto& e : d->events)
					e->OnFileRemoved(file);
			}
		}
	}

	auto TaskRepo::Link(const FileItemPtr& file, int timepoint) -> bool {
		QMutexLocker locker(&d->mutex);

		if (ContainsFile(file->GetFilePath())) {
			Unlink();

			if (file->Link(timepoint)) {
				for (const auto& e : d->events)
					e->OnFileLinked(file, timepoint);

				return true;
			}
		}

		return false;
	}

	auto TaskRepo::Unlink() -> void {
		QMutexLocker locker(&d->mutex);

		for (const auto& g : d->fileMap.keys()) {
			for (const auto& f : d->fileMap[g]) {
				if (const auto tp = f->GetLinkedTimePoint(); tp > -1) {
					f->Unlink();

					for (const auto& e : d->events)
						e->OnFileUnlinked(f, tp);
				}
			}
		}
	}

	auto TaskRepo::OnGroupUpdated(const QString& group) -> void {
		QMutexLocker locker(&d->mutex);

		if (const auto* item = dynamic_cast<const FileItem*>(sender())) {
			if (const auto file = d->GetFileItemPtr(item)) {
				d->fileMap[GetGroup(file)].removeOne(file);
				d->fileMap[group].push_back(file);

				for (const auto& e : d->events)
					e->OnFileUpdated(file);
			}
		}
	}

	auto TaskRepo::OnTypeUpdated() -> void {
		if (const auto* item = dynamic_cast<const FileItem*>(sender())) {
			if (const auto file = d->GetFileItemPtr(item)) {
				for (const auto& e : d->events)
					e->OnFileUpdated(file);
			}
		}
	}

	auto TaskRepo::OnLinked(int timepoint) -> void {
		if (const auto* item = dynamic_cast<const FileItem*>(sender())) {
			if (const auto file = d->GetFileItemPtr(item)) {
				for (const auto& e : d->events)
					e->OnFileLinked(file, timepoint);
			}
		}
	}

	auto TaskRepo::OnUnlinked(int timepoint) -> void {
		if (const auto* item = dynamic_cast<const FileItem*>(sender())) {
			if (const auto file = d->GetFileItemPtr(item)) {
				for (const auto& e : d->events)
					e->OnFileUnlinked(file, timepoint);
			}
		}
	}
}
