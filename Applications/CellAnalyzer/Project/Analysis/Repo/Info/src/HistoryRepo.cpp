#include "HistoryRepo.h"

#include <QDir>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "IPipelineService.h"
#include "IProjectRepo.h"
#include "ITaskRepo.h"
#include "TaskHistory.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	struct HistoryRepo::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		QList<std::shared_ptr<IHistoryRepoEvent>> events;

		QString path;
		TaskHistoryList list;

		auto GetAbsolutePath(const QString& path) const -> QString;
	};

	auto HistoryRepo::Impl::GetAbsolutePath(const QString& path) const -> QString {
		if (const QFileInfo info(path); info.isRelative()) {
			const auto repo = provider->GetService<IProjectRepo>();
			const QDir dir(repo->GetLocation());
			return dir.filePath(path);
		}

		return path;
	}

	HistoryRepo::HistoryRepo(Tomocube::IServiceProvider* provider) : IHistoryRepo(), d(new Impl) {
		d->provider = provider;
	}

	HistoryRepo::~HistoryRepo() = default;

	auto HistoryRepo::AddEvent(const std::shared_ptr<IHistoryRepoEvent>& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto HistoryRepo::RemoveEvent(const std::shared_ptr<IHistoryRepoEvent>& event) -> void {
		d->events.removeOne(event);
	}

	auto HistoryRepo::Open(const QString& path) -> bool {
		d->list.clear();

		if (QFile file(d->GetAbsolutePath(path)); file.open(QIODevice::ReadOnly)) {
			QJsonParseError error;

			if (const auto doc = QJsonDocument::fromJson(file.readAll(), &error); error.error == QJsonParseError::NoError) {
				d->path = d->GetAbsolutePath(path);
				const auto obj = doc.object();
				const auto list = obj["History"].toArray();

				for (const auto& ar : list) {
					const auto map = ar.toObject().toVariantMap();
					const auto created = QDateTime::fromMSecsSinceEpoch(map["Created"].toLongLong());
					const auto finished = map["IsFinished"].toBool();
					const auto history = std::make_shared<TaskHistory>(d->provider, d->path, created, finished);

					for (const auto f : map["Progress"].toJsonArray()) {
						const auto finMap = f.toObject().toVariantMap();
						const auto filepath = finMap["FilePath"].toString();
						const auto timepoint = finMap["TimePoint"].toInt();

						for (const auto p : finMap["Process"].toStringList())
							history->AddHistory(filepath, timepoint, p);
					}

					d->list.push_back(history);
				}

				return true;
			}
		}

		return false;
	}

	auto HistoryRepo::Create(const QString& path) -> bool {
		if (QFile file(d->GetAbsolutePath(path)); file.open(QIODevice::WriteOnly | QIODevice::NewOnly)) {
			QJsonObject obj;
			obj["History"] = QJsonArray();

			const QJsonDocument doc(obj);
			file.write(doc.toJson());

			if (file.flush())
				return true;
		}

		return false;
	}

	auto HistoryRepo::Save() -> bool {
		if (QFile file(d->path); file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
			QJsonObject obj;
			QJsonArray historyList;

			for (const auto& i : d->list) {
				QJsonObject history;
				QJsonArray progList;

				for (const auto& f : i->GetFilePathList()) {
					for (const auto& t : i->GetTimePointList(f)) {
						QJsonObject progress;
						QJsonArray processList;

						for (const auto& p : i->GetProcessList(f, t))
							processList.push_back(p);

						progress["FilePath"] = f;
						progress["TimePoint"] = t;
						progress["Process"] = processList;
						progList.push_back(progress);
					}
				}

				history["Created"] = i->GetCreationDateTime().toMSecsSinceEpoch();
				history["IsFinished"] = i->IsFinished();
				history["Progress"] = progList;
				historyList.push_back(history);
			}

			obj["History"] = historyList;

			const QJsonDocument doc(obj);
			file.write(doc.toJson());

			if (file.flush())
				return true;
		}

		return false;
	}

	auto HistoryRepo::GetLocation() const -> QString {
		const auto repo = d->provider->GetService<IProjectRepo>();
		return QDir(repo->GetLocation()).relativeFilePath(d->path);
	}

	auto HistoryRepo::GetHistoryList() const -> TaskHistoryList {
		return d->list;
	}

	auto HistoryRepo::GetHistory(const QDateTime& datetime) const -> TaskHistoryPtr {
		for (const auto& t : d->list) {
			if (t->GetCreationDateTime() == datetime)
				return t;
		}

		return {};
	}

	auto HistoryRepo::CreateHistory() -> TaskHistoryPtr {
		const QDir dir(QString("%1/Export").arg(QFileInfo(d->path).absolutePath()));
		dir.mkpath(".");

		const auto history = std::make_shared<TaskHistory>(d->provider, dir.absolutePath(), QDateTime::currentDateTime(), false);
		const auto task = d->provider->GetService<ITaskRepo>();
		const auto service = d->provider->GetService<Pipeline::IPipelineService>();

		if (const auto pip = task->GetPipeline())
			service->Copy(pip, history->GetPipelineLocation());

		d->list.push_back(history);

		for (const auto& e : d->events)
			e->OnHistoryCreated();

		return history;
	}
}
