#pragma once

#include "IPresenterRepo.h"

#include "CellAnalyzer.Project.Analysis.Repo.PresenterExport.h"
#include "IServiceProvider.h"

namespace CellAnalyzer::Project::Analysis::Repo {
	class CellAnalyzer_Project_Analysis_Repo_Presenter_API PresenterRepo final : public IPresenterRepo {
	public:
		explicit PresenterRepo(Tomocube::IServiceProvider* provider);
		~PresenterRepo() override;

		auto Load() -> void;

		auto SetPipeline(const Pipeline::PipelinePtr& pipeline) -> void override;

		auto AddPresenter(const Presenter::PresenterPtr& presenter) -> void override;
		auto RemovePresenter(const Presenter::PresenterPtr& presenter) -> void override;

		auto CaptureDefaultPresenter(const QString& filepath) -> void override;
		auto ClearDefaultPresenter() -> void override;
		auto AddDataToDefaultPresenter(const QString& name, const DataPtr& data) -> void override;

		auto GetDefaultPresenter() const -> Presenter::PresenterPtr override;
		auto GetDefaultReport() const -> Presenter::PresenterPtr override;

		auto Contains(const Presenter::PresenterPtr& presenter) const -> bool override;
		auto GetPresenterList() const -> Presenter::PresenterList override;
		auto GetPresenter(const QString& ID) const -> Presenter::PresenterPtr override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
