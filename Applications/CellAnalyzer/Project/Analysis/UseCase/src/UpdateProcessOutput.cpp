#include <QMutexLocker>

#include "UpdateProcessOutput.h"

#include "IPropertyView.h"
#include "ITaskRepo.h"

namespace CellAnalyzer::Project::Analysis {
	struct UpdateProcessOutput::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		Session session = 0;
		QMutex mutex;
	};

	UpdateProcessOutput::UpdateProcessOutput(Tomocube::IServiceProvider* provider) : IProjectViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateProcessOutput::~UpdateProcessOutput() = default;

	auto UpdateProcessOutput::OnSelectionChanged(SelectionCategory category, const QString& name) -> void {
		if (category != SelectionCategory::ProcessOutput)
			return;

		const auto property = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<ITaskRepo>();

		Session temp;

		{
			QMutexLocker locker(&d->mutex);
			d->session = property->CreateSession();
			temp = d->session;
		}

		if (const auto pip = repo->GetPipeline()) {
			const auto output = pip->GetOutput(name);

			if (auto n = property->CreateProperty("General", "Name", Pipeline::AttrCategory::String)) {
				n->SetValue(output->GetName());
				n->SetState(Pipeline::AttrState::Disabled);
				property->AddProperty(temp, std::move(n));
			}

			if (auto savable = property->CreateProperty("General", "Auto Savable", Pipeline::AttrCategory::CheckBox)) {
				savable->SetValue(output->IsAutoSave());
				savable->SetState(Pipeline::AttrState::Disabled);
				property->AddProperty(temp, std::move(savable));
			}
		}
	}
}
