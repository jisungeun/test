#include <QMutexLocker>

#include "UpdateWork.h"

#include "IPropertyView.h"
#include "ITaskRepo.h"

namespace CellAnalyzer::Project::Analysis {
	struct UpdateWork::Impl {
		QMutex mutex;
		Tomocube::IServiceProvider* provider = nullptr;
		FileItemPtr file = nullptr;
		int timepoint = -1;
		Session session = 0;
	};

	UpdateWork::UpdateWork(Tomocube::IServiceProvider* provider) : IProjectViewEvent(), IPropertyViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateWork::~UpdateWork() = default;

	auto UpdateWork::OnSelectionChanged(SelectionCategory category, const QString& name) -> void {
		if (category != SelectionCategory::Work)
			return;

		const auto property = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<ITaskRepo>();
		const auto idx = name.lastIndexOf(":");

		if (idx < 0)
			return;

		FileItemPtr file = nullptr;
		int timepoint;
		Session temp;

		{
			QMutexLocker locker(&d->mutex);
			d->file = repo->GetFile(name.mid(0, idx));
			d->timepoint = name.mid(idx + 1, name.count() - 1).toInt();
			d->session = property->CreateSession();
			temp = d->session;
			timepoint = d->timepoint;
			file = d->file;
		}

		if (auto path = property->CreateProperty("General", "File Path", Pipeline::AttrCategory::File)) {
			path->SetValue(file->GetFilePath());
			path->SetState(Pipeline::AttrState::Disabled);
			property->AddProperty(temp, std::move(path));
		}

		if (auto tp = property->CreateProperty("General", "Time Point", Pipeline::AttrCategory::String)) {
			tp->SetValue(IFileItem::ToTimeString(timepoint));
			tp->SetState(Pipeline::AttrState::Disabled);
			property->AddProperty(temp, std::move(tp));
		}

		for (const auto& n : file->GetTypeList(timepoint)) {
			if (auto mod = property->CreateProperty("Modality", n, Pipeline::AttrCategory::String)) {
				mod->SetValue(ToString(file->GetType(timepoint, n)));
				mod->SetState(Pipeline::AttrState::Disabled);
				property->AddProperty(temp, std::move(mod));
			}
		}

		property->SetExecutable(temp, true);
		property->SetExecuteText(temp, "Use as Active Workset");
		property->SetDiscardable(temp, false);
	}

	auto UpdateWork::OnExecuted(Session session) -> void {
		if (d->session != session)
			return;

		FileItemPtr file = nullptr;
		int timepoint;

		{
			QMutexLocker locker(&d->mutex);
			file = d->file;
			timepoint = d->timepoint;
		}

		if (file && timepoint > -1)
			file->Link(timepoint);
	}

	auto UpdateWork::OnWorkLinked(const QString& filepath, int timepoint) -> void {
		FileItemPtr file = nullptr;

		{
			QMutexLocker locker(&d->mutex);
			file = d->file;
			timepoint = d->timepoint;
		}

		if (file && timepoint > -1)
			file->Link(timepoint);
	}
}
