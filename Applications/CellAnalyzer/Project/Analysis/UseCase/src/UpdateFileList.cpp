#include "UpdateFileList.h"

#include "IPropertyView.h"
#include "ITaskRepo.h"

namespace CellAnalyzer::Project::Analysis {
	struct UpdateFileList::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
	};

	UpdateFileList::UpdateFileList(Tomocube::IServiceProvider* provider) : IProjectViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateFileList::~UpdateFileList() = default;

	auto UpdateFileList::OnGroupAdded(const QString& group) -> void {
		const auto repo = d->provider->GetService<ITaskRepo>();
		repo->AddGroup(group);
	}

	auto UpdateFileList::OnGroupRenamed(const QString& oldName, const QString& newName) -> void {
		const auto repo = d->provider->GetService<ITaskRepo>();
		const auto list = repo->GetFileList(oldName);

		if (list.isEmpty())
			repo->AddGroup(newName);

		for (const auto& i : list)
			i->SetGroup(newName);

		repo->RemoveGroup(oldName);
	}

	auto UpdateFileList::OnGroupRemoved(const QString& group) -> void {
		const auto repo = d->provider->GetService<ITaskRepo>();
		repo->RemoveGroup(group);
	}

	auto UpdateFileList::OnFileAdded(const QString& filepath) -> void {
		const auto repo = d->provider->GetService<ITaskRepo>();

		repo->AddFile(filepath);
		repo->Save();
	}

	auto UpdateFileList::OnFileRemoved(const QString& filepath) -> void {
		const auto repo = d->provider->GetService<ITaskRepo>();

		if (const auto file = repo->GetFile(filepath)) {
			repo->RemoveFile(file);
			repo->Save();
		}
	}

	auto UpdateFileList::OnFileMoved(const QString& filepath, const QString& group) -> void {
		const auto repo = d->provider->GetService<ITaskRepo>();

		if (const auto file = repo->GetFile(filepath)) {
			file->SetGroup(group);
			repo->Save();
		}
	}
}
