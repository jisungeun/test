#include <QCoreApplication>
#include <QMutexLocker>

#include "UpdateProcess.h"

#include "IMeasure.h"
#include "IPipelineService.h"
#include "IPresenterRepo.h"
#include "IProjectRepo.h"
#include "IProjectView.h"
#include "IPropertyView.h"
#include "IStatusBarHandler.h"
#include "ITaskRepo.h"
#include "IVolume2D.h"
#include "IVolume3D.h"

namespace CellAnalyzer::Project::Analysis {
	struct UpdateProcess::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		Pipeline::ProcessPtr process = nullptr;
		StatusGroupPtr group = nullptr;
		Session session = 0;
		QString name;
		QMutex mutex;
		bool aborted = false;

		QMap<QString, Pipeline::AttrValue> attrMap;

		auto Rollback() -> void;
		auto AddDataToDefaultPresenter(const Pipeline::ProcessPtr& proc) -> void;
	};

	auto UpdateProcess::Impl::AddDataToDefaultPresenter(const Pipeline::ProcessPtr& proc) -> void {
		const auto taskRepo = provider->GetService<ITaskRepo>();
		const auto presenterRepo = provider->GetService<IPresenterRepo>();
		const auto outputList = proc->GetOutputList();

		const auto defaultRender = presenterRepo->GetDefaultPresenter();
		const auto defaultReport = presenterRepo->GetDefaultReport();

		for (const auto& o : outputList) {
			const auto processOutput = proc->GetOutput(o);
			if (!processOutput)
				continue;

			const auto processData = processOutput->GetData();
			if (!processData)
				continue;

			if (const auto pipeline = taskRepo->GetPipeline()) {
				const auto dataList = pipeline->GetDataList();

				for (const auto& dataID : dataList) {
					if (pipeline->GetData(dataID)->GetData() == processData) {
						if (std::dynamic_pointer_cast<IVolume2D>(processData) || std::dynamic_pointer_cast<IVolume3D>(processData)) {
							QMetaObject::invokeMethod(qApp, [this, dataID, processData, defaultRender] {
								defaultRender->AddData(dataID, processData, "List of Images");
							});
						} else if (std::dynamic_pointer_cast<IMeasure>(processData)) {
							QString portName = "HT";
							for (const auto& ikey : proc->GetInputList()) {
								if (const auto in = proc->GetInput(ikey); const auto idata = in->GetLinkedData()) {
									if (idata->GetFlags().testFlag(DataFlag::FL)) {
										portName = "FL";
										break;
									}
								}
							}
							QMetaObject::invokeMethod(qApp, [this, dataID, processData, defaultReport,portName] {
								defaultReport->AddData(dataID, processData, portName);
							});
						}
					}
				}
			}
		}
	}


	auto UpdateProcess::Impl::Rollback() -> void {
		if (process) {
			for (const auto& i : attrMap.keys()) {
				const auto attr = process->GetAttribute(i);
				attr->SetValue(attrMap[i]);
			}

			attrMap.clear();
		}
	}

	UpdateProcess::UpdateProcess(Tomocube::IServiceProvider* provider) : IProjectViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateProcess::~UpdateProcess() = default;

	auto UpdateProcess::OnSelectionChanged(SelectionCategory category, const QString& name) -> void {
		if (category != SelectionCategory::Process) {
			d->Rollback();
			return;
		}

		const auto propView = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<ITaskRepo>();

		Session temp;
		Pipeline::ProcessPtr process;
		QMap<QString, QString> inputMap;
		QMap<QString, Pipeline::AttrValue> attrMap;

		{
			QMutexLocker locker(&d->mutex);

			d->Rollback();
			d->session = propView->CreateSession();
			if (const auto pipeline = repo->GetPipeline())
				d->process = pipeline->GetProcess(name);
			else
				d->process = nullptr;

			temp = d->session;
			process = d->process;
		}

		if (process) {
			if (auto pname = propView->CreateProperty("General", "Name", Pipeline::AttrCategory::String)) {
				pname->SetValue(process->GetName());
				pname->SetState(Pipeline::AttrState::Disabled);
				propView->AddProperty(temp, std::move(pname));
			}

			if (auto id = propView->CreateProperty("General", "Process ID", Pipeline::AttrCategory::String)) {
				id->SetValue(process->GetID());
				id->SetState(Pipeline::AttrState::Disabled);
				propView->AddProperty(temp, std::move(id));
			}

			if (auto cat = propView->CreateProperty("General", "Process Category", Pipeline::AttrCategory::String)) {
				cat->SetValue(ToString(process->GetCategory()));
				cat->SetState(Pipeline::AttrState::Disabled);
				propView->AddProperty(temp, std::move(cat));
			}

			for (const auto& i : process->GetInputList()) {
				const auto input = process->GetInput(i);
				inputMap[i] = input->GetLinkedName();

				if (auto prop = propView->CreateProperty("Input Data", i, Pipeline::AttrCategory::String)) {
					prop->SetValue(input->GetLinkedName());
					prop->SetState(Pipeline::AttrState::Disabled);
					propView->AddProperty(temp, std::move(prop));
				}
			}

			for (const auto& i : process->GetAttributeList()) {
				if (const auto attr = process->GetAttribute(i); attr->GetModifier() != Pipeline::AttrModifier::Invisible) {
					attrMap[i] = attr->GetValue();

					if (auto prop = propView->CreateProperty(attr->GetGroup(), attr->GetID(), attr->GetCategory())) {
						prop->SetModel(attr->GetModel());
						prop->SetValue(attr->GetValue());
						prop->SetDescription(attr->GetDescription());
						prop->SetState(attr->GetState());

						if (attr->GetModifier() == Pipeline::AttrModifier::ReadOnly)
							prop->SetState(Pipeline::AttrState::Disabled);

						propView->AddProperty(temp, std::move(prop));
					}
				}
			}
		}

		propView->SetExecutable(temp, true);
		propView->SetAbortable(temp, true);
		propView->SetSavable(temp, true);

		{
			QMutexLocker locker(&d->mutex);

			if (d->session == temp)
				d->attrMap = attrMap;
		}
	}

	auto UpdateProcess::OnPropertyChanged(Session session, const QString& name, const Pipeline::AttrValue& value) -> void {
		if (!d->process)
			return;

		if (d->session != session)
			return;

		Session temp;
		Pipeline::ProcessPtr process;

		{
			QMutexLocker locker(&d->mutex);
			temp = d->session;
			process = d->process;
		}

		const auto repo = d->provider->GetService<IProjectRepo>();
		const auto property = d->provider->GetService<IPropertyView>();

		if (const auto prop = property->GetProperty(temp, name); prop->GetGroup() == "General" || prop->GetGroup() == "Input Data")
			return;

		if (const auto attr = process->GetAttribute(name))
			attr->SetValue(value);

		for (const auto& i : process->GetAttributeList()) {
			const auto attr = process->GetAttribute(i);

			if (const auto prop = property->GetProperty(temp, i)) {
				prop->SetState(attr->GetState());
				prop->SetModel(attr->GetModel());

				if (name != prop->GetName() && prop->GetValue() != attr->GetValue())
					prop->SetValue(attr->GetValue());
			}
		}
	}

	auto UpdateProcess::OnExecuted(Session session) -> void {
		if (d->session != session)
			return;

		Session temp;
		Pipeline::ProcessPtr process;

		{
			QMutexLocker locker(&d->mutex);
			temp = d->session;
			process = d->process;
		}

		const auto service = d->provider->GetService<Pipeline::IPipelineService>();
		const auto handler = d->provider->GetService<IStatusBarHandler>();
		const auto repo = d->provider->GetService<IProjectRepo>();
		const auto projView = d->provider->GetService<IProjectView>();
		const auto propView = d->provider->GetService<IPropertyView>();

		for (const auto& i : process->GetAttributeList()) {
			const auto attr = process->GetAttribute(i);

			if (const auto p = propView->GetProperty(temp, i))
				attr->SetValue(p->GetValue());
		}

		d->attrMap.clear();
		repo->Save();

		if (const auto pip = d->process->GetParent())
			service->Save(pip);

		QMap<Pipeline::ProcessPtr, StatusTaskPtr> procMap;
		d->group = handler->AddGroup(QString("Single Run - %1").arg(process->GetName()));
		const auto precedents = process->GetPrecedents(true);
		d->aborted = false;

		for (const auto& p : precedents)
			procMap[p] = d->group->AddTask(p->GetName());

		for (const auto& p : precedents) {
			if (d->aborted)
				break;

			procMap[p]->SetIntermediate();

			if (p->Execute()) {
				d->AddDataToDefaultPresenter(p);
			}

			projView->UpdatePipeline();
			procMap[p]->Finish();
		}

		if (d->group) {
			handler->RemoveGroup(d->group);
			d->group = nullptr;
		}
	}

	auto UpdateProcess::OnSaved(Session session) -> void {
		if (d->session != session)
			return;

		Session temp;
		Pipeline::ProcessPtr process;

		{
			QMutexLocker locker(&d->mutex);
			temp = d->session;
			process = d->process;
		}

		const auto service = d->provider->GetService<Pipeline::IPipelineService>();
		const auto handler = d->provider->GetService<IStatusBarHandler>();
		const auto repo = d->provider->GetService<IProjectRepo>();
		const auto projView = d->provider->GetService<IProjectView>();
		const auto propView = d->provider->GetService<IPropertyView>();

		for (const auto& i : process->GetAttributeList()) {
			const auto attr = process->GetAttribute(i);

			if (const auto p = propView->GetProperty(temp, i))
				attr->SetValue(p->GetValue());
		}

		for (const auto& o : process->GetOutputList()) {
			const auto output = process->GetOutput(o);
			output->SetData(nullptr);
		}

		projView->UpdatePipeline();
		d->attrMap.clear();
		repo->Save();

		if (const auto pip = d->process->GetParent())
			service->Save(pip);
	}

	auto UpdateProcess::OnAborted() -> void {
		if (d->group) {
			for (const auto& t : d->group->GetTaskList()) {
				if (t->IsRunning())
					t->SetError("Aborted");
			}

			d->aborted = true;
		}
	}
}
