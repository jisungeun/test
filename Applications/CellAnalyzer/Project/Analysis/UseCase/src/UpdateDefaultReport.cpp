#include <QCoreApplication>
#include <QMutexLocker>

#include <IPresenterRepo.h>
#include <ITaskRepo.h>
#include <IView.h>
#include <IMeasure.h>

#include "UpdateDefaultReport.h"

namespace CellAnalyzer::Project::Analysis {
	struct UpdateDefaultReport::Impl {
		QMutex mutex;

		Tomocube::IServiceProvider* provider { nullptr };
		Presenter::PresenterPtr presenter { nullptr };
		Pipeline::ProcessPtr process { nullptr };
	};

	UpdateDefaultReport::UpdateDefaultReport(Tomocube::IServiceProvider* provider) : IPropertyViewEvent(), IProjectViewEvent(), d { new Impl } {
		d->provider = provider;

		const auto repo = d->provider->GetService<IPresenterRepo>();
		d->presenter = repo->GetDefaultReport();
	}

	UpdateDefaultReport::~UpdateDefaultReport() = default;

	auto UpdateDefaultReport::OnSelectionChanged(SelectionCategory category, const QString& name) -> void {
		QMutexLocker locker(&d->mutex);

		if (category == SelectionCategory::Process) {
			const auto repo = d->provider->GetService<ITaskRepo>();

			if (const auto pipeline = repo->GetPipeline())
				d->process = pipeline->GetProcess(name);
		} else if (d->process)
			d->process = nullptr;
	}

	auto UpdateDefaultReport::OnFileLinked(const FileItemPtr& file, int timepoint) -> void {
		IView::Run([this] {
			d->presenter->ClearData();
		});
	}
}
