#include <QMutexLocker>

#include "UpdateProject.h"

#include "IProjectRepo.h"
#include "IPropertyView.h"

namespace CellAnalyzer::Project::Analysis {
	struct UpdateProject::Impl {
		QMutex mutex;
		Tomocube::IServiceProvider* provider = nullptr;
		Session session = 0;
	};

	UpdateProject::UpdateProject(Tomocube::IServiceProvider* provider) : IProjectViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateProject::~UpdateProject() = default;

	auto UpdateProject::OnSelectionChanged(SelectionCategory category, const QString& name) -> void {
		if (category != SelectionCategory::Project)
			return;

		const auto property = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<IProjectRepo>();

		Session temp;

		{
			QMutexLocker locker(&d->mutex);
			d->session = property->CreateSession();
			temp = d->session;
		}

		if (auto pname = property->CreateProperty("General", "Name", Pipeline::AttrCategory::String)) {
			pname->SetValue(repo->GetName());
			property->AddProperty(temp, std::move(pname));
		}

		if (auto user = property->CreateProperty("General", "User", Pipeline::AttrCategory::String)) {
			user->SetValue(repo->GetUser());
			property->AddProperty(temp, std::move(user));
		}

		if (auto desc = property->CreateProperty("General", "Description", Pipeline::AttrCategory::StringText)) {
			desc->SetValue(repo->GetDescription());
			property->AddProperty(temp, std::move(desc));
		}

		if (auto created = property->CreateProperty("General", "Created Time", Pipeline::AttrCategory::String)) {
			created->SetValue(repo->GetCreationDateTime().toString("yyyy/MM/dd HH:mm:ss"));
			created->SetState(Pipeline::AttrState::Disabled);
			property->AddProperty(temp, std::move(created));
		}
	}

	auto UpdateProject::OnExecuted(Session session) -> void {
		if (d->session != session)
			return;

		QMutexLocker locker(&d->mutex);
		const auto repo = d->provider->GetService<IProjectRepo>();
		const auto property = d->provider->GetService<IPropertyView>();

		if (const auto name = property->GetProperty(d->session, "Name"))
			repo->SetName(name->GetValue().toString());
		if (const auto user = property->GetProperty(d->session, "User"))
			repo->SetUser(user->GetValue().toString());
		if (const auto desc = property->GetProperty(d->session, "Description"))
			repo->SetDescription(desc->GetValue().toString());

		repo->Save();
	}
}
