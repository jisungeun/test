#include <QMutexLocker>

#include "UpdateWorkList.h"

#include "IPropertyView.h"
#include "ITaskRepo.h"

namespace CellAnalyzer::Project::Analysis {
	struct UpdateWorkList::Impl {
		QMutex mutex;
		Tomocube::IServiceProvider* provider = nullptr;
		Session session = 0;
	};

	UpdateWorkList::UpdateWorkList(Tomocube::IServiceProvider* provider) : IProjectViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateWorkList::~UpdateWorkList() = default;

	auto UpdateWorkList::OnWorkAdded(const QString& filepath, const QString& sourceName, int timepoint, TaskType type, int timestep) -> void {
		const auto repo = d->provider->GetService<ITaskRepo>();
		const auto file = repo->GetFile(filepath);

		if (file == nullptr)
			return;

		if (const auto pip = repo->GetPipeline()) {
			for (const auto& s : pip->GetSourceList()) {
				const auto source = pip->GetSource(s);

				switch (type) {
					case TaskType::HT2D:
						if (const auto flags = source->GetFlags(); flags.testFlag(DataFlag::Volume2D) && flags.testFlag(DataFlag::HT))
							file->SetType(timepoint, s, type, timestep);
						break;
					case TaskType::HT3D:
						if (const auto flags = source->GetFlags(); flags.testFlag(DataFlag::Volume3D) && flags.testFlag(DataFlag::HT))
							file->SetType(timepoint, s, type, timestep);
						break;
					case TaskType::FL2DCH0:
					case TaskType::FL2DCH1:
					case TaskType::FL2DCH2:
						if (const auto flags = source->GetFlags(); flags.testFlag(DataFlag::Volume2D) && flags.testFlag(DataFlag::FL) && source->GetName() == sourceName)
							file->SetType(timepoint, s, type, timestep);
						break;
					case TaskType::FL3DCH0:
					case TaskType::FL3DCH1:
					case TaskType::FL3DCH2:
						if (const auto flags = source->GetFlags(); flags.testFlag(DataFlag::Volume3D) && flags.testFlag(DataFlag::FL) && source->GetName() == sourceName) {
							file->SetType(timepoint, s, type, timestep);
						}
						break;
					case TaskType::BF:
						if (const auto flags = source->GetFlags(); flags.testFlag(DataFlag::BF))
							file->SetType(timepoint, s, type, timestep);
						break;
				}
			}

			repo->Save();
		}
	}

	auto UpdateWorkList::OnWorkRemoved(const QString& filepath, int timepoint) -> void {
		const auto repo = d->provider->GetService<ITaskRepo>();

		if (const auto file = repo->GetFile(filepath)) {
			file->RemoveTimePoint(timepoint);
			repo->Save();
		}
	}
}
