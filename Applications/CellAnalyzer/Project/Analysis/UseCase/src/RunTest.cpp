#include <QDir>

#include "RunTest.h"

#include "IAlertHandler.h"
#include "IPipelineService.h"
#include "IPresenterRepo.h"
#include "IProjectRepo.h"
#include "IProjectView.h"
#include "IStatusBarHandler.h"
#include "IStatusTask.h"
#include "ITaskRepo.h"

namespace CellAnalyzer::Project::Analysis {
	struct RunTest::Impl {
		Tomocube::IServiceProvider* provider = nullptr;

		bool aborted = false;

		auto GetWholeProcess(const Pipeline::PipelinePtr& pipeline) -> QList<Pipeline::ProcessPtr>;
		auto GetWholeSource(const Pipeline::PipelinePtr& pipeline) -> QList<Pipeline::PipelineSourcePtr>;
		auto CopyRecursively(const QString& srcPath, const QString& destPath) -> bool;
	};

	auto RunTest::Impl::GetWholeProcess(const Pipeline::PipelinePtr& pipeline) -> QList<Pipeline::ProcessPtr> {
		QList<Pipeline::ProcessPtr> procList;

		for (const auto& p : pipeline->GetProcessList()) {
			const auto proc = pipeline->GetProcess(p);

			for (const auto& o : proc->GetOutputList()) {
				if (const auto output = proc->GetOutput(o)) {
					procList.push_back(proc);
					break;
				}
			}
		}

		return procList;
	}

	auto RunTest::Impl::GetWholeSource(const Pipeline::PipelinePtr& pipeline) -> QList<Pipeline::PipelineSourcePtr> {
		QList<Pipeline::PipelineSourcePtr> list;

		for (const auto& s : pipeline->GetSourceList()) {
			if (const auto source = pipeline->GetSource(s))
				list.push_back(source);
		}
		return list;
	}

	auto RunTest::Impl::CopyRecursively(const QString& srcPath, const QString& destPath) -> bool {
		const QDir sourceDir(srcPath);
		const QDir destinationDir(destPath);

		if (!sourceDir.exists())
			return false;

		if (!destinationDir.exists()) {
			if (!destinationDir.mkpath("."))
				return false;
		}

		for (const auto& fileInfo : sourceDir.entryInfoList(QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs | QDir::Hidden)) {
			auto srcFilePath = fileInfo.filePath();
			auto destFilePath = destinationDir.filePath(fileInfo.fileName());

			if (fileInfo.isDir())
				return false;

			if (!QFile::copy(srcFilePath, destFilePath))
				return false;
		}

		return true;
	}

	RunTest::RunTest(Tomocube::IServiceProvider* provider) : IProjectViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	RunTest::~RunTest() = default;

	auto RunTest::OnTestRun() -> void {
		const auto taskRepo = d->provider->GetService<ITaskRepo>();
		const auto pipeline = taskRepo->GetPipeline();
		if (!pipeline)
			return;

		const auto linkedFile = taskRepo->GetLinkedFile();
		if (!linkedFile|| !linkedFile->IsValid())
			return;

		const auto projView = d->provider->GetService<IProjectView>();
		const auto statusHandler = d->provider->GetService<IStatusBarHandler>();
		const auto projRepo = d->provider->GetService<IProjectRepo>();
		const auto alert = d->provider->GetService<IAlertHandler>();
		const auto presRepo = d->provider->GetService<IPresenterRepo>();

		const auto procList = d->GetWholeProcess(pipeline);
		const auto srcList = d->GetWholeSource(pipeline);

		const auto statusGroup = statusHandler->AddGroup("Test Run");

		const auto timepoint = linkedFile->GetLinkedTimePoint();
		if (timepoint < 0)
			return;

		const auto messageTitle = QString("%1 of %2").arg(IFileItem::ToTimeString(timepoint)).arg(linkedFile->GetName());
		const auto task = statusGroup->AddTask(messageTitle);
		task->SetProgress(0, procList.count());

		d->aborted = false;
		for (const auto& proc : procList) {
			if (d->aborted)
				break;

			for (const auto& prc : proc->GetPrecedents(true)) {
				if (!prc->IsExecutable()) continue;

				task->SetMessage(QString("%1: %2").arg(messageTitle).arg(prc->GetName()));
				prc->Execute();
				projView->UpdatePipeline();
			}

			for (const auto& o : proc->GetOutputList()) {
				if (const auto output = proc->GetOutput(o); output->ExistsData())
					presRepo->AddDataToDefaultPresenter(output->GetName(), output->GetData());
				else
					linkedFile->SetError(timepoint, true);
			}

			task->SetMessage(messageTitle);
			task->AddProrgress();
		}

		task->Finish();

		statusHandler->RemoveGroup(statusGroup);
	}

	auto RunTest::OnAborted() -> void {
		d->aborted = true;
	}
}
