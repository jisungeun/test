#include <QMutexLocker>

#include "UpdateFile.h"

#include "IPropertyView.h"
#include "ITaskRepo.h"

namespace CellAnalyzer::Project::Analysis {
	struct UpdateFile::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		Session session = 0;
		QMutex mutex;
	};

	UpdateFile::UpdateFile(Tomocube::IServiceProvider* provider) : IProjectViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateFile::~UpdateFile() = default;

	auto UpdateFile::OnSelectionChanged(SelectionCategory category, const QString& name) -> void {
		if (category != SelectionCategory::File)
			return;

		const auto property = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<ITaskRepo>();

		Session temp;

		{
			QMutexLocker locker(&d->mutex);
			d->session = property->CreateSession();
			temp = d->session;
		}

		if (auto path = property->CreateProperty("General", "File Path", Pipeline::AttrCategory::File)) {
			path->SetValue(name);
			path->SetState(Pipeline::AttrState::Disabled);
			property->AddProperty(temp, std::move(path));
		}

		if (auto state = property->CreateProperty("General", "Status", Pipeline::AttrCategory::String)) {
			const auto file = repo->GetFile(name);
			state->SetValue(file->IsValid() ? "Valid" : "Invalid");
			state->SetState(Pipeline::AttrState::Disabled);
			property->AddProperty(temp, std::move(state));
		}
	}
}
