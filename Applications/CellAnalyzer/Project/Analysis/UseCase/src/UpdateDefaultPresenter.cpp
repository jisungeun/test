#include <QCoreApplication>
#include <QMutexLocker>

#include "UpdateDefaultPresenter.h"

#include "IPresenterRepo.h"
#include "IProcess.h"
#include "IPropertyView.h"
#include "ITaskRepo.h"
#include "IView.h"
#include "IVolume2D.h"
#include "IVolume3D.h"

namespace CellAnalyzer::Project::Analysis {
	struct UpdateDefaultPresenter::Impl {
		QMutex mutex;

		Tomocube::IServiceProvider* provider { nullptr };
		Presenter::PresenterPtr presenter { nullptr };
		Pipeline::PipelineSourcePtr source { nullptr };
		Pipeline::ProcessPtr process { nullptr };
		QString procName;
		auto ReorderHTFirst(const QStringList& original) -> QStringList;
	};

	auto UpdateDefaultPresenter::Impl::ReorderHTFirst(const QStringList& original) -> QStringList {
		const auto taskRepo = provider->GetService<ITaskRepo>();
		const auto pipeline = taskRepo->GetPipeline();

		if (!pipeline)
			return {};

		QString htName;
		QStringList originalSourceList = original;
		QStringList result;
		for (const auto& sourceID : originalSourceList) {
			if (const auto data = pipeline->GetSource(sourceID)) {
				if (data->GetFlags().testFlag(DataFlag::HT)) {
					result.append(sourceID);
					originalSourceList.removeOne(sourceID);
				}
			}
		}

		for (const auto& remains : originalSourceList) {
			result.append(remains);
		}

		return result;
	}


	UpdateDefaultPresenter::UpdateDefaultPresenter(Tomocube::IServiceProvider* provider) : IPropertyViewEvent(), ITaskRepoEvent(), IProjectViewEvent(), d { new Impl } {
		d->provider = provider;

		const auto presenterRepo = d->provider->GetService<IPresenterRepo>();

		d->presenter = presenterRepo->GetDefaultPresenter();
	}

	UpdateDefaultPresenter::~UpdateDefaultPresenter() = default;

	auto UpdateDefaultPresenter::OnSelectionChanged(SelectionCategory category, const QString& name) -> void {
		QMutexLocker locker(&d->mutex);

		if (category == SelectionCategory::Process) {
			const auto taskRepo = d->provider->GetService<ITaskRepo>();
			const auto pipeline = taskRepo->GetPipeline();

			d->process = pipeline->GetProcess(name);
			d->procName = name;
		} else if (d->process) {
			d->process = nullptr;
			d->procName = QString();
		}
	}

	auto UpdateDefaultPresenter::OnFileLinked(const FileItemPtr& file, int timepoint) -> void {
		const auto taskRepo = d->provider->GetService<ITaskRepo>();
		const auto pipeline = taskRepo->GetPipeline();
		IView::Run([this] {
			d->presenter->ClearData();
		});
		const auto sourceList = pipeline->GetSourceList();
		const auto reorderList = d->ReorderHTFirst(sourceList);
		for (const auto& sourceID : reorderList) {
			if (const auto data = pipeline->GetSource(sourceID); data->GetData()) {
				IView::Run([this, sourceID, data] {
					d->presenter->AddData(sourceID, data->GetData(), "List of Images");
				}, Qt::QueuedConnection);
			}
		}
	}
}
