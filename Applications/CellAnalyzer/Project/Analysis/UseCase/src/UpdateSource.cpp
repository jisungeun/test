#include <QMutexLocker>

#include "UpdateSource.h"

#include "IPropertyView.h"
#include "ITaskRepo.h"

namespace CellAnalyzer::Project::Analysis {
	struct UpdateSource::Impl {
		QMutex mutex;
		Tomocube::IServiceProvider* provider = nullptr;
		Session session = 0;
	};

	UpdateSource::UpdateSource(Tomocube::IServiceProvider* provider) : IProjectViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateSource::~UpdateSource() = default;

	auto UpdateSource::OnSelectionChanged(SelectionCategory category, const QString& name) -> void {
		if (category != SelectionCategory::Source && category != SelectionCategory::SourceList && category != SelectionCategory::None)
			return;

		const auto propView = d->provider->GetService<IPropertyView>();

		Session temp;

		{
			QMutexLocker locker(&d->mutex);
			d->session = propView->CreateSession();
			temp = d->session;
		}

		if (category == SelectionCategory::Source) {
			const auto repo = d->provider->GetService<ITaskRepo>();
			
			if (const auto pip = repo->GetPipeline()) {
				const auto source = pip->GetSource(name);

				if (auto n = propView->CreateProperty("General", "Name", Pipeline::AttrCategory::String)) {
					n->SetValue(source->GetName());
					n->SetState(Pipeline::AttrState::Disabled);
					propView->AddProperty(temp, std::move(n));
				}

				if (auto savable = propView->CreateProperty("General", "Export", Pipeline::AttrCategory::CheckBox)) {
					savable->SetValue(source->IsAutoSave());
					savable->SetState(Pipeline::AttrState::Disabled);
					propView->AddProperty(temp, std::move(savable));
				}
			}
		}
	}
}
