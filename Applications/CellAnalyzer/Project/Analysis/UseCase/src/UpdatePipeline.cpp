#include <QDir>
#include <QMutexLocker>
#include <QQueue>

#include "UpdatePipeline.h"

#include "IAlertHandler.h"
#include "IExporter.h"
#include "IExporterRepo.h"
#include "IHistoryRepo.h"
#include "IPipelineService.h"
#include "IPresenterRepo.h"
#include "IProjectRepo.h"
#include "IProjectView.h"
#include "IPropertyView.h"
#include "IStatusBarHandler.h"
#include "IStatusTask.h"
#include "ITaskHistory.h"
#include "ITaskRepo.h"

#include "CsvExporter.h"

namespace CellAnalyzer::Project::Analysis {
	struct UpdatePipeline::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		QMutex mutex;
		Session session = 0;
		StatusGroupPtr group = nullptr;
		bool aborted = false;
		bool running = false;

		static auto GetPrecedentProcessList(const Pipeline::ProcessPtr& process) -> Pipeline::ProcessList;
		static auto GetExporter(const std::shared_ptr<IExporterRepo>& repo, const DataPtr& data) -> IO::ExporterPtr;
		static auto GetExecutableProcess(const Pipeline::PipelinePtr& pipeline) -> QList<Pipeline::ProcessPtr>;
		static auto GetWholeProcess(const Pipeline::PipelinePtr& pipeline) -> QList<Pipeline::ProcessPtr>;
		static auto GetSavableSource(const Pipeline::PipelinePtr& pipeline) -> QList<Pipeline::PipelineSourcePtr>;
		static auto GetWholeSource(const Pipeline::PipelinePtr& pipeline) -> QList<Pipeline::PipelineSourcePtr>;

		static auto GetTaskHistory(const std::shared_ptr<IHistoryRepo>& repo, const std::shared_ptr<IAlertHandler>& handler) -> TaskHistoryPtr;
		static auto GetBasePath(const TaskHistoryPtr& history, const Pipeline::ProcessPtr& process, bool mkdir = false) -> QString;
		static auto GetBasePath(const TaskHistoryPtr& history, const Pipeline::PipelineSourcePtr& source, bool mkdir = false) -> QString;
		static auto GetThumbnailFilename(const TaskHistoryPtr& history, const FileItemPtr& file, int timepoint, const Pipeline::PipelineDataPtr& data) -> QString;
		static auto GetDataName(const QString& base, const FileItemPtr& file, int timepoint, const Pipeline::PipelineDataPtr& data, const IO::ExporterPtr& exporter) -> QString;
		static auto ExistsOutput(const QString& basePath, const FileItemPtr& file, int timepoint) -> bool;

		static auto CopyRecursively(const QString& srcPath, const QString& destPath) -> bool;
		static auto MergeMeasureData(const std::shared_ptr<IExporterRepo>& repo, const QStringList& dirPaths, const QStringList& measureNames, const QString& projName) -> void;
	};

	auto UpdatePipeline::Impl::MergeMeasureData(const std::shared_ptr<IExporterRepo>& repo, const QStringList& dirPaths, const QStringList& measureNames, const QString& projName) -> void {
		const auto exporter = repo->CreateExporter(DataFlag::Measure);
		if (!exporter) {
			return;
		}

		const auto merger = std::dynamic_pointer_cast<IO::Export::CsvExporter>(exporter);
		if (!merger) {
			return;
		}

		for (auto i = 0; i < dirPaths.count(); i++) {
			const auto measureDirPath = dirPaths[i];
			const auto measureDataName = measureNames[i];

			//Merge CSV
			merger->Merge(measureDirPath, QDir(measureDirPath).filePath(QString("%1_%2.csv").arg(measureDataName).arg(projName)));

			//Merge Summary
			const auto summaryDirPath = QString("%1/Summary").arg(measureDirPath);
			merger->Merge(summaryDirPath, QDir(summaryDirPath).filePath(QString("%1_%2_summary.csv").arg(measureDataName).arg(projName)));
		}
	}

	auto UpdatePipeline::Impl::CopyRecursively(const QString& srcPath, const QString& destPath) -> bool {
		const QDir sourceDir(srcPath);
		const QDir destinationDir(destPath);

		if (!sourceDir.exists())
			return false;

		if (!destinationDir.exists()) {
			if (!destinationDir.mkpath("."))
				return false;
		}

		for (const auto& fileInfo : sourceDir.entryInfoList(QDir::NoDotAndDotDot | QDir::Files | QDir::Dirs | QDir::Hidden)) {
			auto srcFilePath = fileInfo.filePath();
			auto destFilePath = destinationDir.filePath(fileInfo.fileName());

			if (fileInfo.isDir())
				return false;

			if (!QFile::copy(srcFilePath, destFilePath))
				return false;
		}

		return true;
	}

	auto UpdatePipeline::Impl::GetPrecedentProcessList(const Pipeline::ProcessPtr& process) -> Pipeline::ProcessList {
		Pipeline::ProcessList list;
		QQueue<Pipeline::ProcessPtr> queue;
		queue.enqueue(process);

		while (!queue.isEmpty()) {
			const auto proc = queue.dequeue();

			for (const auto& i : proc->GetInputList()) {
				const auto input = proc->GetInput(i);

				if (!input->IsLinked())
					return {};

				if (const auto output = input->GetLinkedAsOutput()) {
					const auto parent = output->GetParent();
					list.push_front(parent);
					queue.enqueue(parent);
				}
			}
		}

		list.push_back(process);

		return list;
	}

	auto UpdatePipeline::Impl::GetExporter(const std::shared_ptr<IExporterRepo>& repo, const DataPtr& data) -> IO::ExporterPtr {
		const auto flags = data->GetFlags();

		if (data == nullptr)
			return {};

		if (flags.testFlag(DataFlag::Volume2D))
			return repo->CreateExporter(DataFlag::Volume2D);
		if (flags.testFlag(DataFlag::Volume3D))
			return repo->CreateExporter(DataFlag::Volume3D);
		if (flags.testFlag(DataFlag::Measure))
			return repo->CreateExporter(DataFlag::Measure);

		return {};
	}

	auto UpdatePipeline::Impl::GetWholeProcess(const Pipeline::PipelinePtr& pipeline) -> QList<Pipeline::ProcessPtr> {
		QList<Pipeline::ProcessPtr> procList;

		for (const auto& p : pipeline->GetProcessList()) {
			const auto proc = pipeline->GetProcess(p);

			for (const auto& o : proc->GetOutputList()) {
				if (const auto output = proc->GetOutput(o)) {
					procList.push_back(proc);
					break;
				}
			}
		}

		return procList;
	}


	auto UpdatePipeline::Impl::GetExecutableProcess(const Pipeline::PipelinePtr& pipeline) -> QList<Pipeline::ProcessPtr> {
		QList<Pipeline::ProcessPtr> procList;

		for (const auto& p : pipeline->GetProcessList()) {
			const auto proc = pipeline->GetProcess(p);

			for (const auto& o : proc->GetOutputList()) {
				if (const auto output = proc->GetOutput(o); output->IsAutoSave()) {
					procList.push_back(proc);
					break;
				}
			}
		}

		return procList;
	}

	auto UpdatePipeline::Impl::GetWholeSource(const Pipeline::PipelinePtr& pipeline) -> QList<Pipeline::PipelineSourcePtr> {
		QList<Pipeline::PipelineSourcePtr> list;

		for (const auto& s : pipeline->GetSourceList()) {
			if (const auto source = pipeline->GetSource(s))
				list.push_back(source);
		}
		return list;
	}

	auto UpdatePipeline::Impl::GetSavableSource(const Pipeline::PipelinePtr& pipeline) -> QList<Pipeline::PipelineSourcePtr> {
		QList<Pipeline::PipelineSourcePtr> list;

		for (const auto& s : pipeline->GetSourceList()) {
			if (const auto source = pipeline->GetSource(s); source->IsAutoSave())
				list.push_back(source);
		}
		return list;
	}

	auto UpdatePipeline::Impl::GetTaskHistory(const std::shared_ptr<IHistoryRepo>& repo, const std::shared_ptr<IAlertHandler>& handler) -> TaskHistoryPtr {
		if (const auto list = repo->GetHistoryList(); !list.isEmpty()) {
			if (auto last = list.last(); !last->IsFinished()) {
				if (handler->ShowYesNo("Batch run had been aborted", "Do you want to continue last batch run?"))
					return last;
			}
		}

		return repo->CreateHistory();
	}

	auto UpdatePipeline::Impl::GetBasePath(const TaskHistoryPtr& history, const Pipeline::ProcessPtr& process, bool mkdir) -> QString {
		const QDir dir = QString("%1/%2").arg(history->GetLocation()).arg(process->GetName());

		if (mkdir) {
			dir.mkpath(".");
		}

		return dir.absolutePath();
	}

	auto UpdatePipeline::Impl::GetBasePath(const TaskHistoryPtr& history, const Pipeline::PipelineSourcePtr& source, bool mkdir) -> QString {
		const QDir dir = QString("%1/%2").arg(history->GetLocation()).arg(source->GetName());

		if (mkdir) {
			dir.mkpath(".");
		}

		return dir.absolutePath();
	}

	auto UpdatePipeline::Impl::GetThumbnailFilename(const TaskHistoryPtr& history, const FileItemPtr& file, int timepoint, const Pipeline::PipelineDataPtr& data) -> QString {
		const QDir dir = QString("%1/Thumbnails").arg(history->GetLocation());
		dir.mkpath(".");

		return dir.filePath(QString("%1_%2_%3.png").arg(data->GetName()).arg(file->GetName()).arg(timepoint));
	}

	auto UpdatePipeline::Impl::GetDataName(const QString& base, const FileItemPtr& file, int timepoint, const Pipeline::PipelineDataPtr& data, const IO::ExporterPtr& exporter) -> QString {
		const QDir dir(base);

		return dir.filePath(QString("%1_%2_%3.%4").arg(data->GetName()).arg(file->GetName()).arg(timepoint).arg(exporter->GetFormat()));
	}

	auto UpdatePipeline::Impl::ExistsOutput(const QString& basePath, const FileItemPtr& file, int timepoint) -> bool {
		const QDir dir(basePath);

		for (const auto& e : dir.entryList(QDir::Files | QDir::NoDotAndDotDot)) {
			if (e.contains(QString("%1_%2").arg(file->GetName()).arg(timepoint)))
				return true;
		}

		return false;
	}

	UpdatePipeline::UpdatePipeline(Tomocube::IServiceProvider* provider) : IProjectViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdatePipeline::~UpdatePipeline() = default;

	auto UpdatePipeline::OnSelectionChanged(SelectionCategory category, const QString& name) -> void {
		if (category != SelectionCategory::Pipeline)
			return;

		const auto propView = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<ITaskRepo>();

		Session temp;

		{
			QMutexLocker locker(&d->mutex);
			d->session = propView->CreateSession();
			temp = d->session;
		}

		if (const auto pipeline = repo->GetPipeline()) {
			if (auto pname = propView->CreateProperty("General", "Name", Pipeline::AttrCategory::String)) {
				pname->SetValue(pipeline->GetName());
				pname->SetState(Pipeline::AttrState::Disabled);
				propView->AddProperty(temp, std::move(pname));
			}

			if (auto author = propView->CreateProperty("General", "Author", Pipeline::AttrCategory::String)) {
				author->SetValue(pipeline->GetAuthor());
				author->SetState(Pipeline::AttrState::Disabled);
				propView->AddProperty(temp, std::move(author));
			}

			if (auto desc = propView->CreateProperty("General", "Description", Pipeline::AttrCategory::StringText)) {
				desc->SetValue(pipeline->GetDescription());
				desc->SetState(Pipeline::AttrState::Disabled);
				propView->AddProperty(temp, std::move(desc));
			}

			if (auto created = propView->CreateProperty("General", "Created Time", Pipeline::AttrCategory::String)) {
				created->SetValue(pipeline->GetCreationDateTime().toString("yyyy/MM/dd HH:mm:ss"));
				created->SetState(Pipeline::AttrState::Disabled);
				propView->AddProperty(temp, std::move(created));
			}

			propView->SetDiscardable(temp, false);
			propView->SetAbortable(temp, true);
			propView->SetExecutable(temp, true);
			propView->SetExecuteText(temp, "Batch Run");
		}
	}

	auto UpdatePipeline::OnPipelineChanged(const QString& filepath) -> void {
		const auto service = d->provider->GetService<Pipeline::IPipelineService>();
		const auto repo = d->provider->GetService<ITaskRepo>();
		const auto pRepo = d->provider->GetService<IPresenterRepo>();

		if (const auto pip = service->Read(filepath)) {
			repo->SetPipeline(pip);
			repo->Save();

			pRepo->SetPipeline(pip);
		}
	}

	auto UpdatePipeline::OnExecuted(Session session) -> void {
		if (d->session != session)
			return;

		const auto alert = d->provider->GetService<IAlertHandler>();
		const auto status = d->provider->GetService<IStatusBarHandler>();
		const auto hstRepo = d->provider->GetService<IHistoryRepo>();
		const auto projRepo = d->provider->GetService<IProjectRepo>();
		const auto expRepo = d->provider->GetService<IExporterRepo>();
		const auto taskRepo = d->provider->GetService<ITaskRepo>();
		const auto presRepo = d->provider->GetService<IPresenterRepo>();
		const auto projView = d->provider->GetService<IProjectView>();
		const auto pip = taskRepo->GetPipeline();
		const auto filelist = taskRepo->GetFileList();

		if (!pip)
			return;

		if (expRepo->GetExporter(DataFlag::Volume2D).isEmpty() || expRepo->GetExporter(DataFlag::Volume3D).isEmpty()) {
			projView->UpdateExporter();

			if (expRepo->GetExporter(DataFlag::Volume2D).isEmpty() || expRepo->GetExporter(DataFlag::Volume3D).isEmpty())
				return;
		}

		const auto history = d->GetTaskHistory(hstRepo, alert);
		const auto procList = d->GetWholeProcess(pip);
		const auto srcList = d->GetWholeSource(pip);

		QStringList measureDirPaths;
		QStringList measureDataNames;

		//Copy current ROI to history
		const auto destPath = QDir(history->GetLocation()).filePath("ROI");
		const auto srcPath = QDir(projRepo->GetLocation()).filePath("ROI");
		d->CopyRecursively(srcPath, destPath);
		d->aborted = false;

		QMap<FileItemPtr, QMap<int, StatusTaskPtr>> taskMap;
		d->group = status->AddGroup("Batch Run");

		for (const auto& f : filelist) {
			for (const auto t : f->GetTimePointList())
				taskMap[f][t] = d->group->AddTask(QString("%1 of %2").arg(IFileItem::ToTimeString(t)).arg(f->GetName()));
		}

		for (const auto& f : filelist) {
			if (d->aborted)
				break;

			for (const auto t : f->GetTimePointList()) {
				if (d->aborted)
					break;

				presRepo->ClearDefaultPresenter();

				if (!f->Link(t))
					continue;

				taskMap[f][t]->SetProgress(0, srcList.count() + procList.count());
				const auto title = QString("%1 of %2").arg(IFileItem::ToTimeString(t)).arg(f->GetName());

				for (const auto& s : srcList) {
					if (d->aborted)
						break;

					if (s->IsAutoSave()) {
						if (const auto base = d->GetBasePath(history, s, true); !d->ExistsOutput(base, f, t) && s->ExistsData()) {
							if (const auto exporter = d->GetExporter(expRepo, s->GetData())) {
								taskMap[f][t]->SetMessage(QString("%1: %2").arg(title).arg(s->GetName()));
								exporter->SetMetadata("FilePath", f->GetFilePath());
								exporter->SetMetadata("Group", f->GetGroup());
								exporter->Export(s->GetData(), d->GetDataName(base, f, t, s, exporter));
							}

							if (const auto flags = s->GetData()->GetFlags(); flags.testFlag(DataFlag::Volume2D) || flags.testFlag(DataFlag::Volume3D))
								presRepo->CaptureDefaultPresenter(d->GetThumbnailFilename(history, f, t, s));
						}
					}

					taskMap[f][t]->AddProrgress();
				}

				for (const auto& proc : procList) {
					if (d->aborted)
						break;

					if (const auto base = d->GetBasePath(history, proc); !d->ExistsOutput(base, f, t)) {
						for (const auto& prc : proc->GetPrecedents(true)) {
							if (prc->IsExecutable()) {
								taskMap[f][t]->SetMessage(QString("%1: %2").arg(title).arg(prc->GetName()));
								prc->Execute();
								projView->UpdatePipeline();
							}
						}

						auto error = false;

						for (const auto& o : proc->GetOutputList()) {
							if (const auto output = proc->GetOutput(o); output->ExistsData()) {
								presRepo->AddDataToDefaultPresenter(output->GetName(), output->GetData());
								if (output->IsAutoSave()) {
									const auto mkbase = d->GetBasePath(history, proc, true);
									if (const auto exporter = d->GetExporter(expRepo, output->GetData())) {
										exporter->SetMetadata("FilePath", f->GetFilePath());
										exporter->SetMetadata("Group", f->GetGroup());
										exporter->Export(output->GetData(), d->GetDataName(mkbase, f, t, output, exporter));
									}
									if (const auto flags = output->GetData()->GetFlags(); flags.testFlag(DataFlag::Volume2D) || flags.testFlag(DataFlag::Volume3D))
										presRepo->CaptureDefaultPresenter(d->GetThumbnailFilename(history, f, t, output));
									if (const auto flags = output->GetData()->GetFlags(); flags.testFlag(DataFlag::Measure) && false == measureDirPaths.contains(mkbase)) {
										measureDirPaths.append(mkbase);
										measureDataNames.append(output->GetName());
									}
								}
							} else {
								error = true;
							}
						}

						f->SetError(t, error);
					}

					taskMap[f][t]->SetMessage(title);
					taskMap[f][t]->AddProrgress();
				}

				taskMap[f][t]->Finish();
				hstRepo->Save();
			}
		}
		//Merge csv
		if (false == measureDirPaths.isEmpty()) {
			d->MergeMeasureData(expRepo, measureDirPaths, measureDataNames, projRepo->GetName());
		}

		if (d->group) {
			status->RemoveGroup(d->group);
			d->group = nullptr;
		}

		if (!d->aborted)
			history->Finish();

		for (const auto& i : taskMap.keys()) {
			for (const auto& j : taskMap[i].keys())
				taskMap[i][j]->Finish();
		}

		hstRepo->Save();
		projRepo->Save();
		taskRepo->Save();
	}

	auto UpdatePipeline::OnAborted() -> void {
		if (d->group) {
			for (const auto& t : d->group->GetTaskList()) {
				if (t->IsRunning())
					t->SetError("Aborted");
			}

			d->aborted = true;
		}
	}
}
