#include <QMutexLocker>

#include "UpdateFileGroup.h"

#include "IProjectRepo.h"
#include "IPropertyView.h"

namespace CellAnalyzer::Project::Analysis {
	struct UpdateFileGroup::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		Session session = 0;
		QMutex mutex;
	};

	UpdateFileGroup::UpdateFileGroup(Tomocube::IServiceProvider* provider) : IProjectViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateFileGroup::~UpdateFileGroup() = default;

	auto UpdateFileGroup::OnSelectionChanged(SelectionCategory category, const QString& name) -> void {
		if (category != SelectionCategory::FileGroup)
			return;

		const auto property = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<IProjectRepo>();

		Session temp;

		{
			QMutexLocker locker(&d->mutex);
			d->session = property->CreateSession();
			temp = d->session;
		}
	}
}
