#pragma once

#include <memory>

#include "IServiceProvider.h"

#include "IProjectViewEvent.h"
#include "IPropertyViewEvent.h"

#include "CellAnalyzer.Project.Analysis.UseCaseExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_UseCase_API RunTest final : public IProjectViewEvent, public IPropertyViewEvent {
	public:
		explicit RunTest(Tomocube::IServiceProvider* provider);
		~RunTest() override;

		auto OnTestRun() -> void override;
		auto OnAborted() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
