#pragma once

#include <memory>

#include "IServiceProvider.h"

#include "IProjectViewEvent.h"

#include "CellAnalyzer.Project.Analysis.UseCaseExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_UseCase_API UpdateFileGroup final : public IProjectViewEvent {
	public:
		explicit UpdateFileGroup(Tomocube::IServiceProvider* provider);
		~UpdateFileGroup() override;

		auto OnSelectionChanged(SelectionCategory category, const QString& name) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
