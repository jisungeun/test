#pragma once

#include "IServiceProvider.h"

#include "IScreenEvent.h"
#include "IProjectViewEvent.h"

#include "CellAnalyzer.Project.Analysis.UseCaseExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_UseCase_API UpdatePresenter final : public IScreenEvent, public IProjectViewEvent {
	public:
		UpdatePresenter(Tomocube::IServiceProvider* provider);
		~UpdatePresenter() override;

		auto OnFocusChanged(const ViewPtr& current, const ViewPtr& previous) -> void override;
		auto OnPresenterAdded(const QString& presenterID, const QMap<QString, QStringList>& dataMap, const QString& windowTitle) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
