#pragma once

#include <memory>

#include "IServiceProvider.h"

#include "IProjectViewEvent.h"
#include "IPropertyViewEvent.h"

#include "CellAnalyzer.Project.Analysis.UseCaseExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_UseCase_API UpdateProject final : public IProjectViewEvent, public IPropertyViewEvent {
	public:
		explicit UpdateProject(Tomocube::IServiceProvider* provider);
		~UpdateProject() override;

		auto OnSelectionChanged(SelectionCategory category, const QString& name) -> void override;
		auto OnExecuted(Session session) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
