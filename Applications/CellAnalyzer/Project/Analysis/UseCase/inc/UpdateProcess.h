#pragma once

#include <memory>

#include "IServiceProvider.h"

#include "IProjectViewEvent.h"
#include "IPropertyViewEvent.h"

#include "CellAnalyzer.Project.Analysis.UseCaseExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_UseCase_API UpdateProcess final : public IProjectViewEvent, public IPropertyViewEvent {
	public:
		explicit UpdateProcess(Tomocube::IServiceProvider* provider);
		~UpdateProcess() override;

		auto OnSelectionChanged(SelectionCategory category, const QString& name) -> void override;
		auto OnPropertyChanged(Session session, const QString& name, const Pipeline::AttrValue& value) -> void override;
		auto OnExecuted(Session session) -> void override;
		auto OnSaved(Session session) -> void override;
		auto OnAborted() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
