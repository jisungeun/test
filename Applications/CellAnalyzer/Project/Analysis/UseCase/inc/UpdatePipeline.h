#pragma once

#include <memory>

#include "IServiceProvider.h"

#include "IProjectViewEvent.h"
#include "IPropertyViewEvent.h"

#include "CellAnalyzer.Project.Analysis.UseCaseExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_UseCase_API UpdatePipeline final : public IProjectViewEvent, public IPropertyViewEvent {
	public:
		explicit UpdatePipeline(Tomocube::IServiceProvider* provider);
		~UpdatePipeline() override;

		auto OnSelectionChanged(SelectionCategory category, const QString& name) -> void override;

		auto OnPipelineChanged(const QString& filepath) -> void override;
		auto OnExecuted(Session session) -> void override;
		auto OnAborted() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
