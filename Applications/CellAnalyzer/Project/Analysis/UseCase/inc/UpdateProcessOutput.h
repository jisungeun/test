#pragma once

#include <memory>

#include "IServiceProvider.h"

#include "IProjectViewEvent.h"

#include "CellAnalyzer.Project.Analysis.UseCaseExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_UseCase_API UpdateProcessOutput final : public IProjectViewEvent {
	public:
		explicit UpdateProcessOutput(Tomocube::IServiceProvider* provider);
		~UpdateProcessOutput() override;

		auto OnSelectionChanged(SelectionCategory category, const QString& name) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
