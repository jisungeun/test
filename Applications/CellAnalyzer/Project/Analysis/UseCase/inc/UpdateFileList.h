#pragma once

#include <memory>

#include "IServiceProvider.h"

#include "IProjectViewEvent.h"

#include "CellAnalyzer.Project.Analysis.UseCaseExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_UseCase_API UpdateFileList final : public IProjectViewEvent {
	public:
		explicit UpdateFileList(Tomocube::IServiceProvider* provider);
		~UpdateFileList() override;

		auto OnGroupAdded(const QString& group) -> void override;
		auto OnGroupRenamed(const QString& oldName, const QString& newName) -> void override;
		auto OnGroupRemoved(const QString& group) -> void override;

		auto OnFileAdded(const QString& filepath) -> void override;
		auto OnFileRemoved(const QString& filepath) -> void override;
		auto OnFileMoved(const QString& filepath, const QString& group) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
