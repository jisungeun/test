#pragma once

#include <memory>

#include "IServiceProvider.h"

#include "IProjectViewEvent.h"

#include "CellAnalyzer.Project.Analysis.UseCaseExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_UseCase_API UpdateFile final : public IProjectViewEvent {
	public:
		explicit UpdateFile(Tomocube::IServiceProvider* provider);
		~UpdateFile() override;

		auto OnSelectionChanged(SelectionCategory category, const QString& name) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
