#pragma once

#include <memory>

#include "IServiceProvider.h"

#include "IProjectViewEvent.h"
#include "IPropertyViewEvent.h"

#include "CellAnalyzer.Project.Analysis.UseCaseExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_UseCase_API UpdateWork final : public IProjectViewEvent, public IPropertyViewEvent {
	public:
		explicit UpdateWork(Tomocube::IServiceProvider* provider);
		~UpdateWork() override;

		auto OnSelectionChanged(SelectionCategory category, const QString& name) -> void override;
		auto OnExecuted(Session session) -> void override;
		auto OnWorkLinked(const QString& filepath, int timepoint) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
