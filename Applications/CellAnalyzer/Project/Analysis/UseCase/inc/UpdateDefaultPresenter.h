#pragma once

#include "IServiceProvider.h"

#include "IProjectViewEvent.h"
#include "IPropertyViewEvent.h"
#include "ITaskRepoEvent.h"

#include "CellAnalyzer.Project.Analysis.UseCaseExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_UseCase_API UpdateDefaultPresenter final : public IPropertyViewEvent, public ITaskRepoEvent, public IProjectViewEvent {
	public:
		explicit UpdateDefaultPresenter(Tomocube::IServiceProvider* provider);
		~UpdateDefaultPresenter() override;

		auto OnSelectionChanged(SelectionCategory category, const QString& name) -> void override;
		auto OnFileLinked(const FileItemPtr& file, int timepoint) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
