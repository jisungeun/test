#pragma once

#include <memory>

#include "IServiceProvider.h"

#include "IProjectViewEvent.h"

#include "CellAnalyzer.Project.Analysis.UseCaseExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_UseCase_API UpdateWorkList final : public IProjectViewEvent {
	public:
		explicit UpdateWorkList(Tomocube::IServiceProvider* provider);
		~UpdateWorkList() override;

		auto OnWorkAdded(const QString& filepath, const QString& sourceName, int timepoint, TaskType type, int timestep) -> void override;
		auto OnWorkRemoved(const QString& filepath, int timepoint) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
