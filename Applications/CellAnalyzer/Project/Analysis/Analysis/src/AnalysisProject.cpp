#include "AnalysisProject.h"

#include "ServiceCollection.h"

#include "IAlertHandler.h"
#include "IMenuHandler.h"
#include "InitAnalysis.h"
#include "IScreenHandler.h"
#include "IWindowHandler.h"

#include "ExporterRepo.h"
#include "HistoryRepo.h"
#include "PresenterRepo.h"
#include "ProjectRepo.h"
#include "ProjectView.h"
#include "PropertyView.h"
#include "TaskRepo.h"

#include "UpdateDefaultPresenter.h"
#include "UpdateDefaultReport.h"
#include "UpdateFile.h"
#include "UpdateFileGroup.h"
#include "UpdateFileList.h"
#include "UpdatePipeline.h"
#include "UpdatePresenter.h"
#include "UpdateProcess.h"
#include "UpdateProcessOutput.h"
#include "UpdateProject.h"
#include "UpdateSource.h"
#include "UpdateWork.h"
#include "UpdateWorklist.h"
#include "RunTest.h"

namespace CellAnalyzer::Project::Analysis {
	struct AnalysisProject::Impl {
		std::shared_ptr<Tomocube::IServiceProvider> provider = nullptr;

		std::shared_ptr<Repo::PresenterRepo> preRepo = nullptr;
		std::shared_ptr<Repo::ProjectRepo> projRepo = nullptr;
		std::shared_ptr<Repo::TaskRepo> taskRepo = nullptr;
		std::shared_ptr<Repo::ExporterRepo> expRepo = nullptr;
		std::shared_ptr<Repo::HistoryRepo> historyRepo = nullptr;

		std::shared_ptr<View::ProjectView> projView = nullptr;
		std::shared_ptr<View::PropertyView> propView = nullptr;
		std::shared_ptr<UpdatePresenter> uc11 = nullptr;

		auto UpdateProvider(const Tomocube::IServiceProvider* parent) -> void;
		auto InitRepo() -> void;
		auto InitView() -> void;
		auto InitUseCase() -> void;
		auto InitUI() const -> void;
	};

	auto AnalysisProject::Impl::UpdateProvider(const Tomocube::IServiceProvider* parent) -> void {
		const auto col = std::make_unique<Tomocube::DependencyInjection::ServiceCollection>();

		col->MergeProvider(parent);
		col->AddScoped<Repo::ProjectRepo, IProjectRepo>()
			->AddScoped<Repo::TaskRepo, ITaskRepo>()
			->AddScoped<Repo::ExporterRepo, IExporterRepo>()
			->AddScoped<Repo::HistoryRepo, IHistoryRepo>()
			->AddScoped<Repo::PresenterRepo, IPresenterRepo>()
			->AddScoped<View::ProjectView, IProjectView>()
			->AddScoped<View::PropertyView, IPropertyView>();

		provider = col->BuildProvider();
	}

	auto AnalysisProject::Impl::InitRepo() -> void {
		projRepo = provider->GetService<Repo::ProjectRepo>();
		taskRepo = provider->GetService<Repo::TaskRepo>();
		expRepo = provider->GetService<Repo::ExporterRepo>();
		historyRepo = provider->GetService<Repo::HistoryRepo>();
		preRepo = provider->GetService<Repo::PresenterRepo>();
	}

	auto AnalysisProject::Impl::InitView() -> void {
		projView = provider->GetService<View::ProjectView>();
		propView = provider->GetService<View::PropertyView>();
	}

	auto AnalysisProject::Impl::InitUseCase() -> void {
		const auto uc1 = std::make_shared<UpdateFile>(provider.get());
		const auto uc2 = std::make_shared<UpdateFileList>(provider.get());
		const auto uc3 = std::make_shared<UpdateProcess>(provider.get());
		const auto uc4 = std::make_shared<UpdateProject>(provider.get());
		const auto uc5 = std::make_shared<UpdatePipeline>(provider.get());
		const auto uc6 = std::make_shared<UpdateProcessOutput>(provider.get());
		const auto uc7 = std::make_shared<UpdateWork>(provider.get());
		const auto uc8 = std::make_shared<UpdateWorkList>(provider.get());
		const auto uc9 = std::make_shared<UpdateFileGroup>(provider.get());
		const auto uc10 = std::make_shared<UpdateSource>(provider.get());
		uc11 = std::make_shared<UpdatePresenter>(provider.get());
		const auto uc12 = std::make_shared<UpdateDefaultPresenter>(provider.get());
		const auto uc13 = std::make_shared<UpdateDefaultReport>(provider.get());
		const auto uc14 = std::make_shared<RunTest>(provider.get());
		const auto uc15 = std::make_shared<UpdatePresenter>(provider.get());

		const auto handler = provider->GetService<IScreenHandler>();
		handler->AddEvent(uc11);

		projView->AddEvent(uc1);
		projView->AddEvent(uc2);
		projView->AddEvent(uc3);
		projView->AddEvent(uc4);
		projView->AddEvent(uc5);
		projView->AddEvent(uc6);
		projView->AddEvent(uc7);
		projView->AddEvent(uc8);
		projView->AddEvent(uc9);
		projView->AddEvent(uc10);
		propView->AddEvent(uc3);
		propView->AddEvent(uc5);
		propView->AddEvent(uc4);
		propView->AddEvent(uc7);
		projRepo->AddEvent(projView);
		taskRepo->AddEvent(projView);

		projView->AddEvent(uc12);
		propView->AddEvent(uc12);
		taskRepo->AddEvent(uc12);

		projView->AddEvent(uc13);
		propView->AddEvent(uc13);
		taskRepo->AddEvent(uc13);

		projView->AddEvent(uc14);
		projView->AddEvent(uc15);
	}

	auto AnalysisProject::Impl::InitUI() const -> void {
		const auto menuHandler = provider->GetService<IMenuHandler>();
		const auto windowHandler = provider->GetService<IWindowHandler>();
		const auto screenHandler = provider->GetService<IScreenHandler>();

		windowHandler->Show(propView, WindowPosition::Left);
		windowHandler->Show(projView, WindowPosition::Left);
		menuHandler->AddWindow(projView);
		menuHandler->AddWindow(propView);
	}

	AnalysisProject::AnalysisProject(const Tomocube::IServiceProvider* provider) : IProject(), d(new Impl) {
		d->UpdateProvider(provider);
		d->InitRepo();
	}

	AnalysisProject::~AnalysisProject() = default;

	auto AnalysisProject::GetName() const -> QString {
		return "Analysis Project";
	}

	auto AnalysisProject::GetFormat() const -> QString {
		return "tap";
	}

	auto AnalysisProject::GetDescription() const -> QString {
		return "Open or create an analysis project.";
	}

	auto AnalysisProject::GetIcon() const -> QString {
		return ":/Project/Analysis.svg";
	}

	auto AnalysisProject::GetUrl() const -> QString {
		if (d->projRepo)
			return d->projRepo->GetLocation();

		return {};
	}

	auto AnalysisProject::GetProjectName(const QString& url) const -> QString {
		if (d->projRepo->Open(url))
			return d->projRepo->GetName();

		return {};
	}

	auto AnalysisProject::FindUrl() -> QString {
		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto init = std::make_shared<View::InitAnalysis>(d->provider.get());

		if (handler->ShowDialog(init) == QDialog::Accepted)
			return init->GetUrl();

		return {};
	}

	auto AnalysisProject::Initialize(const QString& url) -> bool {
		if (d->projRepo->Open(url)) {
			d->preRepo->Load();
			d->taskRepo->Load();
			d->preRepo->SetPipeline(d->taskRepo->GetPipeline());
			d->InitView();
			d->InitUI();
			d->InitUseCase();

			return true;
		}
		return false;
	}

	auto AnalysisProject::Dispose() -> void {
		const auto handler = d->provider->GetService<IScreenHandler>();
		handler->RemoveEvent(d->uc11);

		d->projRepo = nullptr;
		d->taskRepo = nullptr;
		d->preRepo = nullptr;
		d->projView = nullptr;
		d->propView = nullptr;
	}
}
