#pragma once

#include "IView.h"

#include "IProjectViewEvent.h"

#include "CellAnalyzer.Project.Analysis.ViewModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_ViewModel_API IProjectView : public IView {
	public:
		virtual auto AddEvent(const std::shared_ptr<IProjectViewEvent>& event) -> void = 0;
		virtual auto RemoveEvent(const std::shared_ptr<IProjectViewEvent>& event) -> void = 0;

		virtual auto Select(SelectionCategory category, const QString& name = {}) -> void = 0;

		virtual auto UpdateExporter() -> void = 0;
		virtual auto UpdatePipeline() -> void = 0;
		virtual auto UpdateFileList() -> void = 0;

		virtual auto GetSelectionCategory() const -> SelectionCategory = 0;
		virtual auto GetSelectionName() const -> QString = 0;
	};
}
