#pragma once

#include <QMap>
#include <QString>

#include "IPipeline.h"

#include "CellAnalyzer.Project.Analysis.ViewModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	enum class TaskType;

	enum class SelectionCategory {
		None,
		Project,
		Pipeline,
		SourceList,
		Source,
		Process,
		ProcessOutput,
		FileGroup,
		File,
		Work
	};

	class CellAnalyzer_Project_Analysis_ViewModel_API IProjectViewEvent {
	public:
		virtual ~IProjectViewEvent();

		virtual auto OnSelectionChanged(SelectionCategory category, const QString& name) -> void;
		virtual auto OnPipelineChanged(const QString& filepath) -> void;

		virtual auto OnGroupAdded(const QString& group) -> void;
		virtual auto OnGroupRemoved(const QString& group) -> void;
		virtual auto OnGroupRenamed(const QString& oldName, const QString& newName) -> void;

		virtual auto OnFileAdded(const QString& filepath) -> void;
		virtual auto OnFileRemoved(const QString& filepath) -> void;
		virtual auto OnFileMoved(const QString& filepath, const QString& group) -> void;

		virtual auto OnWorkAdded(const QString& filepath, const QString& sourceName, int timepoint, TaskType type, int timestep) -> void;
		virtual auto OnWorkRemoved(const QString& filepath, int timepoint) -> void;
		virtual auto OnWorkLinked(const QString& filepath, int timepoint) -> void;

		virtual auto OnTestRun() -> void;
		virtual auto OnPresenterAdded(const QString& presenterID, const QMap<QString, QStringList>& dataMap, const QString& windowTitle) -> void;
	};
}
