#pragma once

#include "IPropertyItem.h"

#include "CellAnalyzer.Project.Analysis.ViewModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class CellAnalyzer_Project_Analysis_ViewModel_API IPropertyViewEvent {
	public:
		virtual ~IPropertyViewEvent();

		virtual auto OnPropertyChanged(Session session, const QString& name, const Pipeline::AttrValue& value) -> void;

		virtual auto OnExecuted(Session session) -> void;
		virtual auto OnDiscarded(Session session) -> void;
		virtual auto OnSaved(Session session) -> void;
		virtual auto OnAborted() -> void;
	};
}
