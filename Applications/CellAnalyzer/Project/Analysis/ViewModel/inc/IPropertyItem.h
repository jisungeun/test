#pragma once

#include "IProcessAttrType.h"

#include "CellAnalyzer.Project.Analysis.ViewModelExport.h"

namespace CellAnalyzer::Project::Analysis {
	class IPropertyItem;
	using PropertyPtr = std::shared_ptr<IPropertyItem>;
	using PropertyList = QList<PropertyPtr>;
	using Session = uint64_t;

	class CellAnalyzer_Project_Analysis_ViewModel_API IPropertyItem {
	public:
		virtual ~IPropertyItem() = default;

		virtual auto GetName() const -> QString = 0;
		virtual auto GetGroup() const -> QString = 0;
		virtual auto GetValue() const -> Pipeline::AttrValue = 0;
		virtual auto GetDescription() const -> QString = 0;

		virtual auto SetDescription(const QString& desc) -> void = 0;
		virtual auto SetValue(const Pipeline::AttrValue& value) -> void = 0;
		virtual auto SetModel(const Pipeline::AttrModel& model) -> void = 0;
		virtual auto SetState(Pipeline::AttrState state) -> void = 0;
	};
}
