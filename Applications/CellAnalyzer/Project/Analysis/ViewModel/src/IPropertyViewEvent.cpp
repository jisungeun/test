#include "IPropertyViewEvent.h"

namespace CellAnalyzer::Project::Analysis {
	IPropertyViewEvent::~IPropertyViewEvent() = default;

	auto IPropertyViewEvent::OnPropertyChanged(Session session, const QString& name, const Pipeline::AttrValue& value) -> void {}

	auto IPropertyViewEvent::OnExecuted(Session session) -> void {}

	auto IPropertyViewEvent::OnDiscarded(Session session) -> void {}

	auto IPropertyViewEvent::OnSaved(Session session) -> void {}

	auto IPropertyViewEvent::OnAborted() -> void {}
}
