#include "IProjectViewEvent.h"

namespace CellAnalyzer::Project::Analysis {
	IProjectViewEvent::~IProjectViewEvent() = default;

	auto IProjectViewEvent::OnSelectionChanged(SelectionCategory category, const QString& name) -> void {}

	auto IProjectViewEvent::OnPipelineChanged(const QString& filepath) -> void {}

	auto IProjectViewEvent::OnGroupAdded(const QString& group) -> void {}

	auto IProjectViewEvent::OnGroupRemoved(const QString& group) -> void {}

	auto IProjectViewEvent::OnGroupRenamed(const QString& oldName, const QString& newName) -> void {}

	auto IProjectViewEvent::OnFileAdded(const QString& filepath) -> void {}

	auto IProjectViewEvent::OnFileRemoved(const QString& filepath) -> void {}

	auto IProjectViewEvent::OnFileMoved(const QString& filepath, const QString& group) -> void {}

	auto IProjectViewEvent::OnWorkAdded(const QString& filepath, const QString& sourceName, int timepoint, TaskType type, int timestep) -> void {}

	auto IProjectViewEvent::OnWorkRemoved(const QString& filepath, int timepoint) -> void {}

	auto IProjectViewEvent::OnWorkLinked(const QString& filepath, int timepoint) -> void {}

	auto IProjectViewEvent::OnTestRun() -> void {}

	auto IProjectViewEvent::OnPresenterAdded(const QString& presenterID, const QMap<QString, QStringList>& dataMap, const QString& windowTitle) -> void { }
}
