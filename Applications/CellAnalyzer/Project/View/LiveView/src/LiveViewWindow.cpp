#include "LiveViewWindow.h"

#include <Inventor/events/SoKeyboardEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/SoPickedPoint.h>
#include <Medical/helpers/MedicalHelper.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeData.h>

#include <OivActivator.h>

#include <Inventor/Qt/SoQt.h>

namespace CellAnalyzer::Project::View::LiveView {
	struct LiveViewWindow::Impl {
		bool right_mouse_pressed { false };
		SbVec2f right_stamp { SbVec2f(0, 0) };
		float prev_scale { 1 };

		bool left_mouse_pressed { false };
		bool middle_mouse_pressed { false };

		SoSwitch* labelRoot { nullptr };
		bool highlight2d { false };
	};

	LiveViewWindow::LiveViewWindow(QWidget* parent) : QOivRenderWindow(parent, true), d { new Impl } {
		setDefaultWindowType();
	}

	LiveViewWindow::~LiveViewWindow() { }

	auto LiveViewWindow::setDefaultWindowType() -> void {
		//set default gradient background color
		const SbVec3f start_color = { 0.0, 0.0, 0.0 };
		const SbVec3f end_color = { 0.0, 0.0, 0.0 };
		setGradientBackground(start_color, end_color);

		//set default camera type
		setCameraType(true);//orthographic
	}

	auto LiveViewWindow::SetHighlightRoot(SoSwitch* rootSW, bool is2D) -> void {
		d->labelRoot = rootSW;
		d->highlight2d = is2D;
	}

	auto LiveViewWindow::MouseButtonEvent(SoEventCallback* node) -> void {
		if (false == isNavigation())
			return;
		const auto mouseButton = reinterpret_cast<const SoMouseButtonEvent*>(node->getEvent());
		const auto vr = getViewportRegion();
		const auto norm_pos = mouseButton->getNormalizedPosition(vr);
		if (SoMouseButtonEvent::isButtonDoubleClickEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
			ResetView2D(Direction2D::XY);
			emit sigPanning();
			emit sigZooming();
		}
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
			d->left_mouse_pressed = true;
			emit sigMouseClick();

			if (d->labelRoot) {
				const auto action = node->getAction();
				const SoEvent* event = node->getEvent();
				auto viewportRegion = action->getViewportRegion();
				auto mousePos = event->getPosition(viewportRegion);
				SoRayPickAction rayPick = SoRayPickAction(viewportRegion);
				rayPick.setPoint(mousePos);
				rayPick.apply(action->getPickRoot());
				SoPickedPoint* pickedPt = rayPick.getPickedPoint();
				if (pickedPt) {
					if (const auto volData = MedicalHelper::find<SoVolumeData>(d->labelRoot)) {
						auto zCandidate = 0;

						if (false == d->highlight2d) {
							if (const auto slice = MedicalHelper::find<SoOrthoSlice>(d->labelRoot)) {
								zCandidate = static_cast<int>(slice->sliceNumber.getValue());
							}
						}

						const auto cur_seed = pickedPt->getPoint();
						auto idx = volData->XYZToVoxel(cur_seed);
						const auto dx = static_cast<int32_t>(idx[0]);
						const auto dy = static_cast<int32_t>(idx[1]);
						const auto dz = zCandidate;
						auto pos = SbVec3i32(dx, dy, dz);
						const auto cur_label = static_cast<int>(volData->getValue(pos));
						emit sigLabelHighlight(cur_label);
					}
				}
			}
		}
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1))
			d->left_mouse_pressed = false;
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
			d->right_mouse_pressed = true;
			d->right_stamp = norm_pos;
			d->prev_scale = 1.0;
		}
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
			d->right_mouse_pressed = false;
			d->right_stamp = SbVec2f(0, 0);
			d->prev_scale = 1.0;
		}
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
			d->middle_mouse_pressed = true;
			startPan(norm_pos);
		}
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2))
			d->middle_mouse_pressed = false;
	}

	auto LiveViewWindow::MouseMoveEvent(SoEventCallback* node) -> void {
		if (false == isNavigation())
			return;
		const auto moveEvent = reinterpret_cast<const SoLocation2Event*>(node->getEvent());
		const auto vr = getViewportRegion();
		const auto norm_pos = moveEvent->getNormalizedPosition(vr);
		emit sigMousePos(norm_pos[0], norm_pos[1]);
		if (d->right_mouse_pressed) {
			auto diff = norm_pos[1] - d->right_stamp[1];
			const auto factor = 1.0f - diff * 1.5f;
			if (factor > 0) {
				getCamera()->scaleHeight(factor / d->prev_scale);
				d->prev_scale = factor;
			}
			emit sigZooming();
		}
		if (d->left_mouse_pressed)
			node->setHandled();
		if (d->middle_mouse_pressed) {
			panCamera(norm_pos);
			emit sigPanning();
		}
	}

	auto LiveViewWindow::MouseWheelEvent(SoEventCallback* node) -> void {
		const auto mouseWheel = reinterpret_cast<const SoMouseWheelEvent*>(node->getEvent());
		const auto delta = mouseWheel->getDelta() > 0 ? 1 : -1;
		emit sigMouseWheel(delta);
	}

	auto LiveViewWindow::KeyboardEvent(SoEventCallback* node) -> void {
		const auto keyEvent = node->getEvent();
		if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
			if (false == isNavigation()) {
				setInteractionMode(true);
				setAltPressed(true);
			}
			node->setHandled();
		}
		if (SoKeyboardEvent::isKeyReleaseEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
			if (isAltPressed()) {
				setInteractionMode(false);
				setAltPressed(false);
			}
			node->setHandled();
		}
	}

	auto LiveViewWindow::ResetView2D(Direction2D direction) -> void {
		MedicalHelper::Axis ax;
		auto root = getSceneGraph();
		const auto volData = MedicalHelper::find<SoVolumeData>(root);//find any volume Data
		if (!volData)
			return;

		float slack;
		auto dims = volData->getDimension();
		auto spacing = volData->getVoxelSize();

		float x_len = dims[0] * spacing[0];
		float y_len = dims[1] * spacing[1];
		float z_len = dims[2] * spacing[2];

		auto wh = static_cast<float>(size().height());
		auto ww = static_cast<float>(size().width());
		auto windowSlack = wh > ww ? ww / wh : wh / ww;

		if (direction._to_integral() == Direction2D::XY) {
			ax = MedicalHelper::AXIAL;
			slack = y_len < x_len ? y_len / x_len : x_len / y_len;
		} else if (direction._to_integral() == Direction2D::YZ) {
			ax = MedicalHelper::SAGITTAL;
			slack = z_len / y_len;
		} else if (direction._to_integral() == Direction2D::XZ) {
			ax = MedicalHelper::CORONAL;
			slack = z_len / x_len;
		} else
			return;

		auto largerLen = (x_len > y_len) ? x_len : y_len;

		if (windowSlack > slack && windowSlack > 0)
			slack = windowSlack > 1 ? 1.0 : windowSlack;
		MedicalHelper::orientView(ax, getCamera(), volData, slack);
		if (ax == MedicalHelper::SAGITTAL)
			getCamera()->orientation.setValue(SbVec3f(0, 1, 0), M_PI);
		if (ax == MedicalHelper::CORONAL)
			getCamera()->orientation.setValue(SbVec3f(1, 0, 0), M_PI * 2);
	}
}
