#include "LiveViewMeta.h"

namespace CellAnalyzer::Project::View::LiveView {
	struct LiveViewMeta::Impl {
		double transparency { 0 };
		double min { 0 }, max { 1 };
		int decimals { 0 };
		double singleStep { 1 };
		QColor singletone { QColor(255, 0, 0) };
		double gamma { 1 };
		QString colormapID { "Custom" };
		QList<float> colormap;
		double divider { 1 };
		double offset { 0 };
		int maxZ { 1 };
		int currentZ { 1 };
	};

	LiveViewMeta::LiveViewMeta() : d { new Impl } { }

	LiveViewMeta::~LiveViewMeta() { }

	auto LiveViewMeta::SetCurrentZ(int z) -> void {
		d->currentZ = z;
	}

	auto LiveViewMeta::GetCurrentZ() const -> int {
		return d->currentZ;
	}

	auto LiveViewMeta::SetMaxZ(int z) -> void {
		d->maxZ = z;
	}

	auto LiveViewMeta::GetMaxZ() const -> int {
		return d->maxZ;
	}

	auto LiveViewMeta::SetOffset(double offset) -> void {
		d->offset = offset;
	}

	auto LiveViewMeta::GetOffset() const -> double {
		return d->offset;
	}

	auto LiveViewMeta::SetDivider(double div) -> void {
		d->divider = div;
	}

	auto LiveViewMeta::GetDivider() const -> double {
		return d->divider;
	}

	auto LiveViewMeta::SetTransparency(double tranp) -> void {
		d->transparency = tranp;
	}

	auto LiveViewMeta::GetTransparancy() const -> double {
		return d->transparency;
	}

	auto LiveViewMeta::SetRange(double min, double max) -> void {
		d->min = min;
		d->max = max;
	}

	auto LiveViewMeta::GetRange() const -> std::tuple<double, double> {
		return std::make_tuple(d->min, d->max);
	}

	auto LiveViewMeta::SetDecimals(int dec) -> void {
		d->decimals = dec;
	}

	auto LiveViewMeta::GetDecimals() const -> int {
		return d->decimals;
	}

	auto LiveViewMeta::SetSingleStep(double singleStep) -> void {
		d->singleStep = singleStep;
	}

	auto LiveViewMeta::GetSingleStep() const -> double {
		return d->singleStep;
	}

	auto LiveViewMeta::SetSingleTone(QColor tone) -> void {
		d->singletone = tone;
	}

	auto LiveViewMeta::GetSingleTone() const -> QColor {
		return d->singletone;
	}

	auto LiveViewMeta::SetGamma(double gamma) -> void {
		d->gamma = gamma;
	}

	auto LiveViewMeta::GetGamma() const -> double {
		return d->gamma;
	}

	auto LiveViewMeta::SetColormapID(const QString& ID) -> void {
		d->colormapID = ID;
	}

	auto LiveViewMeta::GetColormapID() const -> QString {
		return d->colormapID;
	}

	auto LiveViewMeta::SetColormap(QList<float> colormap) -> void {
		if (colormap.count() != 1024) {
			return;
		}
		d->colormap.clear();
		for (auto i = 0; i < 1024; i++) {
			d->colormap.append(colormap[i]);
		}
	}

	auto LiveViewMeta::GetColormap() -> QList<float> {
		return d->colormap;
	}
}
