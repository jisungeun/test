#include <QSettings>
#include <QFileDialog>

#include "LiveViewLayerItem.h"

#include "ui_LiveViewLayer.h"
#include "LiveViewLayer.h"

#include <iostream>

namespace CellAnalyzer::Project::View::LiveView {
	struct LiveViewLayer::Impl {
		Ui::LayerControl ui;
		QList<LayerItem*> layerItems;

		QString currentID;
		QStringList currentLayers;
		QStringList colormapID;
		QMap<QString, QList<float>> colormap;
		bool toggleShowAll{ true };
		auto FindItem(int order) -> LayerItem*;
		auto FindIndex() -> int;
		auto ReorderItems() -> void;
	};

	auto LiveViewLayer::Impl::FindIndex() -> int {
		for (auto item : layerItems) {
			if (item->GetChecked()) {
				return item->GetOrder();
			}
		}
		return -1;
	}

	auto LiveViewLayer::SetToggleViz(const QString& ID, bool show) -> void {
		for (const auto item : d->layerItems) {
			if (item->GetName() == ID) {
				item->SetVisibility(show);
				break;
			}
		}
	}

	auto LiveViewLayer::GetTF(const QString& ID) -> std::tuple<float*, int> {
		for (auto item : d->layerItems) {
			if (item->GetName() == ID) {
				return std::make_tuple(item->GetColorMap(), item->GetMaxTFstep());
			}
		}
		return std::make_tuple(nullptr, 256);
	}


	auto LiveViewLayer::Impl::FindItem(int order) -> LayerItem* {
		for (auto item : layerItems) {
			if (item->GetOrder() == order) {
				return item;
			}
		}
		return {};
	}

	auto LiveViewLayer::Impl::ReorderItems() -> void {
		QStringList orders;
		for (auto i = 0; i < ui.dragList->count() - 1; i++) {
			const auto item = dynamic_cast<LayerItem*>(ui.dragList->itemAt(i)->widget());
			item->SetOrder(i);
			orders.push_back(item->GetName());
		}
		currentLayers = orders;
	}

	auto LiveViewLayer::AddColorMap(const QString& title, float* colormap) -> void {
		QList<float> cm;
		for (auto i = 0; i < 1024; i++) {
			cm.append(*(colormap + i));
		}
		d->colormap[title] = cm;
		d->colormapID.append(title);
	}

	LiveViewLayer::LiveViewLayer(QWidget* parent) : QWidget(parent), IView(), d { new Impl } {
		d->ui.setupUi(this);

		d->ui.moveUpBtn->setIcon(QIcon(":/Widget/MoveUp.svg"));
		d->ui.moveDownBtn->setIcon(QIcon(":/Widget/MoveDown.svg"));
		d->ui.moveTopBtn->setIcon(QIcon(":/Widget/MoveToTop.svg"));
		d->ui.toggleVizBtn->setIcon(QIcon(":/Widget/ShowAll.svg"));
		d->ui.toggleVizBtn->setIconSize(QSize(28, 28));
		d->ui.captureBtn->setIcon(QIcon(":/Widget/CaptureCamera.svg"));
		d->ui.captureBtn->setIconSize(QSize(28, 28));
		d->ui.removeBtn->setIcon(QIcon(":/Widget/Remove.svg"));
		d->ui.removeBtn->setIconSize(QSize(28, 28));

		d->ui.upsampleSpin->setRange(1, 5);
		d->ui.upsampleSpin->setSingleStep(1);
		d->ui.upsampleSpin->setValue(1);

		d->ui.toggleVizBtn->setToolTip("Show/Hide");
		d->ui.moveTopBtn->setToolTip("Move to top");
		d->ui.moveUpBtn->setToolTip("Move up");
		d->ui.moveDownBtn->setToolTip("Move down");
		d->ui.removeBtn->setToolTip("Remove layer from viewer");
		d->ui.captureBtn->setToolTip("Take screenshot");

		connect(d->ui.upsampleSpin, QOverload<int>::of(&QSpinBox::valueChanged), this, [this](int value) {
			emit sigUpsample(value);
		});

		connect(d->ui.moveUpBtn, &QPushButton::clicked, this, [this] {
			OnUp();
		});
		connect(d->ui.moveDownBtn, &QPushButton::clicked, this, [this] {
			OnDown();
		});
		connect(d->ui.moveTopBtn, &QPushButton::clicked, this, [this] {
			OnTop();
		});
		connect(d->ui.toggleVizBtn, &QPushButton::clicked, this, [this] {
			OnToggleVizAll();
		});
		connect(d->ui.captureBtn, &QPushButton::clicked, this, [this] {
			OnCapture();
		});
		connect(d->ui.removeBtn, &QPushButton::clicked, this, [this] {
			OnRemove();
		});
		connect(d->ui.zSpin, QOverload<int>::of(&QSpinBox::valueChanged), this, [this](int value) {
			OnZChanged(value);
		});
		d->ui.zSpin->setEnabled(false);
	}

	auto LiveViewLayer::OnZChanged(int idx) -> void {
		emit sigZChanged(idx);
	}

	auto LiveViewLayer::SetZ(int z) -> void {
		d->ui.zSpin->blockSignals(true);
		d->ui.zSpin->setValue(z);
		d->ui.zSpin->blockSignals(false);
	}

	LiveViewLayer::~LiveViewLayer() = default;

	auto LiveViewLayer::Clear() const -> void {
		for (auto i = d->layerItems.count() - 1; i >= 0; i--) {
			const auto item = d->layerItems[i];
			d->ui.dragList->removeWidget(item);
			d->layerItems.removeOne(item);
			delete item;
		}
	}

	auto LiveViewLayer::OnRemove() -> void {
		auto idx = d->FindIndex();
		if (idx < 0) {
			return;
		}
		const auto targetItem = d->FindItem(idx);
		const auto name = targetItem->GetName();
		d->ui.dragList->removeWidget(targetItem);
		d->layerItems.removeOne(targetItem);
		delete targetItem;

		emit sigRemove(name);

		d->ReorderItems();

		emit sigLayerOrder(d->currentLayers);
	}

	auto LiveViewLayer::RemoveLayer(const QString& ID) -> void {
		for (const auto item : d->layerItems) {
			if (item->GetName() == ID) {
				d->ui.dragList->removeWidget(item);
				d->layerItems.removeOne(item);
				delete item;
				break;
			}
		}
		d->ReorderItems();
		emit sigLayerOrder(d->currentLayers);
	}

	auto LiveViewLayer::UpdateZRange(int max) -> void {
		d->ui.zSpin->blockSignals(true);
		d->ui.zSpin->setRange(1, max);
		d->ui.zSpin->blockSignals(false);
	}

	auto LiveViewLayer::SetHighlight(const QString& ID, int label) -> void {
		for (const auto l : d->layerItems) {
			if (l->GetName() == ID) {
				l->SetHighlight(label);
				return;
			}
		}
	}

	auto LiveViewLayer::AddLayer(const QString& ID, const QString& iconPath, LiveViewMetaPtr meta) -> void {
		if (d->layerItems.count() == 0) {
			if (meta->GetMaxZ() < 2) {
				d->ui.zSpin->setEnabled(false);
			} else {
				d->ui.zSpin->setEnabled(true);
			}
		}
		const auto item = new LayerItem(meta);
		item->SetName(ID);
		item->SetIcon(iconPath);
		for (auto i = 0; i < d->colormapID.count(); i++) {
			const auto cid = d->colormapID[i];
			item->AddColorMap(cid, d->colormap[cid]);
		}
		item->InitUI();
		d->ui.dragList->insertWidget(0, item);
		d->layerItems.append(item);

		d->ReorderItems();
		emit sigLayerOrder(d->currentLayers);

		connect(item, &LayerItem::sigViz, this, [this, item](bool isVisible) {
			OnToggleViz(item->GetName(), isVisible);
		});
		connect(item, &LayerItem::sigSelect, this, [this, item](bool selected) {
			if (selected) {
				for (auto tmpItem : d->layerItems) {
					if (tmpItem->GetName() != item->GetName()) {
						tmpItem->SetChecked(false);
					}
				}
				emit sigItemSelected(item->GetName());
			}
		});
		connect(item, &LayerItem::sigColormap, this, [this, item](float* cm, int maxNum, bool isLabel) {
			emit sigUpdateColormap(item->GetName(), cm, maxNum, isLabel);
		});
		connect(item, &LayerItem::sigHighlightColormap, this, [this, item](float* cm, int maxNum, int labelValue) {
			emit sigUpdateHighlightColormap(item->GetName(), cm, maxNum, labelValue);
		});
		connect(item, &LayerItem::sigDataRange, this, [this, item](double min, double max) {
			emit sigUpdateRange(item->GetName(), min, max);
		});
		connect(item, &LayerItem::sigTransp, this, [this, item](double transp) {
			emit sigupdateTranp(item->GetName(), transp);
		});
	}

	auto LiveViewLayer::ChangeName(const QString& prevName, const QString& changedName) -> void {
		for (const auto item : d->layerItems) {
			if (item->GetName() == prevName) {
				item->SetName(changedName);
				break;
			}
		}
		for (auto i = 0; i < d->currentLayers.count(); i++) {
			if (d->currentLayers[i] == prevName) {
				d->currentLayers.replace(i, changedName);
				break;
			}
		}

	}

	auto LiveViewLayer::GetCurrentID() const -> QString {
		return d->currentID;
	}

	auto LiveViewLayer::OnTop() -> void {
		auto idx = d->FindIndex();
		if (idx < 1) {
			return;
		}
		const auto targetItem = d->FindItem(idx);
		d->ui.dragList->removeWidget(targetItem);
		d->ui.dragList->insertWidget(0, targetItem);

		d->ReorderItems();
		emit sigLayerOrder(d->currentLayers);
	}

	auto LiveViewLayer::OnDown() -> void {
		auto idx = d->FindIndex();
		if (idx < 0) {
			return;
		}
		if (idx + 1 >= d->ui.dragList->count() - 1) {
			return;
		}

		const auto targetItem = d->FindItem(idx);
		const auto swapItem = d->FindItem(idx + 1);
		d->ui.dragList->replaceWidget(targetItem, swapItem);
		d->ui.dragList->insertWidget(idx + 1, targetItem);

		targetItem->SetOrder(idx + 1);
		swapItem->SetOrder(idx);

		const auto prev = d->currentLayers[idx + 1];
		d->currentLayers[idx + 1] = d->currentLayers[idx];
		d->currentLayers[idx] = prev;

		emit sigLayerOrder(d->currentLayers);
	}

	auto LiveViewLayer::OnUp() -> void {
		auto idx = d->FindIndex();
		if (idx < 0) {
			return;
		}
		if (idx - 1 < 0) {
			return;
		}
		const auto targetItem = d->FindItem(idx);
		const auto swapItem = d->FindItem(idx - 1);
		d->ui.dragList->replaceWidget(swapItem, targetItem);
		d->ui.dragList->insertWidget(idx, swapItem);

		targetItem->SetOrder(idx - 1);
		swapItem->SetOrder(idx);

		const auto prev = d->currentLayers[idx - 1];
		d->currentLayers[idx - 1] = d->currentLayers[idx];
		d->currentLayers[idx] = prev;

		emit sigLayerOrder(d->currentLayers);
	}

	auto LiveViewLayer::OnToggleVizAll() -> void {
		for (auto item : d->layerItems) {
			item->SetVisibility(d->toggleShowAll);
		}

		for (auto item : d->layerItems) {
			const auto visibility = item->GetVisibility();
			const auto name = item->GetName();
			OnToggleViz(name, visibility);
		}

		if (d->toggleShowAll) {
			d->ui.toggleVizBtn->setIcon(QIcon(":/Widget/HideAll.svg"));
			d->ui.toggleVizBtn->setIconSize(QSize(28, 28));
		}
		else {
			d->ui.toggleVizBtn->setIcon(QIcon(":/Widget/ShowAll.svg"));
			d->ui.toggleVizBtn->setIconSize(QSize(28, 28));
		}
		d->toggleShowAll = !d->toggleShowAll;
	}

	auto LiveViewLayer::OnCapture() -> void {
		const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/capture", QApplication::applicationDirPath()).toString();
		const auto path = QFileDialog::getSaveFileName(nullptr, tr("Save screen shot"), prev + "/", tr("Image files (*.png *.jpg *.tiff)"));
		if (path.isEmpty()) {
			return;
		}
		QSettings("Tomocube", "TomoAnalysis").setValue("recent/capture", path);

		emit sigCapture(path);
	}

	auto LiveViewLayer::OnToggleViz(const QString& name, bool isVisible) -> void {
		emit sigToggleViz(name, isVisible);
	}
}
