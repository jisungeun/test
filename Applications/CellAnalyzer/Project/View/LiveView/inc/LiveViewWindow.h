#pragma once

#include <enum.h>

#include <QOivRenderWindow.h>

namespace CellAnalyzer::Project::View::LiveView {
	BETTER_ENUM(Direction2D, int,
				XY = 0,
				YZ = 1,
				XZ = 2)

	class LiveViewWindow final : public QOivRenderWindow {
		Q_OBJECT
	public:
		explicit LiveViewWindow(QWidget* parent = nullptr);
		~LiveViewWindow() override;

		auto ResetView2D(Direction2D dir) -> void;

		auto SetHighlightRoot(SoSwitch* rootSW, bool is2D) -> void;

	signals:
		auto sigMouseWheel(int) -> void;
		auto sigMouseClick() -> void;
		auto sigLabelHighlight(int value) -> void;
		auto sigMousePos(float, float) -> void;
		auto sigPanning() -> void;
		auto sigZooming() -> void;

	protected:
		auto MouseButtonEvent(SoEventCallback* node) -> void override;
		auto MouseMoveEvent(SoEventCallback* node) -> void override;
		auto KeyboardEvent(SoEventCallback* node) -> void override;
		auto MouseWheelEvent(SoEventCallback* node) -> void override;

		auto setDefaultWindowType() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
