#pragma once

#include <QList>

#include "WorksetEditorDef.h"

namespace CellAnalyzer::Project::View::RoiEditor {
	struct Config {
		QString tcfPath;
		WorksetEditor::TimeSelectionOutput timeSelection;
		QList<double> realTime;
	};

	using inputStream = QList<Config>;
}
