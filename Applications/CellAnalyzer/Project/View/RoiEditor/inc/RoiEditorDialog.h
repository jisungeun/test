#pragma once

#include <memory>

#include <QDialog>
#include <QWidget>
#include <QTreeWidgetItem>
#include <QTreeWidget>
#include <ROISet.h>
#include "IView.h"
#include "RoiEditorDef.h"

#include "CellAnalyzer.Project.View.RoiEditorExport.h"

namespace CellAnalyzer::Project::View::RoiEditor {
	class DataTree final : public QTreeWidget {
		Q_OBJECT
	public:
		explicit DataTree(QWidget* parent = nullptr);
		auto SetForceDoubleClick(QTreeWidgetItem*) -> void;
	signals:
		void sigItemDoubleClicked(QTreeWidgetItem*, QString, int);//TCF path, Time Point
	protected:
		void mouseDoubleClickEvent(QMouseEvent* event) override;
	};

	class RangeWidget final : public QWidget {
		Q_OBJECT
	public:
		RangeWidget(int toolIdx, int itemIdx, QWidget* parent = nullptr);
		~RangeWidget();

		auto SetRange(int min, int max) -> void;
		auto SetMin(int min) -> void;
		auto SetMax(int max) -> void;

	signals:
		void sigRange(int toolIdx, int itemIdx, int min, int max);
	protected slots:
		void OnRangeMin(int);
		void OnRangeMax(int);
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	class CellAnalyzer_Project_View_RoiEditor_API ROISelectionDialog final : public QDialog, public IView {
		Q_OBJECT
	public:
		ROISelectionDialog(QWidget* parent = nullptr);
		~ROISelectionDialog() override;

		auto SetApplicationName(const QString& name) -> void;
		auto SetInput(inputStream input) -> void;
		auto SetCurRoiRoot(const QString& path) -> void;
		auto GetOutput() -> QMap<QString, QMap<int, TC::IO::ROISet>>;

	protected:
		bool eventFilter(QObject* watched, QEvent* event) override;

	protected slots:
		void OnToolSelected(int, bool);

		void OnMouseWheel(int);

		void OnROISelected(int, int);
		void OnDataSetSelected(QTreeWidgetItem*, QString, int);
		void OnRemove();

		void OnNextBtn();
		void OnOkBtn();
		void OnCancelBtn();
		void OnClearAllBtn();

		void OnApplyBtn();
		void OnImportBtn();
		void OnExportBtn();

		void OnItemSelected(int, int);
		void OnItemAdded(int, int);
		void OnZRangeChanged(int, int, int, int);

		void OnHtChanged(double);
		void OnCh1Changed(int);
		void OnCh2Changed(int);
		void OnCh3Changed(int);

		void OnZSpinChanged(int);
		void OnZSliderChanged(int);

		void OnHtChecked();
		void OnCh1Checked();
		void OnCh2Checked();
		void OnCh3Checked();

	private:
		auto InitConnections() -> void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
