#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoSeparator.h>

#include <SquareROISep.h>
#include <CircleROISep.h>
#include <EllipsoidROISep.h>
#include <PolygonROISep.h>
#include <LassoROISep.h>

#include "ROIToolManager.h"

using namespace TC;

namespace CellAnalyzer::Project::View::RoiEditor {
	struct ROIToolManager::Impl {
		std::shared_ptr<SquareROISep> square { nullptr };
		std::shared_ptr<CircleROISep> circle { nullptr };
		std::shared_ptr<EllipsoidROISep> ellipse { nullptr };
		std::shared_ptr<PolygonROISep> polygon { nullptr };
		std::shared_ptr<LassoROISep> lasso { nullptr };
		QList<IROISep*> toolContainer;
		QList<std::shared_ptr<ItemZRange>> itemRange[5];
		SoRef<SoSwitch> root { nullptr };

		int zMin { 1 };
		int zMax { 1 };
		int curZ { 1 };

		int currentTool = Idle;
		auto Init() -> void;
	};

	auto ROIToolManager::Impl::Init() -> void {
		root = new SoSwitch;
		root->whichChild = -3;
		square = std::make_shared<SquareROISep>();
		circle = std::make_shared<CircleROISep>();
		ellipse = std::make_shared<EllipsoidROISep>();
		polygon = std::make_shared<PolygonROISep>();
		lasso = std::make_shared<LassoROISep>();
		toolContainer.append(square.get());
		toolContainer.append(circle.get());
		toolContainer.append(ellipse.get());
		toolContainer.append(polygon.get());
		toolContainer.append(lasso.get());

		for (auto tool : toolContainer) {
			tool->SetHandleSize(0.5);
		}

		root->addChild(square->GetRoot());
		root->addChild(circle->GetRoot());
		root->addChild(ellipse->GetRoot());
		root->addChild(polygon->GetRoot());
		root->addChild(lasso->GetRoot());
	}

	auto ROIToolManager::InitConnections() -> void {
		connect(d->square.get(), SIGNAL(sigFinish(int)), this, SLOT(OnSquareAdded(int)));
		connect(d->square.get(), SIGNAL(sigSelected(int)), this, SLOT(OnSquareSelected(int)));

		connect(d->circle.get(), SIGNAL(sigFinish(int)), this, SLOT(OnCircleAdded(int)));
		connect(d->circle.get(), SIGNAL(sigSelected(int)), this, SLOT(OnCircleSelected(int)));

		connect(d->ellipse.get(), SIGNAL(sigFinish(int)), this, SLOT(OnEllipseAdded(int)));
		connect(d->ellipse.get(), SIGNAL(sigSelected(int)), this, SLOT(OnEllipseSelected(int)));

		connect(d->polygon.get(), SIGNAL(sigFinish(int)), this, SLOT(OnPolygonAdded(int)));
		connect(d->polygon.get(), SIGNAL(sigSelected(int)), this, SLOT(OnPolygonSelected(int)));

		connect(d->lasso.get(), SIGNAL(sigFinish(int)), this, SLOT(OnLassoAdded(int)));
		connect(d->lasso.get(), SIGNAL(sigSelected(int)), this, SLOT(OnLassoSelected(int)));
	}

	ROIToolManager::ROIToolManager(QObject* parent) : QObject(parent), d { new Impl } {
		d->Init();
		InitConnections();
	}

	ROIToolManager::~ROIToolManager() { }

	auto ROIToolManager::SetHandleSize(double size) -> void {
		for (const auto& tool : d->toolContainer) {
			tool->SetHandleSize(size);
		}
	}

	auto ROIToolManager::SetCurrentZ(int zIdx) -> void {
		if (zIdx < d->zMin || zIdx > d->zMax) {
			return;
		}
		d->curZ = zIdx;
		for (auto i = 0; i < 5; i++) {
			auto tool = d->itemRange[i];
			for (auto j = 0; j < tool.count(); j++) {
				if (zIdx > tool[j]->max || zIdx < tool[j]->min) {
					d->toolContainer[i]->ToggleVisibility(j, false);
				} else {
					d->toolContainer[i]->ToggleVisibility(j, true);
				}
			}
		}
	}

	auto ROIToolManager::SetGlobalZMinMax(int min, int max) -> void {
		d->zMin = min;
		d->zMax = max;
	}

	auto ROIToolManager::SetItemZRange(int toolIdx, int itemIdx, int zMin, int zMax) -> void {
		auto tool = d->itemRange[toolIdx];
		if (tool.count() <= itemIdx) {
			return;
		}
		tool[itemIdx]->min = zMin;
		tool[itemIdx]->max = zMax;
		SetCurrentZ(d->curZ);
	}

	auto ROIToolManager::GetRootSwitch() -> SoSwitch* {
		return d->root.ptr();
	}

	auto ROIToolManager::HighlightItem(int itemIdx) -> bool {
		return d->toolContainer[d->currentTool]->HighlightItem(itemIdx);
	}

	auto ROIToolManager::ActivateTool(int idx) -> void {
		//d->root->whichChild = idx;
		for (auto i = 0; i < d->toolContainer.count(); i++) {
			if (i != idx) {
				d->toolContainer[i]->Deactivate();
			} else {
				d->toolContainer[i]->Activate();
				d->currentTool = idx;
			}
		}
	}

	auto ROIToolManager::DeactivateTool(int idx) -> void {
		if (d->root->whichChild.getValue() == idx) {
			//d->root->whichChild = -1;
		}
		d->toolContainer[idx]->Deactivate();
		d->currentTool = -1;
	}

	auto ROIToolManager::DeactivateAll() -> void {
		//d->root->whichChild = -1;
		for (const auto t : d->toolContainer) {
			t->Deactivate();
		}
		d->currentTool = -1;
	}

	auto ROIToolManager::AddROI(int toolIdx, IO::ROI roi) -> void {
		if (toolIdx >= d->toolContainer.count()) {
			return;
		}
		QList<pointInfo> roiVertices;
		for (auto i = 0; i < roi.GetVertices().count(); i++) {
			const auto [x, y, z] = roi.GetVertices()[i];
			pointInfo pt = std::make_tuple(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z));
			roiVertices.append(pt);
		}
		d->toolContainer[toolIdx]->AddROI(roiVertices);
		const auto [min, max] = roi.GetZRange();
		d->itemRange[toolIdx].last()->min = min;
		d->itemRange[toolIdx].last()->max = max;
	}

	auto ROIToolManager::SelectItem(int itemIdx) -> bool {
		if (d->currentTool < 0 || d->currentTool >= d->toolContainer.count()) {
			return false;
		}
		const auto tool = d->toolContainer[d->currentTool];
		return tool->HighlightItem(itemIdx);
	}

	auto ROIToolManager::DeleteAll() -> void {
		for (auto i = 0; i < d->toolContainer.count(); i++) {
			const auto tool = d->toolContainer[i];
			tool->Clear();
			d->itemRange[i].clear();
		}
	}

	auto ROIToolManager::DeleteItem(int itemIdx) -> bool {
		if (d->currentTool < 0 || d->currentTool >= d->toolContainer.count()) {
			return false;
		}
		const auto tool = d->toolContainer[d->currentTool];
		d->itemRange[d->currentTool].removeAt(itemIdx);
		return tool->DeleteItem(itemIdx);
	}

	auto ROIToolManager::GetROIInfo(int toolIdx) -> QList<IO::ROI> {
		QList<IO::ROI> result;
		const auto vertices = d->toolContainer[toolIdx]->GetROIVertices();
		IO::ROIShapeType type = IO::ROIShapeType::SQUARE;
		QString toolID = "Square";
		if (toolIdx == 1) {
			type = IO::ROIShapeType::CIRCLE;
			toolID = "Circle";
		} else if (toolIdx == 2) {
			type = IO::ROIShapeType::ELLIPSOID;
			toolID = "Ellipse";
		} else if (toolIdx == 3) {
			type = IO::ROIShapeType::POLYGON;
			toolID = "Polygon";
		} else if (toolIdx == 4) {
			type = IO::ROIShapeType::LASSO;
			toolID = "Lasso";
		}
		auto ranges = d->itemRange[toolIdx];
		for (auto i = 0; i < vertices.count(); i++) {
			const auto vertexList = vertices[i];
			auto range = ranges[i];
			IO::ROI roi;
			roi.SetID(QString("%1_%2").arg(toolID).arg(i));
			roi.SetZRange(range->min, range->max);
			roi.SetShapeType(type);
			QList<IO::ROI::Point3D> pts;
			for (auto j = 0; j < vertexList.count(); j++) {
				const auto [x, y, z] = vertexList[j];
				IO::ROI::Point3D point = std::make_tuple(static_cast<double>(x), static_cast<double>(y), static_cast<double>(z));
				pts.append(point);
			}
			roi.SetVertices(pts);
			result.append(roi);
		}
		return result;
	}


	//SLOTS
	void ROIToolManager::OnSquareAdded(int itemIdx) {
		auto range = std::make_shared<ItemZRange>();
		range->min = d->zMin;
		range->max = d->zMax;
		d->itemRange[0].append(range);
		emit sigRoiAdded(SQUARE, itemIdx);
	}

	void ROIToolManager::OnSquareSelected(int itemIdx) {
		emit sigRoiSelected(SQUARE, itemIdx);
	}

	void ROIToolManager::OnCircleAdded(int itemIdx) {
		auto range = std::make_shared<ItemZRange>();
		range->min = d->zMin;
		range->max = d->zMax;
		d->itemRange[1].append(range);
		emit sigRoiAdded(CIRCLE, itemIdx);
	}

	void ROIToolManager::OnCircleSelected(int itemIdx) {
		emit sigRoiSelected(CIRCLE, itemIdx);
	}

	void ROIToolManager::OnEllipseAdded(int itemIdx) {
		auto range = std::make_shared<ItemZRange>();
		range->min = d->zMin;
		range->max = d->zMax;
		d->itemRange[2].append(range);
		emit sigRoiAdded(ELLIPSE, itemIdx);
	}

	void ROIToolManager::OnEllipseSelected(int itemIdx) {
		emit sigRoiSelected(ELLIPSE, itemIdx);
	}

	void ROIToolManager::OnPolygonAdded(int itemIdx) {
		auto range = std::make_shared<ItemZRange>();
		range->min = d->zMin;
		range->max = d->zMax;
		d->itemRange[3].append(range);
		emit sigRoiAdded(POLYGON, itemIdx);
	}

	void ROIToolManager::OnPolygonSelected(int itemIdx) {
		emit sigRoiSelected(POLYGON, itemIdx);
	}

	void ROIToolManager::OnLassoAdded(int itemIdx) {
		auto range = std::make_shared<ItemZRange>();
		range->min = d->zMin;
		range->max = d->zMax;
		d->itemRange[4].append(range);
		emit sigRoiAdded(LASSO, itemIdx);
	}

	void ROIToolManager::OnLassoSelected(int itemIdx) {
		emit sigRoiSelected(LASSO, itemIdx);
	}
}
