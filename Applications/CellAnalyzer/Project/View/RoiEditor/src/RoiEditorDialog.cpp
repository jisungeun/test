#include <QFileDialog>
#include <QButtonGroup>
#include <QSettings>
#include <QSpinBox>
#include <QFileInfo>
#include <QMouseEvent>
#include <QTableWidget>
#include <QHeaderView>
#include <Inventor/Qt/SoQt.h>

#include "ROIRenderWindow.h"
#include "ROISceneManager.h"
#include "ROIToolManager.h"
#include "ui_RoiEditorDialog.h"
#include "RoiEditorDialog.h"
#include "TCFMetaReader.h"

#include <ROIWriter.h>
#include <ROIReader.h>

#include <iostream>
#include <QMessageBox>

using namespace TC;

namespace CellAnalyzer::Project::View::RoiEditor {
	DataTree::DataTree(QWidget* parent) : QTreeWidget(parent) { }

	void DataTree::mouseDoubleClickEvent(QMouseEvent* event) {
		if (event->button() == Qt::LeftButton && event->modifiers() == Qt::NoModifier) {
			QTreeWidgetItem* item = itemAt(event->pos());
			if (item) {
				// Handle double-click on item here
				// Example: Get the text of the double-clicked item
				if (item->parent()) {
					for (auto i = 0; i < topLevelItemCount(); i++) {
						const auto topLevel = topLevelItem(i);
						topLevel->setBackground(0, QColor(0, 0, 0, 0));
						topLevel->setBackground(1, QColor(0, 0, 0, 0));
						for (auto j = 0; j < topLevel->childCount(); j++) {
							const auto child = topLevel->child(j);
							child->setBackground(0, QColor(0, 0, 0, 0));
							child->setBackground(1, QColor(0, 0, 0, 0));
						}
					}
					QString itemText = item->text(0);
					item->setBackground(0, QColor(220, 0, 50, 128));
					item->parent()->setBackground(0, QColor(220, 0, 50, 128));
					item->setBackground(1, QColor(220, 0, 50, 128));
					item->parent()->setBackground(1, QColor(220, 0, 50, 128));

					emit sigItemDoubleClicked(item, item->parent()->toolTip(0), item->data(0, Qt::UserRole).toInt());
				}
			}
		}

		QTreeWidget::mouseDoubleClickEvent(event);
	}

	auto DataTree::SetForceDoubleClick(QTreeWidgetItem* item) -> void {
		if (item) {
			item->setSelected(true);
			// Handle double-click on item here
			// Example: Get the text of the double-clicked item
			if (item->parent()) {
				for (auto i = 0; i < topLevelItemCount(); i++) {
					const auto topLevel = topLevelItem(i);
					topLevel->setBackground(0, QColor(0, 0, 0, 0));
					topLevel->setBackground(1, QColor(0, 0, 0, 0));
					for (auto j = 0; j < topLevel->childCount(); j++) {
						const auto child = topLevel->child(j);
						child->setBackground(0, QColor(0, 0, 0, 0));
						child->setBackground(1, QColor(0, 0, 0, 0));
					}
				}
				QString itemText = item->text(0);
				item->setBackground(0, QColor(220, 0, 50, 128));
				item->parent()->setBackground(0, QColor(220, 0, 50, 128));
				item->setBackground(1, QColor(220, 0, 50, 128));
				item->parent()->setBackground(1, QColor(220, 0, 50, 128));

				emit sigItemDoubleClicked(item, item->parent()->toolTip(0), item->data(0, Qt::UserRole).toInt());
			}
		}
	}


	struct RangeWidget::Impl {
		QHBoxLayout* layout { nullptr };
		QSpinBox* minSpin { nullptr };
		QLabel* label { nullptr };
		QSpinBox* maxSpin { nullptr };
		int toolIdx { 0 };
		int itemIdx { 0 };
	};

	RangeWidget::RangeWidget(int toolIdx, int itemIdx, QWidget* parent) : QWidget(parent), d { new Impl } {
		d->toolIdx = toolIdx;
		d->itemIdx = itemIdx;

		d->layout = new QHBoxLayout;
		d->minSpin = new QSpinBox;
		d->label = new QLabel("~");
		d->label->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
		d->maxSpin = new QSpinBox;

		d->layout->setContentsMargins(2, 2, 2, 2);
		d->layout->setSpacing(9);
		d->layout->addWidget(d->minSpin);
		d->layout->addWidget(d->label);
		d->layout->addWidget(d->maxSpin);
		d->layout->setStretchFactor(d->minSpin, 1);
		d->layout->setStretchFactor(d->label, 1);
		d->layout->setStretchFactor(d->maxSpin, 1);

		d->minSpin->setRange(1, 9);
		d->maxSpin->setRange(2, 10);

		connect(d->minSpin, SIGNAL(valueChanged(int)), this, SLOT(OnRangeMin(int)));
		connect(d->maxSpin, SIGNAL(valueChanged(int)), this, SLOT(OnRangeMax(int)));

		setLayout(d->layout);
	}

	RangeWidget::~RangeWidget() { }

	auto RangeWidget::SetMax(int max) -> void {
		d->maxSpin->setValue(max);
	}

	auto RangeWidget::SetMin(int min) -> void {
		d->minSpin->setValue(min);
	}

	auto RangeWidget::SetRange(int min, int max) -> void {
		d->minSpin->blockSignals(true);
		d->maxSpin->blockSignals(true);
		d->minSpin->setRange(min, max - 1);
		d->minSpin->setValue(min);
		d->maxSpin->setRange(min + 1, max);
		d->maxSpin->setValue(max);
		d->minSpin->blockSignals(false);
		d->maxSpin->blockSignals(false);
	}

	void RangeWidget::OnRangeMax(int val) {
		if (val <= d->minSpin->value()) {
			d->maxSpin->blockSignals(true);
			d->maxSpin->setValue(val + 1);
			d->maxSpin->blockSignals(false);
			return;
		}
		emit sigRange(d->toolIdx, d->itemIdx, d->minSpin->value(), d->maxSpin->value());
	}

	void RangeWidget::OnRangeMin(int val) {
		if (val >= d->maxSpin->value()) {
			d->minSpin->blockSignals(true);
			d->minSpin->setValue(val - 1);
			d->minSpin->blockSignals(false);
			return;
		}
		emit sigRange(d->toolIdx, d->itemIdx, d->minSpin->value(), d->maxSpin->value());
	}

	struct ROISelectionDialog::Impl {
		Ui::Dialog ui;
		QString AppName;
		std::shared_ptr<QButtonGroup> toolGroup { nullptr };
		std::shared_ptr<ROISceneManager> scenemanager { nullptr };
		std::shared_ptr<ROIToolManager> toolmanager { nullptr };
		std::shared_ptr<ROIRenderWindow> renderwindow { nullptr };
		SoRef<SoSeparator> root { nullptr };
		QList<QTableWidgetItem*> itemLabel[5];
		QList<RangeWidget*> itemZRange[5];
		bool is3D { false };
		bool htExist { false };
		int totalCnt { 0 };
		std::shared_ptr<IO::TCFMetaReader> metaReader { nullptr };
		std::shared_ptr<DataTree> workListTree { nullptr };
		std::shared_ptr<QTableWidget> roiTable { nullptr };
		IO::TCFMetaReader::Meta::Pointer cur_meta { nullptr };
		int curZMax { 1 };
		int curTimePoint { -1 };
		QString curTCFpath;
		QMap<QString, WorksetEditor::Modalities> modalityMap;//Key: TCF
		QMap<QString, QList<int>> timePointMap;// Key: TCF
		QMap<QString, QList<double>> realTimeMap;//Key : TCF
		QSpinBox* chMin[3] { nullptr, };
		QSpinBox* chMax[3] { nullptr, };
		QCheckBox* chChk[3] { nullptr, };
		QTreeWidgetItem* currentTreeItem { nullptr };
		QMap<QString, QMap<int, IO::ROISet>> result;

		QString curRoiRoot;

		auto Init() -> void;
		auto formatTime(int totalSeconds) -> QString;
		auto GetToolIdx(const QString& ID) -> int;
	};

	auto ROISelectionDialog::Impl::GetToolIdx(const QString& ID) -> int {
		int toolIdx = -1;
		if (ID.contains("Square")) {
			toolIdx = 0;
		} else if (ID.contains("Circle")) {
			toolIdx = 1;
		} else if (ID.contains("Ellipse")) {
			toolIdx = 2;
		} else if (ID.contains("Polygon")) {
			toolIdx = 3;
		} else if (ID.contains("Lasso")) {
			toolIdx = 4;
		}
		return toolIdx;
	}

	auto ROISelectionDialog::Impl::formatTime(int totalSeconds) -> QString {
		int hours = totalSeconds / 3600;
		int minutes = (totalSeconds % 3600) / 60;
		int seconds = totalSeconds % 60;

		QString timeString = QString("%1:%2:%3")
							.arg(hours, 2, 10, QLatin1Char('0'))
							.arg(minutes, 2, 10, QLatin1Char('0'))
							.arg(seconds, 2, 10, QLatin1Char('0'));

		return timeString;
	}

	auto ROISelectionDialog::Impl::Init() -> void {
		AppName = qApp->applicationName();
		workListTree = std::make_shared<DataTree>();
		workListTree->setSelectionMode(QAbstractItemView::SingleSelection);

		roiTable = std::make_shared<QTableWidget>();

		scenemanager = std::make_shared<ROISceneManager>();
		toolmanager = std::make_shared<ROIToolManager>();
		renderwindow = std::make_shared<ROIRenderWindow>(nullptr);
		metaReader = std::make_shared<IO::TCFMetaReader>();
		workListTree->setColumnCount(1);
		workListTree->setHeaderLabels({ "WorkList" });
		workListTree->header()->setSectionResizeMode(0, QHeaderView::Stretch);
		workListTree->header()->setMinimumSectionSize(100);
		ui.treeSocket->layout()->addWidget(workListTree.get());

		roiTable->setColumnCount(2);
		roiTable->setHorizontalHeaderLabels({ "ROI", "Z Range" });
		roiTable->verticalHeader()->setVisible(true);
		roiTable->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
		roiTable->setSelectionMode(QAbstractItemView::SingleSelection);
		roiTable->setSelectionBehavior(QAbstractItemView::SelectRows);
		roiTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
		roiTable->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
		ui.roiTableSocket->layout()->addWidget(roiTable.get());

		chMin[0] = ui.ch1MinSpin;
		chMin[1] = ui.ch2MinSpin;
		chMin[2] = ui.ch3MinSpin;
		chMax[0] = ui.ch1MaxSpin;
		chMax[1] = ui.ch2MaxSpin;
		chMax[2] = ui.ch3MaxSpin;
		chChk[0] = ui.ch1Chk;
		chChk[1] = ui.ch2Chk;
		chChk[2] = ui.ch3Chk;
		auto rlayout = new QVBoxLayout;
		rlayout->setContentsMargins(0, 0, 0, 0);
		rlayout->setSpacing(0);
		ui.renderSocket->setLayout(rlayout);
		rlayout->addWidget(renderwindow.get());

		//Init UI
		toolGroup = std::make_shared<QButtonGroup>();
		toolGroup->addButton(ui.squareBtn, 0);
		toolGroup->addButton(ui.circleBtn, 1);
		toolGroup->addButton(ui.ellipseBtn, 2);
		toolGroup->addButton(ui.polygonBtn, 3);
		toolGroup->addButton(ui.lassoBtn, 4);
		toolGroup->setExclusive(false);

		ui.squareBtn->setIcon(QIcon(":/image/images/Square.svg"));
		ui.circleBtn->setIcon(QIcon(":/image/images/Circle.svg"));
		ui.ellipseBtn->setIcon(QIcon(":/image/images/Ellipse.svg"));
		ui.polygonBtn->setIcon(QIcon(":/image/images/Polygon.svg"));
		ui.squareBtn->setIcon(QIcon(":/image/images/Square.svg"));
		ui.lassoBtn->setIcon(QIcon(":/Widget/Lasso.svg"));
		ui.removeBtn->setIcon(QIcon(":/image/images/Remove.svg"));
		ui.exportBtn->setIcon(QIcon(":/Flat/Export.svg"));
		ui.exportBtn->setIconSize(QSize(25, 25));
		ui.importBtn->setIcon(QIcon(":/Flat/Import.svg"));
		ui.importBtn->setIconSize(QSize(25, 25));

		ui.htMinSpin->setDecimals(4);
		ui.htMinSpin->setSingleStep(0.0001);
		ui.htMinSpin->setEnabled(false);

		ui.htMaxSpin->setDecimals(4);
		ui.htMaxSpin->setSingleStep(0.0001);
		ui.htMaxSpin->setEnabled(false);

		ui.htChk->setEnabled(false);
		for (auto i = 0; i < 3; i++) {
			chMin[i]->setEnabled(false);
			chMax[i]->setEnabled(false);
			chChk[i]->setEnabled(false);
		}

		ui.zSpin->setEnabled(false);
		ui.zSlider->setEnabled(false);
		root = new SoSeparator;
		root->addChild(scenemanager->GetSceneGraph());
		root->addChild(toolmanager->GetRootSwitch());
		renderwindow->setSceneGraph(root.ptr());
	}

	ROISelectionDialog::ROISelectionDialog(QWidget* parent) : QDialog(parent), IView(), d { new Impl } {
		d->ui.setupUi(this);
		installEventFilter(this);
		setWindowTitle("ROI Editor");
		d->Init();
		InitConnections();
		d->toolmanager->SetHandleSize(1);
	}

	ROISelectionDialog::~ROISelectionDialog() { }

	auto ROISelectionDialog::SetCurRoiRoot(const QString& path) -> void {
		d->curRoiRoot = path;
	}

	auto ROISelectionDialog::GetOutput() -> QMap<QString, QMap<int, IO::ROISet>> {
		return d->result;
	}

	auto ROISelectionDialog::SetInput(inputStream input) -> void {
		//count total item # first
		d->result.clear();
		d->modalityMap.clear();
		d->timePointMap.clear();
		d->realTimeMap.clear();
		d->roiTable->clearContents();
		d->roiTable->setRowCount(0);
		d->workListTree->clear();
		d->currentTreeItem = nullptr;
		d->totalCnt = 0;
		d->is3D = false;
		d->htExist = false;
		d->cur_meta = nullptr;
		d->curZMax = 1;
		d->curTimePoint = -1;
		QString curTCFpath;
		for (auto i = 0; i < 5; i++) {
			d->itemLabel[i].clear();
			d->itemZRange[i].clear();
		}
		d->toolmanager->DeleteAll();
		for (const auto in : input) {
			WorksetEditor::Modalities mod = WorksetEditor::Modality::None;
			for (auto i = 0; i < in.timeSelection.modalities.count(); i++) {
				mod |= in.timeSelection.modalities[i];
			}
			QList<int> timeList;
			for (auto i = 0; i < in.timeSelection.times.count(); i++) {
				const auto time = in.timeSelection.times[i];
				timeList.append(time);
			}
			d->modalityMap[in.tcfPath] = mod;
			d->timePointMap[in.tcfPath] = timeList;
		}
		for (const auto in : input) {
			const auto tcfPath = in.tcfPath;
			QFileInfo info(tcfPath);
			const auto baseName = info.fileName().chopped(4);
			const auto tcfNode = new QTreeWidgetItem(d->workListTree.get(), { baseName });
			tcfNode->setToolTip(0, tcfPath);
			QPixmap pixmap(25, 25);
			pixmap.fill(Qt::green);
			for (auto i = 0; i < in.timeSelection.times.count(); i++) {
				const auto time = in.timeSelection.times[i];
				const auto seconds = static_cast<int>(time);
				const auto childNode = new QTreeWidgetItem(tcfNode, { d->formatTime(seconds) });
				childNode->setData(0, Qt::UserRole, QString::number(seconds));

				const auto roiFilePath = QDir(d->curRoiRoot).filePath(QString("%1_%2.tcroi").arg(baseName).arg(seconds));
				if (QFileInfo::exists(roiFilePath)) {
					const auto reader = std::make_shared<IO::ROIReader>(roiFilePath);
					const auto roiSet = reader->Read();
					if (roiSet.GetROIs().count() < 1) {
						continue;
					}
					if (false == d->result.contains(tcfPath)) {
						d->result[tcfPath] = QMap<int, IO::ROISet>();
					}
					d->result[tcfPath][seconds] = roiSet;
					childNode->setIcon(0, QIcon(":/image/images/ic-check-s.svg"));
				}
			}
		}
		d->workListTree->expandAll();
	}

	auto ROISelectionDialog::SetApplicationName(const QString& name) -> void {
		d->AppName = name;
	}

	bool ROISelectionDialog::eventFilter(QObject* watched, QEvent* event) {
		if (event->type() == QEvent::KeyPress) {
			if (const QKeyEvent* keyEvent = dynamic_cast<QKeyEvent*>(event); keyEvent->key() == Qt::Key_Return || keyEvent->key() == Qt::Key_Enter) {
				return true;
			}
		}
		return QObject::eventFilter(watched, event);
	}

	auto ROISelectionDialog::InitConnections() -> void {
		connect(d->toolGroup.get(), SIGNAL(buttonToggled(int, bool)), this, SLOT(OnToolSelected(int, bool)));
		connect(d->renderwindow.get(), SIGNAL(sigMouseWheel(int)), this, SLOT(OnMouseWheel(int)));
		connect(d->ui.okBtn, SIGNAL(pressed()), this, SLOT(OnOkBtn()));
		connect(d->ui.nextBtn, SIGNAL(pressed()), this, SLOT(OnNextBtn()));
		connect(d->ui.cancelBtn, SIGNAL(pressed()), this, SLOT(OnCancelBtn()));
		connect(d->ui.clearBtn, SIGNAL(pressed()), this, SLOT(OnClearAllBtn()));
		connect(d->ui.applyBtn, SIGNAL(pressed()), this, SLOT(OnApplyBtn()));
		connect(d->ui.importBtn, SIGNAL(pressed()), this, SLOT(OnImportBtn()));
		connect(d->ui.exportBtn, SIGNAL(pressed()), this, SLOT(OnExportBtn()));
		connect(d->roiTable.get(), SIGNAL(cellClicked(int, int)), this, SLOT(OnROISelected(int, int)));
		connect(d->toolmanager.get(), SIGNAL(sigRoiAdded(int, int)), this, SLOT(OnItemAdded(int, int)));
		connect(d->toolmanager.get(), SIGNAL(sigRoiSelected(int, int)), this, SLOT(OnItemSelected(int, int)));
		connect(d->workListTree.get(), SIGNAL(sigItemDoubleClicked(QTreeWidgetItem*, QString, int)), this, SLOT(OnDataSetSelected(QTreeWidgetItem*, QString, int)));
		connect(d->ui.removeBtn, SIGNAL(clicked()), this, SLOT(OnRemove()));

		connect(d->ui.htMinSpin, SIGNAL(valueChanged(double)), this, SLOT(OnHtChanged(double)));
		connect(d->ui.htMaxSpin, SIGNAL(valueChanged(double)), this, SLOT(OnHtChanged(double)));

		connect(d->ui.ch1MinSpin, SIGNAL(valueChanged(int)), this, SLOT(OnCh1Changed(int)));
		connect(d->ui.ch1MaxSpin, SIGNAL(valueChanged(int)), this, SLOT(OnCh1Changed(int)));
		connect(d->ui.ch2MinSpin, SIGNAL(valueChanged(int)), this, SLOT(OnCh2Changed(int)));
		connect(d->ui.ch2MaxSpin, SIGNAL(valueChanged(int)), this, SLOT(OnCh2Changed(int)));
		connect(d->ui.ch3MinSpin, SIGNAL(valueChanged(int)), this, SLOT(OnCh3Changed(int)));
		connect(d->ui.ch3MaxSpin, SIGNAL(valueChanged(int)), this, SLOT(OnCh3Changed(int)));

		connect(d->ui.zSpin, SIGNAL(valueChanged(int)), this, SLOT(OnZSpinChanged(int)));
		connect(d->ui.zSlider, SIGNAL(valueChanged(int)), this, SLOT(OnZSliderChanged(int)));

		connect(d->ui.htChk, SIGNAL(clicked()), this, SLOT(OnHtChecked()));
		connect(d->ui.ch1Chk, SIGNAL(clicked()), this, SLOT(OnCh1Checked()));
		connect(d->ui.ch2Chk, SIGNAL(clicked()), this, SLOT(OnCh2Checked()));
		connect(d->ui.ch3Chk, SIGNAL(clicked()), this, SLOT(OnCh3Checked()));
	}

	//SLOTS

	void ROISelectionDialog::OnRemove() {
		const auto selected = d->roiTable->currentItem();
		if (nullptr == selected) {
			return;
		}
		const auto text = selected->text();
		const auto ID = text.split("_")[0];
		auto toolIdx = d->GetToolIdx(ID);
		if (toolIdx < 0) {
			return;
		}

		const auto itemIdx = text.split("_")[1].toInt();
		d->itemLabel[toolIdx].removeOne(selected);
		for (auto i = 0; i < d->itemLabel[toolIdx].count(); i++) {
			d->itemLabel[toolIdx][i]->setText(QString("%1_%2").arg(ID).arg(i));
		}
		d->roiTable->removeRow(selected->row());
		d->toolmanager->DeleteItem(itemIdx);
		d->totalCnt--;
	}

	void ROISelectionDialog::OnHtChecked() {
		d->scenemanager->ToggleHT(d->ui.htChk->isChecked());
	}

	void ROISelectionDialog::OnCh1Checked() {
		d->scenemanager->ToggleFL(0, d->ui.ch1Chk->isChecked());
	}

	void ROISelectionDialog::OnCh2Checked() {
		d->scenemanager->ToggleFL(1, d->ui.ch2Chk->isChecked());
	}

	void ROISelectionDialog::OnCh3Checked() {
		d->scenemanager->ToggleFL(2, d->ui.ch3Chk->isChecked());
	}

	void ROISelectionDialog::OnZSpinChanged(int val) {
		if (false == d->is3D) {
			return;
		}
		d->ui.zSlider->blockSignals(true);
		d->ui.zSlider->setValue(val);
		d->ui.zSlider->blockSignals(false);

		d->toolmanager->SetCurrentZ(val);
		d->scenemanager->SetSliceNumber(val, d->htExist);
	}

	void ROISelectionDialog::OnZSliderChanged(int val) {
		if (false == d->is3D) {
			return;
		}
		d->ui.zSpin->blockSignals(true);
		d->ui.zSpin->setValue(val);
		d->ui.zSpin->blockSignals(false);

		d->toolmanager->SetCurrentZ(val);
		d->scenemanager->SetSliceNumber(val, d->htExist);
	}

	void ROISelectionDialog::OnMouseWheel(int delta) {
		if (false == d->is3D) {
			return;
		}
		auto curIdx = d->ui.zSpin->value();
		const auto modifiedZ = curIdx + delta;
		if (modifiedZ < 1 || modifiedZ > d->curZMax) {
			return;
		}
		d->ui.zSpin->setValue(modifiedZ);
	}

	void ROISelectionDialog::OnHtChanged(double) {
		const auto min = d->ui.htMinSpin->value();
		const auto max = d->ui.htMaxSpin->value();
		d->scenemanager->SetHTRange(min, max);
	}

	void ROISelectionDialog::OnCh1Changed(int) {
		const auto min = d->ui.ch1MinSpin->value();
		const auto max = d->ui.ch1MaxSpin->value();
		d->scenemanager->SetFLRange(0, min, max);
	}

	void ROISelectionDialog::OnCh2Changed(int) {
		const auto min = d->ui.ch2MinSpin->value();
		const auto max = d->ui.ch2MaxSpin->value();
		d->scenemanager->SetFLRange(1, min, max);
	}

	void ROISelectionDialog::OnCh3Changed(int) {
		const auto min = d->ui.ch3MinSpin->value();
		const auto max = d->ui.ch3MaxSpin->value();
		d->scenemanager->SetFLRange(2, min, max);
	}

	void ROISelectionDialog::OnROISelected(int row, int column) {
		if (d->roiTable->rowCount() <= row) {
			return;
		}
		const auto itemName = d->roiTable->item(row, 0)->text();
		int toolIdx = d->GetToolIdx(itemName);

		if (toolIdx < 0) {
			return;
		}

		const auto itemIdx = itemName.split("_")[1].toInt();
		OnToolSelected(toolIdx, true);
		d->toolmanager->HighlightItem(itemIdx);
	}

	void ROISelectionDialog::OnExportBtn() {
		//Export ROI as *.tcroi file
		if (d->curTCFpath.isEmpty()) {
			return;
		}
		if (false == d->result.contains(d->curTCFpath)) {
			return;
		}
		if (d->curTimePoint < 0) {
			return;
		}
		if (false == d->result[d->curTCFpath].contains(d->curTimePoint)) {
			return;
		}
		const auto roiSet = d->result[d->curTCFpath][d->curTimePoint];
		const auto prev = QSettings("Tomocube", d->AppName).value("recent/roi", QApplication::applicationDirPath()).toString();
		const auto path = QFileDialog::getSaveFileName(nullptr, tr("Save ROI to File"), prev + "/", tr("Tomocube ROI file (*.tcroi)"));
		if (path.isEmpty()) {
			return;
		}
		QSettings("Tomocube", d->AppName).setValue("recent/roi", path);
		const auto writer = std::make_shared<IO::ROIWriter>(path);
		if (false == writer->Write(roiSet)) {
			std::cout << "Failed to Write ROI" << std::endl;
		}
	}

	void ROISelectionDialog::OnImportBtn() {
		//Import ROI from *.tcroi file
		if (d->curTCFpath.isEmpty()) {
			return;
		}
		if (d->curTimePoint < 0) {
			return;
		}
		const auto prev = QSettings("Tomocube", d->AppName).value("recent/roi", QApplication::applicationDirPath()).toString();
		const auto path = QFileDialog::getOpenFileName(nullptr, tr("Import ROI from File"), prev + "/", tr("Tomocube ROI file (*.tcroi)"));
		if (path.isEmpty()) {
			return;
		}
		QSettings("Tomocube", d->AppName).setValue("recent/roi", path);
		const auto reader = std::make_shared<IO::ROIReader>(path);
		const auto roiSet = reader->Read();
		if (roiSet.GetROIs().count() < 1) {
			return;
		}
		const auto [x, y, z] = roiSet.GetImageSize();
		auto sameDimension = true;
		int imageDim[3] = { 1, 1, 1 };
		if (d->is3D) {
			if (d->htExist) {
				if (x != d->cur_meta->data.data3D.sizeX || y != d->cur_meta->data.data3D.sizeY || z != d->cur_meta->data.data3D.sizeZ) {
					sameDimension = false;
					imageDim[0] = d->cur_meta->data.data3D.sizeX;
					imageDim[1] = d->cur_meta->data.data3D.sizeY;
					imageDim[2] = d->cur_meta->data.data3D.sizeZ;
				}
			} else {
				if (x != d->cur_meta->data.data3DFL.sizeX || y != d->cur_meta->data.data3DFL.sizeY || z != d->cur_meta->data.data3DFL.sizeZ) {
					sameDimension = false;
					imageDim[0] = d->cur_meta->data.data3DFL.sizeX;
					imageDim[1] = d->cur_meta->data.data3DFL.sizeY;
					imageDim[2] = d->cur_meta->data.data3DFL.sizeZ;
				}
			}
		} else {
			if (d->htExist) {
				if (x != d->cur_meta->data.data2DMIP.sizeX || y != d->cur_meta->data.data2DMIP.sizeY) {
					sameDimension = false;
					imageDim[0] = d->cur_meta->data.data2DMIP.sizeX;
					imageDim[1] = d->cur_meta->data.data2DMIP.sizeY;
				}
			} else {
				if (x != d->cur_meta->data.data2DFLMIP.sizeX || y != d->cur_meta->data.data2DFLMIP.sizeY) {
					sameDimension = false;
					imageDim[0] = d->cur_meta->data.data2DFLMIP.sizeX;
					imageDim[1] = d->cur_meta->data.data2DFLMIP.sizeY;
				}
			}
		}

		if (false == sameDimension) {
			QMessageBox::warning(nullptr, "Warning", QString("ROI Dimension Mismatch ! \nImage: [%1,%2,%3]\nROI: [%4,%5,%6]").arg(imageDim[0]).arg(imageDim[1]).arg(imageDim[2]).arg(x).arg(y).arg(z));
			return;
		}

		if (false == d->result.contains(d->curTCFpath)) {
			d->result[d->curTCFpath] = QMap<int, IO::ROISet>();
		}
		d->result[d->curTCFpath][d->curTimePoint] = roiSet;

		const auto prevROI = d->result[d->curTCFpath][d->curTimePoint];
		for (auto i = 0; i < prevROI.GetROIs().count(); i++) {
			const auto roi = prevROI.GetROIs()[i];
			const auto toolIdx = d->GetToolIdx(roi.GetID());
			d->toolmanager->AddROI(toolIdx, roi);

			const auto zControl = d->itemZRange[toolIdx].last();
			const auto [min, max] = roi.GetZRange();
			zControl->blockSignals(true);
			zControl->SetRange(1, d->curZMax);
			zControl->SetMin(min);
			zControl->SetMax(max);
			zControl->blockSignals(false);
		}
	}

	void ROISelectionDialog::OnClearAllBtn() {
		//if current item is selected clear it first
		if (d->currentTreeItem) {
			d->roiTable->clearContents();
			d->roiTable->setRowCount(0);
			d->totalCnt = 0;
			d->toolmanager->DeleteAll();
			for (auto i = 0; i < 5; i++) {
				d->itemLabel[i].clear();
				d->itemZRange[i].clear();
			}
		}
		//clear all ROIs
		d->result.clear();

		for (auto i = 0; i < d->workListTree->topLevelItemCount(); i++) {
			const auto topLevelItem = d->workListTree->topLevelItem(i);
			for (auto j = 0; j < topLevelItem->childCount(); j++) {
				const auto item = topLevelItem->child(j);
				if (item) {
					item->setIcon(0, QIcon());
				}
			}
		}
	}

	void ROISelectionDialog::OnApplyBtn() {
		if (nullptr == d->currentTreeItem) {
			return;
		}
		auto total_roi_cnt = 0;
		for (auto i = 0; i < 5; i++) {
			const auto infos = d->toolmanager->GetROIInfo(i);
			total_roi_cnt += infos.count();
		}
		if (total_roi_cnt < 1) {
			d->currentTreeItem->setIcon(0, QIcon());
			if (d->result.contains(d->curTCFpath)) {
				d->result[d->curTCFpath].remove(d->curTimePoint);
			}
			return;
		}
		d->currentTreeItem->setIcon(0, QIcon(":/image/images/ic-check-s.svg"));
		//Save ROI info in memory		
		IO::ROISet roiset;
		roiset.SetImage(d->curTCFpath);
		if (d->is3D) {
			if (d->htExist) {
				roiset.SetImageSize(d->cur_meta->data.data3D.sizeX, d->cur_meta->data.data3D.sizeY, d->cur_meta->data.data3D.sizeZ);
				roiset.SetImageResolution(d->cur_meta->data.data3D.resolutionX, d->cur_meta->data.data3D.resolutionY, d->cur_meta->data.data3D.resolutionZ);
			} else {
				roiset.SetImageSize(d->cur_meta->data.data3DFL.sizeX, d->cur_meta->data.data3DFL.sizeY, d->cur_meta->data.data3DFL.sizeZ);
				roiset.SetImageResolution(d->cur_meta->data.data3DFL.resolutionX, d->cur_meta->data.data3DFL.resolutionY, d->cur_meta->data.data3DFL.resolutionZ);
			}
		} else {
			if (d->htExist) {
				roiset.SetImageSize(d->cur_meta->data.data2DMIP.sizeX, d->cur_meta->data.data2DMIP.sizeY, 1);
				roiset.SetImageResolution(d->cur_meta->data.data2DMIP.resolutionX, d->cur_meta->data.data2DMIP.resolutionY, 1);
			} else {
				roiset.SetImageSize(d->cur_meta->data.data2DFLMIP.sizeX, d->cur_meta->data.data2DFLMIP.sizeY, 1);
				roiset.SetImageResolution(d->cur_meta->data.data2DFLMIP.resolutionX, d->cur_meta->data.data2DFLMIP.resolutionY, 1);
			}
		}
		for (auto i = 0; i < 5; i++) {
			const auto infos = d->toolmanager->GetROIInfo(i);
			for (const auto info : infos) {
				roiset.AddROI(info);//No Bounding Box Info
			}
		}
		if (false == d->result.contains(d->curTCFpath)) {
			d->result[d->curTCFpath] = QMap<int, IO::ROISet>();
		}
		d->result[d->curTCFpath][d->curTimePoint] = roiset;
	}

	void ROISelectionDialog::OnOkBtn() {
		//accept dialog
		accept();
	}

	void ROISelectionDialog::OnCancelBtn() {
		//reject dialog
		reject();
	}

	void ROISelectionDialog::OnNextBtn() {
		//Move to Next Image
		if (nullptr == d->currentTreeItem) {
			return;
		}
		const auto parent = d->currentTreeItem->parent();
		const auto parentIdx = d->workListTree->indexOfTopLevelItem(parent);
		const auto parentCnt = d->workListTree->topLevelItemCount();
		const auto childIdx = parent->indexOfChild(d->currentTreeItem);
		const auto childCount = parent->childCount();

		if (childIdx + 1 < childCount) {
			d->workListTree->blockSignals(true);
			d->workListTree->clearSelection();
			d->workListTree->blockSignals(false);
			const auto nextItem = parent->child(childIdx + 1);
			d->workListTree->SetForceDoubleClick(nextItem);
			return;
		}
		if (parentIdx + 1 < parentCnt) {
			const auto nextParent = d->workListTree->topLevelItem(parentIdx + 1);
			if (nextParent->childCount() < 1) {
				return;
			}
			d->workListTree->blockSignals(true);
			d->workListTree->clearSelection();
			d->workListTree->blockSignals(false);
			const auto nextItem = nextParent->child(0);
			d->workListTree->SetForceDoubleClick(nextItem);
		}
	}

	void ROISelectionDialog::OnItemAdded(int toolIdx, int itemIdx) {
		if (d->curTCFpath.isEmpty()) {
			return;
		}
		if (nullptr == d->cur_meta) {
			return;
		}
		QString toolName;
		if (toolIdx == SQUARE) {
			toolName = "Square";
		} else if (toolIdx == CIRCLE) {
			toolName = "Circle";
		} else if (toolIdx == ELLIPSE) {
			toolName = "Ellipse";
		} else if (toolIdx == POLYGON) {
			toolName = "Polygon";
		} else if (toolIdx == LASSO) {
			toolName = "Lasso";
		}
		const auto labelItem = new QTableWidgetItem;
		labelItem->setText(QString("%1_%2").arg(toolName).arg(itemIdx));
		labelItem->setFlags(labelItem->flags() ^ Qt::ItemIsEditable);
		d->itemLabel[toolIdx].append(labelItem);

		const auto labelSpin = new RangeWidget(toolIdx, itemIdx);
		labelSpin->SetRange(1, d->curZMax);
		d->itemZRange[toolIdx].append(labelSpin);

		d->roiTable->setRowCount(++d->totalCnt);
		d->roiTable->setItem(d->totalCnt - 1, 0, labelItem);
		d->roiTable->setCellWidget(d->totalCnt - 1, 1, labelSpin);

		connect(labelSpin, SIGNAL(sigRange(int, int, int, int)), this, SLOT(OnZRangeChanged(int, int, int, int)));
	}

	void ROISelectionDialog::OnZRangeChanged(int toolIdx, int itemIdx, int zmin, int zmax) {
		d->toolmanager->SetItemZRange(toolIdx, itemIdx, zmin, zmax);
	}

	void ROISelectionDialog::OnDataSetSelected(QTreeWidgetItem* item, QString tcfPath, int timePoint) {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.00001;
		};
		if (false == d->modalityMap.contains(tcfPath)) {
			std::cout << "There is no modality map for " << tcfPath.toStdString() << std::endl;
		}
		if (false == d->timePointMap.contains(tcfPath)) {
			std::cout << "There is no timepoint map for " << tcfPath.toStdString() << std::endl;
		}
		d->currentTreeItem = item;
		bool tcfChanged = false;
		if (d->curTCFpath != tcfPath) {
			d->scenemanager->Clear();
			d->curTCFpath = tcfPath;
			d->cur_meta = d->metaReader->Read(tcfPath);
			d->scenemanager->SetTCFPath(tcfPath);
			d->scenemanager->SetMeta(d->cur_meta);
			d->ui.htMinSpin->setEnabled(false);
			d->ui.htMaxSpin->setEnabled(false);
			for (auto i = 0; i < 3; i++) {
				d->chMin[i]->setEnabled(false);
				d->chMax[i]->setEnabled(false);
				d->chChk[i]->setEnabled(false);
			}
			d->curZMax = 1;
			d->htExist = false;
			d->is3D = false;
			d->ui.zSpin->setEnabled(false);
			d->ui.zSlider->setEnabled(false);
			d->ui.htChk->setEnabled(false);
			tcfChanged = true;
		} else if (d->curTimePoint == timePoint) {
			return;
		}
		d->roiTable->clearContents();
		d->roiTable->setRowCount(0);
		d->totalCnt = 0;
		d->toolmanager->DeleteAll();
		for (auto i = 0; i < 5; i++) {
			d->itemLabel[i].clear();
			d->itemZRange[i].clear();
		}

		QString curTCFpath;
		d->curTimePoint = timePoint;
		d->scenemanager->ClearScene();

		const auto modality = d->modalityMap[tcfPath];

		auto is3D { true };
		if (modality.testFlag(WorksetEditor::Modality::Ht2d)) {
			is3D = false;
			int idx = -1;
			const auto tps = d->cur_meta->data.data2DMIP.timePoints;
			for (auto i = 0; i < tps.count(); i++) {
				if (AreSame(timePoint, tps[i])) {
					idx = i;
					break;
				}
			}
			if (idx < 0) {
				return;
			}
			d->ui.htMinSpin->setEnabled(true);
			d->ui.htMaxSpin->setEnabled(true);

			d->ui.htMinSpin->blockSignals(true);
			d->ui.htMinSpin->setRange(d->cur_meta->data.data2DMIP.riMinList[idx], d->cur_meta->data.data2DMIP.riMaxList[idx]);
			d->ui.htMinSpin->setValue(d->cur_meta->data.data2DMIP.riMinList[idx]);
			d->ui.htMinSpin->blockSignals(false);

			d->ui.htMaxSpin->blockSignals(true);
			d->ui.htMaxSpin->setRange(d->cur_meta->data.data2DMIP.riMinList[idx], d->cur_meta->data.data2DMIP.riMaxList[idx]);
			d->ui.htMaxSpin->setValue(d->cur_meta->data.data2DMIP.riMaxList[idx]);
			d->ui.htMaxSpin->blockSignals(false);

			d->htExist = true;
			d->ui.htChk->blockSignals(true);
			d->ui.htChk->setChecked(true);
			d->ui.htChk->blockSignals(false);
			d->ui.htChk->setEnabled(true);
			d->scenemanager->ReadHT2D(idx);
		}
		bool readFL2D[3] { false, false, false };
		if (modality.testFlag(WorksetEditor::Modality::Fl2dCh1)) {
			is3D = false;
			readFL2D[0] = true;
		}
		if (modality.testFlag(WorksetEditor::Modality::Fl2dCh2)) {
			is3D = false;
			readFL2D[1] = true;
		}
		if (modality.testFlag(WorksetEditor::Modality::Fl2dCh3)) {
			is3D = false;
			readFL2D[2] = true;
		}
		for (auto ch = 0; ch < 3; ch++) {
			if (readFL2D[ch]) {
				int idx = -1;
				const auto tps = d->cur_meta->data.data2DFLMIP.timePoints[0];
				for (auto i = 0; i < tps.count(); i++) {
					if (AreSame(timePoint, tps[i])) {
						idx = i;
						break;
					}
				}
				if (idx < 0) {
					continue;
				}
				d->chMin[ch]->setEnabled(true);
				d->chMax[ch]->setEnabled(true);

				d->chMin[ch]->blockSignals(true);
				d->chMin[ch]->setRange(d->cur_meta->data.data2DFLMIP.minIntensityList[ch][idx], d->cur_meta->data.data2DFLMIP.maxIntensityList[ch][idx]);
				d->chMin[ch]->setValue(d->cur_meta->data.data2DFLMIP.minIntensityList[ch][idx]);
				d->chMin[ch]->blockSignals(false);

				d->chMax[ch]->blockSignals(true);
				d->chMax[ch]->setRange(d->cur_meta->data.data2DFLMIP.minIntensityList[ch][idx], d->cur_meta->data.data2DFLMIP.maxIntensityList[ch][idx]);
				d->chMax[ch]->setValue(d->cur_meta->data.data2DFLMIP.maxIntensityList[ch][idx]);
				d->chMax[ch]->blockSignals(false);

				d->chChk[ch]->blockSignals(true);
				d->chChk[ch]->setChecked(true);
				d->chChk[ch]->blockSignals(false);
				d->chChk[ch]->setEnabled(true);
				d->scenemanager->ReadFL2D(ch, idx);
			}
		}
		auto ht3DExist = false;
		if (modality.testFlag(WorksetEditor::Modality::Ht3d)) {
			int idx = -1;
			ht3DExist = true;
			const auto tps = d->cur_meta->data.data3D.timePoints;
			for (auto i = 0; i < tps.count(); i++) {
				if (AreSame(timePoint, tps[i])) {
					idx = i;
					break;
				}
			}
			if (idx < 0) {
				return;
			}

			d->ui.htMinSpin->setEnabled(true);
			d->ui.htMaxSpin->setEnabled(true);

			d->ui.htMinSpin->blockSignals(true);
			d->ui.htMinSpin->setRange(d->cur_meta->data.data3D.riMinList[idx], d->cur_meta->data.data3D.riMaxList[idx]);
			d->ui.htMinSpin->setValue(d->cur_meta->data.data3D.riMinList[idx]);
			d->ui.htMinSpin->blockSignals(false);

			d->ui.htMaxSpin->blockSignals(true);
			d->ui.htMaxSpin->setRange(d->cur_meta->data.data3D.riMinList[idx], d->cur_meta->data.data3D.riMaxList[idx]);
			d->ui.htMaxSpin->setValue(d->cur_meta->data.data3D.riMaxList[idx]);
			d->ui.htMaxSpin->blockSignals(false);

			d->curZMax = d->cur_meta->data.data3D.sizeZ;
			d->htExist = true;
			d->ui.htChk->blockSignals(true);
			d->ui.htChk->setChecked(true);
			d->ui.htChk->blockSignals(false);
			d->ui.htChk->setEnabled(true);
			d->scenemanager->ReadHT3D(idx);
		}
		bool readFL3D[3] { false, false, false };
		if (modality.testFlag(WorksetEditor::Modality::Fl3dCh1)) {
			readFL3D[0] = true;
		}
		if (modality.testFlag(WorksetEditor::Modality::Fl3dCh2)) {
			readFL3D[1] = true;
		}
		if (modality.testFlag(WorksetEditor::Modality::Fl3dCh3)) {
			readFL3D[2] = true;
		}
		for (auto ch = 0; ch < 3; ch++) {
			if (readFL3D[ch]) {
				int idx = -1;
				const auto tps = d->cur_meta->data.data3DFL.timePoints[ch];
				for (auto i = 0; i < tps.count(); i++) {
					if (AreSame(timePoint, tps[i])) {
						idx = i;
						break;
					}
				}
				if (idx < 0) {
					continue;
				}
				d->chMin[ch]->setEnabled(true);
				d->chMax[ch]->setEnabled(true);

				d->chMin[ch]->blockSignals(true);
				d->chMin[ch]->setRange(d->cur_meta->data.data3DFL.minIntensityList[ch][idx], d->cur_meta->data.data3DFL.maxIntensityList[ch][idx]);
				d->chMin[ch]->setValue(d->cur_meta->data.data3DFL.minIntensityList[ch][idx]);
				d->chMin[ch]->blockSignals(false);

				d->chMax[ch]->blockSignals(true);
				d->chMax[ch]->setRange(d->cur_meta->data.data3DFL.minIntensityList[ch][idx], d->cur_meta->data.data3DFL.maxIntensityList[ch][idx]);
				d->chMax[ch]->setValue(d->cur_meta->data.data3DFL.maxIntensityList[ch][idx]);
				d->chMax[ch]->blockSignals(false);

				if (false == ht3DExist) {
					d->curZMax = d->cur_meta->data.data3DFL.sizeZ;
				}

				d->chChk[ch]->blockSignals(true);
				d->chChk[ch]->setChecked(true);
				d->chChk[ch]->blockSignals(false);
				d->chChk[ch]->setEnabled(true);

				d->scenemanager->ReadFL3D(ch, idx);
			}
		}
		d->is3D = is3D;
		if (is3D) {
			d->ui.zSpin->blockSignals(true);
			d->ui.zSlider->blockSignals(true);
			if (tcfChanged) {
				d->ui.zSpin->setRange(1, d->curZMax);
				d->ui.zSlider->setRange(1, d->curZMax);
				d->ui.zSlider->setEnabled(true);
				d->ui.zSpin->setEnabled(true);
				d->toolmanager->SetGlobalZMinMax(1, d->curZMax);
			}
			d->ui.zSpin->setValue(d->curZMax / 2);
			d->ui.zSlider->setValue(d->curZMax / 2);
			d->toolmanager->SetCurrentZ(d->curZMax / 2);
			d->ui.zSlider->blockSignals(false);
			d->ui.zSpin->blockSignals(false);
		} else {
			d->toolmanager->SetGlobalZMinMax(1, 1);
			d->toolmanager->SetCurrentZ(1);
		}
		d->renderwindow->ResetView2D();

		//load existing ROIs
		if (d->result[d->curTCFpath].contains(d->curTimePoint)) {
			const auto prevROI = d->result[d->curTCFpath][d->curTimePoint];
			for (auto i = 0; i < prevROI.GetROIs().count(); i++) {
				const auto roi = prevROI.GetROIs()[i];
				const auto toolIdx = d->GetToolIdx(roi.GetID());
				d->toolmanager->AddROI(toolIdx, roi);
			}
			int toolNum[5] { 0, 0, 0, 0, 0 };
			for (auto i = 0; i < prevROI.GetROIs().count(); i++) {
				const auto roi = prevROI.GetROIs()[i];
				const auto [zmin, zmax] = roi.GetZRange();
				const auto toolIdx = d->GetToolIdx(roi.GetID());
				if (d->itemZRange[toolIdx].count() > toolNum[toolIdx]) {
					const auto& widget = d->itemZRange[toolIdx][toolNum[toolIdx]];
					widget->SetMin(zmin);
					widget->SetMax(zmax);
					toolNum[toolIdx]++;
				}
			}
		}
	}

	void ROISelectionDialog::OnItemSelected(int toolIdx, int itemIdx) {
		if (d->curTCFpath.isEmpty()) {
			return;
		}
		if (nullptr == d->cur_meta) {
			return;
		}
		if (d->itemLabel[toolIdx].count() <= itemIdx) {
			return;
		}
		d->roiTable->blockSignals(true);
		const auto row = d->roiTable->row(d->itemLabel[toolIdx][itemIdx]);
		d->roiTable->selectRow(row);
		d->roiTable->blockSignals(false);
	}

	void ROISelectionDialog::OnToolSelected(int toolIdx, bool toggled) {
		if (d->curTCFpath.isEmpty()) {
			return;
		}
		if (nullptr == d->cur_meta) {
			return;
		}
		d->toolGroup->blockSignals(true);
		for (auto b : d->toolGroup->buttons()) {
			b->setChecked(false);
		}
		d->toolmanager->DeactivateAll();
		const auto btn = d->toolGroup->button(toolIdx);
		btn->setChecked(toggled);
		d->toolGroup->blockSignals(false);
		if (toggled) {
			d->toolmanager->ActivateTool(toolIdx);
		}
	}

}
