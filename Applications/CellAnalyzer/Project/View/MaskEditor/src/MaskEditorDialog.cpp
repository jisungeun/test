#include <iostream>

#include <QSettings>
#include <QFileDialog>
#include <VolumeViz/nodes/SoVolumeData.h>

#include "MaskEditorToolManager.h"
#include "MaskEditorRenderWindow.h"
#include "MaskEditorSceneManager.h"
#include "MaskEditorTreeControl.h"

#include <HT2D.h>
#include <FL2D.h>
#include <Float2d.h>
#include <BinaryMask2D.h>
#include <LabelMask2D.h>

#include <HT3D.h>
#include <FL3D.h>
#include <Float3d.h>
#include <BinaryMask3D.h>
#include <LabelMask3D.h>

#include <RawImporter.h>
#include <RawExporter.h>
#include <TiffImporter.h>
#include <TiffExporter.h>
#include <TiffWriter.h>
#include <TCRawWriter.h>
#include <PythonNumpyIO.h>

#include "ui_MaskEditorDialog.h"
#include "MaskEditorDialog.h"

#include "MaskEditorSliceControl.h"


namespace CellAnalyzer::Project::View::MaskEditor {

	struct MaskEditorDialog::Impl {
		Ui::Dialog ui;
		MaskEditorDialog* thisPointer { nullptr };

		//tree widget
		std::shared_ptr<MaskEditorTreeControl> treeControl { nullptr };

		std::shared_ptr<MaskEditorRenderWindow> renderWindow { nullptr };
		std::shared_ptr<MaskEditorSceneManager> sceneManager { nullptr };
		std::shared_ptr<MaskEditorToolManager> toolManager { nullptr };

		std::shared_ptr<SliceControl> sliceControl { nullptr };
		//Data Management				
		QMap<QString, DataPtr> imageMap;
		QMap<QString, DataPtr> maskMap;
		QString curMaskID;
		QString curVizSelectionID;
		bool curMaskIs3d { false };
		bool curMaskIsLabel { false };
		double curMaskOffset { 0 };
		int curMaskTimeStep { 0 };
		bool isInit { true };

		//Slice Viz Management		

		auto SetIcons() -> void;
		auto InitControls() -> void;
		auto InitConnections() -> void;
		auto Add2dSource(const QString& ID, const DataPtr& data) -> void;
		auto Add3dSource(const QString& ID, const DataPtr& data) -> void;
		auto Add2dData(const QString& ID, const DataPtr& data) -> void;
		auto Add3dData(const QString& ID, const DataPtr& data) -> void;

		auto UncheckBtns() -> void;
		auto ToggleAnyLabelBtns(bool enable) -> void;
		auto ToggleSingleLabelBtns(bool enable) -> void;
		auto ToggleMultiLabelBtns(bool enable) -> void;
		auto SetBtnShortcut() -> void;
	};

	auto MaskEditorDialog::Impl::SetBtnShortcut() -> void {
		ui.addBtn->setShortcut(QKeySequence(Qt::Key_A));
		ui.addBtn->setToolTip("Lasso [a]dd");
		ui.subBtn->setShortcut(QKeySequence(Qt::Key_S));
		ui.subBtn->setToolTip("Lasso [s]ubtract");
		ui.paintBtn->setShortcut(QKeySequence(Qt::Key_B));
		ui.paintBtn->setToolTip("[b]rush");
		ui.wipeBtn->setShortcut(QKeySequence(Qt::Key_W));
		ui.wipeBtn->setToolTip("[w]ipe");
		ui.fillBtn->setShortcut(QKeySequence(Qt::Key_F));
		ui.fillBtn->setToolTip("[f]ill");
		ui.eraseBtn->setShortcut(QKeySequence(Qt::Key_E));
		ui.eraseBtn->setToolTip("[e]rase");
		ui.divideBtn->setShortcut(QKeySequence(Qt::Key_D));
		ui.divideBtn->setToolTip("[d]ivide");
		ui.handBtn->setShortcut(QKeySequence(Qt::Key_I));
		ui.handBtn->setToolTip("[i]dle");
		ui.pickBtn->setShortcut(QKeySequence(Qt::Key_P));
		ui.pickBtn->setToolTip("[p]ick label");

		ui.erodeBtn->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_E));
		ui.erodeBtn->setToolTip("[E]rosion");
		ui.dilateBtn->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_D));
		ui.dilateBtn->setToolTip("[D]ilation");
		ui.mergeBtn->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_M));
		ui.mergeBtn->setToolTip("[M]erge selected labels");
		ui.interpolationBtn->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_I));
		ui.interpolationBtn->setToolTip("[I]nterpolation");
		ui.cleanBtn->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_W));
		ui.cleanBtn->setToolTip("[W]ipe whole slice");
		ui.cleanSelBtn->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_C));
		ui.cleanSelBtn->setToolTip("[C]lean selected label from slice");
		ui.flushBtn->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_R));
		ui.flushBtn->setToolTip("[R]eset image");
		ui.flushSelBtn->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_F));
		ui.flushSelBtn->setToolTip("[F]lush selected label");

		ui.undoBtn->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Z));
		ui.undoBtn->setToolTip("Undo");

		ui.redoBtn->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_Z));
		ui.redoBtn->setToolTip("Redo");
	}

	auto MaskEditorDialog::Impl::UncheckBtns() -> void {
		ui.addBtn->setChecked(false);
		ui.subBtn->setChecked(false);
		ui.paintBtn->setChecked(false);
		ui.wipeBtn->setChecked(false);
		ui.fillBtn->setChecked(false);
		ui.eraseBtn->setChecked(false);
		ui.divideBtn->setChecked(false);
		ui.handBtn->setChecked(false);
		ui.pickBtn->setChecked(false);

		ui.brushSizeSpin->hide();
		ui.brushLabel->hide();
	}


	auto MaskEditorDialog::Impl::ToggleAnyLabelBtns(bool enable) -> void {
		ui.cleanSelBtn->setEnabled(enable);
		ui.flushSelBtn->setEnabled(enable);
	}


	auto MaskEditorDialog::Impl::ToggleSingleLabelBtns(bool enable) -> void {
		ui.addBtn->setEnabled(enable);
		ui.subBtn->setEnabled(enable);
		ui.paintBtn->setEnabled(enable);
		ui.wipeBtn->setEnabled(enable);
		ui.fillBtn->setEnabled(enable);
		ui.eraseBtn->setEnabled(enable);
		ui.divideBtn->setEnabled(enable);
		ui.interpolationBtn->setEnabled(enable);
		ui.dilateBtn->setEnabled(enable);
		ui.erodeBtn->setEnabled(enable);

		ui.assignBtn->setEnabled(enable);
		ui.assignValueSpin->setEnabled(enable);
	}

	auto MaskEditorDialog::Impl::ToggleMultiLabelBtns(bool enable) -> void {
		ui.mergeBtn->setEnabled(enable);
	}


	auto MaskEditorDialog::Impl::Add2dSource(const QString& ID, const DataPtr& data) -> void {
		imageMap[ID] = data;
		if (isInit) {
			QSignalBlocker blocker(ui.zSpin);
			ui.zSpin->setEnabled(false);
			ui.zSpin->setValue(1);
		}
	}

	auto MaskEditorDialog::Impl::Add3dSource(const QString& ID, const DataPtr& data) -> void {
		imageMap[ID] = data;
		if (isInit) {
			const auto vol = std::dynamic_pointer_cast<IVolume3D>(data);
			const auto size = vol->GetSize();
			QSignalBlocker blocker(ui.zSpin);
			ui.zSpin->setRange(1, size.z);
			const auto startValue = std::floor(size.z / 2.0);
			ui.zSpin->setValue(startValue);
		}
	}

	auto MaskEditorDialog::Impl::Add2dData(const QString& ID, const DataPtr& data) -> void {
		if (std::dynamic_pointer_cast<Data::Float2D>(data) ||
			std::dynamic_pointer_cast<Data::HT2D>(data) ||
			std::dynamic_pointer_cast<Data::FL2D>(data)) {
			imageMap[ID] = data;
			treeControl->AddImage(ID);
		} else if (std::dynamic_pointer_cast<Data::BinaryMask2D>(data) || std::dynamic_pointer_cast<Data::LabelMask2D>(data)) {
			maskMap[ID] = data;
			treeControl->AddMask(ID);
		}
	}

	auto MaskEditorDialog::Impl::Add3dData(const QString& ID, const DataPtr& data) -> void {
		if (std::dynamic_pointer_cast<Data::Float3D>(data) ||
			std::dynamic_pointer_cast<Data::HT3D>(data) ||
			std::dynamic_pointer_cast<Data::FL3D>(data)) {
			imageMap[ID] = data;
			treeControl->AddImage(ID);
		} else if (std::dynamic_pointer_cast<Data::BinaryMask3D>(data) ||
			std::dynamic_pointer_cast<Data::LabelMask3D>(data)) {
			maskMap[ID] = data;
			treeControl->AddMask(ID);
		}
	}

	auto MaskEditorDialog::Impl::InitControls() -> void {
		ui.handBtn->setCheckable(true);
		ui.handBtn->setChecked(true);

		ui.pickBtn->setCheckable(true);

		ui.addBtn->setCheckable(true);
		ui.addBtn->setEnabled(false);

		ui.subBtn->setCheckable(true);
		ui.subBtn->setEnabled(false);

		ui.paintBtn->setCheckable(true);
		ui.paintBtn->setEnabled(false);

		ui.wipeBtn->setCheckable(true);
		ui.wipeBtn->setEnabled(false);

		ui.fillBtn->setCheckable(true);
		ui.fillBtn->setEnabled(false);

		ui.eraseBtn->setCheckable(true);
		ui.eraseBtn->setEnabled(false);

		ui.divideBtn->setCheckable(true);
		ui.divideBtn->setEnabled(false);

		ui.interpolationBtn->setEnabled(false);

		ui.dilateBtn->setEnabled(false);

		ui.erodeBtn->setEnabled(false);

		ui.mergeBtn->setEnabled(false);

		ui.cleanSelBtn->setEnabled(false);

		ui.flushSelBtn->setEnabled(false);

		ui.brushLabel->hide();
		ui.brushSizeSpin->hide();
		ui.brushSizeSpin->setRange(1, 100);
		ui.brushSizeSpin->setValue(10);

		ui.curToolValueSpin->setRange(1, 60000);

		ui.labelSelectionPanel->hide();
		ui.labelSelectionPanel->setReadOnly(true);
		ui.assignValueSpin->setRange(1, 60000);
		ui.assignBtn->setEnabled(false);
		ui.assignValueSpin->setEnabled(false);

		ui.transpSpin->setRange(0, 1);
		ui.transpSpin->setDecimals(2);
		ui.transpSpin->setSingleStep(0.01);
		ui.transpSpin->setValue(0.0);

		ui.contrastMinSpin->setEnabled(false);
		ui.contrastMaxSpin->setEnabled(false);
	}

	auto MaskEditorDialog::Impl::SetIcons() -> void {
		ui.confirmBtn->setIcon(QIcon(":/meimage/images/SaveToLink.png"));
		ui.confirmBtn->setIconSize(QSize(40, 40));

		ui.saveAsBtn->setIcon(QIcon(":/meimage/images/Save.png"));
		ui.saveAsBtn->setIconSize(QSize(40, 40));

		ui.loadBtn->setIcon(QIcon(":/meimage/images/Load.png"));
		ui.loadBtn->setIconSize(QSize(40, 40));

		ui.handBtn->setIcon(QIcon(":/meimage/images/Cursor.png"));
		ui.handBtn->setIconSize(QSize(40, 40));

		ui.pickBtn->setIcon(QIcon(":/meimage/images/Pick.png"));
		ui.pickBtn->setIconSize(QSize(40, 40));

		ui.addBtn->setIcon(QIcon(":/meimage/images/Add.png"));
		ui.addBtn->setIconSize(QSize(40, 40));

		ui.subBtn->setIcon(QIcon(":/meimage/images/Subtract.png"));
		ui.subBtn->setIconSize(QSize(40, 40));

		ui.paintBtn->setIcon(QIcon(":/meimage/images/Paint.png"));
		ui.paintBtn->setIconSize(QSize(40, 40));

		ui.wipeBtn->setIcon(QIcon(":/meimage/images/Wipe.png"));
		ui.wipeBtn->setIconSize(QSize(40, 40));

		ui.fillBtn->setIcon(QIcon(":/meimage/images/Fill.png"));
		ui.fillBtn->setIconSize(QSize(40, 40));

		ui.eraseBtn->setIcon(QIcon(":/meimage/images/Erase.png"));
		ui.eraseBtn->setIconSize(QSize(40, 40));

		ui.divideBtn->setIcon(QIcon(":/meimage/images/Divide.png"));
		ui.divideBtn->setIconSize(QSize(40, 40));

		ui.interpolationBtn->setIcon(QIcon(":/meimage/images/Interpolation.png"));
		ui.interpolationBtn->setIconSize(QSize(40, 40));

		ui.dilateBtn->setIcon(QIcon(":/meimage/images/Dilate.png"));
		ui.dilateBtn->setIconSize(QSize(40, 40));

		ui.erodeBtn->setIcon(QIcon(":/meimage/images/Erode.png"));
		ui.erodeBtn->setIconSize(QSize(40, 40));

		ui.mergeBtn->setIcon(QIcon(":/meimage/images/Merge.png"));
		ui.mergeBtn->setIconSize(QSize(40, 40));

		ui.cleanSelBtn->setIcon(QIcon(":/meimage/images/CleanSelection.png"));
		ui.cleanSelBtn->setIconSize(QSize(40, 40));

		ui.flushSelBtn->setIcon(QIcon(":/meimage/images/FlushSelection.png"));
		ui.flushSelBtn->setIconSize(QSize(40, 40));

		ui.cleanBtn->setIcon(QIcon(":/meimage/images/Clean.png"));
		ui.cleanBtn->setIconSize(QSize(40, 40));

		ui.flushBtn->setIcon(QIcon(":/meimage/images/Flush.png"));
		ui.flushBtn->setIconSize(QSize(40, 40));

		ui.undoBtn->setIcon(QIcon(":/meimage/images/Undo.png"));
		ui.undoBtn->setIconSize(QSize(40, 40));

		ui.redoBtn->setIcon(QIcon(":/meimage/images/Redo.png"));
		ui.redoBtn->setIconSize(QSize(40, 40));
	}

	auto MaskEditorDialog::Impl::InitConnections() -> void {
		connect(ui.confirmBtn, SIGNAL(clicked()), thisPointer, SLOT(OnConfirmClicked()));
		connect(ui.saveAsBtn, SIGNAL(clicked()), thisPointer, SLOT(OnSaveAsClicked()));
		connect(ui.loadBtn, SIGNAL(clicked()), thisPointer, SLOT(OnLoadClicked()));
		connect(ui.handBtn, SIGNAL(clicked()), thisPointer, SLOT(OnHandClicked()));
		connect(ui.pickBtn, SIGNAL(clicked()), thisPointer, SLOT(OnPickClicked()));
		connect(ui.addBtn, SIGNAL(clicked()), thisPointer, SLOT(OnAddClicked()));
		connect(ui.subBtn, SIGNAL(clicked()), thisPointer, SLOT(OnSubtractClicked()));
		connect(ui.paintBtn, SIGNAL(clicked()), thisPointer, SLOT(OnPaintClicked()));
		connect(ui.wipeBtn, SIGNAL(clicked()), thisPointer, SLOT(OnWipeClicked()));
		connect(ui.fillBtn, SIGNAL(clicked()), thisPointer, SLOT(OnFillClicked()));
		connect(ui.eraseBtn, SIGNAL(clicked()), thisPointer, SLOT(OnEraseClicked()));
		connect(ui.divideBtn, SIGNAL(clicked()), thisPointer, SLOT(OnDivideClicked()));
		connect(ui.interpolationBtn, SIGNAL(clicked()), thisPointer, SLOT(OnInterpolationClicked()));
		connect(ui.dilateBtn, SIGNAL(clicked()), thisPointer, SLOT(OnDilateClicked()));
		connect(ui.erodeBtn, SIGNAL(clicked()), thisPointer, SLOT(OnErodeClicked()));
		connect(ui.mergeBtn, SIGNAL(clicked()), thisPointer, SLOT(OnMergeClicked()));
		connect(ui.cleanSelBtn, SIGNAL(clicked()), thisPointer, SLOT(OnCleanSelClicked()));
		connect(ui.flushSelBtn, SIGNAL(clicked()), thisPointer, SLOT(OnFlushSelClicked()));
		connect(ui.cleanBtn, SIGNAL(clicked()), thisPointer, SLOT(OnCleanClicked()));
		connect(ui.flushBtn, SIGNAL(clicked()), thisPointer, SLOT(OnFlushClicked()));

		//connection from tree widget
		connect(treeControl.get(), SIGNAL(sigImage(const QString&, bool)), thisPointer, SLOT(OnImageVisiblity(const QString&,bool)));
		connect(treeControl.get(), SIGNAL(sigMask(const QString&)), thisPointer, SLOT(OnMaskSelection(const QString&)));
		connect(treeControl.get(), SIGNAL(sigSelection(const QString&)), thisPointer, SLOT(OnVizItemSelected(const QString&)));

		//connectoin from z value
		connect(ui.zSpin, SIGNAL(valueChanged(int)), thisPointer, SLOT(OnZSpinValueChanged(int)));
		connect(renderWindow.get(), SIGNAL(sigMouseWheel(int)), thisPointer, SLOT(OnRenderScroll(int)));

		//tool management
		connect(renderWindow.get(), SIGNAL(sigCtrlKey(bool)), toolManager.get(), SLOT(OnCtrlKey(bool)));;
		connect(toolManager.get(), SIGNAL(sigLabelChanged(QList<int>)), thisPointer, SLOT(OnLabelSelectionChanged(QList<int>)));
		connect(ui.brushSizeSpin, SIGNAL(valueChanged(int)), thisPointer, SLOT(OnBrushSizeChanged(int)));

		//custom label control
		connect(ui.labelsBtn, SIGNAL(clicked()), thisPointer, SLOT(OnLabelsClicked()));
		connect(ui.assignBtn, SIGNAL(clicked()), thisPointer, SLOT(OnAssignLabelClicked()));

		connect(ui.curToolValueSpin, SIGNAL(valueChanged(int)), thisPointer, SLOT(OnToolValueChanged(int)));
		connect(toolManager.get(), SIGNAL(sigLabelMaxFromDivider(int)), thisPointer, SLOT(OnLabelMaxChanged(int)));

		connect(ui.transpSpin, SIGNAL(valueChanged(double)), thisPointer, SLOT(OnSliceTranspSpin(double)));
		connect(ui.contrastMinSpin, SIGNAL(valueChanged(double)), thisPointer, SLOT(OnImageRangeChanged(double)));
		connect(ui.contrastMaxSpin, SIGNAL(valueChanged(double)), thisPointer, SLOT(OnImageRangeChanged(double)));
		connect(sceneManager.get(), SIGNAL(sigImageAdded(const QString&)), thisPointer, SLOT(OnImageAdded(const QString&)));
		connect(sceneManager.get(), SIGNAL(sigMaskAdded(const QString&)), thisPointer, SLOT(OnMaskAdded(const QString&)));

		connect(ui.undoBtn, SIGNAL(clicked()), thisPointer, SLOT(OnUndoClicked()));
		connect(ui.redoBtn, SIGNAL(clicked()), thisPointer, SLOT(OnRedoClicked()));
	}

	MaskEditorDialog::MaskEditorDialog(QWidget* parent) : QDialog(parent), IView(), d { new Impl } {
		d->thisPointer = this;
		d->ui.setupUi(this);

		setWindowTitle("Mask Editor");
		setWindowFlags(windowFlags() | Qt::WindowMaximizeButtonHint);

		d->renderWindow = std::make_shared<MaskEditorRenderWindow>(nullptr);
		const auto layout = new QVBoxLayout;
		layout->setContentsMargins(4, 4, 4, 4);
		layout->setSpacing(0);
		layout->addWidget(d->renderWindow.get());
		d->ui.renderSocket->setLayout(layout);

		d->sceneManager = std::make_shared<MaskEditorSceneManager>();

		d->toolManager = std::make_shared<MaskEditorToolManager>();

		d->sceneManager->SetToolRoot(d->toolManager->GetToolSceneRoot());

		d->renderWindow->setSceneGraph(d->sceneManager->GetRootNode());

		d->treeControl = std::make_shared<MaskEditorTreeControl>();

		const auto tlayout = new QVBoxLayout;
		tlayout->setContentsMargins(0, 0, 0, 0);
		tlayout->setSpacing(0);
		tlayout->addWidget(d->treeControl->GetTreeWidget());

		d->ui.treeSocket->setLayout(tlayout);

		const auto slayout = new QVBoxLayout;
		slayout->setContentsMargins(0, 0, 0, 0);
		slayout->setSpacing(0);

		d->ui.controlSocket->setLayout(slayout);

		d->SetIcons();
		d->InitControls();
		d->InitConnections();
		d->SetBtnShortcut();
	}

	MaskEditorDialog::~MaskEditorDialog() = default;

	auto MaskEditorDialog::GetCurrentMask() -> std::tuple<QString, DataPtr> {
		DataPtr result { nullptr };

		const auto data = d->toolManager->GetWorkingVolume();
		double min, max;
		data->getMinMax(min, max);
		const auto dim = data->getDimension();
		const auto res = data->getVoxelSize();

		const auto dimX = dim[0];
		const auto dimY = dim[1];
		const auto dimZ = dim[2];
		const auto resX = res[0];
		const auto resY = res[1];
		const auto resZ = res[2];

		std::unique_ptr<uint16_t[]> dataArr(new uint16_t[dimX * dimY * dimZ](), std::default_delete<uint16_t[]>());
		memcpy(dataArr.get(), data->data.getValue(), dimX * dimY * dimZ * sizeof(uint16_t));

		if (d->curMaskIs3d) {
			if (d->curMaskIsLabel) {
				result = std::shared_ptr<Data::LabelMask3D>(new Data::LabelMask3D(std::move(dataArr), static_cast<int>(max), { dimX, dimY, dimZ }, { resX, resY, resZ }, { -dimX * resX / 2, -dimY * resY / 2, -dimZ * resZ / 2 }, d->curMaskOffset, d->curMaskTimeStep));
			} else {
				result = std::shared_ptr<Data::BinaryMask3D>(new Data::BinaryMask3D(std::move(dataArr), { dimX, dimY, dimZ }, { resX, resY, resZ }, { -dimX * resX / 2, -dimY * resY / 2, -dimZ * resZ / 2 }, d->curMaskOffset, d->curMaskTimeStep));
			}
		} else {
			if (d->curMaskIsLabel) { } else { }
		}

		return std::make_tuple(d->curMaskID, result);
	}

	auto MaskEditorDialog::AddSource(const QString& ID, const std::shared_ptr<IData>& data) -> void {
		if (std::dynamic_pointer_cast<IVolume2D>(data)) {
			d->Add2dSource(ID, data);
		} else if (std::dynamic_pointer_cast<IVolume3D>(data)) {
			d->Add3dSource(ID, data);
		}
		d->treeControl->AddImage(ID);
	}

	auto MaskEditorDialog::AddData(const QString& ID, const std::shared_ptr<IData>& data) -> void {
		if (std::dynamic_pointer_cast<IVolume2D>(data)) {
			d->Add2dData(ID, data);
		} else if (std::dynamic_pointer_cast<IVolume3D>(data)) {
			d->Add3dData(ID, data);
		}
	}


	bool MaskEditorDialog::eventFilter(QObject* watched, QEvent* event) {
		return true;
	}

	////////SLOTS///////////
	void MaskEditorDialog::OnLabelSelectionChanged(QList<int> labels) {
		d->ToggleAnyLabelBtns(false);
		d->ToggleMultiLabelBtns(false);
		d->ToggleSingleLabelBtns(false);
		if (labels.count() > 0) {
			d->ToggleAnyLabelBtns(true);
			if (labels.count() > 1) {
				d->ToggleMultiLabelBtns(true);
			} else {
				d->ToggleSingleLabelBtns(true);
				QSignalBlocker blocker(d->ui.curToolValueSpin);
				d->ui.curToolValueSpin->setValue(labels.first());
			}
		}
		d->sceneManager->SetHighlight(labels);

		d->ui.labelSelectionPanel->clear();
		for (const auto l : labels) {
			d->ui.labelSelectionPanel->appendPlainText(QString::number(l));
		}
	}

	void MaskEditorDialog::OnZSpinValueChanged(int val) {
		d->sceneManager->SetZIndex(val);
		d->toolManager->SetCurSliceIndex(val - 1);
	}

	void MaskEditorDialog::OnRenderScroll(int delta) {
		const auto curZ = d->ui.zSpin->value();
		if (delta > 0) {
			if (curZ + 1 <= d->ui.zSpin->maximum()) {
				d->ui.zSpin->setValue(curZ + 1);
			}
		} else {
			if (curZ - 1 > 0) {
				d->ui.zSpin->setValue(curZ - 1);
			}
		}
	}

	void MaskEditorDialog::OnConfirmClicked() {
		this->accept();
	}

	void MaskEditorDialog::OnSaveAsClicked() {
		const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/maskeditor", QApplication::applicationDirPath()).toString();
		const auto path = QFileDialog::getSaveFileName(nullptr, tr("Save Screenshot as Image"), prev + "/", tr("Image files (*.npy *.raw *.tiff)"));
		if (path.isEmpty()) {
			return;
		}
		QSettings("Tomocube", "TomoAnalysis").setValue("recent/maskeditor", path);

		QFileInfo fInfo(path);


		QString extension = fInfo.suffix();

		const auto volume = d->toolManager->GetWorkingVolume();
		if (!volume) {
			return;
		}
		const auto dim = volume->getDimension();
		const auto res = volume->getVoxelSize();
		double origin[3];
		for (auto i = 0; i < 3; i++) {
			origin[i] = -dim[i] * res[i] / 2.0;
		}

		double min, max;
		volume->getMinMax(min, max);

		if (extension.toLower() == "raw") {
			auto customDeleter = [](uint16_t*) {};
			auto dataPtr = std::make_shared<Data::LabelMask3D>(std::shared_ptr<uint16_t[]>(static_cast<uint16_t*>(volume->data.getValue()), customDeleter), max, Size3D { static_cast<int>(dim[0]), static_cast<int>(dim[1]), static_cast<int>(dim[2]) }, Resolution3D { res[0], res[1], res[2] }, Origin3D { origin[0], origin[1], origin[2] });

			std::shared_ptr<IO::Export::RawExporter> exporter = std::make_shared<IO::Export::RawExporter>();
			exporter->Export(dataPtr, path);
		}
		if (extension.toLower() == "tiff") {
			auto customDeleter = [](uint16_t*) {};
			auto dataPtr = std::make_shared<Data::LabelMask3D>(std::shared_ptr<uint16_t[]>(static_cast<uint16_t*>(volume->data.getValue()), customDeleter), max, Size3D { static_cast<int>(dim[0]), static_cast<int>(dim[1]), static_cast<int>(dim[2]) }, Resolution3D { res[0], res[1], res[2] }, Origin3D { origin[0], origin[1], origin[2] });

			std::shared_ptr<IO::Export::TiffExporter> exporter =
				std::make_shared<IO::Export::TiffExporter>();
			exporter->Export(dataPtr, path);
		} else if (extension.toLower() == "npy") {
			TC::IO::TCNumpyWriter writer;
			std::vector<unsigned long> shape;
			shape.push_back(dim[2]);
			shape.push_back(dim[0]);
			shape.push_back(dim[1]);
			auto type = TC::IO::NpyArrType::arrUSHORT;

			const auto* ushortArr = static_cast<unsigned short*>(volume->data.getValue());
			size_t arrSize = dim[0] * dim[1] * dim[2];
			std::any anyArr = std::vector(ushortArr, ushortArr + arrSize);

			if (writer.Write(anyArr, path, type, shape)) {
				QFileInfo fileInfo(path);
				QString newFilePath = fileInfo.path() + "/" + fileInfo.completeBaseName() + ".txt";
				std::ofstream outFile(newFilePath.toStdString());

				if (outFile.is_open()) {
					outFile << res[0] << " " << res[1] << " " << res[2] << std::endl;
				}

				outFile.close();
			}
		}
	}

	void MaskEditorDialog::OnLoadClicked() {
		const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/maskeditor", QApplication::applicationDirPath()).toString();
		const auto path = QFileDialog::getOpenFileName(nullptr, tr("Save Screenshot as Image"), prev + "/", tr("Image files (*.npy *.raw *.tiff)"));
		if (path.isEmpty()) {
			return;
		}
		QSettings("Tomocube", "TomoAnalysis").setValue("recent/maskeditor", path);

		QFileInfo fInfo(path);

		QString extension = fInfo.suffix();

		const auto volume = d->toolManager->GetWorkingVolume();
		if (!volume) {
			return;
		}
		const auto dim = volume->getDimension();

		if (extension.toLower() == "raw") {
			DataFlags flag;
			flag.setFlag(DataFlag::Volume3D);
			flag.setFlag(DataFlag::Label);
			std::shared_ptr<IO::Import::RawImporter> importer = std::make_shared<IO::Import::RawImporter>();
			if (auto dataPtr = std::dynamic_pointer_cast<IVolume3D>(importer->Import(path, flag))) {
				const auto [x,y,z] = dataPtr->GetSize();

				if (x != dim[0] || y != dim[1] || z != dim[2]) {
					return;
				}

				volume->data.setValue(dim, SbDataType::UNSIGNED_SHORT, 16, dataPtr->GetData(), SoSFArray::COPY);
				double min, max;
				volume->getMinMax(min, max);
				d->sceneManager->SetRangeMax(max);
			}
		}
		if (extension.toLower() == "tiff") {
			DataFlags flag;
			flag.setFlag(DataFlag::Volume3D);
			flag.setFlag(DataFlag::Label);
			std::shared_ptr<IO::Import::TiffImporter> importer = std::make_shared<IO::Import::TiffImporter>();
			if (auto dataPtr = std::dynamic_pointer_cast<IVolume3D>(importer->Import(path, flag))) {
				const auto [x, y, z] = dataPtr->GetSize();

				if (x != dim[0] || y != dim[1] || z != dim[2]) {
					return;
				}

				volume->data.setValue(dim, SbDataType::UNSIGNED_SHORT, 16, dataPtr->GetData(), SoSFArray::COPY);
				double min, max;
				volume->getMinMax(min, max);
				d->sceneManager->SetRangeMax(max);
			}
		} else if (extension.toLower() == "npy") {
			TC::IO::TCNumpyReader reader;
			auto type = TC::IO::NpyArrType::arrUSHORT;
			std::any inputArr;
			const auto [fortran_order, result_shape] = reader.Read(inputArr, path, type);

			if (result_shape.size() < 3) {
				return;
			}

			if (result_shape[0] != dim[2] || result_shape[1] != dim[0] || result_shape[2] != dim[1]) {
				return;
			}

			auto resultArr = std::any_cast<std::vector<unsigned short>>(inputArr);
			volume->data.setValue(dim, SbDataType::UNSIGNED_SHORT, 16, resultArr.data(), SoSFArray::COPY);
			double min, max;
			volume->getMinMax(min, max);
			d->sceneManager->SetRangeMax(max);
		}
	}

	void MaskEditorDialog::OnHandClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->ui.handBtn->setChecked(true);
	}

	void MaskEditorDialog::OnPickClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->ui.pickBtn->setChecked(true);
		d->toolManager->ActivatePickTool();
	}

	void MaskEditorDialog::OnAddClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->ui.addBtn->setChecked(true);
		d->toolManager->ActivateAddTool();
	}

	void MaskEditorDialog::OnSubtractClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->ui.subBtn->setChecked(true);
		d->toolManager->ActivateSubTool();
	}

	void MaskEditorDialog::OnPaintClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->ui.paintBtn->setChecked(true);
		d->toolManager->ActivatePaintTool();
		d->toolManager->SetCurSliceIndex(d->ui.zSpin->value() - 1);
		d->ui.brushSizeSpin->show();
		d->ui.brushLabel->show();
	}

	void MaskEditorDialog::OnWipeClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->ui.wipeBtn->setChecked(true);
		d->toolManager->ActivateWipeTool();
		d->ui.brushSizeSpin->show();
		d->ui.brushLabel->show();
	}

	void MaskEditorDialog::OnFillClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->ui.fillBtn->setChecked(true);
		d->toolManager->ActivateFillTool();
		d->toolManager->SetCurSliceIndex(d->ui.zSpin->value() - 1);
	}

	void MaskEditorDialog::OnEraseClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->ui.eraseBtn->setChecked(true);
		d->toolManager->ActivateEraseTool();
		d->toolManager->SetCurSliceIndex(d->ui.zSpin->value() - 1);
	}

	void MaskEditorDialog::OnDivideClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->ui.divideBtn->setChecked(true);
		d->toolManager->ActivateDivideTool();
		d->toolManager->SetCurSliceIndex(d->ui.zSpin->value() - 1);
	}

	void MaskEditorDialog::OnInterpolationClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->toolManager->PerformInterpolate();
	}

	void MaskEditorDialog::OnDilateClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->toolManager->PerformDilate();
	}

	void MaskEditorDialog::OnErodeClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->toolManager->PerformErode();
	}

	void MaskEditorDialog::OnMergeClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->toolManager->PerformMerge();
	}

	void MaskEditorDialog::OnCleanSelClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->toolManager->SetCurSliceIndexValue(d->ui.zSpin->value() - 1);
		d->toolManager->PerformCleanSel();
	}

	void MaskEditorDialog::OnFlushSelClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->toolManager->PerformFlushSel();
	}

	void MaskEditorDialog::OnCleanClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->toolManager->SetCurSliceIndexValue(d->ui.zSpin->value() - 1);
		d->toolManager->PerformClean();
	}

	void MaskEditorDialog::OnFlushClicked() {
		d->toolManager->DeactivateTool();
		d->UncheckBtns();
		d->toolManager->PerformFlush();
	}

	void MaskEditorDialog::OnImageVisiblity(const QString& ID, bool visible) {
		if (false == d->imageMap.contains(ID)) {
			return;
		}
		d->sceneManager->ShowImageData3d(ID, d->imageMap[ID], visible);

		if (d->isInit) {
			d->renderWindow->ResetView2D();
			d->isInit = false;
		}
	}

	void MaskEditorDialog::OnMaskSelection(const QString& ID) {
		if (false == d->maskMap.contains(ID)) {
			return;
		}
		if (d->curMaskID == ID) {
			return;
		}
		d->curMaskID = ID;
		const auto [isBinary,offset,timestep] = d->sceneManager->SetMaskData3d(ID, d->maskMap[ID]);
		d->curMaskIs3d = true;
		d->curMaskIsLabel = !isBinary;
		d->curMaskOffset = offset;
		d->curMaskTimeStep = timestep;

		d->toolManager->SetWorkingVolume(d->sceneManager->GetCurrentVolume());
	}

	void MaskEditorDialog::OnVizItemSelected(const QString& ID) {
		if (const auto meta = d->sceneManager->GetSliceMeta(ID)) {
			QSignalBlocker minBlock(d->ui.contrastMinSpin);
			QSignalBlocker maxBlock(d->ui.contrastMaxSpin);
			if (d->sceneManager->GetIsImage(ID)) {
				d->ui.contrastMinSpin->setEnabled(true);
				d->ui.contrastMaxSpin->setEnabled(true);

				const auto [lower, upper] = d->sceneManager->GetImageDataRange(ID);

				const auto [min, max] = meta->GetRange();
				const auto decimals = meta->GetDecimals();
				const auto divider = meta->GetDivider();
				const auto singlestep = meta->GetSingleStep();

				d->ui.contrastMinSpin->setRange(min / divider, max / divider);
				d->ui.contrastMinSpin->setDecimals(decimals);
				d->ui.contrastMinSpin->setSingleStep(singlestep);
				d->ui.contrastMinSpin->setValue(lower);

				d->ui.contrastMaxSpin->setRange(min / divider, max / divider);
				d->ui.contrastMaxSpin->setDecimals(decimals);
				d->ui.contrastMaxSpin->setSingleStep(singlestep);
				d->ui.contrastMaxSpin->setValue(upper);
			} else {
				d->ui.contrastMinSpin->setEnabled(false);
				d->ui.contrastMaxSpin->setEnabled(false);
			}
		} else {
			d->ui.contrastMinSpin->setEnabled(false);
			d->ui.contrastMaxSpin->setEnabled(false);
		}
		d->curVizSelectionID = ID;

		const auto transp = d->sceneManager->GetSliceTransp(ID);
		QSignalBlocker blocker(d->ui.transpSpin);
		d->ui.transpSpin->setValue(transp);

		d->ui.curVizLabel->setText(QString("Current selection: %1").arg(ID));
	}

	void MaskEditorDialog::OnImageAdded(const QString& ID) {
		this->OnVizItemSelected(ID);
		return;
		QSignalBlocker minBlocker(d->ui.contrastMinSpin);
		QSignalBlocker maxBlocker(d->ui.contrastMaxSpin);
		QSignalBlocker transBlocker(d->ui.transpSpin);
		d->ui.contrastMaxSpin->setEnabled(true);
		d->ui.contrastMinSpin->setEnabled(true);

		const auto meta = d->sceneManager->GetSliceMeta(ID);

		const auto [lower, upper] = d->sceneManager->GetImageDataRange(ID);

		const auto [min, max] = meta->GetRange();
		const auto decimals = meta->GetDecimals();
		const auto divider = meta->GetDivider();
		const auto singlestep = meta->GetSingleStep();

		d->ui.contrastMinSpin->setRange(min / divider, max / divider);
		d->ui.contrastMinSpin->setDecimals(decimals);
		d->ui.contrastMinSpin->setSingleStep(singlestep);
		d->ui.contrastMinSpin->setValue(lower);

		d->ui.contrastMaxSpin->setRange(min / divider, max / divider);
		d->ui.contrastMaxSpin->setDecimals(decimals);
		d->ui.contrastMaxSpin->setSingleStep(singlestep);
		d->ui.contrastMaxSpin->setValue(upper);

		const auto transp = d->sceneManager->GetSliceTransp(ID);
		d->ui.transpSpin->setValue(transp);
	}

	void MaskEditorDialog::OnMaskAdded(const QString& ID) {
		this->OnVizItemSelected(ID);
		return;
		d->ui.contrastMaxSpin->setEnabled(false);
		d->ui.contrastMinSpin->setEnabled(false);
		QSignalBlocker transBlocker(d->ui.transpSpin);

		const auto transp = d->sceneManager->GetSliceTransp(ID);
		d->ui.transpSpin->setValue(transp);
	}


	void MaskEditorDialog::OnImageRangeChanged(double val) {
		Q_UNUSED(val)
		const auto lower = d->ui.contrastMinSpin->value();
		const auto upper = d->ui.contrastMaxSpin->value();

		d->sceneManager->SetImageDataRange(d->curVizSelectionID, lower, upper);
	}

	void MaskEditorDialog::OnSliceTranspSpin(double transp) {
		d->sceneManager->SetSliceTransp(d->curVizSelectionID, transp);
	}

	void MaskEditorDialog::OnSliceTransp(double transp) {
		std::cout << "Trasnp: " << transp << std::endl;
	}

	void MaskEditorDialog::OnSliceDataRange(double min, double max) {
		std::cout << "min max: " << min << " " << max << std::endl;
	}

	void MaskEditorDialog::OnSliceColormap(float* colormap, int maxNum, bool isLabel) {
		std::cout << "colormap: " << maxNum << std::endl;
	}


	void MaskEditorDialog::OnBrushSizeChanged(int size) {
		if (d->toolManager) {
			d->toolManager->SetBrushSize(size);
		}
	}

	void MaskEditorDialog::OnLabelsClicked() {
		if (d->ui.labelsBtn->isChecked()) {
			d->ui.labelSelectionPanel->show();
		} else {
			d->ui.labelSelectionPanel->hide();
		}
	}

	void MaskEditorDialog::OnAssignLabelClicked() {
		d->toolManager->PerformAssign(d->ui.assignValueSpin->value());
	}

	void MaskEditorDialog::OnLabelMaxChanged(int value) {
		d->sceneManager->SetRangeMax(value);
		QList<int> tmpLabel;
		tmpLabel.append(d->ui.curToolValueSpin->value());
		d->sceneManager->SetHighlight(tmpLabel);
	}

	void MaskEditorDialog::OnToolValueChanged(int value) {
		d->toolManager->SetCustomLabelValue(value);

		d->sceneManager->SetRangeMax(value);

		QList<int> tmpLabel;
		tmpLabel.append(value);

		LabelSelectionChangedFromCustom(tmpLabel);
	}

	auto MaskEditorDialog::LabelSelectionChangedFromCustom(QList<int> labels) -> void {
		d->ToggleAnyLabelBtns(false);
		d->ToggleMultiLabelBtns(false);
		d->ToggleSingleLabelBtns(false);
		if (labels.count() > 0) {
			d->ToggleAnyLabelBtns(true);
			if (labels.count() > 1) {
				d->ToggleMultiLabelBtns(true);
			} else {
				d->ToggleSingleLabelBtns(true);
			}
		}
		d->sceneManager->SetHighlight(labels);

		d->ui.labelSelectionPanel->clear();
		for (const auto l : labels) {
			d->ui.labelSelectionPanel->appendPlainText(QString::number(l));
		}
	}

	void MaskEditorDialog::OnUndoClicked() {
		d->toolManager->PerformUndo();
	}

	void MaskEditorDialog::OnRedoClicked() {
		d->toolManager->PerformRedo();
	}
}
