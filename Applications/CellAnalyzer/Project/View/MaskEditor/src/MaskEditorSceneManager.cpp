#include <HT2D.h>
#include <HT3D.h>
#include <FL2D.h>
#include <FL3D.h>
#include <Float2d.h>
#include <Float3d.h>
#include <BinaryMask2D.h>
#include <BinaryMask3D.h>
#include <LabelMask2D.h>
#include <LabelMask3D.h>
#include <SliceGeneral.h>
#include <ColorMap.h>

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoVolumeShader.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <Medical/helpers/MedicalHelper.h>

#include <MaskEditorSliceMeta.h>

#include "MaskEditorSceneManager.h"

using namespace Tomocube::Rendering::Image;

namespace CellAnalyzer::Project::View::MaskEditor {
	struct MaskEditorSceneManager::Impl {
		SoRef<SoSeparator> root { nullptr };

		ColorMapPtr tempTF { nullptr };

		SoRef<SoSeparator> imageRoot { nullptr };
		SoRef<SoSeparator> toolRoot { nullptr };
		QMap<QString, std::shared_ptr<SliceGeneral>> imageSlice;
		QMap<QString, std::tuple<double, double>> imageDataRange;

		QMap<QString, bool> isImage;

		SoRef<SoSeparator> maskRoot { nullptr };
		QMap<QString, std::shared_ptr<SliceGeneral>> maskSlice;

		QMap<QString, double> sliceTransp;
		QMap<QString, std::shared_ptr<SliceMeta>> sliceMeta;

		float mask_color_table[12][3];
		SoVolumeData* curMaskVolume { nullptr };
		bool ht3dExist { false };
		int curDimZ { 1 };
		double curResZ { 1 };
		double curPhyZ { 0 };

		QList<QString> imageOrder;
		QString invalidLetterInOiv = " +{}[]'""\".";

		auto BuildLabelColorTable() -> void;
		auto BuildLabelColormap(int minVal) -> QList<float>;
		auto BuildSingleTone(QColor col) -> QList<float>;
		auto InitSceneGraph() -> void;
		auto AdjustOffsetZ(double offset) -> double;
		auto ReorderSlice() -> void;
	};

	auto MaskEditorSceneManager::Impl::ReorderSlice() -> void {
		for (auto i = 0; i < imageOrder.count(); i++) {
			const auto iid = imageOrder[i];
			if (const auto slice = imageSlice[iid]) {
				const auto trans = MedicalHelper::find<SoTranslation>(slice->GetRootSwitch());
				//trans->translation.setValue(0, 0, -10000.0f * static_cast<float>(i));
				trans->translation.setValue(0, 0, 10000.0f * (imageOrder.count() - i));
			}
		}

		if (false == maskSlice.isEmpty()) {
			const auto mtrans = MedicalHelper::find<SoTranslation>(maskSlice.first()->GetRootSwitch());
			mtrans->translation.setValue(0, 0, 0);
			//mtrans->translation.setValue(0, 0, -10000.0f * static_cast<float>(imageOrder.count()));
		}
	}

	auto MaskEditorSceneManager::Impl::AdjustOffsetZ(double offset) -> double {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.0001;
		};
		if (AreSame(offset, 0)) {
			return 0;
		}
		if (ht3dExist) {
			return offset - static_cast<double>(curDimZ) * curResZ / 2.0;
		}
		return offset;
	}

	auto MaskEditorSceneManager::SetToolRoot(SoSeparator* root) -> void {
		d->toolRoot = root;
		//d->root->addChild(root);
		d->root->insertChild(root, 0);
	}

	auto MaskEditorSceneManager::GetCurrentVolume() -> SoVolumeData* {
		return d->curMaskVolume;
	}

	auto MaskEditorSceneManager::SetImageDataRange(const QString& ID, double min, double max) -> void {
		const auto meta = d->sliceMeta[ID];
		const auto realMin = min * meta->GetDivider();
		const auto realMax = max * meta->GetDivider();
		d->imageDataRange[ID] = std::make_tuple(realMin, realMax);
		for (const auto is : d->imageSlice) {
			if (is->GetName() == ID) {
				is->SetDataRange(realMin, realMax);
				return;
			}
		}
	}

	auto MaskEditorSceneManager::GetIsImage(const QString& ID) const -> bool {
		if (d->isImage.contains(ID)) {
			return d->isImage[ID];
		}
		return false;
	}

	auto MaskEditorSceneManager::GetImageDataRange(const QString& ID) const -> std::tuple<double, double> {
		const auto meta = d->sliceMeta[ID];
		const auto [min, max] = d->imageDataRange[ID];
		const auto realMin = min / meta->GetDivider();
		const auto realMax = max / meta->GetDivider();
		return std::make_tuple(realMin, realMax);
	}

	auto MaskEditorSceneManager::GetSliceTransp(const QString& ID) const -> double {
		return d->sliceTransp[ID];
	}

	auto MaskEditorSceneManager::SetSliceTransp(const QString& ID, double transp) -> void {
		d->sliceTransp[ID] = transp;

		for (const auto is : d->imageSlice) {
			if (is->GetName() == ID) {
				is->SetSliceTransparency(static_cast<float>(transp));
				return;
			}
		}
		for (const auto ms : d->maskSlice) {
			if (ms->GetName() == ID) {
				ms->SetSliceTransparency(static_cast<float>(transp));
				return;
			}
		}
	}


	auto MaskEditorSceneManager::GetSliceMeta(const QString& ID) -> std::shared_ptr<SliceMeta> {
		return d->sliceMeta[ID];
	}

	auto MaskEditorSceneManager::Impl::BuildSingleTone(QColor col) -> QList<float> {
		const auto colortone = col;
		const auto gamma = 1.0;
		QList<float> result;
		for (auto i = 0; i < 256; i++) {
			const auto ratio = static_cast<float>(i) / 255.0f;
			const auto gammaFactor = static_cast<float>(pow(ratio, gamma));
			result.append(static_cast<float>(colortone.redF()) * gammaFactor);
			result.append(static_cast<float>(colortone.greenF()) * gammaFactor);
			result.append(static_cast<float>(colortone.blueF()) * gammaFactor);
			result.append(gammaFactor);
		}
		return result;
	}

	auto MaskEditorSceneManager::Impl::BuildLabelColorTable() -> void {
		mask_color_table[0][0] = 255;
		mask_color_table[0][1] = 59;
		mask_color_table[0][2] = 48; //red
		mask_color_table[1][0] = 255;
		mask_color_table[1][1] = 149;
		mask_color_table[1][2] = 0; //Orange
		mask_color_table[2][0] = 255;
		mask_color_table[2][1] = 204;
		mask_color_table[2][2] = 0; //yellow
		mask_color_table[3][0] = 52;
		mask_color_table[3][1] = 199;
		mask_color_table[3][2] = 89; //Green
		mask_color_table[4][0] = 0;
		mask_color_table[4][1] = 199;
		mask_color_table[4][2] = 190; //Mint
		mask_color_table[5][0] = 48;
		mask_color_table[5][1] = 176;
		mask_color_table[5][2] = 199; //Teal
		mask_color_table[6][0] = 50;
		mask_color_table[6][1] = 173;
		mask_color_table[6][2] = 230; //Cyan
		mask_color_table[7][0] = 0;
		mask_color_table[7][1] = 122;
		mask_color_table[7][2] = 255; //Blue
		mask_color_table[8][0] = 88;
		mask_color_table[8][1] = 86;
		mask_color_table[8][2] = 214; //Indigo
		mask_color_table[9][0] = 175;
		mask_color_table[9][1] = 82;
		mask_color_table[9][2] = 222; //Purple
		mask_color_table[10][0] = 255;
		mask_color_table[10][1] = 45;
		mask_color_table[10][2] = 85; //Pink
		mask_color_table[11][0] = 162;
		mask_color_table[11][1] = 132;
		mask_color_table[11][2] = 94; //Brown
	}

	auto MaskEditorSceneManager::Impl::BuildLabelColormap(int minVal) -> QList<float> {
		const auto imin = minVal;
		QList<float> arr;
		for (auto i = 0; i < 1024; i++) {
			arr.append(0);
		}
		for (auto i = 0; i < 256; i++) {
			arr[i * 4] = mask_color_table[(i + imin) % 12][0] / 255.0f;
			arr[i * 4 + 1] = mask_color_table[(i + imin) % 12][1] / 255.0f;
			arr[i * 4 + 2] = mask_color_table[(i + imin) % 12][2] / 255.0f;
			arr[i * 4 + 3] = 1;
		}
		return arr;
	}


	auto MaskEditorSceneManager::Impl::InitSceneGraph() -> void {
		root = new SoSeparator;

		//init image slices scene (can be many)
		imageRoot = new SoSeparator;
		root->addChild(imageRoot.ptr());

		//init unique mask slice scene
		maskRoot = new SoSeparator;
		root->addChild(maskRoot.ptr());
	}

	MaskEditorSceneManager::MaskEditorSceneManager(QObject* parent) : QObject(parent), d { new Impl } {
		d->tempTF = std::make_shared<ColorMap>();

		d->InitSceneGraph();
		d->BuildLabelColorTable();
	}

	MaskEditorSceneManager::~MaskEditorSceneManager() = default;

	auto MaskEditorSceneManager::GetRootNode() -> SoSeparator* {
		return d->root.ptr();
	}

	auto MaskEditorSceneManager::ShowImageData2d(const QString& ID, const std::shared_ptr<IData>& data, bool isVisible) -> void {
		if (d->imageSlice.contains(ID)) {
			const auto imageSlice = d->imageSlice[ID];
			d->imageRoot->removeChild(imageSlice->GetRootSwitch());
			d->imageSlice.remove(ID);
			d->imageOrder.removeOne(ID);
		}
		if (false == isVisible) {
			return;
		}
		const auto newSlice = std::make_shared<SliceGeneral>(ID);

		if (const auto ht = std::dynamic_pointer_cast<Data::HT2D>(data)) { } else if (const auto fl = std::dynamic_pointer_cast<Data::FL2D>(data)) { } else if (const auto unknwon = std::dynamic_pointer_cast<Data::Float2D>(data)) { } else {
			return;
		}
		d->imageSlice[ID] = newSlice;
		d->imageOrder.append(ID);
		d->imageRoot->addChild(newSlice->GetRootSwitch());
		d->ReorderSlice();
	}

	auto MaskEditorSceneManager::SetMaskData2d(const QString& ID, const std::shared_ptr<IData>& data) -> std::tuple<bool, double, int> {
		if (false == d->maskSlice.isEmpty()) {
			//remove existing mask layer first
			const auto maskSlice = d->maskSlice.first();
			d->maskRoot->removeChild(maskSlice->GetRootSwitch());
			d->maskSlice.clear();
		}

		const auto newSlice = std::make_shared<SliceGeneral>(ID);

		bool isBinary = false;
		if (const auto binary = std::dynamic_pointer_cast<Data::BinaryMask2D>(data)) {
			isBinary = true;
		}
		if (const auto label = std::dynamic_pointer_cast<Data::LabelMask2D>(data)) { } else {
			return {};
		}

		d->maskSlice[ID] = newSlice;
		d->maskRoot->addChild(newSlice->GetRootSwitch());
		d->ReorderSlice();

		return std::make_tuple(isBinary, 0, 0);
	}

	auto MaskEditorSceneManager::ShowImageData3d(const QString& ID, const std::shared_ptr<IData>& data, bool isVisible) -> void {
		if (d->imageSlice.contains(ID)) {
			const auto imageSlice = d->imageSlice[ID];
			d->imageRoot->removeChild(imageSlice->GetRootSwitch());
			d->imageSlice.remove(ID);
			d->imageOrder.removeOne(ID);
		}
		if (false == isVisible) {
			return;
		}

		if (false == d->sliceTransp.contains(ID)) {
			d->sliceTransp[ID] = 0.0;
		}

		if (false == d->isImage.contains(ID)) {
			d->isImage[ID] = true;
		}

		const auto newSlice = std::make_shared<SliceGeneral>(ID);

		if (const auto ht = std::dynamic_pointer_cast<Data::HT3D>(data)) {
			const auto [riMin, riMax] = ht->GetRI();
			const auto [dimX, dimY, dimZ] = ht->GetSize();
			const auto [resX, resY, resZ] = ht->GetResolution();

			const auto meta = std::make_shared<SliceMeta>();
			meta->SetColormapID("Intensity");
			meta->SetDecimals(4);
			meta->SetRange(riMin * 10000.0, riMax * 10000.0);
			meta->SetDivider(10000);
			meta->SetSingleStep(0.0001);
			meta->SetMaxZ(dimZ);
			meta->SetCurrentZ(floor(dimZ / 2.0));

			d->sliceMeta[ID] = meta;

			SoRef<SoVolumeData> volData = new SoVolumeData;
			volData->data.setValue(SbVec3i32(dimX, dimY, dimZ), SbDataType::UNSIGNED_SHORT, 16, ht->GetData(), SoSFArray::NO_COPY);
			volData->extent.setValue(SbVec3f(-dimX * resX / 2, -dimY * resY / 2, -dimZ * resZ / 2), SbVec3f(dimX * resX / 2, dimY * resY / 2, dimZ * resZ / 2));

			newSlice->SetDataMinMax(riMin * 10000.0, riMax * 10000.0);
			if (d->imageDataRange.contains(ID)) {
				const auto [lower, upper] = d->imageDataRange[ID];
				newSlice->SetDataRange(lower, upper);
			} else {
				d->imageDataRange[ID] = std::make_tuple(riMin * 10000.0, riMax * 10000.0);
			}
			if (false == d->ht3dExist) {
				const auto zPhys = (floor(dimZ / 2.0) - 1) * resZ - dimZ * resZ / 2.0;
				d->curPhyZ = zPhys;
				d->curDimZ = dimZ;
				d->curResZ = resZ;
				d->ht3dExist = true;
			}
			newSlice->SetVolume(volData.ptr());
			newSlice->SetGeometryZ(dimZ, resZ);
			newSlice->SetPhyZPos(d->curPhyZ, true);
			newSlice->SetSliceTransparency(d->sliceTransp[ID]);
		} else if (const auto fl = std::dynamic_pointer_cast<Data::FL3D>(data)) {
			const auto [min, max] = fl->GetIntensity();
			const auto [dimX, dimY, dimZ] = fl->GetSize();
			const auto [resX, resY, resZ] = fl->GetResolution();

			const auto meta = std::make_shared<SliceMeta>();
			meta->SetColormapID("Single Tone");
			meta->SetDecimals(0);
			meta->SetRange(min, max);
			meta->SetSingleStep(1);
			meta->SetMaxZ(dimZ);
			meta->SetCurrentZ(floor(dimZ / 2.0));
			const auto [r, g, b] = fl->GetChannelColor();
			const auto chIdx = fl->GetChannelIndex();
			if (r != 0 || g != 0 || b != 0) {
				meta->SetSingleTone(QColor(r, g, b));
			} else if (chIdx == 0) {
				meta->SetSingleTone(QColor(0, 0, 255));
			} else if (chIdx == 1) {
				meta->SetSingleTone(QColor(0, 255, 0));
			} else {
				meta->SetSingleTone(QColor(255, 0, 0));
			}

			d->sliceMeta[ID] = meta;

			SoRef<SoVolumeData> volData = new SoVolumeData;
			volData->data.setValue(SbVec3i32(dimX, dimY, dimZ), SbDataType::UNSIGNED_SHORT, 16, fl->GetData(), SoSFArray::NO_COPY);
			volData->extent.setValue(SbVec3f(-dimX * resX / 2, -dimY * resY / 2, -dimZ * resZ / 2), SbVec3f(dimX * resX / 2, dimY * resY / 2, dimZ * resZ / 2));

			newSlice->SetDataMinMax(min, max);
			if (d->imageDataRange.contains(ID)) {
				const auto [lower, upper] = d->imageDataRange[ID];
				newSlice->SetDataRange(lower, upper);
			} else {
				d->imageDataRange[ID] = std::make_tuple(min, max);
			}
			newSlice->SetVolume(volData.ptr());
			newSlice->SetGeometryZ(dimZ, resZ, d->AdjustOffsetZ(fl->GetZOffset()));
			newSlice->SetPhyZPos(d->curPhyZ, true);
			d->tempTF->SetColormap(d->BuildSingleTone(QColor(r, g, b)));
			newSlice->SetColormap(d->tempTF->GetDataPointer(), false, 256);
			newSlice->SetSliceTransparency(d->sliceTransp[ID]);

		} else if (const auto unknwon = std::dynamic_pointer_cast<Data::Float3D>(data)) {
			const auto [min, max] = unknwon->GetRange();
			const auto [dimX, dimY, dimZ] = unknwon->GetSize();
			const auto [resX, resY, resZ] = unknwon->GetResolution();

			const auto meta = std::make_shared<SliceMeta>();
			meta->SetColormapID("Intensity");
			meta->SetDecimals(2);
			meta->SetRange(min, max);
			meta->SetSingleStep(0.01);
			meta->SetMaxZ(dimZ);
			meta->SetCurrentZ(floor(dimZ / 2.0));

			d->sliceMeta[ID] = meta;

			SoRef<SoVolumeData> volData = new SoVolumeData;
			volData->data.setValue(SbVec3i32(dimX, dimY, dimZ), SbDataType::FLOAT, 32, unknwon->GetData(), SoSFArray::NO_COPY);
			volData->extent.setValue(SbVec3f(-dimX * resX / 2, -dimY * resY / 2, -dimZ * resZ / 2), SbVec3f(dimX * resX / 2, dimY * resY / 2, dimZ * resZ / 2));

			newSlice->SetDataMinMax(min, max);
			if (d->imageDataRange.contains(ID)) {
				const auto [lower, upper] = d->imageDataRange[ID];
				newSlice->SetDataRange(lower, upper);
			} else {
				d->imageDataRange[ID] = std::make_tuple(min, max);
			}
			newSlice->SetVolume(volData.ptr());
			newSlice->SetGeometryZ(dimZ, resZ, d->AdjustOffsetZ(unknwon->GetZOffset()));
			newSlice->SetPhyZPos(d->curPhyZ, true);
			newSlice->SetSliceTransparency(d->sliceTransp[ID]);

		} else {
			return;
		}

		d->imageSlice[ID] = newSlice;
		d->imageRoot->addChild(newSlice->GetRootSwitch());
		d->imageOrder.append(ID);
		d->ReorderSlice();

		emit sigImageAdded(ID);
	}

	auto MaskEditorSceneManager::SetMaskData3d(const QString& ID, const std::shared_ptr<IData>& data) -> std::tuple<bool, double, int> {
		if (false == d->maskSlice.isEmpty()) {
			//remove existing mask layer first
			const auto maskSlice = d->maskSlice.first();
			d->maskRoot->removeChild(maskSlice->GetRootSwitch());
			d->maskSlice.clear();
		}
		if (false == d->sliceTransp.contains(ID)) {
			d->sliceTransp[ID] = 0.0;
		}

		if (false == d->isImage.contains(ID)) {
			d->isImage[ID] = false;
		}

		const auto newSlice = std::make_shared<SliceGeneral>(ID);

		bool isBinary = false;
		double offset { 0 };
		int timestep { 0 };
		if (const auto binary = std::dynamic_pointer_cast<Data::BinaryMask3D>(data)) {
			const auto [dimX, dimY, dimZ] = binary->GetSize();
			const auto [resX, resY, resZ] = binary->GetResolution();

			const auto meta = std::make_shared<SliceMeta>();
			meta->SetColormapID("Label Mask");
			meta->SetDecimals(0);
			meta->SetRange(0, 1);
			meta->SetSingleStep(1);
			meta->SetMaxZ(dimZ);
			meta->SetCurrentZ(floor(dimZ / 2.0));

			d->sliceMeta[ID] = meta;

			SoRef<SoVolumeData> volData = new SoVolumeData;
			volData->data.setValue(SbVec3i32(dimX, dimY, dimZ), SbDataType::UNSIGNED_SHORT, 16, binary->GetData(), SoSFArray::COPY);
			volData->extent.setValue(SbVec3f(-dimX * resX / 2, -dimY * resY / 2, -dimZ * resZ / 2), SbVec3f(dimX * resX / 2, dimY * resY / 2, dimZ * resZ / 2));
			volData->ldmResourceParameters.getValue()->fixedResolution = TRUE;
			volData->ldmResourceParameters.getValue()->resolution = 0;

			d->curMaskVolume = volData.ptr();

			newSlice->SetDataMinMax(0, 2);
			newSlice->SetVolume(volData.ptr());
			newSlice->SetIsMask(true);
			newSlice->SetGeometryZ(dimZ, resZ, d->AdjustOffsetZ(binary->GetZOffset()));
			newSlice->SetPhyZPos(d->curPhyZ, true);
			newSlice->SetSliceTransparency(d->sliceTransp[ID]);

			d->tempTF->SetColormap(d->BuildLabelColormap(0));
			newSlice->SetColormap(d->tempTF->GetDataPointer(), true, 1);
			isBinary = true;
			offset = binary->GetZOffset();
			timestep = binary->GetTimeStep();
		} else if (const auto label = std::dynamic_pointer_cast<Data::LabelMask3D>(data)) {
			const auto [dimX, dimY, dimZ] = label->GetSize();
			const auto [resX, resY, resZ] = label->GetResolution();

			const auto meta = std::make_shared<SliceMeta>();
			meta->SetColormapID("Label Mask");
			meta->SetDecimals(0);
			const auto maxLabel = label->GetMaxIndex();
			meta->SetRange(0, maxLabel);
			meta->SetSingleStep(1);
			meta->SetMaxZ(dimZ);
			meta->SetCurrentZ(floor(dimZ / 2.0));

			d->sliceMeta[ID] = meta;

			SoRef<SoVolumeData> volData = new SoVolumeData;
			volData->data.setValue(SbVec3i32(dimX, dimY, dimZ), SbDataType::UNSIGNED_SHORT, 16, label->GetData(), SoSFArray::COPY);
			volData->extent.setValue(SbVec3f(-dimX * resX / 2, -dimY * resY / 2, -dimZ * resZ / 2), SbVec3f(dimX * resX / 2, dimY * resY / 2, dimZ * resZ / 2));
			volData->ldmResourceParameters.getValue()->fixedResolution = TRUE;
			volData->ldmResourceParameters.getValue()->resolution = 0;

			d->curMaskVolume = volData.ptr();

			newSlice->SetDataMinMax(0, maxLabel + 1);
			newSlice->SetVolume(volData.ptr());
			newSlice->SetIsMask(true);
			newSlice->SetGeometryZ(dimZ, resZ, d->AdjustOffsetZ(label->GetZOffset()));
			newSlice->SetPhyZPos(d->curPhyZ, true);
			newSlice->SetSliceTransparency(d->sliceTransp[ID]);

			d->tempTF->SetColormap(d->BuildLabelColormap(0));
			newSlice->SetColormap(d->tempTF->GetDataPointer(), true, maxLabel);
			newSlice->SetLabelHighlight(QList<int>());
			offset = label->GetZOffset();
			timestep = label->GetTimeStep();
		} else {
			return {};
		}
		d->maskSlice[ID] = newSlice;
		d->maskRoot->addChild(newSlice->GetRootSwitch());
		d->ReorderSlice();

		emit sigMaskAdded(ID);

		return std::make_tuple(isBinary, offset, timestep);
	}

	auto MaskEditorSceneManager::SetZIndex(int value) -> void {
		const auto zPhys = (value - 1) * d->curResZ - d->curDimZ * d->curResZ / 2.0;
		for (const auto is : d->imageSlice) {
			is->SetPhyZPos(zPhys, true);
		}
		for (const auto ms : d->maskSlice) {
			ms->SetPhyZPos(zPhys, true);
		}
		d->curPhyZ = zPhys;
	}

	auto MaskEditorSceneManager::SetHighlight(QList<int> selectedLabels) -> void {
		for (const auto ms : d->maskSlice) {
			ms->SetLabelHighlight(selectedLabels);
		}
	}

	auto MaskEditorSceneManager::SetRangeMax(int val) -> void {
		for (const auto ms : d->maskSlice) {
			const auto [min, max] = ms->GetDataMinMax();
			if (max < val + 1) {
				ms->SetDataMinMax(0, val + 1);
				ms->SetColormap(d->tempTF->GetDataPointer(), true, val);
			}
		}
	}

}
