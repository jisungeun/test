#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <iolink/view/ImageViewFactory.h>
#include <ioformat/IOFormat.h>

#include <Oiv2DDrawer.h>
#include <OivLasso.h>
#include <OivBrush.h>
#include <OivFloodFill.h>
#include <OivInterpolator.h>
#include <OivDivider2d.h>

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include "MaskEditorToolManager.h"

using namespace imagedev;
using namespace iolink;
using namespace ioformat;

namespace CellAnalyzer::Project::View::MaskEditor {
	struct MaskEditorToolManager::Impl {
		MaskEditorToolManager* thisPointer { nullptr };
		SoRef<SoSeparator> toolRoot { nullptr };
		SoRef<SoSwitch> toolSwitch { nullptr };
		std::shared_ptr<Oiv2DDrawer> drawer { nullptr };

		std::shared_ptr<OivLasso> lasso { nullptr };
		std::shared_ptr<OivBrush> brush { nullptr };
		std::shared_ptr<OivFloodFill> floodFill { nullptr };
		SoRef<OivDivider> divider { nullptr };

		SoVolumeData* workingData { nullptr };
		QList<int> curLabels;
		bool isCtrlKeyPressed { false };
		int curSliceIndex { 0 };

		QList<int> historyID;
		QList<int> undoID;
		int maxHistoryCount { 3 };

		auto SoVolumeToImageView(SoVolumeData* vol) -> std::shared_ptr<ImageView>;
		auto OnFinishDivider(SoPolyLineScreenDrawer::EventArg& eventArg) -> void;
		auto ApplyModifiedVolumeWithHistory(std::shared_ptr<ImageView> modifiedVolume) -> void;
	};

	auto MaskEditorToolManager::Impl::ApplyModifiedVolumeWithHistory(std::shared_ptr<ImageView> modifiedVolume) -> void {
		if (!workingData) {
			return;
		}
		SoRef<SoCpuBufferObject> targetBufferObj = new SoCpuBufferObject;

		auto size = workingData->getDimension();
		SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(size[0] - 1, size[1] - 1, size[2] - 1));

		SoLDMDataAccess::DataInfoBox targetInfoBox = workingData->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
		targetBufferObj->setSize((size_t)targetInfoBox.bufferSize);
		workingData->getLdmDataAccess().getData(0, bBox, targetBufferObj.ptr());
		auto targetdata = static_cast<unsigned short*>(targetBufferObj->map(SoBufferObject::READ_WRITE));

		memcpy(targetdata, modifiedVolume->buffer(), targetBufferObj->getSize());

		int editionId;
		workingData->startEditing(editionId);
		workingData->editSubVolume(bBox, targetBufferObj.ptr());
		workingData->finishEditing(editionId);

		thisPointer->AddHistory(editionId);

		targetBufferObj->unmap();

		targetBufferObj = nullptr;
	}

	auto MaskEditorToolManager::Impl::OnFinishDivider(SoPolyLineScreenDrawer::EventArg& eventArg) -> void {
		const auto [newlabelMax, editionId] = divider->DivideLabel(eventArg, curLabels.first());

		if (newlabelMax < 0) {
			return;
		}

		thisPointer->AddHistory(editionId);

		emit thisPointer->sigLabelMaxFromDivider(newlabelMax);
	}

	auto MaskEditorToolManager::Impl::SoVolumeToImageView(SoVolumeData* vol) -> std::shared_ptr<ImageView> {
		std::shared_ptr<iolink::ImageView> imageView { nullptr };
		try {
			const auto dim = vol->getDimension();
			const auto resolution = vol->getVoxelSize();

			const auto x = dim[0];
			const auto y = dim[1];
			const auto z = dim[2];

			if (z < 2) {	// 2D			
				auto spatialCalibrationProperty = SpatialCalibrationProperty(
																			{ -x * resolution[0] / 2, -y * resolution[1] / 2, 0 },	// origin
																			{ resolution[0], resolution[1], 1 }	// spacing
																			);

				const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
				const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y) };
				imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
				setDimensionalInterpretation(imageView, ImageTypeId::IMAGE);

				const RegionXu64 imageRegion { { 0, 0 }, imageShape };
				imageView->writeRegion(imageRegion, vol->data.getValue());

			} else {	// 3D
				auto spatialCalibrationProperty = SpatialCalibrationProperty(
																			{ -x * resolution[0] / 2, -y * resolution[1] / 2, -z * resolution[2] / 2 },	// origin
																			{ resolution[0], resolution[1], resolution[2] }	// spacing
																			);

				const auto imageProperties = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(DataTypeId::UINT16));
				const VectorXu64 imageShape { static_cast<uint64_t>(x), static_cast<uint64_t>(y), static_cast<uint64_t>(z) };

				imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProperties, nullptr);
				setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);

				const RegionXu64 imageRegion { { 0, 0, 0 }, imageShape };
				imageView->writeRegion(imageRegion, vol->data.getValue());
			}
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return imageView;
	}

	MaskEditorToolManager::MaskEditorToolManager(QObject* parent) : QObject(parent), d { new Impl } {
		d->thisPointer = this;
		OivDivider::initClass();
		d->toolRoot = new SoSeparator;
		d->toolSwitch = new SoSwitch;
		d->toolRoot->addChild(d->toolSwitch.ptr());

		d->drawer = std::make_shared<Oiv2DDrawer>();
		d->lasso = std::make_shared<OivLasso>();
		d->brush = std::make_shared<OivBrush>();
		d->floodFill = std::make_shared<OivFloodFill>();
		d->divider = new OivDivider;
		d->divider->onFinish.add(*d, &Impl::OnFinishDivider);

		d->toolSwitch->addChild(d->drawer->GetSceneGraph());
		d->toolSwitch->addChild(d->lasso->GetSceneGraph());
		d->toolSwitch->addChild(d->brush->GetSceneGraph());
		d->toolSwitch->addChild(d->floodFill->GetSceneGraph());
		d->toolSwitch->addChild(d->divider.ptr());
		connect(d->drawer.get(), SIGNAL(pickValue(int)), this, SLOT(OnLabelPicked(int)));
		connect(d->lasso.get(), SIGNAL(sigHistory(int)), this, SLOT(OnHistory(int)));
		connect(d->brush.get(), SIGNAL(sigHistory(int)), this, SLOT(OnHistory(int)));
		connect(d->floodFill.get(), SIGNAL(sigHistory(int)), this, SLOT(OnHistory(int)));
	}

	MaskEditorToolManager::~MaskEditorToolManager() {
		OivDivider::exitClass();
	}

	void MaskEditorToolManager::OnHistory(int editionID) {
		if (!d->workingData) {
			return;
		}
		AddHistory(editionID);
	}

	void MaskEditorToolManager::AddHistory(int editionID) {
		if (!d->workingData) {
			return;
		}
		if (d->historyID.count() > d->maxHistoryCount - 1) {
			//pop front history
			const auto frontID = d->historyID.front();
			d->historyID.pop_front();
		} else if (d->historyID.count() + d->undoID.count() > d->maxHistoryCount - 1) {
			//pop front undo history
			const auto frontID = d->undoID.front();
			d->undoID.pop_front();
		}
		d->historyID.push_back(editionID);
	}

	auto MaskEditorToolManager::PerformUndo() -> void {
		if (!d->workingData) {
			return;
		}
		if (d->historyID.count() < 1) {
			return;
		}
		const auto backID = d->historyID.back();

		d->workingData->undoEditing(backID);

		d->historyID.pop_back();

		d->undoID.push_back(backID);
	}

	auto MaskEditorToolManager::PerformRedo() -> void {
		if (!d->workingData) {
			return;
		}
		if (d->undoID.count() < 1) {
			return;
		}
		const auto backID = d->undoID.back();

		d->workingData->redoEditing(backID);

		d->undoID.pop_back();

		d->historyID.push_back(backID);
	}

	auto MaskEditorToolManager::SaveEditing() -> void {
		if (!d->workingData) {
			return;
		}
		d->historyID.clear();
		d->undoID.clear();
		d->workingData->saveEditing();
	}

	auto MaskEditorToolManager::SetCurSliceIndexValue(int idx) -> void {
		d->curSliceIndex = idx;
	}

	auto MaskEditorToolManager::SetCurSliceIndex(int idx) -> void {
		d->curSliceIndex = idx;
		if (d->workingData) {
			d->brush->SetSliceNumber(idx);
			d->floodFill->SetSliceNumber(idx);
		}
	}

	void MaskEditorToolManager::OnCtrlKey(bool pressed) {
		d->isCtrlKeyPressed = pressed;
	}

	void MaskEditorToolManager::OnLabelPicked(int val) {
		if (val < 0) {
			return;
		}
		if (val < 1) {
			d->drawer->SetCurLabel(-1);
		} else {
			d->drawer->SetCurLabel(val);
		}
		//emit sigLabelChanged(val);
		if (d->isCtrlKeyPressed) {
			if (val > 0) {
				if (d->curLabels.contains(val)) {
					d->curLabels.removeOne(val);
				} else {
					d->curLabels.append(val);
				}
			}
		} else {
			d->curLabels.clear();
			if (val > 0) {
				d->curLabels.append(val);
			}
		}
		emit sigLabelChanged(d->curLabels);
	}

	auto MaskEditorToolManager::SetCustomLabelValue(int value) -> void {
		d->curLabels.clear();
		d->curLabels.append(value);
		d->lasso->SetLassoValue(value);
		d->brush->SetBrushValue(value);
	}

	auto MaskEditorToolManager::GetToolSceneRoot() -> SoSeparator* {
		return d->toolRoot.ptr();
	}

	auto MaskEditorToolManager::SetWorkingVolume(SoVolumeData* volData) -> void {
		d->workingData = volData;
		d->workingData->setName("MaskVolume");
		d->workingData->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::ALWAYS;
		d->workingData->usePalettedTexture = TRUE;

		d->drawer->SetTargetVolume(d->workingData);
		d->lasso->SetTargetVolume(d->workingData);
		d->brush->SetTargetVolume(d->workingData);
		d->floodFill->SetTargetVolume(d->workingData);
		d->divider->SetTargetVolume(d->workingData);
	}

	auto MaskEditorToolManager::GetWorkingVolume() -> SoVolumeData* {
		return d->workingData;
	}

	auto MaskEditorToolManager::DeactivateTool() -> void {
		d->toolSwitch->whichChild = -1;
		d->drawer->SetTool(DrawerToolType::None);
		SaveEditing();
	}

	auto MaskEditorToolManager::ActivatePickTool() -> void {
		d->toolSwitch->whichChild = 0;
		d->drawer->SetTool(DrawerToolType::Pick);
	}

	auto MaskEditorToolManager::ActivateAddTool() -> void {
		d->toolSwitch->whichChild = 1;
		d->lasso->SetLassoValue(d->curLabels.first());
	}

	auto MaskEditorToolManager::ActivateSubTool() -> void {
		d->toolSwitch->whichChild = 1;
		d->lasso->SetLassoValue(0);
	}

	auto MaskEditorToolManager::ActivatePaintTool() -> void {
		d->toolSwitch->whichChild = 2;
		d->brush->SetBrushValue(d->curLabels.first());
	}

	auto MaskEditorToolManager::ActivateWipeTool() -> void {
		d->toolSwitch->whichChild = 2;
		d->brush->SetBrushValue(0);
	}

	auto MaskEditorToolManager::ActivateFillTool() -> void {
		d->toolSwitch->whichChild = 3;
		d->floodFill->SetCurLabel(d->curLabels.first());
		d->floodFill->ToggleFillErase(true);
	}

	auto MaskEditorToolManager::ActivateEraseTool() -> void {
		d->toolSwitch->whichChild = 3;
		d->floodFill->SetCurLabel(d->curLabels.first());
		d->floodFill->ToggleFillErase(false);
	}

	auto MaskEditorToolManager::SetBrushSize(int size) -> void {
		d->brush->SetBrushSize(size);
	}

	auto MaskEditorToolManager::PerformClean() -> void {
		if (!d->workingData) {
			return;
		}
		const auto input = d->SoVolumeToImageView(d->workingData);
		try {
			const auto sliced = getSliceFromVolume3d(input, GetSliceFromVolume3d::Z_AXIS, d->curSliceIndex);

			const auto reseted = resetImage(sliced, 0);

			const auto inserted = setSliceToVolume3d(input, reseted, SetSliceToVolume3d::Z_AXIS, d->curSliceIndex);


			d->ApplyModifiedVolumeWithHistory(inserted);
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
	}

	auto MaskEditorToolManager::PerformFlush() -> void {
		if (!d->workingData) {
			return;
		}
		const auto input = d->SoVolumeToImageView(d->workingData);
		try {
			const auto reseted = resetImage(input, 0);

			d->ApplyModifiedVolumeWithHistory(reseted);
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
	}

	auto MaskEditorToolManager::PerformCleanSel() -> void {
		if (!d->workingData) {
			return;
		}
		const auto input = d->SoVolumeToImageView(d->workingData);
		try {
			const auto sliced = getSliceFromVolume3d(input, GetSliceFromVolume3d::Z_AXIS, d->curSliceIndex);

			const auto label = static_cast<double>(d->curLabels.first());

			const auto threshed = thresholding(sliced, { label, label + 0.5 });

			const auto inv = logicalNot(threshed);

			const auto masked = maskImage(sliced, inv);

			const auto inserted = setSliceToVolume3d(input, masked, SetSliceToVolume3d::Z_AXIS, d->curSliceIndex);

			d->ApplyModifiedVolumeWithHistory(inserted);
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
	}

	auto MaskEditorToolManager::PerformFlushSel() -> void {
		if (!d->workingData) {
			return;
		}
		const auto input = d->SoVolumeToImageView(d->workingData);
		try {
			const auto label = static_cast<double>(d->curLabels.first());

			const auto threshed = thresholding(input, { label, label + 0.5 });

			const auto inv = logicalNot(threshed);

			const auto masked = maskImage(input, inv);

			d->ApplyModifiedVolumeWithHistory(masked);
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
	}

	auto MaskEditorToolManager::PerformDilate() -> void {
		if (!d->workingData) {
			return;
		}
		const auto input = d->SoVolumeToImageView(d->workingData);
		try {
			const auto label = static_cast<double>(d->curLabels.first());

			const auto threshed = thresholding(input, { label, label + 0.5 });

			const auto dilated = dilation3d(threshed, 3, Dilation3d::CONNECTIVITY_6);

			const auto inv = logicalNot(dilated);

			const auto masked = maskImage(input, inv);

			const auto reseted = resetImage(input, label);

			const auto masked2 = maskImage(reseted, dilated);

			const auto ari = arithmeticOperationWithImage(masked, masked2, ArithmeticOperationWithImage::ADD);

			const auto converted = convertImage(ari, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			d->ApplyModifiedVolumeWithHistory(converted);
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
	}

	auto MaskEditorToolManager::PerformErode() -> void {
		if (!d->workingData) {
			return;
		}
		const auto input = d->SoVolumeToImageView(d->workingData);
		try {
			const auto label = static_cast<double>(d->curLabels.first());

			const auto threshed = thresholding(input, { label, label + 0.5 });

			const auto inv = logicalNot(threshed);

			const auto masked = maskImage(input, inv);

			const auto eroded = erosion3d(threshed, 3, Erosion3d::CONNECTIVITY_6);

			const auto reseted = resetImage(input, label);

			const auto masked2 = maskImage(reseted, eroded);

			const auto ari = arithmeticOperationWithImage(masked, masked2, ArithmeticOperationWithImage::ADD);

			const auto converted = convertImage(ari, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			d->ApplyModifiedVolumeWithHistory(converted);
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
	}

	auto MaskEditorToolManager::PerformAssign(int value) -> void {
		if (!d->workingData) {
			return;
		}
		const auto input = d->SoVolumeToImageView(d->workingData);

		try {
			const auto firstlabel = static_cast<double>(d->curLabels.first());
			auto totalMask = thresholding(input, { firstlabel, firstlabel + 0.5 });

			const auto inv = logicalNot(totalMask);

			const auto masked = maskImage(input, inv);

			const auto reseted = resetImage(input, value);

			const auto masked2 = maskImage(reseted, totalMask);

			const auto ari = arithmeticOperationWithImage(masked, masked2, ArithmeticOperationWithImage::ADD);

			const auto converted = convertImage(ari, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			d->ApplyModifiedVolumeWithHistory(converted);
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}

		d->curLabels.clear();
		d->curLabels.append(value);

		emit sigLabelChanged(d->curLabels);
	}

	auto MaskEditorToolManager::PerformMerge() -> void {
		if (!d->workingData) {
			return;
		}
		const auto input = d->SoVolumeToImageView(d->workingData);

		int minLabel = INT_MAX;
		for (const auto l : d->curLabels) {
			if (l < minLabel) {
				minLabel = l;
			}
		}

		try {
			const auto firstlabel = static_cast<double>(d->curLabels.first());
			auto totalMask = thresholding(input, { firstlabel, firstlabel + 0.5 });
			for (auto i = 1; i < d->curLabels.count(); i++) {
				const auto label = static_cast<double>(d->curLabels[i]);
				auto tempMask = thresholding(input, { label, label + 0.5 });
				totalMask = logicalOperationWithImage(totalMask, tempMask, LogicalOperationWithImage::OR);
			}

			const auto inv = logicalNot(totalMask);

			const auto masked = maskImage(input, inv);

			const auto reseted = resetImage(input, minLabel);

			const auto masked2 = maskImage(reseted, totalMask);

			const auto ari = arithmeticOperationWithImage(masked, masked2, ArithmeticOperationWithImage::ADD);

			const auto converted = convertImage(ari, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			d->ApplyModifiedVolumeWithHistory(converted);
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}

		d->curLabels.clear();
		d->curLabels.append(minLabel);

		emit sigLabelChanged(d->curLabels);
	}

	auto MaskEditorToolManager::PerformInterpolate() -> void {
		if (!d->workingData) {
			return;
		}

		d->workingData->data.touch();
		d->workingData->touch();

		const auto interpolator = std::make_shared<TC::OivInterpolator>();
		interpolator->SetLabelValue(d->curLabels.first());
		interpolator->SetInputVolume(d->workingData);
		const auto [success, editionId] = interpolator->Perform();
		if (success) {
			std::cout << "interpolation success" << std::endl;
			AddHistory(editionId);
		} else {
			std::cout << "interpolation failed" << std::endl;
		}
	}

	auto MaskEditorToolManager::ActivateDivideTool() -> void {
		d->toolSwitch->whichChild = 4;
	}
}
