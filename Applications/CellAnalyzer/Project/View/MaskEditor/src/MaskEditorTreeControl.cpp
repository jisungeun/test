#include "MaskEditorTreeControl.h"

#include <iostream>
#include <QRadioButton>
#include <QButtonGroup>
#include <QCheckBox>
#include <QHeaderView>

namespace CellAnalyzer::Project::View::MaskEditor {
	struct CustomTreeWidget::Impl { };

	CustomTreeWidget::CustomTreeWidget(QWidget* parent) : QTreeWidget(parent), d { new Impl } { }

	CustomTreeWidget::~CustomTreeWidget() = default;

	void CustomTreeWidget::mouseMoveEvent(QMouseEvent* event) {
		Q_UNUSED(event)
	}


	struct MaskEditorTreeControl::Impl {
		std::shared_ptr<CustomTreeWidget> tree { nullptr };
		QTreeWidgetItem* imageRoot { nullptr };
		QTreeWidgetItem* maskRoot { nullptr };

		QMap<QString, QCheckBox*> imageChkMap;
		QMap<QString, QRadioButton*> maskRadioMap;

		QMap<QString, QTreeWidgetItem*> imageItemMap;
		QMap<QString, QTreeWidgetItem*> maskItemMap;

		QButtonGroup* chkGroup { nullptr };
		QButtonGroup* radioGroup { nullptr };
	};

	MaskEditorTreeControl::MaskEditorTreeControl(QObject* parent) : QObject(parent), d { new Impl } {
		d->tree = std::make_shared<CustomTreeWidget>();
		d->tree->setColumnCount(3);
		d->tree->header()->hide();
		d->tree->setSelectionBehavior(QAbstractItemView::SelectRows);
		d->tree->setSelectionMode(QAbstractItemView::SingleSelection);

		d->imageRoot = new QTreeWidgetItem(d->tree.get());
		d->imageRoot->setText(0, "Image");
		d->imageRoot->setExpanded(true);
		d->tree->addTopLevelItem(d->imageRoot);

		d->maskRoot = new QTreeWidgetItem(d->tree.get());
		d->maskRoot->setText(0, "Mask");
		d->maskRoot->setExpanded(true);
		d->tree->addTopLevelItem(d->maskRoot);

		d->radioGroup = new QButtonGroup;

		d->chkGroup = new QButtonGroup;
		d->chkGroup->setExclusive(false);


		connect(d->radioGroup, QOverload<QAbstractButton*>::of(&QButtonGroup::buttonClicked),
				[&](QAbstractButton* button) {
					auto* radio = qobject_cast<QRadioButton*>(button);
					const auto key = d->maskRadioMap.key(radio);
					if (radio->isChecked()) {
						emit sigMask(key);
					}
				});
		connect(d->chkGroup, QOverload<QAbstractButton*>::of(&QButtonGroup::buttonClicked),
				[&](QAbstractButton* button) {
					auto* chk = qobject_cast<QCheckBox*>(button);
					const auto key = d->imageChkMap.key(chk);
					emit sigImage(key, chk->isChecked());
				});
		connect(d->tree.get(), SIGNAL(itemSelectionChanged()), this, SLOT(OnTreeSelectionChanged()));
	}

	MaskEditorTreeControl::~MaskEditorTreeControl() = default;

	auto MaskEditorTreeControl::GetTreeWidget() -> QTreeWidget* {
		return d->tree.get();
	}

	auto MaskEditorTreeControl::AddImage(const QString& ID) -> void {
		auto* chk = new QCheckBox;

		d->imageChkMap[ID] = chk;
		d->chkGroup->addButton(chk);
		if (d->chkGroup->buttons().count() == 1) {
			chk->setChecked(true);
			emit sigImage(ID, true);
		}

		auto* item = new QTreeWidgetItem(d->imageRoot);
		d->imageItemMap[ID] = item;
		d->tree->setItemWidget(item, 0, chk);
		item->setText(1, ID);
		item->setIcon(2, QIcon(":/Flat/Data.svg"));

		d->imageRoot->addChild(item);
	}

	auto MaskEditorTreeControl::AddMask(const QString& ID) -> void {
		auto* radio = new QRadioButton;
		d->maskRadioMap[ID] = radio;
		d->radioGroup->addButton(radio);

		auto* item = new QTreeWidgetItem(d->maskRoot);
		d->maskItemMap[ID] = item;
		d->tree->setItemWidget(item, 0, radio);
		item->setText(1, ID);
		item->setIcon(2, QIcon(":/Flat/Mask.svg"));

		d->maskRoot->addChild(item);
	}

	void MaskEditorTreeControl::OnTreeSelectionChanged() {
		const auto* curItem = d->tree->currentItem();
		if (!curItem) {
			return;
		}
		if (curItem->text(1).isEmpty()) {
			return;
		}
		emit sigSelection(curItem->text(1));
	}
}
