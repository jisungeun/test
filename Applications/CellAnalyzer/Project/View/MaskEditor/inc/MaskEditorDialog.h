#pragma once

#include <memory>

#include <QDialog>
#include <QCheckBox>
#include <QTreeWidget>
#include <QTreeWidgetItem>

#include <IView.h>
#include <IData.h>

#include "CellAnalyzer.Project.View.MaskEditorExport.h"

class SoVolumeData;

namespace CellAnalyzer::Project::View::MaskEditor {
	class CellAnalyzer_Project_View_MaskEditor_API MaskEditorDialog final : public QDialog, public IView {
		Q_OBJECT
	public:
		MaskEditorDialog(QWidget* parent = nullptr);
		~MaskEditorDialog() override;

		auto AddSource(const QString& ID, const std::shared_ptr<IData>& data) -> void;
		auto AddData(const QString& ID, const std::shared_ptr<IData>& data) -> void;

		auto GetCurrentMask() -> std::tuple<QString, DataPtr>;

	protected:
		bool eventFilter(QObject* watched, QEvent* event) override;
		auto LabelSelectionChangedFromCustom(QList<int> labels) -> void;

	protected slots:
		void OnConfirmClicked();
		void OnSaveAsClicked();
		void OnLoadClicked();
		void OnHandClicked();
		void OnPickClicked();
		void OnAddClicked();
		void OnSubtractClicked();
		void OnPaintClicked();
		void OnWipeClicked();
		void OnFillClicked();
		void OnEraseClicked();
		void OnDivideClicked();
		void OnInterpolationClicked();
		void OnDilateClicked();
		void OnErodeClicked();
		void OnMergeClicked();
		void OnCleanSelClicked();
		void OnFlushSelClicked();
		void OnCleanClicked();
		void OnFlushClicked();
		void OnUndoClicked();
		void OnRedoClicked();

		//tree slots
		void OnImageVisiblity(const QString& ID, bool visible);
		void OnMaskSelection(const QString& ID);
		void OnVizItemSelected(const QString& ID);

		//z value
		void OnZSpinValueChanged(int val);
		void OnRenderScroll(int delta);

		//label management
		void OnLabelSelectionChanged(QList<int> labels);
		void OnBrushSizeChanged(int size);
		void OnLabelsClicked();
		void OnAssignLabelClicked();
		void OnToolValueChanged(int);
		void OnLabelMaxChanged(int);

		//slice viz control
		void OnSliceTransp(double);
		void OnImageRangeChanged(double);
		void OnSliceDataRange(double, double);
		void OnSliceColormap(float*, int, bool);
		void OnSliceTranspSpin(double);
		void OnImageAdded(const QString&);
		void OnMaskAdded(const QString&);

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
