#pragma once

#include <memory>

#include <QObject>
#include <QTreeWidget>
#include <QAbstractButton>

namespace CellAnalyzer::Project::View::MaskEditor {
	class CustomTreeWidget final : public QTreeWidget {
		Q_OBJECT
	public:
		CustomTreeWidget(QWidget* parent = nullptr);
		~CustomTreeWidget() override;

	protected:
		void mouseMoveEvent(QMouseEvent* event) override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	class MaskEditorTreeControl : public QObject {
		Q_OBJECT
	public:
		MaskEditorTreeControl(QObject* parent = nullptr);
		~MaskEditorTreeControl() override;

		auto GetTreeWidget() -> QTreeWidget*;
		auto AddImage(const QString& ID) -> void;
		auto AddMask(const QString& ID) -> void;

	signals:
		void sigImage(const QString& ID, bool visible);
		void sigMask(const QString& ID);
		void sigSelection(const QString& ID);

	protected slots:
		void OnTreeSelectionChanged();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
