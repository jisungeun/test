#pragma once

#include <memory>

class SoSeparator;
class SoVolumeData;

namespace CellAnalyzer::Project::View::MaskEditor {
	class MaskEditorToolManager : public QObject {
		Q_OBJECT
	public:
		MaskEditorToolManager(QObject* parent = nullptr);
		~MaskEditorToolManager() override;

		auto GetToolSceneRoot() -> SoSeparator*;

		auto SetWorkingVolume(SoVolumeData* volData) -> void;
		auto GetWorkingVolume() -> SoVolumeData*;
		auto SetCurSliceIndex(int idx) -> void;
		auto SetCurSliceIndexValue(int idx) -> void;
		auto SetBrushSize(int size) -> void;
		auto SetCustomLabelValue(int value) -> void;

		auto DeactivateTool() -> void;
		auto ActivatePickTool() -> void;

		auto ActivateAddTool() -> void;
		auto ActivateSubTool() -> void;
		auto ActivatePaintTool() -> void;
		auto ActivateWipeTool() -> void;
		auto ActivateFillTool() -> void;
		auto ActivateEraseTool() -> void;
		auto ActivateDivideTool() -> void;

		auto PerformCleanSel() -> void;
		auto PerformClean() -> void;

		auto PerformFlushSel() -> void;
		auto PerformFlush() -> void;

		auto PerformMerge() -> void;

		auto PerformDilate() -> void;
		auto PerformErode() -> void;

		auto PerformAssign(int value) -> void;
		auto PerformInterpolate() -> void;

		auto PerformUndo() -> void;
		auto PerformRedo() -> void;
		auto SaveEditing() -> void;

	signals:
		void sigLabelChanged(QList<int>);
		void sigLabelMaxFromDivider(int);

	protected slots:
		void OnLabelPicked(int);
		void OnCtrlKey(bool);
		void OnHistory(int);

	protected:
		void AddHistory(int);

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
