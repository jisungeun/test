#pragma once

#include <memory>
#include <enum.h>

#include <QOivRenderWindow.h>

class SoEventCallback;

namespace CellAnalyzer::Project::View::MaskEditor {
	class MaskEditorRenderWindow : public QOivRenderWindow {
		Q_OBJECT
	public:
		MaskEditorRenderWindow(QWidget* parent);
		~MaskEditorRenderWindow();

		auto ResetView2D() -> void;

	signals:
		void sigMouseWheel(int);
		void sigCtrlKey(bool);

	protected:
		auto MouseMoveEvent(SoEventCallback* node) -> void override;
		auto MouseButtonEvent(SoEventCallback* node) -> void override;
		auto KeyboardEvent(SoEventCallback* node) -> void override;
		auto MouseWheelEvent(SoEventCallback* node) -> void override;

		auto setDefaultWindowType() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
