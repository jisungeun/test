#pragma once

#include <memory>

#include <IData.h>

class SoSeparator;
class SoVolumeData;

namespace CellAnalyzer::Project::View::MaskEditor {
	class SliceMeta;

	class MaskEditorSceneManager : public QObject {
		Q_OBJECT
	public:
		MaskEditorSceneManager(QObject* parent = nullptr);
		~MaskEditorSceneManager() override;

		auto GetRootNode() -> SoSeparator*;

		auto SetToolRoot(SoSeparator* root) -> void;

		auto GetCurrentVolume() -> SoVolumeData*;
		auto GetSliceMeta(const QString& ID) -> std::shared_ptr<SliceMeta>;
		auto SetSliceTransp(const QString& ID, double transp) -> void;
		auto GetSliceTransp(const QString& ID) const -> double;
		auto SetImageDataRange(const QString& ID, double min, double max) -> void;
		auto GetImageDataRange(const QString& ID) const -> std::tuple<double, double>;
		auto GetIsImage(const QString& ID) const -> bool;

		auto SetMaskData2d(const QString& ID, const std::shared_ptr<IData>& data) -> std::tuple<bool, double, int>;
		auto SetMaskData3d(const QString& ID, const std::shared_ptr<IData>& data) -> std::tuple<bool, double, int>;
		auto ShowImageData2d(const QString& ID, const std::shared_ptr<IData>& data, bool isVisible) -> void;
		auto ShowImageData3d(const QString& ID, const std::shared_ptr<IData>& data, bool isVisible) -> void;

		auto SetZIndex(int value) -> void;

		auto SetHighlight(QList<int> selectedLabels) -> void;
		auto SetRangeMax(int val) -> void;

	signals:
		void sigImageAdded(const QString&);
		void sigMaskAdded(const QString&);

	protected slots:
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
