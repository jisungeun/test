#include <QWidget>

#include <enum.h>

#include <MaskEditorSliceMeta.h>

#include "CellAnalyzer.Project.View.MaskEditorExport.h"

namespace CellAnalyzer::Project::View::MaskEditor {
	class CellAnalyzer_Project_View_MaskEditor_API SliceControl : public QWidget {
		Q_OBJECT
	public:
		SliceControl(SliceMetaPtr info, QWidget* parent = nullptr);
		~SliceControl() override;

		auto InitUI() -> void;

		auto SetName(const QString& name) -> void;
		auto GetName() const -> QString;

		auto SetIcon(const QString& icon) -> void;

		auto SetOrder(int order) -> void;
		auto GetOrder() -> int;

		auto SetVisibility(bool isVisible) -> void;
		auto GetVisibility() -> bool;

		auto SetChecked(bool isChecked) -> void;
		auto GetChecked() -> bool;

		auto AddColorMap(const QString& title, QList<float> colormap) -> void;
		auto GetColorMap() -> float*;

		auto GetMaxTFstep() const -> int;

	signals:
		void sigViz(bool);
		void sigSelect(bool);
		void sigTransp(double);
		void sigDataRange(double, double);
		void sigColormap(float*, int, bool);

	protected slots:
		void OnVisibility();
		void OnSelected();
		void OnCollapseToggle();
		void OnSingletone();
		void OnMaxChanged(double);
		void OnMinChanged(double);
		void OnColormapChanged(int);
		void OnGammaChanged(double);
		void OnCustomColormapChanged();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
