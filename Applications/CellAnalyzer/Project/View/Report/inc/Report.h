#pragma once

#include <QWidget>

#include <IData.h>
#include <IPResenter.h>

#include "CellAnalyzer.Project.View.ReportExport.h"

namespace CellAnalyzer::Project::View::Report {
	class CellAnalyzer_Project_View_Report_API Report final : public QWidget, public Presenter::IPresenter {
		Q_OBJECT
	public:
		Report(QWidget* parent = nullptr);
		~Report() override;

		auto SetTitle(const QString& title) -> void override;

		auto GetWindowList() const -> ViewList override;
		auto IsPrimary(const ViewPtr& window) const -> bool override;

		auto IsAcceptable(const DataPtr& data) const -> bool override;
		auto GetDataList() const -> QStringList override;
		auto GetData(const QString& name) const -> DataPtr override;
		auto GetName(const DataPtr& data) const -> QString override;

		auto AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool override;
		auto RenameData(const DataPtr& data, const QString& name) -> bool override;
		auto RemoveData(const QString& name) -> void override;
		auto ClearData() -> void override;

		auto Capture(const QString& filepath) -> void override;

		auto OnHighlight(int label) -> void;

	signals:
		void sigHighlight(QString dataID, int labelIdx);

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
