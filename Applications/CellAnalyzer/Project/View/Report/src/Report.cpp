#include <QMap>
#include <QTableWidgetItem>

#include <Measure.h>

#include "Report.h"

#include <iostream>
#include <QFile>

#include "ui_Report.h"

namespace CellAnalyzer::Project::View::Report {
	struct Report::Impl {
		Ui::SimpleTablePresenter ui;

		std::shared_ptr<QTableWidget> summaryTable { nullptr };

		QString curDataID;
		int labelHeaderIdx { 0 };

		DataFlags dataflag { DataFlag::Measure };
		QMap<QString, DataPtr> dataMap;
		QMap<QString, bool> flMap;

		const QList<QColor> colorTable {
			QColor(255, 59, 48, 128),	// red
			QColor(255, 149, 0, 128),	// orange
			QColor(255, 204, 0, 128),	// yellow
			QColor(52, 199, 89, 128),	// green
			QColor(0, 199, 190, 128),	// mint
			QColor(48, 176, 199, 128),	// teal
			QColor(50, 173, 230, 128),	// cyan
			QColor(0, 122, 255, 128),	// blue
			QColor(88, 86, 214, 128),	// indigo
			QColor(175, 82, 222, 128),	// purple
			QColor(255, 45, 85, 128),	// pink
			QColor(162, 132, 94, 128)	// brown
		};

		auto SetData(DataPtr data) -> void;
		auto SetSummaryData(DataPtr data) -> void;
		auto ShowSummary() -> void;
		auto ResetOrder() -> void;
		auto GetPrecision(const QString& key) -> int;
	};

	auto Report::Impl::GetPrecision(const QString& key) -> int {
		if (key.contains("Mean")) {
			if (flMap[curDataID]) {
				return 0;
			}
			return 4;
		}
		if (key.contains("Centroid")) {
			return 2;
		}
		if (key.contains("SurfaceArea")) {
			return 2;
		}
		if (key.contains("Volume")) {
			return 2;
		}
		if (key.contains("ProjectedArea")) {
			return 2;
		}
		if (key.contains("Drymass")) {
			return 5;
		}
		if (key.contains("Concentration")) {
			return 3;
		}
		if (key.contains("Sphericity")) {
			return 2;
		}
		if (key.contains("Circularity")) {
			return 2;
		}
		if (key.contains("FeretDiameter")) {
			return 2;
		}
		if(key.contains("Length")) {
			return 2;
		}
		return 0;
	}

	auto Report::Impl::SetData(DataPtr data) -> void {
		const auto measureData = std::dynamic_pointer_cast<Data::Measure>(data);
		if (measureData == nullptr || measureData->GetKeys().isEmpty())
			return;

		auto tableBlocker = QSignalBlocker(ui.tableWidget);
		ui.tableWidget->clearContents();
		ui.tableWidget->setRowCount(0);
		ui.tableWidget->setColumnCount(0);

		auto measureMap = measureData->GetMap();

		if (measureMap.isEmpty()) {
			return;
		}

		const auto labelType = measureMap.value("LabelType").toString();
		if (!labelType.isEmpty())
			measureMap.remove("LabelType");

		// get data indexes in ascending order
		QList<int> indexes;
		for (const auto& key : measureMap.keys())
			indexes << key.toInt();

		if (measureMap.count() < 1) {
			return;
		}
		const auto firstMap = measureMap.first().toMap();
		const auto measureKeys = firstMap.keys();

		ui.tableWidget->setRowCount(indexes.count());
		ui.tableWidget->setColumnCount(measureKeys.count());

		QStringList headerLabels;
		auto labelColIdx = 0;
		for (auto& key : measureKeys) {
			if (key.contains("FeretDiameter") || key.contains("Length")) {
				headerLabels << QString(u8"%1(\u00b5m)").arg(key);
			}else if (key.contains("SurfaceArea") || key.contains("ProjectedArea")) {
				headerLabels << QString(u8"%1(\u00b5m\u00b2)").arg(key);
			} else if (key.contains("Volume")) {
				headerLabels << QString(u8"%1(\u00b5m\u00b3)").arg(key);
			} else if (key.contains("Drymass")) {
				headerLabels << QString(u8"%1(pg)").arg(key);
			} else if (key.contains("Concentration")) {
				headerLabels << QString(u8"%1(pg/\u00b5m\u00b3)").arg(key);
			} else {
				headerLabels << key;
			}
			if (key.right(4) == "-idx") {
				labelHeaderIdx = labelColIdx;
			}
			labelColIdx++;
		}

		ui.tableWidget->setHorizontalHeaderLabels(headerLabels);

		// add in a measure table
		for (auto i = 0; i < measureKeys.count(); i++) {
			const auto measureName = measureKeys[i];

			for (auto j = 0; j < indexes.count(); j++) {
				const auto measureValue = measureMap[QString::number(j)].toMap()[measureName].toDouble();
				const auto precision = GetPrecision(measureName);
				auto cellData = QString::number(measureValue, 'f', precision);
				for (auto& elem : QStringList { "Mean", "SurfaceArea", "Volume", "ProjectedArea", "Sphericity","FeretDiameter", "Length", "Drymass", "Concentration", "Circularity" }) {
					if (measureName.contains(elem)) {
						if (measureValue < 0)
							cellData = "N/A";
						break;
					}
				}

				const auto cellItem = new QTableWidgetItem();
				cellItem->setData(Qt::DisplayRole, cellData);
				cellItem->setTextAlignment(Qt::AlignRight);
				cellItem->setBackground(QBrush(colorTable.at(j % 12)));
				ui.tableWidget->setItem(j, i, cellItem);
			}
		}
	}

	auto Report::Impl::SetSummaryData(DataPtr data) -> void {
		// TODO: optimization

		const auto singleLabelStats = QStringList { "Count", "Sum", "Average", "STD", "Min", "Max", "D10", "D50", "D90" };

		const auto measureData = std::dynamic_pointer_cast<Data::Measure>(data);
		if (measureData == nullptr || measureData->GetKeys().isEmpty())
			return;

		auto measureMap = measureData->GetMap();
		const auto labelType = measureMap.value("LabelType").toString();
		if (!labelType.isEmpty())
			measureMap.remove("LabelType");

		auto summaryBlocker = QSignalBlocker(summaryTable.get());
		summaryTable->clearContents();
		summaryTable->setRowCount(0);
		summaryTable->setColumnCount(0);

		QList<int> indexes;
		for (const auto& key : measureMap.keys())
			indexes << key.toInt();

		std::sort(indexes.begin(), indexes.end());

		auto measureKeys = measureMap.first().toMap().keys();

		if (labelType == "SingleLabel") {
			for (auto i = measureKeys.count() - 1; i >= 0; i--) {
				if (measureKeys[i].contains("-idx"))
					measureKeys.removeAt(i);
			}

			QStringList horizontalHeader = { "Stats" };
			for (auto& key : measureKeys) {
				if (key.contains("FeretDiameter")|| key.contains("Length")) {
					horizontalHeader << QString(u8"%1(\u00b5m)").arg(key);
				}else if (key.contains("SurfaceArea") || key.contains("ProjectedArea")) {
					horizontalHeader << QString(u8"%1(\u00b5m\u00b2)").arg(key);
				} else if (key.contains("Volume")) {
					horizontalHeader << QString(u8"%1(\u00b5m\u00b3)").arg(key);
				} else if (key.contains("Drymass")) {
					horizontalHeader << QString(u8"%1(pg)").arg(key);
				} else if (key.contains("Concentration")) {
					horizontalHeader << QString(u8"%1(pg/\u00b5m\u00b3)").arg(key);
				} else {
					horizontalHeader << key;
				}
			}

			summaryTable->setColumnCount(horizontalHeader.count());
			summaryTable->setHorizontalHeaderLabels(horizontalHeader);
			summaryTable->setRowCount(singleLabelStats.count());

			QMap<QString, QMap<QString, double>> summary;

			for (auto row = 0; row < singleLabelStats.count(); row++) {
				const auto statKey = singleLabelStats[row];
				summary[statKey] = QMap<QString, double>();
				const auto item = new QTableWidgetItem(statKey);
				summaryTable->setItem(row, 0, item);
			}

			for (auto i = 0; i < measureKeys.count(); i++) {
				const auto& key = measureKeys[i];

				double sum = 0;
				double stdev = 0;
				double min = INT_MAX;
				double max = INT_MIN;

				QList<double> values;

				for (auto j = 0; j < indexes.count(); j++) {
					const auto value = measureMap[QString::number(j)].toMap()[key].toDouble();

					bool isValid = true;
					for (auto& elem : QStringList { "Mean", "SurfaceArea", "Volume", "ProjectedArea", "Sphericity", "FeretDiameter","Length", "Drymass", "Concentration", "Circularity" }) {
						if (key.contains(elem)) {
							if (value < 0) {
								isValid = false;
							}

							break;
						}
					}

					if (!isValid)
						continue;

					values.append(value);
					if (min > value)
						min = value;
					if (max < value)
						max = value;

					sum += value;
				}

				std::sort(values.begin(), values.end());

				auto mean = 0.;
				auto q10 = -1;
				auto q50 = -1;
				auto q90 = -1;

				if (!values.isEmpty()) {
					auto valueCount = values.count();
					mean = sum / static_cast<double>(valueCount);
					for (auto j = 0; j < valueCount; j++)
						stdev += pow(values[j] - mean, 2);

					stdev = sqrt(stdev / indexes.count());

					q10 = static_cast<int>(valueCount / 10.0);
					q50 = static_cast<int>(valueCount / 2.0);
					q90 = static_cast<int>(valueCount / 10.0 * 9.0);
				}

				summary["Count"][key] = indexes.count();
				summary["Sum"][key] = sum;
				summary["Average"][key] = mean;
				summary["STD"][key] = stdev;
				summary["Min"][key] = min;
				summary["Max"][key] = max;
				summary["D10"][key] = q10 < 0 ? 0 : values[q10];
				summary["D50"][key] = q50 < 0 ? 0 : values[q50];
				summary["D90"][key] = q90 < 0 ? 0 : values[q90];

				for (auto row = 0; row < singleLabelStats.count(); row++) {
					const auto statKey = singleLabelStats[row];
					const auto statValue = summary[statKey][key];
					const auto precision = GetPrecision(key);
					auto cellValue = QString::number(statValue, 'f', precision);
					for (auto& elem : QStringList { "Mean", "SurfaceArea", "Volume", "ProjectedArea", "Sphericity","FeretDiameter","Length", "Drymass", "Concentration", "Circularity" }) {
						if (key.contains(elem)) {
							if (statValue < 0)
								cellValue = "N/A";

							break;
						}
					}

					summaryTable->setItem(row, i + 1, new QTableWidgetItem(cellValue));
					summaryTable->item(row, i + 1)->setTextAlignment(Qt::AlignRight);
				}
			}
		} else if (labelType == "DualLabel") {
			QMap<QString, QMap<QString, QMap<QString, double>>> summary;

			QList<int> parentIdx;
			for (auto i = 0; i < measureKeys.count(); i++) {
				const auto& key = measureKeys[i];
				if (key.contains("-parentidx")) {
					for (auto j = 0; j < indexes.count(); j++) {
						auto value = measureMap[QString::number(j)].toMap()[key].toDouble();
						parentIdx.append(static_cast<int>(value));
					}
					break;
				}
			}
			for (auto i = measureKeys.count() - 1; i >= 0; i--) {
				if (measureKeys[i].contains("-parentidx") || measureKeys[i].contains("-idx"))
					measureKeys.removeAt(i);
			}

			QStringList horizontalHeader = { "ParentIdx", "Stats" };
			for (auto& key : measureKeys) {
				if (key.contains("FeretDiameter") || key.contains("Length")) {
					horizontalHeader << QString(u8"%1(\u00b5m)").arg(key);
				}else if (key.contains("SurfaceArea") || key.contains("ProjectedArea")) {
					horizontalHeader << QString(u8"%1(\u00b5m\u00b2)").arg(key);
				} else if (key.contains("Volume")) {
					horizontalHeader << QString(u8"%1(\u00b5m\u00b3)").arg(key);
				} else if (key.contains("Drymass")) {
					horizontalHeader << QString(u8"%1(pg)").arg(key);
				} else if (key.contains("Concentration")) {
					horizontalHeader << QString(u8"%1(pg/\u00b5m\u00b3)").arg(key);
				} else {
					horizontalHeader << key;
				}
			}

			summaryTable->setColumnCount(horizontalHeader.count());
			summaryTable->setHorizontalHeaderLabels(horizontalHeader);
			QMap<int, int> parentCnt;
			for (auto i = 0; i < parentIdx.count(); i++) {
				const auto pIdx = parentIdx[i];
				if (!parentCnt.contains(pIdx))
					parentCnt[pIdx] = 0;
				parentCnt[pIdx]++;
			}
			summaryTable->setRowCount(singleLabelStats.count() * (parentCnt.keys().count() + 1));
			auto sortedParentKey = parentCnt.keys();
			std::sort(sortedParentKey.begin(), sortedParentKey.end());

			summary["All"] = QMap<QString, QMap<QString, double>>();
			for (auto key : sortedParentKey)
				summary[QString::number(key)] = QMap<QString, QMap<QString, double>>();

			for (auto i = 0; i < singleLabelStats.count(); i++) {
				const auto statKey = singleLabelStats[i];

				summary["All"][statKey] = QMap<QString, double>();
				summaryTable->setItem(i, 0, new QTableWidgetItem("All"));
				summaryTable->setItem(i, 1, new QTableWidgetItem(statKey));

				for (auto k = 0; k < sortedParentKey.count(); k++) {
					const auto key = sortedParentKey[k];
					summary[QString::number(key)][statKey] = QMap<QString, double>();
					summaryTable->setItem(i + singleLabelStats.count() * (k + 1), 0, new QTableWidgetItem(QString::number(key)));
					summaryTable->setItem(i + singleLabelStats.count() * (k + 1), 1, new QTableWidgetItem(statKey));
				}
			}

			for (auto measureIndex = 0; measureIndex < measureKeys.count(); measureIndex++) {
				const auto key = measureKeys[measureIndex];

				double sum = 0;
				double stdev = 0;
				double min = INT_MAX;
				double max = INT_MIN;

				QList<double> values;
				QMap<int, double> sumMap;
				QMap<int, double> stdevMap;
				QMap<int, double> minMap;
				QMap<int, double> maxMap;
				QMap<int, QList<double>> valuesMap;
				QMap<int, int> q10Map;
				QMap<int, int> q50Map;
				QMap<int, int> q90Map;

				for (auto j = 0; j < sortedParentKey.count(); j++) {
					const auto pk = sortedParentKey[j];
					sumMap[pk] = 0;
					stdevMap[pk] = 0;
					minMap[pk] = INT_MAX;
					maxMap[pk] = -INT_MAX;
					valuesMap[pk] = QList<double>();
					q10Map[pk] = static_cast<int>(parentCnt[pk] / 10.0);
					q50Map[pk] = static_cast<int>(parentCnt[pk] / 2.0);
					q90Map[pk] = static_cast<int>(parentCnt[pk] / 10.0 * 9.0);
				}

				for (auto j = 0; j < indexes.count(); j++) {
					auto value = measureMap[QString::number(j)].toMap()[key].toDouble();

					bool isValid = true;
					for (auto& elem : QStringList { "Mean", "SurfaceArea", "Volume", "ProjectedArea", "Sphericity","FeretDiameter","Length","Drymass", "Concentration", "Circularity" }) {
						if (key.contains(elem)) {
							if (value < 0) {
								isValid = false;
							}

							break;
						}
					}

					if (!isValid)
						continue;

					values << value;

					const auto pIdx = parentIdx[j];
					valuesMap[pIdx] << value;
					if (min > value)
						min = value;
					if (minMap[pIdx] > value)
						minMap[pIdx] = value;
					if (max < value)
						max = value;
					if (maxMap[pIdx] < value)
						maxMap[pIdx] = value;

					sum += value;
					sumMap[pIdx] += value;
				}

				std::sort(values.begin(), values.end());

				auto mean = 0.;
				auto q10 = -1;
				auto q50 = -1;
				auto q90 = -1;

				if (!values.isEmpty()) {
					auto valueCount = values.count();
					mean = sum / static_cast<double>(valueCount);
					for (auto j = 0; j < valueCount; j++)
						stdev += pow(values[j] - mean, 2);

					stdev = sqrt(stdev / indexes.count());

					q10 = static_cast<int>(valueCount / 10.0);
					q50 = static_cast<int>(valueCount / 2.0);
					q90 = static_cast<int>(valueCount / 10.0 * 9.0);
				}

				summary["All"]["Count"][key] = indexes.count();
				summary["All"]["Sum"][key] = sum;
				summary["All"]["Average"][key] = mean;
				summary["All"]["STD"][key] = stdev;
				summary["All"]["Min"][key] = min;
				summary["All"]["Max"][key] = max;
				summary["All"]["D10"][key] = q10 < 0 ? 0 : values[q10];
				summary["All"]["D50"][key] = q50 < 0 ? 0 : values[q50];
				summary["All"]["D90"][key] = q90 < 0 ? 0 : values[q90];

				auto countStr = QString::number(indexes.count(), 'f', 0);
				summaryTable->setItem(0, measureIndex + 2, new QTableWidgetItem(countStr));
				summaryTable->item(0, measureIndex + 2)->setTextAlignment(Qt::AlignRight);
				for (auto j = 0; j < singleLabelStats.count(); j++) {
					const auto statKey = singleLabelStats[j];
					const auto statValue = summary["All"][statKey][key];
					const auto precision = GetPrecision(key);
					auto cellValue = QString::number(statValue, 'f', precision);

					for (auto& elem : QStringList { "Mean", "SurfaceArea", "Volume", "ProjectedArea", "Sphericity","FeretDiameter","Length", "Drymass", "Concentration", "Circularity" }) {
						if (key.contains(elem)) {
							if (statValue < 0)
								cellValue = "N/A";

							break;
						}
					}

					summaryTable->setItem(j, measureIndex + 2, new QTableWidgetItem(cellValue));
					summaryTable->item(j, measureIndex + 2)->setTextAlignment(Qt::AlignRight);
				}

				for (auto j = 0; j < sortedParentKey.count(); j++) {
					const auto pk = sortedParentKey[j];
					const auto cnt = parentCnt[pk];
					const auto kmean = sumMap[pk] / cnt;
					for (auto k = 0; k < cnt; k++)
						stdevMap[pk] += pow(valuesMap[pk][k] - kmean, 2);

					stdevMap[pk] = sqrt(stdevMap[pk] / cnt);

					summary[QString::number(pk)]["Count"][key] = cnt;
					summary[QString::number(pk)]["Sum"][key] = sumMap[pk];
					summary[QString::number(pk)]["Average"][key] = kmean;
					summary[QString::number(pk)]["STD"][key] = stdevMap[pk];
					summary[QString::number(pk)]["Min"][key] = minMap[pk];
					summary[QString::number(pk)]["Max"][key] = maxMap[pk];
					summary[QString::number(pk)]["D10"][key] = valuesMap[pk][q10Map[pk]];
					summary[QString::number(pk)]["D50"][key] = valuesMap[pk][q50Map[pk]];
					summary[QString::number(pk)]["D90"][key] = valuesMap[pk][q90Map[pk]];

					const auto offset = (j + 1) * singleLabelStats.count();
					auto pcountStr = QString::number(parentCnt[pk], 'f', 0);
					summaryTable->setItem(offset, measureIndex + 2, new QTableWidgetItem(pcountStr));
					summaryTable->item(offset, measureIndex + 2)->setTextAlignment(Qt::AlignRight);

					for (auto k = 0; k < singleLabelStats.count(); k++) {
						const auto statKey = singleLabelStats[k];
						const auto statValue = summary[QString::number(pk)][statKey][key];
						const auto precision = GetPrecision(key);
						auto cellValue = QString::number(statValue, 'f', precision);

						for (auto& elem : QStringList { "Mean", "SurfaceArea", "Volume", "ProjectedArea", "Sphericity", "FeretDiameter","Length","Drymass", "Concentration", "Circularity" }) {
							if (key.contains(elem)) {
								if (statValue < 0)
									cellValue = "N/A";

								break;
							}
						}

						summaryTable->setItem(offset + k, measureIndex + 2, new QTableWidgetItem(cellValue));
						summaryTable->item(offset + k, measureIndex + 2)->setTextAlignment(Qt::AlignRight);
					}
				}
			}
		}
	}

	auto Report::Impl::ShowSummary() -> void {
		summaryTable->show();
		summaryTable->raise();
	}

	auto Report::Impl::ResetOrder() -> void {
		const auto text = ui.measureComboBox->currentText();
		if (!dataMap.contains(text))
			return;

		SetData(dataMap[text]);
		SetSummaryData(dataMap[text]);
	}

	Report::Report(QWidget* parent) : QWidget(parent), IPresenter(), d { new Impl } {
		d->ui.setupUi(this);

		d->summaryTable = std::make_shared<QTableWidget>(nullptr);
		d->summaryTable->setMinimumSize(QSize(600, 600));
		if (QFile file(":/Stylesheet/CellAnalyzer.qss"); file.open(QIODevice::ReadOnly))
			d->summaryTable->setStyleSheet(file.readAll());
		d->summaryTable->setWindowTitle("Default Summary");
		auto summarytableBlocker = QSignalBlocker(d->summaryTable.get());
		d->ui.tableWidget->setSelectionMode(QAbstractItemView::NoSelection);
		d->ui.tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
		d->ui.tableWidget->setSortingEnabled(true);
		d->summaryTable->setSortingEnabled(true);

		connect(d->ui.measureComboBox, &QComboBox::currentTextChanged, [=](const QString& text) {
			if (!d->dataMap.contains(text))
				return;
			if (false == d->curDataID.isEmpty()) {
				emit sigHighlight(d->curDataID, -1);
			}
			d->ui.tableWidget->blockSignals(true);
			d->ui.tableWidget->setSelectionMode(QAbstractItemView::NoSelection);
			d->ui.tableWidget->clearSelection();
			d->ui.tableWidget->blockSignals(false);
			d->ui.highlightBtn->blockSignals(true);
			d->ui.highlightBtn->setChecked(false);
			d->ui.highlightBtn->blockSignals(false);
			d->curDataID = text;
			d->SetData(d->dataMap[text]);
			d->SetSummaryData(d->dataMap[text]);
		});

		connect(d->ui.summaryButton, &QPushButton::clicked, [=]() {
			d->ShowSummary();
		});

		connect(d->ui.resetSortButton, &QPushButton::clicked, [=]() {
			d->ResetOrder();
		});
		connect(d->ui.highlightBtn, &QPushButton::clicked, [=]() {
			if (d->ui.highlightBtn->isChecked()) {
				d->ui.tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
				if (d->ui.tableWidget->rowCount() > 0) {
					d->ui.tableWidget->selectRow(0);
				}
			} else {
				d->ui.tableWidget->setSelectionMode(QAbstractItemView::NoSelection);
				d->ui.tableWidget->clearSelection();

				emit sigHighlight(d->curDataID, -1);
			}
		});
		connect(d->ui.tableWidget, &QTableWidget::itemSelectionChanged, [=]() {
			if (false == d->ui.highlightBtn->isChecked()) {
				return;
			}
			if (const auto* curItem = d->ui.tableWidget->currentItem()) {
				const auto rowIdx = curItem->row();
				const auto colIdx = d->labelHeaderIdx;

				const auto labelIdx = d->ui.tableWidget->item(rowIdx, colIdx)->text().toInt();

				emit sigHighlight(d->curDataID, labelIdx);
			}
		});
	}

	auto Report::OnHighlight(int label) -> void {
		const auto colIdx = d->labelHeaderIdx;
		for (auto i = 0; i < d->ui.tableWidget->rowCount(); i++) {
			const auto labelIdx = d->ui.tableWidget->item(i, colIdx)->text().toInt();
			if (labelIdx == label) {
				d->ui.tableWidget->selectRow(i);
				return;
			}
		}
	}

	Report::~Report() { }

	auto Report::SetTitle(const QString& title) -> void {
		setWindowTitle(title);
	}

	auto Report::GetWindowList() const -> ViewList {
		return {};
	}

	auto Report::IsPrimary(const ViewPtr& window) const -> bool {
		return true;
	}

	auto Report::IsAcceptable(const DataPtr& data) const -> bool {
		return data->GetFlags() & d->dataflag;
	}

	auto Report::GetDataList() const -> QStringList {
		return d->dataMap.keys();
	}

	auto Report::GetData(const QString& name) const -> DataPtr {
		if (d->dataMap.contains(name))
			return d->dataMap[name];
		return {};
	}

	auto Report::GetName(const DataPtr& data) const -> QString {
		return d->dataMap.key(data);
	}

	auto Report::AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool {
		if (!std::dynamic_pointer_cast<IMeasure>(data))
			return false;
		auto prevData = d->dataMap[name];
		d->dataMap[name] = data;
		d->flMap[name] = portName == "FL";				

		if (prevData != data) {
			d->SetData(data);
			d->SetSummaryData(data);
		}

		if (d->ui.measureComboBox->findText(name) < 0) {
			d->ui.measureComboBox->addItem(name);
			d->ui.measureComboBox->setCurrentText(name);
		}

		return true;
	}

	auto Report::RenameData(const DataPtr& data, const QString& name) -> bool {
		const auto prevName = d->dataMap.key(data);
		if (prevName.isEmpty())
			return false;

		d->dataMap.remove(prevName);
		d->dataMap[name] = data;
		const auto prevFL = d->flMap[name];
		d->flMap.remove(prevName);
		d->flMap[name] = prevFL;

		const auto comboBoxBlocker = QSignalBlocker(d->ui.measureComboBox);
		const auto itemIndex = d->ui.measureComboBox->findText(prevName);
		d->ui.measureComboBox->removeItem(itemIndex);
		d->ui.measureComboBox->insertItem(itemIndex, name);

		return true;
	}

	auto Report::RemoveData(const QString& name) -> void {
		d->dataMap.remove(name);
		d->flMap.remove(name);

		const auto comboBoxBlocker = QSignalBlocker(d->ui.measureComboBox);
		if (d->ui.measureComboBox->currentText() == name) {
			d->ui.tableWidget->clearContents();
			d->ui.tableWidget->setRowCount(0);
			d->ui.tableWidget->setColumnCount(0);

			d->summaryTable->clearContents();
			d->summaryTable->setRowCount(0);
			d->summaryTable->setColumnCount(0);

			d->ui.measureComboBox->setCurrentIndex(-1);
		}

		const auto itemIndex = d->ui.measureComboBox->findText(name);
		d->ui.measureComboBox->removeItem(itemIndex);
	}

	auto Report::ClearData() -> void {
		d->dataMap.clear();
		d->flMap.clear();

		const auto comboBoxBlocker = QSignalBlocker(d->ui.measureComboBox);
		d->ui.measureComboBox->clear();

		d->ui.tableWidget->clearContents();
		d->ui.tableWidget->setRowCount(0);
		d->ui.tableWidget->setColumnCount(0);

		d->summaryTable->clearContents();
		d->summaryTable->setRowCount(0);
		d->summaryTable->setColumnCount(0);
	}

	auto Report::Capture(const QString& filepath) -> void { }
}
