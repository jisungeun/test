﻿#pragma once

#include <QList>

#include <TCFMetaReader.h>

namespace CellAnalyzer::Project::View::WorksetEditor {
	// TODO: delete Fl2d, Fl3d flag.
	enum class Modality {
		None = 0,

		Ht2d = 0x0001,
		Ht3d = 0x0002,

		Fl2dCh1 = 0x0010,
		Fl2dCh2 = 0x0020,
		Fl2dCh3 = 0x0040,
		Fl2dChAll = Fl2dCh1 | Fl2dCh2 | Fl2dCh3,
		Fl2d = 0x0080,

		Fl3dCh1 = 0x0100,
		Fl3dCh2 = 0x0200,
		Fl3dCh3 = 0x0400,
		Fl3dChAll = Fl3dCh1 | Fl3dCh2 | Fl3dCh3,
		Fl3d = 0x0800,

		Bf = 0x1000,

		All = Ht2d | Ht3d | Fl2dChAll | Fl3dChAll | Bf
	};

	Q_DECLARE_FLAGS(Modalities, Modality)
	Q_DECLARE_OPERATORS_FOR_FLAGS(Modalities)

	using TimePoints = QList<double>;

	struct Config {
		enum class SelectionMode {
			Single,
			Multi,
		};

		SelectionMode selectionMode { SelectionMode::Multi };

		QMap<Modality, QStringList> sources;   // key: modality, value: source names
	};

	struct TimeSelectionOutput {
		QList<Modality> modalities;
		QList<int> times;
		QList<double> realTimes;
		QMap<Modality, QMap<int, int>> modalTimes;
	};

	struct ModalitySetting {
		bool ht2d { false };
		bool ht3d { false };
		bool fl2d { false };
		bool fl3d { false };
		bool bf { false };

		std::tuple<Modality, Modality, Modality> fl2dChannelSocket { Modality::None, Modality::None, Modality::None };
		std::tuple<Modality, Modality, Modality> fl3dChannelSocket { Modality::None, Modality::None, Modality::None };
	};

	struct TCFInfo {
		QString path;
		TC::IO::TCFMetaReader::Meta::Pointer meta;
		QList<int> timeIndexes;
		ModalitySetting modalities;
	};
}
