﻿#pragma once

#include <QDialog>
#include <QMap>
#include <QStringList>

#include <TCFMetaReader.h>

#include <IView.h>
#include "WorksetEditorDef.h"
#include "CellAnalyzer.Project.View.WorksetEditorExport.h"

namespace CellAnalyzer::Project::View::WorksetEditor {
	class CellAnalyzer_Project_View_WorksetEditor_API TimePointSettingDialog : public QDialog, public IView {
		Q_OBJECT
	public:
		explicit TimePointSettingDialog(const Config& config, QWidget* parent = nullptr);
		~TimePointSettingDialog() override;

		auto SetTCFs(const QStringList& tcfs) -> void;

		auto GetResult() -> QMap<QString, TimeSelectionOutput>;
		auto GetMeta(const QString& path) -> TC::IO::TCFMetaReader::Meta::Pointer;

	protected:
		void keyPressEvent(QKeyEvent* e) override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
