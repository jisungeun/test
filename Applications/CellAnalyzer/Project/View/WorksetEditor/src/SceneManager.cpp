#include <SliceGeneral.h>
#include <Slice3Channel.h>

#include <TCFReader.h>

#include "SceneManager.h"


using namespace Tomocube::Rendering::Image;


namespace CellAnalyzer::Project::View::WorksetEditor {
	struct SceneManager::Impl {
		SoRef<SoSwitch> sceneSwitch { nullptr };
		std::shared_ptr<SliceGeneral> htScene { nullptr };
		std::shared_ptr<Slice3Channel> flScene { nullptr };

		QString tcfPath;
		TC::IO::TCFMetaReader::Meta::Pointer meta { nullptr };

		std::tuple<double, double> origHtDataRange { 0, 0 };
		QList<std::tuple<double, double>> origFlDataRange;

		// VolumeData
		SoRef<SoVolumeData> ht { nullptr };
		QVector<SoRef<SoVolumeData>> fl = QVector<SoRef<SoVolumeData>>(3, nullptr);

		auto Init() -> void;
		auto calcFLIndexFromHT(int idx) -> int;
	};

	auto SceneManager::Impl::Init() -> void {
		sceneSwitch = new SoSwitch;

		htScene = std::make_shared<SliceGeneral>("ThumbnailHT");
		htScene->SetDataRange(0, 255);

		flScene = std::make_shared<Slice3Channel>("ThumbnailFL");
		flScene->SetColor(0, QColor(0, 0, 255));
		flScene->SetColor(1, QColor(0, 255, 0));
		flScene->SetColor(2, QColor(255, 0, 0));
		flScene->SetDataRange(0, 0, 255);
		flScene->SetDataRange(1, 0, 255);
		flScene->SetDataRange(2, 0, 255);

		sceneSwitch->addChild(htScene->GetRootSwitch());
		sceneSwitch->addChild(flScene->GetRootSwitch());

		ht = new SoVolumeData;
		for (auto& volume : fl)
			volume = new SoVolumeData;

		for (auto i = 0; i < 3; i++)
			origFlDataRange << std::make_tuple(0, 0);
	}

	auto SceneManager::Impl::calcFLIndexFromHT(int idx) -> int {
		const auto flRes = meta->data.data3DFL.resolutionZ;
		const auto offsetZ = meta->data.data3DFL.offsetZ - (meta->data.data3DFL.sizeZ * meta->data.data3DFL.resolutionZ / 2);

		auto phyZ = meta->data.data3D.resolutionZ * idx - offsetZ;
		auto idxFLz = static_cast<int>(phyZ / flRes);

		return idxFLz;
	}

	SceneManager::SceneManager() : d { new Impl } {
		d->Init();
	}

	SceneManager::~SceneManager() { }

	auto SceneManager::Clear() -> void {
		d->tcfPath = QString();
		d->meta = nullptr;
	}

	auto SceneManager::ClearScene() -> void {
		//d->htScene->Clear();
		//d->flScene->Clear();

		d->sceneSwitch->whichChild = SO_SWITCH_NONE;
	}

	auto SceneManager::ReadHT(int timestep) -> void {
		d->ht = nullptr;

		if (d->tcfPath.isEmpty() || d->meta == nullptr) {
			d->sceneSwitch->whichChild = SO_SWITCH_NONE;
			return;
		}

		IO::File::TCFReader reader(d->tcfPath);
		const auto htReader = reader.GetHT();
		if (htReader == nullptr) {
			d->sceneSwitch->whichChild = SO_SWITCH_NONE;
			return;
		}

		if (d->meta->thumbnail.ht.exist) {	// use thumbnail data
			if (const auto thumb = htReader->ReadThumbnail(timestep)) {
				const auto dim = thumb->GetSize();
				const auto res = thumb->GetResolution();

				const auto* buffer = const_cast<uint8_t*>(thumb->ReadAll());

				d->ht = new SoVolumeData;
				d->ht->data.setValue(SbVec3i32(dim.x, dim.y, 1), SbDataType::UNSIGNED_BYTE, 0, buffer, SoSFArray::COPY);
				d->ht->extent.setValue(-dim.x * res.x / 2, -dim.y * res.y / 2, -0.5, dim.x * res.x / 2, dim.y * res.y / 2, 0.5);
				
				d->htScene->SetVolume(d->ht.ptr());
				d->htScene->SetDataMinMax(0, 255);

				d->origHtDataRange = { 0, 255 };
			}

		} else if (d->meta->data.data2DMIP.exist) {	// use MIP data
			if (const auto mip = htReader->ReadMip(timestep)) {
				const auto dim = mip->GetSize();
				const auto res = mip->GetResolution();
				const auto dataRange = mip->GetRange();

				const auto* buffer = const_cast<uint16_t*>(mip->ReadAll());

				d->ht = new SoVolumeData;
				d->ht->data.setValue(SbVec3i32(dim.x, dim.y, 1), SbDataType::UNSIGNED_SHORT, 0, buffer, SoSFArray::COPY);
				d->ht->extent.setValue(-dim.x * res.x / 2, -dim.y * res.y / 2, -0.5, dim.x * res.x / 2, dim.y * res.y / 2, 0.5);
				
				d->htScene->SetVolume(d->ht.ptr());
				d->htScene->SetDataMinMax(dataRange.min, dataRange.max);

				d->origHtDataRange = { dataRange.min , dataRange.max };
			}
		} else if (d->meta->data.data3D.exist) {	// use 3D data
			if (const auto image = htReader->Read(timestep)) {
				const auto dim = image->GetSize();
				const auto res = image->GetResolution();

				const auto* buffer = const_cast<uint16_t*>(image->ReadAll());

				auto bufferIndex = dim.x * dim.y * (dim.z / 2);

				d->ht = new SoVolumeData;
				d->ht->data.setValue(SbVec3i32(dim.x, dim.y, 1), SbDataType::UNSIGNED_SHORT, 0, buffer + bufferIndex, SoSFArray::COPY);
				d->ht->extent.setValue(-dim.x * res.x / 2, -dim.y * res.y / 2, -0.5, dim.x * res.x / 2, dim.y * res.y / 2, 0.5);

				double min, max;
				d->ht->getMinMax(min, max);

				d->htScene->SetVolume(d->ht.ptr());
				d->htScene->SetDataMinMax(min, max);

				d->origHtDataRange = { min , max };
			}
		} else { }

		d->sceneSwitch->whichChild = d->ht.ptr() == nullptr ? SO_SWITCH_NONE : 0;
	}

	auto SceneManager::ReadFL(int timestep) -> void {
		d->fl = QVector<SoRef<SoVolumeData>>(3, nullptr);

		d->flScene->Reset();

		d->origFlDataRange.clear();
		for (auto i = 0; i < 3; i++) 
			d->origFlDataRange << std::make_tuple(0, 0);

		if (d->tcfPath.isEmpty() || d->meta == nullptr) {
			d->sceneSwitch->whichChild = SO_SWITCH_NONE;
			return;
		}

		const auto tileName = QString("%1").arg(timestep, 6, 10, QLatin1Char('0'));

		auto hasData = false;

		IO::File::TCFReader reader(d->tcfPath);
		const auto flReader = reader.GetFL();
		if (flReader == nullptr) {
			d->sceneSwitch->whichChild = SO_SWITCH_NONE;
			return;
		}

		if (d->meta->thumbnail.fl.exist) {	// use thumbnail data
			if (const auto thumb = flReader->ReadThumbnail(timestep)) {
				const auto dim = thumb->GetSize();
				const auto res = thumb->GetResolution();

				const auto* buffer = const_cast<uint8_t*>(thumb->ReadAll());

				for (auto i = 0; i < d->fl.count(); i++) {
					d->fl[i] = new SoVolumeData;
					d->fl[i]->data.setValue(SbVec3i32(dim.x, dim.y, 1), SbDataType::UNSIGNED_BYTE, 0, buffer + dim.x * dim.y * i, SoSFArray::COPY);
					d->fl[i]->extent.setValue(-dim.x * res.x / 2, -dim.y * res.y / 2, -0.5, dim.x * res.x / 2, dim.y * res.y / 2, 0.5);

					d->flScene->SetVolume(i, d->fl[i].ptr(), false);
					d->flScene->SetDataMinMax(i, 0, 255);

					d->origFlDataRange[i] = { 0, 255 };
				}

				d->flScene->SetColor(0, QColor(255, 0, 0));
				d->flScene->SetColor(1, QColor(0, 255, 0));
				d->flScene->SetColor(2, QColor(0, 0, 255));

				hasData = true;
			}
		} else if (d->meta->data.data2DFLMIP.exist) {	// use MIP data
			for (auto i = 0; i < d->fl.count(); i++) {
				if (!flReader->GetChannelList().contains(i)) continue;

				if (const auto mip = flReader->ReadMip(i, timestep)) {
					const auto dim = mip->GetSize();
					const auto res = mip->GetResolution();
					const auto dataRange = mip->GetRange();

					const auto* buffer = const_cast<uint16_t*>(mip->ReadAll());

					d->fl[i] = new SoVolumeData;
					d->fl[i]->data.setValue(SbVec3i32(dim.x, dim.y, 1), SbDataType::UNSIGNED_SHORT, 0, buffer, SoSFArray::COPY);
					d->fl[i]->extent.setValue(-dim.x * res.x / 2, -dim.y * res.y / 2, -0.5, dim.x * res.x / 2, dim.y * res.y / 2, 0.5);
					
					d->flScene->SetVolume(i, d->fl[i].ptr(), false);
					d->flScene->SetDataMinMax(i, dataRange.min, dataRange.max);

					const auto color = flReader->GetChannelColor(i);
					d->flScene->SetColor(i, QColor(color.red, color.green, color.blue));

					d->origFlDataRange[i] = { dataRange.min , dataRange.max };

					hasData = true;
				}
			}
		} else if (d->meta->data.data3DFL.exist) {	// use 3D data
			for (auto i = 0; i < d->fl.count(); i++) {
				if (!flReader->GetChannelList().contains(i)) continue;

				if (const auto image = flReader->Read(i, timestep)) {
					const auto dim = image->GetSize();
					const auto res = image->GetResolution();

					const auto* buffer = const_cast<uint16_t*>(image->ReadAll());

					auto bufferIndex = dim.x * dim.y * (dim.z / 2);

					d->fl[i] = new SoVolumeData;
					d->fl[i]->data.setValue(SbVec3i32(dim.x, dim.y, 1), SbDataType::UNSIGNED_SHORT, 0, buffer + bufferIndex, SoSFArray::COPY);
					d->fl[i]->extent.setValue(-dim.x * res.x / 2, -dim.y * res.y / 2, -0.5, dim.x * res.x / 2, dim.y * res.y / 2, 0.5);
					
					d->flScene->SetVolume(i, d->fl[i].ptr(), false);

					const auto min = d->meta->data.data3DFL.minIntensityList[i][timestep];
					const auto max = d->meta->data.data3DFL.maxIntensityList[i][timestep];

					d->flScene->SetDataMinMax(i, min, max);

					const auto color = flReader->GetChannelColor(i);
					d->flScene->SetColor(i, QColor(color.red, color.green, color.blue));

					d->origFlDataRange[i] = { min , max };

					hasData = true;
				}
			}
		}

		d->sceneSwitch->whichChild = hasData ? 1 : SO_SWITCH_NONE;
	}

	auto SceneManager::SetTCFPath(const QString& path) -> void {
		d->tcfPath = path;
	}

	auto SceneManager::GetTCFPath() const -> QString {
		return d->tcfPath;
	}

	auto SceneManager::SetMeta(TC::IO::TCFMetaReader::Meta::Pointer meta) -> void {
		d->meta = meta;
	}

	auto SceneManager::SetDataRange(int min, int max) -> void {
		// rescale 0 ~ 100 to current volume data range
		const auto rescale = [=](double newMin, double newMax, double value) -> double {
			const auto oldMin = 0;
			const auto oldMax = 100;

			return (newMax - newMin) / (oldMax - oldMin) * (value - oldMin) + newMin;
		};

		if (d->sceneSwitch->whichChild.getValue() == 0) {
			auto [newRangeMin, newRangeMax] = d->origHtDataRange;
			d->htScene->SetDataMinMax(rescale(newRangeMin, newRangeMax, min), rescale(newRangeMin, newRangeMax, max));
		} else {
			for (auto i = 0; i < d->origFlDataRange.count(); i++) {
				auto [newRangeMin, newRangeMax] = d->origFlDataRange[i];
				d->flScene->SetDataMinMax(i, rescale(newRangeMin, newRangeMax, min), rescale(newRangeMin, newRangeMax, max));
			}
		}
	}

	auto SceneManager::GetSceneGraph() -> SoSwitch* {
		return d->sceneSwitch.ptr();
	}
}
