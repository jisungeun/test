﻿#include <QVBoxLayout>
#include <QSplitter>
#include <QCheckBox>
#include <QFileInfo>
#include <QKeyEvent>
#include <QMessageBox>

#include "TimePointSettingControl.h"
#include "ModalitySettingWidget.h"
#include "TCFListWidget.h"
#include "ThumbnailWidget.h"
#include "TimePointTableWidget.h"
#include "WorksetEditorDialog.h"

#include "PointSettingWidget.h"
#include "ui_WorksetEditorDialog.h"

namespace CellAnalyzer::Project::View::WorksetEditor {
	struct TimePointSettingDialog::Impl {
		TimePointSettingControl control;

		Ui::TimePointSettingDialog ui;
		std::unique_ptr<ModalitySettingWidget> modalitySetting { nullptr };
		std::unique_ptr<TCFListWidget> tcfList { nullptr };
		std::unique_ptr<ThumbnailWidget> thumbnailWidget { nullptr };
		std::unique_ptr<TimePointTableWidget> timepointTable { nullptr };
		std::unique_ptr<PointSettingWidget> pointSettingWidget { nullptr };
		QTabWidget* infoTabWidget { nullptr };

		QCheckBox* tableTypeCheckbox { nullptr };

		auto InitUI() -> void;
		auto UpdateTCFList() -> void;
		auto SetCurrentTCF(const QString& tcfId) -> void;
		auto FiltersTimepoints(const QString& tcfId, ModalitySetting modalities) -> QMap<Modality, TimePoints>;
		auto GetTimeCounts(const QString& tcfId) -> QMap<Modality, int>;
	};

	auto TimePointSettingDialog::Impl::InitUI() -> void {
		modalitySetting = std::make_unique<ModalitySettingWidget>();
		tcfList = std::make_unique<TCFListWidget>();

		const auto leftSideLayout = new QVBoxLayout;
		leftSideLayout->setContentsMargins(0, 0, 0, 0);
		leftSideLayout->setSpacing(12);
		leftSideLayout->addWidget(modalitySetting.get());
		leftSideLayout->addWidget(tcfList.get(), 1);

		const auto leftSideWidget = new QWidget;
		leftSideWidget->setLayout(leftSideLayout);

		thumbnailWidget = std::make_unique<ThumbnailWidget>();

		tableTypeCheckbox = new QCheckBox(tr("Show Valid Time Points Only"));
		tableTypeCheckbox->setCheckState(Qt::Checked);
		tableTypeCheckbox->setEnabled(false);

		timepointTable = std::make_unique<TimePointTableWidget>();

		const auto timeTableLayout = new QVBoxLayout;
		timeTableLayout->setContentsMargins(0, 0, 0, 0);
		timeTableLayout->addWidget(tableTypeCheckbox, 0);
		timeTableLayout->addWidget(timepointTable.get(), 1);

		const auto timeTableWidget = new QWidget;
		timeTableWidget->setLayout(timeTableLayout);

		infoTabWidget->setStyleSheet("QTabWidget::pane { border-top: 0px solid #333; }"
									"QTabBar::tab { background-color: #29292B; }"
									"QTabBar::tab:selected { background-color: #38383B; }"
									"QTabWidget::tab-bar { background-color: #29292B; }"
									);
		infoTabWidget->addTab(thumbnailWidget.get(), tr("Thumbnail"));
		infoTabWidget->addTab(timeTableWidget, tr("Info"));

		pointSettingWidget = std::make_unique<PointSettingWidget>(control.GetConfig().selectionMode);

		const auto rightSideLayout = new QVBoxLayout;
		rightSideLayout->setContentsMargins(0, 0, 0, 0);
		rightSideLayout->setSpacing(12);
		rightSideLayout->addWidget(infoTabWidget, 1);
		rightSideLayout->addWidget(pointSettingWidget.get());
		rightSideLayout->addSpacerItem(new QSpacerItem(1, 1, QSizePolicy::Preferred, QSizePolicy::Expanding));

		const auto rightSideWidget = new QWidget;
		rightSideWidget->setLayout(rightSideLayout);
		rightSideWidget->setFixedWidth(480);

		const auto layout = new QHBoxLayout;
		layout->addWidget(leftSideWidget, 1);
		layout->addWidget(rightSideWidget);

		ui.centralWidget->setLayout(layout);

		ui.applyButton->setEnabled(false);
	}

	auto TimePointSettingDialog::Impl::UpdateTCFList() -> void {
		tcfList->Clear();

		const auto IsValidGroup = [=](const QMap<Modality, TimePoints>& group) -> bool {
			const auto modalitySetting = control.GetModalitySetting();

			if (modalitySetting.ht2d && group.find(Modality::Ht2d) == group.end())
				return false;
			if (modalitySetting.ht3d && group.find(Modality::Ht3d) == group.end())
				return false;

			if (modalitySetting.fl2d) {
				auto [socket1, socket2, socket3] = modalitySetting.fl2dChannelSocket;
				if (socket1 != Modality::None && group.find(socket1) == group.end())
					return false;
				if (socket2 != Modality::None && group.find(socket2) == group.end())
					return false;
				if (socket3 != Modality::None && group.find(socket3) == group.end())
					return false;
			}

			if (modalitySetting.fl3d) {
				auto [socket1, socket2, socket3] = modalitySetting.fl3dChannelSocket;
				if (socket1 != Modality::None && group.find(socket1) == group.end())
					return false;
				if (socket2 != Modality::None && group.find(socket2) == group.end())
					return false;
				if (socket3 != Modality::None && group.find(socket3) == group.end())
					return false;
			}

			return true;
		};

		const auto groups = control.GetGroups();
		auto groupIt = QMapIterator(groups);

		QMap<QString, TCFListItem> invalidTcfs;

		while (groupIt.hasNext()) {
			groupIt.next();

			auto id = groupIt.key();

			auto times = control.GetGroupTimePoints(id);
			if (times == std::nullopt)
				continue;

			if (!IsValidGroup(*times)) {
				for (auto tcfId : groupIt.value()) {
					auto tcfInfo = control.GetTCFInfo(tcfId);
					if (tcfInfo == std::nullopt)
						continue;

					invalidTcfs.insert(tcfId, { QFileInfo(tcfInfo->path).fileName().chopped(4), GetTimeCounts(tcfId) });
				}

				continue;
			}

			QMap<Modality, int> timeCounts;
			auto timeIt = QMapIterator(times.value());
			while (timeIt.hasNext()) {
				timeIt.next();
				timeCounts.insert(timeIt.key(), timeIt.value().count());
			}

			QMap<QString, TCFListItem> tcfs;
			for (auto tcfId : groupIt.value()) {
				auto tcfInfo = control.GetTCFInfo(tcfId);
				if (tcfInfo == std::nullopt)
					continue;

				tcfs.insert(tcfId, { QFileInfo(tcfInfo->path).fileName().chopped(4), GetTimeCounts(tcfId) });
			}

			tcfList->AddGroup(id, timeCounts, tcfs);
		}

		if (!invalidTcfs.isEmpty()) {
			tcfList->AddGroup(-1 /* invalid group id*/, {}, invalidTcfs);
		}
	}

	auto TimePointSettingDialog::Impl::SetCurrentTCF(const QString& tcfId) -> void {
		ui.applyButton->setEnabled(false);

		control.SetLastSelectedTCFId(tcfId);

		const auto entireTimepoints = control.GetTimePoints(tcfId);
		const auto modalities = control.GetModalitySetting();
		const auto filteredTimepoints = FiltersTimepoints(tcfId, modalities);

		timepointTable->SetTimePoints(entireTimepoints, filteredTimepoints);
		timepointTable->SetFiltering(tableTypeCheckbox->checkState() == Qt::Checked);

		pointSettingWidget->SetTimePoints(entireTimepoints);
		pointSettingWidget->SetFilteredTimePoints(filteredTimepoints);

		const auto timeIndexes = control.GetTimeIndexes(tcfId);
		if (!timeIndexes.isEmpty()) {
			pointSettingWidget->SetTimeIndexes(timeIndexes);
			ui.applyButton->setEnabled(true);
		}
	}

	auto TimePointSettingDialog::Impl::FiltersTimepoints(const QString& tcfId, ModalitySetting settings) -> QMap<Modality, TimePoints> {
		QMap<Modality, TimePoints> result;

		const auto entireTimes = control.GetTimePoints(tcfId);

		// convert ModalitySetting to Modalities
		QList<Modality> modalities;
		if (settings.ht2d)
			modalities << Modality::Ht2d;
		if (settings.ht3d)
			modalities << Modality::Ht3d;
		if (settings.bf)
			modalities << Modality::Bf;
		if (settings.fl2d) {
			auto [socket1, socket2, socket3] = settings.fl2dChannelSocket;
			if (socket1 != Modality::None)
				modalities << socket1;
			if (socket2 != Modality::None)
				modalities << socket2;
			if (socket3 != Modality::None)
				modalities << socket3;
		}
		if (settings.fl3d) {
			auto [socket1, socket2, socket3] = settings.fl3dChannelSocket;
			if (socket1 != Modality::None)
				modalities << socket1;
			if (socket2 != Modality::None)
				modalities << socket2;
			if (socket3 != Modality::None)
				modalities << socket3;
		}

		// get filtered time points
		TimePoints allTimes;
		for (auto modal : modalities) {
			result.insert(modal, {});

			for (auto time : entireTimes.value(modal))
				if (!allTimes.contains(time))
					allTimes << time;
		}

		TimePoints resultTimes;
		for (auto time : allTimes) {
			bool hasAll = true;

			for (auto modal : modalities) {
				if (!entireTimes.value(modal).contains(time)) {
					hasAll = false;
					break;
				}
			}

			if (hasAll)
				resultTimes << time;
		}

		// set filtered times into selected modalities
		for (auto modal : modalities)
			result.insert(modal, resultTimes);

		return result;
	}

	auto TimePointSettingDialog::Impl::GetTimeCounts(const QString& tcfId) -> QMap<Modality, int> {
		const auto modalities = control.GetTCFModalities(tcfId);
		const auto timePoints = control.GetTimeIndexes(tcfId);

		const auto timeCount = timePoints.count();
		if (timeCount == 0)
			return QMap<Modality, int>();

		QMap<Modality, int> result;

		if (modalities.ht2d)
			result.insert(Modality::Ht2d, timeCount);
		if (modalities.ht3d)
			result.insert(Modality::Ht3d, timeCount);

		if (modalities.fl2d) {
			auto [socket1, socket2, socket3] = modalities.fl2dChannelSocket;
			if (socket1 != Modality::None)
				result.insert(socket1, timeCount);
			if (socket2 != Modality::None)
				result.insert(socket2, timeCount);
			if (socket3 != Modality::None)
				result.insert(socket3, timeCount);
		}

		if (modalities.fl3d) {
			auto [socket1, socket2, socket3] = modalities.fl3dChannelSocket;
			if (socket1 != Modality::None)
				result.insert(socket1, timeCount);
			if (socket2 != Modality::None)
				result.insert(socket2, timeCount);
			if (socket3 != Modality::None)
				result.insert(socket3, timeCount);
		}

		return result;
	}

	TimePointSettingDialog::TimePointSettingDialog(const Config& config, QWidget* parent) : QDialog(parent, Qt::Dialog | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint), IView(), d { std::make_unique<Impl>() } {
		d->control.SetConfig(config);

		d->ui.setupUi(this);
		setMinimumSize(1200, 600);
		d->infoTabWidget = new QTabWidget;
		d->InitUI();

		setWindowTitle("Workset Editor");

		connect(d->infoTabWidget, &QTabWidget::currentChanged, [=](int index) {
			if (index == 0) {//thumbnail
				d->tableTypeCheckbox->setEnabled(false);
			} else if (index == 1) {//table
				d->tableTypeCheckbox->setEnabled(true);
			}
		});

		connect(d->modalitySetting.get(), &ModalitySettingWidget::sigModalityChanged, [=](ModalitySetting modalities) {
			const auto currentTcf = d->control.GetLastSelectedTCFId();

			d->control.SetModalitySetting(modalities);
			d->UpdateTCFList();

			d->timepointTable->SetTimePoints({}, {});
			d->thumbnailWidget->SetTCFs({});
			d->pointSettingWidget->SetTimePoints({});
			d->pointSettingWidget->SetFilteredTimePoints({});
		});

		connect(d->tcfList.get(), &TCFListWidget::sigTCFSelected, [=](const QString& tcfId) {
			d->modalitySetting->setEnabled(true);

			d->SetCurrentTCF(tcfId);

			QList<TCFInfo> tcfInfos;
			for (auto tcf : d->tcfList->GetSelectedItems()) {
				const auto info = d->control.GetTCFInfo(tcf);
				if (info == std::nullopt)
					continue;

				tcfInfos << *info;
			}

			d->thumbnailWidget->SetTCFs(tcfInfos);
		});

		connect(d->tableTypeCheckbox, &QCheckBox::stateChanged, [=](int state) {
			d->timepointTable->SetFiltering(state == Qt::Checked);
		});

		connect(d->pointSettingWidget.get(), &PointSettingWidget::sigTimePointsValidityChanged, [=](bool validity) {
			d->ui.applyButton->setEnabled(validity);
		});

		connect(d->ui.okButton, &QPushButton::clicked, [=]() {
			int notSetCount = 0;

			const auto currentModalitySetting = d->control.GetModalitySetting();
			auto tcfInfoIt = QMapIterator(d->control.GetAllTimePointInfo());
			while (tcfInfoIt.hasNext()) {
				tcfInfoIt.next();

				if (d->control.GetGroup(tcfInfoIt.key()) < 0)
					continue;

				if (tcfInfoIt.value().timeIndexes.isEmpty())
					notSetCount++;
			}

			if (notSetCount > 0) {
				QMessageBox warningMessage(this);
				warningMessage.setIcon(QMessageBox::Warning);
				warningMessage.setWindowTitle("Warning");
				warningMessage.setText(QString("Time points of %1 TCF files are not set.\nDo you want to proceed?").arg(notSetCount));
				warningMessage.addButton(tr("Confirm"), QMessageBox::AcceptRole);
				const auto cancelButton = warningMessage.addButton(tr("Cancel"), QMessageBox::RejectRole);

				warningMessage.exec();
				if (warningMessage.clickedButton() == cancelButton)
					return;
			}

			// apply current changes
			const auto tcfs = d->tcfList->GetSelectedItems();
			if (tcfs.isEmpty())
				reject();

			const auto modalities = d->modalitySetting->GetModalities();
			const auto timeIndexes = d->pointSettingWidget->GetSelectedTimeIndexes();

			for (auto tcf : tcfs) {
				d->control.SetTCFModalities(tcf, modalities);
				d->control.SetTimeIndexes(tcf, timeIndexes);

				d->tcfList->SetTimePointsState(tcf, d->GetTimeCounts(tcf));
			}

			// close window
			accept();
		});

		connect(d->ui.applyButton, &QPushButton::clicked, [=]() {
			const auto tcfs = d->tcfList->GetSelectedItems();

			const auto modalities = d->modalitySetting->GetModalities();
			const auto timeIndexes = d->pointSettingWidget->GetSelectedTimeIndexes();

			for (auto tcf : tcfs) {
				d->control.SetTCFModalities(tcf, modalities);
				d->control.SetTimeIndexes(tcf, timeIndexes);

				d->tcfList->SetTimePointsState(tcf, d->GetTimeCounts(tcf));
			}
		});

		connect(d->ui.cancelButton, &QPushButton::clicked, [=]() {
			reject();
		});

		d->modalitySetting->SetConfig(config);
		d->tcfList->SetConfig(config);
	}

	TimePointSettingDialog::~TimePointSettingDialog() { }

	auto TimePointSettingDialog::SetTCFs(const QStringList& tcfs) -> void {
		d->control.RegisterTCFs(tcfs);

		// update GUIs
		const auto tcfIds = d->control.GetTCFIds();
		if (!tcfIds.isEmpty()) {
			const auto modalitySetting = d->control.GetModalitySetting();
			const auto entireTimepoints = d->control.GetTimePoints(tcfIds.first());
			const auto filteredTimepoints = d->FiltersTimepoints(tcfIds.first(), modalitySetting);
		}

		d->UpdateTCFList();
	}

	auto TimePointSettingDialog::GetResult() -> QMap<QString, TimeSelectionOutput> {
		QMap<QString, TimeSelectionOutput> result;  // key: tcf path

		auto it = QMapIterator(d->control.GetAllTimePointInfo());
		while (it.hasNext()) {
			it.next();

			auto info = it.value();

			TimeSelectionOutput output;

			if (info.modalities.ht2d) {
				output.modalities << Modality::Ht2d;
				output.modalTimes[Modality::Ht2d] = QMap<int, int>();
			}
			if (info.modalities.ht3d) {
				output.modalities << Modality::Ht3d;
				output.modalTimes[Modality::Ht3d] = QMap<int, int>();
			}
			if (info.modalities.fl2d) {
				auto [socket1, socket2, socket3] = info.modalities.fl2dChannelSocket;

				if (socket1 == Modality::Fl2dCh1) {
					output.modalities << socket1;
					output.modalTimes[Modality::Fl2dCh1] = QMap<int, int>();
				} else if (socket1 == Modality::Fl2dCh2) {
					output.modalities << socket1;
					output.modalTimes[Modality::Fl2dCh2] = QMap<int, int>();
				} else if (socket1 == Modality::Fl2dCh3) {
					output.modalities << socket1;
					output.modalTimes[Modality::Fl2dCh3] = QMap<int, int>();
				}

				if (socket2 == Modality::Fl2dCh1) {
					output.modalities << socket2;
					output.modalTimes[Modality::Fl2dCh1] = QMap<int, int>();
				} else if (socket2 == Modality::Fl2dCh2) {
					output.modalities << socket2;
					output.modalTimes[Modality::Fl2dCh2] = QMap<int, int>();
				} else if (socket2 == Modality::Fl2dCh3) {
					output.modalities << socket2;
					output.modalTimes[Modality::Fl2dCh3] = QMap<int, int>();
				}

				if (socket3 == Modality::Fl2dCh1) {
					output.modalities << socket3;
					output.modalTimes[Modality::Fl2dCh1] = QMap<int, int>();
				} else if (socket3 == Modality::Fl2dCh2) {
					output.modalities << socket3;
					output.modalTimes[Modality::Fl2dCh2] = QMap<int, int>();
				} else if (socket3 == Modality::Fl2dCh3) {
					output.modalities << socket3;
					output.modalTimes[Modality::Fl2dCh3] = QMap<int, int>();
				}
			}
			if (info.modalities.fl3d) {
				auto [socket1, socket2, socket3] = info.modalities.fl3dChannelSocket;
				if (socket1 == Modality::Fl3dCh1) {
					output.modalities << socket1;
					output.modalTimes[Modality::Fl3dCh1] = QMap<int, int>();
				} else if (socket1 == Modality::Fl3dCh2) {
					output.modalities << socket1;
					output.modalTimes[Modality::Fl3dCh2] = QMap<int, int>();
				} else if (socket1 == Modality::Fl3dCh3) {
					output.modalities << socket1;
					output.modalTimes[Modality::Fl3dCh3] = QMap<int, int>();
				}

				if (socket2 == Modality::Fl3dCh1) {
					output.modalities << socket2;
					output.modalTimes[Modality::Fl3dCh1] = QMap<int, int>();
				} else if (socket2 == Modality::Fl3dCh2) {
					output.modalities << socket2;
					output.modalTimes[Modality::Fl3dCh2] = QMap<int, int>();
				} else if (socket2 == Modality::Fl3dCh3) {
					output.modalities << socket2;
					output.modalTimes[Modality::Fl3dCh3] = QMap<int, int>();
				}

				if (socket3 == Modality::Fl3dCh1) {
					output.modalities << socket3;
					output.modalTimes[Modality::Fl3dCh1] = QMap<int, int>();
				} else if (socket3 == Modality::Fl3dCh2) {
					output.modalities << socket3;
					output.modalTimes[Modality::Fl3dCh2] = QMap<int, int>();
				} else if (socket3 == Modality::Fl3dCh3) {
					output.modalities << socket3;
					output.modalTimes[Modality::Fl3dCh3] = QMap<int, int>();
				}
			}
			if (info.modalities.bf) {
				output.modalities << Modality::Bf;
				output.modalTimes[Modality::Bf] = QMap<int, int>();
			}

			output.times = info.timeIndexes;

			QList<double> realTimes;
			for (const auto time : info.timeIndexes) {
				const auto realTime = info.meta->data.total_time[time - 1];
				realTimes.append(realTime);
				if (output.modalTimes.contains(Modality::Ht2d)) {
					const auto modalTime = info.meta->data.data2DMIP.timePoints.indexOf(realTime);
					output.modalTimes[Modality::Ht2d][time] = modalTime;
				}
				if (output.modalTimes.contains(Modality::Ht3d)) {
					const auto modalTime = info.meta->data.data3D.timePoints.indexOf(realTime);
					output.modalTimes[Modality::Ht3d][time] = modalTime;
				}
				if (output.modalTimes.contains(Modality::Fl2dCh1)) {
					const auto modalTime = info.meta->data.data2DFLMIP.timePoints[0].indexOf(realTime);
					output.modalTimes[Modality::Fl2dCh1][time] = modalTime;
				}
				if (output.modalTimes.contains(Modality::Fl2dCh2)) {
					const auto modalTime = info.meta->data.data2DFLMIP.timePoints[1].indexOf(realTime);
					output.modalTimes[Modality::Fl2dCh2][time] = modalTime;
				}
				if (output.modalTimes.contains(Modality::Fl2dCh3)) {
					const auto modalTime = info.meta->data.data2DFLMIP.timePoints[2].indexOf(realTime);
					output.modalTimes[Modality::Fl2dCh3][time] = modalTime;
				}
				if (output.modalTimes.contains(Modality::Fl3dCh1)) {
					const auto modalTime = info.meta->data.data3DFL.timePoints[0].indexOf(realTime);
					output.modalTimes[Modality::Fl3dCh1][time] = modalTime;
				}
				if (output.modalTimes.contains(Modality::Fl3dCh2)) {
					const auto modalTime = info.meta->data.data3DFL.timePoints[1].indexOf(realTime);
					output.modalTimes[Modality::Fl3dCh2][time] = modalTime;
				}
				if (output.modalTimes.contains(Modality::Fl3dCh3)) {
					const auto modalTime = info.meta->data.data3DFL.timePoints[2].indexOf(realTime);
					output.modalTimes[Modality::Fl3dCh3][time] = modalTime;
				}
				if (output.modalTimes.contains(Modality::Bf)) {
					const auto modalTime = info.meta->data.dataBF.timePoints.indexOf(realTime);
					output.modalTimes[Modality::Bf][time] = modalTime;
				}
			}

			output.realTimes = realTimes;

			result.insert(info.path, output);
		}

		return result;
	}

	auto TimePointSettingDialog::GetMeta(const QString& path) -> TC::IO::TCFMetaReader::Meta::Pointer {
		auto it = QMapIterator(d->control.GetAllTimePointInfo());
		while (it.hasNext()) {
			it.next();

			auto info = it.value();
			if (info.path == path) {
				return info.meta;
			}
		}
		return nullptr;
	}

	void TimePointSettingDialog::keyPressEvent(QKeyEvent* e) {
		if (e->key() == Qt::Key_Escape)
			return;

		QDialog::keyPressEvent(e);
	}

}
