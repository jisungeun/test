﻿#pragma once

#include <QWidget>

#include "WorksetEditorDef.h"

namespace CellAnalyzer::Project::View::WorksetEditor {
	class ThumbnailWidget : public QWidget {
		Q_OBJECT
	public:
		explicit ThumbnailWidget(QWidget* parent = nullptr);
		~ThumbnailWidget() override;

		auto SetTCFs(const QList<TCFInfo>& infos) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
