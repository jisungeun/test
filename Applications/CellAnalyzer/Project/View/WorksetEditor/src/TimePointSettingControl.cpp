﻿#include "TimePointSettingControl.h"

#include <QMap>

namespace CellAnalyzer::Project::View::WorksetEditor {
    std::string_view Ht2dKey{"HT2D"};
    std::string_view Ht3dKey{"HT3D"};
    std::string_view Fl2dKey{"FL2D"};
    std::string_view Fl3dKey{"FL3D"};
    std::string_view BfKey{"BF"};
    std::string_view ChannelKey{"Channel"};
    std::string_view TimesKey{"Times"};

    using Group = QMap<QString, QVariantMap>;
    /*
     Group {
	    "HT2D": {
	        "Times": [0, 1200, 2400, ...]
	    },
	    "HT3D": {
			"Times": [0, 1200, 2400, ...]
	    },
	    "FL2D": {
			"0": {
	            "Channel": 0,
	            "Times": [0, 1200, 2400, ...]
	        },
	        "1": {
	            "Channel": 1,
	            "Times": [0, 1200, 2400, ...]
	        },
	        "2": {
	            "Channel": 2,
	            "Times": [0, 1200, 2400, ...]
	        }
	    },
	    ...
	}
     */

    struct TimePointSettingControl::Impl {
        QMap<QString, TCFInfo> tcfInfos;    // key: tcf id, value: tcf information

    	QMap<int, Group> groups;  // key: group id, value: group info map
        QMap<int, QStringList> groupMembers;   // key: group id, value: tcf id list

    	QString lastTCFId;
        Config config;

        ModalitySetting modalitySetting;

        auto UpdateGroup() -> void;
    };

    auto TimePointSettingControl::Impl::UpdateGroup() -> void {
        auto ConvertFLChannel = [](Modality modality) -> int {
            int ch = -1;

	        switch(modality) {
            case Modality::Fl2dCh1:
            case Modality::Fl3dCh1:
                ch = 0;
                break;
            case Modality::Fl2dCh2:
            case Modality::Fl3dCh2:
                ch = 1;
				break;
            case Modality::Fl2dCh3:
            case Modality::Fl3dCh3:
                ch = 2;
                break;
	        }

            return ch;
        };

        groups.clear();
        groupMembers.clear();

    	QMap<QString, QVariantMap> timePoints;

	    auto it = QMapIterator(tcfInfos);
        while (it.hasNext()) {
	        it.next();

            auto info = it.value().meta->data;
			auto tcfId = it.value().meta->common.uniqueID;

            Group newGroup;

        	if (info.data2DMIP.exist) newGroup.insert(Ht2dKey.data(), { {TimesKey.data(), QVariantList(info.data2DMIP.timePoints.begin(), info.data2DMIP.timePoints.end())} });
        	if (info.data3D.exist && info.data3D.sizeZ > 1) newGroup.insert(Ht3dKey.data(), { {TimesKey.data(), QVariantList(info.data3D.timePoints.begin(), info.data3D.timePoints.end())} });
            
            if (info.data2DFLMIP.exist) {
                std::sort(info.data2DFLMIP.channelList.begin(), info.data2DFLMIP.channelList.end());

                QVariantMap channels;
                for (auto ch : info.data2DFLMIP.channelList) {
	                const auto chTimePoints = info.data2DFLMIP.timePoints[ch];

                    auto chMap = QVariantMap{
                        { ChannelKey.data(), ch},
                        { TimesKey.data(), QVariantList(chTimePoints.begin(), chTimePoints.end())}
                    };

                    channels.insert(QString::number(ch), chMap);
                }

                newGroup.insert(Fl2dKey.data(), channels);
            }

            if (info.data3DFL.exist && info.data3DFL.sizeZ > 1) {
                std::sort(info.data3DFL.channelList.begin(), info.data3DFL.channelList.end());

                QVariantMap channels;
                for (auto ch : info.data3DFL.channelList) {
	                const auto chTimePoints = info.data3DFL.timePoints[ch];

                    auto chMap = QVariantMap{
                        { ChannelKey.data(), ch},
                        { TimesKey.data(), QVariantList(chTimePoints.begin(), chTimePoints.end())}
                    };

                    channels.insert(QString::number(ch), chMap);
                }

                newGroup.insert(Fl3dKey.data(), channels);
            }

            const auto groupKey = groups.key(newGroup, -1);
            if (groupKey < 0) {
                auto newGroupId = groups.keys().count();
	            groups.insert(newGroupId, newGroup);
                groupMembers.insert(newGroupId, {tcfId});
            } else {
	            auto tcfs = groupMembers[groupKey];
                groupMembers[groupKey] = tcfs << tcfId;
            }
        }
    }

    TimePointSettingControl::TimePointSettingControl() : d{std::make_unique<Impl>()} {}

	TimePointSettingControl::~TimePointSettingControl() {}

    auto TimePointSettingControl::RegisterTCFs(const QStringList& tcfPaths) -> void {
        d->tcfInfos.clear();

		TC::IO::TCFMetaReader reader;
        for (auto path : tcfPaths) {
        	const auto tcfMeta = reader.Read(path);
            if (tcfMeta == nullptr) continue;

        	TCFInfo info;
			info.meta = tcfMeta;
			info.path = path;
			info.modalities = d->modalitySetting;

        	d->tcfInfos.insert(info.meta->common.uniqueID, info);
        }

        d->UpdateGroup();
    }

    auto TimePointSettingControl::GetTCFIds() const -> QStringList {
	    return d->tcfInfos.keys();
    }

    auto TimePointSettingControl::GetGroup(const QString& tcfId) const -> int {
        auto it = QMapIterator(d->groupMembers);
        while (it.hasNext()) {
	        it.next();

            if (it.value().indexOf(tcfId) != -1) {
	            return it.key();
            }
        }

        return -1;
    }

    auto TimePointSettingControl::GetGroups() const -> QMap<int, QStringList> {
	    return d->groupMembers;
    }

    auto TimePointSettingControl::GetGroupTimePoints(int groupId) const -> std::optional<QMap<Modality, TimePoints>> {
        if (d->groups.find(groupId) == d->groups.end()) return {};

        QMap<Modality, TimePoints> times;

        const auto info = d->groups[groupId];

    	auto it = QMapIterator(info);
        while (it.hasNext()) {
	        it.next();

            auto modalityKey = it.key();
            auto data = it.value();

        	if (modalityKey == Fl2dKey.data() || modalityKey == Fl3dKey.data()) {
                for (auto ch = 0; ch < 3; ch++) {
                    const auto key = QString::number(ch);
	                if (data.find(key) == data.end()) continue;

                    const auto chData = data[key].toMap();
                    const auto channel = chData[ChannelKey.data()].toInt();

                    TimePoints modalityTimes;
                    for (auto time : chData[TimesKey.data()].toList())
						modalityTimes << time.toInt();

                    Modality modality{Modality::None};
	                if (modalityKey == Fl2dKey.data()) {
                        if (channel == 0) modality = Modality::Fl2dCh1;
                        else if (channel == 1) modality = Modality::Fl2dCh2;
                        else if (channel == 2) modality = Modality::Fl2dCh3;
                    } else if (modalityKey == Fl3dKey.data()) {
                        if (channel == 0) modality = Modality::Fl3dCh1;
                        else if (channel == 1) modality = Modality::Fl3dCh2;
                        else if (channel == 2) modality = Modality::Fl3dCh3;
                    }

                    if (modality != Modality::None)
                		times.insert(modality, modalityTimes);
                }
            } else {
                TimePoints modalityTimes;
	            for (auto time : data[TimesKey.data()].toList())
		            modalityTimes << time.toInt();

                Modality modality{Modality::None};
                if (modalityKey == Ht2dKey.data()) modality = Modality::Ht2d;
                else if (modalityKey == Ht3dKey.data()) modality = Modality::Ht3d;
                else if (modalityKey == BfKey.data()) modality = Modality::Bf;

                if (modality != Modality::None)
                	times.insert(modality, modalityTimes);
            }
        }

        return times;
    }

    auto TimePointSettingControl::GetTimePoints(const QString& tcfId) const -> QMap<Modality, TimePoints> {
        if (!d->tcfInfos.contains(tcfId)) return {};

        const auto meta = d->tcfInfos[tcfId].meta;

        QMap<Modality, TimePoints> timepoints;
        if (meta->data.data2DMIP.exist) timepoints.insert(Modality::Ht2d, meta->data.data2DMIP.timePoints);
        if (meta->data.data3D.exist) timepoints.insert(Modality::Ht3d, meta->data.data3D.timePoints);
        if (meta->data.data2DFLMIP.exist) {
            const auto channels = QVector(meta->data.data2DFLMIP.channelList.begin(), meta->data.data2DFLMIP.channelList.end());
            if (channels.contains(0)) timepoints.insert(Modality::Fl2dCh1, meta->data.data2DFLMIP.timePoints[0]);
            if (channels.contains(1)) timepoints.insert(Modality::Fl2dCh2, meta->data.data2DFLMIP.timePoints[1]);
            if (channels.contains(2)) timepoints.insert(Modality::Fl2dCh3, meta->data.data2DFLMIP.timePoints[2]);
        }
        if (meta->data.data3DFL.exist) {
	        const auto channels = QVector(meta->data.data3DFL.channelList.begin(), meta->data.data3DFL.channelList.end());
            if (channels.contains(0)) timepoints.insert(Modality::Fl3dCh1, meta->data.data3DFL.timePoints[0]);
            if (channels.contains(1)) timepoints.insert(Modality::Fl3dCh2, meta->data.data3DFL.timePoints[1]);
            if (channels.contains(2)) timepoints.insert(Modality::Fl3dCh3, meta->data.data3DFL.timePoints[2]);
        }
        if (meta->data.dataBF.exist) timepoints.insert(Modality::Bf, meta->data.dataBF.timePoints);

        return timepoints;
    }

    auto TimePointSettingControl::SetModalitySetting(ModalitySetting settings) -> void {
	    d->modalitySetting = settings;
    }

    auto TimePointSettingControl::GetModalitySetting() const -> ModalitySetting {
	    return d->modalitySetting;
    }

    auto TimePointSettingControl::GetTCFInfo(const QString& tcfId) const -> std::optional<TCFInfo> {
	    if (d->tcfInfos.find(tcfId) == d->tcfInfos.end()) return {};

        return d->tcfInfos[tcfId];
    }

    auto TimePointSettingControl::SetLastSelectedTCFId(const QString& tcfId) const -> void {
	    d->lastTCFId = tcfId;
    }

    auto TimePointSettingControl::GetLastSelectedTCFId() const -> QString {
	    return d->lastTCFId;
    }

    auto TimePointSettingControl::SetTCFModalities(const QString& tcfId, ModalitySetting setting) -> void {
        if (!d->tcfInfos.contains(tcfId)) return;

        auto info = d->tcfInfos[tcfId];
        info.modalities = setting;

        d->tcfInfos[tcfId] = info;
    }

    auto TimePointSettingControl::GetTCFModalities(const QString& tcfId) const -> ModalitySetting {
        if (!d->tcfInfos.contains(tcfId)) return {};
        return d->tcfInfos[tcfId].modalities;
    }

    auto TimePointSettingControl::SetTimeIndexes(const QString& tcfId, const QList<int>& indexes) const -> void {
        if (!d->tcfInfos.contains(tcfId)) return;

        auto info = d->tcfInfos[tcfId];
        info.timeIndexes = indexes;

        d->tcfInfos[tcfId] = info;
    }

    auto TimePointSettingControl::GetTimeIndexes(const QString& tcfId) const -> QList<int> {
    	if (!d->tcfInfos.contains(tcfId)) return {};
        return d->tcfInfos[tcfId].timeIndexes;
    }

    auto TimePointSettingControl::GetAllTimePointInfo() const -> QMap<QString, TCFInfo> {
        return d->tcfInfos;
    }

    auto TimePointSettingControl::SetConfig(const Config& config) -> void {
	    d->config = config;

        // init modalities
        ModalitySetting setting;
        if (config.sources.find(Modality::Ht2d) != config.sources.end()) setting.ht2d = true;
        if (config.sources.find(Modality::Ht3d) != config.sources.end()) setting.ht3d = true;
        if (config.sources.find(Modality::Bf) != config.sources.end()) setting.bf = true;

        if (config.sources.find(Modality::Fl2d) != config.sources.end()) {
            const auto fl2dCount = config.sources.value(Modality::Fl2d).count();

            auto socket = QVector(3, Modality::None);

            const auto fl2dChannels = QList({Modality::Fl2dCh1, Modality::Fl2dCh2, Modality::Fl2dCh3});
            for (auto i = 0; i < fl2dChannels.count(); i++) {
	            if (i < fl2dCount)
		            socket[i] = fl2dChannels.at(i);
	        }

            setting.fl2d = true;
            setting.fl2dChannelSocket = std::make_tuple(socket[0], socket[1], socket[2]);
        }

        if (config.sources.find(Modality::Fl3d) != config.sources.end()) {
            const auto fl3dCount = config.sources.value(Modality::Fl3d).count();

            auto socket = QVector(3, Modality::None);

            const auto fl3dChannels = QList({Modality::Fl3dCh1, Modality::Fl3dCh2, Modality::Fl3dCh3});
            for (auto i = 0; i < fl3dChannels.count(); i++) {
	            if (i < fl3dCount)
		            socket[i] = fl3dChannels.at(i);
	        }

            setting.fl3d = true;
            setting.fl3dChannelSocket = std::make_tuple(socket[0], socket[1], socket[2]);
        }

        d->modalitySetting = setting;
    }

    auto TimePointSettingControl::GetConfig() const -> Config {
		return d->config;   
    }

}
