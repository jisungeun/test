﻿#pragma once

#include <QWidget>

#include "WorksetEditorDef.h"

namespace CellAnalyzer::Project::View::WorksetEditor {
	class ModalitySettingWidget : public QWidget {
		Q_OBJECT
	public:
		explicit ModalitySettingWidget(QWidget* parent = nullptr);
		~ModalitySettingWidget() override;

		auto SetConfig(Config config) -> void;

		auto GetModalities() const -> ModalitySetting;

	signals:
		void sigModalityChanged(ModalitySetting setting);

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
