#pragma once

#include <memory>

#include <TCFMetaReader.h>

class SoSwitch;

namespace CellAnalyzer::Project::View::WorksetEditor {
	class SceneManager {
	public:
		SceneManager();
		~SceneManager();

		auto Clear() -> void;
		auto ClearScene() -> void;

		auto SetTCFPath(const QString& path) -> void;
		auto GetTCFPath() const -> QString;

		auto SetMeta(TC::IO::TCFMetaReader::Meta::Pointer meta) -> void;

		auto ReadHT(int timestep) -> void;
		auto ReadFL(int timestep) -> void;

		auto SetDataRange(int min, int max) -> void;

		auto GetSceneGraph() -> SoSwitch*;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
