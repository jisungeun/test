﻿#pragma once

#include <memory>
#include <optional>

#include <QString>
#include <QVariantMap>

#include "WorksetEditorDef.h"

namespace CellAnalyzer::Project::View::WorksetEditor {
	class TimePointSettingControl {
	public:
		explicit TimePointSettingControl();
		~TimePointSettingControl();

		auto RegisterTCFs(const QStringList& tcfPaths) -> void;
		auto GetTCFIds() const -> QStringList;

		auto GetGroup(const QString& tcfId) const -> int;
		auto GetGroups() const -> QMap<int, QStringList>;
		auto GetGroupTimePoints(int groupId) const -> std::optional<QMap<Modality, TimePoints>>;
		auto GetTimePoints(const QString& tcfId) const -> QMap<Modality, TimePoints>;

		auto SetModalitySetting(ModalitySetting settings) -> void;
		auto GetModalitySetting() const -> ModalitySetting;

		auto GetTCFInfo(const QString& tcfId) const -> std::optional<TCFInfo>;

		auto SetLastSelectedTCFId(const QString& tcfId) const -> void;
		auto GetLastSelectedTCFId() const -> QString;

		auto SetTCFModalities(const QString& tcfId, ModalitySetting setting) -> void;
		auto GetTCFModalities(const QString& tcfId) const -> ModalitySetting;

		auto SetTimeIndexes(const QString& tcfId, const QList<int>& indexes) const -> void;
		auto GetTimeIndexes(const QString& tcfId) const -> QList<int>;

		auto GetAllTimePointInfo() const -> QMap<QString, TCFInfo>;

		auto SetConfig(const Config& config) -> void;
		auto GetConfig() const -> Config;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
