#include <QDateTime>
#include <QFileInfo>
#include <QStandardPaths>

#include "ServiceCollection.h"

#include "IDatabase.h"
#include "ILicenseManager.h"

#include "ProjectService.h"
#include "AnalysisProject.h"
#include "PipelineEditorProject.h"

namespace CellAnalyzer::Project {
	struct ProjectService::Impl {
		std::shared_ptr<Tomocube::IServiceProvider> provider = nullptr;
		ProjectPtr project = nullptr;
		QMap<QString, ProjectInfo> projectMap;
		ProjectEventList events;

		auto UpdateProvider(const Tomocube::IServiceProvider* parent) -> void;
		auto UpdateProjectList() -> void;
	};

	auto ProjectService::Impl::UpdateProvider(const Tomocube::IServiceProvider* parent) -> void {
		const auto collection = std::make_unique<Tomocube::DependencyInjection::ServiceCollection>();

		collection->MergeProvider(parent);
		collection->AddTransient<Analysis::AnalysisProject, IProject>()
				->AddTransient<PipelineEditor::PipelineEditorProject, IProject>();

		provider = collection->BuildProvider();
	}

	auto ProjectService::Impl::UpdateProjectList() -> void {
		projectMap.clear();

		for (const auto& p : provider->GetServices<IProject>()) {
			const auto name = p->GetName();
			const auto format = p->GetFormat();
			const auto desc = p->GetDescription();
			const auto icon = p->GetIcon();

			projectMap[name] = { name, format, desc, icon };
		}
	}

	ProjectService::ProjectService(const Tomocube::IServiceProvider* provider) : IProjectService(), d(new Impl) {
		d->UpdateProvider(provider);
		d->UpdateProjectList();
	}

	ProjectService::~ProjectService() = default;

	auto ProjectService::AddEvent(const ProjectEventPtr& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto ProjectService::RemoveEvent(const ProjectEventPtr& event) -> void {
		d->events.removeOne(event);
	}

	auto ProjectService::GetProjectList() const -> QStringList {
		return d->projectMap.keys();
	}

	auto ProjectService::GetProjectInfo(const QString& project) const -> ProjectInfo {
		if (d->projectMap.contains(project))
			return d->projectMap[project];

		return {};
	}

	auto ProjectService::GetProjectName(const QString& project, const QString& url) -> QString {
		if (d->projectMap.contains(project)) {
			for (const auto& p : d->provider->GetServices<IProject>()) {
				if (p->GetName() == project) {
					if (const auto name = p->GetProjectName(url); !name.isEmpty())
						return name;

					break;
				}
			}
		}

		return QFileInfo(url).completeBaseName();
	}

	auto ProjectService::GetBasePath(const QString& project) -> QString {
		return QString("%1/%2").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation).replace("CellAnalyzer", "TomoAnalysis")).arg(project);
	}

	auto ProjectService::FindUrl(const QString& project) -> QString {
		for (const auto& p : d->provider->GetServices<IProject>()) {
			if (p->GetName() == project)
				return p->FindUrl();
		}

		return {};
	}

	auto ProjectService::Initialize(const QString& project, const QString& url) -> bool {
		for (const auto& p : d->provider->GetServices<IProject>()) {
			if (p->GetName() == project) {
				Dispose();

				if (p->Initialize(url)) {
					d->project = p;

					for (const auto& e : d->events)
						e->OnInitialized(project, url);

					return true;
				}
			}
		}

		return false;
	}

	auto ProjectService::Dispose() -> void {
		if (d->project) {
			d->project->Dispose();

			for (const auto& e : d->events)
				e->OnDisposed(d->project->GetName());

			d->project = nullptr;
		}
	}

	auto ProjectService::GetCurrentProject() const -> QString {
		if (d->project)
			return d->project->GetName();

		return {};
	}

	auto ProjectService::GetCurrentUrl() const -> QString {
		if (d->project)
			return d->project->GetUrl();

		return {};
	}
}
