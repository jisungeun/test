#pragma once

#include "IProjectService.h"
#include "IServiceProvider.h"

#include "CellAnalyzer.Project.ManagerExport.h"

namespace CellAnalyzer::Project {
	class CellAnalyzer_Project_Manager_API ProjectService final : public IProjectService {
	public:
		explicit ProjectService(const Tomocube::IServiceProvider* provider);
		~ProjectService() override;

		auto AddEvent(const ProjectEventPtr& event) -> void override;
		auto RemoveEvent(const ProjectEventPtr& event) -> void override;

		auto GetProjectList() const -> QStringList override;
		auto GetProjectInfo(const QString& project) const -> ProjectInfo override;
		auto GetProjectName(const QString& project, const QString& url) -> QString override;
		auto GetBasePath(const QString& project) -> QString override;

		auto FindUrl(const QString& project) -> QString override;
		auto Initialize(const QString& project, const QString& url) -> bool override;
		auto Dispose() -> void override;

		auto GetCurrentProject() const -> QString override;
		auto GetCurrentUrl() const -> QString override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
