#include <QList>
#include <QMap>
#include <QWidget>

#include "PresenterRepo.h"

#include <iostream>

#include "IMenuHandler.h"
#include "IPresenterService.h"
#include "IScreenHandler.h"
#include "IWindowHandler.h"

#include <LiveView.h>
#include <Report.h>

namespace CellAnalyzer::Project::PipelineEditor::Repo {
	struct PresenterRepo::Impl {
		Tomocube::IServiceProvider* provider = nullptr;

		std::shared_ptr<View::LiveView::LiveView> liveview { nullptr };
		std::shared_ptr<View::Report::Report> report { nullptr };

		Presenter::PresenterList list;
		Pipeline::PipelinePtr pipeline { nullptr };
	};

	PresenterRepo::PresenterRepo(Tomocube::IServiceProvider* provider) : IPresenterRepo(), d { new Impl } {
		d->provider = provider;
	}

	PresenterRepo::~PresenterRepo() = default;

	auto PresenterRepo::Load() -> void {
		const auto handler = d->provider->GetService<IMenuHandler>();

		d->liveview = std::make_shared<View::LiveView::LiveView>();
		d->liveview->SetTitle("Live View");

		d->report = std::make_shared<View::Report::Report>();
		d->report->SetTitle("Reports");

		AddPresenter(d->liveview);
		AddPresenter(d->report);

		handler->AddScreen(d->liveview);
		handler->AddScreen(d->report);

		const auto screenhandler = d->provider->GetService<IScreenHandler>();
		screenhandler->SetClosable(d->liveview, false);
		screenhandler->SetClosable(d->report, false);

		const auto windowhandler = d->provider->GetService<IWindowHandler>();
		const auto mipControl = d->liveview->GetWindowList().first();
		windowhandler->Show(mipControl, WindowPosition::Right);
		windowhandler->SetClosable(mipControl, false);

		QObject::connect(d->report.get(), &View::Report::Report::sigHighlight, [=](QString dataID, int labelIdx) {
			if (!d->pipeline) {
				return;
			}
			for (const auto p : d->pipeline->GetProcessList()) {
				if (const auto proc = d->pipeline->GetProcess(p)) {
					for (const auto o : proc->GetOutputList()) {
						if (const auto output = proc->GetOutput(o); output->GetName() == dataID) {
							for (const auto i : proc->GetInputList()) {
								if (const auto data = proc->GetInput(i)->GetLinked(); data->GetFlags().testFlag(DataFlag::Label)) {
									d->liveview->OnHighlight(data->GetName(), labelIdx);
									break;
								}
							}
							break;
						}
					}
				}
			}
		});
		QObject::connect(d->liveview.get(), &View::LiveView::LiveView::sigViewHighlight, [=](int value) {
			d->report->OnHighlight(value);
		});
	}

	auto PresenterRepo::SetPipeline(const Pipeline::PipelinePtr& pipeline) -> void {
		d->pipeline = pipeline;
	}


	auto PresenterRepo::AddPresenter(const Presenter::PresenterPtr& presenter) -> void {
		if (!d->list.contains(presenter)) {
			d->list.push_back(presenter);

			const auto handler = d->provider->GetService<IScreenHandler>();

			if (presenter == d->report)
				handler->Show(presenter, ScreenPosition::Vertical, 0.3);
			else
				handler->Show(presenter);
		}
	}

	auto PresenterRepo::RemovePresenter(const Presenter::PresenterPtr& presenter) -> void {
		d->list.removeOne(presenter);
	}

	auto PresenterRepo::GetDefaultPresenter() const -> Presenter::PresenterPtr {
		return d->liveview;
	}

	auto PresenterRepo::GetDefaultReport() const -> Presenter::PresenterPtr {
		return d->report;
	}

	auto PresenterRepo::Contains(const Presenter::PresenterPtr& presenter) const -> bool {
		return d->list.contains(presenter);
	}

	auto PresenterRepo::GetPresenterList() const -> Presenter::PresenterList {
		return d->list;
	}

	auto PresenterRepo::GetPresenter(const QString& ID) const -> Presenter::PresenterPtr {
		for (auto p : d->list) {
			if (const auto widget = std::dynamic_pointer_cast<QWidget>(p)) {
				if (widget->windowTitle() == ID)
					return p;
			}
		}

		return {};
	}
}
