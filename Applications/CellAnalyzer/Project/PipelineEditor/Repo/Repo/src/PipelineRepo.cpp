#include <QQueue>
#include <QStack>

#include "PipelineRepo.h"

#include "IView.h"

namespace CellAnalyzer::Project::PipelineEditor::Repo {
	struct PipelineRepo::Impl {
		Pipeline::PipelinePtr pipeline = nullptr;
	};

	PipelineRepo::PipelineRepo() : IPipelineRepo(), d(new Impl) { }

	PipelineRepo::~PipelineRepo() = default;

	auto PipelineRepo::SetPipeline(const Pipeline::PipelinePtr& pipeline) -> void {
		d->pipeline = pipeline;
	}

	auto PipelineRepo::GetPipeline() const -> Pipeline::PipelinePtr {
		return d->pipeline;
	}
}
