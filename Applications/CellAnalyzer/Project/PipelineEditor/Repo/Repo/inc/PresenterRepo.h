#pragma once

#include "IPresenterRepo.h"
#include "CellAnalyzer.Project.PipelineEditor.Repo.PipelineExport.h"
#include "IServiceProvider.h"

namespace CellAnalyzer::Project::PipelineEditor::Repo {
	class CellAnalyzer_Project_PipelineEditor_Repo_Pipeline_API PresenterRepo final : public IPresenterRepo {
	public:
		PresenterRepo(Tomocube::IServiceProvider* provider);
		~PresenterRepo() override;

		auto Load() -> void;

		auto SetPipeline(const Pipeline::PipelinePtr& pipeline) -> void override;

		auto AddPresenter(const Presenter::PresenterPtr& presenter) -> void override;
		auto RemovePresenter(const Presenter::PresenterPtr& presenter) -> void override;

		auto GetDefaultPresenter() const -> Presenter::PresenterPtr override;
		auto GetDefaultReport() const -> Presenter::PresenterPtr override;

		auto Contains(const Presenter::PresenterPtr& presenter) const -> bool override;
		auto GetPresenterList() const -> Presenter::PresenterList override;
		auto GetPresenter(const QString& ID) const -> Presenter::PresenterPtr override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
