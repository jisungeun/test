#pragma once

#include "IPipelineRepo.h"

#include "CellAnalyzer.Project.PipelineEditor.Repo.PipelineExport.h"

namespace CellAnalyzer::Project::PipelineEditor::Repo {
	class CellAnalyzer_Project_PipelineEditor_Repo_Pipeline_API PipelineRepo final : public IPipelineRepo {
	public:
		PipelineRepo();
		~PipelineRepo() override;

		auto SetPipeline(const Pipeline::PipelinePtr& pipeline) -> void override;
		auto GetPipeline() const -> Pipeline::PipelinePtr override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
