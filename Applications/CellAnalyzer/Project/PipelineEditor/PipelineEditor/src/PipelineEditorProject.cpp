#include "PipelineEditorProject.h"
#include "ServiceCollection.h"

#include "IAlertHandler.h"
#include "IMenuHandler.h"
#include "IPipelineService.h"
#include "IScreenHandler.h"
#include "IWindowHandler.h"

#include "ClearProperty.h"
#include "IDatabase.h"
#include "SavePipeline.h"
#include "UpdateDefaultPresenter.h"
#include "UpdateDefaultReport.h"
#include "UpdatePipeline.h"
#include "UpdateProcess.h"
#include "UpdateProcessOutput.h"
#include "UpdateSource.h"
#include "UpdatePresenter.h"

#include "InitPipeline.h"
#include "PipelineRepo.h"
#include "PipelineView.h"
#include "PresenterRepo.h"
#include "ProjectMenu.h"
#include "PropertyView.h"

#include "IPresenterService.h"
#include "UpdatePresenter.h"

namespace CellAnalyzer::Project::PipelineEditor {
	struct PipelineEditorProject::Impl {
		std::shared_ptr<Tomocube::IServiceProvider> provider = nullptr;

		std::shared_ptr<UpdatePresenter> uc9 = nullptr;

		std::shared_ptr<IProjectMenu> menu = nullptr;
		std::shared_ptr<Repo::PipelineRepo> repo = nullptr;
		std::shared_ptr<Repo::PresenterRepo> pRepo = nullptr;
		std::shared_ptr<IPipelineView> pipelineView = nullptr;
		std::shared_ptr<IPropertyView> propertyView = nullptr;

		bool initialized = false;

		auto UpdateProvider(const Tomocube::IServiceProvider* parent) -> void;
		auto InitRepo() -> void;
		auto InitView() -> void;
		auto InitUseCase() -> void;
		auto InitUI() const -> void;
	};

	auto PipelineEditorProject::Impl::UpdateProvider(const Tomocube::IServiceProvider* parent) -> void {
		const auto col = std::make_unique<Tomocube::DependencyInjection::ServiceCollection>();

		col->MergeProvider(parent);
		col->AddScoped<Repo::PipelineRepo, IPipelineRepo>()
			->AddScoped<Repo::PresenterRepo, IPresenterRepo>()
			->AddScoped<View::ProjectMenu, IProjectMenu>()
			->AddScoped<View::PipelineView, IPipelineView>()
			->AddScoped<View::PropertyView, IPropertyView>();

		provider = col->BuildProvider();
	}

	auto PipelineEditorProject::Impl::InitRepo() -> void {
		repo = provider->GetService<Repo::PipelineRepo>();
		pRepo = provider->GetService<Repo::PresenterRepo>();
	}

	auto PipelineEditorProject::Impl::InitView() -> void {
		menu = provider->GetService<IProjectMenu>();
		pipelineView = provider->GetService<IPipelineView>();
		propertyView = provider->GetService<IPropertyView>();
	}

	auto PipelineEditorProject::Impl::InitUseCase() -> void {
		const auto handler = provider->GetService<IScreenHandler>();

		const auto uc1 = std::make_shared<SavePipeline>(provider.get());
		const auto uc2 = std::make_shared<UpdatePipeline>(provider.get());
		const auto uc3 = std::make_shared<UpdateSource>(provider.get());
		const auto uc4 = std::make_shared<UpdateProcess>(provider.get());
		const auto uc5 = std::make_shared<UpdateProcessOutput>(provider.get());
		const auto uc6 = std::make_shared<ClearProperty>(provider.get());
		const auto uc7 = std::make_shared<UpdateDefaultPresenter>(provider.get());
		const auto uc8 = std::make_shared<UpdateDefaultReport>(provider.get());
		uc9 = std::make_shared<UpdatePresenter>(provider.get());
		const auto uc10 = std::make_shared<UpdatePresenter>(provider.get());

		handler->AddEvent(uc9);
		menu->AddEvent(uc1);
		pipelineView->AddEvent(uc2);
		pipelineView->AddEvent(uc3);
		pipelineView->AddEvent(uc4);
		pipelineView->AddEvent(uc5);
		pipelineView->AddEvent(uc6);
		pipelineView->AddEvent(uc7);
		pipelineView->AddEvent(uc8);
		pipelineView->AddEvent(uc10);

		propertyView->AddEvent(uc2);
		propertyView->AddEvent(uc3);
		propertyView->AddEvent(uc4);
		propertyView->AddEvent(uc5);
		propertyView->AddEvent(uc7);
		propertyView->AddEvent(uc8);
	}

	auto PipelineEditorProject::Impl::InitUI() const -> void {
		const auto menuHandler = provider->GetService<IMenuHandler>();
		const auto windowHandler = provider->GetService<IWindowHandler>();
		const auto screenHandler = provider->GetService<IScreenHandler>();

		windowHandler->Show(pipelineView, WindowPosition::Left);
		windowHandler->ShowSplit(propertyView, pipelineView, WindowPosition::Bottom, 0.5);

		menuHandler->AddMenu(menu);
		menuHandler->AddWindow(propertyView);
		menuHandler->AddWindow(pipelineView);
	}

	PipelineEditorProject::PipelineEditorProject(const Tomocube::IServiceProvider* provider) : IProject(), d(new Impl) {
		d->UpdateProvider(provider);
		d->InitRepo();
	}

	PipelineEditorProject::~PipelineEditorProject() = default;

	auto PipelineEditorProject::GetName() const -> QString {
		return "Pipeline Editor";
	}

	auto PipelineEditorProject::GetFormat() const -> QString {
		return "tcap";
	}

	auto PipelineEditorProject::GetDescription() const -> QString {
		return "Open or create an analysis pipeline.";
	}

	auto PipelineEditorProject::GetIcon() const -> QString {
		return ":/Project/PipelineEditor.svg";
	}

	auto PipelineEditorProject::GetUrl() const -> QString {
		if (const auto pipeline = d->repo->GetPipeline())
			return pipeline->GetLocation();

		return {};
	}

	auto PipelineEditorProject::GetProjectName(const QString& url) const -> QString {
		return {};
	}

	auto PipelineEditorProject::FindUrl() -> QString {
		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto database = d->provider->GetService<IDatabase>();
		const auto init = std::make_shared<View::InitPipeline>(d->provider.get());

		if (handler->ShowDialog(init) == QDialog::Accepted) {
			QVariantMap map;
			map["Path"] = init->GetPath();
			database->Save("Project\\Pipeline", map);

			return init->GetUrl();
		}

		return {};
	}

	auto PipelineEditorProject::Initialize(const QString& url) -> bool {
		const auto loader = d->provider->GetService<Pipeline::IPipelineService>();
		if (const auto pipeline = loader->Read(url)) {
			d->repo->SetPipeline(pipeline);
			pipeline->Load();
			d->pRepo->Load();

			d->InitView();
			d->InitUseCase();
			d->InitUI();

			d->pRepo->SetPipeline(pipeline);
			return true;
		}

		return false;
	}

	auto PipelineEditorProject::Dispose() -> void {
		const auto handler = d->provider->GetService<IScreenHandler>();
		handler->RemoveEvent(d->uc9);

		d->uc9 = nullptr;
		d->menu = nullptr;
		d->repo = nullptr;
		d->pRepo = nullptr;
		d->pipelineView = nullptr;
		d->propertyView = nullptr;
	}
}
