#pragma once

#include "IProject.h"
#include "IServiceProvider.h"

#include "CellAnalyzer.Project.PipelineEditorExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_API PipelineEditorProject final : public QObject, public IProject {
	public:
		explicit PipelineEditorProject(const Tomocube::IServiceProvider* provider);
		~PipelineEditorProject() override;

		auto GetName() const -> QString override;
		auto GetFormat() const -> QString override;
		auto GetDescription() const -> QString override;
		auto GetIcon() const -> QString override;
		auto GetUrl() const -> QString override;
		auto GetProjectName(const QString& url) const -> QString override;

		auto FindUrl() -> QString override;
		auto Initialize(const QString& url) -> bool override;
		auto Dispose() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
