#pragma once

#include "IPipeline.h"

#include "CellAnalyzer.Project.PipelineEditor.ViewModelExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class IProjectMenuEvent;
	using ProjectMenuEventPtr = std::shared_ptr<IProjectMenuEvent>;
	using ProjectMenuEventList = QVector<ProjectMenuEventPtr>;

	class CellAnalyzer_Project_PipelineEditor_ViewModel_API IProjectMenuEvent {
	public:
		virtual ~IProjectMenuEvent();

		virtual auto OnPipelineSaved() -> void;

		virtual auto OnPipelineSaveAs(const QString& path) -> void;
	};
}
