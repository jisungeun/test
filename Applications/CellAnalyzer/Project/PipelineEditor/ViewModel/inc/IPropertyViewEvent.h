#pragma once

#include "IPipeline.h"

#include "CellAnalyzer.Project.PipelineEditor.ViewModelExport.h"
#include "IPropertyItem.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class IPropertyViewEvent;
	using PropertyViewEventPtr = std::shared_ptr<IPropertyViewEvent>;
	using PropertyViewEventList = QVector<PropertyViewEventPtr>;

	class CellAnalyzer_Project_PipelineEditor_ViewModel_API IPropertyViewEvent {
	public:
		virtual ~IPropertyViewEvent();

		virtual auto OnPropertyChanged(Session session, const QString& name, const Pipeline::AttrValue& value) -> void;
		virtual auto OnModifierChanged(Session session, const QString& name, Pipeline::AttrModifier modifier) -> void;

		virtual auto OnExecuted(Session session) -> void;
		virtual auto OnDiscarded(Session session) -> void;
		virtual auto OnSaved(Session session) -> void;
		virtual auto OnAborted() -> void;
	};
}
