#pragma once

#include "IView.h"

#include "IPropertyItem.h"
#include "IPropertyViewEvent.h"

#include "CellAnalyzer.Project.PipelineEditor.ViewModelExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_ViewModel_API IPropertyView : public IView {
	public:
		virtual auto AddEvent(const PropertyViewEventPtr& event) -> void = 0;
		virtual auto RemoveEvent(const PropertyViewEventPtr& event) -> void = 0;

		virtual auto CreateSession() -> Session = 0;
		virtual auto GetSession() const -> Session = 0;

		virtual auto GetPropertyList(Session session) const -> PropertyList = 0;
		virtual auto GetProperty(Session session, const QString& name) const -> PropertyPtr = 0;

		virtual auto CreateProperty(const QString& group, const QString& name, Pipeline::AttrCategory category) -> PropertyPtr = 0;
		virtual auto AddProperty(Session session, PropertyPtr&& property, bool showModifier = false) -> void = 0;
		virtual auto RemoveProperty(Session session, const QString& name) -> void = 0;

		virtual auto SetExecutable(Session session, bool executable) -> void = 0;
		virtual auto SetAbortable(Session session, bool abortable) -> void = 0;
		virtual auto SetSavable(Session session, bool savable) -> void = 0;
	};
}
