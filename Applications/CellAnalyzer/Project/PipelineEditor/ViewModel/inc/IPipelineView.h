#pragma once

#include "IPipelineViewEvent.h"
#include "IView.h"

#include "CellAnalyzer.Project.PipelineEditor.ViewModelExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_ViewModel_API IPipelineView : public IView {
	public:
		virtual auto AddEvent(const PipelineViewEventPtr& event) -> void = 0;
		virtual auto RemoveEvent(const PipelineViewEventPtr& event) -> void = 0;

		virtual auto SetEditable(bool editable) -> void = 0;
		virtual auto SetLoading(bool loading) -> void = 0;

		virtual auto UpdatePipelineName() -> void = 0;
		virtual auto UpdateSourceList() -> void = 0;
		virtual auto UpdateProcessList() -> void = 0;

		virtual auto SelectPipeline() -> void = 0;
		virtual auto SelectSource(const QString& name) -> void = 0;
		virtual auto SelectProcess(const QString& name) -> void = 0;
		virtual auto SelectProcessOutput(const QString& name) -> void = 0;

		virtual auto GetSelectedType() const -> ItemType = 0;
		virtual auto GetSelectedName() const -> QString = 0;
	};
}
