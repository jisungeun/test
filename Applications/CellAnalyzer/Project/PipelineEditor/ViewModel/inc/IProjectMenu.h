#pragma once

#include "IMenu.h"
#include "IProjectMenuEvent.h"

#include "CellAnalyzer.Project.PipelineEditor.ViewModelExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class IProjectMenu;
	using ProjectMenuPtr = std::shared_ptr<IProjectMenu>;

	class CellAnalyzer_Project_PipelineEditor_ViewModel_API IProjectMenu : public IMenu {
	public:
		virtual auto AddEvent(const ProjectMenuEventPtr& event) -> void = 0;
		virtual auto RemoveEvent(const ProjectMenuEventPtr& event) -> void = 0;

		virtual auto SetPipelineSavable(bool savable) -> void = 0;
	};
}
