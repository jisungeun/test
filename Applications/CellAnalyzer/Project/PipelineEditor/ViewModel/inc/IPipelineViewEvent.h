#pragma once

#include "IPipeline.h"

#include "CellAnalyzer.Project.PipelineEditor.ViewModelExport.h"
#include "IPropertyItem.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class IPipelineViewEvent;
	using PipelineViewEventPtr = std::shared_ptr<IPipelineViewEvent>;
	using PipelineViewEventList = QVector<PipelineViewEventPtr>;

	enum class ItemType {
		None,
		Pipeline,
		Source,
		Process,
		ProcessOutput
	};

	class CellAnalyzer_Project_PipelineEditor_ViewModel_API IPipelineViewEvent {
	public:
		virtual ~IPipelineViewEvent();

		virtual auto OnSelectionChanged(ItemType type, const QString& name) -> void;

		virtual auto OnProcessAdded(const QString& id) -> void;
		virtual auto OnProcessRemoved(const QString& name) -> void;
		virtual auto OnDataRemoved(const QString& parentName) -> void;
		virtual auto OnSourceAdded(const QString& name, DataFlags flags) const -> void;
		virtual auto OnSourceRemoved(const QString& name) -> void;
		virtual auto OnSourceLinked(const QString& name, const QString& path, int timePoint, int ch = 0) -> void;
		virtual auto OnSourceUpdated(const QString& name) -> void;

		virtual auto OnPresenterAdded(const QString& presenterID, const QMap<QString, QStringList>& dataMap, const QString& windowTitle) -> void;
	};
}
