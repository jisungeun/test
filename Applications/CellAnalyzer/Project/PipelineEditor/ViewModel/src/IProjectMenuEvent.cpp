#include "IProjectMenuEvent.h"

namespace CellAnalyzer::Project::PipelineEditor {
	IProjectMenuEvent::~IProjectMenuEvent() = default;

	auto IProjectMenuEvent::OnPipelineSaved() -> void {}

	auto IProjectMenuEvent::OnPipelineSaveAs(const QString& path) -> void { }
}
