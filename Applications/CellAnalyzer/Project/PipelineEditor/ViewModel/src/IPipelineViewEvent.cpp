#include "IPipelineViewEvent.h"

namespace CellAnalyzer::Project::PipelineEditor {
	IPipelineViewEvent::~IPipelineViewEvent() = default;

	auto IPipelineViewEvent::OnSelectionChanged(ItemType type, const QString& name) -> void {}

	auto IPipelineViewEvent::OnProcessAdded(const QString& id) -> void {}

	auto IPipelineViewEvent::OnProcessRemoved(const QString& name) -> void {}

	auto IPipelineViewEvent::OnSourceAdded(const QString& name, DataFlags flags) const -> void {}

	auto IPipelineViewEvent::OnSourceRemoved(const QString& name) -> void {}

	auto IPipelineViewEvent::OnDataRemoved(const QString& parentName) -> void { }

	auto IPipelineViewEvent::OnSourceLinked(const QString& name, const QString& path, int timePoint, int ch) -> void { }

	auto IPipelineViewEvent::OnSourceUpdated(const QString& name) -> void { }

	auto IPipelineViewEvent::OnPresenterAdded(const QString& presenterID, const QMap<QString, QStringList>& dataMap, const QString& windowTitle) -> void { }

}
