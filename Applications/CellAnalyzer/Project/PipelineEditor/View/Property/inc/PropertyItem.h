#pragma once

#include <QTreeWidgetItem>

#include "AttrWidget.h"
#include "ModifierWidget.h"

#include "IPropertyItem.h"

#include "CellAnalyzer.Project.PipelineEditor.View.PropertyExport.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	class CellAnalyzer_Project_PipelineEditor_View_Property_API PropertyItem final : public std::enable_shared_from_this<PropertyItem>, public QTreeWidgetItem, public IPropertyItem {
	public:
		PropertyItem(const QString& text, const QString& group, Pipeline::AttrCategory category);
		~PropertyItem() override;

		auto IsExecutable() const -> bool;
		auto Discard() -> void;

		auto GetCategory() const -> Pipeline::AttrCategory;

		auto SetAttrWidget(Pipeline::Widget::AttrWidget* widget) -> void;
		auto SetModifierWidget(Pipeline::Widget::ModifierWidget* widget) -> void;

		auto GetName() const -> QString override;
		auto GetGroup() const -> QString override;
		auto GetModifier() const -> Pipeline::AttrModifier override;
		auto GetValue() const -> Pipeline::AttrValue override;
		auto GetDescription() const -> QString override;

		auto SetDescription(const QString& desc) -> void override;
		auto SetModifier(Pipeline::AttrModifier modifier) -> void override;
		auto SetValue(const Pipeline::AttrValue& value) -> void override;
		auto SetModel(const Pipeline::AttrModel& model) -> void override;
		auto SetState(Pipeline::AttrState state) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
