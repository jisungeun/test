#pragma once

#include <QMenu>

#include "IServiceProvider.h"

#include "IProjectMenu.h"

#include "CellAnalyzer.Project.PipelineEditor.View.MenuExport.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	class CellAnalyzer_Project_PipelineEditor_View_Menu_API ProjectMenu final : public QMenu, public IProjectMenu {
	public:
		explicit ProjectMenu(Tomocube::IServiceProvider* provider);
		~ProjectMenu() override;

		auto GetParent() const -> MenuParent override;

		auto AddEvent(const ProjectMenuEventPtr& event) -> void override;
		auto RemoveEvent(const ProjectMenuEventPtr& event) -> void override;

		auto SetPipelineSavable(bool savable) -> void override;

	protected slots:
		auto OnPipelineSaved() -> void;
		auto OnPipelineSaveAs() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
