#include <QFileDialog>

#include "IProject.h"
#include "IPipelineRepo.h"
#include "IAlertHandler.h"

#include "ProjectMenu.h"

#include "IDatabase.h"
#include "IPipelineView.h"
#include "IProjectService.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	struct ProjectMenu::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		ProjectMenuEventList events;

		QAction* save = nullptr;
		QAction* saveAs = nullptr;
	};

	ProjectMenu::ProjectMenu(Tomocube::IServiceProvider* provider) : QMenu(), IProjectMenu(), d(new Impl) {
		d->provider = provider;

		d->save = addAction("Save Pipeline");
		d->saveAs = addAction("Save Pipeline As...");

		connect(d->save, &QAction::triggered, this, &ProjectMenu::OnPipelineSaved);
		connect(d->saveAs, &QAction::triggered, this, &ProjectMenu::OnPipelineSaveAs);
	}

	ProjectMenu::~ProjectMenu() = default;

	auto ProjectMenu::GetParent() const -> MenuParent {
		return MenuParent::File;
	}

	auto ProjectMenu::AddEvent(const ProjectMenuEventPtr& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto ProjectMenu::RemoveEvent(const ProjectMenuEventPtr& event) -> void {
		d->events.removeOne(event);
	}

	auto ProjectMenu::SetPipelineSavable(bool savable) -> void {
		Run([this, savable] {
			d->save->setEnabled(savable);
		});
	}

	auto ProjectMenu::OnPipelineSaved() -> void {
		RunAsync([this] {
			for (const auto& e : d->events)
				e->OnPipelineSaved();
		});
	}

	auto ProjectMenu::OnPipelineSaveAs() -> void {
		const auto service = d->provider->GetService<IProjectService>();
		const auto prevPath = service->GetBasePath("Pipeline");
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pip = repo->GetPipeline();

		if (!pip) {
			return;
		}

		const auto name = pip->GetName();
		const auto path = QFileDialog::getSaveFileName(this, "Open TCF", pip->GetLocation(), "Tomocube analysis pipeline (*.tcap)", nullptr);

		if (path.isEmpty()) {
			return;
		}

		const auto handler = d->provider->GetService<IAlertHandler>();

		if (false == handler->ShowYesNo("The current work in progress will be reset.", " Do you want to continue Save As process?")) {
			return;
		}
		Run([this,path] {
			for (const auto& e : d->events) {
				e->OnPipelineSaveAs(path);
			}
		});

		const auto pipView = d->provider->GetService<IPipelineView>();
		pipView->UpdatePipelineName();
	}
}
