#pragma once

#include <QTreeWidgetItem>

#include "IServiceProvider.h"

#include "IView.h"

#include "CellAnalyzer.Project.PipelineEditor.View.PipelineExport.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	class CellAnalyzer_Project_PipelineEditor_View_Pipeline_API ProcessView final : public QWidget, public IView {
		Q_OBJECT

	public:
		explicit ProcessView(Tomocube::IServiceProvider* provider);
		~ProcessView() override;

	protected:
		auto keyPressEvent(QKeyEvent* event) -> void override;

	protected slots:
		auto OnTreeItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous) -> void;
		auto OnIOTreeChanged(QTreeWidgetItem *item, int column) -> void;
		auto OnOpenBtnClicked() -> void;
		auto OnSearchLineChanged(const QString& text) -> void;

	signals:
		auto ProcessAdded(const QString& id) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
