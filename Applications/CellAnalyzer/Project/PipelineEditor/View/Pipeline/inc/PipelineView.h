#pragma once

#include <QTreeWidgetItem>
#include <QWidget>

#include "IServiceProvider.h"

#include "IPipelineView.h"

#include "CellAnalyzer.Project.PipelineEditor.View.PipelineExport.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	class CellAnalyzer_Project_PipelineEditor_View_Pipeline_API PipelineView final : public QWidget, public IPipelineView {
	public:
		explicit PipelineView(Tomocube::IServiceProvider* provider);
		~PipelineView() override;

		auto AddEvent(const std::shared_ptr<IPipelineViewEvent>& event) -> void override;
		auto RemoveEvent(const std::shared_ptr<IPipelineViewEvent>& event) -> void override;

		auto SetEditable(bool editable) -> void override;
		auto SetLoading(bool loading) -> void override;

		auto UpdatePipelineName() -> void override;
		auto UpdateSourceList() -> void override;
		auto UpdateProcessList() -> void override;

		auto SelectPipeline() -> void override;
		auto SelectSource(const QString& name) -> void override;
		auto SelectProcess(const QString& name) -> void override;
		auto SelectProcessOutput(const QString& name) -> void override;

		auto GetSelectedType() const -> ItemType override;
		auto GetSelectedName() const -> QString override;

	protected slots:
		auto OnProcessAdded(const QString& id) -> void;
		auto OnAddBtnClicked() -> void;
		auto OnDelBtnClicked() -> void;
		auto OnLinkClicked() -> void;
		auto OnRoiClicked() -> void;
		auto OnMaskEditorClicked() -> void;
		auto OnSnapshotClicked() -> void;

		auto OnAddProcBtnClicked() -> void;
		auto OnDelProcBtnClicked() -> void;
		auto OnAddSrcBtnClicked() -> void;
		auto OnDelSrcBtnClicked() -> void;

		auto OnAddPresenterBtnClicked() -> void;

		auto OnTreeSelectionChanged() -> void;
		auto OnTreeContextMenuRequested(const QPoint& pos) -> void;

	protected:
		auto eventFilter(QObject* watched, QEvent* event) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
