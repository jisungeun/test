#pragma once

#include <QDialog>

#include "IView.h"

#include "IData.h"

#include "CellAnalyzer.Project.PipelineEditor.View.PipelineExport.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	class CellAnalyzer_Project_PipelineEditor_View_Pipeline_API SourceDialog final : public QDialog, public IView {
	public:
		SourceDialog();
		~SourceDialog() override;

		auto GetSourceName() const -> QString;
		auto GetSourceFlags() const -> DataFlags;

	protected slots:
		auto OnHtRadioToggled(bool checked) -> void;
		auto OnFlRadioToggled(bool checked) -> void;
		auto OnBfRadioToggled(bool checked) -> void;
		auto OnD2RadioToggled(bool checked) -> void;
		auto OnD3RadioToggled(bool checked) -> void;

		auto OnNameChanged(const QString& text) -> void;
		auto OnAddBtnClicked() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
