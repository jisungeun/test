#pragma once

#include <QDialog>
#include <QTreeWidgetItem>

#include "IServiceProvider.h"

#include "IPipeline.h"
#include "IView.h"

#include "CellAnalyzer.Project.PipelineEditor.View.PipelineExport.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	class CellAnalyzer_Project_PipelineEditor_View_Pipeline_API PresenterDialog final : public QDialog, public IView {
	public:
		explicit PresenterDialog(Tomocube::IServiceProvider* provider, const Pipeline::PipelinePtr& pipeline);
		~PresenterDialog() override;

		auto GetWindowName() const -> QString;
		auto GetPresenterID() const -> QString;
		auto GetDataLinkMap() const -> QMap<QString, QStringList>;//PortName, DataIDList

	protected slots:
		auto OnPresenterSelected(QTreeWidgetItem* current, QTreeWidgetItem* previous) -> void;
		auto OnSelectBtnClicked() -> void;
		auto OnCheckChanged(QTreeWidgetItem* item, int column) -> void;
		auto OnOkBtnClicked() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
