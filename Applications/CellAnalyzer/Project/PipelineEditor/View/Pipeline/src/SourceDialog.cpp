#include <QCheckBox>

#include "SourceDialog.h"

#include "ui_SourceDialog.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	struct SourceDialog::Impl {
		Ui::SourceDialog ui;

		QMap<DataFlag, QCheckBox*> checkMap;

		QString name;
		DataFlags flags;
	};

	SourceDialog::SourceDialog() : QDialog(nullptr, Qt::Dialog | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint), IView(), d(new Impl) {
		d->ui.setupUi(this);
		d->ui.addBtn->setObjectName("accent_color");
		d->ui.bfRadio->setVisible(false);
		d->ui.bfRadio->setEnabled(false);

		connect(d->ui.htRadio, &QRadioButton::toggled, this, &SourceDialog::OnHtRadioToggled);
		connect(d->ui.flRadio, &QRadioButton::toggled, this, &SourceDialog::OnFlRadioToggled);
		connect(d->ui.bfRadio, &QRadioButton::toggled, this, &SourceDialog::OnBfRadioToggled);
		connect(d->ui.d2Radio, &QRadioButton::toggled, this, &SourceDialog::OnD2RadioToggled);
		connect(d->ui.d3Radio, &QRadioButton::toggled, this, &SourceDialog::OnD3RadioToggled);
		connect(d->ui.lineEdit, &QLineEdit::textChanged, this, &SourceDialog::OnNameChanged);
		connect(d->ui.addBtn, &QPushButton::clicked, this, &SourceDialog::OnAddBtnClicked);
	}

	SourceDialog::~SourceDialog() = default;

	auto SourceDialog::GetSourceName() const -> QString {
		return (d->name.isEmpty()) ? "Source" : d->name;
	}

	auto SourceDialog::GetSourceFlags() const -> DataFlags {
		return d->flags;
	}

	auto SourceDialog::OnHtRadioToggled(bool checked) -> void {
		d->flags = (checked) ? d->flags | DataFlags(DataFlag::HT) : d->flags & ~DataFlags(DataFlag::HT);
	}

	auto SourceDialog::OnFlRadioToggled(bool checked) -> void {
		d->flags = (checked) ? d->flags | DataFlags(DataFlag::FL) : d->flags & ~DataFlags(DataFlag::FL);
	}

	auto SourceDialog::OnBfRadioToggled(bool checked) -> void {
		d->flags = (checked) ? d->flags | DataFlags(DataFlag::BF) : d->flags & ~DataFlags(DataFlag::BF);
	}

	auto SourceDialog::OnD2RadioToggled(bool checked) -> void {
		d->flags = (checked) ? d->flags | DataFlags(DataFlag::Volume2D) : d->flags & ~DataFlags(DataFlag::Volume2D);
	}

	auto SourceDialog::OnD3RadioToggled(bool checked) -> void {
		d->flags = (checked) ? d->flags | DataFlags(DataFlag::Volume3D) : d->flags & ~DataFlags(DataFlag::Volume3D);
	}

	auto SourceDialog::OnNameChanged(const QString& text) -> void {
		d->name = text;
	}

	auto SourceDialog::OnAddBtnClicked() -> void {
		accept();
	}
}
