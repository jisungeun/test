#include "IAlertHandler.h"
#include "IPipeline.h"
#include "IPresenterService.h"

#include "PresenterDialog.h"

#include "ui_PresenterDialog.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	struct PresenterDialog::Impl {
		Ui::Dialog ui;
		Tomocube::IServiceProvider* provider { nullptr };
		Pipeline::PipelinePtr pipeline { nullptr };

		QString currentPresenterID;
		QString windowTitle;
		Presenter::DataPort currentPort;
		QMap<QTreeWidgetItem*, bool> layeredMap;
		QMap<QString, QStringList> dataPortMap;
	};

	PresenterDialog::PresenterDialog(Tomocube::IServiceProvider* provider, const Pipeline::PipelinePtr& pipeline) : QDialog(nullptr, Qt::Dialog | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint), IView(), d { new Impl } {
		d->ui.setupUi(this);
		setWindowTitle("Display data in a new viewer");
		d->provider = provider;
		d->pipeline = pipeline;
		d->ui.selectBtn->setObjectName("accent_color");
		d->ui.selectBtn->setEnabled(false);

		d->ui.okBtn->setObjectName("accent_color");
		d->ui.okBtn->setEnabled(false);

		d->ui.descText->setReadOnly(true);

		const auto service = d->provider->GetService<Presenter::IPresenterService>();
		QMap<QString, QTreeWidgetItem*> map;
		for (const auto& id : service->GetIdList()) {
			const auto type = service->GetType(id);

			if (false == map.contains(type->GetCategory()))
				map[type->GetCategory()] = new QTreeWidgetItem(d->ui.presenterTree, { type->GetCategory() });
			auto* child = new QTreeWidgetItem(map[type->GetCategory()], { id });
			child->setIcon(0, QIcon(":/Flat/Presenter.svg"));
			map[type->GetCategory()]->addChild(child);
		}
		d->ui.presenterTree->expandAll();

		connect(d->ui.presenterTree, &QTreeWidget::currentItemChanged, this, &PresenterDialog::OnPresenterSelected);
		connect(d->ui.selectBtn, &QPushButton::clicked, this, &PresenterDialog::OnSelectBtnClicked);
		connect(d->ui.dataTree, &QTreeWidget::itemChanged, this, &PresenterDialog::OnCheckChanged);
		connect(d->ui.okBtn, &QPushButton::clicked, this, &PresenterDialog::OnOkBtnClicked);
	}

	PresenterDialog::~PresenterDialog() = default;

	auto PresenterDialog::OnPresenterSelected(QTreeWidgetItem* current, QTreeWidgetItem* previous) -> void {
		const auto service = d->provider->GetService<Presenter::IPresenterService>();

		if (!current) {
			d->currentPresenterID = QString();
			d->ui.selectBtn->setEnabled(false);
			return;
		}
		const auto type = service->GetType(current->text(0));
		if (!type) {
			d->currentPresenterID = QString();
			d->ui.selectBtn->setEnabled(false);
			return;
		}
		d->currentPresenterID = current->text(0);
		d->ui.selectBtn->setEnabled(true);
	}

	auto PresenterDialog::OnSelectBtnClicked() -> void {
		if (d->currentPresenterID.isEmpty())
			return;
		const auto service = d->provider->GetService<Presenter::IPresenterService>();
		const auto type = service->GetType(d->currentPresenterID);
		const auto desc = service->GetDescription(d->currentPresenterID);
		const auto ports = type->GetDataPort();
		d->currentPort = ports;

		d->ui.descText->setPlainText(desc);
		d->ui.okBtn->setEnabled(false);
		d->ui.titleEdit->setPlaceholderText(d->currentPresenterID);
		//refresh data list
		d->ui.dataTree->blockSignals(true);
		d->layeredMap.clear();
		d->ui.dataTree->clear();
		for (const auto key : d->currentPort.keys()) {
			const auto portItem = new QTreeWidgetItem(d->ui.dataTree, { key });
			const auto portFlag = std::get<0>(d->currentPort[key]);
			const auto layered = std::get<1>(d->currentPort[key]);
			d->layeredMap[portItem] = layered;

			for (const auto& dataID : d->pipeline->GetDataList()) {
				const auto data = d->pipeline->GetData(dataID);
				if (false == data->ExistsData())
					continue;
				const auto dataFlag = data->GetData()->GetFlags();
				if ((portFlag & dataFlag) == dataFlag) {
					auto* child = new QTreeWidgetItem(portItem, { dataID });
					child->setFlags(child->flags() | Qt::ItemIsUserCheckable);
					child->setCheckState(0, Qt::Unchecked);
					if (dataFlag.testFlag(DataFlag::Binary) || dataFlag.testFlag(DataFlag::Label))
						child->setIcon(0, QIcon(":/Flat/Mask.svg"));
					else if (dataFlag.testFlag(DataFlag::Measure))
						child->setIcon(0, QIcon(":/Flat/Measure.svg"));
					else
						child->setIcon(0, QIcon(":/Flat/Data.svg"));
					portItem->addChild(child);
				}
			}
		}
		d->ui.dataTree->expandAll();
		d->ui.dataTree->blockSignals(false);
	}

	auto PresenterDialog::OnCheckChanged(QTreeWidgetItem* item, int column) -> void {
		const auto parent = item->parent();
		if (!parent)
			return;
		const auto layered = d->layeredMap[parent];
		const auto checked = item->checkState(0);
		d->ui.dataTree->blockSignals(true);

		if (false == layered && checked == Qt::CheckState::Checked) {
			for (auto i = 0; i < parent->childCount(); i++) {
				const auto child = parent->child(i);
				if (child == item)
					continue;
				child->setCheckState(0, Qt::Unchecked);
			}
		}
		d->ui.dataTree->blockSignals(false);

		//check every port is full
		auto everyPortFull = true;
		for (auto i = 0; i < d->ui.dataTree->topLevelItemCount(); i++) {
			const auto portItem = d->ui.dataTree->topLevelItem(i);
			auto portIsFull = false;
			for (auto j = 0; j < portItem->childCount(); j++) {
				if (portItem->child(j)->checkState(0) == Qt::Checked) {
					portIsFull = true;
					break;
				}
			}
			if (false == portIsFull) {
				everyPortFull = false;
				break;
			}
		}
		d->ui.okBtn->setEnabled(everyPortFull);
	}

	auto PresenterDialog::OnOkBtnClicked() -> void {
		d->dataPortMap.clear();
		for (auto i = 0; i < d->ui.dataTree->topLevelItemCount(); i++) {
			const auto portItem = d->ui.dataTree->topLevelItem(i);
			const auto portID = portItem->text(0);

			QStringList dataList;
			for (auto j = 0; j < portItem->childCount(); j++) {
				const auto child = portItem->child(j);
				if (child->checkState(0) == Qt::Checked)
					dataList.append(child->text(0));
			}
			d->dataPortMap[portID] = dataList;
		}
		if (d->ui.titleEdit->text().isEmpty())
			d->windowTitle = d->ui.titleEdit->placeholderText();
		else
			d->windowTitle = d->ui.titleEdit->text();
		accept();
	}

	auto PresenterDialog::GetWindowName() const -> QString {
		return d->windowTitle;
	}

	auto PresenterDialog::GetPresenterID() const -> QString {
		return d->currentPresenterID;
	}

	auto PresenterDialog::GetDataLinkMap() const -> QMap<QString, QStringList> {
		return d->dataPortMap;
	}
}
