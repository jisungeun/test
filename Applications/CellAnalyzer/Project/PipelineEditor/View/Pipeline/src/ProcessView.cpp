#include <optional>

#include <QKeyEvent>

#include "ProcessView.h"

#include "IAlertHandler.h"
#include "IProcessService.h"

#include "ui_ProcessView.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	struct ProcessView::Impl {
		Ui::ProcessView ui;
		Tomocube::IServiceProvider* provider = nullptr;

		std::optional<QString> selected = std::nullopt;

		auto AddInput(const QString& id, const QStringList& flags) -> void;
		auto AddOutput(const QString& id, const QStringList& flags) -> void;
		auto ResizeIO() -> void;
	};

	auto ProcessView::Impl::AddInput(const QString& id, const QStringList& flags) -> void {
		auto* input = new QTreeWidgetItem(ui.inputTree, { id });

		if (flags.contains("Binary") || flags.contains("Label"))
			input->setIcon(0, QIcon(":/Flat/Mask.svg"));
		else if (flags.contains("Measure"))
			input->setIcon(0, QIcon(":/Flat/Measure.svg"));
		else
			input->setIcon(0, QIcon(":/Flat/Data.svg"));

		auto* frame = new QFrame(ui.outputTree);
		auto* layout = new QHBoxLayout(frame);
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(3);
		frame->setLayout(layout);

		for (const auto& f : flags) {
			auto* label = new QLabel(f, frame);
			label->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
			layout->addWidget(label);
		}
		
		layout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));

		ui.inputTree->setItemWidget(input, 1, frame);
		ui.inputTree->addTopLevelItem(input);
	}

	auto ProcessView::Impl::AddOutput(const QString& id, const QStringList& flags) -> void {
		auto* input = new QTreeWidgetItem(ui.outputTree, { id });
		
		if (flags.contains("Binary") || flags.contains("Label"))
			input->setIcon(0, QIcon(":/Flat/Mask.svg"));
		else if (flags.contains("Measure"))
			input->setIcon(0, QIcon(":/Flat/Measure.svg"));
		else
			input->setIcon(0, QIcon(":/Flat/Data.svg"));

		auto* frame = new QFrame(ui.outputTree);
		auto* layout = new QHBoxLayout(frame);
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(3);
		frame->setLayout(layout);

		for (const auto& f : flags) {
			auto* label = new QLabel(f, frame);
			label->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
			layout->addWidget(label);
		}

		layout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));

		ui.outputTree->setItemWidget(input, 1, frame);
		ui.outputTree->addTopLevelItem(input);
	}

	auto ProcessView::Impl::ResizeIO() -> void {
		const auto widgets = { ui.inputTree, ui.outputTree };

		for (auto* w : widgets) {
			auto height = 0;

			for (auto i = 0; i < w->topLevelItemCount(); i++) {
				const auto* parent = w->topLevelItem(i);
				const auto rect = w->visualItemRect(parent);
				height += rect.height() * (parent->childCount() + 1);
			}

			w->setFixedHeight(height);
		}
	}

	ProcessView::ProcessView(Tomocube::IServiceProvider* provider) : QWidget(nullptr), IView(), d(new Impl) {
		d->ui.setupUi(this);
		d->provider = provider;
		d->ui.openBtn->setObjectName("accent_color");
		d->ui.openBtn->setEnabled(false);
		d->ui.inputTree->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
		d->ui.outputTree->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
		d->ui.inputTree->header()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
		d->ui.outputTree->header()->setSectionResizeMode(1, QHeaderView::ResizeToContents);

		const auto service = d->provider->GetService<Pipeline::IProcessService>();
		QMap<Pipeline::ProcessCategory, QTreeWidgetItem*> map;

		for (const auto& l : service->GetList()) {
			const auto type = service->GetType(l);

			if (!map.contains(type->GetCategory()))
				map[type->GetCategory()] = new QTreeWidgetItem(d->ui.tree, { ToString(type->GetCategory()) });

			auto* child = new QTreeWidgetItem(map[type->GetCategory()], { l });
			child->setIcon(0, QIcon(":/Flat/Process.svg"));
			map[type->GetCategory()]->addChild(child);
		}

		d->ui.tree->expandAll();

		connect(d->ui.inputTree, &QTreeWidget::itemChanged, this, &ProcessView::OnIOTreeChanged);
		connect(d->ui.outputTree, &QTreeWidget::itemChanged, this, &ProcessView::OnIOTreeChanged);
		connect(d->ui.tree, &QTreeWidget::currentItemChanged, this, &ProcessView::OnTreeItemChanged);
		connect(d->ui.openBtn, &QPushButton::clicked, this, &ProcessView::OnOpenBtnClicked);
		connect(d->ui.searchLine, &QLineEdit::textChanged, this, &ProcessView::OnSearchLineChanged);
	}

	ProcessView::~ProcessView() = default;

	auto ProcessView::OnTreeItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous) -> void {
		const auto service = d->provider->GetService<Pipeline::IProcessService>();

		if (current) {
			if (const auto type = service->GetType(current->text(0))) {
				d->selected = current->text(0);
				d->ui.openBtn->setEnabled(true);
				d->ui.descText->setText(type->GetDescription());
				d->ui.inputTree->clear();
				d->ui.outputTree->clear();

				for (const auto& i : type->GetInputTypeList())
					d->AddInput(i, ToStringList(type->GetInputFlags(i)));

				for (const auto& i : type->GetOutputTypeList())
					d->AddOutput(i, ToStringList(type->GetOutputFlags(i)));

				return;
			}
		}

		d->selected = std::nullopt;
		d->ui.openBtn->setEnabled(false);
	}

	auto ProcessView::OnIOTreeChanged(QTreeWidgetItem* item, int column) -> void {
		auto inputCount = 0;
		auto outputCount = 0;
		auto height = 0;

		for (auto i = 0; i < d->ui.inputTree->topLevelItemCount(); i++) {
			const auto* parent = d->ui.inputTree->topLevelItem(i);
			height = d->ui.inputTree->visualItemRect(parent).height();
			inputCount++;
		}

		for (auto i = 0; i < d->ui.outputTree->topLevelItemCount(); i++) {
			const auto* parent = d->ui.outputTree->topLevelItem(i);
			height = d->ui.outputTree->visualItemRect(parent).height();
			outputCount++;
		}

		d->ui.inputTree->setFixedHeight(inputCount * height);
		d->ui.outputTree->setFixedHeight(outputCount * height);
	}

	auto ProcessView::OnOpenBtnClicked() -> void {
		if (d->selected)
			emit ProcessAdded(*d->selected);
	}

	auto ProcessView::OnSearchLineChanged(const QString& text) -> void {
		for (auto i = 0; i < d->ui.tree->topLevelItemCount(); i++) {
			auto* parent = d->ui.tree->topLevelItem(i);
			auto count = 0;

			for (auto j = 0; j < parent->childCount(); j++) {
				auto* child = parent->child(j);
				child->setHidden(!text.isEmpty() && !child->text(0).toLower().contains(text.toLower()));
				count += child->isHidden() ? 1 : 0;
			}

			parent->setHidden(count == parent->childCount());
		}
	}

	auto ProcessView::keyPressEvent(QKeyEvent* event) -> void {
		if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) {
			d->ui.openBtn->clicked();
			return;
		}

		QWidget::keyPressEvent(event);
	}
}
