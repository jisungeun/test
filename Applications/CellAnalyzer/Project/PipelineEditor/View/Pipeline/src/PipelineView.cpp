#include <QMenu>

#include "PipelineView.h"

#include <QDropEvent>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QStandardPaths>

#include <RoiEditorDef.h>
#include <RoiEditorDialog.h>
#include <TCFMetaReader.h>
#include <WorksetEditorDialog.h>
#include <MaskEditorDialog.h>
#include "PresenterDialog.h"
#include "ProcessView.h"
#include "ROIWriter.h"
#include "SourceDialog.h"

#include "IAlertHandler.h"
#include "IEditorService.h"
#include "IExportService.h"
#include "IPipelineRepo.h"
#include "IPresenterRepo.h"
#include "IWindowHandler.h"

#include "ui_PipelineView.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	struct PipelineView::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		PipelineViewEventList events;
		Ui::PipelineView ui;
		bool disposed = false;

		std::shared_ptr<ProcessView> view = nullptr;
		QTreeWidgetItem* pipeline = nullptr;
		QPair<QTreeWidgetItem*, QList<QTreeWidgetItem*>> sourceList;
		QMap<QTreeWidgetItem*, QList<QTreeWidgetItem*>> processMap;

		ItemType currentType = ItemType::None;
		QString currentName;

		QString curPath;
		Project::View::WorksetEditor::TimeSelectionOutput curTimeSelection;
		QList<double> curRealTimes;

		auto GetItemType(const QTreeWidgetItem* item) const -> ItemType;
		auto AddDataToDefaultPresenter(const DataPtr& data, const QString& ID)->void;
	};

	auto PipelineView::Impl::AddDataToDefaultPresenter(const DataPtr& processData, const QString& dataID) -> void {
		if (processData) {
			const auto presenterRepo = provider->GetService<IPresenterRepo>();
			const auto defaultRender = presenterRepo->GetDefaultPresenter();
			QMetaObject::invokeMethod(qApp, [this, dataID, processData, defaultRender] {
				defaultRender->AddData(dataID, processData, "List of Images");
				});
		}
	}


	auto PipelineView::Impl::GetItemType(const QTreeWidgetItem* item) const -> ItemType {
		if (item == pipeline)
			return ItemType::Pipeline;

		if (sourceList.second.contains(const_cast<QTreeWidgetItem*>(item)))
			return ItemType::Source;

		if (processMap.keys().contains(const_cast<QTreeWidgetItem*>(item)))
			return ItemType::Process;

		for (const auto& i : processMap.values()) {
			if (i.contains(const_cast<QTreeWidgetItem*>(item)))
				return ItemType::ProcessOutput;
		}

		return ItemType::None;
	}

	PipelineView::PipelineView(Tomocube::IServiceProvider* provider) : QWidget(), IPipelineView(), d(new Impl) {
		d->provider = provider;
		d->ui.setupUi(this);
		d->ui.addBtn->setObjectName("quiet_color");
		d->ui.delBtn->setObjectName("quiet_color");
		d->ui.linkBtn->setObjectName("quiet_color");
		d->ui.roiBtn->setObjectName("quiet_color");
		d->ui.meBtn->setObjectName("quite_color");
		d->ui.tree->header()->setSectionResizeMode(0, QHeaderView::Stretch);
		d->ui.tree->header()->setSectionResizeMode(2, QHeaderView::Fixed);
		d->ui.tree->viewport()->installEventFilter(this);
		d->ui.tree->setColumnHidden(1, true);
		d->ui.tree->setColumnWidth(2, 30);
		d->view = std::make_shared<ProcessView>(d->provider);

		connect(d->view.get(), &ProcessView::ProcessAdded, this, &PipelineView::OnProcessAdded);
		connect(d->ui.addBtn, &QPushButton::clicked, this, &PipelineView::OnAddBtnClicked);
		connect(d->ui.delBtn, &QPushButton::clicked, this, &PipelineView::OnDelBtnClicked);
		connect(d->ui.linkBtn, &QPushButton::clicked, this, &PipelineView::OnLinkClicked);
		connect(d->ui.roiBtn, &QPushButton::clicked, this, &PipelineView::OnRoiClicked);
		connect(d->ui.meBtn, &QPushButton::clicked, this, &PipelineView::OnMaskEditorClicked);
		connect(d->ui.snapshotBtn, &QPushButton::clicked, this, &PipelineView::OnSnapshotClicked);
		connect(d->ui.tree, &QTreeWidget::itemSelectionChanged, this, &PipelineView::OnTreeSelectionChanged);
		connect(d->ui.tree, &QWidget::customContextMenuRequested, this, &PipelineView::OnTreeContextMenuRequested);

		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		d->pipeline = new QTreeWidgetItem(d->ui.tree, QStringList { pipeline->GetName(), "Pipeline" });
		d->pipeline->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);
		d->pipeline->setIcon(0, QIcon(":/Flat/Pipeline.svg"));
		d->pipeline->setExpanded(true);

		d->sourceList.first = new QTreeWidgetItem(d->pipeline, QStringList { "Data Sources", "SourceList" });
		d->sourceList.first->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		d->sourceList.first->setIcon(0, QIcon(":/Flat/Folder.svg"));
		d->sourceList.first->setExpanded(true);

		UpdateSourceList();
		UpdateProcessList();
	}

	PipelineView::~PipelineView() = default;

	auto PipelineView::AddEvent(const std::shared_ptr<IPipelineViewEvent>& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto PipelineView::RemoveEvent(const std::shared_ptr<IPipelineViewEvent>& event) -> void {
		d->events.removeOne(event);
	}

	auto PipelineView::SetEditable(bool editable) -> void {
		Run([this, editable] {
			d->ui.toolbar->setEnabled(editable);
		});
	}

	auto PipelineView::SetLoading(bool loading) -> void {}

	auto PipelineView::UpdatePipelineName() -> void {
		Run([this] {
			const auto repo = d->provider->GetService<IPipelineRepo>();
			const auto pipeline = repo->GetPipeline();

			d->pipeline->setText(0, pipeline->GetName());
		});
	}

	auto PipelineView::UpdateSourceList() -> void {
		Run([this] {
			const auto repo = d->provider->GetService<IPipelineRepo>();
			const auto pipeline = repo->GetPipeline();

			d->ui.tree->blockSignals(true);

			qDeleteAll(d->sourceList.second);
			d->sourceList.second.clear();

			for (const auto& s : pipeline->GetSourceList()) {
				const auto src = pipeline->GetSource(s);
				auto source = new QTreeWidgetItem(d->sourceList.first, QStringList { s, s });
				source->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
				source->setIcon(0, QIcon(":/Flat/Source.svg"));
				source->setExpanded(true);

				if (src->GetData() == nullptr)
					source->setIcon(2, QIcon(":/Flat/Warning.svg"));

				d->sourceList.second.push_back(source);

				if (d->currentType == ItemType::Source && source->text(1) == d->currentName) {
					d->ui.tree->selectionModel()->clear();
					d->ui.tree->setCurrentItem(source);
				}
			}

			d->ui.tree->blockSignals(false);

			if (d->currentType == ItemType::Source) {
				if (const auto* item = d->ui.tree->currentItem(); d->GetItemType(item) != d->currentType) {
					d->ui.tree->clearSelection();
					d->ui.tree->setCurrentItem(nullptr);
				}
			}
		});
	}

	auto PipelineView::UpdateProcessList() -> void {
		Run([this] {
			const auto repo = d->provider->GetService<IPipelineRepo>();
			const auto pipeline = repo->GetPipeline();

			d->ui.tree->blockSignals(true);

			qDeleteAll(d->processMap.keys());
			d->processMap.clear();

			auto idx = 0;
			for (const auto& p : pipeline->GetProcessList()) {
				const auto process = pipeline->GetProcess(p);
				auto procItem = new QTreeWidgetItem(d->pipeline, QStringList { QString("%1. %2").arg(++idx).arg(p), p });
				procItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDragEnabled);
				procItem->setIcon(0, QIcon(":/Flat/Process.svg"));
				procItem->setExpanded(true);

				if (d->currentType == ItemType::Process && p == d->currentName) {
					d->ui.tree->selectionModel()->clear();
					d->ui.tree->setCurrentItem(procItem);
				}

				d->processMap[procItem];

				for (const auto& o : process->GetOutputList()) {
					const auto output = process->GetOutput(o);
					const auto child = new QTreeWidgetItem(procItem, QStringList { output->GetName(), output->GetName() });
					child->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

					if (output->GetData() == nullptr)
						child->setIcon(2, QIcon(":/Flat/Warning.svg"));

					if (const auto flags = output->GetFlags(); flags.testFlag(DataFlag::Binary) || flags.testFlag(DataFlag::Label))
						child->setIcon(0, QIcon(":/Flat/Mask.svg"));
					else if (flags.testFlag(DataFlag::Measure))
						child->setIcon(0, QIcon(":/Flat/Measure.svg"));
					else
						child->setIcon(0, QIcon(":/Flat/Data.svg"));

					d->processMap[procItem].push_back(child);

					if (d->currentType == ItemType::ProcessOutput && o == d->currentName) {
						d->ui.tree->selectionModel()->clear();
						d->ui.tree->setCurrentItem(child);
					}
				}
			}

			d->ui.tree->blockSignals(false);

			if (d->currentType == ItemType::Process || d->currentType == ItemType::ProcessOutput) {
				if (const auto* item = d->ui.tree->currentItem(); d->GetItemType(item) != d->currentType) {
					d->ui.tree->clearSelection();
					d->ui.tree->setCurrentItem(nullptr);
				}
			}
		});
	}

	auto PipelineView::SelectPipeline() -> void {
		Run([this] {
			if (d->currentType == ItemType::Pipeline)
				return;

			d->ui.tree->clearSelection();
			d->ui.tree->setCurrentItem(d->pipeline);
		});
	}

	auto PipelineView::SelectSource(const QString& name) -> void {
		Run([this, name] {
			if (d->currentName == name && d->currentType == ItemType::Source)
				return;

			d->ui.tree->clearSelection();

			for (auto* s : d->sourceList.second) {
				if (s->text(1) == name) {
					d->ui.tree->setCurrentItem(s);
					break;
				}
			}
		});
	}

	auto PipelineView::SelectProcess(const QString& name) -> void {
		Run([this, name] {
			if (d->currentName == name && d->currentType == ItemType::Process)
				return;

			d->ui.tree->clearSelection();

			for (auto* p : d->processMap.keys()) {
				if (p->text(1) == name) {
					d->ui.tree->setCurrentItem(p);
					break;
				}
			}
		});
	}

	auto PipelineView::SelectProcessOutput(const QString& name) -> void {
		Run([this, name] {
			if (d->currentName == name && d->currentType == ItemType::ProcessOutput)
				return;

			d->ui.tree->clearSelection();

			for (const auto& p : d->processMap.keys()) {
				for (auto* o : d->processMap[p]) {
					if (o->text(1) == name) {
						d->ui.tree->setCurrentItem(o);
						return;
					}
				}
			}
		});
	}

	auto PipelineView::GetSelectedType() const -> ItemType {
		if (const auto& items = d->ui.tree->selectedItems(); !items.isEmpty())
			return d->GetItemType(items.first());

		return ItemType::None;
	}

	auto PipelineView::GetSelectedName() const -> QString {
		return d->currentName;
	}

	auto PipelineView::OnProcessAdded(const QString& id) -> void {
		d->ui.addBtn->setEnabled(false);

		RunAsync([this, id] {
			for (const auto& e : d->events)
				e->OnProcessAdded(id);

			Run([this] {
				d->ui.addBtn->setEnabled(true);
			});
		});
	}

	auto PipelineView::OnAddBtnClicked() -> void {
		QMenu menu(this);

		menu.addAction(QIcon(":/Widget/Add.svg"), "Add Process", [this] {
			OnAddProcBtnClicked();
		});

		menu.addAction(QIcon(":/Widget/Add.svg"), "Add Source", [this] {
			OnAddSrcBtnClicked();
		});

		menu.addAction(QIcon(":/Widget/Add.svg"), "Add Viewer", [this] {
			OnAddPresenterBtnClicked();
		});

		menu.exec(QCursor::pos());
	}

	auto PipelineView::OnDelBtnClicked() -> void {
		if (d->currentType == ItemType::Process)
			OnDelProcBtnClicked();
		else if (d->currentType == ItemType::Source)
			OnDelSrcBtnClicked();
	}

	auto PipelineView::OnMaskEditorClicked() -> void {
		if (d->curPath.isEmpty()) {
			return;
		}

		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		if (pipeline->GetSourceList().count() < 1) {
			//there is no source
			return;
		}

		auto sourceDataExist = false;
		for (const auto& sn : pipeline->GetSourceList()) {
			if (const auto source = pipeline->GetSource(sn); source->GetData()) {
				sourceDataExist = true;
				break;
			}
		}

		if (false == sourceDataExist) {
			return;
		}

		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto dialog = std::make_shared<Project::View::MaskEditor::MaskEditorDialog>();

		for (const auto& sn : pipeline->GetSourceList()) {
			dialog->AddSource(sn, pipeline->GetSource(sn)->GetData());
		}

		for (const auto& on : pipeline->GetOutputList()) {
			if (const auto output = pipeline->GetOutput(on); output->GetData()) {
				dialog->AddData(on, output->GetData());
			}
		}
		
		if (handler->ShowDialog(dialog) != QDialog::Accepted) {
			//Do nothing when ME dialog closed
			return;
		}

		const auto [maskID, modifiedMask] = dialog->GetCurrentMask();

		if(const auto targetOutput = pipeline->GetOutput(maskID)) {
			targetOutput->SetData(modifiedMask);
			d->AddDataToDefaultPresenter(targetOutput->GetData(),maskID);
			UpdateProcessList();
		}		
	}

	auto PipelineView::OnRoiClicked() -> void {
		if (d->curPath.isEmpty())
			return;
		if (d->curTimeSelection.modalities.count() < 1)
			return;
		if (d->curRealTimes.count() < 1)
			return;
		QList<Project::View::RoiEditor::Config> roiConfig;

		Project::View::RoiEditor::Config config;
		config.tcfPath = d->curPath;
		config.timeSelection = d->curTimeSelection;
		config.realTime = d->curRealTimes;

		roiConfig.append(config);

		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto dialog = std::make_shared<Project::View::RoiEditor::ROISelectionDialog>();

		dialog->SetInput(roiConfig);

		if (handler->ShowDialog(dialog) != QDialog::Accepted)
			return;

		const auto tmpROIPath = QStandardPaths::writableLocation(QStandardPaths::TempLocation) + QDir::separator() + "editorTmpROI.tcroi";
		QFile::remove(tmpROIPath);
		const auto result = dialog->GetOutput();

		if (result.count() < 1) {
			return;
		}
		if (false == result.contains(d->curPath)) {
			return;
		}
		if (result[d->curPath].count() < 1) {
			return;
		}

		TC::IO::ROIWriter roi_writer(tmpROIPath);
		roi_writer.Write(result[d->curPath].first());

		const auto service = d->provider->GetService<Editor::IEditorService>();

		const auto cutter = service->CreateEditor("ROICookieCutter");
		QVariantMap propMap;
		propMap["ROIPath"] = tmpROIPath;
		cutter->SetProperty(propMap);

		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		//Accept HT and FL
		QMap<QString, DataPtr> resultMap;
		for (const auto& sourceID : pipeline->GetSourceList()) {
			const auto source = pipeline->GetSource(sourceID);
			const auto dataFlag = source->GetFlags();
			if (dataFlag.testFlag(DataFlag::HT) || dataFlag.testFlag(DataFlag::FL)) {
				cutter->AddData(source->GetName(), source->GetData(), "List of Images");
				resultMap[source->GetName()] = cutter->GetResult("");
			}
		}

		QFile::remove(tmpROIPath);

		QStringList updatedSource;

		for (const auto& sourceID : pipeline->GetSourceList()) {
			const auto source = pipeline->GetSource(sourceID);
			if (resultMap.contains(sourceID)) {
				source->SetData(resultMap[sourceID]);
				updatedSource.append(sourceID);
			}
		}

		QString htName;
		QStringList originalSourceList = updatedSource;
		QStringList htFirst;
		for (const auto& sourceID : originalSourceList) {
			if (const auto data = pipeline->GetSource(sourceID)) {
				if (data->GetFlags().testFlag(DataFlag::HT)) {
					htFirst.append(sourceID);
					originalSourceList.removeOne(sourceID);
				}
			}
		}

		for (const auto& remains : originalSourceList) {
			htFirst.append(remains);
		}

		this->UpdateSourceList();
		for (const auto& sourceID : htFirst) {
			for (const auto e : d->events)
				e->OnSourceUpdated(sourceID);
		}
	}

	auto PipelineView::OnLinkClicked() -> void {
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		if (pipeline->GetSourceList().count() < 1) {
			QMessageBox::warning(this, "Warning", "There is no source node in current pipeline!");
			return;
		}
		const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/tcf", QApplication::applicationDirPath()).toString();

		Project::View::WorksetEditor::Config config;
		config.selectionMode = Project::View::WorksetEditor::Config::SelectionMode::Single;

		auto htCnt = 0;
		auto fl2dCnt = 0;
		auto fl3dCnt = 0;
		auto has2d { false };
		auto has3d { false };

		QString htSourceName;
		QStringList flSourceNames;
		for (const auto& sourceName : pipeline->GetSourceList()) {
			const auto source = pipeline->GetSource(sourceName);
			const auto dataFlag = source->GetFlags();
			if (dataFlag & DataFlag::HT && dataFlag & DataFlag::Volume2D) {
				config.sources.insert(Project::View::WorksetEditor::Modality::Ht2d, { source->GetName() });
				has2d = true;
				htCnt++;
				htSourceName = sourceName;
			}
			if (dataFlag & DataFlag::HT && dataFlag & DataFlag::Volume3D) {
				config.sources.insert(Project::View::WorksetEditor::Modality::Ht3d, { source->GetName() });
				has3d = true;
				htCnt++;
				htSourceName = sourceName;
			}
			if (dataFlag & DataFlag::FL && dataFlag & DataFlag::Volume2D) {
				if (config.sources.find(Project::View::WorksetEditor::Modality::Fl2d) == config.sources.end()) {
					config.sources.insert(Project::View::WorksetEditor::Modality::Fl2d, { source->GetName() });
				} else {
					auto sourceNames = config.sources[Project::View::WorksetEditor::Modality::Fl2d];
					sourceNames << source->GetName();
					config.sources[Project::View::WorksetEditor::Modality::Fl2d] = sourceNames;
				}

				flSourceNames.append(sourceName);
				has2d = true;
				fl2dCnt++;
			}
			if (dataFlag & DataFlag::FL && dataFlag & DataFlag::Volume3D) {
				if (config.sources.find(Project::View::WorksetEditor::Modality::Fl3d) == config.sources.end()) {
					config.sources.insert(Project::View::WorksetEditor::Modality::Fl3d, { source->GetName() });
				} else {
					auto sourceNames = config.sources[Project::View::WorksetEditor::Modality::Fl3d];
					sourceNames << source->GetName();
					config.sources[Project::View::WorksetEditor::Modality::Fl3d] = sourceNames;
				}

				flSourceNames.append(sourceName);
				has3d = true;
				fl3dCnt++;
			}
			if (dataFlag & DataFlag::BF && dataFlag & DataFlag::Volume2D)
				config.sources.insert(Project::View::WorksetEditor::Modality::Bf, { source->GetName() });
		}

		if (false == (has2d != has3d)) {
			QMessageBox::warning(this, "Warning", QString("Current pipeline contains unacceptable source set !\n Both 2d and 3d sources exist"));
			return;
		}

		if (htCnt > 1 || fl2dCnt > 3 || fl3dCnt > 3) {
			QMessageBox::warning(this, "Warning", QString("Current pipeline contains unacceptable source set !\n HT #: %1 FL2d#: %2 FL3d#: %3").arg(htCnt).arg(fl2dCnt).arg(fl3dCnt));
			return;
		}

		const auto path = QFileDialog::getOpenFileName(this, "Open TCF", prev, "TCF (*.tcf)");

		if (path.isEmpty())
			return;

		d->curPath = path;
		QSettings("Tomocube", "TomoAnalysis").setValue("recent/tcf", path);

		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto singleTimeDialog = std::make_shared<Project::View::WorksetEditor::TimePointSettingDialog>(config, this);
		singleTimeDialog->SetTCFs({ path });

		if (handler->ShowDialog(singleTimeDialog) != QDialog::Accepted)
			return;

		auto meta = singleTimeDialog->GetMeta(path);
		const auto timeSelection = singleTimeDialog->GetResult();

		const auto presenterRepo = d->provider->GetService<IPresenterRepo>();
		const auto presenter = presenterRepo->GetDefaultPresenter();
		const auto report = presenterRepo->GetDefaultReport();
		presenter->ClearData();
		report->ClearData();

		const auto timeSelectionOutput = timeSelection[path];
		d->curTimeSelection = timeSelectionOutput;
		d->curRealTimes = timeSelectionOutput.realTimes;
		d->curTimeSelection.times.clear();
		d->curTimeSelection.times.append(static_cast<int>(d->curRealTimes.first()));

		if (timeSelection.isEmpty())
			return;
		RunAsync([this,path, meta,htSourceName,flSourceNames] {
			const auto selection = d->curTimeSelection;
			const auto time = d->curRealTimes.first();
			int timePoint;
			auto flCnt = 0;

			for (auto i = 0; i < selection.modalities.count(); i++) {
				switch (selection.modalities[i]) {
					case Project::View::WorksetEditor::Modality::Ht2d:
						timePoint = meta->data.data2DMIP.timePoints.indexOf(time);
						for (const auto& e : d->events)
							e->OnSourceLinked(htSourceName, path, timePoint);
						break;
					case Project::View::WorksetEditor::Modality::Ht3d:
						timePoint = meta->data.data3D.timePoints.indexOf(time);
						for (const auto& e : d->events)
							e->OnSourceLinked(htSourceName, path, timePoint);
						break;
					case Project::View::WorksetEditor::Modality::Fl2dCh1:
						timePoint = meta->data.data2DFLMIP.timePoints[0].indexOf(time);
						for (const auto& e : d->events)
							e->OnSourceLinked(flSourceNames[flCnt], path, timePoint, 0);
						flCnt++;
						break;
					case Project::View::WorksetEditor::Modality::Fl2dCh2:
						timePoint = meta->data.data2DFLMIP.timePoints[1].indexOf(time);
						for (const auto& e : d->events)
							e->OnSourceLinked(flSourceNames[flCnt], path, timePoint, 1);
						flCnt++;
						break;
					case Project::View::WorksetEditor::Modality::Fl2dCh3:
						timePoint = meta->data.data2DFLMIP.timePoints[2].indexOf(time);
						for (const auto& e : d->events)
							e->OnSourceLinked(flSourceNames[flCnt], path, timePoint, 2);
						flCnt++;
						break;
					case Project::View::WorksetEditor::Modality::Fl3dCh1:
						timePoint = meta->data.data3DFL.timePoints[0].indexOf(time);
						for (const auto& e : d->events)
							e->OnSourceLinked(flSourceNames[flCnt], path, timePoint, 0);
						flCnt++;
						break;
					case Project::View::WorksetEditor::Modality::Fl3dCh2:
						timePoint = meta->data.data3DFL.timePoints[1].indexOf(time);
						for (const auto& e : d->events)
							e->OnSourceLinked(flSourceNames[flCnt], path, timePoint, 1);
						flCnt++;
						break;
					case Project::View::WorksetEditor::Modality::Fl3dCh3:
						timePoint = meta->data.data3DFL.timePoints[2].indexOf(time);
						for (const auto& e : d->events)
							e->OnSourceLinked(flSourceNames[flCnt], path, timePoint, 2);
						flCnt++;
						break;
					case Project::View::WorksetEditor::Modality::Bf:
						break;
				}
			}
		});
	}

	auto PipelineView::OnSnapshotClicked() -> void {
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto preRepo = d->provider->GetService<IPresenterRepo>();
		const auto pipeline = repo->GetPipeline();

		const auto defaultPresenter = preRepo->GetDefaultPresenter();

		if (defaultPresenter->GetDataList().count() < 1) {
			return;
		}

		const auto presenterID = QString("Slice view in 2D");
		auto portMap = QMap<QString, QStringList>();
		const auto currentDateTime = QDateTime::currentDateTime();
		const auto currentTime = currentDateTime.time();
		const auto windowTitle = QString("Snapshot (%1)").arg(currentTime.toString("hh:mm:ss"));

		QStringList originalSourceList = defaultPresenter->GetDataList();
		QStringList htFirst;
		for (const auto& sourceID : originalSourceList) {
			if (const auto data = pipeline->GetSource(sourceID)) {
				if (data->GetFlags().testFlag(DataFlag::HT)) {
					htFirst.append(sourceID);
					originalSourceList.removeOne(sourceID);
				}
			}
		}
		for (const auto& remains : originalSourceList) {
			htFirst.append(remains);
		}

		portMap["List of Images"] = htFirst;

		for (const auto& e : d->events) {
			e->OnPresenterAdded(presenterID, portMap, windowTitle);
		}
	}

	auto PipelineView::OnAddPresenterBtnClicked() -> void {
		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();
		const auto dialog = std::make_shared<PresenterDialog>(d->provider, pipeline);

		if (handler->ShowDialog(dialog) == QDialog::Accepted) {
			const auto presenterID = dialog->GetPresenterID();
			const auto portMap = dialog->GetDataLinkMap();
			const auto windowTitle = dialog->GetWindowName();

			for (const auto& e : d->events) {
				e->OnPresenterAdded(presenterID, portMap, windowTitle);
			}
		}
	}

	auto PipelineView::OnAddProcBtnClicked() -> void {
		const auto handler = d->provider->GetService<IWindowHandler>();
		handler->Show(d->view, WindowPosition::Left);
	}

	auto PipelineView::OnDelProcBtnClicked() -> void {
		d->ui.delBtn->setEnabled(false);

		RunAsync([this] {
			for (const auto& e : d->events)
				e->OnDataRemoved(d->currentName);
			for (const auto& e : d->events)
				e->OnProcessRemoved(d->currentName);

			Run([this] {
				d->ui.delBtn->setEnabled(d->currentType == ItemType::Process);
			});
		});
	}

	auto PipelineView::OnAddSrcBtnClicked() -> void {
		const auto handler = d->provider->GetService<IAlertHandler>();
		const auto dialog = std::make_shared<SourceDialog>();

		if (handler->ShowDialog(dialog) == QDialog::Accepted) {
			d->ui.addBtn->setEnabled(false);

			RunAsync([this, name = dialog->GetSourceName(), flags = dialog->GetSourceFlags()] {
				for (const auto& e : d->events)
					e->OnSourceAdded(name, flags);

				Run([this] {
					d->ui.addBtn->setEnabled(true);
				});
			});
		}
	}

	auto PipelineView::OnDelSrcBtnClicked() -> void {
		d->ui.delBtn->setEnabled(false);

		RunAsync([this, name = d->currentName] {
			for (const auto& e : d->events)			
				e->OnDataRemoved(name);

			for (const auto& e : d->events)
				e->OnSourceRemoved(name);

			Run([this] {
				d->ui.delBtn->setEnabled(d->currentType == ItemType::Source);
			});
		});
	}

	auto PipelineView::OnTreeSelectionChanged() -> void {
		if (d->disposed)
			return;

		if (const auto items = d->ui.tree->selectedItems(); !items.isEmpty()) {
			d->currentType = d->GetItemType(items.first());
			d->currentName = items.first()->text(1);
		} else {
			d->currentType = ItemType::None;
			d->currentName.clear();
		}

		d->ui.delBtn->setEnabled(d->currentType == ItemType::Source || d->currentType == ItemType::Process);
		d->ui.tree->setEnabled(false);

		RunAsync([this, type = d->currentType, name = d->currentName] {
			for (const auto& e : d->events)
				e->OnSelectionChanged(type, name);

			Run([this] {
				d->ui.tree->setEnabled(true);
			});
		});

		if (const auto items = d->ui.tree->selectedItems(); !items.isEmpty())
			if (d->sourceList.first != items.first()) { }
	}

	auto PipelineView::OnTreeContextMenuRequested(const QPoint& pos) -> void {
		QMenu menu(this);

		const auto type = d->GetItemType(d->ui.tree->itemAt(pos));

		if (type == ItemType::Pipeline) {
			menu.addAction(QIcon(":/Widget/Add.svg"), "Add Process", this, &PipelineView::OnAddProcBtnClicked);
			menu.addAction(QIcon(":/Widget/Add.svg"), "Add Source", this, &PipelineView::OnAddSrcBtnClicked);
			menu.addAction(QIcon(":/Widget/Add.svg"), "Add Viewer", this, &PipelineView::OnAddPresenterBtnClicked);
		}

		if (type == ItemType::None)
			menu.addAction(QIcon(":/Widget/Add.svg"), "Add Source", this, &PipelineView::OnAddSrcBtnClicked);

		if (type == ItemType::Process)
			menu.addAction(QIcon(":/Widget/Remove.svg"), "Remove Process", this, &PipelineView::OnDelProcBtnClicked);

		if (type == ItemType::Source)
			menu.addAction(QIcon(":/Widget/Remove.svg"), "Remove Source", this, &PipelineView::OnDelSrcBtnClicked);

		if (type == ItemType::Source || type == ItemType::ProcessOutput) {
			menu.addSeparator();

			const auto service = d->provider->GetService<IO::IExportService>();
			const auto repo = d->provider->GetService<IPipelineRepo>();
			const auto pip = repo->GetPipeline();

			if (const auto data = pip->GetData(d->currentName); data && data->GetData()) {
				const auto list = service->GetExporterList(data->GetFlags());

				for (const auto& l : list) {
					menu.addAction("Export as " + l->GetName(), this, [this, l, data] {
						const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/export", QApplication::applicationDirPath()).toString();
						if (const auto path = QFileDialog::getSaveFileName(this, "Save Data", prev, QString("%1 (*.%2)").arg(l->GetName()).arg(l->GetFormat())); !path.isEmpty()) {
							l->SetMetadata("FilePath", QFileInfo(d->curPath).fileName().chopped(4));
							l->Export(data->GetData(), path);
							QSettings("Tomocube", "TomoAnalysis").setValue("recent/export", QFileInfo(path).dir().path());
						}
					});
				}
			}
		}

		menu.exec(QCursor::pos());
	}

	auto PipelineView::eventFilter(QObject* watched, QEvent* event) -> bool {
		if (watched == d->ui.tree->viewport() && event->type() == QEvent::Drop) {
			const auto repo = d->provider->GetService<IPipelineRepo>();
			const auto pipeline = repo->GetPipeline();
			const auto* item = d->ui.tree->currentItem();
			const auto* drop = dynamic_cast<QDropEvent*>(event);
			const auto proc = pipeline->GetProcess(item->text(1));
			auto* target = d->ui.tree->itemAt(drop->pos());
			auto idx = -1;

			if (target == nullptr)
				idx = d->processMap.count();
			else if (target == d->pipeline || target == d->sourceList.first)
				idx = 0;
			else if (d->processMap.contains(target))
				idx = d->pipeline->indexOfChild(target) - 1;

			if (idx > -1) {
				pipeline->MoveProcess(proc, idx);
				UpdateProcessList();
			}
		}

		return QWidget::eventFilter(watched, event);
	}
}
