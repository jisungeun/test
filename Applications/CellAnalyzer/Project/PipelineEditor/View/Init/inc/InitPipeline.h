#pragma once

#include <optional>

#include <QDialog>
#include <QTreeWidgetItem>

#include "IServiceProvider.h"
#include "IView.h"

#include "CellAnalyzer.Project.PipelineEditor.View.InitExport.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	class CellAnalyzer_Project_PipelineEditor_View_Init_API InitPipeline final : public QDialog, public IView {
	public:
		explicit InitPipeline(Tomocube::IServiceProvider* provider);
		~InitPipeline() override;

		auto GetUrl() const -> QString;
		auto GetPath() const -> QString;
		auto SetPath(const QString& url) -> void;

	protected slots:
		auto OnStartBtnClicked() -> void;
		auto OnCreateBtnClicked() -> void;
		auto OnShowBtnClicked() -> void;
		auto OnOpenBtnClicked() -> void;
		auto OnTreeSelectionChanged() -> void;

	protected:
		auto eventFilter(QObject*, QEvent*) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
