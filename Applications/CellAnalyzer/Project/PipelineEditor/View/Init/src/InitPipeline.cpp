#include <QDesktopServices>
#include <QFileDialog>
#include <QMouseEvent>
#include <QScrollBar>

#include "InitPipeline.h"

#include "IAlertHandler.h"
#include "IDatabase.h"
#include "IPipelineService.h"
#include "IProject.h"
#include "IProjectService.h"

#include "ui_InitPipeline.h"

namespace CellAnalyzer::Project::PipelineEditor::View {
	struct InitPipeline::Impl {
		Ui::InitPipeline ui;
		Tomocube::IServiceProvider* provider = nullptr;
		QString url;
	};

	InitPipeline::InitPipeline(Tomocube::IServiceProvider* provider) : QDialog(nullptr, Qt::Dialog | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint), IView(), d(new Impl) {
		d->provider = provider;
		d->ui.setupUi(this);
		d->ui.createBtn->setVisible(false);
		d->ui.errorFrame->setVisible(false);
		d->ui.startBtn->setObjectName("accent_color");
		d->ui.showBtn->setObjectName("quiet_color");

		d->ui.tree->setColumnHidden(0, true);
		d->ui.tree->header()->setSectionResizeMode(1, QHeaderView::Stretch);
		d->ui.tree->header()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
		d->ui.tree->header()->setSectionResizeMode(3, QHeaderView::ResizeToContents);

		connect(d->ui.startBtn, &QPushButton::clicked, this, &InitPipeline::OnStartBtnClicked);
		connect(d->ui.createBtn, &QPushButton::clicked, this, &InitPipeline::OnCreateBtnClicked);
		connect(d->ui.openBtn, &QPushButton::clicked, this, &InitPipeline::OnOpenBtnClicked);
		connect(d->ui.showBtn, &QPushButton::clicked, this, &InitPipeline::OnShowBtnClicked);
		connect(d->ui.tree, &QTreeWidget::itemSelectionChanged, this, &InitPipeline::OnTreeSelectionChanged);

		const auto database = provider->GetService<IDatabase>();
		const auto project = IDatabase::GetInstance()->GetMap(QString("Project\\Pipeline"));

		if (const auto path = project.value_or(QVariantMap())["Path"].toString(); !path.isEmpty() && QDir(path).exists())
			SetPath(path);
		else {
			const auto service = provider->GetService<IProjectService>();
			const QDir dir(service->GetBasePath("Pipeline"));
			dir.mkpath(".");
			SetPath(service->GetBasePath("Pipeline"));
		}

		d->ui.tree->viewport()->installEventFilter(this);
	}

	InitPipeline::~InitPipeline() = default;

	auto InitPipeline::GetUrl() const -> QString {
		return d->url;
	}

	auto InitPipeline::GetPath() const -> QString {
		return QFileInfo(d->url).absolutePath();
	}

	auto InitPipeline::SetPath(const QString& url) -> void {
		OnCreateBtnClicked();

		d->ui.urlText->setText(url);
		d->ui.tree->clear();

		const QDir dir(url);
		const auto list = dir.entryList({ "*.tcap" });
		const auto service = d->provider->GetService<Pipeline::IPipelineService>();

		for (const auto& l : list) {
			if (const auto pipeline = service->Read(dir.filePath(l)))
				d->ui.tree->addTopLevelItem(new QTreeWidgetItem(d->ui.tree, { l, pipeline->GetName(), pipeline->GetAuthor(), pipeline->GetCreationDateTime().toString("yyyy/MM/dd HH:mm:ss") }));
		}
	}

	auto InitPipeline::OnStartBtnClicked() -> void {
		const QDir dir(d->ui.urlText->text());
		const auto service = d->provider->GetService<Pipeline::IPipelineService>();

		if (d->ui.nameText->text().isEmpty()) {
			d->ui.errorFrame->setVisible(true);
			d->ui.errorLabel->setText("Pipeline name cannot be empty");
		} else {
			if (const auto* item = d->ui.tree->currentItem()) {
				if (const auto pipeline = service->Read(dir.filePath(item->text(0)))) {
					d->url = pipeline->GetLocation();
					accept();
					close();
				}

				d->ui.errorFrame->setVisible(true);
				d->ui.errorLabel->setText("Pipeline could not be initialized");
			} else {
				const QFileInfo info(dir.filePath(d->ui.nameText->text() + ".tcap"));

				if (const auto pipeline = service->Create(info.absoluteFilePath())) {
					pipeline->SetAuthor(d->ui.authorText->text());
					pipeline->SetDescription(d->ui.descText->toPlainText());

					if (service->Save(pipeline)) {
						d->url = pipeline->GetLocation();
						accept();
						close();
					}
				}

				d->ui.errorFrame->setVisible(true);
				d->ui.errorLabel->setText("Pipeline could not be created");
			}
		}
	}

	auto InitPipeline::OnCreateBtnClicked() -> void {
		d->ui.tree->clearSelection();
		d->ui.tree->setCurrentItem(nullptr);
	}

	auto InitPipeline::OnShowBtnClicked() -> void {
		QDesktopServices::openUrl(QUrl::fromLocalFile(d->ui.urlText->text()));
	}

	auto InitPipeline::OnOpenBtnClicked() -> void {
		if (const auto path = QFileDialog::getExistingDirectory(this, "Select the location for pipelines", d->ui.urlText->text()); !path.isEmpty())
			SetPath(path);
	}

	auto InitPipeline::OnTreeSelectionChanged() -> void {
		if (const auto items = d->ui.tree->selectedItems(); !items.isEmpty()) {
			if (d->ui.startBtn->text() == "Create" && (!d->ui.nameText->text().isEmpty() || !d->ui.descText->toPlainText().isEmpty() || !d->ui.authorText->text().isEmpty())) {
				if (const auto alert = d->provider->GetService<IAlertHandler>()) {
					if (!alert->ShowYesNo("Select existing project", "Are you sure to discard the current content of the new project?")) {
						d->ui.tree->clearSelection();
						d->ui.tree->setCurrentItem(nullptr);
						return;
					}
				}
			}

			d->ui.errorFrame->setVisible(false);
			d->ui.createBtn->setVisible(true);
			d->ui.nameText->setReadOnly(true);
			d->ui.authorText->setReadOnly(true);
			d->ui.descText->setReadOnly(true);
			d->ui.startBtn->setText("Open");

			const auto service = d->provider->GetService<Pipeline::IPipelineService>();
			const auto path = QDir(d->ui.urlText->text()).filePath(items.first()->text(0));

			if (const auto pipeline = service->Read(path)) {
				d->ui.nameText->setText(pipeline->GetName());
				d->ui.authorText->setText(pipeline->GetAuthor());
				d->ui.descText->setText(pipeline->GetDescription());

				if (const auto list = pipeline->GetErrorList(); !list.isEmpty()) {
					QString error;
					for (const auto& e : list)
						error.append(e + '\n');
					error = error.mid(0, error.count() - 1);

					d->ui.errorFrame->setVisible(true);
					d->ui.errorLabel->setText("Error found in reading: " + QString::number(list.count()));
					d->ui.errorFrame->setToolTip(error);
				}
			} else {
				d->ui.nameText->clear();
				d->ui.authorText->clear();
				d->ui.descText->clear();
			}
		} else if (d->ui.startBtn->text() != "Create") {
			d->ui.errorFrame->setVisible(false);
			d->ui.nameText->clear();
			d->ui.nameText->setReadOnly(false);
			d->ui.authorText->clear();
			d->ui.authorText->setReadOnly(false);
			d->ui.descText->clear();
			d->ui.descText->setReadOnly(false);
			d->ui.startBtn->setText("Create");
			d->ui.createBtn->setVisible(false);
		}
	}

	auto InitPipeline::eventFilter(QObject* object, QEvent* event) -> bool {
		if (event->type() == QEvent::MouseButtonRelease) {
			if (const auto* arg = dynamic_cast<QMouseEvent*>(event)) {
				if (arg->button() == Qt::LeftButton && d->ui.tree->itemAt(arg->pos()) == nullptr) {
					d->ui.tree->clearSelection();
					d->ui.tree->setCurrentItem(nullptr);
				}
			}
		}

		return QDialog::eventFilter(object, event);
	}
}
