#pragma once

#include "IService.h"

#include "IPipeline.h"

#include "CellAnalyzer.Project.PipelineEditor.RepoModelExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_RepoModel_API IPipelineRepo : public virtual Tomocube::IService {
	public:
		virtual auto SetPipeline(const Pipeline::PipelinePtr& pipeline) -> void = 0;
		virtual auto GetPipeline() const -> Pipeline::PipelinePtr = 0;
	};
}
