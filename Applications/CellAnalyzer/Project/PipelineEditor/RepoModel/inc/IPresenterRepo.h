#pragma once

#include <QString>

#include "IService.h"

#include "IPresenter.h"
#include "IPipeline.h"

#include "CellAnalyzer.Project.PipelineEditor.RepoModelExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_RepoModel_API IPresenterRepo : public virtual Tomocube::IService {
	public:
		virtual auto SetPipeline(const Pipeline::PipelinePtr& pipeline) -> void = 0;

		virtual auto AddPresenter(const Presenter::PresenterPtr& presenter) -> void = 0;
		virtual auto RemovePresenter(const Presenter::PresenterPtr& presenter) -> void = 0;

		virtual auto GetDefaultReport() const -> Presenter::PresenterPtr = 0;
		virtual auto GetDefaultPresenter() const -> Presenter::PresenterPtr = 0;

		virtual auto Contains(const Presenter::PresenterPtr& presenter) const -> bool = 0;
		virtual auto GetPresenterList() const -> Presenter::PresenterList = 0;
		virtual auto GetPresenter(const QString& ID) const -> Presenter::PresenterPtr = 0;
	};
}
