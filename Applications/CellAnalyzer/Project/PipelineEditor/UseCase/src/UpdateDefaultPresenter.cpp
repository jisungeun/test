#include <QCoreApplication>
#include <QStack>
#include <QMutexLocker>

#include "UpdateDefaultPresenter.h"
#include "IView.h"

#include "IPipelineRepo.h"
#include "IPipelineView.h"
#include "IPresenterRepo.h"
#include "IPresenterService.h"
#include "IPropertyView.h"
#include "IScreenHandler.h"
#include "IVolume2D.h"
#include "IVolume3D.h"

namespace CellAnalyzer::Project::PipelineEditor {
	struct UpdateDefaultPresenter::Impl {
		QMutex mutex;

		Tomocube::IServiceProvider* provider { nullptr };
		Presenter::PresenterPtr presenter { nullptr };
		Pipeline::PipelineSourcePtr source { nullptr };
		Pipeline::ProcessOutputPtr output { nullptr };
		Pipeline::ProcessPtr process { nullptr };
		QString name;
	};

	UpdateDefaultPresenter::UpdateDefaultPresenter(Tomocube::IServiceProvider* provider) : IPropertyViewEvent(), d { new Impl } {
		d->provider = provider;
	}

	UpdateDefaultPresenter::~UpdateDefaultPresenter() = default;

	auto UpdateDefaultPresenter::OnSelectionChanged(ItemType type, const QString& name) -> void {
		QMutexLocker locker(&d->mutex);

		if (type == ItemType::Process) {
			const auto pipView = d->provider->GetService<IPipelineView>();
			const auto propView = d->provider->GetService<IPropertyView>();
			const auto repo = d->provider->GetService<IPipelineRepo>();
			const auto pipeline = repo->GetPipeline();

			d->process = pipeline->GetProcess(pipView->GetSelectedName());
			d->source = nullptr;
			d->output = nullptr;
			d->name = name;
		} else if (d->process) {
			d->process = nullptr;
			d->name = QString();
		}
		if (type == ItemType::Source) {
			const auto repo = d->provider->GetService<IPipelineRepo>();
			const auto pipeline = repo->GetPipeline();
			d->source = pipeline->GetSource(name);
			d->process = nullptr;
			d->output = nullptr;
			d->name = name;
		} else if (d->source) {
			d->source = nullptr;
			d->name = QString();
		}
		if (type == ItemType::ProcessOutput) {
			const auto repo = d->provider->GetService<IPipelineRepo>();
			const auto pipeline = repo->GetPipeline();
			d->output = pipeline->GetOutput(name);
			d->process = nullptr;
			d->source = nullptr;
			d->name = name;
		} else if (d->output) {
			d->output = nullptr;
			d->name = QString();
		}
	}

	auto UpdateDefaultPresenter::OnDataRemoved(const QString& parentName) -> void {
		const auto propView = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();
		const auto process = pipeline->GetProcess(parentName);
		const auto presenterRepo = d->provider->GetService<IPresenterRepo>();
		d->presenter = presenterRepo->GetDefaultPresenter();
		if (process) {
			for (const auto ID : process->GetOutputList())
				d->presenter->RemoveData(ID);
			return;
		}

		const auto source = pipeline->GetSource(parentName);
		if (source)
			d->presenter->RemoveData(parentName);
	}

	auto UpdateDefaultPresenter::OnExecuted(Session session) -> void {
		if (d->process) {
			return;
		}
		const auto pipView = d->provider->GetService<IPipelineView>();
		const auto propView = d->provider->GetService<IPropertyView>();

		const auto pipelineRepo = d->provider->GetService<IPipelineRepo>();
		const auto presenterRepo = d->provider->GetService<IPresenterRepo>();

		d->presenter = presenterRepo->GetDefaultPresenter();

		const auto pipeline = pipelineRepo->GetPipeline();
		const auto dataList = pipeline->GetDataList();
		if (d->source) {
			const auto sourceData = d->source->GetData();
			if (std::dynamic_pointer_cast<IVolume2D>(sourceData) || std::dynamic_pointer_cast<IVolume3D>(sourceData)) {
				for (const auto dataID : dataList) {
					if (pipeline->GetData(dataID)->GetData() == sourceData) {
						QMetaObject::invokeMethod(qApp, [this, dataID, sourceData] {
							d->presenter->RenameData(sourceData, dataID);
						});
					}
				}
			}
		} else if (d->output) {
			const auto outputData = d->output->GetData();
			if (std::dynamic_pointer_cast<IVolume2D>(outputData) || std::dynamic_pointer_cast<IVolume3D>(outputData)) {
				for (const auto dataID : dataList) {
					if (pipeline->GetData(dataID)->GetData() == outputData) {
						QMetaObject::invokeMethod(qApp, [this, dataID, outputData] {
							d->presenter->RenameData(outputData, dataID);
						});
					}
				}
			}
		}
	}

	auto UpdateDefaultPresenter::OnSourceLinked(const QString& name, const QString& path, int timePoint, int ch) -> void {
		const auto pipelineRepo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = pipelineRepo->GetPipeline();
		if (const auto source = pipeline->GetSource(name)) {
			const auto presenterRepo = d->provider->GetService<IPresenterRepo>();

			d->presenter = presenterRepo->GetDefaultPresenter();

			const auto dataList = pipeline->GetDataList();
			const auto sourceData = source->GetData();
			if (std::dynamic_pointer_cast<IVolume2D>(sourceData) || std::dynamic_pointer_cast<IVolume3D>(sourceData)) {
				for (auto dataID : dataList) {
					if (pipeline->GetData(dataID)->GetData() == sourceData) {
						QMetaObject::invokeMethod(qApp, [this, dataID, sourceData] {
							d->presenter->AddData(dataID, sourceData, "List of Images");
						});
					}
				}
			}
		}
	}

	auto UpdateDefaultPresenter::OnSourceUpdated(const QString& name) -> void {
		const auto pipelineRepo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = pipelineRepo->GetPipeline();
		if (const auto source = pipeline->GetSource(name)) {
			const auto presenterRepo = d->provider->GetService<IPresenterRepo>();

			d->presenter = presenterRepo->GetDefaultPresenter();

			const auto dataList = pipeline->GetDataList();
			const auto sourceData = source->GetData();
			if (std::dynamic_pointer_cast<IVolume2D>(sourceData) || std::dynamic_pointer_cast<IVolume3D>(sourceData)) {
				for (auto dataID : dataList) {
					if (pipeline->GetData(dataID)->GetData() == sourceData) {
						QMetaObject::invokeMethod(qApp, [this, dataID, sourceData] {
							d->presenter->AddData(dataID, sourceData, "List of Images");
						});
					}
				}
			}
		}
	}
}
