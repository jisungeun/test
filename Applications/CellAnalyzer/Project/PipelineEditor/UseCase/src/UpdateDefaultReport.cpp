#include <QCoreApplication>
#include <QMutexLocker>

#include <IMeasure.h>
#include <IView.h>

#include "IPipelineRepo.h"
#include "IPipelineView.h"
#include "IPresenterRepo.h"
#include "IPresenterService.h"

#include "UpdateDefaultReport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	struct UpdateDefaultReport::Impl {
		QMutex mutex;

		Tomocube::IServiceProvider* provider { nullptr };
		Presenter::PresenterPtr presenter { nullptr };
		Pipeline::ProcessPtr process { nullptr };
		Pipeline::ProcessOutputPtr processOutput { nullptr };
	};

	UpdateDefaultReport::UpdateDefaultReport(Tomocube::IServiceProvider* provider) : IPropertyViewEvent(), d { new Impl } {
		d->provider = provider;
	}

	UpdateDefaultReport::~UpdateDefaultReport() = default;

	auto UpdateDefaultReport::OnSelectionChanged(ItemType type, const QString& name) -> void {
		QMutexLocker locker(&d->mutex);

		d->process = nullptr;
		d->processOutput = nullptr;

		const auto pipView = d->provider->GetService<IPipelineView>();
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		if (type == ItemType::Process)
			d->process = pipeline->GetProcess(name);
		else if (type == ItemType::ProcessOutput)
			d->processOutput = pipeline->GetOutput(name);
	}

	auto UpdateDefaultReport::OnDataRemoved(const QString& parentName) -> void {
		const auto piplineRepo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = piplineRepo->GetPipeline();
		const auto process = pipeline->GetProcess(parentName);

		const auto presenterRepo = d->provider->GetService<IPresenterRepo>();
		d->presenter = presenterRepo->GetDefaultReport();

		if (process) {
			for (const auto id : process->GetOutputList()) {
				const auto output = process->GetOutput(id);
				d->presenter->RemoveData(output->GetName());
			}
		}
	}

	auto UpdateDefaultReport::OnExecuted(Session session) -> void {
		if (d->process) {
			return;
		}
		const auto presenterRepo = d->provider->GetService<IPresenterRepo>();
		d->presenter = presenterRepo->GetDefaultReport();
		if (d->processOutput) {	// from process output property view
			auto outputData = d->processOutput->GetData();
			auto name = d->processOutput->GetName();

			QMetaObject::invokeMethod(qApp, [this, outputData, name] {
				d->presenter->RenameData(outputData, name);
			});
		}
	}
}
