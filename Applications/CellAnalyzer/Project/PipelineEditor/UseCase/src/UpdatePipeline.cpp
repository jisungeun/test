#include <QMutexLocker>

#include "UpdatePipeline.h"

#include "BF.h"
#include "FL2D.h"
#include "FL3D.h"
#include "HT2D.h"
#include "HT3D.h"
#include "IPipelineRepo.h"
#include "IPipelineView.h"
#include "IProcessService.h"
#include "IPropertyView.h"

namespace CellAnalyzer::Project::PipelineEditor {
	struct UpdatePipeline::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		QMutex mutex;
		Session session = 0;
	};

	UpdatePipeline::UpdatePipeline(Tomocube::IServiceProvider* provider) : IPropertyViewEvent(), d { new Impl } {
		d->provider = provider;
	}

	UpdatePipeline::~UpdatePipeline() = default;

	auto UpdatePipeline::OnSourceLinked(const QString& name, const QString& path, int timePoint, int ch) -> void {
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();
		const auto source = pipeline->GetSource(name);

		if (source->GetFlags().testFlag(DataFlag::HT) && source->GetFlags().testFlag(DataFlag::Volume2D)) {
			if (const auto ht = std::make_shared<Data::HT2D>(path, timePoint); ht->IsValid())
				source->SetData(ht);
		} else if (source->GetFlags().testFlag(DataFlag::HT) && source->GetFlags().testFlag(DataFlag::Volume3D)) {
			if (const auto ht = std::make_shared<Data::HT3D>(path, timePoint); ht->IsValid())
				source->SetData(ht);
		} else if (source->GetFlags().testFlag(DataFlag::FL) && source->GetFlags().testFlag(DataFlag::Volume2D)) {
			if (const auto fl = std::make_shared<Data::FL2D>(path, ch, timePoint); fl->IsValid())
				source->SetData(fl);
		} else if (source->GetFlags().testFlag(DataFlag::FL) && source->GetFlags().testFlag(DataFlag::Volume3D)) {
			if (const auto fl = std::make_shared<Data::FL3D>(path, ch, timePoint); fl->IsValid())
				source->SetData(fl);
		} else if (source->GetFlags().testFlag(DataFlag::BF) && source->GetFlags().testFlag(DataFlag::Volume2D)) {
			if (const auto bf = std::make_shared<Data::BF>(path, timePoint); bf->IsValid())
				source->SetData(bf);
		} else
			source->SetData(nullptr);

		const auto pipView = d->provider->GetService<IPipelineView>();
		pipView->UpdateSourceList();
	}

	auto UpdatePipeline::OnSourceAdded(const QString& name, DataFlags flags) const -> void {
		const auto pipView = d->provider->GetService<IPipelineView>();
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		auto sname = name;
		auto idx = 1;

		while (!pipeline->AddSource(sname, flags))
			sname = QString("%1_%2").arg(name).arg(idx++);

		pipView->UpdateSourceList();
	}

	auto UpdatePipeline::OnSourceRemoved(const QString& name) -> void {
		const auto pipView = d->provider->GetService<IPipelineView>();
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		pipeline->RemoveSource(name);
		pipView->UpdateSourceList();
	}

	auto UpdatePipeline::OnProcessAdded(const QString& id) -> void {
		const auto pipView = d->provider->GetService<IPipelineView>();
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();
		const auto service = d->provider->GetService<Pipeline::IProcessService>();

		if (const auto type = service->GetType(id)) {
			auto name = type->GetID();
			auto idx = 1;

			while (pipeline->ContainsProcess(name))
				name = QString("%1_%2").arg(type->GetID()).arg(idx++);

			const auto proc = pipeline->AddProcess(name, type);
			pipView->UpdateProcessList();
		}
	}

	auto UpdatePipeline::OnProcessRemoved(const QString& name) -> void {
		const auto pipView = d->provider->GetService<IPipelineView>();
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		pipeline->RemoveProcess(name);
		pipView->UpdateProcessList();
	}

	auto UpdatePipeline::OnSelectionChanged(ItemType type, const QString& name) -> void {
		if (type != ItemType::Pipeline)
			return;

		const auto propView = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		Session temp;

		{
			QMutexLocker locker(&d->mutex);
			d->session = propView->CreateSession();
			temp = d->session;
		}

		if (auto pname = propView->CreateProperty("General", "Name", Pipeline::AttrCategory::String)) {
			pname->SetValue(pipeline->GetName());
			pname->SetState(Pipeline::AttrState::Disabled);
			propView->AddProperty(temp, std::move(pname));
		}

		if (auto author = propView->CreateProperty("General", "Author", Pipeline::AttrCategory::String)) {
			author->SetValue(pipeline->GetAuthor());
			propView->AddProperty(temp, std::move(author));
		}

		if (auto desc = propView->CreateProperty("General", "Description", Pipeline::AttrCategory::StringText)) {
			desc->SetValue(pipeline->GetDescription());
			propView->AddProperty(temp, std::move(desc));
		}

		if (auto created = propView->CreateProperty("General", "Created Time", Pipeline::AttrCategory::String)) {
			created->SetValue(pipeline->GetCreationDateTime().toString("yyyy/MM/dd HH:mm:ss"));
			created->SetState(Pipeline::AttrState::Disabled);
			propView->AddProperty(temp, std::move(created));
		}
	}

	auto UpdatePipeline::OnExecuted(Session session) -> void {
		if (d->session != session)
			return;

		Session temp;

		{
			QMutexLocker locker(&d->mutex);
			temp = d->session;
		}

		const auto pipView = d->provider->GetService<IPipelineView>();
		const auto propView = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		if (const auto author = propView->GetProperty(temp, "Author"))
			pipeline->SetAuthor(author->GetValue().toString());

		if (const auto desc = propView->GetProperty(temp, "Description"))
			pipeline->SetDescription(desc->GetValue().toString());

		pipView->UpdatePipelineName();
	}
}
