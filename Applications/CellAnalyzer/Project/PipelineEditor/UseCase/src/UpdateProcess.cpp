#include <QCoreApplication>
#include <QMutexLocker>

#include "UpdateProcess.h"

#include "IMeasure.h"
#include "IPipelineRepo.h"
#include "IPipelineView.h"
#include "IPresenterRepo.h"
#include "IPropertyView.h"
#include "IStatusBarHandler.h"
#include "IVolume2D.h"
#include "IVolume3D.h"

namespace CellAnalyzer::Project::PipelineEditor {
	struct UpdateProcess::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		Pipeline::ProcessPtr process = nullptr;
		StatusGroupPtr group = nullptr;
		QMutex mutex;

		Session session = 0;
		QString name;
		bool aborted = false;

		QMap<QString, QString> inputMap;
		QMap<QString, Pipeline::AttrValue> attrMap;

		auto Rollback() -> void;
		auto AddDataToDefaultPresenter(const Pipeline::ProcessPtr& proc) -> void;
	};

	auto UpdateProcess::Impl::AddDataToDefaultPresenter(const Pipeline::ProcessPtr& proc) -> void {
		const auto pipelineRepo = provider->GetService<IPipelineRepo>();
		const auto presenterRepo = provider->GetService<IPresenterRepo>();
		const auto outputList = proc->GetOutputList();
		const auto pipeline = pipelineRepo->GetPipeline();
		const auto dataList = pipeline->GetDataList();

		const auto defaultRender = presenterRepo->GetDefaultPresenter();
		const auto defaultReport = presenterRepo->GetDefaultReport();

		for (const auto& o : outputList) {
			const auto processOutput = proc->GetOutput(o);
			if (!processOutput) {
				continue;
			}
			const auto processData = processOutput->GetData();
			if (!processData) {
				continue;
			}
			for (const auto& dataID : dataList) {
				if (pipeline->GetData(dataID)->GetData() == processData) {
					if (std::dynamic_pointer_cast<IVolume2D>(processData) || std::dynamic_pointer_cast<IVolume3D>(processData)) {
						QMetaObject::invokeMethod(qApp, [this, dataID, processData, defaultRender] {
							defaultRender->AddData(dataID, processData, "List of Images");
						});
					} else if (std::dynamic_pointer_cast<IMeasure>(processData)) {
						QString portName = "HT";
						for (const auto& ikey : proc->GetInputList()) {
							if (const auto in = proc->GetInput(ikey); const auto idata = in->GetLinkedData()) {
								if (idata->GetFlags().testFlag(DataFlag::FL)) {
									portName = "FL";
									break;
								}
							}
						}
						QMetaObject::invokeMethod(qApp, [this, dataID, processData, defaultReport,portName] {
							defaultReport->AddData(dataID, processData, portName);
						});
					}
				}
			}
		}
	}


	auto UpdateProcess::Impl::Rollback() -> void {
		if (process) {
			for (const auto& i : inputMap.keys()) {
				const auto input = process->GetInput(i);

				if (const auto pip = process->GetParent()) {
					const auto data = pip->GetData(inputMap[i]);

					input->Link(data);
				}
			}

			for (const auto& i : attrMap.keys()) {
				const auto attr = process->GetAttribute(i);
				attr->SetValue(attrMap[i]);
			}

			inputMap.clear();
			attrMap.clear();
		}
	}

	UpdateProcess::UpdateProcess(Tomocube::IServiceProvider* provider) : IPropertyViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateProcess::~UpdateProcess() = default;

	auto UpdateProcess::OnSelectionChanged(ItemType type, const QString& name) -> void {
		if (type != ItemType::Process) {
			d->Rollback();
			return;
		}

		const auto propView = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		Session temp;
		Pipeline::ProcessPtr process;
		QMap<QString, QString> inputMap;
		QMap<QString, Pipeline::AttrValue> attrMap;

		{
			QMutexLocker locker(&d->mutex);

			d->Rollback();
			d->session = propView->CreateSession();
			d->process = pipeline->GetProcess(name);
			temp = d->session;
			process = d->process;
		}

		if (auto pname = propView->CreateProperty("General", "Name", Pipeline::AttrCategory::String)) {
			pname->SetValue(process->GetName());
			propView->AddProperty(temp, std::move(pname));
		}

		if (auto id = propView->CreateProperty("General", "Process ID", Pipeline::AttrCategory::String)) {
			id->SetValue(process->GetID());
			id->SetState(Pipeline::AttrState::Disabled);
			propView->AddProperty(temp, std::move(id));
		}

		if (auto category = propView->CreateProperty("General", "Process Category", Pipeline::AttrCategory::String)) {
			category->SetValue(ToString(process->GetCategory()));
			category->SetState(Pipeline::AttrState::Disabled);
			propView->AddProperty(temp, std::move(category));
		}

		for (const auto& i : process->GetInputList()) {
			const auto input = process->GetInput(i);
			inputMap[i] = input->GetLinkedName();

			if (auto prop = propView->CreateProperty("Input Data", i, Pipeline::AttrCategory::StringCombo)) {
				QStringList list;

				for (const auto& data : input->GetLinkableDataList())
					list.push_back(data->GetName());

				prop->SetModel(list);
				prop->SetValue(input->GetLinkedName());
				propView->AddProperty(temp, std::move(prop));
			}
		}

		for (const auto& i : process->GetAttributeList()) {
			const auto attr = process->GetAttribute(i);
			attrMap[i] = attr->GetValue();

			if (auto prop = propView->CreateProperty(attr->GetGroup(), attr->GetID(), attr->GetCategory())) {
				prop->SetModel(attr->GetModel());
				prop->SetValue(attr->GetValue());
				prop->SetDescription(attr->GetDescription());
				prop->SetState(attr->GetState());
				prop->SetModifier(attr->GetModifier());
				propView->AddProperty(temp, std::move(prop), true);
			}
		}

		propView->SetExecutable(temp, true);
		propView->SetSavable(temp, true);

		{
			QMutexLocker locker(&d->mutex);

			if (d->session == temp) {
				d->inputMap = inputMap;
				d->attrMap = attrMap;
			}
		}
	}

	auto UpdateProcess::OnPropertyChanged(Session session, const QString& name, const Pipeline::AttrValue& value) -> void {
		if (!d->process)
			return;

		if (d->session != session)
			return;

		Session temp;
		Pipeline::ProcessPtr process;

		{
			QMutexLocker locker(&d->mutex);
			temp = d->session;
			process = d->process;
		}

		const auto property = d->provider->GetService<IPropertyView>();

		if (const auto prop = property->GetProperty(temp, name); prop->GetGroup() == "General")
			return;
		else if (prop->GetGroup() == "Input Data") {
			const auto input = process->GetInput(name);

			if (const auto pip = process->GetParent()) {
				if (const auto data = pip->GetData(value.toString()))
					input->Link(data);
			}
		} else {
			if (const auto attr = process->GetAttribute(name))
				attr->SetValue(value);
		}

		for (const auto& i : process->GetAttributeList()) {
			const auto attr = process->GetAttribute(i);
			const auto prop = property->GetProperty(temp, i);

			prop->SetState(attr->GetState());
			prop->SetModel(attr->GetModel());

			if (name != prop->GetName() && prop->GetValue() != attr->GetValue())
				prop->SetValue(attr->GetValue());
		}
	}

	auto UpdateProcess::OnExecuted(Session session) -> void {
		if (d->session != session)
			return;

		Session temp;
		Pipeline::ProcessPtr process;

		{
			QMutexLocker locker(&d->mutex);
			d->aborted = false;
			temp = d->session;
			process = d->process;
		}

		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipView = d->provider->GetService<IPipelineView>();
		const auto propView = d->provider->GetService<IPropertyView>();
		const auto status = d->provider->GetService<IStatusBarHandler>();

		propView->SetAbortable(session, true);

		if (const auto name = propView->GetProperty(temp, "Name"))
			process->SetName(name->GetValue().toString());

		for (const auto& i : process->GetInputList()) {
			const auto p = propView->GetProperty(temp, i);
			const auto pipeline = process->GetParent();
			const auto data = pipeline->GetData(p->GetValue().toString());
			const auto input = process->GetInput(p->GetName());

			input->Link(data);
		}

		for (const auto& i : process->GetAttributeList()) {
			const auto attr = process->GetAttribute(i);
			const auto p = propView->GetProperty(temp, i);

			attr->SetValue(p->GetValue());
			attr->SetModifier(p->GetModifier());
		}

		d->inputMap.clear();
		d->attrMap.clear();

		QMap<Pipeline::ProcessPtr, StatusTaskPtr> procMap;
		d->group = status->AddGroup(QString("Single Run - %1").arg(process->GetName()));
		const auto precedents = process->GetPrecedents(true);

		for (const auto& p : precedents)
			procMap[p] = d->group->AddTask(p->GetName());

		for (const auto& p : precedents) {
			if (d->aborted)
				break;

			procMap[p]->SetIntermediate();

			if (p->Execute()) {
				d->AddDataToDefaultPresenter(p);
			}

			pipView->UpdateProcessList();
			procMap[p]->Finish();
		}

		if (d->group) {
			status->RemoveGroup(d->group);
			d->group = nullptr;
		}
	}

	auto UpdateProcess::OnSaved(Session session) -> void {
		if (d->session != session)
			return;

		Session temp;
		Pipeline::ProcessPtr process;

		{
			QMutexLocker locker(&d->mutex);
			d->aborted = false;
			temp = d->session;
			process = d->process;
		}

		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipView = d->provider->GetService<IPipelineView>();
		const auto propView = d->provider->GetService<IPropertyView>();
		const auto status = d->provider->GetService<IStatusBarHandler>();

		if (const auto name = propView->GetProperty(temp, "Name"))
			process->SetName(name->GetValue().toString());

		for (const auto& i : process->GetInputList()) {
			const auto p = propView->GetProperty(temp, i);
			const auto pipeline = process->GetParent();
			const auto data = pipeline->GetData(p->GetValue().toString());
			const auto input = process->GetInput(p->GetName());

			input->Link(data);
		}

		for (const auto& i : process->GetAttributeList()) {
			const auto attr = process->GetAttribute(i);
			const auto p = propView->GetProperty(temp, i);

			attr->SetValue(p->GetValue());
			attr->SetModifier(p->GetModifier());
		}

		for (const auto& o : process->GetOutputList()) {
			const auto output = process->GetOutput(o);
			output->SetData(nullptr);
		}

		pipView->UpdateProcessList();

		d->inputMap.clear();
		d->attrMap.clear();
	}

	auto UpdateProcess::OnAborted() -> void {
		if (d->group) {
			for (const auto& t : d->group->GetTaskList()) {
				if (t->IsRunning())
					t->SetError("Aborted");
			}

			d->aborted = true;
		}
	}
}
