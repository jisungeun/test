#include "UpdatePresenter.h"

#include "HT3D.h"
#include "IPipelineRepo.h"
#include "IPresenter.h"
#include "IPresenterService.h"
#include "IPresenterRepo.h"
#include "IWindowHandler.h"

namespace CellAnalyzer::Project::PipelineEditor {
	struct UpdatePresenter::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		auto GetMeasureTargetType(const QString& outputID)->QString;
	};

	auto UpdatePresenter::Impl::GetMeasureTargetType(const QString& outputID) -> QString {
		const auto repo = provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		for(const auto& procID : pipeline->GetProcessList()) {
			if(const auto proc = pipeline->GetProcess(procID)) {
				for(const auto& oID : proc->GetOutputList()) {
					if (oID == outputID) {
						for (const auto& iID : proc->GetInputList()) {
							if (const auto in = proc->GetInput(iID)) {
								if(const auto idata = in->GetLinkedData(); idata->GetFlags().testFlag(DataFlag::FL)) {
									return "FL";
								}
							}
						}
					}
				}
			}
		}

		return "HT";
	}

	UpdatePresenter::UpdatePresenter(Tomocube::IServiceProvider* provider) : IScreenEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdatePresenter::~UpdatePresenter() = default;

	auto UpdatePresenter::OnFocusChanged(const ViewPtr& current, const ViewPtr& previous) -> void {
		if (const auto presenter = std::dynamic_pointer_cast<Presenter::IPresenter>(previous)) {
			const auto repo = d->provider->GetService<IPresenterRepo>();

			for (const auto& p : repo->GetPresenterList()) {
				if (p == presenter) {
					const auto handler = d->provider->GetService<IWindowHandler>();

					for (const auto& w : p->GetWindowList()) {
						if (handler->IsClosable(w)) {
							handler->SetVisible(w, false);
						}
					}
				}
			}
		}

		if (const auto presenter = std::dynamic_pointer_cast<Presenter::IPresenter>(current)) {
			const auto repo = d->provider->GetService<IPresenterRepo>();

			for (const auto& p : repo->GetPresenterList()) {
				if (p == presenter) {
					const auto handler = d->provider->GetService<IWindowHandler>();

					ViewPtr unclosableView { nullptr };
					for (const auto& v : handler->GetViewList()) {
						if (false == handler->IsClosable(v)) {
							unclosableView = v;
							break;
						}
					}

					for (const auto& w : p->GetWindowList()) {
						if (!handler->Contains(w)) {
							if (unclosableView) {
								handler->ShowTabbed(w, unclosableView);
							} else {
								handler->Show(w, WindowPosition::Right);
							}
						}
						handler->SetVisible(w, true);
					}
				}
			}
		}
	}

	auto UpdatePresenter::OnPresenterAdded(const QString& presenterID, const QMap<QString, QStringList>& dataMap, const QString& windowTitle) -> void {
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();
		const auto preRepo = d->provider->GetService<IPresenterRepo>();
		const auto service = d->provider->GetService<Presenter::IPresenterService>();
		const auto presenter = service->CreatePresenter(presenterID);
		presenter->SetTitle(windowTitle);

		//add reference HT3d first if exist
		for (const auto& sourceID : pipeline->GetSourceList()) {
			const auto sourceData = pipeline->GetSource(sourceID);
			if (sourceData->GetFlags().testFlag(DataFlag::HT) && sourceData->GetFlags().testFlag(DataFlag::Volume3D)) {
				presenter->AddData(sourceID, sourceData->GetData(), "ReferencePort");
			}
		}

		for (const auto& portName : dataMap.keys()) {
			const auto dataList = dataMap[portName];
			for (const auto& dataID : dataList) {
				const auto data = pipeline->GetData(dataID)->GetData();
				auto modifiedPortName = portName;
				if(data->GetFlags().testFlag(DataFlag::Measure)) {
					modifiedPortName = d->GetMeasureTargetType(dataID);
				}
				presenter->AddData(dataID, data, modifiedPortName);
			}
		}
		preRepo->AddPresenter(presenter);
	}
}
