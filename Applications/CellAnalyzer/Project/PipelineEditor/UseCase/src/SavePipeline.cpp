#include "SavePipeline.h"

#include "IProjectService.h"
#include "IPipelineRepo.h"
#include "IPipelineService.h"
#include "IPresenterRepo.h"

namespace CellAnalyzer::Project::PipelineEditor {
	struct SavePipeline::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
	};

	SavePipeline::SavePipeline(Tomocube::IServiceProvider* provider) : IProjectMenuEvent(), d(new Impl) {
		d->provider = provider;
	}

	SavePipeline::~SavePipeline() = default;

	auto SavePipeline::OnPipelineSaved() -> void {
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto service = d->provider->GetService<Pipeline::IPipelineService>();

		service->Save(repo->GetPipeline());
	}

	auto SavePipeline::OnPipelineSaveAs(const QString& path) -> void {
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto service = d->provider->GetService<Pipeline::IPipelineService>();
		const auto pRepo = d->provider->GetService<IPresenterRepo>();

		if (false == service->Copy(repo->GetPipeline(), path)) {
			return;
		}

		if (const auto copiedPip = service->Read(path)) {
			repo->SetPipeline(copiedPip);
			pRepo->SetPipeline(copiedPip);
		}
	}
}
