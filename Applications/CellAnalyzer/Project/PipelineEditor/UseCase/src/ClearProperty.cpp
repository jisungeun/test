#include "ClearProperty.h"

#include "IPipelineView.h"
#include "IPropertyView.h"

namespace CellAnalyzer::Project::PipelineEditor {
	struct ClearProperty::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
	};

	ClearProperty::ClearProperty(Tomocube::IServiceProvider* provider) : IPipelineViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	ClearProperty::~ClearProperty() = default;

	auto ClearProperty::OnSelectionChanged(ItemType type, const QString& name) -> void {
		const auto pipView = d->provider->GetService<IPipelineView>();
		const auto propView = d->provider->GetService<IPropertyView>();

		if (pipView->GetSelectedType() == ItemType::None)
			propView->CreateSession();
	}
}
