#include <QMutexLocker>

#include "UpdateSource.h"

#include "BF.h"
#include "FL2D.h"
#include "FL3D.h"
#include "HT2D.h"
#include "HT3D.h"
#include "IPipelineRepo.h"
#include "IPipelineView.h"
#include "IPropertyView.h"

namespace CellAnalyzer::Project::PipelineEditor {
	struct UpdateSource::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		Pipeline::PipelineSourcePtr source = nullptr;
		Session session = 0;
		QMutex mutex;
	};

	UpdateSource::UpdateSource(Tomocube::IServiceProvider* provider) : IPropertyViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateSource::~UpdateSource() = default;

	auto UpdateSource::OnSelectionChanged(ItemType type, const QString& name) -> void {
		if (type != ItemType::Source)
			return;

		const auto propView = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		Session temp;
		Pipeline::PipelineSourcePtr source;

		{
			QMutexLocker locker(&d->mutex);
			d->session = propView->CreateSession();
			d->source = pipeline->GetSource(name);
			temp = d->session;
			source = d->source;
		}

		if (auto pname = propView->CreateProperty("General", "Name", Pipeline::AttrCategory::String)) {
			pname->SetValue(source->GetName());
			propView->AddProperty(temp, std::move(pname));
		}

		if (auto req = propView->CreateProperty("General", "Requirement", Pipeline::AttrCategory::StringCheck)) {
			req->SetModel(ToStringList(DataFlag::All));
			req->SetValue(ToStringList(source->GetFlags()));
			req->SetState(Pipeline::AttrState::Disabled);
			propView->AddProperty(temp, std::move(req));
		}

		if (auto savable = propView->CreateProperty("General", "Export", Pipeline::AttrCategory::CheckBox)) {
			savable->SetValue(source->IsAutoSave());
			propView->AddProperty(temp, std::move(savable));
		}
	}

	auto UpdateSource::OnExecuted(Session session) -> void {
		if (d->session != session)
			return;

		Session temp;
		Pipeline::PipelineSourcePtr source;

		{
			QMutexLocker locker(&d->mutex);
			temp = d->session;
			source = d->source;
		}

		const auto pipView = d->provider->GetService<IPipelineView>();
		const auto propView = d->provider->GetService<IPropertyView>();

		if (const auto name = propView->GetProperty(temp, "Name")) {
			if (source->SetName(name->GetValue().toString())) {
				pipView->UpdateSourceList();
				pipView->SelectSource(source->GetName());
			}
		}

		if (const auto savable = propView->GetProperty(temp, "Export"))
			source->SetAutoSave(savable->GetValue().toBool());
	}
}
