#include <QMutexLocker>

#include "UpdateProcessOutput.h"

#include "IPipelineRepo.h"
#include "IPipelineView.h"
#include "IPropertyView.h"

namespace CellAnalyzer::Project::PipelineEditor {
	struct UpdateProcessOutput::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
		Pipeline::ProcessOutputPtr output = nullptr;
		QMutex mutex;
		Session session = 0;
	};

	UpdateProcessOutput::UpdateProcessOutput(Tomocube::IServiceProvider* provider) : IPropertyViewEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateProcessOutput::~UpdateProcessOutput() = default;

	auto UpdateProcessOutput::OnSelectionChanged(ItemType type, const QString& name) -> void {
		if (type != ItemType::ProcessOutput)
			return;

		const auto propView = d->provider->GetService<IPropertyView>();
		const auto repo = d->provider->GetService<IPipelineRepo>();
		const auto pipeline = repo->GetPipeline();

		Session temp;
		Pipeline::ProcessOutputPtr output;

		{
			QMutexLocker locker(&d->mutex);
			d->session = propView->CreateSession();
			d->output = pipeline->GetOutput(name);
			temp = d->session;
			output = d->output;
		}

		if (auto pname = propView->CreateProperty("General", "Name", Pipeline::AttrCategory::String)) {
			pname->SetValue(output->GetName());
			propView->AddProperty(temp, std::move(pname));
		}

		if (auto req = propView->CreateProperty("General", "Requirement", Pipeline::AttrCategory::StringCheck)) {
			req->SetModel(ToStringList(DataFlag::All));
			req->SetValue(ToStringList(output->GetFlags()));
			req->SetState(Pipeline::AttrState::Disabled);
			propView->AddProperty(temp, std::move(req));
		}

		if (auto savable = propView->CreateProperty("General", "Export", Pipeline::AttrCategory::CheckBox)) {
			savable->SetValue(output->IsAutoSave());
			propView->AddProperty(temp, std::move(savable));
		}
	}

	auto UpdateProcessOutput::OnExecuted(Session session) -> void {
		if (d->session != session)
			return;

		Session temp;
		Pipeline::ProcessOutputPtr output;

		{
			QMutexLocker locker(&d->mutex);
			temp = d->session;
			output = d->output;
		}

		const auto pipView = d->provider->GetService<IPipelineView>();
		const auto propView = d->provider->GetService<IPropertyView>();

		if (const auto name = propView->GetProperty(temp, "Name")) {
			if (output->SetName(name->GetValue().toString())) {
				pipView->UpdateProcessList();
				pipView->SelectSource(output->GetName());
			}
		}

		if (const auto savable = propView->GetProperty(temp, "Export"))
			output->SetAutoSave(savable->GetValue().toBool());
	}
}
