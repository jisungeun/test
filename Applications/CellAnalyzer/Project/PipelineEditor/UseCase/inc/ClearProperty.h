#pragma once

#include "IServiceProvider.h"

#include "IPipelineViewEvent.h"

#include "CellAnalyzer.Project.PipelineEditor.UseCaseExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_UseCase_API ClearProperty final : public IPipelineViewEvent {
	public:
		explicit ClearProperty(Tomocube::IServiceProvider* provider);
		~ClearProperty() override;

		auto OnSelectionChanged(ItemType type, const QString& name) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
