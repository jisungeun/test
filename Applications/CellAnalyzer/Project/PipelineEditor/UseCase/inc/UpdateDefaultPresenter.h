#pragma once

#include "IServiceProvider.h"

#include "IPipelineViewEvent.h"
#include "IPropertyViewEvent.h"

#include "CellAnalyzer.Project.PipelineEditor.UseCaseExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_UseCase_API UpdateDefaultPresenter final : public IPipelineViewEvent, public IPropertyViewEvent {
	public:
		explicit UpdateDefaultPresenter(Tomocube::IServiceProvider* provider);
		~UpdateDefaultPresenter() override;

		auto OnSelectionChanged(ItemType type, const QString& name) -> void override;
		auto OnDataRemoved(const QString& parentName) -> void override;
		auto OnExecuted(Session session) -> void override;

		auto OnSourceLinked(const QString& name, const QString& path, int timePoint, int ch) -> void override;
		auto OnSourceUpdated(const QString& name) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
