#pragma once

#include "IServiceProvider.h"

#include "IPipelineViewEvent.h"
#include "IPropertyViewEvent.h"

#include "CellAnalyzer.Project.PipelineEditor.UseCaseExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_UseCase_API UpdateProcess final : public IPipelineViewEvent, public IPropertyViewEvent {
	public:
		explicit UpdateProcess(Tomocube::IServiceProvider* provider);
		~UpdateProcess() override;

		auto OnSelectionChanged(ItemType type, const QString& name) -> void override;
		auto OnPropertyChanged(Session session, const QString& name, const Pipeline::AttrValue& value) -> void override;

		auto OnExecuted(Session session) -> void override;
		auto OnSaved(Session session) -> void override;
		auto OnAborted() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
