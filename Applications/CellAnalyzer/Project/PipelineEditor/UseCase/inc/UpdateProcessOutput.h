#pragma once

#include "IServiceProvider.h"

#include "IPipelineViewEvent.h"
#include "IPropertyView.h"

#include "CellAnalyzer.Project.PipelineEditor.UseCaseExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_UseCase_API UpdateProcessOutput final : public IPipelineViewEvent, public IPropertyViewEvent {
	public:
		explicit UpdateProcessOutput(Tomocube::IServiceProvider* provider);
		~UpdateProcessOutput() override;

		auto OnSelectionChanged(ItemType type, const QString& name) -> void override;
		auto OnExecuted(Session session) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
