#pragma once

#include "IServiceProvider.h"

#include "IPipelineViewEvent.h"
#include "IPropertyViewEvent.h"

#include "CellAnalyzer.Project.PipelineEditor.UseCaseExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_UseCase_API UpdateDefaultReport final : public IPipelineViewEvent, public IPropertyViewEvent {
	public:
		explicit UpdateDefaultReport(Tomocube::IServiceProvider* provider);
		~UpdateDefaultReport() override;

		auto OnSelectionChanged(ItemType type, const QString& name) -> void override;
		auto OnDataRemoved(const QString& parentName) -> void override;
		auto OnExecuted(Session session) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
