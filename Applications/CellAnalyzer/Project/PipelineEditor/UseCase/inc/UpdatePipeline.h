#pragma once

#include "IServiceProvider.h"

#include "IPipelineViewEvent.h"
#include "IPropertyViewEvent.h"

#include "CellAnalyzer.Project.PipelineEditor.UseCaseExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_UseCase_API UpdatePipeline final : public IPipelineViewEvent, public IPropertyViewEvent {
	public:
		explicit UpdatePipeline(Tomocube::IServiceProvider* provider);
		~UpdatePipeline() override;

		auto OnSourceAdded(const QString& name, DataFlags flags) const -> void override;
		auto OnSourceRemoved(const QString& name) -> void override;

		auto OnProcessAdded(const QString& id) -> void override;
		auto OnProcessRemoved(const QString& name) -> void override;

		auto OnSelectionChanged(ItemType type, const QString& name) -> void override;
		auto OnExecuted(Session session) -> void override;

		auto OnSourceLinked(const QString& name, const QString& path, int timePoint, int ch) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
