#pragma once

#include "IServiceProvider.h"

#include "IScreenEvent.h"
#include "IPipelineViewEvent.h"

#include "CellAnalyzer.Project.PipelineEditor.UseCaseExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_UseCase_API UpdatePresenter final : public IScreenEvent, public IPipelineViewEvent {
	public:
		UpdatePresenter(Tomocube::IServiceProvider* provider);
		~UpdatePresenter() override;

		auto OnFocusChanged(const ViewPtr& current, const ViewPtr& previous) -> void override;
		auto OnPresenterAdded(const QString& presenterID, const QMap<QString, QStringList>& dataMap, const QString& windowTitle) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
