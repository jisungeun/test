#pragma once

#include "IServiceProvider.h"

#include "IProjectMenuEvent.h"

#include "CellAnalyzer.Project.PipelineEditor.UseCaseExport.h"

namespace CellAnalyzer::Project::PipelineEditor {
	class CellAnalyzer_Project_PipelineEditor_UseCase_API SavePipeline final : public IProjectMenuEvent {
	public:
		explicit SavePipeline(Tomocube::IServiceProvider* provider);
		~SavePipeline() override;

		auto OnPipelineSaved() -> void override;

		auto OnPipelineSaveAs(const QString& path) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
