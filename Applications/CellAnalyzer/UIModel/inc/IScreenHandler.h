#pragma once

#include "IService.h"

#include "IScreenEvent.h"
#include "IView.h"

#include "CellAnalyzer.UIModelExport.h"

namespace CellAnalyzer {
	class CellAnalyzer_UIModel_API IScreenHandler : public virtual Tomocube::IService {
	public:
		virtual auto AddEvent(const ScreenEventPtr& event) -> void = 0;
		virtual auto RemoveEvent(const ScreenEventPtr& event) -> void = 0;

		virtual auto Contains(const ViewPtr& view) const -> bool = 0;
		virtual auto GetViewPeeked() const -> ViewPtr = 0;
		virtual auto GetViewFocused() const -> ViewPtr = 0;
		virtual auto GetViewList() const -> ViewList = 0;

		virtual auto Show(const ViewPtr& view) -> void = 0;
		virtual auto Show(const ViewPtr& view, ScreenPosition position, double ratio = 1.0) -> void = 0;
		virtual auto ShowFloating(const ViewPtr& view) -> void = 0;
		virtual auto Peek(const ViewPtr& view) -> void = 0;
		virtual auto Close(const ViewPtr& view) -> void = 0;
		virtual auto CloseAll() -> void = 0;

		virtual auto IsClosable(const ViewPtr& view) const -> bool = 0;
		virtual auto IsVisible(const ViewPtr& view) const -> bool = 0;

		virtual auto SetFocus(const ViewPtr& view) -> void = 0;
		virtual auto SetClosable(const ViewPtr& view, bool closable) -> void = 0;
		virtual auto SetVisible(const ViewPtr& view, bool visible) -> void = 0;
	};
}
