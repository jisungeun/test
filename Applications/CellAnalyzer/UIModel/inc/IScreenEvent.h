#pragma once

#include "IView.h"

#include "CellAnalyzer.UIModelExport.h"

namespace CellAnalyzer {
	class IScreenEvent;
	using ScreenEventPtr = std::shared_ptr<IScreenEvent>;
	using ScreenEventList = QList<ScreenEventPtr>;

	class CellAnalyzer_UIModel_API IScreenEvent {
	public:
		virtual ~IScreenEvent();

		virtual auto OnFocusChanged(const ViewPtr& current, const ViewPtr& previous) -> void;
		virtual auto OnScreenOpened(const ViewPtr& content) -> void;
		virtual auto OnScreenClosed(const ViewPtr& content) -> void;
	};
}
