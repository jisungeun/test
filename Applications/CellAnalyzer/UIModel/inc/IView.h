#pragma once

#include <functional>
#include <memory>

#include <QObject>
#include <QString>

#include "IService.h"

#include "CellAnalyzer.UIModelExport.h"

namespace CellAnalyzer {
	class IView;
	using ViewPtr = std::shared_ptr<IView>;
	using ViewList = QList<ViewPtr>;

	enum class ToolBarPosition {
		Top,
		Left,
		Right
	};

	enum class WindowPosition {
		Top,
		Bottom,
		Left,
		Right
	};

	enum class ScreenPosition {
		Vertical,
		Horizontal
	};

	enum class RunType {
		Blocking,
		NonBlocking
	};

	class CellAnalyzer_UIModel_API IView : public virtual Tomocube::IService {
	public:
		static auto Run(const std::function<void()>& func, RunType type = RunType::NonBlocking) -> void;
		static auto Run(const std::function<void()>& func, Qt::ConnectionType type) -> void;
		static auto Run(const std::function<void()>& func, QObject* parent, Qt::ConnectionType type = Qt::AutoConnection) -> void;
		static auto RunAsync(const std::function<void()>& func) -> void;
		static auto Wait() -> void;
	};
}
