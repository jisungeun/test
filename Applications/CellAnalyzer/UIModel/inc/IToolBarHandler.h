#pragma once

#include "IView.h"

#include "IService.h"

#include "CellAnalyzer.UIModelExport.h"

namespace CellAnalyzer {
	class CellAnalyzer_UIModel_API IToolBarHandler : public virtual Tomocube::IService {
	public:
		virtual auto Contains(const ViewPtr& view) const -> bool = 0;
		virtual auto Show(const ViewPtr& view, ToolBarPosition position) -> void = 0;
		virtual auto Close(const ViewPtr& view) -> void = 0;

		virtual auto IsVisible(const ViewPtr& view) const -> bool = 0;
		virtual auto SetVisible(const ViewPtr& view, bool visible) -> void = 0;
	};
}
