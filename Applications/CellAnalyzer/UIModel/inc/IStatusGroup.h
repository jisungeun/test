#pragma once

#include "IStatusTask.h"

#include "CellAnalyzer.UIModelExport.h"

namespace CellAnalyzer {
	class IStatusGroup;
	using StatusGroupPtr = std::shared_ptr<IStatusGroup>;
	using StatusGroupList = QList<StatusGroupPtr>;

	class CellAnalyzer_UIModel_API IStatusGroup : public virtual Tomocube::IService {
	public:
		virtual auto SetName(const QString& name) -> void = 0;
		virtual auto AddTask(const QString& status) -> StatusTaskPtr = 0;

		virtual auto GetName() const -> QString = 0;
		virtual auto GetTask(int index) const -> StatusTaskPtr = 0;
		virtual auto GetTaskList() const -> StatusTaskList = 0;
		virtual auto GetTaskCount() const -> int = 0;
		virtual auto GetTaskDoneCount() const -> int = 0;
		virtual auto GetPercentage() const -> int = 0;
	};
}
