#pragma once

#include "IView.h"

#include "CellAnalyzer.UIModelExport.h"

namespace CellAnalyzer {
	class IWindowEvent;
	using WindowEventPtr = std::shared_ptr<IWindowEvent>;
	using WindowEventList = QList<WindowEventPtr>;

	class CellAnalyzer_UIModel_API IWindowEvent {
	public:
		virtual ~IWindowEvent();

		virtual auto OnWindowShown(const ViewPtr& content) -> void;
		virtual auto OnWindowHidden(const ViewPtr& content) -> void;
	};
}
