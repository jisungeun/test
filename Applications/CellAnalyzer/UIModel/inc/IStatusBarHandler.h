#pragma once

#include "IStatusGroup.h"

#include "CellAnalyzer.UIModelExport.h"

namespace CellAnalyzer {
	class CellAnalyzer_UIModel_API IStatusBarHandler : public virtual Tomocube::IService {
	public:
		virtual auto AddGroup(const QString& name = QString()) -> StatusGroupPtr = 0;
		virtual auto RemoveGroup(const StatusGroupPtr& group) -> void = 0;
		virtual auto GetGroupList() const -> StatusGroupList = 0;
	};
}
