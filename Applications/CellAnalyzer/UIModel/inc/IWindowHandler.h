#pragma once

#include "IService.h"

#include "IWindowEvent.h"

#include "CellAnalyzer.UIModelExport.h"

namespace CellAnalyzer {
	class CellAnalyzer_UIModel_API IWindowHandler : public virtual Tomocube::IService {
	public:
		virtual auto AddEvent(const WindowEventPtr& event) -> void = 0;
		virtual auto RemoveEvent(const WindowEventPtr& event) -> void = 0;

		virtual auto GetViewList() const -> ViewList = 0;
		virtual auto Contains(const ViewPtr& view) const -> bool = 0;

		virtual auto Show(const ViewPtr& view, WindowPosition position, double ratio = 0.0) -> void = 0;
		virtual auto ShowFloating(const ViewPtr& view) -> void = 0;
		virtual auto ShowTabbed(const ViewPtr& view, const ViewPtr& target) -> void = 0;
		virtual auto ShowSplit(const ViewPtr& view, const ViewPtr& target, WindowPosition position, double ratio = 0.0) -> void = 0;

		virtual auto Close(const ViewPtr& view) -> void = 0;
		virtual auto CloseAll() -> void = 0;

		virtual auto IsVisible(const ViewPtr& view) const -> bool = 0;
		virtual auto IsClosable(const ViewPtr& view) const -> bool = 0;

		virtual auto SetFocus(const ViewPtr& view) -> void = 0;
		virtual auto SetVisible(const ViewPtr& view, bool visible) -> void = 0;
		virtual auto SetClosable(const ViewPtr& view, bool closable) -> void = 0;
	};
}
