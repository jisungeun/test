#pragma once

#include "IService.h"

#include "IMenu.h"
#include "IView.h"

#include "CellAnalyzer.UIModelExport.h"

namespace CellAnalyzer {
	class CellAnalyzer_UIModel_API IMenuHandler : public virtual Tomocube::IService {
	public:
		virtual auto ContainsToolBar(const ViewPtr& view) const -> bool = 0;
		virtual auto ContainsWindow(const ViewPtr& view) const -> bool = 0;
		virtual auto ContainsScreen(const ViewPtr& view) const -> bool = 0;
		virtual auto ContainsMenu(const MenuPtr& menu) const -> bool = 0;

		virtual auto AddToolBar(const ViewPtr& view) -> void = 0;
		virtual auto AddWindow(const ViewPtr& view) -> void = 0;
		virtual auto AddScreen(const ViewPtr& view) -> void = 0;
		virtual auto AddMenu(const MenuPtr& menu) -> void = 0;

		virtual auto RemoveToolBar(const ViewPtr& view) -> void = 0;
		virtual auto RemoveWindow(const ViewPtr& view) -> void = 0;
		virtual auto RemoveScreen(const ViewPtr& view) -> void = 0;
		virtual auto RemoveMenu(const MenuPtr& menu) -> void = 0;

		virtual auto ClearToolBars() -> void = 0;
		virtual auto ClearWindows() -> void = 0;
		virtual auto ClearScreens() -> void = 0;
		virtual auto ClearMenu() -> void = 0;
	};
}
