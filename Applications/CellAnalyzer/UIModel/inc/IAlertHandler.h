#pragma once

#include <QString>

#include "IService.h"
#include "IView.h"

#include "CellAnalyzer.UIModelExport.h"

namespace CellAnalyzer {
	enum class AlertIcon {
		None,
		Critical,
		Error,
		Information
	};

	class CellAnalyzer_UIModel_API IAlertHandler : public virtual Tomocube::IService {
	public:
		virtual auto ShowMessage(const QString& title, const QString& message, AlertIcon icon = AlertIcon::None) -> void = 0;
		virtual auto ShowYesNo(const QString& title, const QString& message, AlertIcon icon = AlertIcon::None) -> bool = 0;
		virtual auto ShowDialog(const ViewPtr& view) -> int = 0;
	};
}
