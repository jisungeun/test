#pragma once

#include <memory>

#include <QString>

#include "IService.h"

#include "CellAnalyzer.UIModelExport.h"

namespace CellAnalyzer {
	class IStatusTask;
	using StatusTaskPtr = std::shared_ptr<IStatusTask>;
	using StatusTaskList = QList<StatusTaskPtr>;

	enum class TaskStatus {
		Pending,
		Intermediate,
		Running,
		Finished,
		Error
	};

	class CellAnalyzer_UIModel_API IStatusTask : public virtual Tomocube::IService {
	public:
		virtual auto SetMessage(const QString& message) -> void = 0;
		virtual auto SetError(const QString& message) -> void = 0;
		virtual auto SetProgress(int value) -> void = 0;
		virtual auto SetProgress(int value, int max) -> void = 0;
		virtual auto AddProrgress() -> void = 0;

		virtual auto SetIntermediate() -> void = 0;
		virtual auto SetPending() -> void = 0;
		virtual auto SetRunning() -> void = 0;
		virtual auto Finish() -> void = 0;

		virtual auto GetMessage() const -> QString = 0;
		virtual auto GetError() const -> QString = 0;
		virtual auto GetStatus() const -> TaskStatus = 0;
		virtual auto GetProgress() const -> int = 0;
		virtual auto IsRunning() const -> bool = 0;
		virtual auto IsFinished() const -> bool = 0;
	};
}
