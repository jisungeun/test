#pragma once

#include "IView.h"

#include "CellAnalyzer.UIModelExport.h"

namespace CellAnalyzer {
	class IMenu;
	using MenuPtr = std::shared_ptr<IMenu>;
	using MenuList = QList<MenuPtr>;

	enum class MenuParent {
		File,
		Help,
		New
	};

	class CellAnalyzer_UIModel_API IMenu : public virtual Tomocube::IService {
	public:
		virtual auto GetParent() const -> MenuParent = 0;
		
		static auto Run(const std::function<void()>& func, RunType type = RunType::NonBlocking) -> void;
		static auto RunAsync(const std::function<void()>& func) -> void;
	};
}
