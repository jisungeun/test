#include "IScreenEvent.h"

namespace CellAnalyzer {
	IScreenEvent::~IScreenEvent() = default;

	auto IScreenEvent::OnFocusChanged(const ViewPtr& current, const ViewPtr& previous) -> void {}

	auto IScreenEvent::OnScreenOpened(const ViewPtr& content) -> void {}

	auto IScreenEvent::OnScreenClosed(const ViewPtr& content) -> void {}
}
