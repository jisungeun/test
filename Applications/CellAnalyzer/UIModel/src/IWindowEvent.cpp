#include "IWindowEvent.h"

namespace CellAnalyzer {
	IWindowEvent::~IWindowEvent() = default;

	auto IWindowEvent::OnWindowShown(const ViewPtr& content) -> void {}

	auto IWindowEvent::OnWindowHidden(const ViewPtr& content) -> void {}
}
