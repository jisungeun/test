#include <QCoreApplication>
#include <QThreadPool>

#include "IView.h"

namespace {
	QThreadPool pool;
	bool waiting = false;
}

namespace CellAnalyzer {
	auto IView::Run(const std::function<void()>& func, RunType type) -> void {
		if (waiting)
			return;

		const auto* thread = QThread::currentThread();
		const auto* gui = qApp->thread();

		if (type == RunType::Blocking) {
			if (thread == gui)
				QMetaObject::invokeMethod(qApp, func, Qt::DirectConnection);
			else
				QMetaObject::invokeMethod(qApp, func, Qt::BlockingQueuedConnection);
		} else if (type == RunType::NonBlocking) {
			QMetaObject::invokeMethod(qApp, func, Qt::QueuedConnection);
		}
	}

	auto IView::Run(const std::function<void()>& func, Qt::ConnectionType type) -> void {
		if (waiting)
			return;

		Run(func, qApp, type);
	}

	auto IView::Run(const std::function<void()>& func, QObject* parent, Qt::ConnectionType type) -> void {
		if (waiting)
			return;

		if (type == Qt::BlockingQueuedConnection && QThread::currentThread() == parent->thread())
			QMetaObject::invokeMethod(parent, func, Qt::DirectConnection);
		else
			QMetaObject::invokeMethod(parent, func, type);
	}

	auto IView::RunAsync(const std::function<void()>& func) -> void {
		pool.start(func);
	}

	auto IView::Wait() -> void {
		waiting = true;
		pool.waitForDone();
		waiting = false;
	}
}
