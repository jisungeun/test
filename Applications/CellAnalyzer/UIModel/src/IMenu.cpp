#include "IMenu.h"
#include "IView.h"

namespace CellAnalyzer {
	auto IMenu::Run(const std::function<void()>& func, RunType type) -> void {
		IView::Run(func, type);
	}

	auto IMenu::RunAsync(const std::function<void()>& func) -> void {
		IView::RunAsync(func);
	}
}
