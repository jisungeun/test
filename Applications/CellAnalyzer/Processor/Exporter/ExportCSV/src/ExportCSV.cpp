#include "ExportCSV.h"

#include <fstream>
#include <iostream>

#include <MeasureFile.h>
#include <QDir>
#include <QUrl>
#include <ScalarList.h>
#include <TCFData.h>
#include <QtGui/QDesktopServices>

namespace CellAnalyzer::Processor::Export {
	auto ExportCSV::Process(const PropertyMap& properties, const DataMap& input) -> DataMap {
		DataMap map;

		QString tcfPath { "Default.tcf" };
		QString group;
		if (const auto firstsource = std::dynamic_pointer_cast<Data::TCFData>(input["FirstSource"])) {
			tcfPath = firstsource->GetPath();
			group = firstsource->GetGroup();
		}

		if (const auto measure = std::dynamic_pointer_cast<Data::ScalarList>(input["Measurement"])) {
			if (measure->GetCount() < 1)
				return map;
			QFileInfo tcfInfo(tcfPath);
			const auto tcfName = tcfInfo.fileName().chopped(4);
			const auto folderPath = properties["OutputPath"]->GetValue().toString();
			QDir dir(folderPath);
			if (false == dir.exists())
				return map;
			auto autoName = tcfName;
			const auto itemName = measure->GetName();
			if (false == itemName.isEmpty()) {
				autoName += "_";
				autoName += itemName;
			}
			const auto csvPath = folderPath + "/" + autoName + ".csv";
			if (group.isEmpty())
				group = "N/A";
			std::ofstream new_csv;
			new_csv.open(csvPath.toStdString());

			const auto measureKeys = measure->GetData()[0].keys();
			new_csv << "Group,TCF,Label #";
			//new_csv << "Label #";
			for (auto i = 0; i < measureKeys.count(); i++)
				new_csv << "," << measureKeys[i].toStdString();
			new_csv << "\n";
			for (auto i = 0; i < measure->GetCount(); i++) {
				new_csv << group.toStdString() << ",";//group
				new_csv << tcfName.toStdString() << ",";//tcf name
				new_csv << (i + 1);//Label idx;
				for (auto j = 0; j < measureKeys.count(); j++) {
					const auto val = measure->GetData()[i][measureKeys[j]].toDouble();
					new_csv << "," << QString::number(val, 'f', 4).toStdString();
				}
				if (i < measure->GetCount() - 1)
					new_csv << "\n";
			}
			new_csv.close();
			const auto measureFile = std::make_shared<Data::MeasureFile>();
			measureFile->SetFileLocation(folderPath);
			map["File"] = measureFile;
		}
		return map;
	}

	auto ExportCSV::Initialize(const PropertyPtr& property) -> void { }

	auto ExportCSV::Update(const PropertyMap& properties, const DataMap& input) -> void {
		if (properties.contains("OutputPath") && input.isEmpty()) {
			const auto path = properties["OutputPath"]->GetValue().toString();
			if (path.isEmpty())
				return;
			const QDir dir(path);
			if (false == dir.exists())
				return;
			QDesktopServices::openUrl(QUrl::fromLocalFile(path));
		}
	}
}
