#include "Label2dAsRAW.h"

#include <LabelData2d.h>
#include <MaskFile.h>
#include <QDir>
#include <QUrl>
#include <TCFData.h>
#include <TCRawWriter.h>
#include <QtGui/QDesktopServices>

namespace CellAnalyzer::Processor::Export {
	using namespace TC::IO::RawWriter;

	auto Label2dAsRAW::Process(const PropertyMap& properties, const DataMap& input) -> DataMap {
		if (!std::dynamic_pointer_cast<Data::LabelData2d>(input["InputMask"]))
			return {};
		QString tcfPath { "Default.tcf" };
		if (const auto firstsource = std::dynamic_pointer_cast<Data::TCFData>(input["FirstSource"]))
			tcfPath = firstsource->GetPath();
		const auto mask = std::dynamic_pointer_cast<Data::LabelData2d>(input["InputMask"]);

		QFileInfo tcfInfo(tcfPath);
		const auto tcfName = tcfInfo.fileName().chopped(4);
		const auto folderPath = properties["OutputPath"]->GetValue().toString();
		QDir dir(folderPath);
		if (false == dir.exists())
			return {};
		auto autoName = tcfName;
		const auto itemName = mask->GetName();
		if (false == itemName.isEmpty()) {
			autoName += "_";
			autoName += itemName;
		}
		const auto rawPath = folderPath + "/" + autoName + ".RAW";
		const auto [x, y] = mask->GetDimension();
		const auto [rx, ry] = mask->GetResolution();
		Writer writer(rawPath, { static_cast<size_t>(x), static_cast<size_t>(y), 1 }, { static_cast<float>(rx), static_cast<float>(ry), 0.0f }, Writer::Type::UINT16);
		if (false == writer.Append(reinterpret_cast<const char*>(mask->GetData()), x * y * 2, false))
			return {};
		const auto rawFile = std::make_shared<Data::MaskFile>();
		rawFile->SetFileLocation(folderPath);
		DataMap map;
		map["File"] = rawFile;
		return map;
	}

	auto Label2dAsRAW::Initialize(const PropertyPtr& property) -> void { }

	auto Label2dAsRAW::Update(const PropertyMap& properties, const DataMap& input) -> void {
		if (properties.contains("OutputPath") && input.isEmpty()) {
			const auto path = properties["OutputPath"]->GetValue().toString();
			if (path.isEmpty())
				return;
			const QDir dir(path);
			if (false == dir.exists())
				return;
			QDesktopServices::openUrl(QUrl::fromLocalFile(path));
		}
	}
}
