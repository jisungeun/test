#include "Label3dAsRAW.h"

#include <ImageDev/ImageDev.h>
#include <iolink/view/ImageViewFactory.h>

#include <LabelData3d.h>
#include <MaskFile.h>
#include <TCDataConverter.h>
#include <TCFData.h>
#include <TCRawWriter.h>

#include <QDir>
#include <QUrl>
#include <QtGui/QDesktopServices>

namespace CellAnalyzer::Processor::Export {
	using namespace TC::IO::RawWriter;

	auto Label3dAsRAW::Process(const PropertyMap& properties, const DataMap& input) -> DataMap {
		if (!std::dynamic_pointer_cast<Data::LabelData3d>(input["InputMask"]))
			return {};
		QString tcfPath { "Default.tcf" };
		if (const auto firstsource = std::dynamic_pointer_cast<Data::TCFData>(input["FirstSource"]))
			tcfPath = firstsource->GetPath();
		const auto mask = std::dynamic_pointer_cast<Data::LabelData3d>(input["InputMask"]);

		QFileInfo tcfInfo(tcfPath);
		const auto tcfName = tcfInfo.fileName().chopped(4);
		const auto folderPath = properties["OutputPath"]->GetValue().toString();
		QDir dir(folderPath);
		if (false == dir.exists())
			return {};
		auto autoName = tcfName;
		const auto itemName = mask->GetName();
		if (false == itemName.isEmpty()) {
			autoName += "_";
			autoName += itemName;
		}
		const auto rawPath = folderPath + "/" + autoName + ".RAW";
		const auto [x, y, z] = mask->GetDimension();
		const auto [rx, ry, rz] = mask->GetResolution();
		Writer writer(rawPath, { static_cast<size_t>(x), static_cast<size_t>(y), static_cast<size_t>(z) }, { static_cast<float>(rx), static_cast<float>(ry), static_cast<float>(rz) }, Writer::Type::UINT16);

		TCDataConverter convereter;
		int dim[3] { x, y, z };
		double res[3] { rx, ry, rz };
		const auto labelMask = convereter.ArrToLabelMask(mask->GetData(), dim, res);
		const auto labelView = convereter.MaskToImageView(labelMask);
		for (auto i = 0; i < z; i++) {
			auto sliced = getSliceFromVolume3d(labelView, imagedev::GetSliceFromVolume3d::Z_AXIS, i);
			const auto sliceBuffer = static_cast<unsigned short*>(sliced->buffer());
			if (false == writer.Append(reinterpret_cast<const char*>(sliceBuffer), x * y * 2, i > 0))
				return {};
		}
		const auto rawFile = std::make_shared<Data::MaskFile>();
		rawFile->SetFileLocation(folderPath);
		DataMap map;
		map["File"] = rawFile;
		return map;
	}

	auto Label3dAsRAW::Initialize(const PropertyPtr& property) -> void { }

	auto Label3dAsRAW::Update(const PropertyMap& properties, const DataMap& input) -> void {
		if (properties.contains("OutputPath") && input.isEmpty()) {
			const auto path = properties["OutputPath"]->GetValue().toString();
			if (path.isEmpty())
				return;
			const QDir dir(path);
			if (false == dir.exists())
				return;
			QDesktopServices::openUrl(QUrl::fromLocalFile(path));
		}
	}
}
