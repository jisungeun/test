#include "Image2dAsTiff.h"

#include <FLData2d.h>
#include <HTData2d.h>
#include <ImageFile.h>
#include <TCFData.h>
#include <TCTiffWriter.h>

#include <QDir>
#include <QUrl>
#include <QtGui/QDesktopServices>

namespace CellAnalyzer::Processor::Export {
	using namespace TC::IO;

	auto Image2dAsTiff::Process(const PropertyMap& properties, const DataMap& input) -> DataMap {
		QString tcfPath { "Default.tcf" };
		if (const auto firstsource = std::dynamic_pointer_cast<Data::TCFData>(input["FirstSource"]))
			tcfPath = firstsource->GetPath();
		QFileInfo tcfInfo(tcfPath);
		const auto tcfName = tcfInfo.fileName().chopped(4);
		const auto folderPath = properties["OutputPath"]->GetValue().toString();
		QDir dir(folderPath);
		if (false == dir.exists())
			return {};
		auto autoName = tcfName;
		QString itemName;

		size_t x, y;
		float rx, ry;
		uint16_t* buffer { nullptr };
		if (const auto img = std::dynamic_pointer_cast<Data::HTData2d>(input["InputImage"])) {
			buffer = img->GetData();
			const auto [ix, iy] = img->GetDimension();
			const auto [irx, iry] = img->GetResolution();
			x = static_cast<size_t>(ix);
			y = static_cast<size_t>(iy);
			rx = static_cast<float>(irx);
			ry = static_cast<float>(iry);
			itemName = img->GetName();
		} else if (const auto flimg = std::dynamic_pointer_cast<Data::FLData2d>(input["InputImage"])) {
			buffer = flimg->GetData();
			const auto [ix, iy] = flimg->GetDimension();
			const auto [irx, iry] = flimg->GetResolution();
			x = static_cast<size_t>(ix);
			y = static_cast<size_t>(iy);
			rx = static_cast<float>(irx);
			ry = static_cast<float>(iry);
			itemName = flimg->GetName();
		} else
			return {};
		if (false == itemName.isEmpty()) {
			autoName += "_";
			autoName += itemName;
		}
		const auto rawPath = folderPath + "/" + autoName + ".RAW";
		TCTiffWriter writer(TiffMetadata2D { x, y, 2, false, rawPath.toLocal8Bit() });
		writer.SetOption({ rx, ry, 0, 0, std::string(), std::string(), std::string(), std::string(), std::string(), std::string() });
		for (uint64_t i = 0; i < y; i++) {
			if (false == writer.WriteLine(buffer + (i * x)))
				return {};
		}
		const auto rawFile = std::make_shared<Data::ImageFile>();
		rawFile->SetFileLocation(folderPath);
		DataMap map;
		map["File"] = rawFile;
		return map;
	}

	auto Image2dAsTiff::Initialize(const PropertyPtr& property) -> void { }

	auto Image2dAsTiff::Update(const PropertyMap& properties, const DataMap& input) -> void {
		if (properties.contains("OutputPath") && input.isEmpty()) {
			const auto path = properties["OutputPath"]->GetValue().toString();
			if (path.isEmpty())
				return;
			const QDir dir(path);
			if (false == dir.exists())
				return;
			QDesktopServices::openUrl(QUrl::fromLocalFile(path));
		}
	}
}
