#include "Binary3dAsTiff.h"

#include <ImageDev/ImageDev.h>
#include <iolink/view/ImageViewFactory.h>

#include <BinaryData3d.h>
#include <MaskFile.h>
#include <TCDataConverter.h>
#include <TCFData.h>
#include <TCTiffWriter.h>

#include <QDir>
#include <QUrl>
#include <QtGui/QDesktopServices>

namespace CellAnalyzer::Processor::Export {
	using namespace TC::IO;

	auto Binary3dAsTiff::Process(const PropertyMap& properties, const DataMap& input) -> DataMap {
		if (!std::dynamic_pointer_cast<Data::BinaryData3d>(input["InputMask"]))
			return {};
		QString tcfPath { "Default.tcf" };
		if (const auto firstsource = std::dynamic_pointer_cast<Data::TCFData>(input["FirstSource"]))
			tcfPath = firstsource->GetPath();
		const auto mask = std::dynamic_pointer_cast<Data::BinaryData3d>(input["InputMask"]);

		QFileInfo tcfInfo(tcfPath);
		const auto tcfName = tcfInfo.fileName().chopped(4);
		const auto folderPath = properties["OutputPath"]->GetValue().toString();
		QDir dir(folderPath);
		if (false == dir.exists())
			return {};
		auto autoName = tcfName;
		const auto itemName = mask->GetName();
		if (false == itemName.isEmpty()) {
			autoName += "_";
			autoName += itemName;
		}
		const auto rawPath = folderPath + "/" + autoName + ".tiff";
		const auto [x, y, z] = mask->GetDimension();
		const auto [rx, ry, rz] = mask->GetResolution();
		TCTiffWriter writer(TiffMetadata3D { static_cast<uint64_t>(x), static_cast<uint64_t>(y), static_cast<uint64_t>(z), 2, false, rawPath.toLocal8Bit() });
		writer.SetOption({ rx, ry, 0, 0, std::string(), std::string(), std::string(), std::string(), std::string(), std::string() });
		TCDataConverter convereter;
		int dim[3] { x, y, z };
		double res[3] { rx, ry, rz };
		const auto labelMask = convereter.ArrToLabelMask(mask->GetData(), dim, res);
		const auto labelView = convereter.MaskToImageView(labelMask);
		for (auto i = 0; i < z; i++) {
			auto sliced = getSliceFromVolume3d(labelView, imagedev::GetSliceFromVolume3d::Z_AXIS, i);
			const auto sliceBuffer = static_cast<unsigned short*>(sliced->buffer());
			for (auto j = 0; j < y; j++) {
				if (!writer.WriteLine(sliceBuffer + (j * x)))
					return {};
			}
		}
		const auto rawFile = std::make_shared<Data::MaskFile>();
		rawFile->SetFileLocation(folderPath);
		DataMap map;
		map["File"] = rawFile;
		return map;
	}

	auto Binary3dAsTiff::Initialize(const PropertyPtr& property) -> void { }

	auto Binary3dAsTiff::Update(const PropertyMap& properties, const DataMap& input) -> void {
		if (properties.contains("OutputPath") && input.isEmpty()) {
			const auto path = properties["OutputPath"]->GetValue().toString();
			if (path.isEmpty())
				return;
			const QDir dir(path);
			if (false == dir.exists())
				return;
			QDesktopServices::openUrl(QUrl::fromLocalFile(path));
		}
	}
}
