#pragma once

#include "IProcessor.h"

namespace CellAnalyzer::Processor::Export {
	class Binary3dAsRAW final : public IProcessor {
		Q_OBJECT
		Q_INTERFACES(CellAnalyzer::Pipeline::PluginProcess)
		Q_PLUGIN_METADATA(IID "CellAnalyzer::Processor::Export::Binary3dAsRAW" FILE "../rsc/TypeInfo.json")

	public:
		auto Process(const PropertyMap& properties, const DataMap& input) -> DataMap override;
		auto Initialize(const PropertyPtr& property) -> void override;
		auto Update(const PropertyMap& properties, const DataMap& input) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
