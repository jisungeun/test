#include "Image3dAsTiff.h"


#include <ImageDev/ImageDev.h>
#include <iolink/view/ImageViewFactory.h>

#include <FLData3d.h>
#include <HTData3d.h>
#include <ImageFile.h>
#include <TCDataConverter.h>
#include <TCFData.h>
#include <TCTiffWriter.h>

#include <QDir>
#include <QUrl>
#include <QtGui/QDesktopServices>

namespace CellAnalyzer::Processor::Export {
	using namespace TC::IO;

	auto Image3dAsTiff::Process(const PropertyMap& properties, const DataMap& input) -> DataMap {
		QString tcfPath { "Default.tcf" };
		if (const auto firstsource = std::dynamic_pointer_cast<Data::TCFData>(input["FirstSource"]))
			tcfPath = firstsource->GetPath();
		QFileInfo tcfInfo(tcfPath);
		const auto tcfName = tcfInfo.fileName().chopped(4);
		const auto folderPath = properties["OutputPath"]->GetValue().toString();
		QDir dir(folderPath);
		if (false == dir.exists())
			return {};
		auto autoName = tcfName;
		QString itemName;

		size_t x, y, z;
		float rx, ry, rz;
		int dim[3];
		double res[3];
		uint16_t* buffer { nullptr };
		if (const auto img = std::dynamic_pointer_cast<Data::HTData3d>(input["InputImage"])) {
			buffer = img->GetData();
			const auto [ix, iy, iz] = img->GetDimension();
			const auto [irx, iry, irz] = img->GetResolution();
			dim[0] = ix;
			dim[1] = iy;
			dim[2] = iz;
			res[0] = irx;
			res[1] = iry;
			res[2] = irz;
			x = static_cast<size_t>(ix);
			y = static_cast<size_t>(iy);
			z = static_cast<size_t>(iz);
			rx = static_cast<float>(irx);
			ry = static_cast<float>(iry);
			rz = static_cast<float>(irz);
			itemName = img->GetName();
		} else if (const auto flimg = std::dynamic_pointer_cast<Data::FLData3d>(input["InputImage"])) {
			buffer = flimg->GetData();
			const auto [ix, iy, iz] = flimg->GetDimension();
			const auto [irx, iry, irz] = flimg->GetResolution();
			dim[0] = ix;
			dim[1] = iy;
			dim[2] = iz;
			res[0] = irx;
			res[1] = iry;
			res[2] = irz;
			x = static_cast<size_t>(ix);
			y = static_cast<size_t>(iy);
			z = static_cast<size_t>(iz);
			rx = static_cast<float>(irx);
			ry = static_cast<float>(iry);
			rz = static_cast<float>(irz);
			itemName = flimg->GetName();
		} else
			return {};

		TCDataConverter converter;

		const auto tcimage = converter.ArrToImage(buffer, dim, res);
		const auto imageView = converter.ImageToImageView(tcimage);
		if (false == itemName.isEmpty()) {
			autoName += "_";
			autoName += itemName;
		}
		const auto rawPath = folderPath + "/" + autoName + ".RAW";
		TCTiffWriter writer(TiffMetadata3D { x, y, z, 2, false, rawPath.toLocal8Bit() });
		writer.SetOption({ rx, ry, 0, 0, std::string(), std::string(), std::string(), std::string(), std::string(), std::string() });

		for (auto i = 0; i < z; i++) {
			auto sliced = getSliceFromVolume3d(imageView, imagedev::GetSliceFromVolume3d::Z_AXIS, i);
			const auto sliceBuffer = static_cast<unsigned short*>(sliced->buffer());
			for (auto j = 0; j < y; j++) {
				if (!writer.WriteLine(sliceBuffer + (j * x)))
					return {};
			}
		}

		const auto rawFile = std::make_shared<Data::ImageFile>();
		rawFile->SetFileLocation(folderPath);
		DataMap map;
		map["File"] = rawFile;
		return map;
	}

	auto Image3dAsTiff::Initialize(const PropertyPtr& property) -> void { }

	auto Image3dAsTiff::Update(const PropertyMap& properties, const DataMap& input) -> void {
		if (properties.contains("OutputPath") && input.isEmpty()) {
			const auto path = properties["OutputPath"]->GetValue().toString();
			if (path.isEmpty())
				return;
			const QDir dir(path);
			if (false == dir.exists())
				return;
			QDesktopServices::openUrl(QUrl::fromLocalFile(path));
		}
	}
}
