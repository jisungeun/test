#include "Label2dAsTiff.h"

#include <LabelData2d.h>
#include <MaskFile.h>
#include <QDir>
#include <QUrl>
#include <TCFData.h>
#include <TCTiffWriter.h>
#include <QtGui/QDesktopServices>

namespace CellAnalyzer::Processor::Export {
	using namespace TC::IO;

	auto Label2dAsTiff::Process(const PropertyMap& properties, const DataMap& input) -> DataMap {
		if (!std::dynamic_pointer_cast<Data::LabelData2d>(input["InputMask"]))
			return {};
		QString tcfPath { "Default.tcf" };
		if (const auto firstsource = std::dynamic_pointer_cast<Data::TCFData>(input["FirstSource"]))
			tcfPath = firstsource->GetPath();
		const auto mask = std::dynamic_pointer_cast<Data::LabelData2d>(input["InputMask"]);

		QFileInfo tcfInfo(tcfPath);
		const auto tcfName = tcfInfo.fileName().chopped(4);
		const auto folderPath = properties["OutputPath"]->GetValue().toString();
		QDir dir(folderPath);
		if (false == dir.exists())
			return {};
		auto autoName = tcfName;
		const auto itemName = mask->GetName();
		if (false == itemName.isEmpty()) {
			autoName += "_";
			autoName += itemName;
		}
		const auto rawPath = folderPath + "/" + autoName + ".tiff";
		const auto [x, y] = mask->GetDimension();
		const auto [rx, ry] = mask->GetResolution();
		TCTiffWriter writer(TiffMetadata2D { static_cast<uint64_t>(x), static_cast<uint64_t>(y), 2, false, rawPath.toLocal8Bit() });
		writer.SetOption({ rx, ry, 0, 0, std::string(), std::string(), std::string(), std::string(), std::string(), std::string() });
		for (uint64_t i = 0; i < y; i++) {
			if (false == writer.WriteLine(mask->GetData() + (i * x)))
				return {};
		}
		const auto rawFile = std::make_shared<Data::MaskFile>();
		rawFile->SetFileLocation(folderPath);
		DataMap map;
		map["File"] = rawFile;
		return map;
	}

	auto Label2dAsTiff::Initialize(const PropertyPtr& property) -> void { }

	auto Label2dAsTiff::Update(const PropertyMap& properties, const DataMap& input) -> void {
		if (properties.contains("OutputPath") && input.isEmpty()) {
			const auto path = properties["OutputPath"]->GetValue().toString();
			if (path.isEmpty())
				return;
			const QDir dir(path);
			if (false == dir.exists())
				return;
			QDesktopServices::openUrl(QUrl::fromLocalFile(path));
		}
	}
}
