#pragma once

#include "IProcessor.h"

namespace CellAnalyzer::Processor::Export {
	class Label2dAsTiff final : public IProcessor {
		Q_OBJECT
		Q_INTERFACES(CellAnalyzer::Pipeline::PluginProcess)
		Q_PLUGIN_METADATA(IID "CellAnalyzer::Processor::Export::Label2dAsTiff" FILE "../rsc/TypeInfo.json")

	public:
		auto Process(const PropertyMap& properties, const DataMap& input) -> DataMap override;
		auto Initialize(const PropertyPtr& property) -> void override;
		auto Update(const PropertyMap& properties, const DataMap& input) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
