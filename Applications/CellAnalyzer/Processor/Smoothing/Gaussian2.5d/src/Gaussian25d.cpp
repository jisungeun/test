#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCDataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "Gaussian25d.h"

namespace CellAnalyzer::Processor::Smoothing {
	using namespace imagedev;
	using namespace iolink;

	struct Gaussian25d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	Gaussian25d::Gaussian25d() : d { std::make_unique<Impl>() } { }

	Gaussian25d::~Gaussian25d() { }

	auto Gaussian25d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Standard Deviation X") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		} else if (id == "Standard Deviation Y") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		}
	}

	auto Gaussian25d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto Gaussian25d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto Gaussian25d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto Gaussian25d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto Gaussian25d::Process() -> DataMap {
		// create an input data
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset { 0 };
		bool isFL = false;
		bool isFloat = false;
		int timestep { 0 };
		int chIdx { 0 };
		QString chName;
		TCDataConverter converter;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
			chIdx = image->GetChannelIndex();
			chName = image->GetChannelName();
			isFL = true;
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			isFloat = true;
		} else
			return {};

		// set algorithm parameters and input data
		const auto SDx = d->attrMap["Standard Deviation X"]->GetAttrValue().toDouble();
		const auto SDy = d->attrMap["Standard Deviation Y"]->GetAttrValue().toDouble();
		try {
			const auto imageStat = intensityStatistics(refImageView, IntensityStatistics::MIN_MAX, { 0, 1 });

			const auto isZeroPaded = imageStat->minimum() < min;

			auto destImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			for (auto i = 0; i < dimZ; i++) {
				auto sliced = getSliceFromVolume3d(refImageView, GetSliceFromVolume3d::Z_AXIS, i);
				if (isZeroPaded) {
					const auto thersholded = thresholding(sliced, { -INT_MAX, static_cast<double>(min) });
					const auto reseted = resetImage(sliced, min);
					sliced = combineByMask(reseted, sliced, thersholded);
				}
				const auto floatImage = convertImage(sliced, ConvertImage::FLOAT_32_BIT);

				const auto filteredImage = gaussianFilter2d(floatImage, GaussianFilter2d::FilterMode::SEPARABLE, { SDx, SDy }, 2.0, GaussianFilter2d::OutputType::SAME_AS_INPUT, false);

				destImage = setSliceToVolume3d(destImage, filteredImage, SetSliceToVolume3d::Z_AXIS, i);
			}
			const auto resultStat = intensityStatistics(destImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result { nullptr };
			if (isFL) {
				const auto processedImage = convertImage(destImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToFLData<Data::FL3D>(static_cast<uint16_t*>(processedImage->buffer()), chIdx, chName, dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			} else if (isFloat) {
				result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(destImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			} else {
				const auto processedImage = convertImage(destImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToHTData<Data::HT3D>(static_cast<uint16_t*>(processedImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			}
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto Gaussian25d::Abort() -> void { }
}
