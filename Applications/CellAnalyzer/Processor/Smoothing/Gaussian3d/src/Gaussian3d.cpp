#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCDataConverter.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include "Gaussian3d.h"

namespace CellAnalyzer::Processor::Smoothing {
	using namespace imagedev;
	using namespace iolink;

	struct Gaussian3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	Gaussian3d::Gaussian3d() : d { std::make_unique<Impl>() } { }

	Gaussian3d::~Gaussian3d() { }

	auto Gaussian3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Standard Deviation X") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		} else if (id == "Standard Deviation Y") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		} else if (id == "Standard Deviation Z") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		}
	}

	auto Gaussian3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto Gaussian3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto Gaussian3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto Gaussian3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto Gaussian3d::Process() -> DataMap {
		// create an input data
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset { 0 };
		int timestep { 0 };
		TCDataConverter converter;
		bool isFL { false };
		bool isFloat { false };
		int chIdx { 0 };
		QString chName;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			chIdx = image->GetChannelIndex();
			chName = image->GetChannelName();
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
			isFL = true;
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			isFloat = true;
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		// set algorithm parameters and input data
		const auto SDx = d->attrMap["Standard Deviation X"]->GetAttrValue().toDouble();
		const auto SDy = d->attrMap["Standard Deviation Y"]->GetAttrValue().toDouble();
		const auto SDz = d->attrMap["Standard Deviation Z"]->GetAttrValue().toDouble();

		try {
			const auto imageStat = intensityStatistics(refImageView, IntensityStatistics::MIN_MAX, { 0, 1 });
			if (imageStat->minimum() < min) {
				//stitching data with 0 value
				const auto thersholded = thresholding(refImageView, { -INT_MAX, static_cast<double>(min) });
				const auto reseted = resetImage(refImageView, min);
				refImageView = combineByMask(reseted, refImageView, thersholded);
			}
			const auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			const auto filteredImage = gaussianFilter3d(floatImage, GaussianFilter3d::SEPARABLE, { SDx, SDy, SDz }, 2.0, GaussianFilter3d::OutputType::SAME_AS_INPUT, false);

			const auto resultStat = intensityStatistics(filteredImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result { nullptr };
			if (isFL) {
				const auto processedImage = convertImage(filteredImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToFLData<Data::FL3D>(static_cast<uint16_t*>(processedImage->buffer()), chIdx, chName, dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			} else if (isFloat) {
				result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(filteredImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			} else {
				const auto processedImage = convertImage(filteredImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToHTData<Data::HT3D>(static_cast<uint16_t*>(processedImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			}
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto Gaussian3d::Abort() -> void { }
}
