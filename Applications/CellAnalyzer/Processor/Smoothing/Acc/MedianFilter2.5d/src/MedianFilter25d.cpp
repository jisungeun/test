#include <variant>
#include <optional>

//#define TEST_PERF
#ifdef TEST_PERF
#include <chrono>
#endif

#include <arrayfire.h>
#undef min
#undef max
#undef DIFFERENCE

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCDataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include "MedianFilter25d.h"

namespace CellAnalyzer::Processor::Smoothing::Acc {
    using namespace imagedev;
    using namespace iolink;

    struct MedianFilter25d::Impl {
        using DataVariant = std::variant<
            std::shared_ptr<Data::HT3D>,
            std::shared_ptr<Data::FL3D>,
            std::shared_ptr<Data::Float3D>
        >;
        static auto ConvertToVariant(const DataPtr& base)->std::optional<DataVariant>;

        template<typename... Ts>
        struct Overload : Ts... { using Ts::operator()...; };
        template<typename... Ts> Overload(Ts...)->Overload<Ts...>;

        static auto GetMinClamped(const std::shared_ptr<ImageView>& img, double min)->std::shared_ptr<ImageView>;
        static auto FilterImage(const std::shared_ptr<ImageView>& img, int kernelSize)->std::shared_ptr<ImageView>;

        QMap<QString, DataPtr> inputMap;
        QMap<QString, ProcessorAttrPtr> attrMap;
    };

    auto MedianFilter25d::Impl::ConvertToVariant(const DataPtr& base) -> std::optional<DataVariant> {
        if (auto ht = std::dynamic_pointer_cast<Data::HT3D>(base)) return ht;
        if (auto fl = std::dynamic_pointer_cast<Data::FL3D>(base)) return fl;
        if (auto f = std::dynamic_pointer_cast<Data::Float3D>(base)) return f;
        return std::nullopt;
    }

    auto MedianFilter25d::Impl::GetMinClamped(const std::shared_ptr<ImageView>& img, double min)->std::shared_ptr<ImageView> {
        const auto clampRegion = thresholding(img, { std::numeric_limits<int>::min(), min });
        const auto reset = resetImage(img, min);
        return combineByMask(reset, img, clampRegion);
    }

    auto MedianFilter25d::Impl::FilterImage(const std::shared_ptr<ImageView>& img, int kernelSize)->std::shared_ptr<ImageView> {
        const auto dim = VectorXi64(img->shape());
        const auto floatImage = convertImage(img, ConvertImage::FLOAT_32_BIT);
        const auto blob = static_cast<float*>(floatImage->buffer());
        auto arr = af::array(dim[0], dim[1], dim[2], blob);
        arr.eval();
        arr = af::medfilt(arr, kernelSize, kernelSize, AF_PAD_SYM); // AF_PAD_CLAMP_TO_EDGE makes crash
        arr.eval();
        arr.host(blob);
        return floatImage;
    }

    MedianFilter25d::MedianFilter25d() : d{std::make_unique<Impl>()} {
    }

    MedianFilter25d::~MedianFilter25d() = default;

    auto MedianFilter25d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
        d->attrMap[attrID] = attribute;
        if (const auto& id = attrID; id == "Kernel Radius") {
            attribute->SetAttrModel(QVariantMap{{"Min", 1}, {"Max", 10}, {"Step", 1}});
        }
    }

    auto MedianFilter25d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
        if (false == d->attrMap.contains(attrID)) {
            return;
        }
        d->attrMap[attrID]->SetAttrValue(value);
    }

    auto MedianFilter25d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
        d->inputMap[inputID] = data;
    }

    auto MedianFilter25d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
        return d->attrMap[attrID];
    }

    auto MedianFilter25d::GetInputData(const QString& inputID) const -> DataPtr {
        return d->inputMap[inputID];
    }

    auto MedianFilter25d::Process() -> DataMap {
#ifdef TEST_PERF
        using std::chrono::high_resolution_clock;
        using std::chrono::duration_cast;
        using std::chrono::duration;
        using std::chrono::milliseconds;

        const auto startTime = high_resolution_clock::now();
#endif
        if (false == d->inputMap.contains("InputImage")) {
            return {};
        }

        const auto inputImageOpt = Impl::ConvertToVariant(d->inputMap["InputImage"]);
        if (false == inputImageOpt.has_value()) {
            return {};
        }

        const auto& inputImageVar = inputImageOpt.value();
        const auto [dimX, dimY, dimZ] = std::visit(Impl::Overload{
            [](const std::shared_ptr<Data::HT3D>& img) {return img->GetSize(); },
            [](const std::shared_ptr<Data::FL3D>& img) {return img->GetSize(); },
            [](const std::shared_ptr<Data::Float3D>& img) {return img->GetSize(); }
            }, inputImageVar);
        int dim[3] = { dimX, dimY, dimZ };
        const auto [resX, resY, resZ] = std::visit(Impl::Overload{
            [](const std::shared_ptr<Data::HT3D>& img) {return img->GetResolution(); },
            [](const std::shared_ptr<Data::FL3D>& img) {return img->GetResolution(); },
            [](const std::shared_ptr<Data::Float3D>& img) {return img->GetResolution(); }
            }, inputImageVar);
        double res[3]{ resX, resY, resZ };
        const auto timeStep = std::visit(Impl::Overload{
            [](const std::shared_ptr<Data::HT3D>& img) {return img->GetTimeStep(); },
            [](const std::shared_ptr<Data::FL3D>& img) {return img->GetTimeStep(); },
            [](const std::shared_ptr<Data::Float3D>& img) {return img->GetTimeStep(); }
            }, inputImageVar);
        auto converter = TCDataConverter{};
        const auto refImageViewVisitor = Impl::Overload{
            [&converter](const std::shared_ptr<Data::HT3D>& img) {return converter.ImageToImageView(Data::DataConverter::ConvertToTCImage(img)); },
            [&converter](const std::shared_ptr<Data::FL3D>& img) {return converter.ImageToImageView(Data::DataConverter::ConvertToTCImage(img)); },
            [&converter, &dim, &res](const std::shared_ptr<Data::Float3D>& img) {
                return converter.FloatArrToImageView(static_cast<float*>(img->GetData()), dim[0], dim[1], dim[2], res);
            }
        };
        const auto refImageView = std::visit(refImageViewVisitor, inputImageVar);
        const auto offset = std::visit(Impl::Overload{
            []([[maybe_unused]] const std::shared_ptr<Data::HT3D>& img) {return static_cast<double>(0); },
            [](const std::shared_ptr<Data::FL3D>& img) {return img->GetZOffset(); },
            [](const std::shared_ptr<Data::Float3D>& img) {return img->GetZOffset(); }
            }, inputImageVar);
        const auto min = std::visit(Impl::Overload{
            [](const std::shared_ptr<Data::HT3D>& img) {
                const auto [imgMin, imgMax] = img->GetRI();
                return imgMin * 10000.0;
            },
            [](const std::shared_ptr<Data::FL3D>& img) {
                const auto [imgMin, imgMax] = img->GetIntensity();
                return static_cast<double>(imgMin);
            },
            [](const std::shared_ptr<Data::Float3D>& img) {
                const auto [imgMin, imgMax] = img->GetRange();
                return static_cast<double>(imgMin);
            }
            }, inputImageVar);
        const auto chIndex = std::visit(Impl::Overload{
            []([[maybe_unused]] const std::shared_ptr<Data::HT3D>& img) {return 0; },
            [](const std::shared_ptr<Data::FL3D>& img) {return img->GetChannelIndex(); },
            []([[maybe_unused]] const std::shared_ptr<Data::Float3D>& img) {return 0; }
            }, inputImageVar);
        const auto chName = std::visit(Impl::Overload{
            []([[maybe_unused]] const std::shared_ptr<Data::HT3D>& img) {return QString{}; },
            [](const std::shared_ptr<Data::FL3D>& img) {return img->GetChannelName(); },
            []([[maybe_unused]] const std::shared_ptr<Data::Float3D>& img) {return QString{}; }
            }, inputImageVar);

        // create an input data
        TCImage::Pointer inputImage{nullptr};

        // set algorithm parameters and input data
        const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
        try {
            const auto imageStat = intensityStatistics(refImageView, IntensityStatistics::MIN_MAX, {});

            const auto isZeroPadded = imageStat->minimum() < min;
            const auto toBeFiltered = isZeroPadded ? Impl::GetMinClamped(refImageView, min) : refImageView;

            const auto filteredImage = Impl::FilterImage(toBeFiltered, kernelRadius * 2 + 1);

            const auto resultStat = intensityStatistics(filteredImage, IntensityStatistics::MIN_MAX, {});
            const auto result = std::visit(Impl::Overload{
                [&filteredImage, &dim, &res, &resultStat, timeStep] ([[maybe_unused]] const std::shared_ptr<Data::HT3D>& img) {
                    const auto processedImage = convertImage(filteredImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
                    return Data::DataConverter::ConvertToHTData<Data::HT3D>(
                        static_cast<uint16_t*>(processedImage->buffer()), 
                        dim[0], dim[1], dim[2],
                        res[0], res[1], res[2],
                        static_cast<float>(resultStat->minimum()), static_cast<float>(resultStat->maximum()), timeStep);
                },
                [&filteredImage, chIndex, &chName, &dim, &res, &resultStat, timeStep, offset]([[maybe_unused]] const std::shared_ptr<Data::FL3D>& img) {
                    const auto processedImage = convertImage(filteredImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
                    return Data::DataConverter::ConvertToFLData<Data::FL3D>(
                        static_cast<uint16_t*>(processedImage->buffer()),
                        chIndex, chName,
                        dim[0], dim[1], dim[2],
                        res[0], res[1], res[2],
                        static_cast<float>(resultStat->minimum()), static_cast<float>(resultStat->maximum()), timeStep, static_cast<float>(offset));
                },
                [&filteredImage, &dim, &res, &resultStat, timeStep, offset]([[maybe_unused]] const std::shared_ptr<Data::Float3D>& img) {
                    return Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(
                        filteredImage->buffer()),
                        dim[0], dim[1], dim[2],
                        res[0], res[1], res[2],
                        static_cast<float>(resultStat->minimum()), static_cast<float>(resultStat->maximum()), timeStep, static_cast<float>(offset));
                }
                }, inputImageVar);

#ifdef TEST_PERF
            const auto endTime = high_resolution_clock::now();
            const auto ms = duration<double, std::milli>(endTime - startTime);
            qDebug() << "2.5D Median filter accelerated (ms): " << ms.count();
#endif
            return {{"OutputImage", result}};
        }
        catch ([[maybe_unused]] Exception& e) {
            return {};
        }
    }

    auto MedianFilter25d::Abort() -> void {
    }
}
