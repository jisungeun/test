#pragma once

#include <IProcessor.h>

#include "CellAnalyzer.Processor.Smoothing.Acc.MedianFilter2.5dExport.h"

namespace CellAnalyzer::Processor::Smoothing::Acc {
    class CellAnalyzer_Processor_Smoothing_Acc_MedianFilter2_5d_API MedianFilter25d final : public IProcessor {
    public:
        explicit MedianFilter25d();
        ~MedianFilter25d() override;

        auto SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void override;
        auto SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void override;
        auto SetInputData(const QString& inputID, const DataPtr& data) -> void override;

        auto GetAttr(const QString& attrID) const -> ProcessorAttrPtr override;
        auto GetInputData(const QString& inputID) const -> DataPtr override;

        auto Process() -> DataMap override;
        auto Abort() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    DECLARE_PROCESSOR_START(MedianFilter25d) {
        Q_OBJECT DECLARE_PROCESSOR_END(MedianFilter25d, Smoothing::Acc, DEFAULT_PATH)
    };
}
