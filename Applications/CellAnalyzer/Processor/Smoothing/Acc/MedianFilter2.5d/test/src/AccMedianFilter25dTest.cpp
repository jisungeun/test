#include <QCoreApplication>
#include <catch2/catch.hpp>

#include <ImageDev/Data/ImageViewHelper.h>
#include <ImageDev/Initialization.h>
#include <ImageDev/Exception.h>
#include <iolink/view/ImageView.h>
#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>
#include <HT3D.h>
#include <FL3D.h>
#include <ImageDev/ImageDev.h>

#include "DataConverter.h"
#include "IProcessAttrType.h"
#include "MedianFilter25d.h"
#include "PluginFactory.h"

namespace AccMedianFilter25dTest {
    using namespace CellAnalyzer::Processor::Smoothing;
    using namespace iolink;
    using namespace imagedev;

    using ImagePtr = std::shared_ptr<ImageView>;
    class TestProcessAttr final : public CellAnalyzer::Processor::IProcessorAttr {
	public:
		explicit TestProcessAttr(QVariant value): value(std::move(value)) { }
        ~TestProcessAttr() override = default;

		auto GetAttrModel() const -> CellAnalyzer::Processor::ProcessorAttrModel override {
            return {};
		}
		auto GetAttrValue() const -> CellAnalyzer::Processor::ProcessorAttrValue override {
            return value;
		}
		auto GetAttrStyle() const -> CellAnalyzer::Processor::ProcessorAttrStyle override {
            return {};
		}

		auto SetAttrModel(const CellAnalyzer::Processor::ProcessorAttrModel& model) -> void override { }
        auto SetAttrValue(const CellAnalyzer::Processor::ProcessorAttrValue& value) -> void override { this->value = value; }
		auto SetAttrStyle(CellAnalyzer::Processor::ProcessorAttrStyle style) -> void override {}
    private:
        QVariant value;
	};

    auto MakeVolume(size_t x, size_t y, size_t z, double resX, double resY, double resZ)->ImagePtr {
        const auto spatialCalibrationProperty = SpatialCalibrationProperty(
            { -static_cast<double>(x) * resX / 2, -static_cast<double>(y) * resY / 2, -static_cast<double>(z) * resZ / 2 },
            { resX, resY, resZ }
        );
        const auto imageProp = std::make_shared<ImageProperties>(spatialCalibrationProperty, ImageInfoProperty(
            DataTypeId::UINT16));
        const auto imageShape = VectorXu64{ x, y, z };

        const auto imageView = ImageViewFactory::allocate(imageShape, DataTypeId::UINT16, imageProp, nullptr);
        setDimensionalInterpretation(imageView, ImageTypeId::VOLUME);

        const auto imageRegion = RegionXu64{ {0, 0, 0}, imageShape };
        const auto zeros = std::make_unique<uint16_t[]>(x * y * z);
        imageView->writeRegion(imageRegion, zeros.get());
        return imageView;
    }

    auto WriteSphereVolume(const ImagePtr& img, double cx, double cy, double cz, double radius, uint16_t value, bool l1Dist)->void {
        const auto distanceLimit = l1Dist ? radius : radius * radius;
        const auto dim = img->shape();
        const auto x = dim[0];
        const auto y = dim[1];
        const auto z = dim[2];
        const auto spacing = img->properties()->calibration().spacing();
        const double res[3]{ spacing[0], spacing[1], spacing[2] };
        const auto buffer = static_cast<uint16_t*>(img->buffer());
        for (size_t iz = 0; iz < z; ++iz) {
            const auto dz = cz - static_cast<double>(iz) * res[2];
            const auto dz2 = dz * dz;
            const auto dz1 = std::abs(dz);
            for (size_t ix = 0; ix < x; ++ix) {
                const auto dx = cx - static_cast<double>(ix) * res[0];
                const auto dx2 = dx * dx;
                const auto dx1 = std::abs(dx);
                for (size_t iy = 0; iy < y; ++iy) {
                    const auto dy = cy - static_cast<double>(iy) * res[1];
                    const auto dy2 = dy * dy;
                    const auto dy1 = std::abs(dy);

                    if (const auto distance = l1Dist ? dx1 + dy1 + dz1 : dx2 + dy2 + dz2; distance < distanceLimit) {
                        buffer[iy + y * (ix + x * iz)] = value;
                    }
                }
            }
        }
    }

    auto RunImageDev(const ImagePtr& img, int radius)->ImagePtr {
        using namespace imagedev;
        const auto dimZ = img->shape()[2];
        auto destImage = convertImage(img, ConvertImage::FLOAT_32_BIT);
        for(auto i = 0; i < dimZ; ++i) {
            const auto sliced = getSliceFromVolume3d(img, GetSliceFromVolume3d::Z_AXIS, i);
            const auto floatImage = convertImage(sliced, ConvertImage::FLOAT_32_BIT);

            const auto filteredImage = medianFilter2d(floatImage, radius,
                MedianFilter2d::SQUARE, MedianFilter2d::AUTOMATIC);

            destImage = setSliceToVolume3d(destImage, filteredImage, SetSliceToVolume3d::Z_AXIS, i);
        }
        return destImage;
    }

    auto CountDifferentPixels(const uint16_t* buf1, const uint16_t* buf2, size_t bufSize, int tolerance) -> size_t {
        size_t diffPixels = 0;
        for(auto i = 0u; i < bufSize;++i) {
            const auto delta = std::abs(static_cast<int>(buf1[i]) - static_cast<int>(buf2[i]));
            if(delta > tolerance) {
                diffPixels += 1;
            }
        }
        return diffPixels;
    }

    auto Print(const ImagePtr& img) {
        const auto dim = img->shape();
        const auto x = dim[0];
        const auto y = dim[1];
        const auto z = dim[2];
        for (int k = 0; k < z; ++k) {
            std::cout << "slice " << k << '\n';
            for (int i = 0; i < x; ++i) {
                for (int j = 0; j < y; ++j) {
                    std::cout << static_cast<uint16_t*>(img->buffer())[j + y * (i + x * k)] << ' ';
                }
                std::cout << '\n';
            }
            std::cout << '\n';
        }
        std::cout << '\n';
    }

    TEST_CASE("Original vs. Acc Output Compare") {
        using namespace CellAnalyzer::Data;
        try {
            imagedev::init();
        }
        catch (imagedev::Exception& e) {
            FAIL(e.what().c_str());
        }

        const auto image = MakeVolume(10, 10, 10, 1, 1, 1);
        const auto balls = std::vector<std::tuple<VectorXd, double, double>>{
            {{0, 0, 0}, 3.0, 1.337},
            {{1, 2, 3}, 2.0, 1.4},
            {{4, 5, 6}, 1.0, 1.9},
            {{7, 8, 9}, 5.0, 2.2},
            {{1, 3, 3}, 3.0, 3.3},
            {{4, 2, 4}, 2.0, 4.6},
            {{8, 7, 2}, 4.5, 7.1},
            {{9, 1, 0}, 2.2, 1.2}
        };
        auto converter = TCDataConverter{};

        SECTION("HT") {
            for(const auto& [pos, radius, value]: balls) {
                WriteSphereVolume(image, pos[0], pos[1], pos[2], radius, static_cast<uint16_t>(value * 10000.0), false);
            }
            constexpr auto kernelRadius = 3;
            const auto ht = DataConverter::ConvertToHTData<HT3D>(static_cast<uint16_t*>(image->buffer()), 10, 10, 10, 1, 1, 1, 0, 7.1, 0);

            const auto processor = std::dynamic_pointer_cast<CellAnalyzer::Processor::IProcessor>(std::make_shared<Acc::MedianFilter25d>());
            REQUIRE(processor != nullptr);
            processor->SetInputData("InputImage", ht);
            processor->SetAttr("Kernel Radius", std::make_shared<TestProcessAttr>(kernelRadius));
            const auto accOutputPtr = processor->Process()["OutputImage"];
            const auto accOutput = std::dynamic_pointer_cast<HT3D>(accOutputPtr);

            REQUIRE(accOutput != nullptr);
            const auto accBuffer = static_cast<uint16_t*>(accOutput->GetData());

            const auto originPtr = RunImageDev(image, kernelRadius);
            const auto originBuffer = static_cast<uint16_t*>(originPtr->buffer());

            const auto diffPixels = CountDifferentPixels(accBuffer, originBuffer, 10 * 10 * 10, 1);
            CHECK(diffPixels < 5);
        }

        SECTION("FL") {
            for(const auto& [pos, radius, value]: balls) {
                WriteSphereVolume(image, pos[0], pos[1], pos[2], radius, static_cast<uint16_t>(value * 100.0), false);
            }
            constexpr auto kernelRadius = 3;
            const auto fl = DataConverter::ConvertToHTData<FL3D>(static_cast<uint16_t*>(image->buffer()), 10, 10, 10, 1, 1, 1, 0, 7.1, 0);

            const auto processor = std::dynamic_pointer_cast<CellAnalyzer::Processor::IProcessor>(std::make_shared<Acc::MedianFilter25d>());
            REQUIRE(processor != nullptr);
            processor->SetInputData("InputImage", fl);
            processor->SetAttr("Kernel Radius", std::make_shared<TestProcessAttr>(kernelRadius));
            const auto accOutputPtr = processor->Process()["OutputImage"];
            const auto accOutput = std::dynamic_pointer_cast<HT3D>(accOutputPtr);

            REQUIRE(accOutput != nullptr);
            const auto accBuffer = static_cast<uint16_t*>(accOutput->GetData());

            const auto originPtr = RunImageDev(image, kernelRadius);
            const auto originBuffer = static_cast<uint16_t*>(originPtr->buffer());

            const auto diffPixels = CountDifferentPixels(accBuffer, originBuffer, 10 * 10 * 10, 1);
            CHECK(diffPixels < 5);

        }

        imagedev::finish();
    }
}
