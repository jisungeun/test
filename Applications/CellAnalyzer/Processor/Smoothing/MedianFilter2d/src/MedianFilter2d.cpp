#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCDataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include "MedianFilter2d.h"

namespace CellAnalyzer::Processor::Smoothing {
	using namespace imagedev;
	using namespace iolink;

	struct MedianFilter2d::Impl {
		const QMap<int, QString> paramKernelModes {
			{ 0, "Square" },
			{ 1, "Disk" }
		};

		const QMap<int, QString> paramSearchModes {
			{ 0, "Automatic" },
			{ 1, "Histogram" },
			{ 2, "Selection" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	MedianFilter2d::MedianFilter2d() : d { std::make_unique<Impl>() } { }

	MedianFilter2d::~MedianFilter2d() { }

	auto MedianFilter2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 15 }, { "Step", 1 } });
		} else if (id == "Kernel Mode") {
			attribute->SetAttrModel(QStringList(d->paramKernelModes.values()));
		} else if (id == "Search Mode") {
			attribute->SetAttrModel(QStringList(d->paramSearchModes.values()));
		}
	}

	auto MedianFilter2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto MedianFilter2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto MedianFilter2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto MedianFilter2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto MedianFilter2d::Process() -> DataMap {
		// create an input data
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		int timestep { 0 };
		TCDataConverter converter;
		bool isFL { false };
		bool isFloat { false };
		int chIdx { 0 };
		QString chName;
		if (const auto image = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			chIdx = image->GetChannelIndex();
			chName = image->GetChannelName();
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			isFL = true;
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			isFloat = true;
		} else {
			return {};
		}

		// set algorithm parameters and input data
		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		const auto kernelMode = d->attrMap["Kernel Mode"]->GetAttrValue().toString();
		const auto searchMode = d->attrMap["Search Mode"]->GetAttrValue().toString();

		try {
			const auto imageStat = intensityStatistics(refImageView, IntensityStatistics::MIN_MAX, { 0, 1 });
			if (imageStat->minimum() < min) {
				//stitching data with 0 value
				const auto thersholded = thresholding(refImageView, { -INT_MAX, static_cast<double>(min) });
				const auto reseted = resetImage(refImageView, min);
				refImageView = combineByMask(reseted, refImageView, thersholded);
			}
			const auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);

			const auto filteredImage = medianFilter2d(floatImage, kernelRadius, static_cast<imagedev::MedianFilter2d::KernelMode>(d->paramKernelModes.key(kernelMode)), static_cast<imagedev::MedianFilter2d::SearchMode>(d->paramSearchModes.key(searchMode)));

			const auto resultStat = intensityStatistics(filteredImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result { nullptr };
			if (isFL) {
				const auto processedImage = convertImage(filteredImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToFLData<Data::FL2D>(static_cast<uint16_t*>(processedImage->buffer()), chIdx, chName, dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			} else if (isFloat) {
				result = Data::DataConverter::ConvertToFloatData<Data::Float2D>(static_cast<float*>(filteredImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			} else {
				const auto processedImage = convertImage(filteredImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToHTData<Data::HT2D>(static_cast<uint16_t*>(processedImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			}
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto MedianFilter2d::Abort() -> void { }
}
