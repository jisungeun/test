#include <QCoreApplication>

#include <DataConverter.h>
#include <HTData3d.h>
#include <IPluginAlgorithm.h>
#include <PipelineException.h>
#include <PluginRegistry.h>
#include <TCDataConverter.h>

#include "BoxFilter3d.h"

namespace CellAnalyzer::Processor::Smoothing {
	auto BoxFilter3d::Process(const PropertyMap& properties, const DataMap& input) -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/labeling/TC.Algorithm.Smoothing.BoxFilter.3D.dll";
		const auto pluginName = "org.tomocube.algorithm.smoothing.boxfilter.3d";

		// create an input data
		TCImage::Pointer inputImage { nullptr };
		float offset { 0 };
		auto isFL { false };
		if (const auto image = std::dynamic_pointer_cast<Data::HTData3d>(input["InputImage"]))
			inputImage = Data::DataConverter::ConvertToTCImage(image);
		else if (const auto image = std::dynamic_pointer_cast<Data::FLData3d>(input["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			offset = image->GetZOffset();
			isFL = true;
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(pluginName, true));
		if (algorithm == nullptr) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		// set algorithm parameters and input data
		const auto kernelSizeX = properties["Kernel Size X"]->GetValue().toInt();
		const auto kernelSizeY = properties["Kernel Size Y"]->GetValue().toInt();
		const auto kernelSizeZ = properties["Kernel Size Z"]->GetValue().toInt();
		const auto autoScale = properties["Auto Scale"]->GetValue().toBool();

		auto params = algorithm->Parameter();
		params->SetValue("KernelSizeX", kernelSizeX);
		params->SetValue("KernelSizeY", kernelSizeY);
		params->SetValue("KernelSizeZ", kernelSizeZ);
		params->SetValue("AutoScale", autoScale);

		algorithm->SetInput(0, inputImage);
		if (!algorithm->Execute()) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		const auto resultImage = std::dynamic_pointer_cast<TCImage>(algorithm->GetOutput(0));
		if (resultImage == nullptr) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		DataPtr result { nullptr };
		if (isFL) {
			result = Data::DataConverter::ConvertToFLData<Data::FLData3d>(resultImage);
			std::dynamic_pointer_cast<Data::FLData3d>(result)->SetZOffset(offset);
		} else
			result = Data::DataConverter::ConvertToHTData<Data::HTData3d>(resultImage);

		return { { "OutputImage", result } };
	}

	auto BoxFilter3d::Initialize(const PropertyPtr& property) -> void {
		property->SetState(PropertyState::Disabled);

		if (const auto id = property->GetID(); id == "Kernel Size X") {
			property->SetModel(QVariantMap { { "Min", 1 }, { "Max", 100 }, { "Step", 1 } });
			property->SetValue(3);
		} else if (id == "Kernel Size Y") {
			property->SetModel(QVariantMap { { "Min", 1 }, { "Max", 100 }, { "Step", 1 } });
			property->SetValue(3);
		} else if (id == "Kernel Size Z") {
			property->SetModel(QVariantMap { { "Min", 1 }, { "Max", 100 }, { "Step", 1 } });
			property->SetValue(3);
		} else if (id == "Auto Scale")
			property->SetValue(true);
	}

	auto BoxFilter3d::Update(const PropertyMap& properties, const DataMap& input) -> void {
		if (input.find("InputImage") != input.end()) {
			for (auto& prop : properties.values())
				prop->SetState(PropertyState::Enabled);
		}
	}
}
