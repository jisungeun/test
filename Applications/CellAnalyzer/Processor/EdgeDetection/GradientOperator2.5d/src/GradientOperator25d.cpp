#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <Float3d.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "GradientOperator25d.h"

namespace CellAnalyzer::Processor::EdgeDetection {
	using namespace imagedev;
	using namespace iolink;

	struct GradientOperator25d::Impl {
		const QMap<int, QString> paramGradientOperator {
			{ 0, "Canny-Deriche" },
			{ 1, "Shen and Castan" },
			{ 2, "Sobel 3x3 kernel" },
			{ 3, "Canny" },
			{ 4, "Gaussian" },
			{ 5, "Sobel" },
			{ 6, "Prewitt" }
		};

		const QMap<int, QString> paramGradientMode {
			{ 0, "Maximal amplitude" },
			{ 1, "Euclidean amplitude" },
		};

		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	GradientOperator25d::GradientOperator25d() : d { std::make_unique<Impl>() } {}

	GradientOperator25d::~GradientOperator25d() { }

	auto GradientOperator25d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "GradientOperator") {
			attribute->SetAttrModel(QStringList(d->paramGradientOperator.values()));
		} else if (id == "GradientMode") {
			attribute->SetAttrModel(QStringList(d->paramGradientMode.values()));
		} else if (id == "SmoothingFactor") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		}
	}

	auto GradientOperator25d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto GradientOperator25d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto GradientOperator25d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto GradientOperator25d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto GradientOperator25d::Process() -> DataMap {
		DataMap output;

		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset { 0 };
		TCDataConverter converter;

		int timestep { 0 };
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
			offset = image->GetZOffset();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			timestep = image->GetTimeStep();
		} else
			return {};

		const auto gradientOperator = d->attrMap["GradientOperator"]->GetAttrValue().toString();
		const auto gradientMode = d->attrMap["GradientMode"]->GetAttrValue().toString();
		const auto smoothingFactor = d->attrMap["SmoothingFactor"]->GetAttrValue().toDouble();

		try {
			const auto imageStat = intensityStatistics(refImageView, IntensityStatistics::MIN_MAX, { 0, 1 });

			const auto isZeroPaded = imageStat->minimum() < min;
			if (isZeroPaded) {
				const auto thersholded = thresholding(refImageView, { -INT_MAX, static_cast<double>(min) });
				const auto reseted = resetImage(refImageView, min);
				refImageView = combineByMask(reseted, refImageView, thersholded);
			}
			auto floatingInput = rescaleIntensity(refImageView, RescaleIntensity::FLOAT_32_BIT, RescaleIntensity::MIN_MAX, { 1, 1 }, { 1, 1 }, { 0.0, 1.0 });

			auto destImage = floatingInput;
			for (auto i = 0; i < dimZ; i++) {
				auto sliced = getSliceFromVolume3d(floatingInput, GetSliceFromVolume3d::Z_AXIS, i);
				const auto slice_grad = gradientOperator2d(sliced, static_cast<GradientOperator2d::GradientOperator>(d->paramGradientOperator.key(gradientOperator)), static_cast<GradientOperator2d::GradientMode>(d->paramGradientMode.key(gradientMode)), smoothingFactor).outputAmplitudeImage;
				destImage = setSliceToVolume3d(destImage, slice_grad, SetSliceToVolume3d::Z_AXIS, i);
			}
			const auto resultStat = intensityStatistics(destImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(destImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
			return {};
		}
	}

	auto GradientOperator25d::Abort() -> void { }
}
