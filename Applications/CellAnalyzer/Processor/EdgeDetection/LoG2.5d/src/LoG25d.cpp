#include "LoG25d.h"
#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <QCoreApplication>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

namespace CellAnalyzer::Processor::EdgeDetection {
	using namespace imagedev;
	using namespace iolink;

	struct LoG25d::Impl {
		const QMap<int, QString> paramPrecision {
			{ 0, "Faster" },
			{ 1, "Precise" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	LoG25d::LoG25d() : d { std::make_unique<Impl>() } {}

	LoG25d::~LoG25d() { }

	auto LoG25d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;

		const auto id = attrID;
		if (id == "Gaussian SD X") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		} else if (id == "Gaussian SD Y") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		} else if (id == "Laplacian Kernel Radius") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 100 }, { "Step", 1 } });
		} else if (id == "Precision") {
			attribute->SetAttrModel(QStringList(d->paramPrecision.values()));
		}
	}

	auto LoG25d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto LoG25d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto LoG25d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto LoG25d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto LoG25d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset { 0 };
		int timestep { 0 };
		TCDataConverter converter;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			timestep = image->GetTimeStep();
		} else
			return {};

		const auto SDx = d->attrMap["Gaussian SD X"]->GetAttrValue().toDouble();
		const auto SDy = d->attrMap["Gaussian SD Y"]->GetAttrValue().toDouble();

		const auto kernelRadius = d->attrMap["Laplacian Kernel Radius"]->GetAttrValue().toInt();
		const auto precision = d->attrMap["Precision"]->GetAttrValue().toString();

		try {
			const auto imageStat = intensityStatistics(refImageView, IntensityStatistics::MIN_MAX, { 0, 1 });

			const auto isZeroPaded = imageStat->minimum() < min;

			auto destImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			for (auto i = 0; i < dimZ; i++) {
				auto sliced = getSliceFromVolume3d(refImageView, GetSliceFromVolume3d::Z_AXIS, i);
				if (isZeroPaded) {
					const auto thersholded = thresholding(sliced, { -INT_MAX, static_cast<double>(min) });
					const auto reseted = resetImage(sliced, min);
					sliced = combineByMask(reseted, sliced, thersholded);
				}
				const auto floatImage = convertImage(sliced, ConvertImage::FLOAT_32_BIT);

				const auto filteredImage = gaussianFilter2d(floatImage, GaussianFilter2d::FilterMode::SEPARABLE, { SDx, SDy }, 2.0, GaussianFilter2d::OutputType::SAME_AS_INPUT, false);
			}
			destImage = rescaleIntensity(destImage, RescaleIntensity::FLOAT_32_BIT, RescaleIntensity::MIN_MAX, { 0, 1 }, { 0, 1 }, { 0, 1 });
			for (auto i = 0; i < dimZ; i++) {
				auto sliced = getSliceFromVolume3d(destImage, GetSliceFromVolume3d::Z_AXIS, i);

				auto slice_lap = morphologicalLaplacian(sliced, static_cast<MorphologicalLaplacian::Precision>(d->paramPrecision.key(precision)), kernelRadius);
				destImage = setSliceToVolume3d(destImage, slice_lap, SetSliceToVolume3d::Z_AXIS, i);
			}
			const auto resultStat = intensityStatistics(destImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(destImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto LoG25d::Abort() -> void { }
}
