#include "LoG3d.h"
#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <QCoreApplication>
#include <TCDataConverter.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

namespace CellAnalyzer::Processor::EdgeDetection {
	using namespace imagedev;
	using namespace iolink;

	struct LoG3d::Impl {
		const QMap<int, QString> paramPrecision {
			{ 0, "Faster" },
			{ 1, "Precise" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	LoG3d::LoG3d() : d { std::make_unique<Impl>() } { }

	LoG3d::~LoG3d() { }

	auto LoG3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;

		const auto id = attrID;
		if (id == "Gaussian SD X") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		} else if (id == "Gaussian SD Y") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		} else if (id == "Gaussian SD Z") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		} else if (id == "Laplacian Kernel Radius") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 5 }, { "Step", 1 } });
		} else if (id == "Precision") {
			attribute->SetAttrModel(QStringList(d->paramPrecision.values()));
		}
	}

	auto LoG3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto LoG3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto LoG3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto LoG3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto LoG3d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset { 0 };
		int timestep { 0 };
		TCDataConverter converter;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			timestep = image->GetTimeStep();
		} else
			return {};

		const auto SDx = d->attrMap["Gaussian SD X"]->GetAttrValue().toDouble();
		const auto SDy = d->attrMap["Gaussian SD Y"]->GetAttrValue().toDouble();
		const auto SDz = d->attrMap["Gaussian SD Z"]->GetAttrValue().toDouble();

		const auto kernelRadius = d->attrMap["Laplacian Kernel Radius"]->GetAttrValue().toInt();
		const auto precision = d->attrMap["Precision"]->GetAttrValue().toString();

		try {
			const auto imageStat = intensityStatistics(refImageView, IntensityStatistics::MIN_MAX, { 0, 1 });
			if (imageStat->minimum() < min) {
				//stitching data with 0 value
				const auto thersholded = thresholding(refImageView, { -INT_MAX, static_cast<double>(min) });
				const auto reseted = resetImage(refImageView, min);
				refImageView = combineByMask(reseted, refImageView, thersholded);
			}
			const auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			const auto filteredImage = gaussianFilter3d(floatImage, GaussianFilter3d::SEPARABLE, { SDx, SDy, SDz }, 2.0, GaussianFilter3d::OutputType::SAME_AS_INPUT, false);

			auto floatingInput = rescaleIntensity(filteredImage, RescaleIntensity::FLOAT_32_BIT, RescaleIntensity::MIN_MAX, { 1, 1 }, { 1, 1 }, { 0.0, 1.0 });

			MorphologicalLaplacian laplacianAlgo;
			laplacianAlgo.setInputImage(floatingInput);
			laplacianAlgo.setPrecision(static_cast<MorphologicalLaplacian::Precision>(d->paramPrecision.key(precision)));
			laplacianAlgo.setKernelRadius(kernelRadius);
			laplacianAlgo.execute();

			const auto laplacianImage = laplacianAlgo.outputImage();

			const auto resultStat = intensityStatistics(laplacianImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(laplacianImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto LoG3d::Abort() -> void { }
}
