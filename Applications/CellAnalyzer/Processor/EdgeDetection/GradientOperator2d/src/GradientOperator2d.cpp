#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <Float2d.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "GradientOperator2d.h"

namespace CellAnalyzer::Processor::EdgeDetection {
	using namespace imagedev;
	using namespace iolink;
	constexpr std::string_view GradientOperatorId { "GradientOperator" };
	constexpr std::string_view GradientModeId { "GradientMode" };
	constexpr std::string_view SmoothingFactorId { "SmoothingFactor" };

	struct GradientOperator2d::Impl {
		const QMap<int, QString> paramGradientOperator {
			{ 0, "Canny-Deriche" },
			{ 1, "Shen and Castan" },
			{ 2, "Sobel 3x3 kernel" },
			{ 3, "Canny" },
			{ 4, "Gaussian" },
			{ 5, "Sobel" },
			{ 6, "Prewitt" }
		};

		const QMap<int, QString> paramGradientMode {
			{ 0, "Maximal amplitude" },
			{ 1, "Euclidean amplitude" },
		};

		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	GradientOperator2d::GradientOperator2d() : d { std::make_unique<Impl>() } {}

	GradientOperator2d::~GradientOperator2d() { }

	auto GradientOperator2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;

		const auto id = attrID;
		if (id == GradientOperatorId.data()) {
			attribute->SetAttrModel(QStringList(d->paramGradientOperator.values()));
		} else if (id == GradientModeId.data()) {
			attribute->SetAttrModel(QStringList(d->paramGradientMode.values()));
		} else if (id == SmoothingFactorId.data()) {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		}
	}

	auto GradientOperator2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto GradientOperator2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto GradientOperator2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto GradientOperator2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto GradientOperator2d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		TCDataConverter converter;
		int timestep { 0 };
		if (const auto image = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			timestep = image->GetTimeStep();
		} else {
			return {};
		}

		const auto gradientOperator = d->attrMap["GradientOperator"]->GetAttrValue().toString();
		const auto gradientMode = d->attrMap["GradientMode"]->GetAttrValue().toString();
		const auto smoothingFactor = d->attrMap["SmoothingFactor"]->GetAttrValue().toDouble();

		try {
			const auto imageStat = intensityStatistics(refImageView, IntensityStatistics::MIN_MAX, { 0, 1 });
			if (imageStat->minimum() < min) {
				//stitching data with 0 value
				const auto thersholded = thresholding(refImageView, { -INT_MAX, static_cast<double>(min) });
				const auto reseted = resetImage(refImageView, min);
				refImageView = combineByMask(reseted, refImageView, thersholded);
			}
			auto floatingInput = rescaleIntensity(refImageView, RescaleIntensity::FLOAT_32_BIT, RescaleIntensity::MIN_MAX, { 1, 1 }, { 1, 1 }, { 0.0, 1.0 });

			const auto gradImage = gradientOperator2d(floatingInput, static_cast<imagedev::GradientOperator2d::GradientOperator>(d->paramGradientOperator.key(gradientOperator)), static_cast<imagedev::GradientOperator2d::GradientMode>(d->paramGradientMode.key(gradientMode)), smoothingFactor).outputAmplitudeImage;

			const auto resultStat = intensityStatistics(gradImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToFloatData<Data::Float2D>(static_cast<float*>(gradImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			if (result == nullptr) {
				return {};
			}

			return { { "OutputImage", result } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
			return {};
		}
	}

	auto GradientOperator2d::Abort() -> void { }
}
