#include <QCoreApplication>

#include <DataConverter.h>
#include <DataList.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <ScalarData.h>
#include <Measure.h>
#include <TCDataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/NativeMeasurements.h>
#include <iolink/view/ImageViewFactory.h>

#include "DualLabelAnalysis2d.h"

namespace CellAnalyzer::Processor::Measurement {
	using namespace imagedev;

	std::string_view ParentSelectionCriteriaId { "Parent Selection Criteria" };
	const QMap<int, QString> paramParentSelectionCriteria {
		{ 0, "Best match" },
		{ 1, "Strict" }
	};

	struct DualLabelAnalysis2d::Impl {
		DataAttrGetterPtr getter { nullptr };
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	DualLabelAnalysis2d::DualLabelAnalysis2d() : d { std::make_unique<Impl>() } {}

	DualLabelAnalysis2d::~DualLabelAnalysis2d() { }

	auto DualLabelAnalysis2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == ParentSelectionCriteriaId.data()) {
			attribute->SetAttrModel(QStringList(paramParentSelectionCriteria.values()));
		}else if(id == "Feret Count") {
			attribute->SetAttrModel(QVariantMap{ {"Min",1},{"Max",1000},{"Decimals",3} });
			attribute->SetAttrValue(31);
		}
	}

	auto DualLabelAnalysis2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto DualLabelAnalysis2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto DualLabelAnalysis2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto DualLabelAnalysis2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto DualLabelAnalysis2d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/measurement/TC.Algorithm.Measurement.LabelAnalysis.2D.dll";

		// create an input data
		TCImage::Pointer inputImage { nullptr };
		int timestep;
		auto isFL = false;
		if (const auto image = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			isFL = true;
		} else {
			return {};
		}

		TCMask::Pointer inputMask { nullptr };
		QString childName;
		if (const auto mask = std::dynamic_pointer_cast<Data::LabelMask2D>(d->inputMap["ChildLabel"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			childName = d->getter->GetName(mask);
		} else if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["ChildLabel"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			childName = d->getter->GetName(mask);
		} else {
			return {};
		}
		TCMask::Pointer parentMask { nullptr };
		QString parentName;
		if (const auto mask = std::dynamic_pointer_cast<Data::LabelMask2D>(d->inputMap["ParentLabel"])) {
			parentMask = Data::DataConverter::ConvertToTCMask(mask);
			parentName = d->getter->GetName(mask);
		} else if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["ParentLabel"])) {
			parentMask = Data::DataConverter::ConvertToTCMask(mask);
			parentName = d->getter->GetName(mask);
		} else
			return {};

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		// set input data
		algorithm->SetInput(0, inputImage);
		algorithm->SetInput(1, inputMask);
		auto param = algorithm->Parameter();
		param->SetValue("FeretCount", d->attrMap["Feret Count"]->GetAttrValue().toInt());
		if (isFL) {
			param->SetValue("IntensityDiv", 1);
			param->SetValue("ImageType", "FL");
		} else {
			param->SetValue("IntensityDiv", 10000);
			param->SetValue("ImageType", "HT");
		}
		if (!algorithm->Execute()) {
			return {};
		}

		// convert to DataMap
		const auto resultMeasure = std::dynamic_pointer_cast<TC::Framework::DataList>(algorithm->GetOutput(0));

		QStringList measurements;
		if(isFL) {
			measurements = QStringList{
			"MeanIntensity",
			"SurfaceArea",
			"Circularity",
				"FeretDiameter",
			"CentroidX",
			"CentroidY"
			};
		}else {
			measurements = QStringList{
			"MeanRI",
			"SurfaceArea",
			"Circularity",
				"FeretDiameter",
			"CentroidX",
			"CentroidY"
			};
		}
		const 

		auto parentSelectionCriteria = d->attrMap[ParentSelectionCriteriaId.data()]->GetAttrValue().toString();

		QVariantMap measureResults;

		bool succeed = false;
		try {
			//find parentMap
			TCDataConverter converter;
			const auto refView = converter.ImageToImageView(inputImage);
			auto parentView = converter.MaskToImageView(parentMask);
			auto childView = converter.MaskToImageView(inputMask);

			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};

			const auto refOrigin = refView->properties()->calibration().origin();
			const auto parentOrigin = parentView->properties()->calibration().origin();
			const auto childOrigin = childView->properties()->calibration().origin();
			const auto refSpacing = refView->properties()->calibration().spacing();
			const auto parentSpacing = parentView->properties()->calibration().spacing();
			const auto childSpacing = childView->properties()->calibration().spacing();

			auto originDiff = false;
			auto spacingDiff = false;

			for (auto i = 0; i < 2; i++) {
				if (false == AreSame(refOrigin[i], parentOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(refSpacing[i], parentSpacing[i])) {
					spacingDiff = true;
				}
			}

			if (refView->shape() != parentView->shape() || originDiff || spacingDiff) {
				parentView = TCDataConverter::MapMaskGeometry(refView, parentView);
			}

			originDiff = false;
			spacingDiff = false;

			for (auto i = 0; i < 2; i++) {
				if (false == AreSame(refOrigin[i], childOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(refSpacing[i], childSpacing[i])) {
					spacingDiff = true;
				}
			}

			if (refView->shape() != childView->shape() || originDiff || spacingDiff) {
				childView = TCDataConverter::MapMaskGeometry(refView, childView);
			}

			const auto analysis = std::make_shared<AnalysisMsr>();
			const auto intensityMajorityMsr = analysis->select(NativeMeasurements::intensityMajority);
			const auto intensityCountMsr = analysis->select(NativeMeasurements::intensityCount);

			const auto parentInput = convertImage(parentView, ConvertImage::UNSIGNED_INTEGER_16_BIT);
			const auto parentstat = intensityStatistics(parentInput, IntensityStatistics::MIN_MAX, { 0, 1 });

			LabelAnalysis labelAnalysisAlgo;
			labelAnalysisAlgo.setInputIntensityImage(parentInput);
			labelAnalysisAlgo.setOutputAnalysis(analysis);
			labelAnalysisAlgo.setInputLabelImage(childView);
			labelAnalysisAlgo.execute();

			const auto childLabelCnt = analysis->labelCount();

			const auto dataSetList = resultMeasure->GetList();
			auto idx = 0;
			for (const auto dataSet : dataSetList) {
				QVariantMap singleMap;

				const auto labelIndexScalarData = std::dynamic_pointer_cast<TC::Framework::ScalarData>(dataSet->GetData("LabelIndex"));
				const auto labelIndex = labelIndexScalarData ? labelIndexScalarData->ValueAsInt() : -1;
				singleMap[childName + "-idx"] = labelIndex;
				singleMap["TimeStep"] = timestep;
				for (auto& measure : measurements) {
					if (const auto scalarData = std::dynamic_pointer_cast<TC::Framework::ScalarData>(dataSet->GetData(measure)))
						singleMap[measure] = scalarData->ValueAsDouble();
				}

				auto parentIdx = -1;
				if (labelIndex > -1 && childLabelCnt > labelIndex - 1) {
					const auto intensityMajority = static_cast<int>(intensityMajorityMsr->value(labelIndex - 1));
					if (parentSelectionCriteria == paramParentSelectionCriteria[0] /* Mode of label */) {
						if (intensityMajority > 0)
							parentIdx = intensityMajority;
					} else /* Fit into label */ {
						const auto intensityCount = static_cast<int>(intensityCountMsr->value(labelIndex - 1));
						if (intensityCount == 1)
							parentIdx = intensityMajority;
					}
				}

				singleMap[parentName + "-parentidx"] = parentIdx < 1 ? -1 : parentIdx;
				measureResults[QString::number(idx++)] = singleMap;
			}

			succeed = true;
		} catch (Exception& e) {
			std::cout << e.what();
		}
		catch (...) {
			std::cout << "Unknown exception";
		}

		if (!succeed)
			return {};

		measureResults["LabelType"] = "DualLabel";
		const auto result = std::make_shared<Data::Measure>(measureResults, timestep);

		return DataMap { { "Measure", result } };
	}

	auto DualLabelAnalysis2d::Abort() -> void { }

	auto DualLabelAnalysis2d::SetGetter(const DataAttrGetterPtr& getter) -> void {
		d->getter = getter;
	}
}
