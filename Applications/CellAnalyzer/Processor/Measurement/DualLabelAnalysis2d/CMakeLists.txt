project(CellAnalyzer.Processor.Measurement.DualLabelAnalysis2d)

set(HEADERS
	inc/DualLabelAnalysis2d.h
)

set(RESOURCES
	rsc/TypeInfo.json
)

set(SOURCES
	src/DualLabelAnalysis2d.cpp
)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE	${CMAKE_BINARY_DIR}/bin/Release/Processor)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG	${CMAKE_BINARY_DIR}/bin/Debug/Processor)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE	${CMAKE_BINARY_DIR}/lib/Release/Processor)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG	${CMAKE_BINARY_DIR}/lib/Debug/Processor)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE	${CMAKE_BINARY_DIR}/lib/Release/Processor)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG	${CMAKE_BINARY_DIR}/lib/Debug/Processor)

add_library(${PROJECT_NAME} SHARED
	${HEADERS}
	${RESOURCES}
	${SOURCES}
)

add_library(CellAnalyzer::Processor::Measurement::DualLabelAnalysis2d ALIAS ${PROJECT_NAME})
set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/CellAnalyzer/Processor/Measurement")

target_link_libraries(${PROJECT_NAME}
	PUBLIC
		
		CellAnalyzer::Processor::ProcessorModel
		CellAnalyzer::Data::TCF		
		CellAnalyzer::Data::Mask
		CellAnalyzer::Data::Measure
		CellAnalyzer::Data::Converter
		
		TC::Components::DataTypes
		TC::Components::EngineFramework
	PRIVATE
		ImageDev
)

target_include_directories(${PROJECT_NAME}
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
		$<INSTALL_INTERFACE:inc>
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PUBLIC
		/wd4251
)

generate_export_header(${PROJECT_NAME}
	EXPORT_MACRO_NAME ${PROJECT_NAME}_API
	EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
	ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
	LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
	RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR}/Processor COMPONENT application_ta_de)
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT dev)
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)