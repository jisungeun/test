#include <QCoreApplication>

#include <DataConverter.h>
#include <DataList.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <ScalarData.h>
#include <Measure.h>

#include "LabelAnalysis3d.h"

namespace CellAnalyzer::Processor::Measurement {
	struct LabelAnalysis3d::Impl {
		DataAttrGetterPtr getter { nullptr };
		QMap<int, QString> centerlineType {
			{ 0, "None" },
			{ 1, "Skeleton" },
			{ 2, "Centerline" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	LabelAnalysis3d::LabelAnalysis3d() : d { std::make_unique<Impl>() } { }

	LabelAnalysis3d::~LabelAnalysis3d() { }

	auto LabelAnalysis3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Medium RI") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Decimals", 3 } });
		} else if (id == "RII") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Decimals", 3 } });
		} else if (id == "Feret Count") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 1000 }, { "Decimals", 3 } });
			attribute->SetAttrValue(31);
		} else if (id == "New branch sensibility") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 10 }, { "Step", 0.01 }, { "Decimals", 2 } });
			attribute->SetAttrValue(3);
		} else if (id == "Length Measure") {
			attribute->SetAttrModel(QStringList(d->centerlineType.values()));
		}
	}

	auto LabelAnalysis3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}

		d->attrMap[attrID]->SetAttrValue(value);

		if (attrID == "Custom RI") {
			if (const auto attr = d->attrMap.value("Medium RI", nullptr))
				attr->SetAttrStyle(value.toBool() ? ProcessorAttrStyle::Visible : ProcessorAttrStyle::Disabled);
		}
		if (attrID == "Use Feret") {
			if (const auto attr = d->attrMap.value("Feret Count", nullptr))
				attr->SetAttrStyle(value.toBool() ? ProcessorAttrStyle::Visible : ProcessorAttrStyle::Hidden);
		}
		if (attrID == "Length Measure") {
			if (const auto attr = d->attrMap.value("New branch sensibility", nullptr)) {
				if (value.toString() == "Centerline") {
					attr->SetAttrStyle(ProcessorAttrStyle::Visible);
				} else {
					attr->SetAttrStyle(ProcessorAttrStyle::Hidden);
				}
			}
		}
	}

	auto LabelAnalysis3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto LabelAnalysis3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto LabelAnalysis3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto LabelAnalysis3d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/measurement/TC.Algorithm.Measurement.LabelAnalysis.3D.dll";

		// create an input data
		int timestep;
		TCImage::Pointer inputImage { nullptr };
		bool isFL { false };
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			isFL = true;
		} else {
			return {};
		}

		TCMask::Pointer inputMask { nullptr };
		QString labelName;
		if (const auto mask = std::dynamic_pointer_cast<Data::LabelMask3D>(d->inputMap["Label"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			labelName = d->getter->GetName(mask);
		} else if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["Label"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			labelName = d->getter->GetName(mask);
		} else {
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		const auto useFeret = d->attrMap["Use Feret"]->GetAttrValue().toBool();
		const auto lengthType = d->attrMap["Length Measure"]->GetAttrValue().toString();
		// set input data
		algorithm->SetInput(0, inputImage);
		algorithm->SetInput(1, inputMask);

		auto param = algorithm->Parameter();
		param->SetValue("CustomRI", d->attrMap["Custom RI"]->GetAttrValue().toBool());
		param->SetValue("RI", d->attrMap["Medium RI"]->GetAttrValue().toDouble());
		param->SetValue("RII", d->attrMap["RII"]->GetAttrValue().toDouble());
		param->SetValue("UseFeret", useFeret);
		if (useFeret) {
			param->SetValue("FeretCount", d->attrMap["Feret Count"]->GetAttrValue().toInt());
		}
		param->SetValue("LengthType", lengthType);
		if (lengthType == "Centerline") {
			param->SetValue("BranchSensibility", d->attrMap["New branch sensibility"]->GetAttrValue().toDouble());
		}
		if (isFL) {
			param->SetValue("IntensityDiv", 1);
			param->SetValue("ImageType", "FL");
		} else {
			param->SetValue("IntensityDiv", 10000);
			param->SetValue("ImageType", "HT");
		}

		if (!algorithm->Execute()) {
			return {};
		}

		// convert to DataMap
		const auto resultMeasure = std::dynamic_pointer_cast<TC::Framework::DataList>(algorithm->GetOutput(0));

		QStringList measurements;
		if (isFL) {
			measurements = QStringList {
				"Volume",
				"Volume-Filled",
				"SurfaceArea",
				"ProjectedArea",
				"MeanIntensity",
				"Sphericity",
				//"FeretDiameter",
				//"Length",
				"SumIntensity",
				"CentroidX",
				"CentroidY",
				"CentroidZ"
			};
			if (useFeret) {
				measurements.append("FeretDiameter");
			}
			if ("None" != lengthType) {
				measurements.append("Length");
			}
		} else {
			measurements = QStringList {
				"Volume",
				"Volume-Filled",
				"SurfaceArea",
				"ProjectedArea",
				"MeanRI",
				"Sphericity",
				//"FeretDiameter",
				//"Length",
				"Drymass",
				"Concentration",
				"CentroidX",
				"CentroidY",
				"CentroidZ"
			};
			if (useFeret) {
				measurements.append("FeretDiameter");
			}
			if ("None" != lengthType) {
				measurements.append("Length");
			}
		}

		QVariantMap measureMap;
		const auto dataSetList = resultMeasure->GetList();
		auto idx = 0;
		for (const auto dataSet : dataSetList) {
			QVariantMap singleMap;

			const auto labelIndexScalarData = std::dynamic_pointer_cast<TC::Framework::ScalarData>(dataSet->GetData("LabelIndex"));
			singleMap[labelName + "-idx"] = labelIndexScalarData ? labelIndexScalarData->ValueAsInt() : -1;
			singleMap["TimeStep"] = timestep;
			for (auto& measure : measurements) {
				if (const auto scalarData = std::dynamic_pointer_cast<TC::Framework::ScalarData>(dataSet->GetData(measure)))
					singleMap[measure] = scalarData->ValueAsDouble();
			}
			measureMap[QString::number(idx++)] = singleMap;
		}
		measureMap["LabelType"] = "SingleLabel";
		const auto measureResults = std::make_shared<Data::Measure>(measureMap, timestep);
		return DataMap { { "Measure", measureResults } };
	}

	auto LabelAnalysis3d::Abort() -> void { }

	auto LabelAnalysis3d::SetGetter(const DataAttrGetterPtr& getter) -> void {
		d->getter = getter;
	}
}
