#include <QCoreApplication>

#include <DataConverter.h>
#include <DataList.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <ScalarData.h>
#include <Measure.h>
#include <ImageDev/ImageDev.h>
#include <iolink/view/ImageViewFactory.h>
#include <ImageDev/Data/NativeMeasurements.h>
#include <TCDataConverter.h>
#include "GlobalMeasure3d.h"

namespace CellAnalyzer::Processor::Measurement {
	BETTER_ENUM(MeasureName, int,
				VOLUME = 100,
				SURFACE_AREA = 101,
				PROJ_AREA = 102,
				MEAN_RI = 103,
				SPHERICITY = 104,
				DRYMASS = 105,
				CONCENTRATAION = 106,
				CENTROID_X = 107,
				CENTROID_Y = 108,
				CENTROID_Z = 109,
				NONE = -1
				)

	using namespace TC::Framework;
	using namespace imagedev;
	using namespace iolink;

	struct GlobalMeasure3d::Impl {
		DataAttrGetterPtr getter { nullptr };
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
		QMap<QString, QMap<MeasureName, QString>> measureKeys;
		QString imageType { "HT" };
		auto BuildMeasureKeyMap() -> void;
		auto CalcSphericity(double volume, double surfaceArea) -> double;
	};

	auto GlobalMeasure3d::Impl::BuildMeasureKeyMap() -> void {
		const QMap<MeasureName, QString> measuresStringHT {
			{ MeasureName::VOLUME, "Volume" },
			{ MeasureName::SURFACE_AREA, "SurfaceArea" },
			{ MeasureName::PROJ_AREA, "ProjectedArea" },
			{ MeasureName::MEAN_RI, "MeanRI" },
			{ MeasureName::SPHERICITY, "Sphericity" },
			{ MeasureName::DRYMASS, "Drymass" },
			{ MeasureName::CONCENTRATAION, "Concentration" },
			{ MeasureName::CENTROID_X, "CentroidX" },
			{ MeasureName::CENTROID_Y, "CentroidY" },
			{ MeasureName::CENTROID_Z, "CentroidZ" }
		};
		measureKeys["HT"] = measuresStringHT;
		const QMap<MeasureName, QString> measuresStringFL {
			{ MeasureName::VOLUME, "Volume" },
			{ MeasureName::SURFACE_AREA, "SurfaceArea" },
			{ MeasureName::PROJ_AREA, "ProjectedArea" },
			{ MeasureName::MEAN_RI, "MeanIntensity" },
			{ MeasureName::SPHERICITY, "Sphericity" },
			{ MeasureName::DRYMASS, "SumIntensity" },
			{ MeasureName::CENTROID_X, "CentroidX" },
			{ MeasureName::CENTROID_Y, "CentroidY" },
			{ MeasureName::CENTROID_Z, "CentroidZ" }
		};
	}

	auto GlobalMeasure3d::Impl::CalcSphericity(double volume, double surfaceArea) -> double {
		if (volume < 0 || surfaceArea < 0)
			return -1;

		auto phi = pow(3.14, 1.0 / 3.0);
		auto factor = 36.0 * volume * volume;
		auto sa = pow(factor, 1.0 / 3.0);

		return (phi * sa / surfaceArea);
	}

	GlobalMeasure3d::GlobalMeasure3d() : d { std::make_unique<Impl>() } {
		d->BuildMeasureKeyMap();
	}

	GlobalMeasure3d::~GlobalMeasure3d() { }

	auto GlobalMeasure3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Medium RI") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Decimals", 3 } });
		} else if (id == "RII") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Decimals", 3 } });
		}
	}

	auto GlobalMeasure3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);

		if (attrID == "Custom RI") {
			if (const auto attr = d->attrMap.value("Medium RI", nullptr))
				attr->SetAttrStyle(value.toBool() ? ProcessorAttrStyle::Visible : ProcessorAttrStyle::Disabled);
		}
	}

	auto GlobalMeasure3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto GlobalMeasure3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto GlobalMeasure3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto GlobalMeasure3d::Process() -> DataMap {
		// create an input data
		int timestep;
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		TCDataConverter converter;
		auto isHT = true;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [iresx, iresy, iresz] = image->GetResolution();
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			isHT = false;
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			inputImage->SetOffset(image->GetZOffset());
			timestep = image->GetTimeStep();
			const auto [iresx, iresy, iresz] = image->GetResolution();
			refImageView = converter.ImageToImageView(inputImage);
			d->imageType = "FL";
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		try {
			const auto wholeBinary = resetImage(refImageView, 1);
			const auto binaryImage = convertImage(wholeBinary, ConvertImage::BINARY);

			const auto analysis = std::make_shared<AnalysisMsr>();
			const auto volumeMsr = analysis->select(NativeMeasurements::volume3d);
			const auto areaMsr = analysis->select(NativeMeasurements::voxelFaceArea3d);
			const auto intensityMeanMsr = analysis->select(NativeMeasurements::intensityMean);
			const auto intensitySumMsr = analysis->select(NativeMeasurements::intensityIntegral);
			const auto barycenterXMsr = analysis->select(NativeMeasurements::barycenterX);
			const auto barycenterYMsr = analysis->select(NativeMeasurements::barycenterY);
			const auto barycenterZMsr = analysis->select(NativeMeasurements::barycenterZ);

			BinaryAnalysis labelAnalysisAlgo;
			labelAnalysisAlgo.setInputIntensityImage(refImageView);
			labelAnalysisAlgo.setOutputAnalysis(analysis);
			labelAnalysisAlgo.setInputBinaryImage(binaryImage);
			labelAnalysisAlgo.execute();

			const auto labelIndex = 0;

			QVariantMap measureMap;
			QVariantMap singleMap;

			singleMap["Label-idx"] = 1;
			singleMap["TimeStep"] = timestep;

			auto volume = volumeMsr->value(labelIndex);
			if (volume < 0 || qIsInf(volume)) {
				volume = -1;
			}
			singleMap[d->measureKeys[d->imageType][MeasureName::VOLUME]] = volume;

			auto surfaceArea = areaMsr->value(labelIndex);
			if (surfaceArea < 0 || qIsInf(surfaceArea)) {
				surfaceArea = -1;
			}
			singleMap[d->measureKeys[d->imageType][MeasureName::SURFACE_AREA]] = surfaceArea;

			auto meanRi = intensityMeanMsr->value(labelIndex);
			if (meanRi < 0 || qIsInf(meanRi)) {
				meanRi = -1;
			} else {
				meanRi /= isHT ? 10000.0 : 1.0;
			}
			singleMap[d->measureKeys[d->imageType][MeasureName::MEAN_RI]] = meanRi;

			auto sphericity = d->CalcSphericity(volume, surfaceArea);
			singleMap[d->measureKeys[d->imageType][MeasureName::SPHERICITY]] = sphericity;

			const auto res = refImageView->properties()->calibration().spacing();
			const auto dim = refImageView->shape();
			auto projectedArea = res[0] * dim[0] * res[1] * dim[1];
			if (projectedArea < 0 || qIsInf(projectedArea)) {
				projectedArea = -1;
			}
			singleMap[d->measureKeys[d->imageType][MeasureName::PROJ_AREA]] = projectedArea;

			const auto RefractiveIndex = d->attrMap["Medium RI"]->GetAttrValue().toDouble();
			const auto RII = d->attrMap["RII"]->GetAttrValue().toDouble();

			auto drymass = -1.0;
			if (isHT) {
				drymass = (meanRi - RefractiveIndex) * volume / RII;
			} else {
				drymass = intensitySumMsr->value(labelIndex) / (res[0] * res[1] * res[2]);
			}
			if (drymass < 0 || qIsInf(drymass)) {
				drymass = -1;
			}
			singleMap[d->measureKeys[d->imageType][MeasureName::DRYMASS]] = drymass;

			auto concentration = drymass / volume;
			if (volume < 0 || drymass < 0 || qIsInf(concentration)) {
				concentration = -1;
			}
			singleMap[d->measureKeys[d->imageType][MeasureName::CONCENTRATAION]] = concentration;

			const auto centroidX = barycenterXMsr->value(labelIndex);
			singleMap[d->measureKeys[d->imageType][MeasureName::CENTROID_X]] = centroidX;

			const auto centroidY = barycenterYMsr->value(labelIndex);
			singleMap[d->measureKeys[d->imageType][MeasureName::CENTROID_Y]] = centroidY;

			const auto centroidZ = barycenterZMsr->value(labelIndex);
			singleMap[d->measureKeys[d->imageType][MeasureName::CENTROID_Z]] = centroidZ;

			measureMap[QString::number(0)] = singleMap;
			measureMap["LabelType"] = "SingleLabel";

			const auto measureResults = std::make_shared<Data::Measure>(measureMap, timestep);

			return DataMap { { "Measure", measureResults } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}

		return {};
	}

	auto GlobalMeasure3d::Abort() -> void { }

	auto GlobalMeasure3d::SetGetter(const DataAttrGetterPtr& getter) -> void {
		d->getter = getter;
	}
}
