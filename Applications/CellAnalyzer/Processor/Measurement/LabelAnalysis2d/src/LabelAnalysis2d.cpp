#include <QCoreApplication>

#include <DataConverter.h>
#include <DataList.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <ScalarData.h>
#include <Measure.h>
#include <TCDataConverter.h>

#include "LabelAnalysis2d.h"

namespace CellAnalyzer::Processor::Measurement {
	struct LabelAnalysis2d::Impl {
		DataAttrGetterPtr getter { nullptr };
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	LabelAnalysis2d::LabelAnalysis2d() : d { std::make_unique<Impl>() } {}

	LabelAnalysis2d::~LabelAnalysis2d() { }

	auto LabelAnalysis2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		if(attrID == "Feret Count") {
			attribute->SetAttrModel(QVariantMap{ {"Min",1},{"Max",1000},{"Decimals",3} });
			attribute->SetAttrValue(31);
		}
	}

	auto LabelAnalysis2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto LabelAnalysis2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto LabelAnalysis2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto LabelAnalysis2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto LabelAnalysis2d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/measurement/TC.Algorithm.Measurement.LabelAnalysis.2D.dll";
		int timestep;
		// create an input data
		TCImage::Pointer inputImage { nullptr };
		auto isFL = false;
		if (const auto image = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			isFL = true;
		} else {
			return {};
		}

		TCMask::Pointer inputMask { nullptr };
		QString labelName;
		if (const auto mask = std::dynamic_pointer_cast<Data::LabelMask2D>(d->inputMap["Label"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			labelName = d->getter->GetName(mask);
		} else if (const auto binMask = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["Label"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(binMask);
			labelName = d->getter->GetName(binMask);
		} else {
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		// set input data
		algorithm->SetInput(0, inputImage);
		algorithm->SetInput(1, inputMask);

		auto param = algorithm->Parameter();
		param->SetValue("FeretCount", d->attrMap["Feret Count"]->GetAttrValue().toInt());
		if (isFL) {
			param->SetValue("IntensityDiv", 1);
			param->SetValue("ImageType", "FL");
		} else {
			param->SetValue("IntensityDiv", 10000);
			param->SetValue("ImageType", "HT");
		}

		if (!algorithm->Execute()) {
			return {};
		}

		// convert to DataMap
		const auto resultMeasure = std::dynamic_pointer_cast<TC::Framework::DataList>(algorithm->GetOutput(0));

		QStringList measurements;
		if (isFL) {
			measurements = QStringList { "MeanIntensity", "SurfaceArea", "Circularity","FeretDiameter", "CentroidX", "CentroidY" };
		} else {
			measurements = QStringList { "MeanRI", "SurfaceArea", "Circularity","FeretDiameter", "CentroidX", "CentroidY" };
		}

		QVariantMap measureMap;
		const auto dataSetList = resultMeasure->GetList();
		auto idx = 0;
		for (const auto dataSet : dataSetList) {
			QVariantMap singleMap;
			const auto labelIndexScalarData = std::dynamic_pointer_cast<TC::Framework::ScalarData>(dataSet->GetData("LabelIndex"));
			singleMap[labelName + "-idx"] = labelIndexScalarData ? labelIndexScalarData->ValueAsInt() : -1;
			singleMap["TimeStep"] = timestep;

			for (auto& measure : measurements) {
				if (const auto scalarData = std::dynamic_pointer_cast<TC::Framework::ScalarData>(dataSet->GetData(measure))) {
					singleMap[measure] = scalarData->ValueAsDouble();
				}
			}
			measureMap[QString::number(idx++)] = singleMap;
		}
		measureMap["LabelType"] = "SingleLabel";
		const auto measureResults = std::make_shared<Data::Measure>(measureMap, timestep);
		return DataMap { { "Measure", measureResults } };
	}

	auto LabelAnalysis2d::Abort() -> void { }

	auto LabelAnalysis2d::SetGetter(const DataAttrGetterPtr& getter) -> void {
		d->getter = getter;
	}
}
