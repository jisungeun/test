#include <QCoreApplication>

#include <DataConverter.h>
#include <DataList.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <ScalarData.h>
#include <Measure.h>
#include <TCDataConverter.h>
#include <ImageDev/ImageDev.h>
#include <iolink/view/ImageViewFactory.h>

#include <ImageDev/Data/NativeMeasurements.h>
#include "GlobalMeasure2d.h"

namespace CellAnalyzer::Processor::Measurement {
	BETTER_ENUM(MeasureName, int,
				MEAN_RI = 100,
				SURFACE_AREA,
				CIRCULARITY,
				CENTROID_X,
				CENTROID_Y,
				NONE = -1
				)

	using namespace TC::Framework;
	using namespace imagedev;
	using namespace iolink;

	struct GlobalMeasure2d::Impl {
		DataAttrGetterPtr getter { nullptr };
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
		QMap<QString, QMap<MeasureName, QString>> measureKeys;
		QString imageType { "HT" };
		auto BuildMeasureKeyMap() -> void;
	};

	auto GlobalMeasure2d::Impl::BuildMeasureKeyMap() -> void {
		QMap<MeasureName, QString> measuresStringsHT {
			{ MeasureName::MEAN_RI, "MeanRI" },
			{ MeasureName::SURFACE_AREA, "SurfaceArea" },
			{ MeasureName::CIRCULARITY, "Circularity" },
			{ MeasureName::CENTROID_X, "CentroidX" },
			{ MeasureName::CENTROID_Y, "CentroidY" }
		};
		measureKeys["HT"] = measuresStringsHT;

		QMap<MeasureName, QString> measuresStringsFL {
			{ MeasureName::MEAN_RI, "MeanIntensity" },
			{ MeasureName::SURFACE_AREA, "SurfaceArea" },
			{ MeasureName::CIRCULARITY, "Circularity" },
			{ MeasureName::CENTROID_X, "CentroidX" },
			{ MeasureName::CENTROID_Y, "CentroidY" }
		};
		measureKeys["FL"] = measuresStringsFL;
	}

	GlobalMeasure2d::GlobalMeasure2d() : d { std::make_unique<Impl>() } {
		d->BuildMeasureKeyMap();
	}

	GlobalMeasure2d::~GlobalMeasure2d() { }

	auto GlobalMeasure2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
	}

	auto GlobalMeasure2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto GlobalMeasure2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto GlobalMeasure2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto GlobalMeasure2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto GlobalMeasure2d::Process() -> DataMap {
		int timestep;
		// create an input data
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		TCDataConverter converter;
		auto isHT = true;
		if (const auto image = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			isHT = false;
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			refImageView = converter.ImageToImageView(inputImage);
			d->imageType = "FL";
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		try {
			const auto wholeBinary = resetImage(refImageView, 1);
			const auto binaryImage = convertImage(wholeBinary, ConvertImage::BINARY);
			const auto analysis = std::make_shared<AnalysisMsr>();
			const auto areaMsr = analysis->select(NativeMeasurements::area2d);
			const auto intensityMeanMsr = analysis->select(NativeMeasurements::intensityMean);
			const auto inverseCircularityMsr = analysis->select(NativeMeasurements::inverseCircularity2d);
			const auto barycenterXMsr = analysis->select(NativeMeasurements::barycenterX);
			const auto barycenterYMsr = analysis->select(NativeMeasurements::barycenterY);

			BinaryAnalysis binaryAnalysisAlgo;
			binaryAnalysisAlgo.setInputIntensityImage(refImageView);
			binaryAnalysisAlgo.setOutputAnalysis(analysis);
			binaryAnalysisAlgo.setInputBinaryImage(binaryImage);
			binaryAnalysisAlgo.execute();

			auto labelIndex = 0;

			QVariantMap measureMap;
			QVariantMap singleMap;
			singleMap["Label-idx"] = 1;
			singleMap["TimeStep"] = timestep;

			// mean RI
			auto meanRi = intensityMeanMsr->value(labelIndex);
			if (meanRi < 0 || qIsInf(meanRi)) {
				meanRi = -1;
			} else {
				meanRi /= isHT ? 10000.0 : 1.0;
			}

			singleMap[d->measureKeys[d->imageType][MeasureName::MEAN_RI]] = meanRi;

			// surface area
			auto surfaceArea = areaMsr->value(labelIndex);
			if (surfaceArea < 0 || qIsInf(surfaceArea)) {
				surfaceArea = -1;
			}

			singleMap[d->measureKeys[d->imageType][MeasureName::SURFACE_AREA]] = surfaceArea;

			// circularity
			const auto inverseCircularity = inverseCircularityMsr->value(labelIndex);
			double circularity = 0.;
			if (inverseCircularity < 0 || qIsInf(inverseCircularity)) {
				circularity = -1;
			} else {
				circularity = 1. / inverseCircularity;
			}

			singleMap[d->measureKeys[d->imageType][MeasureName::CIRCULARITY]] = circularity;

			// centroid-x
			const auto centroidX = barycenterXMsr->value(labelIndex);
			singleMap[d->measureKeys[d->imageType][MeasureName::CENTROID_X]] = centroidX;

			// centroid-y
			const auto centroidY = barycenterYMsr->value(labelIndex);
			singleMap[d->measureKeys[d->imageType][MeasureName::CENTROID_Y]] = centroidY;

			measureMap[QString::number(0)] = singleMap;

			measureMap["LabelType"] = "SingleLabel";

			const auto measureResults = std::make_shared<Data::Measure>(measureMap, timestep);

			return DataMap { { "Measure", measureResults } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
			return {};
		}
	}

	auto GlobalMeasure2d::Abort() -> void { }

	auto GlobalMeasure2d::SetGetter(const DataAttrGetterPtr& getter) -> void {
		d->getter = getter;
	}
}
