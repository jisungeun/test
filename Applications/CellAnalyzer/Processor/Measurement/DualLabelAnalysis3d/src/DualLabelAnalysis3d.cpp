#include <QCoreApplication>

#include <DataConverter.h>
#include <DataList.h>
#include <Measure.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <ScalarData.h>

#include <TCDataConverter.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/NativeMeasurements.h>
#include <iolink/view/ImageViewFactory.h>

#include "DualLabelAnalysis3d.h"

namespace CellAnalyzer::Processor::Measurement {
	using namespace imagedev;

	struct DualLabelAnalysis3d::Impl {
		std::string_view CustomRI { "Custom RI" };
		std::string_view MediumRI { "Medium RI" };
		std::string_view RII { "RII" };
		std::string_view ParentSelectionCriteria { "Parent Selection Criteria" };

		const QMap<int, QString> paramParentSelectionCriteria {
			{ 0, "Best match" },
			{ 1, "Strict" }
		};

		QMap<int, QString> centerlineType {
			{ 0, "None" },
			{ 1, "Skeleton" },
			{ 2, "Centerline" }
		};

		DataAttrGetterPtr getter { nullptr };
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;

		auto AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double;
	};

	auto DualLabelAnalysis3d::Impl::AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double {
		return offset - static_cast<double>(refDimZ) * refResZ / 2.0;
	}

	DualLabelAnalysis3d::DualLabelAnalysis3d() : d { std::make_unique<Impl>() } { }

	DualLabelAnalysis3d::~DualLabelAnalysis3d() { }

	auto DualLabelAnalysis3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == d->MediumRI.data()) {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Decimals", 3 } });
		} else if (id == d->RII.data()) {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Decimals", 3 } });
		} else if (id == d->ParentSelectionCriteria.data()) {
			attribute->SetAttrModel(QStringList(d->paramParentSelectionCriteria.values()));
		} else if (id == "Feret Count") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 1000 }, { "Decimals", 3 } });
			attribute->SetAttrValue(31);
		} else if (id == "New branch sensibility") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 10 }, { "Step", 0.01 }, { "Decimals", 2 } });
			attribute->SetAttrValue(3);
		} else if (id == "Length Measure") {
			attribute->SetAttrModel(QStringList(d->centerlineType.values()));
		}
	}

	auto DualLabelAnalysis3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);

		if (attrID == "Custom RI") {
			if (const auto attr = d->attrMap.value("Medium RI", nullptr))
				attr->SetAttrStyle(value.toBool() ? ProcessorAttrStyle::Visible : ProcessorAttrStyle::Disabled);
		}
		if (attrID == "Use Feret") {
			if (const auto attr = d->attrMap.value("Feret Count", nullptr))
				attr->SetAttrStyle(value.toBool() ? ProcessorAttrStyle::Visible : ProcessorAttrStyle::Hidden);
		}
		if (attrID == "Length Measure") {
			if (const auto attr = d->attrMap.value("New branch sensibility", nullptr)) {
				if (value.toString() == "Centerline") {
					attr->SetAttrStyle(ProcessorAttrStyle::Visible);
				} else {
					attr->SetAttrStyle(ProcessorAttrStyle::Hidden);
				}
			}
		}
	}

	auto DualLabelAnalysis3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto DualLabelAnalysis3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto DualLabelAnalysis3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto DualLabelAnalysis3d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/measurement/TC.Algorithm.Measurement.LabelAnalysis.3D.dll";

		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.0001;
		};

		// create an input data
		int timestep;
		double refOffset { 0 };
		bool isFL { false };
		TCImage::Pointer inputImage { nullptr };
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			isFL = true;
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			inputImage->SetOffset(image->GetZOffset());
			timestep = image->GetTimeStep();
			refOffset = image->GetZOffset();
		} else {
			return {};
		}

		TCMask::Pointer inputMask { nullptr };
		QString childName;
		double childOffset { 0 };
		if (const auto mask = std::dynamic_pointer_cast<Data::LabelMask3D>(d->inputMap["ChildLabel"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			inputMask->SetOffset(mask->GetZOffset());
			childName = d->getter->GetName(mask);
			childOffset = mask->GetZOffset();
		} else if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["ChildLabel"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			inputMask->SetOffset(mask->GetZOffset());
			childName = d->getter->GetName(mask);
			childOffset = mask->GetZOffset();
		} else {
			return {};
		}

		TCMask::Pointer parentMask { nullptr };
		QString parentName;
		double parentOffset { 0 };
		if (const auto mask = std::dynamic_pointer_cast<Data::LabelMask3D>(d->inputMap["ParentLabel"])) {
			parentMask = Data::DataConverter::ConvertToTCMask(mask);
			parentName = d->getter->GetName(mask);
			parentOffset = mask->GetZOffset();
		} else if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["ParentLabel"])) {
			parentMask = Data::DataConverter::ConvertToTCMask(mask);
			parentName = d->getter->GetName(mask);
			parentOffset = mask->GetZOffset();
		} else
			return {};

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		const auto useFeret = d->attrMap["Use Feret"]->GetAttrValue().toBool();
		const auto lengthType = d->attrMap["Length Measure"]->GetAttrValue().toString();

		// set input data
		algorithm->SetInput(0, inputImage);
		algorithm->SetInput(1, inputMask);

		auto param = algorithm->Parameter();
		param->SetValue("CustomRI", d->attrMap[d->CustomRI.data()]->GetAttrValue().toBool());
		param->SetValue("RI", d->attrMap[d->MediumRI.data()]->GetAttrValue().toDouble());
		param->SetValue("RII", d->attrMap[d->RII.data()]->GetAttrValue().toDouble());
		if (useFeret) {
			param->SetValue("FeretCount", d->attrMap["Feret Count"]->GetAttrValue().toInt());
		}
		param->SetValue("LengthType", lengthType);
		if (lengthType == "Centerline") {
			param->SetValue("BranchSensibility", d->attrMap["New branch sensibility"]->GetAttrValue().toDouble());
		}
		if (isFL) {
			param->SetValue("IntensityDiv", 1);
			param->SetValue("ImageType", "FL");
		} else {
			param->SetValue("IntensityDiv", 10000);
			param->SetValue("ImageType", "HT");
		}

		if (!algorithm->Execute()) {
			return {};
		}

		// convert to DataMap
		const auto resultMeasure = std::dynamic_pointer_cast<TC::Framework::DataList>(algorithm->GetOutput(0));

		QStringList measurements;
		if (isFL) {
			measurements = QStringList {
				"Volume",
				"SurfaceArea",
				"ProjectedArea",
				"MeanIntensity",
				"Sphericity",
				//"FeretDiameter",
				//"Length",
				"SumIntensity",
				"CentroidX",
				"CentroidY",
				"CentroidZ"
			};
			if (useFeret) {
				measurements.append("FeretDiameter");
			}
			if ("None" != lengthType) {
				measurements.append("Length");
			}
		} else {
			measurements = QStringList {
				"Volume",
				"SurfaceArea",
				"ProjectedArea",
				"MeanRI",
				"Sphericity",
				//"FeretDiameter",
				//"Length",
				"Drymass",
				"Concentration",
				"CentroidX",
				"CentroidY",
				"CentroidZ"
			};
			if (useFeret) {
				measurements.append("FeretDiameter");
			}
			if ("None" != lengthType) {
				measurements.append("Length");
			}
		}

		auto parentSelectionCriteria = d->attrMap[d->ParentSelectionCriteria.data()]->GetAttrValue().toString();

		QVariantMap measureMap;
		bool succeed = false;

		try {
			//find parentMap
			TCDataConverter converter;
			const auto inputImageView = converter.ImageToImageView(inputImage);
			auto parentView = converter.MaskToImageView(parentMask);
			auto childView = converter.MaskToImageView(inputMask);

			const auto refOrigin = inputImageView->properties()->calibration().origin();
			const auto parentOrigin = parentView->properties()->calibration().origin();
			const auto childOrigin = childView->properties()->calibration().origin();
			const auto refSpacing = inputImageView->properties()->calibration().spacing();
			const auto parentSpacing = parentView->properties()->calibration().spacing();
			const auto childSpacing = childView->properties()->calibration().spacing();

			bool isRefImageSet { false };
			int htSizeZ { 1 };
			double htResZ { 1 };
			if (AreSame(refOffset, 0)) {
				htSizeZ = static_cast<int>(inputImageView->shape()[2]);
				htResZ = refSpacing[2];
				isRefImageSet = true;
			} else if (AreSame(childOffset, 0)) {
				htSizeZ = static_cast<int>(childView->shape()[2]);
				htResZ = childSpacing[2];
				isRefImageSet = true;
			} else if (AreSame(parentOffset, 0)) {
				htSizeZ = static_cast<int>(parentView->shape()[2]);
				htResZ = parentSpacing[2];
				isRefImageSet = true;
			}

			if (isRefImageSet) {
				if (false == AreSame(refOffset, 0)) {
					refOffset = d->AdjustOffsetZ(refOffset, htSizeZ, htResZ);
				}
				if (false == AreSame(parentOffset, 0)) {
					parentOffset = d->AdjustOffsetZ(parentOffset, htSizeZ, htResZ);
				}
				if (false == AreSame(childOffset, 0)) {
					childOffset = d->AdjustOffsetZ(childOffset, htSizeZ, htResZ);
				}
			}

			auto originDiff = false;
			auto spacingDiff = false;

			for (auto i = 0; i < 3; i++) {
				if (false == AreSame(refOrigin[i], parentOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(refSpacing[i], parentSpacing[i])) {
					spacingDiff = true;
				}
			}

			if (inputImageView->shape() != parentView->shape() || originDiff || spacingDiff) {
				parentView = TCDataConverter::MapMaskGeometry(inputImageView, parentView, refOffset, parentOffset);
			}

			originDiff = false;
			spacingDiff = false;

			for (auto i = 0; i < 3; i++) {
				if (false == AreSame(refOrigin[i], childOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(refSpacing[i], childSpacing[i])) {
					spacingDiff = true;
				}
			}

			if (inputImageView->shape() != childView->shape() || originDiff || spacingDiff) {
				childView = TCDataConverter::MapMaskGeometry(inputImageView, childView, refOffset, childOffset);
			}

			const auto analysis = std::make_shared<AnalysisMsr>();
			const auto intensityMajorityMsr = analysis->select(NativeMeasurements::intensityMajority);
			const auto intensitySecond = analysis->select(NativeMeasurements::intensityMaximum);
			const auto intensityCountMsr = analysis->select(NativeMeasurements::intensityCount);

			const auto parentInput = convertImage(parentView, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			LabelAnalysis labelAnalysisAlgo;
			labelAnalysisAlgo.setInputIntensityImage(parentInput);
			labelAnalysisAlgo.setOutputAnalysis(analysis);
			labelAnalysisAlgo.setInputLabelImage(childView);
			labelAnalysisAlgo.execute();

			const auto childLabelCnt = analysis->labelCount();

			const auto dataSetList = resultMeasure->GetList();
			auto idx = 0;
			for (const auto dataSet : dataSetList) {
				QVariantMap singleMap;

				const auto labelIndexScalarData = std::dynamic_pointer_cast<TC::Framework::ScalarData>(dataSet->GetData("LabelIndex"));
				const auto labelIndex = labelIndexScalarData ? labelIndexScalarData->ValueAsInt() : -1;
				singleMap[childName + "-idx"] = labelIndex;
				singleMap["TimeStep"] = timestep;
				for (auto& measure : measurements) {
					if (const auto scalarData = std::dynamic_pointer_cast<TC::Framework::ScalarData>(dataSet->GetData(measure)))
						singleMap[measure] = scalarData->ValueAsDouble();
				}

				auto parentIdx = -1;
				if (labelIndex > -1 && childLabelCnt > labelIndex - 1) {
					const auto intensityMajority = static_cast<int>(intensityMajorityMsr->value(labelIndex - 1));
					if (parentSelectionCriteria == d->paramParentSelectionCriteria[0] /* Best match */) {
						if (intensityMajority > 0)
							parentIdx = intensityMajority;
					} else /* Strict */ {
						const auto intensityCount = static_cast<int>(intensityCountMsr->value(labelIndex - 1));
						if (intensityCount == 1)
							parentIdx = intensityMajority;
					}
				}

				singleMap[parentName + "-parentidx"] = parentIdx < 1 ? -1 : parentIdx;

				measureMap[QString::number(idx++)] = singleMap;
			}
			succeed = true;
		} catch (Exception& e) {
			std::cout << e.what();
		}
		catch (...) {
			std::cout << "Unknown exception";
		}

		if (!succeed)
			return {};

		measureMap["LabelType"] = "DualLabel";
		const auto measureResults = std::make_shared<Data::Measure>(measureMap, timestep);
		return DataMap { { "Measure", measureResults } };
	}

	auto DualLabelAnalysis3d::Abort() -> void { }

	auto DualLabelAnalysis3d::SetGetter(const DataAttrGetterPtr& getter) -> void {
		d->getter = getter;
	}
}
