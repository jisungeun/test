#pragma once

#include <IProcessor.h>

namespace CellAnalyzer::Processor::Measurement {
	class DualLabelAnalysis3d final : public IProcessor {
	public:
		DualLabelAnalysis3d();
		~DualLabelAnalysis3d() override;

		auto SetGetter(const DataAttrGetterPtr& getter) -> void override;
		auto SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void override;
		auto SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void override;
		auto SetInputData(const QString& inputID, const DataPtr& data) -> void override;

		auto GetAttr(const QString& attrID) const -> ProcessorAttrPtr override;
		auto GetInputData(const QString& inputID) const -> DataPtr override;

		auto Process() -> DataMap override;
		auto Abort() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	DECLARE_PROCESSOR_START(DualLabelAnalysis3d) {
		Q_OBJECT
		DECLARE_PROCESSOR_END(DualLabelAnalysis3d, Measurement, DEFAULT_PATH)
	};
}
