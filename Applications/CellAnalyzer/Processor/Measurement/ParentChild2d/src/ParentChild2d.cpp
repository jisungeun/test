#include <QCoreApplication>

#include <DataConverter.h>
#include <DataList.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <Measure.h>
#include <ScalarData.h>
#include <TCDataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/NativeMeasurements.h>
#include <iolink/view/ImageViewFactory.h>

#include "ParentChild2d.h"

namespace CellAnalyzer::Processor::Measurement {
	using namespace imagedev;

	struct ParentChild2d::Impl {
		std::string_view ParentSelectionCriteria { "Parent Selection Criteria" };

		const QMap<int, QString> paramParentSelectionCriteria {
			{ 0, "Best match" },
			{ 1, "Strict" }
		};
		DataAttrGetterPtr getter { nullptr };
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	ParentChild2d::ParentChild2d() : d { std::make_unique<Impl>() } {}

	ParentChild2d::~ParentChild2d() { }

	auto ParentChild2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == d->ParentSelectionCriteria.data()) {
			attribute->SetAttrModel(QStringList(d->paramParentSelectionCriteria.values()));
		}
	}

	auto ParentChild2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto ParentChild2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto ParentChild2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto ParentChild2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto ParentChild2d::Process() -> DataMap {
		TCMask::Pointer childMask { nullptr };
		QString childName;
		int timestep { 0 };
		if (const auto mask = std::dynamic_pointer_cast<Data::LabelMask2D>(d->inputMap["ChildLabel"])) {
			childMask = Data::DataConverter::ConvertToTCMask(mask);
			childName = d->getter->GetName(mask);
			timestep = mask->GetTimeStep();
		} else if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["ChildLabel"])) {
			childMask = Data::DataConverter::ConvertToTCMask(mask);
			childName = d->getter->GetName(mask);
			timestep = mask->GetTimeStep();
		} else {
			return {};
		}
		TCMask::Pointer parentMask { nullptr };
		QString parentName;
		if (const auto mask = std::dynamic_pointer_cast<Data::LabelMask2D>(d->inputMap["ParentLabel"])) {
			parentMask = Data::DataConverter::ConvertToTCMask(mask);
			parentName = d->getter->GetName(mask);
		} else if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["ParentLabel"])) {
			parentMask = Data::DataConverter::ConvertToTCMask(mask);
			parentName = d->getter->GetName(mask);
		} else
			return {};

		auto parentSelectionCriteria = d->attrMap[d->ParentSelectionCriteria.data()]->GetAttrValue().toString();

		QVariantMap measureMap;
		bool succeed = false;

		try {
			// convert to DataMap		
			//find parentMap
			TCDataConverter converter;
			const auto parentView = converter.MaskToImageView(parentMask);
			auto childView = converter.MaskToImageView(childMask);

			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};

			const auto parentOrigin = parentView->properties()->calibration().origin();
			const auto childOrigin = childView->properties()->calibration().origin();
			const auto parentSpacing = parentView->properties()->calibration().spacing();
			const auto childSpacing = childView->properties()->calibration().spacing();

			auto originDiff = false;
			auto spacingDiff = false;

			for (auto i = 0; i < 2; i++) {
				if (false == AreSame(childOrigin[i], parentOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(childSpacing[i], parentSpacing[i])) {
					spacingDiff = true;
				}
			}

			if (childView->shape() != parentView->shape() || originDiff || spacingDiff) {
				childView = TCDataConverter::MapMaskGeometry(parentView, childView);
			}

			const auto parentStat = intensityStatistics(parentView, IntensityStatistics::MIN_MAX, { 0, 1 });
			const auto parentMax = static_cast<int>(parentStat->maximum());

			const auto analysis = std::make_shared<AnalysisMsr>();
			const auto intensityMajorityMsr = analysis->select(NativeMeasurements::intensityMajority);
			const auto intensityCountMsr = analysis->select(NativeMeasurements::intensityCount);

			const auto parentInput = convertImage(parentView, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			LabelAnalysis labelAnalysisAlgo;
			labelAnalysisAlgo.setInputIntensityImage(parentInput);
			labelAnalysisAlgo.setOutputAnalysis(analysis);
			labelAnalysisAlgo.setInputLabelImage(childView);
			labelAnalysisAlgo.execute();

			QList<QList<int>> parentMap;
			for (auto i = 0; i < parentMax; i++) {
				parentMap.append(QList<int>());
			}

			const auto childLabelCnt = analysis->labelCount();
			for (auto i = 0; i < childLabelCnt; i++) {
				auto parentLabel = -1;

				const auto intensityMajority = static_cast<int>(intensityMajorityMsr->value(i));

				if (parentSelectionCriteria == d->paramParentSelectionCriteria[0] /* Best match */) {
					if (intensityMajority > 0)
						parentLabel = intensityMajority;
				} else /* Strict */ {
					const auto intensityCount = static_cast<int>(intensityCountMsr->value(i));
					if (intensityCount == 1)
						parentLabel = intensityMajority;
				}

				if (parentLabel > 0) {
					parentMap[parentLabel - 1].append(i + 1);
				}
			}

			auto idx = 0;
			for (auto i = 0; i < parentMap.count(); i++) {
				for (auto j = 0; j < parentMap[i].count(); j++) {
					QVariantMap singleMap;
					singleMap["Parent-" + parentName] = i + 1;
					singleMap["Child-" + childName] = parentMap[i][j];
					singleMap["TimeStep"] = timestep;
					measureMap[QString::number(idx++)] = singleMap;
				}
			}
			succeed = true;
		} catch (Exception& e) {
			std::cout << e.what();
		}
		catch (...) {
			std::cout << "Unknown exception";
		}

		if (!succeed)
			return {};
		const auto measureResults = std::make_shared<Data::Measure>(measureMap, timestep);
		return DataMap { { "Measure", measureResults } };
	}

	auto ParentChild2d::Abort() -> void { }

	auto ParentChild2d::SetGetter(const DataAttrGetterPtr& getter) -> void {
		d->getter = getter;
	}
}
