#pragma once

#include <memory>

#include <QVariant>

#include "IService.h"

#include "CellAnalyzer.Processor.ProcessorModelExport.h"

namespace CellAnalyzer::Processor {
	class IProcessorAttr;
	using ProcessorAttrPtr = std::shared_ptr<IProcessorAttr>;
	using ProcessorAttrList = QVector<ProcessorAttrPtr>;
	using ProcessorAttrMap = QMap<QString, ProcessorAttrPtr>;
	using ProcessorAttrValue = QVariant;
	using ProcessorAttrModel = QVariant;

	enum class ProcessorAttrStyle {
		Visible,
		Disabled,
		Hidden
	};

	class CellAnalyzer_Processor_ProcessorModel_API IProcessorAttr : public virtual Tomocube::IService {
	public:
		virtual auto GetAttrModel() const -> ProcessorAttrModel = 0;
		virtual auto GetAttrValue() const -> ProcessorAttrValue = 0;
		virtual auto GetAttrStyle() const -> ProcessorAttrStyle = 0;

		virtual auto SetAttrModel(const ProcessorAttrModel& model) -> void = 0;
		virtual auto SetAttrValue(const ProcessorAttrValue& value) -> void = 0;
		virtual auto SetAttrStyle(ProcessorAttrStyle style) -> void = 0;
	};
}