#pragma once

#include <memory>

#include "IData.h"
#include "IDataAttrGetter.h"
#include "IProcessorAttr.h"
#include "IProcessorFactory.h"

#include "CellAnalyzer.Processor.ProcessorModelExport.h"

namespace CellAnalyzer::Processor {
	class CellAnalyzer_Processor_ProcessorModel_API IProcessor : public Tomocube::IService {
	public:
		virtual auto SetGetter(const DataAttrGetterPtr& getter) -> void;

		virtual auto SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void = 0;
		virtual auto SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void = 0;
		virtual auto SetInputData(const QString& inputID, const DataPtr& data) -> void = 0;

		virtual auto GetAttr(const QString& attrID) const -> ProcessorAttrPtr = 0;
		virtual auto GetInputData(const QString& inputID) const -> DataPtr = 0;

		virtual auto Process() -> DataMap = 0;
		virtual auto Abort() -> void = 0;
	};

	using ProcessorPtr = std::shared_ptr<IProcessor>;
	using ProcessorList = QList<ProcessorPtr>;
}

Q_DECLARE_INTERFACE(CellAnalyzer::Processor::IProcessor, "CellAnalyzer::Processor::IProcessor");

#define DEFAULT_PATH "../rsc/TypeInfo.json"

#define DECLARE_PROCESSOR_START(Name) \
	class Name##Factory final : public CellAnalyzer::Processor::IProcessorFactory

#define DECLARE_PROCESSOR_END(Name, Category, TypeInfo) \
		Q_INTERFACES(CellAnalyzer::Processor::IProcessorFactory)\
		Q_PLUGIN_METADATA(IID "CellAnalyzer::Processor::" #Category "::" #Name "Factory" FILE TypeInfo)\
	public:\
		auto CreateInstance() -> std::shared_ptr<IProcessor> override {\
			return std::make_shared<CellAnalyzer::Processor::##Category##::##Name>();\
		}
