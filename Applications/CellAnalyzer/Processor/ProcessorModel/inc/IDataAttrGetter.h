#pragma once

#include <memory>

#include "IData.h"

#include "CellAnalyzer.Processor.ProcessorModelExport.h"

namespace CellAnalyzer::Processor {
	class IDataAttrGetter;
	using DataAttrGetterPtr = std::shared_ptr<IDataAttrGetter>;

	class CellAnalyzer_Processor_ProcessorModel_API IDataAttrGetter : public virtual Tomocube::IService {
	public:
		virtual auto GetName(const DataPtr& data) const -> QString = 0;
	};
}