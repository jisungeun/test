#pragma once

#include <memory>

#include <QObject>
#include <QString>

#include "IService.h"

#include "CellAnalyzer.Processor.ProcessorModelExport.h"

namespace CellAnalyzer::Processor {
	class IProcessor;

	class CellAnalyzer_Processor_ProcessorModel_API IProcessorFactory : public QObject, public virtual Tomocube::IService {
		Q_OBJECT

	public:
		virtual auto CreateInstance() -> std::shared_ptr<IProcessor> = 0;
	};
}

Q_DECLARE_INTERFACE(CellAnalyzer::Processor::IProcessorFactory, "CellAnalyzer::Processor::IProcessorFactory");
