#pragma once

#include "IProcessor.h"

#include "CellAnalyzer.Processor.ImageDevExport.h"

namespace CellAnalyzer::Processor {
	class CellAnalyzer_Processor_ImageDev_API ImageDevProcessor : public IProcessor {
	public:
		ImageDevProcessor();
		~ImageDevProcessor() override;
	};
}