#pragma once

#include "IAppModule.h"

#include "CellAnalyzer.Processor.ImageDevExport.h"

namespace CellAnalyzer::Processor {
	class CellAnalyzer_Processor_ImageDev_API ImageDevModule final : public IAppModule {
	public:
		auto Start() -> std::optional<Error> override;
		auto Stop() -> void override;
	};
}