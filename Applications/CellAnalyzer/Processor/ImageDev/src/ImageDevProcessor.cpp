#include <iostream>

#include <QCoreApplication>
#include <ImageDev/Exception.h>
#include <ImageDev/Initialization.h>

#include "OivActivator.h"

#include "ImageDevProcessor.h"

namespace CellAnalyzer::Processor {
	ImageDevProcessor::ImageDevProcessor() {
		if (!imagedev::isInitialized()) {
			QMetaObject::invokeMethod(qApp, [] {
				QString keys;

				keys.append("INCREMENT ImageDev asglmd 2022.2 permanent uncounted ");
				keys.append("VENDOR_STRING=INTERNALKEY HOSTID=DEMO ISSUED=14-feb-2023 ");
				keys.append("NOTICE=TomoAnalysis SN=23022968-IMDEV START=13-feb-2023 ");
				QStringList ImageDevPW { "03H6", "3B4:", "AGH6", "EG8I", "E6;<", "4778", "3G67", "01GI", "D559", "4273", "6923", "BC95", "4B58", "A15H", "865D", "6DH;", "DDCF", "A2C;", "EB86", "648G", "2C59" };
				keys.append("ONE_TS_OK SIGN=\"");
				keys.append(OivActivator::Convert(ImageDevPW));
				keys.append("\"");

				if (!imagedev::isInitialized()) {
					try {
						imagedev::init(keys.toStdString().c_str());
					} catch (imagedev::Exception& e) {
						std::cout << e.what() << std::endl;
					}
				}
			}, Qt::QueuedConnection);
		}
	}

	ImageDevProcessor::~ImageDevProcessor() = default;
}
