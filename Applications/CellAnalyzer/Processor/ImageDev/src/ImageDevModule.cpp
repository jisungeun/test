#include <ImageDev/Initialization.h>

#include "ImageDevModule.h"

namespace CellAnalyzer::Processor {
	auto ImageDevModule::Start() -> std::optional<QString> {
		return std::nullopt;
	}

	auto ImageDevModule::Stop() -> void {
		if (imagedev::isInitialized())
			imagedev::finish();
	}
}