#include <QCoreApplication>

#include <enum.h>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <Float2d.h>
#include <HT2D.h>
#include <FL2D.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "ArithmeticOpWithImage2d.h"

namespace CellAnalyzer::Processor::Arithmetic {
	using namespace imagedev;
	using namespace iolink;
	struct ArithmeticOpertaionWithImage2d::Impl {
		const QMap<int, QString> arithmeticOperator{
			{ 0, "Add" },
			{ 1, "Subtract" },
			{ 2, "Multiply" },
			{ 3, "Divide" },
			{ 4, "Minimum" },
			{ 5, "Maximum" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	ArithmeticOpertaionWithImage2d::ArithmeticOpertaionWithImage2d() : d { std::make_unique<Impl>() } { }

	ArithmeticOpertaionWithImage2d::~ArithmeticOpertaionWithImage2d() { }

	auto ArithmeticOpertaionWithImage2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Arithmetic Operator") {
			attribute->SetAttrModel(QStringList(d->arithmeticOperator.values()));
		}		
	}

	auto ArithmeticOpertaionWithImage2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto ArithmeticOpertaionWithImage2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto ArithmeticOpertaionWithImage2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto ArithmeticOpertaionWithImage2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto ArithmeticOpertaionWithImage2d::Process() -> DataMap {
		TCImage::Pointer inputImage{ nullptr };
		std::shared_ptr<ImageView> refImageView{ nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		int timestep{ 0 };
		TCDataConverter converter;		
		if (const auto image = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage1"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		}
		else if (const auto image = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage1"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		}
		else if (const auto image = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage1"])) {
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			timestep = image->GetTimeStep();
		}
		else {
			return {};
		}

		TCImage::Pointer inputImage2{ nullptr };
		std::shared_ptr<ImageView> refImageView2{ nullptr };
		double min2, max2;
		double res2[3];
		int dimX2, dimY2, dimZ2;
		int timestep2;

		if (const auto image2 = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage2"])) {
			inputImage2 = Data::DataConverter::ConvertToTCImage(image2);
			const auto [imin, imax] = image2->GetRI();
			min2 = imin * 10000.0;
			max2 = imax * 10000.0;
			const auto [iresX, iresY] = image2->GetResolution();
			res2[0] = iresX;
			res2[1] = iresY;
			res2[2] = 0;
			const auto [idimx, idimy] = image2->GetSize();
			dimX2 = idimx;
			dimY2 = idimy;
			dimZ2 = 1;
			refImageView2 = converter.ImageToImageView(inputImage2);
			timestep2 = image2->GetTimeStep();
		}
		else if (const auto image2 = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage2"])) {
			inputImage2 = Data::DataConverter::ConvertToTCImage(image2);
			const auto [imin, imax] = image2->GetIntensity();
			min2 = imin;
			max2 = imax;
			const auto [iresX, iresY] = image2->GetResolution();
			res2[0] = iresX;
			res2[1] = iresY;
			res2[2] = 0;
			const auto [idimx, idimy] = image2->GetSize();
			dimX2 = idimx;
			dimY2 = idimy;
			dimZ2 = 1;
			refImageView2 = converter.ImageToImageView(inputImage2);
			timestep2 = image2->GetTimeStep();
		}
		else if (const auto image2 = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage2"])) {
			const auto [imin, imax] = image2->GetRange();
			min2 = imin;
			max2 = imax;
			const auto [iresX, iresY] = image2->GetResolution();
			res2[0] = iresX;
			res2[1] = iresY;
			res2[2] = 0;
			const auto [idimx, idimy] = image2->GetSize();
			dimX2 = idimx;
			dimY2 = idimy;
			dimZ2 = 1;
			refImageView2 = converter.FloatArrToImageView(static_cast<float*>(image2->GetData()), dimX2, dimY2, dimZ2, res2);
			timestep2 = image2->GetTimeStep();
		}
		else {
			return {};
		}

		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.0001;
		};
		if (dimX != dimX2 || dimY != dimY2) {
			return {};
		}
		for (auto i = 0; i < 2; i++) {
			if (false == AreSame(res[i], res2[i])) {
				return {};
			}
		}		

		// set algorithm parameters and input data
		const auto arithmeticOperator = d->attrMap["Arithmetic Operator"]->GetAttrValue().toString();

		try {
			const auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			const auto floatImage2 = convertImage(refImageView2, ConvertImage::FLOAT_32_BIT);

			const auto arithed = arithmeticOperationWithImage(floatImage, floatImage2, static_cast<ArithmeticOperationWithImage::ArithmeticOperator>(d->arithmeticOperator.key(arithmeticOperator)));

			const auto resultStat = intensityStatistics(arithed, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result{ nullptr };
			result = Data::DataConverter::ConvertToFloatData<Data::Float2D>(static_cast<float*>(arithed->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		}catch(Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return {  };
	}

	auto ArithmeticOpertaionWithImage2d::Abort() -> void { }
}
