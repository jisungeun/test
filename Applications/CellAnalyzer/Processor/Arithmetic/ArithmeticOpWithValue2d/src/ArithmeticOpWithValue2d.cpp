#include <QCoreApplication>

#include <enum.h>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <Float2d.h>
#include <HT2D.h>
#include <FL2D.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "ArithmeticOpWithValue2d.h"

namespace CellAnalyzer::Processor::Arithmetic {
	using namespace imagedev;
	using namespace iolink;
	
	struct ArithmeticOpertaionWithValue2d::Impl {
		const QMap<int, QString> arithmeticOperator{
			{ 0, "Add" },
			{ 1, "Subtract" },
			{ 2, "Multiply" },
			{ 3, "Divide" },
			{ 4, "Minimum" },
			{ 5, "Maximum" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	ArithmeticOpertaionWithValue2d::ArithmeticOpertaionWithValue2d() : d { std::make_unique<Impl>() } { }

	ArithmeticOpertaionWithValue2d::~ArithmeticOpertaionWithValue2d() { }

	auto ArithmeticOpertaionWithValue2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Arithmetic Operator") {
			attribute->SetAttrModel(QStringList(d->arithmeticOperator.values()));
		}
		if (id == "Coefficient") {
			attribute->SetAttrModel(QVariantMap{ { "Min", 0.1 }, { "Max", 10000 }, { "Step", 0.1 }, { "Decimals", 1 } });
		}
	}

	auto ArithmeticOpertaionWithValue2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto ArithmeticOpertaionWithValue2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto ArithmeticOpertaionWithValue2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto ArithmeticOpertaionWithValue2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto ArithmeticOpertaionWithValue2d::Process() -> DataMap {
		TCImage::Pointer inputImage{ nullptr };
		std::shared_ptr<ImageView> refImageView{ nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		TCDataConverter converter;
		int timestep{ 0 };
		if (const auto image = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		}
		else if (const auto image = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		}
		else if (const auto image = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			timestep = image->GetTimeStep();
		}
		else {
			return {};
		}

		// set algorithm parameters and input data
		const auto arithmeticOperator = d->attrMap["Arithmetic Operator"]->GetAttrValue().toString();
		const auto coefficient = d->attrMap["Coefficient"]->GetAttrValue().toDouble();
				
		try {
			const auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);

			const auto arithed = arithmeticOperationWithValue(floatImage, coefficient, static_cast<ArithmeticOperationWithValue::ArithmeticOperator>(d->arithmeticOperator.key(arithmeticOperator)));

			const auto resultStat = intensityStatistics(arithed, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result{ nullptr };
			result = Data::DataConverter::ConvertToFloatData<Data::Float2D>(static_cast<float*>(arithed->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		}catch(Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return {};
	}

	auto ArithmeticOpertaionWithValue2d::Abort() -> void { }
}
