#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include "TCDataConverter.h"

#include "MaskingLabel3d.h"

namespace CellAnalyzer::Processor::Logic {
	using namespace imagedev;
	using namespace iolink;

	struct MaskingLabel3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
		auto AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double;
	};

	auto MaskingLabel3d::Impl::AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double {
		return offset - static_cast<double>(refDimZ) * refResZ / 2.0;
	}

	MaskingLabel3d::MaskingLabel3d() : d { std::make_unique<Impl>() } { }

	MaskingLabel3d::~MaskingLabel3d() { }

	auto MaskingLabel3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
	}

	auto MaskingLabel3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto MaskingLabel3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto MaskingLabel3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto MaskingLabel3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto MaskingLabel3d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/filtering/TC.Algorithm.Filtering.Stencil.dll";

		// create an input data
		std::shared_ptr<iolink::ImageView> refMaskView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double refOffset { 0 };
		double targetOffset { 0 };
		bool isFL = false;
		bool isFloat = false;
		int timestep { 0 };
		TCDataConverter converter;
		if (const auto data = std::dynamic_pointer_cast<Data::LabelMask3D>(d->inputMap["InputMask"])) {
			const auto mask = Data::DataConverter::ConvertToTCMask(data);
			timestep = data->GetTimeStep();
			refOffset = data->GetZOffset();
			min = 0;
			max = mask->GetBlobIndexes().count();
			const auto [iresX, iresY, iresZ] = data->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimX, idimY, idimZ] = data->GetSize();
			dimX = idimX;
			dimY = idimY;
			dimZ = idimZ;
			refMaskView = converter.MaskToImageView(mask);
		} else {
			return {};
		}

		std::shared_ptr<iolink::ImageView> stencilMaskView { nullptr };
		double mres[3];
		int mdimX, mdimY, mdimZ;
		if (const auto data = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["StencilMask"])) {
			const auto stencilMask = Data::DataConverter::ConvertToTCMask(data);
			const auto [iresX, iresY, iresZ] = data->GetResolution();
			targetOffset = data->GetZOffset();
			mres[0] = iresX;
			mres[1] = iresY;
			mres[2] = iresZ;
			const auto [idimx, idimy, idimz] = data->GetSize();
			mdimX = idimx;
			mdimY = idimy;
			mdimZ = idimz;
			stencilMaskView = converter.MaskToImageView(stencilMask);
		} else {
			return {};
		}
		try {
			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};

			const auto imageOrigin = refMaskView->properties()->calibration().origin();
			const auto maskOrigin = stencilMaskView->properties()->calibration().origin();
			const auto imageSpacing = refMaskView->properties()->calibration().spacing();
			const auto maskSpacing = stencilMaskView->properties()->calibration().spacing();
			auto originDiff = false;
			auto spacingDiff = false;
			auto dims = 3;
			if (dimZ == 1) {
				dims = 2;
			}
			for (auto i = 0; i < dims; i++) {
				if (false == AreSame(imageOrigin[i], maskOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(imageSpacing[i], maskSpacing[i])) {
					spacingDiff = true;
				}
			}

			if (AreSame(refOffset, 0) && false == AreSame(targetOffset, 0)) {
				targetOffset = d->AdjustOffsetZ(targetOffset, refMaskView->shape()[2], imageSpacing[2]);
			} else if (false == AreSame(refOffset, 0) && AreSame(targetOffset, 0)) {
				refOffset = d->AdjustOffsetZ(refOffset, stencilMaskView->shape()[2], maskSpacing[2]);
			}

			if (refMaskView->shape() != stencilMaskView->shape() || originDiff || spacingDiff) {
				if(imageSpacing[2] > maskSpacing[2]) {
					refMaskView = TCDataConverter::MapMaskGeometry(stencilMaskView, refMaskView, targetOffset, refOffset);
				}
				else {
					stencilMaskView = TCDataConverter::MapMaskGeometry(refMaskView, stencilMaskView, refOffset, targetOffset);
				}
			}
			stencilMaskView = convertImage(stencilMaskView, ConvertImage::BINARY);
			const auto srcImage = maskImage(refMaskView, stencilMaskView);

			const auto minImage = resetImage(refMaskView, min);

			const auto resultImage = combineByMask(refMaskView, minImage, stencilMaskView);

			const auto resultStat = intensityStatistics(resultImage, IntensityStatistics::MIN_MAX, { 0, 1 });

			const auto processedImage = convertImage(resultImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			int dim[3] { dimX, dimY, dimZ };
			const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(processedImage->buffer()), dim, res);
			resultMask->SetTimeStep(timestep);
			resultMask->SetOffset(refOffset);

			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToMaskData<Data::LabelMask3D>(resultMask);

			if (nullptr == result) {
				return {};
			}
			return { { "OutputMask", result } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
			return {};
		}
	}

	auto MaskingLabel3d::Abort() -> void { }
}
