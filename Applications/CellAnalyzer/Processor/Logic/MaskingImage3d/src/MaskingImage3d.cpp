#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <TCDataConverter.h>

#include "MaskingImage3d.h"

namespace CellAnalyzer::Processor::Logic {
	using namespace imagedev;
	using namespace iolink;

	struct MaskingImage3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
		auto AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double;
	};

	auto MaskingImage3d::Impl::AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double {
		return offset - static_cast<double>(refDimZ) * refResZ / 2.0;
	}

	MaskingImage3d::MaskingImage3d() : d { std::make_unique<Impl>() } { }

	MaskingImage3d::~MaskingImage3d() { }

	auto MaskingImage3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
	}

	auto MaskingImage3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto MaskingImage3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto MaskingImage3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto MaskingImage3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto MaskingImage3d::Process() -> DataMap {
		// create an input data
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<iolink::ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double refOffset { 0 };
		double targetOffset { 0 };
		bool isFL = false;
		bool isFloat = false;
		int timestep { 0 };
		int ch;
		QString chName;
		TCDataConverter converter;
		if (const auto data = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(data);
			timestep = data->GetTimeStep();
			const auto [imin, imax] = data->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = data->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = data->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto data = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(data);
			timestep = data->GetTimeStep();
			isFL = true;
			ch = data->GetChannelIndex();
			chName = data->GetChannelName();
			const auto [imin, imax] = data->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = data->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = data->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refOffset = data->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			timestep = image->GetTimeStep();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refOffset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			isFloat = true;
		} else {
			return {};
		}

		TCMask::Pointer stencilMask { nullptr };
		std::shared_ptr<iolink::ImageView> refMaskView { nullptr };
		double mres[3];
		int mdimX, mdimY, mdimZ;
		if (const auto data = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["StencilMask"])) {
			stencilMask = Data::DataConverter::ConvertToTCMask(data);
			const auto [iresX, iresY, iresZ] = data->GetResolution();
			mres[0] = iresX;
			mres[1] = iresY;
			mres[2] = iresZ;
			const auto [idimx, idimy, idimz] = data->GetSize();
			mdimX = idimx;
			mdimY = idimy;
			mdimZ = idimz;
			refMaskView = converter.MaskToImageView(stencilMask);
			targetOffset = data->GetZOffset();
		} else {
			return {};
		}
		try {
			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};

			const auto imageOrigin = refImageView->properties()->calibration().origin();
			const auto maskOrigin = refMaskView->properties()->calibration().origin();
			const auto imageSpacing = refImageView->properties()->calibration().spacing();
			const auto maskSpacing = refMaskView->properties()->calibration().spacing();
			auto originDiff = false;
			auto spacingDiff = false;
			auto dims = 3;
			if (dimZ == 1) {
				dims = 2;
			}
			for (auto i = 0; i < dims; i++) {
				if (false == AreSame(imageOrigin[i], maskOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(imageSpacing[i], maskSpacing[i])) {
					spacingDiff = true;
				}
			}

			if (AreSame(refOffset, 0) && false == AreSame(targetOffset, 0)) {
				targetOffset = d->AdjustOffsetZ(targetOffset, refImageView->shape()[2], imageSpacing[2]);
			} else if (false == AreSame(refOffset, 0) && AreSame(targetOffset, 0)) {
				refOffset = d->AdjustOffsetZ(refOffset, refMaskView->shape()[2], maskSpacing[2]);
			}

			if (refImageView->shape() != refMaskView->shape() || originDiff || spacingDiff) {
				refMaskView = TCDataConverter::MapMaskGeometry(refImageView, refMaskView, refOffset, targetOffset);
			}

			refMaskView = convertImage(refMaskView, ConvertImage::BINARY);

			const auto srcImage = maskImage(refImageView, refMaskView);

			const auto minImage = resetImage(refImageView, min);

			const auto resultImage = combineByMask(refImageView, minImage, refMaskView);

			const auto resultStat = intensityStatistics(resultImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result { nullptr };
			if (isFL) {
				const auto processedImage = convertImage(resultImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToFLData<Data::FL3D>(static_cast<uint16_t*>(processedImage->buffer()), ch, chName, dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, refOffset);
			} else if (isFloat) {
				result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(resultImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, refOffset);
			} else {
				const auto processedImage = convertImage(resultImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToHTData<Data::HT3D>(static_cast<uint16_t*>(processedImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			}
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto MaskingImage3d::Abort() -> void { }
}
