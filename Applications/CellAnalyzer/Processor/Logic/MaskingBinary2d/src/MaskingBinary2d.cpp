#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Exception.h>
#include <iolink/view/ImageView.h>
#include "TCDataConverter.h"

#include "MaskingBinary2d.h"

namespace CellAnalyzer::Processor::Logic {
	using namespace imagedev;
	using namespace iolink;

	struct MaskingBinary2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	MaskingBinary2d::MaskingBinary2d() : d { std::make_unique<Impl>() } { }

	MaskingBinary2d::~MaskingBinary2d() { }

	auto MaskingBinary2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
	}

	auto MaskingBinary2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto MaskingBinary2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto MaskingBinary2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto MaskingBinary2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto MaskingBinary2d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/filtering/TC.Algorithm.Filtering.Stencil.dll";

		// create an input data
		std::shared_ptr<ImageView> refMaskView { nullptr };
		double min, max;
		double res[3];
		int timestep { 0 };
		int dimX, dimY, dimZ;
		TCDataConverter converter;
		if (const auto data = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["InputMask"])) {
			const auto mask = Data::DataConverter::ConvertToTCMask(data);
			timestep = data->GetTimeStep();
			min = 0;
			max = mask->GetBlobIndexes().count();
			const auto [iresX, iresY] = data->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = data->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refMaskView = converter.MaskToImageView(mask);
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		TCMask::Pointer stencilMask { nullptr };
		std::shared_ptr<ImageView> stencilMaskView { nullptr };
		double mres[3];
		int mdimX, mdimY, mdimZ;
		if (const auto data = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["StencilMask"])) {
			stencilMask = Data::DataConverter::ConvertToTCMask(data);
			const auto [iresX, iresY] = data->GetResolution();
			mres[0] = iresX;
			mres[1] = iresY;
			mres[2] = 0;
			const auto [idimx, idimy] = data->GetSize();
			mdimX = idimx;
			mdimY = idimy;
			mdimZ = 1;
			stencilMaskView = converter.MaskToImageView(stencilMask, TCDataConverter::MaskType::Binary);
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		try {
			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};
			const auto imageOrigin = refMaskView->properties()->calibration().origin();
			const auto maskOrigin = stencilMaskView->properties()->calibration().origin();
			const auto imageSpacing = refMaskView->properties()->calibration().spacing();
			const auto maskSpacing = stencilMaskView->properties()->calibration().spacing();

			auto originDiff = false;
			auto spacingDiff = false;
			auto dims = 2;
			for (auto i = 0; i < dims; i++) {
				if (false == AreSame(imageOrigin[i], maskOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(imageSpacing[i], maskSpacing[i])) {
					spacingDiff = true;
				}
			}
			if (refMaskView->shape() != stencilMaskView->shape() || originDiff || spacingDiff) {
				stencilMaskView = TCDataConverter::MapMaskGeometry(refMaskView, stencilMaskView);
			}

			const auto srcImage = maskImage(refMaskView, stencilMaskView);

			const auto minImage = resetImage(refMaskView, min);

			const auto resultImage = combineByMask(refMaskView, minImage, stencilMaskView);

			const auto resultStat = intensityStatistics(resultImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			if (resultStat->maximum() < 1) {
				return {};
			}

			const auto processedImage = convertImage(resultImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
			int dim[3] { dimX, dimY, dimZ };

			const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(processedImage->buffer()), dim, res);
			resultMask->SetTimeStep(timestep);
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask2D>(resultMask);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputMask", result } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
			return {};
		}
	}

	auto MaskingBinary2d::Abort() -> void { }
}
