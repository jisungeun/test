#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <TCDataConverter.h>

#include "MaskingImage2d.h"

namespace CellAnalyzer::Processor::Logic {
	using namespace imagedev;
	using namespace iolink;

	struct MaskingImage2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	MaskingImage2d::MaskingImage2d() : d { std::make_unique<Impl>() } { }

	MaskingImage2d::~MaskingImage2d() { }

	auto MaskingImage2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
	}

	auto MaskingImage2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto MaskingImage2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto MaskingImage2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto MaskingImage2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto MaskingImage2d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<iolink::ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		bool isFL = false;
		bool isFloat = false;
		int timestep { 0 };
		double offset;
		int ch;
		QString chName;
		TCDataConverter converter;
		if (const auto data = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			timestep = data->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(data);
			const auto [imin, imax] = data->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY] = data->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = data->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto data = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			timestep = data->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(data);
			isFL = true;
			chName = data->GetChannelName();
			ch = data->GetChannelIndex();
			const auto [imin, imax] = data->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = data->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = data->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			isFloat = true;
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		TCMask::Pointer stencilMask { nullptr };
		std::shared_ptr<iolink::ImageView> refMaskView { nullptr };
		double mres[3];
		int mdimX, mdimY, mdimZ;
		if (const auto data = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["StencilMask"])) {
			stencilMask = Data::DataConverter::ConvertToTCMask(data);
			const auto [iresX, iresY] = data->GetResolution();
			mres[0] = iresX;
			mres[1] = iresY;
			mres[2] = 0;
			const auto [idimx, idimy] = data->GetSize();
			mdimX = idimx;
			mdimY = idimy;
			mdimZ = 1;
			refMaskView = converter.MaskToImageView(stencilMask, TCDataConverter::MaskType::Binary);
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		try {
			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};

			const auto imageOrigin = refImageView->properties()->calibration().origin();
			const auto maskOrigin = refMaskView->properties()->calibration().origin();
			const auto imageSpacing = refImageView->properties()->calibration().spacing();
			const auto maskSpacing = refMaskView->properties()->calibration().spacing();
			auto originDiff = false;
			auto spacingDiff = false;
			auto dims = 2;
			for (auto i = 0; i < dims; i++) {
				if (false == AreSame(imageOrigin[i], maskOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(imageSpacing[i], maskSpacing[i])) {
					spacingDiff = true;
				}
			}
			if (refImageView->shape() != refMaskView->shape() || originDiff || spacingDiff) {
				refMaskView = TCDataConverter::MapMaskGeometry(refImageView, refMaskView);
			}

			const auto srcImage = maskImage(refImageView, refMaskView);

			const auto minImage = resetImage(refImageView, min);

			const auto resultImage = combineByMask(refImageView, minImage, refMaskView);

			const auto resultStat = intensityStatistics(resultImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result { nullptr };
			if (isFL) {
				const auto processedImage = convertImage(resultImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToFLData<Data::FL2D>(static_cast<uint16_t*>(processedImage->buffer()), ch, chName, dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			} else if (isFloat) {
				result = Data::DataConverter::ConvertToFloatData<Data::Float2D>(static_cast<float*>(resultImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			} else {
				const auto processedImage = convertImage(resultImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToHTData<Data::HT2D>(static_cast<uint16_t*>(processedImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			}
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto MaskingImage2d::Abort() -> void { }
}
