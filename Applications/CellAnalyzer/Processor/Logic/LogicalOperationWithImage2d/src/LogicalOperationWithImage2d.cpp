#include <QCoreApplication>

#include <enum.h>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include "LogicalOperationWithImage2d.h"

namespace CellAnalyzer::Processor::Logic {
	BETTER_ENUM(LogicalOperator, int32_t,
				AND,
				OR,
				XOR,
				SUBTRACT = 6
				);

	struct LogicalOperationWithImage2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	LogicalOperationWithImage2d::LogicalOperationWithImage2d() : d { std::make_unique<Impl>() } { }

	LogicalOperationWithImage2d::~LogicalOperationWithImage2d() { }

	auto LogicalOperationWithImage2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Logical Operator") {
			QStringList operatorNames;
			for (auto name : LogicalOperator::_names()) {
				operatorNames << name;
			}

			attribute->SetAttrModel(operatorNames);
		}
	}

	auto LogicalOperationWithImage2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto LogicalOperationWithImage2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto LogicalOperationWithImage2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto LogicalOperationWithImage2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto LogicalOperationWithImage2d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/filtering/TC.Algorithm.Filtering.LogicalOperationWithImage.dll";

		// create an input data
		TCMask::Pointer inputMask1 { nullptr };
		int timestep { 0 };
		if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["InputMask1"])) {
			inputMask1 = Data::DataConverter::ConvertToTCMask(mask);
			timestep = inputMask1->GetTimeStep();
		} else {
			return {};
		}

		TCMask::Pointer inputMask2 { nullptr };
		if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["InputMask2"]))
			inputMask2 = Data::DataConverter::ConvertToTCMask(mask);
		else {
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		// set algorithm parameters and input data
		const auto logicalOperator = d->attrMap["Logical Operator"]->GetAttrValue().toString();

		better_enums::optional<LogicalOperator> logicalOperatorIndex = LogicalOperator::_from_string_nothrow(logicalOperator.toStdString().c_str());
		if (!logicalOperatorIndex) {
			return {};
		}

		auto params = algorithm->Parameter();
		params->SetValue("LogicalOperator", (*logicalOperatorIndex)._to_integral());

		algorithm->SetInput(0, inputMask1);
		algorithm->SetInput(1, inputMask2);
		if (!algorithm->Execute()) {
			return {};
		}

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetTimeStep(timestep);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask2D>(resultMask);
		if (result == nullptr) {
			return {};
		}

		return { { "OutputMask", result } };
	}

	auto LogicalOperationWithImage2d::Abort() -> void { }
}
