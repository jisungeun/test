#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include "LogicalNot2d.h"

namespace CellAnalyzer::Processor::Logic {
	struct LogicalNot2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	LogicalNot2d::LogicalNot2d() : d { std::make_unique<Impl>() } { }

	LogicalNot2d::~LogicalNot2d() { }

	auto LogicalNot2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
	}

	auto LogicalNot2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto LogicalNot2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto LogicalNot2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto LogicalNot2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto LogicalNot2d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/filtering/TC.Algorithm.Filtering.LogicalNot.dll";

		// create an input data
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			timestep = mask->GetTimeStep();
		} else {
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		// set input data
		algorithm->SetInput(0, inputMask);
		if (!algorithm->Execute()) {
			return {};
		}

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetTimeStep(timestep);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask2D>(resultMask);
		if (result == nullptr) {
			return {};
		}

		return { { "OutputMask", result } };
	}

	auto LogicalNot2d::Abort() -> void { }
}
