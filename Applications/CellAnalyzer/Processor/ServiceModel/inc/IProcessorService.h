#pragma once

#include "IService.h"

#include "IProcessor.h"
#include "IProcessType.h"

#include "CellAnalyzer.Processor.ServiceModelExport.h"

namespace CellAnalyzer::Processor {
	using ProcessorTypePtr = Pipeline::ProcessTypePtr;

	class CellAnalyzer_Processor_ServiceModel_API IProcessorService : public virtual Tomocube::IService {
	public:
		virtual auto GetTypeList() const -> QStringList = 0;
		virtual auto GetTypeInfo(const QString& id) const -> ProcessorTypePtr = 0;

		virtual auto CreateProcessor(const QString& id) const -> ProcessorPtr = 0;

	protected:
		static auto GetLibMap() -> QMap<QString, QStringList>;
		static auto GetLibPath(const QString& category, const QString& process) -> QString;
	};
}
