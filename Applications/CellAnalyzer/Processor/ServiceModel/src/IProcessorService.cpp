#include <QCoreApplication>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include "IProcessorService.h"

namespace CellAnalyzer::Processor {
	auto IProcessorService::GetLibMap() -> QMap<QString, QStringList> {
		if (QFile file(":/ProcessList.json"); file.open(QIODevice::ReadOnly)) {
			QMap<QString, QStringList> libMap;
			const auto doc = QJsonDocument::fromJson(file.readAll());
			const auto obj = doc.object();
			const auto map = obj.toVariantMap();

			for (const auto& category : map.keys())
				libMap[category] = map[category].toStringList();

			return libMap;
		}

		return {};
	}

	auto IProcessorService::GetLibPath(const QString& category, const QString& process) -> QString {
		const auto appPath = QCoreApplication::applicationDirPath();

		if (category.isEmpty())
			return QString("%1/Processor/CellAnalyzer.Processor.%2.dll").arg(appPath).arg(process);

		return QString("%1/Processor/CellAnalyzer.Processor.%2.%3.dll").arg(appPath).arg(category).arg(process);
	}
}
