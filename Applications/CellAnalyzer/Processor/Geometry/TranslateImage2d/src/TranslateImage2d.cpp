#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <DataConverter.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <TCDataConverter.h>
#include "TranslateImage2d.h"

namespace CellAnalyzer::Processor::Geometry {
	using namespace imagedev;
	using namespace iolink;

	struct TranslateImage2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	TranslateImage2d::TranslateImage2d() : d { std::make_unique<Impl>() } { }

	TranslateImage2d::~TranslateImage2d() { }

	auto TranslateImage2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Translation X" || id == "Translation Y") {
			attribute->SetAttrModel(QVariantMap { { "Min", -9999 }, { "Max", 9999 }, { "Step", 1 } });
		}
	}

	auto TranslateImage2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto TranslateImage2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto TranslateImage2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto TranslateImage2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto TranslateImage2d::Process() -> DataMap {
		// create an input data
		TCImage::Pointer inputImage { nullptr };
		int timestep { 0 };
		std::shared_ptr<iolink::ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		bool isFL = false;
		bool isFloat = false;
		int chIdx { 0 };
		QString chName;
		TCDataConverter converter;
		if (const auto data = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(data);
			timestep = data->GetTimeStep();
			const auto [imin, imax] = data->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY] = data->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = data->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto data = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(data);
			timestep = data->GetTimeStep();
			chIdx = data->GetChannelIndex();
			chName = data->GetChannelName();
			isFL = true;
			const auto [imin, imax] = data->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = data->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = data->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			timestep = image->GetTimeStep();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			isFloat = true;
		} else {
			return {};
		}
		const auto tx = d->attrMap["Translation X"]->GetAttrValue().toInt();
		const auto ty = d->attrMap["Translation Y"]->GetAttrValue().toInt();
		try {
			const auto resultImage = translateImage2d(refImageView, { tx, ty }, imagedev::TranslateImage2d::BackgroundMode::FIXED, min);
			const auto resultStat = intensityStatistics(resultImage, IntensityStatistics::MIN_MAX, { 0, 1 });

			DataPtr result { nullptr };
			if (isFL) {
				const auto processedImage = convertImage(resultImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToFLData<Data::FL2D>(static_cast<uint16_t*>(processedImage->buffer()), chIdx, chName, dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			} else if (isFloat) {
				result = Data::DataConverter::ConvertToFloatData<Data::Float2D>(static_cast<float*>(resultImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			} else {
				const auto processedImage = convertImage(resultImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToHTData<Data::HT2D>(static_cast<uint16_t*>(processedImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			}
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto TranslateImage2d::Abort() -> void { }
}
