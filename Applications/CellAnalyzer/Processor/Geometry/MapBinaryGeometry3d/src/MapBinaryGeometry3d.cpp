#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>


#include "MapBinaryGeometry3d.h"

#include "TCDataConverter.h"

namespace CellAnalyzer::Processor::Geometry {
	using ImageViewPtr = std::shared_ptr<iolink::ImageView>;
	using namespace imagedev;
	using namespace iolink;
	struct MapBinaryGeometry3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;

		auto AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double;
	};

	auto MapBinaryGeometry3d::Impl::AdjustOffsetZ(double offset, int refDimZ, double refResZ) -> double {
		return offset - static_cast<double>(refDimZ) * refResZ / 2.0;
	}

	MapBinaryGeometry3d::MapBinaryGeometry3d() : d { std::make_unique<Impl>() } { }

	MapBinaryGeometry3d::~MapBinaryGeometry3d() { }

	auto MapBinaryGeometry3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;		
	}

	auto MapBinaryGeometry3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto MapBinaryGeometry3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto MapBinaryGeometry3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto MapBinaryGeometry3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto MapBinaryGeometry3d::Process() -> DataMap {
		TCImage::Pointer inputImage{ nullptr };
		std::shared_ptr<ImageView> refImageView{ nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset{ 0 };
		int timestep{ 0 };
		QString chName;
		int ch;
		TCDataConverter converter;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		}
		else if (const auto flimage = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(flimage);
			timestep = flimage->GetTimeStep();
			ch = flimage->GetChannelIndex();
			offset = flimage->GetZOffset();
			const auto [imin, imax] = flimage->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = flimage->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = flimage->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		}
		else if (const auto floatimage = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			timestep = floatimage->GetTimeStep();
			offset = floatimage->GetZOffset();
			const auto [imin, imax] = floatimage->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = floatimage->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = floatimage->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(floatimage->GetData()), dimX, dimY, dimZ, res);
		}
		else {
			return {};
		}

		std::shared_ptr<ImageView> maskView{ nullptr };		
		int mtimestep{ 0 };
		int mdimX, mdimY, mdimZ;
		double resX, resY, resZ;
		int max_index;
		float moffset{ 0 };

		if (const auto label = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["InputMask"])) {
			mtimestep = label->GetTimeStep();
			mdimX = label->GetSize().x;
			mdimY = label->GetSize().y;
			mdimZ = label->GetSize().z;
			resX = label->GetResolution().x;
			resY = label->GetResolution().y;
			resZ = label->GetResolution().z;
			moffset = label->GetZOffset();
			const auto labelMask = Data::DataConverter::ConvertToTCMask(label);
			maskView = converter.MaskToImageView(labelMask);
		}
		else {
			return {};
		}

		try {
			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};

			if(AreSame(offset,0) && false == AreSame(moffset,0)) {
				moffset = d->AdjustOffsetZ(moffset, dimZ, res[2]);
			}

			if (false == AreSame(offset, 0) && AreSame(moffset, 0)) {
				offset = d->AdjustOffsetZ(offset, mdimZ, resZ);
			}
						
			maskView = TCDataConverter::MapMaskGeometry(refImageView, maskView, offset, moffset);

			maskView = convertImage(maskView, ConvertImage::LABEL_16_BIT);

			int resultDim[3] = { dimX,dimY,dimZ };
			const auto result = converter.ArrToLabelMask(static_cast<unsigned short*>(maskView->buffer()), resultDim, res);
			result->SetOffset(offset);
			result->SetTimeStep(timestep);

			const auto output = Data::DataConverter::ConvertToMaskData<Data::BinaryMask3D>(result);
			return { {"OutputMask",output} };
		}catch(...) {
			
		}
		return {  };
	}

	auto MapBinaryGeometry3d::Abort() -> void { }
}
