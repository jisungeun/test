#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <TCDataConverter.h>
#include "ResizeImage3d.h"

namespace CellAnalyzer::Processor::Geometry {
	using namespace imagedev;
	using namespace iolink;

	struct ResizeImage3d::Impl {
		QMap<int, QString> interpolationMode { { 0, "Nearest Neighbor" }, { 1, "Linear" }, { 2, "Spline" } };
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	ResizeImage3d::ResizeImage3d() : d { std::make_unique<Impl>() } { }

	ResizeImage3d::~ResizeImage3d() { }

	auto ResizeImage3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Size X" || id == "Size Y" || id == "Size Z") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 100000 }, { "Step", 1 } });
		}
		if (id == "Interpolation type") {
			attribute->SetAttrModel(QStringList(d->interpolationMode.values()));
		}
	}

	auto ResizeImage3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto ResizeImage3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto ResizeImage3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto ResizeImage3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto ResizeImage3d::Process() -> DataMap {
		// create an input data
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<iolink::ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset { 0 };
		int timestep { 0 };
		bool isFL = false;
		bool isFloat = false;
		int chIdx { 0 };
		QString chName;
		TCDataConverter converter;
		if (const auto data = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(data);
			timestep = data->GetTimeStep();
			const auto [imin, imax] = data->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = data->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = data->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto data = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(data);
			timestep = data->GetTimeStep();
			chIdx = data->GetChannelIndex();
			chName = data->GetChannelName();
			isFL = true;
			const auto [imin, imax] = data->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = data->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = data->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = data->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			timestep = image->GetTimeStep();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			isFloat = true;
		} else {
			return {};
		}

		const auto sx = d->attrMap["Size X"]->GetAttrValue().toInt();
		const auto sy = d->attrMap["Size Y"]->GetAttrValue().toInt();
		const auto sz = d->attrMap["Size Z"]->GetAttrValue().toInt();
		const auto mode = d->attrMap["Interpolation type"]->GetAttrValue().toString();
		const auto modeIdx = static_cast<RescaleImage3d::InterpolationType>(d->interpolationMode.key(mode));

		try {			
			const auto dataType = refImageView->dataType();
			const auto floated = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			const auto resultImage = rescaleImage3d(floated, sx, sy, sz, modeIdx);

			ConvertImage::OutputType otype;
			if(dataType == DataTypeId::FLOAT) {
				otype = ConvertImage::OutputType::FLOAT_32_BIT;
			}else if(dataType == DataTypeId::UINT16) {
				otype = ConvertImage::OutputType::UNSIGNED_INTEGER_16_BIT;
			}else if(dataType == DataTypeId::UINT8) {
				otype = ConvertImage::OutputType::UNSIGNED_INTEGER_8_BIT;
			}

			const auto restored = convertImage(resultImage, otype);
			const auto resultStat = intensityStatistics(restored, IntensityStatistics::MIN_MAX, { 0, 1 });

			const auto x_ratio = static_cast<float>(dimX) / static_cast<float>(sx);
			const auto y_ratio = static_cast<float>(dimY) / static_cast<float>(sy);
			const auto z_ratio = static_cast<float>(dimZ) / static_cast<float>(sz);

			DataPtr result { nullptr };
			if (isFL) {
				const auto processedImage = convertImage(restored, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToFLData<Data::FL3D>(static_cast<uint16_t*>(processedImage->buffer()), chIdx, chName, sx, sy, sz, res[0] * x_ratio, res[1] * y_ratio, res[2] * z_ratio, resultStat->minimum(), resultStat->maximum(), timestep, offset);
			} else if (isFloat) {
				result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(restored->buffer()), sx, sy, sz, res[0] * x_ratio, res[1] * y_ratio, res[2] * z_ratio, resultStat->minimum(), resultStat->maximum(), timestep, offset);
			} else {
				const auto processedImage = convertImage(restored, ConvertImage::UNSIGNED_INTEGER_16_BIT);
				result = Data::DataConverter::ConvertToHTData<Data::HT3D>(static_cast<uint16_t*>(processedImage->buffer()), sx, sy, sz, res[0] * x_ratio, res[1] * y_ratio, res[2] * z_ratio, resultStat->minimum(), resultStat->maximum(), timestep);
			}
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto ResizeImage3d::Abort() -> void { }
}
