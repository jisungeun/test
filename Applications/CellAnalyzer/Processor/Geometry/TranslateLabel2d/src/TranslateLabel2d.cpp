#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include "TranslateLabel2d.h"

namespace CellAnalyzer::Processor::Geometry {
	struct TranslateLabel2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	TranslateLabel2d::TranslateLabel2d() : d { std::make_unique<Impl>() } { }

	TranslateLabel2d::~TranslateLabel2d() { }

	auto TranslateLabel2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Translation X" || id == "Translation Y") {
			attribute->SetAttrModel(QVariantMap { { "Min", -9999 }, { "Max", 9999 }, { "Step", 1 } });
		}
	}

	auto TranslateLabel2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto TranslateLabel2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto TranslateLabel2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto TranslateLabel2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto TranslateLabel2d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/geometry/TC.Algorithm.Geometry.Translate.2D.dll";

		// create an input data
		TCImage::Pointer inputImage { nullptr };
		bool isFL = false;
		int timestep { 0 };
		if (const auto data = std::dynamic_pointer_cast<Data::LabelMask2D>(d->inputMap["InputMask"])) {
			const auto mask = Data::DataConverter::ConvertToTCMask(data);
			timestep = data->GetTimeStep();
			inputImage = std::make_shared<TCImage>();

			const auto [resX, resY, resZ] = mask->GetResolution();
			const auto [dimX, dimY, dimZ] = mask->GetSize();

			const auto bufferSize = dimX * dimY;

			std::shared_ptr<unsigned short[]> arr(new unsigned short[bufferSize](), std::default_delete<unsigned short[]>());
			memcpy(arr.get(), mask->GetMaskVolume().get(), bufferSize * sizeof(unsigned short));

			double res[3] = { resX, resY, resZ };
			inputImage->SetResolution(res);
			inputImage->SetBoundingBox(0, 0, 0, dimX - 1, dimY - 1, 0);
			inputImage->SetImageVolume(arr);
			inputImage->SetMinMax(0, 1);
			inputImage->SetOffset(mask->GetOffset());
		} else {
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		const auto param = algorithm->Parameter();
		param->SetValue("TranslationX", d->attrMap["Translation X"]->GetAttrValue().toInt());
		param->SetValue("TranslationY", d->attrMap["Translation Y"]->GetAttrValue().toInt());

		// set input data
		algorithm->SetInput(0, inputImage);
		if (!algorithm->Execute()) {
			return {};
		}

		const auto resultImage = std::dynamic_pointer_cast<TCImage>(algorithm->GetOutput(0));
		if (resultImage == nullptr || resultImage->GetBuffer() == nullptr) {
			return {};
		}

		// convert TCImage to BinaryData2d
		double res[3];
		resultImage->GetResolution(res);
		auto [dimX, dimY, dimZ] = resultImage->GetSize();

		const auto bufferSize = dimX * dimY;
		std::unique_ptr<uint16_t[]> resultArr(new uint16_t[bufferSize](), std::default_delete<uint16_t[]>());
		memcpy(resultArr.get(), resultImage->GetBuffer(), bufferSize * sizeof(uint16_t));

		const auto [min, max] = resultImage->GetMinMax();

		auto result = std::shared_ptr<Data::LabelMask2D>(new Data::LabelMask2D(std::move(resultArr), max, { dimX, dimY }, { res[0], res[1] }, { -dimX * res[0] / 2, -dimY * res[1] / 2 }, timestep));

		return { { "OutputMask", result } };
	}

	auto TranslateLabel2d::Abort() -> void { }
}
