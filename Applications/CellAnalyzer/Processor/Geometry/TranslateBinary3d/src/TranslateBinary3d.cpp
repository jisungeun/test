#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include "TranslateBinary3d.h"

namespace CellAnalyzer::Processor::Geometry {
	struct TranslateBinary3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	TranslateBinary3d::TranslateBinary3d(): d { std::make_unique<Impl>() } { }

	TranslateBinary3d::~TranslateBinary3d() { }

	auto TranslateBinary3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Translation X" || id == "Translation Y" || id == "Translation Z") {
			attribute->SetAttrModel(QVariantMap { { "Min", -9999 }, { "Max", 9999 }, { "Step", 1 } });
		}
	}

	auto TranslateBinary3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto TranslateBinary3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto TranslateBinary3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto TranslateBinary3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto TranslateBinary3d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/geometry/TC.Algorithm.Geometry.Translate.3D.dll";

		// create an input data
		TCImage::Pointer inputImage { nullptr };
		int timestep { 0 };
		double offset { 0 };
		bool isFL = false;
		if (const auto data = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["InputMask"])) {
			const auto mask = Data::DataConverter::ConvertToTCMask(data);
			timestep = data->GetTimeStep();
			offset = data->GetZOffset();
			inputImage = std::make_shared<TCImage>();

			const auto [resX, resY, resZ] = mask->GetResolution();
			auto [dimX, dimY, dimZ] = mask->GetSize();
			if (dimZ < 1)
				dimZ = 1;

			const auto bufferSize = dimX * dimY * dimZ;

			std::shared_ptr<unsigned short[]> arr(new unsigned short[bufferSize](), std::default_delete<unsigned short[]>());
			memcpy(arr.get(), mask->GetMaskVolume().get(), bufferSize * sizeof(unsigned short));

			double res[3] = { resX, resY, resZ };
			inputImage->SetResolution(res);
			inputImage->SetBoundingBox(0, 0, 0, dimX - 1, dimY - 1, dimZ - 1);
			inputImage->SetImageVolume(arr);
			inputImage->SetMinMax(0, 1);
			inputImage->SetOffset(mask->GetOffset());
		} else {
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		const auto param = algorithm->Parameter();
		param->SetValue("TranslationX", d->attrMap["Translation X"]->GetAttrValue().toInt());
		param->SetValue("TranslationY", d->attrMap["Translation Y"]->GetAttrValue().toInt());
		param->SetValue("TranslationZ", d->attrMap["Translation Z"]->GetAttrValue().toInt());

		// set input data
		algorithm->SetInput(0, inputImage);
		if (!algorithm->Execute()) {
			return {};
		}

		const auto resultImage = std::dynamic_pointer_cast<TCImage>(algorithm->GetOutput(0));
		if (resultImage == nullptr || resultImage->GetBuffer() == nullptr) {
			return {};
		}

		// convert TCImage to BinaryData3d
		double res[3];
		resultImage->GetResolution(res);
		auto [dimX, dimY, dimZ] = resultImage->GetSize();
		if (dimZ < 1)
			dimZ = 1;

		const auto bufferSize = dimX * dimY * dimZ;
		std::unique_ptr<uint16_t[]> resultArr(new uint16_t[bufferSize](), std::default_delete<uint16_t[]>());
		memcpy(resultArr.get(), resultImage->GetBuffer(), bufferSize * sizeof(uint16_t));

		auto result = std::shared_ptr<Data::BinaryMask3D>(new Data::BinaryMask3D(std::move(resultArr), { dimX, dimY, dimZ }, { res[0], res[1], res[2] }, { -dimX * res[0] / 2, -dimY * res[1] / 2, -dimZ * res[2] / 2 }, offset, timestep));

		return { { "OutputMask", result } };
	}

	auto TranslateBinary3d::Abort() -> void { }
}
