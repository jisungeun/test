#pragma once

#include <IProcessor.h>

namespace CellAnalyzer::Processor::MophologicalOperation {
	class Closing25d final : public IProcessor {
	public:
		Closing25d();
		~Closing25d() override;

		auto SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void override;
		auto SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void override;
		auto SetInputData(const QString& inputID, const DataPtr& data) -> void override;

		auto GetAttr(const QString& attrID) const -> ProcessorAttrPtr override;
		auto GetInputData(const QString& inputID) const -> DataPtr override;

		auto Process() -> DataMap override;
		auto Abort() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	DECLARE_PROCESSOR_START(Closing25d) {
		Q_OBJECT
		DECLARE_PROCESSOR_END(Closing25d, MophologicalOperation, DEFAULT_PATH)
	};
}
