#include "Opening25d.h"
#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <QCoreApplication>
#include <TCMask.h>

namespace CellAnalyzer::Processor::MophologicalOperation {
	struct Opening25d::Impl {
		const QMap<int, QString> paramNeighborhood {
			{ 0, "4-neighborhood" },
			{ 1, "8-neighborhood" }
		};
		const QMap<int, QString> paramBorderpolicy {
			{ 0, "Ignore" },
			{ 1, "Extended" }
		};

		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	Opening25d::Opening25d() : d { std::make_unique<Impl>() } { }

	Opening25d::~Opening25d() { }

	auto Opening25d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Neighborhood") {
			attribute->SetAttrModel(QStringList(d->paramNeighborhood.values()));
		} else if (id == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 50 }, { "Step", 1 } });
		} else if (id == "Border Policy") {
			attribute->SetAttrModel(QStringList(d->paramBorderpolicy.values()));
		}
	}

	auto Opening25d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto Opening25d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto Opening25d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto Opening25d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto Opening25d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/morphop/TC.Algorithm.MorphOp.Opening.2.5D.dll";
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		float offset { 0 };
		if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			offset = mask->GetZOffset();
			timestep = mask->GetTimeStep();
		} else
			return {};

		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr)
			return {};

		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		const auto neighborhood = d->attrMap["Neighborhood"]->GetAttrValue().toString();
		const auto borderPolicy = d->attrMap["Border Policy"]->GetAttrValue().toString();

		auto params = algorithm->Parameter();
		params->SetValue("KernelRadius", kernelRadius);
		params->SetValue("Neighborhood", d->paramNeighborhood.key(neighborhood));
		params->SetValue("BorderPolicy", d->paramBorderpolicy.key(borderPolicy));

		algorithm->SetInput(0, inputMask);
		if (!algorithm->Execute())
			return {};

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetOffset(offset);
		resultMask->SetTimeStep(timestep);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask3D>(resultMask);
		if (result == nullptr)
			return {};

		return { { "OutputMask", result } };
	}

	auto Opening25d::Abort() -> void { }
}
