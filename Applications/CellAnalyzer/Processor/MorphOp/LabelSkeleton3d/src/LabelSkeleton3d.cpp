#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ImageDev/Data/NativeMeasurements.h>
#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "LabelSkeleton3d.h"


namespace CellAnalyzer::Processor::MorphOp {
	using namespace imagedev;
	using namespace iolink;

	struct LabelSkeleton3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	LabelSkeleton3d::LabelSkeleton3d() : d { std::make_unique<Impl>() } { }

	LabelSkeleton3d::~LabelSkeleton3d() { }

	auto LabelSkeleton3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
	}

	auto LabelSkeleton3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto LabelSkeleton3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto LabelSkeleton3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto LabelSkeleton3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto LabelSkeleton3d::Process() -> DataMap {
		std::shared_ptr<ImageView> labelView { nullptr };
		TCDataConverter converter;
		int timestep { 0 };
		int dimX, dimY, dimZ;
		double resX, resY, resZ;
		int max_index;
		float offset { 0 };
		QVector<int> empty_label;
		if (const auto label = std::dynamic_pointer_cast<Data::LabelMask3D>(d->inputMap["InputMask"])) {
			timestep = label->GetTimeStep();
			dimX = label->GetSize().x;
			dimY = label->GetSize().y;
			dimZ = label->GetSize().z;
			resX = label->GetResolution().x;
			resY = label->GetResolution().y;
			resZ = label->GetResolution().z;
			offset = label->GetZOffset();
			max_index = label->GetMaxIndex();
			const auto labelMask = Data::DataConverter::ConvertToTCMask(label);
			QMap<int, bool> label_existance;
			for (int i = 0; i < labelMask->GetBlobIndexes().count(); i++) {
				const auto bidx = labelMask->GetBlobIndexes()[i];
				const auto blob = labelMask->GetBlob(bidx);
				label_existance[blob.GetCode()] = true;
			}
			for (auto i = 1; i < max_index; i++) {
				if (false == label_existance.contains(i)) {
					empty_label.push_back(i);
				}
			}
			labelView = converter.MaskToImageView(labelMask);
		} else {
			return {};
		}
		
		try {
			const auto bin = thresholding(labelView, { 1,INT_MAX });
			const auto skeletoned = skeleton(bin);
			const auto masked = maskImage(labelView, skeletoned);
			int dim[3] = { dimX, dimY, dimZ };
			double res[3] = { resX, resY, resZ };
			auto result = converter.ArrToLabelMask(static_cast<unsigned short*>(masked->buffer()), dim, res);
			result->SetOffset(offset);
			result->SetTimeStep(timestep);
			const auto output = Data::DataConverter::ConvertToMaskData<Data::LabelMask3D>(result);
			return { { "OutputMask", output } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return {};
	}

	auto LabelSkeleton3d::Abort() -> void { }
}
