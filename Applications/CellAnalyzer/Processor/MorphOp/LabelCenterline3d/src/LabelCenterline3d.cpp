#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ImageDev/Data/NativeMeasurements.h>
#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "LabelCenterline3d.h"


namespace CellAnalyzer::Processor::MorphOp {
	using namespace imagedev;
	using namespace iolink;

	struct LabelCenterline3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	LabelCenterline3d::LabelCenterline3d() : d { std::make_unique<Impl>() } { }

	LabelCenterline3d::~LabelCenterline3d() { }

	auto LabelCenterline3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		if (attrID == "New branch sensibility") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1.0 }, { "Max", 10.0 }, { "Step", 0.01 }, { "Decimals", 2 } });
			attribute->SetAttrValue(3);
		}
	}

	auto LabelCenterline3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto LabelCenterline3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto LabelCenterline3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto LabelCenterline3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto LabelCenterline3d::Process() -> DataMap {
		std::shared_ptr<ImageView> labelView { nullptr };
		TCDataConverter converter;
		int timestep { 0 };
		int dimX, dimY, dimZ;
		double resX, resY, resZ;
		int max_index;
		float offset { 0 };
		QVector<int> empty_label;
		if (const auto label = std::dynamic_pointer_cast<Data::LabelMask3D>(d->inputMap["InputMask"])) {
			timestep = label->GetTimeStep();
			dimX = label->GetSize().x;
			dimY = label->GetSize().y;
			dimZ = label->GetSize().z;
			resX = label->GetResolution().x;
			resY = label->GetResolution().y;
			resZ = label->GetResolution().z;
			offset = label->GetZOffset();
			max_index = label->GetMaxIndex();
			const auto labelMask = Data::DataConverter::ConvertToTCMask(label);
			QMap<int, bool> label_existance;
			for (int i = 0; i < labelMask->GetBlobIndexes().count(); i++) {
				const auto bidx = labelMask->GetBlobIndexes()[i];
				const auto blob = labelMask->GetBlob(bidx);
				label_existance[blob.GetCode()] = true;
			}
			for (auto i = 1; i < max_index; i++) {
				if (false == label_existance.contains(i)) {
					empty_label.push_back(i);
				}
			}
			labelView = converter.MaskToImageView(labelMask);
		} else {
			return {};
		}

		const auto sensibility = d->attrMap["New branch sensibility"]->GetAttrValue().toDouble();

		try {
			const auto skeletoned = centerline3d(labelView, true, 3, sensibility, 0.3, 3);
			int dim[3] = { dimX, dimY, dimZ };
			double res[3] = { resX, resY, resZ };
			auto result = converter.ArrToLabelMask(static_cast<unsigned short*>(skeletoned.outputObjectImage->buffer()), dim, res);
			result->SetOffset(offset);
			result->SetTimeStep(timestep);
			const auto output = Data::DataConverter::ConvertToMaskData<Data::LabelMask3D>(result);
			return { { "OutputMask", output } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return {};
	}

	auto LabelCenterline3d::Abort() -> void { }
}
