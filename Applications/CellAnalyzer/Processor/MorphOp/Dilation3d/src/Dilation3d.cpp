#include "Dilation3d.h"
#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <QCoreApplication>
#include <TCMask.h>

namespace CellAnalyzer::Processor::MophologicalOperation {
	struct Dilation3d::Impl {
		const QMap<int, QString> paramNeighborhood {
			{ 0, "6-neighborhood" },
			{ 1, "18-neighborhood" },
			{ 2, "26-neighborhood" }
		};

		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	Dilation3d::Dilation3d() : d { std::make_unique<Impl>() } { }

	Dilation3d::~Dilation3d() { }

	auto Dilation3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Neighborhood") {
			attribute->SetAttrModel(QStringList(d->paramNeighborhood.values()));
		} else if (id == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 50 }, { "Step", 1 } });
		}
	}

	auto Dilation3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto Dilation3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto Dilation3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto Dilation3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto Dilation3d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/morphop/TC.Algorithm.MorphOp.Dilation.3D.dll";
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		float offset { 0 };
		if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			timestep = mask->GetTimeStep();
			offset = mask->GetZOffset();
		} else {
			return {};
		}

		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		const auto neighborhood = d->attrMap["Neighborhood"]->GetAttrValue().toString();

		auto params = algorithm->Parameter();
		params->SetValue("KernelRadius", kernelRadius);
		params->SetValue("Neighborhood", d->paramNeighborhood.key(neighborhood));

		algorithm->SetInput(0, inputMask);
		if (!algorithm->Execute()) {
			return {};
		}

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetTimeStep(timestep);
		resultMask->SetOffset(offset);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask3D>(resultMask);
		if (result == nullptr) {
			return {};
		}

		return { { "OutputMask", result } };
	}

	auto Dilation3d::Abort() -> void { }
}
