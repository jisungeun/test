#include "BallClosing3d.h"
#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <QCoreApplication>
#include <TCMask.h>
#include <ImageDev/ImageDev.h>
#include <iolink/view/ImageViewFactory.h>
#include <TCDataConverter.h>

namespace CellAnalyzer::Processor::MophologicalOperation {
	using namespace imagedev;
	using namespace iolink;
	struct BallClosing3d::Impl {		
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	BallClosing3d::BallClosing3d() : d { std::make_unique<Impl>() } { }

	BallClosing3d::~BallClosing3d() { }

	auto BallClosing3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Precision") {
			attribute->SetAttrModel(QStringList({ "Faster","Precise" }));
		} else if (id == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 100 }, { "Step", 1 } });
			attribute->SetAttrValue(3);
		} 
	}

	auto BallClosing3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto BallClosing3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto BallClosing3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto BallClosing3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto BallClosing3d::Process() -> DataMap {
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		float offset { 0 };
		std::shared_ptr<ImageView> inputBinary{ nullptr };
		TCDataConverter converter;
		int dim[3];
		double res[3];
		if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			timestep = mask->GetTimeStep();
			offset = mask->GetZOffset();
			const auto tcmask = Data::DataConverter::ConvertToTCMask(mask);
			inputBinary = converter.MaskToImageView(tcmask);
			dim[0] = mask->GetSize().x;
			dim[1] = mask->GetSize().y;
			dim[2] = mask->GetSize().z;
			res[0] = mask->GetResolution().x;
			res[1] = mask->GetResolution().y;
			res[2] = mask->GetResolution().z;
		} else {
			return {};
		}
		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		const auto precision = d->attrMap["Precision"]->GetAttrValue().toString();
		ClosingBallByReconstruction3d::Precision p;
		if(precision == "Faster") {
			p = ClosingBallByReconstruction3d::Precision::FASTER;
		}else {
			p = ClosingBallByReconstruction3d::Precision::PRECISE;
		}
		try {
			auto converted = convertImage(inputBinary, ConvertImage::BINARY);

			auto closed = closingBallByReconstruction3d(converted, kernelRadius, p);

			auto resultIv = convertImage(closed, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(resultIv->buffer()), dim, res);
			resultMask->SetOffset(offset);
			resultMask->SetTimeStep(timestep);
			DataPtr result{ nullptr };
			result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask3D>(resultMask);
			if(result == nullptr) {
				return {};
			}
			return { {"OutputMask",result} };
		}catch(Exception& e) {
			std::cout << e.what() << std::endl;
		}

		return {};
	}

	auto BallClosing3d::Abort() -> void { }
}
