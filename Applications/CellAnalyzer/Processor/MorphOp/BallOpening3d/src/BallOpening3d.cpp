#include "BallOpening3d.h"
#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <QCoreApplication>
#include <TCMask.h>
#include <ImageDev/ImageDev.h>
#include <iolink/view/ImageViewFactory.h>
#include <TCDataConverter.h>

namespace CellAnalyzer::Processor::MophologicalOperation {
	using namespace imagedev;
	using namespace iolink;
	struct BallOpening3d::Impl {
		const QMap<int, QString> paramNeighborhood {
			{ 0, "6-neighborhood" },
			{ 1, "18-neighborhood" },
			{ 2, "26-neighborhood" }
		};
		const QMap<int, QString> paramBorderpolicy {
			{ 0, "Ignore" },
			{ 1, "Extended" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	BallOpening3d::BallOpening3d() : d { std::make_unique<Impl>() } { }

	BallOpening3d::~BallOpening3d() { }

	auto BallOpening3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Precision") {
			attribute->SetAttrModel(QStringList({ "Faster", "Precise" }));
		}
		else if (id == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap{ { "Min", 1 }, { "Max", 100 }, { "Step", 1 } });
			attribute->SetAttrValue(3);
		}
	}

	auto BallOpening3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto BallOpening3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto BallOpening3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto BallOpening3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto BallOpening3d::Process() -> DataMap {
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		float offset { 0 };
		std::shared_ptr<ImageView> inputBinary{ nullptr };
		TCDataConverter converter;
		int dim[3];
		double res[3];
		if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			timestep = mask->GetTimeStep();
			offset = mask->GetZOffset();
			const auto tcmask = Data::DataConverter::ConvertToTCMask(mask);
			inputBinary = converter.MaskToImageView(tcmask);
			dim[0] = mask->GetSize().x;
			dim[1] = mask->GetSize().y;
			dim[2] = mask->GetSize().z;
			res[0] = mask->GetResolution().x;
			res[1] = mask->GetResolution().y;
			res[2] = mask->GetResolution().z;
		} else {
			return {};
		}

		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		const auto precision = d->attrMap["Precision"]->GetAttrValue().toString();
		OpeningBallByReconstruction3d::Precision p;
		if (precision == "Faster") {
			p = OpeningBallByReconstruction3d::Precision::FASTER;
		}
		else {
			p = OpeningBallByReconstruction3d::Precision::PRECISE;
		}
		try {
			auto converted = convertImage(inputBinary, ConvertImage::BINARY);

			auto opened = openingBallByReconstruction3d(converted, kernelRadius, p);

			auto resultIv = convertImage(opened, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(resultIv->buffer()), dim, res);
			resultMask->SetOffset(offset);
			resultMask->SetTimeStep(timestep);
			DataPtr result{ nullptr };
			result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask3D>(resultMask);
			if (result == nullptr) {
				return {};
			}
			return { {"OutputMask",result} };
		}
		catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}

		return {};
	}

	auto BallOpening3d::Abort() -> void { }
}
