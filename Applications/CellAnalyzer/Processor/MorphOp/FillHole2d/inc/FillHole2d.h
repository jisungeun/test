#pragma once

#include <IProcessor.h>

namespace CellAnalyzer::Processor::MophologicalOperation {
	class FillHole2d final : public IProcessor {
	public:
		FillHole2d();
		~FillHole2d() override;

		auto SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void override;
		auto SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void override;
		auto SetInputData(const QString& inputID, const DataPtr& data) -> void override;

		auto GetAttr(const QString& attrID) const -> ProcessorAttrPtr override;
		auto GetInputData(const QString& inputID) const -> DataPtr override;

		auto Process() -> DataMap override;
		auto Abort() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	DECLARE_PROCESSOR_START(FillHole2d) {
		Q_OBJECT
		DECLARE_PROCESSOR_END(FillHole2d, MophologicalOperation, DEFAULT_PATH)
	};
}
