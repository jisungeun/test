#include "GrayClosing3d.h"
#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <QCoreApplication>
#include <TCMask.h>
#include <ImageDev/ImageDev.h>
#include <iolink/view/ImageViewFactory.h>
#include <TCDataConverter.h>

namespace CellAnalyzer::Processor::MophologicalOperation {
	using namespace imagedev;
	using namespace iolink;
	struct GrayClosing3d::Impl {		
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	GrayClosing3d::GrayClosing3d() : d { std::make_unique<Impl>() } { }

	GrayClosing3d::~GrayClosing3d() { }

	auto GrayClosing3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Precision") {
			attribute->SetAttrModel(QStringList({ "Faster","Precise" }));
		} else if (id == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 100 }, { "Step", 1 } });
			attribute->SetAttrValue(3);
		} 
	}

	auto GrayClosing3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto GrayClosing3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto GrayClosing3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto GrayClosing3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto GrayClosing3d::Process() -> DataMap {
		TCImage::Pointer inputImage{ nullptr };
		std::shared_ptr<ImageView> refImageView{ nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		TCDataConverter converter;
		auto isHT = false;
		auto isFL = false;
		auto isFloat = false;
		int timestep{ 0 };
		float offset{ 0 };
		int chIdx{ 0 };
		QString chName;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			isHT = true;
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		}
		else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			isFL = true;
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			chIdx = image->GetChannelIndex();
			chName = image->GetChannelName();
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		}
		else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			isFloat = true;
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			timestep = image->GetTimeStep();
		}
		else
			return {};

		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		const auto precision = d->attrMap["Precision"]->GetAttrValue().toString();
		ClosingBallByReconstruction3d::Precision p;
		if(precision == "Faster") {
			p = ClosingBallByReconstruction3d::Precision::FASTER;
		}else {
			p = ClosingBallByReconstruction3d::Precision::PRECISE;
		}
		try {
			auto destImage = closingBallByReconstruction3d(refImageView, kernelRadius, p);

			const auto resultStat = intensityStatistics(destImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result{ nullptr };
			if (isHT) {
				result = Data::DataConverter::ConvertToHTData<Data::HT3D>(static_cast<uint16_t*>(destImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			}
			if (isFL) {
				result = Data::DataConverter::ConvertToFLData<Data::FL3D>(static_cast<uint16_t*>(destImage->buffer()), chIdx, chName, dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			}
			if (isFloat) {
				result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(destImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			}
			return { { "OutputImage", result } };
		}catch(Exception& e) {
			std::cout << e.what() << std::endl;
		}

		return {};
	}

	auto GrayClosing3d::Abort() -> void { }
}
