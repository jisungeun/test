#include "GrayErosion2d.h"
#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <QCoreApplication>
#include <TCMask.h>

#include <ImageDev/ImageDev.h>
#include <iolink/view/ImageViewFactory.h>
#include "TCDataConverter.h"

namespace CellAnalyzer::Processor::MophologicalOperation {
	using namespace imagedev;
	using namespace iolink;

	struct GrayErosion2d::Impl {
		const QMap<int, QString> paramNeighborhood {
			{ 0, "4-neighborhood" },
			{ 1, "8-neighborhood" }
		};

		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	GrayErosion2d::GrayErosion2d() : d { std::make_unique<Impl>() } { }

	GrayErosion2d::~GrayErosion2d() { }

	auto GrayErosion2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Neighborhood") {
			attribute->SetAttrModel(QStringList(d->paramNeighborhood.values()));
		} else if (id == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 50 }, { "Step", 1 } });
		}
	}

	auto GrayErosion2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto GrayErosion2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto GrayErosion2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto GrayErosion2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto GrayErosion2d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		TCDataConverter converter;
		auto isHT = false;
		auto isFL = false;
		auto isFloat = false;
		int timestep { 0 };
		int chIdx { 0 };
		QString chName;
		if (const auto image = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			isHT = true;
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			isFL = true;
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
			chIdx = image->GetChannelIndex();
			chName = image->GetChannelName();
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage"])) {
			isFloat = true;
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			timestep = image->GetTimeStep();
		} else {
			return {};
		}

		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		const auto neighborhood = d->attrMap["Neighborhood"]->GetAttrValue().toString();
		const auto nei = static_cast<Erosion2d::Neighborhood>(d->paramNeighborhood.key(neighborhood));

		try {
			const auto destImage = erosion2d(refImageView, kernelRadius, nei);

			const auto resultStat = intensityStatistics(destImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result { nullptr };
			if (isHT) {
				result = Data::DataConverter::ConvertToHTData<Data::HT2D>(static_cast<uint16_t*>(destImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			}
			if (isFL) {
				result = Data::DataConverter::ConvertToFLData<Data::FL2D>(static_cast<uint16_t*>(destImage->buffer()), chIdx, chName, dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			}
			if (isFloat) {
				result = Data::DataConverter::ConvertToFloatData<Data::Float2D>(static_cast<float*>(destImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto GrayErosion2d::Abort() -> void { }
}
