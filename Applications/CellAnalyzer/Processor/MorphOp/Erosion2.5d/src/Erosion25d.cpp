#include "Erosion25d.h"
#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <QCoreApplication>
#include <TCMask.h>

namespace CellAnalyzer::Processor::MophologicalOperation {
	struct Erosion25d::Impl {
		const QMap<int, QString> paramNeighborhood {
			{ 0, "4-neighborhood" },
			{ 1, "8-neighborhood" }
		};

		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	Erosion25d::Erosion25d() : d { std::make_unique<Impl>() } { }

	Erosion25d::~Erosion25d() { }

	auto Erosion25d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Neighborhood") {
			attribute->SetAttrModel(QStringList(d->paramNeighborhood.values()));
		} else if (id == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 50 }, { "Step", 1 } });
		}
	}

	auto Erosion25d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto Erosion25d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto Erosion25d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto Erosion25d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto Erosion25d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/morphop/TC.Algorithm.MorphOp.Erosion.2.5D.dll";
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		float offset { 0 };
		if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			timestep = mask->GetTimeStep();
			offset = mask->GetZOffset();
		} else
			return {};

		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr)
			return {};

		const auto neighborhood = d->attrMap["Neighborhood"]->GetAttrValue().toString();
		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();

		auto params = algorithm->Parameter();
		params->SetValue("KernelRadius", kernelRadius);
		params->SetValue("Neighborhood", d->paramNeighborhood.key(neighborhood));

		algorithm->SetInput(0, inputMask);
		if (!algorithm->Execute())
			return {};

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetOffset(offset);
		resultMask->SetTimeStep(timestep);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask3D>(resultMask);
		if (result == nullptr)
			return {};

		return { { "OutputMask", result } };
	}

	auto Erosion25d::Abort() -> void { }
}
