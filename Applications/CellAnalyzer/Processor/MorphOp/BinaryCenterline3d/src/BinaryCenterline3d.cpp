#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ImageDev/Data/NativeMeasurements.h>
#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "BinaryCenterline3d.h"


namespace CellAnalyzer::Processor::MorphOp {
	using namespace imagedev;
	using namespace iolink;

	struct BinaryCenterline3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	BinaryCenterline3d::BinaryCenterline3d() : d { std::make_unique<Impl>() } { }

	BinaryCenterline3d::~BinaryCenterline3d() { }

	auto BinaryCenterline3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		if (attrID == "New branch sensibility") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1.0 }, { "Max", 10.0 }, { "Step", 0.01 }, { "Decimals", 2 } });
			attribute->SetAttrValue(3);
		}
	}

	auto BinaryCenterline3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto BinaryCenterline3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto BinaryCenterline3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto BinaryCenterline3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto BinaryCenterline3d::Process() -> DataMap {
		std::shared_ptr<ImageView> labelView { nullptr };
		TCDataConverter converter;
		int timestep { 0 };
		int dim[3];
		double res[3];
		float offset { 0 };
		QVector<int> empty_label;
		if (const auto label = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["InputMask"])) {
			timestep = label->GetTimeStep();
			dim[0] = label->GetSize().x;
			dim[1] = label->GetSize().y;
			dim[2] = label->GetSize().z;
			res[0] = label->GetResolution().x;
			res[1] = label->GetResolution().y;
			res[2] = label->GetResolution().z;
			offset = label->GetZOffset();
			const auto labelMask = Data::DataConverter::ConvertToTCMask(label);

			labelView = converter.MaskToImageView(labelMask, TCDataConverter::MaskType::Binary);
		} else {
			return {};
		}

		const auto sensibility = d->attrMap["New branch sensibility"]->GetAttrValue().toDouble();

		try {
			const auto binInput = convertImage(labelView, ConvertImage::BINARY);
			const auto skeletoned = centerline3d(binInput, true, 3, sensibility, 0.3, 0);

			auto resultIv = convertImage(skeletoned.outputObjectImage, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(resultIv->buffer()), dim, res);
			resultMask->SetOffset(offset);
			resultMask->SetTimeStep(timestep);
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask3D>(resultMask);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputMask", result } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return {};
	}

	auto BinaryCenterline3d::Abort() -> void { }
}
