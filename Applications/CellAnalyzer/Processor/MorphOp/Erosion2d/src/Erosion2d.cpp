#include "Erosion2d.h"
#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <QCoreApplication>
#include <TCMask.h>

namespace CellAnalyzer::Processor::MophologicalOperation {
	struct Erosion2d::Impl {
		const QMap<int, QString> paramNeighborhood {
			{ 0, "4-neighborhood" },
			{ 1, "8-neighborhood" }
		};

		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	Erosion2d::Erosion2d() : d { std::make_unique<Impl>() } { }

	Erosion2d::~Erosion2d() { }

	auto Erosion2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Neighborhood") {
			attribute->SetAttrModel(QStringList(d->paramNeighborhood.values()));
		} else if (id == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 50 }, { "Step", 1 } });
		}
	}

	auto Erosion2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto Erosion2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto Erosion2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto Erosion2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto Erosion2d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/morphop/TC.Algorithm.MorphOp.Erosion.2D.dll";
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			timestep = mask->GetTimeStep();
		} else {
			return {};
		}

		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		const auto neighborhood = d->attrMap["Neighborhood"]->GetAttrValue().toString();

		auto params = algorithm->Parameter();
		params->SetValue("KernelRadius", kernelRadius);
		params->SetValue("Neighborhood", d->paramNeighborhood.key(neighborhood));

		algorithm->SetInput(0, inputMask);
		if (!algorithm->Execute()) {
			return {};
		}

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetTimeStep(timestep);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask2D>(resultMask);
		if (result == nullptr) {
			return {};
		}

		return { { "OutputMask", result } };
	}

	auto Erosion2d::Abort() -> void { }
}
