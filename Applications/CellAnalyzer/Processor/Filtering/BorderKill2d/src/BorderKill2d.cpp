#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCMask.h>

#include "BorderKill2d.h"

namespace CellAnalyzer::Processor::Filtering {
	struct BorderKill2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	BorderKill2d::BorderKill2d() : d { std::make_unique<Impl>() } { }

	BorderKill2d::~BorderKill2d() { }

	auto BorderKill2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void { }

	auto BorderKill2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto BorderKill2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto BorderKill2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto BorderKill2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto BorderKill2d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/filtering/TC.Algorithm.Filtering.BorderKill.dll";

		// create an input data
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		if (const auto data = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["InputMask"])) {
			timestep = data->GetTimeStep();
			inputMask = Data::DataConverter::ConvertToTCMask(data);
		} else {
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		// set input data
		algorithm->SetInput(0, inputMask);
		if (!algorithm->Execute()) {
			return {};
		}

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetTimeStep(timestep);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask2D>(resultMask);
		if (result == nullptr) {
			return {};
		}

		return { { "OutputMask", result } };
	}

	auto BorderKill2d::Abort() -> void { }
}
