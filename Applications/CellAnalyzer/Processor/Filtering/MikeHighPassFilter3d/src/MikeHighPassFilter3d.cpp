#include <QCoreApplication>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include "TCDataConverter.h"

#include "MikeHighPassFilter3d.h"

#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <vtkImageFFT.h>
#include <vtkImageRFFT.h>
#include <vtkImageMathematics.h>
#include <vtkImageMagnitude.h>
#include <vtkImageExtractComponents.h>

namespace CellAnalyzer::Processor::Filtering {
	using ImageViewPtr = std::shared_ptr<iolink::ImageView>;
	using namespace imagedev;
	using namespace iolink;
	struct MikeHighPassFilter3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;

		//VTK Implementation
		auto SPi2RI(vtkSmartPointer<vtkImageData> SPi, double n_m)->vtkSmartPointer<vtkImageData>;
		auto RI2SPi(vtkSmartPointer<vtkImageData> RI, double n_m)->vtkSmartPointer<vtkImageData>;
		auto MulImg(vtkSmartPointer<vtkImageData> RI)->vtkSmartPointer<vtkImageData>;

		auto sfcFilter_Generate_SphericalLinear(
			int outputSize_tomo[3], double kResXYZ[3], double featureSizeMicron)->vtkSmartPointer<vtkImageData>;
		
		auto sfcFilter_Apply_SphericalLinear(
			vtkSmartPointer<vtkImageData> riDecon, double kResXYZ[3], double featureSizeMicron, double n_m)->vtkSmartPointer<vtkImageData>;		
		auto ifftshift(vtkImageData* inputImage)->vtkSmartPointer<vtkImageData>;		
		auto vtkToImageDev(vtkSmartPointer<vtkImageData> image)->ImageViewPtr;

		//ImageDev Implementation
		auto SPi2RI(ImageViewPtr Spi_real, ImageViewPtr Spi_imag, double n_m)->ImageViewPtr;
		auto RI2SPi(ImageViewPtr RI, double n_m)->std::tuple<ImageViewPtr, ImageViewPtr>;
		auto sfcFilter_Generate_SphericalLinear2(
			int outputSize_tomo[3], double kResXYZ[3], double featureSizeMicron)->ImageViewPtr;
		auto sfcFilter_Apply_SphericalLinear(
			ImageViewPtr riDecon, double kResXYZ[3], double featureSizeMicron, double n_m)->ImageViewPtr;
		auto ifftshift(ImageViewPtr inputImage)->ImageViewPtr;
	};

	auto MikeHighPassFilter3d::Impl::vtkToImageDev(vtkSmartPointer<vtkImageData> image) -> ImageViewPtr {
		int dimX = image->GetDimensions()[0];
		int dimY = image->GetDimensions()[1];
		int dimZ = image->GetDimensions()[2];
		ImageViewPtr iv;

		VectorXu64 imageShape{ static_cast<uint64_t>(dimX), static_cast<uint64_t>(dimY),static_cast<uint64_t>(dimZ) };

		iv = ImageViewFactory::allocate(imageShape, DataTypeId::FLOAT);

		setDimensionalInterpretation(iv, ImageTypeId::VOLUME);
		setImageInterpretation(iv, ImageInterpretation::GRAYSCALE);

		VectorXu64 imageOrig{ 0, 0 ,0 };
		RegionXu64 imageRegion{ imageOrig, imageShape };

		auto result_arr = std::shared_ptr<float[]>(new float[dimX * dimY * dimZ](), std::default_delete<float[]>());

		for (int k = 0; k < dimZ; k++) {
			for (int j = 0; j < dimY; j++) {
				for (int i = 0; i < dimX; i++) {
					float value = image->GetScalarComponentAsFloat(i, j, k, 0);
					*(result_arr.get() + k * dimY * dimX + j * dimX + i) = value;
				}
			}
		}

		iv->writeRegion(imageRegion, result_arr.get());

		return iv;
	}
	

	auto MikeHighPassFilter3d::Impl::ifftshift(vtkImageData* inputImage) -> vtkSmartPointer<vtkImageData> {		
		vtkSmartPointer<vtkImageData> resultImage = vtkSmartPointer<vtkImageData>::New();
		resultImage->SetDimensions(inputImage->GetDimensions());
		resultImage->AllocateScalars(VTK_DOUBLE, 1);

		double* resultData = static_cast<double*>(resultImage->GetScalarPointer());
		double* refData = static_cast<double*>(inputImage->GetScalarPointer());

		int sizeX = inputImage->GetDimensions()[0];
		int sizeY = inputImage->GetDimensions()[1];
		int sizeZ = inputImage->GetDimensions()[2];
		int offsetX = sizeX / 2;
		int offsetY = sizeY / 2;
		int offsetZ = sizeZ / 2;
		for (int i = 0; i < sizeX; ++i) {
			for (int j = 0; j < sizeY; ++j) {
				for (int k = 0; k < sizeZ; ++k) {
					int ni = (i + offsetX) % sizeX;
					int nj = (j + offsetY) % sizeY;
					int nk = (k + offsetZ) % sizeZ;

					resultData[i + sizeX * (j + sizeY * k)] = refData[ni + sizeX * (nj + sizeY * nk)];
				}
			}
		}

		return resultImage;
	}
	
	auto MikeHighPassFilter3d::Impl::sfcFilter_Apply_SphericalLinear(vtkSmartPointer<vtkImageData> riDecon, double kResXYZ[3], double featureSizeMicron, double n_m) -> vtkSmartPointer<vtkImageData> {
		int dims[3];
		riDecon->GetDimensions(dims);		
		
		vtkSmartPointer<vtkImageData> sfcFilter = sfcFilter_Generate_SphericalLinear(dims, kResXYZ, featureSizeMicron);

		vtkSmartPointer<vtkImageData> zeroPad = vtkSmartPointer<vtkImageData>::New();
		zeroPad->SetDimensions(riDecon->GetDimensions());
		zeroPad->AllocateScalars(VTK_DOUBLE, 2);

		double* zeroImagData = static_cast<double*>(zeroPad->GetScalarPointer());
		double* originalImage = static_cast<double*>(sfcFilter->GetScalarPointer());
		for (int i = 0; i < riDecon->GetNumberOfPoints() * 2; i += 2) {
			zeroImagData[i] = originalImage[i / 2];           // Real part
			zeroImagData[i + 1] = originalImage[i / 2];       // Imaginary part
		}

		const auto ri2spi = RI2SPi(riDecon, n_m);
		
		vtkSmartPointer<vtkImageMathematics> multiply = vtkSmartPointer<vtkImageMathematics>::New();
		multiply->SetOperationToMultiply();
		multiply->SetInput1Data(ri2spi);
		multiply->SetInput2Data(zeroPad);
		multiply->Update();

		vtkSmartPointer<vtkImageData> result = SPi2RI(multiply->GetOutput(), n_m);
		
		return result;
	}
	
	auto MikeHighPassFilter3d::Impl::sfcFilter_Generate_SphericalLinear(int outputSize_tomo[3], double kResXYZ[3], double featureSizeMicron) -> vtkSmartPointer<vtkImageData> {		
		vtkSmartPointer<vtkImageData> sfcFilter = vtkSmartPointer<vtkImageData>::New();
		sfcFilter->SetDimensions(outputSize_tomo[0], outputSize_tomo[1], outputSize_tomo[2]);
		sfcFilter->AllocateScalars(VTK_DOUBLE, 1);

		double* sfcFilterData = static_cast<double*>(sfcFilter->GetScalarPointer(0, 0, 0));

		for (int i = 0; i < outputSize_tomo[0]; ++i) {
			for (int j = 0; j < outputSize_tomo[1]; ++j) {
				for (int k = 0; k < outputSize_tomo[2]; ++k) {
					double meshGridRange01 = featureSizeMicron * (-outputSize_tomo[0] / 2 + i) * kResXYZ[1];
					double meshGridRange02 = featureSizeMicron * (-outputSize_tomo[1] / 2 + j) * kResXYZ[0];
					double meshGridRange03 = featureSizeMicron * (-outputSize_tomo[2] / 2 + k) * kResXYZ[2];

					double kCo01 = meshGridRange01;
					double kCo02 = meshGridRange02;
					double kCo03 = meshGridRange03;

					double sfcValue = sqrt(kCo01 * kCo01 + kCo02 * kCo02 + kCo03 * kCo03);
					sfcValue = (sfcValue > 1.0) ? 1.0 : sfcValue;

					sfcFilterData[i + outputSize_tomo[0] * (j + outputSize_tomo[1] * k)] = sfcValue;
				}
			}
		}
		//vtkSmartPointer<vtkImageData> result = vtkSmartPointer<vtkImageData>::New();
		//result->DeepCopy(ifftshift(sfcFilter));
		
		//return result;
		return sfcFilter;
	}
		
	auto MikeHighPassFilter3d::Impl::MulImg(vtkSmartPointer<vtkImageData> RI) -> vtkSmartPointer<vtkImageData> {
		// Create a constant image with the imaginary unit
		vtkSmartPointer<vtkImageData> constantImage = vtkSmartPointer<vtkImageData>::New();
		constantImage->SetDimensions(RI->GetDimensions());
		constantImage->AllocateScalars(VTK_DOUBLE, 2); // Complex data type (real + imaginary)
		double* constantImageData = static_cast<double*>(constantImage->GetScalarPointer());
		for (int i = 0; i < RI->GetNumberOfPoints() * 2; i += 2) {
			constantImageData[i] = 0.0;           // Real part
			constantImageData[i + 1] = 1.0;       // Imaginary part
		}

		vtkSmartPointer<vtkImageData> zeroImag = vtkSmartPointer<vtkImageData>::New();
		zeroImag->SetDimensions(RI->GetDimensions());
		zeroImag->AllocateScalars(VTK_DOUBLE, 2);

		double* zeroImagData = static_cast<double*>(zeroImag->GetScalarPointer());
		float* originalImage = static_cast<float*>(RI->GetScalarPointer());
		for (int i = 0; i < RI->GetNumberOfPoints() * 2; i += 2) {
			zeroImagData[i] = 0;           // Real part
			zeroImagData[i + 1] = originalImage[i / 2];// Imaginary
		}


		// Multiply the input image by the imaginary unit
		vtkSmartPointer<vtkImageMathematics> multFilter = vtkSmartPointer<vtkImageMathematics>::New();
		multFilter->SetOperationToMultiply();
		multFilter->SetInput1Data(zeroImag);
		multFilter->SetInput2Data(constantImage);
		multFilter->Update();

		vtkSmartPointer<vtkImageData> multiplyImg = vtkSmartPointer<vtkImageData>::New();
		multiplyImg->ShallowCopy(multFilter->GetOutput());
		
		return multiplyImg;
	}

	auto MikeHighPassFilter3d::Impl::RI2SPi(vtkSmartPointer<vtkImageData> RI, double n_m) -> vtkSmartPointer<vtkImageData> {
		// Calculate (RI)^2
		vtkSmartPointer<vtkImageMathematics> squareFilter = vtkSmartPointer<vtkImageMathematics>::New();
		squareFilter->SetOperationToMultiply();		
		squareFilter->SetInput1Data(RI);
		squareFilter->SetInput2Data(RI);
		squareFilter->Update();
		
		// Calculate (RI/n_m)^2
		vtkSmartPointer<vtkImageMathematics> divFilter = vtkSmartPointer<vtkImageMathematics>::New();
		divFilter->SetOperationToMultiplyByK();
		divFilter->SetConstantK(1 / (n_m * n_m));
		divFilter->SetInput1Data(squareFilter->GetOutput());
		divFilter->Update();

		// Calculate (RI/n_m)^2 - 1
		vtkSmartPointer<vtkImageMathematics> subtractFilter = vtkSmartPointer<vtkImageMathematics>::New();
		subtractFilter->SetOperationToAddConstant();
		subtractFilter->SetConstantC(-1.0);
		subtractFilter->SetInput1Data(divFilter->GetOutput());
		subtractFilter->Update();

		// Calculate 1-(RI/n_m)^2
		vtkSmartPointer<vtkImageMathematics> invertFilter = vtkSmartPointer<vtkImageMathematics>::New();
		invertFilter->SetOperationToMultiplyByK();
		invertFilter->SetConstantK(-1.0);
		invertFilter->SetInput1Data(subtractFilter->GetOutput());
		invertFilter->Update();
					
		// Multiply by 1i (imaginary unit)
		vtkSmartPointer<vtkImageData> mulImag = MulImg(invertFilter->GetOutput());
		
		// Perform FFT
		vtkSmartPointer<vtkImageFFT> fftFilter = vtkSmartPointer<vtkImageFFT>::New();
		fftFilter->SetInputData(mulImag);		
		fftFilter->Update();

		vtkSmartPointer<vtkImageData> Spi = vtkSmartPointer<vtkImageData>::New();
		Spi->ShallowCopy(fftFilter->GetOutput());

		return Spi;
	}	
	auto MikeHighPassFilter3d::Impl::SPi2RI(vtkSmartPointer<vtkImageData> SPi, double n_m) -> vtkSmartPointer<vtkImageData> {
		//ifftn(SPi)
		vtkSmartPointer<vtkImageRFFT> ifftFilter = vtkSmartPointer<vtkImageRFFT>::New();
		ifftFilter->SetInputData(SPi);		
		ifftFilter->Update();
				
		vtkSmartPointer<vtkImageData> ifftOutput = vtkSmartPointer<vtkImageData>::New();
		ifftOutput->ShallowCopy(ifftFilter->GetOutput());

		//imag(ifftn(SPi))
		vtkSmartPointer<vtkImageExtractComponents> extractImaginaryFilter = vtkSmartPointer<vtkImageExtractComponents>::New();
		extractImaginaryFilter->SetInputData(ifftOutput);
		extractImaginaryFilter->SetComponents(1); // 1 corresponds to the imaginary part
		extractImaginaryFilter->Update();

		//imag(ifftn(SPi) - 1
		vtkSmartPointer<vtkImageMathematics> minusFilter = vtkSmartPointer<vtkImageMathematics>::New();
		minusFilter->SetOperationToAddConstant();
		minusFilter->SetConstantK(-1);
		minusFilter->SetInputData(extractImaginaryFilter->GetOutput());
		minusFilter->Update();

		//1-imag(ifftn(SPi))
		vtkSmartPointer<vtkImageMathematics> multiFilter = vtkSmartPointer<vtkImageMathematics>::New();
		multiFilter->SetOperationToMultiplyByK();
		multiFilter->SetConstantK(-1);
		multiFilter->SetInputData(minusFilter->GetOutput());
		multiFilter->Update();

		//sqrt(1-imag(ifftn(SPi)))
		vtkSmartPointer<vtkImageMagnitude> magnitudeFilter = vtkSmartPointer<vtkImageMagnitude>::New();
		magnitudeFilter->SetInputData(multiFilter->GetOutput());
		magnitudeFilter->Update();				

		// Scale the magnitude by n_m
		vtkSmartPointer<vtkImageMathematics> scaleFilter = vtkSmartPointer<vtkImageMathematics>::New();
		scaleFilter->SetOperationToMultiplyByK();
		scaleFilter->SetConstantK(n_m);
		scaleFilter->SetInput1Data(magnitudeFilter->GetOutput());
		scaleFilter->Update();

		vtkSmartPointer<vtkImageData> RI = vtkSmartPointer<vtkImageData>::New();
		RI->ShallowCopy(scaleFilter->GetOutput());

		return RI;
	}

	auto MikeHighPassFilter3d::Impl::ifftshift(ImageViewPtr inputImage) -> ImageViewPtr {
		ImageViewPtr iv;

		VectorXu64 imageShape{ inputImage->shape() };
		auto sizeX = imageShape[0];
		auto sizeY = imageShape[1];
		auto sizeZ = imageShape[2];

		iv = ImageViewFactory::allocate(imageShape, DataTypeId::FLOAT);

		setDimensionalInterpretation(iv, ImageTypeId::VOLUME);
		setImageInterpretation(iv, ImageInterpretation::GRAYSCALE);

		VectorXu64 imageOrig{ 0, 0 ,0 };
		RegionXu64 imageRegion{ imageOrig, imageShape };

		auto resultData = std::shared_ptr<double[]>(new double[imageShape[0] * imageShape[1] * imageShape[2]](), std::default_delete<double[]>());
		double* refData = static_cast<double*>(inputImage->buffer());

		int offsetX = sizeX / 2;
		int offsetY = sizeY / 2;
		int offsetZ = sizeZ / 2;
		for (int i = 0; i < sizeX; ++i) {
			for (int j = 0; j < sizeY; ++j) {
				for (int k = 0; k < sizeZ; ++k) {
					int ni = (i + offsetX) % sizeX;
					int nj = (j + offsetY) % sizeY;
					int nk = (k + offsetZ) % sizeZ;

					*(resultData.get() + (i + sizeX * (j + sizeY * k))) = *(refData + (ni + sizeX * (nj + sizeY * nk)));
				}
			}
		}

		iv->writeRegion(imageRegion, resultData.get());

		return iv;

	}

	auto MikeHighPassFilter3d::Impl::sfcFilter_Apply_SphericalLinear(ImageViewPtr riDecon, double kResXYZ[3], double featureSizeMicron, double n_m) -> ImageViewPtr {
		int dims[3];
		const auto shape = riDecon->shape();
		for (auto i = 0; i < 3; i++) {
			dims[i] = shape[i];
		}
		const auto sfcFilter = sfcFilter_Generate_SphericalLinear2(dims, kResXYZ, featureSizeMicron);

		const auto [ri2spi_real, ri2spi_imag] = RI2SPi(riDecon, n_m);

		const auto filterStat = intensityStatistics(sfcFilter, IntensityStatistics::MIN_MAX, { 0,1 });

		const auto multi_real = arithmeticOperationWithImage(ri2spi_real, sfcFilter, ArithmeticOperationWithImage::MULTIPLY);
		const auto multi_imag = arithmeticOperationWithImage(ri2spi_imag, sfcFilter, ArithmeticOperationWithImage::MULTIPLY);

		const auto result = SPi2RI(multi_real, multi_imag, n_m);		

		const auto zFliped = flipImage3d(result, FlipImage3d::Z_AXIS);
		const auto xFliped = flipImage3d(zFliped, FlipImage3d::X_AXIS);
		const auto yFliped = flipImage3d(xFliped, FlipImage3d::Y_AXIS);

		return yFliped;
	}

	auto MikeHighPassFilter3d::Impl::sfcFilter_Generate_SphericalLinear2(int outputSize_tomo[3], double kResXYZ[3], double featureSizeMicron) -> ImageViewPtr {
		int dimX = outputSize_tomo[0];
		int dimY = outputSize_tomo[1];
		int dimZ = outputSize_tomo[2];
		ImageViewPtr iv;

		VectorXu64 imageShape{ static_cast<uint64_t>(dimX), static_cast<uint64_t>(dimY),static_cast<uint64_t>(dimZ) };

		iv = ImageViewFactory::allocate(imageShape, DataTypeId::DOUBLE);

		setDimensionalInterpretation(iv, ImageTypeId::VOLUME);
		setImageInterpretation(iv, ImageInterpretation::GRAYSCALE);

		VectorXu64 imageOrig{ 0, 0 ,0 };
		RegionXu64 imageRegion{ imageOrig, imageShape };

		auto sfcFilterData = std::shared_ptr<double[]>(new double[dimX * dimY * dimZ](), std::default_delete<double[]>());

		for (int i = 0; i < outputSize_tomo[0]; ++i) {
			for (int j = 0; j < outputSize_tomo[1]; ++j) {
				for (int k = 0; k < outputSize_tomo[2]; ++k) {
					double meshGridRange01 = featureSizeMicron * (-outputSize_tomo[0] / 2 + i) * kResXYZ[1];
					double meshGridRange02 = featureSizeMicron * (-outputSize_tomo[1] / 2 + j) * kResXYZ[0];
					double meshGridRange03 = featureSizeMicron * (-outputSize_tomo[2] / 2 + k) * kResXYZ[2];

					double kCo01 = meshGridRange01;
					double kCo02 = meshGridRange02;
					double kCo03 = meshGridRange03;

					double sfcValue = sqrt(kCo01 * kCo01 + kCo02 * kCo02 + kCo03 * kCo03);
					sfcValue = (sfcValue > 1.0) ? 1.0 : sfcValue;

					*(sfcFilterData.get() + (i + outputSize_tomo[0] * (j + outputSize_tomo[1] * k))) = sfcValue;
				}
			}
		}

		iv->writeRegion(imageRegion, sfcFilterData.get());
				
		return iv;
	}

	auto MikeHighPassFilter3d::Impl::RI2SPi(ImageViewPtr RI, double n_m) -> std::tuple<ImageViewPtr, ImageViewPtr> {
		// Calculate (RI)^2
		auto squared = square(RI);

		// Calculate (RI/n_m)^2
		auto divided = arithmeticOperationWithValue(squared, 1 / (n_m * n_m), ArithmeticOperationWithValue::MULTIPLY);

		// Calculate (RI/n_m)^2 - 1
		auto subtracted = arithmeticOperationWithValue(divided, 1, ArithmeticOperationWithValue::SUBTRACT);

		// Calculate 1-(RI/n_m)^2
		auto inverted = arithmeticOperationWithValue(subtracted, -1, ArithmeticOperationWithValue::MULTIPLY);

		// Perform FFT
		auto ffted = complexFft(inverted);

		// Multiply by 1i (imaginary unit) - imagenary and real part is inverted since multiplied by 1i
		return std::make_tuple(ffted.outputImaginaryImage, ffted.outputRealImage);
	}


	auto MikeHighPassFilter3d::Impl::SPi2RI(ImageViewPtr Spi_real, ImageViewPtr Spi_imag, double n_m) -> ImageViewPtr {
		//ifftn(Spi)		
		const auto ifft = complexFftInverse(Spi_real, Spi_imag);

		//imag(ifftn(SPi) - 1
		const auto minused = arithmeticOperationWithValue(ifft.outputImaginaryImage, 1, ArithmeticOperationWithValue::SUBTRACT);

		//1-imag(ifftn(SPi))
		const auto inverted = arithmeticOperationWithValue(minused, -1, ArithmeticOperationWithValue::MULTIPLY);

		//sqrt(1-imag(ifftn(SPi)))
		const auto magnitude = squareRoot(inverted);

		//n_m * sqrt(1-imag(ifftn(SPi)))
		const auto multiplied = arithmeticOperationWithValue(magnitude, n_m, ArithmeticOperationWithValue::MULTIPLY);

		return multiplied;
	}

	MikeHighPassFilter3d::MikeHighPassFilter3d() : d{ std::make_unique<Impl>() } {

	}
	MikeHighPassFilter3d::~MikeHighPassFilter3d() {

	}
	auto MikeHighPassFilter3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		attribute->SetAttrStyle(ProcessorAttrStyle::Disabled);

		if (attrID == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap{ { "Min", 0 }, { "Max", 100 }, { "Step", 0.1 } });
			attribute->SetAttrValue(10.0);
		}
		if (attrID == "Media RI") {
			attribute->SetAttrModel(QVariantMap{ { "Min", 1.0 }, { "Max", 3.0 }, { "Step", 0.001 },{"Decimals",3}});
			attribute->SetAttrValue(1.337);
		}
		
	}
	auto MikeHighPassFilter3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto MikeHighPassFilter3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
		auto inputIsImage = false;
		if (const auto inputImage = std::dynamic_pointer_cast<Data::HT3D>(data))
			inputIsImage = true;
		else if (const auto flImage = std::dynamic_pointer_cast<Data::FL3D>(data))
			inputIsImage = true;
		else if (const auto flImage = std::dynamic_pointer_cast<Data::Float3D>(data))
			inputIsImage = true;

		if (inputIsImage) {
			for (auto& prop : d->attrMap.values())
				prop->SetAttrStyle(ProcessorAttrStyle::Visible);
		}
	}

	auto MikeHighPassFilter3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}
	auto MikeHighPassFilter3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}
	auto MikeHighPassFilter3d::Process() -> DataMap {
		// create an input data
		TCImage::Pointer inputImage{ nullptr };
		std::shared_ptr<ImageView> refImageView{ nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset{ 0 };
		int timestep{ 0 };		
		QString chName;
		int ch;
		TCDataConverter converter;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0; max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX; res[1] = iresY; res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx; dimY = idimy; dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		}		
		else {
			return {};
		}

		// set algorithm parameters and input data		
		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toDouble();
		const auto mediaRI = d->attrMap["Media RI"]->GetAttrValue().toDouble();
		try {
			auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			floatImage = arithmeticOperationWithValue(floatImage, 10000, ArithmeticOperationWithValue::DIVIDE);

			double kernelRes[3];
			kernelRes[0] = 1 / (res[0] * dimX);
			kernelRes[1] = 1 / (res[1] * dimY);
			kernelRes[2] = 1 / (res[2] * dimZ);

			auto float_arr = static_cast<const float*>(floatImage->buffer());

			int dimensions[3]{ dimX,dimY,dimZ };

			vtkSmartPointer<vtkImageData> vtkImage = vtkSmartPointer<vtkImageData>::New();
			vtkImage->SetDimensions(dimensions);
			vtkImage->AllocateScalars(VTK_FLOAT, 1);


			for (int i = 0; i < dimensions[0]; i++) {
				for (int j = 0; j < dimensions[1]; j++) {
					for (int k = 0; k < dimensions[2]; k++) {
						float value = *(float_arr + k * dimensions[1] * dimensions[0] + j * dimensions[0] + i);
						vtkImage->SetScalarComponentFromFloat(i, j, k, 0, value);
					}
				}
			}			
			auto resultvtk = d->sfcFilter_Apply_SphericalLinear(vtkImage, kernelRes,kernelRadius,mediaRI);

			auto iv = d->vtkToImageDev(resultvtk);
			
			//auto iv = d->sfcFilter_Apply_SphericalLinear(floatImage, kernelRes, kernelRadius, mediaRI);

			iv = arithmeticOperationWithValue(iv, 10000, ArithmeticOperationWithValue::MULTIPLY);
			iv = convertImage(iv, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			auto resultStat = intensityStatistics(iv, IntensityStatistics::MIN_MAX, { 0,1 });
			DataPtr result{ nullptr };
			result = Data::DataConverter::ConvertToHTData<Data::HT3D>(static_cast<uint16_t*>(iv->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
			//result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(iv->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
			
		}catch(Exception& e) {
			std::cout << e.what();
		}		
		return {};
	}
	auto MikeHighPassFilter3d::Abort() -> void {

	}
}
