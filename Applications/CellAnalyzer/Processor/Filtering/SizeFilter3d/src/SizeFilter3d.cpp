#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCMask.h>

#include "SizeFilter3d.h"

namespace CellAnalyzer::Processor::Filtering {
	struct SizeFilter3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	SizeFilter3d::SizeFilter3d() : d { std::make_unique<Impl>() } {}

	SizeFilter3d::~SizeFilter3d() { }

	auto SizeFilter3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Object Size") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0.001 }, { "Max", 20000 }, { "Step", 0.001 }, { "Decimals", 3 } });
		}
	}

	auto SizeFilter3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto SizeFilter3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto SizeFilter3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto SizeFilter3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto SizeFilter3d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/filtering/TC.Algorithm.Filtering.SizeFilter.dll";

		// create an input data
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		float offset{ 0 };
		if (const auto data = std::dynamic_pointer_cast<Data::LabelMask3D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(data);
			timestep = data->GetTimeStep();
			offset = data->GetZOffset();
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		// set algorithm parameters and input data
		const auto objectSize = d->attrMap["Object Size"]->GetAttrValue().toDouble();
		const auto [resX, resY, resZ] = inputMask->GetResolution();
		if (qFuzzyIsNull(resX) || qFuzzyIsNull(resY) || qFuzzyIsNull(resZ)) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		auto params = algorithm->Parameter();
		params->SetValue("ObjectSize", objectSize / (resX * resY * resZ));

		algorithm->SetInput(0, inputMask);
		if (!algorithm->Execute()) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetTimeStep(timestep);
		resultMask->SetOffset(offset);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::LabelMask3D>(resultMask);
		if (result == nullptr) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		return { { "OutputMask", result } };
	}

	auto SizeFilter3d::Abort() -> void { }
}
