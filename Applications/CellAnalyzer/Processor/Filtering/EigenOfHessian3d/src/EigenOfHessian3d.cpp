#include <QCoreApplication>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <opencv2/opencv.hpp>

#include "TCDataConverter.h"

#include "EigenOfHessian3d.h"

namespace CellAnalyzer::Processor::Filtering {
	using ImageViewPtr = std::shared_ptr<iolink::ImageView>;
	using namespace imagedev;
	using namespace iolink;

	struct EigenOfHessian3d::Impl {
		const QMap<int, QString> outputOption {
			{ 0, "Eigenvalue:Largest" },
			{ 1, "Eigenvalue:Medium" },
			{ 2, "Eigenvalue:Smallest" },
			{ 3, "Eigenvector1:X" },
			{ 4, "Eigenvector1:Y" },
			{ 5, "Eigenvector1:Z" },
			{ 6, "Eigenvector2:X" },
			{ 7, "Eigenvector2:Y" },
			{ 8, "Eigenvector2:Z" },
			{ 9, "Eigenvector3:X" },
			{ 10, "Eigenvector3:Y" },
			{ 11, "Eigenvector3:Z" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	EigenOfHessian3d::EigenOfHessian3d() : d { std::make_unique<Impl>() } { }

	EigenOfHessian3d::~EigenOfHessian3d() { }

	auto EigenOfHessian3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "OutputSelection") {
			attribute->SetAttrModel(QStringList(d->outputOption.values()));
		} else if (id == "Standard Deviation X") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0.1 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		} else if (id == "Standard Deviation Y") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0.1 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		} else if (id == "Standard Deviation Z") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0.1 }, { "Max", 100 }, { "Step", 0.1 }, { "Decimals", 1 } });
		}
	}

	auto EigenOfHessian3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto EigenOfHessian3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto EigenOfHessian3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto EigenOfHessian3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto EigenOfHessian3d::Process() -> DataMap {
		// create an input data
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset { 0 };
		int timestep { 0 };
		QString chName;
		int ch;
		TCDataConverter converter;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto flimage = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(flimage);
			timestep = flimage->GetTimeStep();
			ch = flimage->GetChannelIndex();
			offset = flimage->GetZOffset();
			const auto [imin, imax] = flimage->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = flimage->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = flimage->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto floatimage = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			timestep = floatimage->GetTimeStep();
			offset = floatimage->GetZOffset();
			const auto [imin, imax] = floatimage->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = floatimage->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = floatimage->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(floatimage->GetData()), dimX, dimY, dimZ, res);
		} else {
			return {};
		}

		// set algorithm parameters and input data
		const auto SDx = d->attrMap["Standard Deviation X"]->GetAttrValue().toDouble();
		const auto SDy = d->attrMap["Standard Deviation Y"]->GetAttrValue().toDouble();
		const auto SDz = d->attrMap["Standard Deviation Z"]->GetAttrValue().toDouble();
		const auto outputString = d->attrMap["OutputSelection"]->GetAttrValue().toString();
		int outputKey = -1;
		QMap<int, QString>::const_iterator it = d->outputOption.constBegin();
		while (it != d->outputOption.constEnd()) {
			if (it.value() == outputString) {
				//qDebug() << "Key for value" << outputString << "is" << it.key();
				outputKey = it.key();
				break;
			}
			++it;
		}

		if (outputKey < 0) {
			return {};
		}

		EigenDecomposition3d::OutputSelection outputSelection;
		if (outputString.contains("Eigenvector1")) {
			outputSelection = EigenDecomposition3d::OutputSelection::EIGEN_VECTOR_1;
		} else if (outputString.contains("Eigenvector2")) {
			outputSelection = EigenDecomposition3d::OutputSelection::EIGEN_VECTOR_2;
		} else if (outputString.contains("Eigenvector3")) {
			outputSelection = EigenDecomposition3d::OutputSelection::EIGEN_VECTOR_3;
		} else if (outputString.contains("Eigenvalue")) {
			outputSelection = EigenDecomposition3d::OutputSelection::EIGEN_VALUES;
		} else {
			return {};
		}

		try {
			const auto hessian = gaussianHessianMatrix3d(refImageView, { SDx, SDy, SDz });
			const auto eigen = eigenDecomposition3d(hessian, outputSelection);
			ImageViewPtr eigenedImage;
			if (outputString.contains("Eigenvector1")) {
				eigenedImage = eigen.outputVectorImage1;
			} else if (outputString.contains("Eigenvector2")) {
				eigenedImage = eigen.outputVectorImage2;
			} else if (outputString.contains("Eigenvector3")) {
				eigenedImage = eigen.outputVectorImage3;
			} else if (outputString.contains("Eigenvalue")) {
				eigenedImage = eigen.outputEigenvaluesImage;
			} else {
				return {};
			}

			const auto channelIdx = outputKey % 3;

			const auto extracted = getColorChannel(eigenedImage, channelIdx);

			const auto converted = convertImage(extracted, ConvertImage::FLOAT_32_BIT);

			//float image
			const auto resultStat = intensityStatistics(converted, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(converted->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return {};
	}

	auto EigenOfHessian3d::Abort() -> void { }
}
