#pragma once

#include <IProcessor.h>

namespace CellAnalyzer::Processor::Filtering {
	class EigenOfHessian3d final : public IProcessor {
	public:
		EigenOfHessian3d();
		~EigenOfHessian3d() override;

		auto SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void override;
		auto SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void override;
		auto SetInputData(const QString& inputID, const DataPtr& data) -> void override;

		auto GetAttr(const QString& attrID) const -> ProcessorAttrPtr override;
		auto GetInputData(const QString& inputID) const -> DataPtr override;

		auto Process() -> DataMap override;
		auto Abort() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	DECLARE_PROCESSOR_START(EigenOfHessian3d) {
		Q_OBJECT
	DECLARE_PROCESSOR_END(EigenOfHessian3d, Filtering, DEFAULT_PATH)
	};
}
