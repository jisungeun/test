#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCMask.h>

#include "BorderKill3d.h"

namespace CellAnalyzer::Processor::Filtering {
	struct BorderKill3d::Impl {
		const QMap<int, QString> paramNeighborhood {
			{ 0, "Connectivity-6" },
			{ 1, "Connectivity-18" },
			{ 2, "Connectivity-26" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	BorderKill3d::BorderKill3d() : d { std::make_unique<Impl>() } {}

	BorderKill3d::~BorderKill3d() { }

	auto BorderKill3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Neighborhood") {
			attribute->SetAttrModel(QStringList(d->paramNeighborhood.values()));
		}
	}

	auto BorderKill3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto BorderKill3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto BorderKill3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto BorderKill3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto BorderKill3d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/filtering/TC.Algorithm.Filtering.BorderKill.dll";

		// create an input data
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		float offset{ 0 };
		if (const auto data = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(data);
			timestep = data->GetTimeStep();
			offset = data->GetZOffset();
		} else {
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		// set algorithm parameters and input data
		const auto neighborhood = d->attrMap["Neighborhood"]->GetAttrValue().toString();

		auto params = algorithm->Parameter();
		params->SetValue("Neighborhood", d->paramNeighborhood.key(neighborhood));

		algorithm->SetInput(0, inputMask);
		if (!algorithm->Execute()) {
			return {};
		}

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetTimeStep(timestep);
		resultMask->SetOffset(offset);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask3D>(resultMask);
		if (result == nullptr) {
			return {};
		}

		return { { "OutputMask", result } };
	}

	auto BorderKill3d::Abort() -> void { }
}
