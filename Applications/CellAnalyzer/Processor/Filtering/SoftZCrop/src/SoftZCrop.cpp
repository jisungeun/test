#include <QCoreApplication>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include "TCDataConverter.h"

#include "SoftZCrop.h"

namespace CellAnalyzer::Processor::Filtering {
	using namespace imagedev;
	using namespace iolink;

	struct SoftZCrop::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	SoftZCrop::SoftZCrop() : d { std::make_unique<Impl>() } { }

	SoftZCrop::~SoftZCrop() { }

	auto SoftZCrop::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		attribute->SetAttrStyle(ProcessorAttrStyle::Disabled);
	}

	auto SoftZCrop::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto SoftZCrop::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
		TCImage::Pointer image { nullptr };
		int maxZ;
		if (const auto inputImage = std::dynamic_pointer_cast<Data::HT3D>(data)) {
			const auto [x, y, z] = inputImage->GetSize();
			maxZ = z;
		} else if (const auto flImage = std::dynamic_pointer_cast<Data::FL3D>(data)) {
			const auto [x, y, z] = flImage->GetSize();
			maxZ = z;
		} else if (const auto floatimage = std::dynamic_pointer_cast<Data::Float3D>(data)) {
			const auto [x, y, z] = floatimage->GetSize();
			maxZ = z;
		} else {
			return;
		}
		for (auto p : d->attrMap) {
			p->SetAttrStyle(ProcessorAttrStyle::Visible);
		}
		if (d->attrMap.contains("Upper Z Location")) {
			d->attrMap["Upper Z Location"]->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", maxZ }, { "Step", 1 } });
			d->attrMap["Upper Z Location"]->SetAttrValue(maxZ);
		}
		if (d->attrMap.contains("Lower Z Location")) {
			d->attrMap["Lower Z Location"]->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", maxZ }, { "Step", 1 } });
			d->attrMap["Lower Z Location"]->SetAttrValue(1);
		}
	}

	auto SoftZCrop::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto SoftZCrop::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto SoftZCrop::Process() -> DataMap {
		// create an input data
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int timestep { 0 };
		int dimX, dimY, dimZ;
		double offset { 0 };
		int chIdx { 0 };
		QString chName;
		TCDataConverter converter;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			chIdx = image->GetChannelIndex();
			chName = image->GetChannelName();
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			timestep = image->GetTimeStep();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		// set algorithm parameters and input data
		const auto upperZ = d->attrMap["Upper Z Location"]->GetAttrValue().toInt();
		const auto lowerZ = d->attrMap["Lower Z Location"]->GetAttrValue().toInt();

		if (false == upperZ > lowerZ) {
			return {};
		}
		try {
			const auto formula = QString("if (z < %1 || z > %2, 0 ,1)").arg(lowerZ).arg(upperZ);
			const auto outputBinary = imageFormula(refImageView, nullptr, nullptr, formula.toStdString(), ImageFormula::UNSIGNED_INTEGER_16_BIT);
			int dim[3] { dimX, dimY, dimZ };
			const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(outputBinary->buffer()), dim, res);
			resultMask->SetTimeStep(timestep);
			resultMask->SetOffset(offset);
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask3D>(resultMask);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputMask", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto SoftZCrop::Abort() -> void { }
}
