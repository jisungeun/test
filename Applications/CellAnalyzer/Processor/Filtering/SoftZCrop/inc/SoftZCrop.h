#pragma once

#include <IProcessor.h>

namespace CellAnalyzer::Processor::Filtering {
	class SoftZCrop final : public IProcessor {
	public:
		SoftZCrop();
		~SoftZCrop() override;

		auto SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void override;
		auto SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void override;
		auto SetInputData(const QString& inputID, const DataPtr& data) -> void override;

		auto GetAttr(const QString& attrID) const -> ProcessorAttrPtr override;
		auto GetInputData(const QString& inputID) const -> DataPtr override;

		auto Process() -> DataMap override;
		auto Abort() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	DECLARE_PROCESSOR_START(SoftZCrop) {
		Q_OBJECT
		DECLARE_PROCESSOR_END(SoftZCrop, Filtering, DEFAULT_PATH)
	};
}
