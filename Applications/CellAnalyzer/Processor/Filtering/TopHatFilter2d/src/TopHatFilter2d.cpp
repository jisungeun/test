#include <QCoreApplication>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include "TCDataConverter.h"

#include "TopHatFilter2d.h"

namespace CellAnalyzer::Processor::Filtering {
	using namespace imagedev;
	using namespace iolink;
	struct TopHatFilter2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
		const QMap<int, QString> paramLightness{
			{ 0, "Bright objects" },
			{ 1, "Dark objects" }
		};
	};
	TopHatFilter2d::TopHatFilter2d() : d{ std::make_unique<Impl>() } {

	}
	TopHatFilter2d::~TopHatFilter2d() {

	}
	auto TopHatFilter2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		attribute->SetAttrStyle(ProcessorAttrStyle::Disabled);

		if (attrID == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap{ { "Min", 1 }, { "Max", 1000 }, { "Step", 1 } });
		}
		else if (attrID == "Object Lightness") {
			attribute->SetAttrModel(QStringList(d->paramLightness.values()));
		}
	}
	auto TopHatFilter2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto TopHatFilter2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
		auto inputIsImage = false;
		if (const auto inputImage = std::dynamic_pointer_cast<Data::HT2D>(data))
			inputIsImage = true;
		else if (const auto flImage = std::dynamic_pointer_cast<Data::FL2D>(data))
			inputIsImage = true;
		else if (const auto flImage = std::dynamic_pointer_cast<Data::Float2D>(data))
			inputIsImage = true;

		if (inputIsImage) {
			for (auto& prop : d->attrMap.values())
				prop->SetAttrStyle(ProcessorAttrStyle::Visible);
		}
	}

	auto TopHatFilter2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}
	auto TopHatFilter2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}
	auto TopHatFilter2d::Process() -> DataMap {
		// create an input data
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		int timestep{ 0 };
		TCDataConverter converter;
		// set algorithm parameters and input data
		TCImage::Pointer inputImage{ nullptr };
		std::shared_ptr<ImageView> refImageView{ nullptr };
		if (const auto image = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0; max = imax * 10000.0;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX; res[1] = iresY; res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx; dimY = idimy; dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
		}
		else if (const auto image = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetIntensity();
			min = imin; max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX; res[1] = iresY; res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx; dimY = idimy; dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
		}
		else if (const auto image = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			timestep = image->GetTimeStep();
			min = imin; max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX; res[1] = iresY; res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx; dimY = idimy; dimZ = 1;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
		}
		else {
			return {};
		}

		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		const auto lightstring = d->attrMap["Object Lightness"]->GetAttrValue().toString();
		try {
			DataPtr result{ nullptr };
			const auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			if (lightstring == "Bright objects") {
				const auto opened = openingDisk2d(floatImage, kernelRadius, OpeningDisk2d::FASTER, OpeningDisk2d::LIMITED);
				const auto diff = arithmeticOperationWithImage(floatImage, opened, ArithmeticOperationWithImage::SUBTRACT);
				const auto resultStat = intensityStatistics(diff, IntensityStatistics::MIN_MAX, { 0,1 });				
				result = Data::DataConverter::ConvertToFloatData<Data::Float2D>(static_cast<float*>(diff->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
				if (result == nullptr) {
					return {};
				}				
			}else {				
				const auto closed =closingDisk2d(floatImage, kernelRadius, ClosingDisk2d::FASTER, ClosingDisk2d::LIMITED);
				const auto diff = arithmeticOperationWithImage(closed, floatImage, ArithmeticOperationWithImage::SUBTRACT);
				const auto resultStat = intensityStatistics(diff, IntensityStatistics::MIN_MAX, { 0,1 });				
				result = Data::DataConverter::ConvertToFloatData<Data::Float2D>(static_cast<float*>(diff->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep);
				if (result == nullptr) {
					return {};
				}				
			}
			return { { "OutputImage", result } };
		}
		catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return {};
	}
	auto TopHatFilter2d::Abort() -> void {

	}
}
