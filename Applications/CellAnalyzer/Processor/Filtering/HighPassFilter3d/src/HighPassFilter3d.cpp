#include <QCoreApplication>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <opencv2/opencv.hpp>

#include "TCDataConverter.h"

#include "HighPassFilter3d.h"

#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <vtkImageFFT.h>
#include <vtkImageRFFT.h>
#include <vtkImageButterworthHighPass.h>
#include <vtkImageExtractComponents.h>

namespace CellAnalyzer::Processor::Filtering {
	using ImageViewPtr = std::shared_ptr<iolink::ImageView>;
	using namespace imagedev;
	using namespace iolink;
	struct HighPassFilter3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;		
	};
	
	HighPassFilter3d::HighPassFilter3d() : d{ std::make_unique<Impl>() } {

	}
	HighPassFilter3d::~HighPassFilter3d() {

	}
	auto HighPassFilter3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		attribute->SetAttrStyle(ProcessorAttrStyle::Disabled);

		if (attrID == "Cutoff") {
			attribute->SetAttrModel(QVariantMap{ { "Min", 0 }, { "Max", 1 }, { "Step", 0.01 } });
			attribute->SetAttrValue(0.1);
		}		
	}
	auto HighPassFilter3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto HighPassFilter3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
		auto inputIsImage = false;
		if (const auto inputImage = std::dynamic_pointer_cast<Data::HT3D>(data))
			inputIsImage = true;
		else if (const auto flImage = std::dynamic_pointer_cast<Data::FL3D>(data))
			inputIsImage = true;
		else if (const auto flImage = std::dynamic_pointer_cast<Data::Float3D>(data))
			inputIsImage = true;

		if (inputIsImage) {
			for (auto& prop : d->attrMap.values())
				prop->SetAttrStyle(ProcessorAttrStyle::Visible);
		}
	}

	auto HighPassFilter3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}
	auto HighPassFilter3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}
	auto HighPassFilter3d::Process() -> DataMap {
		// create an input data
		TCImage::Pointer inputImage{ nullptr };
		std::shared_ptr<ImageView> refImageView{ nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset{ 0 };
		int timestep{ 0 };
		bool isFloat = false;
		bool isFL = false;
		QString chName;
		int ch;
		TCDataConverter converter;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0; max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX; res[1] = iresY; res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx; dimY = idimy; dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		}
		else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			isFL = true;
			ch = image->GetChannelIndex();
			chName = image->GetChannelName();
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetIntensity();
			min = imin; max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX; res[1] = iresY; res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx; dimY = idimy; dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
		}
		else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			isFloat = true;
			const auto [imin, imax] = image->GetRange();
			timestep = image->GetTimeStep();
			min = imin; max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX; res[1] = iresY; res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx; dimY = idimy; dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
		}
		else {
			return {};
		}

		// set algorithm parameters and input data
		//const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		const auto cutoff = d->attrMap["Cutoff"]->GetAttrValue().toDouble();
		try {			
			auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			auto float_arr = static_cast<const float*>(floatImage->buffer());

			int dimensions[3]{ dimX,dimY,dimZ };

			vtkSmartPointer<vtkImageData> vtkImage = vtkSmartPointer<vtkImageData>::New();
			vtkImage->SetDimensions(dimensions);
			vtkImage->AllocateScalars(VTK_FLOAT, 1);


			for (int i = 0; i < dimensions[0]; i++) {
				for (int j = 0; j < dimensions[1]; j++) {
					for (int k = 0; k < dimensions[2]; k++) {					
						float value = *(float_arr + k * dimensions[1] * dimensions[0] + j * dimensions[0] + i);
						vtkImage->SetScalarComponentFromFloat(i, j, k, 0, value);
					}
				}
			}
			
			vtkSmartPointer<vtkImageFFT> fft = vtkSmartPointer<vtkImageFFT>::New();
			fft->SetInputData(vtkImage);

			vtkSmartPointer<vtkImageButterworthHighPass> hipass = vtkSmartPointer< vtkImageButterworthHighPass>::New();
			hipass->SetCutOff(cutoff);
			hipass->SetInputConnection(fft->GetOutputPort());			

			vtkSmartPointer<vtkImageRFFT> idealRfft = vtkSmartPointer<vtkImageRFFT>::New();
			idealRfft->SetInputConnection(hipass->GetOutputPort());

			vtkSmartPointer<vtkImageExtractComponents> idealReal = vtkSmartPointer<vtkImageExtractComponents>::New();
			idealReal->SetInputConnection(idealRfft->GetOutputPort());
			idealReal->SetComponents(0);
			idealReal->Update();

			auto resultvtk = idealReal->GetOutput();
			resultvtk->Print(std::cout);

			ImageViewPtr iv;

			VectorXu64 imageShape{ static_cast<uint64_t>(dimX), static_cast<uint64_t>(dimY),static_cast<uint64_t>(dimZ) };

			iv = ImageViewFactory::allocate(imageShape, DataTypeId::FLOAT);

			setDimensionalInterpretation(iv, ImageTypeId::VOLUME);
			setImageInterpretation(iv, ImageInterpretation::GRAYSCALE);

			VectorXu64 imageOrig{ 0, 0 ,0 };
			RegionXu64 imageRegion{ imageOrig, imageShape };

			auto result_arr = std::shared_ptr<float[]>(new float[dimX * dimY * dimZ](), std::default_delete<float[]>());

			for (int k = 0; k < dimensions[2]; k++) {				
				for (int j = 0; j < dimensions[1]; j++) {
					for (int i = 0; i < dimensions[0]; i++) {
						float value = resultvtk->GetScalarComponentAsFloat(i, j, k, 0);
						*(result_arr.get() + k * dimensions[1] * dimensions[0] + j * dimensions[0] + i) = value;						
					}
				}
			}

			iv->writeRegion(imageRegion, result_arr.get());

			auto resultStat = intensityStatistics(iv, IntensityStatistics::MIN_MAX, { 0,1 });
			DataPtr result{ nullptr };
			result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(iv->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		}
		catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		catch (cv::Exception& e) {
			std::cout << e.what() << std::endl;
		}		
		return {};
	}
	auto HighPassFilter3d::Abort() -> void {

	}
}
