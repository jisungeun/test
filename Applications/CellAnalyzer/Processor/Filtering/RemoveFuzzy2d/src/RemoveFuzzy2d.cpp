#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCMask.h>

#include <ImageDev/ImageDev.h>
#include <iolink/view/ImageViewFactory.h>
#include <TCDataConverter.h>

#include "RemoveFuzzy2d.h"

#include "QsLog.h"

namespace CellAnalyzer::Processor::Filtering {
	using namespace imagedev;
	using namespace iolink;

	struct RemoveFuzzy2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	RemoveFuzzy2d::RemoveFuzzy2d() : d { std::make_unique<Impl>() } { }

	RemoveFuzzy2d::~RemoveFuzzy2d() { }

	auto RemoveFuzzy2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "KernelSize") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 3 }, { "Step", 1 } });
			attribute->SetAttrValue(1);
		} else if (id == "NeighborPixels") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 49 }, { "Step", 1 } });
			attribute->SetAttrValue(3);
		}
	}

	auto RemoveFuzzy2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto RemoveFuzzy2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto RemoveFuzzy2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto RemoveFuzzy2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto RemoveFuzzy2d::Process() -> DataMap {
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		std::shared_ptr<ImageView> inputBinary { nullptr };
		TCDataConverter converter;
		int dim[3];
		double res[3];
		if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			timestep = mask->GetTimeStep();
			const auto tcmask = Data::DataConverter::ConvertToTCMask(mask);
			inputBinary = converter.MaskToImageView(tcmask);
			dim[0] = mask->GetSize().x;
			dim[1] = mask->GetSize().y;
			dim[2] = 1;
			res[0] = mask->GetResolution().x;
			res[1] = mask->GetResolution().y;
			res[2] = 1;
		} else {
			return {};
		}

		const auto kernel = d->attrMap["KernelSize"]->GetAttrValue().toInt();
		auto neighbor = d->attrMap["NeighborPixels"]->GetAttrValue().toInt();
		if (neighbor > pow(kernel * 2 + 1, 2)) {
			neighbor = pow(kernel * 2 + 1, 2);
		}

		try {
			const auto converted = convertImage(inputBinary, ConvertImage::UNSIGNED_INTEGER_16_BIT);
			const auto dataPtr = static_cast<uint16_t*>(converted->buffer());

			std::shared_ptr<uint16_t[]> resultPtr(new uint16_t[dim[0] * dim[1]](), std::default_delete<uint16_t[]>());

			for (auto i = 0; i < dim[0]; i++) {
				for (auto j = 0; j < dim[1]; j++) {
					int sum = 0;
					for (auto kx = -kernel; kx <= kernel; kx++) {
						for (auto ky = - -kernel; ky <= kernel; ky++) {
							if (i + kx < 0 || i + kx > dim[0] - 1 || j + ky < 0 || j + ky > dim[1] - 1) {
								continue;
							}
							const auto idx = (i + kx) * dim[1] + j + ky;
							const auto value = *(dataPtr + idx);
							if (value > 0) {
								sum++;
							}
						}
					}
					const auto targetIdx = i * dim[1] + j;
					const auto sourceValue = *(dataPtr + targetIdx);
					if (sum < neighbor) {
						*(resultPtr.get() + targetIdx) = 0;
					} else if(sourceValue > 0){
						*(resultPtr.get() + targetIdx) = 1;
					} else {
						*(resultPtr.get() + targetIdx) = 0;
					}
				}
			}
			TCDataConverter converter;
			const auto resultMask = converter.ArrToLabelMask(resultPtr.get(), dim, res);
			resultMask->SetTimeStep(timestep);
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask2D>(resultMask);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputMask", result } };
		} catch (Exception& e) {
			QLOG_DEBUG() << e.what().c_str();
		}
		return {};
	}

	auto RemoveFuzzy2d::Abort() -> void { }
}
