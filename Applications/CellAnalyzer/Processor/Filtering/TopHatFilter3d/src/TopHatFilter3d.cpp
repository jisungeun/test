#include <QCoreApplication>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include "TCDataConverter.h"

#include "TopHatFilter3d.h"

namespace CellAnalyzer::Processor::Filtering {
	using namespace imagedev;
	using namespace iolink;
	struct TopHatFilter3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
		const QMap<int, QString> paramLightness{
			{ 0, "Bright objects" },
			{ 1, "Dark objects" }
		};
	};
	TopHatFilter3d::TopHatFilter3d() : d{ std::make_unique<Impl>() } {

	}
	TopHatFilter3d::~TopHatFilter3d() {

	}
	auto TopHatFilter3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		attribute->SetAttrStyle(ProcessorAttrStyle::Disabled);

		if (attrID == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap{ { "Min", 1 }, { "Max", 1000 }, { "Step", 1 } });
		}
		else if (attrID == "Object Lightness") {
			attribute->SetAttrModel(QStringList(d->paramLightness.values()));
		}
	}
	auto TopHatFilter3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto TopHatFilter3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
		auto inputIsImage = false;
		if (const auto inputImage = std::dynamic_pointer_cast<Data::HT3D>(data))
			inputIsImage = true;
		else if (const auto flImage = std::dynamic_pointer_cast<Data::FL3D>(data))
			inputIsImage = true;
		else if (const auto flImage = std::dynamic_pointer_cast<Data::Float3D>(data))
			inputIsImage = true;

		if (inputIsImage) {
			for (auto& prop : d->attrMap.values())
				prop->SetAttrStyle(ProcessorAttrStyle::Visible);
		}
	}

	auto TopHatFilter3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}
	auto TopHatFilter3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}
	auto TopHatFilter3d::Process() -> DataMap {
		// create an input data
		TCImage::Pointer inputImage{ nullptr };
		std::shared_ptr<ImageView> refImageView{ nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset{ 0 };
		int timestep{ 0 };
		TCDataConverter converter;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0; max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX; res[1] = iresY; res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx; dimY = idimy; dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		}
		else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetIntensity();
			min = imin; max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX; res[1] = iresY; res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx; dimY = idimy; dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
		}
		else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			timestep = image->GetTimeStep();
			min = imin; max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX; res[1] = iresY; res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx; dimY = idimy; dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
		}
		else {
			return {};
		}

		// set algorithm parameters and input data
		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		const auto lightstring = d->attrMap["Object Lightness"]->GetAttrValue().toString();
		try {
			DataPtr result{ nullptr };
			const auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			if (lightstring == "Bright objects") {
				const auto opened = openingBall3d(floatImage, kernelRadius, OpeningBall3d::FASTER, OpeningBall3d::LIMITED);
				const auto diff = arithmeticOperationWithImage(refImageView, opened, ArithmeticOperationWithImage::SUBTRACT);

				const auto resultStat = intensityStatistics(diff, IntensityStatistics::MIN_MAX, { 0,1 });
				result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(diff->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
				if (result == nullptr) {
					return {};
				}
			}else {
				const auto closed = closingBall3d(floatImage, kernelRadius, ClosingBall3d::FASTER, ClosingBall3d::LIMITED);
				const auto diff = arithmeticOperationWithImage(closed, refImageView, ArithmeticOperationWithImage::SUBTRACT);

				const auto resultStat = intensityStatistics(diff, IntensityStatistics::MIN_MAX, { 0,1 });
				result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(diff->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
				if (result == nullptr) {
					return {};
				}
			}
			return { { "OutputImage", result } };
		}
		catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return {};
	}
	auto TopHatFilter3d::Abort() -> void {

	}
}
