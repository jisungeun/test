#include <QCoreApplication>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include "TCDataConverter.h"

#include "VarianceFilter3d.h"

namespace CellAnalyzer::Processor::Filtering {
	using namespace imagedev;
	using namespace iolink;

	struct VarianceFilter3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	VarianceFilter3d::VarianceFilter3d() : d { std::make_unique<Impl>() } { }

	VarianceFilter3d::~VarianceFilter3d() { }

	auto VarianceFilter3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		attribute->SetAttrStyle(ProcessorAttrStyle::Disabled);

		if (attrID == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 50 }, { "Step", 1 } });
		}
	}

	auto VarianceFilter3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto VarianceFilter3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
		auto inputIsImage = false;
		if (const auto inputImage = std::dynamic_pointer_cast<Data::HT3D>(data))
			inputIsImage = true;
		else if (const auto flImage = std::dynamic_pointer_cast<Data::FL3D>(data))
			inputIsImage = true;
		else if (const auto flImage = std::dynamic_pointer_cast<Data::Float3D>(data))
			inputIsImage = true;

		if (inputIsImage) {
			for (auto& prop : d->attrMap.values())
				prop->SetAttrStyle(ProcessorAttrStyle::Visible);
		}
	}

	auto VarianceFilter3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto VarianceFilter3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto VarianceFilter3d::Process() -> DataMap {
		// create an input data
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset { 0 };
		int timestep { 0 };
		TCDataConverter converter;
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			timestep = image->GetTimeStep();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
		} else {
			return {};
		}

		// set algorithm parameters and input data
		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		try {
			const auto imageStat = intensityStatistics(refImageView, IntensityStatistics::MIN_MAX, { 0, 1 });
			if (imageStat->minimum() < min) {
				//stitching data with 0 value
				const auto thersholded = thresholding(refImageView, { -INT_MAX, static_cast<double>(min) });
				const auto reseted = resetImage(refImageView, min);
				refImageView = combineByMask(reseted, refImageView, thersholded);
			}
			auto floatingInput = rescaleIntensity(refImageView, RescaleIntensity::FLOAT_32_BIT, RescaleIntensity::MIN_MAX, { 1, 1 }, { 1, 1 }, { 0.0, 10.0 });

			const auto variImage = varianceFilter3d(floatingInput, imagedev::VarianceFilter3d::KernelShape::BALL, kernelRadius);

			const auto resultStat = intensityStatistics(variImage, IntensityStatistics::MIN_MAX, { 0, 1 });
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToFloatData<Data::Float3D>(static_cast<float*>(variImage->buffer()), dimX, dimY, dimZ, res[0], res[1], res[2], resultStat->minimum(), resultStat->maximum(), timestep, offset);
			if (result == nullptr) {
				return {};
			}
			return { { "OutputImage", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto VarianceFilter3d::Abort() -> void { }
}
