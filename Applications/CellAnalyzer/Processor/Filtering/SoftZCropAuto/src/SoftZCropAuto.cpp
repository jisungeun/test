#include <variant>

#include <QCoreApplication>

#include <PluginRegistry.h>
#include <IPluginAlgorithm.h>
#include <DataConverter.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include "TCDataConverter.h"
#include "TCBestSliceFinder.h"

#include "SoftZCropAuto.h"

namespace CellAnalyzer::Processor::Filtering {
    using namespace imagedev;
    using namespace iolink;

    struct SoftZCropAuto::Impl {
        using DataVariant = std::variant<
            std::shared_ptr<Data::HT3D>,
            std::shared_ptr<Data::FL3D>,
            std::shared_ptr<Data::Float3D>
        >;

        static auto ConvertToVariant(const DataPtr& base)->std::optional<DataVariant>;

        template<typename... Ts>
        struct Overload : Ts... { using Ts::operator()...; };

        template<typename... Ts> Overload(Ts...)->Overload<Ts...>;

        QMap<QString, ProcessorAttrPtr> attrMap;
        QMap<QString, DataPtr> inputMap;
    };

    auto SoftZCropAuto::Impl::ConvertToVariant(const DataPtr& base) -> std::optional<DataVariant> {
        if (auto ht = std::dynamic_pointer_cast<Data::HT3D>(base)) return ht;
        if (auto fl = std::dynamic_pointer_cast<Data::FL3D>(base)) return fl;
        if (auto f = std::dynamic_pointer_cast<Data::Float3D>(base)) return f;
        return std::nullopt;
    }

    SoftZCropAuto::SoftZCropAuto() : d{std::make_unique<Impl>()} {
    }

    SoftZCropAuto::~SoftZCropAuto() = default;

    auto SoftZCropAuto::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
        d->attrMap[attrID] = attribute;
        attribute->SetAttrStyle(ProcessorAttrStyle::Disabled);
    }

    auto SoftZCropAuto::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
        if (false == d->attrMap.contains(attrID)) {
            return;
        }
        d->attrMap[attrID]->SetAttrValue(value);
    }

    auto SoftZCropAuto::SetInputData(const QString& id, const DataPtr& data) -> void {
        d->inputMap[id] = data;

        if (data == nullptr) return;

        TCImage::Pointer image{nullptr};
        const auto inputImageOpt = Impl::ConvertToVariant(data);
        if (false == inputImageOpt.has_value()) {
            return;
        }

        const auto& inputImage = inputImageOpt.value();
        const auto [maxX, maxY, maxZ] = std::visit(Impl::Overload{
            [](const std::shared_ptr<Data::HT3D>& img) {return img->GetSize(); },
            [](const std::shared_ptr<Data::FL3D>& img) {return img->GetSize(); },
            [](const std::shared_ptr<Data::Float3D>& img) {return img->GetSize(); }
            } , inputImage);

        for (const auto p : d->attrMap) {
            p->SetAttrStyle(ProcessorAttrStyle::Visible);
        }
        if (d->attrMap.contains("Upper Z Delta")) {
            d->attrMap["Upper Z Delta"]->SetAttrModel(QVariantMap{{"Min", 0}, {"Max", maxZ}, {"Step", 1}});
            d->attrMap["Upper Z Delta"]->SetAttrValue(0);
        }
        if (d->attrMap.contains("Lower Z Delta")) {
            d->attrMap["Lower Z Delta"]->SetAttrModel(QVariantMap{{"Min", 0}, {"Max", maxZ}, {"Step", 1}});
            d->attrMap["Lower Z Delta"]->SetAttrValue(0);
        }
    }

    auto SoftZCropAuto::GetAttr(const QString& id) const -> ProcessorAttrPtr {
        return d->attrMap[id];
    }

    auto SoftZCropAuto::GetInputData(const QString& id) const -> DataPtr {
        return d->inputMap[id];
    }

    auto SoftZCropAuto::Process() -> DataMap {
        if (false == d->inputMap.contains("InputImage")) {
            return {};
        }

        const auto inputImageOpt = Impl::ConvertToVariant(d->inputMap["InputImage"]);
        if (false == inputImageOpt.has_value()) {
            return {};
        }

        const auto& inputImageVar = inputImageOpt.value();

        const auto [dimX, dimY, dimZ] = std::visit(Impl::Overload{
            [](const std::shared_ptr<Data::HT3D>& img) {return img->GetSize(); },
            [](const std::shared_ptr<Data::FL3D>& img) {return img->GetSize(); },
            [](const std::shared_ptr<Data::Float3D>& img) {return img->GetSize(); }
            }, inputImageVar);
        int dim[3] = { dimX, dimY, dimZ };
        const auto [resX, resY, resZ] = std::visit(Impl::Overload{
            [](const std::shared_ptr<Data::HT3D>& img) {return img->GetResolution(); },
            [](const std::shared_ptr<Data::FL3D>& img) {return img->GetResolution(); },
            [](const std::shared_ptr<Data::Float3D>& img) {return img->GetResolution(); }
            }, inputImageVar);
        double res[3]{ resX, resY, resZ };
        const auto timeStep = std::visit(Impl::Overload{
            [](const std::shared_ptr<Data::HT3D>& img) {return img->GetTimeStep(); },
            [](const std::shared_ptr<Data::FL3D>& img) {return img->GetTimeStep(); },
            [](const std::shared_ptr<Data::Float3D>& img) {return img->GetTimeStep(); }
            }, inputImageVar);
        auto converter = TCDataConverter{};
        const auto refImageViewVisitor = Impl::Overload{
            [&converter](const std::shared_ptr<Data::HT3D>& img) {return converter.ImageToImageView(Data::DataConverter::ConvertToTCImage(img)); },
            [&converter](const std::shared_ptr<Data::FL3D>& img) {return converter.ImageToImageView(Data::DataConverter::ConvertToTCImage(img)); },
            [&converter, &dim, &res] (const std::shared_ptr<Data::Float3D>& img) {
                return converter.FloatArrToImageView(static_cast<float*>(img->GetData()), dim[0], dim[1], dim[2], res);
            }
        };
        const auto refImageView = std::visit(refImageViewVisitor, inputImageVar);
        const auto offset = std::visit(Impl::Overload{
            []([[maybe_unused]] const std::shared_ptr<Data::HT3D>& img) {return static_cast<double>(0); },
            [](const std::shared_ptr<Data::FL3D>& img) {return img->GetZOffset(); },
            [](const std::shared_ptr<Data::Float3D>& img) {return img->GetZOffset(); }
            }, inputImageVar);

        // set algorithm parameters and input data
        const auto upperZDelta = d->attrMap["Upper Z Delta"]->GetAttrValue().toInt();
        const auto lowerZDelta = d->attrMap["Lower Z Delta"]->GetAttrValue().toInt();

        if (upperZDelta < 0 || lowerZDelta < 0) {
            return {};
        }
        try {
            const auto finder = TC::Components::Algorithm::TCBestSliceFinder{};
            const auto data = static_cast<uint16_t*>(refImageView->buffer());
            const auto bestZIndex = *finder.FindXYSlice(data, dimX, dimY, dimZ)+1;

            const auto upperZ = bestZIndex + upperZDelta;
            const auto lowerZ = bestZIndex - lowerZDelta;
            const auto formula = QString("if (z < %1 || z > %2, 0, 1)").arg(lowerZ).arg(upperZ);
            const auto outputBinary = imageFormula(refImageView, nullptr, nullptr, formula.toStdString(),
                                                   ImageFormula::UNSIGNED_INTEGER_16_BIT);
            const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(outputBinary->buffer()), dim, res);
            resultMask->SetTimeStep(timeStep);
            resultMask->SetOffset(offset);
            DataPtr result{nullptr};
            result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask3D>(resultMask);
            if (result == nullptr) {
                return {};
            }
            return { {"OutputMask", result} };
        }
        catch (Exception& e) {
            return {};
        }
    }

    auto SoftZCropAuto::Abort() -> void {
    }
}
