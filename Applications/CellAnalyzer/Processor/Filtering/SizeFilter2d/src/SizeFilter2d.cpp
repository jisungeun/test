#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCMask.h>

#include "SizeFilter2d.h"

namespace CellAnalyzer::Processor::Filtering {
	struct SizeFilter2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	SizeFilter2d::SizeFilter2d() : d { std::make_unique<Impl>() } { }

	SizeFilter2d::~SizeFilter2d() { }

	auto SizeFilter2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Object Size") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0.001 }, { "Max", 20000 }, { "Step", 0.001 }, { "Decimals", 3 } });
		}
	}

	auto SizeFilter2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto SizeFilter2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto SizeFilter2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto SizeFilter2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto SizeFilter2d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/filtering/TC.Algorithm.Filtering.SizeFilter.dll";

		// create an input data
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		if (const auto data = std::dynamic_pointer_cast<Data::LabelMask2D>(d->inputMap["InputMask"])) {
			timestep = data->GetTimeStep();
			inputMask = Data::DataConverter::ConvertToTCMask(data);
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		// set algorithm parameters and input data
		const auto objectSize = d->attrMap["Object Size"]->GetAttrValue().toDouble();
		const auto [resX, resY, resZ] = inputMask->GetResolution();
		if (qFuzzyIsNull(resX) || qFuzzyIsNull(resY)) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		auto params = algorithm->Parameter();
		params->SetValue("ObjectSize", objectSize / (resX * resY));

		algorithm->SetInput(0, inputMask);
		if (!algorithm->Execute()) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetTimeStep(timestep);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::LabelMask2D>(resultMask);
		if (result == nullptr) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		return { { "OutputMask", result } };
	}

	auto SizeFilter2d::Abort() -> void { }
}
