#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ImageDev/Data/NativeMeasurements.h>
#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "ReassignLabels2d.h"

namespace CellAnalyzer::Processor::Labeling {
	using namespace imagedev;
	using namespace iolink;

	struct ReassignLabels2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	ReassignLabels2d::ReassignLabels2d() : d { std::make_unique<Impl>() } { }

	ReassignLabels2d::~ReassignLabels2d() { }

	auto ReassignLabels2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
	}

	auto ReassignLabels2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto ReassignLabels2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto ReassignLabels2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto ReassignLabels2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto ReassignLabels2d::Process() -> DataMap {
		std::shared_ptr<ImageView> labelView { nullptr };
		TCDataConverter converter;
		int timestep { 0 };
		int dimX, dimY;
		double resX, resY;
		int max_index;
		QVector<int> empty_label;
		if (const auto label = std::dynamic_pointer_cast<Data::LabelMask2D>(d->inputMap["InputMask"])) {
			timestep = label->GetTimeStep();
			dimX = label->GetSize().x;
			dimY = label->GetSize().y;
			resX = label->GetResolution().x;
			resY = label->GetResolution().y;
			max_index = label->GetMaxIndex();
			const auto labelMask = Data::DataConverter::ConvertToTCMask(label);
			QMap<int, bool> label_existance;
			for (int i = 0; i < labelMask->GetBlobIndexes().count(); i++) {
				const auto bidx = labelMask->GetBlobIndexes()[i];
				const auto blob = labelMask->GetBlob(bidx);
				label_existance[blob.GetCode()] = true;
			}
			for (auto i = 1; i < max_index; i++) {
				if (false == label_existance.contains(i)) {
					empty_label.push_back(i);
				}
			}
			labelView = converter.MaskToImageView(labelMask);
		} else {
			return {};
		}
		const auto analysis = std::make_shared<AnalysisMsr>();
		const auto labelMean = analysis->select(NativeMeasurements::intensityMean);
		const auto bboxOXMsr = analysis->select(NativeMeasurements::boundingBoxOX);
		const auto bboxOYMsr = analysis->select(NativeMeasurements::boundingBoxOY);
		const auto bboxDXMsr = analysis->select(NativeMeasurements::boundingBoxDX);
		const auto bboxDYMsr = analysis->select(NativeMeasurements::boundingBoxDY);
		try {
			LabelAnalysis labelAnalysisAlgo;
			labelAnalysisAlgo.setInputIntensityImage(labelView);
			labelAnalysisAlgo.setOutputAnalysis(analysis);
			labelAnalysisAlgo.setInputLabelImage(labelView);
			labelAnalysisAlgo.execute();

			auto globalTarget = resetImage(labelView, 0);
			auto* buffer = static_cast<unsigned short*>(globalTarget->buffer());
			for (auto labelIndex = 0; labelIndex < analysis->labelCount(); labelIndex++) {
				const auto ox = static_cast<int>(round((bboxOXMsr->value(labelIndex) + resX * dimX / 2) / resX));
				const auto oy = static_cast<int>(round((bboxOYMsr->value(labelIndex) + resY * dimY / 2) / resY));
				auto dx = static_cast<int>(bboxDXMsr->value(labelIndex) / resX) + 1;
				auto dy = static_cast<int>(bboxDYMsr->value(labelIndex) / resY) + 1;
				if (dx < 1)
					dx = 1;
				if (dy < 1)
					dy = 1;
				const auto labelValue = labelMean->value(labelIndex);

				auto croppedLabel = cropImage2d(labelView, { ox, oy }, { dx, dy });

				auto targetImage = resetImage(croppedLabel, 0);
				targetImage = convertImage(targetImage, ConvertImage::LABEL_16_BIT);

				auto thresholded = thresholding(croppedLabel, { 1,INT_MAX });

				auto relabel = labeling2d(thresholded, Labeling2d::LabelType::LABEL_16_BIT, Labeling2d::Neighborhood::CONNECTIVITY_8);

				auto labelStat = intensityStatistics(relabel, IntensityStatistics::MIN_MAX, { 0, 1 });

				if (labelStat->maximum() < 1) {
					continue;
				}

				//paste first label using original label value
				auto firstThresh = thresholding(relabel, { 1, 1.5 });
				targetImage = addObjectToLabel(firstThresh, targetImage, labelValue);
				for (auto i = 1; i < labelStat->maximum(); i++) {
					//past additional label using remaining label value (global)
					double value = i + 1;
					auto sepThresh = thresholding(relabel, { value, value + 0.5 });
					auto targetValue = 0;
					if (false == empty_label.isEmpty()) {
						targetValue = empty_label.front();
						empty_label.pop_front();
					} else {
						max_index++;
						targetValue = max_index;
					}
					targetImage = addObjectToLabel(sepThresh, targetImage, targetValue);
				}

				auto* sourceBuffer = static_cast<unsigned short*>(targetImage->buffer());
				for (auto j = oy; j < oy + targetImage->shape()[1]; j++) {
					for (auto i = ox; i < ox + targetImage->shape()[0]; i++) {
						const auto originIdx = j * dimX + i;
						const auto sourceIdx = (j - oy) * targetImage->shape()[0] + (i - ox);
						*(buffer + originIdx) = *(sourceBuffer + sourceIdx);
					}
				}
			}
			globalTarget = convertImage(globalTarget, ConvertImage::LABEL_16_BIT);

			int dim[3] = { dimX, dimY, 1 };
			double res[3] = { resX, resY, 1 };
			auto result = converter.ArrToLabelMask(static_cast<unsigned short*>(globalTarget->buffer()), dim, res);
			result->SetTimeStep(timestep);
			const auto output = Data::DataConverter::ConvertToMaskData<Data::LabelMask2D>(result);
			return { { "OutputMask", output } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return {};
	}

	auto ReassignLabels2d::Abort() -> void { }
}
