#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include "Labeling2d.h"

#include <ImageDev/Labeling2d.h>

namespace CellAnalyzer::Processor::Labeling {
	struct Labeling2d::Impl {
		const QMap<int, QString> paramNeighborhood {
			{ 0, "4-neighborhood" },
			{ 1, "8-neighborhood" }
		};

		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	Labeling2d::Labeling2d() : d { std::make_unique<Impl>() } { }

	Labeling2d::~Labeling2d() { }

	auto Labeling2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Neighborhood") {
			attribute->SetAttrModel(QStringList(d->paramNeighborhood.values()));
		}
	}

	auto Labeling2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto Labeling2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto Labeling2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto Labeling2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto Labeling2d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/labeling/TC.Algorithm.Labeling.ConnectedComponent.2D.dll";

		// create an input data
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			timestep = mask->GetTimeStep();
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		// set algorithm parameters and input data
		const auto neighborhood = d->attrMap["Neighborhood"]->GetAttrValue().toString();

		auto params = algorithm->Parameter();
		params->SetValue("LabelType", 1 /* 16bit label */);
		params->SetValue("Neighborhood", d->paramNeighborhood.key(neighborhood));

		algorithm->SetInput(0, inputMask);
		if (!algorithm->Execute()) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetTimeStep(timestep);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::LabelMask2D>(resultMask);
		if (result == nullptr) {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}
		return { { "OutputMask", result } };
	}

	auto Labeling2d::Abort() -> void { }
}
