#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>
#include <ImageDev/Data/NativeMeasurements.h>
#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "ReassignLabels3d.h"


namespace CellAnalyzer::Processor::Labeling {
	using namespace imagedev;
	using namespace iolink;

	struct ReassignLabels3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	ReassignLabels3d::ReassignLabels3d() : d { std::make_unique<Impl>() } { }

	ReassignLabels3d::~ReassignLabels3d() { }

	auto ReassignLabels3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
	}

	auto ReassignLabels3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto ReassignLabels3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto ReassignLabels3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto ReassignLabels3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto ReassignLabels3d::Process() -> DataMap {
		std::shared_ptr<ImageView> labelView { nullptr };
		TCDataConverter converter;
		int timestep { 0 };
		int dimX, dimY, dimZ;
		double resX, resY, resZ;
		int max_index;
		float offset { 0 };
		QVector<int> empty_label;
		if (const auto label = std::dynamic_pointer_cast<Data::LabelMask3D>(d->inputMap["InputMask"])) {
			timestep = label->GetTimeStep();
			dimX = label->GetSize().x;
			dimY = label->GetSize().y;
			dimZ = label->GetSize().z;
			resX = label->GetResolution().x;
			resY = label->GetResolution().y;
			resZ = label->GetResolution().z;
			offset = label->GetZOffset();
			max_index = label->GetMaxIndex();
			const auto labelMask = Data::DataConverter::ConvertToTCMask(label);
			QMap<int, bool> label_existance;
			for (int i = 0; i < labelMask->GetBlobIndexes().count(); i++) {
				const auto bidx = labelMask->GetBlobIndexes()[i];
				const auto blob = labelMask->GetBlob(bidx);
				label_existance[blob.GetCode()] = true;
			}
			for (auto i = 1; i < max_index; i++) {
				if (false == label_existance.contains(i)) {
					empty_label.push_back(i);
				}
			}
			labelView = converter.MaskToImageView(labelMask);
		} else {
			return {};
		}
		const auto analysis = std::make_shared<AnalysisMsr>();
		const auto labelMean = analysis->select(NativeMeasurements::intensityMean);
		const auto bboxOXMsr = analysis->select(NativeMeasurements::boundingBoxOX);
		const auto bboxOYMsr = analysis->select(NativeMeasurements::boundingBoxOY);
		const auto bboxOZMsr = analysis->select(NativeMeasurements::boundingBoxOZ);
		const auto bboxDXMsr = analysis->select(NativeMeasurements::boundingBoxDX);
		const auto bboxDYMsr = analysis->select(NativeMeasurements::boundingBoxDY);
		const auto bboxDZMsr = analysis->select(NativeMeasurements::boundingBoxDZ);
		try {
			LabelAnalysis labelAnalysisAlgo;
			labelAnalysisAlgo.setInputIntensityImage(labelView);
			labelAnalysisAlgo.setOutputAnalysis(analysis);
			labelAnalysisAlgo.setInputLabelImage(labelView);
			labelAnalysisAlgo.execute();
			auto globalTarget = resetImage(labelView, 0);
			auto* buffer = static_cast<unsigned short*>(globalTarget->buffer());
			for (auto labelIndex = 0; labelIndex < analysis->labelCount(); labelIndex++) {
				const auto ox = static_cast<int>(round((bboxOXMsr->value(labelIndex) + resX * dimX / 2) / resX));
				const auto oy = static_cast<int>(round((bboxOYMsr->value(labelIndex) + resY * dimY / 2) / resY));
				const auto oz = static_cast<int>(round((bboxOZMsr->value(labelIndex) + resZ * dimZ / 2) / resZ));
				auto dx = static_cast<int>(bboxDXMsr->value(labelIndex) / resX) + 1;
				auto dy = static_cast<int>(bboxDYMsr->value(labelIndex) / resY) + 1;
				auto dz = static_cast<int>(bboxDZMsr->value(labelIndex) / resZ) + 1;
				if (dx < 1)
					dx = 1;
				if (dy < 1)
					dy = 1;
				if (dz < 1)
					dz = 1;
				const auto labelValue = labelMean->value(labelIndex);

				auto croppedLabel = cropImage3d(labelView, { ox, oy, oz }, { dx, dy, dz });

				auto targetImage = resetImage(croppedLabel, 0);
				targetImage = convertImage(targetImage, ConvertImage::LABEL_16_BIT);

				auto thresholded = thresholding(croppedLabel, { 1,INT_MAX });

				auto relabel = labeling3d(thresholded, Labeling3d::LabelType::LABEL_16_BIT, Labeling3d::Neighborhood::CONNECTIVITY_26);

				auto labelStat = intensityStatistics(relabel, IntensityStatistics::MIN_MAX, { 0, 1 });

				if (labelStat->maximum() < 1) {
					continue;
				}

				//paste first label using original label value
				auto firstThresh = thresholding(relabel, { 1, 1.5 });
				targetImage = addObjectToLabel(firstThresh, targetImage, labelValue);
				for (auto i = 1; i < labelStat->maximum(); i++) {
					//past additional label using remaining label value (global)
					double value = i + 1;
					auto sepThresh = thresholding(relabel, { value, value + 0.5 });
					auto targetValue = 0;
					if (false == empty_label.isEmpty()) {
						targetValue = empty_label.front();
						empty_label.pop_front();
					} else {
						max_index++;
						targetValue = max_index;
					}
					targetImage = addObjectToLabel(sepThresh, targetImage, targetValue);
				}

				auto* sourceBuffer = static_cast<unsigned short*>(targetImage->buffer());
				for (auto k = oz; k < oz + targetImage->shape()[2]; k++) {
					for (auto j = oy; j < oy + targetImage->shape()[1]; j++) {
						for (auto i = ox; i < ox + targetImage->shape()[0]; i++) {
							const auto originIdx = k * dimX * dimY + j * dimX + i;
							const auto sourceIdx = (k - oz) * targetImage->shape()[0] * targetImage->shape()[1] + (j - oy) * targetImage->shape()[0] + (i - ox);
							*(buffer + originIdx) = *(sourceBuffer + sourceIdx);
						}
					}
				}
			}
			globalTarget = convertImage(globalTarget, ConvertImage::LABEL_16_BIT);

			int dim[3] = { dimX, dimY, dimZ };
			double res[3] = { resX, resY, resZ };
			auto result = converter.ArrToLabelMask(static_cast<unsigned short*>(globalTarget->buffer()), dim, res);
			result->SetOffset(offset);
			result->SetTimeStep(timestep);
			const auto output = Data::DataConverter::ConvertToMaskData<Data::LabelMask3D>(result);
			return { { "OutputMask", output } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return {};
	}

	auto ReassignLabels3d::Abort() -> void { }
}
