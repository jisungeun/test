#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include "ReorderLabels2d.h"

namespace CellAnalyzer::Processor::Labeling {
	struct ReorderLabels2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	ReorderLabels2d::ReorderLabels2d() : d { std::make_unique<Impl>() } { }

	ReorderLabels2d::~ReorderLabels2d() { }

	auto ReorderLabels2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
	}

	auto ReorderLabels2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto ReorderLabels2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto ReorderLabels2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto ReorderLabels2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto ReorderLabels2d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/labeling/TC.Algorithm.Labeling.Relabeling.dll";

		// create an input data
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		if (const auto mask = std::dynamic_pointer_cast<Data::LabelMask2D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			timestep = mask->GetTimeStep();
		} else {
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		// set input data
		algorithm->SetInput(0, inputMask);
		if (!algorithm->Execute()) {
			return {};
		}

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetTimeStep(timestep);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::LabelMask2D>(resultMask);
		if (result == nullptr) {
			return {};
		}

		return { { "OutputMask", result } };
	}

	auto ReorderLabels2d::Abort() -> void { }
}
