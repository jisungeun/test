#include <QCoreApplication>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include "ReorderLabels3d.h"


namespace CellAnalyzer::Processor::Labeling {
	struct ReorderLabels3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	ReorderLabels3d::ReorderLabels3d() : d { std::make_unique<Impl>() } { }

	ReorderLabels3d::~ReorderLabels3d() { }

	auto ReorderLabels3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
	}

	auto ReorderLabels3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto ReorderLabels3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto ReorderLabels3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto ReorderLabels3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto ReorderLabels3d::Process() -> DataMap {
		const auto dllPath = qApp->applicationDirPath() + "/algorithms/labeling/TC.Algorithm.Labeling.Relabeling.dll";

		// create an input data
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		float offset { 0 };
		if (const auto mask = std::dynamic_pointer_cast<Data::LabelMask3D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			timestep = mask->GetTimeStep();
			offset = mask->GetZOffset();
		} else {
			return {};
		}

		// load algorithm plugin
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);
		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr) {
			return {};
		}

		// set input data
		algorithm->SetInput(0, inputMask);
		if (!algorithm->Execute()) {
			return {};
		}

		const auto resultMask = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		resultMask->SetOffset(offset);
		resultMask->SetTimeStep(timestep);
		const auto result = Data::DataConverter::ConvertToMaskData<Data::LabelMask3D>(resultMask);
		if (result == nullptr) {
			return {};
		}

		return { { "OutputMask", result } };
	}

	auto ReorderLabels3d::Abort() -> void { }
}
