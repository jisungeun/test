#include "ProcessorAttr.h"

namespace CellAnalyzer::Processor::Service {
	struct ProcessorAttr::Impl {
		ProcessorAttrModel model;
		ProcessorAttrValue value;
		ProcessorAttrStyle state = ProcessorAttrStyle::Visible;
	};

	ProcessorAttr::ProcessorAttr() : IProcessorAttr(), d(new Impl) {}
	
	ProcessorAttr::~ProcessorAttr() = default;

	auto ProcessorAttr::GetAttrModel() const -> ProcessorAttrModel {
		return d->model;
	}

	auto ProcessorAttr::GetValue() const -> ProcessorAttrValue {
		return d->value;
	}

	auto ProcessorAttr::GetStyle() const -> ProcessorAttrStyle {
		return d->state;
	}

	auto ProcessorAttr::SetModel(const ProcessorAttrModel& model) -> void {
		d->model = model;
	}

	auto ProcessorAttr::SetValue(const ProcessorAttrValue& value) -> void {
		d->value = value;
	}

	auto ProcessorAttr::SetStyle(ProcessorAttrStyle state) -> void {
		d->state = state;
	}
}
