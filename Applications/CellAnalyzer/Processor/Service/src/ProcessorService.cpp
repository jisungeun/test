#include "PluginCollection.h"

#include "ProcessorService.h"
#include "ProcessType.h"

namespace CellAnalyzer::Processor::Service {
	struct ProcessorService::Impl {
		std::shared_ptr<Tomocube::IServiceProvider> provider = nullptr;

		QList<Pipeline::ProcessTypePtr> infoList;
	};

	ProcessorService::ProcessorService() : IProcessorService(), d(new Impl) {}

	ProcessorService::~ProcessorService() = default;

	auto ProcessorService::Start() -> std::optional<Error> {
		Tomocube::PluginInjection::PluginCollection collection;
		const auto map = GetLibMap();

		for (const auto& category : map.keys()) {
			for (const auto& process : map[category])
				collection.AddScoped<IProcessorFactory>(GetLibPath(category, process));
		}

		d->provider = collection.BuildProvider();

		for (const auto& m : d->provider->GetMetadata<IProcessorFactory>()) {
			if (const auto type = std::make_shared<Pipeline::Service::ProcessType>(m); type->IsValid())
				d->infoList.push_back(type);
		}

		return std::nullopt;
	}

	auto ProcessorService::Stop() -> void {}

	auto ProcessorService::GetTypeList() const -> QStringList {
		QStringList list;

		for (const auto& i : d->infoList)
			list.push_back(i->GetID());

		return list;
	}

	auto ProcessorService::GetTypeInfo(const QString& id) const -> ProcessorTypePtr {
		for (const auto& i : d->infoList) {
			if (i->GetID() == id)
				return i;
		}

		return {};
	}

	auto ProcessorService::CreateProcessor(const QString& id) const -> ProcessorPtr {
		for (int i = 0; i < d->infoList.count(); i++) {
			if (d->infoList[i]->GetID() == id) {
				if (const auto factory = d->provider->GetService<IProcessorFactory>(i)) {
					return factory->CreateInstance();
				}
			}
		}

		return {};
	}
}
