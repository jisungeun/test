#pragma once

#include "IProcessorAttr.h"

#include "CellAnalyzer.Processor.ServiceExport.h"

namespace CellAnalyzer::Processor::Service {
	class CellAnalyzer_Processor_Service_API ProcessorAttr final : public IProcessorAttr {
	public:
		ProcessorAttr();
		~ProcessorAttr() override;

		auto GetAttrModel() const -> ProcessorAttrModel override;
		auto GetValue() const -> ProcessorAttrValue override;
		auto GetStyle() const -> ProcessorAttrStyle override;

		auto SetModel(const ProcessorAttrModel& model) -> void override;
		auto SetValue(const ProcessorAttrValue& value) -> void override;
		auto SetStyle(ProcessorAttrStyle state) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
