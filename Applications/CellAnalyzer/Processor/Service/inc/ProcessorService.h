#pragma once

#include "IAppModule.h"

#include "IProcessorService.h"

#include "CellAnalyzer.Processor.ServiceExport.h"

namespace CellAnalyzer::Processor::Service {
	class CellAnalyzer_Processor_Service_API ProcessorService final : public IProcessorService, public IAppModule {
	public:
		explicit ProcessorService();
		~ProcessorService() override;

		auto Start() -> std::optional<Error> override;
		auto Stop() -> void override;

		auto GetTypeList() const -> QStringList override;
		auto GetTypeInfo(const QString& id) const -> ProcessorTypePtr override;

		auto CreateProcessor(const QString& id) const -> ProcessorPtr override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
