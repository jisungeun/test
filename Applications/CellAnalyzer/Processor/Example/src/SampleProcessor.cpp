#include <QMutexLocker>
#include <QWaitCondition>

#include "SampleProcessor.h"

#include "HT2D.h"

namespace CellAnalyzer::Processor::Filtering {
	struct SampleProcessor::Impl {
		QWaitCondition waiter;
		QMutex mutex;

		DataAttrGetterPtr getter = nullptr;
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	SampleProcessor::SampleProcessor() : IProcessor(), d(new Impl) {}

	SampleProcessor::~SampleProcessor() = default;

	auto SampleProcessor::SetGetter(const DataAttrGetterPtr& getter) -> void {
		d->getter = getter;

		// Data name can be retreived by this getter.
		// d->getter->GetName(data);
	}

	auto SampleProcessor::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		// It is called when attribute is created according to its TypeInfo.
		// Update the attribute model for each attribute ID.
		// Save the attribute so that IProcessor::GetAttr() works.

		d->attrMap[attrID] = attribute;

		if (attrID == "Integer")
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 1 } });
		else if (attrID == "IntegerSlider")
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 100 }, { "Step", 1 } });
		else if (attrID == "Double")
			attribute->SetAttrModel(QVariantMap { { "Min", 0.0 }, { "Max", 1.0 }, { "Step", 0.1 }, { "Decimals", 2 } });
		else if (attrID == "DoubleSlider")
			attribute->SetAttrModel(QVariantMap { { "Min", 0.0 }, { "Max", 1.0 }, { "Step", 0.1 }, { "Decimals", 2 } });
		else if (attrID == "String")
			attribute->SetAttrModel(QVariantMap { { "Placeholder", "This is string" }, { "MaxLength", 100 } });
		else if (attrID == "StringList")
			attribute->SetAttrModel(QStringList { "Option 1", "Option 2", "Option 3" });
		else if (attrID == "StringCombo")
			attribute->SetAttrModel(QStringList { "Option 1", "Option 2", "Option 3" });
		else if (attrID == "StringRadio")
			attribute->SetAttrModel(QStringList { "Option 1", "Option 2", "Option 3" });
		else if (attrID == "File")
			attribute->SetAttrModel(QVariantMap { { "Filter", "*.TCF" }, { "Path", "C:\\" }, { "Title", "Find TCF" } });
		else if (attrID == "Directory")
			attribute->SetAttrModel(QVariantMap { { "Path", "C:\\" }, { "Title", "Find TCF" } });
	}

	auto SampleProcessor::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		// It is called when attribute value is updated.
		// Save the value if acceptable.
		// Update the others upon the changes.

		d->attrMap[attrID]->SetAttrValue(value);

		if (attrID == "Switch") {
			if (const auto state = value.toBool())
				d->attrMap["SwitchText"]->SetAttrStyle(ProcessorAttrStyle::Hidden);
			else
				d->attrMap["SwitchText"]->SetAttrStyle(ProcessorAttrStyle::Visible);
		}
	}

	auto SampleProcessor::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		// It is called when input data is updated.
		// Save the data if acceptable.

		d->inputMap[inputID] = data;
	}

	auto SampleProcessor::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		// No need to check ID is correct for TypeInfo

		return d->attrMap[attrID];
	}

	auto SampleProcessor::GetInputData(const QString& inputID) const -> DataPtr {
		// No need to check ID is correct for TypeInfo

		return d->inputMap[inputID];
	}

	auto SampleProcessor::Process() -> DataMap {
		// Create output data with attributes and input data.
		// Return empty DataMap if failed in processing.

		if (d->inputMap["HT2D"] != nullptr) {
			const auto name = d->getter->GetName(d->inputMap["HT2D"]);

			DataMap output;
			output["Output"] = d->inputMap["HT2D"];

			QMutexLocker locker(&d->mutex);
			d->waiter.wait(&d->mutex, 5000);

			return output;
		}

		return {};
	}

	auto SampleProcessor::Abort() -> void {
		// Optional
		// Abort processing ASAP.

		QMutexLocker locker(&d->mutex);
		d->waiter.notify_all();
	}
}
