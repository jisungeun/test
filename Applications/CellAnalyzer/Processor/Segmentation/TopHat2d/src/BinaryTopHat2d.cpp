#include <QCoreApplication>

#include "BinaryTopHat2d.h"

#include <DataConverter.h>
#include <iostream>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCImage.h>
#include <TCMask.h>


#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

namespace CellAnalyzer::Processor::Segmentation {
	using namespace imagedev;
	using namespace iolink;

	struct BinaryTopHat2d::Impl {
		const QMap<int, QString> paramLightness {
			{ 0, "Bright objects" },
			{ 1, "Dark objects" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	BinaryTopHat2d::BinaryTopHat2d() : d { std::make_unique<Impl>() } { }

	BinaryTopHat2d::~BinaryTopHat2d() { }

	auto BinaryTopHat2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		attribute->SetAttrStyle(ProcessorAttrStyle::Disabled);

		const auto id = attrID;
		if (id == "Kernel Radius") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 1000 }, { "Step", 1 } });
		} else if (id == "Object Lightness") {
			attribute->SetAttrModel(QStringList(d->paramLightness.values()));
		} else if (id == "UThreshold") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 10000 }, { "Step", 0.1 }, { "Decimals", 1 } });
		} else if (id == "LThreshold") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 10000 }, { "Step", 0.1 }, { "Decimals", 1 } });
		}
	}

	auto BinaryTopHat2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto BinaryTopHat2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;

		TCImage::Pointer image { nullptr };
		bool isFL = false;
		double min = 0, max = 0;
		if (const auto inputImage = std::dynamic_pointer_cast<Data::HT2D>(data))
			image = Data::DataConverter::ConvertToTCImage(inputImage);
		else if (const auto tcf = std::dynamic_pointer_cast<Data::FL2D>(data)) {
			image = Data::DataConverter::ConvertToTCImage(tcf);
			isFL = true;
		} else if (const auto floatImage = std::dynamic_pointer_cast<Data::Float2D>(data)) {
			const auto [imin, imax] = floatImage->GetRange();
			min = imin;
			max = imax;
			for (auto& prop : d->attrMap.values())
				prop->SetAttrStyle(ProcessorAttrStyle::Visible);
		}

		if (image) {
			for (auto& prop : d->attrMap.values())
				prop->SetAttrStyle(ProcessorAttrStyle::Visible);

			std::tie(min, max) = image->GetMinMax();
			if (false == isFL) {
				min /= 10000.;
				max /= 10000.;
			}
		}
	}

	auto BinaryTopHat2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto BinaryTopHat2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto BinaryTopHat2d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		float factor { 1 };
		TCDataConverter converter;
		int timestep { 0 };
		if (const auto tcf = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			timestep = tcf->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(tcf);
			const auto [imin, imax] = tcf->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY] = tcf->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = tcf->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			factor = 10000.0;
		} else if (const auto tcf = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			timestep = tcf->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(tcf);
			const auto [imin, imax] = tcf->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = tcf->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = tcf->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
		} else {
			return {};
		}

		const auto lthreshold = d->attrMap["LThreshold"]->GetAttrValue().toDouble();
		const auto uthreshold = d->attrMap["UThreshold"]->GetAttrValue().toDouble();
		if (lthreshold > uthreshold) {
			return {};
		}
		const auto kernelRadius = d->attrMap["Kernel Radius"]->GetAttrValue().toInt();
		const auto lightstring = d->attrMap["Object Lightness"]->GetAttrValue().toString();
		const auto lightness = static_cast<TopHat::ObjectLightness>(d->paramLightness.key(lightstring) + 1);

		try {
			const auto threshImage = topHat(refImageView, kernelRadius, lightness, { lthreshold, uthreshold });
			const auto binaryMask = convertImage(threshImage, ConvertImage::OutputType::UNSIGNED_INTEGER_16_BIT);
			int dim[3] { dimX, dimY, dimZ };

			const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(binaryMask->buffer()), dim, res);
			resultMask->SetTimeStep(timestep);
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask2D>(resultMask);
			if (result == nullptr) {
				return {};
			}

			return { { "OutputMask", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto BinaryTopHat2d::Abort() -> void { }
}
