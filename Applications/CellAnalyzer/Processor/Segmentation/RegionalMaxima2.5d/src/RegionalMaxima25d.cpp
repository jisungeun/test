#include <QCoreApplication>

#include "RegionalMaxima25d.h"

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCImage.h>
#include <TCMask.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

namespace CellAnalyzer::Processor::Segmentation {
	using namespace imagedev;
	using namespace iolink;

	struct RegionalMaxima25d::Impl {
		const QMap<int, QString> paramNeighborhood {
			{ 0, "6-neighborhood" },
			{ 1, "18-neighborhood" },
			{ 2, "26-neighborhood" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	RegionalMaxima25d::RegionalMaxima25d() : d { std::make_unique<Impl>() } { }

	RegionalMaxima25d::~RegionalMaxima25d() { }

	auto RegionalMaxima25d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		attribute->SetAttrStyle(ProcessorAttrStyle::Disabled);

		const auto id = attrID;
		if (id == "Neighborhood") {
			attribute->SetAttrModel(QStringList(d->paramNeighborhood.values()));
		}
	}

	auto RegionalMaxima25d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto RegionalMaxima25d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto RegionalMaxima25d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto RegionalMaxima25d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto RegionalMaxima25d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		float offset { 0 };
		double min, max;
		double res[3];
		double factor { 1 };
		int dimX, dimY, dimZ;
		int timestep { 0 };
		TCDataConverter converter;
		if (const auto tcf = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			timestep = tcf->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(tcf);
			const auto [imin, imax] = tcf->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = tcf->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = tcf->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
			factor = 10000.0;
		} else if (const auto tcf = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			timestep = tcf->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(tcf);
			const auto [imin, imax] = tcf->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = tcf->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = tcf->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = tcf->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
		} else {
			return {};
		}

		const auto neighborhood = d->attrMap["Neighborhood"]->GetAttrValue().toString();
		try {
			auto destMask = resetImage(refImageView, 0);
			destMask = convertImage(destMask, ConvertImage::BINARY);
			for (auto i = 0; i < dimZ; i++) {
				const auto sliced = getSliceFromVolume3d(refImageView, GetSliceFromVolume3d::Z_AXIS, i);
				const auto sliceMaxima = regionalMaxima(sliced, static_cast<RegionalMaxima::Neighborhood>(d->paramNeighborhood.key(neighborhood)));

				destMask = setSliceToVolume3d(destMask, sliceMaxima, SetSliceToVolume3d::Z_AXIS, i);
			}
			const auto binaryMask = convertImage(destMask, ConvertImage::OutputType::UNSIGNED_INTEGER_16_BIT);
			int dim[3] { dimX, dimY, dimZ };

			const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(binaryMask->buffer()), dim, res);
			resultMask->SetTimeStep(timestep);
			resultMask->SetOffset(offset);
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask3D>(resultMask);
			if (result == nullptr) {
				return {};
			}

			return { { "OutputMask", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto RegionalMaxima25d::Abort() -> void { }
}
