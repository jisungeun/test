#include <QCoreApplication>

#include "RegionalMaxima2d.h"

#include <DataConverter.h>
#include <iostream>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCImage.h>
#include <TCMask.h>


#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

namespace CellAnalyzer::Processor::Segmentation {
	using namespace imagedev;
	using namespace iolink;

	struct RegionalMaxima2d::Impl {
		const QMap<int, QString> paramNeighborhood {
			{ 0, "6-neighborhood" },
			{ 1, "18-neighborhood" },
			{ 2, "26-neighborhood" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	RegionalMaxima2d::RegionalMaxima2d() : d { std::make_unique<Impl>() } { }

	RegionalMaxima2d::~RegionalMaxima2d() { }

	auto RegionalMaxima2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		attribute->SetAttrStyle(ProcessorAttrStyle::Disabled);

		const auto id = attrID;
		if (id == "Neighborhood") {
			attribute->SetAttrModel(QStringList(d->paramNeighborhood.values()));
		}
	}

	auto RegionalMaxima2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto RegionalMaxima2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto RegionalMaxima2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto RegionalMaxima2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto RegionalMaxima2d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		float factor { 1 };
		TCDataConverter converter;
		int timestep { 0 };
		if (const auto tcf = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			timestep = tcf->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(tcf);
			const auto [imin, imax] = tcf->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY] = tcf->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = tcf->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			factor = 10000.0;
		} else if (const auto tcf = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			timestep = tcf->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(tcf);
			const auto [imin, imax] = tcf->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = tcf->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = tcf->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
		} else {
			return {};
		}

		const auto neighborhood = d->attrMap["Neighborhood"]->GetAttrValue().toString();

		try {
			const auto maxima = regionalMaxima(refImageView, static_cast<RegionalMaxima::Neighborhood>(d->paramNeighborhood.key(neighborhood)));
			const auto binaryMask = convertImage(maxima, ConvertImage::UNSIGNED_INTEGER_16_BIT);
			int dim[3] { dimX, dimY, dimZ };
			const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(binaryMask->buffer()), dim, res);
			resultMask->SetTimeStep(timestep);
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask2D>(resultMask);
			if (result == nullptr) {
				return {};
			}

			return { { "OutputMask", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto RegionalMaxima2d::Abort() -> void { }
}
