#include <QCoreApplication>

#include <variant>
#include <optional>

#include "AutoThreshold2d.h"

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCImage.h>
#include <TCMask.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>
#include <ImageDev/AutoThresholdingBright.h>

namespace CellAnalyzer::Processor::Segmentation {
    using namespace imagedev;
    using namespace iolink;

    struct AutoThreshold2d::Impl {
        using DataVariant = std::variant<
            std::shared_ptr<Data::HT2D>,
            std::shared_ptr<Data::FL2D>,
            std::shared_ptr<Data::Float2D>
        >;
        static auto ConvertToVariant(const DataPtr& base)->std::optional<DataVariant>;

        template<typename... Ts>
        struct Overload : Ts... { using Ts::operator()...; };
        template<typename... Ts> Overload(Ts...)->Overload<Ts...>;

        QStringList foregroundOption{
            "Bright", "Dark"
        };
        QMap<QString, AutoThresholdingValue::ThresholdCriterion> thresholdingAlgorithm{
            {"Entropy", AutoThresholdingValue::ThresholdCriterion::ENTROPY},
            {"Factorization", AutoThresholdingValue::ThresholdCriterion::FACTORISATION},
            {"Moments", AutoThresholdingValue::ThresholdCriterion::MOMENTS},
            {"ISODATA", AutoThresholdingValue::ThresholdCriterion::ISODATA},
        };

        QMap<QString, DataPtr> inputMap;
        QMap<QString, ProcessorAttrPtr> attrMap;
    };

    auto AutoThreshold2d::Impl::ConvertToVariant(const DataPtr& base) -> std::optional<DataVariant> {
        if (auto ht = std::dynamic_pointer_cast<Data::HT2D>(base)) return ht;
        if (auto fl = std::dynamic_pointer_cast<Data::FL2D>(base)) return fl;
        if (auto f = std::dynamic_pointer_cast<Data::Float2D>(base)) return f;
        return std::nullopt;
    }

    AutoThreshold2d::AutoThreshold2d() : IProcessor(), d{ std::make_unique<Impl>() } {
    }

    AutoThreshold2d::~AutoThreshold2d() = default;

    auto AutoThreshold2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
        d->attrMap[attrID] = attribute;
        const auto id = attrID;

        if (id == "Foreground") {
            attribute->SetAttrModel(d->foregroundOption);
        }
        else if (id == "Algorithm") {
            attribute->SetAttrModel(QStringList(d->thresholdingAlgorithm.keys()));
        } else if (id == "Threshold offset") {
            attribute->SetAttrModel(QVariantMap{
                {"Min", std::numeric_limits<int>::min()},
                {"Max", std::numeric_limits<int>::max()},
                {"Step", 0.001},
                {"Decimals", 3}
            });
            attribute->SetAttrValue(0);
        } else if (id == "Threshold scale") {
            attribute->SetAttrModel(QVariantMap{
                {"Min", std::numeric_limits<int>::min()},
                {"Max", std::numeric_limits<int>::max()},
                {"Step", 0.001},
                {"Decimals", 3}
            });
            attribute->SetAttrValue(1);
        }

    }

    auto AutoThreshold2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
        if (false == d->attrMap.contains(attrID)) {
            return;
        }
        d->attrMap[attrID]->SetAttrValue(value);
    }

    auto AutoThreshold2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
        d->inputMap[inputID] = data;
    }

    auto AutoThreshold2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
        return d->attrMap[attrID];
    }

    auto AutoThreshold2d::GetInputData(const QString& inputID) const -> DataPtr {
        return d->inputMap[inputID];
    }

    auto AutoThreshold2d::Process() -> DataMap {
        if (false == d->inputMap.contains("InputImage")) {
            return {};
        }

        const auto isHT = nullptr != std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"]);

        const auto inputImageOpt = Impl::ConvertToVariant(d->inputMap["InputImage"]);
        if (false == inputImageOpt.has_value()) {
            return {};
        }

        const auto& inputImageVar = inputImageOpt.value();
        const auto [dimX, dimY] = std::visit(Impl::Overload{
            [](const std::shared_ptr<Data::HT2D>& img) {return img->GetSize(); },
            [](const std::shared_ptr<Data::FL2D>& img) {return img->GetSize(); },
            [](const std::shared_ptr<Data::Float2D>& img) {return img->GetSize(); }
            }, inputImageVar);
        int dim[3] = { dimX, dimY, 1 };
        const auto [resX, resY] = std::visit(Impl::Overload{
            [](const std::shared_ptr<Data::HT2D>& img) {return img->GetResolution(); },
            [](const std::shared_ptr<Data::FL2D>& img) {return img->GetResolution(); },
            [](const std::shared_ptr<Data::Float2D>& img) {return img->GetResolution(); }
            }, inputImageVar);
        double res[3]{ resX, resY, 0 };
        const auto timeStep = std::visit(Impl::Overload{
            [](const std::shared_ptr<Data::HT2D>& img) {return img->GetTimeStep(); },
            [](const std::shared_ptr<Data::FL2D>& img) {return img->GetTimeStep(); },
            [](const std::shared_ptr<Data::Float2D>& img) {return img->GetTimeStep(); }
            }, inputImageVar);
        auto converter = TCDataConverter{};
        const auto refImageViewVisitor = Impl::Overload{
            [&converter](const std::shared_ptr<Data::HT2D>& img) {return converter.ImageToImageView(Data::DataConverter::ConvertToTCImage(img)); },
            [&converter](const std::shared_ptr<Data::FL2D>& img) {return converter.ImageToImageView(Data::DataConverter::ConvertToTCImage(img)); },
            [&converter, &dim, &res](const std::shared_ptr<Data::Float2D>& img) {
                return converter.FloatArrToImageView(static_cast<float*>(img->GetData()), dim[0], dim[1], dim[2], res);
            }
        };
        const auto refImageView = std::visit(refImageViewVisitor, inputImageVar);

        const auto foreground = d->attrMap["Foreground"]->GetAttrValue().toString();
        const auto algorithm = d->attrMap["Algorithm"]->GetAttrValue().toString();
        const auto thresholdOffset = d->attrMap["Threshold offset"]->GetAttrValue().toDouble() * (isHT ? 10000.0 : 1.0);
        const auto thresholdScale = d->attrMap["Threshold scale"]->GetAttrValue().toDouble();

        try {
            const auto computedThreshold = autoThresholdingValue(refImageView,
                AutoThresholdingValue::MIN_MAX,
                {},
                d->thresholdingAlgorithm[algorithm])->threshold();

            const auto appliedThreshold = computedThreshold * thresholdScale + thresholdOffset;

            constexpr auto thresholdMin = std::numeric_limits<int>::min();
            constexpr auto thresholdMax = std::numeric_limits<int>::max();
            const auto thresholdRange = foreground == "Bright"
                ? Vector2d{appliedThreshold, thresholdMax} : Vector2d{thresholdMin, appliedThreshold};

            const auto outputImage = thresholding(refImageView, thresholdRange);
            const auto binaryMask = convertImage(outputImage, ConvertImage::OutputType::UNSIGNED_INTEGER_16_BIT);
            const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(binaryMask->buffer()), dim, res);
            resultMask->SetTimeStep(timeStep);
            const auto result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask2D>(resultMask);

            if (result == nullptr) {
                return {};
            }

            return { {"OutputMask", result} };
        }
        catch (Exception& e) {
            std::cout << e.what() << '\n';
        }
        catch (...) {
            std::cout << "Unknown exception\n";
        }

        return {};
    }

    auto AutoThreshold2d::Abort() -> void {
    }
}
