#pragma once

#include <IProcessor.h>

namespace CellAnalyzer::Processor::Segmentation {
	class Label2Binary3d final : public IProcessor {
	public:
		Label2Binary3d();
		~Label2Binary3d() override;

		auto SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void override;
		auto SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void override;
		auto SetInputData(const QString& inputID, const DataPtr& data) -> void override;

		auto GetAttr(const QString& attrID) const -> ProcessorAttrPtr override;
		auto GetInputData(const QString& inputID) const -> DataPtr override;

		auto Process() -> DataMap override;
		auto Abort() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	DECLARE_PROCESSOR_START(Label2Binary3d) {
		Q_OBJECT
		DECLARE_PROCESSOR_END(Label2Binary3d, Segmentation, DEFAULT_PATH)
	};
}
