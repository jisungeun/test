#include <QCoreApplication>

#include "Watershed2d.h"

#include <DataConverter.h>
#include <iostream>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCImage.h>
#include <TCMask.h>


#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

namespace CellAnalyzer::Processor::Segmentation {
	using namespace imagedev;
	using namespace iolink;

	struct Watershed2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	Watershed2d::Watershed2d() : d { std::make_unique<Impl>() } { }

	Watershed2d::~Watershed2d() { }

	auto Watershed2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;

		const auto id = attrID;
		if (id == "Object Filter Size") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0.001 }, { "Max", 1000 }, { "Step", 0.001 }, { "Decimals", 3 } });
		}
	}

	auto Watershed2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto Watershed2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto Watershed2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto Watershed2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto Watershed2d::Process() -> DataMap {
		TCMask::Pointer inputLabel { nullptr };
		TCMask::Pointer refMask { nullptr };
		int timestep { 0 };
		if (const auto label = std::dynamic_pointer_cast<Data::LabelMask2D>(d->inputMap["InputLabel"])) {
			inputLabel = Data::DataConverter::ConvertToTCMask(label);
			timestep = label->GetTimeStep();
		} else {
			return {};
		}
		if (const auto binary = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["ReferenceBinary"])) {
			refMask = Data::DataConverter::ConvertToTCMask(binary);
		} else {
			return {};
		}

		auto objectSize = d->attrMap["Object Filter Size"]->GetAttrValue().toDouble();
		const auto [resX, resY, resZ] = refMask->GetResolution();
		if (qFuzzyIsNull(resX) || qFuzzyIsNull(resY)) {
			return {};
		}
		objectSize /= (resX * resY);

		TCDataConverter converter;

		auto inputLabelView = converter.MaskToImageView(inputLabel);
		auto inputBinary = converter.MaskToImageView(refMask, TCDataConverter::MaskType::Binary);

		try {
			auto AreSame = [&](double a, double b) {
				return fabs(a - b) < 0.0001;
			};

			const auto refOrigin = inputLabelView->properties()->calibration().origin();
			const auto maskOrigin = inputBinary->properties()->calibration().origin();
			const auto refSpacing = inputLabelView->properties()->calibration().spacing();
			const auto maskSpacing = inputBinary->properties()->calibration().spacing();

			auto originDiff = false;
			auto spacingDiff = false;
			for (auto i = 0; i < 2; i++) {
				if (false == AreSame(refOrigin[i], maskOrigin[i])) {
					originDiff = true;
				}
				if (false == AreSame(refSpacing[i], maskSpacing[i])) {
					spacingDiff = true;
				}
			}
			if (inputLabelView->shape() != inputBinary->shape() || originDiff || spacingDiff) {
				inputBinary = TCDataConverter::MapMaskGeometry(inputLabelView, inputBinary);
			}

			//Step1: Apply size Filter to input binary image			
			const auto filteredBinary = removeSmallSpots(inputBinary, objectSize);
			//Step2: Create label from filtered input binary image
			const auto filteredLabel = labeling2d(filteredBinary, Labeling2d::LABEL_16_BIT
												, Labeling2d::CONNECTIVITY_8);
			const auto labelStat = objectCount(filteredLabel);
			const auto labelCnt = labelStat.outputMeasurement->count();
			if (labelCnt < 1) {
				return {};
			}

			//Step3: if label region in Step2 does not contains label from InputLabel, append it.
			auto newLabel = inputLabel->GetBlobIndexes().count();
			for (auto i = 1; i < static_cast<int>(labelCnt) + 1; i++) {
				auto labelBinary = thresholding(filteredLabel, { static_cast<double>(i), i + 0.5 });

				const auto maskingBySingleLabel = maskImage(inputLabelView, labelBinary);
				const auto maskingSum = intensityIntegral2d(maskingBySingleLabel);
				if (maskingSum->intensityIntegral() < 1) {
					newLabel++;
					auto valueImage = resetImage(inputLabelView, newLabel);
					auto newlabelSrc = maskImage(valueImage, labelBinary);
					inputLabelView = arithmeticOperationWithImage(inputLabelView, newlabelSrc, ArithmeticOperationWithImage::ADD);
				}
			}
			inputLabelView = maskImage(inputLabelView, filteredBinary);
			inputLabelView = convertImage(inputLabelView, ConvertImage::LABEL_16_BIT);
			inputLabelView = reorderLabels(inputLabelView);

			//Step4: perform watershed			
			const auto binaryInput = convertImage(filteredBinary, ConvertImage::BINARY);
			auto distanceMap = distanceMap2d(binaryInput, DistanceMap2d::MappingMode::INSIDE, DistanceMap2d::BorderCondition::ZERO, 1, 1.4142135623730951, DistanceMap2d::OutputType::FLOAT_32_BIT);
			distanceMap = arithmeticOperationWithValue(distanceMap, -1, ArithmeticOperationWithValue::MULTIPLY);
			const auto watersheded = markerBasedWatershed2d(distanceMap, inputLabelView, MarkerBasedWatershed2d::FAST, MarkerBasedWatershed2d::OutputType::CONTIGUOUS_REGIONS, MarkerBasedWatershed2d::CONNECTIVITY_8);

			//Step5: make region growed label based on wateshed region separation
			const auto result = maskImage(watersheded, binaryInput);

			const auto [x, y, z] = inputLabel->GetSize();
			const auto [rx, ry, rz] = inputLabel->GetResolution();
			int dim[3] { x, y, z };
			double res[3] { rx, ry, rz };
			const auto resultMask = converter.ArrToLabelMask(static_cast<unsigned short*>(result->buffer()), dim, res);
			resultMask->SetTimeStep(timestep);
			const auto output = Data::DataConverter::ConvertToMaskData<Data::LabelMask2D>(resultMask);
			return { { "OutputMask", output } };
		} catch (imagedev::Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return {};
	}

	auto Watershed2d::Abort() -> void { }
}
