#include <QCoreApplication>

#include "ULThreshold2d.h"

#include <DataConverter.h>
#include <iostream>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCImage.h>
#include <TCMask.h>


#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

namespace CellAnalyzer::Processor::Segmentation {
	using namespace imagedev;
	using namespace iolink;

	struct ULThreshold2d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
		double currentMin { INT_MIN };
		double currentMax { INT_MAX };
	};

	ULThreshold2d::ULThreshold2d() : IProcessor(), d { std::make_unique<Impl>() } { }

	ULThreshold2d::~ULThreshold2d() { }

	auto ULThreshold2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		attribute->SetAttrStyle(ProcessorAttrStyle::Disabled);

		const auto id = attrID;
		if (id == "LThreshold" || id == "UThreshold") {
			attribute->SetAttrModel(QVariantMap { { "Min", INT_MIN }, { "Max", INT_MAX }, { "Step", 0.001 }, { "Decimals", 3 } });
		}
	}

	auto ULThreshold2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
		if (attrID == "LThreshold") {
			d->currentMin = value.toDouble();
		}
		if (attrID == "UThreshold") {
			d->currentMax = value.toDouble();
		}
	}

	auto ULThreshold2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.00001;
		};
		if (AreSame(d->currentMin, 0) && AreSame(d->currentMax, 0)) {
			d->currentMin = INT_MIN;
			d->currentMax = INT_MAX;
		}
		d->inputMap[inputID] = data;

		TCImage::Pointer image { nullptr };
		bool isFL = false;
		bool isFloat = false;
		double min = INT_MIN, max = INT_MAX;
		if (const auto inputImage = std::dynamic_pointer_cast<Data::HT2D>(data)) {
			image = Data::DataConverter::ConvertToTCImage(inputImage);
		} else if (const auto flImage = std::dynamic_pointer_cast<Data::FL2D>(data)) {
			isFL = true;
			image = Data::DataConverter::ConvertToTCImage(flImage);
		} else if (const auto floatImage = std::dynamic_pointer_cast<Data::Float2D>(data)) {
			const auto [imin, imax] = floatImage->GetRange();
			min = imin;
			max = imax;
			isFloat = true;
			for (auto& prop : d->attrMap.values())
				prop->SetAttrStyle(ProcessorAttrStyle::Visible);
		}
		if (image) {
			for (auto& prop : d->attrMap.values())
				prop->SetAttrStyle(ProcessorAttrStyle::Visible);

			std::tie(min, max) = image->GetMinMax();
			if (false == isFL) {
				min /= 10000.;
				max /= 10000.;
			}
		}

		if (d->attrMap.contains("LThreshold") && (image || isFloat)) {
			if (isFL) {
				d->attrMap["LThreshold"]->SetAttrModel(QVariantMap { { "Min", min }, { "Max", max }, { "Step", 1.0 }, { "Decimals", 0 } });
				const auto enabled = !(d->attrMap["Disable Lower Threshold"]->GetAttrValue().toBool());
				d->attrMap["LThreshold"]->SetAttrStyle(enabled ? ProcessorAttrStyle::Visible : ProcessorAttrStyle::Disabled);
			} else {
				d->attrMap["LThreshold"]->SetAttrModel(QVariantMap { { "Min", min }, { "Max", max }, { "Step", 0.0001 }, { "Decimals", 4 } });
				const auto enabled = !(d->attrMap["Disable Lower Threshold"]->GetAttrValue().toBool());
				d->attrMap["LThreshold"]->SetAttrStyle(enabled ? ProcessorAttrStyle::Visible : ProcessorAttrStyle::Disabled);
			}
			const auto minVal = min > d->currentMin ? min : d->currentMin;
			d->attrMap["LThreshold"]->SetAttrValue(minVal);
		}
		if (d->attrMap.contains("UThreshold") && (image || isFloat)) {
			if (isFL) {
				d->attrMap["UThreshold"]->SetAttrModel(QVariantMap { { "Min", min }, { "Max", max }, { "Step", 1.0 }, { "Decimals", 0 } });

				const auto enabled = !(d->attrMap["Disable Upper Threshold"]->GetAttrValue().toBool());
				d->attrMap["UThreshold"]->SetAttrStyle(enabled ? ProcessorAttrStyle::Visible : ProcessorAttrStyle::Disabled);
			} else {
				d->attrMap["UThreshold"]->SetAttrModel(QVariantMap { { "Min", min }, { "Max", max }, { "Step", 0.0001 }, { "Decimals", 4 } });

				const auto enabled = !(d->attrMap["Disable Upper Threshold"]->GetAttrValue().toBool());
				d->attrMap["UThreshold"]->SetAttrStyle(enabled ? ProcessorAttrStyle::Visible : ProcessorAttrStyle::Disabled);
			}
			const auto maxVal = max < d->currentMax ? max : d->currentMax;
			d->attrMap["UThreshold"]->SetAttrValue(maxVal);
		}
	}

	auto ULThreshold2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto ULThreshold2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto ULThreshold2d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		float factor { 1 };
		TCDataConverter converter;
		int timestep { 0 };
		if (const auto tcf = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			timestep = tcf->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(tcf);
			const auto [imin, imax] = tcf->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY] = tcf->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = tcf->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			factor = 10000.0;
		} else if (const auto tcf = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			timestep = tcf->GetTimeStep();
			inputImage = Data::DataConverter::ConvertToTCImage(tcf);
			const auto [imin, imax] = tcf->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = tcf->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = tcf->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage"])) {
			timestep = image->GetTimeStep();
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
		} else {
			return {};
		}

		auto lthreshold = d->attrMap["Disable Lower Threshold"]->GetAttrValue().toBool() ? INT_MIN : d->attrMap["LThreshold"]->GetAttrValue().toDouble();
		auto uthreshold = d->attrMap["Disable Upper Threshold"]->GetAttrValue().toBool() ? INT_MAX : d->attrMap["UThreshold"]->GetAttrValue().toDouble();
		if (lthreshold > uthreshold) {
			return {};
		}
		lthreshold *= factor;
		uthreshold *= factor;
		try {
			const auto threshImage = thresholding(refImageView, { lthreshold, uthreshold });
			const auto binaryMask = convertImage(threshImage, ConvertImage::OutputType::UNSIGNED_INTEGER_16_BIT);
			int dim[3] { dimX, dimY, dimZ };

			const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(binaryMask->buffer()), dim, res);
			resultMask->SetTimeStep(timestep);
			DataPtr result { nullptr };
			result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask2D>(resultMask);
			if (result == nullptr) {
				return {};
			}

			return { { "OutputMask", result } };
		} catch (Exception& e) {
			return {};
		}
	}

	auto ULThreshold2d::Abort() -> void { }
}
