#include <QCoreApplication>

#include "Watershed3d.h"

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <TCImage.h>
#include <TCMask.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

namespace CellAnalyzer::Processor::Segmentation {
	using namespace imagedev;
	using namespace iolink;

	struct Watershed3d::Impl {
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	Watershed3d::Watershed3d() : d { std::make_unique<Impl>() } { }

	Watershed3d::~Watershed3d() { }

	auto Watershed3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;

		const auto id = attrID;
		if (id == "Object Filter Size") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0.001 }, { "Max", 1000 }, { "Step", 0.001 }, { "Decimals", 3 } });
		}
	}

	auto Watershed3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto Watershed3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto Watershed3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto Watershed3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto Watershed3d::Process() -> DataMap {
		TCMask::Pointer inputLabel { nullptr };
		TCMask::Pointer refMask { nullptr };
		int timestep { 0 };
		if (const auto label = std::dynamic_pointer_cast<Data::LabelMask3D>(d->inputMap["InputLabel"])) {
			inputLabel = Data::DataConverter::ConvertToTCMask(label);
			timestep = label->GetTimeStep();
		}

		if (const auto binary = std::dynamic_pointer_cast<Data::BinaryMask3D>(d->inputMap["ReferenceBinary"]))
			refMask = Data::DataConverter::ConvertToTCMask(binary);

		if (inputLabel == nullptr || refMask == nullptr)
			return {};
		if (!qFuzzyCompare(inputLabel->GetOffset(), refMask->GetOffset()))
			return {};

		const auto [resX, resY, resZ] = refMask->GetResolution();
		if (qFuzzyIsNull(resX) || qFuzzyIsNull(resY))
			return {};

		const auto dllPath = qApp->applicationDirPath() + "/algorithms/segmentation/TC.Algorithm.Segmentation.Watershed.3D.dll";
		PluginRegistry::GetInstance()->LoadPlugin(dllPath);

		const auto algorithm = std::dynamic_pointer_cast<IPluginAlgorithm>(PluginRegistry::GetPlugin(dllPath, true));
		if (algorithm == nullptr)
			return {};

		auto params = algorithm->Parameter();
		params->SetValue("ObjectSize", d->attrMap["Object Filter Size"]->GetAttrValue().toDouble() / (resX * resY * resZ));

		algorithm->SetInput(0, inputLabel);
		algorithm->SetInput(1, refMask);
		if (!algorithm->Execute())
			return {};

		const auto algoOutput = std::dynamic_pointer_cast<TCMask>(algorithm->GetOutput(0));
		algoOutput->SetTimeStep(timestep);
		algoOutput->SetOffset(refMask->GetOffset());
		const auto result = Data::DataConverter::ConvertToMaskData<Data::LabelMask3D>(algoOutput);

		return { { "OutputMask", result } };
	}

	auto Watershed3d::Abort() -> void { }
}
