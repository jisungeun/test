#include "Label2Binary2d.h"
#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <QCoreApplication>
#include <TCMask.h>

#include <TCDataConverter.h>
#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

namespace CellAnalyzer::Processor::Segmentation {
	using namespace imagedev;
	using namespace iolink;
	struct Label2Binary2d::Impl {		
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	Label2Binary2d::Label2Binary2d() : d { std::make_unique<Impl>() } { }

	Label2Binary2d::~Label2Binary2d() { }

	auto Label2Binary2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;	
	}

	auto Label2Binary2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto Label2Binary2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto Label2Binary2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto Label2Binary2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto Label2Binary2d::Process() -> DataMap {		
		TCMask::Pointer inputMask { nullptr };
		int timestep { 0 };
		float offset { 0 };
		TCDataConverter converter;
		std::shared_ptr<ImageView> inputLabel{ nullptr };
		int dim[3];
		double res[3];
		if (const auto mask = std::dynamic_pointer_cast<Data::BinaryMask2D>(d->inputMap["InputMask"])) {
			inputMask = Data::DataConverter::ConvertToTCMask(mask);
			timestep = mask->GetTimeStep();
			dim[0] = mask->GetSize().x;
			dim[1] = mask->GetSize().y;
			dim[2] = 1;
			res[0] = mask->GetResolution().x;
			res[1] = mask->GetResolution().y;
			res[2] = 1;
		} else {
			return {};
		}

		try {			
			auto threshold = thresholding(inputLabel, { 1,INT_MAX });
			auto binaryMask = convertImage(threshold, ConvertImage::UNSIGNED_INTEGER_16_BIT);

			const auto resultMask = converter.ArrToLabelMask(static_cast<uint16_t*>(binaryMask->buffer()), dim, res);
			resultMask->SetOffset(offset);
			resultMask->SetTimeStep(timestep);

			DataPtr result{ nullptr };
			result = Data::DataConverter::ConvertToMaskData<Data::BinaryMask2D>(resultMask);
			if (result == nullptr) {
				return {};
			}

			return { {"OutputMask",result} };
		}
		catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}
		return {};
	}

	auto Label2Binary2d::Abort() -> void { }
}
