#include <QCoreApplication>

#include <enum.h>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>

#include <Float2d.h>
#include <HT2D.h>
#include <FL2D.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "CellPose2d.h"
#include <PythonNumpyIO.h>
#include <QDir>

#include <QStandardPaths>

namespace CellAnalyzer::Processor::AI {
	using namespace imagedev;
	using namespace iolink;

	struct CellPose2d::Impl {
		QMap<int, QString> cellposeModel {
			{ 0, "cyto" },
			{ 1, "nuclei" },
			{ 2, "cyto2" },
			{ 3, "custom" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	CellPose2d::CellPose2d() : d { std::make_unique<Impl>() } { }

	CellPose2d::~CellPose2d() { }

	auto CellPose2d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "CellPose model") {
			attribute->SetAttrModel(QStringList(d->cellposeModel.values()));
		}
	}

	auto CellPose2d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto CellPose2d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto CellPose2d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto CellPose2d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto CellPose2d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		TCDataConverter converter;
		int timestep { 0 };
		if (const auto image = std::dynamic_pointer_cast<Data::HT2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL2D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float2D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = 0;
			const auto [idimx, idimy] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = 1;
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			timestep = image->GetTimeStep();
		} else {
			return {};
		}

		// set algorithm parameters and input data

		const auto cellposehome = d->attrMap["CellPose home"]->GetAttrValue().toString();
		const auto custommodelpath = d->attrMap["Custom model directory"]->GetAttrValue().toString();
		const auto model = d->attrMap["CellPose model"]->GetAttrValue().toString();

		QString writablePath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);

		QDir writableDir(writablePath);
		if (false == writableDir.exists()) {
			writableDir.cdUp();
			writableDir.mkdir("CellAnalyzer");
		}

		if (cellposehome.isEmpty()) {
			return {};
		}

		if (model == "custom" && custommodelpath.isEmpty()) {
			return {};
		}

		try {
			//Save float image as numpy image
			auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			std::vector<unsigned long> shape;
			shape.push_back(dimX);
			shape.push_back(dimY);
			auto* floatArray = static_cast<float*>(floatImage->buffer());

			std::vector floatVector(floatArray, floatArray + static_cast<size_t>(dimX * dimY));

			auto numpyWriter = std::make_shared<TC::IO::TCNumpyWriter>();
			auto inputFilePath = QString("%1/input_data.npy").arg(writablePath);
			auto outputFilePath = QString("%1/output_data.npy").arg(writablePath);
			numpyWriter->Write(floatVector, inputFilePath, TC::IO::NpyArrType::arrFLOAT, shape);

			//char* currentPath = getenv("PATH");
			char* currentPath = nullptr;
			size_t requiredSize;
			_dupenv_s(&currentPath, &requiredSize, "PATH");

			//Append selected python environment
			std::string newPath = QString("%1;%1/Scripts;").arg(cellposehome).toStdString() + std::string(currentPath);
			_putenv(("PATH=" + newPath).c_str());

			inputFilePath.replace('/', '\\');
			outputFilePath.replace('/', '\\');

			auto scriptPath = QString("%1/PyScript/cellpose2d.py").arg(qApp->applicationDirPath());
			scriptPath.replace('/', '\\');
			std::system(QString("python \"%1\" \"%2\" \"%3\" \"%4\"").arg(scriptPath).arg(inputFilePath).arg(outputFilePath).arg(model).toStdString().c_str());

			//remove input file after process
			inputFilePath.replace('\\', '/');
			QFile::remove(inputFilePath);

			//Restore Path
			_putenv(("PATH=" + std::string(currentPath)).c_str());
			free(currentPath);

			//load output File			
			auto numpyReader = std::make_shared<TC::IO::TCNumpyReader>();
			std::any outputArr;
			const auto [fortran_order,result_shape] = numpyReader->Read(outputArr, outputFilePath, TC::IO::NpyArrType::arrUSHORT);

			//remove output file after load
			outputFilePath.replace('\\', '/');
			QFile::remove(outputFilePath);

			auto resultArr = std::any_cast<std::vector<unsigned short>>(outputArr);

			int dim[3] { dimX, dimY, 1 };
			TCDataConverter converter;
			const auto tcmask = converter.ArrToLabelMask(resultArr.data(), dim, res);
			tcmask->SetTimeStep(timestep);

			const auto output = Data::DataConverter::ConvertToMaskData<Data::LabelMask2D>(tcmask);

			return { { "OutputMask", output } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}catch (...) {
			std::cout << "Unknown exception" << std::endl;
		}
		return {};
	}

	auto CellPose2d::Abort() -> void { }
}
