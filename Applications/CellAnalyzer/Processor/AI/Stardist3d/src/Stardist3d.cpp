#include <QCoreApplication>

#include <enum.h>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>


#include <Float3d.h>
#include <HT3D.h>
#include <FL3D.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "Stardist3d.h"

#include <PythonNumpyIO.h>
#include <QJsonDocument>
#include <QJsonArray>
#include <QDir>
#include <QStandardPaths>

namespace CellAnalyzer::Processor::AI {
	using namespace imagedev;
	using namespace iolink;

	struct Stardist3d::Impl {
		QMap<int, QString> stardistModel { { 0, "3D_demo" }, { 1, "Custom" } };
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	Stardist3d::Stardist3d() : d { std::make_unique<Impl>() } { }

	Stardist3d::~Stardist3d() { }

	auto Stardist3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Stardist model") {
			attribute->SetAttrModel(QStringList(d->stardistModel.values()));
		}
		if (id == "Downsampling factor (XY)") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 10 }, { "Step", 1 } });
		}
		if (id.contains("grid") || id.contains("train_patch_size")) {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 4096 }, { "Step", 1 } });
		}
		if (id == "grid X") {
			attribute->SetAttrValue(4);
		}
		if (id == "grid Y") {
			attribute->SetAttrValue(4);
		}
		if (id == "grid Z") {
			attribute->SetAttrValue(1);
		}
		if (id == "train_patch_size X") {
			attribute->SetAttrValue(128);
		}
		if (id == "train_patch_size Y") {
			attribute->SetAttrValue(128);
		}
		if (id == "train_patch_size") {
			attribute->SetAttrValue(16);
		}
		if (id == "unet_n_depth" || id == "train_batch_size") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 100 }, { "Step", 1 } });
		}
		if (id == "n_rays") {
			attribute->SetAttrModel(QVariantMap { { "Min", 1 }, { "Max", 10000 }, { "Step", 1 } });
		}
		if (id == "unet_n_depth") {
			attribute->SetAttrValue(2);
		}
		if (id == "n_rays") {
			attribute->SetAttrValue(4096);
		}
		if (id == "train_batch_size") {
			attribute->SetAttrValue(2);
		}
		if (id == "prob" || id == "nms") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 1 }, { "Step", 0.001 }, { "Decimals", 3 } });
		}
		if (id == "prob") {
			attribute->SetAttrValue(0.642679735271349);
		}
		if (id == "nms") {
			attribute->SetAttrValue(0.3);
		}
	}

	auto Stardist3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
		if (attrID == "Stardist model") {
			ProcessorAttrStyle style;
			if (value.toString() == "Custom") {
				style = ProcessorAttrStyle::Visible;
			} else {
				style = ProcessorAttrStyle::Hidden;
			}
			d->attrMap["Custom model directory"]->SetAttrStyle(style);
			d->attrMap["grid X"]->SetAttrStyle(style);
			d->attrMap["grid Y"]->SetAttrStyle(style);
			d->attrMap["grid Z"]->SetAttrStyle(style);
			d->attrMap["train_patch_size X"]->SetAttrStyle(style);
			d->attrMap["train_patch_size Y"]->SetAttrStyle(style);
			d->attrMap["train_patch_size Z"]->SetAttrStyle(style);
			d->attrMap["unet_n_depth"]->SetAttrStyle(style);
			d->attrMap["n_rays"]->SetAttrStyle(style);
			d->attrMap["train_batch_size"]->SetAttrStyle(style);
			d->attrMap["prob"]->SetAttrStyle(style);
			d->attrMap["nms"]->SetAttrStyle(style);
		}
		if (attrID == "Custom model directory") {
			const auto custommodelpath = value.toString();
			QFileInfo modelFile(custommodelpath);
			const auto parentFolderPath = modelFile.absolutePath();
			const auto configPath = QString("%1/config.json").arg(parentFolderPath);
			const auto thresholdPath = QString("%1/thresholds.json").arg(parentFolderPath);

			if (QFileInfo(configPath).exists()) {
				QFile configFile(configPath);
				if (configFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
					const auto configData = configFile.readAll();
					QJsonDocument jsonDoc = QJsonDocument::fromJson(configData);
					const auto obj = jsonDoc.object();
					if (obj.contains("grid") && obj["grid"].isArray()) {
						const auto gridZ = obj["grid"].toArray()[0].toInt();
						const auto gridX = obj["grid"].toArray()[1].toInt();
						const auto gridY = obj["grid"].toArray()[2].toInt();
						d->attrMap["grid Z"]->SetAttrValue(gridZ);
						d->attrMap["grid X"]->SetAttrValue(gridX);
						d->attrMap["grid Y"]->SetAttrValue(gridY);
					}
					if (obj.contains("unet_n_depth") && obj["unet_n_depth"].isDouble()) {
						const auto unetDepth = obj["unet_n_depth"].toInt();
						d->attrMap["unet_n_depth"]->SetAttrValue(unetDepth);
					}
					if (obj.contains("n_rays") && obj["n_rays"].isDouble()) {
						const auto nray = obj["n_rays"].toInt();
						d->attrMap["n_rays"]->SetAttrValue(nray);
					}
					if (obj.contains("train_patch_size") && obj["train_patch_size"].isArray()) {
						const auto tpsZ = obj["train_patch_size"].toArray()[0].toInt(16);
						const auto tpsX = obj["train_patch_size"].toArray()[1].toInt(128);
						const auto tpsY = obj["train_patch_size"].toArray()[2].toInt(128);
						d->attrMap["train_patch_size Z"]->SetAttrValue(tpsZ);
						d->attrMap["train_patch_size X"]->SetAttrValue(tpsX);
						d->attrMap["train_patch_size Y"]->SetAttrValue(tpsY);
					}
					if (obj.contains("train_batch_size") && obj["train_batch_size"].isDouble()) {
						const auto tbs = obj["train_batch_size"].toInt();
						d->attrMap["train_batch_size"]->SetAttrValue(tbs);
					}
				}
				configFile.close();
			}
			if (QFileInfo(thresholdPath).exists()) {
				QFile threshFile(thresholdPath);
				if (threshFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
					const auto threshData = threshFile.readAll();
					QJsonDocument jsonDoc = QJsonDocument::fromJson(threshData);
					const auto obj = jsonDoc.object();
					if (obj.contains("prob") && obj["prob"].isDouble()) {
						const auto prob = obj["prob"].toDouble(0.642679735271349);
						d->attrMap["prob"]->SetAttrValue(prob);
					}
					if (obj.contains("nms") && obj["nms"].isDouble()) {
						const auto nms = obj["nms"].toDouble(0.3);
						d->attrMap["nms"]->SetAttrValue(nms);
					}
				}
				threshFile.close();
			}
		}
	}

	auto Stardist3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto Stardist3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto Stardist3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto Stardist3d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset { 0 };
		TCDataConverter converter;
		int timestep { 0 };
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetResolution();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			timestep = image->GetTimeStep();
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		const auto stardisthome = d->attrMap["Stardist home"]->GetAttrValue().toString();
		const auto custommodelpath = d->attrMap["Custom model directory"]->GetAttrValue().toString();
		const auto model = d->attrMap["Stardist model"]->GetAttrValue().toString();
		const auto resize_factor = d->attrMap["Downsampling factor (XY)"]->GetAttrValue().toInt();

		QString writablePath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);

		QDir writableDir(writablePath);
		if (false == writableDir.exists()) {
			writableDir.cdUp();
			writableDir.mkdir("CellAnalyzer");
		}

		if (stardisthome.isEmpty()) {
			return {};
		}

		if (model == "Custom" && custommodelpath.isEmpty()) {
			return {};
		}

		try {
			auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			std::vector<unsigned long> shape;
			shape.push_back(dimZ);
			shape.push_back(dimX);
			shape.push_back(dimY);
			auto* floatArray = static_cast<float*>(floatImage->buffer());

			std::vector floatVector(floatArray, floatArray + static_cast<size_t>(dimX * dimY * dimZ));

			auto numpyWriter = std::make_shared<TC::IO::TCNumpyWriter>();
			auto inputFilePath = QString("%1/input_data.npy").arg(writablePath);
			auto outputFilePath = QString("%1/output_data.npy").arg(writablePath);
			numpyWriter->Write(floatVector, inputFilePath, TC::IO::NpyArrType::arrFLOAT, shape);

			char* currentPath = nullptr;
			size_t requiredSize;
			_dupenv_s(&currentPath, &requiredSize, "PATH");

			//Append selected python environment
			std::string newPath = QString("%1;%1/Scripts;").arg(stardisthome).toStdString() + std::string(currentPath);
			_putenv(("PATH=" + newPath).c_str());

			inputFilePath.replace('/', '\\');
			outputFilePath.replace('/', '\\');

			if (model == "Custom") {
				int gridX = d->attrMap["grid X"]->GetAttrValue().toInt();
				int gridY = d->attrMap["grid Y"]->GetAttrValue().toInt();
				int gridZ = d->attrMap["grid Z"]->GetAttrValue().toInt();;
				int unetDepth = d->attrMap["unet_n_depth"]->GetAttrValue().toInt();
				int nray = d->attrMap["n_rays"]->GetAttrValue().toInt();
				int tpsX = d->attrMap["train_patch_size X"]->GetAttrValue().toInt();
				int tpsY = d->attrMap["train_patch_size Y"]->GetAttrValue().toInt();
				int tpsZ = d->attrMap["train_patch_size Z"]->GetAttrValue().toInt();
				int tbs = d->attrMap["train_batch_size"]->GetAttrValue().toInt();
				double prob = d->attrMap["prob"]->GetAttrValue().toDouble();
				double nms = d->attrMap["nms"]->GetAttrValue().toDouble();

				QString modelPath = QFileInfo(custommodelpath).dir().path();

				auto scriptPath = QString("%1/PyScript/customstardist3d.py").arg(qApp->applicationDirPath());
				scriptPath.replace('/', '\\');
				std::system(QString("python \"%1\" \"%2\" \"%3\" \"%4\" \"%5\" \"%6\" \"%7\" \"%8\" \"%9\" \"%10\" \"%11\" \"%12\" \"%13\" \"%14\" \"%15\" \"%16\"").arg(scriptPath).arg(inputFilePath).arg(outputFilePath).arg(modelPath).arg(gridX).arg(gridY).arg(gridZ).arg(tpsX).arg(tpsY).arg(tpsZ).arg(unetDepth).arg(nray).arg(tbs).arg(prob).arg(nms).arg(resize_factor).toStdString().c_str());
			} else {
				auto scriptPath = QString("%1/PyScript/stardist3d.py").arg(qApp->applicationDirPath());
				scriptPath.replace('/', '\\');
				std::system(QString("python \"%1\" \"%2\" \"%3\" \"%4\" \"%5\"").arg(scriptPath).arg(inputFilePath).arg(outputFilePath).arg(model).arg(resize_factor).toStdString().c_str());
			}

			//remove input file after process
			inputFilePath.replace('\\', '/');
			QFile::remove(inputFilePath);

			//Restore Path
			_putenv(("PATH=" + std::string(currentPath)).c_str());
			free(currentPath);

			//load output File			
			auto numpyReader = std::make_shared<TC::IO::TCNumpyReader>();
			std::any outputArr;
			const auto [fortran_order, result_shape] = numpyReader->Read(outputArr, outputFilePath, TC::IO::NpyArrType::arrINT);

			//remove output file after load
			outputFilePath.replace('\\', '/');
			QFile::remove(outputFilePath);

			const auto resultArr = std::any_cast<std::vector<int32_t>>(outputArr);
			std::vector<uint16_t> ushortVector;
			for (auto i = 0; i < resultArr.size(); i++) {
				if (resultArr[i] < 1)
					ushortVector.push_back(0);
				else
					ushortVector.push_back(static_cast<uint16_t>(resultArr[i]));
			}

			int dim[3] { dimX, dimY, dimZ };
			TCDataConverter converter;
			const auto tcmask = converter.ArrToLabelMask(ushortVector.data(), dim, res);
			tcmask->SetTimeStep(timestep);
			tcmask->SetOffset(offset);

			const auto output = Data::DataConverter::ConvertToMaskData<Data::LabelMask3D>(tcmask);

			return { { "OutputMask", output } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}catch (...) {
			std::cout << "Unknown exception" << std::endl;
		}
		return {};
	}

	auto Stardist3d::Abort() -> void { }
}
