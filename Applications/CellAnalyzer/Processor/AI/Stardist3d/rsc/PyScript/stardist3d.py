# my_script.py
import sys
import numpy as np
from skimage.filters import gaussian
import math
from scipy.ndimage import zoom

from stardist.models import StarDist3D
from csbdeep.utils import normalize

def stardist_inference(file_path,model_name,downsample):
    img = np.load(file_path)    
    img_size = img.shape
    original_size = np.array(img.shape[1:])    
    number = int(original_size[0] / downsample)    
    nearest_lower_power = 2 ** math.floor(math.log2(number))
    nearest_upper_power = 2 ** math.ceil(math.log2(number))
    if abs(nearest_lower_power - number) < abs(nearest_upper_power - number):
        downsample = nearest_lower_power
    else:
        downsample = nearest_upper_power
    if downsample > 1:        
        scale_factors = downsample / original_size        
        img = zoom(img, (1, *scale_factors), order=3, mode='reflect')
    
    axis_norm = (2,0,1)
    img_norm = normalize(img, 1,99.8, axis=axis_norm)    
    
    model = StarDist3D.from_pretrained(model_name)
    
    labels, _ = model.predict_instances(img_norm)        
    
    if downsample > 1:        
        inv_factors = original_size / downsample
        labels = zoom(labels, (1, *inv_factors), order=0, mode='reflect')
        #resized_mask = np.zeros(img_size, dtype=np.uint16)
        #for label_value in np.unique(labels):
            #if label_value == 0:
                #continue
            
            #binary_mask = (labels == label_value).astype(float)
            
            #resized_binary_mask = zoom(binary_mask, (1,*inv_factors), order=1)            
            #blurred_mask = gaussian(resized_binary_mask, sigma=1)        
            
            #smoothed_mask = (blurred_mask > 0.5).astype(np.uint8)
            
            #resized_mask[smoothed_mask > 0] = label_value 
        #return resized_mask
    
    return labels

if __name__ == "__main__":
    if len(sys.argv) < 5:
        print("Usage: python my_script.py <input_file> <output_file> <model_name> <downsample_factor>")
        sys.exit(1)
    
    input_path = sys.argv[1]
    
    output_path = sys.argv[2]
    
    model_name = sys.argv[3]
    
    down_sample_factor = int(sys.argv[4])
        
    result_data = stardist_inference(input_path,model_name,down_sample_factor)
    
    np.save(output_path, result_data)