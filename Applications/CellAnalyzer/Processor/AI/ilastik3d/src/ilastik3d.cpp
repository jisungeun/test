#include <QCoreApplication>

#include <enum.h>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>


#include <Float3d.h>
#include <HT3D.h>
#include <FL3D.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "ilastik3d.h"

#include <PythonNumpyIO.h>
#include <QDir>
#include <QProcess>
#include <QStandardPaths>

namespace CellAnalyzer::Processor::AI {
	using namespace imagedev;
	using namespace iolink;

	struct ilastik3d::Impl {
		QMap<int, QString> stardistModel {
			{ 0, "3D_demo" },
			{ 1, "Custom" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	ilastik3d::ilastik3d() : d { std::make_unique<Impl>() } { }

	ilastik3d::~ilastik3d() { }

	auto ilastik3d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "Channel") {
			attribute->SetAttrModel(QVariantMap { { "Min", 0 }, { "Max", 1 }, { "Step", 1 } });
			attribute->SetAttrValue(0);
		}
	}

	auto ilastik3d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto ilastik3d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto ilastik3d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto ilastik3d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto ilastik3d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset { 0 };
		TCDataConverter converter;
		int timestep { 0 };
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetResolution();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			timestep = image->GetTimeStep();
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		auto ilastikhome = d->attrMap["ilastik"]->GetAttrValue().toString();
		auto model = d->attrMap["ilastik model"]->GetAttrValue().toString();
		const auto channel = d->attrMap["Channel"]->GetAttrValue().toInt();
		auto pythonhome = d->attrMap["Python home"]->GetAttrValue().toString();
		QString writablePath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);

		QDir writableDir(writablePath);
		if (false == writableDir.exists()) {
			writableDir.cdUp();
			writableDir.mkdir("CellAnalyzer");
		}

		if (ilastikhome.isEmpty()) {
			return {};
		}


		try {
			auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			std::vector<unsigned long> shape;
			shape.push_back(dimZ);
			shape.push_back(dimY);
			shape.push_back(dimX);
			auto* floatArray = static_cast<float*>(floatImage->buffer());

			std::vector floatVector(floatArray, floatArray + static_cast<size_t>(dimX * dimY * dimZ));

			auto numpyWriter = std::make_shared<TC::IO::TCNumpyWriter>();
			auto inputFilePath = QString("%1/input_data.npy").arg(writablePath);
			auto outputFilePath = QString("%1/output_data.npy").arg(writablePath);
			auto resultFilePath = QString("%1/result.npy").arg(writablePath);
			numpyWriter->Write(floatVector, inputFilePath, TC::IO::NpyArrType::arrFLOAT, shape);

			inputFilePath.replace('/', '\\');
			outputFilePath.replace('/', '\\');
			resultFilePath.replace('/', '\\');
			pythonhome.replace('/', '\\');

			ilastikhome.replace('/', "\\");
			model.replace('/', "\\");

			const auto script = QString("\"\"%1\" --headless --project=\"%2\" --input_axes=zxy --export_source=\"Probabilities\" --output_format=numpy --output_filename_format=\"%3\" --raw_data=\"%4\"\"").arg(ilastikhome).arg(model).arg(outputFilePath).arg(inputFilePath).toStdString().c_str();

			std::system(script);

			inputFilePath.replace('\\', '/');
			QFile::remove(inputFilePath);

			//run python post processing
			char* currentPath = nullptr;
			size_t requiredSize;
			_dupenv_s(&currentPath, &requiredSize, "PATH");

			std::string newPath = QString("%1;").arg(pythonhome).toStdString() + std::string(currentPath);
			_putenv(("PATH=" + newPath).c_str());

			auto scriptPath = QString("%1/PyScript/ilastikPost.py").arg(qApp->applicationDirPath());

			std::system(QString("python \"%1\" \"%2\" \"%3\" \"%4\"").arg(scriptPath).arg(outputFilePath).arg(resultFilePath).arg(QString::number(channel)).toStdString().c_str());

			outputFilePath.replace('\\', '/');
			QFile::remove(outputFilePath);

			//Restore Path
			_putenv(("PATH=" + std::string(currentPath)).c_str());
			free(currentPath);

			//load output File			
			auto numpyReader = std::make_shared<TC::IO::TCNumpyReader>();
			std::any outputArr;
			const auto [fortran_order, result_shape] = numpyReader->Read(outputArr, resultFilePath, TC::IO::NpyArrType::arrFLOAT);

			//remove output file after load
			resultFilePath.replace('\\', '/');
			QFile::remove(resultFilePath);

			const auto resultArr = std::any_cast<std::vector<float>>(outputArr);
			std::vector<float> resultFloatVector(resultArr.begin(), resultArr.end());

			int dim[3] { dimX, dimY, dimZ };

			const auto output = Data::DataConverter::ConvertToFloatData<Data::Float3D>(resultFloatVector.data(), dimX, dimY, dimZ, res[0], res[1], res[2], 0, 1, timestep, offset);

			return { { "OutputImage", output } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}catch (...) {
			std::cout << "Unknown exception" << std::endl;
		}
		return {};
	}

	auto ilastik3d::Abort() -> void { }
}
