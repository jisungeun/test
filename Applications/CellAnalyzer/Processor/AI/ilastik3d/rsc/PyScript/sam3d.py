# my_script.py
import sys
import numpy as np
import matplotlib.pyplot as plt

from stardist.models import StarDist3D
from csbdeep.utils import normalize

def stardist_inference(file_path,model_name):
    img = np.load(file_path)
    axis_norm = (2,0,1)
    img_norm = normalize(img, 1,99.8, axis=axis_norm)
    #img_xyz = np.transpose(img, (1,2,0))
    #img_zxy = np.transpose(img, (2, 0, 1))
    
    model = StarDist3D.from_pretrained(model_name)
    
    labels, _ = model.predict_instances(img_norm)
    
    #labels_xyz = np.transpose(labels, (1,2,0))
    #labels_zxy = np.transpose(labels, (2, 0, 1))
    
    #return labels_xyz
    return labels

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python my_script.py <input_file> <output_file> <model_name>")
        sys.exit(1)
    
    input_path = sys.argv[1]
    
    output_path = sys.argv[2]
    
    model_name = sys.argv[3]
        
    result_data = stardist_inference(input_path,model_name)
    
    np.save(output_path, result_data)