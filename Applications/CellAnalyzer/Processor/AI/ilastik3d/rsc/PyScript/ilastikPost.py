# my_script.py
import sys
import numpy as np

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: python my_script.py <input_file> <output_file> <channel>")
        sys.exit(1)
    
    input_path = sys.argv[1]        
    
    output_path = sys.argv[2]
    
    ch = int(sys.argv[3])
    
    array_4d = np.load(input_path)

    result_data = array_4d[:, :, :, ch]
    
    np.save(output_path, result_data)