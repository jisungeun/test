#include <QCoreApplication>

#include <enum.h>

#include <DataConverter.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>


#include <Float3d.h>
#include <HT3D.h>
#include <FL3D.h>

#include <ImageDev/ImageDev.h>
#include <ImageDev/Data/ImageViewHelper.h>

#include <iolink/view/ImageViewFactory.h>

#include <TCDataConverter.h>

#include "CellPose25d.h"

#include <PythonNumpyIO.h>
#include <QDir>
#include <QStandardPaths>

namespace CellAnalyzer::Processor::AI {
	using namespace imagedev;
	using namespace iolink;

	struct CellPose25d::Impl {
		QMap<int, QString> cellposeModel {
			{ 0, "cyto" },
			{ 1, "nuclei" },
			{ 2, "cyto2" },
			{ 3, "custom" }
		};
		QMap<QString, DataPtr> inputMap;
		QMap<QString, ProcessorAttrPtr> attrMap;
	};

	CellPose25d::CellPose25d() : d { std::make_unique<Impl>() } { }

	CellPose25d::~CellPose25d() { }

	auto CellPose25d::SetAttr(const QString& attrID, const ProcessorAttrPtr& attribute) -> void {
		d->attrMap[attrID] = attribute;
		const auto id = attrID;
		if (id == "CellPose model") {
			attribute->SetAttrModel(QStringList(d->cellposeModel.values()));
		}
	}

	auto CellPose25d::SetAttrValue(const QString& attrID, const ProcessorAttrValue& value) -> void {
		if (false == d->attrMap.contains(attrID)) {
			return;
		}
		d->attrMap[attrID]->SetAttrValue(value);
	}

	auto CellPose25d::SetInputData(const QString& inputID, const DataPtr& data) -> void {
		d->inputMap[inputID] = data;
	}

	auto CellPose25d::GetAttr(const QString& attrID) const -> ProcessorAttrPtr {
		return d->attrMap[attrID];
	}

	auto CellPose25d::GetInputData(const QString& inputID) const -> DataPtr {
		return d->inputMap[inputID];
	}

	auto CellPose25d::Process() -> DataMap {
		TCImage::Pointer inputImage { nullptr };
		std::shared_ptr<ImageView> refImageView { nullptr };
		double min, max;
		double res[3];
		int dimX, dimY, dimZ;
		double offset { 0 };
		TCDataConverter converter;
		int timestep { 0 };
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetRI();
			min = imin * 10000.0;
			max = imax * 10000.0;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::FL3D>(d->inputMap["InputImage"])) {
			inputImage = Data::DataConverter::ConvertToTCImage(image);
			const auto [imin, imax] = image->GetIntensity();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetResolution();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.ImageToImageView(inputImage);
			timestep = image->GetTimeStep();
		} else if (const auto image = std::dynamic_pointer_cast<Data::Float3D>(d->inputMap["InputImage"])) {
			const auto [imin, imax] = image->GetRange();
			min = imin;
			max = imax;
			const auto [iresX, iresY, iresZ] = image->GetResolution();
			res[0] = iresX;
			res[1] = iresY;
			res[2] = iresZ;
			const auto [idimx, idimy, idimz] = image->GetSize();
			dimX = idimx;
			dimY = idimy;
			dimZ = idimz;
			offset = image->GetZOffset();
			refImageView = converter.FloatArrToImageView(static_cast<float*>(image->GetData()), dimX, dimY, dimZ, res);
			timestep = image->GetTimeStep();
		} else {
			//throw PipelineException(tr("If error occurred during processing, exception must be thrown to print message to user."));
			return {};
		}

		const auto cellposehome = d->attrMap["CellPose home"]->GetAttrValue().toString();
		const auto custommodelpath = d->attrMap["Custom model directory"]->GetAttrValue().toString();
		const auto model = d->attrMap["CellPose model"]->GetAttrValue().toString();
		const auto diameter = d->attrMap["Diameter"]->GetAttrValue().toInt();

		QString writablePath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);

		QDir writableDir(writablePath);
		if (false == writableDir.exists()) {
			writableDir.cdUp();
			writableDir.mkdir("CellAnalyzer");
		}

		if (cellposehome.isEmpty()) {
			return {};
		}

		if (model == "Custom" && custommodelpath.isEmpty()) {
			return {};
		}

		try {
			auto floatImage = convertImage(refImageView, ConvertImage::FLOAT_32_BIT);
			auto resultImage = resetImage(convertImage(refImageView, ConvertImage::UNSIGNED_INTEGER_16_BIT), 0);
			for (auto z = 0; z < dimZ; z++) {
				const auto slicedFloat = getSliceFromVolume3d(floatImage, GetSliceFromVolume3d::Z_AXIS, z);
				std::vector<unsigned long> shape;
				shape.push_back(dimX);
				shape.push_back(dimY);
				auto* floatArray = static_cast<float*>(slicedFloat->buffer());

				std::vector floatVector(floatArray, floatArray + static_cast<size_t>(dimX * dimY));

				auto numpyWriter = std::make_shared<TC::IO::TCNumpyWriter>();
				auto inputFilePath = QString("%1/input_data.npy").arg(writablePath);
				auto outputFilePath = QString("%1/output_data.npy").arg(writablePath);
				numpyWriter->Write(floatVector, inputFilePath, TC::IO::NpyArrType::arrFLOAT, shape);

				//char* currentPath = getenv("PATH");
				char* currentPath = nullptr;
				size_t requiredSize;
				_dupenv_s(&currentPath, &requiredSize, "PATH");

				//Append selected python environment
				std::string newPath = QString("%1;%1/Scripts;").arg(cellposehome).toStdString() + std::string(currentPath);
				_putenv(("PATH=" + newPath).c_str());

				inputFilePath.replace('/', '\\');
				outputFilePath.replace('/', '\\');

				auto scriptPath = QString("%1/PyScript/cellpose25d.py").arg(qApp->applicationDirPath());
				scriptPath.replace('/', '\\');
				std::system(QString("python \"%1\" \"%2\" \"%3\" \"%4\"").arg(scriptPath).arg(inputFilePath).arg(outputFilePath).arg(model).toStdString().c_str());

				//remove input file after process
				inputFilePath.replace('\\', '/');
				QFile::remove(inputFilePath);

				//Restore Path
				_putenv(("PATH=" + std::string(currentPath)).c_str());
				free(currentPath);

				//load output File			
				auto numpyReader = std::make_shared<TC::IO::TCNumpyReader>();
				std::any outputArr;
				const auto [fortran_order, result_shape] = numpyReader->Read(outputArr, outputFilePath, TC::IO::NpyArrType::arrUSHORT);

				//remove output file after load
				outputFilePath.replace('\\', '/');
				QFile::remove(outputFilePath);

				auto resultArr = std::any_cast<std::vector<unsigned short>>(outputArr);

				const auto arrView = converter.UShortArrToImageView(resultArr.data(), dimX, dimY, 1, res);
				resultImage = setSliceToVolume3d(resultImage, arrView, SetSliceToVolume3d::Z_AXIS, z);
			}
			int dim[3] { dimX, dimY, dimZ };
			TCDataConverter converter;
			const auto tcmask = converter.ArrToLabelMask(static_cast<unsigned short*>(resultImage->buffer()), dim, res);
			tcmask->SetTimeStep(timestep);
			tcmask->SetOffset(offset);

			const auto output = Data::DataConverter::ConvertToMaskData<Data::LabelMask3D>(tcmask);

			return { { "OutputMask", output } };
		} catch (Exception& e) {
			std::cout << e.what() << std::endl;
		}catch (...) {
			std::cout << "Unknown exception" << std::endl;
		}
		return {};
	}

	auto CellPose25d::Abort() -> void { }
}
