# my_script.py
import sys
import numpy as np

from cellpose import models
    
def cellpose_inference(file_path,model_name):
    img = np.load(file_path)
    
    model = models.Cellpose(gpu=False, model_type=model_name)
    
    masks, flows, styles, diams = model.eval(img, diameter=None, channels=[0,0])
    
    return masks

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: python my_script.py <input_file> <output_file> <model_name>")
        sys.exit(1)
    
    input_path = sys.argv[1]
    
    output_path = sys.argv[2]
    
    model_name = sys.argv[3]
        
    result_data = cellpose_inference(input_path,model_name)
    
    np.save(output_path, result_data)