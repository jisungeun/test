# my_script.py
import sys
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.ndimage import zoom

from pathlib import Path
from stardist.models import Config3D, StarDist3D
from csbdeep.utils import normalize

if __name__ == "__main__":
    if len(sys.argv) < 15:
        print("Usage: python my_script.py <input_file> <output_file> <model_name> <grid X> <grid Y> <grid Z> <train_patch_size X> <train_patch_size Y> <train_patch_size Z> <unet_n_depth> <n_rays> <train_batch_size> <prob> <nms> <downsample_factor>")
        sys.exit(1)
    
    input_path = sys.argv[1]
    
    output_path = sys.argv[2]
    
    model_name = sys.argv[3]
    
    grid_X = int(sys.argv[4])
    
    grid_Y = int(sys.argv[5])
    
    grid_Z = int(sys.argv[6])
    
    train_patch_size_X = int(sys.argv[7])
    
    train_patch_size_Y = int(sys.argv[8])
    
    train_patch_size_Z = int(sys.argv[9])
    
    unet_n_depth = int(sys.argv[10])
    
    n_rays = int(sys.argv[11])
    
    train_batch_size = int(sys.argv[12])
    
    prob = float(sys.argv[13])
    
    nms = float(sys.argv[14])
    
    downsample = int(sys.argv[15])
    ######################
    
    img = np.load(input_path)
    original_size = np.array(img.shape[1:])
    
    if downsample > 0:        
        #scale_factors = downsample / original_size
        scale_factors = np.array([1,1]) / downsample
        img = zoom(img, (1, *scale_factors), order=3, mode='reflect')
    
    axis_norm = (2,0,1)
    img_norm = normalize(img, 1,99.8, axis=axis_norm)
    
    model_class = StarDist3D
    path = Path(model_name)
    
    model = model_class(None, name=path.name, basedir=str(path.parent))
    conf = model.config;
    #set custom config parameter
    conf.grid = (grid_Z,grid_Y,grid_X)
    conf.train_patch_size = (train_patch_size_Z,train_patch_size_Y,train_patch_size_X)
    #conf.unet_n_depth = unet_n_depth
    conf.n_rays = n_rays
    conf.train_batch_size = train_batch_size    
    #run stardist
    (_, _), cnn_out = model.predict_instances(img_norm,nms_thresh=nms,prob_thresh=prob,return_labels=False,return_predict=True)
    
    prob, _ = cnn_out[:2]    
    
    if downsample > 0:
        inv_factors = original_size / prob.shape[1]
        #inv_factors = np.array([1,1]) * downsample
        prob = zoom(prob, (1, *inv_factors), order=3, mode='reflect')
    
    result_data = prob
            
    np.save(output_path, result_data)