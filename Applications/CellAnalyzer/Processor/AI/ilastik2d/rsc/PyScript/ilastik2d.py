# my_script.py
import sys
import numpy as np
import matplotlib.pyplot as plt

from stardist.models import StarDist2D
from csbdeep.utils import normalize

def show_image_from_numpy_file(file_path):
    # Load the numpy array from the file
    image_data = np.load(file_path)

    # Display the 2D float image
    plt.imshow(image_data, cmap='viridis')  # You can choose a different colormap
    plt.colorbar()  # Add a colorbar for better interpretation
    plt.title('2D Float Image')
    plt.show()
    
def stardist_inference(file_path,model_name):
    img = np.load(file_path)
    
    model = StarDist2D.from_pretrained(model_name)
    labels, _ = model.predict_instances(normalize(img))
    
    return labels

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: python my_script.py <input_file> <output_file> <model_name>")
        sys.exit(1)
    
    input_path = sys.argv[1]
    
    output_path = sys.argv[2]
    
    model_name = sys.argv[3]
        
    result_data = stardist_inference(input_path,model_name)
    
    np.save(output_path, result_data)