#pragma once

#include <QWidget>

#include "IProcessAttr.h"

#include "CellAnalyzer.Pipeline.Widget.PropertyWidgetExport.h"

namespace CellAnalyzer::Pipeline::Widget {
	class CellAnalyzer_Pipeline_Widget_PropertyWidget_API ModifierWidget final : public QWidget {
		Q_OBJECT

	public:
		explicit ModifierWidget(QWidget* parent = nullptr);
		~ModifierWidget() override;

		auto GetModifier() const -> AttrModifier;
		auto SetModifier(AttrModifier modifier) -> void;

	signals:
		auto ModifierChanged(AttrModifier modifier) -> void;

	protected slots:
		auto OnStateChanged(int state) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
