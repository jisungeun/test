#pragma once

#include <QWidget>

#include "IProcessAttr.h"

#include "CellAnalyzer.Pipeline.Widget.PropertyWidgetExport.h"

namespace CellAnalyzer::Pipeline::Widget {
	class CellAnalyzer_Pipeline_Widget_PropertyWidget_API AttrWidget final : public QWidget {
		Q_OBJECT

	public:
		explicit AttrWidget(AttrCategory category, QWidget* parent = nullptr);
		~AttrWidget() override;

		auto SetModel(const AttrValue& model) -> void;
		auto SetValue(const AttrValue& value) -> void;

		auto GetModel() const -> AttrValue;
		auto GetValue() const -> AttrValue;

	protected:
		auto SetCheckBox() -> void;
		auto UpdateCheckBoxValue() -> void;
		auto UpdateCheckBoxModel() -> void;

		auto SetIntSlider() -> void;
		auto UpdateIntSliderValue() -> void;
		auto UpdateIntSliderModel() -> void;

		auto SetIntSpin() -> void;
		auto UpdateIntSpinValue() -> void;
		auto UpdateIntSpinModel() -> void;

		auto SetDoubleSpin() -> void;
		auto UpdateDoubleSpinValue() -> void;
		auto UpdateDoubleSpinModel() -> void;

		auto SetDoubleSlider() -> void;
		auto UpdateDoubleSliderValue() -> void;
		auto UpdateDoubleSliderModel() -> void;

		auto SetString() -> void;
		auto UpdateStringValue() -> void;
		auto UpdateStringModel() -> void;

		auto SetStringText() -> void;
		auto UpdateStringTextValue() -> void;
		auto UpdateStringTextModel() -> void;

		auto SetStringList() -> void;
		auto UpdateStringListValue() -> void;
		auto UpdateStringListModel() -> void;

		auto SetStringCombo() -> void;
		auto UpdateStringComboValue() -> void;
		auto UpdateStringComboModel() -> void;

		auto SetStringRadio() -> void;
		auto UpdateStringRadioValue() -> void;
		auto UpdateStringRadioModel() -> void;

		auto SetStringCheck() -> void;
		auto UpdateStringCheckValue() -> void;
		auto UpdateStringCheckModel() -> void;

		auto SetFile() -> void;
		auto UpdateFileValue() -> void;
		auto UpdateFileModel() -> void;

		auto SetDirectory() -> void;
		auto UpdateDirectoryValue() -> void;
		auto UpdateDirectoryModel() -> void;

		auto SetTCFViewer() -> void;
		auto UpdateTCFViewerValue() -> void;
		auto UpdateTCFViewerModel() -> void;

	signals:
		auto ValueChanged(const AttrValue& value) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
