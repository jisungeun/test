#include <QCheckBox>
#include <QComboBox>
#include <QFileDialog>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QPushButton>
#include <QRadioButton>
#include <QSlider>
#include <QSpinBox>
#include <QTextEdit>
#include <QValidator>

#include "AttrWidget.h"

#include "TCFDialog.h"
#include "ui_AttrWidget.h"

namespace CellAnalyzer::Pipeline::Widget {
	struct AttrWidget::Impl {
		Ui::AttrWidget ui;
		QWidget* widget = nullptr;

		AttrCategory category = AttrCategory::Null;
		AttrValue value;
		AttrValue model;

		bool updating = false;
	};

	AttrWidget::AttrWidget(AttrCategory category, QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);
		d->category = category;
		d->updating = true;

		switch (category) {
			case AttrCategory::Null:
				return;
			case AttrCategory::CheckBox:
				SetCheckBox();
				break;
			case AttrCategory::Integer:
				SetIntSpin();
				break;
			case AttrCategory::IntegerSlider:
				SetIntSlider();
				break;
			case AttrCategory::Double:
				SetDoubleSpin();
				break;
			case AttrCategory::DoubleSlider:
				SetDoubleSlider();
				break;
			case AttrCategory::String:
				SetString();
				break;
			case AttrCategory::StringText:
				SetStringText();
				break;
			case AttrCategory::StringList:
				SetStringList();
				break;
			case AttrCategory::StringCombo:
				SetStringCombo();
				break;
			case AttrCategory::StringRadio:
				SetStringRadio();
				break;
			case AttrCategory::StringCheck:
				SetStringCheck();
				break;
			case AttrCategory::File:
				SetFile();
				break;
			case AttrCategory::Directory:
				SetDirectory();
				break;
			case AttrCategory::TCF:
				SetTCFViewer();
				break;
		}

		d->updating = false;
	}

	AttrWidget::~AttrWidget() = default;

	auto AttrWidget::SetModel(const AttrValue& model) -> void {
		if (d->model == model)
			return;

		d->updating = true;
		d->model = model;

		switch (d->category) {
			case AttrCategory::Null:
				return;
			case AttrCategory::CheckBox:
				UpdateCheckBoxModel();
				break;
			case AttrCategory::Integer:
				UpdateIntSpinModel();
				break;
			case AttrCategory::IntegerSlider:
				UpdateIntSliderModel();
				break;
			case AttrCategory::Double:
				UpdateDoubleSpinModel();
				break;
			case AttrCategory::DoubleSlider:
				UpdateDoubleSliderModel();
				break;
			case AttrCategory::String:
				UpdateStringModel();
				break;
			case AttrCategory::StringText:
				UpdateStringTextModel();
				break;
			case AttrCategory::StringList:
				UpdateStringListModel();
				break;
			case AttrCategory::StringCombo:
				UpdateStringComboModel();
				break;
			case AttrCategory::StringRadio:
				UpdateStringRadioModel();
				break;
			case AttrCategory::StringCheck:
				UpdateStringCheckModel();
				break;
			case AttrCategory::File:
				UpdateFileModel();
				break;
			case AttrCategory::Directory:
				UpdateDirectoryModel();
				break;
			case AttrCategory::TCF:
				UpdateTCFViewerModel();
				break;
		}

		d->updating = false;
	}

	auto AttrWidget::SetValue(const AttrValue& value) -> void {
		if (d->value == value)
			return;

		d->updating = true;
		d->value = value;

		switch (d->category) {
			case AttrCategory::Null:
				return;
			case AttrCategory::CheckBox:
				UpdateCheckBoxValue();
				break;
			case AttrCategory::Integer:
				UpdateIntSpinValue();
				break;
			case AttrCategory::IntegerSlider:
				UpdateIntSliderValue();
				break;
			case AttrCategory::Double:
				UpdateDoubleSpinValue();
				break;
			case AttrCategory::DoubleSlider:
				UpdateDoubleSliderValue();
				break;
			case AttrCategory::String:
				UpdateStringValue();
				break;
			case AttrCategory::StringText:
				UpdateStringTextValue();
				break;
			case AttrCategory::StringList:
				UpdateStringListValue();
				break;
			case AttrCategory::StringCombo:
				UpdateStringComboValue();
				break;
			case AttrCategory::StringRadio:
				UpdateStringRadioValue();
				break;
			case AttrCategory::StringCheck:
				UpdateStringCheckValue();
				break;
			case AttrCategory::File:
				UpdateFileValue();
				break;
			case AttrCategory::Directory:
				UpdateDirectoryValue();
				break;
			case AttrCategory::TCF:
				UpdateTCFViewerValue();
				break;
		}

		d->updating = false;
	}

	auto AttrWidget::GetModel() const -> AttrValue {
		return d->model;
	}

	auto AttrWidget::GetValue() const -> AttrValue {
		return d->value;
	}

	auto AttrWidget::SetCheckBox() -> void {
		auto* widget = new QFrame(this);
		auto* layout = new QHBoxLayout(widget);
		auto* check = new QCheckBox(widget);
		layout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
		layout->addWidget(check);
		layout->setContentsMargins(0, 0, 0, 0);
		check->setLayoutDirection(Qt::RightToLeft);

		connect(check, &QCheckBox::toggled, this, [this](bool current) {
			d->value = current;

			if (!d->updating)
				emit ValueChanged(current);
		});

		d->widget = check;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateCheckBoxValue() -> void {
		if (auto* widget = dynamic_cast<QCheckBox*>(d->widget)) {
			widget->blockSignals(true);
			widget->setChecked(d->value.toBool());
			widget->blockSignals(false);
		}
	}

	auto AttrWidget::UpdateCheckBoxModel() -> void {}

	auto AttrWidget::SetIntSlider() -> void {
		auto* widget = new QFrame(this);
		auto* layout = new QHBoxLayout(widget);
		auto* spin = new QSpinBox(widget);
		auto* slider = new QSlider(widget);

		widget->setLayout(layout);
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(9);
		layout->addWidget(slider);
		layout->addWidget(spin);

		slider->setOrientation(Qt::Horizontal);
		slider->setTickPosition(QSlider::NoTicks);

		connect(slider, &QSlider::valueChanged, this, [this, spin](int current) {
			spin->blockSignals(true);
			spin->setValue(current);
			spin->blockSignals(false);
			d->value = current;

			if (!d->updating)
				emit ValueChanged(current);
		});

		connect(spin, QOverload<int>::of(&QSpinBox::valueChanged), this, [this, slider](int current) {
			slider->blockSignals(true);
			slider->setValue(current);
			slider->blockSignals(false);
			d->value = current;

			if (!d->updating)
				emit ValueChanged(current);
		});

		d->widget = widget;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateIntSliderValue() -> void {
		if (const auto* frame = dynamic_cast<QFrame*>(d->widget)) {
			if (auto* slider = frame->findChild<QSlider*>()) {
				slider->blockSignals(true);
				slider->setValue(d->value.toInt());
				slider->blockSignals(false);
			}

			if (auto* spin = frame->findChild<QSpinBox*>()) {
				spin->blockSignals(true);
				spin->setValue(d->value.toInt());
				spin->blockSignals(false);
			}
		}
	}

	auto AttrWidget::UpdateIntSliderModel() -> void {
		if (const auto* frame = dynamic_cast<QFrame*>(d->widget)) {
			const auto model = d->model.toMap();
			const auto min = model.contains("Min") ? model["Min"].toInt() : 0;
			const auto max = model.contains("Max") ? model["Max"].toInt() : 100;
			const auto step = model.contains("Step") ? model["Step"].toInt() : 1;

			if (auto* slider = frame->findChild<QSlider*>()) {
				slider->setRange(min, max);
				slider->setSingleStep(step);
				slider->setPageStep(step);
			}

			if (auto* spin = frame->findChild<QSpinBox*>()) {
				spin->setRange(min, max);
				spin->setSingleStep(step);
			}
		}
	}

	auto AttrWidget::SetIntSpin() -> void {
		auto* widget = new QSpinBox(this);

		connect(widget, QOverload<int>::of(&QSpinBox::valueChanged), this, [this](int value) {
			d->value = value;

			if (!d->updating)
				emit ValueChanged(value);
		});

		d->widget = widget;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateIntSpinValue() -> void {
		if (auto* widget = dynamic_cast<QSpinBox*>(d->widget)) {
			widget->blockSignals(true);
			widget->setValue(d->value.toInt());
			widget->blockSignals(false);
		}
	}

	auto AttrWidget::UpdateIntSpinModel() -> void {
		if (auto* widget = dynamic_cast<QSpinBox*>(d->widget)) {
			const auto model = d->model.toMap();
			const auto min = model.contains("Min") ? model["Min"].toInt() : 0;
			const auto max = model.contains("Max") ? model["Max"].toInt() : 100;
			const auto step = model.contains("Step") ? model["Step"].toInt() : 1;

			widget->setRange(min, max);
			widget->setSingleStep(step);
		}
	}

	auto AttrWidget::SetDoubleSpin() -> void {
		auto* widget = new QDoubleSpinBox(this);

		connect(widget, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double value) {
			d->value = value;

			if (!d->updating)
				emit ValueChanged(value);
		});

		d->widget = widget;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateDoubleSpinValue() -> void {
		if (auto* widget = dynamic_cast<QDoubleSpinBox*>(d->widget)) {
			widget->blockSignals(true);
			widget->setValue(d->value.toDouble());
			widget->blockSignals(false);
		}
	}

	auto AttrWidget::UpdateDoubleSpinModel() -> void {
		if (auto* widget = dynamic_cast<QDoubleSpinBox*>(d->widget)) {
			const auto model = d->model.toMap();
			const auto min = model.contains("Min") ? model["Min"].toDouble() : 0.0;
			const auto max = model.contains("Max") ? model["Max"].toDouble() : 1.0;
			const auto step = model.contains("Step") ? model["Step"].toDouble() : 0.1;
			const auto decimals = model.contains("Decimals") ? model["Decimals"].toInt() : 2;

			widget->setRange(min, max);
			widget->setSingleStep(step);
			widget->setDecimals(decimals);
		}
	}

	auto AttrWidget::SetDoubleSlider() -> void {
		auto* widget = new QFrame(this);
		auto* layout = new QHBoxLayout(widget);
		auto* slider = new QSlider(widget);
		auto* spin = new QDoubleSpinBox(widget);

		widget->setLayout(layout);
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(9);
		layout->addWidget(slider);
		layout->addWidget(spin);

		slider->setMinimum(0);
		slider->setMaximum(100);
		slider->setRange(0, 100);
		slider->setSingleStep(1);

		slider->setValue(0);
		slider->setOrientation(Qt::Horizontal);
		slider->setTickPosition(QSlider::NoTicks);

		connect(slider, &QSlider::valueChanged, this, [this, spin](int val) {
			const auto model = d->model.toMap();
			const auto decimals = model.contains("Decimals") ? model["Decimals"].toInt() : 2;
			const auto log = std::pow(0.1, decimals);

			spin->blockSignals(true);
			spin->setValue(val * log);
			spin->blockSignals(false);
			d->value = val * log;

			if (!d->updating)
				emit ValueChanged(val * log);
		});

		connect(spin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this, slider](double val) {
			const auto model = d->model.toMap();
			const auto decimals = model.contains("Decimals") ? model["Decimals"].toInt() : 2;
			const auto log = std::pow(10, decimals);

			slider->blockSignals(true);
			slider->setValue(static_cast<int>(val * log));
			slider->blockSignals(false);
			d->value = val;

			if (!d->updating)
				emit ValueChanged(val * log);
		});

		d->widget = widget;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateDoubleSliderValue() -> void {
		if (const auto* frame = dynamic_cast<QFrame*>(d->widget)) {
			const auto model = d->model.toMap();
			const auto decimals = model.contains("Decimals") ? model["Decimals"].toInt() : 2;

			if (auto* slider = frame->findChild<QSlider*>()) {
				const auto log = std::pow(10, decimals);

				slider->blockSignals(true);
				slider->setValue(static_cast<int>(d->value.toDouble() * log));
				slider->blockSignals(false);
			}

			if (auto* spin = frame->findChild<QDoubleSpinBox*>()) {
				spin->blockSignals(true);
				spin->setValue(d->value.toDouble());
				spin->blockSignals(false);
			}
		}
	}

	auto AttrWidget::UpdateDoubleSliderModel() -> void {
		if (const auto* frame = dynamic_cast<QFrame*>(d->widget)) {
			const auto model = d->model.toMap();
			const auto min = model.contains("Min") ? model["Min"].toDouble() : 0;
			const auto max = model.contains("Max") ? model["Max"].toDouble() : 1.0;
			const auto step = model.contains("Step") ? model["Step"].toDouble() : 0.1;
			const auto decimals = model.contains("Decimals") ? model["Decimals"].toInt() : 2;
			const auto log = std::pow(10, decimals);

			if (auto* slider = frame->findChild<QSlider*>()) {
				slider->setMinimum(static_cast<int>(min * log));
				slider->setMaximum(static_cast<int>(max * log));
				slider->setSingleStep(static_cast<int>(step * log));
				slider->setPageStep(static_cast<int>(step * log));
			}

			if (auto* spin = frame->findChild<QDoubleSpinBox*>()) {
				spin->setMinimum(min);
				spin->setMaximum(max);
				spin->setSingleStep(step);
				spin->setDecimals(decimals);
			}
		}
	}

	auto AttrWidget::SetString() -> void {
		auto* widget = new QLineEdit(this);

		connect(widget, &QLineEdit::textChanged, this, [this, widget]() {
			const auto current = widget->text();
			d->value = current;

			if (!d->updating)
				emit ValueChanged(current);
		});

		d->widget = widget;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateStringValue() -> void {
		if (auto* widget = dynamic_cast<QLineEdit*>(d->widget)) {
			widget->blockSignals(true);
			widget->setText(d->value.toString());
			widget->blockSignals(false);
		}
	}

	auto AttrWidget::UpdateStringModel() -> void {
		if (auto* widget = dynamic_cast<QLineEdit*>(d->widget)) {
			const auto model = d->model.toMap();
			const auto maxLength = model.contains("MaxLength") ? model["MaxLength"].toInt() : -1;
			const auto placeholder = model.contains("Placeholder") ? model["Placeholder"].toString() : QString();

			widget->setMaxLength(maxLength);
			widget->setPlaceholderText(placeholder);
		}
	}

	auto AttrWidget::SetStringText() -> void {
		auto* widget = new QTextEdit(this);

		connect(widget, &QTextEdit::textChanged, this, [this, widget]() {
			d->value = widget->toPlainText();

			if (!d->updating)
				emit ValueChanged(d->value);
		});

		d->widget = widget;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateStringTextValue() -> void {
		if (auto* widget = dynamic_cast<QTextEdit*>(d->widget)) {
			widget->blockSignals(true);
			widget->setText(d->value.toString());
			widget->blockSignals(false);
		}
	}

	auto AttrWidget::UpdateStringTextModel() -> void { }

	auto AttrWidget::SetStringList() -> void {
		auto* widget = new QListWidget(this);

		connect(widget->selectionModel(), &QItemSelectionModel::selectionChanged, this, [this, widget] {
			if (const auto* current = widget->currentItem())
				d->value = current->text();
			else
				d->value = QString();

			if (!d->updating)
				emit ValueChanged(d->value);
		});

		d->widget = widget;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateStringListValue() -> void {
		if (auto* widget = dynamic_cast<QListWidget*>(d->widget)) {
			const auto model = d->model.toStringList();
			widget->blockSignals(true);
			widget->setCurrentRow(model.indexOf(d->value.toString()));
			widget->blockSignals(false);
		}
	}

	auto AttrWidget::UpdateStringListModel() -> void {
		if (auto* widget = dynamic_cast<QListWidget*>(d->widget)) {
			const auto model = d->model.toStringList();

			widget->clear();
			widget->addItems(model);
			widget->setCurrentRow(model.indexOf(d->value.toString()));
			widget->setFixedHeight(widget->sizeHintForRow(0) * widget->count());
		}
	}

	auto AttrWidget::SetStringCombo() -> void {
		auto* widget = new QComboBox(this);

		connect(widget, QOverload<int>::of(&QComboBox::currentIndexChanged), this, [this](int current) {
			if (const auto model = d->model.toStringList(); current < model.count() && current > -1)
				d->value = model[current];
			else
				d->value = QString();

			if (!d->updating)
				emit ValueChanged(d->value);
		});

		d->widget = widget;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateStringComboValue() -> void {
		if (auto* widget = dynamic_cast<QComboBox*>(d->widget)) {
			const auto model = d->model.toStringList();

			widget->blockSignals(true);
			widget->setCurrentIndex(model.indexOf(d->value.toString()));
			widget->blockSignals(false);
		}
	}

	auto AttrWidget::UpdateStringComboModel() -> void {
		if (auto* widget = dynamic_cast<QComboBox*>(d->widget)) {
			const auto model = d->model.toStringList();
			widget->clear();

			for (const auto& s : model)
				widget->addItem(s);
		}
	}

	auto AttrWidget::SetStringRadio() -> void {
		auto* widget = new QFrame(this);
		auto* layout = new QVBoxLayout(widget);

		widget->setLayout(layout);
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(9);

		d->widget = widget;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateStringRadioValue() -> void {
		UpdateStringRadioModel();
	}

	auto AttrWidget::UpdateStringRadioModel() -> void {
		if (auto* widget = dynamic_cast<QFrame*>(d->widget)) {
			qDeleteAll(widget->findChildren<QRadioButton*>(QString(), Qt::FindDirectChildrenOnly));

			const auto model = d->model.toStringList();
			auto height = 0;

			for (auto i = 0; i < model.size(); i++) {
				auto* radio = new QRadioButton(model[i], widget);
				widget->layout()->addWidget(radio);
				height += radio->sizeHint().height();
				height += widget->layout()->spacing();

				if (model[i] == d->value.toString())
					radio->setChecked(true);

				connect(radio, &QRadioButton::toggled, this, [this, value = model[i]] {
					d->value = value;

					if (!d->updating)
						emit ValueChanged(value);
				});
			}

			widget->setFixedHeight(height);
		}
	}

	auto AttrWidget::SetStringCheck() -> void {
		auto* widget = new QFrame(this);
		auto* layout = new QVBoxLayout(widget);

		widget->setLayout(layout);
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(9);

		d->widget = widget;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateStringCheckValue() -> void {
		UpdateStringCheckModel();
	}

	auto AttrWidget::UpdateStringCheckModel() -> void {
		if (auto* widget = dynamic_cast<QFrame*>(d->widget)) {
			qDeleteAll(widget->findChildren<QCheckBox*>(QString(), Qt::FindDirectChildrenOnly));

			const auto model = d->model.toStringList();
			auto height = 0;

			for (auto i = 0; i < model.size(); i++) {
				auto* Check = new QCheckBox(model[i], widget);
				widget->layout()->addWidget(Check);
				height += Check->sizeHint().height();
				height += widget->layout()->spacing();

				if (d->value.toStringList().contains(model[i]))
					Check->setChecked(true);

				connect(Check, &QCheckBox::toggled, this, [this, value = model[i]](bool checked) {
					auto list = d->value.toStringList();
					if (checked)
						list.push_back(value);
					else
						list.removeOne(value);
					d->value = list;

					if (!d->updating)
						emit ValueChanged(list);
				});
			}

			widget->setFixedHeight(height);
		}
	}

	auto AttrWidget::SetFile() -> void {
		auto* widget = new QFrame(this);
		auto* line = new QLineEdit(widget);
		auto* btn = new QPushButton("..", widget);

		widget->setLayout(new QHBoxLayout(widget));
		widget->layout()->setContentsMargins(0, 0, 0, 0);
		widget->layout()->addWidget(line);
		widget->layout()->addWidget(btn);
		line->setReadOnly(true);

		connect(btn, &QPushButton::clicked, [this, widget, line] {
			const auto model = d->model.toMap();
			const auto header = model.contains("Title") ? model["Title"].toString() : "Open File";
			const auto path = model.contains("Path") ? model["Path"].toString() : ".";
			const auto filter = model.contains("Filter") ? model["Filter"].toString() : "*.*";

			line->setText(QFileDialog::getOpenFileName(widget, header, path, filter));
		});

		connect(line, &QLineEdit::textChanged, this, [this](const QString& current) {
			d->value = current;

			if (!d->updating)
				emit ValueChanged(current);
		});

		d->widget = line;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateFileValue() -> void {
		if (auto* widget = dynamic_cast<QLineEdit*>(d->widget)) {
			widget->blockSignals(true);
			widget->setText(d->value.toString());
			widget->blockSignals(false);
		}
	}

	auto AttrWidget::UpdateFileModel() -> void { }

	auto AttrWidget::SetDirectory() -> void {
		auto* widget = new QFrame(this);
		auto* line = new QLineEdit(widget);
		auto* btn = new QPushButton("..", widget);

		widget->setLayout(new QHBoxLayout(widget));
		widget->layout()->setContentsMargins(0, 0, 0, 0);
		widget->layout()->addWidget(line);
		widget->layout()->addWidget(btn);
		line->setReadOnly(true);

		connect(btn, &QPushButton::clicked, [this, widget, line] {
			const auto model = d->model.toMap();
			const auto header = model.contains("Title") ? model["Title"].toString() : "Open Directory";
			const auto path = model.contains("Path") ? model["Path"].toString() : ".";

			line->setText(QFileDialog::getExistingDirectory(widget, header, path));
		});

		connect(line, &QLineEdit::textChanged, this, [this](const QString& current) {
			d->value = current;

			if (!d->updating)
				emit ValueChanged(current);
		});

		d->widget = line;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateDirectoryValue() -> void {
		if (auto* widget = dynamic_cast<QLineEdit*>(d->widget)) {
			widget->blockSignals(true);
			widget->setText(d->value.toString());
			widget->blockSignals(false);
		}
	}

	auto AttrWidget::UpdateDirectoryModel() -> void { }

	auto AttrWidget::SetTCFViewer() -> void {
		auto* widget = new QFrame(this);
		auto* line = new QLineEdit(widget);
		auto* btn = new QPushButton("..", widget);

		widget->setLayout(new QHBoxLayout(widget));
		widget->layout()->setContentsMargins(0, 0, 0, 0);
		widget->layout()->addWidget(line);
		widget->layout()->addWidget(btn);
		line->setReadOnly(true);

		connect(btn, &QPushButton::clicked, [this, line] {
			if (IO::Widget::TCFDialog viewer; viewer.exec() == QDialog::Accepted)
				line->setText(viewer.GetSelected().first());
		});

		connect(line, &QLineEdit::textChanged, this, [this](const QString& current) {
			d->value = current;

			if (!d->updating)
				emit ValueChanged(current);
		});

		d->widget = line;
		d->ui.contentLayout->addWidget(widget);
	}

	auto AttrWidget::UpdateTCFViewerValue() -> void {
		if (auto* widget = dynamic_cast<QLineEdit*>(d->widget)) {
			widget->blockSignals(true);
			widget->setText(d->value.toString());
			widget->blockSignals(false);
		}
	}

	auto AttrWidget::UpdateTCFViewerModel() -> void {}
}
