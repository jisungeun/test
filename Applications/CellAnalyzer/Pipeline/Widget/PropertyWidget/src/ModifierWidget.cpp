#include "ModifierWidget.h"

#include "ui_ModifierWidget.h"

namespace CellAnalyzer::Pipeline::Widget {
	struct ModifierWidget::Impl {
		Ui::ModifierWidget ui;

		AttrModifier modifier = AttrModifier::ReadWrite;

		auto GetToolTip() const -> QString;
	};

	auto ModifierWidget::Impl::GetToolTip() const -> QString {
		switch (modifier) {
			case AttrModifier::ReadWrite:
				return "The attribute is Readable and writable";
			case AttrModifier::ReadOnly:
				return "The attribute is only readable";
			case AttrModifier::Invisible:
				return "The attribute is hidden";
			default:
				return {};
		}
	}

	ModifierWidget::ModifierWidget(QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);

		connect(d->ui.checkBox, &QCheckBox::stateChanged, this, &ModifierWidget::OnStateChanged);
	}

	ModifierWidget::~ModifierWidget() = default;

	auto ModifierWidget::GetModifier() const -> AttrModifier {
		return d->modifier;
	}

	auto ModifierWidget::SetModifier(AttrModifier modifier) -> void {
		d->modifier = modifier;

		switch (modifier) {
			case AttrModifier::ReadWrite:
				d->ui.checkBox->setCheckState(Qt::Unchecked);
				break;
			case AttrModifier::ReadOnly:
				d->ui.checkBox->setCheckState(Qt::PartiallyChecked);
				break;
			case AttrModifier::Invisible:
				d->ui.checkBox->setCheckState(Qt::Checked);
				break;
		}

		this->setToolTip(d->GetToolTip());
	}

	auto ModifierWidget::OnStateChanged(int state) -> void {
		switch (state) {
			case Qt::Unchecked:
				d->modifier = AttrModifier::ReadWrite;
				break;
			case Qt::PartiallyChecked:
				d->modifier = AttrModifier::ReadOnly;
				break;
			case Qt::Checked:
				d->modifier = AttrModifier::Invisible;
				break;
		}

		this->setToolTip(d->GetToolTip());
		emit ModifierChanged(d->modifier);
	}
}
