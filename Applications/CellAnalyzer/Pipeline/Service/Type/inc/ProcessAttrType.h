#pragma once

#include "IProcessAttrType.h"

#include "CellAnalyzer.Pipeline.Service.TypeExport.h"

namespace CellAnalyzer::Pipeline::Service {
	class CellAnalyzer_Pipeline_Service_Type_API ProcessAttrType final : public IProcessAttrType {
	public:
		explicit ProcessAttrType(const QVariantMap& typeInfo);
		~ProcessAttrType() override;

		auto IsValid() const -> bool;

		auto GetID() const -> QString override;
		auto GetDescription() const -> QString override;
		auto GetCategory() const -> AttrCategory override;
		auto GetGroup() const -> QString override;
		auto IsAsync() const -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
