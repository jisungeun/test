#pragma once

#include "IProcessType.h"

#include "CellAnalyzer.Pipeline.Service.TypeExport.h"

namespace CellAnalyzer::Pipeline::Service {
	class CellAnalyzer_Pipeline_Service_Type_API ProcessType final : public IProcessType {
	public:
		explicit ProcessType(const QVariantMap& typeInfo);
		~ProcessType() override;

		auto IsValid() const -> bool;

		auto GetID() const -> QString override;
		auto GetDescription() const -> QString override;
		auto GetLicense() const -> QString override;
		auto GetCategory() const -> ProcessCategory override;

		auto ContainsInputType(const QString& id) const -> bool override;
		auto GetInputTypeList() const -> QStringList override;
		auto GetInputFlags(const QString& id) const -> DataFlags override;

		auto ContainsOutputType(const QString& id) const -> bool override;
		auto GetOutputTypeList() const -> QStringList override;
		auto GetOutputFlags(const QString& id) const -> DataFlags override;

		auto ContainsAttributeType(const QString& id) const -> bool override;
		auto GetAttributeTypeList() const -> QStringList override;
		auto GetAttributeType(const QString& id) const -> ProcessAttrTypePtr override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
