#pragma once

#include "IAppModule.h"

#include "IProcessService.h"

#include "IServiceProvider.h"

#include "CellAnalyzer.Pipeline.Service.TypeExport.h"

namespace CellAnalyzer::Pipeline::Service {
	class CellAnalyzer_Pipeline_Service_Type_API ProcessService final : public IAppModule, public IProcessService {
	public:
		explicit ProcessService(Tomocube::IServiceProvider* provider);
		~ProcessService() override;

		auto Start() -> std::optional<Error> override;
		auto Stop() -> void override;

		auto GetList() const -> QStringList override;
		auto GetType(const QString& id) const -> ProcessTypePtr override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
