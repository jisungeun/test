#include "IProcessorService.h"

#include "ProcessService.h"

namespace CellAnalyzer::Pipeline::Service {
	struct ProcessService::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
	};

	ProcessService::ProcessService(Tomocube::IServiceProvider* provider) : IProcessService(), d(new Impl) {
		d->provider = provider;
	}

	ProcessService::~ProcessService() = default;

	auto ProcessService::Start() -> std::optional<Error> {
		return {};
	}

	auto ProcessService::Stop() -> void {}

	auto ProcessService::GetList() const -> QStringList {
		const auto service = d->provider->GetService<Processor::IProcessorService>();
		return service->GetTypeList();
	}

	auto ProcessService::GetType(const QString& id) const -> ProcessTypePtr {
		const auto service = d->provider->GetService<Processor::IProcessorService>();
		return service->GetTypeInfo(id);
	}
}
