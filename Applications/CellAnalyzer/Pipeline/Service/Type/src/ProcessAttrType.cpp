#include "ProcessAttrType.h"

namespace CellAnalyzer::Pipeline::Service {
	struct ProcessAttrType::Impl {
		QString id;
		QString desc;
		QString group;
		AttrCategory category = AttrCategory::Null;
		bool async = false;
	};

	ProcessAttrType::ProcessAttrType(const QVariantMap& typeInfo) : IProcessAttrType(), d(new Impl) {
		d->id = typeInfo["ID"].toString();
		d->desc = typeInfo["Description"].toString();
		d->group = typeInfo["Group"].toString();
		d->category = ToAttrCategory(typeInfo["Category"].toString());
		d->async = typeInfo["IsAsync"].toBool();
	}

	ProcessAttrType::~ProcessAttrType() = default;

	auto ProcessAttrType::IsValid() const -> bool {
		return !d->id.isEmpty() && d->category != AttrCategory::Null;
	}

	auto ProcessAttrType::GetID() const -> QString {
		return d->id;
	}

	auto ProcessAttrType::GetDescription() const -> QString {
		return d->desc;
	}

	auto ProcessAttrType::GetCategory() const -> AttrCategory {
		return d->category;
	}

	auto ProcessAttrType::GetGroup() const -> QString {
		return d->group;
	}

	auto ProcessAttrType::IsAsync() const -> bool {
		return d->async;
	}
}
