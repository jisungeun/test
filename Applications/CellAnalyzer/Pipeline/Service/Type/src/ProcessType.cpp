#include "ProcessType.h"

#include "ProcessAttrType.h"

namespace CellAnalyzer::Pipeline::Service {
	struct ProcessType::Impl {
		QString id;
		QString desc;
		QString license;
		ProcessCategory category = ProcessCategory::Null;
		QMap<QString, DataFlags> inputMap;
		QMap<QString, DataFlags> outputMap;
		QList<ProcessAttrTypePtr> attrList;
	};

	ProcessType::ProcessType(const QVariantMap& typeInfo) : IProcessType(), d(new Impl) {
		d->id = typeInfo["ID"].toString();
		d->desc = typeInfo["Description"].toString();
		d->license = typeInfo["License"].toString();
		d->category = ToProcessCategory(typeInfo["Category"].toString());

		for (const auto& type : typeInfo["InputList"].toList()) {
			const auto map = type.toMap();
			d->inputMap[map["ID"].toString()] = ToDataFlags(map["Flags"].toStringList());
		}

		for (const auto& type : typeInfo["OutputList"].toList()) {
			const auto map = type.toMap();
			d->outputMap[map["ID"].toString()] = ToDataFlags(map["Flags"].toStringList());
		}

		for (const auto& type : typeInfo["AttrList"].toList()) {
			if (const auto attr = std::make_shared<ProcessAttrType>(type.toMap()); attr->IsValid())
				d->attrList.push_back(attr);
		}
	}

	ProcessType::~ProcessType() = default;

	auto ProcessType::IsValid() const -> bool {
		return !d->id.isEmpty() && !d->inputMap.isEmpty() && !d->outputMap.isEmpty();
	}

	auto ProcessType::GetID() const -> QString {
		return d->id;
	}

	auto ProcessType::GetDescription() const -> QString {
		return d->desc;
	}

	auto ProcessType::GetLicense() const -> QString {
		return d->license;
	}

	auto ProcessType::GetCategory() const -> ProcessCategory {
		return d->category;
	}

	auto ProcessType::ContainsInputType(const QString& id) const -> bool {
		return d->inputMap.contains(id);
	}

	auto ProcessType::GetInputTypeList() const -> QStringList {
		return d->inputMap.keys();
	}

	auto ProcessType::GetInputFlags(const QString& id) const -> DataFlags {
		if (d->inputMap.contains(id))
			return d->inputMap[id];

		return {};
	}

	auto ProcessType::ContainsOutputType(const QString& id) const -> bool {
		return d->outputMap.contains(id);
	}

	auto ProcessType::GetOutputTypeList() const -> QStringList {
		return d->outputMap.keys();
	}

	auto ProcessType::GetOutputFlags(const QString& id) const -> DataFlags {
		if (d->outputMap.contains(id))
			return d->outputMap[id];

		return {};
	}

	auto ProcessType::ContainsAttributeType(const QString& id) const -> bool {
		return GetAttributeType(id) != nullptr;
	}

	auto ProcessType::GetAttributeTypeList() const -> QStringList {
		QStringList list;

		for (const auto& i : d->attrList)
			list.push_back(i->GetID());

		return list;
	}

	auto ProcessType::GetAttributeType(const QString& id) const -> ProcessAttrTypePtr {
		for (const auto& i : d->attrList) {
			if (i->GetID() == id)
				return i;
		}

		return {};
	}
}
