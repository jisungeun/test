#pragma once

#include "IPipeline.h"
#include "IProcessorService.h"

#include "CellAnalyzer.Pipeline.ServiceExport.h"

namespace CellAnalyzer::Pipeline::Service {
	class CellAnalyzer_Pipeline_Service_API Process final : public std::enable_shared_from_this<Process>, public IProcess {
	public:
		explicit Process(const QString& name, const ProcessTypePtr& type, const PipelinePtr& parent);
		~Process() override;

		auto Initialize() -> bool;
		auto Invalidate() -> void;
		auto SetProcessor(const Processor::ProcessorPtr& processor) -> void;

		auto GetName() const -> QString override;
		auto GetID() const -> QString override;
		auto GetParent() const -> PipelinePtr override;
		auto GetCategory() const -> ProcessCategory override;
		auto SetName(const QString& name) -> bool override;

		auto GetPrecedents(bool includeSelf) const -> ProcessList override;

		auto GetInputList() const -> QStringList override;
		auto GetInput(const QString& id) const -> ProcessInputPtr override;

		auto GetOutputList() const -> QStringList override;
		auto GetOutput(const QString& id) const -> ProcessOutputPtr override;

		auto GetAttributeList() const -> QStringList override;
		auto GetAttribute(const QString& id) const -> ProcessAttrPtr override;

		auto IsExecutable() const -> bool override;
		auto IsExecuted() const -> bool override;
		auto Execute() -> bool override;

	protected slots:
		auto OnDataLinked() -> void;
		auto OnDataChanged() -> void;
		auto OnValueChanged(const QString& id, const AttrValue& value) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
