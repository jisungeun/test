#pragma once

#include "IProcessAttr.h"
#include "IProcessor.h"
#include "IProcessorAttr.h"

#include "CellAnalyzer.Pipeline.ServiceExport.h"

namespace CellAnalyzer::Pipeline::Service {
	class CellAnalyzer_Pipeline_Service_API ProcessAttr final : public QObject, public IProcessAttr, public Processor::IProcessorAttr {
		Q_OBJECT

	public:
		explicit ProcessAttr(const ProcessAttrTypePtr& type, const ProcessPtr& parent);
		~ProcessAttr() override;

		auto GetID() const -> QString override;
		auto GetDescription() const -> QString override;
		auto GetCategory() const -> AttrCategory override;
		auto GetGroup() const -> QString override;
		auto GetParent() const -> ProcessPtr override;
		auto IsAsync() const -> bool override;

		auto GetModifier() const -> AttrModifier override;
		auto GetState() const -> AttrState override;
		auto GetModel() const -> AttrValue override;
		auto GetValue() const -> AttrValue override;

		auto SetModifier(AttrModifier modifier) -> void override;
		auto SetValue(const AttrValue& value) -> void override;

		auto GetAttrModel() const -> Processor::ProcessorAttrModel override;
		auto GetAttrValue() const -> Processor::ProcessorAttrValue override;
		auto GetAttrStyle() const -> Processor::ProcessorAttrStyle override;

		auto SetAttrModel(const Processor::ProcessorAttrModel& model) -> void override;
		auto SetAttrValue(const Processor::ProcessorAttrValue& value) -> void override;
		auto SetAttrStyle(Processor::ProcessorAttrStyle style) -> void override;

	signals:
		auto ValueChanged(const QString& id, const AttrValue& value) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
