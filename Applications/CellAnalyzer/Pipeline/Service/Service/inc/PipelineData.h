#pragma once
#pragma warning(disable: 4250)

#include <QObject>

#include "IPipelineData.h"

#include "CellAnalyzer.Pipeline.ServiceExport.h"

namespace CellAnalyzer::Pipeline::Service {
	class CellAnalyzer_Pipeline_Service_API PipelineData : public QObject, public virtual IPipelineData {
		Q_OBJECT

	public:
		explicit PipelineData(const QString& name);
		~PipelineData() override;

		auto Invalidate() -> void;

		auto GetName() const -> QString override;
		auto GetData() const -> DataPtr override;
		auto ExistsData() const -> bool override;

		auto IsAutoSave() const -> bool override;
		auto SetAutoSave(bool savable) -> void override;

		auto SetName(const QString& name) -> bool override;
		auto SetData(const DataPtr& data) -> bool override;

	protected:
		virtual auto IsValidDataName(const QString& name) const -> bool = 0;
		virtual auto InvalidateParent() -> void = 0;

	signals:
		auto DataChanged() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
