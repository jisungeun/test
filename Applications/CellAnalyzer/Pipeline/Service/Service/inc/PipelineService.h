#pragma once

#include "IAppModule.h"

#include "IPipelineService.h"
#include "IServiceProvider.h"

#include "CellAnalyzer.Pipeline.ServiceExport.h"

namespace CellAnalyzer::Pipeline::Service {
	class CellAnalyzer_Pipeline_Service_API PipelineService final : public IAppModule, public IPipelineService {
	public:
		explicit PipelineService(Tomocube::IServiceProvider* provider);
		~PipelineService() override;

		auto Start() -> std::optional<Error> override;
		auto Stop() -> void override;

		auto Create(const QString& url) -> PipelinePtr override;
		auto Read(const QString& url) const -> PipelinePtr override;
		auto Save(const PipelinePtr& pipeline) -> bool override;
		auto Copy(const PipelinePtr& pipeline, const QString& url) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
