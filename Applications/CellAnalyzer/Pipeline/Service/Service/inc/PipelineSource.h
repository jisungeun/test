#pragma once

#include "IPipeline.h"
#include "PipelineData.h"

#include "CellAnalyzer.Pipeline.ServiceExport.h"

namespace CellAnalyzer::Pipeline::Service {
	class CellAnalyzer_Pipeline_Service_API PipelineSource final : public PipelineData, public IPipelineSource {
	public:
		explicit PipelineSource(const QString& name, DataFlags flags, const PipelinePtr& parent);
		~PipelineSource() override;

		auto GetFlags() const -> DataFlags override;
		auto GetParent() const -> PipelinePtr override;

	protected:
		auto IsValidDataName(const QString& name) const -> bool override;
		auto InvalidateParent() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
