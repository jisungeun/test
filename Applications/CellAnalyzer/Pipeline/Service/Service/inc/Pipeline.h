#pragma once

#include "IPipeline.h"
#include "IServiceProvider.h"

#include "PipelineSource.h"

#include "CellAnalyzer.Pipeline.ServiceExport.h"

namespace CellAnalyzer::Pipeline::Service {
	class CellAnalyzer_Pipeline_Service_API Pipeline final : public QObject, public std::enable_shared_from_this<Pipeline>, public IPipeline {
	public:
		explicit Pipeline(const QString& filepath, Tomocube::IServiceProvider* provider);
		~Pipeline() override;

		auto Initialize(const QVariantMap& format) -> bool;
		auto ToVariantMap() const -> QVariantMap;

		auto GetLocation() const -> QString override;
		auto GetErrorList() const -> QStringList override;

		auto GetName() const -> QString override;
		auto GetDescription() const -> QString override;
		auto GetCreationDateTime() const -> QDateTime override;
		auto GetAuthor() const -> QString override;

		auto SetDescription(const QString& desc) -> void override;
		auto SetAuthor(const QString& author) -> void override;

		auto ContainsProcess(const QString& name) const -> bool override;
		auto ContainsData(const QString& name) const -> bool override;
		auto ContainsSource(const QString& name) const -> bool override;
		auto ContainsOutput(const QString& name) const -> bool override;

		auto GetProcessList() const -> QStringList override;
		auto GetProcess(const QString& name) const -> ProcessPtr override;
		auto GetDataList() const -> QStringList override;
		auto GetData(const QString& name) const -> PipelineDataPtr override;
		auto GetSourceList() const -> QStringList override;
		auto GetSource(const QString& name) const -> PipelineSourcePtr override;
		auto GetOutputList() const -> QStringList override;
		auto GetOutput(const QString& name) const -> ProcessOutputPtr override;

		auto AddProcess(const QString& name, const ProcessTypePtr& type) -> ProcessPtr override;
		auto AddSource(const QString& name, DataFlags flags) -> PipelineSourcePtr override;
		auto MoveProcess(const ProcessPtr& process, int index) -> void override;
		auto RemoveProcess(const QString& name) -> void override;
		auto RemoveSource(const QString& name) -> void override;

		auto Load() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
