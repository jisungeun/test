#pragma once

#include "IProcessOutput.h"
#include "PipelineData.h"

#include "CellAnalyzer.Pipeline.ServiceExport.h"

namespace CellAnalyzer::Pipeline::Service {
	class CellAnalyzer_Pipeline_Service_API ProcessOutput final : public PipelineData, public IProcessOutput {
	public:
		explicit ProcessOutput(const QString& name, DataFlags flags, const ProcessPtr& parent);
		~ProcessOutput() override;

		auto GetFlags() const -> DataFlags override;
		auto GetParent() const -> ProcessPtr override;

	protected:
		auto IsValidDataName(const QString& name) const -> bool override;
		auto InvalidateParent() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
