#pragma once

#include "IProcess.h"

#include "CellAnalyzer.Pipeline.ServiceExport.h"

namespace CellAnalyzer::Pipeline::Service {
	class CellAnalyzer_Pipeline_Service_API ProcessInput final : public QObject, public IProcessInput {
		Q_OBJECT

	public:
		explicit ProcessInput(DataFlags flags, const ProcessPtr& parent);
		~ProcessInput() override;

		auto Invalidate() -> void;

		auto GetFlags() const -> DataFlags override;
		auto GetParent() const -> ProcessPtr override;
		auto IsSource() const -> bool override;
		auto IsLinked() const -> bool override;
		auto IsLinkable(const PipelineDataPtr& data) const -> bool override;

		auto GetLinked() const -> PipelineDataPtr override;
		auto GetLinkedAsSource() const -> PipelineSourcePtr override;
		auto GetLinkedAsOutput() const -> ProcessOutputPtr override;
		auto GetLinkedName() const -> QString override;
		auto GetLinkedData() const -> DataPtr override;
		auto GetLinkableDataList() const -> PipelineDataList override;

		auto Link(const PipelineDataPtr& data) -> bool override;

	signals:
		auto DataLinked() -> void;
		auto DataChanged() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
