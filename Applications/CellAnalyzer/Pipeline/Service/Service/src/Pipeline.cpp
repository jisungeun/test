#include <QFile>

#include "Pipeline.h"

#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "IProcessService.h"
#include "Process.h"

namespace CellAnalyzer::Pipeline::Service {
	struct Pipeline::Impl {
		Tomocube::IServiceProvider* provider = nullptr;

		QStringList error;
		QString filepath;
		QString desc;
		QString author;
		QDateTime created;
		bool load = false;

		QVector<std::shared_ptr<Process>> processes;
		QVector<std::shared_ptr<PipelineSource>> sources;
	};

	Pipeline::Pipeline(const QString& filepath, Tomocube::IServiceProvider* provider) : QObject(), std::enable_shared_from_this<Pipeline>(), IPipeline(), d(new Impl) {
		d->filepath = filepath;
		d->provider = provider;
		d->created = QDateTime::currentDateTime();
	}

	Pipeline::~Pipeline() = default;

	auto Pipeline::Initialize(const QVariantMap& format) -> bool {
		d->desc = format["Description"].toString();
		d->author = format["Author"].toString();
		d->created = QDateTime::fromMSecsSinceEpoch(format["CreatedAt"].toLongLong());

		for (const auto& i : format["SourceList"].toList()) {
			const auto map = i.toMap();
			const auto name = map["DataName"].toString();
			const auto flags = ToDataFlags(map["Flags"].toStringList());
			const auto savable = map["Savable"].toBool();

			if (flags == DataFlags(DataFlag::Null))
				d->error.push_back(QString("Source has null requirement: %1").arg(name));

			if (ContainsData(name))
				d->error.push_back(QString("Data name is duplicated: %1").arg(name));
			else {
				auto source = std::make_shared<PipelineSource>(name, flags, shared_from_this());
				source->SetAutoSave(savable);
				d->sources.push_back(source);
			}
		}

		for (const auto& i : format["ProcessList"].toList()) {
			const auto service = d->provider->GetService<IProcessService>();
			const auto procMap = i.toMap();
			const auto procId = procMap["ProcessID"].toString();
			const auto procName = procMap["ProcessName"].toString();
			const auto type = service->GetType(procId);

			if (type == nullptr)
				d->error.push_back(QString("Processor is not found: %1(%2)").arg(procId).arg(procId));

			if (ContainsProcess(procName))
				d->error.push_back(QString("Process name is duplicated: %1(%2)").arg(procName).arg(procId));

			if (const auto process = AddProcess(procName, type)) {
				for (const auto& j : procMap["OutputList"].toList()) {
					const auto map = j.toMap();
					const auto id = map["OutputID"].toString();
					const auto name = map["DataName"].toString();
					const auto savable = map["Savable"].toBool();

					if (const auto output = process->GetOutput(id)) {
						output->SetAutoSave(savable);

						if (!output->SetName(name))
							d->error.push_back(QString("Process output name is duplicated: %1(%2)").arg(procName).arg(procId));
					} else
						d->error.push_back(QString("Process output is not correct: %1(%2)").arg(procName).arg(procId));
				}

				for (const auto& j : procMap["AttrList"].toList()) {
					const auto attrMap = j.toMap();
					const auto attrId = attrMap["AttrID"].toString();
					const auto modifier = ToAttrModifier(attrMap["AttrModifier"].toString());
					const auto value = attrMap["AttrValue"];

					if (const auto attr = process->GetAttribute(attrId)) {
						attr->SetModifier(modifier);
						attr->SetValue(value);
					} else
						d->error.push_back(QString("Process attribute is not correct: %1(%2) - %3").arg(procName).arg(procId).arg(attrId));
				}
			} else
				d->error.push_back(QString("Processor type is not found: %1(%2)").arg(procName).arg(procId));
		}

		for (const auto& i : format["ProcessList"].toList()) {
			const auto service = d->provider->GetService<IProcessService>();
			const auto procMap = i.toMap();
			const auto procName = procMap["ProcessName"].toString();

			if (const auto process = GetProcess(procName)) {
				for (const auto& j : procMap["InputList"].toList()) {
					const auto inputMap = j.toMap();
					const auto inputId = inputMap["InputID"].toString();
					const auto inputName = inputMap["DataName"].toString();
					const auto data = GetData(inputName);

					if (const auto input = process->GetInput(inputId)) {
						if (!input->Link(data))
							d->error.push_back(QString("Linkable data is not found: %1(%2) - %3").arg(procName).arg(inputId).arg(inputName));
					} else
						d->error.push_back(QString("Process input is not correct: %1(%2)").arg(procName).arg(inputId));
				}
			}
		}

		return true;
	}

	auto Pipeline::ToVariantMap() const -> QVariantMap {
		QVariantMap format;
		format["Name"] = GetName();
		format["Description"] = GetDescription();
		format["Author"] = GetAuthor();
		format["CreatedAt"] = GetCreationDateTime().toMSecsSinceEpoch();
		format["Version"] = 2;

		QVariantList sourceList;
		QVariantList processList;

		for (const auto& s : GetSourceList()) {
			const auto source = GetSource(s);
			QVariantMap sourceMap;
			sourceMap["DataName"] = source->GetName();
			sourceMap["Flags"] = ToStringList(source->GetFlags());
			sourceMap["Savable"] = source->IsAutoSave();
			sourceList.push_back(sourceMap);
		}

		for (const auto& p : GetProcessList()) {
			const auto process = GetProcess(p);
			QVariantMap processMap;
			processMap["ProcessID"] = process->GetID();
			processMap["ProcessName"] = process->GetName();

			QVariantList inputList;
			QVariantList outputList;
			QVariantList attrList;

			for (const auto& i : process->GetInputList()) {
				const auto input = process->GetInput(i);
				if (input->IsLinked()) {
					QVariantMap inputMap;
					inputMap["InputID"] = i;
					inputMap["DataName"] = input->GetLinkedName();
					inputList.push_back(inputMap);
				}
			}

			for (const auto& o : process->GetOutputList()) {
				const auto output = process->GetOutput(o);
				QVariantMap outputMap;
				outputMap["OutputID"] = o;
				outputMap["DataName"] = output->GetName();
				outputMap["Savable"] = output->IsAutoSave();
				outputList.push_back(outputMap);
			}

			for (const auto& a : process->GetAttributeList()) {
				const auto attr = process->GetAttribute(a);
				QVariantMap attrMap;
				attrMap["AttrID"] = attr->GetID();
				attrMap["AttrModifier"] = ToString(attr->GetModifier());
				attrMap["AttrValue"] = attr->GetValue();
				attrList.push_back(attrMap);
			}

			processMap["InputList"] = inputList;
			processMap["OutputList"] = outputList;
			processMap["AttrList"] = attrList;

			processList.push_back(processMap);
		}

		format["SourceList"] = sourceList;
		format["ProcessList"] = processList;

		return format;
	}

	auto Pipeline::GetLocation() const -> QString {
		return d->filepath;
	}

	auto Pipeline::GetErrorList() const -> QStringList {
		return d->error;
	}

	auto Pipeline::GetName() const -> QString {
		return QFileInfo(d->filepath).completeBaseName();
	}

	auto Pipeline::GetDescription() const -> QString {
		return d->desc;
	}

	auto Pipeline::GetCreationDateTime() const -> QDateTime {
		return d->created;
	}

	auto Pipeline::GetAuthor() const -> QString {
		return d->author;
	}

	auto Pipeline::SetDescription(const QString& desc) -> void {
		d->desc = desc;
	}

	auto Pipeline::SetAuthor(const QString& author) -> void {
		d->author = author;
	}

	auto Pipeline::ContainsProcess(const QString& name) const -> bool {
		for (const auto& p : d->processes) {
			if (p->GetName() == name)
				return true;
		}

		return false;
	}

	auto Pipeline::ContainsData(const QString& name) const -> bool {
		if (ContainsSource(name) || ContainsOutput(name))
			return true;

		return false;
	}

	auto Pipeline::ContainsSource(const QString& name) const -> bool {
		for (const auto& s : d->sources) {
			if (s->GetName() == name)
				return true;
		}

		return false;
	}

	auto Pipeline::ContainsOutput(const QString& name) const -> bool {
		for (const auto& p : d->processes) {
			for (const auto& o : p->GetOutputList()) {
				if (const auto output = p->GetOutput(o); output->GetName() == name)
					return true;
			}
		}

		return false;
	}

	auto Pipeline::GetProcessList() const -> QStringList {
		QStringList list;

		for (const auto& p : d->processes)
			list.push_back(p->GetName());

		return list;
	}

	auto Pipeline::GetProcess(const QString& name) const -> ProcessPtr {
		for (auto& p : d->processes) {
			if (p->GetName() == name)
				return p;
		}

		return {};
	}

	auto Pipeline::GetDataList() const -> QStringList {
		auto list = GetSourceList();
		list.append(GetOutputList());
		return list;
	}

	auto Pipeline::GetData(const QString& name) const -> PipelineDataPtr {
		if (const auto src = GetSource(name))
			return src;

		if (const auto output = GetOutput(name))
			return output;

		return {};
	}

	auto Pipeline::GetSourceList() const -> QStringList {
		QStringList list;

		for (const auto& s : d->sources)
			list.push_back(s->GetName());

		return list;
	}

	auto Pipeline::GetSource(const QString& name) const -> PipelineSourcePtr {
		for (auto& s : d->sources) {
			if (s->GetName() == name)
				return s;
		}

		return {};
	}

	auto Pipeline::GetOutputList() const -> QStringList {
		QStringList list;

		for (const auto& p : d->processes) {
			for (const auto& o : p->GetOutputList()) {
				const auto output = p->GetOutput(o);
				list.push_back(output->GetName());
			}
		}

		return list;
	}

	auto Pipeline::GetOutput(const QString& name) const -> ProcessOutputPtr {
		for (const auto& p : d->processes) {
			for (const auto& o : p->GetOutputList()) {
				if (auto output = p->GetOutput(o); output->GetName() == name)
					return output;
			}
		}

		return {};
	}

	auto Pipeline::AddProcess(const QString& name, const ProcessTypePtr& type) -> ProcessPtr {
		if (ContainsProcess(name))
			return {};

		if (auto process = std::make_shared<Process>(name, type, shared_from_this()); process->Initialize()) {
			d->processes.push_back(process);

			if (d->load) {
				const auto service = d->provider->GetService<Processor::IProcessorService>();
				const auto proc = service->CreateProcessor(process->GetID());
				process->SetProcessor(proc);
			}

			return process;
		}

		return {};
	}

	auto Pipeline::AddSource(const QString& name, DataFlags flags) -> PipelineSourcePtr {
		if (ContainsData(name))
			return {};

		auto source = std::make_shared<PipelineSource>(name, flags, shared_from_this());
		d->sources.push_back(source);
		return source;
	}

	auto Pipeline::MoveProcess(const ProcessPtr& process, int index) -> void {
		if (const auto from = d->processes.indexOf(std::dynamic_pointer_cast<Process>(process)); from > -1)
			d->processes.move(from, std::min(index, d->processes.count() - 1));
	}

	auto Pipeline::RemoveProcess(const QString& name) -> void {
		for (const auto& p : d->processes) {
			if (p->GetName() == name) {
				p->Invalidate();
				d->processes.removeOne(p);
				break;
			}
		}
	}

	auto Pipeline::RemoveSource(const QString& name) -> void {
		for (const auto& s : d->sources) {
			if (s->GetName() == name) {
				s->Invalidate();
				d->sources.removeOne(s);
				break;
			}
		}
	}

	auto Pipeline::Load() -> void {
		d->load = true;

		for (const auto& p : d->processes) {
			const auto service = d->provider->GetService<Processor::IProcessorService>();
			const auto proc = service->CreateProcessor(p->GetID());
			p->SetProcessor(proc);
		}
	}
}
