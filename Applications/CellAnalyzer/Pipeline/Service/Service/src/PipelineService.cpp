#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>

#include "PipelineService.h"
#include "Pipeline.h"

namespace CellAnalyzer::Pipeline::Service {
	struct PipelineService::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
	};

	PipelineService::PipelineService(Tomocube::IServiceProvider* provider) : IPipelineService(), d(new Impl) {
		d->provider = provider;
	}

	PipelineService::~PipelineService() = default;

	auto PipelineService::Start() -> std::optional<Error> {
		return {};
	}

	auto PipelineService::Stop() -> void {}

	auto PipelineService::Create(const QString& url) -> PipelinePtr {
		if (QFile file(url); file.open(QIODevice::WriteOnly | QIODevice::NewOnly)) {
			auto pipeline = std::make_shared<Pipeline>(url, d->provider);
			const auto map = pipeline->ToVariantMap();
			const auto doc = QJsonDocument::fromVariant(map);
			const auto json = doc.toJson();

			if (file.write(json) == json.size() && file.flush())
				return pipeline;
		}

		return {};
	}

	auto PipelineService::Read(const QString& url) const -> PipelinePtr {
		if (QFile file(url); file.open(QIODevice::ReadOnly)) {
			QJsonParseError error {};
			const auto doc = QJsonDocument::fromJson(file.readAll(), &error);

			if (error.error == QJsonParseError::NoError) {
				if (auto pipeline = std::make_shared<Pipeline>(url, d->provider); pipeline->Initialize(doc.object().toVariantMap()))
					return pipeline;
			}
		}

		return {};
	}

	auto PipelineService::Save(const PipelinePtr& pipeline) -> bool {
		return Copy(pipeline, pipeline->GetLocation());
	}

	auto PipelineService::Copy(const PipelinePtr& pipeline, const QString& url) -> bool {
		const QDir dir(QFileInfo(url).absolutePath());
		dir.mkpath(".");

		if (QFile file(url); file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
			const auto srcPip = std::dynamic_pointer_cast<Pipeline>(pipeline);
			auto map = srcPip->ToVariantMap();
			map["CreatedAt"] = QDateTime::currentDateTime().toMSecsSinceEpoch();
			const auto doc = QJsonDocument::fromVariant(map);
			const auto json = doc.toJson();

			if (file.write(json) == json.size() && file.flush())
				return true;
		}

		return false;
	}
}
