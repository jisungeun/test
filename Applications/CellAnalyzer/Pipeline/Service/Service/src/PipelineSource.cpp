#include "PipelineSource.h"

#include "IPipeline.h"

namespace CellAnalyzer::Pipeline::Service {
	struct PipelineSource::Impl {
		DataFlags flags;
		std::weak_ptr<IPipeline> parent;
	};

	PipelineSource::PipelineSource(const QString& name, DataFlags flags, const PipelinePtr& parent) : PipelineData(name), IPipelineSource(), d(new Impl) {
		d->flags = flags;
		d->parent = parent;
	}

	PipelineSource::~PipelineSource() = default;

	auto PipelineSource::GetFlags() const -> DataFlags {
		return d->flags;
	}

	auto PipelineSource::GetParent() const -> PipelinePtr {
		if (d->parent.expired())
			return {};

		return d->parent.lock();
	}

	auto PipelineSource::IsValidDataName(const QString& name) const -> bool {
		if (const auto pip = d->parent.lock())
			return !pip->ContainsData(name);

		return false;
	}

	auto PipelineSource::InvalidateParent() -> void {
		d->parent.reset();
	}
}
