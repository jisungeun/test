#include "ProcessOutput.h"

#include "IPipeline.h"

namespace CellAnalyzer::Pipeline::Service {
	struct ProcessOutput::Impl {
		DataFlags flags;
		std::weak_ptr<IProcess> parent;
	};

	ProcessOutput::ProcessOutput(const QString& name, DataFlags flags, const ProcessPtr& parent) : PipelineData(name), IProcessOutput(), d(new Impl) {
		d->flags = flags;
		d->parent = parent;
	}

	ProcessOutput::~ProcessOutput() = default;

	auto ProcessOutput::GetFlags() const -> DataFlags {
		return d->flags;
	}

	auto ProcessOutput::GetParent() const -> ProcessPtr {
		if (d->parent.expired())
			return {};

		return d->parent.lock();
	}

	auto ProcessOutput::IsValidDataName(const QString& name) const -> bool {
		if (const auto proc = d->parent.lock(); proc->GetParent() == nullptr)
			return false;
		else if (const auto pip = proc->GetParent())
			return !pip->ContainsData(name);

		return false;
	}

	auto ProcessOutput::InvalidateParent() -> void {
		d->parent.reset();
	}
}
