#include <QQueue>
#include <QStack>

#include "Process.h"
#include "ProcessAttr.h"
#include "ProcessInput.h"
#include "ProcessOutput.h"

namespace CellAnalyzer::Pipeline::Service {
	class DataAttrGetter final : public Processor::IDataAttrGetter {
	public:
		explicit DataAttrGetter(const ProcessPtr& parent) : process(parent) {}

		auto GetName(const DataPtr& data) const -> QString override {
			if (process.expired())
				return {};

			const auto proc = process.lock();
			for (const auto& i : proc->GetInputList()) {
				const auto input = proc->GetInput(i);

				if (input->GetLinkedData() == data)
					return input->GetLinkedName();
			}

			return {};
		}

	private:
		std::weak_ptr<IProcess> process;
	};

	struct Process::Impl {
		QString name;
		ProcessTypePtr type = nullptr;
		std::weak_ptr<IPipeline> parent;
		Processor::ProcessorPtr processor = nullptr;
		bool updated = false;

		QMap<QString, std::shared_ptr<ProcessInput>> inputMap;
		QMap<QString, std::shared_ptr<ProcessOutput>> outputMap;
		QMap<QString, std::shared_ptr<ProcessAttr>> attrMap;
	};

	Process::Process(const QString& name, const ProcessTypePtr& type, const PipelinePtr& parent) : std::enable_shared_from_this<Process>(), IProcess(), d(new Impl) {
		d->name = name;
		d->type = type;
		d->parent = parent;
	}

	Process::~Process() = default;

	auto Process::Initialize() -> bool {
		if (!d->type)
			return false;

		for (const auto& id : d->type->GetOutputTypeList()) {
			auto name = id;
			auto idx = 1;

			while (d->parent.lock()->ContainsData(name))
				name = QString("%1_%2").arg(id).arg(idx++);

			d->outputMap[id] = std::make_shared<ProcessOutput>(name, d->type->GetOutputFlags(id), std::const_pointer_cast<Process>(shared_from_this()));

			if (idx > 100)
				return false;
		}

		for (const auto& id : d->type->GetInputTypeList()) {
			d->inputMap[id] = std::make_shared<ProcessInput>(d->type->GetInputFlags(id), std::const_pointer_cast<Process>(shared_from_this()));

			QObject::connect(d->inputMap[id].get(), &ProcessInput::DataChanged, [this] {
				OnDataChanged();
			});
			QObject::connect(d->inputMap[id].get(), &ProcessInput::DataLinked, [this] {
				OnDataLinked();
			});
		}

		for (const auto& id : d->type->GetAttributeTypeList()) {
			const auto type = d->type->GetAttributeType(id);
			const auto attr = std::make_shared<ProcessAttr>(type, std::const_pointer_cast<Process>(shared_from_this()));
			d->attrMap[id] = attr;

			QObject::connect(attr.get(), &ProcessAttr::ValueChanged, [this](const QString& id, const AttrValue& value) {
				OnValueChanged(id, value);
			});
		}

		return true;
	}

	auto Process::Invalidate() -> void {
		d->parent.reset();
	}

	auto Process::SetProcessor(const Processor::ProcessorPtr& processor) -> void {
		d->processor = processor;
		d->updated = true;

		if (d->processor) {
			d->processor->SetGetter(std::make_shared<DataAttrGetter>(shared_from_this()));

			for (const auto& k : d->attrMap.keys())
				d->processor->SetAttr(k, d->attrMap[k]);

			for (const auto& k : d->attrMap.keys())
				d->processor->SetAttrValue(k, d->attrMap[k]->GetAttrValue());

			for (const auto& k : d->inputMap.keys())
				d->processor->SetInputData(k, d->inputMap[k]->GetLinkedData());
		}
	}

	auto Process::GetName() const -> QString {
		return d->name;
	}

	auto Process::GetID() const -> QString {
		return d->type->GetID();
	}

	auto Process::GetParent() const -> PipelinePtr {
		return d->parent.lock();
	}

	auto Process::GetCategory() const -> ProcessCategory {
		return d->type->GetCategory();
	}

	auto Process::SetName(const QString& name) -> bool {
		if (d->parent.expired())
			return false;

		if (const auto parent = d->parent.lock(); parent->ContainsProcess(name))
			return false;

		d->name = name;
		return true;
	}

	auto Process::GetPrecedents(bool includeSelf) const -> ProcessList {
		ProcessList list;
		QQueue<ProcessPtr> queue;
		queue.enqueue(std::const_pointer_cast<Process>(shared_from_this()));

		while (!queue.isEmpty()) {
			const auto proc = queue.dequeue();

			for (const auto& i : proc->GetInputList()) {
				const auto input = proc->GetInput(i);

				if (!input->IsLinked())
					return {};

				if (const auto output = input->GetLinkedAsOutput()) {
					const auto parent = output->GetParent();
					list.push_front(parent);
					queue.enqueue(parent);
				}
			}
		}

		if (includeSelf)
			list.push_back(std::const_pointer_cast<Process>(shared_from_this()));

		return list;
	}

	auto Process::GetInputList() const -> QStringList {
		return d->type->GetInputTypeList();
	}

	auto Process::GetInput(const QString& id) const -> ProcessInputPtr {
		if (d->inputMap.contains(id))
			return d->inputMap[id];

		return {};
	}

	auto Process::GetOutputList() const -> QStringList {
		return d->type->GetOutputTypeList();
	}

	auto Process::GetOutput(const QString& id) const -> ProcessOutputPtr {
		if (d->outputMap.contains(id))
			return d->outputMap[id];

		return {};
	}

	auto Process::GetAttributeList() const -> QStringList {
		return d->type->GetAttributeTypeList();
	}

	auto Process::GetAttribute(const QString& id) const -> ProcessAttrPtr {
		if (d->attrMap.contains(id))
			return d->attrMap[id];

		return {};
	}

	auto Process::IsExecutable() const -> bool {
		return d->processor != nullptr;
	}

	auto Process::IsExecuted() const -> bool {
		return !d->updated;
	}

	auto Process::Execute() -> bool {
		if (!IsExecutable())
			return false;

		if (const auto outputs = d->outputMap.values(); std::all_of(outputs.begin(), outputs.end(), [](const std::shared_ptr<ProcessOutput>& output) {
			return output->ExistsData();
		}) && IsExecuted())
			return false;

		const auto outputMap = d->processor->Process();

		for (const auto& o : d->outputMap.keys())
			d->outputMap[o]->SetData({});

		for (const auto& k : outputMap.keys()) {
			if (const auto output = GetOutput(k))
				output->SetData(outputMap[k]);
		}

		d->updated = false;
		return true;
	}

	auto Process::OnDataLinked() -> void {
		if (d->processor) {
			for (const auto& k : d->inputMap.keys()) {
				if (const auto data = d->inputMap[k]->GetLinkedData(); d->processor->GetInputData(k) != data)
					d->processor->SetInputData(k, data);
			}
		}

		d->updated = true;
	}

	auto Process::OnDataChanged() -> void {
		for (const auto& k : d->outputMap.keys())
			d->outputMap[k]->SetData({});

		if (d->processor) {
			for (const auto& k : d->inputMap.keys()) {
				if (const auto data = d->inputMap[k]->GetLinkedData(); d->processor->GetInputData(k) != data)
					d->processor->SetInputData(k, data);
			}
		}

		d->updated = true;
	}

	auto Process::OnValueChanged(const QString& id, const AttrValue& value) -> void {
		if (d->processor)
			d->processor->SetAttrValue(id, value);

		d->updated = true;
	}
}
