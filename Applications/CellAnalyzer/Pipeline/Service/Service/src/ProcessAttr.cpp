#include "ProcessAttr.h"

namespace CellAnalyzer::Pipeline::Service {
	struct ProcessAttr::Impl {
		ProcessAttrTypePtr type = nullptr;
		std::weak_ptr<IProcess> parent;

		AttrModifier modifier = AttrModifier::ReadWrite;
		AttrState state = AttrState::Enabled;
		Processor::ProcessorAttrModel model;
		Processor::ProcessorAttrValue value;
	};

	ProcessAttr::ProcessAttr(const ProcessAttrTypePtr& type, const ProcessPtr& parent) : QObject(), IProcessAttr(), IProcessorAttr(), d(new Impl) {
		d->type = type;
		d->parent = parent;
		switch (type->GetCategory()) {
			case AttrCategory::Null:
				break;
			case AttrCategory::CheckBox:
				d->value = false;
				break;
			case AttrCategory::Integer:
			case AttrCategory::IntegerSlider:
				break;
			case AttrCategory::Double:
			case AttrCategory::DoubleSlider:
				break;
			case AttrCategory::String:
			case AttrCategory::StringText:
			case AttrCategory::StringList:
			case AttrCategory::StringCombo:
			case AttrCategory::StringRadio:
			case AttrCategory::StringCheck:
			case AttrCategory::File:
			case AttrCategory::Directory:
			case AttrCategory::TCF:
				d->value = QString();
				break;
			default:
				break;
		}
	}

	ProcessAttr::~ProcessAttr() = default;

	auto ProcessAttr::GetID() const -> QString {
		return d->type->GetID();
	}

	auto ProcessAttr::GetDescription() const -> QString {
		return d->type->GetDescription();
	}

	auto ProcessAttr::GetCategory() const -> AttrCategory {
		return d->type->GetCategory();
	}

	auto ProcessAttr::GetGroup() const -> QString {
		return d->type->GetGroup();
	}

	auto ProcessAttr::GetParent() const -> ProcessPtr {
		return d->parent.lock();
	}

	auto ProcessAttr::IsAsync() const -> bool {
		return d->type->IsAsync();
	}

	auto ProcessAttr::GetModifier() const -> AttrModifier {
		return d->modifier;
	}

	auto ProcessAttr::GetState() const -> AttrState {
		return d->state;
	}

	auto ProcessAttr::GetModel() const -> AttrValue {
		return d->model;
	}

	auto ProcessAttr::GetValue() const -> AttrValue {
		return d->value;
	}

	auto ProcessAttr::SetModifier(AttrModifier modifier) -> void {
		d->modifier = modifier;
	}

	auto ProcessAttr::SetValue(const AttrValue& value) -> void {
		if (d->value != value && !value.isNull()) {
			d->value = value;
			emit ValueChanged(d->type->GetID(), d->value);
		}
	}

	auto ProcessAttr::GetAttrModel() const -> Processor::ProcessorAttrModel {
		return d->model;
	}

	auto ProcessAttr::GetAttrValue() const -> Processor::ProcessorAttrValue {
		return d->value;
	}

	auto ProcessAttr::GetAttrStyle() const -> Processor::ProcessorAttrStyle {
		switch (d->state) {
			case AttrState::Enabled:
				return Processor::ProcessorAttrStyle::Visible;
			case AttrState::Disabled:
				return Processor::ProcessorAttrStyle::Disabled;
			case AttrState::Hidden:
				return Processor::ProcessorAttrStyle::Hidden;
			default:
				return Processor::ProcessorAttrStyle::Visible;
		}
	}

	auto ProcessAttr::SetAttrModel(const Processor::ProcessorAttrModel& model) -> void {
		d->model = model;
		if (d->model.type() == QVariant::StringList && d->value.isNull()) {
			if (const auto list = d->model.toStringList(); !list.isEmpty()) {
				d->value = list.first();
			}
		}
		if (d->model.type() == QVariant::Type::Map && d->value.isNull()) {
			if (d->model.toMap().contains("Min")) {
				d->value = d->model.toMap()["Min"];
			}
		}
	}

	auto ProcessAttr::SetAttrValue(const Processor::ProcessorAttrValue& value) -> void {
		d->value = value;
	}

	auto ProcessAttr::SetAttrStyle(Processor::ProcessorAttrStyle style) -> void {
		switch (style) {
			case Processor::ProcessorAttrStyle::Visible:
				d->state = AttrState::Enabled;
				break;
			case Processor::ProcessorAttrStyle::Disabled:
				d->state = AttrState::Disabled;
				break;
			case Processor::ProcessorAttrStyle::Hidden:
				d->state = AttrState::Hidden;
				break;
			default:
				d->state = AttrState::Enabled;
				break;
		}
	}
}
