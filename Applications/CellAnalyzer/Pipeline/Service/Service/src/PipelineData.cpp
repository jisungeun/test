#include "PipelineData.h"

#include "IPipeline.h"

namespace CellAnalyzer::Pipeline::Service {
	struct PipelineData::Impl {
		QString name;
		DataPtr data = nullptr;
		bool exportable = false;
	};

	PipelineData::PipelineData(const QString& name) : IPipelineData(), QObject(), d(new Impl) {
		d->name = name;
	}

	PipelineData::~PipelineData() = default;

	auto PipelineData::Invalidate() -> void {
		InvalidateParent();
		d->data = nullptr;
		emit DataChanged();
	}

	auto PipelineData::GetName() const -> QString {
		return d->name;
	}

	auto PipelineData::GetData() const -> DataPtr {
		return d->data;
	}

	auto PipelineData::ExistsData() const -> bool {
		return d->data != nullptr;
	}

	auto PipelineData::IsAutoSave() const -> bool {
		return d->exportable;
	}

	auto PipelineData::SetAutoSave(bool savable) -> void {
		d->exportable = savable;
	}

	auto PipelineData::SetName(const QString& name) -> bool {
		if (name.isEmpty())
			return false;

		if (d->name == name)
			return true;

		if (!IsValidDataName(name))
			return false;

		d->name = name;
		return true;
	}

	auto PipelineData::SetData(const DataPtr& data) -> bool {
		if (data == nullptr) {
			if (d->data != data) {
				d->data = nullptr;
				emit DataChanged();
			}

			return true;
		}

		if (const auto cmp = data->GetFlags() & this->GetFlags(); cmp == this->GetFlags()) {
			if (d->data != data) {
				d->data = data;
				emit DataChanged();
			}

			return true;
		}

		return false;
	}
}
