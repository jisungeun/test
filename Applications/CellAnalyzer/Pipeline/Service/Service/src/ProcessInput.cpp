#include <QStack>

#include "ProcessInput.h"

#include "IPipeline.h"
#include "ProcessOutput.h"

namespace CellAnalyzer::Pipeline::Service {
	struct ProcessInput::Impl {
		std::weak_ptr<IProcess> parent;
		std::weak_ptr<IPipelineData> data;

		DataFlags flags = DataFlag::Null;
	};

	ProcessInput::ProcessInput(DataFlags flags, const ProcessPtr& parent) : QObject(), IProcessInput(), d(new Impl) {
		d->flags = flags;
		d->parent = parent;
	}

	ProcessInput::~ProcessInput() = default;

	auto ProcessInput::Invalidate() -> void {
		d->parent.reset();
		d->data.reset();
	}

	auto ProcessInput::GetFlags() const -> DataFlags {
		return d->flags;
	}

	auto ProcessInput::GetParent() const -> ProcessPtr {
		if (!d->parent.expired())
			return d->parent.lock();

		return {};
	}

	auto ProcessInput::IsSource() const -> bool {
		return std::dynamic_pointer_cast<IPipelineSource>(GetLinked()) != nullptr;
	}

	auto ProcessInput::IsLinked() const -> bool {
		return !d->data.expired();
	}

	auto ProcessInput::IsLinkable(const PipelineDataPtr& data) const -> bool {
		//if (const auto and = data->GetFlags() & this->GetFlags(); and != this->GetFlags())
		//return false;

		QStack<ProcessPtr> stack;

		if (const auto output = std::dynamic_pointer_cast<IProcessOutput>(data))
			stack.push(output->GetParent());

		while (!stack.isEmpty()) {
			if (const auto item = stack.pop()) {
				if (item == d->parent.lock())
					return false;

				for (const auto& i : item->GetInputList()) {
					if (const auto input = item->GetInput(i); input->IsLinked()) {
						if (const auto output = input->GetLinkedAsOutput())
							stack.push(output->GetParent());
					}
				}
			}
		}

		return true;
	}

	auto ProcessInput::GetLinked() const -> PipelineDataPtr {
		if (!d->data.expired())
			return d->data.lock();

		return {};
	}

	auto ProcessInput::GetLinkedAsSource() const -> PipelineSourcePtr {
		return std::dynamic_pointer_cast<IPipelineSource>(GetLinked());
	}

	auto ProcessInput::GetLinkedAsOutput() const -> ProcessOutputPtr {
		return std::dynamic_pointer_cast<IProcessOutput>(GetLinked());
	}

	auto ProcessInput::GetLinkedName() const -> QString {
		if (!d->data.expired())
			return d->data.lock()->GetName();

		return {};
	}

	auto ProcessInput::GetLinkedData() const -> DataPtr {
		if (!d->data.expired())
			return d->data.lock()->GetData();

		return {};
	}

	auto ProcessInput::GetLinkableDataList() const -> PipelineDataList {
		const auto proc = d->parent.lock();
		const auto pip = proc->GetParent();
		const auto sources = pip->GetSourceList();
		const auto outputs = pip->GetOutputList();
		PipelineDataList list;

		for (const auto& s : sources) {
			if (const auto src = pip->GetSource(s)) {
				if (const auto flags = d->flags & src->GetFlags(); flags == d->flags)
					list.push_back(src);
			}
		}

		auto isInterlock = [proc](const ProcessPtr& origin) -> bool {
			QStack<ProcessPtr> stack;
			stack.push(origin);

			while (!stack.isEmpty()) {
				const auto parent = stack.pop();

				if (parent == proc)
					return true;

				for (const auto& i : parent->GetInputList()) {
					if (const auto po = parent->GetInput(i)->GetLinkedAsOutput())
						stack.push(po->GetParent());
				}
			}

			return false;
		};

		for (const auto& o : outputs) {
			if (const auto output = pip->GetOutput(o); !isInterlock(output->GetParent())) {
				if (output->GetFlags() == d->flags)
					list.push_back(output);
				else if (d->flags.testFlag(DataFlag::Mask)) { // Mask Handling
					if (d->flags.testFlag(DataFlag::Volume2D) && output->GetFlags().testFlag(DataFlag::Volume2D)) {
						if (output->GetFlags().testFlag(DataFlag::Label) || output->GetFlags().testFlag(DataFlag::Binary))
							list.push_back(output);
					} else if (d->flags.testFlag(DataFlag::Volume3D) && output->GetFlags().testFlag(DataFlag::Volume3D)) {
						if (output->GetFlags().testFlag(DataFlag::Label) || output->GetFlags().testFlag(DataFlag::Binary))
							list.push_back(output);
					}
				}
			}
		}

		return list;
	}

	auto ProcessInput::Link(const PipelineDataPtr& data) -> bool {
		if (d->data.lock() == data)
			return true;

		if (data) {
			auto isMask = false;
			if (d->flags.testFlag(DataFlag::Mask)) {
				if (d->flags.testFlag(DataFlag::Volume2D) && data->GetFlags().testFlag(DataFlag::Volume2D)) {
					if (data->GetFlags().testFlag(DataFlag::Label) || data->GetFlags().testFlag(DataFlag::Binary))
						isMask = true;
				} else if (d->flags.testFlag(DataFlag::Volume3D) && data->GetFlags().testFlag(DataFlag::Volume3D)) {
					if (data->GetFlags().testFlag(DataFlag::Label) || data->GetFlags().testFlag(DataFlag::Binary))
						isMask = true;
				}
			}
			if (const auto cmp = data->GetFlags() & d->flags; (cmp != d->flags) && false == isMask)
				return false;

			if (!IsLinkable(data))
				return false;

			d->data = data;

			if (const auto pdata = std::dynamic_pointer_cast<PipelineData>(d->data.lock())) {
				connect(pdata.get(), &PipelineData::DataChanged, [this] {
					emit DataChanged();
				});
			}
		} else
			d->data.reset();

		emit DataLinked();
		return true;
	}
}
