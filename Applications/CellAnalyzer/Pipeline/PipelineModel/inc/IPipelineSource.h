#pragma once

#include "IPipelineData.h"

#include "CellAnalyzer.Pipeline.PipelineModelExport.h"

namespace CellAnalyzer::Pipeline {
	class IPipeline;
	using PipelinePtr = std::shared_ptr<IPipeline>;

	class IPipelineSource;
	using PipelineSourcePtr = std::shared_ptr<IPipelineSource>;
	using PipelineSourceList = QVector<PipelineSourcePtr>;

	class CellAnalyzer_Pipeline_PipelineModel_API IPipelineSource : public virtual IPipelineData {
	public:
		virtual auto GetParent() const -> PipelinePtr = 0;
	};
}
