#pragma once

#include "IProcessAttr.h"
#include "IProcessInput.h"
#include "IProcessOutput.h"
#include "IProcessType.h"

#include "CellAnalyzer.Pipeline.PipelineModelExport.h"

namespace CellAnalyzer::Pipeline {
	class IPipeline;
	using PipelinePtr = std::shared_ptr<IPipeline>;

	class IProcess;
	using ProcessPtr = std::shared_ptr<IProcess>;
	using ProcessList = QVector<ProcessPtr>;

	class CellAnalyzer_Pipeline_PipelineModel_API IProcess : public virtual Tomocube::IService {
	public:
		virtual auto GetName() const -> QString = 0;
		virtual auto GetID() const -> QString = 0;
		virtual auto GetParent() const -> PipelinePtr = 0;
		virtual auto GetCategory() const -> ProcessCategory = 0;
		virtual auto SetName(const QString& name) -> bool = 0;

		virtual auto GetPrecedents(bool includeSelf = false) const -> ProcessList = 0;

		virtual auto GetInputList() const -> QStringList = 0;
		virtual auto GetInput(const QString& id) const -> ProcessInputPtr = 0;

		virtual auto GetOutputList() const -> QStringList = 0;
		virtual auto GetOutput(const QString& id) const -> ProcessOutputPtr = 0;

		virtual auto GetAttributeList() const -> QStringList = 0;
		virtual auto GetAttribute(const QString& id) const -> ProcessAttrPtr = 0;

		virtual auto IsExecutable() const -> bool = 0;
		virtual auto IsExecuted() const -> bool = 0;
		virtual auto Execute() -> bool = 0;
	};
}
