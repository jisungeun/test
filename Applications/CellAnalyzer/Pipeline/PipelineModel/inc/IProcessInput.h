#pragma once

#include "IPipelineSource.h"
#include "IProcessOutput.h"

#include "CellAnalyzer.Pipeline.PipelineModelExport.h"

namespace CellAnalyzer::Pipeline {
	class IProcessInput;
	using ProcessInputPtr = std::shared_ptr<IProcessInput>;
	using ProcessInputList = QVector<ProcessInputPtr>;

	class CellAnalyzer_Pipeline_PipelineModel_API IProcessInput : public virtual Tomocube::IService {
	public:
		virtual auto GetFlags() const -> DataFlags = 0;
		virtual auto GetParent() const -> ProcessPtr = 0;
		virtual auto IsSource() const -> bool = 0;
		virtual auto IsLinked() const -> bool = 0;
		virtual auto IsLinkable(const PipelineDataPtr& data) const -> bool = 0;

		virtual auto GetLinked() const -> PipelineDataPtr = 0;
		virtual auto GetLinkedAsSource() const -> PipelineSourcePtr = 0;
		virtual auto GetLinkedAsOutput() const -> ProcessOutputPtr = 0;
		virtual auto GetLinkedName() const -> QString = 0;
		virtual auto GetLinkedData() const -> DataPtr = 0;
		virtual auto GetLinkableDataList() const -> PipelineDataList = 0;

		virtual auto Link(const PipelineDataPtr& data) -> bool = 0;
	};
}
