#pragma once

#include "IPipelineData.h"

#include "CellAnalyzer.Pipeline.PipelineModelExport.h"

namespace CellAnalyzer::Pipeline {
	class IProcess;
	using ProcessPtr = std::shared_ptr<IProcess>;

	class IProcessOutput;
	using ProcessOutputPtr = std::shared_ptr<IProcessOutput>;
	using ProcessOutputList = QVector<ProcessOutputPtr>;

	class CellAnalyzer_Pipeline_PipelineModel_API IProcessOutput : public virtual IPipelineData {
	public:
		virtual auto GetParent() const -> ProcessPtr = 0;
	};
}
