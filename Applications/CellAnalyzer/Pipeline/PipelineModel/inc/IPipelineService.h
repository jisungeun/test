#pragma once

#include "IPipeline.h"

#include "CellAnalyzer.Pipeline.PipelineModelExport.h"

namespace CellAnalyzer::Pipeline {
	class CellAnalyzer_Pipeline_PipelineModel_API IPipelineService : public virtual Tomocube::IService {
	public:
		virtual auto Create(const QString& url) -> PipelinePtr = 0;
		virtual auto Read(const QString& url) const -> PipelinePtr = 0;
		virtual auto Save(const PipelinePtr& pipeline) -> bool = 0;
		virtual auto Copy(const PipelinePtr& pipeline, const QString& url) -> bool = 0;
	};
}
