#pragma once

#include <optional>

#include <QDateTime>

#include "IPipelineSource.h"
#include "IProcess.h"
#include "IProcessType.h"

#include "CellAnalyzer.Pipeline.PipelineModelExport.h"

namespace CellAnalyzer::Pipeline {
	class IPipeline;
	using PipelinePtr = std::shared_ptr<IPipeline>;
	using PipelineList = QVector<PipelinePtr>;

	class CellAnalyzer_Pipeline_PipelineModel_API IPipeline : public virtual Tomocube::IService {
	public:
		virtual auto GetLocation() const -> QString = 0;
		virtual auto GetErrorList() const -> QStringList = 0;

		virtual auto GetName() const -> QString = 0;
		virtual auto GetDescription() const -> QString = 0;
		virtual auto GetCreationDateTime() const -> QDateTime = 0;
		virtual auto GetAuthor() const -> QString = 0;

		virtual auto SetDescription(const QString& desc) -> void = 0;
		virtual auto SetAuthor(const QString& author) -> void = 0;

		virtual auto ContainsProcess(const QString& name) const -> bool = 0;
		virtual auto ContainsData(const QString& name) const -> bool = 0;
		virtual auto ContainsSource(const QString& name) const -> bool = 0;
		virtual auto ContainsOutput(const QString& name) const -> bool = 0;

		virtual auto GetProcessList() const -> QStringList = 0;
		virtual auto GetProcess(const QString& name) const -> ProcessPtr = 0;
		virtual auto GetDataList() const -> QStringList = 0;
		virtual auto GetData(const QString& name) const -> PipelineDataPtr = 0;
		virtual auto GetSourceList() const -> QStringList = 0;
		virtual auto GetSource(const QString& name) const -> PipelineSourcePtr = 0;
		virtual auto GetOutputList() const -> QStringList = 0;
		virtual auto GetOutput(const QString& name) const -> ProcessOutputPtr = 0;

		virtual auto AddProcess(const QString& name, const ProcessTypePtr& type) -> ProcessPtr = 0;
		virtual auto AddSource(const QString& name, DataFlags flags) -> PipelineSourcePtr = 0;
		virtual auto MoveProcess(const ProcessPtr& process, int index) -> void = 0;
		virtual auto RemoveProcess(const QString& name) -> void = 0;
		virtual auto RemoveSource(const QString& name) -> void = 0;

		virtual auto Load() -> void = 0;
	};
}
