#pragma once

#include "IData.h"

#include "CellAnalyzer.Pipeline.PipelineModelExport.h"

namespace CellAnalyzer::Pipeline {
	class IPipelineData;
	using PipelineDataPtr = std::shared_ptr<IPipelineData>;
	using PipelineDataList = QVector<PipelineDataPtr>;

	class CellAnalyzer_Pipeline_PipelineModel_API IPipelineData : public virtual Tomocube::IService {
	public:
		virtual auto GetFlags() const -> DataFlags = 0;
		virtual auto GetName() const -> QString = 0;
		virtual auto GetData() const -> DataPtr = 0;
		virtual auto ExistsData() const -> bool = 0;

		virtual auto IsAutoSave() const -> bool = 0;
		virtual auto SetAutoSave(bool savable) -> void = 0;

		virtual auto SetName(const QString& name) -> bool = 0;
		virtual auto SetData(const DataPtr& data) -> bool = 0;
	};
}
