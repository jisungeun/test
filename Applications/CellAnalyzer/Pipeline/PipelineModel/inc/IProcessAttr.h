#pragma once

#include "IProcessAttrType.h"

#include "CellAnalyzer.Pipeline.PipelineModelExport.h"

namespace CellAnalyzer::Pipeline {
	class IProcess;
	using ProcessPtr = std::shared_ptr<IProcess>;

	class IProcessAttr;
	using ProcessAttrPtr = std::shared_ptr<IProcessAttr>;
	using ProcessAttrList = QVector<ProcessAttrPtr>;
	using ProcessAttrMap = QMap<QString, ProcessAttrPtr>;

	class CellAnalyzer_Pipeline_PipelineModel_API IProcessAttr : public virtual Tomocube::IService {
	public:
		virtual auto GetID() const -> QString = 0;
		virtual auto GetDescription() const -> QString = 0;
		virtual auto GetCategory() const -> AttrCategory = 0;
		virtual auto GetGroup() const -> QString = 0;
		virtual auto GetParent() const -> ProcessPtr = 0;
		virtual auto IsAsync() const -> bool = 0;

		virtual auto GetModifier() const -> AttrModifier = 0;
		virtual auto GetState() const -> AttrState = 0;
		virtual auto GetModel() const -> AttrValue = 0;
		virtual auto GetValue() const -> AttrValue = 0;

		virtual auto SetModifier(AttrModifier modifier) -> void = 0;
		virtual auto SetValue(const AttrValue& value) -> void = 0;
	};
}
