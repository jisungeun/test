#pragma once

#include "IProcessType.h"

#include "CellAnalyzer.Pipeline.TypeModelExport.h"

namespace CellAnalyzer::Pipeline {
	class CellAnalyzer_Pipeline_TypeModel_API IProcessService : public virtual Tomocube::IService {
	public:
		virtual auto GetList() const -> QStringList = 0;
		virtual auto GetType(const QString& id) const -> ProcessTypePtr = 0;
	};
}
