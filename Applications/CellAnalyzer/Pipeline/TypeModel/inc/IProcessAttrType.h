#pragma once

#include <memory>

#include <QVariant>

#include "IService.h"

#include "CellAnalyzer.Pipeline.TypeModelExport.h"

namespace CellAnalyzer::Pipeline {
	class IProcessAttrType;
	using ProcessAttrTypePtr = std::shared_ptr<IProcessAttrType>;
	using ProcessAttrTypeList = QVector<ProcessAttrTypePtr>;
	using AttrValue = QVariant;
	using AttrModel = QVariant;

	enum class AttrCategory {
		Null,
		CheckBox,
		Integer,
		IntegerSlider,
		Double,
		DoubleSlider,
		String,
		StringText,
		StringList,
		StringCombo,
		StringRadio,
		StringCheck,
		File,
		Directory,
		TCF
	};

	enum class AttrModifier {
		ReadWrite,
		ReadOnly,
		Invisible
	};

	enum class AttrState {
		Enabled,
		Disabled,
		Hidden,
	};

	class CellAnalyzer_Pipeline_TypeModel_API IProcessAttrType : public virtual Tomocube::IService {
	public:
		virtual auto GetID() const -> QString = 0;
		virtual auto GetDescription() const -> QString = 0;
		virtual auto GetCategory() const -> AttrCategory = 0;
		virtual auto GetGroup() const -> QString = 0;
		virtual auto IsAsync() const -> bool = 0;
	};

	static auto ToString(AttrCategory type) -> QString {
		switch (type) {
			case AttrCategory::CheckBox:
				return "CheckBox";
			case AttrCategory::Integer:
				return "Integer";
			case AttrCategory::IntegerSlider:
				return "IntegerSlider";
			case AttrCategory::Double:
				return "Double";
			case AttrCategory::DoubleSlider:
				return "DoubleSlider";
			case AttrCategory::String:
				return "String";
			case AttrCategory::StringText:
				return "StringText";
			case AttrCategory::StringList:
				return "StringList";
			case AttrCategory::StringCombo:
				return "StringCombo";
			case AttrCategory::StringRadio:
				return "StringRadio";
			case AttrCategory::StringCheck:
				return "StringCheck";
			case AttrCategory::File:
				return "File";
			case AttrCategory::Directory:
				return "Directory";
			case AttrCategory::TCF:
				return "TCF";
			default:
				return {};
		}
	}

	static auto ToAttrCategory(const QString& type) -> AttrCategory {
		if (type == "CheckBox")
			return AttrCategory::CheckBox;
		if (type == "Integer")
			return AttrCategory::Integer;
		if (type == "IntegerSlider")
			return AttrCategory::IntegerSlider;
		if (type == "Double")
			return AttrCategory::Double;
		if (type == "DoubleSlider")
			return AttrCategory::DoubleSlider;
		if (type == "String")
			return AttrCategory::String;
		if (type == "StringList")
			return AttrCategory::StringList;
		if (type == "StringCombo")
			return AttrCategory::StringCombo;
		if (type == "StringRadio")
			return AttrCategory::StringRadio;
		if (type == "StringCheck")
			return AttrCategory::StringCheck;
		if (type == "File")
			return AttrCategory::File;
		if (type == "Directory")
			return AttrCategory::Directory;
		if (type == "TCF")
			return AttrCategory::TCF;

		return AttrCategory::Null;
	}

	static auto ToString(AttrModifier type) -> QString {
		switch (type) {
			case AttrModifier::ReadWrite:
				return "ReadWrite";
			case AttrModifier::ReadOnly:
				return "ReadOnly";
			case AttrModifier::Invisible:
				return "Invisible";
			default:
				return "ReadWrite";
		}
	}

	static auto ToAttrModifier(const QString& type) -> AttrModifier {
		if (type == "ReadWrite")
			return AttrModifier::ReadWrite;
		if (type == "ReadOnly")
			return AttrModifier::ReadOnly;
		if (type == "Invisible")
			return AttrModifier::Invisible;

		return AttrModifier::ReadWrite;
	}

	static auto ToString(AttrState type) -> QString {
		switch (type) {
			case AttrState::Enabled:
				return "Enabled";
			case AttrState::Disabled:
				return "Disabled";
			case AttrState::Hidden:
				return "Hidden";
			default:
				return "Enabled";
		}
	}

	static auto ToAttrState(const QString& type) -> AttrState {
		if (type == "Enabled")
			return AttrState::Enabled;
		if (type == "ReadOnly")
			return AttrState::Disabled;
		if (type == "Hidden")
			return AttrState::Hidden;

		return AttrState::Enabled;
	}
}
