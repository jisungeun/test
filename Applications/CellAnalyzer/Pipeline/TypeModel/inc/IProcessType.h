#pragma once

#include "IData.h"
#include "IProcessAttrType.h"

#include "CellAnalyzer.Pipeline.TypeModelExport.h"

namespace CellAnalyzer::Pipeline {
	class IProcessType;
	using ProcessTypePtr = std::shared_ptr<IProcessType>;
	using ProcessTypeList = QVector<ProcessTypePtr>;

	enum class ProcessCategory {
		Null,
		Smoothing,
		EdgeDetection,
		Registration,
		Segmentation,
		Filtering,
		MorphologicalOperation,
		Measurement,
		Export,
		Labeling,
		Geometry,
		Logic,
		Arithmetic,
		AI
	};

	class CellAnalyzer_Pipeline_TypeModel_API IProcessType : public virtual Tomocube::IService {
	public:
		virtual auto GetID() const -> QString = 0;
		virtual auto GetDescription() const -> QString = 0;
		virtual auto GetLicense() const -> QString = 0;
		virtual auto GetCategory() const -> ProcessCategory = 0;

		virtual auto ContainsInputType(const QString& id) const -> bool = 0;
		virtual auto GetInputTypeList() const -> QStringList = 0;
		virtual auto GetInputFlags(const QString& id) const -> DataFlags = 0;

		virtual auto ContainsOutputType(const QString& id) const -> bool = 0;
		virtual auto GetOutputTypeList() const -> QStringList = 0;
		virtual auto GetOutputFlags(const QString& id) const -> DataFlags = 0;

		virtual auto ContainsAttributeType(const QString& id) const -> bool = 0;
		virtual auto GetAttributeTypeList() const -> QStringList = 0;
		virtual auto GetAttributeType(const QString& id) const -> ProcessAttrTypePtr = 0;
	};

	static auto ToString(ProcessCategory type) -> QString {
		switch (type) {
			case ProcessCategory::Smoothing:
				return "Smoothing";
			case ProcessCategory::EdgeDetection:
				return "EdgeDetection";
			case ProcessCategory::Registration:
				return "Registration";
			case ProcessCategory::Segmentation:
				return "Segmentation";
			case ProcessCategory::Filtering:
				return "Filtering";
			case ProcessCategory::MorphologicalOperation:
				return "MorphologicalOperation";
			case ProcessCategory::Measurement:
				return "Measurement";
			case ProcessCategory::Export:
				return "Export";
			case ProcessCategory::Labeling:
				return "Labeling";
			case ProcessCategory::Geometry:
				return "Geometry";
			case ProcessCategory::Logic:
				return "Logic";
			case ProcessCategory::Arithmetic:
				return "Arithmetic";
			case ProcessCategory::AI:
				return "AI";
			default:
				return {};
		}
	}

	static auto ToProcessCategory(const QString& type) -> ProcessCategory {
		if (type == "Smoothing")
			return ProcessCategory::Smoothing;
		if (type == "EdgeDetection")
			return ProcessCategory::EdgeDetection;
		if (type == "Registration")
			return ProcessCategory::Registration;
		if (type == "Segmentation")
			return ProcessCategory::Segmentation;
		if (type == "Filtering")
			return ProcessCategory::Filtering;
		if (type == "MorphOp" || type == "MorphologicalOperation")
			return ProcessCategory::MorphologicalOperation;
		if (type == "Measurement")
			return ProcessCategory::Measurement;
		if (type == "Export")
			return ProcessCategory::Export;
		if (type == "Labeling")
			return ProcessCategory::Labeling;
		if (type == "Geometry")
			return ProcessCategory::Geometry;
		if (type == "Logic")
			return ProcessCategory::Logic;
		if (type == "Arithmetic")
			return ProcessCategory::Arithmetic;
		if (type == "AI")
			return ProcessCategory::AI;

		return ProcessCategory::Null;
	}
}
