#pragma once

#include <memory>

#include <QString>

#include "CellAnalyzer.ProjectModelExport.h"

namespace CellAnalyzer {
	class IProjectEvent;
	using ProjectEventPtr = std::shared_ptr<IProjectEvent>;
	using ProjectEventList = QList<ProjectEventPtr>;

	class CellAnalyzer_ProjectModel_API IProjectEvent {
	public:
		virtual ~IProjectEvent();

		virtual auto OnInitialized(const QString& project, const QString& url) -> void;
		virtual auto OnDisposed(const QString& project) -> void;
	};
}
