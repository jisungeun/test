#pragma once

#include <memory>

#include <QDateTime>

#include "IService.h"

#include "CellAnalyzer.ProjectModelExport.h"

namespace CellAnalyzer {
	class IProject;
	using ProjectPtr = std::shared_ptr<IProject>;
	using ProjectList = QList<ProjectPtr>;

	class CellAnalyzer_ProjectModel_API IProject : public virtual Tomocube::IService {
	public:
		virtual auto GetName() const -> QString = 0;
		virtual auto GetFormat() const -> QString = 0;
		virtual auto GetDescription() const -> QString = 0;
		virtual auto GetIcon() const -> QString = 0;
		virtual auto GetUrl() const -> QString = 0;

		virtual auto GetProjectName(const QString& url) const -> QString = 0;

		virtual auto FindUrl() -> QString = 0;
		virtual auto Initialize(const QString& url) -> bool = 0;
		virtual auto Dispose() -> void = 0;
	};
}
