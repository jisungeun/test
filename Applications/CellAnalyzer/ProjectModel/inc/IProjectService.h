#pragma once

#include "IService.h"

#include "IProject.h"
#include "IProjectEvent.h"

#include "CellAnalyzer.ProjectModelExport.h"

namespace CellAnalyzer {
	struct CellAnalyzer_ProjectModel_API ProjectInfo {
		QString name;
		QString format;
		QString description;
		QString icon;
	};

	class CellAnalyzer_ProjectModel_API IProjectService : public virtual Tomocube::IService {
	public:
		virtual auto AddEvent(const ProjectEventPtr& event) -> void = 0;
		virtual auto RemoveEvent(const ProjectEventPtr& event) -> void = 0;

		virtual auto GetProjectList() const -> QStringList = 0;
		virtual auto GetProjectInfo(const QString& project) const -> ProjectInfo = 0;
		virtual auto GetProjectName(const QString& project, const QString& url) -> QString = 0;
		virtual auto GetBasePath(const QString& project) -> QString = 0;

		virtual auto FindUrl(const QString& project) -> QString = 0;
		virtual auto Initialize(const QString& project, const QString& url) -> bool = 0;
		virtual auto Dispose() -> void = 0;

		virtual auto GetCurrentProject() const -> QString = 0;
		virtual auto GetCurrentUrl() const -> QString = 0;
	};
}
