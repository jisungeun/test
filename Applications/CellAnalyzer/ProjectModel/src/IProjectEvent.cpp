#include "IProjectEvent.h"

namespace CellAnalyzer {
	IProjectEvent::~IProjectEvent() = default;

	auto IProjectEvent::OnInitialized(const QString& project, const QString& url) -> void {}

	auto IProjectEvent::OnDisposed(const QString& project) -> void {}
}
