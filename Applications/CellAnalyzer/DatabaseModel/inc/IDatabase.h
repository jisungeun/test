#pragma once

#include <optional>

#include <QVariantList>

#include "IService.h"

#include "CellAnalyzer.DatabaseModelExport.h"

namespace CellAnalyzer {
	class IDatabase;
	using DatabasePtr = std::shared_ptr<IDatabase>;
	using DatabaseList = QVector<DatabasePtr>;

	class CellAnalyzer_DatabaseModel_API IDatabase : public virtual Tomocube::IService {
	public:
		virtual auto Contains(const QString& key) const -> bool = 0;
		virtual auto IsArray(const QString& key) const -> bool = 0;

		virtual auto GetMap(const QString& key) const -> std::optional<QVariantMap> = 0;
		virtual auto GetList(const QString& key) const -> QVariantList = 0;

		virtual auto Save(const QString& key, const QVariantMap& value) -> bool = 0;
		virtual auto Save(const QString& key, const QVariantList& value) -> bool = 0;

		virtual auto Flush() -> void = 0;

		static auto ReadNumber(const QVariant& variant, const QString& key) -> std::optional<long long> {
			if (variant.type() == QVariant::Map) {
				if (const auto map = variant.toMap(); map.contains(key) && map[key].type() == QVariant::LongLong)
					return map[key].toLongLong();
			}

			return {};
		}

		static auto ReadString(const QVariant& variant, const QString& key) -> std::optional<QString> {
			if (variant.type() == QVariant::Map) {
				if (const auto map = variant.toMap(); map.contains(key) && map[key].type() == QVariant::String)
					return map[key].toString();
			}

			return {};
		}

		static auto GetInstance() -> IDatabase*;

	protected:
		static auto SetInstance(IDatabase* database) -> void;
	};
}
