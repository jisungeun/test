#include "IDatabase.h"

namespace {
	CellAnalyzer::IDatabase* instance = nullptr;
}

namespace CellAnalyzer {
	auto IDatabase::GetInstance() -> IDatabase* {
		return instance;
	}

	auto IDatabase::SetInstance(IDatabase* database) -> void {
		instance = database;
	}
}
