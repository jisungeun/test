#pragma once

#include <QString>
#include <QVector>

#include "IService.h"

#include "CellAnalyzer.AppModelExport.h"

namespace CellAnalyzer {
	struct Version;
	struct OpenSourceLicense;

	class CellAnalyzer_AppModel_API IApplication : public virtual Tomocube::IService {
	public:
		virtual auto GetSoftwareName() const -> QString = 0;
		virtual auto GetVersion() const -> Version = 0;
		virtual auto GetCorporation() const -> QString = 0;
		virtual auto GetContact() const -> QString = 0;
		virtual auto GetOpenSourceLicenseList() const -> QVector<OpenSourceLicense> = 0;
	};

	struct Version {
		int major = 0;
		int minor = 0;
		int patch = 0;
		char identifier = 0;

		auto ToString() const -> QString {
			return (identifier > 0) ? QString("%1.%2.%3%4").arg(major).arg(minor).arg(patch).arg(identifier) : QString("%1.%2.%3").arg(major).arg(minor).arg(patch);
		}
	};

	struct OpenSourceLicense {
		QString name;
		QString url;
		QString copyright;
		QString license;
	};
}
