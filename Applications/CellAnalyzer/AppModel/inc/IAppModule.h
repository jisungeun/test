#pragma once

#include <optional>

#include <QString>

#include "IService.h"

#include "CellAnalyzer.AppModelExport.h"

namespace CellAnalyzer {
	using Error = QString;

	class CellAnalyzer_AppModel_API IAppModule : public virtual Tomocube::IService {
	public:
		virtual auto Start() -> std::optional<Error> = 0;
		virtual auto Stop() -> void = 0;
	};
}
