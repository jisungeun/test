#pragma once

#include <QVariantMap>

#include "IPresenterType.h"

#include "CellAnalyzer.Presenter.ServiceExport.h"

namespace CellAnalyzer::Presenter::Service {
	class CellAnalyzer_Presenter_Service_API PresenterType final : public IPresenterType {
	public:
		explicit PresenterType(const QVariantMap& info);
		~PresenterType() override;

		auto IsValid() const -> bool;

		auto GetID() const -> QString override;
		auto GetCategory() const -> QString override;
		auto GetDescription() const -> QString override;
		auto GetDataPort() const -> DataPort override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
