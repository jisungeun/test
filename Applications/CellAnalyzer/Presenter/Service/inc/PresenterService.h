#pragma once

#include "IAppModule.h"
#include "IPresenterService.h"

#include "CellAnalyzer.Presenter.ServiceExport.h"

namespace CellAnalyzer::Presenter::Service {
	class CellAnalyzer_Presenter_Service_API PresenterService final : public IAppModule, public IPresenterService {
	public:
		PresenterService();
		~PresenterService() override;

		auto Start() -> std::optional<Error> override;
		auto Stop() -> void override;

		auto GetIdList() const -> QStringList override;
		auto GetType(const QString& id) const -> PresenterTypePtr override;
		auto GetDescription(const QString& id) const -> QString override;
		auto CreatePresenter(const QString& id) -> PresenterPtr override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
