#include "PresenterType.h"

namespace CellAnalyzer::Presenter::Service {
	struct PresenterType::Impl {
		QString id;
		QString category;
		QString desc;
		DataPort port;
	};

	PresenterType::PresenterType(const QVariantMap& info) : IPresenterType(), d(new Impl) {
		d->id = info["ID"].toString();
		d->category = info["Category"].toString();
		d->desc = info["Description"].toString();
		const auto portList = info["Port"].toList();
		for (auto port : portList) {
			auto layer = false;
			if (port.toMap().contains("Layered")) {
				layer = port.toMap()["Layered"].toBool();
			}
			const auto dataflags = ToDataFlags(port.toMap()["DataFlags"].toStringList());
			const auto PortName = port.toMap()["Name"].toString();
			d->port[PortName] = std::make_tuple(dataflags, layer);
		}
	}

	PresenterType::~PresenterType() = default;

	auto PresenterType::IsValid() const -> bool {
		return !d->id.isEmpty();
	}

	auto PresenterType::GetID() const -> QString {
		return d->id;
	}

	auto PresenterType::GetCategory() const -> QString {
		return d->category;
	}

	auto PresenterType::GetDescription() const -> QString {
		return d->desc;
	}

	auto PresenterType::GetDataPort() const -> DataPort {
		return d->port;
	}

}
