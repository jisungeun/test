#include <QCoreApplication>
#include <QJsonDocument>
#include <QStringList>

#include "PluginCollection.h"

#include "PresenterService.h"
#include "PresenterType.h"

#include "IPresenterFactory.h"

namespace CellAnalyzer::Presenter::Service {
	struct PresenterService::Impl {
		std::shared_ptr<Tomocube::IServiceProvider> provider = nullptr;
		QMap<QString, PresenterTypePtr> typeMap;

		QList<PresenterTypePtr> infoList;
	};

	PresenterService::PresenterService() : IAppModule(), IPresenterService(), d(new Impl) { }

	PresenterService::~PresenterService() = default;

	auto PresenterService::Start() -> std::optional<Error> {
		Tomocube::PluginInjection::PluginCollection collection;
		const auto map = GetLibMap();

		for (const auto& category : map.keys()) {
			for (const auto& process : map[category])
				collection.AddScoped<IPresenterFactory>(GetLibPath(category, process));
		}

		d->provider = collection.BuildProvider();

		for (const auto& m : d->provider->GetMetadata<IPresenterFactory>()) {
			if (const auto type = std::make_shared<PresenterType>(m); type->IsValid())
				d->infoList.push_back(type);
		}

		return std::nullopt;
	}

	auto PresenterService::Stop() -> void { }

	auto PresenterService::GetIdList() const -> QStringList {
		QStringList list;

		for (const auto& i : d->infoList)
			list.push_back(i->GetID());

		return list;
	}

	auto PresenterService::GetType(const QString& id) const -> PresenterTypePtr {
		for (const auto& i : d->infoList) {
			if (i->GetID() == id)
				return i;
		}

		return {};
	}

	auto PresenterService::GetDescription(const QString& id) const -> QString {
		for (const auto& i : d->infoList) {
			if (i->GetID() == id) {
				return i->GetDescription();
			}
		}
		return {};
	}

	auto PresenterService::CreatePresenter(const QString& type) -> PresenterPtr {
		for (int i = 0; i < d->infoList.count(); i++) {
			if (d->infoList[i]->GetID() == type) {
				if (const auto factory = d->provider->GetService<IPresenterFactory>(i)) {
					return factory->CreateInstance();
				}
			}
		}

		return {};
	}
}
