#pragma once

#include <memory>

#include <QObject>

#include "IService.h"

#include "CellAnalyzer.Presenter.PresenterModelExport.h"

namespace CellAnalyzer::Presenter {
	class IPresenter;

	class CellAnalyzer_Presenter_PresenterModel_API IPresenterFactory : public QObject, public virtual Tomocube::IService {
		Q_OBJECT

	public:
		virtual auto CreateInstance() -> std::shared_ptr<IPresenter> = 0;
	};
}
