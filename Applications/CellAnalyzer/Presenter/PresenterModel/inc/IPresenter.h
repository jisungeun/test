#pragma once

#include "IView.h"

#include "IPresenterFactory.h"
#include "IPresenterType.h"

#include "CellAnalyzer.Presenter.PresenterModelExport.h"

namespace CellAnalyzer::Presenter {
	class IPresenter;
	using PresenterPtr = std::shared_ptr<IPresenter>;
	using PresenterList = QVector<PresenterPtr>;

	class CellAnalyzer_Presenter_PresenterModel_API IPresenter : public IView {
	public:
		virtual auto GetWindowList() const -> ViewList = 0;
		virtual auto IsPrimary(const ViewPtr& window) const -> bool = 0;
		virtual auto IsAcceptable(const DataPtr& data) const -> bool = 0;
		virtual auto SetTitle(const QString& title) -> void = 0;

		virtual auto GetDataList() const -> QStringList = 0;
		virtual auto GetData(const QString& name) const -> DataPtr = 0;
		virtual auto GetName(const DataPtr& data) const -> QString = 0;

		virtual auto AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool = 0;
		virtual auto RenameData(const DataPtr& data, const QString& name) -> bool = 0;
		virtual auto RemoveData(const QString& name) -> void = 0;
		virtual auto ClearData() -> void = 0;

		virtual auto Capture(const QString& filepath) -> void = 0;
	};
}

Q_DECLARE_INTERFACE(CellAnalyzer::Presenter::IPresenterFactory, "CellAnalyzer::Presenter::IPresenterFactory");

#define DEFAULT_PATH "../rsc/TypeInfo.json"

#define DECLARE_PRESENTER_START(Name) \
	class Name##Factory final : public CellAnalyzer::Presenter::IPresenterFactory

#define DECLARE_PRESENTER_END(Name, Category, TypeInfo) \
		Q_INTERFACES(CellAnalyzer::Presenter::IPresenterFactory)\
		Q_PLUGIN_METADATA(IID "CellAnalyzer::Presenter::" #Category "::" #Name "Factory" FILE TypeInfo)\
	public:\
		auto CreateInstance() -> std::shared_ptr<IPresenter> override {\
			return std::make_shared<CellAnalyzer::Presenter::##Category##::##Name>();\
		}
