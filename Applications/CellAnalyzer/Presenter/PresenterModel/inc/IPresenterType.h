#pragma once

#include "IData.h"

#include "CellAnalyzer.Presenter.PresenterModelExport.h"

namespace CellAnalyzer::Presenter {
	class IPresenterType;
	using PresenterTypePtr = std::shared_ptr<IPresenterType>;
	using PresenterTypeList = QVector<PresenterTypePtr>;
	using DataPort = QMap<QString, std::tuple<DataFlags, bool>>;

	class CellAnalyzer_Presenter_PresenterModel_API IPresenterType : public virtual Tomocube::IService {
	public:
		virtual auto GetID() const -> QString = 0;
		virtual auto GetCategory() const -> QString = 0;
		virtual auto GetDescription() const -> QString = 0;
		virtual auto GetDataPort() const -> DataPort = 0;
	};
}
