#pragma once

#include "IPresenter.h"
#include "IPresenterType.h"

#include "CellAnalyzer.Presenter.ServiceModelExport.h"

namespace CellAnalyzer::Presenter {
	class CellAnalyzer_Presenter_ServiceModel_API IPresenterService : public virtual Tomocube::IService {
	public:
		virtual auto GetIdList() const -> QStringList = 0;
		virtual auto GetType(const QString& id) const -> PresenterTypePtr = 0;
		virtual auto GetDescription(const QString& id) const->QString = 0;

		virtual auto CreatePresenter(const QString& id) -> PresenterPtr = 0;

	protected:
		static auto GetLibMap() -> QMap<QString, QStringList>;
		static auto GetLibPath(const QString& category, const QString& presenter) -> QString;
	};
}
