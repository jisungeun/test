#include <QCoreApplication>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include "IPresenterService.h"

namespace CellAnalyzer::Presenter {
	auto IPresenterService::GetLibMap() -> QMap<QString, QStringList> {
		if (QFile file(":/PresenterList.json"); file.open(QIODevice::ReadOnly)) {
			QMap<QString, QStringList> libMap;
			const auto doc = QJsonDocument::fromJson(file.readAll());
			const auto obj = doc.object();
			const auto map = obj.toVariantMap();

			for (const auto& category : map.keys())
				libMap[category] = map[category].toStringList();

			return libMap;
		}
		
		return {}; 
	}

	auto IPresenterService::GetLibPath(const QString& category, const QString& presenter) -> QString {
		const auto appPath = QCoreApplication::applicationDirPath();

		if (category.isEmpty())
			return QString("%1/Presenter/CellAnalyzer.Presenter.%2.dll").arg(appPath).arg(presenter);

		return QString("%1/Presenter/CellAnalyzer.Presenter.%2.%3.dll").arg(appPath).arg(category).arg(presenter);
	}
}
