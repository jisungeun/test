#pragma once

#include <QWidget>

#include "IView.h"

namespace CellAnalyzer::Presenter::Image2D {
	class SampleWindow final : public QWidget, public IView {
	public:
		explicit SampleWindow(QWidget* parent = nullptr);
		~SampleWindow() override;

		auto GetTitle() const -> QString override;
		auto GetIcon() const -> QString override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
