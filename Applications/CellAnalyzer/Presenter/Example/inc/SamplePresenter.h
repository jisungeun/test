#pragma once

#include <QWidget>

#include "IPresenter.h"

namespace CellAnalyzer::Presenter::Image2D {
	class SamplePresenter final : public QWidget, public IPresenter {
	public:
		SamplePresenter();
		~SamplePresenter() override;

		auto GetTitle() const -> QString override;
		auto GetIcon() const -> QString override;
		auto SetTitle(const QString& title) const -> void override;

		auto GetWindowList() const -> ViewList override;
		auto IsPrimary(const ViewPtr& window) const -> bool override;

		auto IsAcceptable(const DataPtr& data) const -> bool override;
		auto GetDataList() const -> QStringList override;
		auto GetData(const QString& name) const -> DataPtr override;
		auto GetName(const DataPtr& data) const -> QString override;

		auto AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool override;
		auto RenameData(const DataPtr& data, const QString& name) -> bool override;
		auto RemoveData(const QString& name) -> void override;
		auto ClearData() -> void override;

		auto Capture(const QString& filepath) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	DECLARE_PRESENTER_START(SamplePresenter) {
		Q_OBJECT
		DECLARE_PRESENTER_END(SamplePresenter, Image2D, DEFAULT_PATH)
	};
}
