#include "SampleWindow.h"

#include "ui_SampleWindow.h"

namespace CellAnalyzer::Presenter::Image2D {
	struct SampleWindow::Impl {
		Ui::SampleWindow ui;
	};

	SampleWindow::SampleWindow(QWidget* parent) : QWidget(parent), IView(), d(new Impl) {
		d->ui.setupUi(this);
	}

	SampleWindow::~SampleWindow() = default;

	auto SampleWindow::GetTitle() const -> QString {
		return "Sample Window";
	}

	auto SampleWindow::GetIcon() const -> QString {
		return {};
	}
}
