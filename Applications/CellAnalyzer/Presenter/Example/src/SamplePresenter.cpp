#include "SamplePresenter.h"

#include "SampleWindow.h"

#include "ui_SamplePresenter.h"

namespace CellAnalyzer::Presenter::Image2D {
	struct SamplePresenter::Impl {
		Ui::SamplePresenter ui;
		QString title;

		std::shared_ptr<SampleWindow> window = nullptr;
		QString name;
	};

	SamplePresenter::SamplePresenter() : QWidget(), IPresenter(), d(new Impl) {
		d->ui.setupUi(this);
		d->window = std::make_shared<SampleWindow>();
	}

	SamplePresenter::~SamplePresenter() = default;

	auto SamplePresenter::SetTitle(const QString& title) const -> void {
		d->title = title;
	}

	auto SamplePresenter::GetTitle() const -> QString {
		return d->title;
	}

	auto SamplePresenter::GetIcon() const -> QString {
		return {};
	}

	auto SamplePresenter::GetWindowList() const -> ViewList {
		return { d->window };
	}

	auto SamplePresenter::IsPrimary(const ViewPtr& window) const -> bool {
		if (window == d->window)
			return true;

		return false;
	}

	auto SamplePresenter::IsAcceptable(const DataPtr& data) const -> bool {
		return false;
	}

	auto SamplePresenter::GetDataList() const -> QStringList {
		return { d->name };
	}

	auto SamplePresenter::GetData(const QString& name) const -> DataPtr {
		return {};
	}



	auto SamplePresenter::GetName(const DataPtr& data) const -> QString {
		return {};
	}

	auto SamplePresenter::AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool {
		return false;
	}

	auto SamplePresenter::RenameData(const DataPtr& data, const QString& name) -> bool {
		return false;
	}

	auto SamplePresenter::RemoveData(const QString& name) -> void {
		if (d->name == name)
			ClearData();
	}

	auto SamplePresenter::ClearData() -> void {
	}

	auto SamplePresenter::Capture(const QString& filepath) -> void {
		
	}
}
