#include <QIcon>

#include "ui_LayerItem.h"
#include "LayerItem.h"

#include <ColorMap.h>
#include <QColormapCanvas.h>

#include <iostream>
#include <QColorDialog>
#include <QFileIconProvider>

namespace CellAnalyzer::Presenter::Slice::Widget {
	struct LayerItem::Impl {
		Ui::Form ui;
		int order { 0 };
		QString name;
		QString iconPath;
		bool visible { true };

		Meta::SliceMetaPtr info { nullptr };

		QMap<QString, QList<float>> predefColormap;
		QPixmap singletonePixmap;
		ColorMapPtr tf { nullptr };
		int maxNum { 256 };
		std::shared_ptr<QColormapCanvas> colormapWidget { nullptr };
		float color_table[12][3];

		auto BuildLabelColorTable() -> void;
		auto BuildLabelColormap() -> QList<float>;
		auto BuildSingletone() -> QList<float>;
		auto RebuildColormap(QList<float> naive) -> QList<float>;
	};

	auto LayerItem::Impl::BuildLabelColorTable() -> void {
		color_table[0][0] = 255;
		color_table[0][1] = 59;
		color_table[0][2] = 48; //red
		color_table[1][0] = 255;
		color_table[1][1] = 149;
		color_table[1][2] = 0; //Orange
		color_table[2][0] = 255;
		color_table[2][1] = 204;
		color_table[2][2] = 0; //yellow
		color_table[3][0] = 52;
		color_table[3][1] = 199;
		color_table[3][2] = 89; //Green
		color_table[4][0] = 0;
		color_table[4][1] = 199;
		color_table[4][2] = 190; //Mint
		color_table[5][0] = 48;
		color_table[5][1] = 176;
		color_table[5][2] = 199; //Teal
		color_table[6][0] = 50;
		color_table[6][1] = 173;
		color_table[6][2] = 230; //Cyan
		color_table[7][0] = 0;
		color_table[7][1] = 122;
		color_table[7][2] = 255; //Blue
		color_table[8][0] = 88;
		color_table[8][1] = 86;
		color_table[8][2] = 214; //Indigo
		color_table[9][0] = 175;
		color_table[9][1] = 82;
		color_table[9][2] = 222; //Purple
		color_table[10][0] = 255;
		color_table[10][1] = 45;
		color_table[10][2] = 85; //Pink
		color_table[11][0] = 162;
		color_table[11][1] = 132;
		color_table[11][2] = 94; //Brown
	}

	auto LayerItem::Impl::BuildLabelColormap() -> QList<float> {
		const auto imin = static_cast<int>(ui.rangeMin->value());
		QList<float> arr;
		for (auto i = 0; i < 1024; i++) {
			arr.append(0);
		}
		tf->Clear();
		for (auto i = 0; i < 256; i++) {
			arr[i * 4] = color_table[(i + imin) % 12][0] / 255.0f;
			arr[i * 4 + 1] = color_table[(i + imin) % 12][1] / 255.0f;
			arr[i * 4 + 2] = color_table[(i + imin) % 12][2] / 255.0f;
			arr[i * 4 + 3] = 1;
		}
		return arr;
	}

	auto LayerItem::Impl::RebuildColormap(QList<float> naive) -> QList<float> {
		const auto gamma = info->GetGamma();
		QList<float> result;
		for (auto i = 0; i < 256; i++) {
			const auto ratio = static_cast<float>(i) / 255.0f;
			const auto gammaFactor = static_cast<float>(pow(ratio, gamma));
			result.append(naive[i * 4] * gammaFactor);
			result.append(naive[i * 4 + 1] * gammaFactor);
			result.append(naive[i * 4 + 2] * gammaFactor);
			result.append(naive[i * 4 + 3] * gammaFactor);
		}
		return result;
	}

	auto LayerItem::Impl::BuildSingletone() -> QList<float> {
		const auto gamma = info->GetGamma();
		const auto colortone = info->GetSingleTone();
		QList<float> result;
		for (auto i = 0; i < 256; i++) {
			const auto ratio = static_cast<float>(i) / 255.0f;
			const auto gammaFactor = static_cast<float>(pow(ratio, gamma));
			result.append(static_cast<float>(colortone.redF()) * gammaFactor);
			result.append(static_cast<float>(colortone.greenF()) * gammaFactor);
			result.append(static_cast<float>(colortone.blueF()) * gammaFactor);
			result.append(gammaFactor);
		}
		return result;
	}

	LayerItem::LayerItem(Meta::SliceMetaPtr info, QWidget* parent) : QWidget(parent), d { new Impl } {
		d->info = info;
		d->ui.setupUi(this);
		d->BuildLabelColorTable();
		d->colormapWidget = std::make_shared<QColormapCanvas>();
		auto layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		d->ui.colormapSocket->setLayout(layout);
		layout->addWidget(d->colormapWidget.get());

		d->tf = std::make_shared<ColorMap>();
		d->colormapWidget->SetColorTransferFunction(d->tf);

		d->ui.frame->hide();

		d->ui.labelSpin->hide();
		d->ui.labelLabel->hide();
	}

	LayerItem::~LayerItem() { }

	auto LayerItem::InitUI() -> void {
		d->ui.collapseToggle->setIcon(QIcon(":/Widget/ArrowExpand.svg"));

		d->ui.titleBtn->setCheckable(true);
		d->ui.titleBtn->setStyleSheet("text-align:left; padding-left:15px;");
		d->ui.visibilityBtn->setIcon(QIcon(":/Widget/Visible.svg"));
		d->ui.visibilityBtn->setIconSize(QSize(28, 28));
		d->ui.titleBtn->setMinimumHeight(30);

		d->ui.transSpin->setRange(0, 1);
		d->ui.transSpin->setValue(0);
		d->ui.transSpin->setSingleStep(0.01);
		d->ui.transSpin->setDecimals(2);

		const auto [min, max] = d->info->GetRange();
		const auto textmin = min / d->info->GetDivider() + d->info->GetOffset();;
		const auto textmax = max / d->info->GetDivider() + d->info->GetOffset();
		d->ui.rangeMin->setRange(textmin, textmax - d->info->GetSingleStep());
		d->ui.rangeMin->setSingleStep(d->info->GetSingleStep());
		d->ui.rangeMin->setDecimals(d->info->GetDecimals());
		d->ui.rangeMin->setValue(textmin);

		d->ui.rangeMax->setRange(textmin + d->info->GetSingleStep(), textmax);
		d->ui.rangeMax->setSingleStep(d->info->GetSingleStep());
		d->ui.rangeMax->setDecimals(d->info->GetDecimals());
		d->ui.rangeMax->setValue(textmax);

		d->ui.gammaSpin->setRange(0.01, 2);
		d->ui.gammaSpin->setSingleStep(0.01);
		d->ui.gammaSpin->setValue(d->info->GetGamma());

		d->ui.colormapCombo->addItem("Single Tone");
		d->ui.colormapCombo->addItem("Label Mask");
		d->ui.colormapCombo->addItem("Custom");

		d->ui.visibilityBtn->setToolTip("Show/Hide");
		d->ui.collapseToggle->setToolTip("Show layer control");

		connect(d->ui.titleBtn, SIGNAL(clicked()), this, SLOT(OnSelected()));
		connect(d->ui.visibilityBtn, SIGNAL(clicked()), this, SLOT(OnVisibility()));
		connect(d->ui.collapseToggle, SIGNAL(pressed()), this, SLOT(OnCollapseToggle()));
		connect(d->ui.singletoneBtn, SIGNAL(clicked()), this, SLOT(OnSingletone()));

		connect(d->ui.transSpin, SIGNAL(valueChanged(double)), this, SIGNAL(sigTransp(double)));
		connect(d->ui.rangeMin, SIGNAL(valueChanged(double)), this, SLOT(OnMinChanged(double)));
		connect(d->ui.rangeMax, SIGNAL(valueChanged(double)), this, SLOT(OnMaxChanged(double)));
		connect(d->ui.colormapCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnColormapChanged(int)));
		connect(d->ui.gammaSpin, SIGNAL(valueChanged(double)), this, SLOT(OnGammaChanged(double)));
		connect(d->colormapWidget.get(), SIGNAL(sigRender()), this, SLOT(OnCustomColormapChanged()));

		const auto singletone = d->info->GetSingleTone();
		d->singletonePixmap = QPixmap(25, 25);
		d->singletonePixmap.fill(singletone);
		d->ui.singletoneBtn->setIcon(QIcon(d->singletonePixmap));

		const auto colormapID = d->info->GetColormapID();
		if (false == colormapID.isEmpty()) {
			const auto idx = d->ui.colormapCombo->findText(colormapID);
			if (idx > -1) {
				d->ui.colormapCombo->setCurrentIndex(idx);
				OnColormapChanged(idx);
				return;
			}
		}
		const auto customIdx = d->ui.colormapCombo->findText("Custom");
		d->ui.colormapCombo->setCurrentIndex(customIdx);
	}

	auto LayerItem::GetMaxTFstep() const -> int {
		return d->maxNum;
	}

	auto LayerItem::GetColorMap() -> float* {
		return d->tf->GetDataPointer();
	}

	auto LayerItem::AddColorMap(const QString& title, QList<float> colormap) -> void {
		if (d->predefColormap.contains(title)) {
			return;
		}
		d->ui.colormapCombo->blockSignals(true);
		d->ui.colormapCombo->addItem(title);
		d->ui.colormapCombo->blockSignals(false);

		d->predefColormap[title] = colormap;
	}

	auto LayerItem::SetChecked(bool isChecked) -> void {
		d->ui.titleBtn->setChecked(isChecked);
	}

	auto LayerItem::GetChecked() -> bool {
		return d->ui.titleBtn->isChecked();
	}

	auto LayerItem::SetVisibility(bool isVisible) -> void {
		d->visible = isVisible;
		if (d->visible) {
			d->ui.visibilityBtn->setIcon(QIcon(":/Widget/Visible.svg"));
		} else {
			d->ui.visibilityBtn->setIcon(QIcon(":/Widget/Invisible.svg"));
		}
		d->ui.visibilityBtn->setIconSize(QSize(28, 28));
	}

	auto LayerItem::GetVisibility() -> bool {
		return d->visible;
	}

	auto LayerItem::GetName() const -> QString {
		return d->name;
	}

	auto LayerItem::SetIcon(const QString& icon) -> void {
		d->iconPath = icon;
		d->ui.titleBtn->setIcon(QIcon(d->iconPath));
	}

	auto LayerItem::GetOrder() -> int {
		return d->order;
	}

	auto LayerItem::SetName(const QString& name) -> void {
		d->name = name;
		d->ui.titleBtn->setText(d->name);
	}

	auto LayerItem::SetOrder(int order) -> void {
		d->order = order;
	}

	//Slost
	void LayerItem::OnGammaChanged(double gamma) {
		d->info->SetGamma(gamma);
		const auto colormapTitle = d->ui.colormapCombo->currentText();
		const auto isLabel = colormapTitle == "Label Mask";
		if (isLabel) {
			//DO NOTHING
		} else if (colormapTitle == "Single Tone") {
			d->tf->SetColormap(d->BuildSingletone());
		} else if (colormapTitle == "Custom") {
			//DO NOTHING
		} else {
			const auto colormapName = d->ui.colormapCombo->currentText();
			const auto naiveColormap = d->predefColormap[colormapName];
			if (gamma < 1.01 && gamma > 0.99) {
				d->tf->SetColormap(naiveColormap);
			} else {
				d->tf->SetColormap(d->RebuildColormap(naiveColormap));
			}
		}
		d->colormapWidget->update();
		emit sigColormap(d->tf->GetDataPointer(), isLabel, d->maxNum);
	}

	void LayerItem::OnColormapChanged(int idx) {
		d->info->SetColormapID(d->ui.colormapCombo->currentText());
		d->colormapWidget->setEnabled(false);
		d->tf->ToggleLinearInterpolation(true);
		const auto colormapTitle = d->ui.colormapCombo->currentText();
		d->maxNum = 256;
		const auto isLabel = colormapTitle == "Label Mask";
		if (isLabel) {
			const auto min = static_cast<int>(d->ui.rangeMin->value());
			const auto max = static_cast<int>(d->ui.rangeMax->value()) - min;
			d->maxNum = max;
			d->tf->ToggleLinearInterpolation(false);
			d->tf->SetColormap(d->BuildLabelColormap());
			emit sigDataRange(min, d->ui.rangeMax->value() + 1);
		} else if (colormapTitle == "Single Tone") {
			d->tf->SetColormap(d->BuildSingletone());
		} else if (colormapTitle == "Custom") {
			d->tf->Clear();
			d->colormapWidget->setEnabled(true);
		} else {
			const auto colormapName = d->ui.colormapCombo->currentText();
			const auto naiveColormap = d->predefColormap[colormapName];
			if (d->info->GetGamma() < 1.01 && d->info->GetGamma() > 0.99) {
				d->tf->SetColormap(naiveColormap);
			} else {
				d->tf->SetColormap(d->RebuildColormap(naiveColormap));
			}
		}
		d->colormapWidget->update();
		emit sigColormap(d->tf->GetDataPointer(), d->maxNum, isLabel);
	}

	void LayerItem::OnMaxChanged(double val) {
		if (val <= d->ui.rangeMin->value()) {
			d->ui.rangeMax->blockSignals(true);
			d->ui.rangeMax->setValue(val + d->info->GetSingleStep());
			d->ui.rangeMax->blockSignals(false);
			return;
		}
		const auto realMin = (d->ui.rangeMin->value() - d->info->GetOffset()) * d->info->GetDivider();
		const auto realMax = (d->ui.rangeMax->value() - d->info->GetOffset()) * d->info->GetDivider();
		if (d->ui.colormapCombo->currentText() == "Label Mask") {
			const auto min = static_cast<int>(d->ui.rangeMin->value());
			const auto max = static_cast<int>(d->ui.rangeMax->value()) - min;
			d->maxNum = max;
			d->tf->SetColormap(d->BuildLabelColormap());
			d->colormapWidget->update();
			emit sigColormap(d->tf->GetDataPointer(), d->maxNum, true);
			emit sigDataRange(min, d->ui.rangeMax->value() + 1);
		} else {
			emit sigDataRange(realMin, realMax);
		}
	}

	void LayerItem::OnMinChanged(double val) {
		if (val >= d->ui.rangeMax->value()) {
			d->ui.rangeMin->blockSignals(true);
			d->ui.rangeMin->setValue(val - d->info->GetSingleStep());
			d->ui.rangeMin->blockSignals(false);
			return;
		}
		const auto realMin = (d->ui.rangeMin->value() - d->info->GetOffset()) * d->info->GetDivider();
		const auto realMax = (d->ui.rangeMax->value() - d->info->GetOffset()) * d->info->GetDivider();

		if (d->ui.colormapCombo->currentText() == "Label Mask") {
			const auto min = static_cast<int>(d->ui.rangeMin->value());
			const auto max = static_cast<int>(d->ui.rangeMax->value()) - min;
			d->maxNum = max;
			d->tf->SetColormap(d->BuildLabelColormap());
			d->colormapWidget->update();
			emit sigColormap(d->tf->GetDataPointer(), d->maxNum, true);
			emit sigDataRange(min, d->ui.rangeMax->value() + 1);
		} else {
			emit sigDataRange(realMin, realMax);
		}
	}

	void LayerItem::OnSingletone() {
		QColor new_col = QColorDialog::getColor(d->info->GetSingleTone(), nullptr, "Select Color");
		if (new_col.isValid()) {
			d->info->SetSingleTone(new_col);

			d->singletonePixmap.fill(new_col);
			d->ui.singletoneBtn->setIcon(QIcon(d->singletonePixmap));

			const auto singletoneIdx = d->ui.colormapCombo->findText("Single Tone");

			d->ui.colormapCombo->blockSignals(true);
			d->ui.colormapCombo->setCurrentIndex(singletoneIdx);
			d->ui.colormapCombo->blockSignals(false);

			d->tf->SetColormap(d->BuildSingletone());
			d->colormapWidget->update();

			emit sigColormap(d->tf->GetDataPointer(), d->maxNum, false);
		}
	}

	void LayerItem::OnCollapseToggle() {
		if (d->ui.collapseToggle->isChecked()) {
			d->ui.frame->show();
			d->ui.collapseToggle->setIcon(QIcon(":/Widget/ArrowCollapse.svg"));
			d->ui.collapseToggle->setToolTip("Hide layer control");
		} else {
			d->ui.frame->hide();
			d->ui.collapseToggle->setIcon(QIcon(":/Widget/ArrowExpand.svg"));
			d->ui.collapseToggle->setToolTip("Show layer control");
		}
	}

	void LayerItem::OnSelected() {
		const auto selected = d->ui.titleBtn->isChecked();
		emit sigSelect(selected);
	}

	void LayerItem::OnVisibility() {
		d->visible = !d->visible;
		if (d->visible) {
			d->ui.visibilityBtn->setIcon(QIcon(":/Widget/Visible.svg"));
		} else {
			d->ui.visibilityBtn->setIcon(QIcon(":/Widget/Invisible.svg"));
		}
		d->ui.visibilityBtn->setIconSize(QSize(28, 28));
		emit sigViz(d->visible);
	}

	void LayerItem::OnCustomColormapChanged() {
		emit sigColormap(d->tf->GetDataPointer(), d->maxNum, false);
	}
}
