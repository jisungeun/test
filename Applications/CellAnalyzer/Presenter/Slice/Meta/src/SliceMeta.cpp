#include "SliceMeta.h"

namespace CellAnalyzer::Presenter::Slice::Meta {
	struct SliceMeta::Impl {
		double transparency{ 0 };
		double min{ 0 }, max{ 1 };
		int decimals{ 0 };
		double singleStep{ 1 };
		QColor singletone{ QColor(255,0,0) };
		double gamma{ 1 };
		QString colormapID{ "Custom" };
		QList<float> colormap;
		double divider{ 1 };
		double offset{ 0 };
		int maxZ{ 1 };
		int currentZ{ 1 };
	};
	SliceMeta::SliceMeta() : d{ new Impl } {
		
	}
	SliceMeta::~SliceMeta() {
		
	}
	auto SliceMeta::SetCurrentZ(int z) -> void {
		d->currentZ = z;
	}
	auto SliceMeta::GetCurrentZ() const -> int {
		return d->currentZ;
	}
	auto SliceMeta::SetMaxZ(int z) -> void {
		d->maxZ = z;
	}
	auto SliceMeta::GetMaxZ() const -> int {
		return d->maxZ;
	}
	auto SliceMeta::SetOffset(double offset) -> void {
		d->offset = offset;
	}
	auto SliceMeta::GetOffset() const -> double {
		return d->offset;
	}
	auto SliceMeta::SetDivider(double div) -> void {
		d->divider = div;
	}
	auto SliceMeta::GetDivider() const -> double {
		return d->divider;
	}
	auto SliceMeta::SetTransparency(double tranp) -> void {
		d->transparency = tranp;
	}
	auto SliceMeta::GetTransparancy() const -> double {
		return d->transparency;
	}
	auto SliceMeta::SetRange(double min, double max) -> void {
		d->min = min;
		d->max = max;
	}
	auto SliceMeta::GetRange() const -> std::tuple<double, double> {
		return std::make_tuple(d->min, d->max);
	}
	auto SliceMeta::SetDecimals(int dec) -> void {
		d->decimals = dec;
	}
	auto SliceMeta::GetDecimals() const -> int {
		return d->decimals;
	}
	auto SliceMeta::SetSingleStep(double singleStep) -> void {
		d->singleStep = singleStep;
	}
	auto SliceMeta::GetSingleStep() const -> double {
		return d->singleStep;
	}
	auto SliceMeta::SetSingleTone(QColor tone) -> void {
		d->singletone = tone;
	}
	auto SliceMeta::GetSingleTone() const -> QColor {
		return d->singletone;
	}
	auto SliceMeta::SetGamma(double gamma) -> void {
		d->gamma = gamma;
	}
	auto SliceMeta::GetGamma() const -> double {
		return d->gamma;
	}
	auto SliceMeta::SetColormapID(const QString& ID) -> void {
		d->colormapID = ID;
	}
	auto SliceMeta::GetColormapID() const -> QString {
		return d->colormapID;
	}
	auto SliceMeta::SetColormap(QList<float> colormap) -> void {
		if(colormap.count() != 1024) {
			return;
		}
		d->colormap.clear();
		for(auto i=0;i<1024;i++) {
			d->colormap.append(colormap[i]);
		}
	}
	auto SliceMeta::GetColormap() -> QList<float> {
		return d->colormap;
	}
}