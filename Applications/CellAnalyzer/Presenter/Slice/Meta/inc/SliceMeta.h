#pragma once

#include <memory>

#include <tuple>

#include <QColor>

#include "CellAnalyzer.Presenter.Slice.MetaExport.h"

namespace CellAnalyzer::Presenter::Slice::Meta {
	class SliceMeta;
	using SliceMetaPtr = std::shared_ptr<SliceMeta>;
	class CellAnalyzer_Presenter_Slice_Meta_API SliceMeta{		
	public:
		SliceMeta();
		~SliceMeta();

		auto SetTransparency(double tranp)->void;
		auto GetTransparancy()const->double;

		auto SetRange(double min, double max)->void;
		auto GetRange()const->std::tuple<double, double>;

		auto SetDecimals(int dec)->void;
		auto GetDecimals()const->int;

		auto SetSingleStep(double singleStep)->void;
		auto GetSingleStep()const->double;

		auto SetSingleTone(QColor tone)->void;
		auto GetSingleTone()const->QColor;

		auto SetGamma(double gamma)->void;
		auto GetGamma()const->double;

		auto SetColormapID(const QString& ID)->void;
		auto GetColormapID()const->QString;

		auto SetColormap(QList<float> colormap)->void;
		auto GetColormap()->QList<float>;

		auto SetDivider(double div)->void;
		auto GetDivider()const->double;

		auto SetOffset(double offset)->void;
		auto GetOffset()const->double;

		auto SetCurrentZ(int z)->void;
		auto GetCurrentZ()const->int;

		auto SetMaxZ(int z)->void;
		auto GetMaxZ()const->int;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};	
}