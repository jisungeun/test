#pragma once

#include <QWidget>
#include <SliceMeta.h>

#include "IView.h"

namespace CellAnalyzer::Presenter::Slice {
	class SliceOIVLayer final : public QWidget, public IView {
		Q_OBJECT
	public:
		explicit SliceOIVLayer(QWidget* parent = nullptr);
		~SliceOIVLayer() override;

		auto AddLayer(const QString& ID, const QString& iconPath, Meta::SliceMetaPtr meta) -> void;
		auto RemoveLayer(const QString& ID) -> void;
		auto Clear() const -> void;
		auto ChangeName(const QString& prevName, const QString& changedName) -> void;
		auto GetCurrentID() const -> QString;

		auto AddColorMap(const QString& title, float* colormap) -> void;
		auto GetTF(const QString& ID) -> std::tuple<float*, int>;
		auto SetToggleViz(const QString& ID, bool show) -> void;

		auto SetZ(int z) -> void;
		auto UpdateZRange(int max) -> void;

	signals:
		void sigLayerOrder(QStringList);
		void sigToggleViz(QString, bool);
		void sigCapture(QString);
		void sigItemSelected(QString);
		void sigUpdateColormap(QString, float*, int, bool);
		void sigUpdateRange(QString, double, double);
		void sigupdateTranp(QString, double);
		void sigZChanged(int);
		void sigRemove(QString);
		void sigUpsample(int);

	protected:
		auto OnTop() -> void;
		auto OnDown() -> void;
		auto OnUp() -> void;
		auto OnToggleVizAll() -> void;
		auto OnRemove() -> void;
		auto OnCapture() -> void;
		auto OnToggleViz(const QString& name, bool isVisible) -> void;
		auto OnZChanged(int idx) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
