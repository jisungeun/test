#pragma once

#include <enum.h>

#include <QOivRenderWindow.h>

namespace CellAnalyzer::Presenter::Slice {
	BETTER_ENUM(Direction2D, int,
				XY = 0,
				YZ = 1,
				XZ = 2)

	class SliceOIVWindow final : public QOivRenderWindow {
		Q_OBJECT
	public:
		explicit SliceOIVWindow(QWidget* parent = nullptr);
		~SliceOIVWindow() override;

		auto ResetView2D(Direction2D dir) -> void;

	signals:
		auto sigMouseWheel(int) -> void;
		auto sigMouseClick() -> void;
		auto sigMousePos(float, float) -> void;
		auto sigPanning() -> void;
		auto sigZooming() -> void;

	protected:
		auto MouseButtonEvent(SoEventCallback* node) -> void override;
		auto MouseMoveEvent(SoEventCallback* node) -> void override;
		auto KeyboardEvent(SoEventCallback* node) -> void override;
		auto MouseWheelEvent(SoEventCallback* node) -> void override;

		auto setDefaultWindowType() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
