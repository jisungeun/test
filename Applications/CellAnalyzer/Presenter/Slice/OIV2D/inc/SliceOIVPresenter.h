#pragma once

#include <QWidget>

#include <IPresenter.h>

namespace CellAnalyzer::Presenter::Slice {
	class SliceOIVPresenter final : public IPresenter, public QWidget {
	public:
		SliceOIVPresenter(QWidget* parent = nullptr);
		~SliceOIVPresenter() override;

		auto SetTitle(const QString& title) -> void override;

		auto GetWindowList() const -> ViewList override;
		auto IsPrimary(const ViewPtr& window) const -> bool override;

		auto IsAcceptable(const DataPtr& data) const -> bool override;
		auto GetDataList() const -> QStringList override;
		auto GetData(const QString& name) const -> DataPtr override;
		auto GetName(const DataPtr& data) const -> QString override;

		auto AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool override;
		auto RenameData(const DataPtr& data, const QString& name) -> bool override;
		auto RemoveData(const QString& name) -> void override;
		auto ClearData() -> void override;

		auto Capture(const QString& filepath) -> void override;

	protected:
		auto OnColormap(QString name, float* arr, bool isLabel, int maxNum) -> void;
		auto OnRange(QString name, double min, double max) -> void;
		auto OnToggleViz(QString name, double show) -> void;
		auto OnTransp(QString name, double transp) -> void;
		auto OnLayerOrder(QStringList layerOrder) -> void;
		auto OnZChanged(int z) -> void;
		auto OnMouseWheel(int delta) -> void;
		auto OnRemoveLayer(QString name) -> void;
		auto OnMouseClick() -> void;

	private:
		auto CaptureScreen(const QString& path, bool fullResolution = false) -> void;
		auto Add2dData(const QString& name, const DataPtr& data) -> bool;
		auto Add3dData(const QString& name, const DataPtr& data) -> bool;

		struct Impl;
		std::unique_ptr<Impl> d;
	};

	DECLARE_PRESENTER_START(SliceOIVPresenter) {
		Q_OBJECT
		DECLARE_PRESENTER_END(SliceOIVPresenter, Slice, DEFAULT_PATH)
	};
}
