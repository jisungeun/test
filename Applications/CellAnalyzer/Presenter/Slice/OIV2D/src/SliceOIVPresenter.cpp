#include "SliceOIVPresenter.h"

#include <HT2D.h>
#include <HT3D.h>
#include <FL2D.h>
#include <FL3D.h>
#include <BinaryMask2D.h>
#include <BinaryMask3D.h>
#include <LabelMask2D.h>
#include <LabelMask3D.h>
#include <Float2d.h>
#include <Float3d.h>
#include <QMessageBox>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFileDialog>
#include <QSettings>

#include <SliceGeneral.h>

#include "SliceOIVLayer.h"
#include "SliceOIVWindow.h"
#include <SliceMeta.h>

#include <VolumeViz/nodes/SoDataRange.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/Qt/SoQt.h>
#include <Medical/helpers/MedicalHelper.h>
#include <VolumeViz/nodes/SoVolumeData.h>

#include "ui_SliceOIVPresenter.h"

namespace CellAnalyzer::Presenter::Slice {
	using namespace Tomocube::Rendering;

	struct SliceOIVPresenter::Impl {
		Ui::SliceOIV ui;

		QString title;
		std::shared_ptr<SliceOIVWindow> window { nullptr };
		std::shared_ptr<SliceOIVLayer> layer { nullptr };

		QMap<QString, DataPtr> dataMap;
		QList<std::shared_ptr<Image::SliceGeneral>> sliceList;
		DataFlags dataflag;

		int curDimZ { 1 };
		double curResZ { 1 };
		double curOffsetZ { 0 };
		bool ht3dExist { false };
		bool ref3dExist { false };

		int upsample { 1 };
		int currentZ { 0 };
		double currentPhyZ { 0 };

		//SceneGraph
		SoRef<SoSeparator> root { nullptr };
		int dimension { -1 };
		QString invalidLetterInOiv = " +{}[]'""\".";

		auto CheckDimension(int dim) -> bool;

		auto BuildColorMap() -> void;

		//2D Layer control UI
		auto AddHT2dLayer(QString name, std::shared_ptr<Data::HT2D> data) -> bool;
		auto AddFL2dLayer(QString name, std::shared_ptr<Data::FL2D> data) -> bool;
		auto AddFloat2dLayer(QString name, std::shared_ptr<Data::Float2D> data) -> bool;
		auto AddBinary2dLayer(QString name, std::shared_ptr<Data::BinaryMask2D> data) -> bool;
		auto AddLabel2dLayer(QString name, std::shared_ptr<Data::LabelMask2D> data) -> bool;

		//3D Layer control UI
		auto AddHT3dLayer(QString name, std::shared_ptr<Data::HT3D> data) -> bool;
		auto AddFL3dLayer(QString name, std::shared_ptr<Data::FL3D> data) -> bool;
		auto AddFloat3dLayer(QString name, std::shared_ptr<Data::Float3D> data) -> bool;
		auto AddBinary3dLayer(QString name, std::shared_ptr<Data::BinaryMask3D> data) -> bool;
		auto AddLabel3dLayer(QString name, std::shared_ptr<Data::LabelMask3D> data) -> bool;

		auto SetLayerOrder(const QString& name, int order) -> void;
		auto GetPhysicalZ(int iz, double spz, double originZ, double offset) -> double;
		auto AdjustOffsetZ(double offset) -> double;

		//save / load cam meta info
		auto saveCamInfo(double x, double y, double z, double zoomFactor, const QString& path) -> bool;
		auto loadCamInfo(double& x, double& y, double& z, double& zoomFactor, const QString& path) -> bool;
	};

	auto SliceOIVPresenter::Impl::saveCamInfo(double x, double y, double z, double zoomFactor, const QString& path) -> bool {
		// Create a JSON object
		QJsonObject jsonObject;
		jsonObject["x"] = x;
		jsonObject["y"] = y;
		jsonObject["z"] = z;
		jsonObject["zoomFactor"] = zoomFactor;

		// Convert JSON object to a QJsonDocument
		QJsonDocument jsonDoc(jsonObject);

		// Write JSON data to a file
		QFile file(path);
		if (!file.open(QIODevice::WriteOnly)) {
			qWarning() << "Couldn't open file for writing.";
			return false;
		}

		file.write(jsonDoc.toJson());

		file.close();

		return true;
	}

	auto SliceOIVPresenter::Impl::loadCamInfo(double& x, double& y, double& z, double& zoomFactor, const QString& path) -> bool {
		// Read JSON data from file
		QFile file(path);
		if (!file.open(QIODevice::ReadOnly)) {
			qWarning() << "Couldn't open file for reading.";
			return false;
		}

		QByteArray jsonData = file.readAll();
		file.close();

		// Parse JSON data into a QJsonDocument
		QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData);
		if (jsonDoc.isNull()) {
			qWarning() << "Failed to create JSON document from data.";
			return false;
		}

		// Extract values from the JSON document
		QJsonObject jsonObject = jsonDoc.object();
		x = jsonObject["x"].toDouble();
		y = jsonObject["y"].toDouble();
		z = jsonObject["z"].toDouble();
		zoomFactor = jsonObject["zoomFactor"].toDouble();

		return true;
	}


	auto SliceOIVPresenter::Impl::AdjustOffsetZ(double offset) -> double {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.0001;
		};
		if (AreSame(offset, 0)) {
			return 0;
		}
		if (ht3dExist) {
			return offset - static_cast<double>(curDimZ) * curResZ / 2.0;
		}
		return offset;
	}

	auto SliceOIVPresenter::Impl::GetPhysicalZ(int iz, double spz, double originZ, double offset) -> double {
		const auto phyZ = originZ + offset + iz * spz;
		return phyZ;
	}

	auto SliceOIVPresenter::Impl::SetLayerOrder(const QString& name, int order) -> void {
		auto nodeName = name;
		for (const QChar character : invalidLetterInOiv) {
			nodeName.replace(character, '_');
		}
		const auto rootName = nodeName + "_SliceSW";
		auto existingNode = MedicalHelper::find<SoSwitch>(root.ptr(), rootName.toStdString());
		if (existingNode) {
			const auto trans = MedicalHelper::find<SoTranslation>(existingNode);
			trans->translation.setValue(0, 0, -10000.0f * static_cast<float>(order));
		}
	}

	auto SliceOIVPresenter::Impl::BuildColorMap() -> void {
		auto dummyTF = new SoTransferFunction;
		dummyTF->ref();
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
			auto p = dummyTF->actualColorMap.startEditing();
			layer->AddColorMap("Intensity", p);
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY_REVERSED;
			auto p = dummyTF->actualColorMap.startEditing();
			layer->AddColorMap("Reversed intensity", p);
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::STANDARD;
			auto p = dummyTF->actualColorMap.startEditing();
			layer->AddColorMap("Standard", p);
			dummyTF->actualColorMap.finishEditing();
		}
		dummyTF->unref();
	}

	auto SliceOIVPresenter::Impl::CheckDimension(int dim) -> bool {
		if (dimension == -1) {
			dimension = dim;
		} else if (dimension != dim) {
			return false;
		}
		return true;
	}

	auto SliceOIVPresenter::Impl::AddHT2dLayer(QString name, std::shared_ptr<Data::HT2D> data) -> bool {
		//Add control Layer
		const auto meta = std::make_shared<Meta::SliceMeta>();
		meta->SetColormapID("Intensity");
		meta->SetDecimals(4);
		const auto [min, max] = data->GetRI();
		meta->SetRange(min * 10000.0, max * 10000.0);
		meta->SetDivider(10000);
		meta->SetSingleStep(0.0001);

		//convert data
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, 1), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -0.1, size.x * res.x / 2, size.y * res.y / 2, 0.1);
		//add SceneGraph

		const auto slice = std::make_shared<Image::SliceGeneral>(name);
		sliceList.append(slice);
		slice->SetDataMinMax(min * 10000.0, max * 10000.0);
		slice->SetVolume(volData.ptr());

		root->addChild(slice->GetRootSwitch());

		layer->AddLayer(name, ToIcon(data->GetFlags()), meta);

		const auto [tf, tfNum] = layer->GetTF(name);

		slice->SetColormap(tf, false, tfNum);

		return true;
	}

	auto SliceOIVPresenter::Impl::AddFL2dLayer(QString name, std::shared_ptr<Data::FL2D> data) -> bool {
		const auto meta = std::make_shared<Meta::SliceMeta>();
		meta->SetColormapID("Single Tone");
		meta->SetDecimals(0);
		const auto [min, max] = data->GetIntensity();
		meta->SetRange(min, max);
		meta->SetSingleStep(1);
		const auto chIdx = data->GetChannelIndex();
		if (chIdx == 0) {
			meta->SetSingleTone(QColor(0, 0, 255));
		} else if (chIdx == 1) {
			meta->SetSingleTone(QColor(0, 255, 0));
		} else {
			meta->SetSingleTone(QColor(255, 0, 0));
		}

		//convert data
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, 1), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -0.1, size.x * res.x / 2, size.y * res.y / 2, 0.1);

		//add SceneGraph
		const auto slice = std::make_shared<Image::SliceGeneral>(name);
		sliceList.append(slice);
		slice->SetDataMinMax(min, max);
		slice->SetVolume(volData.ptr());

		root->addChild(slice->GetRootSwitch());

		layer->AddLayer(name, ToIcon(data->GetFlags()), meta);

		const auto [tf, tfNum] = layer->GetTF(name);

		slice->SetColormap(tf, false, tfNum);

		return true;
	}

	auto SliceOIVPresenter::Impl::AddFloat2dLayer(QString name, std::shared_ptr<Data::Float2D> data) -> bool {
		const auto meta = std::make_shared<Meta::SliceMeta>();
		meta->SetColormapID("Intensity");
		meta->SetDecimals(2);
		const auto [min, max] = data->GetRange();
		meta->SetRange(min, max);
		meta->SetSingleStep(0.01);

		//convert data
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, 1), SbDataType::FLOAT, 32, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -0.1, size.x * res.x / 2, size.y * res.y / 2, 0.1);

		//add SceneGraph
		const auto slice = std::make_shared<Image::SliceGeneral>(name);
		sliceList.append(slice);
		slice->SetDataMinMax(min, max);
		slice->SetVolume(volData.ptr());

		root->addChild(slice->GetRootSwitch());

		layer->AddLayer(name, ToIcon(data->GetFlags()), meta);

		const auto [tf, tfNum] = layer->GetTF(name);

		slice->SetColormap(tf, false, tfNum);

		return true;
	}

	auto SliceOIVPresenter::Impl::AddBinary2dLayer(QString name, std::shared_ptr<Data::BinaryMask2D> data) -> bool {
		const auto meta = std::make_shared<Meta::SliceMeta>();
		meta->SetColormapID("Label Mask");
		meta->SetDecimals(0);
		meta->SetRange(0, 1);
		meta->SetSingleStep(1);

		//convert data
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, 1), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -0.1, size.x * res.x / 2, size.y * res.y / 2, 0.1);

		//add SceneGraph
		const auto slice = std::make_shared<Image::SliceGeneral>(name);
		sliceList.append(slice);
		slice->SetDataMinMax(0, 2);
		slice->SetVolume(volData.ptr());
		slice->SetIsMask(true);

		root->addChild(slice->GetRootSwitch());

		layer->AddLayer(name, ToIcon(data->GetFlags()), meta);

		const auto [tf, tfNum] = layer->GetTF(name);

		slice->SetColormap(tf, true, tfNum);

		return true;
	}

	auto SliceOIVPresenter::Impl::AddLabel2dLayer(QString name, std::shared_ptr<Data::LabelMask2D> data) -> bool {
		const auto meta = std::make_shared<Meta::SliceMeta>();
		meta->SetColormapID("Label Mask");
		meta->SetDecimals(0);
		const auto maxLabel = data->GetMaxIndex();
		meta->SetRange(0, maxLabel);
		meta->SetSingleStep(1);

		//convert data
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, 1), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -0.1, size.x * res.x / 2, size.y * res.y / 2, 0.1);

		//add SceneGraph
		const auto slice = std::make_shared<Image::SliceGeneral>(name);
		sliceList.append(slice);
		slice->SetDataMinMax(0, maxLabel + 1);
		slice->SetVolume(volData.ptr());
		slice->SetIsMask(true);

		root->addChild(slice->GetRootSwitch());

		layer->AddLayer(name, ToIcon(data->GetFlags()), meta);

		const auto [tf, tfNum] = layer->GetTF(name);

		slice->SetColormap(tf, true, tfNum);

		return true;
	}

	auto SliceOIVPresenter::Impl::AddHT3dLayer(QString name, std::shared_ptr<Data::HT3D> data) -> bool {
		//Add control Layer		
		const auto size = data->GetSize();
		const auto res = data->GetResolution();

		const auto meta = std::make_shared<Meta::SliceMeta>();
		meta->SetColormapID("Intensity");
		meta->SetDecimals(4);
		const auto [min, max] = data->GetRI();
		meta->SetRange(min * 10000.0, max * 10000.0);
		meta->SetDivider(10000);
		meta->SetSingleStep(0.0001);
		meta->SetMaxZ(size.z);
		meta->SetCurrentZ(size.z / 2);

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2);
		//add SceneGraph
		const auto slice = std::make_shared<Image::SliceGeneral>(name);
		sliceList.append(slice);
		slice->SetDataMinMax(min * 10000.0, max * 10000.0);
		slice->SetVolume(volData.ptr());
		slice->SetGeometryZ(size.z, res.z);
		slice->SetPhyZPos(currentPhyZ, true);

		if (false == ht3dExist) {
			curDimZ = size.z;
			curResZ = res.z;
			curOffsetZ = 0;
			ht3dExist = true;
			layer->UpdateZRange(size.z);
			if (false == ref3dExist) {
				currentZ = size.z / 2;
				layer->SetZ(currentZ);
			}
			ref3dExist = true;
		}

		root->addChild(slice->GetRootSwitch());

		layer->AddLayer(name, ToIcon(data->GetFlags()), meta);

		const auto [tf, tfNum] = layer->GetTF(name);

		slice->SetColormap(tf, false, tfNum);

		return true;
	}

	auto SliceOIVPresenter::Impl::AddFL3dLayer(QString name, std::shared_ptr<Data::FL3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto meta = std::make_shared<Meta::SliceMeta>();
		meta->SetColormapID("Single Tone");
		meta->SetDecimals(0);
		const auto [min, max] = data->GetIntensity();
		meta->SetRange(min, max);
		meta->SetSingleStep(1);
		meta->SetMaxZ(size.z);
		meta->SetCurrentZ(size.z / 2);
		const auto chIdx = data->GetChannelIndex();
		if (chIdx == 0) {
			meta->SetSingleTone(QColor(0, 0, 255));
		} else if (chIdx == 1) {
			meta->SetSingleTone(QColor(0, 255, 0));
		} else {
			meta->SetSingleTone(QColor(255, 0, 0));
		}

		//convert data		
		const auto offset = data->GetZOffset();
		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2);

		//add SceneGraph
		const auto slice = std::make_shared<Image::SliceGeneral>(name);
		sliceList.append(slice);
		slice->SetDataMinMax(min, max);
		slice->SetVolume(volData.ptr());
		//slice->SetSliceNumber(size.z / 2, true);
		slice->SetGeometryZ(size.z, res.z, AdjustOffsetZ(offset));
		slice->SetPhyZPos(currentPhyZ, true);

		if (false == ref3dExist) {
			curDimZ = size.z;
			curResZ = res.z;
			curOffsetZ = offset;
			ref3dExist = true;
			currentZ = size.z / 2;
			layer->UpdateZRange(size.z);
			layer->SetZ(currentZ);
		}

		root->addChild(slice->GetRootSwitch());

		layer->AddLayer(name, ToIcon(data->GetFlags()), meta);

		const auto [tf, tfNum] = layer->GetTF(name);

		slice->SetColormap(tf, false, tfNum);

		return true;
	}

	auto SliceOIVPresenter::Impl::AddFloat3dLayer(QString name, std::shared_ptr<Data::Float3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto meta = std::make_shared<Meta::SliceMeta>();
		meta->SetColormapID("Intensity");
		meta->SetDecimals(2);
		const auto [min, max] = data->GetRange();
		meta->SetRange(min, max);
		meta->SetSingleStep(0.01);
		meta->SetMaxZ(size.z);
		meta->SetCurrentZ(size.z / 2);

		//convert data		
		const auto offset = data->GetZOffset();
		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::FLOAT, 32, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2);

		//add SceneGraph
		const auto slice = std::make_shared<Image::SliceGeneral>(name);
		sliceList.append(slice);
		slice->SetDataMinMax(min, max);
		slice->SetVolume(volData.ptr());
		//slice->SetSliceNumber(size.z / 2, true);
		slice->SetGeometryZ(size.z, res.z, AdjustOffsetZ(offset));
		slice->SetPhyZPos(currentPhyZ, true);

		if (false == ref3dExist) {
			curDimZ = size.z;
			curResZ = res.z;
			curOffsetZ = offset;
			ref3dExist = true;
			currentZ = size.z / 2;
			layer->UpdateZRange(size.z);
			layer->SetZ(currentZ);
		}

		root->addChild(slice->GetRootSwitch());

		layer->AddLayer(name, ToIcon(data->GetFlags()), meta);

		const auto [tf, tfNum] = layer->GetTF(name);

		slice->SetColormap(tf, false, tfNum);

		return true;
	}

	auto SliceOIVPresenter::Impl::AddBinary3dLayer(QString name, std::shared_ptr<Data::BinaryMask3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto meta = std::make_shared<Meta::SliceMeta>();
		meta->SetColormapID("Label Mask");
		meta->SetDecimals(0);
		meta->SetRange(0, 1);
		meta->SetSingleStep(1);
		meta->SetMaxZ(size.z);
		meta->SetCurrentZ(size.z / 2);

		//convert data		
		const auto offset = data->GetZOffset();
		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2);

		//add SceneGraph
		const auto slice = std::make_shared<Image::SliceGeneral>(name);
		sliceList.append(slice);
		slice->SetDataMinMax(0, 2);
		slice->SetVolume(volData.ptr());
		slice->SetIsMask(true);
		//slice->SetSliceNumber(size.z / 2, true);
		slice->SetGeometryZ(size.z, res.z, AdjustOffsetZ(offset));
		slice->SetPhyZPos(currentPhyZ, true);

		if (false == ref3dExist) {
			curDimZ = size.z;
			curResZ = res.z;
			curOffsetZ = offset;
			ref3dExist = true;
			currentZ = size.z / 2;
			layer->UpdateZRange(size.z);
			layer->SetZ(currentZ);
		}

		root->addChild(slice->GetRootSwitch());

		layer->AddLayer(name, ToIcon(data->GetFlags()), meta);

		const auto [tf, tfNum] = layer->GetTF(name);

		slice->SetColormap(tf, true, tfNum);

		return true;
	}

	auto SliceOIVPresenter::Impl::AddLabel3dLayer(QString name, std::shared_ptr<Data::LabelMask3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto meta = std::make_shared<Meta::SliceMeta>();
		meta->SetColormapID("Label Mask");
		meta->SetDecimals(0);
		const auto maxLabel = data->GetMaxIndex();
		meta->SetRange(0, maxLabel);
		meta->SetSingleStep(1);
		meta->SetMaxZ(size.z);
		meta->SetCurrentZ(size.z / 2);

		//convert data		
		const auto offset = data->GetZOffset();
		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2);

		//add SceneGraph
		const auto slice = std::make_shared<Image::SliceGeneral>(name);
		sliceList.append(slice);
		slice->SetDataMinMax(0, maxLabel + 1);
		slice->SetVolume(volData.ptr());
		slice->SetIsMask(true);
		//slice->SetSliceNumber(size.z / 2, true);
		slice->SetGeometryZ(size.z, res.z, AdjustOffsetZ(offset));
		slice->SetPhyZPos(currentPhyZ, true);

		if (false == ref3dExist) {
			curDimZ = size.z;
			curResZ = res.z;
			curOffsetZ = offset;
			ref3dExist = true;
			currentZ = size.z / 2;
			layer->UpdateZRange(size.z);
			layer->SetZ(currentZ);
		}

		root->addChild(slice->GetRootSwitch());

		layer->AddLayer(name, ToIcon(data->GetFlags()), meta);

		const auto [tf, tfNum] = layer->GetTF(name);

		slice->SetColormap(tf, true, tfNum);

		return true;
	}

	SliceOIVPresenter::SliceOIVPresenter(QWidget* parent) : IPresenter(), QWidget(parent), d { new Impl } {
		d->ui.setupUi(this);
		setMinimumSize(500, 500);

		d->dataflag = DataFlag::Volume2D | DataFlag::Binary | DataFlag::FL | DataFlag::HT;

		d->window = std::make_shared<SliceOIVWindow>();
		d->layer = std::make_shared<SliceOIVLayer>();
		d->BuildColorMap();

		auto layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		d->ui.RenderSocket->setLayout(layout);
		layout->addWidget(d->window.get());


		d->root = new SoSeparator;
		d->window->setSceneGraph(d->root.ptr());

		d->ui.zoomSpin->setRange(0.01, 1000.0);
		d->ui.zoomSpin->setDecimals(2);
		d->ui.zoomSpin->setSingleStep(0.01);

		d->ui.xSpin->setRange(-5000, 5000);
		d->ui.ySpin->setRange(-5000, 5000);
		d->ui.zSpin->setRange(-5000, 5000);
		d->ui.xSpin->setDecimals(2);
		d->ui.ySpin->setDecimals(2);
		d->ui.zSpin->setDecimals(2);
		d->ui.xSpin->setSingleStep(0.01);
		d->ui.ySpin->setSingleStep(0.01);
		d->ui.zSpin->setSingleStep(0.01);

		connect(d->ui.zoomSpin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val) {
			d->window->setCameraZoom(static_cast<float>(val));
		});
		connect(d->ui.xSpin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val) {
			const auto x = static_cast<float>(val);
			const auto y = static_cast<float>(d->ui.ySpin->value());
			const auto z = static_cast<float>(d->ui.zSpin->value());
			d->window->getCamera()->position.setValue(x, y, z);
		});
		connect(d->ui.ySpin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val) {
			const auto x = static_cast<float>(d->ui.xSpin->value());
			const auto y = static_cast<float>(val);
			const auto z = static_cast<float>(d->ui.zSpin->value());
			d->window->getCamera()->position.setValue(x, y, z);
		});
		connect(d->ui.zSpin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val) {
			const auto x = static_cast<float>(d->ui.xSpin->value());
			const auto y = static_cast<float>(d->ui.ySpin->value());
			const auto z = static_cast<float>(val);
			d->window->getCamera()->position.setValue(x, y, z);
		});
		connect(d->ui.loadBtn, &QPushButton::clicked, this, [this] {
			double zoom, x, y, z;
			const auto prev = QSettings("Tomocube", "TomoAnalysis").value("presenter/camera2d", QApplication::applicationDirPath()).toString();
			const auto path = QFileDialog::getOpenFileName(nullptr, "Load 2d camera information", prev, "JSON files(*.json)");
			if (false == path.isEmpty()) {
				if (d->loadCamInfo(x, y, z, zoom, path)) {
					QSettings("Tomocube", "TomoAnalysis").setValue("presenter/camera2d", path);

					QSignalBlocker zoomBlock(d->ui.zoomSpin);
					QSignalBlocker xBlock(d->ui.xSpin);
					QSignalBlocker yBlock(d->ui.ySpin);
					QSignalBlocker zBlock(d->ui.zSpin);

					d->ui.zoomSpin->setValue(zoom);
					d->ui.xSpin->setValue(x);
					d->ui.ySpin->setValue(y);
					d->ui.zSpin->setValue(z);

					d->window->setCameraZoom(zoom);
					d->window->getCamera()->position.setValue(x, y, z);
				}
			}
		});
		connect(d->ui.saveBtn, &QPushButton::clicked, this, [this] {
			const auto zoom = d->ui.zoomSpin->value();
			const auto x = d->ui.xSpin->value();
			const auto y = d->ui.ySpin->value();
			const auto z = d->ui.zSpin->value();
			const auto prev = QSettings("Tomocube", "TomoAnalysis").value("presenter/camera2d", QApplication::applicationDirPath()).toString();
			const auto path = QFileDialog::getSaveFileName(nullptr, "Save current 2d camera information", prev, "JSON files (*.json)");
			if (false == path.isEmpty()) {
				if (d->saveCamInfo(x, y, z, zoom, path)) {
					QSettings("Tomocube", "TomoAnalysis").setValue("presenter/camera2d", path);
				}
			}
		});

		connect(d->window.get(), &SliceOIVWindow::sigZooming, this, [this] {
			QSignalBlocker zoomBlock(d->ui.zoomSpin);
			const auto zoomFactor = d->window->getCameraZoom();
			d->ui.zoomSpin->setValue(zoomFactor);
		});
		connect(d->window.get(), &SliceOIVWindow::sigPanning, this, [this] {
			QSignalBlocker xBlock(d->ui.xSpin);
			QSignalBlocker yBlock(d->ui.ySpin);
			QSignalBlocker zBlock(d->ui.zSpin);
			const auto pos = d->window->getCamera()->position.getValue();
			d->ui.xSpin->setValue(pos[0]);
			d->ui.ySpin->setValue(pos[1]);
			d->ui.zSpin->setValue(pos[2]);
		});

		connect(d->layer.get(), &SliceOIVLayer::sigUpsample, this, [this](int upsample) {
			d->upsample = upsample;
		});
		connect(d->layer.get(), &SliceOIVLayer::sigRemove, this, [this](QString ID) {
			OnRemoveLayer(ID);
		});
		connect(d->layer.get(), &SliceOIVLayer::sigZChanged, this, [this](int z) {
			OnZChanged(z);
		});
		connect(d->layer.get(), &SliceOIVLayer::sigLayerOrder, this, [this](QStringList order) {
			OnLayerOrder(order);
		});
		connect(d->layer.get(), &SliceOIVLayer::sigToggleViz, this, [this](QString name, bool show) {
			OnToggleViz(name, show);
		});
		connect(d->layer.get(), &SliceOIVLayer::sigUpdateColormap, this, [this](QString name, float* arr, int maxNum, bool isLabel) {
			OnColormap(name, arr, isLabel, maxNum);
		});
		connect(d->layer.get(), &SliceOIVLayer::sigUpdateRange, this, [this](QString name, double min, double max) {
			OnRange(name, min, max);
		});
		connect(d->layer.get(), &SliceOIVLayer::sigupdateTranp, this, [this](QString name, double transp) {
			OnTransp(name, transp);
		});
		connect(d->layer.get(), &SliceOIVLayer::sigCapture, this, [this](QString path) {
			CaptureScreen(path);
		});
		connect(d->window.get(), &SliceOIVWindow::sigMouseWheel, this, [this](int delta) {
			OnMouseWheel(delta);
		});
		connect(d->window.get(), &SliceOIVWindow::sigMouseClick, this, [this]() {
			OnMouseClick();
		});
	}

	SliceOIVPresenter::~SliceOIVPresenter() { }

	auto SliceOIVPresenter::OnMouseClick() -> void {
		setFocus();
	}

	auto SliceOIVPresenter::OnRemoveLayer(QString name) -> void {
		auto nodeName = name;
		for (const QChar character : d->invalidLetterInOiv) {
			nodeName.replace(character, '_');
		}
		const auto rootName = nodeName + "_SliceSW";
		const auto rootSW = MedicalHelper::find<SoSwitch>(d->root.ptr(), rootName.toStdString());
		if (rootSW) {
			d->root->removeChild(rootSW);
		}
	}

	auto SliceOIVPresenter::OnZChanged(int z) -> void {
		const auto phyZ = d->GetPhysicalZ(z - 1, d->curResZ, -d->curDimZ * d->curResZ / 2, d->curOffsetZ);
		d->currentZ = z - 1;
		d->currentPhyZ = phyZ;
		for (auto s : d->sliceList) {
			s->SetPhyZPos(phyZ, true);
		}
	}

	auto SliceOIVPresenter::OnMouseWheel(int delta) -> void {
		const auto newZ = d->currentZ + delta;
		if (newZ < 0 || newZ > d->curDimZ - 1) {
			return;
		}
		d->currentZ = newZ;
		const auto phyZ = d->GetPhysicalZ(d->currentZ, d->curResZ, -d->curDimZ * d->curResZ / 2, d->curOffsetZ);
		d->currentPhyZ = phyZ;
		for (auto s : d->sliceList) {
			s->SetPhyZPos(phyZ, true);
		}
		d->layer->SetZ(newZ + 1);
	}

	auto SliceOIVPresenter::OnLayerOrder(QStringList layerOrder) -> void {
		for (auto i = 0; i < layerOrder.count(); i++) {
			d->SetLayerOrder(layerOrder[i], layerOrder.count() - 1 - i);
		}
	}

	auto SliceOIVPresenter::OnTransp(QString name, double transp) -> void {
		for (auto s : d->sliceList) {
			if (s->GetName() == name) {
				s->SetSliceTransparency(static_cast<float>(transp));
				return;
			}
		}
	}

	auto SliceOIVPresenter::OnToggleViz(QString name, double show) -> void {
		for (auto s : d->sliceList) {
			if (s->GetName() == name) {
				s->ToggleViz(show);
				return;
			}
		}
	}

	auto SliceOIVPresenter::OnRange(QString name, double min, double max) -> void {
		for (auto s : d->sliceList) {
			if (s->GetName() == name) {
				s->SetDataRange(min, max);
				return;
			}
		}
	}

	auto SliceOIVPresenter::OnColormap(QString name, float* arr, bool isLabel, int maxNum) -> void {
		for (auto s : d->sliceList) {
			if (s->GetName() == name) {
				s->SetColormap(arr, isLabel, maxNum);
				return;
			}
		}
	}

	auto SliceOIVPresenter::SetTitle(const QString& title) -> void {
		setWindowTitle(title);
		d->layer->setWindowTitle(title + " Control");
	}

	auto SliceOIVPresenter::GetWindowList() const -> ViewList {
		return { d->layer };
	}

	auto SliceOIVPresenter::IsPrimary(const ViewPtr& window) const -> bool {
		return window == d->layer;
	}

	auto SliceOIVPresenter::IsAcceptable(const DataPtr& data) const -> bool {
		if (const auto cmp = data->GetFlags() & d->dataflag; cmp == d->dataflag) {
			return true;
		}
		return false;
	}

	auto SliceOIVPresenter::GetDataList() const -> QStringList {
		return d->dataMap.keys();
	}

	auto SliceOIVPresenter::GetData(const QString& name) const -> DataPtr {
		if (d->dataMap.contains(name)) {
			return d->dataMap[name];
		}
		return {};
	}

	auto SliceOIVPresenter::GetName(const DataPtr& data) const -> QString {
		for (const auto key : d->dataMap.keys()) {
			if (d->dataMap[key] == data) {
				return key;
			}
		}
		return {};
	}

	auto SliceOIVPresenter::AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool {
		Q_UNUSED(portName)
		auto is2D = false;
		if (std::dynamic_pointer_cast<IVolume2D>(data)) {
			if (false == d->CheckDimension(2)) {
				is2D = true;
				QMessageBox::StandardButton reply;
				reply = QMessageBox::question(this, "Confirmation", "Dimension Changed (3D -> 2D).\nDo you want to flush existing layers?", QMessageBox::Yes | QMessageBox::No);
				if (reply == QMessageBox::Yes) {
					ClearData();
					is2D = true;
					d->dimension = 2;
				} else {
					return false;
				}
			} else {
				is2D = true;
			}
		} else if (std::dynamic_pointer_cast<IVolume3D>(data)) {
			if (false == d->CheckDimension(3)) {
				QMessageBox::StandardButton reply;
				reply = QMessageBox::question(this, "Confirmation", "Dimension Changed (2D -> 3D).\nDo you want to flush existing layers?", QMessageBox::Yes | QMessageBox::No);
				if (reply == QMessageBox::Yes) {
					ClearData();
					d->dimension = 3;
				} else {
					return false;
				}
			}
		} else {
			return false;
		}

		if (d->dataMap.contains(name)) {
			//remove layer
			d->layer->RemoveLayer(name);
			//remove scenegraph
			for (auto slice : d->sliceList) {
				if (slice->GetName() == name) {
					auto scenegraph = slice->GetRootSwitch();
					d->root->removeChild(scenegraph);
					d->sliceList.removeOne(slice);
					break;
				}
			}
		}

		d->dataMap[name] = data;
		bool success;
		if (is2D) {
			success = Add2dData(name, data);
		} else {
			success = Add3dData(name, data);
		}
		if (success) {
			if (d->dataMap.count() == 1) {
				d->window->ResetView2D(Direction2D::XY);
				QSignalBlocker zoomBlock(d->ui.zoomSpin);
				QSignalBlocker xBlock(d->ui.xSpin);
				QSignalBlocker yBlock(d->ui.ySpin);
				QSignalBlocker zBlock(d->ui.zSpin);
				const auto zoomfactor = d->window->getCameraZoom();
				d->ui.zoomSpin->setValue(zoomfactor);

				const auto pos = d->window->getCamera()->position.getValue();
				d->ui.xSpin->setValue(pos[0]);
				d->ui.ySpin->setValue(pos[1]);
				d->ui.zSpin->setValue(pos[2]);
			}
			for (auto i = 1; i < d->sliceList.count() - 1; i++) {
				const auto slice = d->sliceList[i];
				slice->ToggleViz(false);
				d->layer->SetToggleViz(slice->GetName(), false);
			}
		}
		return false;
	}

	auto SliceOIVPresenter::Add2dData(const QString& name, const DataPtr& data) -> bool {
		if (const auto ht2d = std::dynamic_pointer_cast<Data::HT2D>(data)) {
			return d->AddHT2dLayer(name, ht2d);
		}
		if (const auto fl2d = std::dynamic_pointer_cast<Data::FL2D>(data)) {
			return d->AddFL2dLayer(name, fl2d);
		}
		if (const auto float2d = std::dynamic_pointer_cast<Data::Float2D>(data)) {
			return d->AddFloat2dLayer(name, float2d);
		}
		if (const auto binary2d = std::dynamic_pointer_cast<Data::BinaryMask2D>(data)) {
			return d->AddBinary2dLayer(name, binary2d);
		}
		if (const auto label2d = std::dynamic_pointer_cast<Data::LabelMask2D>(data)) {
			return d->AddLabel2dLayer(name, label2d);
		}
		return false;
	}

	auto SliceOIVPresenter::Add3dData(const QString& name, const DataPtr& data) -> bool {
		if (const auto ht3d = std::dynamic_pointer_cast<Data::HT3D>(data)) {
			return d->AddHT3dLayer(name, ht3d);
		}
		if (const auto fl3d = std::dynamic_pointer_cast<Data::FL3D>(data)) {
			return d->AddFL3dLayer(name, fl3d);
		}
		if (const auto float3d = std::dynamic_pointer_cast<Data::Float3D>(data)) {
			return d->AddFloat3dLayer(name, float3d);
		}
		if (const auto binary3d = std::dynamic_pointer_cast<Data::BinaryMask3D>(data)) {
			return d->AddBinary3dLayer(name, binary3d);
		}
		if (const auto label3d = std::dynamic_pointer_cast<Data::LabelMask3D>(data)) {
			return d->AddLabel3dLayer(name, label3d);
		}
		return false;
	}

	auto SliceOIVPresenter::RenameData(const DataPtr& data, const QString& name) -> bool {
		const auto prevName = GetName(data);
		if (prevName.isEmpty()) {
			return false;
		}

		d->dataMap.remove(prevName);
		d->dataMap[name] = data;
		//Rename scenegraph
		for (auto slice : d->sliceList) {
			if (slice->GetName() == prevName) {
				slice->ChangeName(name);
				break;
			}
		}
		//Rename Layer
		d->layer->ChangeName(prevName, name);

		return false;
	}

	auto SliceOIVPresenter::RemoveData(const QString& name) -> void {
		if (false == d->dataMap.contains(name)) {
			return;
		}
		//remove scenegraph
		for (auto slice : d->sliceList) {
			if (slice->GetName() == name) {
				auto scenegraph = slice->GetRootSwitch();
				d->root->removeChild(scenegraph);
				d->sliceList.removeOne(slice);
				break;
			}
		}
		//remove layer
		d->layer->RemoveLayer(name);

		//removeData;
		d->dataMap.remove(name);

		if (d->sliceList.count() == 0) {
			d->currentPhyZ = 0;
			d->currentZ = 0;
			d->curDimZ = 1;
			d->curResZ = 1;
			d->curOffsetZ = 0;
			d->ref3dExist = false;
			d->ht3dExist = false;
		}
	}

	auto SliceOIVPresenter::Capture(const QString& filepath) -> void {
		CaptureScreen(filepath, true);
	}

	auto SliceOIVPresenter::CaptureScreen(const QString& path, bool fullResolution) -> void {
		//set fixed resolution
		const auto volDataList = MedicalHelper::findNodes<SoVolumeData>(d->root.ptr());
		if (fullResolution) {
			for (const auto volData : volDataList) {
				volData->ldmResourceParameters.getValue()->resolution = 0;
				volData->ldmResourceParameters.getValue()->fixedResolution = TRUE;
			}
		}
		d->window->saveSnapshot(path, d->upsample);
		//restore resolution
		if (fullResolution) {
			for (const auto volData : volDataList) {
				volData->ldmResourceParameters.getValue()->fixedResolution = FALSE;
			}
		}
	}

	auto SliceOIVPresenter::ClearData() -> void {
		d->dataMap.clear();
		d->root->removeAllChildren();
		d->sliceList.clear();
		d->layer->Clear();
		d->currentPhyZ = 0;
		d->currentZ = 0;
		d->curDimZ = 1;
		d->curResZ = 1;
		d->curOffsetZ = 0;
		d->ref3dExist = false;
		d->ht3dExist = false;
	}
}
