#pragma once

#include <QWidget>

#include <IPresenter.h>
#include "CellAnalyzer.Presenter.OpenInventorExport.h"

namespace CellAnalyzer::Presenter {
    class CellAnalyzer_Presenter_OpenInventor_API OIVPresenter : public IPresenter, public QWidget {
        public:
            OIVPresenter();
            ~OIVPresenter() override;
        };
    }