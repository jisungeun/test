#pragma once

#include "IAppModule.h"

#include "CellAnalyzer.Presenter.OpenInventorExport.h"

namespace CellAnalyzer::Presenter {
    class CellAnalyzer_Presenter_OpenInventor_API OIVModule final : public IAppModule{
    public:
        auto Start()->std::optional<Error> override;
        auto Stop() -> void override;
    };
}