#include <Inventor/Qt/SoQt.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#include <OivActivator.h>

#include "OIVPresenter.h"

namespace CellAnalyzer::Presenter {
	OIVPresenter::OIVPresenter() {
		if (false == SoQt::isInitialized()) {
			QStringList OivKeys;
			OivKeys.append("License OpenInventor 10.12 1-Jan-0 0 HY:PKW=I>@C` \"APP-TOMOANALYSIS\"");
			OivKeys.append("License VolumeVizLDM 10.12 1-Jan-0 0 JDJ]=[<:?LB[ \"APP-TOMOANALYSIS\"");
			OivKeys.append("License ImageViz 10.12 1-Jan-0 0 BCVFNLNZOTPQ \"APP-TOMOANALYSIS\"");
			OivActivator::Activate(OivKeys);

			SoQt::init(this);
		}
		if (false == SoVolumeRendering::isInitialized()) {
			SoVolumeRendering::init();
		}		
	}

	OIVPresenter::~OIVPresenter() = default;
}
