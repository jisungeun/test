#include <Inventor/Qt/SoQt.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>

#include "OIVModule.h"

namespace CellAnalyzer::Presenter {
	auto OIVModule::Start() -> std::optional<Error> {
		return std::nullopt;
	}

	auto OIVModule::Stop() -> void {
		/*
		if (SoVolumeRendering::isInitialized()) {
			SoVolumeRendering::finish();
		}
		if (SoQt::isInitialized()) {
			SoQt::finish();			
		}
		*/
	}

}
