#include "MIP3dTF1dPresenter.h"

#include <QVBoxLayout>

#include <HT3D.h>
#include <FL3D.h>
#include <Float3d.h>
#include <BinaryMask3D.h>
#include <LabelMask3D.h>

#include <RenderWindow3dWidget.h>
#include <VolumeMIPTF1D.h>
#include <MIP3dTF1dControl.h>

#include <VolumeViz/nodes/SoDataRange.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoScale.h>
#include <Medical/helpers/MedicalHelper.h>

#include "ui_MIP3dTF1dPresenter.h"

namespace CellAnalyzer::Presenter::Volume {
	using namespace Tomocube::Rendering;

	struct MIP3dTF1dPresenter::Impl {
		Ui::TF1d3d ui;

		std::shared_ptr<RenderWindow> window { nullptr };
		std::shared_ptr<Image::VolumeMIPTF1D> renderer { nullptr };
		std::shared_ptr<MIP3dTF1dControl> control { nullptr };
		QString title;
		int upsample { 1 };

		QString imageName;
		DataPtr imageData { nullptr };
		int histogram_steps { 0 };
		int maxSignal { 1 };
		int minSignal { 0 };

		auto AddHT3D(std::shared_ptr<Data::HT3D> data) -> bool;
		auto AddFL3D(std::shared_ptr<Data::FL3D> data) -> bool;
		auto AddFloat3D(std::shared_ptr<Data::Float3D> data) -> bool;

		auto CalculateHistogram(QList<int> data, double div) -> QList<int>;
		auto GetCenterSlice(const unsigned short* buffer, int dimX, int dimY, int dimZ) -> QList<int>;
	};

	auto MIP3dTF1dPresenter::Impl::GetCenterSlice(const unsigned short* buffer, int dimX, int dimY, int dimZ) -> QList<int> {
		QList<int> result;
		for (auto k = 0; k < dimZ; k++) {
			for (auto i = 0; i < dimX; i++) {
				for (auto j = 0; j < dimY; j++) {
					const auto idx = k * dimX * dimY + i * dimY + j;
					const auto val = static_cast<int>(*(buffer + idx));
					result.append(val);
				}
			}
		}
		return result;
	}

	auto MIP3dTF1dPresenter::Impl::CalculateHistogram(QList<int> data, double div) -> QList<int> {
		auto result = QList<int>();
		for (auto i = 0; i < histogram_steps / div; i++) {
			result.append(0);
		}

		for (auto i = 0; i < data.count(); i++) {
			auto value = (data[i] - minSignal) / div;
			result[value]++;
		}

		for (auto i = 0; i < histogram_steps / div; i++) {
			if (result[i] > 0) {
				result[i] = static_cast<int>(log(result[i] + 1.5) / log(1.5));
			}
		}

		return result;
	}


	auto MIP3dTF1dPresenter::Impl::AddHT3D(std::shared_ptr<Data::HT3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2);
		//add SceneGraph
		const auto [min, max] = data->GetRI();

		renderer->SetDataMinMax(min * 10000.0, max * 10000.0);
		renderer->SetVolume(volData.ptr());

		control->SetDataRangeConfig(4, 10000, 0, 0.0001);
		control->SetDataMinMax(min * 10000.0, max * 10000.0);
		control->SetImageType(DataFlag::HT);

		const auto [riMin, riMax] = data->GetRI();
		minSignal = static_cast<int>(riMin * 10000.0);
		maxSignal = static_cast<int>(riMax * 10000.0);
		histogram_steps = maxSignal - minSignal + 1;
		const auto centerBuffer = GetCenterSlice(static_cast<const unsigned short*>(data->GetData()), size.x, size.y, size.z);
		const auto histo = CalculateHistogram(centerBuffer, 1);

		control->UpdateHistogram(histo);

		return true;
	}

	auto MIP3dTF1dPresenter::Impl::AddFL3D(std::shared_ptr<Data::FL3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();
		const auto channel = data->GetChannelIndex();

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + offset, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + offset);

		const auto [min, max] = data->GetIntensity();

		renderer->SetDataMinMax(min, max);
		renderer->SetVolume(volData.ptr());

		control->SetDataRangeConfig(0, 1, 0, 1);
		control->SetDataMinMax(min, max);

		control->SetChannelIndex(channel);
		control->SetImageType(DataFlag::FL);
		return true;
	}

	auto MIP3dTF1dPresenter::Impl::AddFloat3D(std::shared_ptr<Data::Float3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::FLOAT, 32, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + offset, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + offset);

		double min, max;
		volData->getMinMax(min, max);

		renderer->SetDataMinMax(min, max);
		renderer->SetVolume(volData.ptr());

		control->SetDataRangeConfig(2, 1, 0, 0.01);
		control->SetDataMinMax(min, max);
		control->SetImageType(DataFlag::Null);
		return true;
	}

	MIP3dTF1dPresenter::MIP3dTF1dPresenter(QWidget* parent) : IPresenter(), QWidget(parent), d { new Impl } {
		d->ui.setupUi(this);
		d->window = std::make_shared<RenderWindow>(nullptr);
		d->window->SetGradientBackground(0,0,0,0,0,0);
		const auto layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		d->ui.RenderSocket->setLayout(layout);
		layout->addWidget(d->window.get());

		d->renderer = std::make_shared<Image::VolumeMIPTF1D>("MIPTF1D");
		d->renderer->ToggleDepthEnhanced(false);
		d->renderer->SetDepthEnhancementFactor(1);
		SoRef<SoSeparator> swapRoot = new SoSeparator;
		SoRef<SoScale> swap = new SoScale;
		swap->scaleFactor.setValue(1, -1, 1);
		swapRoot->addChild(swap.ptr());
		swapRoot->addChild(d->renderer->GetRootSwitch());
		d->window->SetSceneGraph(swapRoot.ptr());
		
		d->control = std::make_shared<MIP3dTF1dControl>();

		InitConnections();

		OnColormap(d->control->GetCurColormap());
	}

	MIP3dTF1dPresenter::~MIP3dTF1dPresenter() = default;

	auto MIP3dTF1dPresenter::InitConnections() -> void {
		connect(d->control.get(), &MIP3dTF1dControl::sigUpsample, this, [this](int upsample) {
			d->upsample = upsample;
		});
		connect(d->control.get(), &MIP3dTF1dControl::sigCapture, this, [this](QString path) {
			CaptureScreen(path);
		});
		connect(d->control.get(), &MIP3dTF1dControl::sigDataRange, this, [this](double min, double max) {
			OnDataRange(min, max);
		});
		connect(d->control.get(), &MIP3dTF1dControl::sigColormap, this, [this](QList<float> colormap) {
			OnColormap(colormap);
		});
		connect(d->control.get(), &MIP3dTF1dControl::sigCropRange, this, [this](double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
			OnCropRange(xmin, xmax, ymin, ymax, zmin, zmax);
		});
		connect(d->control.get(), &MIP3dTF1dControl::sigJittering, this, [this](bool use) {
			d->renderer->ToggleJittering(use);
			});
		connect(d->control.get(), &MIP3dTF1dControl::sigDefer, this, [this](bool use) {
			d->renderer->ToggleDeferredLighting(use);
			});
		connect(d->control.get(), &MIP3dTF1dControl::sigDepthFactor, this, [this](double factor) {
			OnDepthFactor(factor);
			});
		connect(d->control.get(), &MIP3dTF1dControl::sigGradientFactor, this, [this](double factor) {
			OnGradientFactor(factor);
			});
		connect(d->control.get(), &MIP3dTF1dControl::sigTFColor, this, [this](bool useTFColor) {
			OnTFColor(useTFColor);
			});
		connect(d->control.get(), &MIP3dTF1dControl::sigTFOpacity, this, [this](bool useTFOpacity) {
			OnTFOpacity(useTFOpacity);
			});
		connect(d->window.get(), &RenderWindow::sendMouseClick, this, [this]() {
			OnMouseClick();
		});
	}

	void MIP3dTF1dPresenter::OnDepthFactor(double factor) {
		if (factor > 0) {
			d->renderer->ToggleDepthEnhanced(true);
		}
		else {
			d->renderer->ToggleDepthEnhanced(false);
		}
		d->renderer->SetDepthEnhancementFactor(factor);
	}

	void MIP3dTF1dPresenter::OnGradientFactor(double factor) {
		if (factor > 0) {
			d->renderer->ToggleGradientEnhanced(true);
		}
		else {
			d->renderer->ToggleGradientEnhanced(false);
		}
		d->renderer->SetGradientEnhancementFactor(factor);
	}

	void MIP3dTF1dPresenter::OnTFOpacity(bool useTFOpacity) {
		d->renderer->ToggleOpacityTF(useTFOpacity);
	}

	void MIP3dTF1dPresenter::OnTFColor(bool useTFColor) {
		d->renderer->ToggleColorTF(useTFColor);
	}

	void MIP3dTF1dPresenter::OnMouseClick() {
		setFocus();
	}

	void MIP3dTF1dPresenter::OnCropRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
		d->renderer->SetXRange(xmin, xmax);
		d->renderer->SetYRange(ymin, ymax);
		d->renderer->SetZRange(zmin, zmax);
	}

	void MIP3dTF1dPresenter::OnColormap(QList<float> colormapArr) {		
		QList<QColor> colormap;
		for (auto i = 0; i < 256; i++) {
			auto colorItem = QColor(colormapArr[i * 4] * 255, colormapArr[i * 4 + 1] * 255, colormapArr[i * 4 + 2] * 255, colormapArr[i * 4 + 3] *255);
			colormap.append(colorItem);
		}
		d->renderer->SetColormap(colormap);
	}

	void MIP3dTF1dPresenter::OnDataRange(double min, double max) {
		d->renderer->SetDataRange(min, max);
	}

	auto MIP3dTF1dPresenter::SetTitle(const QString& title) -> void {
		setWindowTitle(title);
		d->control->setWindowTitle(title + " Control");
	}

	auto MIP3dTF1dPresenter::GetWindowList() const -> ViewList {
		return { d->control };
	}

	auto MIP3dTF1dPresenter::IsPrimary(const ViewPtr& window) const -> bool {
		return true;
	}

	auto MIP3dTF1dPresenter::IsAcceptable(const DataPtr& data) const -> bool {
		return true;
	}

	auto MIP3dTF1dPresenter::GetDataList() const -> QStringList {
		QStringList datalist;
		if (false == d->imageName.isEmpty()) {
			datalist.append(d->imageName);
		}
		return datalist;
	}

	auto MIP3dTF1dPresenter::GetData(const QString& name) const -> DataPtr {
		if (d->imageName == name) {
			return d->imageData;
		}
		return {};
	}

	auto MIP3dTF1dPresenter::GetName(const DataPtr& data) const -> QString {
		if (d->imageData == data) {
			return d->imageName;
		}
		return {};
	}

	auto MIP3dTF1dPresenter::RemoveData(const QString& name) -> void {
		//DO NOT USE
	}

	auto MIP3dTF1dPresenter::RenameData(const DataPtr& data, const QString& name) -> bool {
		const auto prevName = GetName(data);
		if (prevName.isEmpty()) {
			return false;
		}
		if (d->imageName == prevName) {
			d->imageName = name;
		}

		return true;
	}

	auto MIP3dTF1dPresenter::ClearData() -> void { }

	auto MIP3dTF1dPresenter::Capture(const QString& filepath) -> void { }

	auto MIP3dTF1dPresenter::CaptureScreen(const QString& path, bool fullResolution) -> void {
		//set fixed resolution
		const auto volDataList = MedicalHelper::findNodes<SoVolumeData>(d->renderer->GetRootSwitch());
		if (fullResolution) {
			for (const auto volData : volDataList) {
				volData->ldmResourceParameters.getValue()->resolution = 0;
				volData->ldmResourceParameters.getValue()->fixedResolution = TRUE;
			}
		}
		d->window->SaveSnapShot(path, d->upsample);
		//restore resolution
		if (fullResolution) {
			for (const auto volData : volDataList) {
				volData->ldmResourceParameters.getValue()->fixedResolution = FALSE;
			}
		}
	}

	auto MIP3dTF1dPresenter::AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool {
		Q_UNUSED(portName)
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(data)) {
			if (false == d->AddHT3D(image)) {
				return false;
			}
			d->imageData = data;
			d->imageName = name;
		} else if (const auto fl = std::dynamic_pointer_cast<Data::FL3D>(data)) {
			if (false == d->AddFL3D(fl)) {
				return false;
			}
			d->imageData = data;
			d->imageName = name;
		} else if (const auto floatImg = std::dynamic_pointer_cast<Data::Float3D>(data)) {
			if (false == d->AddFloat3D(floatImg)) {
				return false;
			}
			d->imageData = data;
			d->imageName = name;
		}
		d->window->ResetView();
		return true;
	}
}
