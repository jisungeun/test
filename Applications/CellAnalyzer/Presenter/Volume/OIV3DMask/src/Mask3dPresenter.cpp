#include "Mask3dPresenter.h"

#include <QVBoxLayout>

#include <HT3D.h>
#include <FL3D.h>
#include <Float3d.h>
#include <BinaryMask3D.h>
#include <LabelMask3D.h>

#include <RenderWindow3dWidget.h>
#include <MaskVolume.h>
#include "Mask3dControl.h"

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoScale.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Medical/helpers/MedicalHelper.h>

#include "ui_Mask3dPresenter.h"

namespace CellAnalyzer::Presenter::Volume {
	using namespace Tomocube::Rendering;

	struct Mask3dPresenter::Impl {
		Ui::Mask3dOIV ui;

		std::shared_ptr<RenderWindow> window { nullptr };
		std::shared_ptr<Mask::MaskVolume> renderer { nullptr };
		std::shared_ptr<Mask3dControl> control { nullptr };
		QString title;
		float color_table[12][3];
		int upsample { 1 };

		QString maskName;
		DataPtr maskData { nullptr };

		auto BuildLabelColorTable() -> void;

		auto AddLabel3D(std::shared_ptr<Data::LabelMask3D> data) -> bool;
		auto AddMask3D(std::shared_ptr<Data::BinaryMask3D> data) -> bool;
	};

	auto Mask3dPresenter::Impl::AddMask3D(std::shared_ptr<Data::BinaryMask3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();
		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + offset, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + offset);


		const auto maxidx = 1;
		renderer->SetVolume(volData.ptr());
		for (auto i = 0; i < maxidx; i++) {
			renderer->SetMaskColor(i + 1, QColor(color_table[i % 12][0], color_table[i % 12][1], color_table[i % 12][2]));
		}
		control->SetMaxlabelIndex(maxidx);
		return true;
	}

	auto Mask3dPresenter::Impl::AddLabel3D(std::shared_ptr<Data::LabelMask3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();
		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + offset, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + offset);


		const auto maxidx = data->GetMaxIndex();
		renderer->SetVolume(volData.ptr());
		for (auto i = 0; i < maxidx; i++) {
			renderer->SetMaskColor(i + 1, QColor(color_table[i % 12][0], color_table[i % 12][1], color_table[i % 12][2]));
		}
		control->SetMaxlabelIndex(maxidx);
		return true;
	}

	auto Mask3dPresenter::Impl::BuildLabelColorTable() -> void {
		color_table[0][0] = 255;
		color_table[0][1] = 59;
		color_table[0][2] = 48; //red
		color_table[1][0] = 255;
		color_table[1][1] = 149;
		color_table[1][2] = 0; //Orange
		color_table[2][0] = 255;
		color_table[2][1] = 204;
		color_table[2][2] = 0; //yellow
		color_table[3][0] = 52;
		color_table[3][1] = 199;
		color_table[3][2] = 89; //Green
		color_table[4][0] = 0;
		color_table[4][1] = 199;
		color_table[4][2] = 190; //Mint
		color_table[5][0] = 48;
		color_table[5][1] = 176;
		color_table[5][2] = 199; //Teal
		color_table[6][0] = 50;
		color_table[6][1] = 173;
		color_table[6][2] = 230; //Cyan
		color_table[7][0] = 0;
		color_table[7][1] = 122;
		color_table[7][2] = 255; //Blue
		color_table[8][0] = 88;
		color_table[8][1] = 86;
		color_table[8][2] = 214; //Indigo
		color_table[9][0] = 175;
		color_table[9][1] = 82;
		color_table[9][2] = 222; //Purple
		color_table[10][0] = 255;
		color_table[10][1] = 45;
		color_table[10][2] = 85; //Pink
		color_table[11][0] = 162;
		color_table[11][1] = 132;
		color_table[11][2] = 94; //Brown
	}

	Mask3dPresenter::Mask3dPresenter(QWidget* parent) : IPresenter(), QWidget(parent), d { new Impl } {
		d->ui.setupUi(this);
		d->BuildLabelColorTable();
		d->window = std::make_shared<RenderWindow>(nullptr);
		d->window->SetGradientBackground(0,0,0,0,0,0);
		const auto layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		d->ui.RenderSocket->setLayout(layout);
		layout->addWidget(d->window.get());

		d->renderer = std::make_shared<Mask::MaskVolume>("MIPMask");
		SoRef<SoSeparator> swapRoot = new SoSeparator;
		SoRef<SoScale> swap = new SoScale;
		swap->scaleFactor.setValue(1, -1, 1);
		swapRoot->addChild(swap.ptr());
		swapRoot->addChild(d->renderer->GetRootSwitch());
		d->window->SetSceneGraph(swapRoot.ptr());

		d->control = std::make_shared<Mask3dControl>();

		InitConnections();
	}

	Mask3dPresenter::~Mask3dPresenter() = default;

	auto Mask3dPresenter::InitConnections() -> void {
		connect(d->control.get(), &Mask3dControl::sigUpsample, this, [this](int upsample) {
			d->upsample = upsample;
		});
		connect(d->control.get(), &Mask3dControl::sigCapture, this, [this](QString path) {
			CaptureScreen(path);
		});
		connect(d->control.get(), &Mask3dControl::sigMaskHighlightIdx, this, [this](int idx) {
			OnMaskHighlight(idx);
		});
		connect(d->control.get(), &Mask3dControl::sigHighlightFactor, this, [this](double factor) {
			OnMaskHighlightFactor(factor);
		});
		connect(d->control.get(), &Mask3dControl::sigCropRange, this, [this](double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
			OnCropRange(xmin, xmax, ymin, ymax, zmin, zmax);
		});
		connect(d->control.get(), &Mask3dControl::sigJittering, this, [this](bool useJitter) {
			d->renderer->ToggleJittering(useJitter);
			});
		connect(d->control.get(), &Mask3dControl::sigDefer, this, [this](bool use) {
			d->renderer->ToggleDeferredLighing(use);
			});
		connect(d->window.get(), &RenderWindow::sendMouseClick, this, [this]() {
			OnMouseClick();
		});
	}

	void Mask3dPresenter::OnMouseClick() {
		setFocus();
	}

	auto Mask3dPresenter::CaptureScreen(const QString& path, bool fullResolution) -> void {
		//set fixed resolution
		const auto volDataList = MedicalHelper::findNodes<SoVolumeData>(d->renderer->GetRootSwitch());
		if (fullResolution) {
			for (const auto volData : volDataList) {
				volData->ldmResourceParameters.getValue()->resolution = 0;
				volData->ldmResourceParameters.getValue()->fixedResolution = TRUE;
			}
		}
		d->window->SaveSnapShot(path, d->upsample);
		//restore resolution
		if (fullResolution) {
			for (const auto volData : volDataList) {
				volData->ldmResourceParameters.getValue()->fixedResolution = FALSE;
			}
		}
	}

	void Mask3dPresenter::OnCropRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
		d->renderer->SetXRange(xmin, xmax);
		d->renderer->SetYRange(ymin, ymax);
		d->renderer->SetZRange(zmin, zmax);
	}

	void Mask3dPresenter::OnMaskHighlightFactor(double factor) {
		d->renderer->SetHighlightFactor(factor);
	}

	void Mask3dPresenter::OnMaskHighlight(int idx) {
		if (idx > 0) {
			d->renderer->SetHighlight(true, idx);
		} else {
			d->renderer->SetHighlight(false, -1);
		}
	}

	auto Mask3dPresenter::SetTitle(const QString& title) -> void {
		setWindowTitle(title);
		d->control->setWindowTitle(title + " Control");
	}

	auto Mask3dPresenter::GetWindowList() const -> ViewList {
		return { d->control };
	}

	auto Mask3dPresenter::IsPrimary(const ViewPtr& window) const -> bool {
		return true;
	}

	auto Mask3dPresenter::IsAcceptable(const DataPtr& data) const -> bool {
		return true;
	}

	auto Mask3dPresenter::GetDataList() const -> QStringList {
		QStringList datalist;
		if (false == d->maskName.isEmpty()) {
			datalist.append(d->maskName);
		}
		return datalist;
	}

	auto Mask3dPresenter::GetData(const QString& name) const -> DataPtr {
		if (d->maskName == name) {
			return d->maskData;
		}
		return {};
	}

	auto Mask3dPresenter::GetName(const DataPtr& data) const -> QString {
		if (d->maskData == data) {
			return d->maskName;
		}
		return {};
	}

	auto Mask3dPresenter::RemoveData(const QString& name) -> void {
		//DO NOT USE
	}

	auto Mask3dPresenter::RenameData(const DataPtr& data, const QString& name) -> bool {
		const auto prevName = GetName(data);
		if (prevName.isEmpty()) {
			return false;
		}
		if (d->maskName == prevName) {
			d->maskName = name;
		}

		return true;
	}

	auto Mask3dPresenter::ClearData() -> void { }

	auto Mask3dPresenter::Capture(const QString& filepath) -> void { }

	auto Mask3dPresenter::AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool {
		Q_UNUSED(portName)
		if (const auto binary = std::dynamic_pointer_cast<Data::BinaryMask3D>(data)) {
			if (false == d->AddMask3D(binary)) {
				return false;
			}
			d->maskData = data;
			d->maskName = name;
		} else if (const auto mask = std::dynamic_pointer_cast<Data::LabelMask3D>(data)) {
			if (false == d->AddLabel3D(mask)) {
				return false;
			}
			d->maskData = data;
			d->maskName = name;
		}
		d->window->ResetView();
		return true;
	}
}
