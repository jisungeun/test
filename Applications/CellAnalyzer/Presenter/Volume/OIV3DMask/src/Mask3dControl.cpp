#include <QSettings>
#include <QFileDialog>

#include <iostream>

#include "ui_Mask3dControl.h"
#include "Mask3dControl.h"

#include <QColorDialog>
#include <VolumeViz/nodes/SoTransferFunction.h>

#include "QColormapCanvas.h"
#include "TcColorTF.h"

namespace CellAnalyzer::Presenter::Volume {
	struct Mask3dControl::Impl {
		Ui::LayerControl ui;

		auto InitUI() -> void;
	};

	auto Mask3dControl::Impl::InitUI() -> void {
		ui.xCropMin->setRange(0, 0.99);
		ui.xCropMin->setDecimals(2);
		ui.xCropMin->setSingleStep(0.01);
		ui.xCropMin->setValue(0);
		ui.xCropMax->setRange(0.01, 1);
		ui.xCropMax->setDecimals(2);
		ui.xCropMax->setSingleStep(0.01);
		ui.xCropMax->setValue(1);

		ui.yCropMin->setRange(0, 0.99);
		ui.yCropMin->setDecimals(2);
		ui.yCropMin->setSingleStep(0.01);
		ui.yCropMin->setValue(0);
		ui.yCropMax->setRange(0.01, 1);
		ui.yCropMax->setDecimals(2);
		ui.yCropMax->setSingleStep(0.01);
		ui.yCropMax->setValue(1);

		ui.zCropMin->setRange(0, 0.99);
		ui.zCropMin->setDecimals(2);
		ui.zCropMin->setSingleStep(0.01);
		ui.zCropMin->setValue(0);
		ui.zCropMax->setRange(0.01, 1);
		ui.zCropMax->setDecimals(2);
		ui.zCropMax->setSingleStep(0.01);
		ui.zCropMax->setValue(1);

		ui.cropGroup->setEnabled(false);

		ui.captureBtn->setIcon(QIcon(":/Widget/CaptureCamera.svg"));
		ui.captureBtn->setIconSize(QSize(25, 25));
		ui.captureBtn->setToolTip("Take screenshot");

		ui.maskModeCombo->addItem("Exclusive");
		ui.maskModeCombo->addItem("Use Transparency");

		ui.maskIdxSpin->setEnabled(false);

		ui.maskFactorSpin->setRange(0, 1);
		ui.maskFactorSpin->setValue(1);
		ui.maskFactorSpin->setDecimals(2);
		ui.maskFactorSpin->setSingleStep(0.01);

		ui.upsampleSpin->setRange(1, 5);
		ui.upsampleSpin->setSingleStep(1);
		ui.upsampleSpin->setValue(1);
	}

	Mask3dControl::Mask3dControl(QWidget* parent) : QWidget(parent), d { new Impl } {
		d->ui.setupUi(this);
		d->InitUI();
		InitConnections();
	}

	Mask3dControl::~Mask3dControl() { }

	auto Mask3dControl::InitConnections() -> void {
		//General Connection
		connect(d->ui.captureBtn, SIGNAL(clicked()), this, SLOT(OnCapture()));
		connect(d->ui.maskModeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnMaskModeCombo(int)));
		connect(d->ui.maskFactorSpin, SIGNAL(valueChanged(double)), this, SLOT(OnHighlightFactor(double)));
		connect(d->ui.maskHighlight, SIGNAL(clicked()), this, SLOT(OnMaskHighlight()));
		connect(d->ui.maskIdxSpin, SIGNAL(valueChanged(int)), this, SLOT(OnMaskIdxSpin(int)));
		connect(d->ui.upsampleSpin, SIGNAL(valueChanged(int)), this, SIGNAL(sigUpsample(int)));

		//Crop Connection
		connect(d->ui.cropChk, SIGNAL(clicked()), this, SLOT(OnCropChk()));
		connect(d->ui.xCropMin, SIGNAL(valueChanged(double)), this, SLOT(OnCropXmin(double)));
		connect(d->ui.xCropMax, SIGNAL(valueChanged(double)), this, SLOT(OnCropXmax(double)));
		connect(d->ui.yCropMin, SIGNAL(valueChanged(double)), this, SLOT(OnCropYmin(double)));
		connect(d->ui.yCropMax, SIGNAL(valueChanged(double)), this, SLOT(OnCropYmax(double)));
		connect(d->ui.zCropMin, SIGNAL(valueChanged(double)), this, SLOT(OnCropZmin(double)));
		connect(d->ui.zCropMax, SIGNAL(valueChanged(double)), this, SLOT(OnCropZmax(double)));
				
		connect(d->ui.jitteringChk, SIGNAL(clicked()), this, SLOT(OnJittering()));
		d->ui.deferChk->setChecked(true);
		connect(d->ui.deferChk, SIGNAL(clicked()), this, SLOT(OnDefer()));
	}

	void Mask3dControl::OnDefer() {
		emit sigDefer(d->ui.deferChk->isChecked());
	}

	auto Mask3dControl::SendCropSignal() -> void {
		const auto xmin = d->ui.xCropMin->value();
		const auto xmax = d->ui.xCropMax->value();
		const auto ymin = d->ui.yCropMin->value();
		const auto ymax = d->ui.yCropMax->value();
		const auto zmin = d->ui.zCropMin->value();
		const auto zmax = d->ui.zCropMax->value();
		emit sigCropRange(xmin, xmax, ymin, ymax, zmin, zmax);
	}

	void Mask3dControl::OnCapture() {
		const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/capture", QApplication::applicationDirPath()).toString();
		const auto path = QFileDialog::getSaveFileName(nullptr, tr("Save Screenshot as Image"), prev + "/", tr("Image files (*.png *.jpg *.tiff)"));
		if (path.isEmpty()) {
			return;
		}
		QSettings("Tomocube", "TomoAnalysis").setValue("recent/capture", path);

		emit sigCapture(path);
	}
	void Mask3dControl::OnJittering() {
		emit sigJittering(d->ui.jitteringChk->isChecked());
	}
	void Mask3dControl::OnCropChk() {
		if (d->ui.cropChk->isChecked()) {
			d->ui.cropGroup->setEnabled(true);
			SendCropSignal();
		} else {
			d->ui.cropGroup->setEnabled(false);
			emit sigCropRange(0, 1, 0, 1, 0, 1);
		}
	}

	void Mask3dControl::OnCropXmin(double val) {
		if (val >= d->ui.xCropMax->value()) {
			d->ui.xCropMin->blockSignals(true);
			d->ui.xCropMin->setValue(val - 0.01);
			d->ui.xCropMin->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void Mask3dControl::OnCropXmax(double val) {
		if (val <= d->ui.xCropMin->value()) {
			d->ui.xCropMax->blockSignals(true);
			d->ui.xCropMax->setValue(val + 0.01);
			d->ui.xCropMax->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void Mask3dControl::OnCropYmin(double val) {
		if (val >= d->ui.yCropMax->value()) {
			d->ui.yCropMin->blockSignals(true);
			d->ui.yCropMin->setValue(val - 0.01);
			d->ui.yCropMin->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void Mask3dControl::OnCropYmax(double val) {
		if (val <= d->ui.yCropMin->value()) {
			d->ui.yCropMax->blockSignals(true);
			d->ui.yCropMax->setValue(val + 0.01);
			d->ui.yCropMax->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void Mask3dControl::OnCropZmin(double val) {
		if (val >= d->ui.zCropMax->value()) {
			d->ui.zCropMin->blockSignals(true);
			d->ui.zCropMin->setValue(val - 0.01);
			d->ui.zCropMin->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void Mask3dControl::OnCropZmax(double val) {
		if (val <= d->ui.zCropMin->value()) {
			d->ui.zCropMax->blockSignals(true);
			d->ui.zCropMax->setValue(val + 0.01);
			d->ui.zCropMax->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void Mask3dControl::OnMaskIdxSpin(int idx) {
		emit sigMaskHighlightIdx(idx);
	}

	void Mask3dControl::OnMaskHighlight() {
		const auto useHighlight = d->ui.maskHighlight->isChecked();
		if (useHighlight) {
			d->ui.maskIdxSpin->setEnabled(true);
			emit sigMaskHighlightIdx(d->ui.maskIdxSpin->value());
			return;
		}
		d->ui.maskIdxSpin->setEnabled(false);
		emit sigMaskHighlightIdx(-1);
	}

	void Mask3dControl::OnHighlightFactor(double val) {
		emit sigHighlightFactor(val);
	}

	void Mask3dControl::OnMaskModeCombo(int) {
		const auto modeID = d->ui.maskModeCombo->currentText();
		if (modeID == "Exclusive") {
			d->ui.maskFactorSpin->setEnabled(false);
			emit sigHighlightFactor(1);
		} else if (modeID == "Use Transparency") {
			d->ui.maskFactorSpin->setEnabled(true);
			emit sigHighlightFactor(d->ui.maskFactorSpin->value());
		}
	}

	auto Mask3dControl::SetMaxlabelIndex(int idx) -> void {
		d->ui.maskIdxSpin->blockSignals(true);
		d->ui.maskIdxSpin->setRange(1, idx);
		d->ui.maskIdxSpin->setValue(1);
		d->ui.maskIdxSpin->blockSignals(false);
	}
}
