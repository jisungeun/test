#pragma once

#include <enum.h>

#include <QWidget>

#include "IData.h"
#include "IView.h"

namespace CellAnalyzer::Presenter::Volume {
	class Mask3dControl final : public QWidget, public IView {
		Q_OBJECT
	public:
		explicit Mask3dControl(QWidget* parent = nullptr);
		~Mask3dControl();

		auto SetMaxlabelIndex(int idx)->void;

	signals:
		void sigHighlightFactor(double);
		void sigMaskHighlightIdx(int);		
		void sigCropRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);
		void sigCapture(QString);
		void sigUpsample(int);
		void sigJittering(bool);
		void sigDefer(bool);

	protected slots:
		void OnMaskModeCombo(int);
		void OnHighlightFactor(double);
		void OnMaskHighlight();
		void OnMaskIdxSpin(int);
		
		void OnCropChk();
		void OnCropXmin(double);
		void OnCropXmax(double);
		void OnCropYmin(double);
		void OnCropYmax(double);
		void OnCropZmin(double);
		void OnCropZmax(double);

		void OnCapture();
		void OnJittering();
		void OnDefer();

	protected:
		auto InitConnections()->void;
		auto SendCropSignal()->void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}