#pragma once

#include <enum.h>

#include <QWidget>

#include "IData.h"
#include "IView.h"

class SoSeparator;
class SoVolumeData;

namespace CellAnalyzer::Presenter::Volume {
	class TF2d3dControl final : public QWidget, public IView {
		Q_OBJECT
	public:
		explicit TF2d3dControl(QWidget* parent = nullptr);
		~TF2d3dControl();
		
		auto SetDataRangeConfig(int decimals, double divider, double offset, double singlestep) -> void;
		auto SetDataMinMax(double min, double max) -> void;

		auto SetCurrentVolume(SoVolumeData* vol,double min,double max,bool isFloat)->void;
		auto GetHiddenSepRoot()->SoSeparator*;

	signals:
		void sigDataRange(double, double);
				
		void sigCropRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);

		void sigCapture(QString);
		void sigUpsample(int);

		void sigJittering(bool);
		void sigDefer(bool);				

	protected slots:
		void OnDataMinSpin(double);
		void OnDataMaxSpin(double);
				
		void OnCropChk();
		void OnCropXmin(double);
		void OnCropXmax(double);
		void OnCropYmin(double);
		void OnCropYmax(double);
		void OnCropZmin(double);
		void OnCropZmax(double);

		void OnCapture();

		void OnJittering();
		void OnDefer();

		void OnUpdatePolygon();
		void OnCanvasChanged();
		void OnTFImageChanged();

		void OnCurItemChanged(int);

		void OnTranspSpinChanged(double);
		void OnTranspSliderChanged(int);

	protected:
		auto InitConnections() -> void;
		auto SendCropSignal() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
