#include "TF2d3dPresenter.h"

#include <QVBoxLayout>

#include <HT3D.h>
#include <FL3D.h>
#include <Float3d.h>

#include <RenderWindow3dWidget.h>
#include <VolumeTF2D.h>
#include <TF2d3dControl.h>

#include <VolumeViz/nodes/SoDataRange.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoScale.h>
#include <Medical/helpers/MedicalHelper.h>

#include "ui_TF2d3dPresenter.h"

namespace CellAnalyzer::Presenter::Volume {
	using namespace Tomocube::Rendering;

	struct TF2d3dPresenter::Impl {
		Ui::TF2d3d ui;

		std::shared_ptr<RenderWindow> window { nullptr };
		std::shared_ptr<Image::VolumeTF2D> renderer { nullptr };
		std::shared_ptr<TF2d3dControl> control { nullptr };
		QString title;
		int upsample { 1 };

		QString imageName;
		DataPtr imageData { nullptr };	
		int maxSignal { 1 };
		int minSignal { 0 };

		auto AddHT3D(std::shared_ptr<Data::HT3D> data) -> bool;
		auto AddFL3D(std::shared_ptr<Data::FL3D> data) -> bool;
		auto AddFloat3D(std::shared_ptr<Data::Float3D> data) -> bool;
	};
				
	auto TF2d3dPresenter::Impl::AddHT3D(std::shared_ptr<Data::HT3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2);
		//add SceneGraph
		const auto [min, max] = data->GetRI();

		renderer->SetDataMinMax(min * 10000.0, max * 10000.0);
		renderer->SetVolume(volData.ptr());

		control->SetDataRangeConfig(4, 10000, 0, 0.0001);
		control->SetDataMinMax(min * 10000.0, max * 10000.0);
		control->SetCurrentVolume(volData.ptr(), min * 10000.0, max * 10000.0, false);

		const auto [riMin, riMax] = data->GetRI();
		minSignal = static_cast<int>(riMin * 10000.0);
		maxSignal = static_cast<int>(riMax * 10000.0);
		
		return true;
	}

	auto TF2d3dPresenter::Impl::AddFL3D(std::shared_ptr<Data::FL3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();
		const auto channel = data->GetChannelIndex();

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + offset, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + offset);

		const auto [min, max] = data->GetIntensity();

		renderer->SetDataMinMax(min, max);
		renderer->SetVolume(volData.ptr());

		control->SetDataRangeConfig(0, 1, 0, 1);
		control->SetDataMinMax(min, max);
		control->SetCurrentVolume(volData.ptr(), min, max, false);

		return true;
	}

	auto TF2d3dPresenter::Impl::AddFloat3D(std::shared_ptr<Data::Float3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::FLOAT, 32, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + offset, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + offset);

		double min, max;
		volData->getMinMax(min, max);

		renderer->SetDataMinMax(min, max);
		renderer->SetVolume(volData.ptr());

		control->SetDataRangeConfig(2, 1, 0, 0.01);
		control->SetDataMinMax(min, max);
		control->SetCurrentVolume(volData.ptr(), min, max, true);
		return true;
	}

	TF2d3dPresenter::TF2d3dPresenter(QWidget* parent) : IPresenter(), QWidget(parent), d { new Impl } {
		d->ui.setupUi(this);
		d->window = std::make_shared<RenderWindow>(nullptr);
		d->window->SetGradientBackground(0,0,0,0,0,0);
		const auto layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		d->ui.RenderSocket->setLayout(layout);
		layout->addWidget(d->window.get());
						
		d->control = std::make_shared<TF2d3dControl>();

		d->renderer = std::make_shared<Image::VolumeTF2D>("TF2D");
		d->renderer->SetHiddenSep(d->control->GetHiddenSepRoot());
		d->renderer->BuildSceneGraph();

		SoRef<SoSeparator> swapRoot = new SoSeparator;
		SoRef<SoScale> swap = new SoScale;
		swap->scaleFactor.setValue(1, -1, 1);
		swapRoot->addChild(swap.ptr());
		swapRoot->addChild(d->renderer->GetRootSwitch());
		d->window->SetSceneGraph(swapRoot.ptr());

		InitConnections();		
	}

	TF2d3dPresenter::~TF2d3dPresenter() = default;

	auto TF2d3dPresenter::InitConnections() -> void {
		connect(d->control.get(), &TF2d3dControl::sigUpsample, this, [this](int upsample) {
			d->upsample = upsample;
		});
		connect(d->control.get(), &TF2d3dControl::sigCapture, this, [this](QString path) {
			CaptureScreen(path);
		});
		connect(d->control.get(), &TF2d3dControl::sigDataRange, this, [this](double min, double max) {
			OnDataRange(min, max);
		});		
		connect(d->control.get(), &TF2d3dControl::sigCropRange, this, [this](double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
			OnCropRange(xmin, xmax, ymin, ymax, zmin, zmax);
		});
		connect(d->control.get(), &TF2d3dControl::sigJittering, this, [this](bool use) {
			d->renderer->ToggleJittering(use);
			});
		connect(d->control.get(), &TF2d3dControl::sigDefer, this, [this](bool use) {
			d->renderer->ToggleDeferredLighting(use);
			});		
		connect(d->window.get(), &RenderWindow::sendMouseClick, this, [this]() {
			OnMouseClick();
		});
	}

	void TF2d3dPresenter::OnMouseClick() {
		setFocus();
	}

	void TF2d3dPresenter::OnCropRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
		d->renderer->SetXRange(xmin, xmax);
		d->renderer->SetYRange(ymin, ymax);
		d->renderer->SetZRange(zmin, zmax);
	}
		
	void TF2d3dPresenter::OnDataRange(double min, double max) {
		d->renderer->SetDataRange(min, max);
	}

	auto TF2d3dPresenter::SetTitle(const QString& title) -> void {
		setWindowTitle(title);
		d->control->setWindowTitle(title + " Control");
	}

	auto TF2d3dPresenter::GetWindowList() const -> ViewList {
		return { d->control };
	}

	auto TF2d3dPresenter::IsPrimary(const ViewPtr& window) const -> bool {
		return true;
	}

	auto TF2d3dPresenter::IsAcceptable(const DataPtr& data) const -> bool {
		return true;
	}

	auto TF2d3dPresenter::GetDataList() const -> QStringList {
		QStringList datalist;
		if (false == d->imageName.isEmpty()) {
			datalist.append(d->imageName);
		}
		return datalist;
	}

	auto TF2d3dPresenter::GetData(const QString& name) const -> DataPtr {
		if (d->imageName == name) {
			return d->imageData;
		}
		return {};
	}

	auto TF2d3dPresenter::GetName(const DataPtr& data) const -> QString {
		if (d->imageData == data) {
			return d->imageName;
		}
		return {};
	}

	auto TF2d3dPresenter::RemoveData(const QString& name) -> void {
		//DO NOT USE
	}

	auto TF2d3dPresenter::RenameData(const DataPtr& data, const QString& name) -> bool {
		const auto prevName = GetName(data);
		if (prevName.isEmpty()) {
			return false;
		}
		if (d->imageName == prevName) {
			d->imageName = name;
		}

		return true;
	}

	auto TF2d3dPresenter::ClearData() -> void { }

	auto TF2d3dPresenter::Capture(const QString& filepath) -> void { }

	auto TF2d3dPresenter::CaptureScreen(const QString& path, bool fullResolution) -> void {
		//set fixed resolution
		const auto volDataList = MedicalHelper::findNodes<SoVolumeData>(d->renderer->GetRootSwitch());
		if (fullResolution) {
			for (const auto volData : volDataList) {
				volData->ldmResourceParameters.getValue()->resolution = 0;
				volData->ldmResourceParameters.getValue()->fixedResolution = TRUE;
			}
		}
		d->window->SaveSnapShot(path, d->upsample);
		//restore resolution
		if (fullResolution) {
			for (const auto volData : volDataList) {
				volData->ldmResourceParameters.getValue()->fixedResolution = FALSE;
			}
		}
	}

	auto TF2d3dPresenter::AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool {
		Q_UNUSED(portName)
		if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(data)) {
			if (false == d->AddHT3D(image)) {
				return false;
			}
			d->imageData = data;
			d->imageName = name;
		} else if (const auto fl = std::dynamic_pointer_cast<Data::FL3D>(data)) {
			if (false == d->AddFL3D(fl)) {
				return false;
			}
			d->imageData = data;
			d->imageName = name;
		} else if (const auto floatImg = std::dynamic_pointer_cast<Data::Float3D>(data)) {
			if (false == d->AddFloat3D(floatImg)) {
				return false;
			}
			d->imageData = data;
			d->imageName = name;
		}
		d->window->ResetView();
		return true;
	}
}
