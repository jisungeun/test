#include <QApplication>
#include <QSettings>
#include <QFileDialog>
#include <QIcon>
#include <QVBoxLayout>
#include <QGraphicsView>
#include <QButtonGroup>
#include <QCanvasViewRect.h>
#include <QCanvasViewPolygon.h>
#include <QCanvasViewLasso.h>
#include <HiddenSceneGeneral.h>

#include <iostream>

#include "ui_TF2d3dControl.h"
#include "TF2d3dControl.h"

#include <QColorDialog>
#include <Inventor/nodes/SoSeparator.h>

namespace CellAnalyzer::Presenter::Volume {
	struct TF2d3dControl::Impl {
		Ui::LayerControl ui;
		TF2d3dControl* thisPointer { nullptr };
		double divider { 1 };
		double offset { 0 };
		double singlestep { 0.01 };
		int decimals { 2 };

		int cur_item_idx { -1 };

		QButtonGroup group1;
		QButtonGroup group2;

		std::shared_ptr<QCanvasViewRect> canvas_rect { nullptr };
		std::shared_ptr<QCanvasViewLasso> canvas_lasso { nullptr };
		std::shared_ptr<QCanvasViewPolygon> canvas_polygon { nullptr };
		std::shared_ptr<HiddenSceneGeneral> hiddenScene { nullptr };

		auto InitUI() -> void;
	};

	auto TF2d3dControl::Impl::InitUI() -> void {
		ui.upsampleSpin->setRange(1, 5);
		ui.upsampleSpin->setSingleStep(1);
		ui.upsampleSpin->setValue(1);

		ui.dataRangeMin->setDecimals(decimals);
		ui.dataRangeMax->setDecimals(decimals);

		ui.xCropMin->setRange(0, 0.99);
		ui.xCropMin->setDecimals(2);
		ui.xCropMin->setSingleStep(0.01);
		ui.xCropMin->setValue(0);
		ui.xCropMax->setRange(0.01, 1);
		ui.xCropMax->setDecimals(2);
		ui.xCropMax->setSingleStep(0.01);
		ui.xCropMax->setValue(1);

		ui.yCropMin->setRange(0, 0.99);
		ui.yCropMin->setDecimals(2);
		ui.yCropMin->setSingleStep(0.01);
		ui.yCropMin->setValue(0);
		ui.yCropMax->setRange(0.01, 1);
		ui.yCropMax->setDecimals(2);
		ui.yCropMax->setSingleStep(0.01);
		ui.yCropMax->setValue(1);

		ui.zCropMin->setRange(0, 0.99);
		ui.zCropMin->setDecimals(2);
		ui.zCropMin->setSingleStep(0.01);
		ui.zCropMin->setValue(0);
		ui.zCropMax->setRange(0.01, 1);
		ui.zCropMax->setDecimals(2);
		ui.zCropMax->setSingleStep(0.01);
		ui.zCropMax->setValue(1);

		ui.cropGroup->setEnabled(false);

		ui.captureBtn->setIcon(QIcon(":/Widget/CaptureCamera.svg"));
		ui.captureBtn->setIconSize(QSize(25, 25));
		ui.captureBtn->setToolTip("Take screenshot");

		canvas_rect = std::make_shared<QCanvasViewRect>(thisPointer);
		canvas_lasso = std::make_shared<QCanvasViewLasso>(thisPointer);
		canvas_polygon = std::make_shared<QCanvasViewPolygon>(thisPointer);
		auto* layout1 = new QVBoxLayout;
		layout1->setContentsMargins(0, 0, 0, 0);
		layout1->setSpacing(0);

		ui.rectSocket->setLayout(layout1);
		layout1->addWidget(canvas_rect.get());

		auto* layout2 = new QVBoxLayout;
		layout2->setContentsMargins(0, 0, 0, 0);
		layout2->setSpacing(0);

		ui.polySocket->setLayout(layout2);
		layout2->addWidget(canvas_polygon.get());

		auto* layout3 = new QVBoxLayout;
		layout3->setContentsMargins(0, 0, 0, 0);
		layout3->setSpacing(0);

		ui.lassoSocket->setLayout(layout3);
		layout3->addWidget(canvas_lasso.get());

		hiddenScene = std::make_shared<HiddenSceneGeneral>();
		hiddenScene->SetImageParentDir(qApp->applicationDirPath());

		group1.addButton(ui.rectRadio);
		group1.addButton(ui.lassoRadio);
		group1.addButton(ui.polyRadio);

		group2.addButton(ui.grayTFChk);
		group2.addButton(ui.colorTFChk);

		ui.rectRadio->setChecked(true);

		ui.grayTFChk->setChecked(true);

		ui.jitteringChk->setChecked(true);

		ui.transpSpin->setRange(0, 1);
		ui.transpSpin->setDecimals(2);
		ui.transpSpin->setSingleStep(0.01);

		ui.transpSlider->setRange(0, 100);
		ui.transpSlider->setSingleStep(1);

		ui.transpSpin->setEnabled(false);
		ui.transpSlider->setEnabled(false);
	}

	TF2d3dControl::TF2d3dControl(QWidget* parent) : QWidget(parent), d { new Impl } {
		d->thisPointer = this;
		d->ui.setupUi(this);
		d->InitUI();
		InitConnections();
	}

	TF2d3dControl::~TF2d3dControl() { }

	auto TF2d3dControl::SetCurrentVolume(SoVolumeData* vol, double min, double max, bool isFloat) -> void {
		d->hiddenScene->SetDataMinMax(min, max);
		d->hiddenScene->SetFilePath("NewTrigger");
		if (isFloat) {
			d->hiddenScene->Calc2DHistogramFloat(vol, false);
		} else {
			d->hiddenScene->Calc2DHistogram(vol, false);
		}

		QString histoPath;

		QDir dir(qApp->applicationDirPath());
		dir.cdUp();
		if (d->ui.grayTFChk->isChecked()) {
			histoPath = QString("border-image:url(\"%1/Background.tif\") 0 0 0 0 stretch stretch;").arg(dir.path());
		} else {
			histoPath = QString("border-image:url(\"%1/ColorBackground.tif\") 0 0 0 0 stretch stretch;").arg(dir.path());
		}
		d->canvas_rect->setStyleSheet(histoPath);
		d->canvas_lasso->setStyleSheet(histoPath);
		d->canvas_polygon->setStyleSheet(histoPath);
	}


	auto TF2d3dControl::GetHiddenSepRoot() -> SoSeparator* {
		return d->hiddenScene->GetSceneGraph();
	}

	auto TF2d3dControl::InitConnections() -> void {
		//General Connection
		connect(d->ui.dataRangeMin, SIGNAL(valueChanged(double)), this, SLOT(OnDataMinSpin(double)));
		connect(d->ui.dataRangeMax, SIGNAL(valueChanged(double)), this, SLOT(OnDataMaxSpin(double)));
		connect(d->ui.captureBtn, SIGNAL(clicked()), this, SLOT(OnCapture()));
		connect(d->ui.upsampleSpin, SIGNAL(valueChanged(int)), this, SIGNAL(sigUpsample(int)));


		//Crop Connection
		connect(d->ui.cropChk, SIGNAL(clicked()), this, SLOT(OnCropChk()));
		connect(d->ui.xCropMin, SIGNAL(valueChanged(double)), this, SLOT(OnCropXmin(double)));
		connect(d->ui.xCropMax, SIGNAL(valueChanged(double)), this, SLOT(OnCropXmax(double)));
		connect(d->ui.yCropMin, SIGNAL(valueChanged(double)), this, SLOT(OnCropYmin(double)));
		connect(d->ui.yCropMax, SIGNAL(valueChanged(double)), this, SLOT(OnCropYmax(double)));
		connect(d->ui.zCropMin, SIGNAL(valueChanged(double)), this, SLOT(OnCropZmin(double)));
		connect(d->ui.zCropMax, SIGNAL(valueChanged(double)), this, SLOT(OnCropZmax(double)));

		connect(d->ui.jitteringChk, SIGNAL(clicked()), this, SLOT(OnJittering()));
		connect(d->ui.deferChk, SIGNAL(clicked()), this, SLOT(OnDefer()));

		connect(d->canvas_rect.get(), SIGNAL(sigUpdatePolygons()), this, SLOT(OnUpdatePolygon()));
		connect(d->canvas_lasso.get(), SIGNAL(sigUpdatePolygons()), this, SLOT(OnUpdatePolygon()));
		connect(d->canvas_polygon.get(), SIGNAL(sigUpdatePolygons()), this, SLOT(OnUpdatePolygon()));

		connect(d->canvas_polygon.get(), SIGNAL(sigCurPolygon(int)), this, SLOT(OnCurItemChanged(int)));
		connect(d->canvas_rect.get(), SIGNAL(sigCurPolygon(int)), this, SLOT(OnCurItemChanged(int)));
		connect(d->canvas_lasso.get(), SIGNAL(sigCurPolygon(int)), this, SLOT(OnCurItemChanged(int)));

		connect(d->ui.rectRadio, SIGNAL(clicked()), this, SLOT(OnCanvasChanged()));
		connect(d->ui.lassoRadio, SIGNAL(clicked()), this, SLOT(OnCanvasChanged()));
		connect(d->ui.polyRadio, SIGNAL(clicked()), this, SLOT(OnCanvasChanged()));

		connect(d->ui.grayTFChk, SIGNAL(clicked()), this, SLOT(OnTFImageChanged()));
		connect(d->ui.colorTFChk, SIGNAL(clicked()), this, SLOT(OnTFImageChanged()));

		connect(d->ui.transpSpin, SIGNAL(valueChanged(double)), this, SLOT(OnTranspSpinChanged(double)));
		connect(d->ui.transpSlider, SIGNAL(valueChanged(int)), this, SLOT(OnTranspSliderChanged(int)));
	}

	void TF2d3dControl::OnCurItemChanged(int idx) {
		d->cur_item_idx = idx;
		if (idx < 0) {
			d->ui.transpSlider->setEnabled(false);
			d->ui.transpSpin->setEnabled(false);
			return;
		}
		d->ui.transpSlider->setEnabled(true);
		d->ui.transpSpin->setEnabled(true);

		double transp;
		if (d->ui.rectRadio->isChecked()) {
			const auto color = d->canvas_rect->GetColors()[idx];
			transp = color.alphaF();
		} else if (d->ui.polyRadio->isChecked()) {
			const auto color = d->canvas_polygon->GetColors()[idx];
			transp = color.alphaF();
		} else {
			const auto color = d->canvas_lasso->GetColors()[idx];
			transp = color.alphaF();
		}
		d->ui.transpSlider->blockSignals(true);
		d->ui.transpSlider->setValue(static_cast<int>(std::round(transp * 100)));
		d->ui.transpSlider->blockSignals(false);

		d->ui.transpSpin->blockSignals(true);
		d->ui.transpSpin->setValue(transp);
		d->ui.transpSpin->blockSignals(false);
	}

	void TF2d3dControl::OnTranspSliderChanged(int val) {
		const auto doubleVal = static_cast<double>(val) / 100.0;
		d->ui.transpSpin->blockSignals(true);
		d->ui.transpSpin->setValue(doubleVal);
		d->ui.transpSpin->blockSignals(false);
		if (d->ui.rectRadio->isChecked()) {
			d->canvas_rect->SetTransp(d->cur_item_idx, doubleVal);
		} else if (d->ui.polyRadio->isChecked()) {
			d->canvas_polygon->SetTransp(d->cur_item_idx, doubleVal);
		} else {
			d->canvas_lasso->SetTransp(d->cur_item_idx, doubleVal);
		}

	}

	void TF2d3dControl::OnTranspSpinChanged(double val) {
		const auto intVal = static_cast<int>(val * 100);
		d->ui.transpSlider->blockSignals(true);
		d->ui.transpSlider->setValue(intVal);
		d->ui.transpSlider->blockSignals(false);
		if (d->ui.rectRadio->isChecked()) {
			d->canvas_rect->SetTransp(d->cur_item_idx, val);
		} else if (d->ui.polyRadio->isChecked()) {
			d->canvas_polygon->SetTransp(d->cur_item_idx, val);
		} else {
			d->canvas_lasso->SetTransp(d->cur_item_idx, val);
		}
	}


	void TF2d3dControl::OnTFImageChanged() {
		QString histoPath;

		QDir dir(qApp->applicationDirPath());
		dir.cdUp();
		if (d->ui.grayTFChk->isChecked()) {
			histoPath = QString("border-image:url(\"%1/Background.tif\") 0 0 0 0 stretch stretch;").arg(dir.path());
		} else {
			histoPath = QString("border-image:url(\"%1/ColorBackground.tif\") 0 0 0 0 stretch stretch;").arg(dir.path());
		}
		d->canvas_rect->setStyleSheet(histoPath);
		d->canvas_lasso->setStyleSheet(histoPath);
		d->canvas_polygon->setStyleSheet(histoPath);
	}

	void TF2d3dControl::OnUpdatePolygon() {
		QList<QPolygonF> polygons;
		QList<QColor> colors;
		QList<bool> isHidden;
		if (d->ui.rectRadio->isChecked()) {
			polygons = d->canvas_rect->GetPolygons();
			colors = d->canvas_rect->GetColors();
			isHidden = d->canvas_rect->GetHidden();
		} else if (d->ui.lassoRadio->isChecked()) {
			polygons = d->canvas_lasso->GetPolygons();
			colors = d->canvas_lasso->GetColors();
			isHidden = d->canvas_lasso->GetHidden();
		} else if (d->ui.polyRadio->isChecked()) {
			polygons = d->canvas_polygon->GetPolygons();
			colors = d->canvas_polygon->GetColors();
			isHidden = d->canvas_polygon->GetHidden();
		}

		d->hiddenScene->UpdateTF(polygons, colors, isHidden);
	}

	void TF2d3dControl::OnCanvasChanged() {
		if (d->ui.rectRadio->isChecked()) {
			d->ui.stackedWidget->setCurrentIndex(0);
		} else if (d->ui.lassoRadio->isChecked()) {
			d->ui.stackedWidget->setCurrentIndex(2);
		} else {
			d->ui.stackedWidget->setCurrentIndex(1);
		}
		OnUpdatePolygon();
	}

	void TF2d3dControl::OnDefer() {
		emit sigDefer(d->ui.deferChk->isChecked());
	}

	void TF2d3dControl::OnJittering() {
		emit sigJittering(d->ui.jitteringChk->isChecked());
	}

	auto TF2d3dControl::SendCropSignal() -> void {
		const auto xmin = d->ui.xCropMin->value();
		const auto xmax = d->ui.xCropMax->value();
		const auto ymin = d->ui.yCropMin->value();
		const auto ymax = d->ui.yCropMax->value();
		const auto zmin = d->ui.zCropMin->value();
		const auto zmax = d->ui.zCropMax->value();
		emit sigCropRange(xmin, xmax, ymin, ymax, zmin, zmax);
	}

	void TF2d3dControl::OnCropChk() {
		if (d->ui.cropChk->isChecked()) {
			d->ui.cropGroup->setEnabled(true);
			SendCropSignal();
		} else {
			d->ui.cropGroup->setEnabled(false);
			emit sigCropRange(0, 1, 0, 1, 0, 1);
		}
	}

	void TF2d3dControl::OnCropXmin(double val) {
		if (val >= d->ui.xCropMax->value()) {
			d->ui.xCropMin->blockSignals(true);
			d->ui.xCropMin->setValue(val - 0.01);
			d->ui.xCropMin->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void TF2d3dControl::OnCropXmax(double val) {
		if (val <= d->ui.xCropMin->value()) {
			d->ui.xCropMax->blockSignals(true);
			d->ui.xCropMax->setValue(val + 0.01);
			d->ui.xCropMax->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void TF2d3dControl::OnCropYmin(double val) {
		if (val >= d->ui.yCropMax->value()) {
			d->ui.yCropMin->blockSignals(true);
			d->ui.yCropMin->setValue(val - 0.01);
			d->ui.yCropMin->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void TF2d3dControl::OnCropYmax(double val) {
		if (val <= d->ui.yCropMin->value()) {
			d->ui.yCropMax->blockSignals(true);
			d->ui.yCropMax->setValue(val + 0.01);
			d->ui.yCropMax->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void TF2d3dControl::OnCropZmin(double val) {
		if (val >= d->ui.zCropMax->value()) {
			d->ui.zCropMin->blockSignals(true);
			d->ui.zCropMin->setValue(val - 0.01);
			d->ui.zCropMin->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void TF2d3dControl::OnCropZmax(double val) {
		if (val <= d->ui.zCropMin->value()) {
			d->ui.zCropMax->blockSignals(true);
			d->ui.zCropMax->setValue(val + 0.01);
			d->ui.zCropMax->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void TF2d3dControl::OnCapture() {
		const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/capture", QApplication::applicationDirPath()).toString();
		const auto path = QFileDialog::getSaveFileName(nullptr, tr("Save Screenshot as Image"), prev + "/", tr("Image files (*.png *.jpg *.tiff)"));
		if (path.isEmpty()) {
			return;
		}
		QSettings("Tomocube", "TomoAnalysis").setValue("recent/capture", path);

		emit sigCapture(path);
	}

	void TF2d3dControl::OnDataMinSpin(double val) {
		if (val >= d->ui.dataRangeMax->value()) {
			d->ui.dataRangeMin->blockSignals(true);
			d->ui.dataRangeMin->setValue(val - d->singlestep);
			d->ui.dataRangeMin->blockSignals(false);
			return;
		}
		const auto textmin = d->ui.dataRangeMin->value();
		const auto textmax = d->ui.dataRangeMax->value();
		const auto realMin = (textmin - d->offset) * d->divider;
		const auto realMax = (textmax - d->offset) * d->divider;

		emit sigDataRange(realMin, realMax);
	}

	void TF2d3dControl::OnDataMaxSpin(double val) {
		if (val <= d->ui.dataRangeMin->value()) {
			d->ui.dataRangeMax->blockSignals(true);
			d->ui.dataRangeMax->setValue(val + d->singlestep);
			d->ui.dataRangeMax->blockSignals(false);
			return;
		}
		const auto textmin = d->ui.dataRangeMin->value();
		const auto textmax = d->ui.dataRangeMax->value();
		const auto realMin = (textmin - d->offset) * d->divider;
		const auto realMax = (textmax - d->offset) * d->divider;

		emit sigDataRange(realMin, realMax);
	}

	auto TF2d3dControl::SetDataRangeConfig(int decimals, double divider, double offset, double singlestep) -> void {
		d->decimals = decimals;
		d->divider = divider;
		d->offset = offset;
		d->singlestep = singlestep;

		d->ui.dataRangeMin->blockSignals(true);
		d->ui.dataRangeMax->blockSignals(true);

		d->ui.dataRangeMin->setDecimals(decimals);
		d->ui.dataRangeMin->setSingleStep(singlestep);

		d->ui.dataRangeMax->setDecimals(decimals);
		d->ui.dataRangeMax->setSingleStep(singlestep);

		d->ui.dataRangeMin->blockSignals(false);
		d->ui.dataRangeMax->blockSignals(false);
	}

	auto TF2d3dControl::SetDataMinMax(double min, double max) -> void {
		const auto textmin = min / d->divider + d->offset;
		const auto textmax = max / d->divider + d->offset;

		d->ui.dataRangeMin->blockSignals(true);
		d->ui.dataRangeMax->blockSignals(true);

		d->ui.dataRangeMin->setRange(textmin, textmax - d->singlestep);
		d->ui.dataRangeMax->setRange(textmin + d->singlestep, textmax);

		d->ui.dataRangeMin->setValue(textmin);
		d->ui.dataRangeMax->setValue(textmax);

		d->ui.dataRangeMin->blockSignals(false);
		d->ui.dataRangeMax->blockSignals(false);
	}
}
