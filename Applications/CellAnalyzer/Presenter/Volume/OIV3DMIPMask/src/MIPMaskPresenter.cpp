#include "MIPMaskPresenter.h"

#include <QVBoxLayout>

#include <HT3D.h>
#include <FL3D.h>
#include <Float3d.h>
#include <BinaryMask3D.h>
#include <LabelMask3D.h>

#include <RenderWindow3dWidget.h>
#include <VolumeMipMask.h>
#include <MIPMaskControl.h>

#include <VolumeViz/nodes/SoDataRange.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoScale.h>
#include <Medical/helpers/MedicalHelper.h>

#include "ui_MIPMaskPresenter.h"

namespace CellAnalyzer::Presenter::Volume {
	using namespace Tomocube::Rendering;

	struct MIPMaskPresenter::Impl {
		Ui::MIPMaskOIV ui;

		std::shared_ptr<RenderWindow> window { nullptr };
		std::shared_ptr<Blending::VolumeMipMask> renderer { nullptr };
		std::shared_ptr<MIPMaskControl> control { nullptr };
		QString title;
		float color_table[12][3];
		int upsample { 1 };

		QString imageName;
		DataPtr imageData { nullptr };
		QString maskName;
		DataPtr maskData { nullptr };

		bool ht3dExist { false };

		SoRef<SoVolumeData> maskVol { nullptr };
		double refOffset { 0 };
		Size3D refSize { 1, 1, 1 };
		Resolution3D refRes { 1, 1, 1 };

		auto BuildLabelColorTable() -> void;

		auto AddReference3D(std::shared_ptr<Data::HT3D> data) -> bool;
		auto AddHT3D(std::shared_ptr<Data::HT3D> data) -> bool;
		auto AddFL3D(std::shared_ptr<Data::FL3D> data) -> bool;
		auto AddFloat3D(std::shared_ptr<Data::Float3D> data) -> bool;
		auto AddLabel3D(std::shared_ptr<Data::LabelMask3D> data) -> bool;
		auto AddMask3D(std::shared_ptr<Data::BinaryMask3D> data) -> bool;
		auto AdjustOffsetZ(double offset) -> double;

		auto IsIdentical(Size3D maskSize, Resolution3D maskRes, float maskOffset) -> bool;
		auto MapMaskGeometry(Size3D maskSize, Resolution3D maskRes, float maskOffset, void* maskData) -> void;
	};

	auto MIPMaskPresenter::Impl::MapMaskGeometry(Size3D maskSize, Resolution3D maskRes, float maskOffset, void* maskData) -> void {
		auto GetPhysicalPosition = [](int ix, int iy, int iz, double spx, double spy, double spz, double originX, double originY, double originZ, double offset) -> std::tuple<double, double, double> {
			auto phyX = originX + ix * spx;
			auto phyY = originY + iy * spy;
			auto phyZ = originZ + offset + iz * spz;

			return std::make_tuple(phyX, phyY, phyZ);
		};
		auto GetIndexPosition = [](double px, double py, double pz, double spx, double spy, double spz, double originX, double originY, double originZ, double offset) -> std::tuple<int, int, int> {
			auto ix = static_cast<int>(floor((px - originX) / spx));
			auto iy = static_cast<int>(floor((py - originY) / spy));
			auto iz = static_cast<int>(floor((pz - originZ - offset) / spz));

			return std::make_tuple(ix, iy, iz);
		};

		double refOrigin[3] = { -refSize.x * refRes.x / 2, -refSize.y * refRes.y / 2, -refSize.z * refRes.z / 2 };
		double maskOrigin[3] = { -maskSize.x * maskRes.x / 2, -maskSize.y * maskRes.y / 2, -maskSize.z * maskRes.z / 2 };

		const auto sourceArr = static_cast<const uint16_t*>(maskData);

		std::shared_ptr<uint16_t[]> arr(new uint16_t[refSize.x * refSize.y * refSize.z](), std::default_delete<uint16_t[]>());

		for (auto k = 0; k < refSize.z; k++) {
			for (auto i = 0; i < refSize.x; i++) {
				for (auto j = 0; j < refSize.y; j++) {
					const auto [px, py, pz] = GetPhysicalPosition(i, j, k, refRes.x, refRes.y, refRes.z, refOrigin[0], refOrigin[1], refOrigin[2], AdjustOffsetZ(refOffset));
					const auto [ix, iy, iz] = GetIndexPosition(px, py, pz, maskRes.x, maskRes.y, maskRes.z, maskOrigin[0], maskOrigin[1], maskOrigin[2], AdjustOffsetZ(maskOffset));

					if (ix > -1 && ix < maskSize.x && iy > -1 && iy < maskSize.y && iz > -1 && iz < maskSize.z) {
						const auto sourceidx = iz * maskSize.x * maskSize.y + iy * maskSize.x + ix;
						const auto val = *(sourceArr + sourceidx);
						const auto targetidx = k * refSize.x * refSize.y + j * refSize.x + i;
						*(arr.get() + targetidx) = val;
					}
				}
			}
		}
		maskVol->data.setValue(SbVec3i32(refSize.x, refSize.y, refSize.z), SbDataType::UNSIGNED_SHORT, 16, arr.get(), SoSFArray::COPY);
		maskVol->extent.setValue(-refSize.x * refRes.x / 2, -refSize.y * refRes.y / 2, -refSize.z * refRes.z / 2 + AdjustOffsetZ(refOffset), refSize.x * refRes.x / 2, refSize.y * refRes.y / 2, refSize.z * refRes.z / 2 + AdjustOffsetZ(refOffset));
	}

	auto MIPMaskPresenter::Impl::IsIdentical(Size3D maskSize, Resolution3D maskRes, float maskOffset) -> bool {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.0001;
		};
		if (maskSize.x != refSize.x) {
			return false;
		}
		if (maskSize.y != refSize.y) {
			return false;
		}
		if (maskSize.z != refSize.z) {
			return false;
		}
		if (false == AreSame(maskRes.x, refRes.x)) {
			return false;
		}
		if (false == AreSame(maskRes.y, refRes.y)) {
			return false;
		}
		if (false == AreSame(maskRes.y, refRes.y)) {
			return false;
		}
		if (false == AreSame(maskOffset, refOffset)) {
			return false;
		}
		return true;
	}

	auto MIPMaskPresenter::Impl::AdjustOffsetZ(double offset) -> double {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.0001;
		};
		if (AreSame(offset, 0)) {
			return 0;
		}
		if (ht3dExist) {
			return offset - static_cast<double>(refSize.z) * refRes.z / 2.0;
		}
		return offset;
	}

	auto MIPMaskPresenter::Impl::AddReference3D(std::shared_ptr<Data::HT3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		refSize = size;
		refRes = res;

		return true;
	}

	auto MIPMaskPresenter::Impl::AddHT3D(std::shared_ptr<Data::HT3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		refSize = size;
		refRes = res;

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2);
		//add SceneGraph
		const auto [min, max] = data->GetRI();

		renderer->SetDataMinMax(min * 10000.0, max * 10000.0);
		renderer->SetVolume(volData.ptr());

		control->SetDataRangeConfig(4, 10000, 0, 0.0001);
		control->SetDataMinMax(min * 10000.0, max * 10000.0);
		control->SetImageType(DataFlag::HT);

		return true;
	}

	auto MIPMaskPresenter::Impl::AddFL3D(std::shared_ptr<Data::FL3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();
		const auto channel = data->GetChannelIndex();
		refSize = size;
		refRes = res;
		refOffset = offset;

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + AdjustOffsetZ(offset), size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + AdjustOffsetZ(offset));

		const auto [min, max] = data->GetIntensity();

		renderer->SetDataMinMax(min, max);
		renderer->SetVolume(volData.ptr());

		control->SetDataRangeConfig(0, 1, 0, 1);
		control->SetDataMinMax(min, max);

		control->SetChannelIndex(channel);
		control->SetImageType(DataFlag::FL);
		return true;
	}

	auto MIPMaskPresenter::Impl::AddFloat3D(std::shared_ptr<Data::Float3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();
		refSize = size;
		refRes = res;
		refOffset = offset;

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::FLOAT, 32, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + AdjustOffsetZ(offset), size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + AdjustOffsetZ(offset));

		double min, max;
		volData->getMinMax(min, max);

		renderer->SetDataMinMax(min, max);
		renderer->SetVolume(volData.ptr());

		control->SetDataRangeConfig(2, 1, 0, 0.01);
		control->SetDataMinMax(min, max);
		control->SetImageType(DataFlag::Null);
		return true;
	}

	auto MIPMaskPresenter::Impl::AddMask3D(std::shared_ptr<Data::BinaryMask3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();

		maskVol = new SoVolumeData;

		if (IsIdentical(size, res, offset)) {
			maskVol->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
			maskVol->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + AdjustOffsetZ(offset), size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + AdjustOffsetZ(offset));
		} else {
			MapMaskGeometry(size, res, offset, data->GetData());
		}


		const auto maxidx = 1;
		renderer->SetMaskVolume(maskVol.ptr());
		for (auto i = 0; i < maxidx; i++) {
			renderer->SetMaskColor(i + 1, QColor(color_table[i % 12][0], color_table[i % 12][1], color_table[i % 12][2]));
		}
		control->SetMaxlabelIndex(maxidx);
		return true;
	}

	auto MIPMaskPresenter::Impl::AddLabel3D(std::shared_ptr<Data::LabelMask3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();

		maskVol = new SoVolumeData;

		if (IsIdentical(size, res, offset)) {
			maskVol->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
			maskVol->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + AdjustOffsetZ(offset), size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + AdjustOffsetZ(offset));
		} else {
			MapMaskGeometry(size, res, offset, data->GetData());
		}

		const auto maxidx = data->GetMaxIndex();
		renderer->SetMaskVolume(maskVol.ptr());
		for (auto i = 0; i < maxidx; i++) {
			renderer->SetMaskColor(i + 1, QColor(color_table[i % 12][0], color_table[i % 12][1], color_table[i % 12][2]));
		}
		control->SetMaxlabelIndex(maxidx);
		return true;
	}

	auto MIPMaskPresenter::Impl::BuildLabelColorTable() -> void {
		color_table[0][0] = 255;
		color_table[0][1] = 59;
		color_table[0][2] = 48; //red
		color_table[1][0] = 255;
		color_table[1][1] = 149;
		color_table[1][2] = 0; //Orange
		color_table[2][0] = 255;
		color_table[2][1] = 204;
		color_table[2][2] = 0; //yellow
		color_table[3][0] = 52;
		color_table[3][1] = 199;
		color_table[3][2] = 89; //Green
		color_table[4][0] = 0;
		color_table[4][1] = 199;
		color_table[4][2] = 190; //Mint
		color_table[5][0] = 48;
		color_table[5][1] = 176;
		color_table[5][2] = 199; //Teal
		color_table[6][0] = 50;
		color_table[6][1] = 173;
		color_table[6][2] = 230; //Cyan
		color_table[7][0] = 0;
		color_table[7][1] = 122;
		color_table[7][2] = 255; //Blue
		color_table[8][0] = 88;
		color_table[8][1] = 86;
		color_table[8][2] = 214; //Indigo
		color_table[9][0] = 175;
		color_table[9][1] = 82;
		color_table[9][2] = 222; //Purple
		color_table[10][0] = 255;
		color_table[10][1] = 45;
		color_table[10][2] = 85; //Pink
		color_table[11][0] = 162;
		color_table[11][1] = 132;
		color_table[11][2] = 94; //Brown
	}

	MIPMaskPresenter::MIPMaskPresenter(QWidget* parent) : IPresenter(), QWidget(parent), d { new Impl } {
		d->ui.setupUi(this);
		d->BuildLabelColorTable();
		d->window = std::make_shared<RenderWindow>(nullptr);
		d->window->SetGradientBackground(0,0,0,0,0,0);
		const auto layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		d->ui.RenderSocket->setLayout(layout);
		layout->addWidget(d->window.get());

		d->renderer = std::make_shared<Blending::VolumeMipMask>("MIPMask");
		d->renderer->ToggleDepthEnhanced(false);
		d->renderer->SetDepthEnhancementFactor(1);
		d->renderer->ToggleUseMask(true);
		d->renderer->ToggleMaskColor(false);

		SoRef<SoSeparator> swapRoot = new SoSeparator;
		SoRef<SoScale> swap = new SoScale;
		swap->scaleFactor.setValue(1, -1, 1);
		swapRoot->addChild(swap.ptr());
		swapRoot->addChild(d->renderer->GetRootSwitch());
		d->window->SetSceneGraph(swapRoot.ptr());

		d->control = std::make_shared<MIPMaskControl>();

		InitConnections();
	}

	MIPMaskPresenter::~MIPMaskPresenter() = default;

	auto MIPMaskPresenter::InitConnections() -> void {
		connect(d->control.get(), &MIPMaskControl::sigUpsample, this, [this](int upsample) {
			d->upsample = upsample;
		});
		connect(d->control.get(), &MIPMaskControl::sigCapture, this, [this](QString path) {
			CaptureScreen(path);
		});
		connect(d->control.get(), &MIPMaskControl::sigDataRange, this, [this](double min, double max) {
			OnDataRange(min, max);
		});
		connect(d->control.get(), &MIPMaskControl::sigMaskStencil, this, [this](bool useStencil) {
			OnMaskStencil(useStencil);
		});
		connect(d->control.get(), &MIPMaskControl::sigMaskHighlightIdx, this, [this](int idx) {
			OnMaskHighlight(idx);
		});
		connect(d->control.get(), &MIPMaskControl::sigDepthColor, this, [this](bool useDepthColor) {
			OnDepthColor(useDepthColor);
		});
		connect(d->control.get(), &MIPMaskControl::sigColormap, this, [this](QList<float> colormap) {
			OnColormap(colormap);
		});
		connect(d->control.get(), &MIPMaskControl::sigDepthFactor, this, [this](double factor) {
			OnDepthFactor(factor);
		});
		connect(d->control.get(), &MIPMaskControl::sigGradientFactor, this, [this](double factor) {
			OnGradientFactor(factor);
		});
		connect(d->control.get(), &MIPMaskControl::sigCropRange, this, [this](double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
			OnCropRange(xmin, xmax, ymin, ymax, zmin, zmax);
		});
		connect(d->control.get(), &MIPMaskControl::sigJittering, this, [this](bool use) {
			d->renderer->ToggleJittering(use);
			});
		connect(d->control.get(), &MIPMaskControl::sigDefer, this, [this](bool use) {
			d->renderer->ToggleDeferredLighting(use);
			});
		connect(d->window.get(), &RenderWindow::sendMouseClick, this, [this]() {
			OnMouseClick();
		});
	}

	void MIPMaskPresenter::OnMouseClick() {
		setFocus();
	}

	auto MIPMaskPresenter::CaptureScreen(const QString& path, bool fullResolution) -> void {
		//set fixed resolution
		const auto volDataList = MedicalHelper::findNodes<SoVolumeData>(d->renderer->GetRootSwitch());
		if (fullResolution) {
			for (const auto volData : volDataList) {
				volData->ldmResourceParameters.getValue()->resolution = 0;
				volData->ldmResourceParameters.getValue()->fixedResolution = TRUE;
			}
		}
		d->window->SaveSnapShot(path, d->upsample);
		//restore resolution
		if (fullResolution) {
			for (const auto volData : volDataList) {
				volData->ldmResourceParameters.getValue()->fixedResolution = FALSE;
			}
		}
	}

	void MIPMaskPresenter::OnCropRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
		d->renderer->SetXRange(xmin, xmax);
		d->renderer->SetYRange(ymin, ymax);
		d->renderer->SetZRange(zmin, zmax);
	}

	void MIPMaskPresenter::OnDepthFactor(double factor) {
		if (factor > 0) {
			d->renderer->ToggleDepthEnhanced(true);
		} else {
			d->renderer->ToggleDepthEnhanced(false);
		}
		d->renderer->SetDepthEnhancementFactor(factor);
	}

	void MIPMaskPresenter::OnGradientFactor(double factor) {
		if (factor > 0) {
			d->renderer->ToggleGradientEnhanced(true);
		} else {
			d->renderer->ToggleGradientEnhanced(false);
		}
		d->renderer->SetGradientEnhancementFactor(factor);
	}

	void MIPMaskPresenter::OnColormap(QList<float> colormapArr) {
		d->renderer->SetColorMapArr(colormapArr);
	}

	void MIPMaskPresenter::OnDepthColor(bool useDepthColor) {
		d->renderer->ToggleColorDepth(useDepthColor);
	}

	void MIPMaskPresenter::OnMaskHighlight(int idx) {
		d->renderer->SetHighlight(idx);
	}

	void MIPMaskPresenter::OnMaskStencil(bool maskAsStencil) {
		d->renderer->ToggleMaskColor(!maskAsStencil);
	}

	void MIPMaskPresenter::OnDataRange(double min, double max) {
		d->renderer->SetDataRange(min, max);
	}

	auto MIPMaskPresenter::SetTitle(const QString& title) -> void {
		setWindowTitle(title);
		d->control->setWindowTitle(title + " Control");
	}

	auto MIPMaskPresenter::GetWindowList() const -> ViewList {
		return { d->control };
	}

	auto MIPMaskPresenter::IsPrimary(const ViewPtr& window) const -> bool {
		return true;
	}

	auto MIPMaskPresenter::IsAcceptable(const DataPtr& data) const -> bool {
		return true;
	}

	auto MIPMaskPresenter::GetDataList() const -> QStringList {
		QStringList datalist;
		if (false == d->imageName.isEmpty()) {
			datalist.append(d->imageName);
		}
		if (false == d->maskName.isEmpty()) {
			datalist.append(d->maskName);
		}
		return datalist;
	}

	auto MIPMaskPresenter::GetData(const QString& name) const -> DataPtr {
		if (d->imageName == name) {
			return d->imageData;
		}
		if (d->maskName == name) {
			return d->maskData;
		}
		return {};
	}

	auto MIPMaskPresenter::GetName(const DataPtr& data) const -> QString {
		if (d->imageData == data) {
			return d->imageName;
		}
		if (d->maskData == data) {
			return d->maskName;
		}
		return {};
	}

	auto MIPMaskPresenter::RemoveData(const QString& name) -> void {
		//DO NOT USE
	}

	auto MIPMaskPresenter::RenameData(const DataPtr& data, const QString& name) -> bool {
		const auto prevName = GetName(data);
		if (prevName.isEmpty()) {
			return false;
		}
		if (d->imageName == prevName) {
			d->imageName = name;
		} else if (d->maskName == prevName) {
			d->maskName = name;
		}

		return true;
	}

	auto MIPMaskPresenter::ClearData() -> void { }

	auto MIPMaskPresenter::Capture(const QString& filepath) -> void { }

	auto MIPMaskPresenter::AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool {
		if (portName == "ReferencePort") {
			if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(data); d->AddReference3D(image)) {
				d->ht3dExist = true;
			}
		} else if (portName == "List of Images") {
			if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(data)) {
				if (false == d->AddHT3D(image)) {
					return false;
				}
				d->imageData = data;
				d->imageName = name;
			} else if (const auto fl = std::dynamic_pointer_cast<Data::FL3D>(data)) {
				if (false == d->AddFL3D(fl)) {
					return false;
				}
				d->imageData = data;
				d->imageName = name;
			} else if (const auto floatImg = std::dynamic_pointer_cast<Data::Float3D>(data)) {
				if (false == d->AddFloat3D(floatImg)) {
					return false;
				}
				d->imageData = data;
				d->imageName = name;
			}
		} else if (portName == "List of Masks") {
			if (const auto binary = std::dynamic_pointer_cast<Data::BinaryMask3D>(data)) {
				if (false == d->AddMask3D(binary)) {
					return false;
				}
				d->maskData = data;
				d->maskName = name;
			} else if (const auto mask = std::dynamic_pointer_cast<Data::LabelMask3D>(data)) {
				if (false == d->AddLabel3D(mask)) {
					return false;
				}
				d->maskData = data;
				d->maskName = name;
			}
		} else {
			return false;
		}
		d->window->ResetView();
		return true;
	}
}
