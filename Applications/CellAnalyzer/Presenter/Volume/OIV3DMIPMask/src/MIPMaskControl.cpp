#include <QSettings>
#include <QFileDialog>

#include <iostream>

#include "ui_MIPMaskControl.h"
#include "MIPMaskControl.h"

#include <QColorDialog>
#include <VolumeViz/nodes/SoTransferFunction.h>

#include "QColormapCanvas.h"
#include "ColorMap.h"

namespace CellAnalyzer::Presenter::Volume {
	struct MIPMaskControl::Impl {
		Ui::LayerControl ui;

		QPixmap singletonePixmap;
		ColorMapPtr tf { nullptr };
		std::shared_ptr<QColormapCanvas> colormapWidget { nullptr };

		double divider { 1 };
		double offset { 0 };
		double singlestep { 0.01 };
		QColor singletone = QColor(255, 0, 0);
		int decimals { 2 };

		QMap<QString, QList<float>> predColormapArr;

		auto InitUI() -> void;
		auto BuildSingleTone() -> QList<float>;
		auto BuildPredefinedColormap() -> void;
	};

	auto MIPMaskControl::Impl::BuildPredefinedColormap() -> void {
		auto dummyTF = new SoTransferFunction;
		dummyTF->ref();
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::STANDARD;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Standard"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::TEMPERATURE;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Temperature"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::PHYSICS;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Physics"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::GLOW;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Glow"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::SEISMIC;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Seismic"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::BLUE_RED;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Blue_Red"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::BLUE_WHITE_RED;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Blue_White_Red"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		dummyTF->unref();
		tf->SetColormap(predColormapArr["Standard"]);
		colormapWidget->update();
	}

	auto MIPMaskControl::Impl::BuildSingleTone() -> QList<float> {
		const auto gamma = ui.gammaSpin->value();
		const auto colortone = singletone;
		QList<float> result;
		for (auto i = 0; i < 256; i++) {
			const auto ratio = static_cast<float>(i) / 255.0f;
			const auto gammaFactor = static_cast<float>(pow(ratio, gamma));
			result.append(static_cast<float>(colortone.redF()) * gammaFactor);
			result.append(static_cast<float>(colortone.greenF()) * gammaFactor);
			result.append(static_cast<float>(colortone.blueF()) * gammaFactor);
			result.append(gammaFactor);
		}
		return result;
	}

	auto MIPMaskControl::Impl::InitUI() -> void {
		ui.upsampleSpin->setRange(1, 5);
		ui.upsampleSpin->setSingleStep(1);
		ui.upsampleSpin->setValue(1);

		ui.dataRangeMin->setDecimals(decimals);
		ui.dataRangeMax->setDecimals(decimals);

		ui.depthFactorSpin->setRange(0, 1);
		ui.depthFactorSpin->setSingleStep(0.01);
		ui.depthFactorSpin->setValue(0);

		ui.gammaSpin->setRange(0.01, 2);
		ui.gammaSpin->setSingleStep(0.01);
		ui.gammaSpin->setValue(0);

		ui.gradientFactorSpin->setRange(0, 1);
		ui.gradientFactorSpin->setSingleStep(0.01);
		ui.gradientFactorSpin->setValue(0);

		ui.xCropMin->setRange(0, 0.99);
		ui.xCropMin->setDecimals(2);
		ui.xCropMin->setSingleStep(0.01);
		ui.xCropMin->setValue(0);
		ui.xCropMax->setRange(0.01, 1);
		ui.xCropMax->setDecimals(2);
		ui.xCropMax->setSingleStep(0.01);
		ui.xCropMax->setValue(1);

		ui.yCropMin->setRange(0, 0.99);
		ui.yCropMin->setDecimals(2);
		ui.yCropMin->setSingleStep(0.01);
		ui.yCropMin->setValue(0);
		ui.yCropMax->setRange(0.01, 1);
		ui.yCropMax->setDecimals(2);
		ui.yCropMax->setSingleStep(0.01);
		ui.yCropMax->setValue(1);

		ui.zCropMin->setRange(0, 0.99);
		ui.zCropMin->setDecimals(2);
		ui.zCropMin->setSingleStep(0.01);
		ui.zCropMin->setValue(0);
		ui.zCropMax->setRange(0.01, 1);
		ui.zCropMax->setDecimals(2);
		ui.zCropMax->setSingleStep(0.01);
		ui.zCropMax->setValue(1);

		ui.depthGroup->setEnabled(false);
		ui.cropGroup->setEnabled(false);
		ui.enhanceGroup->setEnabled(false);

		ui.captureBtn->setIcon(QIcon(":/Widget/CaptureCamera.svg"));
		ui.captureBtn->setIconSize(QSize(25, 25));
		ui.captureBtn->setToolTip("Take screenshot");

		colormapWidget = std::make_shared<QColormapCanvas>();

		auto layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		ui.colormapSocket->setLayout(layout);
		layout->addWidget(colormapWidget.get());

		tf = std::make_shared<ColorMap>();
		colormapWidget->SetColorTransferFunction(tf);

		ui.colormapCombo->addItem("Standard");
		ui.colormapCombo->addItem("Single Tone");
		ui.colormapCombo->addItem("Temperature");
		ui.colormapCombo->addItem("Physics");
		ui.colormapCombo->addItem("Glow");
		ui.colormapCombo->addItem("Seismic");
		ui.colormapCombo->addItem("Blue_Red");
		ui.colormapCombo->addItem("Blue_White_Red");
		ui.colormapCombo->addItem("Custom");

		singletonePixmap = QPixmap(25, 25);
		singletonePixmap.fill(singletone);
		ui.singletoneBtn->setIcon(QIcon(singletonePixmap));

		ui.maskModeCombo->addItem("Stencil");
		ui.maskModeCombo->addItem("Blend");

		ui.maskIdxSpin->setEnabled(false);
	}

	MIPMaskControl::MIPMaskControl(QWidget* parent) : QWidget(parent), d { new Impl } {
		d->ui.setupUi(this);
		d->InitUI();
		InitConnections();
		d->BuildPredefinedColormap();
	}

	MIPMaskControl::~MIPMaskControl() { }

	auto MIPMaskControl::InitConnections() -> void {
		//General Connection
		connect(d->ui.captureBtn, SIGNAL(clicked()), this, SLOT(OnCapture()));
		connect(d->ui.dataRangeMin, SIGNAL(valueChanged(double)), this, SLOT(OnDataMinSpin(double)));
		connect(d->ui.dataRangeMax, SIGNAL(valueChanged(double)), this, SLOT(OnDataMaxSpin(double)));
		connect(d->ui.maskModeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnMaskModeCombo(int)));
		connect(d->ui.maskHighlight, SIGNAL(clicked()), this, SLOT(OnMaskHighlight()));
		connect(d->ui.maskIdxSpin, SIGNAL(valueChanged(int)), this, SLOT(OnMaskIdxSpin(int)));
		connect(d->ui.upsampleSpin, SIGNAL(valueChanged(int)), this, SIGNAL(sigUpsample(int)));

		//Depth Colormap Connection
		connect(d->ui.depthChk, SIGNAL(clicked()), this, SLOT(OnDepthColorChk()));
		connect(d->ui.colormapCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnColormapCombo(int)));
		connect(d->colormapWidget.get(), SIGNAL(sigRender()), this, SLOT(OnCustomColormapChanged()));
		connect(d->ui.singletoneBtn, SIGNAL(clicked()), this, SLOT(OnSingletone()));
		connect(d->ui.gammaSpin, SIGNAL(valueChanged(double)), this, SLOT(OnGammaChanged(double)));

		//Enhancement Connection
		connect(d->ui.enhanceChk, SIGNAL(clicked()), this, SLOT(OnEnhanceChk()));
		connect(d->ui.depthFactorSpin, SIGNAL(valueChanged(double)), this, SLOT(OnDepthFactor(double)));
		connect(d->ui.gradientFactorSpin, SIGNAL(valueChanged(double)), this, SLOT(OnGradientFactor(double)));

		//Crop Connection
		connect(d->ui.cropChk, SIGNAL(clicked()), this, SLOT(OnCropChk()));
		connect(d->ui.xCropMin, SIGNAL(valueChanged(double)), this, SLOT(OnCropXmin(double)));
		connect(d->ui.xCropMax, SIGNAL(valueChanged(double)), this, SLOT(OnCropXmax(double)));
		connect(d->ui.yCropMin, SIGNAL(valueChanged(double)), this, SLOT(OnCropYmin(double)));
		connect(d->ui.yCropMax, SIGNAL(valueChanged(double)), this, SLOT(OnCropYmax(double)));
		connect(d->ui.zCropMin, SIGNAL(valueChanged(double)), this, SLOT(OnCropZmin(double)));
		connect(d->ui.zCropMax, SIGNAL(valueChanged(double)), this, SLOT(OnCropZmax(double)));

		connect(d->ui.jitteringChk, SIGNAL(clicked()), this, SLOT(OnJittering()));
		connect(d->ui.deferChk, SIGNAL(clicked()), this, SLOT(OnDefer()));
	}

	void MIPMaskControl::OnDefer() {
		emit sigDefer(d->ui.deferChk->isChecked());
	}

	void MIPMaskControl::OnJittering() {
		emit sigJittering(d->ui.jitteringChk->isChecked());
	}

	auto MIPMaskControl::SendCropSignal() -> void {
		const auto xmin = d->ui.xCropMin->value();
		const auto xmax = d->ui.xCropMax->value();
		const auto ymin = d->ui.yCropMin->value();
		const auto ymax = d->ui.yCropMax->value();
		const auto zmin = d->ui.zCropMin->value();
		const auto zmax = d->ui.zCropMax->value();
		emit sigCropRange(xmin, xmax, ymin, ymax, zmin, zmax);
	}

	void MIPMaskControl::OnCropChk() {
		if (d->ui.cropChk->isChecked()) {
			d->ui.cropGroup->setEnabled(true);
			SendCropSignal();
		} else {
			d->ui.cropGroup->setEnabled(false);
			emit sigCropRange(0, 1, 0, 1, 0, 1);
		}
	}

	void MIPMaskControl::OnCropXmin(double val) {
		if (val >= d->ui.xCropMax->value()) {
			d->ui.xCropMin->blockSignals(true);
			d->ui.xCropMin->setValue(val - 0.01);
			d->ui.xCropMin->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void MIPMaskControl::OnCropXmax(double val) {
		if (val <= d->ui.xCropMin->value()) {
			d->ui.xCropMax->blockSignals(true);
			d->ui.xCropMax->setValue(val + 0.01);
			d->ui.xCropMax->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void MIPMaskControl::OnCropYmin(double val) {
		if (val >= d->ui.yCropMax->value()) {
			d->ui.yCropMin->blockSignals(true);
			d->ui.yCropMin->setValue(val - 0.01);
			d->ui.yCropMin->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void MIPMaskControl::OnCropYmax(double val) {
		if (val <= d->ui.yCropMin->value()) {
			d->ui.yCropMax->blockSignals(true);
			d->ui.yCropMax->setValue(val + 0.01);
			d->ui.yCropMax->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void MIPMaskControl::OnCropZmin(double val) {
		if (val >= d->ui.zCropMax->value()) {
			d->ui.zCropMin->blockSignals(true);
			d->ui.zCropMin->setValue(val - 0.01);
			d->ui.zCropMin->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void MIPMaskControl::OnCropZmax(double val) {
		if (val <= d->ui.zCropMin->value()) {
			d->ui.zCropMax->blockSignals(true);
			d->ui.zCropMax->setValue(val + 0.01);
			d->ui.zCropMax->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void MIPMaskControl::OnDepthFactor(double factor) {
		emit sigDepthFactor(factor);
	}

	void MIPMaskControl::OnGradientFactor(double factor) {
		emit sigGradientFactor(factor);
	}

	void MIPMaskControl::OnEnhanceChk() {
		const auto useEnhancement = d->ui.enhanceChk->isChecked();
		double depthfactor { 0 };
		double gradientfactor { 0 };
		if (useEnhancement) {
			d->ui.enhanceGroup->setEnabled(true);
		} else {
			d->ui.enhanceGroup->setEnabled(false);
			depthfactor = d->ui.depthFactorSpin->value();
			gradientfactor = d->ui.gradientFactorSpin->value();
		}
		emit sigDepthFactor(depthfactor);
		emit sigGradientFactor(gradientfactor);
	}

	void MIPMaskControl::OnMaskIdxSpin(int idx) {
		emit sigMaskHighlightIdx(idx);
	}

	void MIPMaskControl::OnMaskHighlight() {
		const auto useHighlight = d->ui.maskHighlight->isChecked();
		if (useHighlight) {
			d->ui.maskIdxSpin->setEnabled(true);
			emit sigMaskHighlightIdx(d->ui.maskIdxSpin->value());
			return;
		}
		d->ui.maskIdxSpin->setEnabled(false);
		emit sigMaskHighlightIdx(-1);
	}

	void MIPMaskControl::OnMaskModeCombo(int) {
		const auto modeID = d->ui.maskModeCombo->currentText();
		if (modeID == "Stencil") {
			emit sigMaskStencil(true);
			return;
		}
		//Blend
		emit sigMaskStencil(false);
	}

	void MIPMaskControl::OnCapture() {
		const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/capture", QApplication::applicationDirPath()).toString();
		const auto path = QFileDialog::getSaveFileName(nullptr, tr("Save Screenshot as Image"), prev + "/", tr("Image files (*.png *.jpg *.tiff)"));
		if (path.isEmpty()) {
			return;
		}
		QSettings("Tomocube", "TomoAnalysis").setValue("recent/capture", path);

		emit sigCapture(path);
	}

	void MIPMaskControl::OnDataMinSpin(double val) {
		if (val >= d->ui.dataRangeMax->value()) {
			d->ui.dataRangeMin->blockSignals(true);
			d->ui.dataRangeMin->setValue(val - d->singlestep);
			d->ui.dataRangeMin->blockSignals(false);
			return;
		}
		const auto textmin = d->ui.dataRangeMin->value();
		const auto textmax = d->ui.dataRangeMax->value();
		const auto realMin = (textmin - d->offset) * d->divider;
		const auto realMax = (textmax - d->offset) * d->divider;

		emit sigDataRange(realMin, realMax);
	}

	void MIPMaskControl::OnDataMaxSpin(double val) {
		if (val <= d->ui.dataRangeMin->value()) {
			d->ui.dataRangeMax->blockSignals(true);
			d->ui.dataRangeMax->setValue(val + d->singlestep);
			d->ui.dataRangeMax->blockSignals(false);
			return;
		}
		const auto textmin = d->ui.dataRangeMin->value();
		const auto textmax = d->ui.dataRangeMax->value();
		const auto realMin = (textmin - d->offset) * d->divider;
		const auto realMax = (textmax - d->offset) * d->divider;

		emit sigDataRange(realMin, realMax);
	}

	void MIPMaskControl::OnGammaChanged(double gamma) {
		Q_UNUSED(gamma);
		if (d->ui.colormapCombo->currentText() == "Single Tone") {
			const auto colormapArr = d->BuildSingleTone();
			d->tf->SetColormap(colormapArr);
			emit sigColormap(colormapArr);
		}
	}

	void MIPMaskControl::OnSingletone() {
		QColor new_col = QColorDialog::getColor(d->singletone, nullptr, "Select Color");
		if (new_col.isValid()) {
			d->singletonePixmap.fill(new_col);
			d->ui.singletoneBtn->setIcon(QIcon(d->singletonePixmap));

			const auto singletoneIdx = d->ui.colormapCombo->findText("Single Tone");

			d->ui.gammaSpin->setEnabled(true);
			d->ui.colormapCombo->blockSignals(true);
			d->ui.colormapCombo->setCurrentIndex(singletoneIdx);
			d->ui.colormapCombo->blockSignals(false);
			const auto arr = d->BuildSingleTone();
			d->tf->SetColormap(arr);
			d->colormapWidget->update();

			emit sigColormap(arr);
		}
	}

	void MIPMaskControl::OnCustomColormapChanged() {
		const auto colormapPtr = d->tf->GetDataPointer();
		QList<float> colormapArr;
		for (auto i = 0; i < 1024; i++) {
			colormapArr.append(*(colormapPtr + i));
		}
		emit sigColormap(colormapArr);
	}

	void MIPMaskControl::OnColormapCombo(int index) {
		const auto colormapID = d->ui.colormapCombo->itemText(index);
		d->ui.gammaSpin->setEnabled(false);
		d->colormapWidget->setEnabled(false);
		QList<float> colormapArr;
		if (colormapID == "Custom") {
			for (auto i = 0; i < 1024; i++) {
				colormapArr.append(1.0f);
			}
			d->colormapWidget->setEnabled(true);
		} else if (colormapID == "Single Tone") {
			d->ui.gammaSpin->setEnabled(true);
			colormapArr = d->BuildSingleTone();
		} else {
			colormapArr = d->predColormapArr[colormapID];
		}
		d->tf->SetColormap(colormapArr);
		d->colormapWidget->update();
		emit sigColormap(colormapArr);
	}

	void MIPMaskControl::OnDepthColorChk() {
		auto isDepthColor = d->ui.depthChk->isChecked();
		if (isDepthColor) {
			d->ui.depthGroup->setEnabled(true);
		} else {
			d->ui.depthGroup->setEnabled(false);
		}
		emit sigDepthColor(isDepthColor);
	}

	auto MIPMaskControl::SetMaxlabelIndex(int idx) -> void {
		d->ui.maskIdxSpin->blockSignals(true);
		d->ui.maskIdxSpin->setRange(1, idx);
		d->ui.maskIdxSpin->setValue(1);
		d->ui.maskIdxSpin->blockSignals(false);
	}

	auto MIPMaskControl::SetDataRangeConfig(int decimals, double divider, double offset, double singlestep) -> void {
		d->decimals = decimals;
		d->divider = divider;
		d->offset = offset;
		d->singlestep = singlestep;

		d->ui.dataRangeMin->blockSignals(true);
		d->ui.dataRangeMax->blockSignals(true);

		d->ui.dataRangeMin->setDecimals(decimals);
		d->ui.dataRangeMin->setSingleStep(singlestep);

		d->ui.dataRangeMax->setDecimals(decimals);
		d->ui.dataRangeMax->setSingleStep(singlestep);

		d->ui.dataRangeMin->blockSignals(false);
		d->ui.dataRangeMax->blockSignals(false);
	}

	auto MIPMaskControl::SetDataMinMax(double min, double max) -> void {
		const auto textmin = min / d->divider + d->offset;
		const auto textmax = max / d->divider + d->offset;

		d->ui.dataRangeMin->blockSignals(true);
		d->ui.dataRangeMax->blockSignals(true);

		d->ui.dataRangeMin->setRange(textmin, textmax - d->singlestep);
		d->ui.dataRangeMax->setRange(textmin + d->singlestep, textmax);

		d->ui.dataRangeMin->setValue(textmin);
		d->ui.dataRangeMax->setValue(textmax);

		d->ui.dataRangeMin->blockSignals(false);
		d->ui.dataRangeMax->blockSignals(false);
	}

	auto MIPMaskControl::SetChannelIndex(int idx) -> void {
		if (idx == 0) {//Blue
			d->singletone = QColor(0, 0, 255);
		} else if (idx == 1) {//Green
			d->singletone = QColor(0, 255, 0);
		} else {//Red
			d->singletone = QColor(255, 0, 0);
		}
		d->singletonePixmap.fill(d->singletone);
		d->ui.singletoneBtn->setIcon(QIcon(d->singletonePixmap));
	}

	auto MIPMaskControl::SetImageType(DataFlag type) -> void {
		if (type == DataFlag::HT) {
			//DO NOTHING
		} else if (type == DataFlag::FL) {
			const auto singletoneIdx = d->ui.colormapCombo->findText("Single Tone");
			d->ui.colormapCombo->setCurrentIndex(singletoneIdx);
		} else {//Float Image
			//DO NOTHING
		}
	}

}
