#include "SliceOIVWindow.h"

#include <Inventor/events/SoKeyboardEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Medical/helpers/MedicalHelper.h>
#include <VolumeViz/nodes/SoVolumeData.h>

#include <OivActivator.h>

#include <Inventor/Qt/SoQt.h>

namespace CellAnalyzer::Presenter::Slice {
	struct SliceOIVWindow::Impl {
		bool right_mouse_pressed { false };
		SbVec2f right_stamp { SbVec2f(0, 0) };
		float prev_scale { 1 };

		bool left_mouse_pressed { false };
		bool middle_mouse_pressed { false };
	};

	SliceOIVWindow::SliceOIVWindow(QWidget* parent) : QOivRenderWindow(parent, true), d { new Impl } {
		//Temporal
		if (false == SoQt::isInitialized()) {
			QStringList OivKeys;
			OivKeys.append("License OpenInventor 10.12 1-Jan-0 0 HY:PKW=I>@C` \"APP-TOMOANALYSIS\"");
			OivKeys.append("License VolumeVizLDM 10.12 1-Jan-0 0 JDJ]=[<:?LB[ \"APP-TOMOANALYSIS\"");
			OivKeys.append("License ImageViz 10.12 1-Jan-0 0 BCVFNLNZOTPQ \"APP-TOMOANALYSIS\"");
			OivActivator::Activate(OivKeys);

			SoQt::init(this);
		}
		if (false == SoVolumeRendering::isInitialized())
			SoVolumeRendering::init();
		setDefaultWindowType();
	}

	SliceOIVWindow::~SliceOIVWindow() { }

	auto SliceOIVWindow::setDefaultWindowType() -> void {
		//set default gradient background color
		const SbVec3f start_color = { 0.0, 0.0, 0.0 };
		const SbVec3f end_color = { 0.0, 0.0, 0.0 };
		setGradientBackground(start_color, end_color);

		//set default camera type
		setCameraType(true);//orthographic
	}
	
	auto SliceOIVWindow::MouseButtonEvent(SoEventCallback* node) -> void {
		if (false == isNavigation())
			return;
		const auto mouseButton = reinterpret_cast<const SoMouseButtonEvent*>(node->getEvent());
		const auto vr = getViewportRegion();
		const auto norm_pos = mouseButton->getNormalizedPosition(vr);
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1))
			d->left_mouse_pressed = true;
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1))
			d->left_mouse_pressed = false;
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
			d->right_mouse_pressed = true;
			d->right_stamp = norm_pos;
			d->prev_scale = 1.0;
		}
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
			d->right_mouse_pressed = false;
			d->right_stamp = SbVec2f(0, 0);
			d->prev_scale = 1.0;
		}
		if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
			d->middle_mouse_pressed = true;
			startPan(norm_pos);
		}
		if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2))
			d->middle_mouse_pressed = false;
	}

	auto SliceOIVWindow::MouseMoveEvent(SoEventCallback* node) -> void {
		if (false == isNavigation())
			return;
		const auto moveEvent = reinterpret_cast<const SoLocation2Event*>(node->getEvent());
		const auto vr = getViewportRegion();
		const auto norm_pos = moveEvent->getNormalizedPosition(vr);
		emit sigMousePos(norm_pos[0], norm_pos[1]);
		if (d->right_mouse_pressed) {
			auto diff = norm_pos[1] - d->right_stamp[1];
			const auto factor = 1.0f - diff * 1.5f;
			if (factor > 0) {
				getCamera()->scaleHeight(factor / d->prev_scale);
				d->prev_scale = factor;
			}
		}
		if (d->left_mouse_pressed)
			node->setHandled();
		if (d->middle_mouse_pressed)
			panCamera(norm_pos);
	}

	auto SliceOIVWindow::MouseWheelEvent(SoEventCallback* node) -> void {
		const auto mouseWheel = reinterpret_cast<const SoMouseWheelEvent*>(node->getEvent());
		const auto delta = mouseWheel->getDelta() > 0 ? 1 : -1;
		emit sigMouseWheel(delta);
	}

	auto SliceOIVWindow::KeyboardEvent(SoEventCallback* node) -> void {
		const auto keyEvent = node->getEvent();
		if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
			if (false == isNavigation()) {
				setInteractionMode(true);
				setAltPressed(true);
			}
			node->setHandled();
		}
		if (SoKeyboardEvent::isKeyReleaseEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
			if (isAltPressed()) {
				setInteractionMode(false);
				setAltPressed(false);
			}
			node->setHandled();
		}
	}

	auto SliceOIVWindow::ResetView2D(Direction2D direction) -> void {
		MedicalHelper::Axis ax;
		auto root = getSceneGraph();
		const auto volData = MedicalHelper::find<SoVolumeData>(root);//find any volume Data
		if (!volData)
			return;

		float slack;
		auto dims = volData->getDimension();
		auto spacing = volData->getVoxelSize();

		float x_len = dims[0] * spacing[0];
		float y_len = dims[1] * spacing[1];
		float z_len = dims[2] * spacing[2];

		auto wh = static_cast<float>(size().height());
		auto ww = static_cast<float>(size().width());
		auto windowSlack = wh > ww ? ww / wh : wh / ww;

		if (direction._to_integral() == Direction2D::XY) {
			ax = MedicalHelper::AXIAL;
			slack = y_len < x_len ? y_len / x_len : x_len / y_len;
		} else if (direction._to_integral() == Direction2D::YZ) {
			ax = MedicalHelper::SAGITTAL;
			slack = z_len / y_len;
		} else if (direction._to_integral() == Direction2D::XZ) {
			ax = MedicalHelper::CORONAL;
			slack = z_len / x_len;
		} else
			return;

		auto largerLen = (x_len > y_len) ? x_len : y_len;

		if (windowSlack > slack && windowSlack > 0)
			slack = windowSlack > 1 ? 1.0 : windowSlack;
		MedicalHelper::orientView(ax, getCamera(), volData, slack);
		if (ax == MedicalHelper::SAGITTAL)
			getCamera()->orientation.setValue(SbVec3f(0, 1, 0), M_PI);
		if (ax == MedicalHelper::CORONAL)
			getCamera()->orientation.setValue(SbVec3f(1, 0, 0), M_PI * 2);
	}
}
