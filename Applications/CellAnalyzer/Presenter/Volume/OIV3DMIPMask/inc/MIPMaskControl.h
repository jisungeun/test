#pragma once

#include <enum.h>

#include <QWidget>

#include "IData.h"
#include "IView.h"

namespace CellAnalyzer::Presenter::Volume {
	class MIPMaskControl final : public QWidget, public IView {
		Q_OBJECT

	public:
		explicit MIPMaskControl(QWidget* parent = nullptr);
		~MIPMaskControl();

		auto SetImageType(DataFlag type) -> void;
		auto SetChannelIndex(int idx) -> void;

		auto SetDataRangeConfig(int decimals, double divider, double offset, double singlestep) -> void;
		auto SetDataMinMax(double min, double max) -> void;
		auto SetMaxlabelIndex(int idx) -> void;

	signals:
		void sigDataRange(double, double);
		void sigMaskStencil(bool);
		void sigMaskHighlightIdx(int);

		void sigDepthColor(bool);
		void sigColormap(QList<float>);

		void sigDepthFactor(double);
		void sigGradientFactor(double);

		void sigCropRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);

		void sigCapture(QString);
		void sigUpsample(int);

		void sigJittering(bool);
		void sigDefer(bool);

	protected slots:
		void OnDataMinSpin(double);
		void OnDataMaxSpin(double);
		void OnMaskModeCombo(int);
		void OnMaskHighlight();
		void OnMaskIdxSpin(int);

		void OnDepthColorChk();
		void OnColormapCombo(int);
		void OnCustomColormapChanged();
		void OnSingletone();
		void OnGammaChanged(double);

		void OnEnhanceChk();
		void OnDepthFactor(double);
		void OnGradientFactor(double);

		void OnCropChk();
		void OnCropXmin(double);
		void OnCropXmax(double);
		void OnCropYmin(double);
		void OnCropYmax(double);
		void OnCropZmin(double);
		void OnCropZmax(double);

		void OnCapture();
		void OnJittering();
		void OnDefer();

	protected:
		auto InitConnections() -> void;
		auto SendCropSignal() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
