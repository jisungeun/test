#pragma once

#include <QWidget>

#include <IPresenter.h>

namespace CellAnalyzer::Presenter::Volume {
	class TF1d3dPresenter final : public IPresenter, public QWidget {
	public:
		TF1d3dPresenter(QWidget* parent = nullptr);
		~TF1d3dPresenter() override;

		auto SetTitle(const QString& title) -> void override;

		auto GetWindowList() const -> ViewList override;
		auto IsPrimary(const ViewPtr& window) const -> bool override;

		auto IsAcceptable(const DataPtr& data) const -> bool override;
		auto GetDataList() const -> QStringList override;
		auto GetData(const QString& name) const -> DataPtr override;
		auto GetName(const DataPtr& data) const -> QString override;

		auto AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool override;
		auto RenameData(const DataPtr& data, const QString& name) -> bool override;
		auto RemoveData(const QString& name) -> void override;
		auto ClearData() -> void override;

		auto Capture(const QString& filepath) -> void override;

	protected:
		auto InitConnections() -> void;
		void OnDataRange(double min, double max);		
		void OnColormap(QList<float> colormapArr);
		void OnCropRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);
		void OnMouseClick();

	private:
		auto CaptureScreen(const QString& path, bool fullResolution = false) -> void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};

	DECLARE_PRESENTER_START(TF1d3dPresenter) {
		Q_OBJECT
		DECLARE_PRESENTER_END(TF1d3dPresenter, Volume, DEFAULT_PATH)
	};
}
