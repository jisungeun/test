#pragma once

#include <enum.h>

#include <QWidget>

#include "IData.h"
#include "IView.h"

namespace CellAnalyzer::Presenter::Volume {
	class TF1d3dControl final : public QWidget, public IView {
		Q_OBJECT
	public:
		explicit TF1d3dControl(QWidget* parent = nullptr);
		~TF1d3dControl();

		auto SetImageType(DataFlag type) -> void;
		auto SetChannelIndex(int idx) -> void;

		auto SetDataRangeConfig(int decimals, double divider, double offset, double singlestep) -> void;
		auto SetDataMinMax(double min, double max) -> void;
		auto UpdateHistogram(QList<int> value)->void;

		auto GetCurColormap()->QList<float>;

	signals:
		void sigDataRange(double, double);

		void sigColormap(QList<float>);
		
		void sigCropRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);

		void sigCapture(QString);
		void sigUpsample(int);

		void sigJittering(bool);
		void sigDefer(bool);

	protected slots:
		void OnDataMinSpin(double);
		void OnDataMaxSpin(double);

		void OnColormapCombo(int);
		void OnCustomColormapChanged();
		void OnSingletone();
		void OnGammaChanged(double);
		
		void OnCropChk();
		void OnCropXmin(double);
		void OnCropXmax(double);
		void OnCropYmin(double);
		void OnCropYmax(double);
		void OnCropZmin(double);
		void OnCropZmax(double);

		void OnCapture();
		void OnCustomOpacity();
		void OnTransp();

		void OnJittering();
		void OnDefer();

	protected:
		auto InitConnections() -> void;
		auto SendCropSignal() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
