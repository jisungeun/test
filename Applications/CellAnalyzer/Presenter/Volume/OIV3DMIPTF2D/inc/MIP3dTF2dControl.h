#pragma once

#include <enum.h>

#include <QWidget>

#include "IData.h"
#include "IView.h"

class SoSeparator;
class SoVolumeData;

namespace CellAnalyzer::Presenter::Volume {
	class MIP3dTF2dControl final : public QWidget, public IView {
		Q_OBJECT
	public:
		explicit MIP3dTF2dControl(QWidget* parent = nullptr);
		~MIP3dTF2dControl();
		
		auto SetDataRangeConfig(int decimals, double divider, double offset, double singlestep) -> void;
		auto SetDataMinMax(double min, double max) -> void;

		auto SetCurrentVolume(SoVolumeData* vol, double min, double max, bool isFloat)->void;
		auto GetHiddenSepRoot()->SoSeparator*;

	signals:
		void sigDataRange(double, double);

		void sigTFColor(bool);
		void sigTFOpacity(bool);

		void sigDepthFactor(double);
		void sigGradientFactor(double);

		void sigCropRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);

		void sigCapture(QString);
		void sigUpsample(int);

		void sigJittering(bool);
		void sigDefer(bool);

	protected slots:
		void OnDataMinSpin(double);
		void OnDataMaxSpin(double);
				
		void OnCropChk();
		void OnCropXmin(double);
		void OnCropXmax(double);
		void OnCropYmin(double);
		void OnCropYmax(double);
		void OnCropZmin(double);
		void OnCropZmax(double);

		void OnCapture();

		void OnUseColor();
		void OnCustomOpacity();

		void OnJittering();
		void OnDefer();

		void OnUpdatePolygon();
		void OnCanvasChanged();
		void OnTFImageChanged();

		void OnCurItemChanged(int);

		void OnTranspSpinChanged(double);
		void OnTranspSliderChanged(int);

		void OnEnhanceChk();
		void OnDepthFactor(double);
		void OnGradientFactor(double);

	protected:
		auto InitConnections() -> void;
		auto SendCropSignal() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
