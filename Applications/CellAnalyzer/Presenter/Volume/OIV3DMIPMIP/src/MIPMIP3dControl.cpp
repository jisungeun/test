#include <QSettings>
#include <QFileDialog>

#include <iostream>

#include "ui_MIPMIP3dControl.h"
#include "MIPMIP3dControl.h"

#include <QColorDialog>
#include <VolumeViz/nodes/SoTransferFunction.h>

#include "QColormapCanvas.h"
#include "ColorMap.h"

namespace CellAnalyzer::Presenter::Volume {
	struct MIPMIP3dControl::Impl {
		Ui::LayerControl ui;

		QPixmap singletonePixmap;
		ColorMapPtr tf { nullptr };
		std::shared_ptr<QColormapCanvas> colormapWidget { nullptr };

		QPixmap singletonePixmap2;
		ColorMapPtr tf2{ nullptr };
		std::shared_ptr<QColormapCanvas> colormapWidget2{ nullptr };

		double divider { 1 };
		double offset { 0 };
		double singlestep { 0.01 };
		QColor singletone = QColor(255, 0, 0);
		int decimals { 2 };

		double divider2{ 1 };
		double offset2{ 0 };
		double singlestep2{ 0.01 };
		QColor singletone2 = QColor(0, 255, 0);
		int decimals2{ 2 };

		QMap<QString, QList<float>> predColormapArr;

		auto InitUI() -> void;
		auto BuildSingleTone() -> QList<float>;
		auto BuildSingleTone2()->QList<float>;
		auto BuildPredefinedColormap() -> void;
	};

	auto MIPMIP3dControl::Impl::BuildPredefinedColormap() -> void {
		auto dummyTF = new SoTransferFunction;
		dummyTF->ref();
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Intensity"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::STANDARD;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Standard"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::TEMPERATURE;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Temperature"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::PHYSICS;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Physics"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::GLOW;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Glow"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::SEISMIC;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Seismic"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::BLUE_RED;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Blue_Red"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		{
			dummyTF->predefColorMap = SoTransferFunction::PredefColorMap::BLUE_WHITE_RED;
			auto p = dummyTF->actualColorMap.startEditing();
			QList<float> arr;
			for (auto i = 0; i < 256 * 4; i++) {
				arr.append(*(p + i));
			}
			predColormapArr["Blue_White_Red"] = arr;
			dummyTF->actualColorMap.finishEditing();
		}
		dummyTF->unref();
		tf->SetColormap(predColormapArr["Intensity"]);
		tf2->SetColormap(predColormapArr["Intensity"]);
		colormapWidget->update();
		colormapWidget2->update();
	}

	auto MIPMIP3dControl::Impl::BuildSingleTone() -> QList<float> {
		const auto gamma = ui.gammaSpin->value();
		const auto colortone = singletone;
		QList<float> result;
		for (auto i = 0; i < 256; i++) {
			const auto ratio = static_cast<float>(i) / 255.0f;
			const auto gammaFactor = static_cast<float>(pow(ratio, gamma));
			result.append(static_cast<float>(colortone.redF()) * gammaFactor);
			result.append(static_cast<float>(colortone.greenF()) * gammaFactor);
			result.append(static_cast<float>(colortone.blueF()) * gammaFactor);
			result.append(gammaFactor);
		}
		return result;
	}

	auto MIPMIP3dControl::Impl::BuildSingleTone2() -> QList<float> {
		const auto gamma = ui.gammaSpin2->value();
		const auto colortone = singletone2;
		QList<float> result;
		for (auto i = 0; i < 256; i++) {
			const auto ratio = static_cast<float>(i) / 255.0f;
			const auto gammaFactor = static_cast<float>(pow(ratio, gamma));
			result.append(static_cast<float>(colortone.redF()) * gammaFactor);
			result.append(static_cast<float>(colortone.greenF()) * gammaFactor);
			result.append(static_cast<float>(colortone.blueF()) * gammaFactor);
			result.append(gammaFactor);
		}
		return result;
	}

	auto MIPMIP3dControl::Impl::InitUI() -> void {
		ui.mipChk->setChecked(true);
		ui.data1Chk->setChecked(true);
		ui.data2Chk->setChecked(true);

		ui.upsampleSpin->setRange(1, 5);
		ui.upsampleSpin->setSingleStep(1);
		ui.upsampleSpin->setValue(1);

		ui.dataRangeMin->setDecimals(decimals);
		ui.dataRangeMax->setDecimals(decimals);

		ui.dataRangeMin2->setDecimals(decimals2);
		ui.dataRangeMax2->setDecimals(decimals2);

		ui.depthFactorSpin->setRange(0, 1);
		ui.depthFactorSpin->setSingleStep(0.01);
		ui.depthFactorSpin->setValue(0);

		ui.gammaSpin->setRange(0.01, 2);
		ui.gammaSpin->setSingleStep(0.01);
		ui.gammaSpin->setValue(1);
		ui.gammaSpin->setEnabled(false);

		ui.gammaSpin2->setRange(0.01, 2);
		ui.gammaSpin2->setSingleStep(0.01);
		ui.gammaSpin2->setValue(1);
		ui.gammaSpin2->setEnabled(false);

		ui.gradientFactorSpin->setRange(0, 1);
		ui.gradientFactorSpin->setSingleStep(0.01);
		ui.gradientFactorSpin->setValue(0);

		ui.xCropMin->setRange(0, 0.99);
		ui.xCropMin->setDecimals(2);
		ui.xCropMin->setSingleStep(0.01);
		ui.xCropMin->setValue(0);
		ui.xCropMax->setRange(0.01, 1);
		ui.xCropMax->setDecimals(2);
		ui.xCropMax->setSingleStep(0.01);
		ui.xCropMax->setValue(1);

		ui.yCropMin->setRange(0, 0.99);
		ui.yCropMin->setDecimals(2);
		ui.yCropMin->setSingleStep(0.01);
		ui.yCropMin->setValue(0);
		ui.yCropMax->setRange(0.01, 1);
		ui.yCropMax->setDecimals(2);
		ui.yCropMax->setSingleStep(0.01);
		ui.yCropMax->setValue(1);

		ui.zCropMin->setRange(0, 0.99);
		ui.zCropMin->setDecimals(2);
		ui.zCropMin->setSingleStep(0.01);
		ui.zCropMin->setValue(0);
		ui.zCropMax->setRange(0.01, 1);
		ui.zCropMax->setDecimals(2);
		ui.zCropMax->setSingleStep(0.01);
		ui.zCropMax->setValue(1);
				
		ui.cropGroup->setEnabled(false);
		ui.enhanceGroup->setEnabled(false);

		ui.captureBtn->setIcon(QIcon(":/Widget/CaptureCamera.svg"));
		ui.captureBtn->setIconSize(QSize(25, 25));
		ui.captureBtn->setToolTip("Take screenshot");

		colormapWidget = std::make_shared<QColormapCanvas>();

		auto layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		ui.colormapSocket->setLayout(layout);
		layout->addWidget(colormapWidget.get());

		tf = std::make_shared<ColorMap>();
		colormapWidget->SetColorTransferFunction(tf);

		colormapWidget2 = std::make_shared<QColormapCanvas>();

		auto layout2 = new QVBoxLayout;
		layout2->setContentsMargins(0, 0, 0, 0);
		layout2->setSpacing(0);

		ui.colormapSocket2->setLayout(layout2);
		layout2->addWidget(colormapWidget2.get());

		tf2 = std::make_shared<ColorMap>();
		colormapWidget2->SetColorTransferFunction(tf2);

		ui.colormapCombo->addItem("Intensity");
		ui.colormapCombo->addItem("Standard");
		ui.colormapCombo->addItem("Single Tone");
		ui.colormapCombo->addItem("Temperature");
		ui.colormapCombo->addItem("Physics");
		ui.colormapCombo->addItem("Glow");
		ui.colormapCombo->addItem("Seismic");
		ui.colormapCombo->addItem("Blue_Red");
		ui.colormapCombo->addItem("Blue_White_Red");
		ui.colormapCombo->addItem("Custom");

		ui.colormapCombo2->addItem("Intensity");
		ui.colormapCombo2->addItem("Standard");
		ui.colormapCombo2->addItem("Single Tone");
		ui.colormapCombo2->addItem("Temperature");
		ui.colormapCombo2->addItem("Physics");
		ui.colormapCombo2->addItem("Glow");
		ui.colormapCombo2->addItem("Seismic");
		ui.colormapCombo2->addItem("Blue_Red");
		ui.colormapCombo2->addItem("Blue_White_Red");
		ui.colormapCombo2->addItem("Custom");

		singletonePixmap = QPixmap(25, 25);
		singletonePixmap.fill(singletone);
		ui.singletoneBtn->setIcon(QIcon(singletonePixmap));

		singletonePixmap2 = QPixmap(25, 25);
		singletonePixmap2.fill(singletone2);
		ui.singletoneBtn2->setIcon(QIcon(singletonePixmap2));
	}

	MIPMIP3dControl::MIPMIP3dControl(QWidget* parent) : QWidget(parent), d { new Impl } {
		d->ui.setupUi(this);
		d->InitUI();
		InitConnections();
		d->BuildPredefinedColormap();
	}

	MIPMIP3dControl::~MIPMIP3dControl() { }

	auto MIPMIP3dControl::InitConnections() -> void {
		//General Connection
		connect(d->ui.dataRangeMin, SIGNAL(valueChanged(double)), this, SLOT(OnDataMinSpin(double)));
		connect(d->ui.dataRangeMax, SIGNAL(valueChanged(double)), this, SLOT(OnDataMaxSpin(double)));

		connect(d->ui.dataRangeMin2, SIGNAL(valueChanged(double)), this, SLOT(OnDataMinSpin2(double)));
		connect(d->ui.dataRangeMax2, SIGNAL(valueChanged(double)), this, SLOT(OnDataMaxSpin2(double)));

		connect(d->ui.captureBtn, SIGNAL(clicked()), this, SLOT(OnCapture()));
		connect(d->ui.upsampleSpin, SIGNAL(valueChanged(int)), this, SIGNAL(sigUpsample(int)));

		//Depth Colormap Connection		
		connect(d->ui.colormapCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnColormapCombo(int)));
		connect(d->ui.colormapCombo2, SIGNAL(currentIndexChanged(int)), this, SLOT(OnColormapCombo2(int)));

		connect(d->colormapWidget.get(), SIGNAL(sigRender()), this, SLOT(OnCustomColormapChanged()));
		connect(d->colormapWidget2.get(), SIGNAL(sigRender()), this, SLOT(OnCustomColormapChanged2()));

		connect(d->ui.singletoneBtn, SIGNAL(clicked()), this, SLOT(OnSingletone()));
		connect(d->ui.singletoneBtn2, SIGNAL(clicked()), this, SLOT(OnSingletone2()));

		connect(d->ui.gammaSpin, SIGNAL(valueChanged(double)), this, SLOT(OnGammaChanged(double)));
		connect(d->ui.gammaSpin2, SIGNAL(valueChanged(double)), this, SLOT(OnGammaChanged2(double)));

		//Enhancement Connection
		connect(d->ui.enhanceChk, SIGNAL(clicked()), this, SLOT(OnEnhanceChk()));
		connect(d->ui.depthFactorSpin, SIGNAL(valueChanged(double)), this, SLOT(OnDepthFactor(double)));
		connect(d->ui.gradientFactorSpin, SIGNAL(valueChanged(double)), this, SLOT(OnGradientFactor(double)));

		//Crop Connection
		connect(d->ui.cropChk, SIGNAL(clicked()), this, SLOT(OnCropChk()));
		connect(d->ui.xCropMin, SIGNAL(valueChanged(double)), this, SLOT(OnCropXmin(double)));
		connect(d->ui.xCropMax, SIGNAL(valueChanged(double)), this, SLOT(OnCropXmax(double)));
		connect(d->ui.yCropMin, SIGNAL(valueChanged(double)), this, SLOT(OnCropYmin(double)));
		connect(d->ui.yCropMax, SIGNAL(valueChanged(double)), this, SLOT(OnCropYmax(double)));
		connect(d->ui.zCropMin, SIGNAL(valueChanged(double)), this, SLOT(OnCropZmin(double)));
		connect(d->ui.zCropMax, SIGNAL(valueChanged(double)), this, SLOT(OnCropZmax(double)));

		connect(d->ui.mipChk, SIGNAL(clicked()), this, SLOT(OnMIP()));
		connect(d->ui.data1Chk, SIGNAL(clicked()), this, SLOT(OnData1()));
		connect(d->ui.data2Chk, SIGNAL(clicked()), this, SLOT(OnData2()));

		d->ui.jitteringChk->setChecked(true);
		connect(d->ui.jitteringChk, SIGNAL(clicked()), this, SLOT(OnJittering()));
		connect(d->ui.deferChk, SIGNAL(clicked()), this, SLOT(OnDefer()));
	}

	void MIPMIP3dControl::OnDefer() {
		emit sigDefer(d->ui.deferChk->isChecked());
	}

	void MIPMIP3dControl::OnJittering() {
		emit sigJittering(d->ui.jitteringChk->isChecked());
	}

	void MIPMIP3dControl::OnMIP() {
		emit sigUseMIP(d->ui.mipChk->isChecked());
	}

	void MIPMIP3dControl::OnData1() {
		emit sigUseData1(d->ui.data1Chk->isChecked());
	}

	void MIPMIP3dControl::OnData2() {
		emit sigUseData2(d->ui.data2Chk->isChecked());
	}

	auto MIPMIP3dControl::SendCropSignal() -> void {
		const auto xmin = d->ui.xCropMin->value();
		const auto xmax = d->ui.xCropMax->value();
		const auto ymin = d->ui.yCropMin->value();
		const auto ymax = d->ui.yCropMax->value();
		const auto zmin = d->ui.zCropMin->value();
		const auto zmax = d->ui.zCropMax->value();
		emit sigCropRange(xmin, xmax, ymin, ymax, zmin, zmax);
	}

	void MIPMIP3dControl::OnCropChk() {
		if (d->ui.cropChk->isChecked()) {
			d->ui.cropGroup->setEnabled(true);
			SendCropSignal();
		} else {
			d->ui.cropGroup->setEnabled(false);
			emit sigCropRange(0, 1, 0, 1, 0, 1);
		}
	}

	void MIPMIP3dControl::OnCropXmin(double val) {
		if (val >= d->ui.xCropMax->value()) {
			d->ui.xCropMin->blockSignals(true);
			d->ui.xCropMin->setValue(val - 0.01);
			d->ui.xCropMin->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void MIPMIP3dControl::OnCropXmax(double val) {
		if (val <= d->ui.xCropMin->value()) {
			d->ui.xCropMax->blockSignals(true);
			d->ui.xCropMax->setValue(val + 0.01);
			d->ui.xCropMax->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void MIPMIP3dControl::OnCropYmin(double val) {
		if (val >= d->ui.yCropMax->value()) {
			d->ui.yCropMin->blockSignals(true);
			d->ui.yCropMin->setValue(val - 0.01);
			d->ui.yCropMin->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void MIPMIP3dControl::OnCropYmax(double val) {
		if (val <= d->ui.yCropMin->value()) {
			d->ui.yCropMax->blockSignals(true);
			d->ui.yCropMax->setValue(val + 0.01);
			d->ui.yCropMax->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void MIPMIP3dControl::OnCropZmin(double val) {
		if (val >= d->ui.zCropMax->value()) {
			d->ui.zCropMin->blockSignals(true);
			d->ui.zCropMin->setValue(val - 0.01);
			d->ui.zCropMin->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void MIPMIP3dControl::OnCropZmax(double val) {
		if (val <= d->ui.zCropMin->value()) {
			d->ui.zCropMax->blockSignals(true);
			d->ui.zCropMax->setValue(val + 0.01);
			d->ui.zCropMax->blockSignals(false);
			return;
		}
		SendCropSignal();
	}

	void MIPMIP3dControl::OnDepthFactor(double factor) {
		emit sigDepthFactor(factor);
	}

	void MIPMIP3dControl::OnGradientFactor(double factor) {
		emit sigGradientFactor(factor);
	}

	void MIPMIP3dControl::OnEnhanceChk() {
		const auto useEnhancement = d->ui.enhanceChk->isChecked();
		double depthfactor { 0 };
		double gradientfactor { 0 };
		if (useEnhancement) {
			d->ui.enhanceGroup->setEnabled(true);
			depthfactor = d->ui.depthFactorSpin->value();
			gradientfactor = d->ui.gradientFactorSpin->value();
		} else {
			d->ui.enhanceGroup->setEnabled(false);			
		}
		emit sigDepthFactor(depthfactor);
		emit sigGradientFactor(gradientfactor);
	}

	void MIPMIP3dControl::OnCapture() {
		const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/capture", QApplication::applicationDirPath()).toString();
		const auto path = QFileDialog::getSaveFileName(nullptr, tr("Save Screenshot as Image"), prev + "/", tr("Image files (*.png *.jpg *.tiff)"));
		if (path.isEmpty()) {
			return;
		}
		QSettings("Tomocube", "TomoAnalysis").setValue("recent/capture", path);

		emit sigCapture(path);
	}

	void MIPMIP3dControl::OnDataMinSpin(double val) {
		if (val >= d->ui.dataRangeMax->value()) {
			d->ui.dataRangeMin->blockSignals(true);
			d->ui.dataRangeMin->setValue(val - d->singlestep);
			d->ui.dataRangeMin->blockSignals(false);
			return;
		}
		const auto textmin = d->ui.dataRangeMin->value();
		const auto textmax = d->ui.dataRangeMax->value();
		const auto realMin = (textmin - d->offset) * d->divider;
		const auto realMax = (textmax - d->offset) * d->divider;

		emit sigDataRange(realMin, realMax);
	}

	void MIPMIP3dControl::OnDataMaxSpin(double val) {
		if (val <= d->ui.dataRangeMin->value()) {
			d->ui.dataRangeMax->blockSignals(true);
			d->ui.dataRangeMax->setValue(val + d->singlestep);
			d->ui.dataRangeMax->blockSignals(false);
			return;
		}
		const auto textmin = d->ui.dataRangeMin->value();
		const auto textmax = d->ui.dataRangeMax->value();
		const auto realMin = (textmin - d->offset) * d->divider;
		const auto realMax = (textmax - d->offset) * d->divider;

		emit sigDataRange(realMin, realMax);
	}

	void MIPMIP3dControl::OnDataMinSpin2(double val) {
		if (val >= d->ui.dataRangeMax2->value()) {
			d->ui.dataRangeMin2->blockSignals(true);
			d->ui.dataRangeMin2->setValue(val - d->singlestep2);
			d->ui.dataRangeMin2->blockSignals(false);
			return;
		}
		const auto textmin = d->ui.dataRangeMin2->value();
		const auto textmax = d->ui.dataRangeMax2->value();
		const auto realMin = (textmin - d->offset2) * d->divider2;
		const auto realMax = (textmax - d->offset2) * d->divider2;

		emit sigDataRange2(realMin, realMax);
	}

	void MIPMIP3dControl::OnDataMaxSpin2(double val) {
		if (val <= d->ui.dataRangeMin2->value()) {
			d->ui.dataRangeMax2->blockSignals(true);
			d->ui.dataRangeMax2->setValue(val + d->singlestep2);
			d->ui.dataRangeMax2->blockSignals(false);
			return;
		}
		const auto textmin = d->ui.dataRangeMin2->value();
		const auto textmax = d->ui.dataRangeMax2->value();
		const auto realMin = (textmin - d->offset2) * d->divider2;
		const auto realMax = (textmax - d->offset2) * d->divider2;

		emit sigDataRange2(realMin, realMax);
	}

	void MIPMIP3dControl::OnGammaChanged(double gamma) {
		Q_UNUSED(gamma);
		if (d->ui.colormapCombo->currentText() == "Single Tone") {
			const auto colormapArr = d->BuildSingleTone();
			d->tf->SetColormap(colormapArr);
			d->colormapWidget->update();
			emit sigColormap(colormapArr);
		}
	}

	void MIPMIP3dControl::OnGammaChanged2(double gamma) {
		Q_UNUSED(gamma);
		if (d->ui.colormapCombo2->currentText() == "Single Tone") {
			const auto colormapArr = d->BuildSingleTone2();
			d->tf2->SetColormap(colormapArr);
			d->colormapWidget2->update();
			emit sigColormap2(colormapArr);
		}
	}

	void MIPMIP3dControl::OnSingletone() {
		QColor new_col = QColorDialog::getColor(d->singletone, nullptr, "Select Color");
		if (new_col.isValid()) {
			d->singletonePixmap.fill(new_col);
			d->ui.singletoneBtn->setIcon(QIcon(d->singletonePixmap));
			d->singletone = new_col;
			const auto singletoneIdx = d->ui.colormapCombo->findText("Single Tone");

			d->ui.gammaSpin->setEnabled(true);
			d->ui.colormapCombo->blockSignals(true);
			d->ui.colormapCombo->setCurrentIndex(singletoneIdx);
			d->ui.colormapCombo->blockSignals(false);
			const auto arr = d->BuildSingleTone();
			d->tf->SetColormap(arr);
			d->colormapWidget->update();

			emit sigColormap(arr);
		}
	}

	void MIPMIP3dControl::OnSingletone2() {
		QColor new_col = QColorDialog::getColor(d->singletone, nullptr, "Select Color");
		if (new_col.isValid()) {
			d->singletonePixmap2.fill(new_col);
			d->ui.singletoneBtn2->setIcon(QIcon(d->singletonePixmap2));
			d->singletone2 = new_col;
			const auto singletoneIdx = d->ui.colormapCombo2->findText("Single Tone");

			d->ui.gammaSpin2->setEnabled(true);
			d->ui.colormapCombo2->blockSignals(true);
			d->ui.colormapCombo2->setCurrentIndex(singletoneIdx);
			d->ui.colormapCombo2->blockSignals(false);
			const auto arr = d->BuildSingleTone2();
			d->tf2->SetColormap(arr);
			d->colormapWidget2->update();

			emit sigColormap2(arr);
		}
	}

	void MIPMIP3dControl::OnCustomColormapChanged() {
		const auto colormapPtr = d->tf->GetDataPointer();
		QList<float> colormapArr;
		for (auto i = 0; i < 1024; i++) {
			colormapArr.append(*(colormapPtr + i));
		}
		emit sigColormap(colormapArr);
	}

	void MIPMIP3dControl::OnCustomColormapChanged2() {
		const auto colormapPtr = d->tf2->GetDataPointer();
		QList<float> colormapArr;
		for (auto i = 0; i < 1024; i++) {
			colormapArr.append(*(colormapPtr + i));
		}
		emit sigColormap2(colormapArr);
	}

	void MIPMIP3dControl::OnColormapCombo(int index) {
		const auto colormapID = d->ui.colormapCombo->itemText(index);
		d->ui.gammaSpin->setEnabled(false);
		d->colormapWidget->setEnabled(false);
		QList<float> colormapArr;
		if (colormapID == "Custom") {
			for (auto i = 0; i < 1024; i++) {
				colormapArr.append(1.0f);
			}
			d->colormapWidget->setEnabled(true);
		} else if (colormapID == "Single Tone") {
			d->ui.gammaSpin->setEnabled(true);
			colormapArr = d->BuildSingleTone();
		} else {
			colormapArr = d->predColormapArr[colormapID];
		}
		d->tf->SetColormap(colormapArr);
		d->colormapWidget->update();
		emit sigColormap(colormapArr);
	}

	void MIPMIP3dControl::OnColormapCombo2(int index) {
		const auto colormapID = d->ui.colormapCombo2->itemText(index);
		d->ui.gammaSpin2->setEnabled(false);
		d->colormapWidget2->setEnabled(false);
		QList<float> colormapArr;
		if (colormapID == "Custom") {
			for (auto i = 0; i < 1024; i++) {
				colormapArr.append(1.0f);
			}
			d->colormapWidget2->setEnabled(true);
		}
		else if (colormapID == "Single Tone") {
			d->ui.gammaSpin2->setEnabled(true);
			colormapArr = d->BuildSingleTone2();
		}
		else {
			colormapArr = d->predColormapArr[colormapID];
		}
		d->tf2->SetColormap(colormapArr);
		d->colormapWidget2->update();
		emit sigColormap2(colormapArr);
	}
	
	auto MIPMIP3dControl::SetDataRangeConfig(int decimals, double divider, double offset, double singlestep) -> void {
		d->decimals = decimals;
		d->divider = divider;
		d->offset = offset;
		d->singlestep = singlestep;

		d->ui.dataRangeMin->blockSignals(true);
		d->ui.dataRangeMax->blockSignals(true);

		d->ui.dataRangeMin->setDecimals(decimals);
		d->ui.dataRangeMin->setSingleStep(singlestep);

		d->ui.dataRangeMax->setDecimals(decimals);
		d->ui.dataRangeMax->setSingleStep(singlestep);

		d->ui.dataRangeMin->blockSignals(false);
		d->ui.dataRangeMax->blockSignals(false);
	}

	auto MIPMIP3dControl::SetDataRangeConfig2(int decimals, double divider, double offset, double singlestep) -> void {
		d->decimals2 = decimals;
		d->divider2 = divider;
		d->offset2 = offset;
		d->singlestep2 = singlestep;

		d->ui.dataRangeMin2->blockSignals(true);
		d->ui.dataRangeMax2->blockSignals(true);

		d->ui.dataRangeMin2->setDecimals(decimals);
		d->ui.dataRangeMin2->setSingleStep(singlestep);

		d->ui.dataRangeMax2->setDecimals(decimals);
		d->ui.dataRangeMax2->setSingleStep(singlestep);

		d->ui.dataRangeMin2->blockSignals(false);
		d->ui.dataRangeMax2->blockSignals(false);
	}

	auto MIPMIP3dControl::SetDataMinMax(double min, double max) -> void {
		const auto textmin = min / d->divider + d->offset;
		const auto textmax = max / d->divider + d->offset;

		d->ui.dataRangeMin->blockSignals(true);
		d->ui.dataRangeMax->blockSignals(true);

		d->ui.dataRangeMin->setRange(textmin, textmax - d->singlestep);
		d->ui.dataRangeMax->setRange(textmin + d->singlestep, textmax);

		d->ui.dataRangeMin->setValue(textmin);
		d->ui.dataRangeMax->setValue(textmax);

		d->ui.dataRangeMin->blockSignals(false);
		d->ui.dataRangeMax->blockSignals(false);
	}

	auto MIPMIP3dControl::SetDataMinMax2(double min, double max) -> void {
		const auto textmin = min / d->divider2 + d->offset2;
		const auto textmax = max / d->divider2 + d->offset2;

		d->ui.dataRangeMin2->blockSignals(true);
		d->ui.dataRangeMax2->blockSignals(true);

		d->ui.dataRangeMin2->setRange(textmin, textmax - d->singlestep2);
		d->ui.dataRangeMax2->setRange(textmin + d->singlestep2, textmax);

		d->ui.dataRangeMin2->setValue(textmin);
		d->ui.dataRangeMax2->setValue(textmax);

		d->ui.dataRangeMin2->blockSignals(false);
		d->ui.dataRangeMax2->blockSignals(false);
	}

	auto MIPMIP3dControl::SetChannelIndex(int idx) -> void {
		if (idx == 0) {//Blue
			d->singletone = QColor(0, 0, 255);
		} else if (idx == 1) {//Green
			d->singletone = QColor(0, 255, 0);
		} else {//Red
			d->singletone = QColor(255, 0, 0);
		}
		d->singletonePixmap.fill(d->singletone);
		d->ui.singletoneBtn->setIcon(QIcon(d->singletonePixmap));

		const auto arr = d->BuildSingleTone();
		d->tf->SetColormap(arr);
		d->colormapWidget->update();

		emit sigColormap(arr);
	}

	auto MIPMIP3dControl::SetChannelIndex2(int idx) -> void {
		if (idx == 0) {//Blue
			d->singletone2 = QColor(0, 0, 255);
		}
		else if (idx == 1) {//Green
			d->singletone2 = QColor(0, 255, 0);
		}
		else {//Red
			d->singletone2 = QColor(255, 0, 0);
		}
		d->singletonePixmap2.fill(d->singletone2);
		d->ui.singletoneBtn2->setIcon(QIcon(d->singletonePixmap2));

		const auto arr = d->BuildSingleTone2();
		d->tf2->SetColormap(arr);
		d->colormapWidget2->update();

		emit sigColormap2(arr);
	}


	auto MIPMIP3dControl::SetImageType(DataFlag type) -> void {
		if (type == DataFlag::HT) {
			//DO NOTHING
		} else if (type == DataFlag::FL) {
			const auto singletoneIdx = d->ui.colormapCombo->findText("Single Tone");
			d->ui.colormapCombo->setCurrentIndex(singletoneIdx);
		} else {//Float Image
			//DO NOTHING
		}
	}
	auto MIPMIP3dControl::SetImageType2(DataFlag type) -> void {
		if (type == DataFlag::HT) {
			//DO NOTHING
		}
		else if (type == DataFlag::FL) {
			const auto singletoneIdx = d->ui.colormapCombo2->findText("Single Tone");
			d->ui.colormapCombo2->setCurrentIndex(singletoneIdx);
		}
		else {//Float Image
		 //DO NOTHING
		}
	}
}
