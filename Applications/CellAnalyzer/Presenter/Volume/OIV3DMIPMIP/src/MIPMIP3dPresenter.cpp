#include "MIPMIP3dPresenter.h"

#include <QVBoxLayout>

#include <HT3D.h>
#include <FL3D.h>
#include <Float3d.h>
#include <BinaryMask3D.h>
#include <LabelMask3D.h>

#include <RenderWindow3dWidget.h>
#include <VolumeVolume.h>
#include <MIPMIP3dControl.h>
#include <VolumeVolume.h>

#include <VolumeViz/nodes/SoDataRange.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoScale.h>
#include <Medical/helpers/MedicalHelper.h>

#include "ui_MIPMIP3dPresenter.h"

namespace CellAnalyzer::Presenter::Volume {
	using namespace Tomocube::Rendering;

	struct MIPMIP3dPresenter::Impl {
		Ui::MIP3dOIV ui;

		std::shared_ptr<RenderWindow> window { nullptr };
		std::shared_ptr<Blending::VolumeVolume> renderer { nullptr };
		std::shared_ptr<MIPMIP3dControl> control { nullptr };
		QString title;
		int upsample { 1 };

		QMap<QString, DataPtr> imageData;

		bool ht3dExist { false };
		double refOffset { 0 };
		Size3D refSize { 1, 1, 1 };
		Resolution3D refRes { 1, 1, 1 };

		SoRef<SoVolumeData> SecondVol { nullptr };

		auto IsIdentical(Size3D maskSize, Resolution3D maskRes, float maskOffset) -> bool;
		auto AddReference3D(std::shared_ptr<Data::HT3D> data) -> bool;
		auto AddReference3D(std::shared_ptr<Data::FL3D> data) -> bool;
		auto AddReference3D(std::shared_ptr<Data::Float3D> data) -> bool;
		auto MapUshortGeometry(Size3D maskSize, Resolution3D maskRes, float maskOffset, void* maskData) -> void;
		auto MapFloatGeometry(Size3D maskSize, Resolution3D maskRes, float maskOffset, void* maskData) -> void;

		auto AddHT3D(std::shared_ptr<Data::HT3D> data) -> bool;
		auto AddFL3D(std::shared_ptr<Data::FL3D> data) -> bool;
		auto AddFloat3D(std::shared_ptr<Data::Float3D> data) -> bool;

		auto AddHT3D2(std::shared_ptr<Data::HT3D> data) -> bool;
		auto AddFL3D2(std::shared_ptr<Data::FL3D> data) -> bool;
		auto AddFloat3D2(std::shared_ptr<Data::Float3D> data) -> bool;
		auto AdjustOffsetZ(double offset) -> double;
	};

	auto MIPMIP3dPresenter::Impl::IsIdentical(Size3D maskSize, Resolution3D maskRes, float maskOffset) -> bool {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.0001;
		};
		if (maskSize.x != refSize.x) {
			return false;
		}
		if (maskSize.y != refSize.y) {
			return false;
		}
		if (maskSize.z != refSize.z) {
			return false;
		}
		if (false == AreSame(maskRes.x, refRes.x)) {
			return false;
		}
		if (false == AreSame(maskRes.y, refRes.y)) {
			return false;
		}
		if (false == AreSame(maskRes.y, refRes.y)) {
			return false;
		}
		if (false == AreSame(maskOffset, refOffset)) {
			return false;
		}
		return true;
	}


	auto MIPMIP3dPresenter::Impl::AdjustOffsetZ(double offset) -> double {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.0001;
		};
		if (AreSame(offset, 0)) {
			return 0;
		}
		if (ht3dExist) {
			return offset - static_cast<double>(refSize.z) * refRes.z / 2.0;
		}
		return offset;
	}


	auto MIPMIP3dPresenter::Impl::AddReference3D(std::shared_ptr<Data::HT3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		refSize = size;
		refRes = res;

		return true;
	}

	auto MIPMIP3dPresenter::Impl::AddReference3D(std::shared_ptr<Data::FL3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		refSize = size;
		refRes = res;

		return true;
	}

	auto MIPMIP3dPresenter::Impl::AddReference3D(std::shared_ptr<Data::Float3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		refSize = size;
		refRes = res;
		return true;
	}

	auto MIPMIP3dPresenter::Impl::MapUshortGeometry(Size3D maskSize, Resolution3D maskRes, float maskOffset, void* maskData) -> void {
		auto GetPhysicalPosition = [](int ix, int iy, int iz, double spx, double spy, double spz, double originX, double originY, double originZ, double offset) -> std::tuple<double, double, double> {
			auto phyX = originX + ix * spx;
			auto phyY = originY + iy * spy;
			auto phyZ = originZ + offset + iz * spz;

			return std::make_tuple(phyX, phyY, phyZ);
		};
		auto GetIndexPosition = [](double px, double py, double pz, double spx, double spy, double spz, double originX, double originY, double originZ, double offset) -> std::tuple<int, int, int> {
			auto ix = static_cast<int>(floor((px - originX) / spx));
			auto iy = static_cast<int>(floor((py - originY) / spy));
			auto iz = static_cast<int>(floor((pz - originZ - offset) / spz));

			return std::make_tuple(ix, iy, iz);
		};

		double refOrigin[3] = { -refSize.x * refRes.x / 2, -refSize.y * refRes.y / 2, -refSize.z * refRes.z / 2 };
		double maskOrigin[3] = { -maskSize.x * maskRes.x / 2, -maskSize.y * maskRes.y / 2, -maskSize.z * maskRes.z / 2 };

		const auto sourceArr = static_cast<const uint16_t*>(maskData);

		std::shared_ptr<uint16_t[]> arr(new uint16_t[refSize.x * refSize.y * refSize.z](), std::default_delete<uint16_t[]>());

		for (auto k = 0; k < refSize.z; k++) {
			for (auto i = 0; i < refSize.x; i++) {
				for (auto j = 0; j < refSize.y; j++) {
					const auto [px, py, pz] = GetPhysicalPosition(i, j, k, refRes.x, refRes.y, refRes.z, refOrigin[0], refOrigin[1], refOrigin[2], AdjustOffsetZ(refOffset));
					const auto [ix, iy, iz] = GetIndexPosition(px, py, pz, maskRes.x, maskRes.y, maskRes.z, maskOrigin[0], maskOrigin[1], maskOrigin[2], AdjustOffsetZ(maskOffset));

					if (ix > -1 && ix < maskSize.x && iy > -1 && iy < maskSize.y && iz > -1 && iz < maskSize.z) {
						const auto sourceidx = iz * maskSize.x * maskSize.y + iy * maskSize.x + ix;
						const auto val = *(sourceArr + sourceidx);
						const auto targetidx = k * refSize.x * refSize.y + j * refSize.x + i;
						*(arr.get() + targetidx) = val;
					}
				}
			}
		}
		SecondVol->data.setValue(SbVec3i32(refSize.x, refSize.y, refSize.z), SbDataType::UNSIGNED_SHORT, 16, arr.get(), SoSFArray::COPY);
		SecondVol->extent.setValue(-refSize.x * refRes.x / 2, -refSize.y * refRes.y / 2, -refSize.z * refRes.z / 2 + AdjustOffsetZ(refOffset), refSize.x * refRes.x / 2, refSize.y * refRes.y / 2, refSize.z * refRes.z / 2 + AdjustOffsetZ(refOffset));
	}

	auto MIPMIP3dPresenter::Impl::MapFloatGeometry(Size3D maskSize, Resolution3D maskRes, float maskOffset, void* maskData) -> void {
		auto GetPhysicalPosition = [](int ix, int iy, int iz, double spx, double spy, double spz, double originX, double originY, double originZ, double offset) -> std::tuple<double, double, double> {
			auto phyX = originX + ix * spx;
			auto phyY = originY + iy * spy;
			auto phyZ = originZ + offset + iz * spz;

			return std::make_tuple(phyX, phyY, phyZ);
		};
		auto GetIndexPosition = [](double px, double py, double pz, double spx, double spy, double spz, double originX, double originY, double originZ, double offset) -> std::tuple<int, int, int> {
			auto ix = static_cast<int>(floor((px - originX) / spx));
			auto iy = static_cast<int>(floor((py - originY) / spy));
			auto iz = static_cast<int>(floor((pz - originZ - offset) / spz));

			return std::make_tuple(ix, iy, iz);
		};

		double refOrigin[3] = { -refSize.x * refRes.x / 2, -refSize.y * refRes.y / 2, -refSize.z * refRes.z / 2 };
		double maskOrigin[3] = { -maskSize.x * maskRes.x / 2, -maskSize.y * maskRes.y / 2, -maskSize.z * maskRes.z / 2 };

		const auto sourceArr = static_cast<const float*>(maskData);

		std::shared_ptr<float[]> arr(new float[refSize.x * refSize.y * refSize.z](), std::default_delete<float[]>());

		for (auto k = 0; k < refSize.z; k++) {
			for (auto i = 0; i < refSize.x; i++) {
				for (auto j = 0; j < refSize.y; j++) {
					const auto [px, py, pz] = GetPhysicalPosition(i, j, k, refRes.x, refRes.y, refRes.z, refOrigin[0], refOrigin[1], refOrigin[2], AdjustOffsetZ(refOffset));
					const auto [ix, iy, iz] = GetIndexPosition(px, py, pz, maskRes.x, maskRes.y, maskRes.z, maskOrigin[0], maskOrigin[1], maskOrigin[2], AdjustOffsetZ(maskOffset));

					if (ix > -1 && ix < maskSize.x && iy > -1 && iy < maskSize.y && iz > -1 && iz < maskSize.z) {
						const auto sourceidx = iz * maskSize.x * maskSize.y + iy * maskSize.x + ix;
						const auto val = *(sourceArr + sourceidx);
						const auto targetidx = k * refSize.x * refSize.y + j * refSize.x + i;
						*(arr.get() + targetidx) = val;
					}
				}
			}
		}
		SecondVol->data.setValue(SbVec3i32(refSize.x, refSize.y, refSize.z), SbDataType::FLOAT, 16, arr.get(), SoSFArray::COPY);
		SecondVol->extent.setValue(-refSize.x * refRes.x / 2, -refSize.y * refRes.y / 2, -refSize.z * refRes.z / 2 + AdjustOffsetZ(refOffset), refSize.x * refRes.x / 2, refSize.y * refRes.y / 2, refSize.z * refRes.z / 2 + AdjustOffsetZ(refOffset));
	}

	auto MIPMIP3dPresenter::Impl::AddHT3D(std::shared_ptr<Data::HT3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2);
		//add SceneGraph
		const auto [min, max] = data->GetRI();

		renderer->SetDataMinMax(min * 10000.0, max * 10000.0);
		renderer->SetVolume(volData.ptr());

		control->SetDataRangeConfig(4, 10000, 0, 0.0001);
		control->SetDataMinMax(min * 10000.0, max * 10000.0);
		control->SetImageType(DataFlag::HT);

		return true;
	}

	auto MIPMIP3dPresenter::Impl::AddHT3D2(std::shared_ptr<Data::HT3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		SecondVol = new SoVolumeData;
		if (IsIdentical(size, res, 0)) {
			SecondVol->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
			SecondVol->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2);
		} else {
			MapUshortGeometry(size, res, 0, data->GetData());
		}
		//add SceneGraph
		const auto [min, max] = data->GetRI();

		renderer->SetDataMinMax2(min * 10000.0, max * 10000.0);
		renderer->SetVolume2(SecondVol.ptr());

		control->SetDataRangeConfig2(4, 10000, 0, 0.0001);
		control->SetDataMinMax2(min * 10000.0, max * 10000.0);
		control->SetImageType2(DataFlag::HT);

		return true;
	}

	auto MIPMIP3dPresenter::Impl::AddFL3D(std::shared_ptr<Data::FL3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();
		const auto channel = data->GetChannelIndex();

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2);

		const auto [min, max] = data->GetIntensity();

		renderer->SetDataMinMax(min, max);
		renderer->SetVolume(volData.ptr());

		control->SetDataRangeConfig(0, 1, 0, 1);
		control->SetDataMinMax(min, max);

		control->SetChannelIndex(channel);
		control->SetImageType(DataFlag::FL);
		return true;
	}

	auto MIPMIP3dPresenter::Impl::AddFL3D2(std::shared_ptr<Data::FL3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();
		const auto channel = data->GetChannelIndex();

		SecondVol = new SoVolumeData;
		if (IsIdentical(size, res, offset)) {
			SecondVol->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::UNSIGNED_SHORT, 16, data->GetData(), SoSFArray::NO_COPY);
			SecondVol->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + offset, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + offset);
		} else {
			MapUshortGeometry(size, res, offset, data->GetData());
		}

		const auto [min, max] = data->GetIntensity();

		renderer->SetDataMinMax2(min, max);
		renderer->SetVolume2(SecondVol.ptr());

		control->SetDataRangeConfig2(0, 1, 0, 1);
		control->SetDataMinMax2(min, max);

		control->SetChannelIndex2(channel);
		control->SetImageType2(DataFlag::FL);
		return true;
	}

	auto MIPMIP3dPresenter::Impl::AddFloat3D(std::shared_ptr<Data::Float3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();

		SoRef<SoVolumeData> volData = new SoVolumeData;
		volData->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::FLOAT, 32, data->GetData(), SoSFArray::NO_COPY);
		volData->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + offset, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + offset);

		double min, max;
		volData->getMinMax(min, max);

		renderer->SetDataMinMax(min, max);
		renderer->SetVolume(volData.ptr());

		control->SetDataRangeConfig(2, 1, 0, 0.01);
		control->SetDataMinMax(min, max);
		control->SetImageType(DataFlag::Null);
		return true;
	}

	auto MIPMIP3dPresenter::Impl::AddFloat3D2(std::shared_ptr<Data::Float3D> data) -> bool {
		const auto size = data->GetSize();
		const auto res = data->GetResolution();
		const auto offset = data->GetZOffset();

		SecondVol = new SoVolumeData;
		if (IsIdentical(size, res, offset)) {
			SecondVol->data.setValue(SbVec3i32(size.x, size.y, size.z), SbDataType::FLOAT, 32, data->GetData(), SoSFArray::NO_COPY);
			SecondVol->extent.setValue(-size.x * res.x / 2, -size.y * res.y / 2, -size.z * res.z / 2 + offset, size.x * res.x / 2, size.y * res.y / 2, size.z * res.z / 2 + offset);
		} else {
			MapFloatGeometry(size, res, offset, data->GetData());
		}

		double min, max;
		SecondVol->getMinMax(min, max);

		renderer->SetDataMinMax2(min, max);
		renderer->SetVolume2(SecondVol.ptr());

		control->SetDataRangeConfig2(2, 1, 0, 0.01);
		control->SetDataMinMax2(min, max);
		control->SetImageType2(DataFlag::Null);
		return true;
	}

	MIPMIP3dPresenter::MIPMIP3dPresenter(QWidget* parent) : IPresenter(), QWidget(parent), d { new Impl } {
		d->ui.setupUi(this);
		d->window = std::make_shared<RenderWindow >(nullptr);
		d->window->SetGradientBackground(0,0,0,0,0,0);
		const auto layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		d->ui.RenderSocket->setLayout(layout);
		layout->addWidget(d->window.get());

		d->renderer = std::make_shared<Blending::VolumeVolume>("VolumeVolume");
		d->renderer->ToggleDepthEnhanced(false);
		d->renderer->SetDepthEnhancementFactor(1);
		SoRef<SoSeparator> swapRoot = new SoSeparator;
		SoRef<SoScale> swap = new SoScale;
		swap->scaleFactor.setValue(1, -1, 1);
		swapRoot->addChild(swap.ptr());
		swapRoot->addChild(d->renderer->GetRootSwitch());
		d->window->SetSceneGraph(swapRoot.ptr());

		d->control = std::make_shared<MIPMIP3dControl>();

		InitConnections();
	}

	MIPMIP3dPresenter::~MIPMIP3dPresenter() = default;

	auto MIPMIP3dPresenter::InitConnections() -> void {
		connect(d->control.get(), &MIPMIP3dControl::sigUpsample, this, [this](int upsample) {
			d->upsample = upsample;
		});
		connect(d->control.get(), &MIPMIP3dControl::sigCapture, this, [this](QString path) {
			CaptureScreen(path);
		});
		connect(d->control.get(), &MIPMIP3dControl::sigDataRange, this, [this](double min, double max) {
			OnDataRange(min, max, 1);
		});
		connect(d->control.get(), &MIPMIP3dControl::sigDataRange2, this, [this](double min, double max) {
			OnDataRange(min, max, 2);
		});
		connect(d->control.get(), &MIPMIP3dControl::sigColormap, this, [this](QList<float> colormap) {
			OnColormap(colormap, 1);
		});
		connect(d->control.get(), &MIPMIP3dControl::sigColormap2, this, [this](QList<float> colormap) {
			OnColormap(colormap, 2);
		});
		connect(d->control.get(), &MIPMIP3dControl::sigDepthFactor, this, [this](double factor) {
			OnDepthFactor(factor);
		});
		connect(d->control.get(), &MIPMIP3dControl::sigGradientFactor, this, [this](double factor) {
			OnGradientFactor(factor);
		});
		connect(d->control.get(), &MIPMIP3dControl::sigCropRange, this, [this](double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
			OnCropRange(xmin, xmax, ymin, ymax, zmin, zmax);
		});
		connect(d->control.get(), &MIPMIP3dControl::sigUseMIP, this, [this](bool useMIP) {
			d->renderer->ToggleMIP(useMIP);
		});

		connect(d->control.get(), &MIPMIP3dControl::sigUseData1, this, [this](bool data1) {
			d->renderer->ToggleViz(data1);
		});
		connect(d->control.get(), &MIPMIP3dControl::sigUseData2, this, [this](bool data2) {
			d->renderer->ToggleViz2(data2);
		});
		connect(d->control.get(), &MIPMIP3dControl::sigJittering, this, [this](bool use) {
			d->renderer->ToggleJittering(use);
			});
		connect(d->control.get(), &MIPMIP3dControl::sigDefer, this, [this](bool use) {
			d->renderer->ToggleDeferredLighting(use);
			});
		connect(d->window.get(), &RenderWindow::sendMouseClick, this, [this]() {
			OnMouseClick();
		});
	}

	void MIPMIP3dPresenter::OnMouseClick() {
		setFocus();
	}

	void MIPMIP3dPresenter::OnCropRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) {
		d->renderer->SetXRange(xmin, xmax);
		d->renderer->SetYRange(ymin, ymax);
		d->renderer->SetZRange(zmin, zmax);
	}

	void MIPMIP3dPresenter::OnDepthFactor(double factor) {
		if (factor > 0) {
			d->renderer->ToggleDepthEnhanced(true);
		} else {
			d->renderer->ToggleDepthEnhanced(false);
		}
		d->renderer->SetDepthEnhancementFactor(factor);
	}

	void MIPMIP3dPresenter::OnGradientFactor(double factor) {
		if (factor > 0) {
			d->renderer->ToggleGradientEnhanced(true);
		} else {
			d->renderer->ToggleGradientEnhanced(false);
		}
		d->renderer->SetGradientEnhancementFactor(factor);
	}

	void MIPMIP3dPresenter::OnColormap(QList<float> colormapArr, int dataID) {
		if (dataID == 1) {
			d->renderer->SetColormap(colormapArr);
		} else if (dataID == 2) {
			d->renderer->SetColormap2(colormapArr);
		}
	}

	void MIPMIP3dPresenter::OnDataRange(double min, double max, int dataID) {
		if (dataID == 1) {
			d->renderer->SetDataRange(min, max);
		} else if (dataID == 2) {
			d->renderer->SetDataRange2(min, max);
		}
	}

	auto MIPMIP3dPresenter::SetTitle(const QString& title) -> void {
		setWindowTitle(title);
		d->control->setWindowTitle(title + " Control");
	}

	auto MIPMIP3dPresenter::GetWindowList() const -> ViewList {
		return { d->control };
	}

	auto MIPMIP3dPresenter::IsPrimary(const ViewPtr& window) const -> bool {
		return true;
	}

	auto MIPMIP3dPresenter::IsAcceptable(const DataPtr& data) const -> bool {
		return true;
	}

	auto MIPMIP3dPresenter::GetDataList() const -> QStringList {
		return d->imageData.keys();
	}

	auto MIPMIP3dPresenter::GetData(const QString& name) const -> DataPtr {
		return d->imageData[name];
	}

	auto MIPMIP3dPresenter::GetName(const DataPtr& data) const -> QString {
		for (auto k : d->imageData.keys()) {
			if (d->imageData[k] == data) {
				return k;
			}
		}
		return {};
	}

	auto MIPMIP3dPresenter::RemoveData(const QString& name) -> void {
		//DO NOT USE
	}

	auto MIPMIP3dPresenter::RenameData(const DataPtr& data, const QString& name) -> bool {
		const auto prevName = GetName(data);
		if (prevName.isEmpty()) {
			return false;
		}

		d->imageData.remove(prevName);

		d->imageData[name] = data;

		return true;
	}

	auto MIPMIP3dPresenter::ClearData() -> void { }

	auto MIPMIP3dPresenter::Capture(const QString& filepath) -> void { }

	auto MIPMIP3dPresenter::CaptureScreen(const QString& path, bool fullResolution) -> void {
		//set fixed resolution
		const auto volDataList = MedicalHelper::findNodes<SoVolumeData>(d->renderer->GetRootSwitch());
		if (fullResolution) {
			for (const auto volData : volDataList) {
				volData->ldmResourceParameters.getValue()->resolution = 0;
				volData->ldmResourceParameters.getValue()->fixedResolution = TRUE;
			}
		}
		d->window->SaveSnapShot(path, d->upsample);
		//restore resolution
		if (fullResolution) {
			for (const auto volData : volDataList) {
				volData->ldmResourceParameters.getValue()->fixedResolution = FALSE;
			}
		}
	}

	auto MIPMIP3dPresenter::AddData(const QString& name, const DataPtr& data, const QString& portName) -> bool {
		if (portName == "ReferencePort") {
			if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(data); d->AddReference3D(image)) {
				d->ht3dExist = true;
			}
		} else if (portName == "List of Image1") {
			if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(data)) {
				if (false == d->AddHT3D(image)) {
					return false;
				}
				if (false == d->ht3dExist) {
					d->AddReference3D(image);
				}
			} else if (const auto fl = std::dynamic_pointer_cast<Data::FL3D>(data)) {
				if (false == d->AddFL3D(fl)) {
					return false;
				}
				if (false == d->ht3dExist) {
					d->AddReference3D(fl);
				}
			} else if (const auto floatImg = std::dynamic_pointer_cast<Data::Float3D>(data)) {
				if (false == d->AddFloat3D(floatImg)) {
					return false;
				}
				if (false == d->ht3dExist) {
					d->AddReference3D(floatImg);
				}
			}
			d->imageData[name] = data;
		} else if (portName == "List of Image2") {
			if (const auto image = std::dynamic_pointer_cast<Data::HT3D>(data)) {
				if (false == d->AddHT3D2(image)) {
					return false;
				}
			} else if (const auto fl = std::dynamic_pointer_cast<Data::FL3D>(data)) {
				if (false == d->AddFL3D2(fl)) {
					return false;
				}
			} else if (const auto floatImg = std::dynamic_pointer_cast<Data::Float3D>(data)) {
				if (false == d->AddFloat3D2(floatImg)) {
					return false;
				}
			}
			d->imageData[name] = data;
		}
		d->window->ResetView();
		return true;
	}
}
