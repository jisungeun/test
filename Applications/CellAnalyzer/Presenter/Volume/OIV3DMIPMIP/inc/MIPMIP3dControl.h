#pragma once

#include <enum.h>

#include <QWidget>

#include "IData.h"
#include "IView.h"

namespace CellAnalyzer::Presenter::Volume {
	class MIPMIP3dControl final : public QWidget, public IView {
		Q_OBJECT
	public:
		explicit MIPMIP3dControl(QWidget* parent = nullptr);
		~MIPMIP3dControl();

		auto SetImageType(DataFlag type) -> void;
		auto SetImageType2(DataFlag type) -> void;

		auto SetChannelIndex(int idx) -> void;
		auto SetChannelIndex2(int idx) -> void;

		auto SetDataRangeConfig(int decimals, double divider, double offset, double singlestep) -> void;
		auto SetDataRangeConfig2(int decimals, double divider, double offset, double singlestep) -> void;
		auto SetDataMinMax(double min, double max) -> void;
		auto SetDataMinMax2(double min, double max) -> void;

	signals:
		void sigDataRange(double, double);
		void sigDataRange2(double, double);

		void sigColormap(QList<float>);
		void sigColormap2(QList<float>);

		void sigDepthFactor(double);
		void sigGradientFactor(double);

		void sigCropRange(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);

		void sigCapture(QString);
		void sigUpsample(int);

		void sigUseMIP(bool);
		void sigUseData1(bool);
		void sigUseData2(bool);

		void sigJittering(bool);
		void sigDefer(bool);

	protected slots:
		void OnDataMinSpin(double);
		void OnDataMaxSpin(double);

		void OnDataMinSpin2(double);
		void OnDataMaxSpin2(double);

		void OnColormapCombo(int);
		void OnColormapCombo2(int);
		void OnCustomColormapChanged();
		void OnCustomColormapChanged2();
		void OnSingletone();
		void OnSingletone2();
		void OnGammaChanged(double);
		void OnGammaChanged2(double);

		void OnEnhanceChk();
		void OnDepthFactor(double);
		void OnGradientFactor(double);

		void OnCropChk();
		void OnCropXmin(double);
		void OnCropXmax(double);
		void OnCropYmin(double);
		void OnCropYmax(double);
		void OnCropZmin(double);
		void OnCropZmax(double);

		void OnCapture();
		void OnMIP();
		void OnData1();
		void OnData2();

		void OnJittering();
		void OnDefer();

	protected:
		auto InitConnections() -> void;
		auto SendCropSignal() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
