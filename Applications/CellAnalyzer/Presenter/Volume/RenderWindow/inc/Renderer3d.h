#pragma once
#include <memory>

#include <QOivRenderWindow.h>

//#include "CellAnalyzer.Presenter.Volume.RenderWindowExport.h"

namespace CellAnalyzer::Presenter::Volume {
	class Renderer3d : public QOivRenderWindow {
		Q_OBJECT
	public:
		Renderer3d(QWidget* parent);
		~Renderer3d();

	signals:
		void sigWheel(int);
		void sigMousePos(float, float);
		void sigMouseClick();
		void sigSpinning();
		void sigPanning();
		void sigZooming();

	protected:
		auto MouseButtonEvent(SoEventCallback* node) -> void override;
		auto MouseMoveEvent(SoEventCallback* node) -> void override;
		auto KeyboardEvent(SoEventCallback* node) -> void override;
		auto MouseWheelEvent(SoEventCallback* node) -> void override;

		auto setDefaultWindowType() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}