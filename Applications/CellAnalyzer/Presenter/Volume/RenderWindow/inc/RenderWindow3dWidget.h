#include <QWidget>

#include "CellAnalyzer.Presenter.Volume.RenderWindowExport.h"

#include "Renderer3d.h"

namespace CellAnalyzer::Presenter::Volume {
	class CellAnalyzer_Presenter_Volume_RenderWindow_API RenderWindow final : public QWidget {
		Q_OBJECT
	public:
		RenderWindow(QWidget* parent = nullptr);
		~RenderWindow();

		auto ResetView() -> void;
		auto SaveSnapShot(const QString& path, int upsample) -> void;
		auto SetGradientBackground(float sr, float sg, float sb, float er, float eg, float eb) -> void;
		auto SetSceneGraph(SoNode* newScene) -> void;

	signals:
		void sendMouseClick();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
