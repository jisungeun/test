#include <QVBoxLayout>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFileDialog>
#include <QSettings>

#include "ui_RenderWindow3dWidget.h"
#include "RenderWindow3dWidget.h"

namespace CellAnalyzer::Presenter::Volume {
	struct RenderWindow::Impl {
		Ui::Form ui;

		std::shared_ptr<Renderer3d> renderer { nullptr };
		auto saveCamInfo(double x, double y, double z, double xax, double yax, double zax, double rad, double zoomFactor, const QString& path) -> bool;
		auto loadCamInfo(double& x, double& y, double& z, double& xax, double& yax, double& zax, double& rad, double& zoomFactor, const QString& path) -> bool;
	};

	auto RenderWindow::Impl::saveCamInfo(double x, double y, double z, double xax, double yax, double zax, double rad, double zoomFactor, const QString& path) -> bool {
		// Create a JSON object
		QJsonObject jsonObject;
		jsonObject["x"] = x;
		jsonObject["y"] = y;
		jsonObject["z"] = z;
		jsonObject["xax"] = xax;
		jsonObject["yax"] = yax;
		jsonObject["zax"] = zax;
		jsonObject["angle"] = rad;
		jsonObject["zoomFactor"] = zoomFactor;

		// Convert JSON object to a QJsonDocument
		QJsonDocument jsonDoc(jsonObject);

		// Write JSON data to a file
		QFile file(path);
		if (!file.open(QIODevice::WriteOnly)) {
			qWarning() << "Couldn't open file for writing.";
			return false;
		}

		file.write(jsonDoc.toJson());

		file.close();

		return true;
	}

	auto RenderWindow::Impl::loadCamInfo(double& x, double& y, double& z, double& xax, double& yax, double& zax, double& rad, double& zoomFactor, const QString& path) -> bool {
		// Read JSON data from file
		QFile file(path);
		if (!file.open(QIODevice::ReadOnly)) {
			qWarning() << "Couldn't open file for reading.";
			return false;
		}

		QByteArray jsonData = file.readAll();
		file.close();

		// Parse JSON data into a QJsonDocument
		QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData);
		if (jsonDoc.isNull()) {
			qWarning() << "Failed to create JSON document from data.";
			return false;
		}

		// Extract values from the JSON document
		QJsonObject jsonObject = jsonDoc.object();
		x = jsonObject["x"].toDouble();
		y = jsonObject["y"].toDouble();
		z = jsonObject["z"].toDouble();
		xax = jsonObject["xax"].toDouble();
		yax = jsonObject["yax"].toDouble();
		zax = jsonObject["zax"].toDouble();
		rad = jsonObject["angle"].toDouble();
		zoomFactor = jsonObject["zoomFactor"].toDouble();

		return true;
	}


	RenderWindow::RenderWindow(QWidget* parent) : QWidget(parent), d { new Impl } {
		d->ui.setupUi(this);
		d->renderer = std::make_shared<Renderer3d>(nullptr);

		auto layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);

		d->ui.rendererSocket->setLayout(layout);
		layout->addWidget(d->renderer.get());

		d->ui.zoomSpin->setRange(0.01, 1000.0);
		d->ui.zoomSpin->setDecimals(2);
		d->ui.zoomSpin->setSingleStep(0.01);

		d->ui.xSpin->setRange(-5000, 5000);
		d->ui.ySpin->setRange(-5000, 5000);
		d->ui.zSpin->setRange(-5000, 5000);
		d->ui.xSpin->setDecimals(2);
		d->ui.ySpin->setDecimals(2);
		d->ui.zSpin->setDecimals(2);
		d->ui.xSpin->setSingleStep(0.01);
		d->ui.ySpin->setSingleStep(0.01);
		d->ui.zSpin->setSingleStep(0.01);

		d->ui.xAxisSpin->setRange(-1, 1);
		d->ui.yAxisSpin->setRange(-1, 1);
		d->ui.zAxisSpin->setRange(-1, 1);
		d->ui.xAxisSpin->setDecimals(2);
		d->ui.yAxisSpin->setDecimals(2);
		d->ui.zAxisSpin->setDecimals(2);
		d->ui.xAxisSpin->setSingleStep(0.01);
		d->ui.yAxisSpin->setSingleStep(0.01);
		d->ui.zAxisSpin->setSingleStep(0.01);

		d->ui.angleSpin->setRange(0, 10);
		d->ui.angleSpin->setDecimals(2);
		d->ui.angleSpin->setSingleStep(0.01);


		connect(d->ui.zoomSpin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val) {
			d->renderer->setCameraZoom(static_cast<float>(val));
		});
		connect(d->ui.xSpin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val) {
			const auto x = static_cast<float>(val);
			const auto y = static_cast<float>(d->ui.ySpin->value());
			const auto z = static_cast<float>(d->ui.zSpin->value());
			d->renderer->getCamera()->position.setValue(x, y, z);
		});
		connect(d->ui.ySpin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val) {
			const auto x = static_cast<float>(d->ui.xSpin->value());
			const auto y = static_cast<float>(val);
			const auto z = static_cast<float>(d->ui.zSpin->value());
			d->renderer->getCamera()->position.setValue(x, y, z);
		});
		connect(d->ui.zSpin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val) {
			const auto x = static_cast<float>(d->ui.xSpin->value());
			const auto y = static_cast<float>(d->ui.ySpin->value());
			const auto z = static_cast<float>(val);
			d->renderer->getCamera()->position.setValue(x, y, z);
		});
		connect(d->ui.xAxisSpin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val) {
			const auto x = static_cast<float>(val);
			const auto y = static_cast<float>(d->ui.yAxisSpin->value());
			const auto z = static_cast<float>(d->ui.zAxisSpin->value());
			const auto rad = static_cast<float>(d->ui.angleSpin->value());
			d->renderer->getCamera()->orientation.setValue(SbVec3f(x, y, z), rad);
		});
		connect(d->ui.yAxisSpin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val) {
			const auto x = static_cast<float>(d->ui.xAxisSpin->value());
			const auto y = static_cast<float>(val);
			const auto z = static_cast<float>(d->ui.zAxisSpin->value());
			const auto rad = static_cast<float>(d->ui.angleSpin->value());
			d->renderer->getCamera()->orientation.setValue(SbVec3f(x, y, z), rad);
		});
		connect(d->ui.zAxisSpin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val) {
			const auto x = static_cast<float>(d->ui.xAxisSpin->value());
			const auto y = static_cast<float>(d->ui.yAxisSpin->value());
			const auto z = static_cast<float>(val);
			const auto rad = static_cast<float>(d->ui.angleSpin->value());
			d->renderer->getCamera()->orientation.setValue(SbVec3f(x, y, z), rad);
		});
		connect(d->ui.angleSpin, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, [this](double val) {
			const auto x = static_cast<float>(d->ui.xAxisSpin->value());
			const auto y = static_cast<float>(d->ui.yAxisSpin->value());
			const auto z = static_cast<float>(d->ui.zAxisSpin->value());
			const auto rad = static_cast<float>(val);
			d->renderer->getCamera()->orientation.setValue(SbVec3f(x, y, z), rad);
		});
		connect(d->ui.loadBtn, &QPushButton::clicked, this, [this] {
			double zoom, x, y, z, xax, yax, zax, rad;
			const auto prev = QSettings("Tomocube", "TomoAnalysis").value("presenter/camera3d", QApplication::applicationDirPath()).toString();
			const auto path = QFileDialog::getOpenFileName(nullptr, "Load 3d camera information", prev, "JSON files(*.json)");
			if (false == path.isEmpty()) {
				if (d->loadCamInfo(x, y, z, xax, yax, zax, rad, zoom, path)) {
					QSettings("Tomocube", "TomoAnalysis").setValue("presenter/camera3d", path);

					QSignalBlocker zoomBlock(d->ui.zoomSpin);
					QSignalBlocker xBlock(d->ui.xSpin);
					QSignalBlocker yBlock(d->ui.ySpin);
					QSignalBlocker zBlock(d->ui.zSpin);
					QSignalBlocker xaxBlock(d->ui.xAxisSpin);
					QSignalBlocker yaxBlock(d->ui.yAxisSpin);
					QSignalBlocker zaxBlock(d->ui.zAxisSpin);
					QSignalBlocker angleBlock(d->ui.angleSpin);

					d->ui.zoomSpin->setValue(zoom);
					d->ui.xSpin->setValue(x);
					d->ui.ySpin->setValue(y);
					d->ui.zSpin->setValue(z);
					d->ui.xAxisSpin->setValue(xax);
					d->ui.yAxisSpin->setValue(yax);
					d->ui.zAxisSpin->setValue(zax);
					d->ui.angleSpin->setValue(rad);

					d->renderer->setCameraZoom(zoom);
					d->renderer->getCamera()->position.setValue(x, y, z);
					d->renderer->getCamera()->orientation.setValue(SbVec3f(xax, yax, zax), rad);
				}
			}
		});
		connect(d->ui.saveBtn, &QPushButton::clicked, this, [this] {
			const auto zoom = d->ui.zoomSpin->value();
			const auto x = d->ui.xSpin->value();
			const auto y = d->ui.ySpin->value();
			const auto z = d->ui.zSpin->value();
			const auto xax = d->ui.xAxisSpin->value();
			const auto yax = d->ui.yAxisSpin->value();
			const auto zax = d->ui.zAxisSpin->value();
			const auto rad = d->ui.angleSpin->value();
			const auto prev = QSettings("Tomocube", "TomoAnalysis").value("presenter/camera3d", QApplication::applicationDirPath()).toString();
			const auto path = QFileDialog::getSaveFileName(nullptr, "Save current 3d camera information", prev, "JSON files (*.json)");
			if (false == path.isEmpty()) {
				if (d->saveCamInfo(x, y, z, xax, yax, zax, rad, zoom, path)) {
					QSettings("Tomocube", "TomoAnalysis").setValue("presenter/camera3d", path);
				}
			}
		});
		connect(d->renderer.get(), &Renderer3d::sigZooming, this, [this] {
			QSignalBlocker zoomBlock(d->ui.zoomSpin);
			const auto zoomFactor = d->renderer->getCameraZoom();
			d->ui.zoomSpin->setValue(zoomFactor);
		});
		connect(d->renderer.get(), &Renderer3d::sigPanning, this, [this] {
			QSignalBlocker xBlock(d->ui.xSpin);
			QSignalBlocker yBlock(d->ui.ySpin);
			QSignalBlocker zBlock(d->ui.zSpin);
			const auto pos = d->renderer->getCamera()->position.getValue();
			d->ui.xSpin->setValue(pos[0]);
			d->ui.ySpin->setValue(pos[1]);
			d->ui.zSpin->setValue(pos[2]);
		});
		connect(d->renderer.get(), &Renderer3d::sigSpinning, this, [this] {
			QSignalBlocker xBlock(d->ui.xSpin);
			QSignalBlocker yBlock(d->ui.ySpin);
			QSignalBlocker zBlock(d->ui.zSpin);
			const auto pos = d->renderer->getCamera()->position.getValue();
			d->ui.xSpin->setValue(pos[0]);
			d->ui.ySpin->setValue(pos[1]);
			d->ui.zSpin->setValue(pos[2]);
			QSignalBlocker xaxBlock(d->ui.xAxisSpin);
			QSignalBlocker yaxBlock(d->ui.yAxisSpin);
			QSignalBlocker zaxBlock(d->ui.zAxisSpin);
			QSignalBlocker angleBlock(d->ui.angleSpin);
			const auto orientation = d->renderer->getCamera()->orientation.getValue();
			SbVec3f axis;
			float radian;
			orientation.getValue(axis, radian);
			d->ui.xAxisSpin->setValue(axis[0]);
			d->ui.yAxisSpin->setValue(axis[1]);
			d->ui.zAxisSpin->setValue(axis[2]);
			d->ui.angleSpin->setValue(radian);
		});
		connect(d->renderer.get(), &Renderer3d::sigMouseClick, this, [this] {
			emit sendMouseClick();
		});
	}

	RenderWindow::~RenderWindow() { }

	auto RenderWindow::SaveSnapShot(const QString& path, int upsample) -> void {
		d->renderer->saveSnapshot(path, upsample);
	}

	auto RenderWindow::SetSceneGraph(SoNode* newScene) -> void {
		d->renderer->setSceneGraph(newScene);
	}

	auto RenderWindow::SetGradientBackground(float sr, float sg, float sb, float er, float eg, float eb) -> void {
		d->renderer->setGradientBackground(SbVec3f(sr, sg, sb), SbVec3f(er, eg, eb));
	}

	auto RenderWindow::ResetView() -> void {
		d->renderer->viewall();

		QSignalBlocker zoomBlock(d->ui.zoomSpin);
		QSignalBlocker xBlock(d->ui.xSpin);
		QSignalBlocker yBlock(d->ui.ySpin);
		QSignalBlocker zBlock(d->ui.zSpin);
		QSignalBlocker xaxBlock(d->ui.xAxisSpin);
		QSignalBlocker yaxBlock(d->ui.yAxisSpin);
		QSignalBlocker zaxBlock(d->ui.zAxisSpin);
		QSignalBlocker angleBlock(d->ui.angleSpin);

		const auto zoomFactor = d->renderer->getCameraZoom();
		d->ui.zoomSpin->setValue(zoomFactor);

		const auto pos = d->renderer->getCamera()->position.getValue();
		d->ui.xSpin->setValue(pos[0]);
		d->ui.ySpin->setValue(pos[1]);
		d->ui.zSpin->setValue(pos[2]);

		const auto orientation = d->renderer->getCamera()->orientation.getValue();
		SbVec3f axis;
		float radian;
		orientation.getValue(axis, radian);
		d->ui.xAxisSpin->setValue(axis[0]);
		d->ui.yAxisSpin->setValue(axis[1]);
		d->ui.zAxisSpin->setValue(axis[2]);
		d->ui.angleSpin->setValue(radian);
	}
}
