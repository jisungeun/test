#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QStack>
#include <QStandardPaths>
#include <QTimer>

#include "JsonStorage.h"

namespace CellAnalyzer::Database {
	struct JsonStorage::Impl {
		QString filepath;
		QJsonObject obj;
		QTimer timer;

		static auto ToJson(const QVariantMap& map) -> QJsonObject;
		static auto ToArray(const QVariantList& list) -> QJsonArray;

		auto Flushable() -> void;
	};

	auto JsonStorage::Impl::ToJson(const QVariantMap& map) -> QJsonObject {
		QJsonObject obj;

		for (const auto& k : map.keys()) {
			switch (map[k].type()) {
				case QVariant::Bool:
					obj[k] = map[k].toBool();
					break;
				case QVariant::Int:
					obj[k] = map[k].toInt();
					break;
				case QVariant::LongLong:
					obj[k] = map[k].toLongLong();
					break;
				case QVariant::Double:
					obj[k] = map[k].toDouble();
					break;
				case QVariant::String:
					obj[k] = map[k].toString();
					break;
			}
		}

		return obj;
	}

	auto JsonStorage::Impl::ToArray(const QVariantList& list) -> QJsonArray {
		QJsonArray array;

		for (const auto& l : list) {
			const auto map = l.toMap();
			QJsonObject obj;

			for (const auto& k : map.keys()) {
				switch (map[k].type()) {
					case QVariant::Bool:
						obj[k] = map[k].toBool();
						break;
					case QVariant::Int:
						obj[k] = map[k].toInt();
						break;
					case QVariant::LongLong:
						obj[k] = map[k].toLongLong();
						break;
					case QVariant::Double:
						obj[k] = map[k].toDouble();
						break;
					case QVariant::String:
						obj[k] = map[k].toString();
						break;
				}
			}

			array.push_back(obj);
		}

		return array;
	}

	auto JsonStorage::Impl::Flushable() -> void {
		timer.stop();
		timer.start();
	}

	JsonStorage::JsonStorage() : IDatabase(), IAppModule(), d(new Impl) {
		d->timer.setInterval(10000);

		connect(&d->timer, &QTimer::timeout, this, &JsonStorage::OnTimeout);
	}

	JsonStorage::~JsonStorage() {
		Flush();
	}

	auto JsonStorage::Start() -> std::optional<Error> {
		d->filepath = QString("%1/%2").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation).replace("CellAnalyzer", "TomoAnalysis")).arg("settings.json");
		QFile file(d->filepath);

		if (!file.exists()) {
			SetInstance(this);
			return std::nullopt;
		}

		if (file.open(QIODevice::ReadWrite)) {
			QJsonParseError error {};
			const auto bytes = file.readAll();
			const auto doc = QJsonDocument::fromJson(bytes, &error);

			if (error.error == QJsonParseError::NoError)
				d->obj = doc.object();

			SetInstance(this);
			return std::nullopt;
		}

		return "Database could not be initialized: " + file.errorString();
	}

	auto JsonStorage::Stop() -> void {
		Flush();
	}

	auto JsonStorage::Contains(const QString& key) const -> bool {
		const auto splits = key.split("\\");
		QJsonValue obj = d->obj;

		for (const auto& s : splits) {
			if (!obj.isObject() || !obj.toObject().contains(s))
				return false;

			obj = obj[s];
		}

		return true;
	}

	auto JsonStorage::IsArray(const QString& key) const -> bool {
		const auto splits = key.split("\\");
		QJsonValue obj = d->obj;

		for (const auto& s : splits) {
			if (!obj.isObject() || !obj.toObject().contains(s))
				return false;

			obj = obj[s];
		}

		return obj.isArray();
	}

	auto JsonStorage::GetMap(const QString& key) const -> std::optional<QVariantMap> {
		const auto splits = key.split("\\");
		auto obj = d->obj;

		for (const auto& s : splits) {
			if (!obj.isEmpty())
				obj = obj[s].toObject();
			else
				return {};
		}

		if (!obj.isEmpty())
			return obj.toVariantMap();

		return {};
	}

	auto JsonStorage::GetList(const QString& key) const -> QVariantList {
		const auto splits = key.split("\\");
		QJsonValue obj = d->obj;

		for (const auto& s : splits) {
			if (obj.isObject())
				obj = obj[s];
			else
				return {};
		}

		if (obj.isArray())
			return obj.toArray().toVariantList();

		return {};
	}

	auto JsonStorage::Save(const QString& key, const QVariantMap& value) -> bool {
		const auto splits = key.split("\\");
		auto obj = d->obj;
		QStack<QJsonObject> stack;

		for (const auto& s : splits) {
			if (obj[s].isObject())
				obj = obj[s].toObject();
			else
				obj = QJsonObject();

			stack.push(obj);
		}

		stack.top() = d->ToJson(value);

		for (auto i = splits.count() - 1; i >= 0; i--) {
			obj = stack.pop();

			if (stack.isEmpty())
				d->obj[splits[i]] = obj;
			else
				stack.top()[splits[i]] = obj;
		}

		d->Flushable();
		return true;
	}

	auto JsonStorage::Save(const QString& key, const QVariantList& value) -> bool {
		const auto splits = key.split("\\");
		QJsonValue obj = d->obj;
		QStack<QJsonValue> stack;

		for (const auto& s : splits) {
			obj = obj[s];
			stack.push(obj);
		}

		stack.top() = d->ToArray(value);

		for (auto i = splits.count() - 1; i >= 0; i--) {
			obj = stack.pop();

			if (stack.isEmpty())
				d->obj[splits[i]] = obj;
			else {
				auto top = stack.top().toObject();
				top[splits[i]] = obj;
				stack.top() = top;
			}
		}

		d->Flushable();
		return true;
	}

	auto JsonStorage::Flush() -> void {
		const QJsonDocument doc(d->obj);

		if (QFile file(d->filepath); file.open(QIODevice::WriteOnly)) {
			const auto json = doc.toJson();
			file.write(doc.toJson());
		}
	}

	auto JsonStorage::OnTimeout() -> void {
		Flush();
		d->timer.stop();
	}
}
