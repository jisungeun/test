#pragma once

#include "IAppModule.h"
#include "IDatabase.h"

#include "CellAnalyzer.Database.JsonStorageExport.h"

namespace CellAnalyzer::Database {
	class CellAnalyzer_Database_JsonStorage_API JsonStorage final : public QObject, public IDatabase, public IAppModule {
	public:
		JsonStorage();
		~JsonStorage() override;

		auto Start() -> std::optional<Error> override;
		auto Stop() -> void override;

		auto Contains(const QString& key) const -> bool override;
		auto IsArray(const QString& key) const -> bool override;

		auto GetMap(const QString& key) const -> std::optional<QVariantMap> override;
		auto GetList(const QString& key) const -> QVariantList override;

		auto Save(const QString& key, const QVariantMap& value) -> bool override;
		auto Save(const QString& key, const QVariantList& value) -> bool override;

		auto Flush() -> void override;

	protected slots:
		auto OnTimeout() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
