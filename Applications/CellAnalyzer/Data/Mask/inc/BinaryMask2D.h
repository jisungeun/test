#pragma once

#include "IData.h"
#include "IMask.h"
#include "IVolume2D.h"

#include "CellAnalyzer.Data.MaskExport.h"

namespace CellAnalyzer::Data {
	class CellAnalyzer_Data_Mask_API BinaryMask2D final : public IData, public IVolume2D, public IMask {
	public:
		explicit BinaryMask2D(const std::shared_ptr<uint16_t[]>& data, const Size2D& size, const Resolution2D& resolution, const Origin2D& origin, int timestep = 0);
		~BinaryMask2D() override;

		auto GetFlags() const -> DataFlags override;
		auto GetTimeStep() const -> int override;

		auto GetDataType() const -> DataType override;
		auto GetDataSize() const -> uint64_t override;
		auto GetData() const -> void* override;

		auto GetSize() const -> Size2D override;
		auto GetResolution() const -> Resolution2D override;
		auto GetOrigin() const -> Origin2D override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
