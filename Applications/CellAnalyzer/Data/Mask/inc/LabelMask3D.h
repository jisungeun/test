#pragma once

#include "IData.h"
#include "ILabeled.h"
#include "IMask.h"
#include "IVolume3D.h"

#include "CellAnalyzer.Data.MaskExport.h"

namespace CellAnalyzer::Data {
	class CellAnalyzer_Data_Mask_API LabelMask3D final : public IData, public IVolume3D, public IMask, public ILabeled {
	public:
		explicit LabelMask3D(const std::shared_ptr<uint16_t[]>& data, int maxLabel, const Size3D& size, const Resolution3D& resolution, const Origin3D& origin, double offset = 0, int timestep = 0);
		~LabelMask3D() override;

		auto GetFlags() const -> DataFlags override;
		auto GetTimeStep() const -> int override;

		auto GetMaxIndex() const -> int override;

		auto GetDataType() const -> DataType override;
		auto GetDataSize() const -> uint64_t override;
		auto GetData() const -> void* override;

		auto GetSize() const -> Size3D override;
		auto GetResolution() const -> Resolution3D override;
		auto GetOrigin() const -> Origin3D override;
		auto GetZOffset() const -> double override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
