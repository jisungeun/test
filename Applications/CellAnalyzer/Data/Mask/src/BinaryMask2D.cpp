#include "BinaryMask2D.h"

namespace CellAnalyzer::Data {
	struct BinaryMask2D::Impl {
		std::shared_ptr<uint16_t[]> data = nullptr;
		Size2D size;
		Resolution2D resolution;
		Origin2D origin;
		int timestep { 0 };
	};

	BinaryMask2D::BinaryMask2D(const std::shared_ptr<uint16_t[]>& data, const Size2D& size, const Resolution2D& resolution, const Origin2D& origin, int timestep) : IData(), IVolume2D(), IMask(), d(new Impl) {
		d->data = data;
		d->size = size;
		d->resolution = resolution;
		d->origin = origin;
		d->timestep = timestep;
	}

	BinaryMask2D::~BinaryMask2D() = default;

	auto BinaryMask2D::GetFlags() const -> DataFlags {
		return DataFlag::Volume2D | DataFlag::Binary;
	}

	auto BinaryMask2D::GetTimeStep() const -> int {
		return d->timestep;
	}

	auto BinaryMask2D::GetDataType() const -> DataType {
		return DataType::UInt16;
	}

	auto BinaryMask2D::GetDataSize() const -> uint64_t {
		return d->size.x * d->size.y * sizeof(uint16_t);
	}

	auto BinaryMask2D::GetData() const -> void* {
		return d->data.get();
	}

	auto BinaryMask2D::GetSize() const -> Size2D {
		return d->size;
	}

	auto BinaryMask2D::GetResolution() const -> Resolution2D {
		return d->resolution;
	}

	auto BinaryMask2D::GetOrigin() const -> Origin2D {
		return d->origin;
	}
}
