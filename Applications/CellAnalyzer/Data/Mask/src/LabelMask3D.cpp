#include "LabelMask3D.h"

namespace CellAnalyzer::Data {
	struct LabelMask3D::Impl {
		std::shared_ptr<uint16_t[]> data = nullptr;
		int maxLabel = 0;
		Size3D size;
		Resolution3D resolution;
		Origin3D origin;
		double offset { 0 };
		int timestep { 0 };
	};

	LabelMask3D::LabelMask3D(const std::shared_ptr<uint16_t[]>& data, int maxLabel, const Size3D& size, const Resolution3D& resolution, const Origin3D& origin, double offset, int timestep) : IData(), IVolume3D(), IMask(), ILabeled(), d(new Impl) {
		d->data = data;
		d->maxLabel = maxLabel;
		d->size = size;
		d->resolution = resolution;
		d->origin = origin;
		d->offset = offset;
		d->timestep = timestep;
	}

	LabelMask3D::~LabelMask3D() = default;

	auto LabelMask3D::GetFlags() const -> DataFlags {
		return DataFlag::Volume3D | DataFlag::Binary | DataFlag::Label;
	}

	auto LabelMask3D::GetTimeStep() const -> int {
		return d->timestep;
	}

	auto LabelMask3D::GetMaxIndex() const -> int {
		return d->maxLabel;
	}

	auto LabelMask3D::GetDataType() const -> DataType {
		return DataType::UInt16;
	}

	auto LabelMask3D::GetDataSize() const -> uint64_t {
		return d->size.x * d->size.y * d->size.z * sizeof(uint16_t);
	}

	auto LabelMask3D::GetData() const -> void* {
		return d->data.get();
	}

	auto LabelMask3D::GetSize() const -> Size3D {
		return d->size;
	}

	auto LabelMask3D::GetResolution() const -> Resolution3D {
		return d->resolution;
	}

	auto LabelMask3D::GetOrigin() const -> Origin3D {
		return d->origin;
	}

	auto LabelMask3D::GetZOffset() const -> double {
		return d->offset;
	}
}
