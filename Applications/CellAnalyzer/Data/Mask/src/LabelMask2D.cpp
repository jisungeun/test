#include "LabelMask2D.h"

namespace CellAnalyzer::Data {
	struct LabelMask2D::Impl {
		std::shared_ptr<uint16_t[]> data = nullptr;
		int maxLabel = 0;
		Size2D size;
		Resolution2D resolution;
		Origin2D origin;
		int timestep { 0 };
	};

	LabelMask2D::LabelMask2D(const std::shared_ptr<uint16_t[]>& data, int maxLabel, const Size2D& size, const Resolution2D& resolution, const Origin2D& origin, int timestep) : IData(), IVolume2D(), IMask(), ILabeled(), d(new Impl) {
		d->data = data;
		d->maxLabel = maxLabel;
		d->size = size;
		d->resolution = resolution;
		d->origin = origin;
		d->timestep = timestep;
	}

	LabelMask2D::~LabelMask2D() = default;

	auto LabelMask2D::GetFlags() const -> DataFlags {
		return DataFlag::Volume2D | DataFlag::Binary | DataFlag::Label;
	}

	auto LabelMask2D::GetTimeStep() const -> int {
		return d->timestep;
	}

	auto LabelMask2D::GetMaxIndex() const -> int {
		return d->maxLabel;
	}

	auto LabelMask2D::GetDataType() const -> DataType {
		return DataType::UInt16;
	}

	auto LabelMask2D::GetDataSize() const -> uint64_t {
		return d->size.x * d->size.y * sizeof(uint16_t);
	}

	auto LabelMask2D::GetData() const -> void* {
		return d->data.get();
	}

	auto LabelMask2D::GetSize() const -> Size2D {
		return d->size;
	}

	auto LabelMask2D::GetResolution() const -> Resolution2D {
		return d->resolution;
	}

	auto LabelMask2D::GetOrigin() const -> Origin2D {
		return d->origin;
	}
}
