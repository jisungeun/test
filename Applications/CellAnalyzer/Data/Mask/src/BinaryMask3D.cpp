#include "BinaryMask3D.h"

namespace CellAnalyzer::Data {
	struct BinaryMask3D::Impl {
		std::shared_ptr<uint16_t[]> data = nullptr;
		Size3D size;
		Resolution3D resolution;
		Origin3D origin;
		double offset { 0 };
		int timestep { 0 };
	};

	BinaryMask3D::BinaryMask3D(const std::shared_ptr<uint16_t[]>& data, const Size3D& size, const Resolution3D& resolution, const Origin3D& origin, double offset, int timestep) : IData(), IVolume3D(), IMask(), d(new Impl) {
		d->data = data;
		d->size = size;
		d->resolution = resolution;
		d->origin = origin;
		d->offset = offset;
		d->timestep = timestep;
	}

	BinaryMask3D::~BinaryMask3D() = default;

	auto BinaryMask3D::GetFlags() const -> DataFlags {
		return DataFlag::Volume3D | DataFlag::Binary;
	}

	auto BinaryMask3D::GetTimeStep() const -> int {
		return d->timestep;
	}

	auto BinaryMask3D::GetDataType() const -> DataType {
		return DataType::UInt16;
	}

	auto BinaryMask3D::GetDataSize() const -> uint64_t {
		return d->size.x * d->size.y * d->size.z * sizeof(uint16_t);
	}

	auto BinaryMask3D::GetData() const -> void* {
		return d->data.get();
	}

	auto BinaryMask3D::GetSize() const -> Size3D {
		return d->size;
	}

	auto BinaryMask3D::GetResolution() const -> Resolution3D {
		return d->resolution;
	}

	auto BinaryMask3D::GetOrigin() const -> Origin3D {
		return d->origin;
	}

	auto BinaryMask3D::GetZOffset() const -> double {
		return d->offset;
	}
}
