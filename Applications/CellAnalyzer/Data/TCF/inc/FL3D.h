#pragma once

#include "IData.h"
#include "IFL.h"
#include "IVolume3D.h"

#include "CellAnalyzer.Data.TCFExport.h"

namespace CellAnalyzer::Data {
	class CellAnalyzer_Data_TCF_API FL3D final : public IData, public IVolume3D, public IFL {
	public:
		explicit FL3D(const QString& filepath, int channel, int index);
		explicit FL3D(std::shared_ptr<uint16_t[]>&& data, int channelIndex, const QString& channelName, Size3D size, const Resolution3D& resolution, const Origin3D& origin, FLIntensity intensity, double offset = 0, int timestep = 0);
		~FL3D() override;

		auto IsValid() const -> bool;

		auto GetFlags() const -> DataFlags override;
		auto GetTimeStep() const -> int override;

		auto GetIntensity() const -> FLIntensity override;
		auto GetChannelName() const -> QString override;
		auto GetChannelIndex() const -> int override;
		auto GetZOffset() const -> double override;
		auto GetChannelColor() const -> FLColor override;
		auto GetChannelEmission() const -> double override;
		auto GetChannelExcitation() const -> double override;
		auto GetChannelExposureTime() const -> int override;

		auto GetDataType() const -> DataType override;
		auto GetDataSize() const -> uint64_t override;
		auto GetData() const -> void* override;

		auto GetSize() const -> Size3D override;
		auto GetResolution() const -> Resolution3D override;
		auto GetOrigin() const -> Origin3D override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
