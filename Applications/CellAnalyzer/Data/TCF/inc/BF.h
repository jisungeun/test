#pragma once

#include "IBF.h"
#include "IData.h"
#include "IVolume2D.h"

#include "CellAnalyzer.Data.TCFExport.h"

namespace CellAnalyzer::Data {
	class CellAnalyzer_Data_TCF_API BF final : public IData, public IVolume2D, public IBF {
	public:
		explicit BF(const QString& filepath, int index);
		~BF() override;

		auto IsValid() const -> bool;

		auto GetFlags() const -> DataFlags override;
		auto GetTimeStep() const -> int override;

		auto GetDataType() const -> DataType override;
		auto GetDataSize() const -> uint64_t override;
		auto GetData() const -> void* override;

		auto GetSize() const -> Size2D override;
		auto GetResolution() const -> Resolution2D override;
		auto GetOrigin() const -> Origin2D override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
