#pragma once

#include "IData.h"
#include "IHT.h"
#include "IVolume3D.h"

#include "CellAnalyzer.Data.TCFExport.h"

namespace CellAnalyzer::Data {
	class CellAnalyzer_Data_TCF_API HT3D final : public IData, public IVolume3D, public IHT {
	public:
		explicit HT3D(const QString& filepath, int index);
		explicit HT3D(std::shared_ptr<uint16_t[]>&& data, Size3D size, Resolution3D resolution, Origin3D origin, RIRange range, int timestep = 0);
		~HT3D() override;

		auto IsValid() const -> bool;

		auto GetFlags() const -> DataFlags override;
		auto GetTimeStep() const -> int override;

		auto GetRI() const -> RIRange override;

		auto GetDataType() const -> DataType override;
		auto GetDataSize() const -> uint64_t override;
		auto GetData() const -> void* override;

		auto GetSize() const -> Size3D override;
		auto GetResolution() const -> Resolution3D override;
		auto GetOrigin() const -> Origin3D override;
		auto GetZOffset() const -> double override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
