#pragma once

#include "IData.h"
#include "IFL.h"
#include "IVolume2D.h"

#include "CellAnalyzer.Data.TCFExport.h"

namespace CellAnalyzer::Data {
	class CellAnalyzer_Data_TCF_API FL2D final : public IData, public IVolume2D, public IFL {
	public:
		explicit FL2D(const QString& filepath, int channel, int index);
		explicit FL2D(std::shared_ptr<uint16_t[]>&& data, int channelIndex, const QString& channelName, Size2D size, Resolution2D resolution, Origin2D origin, FLIntensity intensity, double offset, int timestep);
		~FL2D() override;

		auto IsValid() const -> bool;

		auto GetFlags() const -> DataFlags override;
		auto GetTimeStep() const -> int override;

		auto GetIntensity() const -> FLIntensity override;
		auto GetChannelName() const -> QString override;
		auto GetChannelIndex() const -> int override;
		auto GetChannelColor() const -> FLColor override;

		auto GetChannelEmission() const -> double override;
		auto GetChannelExcitation() const -> double override;
		auto GetChannelExposureTime() const -> int override;

		auto GetDataType() const -> DataType override;
		auto GetDataSize() const -> uint64_t override;
		auto GetData() const -> void* override;

		auto GetSize() const -> Size2D override;
		auto GetResolution() const -> Resolution2D override;
		auto GetOrigin() const -> Origin2D override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
