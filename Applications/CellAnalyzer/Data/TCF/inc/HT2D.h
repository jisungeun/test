#pragma once

#include "IData.h"
#include "IHT.h"
#include "IVolume2D.h"

#include "CellAnalyzer.Data.TCFExport.h"

namespace CellAnalyzer::Data {
	class CellAnalyzer_Data_TCF_API HT2D final : public IData, public IVolume2D, public IHT {
	public:
		explicit HT2D(const QString& filepath, int index);
		explicit HT2D(std::shared_ptr<uint16_t[]>&& data, Size2D size, Resolution2D resolution, Origin2D origin, RIRange range, int timestep = 0);
		~HT2D() override;

		auto IsValid() const -> bool;

		auto GetFlags() const -> DataFlags override;
		auto GetTimeStep() const -> int override;

		auto GetRI() const -> RIRange override;

		auto GetDataType() const -> DataType override;
		auto GetDataSize() const -> uint64_t override;
		auto GetData() const -> void* override;

		auto GetSize() const -> Size2D override;
		auto GetResolution() const -> Resolution2D override;
		auto GetOrigin() const -> Origin2D override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
