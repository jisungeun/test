#include "HT3D.h"

#include "TCFReader.h"

namespace CellAnalyzer::Data {
	struct HT3D::Impl {
		Size3D size;
		RIRange range;
		Resolution3D resolution;
		Origin3D origin;
		int timestep;
		std::shared_ptr<const uint16_t[]> data = nullptr;
	};

	HT3D::HT3D(const QString& filepath, int index) : IData(), d(new Impl) {
		const IO::File::TCFReader reader(filepath);

		if (const auto ht = reader.GetHT()) {
			if (const auto image = ht->Read(index)) {
				const auto size = image->GetSize();
				const auto res = image->GetResolution();
				const auto [min, max] = image->GetRange();

				d->size.x = size.x;
				d->size.y = size.y;
				d->size.z = size.z;
				d->resolution.x = res.x;
				d->resolution.y = res.y;
				d->resolution.z = res.z;
				d->range.min = min / 10000.0;
				d->range.max = max / 10000.0;
				d->timestep = index;
				d->data = image->GetBuffer();
			}
		}
	}

	HT3D::HT3D(std::shared_ptr<uint16_t[]>&& data, Size3D size, Resolution3D resolution, Origin3D origin, RIRange range, int timestep) : IData(), d(new Impl) {
		d->size = size;
		d->resolution = resolution;
		d->origin = origin;
		d->range = range;
		d->data = data;
		d->timestep = timestep;
	}

	HT3D::~HT3D() = default;

	auto HT3D::IsValid() const -> bool {
		return d->data != nullptr;
	}

	auto HT3D::GetFlags() const -> DataFlags {
		return DataFlag::HT | DataFlag::Volume3D;
	}

	auto HT3D::GetTimeStep() const -> int {
		return d->timestep;
	}

	auto HT3D::GetRI() const -> RIRange {
		return d->range;
	}

	auto HT3D::GetDataType() const -> DataType {
		return DataType::UInt16;
	}

	auto HT3D::GetDataSize() const -> uint64_t {
		return d->size.x * d->size.y * d->size.z * sizeof(uint16_t);
	}

	auto HT3D::GetData() const -> void* {
		return const_cast<uint16_t*>(d->data.get());
	}

	auto HT3D::GetSize() const -> Size3D {
		return d->size;
	}

	auto HT3D::GetResolution() const -> Resolution3D {
		return d->resolution;
	}

	auto HT3D::GetOrigin() const -> Origin3D {
		return d->origin;
	}

	auto HT3D::GetZOffset() const -> double {
		return 0;
	}
}
