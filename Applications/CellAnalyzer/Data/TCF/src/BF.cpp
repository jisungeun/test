#include "BF.h"

#include "TCFReader.h"

namespace CellAnalyzer::Data {
	struct BF::Impl {
		Size2D size;
		Resolution2D resolution;
		Origin2D origin;
		int timestep { 0 };
		std::shared_ptr<const uint8_t[]> data = nullptr;
	};

	BF::BF(const QString& filepath, int index) : IData(), IVolume2D(), IBF(), d(new Impl) {
		const IO::File::TCFReader reader(filepath);
		d->timestep = index;
		if (const auto bf = reader.GetBF()) {
			if (const auto image = bf->Read(index)) {
				const auto [rgb, x, y] = image->GetSize();
				const auto [rx, ry] = image->GetResolution();

				d->size.x = x;
				d->size.y = y;
				d->resolution.x = rx;
				d->resolution.y = ry;
				d->timestep = index;
				d->data = image->GetBuffer();
			}
		}
	}

	BF::~BF() = default;

	auto BF::IsValid() const -> bool {
		return d->data != nullptr;
	}

	auto BF::GetFlags() const -> DataFlags {
		return DataFlag::BF | DataFlag::Volume2D;
	}

	auto BF::GetTimeStep() const -> int {
		return d->timestep;
	}

	auto BF::GetDataType() const -> DataType {
		return DataType::UInt8;
	}

	auto BF::GetDataSize() const -> uint64_t {
		return d->size.x * d->size.y * sizeof(uint8_t);
	}

	auto BF::GetData() const -> void* {
		return const_cast<uint8_t*>(d->data.get());
	}

	auto BF::GetSize() const -> Size2D {
		return d->size;
	}

	auto BF::GetResolution() const -> Resolution2D {
		return d->resolution;
	}

	auto BF::GetOrigin() const -> Origin2D {
		return d->origin;
	}
}
