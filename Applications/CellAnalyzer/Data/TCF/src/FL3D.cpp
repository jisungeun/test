#include "FL3D.h"

#include "TCFReader.h"

namespace CellAnalyzer::Data {
	struct FL3D::Impl {
		QString name;
		int channelIndex;
		Size3D size;
		Resolution3D resolution;
		Origin3D origin;
		FLIntensity intensity;
		FLColor color;

		double offset = 0;
		double emission = 0.0;
		double excitation = 0.0;
		int exposure = 0;
		int timestep { 0 };

		std::shared_ptr<const uint16_t[]> data = nullptr;
	};

	FL3D::FL3D(const QString& filepath, int channel, int index) : IData(), IVolume3D(), IFL(), d(new Impl) {
		const IO::File::TCFReader reader(filepath);
		d->timestep = index;
		if (const auto fl = reader.GetFL()) {
			if (const auto image = fl->Read(channel, index)) {
				const auto size = image->GetSize();
				const auto res = image->GetResolution();
				const auto [min, max] = image->GetRange();

				d->timestep = index;
				d->channelIndex = channel;
				d->name = fl->GetChannelName(channel);
				d->emission = fl->GetChannelEmission(channel);
				d->excitation = fl->GetChannelExcitation(channel);
				d->exposure = fl->GetChannelExposureTime(channel);
				d->offset = fl->GetZOffset();
				d->size.x = size.x;
				d->size.y = size.y;
				d->size.z = size.z;
				d->resolution.x = res.x;
				d->resolution.y = res.y;
				d->resolution.z = res.z;
				d->intensity.min = min;
				d->intensity.max = max;
				d->data = image->GetBuffer();
			}
		}
	}

	FL3D::FL3D(std::shared_ptr<uint16_t[]>&& data, int channelIndex, const QString& channelName, Size3D size, const Resolution3D& resolution, const Origin3D& origin, FLIntensity intensity, double offset, int timestep) : IData(), IVolume3D(), IFL(), d(new Impl) {
		d->size = size;
		d->resolution = resolution;
		d->origin = origin;
		d->intensity = intensity;
		d->data = data;
		d->channelIndex = channelIndex;
		d->name = channelName;
		d->offset = offset;
		d->timestep = timestep;
	}

	FL3D::~FL3D() = default;

	auto FL3D::IsValid() const -> bool {
		return d->data != nullptr;
	}

	auto FL3D::GetFlags() const -> DataFlags {
		return DataFlag::FL | DataFlag::Volume3D;
	}

	auto FL3D::GetTimeStep() const -> int {
		return d->timestep;
	}

	auto FL3D::GetIntensity() const -> FLIntensity {
		return d->intensity;
	}

	auto FL3D::GetChannelName() const -> QString {
		return d->name;
	}

	auto FL3D::GetChannelIndex() const -> int {
		return d->channelIndex;
	}

	auto FL3D::GetChannelColor() const -> FLColor {
		return d->color;
	}

	auto FL3D::GetChannelEmission() const -> double {
		return d->emission;
	}

	auto FL3D::GetChannelExcitation() const -> double {
		return d->excitation;
	}

	auto FL3D::GetChannelExposureTime() const -> int {
		return d->exposure;
	}

	auto FL3D::GetDataType() const -> DataType {
		return DataType::UInt16;
	}

	auto FL3D::GetDataSize() const -> uint64_t {
		return d->size.x * d->size.y * sizeof(uint16_t);
	}

	auto FL3D::GetData() const -> void* {
		return const_cast<uint16_t*>(d->data.get());
	}

	auto FL3D::GetSize() const -> Size3D {
		return d->size;
	}

	auto FL3D::GetResolution() const -> Resolution3D {
		return d->resolution;
	}

	auto FL3D::GetOrigin() const -> Origin3D {
		return d->origin;
	}

	auto FL3D::GetZOffset() const -> double {
		return d->offset;
	}
}
