#include "FL2D.h"

#include "TCFReader.h"

namespace CellAnalyzer::Data {
	struct FL2D::Impl {
		QString name;
		int channelIndex = 0;
		Size2D size;
		Resolution2D resolution;
		Origin2D origin;
		FLIntensity intensity;
		FLColor color;

		double offset = 0.0;
		double emission = 0.0;
		double excitation = 0.0;
		int exposure = 0;
		int timestep { 0 };
		
		std::shared_ptr<const uint16_t[]> data = nullptr;
	};

	FL2D::FL2D(const QString& filepath, int channel, int index) : IData(), IVolume2D(), IFL(), d(new Impl) {
		const IO::File::TCFReader reader(filepath);

		if (const auto fl = reader.GetFL()) {
			if (const auto image = fl->ReadMip(channel, index)) {
				const auto [rgb, x, y] = image->GetSize();
				const auto [rx, ry] = image->GetResolution();
				const auto [min, max] = image->GetRange();

				d->channelIndex = channel;
				d->name = fl->GetChannelName(channel);
				d->emission = fl->GetChannelEmission(channel);
				d->excitation = fl->GetChannelExcitation(channel);
				d->exposure = fl->GetChannelExposureTime(channel);
				d->size.x = x;
				d->size.y = y;
				d->resolution.x = rx;
				d->resolution.y = ry;
				d->intensity.min = static_cast<uint16_t>(min);
				d->intensity.max = static_cast<uint16_t>(max);
				d->data = image->GetBuffer();
				d->offset = fl->GetZOffset();
				d->timestep = index;
			}
		}
	}

	FL2D::FL2D(std::shared_ptr<uint16_t[]>&& data, int channelIndex, const QString& channelName, Size2D size, Resolution2D resolution, Origin2D origin, FLIntensity intensity, double offset, int timestep) : IData(), IVolume2D(), IFL(), d(new Impl) {
		d->size = size;
		d->resolution = resolution;
		d->origin = origin;
		d->intensity = intensity;
		d->data = data;
		d->channelIndex = channelIndex;
		d->name = channelName;
		d->offset = offset;
		d->timestep = timestep;
	}

	FL2D::~FL2D() = default;

	auto FL2D::IsValid() const -> bool {
		return d->data != nullptr;
	}

	auto FL2D::GetFlags() const -> DataFlags {
		return DataFlag::FL | DataFlag::Volume2D;
	}

	auto FL2D::GetTimeStep() const -> int {
		return d->timestep;
	}

	auto FL2D::GetIntensity() const -> FLIntensity {
		return d->intensity;
	}

	auto FL2D::GetChannelName() const -> QString {
		return d->name;
	}

	auto FL2D::GetChannelIndex() const -> int {
		return d->channelIndex;
	}

	auto FL2D::GetChannelColor() const -> FLColor {
		return d->color;
	}

	auto FL2D::GetChannelEmission() const -> double {
		return d->emission;
	}

	auto FL2D::GetChannelExcitation() const -> double {
		return d->excitation;
	}

	auto FL2D::GetChannelExposureTime() const -> int {
		return d->exposure;
	}

	auto FL2D::GetDataType() const -> DataType {
		return DataType::UInt16;
	}

	auto FL2D::GetDataSize() const -> uint64_t {
		return d->size.x * d->size.y * sizeof(uint16_t);
	}

	auto FL2D::GetData() const -> void* {
		return const_cast<uint16_t*>(d->data.get());
	}

	auto FL2D::GetSize() const -> Size2D {
		return d->size;
	}

	auto FL2D::GetResolution() const -> Resolution2D {
		return d->resolution;
	}

	auto FL2D::GetOrigin() const -> Origin2D {
		return d->origin;
	}
}
