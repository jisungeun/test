#include "HT2D.h"

#include "TCFReader.h"

namespace CellAnalyzer::Data {
	struct HT2D::Impl {
		Size2D size;
		RIRange range;
		Resolution2D resolution;
		Origin2D origin;
		int timestep { 0 };
		
		std::shared_ptr<const uint16_t[]> data = nullptr;
	};

	HT2D::HT2D(const QString& filepath, int index) : IData(), d(new Impl) {
		const IO::File::TCFReader reader(filepath);
		d->timestep = index;
		if (const auto ht = reader.GetHT()) {
			if (const auto image = ht->ReadMip(index)) {
				const auto [rgb, x, y] = image->GetSize();
				const auto [rx, ry] = image->GetResolution();
				const auto [min, max] = image->GetRange();

				d->size.x = x;
				d->size.y = y;
				d->resolution.x = rx;
				d->resolution.y = ry;
				d->range.min = min / 10000.0;
				d->range.max = max / 10000.0;
				d->timestep = index;
				d->data = image->GetBuffer();
			}
		}
	}

	HT2D::HT2D(std::shared_ptr<uint16_t[]>&& data, Size2D size, Resolution2D resolution, Origin2D origin, RIRange range, int timestep) : IData(), d(new Impl) {
		d->size = size;
		d->resolution = resolution;
		d->origin = origin;
		d->range = range;
		d->data = data;
		d->timestep = timestep;
	}

	HT2D::~HT2D() = default;

	auto HT2D::IsValid() const -> bool {
		return d->data != nullptr;
	}

	auto HT2D::GetFlags() const -> DataFlags {
		return DataFlag::HT | DataFlag::Volume2D;
	}

	auto HT2D::GetTimeStep() const -> int {
		return d->timestep;
	}

	auto HT2D::GetRI() const -> RIRange {
		return d->range;
	}

	auto HT2D::GetDataType() const -> DataType {
		return DataType::UInt16;
	}

	auto HT2D::GetDataSize() const -> uint64_t {
		return d->size.x * d->size.y * sizeof(uint16_t);
	}

	auto HT2D::GetData() const -> void* {
		return const_cast<uint16_t*>(d->data.get());
	}

	auto HT2D::GetSize() const -> Size2D {
		return d->size;
	}

	auto HT2D::GetResolution() const -> Resolution2D {
		return d->resolution;
	}

	auto HT2D::GetOrigin() const -> Origin2D {
		return d->origin;
	}
}
