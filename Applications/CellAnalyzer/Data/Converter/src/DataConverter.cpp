#include "DataConverter.h"

#include <TCDataConverter.h>

namespace CellAnalyzer::Data {
	auto DataConverter::ConvertToTCImage(const std::shared_ptr<HT2D> data) -> TCImage::Pointer {
		if (data == nullptr || data->GetData() == nullptr)
			return nullptr;

		int dim[3] = { 0, };
		double res[3] = { 0, };

		auto [dimX, dimY] = data->GetSize();
		dim[0] = dimX;
		dim[1] = dimY;
		auto [resX, resY] = data->GetResolution();
		res[0] = resX;
		res[1] = resY;		
		auto [riMin, riMax] = data->GetRI();

		TCDataConverter converter;
		auto image = converter.ArrToImage(static_cast<uint16_t*>(data->GetData()), dim, res);
		image->SetMinMax(riMin * 10000, riMax * 10000);
		image->SetTimeStep(data->GetTimeStep());
		return image;
	}

	auto DataConverter::ConvertToTCImage(const std::shared_ptr<HT3D> data) -> TCImage::Pointer {
		if (data == nullptr || data->GetData() == nullptr)
			return nullptr;

		int dim[3] = { 0, };
		double res[3] = { 0, };

		auto [dimX, dimY,dimZ] = data->GetSize();
		dim[0] = dimX;
		dim[1] = dimY;
		dim[2] = dimZ;
		auto [resX, resY, resZ] = data->GetResolution();
		res[0] = resX;
		res[1] = resY;
		res[2] = resZ;

		auto [riMin, riMax] = data->GetRI();

		TCDataConverter converter;
		auto image = converter.ArrToImage(static_cast<uint16_t*>(data->GetData()), dim, res);
		image->SetMinMax(riMin * 10000, riMax * 10000);
		image->SetTimeStep(data->GetTimeStep());
		return image;
	}

	auto DataConverter::ConvertToTCImage(const std::shared_ptr<FL2D> data) -> TCImage::Pointer {
		if (data == nullptr || data->GetData() == nullptr)
			return nullptr;

		int dim[3] = { 0, };
		double res[3] = { 0, };

		auto [dimX, dimY] = data->GetSize();
		dim[0] = dimX;
		dim[1] = dimY;
		auto [resX, resY] = data->GetResolution();
		res[0] = resX;
		res[1] = resY;
		auto [min, max] = data->GetIntensity();
		
		dim[0] = dimX;
		dim[1] = dimY;

		res[0] = resX;
		res[1] = resY;

		const auto chIndex = data->GetChannelIndex();

		TCDataConverter converter;
		const auto result = converter.ArrToImage(static_cast<uint16_t*>(data->GetData()), dim, res);
		if (result == nullptr)
			return nullptr;

		// set extra values		
		result->SetMinMax(min, max);
		result->setChannel(chIndex);
		result->SetTimeStep(data->GetTimeStep());		

		return result;
	}

	auto DataConverter::ConvertToTCImage(const std::shared_ptr<FL3D> data) -> TCImage::Pointer {
		if (data == nullptr || data->GetData() == nullptr)
			return nullptr;

		int dim[3] = { 0, };
		double res[3] = { 0, };

		auto [dimX, dimY, dimZ] = data->GetSize();
		dim[0] = dimX;
		dim[1] = dimY;
		dim[2] = dimZ;
		auto [resX, resY, resZ] = data->GetResolution();
		res[0] = resX;
		res[1] = resY;
		res[2] = resZ;

		auto [min, max] = data->GetIntensity();

		dim[0] = dimX;
		dim[1] = dimY;
		dim[2] = dimZ;

		res[0] = resX;
		res[1] = resY;
		res[2] = resZ;

		const auto chIndex = data->GetChannelIndex();
		const auto offset = data->GetZOffset();
		TCDataConverter converter;
		auto result = converter.ArrToImage(static_cast<uint16_t*>(data->GetData()), dim, res);
		if (result == nullptr)
			return nullptr;

		// set extra values		
		result->SetMinMax(min, max);
		result->SetOffset(data->GetZOffset());
		result->SetTimeStep(data->GetTimeStep());
		result->setChannel(data->GetChannelIndex());

		return result;
	}

	auto DataConverter::ConvertToTCMask(const std::shared_ptr<BinaryMask2D> data) -> TCMask::Pointer {
		if (data == nullptr || data->GetData() == nullptr)
			return nullptr;

		int dim[3] = { 0, };
		double res[3] = { 0, };

		const auto [dimX, dimY] = data->GetSize();
		const auto [resX, resY] = data->GetResolution();

		dim[0] = dimX;
		dim[1] = dimY;

		res[0] = resX;
		res[1] = resY;

		TCDataConverter converter;
		const auto result = converter.ArrToLabelMask(static_cast<uint16_t*>(data->GetData()), dim, res);
		if (result == nullptr)
			return nullptr;
		result->SetTimeStep(data->GetTimeStep());
		return result;
	}

	auto DataConverter::ConvertToTCMask(const std::shared_ptr<BinaryMask3D> data) -> TCMask::Pointer {
		if (data == nullptr || data->GetData() == nullptr)
			return nullptr;

		int dim[3] = { 0, };
		double res[3] = { 0, };

		const auto [dimX, dimY,dimZ] = data->GetSize();
		const auto [resX, resY,resZ] = data->GetResolution();

		dim[0] = dimX;
		dim[1] = dimY;
		dim[2] = dimZ;

		res[0] = resX;
		res[1] = resY;
		res[2] = resZ;


		TCDataConverter converter;
		auto result = converter.ArrToLabelMask(static_cast<uint16_t*>(data->GetData()), dim, res);
		if (result == nullptr)
			return nullptr;

		// set extra values
		result->SetOffset(data->GetZOffset());
		result->SetTimeStep(data->GetTimeStep());

		return result;
	}

	auto DataConverter::ConvertToTCMask(const std::shared_ptr<LabelMask2D> data) -> TCMask::Pointer {
		if (data == nullptr || data->GetData() == nullptr)
			return nullptr;

		int dim[3] = { 0, };
		double res[3] = { 0, };

		const auto [dimX, dimY] = data->GetSize();
		const auto [resX, resY] = data->GetResolution();

		dim[0] = dimX;
		dim[1] = dimY;

		res[0] = resX;
		res[1] = resY;

		TCDataConverter converter;
		const auto result = converter.ArrToLabelMask(static_cast<uint16_t*>(data->GetData()), dim, res);
		if(nullptr == result) {
			return nullptr;
		}
		result->SetTimeStep(data->GetTimeStep());
		return result;
	}

	auto DataConverter::ConvertToTCMask(const std::shared_ptr<LabelMask3D> data) -> TCMask::Pointer {
		if (data == nullptr || data->GetData() == nullptr)
			return nullptr;

		int dim[3] = { 0, };
		double res[3] = { 0, };

		const auto [dimX, dimY, dimZ] = data->GetSize();
		const auto [resX, resY, resZ] = data->GetResolution();

		dim[0] = dimX;
		dim[1] = dimY;
		dim[2] = dimZ;

		res[0] = resX;
		res[1] = resY;
		res[2] = resZ;
		
		TCDataConverter converter;
		auto result = converter.ArrToLabelMask(static_cast<uint16_t*>(data->GetData()), dim, res);
		if (result == nullptr)
			return nullptr;

		// set extra values
		result->SetOffset(data->GetZOffset());
		result->SetTimeStep(data->GetTimeStep());

		return result;
	}
}
