#pragma once

#pragma warning(disable:4309)

#include <type_traits>

#include <TCImage.h>
#include <TCMask.h>

#include <BinaryMask2D.h>
#include <BinaryMask3D.h>
#include <FL2D.h>
#include <FL3D.h>
#include <HT2D.h>
#include <HT3D.h>
#include <Float2d.h>
#include <Float3d.h>
#include <LabelMask2D.h>
#include <LabelMask3D.h>

#include "CellAnalyzer.Data.ConverterExport.h"

namespace CellAnalyzer::Data {
	/**
	 * @brief This class offer help to convert between CellAnalyzer::Data and TCDataTypes.
	 */
	class CellAnalyzer_Data_Converter_API DataConverter {
		// TODO: remove redundant codes
	public:
		static auto ConvertToTCImage(std::shared_ptr<HT2D> data) -> TCImage::Pointer;
		static auto ConvertToTCImage(std::shared_ptr<HT3D> data) -> TCImage::Pointer;
		static auto ConvertToTCImage(std::shared_ptr<FL2D> data) -> TCImage::Pointer;
		static auto ConvertToTCImage(std::shared_ptr<FL3D> data) -> TCImage::Pointer;

		template <class T>
		static auto ConvertToFloatData(const float* arr, int dimX, int dimY, int dimZ, double resX, double resY, double resZ, float min, float max, int timestep, float offset = 0.0) -> DataPtr {
			if (arr == nullptr) return nullptr;
			if (dimZ < 2)
				dimZ = 1;
			const auto bufferSize = dimX * dimY * dimZ;
			std::unique_ptr<float[]> resultArr(new float[bufferSize](), std::default_delete<float[]>());
			memcpy(resultArr.get(), arr, bufferSize * sizeof(float));

			DataPtr result{ nullptr };
			if (std::is_same_v<T, Float2D>) {
				const auto float2d = std::shared_ptr<Float2D>(new Float2D(std::move(resultArr), { dimX,dimY }, { resX,resY }, { -dimX * resX / 2,-dimY * resY / 2 }, { min,max }, timestep));
				result = float2d;
			}
			else if (std::is_same_v<T, Float3D>) {
				const auto float3d = std::shared_ptr<Float3D>(new Float3D(std::move(resultArr), { dimX,dimY,dimZ }, { resX,resY,resZ }, { -dimX * resX / 2,-dimY * resY / 2,-dimZ * resZ / 2 }, { min,max }, offset, timestep));
				result = float3d;
			}

			return result;
		}


		template <class T>
		static auto ConvertToHTData(const uint16_t* arr, int dimX, int dimY, int dimZ, double resX, double resY, double resZ, float min, float max, int timestep) -> DataPtr {
			if (arr == nullptr) return nullptr;
			if (dimZ < 2)
				dimZ = 1;
			const auto bufferSize = dimX * dimY * dimZ;
			std::unique_ptr<uint16_t[]> resultArr(new uint16_t[bufferSize](), std::default_delete<uint16_t[]>());
			memcpy(resultArr.get(), arr, bufferSize * sizeof(uint16_t));

			DataPtr result{ nullptr };
			if (std::is_same_v<T, HT2D>) {
				const auto ht2d = std::shared_ptr<HT2D>(new HT2D(std::move(resultArr), { dimX,dimY }, { resX,resY }, { -dimX * resX / 2,-dimY * resY / 2 }, { min/10000.0,max/10000.0 }, timestep));
				result = ht2d;
			}
			else if (std::is_same_v<T, HT3D>) {
				const auto ht3d = std::shared_ptr<HT3D>(new HT3D(std::move(resultArr),{dimX,dimY,dimZ},{resX,resY,resZ},{-dimX*resX / 2,-dimY*resY/2,-dimZ*resZ/2},{min/10000.0,max/10000.0},timestep));
				result = ht3d;
			}

			return result;
		}

		template <class T>
		static auto ConvertToHTData(const TCImage::Pointer data) -> DataPtr {
			static_assert(std::is_same_v<T, HT2D> || std::is_same_v<T, HT3D>, "This template type is unavailable as return type.");

			if (data == nullptr || data->GetBuffer() == nullptr)
				return nullptr;

			auto [dimX, dimY, dimZ] = data->GetSize();
			if (dimZ < 2)
				dimZ = 1;

			double res[3] = { 0, };
			data->GetResolution(res);
			const auto timestep = data->GetTimeStep();
			auto [originRIMin, originRIMax] = data->GetOriginalMinMax();
			auto [riMin, riMax] = data->GetMinMax();
						
			const auto bufferSize = dimX * dimY * dimZ;
			std::unique_ptr<uint16_t[]> resultArr(new uint16_t[bufferSize](), std::default_delete<uint16_t[]>());
			memcpy(resultArr.get(), data->GetBuffer(), bufferSize * sizeof(uint16_t));

			DataPtr result { nullptr };
			if (std::is_same_v<T, HT2D>) {
				const auto ht2d = std::shared_ptr<HT2D>(new HT2D(std::move(resultArr), { dimX,dimY }, { res[0],res[1] }, { -dimX * res[0] / 2,-dimY * res[1] / 2 }, {static_cast<double>(riMin)/10000.0, static_cast<double>(riMax)/10000.0 },timestep));

				result = ht2d;
			} else if (std::is_same_v<T, HT3D>) {
				const auto ht3d = std::shared_ptr<HT3D>(new HT3D(std::move(resultArr),{dimX,dimY},{res[0],res[1],res[2]},{ -dimX * res[0] / 2,-dimY * res[1] / 2 ,-dimZ*res[2] /2}, { static_cast<double>(riMin) / 10000.0, static_cast<double>(riMax) / 10000.0},timestep));

				result = ht3d;
			}

			return result;
		}

		template <class T>
		static auto ConvertToFLData(const uint16_t* arr, int chIdx, const QString& chName, int dimX, int dimY, int dimZ, double resX, double resY, double resZ, float min, float max, int timestep, float offset = 0.0) -> DataPtr {
			if (arr == nullptr) return nullptr;
			if (dimZ < 2)
				dimZ = 1;
			const auto bufferSize = dimX * dimY * dimZ;
			std::unique_ptr<uint16_t[]> resultArr(new uint16_t[bufferSize](), std::default_delete<uint16_t[]>());
			memcpy(resultArr.get(), arr, bufferSize * sizeof(uint16_t));

			DataPtr result{ nullptr };
			if (std::is_same_v<T, FL2D>) {
				const auto fl2d = std::shared_ptr<FL2D>(new FL2D(std::move(resultArr), chIdx, chName, { dimX,dimY }, { resX,resY }, { -dimX * resX / 2,-dimY * resY / 2 }, { static_cast<uint16_t>(min),static_cast<uint16_t>(max) }, offset, timestep));

				result = fl2d;
			}
			else if (std::is_same_v<T, FL3D>) {
				const auto fl3d = std::shared_ptr<FL3D>(new FL3D(std::move(resultArr),chIdx,chName,{dimX,dimY,dimZ},{resX,resY,resZ},{ -dimX * resX / 2,-dimY * resY / 2 ,-dimZ*resZ/2}, { static_cast<uint16_t>(min),static_cast<uint16_t>(max) }, offset, timestep));

				result = fl3d;
			}

			return result;
		}

		template <class T>
		static auto ConvertToFLData(const TCImage::Pointer data) -> DataPtr {
			static_assert(std::is_same_v<T, FL2D> || std::is_same_v<T, FL3D>, "This template type is unavailable as return type.");

			if (data == nullptr || data->GetBuffer() == nullptr)
				return nullptr;

			auto [dimX, dimY, dimZ] = data->GetSize();
			if (dimZ < 2)
				dimZ = 1;

			double res[3] = { 0, };
			data->GetResolution(res);
			const auto chIdx = data->getChannel();			
			auto [originRIMin, originRIMax] = data->GetOriginalMinMax();
			auto [flMin, flMax] = data->GetMinMax();
			const auto offset = data->GetOffset();
			const auto timestep = data->GetTimeStep();
			const auto bufferSize = dimX * dimY * dimZ;
			std::unique_ptr<uint16_t[]> resultArr(new uint16_t[bufferSize](), std::default_delete<uint16_t[]>());
			memcpy(resultArr.get(), data->GetBuffer(), bufferSize * sizeof(uint16_t));

			DataPtr result { nullptr };
			if (std::is_same_v<T, FL2D>) {
				const auto fl2d = std::shared_ptr<FL2D>(new FL2D(std::move(resultArr),chIdx,QString(),{dimX,dimY},{res[0],res[1]},{-dimX*res[0]/2,-dimY*res[1]/2},{static_cast<uint16_t>(flMin),static_cast<uint16_t>(flMax)},offset,timestep));

				result = fl2d;
			} else if (std::is_same_v<T, FL3D>) {
				const auto fl3d = std::shared_ptr<FL3D>(new FL3D(std::move(resultArr), chIdx, QString(), { dimX,dimY,dimZ }, { res[0],res[1],res[2] }, { -dimX * res[0] / 2,-dimY * res[1] / 2 ,-dimZ *res[2] /2}, { static_cast<uint16_t>(flMin),static_cast<uint16_t>(flMax) },offset,timestep));

				result = fl3d;
			}

			return result;
		}

		static auto ConvertToTCMask(std::shared_ptr<BinaryMask2D> data) -> TCMask::Pointer;
		static auto ConvertToTCMask(std::shared_ptr<BinaryMask3D> data) -> TCMask::Pointer;
		static auto ConvertToTCMask(std::shared_ptr<LabelMask2D> data) -> TCMask::Pointer;
		static auto ConvertToTCMask(std::shared_ptr<LabelMask3D> data) -> TCMask::Pointer;

		template <class T>
		static auto ConvertToMaskData(const TCMask::Pointer data) -> DataPtr {
			static_assert(
						std::is_same_v<T, BinaryMask2D> || std::is_same_v<T, BinaryMask3D> || std::is_same_v<T, LabelMask2D> || std::is_same_v<T, LabelMask3D>,
						"This template type is unavailable as return type."
						);

			if (data == nullptr || data->GetMaskVolume() == nullptr)
				return nullptr;

			auto [dimX, dimY, dimZ] = data->GetSize();
			if (dimZ < 2)
				dimZ = 1;

			auto [resX, resY, resZ] = data->GetResolution();
			const auto offset = data->GetOffset();
			const auto timestep = data->GetTimeStep();
			auto maxLabel = data->GetMaxLabelValue();
			const auto bufferSize = dimX * dimY * dimZ;
			std::unique_ptr<uint16_t[]> resultArr(new uint16_t[bufferSize](), std::default_delete<uint16_t[]>());
			memcpy(resultArr.get(), data->GetMaskVolume().get(), bufferSize * sizeof(uint16_t));

			DataPtr result { nullptr };
			if (std::is_same_v<T, BinaryMask2D>) {
				auto binary2d = std::shared_ptr<BinaryMask2D>(new BinaryMask2D(std::move(resultArr),{dimX,dimY},{resX,resY},{-dimX*resX/2,-dimY*resY/2},timestep));

				result = binary2d;
			} else if (std::is_same_v<T, BinaryMask3D>) {
				auto binary3d = std::shared_ptr<BinaryMask3D>(new BinaryMask3D(std::move(resultArr),{dimX,dimY,dimZ},{resX,resY,resZ}, { -dimX * resX / 2,-dimY * resY / 2 ,-dimZ*resZ/2},offset,timestep));

				result = binary3d;
			} else if (std::is_same_v<T, LabelMask2D>) {
				auto label2d = std::shared_ptr<LabelMask2D>(new LabelMask2D(std::move(resultArr), maxLabel,{ dimX,dimY }, { resX,resY }, { -dimX * resX / 2,-dimY * resY / 2 },timestep));

				result = label2d;
			} else if (std::is_same_v<T, LabelMask3D>) {
				auto label3d = std::shared_ptr<LabelMask3D>(new LabelMask3D(std::move(resultArr), maxLabel,{dimX,dimY,dimZ},{resX,resY,resZ}, { -dimX * resX / 2,-dimY * resY / 2 ,-dimZ * resZ / 2 },offset,timestep));

				result = label3d;
			}

			return result;
		}
	};
}
