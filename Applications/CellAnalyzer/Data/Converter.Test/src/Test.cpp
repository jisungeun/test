#define CATCH_CONFIG_MAIN

#include <ImageDev/ImageDev.h>

#include <QFile>

#include <H5Cpp.h>
#include <catch2/catch.hpp>

#include <DataList.h>
#include <IPluginAlgorithm.h>
#include <PluginRegistry.h>
#include <ScalarData.h>
#include <TCDataConverter.h>

#include <BinaryMask2D.h>
#include <DataConverter.h>
#include <HT2D.h>
#include <HT3D.h>


using namespace CellAnalyzer;


auto GetMask(const TCImage::Pointer image, int dimension) -> std::tuple<TCMask::Pointer, TCMask::Pointer> {
	// thresholding
	const auto autoThresholdModuleInstance = PluginRegistry::GetPlugin("org.tomocube.algorithm.segmentation.threshold");
	const auto autoThresholdModule = std::dynamic_pointer_cast<IPluginAlgorithm>(autoThresholdModuleInstance);

	const auto autoThresholdParam = autoThresholdModuleInstance->Parameter();
	autoThresholdParam->SetValue("Method", IParameter::ValueType(true));

	autoThresholdModule->SetInput(0, image);
	autoThresholdModule->Execute();

	const auto autoThresholdResult = std::dynamic_pointer_cast<TC::Framework::DataList>(autoThresholdModule->GetOutput(0));
	const auto ulThreshold = std::dynamic_pointer_cast<DataSet>(autoThresholdResult->GetData(0));

	const auto lower = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("LThreshold"))->ValueAsDouble();
	const auto upper = std::dynamic_pointer_cast<TC::Framework::ScalarData>(ulThreshold->GetData("UThreshold"))->ValueAsDouble();

	const auto thresholdingModuleInstance = PluginRegistry::GetPlugin("org.tomocube.algorithm.segmentation.manualthreshold");
	const auto thresholdingModule = std::dynamic_pointer_cast<IPluginAlgorithm>(thresholdingModuleInstance);

	const auto thresholdingParam = thresholdingModuleInstance->Parameter();
	thresholdingParam->SetValue("LThreshold", IParameter::ValueType(lower));
	thresholdingParam->SetValue("UThreshold", IParameter::ValueType(upper));

	thresholdingModule->SetInput(0, image);
	thresholdingModule->Execute();

	const auto instanceMask = std::dynamic_pointer_cast<TCMask>(thresholdingModule->GetOutput(0));

	// labeling
	IPluginModule::Pointer labelingModuleInstance { nullptr };
	if (dimension == 2)
		labelingModuleInstance = PluginRegistry::GetPlugin("org.tomocube.algorithm.labeling.connectedcomponent.2d");
	else if (dimension == 3)
		labelingModuleInstance = PluginRegistry::GetPlugin("org.tomocube.algorithm.labeling.connectedcomponent.3d");
	else
		return { nullptr, nullptr };

	const auto labelingModule = std::dynamic_pointer_cast<IPluginAlgorithm>(labelingModuleInstance);

	labelingModule->SetInput(0, instanceMask);
	labelingModule->Execute();

	const auto labelMask = std::dynamic_pointer_cast<TCMask>(labelingModule->GetOutput(0));

	return { instanceMask, labelMask };
}

TEST_CASE("DataConverter") {
	try {
		imagedev::init();
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}

	REQUIRE(QFile::exists(_TEST_DATA));

	// load algorithm plugins for making mask data
	QStringList plugins {
		"/segmentation/TC.Algorithm.Segmentation.ManualThreshold.dll",
		"/segmentation/TC.Algorithm.Segmentation.Threshold.dll",
		"/labeling/TC.Algorithm.Labeling.ConnectedComponent.2D.dll",
		"/labeling/TC.Algorithm.Labeling.ConnectedComponent.3D.dll",
	};

	for (auto& plugin : plugins)
		REQUIRE(PluginRegistry::LoadPlugin(QString("%1%2").arg(_PLUGIN_DIR).arg(plugin)) > 0);

	try {
		// load HT
		H5::H5File file(_TEST_DATA, H5F_ACC_RDONLY);
		auto group = file.openGroup("/Data/3D");
		auto dataSet = group.openDataSet("000000");

		auto GetResolutionAttrValue = [group](std::string_view name) -> float {
			float res;
			auto sizeAttr = group.openAttribute(name.data());
			sizeAttr.read(H5::PredType::NATIVE_FLOAT, &res);
			sizeAttr.close();

			return res;
		};

		double res[3];
		res[0] = GetResolutionAttrValue("ResolutionX");
		res[1] = GetResolutionAttrValue("ResolutionY");
		res[2] = GetResolutionAttrValue("ResolutionZ");

		auto GetSizeAttrValue = [group](std::string_view name) -> int64_t {
			int64_t size;
			auto sizeAttr = group.openAttribute(name.data());
			sizeAttr.read(H5::PredType::NATIVE_INT64, &size);
			sizeAttr.close();

			return size;
		};

		int size[3];
		size[0] = GetSizeAttrValue("SizeX");
		size[1] = GetSizeAttrValue("SizeY");
		size[2] = GetSizeAttrValue("SizeZ");

		auto data2d = std::make_unique<uint16_t[]>(size[0] * size[1]);
		auto data3d = std::make_unique<uint16_t[]>(size[0] * size[1] * size[2]);

		const hsize_t mcount[] { static_cast<hsize_t>(size[0]), static_cast<hsize_t>(size[1]) };
		const H5::DataSpace mSpace { 2, mcount };

		const hsize_t fstart[] { static_cast<hsize_t>(size[2] / 2), 0ULL, 0ULL };
		const hsize_t fcount[] { 1ULL, static_cast<hsize_t>(size[1]), static_cast<hsize_t>(size[0]) };
		const auto fSpace = dataSet.getSpace();
		fSpace.selectHyperslab(H5S_SELECT_SET, fcount, fstart);

		dataSet.read(data2d.get(), H5::PredType::NATIVE_UINT16, mSpace, fSpace);
		dataSet.read(data3d.get(), H5::PredType::NATIVE_UINT16);

		dataSet.close();
		group.close();
		file.close();

		auto ht2d = std::shared_ptr<Data::HT2D>(new Data::HT2D(std::move(data2d), { size[0],size[1] }, { res[0],res[1] }, { -size[0] * res[0] / 2, -size[1] * res[1] / 2 }, { 13000,15000 }));

		REQUIRE(ht2d->GetData() != nullptr);

		auto ht3d = std::shared_ptr<Data::HT3D>(new Data::HT3D(std::move(data3d), { size[0],size[1] ,size[2] }, { res[0],res[1] ,res[2] }, { -size[0] * res[0] / 2, -size[1] * res[1] / 2,-size[2] * res[2] / 2 }, { 13000,15000 }));

		REQUIRE(ht3d->GetData() != nullptr);

		SECTION("Convert between HTData2d and TCImage") {
			auto convertedTCImage2d = Data::DataConverter::ConvertToTCImage(ht2d);
			REQUIRE(convertedTCImage2d != nullptr);

			auto convertedHt2d = Data::DataConverter::ConvertToHTData<Data::HT2D>(convertedTCImage2d);
			REQUIRE(convertedHt2d != nullptr);

			SECTION("Convert 2D Mask data") {
				auto [instMask, labelMask] = GetMask(convertedTCImage2d, 2);
				REQUIRE(instMask != nullptr);
				REQUIRE(labelMask != nullptr);

				auto convertedBinary2d = std::dynamic_pointer_cast<Data::BinaryMask2D>(Data::DataConverter::ConvertToMaskData<Data::BinaryMask2D>(instMask));
				REQUIRE(convertedBinary2d != nullptr);

				auto convertedBinaryTCMask = Data::DataConverter::ConvertToTCMask(convertedBinary2d);
				REQUIRE(convertedBinaryTCMask != nullptr);

				auto convertedLabel2d = std::dynamic_pointer_cast<Data::LabelMask2D>(Data::DataConverter::ConvertToMaskData<Data::LabelMask2D>(labelMask));
				REQUIRE(convertedLabel2d != nullptr);

				auto convertedLabelTCMask = Data::DataConverter::ConvertToTCMask(convertedLabel2d);
				REQUIRE(convertedLabelTCMask != nullptr);
			}
		}

		SECTION("Convert HTData3d to TCImage") {
			auto convertedTCImage3d = Data::DataConverter::ConvertToTCImage(ht3d);
			REQUIRE(convertedTCImage3d != nullptr);

			auto convertedHt3d = Data::DataConverter::ConvertToHTData<Data::HT3D>(convertedTCImage3d);
			REQUIRE(convertedHt3d != nullptr);

			SECTION("Convert 3D Mask data") {
				auto [instMask, labelMask] = GetMask(convertedTCImage3d, 3);
				REQUIRE(instMask != nullptr);
				REQUIRE(labelMask != nullptr);

				auto convertedBinary3d = std::dynamic_pointer_cast<Data::BinaryMask3D>(Data::DataConverter::ConvertToMaskData<Data::BinaryMask3D>(instMask));
				REQUIRE(convertedBinary3d != nullptr);

				auto convertedBinaryTCMask = Data::DataConverter::ConvertToTCMask(convertedBinary3d);
				REQUIRE(convertedBinaryTCMask != nullptr);

				auto convertedLabel3d = std::dynamic_pointer_cast<Data::LabelMask3D>(Data::DataConverter::ConvertToMaskData<Data::LabelMask3D>(labelMask));
				REQUIRE(convertedLabel3d != nullptr);

				auto convertedLabelTCMask = Data::DataConverter::ConvertToTCMask(convertedLabel3d);
				REQUIRE(convertedLabelTCMask != nullptr);
			}
		}
	} catch (H5::Exception& e) {
		FAIL(e.getDetailMsg().c_str());
	} catch (imagedev::Exception& e) {
		FAIL(e.what().c_str());
	}

	imagedev::finish();
}
