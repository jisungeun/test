#include <Float2D.h>

namespace CellAnalyzer::Data {
	struct Float2D::Impl {
		std::shared_ptr<float[]> data { nullptr };
		Size2D size;
		Resolution2D resolution;
		Origin2D origin;
		int timestep { 0 };
		std::tuple<float, float> range;
	};

	Float2D::Float2D(const std::shared_ptr<float[]>& data, const Size2D& size, const Resolution2D& resolution, const Origin2D& origin, std::tuple<float, float> range, int timestep) : IData(), IVolume2D(), d { new Impl } {
		d->data = data;
		d->size = size;
		d->resolution = resolution;
		d->origin = origin;
		d->timestep = timestep;
		d->range = range;
	}

	Float2D::~Float2D() = default;

	auto Float2D::GetFlags() const -> DataFlags {
		return DataFlag::Volume2D;
	}

	auto Float2D::GetTimeStep() const -> int {
		return d->timestep;
	}

	auto Float2D::GetDataType() const -> DataType {
		return DataType::Float32;
	}

	auto Float2D::GetDataSize() const -> uint64_t {
		return d->size.x * d->size.y * sizeof(float);
	}

	auto Float2D::GetData() const -> void* {
		return d->data.get();
	}

	auto Float2D::GetSize() const -> Size2D {
		return d->size;
	}

	auto Float2D::GetResolution() const -> Resolution2D {
		return d->resolution;
	}

	auto Float2D::GetOrigin() const -> Origin2D {
		return d->origin;
	}

	auto Float2D::GetRange() const -> std::tuple<float, float> {
		return d->range;
	}
}
