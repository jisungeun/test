#include <Float3D.h>

namespace CellAnalyzer::Data {
	struct Float3D::Impl {
		std::shared_ptr<float[]> data { nullptr };
		Size3D size;
		Resolution3D resolution;
		Origin3D origin;
		double offset { 0 };
		int timestep { 0 };
		std::tuple<float, float> range;
	};

	Float3D::Float3D(const std::shared_ptr<float[]>& data, const Size3D& size, const Resolution3D& resolution, const Origin3D& origin, std::tuple<float, float> range, double offset, int timestep) : IData(), IVolume3D(), d { new Impl } {
		d->data = data;
		d->size = size;
		d->resolution = resolution;
		d->origin = origin;
		d->timestep = timestep;
		d->offset = offset;
		d->timestep = timestep;
		d->range = range;
	}

	Float3D::~Float3D() = default;

	auto Float3D::GetFlags() const -> DataFlags {
		return DataFlag::Volume3D;
	}

	auto Float3D::GetTimeStep() const -> int {
		return d->timestep;
	}

	auto Float3D::GetZOffset() const -> double {
		return d->offset;
	}

	auto Float3D::GetDataType() const -> DataType {
		return DataType::Float32;
	}

	auto Float3D::GetDataSize() const -> uint64_t {
		return d->size.x * d->size.y * d->size.z * sizeof(float);
	}

	auto Float3D::GetData() const -> void* {
		return d->data.get();
	}

	auto Float3D::GetSize() const -> Size3D {
		return d->size;
	}

	auto Float3D::GetResolution() const -> Resolution3D {
		return d->resolution;
	}

	auto Float3D::GetOrigin() const -> Origin3D {
		return d->origin;
	}

	auto Float3D::GetRange() const -> std::tuple<float, float> {
		return d->range;
	}
}
