#pragma once

#include "IData.h"
#include "IVolume3D.h"

#include "CellAnalyzer.Data.ImageExport.h"

namespace CellAnalyzer::Data {
	class CellAnalyzer_Data_Image_API Float3D final : public IData, public IVolume3D {
	public:
		explicit Float3D(const std::shared_ptr<float[]>& data, const Size3D& size, const Resolution3D& resolution, const Origin3D& origin, std::tuple<float, float> range, double offset = 0, int timestep = 0);
		~Float3D() override;

		auto GetFlags() const -> DataFlags override;
		auto GetTimeStep() const -> int override;

		auto GetDataType() const -> DataType override;
		auto GetDataSize() const -> uint64_t override;
		auto GetData() const -> void* override;

		auto GetSize() const -> Size3D override;
		auto GetResolution() const -> Resolution3D override;
		auto GetOrigin() const -> Origin3D override;
		auto GetZOffset() const -> double override;

		auto GetRange() const -> std::tuple<float, float>;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
