#pragma once

#include "IData.h"
#include "IVolume2D.h"

#include "CellAnalyzer.Data.ImageExport.h"

namespace CellAnalyzer::Data {
	class CellAnalyzer_Data_Image_API Float2D final : public IData, public IVolume2D {
	public:
		explicit Float2D(const std::shared_ptr<float[]>& data, const Size2D& size, const Resolution2D& resolution, const Origin2D& origin, std::tuple<float, float> range, int timestep = 0);
		~Float2D() override;

		auto GetFlags() const -> DataFlags override;
		auto GetTimeStep() const -> int override;

		auto GetDataType() const -> DataType override;
		auto GetDataSize() const -> uint64_t override;
		auto GetData() const -> void* override;

		auto GetSize() const -> Size2D override;
		auto GetResolution() const -> Resolution2D override;
		auto GetOrigin() const -> Origin2D override;

		auto GetRange() const -> std::tuple<float, float>;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
