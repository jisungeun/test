#pragma once

#include "IData.h"
#include "IMeasure.h"

#include "CellAnalyzer.Data.MeasureExport.h"

namespace CellAnalyzer::Data {
	class CellAnalyzer_Data_Measure_API Measure final : public IData, public IMeasure {
	public:
		explicit Measure(int timestep = 0);
		explicit Measure(const QVariantMap& measure, int timestep = 0);
		~Measure() override;

		auto Add(const QString& key, const QVariant& value) -> void;
		auto Remove(const QString& key) -> void;

		auto GetFlags() const -> DataFlags override;
		auto GetTimeStep() const -> int override;

		auto GetKeys() const -> QStringList override;
		auto GetValue(const QString& key) const -> QVariant override;
		auto GetMap() const -> QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
