#include "Measure.h"

namespace CellAnalyzer::Data {
	struct Measure::Impl {
		QVariantMap map;
		int timestep { 0 };
	};

	Measure::Measure(int timestep) : IData(), IMeasure(), d(new Impl) {
		d->timestep = timestep;
	}

	Measure::Measure(const QVariantMap& measure, int timestep) : IData(), IMeasure(), d(new Impl) {
		d->map = measure;
		d->timestep = timestep;
	}

	Measure::~Measure() = default;

	auto Measure::Add(const QString& key, const QVariant& value) -> void {
		d->map[key] = value;
	}

	auto Measure::Remove(const QString& key) -> void {
		d->map.remove(key);
	}

	auto Measure::GetFlags() const -> DataFlags {
		return DataFlag::Measure;
	}

	auto Measure::GetTimeStep() const -> int {
		return d->timestep;
	}

	auto Measure::GetKeys() const -> QStringList {
		return d->map.keys();
	}

	auto Measure::GetValue(const QString& key) const -> QVariant {
		if (d->map.contains(key))
			return d->map[key];

		return {};
	}

	auto Measure::GetMap() const -> QVariantMap {
		return d->map;
	}
}
