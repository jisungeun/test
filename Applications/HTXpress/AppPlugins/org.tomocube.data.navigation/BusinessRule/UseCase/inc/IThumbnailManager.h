#pragma once

#include <QImage>

#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IThumbnailManager {

    public:
        IThumbnailManager();
        virtual ~IThumbnailManager();

        virtual auto GetHTThumbnail(const QString& path, QImage& image)->bool = 0;
        virtual auto GetFLThumbnail(const QString& path, QImage& image)->bool = 0;
        virtual auto GetBFThumbnail(const QString& path, QImage& image)->bool = 0;
    };
}