#pragma once
#include <memory>
#include <QString>

#include "Experiment.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IConfigOutputPort {
    public:
        IConfigOutputPort();
        virtual ~IConfigOutputPort();

        virtual auto SetAcqInfo(const int& wellIndex, const AppEntity::Position& position) -> void = 0;
        virtual auto GetAcqInfo(int& wellIndex, AppEntity::Position& position) -> void = 0;
    };
}