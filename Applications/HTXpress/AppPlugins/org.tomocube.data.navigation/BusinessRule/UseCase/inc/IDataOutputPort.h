#pragma once
#include <memory>
#include <QString>

#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IDataOutputPort {
    public:
        IDataOutputPort();
        virtual ~IDataOutputPort();
        
        virtual auto ScannedExperimentData(const QString& user, const QString& project, const QString& experiment)->void = 0;

        virtual auto AddData(const QString& fileFullPath)->void = 0;
        virtual auto UpdateData(const QString& fileFullPath)->void = 0;
        virtual auto DeleteData(const QString& fileFullPath)->void = 0;
        virtual auto DeleteDataRootFolder(const QString& fileFullPath)->void = 0;
    };
}