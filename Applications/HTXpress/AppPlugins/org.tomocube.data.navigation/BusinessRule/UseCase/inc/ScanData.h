#pragma once

#include <memory>

#include <AppEntityDefines.h>
#include <IDataOutputPort.h>

#include "IUseCase.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API ScanData : public IUseCase {
    public:
        ScanData(IDataOutputPort* outputPort = nullptr);
        ~ScanData();

        auto SetExperiment(const QString& experiment) const ->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
