#pragma once

#include <Project.h>

#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IProjectReaderPort {

    protected:
        IProjectReaderPort();

    public:
        virtual ~IProjectReaderPort();
        static auto GetInstance()->IProjectReaderPort*;

        virtual auto Read(const QString& path, AppEntity::Project::Pointer& project) const -> bool = 0;
    };
}