#pragma once

#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IInitializationOutputPort {
    public:
        IInitializationOutputPort();
        virtual ~IInitializationOutputPort();

        virtual auto InitializeUI() -> void = 0;
    };
}