#pragma once
#include <memory>
#include <QString>

#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IUseCase {
    public:
        IUseCase(const QString& title);
        virtual ~IUseCase();

        auto Request()->bool;

        auto SetNormalLogEnabled(bool normalLogEnable)->void;
        auto SetErrorLogEnabled(bool enable)->void;

    protected:
        auto Print(const QString& message)->void;
        auto Error(const QString& error)->void;
        
        virtual auto Perform()->bool = 0;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}