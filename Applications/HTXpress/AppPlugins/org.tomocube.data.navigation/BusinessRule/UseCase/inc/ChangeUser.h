#pragma once

#include <memory>
#include <QString>

#include <AppEntityDefines.h>
#include <IUserOutputPort.h>

#include "IUseCase.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API ChangeUser : public IUseCase {
    public:
        ChangeUser(IUserOutputPort* outputPort = nullptr);
        ~ChangeUser() override;

        auto SetUser(const AppEntity::UserID& user)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
