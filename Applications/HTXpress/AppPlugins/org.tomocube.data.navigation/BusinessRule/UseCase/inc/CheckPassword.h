#pragma once
#include <memory>
#include <QString>

#include <AppEntityDefines.h>
#include <IUserOutputPort.h>

#include "IUseCase.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API CheckPassword : public IUseCase {
    public:
        CheckPassword(IUserOutputPort* outputPort = nullptr);
        ~CheckPassword() override;

        auto SetPassword(const QString& paswword)->void;
        auto Check() const->bool;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
