#pragma once

#include <memory>

#include <IUserOutputPort.h>

#include "IUseCase.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API LoadUserProjectList : public IUseCase {
    public:
        LoadUserProjectList(IUserOutputPort* outputPort = nullptr);
        ~LoadUserProjectList();

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
