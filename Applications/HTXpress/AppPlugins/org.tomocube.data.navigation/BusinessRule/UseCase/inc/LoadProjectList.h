#pragma once

#include <memory>

#include <AppEntityDefines.h>
#include <IProjectOutputPort.h>

#include "IUseCase.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API LoadProjectList : public IUseCase {
    public:
        LoadProjectList(IProjectOutputPort* outputPort = nullptr);
        ~LoadProjectList();

        auto SetUserID(const AppEntity::UserID& id)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
