#pragma once
#include <memory>
#include <QString>
#include <QImage>

#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IThumbnailOutputPort {
    public:
        IThumbnailOutputPort();
        virtual ~IThumbnailOutputPort();

        virtual auto LoadedThumbnail(const QString& path, const QImage& image) const -> void = 0;
    };
}