#pragma once
#include <memory>
#include <QString>

#include "IDataOutputPort.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IDataManager {
    protected:
        IDataManager();

    public:
        virtual ~IDataManager();
        static auto GetInstance()->IDataManager*;

        virtual auto SetScanExperiment(const QString& user, const QString& project, const QString& experiment)->void = 0;

        virtual auto InstallDataScannerOutputPort(UseCase::IDataOutputPort* outputPort)->void = 0;
        virtual auto UninstallDataScannerOutputPort(UseCase::IDataOutputPort* outputPort)->void = 0;

        virtual auto InstallDataMonitorOutputPort(UseCase::IDataOutputPort* outputPort)->void = 0;
        virtual auto UninstallDataMonitorOutputPort(UseCase::IDataOutputPort* outputPort)->void = 0;
    };
}