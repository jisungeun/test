#pragma once
#include <memory>

#include <User.h>

#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IUserManager {
    public:
        using Pointer = std::shared_ptr<IUserManager>;

    protected:
        IUserManager();

    public:
        virtual ~IUserManager();

        static auto GetInstance()->IUserManager*;

        virtual auto IsValid(const AppEntity::UserID& id, const QString& password) const->bool = 0;
    };
}