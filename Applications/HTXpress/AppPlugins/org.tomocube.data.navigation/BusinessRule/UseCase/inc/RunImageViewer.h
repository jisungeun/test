#pragma once

#include <memory>

#include "IUseCase.h"
#include "HTX_Data_Navigation_UseCaseExport.h"
#include "IImageViewerManager.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API RunImageViewer final : public IUseCase {
    public:
        explicit RunImageViewer(IImageViewerManager::Pointer imageViewerManager);
        ~RunImageViewer() override;

        auto SetTcfList(const QStringList& tcfList) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
