#pragma once

#include <memory>
#include <QString>

#include <AppEntityDefines.h>
#include <IProjectOutputPort.h>

#include "IUseCase.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API ChangeCurrentProject : public IUseCase {
    public:
        ChangeCurrentProject(IProjectOutputPort* outputPort = nullptr);
        ~ChangeCurrentProject();

        auto SetCurrentProject(const QString& projectTitle)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
