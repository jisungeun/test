#pragma once
#include <memory>
#include <QString>

#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IProjectOutputPort {
    public:
        IProjectOutputPort();
        virtual ~IProjectOutputPort();

        virtual auto UpdateProjectList(QList<QString> projects)->void = 0;

        virtual auto ChangeCurrentProject(const QString& projectTitle)->void = 0;
    };
}