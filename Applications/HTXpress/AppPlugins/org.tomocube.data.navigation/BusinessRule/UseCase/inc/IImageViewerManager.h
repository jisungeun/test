﻿#pragma once
#include <memory>
#include <QStringList>

#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IImageViewerManager {
    public:
        using Pointer = std::shared_ptr<IImageViewerManager>;

        IImageViewerManager() = default;
        virtual ~IImageViewerManager() = default;

        virtual auto SetTCFList(const QStringList& tcfList) -> void = 0;
        virtual auto Execute() const -> bool = 0;
    };
}
