#pragma once

#include "AppEntityDefines.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IUserOutputPort {
    public:
        IUserOutputPort();
        virtual ~IUserOutputPort();

        virtual auto ChangeUser(const AppEntity::UserID& user)->void = 0;
        virtual auto UpdateUserList(const QList<AppEntity::UserID>& users)->void = 0;
    };
}