#pragma once

#include <memory>

#include <IConfigOutputPort.h>

#include "IUseCase.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API LoadConfig : public IUseCase {
    public:
        LoadConfig(IConfigOutputPort* outputPort = nullptr);
        ~LoadConfig();

        auto SetPath(const QString& path)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
