#pragma once

#include <memory>

#include <IInitializationOutputPort.h>

#include "IUseCase.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API InitializeUI : public IUseCase {
    public:
        InitializeUI(IInitializationOutputPort* outputPort = nullptr);
        ~InitializeUI();

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
