#pragma once

#include <memory>

#include <IThumbnailOutputPort.h>
#include "IThumbnailManager.h"

#include "IUseCase.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API LoadFLThumbnail : public IUseCase {
    public:
        LoadFLThumbnail(IThumbnailManager* manager);
        ~LoadFLThumbnail();

        auto SetPath(const QString& path)->void;
        auto GetImage()->QImage&;
        auto IsThumbnail()->bool;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
