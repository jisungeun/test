#pragma once
#include <memory>
#include <QString>

#include "Experiment.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IExperimentOutputPort {
    public:
        IExperimentOutputPort();
        virtual ~IExperimentOutputPort();

        virtual auto LoadExperiment(const AppEntity::Experiment::Pointer& data) const -> void = 0;
        virtual auto LoadExperimentList(const QList<QString>& experiment)->void = 0;
    };
}