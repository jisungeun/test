#pragma once
#include <QString>

#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API IUseCaseLogger {
    public:
        IUseCaseLogger();
        virtual ~IUseCaseLogger();

        static auto PrintLog(const QString& useCase, const QString& message)->void;
        static auto PrintError(const QString& useCase, const QString& message)->void;

    protected:
        virtual auto Log(const QString& useCase, const QString& message)->void = 0;
        virtual auto Error(const QString& useCase, const QString& message)->void = 0;
    };
}