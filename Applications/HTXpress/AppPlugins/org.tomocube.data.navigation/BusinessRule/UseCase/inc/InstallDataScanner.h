#pragma once

#include <memory>

#include <IDataOutputPort.h>

#include "IUseCase.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API InstallDataScanner : public IUseCase {
    public:
        InstallDataScanner();
        ~InstallDataScanner();

        auto SetDataOutputPort(IDataOutputPort* outputPort)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
