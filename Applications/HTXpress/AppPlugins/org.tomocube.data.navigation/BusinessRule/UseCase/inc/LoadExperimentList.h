#pragma once

#include <memory>

#include <IExperimentOutputPort.h>

#include "IUseCase.h"
#include "HTX_Data_Navigation_UseCaseExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    class HTX_Data_Navigation_UseCase_API LoadExperimentList : public IUseCase {
    public:
        LoadExperimentList(IExperimentOutputPort* outputPort = nullptr);
        ~LoadExperimentList();

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
