#include <utility>

#include "RunImageViewer.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    struct RunImageViewer::Impl {
        QStringList tcfList{};
        IImageViewerManager::Pointer imageViewerManager{};
    };

    RunImageViewer::RunImageViewer(IImageViewerManager::Pointer imageViewerManager) : IUseCase("RunImageViewer"), d{new Impl} {
        d->imageViewerManager = std::move(imageViewerManager);
    }

    RunImageViewer::~RunImageViewer() {
    }

    auto RunImageViewer::SetTcfList(const QStringList& tcfList) -> void {
        d->tcfList = tcfList;
    }

    auto RunImageViewer::Perform() -> bool {
        if (d->imageViewerManager == nullptr) {
            return false;
        }

        d->imageViewerManager->SetTCFList(d->tcfList);
        return d->imageViewerManager->Execute();
    }
}
