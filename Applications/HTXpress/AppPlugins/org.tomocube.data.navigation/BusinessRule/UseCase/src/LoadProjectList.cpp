#include "LoadProjectList.h"

#include <QDirIterator>

#include <System.h>
#include <UserProjectRepo.h>

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct LoadProjectList::Impl {
		IProjectOutputPort* output{ nullptr };

        AppEntity::UserID id;
	};

    LoadProjectList::LoadProjectList(IProjectOutputPort* output)
    : IUseCase("LoadProjectList"), d{ new Impl } {
        d->output = output;
    }

    LoadProjectList::~LoadProjectList() {
    }

    auto LoadProjectList::SetUserID(const AppEntity::UserID& id) -> void {
        d->id = id;
    }

    auto LoadProjectList::Perform() -> bool {
        if (nullptr == d->output) {
            Error("Output port is not initialized");
            return false;
        }

        if (d->id.isEmpty()) {
            Error("Failed to get current user information.");
            return false;
        }
        
        const auto projects = Entity::UserProjectRepo::GetInstance()->GetProjects(d->id);
        d->output->UpdateProjectList(projects);

        return true;
    }
}
