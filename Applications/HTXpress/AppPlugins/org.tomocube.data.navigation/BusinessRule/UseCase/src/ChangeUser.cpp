#include "ChangeUser.h"

#include <AppData.h>
#include <UserProjectRepo.h>

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct ChangeUser::Impl {
		IUserOutputPort* output{ nullptr };

        AppEntity::UserID user;
	};

    ChangeUser::ChangeUser(IUserOutputPort* output) : IUseCase("ChangeUser"), d{ new Impl } {
        d->output = output;
    }

    ChangeUser::~ChangeUser() {
    }

    auto ChangeUser::SetUser(const AppEntity::UserID& user) -> void {
        d->user = user;
    }
    
    auto ChangeUser::Perform() -> bool {
        if (d->user.isEmpty()) {
            Error("User name is blank.");
            return false;
        }

        const auto users = Entity::UserProjectRepo::GetInstance()->GetUsers();
        if (!users.contains(d->user)) {
            Error(QString("User is not exist. - user=%1").arg(d->user));
            return false;
        }

        Entity::AppData::GetInstance()->SetUser(d->user);

        if (d->output) d->output->ChangeUser(d->user);

        return true;
    }
}
