#include "LoadFLThumbnail.h"

#include "IThumbnailManager.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct LoadFLThumbnail::Impl {
        IThumbnailManager* manager{ nullptr };

        QString path;
        QImage image;
        bool valid = false;
	};

    LoadFLThumbnail::LoadFLThumbnail(IThumbnailManager* manager)
    : IUseCase("LoadFLThumbnail"), d{ new Impl } {
        d->manager = manager;
    }

    LoadFLThumbnail::~LoadFLThumbnail() {
    }

    auto LoadFLThumbnail::SetPath(const QString& path)->void {
        d->path = path;
    }

    auto LoadFLThumbnail::GetImage()->QImage& {
        return d->image;
    }

    auto LoadFLThumbnail::IsThumbnail()->bool {
        return d->valid;
    }

    auto LoadFLThumbnail::Perform() -> bool {
        if (nullptr == d->manager) return false;
        if (d->path.isEmpty()) return false;

        d->valid = d->manager->GetFLThumbnail(d->path, d->image);

        return true;
    }
}
