#include <QStringList>

#include "IUseCaseLogger.h"
#include "IUseCase.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    struct IUseCase::Impl {
        QString useCaseTitle;

        bool normalLogEnable{ true };
        bool errorLogEnable{ true };

        auto Title() const->QString {
            return useCaseTitle;
        }
    };

    IUseCase::IUseCase(const QString& useCaseTitle) : d{ new Impl } {
        d->useCaseTitle = useCaseTitle;
    }

    IUseCase::~IUseCase() {
    }

    auto IUseCase::Request() -> bool {
        Print("> Start");

        if (!Perform()) {
            Print("> Failed");
            return false;
        }

        Print("> Success");
        return true;
    }

    auto IUseCase::SetNormalLogEnabled(bool enable) -> void {
        d->normalLogEnable = enable;
    }

    auto IUseCase::SetErrorLogEnabled(bool enable) -> void {
        d->errorLogEnable = enable;
    }

    auto IUseCase::Print(const QString& message) -> void {
        if(d->normalLogEnable) {
            IUseCaseLogger::PrintLog(d->Title(), message);
        }
    }
    
    auto IUseCase::Error(const QString& message) -> void {
        if(d->errorLogEnable) {
            IUseCaseLogger::PrintError(d->Title(), message);
        }
    }
}
