#include "InitializeUI.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct InitializeUI::Impl {
		IInitializationOutputPort* output{ nullptr };
	};

    InitializeUI::InitializeUI(IInitializationOutputPort* output) : IUseCase("InitializeUI"), d{ new Impl } {
        d->output = output;
    }

    InitializeUI::~InitializeUI() {
    }

    auto InitializeUI::Perform() -> bool {
        if (nullptr == d->output) {
            Error("Output port is not initialized");
            return false;
        }

        d->output->InitializeUI();

        return true;
    }
}
