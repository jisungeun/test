#include "InstallDataScanner.h"

#include <IDataManager.h>

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct InstallDataScanner::Impl {
		IDataOutputPort* output{ nullptr };
	};

    InstallDataScanner::InstallDataScanner() : IUseCase("InstallDataScanner"), d{ new Impl } {
    }

    InstallDataScanner::~InstallDataScanner() {
    }

    auto InstallDataScanner::SetDataOutputPort(IDataOutputPort* outputPort)->void {
        d->output = outputPort;
    }

    auto InstallDataScanner::Perform() -> bool {
        if (nullptr == d->output) {
            Error("Output port is not initialized");
            return false;
        }

        IDataManager::GetInstance()->InstallDataScannerOutputPort(d->output);
        return true;
    }
}
