#include "LoadExperiment.h"

#include <System.h>
#include <ExperimentReader.h>
#include <AppData.h>

#include "Experiment.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct LoadExperiment::Impl {
		IExperimentOutputPort* output{ nullptr };

        QString experiment;
	};

    LoadExperiment::LoadExperiment(IExperimentOutputPort* output)
    : IUseCase("LoadExperiment"), d{ new Impl } {
        d->output = output;
    }

    LoadExperiment::~LoadExperiment() {
    }

    auto LoadExperiment::SetArgument(const QString& experiment)->void {
        d->experiment = experiment;
    }

    auto LoadExperiment::Perform() -> bool {
        const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
        const auto dataPath = systemConfig->GetDataDir();

        const auto appData = Entity::AppData::GetInstance();
        const auto user = appData->GetUser();
        const auto project = appData->GetProject();

        if (user.isEmpty()) {
            Error("User name is blank.");
            return false;
        }

        if (project.isEmpty()) {
            Error("Project name is blank.");
            return false;
        }

        auto experimentFilePath = dataPath + "/" + user + "/" + project + "/" + d->experiment + "/" + d->experiment + "." + AppEntity::ExperimentExtension;

        AppComponents::ExperimentIO::ExperimentReader reader;
        auto experiment = reader.Read(experimentFilePath);
        if (nullptr == experiment) {
            Error(QString("It fails to read an experiment. - %1").arg(experimentFilePath));
            return false;
        }

        appData->SetExperiment(d->experiment);

        if (d->output) d->output->LoadExperiment(experiment);

        return true;
    }
}
