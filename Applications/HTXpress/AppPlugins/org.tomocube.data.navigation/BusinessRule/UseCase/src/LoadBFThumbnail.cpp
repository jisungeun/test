#include "LoadBFThumbnail.h"

#include "IThumbnailManager.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct LoadBFThumbnail::Impl {
        IThumbnailManager* manager{ nullptr };

        QString path;
        QImage image;
        bool valid = false;
	};

    LoadBFThumbnail::LoadBFThumbnail(IThumbnailManager* manager)
    : IUseCase("LoadBFThumbnail"), d{ new Impl } {
        d->manager = manager;
    }

    LoadBFThumbnail::~LoadBFThumbnail() {
    }

    auto LoadBFThumbnail::SetPath(const QString& path)->void {
        d->path = path;
    }

    auto LoadBFThumbnail::GetImage()->QImage& {
        return d->image;
    }

    auto LoadBFThumbnail::IsThumbnail()->bool {
        return d->valid;
    }

    auto LoadBFThumbnail::Perform() -> bool {
        if (nullptr == d->manager) return false;
        if (d->path.isEmpty()) return false;

        d->valid = d->manager->GetBFThumbnail(d->path, d->image);

        return true;
    }
}
