#include "ChangeCurrentProject.h"

#include <AppData.h>
#include <UserProjectRepo.h>

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct ChangeCurrentProject::Impl {
		IProjectOutputPort* output{ nullptr };

        QString currentProject;
	};

    ChangeCurrentProject::ChangeCurrentProject(IProjectOutputPort* output)
    : IUseCase("ChangeCurrentProject"), d{ new Impl } {
        d->output = output;
    }

    ChangeCurrentProject::~ChangeCurrentProject() {
    }

    auto ChangeCurrentProject::SetCurrentProject(const QString& projectTitle)->void {
        d->currentProject = projectTitle;
    }

    auto ChangeCurrentProject::Perform() -> bool {
        const auto appData = Entity::AppData::GetInstance();
        appData->SetProject(d->currentProject);

        if (d->output) {
            d->output->ChangeCurrentProject(d->currentProject);
        }

        return true;
    }
}
