#include "LoadUserProjectList.h"

#include <QDir>
#include <QDirIterator>

#include <SessionManager.h>
#include <UserManager.h>
#include <System.h>
#include <UserProjectRepo.h>


namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct LoadUserProjectList::Impl {
		IUserOutputPort* output{ nullptr };

        auto GetProjects(const AppEntity::UserID& user) -> QStringList;
	};

    auto LoadUserProjectList::Impl::GetProjects(const AppEntity::UserID& user) -> QStringList {
        const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
        const auto dataPath = systemConfig->GetDataDir();

        auto userDir = QDir(dataPath + "/" + user);

        // get project folders under user
		auto projectDirs = userDir.entryList(
            QDir::Filters(QDir::AllDirs | QDir::NoDotAndDotDot),
            QDir::SortFlags(QDir::Name | QDir::IgnoreCase)
        );

        // get valid project that has project file(*.tcxpro)
        QStringList projects;
		for (auto& projectDir : projectDirs) {
		    QDirIterator projectFileIt(userDir.absolutePath() + "/" + projectDir, { "*." + AppEntity::ProjectExtension }, QDir::Files);
		    while (projectFileIt.hasNext()) {
                projects << QFileInfo(projectFileIt.next()).baseName();
		    }    
		}

        return projects;
    }

    LoadUserProjectList::LoadUserProjectList(IUserOutputPort* output) : IUseCase("LoadUserList"), d{ new Impl } {
        d->output = output;
    }

    LoadUserProjectList::~LoadUserProjectList() {
    }

    auto LoadUserProjectList::Perform() -> bool {
        auto repo = Entity::UserProjectRepo::GetInstance();
        repo->Clear();

        QList<AppEntity::UserID> users;

        const auto sessionManager = AppEntity::SessionManager::GetInstance();
        const auto currentUserId = sessionManager->GetID();
        if (currentUserId.isEmpty()) {
            Error("User ID is blank.");
            return false;
        }

        users << currentUserId;
        repo->AddProjects(currentUserId, d->GetProjects(currentUserId));

        // if logged in account has authority more than Administrator, it can browse all user data.
        const auto currentUserProfile = sessionManager->GetProfile();
        if (currentUserProfile == +AppEntity::Profile::Administrator ||
            currentUserProfile == +AppEntity::Profile::ServiceEngineer) {
            // get all user IDs
            for (const auto &user : AppEntity::UserManager::GetInstance()->GetUsers()) {
                const auto id = user->GetUserID();
                if (users.contains(id)) continue;

                users << id;
                repo->AddProjects(id, d->GetProjects(id));
            }
        }

        if (d->output) d->output->UpdateUserList(users);

        return true;
    }
}
