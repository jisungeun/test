#include "IProjectReaderPort.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    static IProjectReaderPort* theInstance;

    IProjectReaderPort::IProjectReaderPort() {
        theInstance = this;
    }

    IProjectReaderPort::~IProjectReaderPort() = default;

    auto IProjectReaderPort::GetInstance() -> IProjectReaderPort* {
        return theInstance;
    }
}
