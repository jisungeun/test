#include "IDataManager.h"

#include <QList>

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    static IDataManager* theInstance;

    IDataManager::IDataManager() {
        theInstance = this;
    }

    IDataManager::~IDataManager() {
    }

    auto IDataManager::GetInstance() -> IDataManager* {
        return theInstance;
    }
}
