#include "LoadConfig.h"     

#include <QSettings>
#include <IConfigOutputPort.h>

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct LoadConfig::Impl {
        IConfigOutputPort* output{ nullptr };

        QString path;
	};

    LoadConfig::LoadConfig(IConfigOutputPort* output)
    : IUseCase("LoadConfig"), d{ new Impl } {
        d->output = output;
    }

    LoadConfig::~LoadConfig() {
    }

    auto LoadConfig::SetPath(const QString& path)->void {
        d->path = path;
    }

    auto LoadConfig::Perform() -> bool {
        if (nullptr == d->output) {
            Error("Output port is not initialized");
            return false;
        }


        QSettings file(d->path, QSettings::IniFormat);
        file.beginGroup("AcquisitionPosition");

        auto wellIndex = file.value("WellIndex", "0").toString().toInt();
        auto position = AppEntity::Position::fromMM(file.value("Position_X_Millimeter", "0").toString().toDouble(),
                                                    file.value("Position_Y_Millimeter", "0").toString().toDouble(),
                                                    file.value("Position_Z_Millimeter", "0").toString().toDouble());

        file.endGroup();

        d->output->SetAcqInfo(wellIndex, position);

        return true;
    }
}
