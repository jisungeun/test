#include "InstallDataMonitor.h"

#include <IDataManager.h>

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct InstallDataMonitor::Impl {
		IDataOutputPort* output{ nullptr };
	};

    InstallDataMonitor::InstallDataMonitor() : IUseCase("InstallDataMonitor"), d{ new Impl } {
    }

    InstallDataMonitor::~InstallDataMonitor() {
    }

    auto InstallDataMonitor::SetDataOutputPort(IDataOutputPort* outputPort)->void {
        d->output = outputPort;
    }

    auto InstallDataMonitor::Perform() -> bool {
        if (nullptr == d->output) {
            Error("Output port is not initialized");
            return false;
        }

        IDataManager::GetInstance()->InstallDataMonitorOutputPort(d->output);
        return true;
    }
}
