#include "LoadExperimentList.h"

#include <QDirIterator>

#include <System.h>
#include <AppData.h>

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct LoadExperimentList::Impl {
        IExperimentOutputPort* output{ nullptr };
	};

    LoadExperimentList::LoadExperimentList(IExperimentOutputPort* output)
    : IUseCase("LoadExperimentList"), d{ new Impl } {
        d->output = output;
    }

    LoadExperimentList::~LoadExperimentList() {
    }

    auto LoadExperimentList::Perform() -> bool {
        const auto appData = Entity::AppData::GetInstance();
        const auto user = appData->GetUser();
        const auto project = appData->GetProject();

        if (user.isEmpty()) {
            Error("User name is blank.");
            return false;
        }

        if(project.isEmpty()) {
            Error("Project title is empty");
            return false;
        }

        const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
        const auto dataPath = systemConfig->GetDataDir();
        const auto userDir = dataPath + "/" + user;
        const auto projectPath = userDir + "/" + project;
        const QDir projectDir(projectPath);

        auto findFilter = "*." + AppEntity::ExperimentExtension;
        const auto subDirs = projectDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);

        QStringList experiments;

        for (const auto& subDir : subDirs) {
            QDirIterator iter(projectPath + "/" + subDir, { findFilter }, QDir::Files, QDirIterator::NoIteratorFlags);
            while (iter.hasNext()) {
                auto name = iter.next().split("/").last();
                name.chop(AppEntity::ExperimentExtension.length() + 1);
                experiments.append(name);
            }
        }

        if (d->output) d->output->LoadExperimentList(experiments);

        return true;
    }
}
