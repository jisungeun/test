#include <SessionManager.h>

#include "IUserManager.h"
#include "CheckPassword.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
    struct CheckPassword::Impl {
		IUserOutputPort* output{ nullptr };
        QString password;
        bool valid{ false };
    };

    CheckPassword::CheckPassword(IUserOutputPort* outputPort) : IUseCase("CheckPassword"), d{ new Impl } {
        d->output = outputPort;
    }

    CheckPassword::~CheckPassword() {
    }

    auto CheckPassword::SetPassword(const QString& paswword) -> void {
        d->password = paswword;
    }

    auto CheckPassword::Check() const -> bool {
        return d->valid;
    }

    auto CheckPassword::Perform() -> bool {
        auto session = AppEntity::SessionManager::GetInstance();
        auto userManager = IUserManager::GetInstance();

        d->valid = userManager->IsValid(session->GetID(), d->password);

        return true;
    }
}
