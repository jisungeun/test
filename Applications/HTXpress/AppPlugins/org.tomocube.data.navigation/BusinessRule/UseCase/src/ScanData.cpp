#include "ScanData.h"

#include <SessionManager.h>
#include <IDataManager.h>
#include <AppData.h>

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct ScanData::Impl {
		IDataOutputPort* output{ nullptr };

	    QString experiment;
	};

    ScanData::ScanData(IDataOutputPort* output) : IUseCase("ScanData"), d{ new Impl } {
        d->output = output;
    }

    ScanData::~ScanData() {
    }

    auto ScanData::SetExperiment(const QString& experiment) const ->void {
        d->experiment = experiment;
    }

    auto ScanData::Perform() -> bool {
        const auto appData = Entity::AppData::GetInstance();
        const auto user = appData->GetUser();
        const auto project = appData->GetProject();

        if (user.isEmpty()) {
            Error("User is empty.");
            return false;
        }

        if (project.isEmpty()) {
            Error("Project name is empty.");
            return false;
        }

        if (d->experiment.isEmpty()) {
            Error("Experiment name is empty.");
            return false;
        }

        IDataManager::GetInstance()->SetScanExperiment(user, project, d->experiment);

        return true;
    }
}
