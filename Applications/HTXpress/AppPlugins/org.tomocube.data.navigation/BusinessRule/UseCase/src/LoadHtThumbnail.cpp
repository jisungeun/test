#include "LoadHTThumbnail.h"

#include "IThumbnailManager.h"

namespace HTXpress::AppPlugins::Data::Navigation::UseCase {
	struct LoadHTThumbnail::Impl {
        IThumbnailManager* manager{ nullptr };

        QString path;
        QImage image;
        bool valid = false;
	};

    LoadHTThumbnail::LoadHTThumbnail(IThumbnailManager* manager)
    : IUseCase("LoadHTThumbnail"), d{ new Impl } {
        d->manager = manager;
    }

    LoadHTThumbnail::~LoadHTThumbnail() {
    }

    auto LoadHTThumbnail::SetPath(const QString& path)->void {
        d->path = path;
    }

    auto LoadHTThumbnail::GetImage()->QImage& {
        return d->image;
    }

    auto LoadHTThumbnail::IsThumbnail()->bool {
        return d->valid;
    }

    auto LoadHTThumbnail::Perform() -> bool {
        if (nullptr == d->manager) return false;
        if (d->path.isEmpty()) return false;
        
        d->valid = d->manager->GetHTThumbnail(d->path, d->image);

        return true;
    }
}
