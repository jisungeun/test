#include <QMap>

#include "UserProjectRepo.h"

namespace HTXpress::AppPlugins::Data::Navigation::Entity {
    struct UserProjectRepo::Impl {
        QMap<AppEntity::UserID, QStringList> projects;
    };

    UserProjectRepo::UserProjectRepo() : d{new Impl} {
    }
    
    UserProjectRepo::~UserProjectRepo() {
    }

    auto UserProjectRepo::GetInstance()->Pointer {
        static Pointer theInstance{ new UserProjectRepo() };
        return theInstance;
    }

    auto UserProjectRepo::GetUsers() const -> QList<AppEntity::UserID> {
        return d->projects.keys();
    }

    auto UserProjectRepo::AddProject(const AppEntity::UserID& user, const QString& project) const -> void{
        auto projects = d->projects[user];
        if (!projects.contains(project)) {
            projects << project;
            d->projects[user] = projects ;
        }
    }

    auto UserProjectRepo::AddProjects(const AppEntity::UserID& user, const QStringList& projects) const -> void {
        if (d->projects.find(user) == d->projects.end()) {
            d->projects.insert(user, QStringList());
        }

        for (const auto &project : projects) {
            AddProject(user, project);
        }
    }

    auto UserProjectRepo::GetProjects(const AppEntity::UserID& user) const -> QStringList {
        if (d->projects.find(user) == d->projects.end()) return QList<QString>();

        return d->projects[user];
    }

    auto UserProjectRepo::Clear() const -> void {
        d->projects.clear();
    }
}