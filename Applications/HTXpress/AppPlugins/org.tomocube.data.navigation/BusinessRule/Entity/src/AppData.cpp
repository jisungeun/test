#include "AppData.h"

namespace HTXpress::AppPlugins::Data::Navigation::Entity {
    struct AppData::Impl {
        AppEntity::UserID user;
        QString project;
        QString experiment;
    };

    AppData::AppData() : d{ std::make_unique<Impl>() } {
    }
    
    AppData::~AppData() {
    }

    auto AppData::GetInstance() -> Pointer {
        static Pointer theInstance{ new AppData() };
        return theInstance;
    }

    auto AppData::SetUser(const AppEntity::UserID& user) const -> void {
        d->user = user;
    }

    auto AppData::GetUser() const -> AppEntity::UserID {
        return d->user;
    }

    auto AppData::SetProject(const QString& project) const -> void {
        d->project = project;
    }

    auto AppData::GetProject() const -> QString {
        return d->project;
    }

    auto AppData::SetExperiment(const QString& experiment) const -> void {
        d->experiment = experiment;
    }

    auto AppData::GetExperiment() const -> QString {
        return d->experiment;
    }

}