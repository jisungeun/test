#include "ThumbnailRepo.h"

#include <QMutex>

namespace HTXpress::AppPlugins::Data::Navigation::Entity {
    struct ThumbnailRepo::Impl {
        QList<ThumbnailMap> list;
    };

    ThumbnailRepo::ThumbnailRepo() : d{ new Impl } {
    }

    ThumbnailRepo::~ThumbnailRepo() {
    }

    auto ThumbnailRepo::GetInstance() -> Pointer {
        static Pointer theInstance{ new ThumbnailRepo() };
        return theInstance;
    }

    auto ThumbnailRepo::Register(const QString& key, const ImageType& type, const QImage& image)->void {
        if (key.isEmpty()) return;
        d->list.append(std::make_tuple(key, type, image));
    }

    auto ThumbnailRepo::Contains(const QString& key, const ImageType& type)->bool {
        if (key.isEmpty()) {
            return false;
        }

        for(auto info :  d->list) {
            const auto k = std::get<0>(info);
            const auto t = std::get<1>(info);
            if(k == key && t == type) {
                return true;
            }
        }

        return false;
    }

    auto ThumbnailRepo::GetThumbnailImage(const QString& key, const ImageType& type)->QImage {
        if (key.isEmpty()) {
            return QImage();
        }

        if(false == Contains(key, type)) {
            return QImage();
        }

        for (auto info : d->list) {
            const auto k = std::get<0>(info);
            const auto t = std::get<1>(info);
            if (k == key && t == type) {
                return std::get<2>(info);
            }
                
        }

        return QImage();
    }

    auto ThumbnailRepo::Clear()->void {
        d.reset(new Impl());
    }
}
