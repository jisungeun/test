#pragma once

#include <memory>
#include <enum.h>
#include <tuple>

#include <QString>
#include <QImage>

#include <Define.h>

#include "HTX_Data_Navigation_EntityExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Entity {
    typedef std::tuple<QString, ImageType, QImage> ThumbnailMap;

    class HTX_Data_Navigation_Entity_API ThumbnailRepo {
    public:
        using Self = ThumbnailRepo;
        using Pointer = std::shared_ptr<Self>;

    private:
        ThumbnailRepo();

    public:
        virtual ~ThumbnailRepo();
        static auto GetInstance()->Pointer;

        auto Register(const QString& key, const ImageType& type, const QImage& image)->void;

        auto Contains(const QString& key, const ImageType& type)->bool;
        auto GetThumbnailImage(const QString& key, const ImageType& type)->QImage;

        auto Clear()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
