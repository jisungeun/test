#pragma once

#include <enum.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    BETTER_ENUM(ImageType, uint8_t, HT, FL, BF)
}