#pragma once

#include <memory>

#include <AppEntityDefines.h>

#include "HTX_Data_Navigation_EntityExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Entity {
    class HTX_Data_Navigation_Entity_API AppData {
    public:
        using Pointer = std::shared_ptr<AppData>;

    private:
        AppData();

    public:
        ~AppData();

        static auto GetInstance()->Pointer;

        auto SetUser(const AppEntity::UserID& user) const -> void;
        auto GetUser() const -> AppEntity::UserID;

        auto SetProject(const QString& project) const -> void;
        auto GetProject() const -> QString;

        auto SetExperiment(const QString& experiment) const -> void;
        auto GetExperiment() const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}