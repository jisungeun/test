#pragma once
#include <memory>

#include <QStringList>

#include <AppEntityDefines.h>

#include "HTX_Data_Navigation_EntityExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Entity {
    class HTX_Data_Navigation_Entity_API UserProjectRepo {
    public:
        typedef std::shared_ptr<UserProjectRepo> Pointer;

    private:
        UserProjectRepo();

    public:
        ~UserProjectRepo();

        static auto GetInstance()->Pointer;

        auto GetUsers() const->QList<AppEntity::UserID>;

        auto AddProject(const AppEntity::UserID& user, const QString& project) const->void;
        auto AddProjects(const AppEntity::UserID& user, const QStringList& projects) const->void;
        auto GetProjects(const AppEntity::UserID& user) const->QStringList;

        auto Clear() const->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}