#include <TAExecutor.h>

#include "ImageViewerManager.h"

namespace HTXpress::AppPlugins::Data::Navigation::Plugins::ImageViewerManager {
    struct ImageViewerManager::Impl {
        AppComponents::TomoaAnalysisExecutor::TAExecutor taExecutor{};
        QStringList tcfList{};
    };

    ImageViewerManager::ImageViewerManager() : IImageViewerManager(), d{std::make_unique<Impl>()} {
    }

    ImageViewerManager::~ImageViewerManager() {
    }

    auto ImageViewerManager::SetTCFList(const QStringList& tcfList) -> void {
        d->tcfList = tcfList;
    }

    auto ImageViewerManager::Execute() const -> bool {
        if (!d->taExecutor.Write(d->tcfList)) {
            return false;
        }

        if (!d->taExecutor.Execute()) {
            return false;
        }
        return true;
    }
}
