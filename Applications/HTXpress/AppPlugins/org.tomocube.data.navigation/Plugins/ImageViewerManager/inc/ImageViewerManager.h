#pragma once
#include <memory>

#include <IImageViewerManager.h>

#include "HTX_Data_Navigation_ImageViewerManagerExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Plugins::ImageViewerManager {
    class HTX_Data_Navigation_ImageViewerManager_API ImageViewerManager final : public UseCase::IImageViewerManager {
    public:
        using Pointer = std::shared_ptr<ImageViewerManager>;

        ImageViewerManager();
        ~ImageViewerManager() override;

        auto SetTCFList(const QStringList& tcfList) -> void override;
        auto Execute() const -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
