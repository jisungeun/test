#pragma once
#include <memory>

#include <IUserManager.h>
#include "HTX_Data_NAvigation_UserManagerExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Plugins::UserManager {
    class HTX_Data_Navigation_UserManager_API Plugin : public UseCase::IUserManager {
    public:
        Plugin();
        ~Plugin() override;

        auto IsValid(const AppEntity::UserID& id, const QString& password) const -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}