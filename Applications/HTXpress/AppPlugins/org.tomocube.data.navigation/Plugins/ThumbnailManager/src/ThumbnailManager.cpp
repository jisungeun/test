#include "ThumbnailManager.h"

#include <enum.h>

#include <QCoreApplication>
#include <QDir>
#include <QPixmap>
#include <QPainter>

#include <TCFRawDataReader.h>
#include <TCFMetaReader.h>
#include <FileUtility.h>
#include <ThumbnailHT.h>
#include <ThumbnailFL.h>
#include <ThumbnailBF.h>

#include "HDF5Mutex.h"
#include "TCFThumbnailReader.h"

namespace HTXpress::AppPlugins::Data::Navigation::Plugins::ThumbnailManager {
    BETTER_ENUM(ThumbType, uint8_t, HT, FL, BF)

	struct ThumbnailManager::Impl {
		TC::IO::TCFRawDataReader* dataReader{ nullptr };
        TC::IO::TCFMetaReader* metaReader{ nullptr };

        const int _image_size = 1024;

        auto GetImage(const QString& path, const ThumbType& type, QImage& image)->bool;

        auto HasThumbnail(const QString& path, const ThumbType& type) const ->bool;
        auto HasType(const QString& path, const ThumbType& type) const ->bool;

		auto LoadNotCompletedImage(QImage& image)->bool;
		auto GetNotCompletedFilePath()->QString;
		auto GenerateNotCompletedImage()->bool;

        auto GenerateNoImage(const QString& path, const ThumbType& type)->bool;
        auto GenerateNoImage(const ThumbType& type, QImage& image)->bool;

		auto LoadThumbnail(const QString& path, const ThumbType& type, QImage& image)->bool;
		auto LoadHTThumbnail(const QString& path, QImage& image)->bool;
		auto LoadFLThumbnail(const QString& path, QImage& image)->bool;
		auto LoadBFThumbnail(const QString& path, QImage& image)->bool;

        auto GenerateHTThumbnail(const QString& path, QImage& image)->bool;
        auto GenerateFLThumbnail(const QString& path, QImage& image)->bool;
        auto GenerateBFThumbnail(const QString& path, QImage& image)->bool;

        auto GetThumbnail(const QString& tcfPath, const ThumbType& type, QImage& image)->bool;

		auto ReadThumbnailFile(const QString& tcfPath, const ThumbType& type, QImage& image)->bool;
		auto GenerateThumbnailFile(const QString& tcfPath, const ThumbType& type)->bool;
        auto GenerateThumbnailDirPath(const QString& tcfPath)->QString;
        auto GenerateThumbnailFilePath(const QString& tcfPath, const ThumbType& type)->QString;

        auto LoadHT(const QString& path, QImage& image)->bool;
        auto LoadFL(const QString& path, QImage& image)->bool;
        auto LoadBF(const QString& path, QImage& image)->bool;
	};

	ThumbnailManager::ThumbnailManager() : d { new Impl } {
        d->dataReader = new TC::IO::TCFRawDataReader;
        d->metaReader = new TC::IO::TCFMetaReader;
	}
	
	ThumbnailManager::~ThumbnailManager() {
	}

    auto ThumbnailManager::GetHTThumbnail(const QString& path, QImage& image)->bool {
        if (path.isEmpty()) return false;
        return d->GetImage(path, ThumbType::HT, image);
	}

    auto ThumbnailManager::GetFLThumbnail(const QString& path, QImage& image)->bool {
        if (path.isEmpty()) return false;
        return d->GetImage(path, ThumbType::FL, image);
	}

    auto ThumbnailManager::GetBFThumbnail(const QString& path, QImage& image)->bool {
        if (path.isEmpty()) return false;
        return d->GetImage(path, ThumbType::BF, image);
	}
    
    auto ThumbnailManager::Impl::GetImage(const QString& path, const ThumbType& type, QImage& image)->bool {
        if (path.isEmpty()) return false;

        if (false == QFileInfo::exists(path)) {
            LoadNotCompletedImage(image);
            return false;
        }

        if (HasThumbnail(path, type)) {
            return LoadThumbnail(path, type, image);
        }

        if(false == HasType(path, type)) {
            GenerateNoImage(type, image);
            return false;
        }
        else {
            if (false == GetThumbnail(path, type, image)) return false;
        }

        return true;
	}

    auto ThumbnailManager::Impl::HasThumbnail(const QString& path, const ThumbType& type) const ->bool {
        Q_UNUSED(type);
        if (path.isEmpty()) return false;
        if (false == QFileInfo::exists(path)) return false;

        const auto meta = metaReader->Read(path);
        if (meta == nullptr) return false;

        switch (type) {
        case ThumbType::HT: return meta->thumbnail.ht.exist;
        case ThumbType::FL: return meta->thumbnail.fl.exist;
        case ThumbType::BF: return meta->thumbnail.bf.exist;
        default: ;
        }
        
        return true;
    }

    auto ThumbnailManager::Impl::HasType(const QString& path, const ThumbType& type) const ->bool{
        if (nullptr == metaReader) return false;
	    if (path.isEmpty()) return false;
        if (false == QFileInfo::exists(path)) return false;

        const auto meta = metaReader->Read(path);
        if(nullptr == meta) return false;

        switch (type) {
        case ThumbType::HT: return meta->data.data2DMIP.exist;
        case ThumbType::FL: return meta->data.data2DFLMIP.exist;
        case ThumbType::BF: return meta->data.dataBF.exist;
        }

        return false;
    }

    auto ThumbnailManager::Impl::LoadNotCompletedImage(QImage& image)->bool {
        const auto path = GetNotCompletedFilePath();
        if (path.isEmpty()) return false;
        if (false == QFileInfo::exists(path)) {
            if(false == GenerateNotCompletedImage()) {
                return false;
            }
        }

        image = QImage(path);
        return true;
    }

    auto ThumbnailManager::Impl::GetNotCompletedFilePath()->QString {
        const auto appDir = QCoreApplication::applicationDirPath();
        const auto imgDir = appDir + "/" + "img";
        return imgDir + "/" + "NotCompletedThumbnail.png";
    }

    auto ThumbnailManager::Impl::GenerateNotCompletedImage()->bool {
        const auto path = GetNotCompletedFilePath();
        if (path.isEmpty()) return false;

        const QFileInfo fileInfo(path);
        const auto dirPath = fileInfo.dir().absolutePath();

        if (false == TC::MakeDir(dirPath)) {
            return "";
        }

        const QSize size(_image_size, _image_size);
        QPixmap pixmap(size);
        pixmap.fill(Qt::transparent);

        QBrush brush;
        brush.setColor(QColor(255, 255, 255));

        QPainter painter(&pixmap);
        painter.setFont(QFont("Arial", 72));
        painter.setPen(QColor(255, 255, 255));
        painter.drawText(QRect(0, 0, size.width(), size.height()), Qt::AlignCenter, "Not Completed");
        return pixmap.save(path);
    }

    auto ThumbnailManager::Impl::GenerateNoImage(const QString& path, const ThumbType& type)->bool {
        const auto thumbnailPath = GenerateThumbnailFilePath(path, type);
        if (thumbnailPath.isEmpty()) return false;

        const QFileInfo fileInfo(thumbnailPath);
        const auto dirPath = fileInfo.dir().absolutePath();

        if (false == TC::MakeDir(dirPath)) {
            return "";
        }

        const QSize size(_image_size, _image_size);
        QPixmap pixmap(size);
        pixmap.fill(Qt::transparent);

        QBrush brush;
        brush.setColor(QColor(255, 255, 255));

        QPainter painter(&pixmap);
        painter.setFont(QFont("Arial", 72));
        painter.setPen(QColor(255, 255, 255));
        painter.drawText(QRect(0, 0, size.width(), size.height()), Qt::AlignCenter, QString("No %1").arg(type._to_string()));
        return pixmap.save(thumbnailPath);
	}

    auto ThumbnailManager::Impl::GenerateNoImage(const ThumbType& type, QImage& image)->bool {
        const QSize size(_image_size, _image_size);
        QPixmap pixmap(size);
        pixmap.fill(Qt::transparent);

        QBrush brush;
        brush.setColor(QColor(255, 255, 255));

        QPainter painter(&pixmap);
        painter.setFont(QFont("Arial", 72));
        painter.setPen(QColor(255, 255, 255));
        painter.drawText(QRect(0, 0, size.width(), size.height()), Qt::AlignCenter, QString("No %1").arg(type._to_string()));

        image = pixmap.toImage();
        return true;
	}

    auto ThumbnailManager::Impl::LoadThumbnail(const QString& path, const ThumbType& type, QImage& image)->bool {
        switch (type) {
        case ThumbType::HT: return LoadHTThumbnail(path, image);
        case ThumbType::FL: return LoadFLThumbnail(path, image);
        case ThumbType::BF: return LoadBFThumbnail(path, image);
        default:;
        }

        return false;
    }

    auto ThumbnailManager::Impl::LoadHTThumbnail(const QString& path, QImage& image)->bool {
        using namespace TC::TCFIO;
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        TCFThumbnailReader tcfThumbnailReader;
        tcfThumbnailReader.SetTCFFilePath(path);
        tcfThumbnailReader.SetTarget(TCFThumbnailReader::Type::HT, 0);

	    if (!tcfThumbnailReader.Read()) { return false; }

        const auto thumbnail = tcfThumbnailReader.GetThumbnail();

        const auto data = thumbnail.GetData();
        const auto sizeX = thumbnail.GetSizeX();
        const auto sizeY = thumbnail.GetSizeY();
        const auto sizeZ = thumbnail.GetSizeZ();

        if (sizeZ != 1) { return false; }

        QImage thumbnailImage{ data.get(), sizeX, sizeY, sizeX, QImage::Format_Grayscale8 };
        image = thumbnailImage.convertToFormat(QImage::Format_RGB32).scaled(_image_size, _image_size, Qt::KeepAspectRatio);

        return true;
    }

    auto ThumbnailManager::Impl::LoadFLThumbnail(const QString& path, QImage& image)->bool {
        using namespace TC::TCFIO;
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        TCFThumbnailReader tcfThumbnailReader;
        tcfThumbnailReader.SetTCFFilePath(path);
        tcfThumbnailReader.SetTarget(TCFThumbnailReader::Type::FL, 0);

        if (!tcfThumbnailReader.Read()) { return false; }

        const auto thumbnail = tcfThumbnailReader.GetThumbnail();

        const auto data = thumbnail.GetData();
        const auto sizeX = thumbnail.GetSizeX();
        const auto sizeY = thumbnail.GetSizeY();
        const auto sizeZ = thumbnail.GetSizeZ();

        if (sizeZ != 3) { return false; }

        //(rrrr....gggg....bbbb -> rgbrgbrgbrgb....)
        std::shared_ptr<uint8_t[]> imageBufferFL{ new uint8_t[sizeX * sizeY * sizeZ]() };

        for (auto indexX = 0; indexX < sizeX; ++indexX) {
            for (auto indexY = 0; indexY < sizeY; ++indexY) {
                const auto srcRIndex = indexY + indexX * sizeY + 0 * (sizeX * sizeY);
                const auto srcGIndex = indexY + indexX * sizeY + 1 * (sizeX * sizeY);
                const auto srcBIndex = indexY + indexX * sizeY + 2 * (sizeX * sizeY);

                const auto rValue = data.get()[srcRIndex];
                const auto gValue = data.get()[srcGIndex];
                const auto bValue = data.get()[srcBIndex];

                const auto destRIndex = 3 * (indexY + indexX * sizeY) + 0;
                const auto destGIndex = 3 * (indexY + indexX * sizeY) + 1;
                const auto destBIndex = 3 * (indexY + indexX * sizeY) + 2;

                imageBufferFL.get()[destRIndex] = rValue;
                imageBufferFL.get()[destGIndex] = gValue;
                imageBufferFL.get()[destBIndex] = bValue;
            }
        }

        QImage thumbnailImage{ imageBufferFL.get(), sizeX, sizeY, 3 * sizeX, QImage::Format_RGB888 };
        image = thumbnailImage.convertToFormat(QImage::Format_RGB32).scaled(_image_size, _image_size, Qt::KeepAspectRatio);

        return true;
    }

    auto ThumbnailManager::Impl::LoadBFThumbnail(const QString& path, QImage& image)->bool {
        using namespace TC::TCFIO;
        TC::IO::HDF5MutexLocker locker{ TC::IO::HDF5Mutex::GetInstance() };
        TCFThumbnailReader tcfThumbnailReader;
        tcfThumbnailReader.SetTCFFilePath(path);
        tcfThumbnailReader.SetTarget(TCFThumbnailReader::Type::BF, 0);

        if (!tcfThumbnailReader.Read()) { return false; }

        const auto thumbnail = tcfThumbnailReader.GetThumbnail();

        const auto data = thumbnail.GetData();
        const auto sizeX = thumbnail.GetSizeX();
        const auto sizeY = thumbnail.GetSizeY();
        const auto sizeZ = thumbnail.GetSizeZ();

        if (sizeZ != 3) { return false; }

        //(rrrr....gggg....bbbb -> rgbrgbrgbrgb....)
        std::shared_ptr<uint8_t[]> imageBufferBF{ new uint8_t[sizeX * sizeY * sizeZ]() };

        for (auto indexX = 0; indexX < sizeX; ++indexX) {
            for (auto indexY = 0; indexY < sizeY; ++indexY) {
                const auto srcRIndex = indexY + indexX * sizeY + 0 * (sizeX * sizeY);
                const auto srcGIndex = indexY + indexX * sizeY + 1 * (sizeX * sizeY);
                const auto srcBIndex = indexY + indexX * sizeY + 2 * (sizeX * sizeY);

                const auto rValue = data.get()[srcRIndex];
                const auto gValue = data.get()[srcGIndex];
                const auto bValue = data.get()[srcBIndex];

                const auto destRIndex = 3 * (indexY + indexX * sizeY) + 0;
                const auto destGIndex = 3 * (indexY + indexX * sizeY) + 1;
                const auto destBIndex = 3 * (indexY + indexX * sizeY) + 2;

                imageBufferBF.get()[destRIndex] = rValue;
                imageBufferBF.get()[destGIndex] = gValue;
                imageBufferBF.get()[destBIndex] = bValue;
            }
        }

        QImage thumbnailImage{ imageBufferBF.get(), sizeX, sizeY, 3 * sizeX, QImage::Format_RGB888 };
        image = thumbnailImage.convertToFormat(QImage::Format_RGB32).scaled(_image_size, _image_size, Qt::KeepAspectRatio);

        return true;
    }

    auto ThumbnailManager::Impl::GenerateHTThumbnail(const QString& path, QImage& image)->bool {
        if (nullptr == metaReader) return false;
        if (nullptr == dataReader) return false;
        if (path.isEmpty()) return false;

        Q_UNUSED(image);

        /*const auto meta = metaReader->Read(path);
        if (nullptr == meta) return false;
        if (false == meta->data.data2DMIP.exist) return false;

	    if (false == dataReader->Open(path))  return false;

        int w = 0; int h = 0;
        auto rawData = std::shared_ptr<uint16_t[]>();
        if (meta->data.isLDM) {
            dataReader->GetLdmHTMIP(0, rawData, w, h);
        }
        else {
            dataReader->GetHTMIP(0, rawData);
            w = meta->data.data2DMIP.sizeX;
            h = meta->data.data2DMIP.sizeY;
        }

        TC::Processing::ThumbnailGenerator::ThumbnailHT generator;
        generator.SetHTData(rawData);
        generator.SetDataSize(w, h);
        if (false == generator.Generate()) return false;

        const auto thumbnailData = generator.GetThumbnailData();
        if (nullptr == thumbnailData) return false;

        const auto x = generator.GetThumbnailSizeX();
        const auto y = generator.GetThumbnailSizeY();

        auto buffer = QImage(thumbnailData.get(), x, y, x, QImage::Format_Indexed8);
        image = buffer.convertToFormat(QImage::Format_RGB32);*/

        return true;
	}

    auto ThumbnailManager::Impl::GenerateFLThumbnail(const QString& path, QImage& image)->bool {
        if (nullptr == metaReader) return false;
        if (nullptr == dataReader) return false;
        if (path.isEmpty()) return false;

        Q_UNUSED(image);

        /*const auto meta = metaReader->Read(path);
        if (nullptr == meta) return false;
        if (false == meta->data.data2DFLMIP.exist) return false;

        if (false == dataReader->Open(path))  return false;

        int w = 0; int h = 0;
        TC::IO::TCFRawDataReader::FLChannelData chData0;
        TC::IO::TCFRawDataReader::FLChannelData chData1;
        TC::IO::TCFRawDataReader::FLChannelData chData2;

        if (meta->data.isLDM) {
            dataReader->GetLdmFLMIP(0, chData0, chData1, chData2, w, h);
        }
        else {
            dataReader->GetFLMIP(0, chData0, chData1, chData2);
            w = meta->data.data2DFLMIP.sizeX;
            h = meta->data.data2DFLMIP.sizeY;
        }

        TC::Processing::ThumbnailGenerator::ThumbnailFL generator;
        generator.SetFLCH0Data(chData0.data, chData0.r, chData0.g, chData0.b);
        generator.SetFLCH1Data(chData1.data, chData1.r, chData1.g, chData1.b);
        generator.SetFLCH2Data(chData2.data, chData2.r, chData2.g, chData2.b);
        generator.SetDataSize(w, h);
        if (false == generator.Generate()) return false;

        const auto thumbnailData = generator.GetThumbnailData();
        if (nullptr == thumbnailData) return false;

        const auto x = generator.GetThumbnailSizeX();
        const auto y = generator.GetThumbnailSizeY();

        image = QImage(thumbnailData.get(), x, y * 3, x, QImage::Format_RGB888);*/

        return true;
	}

    auto ThumbnailManager::Impl::GenerateBFThumbnail(const QString& path, QImage& image)->bool {
        if (nullptr == metaReader) return false;
        if (nullptr == dataReader) return false;
        if (path.isEmpty()) return false;

        Q_UNUSED(image);

        /*const auto meta = metaReader->Read(path);
        if (nullptr == meta) return false;
        if (false == meta->data.dataBF.exist) return false;

        if (false == dataReader->Open(path))  return false;

        int w = 0; int h = 0;
        auto rawData = std::shared_ptr<uint8_t[]>();
        if (meta->data.isLDM) {
            dataReader->GetLdmBF(0, rawData, w, h);
        }
        else {
            dataReader->GetBF(0, rawData);
            w = meta->data.dataBF.sizeX;
            h = meta->data.dataBF.sizeY;
        }

        TC::Processing::ThumbnailGenerator::ThumbnailBF generator;
        generator.SetBFData(rawData);
        generator.SetDataSize(w, h);
        if (false == generator.Generate()) return false;

        const auto thumbnailData = generator.GetThumbnailData();
        if (nullptr == thumbnailData) return false;

        const auto x = generator.GetThumbnailSizeX();
        const auto y = generator.GetThumbnailSizeY();
        
        image = QImage(thumbnailData.get(), x, y, x*3, QImage::Format_RGB888);*/

        return true;
	}

    auto ThumbnailManager::Impl::ReadThumbnailFile(const QString& tcfPath, const ThumbType& type, QImage& image)->bool {
        const auto thumbnailPath = GenerateThumbnailFilePath(tcfPath, type);
        if (thumbnailPath.isEmpty()) return false;
        if (false == QFileInfo::exists(thumbnailPath)) return false;

        image = QImage(thumbnailPath);

        return true;
    }

    auto ThumbnailManager::Impl::GenerateThumbnailFile(const QString& tcfPath, const ThumbType& type)->bool {
        const auto thumbnailPath = GenerateThumbnailFilePath(tcfPath, type);
        if (thumbnailPath.isEmpty()) return false;

        const QFileInfo fileInfo(thumbnailPath);
        const auto dirPath = fileInfo.dir().absolutePath();

        if (false == TC::MakeDir(dirPath)) {
            return "";
        }

        QImage buffer;
        switch (type) {
        case ThumbType::HT: LoadHT(tcfPath, buffer); break;
        case ThumbType::FL: LoadFL(tcfPath, buffer); break;
        case ThumbType::BF: LoadBF(tcfPath, buffer); break;
        default: break;
        }

        if (buffer.isNull()) return false;

        
        return buffer.save(thumbnailPath);
    }

    auto ThumbnailManager::Impl::GetThumbnail(const QString& tcfPath, const ThumbType& type, QImage& image)->bool {
        switch (type) {
        case ThumbType::HT: LoadHT(tcfPath, image); break;
        case ThumbType::FL: LoadFL(tcfPath, image); break;
        case ThumbType::BF: LoadBF(tcfPath, image); break;
        default: break;
        }

        return true;
    }

    auto ThumbnailManager::Impl::GenerateThumbnailDirPath(const QString& tcfPath)->QString {
        if (tcfPath.isEmpty()) return QString();

        const QFileInfo fileInfo(tcfPath);
        const auto dirPath = fileInfo.dir().absolutePath();
        const auto thumbnailDirPath = dirPath + "/" + "thumbnail";
        return thumbnailDirPath;
	}

    auto ThumbnailManager::Impl::GenerateThumbnailFilePath(const QString& tcfPath, const ThumbType& type)->QString {
        if (tcfPath.isEmpty()) return QString();

        const auto thumbnailDirPath = GenerateThumbnailDirPath(tcfPath);
        return thumbnailDirPath + "/" + type._to_string() + ".png";
    }

    auto ThumbnailManager::Impl::LoadHT(const QString& path, QImage& image)->bool {
        if (nullptr == metaReader) return false;
        if (nullptr == dataReader) return false;
        if (path.isEmpty()) return false;

        const auto meta = metaReader->Read(path);
        if (nullptr == meta) return false;
        if (false == meta->data.data2DMIP.exist) return false;

        auto min = meta->data.data2DMIP.riMin;
        auto max = meta->data.data2DMIP.riMax;
        int length = 0;
        if (false == meta->data.isLDM) {
            length = meta->data.data2DMIP.sizeX * meta->data.data2DMIP.sizeY;
        }

        auto temp = std::shared_ptr<unsigned short[]>();
        int x = 0;
        int y = 0;

        if (false == dataReader->Open(path)) {
            return false;
        }

        if (meta->data.isLDM) {
            dataReader->ReadLdmHTMIP(0, temp, x, y);
            length = x * y;
        }
        else {
            dataReader->ReadHTMIP(0, temp);
        }

        if (nullptr == temp) {
            return false;
        }

        if (max == 0.0 || min == 0.0) {
            max = temp.get()[0] / 10000.f;
            min = temp.get()[0] / 10000.f;

            for (auto i = 0; i < length; ++i) {
                const auto ele = temp.get()[i] / 10000.f;
                if (ele > max) {
                    max = ele;
                }

                if (ele < min) {
                    min = ele;
                }

                meta->data.data2DMIP.riMin = min;
                meta->data.data2DMIP.riMax = max;
            }
        }

        int w = 0;
        int h = 0;

        if (meta->data.isLDM) {
            w = x;
            h = y;
        }
        else {
            w = meta->data.data2DMIP.sizeX;
            h = meta->data.data2DMIP.sizeY;
        }
        const auto range = max - min;

        auto rawData = std::shared_ptr<unsigned char[]>(new unsigned char[length]);
        for (auto i = 0; i < length; ++i) {
            auto value = temp.get()[i] / 10000.f;
            value = std::max<double>(value, min);
            value = std::min<double>(value, max);
            value = value - min;

            rawData.get()[i] = static_cast<unsigned char>(value / range * 255);
        }

        auto buffer = QImage(rawData.get(), w, h, w, QImage::Format_Indexed8);
        image = buffer.convertToFormat(QImage::Format_RGB32).scaled(_image_size, _image_size, Qt::KeepAspectRatio);

        return true;
    }

    auto ThumbnailManager::Impl::LoadFL(const QString& path, QImage& image)->bool {
        if (nullptr == metaReader) return false;
        if (nullptr == dataReader) return false;
        if (path.isEmpty()) return false;

        const auto meta = metaReader->Read(path);
        if (nullptr == meta) return false;
        if (false == meta->data.data2DFLMIP.exist) return false;

        int w = 0;
        int h = 0;

        auto data = std::shared_ptr<uint32_t[]>();

        if (false == dataReader->Open(path)) {
            return false;
        }

        if (meta->data.isLDM) {
            dataReader->ReadLdmFLMIP(0, data, w, h);
        }
        else {
            dataReader->ReadFLMIP(0, data);
            w = meta->data.data2DFLMIP.sizeX;
            h = meta->data.data2DFLMIP.sizeY;
        }

        auto temp = QImage(reinterpret_cast<uchar*>(data.get()), w, h, QImage::Format_ARGB32);
        image = temp.convertToFormat(QImage::Format_RGB32);

        return true;
    }

    auto ThumbnailManager::Impl::LoadBF(const QString& path, QImage& image)->bool {
        if (nullptr == metaReader) return false;
        if (nullptr == dataReader) return false;
        if (path.isEmpty()) return false;

        const auto meta = metaReader->Read(path);
        if (nullptr == meta) return false;
        if (false == meta->data.dataBF.exist) return false;

        int w = 0;
        int h = 0;

        auto data = std::shared_ptr<uint32_t[]>();

        if (false == dataReader->Open(path)) {
            return false;
        }

        if (meta->data.isLDM) {
            dataReader->ReadLdmBF(0, data, w, h);
        }
        else {
            w = meta->data.dataBF.sizeX;
            h = meta->data.dataBF.sizeY;

            dataReader->ReadBF(0, data);
        }

        auto temp = QImage(reinterpret_cast<uchar*>(data.get()), w, h, QImage::Format_ARGB32);
        image = temp.convertToFormat(QImage::Format_RGB32);

        return true;
    }
}