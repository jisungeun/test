#pragma once
#include <memory>
#include <QImage>

#include <IThumbnailManager.h>

#include "HTX_Data_Navigation_ThumbnailManagerExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Plugins::ThumbnailManager {
	class HTX_Data_Navigation_ThumbnailManager_API ThumbnailManager : public UseCase::IThumbnailManager {
	public:
		ThumbnailManager();
		~ThumbnailManager();
		
		auto GetHTThumbnail(const QString& path, QImage& image)->bool override;
		auto GetFLThumbnail(const QString& path, QImage& image)->bool override;
		auto GetBFThumbnail(const QString& path, QImage& image)->bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
