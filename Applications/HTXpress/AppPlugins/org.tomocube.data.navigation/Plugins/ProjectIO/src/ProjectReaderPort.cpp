#include "ProjectReaderPort.h"

#include <ProjectReader.h>

namespace HTXpress::AppPlugins::Data::Navigation::Plugins::ProjectIO {
	ProjectReaderPort::ProjectReaderPort() {
	    
	}

	ProjectReaderPort::~ProjectReaderPort() {
	    
	}

	auto ProjectReaderPort::Read(const QString& path, AppEntity::Project::Pointer& Project) const -> bool {
        AppComponents::ProjectIO::ProjectReader reader;
		return reader.Read(path, Project);
    }
}