#pragma once
#include <memory>

#include <IProjectReaderPort.h>

#include "HTX_Data_Navigation_ProjectIOExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Plugins::ProjectIO {
	class HTX_Data_Navigation_ProjectIO_API ProjectReaderPort : public UseCase::IProjectReaderPort{
	public:
		ProjectReaderPort();
		~ProjectReaderPort();

		auto Read(const QString& path, AppEntity::Project::Pointer& project) const -> bool override;
	};
}
