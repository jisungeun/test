#define LOGGER_TAG "[DataManagerObserver]"

#include <TCLogger.h>
#include "DataManagerUpdater.h"
#include "DataManagerObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation::Plugins::DataManager {
    DataManagerObserver::DataManagerObserver() {
        DataManagerUpdater::GetInstance()->Register(this);
    }   

    DataManagerObserver::~DataManagerObserver() {
        DataManagerUpdater::GetInstance()->Deregister(this);
    }

    auto DataManagerObserver::UpdateExperiment(const QString& user, const QString& project, const QString& experiment)->void {
        emit sigUpdateExperiment(user, project, experiment);
    }

    auto DataManagerObserver::WatchedAddData(const QString& fileFullPath)->void {
        emit sigDataAdded(fileFullPath);
    }

    auto DataManagerObserver::WatchedDeleteData(const QString& fileFullPath)->void {
        emit sigDataDeleted(fileFullPath);
    }

    auto DataManagerObserver::WatchedUpdateData(const QString& fileFullPath)->void {
        emit sigDataUpdated(fileFullPath);
    }

    void DataManagerObserver::WatchedDeleteDataRootFolder(const QString& fileFullPath) {
        emit sigDataRootFolderDeleted(fileFullPath);
    }
}
