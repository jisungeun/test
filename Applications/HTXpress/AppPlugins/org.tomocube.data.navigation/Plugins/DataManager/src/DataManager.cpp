#define LOGGER_TAG "[DataManager]"

#include <TCLogger.h>
#include <HTXDataManager.h>
#include "DataManager.h"
#include "DataManagerObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation::Plugins::DataManager {
    struct DataManager::Impl {
        DataManagerObserver* observer{ nullptr };

        QList<UseCase::IDataOutputPort*> monitorPorts;

        QString user;
        QString project;
        QString experiment;
    };

    DataManager::DataManager() : UseCase::IDataManager(), d{ new Impl } {
        d->observer = new DataManagerObserver;

        connect(d->observer, &DataManagerObserver::sigUpdateExperiment,  this, &DataManager::onScanExperimentData);
        connect(d->observer, &DataManagerObserver::sigDataAdded,    this, &DataManager::onWatchedDataAdded);
        connect(d->observer, &DataManagerObserver::sigDataDeleted, this, &DataManager::onWatchedDataDeleted);
        connect(d->observer, &DataManagerObserver::sigDataUpdated, this, &DataManager::onWatchedDataUpdated);
        connect(d->observer, &DataManagerObserver::sigDataRootFolderDeleted, this, &DataManager::onWatchedDataRootFolderDeleted);
    }

    DataManager::~DataManager() {
    }

    auto DataManager::SetScanExperiment(const QString& user, const QString& project, const QString& experiment) -> void {
        if (user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;

        AppComponents::HTXDataManager::HTXDataManager::GetInstance()->RemoveScanExperiment(d->user, d->project, d->experiment);
        AppComponents::HTXDataManager::HTXDataManager::GetInstance()->AppendScanExperiment(user, project, experiment);

        AppComponents::HTXDataManager::HTXDataManager::GetInstance()->RemoveMonitorExperiment(d->user, d->project, d->experiment);
        AppComponents::HTXDataManager::HTXDataManager::GetInstance()->AppendMonitorExperiment(user, project, experiment);

        d->user = user;
        d->project = project;
        d->experiment = experiment;
    }

    auto DataManager::InstallDataScannerOutputPort(UseCase::IDataOutputPort* outputPort) -> void {
        Q_UNUSED(outputPort)
    }

    auto DataManager::UninstallDataScannerOutputPort(UseCase::IDataOutputPort* outputPort) -> void {
        Q_UNUSED(outputPort)
    }

    auto DataManager::InstallDataMonitorOutputPort(UseCase::IDataOutputPort* outputPort)->void {
        if (false == d->monitorPorts.contains(outputPort))
            d->monitorPorts.append(outputPort);
    }

    auto DataManager::UninstallDataMonitorOutputPort(UseCase::IDataOutputPort* outputPort)->void {
        d->monitorPorts.removeOne(outputPort);
    }

    void DataManager::onScanExperimentData(const QString& user, const QString& project, const QString& experiment) {
        for (auto output : d->monitorPorts) {
            output->ScannedExperimentData(user, project, experiment);
        }
    }

    void DataManager::onWatchedDataAdded(const QString& fileFullPath) {
        for(auto output : d->monitorPorts) {
            output->AddData(fileFullPath);
        }
    }
    
    void DataManager::onWatchedDataDeleted(const QString& fileFullPath) {
        for (auto output : d->monitorPorts) {
            output->DeleteData(fileFullPath);
        }
    }

    void DataManager::onWatchedDataUpdated(const QString& fileFullPath) {
        for (auto output : d->monitorPorts) {
            output->UpdateData(fileFullPath);
        }
    }

    void DataManager::onWatchedDataRootFolderDeleted(const QString& fileFullPath) {
        for (auto output : d->monitorPorts) {
            output->DeleteDataRootFolder(fileFullPath);
        }
    }
}
