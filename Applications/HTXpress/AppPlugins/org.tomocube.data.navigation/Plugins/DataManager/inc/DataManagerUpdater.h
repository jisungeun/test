#pragma once
#include <memory>

#include "DataManagerObserver.h"
#include "IHTXDataManagerUpdater.h"
#include "HTX_Data_Navigation_DataManagerExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Plugins::DataManager {
    class HTX_Data_Navigation_DataManager_API DataManagerUpdater : public AppComponents::HTXDataManager::IHTXDataManagerUpdater {

    public:
        using Self = DataManagerUpdater;
        using Pointer = std::shared_ptr<Self>;

    protected:
        DataManagerUpdater();

    public:
        ~DataManagerUpdater() override;
        static auto GetInstance()->Pointer;

        auto Register(AppComponents::HTXDataManager::IHTXDataManagerObserver* observer)->void;
        auto Deregister(AppComponents::HTXDataManager::IHTXDataManagerObserver* observer)->void;

        auto UpdateExperimentData(const QString& user, const QString& project, const QString& experiment) -> void override;

        auto WatchedAddData(const QString& fileFullPath)->void override;
        auto WatchedDeleteData(const QString& fileFullPath)->void override;
        auto WatchedUpdateData(const QString& fileFullPath)->void override;
        auto WatchedDeleteDataRootFolder(const QString& fileFullPath) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
