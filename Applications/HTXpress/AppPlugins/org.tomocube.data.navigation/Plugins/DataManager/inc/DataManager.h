#pragma once
#include <memory>
#include <QThread>

#include <IDataManager.h>
#include "HTX_Data_Navigation_DataManagerExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Plugins::DataManager {
    class HTX_Data_Navigation_DataManager_API DataManager : public QObject, public UseCase::IDataManager {
        Q_OBJECT

    public:
        DataManager();
        ~DataManager() override;

        auto SetScanExperiment(const QString& user, const QString& project, const QString& experiment) -> void override;

        auto InstallDataScannerOutputPort(UseCase::IDataOutputPort* outputPort) -> void override;
        auto UninstallDataScannerOutputPort(UseCase::IDataOutputPort* outputPort) -> void override;

        auto InstallDataMonitorOutputPort(UseCase::IDataOutputPort* outputPort)->void override;
        auto UninstallDataMonitorOutputPort(UseCase::IDataOutputPort* outputPort)->void override;

    private slots:
        void onScanExperimentData(const QString& user, const QString& project, const QString& experiment);
        void onWatchedDataAdded(const QString& fileFullPath);
        void onWatchedDataDeleted(const QString& fileFullPath);
        void onWatchedDataUpdated(const QString& fileFullPath);
        void onWatchedDataRootFolderDeleted(const QString& fileFullPath);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
