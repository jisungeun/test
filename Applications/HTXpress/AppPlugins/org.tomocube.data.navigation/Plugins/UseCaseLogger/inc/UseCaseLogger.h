#pragma once
#include <memory>

#include <IUseCaseLogger.h>
#include "HTX_Data_NAvigation_UseCaseLoggerExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Plugins::UseCaseLogger {
    class HTX_Data_Navigation_UseCaseLogger_API Logger : public UseCase::IUseCaseLogger {
    public:
        Logger();
        ~Logger() override;

    protected:
        auto Log(const QString& useCase, const QString& message)->void override;
        auto Error(const QString& useCase, const QString& message)->void override;
    };
}