#define LOGGER_TAG "[MainControl]"

#include <TCLogger.h>

#include "MainWindowControl.h"

#include <System.h>
#include <SessionManager.h>
#include <SystemStatus.h>

#include "Internal/ProjectUpdater.h"
#include <ProjectPresenter.h>
#include <ProjectController.h>

#include "Internal/DataUpdater.h"
#include <DataPresenter.h>
#include <DataController.h>

#include "Internal/InitializationUpdater.h"
#include <InitializationPresenter.h>
#include <InitializationController.h>

#include <ImageViewerController.h>
#include <ImageViewerManager.h>

#include "Internal/UserUpdater.h"
#include <UserPresenter.h>
#include <UserController.h>

#include <UseCaseLogger.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    struct MainWindowControl::Impl {
        std::shared_ptr<Interactor::DataPresenter> port;
        std::shared_ptr<Plugins::UseCaseLogger::Logger> usecaseLogger{ std::make_shared<Plugins::UseCaseLogger::Logger>() };
    };

    MainWindowControl::MainWindowControl() : d{new Impl} {
        d->port.reset(new Interactor::DataPresenter(DataUpdater::GetInstance().get()));
    }

    MainWindowControl::~MainWindowControl() {
        QLOG_INFO() << "Clean up";
    }

    auto MainWindowControl::InitializeUI()->void{
        auto updater = InitializationUpdater::GetInstance();
        auto presenter = Interactor::InitializationPresenter(updater.get());
        auto controller = Interactor::InitializationController(&presenter);
        if(false == controller.InitializeUI()) {
            // error 
        }
    }

    auto MainWindowControl::LoadUserList() -> bool {
        auto updater = UserUpdater::GetInstance();
        auto presenter = Interactor::UserPresenter(updater.get());
        auto controller = Interactor::UserController(&presenter);

        return controller.LoadUserList();
    }

    auto MainWindowControl::SetLoggedInUser() -> bool {
        auto updater = UserUpdater::GetInstance();
        auto presenter = Interactor::UserPresenter(updater.get());
        auto controller = Interactor::UserController(&presenter);

        return controller.ChangeUser(AppEntity::SessionManager::GetInstance()->GetID());
    }


    auto MainWindowControl::LoadProjectList()->void {
        auto updater = ProjectUpdater::GetInstance();
        auto presenter = Interactor::ProjectPresenter(updater.get());
        auto controller = Interactor::ProjectController(&presenter);

        if(false == controller.LoadProjectList(AppEntity::SessionManager::GetInstance()->GetID())) {
            // error 
        }
    }

    auto MainWindowControl::InstallDataScannerOutputPort()->void {
        auto controller = Interactor::DataController(nullptr);
        if (false == controller.InstallDataScannerOutputPort(d->port.get())) {
            //error message
        }
    }

    auto MainWindowControl::InstallDataMonitorOutputPort()->void {
        auto controller = Interactor::DataController(nullptr);
        if (false == controller.InstallDataMonitorOutputPort(d->port.get())) {
            //error message
        }
    }

    auto MainWindowControl::RunImageViewer(const QStringList& tcfList)->bool {
        const auto manager = std::make_shared<Plugins::ImageViewerManager::ImageViewerManager>();
        Interactor::ImageViewerController controller(manager);
        return controller.RunImageViewer(tcfList);
    }

    auto MainWindowControl::IsTimelapseRunning() -> bool {
        return AppEntity::SystemStatus::GetInstance()->GetTimelapseRunning();
    }
}
