﻿#include <QFileInfo>

#include <Experiment.h>
#include <ExperimentReader.h>

#include "ExperimentLoader.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ExperimentLoader::Impl {
        QString vesselFilePath{};
        QString experimentFilePath{};
    };

    ExperimentLoader::ExperimentLoader(const QString& tcfFilePath) : d{std::make_unique<Impl>()} {
        const QFileInfo fileInfo(tcfFilePath);
        d->experimentFilePath = fileInfo.absolutePath() + "/.experiment";
        d->vesselFilePath = fileInfo.absolutePath() + "/.vessel";
    }

    ExperimentLoader::~ExperimentLoader() {
    }

    auto ExperimentLoader::GetExperimentInDataFolder() const -> std::optional<AppEntity::Experiment::Pointer> {
        if (QFile::exists(d->vesselFilePath) && QFile::exists(d->experimentFilePath)) {
            AppComponents::ExperimentIO::ExperimentReader reader{};
            reader.SetVesselFilePath(d->vesselFilePath);
            const auto exp = reader.Read(d->experimentFilePath);
            if (exp) {
                return exp;
            }
        }

        return std::nullopt;
    }
}
