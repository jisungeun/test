#pragma once
#include <memory>
#include <enum.h>

#include <QString>
#include <QImage>

#include <Define.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    class ThumbnailWidgetControl {
    public:
        ThumbnailWidgetControl();
        ~ThumbnailWidgetControl();

        auto LoadHTThumbnail(const QString& path, QImage& image, bool& cache)->bool;
        auto LoadFLThumbnail(const QString& path, QImage& image, bool& cache)->bool;
        auto LoadBFThumbnail(const QString& path, QImage& image, bool& cache)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}