#include <QMutex>
#include <QFileInfo>
#include <QDir>

#include <FileUtility.h>
#include <UserController.h>

#include "DeleteDialogControl.h"


namespace HTXpress::AppPlugins::Data::Navigation {
    struct DeleteDialogControl::Impl {
        QList<QString> sourceList;
        Option option{ Option::RAWFILES_ONLY };

        QMutex mutex;
        bool running{ false };

        auto DeleteRawFilesOnly(const QString& source, Impl* p)->bool;
        auto DeleteAll(const QString& source, Impl* p)->bool;
    };

    auto DeleteDialogControl::Impl::DeleteRawFilesOnly(const QString& source, Impl* p) -> bool {
        QFileInfo finfo(source);
        const auto& targetPath = QString("%1/data").arg(finfo.absolutePath());

        if(!QFile::exists(source)) return false;

        return TC::RemoveRecursive(targetPath, [&p]()->bool {
            QMutexLocker locker(&p->mutex);
            return !p->running;
        });
    }

    auto DeleteDialogControl::Impl::DeleteAll(const QString& source, Impl* p) -> bool {
        QFileInfo finfo(source);

        return TC::RemoveRecursive(finfo.absolutePath(), [&p]()->bool {
            QMutexLocker locker(&p->mutex);
            return !p->running;
        });
    }

    DeleteDialogControl::DeleteDialogControl() : QThread(), d{ std::make_unique<Impl>() } {
    }

    DeleteDialogControl::~DeleteDialogControl() {
    }

    auto DeleteDialogControl::SetList(const QList<QString>& list) -> void {
        d->sourceList = list;
    }

    auto DeleteDialogControl::Start(Option option) -> void {
        d->option = option;

        QMutexLocker locker(&d->mutex);
        d->running = true;
        start();
    }

    auto DeleteDialogControl::Stop() -> void {
        d->mutex.lock();
        d->running = false;
        d->mutex.unlock();

        wait();
    }

    auto DeleteDialogControl::CheckPassword(const QString& password) const -> bool {
        auto controller = Interactor::UserController(nullptr);
        return controller.CheckPassword(password);
    }

    void DeleteDialogControl::run() {
        for(const auto source : d->sourceList) {
            switch(d->option) {
            case Option::RAWFILES_ONLY:
                if(!d->DeleteRawFilesOnly(source, d.get())) continue;
                break;
            case Option::ALL_FILES:
                d->DeleteAll(source, d.get());
                break;
            }

            QMutexLocker locker(&d->mutex);
            if(!d->running) break;
            emit sigDeleted();
        }

        emit sigCompleted();
    }
}
