#include "InitializationUpdater.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct InitializationUpdater::Impl {
        QList<InitializationObserver*> observers;
    };

    InitializationUpdater::InitializationUpdater() : d{ new Impl } {
    }

    InitializationUpdater::~InitializationUpdater() {
    }

    auto InitializationUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new InitializationUpdater() };
        return theInstance;
    }

    auto InitializationUpdater::InitializeUI() -> void{
        for (auto observer : d->observers) {
            observer->InitializeUI();
        }
    }

    auto InitializationUpdater::Register(InitializationObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto InitializationUpdater::Deregister(InitializationObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}