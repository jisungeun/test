#pragma once

#include <memory>

#include <QWidget>

namespace HTXpress::AppPlugins::Data::Navigation {
    class CollapseWidget : public QWidget {
        Q_OBJECT
    public:
        enum class Type {
            Specimen,
            Well
        };

        explicit CollapseWidget(QWidget* parent = nullptr);
        ~CollapseWidget() override;

        auto SetTitle(const QString& title) -> void;
        auto GetTitle() const -> QString;

        auto SetType(const Type& widgetType) -> void;
        auto GetType() const -> Type;

        auto AddWidget(QWidget* widget) -> void;

        auto ExpandAll() -> void;

    signals:
        void sigChangedCollapse();

    protected slots:
        void onExpanded(bool);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
