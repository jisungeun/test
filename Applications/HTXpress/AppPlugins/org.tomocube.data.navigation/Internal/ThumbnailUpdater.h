#pragma once

#include <memory>

#include <IThumbnailView.h>
#include "ThumbnailObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class ThumbnailUpdater : public Interactor::IThumbnailView {
    public:
        using Pointer = std::shared_ptr<ThumbnailUpdater>;

    protected:
        ThumbnailUpdater();

    public:
        ~ThumbnailUpdater();

        static auto GetInstance()->Pointer;

        auto LoadedThumbnail(const QString& path, const QImage& image)->void override;
        
        auto Register(ThumbnailObserver* observer)->void;
        auto Deregister(ThumbnailObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}