#pragma once
#include <memory>
#include <QList>
#include <QString>
#include <QThread>

namespace HTXpress::AppPlugins::Data::Navigation {
    class CopyDialogControl : public QThread {
        Q_OBJECT

    public:
        enum Option {
            TCF_ONLY,
            TCF_ONLY_KEEP_HIERARCHY,
            ALL_FILES
        };

    public:
        CopyDialogControl();
        ~CopyDialogControl();

        auto SetList(const QList<QString>& list)->void;
        auto Start(Option option, const QString& target)->void;
        auto Stop()->void;

    protected:
        auto run() -> void override;

    signals:
        void sigCopied();
        void sigCompleted();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}