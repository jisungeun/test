#include "DataTreeWidget.h"

#include <QHeaderView>

using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;

namespace HTXpress::AppPlugins::Data::Navigation {
    struct DataTreeWidget::Impl {
        const int _treeNameIndex = 0;
        const int _treeTypeIndex = 1;
        const int _treeCountIndex = 2;

        auto GetParentExperimentItem(QTreeWidgetItem* item)->QTreeWidgetItem*;

        auto GetDataCount(QTreeWidgetItem* item) const ->int;
        auto IncreaseDataCount(QTreeWidgetItem* item) const ->void;
        auto DecreaseDataCount(QTreeWidgetItem* item) const ->void;
    };

    auto DataTreeWidget::Impl::GetParentExperimentItem(QTreeWidgetItem* item)->QTreeWidgetItem* {
        QTreeWidgetItem* experimentItem = nullptr;
        if (item->type() == +TreeItemType::Experiment) {
            experimentItem = item;
        }
        else {
            QTreeWidgetItem* parentItem = item->parent();
            while (parentItem) {
                if (parentItem->type() == +TreeItemType::Experiment) {
                    experimentItem = parentItem;
                    break;
                }
                parentItem = parentItem->parent();
            }
        }
        return experimentItem;
    }

    auto DataTreeWidget::Impl::GetDataCount(QTreeWidgetItem* item) const ->int {
        if (nullptr == item) return -1;
        return item->text(_treeCountIndex).toInt();
    }

    auto DataTreeWidget::Impl::IncreaseDataCount(QTreeWidgetItem* item) const ->void {
        if (nullptr == item) return;
        const auto count = GetDataCount(item) + 1;
        item->setText(_treeCountIndex, QString::number(count));
    }

    auto DataTreeWidget::Impl::DecreaseDataCount(QTreeWidgetItem* item) const ->void {
        if (nullptr == item) return;
        const auto count = GetDataCount(item) - 1;
        item->setText(_treeCountIndex, QString::number(count));
    }

    DataTreeWidget::DataTreeWidget(QWidget* parent): QTreeWidget(parent), d{ new Impl } {
        clear();
        setStyleSheet("QTreeView:disabled { color: #F0F0F0; }");
        setColumnCount(3);
        hideColumn(d->_treeTypeIndex);
        hideColumn(d->_treeCountIndex);
        setHeaderLabels(QStringList(""));

        header()->setSectionResizeMode(QHeaderView::ResizeToContents);
        header()->setStretchLastSection(false);

        setRootIsDecorated(false);

        setSortingEnabled(true);
        sortByColumn(d->_treeTypeIndex, Qt::SortOrder::AscendingOrder);

        setContextMenuPolicy(Qt::NoContextMenu);


        connect(this, SIGNAL(itemClicked(QTreeWidgetItem*, int)), this, SLOT(onTreeItemClicked(QTreeWidgetItem*, int)));
        connect(this, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)), this, SLOT(onTreeCurrentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)));
    }

    DataTreeWidget::~DataTreeWidget() {
    }

    auto DataTreeWidget::SetExperimentList(const QList<QString>& experiments)->void {
        clear();
        for(const auto& experiment : experiments) {
            if(experiment.isEmpty()) continue;
            this->addTopLevelItem(MakeTreeItem(TreeItemType::Experiment, experiment));
        }
    }

    auto DataTreeWidget::SetSpecimenList(const QString& experiment, TCFDataRepo::SpecimenList specimens)->void {
        if (experiment.isEmpty()) return;
        if (specimens.isEmpty()) return;

        QTreeWidgetItem* experimentItem = nullptr;
        for (auto i = 0; i < this->topLevelItemCount(); ++i) {
            const auto item = this->topLevelItem(i);
            if (item->text(d->_treeNameIndex) == experiment) {
                experimentItem = item;
                break;
            }
        }

        if (nullptr == experimentItem) return;
        if (0 < experimentItem->childCount()) return;

        TCFDataRepo::SpecimenList::iterator specIter;
        for (specIter = specimens.begin(); specIter != specimens.end(); ++specIter) {
            if (specIter.key().isEmpty()) continue;
            auto specimenItem = MakeTreeItem(TreeItemType::Specimen, specIter.key());
            experimentItem->addChild(specimenItem);

            // well
            auto wells = specIter.value();
            TCFDataRepo::WellList::iterator wellIter;
            for (wellIter = wells.begin(); wellIter != wells.end(); ++wellIter) {
                if (wellIter.key().isEmpty()) continue;
                auto wellItem = MakeTreeItem(TreeItemType::Well, wellIter.key());
                specimenItem->addChild(wellItem);

                // data count
                d->IncreaseDataCount(wellItem);
                d->IncreaseDataCount(specimenItem);
                d->IncreaseDataCount(experimentItem);
            }
        }
    }

    auto DataTreeWidget::SetCurrentItem(QTreeWidgetItem* item)->void {
        if(nullptr == item) return;
        setCurrentItem(item, d->_treeNameIndex);
        item->setCheckState(d->_treeNameIndex, Qt::Checked);
    }

    auto DataTreeWidget::SetCurrentExperimentItem(const QString& experiment)->void {
        if (experiment.isEmpty()) return;

        for (auto i = 0; i < this->topLevelItemCount(); ++i) {
            const auto experimentItem = this->topLevelItem(i);
            if (experimentItem->text(d->_treeNameIndex) == experiment) {
                SetCurrentItem(experimentItem);
                return;
            }
        }
    }

    void DataTreeWidget::AddItem(const QString& experiment, const QString& specimen, const QString& well, const QString& file) {
        if (experiment.isEmpty()) return;
        if (specimen.isEmpty()) return;
        if (well.isEmpty()) return;
        if (file.isEmpty()) return;

        QTreeWidgetItem* findExperimentItem = nullptr;
        QTreeWidgetItem* findSpecimenItem = nullptr;
        QTreeWidgetItem* findWellItem = nullptr;

        for (auto i = 0; i < this->topLevelItemCount(); ++i) {
            const auto experimentItem = this->topLevelItem(i);
            if (experimentItem->text(d->_treeNameIndex) == experiment) {
                findExperimentItem = experimentItem;

                for (auto j = 0; j < experimentItem->childCount(); ++j) {
                    const auto specimenItem = experimentItem->child(j);
                    if (specimenItem->text(d->_treeNameIndex) == specimen) {
                        findSpecimenItem = specimenItem;

                        for (auto k = 0; k < specimenItem->childCount(); ++k) {
                            const auto wellItem = specimenItem->child(k);
                            if (wellItem->text(d->_treeNameIndex) == well) {
                                findWellItem = wellItem;
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (nullptr == findExperimentItem) {
            findExperimentItem = MakeTreeItem(TreeItemType::Experiment, experiment);
            findSpecimenItem = MakeTreeItem(TreeItemType::Specimen, specimen);
            findWellItem = MakeTreeItem(TreeItemType::Well, well);
            findSpecimenItem->addChild(findWellItem);
            findExperimentItem->addChild(findSpecimenItem);
            this->addTopLevelItem(findExperimentItem);
        }
        else if (nullptr != findExperimentItem && nullptr == findSpecimenItem) {
            findSpecimenItem = MakeTreeItem(TreeItemType::Specimen, specimen);
            findWellItem = MakeTreeItem(TreeItemType::Well, well);
            findSpecimenItem->addChild(findWellItem);
            findExperimentItem->addChild(findSpecimenItem);
        }
        else if (nullptr != findSpecimenItem && nullptr == findWellItem) {
            findWellItem = MakeTreeItem(TreeItemType::Well, well);
            findSpecimenItem->addChild(findWellItem);
        }

        d->IncreaseDataCount(findWellItem);
        d->IncreaseDataCount(findSpecimenItem);
        d->IncreaseDataCount(findExperimentItem);

        if (1 > d->GetDataCount(findWellItem)) {

        }
    }

    void DataTreeWidget::UpdateItem(const QString& experiment, const QString& specimen, const QString& well, const QString& file) {
        if (experiment.isEmpty()) return;
        if (specimen.isEmpty()) return;
        if (well.isEmpty()) return;
        if (file.isEmpty()) return;
    }

    void DataTreeWidget::DeleteItem(const QString& experiment, const QString& specimen, const QString& well, const QString& file) {
        if (experiment.isEmpty()) return;
        if (specimen.isEmpty()) return;
        if (well.isEmpty()) return;
        if (file.isEmpty()) return;

        QTreeWidgetItem* findExperimentItem = nullptr;
        QTreeWidgetItem* findSpecimenItem = nullptr;
        QTreeWidgetItem* findWellItem = nullptr;

        for (auto i = 0; i < this->topLevelItemCount(); ++i) {
            const auto experimentItem = this->topLevelItem(i);
            if (experimentItem->text(d->_treeNameIndex) == experiment) {
                findExperimentItem = experimentItem;

                for (auto j = 0; j < experimentItem->childCount(); ++j) {
                    const auto specimenItem = experimentItem->child(j);
                    if (specimenItem->text(d->_treeNameIndex) == specimen) {
                        findSpecimenItem = specimenItem;

                        for (auto k = 0; k < specimenItem->childCount(); ++k) {
                            const auto wellItem = specimenItem->child(k);
                            if (wellItem->text(d->_treeNameIndex) == well) {
                                findWellItem = wellItem;
                                break;
                            }
                        }
                    }
                }
            }
        }

        if (nullptr != findExperimentItem && nullptr != findSpecimenItem && nullptr != findWellItem) {
            d->DecreaseDataCount(findWellItem);
            d->DecreaseDataCount(findSpecimenItem);
            d->DecreaseDataCount(findExperimentItem);
        }
    }

    void DataTreeWidget::onTreeItemClicked(QTreeWidgetItem* item, int column) {
        if (nullptr == item) return;
        if (d->_treeNameIndex != column) return;

        if (item->type() != +TreeItemType::Experiment) return;

        if (Qt::Unchecked == item->checkState(d->_treeNameIndex)) {
            item->setCheckState(d->_treeNameIndex, Qt::Checked);
            return;
        }

        this->setCurrentItem(item, column);
    }

    void DataTreeWidget::onTreeCurrentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous) {
        if (nullptr == current) return;
        if (current == previous) return;

        auto currentExperimentItem = d->GetParentExperimentItem(current);
        if (nullptr == currentExperimentItem) {
            return;
        }

        if (nullptr != previous) {
            auto previousExperimentItem = d->GetParentExperimentItem(previous);
            if (nullptr == previousExperimentItem) {
                return;
            }

            if (previousExperimentItem != currentExperimentItem) {
                currentExperimentItem->setCheckState(d->_treeNameIndex, Qt::CheckState::Checked);
                previousExperimentItem->setCheckState(d->_treeNameIndex, Qt::CheckState::Unchecked);
            }
        }

        const QString experiment = currentExperimentItem->text(d->_treeNameIndex);
        QString specimen, well;
        if (current->type() == +TreeItemType::Specimen) {
            specimen = current->text(d->_treeNameIndex);
        }
        else if (current->type() == +TreeItemType::Well) {
            const auto parent = current->parent();
            if (parent) specimen = parent->text(d->_treeNameIndex);
            well = current->text(d->_treeNameIndex);
        }

        emit sigCurrentItemChanged(experiment, specimen, well);
    }

    auto DataTreeWidget::MakeTreeItem(const TreeItemType& type, const QString& text)->QTreeWidgetItem* {
        auto GetIcon = [=](const TreeItemType& itemType) {
            switch (itemType) {
            case TreeItemType::Project:
                return QIcon();
            case TreeItemType::Experiment:
                return QIcon();
            case TreeItemType::Specimen:
                return QIcon(":/img/tsx/specimen");
            case TreeItemType::Well:
                return QIcon();
            case TreeItemType::Data:
                return QIcon();
            }

            return QIcon();
        };

        auto item = new QTreeWidgetItem(type);
        item->setText(d->_treeNameIndex, text);
        item->setToolTip(d->_treeNameIndex, text);
        item->setIcon(d->_treeNameIndex, GetIcon(type));
        item->setText(d->_treeTypeIndex, QString::number(type));
        item->setText(d->_treeCountIndex, QString::number(0));

        const auto w = this->width();
        auto font = this->font();

        if (type == +TreeItemType::Experiment) {
            item->setCheckState(d->_treeNameIndex, Qt::Unchecked);
            item->setSizeHint(d->_treeNameIndex, QSize(360, 35));

            font.setBold(true);
            font.setPointSize(12);
            item->setFont(d->_treeNameIndex, font);

            const QBrush brush(QColor(255, 255, 255));
            item->setForeground(d->_treeNameIndex, brush);
        }
        else if (type == +TreeItemType::Specimen) {
            item->setSizeHint(d->_treeNameIndex, QSize(330, 30));

            font.setBold(false);
            font.setPointSize(12);
            item->setFont(d->_treeNameIndex, font);

            const QBrush brush(QColor(255, 255, 255));
            item->setForeground(d->_treeNameIndex, brush);
        }
        else if (type == +TreeItemType::Well) {
            item->setSizeHint(d->_treeNameIndex, QSize(300, 26));

            font.setBold(false);
            font.setPointSize(11);
            item->setFont(d->_treeNameIndex, font);

            const QBrush brush(QColor(111, 130, 144));
            item->setForeground(d->_treeNameIndex, brush);
        }

        return item;
    }

    auto DataTreeWidget::RemoveTreeItem(QTreeWidgetItem* item, const bool& isExperiment)->void {
        if (nullptr == item) return;

        if (isExperiment) {
            const auto index = indexOfTopLevelItem(item);
            takeTopLevelItem(index);
            return;
        }

        auto parent = item->parent();
        if (nullptr == parent)
            return;

        parent->removeChild(item);
        delete item;
    }
}
