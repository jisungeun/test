#include "InitializationObserver.h"
#include "InitializationUpdater.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    InitializationObserver::InitializationObserver(QObject* parent) : QObject(parent) {
        InitializationUpdater::GetInstance()->Register(this);
    }

    InitializationObserver::~InitializationObserver() {
        InitializationUpdater::GetInstance()->Deregister(this);
    }

    auto InitializationObserver::InitializeUI() -> void {
        emit sigInitialized();
    }
}
