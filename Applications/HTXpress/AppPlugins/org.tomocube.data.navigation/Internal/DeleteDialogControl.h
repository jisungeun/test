#pragma once
#include <memory>
#include <QList>
#include <QString>
#include <QThread>

namespace HTXpress::AppPlugins::Data::Navigation {
    class DeleteDialogControl : public QThread {
        Q_OBJECT

    public:
        enum Option {
            RAWFILES_ONLY,
            ALL_FILES
        };

    public:
        DeleteDialogControl();
        ~DeleteDialogControl();

        auto SetList(const QList<QString>& list)->void;
        auto Start(Option option)->void;
        auto Stop()->void;

        auto CheckPassword(const QString& password) const->bool;

    protected:
        auto run() -> void override;

    signals:
        void sigDeleted();
        void sigCompleted();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}