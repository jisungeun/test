#pragma once

#include <memory>
#include <QString>
#include <QWidget>

#include "Experiment.h"
#include "TCFMetaReader.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class DataInfoDialog : public QWidget
    {
        Q_OBJECT
    public:
        explicit DataInfoDialog(QWidget* parent = nullptr);
        ~DataInfoDialog() override;

        auto SetData(const AppEntity::Experiment::Pointer& experiment, const QString& path)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
