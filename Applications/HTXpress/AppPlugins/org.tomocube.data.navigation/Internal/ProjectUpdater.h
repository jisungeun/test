#pragma once

#include <memory>

#include <IProjectView.h>
#include "ProjectObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class ProjectUpdater : public Interactor::IProjectView {
    public:
        using Pointer = std::shared_ptr<ProjectUpdater>;

    protected:
        ProjectUpdater();

    public:
        ~ProjectUpdater();

        static auto GetInstance()->Pointer;

        auto UpdateProjectList(const QList<QString>& projects) -> void override;

        auto ChangeCurrentProject(const QString& projectTitle)->void override;

        auto Register(ProjectObserver* observer)->void;
        auto Deregister(ProjectObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}