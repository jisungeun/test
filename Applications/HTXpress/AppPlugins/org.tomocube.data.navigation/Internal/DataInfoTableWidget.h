#pragma once

#include <memory>
#include <QString>
#include <QWidget>

#include "TCFMetaReader.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class DataInfoTableWidget : public QWidget
    {
        Q_OBJECT
    public:
        explicit DataInfoTableWidget(QWidget* parent = nullptr);
        ~DataInfoTableWidget();

        auto SetTitle(const QString& title) const ->void;
        auto SetPath(const QString& path)->bool;
        auto SetMeta(const TC::IO::TCFMetaReader::Meta::Pointer& meta) const ->bool;
        auto SetVisibleWidgetTitle(const bool& visible) const ->void;
        auto SetHeaderWidth(const int& width) const ->void;

        auto Clear() const ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
