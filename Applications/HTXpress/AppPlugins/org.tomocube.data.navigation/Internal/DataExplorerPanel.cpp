#include <TCFDataRepo.h>

#include "DataExplorerPanel.h"
#include "ui_DataExplorerPanel.h"
#include "DataExplorerPanelControl.h"

#include "ExperimentUpdater.h"
#include "CurrentItemUpdater.h"
#include "ProjectObserver.h"
#include "ExperimentObserver.h"
#include "DataObserver.h"
#include "InitializationObserver.h"
#include "UserObserver.h"

using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;

namespace HTXpress::AppPlugins::Data::Navigation {
    struct DataExplorerPanel::Impl {
        Ui::DataExplorerPanel ui;
        DataExplorerPanelControl control;

        UserObserver*           userObserver{ nullptr };
        ProjectObserver*        projectObserver{ nullptr };
        ExperimentObserver*     experimentObserver{ nullptr };
        DataObserver*           dataObserver{ nullptr };
        InitializationObserver* initObserver{ nullptr };

        AppEntity::UserID currentUser;
        QString currentProject;
        QString currentExperiment;
    };

    DataExplorerPanel::DataExplorerPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.baseWidget->setObjectName("panel");
        d->ui.line->setObjectName("line-divider");

        d->ui.exprimentButton->setIconSize(QSize(24, 24));
        d->ui.exprimentButton->AddIcon(":/img/tsx/experiment");
        d->ui.exprimentButton->AddIcon(":/img/tsx/experiment-d", QIcon::Disabled);

        d->ui.imageButton->setIconSize(QSize(24, 24));
        d->ui.imageButton->AddIcon(":/img/tsx/setup-imaging");
        d->ui.imageButton->AddIcon(":/img/tsx/setup-imaging-d", QIcon::Disabled);

        d->userObserver =       new UserObserver;
        d->projectObserver =    new ProjectObserver;
        d->experimentObserver = new ExperimentObserver;
        d->dataObserver =       new DataObserver;
        d->initObserver =       new InitializationObserver;

        connect(d->userObserver, SIGNAL(sigUserChanged(const AppEntity::UserID&)), this, SLOT(onChangeUser(const AppEntity::UserID&)));

        connect(d->projectObserver, SIGNAL(sigCurrentProjectChanged(const QString&)), this, SLOT(onChangeCurrentProject(const QString&)));

        connect(d->experimentObserver, SIGNAL(sigExperimentListLoaded(const QList<QString>&)), this, SLOT(onLoadedExperimentList(const QList<QString>&)));

        connect(d->dataObserver, &DataObserver::sigExperimentDataScanned, this, &DataExplorerPanel::onExperimentDataScanned);
        connect(d->dataObserver, &DataObserver::sigDataAdded,             this, &DataExplorerPanel::onDataAdded);
        connect(d->dataObserver, &DataObserver::sigDataUpdated,           this, &DataExplorerPanel::onDataUpdated);
        connect(d->dataObserver, &DataObserver::sigDataRootFolderDeleted, this, &DataExplorerPanel::onDataRootFolderDeleted);

        connect(d->initObserver, SIGNAL(sigInitialized()), this, SLOT(onInitialized()));

        connect(d->ui.treeWidget, &DataTreeWidget::sigCurrentItemChanged, this, &DataExplorerPanel::onTreeCurrentItemChanged);

        connect(d->ui.exprimentButton, SIGNAL(clicked()), this, SIGNAL(sigGotoExperimentSetup()));
        connect(d->ui.imageButton, SIGNAL(clicked()), this, SIGNAL(sigGotoExperimentPerform()));
    }

    DataExplorerPanel::~DataExplorerPanel() {
    }

    auto DataExplorerPanel::SetCurrentExperiment(const QString& experiment)->void {
        if (experiment.isEmpty()) return;
        d->ui.treeWidget->SetCurrentExperimentItem(experiment);
    }

    void DataExplorerPanel::onChangeUser(const AppEntity::UserID& user) {
        d->currentUser = user;

        d->currentProject = "";

        d->currentExperiment = "";

        d->ui.treeWidget->clear();
        d->ui.treeWidget->setRootIsDecorated(false);

        const auto projects = d->control.GetProjects();
        if (!projects.isEmpty()) {
            d->currentProject = projects.first();
            d->control.LoadExperimentList();
        }
    }

    void DataExplorerPanel::onChangeCurrentProject(const QString& projectTitle) {
        if (projectTitle.isEmpty()) return;

        d->ui.treeWidget->clear();
        d->ui.treeWidget->setRootIsDecorated(false);

        d->currentProject = projectTitle;
        d->currentExperiment = "";

        d->control.LoadExperimentList();
    }

    void DataExplorerPanel::onLoadedExperimentList(const QList<QString>& experiments) {
        auto list = experiments;
        std::sort(list.begin(), list.end(),
            [](const QString& name1, const QString& name2) -> bool
            {
                return name1.toLower() < name2.toLower();
            });

        
        d->ui.treeWidget->SetExperimentList(list);

        // set current experiment
        if (0 == d->ui.treeWidget->topLevelItemCount()) {
            ExperimentUpdater::GetInstance()->ChangeCurrentExperiment(d->currentProject, "");
            CurrentItemUpdater::GetInstance()->ChangeCurrentItem(d->currentUser, d->currentProject, "", "", "");
        }
        else {
            d->ui.treeWidget->SetCurrentItem(d->ui.treeWidget->topLevelItem(0));
        }
    }

    void DataExplorerPanel::onTreeCurrentItemChanged(const QString& experiment, const QString& specimen, const QString& well) {
        if (experiment.isEmpty()) return;

        CurrentItemUpdater::GetInstance()->ChangeCurrentItem(d->currentUser, d->currentProject, experiment, specimen, well);

        if (d->currentExperiment != experiment) {
            d->currentExperiment = experiment;
            ExperimentUpdater::GetInstance()->ChangeCurrentExperiment(d->currentProject, experiment);
            d->control.LoadExperiment(d->currentExperiment);
            d->control.ScanExperimentData(d->currentExperiment);
        }
    }

    void DataExplorerPanel::onExperimentDataScanned(const QString& user, const QString& project, const QString& experiment) {
        if (user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;

         const auto experiments = TCFDataRepo::GetInstance()->GetSpecimenList(user, project, experiment);
         d->ui.treeWidget->SetSpecimenList(experiment, experiments);

         CurrentItemUpdater::GetInstance()->ChangeCurrentItem(user, project, experiment, "", "");
    }

    void DataExplorerPanel::onDataAdded(const QString& fileFullPath) {
        if (fileFullPath.isEmpty()) return;

        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);

        if (d->currentProject != project) return;

        d->ui.treeWidget->AddItem(experiment, specimen, well, fileFullPath);
    }

    void DataExplorerPanel::onDataUpdated(const QString& fileFullPath) {
        if (fileFullPath.isEmpty()) return;

        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);

        if (d->currentProject != project) return;

        d->ui.treeWidget->UpdateItem(experiment, specimen, well, fileFullPath);
    }

    void DataExplorerPanel::onDataRootFolderDeleted(const QString& fileFullPath) {
        if (fileFullPath.isEmpty()) return;

        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);

        if (d->currentProject != project) return;

        d->ui.treeWidget->DeleteItem(experiment, specimen, well, fileFullPath);
    }

    void DataExplorerPanel::onInitialized() {
        d->ui.imageButton->setVisible(d->control.IsLoadedExperiment());
    }
}
