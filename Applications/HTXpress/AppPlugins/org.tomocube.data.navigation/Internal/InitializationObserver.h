#pragma once

#include <memory>
#include <QObject>

namespace HTXpress::AppPlugins::Data::Navigation {
    class InitializationObserver : public QObject {
        Q_OBJECT

    public:
        using Pointer = std::shared_ptr<InitializationObserver>;

        InitializationObserver(QObject* parent = nullptr);
        ~InitializationObserver();

        auto InitializeUI()->void;

    signals:
        void sigInitialized();
    };
}