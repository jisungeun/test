#pragma once
#include <memory>

#include <QString>

#include <Position.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    class ListPanelControl {
    public:
        ListPanelControl();
        ~ListPanelControl();

        auto LoadAcqInfo(const QString& path, int& wellIndex, AppEntity::Position& position) -> void;

        auto ParseUserName(const QString& fileFullPath) const -> QString;
        auto ParseProjectName(const QString& fileFullPath) const -> QString;
        auto ParseExperimentName(const QString& fileFullPath) const -> QString;
        auto ParseSpecimenName(const QString& fileFullPath) const -> QString;
        auto ParseWellName(const QString& fileFullPath) const -> QString;
        auto ParseDataName(const QString& fileFullPath) const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
