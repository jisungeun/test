#pragma once

#include <memory>
#include <QObject>

#include "Experiment.h"

namespace HTXpress::AppPlugins::Data::Navigation{
    class ExperimentObserver : public QObject {
        Q_OBJECT

    public:
        using Pointer = std::shared_ptr<ExperimentObserver>;

        ExperimentObserver(QObject* parent = nullptr);
        ~ExperimentObserver() override;

        auto ChangeCurrentExperiment(const QString& project, const QString& experiment)->void;
        auto LoadExperiment(const AppEntity::Experiment::Pointer& experiment)->void;
        auto LoadExperimentList(const QList<QString>& experiments)->void;

    signals:
        void sigCurrentExperimentChanged(const QString& project, const QString& experiment);
        void sigExperimentLoaded(const AppEntity::Experiment::Pointer& experiment);
        void sigExperimentListLoaded(const QList<QString>& experiments);
    };
}
