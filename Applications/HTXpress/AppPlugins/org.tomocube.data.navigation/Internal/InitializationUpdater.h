#pragma once

#include <memory>

#include <IInitializationView.h>
#include "InitializationObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class InitializationUpdater : public Interactor::IInitializationView {
    public:
        using Pointer = std::shared_ptr<InitializationUpdater>;

    protected:
        InitializationUpdater();

    public:
        ~InitializationUpdater();

        static auto GetInstance()->Pointer;

        auto InitializeUI() -> void override;

        auto Register(InitializationObserver* observer)->void;
        auto Deregister(InitializationObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}