#include <QTableWidgetItem>

#include <TCFDataRepo.h>
#include <DataMisc.h>

#include "DataInfoTableWidget.h"
#include "ui_DataInfoTableWidget.h"
#include "DataInfoTableWidgetControl.h"

using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;
using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Data::Navigation {
    struct DataInfoTableWidget::Impl {
        Ui::DataInfoTableWidget ui;
        DataInfoTableWidgetControl control;

        const QStringList headerList = { "Type", "Modality", "Position", "Timeframe", "FL ch1", "FL ch2" , "FL ch3" };
        const QString typeSingle = "Single";
        const QString typeTimelapse = "Time-lapse";

        auto Clear()->void;
        auto GetTypeText(const TC::IO::TCFMetaReader::Meta::Pointer& meta) const ->QString;
        auto GetModalityText(const TC::IO::TCFMetaReader::Meta::Pointer& meta)->QString;
        auto GetPositionText(const TC::IO::TCFMetaReader::Meta::Pointer& meta)->QString;
        auto GetFLChannelInfoText(const TC::IO::TCFMetaReader::Meta::Pointer& meta, const int& ch)->QString;
    };

    DataInfoTableWidget::DataInfoTableWidget(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);
        d->ui.textLabel->setObjectName("label-h2");
        d->ui.iconLabel->setStyleSheet("background-image: url(:/img/tsx/fileinfo-2);");
        d->ui.titleIconLabel->setStyleSheet("background-image: url(:/img/tsx/fileinfo);");

        d->ui.frame->setObjectName("fr-tsx-round");
        d->ui.titleFrame->setObjectName("fr-tsx-inner");
        d->ui.titleLabel->setObjectName("lb-tsx-inner-title");

        d->ui.tableWidget->setObjectName("tv-tsx-vertical");
        d->ui.tableWidget->verticalHeader()->setObjectName("hv-tsx-vertical");

        d->ui.tableWidget->setRowCount(d->headerList.size());
        d->ui.tableWidget->setColumnCount(1);
        d->ui.tableWidget->setVerticalHeaderLabels(d->headerList);
        d->ui.tableWidget->verticalHeader()->setFixedWidth(170);
        d->ui.tableWidget->verticalHeader()->setStretchLastSection(true);
        d->ui.tableWidget->setHorizontalHeaderLabels({ "" });
        d->ui.tableWidget->horizontalHeader()->setVisible(false);
        d->ui.tableWidget->horizontalHeader()->setStretchLastSection(true);
    }

    DataInfoTableWidget::~DataInfoTableWidget() {
    }

    auto DataInfoTableWidget::SetTitle(const QString& title) const ->void {
        d->ui.titleLabel->setText(title);
    }

    auto DataInfoTableWidget::SetPath(const QString& path)->bool {
        if (path.isEmpty()) return false;

        const auto projectName = DataMisc::GetProjectName(path);
        const auto experimentName = DataMisc::GetExperimentName(path);
        const auto specimenName = DataMisc::GetSpecimenName(path);
        const auto wellName = DataMisc::GetWellName(path);
        const auto tcfBaseName = DataMisc::GetTcfBaseName(path);
        const auto tcfTypeName = DataMisc::GetTcfTypeInfo(path);

        const auto title = QString("%1-%2-%3").arg(specimenName, wellName, tcfTypeName);
        this->SetTitle(title);

        Clear();

        const auto meta = d->control.GetMeta(path);

        // type
        QString type;
        if (tcfTypeName.left(1)== "S") type = d->typeSingle;
        else if (tcfTypeName.left(1) == "T") type = d->typeTimelapse;

        if (type.isEmpty()) type = d->GetTypeText(meta);

        auto typeItem = new QTableWidgetItem(type);
        typeItem->setTextAlignment(Qt::AlignCenter);
        d->ui.tableWidget->setItem(0, 0, typeItem);
        
        return this->SetMeta(meta);
    }

    auto DataInfoTableWidget::SetMeta(const TC::IO::TCFMetaReader::Meta::Pointer& meta) const ->bool {
        if (nullptr == meta) return false;

        if(d->ui.titleLabel->text().isEmpty()) d->ui.titleLabel->setText(meta->common.title);

        auto modalityItem = new QTableWidgetItem(d->GetModalityText(meta));
        modalityItem->setTextAlignment(Qt::AlignCenter);
        d->ui.tableWidget->setItem(1, 0, modalityItem);

        auto positionItem = new QTableWidgetItem(d->GetPositionText(meta));
        positionItem->setTextAlignment(Qt::AlignCenter);
        d->ui.tableWidget->setItem(2, 0, positionItem);

        auto timeFrameItem = new QTableWidgetItem(QString::number(meta->data.total_time.count()));
        timeFrameItem->setTextAlignment(Qt::AlignCenter);
        d->ui.tableWidget->setItem(3, 0, timeFrameItem);

        auto chItem0 = new QTableWidgetItem(d->GetFLChannelInfoText(meta, 0));
        chItem0->setTextAlignment(Qt::AlignCenter);
        d->ui.tableWidget->setItem(4, 0, chItem0);

        auto chItem1 = new QTableWidgetItem(d->GetFLChannelInfoText(meta, 1));
        chItem1->setTextAlignment(Qt::AlignCenter);
        d->ui.tableWidget->setItem(5, 0, chItem1);

        auto chItem2 = new QTableWidgetItem(d->GetFLChannelInfoText(meta, 2));
        chItem2->setTextAlignment(Qt::AlignCenter);
        d->ui.tableWidget->setItem(6, 0, chItem2);

        return true;
    }

    auto DataInfoTableWidget::SetVisibleWidgetTitle(const bool& visible) const -> void {
        d->ui.textLabel->setVisible(visible);
        d->ui.iconLabel->setVisible(visible);
    }

    auto DataInfoTableWidget::SetHeaderWidth(const int& width) const ->void {
        d->ui.tableWidget->verticalHeader()->setFixedWidth(width);
    }

    auto DataInfoTableWidget::Clear() const ->void {
        d->ui.tableWidget->setRowCount(0);

        d->ui.tableWidget->setRowCount(d->headerList.size());
        d->ui.tableWidget->setColumnCount(1);
        d->ui.tableWidget->setVerticalHeaderLabels(d->headerList);
    }

    auto DataInfoTableWidget::Impl::GetTypeText(const TC::IO::TCFMetaReader::Meta::Pointer& meta) const ->QString {
        if (nullptr == meta) return QString();

        if(1 < meta->data.total_time.count()) return typeTimelapse;

        return typeSingle;
    }

    auto DataInfoTableWidget::Impl::GetModalityText(const TC::IO::TCFMetaReader::Meta::Pointer& meta)->QString {
        QString text;
        if (nullptr == meta) return text;

        if (meta->data.data3D.exist)      text.append("HT 3D / ");
        if (meta->data.data2DMIP.exist)   text.append("HT MIP / ");
        if (meta->data.data2D.exist)      text.append("HT / ");
        if (meta->data.dataBF.exist)      text.append("BF / ");
        if (meta->data.data3DFL.exist)    text.append("FL 3D / ");
        if (meta->data.data2DFLMIP.exist) text.append("FL / ");
        if (false == text.isEmpty())      text = text.left(text.length() - 3);

        return text;
    }

    auto DataInfoTableWidget::Impl::GetPositionText(const TC::IO::TCFMetaReader::Meta::Pointer& meta)->QString {
        if (nullptr == meta) return QString();

        if (meta->data.data3D.exist)      return QString("X: %1 Y: %2 Z: %3").arg(meta->data.data3D.positionX).arg(meta->data.data3D.positionY).arg(meta->data.data3D.positionZ);
        if (meta->data.data2DMIP.exist)   return QString("X: %1 Y: %2 Z: %3").arg(meta->data.data2DMIP.positionX).arg(meta->data.data2DMIP.positionY).arg(meta->data.data2DMIP.positionZ);
        if (meta->data.data2D.exist)      return QString("X: %1 Y: %2 Z: %3").arg(meta->data.data2D.positionX).arg(meta->data.data2D.positionY).arg(meta->data.data2D.positionZ);
        if (meta->data.dataBF.exist)      return QString("X: %1 Y: %2 Z: %3").arg(meta->data.dataBF.positionX).arg(meta->data.dataBF.positionY).arg(meta->data.dataBF.positionZ);
        if (meta->data.data3DFL.exist)    return QString("X: %1 Y: %2 Z: %3").arg(meta->data.data3DFL.positionX).arg(meta->data.data3DFL.positionY).arg(meta->data.data3DFL.positionZ);
        if (meta->data.data2DFLMIP.exist) return QString("X: %1 Y: %2 Z: %3").arg(meta->data.data2DFLMIP.positionX).arg(meta->data.data2DFLMIP.positionY).arg(meta->data.data2DFLMIP.positionZ);
        
        return QString();
    }

    auto DataInfoTableWidget::Impl::GetFLChannelInfoText(const TC::IO::TCFMetaReader::Meta::Pointer& meta, const int& ch)->QString {
        if (nullptr == meta) return QString();
        if(0 > ch || ch > 2) return QString();

        if (meta->data.data3DFL.exist)    return QString("I %1 / E %2ms").arg(meta->data.data3DFL.intensity[ch]).arg(meta->data.data3DFL.exposureTime[ch]);
        if (meta->data.data2DFLMIP.exist) return QString("I %1 / E %2ms").arg(meta->data.data3DFL.intensity[ch]).arg(meta->data.data3DFL.exposureTime[ch]);

        return QString();
    }
}
