#include <QThread>

#include <TCFExporter.h>

#include "ExportWorker.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ExportWorker::Impl {
        Impl(Self* self) : self(self) {}
        Self* self{};
        QStringList pathList;
        QList<DataType> dataTypes;
        QString outputPath;
        ExportFormat format = ExportFormat::Raw;
        QString error;

        auto GetDataID(const DataType& types)->QString;
        auto Process(const QString& path, const DataType& type)->bool;
    };

    auto ExportWorker::Impl::GetDataID(const DataType& type)-> QString {
        QString id;

        switch(type) {
        case DataType::RITomogram:
            id = "3D";
            break;
        case DataType::RIMIP:
            id = "2DMIP";
            break;
        case DataType::FLTomogram:
            id = "3DFL";
            break;
        case DataType::FLMIP:
            id = "2DFLMIP";
            break;
        case DataType::BrightField:
            id = "BF";
            break;
        default: ;
        }

        return id;
    }

    auto ExportWorker::Impl::Process(const QString& path, const DataType& dataType)->bool {
        AppComponents::TCFExporter::Exporter exporter;

        error.clear();

        exporter.SetSource(path, GetDataID(dataType));
        exporter.SetTarget(outputPath, AppComponents::TCFExporter::Exporter::Extension::RAW);

        connect(&exporter, &AppComponents::TCFExporter::Exporter::sigTotalCount, self, [this, dataType](const int& totalCount) {
            emit self->sigTotalCount(dataType._to_integral(), totalCount);
        });

        connect(&exporter, &AppComponents::TCFExporter::Exporter::sigCurrentCount, self, [this, dataType](const int& currentCount) {
            emit self->sigCurrentCount(dataType._to_integral(), currentCount);
        });

        const auto result = exporter.Export();

        error = exporter.GetLastError();

        return result;
    }

    ExportWorker::ExportWorker() : QObject(), d{ std::make_unique<Impl>(this) } {
    }

    ExportWorker::~ExportWorker() {
    }

    auto ExportWorker::SetData(const QStringList& pathList, const QList<DataType>& dataTypes) -> void {
        d->pathList = pathList;
        d->dataTypes = dataTypes;
    }

    auto ExportWorker::SetOutput(const QString& outputPath, ExportFormat format) -> void {
        d->outputPath = outputPath;
        d->format = format;
    }

    void ExportWorker::onProcess() {
        for (const auto& path : d->pathList) {
            for (const auto& dataType : d->dataTypes) {
                if (d->Process(path, dataType)) {
                    emit sigSucceeded(path, dataType._to_integral());
                }
                else {
                    emit sigFailed(path, dataType._to_integral(), d->error);
                }
            }
            emit sigProcessed(path);
        }
        emit sigFinished();
    }
}