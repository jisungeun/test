#pragma once
#include <memory>

#include <QString>

#include "TCFMetaReader.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class DataInfoTableWidgetControl {
    public:
        DataInfoTableWidgetControl();
        ~DataInfoTableWidgetControl();

        auto GetMeta(const QString& path)->TC::IO::TCFMetaReader::Meta::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
