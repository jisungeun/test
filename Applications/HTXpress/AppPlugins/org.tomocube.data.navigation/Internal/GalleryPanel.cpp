#include "GalleryPanel.h"
#include "ui_GalleryPanel.h"
#include "GalleryPanelControl.h"

#include "ThumbnailWidget.h"
#include "CollapseWidget.h"

#include "CurrentItemObserver.h"
#include "DataObserver.h"
#include "ExperimentObserver.h"

#include <TCFDataRepo.h>
#include <Define.h>
#include <ppltasks.h>
#include <ThumbnailRepo.h>
#include <MessageDialog.h>

#include <QButtonGroup>
#include <QFileInfo>
#include <UIUtility.h>
#include <QProgressDialog>

#include "ThumbnailLayoutWidget.h"

using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;

namespace HTXpress::AppPlugins::Data::Navigation {
    struct GalleryPanel::Impl {
        explicit Impl(Self* self) : self(self) {}
        Self* self{};
        Ui::GalleryPanel ui;
        GalleryPanelControl control;

        DataObserver* dataObserver{ nullptr };
        CurrentItemObserver* itemObserver{ nullptr };
        ExperimentObserver* experimentObserver{ nullptr };

        AppEntity::Experiment::Pointer currentExperiment{ nullptr };

        QButtonGroup* buttonGroup{ nullptr };

        QString user;
        QString project;
        QString experiment;

        struct {
            QString user;
            QString project;
            QString experiment;
        } lastLoadedThumbnails;

        auto GetCheckedPaths() const->QStringList;
        auto Exist(const QStringList& files) -> bool;
        auto RefreshThumbnails(const TCFDataRepo::SpecimenList& specimens) -> void;
        auto ClearCurrentThumbnailInfo()->void;
    };

    auto GalleryPanel::Impl::GetCheckedPaths() const ->QStringList {
        if (nullptr == ui.scrollArea) return QStringList();

        QStringList list;
        for (const auto& thumbnail : ui.scrollArea->GetWidgets()) {
            if (nullptr == thumbnail) continue;
            if (false == thumbnail->property("check").toBool()) continue;

            const auto path = thumbnail->property("path").toString();
            if (false == path.isEmpty()) list << path;
        }

        return list;
    }

    auto GalleryPanel::Impl::Exist(const QStringList& files) -> bool {
        for (const auto& file : files) {
            if (!QFileInfo::exists(file)) {
                return false;
            }
        }
        return true;
    }

    auto GalleryPanel::Impl::RefreshThumbnails(const TCFDataRepo::SpecimenList& specimens) -> void {
        self->Clear();

        Qt::WindowFlags flags = { Qt::Dialog | Qt::Popup | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint };
        QProgressDialog progDlg("Loading data, please wait...", "", 0, 0, self, flags);
        progDlg.setWindowModality(Qt::WindowModal);
        progDlg.setModal(true);
        progDlg.setCancelButton(nullptr);
        progDlg.setStyleSheet("QProgressDialog QLabel {"
                              "color: #5b8b97; "
                              "font-size: 16px;"
                              "font-weight: bold;}"
                              "QProgressDialog {"
                              "background-color: #1b2629;"
                              "border: 2px solid #5f6f7a;"
                              "border-radius: 4px;}");

        int32_t count{};
        int32_t totalCount{};

        for (auto specIter = specimens.cbegin(); specIter != specimens.cend(); ++specIter) {
            if (specIter.key().isEmpty()) continue;
            auto wells = specIter.value();
            TCFDataRepo::WellList::iterator wellIter;
            for (wellIter = wells.begin(); wellIter != wells.end(); ++wellIter) {
                if (wellIter.key().isEmpty()) continue;
                auto datas = wellIter.value();
                totalCount += datas.size();
            }
        }
        progDlg.setMaximum(totalCount);
        auto contentWidget = new QWidget();
        contentWidget->setObjectName("panel");
        auto contentLayout = new QVBoxLayout;
        contentWidget->setLayout(contentLayout);
        ui.scrollArea->setWidget(contentWidget);

        for (auto specIter = specimens.begin(); specIter != specimens.end(); ++specIter) {
            if (specIter.key().isEmpty()) continue;

            auto specimenWidget = new CollapseWidget();
            specimenWidget->setObjectName(specIter.key());
            specimenWidget->SetTitle(specIter.key());
            specimenWidget->SetType(CollapseWidget::Type::Specimen);
            contentLayout->addWidget(specimenWidget);

            // well
            auto wells = specIter.value();
            TCFDataRepo::WellList::iterator wellIter;
            for (wellIter = wells.begin(); wellIter != wells.end(); ++wellIter) {
                if (wellIter.key().isEmpty()) continue;
                auto wellWidget = new CollapseWidget(specimenWidget);
                wellWidget->setObjectName(wellIter.key());
                wellWidget->SetTitle(wellIter.key());
                wellWidget->SetType(CollapseWidget::Type::Well);
                specimenWidget->AddWidget(wellWidget);

                // add data widget
                auto layoutWidget = new ThumbnailLayoutWidget(wellWidget);
                layoutWidget->setObjectName("layoutWidget");
                wellWidget->AddWidget(layoutWidget);

                auto datas = wellIter.value();
                TCFDataRepo::DataList::iterator dataIter;

                for (dataIter = datas.begin(); dataIter != datas.end(); ++dataIter) {
                    if (dataIter.key().isEmpty() || nullptr == dataIter.value()) continue;

                    progDlg.setValue(++count);
                    auto thumbnailWidget = new ThumbnailWidget;
                    thumbnailWidget->SetExperiment(currentExperiment);
                    thumbnailWidget->SetPath(dataIter.value()->GetPath());
                    layoutWidget->AppendWidget(thumbnailWidget);
                    ui.scrollArea->RegisterWidget(thumbnailWidget);
                }

                connect(wellWidget, &CollapseWidget::sigChangedCollapse, self, &Self::onChangedCollapse);
                connect(specimenWidget, &CollapseWidget::sigChangedCollapse, self, &Self::onChangedCollapse);
            }
        }

        if (false == specimens.isEmpty()) {
            contentLayout->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
        }

        emit self->sigRequestRenderIndexes();
    }

    auto GalleryPanel::Impl::ClearCurrentThumbnailInfo() -> void {
        lastLoadedThumbnails = {};
    }

    GalleryPanel::GalleryPanel(QWidget* parent) : QWidget(parent), d{ std::make_unique<Impl>(this) } {
        d->ui.setupUi(this);
        d->ui.baseWidget->setObjectName("panel-light");
        d->ui.line->setObjectName("line-divider");
        d->ui.scrollArea->setObjectName("panel");
        d->ui.scrollArea->widget()->setObjectName("panel");
        d->ui.exportButton->setObjectName("bt-highlight");
        d->ui.copyButton->setObjectName("bt-highlight");
        d->ui.deleteButton->setObjectName("bt-highlight");
        d->ui.viewButton->setObjectName("bt-highlight");

        d->ui.htButton->setObjectName("bt-light");
        d->ui.flButton->setObjectName("bt-light");
        d->ui.bfButton->setObjectName("bt-light");

        d->buttonGroup = new QButtonGroup;
        d->buttonGroup->addButton(d->ui.htButton, ImageType::HT);
        d->buttonGroup->addButton(d->ui.flButton, ImageType::FL);
        d->buttonGroup->addButton(d->ui.bfButton, ImageType::BF);

        d->itemObserver = new CurrentItemObserver;
        d->dataObserver = new DataObserver;
        d->experimentObserver = new ExperimentObserver;

        connect(d->itemObserver, &CurrentItemObserver::sigCurrentItemChanged, this, &GalleryPanel::onTreeItemSelectionChanged);
        connect(d->experimentObserver, &ExperimentObserver::sigExperimentLoaded, this, &GalleryPanel::onLoadedExperiment);

        connect(d->dataObserver, &DataObserver::sigDataAdded, this, &GalleryPanel::onDataAdded);
        connect(d->dataObserver, &DataObserver::sigDataUpdated, this, &GalleryPanel::onDataUpdated);
        connect(d->dataObserver, &DataObserver::sigDataRootFolderDeleted, this, &GalleryPanel::onDataRootFolderDeleted);

        connect(d->buttonGroup, &QButtonGroup::idClicked, this, &GalleryPanel::onButtonGroupClicked);
        connect(d->ui.selectButton, &QPushButton::clicked, this, &GalleryPanel::onSelectButtonClicked);
        connect(d->ui.deselectButton, &QPushButton::clicked, this, &GalleryPanel::onDeselectButtonClicked);
        connect(d->ui.viewButton, &QPushButton::clicked, this, &GalleryPanel::onViewButtonClicked);
        connect(d->ui.exportButton, &QPushButton::clicked, this, &GalleryPanel::onExportButtonClicked);
        connect(d->ui.copyButton, SIGNAL(clicked()), this, SLOT(onCopyButtonClicked()));
        connect(d->ui.deleteButton, &QPushButton::clicked, this, &GalleryPanel::onDeleteButtonClicked);

        connect(d->ui.scrollArea, &AsyncScrollArea::sigUpdateRender, this, &GalleryPanel::onScrollAreaRenderUpdated);

        connect(this, &GalleryPanel::sigRequestRenderIndexes, this, &GalleryPanel::onRenderIndexesRequested, Qt::QueuedConnection);

        TC::SilentCall(d->ui.htButton)->setChecked(true);
    }

    GalleryPanel::~GalleryPanel() {
    }

    auto GalleryPanel::RenderThumbnails()->void {
        if (nullptr == d->ui.scrollArea) return;

        const auto type = ImageType::_from_integral(d->buttonGroup->checkedId());

        auto indexes = d->ui.scrollArea->GetRenderIndexes();
        for (auto index : indexes) {
            if (0 > index) continue;

            const auto widget = d->ui.scrollArea->GetWidget(index);
            if (nullptr == widget) continue;

            const auto path = widget->property("path").toString();
            if (path.isEmpty()) continue;

            widget->setProperty("load", type._to_integral());
        }
    }

    auto GalleryPanel::LoadThumbnails(const QString& user, const QString& project, const QString& experiment) -> void {
        if (d->lastLoadedThumbnails.user == user && 
            d->lastLoadedThumbnails.project == project &&
            d->lastLoadedThumbnails.experiment == experiment) {
            return;
        }

        const auto specimens = TCFDataRepo::GetInstance()->GetSpecimenList(user, project, experiment);
        d->RefreshThumbnails(specimens);
        d->lastLoadedThumbnails = { user, project, experiment };
    }

    void GalleryPanel::onLoadedExperiment(const AppEntity::Experiment::Pointer& experiment) {
        d->currentExperiment = experiment;
    }

    void GalleryPanel::onTreeItemSelectionChanged(const QString& user, const QString& project, const QString& experiment, const QString& specimen, const QString& well) {
        if (user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;
        if (nullptr == d->currentExperiment) return;

        d->user = user;
        d->project = project;
        d->experiment = experiment;

        if (!specimen.isEmpty()) {
            CollapseWidget* specimenWidget = nullptr;
            for (const auto widget : d->ui.scrollArea->findChildren<CollapseWidget*>()) {
                if (widget->GetType() == CollapseWidget::Type::Specimen) {
                    widget->hide();
                    if (widget->GetTitle() == specimen) {
                        specimenWidget = widget;
                    }
                }
            }
            if (specimenWidget) {
                specimenWidget->show();
                if (!well.isEmpty()) {
                    const auto wellWidgets = specimenWidget->findChildren<CollapseWidget*>();
                    for (const auto wellWidget : wellWidgets) {
                        if (wellWidget->GetType() == CollapseWidget::Type::Well) {
                            if (wellWidget->GetTitle() == well) {
                                wellWidget->show();
                            } else {
                                wellWidget->hide();
                            }
                        }
                    }
                }
            }
        } else {
            for (const auto widget : d->ui.scrollArea->findChildren<CollapseWidget*>()) {
                widget->show();
                widget->ExpandAll();
            }
        }
    }

    void GalleryPanel::onRenderIndexesRequested() {
        RenderThumbnails();
    }

    void GalleryPanel::onDataAdded(const QString& fileFullPath) {
        if (fileFullPath.isEmpty()) return;
        if (nullptr == d->ui.scrollArea->widget()) return;

        const auto user = d->control.ParseUserName(fileFullPath);
        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);

        if (user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;
        if (specimen.isEmpty()) return;
        if (well.isEmpty()) return;

        if (user != d->user || project != d->project || experiment != d->experiment) return;

        if (nullptr == d->currentExperiment) return;
        
        const auto imageData = TCFDataRepo::GetInstance()->GetData(user, project, experiment, specimen, well, dataName);
        if (nullptr == imageData) return;

        auto specimenWidget = d->ui.scrollArea->widget()->findChild<CollapseWidget*>(specimen);
        if (nullptr == specimenWidget) {
            specimenWidget = new CollapseWidget(d->ui.scrollArea->widget());
            specimenWidget->setObjectName(specimen);
            specimenWidget->SetTitle(specimen);
            specimenWidget->SetType(CollapseWidget::Type::Specimen);
            d->ui.scrollArea->widget()->layout()->addWidget(specimenWidget);

            connect(specimenWidget, &CollapseWidget::sigChangedCollapse, this, &GalleryPanel::onChangedCollapse);
        }

        auto wellWidget = specimenWidget->findChild<CollapseWidget*>(well);
        if (nullptr == wellWidget) {
            wellWidget = new CollapseWidget(specimenWidget);
            wellWidget->setObjectName(well);
            wellWidget->SetTitle(well);
            wellWidget->SetType(CollapseWidget::Type::Well);
            specimenWidget->AddWidget(wellWidget);

            connect(wellWidget, &CollapseWidget::sigChangedCollapse, this, &GalleryPanel::onChangedCollapse);
        }

        auto layoutWidget = wellWidget->findChild<ThumbnailLayoutWidget*>("layoutWidget");
        if (nullptr == layoutWidget) {
            layoutWidget = new ThumbnailLayoutWidget(wellWidget);
            layoutWidget->setObjectName("layoutWidget");
            wellWidget->AddWidget(layoutWidget);
        }

        auto thumbnailWidget = new ThumbnailWidget;
        thumbnailWidget->SetExperiment(d->currentExperiment);
        thumbnailWidget->SetPath(fileFullPath);
        layoutWidget->AppendWidget(thumbnailWidget);
        d->ui.scrollArea->RegisterWidget(thumbnailWidget);

        if (1 == d->ui.scrollArea->GetWidgets().size()) {
            auto layout = d->ui.scrollArea->widget()->layout();
            static_cast<QVBoxLayout*>(layout)->addSpacerItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));
        }

        // Update find widget
        thumbnailWidget->setProperty("load", d->buttonGroup->checkedId());
    }

    void GalleryPanel::onDataUpdated(const QString& fileFullPath) {
        if (fileFullPath.isEmpty()) return;

        const auto user = d->control.ParseUserName(fileFullPath);
        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);

        if (user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;
        if (specimen.isEmpty()) return;
        if (well.isEmpty()) return;

        if (user != d->user || project != d->project || experiment != d->experiment) return;

        if (nullptr == d->ui.scrollArea) return;
        if (nullptr == d->ui.scrollArea->widget()) return;
        if (nullptr == d->ui.scrollArea->widget()->layout()) return;

        const auto imageData = TCFDataRepo::GetInstance()->GetData(user, project, experiment, specimen, well, dataName);
        if (nullptr == imageData) return;

        const auto path = imageData->GetPath();
        if (path.isEmpty()) return;

        auto specimenWidget = d->ui.scrollArea->widget()->findChild<CollapseWidget*>(specimen);
        if (nullptr == specimenWidget) {
            specimenWidget = new CollapseWidget(d->ui.scrollArea->widget());
            specimenWidget->setObjectName(specimen);
            specimenWidget->SetTitle(specimen);
            specimenWidget->SetType(CollapseWidget::Type::Specimen);
            d->ui.scrollArea->widget()->layout()->addWidget(specimenWidget);

            connect(specimenWidget, &CollapseWidget::sigChangedCollapse, this, &GalleryPanel::onChangedCollapse);
        }

        auto wellWidget = specimenWidget->findChild<CollapseWidget*>(well);
        if (nullptr == wellWidget) {
            wellWidget = new CollapseWidget(specimenWidget);
            wellWidget->setObjectName(well);
            wellWidget->SetTitle(well);
            wellWidget->SetType(CollapseWidget::Type::Well);
            specimenWidget->AddWidget(wellWidget);

            connect(wellWidget, &CollapseWidget::sigChangedCollapse, this, &GalleryPanel::onChangedCollapse);
        }

        auto layoutWidget = wellWidget->findChild<ThumbnailLayoutWidget*>("layoutWidget");
        if (nullptr == layoutWidget) {
            layoutWidget = new ThumbnailLayoutWidget(wellWidget);
            wellWidget->setObjectName("layoutWidget");
            wellWidget->AddWidget(layoutWidget);
        }

        QWidget* find{ nullptr };
        for (const auto& thumbnail : d->ui.scrollArea->GetWidgets()) {
            if (nullptr == thumbnail) continue;
            if (thumbnail->property("path") == path) {
                find = thumbnail;
                break;
            }
        }

        if (nullptr == find) return;
        if (false == d->ui.scrollArea->IsVisible(find)) return;

        // Update find widget
        find->setProperty("load", d->buttonGroup->checkedId());
    }

    void GalleryPanel::onDataRootFolderDeleted(const QString& fileFullPath) {
        if (fileFullPath.isEmpty()) return;

        const auto user = d->control.ParseUserName(fileFullPath);
        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);

        if (user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;
        if (specimen.isEmpty()) return;
        if (well.isEmpty()) return;
        if (user != d->user || project != d->project || experiment != d->experiment) return;

        if (nullptr == d->ui.scrollArea->widget()) return;

        const auto imageData = TCFDataRepo::GetInstance()->GetData(user, project, experiment, specimen, well, dataName);
        if (nullptr == imageData) return;

        const auto specimenWidget = d->ui.scrollArea->widget()->findChild<CollapseWidget*>(specimen);
        if (nullptr == specimenWidget) {
            return;
        }

        const auto wellWidget = specimenWidget->findChild<CollapseWidget*>(well);
        if (nullptr == wellWidget) {
            return;
        }

        const auto groupWidget = wellWidget->findChild<ThumbnailLayoutWidget*>("layoutWidget");
        if (nullptr == groupWidget) {
            return;
        }

        QWidget* find{ nullptr };
        for (const auto& thumbnail : d->ui.scrollArea->GetWidgets()) {
            if (nullptr == thumbnail) continue;
            if (thumbnail->property("path") == fileFullPath) {
                find = thumbnail;
                break;
            }
        }

        if (nullptr == find) return;

        d->ui.scrollArea->DergisterWidget(find);
        groupWidget->RemoveWidget(dynamic_cast<ThumbnailWidget*>(find));
        find->hide();

        delete find;
        find = nullptr;
    }

    void GalleryPanel::onScrollAreaRenderUpdated(QList<int> indexes) {
        Q_UNUSED(indexes)
        onRenderIndexesRequested();
    }

    void GalleryPanel::onChangedCollapse() {
        update();
        onRenderIndexesRequested();
    }

    void GalleryPanel::onSelectButtonClicked() {
        if (nullptr == d->ui.scrollArea) return;

        for (auto thumbnail : d->ui.scrollArea->GetWidgets()) {
            if (thumbnail) {
                if (thumbnail->isVisible()) {
                    thumbnail->setProperty("check", true);
                } else {
                    thumbnail->setProperty("check", false);
                }
            }
        }
    }

    void GalleryPanel::onDeselectButtonClicked() {
        if (nullptr == d->ui.scrollArea) return;

        for (auto thumbnail : d->ui.scrollArea->GetWidgets()) {
            if (thumbnail) thumbnail->setProperty("check", false);
        }
    }

    void GalleryPanel::onButtonGroupClicked(int id) {
        Q_UNUSED(id)
        onRenderIndexesRequested();
    }

    void GalleryPanel::onViewButtonClicked() {
        if (nullptr == d->ui.scrollArea) return;

        auto list = d->GetCheckedPaths();
        if (list.isEmpty()) return;
        if (!d->Exist(list)) {
            TC::MessageDialog::warning(this, tr("Unable to launch"), tr("Data processing is not completed.\nPlease check the processing of the selected data is complete."));
            return;
        }
        emit sigViewImage(list);
    }

    void GalleryPanel::onExportButtonClicked() {
        if (nullptr == d->ui.scrollArea) return;

        auto list = d->GetCheckedPaths();
        if (list.isEmpty()) return;
        if (!d->Exist(list)) {
            TC::MessageDialog::warning(this, tr("Export failed"), tr("Data processing is not completed.\nPlease check the processing of the selected data is complete."));
            return;
        }
        emit sigExport(list);
    }

    void GalleryPanel::onCopyButtonClicked() {
        if (nullptr == d->ui.scrollArea) return;

        auto list = d->GetCheckedPaths();
        if (list.isEmpty()) return;
        if (!d->Exist(list)) {
            TC::MessageDialog::warning(this, tr("Copy failed"), tr("Data processing is not completed.\nPlease check the processing of the selected data is complete."));
            return;
        }
        emit sigCopy(list);
    }

    void GalleryPanel::onDeleteButtonClicked() {
        if (nullptr == d->ui.scrollArea) return;

        auto list = d->GetCheckedPaths();
        if (list.isEmpty()) return;

        emit sigDelete(list);
    }

    auto GalleryPanel::Clear()->void {
        if (nullptr != d->ui.scrollArea->widget()) {
            for (auto child : d->ui.scrollArea->widget()->children()) {
                delete child;
                child = nullptr;
            }
        }

        delete d->ui.scrollArea->widget();

        d->ui.scrollArea->DergisterAllWidgets();
        Entity::ThumbnailRepo::GetInstance()->Clear();
        d->ClearCurrentThumbnailInfo();
    }
}
