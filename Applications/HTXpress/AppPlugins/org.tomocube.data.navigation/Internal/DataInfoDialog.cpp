#include <QDirIterator>
#include <QFileInfo>

#include <TCFData.h>

#include "DataInfoDialog.h"
#include "DataInfoDialogControl.h"
#include "ui_DataInfoDialog.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct DataInfoDialog::Impl {
        Ui::DataInfoDialog ui;
        DataInfoDialogControl control;
    };

    DataInfoDialog::DataInfoDialog(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);
        d->ui.imageWidget->SetTypeVisible(false);
        d->ui.infoWidget->SetVisibleWidgetTitle(false);
        d->ui.infoWidget->SetHeaderWidth(104);
        d->ui.vesselMapPanel->SetMode(VesselMapPanel::Mode::Info);
        d->ui.vesselMapPanel->SetViewMode(TC::ViewMode::InfoMode);
    }

    DataInfoDialog::~DataInfoDialog() {
        qDebug() << "DataInfoDialog destroyed...";
    }

    auto DataInfoDialog::SetData(const AppEntity::Experiment::Pointer& experiment, const QString& path)->bool {
        if (nullptr == experiment) return false;
        if (path.isEmpty()) return false;

        d->ui.vesselMapPanel->SetExperiment(experiment);

        d->ui.imageWidget->SetPath(path);
        d->ui.infoWidget->SetPath(path);

        const auto imagingDataFolderPath = QFileInfo(path).absoluteDir().absolutePath();

        d->ui.vesselMapPanel->SetCurrentLocationPoint(imagingDataFolderPath);
        d->ui.vesselMapPanel->FitWellCanvas();

        return true;
    }
}
