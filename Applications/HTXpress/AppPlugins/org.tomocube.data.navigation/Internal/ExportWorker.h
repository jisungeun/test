#pragma once
#include <memory>
#include <QObject>
#include <enum.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    BETTER_ENUM(DataType, uint8_t,
                RITomogram,
                RIMIP,
                FLTomogram,
                FLMIP,
                BrightField)

    BETTER_ENUM(ExportFormat, uint8_t,
                Raw,
                Tiff)

    class ExportWorker : public QObject {
        Q_OBJECT

    public:
        using Self = ExportWorker;

        ExportWorker();
        ~ExportWorker() override;

        auto SetData(const QStringList& pathList, const QList<DataType>& types)->void;
        auto SetOutput(const QString& outputPath, ExportFormat format)->void;

    public slots:
        void onProcess();

    signals:
        void sigProcessed(const QString& path);
        void sigSucceeded(const QString& path, const int& type);
        void sigFailed(const QString& path, const int& type, const QString& error);
        void sigFinished();

        void sigTotalCount(const int& type, const int& count);
        void sigCurrentCount(const int& type, const int& count);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}