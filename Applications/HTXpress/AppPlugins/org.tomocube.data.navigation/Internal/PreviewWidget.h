#pragma once

#include <memory>
#include <enum.h>
#include <QWidget>
#include <QEvent>

namespace HTXpress::AppPlugins::Data::Navigation {
    class PreviewWidget : public QWidget {
        Q_OBJECT

    public:
        PreviewWidget(QWidget* parent = nullptr);
        ~PreviewWidget();

        auto SetPath(const QString& path) ->void;
        auto SetTypeVisible(const bool& visible) const ->void;
        
    protected:
        auto eventFilter(QObject* object, QEvent* event)->bool override;

    private slots:
        void onButtonGroupClicked(int id);
   
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}