#include "DataInfoDialogControl.h"

#include <ConfigController.h>
#include <ConfigPresenter.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    struct DataInfoDialogControl::Impl {
    };

    DataInfoDialogControl::DataInfoDialogControl() : d{new Impl} {
    }

    DataInfoDialogControl::~DataInfoDialogControl() {
    }

    auto DataInfoDialogControl::GetMeta(const QString& path)->TC::IO::TCFMetaReader::Meta::Pointer {
        if (path.isEmpty()) return nullptr;

        auto metaReader = new TC::IO::TCFMetaReader;
        return metaReader->Read(path);
    }

    auto DataInfoDialogControl::LoadAcqInfo(const QString& path, int& wellIndex, AppEntity::Position& position)->void {
        if (path.isEmpty()) return;

        auto presenter = new Interactor::ConfigPresenter();
        auto controller = Interactor::ConfigController(presenter);

        if (false == controller.LoadAcuInfo(path)) {
            // error
        }

        presenter->GetAcqInfo(wellIndex, position);
    }
}
