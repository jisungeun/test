#include <QMutex>
#include <QFileInfo>

#include "FileUtility.h"
#include "CopyDialogControl.h"

#include <QDir>

namespace HTXpress::AppPlugins::Data::Navigation {
    struct CopyDialogControl::Impl {
        QList<QString> sourceList;
        Option option{ Option::TCF_ONLY };
        QString targetPath;

        QMutex mutex;
        bool running{ false };

        auto CopyTCFOnly(const QString& source)->bool;
        auto CopyTCFWithDirectory(const QString& source)->bool;
        auto CopyAll(const QString& source, Impl* p)->bool;
    };

    auto CopyDialogControl::Impl::CopyTCFOnly(const QString& source) -> bool {
        QFileInfo finfo(source);
        return QFile::copy(source, QString("%1/%2").arg(targetPath).arg(finfo.fileName()));
    }

    auto CopyDialogControl::Impl::CopyTCFWithDirectory(const QString& source) -> bool {
        QFileInfo finfo(source);
        const auto prefix = [finfo]()->QString {
            const auto dirPath = finfo.absolutePath().replace('\\', '/');
            const auto toks = dirPath.split('/');
            if(toks.length() < 2) return "";

            const auto nameLen = std::min(3, toks.length() - 1);
            return toks.mid(toks.length() - nameLen).join('/');
        }();

        const auto newTargetPath = QString("%1/%2").arg(targetPath).arg(prefix);
        if(!QDir().mkpath(newTargetPath)) return false;

        return QFile::copy(source, QString("%1/%2").arg(newTargetPath).arg(finfo.fileName()));
    }

    auto CopyDialogControl::Impl::CopyAll(const QString& source, Impl* p) -> bool {
        QFileInfo finfo(source);
        const auto prefix = [finfo]()->QString {
            const auto dirPath = finfo.absolutePath().replace('\\', '/');
            const auto toks = dirPath.split('/');
            if(toks.length() < 2) return "";

            const auto nameLen = std::min(3, toks.length() - 1);
            return toks.mid(toks.length() - nameLen).join('/');
        }();

        const auto newTargetPath = QString("%1/%2").arg(targetPath).arg(prefix);
        if(!QDir().mkpath(newTargetPath)) return false;

        return TC::CopyFilesRecursive(finfo.absolutePath(), newTargetPath, [&p]()->bool {
            QMutexLocker locker(&p->mutex);
            return !p->running;
        });
    }

    CopyDialogControl::CopyDialogControl() : QThread(), d{ std::make_unique<Impl>() } {
    }

    CopyDialogControl::~CopyDialogControl() {
    }

    auto CopyDialogControl::SetList(const QList<QString>& list) -> void {
        d->sourceList = list;
    }

    auto CopyDialogControl::Start(Option option, const QString& target) -> void {
        d->option = option;
        d->targetPath = target;

        QMutexLocker locker(&d->mutex);
        d->running = true;
        start();
    }

    auto CopyDialogControl::Stop() -> void {
        d->mutex.lock();
        d->running = false;
        d->mutex.unlock();

        wait();
    }

    void CopyDialogControl::run() {
        for(const auto source : d->sourceList) {
            switch(d->option) {
            case Option::TCF_ONLY:
                d->CopyTCFOnly(source);
                break;
            case Option::TCF_ONLY_KEEP_HIERARCHY:
                d->CopyTCFWithDirectory(source);
                break;
            case Option::ALL_FILES:
                d->CopyAll(source, d.get());
                break;
            }

            QMutexLocker locker(&d->mutex);
            if(!d->running) break;
            emit sigCopied();
        }

        emit sigCompleted();
    }
}
