#include "ProjectUpdater.h"
#include "ProjectObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ProjectUpdater::Impl {
        QList<ProjectObserver*> observers;
    };

    ProjectUpdater::ProjectUpdater() : d{ new Impl } {
    }

    ProjectUpdater::~ProjectUpdater() {
    }

    auto ProjectUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new ProjectUpdater() };
        return theInstance;
    }

    auto ProjectUpdater::UpdateProjectList(const QList<QString>& projects) ->void {
        for (auto observer : d->observers) {
            observer->UpdateProjectList(projects);
        }
    }

    auto ProjectUpdater::ChangeCurrentProject(const QString& projectTitle)->void {
        for (auto observer : d->observers) {
            observer->ChangeCurrentProject(projectTitle);
        }
    }

    auto ProjectUpdater::Register(ProjectObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto ProjectUpdater::Deregister(ProjectObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}