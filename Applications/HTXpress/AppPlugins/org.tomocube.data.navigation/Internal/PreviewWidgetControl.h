#pragma once
#include <memory>
#include <enum.h>

#include <QString>
#include <QImage>

namespace HTXpress::AppPlugins::Data::Navigation {
    class PreviewWidgetControl {
    public:
        PreviewWidgetControl();
        ~PreviewWidgetControl();

        auto Exist(const QString& path, bool& ht, bool& fl, bool& bf)->void;

        auto LoadHTThumbnail(const QString& path, QImage& image)->bool;
        auto LoadFLThumbnail(const QString& path, QImage& image)->bool;
        auto LoadBFThumbnail(const QString& path, QImage& image)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}