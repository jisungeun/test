#define LOGGER_TAG "[ThumbnailWidget]"
#include <TCLogger.h>

#include <QGraphicsOpacityEffect>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QFrame>
#include <QToolTip>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QtConcurrent/QtConcurrent>

#include <TCFData.h>
#include <DataMisc.h>

#include "ThumbnailWidget.h"
#include "ThumbnailWidgetControl.h"
#include "DataInfoDialog.h"
#include "HTXDialog.h"
#include "ExperimentLoader.h"

using HTXpress::AppComponents::ProcessedDataDefinition::TCFData;
using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ImageView::Impl {
        QGraphicsPixmapItem* item{ nullptr };
    };

    ImageView::ImageView(QWidget* parent) : QGraphicsView(parent), d{ new Impl } {
        d->item = new QGraphicsPixmapItem;
        this->setScene(new QGraphicsScene(this));
        this->scene()->addItem(d->item);
        this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        this->setContentsMargins(0, 0, 0, 0);
    }

    ImageView::~ImageView() {
       
    }

    auto ImageView::SetPixmap(const QPixmap& pixmap)->void {
        if (nullptr == this->scene()) return;
        if (nullptr == d->item) return;

        this->scene()->setSceneRect(0, 0, pixmap.width(), pixmap.height());
        d->item->setPixmap(pixmap);
        KeepSquare();
    }

    auto ImageView::KeepSquare()->void {
        if (nullptr == this->scene()) return;
        this->fitInView(this->scene()->sceneRect(), Qt::KeepAspectRatio);
    }

    ////////////////////////////////////////////////////////////////////////////////////
    ///
    struct ThumbnailWidget::Impl {
        ThumbnailWidgetControl control;

        ImageView* view{ nullptr };
        QCheckBox* checkbox{ nullptr };
        QFrame* selectFrame{ nullptr };

        QString path;
        AppEntity::Experiment::Pointer experiment{ nullptr };

        int width = 1024;
        int height = 1024;

        QFuture<std::tuple<int, QImage, bool>> future;
        QFutureWatcher<void> watcher;

        struct {
            bool visible{ false };
            QString text;
            QTimer timer;
            bool displayed{ false };
        } tooltip;

        struct {
            QImage ht;
            QImage fl;
            QImage bf;
        } loaded;

        auto Load(const ImageType& type)->std::tuple<int, QImage, bool>;
    };

    auto ThumbnailWidget::Impl::Load(const ImageType& type)-> std::tuple<int, QImage, bool> {
        if (path.isEmpty()) return std::make_tuple(type._to_integral(), QImage(), false);

        bool cache = false;
        QImage image;
        switch (type) {
        case ImageType::HT: control.LoadHTThumbnail(path, image, cache); break;
        case ImageType::FL: control.LoadFLThumbnail(path, image, cache); break;
        case ImageType::BF: control.LoadBFThumbnail(path, image, cache); break;
        }

        return std::make_tuple(type._to_integral(), image, cache);
    }

    ThumbnailWidget::ThumbnailWidget(QWidget* parent): QWidget(parent), d{ new Impl } {
        this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        d->view = new ImageView;
        
        d->checkbox = new QCheckBox;
        d->checkbox->setObjectName("ckb-tsx-thumbnail");

        d->selectFrame = new QFrame;
        d->selectFrame->setObjectName("frame");
        d->selectFrame->setStyleSheet("#frame { border: none; background: #000000; }");
               
        auto checkLayout = new QGridLayout(d->view);
        checkLayout->setContentsMargins(6, 6, 0, 0);
        checkLayout->addWidget(d->checkbox, 0, 0, 0, 0, Qt::AlignTop | Qt::AlignLeft);
        d->view->setLayout(checkLayout);

        auto viewLayout = new QVBoxLayout(d->selectFrame);
        viewLayout->setContentsMargins(0, 0, 0, 0);
        viewLayout->addWidget(d->view);

        auto mainLayout = new QVBoxLayout();
        mainLayout->addWidget(d->selectFrame);
        mainLayout->setMargin(0);
        mainLayout->setContentsMargins(0, 0, 20, 20);
        this->setLayout(mainLayout);

        connect(d->checkbox, SIGNAL(stateChanged(int)), this, SLOT(onCheckBoxStateChanged(int)));
        connect(&d->tooltip.timer, SIGNAL(timeout()), this, SLOT(onHideTooltip()));

        connect(&d->watcher, SIGNAL(finished()), this, SLOT(onLoadThumbnailDone()));

        this->installEventFilter(this);
    }

    ThumbnailWidget::~ThumbnailWidget() {
        if (d->future.isRunning()) {
            d->future.cancel();
        }
    }
    
    auto ThumbnailWidget::SetExperiment(const AppEntity::Experiment::Pointer& experiment)->void {
        d->experiment = experiment;
    }

    auto ThumbnailWidget::SetPath(const QString& path) const ->void {
        d->path = path;
        d->tooltip.text = DataMisc::GetTcfBaseName(path);
        if (d->checkbox) d->checkbox->setText(DataMisc::GetTcfTypeInfo(path));
    }

    auto ThumbnailWidget::GetPath() const ->QString {
        return d->path;
    }

    auto ThumbnailWidget::SetCheck(const bool& check)->void {
        if (nullptr == d->selectFrame) return;
        if (nullptr == d->checkbox) return;

        d->checkbox->setChecked(check);

        if(GetChecked()) {
            d->selectFrame->setStyleSheet("#frame { border: 2px solid #00c2d3;  background: #000000; } ");
            emit sigChecked();
        }
        else {
            d->selectFrame->setStyleSheet("#frame { border: none; background: #000000; }");
        }
    }

    auto ThumbnailWidget::GetChecked() const ->bool {
        if (nullptr == d->checkbox) return false;
        return d->checkbox->isChecked();
    }

    auto ThumbnailWidget::SetCheckable(const bool& able) const ->void {
        if (nullptr == d->checkbox) return;
        d->checkbox->setVisible(able);
    }

    auto ThumbnailWidget::SetVisibleTooltip(const bool& visible)->void {
        if (visible) setToolTip(d->tooltip.text);
        else setToolTip("");
    }

    auto ThumbnailWidget::SetImage(const QImage& image)->void {
        if (nullptr == d->view) return;
        d->view->SetPixmap(QPixmap::fromImage(image));
        d->view->KeepSquare();
    }

    auto ThumbnailWidget::SetImageSize(const int& w, const int& h) const ->void {
        d->width = w;
        d->height = h;
    }

    auto ThumbnailWidget::LoadThumbnail(const ImageType& type)->bool {
        if (d->path.isEmpty()) return false;

        if (d->future.isRunning()) {
            d->future.cancel();
        }

        switch (type) {
        case ImageType::HT: {
            if (!d->loaded.ht.isNull()) {
                SetImage(d->loaded.ht);
                return true;
            }
            break;
        }
        case ImageType::FL: {
            if (!d->loaded.fl.isNull()) {
                SetImage(d->loaded.fl);
                return true;
            }
            break;
        }
        case ImageType::BF: {
            if (!d->loaded.bf.isNull()) {
                SetImage(d->loaded.bf);
                return true;
            }
            break;
        }
        default:break;
        }

        d->future = QtConcurrent::run([=] { return d->Load(type); });
        d->watcher.setFuture(d->future);

        return true;
    }

    auto ThumbnailWidget::KeepSquare()->void {
        setFixedHeight(width());
        if (d->view) d->view->KeepSquare();
    }

    auto ThumbnailWidget::eventFilter(QObject* object, QEvent* event) -> bool {
        Q_UNUSED(object)

        if (event->type() == QEvent::MouseButtonPress) {
            const auto mouseEvent = static_cast<QMouseEvent*>(event);
            if (mouseEvent->button() == Qt::LeftButton) {
                SetCheck(!GetChecked());
                return true;
            }
        }
        else if(event->type() == QEvent::MouseButtonDblClick) {
            const auto mouseEvent = static_cast<QMouseEvent*>(event);
            if (mouseEvent->button() == Qt::LeftButton) {
                emit sigDoubleClicked();

                if (nullptr == d->experiment) return true;
                if (d->path.isEmpty()) return true;

                const auto infoWidget = new DataInfoDialog();
                ExperimentLoader experimentLoader(d->path);
                const auto experiment = experimentLoader.GetExperimentInDataFolder();
                if(experiment.has_value()) {
                    infoWidget->SetData(experiment.value(), d->path);
                }
                else {
                    QLOG_INFO() << "Failed to load Vessel at the time of data acquisition.";
                    infoWidget->SetData(d->experiment, d->path);
                }

                HTXDialog dialog;
                dialog.SetTitle("File Information");
                dialog.SetWidget(infoWidget);
                dialog.exec();

                return true;
            }
        }
        else if (event->type() == QEvent::ToolTip) {
            if(!d->tooltip.displayed) {
                QToolTip::showText(this->mapToGlobal( QPoint( 0, 0 )), d->tooltip.text);
                d->tooltip.timer.start(4000);
                d->tooltip.displayed = true;
            }
            return true;
        }
        else if (event->type() == QEvent::Leave) {
            d->tooltip.timer.stop();
            d->tooltip.displayed = false;
            return true;
        }

        return QWidget::eventFilter(object, event);
    }

    void ThumbnailWidget::onCheckBoxStateChanged(int state) {
        Q_UNUSED(state)
        SetCheck(GetChecked());
    }

    void ThumbnailWidget::onHideTooltip() {
        QToolTip::hideText();
    }

    void ThumbnailWidget::onLoadThumbnailDone() {
        if (d->future.isCanceled()) return;

        auto ret = d->future.result();
        auto type = ImageType::_from_integral(std::get<0>(ret));
        auto image = std::get<1>(ret);
        auto cache = std::get<2>(ret);

        image = image.scaled(d->width, d->height, Qt::KeepAspectRatio);
        SetImage(image);

        if(cache) {
            switch (type) {
            case ImageType::HT: d->loaded.ht = image; break;
            case ImageType::FL: d->loaded.fl = image; break;
            case ImageType::BF: d->loaded.bf = image; break;
            default:break;
            }
        }
    }
    
    auto ThumbnailWidget::checked(void) const ->bool {
        if (nullptr == d->checkbox) return false;
        return d->checkbox->isChecked();
    }

    auto ThumbnailWidget::setCheck(const bool& check)->void {
        d->checkbox->setChecked(check);
    }

    auto ThumbnailWidget::getPath()->QString {
        return d->path;
    }

    auto ThumbnailWidget::setPath(const QString& path)->void {
        d->path = path;
    }

    auto ThumbnailWidget::getImage()->QImage {
        return QImage();
    }

    auto ThumbnailWidget::setImage(const QImage& image)->void {
        SetImage(image);
    }

    auto ThumbnailWidget::load(const int& type)->void {
        LoadThumbnail(ImageType::_from_index(type));
    }

    auto ThumbnailWidget::loaded()->int {
        return 0;
    }
}
