#include "CurrentItemObserver.h"
#include "CurrentItemUpdater.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    CurrentItemObserver::CurrentItemObserver(QObject* parent) : QObject(parent) {
        CurrentItemUpdater::GetInstance()->Register(this);
    }

    CurrentItemObserver::~CurrentItemObserver() {
        CurrentItemUpdater::GetInstance()->Deregister(this);
    }

    auto CurrentItemObserver::ChangeCurrentItem(const QString& user, const QString& project, const QString& experiment, const QString& specimen, const QString& well) -> void {
        emit sigCurrentItemChanged(user, project, experiment, specimen, well);
    }
}
