#pragma once

#include <memory>

#include <QString>
#include <QColor>

#include <Experiment.h>
#include <AppEntityDefines.h>
#include <Position.h>
#include <VesselMap.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    class VesselMapPanelControl {
    public:
        VesselMapPanelControl();
        ~VesselMapPanelControl();

        auto GenerateVesselMap(const AppEntity::VesselIndex& vesselIndex, const AppEntity::Experiment::Pointer& experiment)->TC::VesselMap::Pointer;
        auto LoadAcqInfo(const QString& path, int& wellIndex, AppEntity::Position& position) -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
