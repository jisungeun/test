#include "GalleryPanelControl.h"

#include <ThumbnailController.h>
#include <ThumbnailManager.h>
#include <DataMisc.h>

using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Data::Navigation {
    struct GalleryPanelControl::Impl {
    };

    GalleryPanelControl::GalleryPanelControl() : d{new Impl} {
    }

    GalleryPanelControl::~GalleryPanelControl() {
    }

    auto GalleryPanelControl::ParseUserName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetUserName(fileFullPath);
    }

    auto GalleryPanelControl::ParseProjectName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetProjectName(fileFullPath);
    }

    auto GalleryPanelControl::ParseExperimentName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetExperimentName(fileFullPath);
    }

    auto GalleryPanelControl::ParseSpecimenName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetSpecimenName(fileFullPath);
    }

    auto GalleryPanelControl::ParseWellName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetWellName(fileFullPath);
    }

    auto GalleryPanelControl::ParseDataName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetTcfBaseName(fileFullPath);
    }
}
