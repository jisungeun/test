#include "DataObserver.h"
#include "DataUpdater.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    DataObserver::DataObserver(QObject* parent) : QObject(parent) {
        DataUpdater::GetInstance()->Register(this);
    }

    DataObserver::~DataObserver() {
        DataUpdater::GetInstance()->Deregister(this);
    }

    auto DataObserver::ScannedExperimentData(const QString& user, const QString& project, const QString& experiment) -> void {
        emit sigExperimentDataScanned(user, project, experiment);
    }

    auto DataObserver::AddData(const QString& fileFullPath) -> void {
        emit sigDataAdded(fileFullPath);
    }

    auto DataObserver::UpdateData(const QString& fileFullPath) -> void {
        emit sigDataUpdated(fileFullPath);
    }

    auto DataObserver::DeleteData(const QString& fileFullPath) -> void {
        emit sigDataDeleted(fileFullPath);
    }

    auto DataObserver::DeleteDataRootFolder(const QString& fileFullPath) -> void {
        emit sigDataRootFolderDeleted(fileFullPath);
    }
}
