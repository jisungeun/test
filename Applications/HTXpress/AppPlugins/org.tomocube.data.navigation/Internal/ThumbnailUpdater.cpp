#include "ThumbnailUpdater.h"
#include "ThumbnailObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ThumbnailUpdater::Impl {
        QList<ThumbnailObserver*> observers;
    };

    ThumbnailUpdater::ThumbnailUpdater() : d{ new Impl } {
    }

    ThumbnailUpdater::~ThumbnailUpdater() {
    }

    auto ThumbnailUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new ThumbnailUpdater() };
        return theInstance;
    }

    auto ThumbnailUpdater::LoadedThumbnail(const QString& path, const QImage& image)->void {
        for (auto observer : d->observers) {
            observer->LoadedThumbnail(path, image);
        }
    }
    
    auto ThumbnailUpdater::Register(ThumbnailObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto ThumbnailUpdater::Deregister(ThumbnailObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}