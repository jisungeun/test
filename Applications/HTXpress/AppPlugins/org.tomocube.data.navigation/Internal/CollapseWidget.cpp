#include "ui_CollapseWidget.h"
#include "CollapseWidget.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct CollapseWidget::Impl {
        Ui::CollapseWidget ui;

        bool collapsed{ false };
        Type widgetType{};
    };

    CollapseWidget::CollapseWidget(QWidget* parent): QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);
        this->setObjectName("panel");

        d->ui.titleButton->setChecked(true);
        d->ui.arrowButton->setChecked(true);
        d->ui.arrowButton->setObjectName("bt-arrow");
        d->ui.contentsFrame->setContentsMargins(0, 0, 0, 0);
        d->ui.contentsFrame->layout()->setMargin(0);

        connect(d->ui.titleButton, SIGNAL(clicked(bool)), this, SLOT(onExpanded(bool)));
        connect(d->ui.arrowButton, SIGNAL(clicked(bool)), this, SLOT(onExpanded(bool)));
    }    

    CollapseWidget::~CollapseWidget() {
    }

    auto CollapseWidget::SetTitle(const QString& title)->void {
        d->ui.titleButton->setText(title);
    }

    auto CollapseWidget::GetTitle() const -> QString {
        return d->ui.titleButton->text();
    }

    auto CollapseWidget::SetType(const Type& widgetType) -> void {
        d->widgetType = widgetType;

        if (d->widgetType == Type::Specimen) {
            d->ui.titleButton->setIcon(QIcon(":/img/tsx/specimen"));
            d->ui.titleButton->setObjectName("bt-tsx-thumbnail-specimen");
            d->ui.lineLabel->setStyleSheet("QLabel { background-color : #394c53; }");
        } else if(d->widgetType == Type::Well) {
            d->ui.titleButton->setIcon(QIcon());
            d->ui.titleButton->setObjectName("bt-tsx-thumbnail-well");
        }
    }

    auto CollapseWidget::GetType() const -> Type {
        return d->widgetType;
    }

    auto CollapseWidget::AddWidget(QWidget* widget)->void {
        d->ui.contentsFrame->layout()->addWidget(widget);
    }

    auto CollapseWidget::ExpandAll() -> void {
        if(d->collapsed) {
            onExpanded(true);
        }
    }

    void CollapseWidget::onExpanded(bool checked) {
        if (sender() == d->ui.titleButton) {
            d->ui.arrowButton->blockSignals(true);
            d->ui.arrowButton->setChecked(checked);
            d->ui.arrowButton->blockSignals(false);
        }
        else if (sender() == d->ui.arrowButton) {
            d->ui.titleButton->blockSignals(true);
            d->ui.titleButton->setChecked(checked);
            d->ui.titleButton->blockSignals(false);
        }
        else {
            d->ui.arrowButton->blockSignals(true);
            d->ui.arrowButton->setChecked(checked);
            d->ui.arrowButton->blockSignals(false);

            d->ui.titleButton->blockSignals(true);
            d->ui.titleButton->setChecked(checked);
            d->ui.titleButton->blockSignals(false);
        }

        d->collapsed = !checked;

        d->ui.contentsFrame->setVisible(!d->collapsed);
        d->ui.lineLabel->setVisible(!d->collapsed);

        emit sigChangedCollapse();
    }
}
