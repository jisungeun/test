#define LOGGER_TAG "[ListPanel]"

#include "ListPanel.h"
#include "ui_ListPanel.h"
#include "ListPanelControl.h"

#include <QDirIterator>
#include <QFileInfo>

#include <TCLogger.h>
#include <System.h>
#include <SessionManager.h>
#include <TCFDataRepo.h>
#include <MessageDialog.h>

#include "CurrentItemObserver.h"
#include "ExperimentObserver.h"
#include "DataObserver.h"
#include "ExperimentLoader.h"

using HTXpress::AppComponents::ProcessedDataDefinition::TCFProcessingStatus;
using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;
using HTXpress::AppComponents::AcquisitionListPanel::AcquisitionListPanel;
using HTXpress::AppComponents::AcquisitionListPanel::AcquisitionDataIndex;

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ListPanel::Impl {
        Ui::ListPanel ui;
        ListPanelControl control;

        AppEntity::Experiment::Pointer experiment{};

        DataObserver* dataObserver{ nullptr };
        CurrentItemObserver* itemObserver{ nullptr };
        ExperimentObserver* experimentObserver{ nullptr };

        auto GenerateAcquisitionListItem(const QString& path, const QString& specimen, const QString& well, const QString& acqData)->AppComponents::AcquisitionListPanel::AcquisitionDataIndex;
        auto Convert(TCFProcessingStatus state)->AppComponents::AcquisitionListPanel::ProcessingStatus;
        auto Exist(const QStringList& files) -> bool;
    };

    auto ListPanel::Impl::GenerateAcquisitionListItem(const QString& path, const QString& specimen, const QString& well, const QString& acqData) -> AppComponents::AcquisitionListPanel::AcquisitionDataIndex {
        AcquisitionDataIndex acquisitionData;
        acquisitionData.SetGroupName(specimen);
        acquisitionData.SetWellPositionName(well);
        acquisitionData.SetAcquisitionDataName(acqData);
        acquisitionData.SetAcquisitionDataPath(path);

        // data folder의 시작 포맷이 [yyMMdd.hhmmss.] 일때 유효
        const auto subStrings = acqData.section(".", 0, 1);
        if (!subStrings.isEmpty()) {
            acquisitionData.SetTimestamp(subStrings);
        }

        return acquisitionData;
    }

    auto ListPanel::Impl::Convert(TCFProcessingStatus state) -> AppComponents::AcquisitionListPanel::ProcessingStatus {
        auto result = AppComponents::AcquisitionListPanel::ProcessingStatus::Invalid;
        if (state == +TCFProcessingStatus::NotYet || state == +TCFProcessingStatus::InProgress) {
            result = AppComponents::AcquisitionListPanel::ProcessingStatus::Unprocessed;
        }
        else if (state == +TCFProcessingStatus::Completed) {
            result = AppComponents::AcquisitionListPanel::ProcessingStatus::Processed;
        }

        return result;
    }

    auto ListPanel::Impl::Exist(const QStringList& files) -> bool {
        for(const auto& file : files) {
            if(!QFileInfo::exists(file)) {
                return false;
            }
        }
        return true;
    }

    ListPanel::ListPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);
        d->ui.vline->setObjectName("line-divider");
        d->ui.hline->setObjectName("line-divider");
        d->ui.dataListView->setStyleSheet("QTableView {border-bottom: none;}");
        d->ui.exportButton->setObjectName("bt-highlight");
        d->ui.copyButton->setObjectName("bt-highlight");
        d->ui.deleteButton->setObjectName("bt-highlight");
        d->ui.viewButton->setObjectName("bt-highlight");

        d->ui.dataListView->SetCheckVisibility(true);
        d->ui.dataListView->SetSelectionMode(QAbstractItemView::SelectionMode::MultiSelection);
        d->ui.vesselMapPanel->SetMode(VesselMapPanel::Mode::List);
        d->ui.vesselMapPanel->SetViewMode(TC::ViewMode::CanvasMode);
        d->ui.stackedWidget->setCurrentIndex(0);

        d->itemObserver = new CurrentItemObserver;
        d->experimentObserver = new ExperimentObserver;
        d->dataObserver = new DataObserver;

        connect(d->itemObserver, &CurrentItemObserver::sigCurrentItemChanged, this, &ListPanel::onLoadExperimentData);
        connect(d->experimentObserver, &ExperimentObserver::sigExperimentLoaded, this, &ListPanel::onLoadedExperiment);

        connect(d->ui.dataListView, &AcquisitionListPanel::sigSelectedDataIndex, this, &ListPanel::onSelectedDataIndex);

        connect(d->dataObserver, &DataObserver::sigDataAdded, this, &ListPanel::onDataAdded);
        connect(d->dataObserver, &DataObserver::sigDataUpdated, this, &ListPanel::onDataUpdated);
        connect(d->dataObserver, &DataObserver::sigDataDeleted, this, &ListPanel::onDataDeleted);
        connect(d->dataObserver, &DataObserver::sigDataRootFolderDeleted, this, &ListPanel::onDataRootFolderDeleted);

        connect(d->ui.viewButton, SIGNAL(clicked()), this, SLOT(onViewButtonClicked()));
        connect(d->ui.exportButton, SIGNAL(clicked()), this, SLOT(onExportButtonClicked()));
        connect(d->ui.copyButton, SIGNAL(clicked()), this, SLOT(onCopyButtonClicked()));
        connect(d->ui.deleteButton, SIGNAL(clicked()), this, SLOT(onDeleteButtonClicked()));
    }

    ListPanel::~ListPanel() {
    }

    auto ListPanel::Clear() -> void {
        d->ui.dataListView->ClearAll();
        d->ui.stackedWidget->setCurrentIndex(0);
    }

    void ListPanel::onLoadedExperiment(const AppEntity::Experiment::Pointer& experiment) {
        if (nullptr == experiment) return;
        d->experiment = experiment;
        d->ui.vesselMapPanel->SetExperiment(d->experiment);
    }

    void ListPanel::onLoadExperimentData(const QString& user, const QString& project, const QString& experiment, const QString& specimen, const QString& well) {
        Q_UNUSED(specimen)
        Q_UNUSED(well)

        if (user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;

        d->ui.dataListView->ClearAll();
        d->ui.stackedWidget->setCurrentIndex(0);

        const auto dataRepo = TCFDataRepo::GetInstance();
        const auto experiments = dataRepo->GetExperimentList(user, project);
        if (!experiments.contains(experiment)) {
            QLOG_ERROR() << "It fails to get acquisition data";
            return;
        }

        auto specimens = experiments.value(experiment);
        TCFDataRepo::SpecimenList::iterator specimenIt;
        for (specimenIt = specimens.begin(); specimenIt != specimens.end(); ++specimenIt) {
            if (specimenIt.key().isEmpty()) continue;

            auto wells = specimenIt.value();
            TCFDataRepo::WellList::iterator wellIt;
            for (wellIt = wells.begin(); wellIt != wells.end(); ++wellIt) {
                if (wellIt.key().isEmpty()) continue;

                auto dataList = wellIt.value();
                TCFDataRepo::DataList::iterator dataIt;
                for (dataIt = dataList.begin(); dataIt != dataList.end(); ++dataIt) {
                    if (dataIt.key().isEmpty()) continue;

                    auto acqData = dataIt.value();
                    if (nullptr == acqData) continue;

                    auto state = AppComponents::AcquisitionListPanel::ProcessingStatus::Invalid;
                    state = d->Convert(acqData->GetStatus());

                    d->ui.dataListView->AddAcquisitionData(
                        d->GenerateAcquisitionListItem(acqData->GetPath(), specimenIt.key(), wellIt.key(), dataIt.key()),
                        state
                    );
                }
            }
        }

        d->ui.dataListView->SetDefaultTableOrder();
    }

    void ListPanel::onDataAdded(const QString& fileFullPath) {
        const auto dataRepo = TCFDataRepo::GetInstance();
        const auto user = d->control.ParseUserName(fileFullPath);
        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);
        const auto dataInfo = dataRepo->GetData(user, project, experiment, specimen, well, dataName);
        if (dataInfo == nullptr) return;
        const auto path = dataInfo->GetPath();
        
        d->ui.dataListView->AddAcquisitionData(d->GenerateAcquisitionListItem(path, specimen, well, dataName));
    }

    void ListPanel::onDataUpdated(const QString& fileFullPath) {
        const auto dataRepo = TCFDataRepo::GetInstance();
        const auto user = d->control.ParseUserName(fileFullPath);
        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);
        const auto dataInfo = dataRepo->GetData(user, project, experiment, specimen, well, dataName);
        if (dataInfo == nullptr) return;
        const auto path = dataInfo->GetPath();

        d->ui.dataListView->SetDataProcessingStatus(
            d->GenerateAcquisitionListItem(path, specimen, well, dataName),
            d->Convert(dataInfo->GetStatus())
        );
    }

    void ListPanel::onDataDeleted(const QString& fileFullPath) {
        const auto dataRepo = TCFDataRepo::GetInstance();
        const auto user = d->control.ParseUserName(fileFullPath);
        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);
        const auto dataInfo = dataRepo->GetData(user, project, experiment, specimen, well, dataName);
        if (dataInfo == nullptr) return;
        const auto path = dataInfo->GetPath();

        d->ui.dataListView->SetDataProcessingStatus(
            d->GenerateAcquisitionListItem(path, specimen, well, dataName),
            d->Convert(TCFProcessingStatus::NotYet));
    }

    void ListPanel::onDataRootFolderDeleted(const QString& fileFullPath) {
        const auto dataRePo = TCFDataRepo::GetInstance();
        const auto user = d->control.ParseUserName(fileFullPath);
        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);
        const auto dataInfo = dataRePo->GetData(user, project, experiment, specimen, well, dataName);
        if (dataInfo == nullptr) {
            const auto dataInfoString = QString("%1/%2/%3/%4/%5/%6").arg(user).arg(project).arg(experiment).arg(specimen).arg(well).arg(dataName);
            QLOG_ERROR() << dataInfoString << "is nullptr";
            return;
        }
        const auto path = dataInfo->GetPath();

        const auto acquisitionDataIndex = d->GenerateAcquisitionListItem(path, specimen, well, dataName);
        d->ui.dataListView->DeleteAcquisitionData(acquisitionDataIndex);
    }

    void ListPanel::onSelectedDataIndex(const AppComponents::AcquisitionListPanel::AcquisitionDataIndex& index) {
        d->ui.stackedWidget->setCurrentIndex(1);

        const auto path = index.GetAcquisitionDataPath();
        d->ui.imageWidget->SetPath(path);
        d->ui.infoWidget->SetPath(path);

        ExperimentLoader experimentLoader(path);
        const auto experiment = experimentLoader.GetExperimentInDataFolder();
        if(experiment.has_value()) {
            d->ui.vesselMapPanel->SetExperiment(experiment.value());
        }
        else {
            QLOG_INFO() << "Failed to load Vessel at the time of data acquisition.";
            d->ui.vesselMapPanel->SetExperiment(d->experiment);
        }

        const auto imagingDataFolderPath = QFileInfo(path).absoluteDir().absolutePath();

        d->ui.vesselMapPanel->SetCurrentLocationPoint(imagingDataFolderPath);
        d->ui.vesselMapPanel->FitWellCanvas();
    }

    void ListPanel::onViewButtonClicked() {
        auto list = d->ui.dataListView->GetSelectedList();
        if (list.isEmpty()) return;
        if (!d->Exist(list)) {
            TC::MessageDialog::warning(this, tr("Unable to launch"), tr("Data processing is not completed.\nPlease check the processing of the selected data is complete."));
            return;
        }
        emit sigViewImage(list);
    }

    void ListPanel::onExportButtonClicked() {
        auto list = d->ui.dataListView->GetSelectedList();
        if (list.isEmpty()) return;
        if (!d->Exist(list)) {
            TC::MessageDialog::warning(this, tr("Export failed"), tr("Data processing is not completed.\nPlease check the processing of the selected data is complete."));
            return;
        }
        emit sigExport(list);
    }

    void ListPanel::onCopyButtonClicked() {
        auto list = d->ui.dataListView->GetSelectedList();
        if (list.isEmpty()) return;
        if (!d->Exist(list)) {
            TC::MessageDialog::warning(this, tr("Copy failed"), tr("Data processing is not completed.\nPlease check the processing of the selected data is complete."));
            return;
        }
        emit sigCopy(list);
    }

    void ListPanel::onDeleteButtonClicked() {
        auto list = d->ui.dataListView->GetSelectedList();
        if (list.isEmpty()) return;

        emit sigDelete(list);
    }
}
