#include "ProjectObserver.h"
#include "ProjectUpdater.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    ProjectObserver::ProjectObserver(QObject* parent) : QObject(parent) {
        ProjectUpdater::GetInstance()->Register(this);
    }

    ProjectObserver::~ProjectObserver() {
        ProjectUpdater::GetInstance()->Deregister(this);
    }

    auto ProjectObserver::UpdateProjectList(const QList<QString>& projects)->void {
        emit sigProjectListUpdated(projects);
    }

    auto ProjectObserver::ChangeCurrentProject(const QString& projectTitle) -> void {
        emit sigCurrentProjectChanged(projectTitle);
    }
}
