#pragma once

#include <memory>

#include <IExperimentView.h>
#include "ExperimentObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class ExperimentUpdater : public Interactor::IExperimentView {
    public:
        using Pointer = std::shared_ptr<ExperimentUpdater>;

    protected:
        ExperimentUpdater();

    public:
        ~ExperimentUpdater();

        static auto GetInstance()->Pointer;

        auto ChangeCurrentExperiment(const QString& project, const QString& experiment)->void override;
        auto LoadExperiment(const AppEntity::Experiment::Pointer& experiment) const->void override;
        auto LoadExperimentList(const QList<QString>& experiments)->void override;

        auto Register(ExperimentObserver* observer)->void;
        auto Deregister(ExperimentObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}