#include <QFileDialog>
#include <QTimer>

#include "MessageDialog.h"
#include "CopyDialogControl.h"
#include "CopyDialog.h"
#include "ui_CopyDialog.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct CopyDialog::Impl {
        CopyDialog* p { nullptr };
        CopyDialogControl control;
        Ui::CopyDialog ui;
        QFrame* frame{ nullptr };

        bool copying{ false };
        int32_t copiedCount{ 0 };
        int32_t requestedCount{ 0 };

        Impl(CopyDialog* p) : p{ p } {}
        auto InitUi()->void;
        auto UpdateUi()->void;
        auto SelectTarget()->void;
        auto Copy()->void;
        auto StopCopy()->void;
        auto UpdateProgress()->void;
        auto Completed()->void;
    };

    auto CopyDialog::Impl::InitUi() -> void {
        frame = new QFrame(p);
        frame->setMinimumWidth(550);
        ui.setupUi(frame);
        p->SetContext(frame);
        p->SetStandardButtons(StandardButton::Close);
        p->SetClosable(false);
        p->adjustSize();

        ui.optionBox->addItems({"", "TCF only", "TCF with the folder structure", "All files and folders"});

        ui.startBtn->setDisabled(true);
        ui.startBtn->setObjectName("bt-light");

        ui.progressBar->hide();
        ui.progressBar->setValue(0);

        ui.statusLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.statusLabel->hide();
    }

    auto CopyDialog::Impl::UpdateUi() -> void {
        auto optionSelected = !ui.optionBox->currentText().isEmpty();
        auto targetSelected = !ui.targetEdit->text().isEmpty();
        ui.startBtn->setEnabled(optionSelected && targetSelected);
    }

    auto CopyDialog::Impl::SelectTarget() -> void {
        static QString prevDir("D:/");

        auto selection = QFileDialog::getExistingDirectory(p, "Select a target directory", prevDir);
        if(selection.isEmpty()) return;
        prevDir = selection;

        ui.targetEdit->setText(selection);

        UpdateUi();
    }

    auto CopyDialog::Impl::Copy() -> void {
        const auto option = ui.optionBox->currentIndex();
        const auto target = ui.targetEdit->text();

        ui.startBtn->setText("Stop");
        p->GetDialogButtonBox()->setDisabled(true);
        copiedCount = 0;
        copying = true;
        ui.progressBar->setValue(0);
        ui.progressBar->show();
        p->adjustSize();
        control.Start(static_cast<CopyDialogControl::Option>(option - 1), target);
    }

    auto CopyDialog::Impl::StopCopy() -> void {
        const auto selection = TC::MessageDialog::question(p, "Stop", "Do you want to stop copying files?");
        if(selection != TC::MessageDialog::StandardButton::Yes) return;

        control.Stop();
    }

    auto CopyDialog::Impl::UpdateProgress() -> void {
        copiedCount++;
        ui.progressBar->setValue(copiedCount);
    }

    auto CopyDialog::Impl::Completed() -> void {
        copying = false;
        ui.startBtn->setText("Start");
        p->GetDialogButtonBox()->setDisabled(false);
        ui.progressBar->hide();

        if(requestedCount == copiedCount) {
            ui.statusLabel->setText(QString("All %1 data have been copied as requested").arg(requestedCount));
        } else {
            ui.statusLabel->setText(QString("Only %1 / %2 data have been successfully copied").arg(copiedCount).arg(requestedCount));
        }
        ui.statusLabel->show();
        p->adjustSize();

        QTimer::singleShot(2000, p, [this]() {
            ui.statusLabel->hide();
            p->adjustSize();
        });
    }

    CopyDialog::CopyDialog(QWidget* parent) : CustomDialog(parent), d{ std::make_unique<Impl>(this) } {
        d->InitUi();

        connect(d->ui.targetBtn, &QPushButton::clicked, this, [this]() {
            d->SelectTarget();
        });

        connect(d->ui.optionBox, qOverload<int>(&QComboBox::currentIndexChanged), this, [this](int32_t index) {
            Q_UNUSED(index)
            d->UpdateUi();
        });

        connect(d->ui.startBtn, &QPushButton::clicked, this, [this]() {
            if(d->copying) d->StopCopy();
            else d->Copy();
        });

        connect(&d->control, &CopyDialogControl::sigCopied, this, [this]() {
            d->UpdateProgress();
        });

        connect(&d->control, &CopyDialogControl::sigCompleted, this, [this]() {
            d->Completed();
        });
    }

    CopyDialog::~CopyDialog() {
    }

    auto CopyDialog::SetList(const QList<QString>& list) -> void {
        d->control.SetList(list);
        d->ui.progressBar->setRange(0, list.size());
        d->requestedCount = list.size();
    }

    int CopyDialog::GetMinimumWidth() const {
        return minimumSize().width();
    }
}
