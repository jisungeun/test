#include "AsyncScrollArea.h"

#include <QList>
#include <QScrollBar>
#include <QMouseEvent>

namespace HTXpress::AppPlugins::Data::Navigation {
    struct AsyncScrollArea::Impl {
        QList<QWidget*> widgets;
    };

    AsyncScrollArea::AsyncScrollArea(QWidget* parent) : QScrollArea(parent), d{new Impl} {
        connect(this->verticalScrollBar(), &QScrollBar::valueChanged, this, &AsyncScrollArea::onScrollBarValueChanged);
    }

    AsyncScrollArea::~AsyncScrollArea() {
    }

    auto AsyncScrollArea::RegisterWidget(QWidget* widget) ->void {
        d->widgets.append(widget);
    }

    auto AsyncScrollArea::DergisterWidget(QWidget* widget)->void {
        d->widgets.removeOne(widget);
    }

    auto AsyncScrollArea::DergisterAllWidgets()->void {
        d.reset(new Impl);
    }

    auto AsyncScrollArea::GetWidgets() const ->QList<QWidget*> {
        return d->widgets;
    }

    auto AsyncScrollArea::GetWidget(const int& index) const ->QWidget* {
        return d->widgets.at(index);
    }

    auto AsyncScrollArea::GetIndex(QWidget* widget) const ->int {
        return d->widgets.indexOf(widget, 0);
    }

    auto AsyncScrollArea::GetRenderWidgets() const ->QList<QWidget*> {
        QList<QWidget*> widgets;
        for(const auto& widget : d->widgets) {
            if (nullptr == widget) continue;
            if (false == IsVisible(widget)) continue;
            widgets.append(widget);
        }

        return widgets;
    }

    auto AsyncScrollArea::GetRenderIndexes() const ->QList<int> {
        QList<int> indexes;
        for(auto i = 0; i < d->widgets.size(); ++i) {
            auto widget = d->widgets.at(i);
            if (nullptr == widget) continue;
            if (false == IsVisible(widget)) continue;
            indexes.append(i);
        }

        return indexes;
    }

    auto AsyncScrollArea::IsVisible(QWidget* widget) const ->bool {
        if (nullptr == widget) return false;
        const auto index = d->widgets.indexOf(widget, 0);
        if (0 > index) return false;

        return IsVisible(index);
    }

    auto AsyncScrollArea::IsVisible(const int& index) const ->bool {
        const auto widget = d->widgets.at(index);
        if (nullptr == widget) return false;
        if (widget->visibleRegion().isEmpty() || widget->visibleRegion().isNull()) return false;

        return true;
    }
    
    void AsyncScrollArea::onScrollBarValueChanged(int value) {
        Q_UNUSED(value)
        emit sigUpdateRender(GetRenderIndexes());
    }
}
