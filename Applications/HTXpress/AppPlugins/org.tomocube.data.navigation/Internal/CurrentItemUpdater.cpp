#include "CurrentItemUpdater.h"
#include "CurrentItemObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct CurrentItemUpdater::Impl {
        QList<CurrentItemObserver*> observers;
    };

    CurrentItemUpdater::CurrentItemUpdater() : d{ new Impl } {
    }

    CurrentItemUpdater::~CurrentItemUpdater() {
    }

    auto CurrentItemUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new CurrentItemUpdater() };
        return theInstance;
    }

    auto CurrentItemUpdater::Register(CurrentItemObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto CurrentItemUpdater::Deregister(CurrentItemObserver* observer) -> void {
        d->observers.removeOne(observer);
    }

    auto CurrentItemUpdater::ChangeCurrentItem(const QString& user, const QString& project, const QString& experiment, const QString& specimen, const QString& well)->void {
        for (auto observer : d->observers) {
            observer->ChangeCurrentItem(user, project, experiment, specimen, well);
        }
    }
}