#include "VesselMapPanelControl.h"

#include <VesselRepo.h>
#include <ConfigController.h>
#include <ConfigPresenter.h>
#include <VesselMapUtility.h>
#include <SystemStatus.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    struct VesselMapPanelControl::Impl {
        auto ConvertWellShape(AppEntity::WellShape shape)->TC::WellShape;
        auto ConvertImagingAreaShape(AppEntity::ImagingAreaShape shape)->TC::ImagingAreaShape;
    };

    auto VesselMapPanelControl::Impl::ConvertWellShape(AppEntity::WellShape shape) -> TC::WellShape {
        auto wellShape = TC::WellShape::Circle;
        if (shape == +AppEntity::WellShape::Rectangle) {
            wellShape = TC::WellShape::Rectangle;
        }

        return wellShape;
    }

    auto VesselMapPanelControl::Impl::ConvertImagingAreaShape(AppEntity::ImagingAreaShape shape) -> TC::ImagingAreaShape {
        auto imagingAreaShape = TC::ImagingAreaShape::Circle;
        if (shape == +AppEntity::ImagingAreaShape::Rectangle) {
            imagingAreaShape = TC::ImagingAreaShape::Rectangle;
        }

        return imagingAreaShape;
    }

    VesselMapPanelControl::VesselMapPanelControl() : d{ new Impl } {
        
    }

    VesselMapPanelControl::~VesselMapPanelControl() {
        
    }

    auto VesselMapPanelControl::GenerateVesselMap(const AppEntity::VesselIndex& vesselIndex, const AppEntity::Experiment::Pointer& experiment) -> TC::VesselMap::Pointer {
        using Converter = AppComponents::VesselMapUtility::Converter;

        const auto& vessel = experiment->GetVessel();
        const auto& wellNames = experiment->GetWellNames(vesselIndex);
        auto vesselmap = Converter::ToTCVesselMap(vesselIndex, experiment, vessel, wellNames);
        return vesselmap;
    }

    auto VesselMapPanelControl::LoadAcqInfo(const QString& path, int& wellIndex, AppEntity::Position& position) -> bool {
        auto presenter = new Interactor::ConfigPresenter();
        auto controller = Interactor::ConfigController(presenter);

        if(false == controller.LoadAcuInfo(path)) return false;
        
        presenter->GetAcqInfo(wellIndex, position);
        return true;
    }
}
