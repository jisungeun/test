#include <QFileDialog>
#include <QTimer>

#include <MessageDialog.h>
#include <InputDialog.h>

#include "DeleteDialogControl.h"
#include "DeleteDialog.h"
#include "ui_DeleteDialog.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct DeleteDialog::Impl {
        DeleteDialog* p { nullptr };
        DeleteDialogControl control;
        Ui::DeleteDialog ui;
        QFrame* frame{ nullptr };

        bool deleting{ false };
        int32_t deletedCount{ 0 };
        int32_t requestedCount{ 0 };

        Impl(DeleteDialog* p) : p{ p } {}
        auto InitUi()->void;
        auto UpdateUi()->void;
        auto Delete()->void;
        auto StopDelete()->void;
        auto UpdateProgress()->void;
        auto Completed()->void;
    };

    auto DeleteDialog::Impl::InitUi() -> void {
        frame = new QFrame(p);
        frame->setMinimumWidth(550);
        ui.setupUi(frame);
        p->SetContext(frame);
        p->SetStandardButtons(StandardButton::Close);
        p->SetClosable(false);
        p->adjustSize();

        ui.optionBox->addItems({"", "Raw files only", "All files and folders"});

        ui.startBtn->setDisabled(true);
        ui.startBtn->setObjectName("bt-light");

        ui.progressBar->hide();
        ui.progressBar->setValue(0);

        ui.statusLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.statusLabel->hide();
    }

    auto DeleteDialog::Impl::UpdateUi() -> void {
        auto optionSelected = !ui.optionBox->currentText().isEmpty();
        ui.startBtn->setEnabled(optionSelected && (requestedCount>0));
    }

    auto DeleteDialog::Impl::Delete() -> void {
        const auto password = TC::InputDialog::getText(p, "Authentication", "Enter your password", QLineEdit::Password);
        if(!control.CheckPassword(password)) {
            TC::MessageDialog::warning(p, "Authentication", "Invalid password is entered");
            return;
        }

        const auto option = ui.optionBox->currentIndex();

        ui.startBtn->setText("Stop");
        p->GetDialogButtonBox()->setDisabled(true);
        deletedCount = 0;
        deleting = true;
        ui.progressBar->setValue(0);
        ui.progressBar->show();
        p->adjustSize();
        control.Start(static_cast<DeleteDialogControl::Option>(option - 1));
    }

    auto DeleteDialog::Impl::StopDelete() -> void {
        const auto selection = TC::MessageDialog::question(p, "Stop", "Do you want to stop deleting files?");
        if(selection != TC::MessageDialog::StandardButton::Yes) return;

        control.Stop();
    }

    auto DeleteDialog::Impl::UpdateProgress() -> void {
        deletedCount++;
        ui.progressBar->setValue(deletedCount);
    }

    auto DeleteDialog::Impl::Completed() -> void {
        deleting = false;
        ui.startBtn->setText("Start");
        p->GetDialogButtonBox()->setDisabled(false);
        ui.progressBar->hide();

        if(requestedCount == deletedCount) {
            ui.statusLabel->setText(QString("All %1 data have been deleted as requested").arg(requestedCount));
        } else {
            ui.statusLabel->setText(QString("Only %1 / %2 data have been successfully deleted").arg(deletedCount).arg(requestedCount));
        }
        ui.statusLabel->show();
        p->adjustSize();

        QTimer::singleShot(2000, p, [this]() {
            ui.statusLabel->hide();
            p->adjustSize();
        });
    }

    DeleteDialog::DeleteDialog(QWidget* parent) : CustomDialog(parent), d{ std::make_unique<Impl>(this) } {
        d->InitUi();

        connect(d->ui.optionBox, qOverload<int>(&QComboBox::currentIndexChanged), this, [this](int32_t index) {
            Q_UNUSED(index)
            d->UpdateUi();
        });

        connect(d->ui.startBtn, &QPushButton::clicked, this, [this]() {
            if(d->deleting) d->StopDelete();
            else d->Delete();
        });

        connect(&d->control, &DeleteDialogControl::sigDeleted, this, [this]() {
            d->UpdateProgress();
        });

        connect(&d->control, &DeleteDialogControl::sigCompleted, this, [this]() {
            d->Completed();
        });
    }

    DeleteDialog::~DeleteDialog() {
        d->p = nullptr;
    }

    auto DeleteDialog::SetList(const QList<QString>& list) -> void {
        d->control.SetList(list);
        d->ui.progressBar->setRange(0, list.size());
        d->requestedCount = list.size();
    }

    int DeleteDialog::GetMinimumWidth() const {
        return minimumSize().width();
    }
}
