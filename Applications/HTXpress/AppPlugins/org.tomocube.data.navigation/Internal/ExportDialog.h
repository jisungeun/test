#pragma once

#include <memory>
#include <QString>
#include <QWidget>

#include "CustomDialog.h"
#include "Experiment.h"
#include "TCFMetaReader.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class ExportDialog : public TC::CustomDialog
    {
        Q_OBJECT
    public:
        using Self = ExportDialog;

        explicit ExportDialog(QWidget* parent = nullptr);
        ~ExportDialog() override;

        auto SetList(const QList<QString>& list)->void;
        auto SetTCFPath(const QString& path) const ->void;

    private:
        auto GetMinimumWidth() const -> int override;

    signals:
        void sigProcess();

    private slots:
        void onExportButtonClicked();

        void onUpdateCurrentCount(const int& type, const int& count);
        void onUpdateTotalCount(const int& type, const int& count);

        void onProcessed(const QString& path);
        void onProcessSucceeded(const QString& path, const int& type);
        void onProcessFailed(const QString& path, const int& type, const QString& error);
        void onProcessFinished();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
