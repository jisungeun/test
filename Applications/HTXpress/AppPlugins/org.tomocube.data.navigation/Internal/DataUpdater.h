#pragma once

#include <memory>

#include <IDataView.h>
#include "DataObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class DataUpdater : public Interactor::IDataView {
    public:
        using Pointer = std::shared_ptr<DataUpdater>;

    protected:
        DataUpdater();

    public:
        ~DataUpdater() override;

        static auto GetInstance()->Pointer;

        auto ScannedExperimentData(const QString& user, const QString& project, const QString& experiment)->void override;
        auto AddData(const QString& fileFullPath)->void override;
        auto UpdateData(const QString& fileFullPath)->void override;
        auto DeleteData(const QString& fileFullPath)->void override;
        auto DeleteDataRootFolder(const QString& fileFullPath) -> void override;

        auto Register(DataObserver* observer)->void;
        auto Deregister(DataObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}