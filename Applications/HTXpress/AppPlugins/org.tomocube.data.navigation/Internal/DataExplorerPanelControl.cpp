#include "DataExplorerPanelControl.h"

#include <SystemStatus.h>
#include <UserProjectRepo.h>
#include <AppData.h>

#include "Internal/DataUpdater.h"
#include <DataPresenter.h>
#include <DataController.h>

#include "Internal/ExperimentUpdater.h"
#include <ExperimentController.h>
#include <ExperimentPresenter.h>

#include <DataMisc.h>

using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Data::Navigation {
    struct DataExplorerPanelControl::Impl {
    };

    DataExplorerPanelControl::DataExplorerPanelControl() : d{new Impl} {
    }

    DataExplorerPanelControl::~DataExplorerPanelControl() {
    }

    auto DataExplorerPanelControl::LoadExperiment(const QString& experiment)->void {
        if (experiment.isEmpty()) return;

        auto updater = ExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter(updater.get());
        auto controller = Interactor::ExperimentController(&presenter);

        if (false == controller.LoadExperiment(experiment)) {
            // error
        }
    }

    auto DataExplorerPanelControl::LoadExperimentList()->void {
        auto updater = ExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter(updater.get());
        auto controller = Interactor::ExperimentController(&presenter);

        if (false == controller.LoadExperimentList()) {
            // error
        }
    }

    auto DataExplorerPanelControl::ScanExperimentData(const QString& experiment)->void {
        if (experiment.isEmpty()) return;

        auto updater = DataUpdater::GetInstance();
        auto presenter = Interactor::DataPresenter(updater.get());
        auto controller = Interactor::DataController(&presenter);

        if (false == controller.ScanExperimentData(experiment)) {
            //error message
        }
    }

    auto DataExplorerPanelControl::IsLoadedExperiment() -> bool {
        return (AppEntity::SystemStatus::GetInstance()->GetExperiment() != nullptr);
    }

    auto DataExplorerPanelControl::GetProjects() const -> QStringList {
        const auto user = Entity::AppData::GetInstance()->GetUser();
        return Entity::UserProjectRepo::GetInstance()->GetProjects(user);
    }

    auto DataExplorerPanelControl::ParseProjectName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetProjectName(fileFullPath);
    }

    auto DataExplorerPanelControl::ParseExperimentName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetExperimentName(fileFullPath);
    }

    auto DataExplorerPanelControl::ParseSpecimenName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetSpecimenName(fileFullPath);
    }

    auto DataExplorerPanelControl::ParseWellName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetWellName(fileFullPath);
    }
}
