#pragma once
#include <memory>

#include <QString>

#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    class DataExplorerPanelControl {
    public:
        DataExplorerPanelControl();
        ~DataExplorerPanelControl();

        auto LoadExperiment(const QString& experiment) -> void;
        auto LoadExperimentList() -> void;

        auto ScanExperimentData(const QString& experiment) -> void;

        auto IsLoadedExperiment() -> bool;

        auto GetProjects() const -> QStringList;

        auto ParseProjectName(const QString& fileFullPath) const -> QString;
        auto ParseExperimentName(const QString& fileFullPath) const -> QString;
        auto ParseSpecimenName(const QString& fileFullPath) const -> QString;
        auto ParseWellName(const QString& fileFullPath) const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
