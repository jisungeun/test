#include "UserObserver.h"
#include "UserUpdater.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    UserObserver::UserObserver(QObject* parent) : QObject(parent) {
        UserUpdater::GetInstance()->Register(this);
    }

    UserObserver::~UserObserver() {
        UserUpdater::GetInstance()->Deregister(this);
    }

    auto UserObserver::ChangeUser(const AppEntity::UserID& user) -> void {
        emit sigUserChanged(user);
    }

    auto UserObserver::UpdateUserList(const QList<AppEntity::UserID>& users) -> void {
        emit sigUserListUpdated(users);
    }

}
