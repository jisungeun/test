#pragma once
#include <memory>
#include <QWidget>

#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    class ProjectPanel : public QWidget {
        Q_OBJECT

    public:
        ProjectPanel(QWidget* parent = nullptr);
        ~ProjectPanel() override;

        auto SetCurrentProject(const QString& project)->void;

    private slots:
        void onUserComboBoxTextChanged(const QString& user);
        void onProjectComboBoxTextChanged(const QString& text);

        void onUpdateUserList(const QList<AppEntity::UserID>& users);
        void onChangeUser(const AppEntity::UserID&user); 
        void onUpdateProjectList(const QList<QString>& projects);
        void onChangeCurrentExperiment(const QString& project, const QString& experiment);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
