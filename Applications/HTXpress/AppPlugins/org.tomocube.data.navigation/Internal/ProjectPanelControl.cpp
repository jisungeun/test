#define LOGGER_TAG "[ProjectPanelControl]"
#include <TCLogger.h>

#include <SessionManager.h>
#include <UserProjectRepo.h>

#include "ProjectPanelControl.h"

#include "Internal/ProjectUpdater.h"
#include <ProjectPresenter.h>
#include <ProjectController.h>

#include "Internal/UserUpdater.h"
#include <UserPresenter.h>
#include <UserController.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ProjectPanelControl::Impl {
    };

    ProjectPanelControl::ProjectPanelControl() : d{new Impl} {
    }

    ProjectPanelControl::~ProjectPanelControl() {
    }

    auto ProjectPanelControl::GetCurrentUserProfile() -> AppEntity::Profile {
        return AppEntity::SessionManager::GetInstance()->GetProfile();
    }

    auto ProjectPanelControl::GetProjects(const AppEntity::UserID& user) const -> QStringList {
        const auto repo = Entity::UserProjectRepo::GetInstance();
        return repo->GetProjects(user);
    }

    auto ProjectPanelControl::ChangeUser(const QString& user) -> bool {
        auto updater = UserUpdater::GetInstance();
        auto presenter = Interactor::UserPresenter(updater.get());
        auto controller = Interactor::UserController(&presenter);

        return controller.ChangeUser(user);
    }

    auto ProjectPanelControl::ChangeCurrentProject(const QString& projectTitle)->void {
        auto updater = ProjectUpdater::GetInstance();
        auto presenter = Interactor::ProjectPresenter(updater.get());
        auto controller = Interactor::ProjectController(&presenter);

        if (false == controller.ChangeCurrentProject(projectTitle)) {
            // error 
        }
    }
}
