#pragma once
#include <memory>

#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    class ProjectPanelControl {
    public:
        ProjectPanelControl();
        ~ProjectPanelControl();

    public:
        auto GetCurrentUserProfile()->AppEntity::Profile;
        auto GetProjects(const AppEntity::UserID& user) const->QStringList;

        auto ChangeUser(const QString& user)->bool;
        auto ChangeCurrentProject(const QString& projectTitle)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}