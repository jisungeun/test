#pragma once

#include <memory>
#include <QTreeWidget>
#include <QString>

#include <TCFDataRepo.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    BETTER_ENUM(TreeItemType, int32_t, Project, Experiment, Specimen, Well, Data);

    class DataTreeWidget : public QTreeWidget
    {
        Q_OBJECT
    public:
        explicit DataTreeWidget(QWidget* parent = nullptr);
        ~DataTreeWidget();

        auto SetExperimentList(const QList<QString>& experiments)->void;
        auto SetSpecimenList(const QString& experiment, HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo::SpecimenList specimens)->void;

        auto SetCurrentItem(QTreeWidgetItem* item)->void;
        auto SetCurrentExperimentItem(const QString& experiment)->void;

        void AddItem(const QString& experiment, const QString& specimen, const QString& well, const QString& file);
        void UpdateItem(const QString& experiment, const QString& specimen, const QString& well, const QString& file);
        void DeleteItem(const QString& experiment, const QString& specimen, const QString& well, const QString& file);

    signals:
        void sigCurrentItemChanged(const QString& experiment, const QString& specimen, const QString& well);

    private slots:
        void onTreeItemClicked(QTreeWidgetItem* item, int column);
        void onTreeCurrentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous);

    private:
        auto MakeTreeItem(const TreeItemType& type, const QString& text)->QTreeWidgetItem*;
        auto RemoveTreeItem(QTreeWidgetItem* item, const bool& isExperiment = false) ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}