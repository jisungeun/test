#include "SummaryPanel.h"
#include "ui_SummaryPanel.h"

#include "ExperimentObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct SummaryPanel::Impl {
        Ui::SummaryPanel ui;

        ExperimentObserver* experimentObserver{ nullptr };

        auto ConvertMediumDataToText(const AppEntity::MediumData& medium)->QString;
    };

    auto SummaryPanel::Impl::ConvertMediumDataToText(const AppEntity::MediumData& medium) -> QString {
        return QString("%1(%2)").arg(medium.GetName()).arg(medium.GetRI());
    }

    SummaryPanel::SummaryPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);
        d->ui.baseWidget->setObjectName("panel-light");

        d->experimentObserver = new ExperimentObserver;

        connect(d->experimentObserver, SIGNAL(sigCurrentExperimentChanged(const QString&, const QString&)), this, SLOT(onChangeCurrentExperiment(const QString&, const QString&)));
        connect(d->experimentObserver, SIGNAL(sigExperimentLoaded(const AppEntity::Experiment::Pointer&)), this, SLOT(onLoadExperiment(const AppEntity::Experiment::Pointer&)));
    }

    SummaryPanel::~SummaryPanel() {
    }

    auto SummaryPanel::Clear() -> void {
        d->ui.experimentNameLineEdit->clear();
        d->ui.vesselMapPanel->Clear();
        d->ui.mediumLineEdit->clear();
        d->ui.vesselTypeLineEdit->clear();
        d->ui.numberOfVesselLineEdit->clear();
        d->ui.timelapseTableWidget->Clear();
    }

    void SummaryPanel::onChangeCurrentExperiment(const QString& project, const QString& experiment) {
        if (project.isEmpty() || experiment.isEmpty()) return;

        d->ui.experimentNameLineEdit->setText(experiment);
    }

    void SummaryPanel::onLoadExperiment(const AppEntity::Experiment::Pointer& experiment) {
        if (nullptr == experiment) return;

        d->ui.vesselMapPanel->SetExperiment(experiment);

        const auto currentMedium = experiment->GetMedium();
        const auto currentMediumText = d->ConvertMediumDataToText(currentMedium);
        d->ui.mediumLineEdit->setText(currentMediumText);

        d->ui.vesselTypeLineEdit->setText(experiment->GetVessel()->GetName());
        d->ui.numberOfVesselLineEdit->setText(QString::number(experiment->GetVesselCount()));

        d->ui.timelapseTableWidget->SetScenario(experiment->GetScenario(0));
    }
}
