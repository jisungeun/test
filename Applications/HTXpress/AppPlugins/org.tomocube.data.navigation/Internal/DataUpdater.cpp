#include "DataUpdater.h"

#include "DataObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct DataUpdater::Impl {
        QList<DataObserver*> observers;
    };

    DataUpdater::DataUpdater() : d{ new Impl } {
        
    }

    DataUpdater::~DataUpdater() {
        
    }

    auto DataUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new DataUpdater() };
        return theInstance;
    }

    auto DataUpdater::ScannedExperimentData(const QString& user, const QString& project, const QString& experiment)->void {
        for (auto observer : d->observers) {
            observer->ScannedExperimentData(user, project, experiment);
        }
    }

    auto DataUpdater::AddData(const QString& fileFullPath)->void {
        for (auto observer : d->observers) {
            observer->AddData(fileFullPath);
        }
    }

    auto DataUpdater::UpdateData(const QString& fileFullPath)->void {
        for (auto observer : d->observers) {
            observer->UpdateData(fileFullPath);
        }
    }

    auto DataUpdater::DeleteData(const QString& fileFullPath)->void {
        for (auto observer : d->observers) {
            observer->DeleteData(fileFullPath);
        }
    }

    auto DataUpdater::DeleteDataRootFolder(const QString& fileFullPath) -> void {
        for (auto observer : d->observers) {
            observer->DeleteDataRootFolder(fileFullPath);
        }
    }

    auto DataUpdater::Register(DataObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto DataUpdater::Deregister(DataObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}