#pragma once

#include <memory>

#include "CurrentItemObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class CurrentItemUpdater {
    public:
        using Pointer = std::shared_ptr<CurrentItemUpdater>;

    protected:
        CurrentItemUpdater();

    public:
        ~CurrentItemUpdater();

        static auto GetInstance()->Pointer;

        auto Register(CurrentItemObserver* observer)->void;
        auto Deregister(CurrentItemObserver* observer)->void;

        auto ChangeCurrentItem(const QString& user, const QString& project, const QString& experiment, const QString& specimen, const QString& well)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}