#include "ThumbnailWidgetControl.h"

#include <ThumbnailController.h>
#include <ThumbnailManager.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ThumbnailWidgetControl::Impl {
        
    };

    ThumbnailWidgetControl::ThumbnailWidgetControl() {
        
    }

    ThumbnailWidgetControl::~ThumbnailWidgetControl() {
        
    }

    auto ThumbnailWidgetControl::LoadHTThumbnail(const QString& path, QImage& image, bool& cache)->bool {
        if (path.isEmpty()) return false;

        auto manager = new Plugins::ThumbnailManager::ThumbnailManager;
        auto controller = Interactor::ThumbnailController(manager);

        if (false == controller.LoadHTThumbnail(path, image, cache)) {
            return false;
        }

        return true;
    }

    auto ThumbnailWidgetControl::LoadFLThumbnail(const QString& path, QImage& image, bool& cache)->bool {
        if (path.isEmpty()) return false;

        auto manager = new Plugins::ThumbnailManager::ThumbnailManager;
        auto controller = Interactor::ThumbnailController(manager);

        if (false == controller.LoadFLThumbnail(path, image, cache)) {
            return false;
        }

        return true;
    }

    auto ThumbnailWidgetControl::LoadBFThumbnail(const QString& path, QImage& image, bool& cache)->bool {
        if (path.isEmpty()) return false;

        auto manager = new Plugins::ThumbnailManager::ThumbnailManager;
        auto controller = Interactor::ThumbnailController(manager);

        if (false == controller.LoadBFThumbnail(path, image, cache)) {
            return false;
        }

        return true;
    }

}
