#pragma once

#include <memory>
#include "ThumbnailWidget.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class ThumbnailLayoutWidget : public QWidget
    {
        Q_OBJECT
    public:
        explicit ThumbnailLayoutWidget(QWidget* parent = nullptr);
        ~ThumbnailLayoutWidget();

        auto AppendWidget(ThumbnailWidget* widget) const ->void;
        auto RemoveWidget(ThumbnailWidget* widget) const ->void;

    protected:
        auto eventFilter(QObject* object, QEvent* event)->bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}