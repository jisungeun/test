#define LOGGER_TAG "[VesselMapPanel]"

#include "VesselMapPanel.h"

#include <QList>
#include <QDebug>
#include <QDir>
#include <QFileInfo>

#include <TCLogger.h>
#include <UIUtility.h>
#include <MessageDialog.h>

#include "VesselMapPanelControl.h"
#include "ui_VesselMapPanel.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct VesselMapPanel::Impl {
        Ui::VesselMapPanel ui;
        VesselMapPanelControl control;

        QString currentVesselModel;

        AppEntity::Experiment::Pointer experiment{ nullptr };

        auto ConvertRowColumnsToVesselMapWellIndex(const QList<AppEntity::RowColumn>& rowColumns) const->QList<TC::WellIndex>;
        auto GetAcquisitionType(bool isTile) -> TC::AcquisitionType;
        auto GetLocationTypeByImagingDataFolder(const QString& imagingDataFolderPath, TC::LocationType& locationType) -> bool;
    };

    auto VesselMapPanel::Impl::ConvertRowColumnsToVesselMapWellIndex(const QList<AppEntity::RowColumn>& rowColumns) const -> QList<TC::WellIndex> {
        if (ui.vesselMapWidget == nullptr) {
            return QList<TC::WellIndex>();
        }

        QList<TC::WellIndex> wells;
        for (auto& rc : rowColumns) {
            wells << ui.vesselMapWidget->GetWellIndex(rc.first, rc.second);
        }

        return wells;
    }

    auto VesselMapPanel::Impl::GetAcquisitionType(bool isTile) -> TC::AcquisitionType {
        if(isTile) {
            return TC::AcquisitionType::Tile;
        }
        return TC::AcquisitionType::Point;
    }

    auto VesselMapPanel::Impl::GetLocationTypeByImagingDataFolder(const QString& imagingDataFolderPath, TC::LocationType& locationType) -> bool {
        bool valid = false;
        if(imagingDataFolderPath.section('.', -1).contains("S")) {
            locationType = TC::LocationType::MarkLocation;
            valid = true;
        }

        if(imagingDataFolderPath.section('.', -1).contains("T")) {
            locationType = TC::LocationType::AcquisitionLocation;
            valid = true;
        }

        return valid;
    }

    VesselMapPanel::VesselMapPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);
        d->ui.vesselMapWidget->SetViewMode(TC::ViewMode::DataNaviMode);
    }

    VesselMapPanel::~VesselMapPanel() {
    }

    auto VesselMapPanel::SetMode(const Mode& mode) const ->void {
        auto w = 0, h = 0;
        if(mode == Mode::List) {
            w = 328; h = 304;
            d->ui.vesselMapWidget->setFixedSize(w, h);
            d->ui.vesselMapWidget->SetWellCanvasFixedSize(w, h);
        }
        else {
            w = 132; h = 132;
            d->ui.vesselMapWidget->SetWellCanvasFixedSize(w, h);
        }
    }

    auto VesselMapPanel::SetExperiment(const AppEntity::Experiment::Pointer& experiment)->void {
        if (nullptr == experiment) return;

        d->experiment = experiment;
        d->ui.vesselMapWidget->ClearAll();
        const auto vessel = experiment->GetVessel();
        d->currentVesselModel = vessel->GetModel();
        const auto vesselIndex = 0;

        auto newVesselMap = d->control.GenerateVesselMap(vesselIndex, d->experiment);
        if (newVesselMap == nullptr) {
            return;
        }

        d->ui.vesselMapWidget->SetVesselMap(newVesselMap);

        auto locations = experiment->GetAllLocations(vesselIndex);
        const auto wellList = locations.keys();
        for (auto wellIdx : wellList) {
            auto& locs = locations[wellIdx];
            for (auto iter = locs.begin(); iter != locs.end(); iter++) {
                auto center = iter.value().GetCenter();
                auto area = iter.value().GetArea();
                auto type = d->GetAcquisitionType(iter.value().IsTile());
                d->ui.vesselMapWidget->AddAcquisitionLocationByWellPos(wellIdx, iter.key(), type, center.toMM().x, center.toMM().y, center.toMM().z, area.toMM().width, area.toMM().height);
            }
        }

        const auto wellGroupIndices = experiment->GetWellGroupIndices(vesselIndex);
        for (auto& wellGroupIndex : wellGroupIndices) {
            const auto wellGroup = experiment->GetWellGroup(vesselIndex, wellGroupIndex);

            QList<AppEntity::RowColumn> wellRowColumns;
            auto wells = wellGroup.GetWells();
            for (auto& well : wells) {
                wellRowColumns << AppEntity::RowColumn(well.rowIdx, well.colIdx);
            }
            auto groupName = wellGroup.GetTitle();
            auto groupColor = QColor(wellGroup.GetColor().r,
                                     wellGroup.GetColor().g,
                                     wellGroup.GetColor().b,
                                     wellGroup.GetColor().a);

            d->ui.vesselMapWidget->CreateNewGroup(wellGroupIndex, 
                                                  groupName,
                                                  groupColor,
                                                  d->ConvertRowColumnsToVesselMapWellIndex(wellRowColumns));
        }

    }

    auto VesselMapPanel::SetViewMode(TC::ViewMode mode) const ->void {
        d->ui.vesselMapWidget->SetViewMode(mode);
    }

    auto VesselMapPanel::SetCurrentLocationPoint(const QString& imagingDataFolderPath) -> void {
        const auto configPath = QDir::cleanPath(imagingDataFolderPath + QDir::separator() + "config.dat");

        int32_t wellIndex = -1;
        AppEntity::Position wellPosition{};
        if(false==d->control.LoadAcqInfo(configPath, wellIndex, wellPosition)) {
            QLOG_ERROR() << "Failed to read config file at" << configPath;
            return;
        }

        d->ui.vesselMapWidget->SetSelectedWell(wellIndex);
        // TODO set current location
        TC::LocationType locationType{TC::LocationType::MarkLocation};
        const auto isValidType = d->GetLocationTypeByImagingDataFolder(imagingDataFolderPath, locationType);

        if(isValidType) {
            d->ui.vesselMapWidget->SetCurrentAcquisitionPointByWellPos(locationType, 
                                                                       wellPosition.toMM().x,
                                                                       wellPosition.toMM().y);
        }
        else {
            QLOG_ERROR() << "Unexpected error. failed to get location type:" << locationType._to_string();
        }
    }

    auto VesselMapPanel::FitWellCanvas()->void {
        d->ui.vesselMapWidget->FitWellCanvas();
    }

    void VesselMapPanel::Clear() {
        d->ui.vesselMapWidget->ClearAll();
    }
}
