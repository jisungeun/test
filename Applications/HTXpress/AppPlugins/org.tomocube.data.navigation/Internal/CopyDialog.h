#pragma once
#include <memory>

#include "CustomDialog.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class CopyDialog : public TC::CustomDialog {
        Q_OBJECT

    public:
        explicit CopyDialog(QWidget* parent = nullptr);
        ~CopyDialog() override;

        auto SetList(const QList<QString>& list)->void;

    private:
        auto GetMinimumWidth() const -> int override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
