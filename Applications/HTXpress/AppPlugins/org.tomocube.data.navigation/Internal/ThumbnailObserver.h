#pragma once

#include <memory>
#include <QObject>

namespace HTXpress::AppPlugins::Data::Navigation{
    class ThumbnailObserver : public QObject {
        Q_OBJECT

    public:
        using Pointer = std::shared_ptr<ThumbnailObserver>;

        ThumbnailObserver(QObject* parent = nullptr);
        ~ThumbnailObserver();

        auto LoadedThumbnail(const QString& path, const QImage& image)->void;

    signals:
        void sigThumbnailLoaded(const QString& path, const QImage& image);
    };
}
