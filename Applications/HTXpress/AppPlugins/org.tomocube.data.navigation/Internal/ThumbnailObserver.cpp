#include "ThumbnailObserver.h"
#include "ThumbnailUpdater.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    ThumbnailObserver::ThumbnailObserver(QObject* parent) : QObject(parent) {
        ThumbnailUpdater::GetInstance()->Register(this);
    }

    ThumbnailObserver::~ThumbnailObserver() {
        ThumbnailUpdater::GetInstance()->Deregister(this);
    }

    auto ThumbnailObserver::LoadedThumbnail(const QString& path, const QImage& image)->void {
        emit sigThumbnailLoaded(path, image);
    }
}
