#include "ThumbnailLayoutWidget.h"

#include <QEvent>
#include <QGridLayout>

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ThumbnailLayoutWidget::Impl {
        QGridLayout* layout{ nullptr };

        QList<ThumbnailWidget*> widgets;
        const int _thumbnailColumnCount = 5;
    };

    ThumbnailLayoutWidget::ThumbnailLayoutWidget(QWidget* parent): QWidget(parent), d{ new Impl } {
        d->layout = new QGridLayout;
        d->layout->setHorizontalSpacing(0);
        d->layout->setVerticalSpacing(0);
        d->layout->setMargin(0);
        d->layout->setColumnStretch(0, 1);
        d->layout->setColumnStretch(1, 1);
        d->layout->setColumnStretch(2, 1);
        d->layout->setColumnStretch(3, 1);
        d->layout->setColumnStretch(4, 1);
        this->setLayout(d->layout);

        this->installEventFilter(this);
    }    

    ThumbnailLayoutWidget::~ThumbnailLayoutWidget() {
    }    

    auto ThumbnailLayoutWidget::AppendWidget(ThumbnailWidget* widget) const ->void {
        if (nullptr == d->layout) return;
        if (nullptr == widget) return;

        const auto index = d->layout->count();
        const int row = index / d->_thumbnailColumnCount;
        const int column = index % d->_thumbnailColumnCount;
        d->layout->addWidget(widget, row, column);
        d->layout->setRowStretch(row, 1);

        d->widgets.append(widget);
    }

    auto ThumbnailLayoutWidget::RemoveWidget(ThumbnailWidget* widget) const ->void {
        if (nullptr == d->layout) return;
        if (nullptr == widget) return;

        d->layout->removeWidget(widget);
        const auto removeIndex = d->widgets.indexOf(widget);
        d->widgets.removeAt(removeIndex);

        for(auto i = removeIndex; i < d->widgets.size(); ++i) {
            auto currentWidget = d->widgets.at(i);
            const auto newRow = i / d->_thumbnailColumnCount;
            const auto newColumn = i % d->_thumbnailColumnCount;

            d->layout->removeWidget(currentWidget);
            d->layout->addWidget(currentWidget, newRow, newColumn);
        }

        const auto lastRow = (d->widgets.size() - 1) / d->_thumbnailColumnCount;
        for (auto row = 0; row <= lastRow; ++row) {
            d->layout->setRowStretch(row, 1);
        }
    }

    auto ThumbnailLayoutWidget::eventFilter(QObject* object, QEvent* event) -> bool {
        Q_UNUSED(object)

        if (event->type() == QEvent::Resize) {
            for(auto widget : d->widgets) {
                if(nullptr == widget) continue;
                widget->setFixedHeight(widget->width());
                widget->KeepSquare();
            }

            return true;
        }

        return QWidget::eventFilter(object, event);
    }
}
