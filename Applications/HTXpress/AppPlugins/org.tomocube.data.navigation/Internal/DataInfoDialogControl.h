#pragma once
#include <memory>

#include <QString>

#include "TCFMetaReader.h"
#include <Position.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    class DataInfoDialogControl {
    public:
        DataInfoDialogControl();
        ~DataInfoDialogControl();

        auto GetMeta(const QString& path)->TC::IO::TCFMetaReader::Meta::Pointer;
        auto LoadAcqInfo(const QString& path, int& wellIndex, AppEntity::Position& position)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
