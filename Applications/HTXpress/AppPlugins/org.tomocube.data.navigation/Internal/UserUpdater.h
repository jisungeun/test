#pragma once

#include <memory>

#include <IUserView.h>

#include "UserObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class UserUpdater : public Interactor::IUserView {
    public:
        using Pointer = std::shared_ptr<UserUpdater>;

    protected:
        UserUpdater();

    public:
        ~UserUpdater();

        static auto GetInstance()->Pointer;

        auto ChangeUser(const AppEntity::UserID& user) -> void override;
        auto UpdateUserList(const QList<AppEntity::UserID>& users) -> void override;

        auto Register(UserObserver* observer)->void;
        auto Deregister(UserObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}