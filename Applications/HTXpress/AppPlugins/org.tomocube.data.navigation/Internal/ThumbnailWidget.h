#pragma once

#include <memory>
#include <enum.h>
#include <QWidget>
#include <QGraphicsView>

#include <IThumbnailView.h>
#include <Experiment.h>

#include "ThumbnailWidgetControl.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class ImageView : public QGraphicsView {
    public:
        ImageView(QWidget* parent = nullptr);
        ~ImageView();

        auto SetPixmap(const QPixmap& pixmap)->void;
        auto KeepSquare()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class ThumbnailWidget : public QWidget {
        Q_OBJECT

            Q_PROPERTY(bool check READ checked WRITE setCheck)
            Q_PROPERTY(QString path READ getPath WRITE setPath)
            Q_PROPERTY(QImage image READ getImage WRITE setImage)
            Q_PROPERTY(int load READ loaded WRITE load)

    public:
        ThumbnailWidget(QWidget* parent = nullptr);
        ~ThumbnailWidget();

        auto SetExperiment(const AppEntity::Experiment::Pointer& experiment)->void;
        auto SetPath(const QString& path) const ->void;
        auto GetPath() const ->QString;

        auto SetCheck(const bool& check)->void;
        auto GetChecked() const ->bool;
        auto SetCheckable(const bool& able) const ->void;

        auto SetVisibleTooltip(const bool& visible)->void;

        auto SetImage(const QImage& image)->void;
        auto SetImageSize(const int& w, const int& h) const ->void;
        auto LoadThumbnail(const ImageType& type)->bool;

        auto KeepSquare()->void;

    protected:
        auto eventFilter(QObject* object, QEvent* event)->bool override;

    signals:
        void sigChecked();
        void sigDoubleClicked();

    private slots:
        void onCheckBoxStateChanged(int state);
        void onHideTooltip();

        void onLoadThumbnailDone();

    private:
        auto checked() const ->bool;
        auto setCheck(const bool& check)->void;

        auto getPath()->QString;
        auto setPath(const QString& path)->void;

        auto getImage()->QImage;
        auto setImage(const QImage& image)->void;

        auto load(const int& type)->void;
        auto loaded()->int;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
