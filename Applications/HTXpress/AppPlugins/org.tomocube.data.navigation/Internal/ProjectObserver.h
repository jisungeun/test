#pragma once

#include <memory>
#include <QObject>

namespace HTXpress::AppPlugins::Data::Navigation{
    class ProjectObserver : public QObject {
        Q_OBJECT

    public:
        using Pointer = std::shared_ptr<ProjectObserver>;

        ProjectObserver(QObject* parent = nullptr);
        ~ProjectObserver() override;

        auto UpdateProjectList(const QList<QString>& projects)->void;
        auto ChangeCurrentProject(const QString& projectTitle)->void;

    signals:
        void sigProjectListUpdated(const QList<QString>& projects);
        void sigCurrentProjectChanged(const QString& projectTitle);
    };
}