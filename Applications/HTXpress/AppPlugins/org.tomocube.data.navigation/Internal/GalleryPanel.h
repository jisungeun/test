#pragma once
#include <memory>
#include <QWidget>

#include <QTreeWidgetItem>
#include <QStyledItemDelegate>

#include <Define.h>

#include "Experiment.h"

#include "GalleryPanelControl.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class GalleryPanel : public QWidget {
        Q_OBJECT

    public:
        using Self = GalleryPanel;

        explicit GalleryPanel(QWidget* parent = nullptr);
        ~GalleryPanel() override;

        auto RenderThumbnails()->void;
        auto LoadThumbnails(const QString& user, const QString& project, const QString& experiment)->void;

        auto Clear()->void;

    signals:
        void sigViewImage(const QList<QString>& list);
        void sigExport(const QList<QString>& list);
        void sigCopy(const QList<QString>& list);
        void sigDelete(const QList<QString>& list);

        void sigRequestUpdate();
        void sigRequestRenderIndexes();

    private slots:
        void onLoadedExperiment(const AppEntity::Experiment::Pointer& experiment);
        void onTreeItemSelectionChanged(const QString& user, const QString& project, const QString& experiment, const QString& specimen, const QString& well);
        void onRenderIndexesRequested();

        void onDataAdded(const QString& fileFullPath);
        void onDataUpdated(const QString& fileFullPath);
        void onDataRootFolderDeleted(const QString& fileFullPath);

        void onScrollAreaRenderUpdated(QList<int> indexes);

        void onChangedCollapse();

        void onSelectButtonClicked();
        void onDeselectButtonClicked();
        void onButtonGroupClicked(int id);

        void onViewButtonClicked();
        void onExportButtonClicked();
        void onCopyButtonClicked();
        void onDeleteButtonClicked();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}