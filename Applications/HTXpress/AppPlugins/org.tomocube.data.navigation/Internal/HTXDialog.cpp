#include "HTXDialog.h"
#include "ui_HTXDialog.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct HTXDialog::Impl {
        Ui::HTXDialog ui;
        bool clicked = false;
        int x = 0;
        int y = 0;
    };

    HTXDialog::HTXDialog(QWidget* parent) : QDialog(parent), d{ new Impl } {
        d->ui.setupUi(this);
        d->ui.frame->setObjectName("fr-tsx-dialog");
        d->ui.titleLabel->setObjectName("label-h2");
        d->ui.closeButton->setObjectName("bt-tsx-close");

        this->setModal(true);
        this->setWindowFlags(Qt::FramelessWindowHint);
        this->setAttribute(Qt::WA_TranslucentBackground);

        connect(d->ui.closeButton, SIGNAL(clicked()), this, SLOT(onCloseButtonClicked()));
    }

    HTXDialog::~HTXDialog() {
    }

    auto HTXDialog::SetTitle(const QString& title)->void {
        d->ui.titleLabel->setText(title);
        this->setWindowTitle(title);
    }

    auto HTXDialog::SetWidget(QWidget* widget)->void {
        if (nullptr == widget) return;

        d->ui.contentsLayout->addWidget(widget);
        this->setFixedSize(widget->width(), widget->height() + d->ui.closeButton->height());

        connect(widget, SIGNAL(destroyed()), this, SLOT(onChildDestroyed()));
    }

    void HTXDialog::mousePressEvent(QMouseEvent* event) {
        d->clicked = true;

        d->x = event->x();
        d->y = event->y();
    }

    void HTXDialog::mouseMoveEvent(QMouseEvent* event) {
        if (false == d->clicked) return;
        move(event->globalX() - d->x, event->globalY() - d->y);
    }

    void HTXDialog::mouseReleaseEvent(QMouseEvent* event) {
        Q_UNUSED(event);
        d->clicked = false;
    }

    void HTXDialog::onCloseButtonClicked() {
        this->close();
    }

    void HTXDialog::onChildDestroyed() {
        this->close();
    }
}
