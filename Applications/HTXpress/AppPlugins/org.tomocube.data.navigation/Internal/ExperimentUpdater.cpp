#include "ExperimentUpdater.h"
#include "ExperimentObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ExperimentUpdater::Impl {
        QList<ExperimentObserver*> observers;
    };

    ExperimentUpdater::ExperimentUpdater() : d{ new Impl } {
    }

    ExperimentUpdater::~ExperimentUpdater() {
    }

    auto ExperimentUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new ExperimentUpdater() };
        return theInstance;
    }

    auto ExperimentUpdater::ChangeCurrentExperiment(const QString & project, const QString & experiment)->void {
        for (auto observer : d->observers) {
            observer->ChangeCurrentExperiment(project, experiment);
        }
    }

    auto ExperimentUpdater::LoadExperiment(const AppEntity::Experiment::Pointer& experiment) const->void {
        for (auto observer : d->observers) {
            observer->LoadExperiment(experiment);
        }
    }

    auto ExperimentUpdater::LoadExperimentList(const QList<QString>& experiments)->void {
        for (auto observer : d->observers) {
            observer->LoadExperimentList(experiments);
        }
    }

    auto ExperimentUpdater::Register(ExperimentObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto ExperimentUpdater::Deregister(ExperimentObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}