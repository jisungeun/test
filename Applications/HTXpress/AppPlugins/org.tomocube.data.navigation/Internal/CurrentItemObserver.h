#pragma once

#include <memory>
#include <QObject>

namespace HTXpress::AppPlugins::Data::Navigation {
    class CurrentItemObserver : public QObject {
        Q_OBJECT
    public:
        using Pointer = std::shared_ptr<CurrentItemObserver>;

        CurrentItemObserver(QObject* parent = nullptr);
        ~CurrentItemObserver() override;

        auto ChangeCurrentItem(const QString& user, const QString& project, const QString& experiment, const QString& specimen, const QString& well) -> void;

    signals:
        auto sigCurrentItemChanged(const QString& user, const QString& project, const QString& experiment, const QString& specimen, const QString& well) -> void;
    };
}
