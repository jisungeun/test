#include "DataInfoTableWidgetControl.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct DataInfoTableWidgetControl::Impl {
    };

    DataInfoTableWidgetControl::DataInfoTableWidgetControl() : d{new Impl} {
    }

    DataInfoTableWidgetControl::~DataInfoTableWidgetControl() {
    }

    auto DataInfoTableWidgetControl::GetMeta(const QString& path)->TC::IO::TCFMetaReader::Meta::Pointer {
        if (path.isEmpty()) return nullptr;

        auto metaReader = new TC::IO::TCFMetaReader;
        return metaReader->Read(path);
    }
}
