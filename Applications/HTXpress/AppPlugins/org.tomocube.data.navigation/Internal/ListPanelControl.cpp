#include "ListPanelControl.h"

#include <ExperimentController.h>
#include <ExperimentPresenter.h>
#include <DataMisc.h>
#include <ConfigController.h>
#include <ConfigPresenter.h>

#include "Internal/ExperimentUpdater.h"

using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ListPanelControl::Impl {
    };

    ListPanelControl::ListPanelControl() : d{new Impl} {
    }

    ListPanelControl::~ListPanelControl() {
    }

    auto ListPanelControl::LoadAcqInfo(const QString& path, int& wellIndex, AppEntity::Position& position)->void {
        if (path.isEmpty()) return;

        auto presenter = new Interactor::ConfigPresenter();
        auto controller = Interactor::ConfigController(presenter);

        if(false == controller.LoadAcuInfo(path)) {
            // error
        }

        presenter->GetAcqInfo(wellIndex, position);
    }

    auto ListPanelControl::ParseUserName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetUserName(fileFullPath);
    }

    auto ListPanelControl::ParseProjectName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetProjectName(fileFullPath);
    }

    auto ListPanelControl::ParseExperimentName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetExperimentName(fileFullPath);
    }

    auto ListPanelControl::ParseSpecimenName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetSpecimenName(fileFullPath);
    }

    auto ListPanelControl::ParseWellName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetWellName(fileFullPath);
    }

    auto ListPanelControl::ParseDataName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetTcfBaseName(fileFullPath);
    }
}
