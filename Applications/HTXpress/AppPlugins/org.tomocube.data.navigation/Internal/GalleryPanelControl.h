#pragma once
#include <memory>

#include <QString>
#include <QImage>

namespace HTXpress::AppPlugins::Data::Navigation {
    class GalleryPanelControl {
    public:
        GalleryPanelControl();
        ~GalleryPanelControl();

        auto ParseUserName(const QString& fileFullPath) const -> QString;
        auto ParseProjectName(const QString& fileFullPath) const -> QString;
        auto ParseExperimentName(const QString& fileFullPath) const -> QString;
        auto ParseSpecimenName(const QString& fileFullPath) const -> QString;
        auto ParseWellName(const QString& fileFullPath) const -> QString;
        auto ParseDataName(const QString& fileFullPath) const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
