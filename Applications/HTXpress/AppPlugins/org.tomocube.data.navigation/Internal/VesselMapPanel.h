#pragma once

#include <memory>

#include <QWidget>

#include <Experiment.h>
#include <VesselMap.h>
#include <Position.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    class VesselMapPanel : public QWidget {
        Q_OBJECT

    public:
        enum Mode { List, Info };

        VesselMapPanel(QWidget* parent = nullptr);
        ~VesselMapPanel() override;

        auto SetMode(const Mode& mode) const ->void;
        auto SetExperiment(const AppEntity::Experiment::Pointer& experiment)->void;
        auto SetViewMode(TC::ViewMode mode) const ->void;
        auto SetCurrentLocationPoint(const QString& imagingDataFolderPath) -> void;
        auto FitWellCanvas()->void;

        void Clear();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
