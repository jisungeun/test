#include <QFileDialog>
#include <QThread>

#include "ExportDialog.h"
#include "ui_ExportDialog.h"
#include "ExportWorker.h"
#include "ToastMessageBox.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ExportDialog::Impl {
        explicit Impl(Self* self) : self(self) {}
        Self* self{};

        QFrame* frame{};
        Ui::ExportDialog ui;

        QList<QString> list;
        int32_t successCount{};
        int32_t failCount{};
        int32_t currentFileIndex{1};
        int32_t totalFiles{};

        bool processFinished{true};

        QThread exportThread{};
        ExportWorker* exportWorker{};

        struct SubWidget {
            QCheckBox* checkBox{};
            QProgressBar* progBar{};
        };

        QMap<int32_t, SubWidget> subwidgets;

        const QString successProgressBarStyle = QString("QProgressBar::chunk {"
                                                        "background-color: #64bc6a;"
                                                        "color: #ffffff;"
                                                        "border-radius: 6px;"
                                                        "}");
        const QString failProgressBarStyle = QString("QProgressBar::chunk {"
                                                     "background-color: #D55C56;"
                                                     "color: #ffffff;"
                                                     "border-radius: 6px;"
                                                     "}");

        auto ClearProgressBars() const -> void;
        auto SetProcessStatus(bool isFinished) const -> void;
    };

    auto ExportDialog::Impl::ClearProgressBars() const -> void {
        for(const auto [checkBox, progBar] : subwidgets) {
            progBar->setRange(0,1);
            progBar->setValue(0);
            progBar->setTextVisible(false);
            progBar->setStyleSheet({});
        }
    }

    auto ExportDialog::Impl::SetProcessStatus(bool isFinished) const -> void {
        self->GetButton(StandardButton::Close)->setEnabled(isFinished);
        ui.exportButton->setEnabled(isFinished);
        for(const auto[checkBox, progBar] : subwidgets) {
            checkBox->setEnabled(isFinished);
        }
    }

    ExportDialog::ExportDialog(QWidget* parent) : CustomDialog(parent), d{ std::make_unique<Impl>(this) } {
        d->frame = new QFrame(this);
        d->ui.setupUi(d->frame);
        SetContext(d->frame);
        SetStandardButtons(StandardButton::Close);
        SetClosable(false);

        d->ui.line->setObjectName("line-divider");
        d->ui.exportButton->setObjectName("bt-light");

        d->exportWorker = new ExportWorker();
        d->exportWorker->moveToThread(&d->exportThread);
        d->exportThread.start();

        connect(d->ui.exportButton, SIGNAL(clicked()), this, SLOT(onExportButtonClicked()));

        connect(&d->exportThread, &QThread::finished, d->exportWorker, &QObject::deleteLater);
        connect(this, &ExportDialog::sigProcess, d->exportWorker, &ExportWorker::onProcess);
        connect(d->exportWorker, &ExportWorker::sigProcessed, this, &ExportDialog::onProcessed);
        connect(d->exportWorker, &ExportWorker::sigSucceeded, this, &ExportDialog::onProcessSucceeded);
        connect(d->exportWorker, &ExportWorker::sigFailed, this, &ExportDialog::onProcessFailed);
        connect(d->exportWorker, &ExportWorker::sigFinished, this, &ExportDialog::onProcessFinished);

        d->subwidgets = {
            {DataType::RITomogram,  {d->ui.riTomogramCheckBox, d->ui.progBarHT3D}},
            {DataType::RIMIP,       {d->ui.riMIPCheckBox,      d->ui.progBarHT2D}},
            {DataType::FLTomogram,  {d->ui.flTomogramCheckBox, d->ui.progBarFL3D}},
            {DataType::FLMIP,       {d->ui.flMIPCheckBox,      d->ui.progBarFL2D}},
            {DataType::BrightField, {d->ui.bfCheckBox,         d->ui.progBarBF2D}}
        };
        
        for(auto w : d->subwidgets) {
            w.progBar->setTextVisible(false);
            w.progBar->setVisible(w.checkBox->isChecked());
            connect(w.checkBox, &QCheckBox::toggled, w.progBar, &QProgressBar::setVisible);
        }

        connect(d->exportWorker, &ExportWorker::sigCurrentCount, this, &Self::onUpdateCurrentCount);
        connect(d->exportWorker, &ExportWorker::sigTotalCount, this, &Self::onUpdateTotalCount);
    }

    ExportDialog::~ExportDialog() {
        d->exportThread.quit();
        d->exportThread.wait();
    }

    auto ExportDialog::SetList(const QList<QString>& list)->void {
        d->list = list;
        d->totalFiles = d->list.size();

        if(d->totalFiles==1) {
            d->ui.statusLabel->setText("The file is ready for extraction.");
        }
        else {
            d->ui.statusLabel->setText("Multiple files are ready for extraction.");
        }
    }
    
    auto ExportDialog::GetMinimumWidth() const -> int {
        return d->frame->minimumSize().width();
    }

    void ExportDialog::onExportButtonClicked() {
        if (d->list.isEmpty()) return;

        const auto targetDir = QFileDialog::getExistingDirectory(this, tr("Select export folder"));
        if (targetDir.isEmpty()) return;

        QList<DataType> types;
        if (d->ui.riTomogramCheckBox->isChecked()) types.push_back(DataType::RITomogram);
        if (d->ui.riMIPCheckBox->isChecked()) types.push_back(DataType::RIMIP);
        if (d->ui.flTomogramCheckBox->isChecked()) types.push_back(DataType::FLTomogram);
        if (d->ui.flMIPCheckBox->isChecked()) types.push_back(DataType::FLMIP);
        if (d->ui.bfCheckBox->isChecked()) types.push_back(DataType::BrightField);

        if (types.isEmpty()) return;

        d->exportWorker->SetData(d->list, types);
        d->exportWorker->SetOutput(targetDir, ExportFormat::Raw);

        if (d->totalFiles == 1) {
            d->ui.statusLabel->setText(tr("The file is currently being extracted."));
        }
        else {
            d->ui.statusLabel->setText(tr("1 of %1 files are currently being extracted.").arg(d->totalFiles));
        }

        emit sigProcess();
        d->SetProcessStatus(false);
    }

    void ExportDialog::onUpdateCurrentCount(const int& type, const int& count) {
        d->subwidgets[type].progBar->setValue(count);
    }

    void ExportDialog::onUpdateTotalCount(const int& type, const int& count) {
        if (count != 0) {
            d->subwidgets[type].progBar->setRange(0, count);
            d->subwidgets[type].progBar->setValue(0);
            d->subwidgets[type].progBar->setTextVisible(true);
        }
    }

    void ExportDialog::onProcessed(const QString& path) {
        Q_UNUSED(path)
        if (d->totalFiles > d->currentFileIndex) {
            d->currentFileIndex++;
        }

        if (d->totalFiles != 1) {
            d->ui.statusLabel->setText(tr("%1 of %2 files are currently being extracted.").arg(d->currentFileIndex).arg(d->totalFiles));
        }

        d->ClearProgressBars();
    }

    void ExportDialog::onProcessSucceeded(const QString& path, const int& type) {
        d->successCount++;

        const auto text = tr("Data export successful.\n%1\nType: %2").arg(path).arg(DataType::_from_integral(type)._to_string());
        const auto toastMessageBox = new TC::ToastMessageBox(this);
        toastMessageBox->Show(text);

        d->subwidgets[type].progBar->setStyleSheet(d->successProgressBarStyle);
    }

    void ExportDialog::onProcessFailed(const QString& path, const int& type, const QString& error) {
        d->failCount++;

        const auto text = tr("Data export failed.\n%1\nType: %2\nError: %3").arg(path).arg(DataType::_from_integral(type)._to_string()).arg(error);
        const auto toastMessageBox = new TC::ToastMessageBox(this);
        toastMessageBox->Show(text);

        d->subwidgets[type].progBar->setValue(d->subwidgets[type].progBar->maximum());
        d->subwidgets[type].progBar->setStyleSheet(d->failProgressBarStyle);
    }

    void ExportDialog::onProcessFinished() {
        const auto text = tr("Data export summary\n"
                             "Total data export processed: %1\n"
                             "Successfully exported: %2\n"
                             "Failed to export: %3").arg(d->successCount + d->failCount).arg(d->successCount).arg(d->failCount);

        const auto toastMessageBox = new TC::ToastMessageBox(this);
        toastMessageBox->Show(text, 6000);
        d->SetProcessStatus(true);
        close();
    }
}
