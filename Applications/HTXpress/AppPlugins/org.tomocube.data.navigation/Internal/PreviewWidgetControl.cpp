#include "PreviewWidgetControl.h"

#include "TCFMetaReader.h"

#include <ThumbnailController.h>
#include <ThumbnailManager.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    struct PreviewWidgetControl::Impl {
        TC::IO::TCFMetaReader* metaReader{ nullptr };
    };

    PreviewWidgetControl::PreviewWidgetControl() : d{new Impl} {
        d->metaReader = new TC::IO::TCFMetaReader;
    }

    PreviewWidgetControl::~PreviewWidgetControl() {
    }

    auto PreviewWidgetControl::Exist(const QString& path, bool& ht, bool& fl, bool& bf)->void {
        if (path.isEmpty()) return;
        if (nullptr == d->metaReader) return;

        auto meta = d->metaReader->Read(path);
        if (nullptr == meta) return;

        ht = meta->data.data2DMIP.exist;
        fl = meta->data.data2DFLMIP.exist;
        bf = meta->data.dataBF.exist;
    }

    auto PreviewWidgetControl::LoadHTThumbnail(const QString& path, QImage& image)->bool {
        if (path.isEmpty()) return false;

        auto manager = new Plugins::ThumbnailManager::ThumbnailManager;
        auto controller = Interactor::ThumbnailController(manager);

        bool cache = false;
        if (false == controller.LoadHTThumbnail(path, image, cache)) {
            return false;
        }

        return true;
    }

    auto PreviewWidgetControl::LoadFLThumbnail(const QString& path, QImage& image)->bool {
        if (path.isEmpty()) return false;

        auto manager = new Plugins::ThumbnailManager::ThumbnailManager;
        auto controller = Interactor::ThumbnailController(manager);

        bool cache = false;
        if (false == controller.LoadFLThumbnail(path, image, cache)) {
            return false;
        }

        return true;
    }

    auto PreviewWidgetControl::LoadBFThumbnail(const QString& path, QImage& image)->bool {
        if (path.isEmpty()) return false;

        auto manager = new Plugins::ThumbnailManager::ThumbnailManager;
        auto controller = Interactor::ThumbnailController(manager);

        bool cache = false;
        if (false == controller.LoadBFThumbnail(path, image, cache)) {
            return false;
        }

        return true;
    }
}
