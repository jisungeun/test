#include "UserUpdater.h"
#include "UserObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct UserUpdater::Impl {
        QList<UserObserver*> observers;
    };

    UserUpdater::UserUpdater() : d{ new Impl } {
    }

    UserUpdater::~UserUpdater() {
    }

    auto UserUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new UserUpdater() };
        return theInstance;
    }

    auto UserUpdater::ChangeUser(const AppEntity::UserID& user) -> void {
        for (auto observer : d->observers) {
            observer->ChangeUser(user);
        }
    }

    auto UserUpdater::UpdateUserList(const QList<AppEntity::UserID>& users) -> void {
        for (auto observer : d->observers) {
            observer->UpdateUserList(users);
        }
    }

    auto UserUpdater::Register(UserObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto UserUpdater::Deregister(UserObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}