#pragma once
#include <memory>
#include <QWidget>

#include "Experiment.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class SummaryPanel : public QWidget {
        Q_OBJECT

    public:
        explicit SummaryPanel(QWidget* parent = nullptr);
        ~SummaryPanel() override;

        auto Clear() -> void;

    protected slots:
        void onChangeCurrentExperiment(const QString& project, const QString& experiment);
        void onLoadExperiment(const AppEntity::Experiment::Pointer& experiment);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
