#define LOGGER_TAG "[VesselMapPanel]"
#include <TCLogger.h>
#include <MessageDialog.h>

#include "ProjectPanel.h"

#include <QListView>

#include "ui_ProjectPanel.h"
#include "ProjectPanelControl.h"

#include "UserObserver.h"
#include "ProjectObserver.h"
#include "ExperimentObserver.h"

#include "UIUtility.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct ProjectPanel::Impl {
        Ui::ProjectPanel ui;
        ProjectPanelControl control;

        UserObserver* userObserver{ nullptr };
        ProjectObserver* projectObserver{ nullptr };
        ExperimentObserver* experimentObserver{ nullptr };
    };

    ProjectPanel::ProjectPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.baseWidget->setObjectName("panel-light");
        d->ui.userComboBox->setObjectName("combobox-large");
        d->ui.projectComboBox->setObjectName("combobox-large");
        d->ui.experimentLineEdit->setObjectName("input-large");
        d->ui.experimentLineEdit->setStyleSheet("padding-left: 20px;");

        d->ui.userComboBox->clear();
        d->ui.userComboBox->setView(new QListView);
        d->ui.userComboBox->setInsertPolicy(QComboBox::NoInsert);
        d->ui.userComboBox->hide();

        d->ui.projectComboBox->clear();
        d->ui.projectComboBox->setView(new QListView);
        d->ui.projectComboBox->setInsertPolicy(QComboBox::NoInsert);
        d->ui.projectComboBox->model()->sort(0, Qt::AscendingOrder);

        d->userObserver = new UserObserver;
        d->projectObserver = new ProjectObserver;
        d->experimentObserver = new ExperimentObserver;

        connect(d->ui.userComboBox, SIGNAL(currentTextChanged(const QString&)), this, SLOT(onUserComboBoxTextChanged(const QString&)));
        connect(d->ui.projectComboBox, SIGNAL(currentTextChanged(const QString&)), this, SLOT(onProjectComboBoxTextChanged(const QString&)));

        connect(d->userObserver, SIGNAL(sigUserListUpdated(const QList<AppEntity::UserID>&)), this, SLOT(onUpdateUserList(const QList<AppEntity::UserID>&)));
        connect(d->userObserver, SIGNAL(sigUserChanged(const AppEntity::UserID&)), this, SLOT(onChangeUser(const AppEntity::UserID&)));
        connect(d->projectObserver, SIGNAL(sigProjectListUpdated(const QList<QString>&)), this, SLOT(onUpdateProjectList(const QList<QString>&)));
        connect(d->experimentObserver, SIGNAL(sigCurrentExperimentChanged(const QString&, const QString&)), this, SLOT(onChangeCurrentExperiment(const QString&, const QString&)));
    }

    ProjectPanel::~ProjectPanel() {
    }

    auto ProjectPanel::SetCurrentProject(const QString& project)->void {
        d->ui.projectComboBox->setCurrentText(project);
    }

    void ProjectPanel::onUserComboBoxTextChanged(const QString& user) {
        if (!d->control.ChangeUser(user)) {
            TC::MessageDialog::warning(this, tr("Change User"), tr("Failed to change user."));
        }

        if (d->control.GetProjects(user).isEmpty()) {
            d->ui.experimentLineEdit->clear();
        }
    }

    void ProjectPanel::onProjectComboBoxTextChanged(const QString& text) {
        d->control.ChangeCurrentProject(text);
    }

    void ProjectPanel::onUpdateUserList(const QList<AppEntity::UserID>& users) {
        d->ui.userComboBox->hide();

        auto profile = d->control.GetCurrentUserProfile();
        if (profile == +AppEntity::Profile::Administrator ||
            profile == +AppEntity::Profile::ServiceEngineer) {
            d->ui.userComboBox->show();
        }

        QSignalBlocker blocker(d->ui.userComboBox);
        d->ui.userComboBox->clear();
        d->ui.userComboBox->addItems(users);
        d->ui.userComboBox->setCurrentIndex(0);
    }

    void ProjectPanel::onChangeUser(const AppEntity::UserID& user) {
        d->ui.projectComboBox->clear();
        
        const auto projects = d->control.GetProjects(user);
        for(const auto& project : projects) {
            if (!project.isEmpty() && d->ui.projectComboBox->findText(project) < 0) {
                d->ui.projectComboBox->addItem(project);
            }
        }
    }

    void ProjectPanel::onUpdateProjectList(const QList<QString>& projects) {
        d->ui.projectComboBox->clear();

        for(const auto& project : projects) {
            if (project.isEmpty()) continue;

            auto exist = false;
            for(auto i = 0; i < d->ui.projectComboBox->count(); ++i) {
                if(d->ui.projectComboBox->itemText(i) == project) {
                    exist = true;
                    break;
                }
            }

            if(false == exist) {
                d->ui.projectComboBox->addItem(project);
            }
        }
    }

    void ProjectPanel::onChangeCurrentExperiment(const QString& project, const QString& experiment) {
        Q_UNUSED(project)
        d->ui.experimentLineEdit->setText(experiment);
    }
}

