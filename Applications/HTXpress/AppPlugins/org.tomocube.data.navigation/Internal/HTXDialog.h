#pragma once

#include <memory>
#include <QDialog>
#include <QMouseEvent>

namespace HTXpress::AppPlugins::Data::Navigation {
    class HTXDialog : public QDialog {
        Q_OBJECT
    public:
        explicit HTXDialog(QWidget* parent = nullptr);
        ~HTXDialog();

        auto SetTitle(const QString& title)->void;
        auto SetWidget(QWidget* widget)->void;

    protected:
        void mousePressEvent(QMouseEvent* event) override;
        void mouseMoveEvent(QMouseEvent* event) override;
        void mouseReleaseEvent(QMouseEvent* event) override;

    private slots:
        void onCloseButtonClicked();

        void onChildDestroyed();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}