#pragma once
#include <memory>
#include <QWidget>

#include <AcquisitionDataIndex.h>

#include "Experiment.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class ListPanel : public QWidget {
        Q_OBJECT

    public:
        explicit ListPanel(QWidget* parent = nullptr);
        ~ListPanel() override;

        auto Clear() -> void;

    signals:
        void sigViewImage(const QList<QString>& list);
        void sigExport(const QList<QString>& list);
        void sigCopy(const QList<QString>& list);
        void sigDelete(const QList<QString>& list);

    private slots:
        void onLoadedExperiment(const AppEntity::Experiment::Pointer& experiment);
        void onLoadExperimentData(const QString& user, const QString& project, const QString& experiment, const QString& specimen, const QString& well);

        void onDataAdded(const QString& fileFullPath);
        void onDataUpdated(const QString& fileFullPath);
        void onDataDeleted(const QString& fileFullPath);
        void onDataRootFolderDeleted(const QString& fileFullPath);

        void onSelectedDataIndex(const HTXpress::AppComponents::AcquisitionListPanel::AcquisitionDataIndex& index);

        void onViewButtonClicked();
        void onExportButtonClicked();
        void onCopyButtonClicked();
        void onDeleteButtonClicked();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
