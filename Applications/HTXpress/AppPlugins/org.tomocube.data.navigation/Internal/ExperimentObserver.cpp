#include "ExperimentObserver.h"
#include "ExperimentUpdater.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    ExperimentObserver::ExperimentObserver(QObject* parent) : QObject(parent) {
        ExperimentUpdater::GetInstance()->Register(this);
    }

    ExperimentObserver::~ExperimentObserver() {
        ExperimentUpdater::GetInstance()->Deregister(this);
    }

    auto ExperimentObserver::ChangeCurrentExperiment(const QString& project, const QString& experiment)->void {
        emit sigCurrentExperimentChanged(project, experiment);
    }

    auto ExperimentObserver::LoadExperiment(const AppEntity::Experiment::Pointer& experiment)->void {
        emit sigExperimentLoaded(experiment);
    }

    auto ExperimentObserver::LoadExperimentList(const QList<QString>& experiments)->void {
        emit sigExperimentListLoaded(experiments);
    }
}
