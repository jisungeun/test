#include "PreviewWidget.h"
#include "PreviewWidgetControl.h"


#include <QButtonGroup>
#include <QHBoxLayout>


#include <UIUtility.h>

#include <Define.h>
#include "ThumbnailWidget.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct PreviewWidget::Impl {
        PreviewWidgetControl control;

        QString path;

        QButtonGroup* buttonGroup{ nullptr };

        ImageView* view{ nullptr };

        auto SetType(const ImageType& type)->void;
    };

    auto PreviewWidget::Impl::SetType(const ImageType& type)->void {
        if(nullptr == view) return;
        if (path.isEmpty()) return;

        QImage image;
        switch (type) {
        case ImageType::HT: control.LoadHTThumbnail(path, image); break;
        case ImageType::FL: control.LoadFLThumbnail(path, image); break;
        case ImageType::BF: control.LoadBFThumbnail(path, image); break;
        }

        view->SetPixmap(QPixmap::fromImage(image));
    }

    PreviewWidget::PreviewWidget(QWidget* parent): QWidget(parent), d{ new Impl } {
        d->buttonGroup = new QButtonGroup;
        auto htButton = new QPushButton(ImageType::_from_index(ImageType::HT)._to_string());
        htButton->setFixedSize(46, 34);
        htButton->setCheckable(true);
        htButton->setEnabled(false);
        htButton->setObjectName("bt-light");

        auto flButton = new QPushButton(ImageType::_from_index(ImageType::FL)._to_string());
        flButton->setFixedSize(46, 34);
        flButton->setCheckable(true);
        flButton->setEnabled(false);
        flButton->setObjectName("bt-light");

        auto bfButton = new QPushButton(ImageType::_from_index(ImageType::BF)._to_string());
        bfButton->setFixedSize(46, 34);
        bfButton->setCheckable(true);
        bfButton->setEnabled(false);
        bfButton->setObjectName("bt-light");

        d->buttonGroup->addButton(htButton, ImageType::HT);
        d->buttonGroup->addButton(flButton, ImageType::FL);
        d->buttonGroup->addButton(bfButton, ImageType::BF);
        d->buttonGroup->setExclusive(true);

        auto buttonLayout = new QVBoxLayout;
        buttonLayout->addWidget(d->buttonGroup->button(ImageType::HT));
        buttonLayout->addWidget(d->buttonGroup->button(ImageType::FL));
        buttonLayout->addWidget(d->buttonGroup->button(ImageType::BF));
        buttonLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding));

        auto subLayout = new QHBoxLayout();
        subLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));
        subLayout->addLayout(buttonLayout);

        d->view = new ImageView();
        d->view->setLayout(subLayout);

        auto frameLayout = new QVBoxLayout();
        frameLayout->addWidget(d->view, Qt::AlignCenter);
        frameLayout->setContentsMargins(0, 0, 0, 0);

        auto frame = new QFrame;
        frame->setObjectName("frame");
        frame->setLayout(frameLayout);
        frame->setStyleSheet("#frame { border: none; background: #000000; }");

        auto layout = new QVBoxLayout();
        layout->addWidget(frame, Qt::AlignCenter);
        layout->setContentsMargins(0, 0, 0, 0);

        this->setLayout(layout);

        this->installEventFilter(this);

        connect(d->buttonGroup, SIGNAL(idClicked(int)), this, SLOT(onButtonGroupClicked(int)));
    }

    PreviewWidget::~PreviewWidget() = default;

    auto PreviewWidget::SetPath(const QString& path) ->void {
        if (path.isEmpty()) return;

        d->path = path;

        bool ht = false; bool fl = false; bool bf = false;
        d->control.Exist(d->path, ht, fl, bf);

        d->buttonGroup->button(ImageType::HT)->setEnabled(ht);
        d->buttonGroup->button(ImageType::FL)->setEnabled(fl);
        d->buttonGroup->button(ImageType::BF)->setEnabled(bf);

        auto type = ImageType::HT;
        if (ht) {
            type = ImageType::HT;
        }            
        else if (fl) {
            type = ImageType::FL;
        }
        else if (bf) {
            type = ImageType::BF;
        }

        d->buttonGroup->blockSignals(true);
        d->buttonGroup->button(type)->click();
        d->buttonGroup->blockSignals(false);

        onButtonGroupClicked(type);
    }

    auto PreviewWidget::SetTypeVisible(const bool& visible) const ->void {
        for (auto button : d->buttonGroup->buttons()) {
            if (button) button->setVisible(visible);
        }
    }
        
    auto PreviewWidget::eventFilter(QObject* object, QEvent* event) -> bool {
        Q_UNUSED(object)
        if (event->type() == QEvent::Paint) {
            if (d->view) d->view->KeepSquare();
            return false;
        }

        return false;
    }

    void PreviewWidget::onButtonGroupClicked(int id) {
        if (nullptr == d->view) return;
        if (d->path.isEmpty()) return;

        auto type = ImageType::_from_integral(id);
        d->SetType(type);
    }
}
