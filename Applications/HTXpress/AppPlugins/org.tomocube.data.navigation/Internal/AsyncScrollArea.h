#pragma once
#include <memory>
#include <QScrollArea>

namespace HTXpress::AppPlugins::Data::Navigation {
    class AsyncScrollArea : public QScrollArea {
        Q_OBJECT

    public:
        AsyncScrollArea(QWidget* parent = nullptr);
        ~AsyncScrollArea();

        auto RegisterWidget(QWidget* widget)->void;
        auto DergisterWidget(QWidget* widget)->void;
        auto DergisterAllWidgets()->void;

        auto GetWidgets() const ->QList<QWidget*>;
        auto GetWidget(const int& index) const ->QWidget*;
        auto GetIndex(QWidget* widget) const ->int;

        auto GetRenderWidgets() const ->QList<QWidget*>;
        auto GetRenderIndexes() const ->QList<int>;

        auto IsVisible(QWidget* widget) const ->bool;
        auto IsVisible(const int& index) const ->bool;

    signals:
        void sigWidgetClicked(int index);
        void sigWidgetDoubleClicked(int index);
        void sigUpdateRender(QList<int> indexes);
        
    private slots:
        void onScrollBarValueChanged(int value);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}