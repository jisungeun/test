﻿#pragma once
#include <memory>
#include <optional>

namespace HTXpress::AppPlugins::Data::Navigation {
    class ExperimentLoader {
    public:
        explicit ExperimentLoader(const QString& tcfFilePath);
        ~ExperimentLoader();

        [[nodiscard]] auto GetExperimentInDataFolder() const -> std::optional<AppEntity::Experiment::Pointer>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
