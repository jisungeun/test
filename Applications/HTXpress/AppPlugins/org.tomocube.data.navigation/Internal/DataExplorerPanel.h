#pragma once
#include <memory>
#include <QWidget>
#include <enum.h>

#include <QTreeWidgetItem>

#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::Data::Navigation {
    class DataExplorerPanel : public QWidget {
        Q_OBJECT

    public:
        explicit DataExplorerPanel(QWidget* parent = nullptr);
        ~DataExplorerPanel() override;

        auto SetCurrentExperiment(const QString& experiment)->void;

    private slots:
        void onChangeUser(const AppEntity::UserID& user);
        void onChangeCurrentProject(const QString& projectTitle);
        void onLoadedExperimentList(const QList<QString>& experiments);
        void onTreeCurrentItemChanged(const QString& experiment, const QString& specimen, const QString& well);
        void onExperimentDataScanned(const QString& user, const QString& project, const QString& experiment);
        void onDataAdded(const QString& fileFullPath);
        void onDataUpdated(const QString& fileFullPath);
        void onDataRootFolderDeleted(const QString& fileFullPath);
        void onInitialized();

    signals:
        void sigGotoExperimentSetup();
        void sigGotoExperimentPerform();
    
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}