#pragma once

#include <memory>
#include <QObject>

namespace HTXpress::AppPlugins::Data::Navigation {
    class DataObserver : public QObject {
        Q_OBJECT public:
        using Pointer = std::shared_ptr<DataObserver>;

        explicit DataObserver(QObject* parent = nullptr);
        ~DataObserver() override;

        auto ScannedExperimentData(const QString& user, const QString& project, const QString& experiment) -> void;
        auto AddData(const QString& fileFullPath) -> void;
        auto UpdateData(const QString& fileFullPath) -> void;
        auto DeleteData(const QString& fileFullPath) -> void;
        auto DeleteDataRootFolder(const QString& fileFullPath) -> void;

    signals:
        auto sigExperimentDataScanned(const QString& user, const QString& project, const QString& experiment) -> void;
        auto sigDataAdded(const QString& fileFullPath) -> void;
        auto sigDataUpdated(const QString& fileFullPath) -> void;
        auto sigDataDeleted(const QString& fileFullPath) -> void;
        auto sigDataRootFolderDeleted(const QString& fileFullPath) -> void;
    };
}
