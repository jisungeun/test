#pragma once

#include <memory>
#include <QObject>

#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::Data::Navigation{
    class UserObserver : public QObject {
        Q_OBJECT

    public:
        using Pointer = std::shared_ptr<UserObserver>;

        UserObserver(QObject* parent = nullptr);
        ~UserObserver();

        auto ChangeUser(const AppEntity::UserID& user)->void;
        auto UpdateUserList(const QList<AppEntity::UserID>& users)->void;

    signals:
        void sigUserChanged(const AppEntity::UserID& user);
        void sigUserListUpdated(const QList<AppEntity::UserID>& users);
    };
}