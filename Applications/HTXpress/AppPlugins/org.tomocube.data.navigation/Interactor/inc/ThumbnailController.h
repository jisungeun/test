#pragma once

#include "HTX_Data_Navigation_InteractorExport.h"

#include <IThumbnailManager.h>

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API ThumbnailController {
    public:
        ThumbnailController(UseCase::IThumbnailManager* manager);
        ~ThumbnailController();

        auto LoadHTThumbnail(const QString& path, QImage& image, bool& cache) const->bool;
        auto LoadFLThumbnail(const QString& path, QImage& image, bool& cache) const->bool;
        auto LoadBFThumbnail(const QString& path, QImage& image, bool& cache) const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}