#pragma once
#include <memory>

#include "InitializationPresenter.h"
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API InitializationController {
    public:
        InitializationController(InitializationPresenter* presenter);
        ~InitializationController();

        auto InitializeUI()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}