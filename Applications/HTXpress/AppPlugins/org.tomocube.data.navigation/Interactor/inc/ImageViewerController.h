#pragma once
#include <memory>

#include <QString>

#include <IImageViewerManager.h>

#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API ImageViewerController final {
    public:
        explicit ImageViewerController(UseCase::IImageViewerManager::Pointer manager);
        ~ImageViewerController();

        auto RunImageViewer(const QStringList& tcfList) -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
