#pragma once
#include <memory>

#include <IInitializationOutputPort.h>

#include "IInitializationView.h"
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API InitializationPresenter : public UseCase::IInitializationOutputPort {
    public:
        InitializationPresenter(IInitializationView* view);
        ~InitializationPresenter() override;

        auto InitializeUI() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}