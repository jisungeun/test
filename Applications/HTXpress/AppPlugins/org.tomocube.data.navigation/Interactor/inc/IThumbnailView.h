#pragma once

#include <QImage>

#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API IThumbnailView {
    public:
        IThumbnailView();
        ~IThumbnailView();

        virtual auto LoadedThumbnail(const QString& path, const QImage& image)->void = 0;
    };
}