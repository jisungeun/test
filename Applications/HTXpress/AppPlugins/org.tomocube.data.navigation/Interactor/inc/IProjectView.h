#pragma once

#include <QList>

#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API IProjectView {
    public:
        IProjectView();
        ~IProjectView();

        virtual auto UpdateProjectList(const QList<QString>& projects)->void = 0;

        virtual auto ChangeCurrentProject(const QString& projectTitle)->void = 0;
    };
}