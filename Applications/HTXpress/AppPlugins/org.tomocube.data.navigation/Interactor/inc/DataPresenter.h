#pragma once
#include <memory>

#include <IDataOutputPort.h>

#include "IDataView.h"
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API DataPresenter : public UseCase::IDataOutputPort {
    public:
        DataPresenter(IDataView* view);
        ~DataPresenter() override;

        auto ScannedExperimentData(const QString& user, const QString& project, const QString& experiment)->void override;
        auto AddData(const QString& fileFullPath)->void override;
        auto UpdateData(const QString& fileFullPath)->void override;
        auto DeleteData(const QString& fileFullPath)->void override;
        auto DeleteDataRootFolder(const QString& fileFullPath) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}