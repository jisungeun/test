#pragma once

#include <QList>

#include <AppEntityDefines.h>

#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API IUserView {
    public:
        IUserView();
        virtual ~IUserView();

        virtual auto ChangeUser(const AppEntity::UserID& user)->void = 0;
        virtual auto UpdateUserList(const QList<AppEntity::UserID>& users)->void = 0;
    };
}