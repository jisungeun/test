#pragma once
#include <memory>

#include "ExperimentPresenter.h"
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API ExperimentController {
    public:
        ExperimentController(ExperimentPresenter* presenter);
        ~ExperimentController();

        auto LoadExperiment(const QString& experiment) const ->bool;
        auto LoadExperimentList() const ->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}