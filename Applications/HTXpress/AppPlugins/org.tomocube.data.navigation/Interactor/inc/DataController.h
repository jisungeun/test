#pragma once
#include <memory>

#include <AppEntityDefines.h>

#include "DataPresenter.h"
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API DataController {
    public:
        DataController(DataPresenter* presenter);
        ~DataController();

        auto ScanExperimentData(const QString& experiment)->bool;

        auto InstallDataScannerOutputPort(DataPresenter* port)->bool;
        auto InstallDataMonitorOutputPort(DataPresenter* port)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}