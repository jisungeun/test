#pragma once
#include <memory>

#include "UserPresenter.h"
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API UserController {
    public:
        UserController(UserPresenter* presenter);
        ~UserController();

        auto LoadUserList() -> bool;
        auto ChangeUser(const AppEntity::UserID& user) -> bool;
        auto CheckPassword(const QString& password) const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}