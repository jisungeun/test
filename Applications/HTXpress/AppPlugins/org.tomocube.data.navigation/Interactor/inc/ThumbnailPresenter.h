#pragma once

#include <Experiment.h>
#include <IThumbnailOutputPort.h>

#include "IThumbnailView.h"

#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API ThumbnailPresenter : public UseCase::IThumbnailOutputPort {
    public:
        ThumbnailPresenter(IThumbnailView* view);
        ~ThumbnailPresenter();

        auto LoadedThumbnail(const QString& path, const QImage& image) const -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
