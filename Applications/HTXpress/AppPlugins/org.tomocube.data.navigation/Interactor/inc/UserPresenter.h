#pragma once
#include <memory>

#include <IUserOutputPort.h>

#include "IUserView.h"
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API UserPresenter : public UseCase::IUserOutputPort {
    public:
        UserPresenter(IUserView* view);
        ~UserPresenter() override;

        auto ChangeUser(const AppEntity::UserID& user) -> void override;
        auto UpdateUserList(const QList<AppEntity::UserID>& users) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}