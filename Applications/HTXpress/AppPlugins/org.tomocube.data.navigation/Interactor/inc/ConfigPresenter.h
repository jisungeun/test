#pragma once
#include <memory>

#include <IConfigOutputPort.h>
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API ConfigPresenter : public UseCase::IConfigOutputPort {
    public:
        ConfigPresenter();
        ~ConfigPresenter() override;

        auto SetAcqInfo(const int& wellIndex, const AppEntity::Position& position) -> void override;
        auto GetAcqInfo(int& wellIndex, AppEntity::Position& position) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}