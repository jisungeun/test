#pragma once

#include <QList>

#include "Experiment.h"
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API IExperimentView {
    public:
        IExperimentView();
        ~IExperimentView();

        virtual auto ChangeCurrentExperiment(const QString& project, const QString& experiment)->void = 0;
        virtual auto LoadExperiment(const AppEntity::Experiment::Pointer& experiment) const->void = 0;
        virtual auto LoadExperimentList(const QList<QString>& experiments)->void = 0;
    };
}