#pragma once

#include <QList>

#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API IInitializationView {
    public:
        IInitializationView();
        ~IInitializationView();

        virtual auto InitializeUI()->void = 0;
    };
}