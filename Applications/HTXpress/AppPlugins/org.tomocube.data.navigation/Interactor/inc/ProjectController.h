#pragma once
#include <memory>

#include <AppEntityDefines.h>

#include "ProjectPresenter.h"
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API ProjectController {
    public:
        ProjectController(ProjectPresenter* presenter);
        ~ProjectController();

        auto LoadProjectList(const AppEntity::UserID& id)->bool;

        auto ChangeCurrentProject(const QString& projectTitle)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}