#pragma once

#include <QString>

#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API IDataView {
    public:
        IDataView();
        virtual ~IDataView();

        virtual auto ScannedExperimentData(const QString& user, const QString& project, const QString& experiment) -> void = 0;
        virtual auto AddData(const QString& fileFullPath) -> void = 0;
        virtual auto UpdateData(const QString& fileFullPath) -> void = 0;
        virtual auto DeleteData(const QString& fileFullPath) -> void = 0;
        virtual auto DeleteDataRootFolder(const QString& fileFullPath) -> void = 0;
    };
}
