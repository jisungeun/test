#pragma once
#include <memory>

#include <IExperimentOutputPort.h>

#include "IExperimentView.h"
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API ExperimentPresenter : public UseCase::IExperimentOutputPort {
    public:
        ExperimentPresenter(IExperimentView* view);
        ~ExperimentPresenter() override;

        auto LoadExperiment(const AppEntity::Experiment::Pointer& data) const -> void override;
        auto LoadExperimentList(const QList<QString>& experiments)->void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}