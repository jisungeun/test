#pragma once
#include <memory>

#include "ConfigPresenter.h"
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API ConfigController {
    public:
        ConfigController(ConfigPresenter* presenter);
        ~ConfigController();

        auto LoadAcuInfo(const QString& path)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}