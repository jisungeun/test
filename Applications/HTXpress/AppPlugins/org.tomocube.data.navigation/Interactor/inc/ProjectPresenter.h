#pragma once
#include <memory>

#include <IProjectOutputPort.h>

#include "IProjectView.h"
#include "HTX_Data_Navigation_InteractorExport.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    class HTX_Data_Navigation_Interactor_API ProjectPresenter : public UseCase::IProjectOutputPort {
    public:
        ProjectPresenter(IProjectView* view);
        ~ProjectPresenter() override;

        auto UpdateProjectList(QList<QString> projects)->void override;

        auto ChangeCurrentProject(const QString& projectTitle)->void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}