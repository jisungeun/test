#include "DataController.h"

#include <ScanData.h>
#include <InstallDataScanner.h>
#include <InstallDataMonitor.h>

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct DataController::Impl {
        DataPresenter* presenter{ nullptr };
    };

    DataController::DataController(DataPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    DataController::~DataController() {
    }

    auto DataController::ScanExperimentData(const QString& experiment)->bool {
        auto usecase = UseCase::ScanData(d->presenter);
        usecase.SetExperiment(experiment);
        return usecase.Request();
    }

    auto DataController::InstallDataScannerOutputPort(DataPresenter * port)->bool {
        if (nullptr == port)
            return false;

        auto usecase = UseCase::InstallDataScanner();
        usecase.SetDataOutputPort(port);
        return usecase.Request();
    }

    auto DataController::InstallDataMonitorOutputPort(DataPresenter* port)->bool {
        if (nullptr == port)
            return false;

        auto usecase = UseCase::InstallDataMonitor();
        usecase.SetDataOutputPort(port);
        return usecase.Request();
    }
}
