#include "InitializationController.h"

#include <InitializeUI.h>

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct InitializationController::Impl {
        InitializationPresenter* presenter{ nullptr };
    };

    InitializationController::InitializationController(InitializationPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    InitializationController::~InitializationController() {
    }

    auto InitializationController::InitializeUI()->bool {
        auto usecase = UseCase::InitializeUI(d->presenter);
        return usecase.Request();
    }

}
