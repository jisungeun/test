#include "ProjectController.h"

#include <LoadProjectList.h>
#include <ChangeCurrentProject.h>

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct ProjectController::Impl {
        ProjectPresenter* presenter{ nullptr };
    };

    ProjectController::ProjectController(ProjectPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    ProjectController::~ProjectController() {
    }

    auto ProjectController::LoadProjectList(const AppEntity::UserID& id)->bool {
        auto usecase = UseCase::LoadProjectList(d->presenter);
        usecase.SetUserID(id);
        return usecase.Request();
    }

    auto ProjectController::ChangeCurrentProject(const QString& projectTitle)->bool {
        auto usecase = UseCase::ChangeCurrentProject(d->presenter);
        usecase.SetCurrentProject(projectTitle);
        return usecase.Request();
    }
}
