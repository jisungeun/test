#include "ThumbnailPresenter.h"

namespace  HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct ThumbnailPresenter::Impl {
        IThumbnailView* view{ nullptr };
    };

    ThumbnailPresenter::ThumbnailPresenter(IThumbnailView* view) : d{ new Impl } {
        d->view = view;
    }

    ThumbnailPresenter::~ThumbnailPresenter() {

    }

    auto ThumbnailPresenter::LoadedThumbnail(const QString& path, const QImage& image) const -> void {
        if (nullptr == d->view) return;
        if (path.isEmpty()) return;
        d->view->LoadedThumbnail(path, image);
    }
}
