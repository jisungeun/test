#include "UserPresenter.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct UserPresenter::Impl {
        IUserView* view{ nullptr };
    };

    UserPresenter::UserPresenter(IUserView* view) : UseCase::IUserOutputPort(), d{new Impl} {
        d->view = view;
    }

    UserPresenter::~UserPresenter() {
    }

    auto UserPresenter::ChangeUser(const AppEntity::UserID& user) -> void {
        if (d->view == nullptr) return;
        d->view->ChangeUser(user);
    }

    auto UserPresenter::UpdateUserList(const QList<AppEntity::UserID>& users) -> void {
        if (d->view == nullptr) return;
        d->view->UpdateUserList(users);
    }

}