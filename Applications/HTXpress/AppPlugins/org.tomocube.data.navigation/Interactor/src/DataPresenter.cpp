#include "DataPresenter.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct DataPresenter::Impl {
        IDataView* view{ nullptr };
    };

    DataPresenter::DataPresenter(IDataView* view) : UseCase::IDataOutputPort(), d{new Impl} {
        d->view = view;
    }

    DataPresenter::~DataPresenter() {
    }

    auto DataPresenter::ScannedExperimentData(const QString& user, const QString& project, const QString& experiment)->void {
        if(user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;
        if (nullptr == d->view) return;

        d->view->ScannedExperimentData(user, project, experiment);
    }

    auto DataPresenter::AddData(const QString& fileFullPath)->void {
        if (fileFullPath.isEmpty()) return;
        if (nullptr == d->view) return;

        d->view->AddData(fileFullPath);
    }

    auto DataPresenter::UpdateData(const QString& fileFullPath)->void {
        if (fileFullPath.isEmpty()) return;
        if (nullptr == d->view) return;

        d->view->UpdateData(fileFullPath);
    }

    auto DataPresenter::DeleteData(const QString& fileFullPath)->void {
        if (fileFullPath.isEmpty()) return;
        if (nullptr == d->view) return;

        d->view->DeleteData(fileFullPath);
    }

    auto DataPresenter::DeleteDataRootFolder(const QString& fileFullPath) -> void {
        if (fileFullPath.isEmpty()) return;
        if (nullptr == d->view) return;

        d->view->DeleteDataRootFolder(fileFullPath);
    }
}
