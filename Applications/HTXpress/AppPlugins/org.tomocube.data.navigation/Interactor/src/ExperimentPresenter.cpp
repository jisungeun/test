#include "ExperimentPresenter.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct ExperimentPresenter::Impl {
        IExperimentView* view{ nullptr };
    };

    ExperimentPresenter::ExperimentPresenter(IExperimentView* view) : UseCase::IExperimentOutputPort(), d{new Impl} {
        d->view = view;
    }

    ExperimentPresenter::~ExperimentPresenter() {
    }

    auto ExperimentPresenter::LoadExperiment(const AppEntity::Experiment::Pointer& data) const -> void {
        if (d->view) {
            d->view->LoadExperiment(data);
        }
    }

    auto ExperimentPresenter::LoadExperimentList(const QList<QString>& experiments)->void {
        if (d->view) {
            d->view->LoadExperimentList(experiments);
        }
    }
}