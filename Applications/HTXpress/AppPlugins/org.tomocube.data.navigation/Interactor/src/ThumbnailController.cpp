#include "ThumbnailController.h"

#include <LoadHTThumbnail.h>
#include <LoadFLThumbnail.h>
#include <LoadBFThumbnail.h>

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct ThumbnailController::Impl {
        UseCase::IThumbnailManager* manager{ nullptr };
    };

    ThumbnailController::ThumbnailController(UseCase::IThumbnailManager* manager) : d{ new Impl } {
        d->manager = manager;
    }

    ThumbnailController::~ThumbnailController() {
    }

    auto ThumbnailController::LoadHTThumbnail(const QString& path, QImage& image, bool& cache) const->bool {
        if (path.isEmpty()) return false;

        auto usecase = UseCase::LoadHTThumbnail(d->manager);
        usecase.SetNormalLogEnabled(false);
        usecase.SetPath(path);

        if (false == usecase.Request()) return false;
        image = usecase.GetImage();
        cache = usecase.IsThumbnail();

        return true;
    }

    auto ThumbnailController::LoadFLThumbnail(const QString& path, QImage& image, bool& cache) const->bool {
        if (path.isEmpty()) return false;

        auto usecase = UseCase::LoadFLThumbnail(d->manager);
        usecase.SetNormalLogEnabled(false);
        usecase.SetPath(path);
        
        if (false == usecase.Request()) return false;
        image = usecase.GetImage();
        cache = usecase.IsThumbnail();

        return true;
    }

    auto ThumbnailController::LoadBFThumbnail(const QString& path, QImage& image, bool& cache) const->bool {
        if (path.isEmpty()) return false;

        auto usecase = UseCase::LoadBFThumbnail(d->manager);
        usecase.SetNormalLogEnabled(false);
        usecase.SetPath(path);
        
        if (false == usecase.Request()) return false;
        image = usecase.GetImage();
        cache = usecase.IsThumbnail();

        return true;
    }
}
