#include "ConfigPresenter.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct ConfigPresenter::Impl {
        int wellIndex = -1;
        AppEntity::Position position;
    };

    ConfigPresenter::ConfigPresenter() : UseCase::IConfigOutputPort(), d{new Impl} {
    }

    ConfigPresenter::~ConfigPresenter() {
    }

    auto ConfigPresenter::SetAcqInfo(const int& wellIndex, const AppEntity::Position& position) -> void {
        d->wellIndex = wellIndex;
        d->position = position;
    }

    auto ConfigPresenter::GetAcqInfo(int& wellIndex, AppEntity::Position& position) -> void {
        wellIndex = d->wellIndex;
        position = d->position;
    }
}