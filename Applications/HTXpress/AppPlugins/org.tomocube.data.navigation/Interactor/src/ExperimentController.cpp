#include "ExperimentController.h"

#include <LoadExperiment.h>
#include <LoadExperimentList.h>

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct ExperimentController::Impl {
        ExperimentPresenter* presenter{ nullptr };
    };

    ExperimentController::ExperimentController(ExperimentPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    ExperimentController::~ExperimentController() {
    }

    auto ExperimentController::LoadExperiment(const QString& experiment) const ->bool {
        if (experiment.isEmpty()) return false;

        auto usecase = UseCase::LoadExperiment(d->presenter);
        usecase.SetArgument(experiment);
        return usecase.Request();
    }

    auto ExperimentController::LoadExperimentList() const ->bool {
        auto usecase = UseCase::LoadExperimentList(d->presenter);
        return usecase.Request();
    }
}