#include "UserController.h"

#include <LoadUserProjectList.h>
#include <ChangeUser.h>
#include <CheckPassword.h>

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct UserController::Impl {
        UserPresenter* presenter{ nullptr };
    };

    UserController::UserController(UserPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    UserController::~UserController() {
    }

    auto UserController::LoadUserList() -> bool {
        auto usecase = UseCase::LoadUserProjectList(d->presenter);
        return usecase.Request();
    }

    auto UserController::ChangeUser(const AppEntity::UserID& user) -> bool {
        auto usecase = UseCase::ChangeUser(d->presenter);
        usecase.SetUser(user);
        return usecase.Request();
    }

    auto UserController::CheckPassword(const QString& password) const -> bool {
        auto usecase = UseCase::CheckPassword(d->presenter);
        usecase.SetPassword(password);
        if(!usecase.Request()) return false;
        return usecase.Check();
    }
}
