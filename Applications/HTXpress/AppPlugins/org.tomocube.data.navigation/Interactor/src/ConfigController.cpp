#include "ConfigController.h"

#include <LoadConfig.h>

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct ConfigController::Impl {
        ConfigPresenter* presenter{ nullptr };
    };

    ConfigController::ConfigController(ConfigPresenter* presenter) : d{ new Impl } {
        d->presenter = presenter;
    }

    ConfigController::~ConfigController() {
    }

    auto ConfigController::LoadAcuInfo(const QString& path)->bool {
        if (path.isEmpty()) return false;

        auto usecase = UseCase::LoadConfig(d->presenter);
        usecase.SetPath(path);
        return usecase.Request();
    }
}