#include <RunImageViewer.h>

#include "ImageViewerController.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct ImageViewerController::Impl {
        UseCase::IImageViewerManager::Pointer imageViewerManager{};
    };

    ImageViewerController::ImageViewerController(UseCase::IImageViewerManager::Pointer manager) : d{std::make_unique<Impl>()} {
        d->imageViewerManager = std::move(manager);
    }

    ImageViewerController::~ImageViewerController() {
    }

    auto ImageViewerController::RunImageViewer(const QStringList& tcfList) -> bool {
        auto usecase = UseCase::RunImageViewer(d->imageViewerManager);
        usecase.SetTcfList(tcfList);
        return usecase.Request();
    }
}
