#include "ProjectPresenter.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct ProjectPresenter::Impl {
        IProjectView* view{ nullptr };
    };

    ProjectPresenter::ProjectPresenter(IProjectView* view) : UseCase::IProjectOutputPort(), d{new Impl} {
        d->view = view;
    }

    ProjectPresenter::~ProjectPresenter() {
    }

    auto ProjectPresenter::UpdateProjectList(QList<QString> projects)->void {
        d->view->UpdateProjectList(projects);
    }

    auto ProjectPresenter::ChangeCurrentProject(const QString& projectTitle)->void {
        d->view->ChangeCurrentProject(projectTitle);
    }
}