#include "InitializationPresenter.h"

namespace HTXpress::AppPlugins::Data::Navigation::Interactor {
    struct InitializationPresenter::Impl {
        IInitializationView* view{ nullptr };
    };

    InitializationPresenter::InitializationPresenter(IInitializationView* view) : UseCase::IInitializationOutputPort(), d{new Impl} {
        d->view = view;
    }

    InitializationPresenter::~InitializationPresenter() {
    }

    auto InitializationPresenter::InitializeUI()->void {
        d->view->InitializeUI();
    }
    
}