#define LOGGER_TAG "[MainWindow]"

#include <QTimer>

#include <TCLogger.h>
#include <AppEvent.h>
#include <MessageDialog.h>

#include "MainWindowControl.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <DataManager.h>
#include <ProjectReaderPort.h>
#include <UserManagerPlugin.h>

#include "Internal/ExportDialog.h"
#include "Internal/CopyDialog.h"
#include "Internal/DeleteDialog.h"
#include "Internal/ExperimentObserver.h"
#include "Internal/ProjectObserver.h"
#include "Internal/UserObserver.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    struct MainWindow::Impl {
        explicit Impl(Self* self) : self(self){}
        Self* self{};
        Ui::MainWindow ui;
        MainWindowControl control;

        ExperimentObserver* experimentObserver{};
        ProjectObserver* projectObserver{};
        UserObserver* userObserver{};

        std::shared_ptr<Plugins::DataManager::DataManager> dataManager{ nullptr };
        std::shared_ptr<Plugins::ProjectIO::ProjectReaderPort> projectReader{ nullptr };
        std::shared_ptr<Plugins::UserManager::Plugin> userManager{ nullptr };

        QString currentUser{};
        QString currentProject{};
        QString currentExperiment{};

        enum PageIndex {
            Summary = 0,
            Gallery = 1,
            List = 2,
        };

        auto ClearTabSubWidgets() -> void;
        auto ChangePluginApp(const QString& fullName, const QString& appName) -> void;
    };

    MainWindow::MainWindow(QWidget* parent) : IMainWindowHTX("Data Navigation", "DataNavigation", parent), d{std::make_unique<Impl>(this)} {
        d->ui.setupUi(this);
        d->ui.baseWidget->setObjectName("panel");

        subscribeEvent("TabChange");

        // init
        d->dataManager.reset(new Plugins::DataManager::DataManager());
        d->projectReader.reset(new Plugins::ProjectIO::ProjectReaderPort());
        d->userManager.reset(new Plugins::UserManager::Plugin());

        d->experimentObserver = new ExperimentObserver(this);
        d->projectObserver = new ProjectObserver(this);
        d->userObserver = new UserObserver(this);

        connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnHandleEvent(ctkEvent)));
        
        connect(d->ui.dataExplorerPanel, SIGNAL(sigGotoExperimentSetup()), this, SLOT(onOpenExperimentSetup()));
        connect(d->ui.dataExplorerPanel, SIGNAL(sigGotoExperimentPerform()), this, SLOT(onOpenExperimentPerform()));

        connect(d->ui.galleryPanel, SIGNAL(sigViewImage(const QList<QString>&)), this, SLOT(onViewImage(const QList<QString>&)));
        connect(d->ui.listPanel, SIGNAL(sigViewImage(const QList<QString>&)), this, SLOT(onViewImage(const QList<QString>&)));

        connect(d->ui.galleryPanel, SIGNAL(sigExport(const QList<QString>&)), this, SLOT(onExport(const QList<QString>&)));
        connect(d->ui.listPanel, SIGNAL(sigExport(const QList<QString>&)), this, SLOT(onExport(const QList<QString>&)));
        connect(d->ui.galleryPanel, SIGNAL(sigCopy(const QList<QString>&)), this, SLOT(onCopy(const QList<QString>&)));
        connect(d->ui.listPanel, SIGNAL(sigCopy(const QList<QString>&)), this, SLOT(onCopy(const QList<QString>&)));
        connect(d->ui.galleryPanel, SIGNAL(sigDelete(const QList<QString>&)), this, SLOT(onDelete(const QList<QString>&)));
        connect(d->ui.listPanel, SIGNAL(sigDelete(const QList<QString>&)), this, SLOT(onDelete(const QList<QString>&)));

        connect(d->ui.tabWidget, &QTabWidget::currentChanged, this, &MainWindow::onTabCurrentChanged);

        connect(d->experimentObserver, &ExperimentObserver::sigCurrentExperimentChanged, this, &MainWindow::onExperimentChanged);
        connect(d->projectObserver, &ProjectObserver::sigCurrentProjectChanged, this, &MainWindow::onProjectChanged);
        connect(d->userObserver, &UserObserver::sigUserChanged, this, &MainWindow::onUserChanged);

        d->ui.tabWidget->setCurrentIndex(Impl::PageIndex::Summary);
    }
    
    MainWindow::~MainWindow() {        
    }

    auto MainWindow::Execute(const QVariantMap& params) -> bool {
        const QString keyProject = "Project";
        const QString keyExperiment = "Experiment";

        d->control.InitializeUI();

        d->control.InstallDataScannerOutputPort();
        d->control.InstallDataMonitorOutputPort();

        if (d->control.LoadUserList()) {
            d->control.SetLoggedInUser();
            if (params.contains(keyProject)) d->ui.projectPanel->SetCurrentProject(params[keyProject].toString());
            if (params.contains(keyExperiment)) d->ui.dataExplorerPanel->SetCurrentExperiment(params[keyExperiment].toString());
        } else {
            TC::MessageDialog::warning(this, tr("Initialization"), tr("Failed to get logged in user information."));
        }

        return true;
    }

    auto MainWindow::TryActivate() -> bool {
        return true;
    }

    auto MainWindow::TryDeactivate() -> bool {
        return true;
    }

    auto MainWindow::IsActivate() -> bool {
        return true;
    }

    auto MainWindow::GetMetaInfo()->QVariantMap {
        return QVariantMap();
    }

    void MainWindow::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
    }

    void MainWindow::OnHandleEvent(const ctkEvent& ctkEvent) {
        Q_UNUSED(ctkEvent)
    }

    void MainWindow::onOpenExperimentSetup() {
        d->ChangePluginApp("org.tomocube.experiment.setup", "Experiment Setup");
    }

    void MainWindow::onOpenExperimentPerform() {
        QString fullName;
        QString appName;

        if (d->control.IsTimelapseRunning()) {
            fullName = "org.tomocube.experiment.timelapse";
            appName = "Experiment Timelapse";
        }
        else {
            fullName = "org.tomocube.experiment.perform";
            appName = "Experiment Perform";
        }
        
        d->ChangePluginApp(fullName, appName);
    }

    void MainWindow::onViewImage(const QList<QString>& list) {
        if (list.isEmpty()) {
            QLOG_ERROR() << "TCF list is empty";
            return;
        }

        if (!d->control.RunImageViewer(list)) {
            QLOG_ERROR() << "Failed to run image viewer";
        }
    }

    void MainWindow::onExport(const QList<QString>& list) {
        ExportDialog widget;
        widget.SetList(list);
        widget.SetTitle("Export Option");
        widget.exec();
    }

    void MainWindow::onCopy(const QList<QString>& list) {
        CopyDialog widget;
        widget.SetList(list);
        widget.SetTitle("Copy Data");
        widget.exec();
    }

    void MainWindow::onDelete(const QList<QString>& list) {
        DeleteDialog widget;
        widget.SetList(list);
        widget.SetTitle("Delete Data");
        widget.exec();
    }

    void MainWindow::onTabCurrentChanged(int index) {
        if (1 == index) {
            if(!d->currentUser.isEmpty() &&
               !d->currentExperiment.isEmpty() &&
               !d->currentProject.isEmpty()) {
                d->ui.galleryPanel->LoadThumbnails(d->currentUser, d->currentProject, d->currentExperiment);
            }
        }
    }

    void MainWindow::onExperimentChanged(const QString& project, const QString& experiment) {
        d->currentProject = project;
        d->currentExperiment = experiment;
        d->ui.tabWidget->setCurrentIndex(Impl::PageIndex::Summary);

        if(experiment.isEmpty()) {
            d->ClearTabSubWidgets();
        }
    }

    void MainWindow::onProjectChanged(const QString& project) {
        if(project.isEmpty()) {
            d->ClearTabSubWidgets();
        }
    }

    void MainWindow::onUserChanged(const AppEntity::UserID& user) {
        d->currentUser = user;

        if(user.isEmpty()) {
            d->ClearTabSubWidgets();
        }
    }

    auto MainWindow::Impl::ClearTabSubWidgets() -> void {
        ui.summaryPanel->Clear();
        ui.galleryPanel->Clear();
        ui.listPanel->Clear();
    }

    auto MainWindow::Impl::ChangePluginApp(const QString& fullName, const QString& appName) -> void {
        ClearTabSubWidgets();

        TC::Framework::AppEvent appEvt(TC::Framework::AppTypeEnum::APP_WITH_ARGS);

        appEvt.setFullName(fullName);
        appEvt.setAppName(appName);

        self->publishSignal(appEvt, "Data::Navigation");
    }
}
