#pragma once
#include <memory>
#include <IMainWindowHTX.h>

#include "AppEntityDefines.h"

namespace HTXpress::AppPlugins::Data::Navigation {
    class MainWindow : public AppComponents::Framework::IMainWindowHTX {
        Q_OBJECT
    public:
        using Self = MainWindow;

        explicit MainWindow(QWidget* parent = nullptr);
        virtual ~MainWindow();
                
        auto Execute(const QVariantMap& params) -> bool override;

    protected:
        auto TryActivate() -> bool override;
        auto TryDeactivate() -> bool override;
        auto IsActivate() -> bool override;
        auto GetMetaInfo()->QVariantMap override;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    private slots:
        void OnHandleEvent(const ctkEvent& ctkEvent);
        
        void onOpenExperimentSetup();
        void onOpenExperimentPerform();

        void onViewImage(const QList<QString>& list);
        void onExport(const QList<QString>& list);
        void onCopy(const QList<QString>& list);
        void onDelete(const QList<QString>& list);

        void onTabCurrentChanged(int index);
        void onExperimentChanged(const QString& project, const QString& experiment);
        void onProjectChanged(const QString& project);
        void onUserChanged(const AppEntity::UserID& user);
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
