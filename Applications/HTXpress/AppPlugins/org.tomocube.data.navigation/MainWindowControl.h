#pragma once
#include <memory>

namespace HTXpress::AppPlugins::Data::Navigation {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

        auto InitializeUI()->void;

        auto LoadUserList()->bool;
        auto SetLoggedInUser()->bool;
        auto LoadProjectList()->void;

        auto InstallDataScannerOutputPort()->void;
        auto InstallDataMonitorOutputPort()->void;

        auto RunImageViewer(const QStringList& tcfList)->bool;

        auto IsTimelapseRunning()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}