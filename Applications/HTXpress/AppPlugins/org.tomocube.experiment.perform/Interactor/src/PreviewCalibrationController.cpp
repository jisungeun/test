#include <GetBFExposureIntensityRange.h>
#include <GetBFGainIntensityRange.h>

#include "PreviewCalibrationController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct PreviewCalibrationController::Impl {
        InstrumentPresenter* presenter{ nullptr };
    };

    PreviewCalibrationController::PreviewCalibrationController(InstrumentPresenter* presenter) : d{ new Impl } {
        d->presenter = presenter;
    }

    PreviewCalibrationController::~PreviewCalibrationController() {
    }

    auto PreviewCalibrationController::MeasureExposure(ValueList& values, double& coeffP, double& coeffQ) -> bool {
        auto usecase = UseCase::GetBFExposureIntensityRange(d->presenter);
        if (!usecase.Request()) return false;

        values = usecase.GetValues();
        std::tie(coeffP, coeffQ) = usecase.GetCoefficients();

        return true;
    }

    auto PreviewCalibrationController::MeasureGain(ValueList& values, double& coeffA, double& coeffB) -> bool {
        auto usecase = UseCase::GetBFGainIntensityRange(d->presenter);
        if (!usecase.Request()) return false;

        values = usecase.GetValues();
        std::tie(coeffA, coeffB) = usecase.GetCoefficients();

        return true;
    }
}