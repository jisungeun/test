#include "VesselPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct VesselPresenter::Impl {
        IVesselView* view{ nullptr };
    };

    VesselPresenter::VesselPresenter(IVesselView* view) : d{new Impl} {
        d->view = view;
    }

    VesselPresenter::~VesselPresenter() {
    }

    auto VesselPresenter::UpdateSelected(AppEntity::WellIndex wellIdx) -> void {
        if(d->view) {
            d->view->UpdateCurrentWell(wellIdx);
        }
    }

    auto VesselPresenter::UpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM) -> void {
        if(d->view) {
            d->view->UpdateSafeMovingRange(axis, minMM, maxMM);
        }
    }
}
