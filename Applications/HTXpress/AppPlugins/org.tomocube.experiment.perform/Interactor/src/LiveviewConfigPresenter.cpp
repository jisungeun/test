#include "LiveviewConfigPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct LiveviewConfigPresenter::Impl {
        ILiveviewConfigView* view{ nullptr };
    };

    LiveviewConfigPresenter::LiveviewConfigPresenter(ILiveviewConfigView* view) : UseCase::ILiveviewConfigOutputPort(), d{new Impl} {
        d->view = view;
    }

    LiveviewConfigPresenter::~LiveviewConfigPresenter() {
    }

    auto LiveviewConfigPresenter::UpdateLiveConfig(const uint32_t intensity, const uint32_t exposure, const double gain) -> void {
        if (d->view) {
            d->view->UpdateLiveConfig(intensity, exposure, gain);
        }
    }

    auto LiveviewConfigPresenter::UpdateAcquisitionConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain) -> void {
        if (d->view) {
            d->view->UpdateAcquisitionConfig(mode, intensity, exposure, gain);
        }
    }

    auto LiveviewConfigPresenter::UpdateImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode) -> void {
        if(d->view) {
            d->view->UpdateImagingSettingMode(imagingSettingMode);
        }
    }
}
