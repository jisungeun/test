#include "AcquisitionDataController.h"

#include <InstallDataMonitor.h>
#include <InstallDataScanner.h>
#include <ScanAcquisitionData.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct AcquisitionDataController::Impl {
        AcquisitionDataPresenter* presenter{ nullptr };
    };

    AcquisitionDataController::AcquisitionDataController(AcquisitionDataPresenter* presenter) : d{ new Impl } {
        d->presenter = presenter;
    }
    
    AcquisitionDataController::~AcquisitionDataController() {
    }

    auto AcquisitionDataController::InstallDataMonitor() -> bool {
        auto usecase = UseCase::InstallDataMonitor();
        usecase.SetDataOutputPort(d->presenter);
        return usecase.Request();
    }

    auto AcquisitionDataController::InstallDataScanner() -> bool {
        auto usecase = UseCase::InstallDataScanner();
        usecase.SetDataOutputPort(d->presenter);
        return usecase.Request();
    }

    auto AcquisitionDataController::ScanExperiment()->bool {
        auto usecase = UseCase::ScanAcquisitionData(d->presenter);
        return usecase.Request();
    }
}
