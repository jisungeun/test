#include <StartInitialize.h>
#include <StartLiveView.h>
#include <ChangeLiveViewParameter.h>
#include <EnableAutoFocus.h>
#include <PerformAutofocus.h>
#include <ScanFocus.h>
#include <ReadAFSensorValue.h>
#include <SetBestFocus.h>
#include <LoadVessel.h>
#include <UnloadVessel.h>
#include <PerformCondenserAutofocus.h>
#include <CalibrateHTIllumination.h>
#include <ShowHTIlluminationSetupPattern.h>

#include "InstrumentController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct InstrumentController::Impl {
        InstrumentPresenter* presenter{ nullptr };
    };

    InstrumentController::InstrumentController(InstrumentPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    InstrumentController::~InstrumentController() {
    }

    auto InstrumentController::StartInitialize() -> bool {
        auto usecase = UseCase::StartInitialize(d->presenter);
        return usecase.Request();
    }

    auto InstrumentController::StartLiveBF() -> bool {
        auto usecase = UseCase::StartLiveView(d->presenter);
        usecase.SetImageType(AppEntity::Modality::BF);
        return usecase.Request();
    }

    auto InstrumentController::StartLiveFL(int32_t channel) -> bool {
        auto usecase = UseCase::StartLiveView(d->presenter);
        usecase.SetImageType(AppEntity::Modality::FL, channel);
        return usecase.Request();
    }

    auto InstrumentController::Recover() -> bool {
        auto usecase = UseCase::StartLiveView(d->presenter);
        usecase.SetLatest();
        return usecase.Request();
    }

    auto InstrumentController::ChangeLiveBF() -> bool {
        auto usecase = UseCase::ChangeLiveViewParameter(d->presenter);
        usecase.SetImageType(AppEntity::Modality::BF);
        return usecase.Request();
    }

    auto InstrumentController::ChangeLiveFL(int32_t channel) -> bool {
        auto usecase = UseCase::ChangeLiveViewParameter(d->presenter);
        usecase.SetImageType(AppEntity::Modality::FL, channel);
        return usecase.Request();
    }

    auto InstrumentController::LoadVessel() -> bool {
        auto usecase = UseCase::LoadVessel(d->presenter);
        return usecase.Request();
    }

    auto InstrumentController::UnloadVessel() -> bool {
        auto usecase = UseCase::UnloadVessel(d->presenter);
        return usecase.Request();
    }

    auto InstrumentController::PerformAutoFocus() -> bool {
        auto usecase = UseCase::PerformAutofocus(d->presenter);
        return usecase.Request();

    }

    auto InstrumentController::EnableAutoFocus() -> bool {
        auto usecase = UseCase::EnableAutoFocus(d->presenter);
        usecase.SetEnable(true);
        return usecase.Request();
    }

    auto InstrumentController::DisalbeAutoFocus() -> bool {
        auto usecase = UseCase::EnableAutoFocus(d->presenter);
        usecase.SetEnable(false);
        return usecase.Request();
    }

    auto InstrumentController::SetBestFocusCurrent() -> bool {
        auto usecase = UseCase::SetBestFocus(d->presenter);
        usecase.SetCurrent();
        return usecase.Request();
    }

    auto InstrumentController::SetBestFocusTarget(int32_t value) -> bool {
        auto usecase = UseCase::SetBestFocus(d->presenter);
        usecase.SetTarget(value);
        return usecase.Request();
    }

    auto InstrumentController::ScanFocus(double startMm, double distMm, int32_t count) -> QList<int32_t> {
        auto usecase = UseCase::ScanFocus(d->presenter);
        usecase.SetStartPositionInMm(startMm);
        usecase.SetDistanceInMm(distMm);
        usecase.SetCount(count);
        if(!usecase.Request()) return QList<int32_t>();

        return usecase.GetValues();
    }

    auto InstrumentController::ReadAFSensorValue() -> int32_t {
        auto usecase = UseCase::ReadAFSensorValue(d->presenter);
        if(!usecase.Request()) return -1;
        return usecase.GetValue();
    }

    auto InstrumentController::PerformCondenserAutoFocus() -> bool {
        auto usecase = UseCase::PerformCondenserAutofocus(d->presenter);
        return usecase.Request();
    }

    auto InstrumentController::CalibrateHTIllumination() -> bool {
        auto usecase = UseCase::CalibrateHTIllumination(d->presenter);
        return usecase.Request();
    }

    auto InstrumentController::ShowHTIlluminationSetupPattern(double NA, int32_t intensity) -> bool {
        auto usecase = UseCase::ShowHTIlluminationSetupPattern(d->presenter);
        usecase.SetNA(NA);
        usecase.SetIntensity(intensity);
        return usecase.Request();
    }
}
