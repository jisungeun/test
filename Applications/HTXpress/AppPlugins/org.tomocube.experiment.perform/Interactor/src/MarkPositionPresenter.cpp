﻿#include "MarkPositionPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct MarkPositionPresenter::Impl {
       IMarkPositionView* view {nullptr}; 
    };
    MarkPositionPresenter::MarkPositionPresenter(IMarkPositionView* view) : IMarkPositionOutputPort(), d{std::make_unique<Impl>()} {
        d->view = view;
    }

    MarkPositionPresenter::~MarkPositionPresenter() = default;

    auto MarkPositionPresenter::AddMarkPosition(AppEntity::WellIndex wellIndex, AppEntity::Location::Pointer location) -> void {
        if(d->view) { d->view->AddMarkPosition(wellIndex, location); }
    }
}
