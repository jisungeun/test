﻿#include "SystemStoragePresenter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct SystemStoragePresenter::Impl {
        ISystemStorageView::Pointer view{};
    };

    SystemStoragePresenter::SystemStoragePresenter(ISystemStorageView::Pointer view) : d{std::make_unique<Impl>()} {
        d->view = view;
    }

    SystemStoragePresenter::~SystemStoragePresenter() {

    }

    auto SystemStoragePresenter::UpdateSystemStorageSpace(const int64_t& totalBytes, const int64_t& availableBytes) -> void {
        if(d->view != nullptr) {
            d->view->UpdateSystemStorageSpace(totalBytes, availableBytes);
        }
    }

    auto SystemStoragePresenter::UpdateMinRequiredSpace(const int32_t& gigabytes) -> void {
        if(d->view != nullptr) {
            d->view->UpdateMinRequiredSpace(gigabytes);
        }
    }
}
