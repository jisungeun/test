#include <GetPosition.h>
#include <GetGlobalPosition.h>
#include <MoveSampleStage.h>
#include <MoveCAxis.h>
#include <StartJog.h>
#include <StopJog.h>

#include "MotionController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct MotionController::Impl {
        MotionPresenter* presenter{ nullptr };
    };

    MotionController::MotionController(MotionPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    MotionController::~MotionController() {
    }

    auto MotionController::GetPosition(const AppEntity::WellIndex& wellIdx, AppEntity::Position& position) -> bool {
        auto usecase = UseCase::GetPosition(d->presenter);
        usecase.SetWell(wellIdx);
        if(usecase.Request()) {
            position = usecase.CurrentPosition();
            return true;
        }
        return false;
    }

    auto MotionController::GetGlobalPosition(AppEntity::Position& position) -> bool {
        auto usecase = UseCase::GetGlobalPosition(d->presenter);
        if(!usecase.Request()) return false;
        position = usecase.CurrentPosition();
        return true;
    }

    auto MotionController::GetGlobalPosition(AppEntity::Position& position, AppEntity::WellIndex& wellIdx) -> bool {
        auto usecase = UseCase::GetGlobalPosition(d->presenter);
        if(!usecase.Request()) return false;
        position = usecase.CurrentPosition();
        wellIdx = usecase.CurrentWell();
        return true;
    }

    auto MotionController::GetGlobalPosition(AppEntity::Axis axis, double& posInMM) -> bool {
        auto usecase = UseCase::GetGlobalPosition(d->presenter);

        switch(axis) {
        case AppEntity::Axis::X:
        case AppEntity::Axis::Y:
        case AppEntity::Axis::Z:
            break;
        default:
            usecase.SetSpecificAxis(axis);
        }

        if(!usecase.Request()) return false;
        posInMM = usecase.CurrentPosition(axis);

        return true;
    }

    auto MotionController::MoveXY(const AppEntity::WellIndex wellIdx, const double xInMM, const double yInMM) -> bool {
        auto usecase = UseCase::MoveSampleStage(d->presenter);
        usecase.SetTargetXYInUm(wellIdx, xInMM*1000, yInMM*1000);
        return usecase.Request();
    }

    auto MotionController::MoveMM(const AppEntity::WellIndex& wellIdx, 
                                  const AppEntity::Axis& axis,
                                  const double& posInMM) -> bool {
        auto usecase = UseCase::MoveSampleStage(d->presenter);

        switch(axis) {
        case AppEntity::Axis::X:
            usecase.SetTargetXInUm(wellIdx, posInMM*1000);
            break;
        case AppEntity::Axis::Y:
            usecase.SetTargetYInUm(wellIdx, posInMM*1000);
            break;
        case AppEntity::Axis::Z:
            usecase.SetTargetZInUm(wellIdx, posInMM*1000);
            break;
        default:
            return false;
        }

        return usecase.Request();
    }

    auto MotionController::MoveMM(const AppEntity::Axis axis, const double posInMm) -> bool {
        auto usecase = UseCase::MoveSampleStage(d->presenter);
        usecase.SetTargetUm(axis, posInMm * 1000);
        return usecase.Request();
    }

    auto MotionController::MoveXYInGlobal(const double xInMM, const double yInMM) -> bool {
        auto usecase = UseCase::MoveSampleStage(d->presenter);
        usecase.SetGlobalTargetXYInUm(xInMM*1000, yInMM*1000);
        return usecase.Request();
    }

    auto MotionController::MoveRelativePixels(int32_t relPosX, int32_t relPosY) -> bool {
        auto usecase = UseCase::MoveSampleStage(d->presenter);
        usecase.SetRelativeTargetPixels(relPosX, relPosY);
        return usecase.Request();
    }

    auto MotionController::MoveRelativeMM(const AppEntity::Axis axis, const double distMm) -> bool {
        auto usecase = UseCase::MoveSampleStage(d->presenter);
        usecase.SetRelativeTargetUm(axis, distMm * 1000);
        return usecase.Request();
    }

    auto MotionController::MoveRelativeUM(const AppEntity::Axis axis, const double distUm) -> bool {
        auto usecase = UseCase::MoveSampleStage(d->presenter);
        usecase.SetRelativeTargetUm(axis, distUm);
        return usecase.Request();
    }

    auto MotionController::MoveCAxis(double targetInMm) -> bool {
        auto usecase = UseCase::MoveCAxis(d->presenter);
        usecase.SetTarget(targetInMm);
        return usecase.Request();
    }

    auto MotionController::StartJog(const AppEntity::Axis axis, bool plusDirection) -> bool {
        auto usecase = UseCase::StartJog(d->presenter);
        usecase.Set(axis, plusDirection);
        return usecase.Request();
    }

    auto MotionController::StopJog() -> bool {
        auto usecase = UseCase::StopJog(d->presenter);
        return usecase.Request();
    }
}
