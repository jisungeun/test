﻿#include <AddMarkPosition.h>

#include "MarkPositionSetupController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct MarkPositionSetupController::Impl {
        MarkPositionPresenter* presenter{nullptr};
    };

    MarkPositionSetupController::MarkPositionSetupController(MarkPositionPresenter* presenter) :  d{std::make_unique<Impl>()} {
        d->presenter = presenter;
    }

    MarkPositionSetupController::~MarkPositionSetupController() = default;

    auto MarkPositionSetupController::AddMarkPosition(double xInMm, double yInMm, const AppEntity::Area& area, bool isTile) -> bool {
        auto usecase = UseCase::AddMarkPosition(d->presenter);
        usecase.SetCurrentArea(xInMm, yInMm, area, isTile);
        return usecase.Request();
    }
}
