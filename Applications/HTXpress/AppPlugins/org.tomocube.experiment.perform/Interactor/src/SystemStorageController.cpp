﻿#include <GetAvailableStorageSpace.h>
#include <InstallStorageMonitor.h>
#include <GetDataFolderPath.h>
#include <GetMinRequiredSpace.h>

#include "SystemStorageController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct SystemStorageController::Impl {
        SystemStoragePresenter::Pointer presenter{};
    };

    SystemStorageController::SystemStorageController(SystemStoragePresenter::Pointer presenter) : d{std::make_unique<Impl>()} {
        d->presenter = presenter;
    }

    SystemStorageController::~SystemStorageController() {
    }

    auto SystemStorageController::UpdateStorageInfo() const -> bool {
        auto usecase = UseCase::GetAvailableStorageSpace();
        return usecase.Request();
        
    }
    auto SystemStorageController::InstallStorageMonitor() const -> bool {
        auto usecase = UseCase::InstallStorageMonitor(d->presenter);
        return usecase.Request();
    }

    auto SystemStorageController::UpdateRootDrive() const -> bool {
        auto usecase = UseCase::GetDataFolderPath();
        return usecase.Request();
    }

    auto SystemStorageController::UpdateMinRequiredSpace() const -> bool {
        auto usecase = UseCase::GetMinRequiredSpace();
        return usecase.Request();
    }
}
