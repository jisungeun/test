#include "AcquireImagePresenter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct AcquireImagePresenter::Impl {
        IAcquireImageView* view{ nullptr };
    };

    AcquireImagePresenter::AcquireImagePresenter(IAcquireImageView* view) : IAcquireImageOutputPort(), d{new Impl} {
        d->view = view;
    }

    AcquireImagePresenter::~AcquireImagePresenter() {
    }

    auto AcquireImagePresenter::UpdateProgress(double progress) -> void {
        if(d->view) {
            d->view->UpdateProgress(progress);
        }
    }
}
