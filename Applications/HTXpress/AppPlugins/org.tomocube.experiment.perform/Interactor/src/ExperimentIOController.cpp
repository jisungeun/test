#include <LoadExperiment.h>
#include <ReloadCurrentExperiment.h>

#include "ExperimentIOController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct ExperimentIOController::Impl {
        ExperimentIOPresenter* presenter{ nullptr };
    };

    ExperimentIOController::ExperimentIOController(ExperimentIOPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    ExperimentIOController::~ExperimentIOController() {
    }

    auto ExperimentIOController::Load(const QString& project, const QString& title) -> bool {
        auto usecase = UseCase::LoadExperiment(d->presenter);
        usecase.SetExperiment(project, title);
        return usecase.Request();
    }

    auto ExperimentIOController::Reload() -> bool {
        auto usecase = UseCase::ReloadCurrentExperiment(d->presenter);
        return usecase.Request();
    }
}
