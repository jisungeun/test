#include <CapturePreview.h>
#include <SetCustomPreviewArea.h>
#include <SetCustomPreviewAreaEditableStatus.h>

#include "PreviewAcquisitionController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct PreviewAcquisitionController::Impl {
        PreviewPresenter* presenter{ nullptr };
    };

    PreviewAcquisitionController::PreviewAcquisitionController(PreviewPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    PreviewAcquisitionController::~PreviewAcquisitionController() = default;

    auto PreviewAcquisitionController::CaptureArea(int32_t widthUm, int32_t heightUm, std::function<bool()> stopFunc) -> bool {
        auto usecase = UseCase::CapturePreview(d->presenter, stopFunc);
        usecase.SetArea(AppEntity::Area::fromUM(widthUm, heightUm));
        return usecase.Request();
    }

    auto PreviewAcquisitionController::SetCustomPreviewArea(double x, double y, int32_t widthUm, int32_t heightUm) -> bool {
        auto usecase = UseCase::SetCustomPreviewArea(d->presenter);
        usecase.SetPreviewArea(x, y, widthUm, heightUm);
        return usecase.Request();
    }

    auto PreviewAcquisitionController::DoCustomPreviewAreaSetting() -> bool {
        auto usecase = UseCase::SetCustomPreviewAreaEditableStatus(d->presenter);
        usecase.DoAreaSetting();
        return usecase.Request();
    }

    auto PreviewAcquisitionController::CancelCustomPreviewAreaSetting() -> bool {
        auto usecase = UseCase::SetCustomPreviewAreaEditableStatus(d->presenter);
        usecase.CancelAreaSetting();
        return usecase.Request();
    }
}
