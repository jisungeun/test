#include <GetProjectList.h>
#include <GetExperimentList.h>

#include "ProjectController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    ProjectController::ProjectController() {
    }

    ProjectController::~ProjectController() {
    }

    auto ProjectController::GetProjects() const -> QStringList {
        auto usecase = UseCase::GetProjectList();
        usecase.Request();
        return usecase.GetList();
    }

    auto ProjectController::GetExperiments(const QString& project) const -> QStringList {
        auto usecase = UseCase::GetExperimentList();
        usecase.SetProject(project);
        usecase.Request();
        return usecase.GetList();
    }
}
