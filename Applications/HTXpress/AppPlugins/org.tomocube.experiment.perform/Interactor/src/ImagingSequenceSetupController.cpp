#include <ApplyImagingSequences.h>
#include <ClearImagingSequences.h>
#include <ApplyImagingSequencesFull3D.h>
#include <GetAcquisitionDataRequiredSpace.h>

#include "ImagingSequenceSetupController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct ImagingSequenceSetupController::Impl {
        ImagingPlanPresenter* presenter{ nullptr };
    };

    ImagingSequenceSetupController::ImagingSequenceSetupController(ImagingPlanPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    };

    ImagingSequenceSetupController::~ImagingSequenceSetupController() {
    }

    auto ImagingSequenceSetupController::ApplyImagingSequences(const QList<Sequence>& sequences) -> bool {
        auto usecase = UseCase::ApplyImagingSequences(d->presenter);

        for (auto& sequence : sequences) {
            usecase.BeginNewSequence();
            for(auto modality : sequence.modalities) {
                usecase.AddModality(modality.modality, modality.is3D, modality.isGray, modality.channels);
            }
            usecase.SetStartTimestamp(sequence.startTimeInSecs);
            usecase.SetInterval(sequence.intervalInSecs);
            usecase.SetCount(sequence.count);
        }

        return usecase.Request();
    }

    auto ImagingSequenceSetupController::ApplyImagingSequencesFull3D(const QList<Sequence>& sequences) -> bool {
        auto usecase = UseCase::ApplyImagingSequencesFull3D(d->presenter);

        for(auto& sequence : sequences) {
            usecase.BeginNewSequence();
            for(auto m : sequence.modalities) {
                usecase.AddModality(m.modality, m.is3D, m.isGray, m.channels);
            }
            usecase.SetStartTimestamp(sequence.startTimeInSecs);
            usecase.SetInterval(sequence.intervalInSecs);
            usecase.SetCount(sequence.count);
        }

        return usecase.Request();
    }

    auto ImagingSequenceSetupController::ApplyImagingSequencesFull3D(const Sequence& test3Dsequence) -> bool {
        auto usecase = UseCase::ApplyImagingSequencesFull3D(d->presenter);

        usecase.BeginNewSequence();
        for(auto modality : test3Dsequence.modalities) {
            usecase.AddModality(modality.modality, modality.is3D, modality.isGray, modality.channels);
        }
        usecase.SetStartTimestamp(0);
        usecase.SetInterval(0);
        usecase.SetCount(0);

        return usecase.Request();
    }

    auto ImagingSequenceSetupController::GetAcquisitionDataRequiredSpace(const int32_t& acquisitionPoints) const -> bool {
        auto usecase = UseCase::GetAcquisitionDataRequiredSpace(d->presenter);
        usecase.SetAcquisitionPoints(acquisitionPoints);
        return usecase.Request();
    }

    auto ImagingSequenceSetupController::Clear() -> bool {
        auto usecase = UseCase::ClearImagingSequences();
        return usecase.Request();
    }
}
