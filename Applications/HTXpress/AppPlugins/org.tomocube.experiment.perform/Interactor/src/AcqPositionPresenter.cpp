#include "AcqPositionPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct AcqPositionPresenter::Impl {
        IAcqPositionView* view{ nullptr };
    };

    AcqPositionPresenter::AcqPositionPresenter(IAcqPositionView* view) : UseCase::IAcquisitionPositionOutputPort(), d{new Impl} {
        d->view = view;
    }

    AcqPositionPresenter::~AcqPositionPresenter() {
    }

    auto AcqPositionPresenter::AddPosition(LocationIndex locationIdx, WellIndex wellIndex, Location::Pointer loc) -> void {
        if(d->view) {
            d->view->AddPosition(locationIdx, wellIndex, loc);
        }
    }

    auto AcqPositionPresenter::RefreshList(WellIndex wellIndex, QMap<LocationIndex, Location::Pointer>& locList) -> void {
        if(d->view) {
            d->view->RefreshList(wellIndex, locList);
        }
    }

    auto AcqPositionPresenter::DeletePosition(WellIndex wellIdx, LocationIndex locationIdx) -> void {
        if(d->view) {
            d->view->DeletePosition(wellIdx, locationIdx);
        }
    }
}
