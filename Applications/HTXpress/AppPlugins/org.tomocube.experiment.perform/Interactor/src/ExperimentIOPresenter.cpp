#include "ExperimentIOPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct ExperimentIOPresenter::Impl {
        IExperimentIOView* view{ nullptr };
    };

    ExperimentIOPresenter::ExperimentIOPresenter(IExperimentIOView* view) : UseCase::IExperimentOutputPort(), d{new Impl} {
        d->view = view;
    }

    ExperimentIOPresenter::~ExperimentIOPresenter() {
    }

    auto ExperimentIOPresenter::Update(AppEntity::Experiment::Pointer experiment, bool reloaded) -> void {
        if(d->view) {
            d->view->Update(experiment, reloaded);
        }
    }

    auto ExperimentIOPresenter::Error(const QString& message) -> void {
        if(d->view) {
            d->view->Error(message);
        }
    }
}
