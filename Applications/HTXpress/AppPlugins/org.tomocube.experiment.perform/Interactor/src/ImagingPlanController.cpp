#include <System.h>
#include <SystemStatus.h>
#include <ConfigureImgingCondition.h>
#include <SetFieldOfView.h>
#include <SetTileScanArea.h>
#include <SetFLScanConfig.h>
#include <SetFLChannel.h>
#include <ClearFLChannels.h>
#include <SetBFEnable.h>
#include <SetHTEnable.h>
#include <SetFLEnable.h>
#include <SetAcquisitionLock.h>
#include <SetTileActivation.h>

#include "ImagingPlanController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct ImagingPlanController::Impl {
        ImagingPlanPresenter* presenter{ nullptr };
        bool updateExperiment{ true };

        auto FillConditionHT(AppEntity::ImagingConditionHT::Pointer& cond, bool is3D)->void;
        auto FillConditionFL(AppEntity::ImagingConditionFL::Pointer& cond, QList<int32_t> channels, bool is3D)->void;
        auto FillConditionBF(AppEntity::ImagingConditionBF::Pointer& cond, bool isGray)->void;
    };

    auto ImagingPlanController::Impl::FillConditionHT(AppEntity::ImagingConditionHT::Pointer& cond, bool is3D) -> void {
        const auto imagingMode = is3D ? AppEntity::ImagingMode::HT3D : AppEntity::ImagingMode::HT2D;
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto channelConfig = sysStatus->GetChannelConfig(imagingMode);
        auto scanConfig = sysStatus->GetScanConfig(imagingMode);

        cond->SetExposure(channelConfig->GetCameraExposureUSec());
        cond->SetInterval(channelConfig->GetCameraIntervalUSec());
        cond->SetIntensity(channelConfig->GetLightIntensity());

        cond->SetSliceStart(0);
        if(is3D) {
            cond->SetSliceCount(scanConfig->GetSteps());
            cond->SetDimension(3);
        } else {
            cond->SetSliceCount(1);
            cond->SetDimension(2);
        }
        cond->SetSliceStep(scanConfig->GetStep());
        
    }

    auto ImagingPlanController::Impl::FillConditionFL(AppEntity::ImagingConditionFL::Pointer& cond, 
                                                      QList<int32_t> channels, 
                                                      bool is3D) -> void {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto sysConfig = AppEntity::System::GetSystemConfig();

        for(auto channel : channels) {
            auto flChannelMode = AppEntity::ImagingMode::_from_integral(AppEntity::ImagingMode::FLCH0 + channel);
            auto channelConfig = sysStatus->GetChannelConfig(flChannelMode);
            auto scanConfig = sysStatus->GetScanConfig(flChannelMode);

            if(is3D) {
                cond->SetSliceCount(scanConfig->GetSteps());
                cond->SetDimension(3);
            } else {
                cond->SetSliceCount(1);
                cond->SetDimension(2);
            }
            cond->SetSliceStep(scanConfig->GetStep());
            cond->SetScanMode(scanConfig->GetScanMode());
            cond->SetScanRange(scanConfig->GetBottom(), scanConfig->GetTop(), scanConfig->GetStep());
            cond->SetScanFocus(0, scanConfig->GetFocusFLOffsetUm());

            AppEntity::FLChannel flChannel;
            AppEntity::FLFilter exFilter, emFilter;
            sysConfig->GetFLActiveExcitation(channelConfig->GetLightChannel(), exFilter);
            sysConfig->GetFLEmission(channelConfig->GetFilterChannel(), emFilter);
            sysConfig->GetFLChannel(channelConfig->GetChannelName(), flChannel);

            AppEntity::ImagingConditionFL::Color color;
            color.red = flChannel.GetColor().red();
            color.green = flChannel.GetColor().green();
            color.blue = flChannel.GetColor().blue();

            cond->SetChannel(channel, 
                             channelConfig->GetCameraExposureUSec(),
                             channelConfig->GetCameraIntervalUSec(),
                             channelConfig->GetLightIntensity(),
                             0,
                             exFilter,
                             emFilter,
                             channelConfig->GetChannelName(),
                             color);
        }
    }

    auto ImagingPlanController::Impl::FillConditionBF(AppEntity::ImagingConditionBF::Pointer& cond, bool isGray) -> void {
        auto imagingMode = (isGray)?AppEntity::ImagingMode::BFGray : AppEntity::ImagingMode::BFColor;
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto channelConfig = sysStatus->GetChannelConfig(imagingMode);
        auto scanConfig = sysStatus->GetScanConfig(imagingMode);

        cond->SetExposure(channelConfig->GetCameraExposureUSec());
        cond->SetInterval(channelConfig->GetCameraIntervalUSec());
        cond->SetIntensity(channelConfig->GetLightIntensity());
        cond->SetColorMode(!isGray);

        cond->SetSliceStart(0);
        cond->SetSliceCount(1);
        cond->SetSliceStep(0);

        cond->SetDimension(2);
    }

    ImagingPlanController::ImagingPlanController(bool updateExepriment, ImagingPlanPresenter* presenter) : d{new Impl} {
        d->updateExperiment = updateExepriment;
        d->presenter = presenter;
    }

    ImagingPlanController::~ImagingPlanController() {
    }

    auto ImagingPlanController::SetFieldOfView(double xInUm, double yInUm, double& adjustedXInUm, double& adjustedYInUm) -> bool {
        auto usecase = UseCase::SetFieldOfView(d->updateExperiment, d->presenter);
        usecase.SetFOV(xInUm, yInUm);
        if(!usecase.Request()) return false;

        usecase.GetAdjustedFOV(adjustedXInUm, adjustedYInUm);
        return true;
    }

    auto ImagingPlanController::SetFLScanConfig(const AppEntity::ScanConfig& scanConfig) -> bool {
        auto usecase = UseCase::SetFLScanConfig(d->updateExperiment, d->presenter);
        usecase.SetScanConfig(scanConfig);
        return usecase.Request();
    }

    auto ImagingPlanController::SetTileScanArea(bool enable, AppEntity::WellIndex wellIndex, 
                                                double x, double y, double width, double height) -> bool {
        auto usecase = UseCase::SetTileScanArea(d->presenter);
        usecase.SetArea(enable, wellIndex, x ,y, width, height);
        return usecase.Request();
    }

    auto ImagingPlanController::ClearFLChannels() -> bool {
        auto usecase = UseCase::ClearFLChannels(d->presenter);
        return usecase.Request();
    }

    auto ImagingPlanController::SetFLChannel(const int32_t channel, 
                                             const AppEntity::ChannelConfig& channelConfig) -> bool {
        auto usecase = UseCase::SetFLChannel(d->updateExperiment, d->presenter);
        usecase.SetChannel(channel, channelConfig);
        return usecase.Request();
    }

    auto ImagingPlanController::SetHTEnabled(const bool is3D, const bool enabled) -> bool {
        auto usecase = UseCase::SetHTEnable(d->updateExperiment, d->presenter);
        usecase.SetEnable(is3D, enabled);
        return usecase.Request();
    }

    auto ImagingPlanController::SetFLEnabled(const bool is3D, const bool enabled) -> bool {
         auto usecase = UseCase::SetFLEnable(d->updateExperiment, d->presenter);
        usecase.SetEnable(is3D, enabled);
        return usecase.Request();
    }

    auto ImagingPlanController::SetBFEnabled(const bool enabled) -> bool {
        auto usecase = UseCase::SetBFEnable(d->updateExperiment, d->presenter);
        usecase.SetEnable(true, enabled);
        return usecase.Request();
    }

    auto ImagingPlanController::SetAcquisitionLock(bool locked) -> bool {
        auto usecase = UseCase::SetAcquisitionLock(d->presenter);
        usecase.SetLock(locked);
        return usecase.Request();
    }

    auto ImagingPlanController::TileDeactivation() -> bool {
        auto usecase = UseCase::SetTileActivation(d->presenter);
        usecase.SetEnable(false);
        return usecase.Request();
    }
}
