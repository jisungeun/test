﻿#include <SetSafeMovingRange.h>

#include "LensMovingRangeController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct LensMovingRangeController::Impl {
        VesselPresenter* presenter {nullptr};
    };

    LensMovingRangeController::LensMovingRangeController(VesselPresenter* presenter) : d{std::make_unique<Impl>()} {
        d->presenter = presenter;
    }

    LensMovingRangeController::~LensMovingRangeController() {
    }

    auto LensMovingRangeController::SetSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM) -> bool {
        auto usecase = UseCase::SetSafeMovingRange(d->presenter);
        usecase.SetSafeRange(axis, minMM, maxMM);
        return usecase.Request();
    }
}
