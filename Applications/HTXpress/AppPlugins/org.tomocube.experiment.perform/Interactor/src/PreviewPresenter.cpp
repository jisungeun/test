#include "PreviewPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct PreviewPresenter::Impl {
        IPreviewView* view { nullptr };
    };

    PreviewPresenter::PreviewPresenter(IPreviewView* view) : UseCase::IPreviewOutputPort(), d{std::make_unique<Impl>()} {
        d->view = view;
    }

    PreviewPresenter::~PreviewPresenter() {
    }

    auto PreviewPresenter::Progress(double progress, AppEntity::Position& position) -> void {
        if(d->view) d->view->Progress(progress, position);
    }

    auto PreviewPresenter::UpdateImageSize(int32_t xPixels, int32_t yPixels) -> void {
        if(d->view) d->view->UpdateImageSize(xPixels, yPixels);
    }

    auto PreviewPresenter::UpdateBlock(int32_t xPos, int32_t yPos, const QImage& blockImage) -> void {
        if(d->view) d->view->UpdateBlock(xPos, yPos, blockImage);
    }

    auto PreviewPresenter::UpdateCustomPreviewArea(double wellXinMM, double wellYinMM, int32_t widthUM, int32_t heightUM) -> void {
        if(d->view) d->view->UpdateCustomPreviewArea(wellXinMM, wellYinMM, widthUM, heightUM);
    }

    auto PreviewPresenter::DoCustomPreviewAreaSetting() -> void {
        if(d->view) d->view->StartCustomPreviewAreaSetting();
    }

    auto PreviewPresenter::CancelCustomPreviewAreaSetting() -> void {
        if(d->view) d->view->CancelCustomPreviewAreaSetting();
    }

    auto PreviewPresenter::SetPreviewArea(double wellXinMM, double wellYinMM, double widthMM, double heightMM) -> void {
        if (d->view) d->view->SetPreviewArea(wellXinMM, wellYinMM, widthMM, heightMM);
    }
}
