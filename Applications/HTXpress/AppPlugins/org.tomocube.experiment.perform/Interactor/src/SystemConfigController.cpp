#include <SaveSystemConfiguration.h>
#include <ApplySystemConfiguration.h>
#include "SystemConfigController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct SystemConfigController::Impl {
    };

    SystemConfigController::SystemConfigController() : d{new Impl} {
    }

    SystemConfigController::~SystemConfigController() {
    }

    auto SystemConfigController::Save() -> bool {
        auto usecase = UseCase::SaveSystemConfiguration();
        return usecase.Request();
    }

    auto SystemConfigController::ApplyOnly() -> bool {
        auto usecase = UseCase::ApplySystemConfiguration();
        return usecase.Request();
    }
}
