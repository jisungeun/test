#include <QMap>

#include <SetFieldOfView.h>
#include <SetFLScanConfig.h>
#include <SetFLChannel.h>
#include <AcquireImage.h>

#include "AcquireImageController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct AcquireImageController::Impl {
        ExperimentPresenter* presenter{ nullptr };
    };

    AcquireImageController::AcquireImageController(ExperimentPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    AcquireImageController::~AcquireImageController() {
    }

    auto AcquireImageController::SetFieldOfView(double xInUm, double yInUm) -> bool {
        auto usecase = UseCase::SetFieldOfView(true);
        usecase.SetFOV(xInUm, yInUm);
        return usecase.Request();
    }

    auto AcquireImageController::SetFLScanConfig(const AppEntity::ScanConfig& scanConfig) -> bool {
        auto usecase = UseCase::SetFLScanConfig(true);
        usecase.SetScanConfig(scanConfig);
        return usecase.Request();
    }

    auto AcquireImageController::SetFLChannel(const int32_t channel, 
                                              const ChannelConfig& channelConfig) -> bool {
        auto usecase = UseCase::SetFLChannel(true);
        usecase.SetChannel(channel, channelConfig);
        return usecase.Request();
    }

    auto AcquireImageController::Acquire(const QList<ImagingType>& types, 
                                         double xInMm, double yInMm, 
                                         double roiXInUm, double roiYInUm) -> bool {
        auto usecase = UseCase::AcquireImage(d->presenter);

        for(const auto& type : types) {
            switch(type.modality) {
            case AppEntity::Modality::HT:
                usecase.AddModalityHT(type.is3D);
                break;
            case AppEntity::Modality::FL:
                usecase.AddModalityFL(type.is3D, type.channels);
                break;
            case AppEntity::Modality::BF:
                usecase.AddModalityBF(type.isGray);
                break;
            }
        }

        usecase.SetCenter(xInMm, yInMm);
        usecase.SetROI(roiXInUm, roiYInUm);

        return usecase.Request();
    }
}
