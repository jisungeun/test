#include <RunExperiment.h>
#include <TestExperiment.h>
#include <StopExperiment.h>

#include "ExperimentController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct ExperimentController::Impl {
        ExperimentPresenter* presenter{ nullptr };
    };

    ExperimentController::ExperimentController(ExperimentPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    ExperimentController::~ExperimentController() {
    }

    auto ExperimentController::RunExperiment() -> bool {
        auto usecase = UseCase::RunExperiment(d->presenter);
        return usecase.Request();
    }

    auto ExperimentController::TestExperiment() -> bool {
        auto usecase = UseCase::TestExperiment(d->presenter);
        return usecase.Request();
    }

    auto ExperimentController::StopExperiment() -> bool {
        auto usecase = UseCase::StopExperiment(d->presenter);
        return usecase.Request();
    }
}
