#include <SetLiveImagingMode.h>
#include <SetLiveImagingConfig.h>
#include <ApplyImagingConditionToLiveConfig.h>
#include <ApplyLiveConfigToAcquisitionConfig.h>
#include <GetImagingCondition.h>
#include <ChangeImagingSettingMode.h>

#include "LiveviewConfigController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct LiveviewConfigController::Impl {
        LiveviewConfigPresenter* presenter{ nullptr };
    };

    LiveviewConfigController::LiveviewConfigController(LiveviewConfigPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    LiveviewConfigController::~LiveviewConfigController() {
    }

    auto LiveviewConfigController::SetImagingMode(const AppEntity::ImagingMode mode) -> bool {
        auto usecase = UseCase::SetLiveImagingMode(d->presenter);
        usecase.SetMode(mode);
        return usecase.Request();
    }

    auto LiveviewConfigController::SetLiveImagingConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain) -> bool {
        auto usecase = UseCase::SetLiveImagingConfig(d->presenter);
        usecase.SetConfig(mode, intensity, exposure, gain);
        return usecase.Request();
    }

    auto LiveviewConfigController::ApplyChannelCondition(const AppEntity::ImagingMode mode) -> bool {
        auto usecase = UseCase::ApplyImagingConditionToLiveConfig(d->presenter);
        usecase.ApplyConfig(mode);
        return usecase.Request();
    }

    auto LiveviewConfigController::ApplyToAcquisitionConfig(const AppEntity::ImagingMode mode) -> bool {
        auto usecase = UseCase::ApplyLiveConfigToAcqusitionConfig(d->presenter);
        usecase.ApplyConfig(mode);
        return usecase.Request();
    }

    auto LiveviewConfigController::GetChannelConfig(const AppEntity::ImagingMode mode) ->AppEntity::ChannelConfig::Pointer{
        auto usecase = UseCase::GetImagingCondition();
        if (!usecase.Request()) {
            return nullptr;
        }

        return usecase.GetConfig(mode);
    }

    auto LiveviewConfigController::ChangeImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode) -> bool {
        auto usecase = UseCase::ChangeImagingSettingMode(d->presenter);
        usecase.SetCurrentImagingSettingMode(imagingSettingMode);
        return usecase.Request();
    }

}
