#include "QualityCheckPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct QualityCheckPresenter::Impl {
        IQualityCheckView* view{ nullptr };
    };

    QualityCheckPresenter::QualityCheckPresenter(IQualityCheckView* view)
        : IRunExperimentOutputPort()
        , IQualityCheckOutputPort()
        , d{ std::make_unique<Impl>() } {
        d->view = view;
    }

    QualityCheckPresenter::~QualityCheckPresenter() {
    }

    auto QualityCheckPresenter::GetInstance(IQualityCheckView* view) -> Pointer {
        static Pointer theInstance{ new QualityCheckPresenter(view) };
        return theInstance;
    }

    auto QualityCheckPresenter::UpdateProgress(const UseCase::ExperimentStatus& status) -> void {
        if(d->view) {
            if(status.IsError()) {
                d->view->NotifyAcquisitionError(status.GetErrorMessage());
            } else {
                d->view->UpdateAcquisitionProgress(status.GetProgress());
            }
        }
    }

    auto QualityCheckPresenter::NotifyStopped() -> void {
        if(d->view) {
            d->view->NotifyAcquisitionStopped();
        }
    }

    auto QualityCheckPresenter::UpdateEvaluationProgress(double progress) -> void {
        if(d->view) {
            d->view->UpdateEvaluationProgress(progress);
        }
    }
}