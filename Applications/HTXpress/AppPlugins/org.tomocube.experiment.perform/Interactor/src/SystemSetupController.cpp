#include <GenerateBackground.h>

#include "SystemSetupController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct SystemSetupController::Impl {
    };

    SystemSetupController::SystemSetupController() : d{new Impl} {
    }

    SystemSetupController::~SystemSetupController() {
    }

    auto SystemSetupController::GenerateBackground(const QString& srcDir, const QString& destDir) -> bool {
        auto usecase = UseCase::GenerateBackground();
        usecase.SetSource(srcDir);
        usecase.SetTarget(destDir);
        return usecase.Request();
    }
}