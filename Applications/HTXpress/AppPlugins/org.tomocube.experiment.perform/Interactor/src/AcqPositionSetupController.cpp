#include <AddAcquisitionPosition.h>
#include <DeleteAcquisitionPosition.h>
#include <CopyAcquisitionPoints.h>
#include <AddMatrixAcquisitionPoints.h>

#include "AcqPositionSetupController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct AcqPositionSetupController::Impl {
        AcqPositionPresenter* presenter{ nullptr };
    };

    AcqPositionSetupController::AcqPositionSetupController(AcqPositionPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    AcqPositionSetupController::~AcqPositionSetupController() {
    }

    auto AcqPositionSetupController::AddCurrentPosition() -> bool {
        auto usecase = UseCase::AddAcquisitionPosition(d->presenter);
        usecase.SetCurrentPosition();
        return usecase.Request();
    }

    auto AcqPositionSetupController::AddCurrentArea(const Area& area, bool isTile) -> bool {
        auto usecase = UseCase::AddAcquisitionPosition(d->presenter);
        usecase.SetCurrentArea(area, isTile);
        return usecase.Request();
    }

    auto AcqPositionSetupController::AddCurrentTileArea(double xInMm, double yInMm, 
                                                        const Area& area, bool isTile) -> bool {
        auto usecase = UseCase::AddAcquisitionPosition(d->presenter);
        usecase.SetCurrentTileArea(xInMm, yInMm, area, isTile);
        return usecase.Request();
    }

    auto AcqPositionSetupController::DeletePosition(const WellIndex wellIdx,
                                                    const LocationIndex locationIdx) -> bool {
        auto usecase = UseCase::DeleteAcquisitionPosition(d->presenter);
        usecase.SetPosition(wellIdx, locationIdx);
        return usecase.Request();
    }

    auto AcqPositionSetupController::CopyPositions(const WellIndex sourceWellIndex,
                                                   const QList<LocationIndex>& sourceLocations,
                                                   const QList<WellIndex>& targetWells) -> bool {
        auto usecase = UseCase::CopyAcquisitionPositions(d->presenter);
        usecase.SetSources(sourceWellIndex, sourceLocations);
        usecase.SetTargetWells(targetWells);
        return usecase.Request();
    }

    auto AcqPositionSetupController::AddMatrixPositions(const WellIndex sourceWellIndex,
                                                        const LocationIndex sourceLocationIndex,
                                                        const int32_t cols, const int32_t rows,
                                                        const double horGapMm, const double verGapMm,
                                                        const QList<WellIndex>& wells) -> bool {
        auto usecase = UseCase::AddMatrixAcquisitionPoints(d->presenter);
        usecase.SetCenter(sourceWellIndex, sourceLocationIndex);
        usecase.SetMatrix(cols, rows, horGapMm, verGapMm);
        usecase.SetTargets(wells);
        return usecase.Request();
    }
}
