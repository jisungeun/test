#include <SelectWell.h>
#include <MoveSampleStage.h>

#include "VesselController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct VesselController::Impl {
        MotionPresenter* presenter{ nullptr };
    };

    VesselController::VesselController(MotionPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    VesselController::~VesselController() {
    }

    auto VesselController::SetCurrentWell(AppEntity::WellIndex wellIdx) -> bool {
        auto usecase = UseCase::SelectWell(d->presenter);
        usecase.SetWell(wellIdx);
        if(!usecase.Request()) return false;

        auto moveUsecase = UseCase::MoveSampleStage(d->presenter);
        moveUsecase.SetTargetWell(wellIdx);
        return moveUsecase.Request();
    }

    auto VesselController::SetSampleStagePosition(AppEntity::WellIndex wellIdx, double xMM, double yMM) -> bool {
        auto moveUsecase = UseCase::MoveSampleStage(d->presenter);
        moveUsecase.SetTargetXYInUm(wellIdx, xMM*1000.0, yMM*1000.0);
        return moveUsecase.Request();
    }

    auto VesselController::SetSampleStagePosition(AppEntity::WellIndex wellIdx, double zMM) -> bool {
        auto moveUsecase = UseCase::MoveSampleStage(d->presenter);
        moveUsecase.SetTargetZInUm(wellIdx, zMM * 1000.0);
        return moveUsecase.Request();
    }
}
