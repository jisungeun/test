#include <QStandardPaths>
#include <QDir>

#include <QualityCheckExperiment.h>
#include <EvaluateBead.h>

#include "QualityCheckController.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    struct QualityCheckController::Impl {
        QualityCheckPresenter* presenter{ nullptr };

        auto BeadScoreOutputPath() const->QString;
    };

    auto QualityCheckController::Impl::BeadScoreOutputPath() const -> QString {
        auto tempPath = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
        return QString("%1/.tsx.eval.beadscores").arg(tempPath);
    }

    QualityCheckController::QualityCheckController(QualityCheckPresenter* presenter) : d{ std::make_unique<Impl>() } {
        d->presenter = presenter;
    }

    QualityCheckController::~QualityCheckController() {
    }

    auto QualityCheckController::AcquireAllPoints() -> bool {
        auto usecase = UseCase::QualityCheckExperiment(d->presenter);
        usecase.SetAllPotins();
        usecase.SetRepeatCount(1);
        return usecase.Request();
    }

    auto QualityCheckController::AcquireTheCurrent(int32_t repeatCount) -> bool {
        auto usecase = UseCase::QualityCheckExperiment(d->presenter);
        usecase.SetCurrentPoint();
        usecase.SetRepeatCount(repeatCount);
        return usecase.Request();
    }

    auto QualityCheckController::AcquireMatrixPoints(int32_t xTiles, int32_t yTiles,
                                                     int32_t xGapUm, int32_t yGapUm) -> bool {
        auto usecase = UseCase::QualityCheckExperiment(d->presenter);
        usecase.SetMatrixPoints(xTiles, yTiles, xGapUm, yGapUm);
        usecase.SetRepeatCount(1);
        return usecase.Request();
    }

    auto QualityCheckController::EvaluateBeadScore(const QList<Data>& dataList) -> QList<Entity::BeadScore> {
        auto usecase = UseCase::EvaluateBead(d->presenter);

        for(auto data : dataList) {
            usecase.AddData(data.path, data.xInPixel, data.yInPixel);
        }

        auto outPath = d->BeadScoreOutputPath();
        if(QDir().exists(outPath)) QDir(outPath).removeRecursively();
        QDir().mkpath(outPath);

        usecase.SetOutputPath(outPath);

        if(!usecase.Request()) return QList<Entity::BeadScore>();

        return usecase.GetResults();
    }

    auto QualityCheckController::GetOutputPath() const -> QString {
        return d->BeadScoreOutputPath();
    }
}
