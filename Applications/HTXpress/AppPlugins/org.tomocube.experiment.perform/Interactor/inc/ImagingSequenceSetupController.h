#pragma once
#include <memory>
#include <QList>

#include <AppEntityDefines.h>

#include "ImagingPlanPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API ImagingSequenceSetupController {
    public:
        struct Sequence {
            uint32_t startTimeInSecs;
            uint32_t intervalInSecs;
            uint32_t count;

            struct Modality {
                AppEntity::Modality modality{ AppEntity::Modality::HT };
                bool is3D{ true };
                bool isGray{ true };
                QList<int32_t> channels;
            };
            QList<Modality> modalities;

            auto AddModality(const Modality& modality)->void {
                modalities.push_back(modality);
            }

            auto SetStartTimestamp(const uint32_t seconds)->void {
                startTimeInSecs = seconds;
            }

            auto SetInterval(const uint32_t seconds)->void {
                intervalInSecs = seconds;
            }

            auto SetCount(uint32_t _count)->void {
                count = _count;
            }
        };

    public:
        ImagingSequenceSetupController(ImagingPlanPresenter* presenter);
        ~ImagingSequenceSetupController();

        auto ApplyImagingSequences(const QList<Sequence>& sequences)->bool;
        auto ApplyImagingSequencesFull3D(const QList<Sequence>& sequences)->bool;
        auto ApplyImagingSequencesFull3D(const Sequence& test3Dsequence)->bool;

        auto GetAcquisitionDataRequiredSpace(const int32_t& acquisitionPoints) const ->bool;
        
        auto Clear()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}