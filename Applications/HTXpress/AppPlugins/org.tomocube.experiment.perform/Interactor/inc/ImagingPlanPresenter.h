#pragma once
#include <memory>

#include <IImagingConditionOutputPort.h>
#include "IImagingPlanView.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API ImagingPlanPresenter : public UseCase::IImagingConditionOutputPort {
    public:
        ImagingPlanPresenter(IImagingPlanView* view);
        ~ImagingPlanPresenter() override;

        auto Update(const UseCase::ImagingTimeTable::Pointer& table) -> void override;
        auto Update3DMinimumInterval(const UseCase::ImagingTimeTable::Pointer& table) -> void override;
        auto UpdateFOV(const double xInUm, const double yInUm) -> void override;
        auto UpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex, double xInMm, double yInMm, double widthInUm, double heightInUm) -> void override;
        auto ClearFLChannels() -> void override;
        auto UpdateBFEnabled(bool enabled) -> void override;
        auto UpdateHTEnabled(bool enabled) -> void override;
        auto UpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types) -> void override;
        auto UpdateFLChannelConfig(const int32_t channel, ChannelConfig::Pointer config) -> void override;
        auto UpdateAcquisitionLock(bool locked) -> void override;
        auto UpdateAcquisitionDataRequiredSpace(const int64_t& bytes) -> void override;
        auto UpdateTileActivation(bool enable) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}