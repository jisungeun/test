#pragma once

#include "PreviewPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API PreviewAcquisitionController {
    public:
        using WellIndex = AppEntity::WellIndex;
        using Position = AppEntity::Position;
        using Area = AppEntity::Area;

    public:
        PreviewAcquisitionController(PreviewPresenter* presenter);
        virtual ~PreviewAcquisitionController();

        auto CaptureArea(int32_t widthUm, int32_t heightUm,
                         std::function<bool()> stopFunc = {})->bool;

        auto SetCustomPreviewArea(double x, double y, int32_t widthUm, int32_t heightUm)->bool;
        auto DoCustomPreviewAreaSetting()->bool;
        auto CancelCustomPreviewAreaSetting()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}