#pragma once
#include <memory>

#include <ExperimentStatus.h>
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API IExperimentView {
    public:
        IExperimentView();
        virtual ~IExperimentView();

        virtual auto UpdateProgress(const UseCase::ExperimentStatus& status) -> void = 0;
        virtual auto NotifyStopped() -> void = 0;
    };
}