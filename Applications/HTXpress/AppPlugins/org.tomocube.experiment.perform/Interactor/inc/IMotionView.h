#pragma once

#include <Position.h>
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API IMotionView {
    public:
        IMotionView();
        virtual ~IMotionView();

        virtual auto UpdateStatus(const bool moving)->void = 0;
        virtual auto UpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position)->void = 0;
        virtual auto UpdateGlobalPosition(const AppEntity::Position& position)->void = 0;
        virtual auto UpdateGlobalPosition(AppEntity::Axis axis, double posInMm)->void = 0;
        virtual auto UpdateSelectedWell(const AppEntity::WellIndex wellIdx)->void = 0;
        virtual auto UpdateBestFocus(double posInMm)->void = 0;

        virtual auto ReportError(const QString& message) -> void = 0;
        virtual auto ReportAFFailure() -> void = 0;
    };
}
