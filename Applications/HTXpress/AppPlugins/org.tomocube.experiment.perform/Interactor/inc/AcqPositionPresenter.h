#pragma once
#include <memory>

#include <IAcquisitionPositionOutputPort.h>

#include "IAcqPositionView.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API AcqPositionPresenter : public UseCase::IAcquisitionPositionOutputPort {
    public:
        AcqPositionPresenter(IAcqPositionView* view = nullptr);
        ~AcqPositionPresenter();

        auto AddPosition(LocationIndex locationIdx, WellIndex wellIndex, Location::Pointer loc) -> void override;
        auto RefreshList(WellIndex wellIndex, QMap<LocationIndex, Location::Pointer>& locList) -> void override;
        auto DeletePosition(WellIndex wellIdx, LocationIndex locationIdx) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
