﻿#pragma once
#include <memory>

#include "HTX_Experiment_Perform_InteractorExport.h"

#include "SystemStoragePresenter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API SystemStorageController final {
    public:
        explicit SystemStorageController(SystemStoragePresenter::Pointer presenter);
        ~SystemStorageController();

        auto UpdateStorageInfo() const -> bool;
        auto InstallStorageMonitor() const -> bool;
        auto UpdateRootDrive() const -> bool;
        auto UpdateMinRequiredSpace() const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
