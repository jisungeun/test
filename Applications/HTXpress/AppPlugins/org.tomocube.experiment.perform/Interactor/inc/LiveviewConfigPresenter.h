#pragma once
#include <memory>

#include <ILiveviewConfigOutputPort.h>

#include "ILiveviewConfigView.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API LiveviewConfigPresenter : public UseCase::ILiveviewConfigOutputPort {
    public:
        LiveviewConfigPresenter(ILiveviewConfigView* view);
        ~LiveviewConfigPresenter() override;

        auto UpdateLiveConfig(const uint32_t intensity, const uint32_t exposure, const double gain) -> void override;
        auto UpdateAcquisitionConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain) -> void override;
        auto UpdateImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}