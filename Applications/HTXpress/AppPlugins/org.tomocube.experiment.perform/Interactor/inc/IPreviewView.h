#pragma once

#include <Position.h>
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API IPreviewView {
    public:
        IPreviewView();
        virtual ~IPreviewView();

        virtual auto Progress(double progress, AppEntity::Position& position) -> void = 0;
        virtual auto UpdateImageSize(int32_t xPixels, int32_t yPixels) -> void = 0;
        virtual auto UpdateBlock(int32_t xPos, int32_t yPos, const QImage& blockImage) -> void = 0;
        virtual auto UpdateCustomPreviewArea(double wellXinMM, double wellYinMM, int32_t widthUm, int32_t heightUm) -> void = 0;
        virtual auto StartCustomPreviewAreaSetting() -> void = 0;
        virtual auto CancelCustomPreviewAreaSetting() -> void = 0;
        virtual auto SetPreviewArea(double wellXinMM, double wellYinMM, double widthMM, double heightMM) -> void = 0;
    };
}