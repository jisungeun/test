#pragma once
#include <memory>

#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API SystemConfigController {
    public:
        SystemConfigController();
        ~SystemConfigController();

        auto Save()->bool;

        auto ApplyOnly()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}