#pragma once
#include <memory>

#include <ImagingTimeTable.h>
#include <ChannelConfig.h>

#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API IImagingPlanView {
    public:
        using ChannelConfig = AppEntity::ChannelConfig;

    public:
        IImagingPlanView();
        virtual ~IImagingPlanView();

        virtual auto Update(const UseCase::ImagingTimeTable::Pointer& table)->void = 0;
        virtual auto Update3DMinimumInterval(const UseCase::ImagingTimeTable::Pointer& table)->void = 0;
        virtual auto UpdateFOV(const double xInUm, const double yInUm)->void = 0;
        virtual auto UpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex, double xInMm, double yInMm, double widthInUm, double heightInUm)->void = 0;
        virtual auto ClearFLChannels()->void = 0;
        virtual auto UpdateBFEnabled(bool enabled)->void = 0;
        virtual auto UpdateHTEnabled(bool enabled)->void = 0;
        virtual auto UpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types)->void = 0;
        virtual auto UpdateFLChannelConfig(const int32_t channel, ChannelConfig::Pointer config)->void = 0;
        virtual auto UpdateAcquisitionLock(bool locked)->void=0;
        virtual auto UpdateAcquisitionDataRequiredSpace(const int64_t& bytes)->void = 0;
        virtual auto UpdateTileActivation(bool enable) -> void = 0;
    };
}