#pragma once
#include <memory>

#include "AcquisitionDataPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API AcquisitionDataController {
    public:
        AcquisitionDataController(AcquisitionDataPresenter* presenter);
        ~AcquisitionDataController();

        auto InstallDataMonitor()->bool;
        auto InstallDataScanner()->bool;
        auto ScanExperiment()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}