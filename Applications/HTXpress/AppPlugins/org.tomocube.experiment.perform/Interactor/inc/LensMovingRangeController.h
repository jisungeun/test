﻿#pragma once
#include <memory>

#include <AppEntityDefines.h>

#include "VesselPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API LensMovingRangeController {
    public:
        explicit LensMovingRangeController(VesselPresenter* presenter);
        ~LensMovingRangeController();

        auto SetSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM) -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
