#pragma once
#include <memory.h>
#include <QString>

#include "ExperimentIOPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API ExperimentIOController {
    public:
        ExperimentIOController(ExperimentIOPresenter* presenter);
        ~ExperimentIOController();

        auto Load(const QString& project, const QString& title)->bool;
        auto Reload()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}