#pragma once
#include <memory>

#include <IVesselOutputPort.h>

#include "IVesselView.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API VesselPresenter : public UseCase::IVesselOutputPort {
    public:
        VesselPresenter(IVesselView* view);
        ~VesselPresenter() override;

        auto UpdateSelected(AppEntity::WellIndex wellIdx) -> void override;
        auto UpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM) -> void override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}