#pragma once
#include <memory>

#include <IRunExperimentOutputPort.h>
#include <IQualityCheckOutputPort.h>

#include "IQualityCheckView.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API QualityCheckPresenter
        : public UseCase::IRunExperimentOutputPort
        , public UseCase::IQualityCheckOutputPort {
    public:
            using Pointer = std::shared_ptr<QualityCheckPresenter>;

    protected:
        QualityCheckPresenter(IQualityCheckView* view = nullptr);

    public:
        ~QualityCheckPresenter() override;

        static auto GetInstance(IQualityCheckView* view  = nullptr)->Pointer;

        auto UpdateProgress(const UseCase::ExperimentStatus& status) -> void override;
        auto NotifyStopped() -> void override;

        auto UpdateEvaluationProgress(double progress) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}