#pragma once
#include <memory>

#include <ImagingConfig.h>

#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API ILiveviewConfigView {
    public:
        ILiveviewConfigView();
        virtual ~ILiveviewConfigView();

        virtual auto UpdateLiveConfig(const uint32_t intensity, const uint32_t exposure, const double gain)->void = 0;
        virtual auto UpdateAcquisitionConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain)->void = 0;
        virtual auto UpdateImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode) -> void = 0;
    };
}