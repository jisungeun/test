#pragma once
#include <memory>

#include "AcqPositionPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API AcqPositionSetupController {
    public:
        using Area = AppEntity::Area;
        using WellIndex = AppEntity::WellIndex;
        using LocationIndex = AppEntity::LocationIndex;

    public:
        AcqPositionSetupController(AcqPositionPresenter* presenter);
        ~AcqPositionSetupController();

        auto AddCurrentPosition()->bool;
        auto AddCurrentArea(const Area& area, bool isTile)->bool;
        auto AddCurrentTileArea(double xInMm, double yInMm, const Area& area, bool isTile = true)->bool;

        auto DeletePosition(const WellIndex wellIdx, const LocationIndex locationIdx)->bool;

        auto CopyPositions(const WellIndex sourceWellIndex, const QList<LocationIndex>& sourceLocations,
                           const QList<WellIndex>& targetWells)->bool;

        auto AddMatrixPositions(const WellIndex sourceWellIndex, const LocationIndex sourceLocationIndex,
                                const int32_t cols, const int32_t rows, const double horGapMm, const double verGapMm,
                                const QList<WellIndex>& wells)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}