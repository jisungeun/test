#pragma once
#include <memory>
#include <QString>

#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API SystemSetupController {
    public:
        SystemSetupController();
        ~SystemSetupController();

        auto GenerateBackground(const QString& srcDir, const QString& destDir)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}