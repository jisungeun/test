﻿#pragma once
#include <memory>

#include <ISystemStorageOutputPort.h>

#include "HTX_Experiment_Perform_InteractorExport.h"
#include "ISystemStorageView.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API SystemStoragePresenter final : public UseCase::ISystemStorageOutputPort {
    public:
        using Self = SystemStoragePresenter;
        using Pointer = std::shared_ptr<Self>;

        explicit SystemStoragePresenter(ISystemStorageView::Pointer view);
        ~SystemStoragePresenter() override;

        auto UpdateSystemStorageSpace(const int64_t& totalBytes, const int64_t& availableBytes) -> void override;
        auto UpdateMinRequiredSpace(const int32_t& gigabytes) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
