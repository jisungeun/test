#pragma once
#include <memory>

#include <IRunExperimentOutputPort.h>

#include "IExperimentView.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API ExperimentPresenter : public UseCase::IRunExperimentOutputPort {
    private:
        using Pointer = std::shared_ptr<ExperimentPresenter>;

    protected:
        ExperimentPresenter(IExperimentView* view);

    public:
        ~ExperimentPresenter() override;
        static auto GetInstance(IExperimentView* view)->Pointer;

        auto UpdateProgress(const UseCase::ExperimentStatus& status) -> void override;
        auto NotifyStopped() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}