#pragma once
#include <memory>
#include <QStringList>

#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API ProjectController {
    public:
        ProjectController();
        ~ProjectController();

        auto GetProjects() const->QStringList;
        auto GetExperiments(const QString& project) const->QStringList;
    };
}
