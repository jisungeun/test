#pragma once
#include <memory>

#include "MotionPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API VesselController {
    public:
        VesselController(MotionPresenter* presenter);
        ~VesselController();

        auto SetCurrentWell(AppEntity::WellIndex wellIdx)->bool;
        auto SetSampleStagePosition(AppEntity::WellIndex wellIdx, double xMM, double yMM)->bool;
        auto SetSampleStagePosition(AppEntity::WellIndex wellIdx, double zMM)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}