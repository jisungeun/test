#pragma once

#include <Position.h>
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API IQualityCheckView {
    public:
        IQualityCheckView();
        virtual ~IQualityCheckView();

        virtual auto UpdateAcquisitionProgress(double progress)->void = 0;
        virtual auto NotifyAcquisitionError(const QString& error)->void = 0;
        virtual auto NotifyAcquisitionStopped()->void = 0;
        virtual auto UpdateEvaluationProgress(double progess)->void = 0;
    };
}