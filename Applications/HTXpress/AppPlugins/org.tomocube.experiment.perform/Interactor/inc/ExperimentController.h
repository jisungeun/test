#pragma once
#include <memory>

#include "ExperimentPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API ExperimentController {
    public:
        ExperimentController(ExperimentPresenter* presenter);
        ~ExperimentController();

        auto RunExperiment()->bool;
        auto TestExperiment()->bool;
        auto StopExperiment()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}