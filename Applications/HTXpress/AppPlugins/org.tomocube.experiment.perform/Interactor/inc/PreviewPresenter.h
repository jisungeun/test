#pragma once

#include <IPreviewOutputPort.h>

#include "IPreviewView.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API PreviewPresenter : public UseCase::IPreviewOutputPort {
    public:
        PreviewPresenter(IPreviewView* view);
        ~PreviewPresenter() override;

        auto Progress(double progress, AppEntity::Position& position) -> void override;
        auto UpdateImageSize(int32_t xPixels, int32_t yPixels) -> void override;
        auto UpdateBlock(int32_t xPos, int32_t yPos, const QImage& blockImage) -> void override;

        auto UpdateCustomPreviewArea(double wellXinMM, double wellYinMM, int32_t widthUM, int32_t heightUM) -> void override;
        auto DoCustomPreviewAreaSetting() -> void override;
        auto CancelCustomPreviewAreaSetting() -> void override;
        auto SetPreviewArea(double wellXinMM, double wellYinMM, double widthMM, double heightMM) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}