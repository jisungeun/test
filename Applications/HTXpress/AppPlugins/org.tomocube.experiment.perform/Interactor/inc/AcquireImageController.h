#pragma once
#include <memory>
#include <QList>

#include <ScanConfig.h>
#include <ChannelConfig.h>

#include "ExperimentPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API AcquireImageController {
    public:
        using ChannelConfig = AppEntity::ChannelConfig;

        struct ImagingType {
            AppEntity::Modality modality{ AppEntity::Modality::HT };
            QList<int32_t> channels;
            bool is3D{ true };
            bool isGray{ true };
        };

    public:
        AcquireImageController(ExperimentPresenter* presenter);
        ~AcquireImageController();

        auto SetFieldOfView(double xInUm, double yInUm)->bool;
        auto SetFLScanConfig(const AppEntity::ScanConfig& scanConfig)->bool;
        auto SetFLChannel(const int32_t channel, const ChannelConfig& channelConfig)->bool;

        auto Acquire(const QList<ImagingType>& types, double xInMm, double yInMm, double roiXInUm, double roiYInUm)->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}