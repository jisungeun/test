﻿#pragma once
#include <memory>

#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API ISystemStorageView {
    public:
        using Self = ISystemStorageView;
        using Pointer = std::shared_ptr<Self>;

        ISystemStorageView();
        virtual ~ISystemStorageView();

        virtual auto UpdateSystemStorageSpace(const int64_t& totalBytes, const int64_t& availableBytes) -> void = 0;
        virtual auto UpdateMinRequiredSpace(const int32_t& minRequiredGigabytes) -> void = 0;
    };
}