#pragma once
#include <memory>

#include <InstrumentPresenter.h>
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API InstrumentController {
    public:
        InstrumentController(InstrumentPresenter* presenter = nullptr);
        ~InstrumentController();

        auto StartInitialize()->bool;

        auto StartLiveBF()->bool;
        auto StartLiveFL(int32_t channel)->bool;
        auto Recover()->bool;

        auto ChangeLiveBF()->bool;
        auto ChangeLiveFL(int32_t channel)->bool;

        auto LoadVessel()->bool;
        auto UnloadVessel()->bool;

        auto PerformAutoFocus()->bool;
        auto EnableAutoFocus()->bool;
        auto DisalbeAutoFocus()->bool;
        auto SetBestFocusCurrent()->bool;
        auto SetBestFocusTarget(int32_t value)->bool;
        auto ScanFocus(double startMm, double distMm, int32_t count)->QList<int32_t>;
        auto ReadAFSensorValue()->int32_t;

        auto PerformCondenserAutoFocus()->bool;
        auto CalibrateHTIllumination()->bool;
        auto ShowHTIlluminationSetupPattern(double NA, int32_t intensity)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}