#pragma once
#include <memory>

#include <IAcquireImageOutputPort.h>

#include "IAcquireImageView.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API AcquireImagePresenter : public UseCase::IAcquireImageOutputPort {
    public:
        AcquireImagePresenter(IAcquireImageView* view);
        ~AcquireImagePresenter() override;

        auto UpdateProgress(double progress) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}