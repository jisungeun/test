#pragma once
#include <memory>
#include <QList>

#include "InstrumentPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API PreviewCalibrationController {
    public:
        using ValueList = QList<QPair<int32_t, double>>;

    public:
        PreviewCalibrationController(InstrumentPresenter* presenter = nullptr);
        virtual ~PreviewCalibrationController();

        auto MeasureExposure(ValueList& values, double& coeffP, double& coeffQ)->bool;
        auto MeasureGain(ValueList& values, double& coeffA, double& coeffB)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}