﻿#pragma once

#include <memory>

#include "MarkPositionPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API MarkPositionSetupController {
    public:
        explicit MarkPositionSetupController(MarkPositionPresenter* presenter);
        ~MarkPositionSetupController();

        auto AddMarkPosition(double xInMm, double yInMm, const AppEntity::Area& area, bool isTile) -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
