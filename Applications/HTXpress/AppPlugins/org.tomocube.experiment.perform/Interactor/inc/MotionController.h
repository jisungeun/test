#pragma once

#include <Position.h>

#include "MotionPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API MotionController {
    public:
        MotionController(MotionPresenter* presenter = nullptr);
        ~MotionController();

        auto GetPosition(const AppEntity::WellIndex& wellIdx, AppEntity::Position& position)->bool;
        auto GetGlobalPosition(AppEntity::Position& position)->bool;
        auto GetGlobalPosition(AppEntity::Position& position, AppEntity::WellIndex& wellIdx)->bool;
        auto GetGlobalPosition(AppEntity::Axis axis, double& posInMM)->bool;

        auto MoveXY(const AppEntity::WellIndex wellIdx, const double xInMM, const double yInMM)->bool;
        auto MoveMM(const AppEntity::WellIndex& wellIdx, const AppEntity::Axis& axis, const double& posInMM)->bool;
        auto MoveMM(const AppEntity::Axis axis, const double posInMm)->bool;
        auto MoveXYInGlobal(const double xInMM, const double yInMM)->bool;
        auto MoveRelativePixels(int32_t relPosX, int32_t relPosY)->bool;
        auto MoveRelativeMM(const AppEntity::Axis axis, const double distMm)->bool;
        auto MoveRelativeUM(const AppEntity::Axis axis, const double distUm)->bool;
        auto MoveCAxis(double targetInMm)->bool;

        auto StartJog(const AppEntity::Axis axis, bool plusDirection)->bool;
        auto StopJog()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}