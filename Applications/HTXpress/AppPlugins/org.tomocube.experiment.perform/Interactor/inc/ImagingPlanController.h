#pragma once
#include <memory>
#include <QMap>

#include <ImagingConfig.h>

#include "ImagingPlanPresenter.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API ImagingPlanController {
    public:
        struct ImagingType {
            AppEntity::Modality modality{ AppEntity::Modality::HT };
            QList<int32_t> channels;
            bool is3D{ true };
            bool isGray{ true };
        };

    public:
        ImagingPlanController(bool updateExperiment, ImagingPlanPresenter* presenter);
        ~ImagingPlanController();

        auto SetFieldOfView(const double xInUm, const double yInUm, double& adjustedXInUm, double& adjustedYInUm)->bool;
        auto SetFLScanConfig(const AppEntity::ScanConfig& scanConfig)->bool;

        auto SetTileScanArea(bool enable, AppEntity::WellIndex wellIndex, double x, double y, double width, double height)->bool;

        auto ClearFLChannels()->bool;
        auto SetFLChannel(const int32_t channel, const AppEntity::ChannelConfig& channelConfig)->bool;

        auto SetHTEnabled(const bool is3D, const bool enabled)->bool;
        auto SetFLEnabled(const bool is3D, const bool enabled)->bool;
        auto SetBFEnabled(const bool enabled)->bool;

        auto SetAcquisitionLock(bool locked)->bool;
        auto TileDeactivation() -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}