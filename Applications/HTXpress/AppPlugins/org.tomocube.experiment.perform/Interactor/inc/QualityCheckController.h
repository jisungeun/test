#pragma once
#include <memory>
#include <QList>

#include <BeadScore.h>

#include "QualityCheckPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API QualityCheckController {
    public:
        struct Data {
            QString path;
            int32_t xInPixel{ 0 };
            int32_t yInPixel{ 0 };
        };

    public:
        QualityCheckController(QualityCheckPresenter* presenter = nullptr);
        ~QualityCheckController();

        auto AcquireAllPoints()->bool;
        auto AcquireTheCurrent(int32_t repeatCount)->bool;
        auto AcquireMatrixPoints(int32_t xTiles, int32_t yTiles, int32_t xGapUm, int32_t yGapUm)->bool;

        auto EvaluateBeadScore(const QList<Data>& dataList)->QList<Entity::BeadScore>;
        auto GetOutputPath() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

