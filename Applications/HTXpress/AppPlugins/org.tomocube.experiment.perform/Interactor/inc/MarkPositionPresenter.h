﻿#pragma once

#include <memory>

#include <IMarkPositionOutputPort.h>

#include "IMarkPositionView.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API MarkPositionPresenter : public UseCase::IMarkPositionOutputPort {
    public:
        explicit MarkPositionPresenter(IMarkPositionView* view = nullptr);
        ~MarkPositionPresenter() override;

        auto AddMarkPosition(AppEntity::WellIndex wellIndex, AppEntity::Location::Pointer location) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
