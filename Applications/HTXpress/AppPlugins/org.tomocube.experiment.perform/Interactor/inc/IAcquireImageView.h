#pragma once
#include <memory>

#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API IAcquireImageView {
    public:
        IAcquireImageView();
        virtual ~IAcquireImageView();

        virtual auto UpdateProgress(double progress)->void = 0;
    };
}