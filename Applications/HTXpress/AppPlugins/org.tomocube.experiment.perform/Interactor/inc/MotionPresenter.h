#pragma once
#include <memory>

#include <IMotionOutputPort.h>
#include "IMotionView.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API MotionPresenter : public UseCase::IMotionOutputPort {
    public:
        MotionPresenter(IMotionView* view);
        ~MotionPresenter() override;

        auto UpdateStatus(bool moving) -> void override;
        auto UpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position) -> void override;
        auto UpdateGlobalPosition(const AppEntity::Position& position) -> void override;
        auto UpdateGlobalPosition(AppEntity::Axis axis, double posInMm) -> void override;
        auto UpdateSelectedWell(const AppEntity::WellIndex wellIdx)->void override;
        auto UpdateBestFocus(double posInMm) -> void override;

        auto ReportError(const QString& message) -> void override;
        auto ReportAFFailure() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}