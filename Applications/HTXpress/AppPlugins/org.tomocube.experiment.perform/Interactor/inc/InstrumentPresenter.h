#pragma once
#include <memory>

#include <IInstrumentOutputPort.h>
#include "IInstrumentView.h"
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API InstrumentPresenter : public UseCase::IInstrumentOutputPort {
    public:
        InstrumentPresenter(IInstrumentView* view = nullptr);
        ~InstrumentPresenter() override;

        auto UpdateFailed(const QString& message) -> void override;
        auto UpdateProgress(double progress, const QString& message) -> void override;

        auto UpdateGlobalPosition(const AppEntity::Position& position) -> void override;
        auto ReportAFFailed() -> void override;
        auto UpdateBestFocus(double posInMm) -> void override;
        auto AutoFocusEnabled(bool enable) -> void override;

        auto LiveStarted() -> void override;
        auto LiveStopped() -> void override;
        auto LiveImagingFailed(const QString& message) -> void override;

        auto VesselLoaded() -> void override;
        auto VesselUnloaded() -> void override;

        auto UpdateCAFScores(const QList<double>& scores, int32_t bestFocusIndex) -> void override;
        auto UpdateHTIlluminationResult(const QImage& image) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}