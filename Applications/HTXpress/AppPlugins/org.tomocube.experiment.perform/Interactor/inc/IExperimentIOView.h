#pragma once
#include <memory>

#include <Experiment.h>
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API IExperimentIOView {
    public:
        IExperimentIOView();
        virtual ~IExperimentIOView();

        virtual auto Update(AppEntity::Experiment::Pointer experiment, bool reloaded)->void = 0;
        virtual auto Error(const QString& message)->void = 0;
    };
}