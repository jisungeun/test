#pragma once
#include <memory>

#include <Location.h>
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API IAcqPositionView {
    public:
        using LocationIndex = AppEntity::LocationIndex;
        using Location = AppEntity::Location;
        using WellIndex = AppEntity::WellIndex;

    public:
        IAcqPositionView();
        ~IAcqPositionView();

        virtual auto AddPosition(LocationIndex locationIdx, WellIndex wellIndex, Location::Pointer loc)->void = 0;
        virtual auto RefreshList(WellIndex wellIndex, QMap<LocationIndex, Location::Pointer>& locList) -> void = 0;
        virtual auto DeletePosition(WellIndex wellIdx, LocationIndex locationIdx) -> void = 0;
    };
}