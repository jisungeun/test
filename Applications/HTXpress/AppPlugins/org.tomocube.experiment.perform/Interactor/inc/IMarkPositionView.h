﻿#pragma once

#include <memory>

#include <Location.h>
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
class HTX_Experiment_Perform_Interactor_API IMarkPositionView {
public:
    IMarkPositionView();
    virtual ~IMarkPositionView();

    virtual auto AddMarkPosition(AppEntity::WellIndex wellIndex, AppEntity::Location::Pointer location)->void=0;
};
}