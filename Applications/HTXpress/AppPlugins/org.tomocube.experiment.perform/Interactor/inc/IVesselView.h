#pragma once
#include <AppEntityDefines.h>
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API IVesselView {
    public:
        IVesselView();
        virtual ~IVesselView();

        virtual auto UpdateCurrentWell(AppEntity::WellIndex wellIdx)->void = 0;
        virtual auto UpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM)->void = 0;
    };
}