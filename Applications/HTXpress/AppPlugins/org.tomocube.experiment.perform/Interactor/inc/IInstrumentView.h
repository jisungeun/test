#pragma once
#include <memory>
#include <QString>

#include <Position.h>
#include "HTX_Experiment_Perform_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Interactor {
    class HTX_Experiment_Perform_Interactor_API IInstrumentView {
    public:
        IInstrumentView();
        virtual ~IInstrumentView();

        virtual auto UpdateFailed(const QString& message)->void = 0;
        virtual auto UpdateProgress(double progress, const QString& message=QString())->void = 0;

        virtual auto UpdateGlobalPosition(const AppEntity::Position& position)->void = 0;
        virtual auto ReportAFFailed()->void = 0;
        virtual auto UpdateBestFocus(double posInMm)->void = 0;
        virtual auto AutoFocusEnabled(bool enable) -> void = 0;

        virtual auto LiveStarted()->void = 0;
        virtual auto LiveStopped()->void = 0;
        virtual auto LiveImagingFailed(const QString& message)->void = 0;

        virtual auto VesselLoaded()->void = 0;
        virtual auto VesselUnloaded()->void = 0;

        virtual auto UpdateCAFScores(const QList<double>& scores, int32_t bestFocusIndex)->void = 0;
        virtual auto UpdateHTIlluminationResult(const QImage& image)->void = 0;
    };
}