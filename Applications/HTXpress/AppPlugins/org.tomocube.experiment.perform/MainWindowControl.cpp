#define LOGGER_TAG "[MainControl]"
#include <QStandardPaths>
#include <QCoreApplication>
#include <QDir>

#include <TCLogger.h>
#include <System.h>
#include <SystemStatus.h>
#include <SessionManager.h>
#include <UserManager.h>
#include <Model.h>

#include <InstrumentController.h>
#include <ExperimentIOController.h>
#include <MotionController.h>

#include <InstrumentPlugin.h>
#include <ExperimentRunner.h>
#include <PreviewAcquisition.h>
#include <PreviewCalibrator.h>
#include <ScanTimeCalcaultorFactory.h>
#include <ExperimentReaderPlugin.h>
#include <ExperimentWriterPlugin.h>
#include <ChannelConfigIOPlugin.h>
#include <DataManager.h>
#include <ImagingSystemPlugin.h>
#include <CondenserAutofocus.h>
#include <HTIlluminationAutoCalibration.h>
#include <SystemConfigWriter.h>
#include <SystemStorageManager.h>
#include <UseCaseLogger.h>
#include <BGGeneratorPlugin.h>
#include <BeadEvaluatorPlugin.h>
#include <AcquisitionDataSpaceCalculator.h>
#include <ImagingProfileLoaderPlugin.h>

#include "Internal/InstrumentUpdater.h"
#include "Internal/ExperimentIOUpdater.h"
#include "Internal/MotionUpdater.h"

#include "MainWindowControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    using InstrumentPlugin          = Plugins::Instrument::Instrument;
    using ExpRunnerPlugin           = Plugins::ExperimentRunner::Runner;
    using PreviewAcquisitionPlugin  = Plugins::PreviewAcquisition::PreviewAcquisition;
    using PreviewCalibratorPlugin   = Plugins::PreviewCalibrator::Plugin;
    using ScanTimeCalculatorFactory = Plugins::ScanTimeCalculator::Factory;
    using ScanTimeCalculatorPlugin  = UseCase::IScanTimeCalculator;
    using UseCaseLoggerPlugin       = Plugins::UseCaseLogger::Logger;
    using ExperimentReaderPlugin    = Plugins::ExperimentReader::Reader;
    using ExperimentWriterPlugin    = Plugins::ExperimentWriter::Writer;
    using ImagingSystemPlugin       = Plugins::ImagingSystem::Plugin;
    using ChannelConfigIOPlugin     = Plugins::ChannelConfigIO::Plugin;
    using DataManagerPlugin         = Plugins::DataManager::DataManager;
    using CondenserAFPlugin         = Plugins::CondenserAutofocus::Plugin;
    using HTIlluminationCalPlugin   = Plugins::HTIlluminationAutoCalibration::Plugin;
    using SystemConfigWriterPlugin  = Plugins::SystemConfigWriter::Writer;
    using SystemStorageManager      = Plugins::SystemStorageManager::SystemStorageManager;
    using BGGeneratorPlugin         = Plugins::BGGenerator::Plugin;
    using BeadEvaluatorPlugin       = Plugins::BeadEvaluator::Plugin;
    using AcqDataSpaceCalculatorPlugin = Plugins::AcquisitionDataSpaceCalculator::AcquisitionDataSpaceCalculator;
    using ImagingProfileLoaderPlugin = Plugins::ImagingProfileLoader::Loader;

    struct MainWindowControl::Impl {
        std::shared_ptr<InstrumentPlugin> instrument{ new InstrumentPlugin() };
        std::shared_ptr<ExpRunnerPlugin> experimentRunner{ new ExpRunnerPlugin() };
        std::shared_ptr<PreviewAcquisitionPlugin> previewAcquisition{ new PreviewAcquisitionPlugin() };
        std::shared_ptr<PreviewCalibratorPlugin> previewCalibrator{ new PreviewCalibratorPlugin() };
        std::shared_ptr<ScanTimeCalculatorPlugin> scanTimeCalculator{ ScanTimeCalculatorFactory::Build() };
        std::shared_ptr<UseCaseLoggerPlugin> usecaseLogger{ new UseCaseLoggerPlugin() };
        std::shared_ptr<ExperimentReaderPlugin> experimentReader{ new ExperimentReaderPlugin() };
        std::shared_ptr<ExperimentWriterPlugin> experimentWriter{ new ExperimentWriterPlugin() };
        std::shared_ptr<ChannelConfigIOPlugin> channelConfigIO{ new ChannelConfigIOPlugin() };
        std::shared_ptr<ImagingSystemPlugin> imagingSystem{ new ImagingSystemPlugin() };
        std::shared_ptr<DataManagerPlugin> dataManager{ new DataManagerPlugin() };
        std::shared_ptr<CondenserAFPlugin> condenserAf{ new CondenserAFPlugin() };
        std::shared_ptr<HTIlluminationCalPlugin> htIllumCal{ new HTIlluminationCalPlugin() };
        std::shared_ptr<SystemConfigWriterPlugin> sysConfigWriter{ new SystemConfigWriterPlugin() };
        std::shared_ptr<SystemStorageManager> systemStorageManager{ new SystemStorageManager() };
        std::shared_ptr<BGGeneratorPlugin> bgGenerator{ new BGGeneratorPlugin() };
        std::shared_ptr<BeadEvaluatorPlugin> beadEvaluator{ new BeadEvaluatorPlugin() };
        std::shared_ptr<AcqDataSpaceCalculatorPlugin> acqDataSpaceCalculator{ new AcqDataSpaceCalculatorPlugin() };
        std::shared_ptr<ImagingProfileLoaderPlugin> imagingProfileLoader{ new ImagingProfileLoaderPlugin() };
    };

    MainWindowControl::MainWindowControl() : d{new Impl} {
        const auto topPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
        const auto configFolder = QString("%1/config").arg(topPath);
        const auto dataFolderPath = AppEntity::System::GetSystemConfig()->GetDataDir();

        d->sysConfigWriter->SetPath(QString("%1/system.ini").arg(configFolder));
        d->channelConfigIO->SetTopPath(configFolder);
        d->systemStorageManager->SetPath(dataFolderPath);

        const auto appDirPath = QCoreApplication::applicationDirPath();
        const auto evalTemp = QString("%1/.tsx.eval").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation));

        auto beadEvalPath = [=](const QString& name)->QString {
            auto path = QString("%1/%2").arg(appDirPath).arg(name);
            if (!QFile::exists(path)) {
                path = QString("%1/HTProc/%2").arg(appDirPath).arg(name);
            }

            return path;
        };

        d->beadEvaluator->SetEvaluationApp(beadEvalPath("TCBeadEvaluationMatlabApp.exe"), evalTemp);
        d->beadEvaluator->SetMoudlePath(beadEvalPath("PSFModule_v1_4_1_c.ctf"), beadEvalPath("BeadModule.ctf"));

        const auto psfDirPath = QString("%1/PSF").arg(topPath);
        d->beadEvaluator->SetPSFFolder(psfDirPath);
    }

    MainWindowControl::~MainWindowControl() {
        QLOG_INFO() << "Clean up";
    }

    auto MainWindowControl::CleanUp() -> void {
        d->instrument->CleanUp();
    }

    auto MainWindowControl::StartInitialize() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        if(!controller.StartInitialize()) {
            return false;
        }

        return controller.StartLiveBF();
    }

    auto MainWindowControl::LoadExperiment(const QString& projectTitle, const QString& experimentTitle) -> bool {
        auto updater = ExperimentIOUpdater::GetInstance();
        auto presenter = Interactor::ExperimentIOPresenter(updater.get());
        auto controller = Interactor::ExperimentIOController(&presenter);

        return controller.Load(projectTitle, experimentTitle);
    }

    auto MainWindowControl::ReadyToLoadVessel() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);
        return controller.UnloadVessel();
    }

    auto MainWindowControl::IsExperimentRunning() -> bool {
        return AppEntity::SystemStatus::GetInstance()->GetBusy();
    }

    auto MainWindowControl::StartJog(AppEntity::Axis axis, bool plusDirection) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.StartJog(axis, plusDirection);
    }

    auto MainWindowControl::StopJog() -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.StopJog();
    }
}
