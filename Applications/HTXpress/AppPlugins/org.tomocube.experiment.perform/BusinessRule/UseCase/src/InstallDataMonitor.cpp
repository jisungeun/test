#include "InstallDataMonitor.h"

#include <IDataManager.h>

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
	struct InstallDataMonitor::Impl {
		IDataOutputPort* output{ nullptr };
	};

    InstallDataMonitor::InstallDataMonitor() : IUseCase("InstallDataMonitor"), d{ new Impl } {
    }

    InstallDataMonitor::~InstallDataMonitor() {
    }

    auto InstallDataMonitor::SetDataOutputPort(IDataOutputPort* outputPort)->void {
        d->output = outputPort;
    }

    auto InstallDataMonitor::Perform() -> bool {
        if (d->output == nullptr) {
            Error("Invalid output port.");
            return false;
        }

        IDataManager::GetInstance()->InstallDataMonitorOutputPort(d->output);
        return true;
    }
}
