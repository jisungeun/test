#include <QCoreApplication>

#include <System.h>
#include <SystemStatus.h>

#include "IInstrument.h"
#include "ICondenserAutofocus.h"
#include "PerformCondenserAutofocus.h"

#include <QThread>

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct PerformCondenserAutofocus::Impl {
        PerformCondenserAutofocus* p{ nullptr };
        IInstrumentOutputPort* output{ nullptr };

        QList<double> cafScores;

        Impl(PerformCondenserAutofocus* p, IInstrumentOutputPort* output) : p{ p }, output{ output } {
        }

        auto ReportError(const QString& message);
        auto UpdateProgress(double progress, const QString& message = QString())->void;
        auto MoveCAxis(double pos, int32_t timeoutSec = 120)->bool;
        auto Perform(IImagePort::Pointer imagePort)->std::tuple<bool, int32_t>;
        auto PerformDebug()->double;

        auto CAFStartPosPulse()->int32_t;
        auto CAFIntervalPulse()->int32_t;
    };

    auto PerformCondenserAutofocus::Impl::ReportError(const QString& message) {
        p->Error(message);
        if(output) output->UpdateFailed(message);
    }

    auto PerformCondenserAutofocus::Impl::UpdateProgress(double progress, const QString& message) -> void {
        if(output) output->UpdateProgress(progress, message);
    }

    auto PerformCondenserAutofocus::Impl::MoveCAxis(double pos, int32_t timeoutSec) -> bool {
        auto instrument = IInstrument::GetInstance();
        instrument->MoveAxis(AppEntity::Axis::C, pos);

        int32_t checkCount = 0;

        auto motionStatus = instrument->CheckAxisMotion();
        while(motionStatus.moving) {
            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 100);
            motionStatus = instrument->CheckAxisMotion();
            checkCount++;

            if(checkCount >= (timeoutSec*10)) return false;
            if(motionStatus.error) return false;
        }

        return true;
    }

    auto PerformCondenserAutofocus::Impl::Perform(IImagePort::Pointer imagePort) -> std::tuple<bool, int32_t> {
        auto instrument = IInstrument::GetInstance();
        auto condenserAf = ICondenserAutofocus::GetInstance();

        int32_t stackIndex = 0;

        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto NA = AppEntity::SystemStatus::GetInstance()->GetNA();
        auto[ptnIndex, intensity, zOffset] = sysConfig->GetCondenserAFParameter(NA);
        p->Print(QString("NA=%1 Pattern Index=%2 Intensity=%3 zOffset=%4").arg(NA).arg(ptnIndex).arg(intensity).arg(zOffset));

        const auto afStarted = instrument->PerformCondenserAutoFocus(imagePort, ptnIndex, intensity);
        if(!afStarted) {
            ReportError(QString("It fails to run condenser autofocus"));
            return std::make_tuple(false, stackIndex);
        }

        int32_t waitCount{ 0 };
        do {
            auto [error, progress] = instrument->CheckCondenserAutoFocusProgress();
            if(progress == 1.0) break;
            if(error) {
                ReportError(QString("Condenser autofocus is failed"));
                return std::make_tuple(false, stackIndex);
            }

            UpdateProgress(progress*0.8);
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

            QThread::msleep(100);
            waitCount++;

            if(waitCount > 1200) {
                ReportError(QString("It fails to get all images"));
                return std::make_tuple(false, stackIndex);
            }
        } while(true);

        bool found{ false };
        std::tie(found, stackIndex) = condenserAf->FindBestFocus();

        cafScores = condenserAf->GetScores();

        if(!found) {
            ReportError(QString("It fails to find best position of condenser"));
            return std::make_tuple(false, stackIndex);
        }

        return std::make_tuple(true, stackIndex);
    }

    auto PerformCondenserAutofocus::Impl::PerformDebug() -> double {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto NA = AppEntity::SystemStatus::GetInstance()->GetNA();
        auto[ptnIndex, intensity, zOffset] = sysConfig->GetCondenserAFParameter(NA);
        p->Print(QString("NA=%1 Pattern Index=%2 Intensity=%3 zOffset=%4").arg(NA).arg(ptnIndex).arg(intensity).arg(zOffset));

        p->Print("IT SKIPS CONDENSER AUTOFOCUS WHICH IS NOT SUPPORTED ON DEBUG MODE");
        return 6.0;
    }

    auto PerformCondenserAutofocus::Impl::CAFStartPosPulse() -> int32_t {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        if (sysConfig->IsCondenserAFScanParameterOverriden()) {
            return sysConfig->GetCondenserAFScanStartPosPulse();
        }

        auto model = AppEntity::System::GetModel();
        return model->CondenserAFScanStartPosPulse();
    }

    auto PerformCondenserAutofocus::Impl::CAFIntervalPulse() -> int32_t {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        if (sysConfig->IsCondenserAFScanParameterOverriden()) {
            return sysConfig->GetCondenserAFScanIntervalPulse();
        }

        auto model = AppEntity::System::GetModel();
        return model->CondenserAFScanIntervalPulse();
    }

    PerformCondenserAutofocus::PerformCondenserAutofocus(IInstrumentOutputPort* output)
        : IUseCase("PerformCondenserAutofocus")
        , d{new Impl(this, output) } {
    }

    PerformCondenserAutofocus::~PerformCondenserAutofocus() {
    }

    auto PerformCondenserAutofocus::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();
        auto condenserAf = ICondenserAutofocus::GetInstance();

        d->UpdateProgress(0, "Performing condenser autofocus");

        instrument->StopLive();

        const auto startPos = 0;
        if(!d->MoveCAxis(startPos)) {
            Print(QString("It fails to to start position, %1mm").arg(0));
        }

        condenserAf->EnableLogging(IsServiceEngineer());
        auto imagePort = condenserAf->GetImagePort();
        
#ifdef _DEBUG
        constexpr bool isSimulation = true;
#else
        const auto isSimulation = AppEntity::System::GetSystemConfig()->GetSimulation();
#endif
        int32_t bestFoucsIndex = 0;
        if(isSimulation) {
            const auto bestFocusPos = d->PerformDebug();
            Print(QString("It moves the condenser axis to %1mm").arg(bestFocusPos));
            instrument->MoveAxis(AppEntity::Axis::C, bestFocusPos);
        } else {
            const auto[result, stackIndex] = d->Perform(imagePort);
            bestFoucsIndex = stackIndex;

            instrument->FinishCondenserAutoFocus(imagePort);
            instrument->ResumeLive();

            if(!result) {
                Print(QString("The condenser axis returns to the %1mm").arg(startPos));
                instrument->MoveAxis(AppEntity::Axis::C, startPos);
            } else {
                auto model = AppEntity::System::GetModel();
                auto sysConfig = AppEntity::System::GetSystemConfig();
                auto NA = AppEntity::SystemStatus::GetInstance()->GetNA();

                const auto ppm = model->AxisResolutionPPM(AppEntity::Model::Axis::C);
                const auto offsetMM = ((stackIndex * 1.0) * d->CAFIntervalPulse() + d->CAFStartPosPulse()) / ppm;
                const auto zCompensationMM = std::get<2>(sysConfig->GetCondenserAFParameter(NA));

                const auto bestFocusPos = startPos + offsetMM + zCompensationMM;
                auto log = QString("It moves the condenser axis to %1mm [start:%2 offset:%3 compensation:%4]")
                                .arg(bestFocusPos)
                                .arg(startPos)
                                .arg(offsetMM)
                                .arg(zCompensationMM);
                condenserAf->SaveLog(log);
                Print(log);

                instrument->MoveAxis(AppEntity::Axis::C, bestFocusPos);
            }
        }

        int32_t waitCount = 0;
        auto motionStatus = instrument->CheckAxisMotion();
        while(motionStatus.moving) {
            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 100);
            motionStatus = instrument->CheckAxisMotion();

            d->UpdateProgress(std::min(0.8 + waitCount*0.01, 0.99));
        }

        const auto cAxisReadyPos = instrument->GetAxisPositionMM(AppEntity::Axis::C);
        AppEntity::SystemStatus::GetInstance()->SetCurrentCondensorPosition(cAxisReadyPos);

        if(d->output) {
            d->output->UpdateCAFScores(d->cafScores, bestFoucsIndex);
        }

        d->UpdateProgress(1.0);

        return true;
    }
}
