#include <System.h>
#include <SessionManager.h>
#include <SystemStatus.h>

#include "IExperimentReader.h"
#include "ReloadCurrentExperiment.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ReloadCurrentExperiment::Impl {
        IExperimentOutputPort* output{ nullptr };

        auto Error(ReloadCurrentExperiment* p, const QString& message)->void;
    };

    auto ReloadCurrentExperiment::Impl::Error(ReloadCurrentExperiment* p, const QString& message) -> void {
        p->Error(message);
        if(output) output->Error(message);
    }

    ReloadCurrentExperiment::ReloadCurrentExperiment(IExperimentOutputPort* output)
        : IUseCase("ReloadCurrentExperiment")
        , d{new Impl} {
        d->output = output;
    }

    ReloadCurrentExperiment::~ReloadCurrentExperiment() {
    }

    auto ReloadCurrentExperiment::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
		const auto path = sysStatus->GetExperimentPath();

        auto* reader = IExperimentReader::GetInstance();
        if(!reader) {
            d->Error(this, "No experiment reader exists");
            return false;
        }

        auto experiment = reader->Read(path);
        if(!experiment) {
             d->Error(this, QString("It fails read an experiment from %1").arg(path));
            return false;
        }

        sysStatus->SetExperiment(experiment, path);

        if(d->output) d->output->Update(experiment, true);

        return true;
    }
}