#include <QCoreApplication>
#include <QElapsedTimer>

#include <System.h>

#include <SystemStatus.h>

#include "Utility.h"
#include "IPreviewAcquisition.h"
#include "IInstrument.h"
#include "CapturePreview.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct CapturePreview::Impl {
        IPreviewOutputPort* output{ nullptr };
        std::function<bool()> stopFunc;

        AppEntity::WellIndex wellIndex;
        AppEntity::Position positionInWell;
        AppEntity::Area area;

        auto MakeImagingSequence(AppEntity::Area fov)->AppEntity::ImagingSequence::Pointer;
        auto FocusReadyMM() const->double;
        auto UpdateTiles(int32_t maxCount, IPreviewAcquisition* previewAcquisition)->void;
    };

    auto CapturePreview::Impl::MakeImagingSequence(AppEntity::Area fov) -> AppEntity::ImagingSequence::Pointer {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto channelConfig = sysStatus->GetChannelConfig(AppEntity::ImagingMode::BFGray);

        auto imgCond = std::make_shared<AppEntity::ImagingConditionBF>();
        imgCond->SetExposure(channelConfig->GetCameraExposureUSec());
        imgCond->SetInterval(channelConfig->GetCameraIntervalUSec());
        imgCond->SetIntensity(channelConfig->GetLightIntensity());
        imgCond->SetColorMode(false);

        imgCond->SetSliceStart(0);
        imgCond->SetSliceCount(1);
        imgCond->SetSliceStep(0);
        imgCond->SetDimension(2);

        auto sequence = std::make_shared<AppEntity::ImagingSequence>();
        sequence->AddImagingCondition(imgCond);

        return sequence;
    }

    auto CapturePreview::Impl::FocusReadyMM() const -> double {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto status = AppEntity::SystemStatus::GetInstance();
        const auto zReadyMM = sysConfig->GetAutofocusReadyPos();
        const auto afOffset = status->GetExperiment()->GetVessel()->GetAFOffset();
        return zReadyMM + afOffset/1000;
    }

    auto CapturePreview::Impl::UpdateTiles(int32_t maxCount, IPreviewAcquisition* previewAcquisition) -> void {
        int32_t xPos, yPos;
        QImage tileImage;

        for(int32_t idx=0; idx<maxCount; ++idx) {
            if(previewAcquisition->GetPreviewTile(xPos, yPos, tileImage)) {
                output->UpdateBlock(xPos, yPos, tileImage);
            } else {
                break;
            }
        }
    }

    CapturePreview::CapturePreview(IPreviewOutputPort* output, std::function<bool()> stopFunc) : IUseCase("CapturePreview"), d{new Impl} {
        d->output = output;
        d->stopFunc = stopFunc;
    }
    
    CapturePreview::~CapturePreview() {
    }

    auto CapturePreview::SetArea(AppEntity::Area area) -> void {
        auto status = AppEntity::SystemStatus::GetInstance();

        d->wellIndex = status->GetCurrentWell();
        d->positionInWell = global2well(status->GetCurrentGlobalPosition());
        d->area = area;
    }

    auto CapturePreview::Perform() -> bool {
        auto* previewAcquisition = IPreviewAcquisition::GetInstance();
        if(previewAcquisition == nullptr) {
            Error("No preview acquisition plugin exists");
            return false;
        }

        auto* instrument = IInstrument::GetInstance();
        if(instrument == nullptr) {
            Error("No instrument plugin exists");
            return false;
        }

        if(!instrument->IsInitialized()) {
            Error("Instrument is not initialized");
            return false;
        }

        const auto startPos = instrument->GetAxisPosition();
        Print(QString("Preview Start Position = [%1, %2, %3]mm").arg(startPos.toMM().x)
              .arg(startPos.toMM().y).arg(startPos.toMM().z));

        instrument->StopLive();

        auto status = AppEntity::SystemStatus::GetInstance();
        auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto model = AppEntity::System::GetModel();

        previewAcquisition->SetArea(well2global(d->positionInWell), d->area, d->positionInWell);
        Print(QString("X:%1mm Y:%2mm Area: %3um x %4um").arg(d->positionInWell.toMM().x).arg(d->positionInWell.toMM().y)
                                                        .arg(d->area.toUM().width).arg(d->area.toUM().width));
        if(d->output) {
            d->output->SetPreviewArea(d->positionInWell.toMM().x, 
                                      d->positionInWell.toMM().y, 
                                      d->area.toMM().width,
                                      d->area.toMM().height);
        }
        using PreviewParam = AppEntity::PreviewParam;

        const auto fov = AppEntity::System::GetMaximumFOV();
        const auto overlap = model->PreviewParameter(PreviewParam::OverlapMM).toDouble() * 1000;
        const auto singleMoveLimit = model->PreviewParameter(PreviewParam::SingleMoveMM).toDouble() * 1000;

        previewAcquisition->SetMaximumFOV(fov);
        previewAcquisition->SetOverlapInUM(overlap);
        previewAcquisition->SetSingleMoveLimitUm(singleMoveLimit);
        Print(QString("FOV: %1um x %2um Overlap: %3um").arg(fov.toUM().width).arg(fov.toUM().height).arg(overlap));
        
        auto imagePort = previewAcquisition->GetImagePort();
        instrument->InstallImagePort(imagePort);

        const auto bfExposure = status->GetLiveConfig(AppEntity::ImagingMode::BFGray)->GetCameraExposureUSec();

        const auto [tileRows, tileCols, motionPlan] = previewAcquisition->StartAcquisition();
        if(d->output) {
            d->output->UpdateImageSize(tileCols*model->CameraPixelsH(), tileRows*model->CameraPixelsV());
        }

        if(!instrument->StartPreviewAcquisition(motionPlan, bfExposure/1000)) {
            Error("It fails to start preview acquisition");
            instrument->UninstallImagePort(imagePort);
            return false;
        }

        //TODO Timeout 包府 内靛 眠啊
        QElapsedTimer elapsedTimer;
        elapsedTimer.start();

        double progress = 0.0;
        while(progress < 1.0) {
            QCoreApplication::processEvents(QEventLoop::AllEvents, 50);
            d->UpdateTiles(2, previewAcquisition);

            if(elapsedTimer.elapsed() > 200) {
                auto resp = instrument->CheckSequenceProgress();
                if(std::get<0>(resp) == false) {
                    Error(QString("It fails to get preview images : %1").arg(instrument->GetErrorMessage()));
                    instrument->UninstallImagePort(imagePort);
                    return false;
                }

                progress = std::get<1>(resp);
                auto position = std::get<2>(resp);
                if(d->output) {
                    d->output->Progress(progress*0.95, position);
                }

                elapsedTimer.restart();
            }

            if(d->stopFunc && d->stopFunc()) {
                instrument->StopAcquisition();
                previewAcquisition->StopAcquisition();
                break;
            }
        }

        instrument->UninstallImagePort(imagePort);

        instrument->ResumeLive();

        instrument->MoveAxis(AppEntity::Axis::Z, startPos.toMM().z);
        auto motionStatus = instrument->CheckAxisMotion();
        auto currentPos = instrument->GetAxisPosition();
        while(motionStatus.moving) {
            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);
            motionStatus = instrument->CheckAxisMotion();
        }

        instrument->MoveAxis(startPos);
        motionStatus = instrument->CheckAxisMotion();
        while(motionStatus.moving) {
            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);
            motionStatus = instrument->CheckAxisMotion();
            currentPos = instrument->GetAxisPosition();
            if(d->output) {
                d->UpdateTiles(5, previewAcquisition);
                d->output->Progress(0.97, currentPos);
            }
        }

        //TODO Timeout 包府 内靛 眠啊
        bool imageReady = false;
        while(!imageReady) {
            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 100);
            d->UpdateTiles(5, previewAcquisition);
            imageReady = previewAcquisition->IsFinished();
        }

        if(d->output) {
            if(d->output) d->output->Progress(1.0, currentPos);
        }

        return true;
    }
}