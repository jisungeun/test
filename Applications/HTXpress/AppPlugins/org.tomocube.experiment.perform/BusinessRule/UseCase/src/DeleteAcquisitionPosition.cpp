#include <System.h>
#include <SystemStatus.h>

#include "ExperimentUpdater.h"
#include "DeleteAcquisitionPosition.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct DeleteAcquisitionPosition::Impl {
        IAcquisitionPositionOutputPort* output{ nullptr };
        AppEntity::WellIndex wellIdx{ -1 };
        AppEntity::LocationIndex locationIdx{ -1 };
    };

    DeleteAcquisitionPosition::DeleteAcquisitionPosition(IAcquisitionPositionOutputPort* output) : IUseCase("DeleteAcquisitionPosition"), d{new Impl} {
        d->output = output;
    }

    DeleteAcquisitionPosition::~DeleteAcquisitionPosition() {
    }

    auto DeleteAcquisitionPosition::SetPosition(AppEntity::WellIndex wellIdx, AppEntity::LocationIndex locationIdx) -> void {
        d->wellIdx = wellIdx;
        d->locationIdx = locationIdx;
    }

    auto DeleteAcquisitionPosition::Perform() -> bool {
        auto status = AppEntity::SystemStatus::GetInstance();
        auto vesselIdx = status->GetCurrentVesselIndex();

        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        if(!experiment) {
            Error("No experiment is loaded");
            return false;
        }

        auto exUpdater = ExperimentUpdater(experiment);
        if(!exUpdater.DeleteLocation(vesselIdx, d->wellIdx, d->locationIdx)) {
            Error(QString("It fails to delete a location (#=%1)").arg(d->locationIdx));
            return false;
        }

        if(d->output) {
            d->output->DeletePosition(d->wellIdx, d->locationIdx);
        }

        return true;
    }
}
