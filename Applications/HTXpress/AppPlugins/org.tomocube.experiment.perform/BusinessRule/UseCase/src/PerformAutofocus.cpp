#include <QElapsedTimer>
#include <SystemStatus.h>

#include "IInstrument.h"
#include "Utility.h"
#include "PerformAutofocus.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    namespace {
        class CurrentPositionUpdater {
        public:
            CurrentPositionUpdater(IInstrumentOutputPort* output)
                : output{ output } {
            }

            ~CurrentPositionUpdater() {
                auto instrument = IInstrument::GetInstance();

                //Update position even though it fails to find the best focus
                auto sysStatus = AppEntity::SystemStatus::GetInstance();
                const auto curPos = instrument->GetAxisPosition();
                sysStatus->SetCurrentGlobalPosition(curPos);

                output->UpdateGlobalPosition(curPos);
                if(afPerformed) output->UpdateBestFocus(curPos.toMM().z);
            }

            auto SetAutofocusPeformed(bool performed) {
                afPerformed = performed;
            }

            IInstrumentOutputPort* output{ nullptr };
            bool afPerformed{ false };
        };
    }

    struct PerformAutofocus::Impl {
        PerformAutofocus* p{nullptr};
        IInstrumentOutputPort* output{ nullptr };

        Impl(PerformAutofocus* p) : p(p) {}
        auto ReportError(const QString& message)->void;
        auto WaitZMotion(int32_t waitInSeconds)->bool;
    };

    auto PerformAutofocus::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
    }

    auto PerformAutofocus::Impl::WaitZMotion(int32_t waitInSeconds) -> bool {
        auto instrument = IInstrument::GetInstance();

        QElapsedTimer timer;
        timer.start();

        auto motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < (waitInSeconds*1000)) && motionStatus.moving) {
            if(motionStatus.error) {
                ReportError(tr("It fails to move Z stage : %1").arg(motionStatus.message));
                return false;
            }

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

            const auto curPos = instrument->GetAxisPosition();
            AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);

            if(output) {
                output->UpdateGlobalPosition(curPos);
            }

            motionStatus = instrument->CheckAxisMotion();
        }

        return !motionStatus.moving;
    }

    PerformAutofocus::PerformAutofocus(IInstrumentOutputPort* output) : IUseCase("PerformAutofocus"), d{new Impl(this)} {
        d->output = output;
    }

    PerformAutofocus::~PerformAutofocus() {
    }

    auto PerformAutofocus::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();
        if(!instrument) return false;

        CurrentPositionUpdater positionUpdater(d->output);

        auto zStartPosInMM = instrument->GetAxisPositionMM(AppEntity::Axis::Z);

        bool afPerformed{ false };
        bool success{ true };
        const auto afResult = instrument->PerformAutoFocus();
        if(!std::get<0>(afResult)) {
            d->ReportError(tr("Autofocus failure because of an error = %1").arg(instrument->GetErrorMessage()));
            success = false;
        } else {
            afPerformed = true;
            if(!std::get<1>(afResult)) {
                d->ReportError(tr("Failed to find best focus position"));
                success = false;
            }
        }

        if(!success) {
            d->output->ReportAFFailed();

            if(!instrument->MoveAxis(AppEntity::Axis::Z, zStartPosInMM)) {
                d->ReportError(tr("It fails to move Z stage [%1]").arg(instrument->GetErrorMessage()));
                return false;
            }

            if(!d->WaitZMotion(30)) {
                return false;
            }
        }

        positionUpdater.SetAutofocusPeformed(afPerformed);

        return success;
    }
}