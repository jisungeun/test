#include <QCoreApplication>
#include <QElapsedTimer>

#include <SystemStatus.h>

#include "Utility.h"
#include "IInstrument.h"
#include "IImagingSystem.h"
#include "IScanTimeCalculator.h"
#include "IExperimentRunner.h"
#include "AcquireImage.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct AcquireImage::Impl {
        IRunExperimentOutputPort* output{ nullptr };
        AppEntity::ImagingSequence::Pointer sequence{ new AppEntity::ImagingSequence() };
        struct {
            double x;
            double y;
        } roiInUm;

        struct {
            double x;
            double y;
        } centerInMm;

        IImagingSystem* imagingSystem{ nullptr };
        IInstrument* instrument{ nullptr };
        QString outPath;

        auto Ready(AcquireImage* p)->bool;
    };

    auto AcquireImage::Impl::Ready(AcquireImage* p) -> bool {
        imagingSystem = IImagingSystem::GetInstance();
        if(!imagingSystem) {
            p->Error("No imaging system exists");
            return false;
        }

        instrument = IInstrument::GetInstance();
        if(!instrument) {
            p->Error("No instrument exists");
            return false;
        }

        if(!instrument->IsInitialized()) {
            p->Error("Instrument is not initialized");
            return false;
        }

        if(sequence->GetModalityCount() == 0) {
            p->Error("No imaging condition is set");
            return false;
        }

        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        outPath = sysStatus->GetExperimentOutputPath();
        if(outPath.isEmpty()) {
            p->Error("No data path is not specified");
            return false;
        }

        return true;
    }

    AcquireImage::AcquireImage(IRunExperimentOutputPort* output) : IUseCase("AcquireImage"), d{new Impl} {
        d->output = output;
    }

    AcquireImage::~AcquireImage() {
    }

    auto AcquireImage::AddModalityHT(bool is3D) -> void {
        auto cond = CreateCondHT(is3D);
        d->sequence->AddImagingCondition(cond);
    }

    auto AcquireImage::AddModalityFL(bool is3D, const QList<int32_t>& channels) -> void {
        auto cond = CreateCondFL(is3D, channels);
        d->sequence->AddImagingCondition(cond);
    }

    auto AcquireImage::AddModalityBF(bool isGray) -> void {
        auto cond = CreateCondBF(isGray);
        d->sequence->AddImagingCondition(cond);
    }

    auto AcquireImage::SetCenter(double xInMm, double yInMm) -> void {
        d->centerInMm.x = xInMm;
        d->centerInMm.y = yInMm;
    }

    auto AcquireImage::SetROI(double roiXInUm, double roiYInUm) -> void {
        d->roiInUm.x = roiXInUm;
        d->roiInUm.y = roiYInUm;
    }

    auto AcquireImage::Perform() -> bool {
        if(!d->Ready(this)) {
            Error("System is not ready to acquire image");
            return false;
        }

        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto globalPos = sysStatus->GetCurrentGlobalPosition();
        const auto wellPos = AppEntity::Position::fromMM(d->centerInMm.x, d->centerInMm.y, globalPos.toMM().z);

        Print(QString("X:%1 Y:%2mm Z:%3mm ROI[W:%4 H:%5]um").arg(d->centerInMm.x).arg(d->centerInMm.y).arg(globalPos.toMM().z)
                                                            .arg(d->roiInUm.x).arg(d->roiInUm.y));

        AppEntity::Location location;
        location.SetCenter(wellPos);
        location.SetArea(AppEntity::Area::fromUM(d->roiInUm.x, d->roiInUm.y), false);

        auto scenario = std::make_shared<AppEntity::ImagingScenario>();
        scenario->AddSequence(0, "", d->sequence);

        auto experiment = std::make_shared<AppEntity::Experiment>(*sysStatus->GetExperiment());
        experiment->ClearLocation(0);
        experiment->AddLocation(0, sysStatus->GetCurrentWell(), location);
        experiment->SetScenario(0, scenario);

        auto* runner = IExperimentRunner::GetInstance();
        if(!runner) {
            Error("No experiment runner exists");
            return false;
        }

        runner->SetOutputPort(d->output);
        runner->SetExperiment(sysStatus->GetProjectTitle(), experiment);
        runner->SetVesselIndex(0);
        runner->SetOverlapInUM(AppEntity::System::GetTileScanOverlap());
        runner->SetAcquisitionType(AcquisitionType::Single);
        runner->SetMultiDishHolder(experiment->GetVessel()->IsMultiDish());

        if(!runner->Run()) {
            Error("Failed to run the experiment runner");
            return false;
        }

        return true;
    }
}