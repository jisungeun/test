#include "IPreviewAcquisition.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static IPreviewAcquisition* theInstance{ nullptr };

    IPreviewAcquisition::IPreviewAcquisition() {
        theInstance = this;
    }

    IPreviewAcquisition::~IPreviewAcquisition() {
    }

    auto IPreviewAcquisition::GetInstance() -> IPreviewAcquisition* {
        return theInstance;
    }
}
