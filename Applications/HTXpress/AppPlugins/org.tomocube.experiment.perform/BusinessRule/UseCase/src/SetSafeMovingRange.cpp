﻿#include "SetSafeMovingRange.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetSafeMovingRange::Impl {
        IVesselOutputPort* output{nullptr};

        AppEntity::Axis::_enumerated axis;
        double minimum{};
        double maximum{};

    };

    SetSafeMovingRange::SetSafeMovingRange(IVesselOutputPort* output) : IUseCase("SetSafeMovingRange"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    SetSafeMovingRange::~SetSafeMovingRange() {
    }

    auto SetSafeMovingRange::SetSafeRange(AppEntity::Axis axis, double minMM, double maxMM) -> void {
        d->axis = axis;
        d->minimum = minMM;
        d->maximum = maxMM;
    }

    auto SetSafeMovingRange::Perform() -> bool {
        if(d->output) {
            d->output->UpdateSafeMovingRange(d->axis, d->minimum, d->maximum);
            return true;
        }

        return false;
    }
}
