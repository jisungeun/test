#include "IHTIlluminationAutoCalibration.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static IHTIlluminationAutoCalibration* theInstance{ nullptr };
    IHTIlluminationAutoCalibration::IHTIlluminationAutoCalibration() {
        theInstance = this;
    }

    IHTIlluminationAutoCalibration::~IHTIlluminationAutoCalibration() {
    }

    auto IHTIlluminationAutoCalibration::GetInstance() -> IHTIlluminationAutoCalibration* {
        return theInstance;
    }
}