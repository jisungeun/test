#include <SystemStatus.h>
#include <ChannelConfig.h>
#include "ExperimentUpdater.h"
#include "SetHTEnable.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetHTEnable::Impl {
        IImagingConditionOutputPort* output{ nullptr };
        bool updatExperiment{ true };
        bool is3D{ true };
        bool enable{ false };
    };

    SetHTEnable::SetHTEnable(bool updateExperiment, IImagingConditionOutputPort* output) : IUseCase("SetHTEnable"), d{new Impl} {
        d->output = output;
        d->updatExperiment = updateExperiment;
    }

    SetHTEnable::~SetHTEnable() {
    }

    auto SetHTEnable::SetEnable(const bool is3D, const bool enable) -> void {
        d->is3D = is3D;
        d->enable = enable;
    }

    auto SetHTEnable::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto mode = AppEntity::ImagingMode::_from_integral(d->is3D?AppEntity::ImagingMode::HT3D:AppEntity::ImagingMode::HT2D);

        auto htConfig = sysStatus->GetChannelConfig(mode);
        auto htScanConfig = sysStatus->GetScanConfig(mode);
        if(!htConfig || !htScanConfig) return true;

        htConfig->SetEnable(d->enable);

        if(d->updatExperiment) {
            auto expUpdater = ExperimentUpdater(sysStatus->GetExperiment());
            expUpdater.SetHTChannelConfig(d->is3D, *htConfig, *htScanConfig);
        }

        if(d->output) d->output->UpdateHTEnabled(d->enable);

        //Todo FL 이미징 채널 설정 변경을 적용하여 Timetable 갱신하기

        return true;
    }
};