#include <SystemStatus.h>
#include <IInstrument.h>

#include "ChangeLiveViewParameter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ChangeLiveViewParameter::Impl {
        IInstrumentOutputPort* output{ InstrumentNullOutputPort::GetInstance() };
        AppEntity::Modality modality{ AppEntity::Modality::HT };
        int32_t channel{ 0 };

        auto ReportError(ChangeLiveViewParameter* p, const QString& message)->void;
    };

    auto ChangeLiveViewParameter::Impl::ReportError(ChangeLiveViewParameter* p, const QString& message) -> void {
        p->Error(message);
        output->LiveImagingFailed(message);
    }

    ChangeLiveViewParameter::ChangeLiveViewParameter(IInstrumentOutputPort* output) : IUseCase("ChangeLiveViewParameter"), d{new Impl} {
        d->output = output;
    }

    ChangeLiveViewParameter::~ChangeLiveViewParameter() {
    }

    auto ChangeLiveViewParameter::SetImageType(AppEntity::Modality modality, int32_t channel) -> void {
        d->modality = modality;
        d->channel = channel;
    }

    auto ChangeLiveViewParameter::Perform() -> bool {
        if(d->modality == +AppEntity::Modality::HT) {
            d->ReportError(this, "It does not support live image of hologram");
            return false;
        }

        auto* instrument = IInstrument::GetInstance();
        if(!instrument) {
            d->ReportError(this, "No instrument exists");
            return false;
        }

        if(!instrument->StartLive(d->modality, d->channel, true)) {
            const auto msg = QString("It fails to change live imaging modality - %1").arg(instrument->GetErrorMessage());
            d->ReportError(this, msg);
            return false;
        }

        return true;
    }
}
