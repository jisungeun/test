#include "IExperimentRunner.h"
#include "StopExperiment.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct StopExperiment::Impl {
        IRunExperimentOutputPort* output{ nullptr };
    };

    StopExperiment::StopExperiment(IRunExperimentOutputPort* output) : IUseCase("StopExperiment"), d{new Impl} {
        d->output = output;
    }

    StopExperiment::~StopExperiment() {
    }

    auto StopExperiment::Perform() -> bool {
        auto* runner = IExperimentRunner::GetInstance();
        if(!runner) {
            Error("No experiment runner exists");
            return false;
        }

        if(!runner->Stop()) {
            Error("It fails to stop experiment");
            return false;
        }

        return true;
    }
}
