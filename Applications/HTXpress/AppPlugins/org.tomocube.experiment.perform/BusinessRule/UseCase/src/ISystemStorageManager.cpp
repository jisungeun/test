﻿#include "ISystemStorageManager.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static ISystemStorageManager* theInstance{};

    ISystemStorageManager::ISystemStorageManager() {
        theInstance = this;
    }

    ISystemStorageManager::~ISystemStorageManager() {
    }

    auto ISystemStorageManager::GetInstance() -> ISystemStorageManager* {
        return theInstance;
    }
}
