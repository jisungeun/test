﻿#include <System.h>

#include "GetDataFolderPath.h"
#include "ISystemStorageManager.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    GetDataFolderPath::GetDataFolderPath() : IUseCase("GetDataFolderPath") {
    }

    GetDataFolderPath::~GetDataFolderPath() {
    }

    auto GetDataFolderPath::Perform() -> bool {
        const auto& dataFolderPath = AppEntity::System::GetSystemConfig()->GetDataDir();

        if(dataFolderPath.isEmpty()) {
            return false;
        }

        ISystemStorageManager::GetInstance()->SetPath(dataFolderPath);
        return true;
    }
}
