#include <SystemStatus.h>

#include "GetImagingCondition.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct GetImagingCondition::Impl {
        QMap<AppEntity::ImagingMode,AppEntity::ChannelConfig::Pointer> channelConfigs;
    };

    GetImagingCondition::GetImagingCondition() : IUseCase("GetImagingCondition"), d{new Impl} {
    }

    GetImagingCondition::~GetImagingCondition() {
    }

    auto GetImagingCondition::GetConfig(AppEntity::ImagingMode mode) ->AppEntity::ChannelConfig::Pointer {
        return d->channelConfigs.value(mode);
    }

    auto GetImagingCondition::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        d->channelConfigs = sysStatus->GetChannelConfigAll();

        return true;
    }
}
