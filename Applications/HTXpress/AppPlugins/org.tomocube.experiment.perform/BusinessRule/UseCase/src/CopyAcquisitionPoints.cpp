#include <System.h>
#include <SystemStatus.h>

#include "Utility.h"
#include "ExperimentUpdater.h"
#include "CopyAcquisitionPoints.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct CopyAcquisitionPositions::Impl {
        IAcquisitionPositionOutputPort* output{ nullptr };

        struct {
            AppEntity::WellIndex wellIndex;
            QList<AppEntity::LocationIndex> locations;
        } source;

        struct {
            QList<AppEntity::WellIndex> wells;
        } target;
    };

    CopyAcquisitionPositions::CopyAcquisitionPositions(IAcquisitionPositionOutputPort* output)
        : IUseCase("CopyAcquisitionPositions")
        , d{ std::make_unique<Impl>() } {
        d->output = output;
    }

    CopyAcquisitionPositions::~CopyAcquisitionPositions() {
    }

    auto CopyAcquisitionPositions::SetSources(const AppEntity::WellIndex wellIdx, const QList<AppEntity::LocationIndex>& locations) -> void {
        d->source.wellIndex = wellIdx;
        d->source.locations = locations;
    }

    auto CopyAcquisitionPositions::SetTargetWells(const QList<AppEntity::WellIndex>& wells) -> void {
        d->target.wells = wells;
    }

    auto CopyAcquisitionPositions::Perform() -> bool {
        auto status = AppEntity::SystemStatus::GetInstance();
        auto vesselIdx = status->GetCurrentVesselIndex();

        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        if(!experiment) {
            Error("No experiment is loaded");
            return false;
        }

        QList<std::shared_ptr<AppEntity::Location>> locations;
        for(auto locIdx : d->source.locations) {
            auto location = std::make_shared<AppEntity::Location>(experiment->GetLocation(vesselIdx, d->source.wellIndex, locIdx));
            locations.push_back(location);
        }

        auto exUpdater = ExperimentUpdater(experiment);
        for(auto location : locations) {
            for(auto wellIdx : d->target.wells) {
                const auto newIdx = exUpdater.AddLocation(vesselIdx, wellIdx, *location);
                if(d->output) {
                    d->output->AddPosition(newIdx, wellIdx, location);
                }
            }
        }

        return true;
    }
}