#include <System.h>
#include <SystemStatus.h>

#include "Utility.h"
#include "ExperimentUpdater.h"
#include "IImagingSystem.h"
#include "IInstrument.h"
#include "IExperimentRunner.h"
#include "QualityCheckExperiment.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    enum class PointsMode {
        AllPoints,
        SinglePoint
    };

    enum class MatrixMode {
        NoMatrix,
        Matrix
    };

    struct QualityCheckExperiment::Impl {
        IRunExperimentOutputPort* output{ nullptr };
        QualityCheckExperiment* p{ nullptr };

        PointsMode pointMode{ PointsMode::AllPoints };
        struct {
            WellIdx wellIdx{ 0 };
            AppEntity::Position position;
        } singlePoint;

        int32_t repeatCount{ 1 };

        MatrixMode matrixMode{ MatrixMode::NoMatrix };
        struct {
            int32_t xCount{ 1 };
            int32_t yCount{ 1 };
            int32_t xGapUm { 0 };
            int32_t yGapUm{ 0 };
        } matrixCond;

        AppEntity::Experiment::Pointer experiment{ nullptr };

        IImagingSystem* imagingSystem{ nullptr };
        IInstrument* instrument{ nullptr };
        QString outPath;

        Impl(QualityCheckExperiment* p) : p{ p } {}
        auto Ready()->bool;
        auto BuildAllPointsExperiment()->AppEntity::Experiment::Pointer;
        auto BuildSinglePointExperiment()->AppEntity::Experiment::Pointer;
        auto BuildMatrixExperiment()->AppEntity::Experiment::Pointer;
        auto ReportError(const QString& message)->void;
    };

    auto QualityCheckExperiment::Impl::Ready() -> bool {
        imagingSystem = IImagingSystem::GetInstance();
        if(!imagingSystem) {
            ReportError(tr("No imaging system exists"));
            return false;
        }

        instrument = IInstrument::GetInstance();
        if(!instrument) {
            ReportError(tr("No instrument exists"));
            return false;
        }

        if(!instrument->IsInitialized()) {
            ReportError(tr("Instrument is not initialized"));
            return false;
        }

        const auto sysStatus = AppEntity::SystemStatus::GetInstance();

        experiment = sysStatus->GetExperiment();
        if(!experiment) {
            ReportError(tr("No experiment is loaded"));
            return false;
        }

        outPath = sysStatus->GetExperimentOutputPath();
        if(outPath.isEmpty()) {
            ReportError(tr("No data path is not specified"));
            return false;
        }

        auto newExperiment = [&]()->AppEntity::Experiment::Pointer {
            if(matrixMode == MatrixMode::NoMatrix) {
                if(pointMode == PointsMode::AllPoints) {
                    return BuildAllPointsExperiment();
                }
                return BuildSinglePointExperiment();
            }

            return BuildMatrixExperiment();
        }();

        if(newExperiment == nullptr) {
            ReportError(tr("Invalid experiment is loaded. It may have no acquisition point."));
            return false;
        }

        experiment = newExperiment;

        return true;
    }

    auto QualityCheckExperiment::Impl::BuildAllPointsExperiment() -> AppEntity::Experiment::Pointer {
        auto condHT = CreateCondHT(true);

        auto sequence = std::make_shared<AppEntity::ImagingSequence>();
        sequence->AddImagingCondition(condHT);

        auto scenario = std::make_shared<AppEntity::ImagingScenario>();
        scenario->AddSequence(0, "0", sequence);

        auto newExperiment = std::make_shared<AppEntity::Experiment>(*experiment);

        auto expUpdater = ExperimentUpdater(newExperiment, false);
        expUpdater.SetImagingScenario(0, scenario);

        const auto allLocations = newExperiment->GetAllLocations(0);
        if(allLocations.isEmpty()) return nullptr;

        return newExperiment;
    }

    auto QualityCheckExperiment::Impl::BuildSinglePointExperiment() -> AppEntity::Experiment::Pointer {
        auto condHT = CreateCondHT(true);

        auto sequence = std::make_shared<AppEntity::ImagingSequence>();
        sequence->AddImagingCondition(condHT);

        auto scenario = std::make_shared<AppEntity::ImagingScenario>();
        scenario->AddSequence(0, "0", sequence);

        auto newExperiment = std::make_shared<AppEntity::Experiment>(*experiment);

        auto expUpdater = ExperimentUpdater(newExperiment, false);
        expUpdater.SetImagingScenario(0, scenario);

        //Create Point
        const auto allLocations = newExperiment->GetAllLocations(0);
        if(allLocations.isEmpty()) return nullptr;

        const auto area = allLocations.begin()->begin().value().GetArea();

        AppEntity::Location location;
        location.SetCenter(singlePoint.position);
        location.SetArea(area, false);

        expUpdater.ClearAllLocations();
        for(auto idx=0; idx<repeatCount; ++idx) {
            expUpdater.AddLocation(0, singlePoint.wellIdx, location);
        }

        return newExperiment;
    }

    auto QualityCheckExperiment::Impl::BuildMatrixExperiment() -> AppEntity::Experiment::Pointer {
        auto condHT = CreateCondHT(true);

        auto sequence = std::make_shared<AppEntity::ImagingSequence>();
        sequence->AddImagingCondition(condHT);

        auto scenario = std::make_shared<AppEntity::ImagingScenario>();
        scenario->AddSequence(0, "0", sequence);

        auto newExperiment = std::make_shared<AppEntity::Experiment>(*experiment);

        auto expUpdater = ExperimentUpdater(newExperiment, false);
        expUpdater.SetImagingScenario(0, scenario);

        //Create Point
        const auto allLocations = newExperiment->GetAllLocations(0);
        if(allLocations.isEmpty()) return nullptr;

        const auto area = allLocations.begin()->begin().value().GetArea();

        const auto xOffset = static_cast<int32_t>(std::floor(matrixCond.xCount/2));
        const auto yOffset = static_cast<int32_t>(std::floor(matrixCond.yCount/2));
        const auto centerPos = singlePoint.position.toUM();

        expUpdater.ClearAllLocations();
        for(auto yIdx=0; yIdx<matrixCond.yCount; ++yIdx) {
            auto yPosUm = centerPos.y + (yIdx - yOffset) * matrixCond.yGapUm;
            for(auto xIdx=0; xIdx<matrixCond.xCount; ++xIdx) {
                auto xPosUm = centerPos.x + (xOffset - xIdx) * matrixCond.xGapUm;

                AppEntity::Location location;
                location.SetCenter(AppEntity::Position::fromUM(xPosUm, yPosUm, centerPos.z));
                location.SetArea(area, false);

                expUpdater.AddLocation(0, singlePoint.wellIdx, location);
            }
        }

        return newExperiment;
    }

    auto QualityCheckExperiment::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
        if(output) {
            ExperimentStatus status;
            status.SetError(message);
            output->UpdateProgress(status);
        }
    }

    QualityCheckExperiment::QualityCheckExperiment(IRunExperimentOutputPort* output)
        : IUseCase("QualityCheckExperiment")
        , d{ std::make_unique<Impl>(this) } {
        d->output = output;
    }

    QualityCheckExperiment::~QualityCheckExperiment() {
    }

    auto QualityCheckExperiment::SetAllPotins() -> void {
        d->pointMode = PointsMode::AllPoints;
    }

    auto QualityCheckExperiment::SetCurrentPoint() -> void {
        d->pointMode = PointsMode::SinglePoint;

        auto status = AppEntity::SystemStatus::GetInstance();
        d->singlePoint.wellIdx = status->GetCurrentWell();
        d->singlePoint.position = global2well(status->GetCurrentGlobalPosition(), d->singlePoint.wellIdx);
    }

    auto QualityCheckExperiment::SetMatrixPoints(int32_t xCount, int32_t yCount, int32_t xGapUm, int32_t yGapUm) -> void {
        d->pointMode = PointsMode::SinglePoint;
        d->matrixMode = MatrixMode::Matrix;
        d->matrixCond.xCount = xCount;
        d->matrixCond.yCount = yCount;
        d->matrixCond.xGapUm = xGapUm;
        d->matrixCond.yGapUm = yGapUm;

        auto status = AppEntity::SystemStatus::GetInstance();
        d->singlePoint.wellIdx = status->GetCurrentWell();
        d->singlePoint.position = global2well(status->GetCurrentGlobalPosition(), d->singlePoint.wellIdx);
    }

    auto QualityCheckExperiment::SetRepeatCount(int32_t repeatCount) -> void {
        d->repeatCount = repeatCount;
    }

    auto QualityCheckExperiment::Perform() -> bool {
        if(!d->Ready()) {
            d->ReportError(tr("System is not ready to run experiment"));
            return false;
        }

        if(d->experiment->GetAllLocations(0).isEmpty()) {
            d->ReportError(tr("No acquisition points exist to be evaluated"));
            return false;
        }

        const auto sysStatus = AppEntity::SystemStatus::GetInstance();

        auto* runner = IExperimentRunner::GetInstance();
        if(!runner) {
            d->ReportError(tr("No experiment runner exists"));
            return false;
        }

        runner->SetOutputPort(d->output);
        runner->SetExperiment(sysStatus->GetProjectTitle(), d->experiment);
        runner->SetVesselIndex(0);
        runner->SetOverlapInUM(AppEntity::System::GetTileScanOverlap());
        runner->SetAcquisitionType(AcquisitionType::Timelapse);
        runner->SetMultiDishHolder(d->experiment->GetVessel()->IsMultiDish());

        if(!runner->Run()) {
            d->ReportError(tr("Failed to run the experiment runner"));
            return false;
        }

        return true;
    }
}