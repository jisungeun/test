﻿#include "InstallStorageMonitor.h"
#include "ISystemStorageManager.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct InstallStorageMonitor::Impl {
        ISystemStorageOutputPort::Pointer output {};
    };

    InstallStorageMonitor::InstallStorageMonitor(ISystemStorageOutputPort::Pointer output) : IUseCase("InstallStorageMonitor"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    InstallStorageMonitor::~InstallStorageMonitor() {
    }

    auto InstallStorageMonitor::Perform() -> bool {
        if(!d->output) {
            Error("Output port is null.");
            return false;
        }

        ISystemStorageManager::GetInstance()->RegistOutputPort(d->output);
        return true;
    }
}
