#include "IChannelConfigWriter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static IChannelConfigWriter* theInstance{ nullptr };

    IChannelConfigWriter::IChannelConfigWriter() {
        theInstance = this;
    }

    IChannelConfigWriter::~IChannelConfigWriter() {
    }

    auto IChannelConfigWriter::GetInstance() -> IChannelConfigWriter* {
        return theInstance;
    }
}