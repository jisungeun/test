#include <System.h>
#include <SystemStatus.h>

#include "Utility.h"
#include "ExperimentUpdater.h"
#include "AddAcquisitionPosition.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct AddAcquisitionPosition::Impl {
        IAcquisitionPositionOutputPort* output{ nullptr };

        AppEntity::WellIndex wellIndex;
        AppEntity::Position globalPosition;
        AppEntity::Area area;
        bool isTile{ false };
    };

    AddAcquisitionPosition::AddAcquisitionPosition(IAcquisitionPositionOutputPort* output) : IUseCase("AddAcquisitionPosition"), d{new Impl} {
        d->output = output;
    }

    AddAcquisitionPosition::~AddAcquisitionPosition() {
    }

    auto AddAcquisitionPosition::SetCurrentPosition() -> void {
        auto status = AppEntity::SystemStatus::GetInstance();
        d->wellIndex = status->GetCurrentWell();
        d->globalPosition = status->GetCurrentGlobalPosition();
        d->area = AppEntity::System::GetMaximumFOV();
    }

    auto AddAcquisitionPosition::SetCurrentArea(const AppEntity::Area& area, bool isTile) -> void {
        auto status = AppEntity::SystemStatus::GetInstance();
        d->wellIndex = status->GetCurrentWell();
        d->globalPosition = status->GetCurrentGlobalPosition();
        d->area = area;
        d->isTile = isTile;
    }

    auto AddAcquisitionPosition::SetCurrentTileArea(double xInMm, double yInMm, const AppEntity::Area& area, bool isTile) -> void {
        auto status = AppEntity::SystemStatus::GetInstance();
        d->wellIndex = status->GetCurrentWell();
        const auto wellPos2D = AppEntity::Position::fromMM(xInMm, yInMm, 0);
        const auto globalPos2D = well2global(d->wellIndex, wellPos2D);
        d->globalPosition = {globalPos2D.X(),globalPos2D.Y(),status->GetCurrentGlobalPosition().Z()};
        d->area = area;
        d->isTile = isTile;
    }

    auto AddAcquisitionPosition::Perform() -> bool {
        auto status = AppEntity::SystemStatus::GetInstance();
        auto vesselIdx = status->GetCurrentVesselIndex();

        auto posInWell = global2well(d->globalPosition, d->wellIndex);

        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        if(!experiment) {
            Error("No experiment is loaded");
            return false;
        }

        auto location = std::make_shared<AppEntity::Location>();
        location->SetCenter(posInWell);
        location->SetArea(d->area, d->isTile);

        auto exUpdater = ExperimentUpdater(experiment);
        const auto locationIdx = exUpdater.AddLocation(vesselIdx, d->wellIndex, *location);

        if(d->output) {
            d->output->AddPosition(locationIdx, d->wellIndex, location);
        }

        return true;
    }
}
