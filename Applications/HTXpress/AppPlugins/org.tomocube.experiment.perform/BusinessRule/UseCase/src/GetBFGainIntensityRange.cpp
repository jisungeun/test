#include <QCoreApplication>
#include <QThread>

#include <SystemStatus.h>

#include "IInstrument.h"
#include "IPreviewCalibrator.h"
#include "GetBFGainIntensityRange.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct GetBFGainIntensityRange::Impl {
        GetBFGainIntensityRange* p{ nullptr };
        IInstrumentOutputPort* output{ nullptr };

        ValueList values;
        double coeffA{ 0 };
        double coeffB{ 0 };

        Impl(GetBFGainIntensityRange* p, IInstrumentOutputPort* output) : p{ p }, output{ output } {
        }

        auto ReportError(const QString& message);
        auto UpdateProgress(double progress, const QString& message = QString())->void;
    };

    auto GetBFGainIntensityRange::Impl::ReportError(const QString& message) {
        p->Error(message);
        if (output) output->UpdateFailed(message);
    }

    auto GetBFGainIntensityRange::Impl::UpdateProgress(double progress, const QString& message) -> void {
        if (output) {
            output->UpdateProgress(progress, message);
            QCoreApplication::processEvents(QEventLoop::AllEvents, 50);
        }
    }

    GetBFGainIntensityRange::GetBFGainIntensityRange(IInstrumentOutputPort* output) 
        : IUseCase("GetBFGainIntensityRange")
        , d{ new Impl(this, output) } {
    }

    GetBFGainIntensityRange::~GetBFGainIntensityRange() {
    }

    auto GetBFGainIntensityRange::GetValues() const -> ValueList {
        return d->values;
    }

    auto GetBFGainIntensityRange::GetCoefficients() const -> std::tuple<double, double> {
        return std::make_tuple(d->coeffA, d->coeffB);
    }

    auto GetBFGainIntensityRange::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();
        auto calibrator = IPreviewCalibrator::GetInstance();
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        d->values.clear();
        d->coeffA = 0;
        d->coeffB = 0;

        d->UpdateProgress(0, "Calculating BF Gain range");
        
        instrument->StopLive();

        auto imagePort = calibrator->GetImagePort();
        instrument->InstallImagePort(imagePort);

        auto gain = 0;   //0dB
        while (1) {
            imagePort->Clear();
            instrument->CapturePreviews(3, gain);

            const auto intensity = calibrator->GetIntensity();
            if (intensity == -1) {
                d->ReportError(QString("It fails to get image to get its intensity for gain  %1 dB").arg(gain));
                break;
            }

            d->values.push_back({ gain, intensity });

            d->UpdateProgress(intensity / 255.0);
            if (intensity >= 200.0) break;

            gain += 2;
        }

        instrument->UninstallImagePort(imagePort);
        imagePort->Clear();

        //resotre the original channel configuration
        instrument->StartLive(AppEntity::Modality::BF);

        std::tie(d->coeffA, d->coeffB) = calibrator->CalcGainCoefficients(d->values);

        d->UpdateProgress(1.0);


        return true;
    }
}