﻿#include <QMap>

#include "IAcquisitionDataSpaceCalculator.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    using Modality = AcqDataSpaceCal::Modality;

    static IAcquisitionDataSpaceCalculator* theInstnace{};

    struct IAcquisitionDataSpaceCalculator::Impl {
    };

    IAcquisitionDataSpaceCalculator::IAcquisitionDataSpaceCalculator() : d{std::make_unique<Impl>()} {
        theInstnace = this;
    }

    IAcquisitionDataSpaceCalculator::~IAcquisitionDataSpaceCalculator() = default;

    auto IAcquisitionDataSpaceCalculator::GetInstance() -> IAcquisitionDataSpaceCalculator* {
        return theInstnace;
    }
}
