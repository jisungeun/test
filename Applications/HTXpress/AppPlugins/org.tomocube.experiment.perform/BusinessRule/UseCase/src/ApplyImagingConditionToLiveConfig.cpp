#include <SystemStatus.h>
#include <ChannelConfig.h>

#include "IChannelConfigWriter.h"
#include "ApplyImagingConditionToLiveConfig.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ApplyImagingConditionToLiveConfig::Impl {
        ILiveviewConfigOutputPort* output{ nullptr };
        AppEntity::ImagingMode mode{ AppEntity::ImagingMode::FLCH0 };
       AppEntity::ChannelConfig config;
    };

    ApplyImagingConditionToLiveConfig::ApplyImagingConditionToLiveConfig(ILiveviewConfigOutputPort* output) : IUseCase("SetLiveviewFLChannel"), d{ new Impl } {
        d->output = output;
    }

    ApplyImagingConditionToLiveConfig::~ApplyImagingConditionToLiveConfig() {
    }

    auto ApplyImagingConditionToLiveConfig::ApplyConfig(const AppEntity::ImagingMode mode) -> void {
        d->mode = mode;
    }

    auto ApplyImagingConditionToLiveConfig::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        // channel config 값을 가져와 live config에 적용
        auto flConfig = sysStatus->GetChannelConfig(d->mode);
        if(!flConfig) {
            Error("It fails to fill FL channel configuration.");
            return false;
        }

        auto intensity = flConfig->GetLightIntensity();
        auto exposure = flConfig->GetCameraExposureUSec();

        auto liveConfig = sysStatus->GetLiveConfig(d->mode);
        liveConfig->SetLightIntensity(intensity);
        liveConfig->SetCameraExposureUSec(exposure);

        sysStatus->SetLiveConfig(d->mode, *liveConfig);

        if (d->output) {
            d->output->UpdateLiveConfig(intensity, exposure, 0);
        }

        return true;
    }
};