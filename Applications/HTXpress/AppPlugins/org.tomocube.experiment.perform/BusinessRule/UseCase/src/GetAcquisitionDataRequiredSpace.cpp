﻿#include <AppEntityDefines.h>
#include <SystemStatus.h>
#include <System.h>

#include "GetAcquisitionDataRequiredSpace.h"
#include "IAcquisitionDataSpaceCalculator.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct GetAcquisitionDataRequiredSpace::Impl {
        IImagingConditionOutputPort* output{};
        int32_t points{};

        auto ConvertModality(const AppEntity::ImagingType& entityType) const -> AcqDataSpaceCal::Modality;
    };

    GetAcquisitionDataRequiredSpace::GetAcquisitionDataRequiredSpace(IImagingConditionOutputPort* output) :
        IUseCase("GetAcquisitionDataRequiredSpace"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    GetAcquisitionDataRequiredSpace::~GetAcquisitionDataRequiredSpace() = default;

    auto GetAcquisitionDataRequiredSpace::SetAcquisitionPoints(const int32_t& points) -> void {
        if (points == 0) {
            d->points = 1;
        }
        else {
            d->points = points;
        }
    }

    auto GetAcquisitionDataRequiredSpace::Perform() -> bool {
        bool result{true};

        using Modality = AcqDataSpaceCal::Modality;

        const auto calculator = IAcquisitionDataSpaceCalculator::GetInstance();
        int64_t bytesNeed{};

        if (!calculator) {
            Error("AcquisitionDataSpaceCalculator plugin doesn't created.");
            result = false;
        }
        else {
            calculator->Clear();

            const auto sysStatus = AppEntity::SystemStatus::GetInstance();
            const auto vesselIdx = sysStatus->GetCurrentVesselIndex();
            const auto experiment = sysStatus->GetExperiment();
            const auto fov = experiment->GetFOV();
            const auto xInUm = fov.toUM().width;
            const auto yInUm = fov.toUM().height;
            const auto model = AppEntity::System::GetModel();
            const auto pixelSize = model->CameraPixelSize() / model->ObjectiveLensMagnification();
            const auto totalPixel = std::make_tuple(std::round(xInUm / pixelSize), std::round(yInUm / pixelSize));
            const auto scenario = experiment->GetScenario(vesselIdx);
            const auto seqCount = scenario->GetCount();

            calculator->SetPoints(d->points);
            calculator->SetPixels(static_cast<int32_t>(std::get<0>(totalPixel)), 
                                  static_cast<int32_t>(std::get<1>(totalPixel)));

            QMap<Modality, int32_t> modalityCounts;
            for (auto seqIndex = 0; seqIndex < seqCount; ++seqIndex) {
                auto sequence = scenario->GetSequence(seqIndex);

                for (const auto& condition : sequence->GetImagingCondition()) {
                    const auto& imagingType = condition->GetImagingType();
                    const auto& modality = d->ConvertModality(imagingType);

                    if (modality == +Modality::NotSupport) { continue; }

                    switch (modality) {
                        case Modality::BFGray: 
                            modalityCounts[modality] += sequence->GetTimeCount();
                            calculator->SetSliceCount(modality, condition->GetSliceCount());
                            break;
                        case Modality::FL2D:
                        case Modality::FL3D: {
                            const auto& flCondition = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(condition);
                            modalityCounts[modality] = modalityCounts[modality] + (sequence->GetTimeCount() * flCondition->GetChannels().size());
                            calculator->SetSliceCount(modality, flCondition->GetSliceCount());
                            break;
                        }
                        case Modality::HT3D: 
                            modalityCounts[modality] += sequence->GetTimeCount();
                            calculator->SetSliceCount(modality, condition->GetSliceCount() * 4);
                            break;
                        case Modality::NotSupport: 
                        default:
                            break;
                    }
                }
            }

            for (auto it = modalityCounts.cbegin(); it != modalityCounts.cend(); ++it) {
                const auto& modality = it.key();
                const auto& modalityCount = it.value();
                calculator->SetModalityCount(modality, modalityCount);
            }

            bytesNeed = calculator->GetRequiredSpace();
        }

        if (d->output) {
            d->output->UpdateAcquisitionDataRequiredSpace(bytesNeed);
        }
        else {
            Error("IImagingConditionOutputPort doesn't created.");
            result = false;
        }
        return result;
    }

    auto GetAcquisitionDataRequiredSpace::Impl::ConvertModality(const AppEntity::ImagingType& entityType) const -> AcqDataSpaceCal::Modality {
        switch (entityType) {
            case AppEntity::ImagingType::BFGray: return AcqDataSpaceCal::Modality::BFGray;
            case AppEntity::ImagingType::FL2D: return AcqDataSpaceCal::Modality::FL2D;
            case AppEntity::ImagingType::FL3D: return AcqDataSpaceCal::Modality::FL3D;
            case AppEntity::ImagingType::HT3D: return AcqDataSpaceCal::Modality::HT3D;
            case AppEntity::ImagingType::HT2D:
            case AppEntity::ImagingType::BFColor: default: return AcqDataSpaceCal::Modality::NotSupport;
        }
    }
}
