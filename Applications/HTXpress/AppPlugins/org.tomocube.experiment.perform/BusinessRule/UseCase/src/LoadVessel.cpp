#include <QElapsedTimer>
#include <QCoreApplication>

#include <System.h>
#include <SystemStatus.h>
#include <IInstrument.h>

#include "ICondenserAutofocus.h"
#include "Utility.h"
#include "LoadVessel.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct LoadVessel::Impl {
        LoadVessel* p{ nullptr };
        IInstrumentOutputPort* output{ nullptr };

        Impl(LoadVessel* p) : p(p) {}

        auto ReportError(const QString& message)->void;
        auto UpdateProgress(double progress, const QString& message = QString())->void;
        auto MoveCAxis(double pos, int32_t timeoutSec, double progressStart, double progressEnd)->bool;
        auto FindFirstWell()->AppEntity::WellIndex;
        auto GetCenterPosition(const AppEntity::WellIndex wellIdx, double zPosMM)->AppEntity::Position;
    };

    auto LoadVessel::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
        if(output) output->UpdateFailed(message);
    }

    auto LoadVessel::Impl::UpdateProgress(double progress, const QString& message) -> void {
        if(output) output->UpdateProgress(progress, message);
    }

    auto LoadVessel::Impl::MoveCAxis(double pos, int32_t timeoutSec, double progressStart, double progressEnd) -> bool {
        auto instrument = IInstrument::GetInstance();
        if(!instrument->MoveAxis(AppEntity::Axis::C, pos)) return false;

        QElapsedTimer timer;
        timer.start();

        auto motionStatus = instrument->CheckAxisMotion();
        while(motionStatus.moving) {
            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 100);
            motionStatus = instrument->CheckAxisMotion();
            UpdateProgress(std::min((timer.elapsed()/100)*0.001+progressStart, progressEnd));

            if(timer.elapsed() > timeoutSec*1000) return false;
        }

        return true;
    }

    auto LoadVessel::Impl::FindFirstWell() -> AppEntity::WellIndex {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto experiment = sysStatus->GetExperiment();
        if(experiment == nullptr) {
            ReportError(tr("An experiment must be selected first."));
            return -1;
        }

        const auto vesselIndex = sysStatus->GetCurrentVesselIndex();
        const auto wellGroups = experiment->GetWellGroupCount(vesselIndex);
        if( wellGroups == 0) {
            ReportError(tr("Invalid experiment. No specimen is defined in the experiment."));
            return -1;
        }

        const auto vessel = experiment->GetVessel();
        const auto wellA1Pos = vessel->GetWellPosition(0, 0);
        AppEntity::WellIndex wellIndex = -1;
        double minDist = std::numeric_limits<double>::max();

        for(auto idx=0; idx<wellGroups; idx++) {
            const auto wellGroup = experiment->GetWellGroup(vesselIndex, idx);
            const auto wells = wellGroup.GetWells();
            if(wells.isEmpty()) continue;

            for(auto well : wells) {
                auto pos = vessel->GetWellPosition(well.rowIdx, well.colIdx);
                auto dist = std::sqrt(std::pow(wellA1Pos.toMM().x - pos.toMM().x, 2) + std::pow(wellA1Pos.toMM().y - pos.toMM().y, 2));
                if(dist < minDist) {
                    minDist = dist;
                    wellIndex = vessel->GetWellIndex(well.rowIdx, well.colIdx);
                }
            }
        }

        if(wellIndex == -1) {
            ReportError(tr("Invalid experiment. All specimens does not contain a well."));
        }

        return wellIndex;
    }

    auto LoadVessel::Impl::GetCenterPosition(const AppEntity::WellIndex wellIdx, double zPosMM) -> AppEntity::Position {
        const auto status = AppEntity::SystemStatus::GetInstance();
        const auto vesselIndex = status->GetCurrentVesselIndex();
        const auto experiment = status->GetExperiment();
        const auto ROIs = experiment->GetROIs(vesselIndex, wellIdx);

        if(ROIs.isEmpty()) {
            const auto vessel = experiment->GetVessel();
            return well2global(wellIdx, AppEntity::Position::fromMM(vessel->GetImagingAreaPositionX(),
                                                                    vessel->GetImagingAreaPositionY(),
                                                                    zPosMM));
        }

        const auto center = ROIs.first()->GetCenter().toMM();
        return well2global(wellIdx, AppEntity::Position::fromMM(center.x, center.y,zPosMM));
    }

    LoadVessel::LoadVessel(IInstrumentOutputPort* output) : IUseCase("LoadVessel"), d{new Impl(this)} {
        d->output = output;
    }

    LoadVessel::~LoadVessel() {
    }

    auto LoadVessel::Perform() -> bool {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto instrument = IInstrument::GetInstance();
        auto vessel = sysStatus->GetExperiment()->GetVessel();

        const auto startPos = instrument->GetAxisPosition();

        const auto zReadyMM = [=]()->double {
            auto scanReady = sysConfig->GetAutofocusReadyPos();
            auto afOffset = vessel->GetAFOffset()/1000.0;
            return scanReady + afOffset;
        }();

        const auto firstWell = d->FindFirstWell();
        if(firstWell == -1) {
            return false;
        }

        Print(QString("The first well is found at %1").arg(firstWell));
        const auto targetPos = d->GetCenterPosition(firstWell, zReadyMM);

        if(d->output) {
            d->UpdateProgress(0, "Loading vessel");
        }

        if(!instrument->MoveAxis(targetPos)) {
            d->ReportError(tr("It fails to move sample stage [%1]").arg(instrument->GetErrorMessage()));
            return false;
        }

        sysStatus->SetCurrentWell(firstWell);

        QElapsedTimer timer;
        timer.start();

        auto motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < 120000) && motionStatus.moving) {
            if(motionStatus.error) {
                d->ReportError(tr("It fails to move sample stage : %1").arg(motionStatus.message));
                return false;
            }

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

            const auto curPos = instrument->GetAxisPosition();
            AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);

            if(d->output) {
                const auto progressX = (curPos.toMM().x - startPos.toMM().x) / (targetPos.toMM().x - startPos.toMM().x);
                const auto progressY = (curPos.toMM().y - startPos.toMM().y) / (targetPos.toMM().y - startPos.toMM().y);
                d->UpdateProgress(std::min(progressX, progressY) * 0.5);
                d->output->UpdateGlobalPosition(curPos);
            }
            motionStatus = instrument->CheckAxisMotion();
        }

        if(!instrument->MoveAxis(AppEntity::Axis::Z, targetPos.toMM().z)) {
            d->ReportError(tr("It fails to move Z axis to the ready position [%1]").arg(instrument->GetErrorMessage()));
            return false;
        }

        timer.start();

        motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < 60000) && motionStatus.moving) {
            if(motionStatus.error) {
                d->ReportError(tr("It fails to move Z axis to the ready position : %1").arg(motionStatus.message));
                return false;
            }

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

            const auto curPos = instrument->GetAxisPosition();
            AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);

            if(d->output) {
                const auto progress = (curPos.toMM().z - startPos.toMM().z) / (targetPos.toMM().z - startPos.toMM().z);
                d->UpdateProgress(progress * 0.4 + 0.5);
                d->output->UpdateGlobalPosition(curPos);
            }

            motionStatus = instrument->CheckAxisMotion();
        }

        d->UpdateProgress(0.9, "Finding the best focus");

        using AFParam = AppEntity::AutoFocusParameter;
        const auto defaultFocus = sysConfig->GetAutofocusParameter(AFParam::DefaultTargetValue);
        if(!instrument->SetBestFocus(defaultFocus)) {
            d->ReportError(tr("It fails to set default focus target value as %1").arg(defaultFocus));
        }

        bool afSuccess = true;
        const auto afResult = instrument->PerformAutoFocus();
        if(!std::get<0>(afResult)) {
            afSuccess = false;
            d->ReportError(tr("Autofocus failure because of an error = %1 . Click \"OK\" to continue vessel unloading.").arg(instrument->GetErrorMessage()));
        } else if (!std::get<1>(afResult)) {
            afSuccess = false;
            d->ReportError(tr("Autofocus failure. Please check that the vessel is properly equipped with the holder and chamber. Click \"OK\" to continue vessel unloading."));
        }

        d->output->AutoFocusEnabled(afSuccess);

        if(!afSuccess) {
            d->UpdateProgress(1.0);
            return false;
        }

        const auto curPos = instrument->GetAxisPosition();
        sysStatus->SetCurrentGlobalPosition(curPos);
        d->output->UpdateGlobalPosition(curPos);
        sysStatus->SetCurrentWell(firstWell);

        const auto cAxisReadyPos = instrument->GetAxisPositionMM(AppEntity::Axis::C);
        sysStatus->SetCurrentCondensorPosition(cAxisReadyPos);

        d->UpdateProgress(1.0);
        d->output->VesselLoaded();

        return true;
    }
}
