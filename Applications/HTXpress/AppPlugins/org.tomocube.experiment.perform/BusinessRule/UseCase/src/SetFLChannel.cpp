#include <SystemStatus.h>
#include <ChannelConfig.h>
#include <System.h>

#include "ExperimentUpdater.h"
#include "IChannelConfigWriter.h"
#include "SetFLChannel.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetFLChannel::Impl {
        IImagingConditionOutputPort* output{ nullptr };
        bool updateExperiment{ true };
        int32_t channel{ 0 };
       AppEntity::ChannelConfig config;
    };

    SetFLChannel::SetFLChannel(bool updateExperiment, IImagingConditionOutputPort* output)
        : IUseCase("SetFLChannel"), d{new Impl} {
        d->output = output;
        d->updateExperiment = updateExperiment;
    }

    SetFLChannel::~SetFLChannel() {
    }

    auto SetFLChannel::SetChannel(const int32_t channel, const ChannelConfig& config) -> void {
        d->channel = channel;
        d->config = config;
    }

    auto SetFLChannel::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto mode = AppEntity::ImagingMode::_from_integral(AppEntity::ImagingMode::FLCH0 + d->channel);

        auto flConfig = sysStatus->GetChannelConfig(mode);
        if(!flConfig) return true;

        const auto minExposure = static_cast<uint32_t>(AppEntity::System::GetModel()->MinimumExposureUSec());
        if(d->config.GetCameraExposureUSec() < minExposure) {
            Error(QString("Exposure time, %1usec is smaller than the allowed minimum value, %2usec")
                  .arg(d->config.GetCameraExposureUSec()).arg(minExposure));
            return false;
        }

        flConfig->operator=(d->config);
        auto writer = IChannelConfigWriter::GetInstance();
        writer->Write(mode, *flConfig);

        if(d->updateExperiment) {
            auto exUpdater = ExperimentUpdater(sysStatus->GetExperiment());
            if(!exUpdater.SetFLChannelConfig(d->channel, *flConfig)) {
                Error("It fails to fill FL channel configuration.");
                return false;
            }
        }

        if (d->output) {
            d->output->UpdateFLChannelConfig(d->channel, flConfig);
        }

        //Todo FL 이미징 채널 설정 변경을 적용하여 Timetable 갱신하기

        return true;
    }
};