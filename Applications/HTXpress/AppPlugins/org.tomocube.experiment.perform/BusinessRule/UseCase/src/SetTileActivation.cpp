#include "SetTileActivation.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetTileActivation::Impl {
        IImagingConditionOutputPort* output{nullptr};
        bool enable{false};
    };

    SetTileActivation::SetTileActivation(IImagingConditionOutputPort* output) : IUseCase("SetTileActivation"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    SetTileActivation::~SetTileActivation() = default;
    
    auto SetTileActivation::SetEnable(bool enable) -> void {
        d->enable = enable;
    }

    auto SetTileActivation::Perform() -> bool {
        if (d->output) {
            d->output->UpdateTileActivation(d->enable);
        }

        return true;
    }
}
