#pragma once
#include <memory>
#include <QList>

#include <ImagingCondition.h>
#include <PositionGroup.h>
#include <Location.h>

#include "ImagingTimeTable.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class TimeTableUpdater {
    public:
        using ImagingCondition = AppEntity::ImagingCondition;
        using PositionGroup = AppEntity::PositionGroup;
        using Location = AppEntity::Location;

    public:
        TimeTableUpdater();
        ~TimeTableUpdater();

        auto Apply(ImagingCondition::Pointer cond)->void;
        auto Apply(const QList<PositionGroup>& groups)->void;
        auto Apply(const QList<Location>& locations)->void;

        auto Update() const->ImagingTimeTable::Pointer;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}