#include <SystemStatus.h>

#include "IInstrument.h"
#include "Utility.h"
#include "GetGlobalPosition.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct GetGlobalPosition::Impl {
        enum class Mode {
            XYZ,
            Specific
        };

        IMotionOutputPort* output{ nullptr };
        AppEntity::WellIndex wellIndex{ -1 };
        AppEntity::Position position;

        Mode mode { Mode::XYZ };
        AppEntity::Axis specificAxis { AppEntity::Axis::X };
        double specificAxisPos{ 0 };
    };

    GetGlobalPosition::GetGlobalPosition(IMotionOutputPort* outputPort) : IUseCase("GetGlobalPosition"), d{new Impl} {
        d->output = outputPort;
    }

    GetGlobalPosition::~GetGlobalPosition() {
    }

    auto GetGlobalPosition::SetSpecificAxis(AppEntity::Axis axis) -> void {
        d->specificAxis = axis;
        d->mode = Impl::Mode::Specific;
    }

    auto GetGlobalPosition::CurrentPosition() const -> AppEntity::Position {
        if(d->mode == Impl::Mode::Specific) {
            throw std::invalid_argument("Not supported with the specific mode");
        }

        return d->position;
    }

    auto GetGlobalPosition::CurrentPosition(AppEntity::Axis axis) -> double {
        double posInMM = 0;

        if(d->mode == Impl::Mode::XYZ) {
            switch(axis) {
            case AppEntity::Axis::X:
                posInMM = d->position.toMM().x;
                break;
            case AppEntity::Axis::Y:
                posInMM = d->position.toMM().y;
                break;
            case AppEntity::Axis::Z:
                posInMM = d->position.toMM().z;
                break;
            default:
                Error(QString("Invalid axis : %1").arg(axis._to_string()));
                throw std::invalid_argument("Not supported axis");
            }
        } else {
            posInMM = d->specificAxisPos;
        }

        return posInMM;
    }

    auto GetGlobalPosition::CurrentWell() const->AppEntity::WellIndex {
        if(d->mode == Impl::Mode::Specific) {
            throw std::invalid_argument("Not supported with the specific mode");
        }

        return d->wellIndex;
    }

    auto GetGlobalPosition::Perform() -> bool {
        auto* instrument = IInstrument::GetInstance();
        if(!instrument) {
            Error("Instrument is not available");
            return false;
        }

        auto status = AppEntity::SystemStatus::GetInstance();

        if(d->mode == Impl::Mode::XYZ) {
            auto globalPos = instrument->GetAxisPosition();

            d->position = globalPos;
            d->wellIndex = global2wellIndex(globalPos);

            status->SetCurrentGlobalPosition(globalPos);
            status->SetCurrentWell(d->wellIndex);

            if(d->output) d->output->UpdateGlobalPosition(d->position);
        } else {
            d->specificAxisPos = instrument->GetAxisPositionMM(d->specificAxis);
        }

        return true;
    }
}
