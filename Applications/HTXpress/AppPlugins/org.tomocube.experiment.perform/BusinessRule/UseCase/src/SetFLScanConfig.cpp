#include <SystemStatus.h>
#include "ExperimentUpdater.h"
#include "SetFLScanConfig.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetFLScanConfig::Impl {
        IImagingConditionOutputPort* output{ nullptr };
        bool updatExperiment{ true };
        AppEntity::ScanConfig config;
    };

    SetFLScanConfig::SetFLScanConfig(bool updateExperiment, IImagingConditionOutputPort* output)
        : IUseCase("SetFLScanConfig"), d{new Impl} {
        d->output = output;
        d->updatExperiment = updateExperiment;
    }

    SetFLScanConfig::~SetFLScanConfig() {
    }

    auto SetFLScanConfig::SetScanConfig(const AppEntity::ScanConfig& config) -> void {
        d->config = config;
    }

    auto SetFLScanConfig::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        QList<AppEntity::ImagingMode> channels {AppEntity::ImagingMode::FLCH0,
                                             AppEntity::ImagingMode::FLCH1,
                                             AppEntity::ImagingMode::FLCH2};

        for(const auto& channel : channels) {
            auto flConfig = sysStatus->GetScanConfig(channel);
            if(!flConfig) continue;
            flConfig->operator=(d->config);
        }

        if(d->updatExperiment) {
            auto expUpdater = ExperimentUpdater(sysStatus->GetExperiment());
            expUpdater.SetFLScanConfig(d->config);
        }

        //Todo FL Scan 범위 변경을 적용하여 Timetable 갱신하기

        return true;
    }
}
