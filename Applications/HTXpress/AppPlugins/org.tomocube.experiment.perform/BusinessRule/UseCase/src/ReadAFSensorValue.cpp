#include "IInstrument.h"
#include "ReadAFSensorValue.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ReadAFSensorValue::Impl {
        IInstrumentOutputPort* output{ nullptr };
        int32_t value{ 0 };
    };

    ReadAFSensorValue::ReadAFSensorValue(IInstrumentOutputPort* output) : IUseCase("ReadAFSensorValue"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    ReadAFSensorValue::~ReadAFSensorValue() {
    }

    auto ReadAFSensorValue::GetValue() const -> int32_t {
        return d->value;
    }

    auto ReadAFSensorValue::Perform() -> bool {
        auto* instrument = IInstrument::GetInstance();
        if(!instrument) return false;

        d->value = instrument->ReadAFSensorValue();
        return true;
    }
}
