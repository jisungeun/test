#include <System.h>
#include <SystemStatus.h>
#include "IExperimentWriter.h"
#include "ScanTimeCalculatorUpdater.h"
#include "ExperimentUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ExperimentUpdater::Impl {
        explicit Impl(bool save) : saveWhenDestroyed(save){}
        AppEntity::Experiment::Pointer experiment{ nullptr };
        const bool saveWhenDestroyed{};
    };

    ExperimentUpdater::ExperimentUpdater(Experiment::Pointer experiment, bool saveWhenDestroyed) : d{std::make_unique<Impl>(saveWhenDestroyed)} {
        d->experiment = experiment;
    }

    ExperimentUpdater::~ExperimentUpdater() {
        if(d->saveWhenDestroyed) {
            auto sysStatus = AppEntity::SystemStatus::GetInstance();
            IExperimentWriter::GetInstance()->Write(d->experiment, sysStatus->GetExperimentPath());
            ScanTimeCalculatorUpdater::SetExperiment(d->experiment);
        }
    }

    auto ExperimentUpdater::SetFieldOfView(double xInUm, double yInUm) -> void {
        const auto area = AppEntity::Area::fromUM(xInUm, yInUm);
        d->experiment->SetFOV(area);

        const auto vesselCount = d->experiment->GetVesselCount();
        for(auto vesselIdx=0; vesselIdx<vesselCount; vesselIdx++) {
            auto& locationGroups = d->experiment->GetAllLocations(vesselIdx);
            for(auto& locationGroup : locationGroups) {
                for(auto& location : locationGroup) {
                    if(location.IsTile()) continue;
                    location.SetArea(area, false);
                }
            }
        }
    }

    auto ExperimentUpdater::SetHTChannelConfig(bool is3D, const ChannelConfig& config,
                                               const ScanConfig& scanConfig) -> void {
        auto update = [=](AppEntity::ImagingCondition::Pointer imgCond)->void {
            auto htCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionHT>(imgCond);

            htCond->SetExposure(config.GetCameraExposureUSec());
            htCond->SetInterval(config.GetCameraIntervalUSec());
            htCond->SetIntensity(config.GetLightIntensity());

            htCond->SetSliceStart(scanConfig.GetBottom());
            htCond->SetSliceCount(scanConfig.GetSteps());
            htCond->SetSliceStep(scanConfig.GetStep());
        };

        const auto imagingType = is3D ? AppEntity::ImagingType::HT3D : AppEntity::ImagingType::HT2D;
        auto imgCond = d->experiment->GetSingleImagingCondition(imagingType);
        if(!imgCond) {
            imgCond = std::make_shared<AppEntity::ImagingConditionHT>();
            imgCond->SetDimension(is3D ? 3 : 2);
            d->experiment->SetSingleImagingCondition(imagingType, imgCond);
        }
        update(imgCond);

        d->experiment->SetEnableSingleImaging(imagingType, config.GetEnable());

        const auto vesselCount = d->experiment->GetVesselCount();
        for(auto vesselIdx=0; vesselIdx<vesselCount; vesselIdx++) {
            auto scenario = d->experiment->GetScenario(vesselIdx);
            if(!scenario) continue;

            const auto seqCount = scenario->GetCount();
            for(auto seqIdx=0; seqIdx<seqCount; seqIdx++) {
                auto sequence = scenario->GetSequence(seqIdx);
                if(!sequence) continue;

                auto modalityCount = sequence->GetModalityCount();
                for(auto modIdx=0; modIdx<modalityCount; modIdx++) {
                    imgCond = sequence->GetImagingCondition(modIdx);
                    if(!imgCond) continue;
                    if(imgCond->GetModality() != +AppEntity::Modality::HT) continue;
                    if(is3D != imgCond->Is3D()) continue;
                    update(imgCond);
                }
            }
        }
    }

    auto ExperimentUpdater::SetHTIntensity(int32_t intensity) -> void {
        using ImagingType = AppEntity::ImagingType;

        std::vector<ImagingType> htTypes{ ImagingType::HT2D, ImagingType::HT3D };
        for(auto type : htTypes) {
            auto imgCond = d->experiment->GetSingleImagingCondition(type);
            if(!imgCond) continue;

            auto htCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionHT>(imgCond);
            htCond->SetIntensity(intensity);
        }

        const auto vesselCount = d->experiment->GetVesselCount();
        for(auto vesselIdx=0; vesselIdx<vesselCount; vesselIdx++) {
            auto scenario = d->experiment->GetScenario(vesselIdx);
            if(!scenario) continue;

            const auto seqCount = scenario->GetCount();
            for(auto seqIdx=0; seqIdx<seqCount; seqIdx++) {
                auto sequence = scenario->GetSequence(seqIdx);
                if(!sequence) continue;

                const auto modalityCount = sequence->GetModalityCount();
                for(auto modIdx=0; modIdx<modalityCount; modIdx++) {
                    auto imgCond = sequence->GetImagingCondition(modIdx);
                    if(!imgCond) continue;
                    if(imgCond->GetModality() != +AppEntity::Modality::HT) continue;

                    auto htCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionHT>(imgCond);
                    htCond->SetIntensity(intensity);
                }
            }
        }
    }

    auto ExperimentUpdater::SetFLScanConfig(const ScanConfig& config) -> void {
        auto update = [=](AppEntity::ImagingCondition::Pointer imgCond)->void {
            auto flCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(imgCond);
            flCond->SetScanMode(config.GetScanMode());
            flCond->SetScanRange(config.GetBottom(), config.GetTop(), config.GetStep());
            flCond->SetScanFocus(0, config.GetFocusFLOffsetUm());
            flCond->SetSliceCount(config.GetSteps());
            flCond->SetSliceStep(config.GetStep());
        };

        auto imgCond = d->experiment->GetSingleImagingCondition(AppEntity::ImagingType::FL3D);
        if(!imgCond) {
            imgCond = std::make_shared<AppEntity::ImagingConditionFL>();
            imgCond->SetDimension(3);
            d->experiment->SetSingleImagingCondition(AppEntity::ImagingType::FL3D, imgCond);
        }
        update(imgCond);

        const auto vesselCount = d->experiment->GetVesselCount();
        for(auto vesselIdx=0; vesselIdx<vesselCount; vesselIdx++) {
            auto scenario = d->experiment->GetScenario(vesselIdx);
            if(!scenario) continue;

            const auto seqCount = scenario->GetCount();
            for(auto seqIdx=0; seqIdx<seqCount; seqIdx++) {
                auto sequence = scenario->GetSequence(seqIdx);
                if(!sequence) continue;

                auto modalityCount = sequence->GetModalityCount();
                for(auto modIdx=0; modIdx<modalityCount; modIdx++) {
                    imgCond = sequence->GetImagingCondition(modIdx);
                    if(!imgCond) continue;
                    if(imgCond->GetModality() != +AppEntity::Modality::FL) continue;
                    if(!imgCond->Is3D()) continue;

                    update(imgCond);
                }
            }
        }
    }

    auto ExperimentUpdater::SetFLChannelConfig(int32_t channel, const ChannelConfig& config) -> bool {
        const auto sysConfig = AppEntity::System::GetSystemConfig();

        AppEntity::FLChannel flChannel;
        AppEntity::FLFilter exFilter, emFilter;

        // channel name이 empty일 경우 해당 channel 삭제(FL2D/FL3D)
        if (config.GetChannelName().isEmpty()) {
            auto imgCond = d->experiment->GetSingleImagingCondition(AppEntity::ImagingType::FL3D);
            if (imgCond) {
                auto flCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(imgCond);
                flCond->DeleteChannel(channel);
            }
            imgCond = d->experiment->GetSingleImagingCondition(AppEntity::ImagingType::FL2D);
            if (imgCond) {
                auto flCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(imgCond);
                flCond->DeleteChannel(channel);
            }
        }

        if (!sysConfig->GetFLActiveExcitation(config.GetLightChannel(), exFilter)) return false;
        if (!sysConfig->GetFLEmission(config.GetFilterChannel(), emFilter)) return false;
        if (!sysConfig->GetFLChannel(config.GetChannelName(), flChannel)) return false;

        AppEntity::ImagingConditionFL::Color color;
        color.red = flChannel.GetColor().red();
        color.green = flChannel.GetColor().green();
        color.blue = flChannel.GetColor().blue();

        auto update = [=](AppEntity::ImagingCondition::Pointer imgCond)->void {
            auto flCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(imgCond);
            flCond->SetChannel(channel,
                               config.GetCameraExposureUSec(),
                               config.GetCameraIntervalUSec(),
                               config.GetLightIntensity(),
                               0,
                               exFilter,
                               emFilter,
                               config.GetChannelName(),
                               color);
        };

        auto imgCond = d->experiment->GetSingleImagingCondition(AppEntity::ImagingType::FL3D);
        if(!imgCond) {
            imgCond = std::make_shared<AppEntity::ImagingConditionFL>();
            imgCond->SetDimension(3);
            d->experiment->SetSingleImagingCondition(AppEntity::ImagingType::FL3D, imgCond);
        }
        update(imgCond);

        imgCond = d->experiment->GetSingleImagingCondition(AppEntity::ImagingType::FL2D);
        if(!imgCond) {
            imgCond = std::make_shared<AppEntity::ImagingConditionFL>();
            imgCond->SetDimension(2);
            d->experiment->SetSingleImagingCondition(AppEntity::ImagingType::FL2D, imgCond);
        }
        update(imgCond);

        const auto vesselCount = d->experiment->GetVesselCount();
        for(auto vesselIdx=0; vesselIdx<vesselCount; vesselIdx++) {
            auto scenario = d->experiment->GetScenario(vesselIdx);
            if(!scenario) continue;
            
            const auto seqCount = scenario->GetCount();
            for(auto seqIdx=0; seqIdx<seqCount; seqIdx++) {
                auto sequence = scenario->GetSequence(seqIdx);
                if(!sequence) continue;
                
                auto modalityCount = sequence->GetModalityCount();
                for(auto modIdx=0; modIdx<modalityCount; modIdx++) {
                    imgCond = sequence->GetImagingCondition(modIdx);
                    if(!imgCond) continue;
                    if(imgCond->GetModality() != +AppEntity::Modality::FL) continue;

                    auto flCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(imgCond);
                    if (flCond && flCond->GetChannels().contains(channel)) update(imgCond);
                }
            }
        }

        return true;
    }

    auto ExperimentUpdater::SetFLEnabled(const bool is3D, const bool enabled) -> void {
        auto imagingType = is3D ? AppEntity::ImagingType::FL3D : AppEntity::ImagingType::FL2D;
        auto imgCond = d->experiment->GetSingleImagingCondition(imagingType);
        if(!imgCond) {
            imgCond = std::make_shared<AppEntity::ImagingConditionFL>();
            imgCond->SetDimension(is3D ? 3 : 2);
            d->experiment->SetSingleImagingCondition(imagingType, imgCond);
        }
        d->experiment->SetEnableSingleImaging(imagingType, enabled);
    }

    auto ExperimentUpdater::SetBFChannelConfig(const bool isGray, const ChannelConfig& config) -> void {
        auto update = [=](AppEntity::ImagingCondition::Pointer imgCond)->void {
            auto bfCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionBF>(imgCond);

            bfCond->SetExposure(config.GetCameraExposureUSec());
            bfCond->SetInterval(config.GetCameraIntervalUSec());
            bfCond->SetIntensity(config.GetLightIntensity());
            bfCond->SetColorMode(!isGray);

            bfCond->SetSliceStart(0);
            bfCond->SetSliceCount(1);
            bfCond->SetSliceStep(0);
        };

        const auto imagingType = isGray ? AppEntity::ImagingType::BFGray : AppEntity::ImagingType::BFColor;
        auto imgCond = d->experiment->GetSingleImagingCondition(imagingType);
        if(!imgCond) {
            imgCond = std::make_shared<AppEntity::ImagingConditionBF>();
            imgCond->SetDimension(2);
            d->experiment->SetSingleImagingCondition(imagingType, imgCond);
        }
        update(imgCond);

        d->experiment->SetEnableSingleImaging(imagingType, config.GetEnable());

        const auto vesselCount = d->experiment->GetVesselCount();
        for(auto vesselIdx=0; vesselIdx<vesselCount; vesselIdx++) {
            auto scenario = d->experiment->GetScenario(vesselIdx);
            if(!scenario) continue;

            const auto seqCount = scenario->GetCount();
            for(auto seqIdx=0; seqIdx<seqCount; seqIdx++) {
                auto sequence = scenario->GetSequence(seqIdx);
                if(!sequence) continue;

                auto modalityCount = sequence->GetModalityCount();
                for(auto modIdx=0; modIdx<modalityCount; modIdx++) {
                    imgCond = sequence->GetImagingCondition(modIdx);
                    if(!imgCond) continue;
                    if(imgCond->GetModality() != +AppEntity::Modality::BF) continue;
                    update(imgCond);
                }
            }
        }
    }

    auto ExperimentUpdater::SetImagingScenario(VesselIndex vesselIdx, ImagingScenario::Pointer scenario) -> void {
        d->experiment->SetScenario(vesselIdx, scenario);
    }

    auto ExperimentUpdater::AddLocation(VesselIndex vesselIdx, WellIndex wellIdx, Location location) -> LocationIndex {
        return d->experiment->AddLocation(vesselIdx, wellIdx, location);
    }

    auto ExperimentUpdater::DeleteLocation(VesselIndex vesselIdx, WellIndex wellIdx, LocationIndex index) -> bool {
        return d->experiment->DeleteLocation(vesselIdx, wellIdx, index);
    }

    auto ExperimentUpdater::ClearAllLocations() -> void {
        const auto vesselCount = d->experiment->GetVesselCount();
        for(auto idx=0; idx<vesselCount; ++idx) {
            d->experiment->ClearLocation(idx);
        }
    }

}
