#include <SystemStatus.h>

#include "SetAcquisitionLock.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetAcquisitionLock::Impl {
        IImagingConditionOutputPort* output{ nullptr };
        bool locked { true };
    };

    SetAcquisitionLock::SetAcquisitionLock(IImagingConditionOutputPort* output) : IUseCase("SetAcquisitionLock"), d{new Impl} {
        d->output = output;
    }

    SetAcquisitionLock::~SetAcquisitionLock() {
    }

    auto SetAcquisitionLock::SetLock(bool locked) -> void {
        d->locked = locked;
    }

    auto SetAcquisitionLock::Perform() -> bool {
        if (d->output) d->output->UpdateAcquisitionLock(d->locked);

        return true;
    }
};