#include "Utility.h"
#include "IExperimentRunner.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static IExperimentRunner* theInstance{ nullptr };

    IExperimentRunner::IExperimentRunner() {
        theInstance = this;
    }

    IExperimentRunner::~IExperimentRunner() {
    }

    auto IExperimentRunner::GetInstance() -> IExperimentRunner* {
        return theInstance;
    }

    auto IExperimentRunner::ConvertGlobal2WellIndex(const AppEntity::Position& globalPos) -> AppEntity::WellIndex {
        return global2wellIndex(globalPos);
    }

    auto IExperimentRunner::ConvertGlobal2Well(const AppEntity::Position& globalPos,
                                               const AppEntity::WellIndex wellIdx) -> AppEntity::Position {
        return global2well(globalPos, wellIdx);
    }
}

