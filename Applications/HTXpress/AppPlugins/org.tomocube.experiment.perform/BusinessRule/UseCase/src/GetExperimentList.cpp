#include <QDir>

#include <System.h>
#include <SessionManager.h>

#include "GetExperimentList.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct GetExperimentList::Impl {
        QString project;
        QStringList list;
    };

    GetExperimentList::GetExperimentList() : IUseCase("GetExperimentList"), d{new Impl} {
    }

    GetExperimentList::~GetExperimentList() {
    }

    auto GetExperimentList::SetProject(const QString& project) -> void {
        d->project = project;
    }

    auto GetExperimentList::GetList() const -> QStringList {
        return d->list;
    }

    auto GetExperimentList::Perform() -> bool {
        const auto& dataDir = AppEntity::System::GetSystemConfig()->GetDataDir();
        const auto& userID = AppEntity::SessionManager::GetInstance()->GetID();

        QDir dir(QString("%1/%2/%3").arg(dataDir).arg(userID).arg(d->project));
        d->list = dir.entryList(QDir::Filter::Dirs|QDir::Filter::NoDotAndDotDot, QDir::SortFlag::Name);

        return true;
    }
}