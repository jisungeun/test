#include "IBeadEvaluator.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static IBeadEvaluator* theInstance{ nullptr };

    IBeadEvaluator::IBeadEvaluator() {
        theInstance = this;
    }

    IBeadEvaluator::~IBeadEvaluator() {
    }

    auto IBeadEvaluator::GetInstance() -> IBeadEvaluator* {
        return theInstance;
    }
}