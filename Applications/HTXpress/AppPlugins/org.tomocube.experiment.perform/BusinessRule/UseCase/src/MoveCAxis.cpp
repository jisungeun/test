#include <QElapsedTimer>

#include "IInstrument.h"
#include "MoveCAxis.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct MoveCAxis::Impl {
        IMotionOutputPort* output{ nullptr };
        MoveCAxis* p{ nullptr };

        double target{ 0 };

        Impl(MoveCAxis* p) : p{ p } {}
        auto ReportError(const QString& message)->void;
    };

    auto MoveCAxis::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
        if(output) output->ReportError(message);
    }

    MoveCAxis::MoveCAxis(IMotionOutputPort* output) : IUseCase("MoveCAxis"), d{new Impl(this)} {
        d->output = output;
    }

    MoveCAxis::~MoveCAxis() {
    }

    auto MoveCAxis::SetTarget(double posInMm) -> void {
        d->target = posInMm;
    }

    auto MoveCAxis::Perform() -> bool {
        using Axis = AppEntity::Axis;

        auto instrument = IInstrument::GetInstance();
        if(!instrument->IsInitialized()) return true;

        Print(QString("Move C axis to %1mm").arg(d->target));

        if(!instrument->MoveAxis(Axis::C, d->target)) {
            d->ReportError(tr("It fails to move C axis [%1]").arg(instrument->GetErrorMessage()));
            return false;
        }

        QElapsedTimer timer;
        timer.start();

        auto motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < 60000) && motionStatus.moving) {
            if(motionStatus.error) {
                d->ReportError(tr("It fails to move C axis : %1").arg(motionStatus.message));
                return false;
            }

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

            auto pos = instrument->GetAxisPositionMM(Axis::C);
            if(d->output) d->output->UpdateGlobalPosition(Axis::C, pos);

            motionStatus = instrument->CheckAxisMotion();
        }

        if(motionStatus.moving) {
            d->ReportError(tr("Timeout is occurred"));
            return false;
        }

        auto pos = instrument->GetAxisPositionMM(Axis::C);
        if(d->output) d->output->UpdateGlobalPosition(Axis::C, pos);

        return true;
    }
}