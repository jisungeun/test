#pragma once
#include <memory>

#include <Experiment.h>
#include <ScanConfig.h>
#include <ChannelConfig.h>

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class ExperimentUpdater {
    public:
        using Experiment = AppEntity::Experiment;
        using VesselIndex = AppEntity::VesselIndex;
        using WellIndex = AppEntity::WellIndex;
        using ImagingScenario = AppEntity::ImagingScenario;
        using Location = AppEntity::Location;
        using LocationIndex = AppEntity::LocationIndex;
        using ChannelConfig =AppEntity::ChannelConfig;
        using ScanConfig = AppEntity::ScanConfig;

    public:
        ExperimentUpdater(Experiment::Pointer experiment, bool saveWhenDestroyed = true);
        ~ExperimentUpdater();

        auto SetFieldOfView(double xInUm, double yInUm)->void;

        auto SetHTChannelConfig(bool is3D, const ChannelConfig& config, const ScanConfig& scanConfig)->void;
        auto SetHTIntensity(int32_t intensity)->void;

        auto SetFLScanConfig(const ScanConfig& config)->void;
        auto SetFLChannelConfig(int32_t channel, const ChannelConfig& confg)->bool;
        auto SetFLEnabled(const bool is3D, const bool enabled)->void;

        auto SetBFChannelConfig(const bool isGray, const ChannelConfig& config)->void;

        auto SetImagingScenario(VesselIndex vesselIdx, ImagingScenario::Pointer scenario)->void;

        auto AddLocation(VesselIndex vesselIdx, WellIndex wellIdx, Location location)->LocationIndex;
        auto DeleteLocation(VesselIndex vesselIdx, WellIndex wellIdx, LocationIndex index)->bool;
        auto ClearAllLocations()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}