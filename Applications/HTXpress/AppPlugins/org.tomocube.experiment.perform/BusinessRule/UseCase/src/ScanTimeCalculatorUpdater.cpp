#include <QMap>
#include <System.h>

#include "IScanTimeCalculator.h"
#include "ScanTimeCalculatorUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    auto ScanTimeCalculatorUpdater::SetExperiment(Experiment::Pointer experiment) -> bool {
        using ImagingType = AppEntity::ImagingType;
        using ImagingConditionFL = AppEntity::ImagingConditionFL;
        using FLFilter = AppEntity::FLFilter;
        using Modality = AppEntity::Modality;

        auto* calculator = IScanTimeCalculator::GetInstance();
        if(!calculator) return false;

        auto imgCond = experiment->GetSingleImagingCondition(ImagingType::FL3D);
        if(imgCond == nullptr) imgCond = experiment->GetSingleImagingCondition(ImagingType::FL2D);
        if(imgCond == nullptr) return true;

        auto emFilters = []()->QMap<FLFilter,int32_t> {
            QMap<FLFilter,int32_t> filters;

            auto sysConfig = AppEntity::System::GetSystemConfig();
            for(const auto filterIndex : sysConfig->GetFLEmissions()) {
                FLFilter filter;
                sysConfig->GetFLEmission(filterIndex, filter);
                filters[filter] = filterIndex; 
            }

            return filters;
        }();

        auto flCond = std::static_pointer_cast<ImagingConditionFL>(imgCond);
        for(const auto channel : flCond->GetChannels()) {
            auto emFilter = flCond->GetEmFilter(channel);
            auto itr = emFilters.find(emFilter);
            if(itr == emFilters.end()) return false;

            calculator->SetFilterPosition(Modality::FL, channel, itr.value());
        }

        return true;
    }
}