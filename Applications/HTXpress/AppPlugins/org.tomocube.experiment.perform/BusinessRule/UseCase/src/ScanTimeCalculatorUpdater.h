#pragma once
#include <memory>

#include <Experiment.h>
#include <ChannelConfig.h>

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class ScanTimeCalculatorUpdater {
    public:
        using Experiment = AppEntity::Experiment;

    public:
        static auto SetExperiment(Experiment::Pointer experiment)->bool;
    };
}