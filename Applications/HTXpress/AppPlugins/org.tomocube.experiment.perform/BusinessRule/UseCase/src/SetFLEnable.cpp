#include <SystemStatus.h>
#include <ChannelConfig.h>
#include "ExperimentUpdater.h"
#include "SetFLEnable.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetFLEnable::Impl {
        IImagingConditionOutputPort* output{ nullptr };
        bool updateExperiment{ true };
        bool is3D{ true };
        bool enable{ false };
    };

    SetFLEnable::SetFLEnable(bool updateExperiment, IImagingConditionOutputPort* output) : IUseCase("SetFLEnable"), d{new Impl} {
        d->output = output;
        d->updateExperiment = updateExperiment;
    }

    SetFLEnable::~SetFLEnable() {
    }

    auto SetFLEnable::SetEnable(const bool is3D, const bool enable) -> void {
        d->is3D = is3D;
        d->enable = enable;
    }

    auto SetFLEnable::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto experiment = sysStatus->GetExperiment();

        if(d->updateExperiment) {
            auto expUpdater = ExperimentUpdater(experiment);
            expUpdater.SetFLEnabled(d->is3D, d->enable);
        }

        if(d->output) d->output->UpdateEnabledImagingTypes(experiment->GetEnabledImagingTypes());

        //Todo FL 이미징 채널 설정 변경을 적용하여 Timetable 갱신하기

        return true;
    }
};