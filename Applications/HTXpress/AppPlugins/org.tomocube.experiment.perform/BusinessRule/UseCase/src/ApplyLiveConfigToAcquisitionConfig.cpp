#include <SystemStatus.h>
#include <ChannelConfig.h>

#include "ExperimentUpdater.h"
#include "IChannelConfigWriter.h"
#include "ApplyLiveConfigToAcquisitionConfig.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ApplyLiveConfigToAcqusitionConfig::Impl {
        ILiveviewConfigOutputPort* output{ nullptr };
        AppEntity::ImagingMode mode{ AppEntity::ImagingMode::FLCH0 };
       AppEntity::ChannelConfig config;
    };

    ApplyLiveConfigToAcqusitionConfig::ApplyLiveConfigToAcqusitionConfig(ILiveviewConfigOutputPort* output) : IUseCase("ApplyLiveConfigToAcqusitionConfig"), d{ new Impl } {
        d->output = output;
    }

    ApplyLiveConfigToAcqusitionConfig::~ApplyLiveConfigToAcqusitionConfig() {
    }

    auto ApplyLiveConfigToAcqusitionConfig::ApplyConfig(const AppEntity::ImagingMode mode) -> void {
        d->mode = mode;
    }

    auto ApplyLiveConfigToAcqusitionConfig::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        auto liveConfig = sysStatus->GetLiveConfig(d->mode);
        if (liveConfig == nullptr) {
            Error("It fails to apply live configuration.");
            return false;
        }

        auto channelConfig = sysStatus->GetChannelConfig(d->mode);
        if(channelConfig == nullptr) {
            Error("It fails to get channel configuration.");
            return false;
        }

        // live config 값을 해당 channel config에 적용
        auto intensity = liveConfig->GetLightIntensity();
        auto exposure = liveConfig->GetCameraExposureUSec();

        channelConfig->SetCameraExposureUSec(exposure);

        if (d->mode == +AppEntity::ImagingMode::FLCH0 ||
            d->mode == +AppEntity::ImagingMode::FLCH1 ||
            d->mode == +AppEntity::ImagingMode::FLCH2) {
            channelConfig->SetLightIntensity(intensity);
        }

        auto writer = IChannelConfigWriter::GetInstance();
        writer->Write(d->mode, *channelConfig);

        auto exUpdater = ExperimentUpdater(sysStatus->GetExperiment());
        switch (d->mode) {
        case AppEntity::ImagingMode::HT3D:
        case AppEntity::ImagingMode::HT2D:
            exUpdater.SetHTChannelConfig(d->mode == +AppEntity::ImagingMode::HT3D, *channelConfig, *sysStatus->GetScanConfig(d->mode));
            break;
        case AppEntity::ImagingMode::BFGray:
        case AppEntity::ImagingMode::BFColor:
            exUpdater.SetBFChannelConfig(d->mode == +AppEntity::ImagingMode::BFGray, *channelConfig);
            break;
        case AppEntity::ImagingMode::FLCH0:
        case AppEntity::ImagingMode::FLCH1:
        case AppEntity::ImagingMode::FLCH2:
            if(!exUpdater.SetFLChannelConfig(d->mode - AppEntity::ImagingMode::FLCH0, *channelConfig)) {
                Error("It fails to fill FL channel configuration.");
                return false;
            }
            break;
        }

        if (d->output) {
            d->output->UpdateAcquisitionConfig(d->mode, intensity, exposure, 0);
        }

        return true;
    }
};