#include <IBeadEvaluator.h>
#include "EvaluateBead.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct BeadData {
        QString path;
        int32_t xInPixel{ 0 };
        int32_t yInPixel{ 0 };
    };

    struct EvaluateBead::Impl {
        IQualityCheckOutputPort* output{ nullptr };
        EvaluateBead* p{ nullptr };

        QList<BeadData> inputs;
        QString outPath;
        QList<Entity::BeadScore> results;

        Impl(EvaluateBead* p) : p{ p } {}

        auto ReportError(const QString& message)->void;
    };

    auto EvaluateBead::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
    }

    EvaluateBead::EvaluateBead(IQualityCheckOutputPort* output) : IUseCase("EvaluateBead"), d{ std::make_unique<Impl>(this) } {
        d->output = output;
    }

    EvaluateBead::~EvaluateBead() {
    }

    auto EvaluateBead::AddData(const QString& path, int32_t xInPixel, int32_t yInPixel) -> void {
        d->inputs.append({path, xInPixel, yInPixel});
    }

    auto EvaluateBead::SetOutputPath(const QString& path) -> void {
        d->outPath = path;
    }

    auto EvaluateBead::GetResults() const -> QList<Entity::BeadScore> {
        return d->results;
    }

    auto EvaluateBead::Perform() -> bool {
        auto* evaluator = IBeadEvaluator::GetInstance();
        if(!evaluator) {
            d->ReportError("No bead evaluator is installed");
            return false;
        }

        evaluator->Clear();
        evaluator->InstallOutputPort(d->output);
        evaluator->SetOutputPath(d->outPath);

        for(const auto& input : d->inputs) {
            evaluator->AddData(input.path, input.xInPixel, input.yInPixel);
        }

        if(!evaluator->Evaluate()) {
            d->ReportError("It fails to evaluate optical performance");
            return false;
        }

        for(auto idx=0; idx<evaluator->GetCount(); ++idx) {
            d->results.push_back(evaluator->GetResult(idx));
        }

        return true;
    }
}
