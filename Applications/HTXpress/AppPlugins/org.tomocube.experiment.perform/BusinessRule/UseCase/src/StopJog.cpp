#include <QElapsedTimer>

#include <SystemStatus.h>

#include "IInstrument.h"
#include "Utility.h"
#include "StopJog.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct StopJog::Impl {
        StopJog* p{ nullptr };
        IMotionOutputPort* output{ nullptr };

        Impl(StopJog* p) : p{p} {}
        auto ReportError(const QString& message)->void;
    };

    auto StopJog::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
        if(output) output->ReportError(message);
    }

    StopJog::StopJog(IMotionOutputPort* output) : IUseCase("StopJog"), d{new Impl(this)} {
        d->output = output;
    }

    StopJog::~StopJog() {
    }

    auto StopJog::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();
        if(!instrument->IsInitialized()) return true;

        if(!instrument->StopJogAxis()) {
            d->ReportError(tr("It fails to stop jog : %1").arg(instrument->GetErrorMessage()));
            return false;
        }
        
        return true;
    }
}
