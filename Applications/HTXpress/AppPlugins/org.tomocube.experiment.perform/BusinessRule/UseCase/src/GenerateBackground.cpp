#include "IBGGenerator.h"
#include "GenerateBackground.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct GenerateBackground::Impl {
        QString source;
        QString target;
    };

    GenerateBackground::GenerateBackground() : IUseCase("GenerateBackground"), d{ std::make_unique<Impl>() } {
    }

    GenerateBackground::~GenerateBackground() {
    }

    auto GenerateBackground::SetSource(const QString& path) -> void {
        d->source = path;
    }

    auto GenerateBackground::SetTarget(const QString& path) -> void {
        d->target = path;
    }

    auto GenerateBackground::Perform() -> bool {
        auto* generator = IBGGenerator::GetInstance();
        if(generator == nullptr) return false;

        return generator->Process(d->source, d->target);
    }
}