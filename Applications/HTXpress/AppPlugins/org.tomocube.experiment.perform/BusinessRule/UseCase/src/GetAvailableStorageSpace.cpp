﻿#include <QString>

#include "GetAvailableStorageSpace.h"
#include "ISystemStorageManager.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct GetAvailableStorageSpace::Impl {
        explicit Impl(GetAvailableStorageSpace* self): self(self) {}

        GetAvailableStorageSpace* self{};

        auto Error(const QString& message) const -> void;
    };

    auto GetAvailableStorageSpace::Impl::Error(const QString& message) const -> void {
        self->Error(message);
    }

    GetAvailableStorageSpace::GetAvailableStorageSpace(): IUseCase("GetAvailableStorageSpace"), d{std::make_unique<Impl>(this)} {
    }

    GetAvailableStorageSpace::~GetAvailableStorageSpace() {
    }

    auto GetAvailableStorageSpace::Perform() -> bool {
        const auto manager = ISystemStorageManager::GetInstance();

        if(!manager) {
            d->Error("SystemStorageManager is null.");   
            return false;
        }

        manager->UpdateStorageInfo();
        return true;
    }
}
