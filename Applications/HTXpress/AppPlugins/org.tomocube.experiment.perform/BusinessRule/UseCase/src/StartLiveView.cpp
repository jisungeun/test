﻿#include <SystemStatus.h>
#include <ILiveImageAcquisition.h>
#include <IInstrument.h>

#include "StartLiveView.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct StartLiveView::Impl {
        IInstrumentOutputPort* output{ InstrumentNullOutputPort::GetInstance() };
        AppEntity::Modality modality{ AppEntity::Modality::HT };
        int32_t channel{ 0 };

        auto ReportError(StartLiveView* p, const QString& message)->void;
    };

    auto StartLiveView::Impl::ReportError(StartLiveView* p, const QString& message) -> void {
        p->Error(message);
        output->LiveImagingFailed(message);
    }

    StartLiveView::StartLiveView(IInstrumentOutputPort* output) : IUseCase("StartLiveView"), d{new Impl} {
        d->output = output;
    }

    StartLiveView::~StartLiveView() {
    }

    auto StartLiveView::SetImageType(AppEntity::Modality modality, int32_t channel) -> void {
        d->modality = modality;
        d->channel = channel;
    }

    auto StartLiveView::SetLatest() -> void {
        const auto latest = AppEntity::SystemStatus::GetInstance()->GetCurrentLiveImage();
        d->modality = std::get<0>(latest);
        d->channel = std::get<0>(latest);
    }

    auto StartLiveView::Perform() -> bool {
        if(d->modality == +AppEntity::Modality::HT) {
            d->ReportError(this, "It does not support live image of hologram");
            return false;
        }

        auto* instrument = IInstrument::GetInstance();
        if(!instrument) {
            d->ReportError(this, "No instrument exists");
            return false;
        }

        auto* liveAcquisition = ILiveImageAcquisition::GetInstance();
        if(!liveAcquisition) {
            d->ReportError(this, "Live acquisition is not available");
            return false;
        }

        liveAcquisition->Pause();

        if(!instrument->ChangeLiveModality(d->modality, d->channel)) {
            const auto msg = QString("It fails to change live imaging modality - %1").arg(instrument->GetErrorMessage());
            d->ReportError(this, msg);
            return false;
        }

        AppEntity::SystemStatus::GetInstance()->SetCurrentLiveImage(d->modality, d->channel);

        liveAcquisition->SetMaxColor(); //TODO ��ٸ���/ä�� �� �÷� ���� ���ͼ� �����ϱ�

        liveAcquisition->Resume();

        auto imagePort = liveAcquisition->GetImagePort();
        instrument->InstallImagePort(imagePort);

        d->output->LiveStarted();

        return true;
    }
}