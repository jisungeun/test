#include <SystemStatus.h>
#include <ChannelConfig.h>
#include "ExperimentUpdater.h"
#include "SetBFEnable.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetBFEnable::Impl {
        IImagingConditionOutputPort* output{ nullptr };
        bool updateExperiment{ true };
        bool isGray{ true };
        bool enable{ false };
    };

    SetBFEnable::SetBFEnable(bool updateExperiment, IImagingConditionOutputPort* output) : IUseCase("SetBFEnable"), d{new Impl} {
        d->output = output;
        d->updateExperiment = updateExperiment;
    }

    SetBFEnable::~SetBFEnable() {
    }

    auto SetBFEnable::SetEnable(const bool isGray, const bool enable) -> void {
        d->isGray = isGray;
        d->enable = enable;
    }

    auto SetBFEnable::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto mode = AppEntity::ImagingMode::_from_integral(AppEntity::ImagingMode::BFGray);

        auto bfConfig = sysStatus->GetChannelConfig(mode);
        if(!bfConfig) return true;

        bfConfig->SetEnable(d->enable);

        auto experiment = sysStatus->GetExperiment();

        if(d->updateExperiment) {
            auto expUpdater = ExperimentUpdater(experiment);
            expUpdater.SetBFChannelConfig(d->isGray, *bfConfig);
        }

        if(d->output) d->output->UpdateEnabledImagingTypes(experiment->GetEnabledImagingTypes());

        //Todo FL 이미징 채널 설정 변경을 적용하여 Timetable 갱신하기

        return true;
    }
};