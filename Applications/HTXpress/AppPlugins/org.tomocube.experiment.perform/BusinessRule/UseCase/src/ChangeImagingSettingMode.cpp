﻿#include "ChangeImagingSettingMode.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ChangeImagingSettingMode::Impl {
        ILiveviewConfigOutputPort* output{nullptr};

        AppEntity::ImagingSettingMode::_enumerated mode{};
    };

    ChangeImagingSettingMode::ChangeImagingSettingMode(ILiveviewConfigOutputPort* output) : IUseCase("ChangeImagingSettingMode"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    ChangeImagingSettingMode::~ChangeImagingSettingMode() {
    }

    auto ChangeImagingSettingMode::SetCurrentImagingSettingMode(const AppEntity::ImagingSettingMode& mode) -> void {
        d->mode = mode;
    }

    auto ChangeImagingSettingMode::Perform() -> bool {
        if(d->output) {
            d->output->UpdateImagingSettingMode(d->mode);
            return true;
        }

        return false;
    }
}
