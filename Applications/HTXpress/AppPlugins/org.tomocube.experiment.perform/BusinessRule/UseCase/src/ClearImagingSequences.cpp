#include <SystemStatus.h>

#include "ClearImagingSequences.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    ClearImagingSequences::ClearImagingSequences() : IUseCase("ClearImagingSequences") {
    }

    ClearImagingSequences::~ClearImagingSequences() {
    }

    auto ClearImagingSequences::Perform() -> bool {
        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        if(!experiment) {
            Error("No experiment exists.");
            return false;
        }

        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        auto scenario = std::make_shared<AppEntity::ImagingScenario>();
        scenario->RemoveAll();

        experiment->SetScenario(sysStatus->GetCurrentVesselIndex(), scenario);

        return true;
    }
}
