﻿#include "SetCustomPreviewArea.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetCustomPreviewArea::Impl {
        IPreviewOutputPort* output {nullptr};
        double xInMM{};
        double yInMM{};
        int32_t widthInUm{};
        int32_t heightInUm{};
    };

    SetCustomPreviewArea::SetCustomPreviewArea(IPreviewOutputPort* output) : IUseCase("SetCustomPreviewArea"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    SetCustomPreviewArea::~SetCustomPreviewArea() = default;

    auto SetCustomPreviewArea::SetPreviewArea(double wellXInMM, double wellYInMM, int32_t widthUm, int32_t heightUm) -> void {
        d->xInMM = wellXInMM;
        d->yInMM = wellYInMM;
        d->widthInUm = widthUm;
        d->heightInUm = heightUm;
    }

    auto SetCustomPreviewArea::Perform() -> bool {
        if(d->output) {
            d->output->UpdateCustomPreviewArea(d->xInMM, d->yInMM, d->widthInUm, d->heightInUm);
            return true;
        }

        return false;
    }
}
