﻿#include "IImagingProfileLoader.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static IImagingProfileLoader* theInstance{};

    auto IImagingProfileLoader::GetInstance() -> IImagingProfileLoader* {
        return theInstance;
    }

    IImagingProfileLoader::IImagingProfileLoader() {
        theInstance = this;
    }
}
