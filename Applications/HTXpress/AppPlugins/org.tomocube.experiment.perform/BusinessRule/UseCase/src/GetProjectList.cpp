#include <QDir>

#include <System.h>
#include <SessionManager.h>

#include "GetProjectList.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct GetProjectList::Impl {
        QStringList list;
    };

    GetProjectList::GetProjectList() : IUseCase("GetProjectList"), d{new Impl} {
    }

    GetProjectList::~GetProjectList() {
    }

    auto GetProjectList::GetList() const -> QStringList {
        return d->list;
    }

    auto GetProjectList::Perform() -> bool {
        const auto& dataDir = AppEntity::System::GetSystemConfig()->GetDataDir();
        const auto& userID = AppEntity::SessionManager::GetInstance()->GetID();

        QDir dir(QString("%1/%2").arg(dataDir).arg(userID));
        d->list = dir.entryList(QDir::Filter::Dirs|QDir::Filter::NoDotAndDotDot, QDir::SortFlag::Name);

        return true;
    }
}