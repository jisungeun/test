#include <SystemStatus.h>
#include "ClearFLChannels.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ClearFLChannels::Impl {
        IImagingConditionOutputPort* output{ nullptr };
    };

    ClearFLChannels::ClearFLChannels(IImagingConditionOutputPort* output) : IUseCase("ClearFLChannels"), d{new Impl} {
        d->output = output;
    }

    ClearFLChannels::~ClearFLChannels() {
    }

    auto ClearFLChannels::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        for(auto mode : AppEntity::ImagingMode::_values()) {
            auto flConfig = sysStatus->GetChannelConfig(mode);
            if(!flConfig) return true;

            flConfig->SetEnable(false);
        }

        return true;
    }
}