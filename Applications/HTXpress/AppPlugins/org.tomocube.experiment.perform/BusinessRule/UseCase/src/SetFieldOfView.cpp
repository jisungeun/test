#include <QDebug>

#include <System.h>
#include <SystemStatus.h>

#include "Utility.h"
#include "ExperimentUpdater.h"
#include "IChannelConfigWriter.h"
#include "SetFieldOfView.h"
#include "IScanTimeCalculator.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetFieldOfView::Impl {
        IImagingConditionOutputPort* output{ nullptr };
        bool updateExperiment{ true };
        double xInUm{ 0 };
        double yInUm{ 0 };
    };

    SetFieldOfView::SetFieldOfView(bool updateExperiment, IImagingConditionOutputPort* output) : IUseCase("SetFieldOfView"), d{new Impl} {
        d->output = output;
        d->updateExperiment = updateExperiment;
    }

    SetFieldOfView::~SetFieldOfView() {
    }

    auto SetFieldOfView::SetFOV(double xInUm, double yInUm) -> void {
        d->xInUm = xInUm;
        d->yInUm = yInUm;
    }

    auto SetFieldOfView::GetAdjustedFOV(double& xInUm, double& yInUm) -> void {
        xInUm = d->xInUm;
        yInUm = d->yInUm;
    }

    auto SetFieldOfView::Perform() -> bool {
        auto xInPixels = um2pixels(d->xInUm);
        auto yInPixels = um2pixels(d->yInUm);
        qDebug() << "(" << d->xInUm << "," << d->yInUm << ")um ==> (" << xInPixels << "," << yInPixels << ")px";

        auto model = AppEntity::System::GetModel();
        const auto stepH = model->CameraFOVStepH();
        const auto stepV = model->CameraFOVStepV();
        if(stepH==0 || stepV==0) {
            qDebug() << "Invalid FOV step setting (h=" << stepH << " v=" << stepV << ")";
            return false;
        }

        const auto fovStep = std::lcm(stepH, stepV);

        xInPixels = (xInPixels / fovStep) * fovStep;
        yInPixels = (yInPixels / fovStep) * fovStep;

        xInPixels = std::min<int32_t>(xInPixels, model->CameraPixelsH());
        yInPixels = std::min<int32_t>(yInPixels, model->CameraPixelsV());

        qDebug() << "(" << xInPixels << "," << yInPixels << ")px Step=(" << fovStep << "," << fovStep <<")";

        d->xInUm = pixels2um(xInPixels);
        d->yInUm = pixels2um(yInPixels);

        qDebug() << "(" << xInPixels << "," << yInPixels << ")px ==> (" << d->xInUm << "," << d->yInUm << ")um";

        auto writer = IChannelConfigWriter::GetInstance();
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto& configs = sysStatus->GetChannelConfigAll();
        for(auto key : configs.keys()) {
            configs[key]->SetCameraFov(xInPixels, yInPixels);
            writer->Write(key, *configs[key]);
        }

        if(d->updateExperiment) {
            auto expUpdater = ExperimentUpdater(sysStatus->GetExperiment());
            expUpdater.SetFieldOfView(d->xInUm, d->yInUm);
        }

        auto timeCalculator = IScanTimeCalculator::GetInstance();
        if(!timeCalculator) {
            Error("No time calculator is configured.");
            return false;
        }
        timeCalculator->SetMaximumFOV(AppEntity::Area::fromUM(d->xInUm, d->yInUm));

        if(d->output) {
            d->output->UpdateFOV(d->xInUm, d->yInUm);
        }

        //Todo FOV 변경으로 이미징 시간 및 타일 획득 조건에 변경이 없는지 체크 후 Timetable 갱신하기

        return true;
    }
}
