#include <SystemStatus.h>

#include "Utility.h"
#include "TimeTableUpdater.h"
#include "ExperimentUpdater.h"
#include "ApplyImagingSequences.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ApplyImagingSequences::Impl {
        struct ModalityItem {
            AppEntity::Modality modality{ AppEntity::Modality::HT };
            bool is3D{ true };
            bool isGray{ true };
            QList<int32_t> channels;
        };

        struct SequenceItem {
            uint32_t startTimestamp{ 0 };
            uint32_t interval{ 0 };
            uint32_t count{ 0 };
            QList<ModalityItem> modalityItems;
        };

        IImagingConditionOutputPort* output{ nullptr };
        QList<SequenceItem> items;
        int32_t index{ -1 };

        auto CreateImagingCondition(const ModalityItem& item, QList<int32_t>& channels)->AppEntity::ImagingCondition::Pointer;
    };

    auto ApplyImagingSequences::Impl::CreateImagingCondition(const ModalityItem& item, QList<int32_t>& channels) -> AppEntity::ImagingCondition::Pointer {
        if (item.modality == +AppEntity::Modality::HT) {
            auto condHT = CreateCondHT(item.is3D);
            if(condHT) return condHT;
        } else if (item.modality == +AppEntity::Modality::FL) {
            auto condFL = CreateCondFL(item.is3D, channels);
            if(condFL) return condFL;
        } else if (item.modality == +AppEntity::Modality::BF) {
            auto condBF = CreateCondBF(item.isGray);
            if(condBF) return condBF;
        }

        return nullptr;
    }

    ApplyImagingSequences::ApplyImagingSequences(IImagingConditionOutputPort* output) : IUseCase("ApplyImagingSequences"), d{new Impl} {
        d->output = output;
    }

    ApplyImagingSequences::~ApplyImagingSequences() {
    }

    auto ApplyImagingSequences::BeginNewSequence() -> void {
        d->index = d->index + 1;
        d->items.push_back(Impl::SequenceItem());
    }

    auto ApplyImagingSequences::AddModality(const AppEntity::Modality& modality, bool is3D, bool isGray, const QList<int32_t>& channels) -> void {
        Impl::ModalityItem modalityItem;
        modalityItem.modality = modality;
        modalityItem.is3D = is3D;
        modalityItem.isGray = isGray;
        modalityItem.channels = channels;
        d->items[d->index].modalityItems.push_back(modalityItem);
    }

    auto ApplyImagingSequences::SetStartTimestamp(const uint32_t seconds) -> void {
        d->items[d->index].startTimestamp = seconds;
    }

    auto ApplyImagingSequences::SetInterval(const uint32_t seconds) -> void {
        d->items[d->index].interval = seconds;
    }

    auto ApplyImagingSequences::SetCount(uint32_t count) -> void {
        d->items[d->index].count = count;
    }

    auto ApplyImagingSequences::Perform() -> bool {
        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        if(!experiment) {
            Error("No experiment exists.");
            return false;
        }

        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        auto scenario = std::make_shared<AppEntity::ImagingScenario>();

        TimeTableUpdater timeTableUpdater;

        auto index = 0;
        for(const auto& item : d->items) {
            auto sequence = std::make_shared<AppEntity::ImagingSequence>();

            for(auto modalityItem : item.modalityItems) {
                auto cond = d->CreateImagingCondition(modalityItem, modalityItem.channels);
                if(!cond) {
                    Error("Invalid imaging condition");
                    return false;
                }

                sequence->AddImagingCondition(cond);
                sequence->SetTimelapseCondition(item.interval, item.count);

                timeTableUpdater.Apply(cond);
            }

            scenario->AddSequence(item.startTimestamp, QString::number(index++), sequence);
        }

        auto expUpdater = ExperimentUpdater(experiment);
        expUpdater.SetImagingScenario(sysStatus->GetCurrentVesselIndex(), scenario);

        if(d->output) {
            timeTableUpdater.Apply(experiment->GetAllLocationsInVessel(sysStatus->GetCurrentVesselIndex()));

            auto timeTable = timeTableUpdater.Update();
            d->output->Update(timeTable);
        }

        return true;
    }
}
