#include "IInstrument.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static IInstrument* theInstance{ nullptr };

    IInstrument::IInstrument() {
        theInstance = this;
    }

    IInstrument::~IInstrument() {
    }

    auto IInstrument::GetInstance() -> IInstrument* {
        return theInstance;
    }
}
