#include <SystemStatus.h>
#include <ChannelConfig.h>
#include <ILiveImageAcquisition.h>
#include <IInstrument.h>

#include "ExperimentUpdater.h"
#include "IChannelConfigWriter.h"
#include "SetLiveImagingConfig.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetLiveImagingConfig::Impl {
        ILiveviewConfigOutputPort* output{ nullptr };
        AppEntity::ImagingMode mode{ AppEntity::ImagingMode::FLCH0 };
        uint32_t intensity{ 0 };
        uint32_t exposure{ 0 };
    };

    SetLiveImagingConfig::SetLiveImagingConfig(ILiveviewConfigOutputPort* output) : IUseCase("SetLiveImagingConfig"), d{ new Impl } {
        d->output = output;
    }

    SetLiveImagingConfig::~SetLiveImagingConfig() {
    }

    auto SetLiveImagingConfig::SetConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain) -> void {
        Q_UNUSED(gain)
        d->mode = mode;
        d->intensity = intensity;
        d->exposure = exposure;
    }

    auto SetLiveImagingConfig::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        auto liveConfig = sysStatus->GetLiveConfig(d->mode);
        liveConfig->SetCameraExposureUSec(d->exposure);
        if (d->mode == +AppEntity::ImagingMode::FLCH0 ||
            d->mode == +AppEntity::ImagingMode::FLCH1 ||
            d->mode == +AppEntity::ImagingMode::FLCH2 ||
            d->mode == +AppEntity::ImagingMode::BFGray ||
            d->mode == +AppEntity::ImagingMode::BFColor) {
            liveConfig->SetLightIntensity(d->intensity);
        } 

        sysStatus->SetLiveConfig(d->mode, *liveConfig);

        // BF의 live config가 변경되는 경우 acquisition config에 바로 적용
        if (d->mode == +AppEntity::ImagingMode::BFGray || d->mode == +AppEntity::ImagingMode::BFColor) {
            auto bfAcqConfig = sysStatus->GetChannelConfig(d->mode);
            bfAcqConfig->SetLightIntensity(d->intensity);
            bfAcqConfig->SetCameraExposureUSec(d->exposure);

            auto experiment = sysStatus->GetExperiment();
            auto expUpdater = ExperimentUpdater(experiment);
            expUpdater.SetBFChannelConfig(
                (d->mode == +AppEntity::ImagingMode::BFGray),
                *bfAcqConfig);
        }

        auto writer = IChannelConfigWriter::GetInstance();
        writer->Write(d->mode, *liveConfig);

        if (d->output) {
            d->output->UpdateLiveConfig(
                liveConfig->GetLightIntensity(),
                liveConfig->GetCameraExposureUSec(),
                0
            );
        }

        return true;
    }
};