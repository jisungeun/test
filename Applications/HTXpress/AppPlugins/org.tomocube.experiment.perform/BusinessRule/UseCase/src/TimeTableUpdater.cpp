#include "IScanTimeCalculator.h"
#include "TimeTableUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct TimeTableUpdater::Impl {
        ImagingTimeTable::Pointer timeTable{ nullptr };
        QList<ImagingCondition::Pointer> conds;
    };

    TimeTableUpdater::TimeTableUpdater() : d{new Impl} {
        d->timeTable = std::make_shared<ImagingTimeTable>();
    }

    TimeTableUpdater::~TimeTableUpdater() {
    }

    auto TimeTableUpdater::Apply(ImagingCondition::Pointer cond) -> void {
        for(const auto item : d->conds) {
            if(!item->CheckModality(cond->GetModality())) continue;
            if(item->Is3D() != cond->Is3D()) continue;
            return;
        }

        d->conds.push_back(cond);
    }

    auto TimeTableUpdater::Apply(const QList<PositionGroup>& groups) -> void {
        IScanTimeCalculator::GetInstance()->CalcInSec(groups, d->timeTable);
    }

    auto TimeTableUpdater::Apply(const QList<Location>& locations) -> void {
        IScanTimeCalculator::GetInstance()->CalcInSec(locations, d->timeTable);
    }

    auto TimeTableUpdater::Update() const -> ImagingTimeTable::Pointer {
        AppEntity::ImagingCondition::Pointer prevCond{ nullptr };
        for(auto cond : d->conds) {
            IScanTimeCalculator::GetInstance()->CalcInSec(cond, d->timeTable, prevCond);
            prevCond = cond;
        }

        return d->timeTable;
    }
}
