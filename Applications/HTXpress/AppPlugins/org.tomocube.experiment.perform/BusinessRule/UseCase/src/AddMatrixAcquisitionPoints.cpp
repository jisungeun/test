#include <System.h>
#include <SystemStatus.h>

#include "Utility.h"
#include "ExperimentUpdater.h"
#include "AddMatrixAcquisitionPoints.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct AddMatrixAcquisitionPoints::Impl {
        IAcquisitionPositionOutputPort* output{ nullptr };

        struct {
            AppEntity::WellIndex wellIdx;
            AppEntity::LocationIndex locationIdx;
            int32_t cols{ 1 };
            int32_t rows{ 1 };
            double horGapMm{ 0 };
            double verGapMm{ 0 };
        } source;

        struct {
            QList<AppEntity::WellIndex> wells;
        } target;
    };

    AddMatrixAcquisitionPoints::AddMatrixAcquisitionPoints(IAcquisitionPositionOutputPort* output)
        : IUseCase("AddMatrixAcquisitionPoints")
        , d{ std::make_unique<Impl>()} {
        d->output = output;
    }

    AddMatrixAcquisitionPoints::~AddMatrixAcquisitionPoints() {
    }

    auto AddMatrixAcquisitionPoints::SetCenter(const AppEntity::WellIndex wellIdx, const AppEntity::LocationIndex locationIdx) -> void {
        d->source.wellIdx = wellIdx;
        d->source.locationIdx = locationIdx;
    }

    auto AddMatrixAcquisitionPoints::SetMatrix(const int32_t cols, const int32_t rows, const double horGapMm, const double verGapMm) -> void {
        d->source.cols = cols;
        d->source.rows = rows;
        d->source.horGapMm = horGapMm;
        d->source.verGapMm = verGapMm;
    }

    auto AddMatrixAcquisitionPoints::SetTargets(const QList<AppEntity::WellIndex>& wells) -> void {
        d->target.wells = wells;
    }

    auto AddMatrixAcquisitionPoints::Perform() -> bool {
        using Location = AppEntity::Location;
        using Position = AppEntity::Position;

        auto status = AppEntity::SystemStatus::GetInstance();
        auto vesselIdx = status->GetCurrentVesselIndex();

        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        if(!experiment) {
            Error("No experiment is loaded");
            return false;
        }

        int32_t cxIdx = std::floor((d->source.cols-1)/2);
        int32_t cyIdx = std::floor((d->source.rows-1)/2);

        const auto centerLoc = experiment->GetLocation(vesselIdx, d->source.wellIdx, d->source.locationIdx);
        const auto zpos = centerLoc.GetCenter().toMM().z;

        QList<std::shared_ptr<AppEntity::Location>> locations;
        for(int32_t rowIdx=0; rowIdx<d->source.rows; rowIdx++) {
            auto ypos = centerLoc.GetCenter().toMM().y + (rowIdx - cyIdx)*d->source.verGapMm;
            for(int32_t colIdx=0; colIdx<d->source.cols; colIdx++) {
                auto xpos = centerLoc.GetCenter().toMM().x + (colIdx - cxIdx)*d->source.horGapMm;

                auto location = std::make_shared<Location>(centerLoc);
                location->SetCenter(Position::fromMM(xpos, ypos, zpos));
                locations.push_back(location);
            }
        }

        auto exUpdater = ExperimentUpdater(experiment);
        for(auto location : locations) {
            for (auto wellIdx : d->target.wells) {
                const auto newIdx = exUpdater.AddLocation(vesselIdx, wellIdx, *location);
                if(d->output) {
                    d->output->AddPosition(newIdx, wellIdx, location);
                }
            }
        }

        exUpdater.DeleteLocation(vesselIdx, d->source.wellIdx, d->source.locationIdx);

        return true;
    }
}