#include <SystemStatus.h>
#include "IChannelConfigWriter.h"
#include "ConfigureImgingCondition.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ConfigureImagingCondition::Impl {
        IImagingConditionOutputPort* output{ nullptr };

        QMap<AppEntity::ImagingMode,AppEntity::ChannelConfig> channelConfigs;
        QMap<AppEntity::ImagingMode, AppEntity::ScanConfig> scanConfigs;
    };

    ConfigureImagingCondition::ConfigureImagingCondition(IImagingConditionOutputPort* output)
        : IUseCase("ConfigureImagingCondition"), d{new Impl} {
        d->output = output;
    }

    ConfigureImagingCondition::~ConfigureImagingCondition() {
    }

    auto ConfigureImagingCondition::SetConfigs(AppEntity::ImagingMode mode, 
                                               const AppEntity::ChannelConfig& channelConfig, 
                                               const AppEntity::ScanConfig& scanConfig) -> void {
        d->channelConfigs[mode] = channelConfig;
        d->scanConfigs[mode] = scanConfig;
    }

    auto ConfigureImagingCondition::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        auto writer = IChannelConfigWriter::GetInstance();

        for(auto mode : d->channelConfigs.keys()) {
            sysStatus->SetChannelConfig(mode, d->channelConfigs[mode]);
            sysStatus->SetScanConfig(mode, d->scanConfigs[mode]);

            writer->Write(mode, d->channelConfigs[mode]);
        }

        //TODO Scenario를 갱신하고 Timetable 갱신하기

        return true;
    }
}
