#include "IInstrument.h"
#include "ISystemConfigWriter.h"
#include "SaveSystemConfiguration.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SaveSystemConfiguration::Impl {
    };

    SaveSystemConfiguration::SaveSystemConfiguration() : IUseCase("SaveSystemConfiguration"), d{new Impl} {
    }

    SaveSystemConfiguration::~SaveSystemConfiguration() {
    }

    auto SaveSystemConfiguration::Perform() -> bool {
        auto* writer = ISystemConfigWriter::GetInstance();
        if(!writer) {
            Error("No valid system configuration writer exists");
            return false;
        }

        if(!writer->Write()) {
            Error("It fails to write system configuration");
            return false;
        }

        auto* instrument = IInstrument::GetInstance();
        if(instrument) {
            instrument->UpdateConfiguration();
        }

        return true;
    }
}