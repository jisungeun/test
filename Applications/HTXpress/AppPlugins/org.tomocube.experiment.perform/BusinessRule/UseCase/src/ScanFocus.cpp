#include "IInstrument.h"
#include "ScanFocus.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ScanFocus::Impl {
        IInstrumentOutputPort* output{ nullptr };
        ScanFocus* p{ nullptr };

        double startPositionMm{ 0 };
        double distanceMm{ 0 };
        int32_t count{ 100 };

        QList<int32_t> values;

        Impl(ScanFocus* p) : p{ p } {}
    };

    ScanFocus::ScanFocus(IInstrumentOutputPort* output) : IUseCase("ScanFocus"), d{std::make_unique<Impl>(this)} {
        d->output = output;
    }

    ScanFocus::~ScanFocus() {
    }

    auto ScanFocus::SetStartPositionInMm(double start) -> void {
        d->startPositionMm = start;
    }

    auto ScanFocus::SetDistanceInMm(double distance) -> void {
        d->distanceMm = distance;
    }

    auto ScanFocus::SetCount(int32_t count) -> void {
        d->count = count;
    }

    auto ScanFocus::GetValues() const -> QList<int32_t> {
        return d->values;
    }

    auto ScanFocus::Perform() -> bool {
        auto* instrument = IInstrument::GetInstance();
        if(!instrument) return false;

        d->values = instrument->ScanFocus(d->startPositionMm, d->distanceMm, d->count);
        return !d->values.isEmpty();
    }
}