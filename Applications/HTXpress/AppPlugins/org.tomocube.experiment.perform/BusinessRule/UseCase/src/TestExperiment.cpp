#include <QCoreApplication>

#include <System.h>
#include <SystemStatus.h>

#include "IImagingSystem.h"
#include "IInstrument.h"
#include "IExperimentRunner.h"
#include "Utility.h"
#include "TestExperiment.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct TestExperiment::Impl {
        IRunExperimentOutputPort* output{ nullptr };

        AppEntity::Experiment::Pointer experiment{ nullptr };
        AppEntity::VesselIndex vesselIdx{ 0 };

        IImagingSystem* imagingSystem{ nullptr };
        IInstrument* instrument{ nullptr };
        QString outPath;

        auto Ready(TestExperiment* p)->bool;
    };

    auto TestExperiment::Impl::Ready(TestExperiment* p) -> bool {
        imagingSystem = IImagingSystem::GetInstance();
        if(!imagingSystem) {
            p->Error("No imaging system exists");
            return false;
        }

        instrument = IInstrument::GetInstance();
        if(!instrument) {
            p->Error("No instrument exists");
            return false;
        }

        if(!instrument->IsInitialized()) {
            p->Error("Instrument is not initialized");
            return false;
        }

        const auto sysStatus = AppEntity::SystemStatus::GetInstance();

        experiment = sysStatus->GetExperiment();
        if(!experiment) {
            p->Error("No experiment is loaded");
            return false;
        }

        vesselIdx = sysStatus->GetCurrentVesselIndex();
        auto scenario = experiment->GetScenario(vesselIdx);
        if(!scenario) {
            p->Error(QString("No imaging scenario exists for vessel #%1").arg(vesselIdx));
            return false;
        }

        if(experiment->GetAllLocations(vesselIdx).isEmpty()) {
            p->Print("There is no images on the experiment, so it will run the experiment on the current position");
            experiment = std::make_shared<AppEntity::Experiment>(*experiment);

            const auto wellIdx = sysStatus->GetCurrentWell();
            if(wellIdx == -1) {
                p->Error("Current well is not valid");
                return false;
            }

            const auto pos = instrument->GetAxisPosition();
            const auto posInWell = global2well(pos, wellIdx);

            auto location = AppEntity::Location(posInWell, experiment->GetFOV(), false);
            experiment->AddLocation(vesselIdx, wellIdx, location);
        }

        auto imagingTypes = scenario->GetImagingTypes();
        if(imagingTypes.isEmpty()) {
            p->Error("No imaging modality is added to timelapse imaging plan");
            return false;
        }

        outPath = sysStatus->GetExperimentOutputPath();
        if(outPath.isEmpty()) {
            p->Error("No data path is not specified");
            return false;
        }

        auto newExperiment = std::make_shared<AppEntity::Experiment>(*experiment);
        auto newScenario = newExperiment->GetScenario(vesselIdx);
        newScenario->RemoveAll();

        auto newSequence = [&]()->AppEntity::ImagingSequence::Pointer {
            auto newSequence = std::make_shared<AppEntity::ImagingSequence>();

            const auto seqCount = scenario->GetCount();
            for(auto seqIdx=0; seqIdx<seqCount; seqIdx++) {
                auto sequence = scenario->GetSequence(seqIdx);
                auto modalityCount = sequence->GetModalityCount();
                for(auto idx=0; idx<modalityCount; idx++) {
                    auto imgCond = sequence->GetImagingCondition(idx);
                    if(!imagingTypes.contains(imgCond->GetImagingType())) continue;

                    newSequence->AddImagingCondition(imgCond);
                    imagingTypes.remove(imgCond->GetImagingType());
                    if(imagingTypes.isEmpty()) return newSequence;
                }
            }

            return newSequence;
        }();

        newScenario->AddSequence(0, "TestRun", newSequence);

        experiment = newExperiment;

        return true;
    }

    TestExperiment::TestExperiment(IRunExperimentOutputPort* output) : IUseCase("TestExperiment"), d{new Impl} {
        d->output = output;
    }

    TestExperiment::~TestExperiment() {
    }

    auto TestExperiment::Perform() -> bool {
        if(!d->Ready(this)) {
            Error("System is not ready to run experiment");
            return false;
        }

        const auto sysStatus = AppEntity::SystemStatus::GetInstance();

        auto* runner = IExperimentRunner::GetInstance();
        if(!runner) {
            Error("No experiment runner exists");
            return false;
        }

        runner->SetExperiment(sysStatus->GetProjectTitle(), d->experiment);
        runner->SetVesselIndex(d->vesselIdx);
        runner->SetOverlapInUM(AppEntity::System::GetTileScanOverlap());
        runner->SetAcquisitionType(AcquisitionType::Timelapse);
        runner->SetMultiDishHolder(d->experiment->GetVessel()->IsMultiDish());

        if(!runner->Run()) {
            Error("Failed to run the experiment runner");
            return false;
        }

        return true;
    }
}