﻿#include "SetCustomPreviewAreaEditableStatus.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
	struct SetCustomPreviewAreaEditableStatus::Impl {
		IPreviewOutputPort* output{nullptr};
	
		bool flag{};
	};
	
	SetCustomPreviewAreaEditableStatus::SetCustomPreviewAreaEditableStatus(IPreviewOutputPort* output) 
		: IUseCase("SetCustomPreviewAreaEditableStatus"), d{std::make_unique<Impl>()} {
		d->output = output;
	}
	
	SetCustomPreviewAreaEditableStatus::~SetCustomPreviewAreaEditableStatus() = default;
	
	auto SetCustomPreviewAreaEditableStatus::DoAreaSetting() -> void {
		d->flag = true;
	}
	
	auto SetCustomPreviewAreaEditableStatus::CancelAreaSetting() -> void {
		d->flag = false;
	}
	
	auto SetCustomPreviewAreaEditableStatus::Perform() -> bool {
		if(d->output) {
			if(d->flag) {d->output->DoCustomPreviewAreaSetting();}
			else {d->output->CancelCustomPreviewAreaSetting();}

		    return true;
		}
	
		return false;
	}
}