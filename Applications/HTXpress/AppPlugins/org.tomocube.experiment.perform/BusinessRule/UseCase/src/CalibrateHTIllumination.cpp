#include <QThread>
#include <QCoreApplication>
#include <QVector>
#include <QRgb>

#include <System.h>
#include <SystemStatus.h>

#include "ExperimentUpdater.h"
#include "IHTIlluminationAutoCalibration.h"
#include "IInstrument.h"
#include "IChannelConfigWriter.h"
#include "CalibrateHTIllumination.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static QVector<QRgb> colormap;

    struct CalibrateHTIllumination::Impl {
        CalibrateHTIllumination* p{ nullptr };
        IInstrumentOutputPort* output{ nullptr };


        Impl(CalibrateHTIllumination* p, IInstrumentOutputPort* output) : p{ p }, output{ output } {
            if(colormap.isEmpty()) {
                colormap.resize(256);
                for (int i = 0; i < 256; i++) {
                    colormap[i] = qRgb(i,i,i);
                }
            }
        }

        auto ReportError(const QString& message);
        auto UpdateProgress(double progress, const QString& message = QString())->void;
        auto MoveZAxis(double pos, int32_t timeoutSec = 120)->bool;
    };

    auto CalibrateHTIllumination::Impl::ReportError(const QString& message) {
        p->Error(message);
        if(output) output->UpdateFailed(message);
    }

    auto CalibrateHTIllumination::Impl::UpdateProgress(double progress, const QString& message) -> void {
        if(output) output->UpdateProgress(progress, message);
    }

    auto CalibrateHTIllumination::Impl::MoveZAxis(double pos, int32_t timeoutSec) -> bool {
        auto instrument = IInstrument::GetInstance();
        instrument->MoveAxis(AppEntity::Axis::Z, pos);

        int32_t checkCount = 0;

        auto motionStatus = instrument->CheckAxisMotion();
        while(motionStatus.moving) {
            QThread::msleep(100);
            motionStatus = instrument->CheckAxisMotion();
            checkCount++;

            if(checkCount >= (timeoutSec*10)) return false;
            if(motionStatus.error) return false;
        }

        return true;
    }

    CalibrateHTIllumination::CalibrateHTIllumination(IInstrumentOutputPort* output)
        : IUseCase("CalibrateHTIllumination")
        , d{new Impl(this, output)} {
    }

    CalibrateHTIllumination::~CalibrateHTIllumination() {
    }

    auto CalibrateHTIllumination::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();
        auto calibrator = IHTIlluminationAutoCalibration::GetInstance();
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        const auto NA = sysStatus->GetNA();

        d->UpdateProgress(0, "Calibrating HT illumination");

        sysStatus->SetHTIlluminationCalibrated(false);

        auto [intensityStart, intensityStep, threshold] = sysConfig->GetIlluminationCalibrationParameter(NA);
        intensityStep = 0 - std::abs(intensityStep);    //it should be negative number...
        Print(QString("NA=%1 Start=%2 Step=%3 Threshold=%4").arg(NA).arg(intensityStart).arg(intensityStep).arg(threshold));
        auto [htStep, htSlices] = sysConfig->GetHTScanParameter(NA);

        instrument->StopLive();
        calibrator->EnableLogging(IsServiceEngineer());

        const auto startZ = instrument->GetAxisPositionMM(AppEntity::Axis::Z);

        auto calibrate = [&](double zPosition,
                             const QList<int32_t>& intensityList,
                             int32_t threshold,
                             int32_t imagesPerSet,
                             int32_t imagesPerSlice,
                             int32_t offset,
                             double progressStart,
                             double progressEnd
                             )->std::tuple<bool,int32_t, std::shared_ptr<AppEntity::RawImage>> {
            if(!d->MoveZAxis(zPosition)) {
                d->ReportError(QString("It fails to move Z axis to %1").arg(zPosition));
                return std::make_tuple(false, 0, nullptr);
            }

            calibrator->SetImages(intensityList.size(), imagesPerSet);
            calibrator->SetOffset(imagesPerSlice, offset);
            calibrator->SetThreshold(threshold);
            calibrator->StartCalibrate();

            auto imagePort = calibrator->GetImagePort();
            const auto started = instrument->PerformHTIlluminationCalibration(imagePort, intensityList);
            if(!started) {
                d->ReportError(QString("It fails to run HT illumination calibration"));
                instrument->FinishHTIlluminationCalibration(imagePort);
                return std::make_tuple(false, 0, nullptr);
            }

            int32_t waitCount{ 0 };
            do {
                auto [error, progress] = instrument->CheckHTIlluminationCalibrationProgress();
                if(progress == 1.0) break;
                if(error) {
                    d->ReportError(QString("HT Illumination auto calibration is failed"));
                    instrument->FinishHTIlluminationCalibration(imagePort);
                    return std::make_tuple(false, 0, nullptr);
                }

                d->UpdateProgress(progressStart + (progressEnd-progressStart)*progress);
                QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

                QThread::msleep(100);
                waitCount++;

                if(waitCount > 1200) {
                    d->ReportError(QString("It fails to get all images"));
                    instrument->FinishHTIlluminationCalibration(imagePort);
                    return std::make_tuple(false, 0, nullptr);
                }
            } while(true);

            instrument->FinishHTIlluminationCalibration(imagePort);

            auto [index, image] = calibrator->GetResult();
            if(index < 0) {
                return std::make_tuple(true, -1, image);
            }

            Print(QString("Intensity = %1").arg(intensityList.at(index)));

            return std::make_tuple(true, intensityList.at(index), image);
        };

        bool calibrated{ false };
        auto calibratedIntensity{ 0 };
        std::shared_ptr<AppEntity::RawImage> resultImage;

        auto totalCount = intensityStart / std::abs(intensityStep);
        auto triedCount = 0.f;

        auto intensityIndex{ 0 };
        do {
            if(AppEntity::System::GetSystemConfig()->GetSimulation()) {
                calibrated = true;
                break;
            }

            QList<int32_t> intensityList;
            for(int32_t idx=0; idx<1; idx++, intensityIndex++) {
                const auto intensity = intensityStart + (intensityStep * intensityIndex);
                if(intensity <= 0) break;
                intensityList.push_back(intensity);
            }

            if(intensityList.empty()) return false;

            auto [result, intensity, image] = calibrate(startZ,
                                                 intensityList,
                                                 threshold,
                                                 htSlices*4,
                                                 4,                 //4 images per slice
                                                 2,                 //it is the most brightest one.
                                                 triedCount/totalCount,
                                                 (triedCount+intensityList.size())/totalCount);
            triedCount = triedCount + intensityList.size();
            if(result == false) {
                instrument->ResumeLive();
                d->ReportError(QString("It fails to calibrate HT illumination"));
                return false;
            }

            if(intensity == -1) continue;

            calibrated = true;
            calibratedIntensity = intensity;
            resultImage = image;
            break;
        }while(true);

        d->MoveZAxis(startZ, 120);

        instrument->ResumeLive();

        if(calibrated) {
            Print(QString("New HT Illumination Intensity = %1 [NA=%2]").arg(calibratedIntensity).arg(NA));
            sysConfig->SetHTScanParameter(NA, htStep, htSlices);

            auto exUpdater = ExperimentUpdater(sysStatus->GetExperiment());
            exUpdater.SetHTIntensity(calibratedIntensity);

            for(auto mode : {AppEntity::ImagingMode::HT3D, AppEntity::ImagingMode::HT2D}) {
                auto htConfig = sysStatus->GetChannelConfig(mode);
                htConfig->SetLightIntensity(calibratedIntensity);
                sysStatus->SetChannelConfig(mode, *htConfig);
                sysStatus->SetLiveConfig(mode, *htConfig);

                auto writer = IChannelConfigWriter::GetInstance();
                writer->Write(mode, *htConfig);
            }

            if(resultImage && d->output) {
                QImage qImage(resultImage->GetBuffer().get(), 
                      resultImage->GetSizeX(), 
                      resultImage->GetSizeY(), 
                      QImage::Format_Indexed8);
                qImage.setColorTable(colormap);
                d->output->UpdateHTIlluminationResult(qImage.copy());
            }
        }

        d->UpdateProgress(1.0);

        sysStatus->SetHTIlluminationCalibrated(calibrated);

        return calibrated;
    }
}