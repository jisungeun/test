﻿#include <IInstrument.h>
#include <ILiveImageAcquisition.h>

#include "ChangeLiveModality.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ChangeLiveModality::Impl {
        IInstrumentOutputPort* output{ InstrumentNullOutputPort::GetInstance() };
        AppEntity::Modality modality{ AppEntity::Modality::HT };
        int32_t channel{ 0 };

        auto ReportError(ChangeLiveModality* p, const QString& message)->void;
    };

    auto ChangeLiveModality::Impl::ReportError(ChangeLiveModality* p, const QString& message) -> void {
        p->Error(message);
        output->LiveImagingFailed(message);
    }

    ChangeLiveModality::ChangeLiveModality(IInstrumentOutputPort* output) : IUseCase("ChangeLiveModality"), d{new Impl} {
        d->output = output;
    }

    ChangeLiveModality::~ChangeLiveModality() {
    }

    auto ChangeLiveModality::SetImageType(AppEntity::Modality modality, int32_t channel) -> void {
        d->modality = modality;
        d->channel = channel;
    }

    auto ChangeLiveModality::Perform() -> bool {
        auto* instrument = IInstrument::GetInstance();
        if(!instrument) {
            d->ReportError(this, "No instrument exists");
            return false;
        }

        auto* liveAcquisition = ILiveImageAcquisition::GetInstance();
        if(!liveAcquisition) {
            d->ReportError(this, "Live acquisition is not available");
            return false;
        }

        liveAcquisition->Pause();

        if(!instrument->ChangeLiveModality(d->modality, d->channel)) {
            const auto msg = QString("It fails to change live imaging modality - %1").arg(instrument->GetErrorMessage());
            d->ReportError(this, msg);
            return false;
        }

        liveAcquisition->SetMaxColor();  //TODO ��ٸ���/ä�� �� �÷� ���� ���ͼ� �����ϱ�

        liveAcquisition->Resume();

        return true;
    }
}
