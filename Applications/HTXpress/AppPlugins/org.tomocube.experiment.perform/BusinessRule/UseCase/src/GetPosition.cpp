#include <SystemStatus.h>

#include "IInstrument.h"
#include "Utility.h"
#include "GetPosition.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct GetPosition::Impl {
        IMotionOutputPort* output{ nullptr };
        AppEntity::WellIndex wellIndex{ 0 };
        AppEntity::Position position;
    };

    GetPosition::GetPosition(IMotionOutputPort* outputPort) : IUseCase("GetPosition"), d{new Impl} {
        d->output = outputPort;
    }

    GetPosition::~GetPosition() {
    }

    auto GetPosition::SetWell(const AppEntity::WellIndex& wellIdx) -> void {
        d->wellIndex = wellIdx;
    }

    auto GetPosition::CurrentPosition() const -> AppEntity::Position {
        return d->position;
    }

    auto GetPosition::Perform() -> bool {
        auto* instrument = IInstrument::GetInstance();
        if(!instrument) {
            Error("Instrument is not available");
            return false;
        }

        auto globalPos = instrument->GetAxisPosition();
        AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(globalPos);

        d->position = global2well(globalPos, d->wellIndex);

        if(d->output) d->output->UpdatePosition(d->wellIndex, d->position); 

        return true;
    }
}
