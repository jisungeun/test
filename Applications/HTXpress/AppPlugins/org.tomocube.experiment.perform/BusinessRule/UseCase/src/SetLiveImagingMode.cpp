#include <SystemStatus.h>
#include <ChannelConfig.h>

#include "IChannelConfigWriter.h"
#include "SetLiveImagingMode.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetLiveImagingMode::Impl {
        ILiveviewConfigOutputPort* output{ nullptr };
        AppEntity::ImagingMode mode{ AppEntity::ImagingMode::FLCH0 };
    };

    SetLiveImagingMode::SetLiveImagingMode(ILiveviewConfigOutputPort* output) : IUseCase("SetLiveImagingMode"), d{ new Impl } {
        d->output = output;
    }

    SetLiveImagingMode::~SetLiveImagingMode() {
    }

    auto SetLiveImagingMode::SetMode(const AppEntity::ImagingMode mode) -> void {
        d->mode = mode;
    }

    auto SetLiveImagingMode::Perform() -> bool {
        // LiverViewerControl에서 mode 변경시 live view 이미지 획득 명령을 재실행 하므로,
        // 여기서는 live config 값만 변경한다.

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        sysStatus->SetLiveConfig(d->mode, *sysStatus->GetChannelConfig(d->mode));

        auto liveConfig = sysStatus->GetLiveConfig(d->mode);

        if (d->output) {
            d->output->UpdateLiveConfig(
                liveConfig->GetLightIntensity(),
                liveConfig->GetCameraExposureUSec(),
                0
            );
        }

        return true;
    }
};