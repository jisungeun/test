#include <QCoreApplication>

#include <System.h>
#include <SystemStatus.h>

#include "IInstrument.h"
#include "IChannelConfigReader.h"
#include "IScanTimeCalculator.h"
#include "StartInitialize.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct StartInitialize::Impl {
        IInstrumentOutputPort* output{ InstrumentNullOutputPort::GetInstance() };

        auto ReportError(StartInitialize* p, const QString& message)->void;

        auto InitHTScanConfig()->void;
        auto InitBFScanConfig()->void;
    };

    auto StartInitialize::Impl::ReportError(StartInitialize* p, const QString& message) -> void {
        p->Error(message);
        output->UpdateFailed(message);
    }

    auto StartInitialize::Impl::InitHTScanConfig() -> void {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        AppEntity::ScanConfig ht2dConfig;
        ht2dConfig.SetRange(0, 0, 0);
        ht2dConfig.SetSteps(1);
        ht2dConfig.SetFocusUm(0);
        sysStatus->SetScanConfig(AppEntity::ImagingMode::HT2D, ht2dConfig);
    }

    auto StartInitialize::Impl::InitBFScanConfig() -> void {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        AppEntity::ScanConfig bfGrayConfig;
        bfGrayConfig.SetSteps(1);
        sysStatus->SetScanConfig(AppEntity::ImagingMode::BFGray, bfGrayConfig);

        AppEntity::ScanConfig bfColorConfig;
        bfColorConfig.SetSteps(1);
        sysStatus->SetScanConfig(AppEntity::ImagingMode::BFColor, bfColorConfig);
    }

    StartInitialize::StartInitialize(IInstrumentOutputPort* output) : IUseCase("StartInitialize"), d{new Impl} {
        d->output = output;
    }

    StartInitialize::~StartInitialize() {
    }

    auto StartInitialize::Perform() -> bool {
        auto status = AppEntity::SystemStatus::GetInstance();
        auto sysConfig = AppEntity::System::GetSystemConfig();

        auto instrument = IInstrument::GetInstance();
        if(!instrument) {
            Error("No instrument instance exists");
            return false;
        }

        if(instrument->IsInitialized()) {
            Print("Instrument is already initialized");
        } else {
            d->output->UpdateProgress(0, "Instrument Initialization");

            if(!instrument->StartInitialize()) {
                const auto msg = QString("Initialization can't be started - %1").arg(instrument->GetErrorMessage());
                d->ReportError(this, msg);
                return false;
            }

            double progress = 0;
            while(progress < 1.0) {
                QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);
                auto resp = instrument->GetInitializationProgress();
                progress = std::get<1>(resp);
                d->output->UpdateProgress(progress * 0.9);
                if(std::get<0>(resp) == false) {
                    const auto msg = QString("Initialization is failed - %1").arg(instrument->GetErrorMessage());
                    d->ReportError(this, msg);
                    return false;
                }
            }

            d->InitHTScanConfig();
            d->InitBFScanConfig();

            auto defaultZFocus = [=]()->double {
                auto scanReady = sysConfig->GetAutofocusReadyPos();
                auto afOffset= status->GetExperiment()->GetVessel()->GetAFOffset()/1000.0;
                return scanReady + afOffset;
            }();

            status->SetCurrentFocusTarget(sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::DefaultTargetValue));
            status->SetFocusZPosition(defaultZFocus);
            status->SetCurrentVesselIndex(0);
            status->SetCurrentWell(0);

            //In case, Z axis is not safe position
            const auto curPos = instrument->GetAxisPosition();
            if(std::abs(curPos.toMM().z - status->GetFocusZPosition()) < 1.0) {
                instrument->EnableAutoFocus();
            }
        }

        auto cPos = instrument->GetAxisPositionMM(AppEntity::Axis::C);

        const auto curPos = instrument->GetAxisPosition();
        cPos = instrument->GetAxisPositionMM(AppEntity::Axis::C);
        status->SetCurrentGlobalPosition(curPos);
        status->SetCurrentCondensorPosition(cPos);
        Print(QString("Current position (X,Y,Z,C)=(%1,%2,%3,%4)").arg(curPos.toMM().x).arg(curPos.toMM().y)
                                                                 .arg(curPos.toMM().z).arg(cPos));

        auto timeCalculator = IScanTimeCalculator::GetInstance();
        if(!timeCalculator) {
            Error("No time calculator is configured.");
            return false;
        }

        const auto roi = status->GetExperiment()->GetFOV();
        timeCalculator->SetMaximumFOV(roi);
        timeCalculator->SetOverlapInUM(AppEntity::System::GetTileScanOverlap());

        status->SetSampleStageEncoderSupported(instrument->SampleStageEncoderSupported());

        d->output->UpdateProgress(1.0);

        return true;
    }
}
