#include <System.h>
#include <SessionManager.h>
#include <SystemStatus.h>

#include "LoadExperiment.h"
#include "IExperimentReader.h"
#include "IChannelConfigReader.h"
#include "IScanTimeCalculator.h"
#include "IChannelConfigWriter.h"
#include "ScanTimeCalculatorUpdater.h"
#include "IImagingProfileLoader.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct LoadExperiment::Impl {
        LoadExperiment* p{ nullptr };
        IExperimentOutputPort* output{ nullptr };
        QString projectTitle;
        QString expOutputPath;
        QString path;

        explicit Impl(LoadExperiment* p) : p{p} {}
        auto GetHtScanParameter() const -> std::optional<std::tuple<int32_t, int32_t>>;
        auto ReportError(const QString& message)->void;
    };

    auto LoadExperiment::Impl::GetHtScanParameter() const -> std::optional<std::tuple<int32_t, int32_t>> {
        using Step = int32_t;
        using Slices = int32_t;

        const auto profileLoader = IImagingProfileLoader::GetInstance();
        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto sysConfig = AppEntity::System::GetSystemConfig();

        if (profileLoader) {
            if (const auto scanParameterInExperiment = profileLoader->GetHtScanParameter()) {
                if (scanParameterInExperiment.has_value()) {
                    const auto scanParameters = scanParameterInExperiment.value();
                    p->Print(QString("HT Scan Parameters from experiment [Step:%1, Slices:%2]").arg(std::get<0>(scanParameters)).arg(std::get<1>(scanParameters)));
                    return scanParameters;
                }
            }
        }

        return std::nullopt;
    }

    auto LoadExperiment::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
        if(output) output->Error(message);
    }

    LoadExperiment::LoadExperiment(IExperimentOutputPort* output) : IUseCase("LoadExperiment"), d{new Impl(this)} {
        d->output = output;
    }

    LoadExperiment::~LoadExperiment() {
    }

    auto LoadExperiment::SetExperiment(const QString& projectTitle, const QString& experimentTitle) -> void {
		const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
		const auto dataPath = systemConfig->GetDataDir();
		const auto user = AppEntity::SessionManager::GetInstance()->GetID();
        d->projectTitle = projectTitle;
        d->expOutputPath = dataPath + "/" + user + "/" + projectTitle + "/" + experimentTitle;
        d->path = d->expOutputPath + "/" + experimentTitle + "." + AppEntity::ExperimentExtension;
    }

    auto LoadExperiment::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto LoadExperiment::Perform() -> bool {
        auto* reader = IExperimentReader::GetInstance();
        if(!reader) {
            d->ReportError("No experiment reader exists");
            return false;
        }

        if(d->path.isEmpty()) {
             d->ReportError("Experiment file path is not specified");
            return false;
        }

        auto experiment = reader->Read(d->path);
        if(!experiment) {
             d->ReportError(QString("It fails read an experiment from %1").arg(d->path));
            return false;
        }

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        sysStatus->SetProjectTitle(d->projectTitle);
        sysStatus->SetExperiment(experiment, d->path);
        sysStatus->SetExperimentOutputPath(d->expOutputPath);

        const auto vesselModel = experiment->GetVessel()->GetModel();
        const auto NA = experiment->GetVessel()->GetNA();
        Print(QString("Vessel Model=%1 NA=%2").arg(vesselModel).arg(NA));

        auto sysConfig = AppEntity::System::GetSystemConfig();

        const auto htScanParam = d->GetHtScanParameter();
        if(!htScanParam.has_value()) {
            d->ReportError(QString("No valid profile in %1").arg(experiment->GetTitle()));
            return false;
        }

        const auto htScanStep = std::get<0>(*htScanParam);
        const auto htScanSlices = std::get<1>(*htScanParam);
        const auto halfPulses = (htScanStep * htScanSlices) / 2;
        Print(QString("HT Scan Parameter. Step=%1 Slices=%2 [S:%3~ E:%4]")
              .arg(htScanStep)
              .arg(htScanSlices)
              .arg(-halfPulses)
              .arg(halfPulses));

        AppEntity::ScanConfig ht3DConfig;
        ht3DConfig.SetRange(-halfPulses, halfPulses, htScanStep);
        ht3DConfig.SetSteps(htScanSlices);
        sysStatus->SetScanConfig(AppEntity::ImagingMode::HT3D, ht3DConfig);

        if(!sysConfig->GetCondenserAFParameterNAs().contains(NA)) {
            d->ReportError(QString("No valid condenser AF parameters for NA %1").arg(NA));
            return false;
        }

        Print(QString("Experiment is loaded from %1").arg(d->path));

        Print("Load channel configurations from file");
        auto channelConfigReader = IChannelConfigReader::GetInstance();
        for(auto mode : AppEntity::ImagingMode::_values()) {
           AppEntity::ChannelConfig config;
            if(!channelConfigReader->Read(mode, config)) {
                if((mode == +AppEntity::ImagingMode::HT2D) || (mode == +AppEntity::ImagingMode::HT3D)) {
                    auto model = AppEntity::System::GetModel();

                    config.SetEnable(false);
                    config.SetCameraExposureUSec(model->HTTriggerExposureUSec());
                    config.SetCameraIntervalUSec(model->HTTriggerExposureUSec() + model->HTTriggerReadOutUSec());
                    config.SetCameraInternal(true);
                    config.SetLightChannel(model->HTLightChannel());
                    config.SetLightInternal(true);
                    config.SetFilterChannel(model->HTFilterChannel());
                    config.SetChannelName("HT");
                    config.SetCameraFov(model->CameraPixelsH(), model->CameraPixelsV());
                }
            }

            if((mode == +AppEntity::ImagingMode::BFGray) || (mode == +AppEntity::ImagingMode::BFColor)) {
                config.SetLightIntensity(sysConfig->GetBFLightIntensity());
            }

            sysStatus->SetChannelConfig(mode, config);
            sysStatus->SetLiveConfig(mode, config);
        }

        const auto timeCalculator = IScanTimeCalculator::GetInstance();
        if(!timeCalculator) {
            Error("No time calculator is configured.");
            return false;
        }
        timeCalculator->SetMaximumFOV(experiment->GetFOV());
        timeCalculator->SetUsingMultidishHolder(experiment->GetVessel()->IsMultiDish());

        ScanTimeCalculatorUpdater::SetExperiment(experiment);

        if(d->output) d->output->Update(experiment, false);

        return true;
    }
}
