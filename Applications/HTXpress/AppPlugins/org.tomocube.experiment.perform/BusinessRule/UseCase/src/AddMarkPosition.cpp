﻿#include <SystemStatus.h>
#include <Position.h>

#include "AddMarkPosition.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct AddMarkPosition::Impl {
        IMarkPositionOutputPort* output{nullptr};

        AppEntity::WellIndex wellIndex{};
        AppEntity::Position wellPos{};
        AppEntity::Area area{};
        bool isTile{};
    };
    AddMarkPosition::AddMarkPosition(IMarkPositionOutputPort* output) : IUseCase("AddMarkPosition"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    AddMarkPosition::~AddMarkPosition() = default;

    auto AddMarkPosition::SetCurrentArea(double xInMM, double yInMM, const AppEntity::Area& area, bool isTile) -> void {
        auto status = AppEntity::SystemStatus::GetInstance();
        d->wellIndex = status->GetCurrentWell();
        d->wellPos = AppEntity::Position::fromMM(xInMM, yInMM, status->GetCurrentGlobalPosition().toMM().z);
        d->area = area;
        d->isTile = isTile;
    }

    auto AddMarkPosition::Perform() -> bool {
        const auto location = std::make_shared<AppEntity::Location>();
        location->SetCenter(d->wellPos);
        location->SetArea(d->area, d->isTile);

        if(d->output) {
            d->output->AddMarkPosition(d->wellIndex, location);
            return true;
        }

        return false;
    }
}
