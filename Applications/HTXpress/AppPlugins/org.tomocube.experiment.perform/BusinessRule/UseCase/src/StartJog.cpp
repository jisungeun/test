#include <QElapsedTimer>

#include <SystemStatus.h>

#include "Utility.h"
#include "IInstrument.h"
#include "StartJog.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct StartJog::Impl {
        StartJog* p{ nullptr };
        IMotionOutputPort* output{ nullptr };
        AppEntity::Axis axis{AppEntity::Axis::X};
        bool plusDirection{ true };

        Impl(StartJog* p) : p{p} {}
        auto ReportError(const QString& message)->void;
    };

    auto StartJog::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
        if(output) output->ReportError(message);
    }

    StartJog::StartJog(IMotionOutputPort* output) : IUseCase("StartJog"), d{new Impl(this)} {
        d->output = output;
    }

    StartJog::~StartJog() {
    }

    auto StartJog::Set(AppEntity::Axis axis, bool plusDirection) -> void {
        d->axis = axis;
        d->plusDirection = plusDirection;
    }

    auto StartJog::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();
        if(!instrument->IsInitialized()) return false;

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto wellIdx = sysStatus->GetCurrentWell();
        const auto controlAF = sysStatus->GetAutoFocusEnabled() && (d->axis != +AppEntity::Axis::Z);

        if(controlAF) {
            if(!instrument->DisableAutoFocus()) {
                d->ReportError(tr("It fails to disable auto focus"));
                return false;
            }
        }

        if(!instrument->StartJogAxis(d->axis, d->plusDirection)) {
            d->ReportError(tr("It fails to start jog motion : %1").arg(d->axis._to_string()));
            return false;
        }

        QElapsedTimer timer;
        timer.start();

        auto motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < 60000) && motionStatus.moving) {
            if(motionStatus.error) {
                d->ReportError(tr("It fails to move %1 stage : %2").arg(d->axis._to_string()).arg(motionStatus.message));
                return false;
            }

            const auto curPos = instrument->GetAxisPosition();
            sysStatus->SetCurrentGlobalPosition(curPos);

            if(d->output) {
                auto posInWell = global2well(curPos, wellIdx);
                d->output->UpdatePosition(wellIdx, posInWell);
            }

            QCoreApplication::processEvents(QEventLoop::AllEvents, 300);

            motionStatus = instrument->CheckAxisMotion();
        }

        if(controlAF) {
            const auto [enabled, success] = instrument->EnableAutoFocus();
            if(!enabled) {
                d->ReportError(tr("It fails to enable auto focus"));
                return false;
            }
        }

        if(d->output) {
            const auto curPos = instrument->GetAxisPosition();
            sysStatus->SetCurrentGlobalPosition(curPos);

            auto posInWell = global2well(curPos, wellIdx);
            d->output->UpdatePosition(wellIdx, posInWell);
        }

        return true;
    }
}