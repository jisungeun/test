#include "ExperimentStatus.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static auto _id1 = qRegisterMetaType<UseCase::ExperimentStatus>("HTXpress::AppPlugins::Experiment::Perform::UseCase::ExperimentStatus_1");
    static auto _id3 = qRegisterMetaType<ExperimentStatus>("HTXpress::AppPlugins::Experiment::Perform::UseCase::ExperimentStatus_2");

    struct ExperimentStatus::Impl {
        double progress{ 0 };
        int32_t remainSeconds{ 0 };
        int32_t elapsedSeconds{ 0 };
        bool stopped{ false };
        bool error{ false };
        QString message;
        QString errorMessage;
        int32_t currentSequence{ 0 };
        AppEntity::WellIndex wellIndex;
        AppEntity::Position positionInWell;
    };

    ExperimentStatus::ExperimentStatus() : d{new Impl} {
    }

    ExperimentStatus::ExperimentStatus(const ExperimentStatus& other) : d{new Impl} {
        *d = *other.d;
    }

    ExperimentStatus::~ExperimentStatus() {
    }

    auto ExperimentStatus::operator=(const ExperimentStatus& other) -> ExperimentStatus& {
        *d = *other.d;
        return *this;
    }

    auto ExperimentStatus::IsCompleted() const -> bool {
        return d->progress==1.0;
    }

    auto ExperimentStatus::SetProgress(double progress) -> void {
        d->progress = progress;
    }

    auto ExperimentStatus::GetProgress() const -> double {
        return d->progress;
    }

    auto ExperimentStatus::SetRemainTime(int32_t seconds) -> void {
        d->remainSeconds = seconds;
    }

    auto ExperimentStatus::GetRemainTime() const -> int32_t {
        return d->remainSeconds;
    }

    auto ExperimentStatus::SetElapsedTime(int32_t seconds) -> void {
        d->elapsedSeconds = seconds;
    }

    auto ExperimentStatus::GetElapsedTime() const -> int32_t {
        return d->elapsedSeconds;
    }

    auto ExperimentStatus::SetStopped() -> void {
        d->stopped = true;
    }

    auto ExperimentStatus::IsStopped() const -> bool {
        return d->stopped;
    }

    auto ExperimentStatus::SetStatusMessage(const QString& message) -> void {
        d->message = message;
    }

    auto ExperimentStatus::GetStatusMessage() const -> QString {
        return d->message;
    }

    auto ExperimentStatus::SetError(const QString& message) -> void {
        d->error = true;
        d->errorMessage = message;
    }

    auto ExperimentStatus::IsError() const -> bool {
        return d->error;
    }

    auto ExperimentStatus::GetErrorMessage() const -> QString {
        return d->errorMessage;
    }

    auto ExperimentStatus::SetRunningSequence(int32_t seqIdx) -> void {
        d->currentSequence = seqIdx;
    }

    auto ExperimentStatus::GetRunningSequence() const -> int32_t {
        return d->currentSequence;
    }

    auto ExperimentStatus::SetCurrentPosition(const AppEntity::WellIndex& wellIdx, 
                                              const AppEntity::Position& position) -> void {
        d->wellIndex = wellIdx;
        d->positionInWell = position;
    }

    auto ExperimentStatus::GetCurrentPosition() const -> std::tuple<AppEntity::WellIndex, AppEntity::Position> {
        return std::make_tuple(d->wellIndex, d->positionInWell);
    }
}
