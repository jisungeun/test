#include "IInstrument.h"
#include "ApplySystemConfiguration.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ApplySystemConfiguration::Impl {
    };

    ApplySystemConfiguration::ApplySystemConfiguration() : IUseCase("ApplySystemConfiguration"), d{new Impl} {
    }

    ApplySystemConfiguration::~ApplySystemConfiguration() {
    }

    auto ApplySystemConfiguration::Perform() -> bool {
        auto* instrument = IInstrument::GetInstance();
        if(instrument) {
            instrument->UpdateConfiguration();
        }

        return true;
    }
}