#include "ICondenserAutofocus.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static ICondenserAutofocus* theInstance{ nullptr };

    ICondenserAutofocus::ICondenserAutofocus() {
        theInstance = this;
    }

    ICondenserAutofocus::~ICondenserAutofocus() {
    }

    auto ICondenserAutofocus::GetInstance() -> ICondenserAutofocus* {
        return theInstance;
    }
}
