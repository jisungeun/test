#include <SystemStatus.h>
#include <MoveSampleStage.h>
#include <IInstrument.h>

#include "Utility.h"
#include "SelectWell.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SelectWell::Impl {
        AppEntity::WellIndex wellIdx{ -1 };
        IMotionOutputPort* output{ nullptr };
    };

    SelectWell::SelectWell(IMotionOutputPort* output) : IUseCase("SelectWell"), d{new Impl} {
        d->output = output;
    }

    SelectWell::~SelectWell() {
    }

    auto SelectWell::SetWell(AppEntity::WellIndex wellIdx) -> void {
        d->wellIdx = wellIdx;
    }

    auto SelectWell::Perform() -> bool {
        using Axis = AppEntity::Axis;

        Print(QString("The selected well index is %1").arg(d->wellIdx));

        auto status = AppEntity::SystemStatus::GetInstance();
        status->SetCurrentWell(d->wellIdx);

        if(d->output) {
            d->output->UpdateSelectedWell(d->wellIdx);
        }

        auto* instrument = IInstrument::GetInstance();
        instrument->EnableJoystick();

        const auto vessel = status->GetExperiment()->GetVessel();
        const auto centerX = vessel->GetImagingAreaPositionX();
        const auto centerY = vessel->GetImagingAreaPositionY();
        const auto sizeX = vessel->GetImagingAreaSizeX();
        const auto sizeY = vessel->GetImagingAreaSizeY();

        auto minCorner = well2global(AppEntity::Position::fromMM(centerX-sizeX/2, centerY-sizeY/2, 0));
        auto maxCorner = well2global(AppEntity::Position::fromMM(centerX+sizeX/2, centerY+sizeY/2, 0));

        instrument->SetJogRange(Axis::X, minCorner.toMM().x, maxCorner.toMM().x);
        instrument->SetJogRange(Axis::Y, minCorner.toMM().y, maxCorner.toMM().y);

        auto model = AppEntity::System::GetModel();
        instrument->SetJogRange(Axis::Z, model->AxisLowerLimitMM(Axis::Z), model->AxisUpperLimitMM(Axis::Z));

        return true;
    }
}
