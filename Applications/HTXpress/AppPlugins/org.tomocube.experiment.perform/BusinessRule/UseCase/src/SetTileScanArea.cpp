#include "SetTileScanArea.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct SetTileScanArea::Impl {
        IImagingConditionOutputPort* output{ nullptr };
        bool enable{ false };
        AppEntity::WellIndex wellIndex{ -1 };
        double xInMm {0};
        double yInMm {0};
        double widthInUm {0};
        double heightInUm {0};
    };

    SetTileScanArea::SetTileScanArea(IImagingConditionOutputPort* output) : IUseCase("SetTileScanArea"), d{new Impl} {
        d->output = output;
    }

    SetTileScanArea::~SetTileScanArea() {
    }

    auto SetTileScanArea::SetArea(bool enable, AppEntity::WellIndex wellIndex,
                                  double xInMm, double yInMm, 
                                  double widthInUm, double heightInUm) -> void {
        d->enable = enable;
        d->wellIndex = wellIndex;
        d->xInMm = xInMm;
        d->yInMm = yInMm;
        d->widthInUm = widthInUm;
        d->heightInUm = heightInUm;
    }

    auto SetTileScanArea::Perform() -> bool {
        //Todo May need to adjust tile imaging area if the center is near to boundary of imaging area
        if(d->output) {
            d->output->UpdateTileScanArea(d->enable, d->wellIndex, d->xInMm, d->yInMm, d->widthInUm, d->heightInUm);
        }

        return true;
    }
}
