#include "IPreviewCalibrator.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static IPreviewCalibrator* theInstance{ nullptr };
    
    IPreviewCalibrator::IPreviewCalibrator() {
        theInstance = this;
    }
    
    IPreviewCalibrator::~IPreviewCalibrator() {
    }
    
    auto IPreviewCalibrator::GetInstance() -> IPreviewCalibrator* {
        return theInstance;
    }
}