#include "IBGGenerator.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    static IBGGenerator* theInstance{ nullptr };

    IBGGenerator::IBGGenerator() {
        theInstance = this;
    }

    IBGGenerator::~IBGGenerator() {
    }

    auto IBGGenerator::GetInstance() -> IBGGenerator* {
        return theInstance;
    }
}