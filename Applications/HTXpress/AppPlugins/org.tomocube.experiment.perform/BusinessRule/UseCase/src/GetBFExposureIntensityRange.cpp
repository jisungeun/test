#include <QCoreApplication>
#include <QThread>

#include <SystemStatus.h>

#include "IInstrument.h"
#include "IPreviewCalibrator.h"
#include "GetBFExposureIntensityRange.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct GetBFExposureIntensityRange::Impl {
        GetBFExposureIntensityRange* p{ nullptr };
        IInstrumentOutputPort* output{ nullptr };

        ValueList values;
        double coeffP{ 0 };
        double coeffQ{ 0 };

        Impl(GetBFExposureIntensityRange* p, IInstrumentOutputPort* output) : p{ p }, output{ output } {
        }

        auto ReportError(const QString& message);
        auto UpdateProgress(double progress, const QString& message = QString())->void;
    };

    auto GetBFExposureIntensityRange::Impl::ReportError(const QString& message) {
        p->Error(message);
        if (output) output->UpdateFailed(message);
    }

    auto GetBFExposureIntensityRange::Impl::UpdateProgress(double progress, const QString& message) -> void {
        if (output) {
            output->UpdateProgress(progress, message);
            QCoreApplication::processEvents(QEventLoop::AllEvents, 50);
        }
    }

    GetBFExposureIntensityRange::GetBFExposureIntensityRange(IInstrumentOutputPort* output) 
        : IUseCase("GetBFExposureIntensityRange")
        , d{ new Impl(this, output) } {
    }

    GetBFExposureIntensityRange::~GetBFExposureIntensityRange() {
    }

    auto GetBFExposureIntensityRange::GetValues() const -> ValueList {
        return d->values;
    }

    auto GetBFExposureIntensityRange::GetCoefficients() const -> std::tuple<double, double> {
        return std::make_tuple(d->coeffP, d->coeffQ);
    }

    auto GetBFExposureIntensityRange::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();
        auto calibrator = IPreviewCalibrator::GetInstance();
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        d->values.clear();
        d->coeffP = 0;
        d->coeffQ = 0;

        d->UpdateProgress(0, "Calculating BF exposure range");

        //store the original channel configuration
        const auto original = *sysStatus->GetLiveConfig(AppEntity::ImagingMode::BFGray).get();

        auto imagePort = calibrator->GetImagePort();
        instrument->InstallImagePort(imagePort);

        auto exposureTime = 3;   //3msec
        while (1) {
            instrument->StopLive();
            auto chConfig = original;
            chConfig.SetCameraExposureUSec(exposureTime * 1000);
            sysStatus->SetLiveConfig(AppEntity::ImagingMode::BFGray, chConfig);
            instrument->StartLive(AppEntity::Modality::BF);
            QThread::msleep(100);
            imagePort->Clear();

            const auto intensity = calibrator->GetIntensity();
            if (intensity == -1) {
                d->ReportError(QString("It fails to get image to get its intensity for exposure time %1 msec").arg(exposureTime));
                break;
            }

            d->values.push_back({ exposureTime, intensity });

            d->UpdateProgress(intensity / 255.0);
            if (intensity >= 200.0) break;

            exposureTime+=2;
        }

        instrument->UninstallImagePort(imagePort);
        imagePort->Clear();

        instrument->StopLive();
        //resotre the original channel configuration
        sysStatus->SetLiveConfig(AppEntity::ImagingMode::BFGray, original);
        instrument->StartLive(AppEntity::Modality::BF);

        std::tie(d->coeffP, d->coeffQ) = calibrator->CalcExposureCoefficients(d->values);

        d->UpdateProgress(1.0);


        return true;
    }
}