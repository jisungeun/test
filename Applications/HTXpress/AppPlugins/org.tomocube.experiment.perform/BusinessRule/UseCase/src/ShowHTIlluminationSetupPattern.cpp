#include <System.h>

#include "IInstrument.h"
#include "ShowHTIlluminationSetupPattern.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    struct ShowHTIlluminationSetupPattern::Impl {
        ShowHTIlluminationSetupPattern* p{ nullptr };
        IInstrumentOutputPort* output{ nullptr };
        double NA;
        int32_t intensity;

        Impl(ShowHTIlluminationSetupPattern* p) : p{ p } {}
        auto ReportError(const QString& message)->void;
    };

    auto ShowHTIlluminationSetupPattern::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
        if(output) output->UpdateFailed(message);
    }

    ShowHTIlluminationSetupPattern::ShowHTIlluminationSetupPattern(IInstrumentOutputPort* output)
        : IUseCase("ShowHTIlluminationSetupPattern")
        , d{ std::make_unique<Impl>(this) } {
        d->output = output;
    }

    ShowHTIlluminationSetupPattern::~ShowHTIlluminationSetupPattern() {
    }

    auto ShowHTIlluminationSetupPattern::SetNA(double NA) -> void {
        d->NA = NA;
    }

    auto ShowHTIlluminationSetupPattern::SetIntensity(int32_t intensity) -> void {
        d->intensity = intensity;
    }

    auto ShowHTIlluminationSetupPattern::Perform() -> bool {
        auto model = AppEntity::System::GetModel();
        auto patternIndex = model->GetHTIlluminationSetupPattern(d->NA);
        if(patternIndex < 0) {
            d->ReportError(tr("No illumination pattern exists for NA : %1").arg(d->NA));
            return false;
        }

        auto instrument = IInstrument::GetInstance();
        return instrument->ShowHTIlluminationSetupPattern(patternIndex, d->intensity);
    }
}