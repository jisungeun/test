﻿#include <System.h>

#include "GetMinRequiredSpace.h"
#include "ISystemStorageManager.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    GetMinRequiredSpace::GetMinRequiredSpace() : IUseCase("GetMinRequiredSpace") {
    }

    GetMinRequiredSpace::~GetMinRequiredSpace() {
    }

    auto GetMinRequiredSpace::Perform() -> bool {
        const auto minSpaceGB = AppEntity::System::GetSystemConfig()->GetMinRequiredSpace();
        const auto manager = ISystemStorageManager::GetInstance();

        if(minSpaceGB < 0) {
            return false;
        }

        manager->UpdateMinRequiredSpace(minSpaceGB);

        return true;
    }
}
