#pragma once

#include <memory>

#include <IDataOutputPort.h>

#include "IUseCase.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ScanAcquisitionData : public IUseCase {
    public:
        ScanAcquisitionData(IDataOutputPort* outputPort = nullptr);
        ~ScanAcquisitionData();

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
