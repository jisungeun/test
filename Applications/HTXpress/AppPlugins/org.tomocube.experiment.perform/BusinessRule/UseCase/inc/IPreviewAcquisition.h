#pragma once
#include <memory>
#include <QString>
#include <QList>

#include <PositionGroup.h>
#include <Area.h>
#include <PreviewUnitMove.h>
#include <ImagingScenario.h>
#include <Location.h>

#include "IImagePort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IPreviewAcquisition {
    public:
        using Pointer = std::shared_ptr<IPreviewAcquisition>;
        using WellIndex = AppEntity::WellIndex;

    protected:
        IPreviewAcquisition();

    public:
        virtual ~IPreviewAcquisition();
        static auto GetInstance()->IPreviewAcquisition*;

        virtual auto GetImagePort() const->IImagePort::Pointer = 0;

        virtual auto SetMaximumFOV(AppEntity::Area area)->void = 0;
        virtual auto SetOverlapInUM(uint32_t overlap)->void = 0;
        virtual auto SetSingleMoveLimitUm(uint32_t distance)->void = 0;
        virtual auto SetArea(AppEntity::Position globalPos, AppEntity::Area area, AppEntity::Position wellPos)->void = 0;
        virtual auto StartAcquisition() const->std::tuple<int32_t, int32_t, QList<Entity::PreviewUnitMove>> = 0;
        virtual auto StopAcquisition()->void = 0;

        virtual auto IsFinished() const->bool = 0;
        virtual auto GetPreviewTile(int32_t& xPos, int32_t& yPos, QImage& image) const->bool = 0;
    };
}