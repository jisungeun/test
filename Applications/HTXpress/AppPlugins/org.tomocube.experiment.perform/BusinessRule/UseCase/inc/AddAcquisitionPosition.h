#pragma once

#include <Position.h>
#include <Area.h>

#include "IUseCase.h"
#include "IAcquisitionPositionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API AddAcquisitionPosition : public IUseCase {
    public:
        AddAcquisitionPosition(IAcquisitionPositionOutputPort* output = nullptr);
        ~AddAcquisitionPosition() override;

        auto SetCurrentPosition()->void;
        auto SetCurrentArea(const AppEntity::Area& area, bool isTile)->void;
        auto SetCurrentTileArea(double xInMm, double yInMm, const AppEntity::Area& area, bool isTile = true)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}