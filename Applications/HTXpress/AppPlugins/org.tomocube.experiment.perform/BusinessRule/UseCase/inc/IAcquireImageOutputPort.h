#pragma once
#include <memory>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IAcquireImageOutputPort {
    public:
        IAcquireImageOutputPort();
        virtual ~IAcquireImageOutputPort();

        virtual auto UpdateProgress(double progress)->void = 0;
    };
}