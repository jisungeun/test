#pragma once
#include <memory>

#include <ChannelConfig.h>
#include <ImagingConfig.h>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IChannelConfigWriter {
    public:
        using Pointer = std::shared_ptr<IChannelConfigWriter>;
        using ImagingMode = AppEntity::ImagingMode;
        using ChannelConfig =AppEntity::ChannelConfig;

    protected:
        IChannelConfigWriter();

    public:
        virtual ~IChannelConfigWriter();

        static auto GetInstance()->IChannelConfigWriter*;

        virtual auto Write(ImagingMode mode, const ChannelConfig& config)->bool = 0;
    };
}