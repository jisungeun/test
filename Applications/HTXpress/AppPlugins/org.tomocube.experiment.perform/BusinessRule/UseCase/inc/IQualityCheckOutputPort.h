#pragma once
#include <memory>

#include <AppEntityDefines.h>
#include <Position.h>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IQualityCheckOutputPort {
    public:
        IQualityCheckOutputPort();
        virtual ~IQualityCheckOutputPort();

        virtual auto UpdateEvaluationProgress(double progress)->void = 0;
    };
}