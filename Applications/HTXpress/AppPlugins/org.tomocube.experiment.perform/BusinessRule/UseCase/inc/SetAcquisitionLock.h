#pragma once
#include <memory>

#include "IUseCase.h"
#include "IImagingConditionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SetAcquisitionLock : public IUseCase {
    public:
        SetAcquisitionLock(IImagingConditionOutputPort* output = nullptr);
        ~SetAcquisitionLock() override;

        auto SetLock(bool locked)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}