﻿#pragma once

#include "IUseCase.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API GetMinRequiredSpace : public IUseCase {
    public:
        GetMinRequiredSpace();
        ~GetMinRequiredSpace() override;

    protected:
        auto Perform() -> bool override;
    };
}
