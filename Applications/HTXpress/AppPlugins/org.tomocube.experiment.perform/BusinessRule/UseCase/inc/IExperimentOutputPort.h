#pragma once
#include <memory>

#include <Experiment.h>
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IExperimentOutputPort {
    public:
        IExperimentOutputPort();
        virtual ~IExperimentOutputPort();

        virtual auto Update(AppEntity::Experiment::Pointer experiment, bool reloaded = false)->void = 0;
        virtual auto Error(const QString& message)->void = 0;
    };
}