#pragma once

#include <AppEntityDefines.h>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IVesselOutputPort {
    public:
        IVesselOutputPort();
        virtual ~IVesselOutputPort();

        virtual auto UpdateSelected(AppEntity::WellIndex wellIdx)->void = 0;
        virtual auto UpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM)->void = 0;
    };
}