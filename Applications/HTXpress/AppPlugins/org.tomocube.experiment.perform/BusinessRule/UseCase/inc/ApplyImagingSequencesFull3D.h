﻿#pragma once

#include <memory>
#include <QList>

#include <AppEntityDefines.h>
#include "IUseCase.h"
#include "IImagingConditionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ApplyImagingSequencesFull3D : public IUseCase {
    public:
        explicit ApplyImagingSequencesFull3D(IImagingConditionOutputPort* output);
        ~ApplyImagingSequencesFull3D() override;

        auto BeginNewSequence() -> void;
        auto AddModality(const AppEntity::Modality& modality, bool is3D, bool isGray = true, const QList<int32_t>& channels = {0}) -> void;
        auto SetStartTimestamp(uint32_t seconds) -> void;
        auto SetInterval(uint32_t seconds) -> void;
        auto SetCount(uint32_t count) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
