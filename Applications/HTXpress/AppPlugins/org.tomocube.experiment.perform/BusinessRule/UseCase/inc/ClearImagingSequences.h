#pragma once
#include <memory>
#include <QList>

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ClearImagingSequences : public IUseCase {
    public:
        ClearImagingSequences();
        ~ClearImagingSequences() override;

    protected:
        auto Perform() -> bool override;
    };
}