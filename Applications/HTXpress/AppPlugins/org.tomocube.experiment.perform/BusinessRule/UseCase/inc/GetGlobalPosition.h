#pragma once
#include <memory>

#include "IUseCase.h"
#include "IMotionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API GetGlobalPosition : public IUseCase {
    public:
        GetGlobalPosition(IMotionOutputPort* outputPort);
        ~GetGlobalPosition();

        auto SetSpecificAxis(AppEntity::Axis axis)->void;

        auto CurrentPosition() const->AppEntity::Position;
        auto CurrentPosition(AppEntity::Axis axis)->double;
        auto CurrentWell() const->AppEntity::WellIndex;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
