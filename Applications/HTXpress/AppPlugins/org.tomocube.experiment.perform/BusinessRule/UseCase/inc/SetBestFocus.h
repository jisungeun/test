#pragma once
#include <memory>

#include "IUseCase.h"
#include "IInstrumentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SetBestFocus : public IUseCase {
    public:
        SetBestFocus(IInstrumentOutputPort* output = nullptr);
        ~SetBestFocus() override;

        auto SetCurrent()->void;
        auto SetTarget(double value)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}