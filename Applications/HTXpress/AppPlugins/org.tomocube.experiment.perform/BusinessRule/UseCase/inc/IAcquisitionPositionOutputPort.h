#pragma once
#include <memory>
#include <QMap>

#include <AppEntityDefines.h>
#include <Location.h>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IAcquisitionPositionOutputPort {
    public:
        using LocationIndex = AppEntity::LocationIndex;
        using Location = AppEntity::Location;
        using WellIndex = AppEntity::WellIndex;

    public:
        IAcquisitionPositionOutputPort();
        virtual ~IAcquisitionPositionOutputPort();

        virtual auto AddPosition(LocationIndex locationIdx, WellIndex wellIndex, Location::Pointer loc)->void = 0;
        virtual auto RefreshList(WellIndex wellIndex, QMap<LocationIndex, Location::Pointer>& locList)->void = 0;
        virtual auto DeletePosition(WellIndex wellIdx, LocationIndex locationIdx)->void = 0;
    };
}