﻿#pragma once
#include <memory>

#include "IUseCase.h"
#include "IImagingConditionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API GetAcquisitionDataRequiredSpace final : public IUseCase {
    public:
        explicit GetAcquisitionDataRequiredSpace(IImagingConditionOutputPort* output);
        ~GetAcquisitionDataRequiredSpace() override;

        auto SetAcquisitionPoints(const int32_t& points) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
