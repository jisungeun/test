﻿#pragma once
#include <memory>

#include "IUseCase.h"
#include "IVesselOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SetSafeMovingRange : public IUseCase {
    public:
        explicit SetSafeMovingRange(IVesselOutputPort* output = nullptr);
        ~SetSafeMovingRange() override;

        auto SetSafeRange(AppEntity::Axis axis, double minMM, double maxMM) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
