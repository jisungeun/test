#pragma once
#include <memory>

#include <Area.h>

#include "IUseCase.h"
#include "IPreviewOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API CapturePreview : public IUseCase {
    public:
        CapturePreview(IPreviewOutputPort* output, std::function<bool()> stopFunc = {});
        ~CapturePreview() override;

        auto SetArea(AppEntity::Area area)->void;

    protected:
        auto Perform()->bool override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}