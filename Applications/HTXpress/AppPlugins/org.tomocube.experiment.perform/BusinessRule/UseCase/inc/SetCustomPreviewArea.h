﻿#pragma once

#include <memory>

#include "IUseCase.h"
#include "IPreviewOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SetCustomPreviewArea : public IUseCase{
    public:
        explicit SetCustomPreviewArea(IPreviewOutputPort* output = nullptr);
        ~SetCustomPreviewArea() override;
        
        auto SetPreviewArea(double wellXInMM, double wellYInMM, int32_t widthUm, int32_t heightUm) -> void;

    protected:
       auto Perform()->bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}