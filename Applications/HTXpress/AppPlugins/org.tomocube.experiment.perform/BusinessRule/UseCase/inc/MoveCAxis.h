#pragma once
#include <memory>
#include <QCoreApplication>

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "IMotionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API MoveCAxis : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(MoveCAxis)

    public:
        MoveCAxis(IMotionOutputPort* output = nullptr);
        ~MoveCAxis();

        auto SetTarget(double posInMm)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}