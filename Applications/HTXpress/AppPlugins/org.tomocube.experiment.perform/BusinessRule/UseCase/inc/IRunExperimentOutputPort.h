#pragma once
#include <memory>

#include "ExperimentStatus.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IRunExperimentOutputPort {
    public:
        IRunExperimentOutputPort();
        virtual ~IRunExperimentOutputPort();

        virtual auto UpdateProgress(const ExperimentStatus& status)->void = 0;
        virtual auto NotifyStopped()->void = 0;
    };
}