#pragma once
#include <memory>
#include <QList>

#include "IImagePort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IPreviewCalibrator {
    public:
        using Pair = QPair<int32_t, double>;
        
    protected:
        IPreviewCalibrator();
        
    public:
        virtual ~IPreviewCalibrator();
        static auto GetInstance()->IPreviewCalibrator*;
        
        virtual auto GetImagePort() const->IImagePort::Pointer = 0;

        virtual auto GetIntensity() const->double = 0;
        virtual auto CalcExposureCoefficients(const QList<Pair>& values)->std::tuple<double,double> = 0;
        virtual auto CalcGainCoefficients(const QList<Pair>& values)->std::tuple<double,double> = 0;
    };
}