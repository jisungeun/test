#pragma once
#include <QString>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IUseCaseLogger {
    public:
        IUseCaseLogger();
        virtual ~IUseCaseLogger();

        static auto PrintLog(const QString& useCase, const QString& message)->void;
        static auto PrintError(const QString& useCase, const QString& message)->void;

    protected:
        virtual auto Log(const QString& useCase, const QString& message)->void = 0;
        virtual auto Error(const QString& useCase, const QString& message)->void = 0;
    };
}