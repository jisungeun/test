#pragma once
#include <memory>
#include <QMap>

#include <ImagingConfig.h>

#include "IUseCase.h"
#include "IImagingConditionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ConfigureImagingCondition : public IUseCase {
    public:
        ConfigureImagingCondition(IImagingConditionOutputPort* output);
        ~ConfigureImagingCondition();

        auto SetConfigs(AppEntity::ImagingMode mode, 
                        const AppEntity::ChannelConfig& channelConfig, 
                        const AppEntity::ScanConfig& scanConfig)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
