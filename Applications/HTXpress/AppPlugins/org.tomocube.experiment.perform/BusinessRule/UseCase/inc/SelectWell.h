#pragma once
#include <memory>

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "IMotionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SelectWell : public IUseCase {
    public:
        SelectWell(IMotionOutputPort* output = nullptr);
        ~SelectWell();

        auto SetWell(AppEntity::WellIndex wellIdx)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}