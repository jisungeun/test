#pragma once
#include <memory>

#include <ChannelConfig.h>

#include "IUseCase.h"
#include "IImagingConditionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SetFLChannel : public IUseCase {
    public:
        using ChannelConfig = AppEntity::ChannelConfig;

    public:
        SetFLChannel(bool updateExperiment, IImagingConditionOutputPort* output = nullptr);
        ~SetFLChannel() override;

        auto SetChannel(const int32_t channel, const ChannelConfig& config)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}