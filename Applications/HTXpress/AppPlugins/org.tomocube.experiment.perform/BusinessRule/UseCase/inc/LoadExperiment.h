#pragma once
#include <memory>
#include <QString>

#include "IUseCase.h"
#include "IExperimentOutputPort.h"
#include "IImagingProfileLoader.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API LoadExperiment : public IUseCase {
    public:
        LoadExperiment(IExperimentOutputPort* output = nullptr);
        ~LoadExperiment() override;

        auto SetImagingProfileLoader(IImagingProfileLoader* profileLoader) -> void;
        auto SetExperiment(const QString& projectTitle, const QString& experimentTitle)->void;
        auto SetPath(const QString& path)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}