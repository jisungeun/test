#pragma once
#include <memory>

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "IAcquisitionPositionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API CopyAcquisitionPositions : public IUseCase {
    public:
        CopyAcquisitionPositions(IAcquisitionPositionOutputPort* output = nullptr);
        ~CopyAcquisitionPositions() override;

        auto SetSources(const AppEntity::WellIndex wellIdx, const QList<AppEntity::LocationIndex>& locations)->void;
        auto SetTargetWells(const QList<AppEntity::WellIndex>& wells)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}