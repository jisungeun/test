#pragma once
#include <memory>

#include "IUseCase.h"
#include "IImagingConditionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SetTileActivation final : public IUseCase {
    public:
        SetTileActivation(IImagingConditionOutputPort* output = nullptr);
        ~SetTileActivation() override;

        auto SetEnable(bool enable) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
