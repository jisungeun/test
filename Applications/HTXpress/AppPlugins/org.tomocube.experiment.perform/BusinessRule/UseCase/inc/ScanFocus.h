#pragma once
#include <memory>
#include <QCoreApplication>

#include "IUseCase.h"
#include "IInstrumentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ScanFocus : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(ScanFocus)

    public:
        ScanFocus(IInstrumentOutputPort* output = nullptr);
        ~ScanFocus();

        auto SetStartPositionInMm(double start)->void;
        auto SetDistanceInMm(double distance)->void;
        auto SetCount(int32_t count)->void;

        auto GetValues() const->QList<int32_t>;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}