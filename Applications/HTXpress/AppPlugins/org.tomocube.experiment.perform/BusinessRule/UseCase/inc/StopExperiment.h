#pragma once
#include <memory>

#include "IUseCase.h"
#include "IRunExperimentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API StopExperiment : public IUseCase {
    public:
        StopExperiment(IRunExperimentOutputPort* output = nullptr);
        ~StopExperiment();

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}