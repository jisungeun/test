#pragma once
#include <memory>

#include "IUseCase.h"
#include "ILiveviewConfigOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SetLiveImagingMode : public IUseCase {
    public:
        SetLiveImagingMode(ILiveviewConfigOutputPort* output = nullptr);
        ~SetLiveImagingMode() override;

        auto SetMode(const AppEntity::ImagingMode mode)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}