#pragma once

#include <ImagingConfig.h>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ILiveviewConfigOutputPort {
    public:
        ILiveviewConfigOutputPort();
        virtual ~ILiveviewConfigOutputPort();

        virtual auto UpdateLiveConfig(const uint32_t intensity, const uint32_t exposure, const double gain)->void = 0;
        virtual auto UpdateAcquisitionConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain)->void = 0;
        virtual auto UpdateImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode) -> void = 0;
    };
}