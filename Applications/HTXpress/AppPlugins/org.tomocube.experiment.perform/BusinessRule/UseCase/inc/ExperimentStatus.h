#pragma once
#include <memory>
#include <QMetaObject>

#include <Position.h>
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ExperimentStatus {
    public:
        ExperimentStatus();
        ExperimentStatus(const ExperimentStatus& other);
        ~ExperimentStatus();

        auto operator=(const ExperimentStatus& other)->ExperimentStatus&;

        auto IsCompleted() const->bool;

        auto SetProgress(double progress)->void;
        auto GetProgress() const->double;

        auto SetRemainTime(int32_t seconds)->void;
        auto GetRemainTime() const->int32_t;

        auto SetElapsedTime(int32_t seconds)->void;
        auto GetElapsedTime() const->int32_t;

        auto SetStopped()->void;
        auto IsStopped() const->bool;

        auto SetStatusMessage(const QString& message)->void;
        auto GetStatusMessage() const->QString;

        auto SetError(const QString& message)->void;
        auto IsError() const->bool;
        auto GetErrorMessage() const->QString;

        auto SetRunningSequence(int32_t seqIdx)->void;
        auto GetRunningSequence() const->int32_t;

        auto SetCurrentPosition(const AppEntity::WellIndex& wellIdx, const AppEntity::Position& position)->void;
        auto GetCurrentPosition() const->std::tuple<AppEntity::WellIndex, AppEntity::Position>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

Q_DECLARE_METATYPE(HTXpress::AppPlugins::Experiment::Perform::UseCase::ExperimentStatus)