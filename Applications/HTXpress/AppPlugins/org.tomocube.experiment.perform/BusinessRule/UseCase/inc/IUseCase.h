#pragma once
#include <memory>
#include <QString>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IUseCase {
    public:
        IUseCase(const QString& title);
        virtual ~IUseCase();

        auto Request()->bool;

    protected:
        auto Print(const QString& message)->void;
        auto Error(const QString& error)->void;

        auto IsServiceEngineer() const->bool;
        
        virtual auto Perform()->bool = 0;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}