#pragma once
#include <memory>
#include <QCoreApplication>

#include "IUseCase.h"
#include "IInstrumentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API LoadVessel : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(LoadVessel)

    public:
        LoadVessel(IInstrumentOutputPort* output = nullptr);
        ~LoadVessel() override;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}