﻿#pragma once

#include <QString>

#include "HTX_Experiment_Perform_UseCaseExport.h"
#include "ISystemStorageOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ISystemStorageManager {
    public:
        virtual ~ISystemStorageManager();

        static auto GetInstance() -> ISystemStorageManager*;

        virtual auto RegistOutputPort(UseCase::ISystemStorageOutputPort::Pointer output) -> void = 0;
        virtual auto DeregistOutputPort(UseCase::ISystemStorageOutputPort::Pointer output) -> void = 0;

        virtual auto SetPath(const QString& path) -> void = 0;
        virtual auto SetUpdateInterval(const int32_t& msec) -> void = 0;

        virtual auto UpdateMinRequiredSpace(const int32_t& gb) -> void = 0;
        virtual auto UpdateStorageInfo() -> void = 0;

    protected:
        ISystemStorageManager();
    };
}
