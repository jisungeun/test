﻿#pragma once
#include <memory>

#include <ISystemStorageOutputPort.h>

#include "IUseCase.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API InstallStorageMonitor final : public IUseCase {
    public:
        explicit InstallStorageMonitor(ISystemStorageOutputPort::Pointer output);
        ~InstallStorageMonitor() override;

    private:
        auto Perform() -> bool override;
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
