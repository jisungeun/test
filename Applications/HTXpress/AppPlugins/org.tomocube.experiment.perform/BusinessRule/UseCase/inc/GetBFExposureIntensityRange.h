#pragma once
#include <memory>
#include <QList>

#include "IInstrumentOutputPort.h"
#include "IUseCase.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API GetBFExposureIntensityRange : public IUseCase {
    public:
        using ValueList = QList<QPair<int32_t, double>>;

    public:
        GetBFExposureIntensityRange(IInstrumentOutputPort* output = nullptr);
        ~GetBFExposureIntensityRange();

        auto GetValues() const->ValueList;
        auto GetCoefficients() const->std::tuple<double, double>;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}