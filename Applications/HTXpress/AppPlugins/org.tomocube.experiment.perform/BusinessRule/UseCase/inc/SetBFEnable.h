#pragma once
#include <memory>

#include "IUseCase.h"
#include "IImagingConditionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SetBFEnable : public IUseCase {
    public:
        SetBFEnable(bool updateExperiment, IImagingConditionOutputPort* output = nullptr);
        ~SetBFEnable() override;

        auto SetEnable(const bool isGray, const bool enable)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}