#pragma once
#include <memory>

#include <ChannelConfig.h>

#include "IUseCase.h"
#include "ILiveviewConfigOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ApplyLiveConfigToAcqusitionConfig : public IUseCase {
    public:
        ApplyLiveConfigToAcqusitionConfig(ILiveviewConfigOutputPort* output = nullptr);
        ~ApplyLiveConfigToAcqusitionConfig() override;

        auto ApplyConfig(const AppEntity::ImagingMode mode)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}