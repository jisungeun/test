#pragma once
#include <memory>

#include "IUseCase.h"
#include "IRunExperimentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API AcquireImage : public IUseCase {
    public:
        AcquireImage(IRunExperimentOutputPort* output);
        ~AcquireImage() override;

        auto AddModalityHT(bool is3D)->void;
        auto AddModalityFL(bool is3D, const QList<int32_t>& channels)->void;
        auto AddModalityBF(bool isGray)->void;

        auto SetCenter(double xInMm, double yInMm)->void;
        auto SetROI(double roiXInUm, double roiYInUm)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}