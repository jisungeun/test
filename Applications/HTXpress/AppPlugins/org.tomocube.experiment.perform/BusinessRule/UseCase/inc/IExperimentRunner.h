#pragma once
#include <memory>

#include <Experiment.h>
#include <Area.h>
#include <enum.h>

#include "IImagePort.h"
#include "IRunExperimentOutputPort.h"
#include "ExperimentStatus.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    BETTER_ENUM(AcquisitionType, int32_t, Single, Timelapse, OneCycle);

    class HTX_Experiment_Perform_UseCase_API IExperimentRunner {
    public:
        using Pointer = std::shared_ptr<IExperimentRunner>;
        
    protected:
        IExperimentRunner();

    public:
        virtual ~IExperimentRunner();

        static auto GetInstance()->IExperimentRunner*;

        virtual auto SetOutputPort(IRunExperimentOutputPort* output)->void = 0;
        virtual auto SetExperiment(const QString& projectTitle, AppEntity::Experiment::Pointer experiment)->void = 0;
        virtual auto SetVesselIndex(AppEntity::VesselIndex index)->void = 0;
        virtual auto SetOverlapInUM(const uint32_t overlapInUM)->void = 0;
        virtual auto SetAcquisitionType(const AcquisitionType& type)->void = 0;
        virtual auto SetMultiDishHolder(bool isMultiDishHolder) -> void = 0;

        virtual auto Run()->bool = 0;
        virtual auto Stop()->bool = 0;

    protected:
        auto ConvertGlobal2WellIndex(const AppEntity::Position& globalPos)->AppEntity::WellIndex;
        auto ConvertGlobal2Well(const AppEntity::Position& globalPos, const AppEntity::WellIndex wellIdx)->AppEntity::Position;
    };
}