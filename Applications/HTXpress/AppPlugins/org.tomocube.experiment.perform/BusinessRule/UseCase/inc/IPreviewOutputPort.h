#pragma once
#include <memory>
#include <QImage>

#include <AppEntityDefines.h>
#include <Position.h>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IPreviewOutputPort {
    public:
        IPreviewOutputPort();
        virtual ~IPreviewOutputPort();

        virtual auto Progress(double progress, AppEntity::Position& position)->void = 0;
        virtual auto UpdateImageSize(int32_t xPixels, int32_t yPixels)->void = 0;
        virtual auto UpdateBlock(int32_t xPos, int32_t yPos, const QImage& blockImage)->void = 0;

        virtual auto UpdateCustomPreviewArea(double wellXinMM, double wellYinMM, int32_t widthUM, int32_t heightUM)->void=0;
        virtual auto DoCustomPreviewAreaSetting()->void=0;
        virtual auto CancelCustomPreviewAreaSetting()->void=0;
        virtual auto SetPreviewArea(double wellXinMM, double wellYinMM, double widthMM, double heightMM)->void=0;
    };
}