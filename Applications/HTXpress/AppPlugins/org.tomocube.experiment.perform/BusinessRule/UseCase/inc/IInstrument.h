#pragma once
#include <memory>

#include <PositionGroup.h>
#include <ImagingScenario.h>

#include <ChannelConfig.h>
#include <PreviewUnitMove.h>

#include "IImagePort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IInstrument {
    public:
        using Pointer = std::shared_ptr<IInstrument>;

        struct MotionStatus {
            bool error{ false };
            bool moving{ false };
            bool afFailed{ false };
            QString message;
        };

        struct JoystickStatus {
            enum Flag {
                Hold,
                PlusDirection,
                MinusDirection
            };

            Flag flag{ Flag::Hold };
            AppEntity::Axis axis{ AppEntity::Axis::X };
        };

    protected:
        IInstrument();

    public:
        virtual ~IInstrument();

        static auto GetInstance()->IInstrument*;

        virtual auto StartInitialize()->bool = 0;
        virtual auto GetInitializationProgress() const->std::tuple<bool,double> = 0;
        virtual auto IsInitialized() const->bool = 0;
        virtual auto UpdateConfiguration()->bool = 0;

        virtual auto InstallImagePort(IImagePort::Pointer port)->void = 0;
        virtual auto UninstallImagePort(IImagePort::Pointer port)->void = 0;
        virtual auto ImageCountInBuffer() const->int32_t = 0;

        virtual auto ChangeFOV(int32_t xPixels, int32_t yPixels, int32_t& offsetX, int32_t& offsetY)->bool = 0;
        virtual auto ChangeFullFOV()->bool = 0;
        virtual auto ChangeLiveModality(AppEntity::Modality modality, int32_t channel = 0)->bool = 0;
        virtual auto GetCurrentLiveModality() const->std::tuple<AppEntity::Modality, int32_t> = 0;

        virtual auto RunImagingSequence(const AppEntity::ImagingSequence::Pointer sequence,
                                        const QList<AppEntity::PositionGroup>& positions,
                                        const AppEntity::WellIndex startingWellIndex,
                                        const double focusReadyMM,
                                        const bool useMultiDishHolder)->bool = 0;
        virtual auto CheckSequenceProgress() const->std::tuple<bool,double,AppEntity::Position> = 0;
        virtual auto StopAcquisition()->bool = 0;

        virtual auto MoveAxis(const AppEntity::Position& target)->bool = 0;
        virtual auto MoveAxis(const AppEntity::Axis axis, const double targetMM)->bool = 0;
        virtual auto MoveZtoSafePos()->bool = 0;
        virtual auto StartJogAxis(const AppEntity::Axis axis, bool plusDirection)->bool = 0;
        virtual auto StopJogAxis()->bool = 0;
        virtual auto CheckAxisMotion() const->MotionStatus = 0;
        virtual auto GetAxisPosition() const->AppEntity::Position = 0;
        virtual auto GetAxisPositionMM(const AppEntity::Axis axis) const->double = 0;

        virtual auto SetJogRange(const AppEntity::Axis axis, const double minPosMM, const double maxPosMM)->void = 0;
        virtual auto EnableJoystick()->bool = 0;
        virtual auto DisableJoystick()->bool = 0;
        virtual auto CheckJoystick() -> JoystickStatus = 0;

        virtual auto StartLive(AppEntity::Modality modality, int32_t channel=0, bool onlyUpdateParameter = false)->bool = 0;
        virtual auto StopLive()->bool = 0;
        virtual auto ResumeLive()->bool = 0;

        virtual auto CapturePreviews(int32_t count, double gain)->bool = 0;
        virtual auto StartPreviewAcquisition(const QList<Entity::PreviewUnitMove>& moves, double bfExposureMSec)->bool = 0;

        virtual auto EnableAutoFocus()->std::tuple<bool,bool> = 0;
        virtual auto DisableAutoFocus()->bool = 0;
        virtual auto AutoFocusEnabled() const->bool = 0;
        virtual auto PerformAutoFocus()->std::tuple<bool,bool> = 0;
        virtual auto ReadAFSensorValue()->int32_t = 0;
        virtual auto SetBestFocusCurrent(int32_t& value)->bool = 0;
        virtual auto SetBestFocus(int32_t value)->bool = 0;
        virtual auto ScanFocus(double startMm, double distMm, int32_t count)->QList<int32_t> = 0;

        virtual auto PerformCondenserAutoFocus(IImagePort::Pointer port, int32_t patternIndex, int32_t intensity)->bool = 0;
        virtual auto CheckCondenserAutoFocusProgress() const->std::tuple<bool,double> = 0;
        virtual auto FinishCondenserAutoFocus(IImagePort::Pointer port)->void = 0;

        virtual auto PerformHTIlluminationCalibration(IImagePort::Pointer port, const QList<int32_t>& intensityList)->bool = 0;
        virtual auto CheckHTIlluminationCalibrationProgress() const->std::tuple<bool,double> = 0;
        virtual auto FinishHTIlluminationCalibration(IImagePort::Pointer port)->void = 0;

        virtual auto ShowHTIlluminationSetupPattern(int32_t patternIndex, int32_t intensity)->bool = 0;

        virtual auto SampleStageEncoderSupported() const->bool = 0;

        virtual auto GetErrorMessage() const->QString = 0;
    };
}
