#pragma once
#include <memory>

#include "IUseCase.h"
#include "IImagingConditionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SetFieldOfView : public IUseCase {
    public:
        SetFieldOfView(bool updateExperiment, IImagingConditionOutputPort* output = nullptr);
        ~SetFieldOfView() override;

        auto SetFOV(double xInUm, double yInUm)->void;
        auto GetAdjustedFOV(double& xInUm, double& yInUm)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}