#pragma once
#include <memory>
#include <QCoreApplication>

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "IRunExperimentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API QualityCheckExperiment : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(QualityCheckExperiment)

    public:
        using WellIdx = AppEntity::WellIndex;
        using LocationIdx = AppEntity::LocationIndex;

    public:
        QualityCheckExperiment(IRunExperimentOutputPort* output = nullptr);
        ~QualityCheckExperiment() override;

        auto SetAllPotins()->void;
        auto SetCurrentPoint()->void;
        auto SetMatrixPoints(int32_t xCount, int32_t yCount, int32_t xGapUm, int32_t yGapUm)->void;

        auto SetRepeatCount(int32_t repeatCount)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}