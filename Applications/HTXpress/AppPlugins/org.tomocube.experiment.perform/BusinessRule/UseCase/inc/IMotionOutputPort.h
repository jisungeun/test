#pragma once
#include <memory>

#include <AppEntityDefines.h>
#include <Position.h>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IMotionOutputPort {
    public:
        IMotionOutputPort();
        virtual ~IMotionOutputPort();

        virtual auto UpdateStatus(bool moving)->void = 0;
        virtual auto UpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position)->void = 0;
        virtual auto UpdateGlobalPosition(const AppEntity::Position& position)->void = 0;
        virtual auto UpdateGlobalPosition(AppEntity::Axis axis, double posInMm)->void = 0;
        virtual auto UpdateSelectedWell(const AppEntity::WellIndex wellIdx)->void = 0;
        virtual auto UpdateBestFocus(double posInMm)->void = 0;
        
        virtual auto ReportError(const QString& message)->void = 0;
        virtual auto ReportAFFailure()->void = 0;
    };
}