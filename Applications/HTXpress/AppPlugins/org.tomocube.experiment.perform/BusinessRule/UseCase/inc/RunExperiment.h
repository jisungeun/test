#pragma once
#include <memory>

#include "IUseCase.h"
#include "IRunExperimentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API RunExperiment : public IUseCase {
    public:
        RunExperiment(IRunExperimentOutputPort* output = nullptr);
        ~RunExperiment();

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}