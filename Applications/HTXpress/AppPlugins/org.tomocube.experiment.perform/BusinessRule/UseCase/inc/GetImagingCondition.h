#pragma once
#include <memory>

#include <ImagingConfig.h>

#include "IUseCase.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API GetImagingCondition : public IUseCase {
    public:
        GetImagingCondition();
        ~GetImagingCondition();

        auto GetConfig(AppEntity::ImagingMode mode)->AppEntity::ChannelConfig::Pointer;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
