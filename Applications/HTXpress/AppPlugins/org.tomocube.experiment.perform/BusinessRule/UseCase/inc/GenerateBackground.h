#pragma once
#include <memory>

#include "IUseCase.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API GenerateBackground : public IUseCase {
    public:
        GenerateBackground();
        ~GenerateBackground() override;

        auto SetSource(const QString& path)->void;
        auto SetTarget(const QString& path)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}