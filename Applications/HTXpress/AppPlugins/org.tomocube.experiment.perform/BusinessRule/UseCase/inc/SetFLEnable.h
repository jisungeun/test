#pragma once
#include <memory>

#include "IUseCase.h"
#include "IImagingConditionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SetFLEnable : public IUseCase {
    public:
        SetFLEnable(bool updateExperiment, IImagingConditionOutputPort* output = nullptr);
        ~SetFLEnable() override;

        auto SetEnable(const bool is3D, const bool enable)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}