#pragma once
#include <memory>
#include <QCoreApplication>

#include "IUseCase.h"
#include "IMotionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API StartJog : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(StartJog)

    public:
        StartJog(IMotionOutputPort* output = nullptr);
        ~StartJog() override;

        auto Set(AppEntity::Axis axis, bool plusDirection)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}