﻿#pragma once

#include <memory>

#include <AppEntityDefines.h>
#include <Location.h>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IMarkPositionOutputPort {
    public:
        IMarkPositionOutputPort();
        virtual ~IMarkPositionOutputPort();

        virtual auto AddMarkPosition(AppEntity::WellIndex wellIndex, AppEntity::Location::Pointer location)->void = 0;
    };
}
