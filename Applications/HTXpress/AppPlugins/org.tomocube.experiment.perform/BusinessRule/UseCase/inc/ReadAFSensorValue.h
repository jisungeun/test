#pragma once
#include <memory>
#include <qcoreapplication.h>

#include "IUseCase.h"
#include "IInstrumentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ReadAFSensorValue : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(ReadAFSensorValue)

    public:
        ReadAFSensorValue(IInstrumentOutputPort* output = nullptr);
        ~ReadAFSensorValue();

        auto GetValue() const->int32_t;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}