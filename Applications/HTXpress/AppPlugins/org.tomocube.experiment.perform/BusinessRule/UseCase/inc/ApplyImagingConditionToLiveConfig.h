#pragma once
#include <memory>

#include <ChannelConfig.h>

#include "IUseCase.h"
#include "ILiveviewConfigOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ApplyImagingConditionToLiveConfig : public IUseCase {
    public:
        ApplyImagingConditionToLiveConfig(ILiveviewConfigOutputPort* output = nullptr);
        ~ApplyImagingConditionToLiveConfig() override;

        auto ApplyConfig(const AppEntity::ImagingMode mode)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}