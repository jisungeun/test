#pragma once
#include <memory>

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "IAcquisitionPositionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API AddMatrixAcquisitionPoints : public IUseCase {
    public:
        AddMatrixAcquisitionPoints(IAcquisitionPositionOutputPort* output = nullptr);
        ~AddMatrixAcquisitionPoints() override;

        auto SetCenter(const AppEntity::WellIndex wellIdx, const AppEntity::LocationIndex locationIdx)->void;
        auto SetMatrix(const int32_t cols, const int32_t rows, const double horGapMm, const double verGapMm)->void;
        auto SetTargets(const QList<AppEntity::WellIndex>& wells)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}