﻿#pragma once

#include <memory>

#include "IUseCase.h"
#include "IPreviewOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
	class HTX_Experiment_Perform_UseCase_API SetCustomPreviewAreaEditableStatus : public IUseCase {
	public:
		explicit SetCustomPreviewAreaEditableStatus(IPreviewOutputPort* output = nullptr);
		~SetCustomPreviewAreaEditableStatus() override;
		
		auto DoAreaSetting() -> void;
		auto CancelAreaSetting() -> void;
		
	protected:
		auto Perform() -> bool override;
		
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}