#pragma once
#include <memory>
#include <QStringList>

#include "IUseCase.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API GetProjectList : public IUseCase {
    public:
        GetProjectList();
        ~GetProjectList();

        auto GetList() const->QStringList;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
        