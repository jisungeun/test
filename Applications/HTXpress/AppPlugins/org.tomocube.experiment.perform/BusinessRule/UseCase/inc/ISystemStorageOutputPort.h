﻿#pragma once
#include <memory>
#include <QString>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ISystemStorageOutputPort {
    public:
        using Self = ISystemStorageOutputPort;
        using Pointer = std::shared_ptr<Self>;
        
        ISystemStorageOutputPort();
        virtual ~ISystemStorageOutputPort();

        virtual auto UpdateSystemStorageSpace(const int64_t& totalBytes, const int64_t& availableBytes) -> void = 0;
        virtual auto UpdateMinRequiredSpace(const int32_t& gigabytes) -> void = 0;
    };
}
