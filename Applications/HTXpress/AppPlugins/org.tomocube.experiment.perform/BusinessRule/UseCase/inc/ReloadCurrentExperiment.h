#pragma once
#include <memory>
#include <QString>

#include "IUseCase.h"
#include "IExperimentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ReloadCurrentExperiment : public IUseCase {
    public:
        ReloadCurrentExperiment(IExperimentOutputPort* output = nullptr);
        ~ReloadCurrentExperiment() override;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}