#pragma once
#include <memory>
#include <QCoreApplication>

#include "IUseCase.h"
#include "IInstrumentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API EnableAutoFocus : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(EnableAutoFocus)

    public:
        EnableAutoFocus(IInstrumentOutputPort* output = nullptr);
        ~EnableAutoFocus() override;

        auto SetEnable(bool enable)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}