#pragma once
#include <memory>

#include <Experiment.h>
#include <PositionGroup.h>
#include "ImagingTimeTable.h"

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IScanTimeCalculator {
    public:
        using ImagingCondition = AppEntity::ImagingCondition;
        using ImagingSequence = AppEntity::ImagingSequence;
        using WellIndex = AppEntity::WellIndex;
        using Location = AppEntity::Location;
        using LocationIndex = AppEntity::LocationIndex;
        using PositionGroup = AppEntity::PositionGroup;

    protected:
        IScanTimeCalculator();

    public:
        virtual ~IScanTimeCalculator();

        static auto GetInstance()->IScanTimeCalculator*;

        virtual auto SetMaximumFOV(AppEntity::Area area)->void = 0;
        virtual auto SetOverlapInUM(uint32_t overlap)->void = 0;
        virtual auto SetFilterPosition(AppEntity::Modality modality, int32_t channel, int32_t position)->void = 0;
        virtual auto SetUsingMultidishHolder(bool useMultidishHolder)->void = 0;

        virtual auto CalcInSec(const ImagingCondition::Pointer& imgCond, 
                               ImagingTimeTable::Pointer& timeTable,
                               const ImagingCondition::Pointer& prevCond)->void = 0;
        virtual auto CalcInSec(const QList<Location>& locations, ImagingTimeTable::Pointer& timeTable)->void = 0;
        virtual auto CalcInSec(const QList<PositionGroup>& positions, ImagingTimeTable::Pointer& timeTable)->void = 0;
    };
}