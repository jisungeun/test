#pragma once
#include <memory>
#include <QString>

#include <BeadScore.h>

#include "IQualityCheckOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IBeadEvaluator {
    public:
        using Pointer = std::shared_ptr<IBeadEvaluator>;

    protected:
        IBeadEvaluator();

    public:
        virtual ~IBeadEvaluator();

        static auto GetInstance()->IBeadEvaluator*;

        virtual auto Clear()->void = 0;

        virtual auto InstallOutputPort(IQualityCheckOutputPort* output)->void = 0;
        virtual auto AddData(const QString& path, int32_t centerXInPixel, int32_t centerYInPixel)->void = 0;
        virtual auto SetOutputPath(const QString& path)->void = 0;

        virtual auto Evaluate()->bool = 0;

        virtual auto GetCount() const->int32_t = 0;
        virtual auto GetResult(int32_t index) const->Entity::BeadScore = 0;
    };
}
