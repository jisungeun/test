#pragma once

#include <Position.h>
#include <Area.h>

#include "IUseCase.h"
#include "IAcquisitionPositionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API DeleteAcquisitionPosition : public IUseCase {
    public:
        DeleteAcquisitionPosition(IAcquisitionPositionOutputPort* output = nullptr);
        ~DeleteAcquisitionPosition();

        auto SetPosition(AppEntity::WellIndex wellIdx, AppEntity::LocationIndex locationIdx)->void;

    protected:
        auto Perform() ->bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}