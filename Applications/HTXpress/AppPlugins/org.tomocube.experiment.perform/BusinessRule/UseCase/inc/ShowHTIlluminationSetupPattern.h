#pragma once
#include <memory>
#include <QCoreApplication>

#include "IUseCase.h"
#include "IInstrumentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ShowHTIlluminationSetupPattern : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(StartJog)

    public:
        ShowHTIlluminationSetupPattern(IInstrumentOutputPort* output = nullptr);
        ~ShowHTIlluminationSetupPattern() override;

        auto SetNA(double NA)->void;
        auto SetIntensity(int32_t intensity)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}