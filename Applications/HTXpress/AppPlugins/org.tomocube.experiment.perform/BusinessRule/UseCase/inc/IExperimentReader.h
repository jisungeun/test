#pragma once
#include <memory>

#include <Experiment.h>
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IExperimentReader {
    public:
        using Pointer = std::shared_ptr<IExperimentReader>;

    protected:
        IExperimentReader();

    public:
        virtual ~IExperimentReader();

        static auto GetInstance()->IExperimentReader*;

        virtual auto Read(const QString& path) const->AppEntity::Experiment::Pointer = 0;
    };
}
