#pragma once
#include <memory>
#include <QCoreApplication>

#include <AppEntityDefines.h>
#include <Position.h>

#include "IUseCase.h"
#include "IMotionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API MoveSampleStage : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(MoveSampleStage)

    public:
        MoveSampleStage(IMotionOutputPort* output = nullptr);
        ~MoveSampleStage();

        auto SetTargetWell(const AppEntity::WellIndex wellIdx)->void;
        auto SetTargetXInUm(const AppEntity::WellIndex wellIdx, double pos)->void;
        auto SetTargetYInUm(const AppEntity::WellIndex wellIdx, double pos)->void;
        auto SetTargetZInUm(const AppEntity::WellIndex wellIdx, double pos)->void;
        auto SetTargetXYInUm(const AppEntity::WellIndex wellIdx, double xUm, double yUm)->void;
        auto SetGlobalTargetXYInUm(double xUm, double yUm)->void;
        auto SetRelativeTargetPixels(const int32_t relPosX, const int32_t relPosY)->void;
        auto SetRelativeTargetUm(const AppEntity::Axis axis, const double distUm)->void;
        auto SetTargetUm(const AppEntity::Axis axis, const double posInUM)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}