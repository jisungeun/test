#pragma once
#include <memory>

#include "IImagePort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ICondenserAutofocus {
    public:
        using Pointer = std::shared_ptr<ICondenserAutofocus>;

    protected:
        ICondenserAutofocus();

    public:
        virtual ~ICondenserAutofocus();

        static auto GetInstance()->ICondenserAutofocus*;

        virtual auto GetImagePort() const->IImagePort::Pointer = 0;
        virtual auto EnableLogging(const bool enable)->void = 0;
        virtual auto SaveLog(const QString& message)->void = 0;
        virtual auto FindBestFocus() const->std::tuple<bool, int32_t> = 0;
        virtual auto GetScores() const->QList<double> = 0;
    };
}