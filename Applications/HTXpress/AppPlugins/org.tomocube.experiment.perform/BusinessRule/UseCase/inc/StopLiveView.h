#pragma once
#include <memory>

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "IInstrumentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API StopLiveView : public IUseCase {
    public:
        StopLiveView(IInstrumentOutputPort* output);
        ~StopLiveView() override;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}