#pragma once
#include <memory>

#include "IUseCase.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SaveSystemConfiguration : public IUseCase {
    public:
        SaveSystemConfiguration();
        ~SaveSystemConfiguration() override;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}