#pragma once
#include <memory>

#include "IImagePort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IHTIlluminationAutoCalibration {
    public:
        using Pointer = std::shared_ptr<IHTIlluminationAutoCalibration>;

    protected:
        IHTIlluminationAutoCalibration();

    public:
        virtual ~IHTIlluminationAutoCalibration();

        static auto GetInstance()->IHTIlluminationAutoCalibration*;

        virtual auto GetImagePort() const->IImagePort::Pointer = 0;
        virtual auto SetImages(int32_t setCount, int32_t imagesPerSet)->void = 0;
        virtual auto SetOffset(int32_t imagesPerSlice, int32_t offset)->void = 0;
        virtual auto SetThreshold(int32_t threshold)->void = 0;
        virtual auto EnableLogging(const bool enable)->void = 0;

        virtual auto StartCalibrate() const->void = 0;
        virtual auto GetResult() const->std::tuple<int32_t, std::shared_ptr<AppEntity::RawImage>> = 0;
    };
}