#pragma once
#include <memory>
#include <QCoreApplication>
#include <QList>

#include <BeadScore.h>

#include "IUseCase.h"
#include "IQualityCheckOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API EvaluateBead : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(EvaluateBead)

    public:
        EvaluateBead(IQualityCheckOutputPort* output = nullptr);
        ~EvaluateBead() override;

        auto AddData(const QString& path, int32_t xInPixel, int32_t yInPixel)->void;
        auto SetOutputPath(const QString& path)->void;

        auto GetResults() const->QList<Entity::BeadScore>;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}