#pragma once
#include <memory>
#include <QString>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IBGGenerator {
    public:
        using Pointer = std::shared_ptr<IBGGenerator>;

    protected:
        IBGGenerator();

    public:
        virtual ~IBGGenerator();

        static auto GetInstance()->IBGGenerator*;

        virtual auto Process(const QString& srourcePath, const QString& targetPath)->bool = 0;
    };

}