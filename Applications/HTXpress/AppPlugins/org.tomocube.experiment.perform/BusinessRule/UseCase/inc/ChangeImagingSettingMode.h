﻿#pragma once
#include <memory>

#include "IUseCase.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"
#include "ILiveviewConfigOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ChangeImagingSettingMode : public IUseCase{
    public:
        explicit ChangeImagingSettingMode(ILiveviewConfigOutputPort* output = nullptr);
        ~ChangeImagingSettingMode() override;

        auto SetCurrentImagingSettingMode(const AppEntity::ImagingSettingMode& mode) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}