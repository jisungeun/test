#pragma once
#include <memory>

#include <ChannelConfig.h>

#include "IUseCase.h"
#include "ILiveviewConfigOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SetLiveImagingConfig : public IUseCase {
    public:
        SetLiveImagingConfig(ILiveviewConfigOutputPort* output = nullptr);
        ~SetLiveImagingConfig() override;

        auto SetConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}