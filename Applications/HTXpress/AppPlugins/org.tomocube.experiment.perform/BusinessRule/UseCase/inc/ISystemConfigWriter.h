#pragma once
#include <memory>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ISystemConfigWriter {
    public:
        using Pointer = std::shared_ptr<ISystemConfigWriter>;

    protected:
        ISystemConfigWriter();

    public:
        virtual ~ISystemConfigWriter();

        static auto GetInstance()->ISystemConfigWriter*;

        virtual auto Write()->bool = 0;
    };
}
