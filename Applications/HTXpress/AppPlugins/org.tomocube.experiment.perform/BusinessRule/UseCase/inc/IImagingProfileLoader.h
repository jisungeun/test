﻿#pragma once
#include <optional>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IImagingProfileLoader {
    public:
        virtual ~IImagingProfileLoader() = default;
        static auto GetInstance()->IImagingProfileLoader*;
        
        virtual auto GetHtScanParameter() const->std::optional<std::tuple<int32_t, int32_t>> = 0;

    protected:
        IImagingProfileLoader();
    };
}
