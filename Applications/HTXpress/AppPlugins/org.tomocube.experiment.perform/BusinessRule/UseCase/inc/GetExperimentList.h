#pragma once
#include <memory>
#include <QStringList>

#include "IUseCase.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API GetExperimentList : public IUseCase {
    public:
        GetExperimentList();
        ~GetExperimentList();

        auto SetProject(const QString& project)->void;
        auto GetList() const->QStringList;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}