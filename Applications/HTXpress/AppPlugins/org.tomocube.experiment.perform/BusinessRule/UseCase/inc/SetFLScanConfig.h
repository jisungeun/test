#pragma once
#include <memory>

#include <ScanConfig.h>

#include "IUseCase.h"
#include "IImagingConditionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API SetFLScanConfig : public IUseCase {
    public:
        SetFLScanConfig(bool updateExperiment, IImagingConditionOutputPort* output = nullptr);
        ~SetFLScanConfig() override;

        auto SetScanConfig(const AppEntity::ScanConfig& config)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}