﻿#pragma once

#include <Area.h>

#include "IUseCase.h"
#include "IMarkPositionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API AddMarkPosition : public IUseCase {
    public:
        explicit AddMarkPosition(IMarkPositionOutputPort* output = nullptr);
        ~AddMarkPosition() override;

        auto SetCurrentArea(double xInMM, double yInMM, const AppEntity::Area& area, bool isTile)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
