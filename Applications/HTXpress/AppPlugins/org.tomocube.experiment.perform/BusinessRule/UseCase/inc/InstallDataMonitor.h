#pragma once

#include <memory>

#include <IDataOutputPort.h>

#include "IUseCase.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API InstallDataMonitor : public IUseCase {
    public:
        InstallDataMonitor();
        ~InstallDataMonitor();

        auto SetDataOutputPort(IDataOutputPort* outputPort)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
