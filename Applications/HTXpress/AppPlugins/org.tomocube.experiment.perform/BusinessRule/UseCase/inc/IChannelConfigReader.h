#pragma once
#include <memory>

#include <ChannelConfig.h>
#include <ImagingConfig.h>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API IChannelConfigReader {
    public:
        using Pointer = std::shared_ptr<IChannelConfigReader>;
        using ImagingMode = AppEntity::ImagingMode;
        using ChannelConfig =AppEntity::ChannelConfig;

    protected:
        IChannelConfigReader();

    public:
        virtual ~IChannelConfigReader();

        static auto GetInstance()->IChannelConfigReader*;

        virtual auto Read(ImagingMode mode, ChannelConfig& config) const->bool = 0;
    };
}