#pragma once

#include "IUseCase.h"
#include "IRunExperimentOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API TestExperiment : public IUseCase {
    public:
        TestExperiment(IRunExperimentOutputPort* output = nullptr);
        ~TestExperiment();

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}