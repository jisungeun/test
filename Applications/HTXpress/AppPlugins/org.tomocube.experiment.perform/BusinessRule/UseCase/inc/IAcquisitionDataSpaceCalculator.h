﻿#pragma once
#include <memory>

#include <enum.h>

#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    namespace AcqDataSpaceCal {
        BETTER_ENUM(Modality, int32_t, HT3D, FL3D, FL2D, BFGray, NotSupport);
    }

    class HTX_Experiment_Perform_UseCase_API IAcquisitionDataSpaceCalculator {
    public:
        using Pointer = std::shared_ptr<IAcquisitionDataSpaceCalculator>;

        virtual ~IAcquisitionDataSpaceCalculator();
        static auto GetInstance() -> IAcquisitionDataSpaceCalculator*;

        virtual auto Clear() -> void = 0;

        virtual auto SetPixels(const int32_t& pixelX, const int32_t& pinxelY) -> void = 0;
        virtual auto SetPoints(const int32_t& points) -> void = 0;
        virtual auto SetModalityCount(const AcqDataSpaceCal::Modality& modality, const int32_t& count) -> void = 0;
        virtual auto SetSliceCount(const AcqDataSpaceCal::Modality& modality3D, const int32_t& count) -> void = 0;

        virtual auto GetRequiredSpace() const -> int64_t = 0;

    protected:
        IAcquisitionDataSpaceCalculator();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
