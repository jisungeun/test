#pragma once
#include <memory>

#include <ChannelConfig.h>

#include "IUseCase.h"
#include "IImagingConditionOutputPort.h"
#include "HTX_Experiment_Perform_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::UseCase {
    class HTX_Experiment_Perform_UseCase_API ClearFLChannels : public IUseCase {
    public:
        ClearFLChannels(IImagingConditionOutputPort* output = nullptr);
        ~ClearFLChannels() override;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}