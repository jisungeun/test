#pragma once
#include <memory>

#include "HTX_Experiment_Perform_EntityExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Entity {
    struct HTX_Experiment_Perform_Entity_API PreviewUnitMove {
        int32_t motionStartXPulse;
        int32_t motionStartYPulse;
        int32_t motionStartZPulse;

        bool forwardDirection;        
        int32_t triggerIntervalPulse;
        int32_t triggerCount;
    };
}