#pragma once
#include <memory>
#include <QImage>

#include "HTX_Experiment_Perform_EntityExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Entity {
    class HTX_Experiment_Perform_Entity_API BeadScore {
    public:
        BeadScore();
        BeadScore(const BeadScore& other);
        ~BeadScore();

        auto operator=(const BeadScore& rhs)->BeadScore&;

        auto SetVolume(const double& volume)->void;
        auto GetVolume() const->double;

        auto SetDrymass(const double& drymass)->void;
        auto GetDrymass() const->double;

        auto SetCorrelation(const double& correlation)->void;
        auto GetCorrelation() const->double;

        auto SetMeanDeltaRI(const double& meanDeltaRI)->void;
        auto GetMeanDeltaRI() const->double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
