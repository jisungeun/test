#include "BeadScore.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Entity {
    struct BeadScore::Impl {
        double volume{ 0 };
        double drymass{ 0 };
        double correlation{ 0 };
        double meanDeltaRI{ 0 };
    };

    BeadScore::BeadScore() : d{ std::make_unique<Impl>() } {
    }

    BeadScore::BeadScore(const BeadScore& other) : d{ std::make_unique<Impl>() } {
        *d = *other.d;
    }

    BeadScore::~BeadScore() {
    }

    auto BeadScore::operator=(const BeadScore& rhs) -> BeadScore& {
        *d = *rhs.d;
        return *this;
    }

    auto BeadScore::SetVolume(const double& volume) -> void {
        d->volume = volume;
    }

    auto BeadScore::GetVolume() const -> double {
        return d->volume;
    }

    auto BeadScore::SetDrymass(const double& drymass) -> void {
        d->drymass = drymass;
    }

    auto BeadScore::GetDrymass() const -> double {
        return d->drymass;
    }

    auto BeadScore::SetCorrelation(const double& correlation) -> void {
        d->correlation = correlation;
    }

    auto BeadScore::GetCorrelation() const -> double {
        return d->correlation;
    }

    auto BeadScore::SetMeanDeltaRI(const double& meanDeltaRI) -> void {
        d->meanDeltaRI = meanDeltaRI;
    }

    auto BeadScore::GetMeanDeltaRI() const -> double {
        return d->meanDeltaRI;
    }
}
