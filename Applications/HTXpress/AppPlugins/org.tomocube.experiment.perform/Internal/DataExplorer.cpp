#define LOGGER_TAG "[DataExplorer]"
#include <QStyle>
#include <QDebug>

#include <TCLogger.h>

#include <TCFDataRepo.h>
#include <SystemStatus.h>

#include "InstrumentObserver.h"
#include "ExperimentIOObserver.h"
#include "AcquisitionDataObserver.h"
#include "SystemStorageObserver.h"

#include "DataExplorerControl.h"
#include "DataExplorer.h"
#include "ui_DataExplorer.h"

using HTXpress::AppComponents::ProcessedDataDefinition::TCFProcessingStatus;
using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct DataExplorer::Impl {
        Ui::DataExplorer ui;
        DataExplorerControl control;

        InstrumentObserver* instrumentObserver{ nullptr };
        ExperimentIOObserver* experimentObserver{ nullptr };
        AcquisitionDataObserver* acquisitionDataObserver{ nullptr };
        SystemStorageObserver* storageObserver{ nullptr };

        auto GenerateAcquisitionListItem(const QString& path, const QString& specimen, const QString& well, const QString& acqData)-> AppComponents::AcquisitionListPanel::AcquisitionDataIndex;
        auto Convert(TCFProcessingStatus state)->AppComponents::AcquisitionListPanel::ProcessingStatus;
    };

    auto DataExplorer::Impl::GenerateAcquisitionListItem(const QString& path, const QString& specimen, const QString& well, const QString& acqData) -> AppComponents::AcquisitionListPanel::AcquisitionDataIndex {
        AppComponents::AcquisitionListPanel::AcquisitionDataIndex acquisitionData;
        acquisitionData.SetGroupName(specimen);
        acquisitionData.SetWellPositionName(well);
        acquisitionData.SetAcquisitionDataName(acqData);
        acquisitionData.SetAcquisitionDataPath(path);

        // data folder의 시작 포맷이 [yyMMdd.hhmmss.] 일때 유효
        const auto subStrings = acqData.section(".", 0, 1);
        if (!subStrings.isEmpty()) {
            acquisitionData.SetTimestamp(subStrings);
        }
        
        return acquisitionData;
    }

    auto DataExplorer::Impl::Convert(TCFProcessingStatus state) -> AppComponents::AcquisitionListPanel::ProcessingStatus {
        auto result = AppComponents::AcquisitionListPanel::ProcessingStatus::Invalid;
        if (state == +TCFProcessingStatus::NotYet || state == +TCFProcessingStatus::InProgress) {
            result = AppComponents::AcquisitionListPanel::ProcessingStatus::Unprocessed;
        } else if (state == +TCFProcessingStatus::Completed) {
            result = AppComponents::AcquisitionListPanel::ProcessingStatus::Processed;
        }

        return result;
    }

    DataExplorer::DataExplorer(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.baseWidget->setObjectName("panel");
        d->ui.vesselLoadBtn->setObjectName("bt-tsx-unloadVessel");
        d->ui.goToExpSetupBtn->setObjectName("bt-tsx-setupExperiment");
        d->ui.endBtn->setObjectName("bt-tsx-end");
        d->ui.goToDataNaviBtn->setObjectName("bt-tsx-dataNavigation");

        d->instrumentObserver = new InstrumentObserver(this);
        d->experimentObserver = new ExperimentIOObserver(this);
        d->acquisitionDataObserver = new AcquisitionDataObserver(this);
        d->storageObserver = new SystemStorageObserver(this);

        connect(d->ui.vesselLoadBtn, &QPushButton::clicked, this, &DataExplorer::onVesselLoadClicked);
        connect(d->ui.endBtn, &QPushButton::clicked, this, &DataExplorer::onEndExperimentClicked);
        connect(d->ui.goToExpSetupBtn, &QPushButton::clicked, this, &DataExplorer::sigGotoExpSetup);
        connect(d->ui.goToDataNaviBtn, &QPushButton::clicked, this, &DataExplorer::sigGotoDataNavigation);
        connect(d->ui.storageInfoView, &AppComponents::StorageInfoPanel::StorageInfoPanel::sigRefresh,
                this, &DataExplorer::onRefreshStorageInfo);

        connect(d->instrumentObserver, SIGNAL(sigVesselLoaded(bool)), this, SLOT(onUpdateVesselStatus(bool)));

        connect(d->experimentObserver, &ExperimentIOObserver::sigUpdate, this, &DataExplorer::onUpdateExperiment);

        connect(d->acquisitionDataObserver, &AcquisitionDataObserver::sigDataAdded, this, &DataExplorer::onAddAcquisitionData);
        connect(d->acquisitionDataObserver, &AcquisitionDataObserver::sigDataUpdated, this, &DataExplorer::onUpdateAcquisitionData);
        connect(d->acquisitionDataObserver, &AcquisitionDataObserver::sigDataDeleted, this, &DataExplorer::onDeleteAcquisitionData);
        connect(d->acquisitionDataObserver, &AcquisitionDataObserver::sigDataRootFolderDeleted, this, &DataExplorer::onDeleteAcquisitionDataFolder);

        connect(d->storageObserver, &SystemStorageObserver::sigUpdateSystemStorageSpace, this, &DataExplorer::onUpdateStorageInfo);
        connect(d->storageObserver, &SystemStorageObserver::sigUpdateMinRequiredSpace, this, &DataExplorer::onUpdateMinRequiredSpace);

        if (!d->control.InstallAcquisitionDataMonitor()) {
            QLOG_ERROR() << "It fails to install acquisition data monitor";
        }

        if (!d->control.InstallStorageMonitor()) {
            QLOG_ERROR() << "It fails to install storage monitor";
        }
    }

    DataExplorer::~DataExplorer() {
    }

    void DataExplorer::onVesselLoadClicked() {
        if (d->control.IsVesselLoaded()) {
            d->control.UnloadVessel();
        } else {
            d->control.LoadVessel();
        }
    }

    void DataExplorer::onUpdateVesselStatus(bool loaded) {
        if(loaded) {
            d->ui.vesselLoadBtn->setText("Unload Vessel");
            d->ui.vesselLoadBtn->setObjectName("bt-tsx-unloadVessel");

            d->control.SetVesselLoadStatus(true);
        } else {
            d->ui.vesselLoadBtn->setText("Load Vessel");
            d->ui.vesselLoadBtn->setObjectName("bt-tsx-loadVessel");

            d->control.SetVesselLoadStatus(false);
        }

        d->ui.vesselLoadBtn->style()->unpolish(d->ui.vesselLoadBtn);
        d->ui.vesselLoadBtn->style()->polish(d->ui.vesselLoadBtn);
        d->ui.vesselLoadBtn->update();
    }

    void DataExplorer::onEndExperimentClicked() {
        d->control.UnloadVessel();
        emit sigEndExperiment();
    }

    void DataExplorer::onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool undo) {
        Q_UNUSED(experiment)

        if(undo) {
            return;
        }

        d->ui.dataListView->ClearAll();

        connect(d->acquisitionDataObserver, &AcquisitionDataObserver::sigDataScanned, this, &DataExplorer::onScanAcquisitionData);

        if (!d->control.ScanAcquisitionData()) {
            QLOG_ERROR() << "It fails to get acquisition data";
        }

        if (!d->control.UpdateRootDrive()) {
            QLOG_ERROR() << "It fails to update root drive";
        }

        if (!d->control.GetStorageInformation()) {
            QLOG_ERROR() << "It fails to get storage infomration";
        }

        if (!d->control.UpdateMinRequiredSpace()) {
            QLOG_ERROR() << "It fails to get min. required space information";
        }
    }

    void DataExplorer::onScanAcquisitionData(const QString& user, const QString& project, const QString& experiment) {
        Q_UNUSED(user)

        const auto dataRepo = TCFDataRepo::GetInstance();
        const auto experiments = dataRepo->GetExperimentList(user, project);
        if (!experiments.contains(experiment)) {
            QLOG_ERROR() << "It fails to get acquisition data";
            return;
        }

        auto specimens = experiments.value(experiment);
        TCFDataRepo::SpecimenList::iterator specimenIt;
        for (specimenIt = specimens.begin(); specimenIt != specimens.end(); ++specimenIt) {
            if (specimenIt.key().isEmpty()) continue;

            auto wells = specimenIt.value();
            TCFDataRepo::WellList::iterator wellIt;
            for (wellIt = wells.begin(); wellIt != wells.end(); ++wellIt) {
                if (wellIt.key().isEmpty()) continue;

                auto dataList = wellIt.value();
                TCFDataRepo::DataList::iterator dataIt;
                for (dataIt = dataList.begin(); dataIt != dataList.end(); ++dataIt) {
                    if (dataIt.key().isEmpty() || nullptr == dataIt.value()) continue;

                    auto state = AppComponents::AcquisitionListPanel::ProcessingStatus::Invalid;
                    auto acqData = dataIt.value();
                    if (acqData) {
                        state = d->Convert(acqData->GetStatus());
                    }

                    d->ui.dataListView->AddAcquisitionData(d->GenerateAcquisitionListItem(acqData->GetPath(), 
                                                                                          specimenIt.key(), 
                                                                                          wellIt.key(), 
                                                                                          dataIt.key()),
                                                                                          state);
                }
            }
        }

        d->ui.dataListView->SetDefaultTableOrder();

        disconnect(d->acquisitionDataObserver, &AcquisitionDataObserver::sigDataScanned, this, &DataExplorer::onScanAcquisitionData);
    }

    void DataExplorer::onAddAcquisitionData(const QString& fileFullPath) {
        const auto systemStatus = AppEntity::SystemStatus::GetInstance();
        const auto dataRepo = TCFDataRepo::GetInstance();
        const auto user = d->control.ParseUserName(fileFullPath);
        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);
        const auto acqData = dataRepo->GetData(user, project, experiment, specimen, well, dataName);

        if (acqData == nullptr) {
            QLOG_ERROR() << "Failed to add acquisition data."  << "Acquisition data pointer is nullptr";
            return;
        }

        d->ui.dataListView->AddAcquisitionData(d->GenerateAcquisitionListItem(fileFullPath, specimen, well, dataName));
    }

    void DataExplorer::onUpdateAcquisitionData(const QString& fileFullPath) {
        const auto systemStatus = AppEntity::SystemStatus::GetInstance();
        const auto dataRepo = TCFDataRepo::GetInstance();
        const auto user = d->control.ParseUserName(fileFullPath);
        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);

        const auto acqData = dataRepo->GetData(user, project, experiment, specimen, well, dataName);

        if (acqData == nullptr) {
            QLOG_ERROR() << "Failed to update acquisition data." << "Acquisition data pointer is nullptr";
            return;
        }
        // data 획득 진행 상태 확인하여 list에 적용
        d->ui.dataListView->SetDataProcessingStatus(d->GenerateAcquisitionListItem(fileFullPath, specimen, well, dataName),
                                                    d->Convert(acqData->GetStatus())
        );
    }

    void DataExplorer::onDeleteAcquisitionData(const QString& fileFullPath) {
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);

        d->ui.dataListView->SetDataProcessingStatus(d->GenerateAcquisitionListItem(fileFullPath, specimen, well, dataName),
                                                    d->Convert(TCFProcessingStatus::NotYet));
    }

    void DataExplorer::onDeleteAcquisitionDataFolder(const QString& fileFullPath) {
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);

        d->ui.dataListView->DeleteAcquisitionData(d->GenerateAcquisitionListItem(fileFullPath, specimen, well, dataName));
    }

    void DataExplorer::onUpdateStorageInfo(const int64_t& bytesTotal, const int64_t& bytesAvailable) {
        d->ui.storageInfoView->SetStorageInfo(bytesTotal, bytesAvailable);
    }

    void DataExplorer::onUpdateMinRequiredSpace(const int32_t& gigabytes) {
        d->ui.storageInfoView->SetMinRequiredSpace(gigabytes);
    }

    void DataExplorer::onRefreshStorageInfo() {
        if (!d->control.GetStorageInformation()) {
            QLOG_ERROR() << "Failed to get storage infomration with refresh button.";
        }
    }
}
