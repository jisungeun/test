﻿#include <QList>

#include "MarkPositionUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct MarkPositionUpdater::Impl {
        QList<MarkPositionObserver*> observers;
    };

    MarkPositionUpdater::MarkPositionUpdater() : IMarkPositionView(), d{std::make_unique<Impl>()} {
    }

    MarkPositionUpdater::~MarkPositionUpdater() {
    }

    auto MarkPositionUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{new MarkPositionUpdater()};
        return theInstance;
    }

    auto MarkPositionUpdater::AddMarkPosition(AppEntity::WellIndex wellIndex, AppEntity::Location::Pointer location) -> void {
        for(auto observer : d->observers) {
            observer->AddMarkPosition(wellIndex, location);
        }
    }

    auto MarkPositionUpdater::Register(MarkPositionObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto MarkPositionUpdater::Deregister(MarkPositionObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}
