#define LOGGER_TAG "[VesselMapPanel]"
#include <TCLogger.h>
#include <MessageDialog.h>

#include <VesselMapUtility.h>
#include <SystemStatus.h>
#include <System.h>
#include <ImagingPointCopy.h>

#include "AcquisitionPositionObserver.h"
#include "ExperimentIOObserver.h"
#include "RunExperimentObserver.h"
#include "MotionObserver.h"
#include "InstrumentObserver.h"
#include "ImagingPlanObserver.h"
#include "PreviewObserver.h"
#include "MarkPositionObserver.h"
#include "VesselObserver.h"

#include "VesselmapPanel.h"
#include "ui_VesselmapPanel.h"
#include "VesselmapPanelControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct VesselmapPanel::Impl {
        Ui::VesselmapPanel ui;
        VesselmapPanelControl control;

        AcquisitionPositionObserver* positionObserver{ nullptr };
        ExperimentIOObserver* experimentIOObserver{ nullptr };
        RunExperimentObserver* runExperimentObserver{ nullptr };
        MotionObserver* motionObserver{ nullptr };
        InstrumentObserver* instrumentObserver{ nullptr };
        ImagingPlanObserver* imagingPlanObserver{ nullptr };
        PreviewObserver* previewObserver{ nullptr };
        MarkPositionObserver *markPositionObserver{nullptr};
        VesselObserver *vesselObserver{nullptr};

        bool lastTileVisibleStatus{};

        auto ConvertRowColumnsToVesselMapWellIndex(const QList<AppEntity::RowColumn>& rowColumns) const -> QList<TC::WellIndex>;
        auto ConvertEntityAxisToVesselMapAxis(AppEntity::Axis axis) -> TC::VesselAxis;
        auto GetAcquisitionType(bool isTile) -> TC::AcquisitionType;
        auto GetMarkType(bool isTile) -> TC::MarkType;
    };

    VesselmapPanel::VesselmapPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);
        d->ui.view->SetViewMode(TC::ViewMode::PerformMode);

        d->positionObserver = new AcquisitionPositionObserver(this);
        d->experimentIOObserver = new ExperimentIOObserver(this);
        d->runExperimentObserver = new RunExperimentObserver(this);
        d->motionObserver = new MotionObserver(this);
        d->instrumentObserver = new InstrumentObserver(this);
        d->imagingPlanObserver = new ImagingPlanObserver(this);
        d->previewObserver = new PreviewObserver(this);
        d->markPositionObserver = new MarkPositionObserver(this);
        d->vesselObserver = new VesselObserver(this);

        //Acquisition Positions
        connect(d->positionObserver, SIGNAL(sigAddPosition(AppEntity::LocationIndex, AppEntity::WellIndex, AppEntity::Location::Pointer)), this, 
                SLOT(onAddPosition(AppEntity::LocationIndex, AppEntity::WellIndex, AppEntity::Location::Pointer)));
        connect(d->positionObserver, SIGNAL(sigRefreshList(AppEntity::WellIndex, QMap<AppEntity::LocationIndex, AppEntity::Location::Pointer>&)), this, 
                SLOT(onRefreshList(AppEntity::WellIndex, QMap<AppEntity::LocationIndex, AppEntity::Location::Pointer>&)));
        connect(d->positionObserver, SIGNAL(sigDeletePosition(AppEntity::WellIndex, AppEntity::LocationIndex)), this, 
                SLOT(onDeletePosition(AppEntity::WellIndex, AppEntity::LocationIndex)));
        connect(d->experimentIOObserver, SIGNAL(sigUpdate(AppEntity::Experiment::Pointer, bool)), this,
                SLOT(onUpdateExperiment(AppEntity::Experiment::Pointer, bool)));
        connect(d->runExperimentObserver, 
                SIGNAL(sigUpdateCurrentPosition(const AppEntity::WellIndex, const AppEntity::Position&)), 
                this,
                SLOT(onUpdatePosition(const AppEntity::WellIndex, const AppEntity::Position&)));
        connect(d->runExperimentObserver, &RunExperimentObserver::sigUpdateProgress,
                this, &VesselmapPanel::onUpdateSingleAcquireProgress);
        connect(d->motionObserver, SIGNAL(sigUpdatePosition(const AppEntity::WellIndex, const AppEntity::Position&)), this,
                SLOT(onUpdatePosition(const AppEntity::WellIndex, const AppEntity::Position&)));
        connect(d->motionObserver, SIGNAL(sigUpdateGlobalPosition(const AppEntity::Position&)), this,
                SLOT(onUpdateGlobalPosition(const AppEntity::Position&)));
        connect(d->motionObserver, SIGNAL(sigUpdateCurrentWell(const AppEntity::WellIndex)), this,
                SLOT(onUpdateCurrentWell(const AppEntity::WellIndex)));
        connect(d->instrumentObserver, SIGNAL(sigVesselLoaded(bool)), this, SLOT(onUpdateVesselStatus(bool)));
        connect(d->instrumentObserver, SIGNAL(sigUpdateGlobalPosition(const AppEntity::Position&)), this,
                SLOT(onUpdateGlobalPosition(const AppEntity::Position&)));
        connect(d->instrumentObserver, SIGNAL(sigProgress(double, const QString&)), this,
                SLOT(onUpdateInitProgress(double, const QString&)));

        connect(d->ui.view, SIGNAL(sigChangeFocusedWell(TC::WellIndex, int32_t, int32_t)), this, 
                SLOT(onSetCurrentWell(TC::WellIndex, int32_t, int32_t)));
        connect(d->ui.view, SIGNAL(sigChangeFocusedWell(TC::WellIndex)), this, 
                SLOT(onSetCurrentWell(TC::WellIndex)));
        connect(d->ui.view, SIGNAL(sigDoubleClickedWellCanvasPosition(double, double)), this, SLOT(onChangeSampleStagePosition(double, double)));
        connect(d->ui.view, SIGNAL(sigDoubleClickedLocationPosition(double, double, double)),
                this, SLOT(onChangeSampleStagePosition(double, double, double)));
        connect(d->ui.view, SIGNAL(sigAcquisitionItemDeleted(const TC::WellIndex&, const TC::AcquisitionIndex&)), this,
                SLOT(onDeleteAcquisitionPoint(const TC::WellIndex&, const TC::AcquisitionIndex&)));
        connect(d->ui.view, SIGNAL(sigChangeLensPosition(const TC::VesselAxis, const double)), this,
                SLOT(onChangePosition(const TC::VesselAxis, const double)));
    	connect(d->ui.view, SIGNAL(sigTileImagingAreaChanged(const double&, const double&, const double&, const double&, const double&)),
                this, SLOT(onTileImagingAreaChanged(double,double,double,double,double)));
        connect(d->ui.view, &TC::VesselMapWidget::sigPreviewAreaChanged, this, &Self::onCustomPreviewAreaChanged);
        connect(d->ui.view, &TC::VesselMapWidget::sigChangeImagingOrder, this, &Self::onChangeImagingOrder);
        connect(d->ui.view, &TC::VesselMapWidget::sigCopyImagingPoints, this, &Self::onCopyImagingPoints);

        connect(d->imagingPlanObserver, SIGNAL(sigUpdateTileScanArea(bool, AppEntity::WellIndex, double, double, double, double)), this,
                SLOT(onUpdateTileScanArea(bool, AppEntity::WellIndex, double, double, double, double)));
        connect(d->imagingPlanObserver, SIGNAL(sigUpdateFOV(const double, const double)), this, 
                SLOT(onUpdateFOV(const double, const double)));
        connect(d->imagingPlanObserver, &ImagingPlanObserver::sigUpdateTileActivation, this, &Self::onShowTileArea);
        connect(d->previewObserver, SIGNAL(sigUpdatePosition(const AppEntity::Position&)), this,
                SLOT(onUpdateGlobalPosition(const AppEntity::Position&)));
        connect(d->previewObserver, SIGNAL(sigStartCustomPreviewAreaSetting()), this, SLOT(onStartCustomPreviewAreaSetting()));
        connect(d->previewObserver, SIGNAL(sigCancelCustomPreviewAreaSetting()), this, SLOT(onCancelCustomPreviewAreaSetting()));
        connect(d->previewObserver, &PreviewObserver::sigUpdateImageSize, this, [this](int32_t xPixels, int32_t yPixels) {
            Q_UNUSED(xPixels)
            Q_UNUSED(yPixels)
            d->ui.view->SetPreviewLocationEditable(false);
        });
        connect(d->previewObserver, &PreviewObserver::sigSetPreviewArea, this, &Self::onSetPreviewArea);

        connect(d->markPositionObserver, &MarkPositionObserver::sigAddMarkPosition, this, &VesselmapPanel::onAddMarkPosition);
        connect(d->vesselObserver, &VesselObserver::sigUpdateSafeMovingRange, this, &VesselmapPanel::onUpdateSafeMovingRange);
    }

    VesselmapPanel::~VesselmapPanel() {
    }

    void VesselmapPanel::onAddPosition(AppEntity::LocationIndex locationIdx, AppEntity::WellIndex wellIdx,
                                       AppEntity::Location::Pointer location) {
        //Todo Acquisition Position 추가 코드 구현

        const auto center = location->GetCenter();
        const auto area = location->GetArea();
        const auto type = d->GetAcquisitionType(location->IsTile());
        d->ui.view->AddAcquisitionLocationByWellPos(wellIdx, 
                                                    locationIdx, 
                                                    type,
                                                    center.toMM().x,
                                                    center.toMM().y,
                                                    center.toMM().z,
                                                    area.toMM().width,
                                                    area.toMM().height);
    }

    void VesselmapPanel::onAddMarkPosition(AppEntity::WellIndex wellIdx, AppEntity::Location::Pointer location) {
        const auto center = location->GetCenter();
        const auto area = location->GetArea();
        const auto type = d->GetMarkType(location->IsTile());
        d->ui.view->AddMarkLocationByWellPos(wellIdx, type, center.toMM().x, center.toMM().y, center.toMM().z, area.toMM().width, area.toMM().height);
    }

    void VesselmapPanel::onRefreshList(AppEntity::WellIndex wellIdx,
                                       QMap<AppEntity::LocationIndex, AppEntity::Location::Pointer>& locationList) {
        //Todo Acquisition Position 목록 갱신 구현
        Q_UNUSED(wellIdx)
        Q_UNUSED(locationList)
    }

    void VesselmapPanel::onDeletePosition(AppEntity::WellIndex wellIdx, AppEntity::LocationIndex locationIdx) {
        //Todo Acquisition Position 삭제 구현
        Q_UNUSED(wellIdx)
        Q_UNUSED(locationIdx)
    }

    void VesselmapPanel::onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool reloaded) {
        if(reloaded) {
            return;
        }

        using Converter = AppComponents::VesselMapUtility::Converter;

        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        const auto vesselIndex = sysStatus->GetCurrentVesselIndex();
        const auto& vessel = experiment->GetVessel();
        const auto& wellNames = experiment->GetWellNames(vesselIndex);
        auto vesselmap = Converter::ToTCVesselMap(vesselIndex, experiment, vessel, wellNames);

        d->ui.view->ClearAll();
        d->ui.view->SetVesselMap(vesselmap);

        auto model = AppEntity::System::GetModel();
        auto minPosZ = model->AxisLowerLimitMM(AppEntity::Axis::Z);
        auto maxPosZ = model->AxisUpperLimitMM(AppEntity::Axis::Z);

        const auto range2D = vesselmap->GetImagingAreaRange();
        d->control.SetSafeMovingRange(AppEntity::Axis::X, range2D.rangeX().min, range2D.rangeX().max);
        d->control.SetSafeMovingRange(AppEntity::Axis::Y, range2D.rangeY().min, range2D.rangeY().max);
        d->control.SetSafeMovingRange(AppEntity::Axis::Z, minPosZ, maxPosZ);

        auto locations = experiment->GetAllLocations(vesselIndex);
        const auto wellList = locations.keys();
        for(auto wellIdx : wellList) {
            auto& locs = locations[wellIdx];
            for(auto iter=locs.begin(); iter!=locs.end(); iter++) {
                auto center = iter.value().GetCenter();
                auto area = iter.value().GetArea();
                auto type = d->GetAcquisitionType(iter.value().IsTile());
                d->ui.view->AddAcquisitionLocationByWellPos(wellIdx, 
                                                            iter.key(),
                                                            type,
                                                            center.toMM().x,
                                                            center.toMM().y,
                                                            center.toMM().z,
                                                            area.toMM().width,
                                                            area.toMM().height);
            }
        }

        const auto wellGroupIndices = experiment->GetWellGroupIndices(vesselIndex);
        for(const auto& wellGroupIndex : wellGroupIndices) {
            const auto wellGroup = experiment->GetWellGroup(vesselIndex, wellGroupIndex);
            QList<AppEntity::RowColumn> wellRowColumns;
            auto wells = wellGroup.GetWells();
            for(const auto &well : wells) {
                wellRowColumns.push_back(AppEntity::RowColumn(well.rowIdx, well.colIdx));
            }

            auto groupName = wellGroup.GetTitle();
            auto groupColor = QColor(wellGroup.GetColor().r,
                                     wellGroup.GetColor().g,
                                     wellGroup.GetColor().b,
                                     wellGroup.GetColor().a);

            d->ui.view->CreateNewGroup(wellGroupIndex, 
                                       groupName,
                                       groupColor,
                                       d->ConvertRowColumnsToVesselMapWellIndex(wellRowColumns));
        }

        d->ui.view->SetWellImagingOrder({});
        d->ui.view->HidePreviewLocation();
     }

    void VesselmapPanel::onUpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position) {
        double vesselX, vesselY, vesselZ;
        d->control.GetVesselPosition(wellIdx, position, vesselX, vesselY, vesselZ);
        d->ui.view->SetObjectiveLensPos(vesselX, vesselY, vesselZ);
        d->ui.view->SetTileImagingAreaCenter(position.toMM().x, position.toMM().y, position.toMM().z);
    }

    void VesselmapPanel::onUpdateGlobalPosition(const AppEntity::Position& position) {
        double vesselX, vesselY, vesselZ;
        d->control.GetVesselPosition(position, vesselX, vesselY, vesselZ);
        d->ui.view->SetObjectiveLensPos(vesselX, vesselY, vesselZ);

        double wellX, wellY, wellZ;
        d->control.GetWellPosition(position, wellX, wellY, wellZ);
        d->ui.view->SetTileImagingAreaCenter(wellX, wellY, wellZ);
    }

    void VesselmapPanel::onUpdateCurrentWell(const AppEntity::WellIndex wellIndex) {
        d->ui.view->SetSelectedWell(wellIndex);
    }

    void VesselmapPanel::onUpdateInitProgress(double progress, const QString& message) {
        Q_UNUSED(message)
        if(progress < 1.0) return;

        AppEntity::WellIndex wellIndex;
        AppEntity::Position globalPostion;
        if(d->control.RefreshCurrentPosition(wellIndex, globalPostion)) {
            d->ui.view->SetSelectedWell(wellIndex);
            onUpdateGlobalPosition(globalPostion);
        }
    }

    void VesselmapPanel::onUpdateVesselStatus(bool loaded) {
        setEnabled(loaded);
    }

    void VesselmapPanel::onSetCurrentWell(TC::WellIndex wellIndex, int32_t row, int32_t col) {
        Q_UNUSED(row)
        Q_UNUSED(col)
        if(wellIndex == -1) return;
        d->control.ChangeCurrentWell(static_cast<AppEntity::WellIndex>(wellIndex));
    }

    void VesselmapPanel::onSetCurrentWell(TC::WellIndex wellIndex) {
        if(wellIndex == -1) return;
        d->control.ChangeCurrentWell(static_cast<AppEntity::WellIndex>(wellIndex));
    }

    void VesselmapPanel::onChangeSampleStagePosition(double xMM, double yMM) {
        d->control.ChangeSampleStagePosition(d->ui.view->GetFocusedWellIndex(), xMM, yMM);
    }

    void VesselmapPanel::onChangeSampleStagePosition(double xMM, double yMM, double zMM) {
        d->control.ChangeSampleStagePosition(d->ui.view->GetFocusedWellIndex(), xMM, yMM, zMM);
    }

    void VesselmapPanel::onDeleteAcquisitionPoint(const TC::WellIndex& wellIdx, 
                                                  const TC::AcquisitionIndex& pointIndex) {
        d->control.DeleteAcquisitionPosition(wellIdx, pointIndex);
    }

    void VesselmapPanel::onChangePosition(const TC::VesselAxis axis, const double posInMM) {
        auto axisConverted = [=]()->AppEntity::Axis {
            auto ret = AppEntity::Axis::X;
            switch(axis) {
            case TC::VesselAxis::Y:
                ret = AppEntity::Axis::Y;
                break;
            case TC::VesselAxis::Z:
                ret = AppEntity::Axis::Z;
                break;
            }
            return ret;
        }();
        d->control.MoveAxis(axisConverted, posInMM);
    }

    void VesselmapPanel::onUpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex, 
                                              double xInMm, double yInMm, 
                                              double widthInUm, double heightInUm) {
        Q_UNUSED(wellIndex)

        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        if(sysStatus->GetCurrentWell() != wellIndex) return;

        const auto globalPosition = sysStatus->GetCurrentGlobalPosition();

        double posXInMm, posYInMm, posZInMm;
        d->control.GetVesselPosition(globalPosition, posXInMm, posYInMm, posZInMm);

        d->ui.view->SetTileImagingArea(xInMm, yInMm, posZInMm, widthInUm/1000, heightInUm/1000);
        onShowTileArea(enable);
    }

    void VesselmapPanel::onUpdateFOV(const double xInUm, const double yInUm) {
        Q_UNUSED(xInUm)
        Q_UNUSED(yInUm)

        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        onUpdateRoiSize(AppEntity::SystemStatus::GetInstance()->GetExperiment());
    }
    
    void VesselmapPanel::onTileImagingAreaChanged(double xMM, double yMM, double zMM, double widthMM, double heightMM) {
        Q_UNUSED(zMM)
        QLOG_INFO() << "Tile imaging area is update [X=" << xMM << " Y=" << yMM
            << " W=" << widthMM << " H=" << heightMM << "]";
        if(!d->control.ChangeTileImagingArea(xMM, yMM, zMM, widthMM, heightMM)) {
            QLOG_ERROR() << "It fails to update tile image area information";
            TC::MessageDialog::warning(this, tr("Update Tile Imaging Area"), tr("Failed to update tiling information"));
        }
    }

    void VesselmapPanel::onUpdateRoiSize(AppEntity::Experiment::Pointer experiment) {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto vesselIndex = sysStatus->GetCurrentVesselIndex();
        auto locations = experiment->GetAllLocations(vesselIndex);
        const auto wellList = locations.keys();
        for(auto wellIdx : wellList) {
            auto& locs = locations[wellIdx];
            for(auto iter=locs.begin(); iter!=locs.end(); iter++) {
                auto center = iter.value().GetCenter();
                auto area = iter.value().GetArea();
                auto type = d->GetAcquisitionType(iter.value().IsTile());
                d->ui.view->SetAcquisitionLocationByWellPos(wellIdx, iter.key(),
                                                            type,
                                                            center.toMM().x,
                                                            center.toMM().y,
                                                            center.toMM().z,
                                                            area.toMM().width,
                                                            area.toMM().height);
            }
        }
    }

    void VesselmapPanel::onCustomPreviewAreaChanged(const double& xMM, const double& yMM, const double& widthMM, const double& heightMM) {
        QLOG_INFO() << "Preview area is update [ W=" << widthMM << " H=" << heightMM << "]";

        if(!d->control.ChangeCustomPreviewArea(xMM, yMM, widthMM, heightMM)) {
            QLOG_ERROR() << "It fails to update preview area information";
            TC::MessageDialog::warning(this, tr("Update Preview Area"), tr("Failed to update preview area"));
        }
    }

    void VesselmapPanel::onStartCustomPreviewAreaSetting() {
        d->ui.view->StartCustomPreviewAreaSetting();
    }

    void VesselmapPanel::onCancelCustomPreviewAreaSetting() {
        d->ui.view->StopCustomPreviewAreaSetting();
    }

    void VesselmapPanel::onChangeImagingOrder(const TC::ImagingOrder& order) {
        QLOG_INFO() << "Imaging order is changed:" << order._to_string();

        // TO DO well imaging order 받아서 view에 할당해주기
        d->ui.view->SetWellImagingOrder({});
    }

    void VesselmapPanel::onUpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM) {
        d->ui.view->SetSafePositionRange(d->ConvertEntityAxisToVesselMapAxis(axis), 
                                         minMM, 
                                         maxMM);
    }

    void VesselmapPanel::onUpdateSingleAcquireProgress(const double progress, const int elapsedSeconds, const int remainSeconds) {
        Q_UNUSED(elapsedSeconds)
        Q_UNUSED(remainSeconds)

        d->ui.view->ShowTileImagingArea(false);
        const bool isCompleted = progress >=1.0 ? true : false;
        if(isCompleted) {
            onShowTileArea(d->lastTileVisibleStatus);
        }
    }

    void VesselmapPanel::onCopyImagingPoints(const TC::WellIndex& sourceWellIndex, const QList<TC::AcquisitionIndex>& targetPositionIndices) {
        using CopyDialog = AppComponents::ImagingPointCopyDialog::ImagingPointCopy;
        using Type = AppComponents::ImagingPointCopyDialog::Type;
        using InputData = AppComponents::ImagingPointCopyDialog::Input;
        using Location = AppComponents::ImagingPointCopyDialog::Location;
        using Converter = AppComponents::VesselMapUtility::Converter;

        const auto& experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        const auto& vessel = experiment->GetVessel();
        const auto& vesselIndex = AppEntity::SystemStatus::GetInstance()->GetCurrentVesselIndex();
        const auto& wellNames = experiment->GetWellNames(vesselIndex);
        const auto wellGroupIndices = experiment->GetWellGroupIndices(vesselIndex);

        auto input = std::make_shared<InputData>();

        for (const auto& wellGroupIndex : wellGroupIndices) {
            const auto wellGroup = experiment->GetWellGroup(vesselIndex, wellGroupIndex);
            QList<AppEntity::RowColumn> wellRowColumns;
            auto wells = wellGroup.GetWells();
            for (const auto& well : wells) {
                wellRowColumns.push_back(AppEntity::RowColumn(well.rowIdx, well.colIdx));
            }

            auto groupName = wellGroup.GetTitle();
            auto groupColor = QColor(wellGroup.GetColor().r, wellGroup.GetColor().g, wellGroup.GetColor().b, wellGroup.GetColor().a);
            input->wellGroups[wellGroupIndex] = {groupColor, d->ConvertRowColumnsToVesselMapWellIndex(wellRowColumns)};
        }
        auto locations = experiment->GetAllLocations(vesselIndex);
        auto& locs = locations[sourceWellIndex];
        for (const auto& locIndex : targetPositionIndices) {
            const auto loc = locs.find(locIndex);
            const auto center = loc.value().GetCenter();
            const auto area = loc.value().GetArea();
            const auto acqType = d->GetAcquisitionType(loc.value().IsTile());

            Location location;
            location.index = locIndex;
            location.type = acqType;
            location.centerPos = {center.toMM().x, center.toMM().y, center.toMM().z};
            location.size = {area.toMM().width, area.toMM().height};

            input->targetPoints.push_back(location);
        }

        input->sourceWellIndex = sourceWellIndex;
        input->vesselMap = Converter::ToTCVesselMap(vesselIndex, experiment, vessel, wellNames);

        CopyDialog dlg;
        dlg.SetInputData(input);
        if (dlg.Execute()) {
            const auto output = dlg.GetOutputData();
            switch (output->type) {
                case Type::Copy: 
                    if(output->targetWells.empty()) break;
                    d->control.CopyAcquisitionPositions(output->sourceWellIndex, output->targetPointIndices, output->targetWells);
                    break;
                case Type::Matrix: {
                    const auto result = d->control.AddMatrixPositions(output->sourceWellIndex, 
                                                  output->targetPointIndices.first(), 
                                                  output->count.col, 
                                                  output->count.row, 
                                                  output->spacing.hor, 
                                                  output->spacing.ver, 
                                                  output->targetWells);
                    if(result) { d->ui.view->DeleteAcquisitionLocation(sourceWellIndex, targetPositionIndices.first()); }
                    break;
                }
                default: break;
            }
        }
    }

    void VesselmapPanel::onSetPreviewArea(double xMM, double yMM, double widthMM, double heightMM) {
        d->ui.view->ShowPreviewLocation();
        d->ui.view->SetPreviewLocationByWellPos(d->control.GetCurrentWell(), xMM, yMM, widthMM, heightMM);
    }

    void VesselmapPanel::onShowTileArea(bool show) {
        d->lastTileVisibleStatus = show;
        d->ui.view->ShowTileImagingArea(show);
    }

    auto VesselmapPanel::Impl::ConvertRowColumnsToVesselMapWellIndex(const QList<AppEntity::RowColumn>& rowColumns) const -> QList<TC::WellIndex> {
        if (ui.view == nullptr) {
            return QList<TC::WellIndex>();
        }

        QList<TC::WellIndex> wells;
        for (auto& rc : rowColumns) {
            const auto index = ui.view->GetWellIndex(rc.first, rc.second);
            if(index != -1) wells << index;
        }

        return wells;
    }

    auto VesselmapPanel::Impl::ConvertEntityAxisToVesselMapAxis(AppEntity::Axis entityAxis) -> TC::VesselAxis {
        switch (entityAxis) {
            case AppEntity::Axis::X: return TC::VesselAxis::X;
            case AppEntity::Axis::Y: return TC::VesselAxis::Y;
            case AppEntity::Axis::Z: return TC::VesselAxis::Z;
            default: return TC::VesselAxis::Inavlid;
        }
    }

    auto VesselmapPanel::Impl::GetAcquisitionType(bool isTile) -> TC::AcquisitionType {
        if(isTile) {
            return TC::AcquisitionType::Tile;
        }

        return TC::AcquisitionType::Point;
    }

    auto VesselmapPanel::Impl::GetMarkType(bool isTile) -> TC::MarkType {
        if(isTile) { return TC::MarkType::Tile; }
        return TC::MarkType::Point;
    }
}

