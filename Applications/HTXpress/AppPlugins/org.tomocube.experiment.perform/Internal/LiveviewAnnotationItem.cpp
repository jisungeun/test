#include "LiveviewAnnotator.h"
#include "LiveviewAnnotationItem.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    LiveviewAnnotationItem::LiveviewAnnotationItem() {
    }

    LiveviewAnnotationItem::~LiveviewAnnotationItem() {
    }

    auto LiveviewAnnotationItem::Install(int32_t xInPixel, int32_t yInPixel) -> void {
        auto annotator = LiveviewAnnotator::GetInstance();
        annotator->AddItem(this, xInPixel, yInPixel);
    }

    auto LiveviewAnnotationItem::Unintall() -> void {
        auto annotator = LiveviewAnnotator::GetInstance();
        annotator->RemoveItem(this);
    }
}
