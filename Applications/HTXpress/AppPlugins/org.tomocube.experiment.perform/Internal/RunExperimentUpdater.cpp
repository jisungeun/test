#include <QList>

#include "RunExperimentObserver.h"
#include "RunExperimentUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct RunExperimentUpdater::Impl {
        QList<RunExperimentObserver*> observers;
    };

    RunExperimentUpdater::RunExperimentUpdater() : Interactor::IExperimentView(), d{new Impl} {
    }

    RunExperimentUpdater::~RunExperimentUpdater() {
    }

    auto RunExperimentUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new RunExperimentUpdater() };
        return theInstance;
    }

    auto RunExperimentUpdater::UpdateProgress(const UseCase::ExperimentStatus& status) -> void {
        for(auto observer : d->observers) {
            observer->UpdateProgress(status);
        }
    }

    auto RunExperimentUpdater::NotifyStopped() -> void {
        for(auto observer : d->observers) {
            observer->NotifyStopped();
        }
    }

    auto RunExperimentUpdater::Register(RunExperimentObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto RunExperimentUpdater::Deregister(RunExperimentObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}
