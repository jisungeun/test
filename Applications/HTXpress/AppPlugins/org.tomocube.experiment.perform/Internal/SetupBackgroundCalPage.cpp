#include <QTimer>

#include <MessageDialog.h>

#include "RunExperimentObserver.h"
#include "AcquisitionDataObserver.h"
#include "SetupBackgroundCalPageControl.h"
#include "SetupBackgroundCalPage.h"
#include "ui_SetupBackgroundCalPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupBackgroundCalPage::Impl {
        SetupBackgroundCalPageControl control;
        Ui::SetupBackgroundCalPage ui;
        SetupBackgroundCalPage* p{ nullptr };

        RunExperimentObserver* experimentObserver{ nullptr };
        AcquisitionDataObserver* dataObserver{ nullptr };

        Impl(SetupBackgroundCalPage* p) : p{p} {}

        auto InitUi()->void;
        auto ClearResult()->void;
        auto ChangeNA()->void;
        auto RunCAF()->void;
        auto Calibrate()->void;
        auto StopCalibration()->void;
        auto Save()->void;
        auto ShowImage(int32_t index)->void;
        auto UpdateProgress(double progress)->void;
        auto SetLatestData(const QString& dataName);
    };

    auto SetupBackgroundCalPage::Impl::InitUi() -> void {
        ui.htNA->clear();
        ui.htNA->addItem("", 0);
        auto NAs = control.GetNAs();
        for(auto NA : NAs) {
            ui.htNA->addItem(QString::number(NA), NA);
        }

        ui.progressBar->hide();

        ui.calibrateBtn->setDisabled(true);

        ui.saveBtn->setDisabled(true);
        ui.savedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.savedLabel->hide();

        ui.imageView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.imageView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        ClearResult();
    }

    auto SetupBackgroundCalPage::Impl::ClearResult() -> void {
        ui.imageView->ClearImage();
    }

    auto SetupBackgroundCalPage::Impl::ChangeNA() -> void {
        ui.calibrateBtn->setDisabled(true);
        ui.saveBtn->setDisabled(true);
        ui.savedLabel->hide();

        ui.img1Btn->setDisabled(true);
        ui.img2Btn->setDisabled(true);
        ui.img3Btn->setDisabled(true);
        ui.img4Btn->setDisabled(true);

        ClearResult();
    }

    auto SetupBackgroundCalPage::Impl::RunCAF() -> void {
        const auto NA = ui.htNA->currentData().toDouble();
        if(NA == 0) return;

        ClearResult();

        if(!control.Parepare(NA)) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("Condenser autofocus is failed"));
            return;
        }

        ui.calibrateBtn->setEnabled(true);
    }

    auto SetupBackgroundCalPage::Impl::Calibrate() -> void {
        const auto NA = ui.htNA->currentData().toDouble();
        if(NA == 0) return;

        ClearResult();
        ui.progressBar->show();
        ui.progressBar->setValue(0);

        if(!control.StartCalibrate(NA)) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("Background calibration is failed"));
        }
    }

    auto SetupBackgroundCalPage::Impl::StopCalibration() -> void {
        ui.progressBar->setValue(0);
        ui.progressBar->hide();
    }

    auto SetupBackgroundCalPage::Impl::Save() -> void {
        const auto NA = ui.htNA->currentData().toDouble();
        if(NA == 0) return;

        if(!control.Save(NA)) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("Failed to save background images for NA %1").arg(NA));
            return;
        }

        ui.savedLabel->show();
        QTimer::singleShot(2000, [this]() { ui.savedLabel->hide(); });
    }

    auto SetupBackgroundCalPage::Impl::ShowImage(int32_t index) -> void {
        const auto NA = ui.htNA->currentData().toDouble();
        if(NA == 0) return;

        auto image = control.LoadImage(index);
        if(image.isNull()) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("No calibrated background image exists"));
            return;
        }

        ui.imageView->ShowImage(image);
    }

    auto SetupBackgroundCalPage::Impl::UpdateProgress(double progress) -> void {
        ui.progressBar->setValue(std::min(98, static_cast<int32_t>(progress*98)));
        if(progress < 1.0) {
            if(ui.progressBar->isHidden()) ui.progressBar->show();
            return;
        }

        const auto NA = ui.htNA->currentData().toDouble();
        if(!control.Calibrate(NA)) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("Background calibration is failed"));
            return;
        }

        ui.progressBar->setValue(100);
        QTimer::singleShot(2000, [this]() { ui.progressBar->hide(); });

        ui.saveBtn->setEnabled(true);
        ui.img1Btn->setEnabled(true);
        ui.img2Btn->setEnabled(true);
        ui.img3Btn->setEnabled(true);
        ui.img4Btn->setEnabled(true);

        ShowImage(0);
    }

    auto SetupBackgroundCalPage::Impl::SetLatestData(const QString& dataName) {
        control.SetLatestData(dataName);
    }

    SetupBackgroundCalPage::SetupBackgroundCalPage(QWidget* parent) : QWidget(parent), d{new Impl(this)} {
        d->ui.setupUi(this);
        d->InitUi();

        d->experimentObserver = new RunExperimentObserver(this);
        d->dataObserver = new AcquisitionDataObserver(this);

        connect(d->ui.htNA, &QComboBox::currentTextChanged, this, [=]() {
            d->ChangeNA();
        });

        connect(d->ui.runCAFBtn, &QPushButton::clicked, this, [this]() {
            d->RunCAF();
        });

        connect(d->ui.calibrateBtn, &QPushButton::clicked, this, [this]() {
            d->Calibrate();
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this]() {
            d->Save();
        });

        connect(d->ui.img1Btn, &QPushButton::clicked, this, [this]() {
            d->ShowImage(0);
        });

        connect(d->ui.img2Btn, &QPushButton::clicked, this, [this]() {
            d->ShowImage(1);
        });

        connect(d->ui.img3Btn, &QPushButton::clicked, this, [this]() {
            d->ShowImage(2);
        });

        connect(d->ui.img4Btn, &QPushButton::clicked, this, [this]() {
            d->ShowImage(3);
        });

        connect(d->experimentObserver, &RunExperimentObserver::sigUpdateProgress, this, 
                [this](const double progress, const int elapsedSeconds, const int remainSeconds) {
            Q_UNUSED(elapsedSeconds)
            Q_UNUSED(remainSeconds)
            d->UpdateProgress(progress);
        });

        connect(d->experimentObserver, &RunExperimentObserver::sigStopped, this, [this]() {
            d->StopCalibration();
        });

        connect(d->dataObserver, &AcquisitionDataObserver::sigDataAdded, this, 
                [this](const QString& fileFullPath) {
            d->SetLatestData(fileFullPath);
        });

        d->ui.leftWidget->setObjectName("panel");
        d->ui.botWidget->setObjectName("panel");

        for (const auto& label : findChildren<QLabel*>()) {
            if(label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
        }

        for (const auto& button : findChildren<QPushButton*>()) {
            if(button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            } else {
                button->setObjectName("bt-maintenance-light");
            }
        }
    }

    SetupBackgroundCalPage::~SetupBackgroundCalPage() {
    }

    auto SetupBackgroundCalPage::Enter() -> void {
        d->ui.htNA->setCurrentIndex(0);
    }

    auto SetupBackgroundCalPage::Leave() -> void {
        d->ui.htNA->setCurrentIndex(0);
    }

    void SetupBackgroundCalPage::resizeEvent(QResizeEvent* event) {
        d->ui.imageView->FitZoom();
        QWidget::resizeEvent(event);
    }
}