#include <QKeyEvent>

#include "InstrumentObserver.h"
#include "EvaluationSetupPage.h"
#include "EvaluationIntensityPage.h"
#include "EvaluationCAFPage.h"
#include "EvaluationHTIllumCalPage.h"
#include "EvaluationAFPage.h"
#include "EvaluationSingleBeadPage.h"
#include "EvaluationBeadRepeatabilityPage.h"
#include "EvaluationMatrixBeadsPage.h"
#include "EvaluationDialog.h"
#include "ui_EvaluationDialog.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationDialog::Impl {
        Ui::EvaluationDialog ui;
        int32_t lastTab = -1;

        InstrumentObserver* instrumentObserver{ nullptr };

        auto Enable(bool enable)->void;
    };

    auto EvaluationDialog::Impl::Enable(bool enable) -> void {
        ui.tabWidget->setEnabled(enable);
    }

    EvaluationDialog::EvaluationDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
        d->ui.setupUi(this);

        setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);

        d->Enable(false);

        d->instrumentObserver = new InstrumentObserver(this);

        auto* setupPage = new EvaluationSetupPage(this);

        d->ui.tabWidget->addTab(setupPage, "Setup");
        d->ui.tabWidget->addTab(new EvaluationIntensityPage(this), "Intensity");
        d->ui.tabWidget->addTab(new EvaluationCAFPage(this), "Condenser AF");
        d->ui.tabWidget->addTab(new EvaluationHTIllumCalPage(this), "HT Calibration");
        d->ui.tabWidget->addTab(new EvaluationAFPage(this), "Autofocus");
        d->ui.tabWidget->addTab(new EvaluationSingleBeadPage(this), "Bead");
        d->ui.tabWidget->addTab(new EvaluationBeadRepeatabilityPage(this), "Repeatability");
        d->ui.tabWidget->addTab(new EvaluationMatrixBeadsPage(this), "Matrix");

        const auto tabCount = d->ui.tabWidget->count();
        for(auto idx=1; idx<tabCount; idx++) {
            d->ui.tabWidget->setTabEnabled(idx, false);
        }

        connect(setupPage, &EvaluationSetupPage::sigReady, this, [this, tabCount]() {
            for(auto idx=1; idx<tabCount; idx++) {
                d->ui.tabWidget->setTabEnabled(idx, true);
            }
        });

        connect(d->ui.tabWidget, &QTabWidget::currentChanged, this, [this](int32_t index) {
            auto* prevPage = dynamic_cast<EvaluationPage*>(d->ui.tabWidget->widget(d->lastTab));
            if(prevPage) {
                prevPage->Leave();
            }

            auto* page = dynamic_cast<EvaluationPage*>(d->ui.tabWidget->currentWidget());
            if(page) {
                page->Enter();
                d->lastTab = index;
            }
        });

        connect(d->instrumentObserver, &InstrumentObserver::sigVesselLoaded, this, [this](bool loaded) {
            d->Enable(loaded);
        });

        d->ui.baseWidget->setObjectName("panel");
    }

    void EvaluationDialog::keyPressEvent(QKeyEvent* event) {
        switch(event->key()) {
        case Qt::Key_Enter:
        case Qt::Key_Return:
            return;
        case Qt::Key_Escape:
            return;
        default:
            QDialog::keyPressEvent(event);
        }
    }
}
