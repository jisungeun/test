#pragma once
#include <memory>

#include <QWidget>
#include "SetupPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SetupAutofocusPage : public QWidget, public SetupPage {
        Q_OBJECT
    public:
        SetupAutofocusPage(QWidget* parent = nullptr);
        ~SetupAutofocusPage() override;

        auto Enter() -> void override;
        auto Leave() -> void override;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}