#pragma once
#include <memory>
#include <QList>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SetupPreviewPageControl {
    public:
        using Pair = QPair<int32_t, double>;

    public:
        SetupPreviewPageControl();
        ~SetupPreviewPageControl();

        auto MeasureExposure(QList<Pair>& values, double& coeffP, double& coeffQ)->bool;
        auto MeasureGain(QList<Pair>& values, double& coeffA, double& coeffB)->bool;
        auto Save(double coeffA, double coeffB, double coeffP, double coeffQ)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}