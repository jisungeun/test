#pragma once
#include <memory>
#include <QDialog>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class ExperimentSelectDialog : public QDialog {
        Q_OBJECT
    public:
        ExperimentSelectDialog(QWidget* parent = nullptr);
        ~ExperimentSelectDialog();

        auto GetProject() const->QString;
        auto GetExperiment() const->QString;

    protected slots:
        void onNew();
        void onSelected();
        void onProjectSelected(const QString& project);
        void onExperimentSelected(const QString& experiment);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}