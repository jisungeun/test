#pragma once

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationPage {
    public:
        EvaluationPage();
        virtual ~EvaluationPage();

        virtual auto Enter()->void = 0;
        virtual auto Leave()->void = 0;
    };
}