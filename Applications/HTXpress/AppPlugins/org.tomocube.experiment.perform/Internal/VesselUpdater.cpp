#include <QList>
#include "VesselUpdater.h"

#include "VesselObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct VesselUpdater::Impl {
        QList<VesselObserver*> observers;
    };

    VesselUpdater::VesselUpdater() : d{new Impl} {
    }

    VesselUpdater::~VesselUpdater() {
    }

    auto VesselUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new VesselUpdater() };
        return theInstance;
    }

    auto VesselUpdater::UpdateCurrentWell(AppEntity::WellIndex wellIdx) -> void {
        for(auto observer : d->observers) {
            observer->UpdateCurrentWell(wellIdx);
        }
    }

    auto VesselUpdater::UpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM) -> void {
        for(auto observer : d->observers) {
            observer->UpdateSafeMovingRange(axis, minMM, maxMM);
        }
    }

    auto VesselUpdater::Register(VesselObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto VesselUpdater::Deregister(VesselObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}
