#include <QStandardPaths>
#include <QDir>

#include <System.h>
#include <SystemStatus.h>
#include <TCFDataRepo.h>
#include <InstrumentController.h>
#include <AcquireImageController.h>
#include <SystemSetupController.h>
#include <DataMisc.h>

#include "Utility.h"
#include "InstrumentUpdater.h"
#include "RunExperimentUpdater.h"
#include "SetupBackgroundCalPageControl.h"

using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;
using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupBackgroundCalPageControl::Impl {
        double originalNA{ 0 };
        QString dataPath;
        QString tempPath;

        auto CurrentPosition() const->AppEntity::Position;
    };

    auto SetupBackgroundCalPageControl::Impl::CurrentPosition() const -> AppEntity::Position {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        return Utility::global2well(sysStatus->GetCurrentWell(), sysStatus->GetCurrentGlobalPosition());
    }

    SetupBackgroundCalPageControl::SetupBackgroundCalPageControl() : d{new Impl} {
    }

    SetupBackgroundCalPageControl::~SetupBackgroundCalPageControl() {
    }

    auto SetupBackgroundCalPageControl::GetNAs() const -> QList<double> {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        return sysConfig->GetIlluminationCalibrationParameterNAs();
    }

    auto SetupBackgroundCalPageControl::Parepare(double NA) const -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto experiment = sysStatus->GetExperiment();

        auto originalNA = Utility::Service::OverrideNA(experiment, NA);
        d->dataPath.clear();

        bool success = false;
        try {
            auto instUpdater = InstrumentUpdater::GetInstance();
            auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
            auto instController = Interactor::InstrumentController(&instPresenter);

            success = instController.PerformCondenserAutoFocus();
            if(success) {
                success = instController.CalibrateHTIllumination();
            }
        } catch(...) {
            success = false;
        }

        Utility::Service::OverrideNA(experiment, originalNA);

        return success;
    }

    auto SetupBackgroundCalPageControl::StartCalibrate(double NA) const -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto experiment = sysStatus->GetExperiment();

        auto updater = RunExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter::GetInstance(updater.get());
        auto controller = Interactor::AcquireImageController(presenter.get());

        d->originalNA = Utility::Service::OverrideNA(experiment, NA);
        d->dataPath.clear();

        const auto maxFov = AppEntity::System::GetMaximumFOV();
        controller.SetFieldOfView(maxFov.toUM().width, maxFov.toUM().height);

        using ImagingType = Interactor::AcquireImageController::ImagingType;
        ImagingType type;
        type.modality = AppEntity::Modality::HT;
        type.is3D = true;

        const auto pos = d->CurrentPosition();

        if(!controller.Acquire({type}, pos.toMM().x, pos.toMM().y, maxFov.toUM().width, maxFov.toUM().height)) {
            Utility::Service::OverrideNA(experiment, d->originalNA);
            return false;
        }

        return true;
    }

    auto SetupBackgroundCalPageControl::Calibrate(double NA) const -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto experiment = sysStatus->GetExperiment();

        Utility::Service::OverrideNA(experiment, d->originalNA);

        auto srcDir = QString("%1/data/0000/HT3D/0000").arg(d->dataPath);
        d->tempPath = QString("%1/.tsx.temp/bg/%2").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation)).arg(NA);
        if(!QFile::exists(d->tempPath)) QDir().mkpath(d->tempPath);

        auto controller = Interactor::SystemSetupController();
        return controller.GenerateBackground(srcDir, d->tempPath);
    }

    auto SetupBackgroundCalPageControl::Save(double NA) const -> bool {
        auto targetPath = AppEntity::System::GetBackgroundPath(NA);
        if(!QFile::exists(targetPath)) QDir().mkpath(targetPath);

        QDir targetDir(targetPath);
        for(auto idx=0; idx<4; idx++) {
            auto path = QString("%1.png").arg(idx);
            if(!QFile::exists(path)) continue;
            if(!targetDir.remove(path)) return false;
        }

        for(auto idx=0; idx<4; idx++) {
            auto srcPath = QString("%1/%2.png").arg(d->tempPath).arg(idx);
            auto dstPath = QString("%1/%2.png").arg(targetPath).arg(idx);
            if(!QFile(srcPath).copy(dstPath)) return false;
        }

        return true;
    }

    auto SetupBackgroundCalPageControl::LoadImage(int32_t index) -> QImage {
        if(d->tempPath.isEmpty() || !QFile::exists(d->tempPath)) return QImage();

        auto path = QString("%1/%2.png").arg(d->tempPath).arg(index);
        if(!QFile::exists(path)) return QImage();

        return QImage(path);
    }

    auto SetupBackgroundCalPageControl::SetLatestData(const QString& fileFullPath) -> bool {
        const auto systemStatus = AppEntity::SystemStatus::GetInstance();
        const auto dataRepo = TCFDataRepo::GetInstance();
        const auto user = DataMisc::GetUserName(fileFullPath);
        const auto project = DataMisc::GetProjectName(fileFullPath);
        const auto experiment = DataMisc::GetExperimentName(fileFullPath);
        const auto specimen = DataMisc::GetSpecimenName(fileFullPath);
        const auto well = DataMisc::GetWellName(fileFullPath);
        const auto dataName = DataMisc::GetTcfBaseName(fileFullPath);
        const auto acqData = dataRepo->GetData(user, project, experiment, specimen, well, dataName);

        if (acqData == nullptr) return false;

        d->dataPath = DataMisc::GetDataFolderPath(fileFullPath);

        return true;
    }
}
