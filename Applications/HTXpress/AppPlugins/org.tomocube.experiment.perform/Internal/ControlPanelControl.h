#pragma once
#include <memory>

#include <Area.h>
#include <TimelapseSequence.h>
#include <ScanConfig.h>
#include <ChannelConfig.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class ControlPanelControl {
    public:
        using Sequence = AppComponents::TimelapseImagingPanel::TimelapseSequence;

        enum class ImageType {
            HT3D,
            HT2D,
            FL3D,
            FL2D,
            BFGray,
            BFColor
        };

        struct FLScanConfig {
            AppEntity::FLScanMode scanMode{ AppEntity::FLScanMode::Default };
            double scanBottom;
            double scanTop;
            double scanStep;
            double flFocusOffset;
            int32_t scanSteps;
        };

    public:
        ControlPanelControl();
        ~ControlPanelControl();

        auto LoadingExperiment(const bool loading)->void;
        auto PerformHTIlluminationCalibration()->bool;

        auto AddCurrentPosition(const AppEntity::Area& area, bool isTile)->bool;
        auto AddCurrentTilePosition(double xInMm, double yInMm, const AppEntity::Area& area, bool isTile = true)->bool;
        auto AddMarkPosition(double xInMm, double yInMm, const AppEntity::Area& area, bool isTile)->bool;

        auto SetTileScanCondition(bool enable, double xInMm, double yInMm, double widthInUm, double heightInUm)->bool;

        auto SetFieldOfView(double xInUm, double yInUm)->bool;

        auto SetFLScanConfig(const FLScanConfig& uiConfig)->bool;
        auto GetFLScanConfig() const->FLScanConfig;

        auto ClearFLChannels()->void;
        auto SetFLChannel(const int32_t channel, const AppEntity::ChannelConfig& channelConfig)->bool;

        auto SetHTEnabled(const bool is3D, const bool enabled)->bool;
        auto SetFLEnabled(const bool is3D, const bool enabled)->bool;
        auto SetBFEnabled(const bool enabled)->bool;

        auto ReloadExperiment()->bool;

        auto ApplySequences(const QList<Sequence>& sequences)->bool;
        auto ValidateSequences(const QList<Sequence>& sequences, const UseCase::ImagingTimeTable::Pointer& timeTable,
                               QList<int32_t>& requiredTime)->bool;

        auto AcquireImage(const QList<ImageType>& images, double roiXInUm, double roiYInUm)->bool;
        auto AcquireImage(const QList<ImageType>& images, double xInMm, double yInMm, double roiXInUm, double roiYInUm)->bool;
        auto TestAcquire()->bool;
        auto RunExperiment()->bool;
        auto StopAcquisition()->bool;

        auto SetAcquisitionLock(bool locked)->bool;

        auto GetCurrentWellIndex() const->AppEntity::WellIndex;
        auto GetCurrentPositionInWell() const->AppEntity::Position;

        auto MoveZAxis(double posInMm)->bool;

        auto SetEstimated3DModeInterval(const QList<int32_t>& flEnabledChannels) -> bool;
        auto GetEstimated3DModeInterval(const UseCase::ImagingTimeTable::Pointer& timeTable)->AppComponents::TimelapseImagingPanel::TimelapseImagingTime;

        auto ChangeImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode) -> bool;
        auto UpdateRequiredSpace(const int32_t& acquisitionPoints) -> bool;

        auto SetAvailableStorageSpace(const int64_t& bytesAvailable) -> void;
        auto IsSpaceEnough(const int64_t& bytesRequired) const -> bool;
        auto TileDeactivation() -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
