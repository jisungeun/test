#include <QStringList>
#include <QFileInfo>
#include <QMap>

#include <System.h>
#include <SystemStatus.h>
#include <TCFDataRepo.h>
#include <QualityCheckController.h>
#include <InstrumentController.h>
#include <DataMisc.h>

#include "Utility.h"
#include "EvaluationReportWriter.h"
#include "EvaluationBeadUpdater.h"
#include "InstrumentUpdater.h"
#include "EvaluationSingleBeadPageControl.h"

using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;
using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationSingleBeadPageControl::Impl {
        QStringList dataPathList;
        QList<QPair<QString,QString>> dataTitleList;
        QString outputPath;

        struct {
            double average{ 0 };
            bool pass{ false };
            QList<Entity::BeadScore> scores;
        } result;

        auto MakeDecision()->void;
    };

    auto EvaluationSingleBeadPageControl::Impl::MakeDecision() -> void {
        const auto count = result.scores.size();
        if(count == 0) return;

        double sumValue = 0;
        for(auto idx=0; idx<count; ++idx) {
            sumValue += result.scores[idx].GetCorrelation();
        }

        result.average = sumValue / count;

        auto model = AppEntity::System::GetModel();
        auto scoreRef = model->EvaluationReference(AppEntity::EvaluationRef::BeadScoreThreshold);

        result.pass = (result.average > scoreRef);
    }

    EvaluationSingleBeadPageControl::EvaluationSingleBeadPageControl() : d{ std::make_unique<Impl>() } {
    }

    EvaluationSingleBeadPageControl::~EvaluationSingleBeadPageControl() {
    }

    auto EvaluationSingleBeadPageControl::GetScoreThreshold() const -> double {
        auto model = AppEntity::System::GetModel();
        return model->EvaluationReference(AppEntity::EvaluationRef::BeadScoreThreshold);
    }

    auto EvaluationSingleBeadPageControl::RunCAF() const -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto experiment = sysStatus->GetExperiment();

        bool success = false;
        try {
            auto instUpdater = InstrumentUpdater::GetInstance();
            auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
            auto instController = Interactor::InstrumentController(&instPresenter);

            success = instController.PerformCondenserAutoFocus();
        } catch(...) {
            success = false;
        }

        return success;
    }

    auto EvaluationSingleBeadPageControl::RunHTIllumCalibration() const -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto experiment = sysStatus->GetExperiment();

        bool success = false;
        try {
            auto instUpdater = InstrumentUpdater::GetInstance();
            auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
            auto instController = Interactor::InstrumentController(&instPresenter);

            if(instController.CalibrateHTIllumination()) {
                success = true;
            }
        } catch(...) {
            success = false;
        }

        return success;
    }

    auto EvaluationSingleBeadPageControl::AcquireData() -> bool {
        auto updater = EvaluationBeadUpdater::GetInstance();
        auto presenter = Interactor::QualityCheckPresenter::GetInstance(updater.get());
        auto controller = Interactor::QualityCheckController(presenter.get());
        return controller.AcquireAllPoints();
    }

    auto EvaluationSingleBeadPageControl::AddDataPath(const QString& fileFullPath) -> bool {
        const auto systemStatus = AppEntity::SystemStatus::GetInstance();
        const auto dataRepo = TCFDataRepo::GetInstance();
        const auto user = DataMisc::GetUserName(fileFullPath);
        const auto project = DataMisc::GetProjectName(fileFullPath);
        const auto experiment = DataMisc::GetExperimentName(fileFullPath);
        const auto specimen = DataMisc::GetSpecimenName(fileFullPath);
        const auto well = DataMisc::GetWellName(fileFullPath);
        const auto dataName = DataMisc::GetTcfBaseName(fileFullPath);
        const auto acqData = dataRepo->GetData(user, project, experiment, specimen, well, dataName);

        if (acqData == nullptr) return false;

        d->dataPathList.append(DataMisc::GetDataFolderPath(fileFullPath));

        auto strPoint = [=]()->QString {
            auto list = dataName.split(QRegExp(".T\\d+P"));
            if(list.isEmpty()) return "";
            return QString("P%1").arg(list.at(1));
        }();
        d->dataTitleList.append({well, strPoint});

        return true;
    }

    auto EvaluationSingleBeadPageControl::GetDataCount() const -> int32_t {
        return d->dataPathList.size();
    }

    auto EvaluationSingleBeadPageControl::GetDataTitle(int32_t index) const -> std::tuple<QString, QString> {
        if(index >= d->dataTitleList.size()) return std::make_tuple("","");
        return std::make_tuple(d->dataTitleList.at(index).first, d->dataTitleList.at(index).second);
    }

    auto EvaluationSingleBeadPageControl::StartEvaluation() -> void {
        auto updater = EvaluationBeadUpdater::GetInstance();
        auto presenter = Interactor::QualityCheckPresenter::GetInstance(updater.get());
        auto controller = Interactor::QualityCheckController(presenter.get());

        const auto fov = AppEntity::SystemStatus::GetInstance()->GetExperiment()->GetFOV().toUM();
        const auto resolution = AppEntity::System::GetCameraResolutionInUM();
        const auto xInPixel = static_cast<int32_t>((fov.width / 2.0) / resolution);
        const auto yInPixel = static_cast<int32_t>((fov.height / 2.0) / resolution);

        QList<Interactor::QualityCheckController::Data> dataList;
        for(auto path : d->dataPathList) {
            dataList.push_back({path, xInPixel, yInPixel});
        }

        d->result.scores = controller.EvaluateBeadScore(dataList);
        d->outputPath = controller.GetOutputPath();
        d->MakeDecision();
    }

    auto EvaluationSingleBeadPageControl::EvaluationResult() -> std::tuple<double, bool> {
        return std::make_tuple(d->result.average, d->result.pass);
    }

    auto EvaluationSingleBeadPageControl::GetScore(int32_t index) const -> Entity::BeadScore {
        if(index >= d->result.scores.size()) return Entity::BeadScore();
        return d->result.scores.at(index);
    }

    auto EvaluationSingleBeadPageControl::Save(const QString& path) const -> bool {
        QMap<QString, QVariant> results;
        results["NA"] = AppEntity::SystemStatus::GetInstance()->GetNA();
        for(auto idx=0; idx<d->result.scores.size(); ++idx) {
            auto well = d->dataTitleList.at(idx).first;
            auto point = d->dataTitleList.at(idx).second;
            results[QString("point_%1").arg(idx+1)] = QString("%1, %2").arg(well).arg(point);

            auto& score = d->result.scores.at(idx);
            results[QString("volume_%1").arg(idx+1)] = score.GetVolume();
            results[QString("drymass_%1").arg(idx+1)] = score.GetDrymass();
            results[QString("mean_dRI_%1").arg(idx+1)] = score.GetMeanDeltaRI();
            results[QString("correlation_%1").arg(idx+1)] = score.GetCorrelation();
        }
        results["average"] = d->result.average;
        results["pass"] = d->result.pass;

        auto writer = EvaluationReportWriter::GetInstance();
        writer->SetPath(path);
        writer->SetTitle("single_bead");
        if(!writer->SaveResults("report", results)) return false;
        if(!writer->CopyDirectory("raw_data", d->outputPath)) return false;

        return true;
    }

    auto EvaluationSingleBeadPageControl::Clear() -> void {
        d->dataPathList.clear();
        d->dataTitleList.clear();
        d->outputPath.clear();
        d->result.average = 0;
        d->result.pass = false;
        d->result.scores.clear();
    }
}
