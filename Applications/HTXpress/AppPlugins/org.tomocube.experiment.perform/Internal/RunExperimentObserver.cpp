#include "RunExperimentUpdater.h"
#include "RunExperimentObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    RunExperimentObserver::RunExperimentObserver(QObject* parent) : QObject(parent) {
        RunExperimentUpdater::GetInstance()->Register(this);
    }

    RunExperimentObserver::~RunExperimentObserver() {
        RunExperimentUpdater::GetInstance()->Deregister(this);
    }

    auto RunExperimentObserver::UpdateProgress(const UseCase::ExperimentStatus& status) -> void {
        const auto pos = status.GetCurrentPosition();
        emit sigUpdateProgress(status.GetProgress(), status.GetElapsedTime(), status.GetRemainTime());
        emit sigUpdateCurrentPosition(std::get<0>(pos), std::get<1>(pos));

        if(status.IsError()) {
            emit sigError(status.GetErrorMessage());
        } else {
            const auto message = status.GetStatusMessage();
            emit sigUpdateMessage(message);
        }
    }

    auto RunExperimentObserver::NotifyStopped() -> void {
        emit sigStopped();
    }
}
