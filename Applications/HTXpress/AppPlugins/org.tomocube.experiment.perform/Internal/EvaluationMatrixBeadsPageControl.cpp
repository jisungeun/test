#include <QStringList>
#include <QFileInfo>
#include <QMap>

#include <System.h>
#include <SystemStatus.h>
#include <TCFDataRepo.h>
#include <QualityCheckController.h>
#include <DataMisc.h>

#include "EvaluationReportWriter.h"
#include "EvaluationBeadUpdater.h"
#include "EvaluationMatrixBeadsPageControl.h"

using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;
using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationMatrixBeadsPageControl::Impl {
        QStringList dataPathList;
        QString outputPath;

        struct {
            int32_t xCount{ 1 };
            int32_t yCount{ 1 };
            int32_t xGap{ 0 };
            int32_t yGap{ 0 };
        } cond;

        struct {
            double cv{ 0 };
            bool pass{ false };
            QList<Entity::BeadScore> scores;
        } result;

        auto MakeDecision()->void;
    };

    auto EvaluationMatrixBeadsPageControl::Impl::MakeDecision() -> void {
        const auto count = result.scores.size();
        if(count == 0) return;

        double sumValue = 0;
        for(auto idx=0; idx<count; ++idx) {
            sumValue += result.scores[idx].GetCorrelation();
        }

        auto meanValue = sumValue / count;
        if(meanValue == 0) return;

        double stdValue = 0;
        for(auto idx=0; idx<count; ++idx) {
            stdValue += std::pow(result.scores[idx].GetCorrelation() - meanValue, 2);
        }

        result.cv = std::sqrt(stdValue / count / meanValue);

        auto model = AppEntity::System::GetModel();
        auto cvRef = model->EvaluationReference(AppEntity::EvaluationRef::BeadReproducibilityCV);

        result.pass = (result.cv < cvRef);
    }

    EvaluationMatrixBeadsPageControl::EvaluationMatrixBeadsPageControl() : d{ std::make_unique<Impl>()} {
    }

    EvaluationMatrixBeadsPageControl::~EvaluationMatrixBeadsPageControl() {
    }

    auto EvaluationMatrixBeadsPageControl::GetCVReference() const -> double {
        auto model = AppEntity::System::GetModel();
        return model->EvaluationReference(AppEntity::EvaluationRef::BeadReproducibilityCV);
    }

    auto EvaluationMatrixBeadsPageControl::GetTileCount() const -> int32_t {
        auto model = AppEntity::System::GetModel();
        const int32_t count = model->EvaluationReference(AppEntity::EvaluationRef::BeadReproducibilityMatrix);
        return std::floor(count/2)*2+1; //allow odd number only
    }

    auto EvaluationMatrixBeadsPageControl::GetGapX() const -> int32_t {
        auto model = AppEntity::System::GetModel();
        return model->EvaluationReference(AppEntity::EvaluationRef::BeadReproducibilityGapX);
    }

    auto EvaluationMatrixBeadsPageControl::GetGapY() const -> int32_t {
        auto model = AppEntity::System::GetModel();
        return model->EvaluationReference(AppEntity::EvaluationRef::BeadReproducibilityGapY);
    }

    auto EvaluationMatrixBeadsPageControl::AcquireData(int32_t xTiles, int32_t yTiles, 
                                                       int32_t xGapUm, int32_t yGapUm) -> bool {
        d->cond.xCount = xTiles;
        d->cond.yCount = yTiles;
        d->cond.xGap = xGapUm;
        d->cond.yGap = yGapUm;

        auto updater = EvaluationBeadUpdater::GetInstance();
        auto presenter = Interactor::QualityCheckPresenter::GetInstance(updater.get());
        auto controller = Interactor::QualityCheckController(presenter.get());
        return controller.AcquireMatrixPoints(xTiles, yTiles, xGapUm, yGapUm);
    }

    auto EvaluationMatrixBeadsPageControl::AddDataPath(const QString& dataPath) -> bool {
        const auto systemStatus = AppEntity::SystemStatus::GetInstance();
        const auto dataRepo = TCFDataRepo::GetInstance();
        const auto user = DataMisc::GetUserName(dataPath);
        const auto project = DataMisc::GetProjectName(dataPath);
        const auto experiment = DataMisc::GetExperimentName(dataPath);
        const auto specimen = DataMisc::GetSpecimenName(dataPath);
        const auto well = DataMisc::GetWellName(dataPath);
        const auto dataName = DataMisc::GetTcfBaseName(dataPath);
        const auto acqData = dataRepo->GetData(user, project, experiment, specimen, well, dataName);

        if (acqData == nullptr) return false;
        d->dataPathList.append(QFileInfo(dataPath).absolutePath());

        return true;
    }

    auto EvaluationMatrixBeadsPageControl::GetDataCount() const -> int32_t {
        return d->dataPathList.size();
    }

    auto EvaluationMatrixBeadsPageControl::StartEvaluation() -> void {
        auto updater = EvaluationBeadUpdater::GetInstance();
        auto presenter = Interactor::QualityCheckPresenter::GetInstance(updater.get());
        auto controller = Interactor::QualityCheckController(presenter.get());

        const auto fov = AppEntity::SystemStatus::GetInstance()->GetExperiment()->GetFOV().toUM();
        const auto resolution = AppEntity::System::GetCameraResolutionInUM();
        const auto xCenterInPixel = static_cast<int32_t>((fov.width / 2.0) / resolution);
        const auto yCenterInPixel = static_cast<int32_t>((fov.height / 2.0) / resolution);
        const auto xOffset = static_cast<int32_t>(std::floor(d->cond.xCount/2));
        const auto yOffset = static_cast<int32_t>(std::floor(d->cond.yCount/2));

        int32_t index = 0;
        QList<Interactor::QualityCheckController::Data> dataList;
        for(auto path : d->dataPathList) {
            const auto rowIdx = static_cast<int32_t>(index / d->cond.xCount);
            const auto colIdx = static_cast<int32_t>(index % d->cond.xCount);
            const auto yInPixel = yCenterInPixel + static_cast<int32_t>((rowIdx - yOffset) * d->cond.yGap / resolution);
            const auto xInPixel = xCenterInPixel + static_cast<int32_t>((colIdx - xOffset) * d->cond.xGap / resolution);

            dataList.push_back({path, xInPixel, yInPixel});
            index = index + 1;
        }

        d->result.scores = controller.EvaluateBeadScore(dataList);
        d->outputPath = controller.GetOutputPath();
        d->MakeDecision();
    }

    auto EvaluationMatrixBeadsPageControl::EvaluationResult() -> std::tuple<double, bool> {
        return std::make_tuple(d->result.cv, d->result.pass);
    }

    auto EvaluationMatrixBeadsPageControl::GetScore(int32_t index) const -> std::tuple<int32_t, int32_t, Entity::BeadScore> {
        const auto rowIdx = static_cast<int32_t>(index/d->cond.xCount) + 1;
        const auto colIdx = static_cast<int32_t>(index%d->cond.xCount) + 1;

        if(index >= d->result.scores.size()) return std::make_tuple(rowIdx, colIdx, Entity::BeadScore());
        return std::make_tuple(rowIdx, colIdx, d->result.scores.at(index));
    }

    auto EvaluationMatrixBeadsPageControl::Save(const QString& path) const -> bool {
        QMap<QString, QVariant> results;
        results["NA"] = AppEntity::SystemStatus::GetInstance()->GetNA();
        results["count_x"] = d->cond.xCount;
        results["count_y"] = d->cond.yCount;
        results["gap_x"] = d->cond.xGap;
        results["gap_y"] = d->cond.yGap;
        for(auto idx=0; idx<d->result.scores.size(); ++idx) {
            results[QString("row_%1").arg(idx+1)] = static_cast<int32_t>(idx/d->cond.xCount);
            results[QString("col_%1").arg(idx+1)] = static_cast<int32_t>(idx%d->cond.xCount);

            auto& score = d->result.scores.at(idx);
            results[QString("volume_%1").arg(idx+1)] = score.GetVolume();
            results[QString("drymass_%1").arg(idx+1)] = score.GetDrymass();
            results[QString("mean_dRI_%1").arg(idx+1)] = score.GetMeanDeltaRI();
            results[QString("correlation_%1").arg(idx+1)] = score.GetCorrelation();
        }
        results["cv"] = d->result.cv;
        results["pass"] = d->result.pass;

        auto writer = EvaluationReportWriter::GetInstance();
        writer->SetPath(path);
        writer->SetTitle("single_bead");
        if(!writer->SaveResults("report", results)) return false;
        if(!writer->CopyDirectory("raw_data", d->outputPath)) return false;

        return true;
    }

    auto EvaluationMatrixBeadsPageControl::Clear() -> void {
        d->dataPathList.clear();
        d->outputPath.clear();
        d->result.cv = 0;
        d->result.pass = false;
        d->result.scores.clear();
    }
}
