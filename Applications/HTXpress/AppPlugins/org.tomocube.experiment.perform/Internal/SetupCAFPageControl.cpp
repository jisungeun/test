#include <QStandardPaths>
#include <QDir>
#include <QSettings>

#include <System.h>
#include <SystemStatus.h>
#include <InstrumentController.h>
#include <SystemConfigController.h>
#include <MotionController.h>

#include "Utility.h"
#include "LiveImageSource.h"
#include "InstrumentUpdater.h"
#include "MotionUpdater.h"
#include "SetupCAFPageControl.h"

#include <QImage>

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupCAFPageControl::Impl {
        auto OverrideIntensity(double NA, int32_t intensity)->int32_t;
    };

    auto SetupCAFPageControl::Impl::OverrideIntensity(double NA, int32_t intensity) -> int32_t {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto[ptnIndex, originalIntensity, zOffset] = sysConfig->GetCondenserAFParameter(NA);
        sysConfig->SetCondenserAFParameter(NA, ptnIndex, intensity, zOffset);
        return originalIntensity;
    }

    SetupCAFPageControl::SetupCAFPageControl() :d {new Impl} {
    }
    
    SetupCAFPageControl::~SetupCAFPageControl() {
    }

    auto SetupCAFPageControl::GetNAs() const -> QList<double> {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        return sysConfig->GetIlluminationCalibrationParameterNAs();
    }

    auto SetupCAFPageControl::GetLEDIntensity(double NA) const -> int32_t {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto[ptnIndex, originalIntensity, zOffset] = sysConfig->GetCondenserAFParameter(NA);
        return originalIntensity;
    }

    auto SetupCAFPageControl::RunCAF(double NA, int32_t intensity) const -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto experiment = sysStatus->GetExperiment();

        auto originalNA = Utility::Service::OverrideNA(experiment, NA);
        auto originalIntensity = d->OverrideIntensity(NA, intensity);

        bool success = false;
        try {
            auto instUpdater = InstrumentUpdater::GetInstance();
            auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
            auto instController = Interactor::InstrumentController(&instPresenter);

            success = instController.PerformCondenserAutoFocus();
        } catch(...) {
            success = false;
        }

        Utility::Service::OverrideNA(experiment, originalNA);
        d->OverrideIntensity(NA, originalIntensity);

        return success;
    }

    auto SetupCAFPageControl::SaveLEDIntensity(double NA, int32_t intensity) const -> bool {
        d->OverrideIntensity(NA, intensity);

        auto controller = Interactor::SystemConfigController();
        return controller.Save();
    }

    auto SetupCAFPageControl::GetDiskImageCount() const -> int32_t {
        auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
        auto dirPath = QString("%1/condenserCal").arg(appDataPath);
        QDir dir(dirPath);
        return dir.entryList({"*.png"}, QDir::Files).size() / 2;
    }

    auto SetupCAFPageControl::LoadDiskImage(int32_t index, QImage& image, double& meanIntensity) const -> bool {
        auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
        auto imagePath = QString("%1/condenserCal/%2.png").arg(appDataPath).arg(index*2 + 1, 3, 10, QLatin1Char('0'));
        auto intensityPath = QString("%1/condenserCal/MeanIntensity.txt").arg(appDataPath);

        if(!QFile::exists(imagePath)) return false;

        QSettings qs(intensityPath, QSettings::IniFormat);
        meanIntensity = qs.value(QString("MeanIntensity/%1").arg(index), 0).toDouble();

        return image.load(imagePath);
    }

    auto SetupCAFPageControl::GetCAxisBaseline(double NA) const -> double {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto compensagedPos = sysStatus->GetCurrentCondensorPosition();

        auto[ptnIndex, intensity, offset] = sysConfig->GetCondenserAFParameter(NA);

        return compensagedPos - offset;
    }

    auto SetupCAFPageControl::GetCAxisCurrent() const -> double {
        auto controller = Interactor::MotionController(nullptr);

        double posInMM = 0;
        controller.GetGlobalPosition(AppEntity::Axis::C, posInMM);

        return posInMM;
    }

    auto SetupCAFPageControl::GetLatestImage(QImage& image) -> bool {
        return LiveImageSource::GetInstance()->GetLatestImage(image);
    }

    auto SetupCAFPageControl::MoveCAxis(double targetMm) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveCAxis(targetMm);
    }

    auto SetupCAFPageControl::SaveCAxisOffset(double NA, double offset) -> bool {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto NAs = sysConfig->GetCondenserAFParameterNAs();
        if(!NAs.contains(NA)) return false;

        auto[ptnIndex, intensity, originalOffset] = sysConfig->GetCondenserAFParameter(NA);
        sysConfig->SetCondenserAFParameter(NA, ptnIndex, intensity, offset);

        auto controller = Interactor::SystemConfigController();
        return controller.Save();
    }
}
