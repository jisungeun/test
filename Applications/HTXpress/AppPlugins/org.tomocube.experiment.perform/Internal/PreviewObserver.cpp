#include "PreviewUpdater.h"
#include "PreviewObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    PreviewObserver::PreviewObserver(QObject* parent) : QObject(parent) {
        PreviewUpdater::GetInstance()->Register(this);
    }

    PreviewObserver::~PreviewObserver() {
        PreviewUpdater::GetInstance()->Deregister(this);
    }

    auto PreviewObserver::Progress(double progress) -> void {
        emit sigUpdateProgress(progress);
    }

    auto PreviewObserver::UpdatePosition(const AppEntity::Position& position) -> void {
        emit sigUpdatePosition(position);
    }

    auto PreviewObserver::UpdateImageSize(int32_t xPixels, int32_t yPixels) -> void {
        emit sigUpdateImageSize(xPixels, yPixels);
    }

    auto PreviewObserver::UpdateBlock(int32_t xPos, int32_t yPos, const QImage& blockImage) -> void {
        emit sigUpdateBlock(xPos, yPos, blockImage);
    }

    auto PreviewObserver::SetCustomPreviewArea(double wellXinMM, double wellYinMM, int32_t widthInUm, int32_t heightInUm) -> void {
        emit sigUpdateCustomPreviewArea(wellXinMM, wellYinMM, widthInUm, heightInUm);
    }

    auto PreviewObserver::SetPreviewArea(double wellXinMM, double wellYinMM, double widthMM, double heightMM) -> void {
        emit sigSetPreviewArea(wellXinMM, wellYinMM, widthMM, heightMM);
    }

    auto PreviewObserver::StartCustomPreviewAreaSetting() -> void {
        emit sigStartCustomPreviewAreaSetting();
    }

    auto PreviewObserver::CancelCustomPreviewAreaSetting() -> void {
        emit sigCancelCustomPreviewAreaSetting();
    }
}
