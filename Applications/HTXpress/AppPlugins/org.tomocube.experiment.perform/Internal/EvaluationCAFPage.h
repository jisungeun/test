#pragma once
#include <memory>

#include <QWidget>

#include "EvaluationPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationCAFPage : public QWidget, public EvaluationPage {
        Q_OBJECT

    public:
        EvaluationCAFPage(QWidget* parent = nullptr);
        ~EvaluationCAFPage() override;

        auto Enter() -> void override;
        auto Leave() -> void override;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}