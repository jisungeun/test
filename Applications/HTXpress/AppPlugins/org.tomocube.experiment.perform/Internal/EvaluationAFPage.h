#pragma once
#include <memory>

#include <QWidget>

#include "EvaluationPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationAFPage : public QWidget, public EvaluationPage {
        Q_OBJECT

    public:
        EvaluationAFPage(QWidget* parent = nullptr);
        ~EvaluationAFPage() override;

        auto Enter() -> void override;
        auto Leave() -> void override;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}