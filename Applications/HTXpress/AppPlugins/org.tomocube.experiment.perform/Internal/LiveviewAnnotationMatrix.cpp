#include <QPen>
#include <QPainter>

#include "LiveviewAnnotationMatrix.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class GraphicsMatrixItem : public QGraphicsItem {
    public:
        GraphicsMatrixItem() : QGraphicsItem() {}
        ~GraphicsMatrixItem() override {}

        auto boundingRect() const -> QRectF override {
            const auto width = tile.width + (tile.gapW * matrix.cols);
            const auto height = tile.height + (tile.gapH * matrix.rows);
            return QRectF(0 - width/2, 0 - height/2, width, height);
        }

	    auto paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget) -> void override {
            Q_UNUSED(option)
            Q_UNUSED(widget)

            auto rect = boundingRect();

		    painter->setPen(options.pen);

            for(auto rowIdx=0; rowIdx<matrix.rows; rowIdx++) {
                const auto y0 = (rowIdx + 0.5) * tile.gapH;

                for(auto colIdx=0; colIdx<matrix.cols; colIdx++) {
                    const auto x0 = (colIdx + 0.5) * tile.gapW;

                    painter->drawRect(x0 - rect.width()/2, y0 - rect.height()/2, tile.width, tile.height);
                }
            }
        }

        auto SetTile(int32_t width, int32_t height, int32_t gapW, int32_t gapH)->void {
            tile.width = width;
            tile.height = height;
            tile.gapW = gapW;
            tile.gapH = gapH;
        }

        auto SetMatrix(int32_t cols, int32_t rows) -> void {
            matrix.cols = cols;
            matrix.rows = rows;
        }

        auto SetPen(const QPen& pen) {
            options.pen = pen;
        }

    private:
        struct {
            QPen pen{Qt::yellow, 0};
        } options;

        struct {
            int32_t width{ 0 };
            int32_t height{ 0 };
            int32_t gapW{ 0 };
            int32_t gapH{ 0 };
        } tile;

        struct {
            int32_t cols{ 1 };
            int32_t rows{ 1 };
        } matrix;
    };

    struct LiveviewAnnotationMatrix::Impl {
        LiveviewAnnotationMatrix* p {nullptr };
        GraphicsMatrixItem* item{ nullptr };

        Impl(LiveviewAnnotationMatrix* p) : p{ p } {
            item = new GraphicsMatrixItem();
            item->SetPen(QPen(Qt::yellow));
        }
    };

    LiveviewAnnotationMatrix::LiveviewAnnotationMatrix() : LiveviewAnnotationItem(), d{ std::make_unique<Impl>(this) } {
    }

    LiveviewAnnotationMatrix::~LiveviewAnnotationMatrix() {
    }

    auto LiveviewAnnotationMatrix::GetGraphicsItem() const -> QGraphicsItem* {
        return d->item;
    }

    auto LiveviewAnnotationMatrix::SetTile(int32_t width, int32_t height, int32_t gapW, int32_t gapH) -> void {
        d->item->SetTile(width, height, gapW, gapH);
    }

    auto LiveviewAnnotationMatrix::SetMatrix(int32_t cols, int32_t rows) -> void {
        d->item->SetMatrix(cols, rows);
    }
}
