#pragma once
#include <memory>

#include <QDialog>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationDialog : public QDialog {
        Q_OBJECT
    public:
        EvaluationDialog(QWidget* parent = nullptr);

    protected:
        void keyPressEvent(QKeyEvent* event) override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}