#pragma once
#include <memory>

#include <QObject>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationBeadObserver : public QObject {
        Q_OBJECT
    public:
        EvaluationBeadObserver(QObject* parent = nullptr);
        ~EvaluationBeadObserver() override;
        
        auto UpdateAcquisitionProgress(double progress)->void;
        auto NotifyAcquisitionError(const QString& error)->void;
        auto NotifyAcquisitionStopped()->void;
        auto UpdateEvaluationProgress(double progress)->void;
        
    signals:
        void sigUpdateAcquisitionProgress(double progress);
        void sigNotifyAcquisitionError(const QString& error);
        void sigNotifyAcquisitionStopped();
        void sigUpdateEvaluationProgress(double progress);
    };
}