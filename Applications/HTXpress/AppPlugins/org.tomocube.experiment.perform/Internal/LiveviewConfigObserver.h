#pragma once

#include <memory>

#include <QObject>

#include <ImagingConfig.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class LiveviewConfigObserver : public QObject {
        Q_OBJECT

    public:
        LiveviewConfigObserver(QObject* parent=nullptr);
        ~LiveviewConfigObserver() override;

        auto UpdateLiveConfig(const uint32_t intensity, const uint32_t exposure, const double gain)->void;
        auto UpdateAcquisitionConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain)->void;
        auto UpdateImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode)->void;

    signals:
        void sigLiveviewConfigChanged(const uint32_t intensity, const uint32_t exposure, const double gain);
        void sigAcquisitionConfigChanged(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain);
        void sigImagingSettingModeChanged(const AppEntity::ImagingSettingMode&);
    };
}