#include <Image2DProc.h>

#include <System.h>
#include <SystemStatus.h>
#include <InstrumentController.h>
#include <LiveviewConfigController.h>
#include <SystemConfigController.h>

#include "Utility.h"
#include "LiveImageSource.h"
#include "LiveviewConfigUpdater.h"
#include "InstrumentUpdater.h"
#include "SetupIntensityPageControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupIntensityPageControl::Impl {
    };

    SetupIntensityPageControl::SetupIntensityPageControl() : d{new Impl} {
    }

    SetupIntensityPageControl::~SetupIntensityPageControl() {
    }

    auto SetupIntensityPageControl::GetNAs() const -> QList<double> {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        return sysConfig->GetIlluminationCalibrationParameterNAs();
    }

    auto SetupIntensityPageControl::ChangeBFIntensity(int32_t value) -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        auto updater = LiveviewConfigUpdater::GetInstance();
        auto presenter = Interactor::LiveviewConfigPresenter(updater.get());
        auto controller = Interactor::LiveviewConfigController(&presenter);

        auto imagingMode = sysStatus->GetChannelConfig(AppEntity::ImagingMode::BFGray);
        auto intensity = value;
        auto exposure = imagingMode->GetCameraExposureUSec();

        if(controller.SetLiveImagingConfig(AppEntity::ImagingMode::BFGray, 
                                           intensity, 
                                           exposure, 
                                           0)) {
            auto instUpdater = InstrumentUpdater::GetInstance();
            auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
            auto instController = Interactor::InstrumentController(&instPresenter);

            return instController.ChangeLiveBF();
        }

        return false;
    }

    auto SetupIntensityPageControl::ChangeBFExposure(int32_t value) -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        auto updater = LiveviewConfigUpdater::GetInstance();
        auto presenter = Interactor::LiveviewConfigPresenter(updater.get());
        auto controller = Interactor::LiveviewConfigController(&presenter);

        auto imagingMode = sysStatus->GetChannelConfig(AppEntity::ImagingMode::BFGray);
        auto intensity = imagingMode->GetLightIntensity();
        auto exposure = value;

        if(controller.SetLiveImagingConfig(AppEntity::ImagingMode::BFGray, 
                                           intensity, 
                                           exposure, 
                                           0)) {
            auto instUpdater = InstrumentUpdater::GetInstance();
            auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
            auto instController = Interactor::InstrumentController(&instPresenter);

            return instController.ChangeLiveBF();
        }

        return false;
    }

    auto SetupIntensityPageControl::SaveBFLEDIntensity(int32_t intensity) -> bool {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        sysConfig->SetBFLightIntensity(intensity);

        const auto model = AppEntity::System::GetModel();
        const auto ratio = model->PreviewParameter(AppEntity::PreviewParam::IntensityScaleFactor).toDouble();
        sysConfig->SetPreviewLightIntensity(intensity * ratio);

        auto controller = Interactor::SystemConfigController();
        return controller.Save();
    }

    auto SetupIntensityPageControl::RunCAF(double NA) const -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto experiment = sysStatus->GetExperiment();

        auto originalNA = Utility::Service::OverrideNA(experiment, NA);

        bool success = false;
        try {
            auto instUpdater = InstrumentUpdater::GetInstance();
            auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
            auto instController = Interactor::InstrumentController(&instPresenter);

            success = instController.PerformCondenserAutoFocus();
        } catch(...) {
            success = false;
        }

        Utility::Service::OverrideNA(experiment, originalNA);
        return success;
    }

    auto SetupIntensityPageControl::ShowHTIlluminationSetupPattern(double NA, int32_t intensity) const -> bool {
        auto instUpdater = InstrumentUpdater::GetInstance();
        auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
        auto instController = Interactor::InstrumentController(&instPresenter);

        return instController.ShowHTIlluminationSetupPattern(NA, intensity);
    }

    auto SetupIntensityPageControl::GetLatestImage(QImage& image) -> bool {
        return LiveImageSource::GetInstance()->GetLatestImage(image);
    }

    auto SetupIntensityPageControl::SaveHTLEDIntensity(double NA, int32_t intensity) -> bool {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        if(!sysConfig->GetIlluminationCalibrationParameterNAs().contains(NA)) return false;

        auto [start, step, threshold] = sysConfig->GetIlluminationCalibrationParameter(NA);
        start = intensity;
        sysConfig->SetIlluminationCalibrationParameter(NA, start, step, threshold);

        auto controller = Interactor::SystemConfigController();
        return controller.Save();
    }

    auto SetupIntensityPageControl::CalculateAverage(QImage& image) -> double {
        return TC::Processing::Image2DProc::CalculateAverage(image);
    }
}
