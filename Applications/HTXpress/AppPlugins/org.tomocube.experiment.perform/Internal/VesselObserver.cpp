#include "VesselUpdater.h"
#include "VesselObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    VesselObserver::VesselObserver(QObject* parent) : QObject(parent) {
        VesselUpdater::GetInstance()->Register(this);
    }

    VesselObserver::~VesselObserver() {
        VesselUpdater::GetInstance()->Deregister(this);
    }

    auto VesselObserver::UpdateCurrentWell(AppEntity::WellIndex wellIdx) -> void {
        emit sigUpdateCurrentWell(wellIdx);
    }

    auto VesselObserver::UpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM) -> void {
        emit sigUpdateSafeMovingRange(axis, minMM, maxMM);
    }
}
