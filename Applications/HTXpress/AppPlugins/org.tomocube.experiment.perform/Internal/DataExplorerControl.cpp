#include <SystemStatus.h>

#include <InstrumentController.h>
#include <VesselController.h>
#include <AcquisitionDataController.h>
#include <AcquisitionDataPresenter.h>
#include <SystemStorageController.h>
#include <SystemStoragePresenter.h>
#include <DataMisc.h>

#include "InstrumentUpdater.h"
#include "DataExplorerControl.h"
#include "MotionUpdater.h"

#include "AcquisitionDataUpdater.h"
#include "SystemStorageUpdater.h"
#include "MotionObserver.h"

using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct DataExplorerControl::Impl {
        std::shared_ptr<Interactor::AcquisitionDataPresenter> acquisitionDataPresenter;
        Interactor::SystemStoragePresenter::Pointer storagePresenter{};

        bool vesselLoaded{true};
    };

    DataExplorerControl::DataExplorerControl() : d{new Impl} {
        d->acquisitionDataPresenter.reset(new Interactor::AcquisitionDataPresenter(AcquisitionDataUpdater::GetInstance().get()));
        d->storagePresenter = std::make_shared<Interactor::SystemStoragePresenter>(SystemStorageUpdater::GetInstance());
    }

    DataExplorerControl::~DataExplorerControl() {
    }

    auto DataExplorerControl::InstallAcquisitionDataMonitor() -> bool {
        auto controller = Interactor::AcquisitionDataController(d->acquisitionDataPresenter.get());
        return controller.InstallDataMonitor();
    }

    auto DataExplorerControl::ScanAcquisitionData() -> bool {
        auto controller = Interactor::AcquisitionDataController(d->acquisitionDataPresenter.get());
        if (!controller.InstallDataScanner()) return false;
        return controller.ScanExperiment();
    }

    auto DataExplorerControl::SetVesselLoadStatus(bool loaded) -> void {
        d->vesselLoaded = loaded;
    }

    auto DataExplorerControl::IsVesselLoaded() const -> bool {
        return d->vesselLoaded;
    }

    auto DataExplorerControl::LoadVessel() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);
        if(!controller.LoadVessel()) {
            controller.UnloadVessel();
            return false;
        }

        if(!controller.PerformCondenserAutoFocus()) return false;
        if(!controller.CalibrateHTIllumination()) return false;

        auto wellIndex = AppEntity::SystemStatus::GetInstance()->GetCurrentWell();
        if(wellIndex >= 0) {
            auto motionUpdater = MotionUpdater::GetInstance();
            auto motionPresenter = Interactor::MotionPresenter(motionUpdater.get());
            auto vesselController = Interactor::VesselController(&motionPresenter);

            if(!vesselController.SetCurrentWell(wellIndex)) return false;
        }

        return true;
    }

    auto DataExplorerControl::UnloadVessel() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);
        return controller.UnloadVessel();
    }

    auto DataExplorerControl::InstallStorageMonitor() -> bool {
        const auto controller = Interactor::SystemStorageController(d->storagePresenter);
        return controller.InstallStorageMonitor();
    }

    auto DataExplorerControl::GetStorageInformation() -> bool {
        const auto controller = Interactor::SystemStorageController(d->storagePresenter);
        return controller.UpdateStorageInfo();
    }

    auto DataExplorerControl::UpdateRootDrive() -> bool {
        const auto controller = Interactor::SystemStorageController(d->storagePresenter);
        return controller.UpdateRootDrive();
    }

    auto DataExplorerControl::UpdateMinRequiredSpace() -> bool {
        const auto controller = Interactor::SystemStorageController({});
        return controller.UpdateMinRequiredSpace();
    }

    auto DataExplorerControl::ParseUserName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetUserName(fileFullPath);
    }

    auto DataExplorerControl::ParseProjectName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetProjectName(fileFullPath);
    }

    auto DataExplorerControl::ParseExperimentName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetExperimentName(fileFullPath);
    }

    auto DataExplorerControl::ParseSpecimenName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetSpecimenName(fileFullPath);
    }

    auto DataExplorerControl::ParseWellName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetWellName(fileFullPath);
    }

    auto DataExplorerControl::ParseDataName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetTcfBaseName(fileFullPath);
    }
}
