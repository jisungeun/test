#pragma once
#include <memory>

#include <QObject>
#include <IAcqPositionView.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class AcquisitionPositionObserver : public QObject {
        Q_OBJECT
    public:
        using LocationIndex = AppEntity::LocationIndex;
        using Location = AppEntity::Location;
        using WellIndex = AppEntity::WellIndex;

    public:
        AcquisitionPositionObserver(QObject* parent = nullptr);
        ~AcquisitionPositionObserver() override;

        auto AddPosition(LocationIndex locationIdx, WellIndex wellIndex, Location::Pointer loc)->void;
        auto RefreshList(WellIndex wellIndex, QMap<LocationIndex, Location::Pointer>& locList)->void;
        auto DeletePosition(WellIndex wellIdx, LocationIndex locationIdx) -> void;

    signals:
        void sigAddPosition(AppEntity::LocationIndex locationIdx, AppEntity::WellIndex wellIndex, AppEntity::Location::Pointer loc);
        void sigRefreshList(AppEntity::WellIndex wellIndex, QMap<AppEntity::LocationIndex, AppEntity::Location::Pointer>& locList);
        void sigDeletePosition(AppEntity::WellIndex wellIdx, AppEntity::LocationIndex locationIdx);
    };
}