#include "InstrumentUpdater.h"
#include "InstrumentObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    InstrumentObserver::InstrumentObserver(QObject* parent) : QObject(parent) {
        InstrumentUpdater::GetInstance()->Register(this);
    }

    InstrumentObserver::~InstrumentObserver() {
        InstrumentUpdater::GetInstance()->Deregister(this);
    }

    auto InstrumentObserver::UpdateFailed(const QString& message) -> void {
        emit sigFailed(message);
    }

    auto InstrumentObserver::UpdateProgress(double progress, const QString& message) -> void {
        emit sigProgress(progress, message);
    }

    auto InstrumentObserver::UpdateGlobalPosition(const AppEntity::Position& position) -> void {
        emit sigUpdateGlobalPosition(position);
    }

    auto InstrumentObserver::ReportAFFailed() -> void {
        emit sigAFFailed();
    }

    auto InstrumentObserver::UpdateBestFocus(double posInMm) -> void {
        emit sigUpdateBestFocus(posInMm);
    }

    auto InstrumentObserver::EnableAutofocus(bool enable) -> void {
        emit sigAFEnabled(enable);
    }

    auto InstrumentObserver::LiveStarted() -> void {
        emit sigLiveStarted();
    }

    auto InstrumentObserver::LiveStopped() -> void {
        emit sigLiveStopped();
    }

    auto InstrumentObserver::LiveImagingFailed(const QString& message) -> void {
        emit sigLiveImagingFailed(message);
    }

    auto InstrumentObserver::SetVesselLoaded(bool loaded) -> void {
        emit sigVesselLoaded(loaded);
    }

    auto InstrumentObserver::UpdateCAFScores(const QList<double>& scores, int32_t bestFocusIndex) -> void {
        emit sigUpdateCAFScores(scores, bestFocusIndex);
    }

    auto InstrumentObserver::UpdateHTIlluminationResult(const QImage& image) -> void {
        emit sigUpdateHTIlluminationResult(image);
    }
}
