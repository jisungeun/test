#include "ImagingPlanUpdater.h"
#include "ImagingPlanObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    ImagingPlanObserver::ImagingPlanObserver(QObject* parent) : QObject(parent) {
        ImagingPlanUpdater::GetInstance()->Register(this);
    }

    ImagingPlanObserver::~ImagingPlanObserver() {
        ImagingPlanUpdater::GetInstance()->Deregister(this);
    }

    auto ImagingPlanObserver::Update(const UseCase::ImagingTimeTable::Pointer& table) -> void {
        emit sigUpdate(table);
    }

    auto ImagingPlanObserver::Update3DMinimumInterval(const UseCase::ImagingTimeTable::Pointer& table) -> void {
        emit sigUpdate3DMinimumInterval(table);
    }

    auto ImagingPlanObserver::UpdateFOV(const double xInUm, const double yInUm) -> void {
        emit sigUpdateFOV(xInUm, yInUm);
    }

    auto ImagingPlanObserver::UpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex,
                                                 double xInMm, double yInMm, 
                                                 double widthInUm, double heightInUm) -> void {
        emit sigUpdateTileScanArea(enable, wellIndex, xInMm, yInMm, widthInUm, heightInUm);
    }

    auto ImagingPlanObserver::ClearFLChannels() -> void {
        emit sigClearFLChannels();
    }

    auto ImagingPlanObserver::UpdateBFEnabled(bool enabled) -> void {
        emit sigUpdateBFEnabled(enabled);
    }

    auto ImagingPlanObserver::UpdateHTEnabled(bool enabled) -> void {
        emit sigUpdateHTEnabled(enabled);
    }

    auto ImagingPlanObserver::UpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types) -> void {
        emit sigUpdateEnabledImagingTypes(types);
    }

    auto ImagingPlanObserver::UpdateFLChannelConfig(const int32_t channel, const AppEntity::ChannelConfig::Pointer config) -> void {
        emit sigUpdateFLChannelConfig(channel, config);
    }

    auto ImagingPlanObserver::UpdateAcquisitionLock(bool locked) -> void {
        emit sigUpdateAcquisitionLock(locked);
    }

    auto ImagingPlanObserver::UpdateAcquisitionDataRequiredSpace(const int64_t& bytes) -> void {
        emit sigUpdateAcquisitionDataRequiredSpace(bytes);
    }

    auto ImagingPlanObserver::UpdateTileActivation(bool enable) -> void {
        emit sigUpdateTileActivation(enable);
    }
}
