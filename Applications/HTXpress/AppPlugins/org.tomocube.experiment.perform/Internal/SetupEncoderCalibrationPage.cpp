#include <QTimer>

#include <GraphicsCrossItem.h>
#include <MessageDialog.h>

#include "MotionObserver.h"
#include "SetupEncoderCalibrationPageControl.h"
#include "SetupEncoderCalibrationPage.h"
#include "ui_SetupEncoderCalibrationPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    using CalPosition = SetupEncoderCalibrationPageControl::Position;

    struct SetupEncoderCalibrationPage::Impl {
        SetupEncoderCalibrationPageControl control;
        Ui::SetupEncoderCalibrationPage ui;
        SetupEncoderCalibrationPage* p{ nullptr };
        QTimer updateTimer;
        TC::Widgets::GraphicsCrossItem* fovCenterMark{ nullptr };
        TC::Widgets::GraphicsCrossItem* crossCenterMark{ nullptr };
        QImage latestImage;
        QSize imageSize;

        Impl(SetupEncoderCalibrationPage* p) : p{ p } {}
        auto InitUi()->void;
        auto Reset()->void;
        auto StartUpdate(bool start)->void;
        auto UpdateImage()->void;
        auto DisableAutofocus()->void;
        auto EnableFind(CalPosition index)->void;
        auto FindCenter(CalPosition index)->void;
        auto EnableCompensation()->void;
        auto Compensate()->void;
        auto UpdateCompensated(bool compensated)->void;
        auto CheckMove(CalPosition index)->void;
        auto Save()->void;
    };

    auto SetupEncoderCalibrationPage::Impl::InitUi() -> void {
        ui.imageView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.imageView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        ui.findCenterP1Btn->setDisabled(true);
        ui.findCenterP2Btn->setDisabled(true);
        ui.findCenterP3Btn->setDisabled(true);
        ui.compensateBtn->setDisabled(true);
        ui.compensationBox->setDisabled(true);
        ui.saveBtn->setDisabled(true);
        ui.savedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.savedLabel->hide();

        fovCenterMark = new TC::Widgets::GraphicsCrossItem();
        fovCenterMark->SetLength(100);
        fovCenterMark->SetThickness(5);
        fovCenterMark->SetColor(0x00C2D3);
        ui.imageView->AddGraphicsItem(fovCenterMark);

        crossCenterMark = new TC::Widgets::GraphicsCrossItem();
        crossCenterMark->SetLength(100);
        crossCenterMark->SetThickness(5);
        crossCenterMark->SetColor(Qt::yellow);
        crossCenterMark->hide();
        ui.imageView->AddGraphicsItem(crossCenterMark);
    }

    auto SetupEncoderCalibrationPage::Impl::Reset() -> void {
        control.Clear();

        ui.xRatio->setValue(1.0);
        ui.yRatio->setValue(1.0);

        EnableCompensation();
        UpdateCompensated(false);
    }

    auto SetupEncoderCalibrationPage::Impl::StartUpdate(bool start) -> void {
        if(start) updateTimer.start(200);
        else updateTimer.stop();
    }

    auto SetupEncoderCalibrationPage::Impl::UpdateImage() -> void {
        if(!control.GetLatestImage(latestImage)) return;
        ui.imageView->ShowImage(latestImage);

        if(imageSize != latestImage.size()) {
            imageSize = latestImage.size();
            fovCenterMark->SetCenter(imageSize.width()/2, imageSize.height()/2);
            ui.imageView->FitZoom();
        }
    }

    auto SetupEncoderCalibrationPage::Impl::DisableAutofocus() -> void {
        control.DisableAutofocus();
        control.LowerZStage(110);

        ui.moveP1Btn->setDisabled(false);
        ui.moveP2Btn->setDisabled(false);
        ui.moveP3Btn->setDisabled(false);
    }

    auto SetupEncoderCalibrationPage::Impl::EnableFind(CalPosition index) -> void {
        ui.findCenterP1Btn->setEnabled(CalPosition::P1 == index);
        ui.findCenterP2Btn->setEnabled(CalPosition::P2 == index);
        ui.findCenterP3Btn->setEnabled(CalPosition::P3 == index);
    }

    auto SetupEncoderCalibrationPage::Impl::FindCenter(CalPosition index) -> void {
        auto [xPixels, yPixels] = control.FindCenter(index, latestImage);
        if((xPixels<0) || (yPixels<0)) {
            ui.saveBtn->setDisabled(true);
            crossCenterMark->hide();
            TC::MessageDialog::warning(p, "Setup", tr("It fails to find center of the cross mark"));
        } else {
            crossCenterMark->SetCenter(xPixels, yPixels);
            crossCenterMark->show();
            EnableCompensation();
        }
    }

    auto SetupEncoderCalibrationPage::Impl::EnableCompensation() -> void {
        ui.compensateBtn->setEnabled(control.IsAllFound());
    }

    auto SetupEncoderCalibrationPage::Impl::Compensate() -> void {
        crossCenterMark->hide();
        ui.saveBtn->setDisabled(true);

        if(!control.Compensate()) {
            TC::MessageDialog::warning(p, "Setup", tr("Encoder compensation is failed"));
            return;
        }

        const auto[xRatio, yRatio] = control.GetCompensation();
        ui.xRatio->setValue(xRatio);
        ui.yRatio->setValue(yRatio);

        UpdateCompensated(true);
    }

    auto SetupEncoderCalibrationPage::Impl::UpdateCompensated(bool compensated) -> void {
        ui.compensationBox->setEnabled(compensated);
        ui.saveBtn->setEnabled(compensated);

        ui.p1Box->setDisabled(compensated);
        ui.p2Box->setDisabled(compensated);
        ui.p3Box->setDisabled(compensated);
    }

    auto SetupEncoderCalibrationPage::Impl::CheckMove(CalPosition index) -> void {
        control.CheckMove(index);
    }

    auto SetupEncoderCalibrationPage::Impl::Save() -> void {
        if(!control.Save()) return;

        ui.savedLabel->show();
        QTimer::singleShot(2000, [this]() { ui.savedLabel->hide(); });
    }

    SetupEncoderCalibrationPage::SetupEncoderCalibrationPage(QWidget* parent) : QWidget(parent), d{ std::make_unique<Impl>(this) } {
        d->ui.setupUi(this);
        d->InitUi();

        connect(&d->updateTimer, &QTimer::timeout, this, [this]() {
            d->UpdateImage();
        });

        connect(d->ui.disableAFBtn, &QPushButton::clicked, this, [this]() {
            d->DisableAutofocus();
        });

        connect(d->ui.moveP1Btn, &QPushButton::clicked, this, [this]() {
            d->EnableFind(CalPosition::P1);
            d->control.MovePoint(CalPosition::P1);
        });

        connect(d->ui.moveP2Btn, &QPushButton::clicked, this, [this]() {
            d->EnableFind(CalPosition::P2);
            d->control.MovePoint(CalPosition::P2);
        });

        connect(d->ui.moveP3Btn, &QPushButton::clicked, this, [this]() {
            d->EnableFind(CalPosition::P3);
            d->control.MovePoint(CalPosition::P3);
        });

        connect(d->ui.findCenterP1Btn, &QPushButton::clicked, this, [this]() {
            d->FindCenter(CalPosition::P1);
        });

        connect(d->ui.findCenterP2Btn, &QPushButton::clicked, this, [this]() {
            d->FindCenter(CalPosition::P2);
        });

        connect(d->ui.findCenterP3Btn, &QPushButton::clicked, this, [this]() {
            d->FindCenter(CalPosition::P3);
        });

        connect(d->ui.compensateBtn, &QPushButton::clicked, this, [this]() {
            d->Compensate();
        });

        connect(d->ui.checkP1Btn, &QPushButton::clicked, this, [this]() {
            d->CheckMove(CalPosition::P1);
        });

        connect(d->ui.checkP2Btn, &QPushButton::clicked, this, [this]() {
            d->CheckMove(CalPosition::P2);
        });

        connect(d->ui.checkP3Btn, &QPushButton::clicked, this, [this]() {
            d->CheckMove(CalPosition::P3);
        });

        connect(d->ui.checkP4Btn, &QPushButton::clicked, this, [this]() {
            d->CheckMove(CalPosition::P4);
        });

        connect(d->ui.checkCenterBtn, &QPushButton::clicked, this, [this]() {
            d->CheckMove(CalPosition::Center);
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this]() {
            d->Save();
        });

        connect(d->ui.resetBtn, &QPushButton::clicked, this, [this]() {
            d->Reset();
        });

        d->ui.widget->setObjectName("panel");

        for (const auto& label : findChildren<QLabel*>()) {
            if(label->objectName().contains("subtitle")) {
                label->setObjectName("label-h5");
            } else if (label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
        }

        for (const auto& button : findChildren<QPushButton*>()) {
            if(button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            } else {
                button->setObjectName("bt-maintenance-light");
            }
        }

        for (const auto& groupBox : findChildren<QGroupBox*>()) {
            groupBox->setObjectName("gb-maintenance-main");
        }   
    }

    SetupEncoderCalibrationPage::~SetupEncoderCalibrationPage() {
    }

    auto SetupEncoderCalibrationPage::Enter() -> void {
        d->control.StoreOriginal();
        d->Reset();
        d->StartUpdate(true);
    }

    auto SetupEncoderCalibrationPage::Leave() -> void {
        d->control.RestoreOriginal();
        d->Reset();
        d->StartUpdate(false);
    }

    void SetupEncoderCalibrationPage::resizeEvent(QResizeEvent* event) {
        QWidget::resizeEvent(event);
    }
}
