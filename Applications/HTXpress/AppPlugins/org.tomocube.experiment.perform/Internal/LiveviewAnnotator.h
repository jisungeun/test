#pragma once
#include <memory>

#include "LiveViewer.h"
#include "LiveviewAnnotationItem.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class LiveviewAnnotator {
    public:
        using Pointer = std::shared_ptr<LiveviewAnnotator>;

    protected:
        LiveviewAnnotator();

    public:
        ~LiveviewAnnotator();

        static auto GetInstance()->Pointer;
        auto InstallViewer(LiveViewer* viewer)->void;

        auto AddItem(LiveviewAnnotationItem* item, int32_t xInPixel, int32_t yInPixel)->void;
        auto RemoveItem(LiveviewAnnotationItem* item)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}