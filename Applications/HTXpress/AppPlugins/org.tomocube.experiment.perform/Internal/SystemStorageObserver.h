﻿#pragma once
#include <memory>

#include <QObject>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SystemStorageObserver final : public QObject {
        Q_OBJECT
    public:
        using Self = SystemStorageObserver;
        using Pointer = std::shared_ptr<Self>;

        explicit SystemStorageObserver(QObject* parent = nullptr);
        ~SystemStorageObserver() override;

        auto UpdateSystemStorageSpace(const int64_t& totalBytes, const int64_t& availableBytes) -> void;
        auto UpdateMinRequiredSpace(const int32_t& gigabytes) -> void;

    signals:
        void sigUpdateSystemStorageSpace(const int64_t& totalBytes, const int64_t& availableBytes);
        void sigUpdateMinRequiredSpace(const int32_t& gigabytes);
    };
}
