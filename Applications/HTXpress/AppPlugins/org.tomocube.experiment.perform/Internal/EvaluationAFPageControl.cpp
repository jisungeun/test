#include <QList>
#include <QElapsedTimer>
#include <QVariant>

#include <System.h>
#include <MotionController.h>
#include <InstrumentController.h>

#include "EvaluationReportWriter.h"
#include "MotionUpdater.h"
#include "InstrumentUpdater.h"
#include "LiveImageSource.h"
#include "EvaluationAFPageControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationAFPageControl::Impl {
        struct {
            QList<QImage> images;
            QList<double> positions;
            double difference{ 0 };
            bool pass{ false };
        } result;

        auto GetImage() const->QImage;
        auto GetZPosition(double& position) const->bool;
        auto MoveZ(double targetMM) const->bool;
        auto UpdatePosition()->bool;
        auto UpdateImage()->bool;
        auto PerformAF() const->bool;
        auto Calculate()->void;
    };

    auto EvaluationAFPageControl::Impl::GetImage() const -> QImage {
        QImage image;

        QElapsedTimer timer;
        timer.start();

        while(image.isNull() && (timer.elapsed() < 2000)) {
              if(LiveImageSource::GetInstance()->GetLatestImage(image)) break;
        }

        return image;
    }

    auto EvaluationAFPageControl::Impl::GetZPosition(double& position) const -> bool {
        auto controller = Interactor::MotionController();
        return controller.GetGlobalPosition(AppEntity::Axis::Z, position);
    }

    auto EvaluationAFPageControl::Impl::MoveZ(double targetMM) const -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveMM(AppEntity::Axis::Z, targetMM);
    }

    auto EvaluationAFPageControl::Impl::UpdatePosition() -> bool {
        double zpos = 0;
        if(!GetZPosition(zpos)) return false;
        result.positions.push_back(zpos);
        return true;
    }

    auto EvaluationAFPageControl::Impl::UpdateImage() -> bool {
        auto image = GetImage();
        if(image.isNull()) return false;
        result.images.push_back(image);
        return true;
    }

    auto EvaluationAFPageControl::Impl::PerformAF() const -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);
        return controller.PerformAutoFocus();
    }

    auto EvaluationAFPageControl::Impl::Calculate() -> void {
        auto model = AppEntity::System::GetModel();
        auto gap = model->EvaluationReference(AppEntity::EvaluationRef::AFDistanceGap);

        auto minv = std::min(result.positions.at(0), result.positions.at(1));
        minv = std::min(minv, result.positions.at(2));

        auto maxv = std::max(result.positions.at(0), result.positions.at(1));
        maxv = std::max(maxv, result.positions.at(2));

        result.difference = (maxv - minv) * 1000;
        result.pass = (result.difference <= gap);
    }

    EvaluationAFPageControl::EvaluationAFPageControl() : d{ std::make_unique<Impl>() }{
    }

    EvaluationAFPageControl::~EvaluationAFPageControl() {
    }

    auto EvaluationAFPageControl::GetReference() const -> double {
        auto model = AppEntity::System::GetModel();
        return model->EvaluationReference(AppEntity::EvaluationRef::AFDistanceGap);
    }

    auto EvaluationAFPageControl::Clear() -> void {
        d->result.images.clear();
        d->result.positions.clear();
        d->result.difference = 0;
        d->result.pass = 0;
    }

    auto EvaluationAFPageControl::GetImage(int32_t index) const -> QImage {
        if(index >= d->result.images.size()) return QImage();
        return d->result.images.at(index);
    }

    auto EvaluationAFPageControl::Evaluate() -> std::tuple<double, bool> {
        auto model = AppEntity::System::GetModel();
        auto belowOffset = model->EvaluationReference(AppEntity::EvaluationRef::AFPositionBelow);
        auto aboveOffset = model->EvaluationReference(AppEntity::EvaluationRef::AFPositionAbove);

        if(!d->UpdatePosition() || !d->UpdateImage()) return std::make_tuple(0, false);
        const auto start = d->result.positions.at(0);

        if(!d->MoveZ(start + belowOffset)) return std::make_tuple(0, false);

        if(!d->PerformAF()) return std::make_tuple(0, false);

        if(!d->UpdatePosition() || !d->UpdateImage()) return std::make_tuple(0, false);

        if(!d->MoveZ(start + aboveOffset)) return std::make_tuple(0, false);

        if(!d->PerformAF()) return std::make_tuple(0, false);

        if(!d->UpdatePosition() || !d->UpdateImage()) return std::make_tuple(0, false);

        d->Calculate();

        return std::make_tuple(d->result.difference, d->result.pass);
    }

    auto EvaluationAFPageControl::Save(const QString& path) -> bool {
        QMap<QString, QVariant> results;
        for(auto idx=0; idx<d->result.positions.size(); idx++) {
            results[QString("Z%1").arg(idx)] = d->result.positions.at(idx);
        }
        results["pass"] = d->result.pass;
        results["difference"] = d->result.difference;

        auto writer = EvaluationReportWriter::GetInstance();
        writer->SetPath(path);
        writer->SetTitle("autofocus");
        if(!writer->SaveResults("report", results)) return false;

        for(auto idx=0; idx<d->result.images.size(); idx++) {
            if(!writer->SaveImage(QString("image_%1").arg(idx), d->result.images.at(idx))) return false;
        }

        return true;
    }
}
