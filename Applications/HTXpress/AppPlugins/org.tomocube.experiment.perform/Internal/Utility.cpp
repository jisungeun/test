#include <System.h>
#include <SystemStatus.h>

#include "Utility.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Utility {
    auto pixel2um(int32_t pixels) -> double {
        return pixels * AppEntity::System::GetCameraResolutionInUM();
    }

    auto um2pixel(double dist) -> int32_t {
        return dist / AppEntity::System::GetCameraResolutionInUM();
    }

    auto global2vessel(const AppEntity::Position& globalPos) -> AppEntity::Position {
        auto systemCenter = AppEntity::System::GetSystemCenter();
        return globalPos - systemCenter;
    }

    auto well2global(const AppEntity::WellIndex& wellIdx, const AppEntity::Position& position) -> AppEntity::Position {
        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        auto systemCenter = AppEntity::System::GetSystemCenter();
        auto vessel = experiment->GetVessel();
        auto wellPosition = vessel->GetWellPosition(wellIdx);
        return systemCenter + wellPosition + position;
    }

    auto global2well(const AppEntity::WellIndex& wellIdx, const AppEntity::Position& globalPos) -> AppEntity::Position {
        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        auto systemCenter = AppEntity::System::GetSystemCenter();
        auto vessel = experiment->GetVessel();
        auto wellPosition = vessel->GetWellPosition(wellIdx);
        return globalPos - wellPosition - systemCenter;
    }

    auto well2vessel(const AppEntity::WellIndex& wellIdx, const AppEntity::Position& position) -> AppEntity::Position {
        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        auto vessel = experiment->GetVessel();
        auto wellPosition = vessel->GetWellPosition(wellIdx);
        return wellPosition + position;
    }

    auto vessel2well(const AppEntity::WellIndex& wellIdx, const AppEntity::Position& globalPos) -> AppEntity::Position {
        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        auto vessel = experiment->GetVessel();
        auto wellPosition = vessel->GetWellPosition(wellIdx);
        return globalPos - wellPosition;
    }

    auto GetExChannel(int32_t waveLength, int32_t bandWidth) -> int32_t {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto& channelIndexes = sysConfig->GetFLActiveExcitations();

        for(auto channelIndex : channelIndexes) {
            AppEntity::FLFilter filter;
            sysConfig->GetFLActiveExcitation(channelIndex, filter);
            if((filter.GetWaveLength() == waveLength) && (filter.GetBandwidth() == bandWidth)) {
                return channelIndex;
            }
        }

        return -1;
    }

    auto GetEmChannel(int32_t waveLength, int32_t bandWidth) -> int32_t {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto& channelIndexes = sysConfig->GetFLEmissions();

        for(auto channelIndex : channelIndexes) {
            AppEntity::FLFilter filter;
            sysConfig->GetFLEmission(channelIndex, filter);
            if((filter.GetWaveLength() == waveLength) && (filter.GetBandwidth() == bandWidth)) {
                return channelIndex;
            }
        }

        return -1;
    }

    auto Service::OverrideNA(AppEntity::Experiment::Pointer& experiment, double NA) -> double {
        auto vessel = experiment->GetVessel();
        auto prevNA = vessel->GetNA();
        vessel->SetNA(NA);

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto htScanParam = sysConfig->GetHTScanParameter(NA);
        const auto htScanStep = std::get<0>(htScanParam);
        const auto htScanSlices = std::get<1>(htScanParam);
        const auto halfPulses = (htScanStep * htScanSlices) / 2;

        AppEntity::ScanConfig ht3DConfig;
        ht3DConfig.SetRange(-halfPulses, halfPulses, htScanStep);
        ht3DConfig.SetSteps(htScanSlices);
        sysStatus->SetScanConfig(AppEntity::ImagingMode::HT3D, ht3DConfig);

        return prevNA;
    }
}
