#include <QList>

#include "AcquisitionDataObserver.h"
#include "AcquisitionDataUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct AcquisitionDataUpdater::Impl {
        QList<AcquisitionDataObserver*> observers;
    };

    AcquisitionDataUpdater::AcquisitionDataUpdater() : IAcquisitionDataView(), d{ new Impl } {
    }

    AcquisitionDataUpdater::~AcquisitionDataUpdater() {
    }

    auto AcquisitionDataUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new AcquisitionDataUpdater() };
        return theInstance;
    }

    auto AcquisitionDataUpdater::ScannedData(const QString& user, const QString& project, const QString& experiment) -> void {
        for(auto observer : d->observers) {
            observer->ScannedData(user, project, experiment);
        }
    }

    auto AcquisitionDataUpdater::AddedData(const QString& fileFullPath) -> void {
        for(auto observer : d->observers) {
            observer->AddedData(fileFullPath);
        }
    }

    auto AcquisitionDataUpdater::UpdatedData(const QString& fileFullPath) -> void {
        for(auto observer : d->observers) {
            observer->UpdatedData(fileFullPath);
        }
    }

    auto AcquisitionDataUpdater::DeletedData(const QString& fileFullPath) -> void {
        for(auto observer : d->observers) {
            observer->DeletedData(fileFullPath);
        }
    }

    auto AcquisitionDataUpdater::DeletedDataRootFolder(const QString& fileFullPath) -> void {
        for (auto observer : d->observers) {
            observer->DeletedDataRootFolder(fileFullPath);
        }
    }

    auto AcquisitionDataUpdater::Register(AcquisitionDataObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto AcquisitionDataUpdater::Deregister(AcquisitionDataObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}