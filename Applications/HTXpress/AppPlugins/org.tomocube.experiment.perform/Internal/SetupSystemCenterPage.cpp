#include <QTimer>

#include <GraphicsCrossItem.h>

#include "MotionObserver.h"
#include "SetupSystemCenterPageControl.h"
#include "SetupSystemCenterPage.h"
#include "ui_SetupSystemCenterPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupSystemCenterPage::Impl {
        SetupSystemCenterPageControl control;
        Ui::SetupSystemCenterPage ui;
        QTimer updateTimer;
        TC::Widgets::GraphicsCrossItem* fovCenterMark{ nullptr };
        TC::Widgets::GraphicsCrossItem* crossCenterMark{ nullptr };
        QImage latestImage;
        QSize imageSize;

        MotionObserver* motionObserver{ nullptr };

        auto InitUi()->void;
        auto StartUpdate(bool start)->void;
        auto UpdateImage()->void;
        auto DisableAutofocus()->void;
        auto FindCenter()->void;
        auto SaveCenter()->void;
    };

    auto SetupSystemCenterPage::Impl::InitUi() -> void {
        ui.imageView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.imageView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        ui.findCenterBtn->setDisabled(true);
        ui.saveBtn->setDisabled(true);
        ui.savedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.savedLabel->hide();

        fovCenterMark = new TC::Widgets::GraphicsCrossItem();
        fovCenterMark->SetLength(100);
        fovCenterMark->SetThickness(5);
        fovCenterMark->SetColor(0x00C2D3);
        ui.imageView->AddGraphicsItem(fovCenterMark);

        crossCenterMark = new TC::Widgets::GraphicsCrossItem();
        crossCenterMark->SetLength(100);
        crossCenterMark->SetThickness(5);
        crossCenterMark->SetColor(Qt::yellow);
        crossCenterMark->hide();
        ui.imageView->AddGraphicsItem(crossCenterMark);
    }

    auto SetupSystemCenterPage::Impl::StartUpdate(bool start) -> void {
        if(start) updateTimer.start(200);
        else updateTimer.stop();
    }

    auto SetupSystemCenterPage::Impl::UpdateImage() -> void {
        if(!control.GetLatestImage(latestImage)) return;
        ui.imageView->ShowImage(latestImage);

        if(imageSize != latestImage.size()) {
            imageSize = latestImage.size();
            fovCenterMark->SetCenter(imageSize.width()/2, imageSize.height()/2);
            ui.imageView->FitZoom();
        }
    }

    auto SetupSystemCenterPage::Impl::DisableAutofocus() -> void {
        control.DisableAutofocus();
        control.LowerZStage(110);
        ui.findCenterBtn->setEnabled(true);
    }

    auto SetupSystemCenterPage::Impl::FindCenter() -> void {
        auto [xPixels, yPixels, xOffset, yOffset] = control.FindCenter(latestImage);
        if((xPixels<0) || (yPixels<0)) {
            ui.offsetX->setValue(0);
            ui.offsetY->setValue(0);
            ui.saveBtn->setDisabled(true);
            crossCenterMark->hide();
        } else {
            ui.offsetX->setValue(ui.currentXPos->value() + xOffset);
            ui.offsetY->setValue(ui.currentYPos->value() + yOffset);
            ui.saveBtn->setDisabled(false);
            crossCenterMark->SetCenter(xPixels, yPixels);
            crossCenterMark->show();
        }
    }

    auto SetupSystemCenterPage::Impl::SaveCenter() -> void {
        auto xCenterOffset = ui.offsetX->value();
        auto yCenterOffset = ui.offsetY->value();
        if(!control.Save(xCenterOffset, yCenterOffset)) return;

        ui.offsetX->setValue(0);
        ui.offsetY->setValue(0);
        crossCenterMark->hide();
        ui.saveBtn->setDisabled(true);

        ui.savedLabel->show();
        QTimer::singleShot(2000, [this]() { ui.savedLabel->hide(); });

        control.MoveSampleStageInWell(0.001, 0.0);
        control.MoveSampleStageInWell(0.0, 0.0);
    }

    SetupSystemCenterPage::SetupSystemCenterPage(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);
        d->InitUi();
        Enter();

        d->motionObserver = new MotionObserver(this);

        connect(d->motionObserver, &MotionObserver::sigUpdatePosition, this, 
                [this](const AppEntity::WellIndex wellIdx, const AppEntity::Position& position) {
            Q_UNUSED(wellIdx);
            d->ui.currentXPos->setValue(position.toMM().x);
            d->ui.currentYPos->setValue(position.toMM().y);
        });

        connect(&d->updateTimer, &QTimer::timeout, this, [this]() {
            d->UpdateImage();
        });

        connect(d->ui.disableAFBtn, &QPushButton::clicked, this, [this]() {
            d->DisableAutofocus();
        });

        connect(d->ui.findCenterBtn, &QPushButton::clicked, this, [this]() {
            d->FindCenter();
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this]() {
            d->SaveCenter();
        });

        d->ui.widget->setObjectName("panel");

        for (const auto& label : findChildren<QLabel*>()) {
            if(label->objectName().contains("subtitle")) {
                label->setObjectName("label-h5");
            } else if (label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
        }

        for (const auto& button : findChildren<QPushButton*>()) {
            if(button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            } else {
                button->setObjectName("bt-maintenance-light");
            }
        }

        for (const auto& groupBox : findChildren<QGroupBox*>()) {
            groupBox->setObjectName("gb-maintenance-main");
        }
    }

    SetupSystemCenterPage::~SetupSystemCenterPage() {
    }

    auto SetupSystemCenterPage::Enter() -> void {
        d->StartUpdate(true);
    }

    auto SetupSystemCenterPage::Leave() -> void {
        d->StartUpdate(false);
    }

    void SetupSystemCenterPage::resizeEvent(QResizeEvent* event) {
        d->ui.imageView->FitZoom();
        QWidget::resizeEvent(event);
    }
}