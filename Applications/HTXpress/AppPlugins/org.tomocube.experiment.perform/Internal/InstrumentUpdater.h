#pragma once
#include <memory>
#include <IInstrumentView.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class InstrumentObserver;

    class InstrumentUpdater : public Interactor::IInstrumentView {
    public:
        using Pointer = std::shared_ptr<InstrumentUpdater>;

    protected:
        InstrumentUpdater();

    public:
        ~InstrumentUpdater();

        static auto GetInstance()->Pointer;

        auto UpdateFailed(const QString& message) -> void override;
        auto UpdateProgress(double progress, const QString& messsage) -> void override;

        auto UpdateGlobalPosition(const AppEntity::Position& position) -> void override;
        auto ReportAFFailed() -> void override;
        auto UpdateBestFocus(double posInMm) -> void override;
        auto AutoFocusEnabled(bool enable) -> void override;

        auto LiveStarted() -> void override;
        auto LiveStopped() -> void override;
        auto LiveImagingFailed(const QString& message) -> void override;

        auto VesselLoaded() -> void override;
        auto VesselUnloaded() -> void override;

        auto UpdateCAFScores(const QList<double>& scores, int32_t bestFocusIndex) -> void override;
        auto UpdateHTIlluminationResult(const QImage& image) -> void override;

    protected:
        auto Register(InstrumentObserver* observer)->void;
        auto Deregister(InstrumentObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class InstrumentObserver;
    };
}