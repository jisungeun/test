#include <QChart>

#include <MessageDialog.h>

#include "EvaluationConfig.h"
#include "EvaluationReportWriter.h"
#include "InstrumentObserver.h"
#include "EvaluationCAFPageControl.h"
#include "EvaluationCAFPage.h"
#include "ui_EvaluationCAFPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationCAFPage::Impl {
        EvaluationCAFPageControl control;
        Ui::EvaluationCAFPage ui;
        EvaluationCAFPage* p{ nullptr };
        QChart* chart{ new QChart() };

        InstrumentObserver* instObserver{ nullptr };

        bool cafSetupRun{ false };

        Impl(EvaluationCAFPage* p) : p{ p } {}
        auto InitUi()->void;
        auto Clear()->void;
        auto ChangeNA()->void;
        auto Evaluate()->void;
        auto UpdateScores(const QList<double>& scores, int32_t bestFocusIndex)->void;
        auto Save()->void;
    };

    auto EvaluationCAFPage::Impl::InitUi() -> void {
        ui.setupUi(p);

        ui.condenserNA->clear();
        auto NAs = control.GetNAs();
        for(auto NA : NAs) {
            ui.condenserNA->addItem(QString::number(NA), NA);
        }

        ui.savedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.savedLabel->hide();

        ui.saveBtn->setDisabled(true);

        ui.chartView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.chartView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        chart->legend()->hide();
        ui.chartView->setChart(chart);
        ui.chartView->setRenderHint(QPainter::Antialiasing);

        auto [scoreRef, brightnessRef] = control.GetReference(ui.condenserNA->currentData().toDouble());
        ui.scoreRefLabel->setText(tr("(Ref: %1)").arg(scoreRef, 0, 'f', 3));
        ui.brightnessRefLabel->setText(tr("(Ref: %1)").arg(brightnessRef, 0, 'f', 3));

        ui.resultLabel->hide();

        for (const auto& label : p->findChildren<QLabel*>()) {
            if (label->objectName().contains("subtitle")) {
                label->setObjectName("label-h5");
            }
            else if (label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
            else if(label->objectName().contains("ref")) {
                label->setObjectName("label-h6");
            }
        }

        for (const auto& button : p->findChildren<QPushButton*>()) {
            if (button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            }
            else {
                button->setObjectName("bt-maintenance-light");
            }
        }

        for (const auto& groupBox : p->findChildren<QGroupBox*>()) {
            groupBox->setObjectName("gb-maintenance-main");
        }

        for (const auto& table : p->findChildren<QTableWidget*>()) {
            table->setStyleSheet(QString("QTableWidget{border-bottom: 0px;}"));
            table->verticalHeader()->setStyleSheet("QHeaderView{border-bottom: 0px;}");
            table->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);
        }

        ui.leftWidget->setObjectName("panel");
        ui.rightWidget->setObjectName("panel");
        ui.botWidget->setObjectName("panel");
    }

    auto EvaluationCAFPage::Impl::Clear() -> void {
        ui.cafScore->setValue(0);
        ui.diskBrightness->setValue(0);
        ui.saveBtn->setDisabled(true);
        ui.resultLabel->hide();
        chart->removeAllSeries();

        cafSetupRun = false;

        control.Clear();
    }

    auto EvaluationCAFPage::Impl::ChangeNA() -> void {
        auto [scoreRef, brightnessRef] = control.GetReference(ui.condenserNA->currentData().toDouble());
        ui.scoreRefLabel->setText(tr("(Ref: %1)").arg(scoreRef, 0, 'f', 3));
        ui.brightnessRefLabel->setText(tr("(Ref: %1)").arg(brightnessRef, 0, 'f', 3));
    }

    auto EvaluationCAFPage::Impl::Evaluate() -> void {
        const auto NA = ui.condenserNA->currentData().toDouble();
        if(NA == 0) return;

        Clear();

        cafSetupRun = true;

        if(!control.RunCAF(NA)) {
            TC::MessageDialog::warning(p, tr("Evaluation"), tr("Condenser autofocus is failed"));
            return;
        }

        cafSetupRun = false;
    }

    auto EvaluationCAFPage::Impl::UpdateScores(const QList<double>& scores, int32_t bestFocusIndex) -> void {
        if(!cafSetupRun) return;

        auto* series = new QLineSeries();
        auto counts = scores.size();
        for(auto idx=0; idx<counts; idx++) {
            series->append(idx, scores.at(idx));
        }

        chart->removeAllSeries();
        chart->addSeries(series);
        chart->createDefaultAxes();
        auto* xAxis = qobject_cast<QValueAxis*>(chart->axes(Qt::Horizontal).at(0));
        xAxis->setLabelFormat("%d");

        if(bestFocusIndex >= counts) {
            TC::MessageDialog::warning(p, "Evaluation", tr("Invalid best focus index = %1").arg(bestFocusIndex));
            return;
        }

        const auto NA = ui.condenserNA->currentData().toDouble();
        auto bestScore = scores.at(bestFocusIndex);
        control.SetResult(NA, bestScore, bestFocusIndex);

        auto [score, brightenss, result] = control.Evaluate();
        ui.cafScore->setValue(score);
        ui.diskBrightness->setValue(brightenss);

        if(result) {
            ui.resultLabel->setText("Pass");
            ui.resultLabel->setStyleSheet("QLabel {color:green; font: bold 14px;}");
            ui.resultLabel->show();
        } else {
            ui.resultLabel->setText("Fail");
            ui.resultLabel->setStyleSheet("QLabel {color:red;  font: bold 14px;}");
            ui.resultLabel->show();
        }

        ui.saveBtn->setEnabled(true);
    }

    auto EvaluationCAFPage::Impl::Save() -> void {
        const auto path = EvaluationConfig::GetInstance()->GetReportFolder();
        if(!control.Save(path)) {
            TC::MessageDialog::warning(p, tr("Evaluation"), tr("Failed to save the condenser autofocus evaluation report"));
            return;
        }

        ui.savedLabel->show();
        QTimer::singleShot(2000, [this](){ ui.savedLabel->hide(); });
    }

    EvaluationCAFPage::EvaluationCAFPage(QWidget* parent) : QWidget(parent), d{ std::make_unique<Impl>(this) } {
        d->InitUi();
        d->Clear();

        d->instObserver = new InstrumentObserver(this);

        connect(d->ui.condenserNA, qOverload<int32_t>(&QComboBox::currentIndexChanged), this, [this](int32_t index) {
            Q_UNUSED(index)
            d->ChangeNA();
        });

        connect(d->ui.evaluateBtn, &QPushButton::clicked, this, [this]() {
            d->Evaluate();
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this]() {
            d->Save();
        });

        connect(d->instObserver, &InstrumentObserver::sigUpdateCAFScores, this, 
                [this](const QList<double>& scores, int32_t bestFocusIndex) {
            d->UpdateScores(scores, bestFocusIndex);
        });
    }

    EvaluationCAFPage::~EvaluationCAFPage() {
    }

    auto EvaluationCAFPage::Enter() -> void {
        d->Clear();
    }

    auto EvaluationCAFPage::Leave() -> void {
        d->Clear();
    }

    void EvaluationCAFPage::resizeEvent(QResizeEvent* event) {
        QWidget::resizeEvent(event);
    }
}
