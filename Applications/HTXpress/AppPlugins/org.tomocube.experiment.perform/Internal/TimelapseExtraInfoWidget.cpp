﻿#include "TimelapseExtraInfoWidget.h"
#include "ui_TimelapseExtraInfoWidget.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    constexpr int64_t KB = 1024;
    constexpr int64_t MB = 1048576;
    constexpr int64_t GB = 1073741824;
    constexpr int64_t TB = 1099511627776;

    struct TimelapseExtraInfoWidget::Impl {
        Ui::Form ui{};
        int64_t requiredBytes{};

        auto InitPointsEdit() -> void;
        auto InitIntervalEdit() -> void;
        auto InitSpaceEdit() -> void;
    };

    TimelapseExtraInfoWidget::TimelapseExtraInfoWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui.setupUi(this);

        d->InitPointsEdit();
        d->InitIntervalEdit();
        d->InitSpaceEdit();
    }

    TimelapseExtraInfoWidget::~TimelapseExtraInfoWidget() = default;

    auto TimelapseExtraInfoWidget::SetImagingPoints(int32_t count) -> void {
        d->ui.pointsEdit->setText(QString::number(count));
    }

    auto TimelapseExtraInfoWidget::GetImagingPoints() const -> int32_t {
        return d->ui.pointsEdit->text().toInt();
    }

    auto TimelapseExtraInfoWidget::SetInterval(const QString& interval) -> void {
        d->ui.intervalEdit->setText(interval);
    }

    auto TimelapseExtraInfoWidget::SetRequiredSpace(const int64_t& bytes) -> void {
        d->requiredBytes = bytes;

        auto displayText = [](const int64_t& bytes)-> QString {
            int64_t denominator{1};
            QString unit{"bytes"};
            if (bytes > TB) {
                denominator = TB;
                unit = "TB";
            }
            else if (bytes > GB) {
                denominator = GB;
                unit = "GB";
            }
            else if (bytes > MB) {
                denominator = MB;
                unit = "MB";
            }
            else {
                denominator = KB;
                unit = "KB";
            }

            return QString("%1 %2").arg(QString::number(static_cast<double>(bytes) / denominator, 'f', 2)).arg(unit);
        };

        d->ui.reqSpaceEdit->setText(displayText(bytes));
    }

    auto TimelapseExtraInfoWidget::GetRequiredSpace() const -> int64_t {
        return d->requiredBytes;
    }

    auto TimelapseExtraInfoWidget::SetSpaceStatus(const bool& isEnough) -> void {
        // TODO text color change here
        if(!isEnough) {
            d->ui.reqSpaceEdit->setStyleSheet("QLineEdit{color:#D55C56; font-weight:bold;}");
        } else {
            d->ui.reqSpaceEdit->setStyleSheet("");
        }
    }

    // private
    auto TimelapseExtraInfoWidget::Impl::InitPointsEdit() -> void {
        ui.pointsLabel->setObjectName("label-h5");

        ui.pointsEdit->setAlignment(Qt::AlignCenter);
        ui.pointsEdit->setReadOnly(true);
        ui.pointsEdit->setText("0");
    }

    auto TimelapseExtraInfoWidget::Impl::InitIntervalEdit() -> void {
        ui.intervalLabel->setObjectName("label-h5");

        ui.intervalEdit->setAlignment(Qt::AlignCenter);
        ui.intervalEdit->setReadOnly(true);
        ui.intervalEdit->setText("00:00:00");
    }

    auto TimelapseExtraInfoWidget::Impl::InitSpaceEdit() -> void {
        ui.reqSpaceLabel->setObjectName("label-h5");

        ui.reqSpaceEdit->setAlignment(Qt::AlignCenter);
        ui.reqSpaceEdit->setReadOnly(true);
        ui.reqSpaceEdit->setText("0");
    }
}
