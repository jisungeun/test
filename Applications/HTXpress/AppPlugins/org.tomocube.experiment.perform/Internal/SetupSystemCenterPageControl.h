#pragma once
#include <memory>
#include <QList>
#include <QImage>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SetupSystemCenterPageControl {
    public:
        SetupSystemCenterPageControl();
        ~SetupSystemCenterPageControl();

        auto GetCenter() const->std::tuple<double,double>;
        auto GetLatestImage(QImage& image)->bool;
        auto DisableAutofocus()->bool;
        auto LowerZStage(double distInUm)->bool;
        auto MoveSampleStageInWell(double xInMm, double yInMm)->bool;
        auto FindCenter(QImage& image) const->std::tuple<int32_t,int32_t,double,double>;
        auto Save(double xOffsetInMm, double yOffsetInMm)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}