#pragma once
#include <memory>

#include <QImage>
#include <QString>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationAFPageControl {
    public:
        EvaluationAFPageControl();
        ~EvaluationAFPageControl();

        auto GetReference() const->double;
        auto Clear()->void;
        auto GetImage(int32_t index) const->QImage;
        auto Evaluate()->std::tuple<double,bool>;
        auto Save(const QString& path)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
