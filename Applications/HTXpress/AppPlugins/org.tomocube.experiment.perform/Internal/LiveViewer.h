#pragma once
#include <memory>
#include <QWidget>
#include <QResizeEvent>

#include <AppEntityDefines.h>
#include <ChannelConfig.h>
#include <ImagingConfig.h>

#include "LiveviewAnnotationItem.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class LiveViewer : public QWidget {
        Q_OBJECT

    public:
        explicit LiveViewer(QWidget* parent = nullptr);
        ~LiveViewer() override;

    public:
        auto HideLiveControlDialog() -> void;

        auto AddAnnotation(LiveviewAnnotationItem* annotation, int32_t xInPixel, int32_t yInPixel)->void;
        auto RemoveAnnotation(LiveviewAnnotationItem* annotation)->void;

    protected slots:
        void onUpdateImage();
        void onPositionSelected(int32_t posX, int32_t posY);
        void onModeSelected(AppEntity::Modality modality, int32_t channel);
        void onUpdateVesselStatus(bool loaded);
        void onUpdateFOV(const double xInUm, const double yInUm);
        void onUpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types);
        void onUpdateFLChannelConfig(const int32_t channel, const AppEntity::ChannelConfig::Pointer config);

        void onChangedLiveControlValue(const uint32_t intensity, const uint32_t exposure, const double gain);
        void onApplyChannelConditionToLiveControl();
        void onSaveLiveCondition();
        void onCloseLiveControl(int result);

        void onUpdatedLiveCondition(const uint32_t intensity, const uint32_t exposure, const double gain);
        void onUpdatedAcquisitionConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain);
        void onUpdateAcquisitionLock(bool locked);

        void onChangeImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode);
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}