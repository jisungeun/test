#include <QList>

#include "ExperimentIOObserver.h"
#include "ExperimentIOUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct ExperimentIOUpdater::Impl {
        QList<ExperimentIOObserver*> observers;
    };

    ExperimentIOUpdater::ExperimentIOUpdater() : Interactor::IExperimentIOView(), d{new Impl} {
    }

    ExperimentIOUpdater::~ExperimentIOUpdater() {
    }

    auto ExperimentIOUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new ExperimentIOUpdater() };
        return theInstance;
    }

    auto ExperimentIOUpdater::Update(AppEntity::Experiment::Pointer experiment, bool reloaded) -> void {
        for(auto& observer : d->observers) {
            observer->Update(experiment, reloaded);
        }
    }

    auto ExperimentIOUpdater::Error(const QString& message) -> void {
        for(auto& observer : d->observers) {
            observer->Error(message);
        }
    }

    auto ExperimentIOUpdater::Register(ExperimentIOObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto ExperimentIOUpdater::Deregister(ExperimentIOObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}
