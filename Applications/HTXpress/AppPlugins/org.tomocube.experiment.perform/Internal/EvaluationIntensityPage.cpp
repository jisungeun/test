#include <QFileDialog>
#include <QTimer>

#include <MessageDialog.h>

#include "EvaluationConfig.h"
#include "EvaluationReportWriter.h"
#include "EvaluationIntensityPageControl.h"
#include "EvaluationIntensityPage.h"
#include "ui_EvaluationIntensityPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationIntensityPage::Impl {
        EvaluationIntensityPageControl control;
        Ui::EvaluationIntensityPanel ui;
        EvaluationIntensityPage*p {nullptr};

        Impl(EvaluationIntensityPage* p) : p{ p } {}

        auto InitUi()->void;
        auto Clear()->void;
        auto Evaluate()->void;
        auto Save()->void;
    };

    auto EvaluationIntensityPage::Impl::InitUi() -> void {
        ui.setupUi(p);

        ui.savedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.savedLabel->hide();

        ui.imageView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.imageView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        ui.refLabel->setText(tr("(Ref: %1)").arg(control.GetCVReference(), 0, 'f', 3));

        for (const auto& label : p->findChildren<QLabel*>()) {
            if (label->objectName().contains("subtitle")) {
                label->setObjectName("label-h5");
            }
            else if (label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
            else if(label->objectName().contains("ref")) {
                label->setObjectName("label-h6");
            }
        }

        for (const auto& button : p->findChildren<QPushButton*>()) {
            if (button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            }
            else {
                button->setObjectName("bt-maintenance-light");
            }
        }
        
        ui.leftWidget->setObjectName("panel");
        ui.botWidget->setObjectName("panel");
    }

    auto EvaluationIntensityPage::Impl::Clear() -> void {
        ui.imageView->ClearImage();
        ui.cvValue->setValue(0);
        ui.resultLabel->hide();
        ui.saveBtn->setDisabled(true);

        control.Clear();
    }

    auto EvaluationIntensityPage::Impl::Evaluate() -> void {
        Clear();

        auto image = control.GetImage();
        if(image.isNull()) {
            TC::MessageDialog::warning(p, tr("Evaluation"), "Failed to get an image to be evaluated");
            return;
        }

        ui.imageView->ShowImage(image);

        auto [cvValue, result] = control.Evaluate();
        ui.cvValue->setValue(cvValue);

        if(result) {
            ui.resultLabel->setText("Pass");
            ui.resultLabel->setStyleSheet("QLabel {color:green; font: bold 14px;}");
            ui.resultLabel->show();
        } else {
            ui.resultLabel->setText("Fail");
            ui.resultLabel->setStyleSheet("QLabel {color:red;  font: bold 14px;}");
            ui.resultLabel->show();
        }

        ui.saveBtn->setEnabled(true);
    }

    auto EvaluationIntensityPage::Impl::Save() -> void {
        const auto path = EvaluationConfig::GetInstance()->GetReportFolder();
        if(!control.Save(path)) {
            TC::MessageDialog::warning(p, tr("Evaluation"), tr("Failed to save the intensity uniformity evaluation report"));
            return;
        }

        ui.savedLabel->show();
        QTimer::singleShot(2000, [this](){ ui.savedLabel->hide(); });
    }

    EvaluationIntensityPage::EvaluationIntensityPage(QWidget* parent)
        : QWidget(parent)
        , EvaluationPage()
        , d{ std::make_unique<Impl>(this) } {
        d->InitUi();
        d->Clear();

        connect(d->ui.evalBtn, &QPushButton::clicked, this, [this]() {
            d->Evaluate();
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this]() {
            d->Save();
        });
    }

    EvaluationIntensityPage::~EvaluationIntensityPage() {
    }

    auto EvaluationIntensityPage::Enter() -> void {
        d->Clear();
    }

    auto EvaluationIntensityPage::Leave() -> void {
        d->Clear();
    }

    void EvaluationIntensityPage::resizeEvent(QResizeEvent* event) {
        d->ui.imageView->FitZoom();
        QWidget::resizeEvent(event);
    }
}
