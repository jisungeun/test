#pragma once
#include <memory>

#include <QList>
#include <QString>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationCAFPageControl {
    public:
        EvaluationCAFPageControl();
        ~EvaluationCAFPageControl();

        auto GetNAs() const->QList<double>;
        auto GetReference(double NA) const->std::tuple<double,double>;
        auto Clear()->void;
        auto RunCAF(double NA)->bool;
        auto SetResult(double NA, double score, int32_t index)->void;
        auto Evaluate()->std::tuple<double, double, bool>;
        auto Save(const QString& path)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}