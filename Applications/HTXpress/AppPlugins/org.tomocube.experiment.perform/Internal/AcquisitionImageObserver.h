#pragma once
#include <memory>
#include <QObject>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class AcquisitionImageObserver : public QObject {
        Q_OBJECT

    public:
        AcquisitionImageObserver(QObject* parent = nullptr);
        ~AcquisitionImageObserver() override;

        virtual auto UpdateProgress(double progress) -> void;

    signals:
        void sigUpdateProgress(double progress);
    };
}