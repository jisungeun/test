#pragma once
#include <memory>

#include <QObject>
#include <IVesselView.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class VesselObserver : public QObject {
        Q_OBJECT

    public:
        VesselObserver(QObject* parent = nullptr);
        ~VesselObserver() override;

        auto UpdateCurrentWell(AppEntity::WellIndex wellIdx) -> void;
        auto UpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM) -> void;

    signals:
        void sigUpdateCurrentWell(AppEntity::WellIndex wellIdx);
        void sigUpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM);
    };
}