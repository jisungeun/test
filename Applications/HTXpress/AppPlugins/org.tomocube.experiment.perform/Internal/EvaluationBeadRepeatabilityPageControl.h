#pragma once
#include <memory>
#include <QList>

#include <BeadScore.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationBeadRepeatabilityPageControl {
    public:
        EvaluationBeadRepeatabilityPageControl();
        ~EvaluationBeadRepeatabilityPageControl();

        auto GetCVReference() const->double;
        auto GetRepeatCount() const->int32_t;
        auto AcquireData(int32_t repeatCount)->bool;

        auto AddDataPath(const QString& dataPath)->bool;
        auto GetDataCount() const->int32_t;
        auto StartEvaluation()->void;
        auto EvaluationResult()->std::tuple<double, bool>;
        auto GetScore(int32_t index) const->Entity::BeadScore;

        auto Save(const QString& path) const->bool;
        auto Clear()->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}