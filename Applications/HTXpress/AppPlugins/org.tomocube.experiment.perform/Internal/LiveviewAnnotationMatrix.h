#pragma once
#include <memory>

#include "LiveviewAnnotationItem.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class LiveviewAnnotationMatrix : public LiveviewAnnotationItem {
    public:
        LiveviewAnnotationMatrix();
        ~LiveviewAnnotationMatrix() override;

        auto GetGraphicsItem() const -> QGraphicsItem* override;

        auto SetTile(int32_t width, int32_t height, int32_t gapW, int32_t gapH)->void;
        auto SetMatrix(int32_t cols, int32_t rows) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}