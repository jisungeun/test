#pragma once
#include <memory>
#include <QImage>
#include <QString>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationHTIllumCalPageControl {
    public:
        EvaluationHTIllumCalPageControl();
        ~EvaluationHTIllumCalPageControl();

        auto GetNAs() const -> QList<double>;
        auto RunCAF(double NA)->bool;
        auto RunHTIllumCalibration(double NA)->bool;
        auto SetImage(const QImage& image)->void;
        auto Clear()->void;
        auto Save(const QString& path)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}