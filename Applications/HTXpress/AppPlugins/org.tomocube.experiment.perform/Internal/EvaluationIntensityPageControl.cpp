#include <QImage>
#include <QElapsedTimer>
#include <QSettings>

#include <Image2DProc.h>
#include <System.h>

#include "LiveImageSource.h"
#include "EvaluationReportWriter.h"
#include "EvaluationIntensityPageControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationIntensityPageControl::Impl {
        QImage latestImage;
        double cvValue{ 0 };
        bool result{ true };
    };

    EvaluationIntensityPageControl::EvaluationIntensityPageControl() : d{ std::make_unique<Impl>() } {
    }

    EvaluationIntensityPageControl::~EvaluationIntensityPageControl() {
    }

    auto EvaluationIntensityPageControl::GetCVReference() const -> double {
        auto model = AppEntity::System::GetModel();
        return model->EvaluationReference(AppEntity::EvaluationRef::IntensityUniformityCV);
    }

    auto EvaluationIntensityPageControl::GetImage() -> QImage {
        d->latestImage = QImage();

        QElapsedTimer timer;
        timer.start();

        while(d->latestImage.isNull() && (timer.elapsed() < 2000)) {
              if(LiveImageSource::GetInstance()->GetLatestImage(d->latestImage)) break;
        }

        return d->latestImage;
    }

    auto EvaluationIntensityPageControl::Evaluate() -> std::tuple<double, bool> {
        d->cvValue = TC::Processing::Image2DProc::CalcUnifomityCV(d->latestImage, 10, 10);
        d->result = (d->cvValue <= GetCVReference());
        return std::make_tuple(d->cvValue, d->result);
    }

    auto EvaluationIntensityPageControl::Save(const QString& path) const -> bool {
        QMap<QString, QVariant> results;
        results["cv"] = d->cvValue;
        results["pass"] = d->result;

        auto writer = EvaluationReportWriter::GetInstance();
        writer->SetPath(path);
        writer->SetTitle("intensity_uniformity");
        if(!writer->SaveImage("image", d->latestImage)) return false;
        if(!writer->SaveResults("report", results)) return false;

        return true;
    }

    auto EvaluationIntensityPageControl::Clear() -> void {
        d->latestImage = QImage();
        d->cvValue = 0;
        d->result = true;
    }
}
