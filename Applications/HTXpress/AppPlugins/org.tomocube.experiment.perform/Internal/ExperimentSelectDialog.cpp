#include "ExperimentSelectDialogControl.h"
#include "ExperimentSelectDialog.h"
#include "ui_ExperimentSelectDialog.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct ExperimentSelectDialog::Impl {
        ExperimentSelectDialogControl control;
        Ui::ExperimentSelectDialog ui;
        QString project;
        QString experiment;
    };

    ExperimentSelectDialog::ExperimentSelectDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
        d->ui.setupUi(this);
        d->ui.projectList->addItems(d->control.GetProjects());
        d->ui.openBtn->setEnabled(false);

        connect(d->ui.newBtn, SIGNAL(clicked()), this, SLOT(onNew()));
        connect(d->ui.openBtn, SIGNAL(clicked()), this, SLOT(onSelected()));
        connect(d->ui.projectList, SIGNAL(currentTextChanged(const QString&)), this, 
                SLOT(onProjectSelected(const QString&)));
        connect(d->ui.experimentList, SIGNAL(currentTextChanged(const QString&)), this, 
                SLOT(onExperimentSelected(const QString&)));
    }

    ExperimentSelectDialog::~ExperimentSelectDialog() {
    }

    auto ExperimentSelectDialog::GetProject() const -> QString {
        return d->project;
    }

    auto ExperimentSelectDialog::GetExperiment() const -> QString {
        return d->experiment;
    }

    void ExperimentSelectDialog::onNew() {
        d->project.clear();
        d->experiment.clear();
        accept();
    }

    void ExperimentSelectDialog::onSelected() {
        d->project = d->ui.projectList->currentItem()->text();
        d->experiment = d->ui.experimentList->currentItem()->text();
        accept();
    }

    void ExperimentSelectDialog::onProjectSelected(const QString& project) {
        d->ui.openBtn->setEnabled(false);
        d->ui.experimentList->clear();
        d->ui.experimentList->addItems(d->control.GetExperiments(project));
    }

    void ExperimentSelectDialog::onExperimentSelected(const QString& experiment) {
        Q_UNUSED(experiment)
        d->ui.openBtn->setEnabled(true);
    }
}
