#include <QList>

#include "PreviewObserver.h"
#include "PreviewUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct PreviewUpdater::Impl {
        QList<PreviewObserver*> observers;
    };

    PreviewUpdater::PreviewUpdater() : d{new Impl} {
    }

    PreviewUpdater::~PreviewUpdater() {
    }

    auto PreviewUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new PreviewUpdater };
        return theInstance;
    }

    auto PreviewUpdater::Progress(double progress, AppEntity::Position& position) -> void {
        for(auto observer : d->observers) {
            observer->Progress(progress);
            observer->UpdatePosition(position);
        }
    }

    auto PreviewUpdater::UpdateImageSize(int32_t xPixels, int32_t yPixels) -> void {
        for(auto observer : d->observers) {
            observer->UpdateImageSize(xPixels, yPixels);
        }
    }

    auto PreviewUpdater::UpdateBlock(int32_t xPos, int32_t yPos, const QImage& blockImage) -> void {
        for(auto observer : d->observers) {
            observer->UpdateBlock(xPos, yPos, blockImage);
        }
    }

    auto PreviewUpdater::UpdateCustomPreviewArea(double wellXinMM, double wellYinMM, int32_t widthUm, int32_t heightUm) -> void {
        for(auto observer : d->observers) {
            observer->SetCustomPreviewArea(wellXinMM, wellYinMM, widthUm, heightUm);
        }
    }

    auto PreviewUpdater::StartCustomPreviewAreaSetting() -> void {
        for(auto observer : d->observers) {
            observer->StartCustomPreviewAreaSetting();
        }
    }

    auto PreviewUpdater::CancelCustomPreviewAreaSetting() -> void {
        for(auto observer : d->observers) {
            observer->CancelCustomPreviewAreaSetting();
        }
    }

    auto PreviewUpdater::SetPreviewArea(double wellXinMM, double wellYinMM, double widthMM, double heightMM) -> void {
        for(auto observer : d->observers) {
            observer->SetPreviewArea(wellXinMM, wellYinMM, widthMM, heightMM);
        }
    }

    auto PreviewUpdater::Register(PreviewObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto PreviewUpdater::Deregister(PreviewObserver* observer) -> void {
        d->observers.push_back(observer);
    }
}
