#pragma once
#include <memory>
#include <QList>

#include <BeadScore.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationMatrixBeadsPageControl {
    public:
        EvaluationMatrixBeadsPageControl();
        ~EvaluationMatrixBeadsPageControl();

        auto GetCVReference() const->double;
        auto GetTileCount() const->int32_t;
        auto GetGapX() const->int32_t;
        auto GetGapY() const->int32_t;
        auto AcquireData(int32_t xTiles, int32_t yTiles, int32_t xGapUm, int32_t yGapUm)->bool;

        auto AddDataPath(const QString& dataPath)->bool;
        auto GetDataCount() const->int32_t;
        auto StartEvaluation()->void;
        auto EvaluationResult()->std::tuple<double, bool>;
        auto GetScore(int32_t index) const->std::tuple<int32_t, int32_t, Entity::BeadScore>;

        auto Save(const QString& path) const->bool;
        auto Clear()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}