#pragma once
#include <memory>
#include <QList>

#include <BeadScore.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationSingleBeadPageControl {
    public:
        EvaluationSingleBeadPageControl();
        ~EvaluationSingleBeadPageControl();

        auto GetScoreThreshold() const->double;
        auto RunCAF() const->bool;
        auto RunHTIllumCalibration() const->bool;
        auto AcquireData()->bool;

        auto AddDataPath(const QString& fileFullPath)->bool;
        auto GetDataCount() const->int32_t;
        auto GetDataTitle(int32_t index) const->std::tuple<QString,QString>;
        auto StartEvaluation()->void;
        auto EvaluationResult()->std::tuple<double, bool>;
        auto GetScore(int32_t index) const->Entity::BeadScore;

        auto Save(const QString& path) const->bool;
        auto Clear()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}