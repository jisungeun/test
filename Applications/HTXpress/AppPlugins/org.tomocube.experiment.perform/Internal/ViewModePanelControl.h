#pragma once
#include <memory>
#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class ViewModePanelControl {
    public:
        ViewModePanelControl();
        ~ViewModePanelControl();

        auto GetCount() const->int32_t;
        auto GetTitle(int32_t index) const->QString;
        auto GetModality(int32_t index) const->AppEntity::Modality;
        auto GetChannel(int32_t index) const->int32_t;
        auto GetIndex(AppEntity::Modality modality, int32_t channel=0)->int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}