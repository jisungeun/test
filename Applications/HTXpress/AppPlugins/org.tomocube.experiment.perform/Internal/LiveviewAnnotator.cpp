#include "LiveviewAnnotator.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct LiveviewAnnotator::Impl {
        LiveViewer* viewer{ nullptr };
    };

    LiveviewAnnotator::LiveviewAnnotator() : d{ std::make_unique<Impl>() } {
    }

    LiveviewAnnotator::~LiveviewAnnotator() {
    }

    auto LiveviewAnnotator::GetInstance() -> Pointer {
        static Pointer theInstance{ new LiveviewAnnotator() };
        return theInstance;
    }

    auto LiveviewAnnotator::InstallViewer(LiveViewer* viewer) -> void {
        d->viewer = viewer;
    }

    auto LiveviewAnnotator::AddItem(LiveviewAnnotationItem* item, int32_t xInPixel, int32_t yInPixel) -> void {
        if(!d->viewer) return;
        d->viewer->AddAnnotation(item, xInPixel, yInPixel);
    }

    auto LiveviewAnnotator::RemoveItem(LiveviewAnnotationItem* item) -> void {
        if(!d->viewer) return;
        d->viewer->RemoveAnnotation(item);
    }
}
