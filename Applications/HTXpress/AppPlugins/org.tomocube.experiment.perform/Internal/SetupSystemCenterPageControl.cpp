#include <Image2DProc.h>

#include <System.h>
#include <MotionController.h>
#include <SystemConfigController.h>
#include <InstrumentController.h>

#include "Utility.h"
#include "LiveImageSource.h"
#include "MotionUpdater.h"
#include "InstrumentUpdater.h"
#include "SetupSystemCenterPageControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupSystemCenterPageControl::Impl {
    };

    SetupSystemCenterPageControl::SetupSystemCenterPageControl() : d{new Impl} {
    }

    SetupSystemCenterPageControl::~SetupSystemCenterPageControl() {
    }

    auto SetupSystemCenterPageControl::GetCenter() const -> std::tuple<double, double> {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        return std::make_tuple(sysConfig->GetSystemCenter().toMM().x,
                               sysConfig->GetSystemCenter().toMM().y);
    }

    auto SetupSystemCenterPageControl::GetLatestImage(QImage& image) -> bool {
        return LiveImageSource::GetInstance()->GetLatestImage(image);
    }

    auto SetupSystemCenterPageControl::DisableAutofocus() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return controller.DisalbeAutoFocus();
    }

    auto SetupSystemCenterPageControl::LowerZStage(double distInUm) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveRelativeUM(AppEntity::Axis::Z, -distInUm);
    }

    auto SetupSystemCenterPageControl::MoveSampleStageInWell(double xInMm, double yInMm) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveXY(0, xInMm, yInMm);
    }

    auto SetupSystemCenterPageControl::FindCenter(QImage& image) const -> std::tuple<int32_t,int32_t,double, double> {
        auto [xCenter, yCenter] = TC::Processing::Image2DProc::FindCenterOfCross(image, false);
        if((xCenter < 0) || (yCenter < 0)) return std::make_tuple(-1, -1, 0, 0);

        auto model = AppEntity::System::GetModel();
        auto xCenterUm = Utility::pixel2um(xCenter - (model->CameraPixelsH() / 2));
        auto yCenterUm = Utility::pixel2um(yCenter - (model->CameraPixelsV() / 2));

        return std::make_tuple(xCenter, yCenter, xCenterUm/1000, -yCenterUm/1000);
    }

    auto SetupSystemCenterPageControl::Save(double xOffsetInMm, double yOffsetInMm) -> bool {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto centerPos = sysConfig->GetSystemCenter();

        centerPos = AppEntity::Position::fromMM(centerPos.toMM().x + xOffsetInMm,
                                                centerPos.toMM().y + yOffsetInMm,
                                                centerPos.toMM().z);
        sysConfig->SetSystemCenter(centerPos);

        auto controller = Interactor::SystemConfigController();
        return controller.Save();
    }
}