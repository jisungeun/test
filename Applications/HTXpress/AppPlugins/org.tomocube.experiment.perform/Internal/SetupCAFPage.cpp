#include <MessageDialog.h>

#include "InstrumentObserver.h"
#include "MotionObserver.h"
#include "SetupCAFPageControl.h"
#include "SetupCAFPage.h"
#include "ui_SetupCAFPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupCAFPage::Impl {
        SetupCAFPage* p{ nullptr };
        SetupCAFPageControl control;
        Ui::SetupCAFPage ui;
        InstrumentObserver* instObserver{ nullptr };
        MotionObserver* motionObserver{ nullptr };
        QChart* chart{ new QChart() };
        QTimer updateTimer;
        
        bool cafValid{ false };
        bool cafSetupRun{ false };

        Impl(SetupCAFPage* p) : p{p} {}

        auto InitUi()->void;
        auto ControlTabChanged(int32_t index)->void;
        auto ChangeNA()->void;
        auto RunCAF()->void;
        auto SaveLedIntensity()->void;
        auto UpdateScores(const QList<double>& scores, int32_t bestFocusIndex)->void;
        auto ClearResult()->void;
        auto ResultTabChanged(int32_t index)->void;
        auto UpdateDiskImage()->void;
        auto RefreshBFImage(bool refresh)->void;
        auto ChangeCAxisRelative(double relativeMm)->void;
        auto UpdateCAxisPosition(double posMm)->void;
        auto SaveCAxisOffset()->void;

        //Start/Stop updating image
        auto StartUpdate(bool start)->void;
        auto UpdateImage()->void;
    };

    auto SetupCAFPage::Impl::InitUi() -> void {
        //Intensity
        ui.condenserNA->clear();
        ui.condenserNA->addItem("", 0);
        auto NAs = control.GetNAs();
        for(auto NA : NAs) {
            ui.condenserNA->addItem(QString::number(NA), NA);
        }

        ui.saveLedIntensityBtn->setDisabled(true);

        ui.intensitySavedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.intensitySavedLabel->hide();

        //Offset
        ui.cOffsetSavedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.cOffsetSavedLabel->hide();

        ui.cAxisBaseLine->setRange(0, 100);
        ui.cAxisCurrent->setRange(0, 100);
        ui.cAxisOffset->setRange(-10, 10);

        //Common
        ui.controlTabWidget->setCurrentIndex(0);
        ui.resultTabWidget->setCurrentIndex(0);

        ui.chartView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.chartView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.imageView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.imageView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        chart->legend()->hide();
        ui.chartView->setChart(chart);
        ui.chartView->setRenderHint(QPainter::Antialiasing);

        ClearResult();
    }

    auto SetupCAFPage::Impl::ControlTabChanged(int32_t index) -> void {
        if(index == 1) {
            const auto NA = ui.condenserNA->currentData().toDouble();
            if(NA == 0) return;

            auto cReady = control.GetCAxisBaseline(NA);
            ui.cAxisBaseLine->setValue(cReady);

            auto cCurrent = control.GetCAxisCurrent();
            ui.cAxisCurrent->setValue(cCurrent);

            ui.cAxisOffset->setValue(cCurrent - cReady);
            ui.imgBotWidget->hide();
            ui.imageView->ClearImage();
            ui.resultTabWidget->setCurrentIndex(1);
            StartUpdate(true);
        } else {
            ui.imgBotWidget->show();
            ui.imageView->ClearImage();
            StartUpdate(false);
        }
    }

    auto SetupCAFPage::Impl::ChangeNA() -> void {
        cafValid = false;
        ui.saveLedIntensityBtn->setDisabled(true);
        ui.intensitySavedLabel->hide();

        ClearResult();

        const auto NA = ui.condenserNA->currentData().toDouble();
        if(NA == 0) return;
        ui.ledIntensity->setValue(control.GetLEDIntensity(NA));
    }

    auto SetupCAFPage::Impl::RunCAF() -> void {
        const auto NA = ui.condenserNA->currentData().toDouble();
        if(NA == 0) return;

        const auto intensity = ui.ledIntensity->value();

        ClearResult();

        cafSetupRun = true;

        if(!control.RunCAF(NA, intensity)) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("Condenser autofocus is failed"));
            return;
        }

        cafValid = true;
        cafSetupRun = false;
        ui.saveLedIntensityBtn->setEnabled(true);
    }

    auto SetupCAFPage::Impl::SaveLedIntensity() -> void {
        const auto NA = ui.condenserNA->currentData().toDouble();
        const auto intensity = ui.ledIntensity->value();
        if(NA == 0) return;

        if(!control.SaveLEDIntensity(NA, intensity)) {
            TC::MessageDialog::warning(p, tr("Setup"),
                                       tr("Failed to save LED Intensity for NA %1").arg(NA));
            return;
        }

        ui.intensitySavedLabel->show();
        QTimer::singleShot(2000, [this]() { ui.intensitySavedLabel->hide(); });
    }

    auto SetupCAFPage::Impl::UpdateScores(const QList<double>& scores, int32_t bestFocusIndex) -> void {
        if(!cafSetupRun) return;

        auto* series = new QLineSeries();
        auto counts = scores.size();
        for(auto idx=0; idx<counts; idx++) {
            series->append(idx, scores.at(idx));
        }

        chart->removeAllSeries();
        chart->addSeries(series);
        chart->createDefaultAxes();
        auto* xAxis = qobject_cast<QValueAxis*>(chart->axes(Qt::Horizontal).at(0));
        xAxis->setLabelFormat("%d");

        ui.bfIndex->setValue(bestFocusIndex);

        if(bestFocusIndex < counts) {
            ui.bfScore->setValue(scores.at(bestFocusIndex));
        } else {
            ui.bfScore->setValue(0);
            TC::MessageDialog::warning(p, "Setup", tr("Invalid best focus index = %1").arg(bestFocusIndex));
        }
    }

    auto SetupCAFPage::Impl::ClearResult() -> void {
        chart->removeAllSeries();
        ui.bfIndex->setValue(0);
        ui.bfScore->setValue(0);

        ui.imageView->ClearImage();
        ui.imageIndex->setValue(0);
        ui.averageBrightness->setValue(0);
    }

    auto SetupCAFPage::Impl::ResultTabChanged(int32_t index) -> void {
        if(index==1) {
            auto count = control.GetDiskImageCount();
            ui.imageIndex->setRange(0, count-1 );
            ui.imageIndex->setToolTip(QString("Index range : 0 - %1").arg(count - 1));
        }
    }

    auto SetupCAFPage::Impl::UpdateDiskImage() -> void {
        auto index = ui.imageIndex->value();
        QImage image;
        double meanIntensity;

        if(!control.LoadDiskImage(index, image, meanIntensity)) {
            ui.imageView->ClearImage();
            ui.averageBrightness->setValue(0);
            return;
        }

        ui.imageView->ShowImage(image);
        ui.averageBrightness->setValue(meanIntensity);
    }

    auto SetupCAFPage::Impl::RefreshBFImage(bool refresh) -> void {
        if (refresh) {
            ui.bfRefreshBtn->setText("Live");
            StartUpdate(true);
        } else {
            ui.bfRefreshBtn->setText("Paused");
            StartUpdate(false);
        }
    }

    auto SetupCAFPage::Impl::ChangeCAxisRelative(double relativeMm) -> void {
        auto base = ui.cAxisBaseLine->value();
        auto target = base + relativeMm;

        control.MoveCAxis(target);
    }

    auto SetupCAFPage::Impl::UpdateCAxisPosition(double posMm) -> void {
        ui.cAxisCurrent->setValue(posMm);
    }

    auto SetupCAFPage::Impl::SaveCAxisOffset() -> void {
        const auto NA = ui.condenserNA->currentData().toDouble();
        const auto cAxisOffset = ui.cAxisOffset->value();
        if(NA == 0) return;

        if(!control.SaveCAxisOffset(NA, cAxisOffset)) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("Failed to save C Axis Offset for NA %1").arg(NA));
            return;
        }

        ui.cOffsetSavedLabel->show();
        QTimer::singleShot(2000, [this]() {ui.cOffsetSavedLabel->hide(); });
    }

    auto SetupCAFPage::Impl::StartUpdate(bool start) -> void {
        if(start) updateTimer.start(200);
        else updateTimer.stop();
    }

    auto SetupCAFPage::Impl::UpdateImage() -> void {
        QImage image;
        if(!control.GetLatestImage(image)) return;

        ui.imageView->ShowImage(image);
    }

    SetupCAFPage::SetupCAFPage(QWidget* parent) : QWidget(parent), d{new Impl(this)} {
        d->ui.setupUi(this);
        d->InitUi();

        d->instObserver = new InstrumentObserver(this);
        d->motionObserver = new MotionObserver(this);

        connect(d->ui.controlTabWidget, &QTabWidget::currentChanged, this, [this](int32_t index) {
            d->ControlTabChanged(index);
        });

        connect(d->ui.condenserNA, &QComboBox::currentTextChanged, this, [=]() {
            d->ChangeNA();
        });

        connect(d->ui.runCAFBtn, &QPushButton::clicked, this, [this]() {
            d->RunCAF();
        });

        connect(d->ui.saveLedIntensityBtn, &QPushButton::clicked, this, [this]() {
            d->SaveLedIntensity();
        });

        connect(d->instObserver, &InstrumentObserver::sigUpdateCAFScores, this, 
                [this](const QList<double>& scores, int32_t bestFocusIndex) {
            d->UpdateScores(scores, bestFocusIndex);
        });

        connect(d->ui.resultTabWidget, &QTabWidget::currentChanged, this, [this](int32_t index) {
            d->ResultTabChanged(index);
        });

        connect(d->ui.imageIndex, &QSpinBox::editingFinished, this, [this]() {
            d->UpdateDiskImage();
        });

        connect(d->ui.bfRefreshBtn, &QAbstractButton::toggled, this, [this]() {
            d->RefreshBFImage(d->ui.bfRefreshBtn->isChecked());
        });

        connect(&d->updateTimer, &QTimer::timeout, this, [this]() {
            d->UpdateImage();
        });

        connect(d->ui.cAxisOffset, &QAbstractSpinBox::editingFinished, this, [this]() {
            d->ChangeCAxisRelative(d->ui.cAxisOffset->value());
        });

        connect(d->ui.cAxisMinusBtn, &QPushButton::clicked, this, [this]() {
            d->ui.cAxisOffset->setValue(d->ui.cAxisOffset->value() - d->ui.cAxisDelta->value());
            d->ChangeCAxisRelative(d->ui.cAxisOffset->value());
        });

        connect(d->ui.cAxisPlusBtn, &QPushButton::clicked, this, [this]() {
            d->ui.cAxisOffset->setValue(d->ui.cAxisOffset->value() + d->ui.cAxisDelta->value());
            d->ChangeCAxisRelative(d->ui.cAxisOffset->value());
        });

        connect(d->motionObserver, &MotionObserver::sigUpdateGlobalAxisPosition, this, [this](AppEntity::Axis axis, double posInMM) {
            if(axis != +AppEntity::Axis::C) return;
            d->UpdateCAxisPosition(posInMM);
        });

        connect(d->ui.saveCOffsetBtn, &QPushButton::clicked, this, [this]() {
            d->SaveCAxisOffset();
        });

        for (const auto& label : findChildren<QLabel*>()) {
            if (label->objectName().contains("subtitle")) {
                label->setObjectName("label-h5");
            }
            else if (label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
            else if (label->objectName().contains("range")) {
                label->setObjectName("label->h6");
            }
        }

        for (const auto& button : findChildren<QPushButton*>()) {
            if (button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            }
            else {
                button->setObjectName("bt-maintenance-light");
            }
        }

        for (const auto& groupBox : findChildren<QGroupBox*>()) {
            groupBox->setObjectName("gb-maintenance-main");
        }

        d->ui.imgBotWidget->setObjectName("panel");
        d->ui.chartBotWidget->setObjectName("panel");
    }
    
    SetupCAFPage::~SetupCAFPage() {
    }

    auto SetupCAFPage::Enter() -> void {
    }

    auto SetupCAFPage::Leave() -> void {
    }

    void SetupCAFPage::resizeEvent(QResizeEvent* event) {
        d->ui.imageView->FitZoom();
        QWidget::resizeEvent(event);
    }
}
