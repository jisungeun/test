#pragma once
#include <memory>
#include <QImage>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class LiveImageSource {
    public:
        using Pointer = std::shared_ptr<LiveImageSource>;

    protected:
        LiveImageSource();

    public:
        ~LiveImageSource();
        static auto GetInstance()->Pointer;

        auto GetLatestImage(QImage& image)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}