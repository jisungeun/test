#include <ProjectController.h>

#include "ExperimentSelectDialogControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct ExperimentSelectDialogControl::Impl {
    };

    ExperimentSelectDialogControl::ExperimentSelectDialogControl() : d{new Impl} {
    }

    ExperimentSelectDialogControl::~ExperimentSelectDialogControl() {
    }

    auto ExperimentSelectDialogControl::GetProjects() const -> QStringList {
        auto controller = Interactor::ProjectController();
        return controller.GetProjects();
    }

    auto ExperimentSelectDialogControl::GetExperiments(const QString& project) const -> QStringList {
        auto controller = Interactor::ProjectController();
        return controller.GetExperiments(project);
    }
}