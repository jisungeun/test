#pragma once
#include <memory>

#include <QWidget>

#include "SetupPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SetupIntensityPage : public QWidget, public SetupPage {
        Q_OBJECT

    public:
        SetupIntensityPage(QWidget* parent =  nullptr);
        ~SetupIntensityPage() override;

        auto Enter() -> void override;
        auto Leave() -> void override;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}