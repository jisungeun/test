#include <QList>

#include "EvaluationBeadUpdater.h"
#include "EvaluationBeadObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationBeadUpdater::Impl {
        QList<EvaluationBeadObserver*> observers;
    };

    EvaluationBeadUpdater::EvaluationBeadUpdater() : d{ std::make_unique<Impl>() } {
    }

    EvaluationBeadUpdater::~EvaluationBeadUpdater() {
    }

    auto EvaluationBeadUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new EvaluationBeadUpdater() };
        return theInstance;
    }

    auto EvaluationBeadUpdater::UpdateAcquisitionProgress(double progress) -> void {
        for(auto observer : d->observers) {
            observer->UpdateAcquisitionProgress(progress);
        }
    }

    auto EvaluationBeadUpdater::NotifyAcquisitionError(const QString& error) -> void {
        for(auto observer : d->observers) {
            observer->NotifyAcquisitionError(error);
        }
    }

    auto EvaluationBeadUpdater::NotifyAcquisitionStopped() -> void {
        for(auto observer : d->observers) {
            observer->NotifyAcquisitionStopped();
        }
    }

    auto EvaluationBeadUpdater::UpdateEvaluationProgress(double progress) -> void {
        for(auto observer : d->observers) {
            observer->UpdateEvaluationProgress(progress);
        }
    }

    auto EvaluationBeadUpdater::Register(EvaluationBeadObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto EvaluationBeadUpdater::Deregister(EvaluationBeadObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}
