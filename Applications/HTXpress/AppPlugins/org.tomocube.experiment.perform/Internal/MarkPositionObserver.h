﻿#pragma once

#include <memory>

#include <QObject>
#include <IMarkPositionView.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class MarkPositionObserver : public QObject {
        Q_OBJECT
    public:
        explicit MarkPositionObserver(QObject* parent = nullptr);
        ~MarkPositionObserver() override;

        auto AddMarkPosition(AppEntity::WellIndex wellIndex, AppEntity::Location::Pointer location) -> void;

    signals:
        void sigAddMarkPosition(AppEntity::WellIndex, AppEntity::Location::Pointer);
    };
}
