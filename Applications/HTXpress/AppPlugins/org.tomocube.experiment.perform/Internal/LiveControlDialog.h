#pragma once

#include <enum.h>
#include <memory>

#include <QDialog>

#include <SystemStatus.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class LiveControlDialog : public QDialog {
        Q_OBJECT
    public:
        LiveControlDialog(QWidget* parent = nullptr);
        ~LiveControlDialog() override;

        auto SetMode(AppEntity::ImagingMode mode)->void;
        auto GetMode()->AppEntity::ImagingMode;

        auto SetImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode)->void;
        auto GetImagingSettingMode() const -> AppEntity::ImagingSettingMode;

        auto SetIntensity(uint32_t value)->void;
        auto GetIntensity()->uint32_t;

        auto SetExposure(uint32_t value)->void;
        auto GetExposure()->uint32_t;

        auto SetGain(double value)->void;
        auto GetGain()->double;

        auto SetExposureRange(uint32_t minVal, uint32_t maxVal)->void;
        auto ShowBoostMode(bool show)->void;

        auto SetEnabledSaveButton(bool enabled)->void;

        auto SetSaturation(bool show)->void;
        auto GetSaturation()const->bool;

    protected:
        auto showEvent(QShowEvent* event) -> void override;
        auto hideEvent(QHideEvent* event) -> void override;

    protected slots:
        void onChangedSliderValue(int value);
        void onChangedSpinBoxValue(double value);
        void onBoost(bool checked);
        void onChangedSaturationMode(bool checked);

    signals:
        void sigLiveImageConditionChanged(const uint32_t intensity, const  uint32_t exposure, const double gain);
        void sigLiveSaturationActivated(bool activated);
        void sigAcquisitionConditionRequested();
        void sigSaveRequested();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}