#include <QMap>
#include <QVariant>
#include <QElapsedTimer>

#include <System.h>
#include <SystemStatus.h>
#include <InstrumentController.h>

#include "Utility.h"
#include "InstrumentUpdater.h"
#include "LiveImageSource.h"
#include "EvaluationReportWriter.h"
#include "EvaluationHTIllumCalPageControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationHTIllumCalPageControl::Impl {
        struct {
            QImage image;
            bool pass{ false };
            double NA{ 0 };
            int32_t intensity{ 0 };
        } result;
    };

    EvaluationHTIllumCalPageControl::EvaluationHTIllumCalPageControl() : d{ std::make_unique<Impl>()} {
    }

    EvaluationHTIllumCalPageControl::~EvaluationHTIllumCalPageControl() {
    }

    auto EvaluationHTIllumCalPageControl::GetNAs() const -> QList<double> {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        return sysConfig->GetIlluminationCalibrationParameterNAs();
    }

    auto EvaluationHTIllumCalPageControl::RunCAF(double NA) -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto experiment = sysStatus->GetExperiment();

        auto originalNA = Utility::Service::OverrideNA(experiment, NA);

        bool success = false;
        try {
            auto instUpdater = InstrumentUpdater::GetInstance();
            auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
            auto instController = Interactor::InstrumentController(&instPresenter);

            success = instController.PerformCondenserAutoFocus();
        } catch(...) {
            success = false;
        }

        Utility::Service::OverrideNA(experiment, originalNA);

        return success;
    }

    auto EvaluationHTIllumCalPageControl::RunHTIllumCalibration(double NA) -> bool {
        d->result.NA = NA;
        d->result.pass = false;
        d->result.intensity = 0;

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto experiment = sysStatus->GetExperiment();

        auto originalNA = Utility::Service::OverrideNA(experiment, NA);

        try {
            auto instUpdater = InstrumentUpdater::GetInstance();
            auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
            auto instController = Interactor::InstrumentController(&instPresenter);

            if(instController.CalibrateHTIllumination()) {
                d->result.pass = true;

                auto htConfig = sysStatus->GetChannelConfig(AppEntity::ImagingMode::HT3D);
                d->result.intensity = htConfig->GetLightIntensity();
            }
        } catch(...) {
            d->result.pass = false;
        }

        Utility::Service::OverrideNA(experiment, originalNA);

        return d->result.pass;
    }

    auto EvaluationHTIllumCalPageControl::SetImage(const QImage& image) -> void {
        d->result.image = image;
    }

    auto EvaluationHTIllumCalPageControl::Clear() -> void {
        d->result.image = QImage();
        d->result.pass = false;
    }

    auto EvaluationHTIllumCalPageControl::Save(const QString& path) -> bool {
        QMap<QString, QVariant> results;
        results["NA"] = d->result.NA;
        results["intensity"] = d->result.intensity;
        results["pass"] = d->result.pass;

        auto conv = [=](const QString& name)->QString {
            return QString("NA%1.%2").arg(d->result.NA, 0, 'f', 2).arg(name);
        };

        auto writer = EvaluationReportWriter::GetInstance();
        writer->SetPath(path);
        writer->SetTitle("ht_illumination_calibration");
        if(!writer->SaveImage(conv("image"), d->result.image)) return false;
        if(!writer->SaveResults(conv("report"), results)) return false;

        return true;
    }
}
