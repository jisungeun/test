#pragma once
#include <memory>

#include "LiveviewAnnotationItem.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class LiveviewAnnotationBox : public LiveviewAnnotationItem {
    public:
        LiveviewAnnotationBox();
        ~LiveviewAnnotationBox() override;

        auto GetGraphicsItem() const -> QGraphicsItem* override;

        auto SetRect(double x, double y, double w, double h)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}