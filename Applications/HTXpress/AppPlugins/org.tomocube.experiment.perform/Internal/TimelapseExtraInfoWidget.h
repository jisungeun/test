﻿#pragma once

#include <QWidget>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class TimelapseExtraInfoWidget : public QWidget {
        Q_OBJECT
    public:
        enum class Type {Timelapse, Acquisition};
        using Self = TimelapseExtraInfoWidget;

        explicit TimelapseExtraInfoWidget(QWidget* parent = nullptr);
        ~TimelapseExtraInfoWidget() override;

        auto SetImagingPoints(int32_t count) -> void;
        auto GetImagingPoints() const -> int32_t;

        auto SetInterval(const QString& interval) -> void; // format should be 00:00:00 (hh:mm:ss)

        auto SetRequiredSpace(const int64_t& bytes) -> void;
        auto GetRequiredSpace() const -> int64_t;

        auto SetSpaceStatus(const bool& isEnough) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
