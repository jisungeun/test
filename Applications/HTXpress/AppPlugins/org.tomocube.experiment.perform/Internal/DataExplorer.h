#pragma once

#include <memory>

#include <QWidget>

#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class DataExplorer : public QWidget {
        Q_OBJECT

    public:
        DataExplorer(QWidget* parent = nullptr);
        ~DataExplorer();

    protected slots:
        void onVesselLoadClicked();
        void onUpdateVesselStatus(bool loaded);
        void onEndExperimentClicked();

        void onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool undo);

        void onScanAcquisitionData(const QString& user, const QString& project, const QString& experiment);
        void onAddAcquisitionData(const QString& fileFullPath);
        void onUpdateAcquisitionData(const QString& fileFullPath);
        void onDeleteAcquisitionData(const QString& fileFullPath);
        void onDeleteAcquisitionDataFolder(const QString& fileFullPath);

        void onUpdateStorageInfo(const int64_t& bytesTotal, const int64_t& bytesAvailable);
        void onUpdateMinRequiredSpace(const int32_t& gigabytes);
        void onRefreshStorageInfo();

    signals:
        void sigEndExperiment();
        void sigGotoExpSetup();
        void sigGotoDataNavigation();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}