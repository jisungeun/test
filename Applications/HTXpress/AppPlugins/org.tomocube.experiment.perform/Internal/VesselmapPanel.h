#pragma once
#include <memory>
#include <QWidget>
#include <QMap>

#include <Location.h>
#include <Experiment.h>
#include <VesselMapExternalData.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class VesselmapPanel final : public QWidget {
        Q_OBJECT

    public:
        using Self = VesselmapPanel;

        explicit VesselmapPanel(QWidget* parent = nullptr);
        ~VesselmapPanel() override;

    protected slots:
        void onAddPosition(AppEntity::LocationIndex locationIdx, AppEntity::WellIndex wellIdx, AppEntity::Location::Pointer location);
        void onAddMarkPosition(AppEntity::WellIndex wellIdx, AppEntity::Location::Pointer location);
        void onRefreshList(AppEntity::WellIndex wellIdx, QMap<AppEntity::LocationIndex, AppEntity::Location::Pointer>& locationList);
        void onDeletePosition(AppEntity::WellIndex wellIdx, AppEntity::LocationIndex locationIdx);
        void onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool reloaded);
        void onUpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position);
        void onUpdateGlobalPosition(const AppEntity::Position& position);
        void onUpdateCurrentWell(const AppEntity::WellIndex wellIndex);
        void onUpdateInitProgress(double progress, const QString& message);
        void onUpdateVesselStatus(bool loaded);
        void onSetCurrentWell(TC::WellIndex wellIndex, int32_t row, int32_t col);
        void onSetCurrentWell(TC::WellIndex wellIndex);
        void onChangeSampleStagePosition(double xMM, double yMM);
        void onChangeSampleStagePosition(double xMM, double yMM, double zMM);
        void onDeleteAcquisitionPoint(const TC::WellIndex& wellIdx, const TC::AcquisitionIndex& pointIndex);
        void onChangePosition(const TC::VesselAxis axis, const double posInMM);
        void onUpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex, double xInMm, double yInMm, double widthInUm, double heightInUm);
        void onUpdateFOV(const double xInUm, const double yInUm);
        void onTileImagingAreaChanged(double xMM, double yMM, double zMM, double widthMM, double heightMM);
        void onUpdateRoiSize(AppEntity::Experiment::Pointer experiment);
        void onCustomPreviewAreaChanged(const double& xMM, const double& yMM, const double& widthMM, const double& heightMM);
        void onStartCustomPreviewAreaSetting();
        void onCancelCustomPreviewAreaSetting();
        void onChangeImagingOrder(const TC::ImagingOrder& order);
        void onUpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM);
        void onUpdateSingleAcquireProgress(const double progress, const int elapsedSeconds, const int remainSeconds);
        void onCopyImagingPoints(const TC::WellIndex& sourceWellIndex, const QList<TC::AcquisitionIndex>& targetPositionIndices);
        void onSetPreviewArea(double xMM, double yMM, double widthMM, double heightMM);
        void onShowTileArea(bool show);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
