#include <InstrumentController.h>
#include <MotionController.h>

#include "MotionUpdater.h"
#include "InstrumentUpdater.h"
#include "FocusPanelControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct FocusPanelControl::Impl {
    };

    FocusPanelControl::FocusPanelControl() : d{new Impl} {
    }

    FocusPanelControl::~FocusPanelControl() {
    }

    auto FocusPanelControl::PerformAF() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return controller.PerformAutoFocus();
    }

    auto FocusPanelControl::SetBestFocus() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return controller.SetBestFocusCurrent();
    }

    auto FocusPanelControl::EnableAutoFocus(bool enable) -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return enable ? controller.EnableAutoFocus() : controller.DisalbeAutoFocus();
    }

    auto FocusPanelControl::MoveZ(double distUm) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveRelativeUM(AppEntity::Axis::Z, distUm);
    }

    auto FocusPanelControl::StartJog(bool plusDirection) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.StartJog(AppEntity::Axis::Z, plusDirection);
    }

    auto FocusPanelControl::StopJog() -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.StopJog();
    }
}
