#include <System.h>
#include <SystemConfig.h>

#include <MotionController.h>
#include <InstrumentController.h>
#include <SystemConfigController.h>

#include "SetupAutofocusPageControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupAutofocusPageControl::Impl {
        const int32_t counts{ 100 };
    };

    SetupAutofocusPageControl::SetupAutofocusPageControl() : d{new Impl} {
    }

    SetupAutofocusPageControl::~SetupAutofocusPageControl() {
    }

    auto SetupAutofocusPageControl::GetParameters() const -> std::tuple<int32_t, int32_t, int32_t> {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        return std::make_tuple(sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::MinSensorValue),
                               sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::MaxSensorValue),
                               sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::ResolutionDiv));
    }

    auto SetupAutofocusPageControl::GetSteps() const->int32_t {
        return d->counts;
    }

    auto SetupAutofocusPageControl::Scan(double lowerRelPos, double upperRelPos) const->QList<int32_t> {
        double currentZPosMm{ 0 };
        if(!Interactor::MotionController().GetGlobalPosition(AppEntity::Axis::Z, currentZPosMm)) {
            return QList<int32_t>();
        }

        auto controller = Interactor::InstrumentController();
        auto startMm = currentZPosMm + lowerRelPos;
        auto distMm = upperRelPos - lowerRelPos;

        return controller.ScanFocus(startMm, distMm, d->counts);
    }

    auto SetupAutofocusPageControl::CalcSlope(double sensorRange, double distRange) const -> int32_t {
        auto model = AppEntity::System::GetModel();
        auto distPulses = distRange * model->AxisResolutionPPM(AppEntity::Model::Axis::Z);

        return static_cast<int32_t>(sensorRange / distPulses);
    }

    auto SetupAutofocusPageControl::ReadSensorValue() const -> int32_t {
        auto controller = Interactor::InstrumentController();
        return controller.ReadAFSensorValue();
    }

    auto SetupAutofocusPageControl::SaveParameter(int32_t minSensor, int32_t maxSensor, int32_t divResolution, int32_t defaultSensor) -> bool {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::MinSensorValue, minSensor);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::MaxSensorValue, maxSensor);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::ResolutionDiv, divResolution);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::DefaultTargetValue, defaultSensor);

        auto controller = Interactor::SystemConfigController();
        return controller.Save();
    }
}
