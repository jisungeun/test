#define LOGGER_TAG "[ControlPanel]"
#include <QDebug>

#include <TCLogger.h>
#include <MessageDialog.h>

#include <System.h>
#include <SystemStatus.h>
#include <SessionManager.h>

#include "MotionObserver.h"
#include "ImagingPlanObserver.h"
#include "ExperimentIOObserver.h"
#include "LiveviewConfigObserver.h"
#include "InstrumentObserver.h"
#include "RunExperimentObserver.h"
#include "ExperimentProgressDialog.h"
#include "AcquisitionPositionObserver.h"
#include "PreviewObserver.h"
#include "VesselObserver.h"
#include "SystemStorageObserver.h"

#include "Utility.h"
#include "FLConditionParser.h"
#include "ControlPanelControl.h"
#include "ControlPanel.h"
#include "ui_ControlPanel.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    using ModalityType = AppComponents::ImagingConditionPanel::ModalityType;
    using FLChannelConfig = AppComponents::ImagingConditionPanel::FLChannelConfig;

    using MarkType = bool;
    constexpr bool SingleTileType = true;
    constexpr bool SinglePointType = false;

    struct ControlPanel::Impl {
        Ui::ControlPanel ui;
        ControlPanelControl control;

        ExperimentProgressDialog* experimentProgress{ nullptr };

        InstrumentObserver* instrumentObserver{ nullptr };
        MotionObserver* motionObserver{ nullptr };
        ImagingPlanObserver* planObserver{ nullptr };
        ExperimentIOObserver* experimentIOObserver{ nullptr };
        LiveviewConfigObserver* liveviewConfigObserver{ nullptr };
        RunExperimentObserver* experimentObserver{ nullptr };
        AcquisitionPositionObserver* positionObserver{ nullptr };
        PreviewObserver* previewObserver { nullptr };
        VesselObserver* vesselObserver { nullptr };
        SystemStorageObserver* storageObserver { nullptr };

        auto CustomROIChecked() const->bool;
        auto GetAcquisitionArea() const->AppEntity::Area;
        auto Convert(const QList<AppEntity::ImagingType>& imagingTypes)->QList<ModalityType>;
        auto Convert(const std::tuple<int32_t, int32_t>& fovInPixels)->std::tuple<double, double>;
        auto Convert(const AppEntity::FLChannel& input)->FLChannelConfig;
        auto Convert(const FLChannelConfig& flConfig)->AppEntity::ChannelConfig;
        auto Convert(const QList<ModalityType>& modalities)->QList<ControlPanelControl::ImageType>;
        auto Convert(const AppEntity::ImagingMode& mode)->std::tuple<AppEntity::Modality, int32_t>;

        auto Refresh3DInterval()->void;
        auto EnableAcquireButtons(bool enable)->void;

        auto AddMarkPosition(MarkType type) -> void;
        auto GetImagingModeByTabIndex(int32_t index) -> AppEntity::ImagingSettingMode;

        auto InitSingleImagingPanel()->void;
        auto InitFlChannelList()->void;

        auto UpdateFLChannelInUse()->void;
        auto IsValid(const AppEntity::Experiment::Pointer& experiment) -> bool;
    };

    auto ControlPanel::Impl::CustomROIChecked() const -> bool {
        //TODO Tile scan 모드가 활성화 되어 있는지 확인하는 코드 (UC-021101)
        return false;
    }

    auto ControlPanel::Impl::GetAcquisitionArea() const -> AppEntity::Area {
        //TODO 사용자가 지정한 획득 영역 크기 정보 얻어 오기 구현
        AppEntity::Area area;
        return area;
    }

    auto ControlPanel::Impl::Convert(const QList<AppEntity::ImagingType>& imagingTypes) -> QList<ModalityType> {
        QList<ModalityType> modalityTypes;
        for(auto type : imagingTypes) {
            switch(type) {
            case AppEntity::ImagingType::HT3D:
                modalityTypes.push_back(ModalityType::HT3D);
                break;
            case AppEntity::ImagingType::HT2D:
                modalityTypes.push_back(ModalityType::HT2D);
                break;
            case AppEntity::ImagingType::FL3D:
                modalityTypes.push_back(ModalityType::FL3D);
                break;
            case AppEntity::ImagingType::FL2D:
                modalityTypes.push_back(ModalityType::FL2D);
                break;
            case AppEntity::ImagingType::BFColor:
                modalityTypes.push_back(ModalityType::BF);
                break;
            case AppEntity::ImagingType::BFGray:
                modalityTypes.push_back(ModalityType::BF);
                break;
            }
        }
        return modalityTypes;
    }

    auto ControlPanel::Impl::Convert(const std::tuple<int32_t, int32_t>& fovInPixels) -> std::tuple<double, double> {
        const auto resolution = AppEntity::System::GetCameraResolutionInUM();
        int32_t fovX, fovY;
        std::tie(fovX, fovY) = fovInPixels;
        return std::make_tuple(fovX*resolution, fovY*resolution);
    }

    auto ControlPanel::Impl::Convert(const AppEntity::FLChannel& input) -> FLChannelConfig {
        FLChannelConfig output;

        output.SetChannelName(input.GetName());
        output.SetExcitation(input.GetExcitation(), input.GetExcitationBandwidth());
        output.SetEmission(input.GetEmission(), input.GetEmissionBandwidth());

        return output;
    }

    auto ControlPanel::Impl::Convert(const FLChannelConfig& flConfig) ->AppEntity::ChannelConfig {
       AppEntity::ChannelConfig config;
        config.SetChannelName(flConfig.GetChannelName());
        config.SetLightChannel(Utility::GetExChannel(flConfig.GetExcitationWaveLength(), 
                                                     flConfig.GetExcitationBandWidth()));
        config.SetFilterChannel(Utility::GetEmChannel(flConfig.GetEmissionWaveLength(), 
                                                      flConfig.GetEmissionBandWidth()));
        config.SetLightIntensity(flConfig.GetIntensity());
        config.SetCameraExposureUSec(flConfig.GetExposure() * 1000);    //msec to usec
        return config;
    }

    auto ControlPanel::Impl::Convert(const QList<ModalityType>& modalities) -> QList<ControlPanelControl::ImageType> {
        using ImageType = ControlPanelControl::ImageType;

        QList<ControlPanelControl::ImageType> list;

        for(auto modality : modalities) {
            switch(modality) {
            case ModalityType::HT3D:
                list.push_back(ImageType::HT3D);
                break;
            case ModalityType::HT2D:
                list.push_back(ImageType::HT2D);
                break;
            case ModalityType::FL3D:
                list.push_back(ImageType::FL3D);
                break;
            case ModalityType::FL2D:
                list.push_back(ImageType::FL2D);
                break;
            case ModalityType::BF:
                list.push_back(ImageType::BFGray);
                break;
            }
        }

        return list;
    }

    auto ControlPanel::Impl::Convert(const AppEntity::ImagingMode& mode) -> std::tuple<AppEntity::Modality, int32_t> {
        auto modality = AppEntity::Modality::HT;
        auto channel = 0;

        switch(mode) {
        case AppEntity::ImagingMode::HT2D:
        case AppEntity::ImagingMode::HT3D:
            break;
        case AppEntity::ImagingMode::FLCH0:
        case AppEntity::ImagingMode::FLCH1:
        case AppEntity::ImagingMode::FLCH2:
            modality = AppEntity::Modality::FL;
            channel = mode - AppEntity::ImagingMode::FLCH0;
            break;
        case AppEntity::ImagingMode::BFGray:
        case AppEntity::ImagingMode::BFColor:
            modality = AppEntity::Modality::BF;
            break;
        }

        return std::make_tuple(modality, channel);
    }

    auto ControlPanel::Impl::Refresh3DInterval() -> void {
        QList<int32_t> enabledFLChannels;

        for(int ch = 0; ch < 3; ++ch) {
            if(ui.singleImagingPanel->GetFLEnabled(ch)) {
                enabledFLChannels << ch;
            }
        }

        qDebug() << "enabledFLChannels=" << enabledFLChannels;

        control.SetEstimated3DModeInterval(enabledFLChannels);
    }

    auto ControlPanel::Impl::EnableAcquireButtons(bool enable) -> void {
        const QString msg = enable ? "" : tr("Set the best focus position before proceeding.");
        ui.singleImagingPanel->EnableAcquire(enable, msg);
        ui.timelapseAcqButtons->setEnabled(enable);
        ui.acquireButton->setEnabled(enable);
        ui.acquireButton->setToolTip(msg);
        ui.testButton->setEnabled(enable);
        ui.testButton->setToolTip(msg);
    }

    auto ControlPanel::Impl::AddMarkPosition(MarkType type) -> void {
        auto [xInMM, yInMM] = ui.singleImagingPanel->GetTileCenterInMM();
        auto [widthInUM, heightInUM] = ui.singleImagingPanel->GetTileSizeInUM();

        if(type == SinglePointType) {
            xInMM = control.GetCurrentPositionInWell().toMM().x;
            yInMM = control.GetCurrentPositionInWell().toMM().y;
            widthInUM = ui.singleImagingPanel->GetFovX();
            heightInUM = ui.singleImagingPanel->GetFovY();
        }

        control.AddMarkPosition(xInMM, yInMM, AppEntity::Area::fromUM(widthInUM, heightInUM), type);
    }

    auto ControlPanel::Impl::GetImagingModeByTabIndex(int32_t index) -> AppEntity::ImagingSettingMode {
        // ui tab 위치 변경되면 로직도 수정되어야 함.
        if(index == 0) return AppEntity::ImagingSettingMode::SingleImaging;
        if(index == 1) return AppEntity::ImagingSettingMode::TimelapseImaging;

        return AppEntity::ImagingSettingMode::SingleImaging;
    }

    auto ControlPanel::Impl::InitSingleImagingPanel() -> void {
        const auto model = AppEntity::System::GetModel();
        const auto minExposure = model->MinimumExposureUSec() / 1000;
        const auto maxExposure = model->MaximumExposureUSec() / 1000;
        ui.singleImagingPanel->SetExposureRange(minExposure, maxExposure);

        const auto fov = AppEntity::System::GetMaximumFOV();
        ui.singleImagingPanel->SetMaximumFOV(fov.toUM().width, fov.toUM().height);
        ui.singleImagingPanel->InitStepValue(0.5);

        ui.singleImagingPanel->SetFLMaximumSlices(model->FLMaximumSlices());

        InitFlChannelList();
    }

    auto ControlPanel::Impl::InitFlChannelList() -> void {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto flChannelCount = sysConfig->GetFLChannels();
        QList<FLChannelConfig> flConfigs;
        for(const auto& flChannel : flChannelCount) {
            AppEntity::FLChannel flConfig;
            if(!sysConfig->GetFLChannel(flChannel, flConfig)) continue;
            flConfigs.push_back(Convert(flConfig));
        }
        ui.singleImagingPanel->blockSignals(true);
        ui.singleImagingPanel->SetFLChannels(flConfigs);
        ui.singleImagingPanel->blockSignals(false);
    }

    auto ControlPanel::Impl::UpdateFLChannelInUse() -> void {
        auto& timelapsePanel = ui.timelapseImagingPanel;
        auto& singleImgPanel = ui.singleImagingPanel;
        using ChannelIndex = int32_t;
        QList<ChannelIndex> indices; 
        for(int i = 0; i < 3; i++) {
            if(timelapsePanel->IsFLChannelInUse(i)) {
                indices.push_back(i);
            }
        }
        singleImgPanel->SetFLChannelIndexInUse(indices);
    }

    auto ControlPanel::Impl::IsValid(const AppEntity::Experiment::Pointer& experiment) -> bool {
        const auto imagingTypes = experiment->GetEnabledImagingTypes();

        if (imagingTypes.contains(AppEntity::ImagingType::FL3D) || imagingTypes.contains(AppEntity::ImagingType::FL2D)) {
            FLConditionParser parser(experiment);
            auto sysConfig = AppEntity::System::GetSystemConfig();
            const auto flChannelCount = sysConfig->GetFLChannels();

            const auto channels = parser.GetChannels();
            for (auto channelIdx : channels) {
                const auto channelName = parser.GetName(channelIdx);
                const auto excitationBandWidth = parser.GetExFilterBandWidth(channelIdx);
                const auto excitationWaveLength = parser.GetExFilterWaveLength(channelIdx);
                const auto emissionBandWidth = parser.GetEmFIlterBandWidth(channelIdx);
                const auto emissionWaveLength = parser.GetEmFilterWaveLength(channelIdx);

                bool isFlValid{false};
                for (const auto& index : flChannelCount) {
                    AppEntity::FLChannel flChannel;
                    if (!sysConfig->GetFLChannel(index, flChannel)) continue;

                    if (flChannel.GetName() == channelName
                        && static_cast<int32_t>(flChannel.GetEmission()) == emissionWaveLength
                        && static_cast<int32_t>(flChannel.GetEmissionBandwidth()) == emissionBandWidth
                        && static_cast<int32_t>(flChannel.GetExcitation()) == excitationWaveLength
                        && static_cast<int32_t>(flChannel.GetExcitationBandwidth()) == excitationBandWidth) {
                        isFlValid = true;
                    }
                }
                if(!isFlValid) {
                    return false;
                }
            }
        }

        return true;
    }

    ControlPanel::ControlPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.baseWidget->setObjectName("panel");

        d->instrumentObserver = new InstrumentObserver(this);
        d->motionObserver = new MotionObserver(this);
        d->planObserver = new ImagingPlanObserver(this);
        d->experimentIOObserver = new ExperimentIOObserver(this);
        d->liveviewConfigObserver = new LiveviewConfigObserver(this);
        d->experimentObserver = new RunExperimentObserver(this);
        d->positionObserver = new AcquisitionPositionObserver(this);
        d->previewObserver = new PreviewObserver(this);
        d->vesselObserver = new VesselObserver(this);
        d->storageObserver = new SystemStorageObserver(this);

        d->ui.tabWidget->setCurrentIndex(0);

        d->experimentProgress = new ExperimentProgressDialog(this);
        d->experimentProgress->setWindowModality(Qt::WindowModal);
        d->experimentProgress->hide();

        d->InitSingleImagingPanel();
        //Timelapse Imaging Panel
        d->ui.timelapseImagingPanel->SetPanelType(AppComponents::TimelapseImagingPanel::PanelType::Edit);

        d->ui.timelapseImagingPageDivider->setObjectName("line-divider");

		d->ui.testButton->setObjectName("bt-tsx-test");
        d->ui.acquireButton->setObjectName("bt-tsx-acquire");
        d->ui.timelapseAcqButtons->setObjectName("panel");

        d->EnableAcquireButtons(false);
        
        //Acquisition Panel
        connect(d->instrumentObserver, SIGNAL(sigUpdateGlobalPosition(const AppEntity::Position&)), this,
                SLOT(onUpdateGlobalPosition(const AppEntity::Position&)));
        connect(d->instrumentObserver, SIGNAL(sigUpdateBestFocus(double)), this,
                SLOT(onUpdateBestFocus(double)));
        connect(d->instrumentObserver, SIGNAL(sigVesselLoaded(bool)), this,
                SLOT(onUpdateVesselStatus(bool)));
        connect(d->motionObserver, SIGNAL(sigUpdatePosition(const AppEntity::WellIndex, const AppEntity::Position&)), this,
                SLOT(onUpdatePosition(const AppEntity::WellIndex, const AppEntity::Position&)));
        connect(d->motionObserver, SIGNAL(sigUpdateGlobalPosition(const AppEntity::Position&)), this,
                SLOT(onUpdateGlobalPosition(const AppEntity::Position&)));
        connect(d->motionObserver, SIGNAL(sigUpdateBestFocus(double)), this,
                SLOT(onUpdateBestFocus(double)));
        connect(d->planObserver, SIGNAL(sigUpdate(const UseCase::ImagingTimeTable::Pointer&)), this, 
                SLOT(onUpdatePlan(const UseCase::ImagingTimeTable::Pointer&)));
        connect(d->planObserver, SIGNAL(sigUpdate3DMinimumInterval(const UseCase::ImagingTimeTable::Pointer&)), this, 
                SLOT(onUpdate3DFullPlan(const UseCase::ImagingTimeTable::Pointer&)));
        connect(d->planObserver, SIGNAL(sigClearFLChannels()), this, SLOT(onClearFLChannels()));
        connect(d->planObserver, SIGNAL(sigUpdateFOV(const double, const double)), this, 
                SLOT(onUpdateFOV(const double, const double)));
        connect(d->planObserver, SIGNAL(sigUpdateTileScanArea(bool, AppEntity::WellIndex, double, double, double, double)), this, 
                SLOT(onUpdateTileScanArea(bool, AppEntity::WellIndex, double, double, double, double)));
        connect(d->planObserver, &ImagingPlanObserver::sigUpdateTileActivation, this, &Self::onUpdateTileActivation);
        connect(d->experimentIOObserver, SIGNAL(sigUpdate(AppEntity::Experiment::Pointer, bool)), this,
                SLOT(onUpdateExperiment(AppEntity::Experiment::Pointer, bool)));
        connect(d->liveviewConfigObserver, SIGNAL(sigLiveviewConfigChanged(const uint32_t, const uint32_t, const double)), this,
                SLOT(onUpdateLiveConfig(const uint32_t, const uint32_t, const double)));
        connect(d->liveviewConfigObserver, SIGNAL(sigAcquisitionConfigChanged(const AppEntity::ImagingMode, const uint32_t, const uint32_t, const double)), this,
                SLOT(onUpdateAcquisitionConfig(const AppEntity::ImagingMode, const uint32_t, const uint32_t, const double)));
        connect(d->experimentObserver, SIGNAL(sigError(const QString&)), this, SLOT(onExperimentStoppedByError(const QString&)));
        connect(d->experimentObserver, SIGNAL(sigUpdateMessage(const QString&)), this, SLOT(onShowExperimentMessage(const QString&)));
        connect(d->experimentObserver, 
                SIGNAL(sigUpdateCurrentPosition(const AppEntity::WellIndex, const AppEntity::Position&)), 
                this,
                SLOT(onUpdatePosition(const AppEntity::WellIndex, const AppEntity::Position&)));
      
        connect(d->ui.singleImagingPanel, SIGNAL(sigChangeFov(double, double)), this, SLOT(onFovChanged(double, double)));
        connect(d->ui.singleImagingPanel, SIGNAL(sigChangeActiveModality()), this, SLOT(onModalitiesChanged()));
        connect(d->ui.singleImagingPanel, SIGNAL(sigChangeAcquisitionTableLock(bool)), this, SLOT(onChangeAcquisitionTableLock(bool)));
        connect(d->ui.singleImagingPanel,
                SIGNAL(sigChangeAcquisitionTableData(int32_t, const HTXpress::AppComponents::ImagingConditionPanel::FLChannelConfig&)), 
                this, 
                SLOT(onApplyFLImagingCondition(int32_t, const HTXpress::AppComponents::ImagingConditionPanel::FLChannelConfig&)));
        connect(d->ui.singleImagingPanel, SIGNAL(sigChangeChannelEnabled(int32_t, bool)), this, SLOT(onChangeFLChannelEnabled(int32_t, bool)));
        connect(d->ui.singleImagingPanel, SIGNAL(sigApplyFLScanCondition()), this, SLOT(onApplyFLScanCondition()));
        connect(d->ui.singleImagingPanel, SIGNAL(sigUndoFLScanCondition()), this, SLOT(onUndoFLScanCondition()));
        connect(d->ui.singleImagingPanel, SIGNAL(sigTileImagingChanged()), this, SLOT(onTileImagingConditionChanged()));
        connect(d->ui.singleImagingPanel, SIGNAL(sigAddAcquisitionPoint()), this, SLOT(onAddPoint()));
        connect(d->ui.singleImagingPanel, SIGNAL(sigAddAcquisitionTile(double ,double)), this, SLOT(onAddTile(double, double)));
        connect(d->ui.singleImagingPanel, SIGNAL(sigAcquirePoint()), this, SLOT(onAcquireSingle()));
        connect(d->ui.singleImagingPanel, SIGNAL(sigAcquireTile()), this, SLOT(onAcquireSingleTile()));
        connect(d->ui.singleImagingPanel, SIGNAL(sigZStackZPosPressed(double)), this, SLOT(onMoveZAxis(double)));

        connect(d->ui.timelapseImagingPanel, SIGNAL(sigSequencesChanged(const QList<TimelapseSequence>&)),
            this, SLOT(onApplySequences(const QList<TimelapseSequence>&)));
        
        connect(d->ui.tabWidget, SIGNAL(currentChanged(int)), this, SLOT(onChangeImagingTabIndex(int)));
        connect(d->ui.testButton, SIGNAL(clicked()), this, SLOT(onTestAcquire()));
        connect(d->ui.acquireButton, SIGNAL(clicked()), this, SLOT(onAcquire()));
        connect(d->experimentProgress, SIGNAL(sigStop()), this, SLOT(onStopAcquisition()));

        connect(d->positionObserver, SIGNAL(sigDeletePosition(AppEntity::WellIndex, AppEntity::LocationIndex)), 
                this, SLOT(onDeleteAcquisitionPoint()));

        connect(d->vesselObserver, &VesselObserver::sigUpdateSafeMovingRange, this, &ControlPanel::onUpdateSafeMovingRange);
        connect(d->positionObserver, &AcquisitionPositionObserver::sigAddPosition, this, &ControlPanel::onPositionAdded);

        connect(d->planObserver, &ImagingPlanObserver::sigUpdateAcquisitionDataRequiredSpace, this, &ControlPanel::onUpdateAcquisitionDataRequiredSpace);
        connect(d->storageObserver, &SystemStorageObserver::sigUpdateSystemStorageSpace, this, &ControlPanel::onUpdateSystemStorageSpace);
    }

    ControlPanel::~ControlPanel() {
    }

    void ControlPanel::onUpdateVesselStatus(bool loaded) {
        d->ui.singleImagingPanel->setEnabled(loaded);
        d->ui.timelapseImagingPanel->setEnabled(loaded);
        d->ui.timelapseAcqButtons->setEnabled(loaded);
        if(!loaded) {
            d->control.TileDeactivation();
        }
    }

    void ControlPanel::onUpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position) {
        d->ui.singleImagingPanel->SetCurrentZ(position.toMM().z);

        // FL Z Stack setting에서 focus button을 누를경우 wellIdx가 -1로 설정됨. 따라서 x,y pos 변경을 하지 않음
        if(wellIdx != -1) {
            d->ui.singleImagingPanel->SetCenterPosition(position.toMM().x, position.toMM().y);
        }
    }

    void ControlPanel::onUpdateGlobalPosition(const AppEntity::Position& position) {
        d->ui.singleImagingPanel->SetCurrentZ(position.toMM().z);

        const auto wellIdx = d->control.GetCurrentWellIndex();
        if(wellIdx != -1) {
            const auto pos = d->control.GetCurrentPositionInWell();
            d->ui.singleImagingPanel->SetCenterPosition(pos.toMM().x, pos.toMM().y);
        }
    }

    void ControlPanel::onUpdateBestFocus(double posInMm) {
        d->ui.singleImagingPanel->SetZFocus(posInMm);
        d->EnableAcquireButtons(true);
    }

    void ControlPanel::onUpdatePlan(const UseCase::ImagingTimeTable::Pointer& timeTable) {
        using TimelapseImagingTime = AppComponents::TimelapseImagingPanel::TimelapseImagingTime;

        const auto sequences = d->ui.timelapseImagingPanel->GetSequences();

        d->ui.timelapseImagingPanel->InitEstimatedList();
        QMap<int32_t, QPair<TimelapseImagingTime, TimelapseImagingTime>> plans; // pair<startTime, requiredTime>
        QList<int32_t> requiredTime;

        //TODO Update timelapse imaging panel to indicate whether the previous image acquisition is completed before the next time slice begins
        //     requiredTime - Time in seconds to get all images for each time slice

        // 현재 valid condition을 사용하지 않음.
        const bool valid = d->control.ValidateSequences(sequences, timeTable, requiredTime);
        int32_t idx = 0;

        for(auto sequence : sequences) {
            const auto startTime = sequence.GetStartTime();
            const auto interval = sequence.GetInterval().ToSeconds();
            const auto duration = sequence.GetDuration().ToSeconds();
            const auto timeSliceCount = (interval == 0) ? 1 : (duration / interval + 1);

            for(auto timeIdx=0; timeIdx<timeSliceCount; timeIdx++, idx++) {
                const auto start = TimelapseImagingTime(startTime.ToSeconds() + (interval * timeIdx));
                const auto required = TimelapseImagingTime(requiredTime.at(idx));
                QLOG_INFO() << "[" << idx + 1 << "] Start:" << start.ToString() << " Required:" << required.ToString();
                plans[idx+1] = {start, required};
            }
        }

        if(!plans.empty())
                d->ui.timelapseImagingPanel->SetEstimatedEndTimes(plans);
    }

    void ControlPanel::onClearFLChannels() {
        d->ui.singleImagingPanel->SetFLEnable(0, false);
        d->ui.singleImagingPanel->SetFLEnable(1, false);
        d->ui.singleImagingPanel->SetFLEnable(2, false);
    }

    void ControlPanel::onUpdateFOV(const double xInUm, const double yInUm) {
        d->ui.singleImagingPanel->SetFOV(xInUm, yInUm);
    }

    void ControlPanel::onUpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex, 
                                            double xInMm, double yInMm, double widthInUm, double heightInUm) {
        Q_UNUSED(wellIndex)
        d->ui.singleImagingPanel->SetTileImagingActivation(enable);
        d->ui.singleImagingPanel->SetCenterPosition(xInMm, yInMm);
        d->ui.singleImagingPanel->SetTileSize(widthInUm, heightInUm);
    }

    void ControlPanel::onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool reloaded) {
        d->control.LoadingExperiment(true);

        const auto convertZStackMode = [](const AppEntity::FLScanMode& scanMode)-> AppComponents::ImagingConditionPanel::FLZStackMode {
                if (scanMode == +AppEntity::FLScanMode::Default) {
                    return AppComponents::ImagingConditionPanel::FLZStackMode::Default;
                }
                return AppComponents::ImagingConditionPanel::FLZStackMode::FLFocus;
        };


        if (!reloaded) {
            d->ui.tabWidget->setCurrentWidget(d->ui.tabSingleImaging);

            auto sysStatus = AppEntity::SystemStatus::GetInstance();
            auto vesselIdx = sysStatus->GetCurrentVesselIndex();

            auto scenario = experiment->GetScenario(vesselIdx);

            //Set Timelapse Imaging Scenario to Timelapse table
            auto& timelapsePanel = d->ui.timelapseImagingPanel;
            timelapsePanel->SetScenario(scenario);

            auto& singlePanel = d->ui.singleImagingPanel;

            const auto imagingTypes = experiment->GetEnabledImagingTypes();
            singlePanel->SetActiveModality(d->Convert(imagingTypes));

            // Clear FL table's data before using SetFLChannel()
            singlePanel->ClearFLAcquisitionTable();
            singlePanel->ClearZStackSetting();

            //Set FL Configurations
            FLConditionParser parser(experiment);

            const auto channels = parser.GetChannels();
            for (auto channelIdx : channels) {
                FLChannelConfig flConfig;
                flConfig.SetExposure(parser.GetExposure(channelIdx) / 1000); //used to msec
                flConfig.SetIntensity(parser.GetIntensity(channelIdx));
                flConfig.SetGain(parser.GetGain(channelIdx));
                flConfig.SetEmission(parser.GetEmFilterWaveLength(channelIdx), parser.GetEmFIlterBandWidth(channelIdx));
                flConfig.SetExcitation(parser.GetExFilterWaveLength(channelIdx), parser.GetExFilterBandWidth(channelIdx));
                flConfig.SetChannelName(parser.GetName(channelIdx));

                singlePanel->blockSignals(true);
                singlePanel->SetFLChannel(channelIdx, flConfig);
                singlePanel->blockSignals(false);

                singlePanel->SetFLEnable(channelIdx, true);
            }
            
            singlePanel->SetFLZStackValues(convertZStackMode(parser.GetScanMode()), parser.GetScanRangeTop(), parser.GetScanRangeBottom(), parser.GetScanFocusFLOffset(), parser.GetScanRangeStep());

            //Update FOV
            auto fov = experiment->GetFOV();
            d->control.SetFieldOfView(fov.toUM().width, fov.toUM().height);

            // For operator, init fov mode to keep square mode at first
            auto userProfile = AppEntity::SessionManager::GetInstance()->GetProfile();
            if (userProfile == +AppEntity::Profile::ServiceEngineer) {
                using ROIMode = AppComponents::ImagingConditionPanel::ROIMode;
                singlePanel->InitFovMode(ROIMode::Free);
            }
            else {
                using ROIMode = AppComponents::ImagingConditionPanel::ROIMode;
                singlePanel->InitFovMode(ROIMode::Square);
            }

            const auto maximumFOV = AppEntity::System::GetMaximumFOV();
            d->ui.singleImagingPanel->SetMaximumFOV(maximumFOV.toUM().width, maximumFOV.toUM().height);
        }
        else { // z-stack setting undo process
            FLConditionParser parser(experiment);
            d->ui.singleImagingPanel->SetFLZStackValues(convertZStackMode(parser.GetScanMode()), 
                                                        parser.GetScanRangeTop(),
                                                        parser.GetScanRangeBottom(), 
                                                        parser.GetScanFocusFLOffset(), 
                                                        parser.GetScanRangeStep());
        }

        if(!d->IsValid(experiment)) {
            TC::MessageDialog::warning(this, 
                                       tr("FL Condition Setting"),
                                       tr("Invalid FL channel is being used.\nAll contents of FL Condition table and Timelapse table will be cleared."));
            d->ui.singleImagingPanel->ClearFLAcquisitionTable();
            d->ui.timelapseImagingPanel->Clear();
        }

        const auto overlapUM = AppEntity::System::GetSystemConfig()->GetTileScanOverlap();
        d->ui.singleImagingPanel->SetMinimumOverlapArea(overlapUM, overlapUM);

        onApplyFLImagingConditionAll();
        onApplyFLScanCondition();
        d->UpdateFLChannelInUse();
        d->InitFlChannelList();
        d->control.LoadingExperiment(false);

        onModalitiesChanged();
    }

    void ControlPanel::onApplySequences(const QList<TimelapseSequence>& sequences) {
        d->control.ApplySequences(sequences);
        d->control.UpdateRequiredSpace(d->ui.timelapseExtraInfo->GetImagingPoints());
        d->UpdateFLChannelInUse();
    }

    void ControlPanel::onUpdateLiveConfig(const uint32_t intensity, const uint32_t exposure, const double gain) {
        if (d->ui.singleImagingPanel->IsFLAcquisitionTableLocked()) {
            return;
        }

        const auto systemStatus = AppEntity::SystemStatus::GetInstance();
        const auto [modality, channel] = d->Convert(systemStatus->GetCurrentLiveConfigMode());
        if (modality == +AppEntity::Modality::FL) {
            d->ui.singleImagingPanel->ChangeFLConfigValues(channel, intensity, exposure / 1000, gain);
        }
    }

    void ControlPanel::onUpdateAcquisitionConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain) {
        auto channel = 0;
        if (mode == +AppEntity::ImagingMode::FLCH0) channel = 0;
        else if (mode == +AppEntity::ImagingMode::FLCH1) channel = 1;
        else if (mode == +AppEntity::ImagingMode::FLCH2) channel = 2;
        else return;

        d->ui.singleImagingPanel->blockSignals(true);
        d->ui.singleImagingPanel->ChangeFLConfigValues(channel, intensity, exposure / 1000, gain, true);
        d->ui.singleImagingPanel->blockSignals(false);
    }

    void ControlPanel::onExperimentStoppedByError(const QString& message) {
        const auto str = [=]()->QString {
            auto str = tr("Experiment is stopped.");
            if(message.isEmpty()) return str;
            str.append(tr("\nError: "));
            str.append(tr("%1").arg(message));
            return str;
        }();
        TC::MessageDialog::warning(this, tr("Experiment"), str);
        d->experimentProgress->hide();
    }

    void ControlPanel::onShowExperimentMessage(const QString& message) {
        if(!d->experimentProgress->isHidden()) {
            d->experimentProgress->ShowMessage(message);
        }
    }

    void ControlPanel::onUpdateExperimentProgress(const double progress, const int elapsedSeconds, const int remainSeconds) {
        if(progress >= 1.0) {
            disconnect(d->experimentObserver, SIGNAL(sigUpdateProgress(const double, const int, const int)), nullptr, nullptr);
            d->experimentProgress->hide();
        } else {
            if(d->experimentProgress->isHidden()) {
                d->experimentProgress->show();
                d->experimentProgress->raise();
                d->experimentProgress->activateWindow();
            }

            d->experimentProgress->UpdateProgress(progress, elapsedSeconds, remainSeconds);
        }
    }

    void ControlPanel::onExperimentStopped() {
        d->experimentProgress->hide();
    }

    void ControlPanel::onFovChanged(double fovXInUm, double fovYInUm) {
        if(!d->control.SetFieldOfView(fovXInUm, fovYInUm)) {
            TC::MessageDialog::warning(this, tr("ROI"), tr("Failed to change the ROI"));
        }
    }

    void ControlPanel::onModalitiesChanged() {
        auto modalities = d->ui.singleImagingPanel->GetActiveModality();

        //ModalityType::HT3D / HT2D
        d->control.SetHTEnabled(true, modalities.contains(ModalityType::HT3D));
        d->control.SetHTEnabled(false, modalities.contains(ModalityType::HT2D));

        //ModalityType::FL3D / FL2D
        d->control.SetFLEnabled(true, modalities.contains(ModalityType::FL3D));
        d->control.SetFLEnabled(false, modalities.contains(ModalityType::FL2D));

        //ModalityType::BF
        d->control.SetBFEnabled(modalities.contains(ModalityType::BF));
    }

    void ControlPanel::onChangeAcquisitionTableLock(bool locked) {
        d->control.SetAcquisitionLock(locked);

        if (locked) {
            return;
        }

        // 현재 live view config 값을 table에 적용
        const auto systemStatus = AppEntity::SystemStatus::GetInstance();
        const auto [modality, channel] = d->Convert(systemStatus->GetCurrentLiveConfigMode());
        if (modality == +AppEntity::Modality::FL) {
            auto liveConfig = systemStatus->GetLiveConfig(AppEntity::ImagingMode::_from_integral(AppEntity::ImagingMode::FLCH0 + channel));
            d->ui.singleImagingPanel->ChangeFLConfigValues(
                channel,
                liveConfig->GetLightIntensity(),
                liveConfig->GetCameraExposureUSec() / 1000,
                0
            );
        }
    }

    void ControlPanel::onApplyFLImagingCondition(int32_t row, const FLChannelConfig& config) {
        Q_UNUSED(row)
        Q_UNUSED(config)

        onApplyFLImagingConditionAll();
    }

    void ControlPanel::onApplyFLImagingConditionAll() {
        auto modalities = d->ui.singleImagingPanel->GetActiveModality();

        //ModalityType::FL3D / FL2D
        d->control.ClearFLChannels();

        QList<int32_t> failedFLChannels;

        for(auto idx=0; idx<3; idx++) {
            if(!d->ui.singleImagingPanel->GetFLEnabled(idx)) {
                d->control.SetFLChannel(idx, d->Convert(d->ui.singleImagingPanel->GetFLChannel(idx)));
            }
            else {
                const auto& flConfig = d->ui.singleImagingPanel->GetFLChannel(idx);
                QLOG_INFO() << "[" << idx << "] " << flConfig.ToStr();

                auto channelConfig = d->Convert(flConfig);
                channelConfig.SetEnable(d->ui.singleImagingPanel->GetFLEnabled(idx));
                if(!d->control.SetFLChannel(idx, channelConfig)) {
                    failedFLChannels.push_back(idx+1);
                }
            }
        }

        if(!failedFLChannels.isEmpty()) {
            QString str;
            std::for_each(failedFLChannels.begin(), failedFLChannels.end(), [&str](const int32_t& n) {
                str.append(QString(",%1").arg(n));
            });
            str = str.mid(1);
            TC::MessageDialog::warning(this, tr("FL imaging condition"), 
                                 tr("Failed to apply FL imaging parameters for channel(s): [%1]").arg(str));
        }
    }

    void ControlPanel::onChangeFLChannelEnabled(int32_t row, bool enabled) {
        auto channelConfig = d->Convert(d->ui.singleImagingPanel->GetFLChannel(row));
        channelConfig.SetEnable(enabled);

        d->control.SetFLChannel(row, channelConfig);
    }

    void ControlPanel::onApplyFLScanCondition() {
        const auto scanmode = [=]()->AppEntity::FLScanMode {
            if(d->ui.singleImagingPanel->GetZStackMode() == +AppComponents::ImagingConditionPanel::FLZStackMode::Default)
                return AppEntity::FLScanMode::Default;
            return AppEntity::FLScanMode::FLFocus;
        }();

        auto flScanConfig = ControlPanelControl::FLScanConfig();
        flScanConfig.scanMode = scanmode;
        flScanConfig.scanBottom = d->ui.singleImagingPanel->GetFLScanBottom();
        flScanConfig.scanTop = d->ui.singleImagingPanel->GetFLScanTop();
        flScanConfig.scanStep = d->ui.singleImagingPanel->GetFLScanStep();
        flScanConfig.flFocusOffset = d->ui.singleImagingPanel->GetFLFocusOffset();
        flScanConfig.scanSteps = d->ui.singleImagingPanel->GetFLScanSlices();

        QLOG_INFO() << "FL Scan Mode - " << flScanConfig.scanMode << ", "
                    << "Range (T,B)=(" << flScanConfig.scanTop << ", " << flScanConfig.scanBottom << "), "
                    << "Step=" << flScanConfig.scanStep << ", Steps=" << flScanConfig.scanSteps << ", "
                    << "FL Offset=" << flScanConfig.flFocusOffset;

        d->control.SetFLScanConfig(flScanConfig);

        flScanConfig = d->control.GetFLScanConfig();

        auto& singlePanel = d->ui.singleImagingPanel;
        singlePanel->SetFLZStackValues(singlePanel->GetZStackMode(), 
                                       flScanConfig.scanTop,
                                       flScanConfig.scanBottom,
                                       flScanConfig.flFocusOffset,
                                       flScanConfig.scanStep);
    }

    void ControlPanel::onUndoFLScanCondition() {
        d->control.ReloadExperiment();
    }

    void ControlPanel::onTileImagingConditionChanged() {
        const auto enable = d->ui.singleImagingPanel->GetTileActivation();
        const auto [centerX, centerY] = d->ui.singleImagingPanel->GetTileCenterInMM();
        const auto [tileSizeX, tileSizeY] = d->ui.singleImagingPanel->GetTileSizeInUM();

        d->control.SetTileScanCondition(enable, centerX, centerY, tileSizeX, tileSizeY);
    }

    void ControlPanel::onAddPoint() {
        const auto fovX = d->ui.singleImagingPanel->GetFovX();
        const auto fovY = d->ui.singleImagingPanel->GetFovY();
        d->control.AddCurrentPosition(AppEntity::Area::fromUM(fovX, fovY), false);
    }

    void ControlPanel::onAddTile(double widthInUm, double heightInUm) {
        auto [posXInMm, posYInMm] = d->ui.singleImagingPanel->GetTileCenterInMM();
        d->control.AddCurrentTilePosition(posXInMm, posYInMm, AppEntity::Area::fromUM(widthInUm, heightInUm), true);
    }

    void ControlPanel::onAcquireSingle() {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        if(sysStatus->GetCurrentWell() == -1) {
            TC::MessageDialog::warning(this, tr("Single Imaging"), tr("Acquisition field is outside of well"));
            return;
        }

        if(sysStatus->GetHTIlluminationCalibrated() == false) {
            TC::MessageDialog::information(this, tr("Single Imaging"), 
                                           tr("HT Illumination Calibration is required. Therefore, the calibration is performed first, and then the image is acquired."));

            if(!d->control.PerformHTIlluminationCalibration()) {
                TC::MessageDialog::warning(this, tr("Single Imaging"), 
                                           tr("It fails to calibrate HT Illumination, so it can't proceed image acqusition."));
                return;
            }
        }

        const auto modalities = d->ui.singleImagingPanel->GetActiveModality();
        const auto imageTypes = d->Convert(modalities);

        const auto fovX = d->ui.singleImagingPanel->GetFovX();
        const auto fovY = d->ui.singleImagingPanel->GetFovY();

        connect(d->experimentObserver, SIGNAL(sigUpdateProgress(const double, const int, const int)), this, 
                SLOT(onUpdateExperimentProgress(const double, const int, const int)));
        connect(d->experimentObserver, SIGNAL(sigStopped()), this, SLOT(onExperimentStopped()));

        if(!d->control.AcquireImage(imageTypes, fovX, fovY)) {
            TC::MessageDialog::warning(this, tr("Single Imaging"), tr("Acquisition failed"));
            disconnect(d->experimentObserver, SIGNAL(sigUpdateProgress(const double, const int, const int)), nullptr, nullptr);
            d->experimentProgress->hide();
        }

        d->AddMarkPosition(SinglePointType);
    }

    void ControlPanel::onAcquireSingleTile() {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        if(sysStatus->GetCurrentWell() == -1) {
            TC::MessageDialog::warning(this, tr("Single Imaging"), tr("Acquisition field is outside of well"));
            return;
        }

        if(sysStatus->GetHTIlluminationCalibrated() == false) {
            TC::MessageDialog::information(this, tr("Single Imaging"), 
                                           tr("HT Illumination Calibration is required. Therefore, the calibration is performed first, and then the image is acquired."));

            if(!d->control.PerformHTIlluminationCalibration()) {
                TC::MessageDialog::warning(this, tr("Single Imaging"), 
                                           tr("It fails to calibrate HT Illumination, so it can't proceed image acqusition."));
                return;
            }
        }

        const auto modalities = d->ui.singleImagingPanel->GetActiveModality();
        const auto imageTypes = d->Convert(modalities);

        auto [posXInMm, posYInMm] = d->ui.singleImagingPanel->GetTileCenterInMM();
        auto [roiXInUm, roiYInUm] = d->ui.singleImagingPanel->GetTileSizeInUM();

        connect(d->experimentObserver, SIGNAL(sigUpdateProgress(const double, const int, const int)), this, 
                SLOT(onUpdateExperimentProgress(const double, const int, const int)));

        if(!d->control.AcquireImage(imageTypes, posXInMm, posYInMm, roiXInUm, roiYInUm)) {
            TC::MessageDialog::warning(this, tr("Single Imaging"), tr("Acquisition failed"));
            disconnect(d->experimentObserver, SIGNAL(sigUpdateProgress(const double, const int, const int)), nullptr, nullptr);
            d->experimentProgress->hide();
        }

        d->AddMarkPosition(SingleTileType);
    }

    void ControlPanel::onMoveZAxis(double posInMm) {
        d->control.MoveZAxis(posInMm);
    }

    void ControlPanel::onPositionAdded(AppEntity::LocationIndex locationIdx, AppEntity::WellIndex wellIndex, AppEntity::Location::Pointer loc) {
        Q_UNUSED(locationIdx)
        Q_UNUSED(wellIndex)
        Q_UNUSED(loc)

        if (d->ui.tabWidget->currentIndex() == 1) {
            d->Refresh3DInterval();
            d->ui.timelapseImagingPanel->RefreshEstimatedEndTimes();
        }
    }

    void ControlPanel::onChangeImagingTabIndex(int index) {
        d->control.ChangeImagingSettingMode(d->GetImagingModeByTabIndex(index));

        if (d->ui.tabTimelapse != d->ui.tabWidget->widget(index)) return;
        using Dim = AppComponents::TimelapseImagingPanel::Dimension;
        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto sysConfig = AppEntity::System::GetSystemConfig();

        QList<int32_t> enabledChannels;
        QMap<int32_t, QColor> flChannelColors;

        for(auto idx = 0; idx < 3; idx++) {
            const auto& flConfig = d->ui.singleImagingPanel->GetFLChannel(idx);
            const auto channelName = flConfig.GetChannelName();
            if (!channelName.isEmpty()) {
                enabledChannels << idx;

                // system setup에서 설정한 FL 색을 Timelapse table에 표시
                AppEntity::FLChannel flChannel;
                if (!sysConfig->GetFLChannel(channelName, flChannel)) continue;
                flChannelColors.insert(idx, flChannel.GetColor());
            }
        }

        d->ui.timelapseImagingPanel->SetFLChannelsColor(flChannelColors);

        d->ui.timelapseImagingPanel->SetFLEnabledChannels(Dim::Dimension2, enabledChannels);
        d->ui.timelapseImagingPanel->SetFLEnabledChannels(Dim::Dimension3, enabledChannels);

        // maximum interval과 예상 종료시간 업데이트
        d->Refresh3DInterval();
        d->ui.timelapseImagingPanel->RefreshEstimatedEndTimes();
    }

    void ControlPanel::onTestAcquire() {
        if (d->ui.timelapseImagingPanel->GetSequences().isEmpty()) {
            TC::MessageDialog::warning(this, tr("Timelapse Imaging"), tr("Can't acquire images. There is no timelapse sequence."));
            return;
        }

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        if(sysStatus->GetHTIlluminationCalibrated() == false) {
            TC::MessageDialog::information(this, tr("Single Imaging"), 
                                           tr("HT Illumination Calibration is required. First capture a sample image to perform the calibration."));
            return;
        }

        emit sigRunExperimentSingle();
    }

    void ControlPanel::onAcquire() {
        if (d->ui.timelapseImagingPanel->GetSequences().isEmpty()) {
            TC::MessageDialog::warning(this, tr("Timelapse Imaging"), tr("Can't acquire images. There is no timelapse sequence."));
            return;
        }

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        if(sysStatus->GetHTIlluminationCalibrated() == false) {
            TC::MessageDialog::information(this, tr("Timelapse Imaging"), 
                                           tr("HT Illumination Calibration is required. First capture a sample image to perform the calibration."));
            return;
        }

        const auto isEnough = d->control.IsSpaceEnough(d->ui.timelapseExtraInfo->GetRequiredSpace());
        if(!isEnough) {
            TC::MessageDialog::warning(this, 
                                       tr("Timelapse Imaging"),
                                       tr("There is not enough space to acquire images on the disk."));
            return;
        }

        emit sigRunExperiment();
    }

    void ControlPanel::onStopAcquisition() {
        d->control.StopAcquisition();
        d->experimentProgress->hide();
    }

    void ControlPanel::onUpdate3DFullPlan(const UseCase::ImagingTimeTable::Pointer& timeTable) {
        using TimelapseImagingTime = AppComponents::TimelapseImagingPanel::TimelapseImagingTime;

        TimelapseImagingTime interval;
        interval = d->control.GetEstimated3DModeInterval(timeTable);
        QLOG_INFO() << "Imaging point count:" << timeTable->GetPointCount()
                    << ", Estimated 3d full mode min. interval" << interval.ToString();

        // TODO 동일한 위젯을 다른 레이아웃에서 사용
        d->ui.timelapseExtraInfo->SetImagingPoints(timeTable->GetPointCount());
        d->ui.timelapseExtraInfo->SetInterval(interval.ToString());
    }

    void ControlPanel::onDeleteAcquisitionPoint() {
        d->Refresh3DInterval();
        d->ui.timelapseImagingPanel->RefreshEstimatedEndTimes();
    }

    void ControlPanel::onUpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM) {
        auto convertAxis = [axis](bool& ok) -> AppComponents::ImagingConditionPanel::Axis {
            ok = true;
            switch(axis) {
                case AppEntity::Axis::X: 
                    return AppComponents::ImagingConditionPanel::Axis::X;
                case AppEntity::Axis::Y: 
                    return AppComponents::ImagingConditionPanel::Axis::Y;
                case AppEntity::Axis::Z: 
                    return AppComponents::ImagingConditionPanel::Axis::Z;
                default:
                    ok = false;
                    return AppComponents::ImagingConditionPanel::Axis::Invalid;
            }
        };

        auto ok = false;
        const auto convertedAxisResult = convertAxis(ok);

        if(ok) {
            d->ui.singleImagingPanel->SetSafeMovingRange(convertedAxisResult, minMM, maxMM);
        }
    }

    void ControlPanel::onUpdateTileActivation(bool enable) {
        d->ui.singleImagingPanel->SetTileImagingActivation(enable);
    }

    void ControlPanel::onUpdateAcquisitionDataRequiredSpace(const int64_t& bytes) {
        d->ui.timelapseExtraInfo->SetRequiredSpace(bytes);
        d->ui.timelapseExtraInfo->SetSpaceStatus(d->control.IsSpaceEnough(bytes));
    }

    void ControlPanel::onUpdateSystemStorageSpace(const int64_t& bytesTotal, const int64_t& bytesAvailable) {
        Q_UNUSED(bytesTotal)
        d->control.SetAvailableStorageSpace(bytesAvailable);
    }
}
