#pragma once
#include <memory>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class FocusPanelControl {
    public:
        FocusPanelControl();
        ~FocusPanelControl();

        auto PerformAF()->bool;
        auto SetBestFocus()->bool;
        auto EnableAutoFocus(bool enable)->bool;
        auto MoveZ(double distUm)->bool;
        auto StartJog(bool plusDirection)->bool;
        auto StopJog()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}