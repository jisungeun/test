#pragma once

#include <QObject>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class AcquisitionDataObserver : public QObject {
        Q_OBJECT
    public:
        AcquisitionDataObserver(QObject* parent = nullptr);
        ~AcquisitionDataObserver() override;

        auto ScannedData(const QString& user, const QString& project, const QString& experiment) -> void;
        auto AddedData(const QString& fileFullPath) -> void;
        auto UpdatedData(const QString& fileFullPath) -> void;
        auto DeletedData(const QString& fileFullPath) -> void;
        auto DeletedDataRootFolder(const QString& fileFullPath) -> void;

    signals:
        void sigDataScanned(const QString& user, const QString& project, const QString& experiment);
        void sigDataAdded(const QString& fileFullPath);
        void sigDataUpdated(const QString& fileFullPath);
        void sigDataDeleted(const QString& fileFullPath);
        void sigDataRootFolderDeleted(const QString& fileFullPath);
    };
}
