﻿#include "MarkPositionObserver.h"
#include "MarkPositionUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    MarkPositionObserver::MarkPositionObserver(QObject* parent) : QObject(parent){
        MarkPositionUpdater::GetInstance()->Register(this);
    }

    MarkPositionObserver::~MarkPositionObserver() {
        MarkPositionUpdater::GetInstance()->Deregister(this);
    }

    auto MarkPositionObserver::AddMarkPosition(AppEntity::WellIndex wellIndex, AppEntity::Location::Pointer location) -> void {
        emit sigAddMarkPosition(wellIndex, location);
    }
}
