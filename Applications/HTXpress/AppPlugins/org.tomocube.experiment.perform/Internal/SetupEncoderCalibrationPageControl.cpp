#define LOGGER_TAG "[EncoderCalibration]"
#include <TCLogger.h>
#include <Image2DProc.h>

#include <System.h>
#include <SystemStatus.h>
#include <MotionController.h>
#include <SystemConfigController.h>
#include <InstrumentController.h>

#include "Utility.h"
#include "LiveImageSource.h"
#include "MotionUpdater.h"
#include "InstrumentUpdater.h"
#include "SetupEncoderCalibrationPageControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupEncoderCalibrationPageControl::Impl {
        struct PosData {
            double xInMm{ 0 };
            double yInMm{ 0 };
        };
        QMap<Position, PosData> positions;

        struct Compensation {
            double xRatio{ 1.0 };
            double yRatio{ 1.0 };
        } original, calculated;

        auto CurrentPosition() const -> AppEntity::Position;
        auto Reload()->void;
    };

    auto SetupEncoderCalibrationPageControl::Impl::CurrentPosition() const -> AppEntity::Position {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        return Utility::global2well(sysStatus->GetCurrentWell(), sysStatus->GetCurrentGlobalPosition());
    }

    auto SetupEncoderCalibrationPageControl::Impl::Reload() -> void {
        const auto config = AppEntity::System::GetSystemConfig();
        original.xRatio = config->GetAxisCompensation(AppEntity::Axis::X);
        original.yRatio = config->GetAxisCompensation(AppEntity::Axis::Y);

        calculated = original;
    }

    SetupEncoderCalibrationPageControl::SetupEncoderCalibrationPageControl() : d{new Impl} {
        d->Reload();
    }

    SetupEncoderCalibrationPageControl::~SetupEncoderCalibrationPageControl() {
    }

    auto SetupEncoderCalibrationPageControl::StoreOriginal() -> void {
        d->Reload();

        auto sysConfig = AppEntity::System::GetSystemConfig();
        sysConfig->SetAxisCompensation(AppEntity::Axis::X, 1.0);
        sysConfig->SetAxisCompensation(AppEntity::Axis::Y, 1.0);

        auto controller = Interactor::SystemConfigController();
        controller.ApplyOnly();
    }

    auto SetupEncoderCalibrationPageControl::RestoreOriginal() -> void {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto centerPos = sysConfig->GetSystemCenter();
        
        sysConfig->SetAxisCompensation(AppEntity::Axis::X, d->original.xRatio);
        sysConfig->SetAxisCompensation(AppEntity::Axis::Y, d->original.yRatio);

        auto controller = Interactor::SystemConfigController();
        controller.ApplyOnly();
    }

    auto SetupEncoderCalibrationPageControl::Clear() -> void {
        d->positions.clear();
        d->Reload();
    }

    auto SetupEncoderCalibrationPageControl::GetCompensation() const -> std::tuple<double, double> {
        return std::make_tuple(d->calculated.xRatio, d->calculated.yRatio);
    }

    auto SetupEncoderCalibrationPageControl::GetLatestImage(QImage& image) -> bool {
        return LiveImageSource::GetInstance()->GetLatestImage(image);
    }

    auto SetupEncoderCalibrationPageControl::DisableAutofocus() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return controller.DisalbeAutoFocus();
    }

    auto SetupEncoderCalibrationPageControl::LowerZStage(double distInUm) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveRelativeUM(AppEntity::Axis::Z, -distInUm);
    }

    auto SetupEncoderCalibrationPageControl::MovePoint(Position index) -> bool {
        auto model = AppEntity::System::GetModel();
        const auto xDist = model->SetupReference(AppEntity::SetupRef::EncoderCalTargetDistanceX);
        const auto yDist = model->SetupReference(AppEntity::SetupRef::EncoderCalTargetDistanceY);

        double xInMm, yInMm;
        switch(index) {
        case Position::P1:     //Lower Left
            xInMm = -xDist/2.0;
            yInMm = -yDist/2.0;
            break;
        case Position::P2:     //Upper Left
            xInMm = -xDist/2.0;
            yInMm = yDist/2.0;
            break;
        case Position::P3:     //Lower Right
            xInMm = xDist/2.0;
            yInMm = -yDist/2.0;
            break;
        case Position::P4:     //Upper Right
            xInMm = xDist/2.0;
            yInMm = yDist/2.0;
            break;
        default:
            return false;
        }

        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveXY(0, xInMm, yInMm);
    }

    auto SetupEncoderCalibrationPageControl::CheckMove(Position index) -> bool {
        auto model = AppEntity::System::GetModel();
        const auto xDist = model->SetupReference(AppEntity::SetupRef::EncoderCalTargetDistanceX);
        const auto yDist = model->SetupReference(AppEntity::SetupRef::EncoderCalTargetDistanceY);

        if(!IsAllFound()) return false;

        const auto dx = d->positions[Position::P2].xInMm - d->positions[Position::P1].xInMm;
        const auto dy = d->positions[Position::P3].yInMm - d->positions[Position::P1].yInMm;

        double xInMm, yInMm;
        switch(index) {
        case Position::P1:
            xInMm = d->positions[Position::P1].xInMm;
            yInMm = d->positions[Position::P1].yInMm;
            break;
        case Position::P2:
            xInMm = d->positions[Position::P2].xInMm;
            yInMm = d->positions[Position::P1].yInMm + yDist;
            break;
        case Position::P3:
            xInMm = d->positions[Position::P1].xInMm + xDist;
            yInMm = d->positions[Position::P3].yInMm;
            break;
        case Position::P4:
            xInMm = d->positions[Position::P1].xInMm + xDist + dx;
            yInMm = d->positions[Position::P1].yInMm + yDist + dy;
            break;
        case Position::Center:
            xInMm = d->positions[Position::P1].xInMm + (xDist + dx)/2.0;
            yInMm = d->positions[Position::P1].yInMm + (yDist + dy)/2.0;
            break;
        default:
            return false;
        }

        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveXY(0, xInMm, yInMm);
    }

    auto SetupEncoderCalibrationPageControl::FindCenter(Position index, QImage& image) const -> std::tuple<int32_t,int32_t> {
        auto [xCenter, yCenter] = TC::Processing::Image2DProc::FindCenterOfCross(image, false, 0.7);
        if((xCenter < 0) || (yCenter < 0)) {
            d->positions.remove(index);
            return std::make_tuple(-1, -1);
        }

        const auto model = AppEntity::System::GetModel();
        const auto xCenterUm = Utility::pixel2um(xCenter - (model->CameraPixelsH() / 2.0));
        const auto yCenterUm = Utility::pixel2um(yCenter - (model->CameraPixelsV() / 2.0));

        const auto currentPos = d->CurrentPosition();
        d->positions[index] = {currentPos.toMM().x + xCenterUm/1000.0,
                               currentPos.toMM().y - yCenterUm/1000.0};

        QLOG_INFO() << "[" << index << "] Rel Center(um)=(" << xCenterUm << "," << yCenterUm << ")"
                    << " Current(mm)=(" << currentPos.toMM().x << "," << currentPos.toMM().y << ")"
                    << " Center(mm)=(" << d->positions[index].xInMm << "," << d->positions[index].yInMm <<")";

        return std::make_tuple(xCenter, yCenter);
    }

    auto SetupEncoderCalibrationPageControl::IsAllFound() const -> bool {
        if(!d->positions.contains(Position::P1)) return false;
        if(!d->positions.contains(Position::P2)) return false;
        if(!d->positions.contains(Position::P3)) return false;
        return true;
    }

    auto SetupEncoderCalibrationPageControl::Compensate() -> bool {
        if(!IsAllFound()) return false;

        auto model = AppEntity::System::GetModel();
        const auto xDist = model->SetupReference(AppEntity::SetupRef::EncoderCalTargetDistanceX);
        const auto yDist = model->SetupReference(AppEntity::SetupRef::EncoderCalTargetDistanceY);

        d->calculated.xRatio = std::abs(d->positions[Position::P3].xInMm - d->positions[Position::P1].xInMm) / xDist;
        d->calculated.yRatio = std::abs(d->positions[Position::P2].yInMm - d->positions[Position::P1].yInMm) / yDist;

        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto centerPos = sysConfig->GetSystemCenter();
        
        sysConfig->SetAxisCompensation(AppEntity::Axis::X, d->calculated.xRatio);
        sysConfig->SetAxisCompensation(AppEntity::Axis::Y, d->calculated.yRatio);

        auto controller = Interactor::SystemConfigController();
        controller.ApplyOnly();

        return true;
    }

    auto SetupEncoderCalibrationPageControl::Save() -> bool {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto centerPos = sysConfig->GetSystemCenter();
        
        sysConfig->SetAxisCompensation(AppEntity::Axis::X, d->calculated.xRatio);
        sysConfig->SetAxisCompensation(AppEntity::Axis::Y, d->calculated.yRatio);

        auto controller = Interactor::SystemConfigController();
        if(controller.Save()) {
            d->original = d->calculated;
            return true;
        }

        return false;
    }
}