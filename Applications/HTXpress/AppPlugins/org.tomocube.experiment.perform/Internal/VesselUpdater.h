#pragma once
#include <memory>

#include <IVesselView.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class VesselObserver;

    class VesselUpdater : public Interactor::IVesselView {
    public:
        using Pointer = std::shared_ptr<VesselUpdater>;

    protected:
        VesselUpdater();

    public:
        ~VesselUpdater();

        static auto GetInstance()->Pointer;

        auto UpdateCurrentWell(AppEntity::WellIndex wellIdx) -> void override;
        auto UpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM) -> void override;

    protected:
        auto Register(VesselObserver* observer)->void;
        auto Deregister(VesselObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class VesselObserver;
    };
}