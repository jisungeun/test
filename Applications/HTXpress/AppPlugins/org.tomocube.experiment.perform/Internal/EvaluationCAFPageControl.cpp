#include <QMap>
#include <QVariant>
#include <QStandardPaths>
#include <QSettings>

#include <System.h>
#include <SystemStatus.h>
#include <InstrumentController.h>

#include "Utility.h"
#include "InstrumentUpdater.h"
#include "EvaluationReportWriter.h"
#include "EvaluationCAFPageControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationCAFPageControl::Impl {
        struct {
            double NA{ 0 };
            int32_t index{ 0 };
            double score{ 0 };
            double brightness{ 0 };
            double pass{ false };
        } result;
    };

    EvaluationCAFPageControl::EvaluationCAFPageControl() : d{ std::make_unique<Impl>()} {
    }

    EvaluationCAFPageControl::~EvaluationCAFPageControl() {
    }

    auto EvaluationCAFPageControl::GetNAs() const -> QList<double> {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        return sysConfig->GetIlluminationCalibrationParameterNAs();
    }

    auto EvaluationCAFPageControl::GetReference(double NA) const -> std::tuple<double, double> {
        auto model = AppEntity::System::GetModel();
        return std::make_tuple(model->EvaluationReference(AppEntity::EvaluationRef::CAFScore, NA),
                               model->EvaluationReference(AppEntity::EvaluationRef::CAFBrightness, NA));
    }

    auto EvaluationCAFPageControl::Clear() -> void {
        d->result.NA = 0;
        d->result.index = 0;
        d->result.score = 0;
        d->result.brightness = 0;
        d->result.pass = false;
    }

    auto EvaluationCAFPageControl::RunCAF(double NA) -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto experiment = sysStatus->GetExperiment();

        auto originalNA = Utility::Service::OverrideNA(experiment, NA);

        bool success = false;
        try {
            auto instUpdater = InstrumentUpdater::GetInstance();
            auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
            auto instController = Interactor::InstrumentController(&instPresenter);

            success = instController.PerformCondenserAutoFocus();
        } catch(...) {
            success = false;
        }

        Utility::Service::OverrideNA(experiment, originalNA);

        return success;
    }

    auto EvaluationCAFPageControl::SetResult(double NA, double score, int32_t index) -> void {
        d->result.NA = NA;
        d->result.index = index;
        d->result.score = score;
    }

    auto EvaluationCAFPageControl::Evaluate() -> std::tuple<double, double, bool> {
        auto model = AppEntity::System::GetModel();
        auto refScore = model->EvaluationReference(AppEntity::EvaluationRef::CAFScore, d->result.NA);
        auto refBrightness = model->EvaluationReference(AppEntity::EvaluationRef::CAFBrightness, d->result.NA);

        auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
        auto intensityPath = QString("%1/condenserCal/MeanIntensity.txt").arg(appDataPath);
        QSettings qs(intensityPath, QSettings::IniFormat);
        d->result.brightness = qs.value(QString("MeanIntensity/%1").arg(d->result.index), 0).toDouble();

        d->result.pass = (d->result.score >= refScore) && (d->result.brightness >= refBrightness);

        return std::make_tuple(d->result.score, d->result.brightness, d->result.pass);
    }

    auto EvaluationCAFPageControl::Save(const QString& path) -> bool {
        QMap<QString, QVariant> results;
        results["NA"] = d->result.NA;
        results["score"] = d->result.score;
        results["brightness"] = d->result.brightness;
        results["index"] = d->result.index;
        results["pass"] = d->result.pass;

        auto cafPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation) + "/condenserCal";
        auto diskImgPath = QString("%1/%2.png").arg(cafPath).arg(d->result.index*2 + 1, 3, 10, QLatin1Char('0'));
        auto ringImgPath = QString("%1/%2.png").arg(cafPath).arg(d->result.index*2 + 2, 3, 10, QLatin1Char('0'));
        auto scorePath = QString("%1/CAFScore.txt").arg(cafPath);

        auto conv = [=](const QString& name)->QString {
            return QString("NA%1.%2").arg(d->result.NA, 0, 'f', 2).arg(name);
        };

        auto writer = EvaluationReportWriter::GetInstance();
        writer->SetPath(path);
        writer->SetTitle("condenser_autofocus");
        if(!writer->SaveResults(conv("report"), results)) return false;
        if(!writer->SaveFile(conv("diskimage"), diskImgPath)) return false;
        if(!writer->SaveFile(conv("ringimage"), ringImgPath)) return false;
        if(!writer->SaveFile(conv("scores"), scorePath)) return false;

        return true;
    }
}
