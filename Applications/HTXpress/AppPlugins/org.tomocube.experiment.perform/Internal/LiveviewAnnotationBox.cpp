#include <QPen>

#include "LiveviewAnnotationBox.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct LiveviewAnnotationBox::Impl {
        LiveviewAnnotationBox* p{ nullptr };
        QGraphicsRectItem* item{ nullptr };

        Impl(LiveviewAnnotationBox* p) : p{ p } {
            item = new QGraphicsRectItem();
            item->setRect(0, 0, 0, 0);
            item->setPen(QPen(Qt::yellow));
        }
    };

    LiveviewAnnotationBox::LiveviewAnnotationBox() : LiveviewAnnotationItem(), d{ std::make_unique<Impl>(this) } {
    }

    LiveviewAnnotationBox::~LiveviewAnnotationBox() {
    }

    auto LiveviewAnnotationBox::GetGraphicsItem() const -> QGraphicsItem* {
        return d->item;
    }

    auto LiveviewAnnotationBox::SetRect(double x, double y, double w, double h) -> void {
        d->item->setRect(x, y, w, h);
    }
}
