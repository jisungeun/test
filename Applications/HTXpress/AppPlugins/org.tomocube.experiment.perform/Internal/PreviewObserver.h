#pragma once
#include <memory>

#include <QObject>
#include <IPreviewView.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class PreviewObserver : public QObject {
        Q_OBJECT
    public:
        PreviewObserver(QObject* parent = nullptr);
        ~PreviewObserver() override;

        auto Progress(double progress) -> void;
        auto UpdatePosition(const AppEntity::Position& position) -> void;
        auto UpdateImageSize(int32_t xPixels, int32_t yPixels) -> void;
        auto UpdateBlock(int32_t xPos, int32_t yPos, const QImage& tileImage) -> void;

        auto SetCustomPreviewArea(double wellXinMM, double wellYinMM, int32_t widthInUm, int32_t heightInUm) -> void;
        auto SetPreviewArea(double wellXinMM, double wellYinMM, double widthMM, double heightMM) -> void;

        auto StartCustomPreviewAreaSetting() -> void;
        auto CancelCustomPreviewAreaSetting() -> void;

    signals:
        void sigUpdateProgress(double progress);
        void sigUpdatePosition(const AppEntity::Position& position);
        void sigUpdateImageSize(int32_t xPixel, int32_t yPixels);
        void sigUpdateBlock(int32_t xPos, int32_t yPos, const QImage& blockImage);
        void sigUpdateCustomPreviewArea(double wellXinMM, double wellYinMM, int32_t widthInUm, int32_t heightInUm);
        void sigSetPreviewArea(double wellXinMM, double wellYinMM, double widthMM, double heightMM);
        void sigStartCustomPreviewAreaSetting();
        void sigCancelCustomPreviewAreaSetting();
    };
}
