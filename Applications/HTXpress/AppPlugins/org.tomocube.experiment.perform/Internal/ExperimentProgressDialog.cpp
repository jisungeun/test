#include <QKeyEvent>

#include "ExperimentProgressDialog.h"
#include "ui_ExperimentProgressDialog.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct ExperimentProgressDialog::Impl {
        Ui::ExperimentProgressDialog ui;

        auto Secs2Str(int32_t seconds) const->QString;
    };

    auto ExperimentProgressDialog::Impl::Secs2Str(int32_t seconds) const -> QString {
        constexpr int32_t minInsecs = 60;
        constexpr int32_t hoursInsecs = 60 * minInsecs;
        constexpr int32_t daysInsecs = 24 * hoursInsecs;

        int32_t remain = seconds;
        const auto days = seconds / daysInsecs;
        remain -= days * daysInsecs;

        const auto hours = remain / hoursInsecs;
        remain -= hours * hoursInsecs;

        const auto mins = remain / minInsecs;
        remain -= mins * minInsecs;

        QString str;

        if(days>0) str = QString("%1d ").arg(days);
        str = str + QString("%1:%2:%3").arg(hours, 2, 10, QLatin1Char(' '))
                                       .arg(mins, 2, 10, QLatin1Char(' '))
                                       .arg(remain, 2, 10, QLatin1Char(' '));
        return str;
    }

    ExperimentProgressDialog::ExperimentProgressDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
        setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

        d->ui.setupUi(this);
        d->ui.progressBar->setValue(0);
        d->ui.progressBar->setRange(0, 100);
        d->ui.remainTimeEdit->setReadOnly(true);
        d->ui.message->setText("");

        d->ui.stopBtn->setObjectName("tc-experimentprogress-dialog-stop");

        installEventFilter(this);

        connect(d->ui.stopBtn, SIGNAL(clicked()), this, SIGNAL(sigStop()));
    }

    ExperimentProgressDialog::~ExperimentProgressDialog() {
    }

    auto ExperimentProgressDialog::UpdateProgress(double progress, int32_t elapsedSeconds,
                                                  int32_t remainSeconds) -> void {
        d->ui.progressBar->setValue(progress * 100);
        d->ui.elapsedTimeEdit->setText(d->Secs2Str(elapsedSeconds));
        d->ui.remainTimeEdit->setText(d->Secs2Str(remainSeconds));
    }

    auto ExperimentProgressDialog::ShowMessage(const QString& message) -> void {
        d->ui.message->setText(message);
    }

    bool ExperimentProgressDialog::eventFilter(QObject* watched, QEvent* event) {
        if (event->type() == QEvent::KeyPress) {
			const auto* keyEvent = dynamic_cast<QKeyEvent*>(event);

			switch (keyEvent->key()) {
			case Qt::Key_Enter:
			case Qt::Key_Return:
				return true;
			}
		}

        return QDialog::eventFilter(watched, event);
    }

    void ExperimentProgressDialog::hideEvent(QHideEvent* event) {
        d->ui.message->setText("");
        QDialog::hideEvent(event);
    }
}
