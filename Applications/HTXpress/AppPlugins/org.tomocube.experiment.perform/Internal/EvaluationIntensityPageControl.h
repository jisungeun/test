#pragma once
#include <memory>
#include <QImage>
#include <QString>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationIntensityPageControl {
    public:
        EvaluationIntensityPageControl();
        ~EvaluationIntensityPageControl();

        auto GetCVReference() const->double;
        auto GetImage()->QImage;
        auto Evaluate()->std::tuple<double, bool>;
        auto Save(const QString& path) const ->bool;
        auto Clear()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
