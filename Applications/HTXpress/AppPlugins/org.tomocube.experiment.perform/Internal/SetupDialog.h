#pragma once
#include <memory>

#include <QDialog>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SetupDialog : public QDialog {
        Q_OBJECT
    public:
        SetupDialog(QWidget* parent = nullptr);

    protected:
        void keyPressEvent(QKeyEvent* event) override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}