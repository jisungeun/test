#include "EvaluationBeadUpdater.h"
#include "EvaluationBeadObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    EvaluationBeadObserver::EvaluationBeadObserver(QObject* parent) : QObject(parent) {
        EvaluationBeadUpdater::GetInstance()->Register(this);
    }
    EvaluationBeadObserver::~EvaluationBeadObserver() {
        EvaluationBeadUpdater::GetInstance()->Deregister(this);
    }

    auto EvaluationBeadObserver::UpdateAcquisitionProgress(double progress) -> void {
        emit sigUpdateAcquisitionProgress(progress);
    }

    auto EvaluationBeadObserver::NotifyAcquisitionError(const QString& error) -> void {
        emit sigNotifyAcquisitionError(error);
    }

    auto EvaluationBeadObserver::NotifyAcquisitionStopped() -> void {
        emit sigNotifyAcquisitionStopped();
    }

    auto EvaluationBeadObserver::UpdateEvaluationProgress(double progress) -> void {
        emit sigUpdateEvaluationProgress(progress);
    }
}