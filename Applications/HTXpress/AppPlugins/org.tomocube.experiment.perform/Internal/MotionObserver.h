#pragma once
#include <memory>
#include <QObject>

#include <Position.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class MotionObserver : public QObject{
        Q_OBJECT

    public:
        MotionObserver(QObject* parent = nullptr);
        ~MotionObserver();

        auto UpdateStatus(const bool moving) -> void;
        auto UpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position) -> void;
        auto UpdateGlobalPosition(const AppEntity::Position& position) -> void;
        auto UpdateGlobalPosition(AppEntity::Axis axis, double posInMm) -> void;
        auto UpdateSelectedWell(const AppEntity::WellIndex wellIdx) -> void;
        auto UpdateBestFocus(double posInMm) -> void;

        auto ReportError(const QString& message) -> void;
        auto ReportAFFailure() -> void;

    signals:
        void sigUpdateStatus(const bool moving);
        void sigUpdatePosition(const AppEntity::WellIndex wellidx, const AppEntity::Position& position);
        void sigUpdateGlobalPosition(const AppEntity::Position& position);
        void sigUpdateGlobalAxisPosition(AppEntity::Axis axis, double posInMm);
        void sigUpdateCurrentWell(const AppEntity::WellIndex wellIdx);
        void sigUpdateBestFocus(double posInMm);
        void sigReportError(const QString& message);
        void sigAFFailed();
    };
}