#include <QMutex>

#include <LiveImageAcquisition.h>
#include "LiveImageSource.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    using LiveImagingPlugin = Plugins::LiveImageAcquisition::Plugin;

    struct LiveImageSource::Impl {
        std::shared_ptr<LiveImagingPlugin> liveImaging{ new LiveImagingPlugin() };
        QMutex mutex;
    };

    LiveImageSource::LiveImageSource() : d{new Impl} {
    }

    LiveImageSource::~LiveImageSource() {
    }

    auto LiveImageSource::GetInstance() -> Pointer {
        static Pointer theInstance{ new LiveImageSource() };
        return theInstance;
    }

    auto LiveImageSource::GetLatestImage(QImage& image) -> bool {
        QMutexLocker locker(&d->mutex);
        return d->liveImaging->GetLatest(image);
    }
}