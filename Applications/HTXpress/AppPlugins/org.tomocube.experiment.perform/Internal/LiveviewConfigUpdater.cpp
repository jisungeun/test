#include <QList>

#include "LiveviewConfigObserver.h"
#include "LiveviewConfigUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct LiveviewConfigUpdater::Impl {
        QList<LiveviewConfigObserver*> observers;
    };

    LiveviewConfigUpdater::LiveviewConfigUpdater() : d{new Impl} {
    }

    LiveviewConfigUpdater::~LiveviewConfigUpdater() {
    }

    auto LiveviewConfigUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new LiveviewConfigUpdater() };
        return theInstance;
    }

    auto LiveviewConfigUpdater::UpdateLiveConfig(const uint32_t intensity, const uint32_t exposure, const double gain) -> void {
        for (auto& observer : d->observers) {
            observer->UpdateLiveConfig(intensity, exposure, gain);
        }
    }

    auto LiveviewConfigUpdater::UpdateAcquisitionConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain) -> void {
        for (auto& observer : d->observers) {
            observer->UpdateAcquisitionConfig(mode, intensity, exposure, gain);
        }
    }

    auto LiveviewConfigUpdater::UpdateImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode) -> void {
        for(auto& observer : d->observers) {
            observer->UpdateImagingSettingMode(imagingSettingMode);
        }
    }

    auto LiveviewConfigUpdater::Register(LiveviewConfigObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto LiveviewConfigUpdater::Deregister(LiveviewConfigObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}
