#pragma once
#include <memory>

#include <QList>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SetupAutofocusPageControl {
    public:
        SetupAutofocusPageControl();
        ~SetupAutofocusPageControl();

        auto GetParameters() const->std::tuple<int32_t, int32_t, int32_t>;
        auto GetSteps() const->int32_t;
        auto Scan(double lowerRelPos, double upperRelPos) const->QList<int32_t>;
        auto CalcSlope(double sensorRange, double distRange) const->int32_t;
        auto ReadSensorValue() const->int32_t;
        auto SaveParameter(int32_t minSensor, int32_t maxSensor, int32_t divResolution, int32_t defaultSensor)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}