﻿#include <QList>

#include "SystemStorageUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SystemStorageUpdater::Impl {
        QList<SystemStorageObserver*> observers;
    };

    SystemStorageUpdater::SystemStorageUpdater() : d{std::make_unique<Impl>()} {
    }

    SystemStorageUpdater::~SystemStorageUpdater() {
    }

    auto SystemStorageUpdater::GetInstance() -> Pointer {
        static Pointer theInstance {new SystemStorageUpdater() };
        return theInstance;
    }

    auto SystemStorageUpdater::Register(SystemStorageObserver* observer) -> void {
         d->observers.push_back(observer);
    }

    auto SystemStorageUpdater::Deregister(SystemStorageObserver* observer) -> void {
        d->observers.removeOne(observer);
    }

    auto SystemStorageUpdater::UpdateSystemStorageSpace(const int64_t& totalBytes, const int64_t& availableBytes) -> void {
        for(const auto& observer : d->observers) {
            observer->UpdateSystemStorageSpace(totalBytes, availableBytes);
        }
    }

    auto SystemStorageUpdater::UpdateMinRequiredSpace(const int32_t& minRequiredGigabytes) -> void {
        for(const auto& observer : d->observers) {
            observer->UpdateMinRequiredSpace(minRequiredGigabytes);
        }
    }
}
