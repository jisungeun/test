#include <QFileDialog>
#include <QTimer>

#include <MessageDialog.h>

#include "EvaluationConfig.h"
#include "InstrumentObserver.h"
#include "EvaluationReportWriter.h"
#include "EvaluationHTIllumCalPageControl.h"
#include "EvaluationHTIllumCalPage.h"
#include "ui_EvaluationHTIllumCalPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationHTIllumCalPage::Impl {
        EvaluationHTIllumCalPageControl control;
        Ui::EvaluationHTIllumCalPage ui;
        EvaluationHTIllumCalPage* p{ nullptr };

        InstrumentObserver* observer{ nullptr };

        Impl(EvaluationHTIllumCalPage* p) : p{ p } {}

        auto InitUi()->void;
        auto Clear()->void;
        auto Evaluate()->void;
        auto Save()->void;
    };

    auto EvaluationHTIllumCalPage::Impl::InitUi() -> void {
        ui.setupUi(p);

        ui.condenserNA->clear();
        auto NAs = control.GetNAs();
        for(auto NA : NAs) {
            ui.condenserNA->addItem(QString::number(NA), NA);
        }

        ui.savedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.savedLabel->hide();

        ui.imageView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.imageView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        ui.resultLabel->hide();

        for (const auto& label : p->findChildren<QLabel*>()) {
            if (label->objectName().contains("subtitle")) {
                label->setObjectName("label-h5");
            }
            else if (label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
            else if(label->objectName().contains("ref")) {
                label->setObjectName("label-h6");
            }
        }

        for (const auto& button : p->findChildren<QPushButton*>()) {
            if (button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            }
            else {
                button->setObjectName("bt-maintenance-light");
            }
        }

        ui.leftWidget->setObjectName("panel");
        ui.rightWidget->setObjectName("panel");
        ui.botWidget->setObjectName("panel");
    }

    auto EvaluationHTIllumCalPage::Impl::Clear() -> void {
        ui.resultLabel->hide();
        ui.imageView->ClearImage();
        observer->disconnect(p);
    }

    auto EvaluationHTIllumCalPage::Impl::Evaluate() -> void {
        const auto NA = ui.condenserNA->currentData().toDouble();
        if(NA == 0) return;

        Clear();

        connect(observer, &InstrumentObserver::sigUpdateHTIlluminationResult, p, [this](const QImage& image) {
            if(image.isNull()) return;
            control.SetImage(image);
            ui.imageView->ShowImage(image);
        });

        if(!control.RunCAF(NA)) {
            TC::MessageDialog::warning(p, tr("Evaluation"), tr("Condenser autofocus is failed"));
            return;
        }

        if(!control.RunHTIllumCalibration(NA)) {
            ui.resultLabel->setText("Fail");
            ui.resultLabel->setStyleSheet("QLabel {color:red;  font: bold 14px;}");
            ui.resultLabel->show();
        } else {
            ui.resultLabel->setText("Pass");
            ui.resultLabel->setStyleSheet("QLabel {color:green; font: bold 14px;}");
            ui.resultLabel->show();
        }
    }

    auto EvaluationHTIllumCalPage::Impl::Save() -> void {
        const auto path = EvaluationConfig::GetInstance()->GetReportFolder();
        if(!control.Save(path)) {
            TC::MessageDialog::warning(p, tr("Evaluation"), tr("Failed to save the HT illumination auto calibration evaluation report"));
            return;
        }

        ui.savedLabel->show();
        QTimer::singleShot(2000, [this](){ ui.savedLabel->hide(); });
    }

    EvaluationHTIllumCalPage::EvaluationHTIllumCalPage(QWidget* parent)
        : QWidget(parent)
        , EvaluationPage()
        , d{ std::make_unique<Impl>(this) } {
        d->InitUi();

        d->observer = new InstrumentObserver(this);

        connect(d->ui.evaluateBtn, &QPushButton::clicked, this, [this]() {
            d->Evaluate();
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this]() {
            d->Save();
        });
    }

    EvaluationHTIllumCalPage::~EvaluationHTIllumCalPage() {
    }

    auto EvaluationHTIllumCalPage::Enter() -> void {
        d->Clear();
    }

    auto EvaluationHTIllumCalPage::Leave() -> void {
        d->Clear();
    }

    void EvaluationHTIllumCalPage::resizeEvent(QResizeEvent* event) {
        d->ui.imageView->FitZoom();
        QWidget::resizeEvent(event);
    }
}
