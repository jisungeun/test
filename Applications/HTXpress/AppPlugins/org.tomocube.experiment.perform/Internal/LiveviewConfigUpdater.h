#pragma once
#include <memory>

#include <ILiveviewConfigView.h>
#include <ImagingConfig.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class LiveviewConfigObserver;

    class LiveviewConfigUpdater : public Interactor::ILiveviewConfigView {
    public:
        using Pointer = std::shared_ptr<LiveviewConfigUpdater>;

    protected:
        LiveviewConfigUpdater();

    public:
        virtual ~LiveviewConfigUpdater();

        static auto GetInstance()->Pointer;

        auto UpdateLiveConfig(const uint32_t intensity, const uint32_t exposure, const double gain) -> void override;
        auto UpdateAcquisitionConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain) -> void override;
        auto UpdateImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode) -> void override;

    protected:
        auto Register(LiveviewConfigObserver* observer)->void;
        auto Deregister(LiveviewConfigObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class LiveviewConfigObserver;
    };
}