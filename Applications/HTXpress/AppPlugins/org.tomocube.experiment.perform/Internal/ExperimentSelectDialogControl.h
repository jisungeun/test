#pragma once
#include <memory>
#include <QStringList>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class ExperimentSelectDialogControl {
    public:
        ExperimentSelectDialogControl();
        ~ExperimentSelectDialogControl();

        auto GetProjects() const->QStringList;
        auto GetExperiments(const QString& project) const->QStringList;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}