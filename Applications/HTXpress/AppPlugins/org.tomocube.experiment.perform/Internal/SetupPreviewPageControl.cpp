#include <System.h>
#include <SystemConfig.h>

#include <InstrumentPresenter.h>
#include <SystemConfigController.h>
#include <PreviewCalibrationController.h>

#include "InstrumentUpdater.h"
#include "SetupPreviewPageControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupPreviewPageControl::Impl {
    };

    SetupPreviewPageControl::SetupPreviewPageControl() : d{ new Impl } {
    }

    SetupPreviewPageControl::~SetupPreviewPageControl() {
    }

    auto SetupPreviewPageControl::MeasureExposure(QList<Pair>& values, double& coeffP, double& coeffQ) -> bool {
        auto updator = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updator.get());
        auto controller = Interactor::PreviewCalibrationController(&presenter);
        return controller.MeasureExposure(values, coeffP, coeffQ);
    }

    auto SetupPreviewPageControl::MeasureGain(QList<Pair>& values, double& coeffA, double& coeffB) -> bool {
        auto updator = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updator.get());
        auto controller = Interactor::PreviewCalibrationController(&presenter);
        return controller.MeasureGain(values, coeffA, coeffB);
    }

    auto SetupPreviewPageControl::Save(double coeffA, double coeffB, double coeffP, double coeffQ) -> bool {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        sysConfig->SetPreviewGainCoefficient(coeffA, coeffB, coeffP, coeffQ);

        auto controller = Interactor::SystemConfigController();
        return controller.Save();
    }
}