#define LOGGER_TAG "[LiveViewer]"

#include <QTimer>
#include <TCLogger.h>

#include <System.h>
#include <SystemStatus.h>
#include <Image2DProc.h>

#include "InstrumentObserver.h"
#include "ImagingPlanObserver.h"
#include "LiveviewConfigObserver.h"

#include "Utility.h"
#include "LiveViewerControl.h"
#include "LiveViewer.h"
#include "LiveControlDialog.h"
#include "ui_LiveViewer.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct LiveViewer::Impl {
        Ui::LiveViewer ui;
        LiveViewerControl control;

        LiveControlDialog* liveControlDialog{ nullptr };

        InstrumentObserver* instrumentObserver{ nullptr };
        ImagingPlanObserver* imagingPlanObserver{ nullptr };
        LiveviewConfigObserver* liveviewConfigObserver{ nullptr };

        QTimer timer;

        auto Convert(const AppEntity::Modality modality, const int32_t channel = 0) -> AppEntity::ImagingMode;
        auto Compare(const AppEntity::ChannelConfig::Pointer lhs, const AppEntity::ChannelConfig::Pointer rhs)->bool;
        auto Convert(const QImage& original)->std::shared_ptr<QImage>;
    };

    auto LiveViewer::Impl::Convert(const AppEntity::Modality modality, const int32_t channel) -> AppEntity::ImagingMode {
        auto imagingMode = -1;

        if (modality == +AppEntity::Modality::HT) {
            imagingMode = AppEntity::ImagingMode::HT2D;
        } else if (modality == +AppEntity::Modality::FL) {
            imagingMode = AppEntity::ImagingMode::FLCH0 + channel;
        } else if (modality == +AppEntity::Modality::BF) {
            imagingMode = AppEntity::ImagingMode::BFGray;
        }

        return AppEntity::ImagingMode::_from_integral(imagingMode);
    }

    auto LiveViewer::Impl::Compare(const AppEntity::ChannelConfig::Pointer lhs, const AppEntity::ChannelConfig::Pointer rhs) -> bool {
        if (lhs->GetLightIntensity() != rhs->GetLightIntensity()) return false;
        if (lhs->GetCameraExposureUSec() != rhs->GetCameraExposureUSec()) return false;

        return true;
    }

    auto LiveViewer::Impl::Convert(const QImage& original) -> std::shared_ptr<QImage> {
        std::shared_ptr<QImage> srcImage{new QImage(original)};
        std::shared_ptr<QImage> outImage;
        TC::Processing::Image2DProc::ColorizeOutOfRange(srcImage, 
                                                        outImage, 
                                                        control.GetSaturationMinThreshold(), 
                                                        control.GetSaturationMaxThreshold());
        return outImage;
    }

    LiveViewer::LiveViewer(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.baseWidget->setObjectName("panel");

        const auto model = AppEntity::System::GetModel();        
        d->ui.viewPanel->SetResolution(model->CameraPixelSize() / model->ObjectiveLensMagnification());

        d->liveControlDialog = new LiveControlDialog(this);

        d->instrumentObserver = new InstrumentObserver(this);
        d->imagingPlanObserver = new ImagingPlanObserver(this);
        d->liveviewConfigObserver = new LiveviewConfigObserver(this);

        connect(d->ui.viewPanel, SIGNAL(sigDoubleClicked(int32_t, int32_t)), this, 
                SLOT(onPositionSelected(int32_t, int32_t)));
        connect(d->ui.viewMode, SIGNAL(sigSelected(AppEntity::Modality,int32_t)), this, 
                SLOT(onModeSelected(AppEntity::Modality,int32_t)));
        connect(&d->timer, SIGNAL(timeout()), this, SLOT(onUpdateImage()));

        connect(d->liveControlDialog, SIGNAL(sigLiveImageConditionChanged(const uint32_t, const uint32_t, const double)), this,
                SLOT(onChangedLiveControlValue(const uint32_t, const uint32_t, const double)));
        connect(d->liveControlDialog, SIGNAL(sigAcquisitionConditionRequested()), this,
                SLOT(onApplyChannelConditionToLiveControl()));
        connect(d->liveControlDialog, SIGNAL(sigSaveRequested()), this,
                SLOT(onSaveLiveCondition()));
        connect(d->liveControlDialog, SIGNAL(finished(int)), this, SLOT(onCloseLiveControl(int)));

        connect(d->instrumentObserver, SIGNAL(sigVesselLoaded(bool)), this, SLOT(onUpdateVesselStatus(bool)));
        connect(d->imagingPlanObserver, SIGNAL(sigUpdateFOV(const double, const double)), this, 
                SLOT(onUpdateFOV(const double, const double)));
        connect(d->imagingPlanObserver, SIGNAL(sigUpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>&)), this,
                SLOT(onUpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>&)));
        connect(d->imagingPlanObserver, SIGNAL(sigUpdateFLChannelConfig(const int32_t, const AppEntity::ChannelConfig::Pointer)), this,
                SLOT(onUpdateFLChannelConfig(const int32_t, const AppEntity::ChannelConfig::Pointer)));
        connect(d->imagingPlanObserver, SIGNAL(sigUpdateAcquisitionLock(bool)), this,
                SLOT(onUpdateAcquisitionLock(bool)));

        connect(d->liveviewConfigObserver, SIGNAL(sigLiveviewConfigChanged(const uint32_t, const uint32_t, const double)), this,
                SLOT(onUpdatedLiveCondition(const uint32_t, const uint32_t, const double)));
        connect(d->liveviewConfigObserver, SIGNAL(sigAcquisitionConfigChanged(const AppEntity::ImagingMode, const uint32_t, const uint32_t, const double)), this,
                SLOT(onUpdatedAcquisitionConfig(const AppEntity::ImagingMode, const uint32_t, const uint32_t, const double)));

        connect(d->liveviewConfigObserver, SIGNAL(sigImagingSettingModeChanged(const AppEntity::ImagingSettingMode&)),
                this, SLOT(onChangeImagingSettingMode(const AppEntity::ImagingSettingMode&)));
        d->timer.start(100);
    }

    LiveViewer::~LiveViewer() {
    }

    auto LiveViewer::HideLiveControlDialog() -> void {
        if(true == d->liveControlDialog->isVisible() && nullptr != d->liveControlDialog) {
            d->liveControlDialog->hide();
        }
    }

    auto LiveViewer::AddAnnotation(LiveviewAnnotationItem* annotation, int32_t xInPixel, int32_t yInPixel) -> void {
        d->ui.viewPanel->AddAnnotation(annotation, xInPixel, yInPixel);
    }

    auto LiveViewer::RemoveAnnotation(LiveviewAnnotationItem* annotation) -> void {
        d->ui.viewPanel->RemoveAnnotation(annotation);
    }

    void LiveViewer::onUpdateImage() {
        QImage image;
        if(!d->control.GetLatestImage(image)) return;

        if(false == d->liveControlDialog->GetSaturation()) {
            d->ui.viewPanel->SetImage(image);
        } else {
            d->ui.viewPanel->SetImage(*d->Convert(image));
        }
    }

    void LiveViewer::onPositionSelected(int32_t posX, int32_t posY) {
        QLOG_INFO() << "X=" << posX << " Y=" << posY;
        if(!d->control.MoveSampleStage(posX, posY)) {
            //TODO Show Error Mesage??
        }
    }

    void LiveViewer::onModeSelected(AppEntity::Modality modality, int32_t channel) {
        if(d->liveControlDialog->GetImagingSettingMode() == +AppEntity::ImagingSettingMode::TimelapseImaging) {
            return;
        }

        auto popUpPos = QCursor::pos();
        auto rect = this->geometry();
        rect.moveTopLeft(parentWidget()->mapToGlobal(rect.topLeft()));
        popUpPos.setX(rect.right());
        popUpPos.setY(rect.top());

        if (!d->liveControlDialog->isHidden() && d->liveControlDialog->GetMode() == d->Convert(modality, channel)) {
            d->liveControlDialog->move(popUpPos);
            return;
        }

        bool showControlDialog = false;

        if (modality == +AppEntity::Modality::FL) {
            d->liveControlDialog->SetMode(d->Convert(modality, channel));
            const auto model = AppEntity::System::GetModel();
            d->liveControlDialog->SetExposureRange(
                model->MinimumExposureUSec() / 1000,
                model->MaximumExposureUSec() / 1000
            );
            d->liveControlDialog->ShowBoostMode(true);
            showControlDialog = true;
        } else if (modality == +AppEntity::Modality::BF) {
            const auto model = AppEntity::System::GetModel();
            const auto minExposureMSec = model->MinimumExposureUSec() / 1000;
            const auto maxExposureMSec = minExposureMSec < 50 ? 50 : model->MaximumExposureUSec() / 1000;
            d->liveControlDialog->SetMode(d->Convert(modality, channel));
            d->liveControlDialog->SetExposureRange(minExposureMSec, 
                                                   maxExposureMSec);
            d->liveControlDialog->ShowBoostMode(false);
            showControlDialog = true;
        } else {
			return;
		}

        if (showControlDialog) {
            if (d->liveControlDialog->isHidden()) {
                d->liveControlDialog->show();
            }

            d->liveControlDialog->move(popUpPos);
        }
        else {
            d->liveControlDialog->hide();
        }

        // 변경된 mode의 live config를 entity에 적용
        if (!d->control.ChangeLiveImagingMode(d->Convert(modality, channel))) {
            return;
        }

        // 변경된 mode로 live imaging을 시작
        if (!d->control.SetViewMode(modality, channel)) {
            //TODO Show Error Message??
        }
    }

    void LiveViewer::onUpdateVesselStatus(bool loaded) {
        setEnabled(loaded);
        if(!loaded) {
            HideLiveControlDialog();
        }
    }

    void LiveViewer::onUpdateFOV(const double xInUm, const double yInUm) {
        const auto fovXPixels = Utility::um2pixel(xInUm);
        const auto fovYPixels = Utility::um2pixel(yInUm);
        d->ui.viewPanel->SetROI(fovXPixels, fovYPixels);
    }

    void LiveViewer::onUpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types) {
        // Live view mode의 FL 버튼이 비활성화 될 때 FL live control 창이 열려있다면 닫음 
        if ( !(types.contains(AppEntity::ImagingType::FL2D) || types.contains(AppEntity::ImagingType::FL3D)) ) {
            const auto currentMode = d->liveControlDialog->GetMode();
            if (currentMode == +AppEntity::ImagingMode::FLCH0 ||
                currentMode == +AppEntity::ImagingMode::FLCH1 ||
                currentMode == +AppEntity::ImagingMode::FLCH2) {
                d->liveControlDialog->hide();
            }
        }
    }

    void LiveViewer::onUpdateFLChannelConfig(const int32_t channel, const AppEntity::ChannelConfig::Pointer config) {
        auto currentLiveControlMode = d->liveControlDialog->GetMode();

        if (config->GetEnable()) {
            // FL의 Acquisition config와 Live config가 다른 경우 save 버튼 활성화
            if (currentLiveControlMode == +AppEntity::ImagingMode::FLCH0 ||
                currentLiveControlMode == +AppEntity::ImagingMode::FLCH1 ||
                currentLiveControlMode == +AppEntity::ImagingMode::FLCH2) {
                auto systemStatus = AppEntity::SystemStatus::GetInstance();

                auto channelConfig = systemStatus->GetChannelConfig(currentLiveControlMode);
                auto liveConfig = systemStatus->GetLiveConfig(currentLiveControlMode);

                d->liveControlDialog->SetEnabledSaveButton(!d->Compare(channelConfig, liveConfig));
            }

        } else {
            // Live view mode의 FL channel 버튼이 비활성화 될 때 해당 FL channel의 live control 창이 열려있다면 닫음

            auto disabledMode = AppEntity::ImagingMode::FLCH0;
            if (channel == 1) {
                disabledMode = AppEntity::ImagingMode::FLCH1;
            } else if (channel == 2) {
                disabledMode = AppEntity::ImagingMode::FLCH2;
            }

            if (currentLiveControlMode == +disabledMode) {
                d->liveControlDialog->hide();
            }
        }
    }

    void LiveViewer::onChangedLiveControlValue(const uint32_t intensity, const uint32_t exposure, const double gain) {
        auto mode = d->liveControlDialog->GetMode();
        if (!d->control.ChangeLiveImagingCondition(mode, intensity, exposure * 1000/*convert to usec*/, gain)) {
            // TODO: error message
            return;
        }
    }

    void LiveViewer::onApplyChannelConditionToLiveControl() {
        auto mode = d->liveControlDialog->GetMode();
        if (!d->control.ApplyChannelConditionToLiveCondition(mode)) {
            // TODO: error message
        }
    }

    void LiveViewer::onSaveLiveCondition() {
        auto currentMode = d->liveControlDialog->GetMode();

        if (currentMode == +AppEntity::ImagingMode::FLCH0 ||
            currentMode == +AppEntity::ImagingMode::FLCH1 ||
            currentMode == +AppEntity::ImagingMode::FLCH2) {
            if (!d->control.ApplyToAcquisitionConfig(currentMode)) {
                // TODO: error message
            }
        }
    }

    void LiveViewer::onCloseLiveControl(int result) {
        Q_UNUSED(result)

        d->ui.viewMode->Deselect();
    }

    void LiveViewer::onUpdatedLiveCondition(const uint32_t intensity, const uint32_t exposure, const double gain) {
        d->liveControlDialog->SetIntensity(intensity);
        d->liveControlDialog->SetExposure(exposure / 1000); // convert to msec
        d->liveControlDialog->SetGain(gain);

        // live config와 acquisition config 비교하여 save 버튼 상태 설정
        auto systemStatus = AppEntity::SystemStatus::GetInstance();
        auto channelConfig = systemStatus->GetChannelConfig(d->liveControlDialog->GetMode());
        auto liveConfig = systemStatus->GetLiveConfig(d->liveControlDialog->GetMode());

        d->liveControlDialog->SetEnabledSaveButton(!d->Compare(channelConfig, liveConfig));
    }
    
    void LiveViewer::onUpdatedAcquisitionConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain) {
        Q_UNUSED(mode)
        Q_UNUSED(intensity)
        Q_UNUSED(exposure)
        Q_UNUSED(gain)

        // Live control의 save 버튼 동작으로 acquisition config가 업데이트 된 것이므로
        // save 버튼 상태만 변경
        d->liveControlDialog->SetEnabledSaveButton(false);
    }

    void LiveViewer::onUpdateAcquisitionLock(bool locked) {
        if (locked) {
            auto systemStatus = AppEntity::SystemStatus::GetInstance();
            auto channelConfig = systemStatus->GetChannelConfig(d->liveControlDialog->GetMode());
            auto liveConfig = systemStatus->GetLiveConfig(d->liveControlDialog->GetMode());

            d->liveControlDialog->SetEnabledSaveButton(!d->Compare(channelConfig, liveConfig));
        } else {
            d->liveControlDialog->SetEnabledSaveButton(false);
        }
    }

    void LiveViewer::onChangeImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode) {
        if(d->liveControlDialog) {
            d->liveControlDialog->SetImagingSettingMode(imagingSettingMode);
        }
    }
}
