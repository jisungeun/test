#pragma once
#include <memory>

#include <IPreviewView.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class PreviewObserver;

    class PreviewUpdater : public Interactor::IPreviewView {
    public:
        using Pointer = std::shared_ptr<PreviewUpdater>;

    protected:
        PreviewUpdater();

    public:
        ~PreviewUpdater();

        static auto GetInstance()->Pointer;

        auto Progress(double progress, AppEntity::Position& position) -> void override;
        auto UpdateImageSize(int32_t xPixels, int32_t yPixels) -> void override;
        auto UpdateBlock(int32_t xPos, int32_t yPos, const QImage& blockImage) -> void override;
        auto UpdateCustomPreviewArea(double wellXinMM, double wellYinMM, int32_t widthUm, int32_t heightUm) -> void override;
        auto StartCustomPreviewAreaSetting() -> void override;
        auto CancelCustomPreviewAreaSetting() -> void override;
        auto SetPreviewArea(double wellXinMM, double wellYinMM, double widthMM, double heightMM) -> void override;

    protected:
        auto Register(PreviewObserver* observer)->void;
        auto Deregister(PreviewObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class PreviewObserver;
    };
}