#pragma once
#include <memory>
#include <QWidget>
#include <QMap>

#include <Location.h>
#include <Experiment.h>
#include <IPreviewView.h>
#include <PreviewPanel.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class PreviewViewer : public QWidget {
        Q_OBJECT

    public:
        PreviewViewer(QWidget* parent = nullptr);
        ~PreviewViewer() override;

    protected slots:
        void onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool reloaded);

        void onCapturePreview(const HTXpress::AppComponents::PreviewPanel::PreviewPanel::PreviewScanType& type);
        void onRequestSetPreviewArea();
        void onCancelSetPreviewArea();

        void onMoveLive(int32_t posX, int32_t posY);
        void onTileImagingAreaChanged(double xInPixel, double yInPixel, double widthInPixel, double heightInPixel);

        void onUpdateProgress(double progress);
        void onVesselChanged(AppEntity::WellIndex wellIdx);
        void onUpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position);
        void onUpdateVesselStatus(bool loaded);

        //Acquisition positions
        void onAddPosition(AppEntity::LocationIndex locationIdx, AppEntity::WellIndex wellIdx, AppEntity::Location::Pointer location);
        void onRefreshList(AppEntity::WellIndex wellIdx, QMap<AppEntity::LocationIndex, AppEntity::Location::Pointer>& locationList);
        void onDeletePosition(AppEntity::WellIndex wellIdx, AppEntity::LocationIndex locationIdx);

        //Imaging Plan
        void onUpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex, double xInMm, double yInMm, double widthInUm, double heightInUm);
        void onUpdateFOV(const double xInUm, const double yInUm);

        void onUpdateCustomPreviewArea(double wellXinMM, double wellYinMM, int32_t widthInUm, int32_t heightInUm);
        void onUpdateSingleAcquireProgress(const double progress, const int elapsedSeconds, const int remainSeconds);
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}