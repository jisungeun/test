#pragma once
#include <memory>

#include <IQualityCheckView.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationBeadObserver;

    class EvaluationBeadUpdater : public Interactor::IQualityCheckView {
    public:
        using Pointer = std::shared_ptr<EvaluationBeadUpdater>;

    protected:
        EvaluationBeadUpdater();

    public:
        ~EvaluationBeadUpdater() override;

        static auto GetInstance()->Pointer;

        auto UpdateAcquisitionProgress(double progress) -> void override;
        auto NotifyAcquisitionError(const QString& error) -> void override;
        auto NotifyAcquisitionStopped() -> void override;
        auto UpdateEvaluationProgress(double progress) -> void override;

    protected:
        auto Register(EvaluationBeadObserver* observer)->void;
        auto Deregister(EvaluationBeadObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class EvaluationBeadObserver;
    };
}