#include "AcquisitionPositionUpdater.h"
#include "AcquisitionPositionObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    AcquisitionPositionObserver::AcquisitionPositionObserver(QObject* parent) : QObject(parent) {
        AcquisitionPositionUpdater::GetInstance()->Register(this);
    }

    AcquisitionPositionObserver::~AcquisitionPositionObserver() {
        AcquisitionPositionUpdater::GetInstance()->Deregister(this);
    }

    auto AcquisitionPositionObserver::AddPosition(LocationIndex locationIdx, WellIndex wellIndex, Location::Pointer loc) -> void {
        emit sigAddPosition(locationIdx, wellIndex, loc);
    }

    auto AcquisitionPositionObserver::RefreshList(WellIndex wellIndex, QMap<LocationIndex, Location::Pointer>& locList) -> void {
        emit sigRefreshList(wellIndex, locList);
    }

    auto AcquisitionPositionObserver::DeletePosition(WellIndex wellIdx, LocationIndex locationIdx) -> void {
        emit sigDeletePosition(wellIdx, locationIdx);
    }
}
