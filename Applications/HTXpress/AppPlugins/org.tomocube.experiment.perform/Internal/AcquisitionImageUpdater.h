#pragma once
#include <memory>

#include <IAcquireImageView.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class AcquisitionImageObserver;

    class AcquisitionImageUpdater : public Interactor::IAcquireImageView {
    public:
        using Pointer = std::shared_ptr<AcquisitionImageUpdater>;

    protected:
        AcquisitionImageUpdater();

    public:
        ~AcquisitionImageUpdater() override;
        static auto GetInstance()->Pointer;

        auto UpdateProgress(double progress) -> void override;

    protected:
        auto Register(AcquisitionImageObserver* observer)->void;
        auto Deregister(AcquisitionImageObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class AcquisitionImageObserver;
    };
}