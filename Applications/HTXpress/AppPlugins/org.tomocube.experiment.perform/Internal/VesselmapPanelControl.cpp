#define LOGGER_TAG "[VesselMapPanel]"
#include <TCLogger.h>

#include <AcqPositionSetupController.h>
#include <VesselController.h>
#include <MotionController.h>
#include <ImagingPlanController.h>
#include <PreviewAcquisitionController.h>
#include <LensMovingRangeController.h>

#include "Utility.h"
#include "AcquisitionPositionUpdater.h"
#include "MotionUpdater.h"
#include "VesselmapPanelControl.h"
#include "ImagingPlanUpdater.h"
#include "SystemStatus.h"
#include "PreviewUpdater.h"
#include "VesselUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct VesselmapPanelControl::Impl {
        AppEntity::WellIndex currentWellIndex{ -1 };
    };

    VesselmapPanelControl::VesselmapPanelControl() : d{new Impl} {
    }

    VesselmapPanelControl::~VesselmapPanelControl() {
    }

    bool VesselmapPanelControl::CopyAcquisitionPositions(const AppEntity::WellIndex& sourceWellIndex, const QList<AppEntity::LocationIndex>& sourceLocations, const QList<AppEntity::WellIndex>& targetWells) {
        auto updater = AcquisitionPositionUpdater::GetInstance();
        auto presenter = Interactor::AcqPositionPresenter(updater.get());
        auto controller = Interactor::AcqPositionSetupController(&presenter);
        return controller.CopyPositions(sourceWellIndex, sourceLocations, targetWells);
    }

    auto VesselmapPanelControl::AddMatrixPositions(const AppEntity::WellIndex& sourceWellIndex, 
                                                   const AppEntity::LocationIndex& sourceLocationIndex, 
                                                   const int32_t& cols, 
                                                   const int32_t& rows, 
                                                   const double& horSpacingMM, 
                                                   const double& verSpacingMM, 
                                                   const QList<AppEntity::WellIndex>& targetWells) -> bool {
        auto updater = AcquisitionPositionUpdater::GetInstance();
        auto presenter = Interactor::AcqPositionPresenter(updater.get());
        auto controller = Interactor::AcqPositionSetupController(&presenter);
        return controller.AddMatrixPositions(sourceWellIndex, sourceLocationIndex, cols, rows, horSpacingMM, verSpacingMM, targetWells);
    }

    auto VesselmapPanelControl::DeleteAcquisitionPosition(AppEntity::WellIndex wellIdx, AppEntity::LocationIndex locationIdx) -> bool {
        auto updater = AcquisitionPositionUpdater::GetInstance();
        auto presenter = Interactor::AcqPositionPresenter(updater.get());
        auto controller = Interactor::AcqPositionSetupController(&presenter);
        return controller.DeletePosition(wellIdx, locationIdx);
    }

    auto VesselmapPanelControl::GetVesselPosition(const AppEntity::WellIndex& wellIdx,
                                                 const AppEntity::Position& position, 
                                                 double& posX, double& posY, double& posZ) -> void {
        auto vesselPos = Utility::well2vessel(wellIdx, position);
        posX = vesselPos.toMM().x;
        posY = vesselPos.toMM().y;
        posZ = vesselPos.toMM().z;
    }

    auto VesselmapPanelControl::GetVesselPosition(const AppEntity::Position& globalPosition, 
                                                  double& posX, double& posY, double& posZ) -> void {
        auto vesselPos = Utility::global2vessel(globalPosition);
        posX = vesselPos.toMM().x;
        posY = vesselPos.toMM().y;
        posZ = vesselPos.toMM().z;
    }

    auto VesselmapPanelControl::GetWellPosition(const AppEntity::Position& globalPosition, 
                                                double& posX, double& posY, double& posZ) -> void {
        auto wellPos = Utility::global2well(d->currentWellIndex, globalPosition);
        posX = wellPos.toMM().x;
        posY = wellPos.toMM().y;
        posZ = wellPos.toMM().z;
    }

    auto VesselmapPanelControl::ChangeCurrentWell(AppEntity::WellIndex wellIndex) -> bool {
        if(d->currentWellIndex == wellIndex) return true;

        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::VesselController(&presenter);

        d->currentWellIndex = wellIndex;
        if(!controller.SetCurrentWell(wellIndex)) return false;

        return true;
    }

    auto VesselmapPanelControl::GetCurrentWell() const -> AppEntity::WellIndex {
        return d->currentWellIndex;
    }

    auto VesselmapPanelControl::ChangeSampleStagePosition(AppEntity::WellIndex wellIndex, double xMM, double yMM) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::VesselController(&presenter);

        return controller.SetSampleStagePosition(wellIndex, xMM, yMM);
    }

    auto VesselmapPanelControl::ChangeSampleStagePosition(AppEntity::WellIndex wellIndex, double xMM, double yMM, double zMM) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::VesselController(&presenter);

        const auto autoFocus = AppEntity::SystemStatus::GetInstance()->GetAutoFocusEnabled();

        if (controller.SetSampleStagePosition(wellIndex, xMM, yMM)) {
            if (!autoFocus) {
                return controller.SetSampleStagePosition(wellIndex, zMM);
            }
            return true;
        }
        return false;
    }

    auto VesselmapPanelControl::MoveAxis(AppEntity::Axis axis, double posMM) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        const auto wellIndex = AppEntity::SystemStatus::GetInstance()->GetCurrentWell();

        return controller.MoveMM(wellIndex, axis, posMM);
    }

    auto VesselmapPanelControl::RefreshCurrentPosition(AppEntity::WellIndex& wellIndex, AppEntity::Position& globalPosition) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);

        if(!controller.GetGlobalPosition(globalPosition, wellIndex)) {
            d->currentWellIndex = -1;
            return false;
        }

        d->currentWellIndex = wellIndex;

        return true;
    }

    auto VesselmapPanelControl::ChangeTileImagingArea(double xMM, double yMM, double zMM, double widthMM, double heightMM) -> bool {
        Q_UNUSED(zMM)
        const auto wellIndex = AppEntity::SystemStatus::GetInstance()->GetCurrentWell();

        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingPlanController(false, &presenter);

        const auto widthInUM = widthMM*1000;
        const auto heightInUM = heightMM*1000;

        return controller.SetTileScanArea(true, wellIndex, xMM, yMM, widthInUM, heightInUM);
    }

    auto VesselmapPanelControl::ChangeCustomPreviewArea(const double& xMM, const double& yMM, const double& widthMM, const double& heightMM) -> bool {
        auto updater = PreviewUpdater::GetInstance();
        auto presenter = Interactor::PreviewPresenter(updater.get());
        auto controller = Interactor::PreviewAcquisitionController(&presenter);

        const auto widthUm = static_cast<int32_t>(widthMM*1000);
        const auto heightUm = static_cast<int32_t>(heightMM*1000);

        return controller.SetCustomPreviewArea(xMM, yMM, widthUm, heightUm);
    }

    auto VesselmapPanelControl::SetSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM) -> void {
        auto updater = VesselUpdater::GetInstance();
        auto presenter = Interactor::VesselPresenter(updater.get());
        auto controller = Interactor::LensMovingRangeController(&presenter);

        controller.SetSafeMovingRange(axis, minMM, maxMM);
    }
}
