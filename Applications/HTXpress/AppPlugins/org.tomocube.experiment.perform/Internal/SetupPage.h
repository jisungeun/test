#pragma once

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SetupPage {
    public:
        SetupPage();
        virtual ~SetupPage();

        virtual auto Enter()->void = 0;
        virtual auto Leave()->void = 0;
    };
}