#include <QVBoxLayout>
#include <QButtonGroup>
#include <QPushButton>
#include <QMap>

#include <ImagingConfig.h>

#include "ImagingPlanObserver.h"
#include "ViewModePanelControl.h"
#include "ViewModePanel.h"

#include <QLabel>

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct ViewModePanel::Impl {
        ViewModePanelControl control;
        QButtonGroup* buttons{ nullptr };

        ImagingPlanObserver* imagingPlanObserver{ nullptr };

        QMap<int32_t, bool> flEnabledState = { {0, false}, {1, false}, {2, false} };    // FL channel 별 활성화 상태
    };

    ViewModePanel::ViewModePanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->imagingPlanObserver = new ImagingPlanObserver(this);

        d->buttons = new QButtonGroup(this);
        d->buttons->setExclusive(true);

        const auto counts = d->control.GetCount();

        auto* vboxLayout = new QVBoxLayout(this);
        {
            for(int idx=0; idx<counts; idx++) {
                auto* btn = new QPushButton(d->control.GetTitle(idx), this);
                btn->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed) ;
                btn->setFixedSize(46, 34);
                btn->setCheckable(true);
                if (d->control.GetModality(idx) == +AppEntity::Modality::FL) btn->setEnabled(false);
                d->buttons->addButton(btn, idx);
                vboxLayout->addWidget(btn);
            }
            vboxLayout->addStretch();
        }
        vboxLayout->insertSpacing(1, 5);
        vboxLayout->insertWidget(2, new QLabel("FL"), 0, Qt::AlignCenter);
        vboxLayout->setContentsMargins(10, 20, 10, 20);
        vboxLayout->setSpacing(10);
        setLayout(vboxLayout);

        connect(d->imagingPlanObserver, SIGNAL(sigUpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>&)),
            this, SLOT(onUpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>&)));
        connect(d->imagingPlanObserver, SIGNAL(sigUpdateFLChannelConfig(const int32_t, const AppEntity::ChannelConfig::Pointer)),
            this, SLOT(onUpdateFLChannelConfig(const int32_t, const AppEntity::ChannelConfig::Pointer)));

        connect(d->buttons, SIGNAL(idClicked(int)), this, SLOT(onSelected(int)));
    }

    ViewModePanel::~ViewModePanel() {
    }

    auto ViewModePanel::SetCurrent(AppEntity::Modality modality, int32_t channel) -> void {
        const auto index = d->control.GetIndex(modality, channel);
        if(index < 0) return;

        auto* btn = d->buttons->button(index);
        if(btn) btn->animateClick(50);
    }

    auto ViewModePanel::Deselect() const -> void {
        d->buttons->setExclusive(false);

        for (auto button : d->buttons->buttons()) {
            button->setChecked(false);
        }

        d->buttons->setExclusive(true);
    }


    void ViewModePanel::onSelected(int index) {
        emit sigSelected(d->control.GetModality(index), d->control.GetChannel(index));
    }

    void ViewModePanel::onUpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types) {
        bool flEnabled = types.contains(AppEntity::ImagingType::FL2D) || types.contains(AppEntity::ImagingType::FL3D);

        auto it = QMapIterator(d->flEnabledState);
        while (it.hasNext()) {
            it.next();

            auto channel = it.key();
            auto flButton = d->buttons->button(d->control.GetIndex(AppEntity::Modality::FL, channel));
            if (flButton == nullptr) continue;

            if (flEnabled) {
                flButton->setEnabled(it.value());
            } else {
                flButton->setEnabled(false);
            }
        }
    }

    void ViewModePanel::onUpdateFLChannelConfig(const int32_t channel, const AppEntity::ChannelConfig::Pointer config) {
        d->flEnabledState[channel] = config->GetEnable();

        auto flButton = d->buttons->button(d->control.GetIndex(AppEntity::Modality::FL, channel));
        if (flButton) flButton->setEnabled(d->flEnabledState[channel]);
    }
}
