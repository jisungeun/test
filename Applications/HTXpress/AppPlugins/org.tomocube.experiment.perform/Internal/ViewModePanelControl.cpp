#include <QList>

#include "Utility.h"
#include "ViewModePanelControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct ViewModePanelControl::Impl {
        struct Mode {
            AppEntity::Modality modality;
            QString title;
            int32_t channel{ 0 };
        };

        //Todo 시스템 설정/모델 정보에서 mode를 구성하도록 하기
        QList<Mode> modes{
            {AppEntity::Modality::BF, "BF", 0},
            {AppEntity::Modality::FL, "CH1", 0},
            {AppEntity::Modality::FL, "CH2", 1},
            {AppEntity::Modality::FL, "CH3", 2},
        };
    };

    ViewModePanelControl::ViewModePanelControl() : d{new Impl} {
    }

    ViewModePanelControl::~ViewModePanelControl() {
    }

    auto ViewModePanelControl::GetCount() const -> int32_t {
        return d->modes.size();
    }

    auto ViewModePanelControl::GetTitle(int32_t index) const -> QString {
        if(index >= d->modes.size()) return "";
        return d->modes.at(index).title;
    }

    auto ViewModePanelControl::GetModality(int32_t index) const -> AppEntity::Modality {
        if(index >= d->modes.size()) return AppEntity::Modality::HT;
        return d->modes.at(index).modality;
    }

    auto ViewModePanelControl::GetChannel(int32_t index) const -> int32_t {
        if(index >= d->modes.size()) return 0;
        return d->modes.at(index).channel;
    }

    auto ViewModePanelControl::GetIndex(AppEntity::Modality modality, int32_t channel) -> int32_t {
        int32_t index = -1;

        for(const auto& mode : d->modes) {
            index = index + 1;
            if(mode.modality != modality) continue;
            if(mode.channel != channel) continue;
            return index;
        }

        return -1;
    }
}
