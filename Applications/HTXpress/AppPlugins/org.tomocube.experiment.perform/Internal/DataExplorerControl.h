#pragma once
#include <memory>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class DataExplorerControl {
    public:
        DataExplorerControl();
        ~DataExplorerControl();

        auto InstallAcquisitionDataMonitor() -> bool;
        auto ScanAcquisitionData() -> bool;

        auto SetVesselLoadStatus(bool loaded) -> void;
        auto IsVesselLoaded() const -> bool;

        auto LoadVessel() -> bool;
        auto UnloadVessel() -> bool;

        auto InstallStorageMonitor() -> bool;
        auto GetStorageInformation() -> bool;
        auto UpdateRootDrive() -> bool;
        auto UpdateMinRequiredSpace() -> bool;

        auto ParseUserName(const QString& fileFullPath) const -> QString;
        auto ParseProjectName(const QString& fileFullPath) const -> QString;
        auto ParseExperimentName(const QString& fileFullPath) const -> QString;
        auto ParseSpecimenName(const QString& fileFullPath) const -> QString;
        auto ParseWellName(const QString& fileFullPath) const -> QString;
        auto ParseDataName(const QString& fileFullPath) const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
