#pragma once
#include <memory>

#include <Position.h>
#include <Location.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class VesselmapPanelControl {
    public:
        VesselmapPanelControl();
        ~VesselmapPanelControl();

        auto CopyAcquisitionPositions(const AppEntity::WellIndex& sourceWellIndex, 
                              const QList<AppEntity::LocationIndex>& sourceLocations,
                              const QList<AppEntity::WellIndex>& targetWells)->bool;
        auto AddMatrixPositions(const AppEntity::WellIndex& sourceWellIndex,
                                const AppEntity::LocationIndex& sourceLocationIndex,
                                const int32_t& cols,
                                const int32_t& rows,
                                const double& horSpacingMM, 
                                const double& verSpacingMM,
                                const QList<AppEntity::WellIndex>& targetWells)->bool;
        auto DeleteAcquisitionPosition(AppEntity::WellIndex wellIdx, AppEntity::LocationIndex locationIdx)->bool;
        auto GetVesselPosition(const AppEntity::WellIndex& wellIdx, const AppEntity::Position& position, 
                               double& posX, double& posY, double& posZ)->void;
        auto GetVesselPosition(const AppEntity::Position& globalPosition, double& posX, double& posY, double& posZ)->void;
        auto GetWellPosition(const AppEntity::Position& globalPosition, double& posX, double& posY, double& posZ)->void;

        auto ChangeCurrentWell(AppEntity::WellIndex wellIndex)->bool;
        auto GetCurrentWell() const->AppEntity::WellIndex;
        auto ChangeSampleStagePosition(AppEntity::WellIndex wellIndex, double xMM, double yMM)->bool;
        auto ChangeSampleStagePosition(AppEntity::WellIndex wellIndex, double xMM, double yMM, double zMM)->bool;
        auto MoveAxis(AppEntity::Axis axis, double posMM)->bool;

        auto RefreshCurrentPosition(AppEntity::WellIndex& wellIndex, AppEntity::Position& globalPosition)->bool;

        auto ChangeTileImagingArea(double xMM, double yMM, double zMM, double widthMM, double heightMM)->bool;
        auto ChangeCustomPreviewArea(const double& xMM, const double& yMM, const double& widthMM, const double& heightMM)->bool;

        auto SetSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}