#pragma once
#include <memory>
#include <QObject>

#include <ImagingTimeTable.h>
#include <ChannelConfig.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class ImagingPlanObserver : public QObject{
        Q_OBJECT
    public:
        ImagingPlanObserver(QObject* parent = nullptr);
        ~ImagingPlanObserver();

        auto Update(const UseCase::ImagingTimeTable::Pointer& table)->void;
        auto Update3DMinimumInterval(const UseCase::ImagingTimeTable::Pointer& table)->void;
        auto UpdateFOV(const double xInUm, const double yInUm)->void;
        auto UpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex, double xInMm, double yInMm, double widthInUm, double heightInUm)->void;
        auto ClearFLChannels()->void;
        auto UpdateBFEnabled(bool enabled)->void;
        auto UpdateHTEnabled(bool enabled)->void;
        auto UpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types)->void;
        auto UpdateFLChannelConfig(const int32_t channel, const AppEntity::ChannelConfig::Pointer config)->void;
        auto UpdateAcquisitionLock(bool locked)->void;
        auto UpdateAcquisitionDataRequiredSpace(const int64_t& bytes)->void;
        auto UpdateTileActivation(bool enable) -> void;

    signals:
        void sigUpdate(const UseCase::ImagingTimeTable::Pointer& table);
        void sigUpdate3DMinimumInterval(const UseCase::ImagingTimeTable::Pointer& table);
        void sigUpdateFOV(const double xInUm, const double yInUm);
        void sigUpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex, double xInMm, double yInMm, double widthInUm, double heightInUm);
        void sigClearFLChannels();
        void sigUpdateBFEnabled(bool enabled);
        void sigUpdateHTEnabled(bool enabled);
        auto sigUpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types)->void;
        void sigUpdateFLChannelConfig(const int32_t channel, const AppEntity::ChannelConfig::Pointer config);
        void sigUpdateAcquisitionLock(bool locked);
        void sigUpdateAcquisitionDataRequiredSpace(const int64_t& bytes);
        void sigUpdateTileActivation(bool enable);
    };
}
