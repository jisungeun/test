#include <QStringList>
#include <QFileInfo>
#include <QMap>

#include <System.h>
#include <SystemStatus.h>
#include <TCFDataRepo.h>
#include <QualityCheckController.h>
#include <DataMisc.h>

#include "EvaluationReportWriter.h"
#include "EvaluationBeadUpdater.h"
#include "EvaluationBeadRepeatabilityPageControl.h"

using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;
using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationBeadRepeatabilityPageControl::Impl {
        QStringList dataPathList;
        QString outputPath;

        struct {
            double cv{ 0 };
            bool pass{ false };
            QList<Entity::BeadScore> scores;
        } result;

        auto MakeDecision()->void;
    };

    auto EvaluationBeadRepeatabilityPageControl::Impl::MakeDecision() -> void {
        const auto count = result.scores.size();
        if(count == 0) return;

        double sumValue = 0;
        for(auto idx=0; idx<count; ++idx) {
            sumValue += result.scores[idx].GetCorrelation();
        }

        auto meanValue = sumValue / count;
        if(meanValue == 0) return;

        double stdValue = 0;
        for(auto idx=0; idx<count; ++idx) {
            stdValue += std::pow(result.scores[idx].GetCorrelation() - meanValue, 2);
        }

        result.cv = std::sqrt(stdValue / count / meanValue);

        auto model = AppEntity::System::GetModel();
        auto cvRef = model->EvaluationReference(AppEntity::EvaluationRef::BeadRepeatabilityCV);

        result.pass = (result.cv < cvRef);
    }

    EvaluationBeadRepeatabilityPageControl::EvaluationBeadRepeatabilityPageControl() : d{ std::make_unique<Impl>() } {
    }

    EvaluationBeadRepeatabilityPageControl::~EvaluationBeadRepeatabilityPageControl() {
    }

    auto EvaluationBeadRepeatabilityPageControl::GetCVReference() const -> double {
        auto model = AppEntity::System::GetModel();
        return model->EvaluationReference(AppEntity::EvaluationRef::BeadRepeatabilityCV);
    }

    auto EvaluationBeadRepeatabilityPageControl::GetRepeatCount() const -> int32_t {
        auto model = AppEntity::System::GetModel();
        return model->EvaluationReference(AppEntity::EvaluationRef::BeadRepeatabilityCount);
    }

    auto EvaluationBeadRepeatabilityPageControl::AcquireData(int32_t repeatCount) -> bool {
        auto updater = EvaluationBeadUpdater::GetInstance();
        auto presenter = Interactor::QualityCheckPresenter::GetInstance(updater.get());
        auto controller = Interactor::QualityCheckController(presenter.get());
        return controller.AcquireTheCurrent(repeatCount);
    }

    auto EvaluationBeadRepeatabilityPageControl::AddDataPath(const QString& dataPath) -> bool {
        const auto systemStatus = AppEntity::SystemStatus::GetInstance();
        const auto dataRepo = TCFDataRepo::GetInstance();
        const auto user = DataMisc::GetUserName(dataPath);
        const auto project = DataMisc::GetProjectName(dataPath);
        const auto experiment = DataMisc::GetExperimentName(dataPath);
        const auto specimen = DataMisc::GetSpecimenName(dataPath);
        const auto well  = DataMisc::GetWellName(dataPath);
        const auto dataName = DataMisc::GetTcfBaseName(dataPath);
        const auto acqData = dataRepo->GetData(user, project, experiment, specimen, well, dataName);

        if (acqData == nullptr) return false;
        d->dataPathList.append(QFileInfo(dataPath).absolutePath());

        return true;
    }

    auto EvaluationBeadRepeatabilityPageControl::GetDataCount() const -> int32_t {
        return d->dataPathList.size();
    }

    auto EvaluationBeadRepeatabilityPageControl::StartEvaluation() -> void {
        auto updater = EvaluationBeadUpdater::GetInstance();
        auto presenter = Interactor::QualityCheckPresenter::GetInstance(updater.get());
        auto controller = Interactor::QualityCheckController(presenter.get());

        const auto fov = AppEntity::SystemStatus::GetInstance()->GetExperiment()->GetFOV().toUM();
        const auto resolution = AppEntity::System::GetCameraResolutionInUM();
        const auto xInPixel = static_cast<int32_t>((fov.width / 2.0) / resolution);
        const auto yInPixel = static_cast<int32_t>((fov.height / 2.0) / resolution);

        QList<Interactor::QualityCheckController::Data> dataList;
        for(auto path : d->dataPathList) {
            dataList.push_back({path, xInPixel, yInPixel});
        }

        d->result.scores = controller.EvaluateBeadScore(dataList);
        d->outputPath = controller.GetOutputPath();
        d->MakeDecision();
    }

    auto EvaluationBeadRepeatabilityPageControl::EvaluationResult() -> std::tuple<double, bool> {
        return std::make_tuple(d->result.cv, d->result.pass);
    }

    auto EvaluationBeadRepeatabilityPageControl::GetScore(int32_t index) const -> Entity::BeadScore {
        if(index >= d->result.scores.size()) return Entity::BeadScore();
        return d->result.scores.at(index);
    }

    auto EvaluationBeadRepeatabilityPageControl::Save(const QString& path) const -> bool {
        QMap<QString, QVariant> results;
        results["NA"] = AppEntity::SystemStatus::GetInstance()->GetNA();
        for(auto idx=0; idx<d->result.scores.size(); ++idx) {
            auto& score = d->result.scores.at(idx);
            results[QString("volume_%1").arg(idx+1)] = score.GetVolume();
            results[QString("drymass_%1").arg(idx+1)] = score.GetDrymass();
            results[QString("mean_dRI_%1").arg(idx+1)] = score.GetMeanDeltaRI();
            results[QString("correlation_%1").arg(idx+1)] = score.GetCorrelation();
        }
        results["cv"] = d->result.cv;
        results["pass"] = d->result.pass;

        auto writer = EvaluationReportWriter::GetInstance();
        writer->SetPath(path);
        writer->SetTitle("repeatability");
        if(!writer->SaveResults("report", results)) return false;
        if(!writer->CopyDirectory("raw_data", d->outputPath)) return false;

        return true;
    }

    auto EvaluationBeadRepeatabilityPageControl::Clear() -> void {
        d->dataPathList.clear();
        d->outputPath.clear();
        d->result.cv = 0;
        d->result.pass = false;
        d->result.scores.clear();
    }
}
