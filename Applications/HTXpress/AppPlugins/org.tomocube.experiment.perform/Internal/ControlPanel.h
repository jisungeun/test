#pragma once
#include <memory>
#include <QWidget>

#include <Position.h>
#include <ImagingTimeTable.h>
#include <Experiment.h>
#include <ImagingConditionPanel.h>
#include <TimelapseImagingPanel.h>
#include <ImagingConfig.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class ControlPanel : public QWidget {
        Q_OBJECT

    public:
        using Self = ControlPanel;
        using TimelapseSequence = AppComponents::TimelapseImagingPanel::TimelapseSequence;
        
        ControlPanel(QWidget* parent = nullptr);
        ~ControlPanel() override;

    protected slots:
        void onUpdateVesselStatus(bool loaded);
        void onUpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position);
        void onUpdateGlobalPosition(const AppEntity::Position& position);
        void onUpdateBestFocus(double posInMm);
        void onUpdatePlan(const UseCase::ImagingTimeTable::Pointer& timeTable);
        void onUpdate3DFullPlan(const UseCase::ImagingTimeTable::Pointer& timeTable);
        void onClearFLChannels();
        void onUpdateFOV(const double xInUm, const double yInUm);
        void onUpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex, 
                                  double xInMm, double yInMm, 
                                  double widthInUm, double heightInUm);
        void onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool reloaded);
        void onApplySequences(const QList<TimelapseSequence>& sequences);

        void onUpdateLiveConfig(const uint32_t intensity, const uint32_t exposure, const double gain);
        void onUpdateAcquisitionConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain);
        void onExperimentStoppedByError(const QString& message);
        void onShowExperimentMessage(const QString& message);
        void onUpdateExperimentProgress(const double progress, const int elapsedSeconds, const int remainSeconds);
        void onExperimentStopped();

        void onFovChanged(double fovXInUm, double fovYInUm);
        void onModalitiesChanged();
        void onChangeAcquisitionTableLock(bool locked);
        void onApplyFLImagingCondition(int32_t row, 
                                       const HTXpress::AppComponents::ImagingConditionPanel::FLChannelConfig& config);
        void onApplyFLImagingConditionAll();
        void onChangeFLChannelEnabled(int32_t row, bool enabled);
        void onApplyFLScanCondition();
        void onUndoFLScanCondition();

        void onTileImagingConditionChanged();

        void onAddPoint();
        void onAddTile(double widthInUm, double heightInUm);
        void onAcquireSingle();
        void onAcquireSingleTile();
        void onMoveZAxis(double posInMm);

        void onPositionAdded(AppEntity::LocationIndex locationIdx, AppEntity::WellIndex wellIndex, AppEntity::Location::Pointer loc);

        void onChangeImagingTabIndex(int index);
        void onTestAcquire();
        void onAcquire();
        void onStopAcquisition();

		void onDeleteAcquisitionPoint();

        void onUpdateSafeMovingRange(AppEntity::Axis axis, double minMM, double maxMM);
        void onUpdateTileActivation(bool enable);

        void onUpdateAcquisitionDataRequiredSpace(const int64_t& bytes);
        void onUpdateSystemStorageSpace(const int64_t& bytesTotal, const int64_t& bytesAvailable);

signals:
        void sigRunExperimentSingle();
        void sigRunExperiment();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
