#include <QKeyEvent>

#include "InstrumentObserver.h"
#include "SetupSystemCenterPage.h"
#include "SetupEncoderCalibrationPage.h"
#include "SetupIntensityPage.h"
#include "SetupCAFPage.h"
#include "SetupBackgroundCalPage.h"
#include "SetupAutofocusPage.h"
#include "SetupPreviewPage.h"
#include "SetupDialog.h"
#include "ui_SetupDialog.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupDialog::Impl {
        Ui::SetupDialog ui;
        int32_t lastTab = -1;

        InstrumentObserver* instrumentObserver{ nullptr };

        auto Enable(bool enable)->void;
    };

    auto SetupDialog::Impl::Enable(bool enable) -> void {
        ui.tabWidget->setEnabled(enable);
    }

    SetupDialog::SetupDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
        d->ui.setupUi(this);

        setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint);

        d->Enable(false);

        d->instrumentObserver = new InstrumentObserver(this);

        d->ui.tabWidget->addTab(new SetupSystemCenterPage(this), "System Center");
        d->ui.tabWidget->addTab(new SetupEncoderCalibrationPage(this), "Encoder");
        d->ui.tabWidget->addTab(new SetupIntensityPage(this), "BF/HT Intensity");
        d->ui.tabWidget->addTab(new SetupAutofocusPage(this), "Autofocus");
        d->ui.tabWidget->addTab(new SetupCAFPage(this), "Condenser AF");
        d->ui.tabWidget->addTab(new SetupBackgroundCalPage(this), "Background");
        d->ui.tabWidget->addTab(new SetupPreviewPage(this), "Preview");

        connect(d->ui.tabWidget, &QTabWidget::currentChanged, this, [this](int32_t index) {
            auto* prevPage = dynamic_cast<SetupPage*>(d->ui.tabWidget->widget(d->lastTab));
            if(prevPage) {
                prevPage->Leave();
            }

            auto* page = dynamic_cast<SetupPage*>(d->ui.tabWidget->currentWidget());
            if(page) {
                page->Enter();
                d->lastTab = index;
            }
        });

        connect(d->instrumentObserver, &InstrumentObserver::sigVesselLoaded, this, [this](bool loaded) {
            d->Enable(loaded);
        });

        d->ui.baseWidget->setObjectName("panel");
    }

    void SetupDialog::keyPressEvent(QKeyEvent* event) {
        switch(event->key()) {
        case Qt::Key_Enter:
        case Qt::Key_Return:
            return;
        case Qt::Key_Escape:
            return;
        default:
            QDialog::keyPressEvent(event);
        }
    }
}
