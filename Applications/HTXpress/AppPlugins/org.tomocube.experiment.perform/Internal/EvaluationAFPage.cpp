#include <QFileDialog>
#include <QTimer>

#include <MessageDialog.h>

#include "EvaluationConfig.h"
#include "EvaluationReportWriter.h"
#include "EvaluationAFPageControl.h"
#include "EvaluationAFPage.h"

#include <QPushButton>

#include "ui_EvaluationAFPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationAFPage::Impl {
        EvaluationAFPageControl control;
        Ui::EvaluationAFPage ui;
        EvaluationAFPage* p{ nullptr };

        Impl(EvaluationAFPage* p) : p{ p } {}

        auto InitUi()->void;
        auto Clear()->void;
        auto Evaluate()->void;
        auto LoadImage(int32_t index)->void;
        auto Save()->void;
    };

    auto EvaluationAFPage::Impl::InitUi() -> void {
        ui.setupUi(p);

        ui.savedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.savedLabel->hide();

        ui.saveBtn->setDisabled(true);

        ui.imageView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.imageView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        auto ref = control.GetReference();
        ui.refLabel->setText(tr("(Ref: %1um)").arg(ref, 0, 'f', 1));

        ui.resultLabel->hide();

        for (const auto& label : p->findChildren<QLabel*>()) {
            if (label->objectName().contains("subtitle")) {
                label->setObjectName("label-h5");
            }
            else if (label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
            else if(label->objectName().contains("ref")) {
                label->setObjectName("label-h6");
            }
        }

        for (const auto& button : p->findChildren<QPushButton*>()) {
            if (button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            }
            else {
                button->setObjectName("bt-maintenance-light");
            }
        }

        ui.leftWidget->setObjectName("panel");
        ui.rightWidget->setObjectName("panel");
        ui.botWidget->setObjectName("panel");
    }

    auto EvaluationAFPage::Impl::Clear() -> void {
        ui.diffValue->setValue(0);
        ui.resultLabel->hide();
        ui.imageView->ClearImage();

        control.Clear();
    }

    auto EvaluationAFPage::Impl::Evaluate() -> void {
        Clear();

        auto [distance, result] = control.Evaluate();

        ui.diffValue->setValue(distance);

        if(result) {
            ui.resultLabel->setText("Pass");
            ui.resultLabel->setStyleSheet("QLabel {color:green; font: bold 14px;}");
            ui.resultLabel->show();
        } else {
            ui.resultLabel->setText("Fail");
            ui.resultLabel->setStyleSheet("QLabel {color:red;  font: bold 14px;}");
            ui.resultLabel->show();
        }

        ui.image3Btn->setChecked(true);
        ui.saveBtn->setEnabled(true);

        LoadImage(2);
    }

    auto EvaluationAFPage::Impl::LoadImage(int32_t index) -> void {
        ui.imageView->ShowImage(control.GetImage(index));
    }

    auto EvaluationAFPage::Impl::Save() -> void {
        const auto path = EvaluationConfig::GetInstance()->GetReportFolder();
        if(!control.Save(path)) {
            TC::MessageDialog::warning(p, tr("Evaluation"), tr("Failed to save the autofocus evaluation report"));
            return;
        }

        ui.savedLabel->show();
        QTimer::singleShot(2000, [this](){ ui.savedLabel->hide(); });
    }

    EvaluationAFPage::EvaluationAFPage(QWidget* parent) : QWidget(parent), EvaluationPage(), d{ std::make_unique<Impl>(this) } {
        d->InitUi();

        connect(d->ui.evaluateBtn, &QPushButton::clicked, this, [this]() {
            d->Evaluate();
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this]() {
            d->Save();
        });

        connect(d->ui.image1Btn, &QPushButton::clicked, this, [this]() {
            d->LoadImage(0);
        });

        connect(d->ui.image2Btn, &QPushButton::clicked, this, [this]() {
            d->LoadImage(1);
        });

        connect(d->ui.image3Btn, &QPushButton::clicked, this, [this]() {
            d->LoadImage(2);
        });
    }

    EvaluationAFPage::~EvaluationAFPage() {
    }

    auto EvaluationAFPage::Enter() -> void {
        d->Clear();
    }

    auto EvaluationAFPage::Leave() -> void {
        d->Clear();
    }

    void EvaluationAFPage::resizeEvent(QResizeEvent* event) {
        d->ui.imageView->FitZoom();
        QWidget::resizeEvent(event);
    }
}
