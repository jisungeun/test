#pragma once
#include <memory>
#include <QList>

#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class FLConditionParser {
    public:
        FLConditionParser(const AppEntity::Experiment::Pointer& experiment);
        ~FLConditionParser();

        auto GetChannels()->QList<int32_t>;

        auto GetExposure(int32_t channel) const->int32_t;
        auto GetInterval(int32_t channel) const->int32_t;
        auto GetIntensity(int32_t channel) const->int32_t;
        auto GetGain(int32_t channel) const->double;
        auto GetExFilterChannel(int32_t channel) const->int32_t;
        auto GetExFilterWaveLength(int32_t channel) const->int32_t;
        auto GetExFilterBandWidth(int32_t channel) const->int32_t;
        auto GetEmFilterChannel(int32_t channel) const->int32_t;
        auto GetEmFilterWaveLength(int32_t channel) const->int32_t;
        auto GetEmFIlterBandWidth(int32_t channel) const->int32_t;
        auto GetName(int32_t channel) const->QString;

        auto GetScanMode() const->AppEntity::FLScanMode;
        auto GetScanRangeBottom() const->double;
        auto GetScanRangeTop() const->double;
        auto GetScanRangeStep() const->double;
        auto GetScanRangeSteps() const->int32_t;
        auto GetScanFocusHT() const->double;
        auto GetScanFocusFLOffset() const->double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}