#include "AcquisitionImageUpdater.h"
#include "AcquisitionImageObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    AcquisitionImageObserver::AcquisitionImageObserver(QObject* parent) : QObject(parent) {
        AcquisitionImageUpdater::GetInstance()->Register(this);
    }

    AcquisitionImageObserver::~AcquisitionImageObserver() {
        AcquisitionImageUpdater::GetInstance()->Deregister(this);
    }

    auto AcquisitionImageObserver::UpdateProgress(double progress) -> void {
        emit sigUpdateProgress(progress);
    }
}