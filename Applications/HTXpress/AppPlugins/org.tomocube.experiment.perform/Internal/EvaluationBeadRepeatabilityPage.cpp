#include <QFileDialog>
#include <QTimer>

#include <MessageDialog.h>

#include "LiveviewAnnotationBox.h"
#include "EvaluationConfig.h"
#include "AcquisitionDataObserver.h"
#include "EvaluationBeadObserver.h"
#include "EvaluationReportWriter.h"
#include "EvaluationBeadRepeatabilityPageControl.h"
#include "EvaluationBeadRepeatabilityPage.h"
#include "ui_EvaluationBeadRepeatabilityPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationBeadRepeatabilityPage::Impl {
        EvaluationBeadRepeatabilityPageControl control;
        Ui::EvaluationBeadRepeatabilityPage ui;
        EvaluationBeadRepeatabilityPage* p{ nullptr };
        LiveviewAnnotationBox* annotation{ nullptr };

        EvaluationBeadObserver* evalObserver{ new EvaluationBeadObserver(p) };
        AcquisitionDataObserver* dataObserver{ new AcquisitionDataObserver(p) };

        Impl(EvaluationBeadRepeatabilityPage* p) : p{ p } {}
        auto InitUi()->void;
        auto Clear()->void;
        auto Acquire()->void;
        auto AcquireCompleted()->void;
        auto AcquisitionFailed(const QString& error)->void;
        auto Evaluate()->void;
        auto EvaluateCompleted()->void;
        auto Save()->void;
    };

    auto EvaluationBeadRepeatabilityPage::Impl::InitUi() -> void {
        ui.setupUi(p);

        ui.acquireProgressBar->hide();
        ui.evaluateBtn->setDisabled(true);
        ui.evalProgressBar->hide();
        ui.saveBtn->setDisabled(true);
        ui.savedLabel->hide();
        ui.cvValue->setValue(0);
        ui.resultLabel->hide();

        ui.repeatCount->setValue(control.GetRepeatCount());
        ui.repeatCount->setReadOnly(true);

        ui.resultTable->setColumnCount(4);
        ui.resultTable->setHorizontalHeaderLabels({"Volume", "Dry Mass", "Mean dRI", "Correlation"});
        ui.resultTable->horizontalHeader()->setStretchLastSection(true);

        ui.refLabel->setText(QString("(Ref : %1)").arg(control.GetCVReference(), 0, 'f', 3));

        for (const auto& label : p->findChildren<QLabel*>()) {
            if (label->objectName().contains("subtitle")) {
                label->setObjectName("label-h5");
            }
            else if (label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
            else if(label->objectName().contains("ref")) {
                label->setObjectName("label-h6");
            }
        }

        for (const auto& button : p->findChildren<QPushButton*>()) {
            if (button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            }
            else {
                button->setObjectName("bt-maintenance-light");
            }
        }

        for (const auto& table : p->findChildren<QTableWidget*>()) {
            table->setStyleSheet(QString("QTableWidget{border-bottom: 0px;}"));
            table->verticalHeader()->setStyleSheet("QHeaderView{border-bottom: 0px;}");
            table->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);
        }

        ui.leftWidget->setObjectName("panel");
        ui.rightWidget->setObjectName("panel");
        ui.botWidget->setObjectName("panel");
    }

    auto EvaluationBeadRepeatabilityPage::Impl::Clear() -> void {
        control.Clear();

        ui.acquireProgressBar->hide();
        ui.evaluateBtn->setDisabled(true);
        ui.evalProgressBar->hide();
        ui.saveBtn->setDisabled(true);
        ui.savedLabel->hide();
        ui.cvValue->setValue(0);
        ui.resultLabel->hide();
        ui.resultTable->clearContents();

        ui.acquireProgressBar->setValue(0);
        ui.evalProgressBar->setValue(0);

        evalObserver->disconnect(p);
        dataObserver->disconnect(p);
    }

    auto EvaluationBeadRepeatabilityPage::Impl::Acquire() -> void {
        Clear();
        ui.acquireProgressBar->show();

        connect(evalObserver, &EvaluationBeadObserver::sigUpdateAcquisitionProgress, p, [this](double progress) {
            ui.acquireProgressBar->setValue(progress*100);
            if(progress == 1.0) AcquireCompleted();
        });

        connect(evalObserver, &EvaluationBeadObserver::sigNotifyAcquisitionError, p, [this](const QString& error) {
            AcquisitionFailed(error);
        });

        connect(evalObserver, &EvaluationBeadObserver::sigUpdateEvaluationProgress, p, [this](double progress) {
            ui.evalProgressBar->setValue(progress*100);
        });

        connect(dataObserver, &AcquisitionDataObserver::sigDataAdded, p, 
                [this](const QString& fileFullPath) {
            if(!control.AddDataPath(fileFullPath)) {
                TC::MessageDialog::warning(p, tr("Evaluation"), tr("Evaluation data may not be stored correctly"));
            }
        });

        if(!control.AcquireData(ui.repeatCount->value())) {
            Clear();
        }
    }

    auto EvaluationBeadRepeatabilityPage::Impl::AcquireCompleted() -> void {
        ui.acquireProgressBar->hide();
        ui.evaluateBtn->setEnabled(true);
    }

    auto EvaluationBeadRepeatabilityPage::Impl::AcquisitionFailed(const QString& error) -> void {
        Clear();
        TC::MessageDialog::warning(p, tr("Evaluation"), error);
    }

    auto EvaluationBeadRepeatabilityPage::Impl::Evaluate() -> void {
        ui.evalProgressBar->show();
        control.StartEvaluation();
        EvaluateCompleted();
    }

    auto EvaluationBeadRepeatabilityPage::Impl::EvaluateCompleted() -> void {
        ui.evalProgressBar->hide();

        auto [cvValue, result] = control.EvaluationResult();
        ui.cvValue->setValue(cvValue);

        if(result) {
            ui.resultLabel->setText("Pass");
            ui.resultLabel->setStyleSheet("QLabel {color:green; font: bold 14px;}");
            ui.resultLabel->show();
        } else {
            ui.resultLabel->setText("Fail");
            ui.resultLabel->setStyleSheet("QLabel {color:red;  font: bold 14px;}");
            ui.resultLabel->show();
        }

        const auto rowCount = control.GetDataCount();
        ui.resultTable->setRowCount(rowCount);
        for(auto rowIdx=0; rowIdx<rowCount; rowIdx++) {
            auto score = control.GetScore(rowIdx);

            auto item = new QTableWidgetItem(QString::number(score.GetVolume(), 'f', 2));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 0, item);

            item = new QTableWidgetItem(QString::number(score.GetDrymass(), 'f', 3));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 1, item);

            item = new QTableWidgetItem(QString::number(score.GetMeanDeltaRI(), 'f', 3));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 2, item);

            item = new QTableWidgetItem(QString::number(score.GetCorrelation(), 'f', 3));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 3, item);
        }

        ui.saveBtn->setEnabled(true);
    }

    auto EvaluationBeadRepeatabilityPage::Impl::Save() -> void {
        const auto path = EvaluationConfig::GetInstance()->GetReportFolder();
        if(!control.Save(path)) {
            TC::MessageDialog::warning(p, tr("Evaluation"), tr("Failed to save the repeatability evaluation report"));
            return;
        }

        ui.savedLabel->show();
        QTimer::singleShot(2000, [this](){ ui.savedLabel->hide(); });
    }

    EvaluationBeadRepeatabilityPage::EvaluationBeadRepeatabilityPage(QWidget* parent)
        : QWidget(parent)
        , EvaluationPage()
        , d{ std::make_unique<Impl>(this) } {
        d->InitUi();

        auto cropSize = EvaluationConfig::GetInstance()->GetBeadCropSizeInPixels();
        d->annotation = new LiveviewAnnotationBox();
        d->annotation->SetRect(-cropSize/2, -cropSize/2, cropSize, cropSize);

        connect(d->ui.acquireBtn, &QPushButton::clicked, this, [this]() {
            d->Acquire();
        });

        connect(d->ui.evaluateBtn, &QPushButton::clicked, this, [this]() {
            d->Evaluate();
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this]() {
            d->Save();
        });
    }

    EvaluationBeadRepeatabilityPage::~EvaluationBeadRepeatabilityPage() {
    }

    auto EvaluationBeadRepeatabilityPage::Enter() -> void {
        d->Clear();
        d->annotation->Install(0, 0);
    }

    auto EvaluationBeadRepeatabilityPage::Leave() -> void {
        d->Clear();
        d->annotation->Unintall();
    }

    void EvaluationBeadRepeatabilityPage::resizeEvent(QResizeEvent* event) {
        QWidget::resizeEvent(event);
    }
}