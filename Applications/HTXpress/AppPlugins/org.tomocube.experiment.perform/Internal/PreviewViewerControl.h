#pragma once
#include <memory>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class PreviewViewerControl {
    public:
        PreviewViewerControl();
        ~PreviewViewerControl();

        auto GetDefaultROI() const->std::tuple<int32_t,int32_t>;
        auto SetCustomPreviewArea(const double& wellXinMM, const double& wellYinMM, const int32_t& widthInUM, const int32_t& heightInUM)->void;
        auto GetCustomPreviewSizeInUM() const->std::tuple<int32_t,int32_t>;

        auto CapturePreview(int32_t width, int32_t height)->bool;
        auto StopCapture()->void;

        auto MoveRelativeLive(int32_t relPosX, int32_t relPosY)->bool;
        auto ChangeTileImagingArea(int32_t xInPixel, int32_t yInPixel, int32_t widthInPixel, int32_t heightInPixel)->bool;

        auto DoPreviewAreaSetting()->void;
        auto CancelPreviewAreaSetting()->void;

        auto CurrentWell() const->AppEntity::WellIndex;
        auto CurrentCenterPosition() const->AppEntity::Position;

        auto GetOffsetInPixels(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position, int32_t& posX, int32_t& posY)->bool;

        auto SetFOVInPixels(const int32_t xInPixels, const int32_t yInPixels)->void;
        auto GetFOVInPixels() const->std::tuple<int32_t, int32_t>;

        auto SetPositionInPixels(const int32_t posX, const int32_t posY)->void;
        auto GetPositionInPixels() const->std::tuple<int32_t, int32_t>;

        auto MoveLensPosition() -> void;

        auto UpdateImageSize(int32_t xPixels, int32_t yPixels)->void;
        auto GetDefaultPath() const->QString;
        auto GetDefaultName(const QString& dirPath) const->QString;
        auto StorePreview(const QString& path, const QPixmap& pixmap)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}