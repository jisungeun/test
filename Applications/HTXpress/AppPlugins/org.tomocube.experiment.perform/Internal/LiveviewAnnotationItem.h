#pragma once
#include <memory>

#include <LiveviewAnnotation.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class LiveviewAnnotationItem : public AppComponents::LiveviewPanel::Annotation {
    public:
        LiveviewAnnotationItem();
        ~LiveviewAnnotationItem() override;

        auto Install(int32_t xInPixel, int32_t yInPixel)->void;
        auto Unintall()->void;
    };
}