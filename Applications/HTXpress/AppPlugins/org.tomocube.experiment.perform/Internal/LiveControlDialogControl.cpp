#include <System.h>

#include "LiveControlDialogControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct LiveControlDialogControl::Impl {
    };

    LiveControlDialogControl::LiveControlDialogControl() : d{new Impl} {
    }

    LiveControlDialogControl::~LiveControlDialogControl() {
    }

    auto LiveControlDialogControl::MinimumExposureMSec() const -> int32_t {
        return AppEntity::System::GetModel()->MinimumExposureUSec() / 1000;
    }

    auto LiveControlDialogControl::MaximumExposureMSec() const -> int32_t {
        return AppEntity::System::GetModel()->MaximumExposureUSec() / 1000;
    }
}
