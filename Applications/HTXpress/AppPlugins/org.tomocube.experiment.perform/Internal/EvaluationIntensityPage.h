#pragma once
#include <memory>

#include <QWidget>

#include "EvaluationPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationIntensityPage : public QWidget, public EvaluationPage {
        Q_OBJECT

    public:
        EvaluationIntensityPage(QWidget* parent = nullptr);
        ~EvaluationIntensityPage() override;

        auto Enter() -> void override;
        auto Leave() -> void override;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}