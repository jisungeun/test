#pragma once
#include <memory>
#include <QImage>

#include <AppEntityDefines.h>
#include <ImagingConfig.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class LiveViewerControl {
    public:
        LiveViewerControl();
        ~LiveViewerControl();

        auto GetLatestImage(QImage& image)->bool;

        auto MoveSampleStage(int32_t posX, int32_t posY)->bool;
        auto SetViewMode(AppEntity::Modality modality, int32_t channel = 0)->bool;

        auto ChangeLiveImagingMode(const AppEntity::ImagingMode mode)->bool;
        auto ChangeLiveImagingCondition(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain)->bool;
        auto ApplyChannelConditionToLiveCondition(const AppEntity::ImagingMode mode)->bool;
        auto ApplyToAcquisitionConfig(const AppEntity::ImagingMode mode)->bool;

        auto GetSaturationMaxThreshold() const -> int32_t;
        auto GetSaturationMinThreshold() const -> int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}