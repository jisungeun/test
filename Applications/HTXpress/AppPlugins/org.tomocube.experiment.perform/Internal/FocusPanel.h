#pragma once
#include <memory>
#include <QWidget>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class FocusPanel : public QWidget {
        Q_OBJECT
    public:
        explicit FocusPanel(QWidget* parent = nullptr);
        ~FocusPanel() override;

    protected slots:
        void onAutoFocusFailed();
        void onEnableAutofocus(bool enable);
        void onSelected(int index);
        void onMove(double step);
        void onStartJog(int direction);
        void onStopJog();
        void onControlAF(int chkState);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}