#pragma once
#include <memory>
#include <QWidget>

#include <AppEntityDefines.h>
#include <ChannelConfig.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class ViewModePanel : public QWidget {
        Q_OBJECT
    public:
        ViewModePanel(QWidget* parent = nullptr);
        ~ViewModePanel();

        auto SetCurrent(AppEntity::Modality modality, int32_t channel = 0)->void;
        auto Deselect() const->void;

    protected slots:
        void onSelected(int index);
        void onUpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types);
        void onUpdateFLChannelConfig(const int32_t channel, const AppEntity::ChannelConfig::Pointer config);

    signals:
        void sigSelected(AppEntity::Modality modality, int32_t channel);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
