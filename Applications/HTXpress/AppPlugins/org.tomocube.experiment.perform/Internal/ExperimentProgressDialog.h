#pragma once
#include <memory>
#include <QDialog>
#include <QString>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class ExperimentProgressDialog : public QDialog {
        Q_OBJECT

    public:
        ExperimentProgressDialog(QWidget* parent = nullptr);
        ~ExperimentProgressDialog() override;

        auto UpdateProgress(double progress, int32_t elapsedSeconds, int32_t remainSeconds)->void;
        auto ShowMessage(const QString& message)->void;

    protected:
        bool eventFilter(QObject* watched, QEvent* event) override;
        void hideEvent(QHideEvent* event) override;

    signals:
        void sigStop();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}