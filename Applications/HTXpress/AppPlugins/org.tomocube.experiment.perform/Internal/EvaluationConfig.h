#pragma once
#include <memory>

#include <QString>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationConfig {
    public:
        using Pointer = std::shared_ptr<EvaluationConfig>;

    protected:
        EvaluationConfig();

    public:
        ~EvaluationConfig();

        static auto GetInstance()->Pointer;

        auto SetReportFolder(const QString& path)->void;
        auto GetReportFolder() const->QString;

        auto GetBeadCropSizeInPixels() const->int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}