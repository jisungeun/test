﻿#include "SystemStorageObserver.h"
#include "SystemStorageUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    SystemStorageObserver::SystemStorageObserver(QObject* parent) : QObject(parent) {
        SystemStorageUpdater::GetInstance()->Register(this);
    }

    SystemStorageObserver::~SystemStorageObserver() {
        SystemStorageUpdater::GetInstance()->Deregister(this);
    }

    auto SystemStorageObserver::UpdateSystemStorageSpace(const int64_t& totalBytes, const int64_t& availableBytes) -> void {
        emit sigUpdateSystemStorageSpace(totalBytes, availableBytes);
    }

    auto SystemStorageObserver::UpdateMinRequiredSpace(const int32_t& gigabytes) -> void {
        emit sigUpdateMinRequiredSpace(gigabytes);
    }
}
