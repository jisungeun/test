#pragma once
#include <memory>
#include <QList>
#include <QImage>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SetupBackgroundCalPageControl {
    public:
        SetupBackgroundCalPageControl();
        ~SetupBackgroundCalPageControl();

        auto GetNAs() const->QList<double>;
        auto Parepare(double NA) const->bool;
        auto StartCalibrate(double NA) const->bool;
        auto Calibrate(double NA) const->bool;
        auto Save(double NA) const->bool;
        auto LoadImage(int32_t index)->QImage;
        auto SetLatestData(const QString& fileFullPath)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}