#pragma once
#include <memory>
#include <IExperimentIOView.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class ExperimentIOObserver;

    class ExperimentIOUpdater : public Interactor::IExperimentIOView {
    public:
        using Pointer = std::shared_ptr<ExperimentIOUpdater>;

    protected:
        ExperimentIOUpdater();

    public:
        ~ExperimentIOUpdater() override;

        static auto GetInstance()->Pointer;

        auto Update(AppEntity::Experiment::Pointer experiment, bool reloaded) -> void override;
        auto Error(const QString& message) -> void override;

    protected:
        auto Register(ExperimentIOObserver* observer)->void;
        auto Deregister(ExperimentIOObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class ExperimentIOObserver;
    };
}
