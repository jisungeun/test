#include <QFileDialog>
#include <QTimer>

#include <MessageDialog.h>

#include "Utility.h"
#include "LiveviewAnnotationMatrix.h"
#include "EvaluationConfig.h"
#include "AcquisitionDataObserver.h"
#include "EvaluationBeadObserver.h"
#include "EvaluationReportWriter.h"
#include "EvaluationMatrixBeadsPageControl.h"
#include "EvaluationMatrixBeadsPage.h"
#include "ui_EvaluationMatrixBeadsPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationMatrixBeadsPage::Impl {
        EvaluationMatrixBeadsPageControl control;
        Ui::EvaluationMatrixBeadsPage ui;
        EvaluationMatrixBeadsPage* p{ nullptr };
        LiveviewAnnotationMatrix* annotation{ nullptr };

        EvaluationBeadObserver* evalObserver{ new EvaluationBeadObserver(p) };
        AcquisitionDataObserver* dataObserver{ new AcquisitionDataObserver(p) };

        Impl(EvaluationMatrixBeadsPage* p) : p{ p } {}

        auto InitUi()->void;
        auto Clear()->void;
        auto Acquire()->void;
        auto AcquireCompleted()->void;
        auto AcquisitionFailed(const QString& error)->void;
        auto Evaluate()->void;
        auto EvaluateCompleted()->void;
        auto Save()->void;
        auto UpdateAnnotation()->void;
    };

    auto EvaluationMatrixBeadsPage::Impl::InitUi() -> void {
        ui.setupUi(p);

        ui.countCombo->addItem("3x3", 3);
        ui.countCombo->addItem("5x5", 5);
        ui.countCombo->addItem("7x7", 7);
        ui.countCombo->addItem("9x9", 9);
        ui.countCombo->addItem("11x11", 11);
        ui.countCombo->addItem("13x13", 13);
        ui.countCombo->addItem("15x15", 15);

        ui.countCombo->setCurrentText(QString("%1x%1").arg(control.GetTileCount()));
        ui.countCombo->setEnabled(false);

        ui.acquireProgressBar->hide();
        ui.evaluateBtn->setDisabled(true);
        ui.evalProgressBar->hide();
        ui.saveBtn->setDisabled(true);
        ui.savedLabel->hide();
        ui.cvValue->setValue(0);
        ui.resultLabel->hide();

        ui.xGap->setValue(control.GetGapX());
        ui.xGap->setReadOnly(true);

        ui.yGap->setValue(control.GetGapY());
        ui.yGap->setReadOnly(true);

        ui.resultTable->setColumnCount(6);
        ui.resultTable->setHorizontalHeaderLabels({"Row", "Column", "Volume", "Dry Mass", "Mean dRI", "Correlation"});
        ui.resultTable->horizontalHeader()->setStretchLastSection(true);
        ui.resultTable->setColumnWidth(1, 50);
        ui.resultTable->setColumnWidth(2, 50);

        ui.refLabel->setText(QString("(Ref : %1)").arg(control.GetCVReference(), 0, 'f', 3));

        for (const auto& label : p->findChildren<QLabel*>()) {
            if (label->objectName().contains("subtitle")) {
                label->setObjectName("label-h5");
            }
            else if (label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
            else if(label->objectName().contains("ref")) {
                label->setObjectName("label-h6");
            }
        }

        for (const auto& button : p->findChildren<QPushButton*>()) {
            if (button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            }
            else {
                button->setObjectName("bt-maintenance-light");
            }
        }

        for (const auto& groupBox : p->findChildren<QGroupBox*>()) {
            groupBox->setObjectName("gb-maintenance-main");
        }

        for (const auto& table : p->findChildren<QTableWidget*>()) {
            table->setStyleSheet(QString("QTableWidget{border-bottom: 0px;}"));
            table->verticalHeader()->setStyleSheet("QHeaderView{border-bottom: 0px;}");
            table->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);
        }

        ui.leftWidget->setObjectName("panel");
        ui.rightWidget->setObjectName("panel");
        ui.botWidget->setObjectName("panel");
    }

    auto EvaluationMatrixBeadsPage::Impl::Clear() -> void {
        control.Clear();

        ui.acquireProgressBar->hide();
        ui.evaluateBtn->setDisabled(true);
        ui.evalProgressBar->hide();
        ui.saveBtn->setDisabled(true);
        ui.savedLabel->hide();
        ui.cvValue->setValue(0);
        ui.resultLabel->hide();
        ui.resultTable->clearContents();

        ui.acquireProgressBar->setValue(0);
        ui.evalProgressBar->setValue(0);

        evalObserver->disconnect(p);
        dataObserver->disconnect(p);
    }

    auto EvaluationMatrixBeadsPage::Impl::Acquire() -> void {
        Clear();
        ui.acquireProgressBar->show();

        connect(evalObserver, &EvaluationBeadObserver::sigUpdateAcquisitionProgress, p, [this](double progress) {
            ui.acquireProgressBar->setValue(progress*100);
            if(progress == 1.0) AcquireCompleted();
        });

        connect(evalObserver, &EvaluationBeadObserver::sigNotifyAcquisitionError, p, [this](const QString& error) {
            AcquisitionFailed(error);
        });

        connect(evalObserver, &EvaluationBeadObserver::sigUpdateEvaluationProgress, p, [this](double progress) {
            ui.evalProgressBar->setValue(progress*100);
        });

        connect(dataObserver, &AcquisitionDataObserver::sigDataAdded, p, 
                [this](const QString& fileFullPath) {
            if(!control.AddDataPath(fileFullPath)) {
                TC::MessageDialog::warning(p, tr("Evaluation"), tr("Evaluation data may not be stored correctly"));
            }
        });

        const auto tileCount = ui.countCombo->currentData().toInt();
        if(!control.AcquireData(tileCount, tileCount, ui.xGap->value(), ui.yGap->value())) {
            Clear();
        }
    }

    auto EvaluationMatrixBeadsPage::Impl::AcquireCompleted() -> void {
        ui.acquireProgressBar->hide();
        ui.evaluateBtn->setEnabled(true);
    }

    auto EvaluationMatrixBeadsPage::Impl::AcquisitionFailed(const QString& error) -> void {
        Clear();
        TC::MessageDialog::warning(p, tr("Evaluation"), error);
    }

    auto EvaluationMatrixBeadsPage::Impl::Evaluate() -> void {
        ui.evalProgressBar->show();
        control.StartEvaluation();
        EvaluateCompleted();
    }

    auto EvaluationMatrixBeadsPage::Impl::EvaluateCompleted() -> void {
        ui.evalProgressBar->hide();

        auto [cvValue, result] = control.EvaluationResult();
        ui.cvValue->setValue(cvValue);

        if(result) {
            ui.resultLabel->setText("Pass");
            ui.resultLabel->setStyleSheet("QLabel {color:green; font: bold 14px;}");
            ui.resultLabel->show();
        } else {
            ui.resultLabel->setText("Fail");
            ui.resultLabel->setStyleSheet("QLabel {color:red;  font: bold 14px;}");
            ui.resultLabel->show();
        }

        const auto rowCount = control.GetDataCount();
        ui.resultTable->setRowCount(rowCount);
        for(auto rowIdx=0; rowIdx<rowCount; rowIdx++) {
            auto [rowInWell, colInWell, score] = control.GetScore(rowIdx);

            auto item = new QTableWidgetItem(QString::number(rowInWell));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 0, item);

            item = new QTableWidgetItem(QString::number(colInWell));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 1, item);

            item = new QTableWidgetItem(QString::number(score.GetVolume(), 'f', 2));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 2, item);

            item = new QTableWidgetItem(QString::number(score.GetDrymass(), 'f', 3));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 3, item);

            item = new QTableWidgetItem(QString::number(score.GetMeanDeltaRI(), 'f', 3));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 4, item);

            item = new QTableWidgetItem(QString::number(score.GetCorrelation(), 'f', 3));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 5, item);
        }

        ui.saveBtn->setEnabled(true);
    }

    auto EvaluationMatrixBeadsPage::Impl::Save() -> void {
        const auto path = EvaluationConfig::GetInstance()->GetReportFolder();
        if(!control.Save(path)) {
            TC::MessageDialog::warning(p, tr("Evaluation"), tr("Failed to save the reproducibility evaluation report"));
            return;
        }

        ui.savedLabel->show();
        QTimer::singleShot(2000, [this](){ ui.savedLabel->hide(); });
    }

    auto EvaluationMatrixBeadsPage::Impl::UpdateAnnotation() -> void {
        const auto tileCount = ui.countCombo->currentData().toInt();
        const auto cropSize = EvaluationConfig::GetInstance()->GetBeadCropSizeInPixels();
        const auto xGap = Utility::um2pixel(ui.xGap->value());
        const auto yGap = Utility::um2pixel(ui.yGap->value());

        annotation->SetTile(cropSize, cropSize, xGap, yGap);
        annotation->SetMatrix(tileCount, tileCount);
    }

    EvaluationMatrixBeadsPage::EvaluationMatrixBeadsPage(QWidget* parent)
        : QWidget(parent)
        , EvaluationPage()
        , d{ std::make_unique<Impl>(this) } {
        d->InitUi();

        d->annotation = new LiveviewAnnotationMatrix();
        d->UpdateAnnotation();

        connect(d->ui.countCombo, QOverload<int>::of(&QComboBox::currentIndexChanged), this, [this](int32_t index) {
            Q_UNUSED(index)
            d->UpdateAnnotation();
        });

        connect(d->ui.xGap, QOverload<int>::of(&QSpinBox::valueChanged), this, [this](int32_t value) {
            Q_UNUSED(value)
            d->UpdateAnnotation();
        });

        connect(d->ui.yGap, QOverload<int>::of(&QSpinBox::valueChanged), this, [this](int32_t value) {
            Q_UNUSED(value)
            d->UpdateAnnotation();
        });

        connect(d->ui.acquireBtn, &QPushButton::clicked, this, [this]() {
            d->Acquire();
        });

        connect(d->ui.evaluateBtn, &QPushButton::clicked, this, [this]() {
            d->Evaluate();
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this]() {
            d->Save();
        });
    }

    EvaluationMatrixBeadsPage::~EvaluationMatrixBeadsPage() {
    }

    auto EvaluationMatrixBeadsPage::Enter() -> void {
        d->Clear();
        d->annotation->Install(0, 0);
    }

    auto EvaluationMatrixBeadsPage::Leave() -> void {
        d->Clear();
        d->annotation->Unintall();
    }

    void EvaluationMatrixBeadsPage::resizeEvent(QResizeEvent* event) {
        QWidget::resizeEvent(event);
    }
}
