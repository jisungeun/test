#pragma once
#include <memory>

#include <AppEntityDefines.h>
#include <Position.h>
#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Utility {
    auto pixel2um(int32_t pixels)->double;
    auto um2pixel(double dist)->int32_t;

    auto global2vessel(const AppEntity::Position& globalPos)->AppEntity::Position;

    auto well2global(const AppEntity::WellIndex& wellIdx, const AppEntity::Position& position)->AppEntity::Position;
    auto global2well(const AppEntity::WellIndex& wellIdx, const AppEntity::Position& globalPos)->AppEntity::Position;

    auto well2vessel(const AppEntity::WellIndex& wellIdx, const AppEntity::Position& position)->AppEntity::Position;
    auto vessel2well(const AppEntity::WellIndex& wellIdx, const AppEntity::Position& globalPos)->AppEntity::Position;

    auto GetExChannel(int32_t waveLength, int32_t bandWidth)->int32_t;
    auto GetEmChannel(int32_t waveLength, int32_t bandWidth)->int32_t;

    //Utilities only for service features
    namespace Service {
        auto OverrideNA(AppEntity::Experiment::Pointer& experiment, double NA) -> double;
    }
}