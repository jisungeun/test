#pragma once
#include <memory>

#include <QList>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SetupCAFPageControl {
    public:
        SetupCAFPageControl();
        ~SetupCAFPageControl();

        auto GetNAs() const->QList<double>;
        auto GetLEDIntensity(double NA) const->int32_t;
        auto RunCAF(double NA, int32_t intensity) const->bool;
        auto SaveLEDIntensity(double NA, int32_t intensity) const->bool;
        auto GetDiskImageCount() const->int32_t;
        auto LoadDiskImage(int32_t index, QImage& image, double& meanIntensity) const->bool;
        auto GetCAxisBaseline(double NA) const->double;
        auto GetCAxisCurrent() const->double;
        auto GetLatestImage(QImage& image)->bool;
        auto MoveCAxis(double targetMm)->bool;
        auto SaveCAxisOffset(double NA, double offset)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}