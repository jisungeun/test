#include <QTimer>

#include <MessageDialog.h>

#include "SetupIntensityPageControl.h"
#include "SetupIntensityPage.h"
#include "ui_SetupIntensityPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupIntensityPage::Impl {
        enum ModalityIndex {
            BF = 0,
            HT
        };

        SetupIntensityPage* p{ nullptr };
        SetupIntensityPageControl control;
        Ui::SetupIntensityPage ui;
        QTimer updateTimer;

        ModalityIndex modality{ ModalityIndex::BF };
        bool bfRefresh{ false };
        bool htRefresh{ false };
        bool cafValid{ false };

        Impl(SetupIntensityPage* p) : p{p} {
        }

        auto InitUi()->void;
        auto ChangeModality(int32_t index)->void;
        auto ChangeBFIntensity(int32_t value)->void;
        auto ChangeBFExposure(int32_t value)->void;
        auto SaveBFIntensity()->void;
        auto RefreshBFImage(bool refresh)->void;
        auto RefreshHTImage(bool refresh)->void;
        auto ChangeHTNA()->void;
        auto ChangeHTIntensity(int32_t value)->void;
        auto RunCAF()->void;
        auto SaveHTIntensity()->void;

        //Start/Stop updating image
        auto StartUpdate(bool start)->void;
        auto UpdateImage()->void;
    };

    auto SetupIntensityPage::Impl::InitUi() -> void {
        //BF
        ui.bfExposure->setRange(1, 50);

        ui.bfPausedBtn->setChecked(true);
        ui.bfSavedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.bfSavedLabel->hide();

        //HT
        ui.htNA->clear();
        ui.htNA->addItem("", 0);
        auto NAs = control.GetNAs();
        for(auto NA : NAs) {
            ui.htNA->addItem(QString::number(NA), NA);
        }

        ui.htPausedBtn->setChecked(true);
        ui.htLiveBtn->setDisabled(true);
        ui.htPausedBtn->setDisabled(true);
        ui.saveHtIntensityBtn->setDisabled(true);
        ui.saveHtIntensityBtn->setToolTip("It saves the current LED intensity value to HT Illumination Calibration Parameter on system configuration");
        ui.htSavedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.htSavedLabel->hide();

        //Common
        ui.tabWidget->setCurrentIndex(0);
        ui.intensityView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.intensityView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        //TODO calculating average values per tile and then display values on the image (Not requested yet)
        ui.tilesCombo->hide();

        for (const auto& label : p->findChildren<QLabel*>()) {
            if(label->objectName().contains("subtitle")) {
                label->setObjectName("label-h5");
            }
            else if(label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
        }

        for (const auto& button : p->findChildren<QPushButton*>()) {
            if(button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            } else {
                button->setObjectName("bt-maintenance-light");
            }
        }
    }

    auto SetupIntensityPage::Impl::ChangeModality(int32_t index) -> void {
        auto modalityChanged = (modality != index);
        if(!modalityChanged) return;

        if(modality == ModalityIndex::BF) RefreshBFImage(false);
        if(modality == ModalityIndex::HT) {
            ui.htNA->setCurrentIndex(0);
            ChangeHTNA();
        }

        modality = static_cast<ModalityIndex>(index);
    }

    auto SetupIntensityPage::Impl::ChangeBFIntensity(int32_t value) -> void {
        if(!bfRefresh) return;
        control.ChangeBFIntensity(value);
    }

    auto SetupIntensityPage::Impl::ChangeBFExposure(int32_t value) -> void {
        auto usec = [](int32_t value)->int32_t { return value * 1000; };
        if(!bfRefresh) return;
        control.ChangeBFExposure(usec(value));
    }

    auto SetupIntensityPage::Impl::SaveBFIntensity() -> void {
        const auto intensity = ui.bfLEDIntensity->value();

        if(!control.SaveBFLEDIntensity(intensity)) {
            TC::MessageDialog::warning(p, tr("Setup"),
                                       tr("Failed to save BF LED Intensity"));
            return;
        }

        ui.bfSavedLabel->show();
        QTimer::singleShot(2000, [this]() { ui.bfSavedLabel->hide(); });
    }

    auto SetupIntensityPage::Impl::RefreshBFImage(bool refresh) -> void {
        auto stateChanged = (bfRefresh != refresh);
        if(!stateChanged) return;
        bfRefresh = refresh;

        if (bfRefresh) {
            ChangeBFExposure(ui.bfExposure->value());
            StartUpdate(true);
        } else {
            StartUpdate(false);
        }
    }

    auto SetupIntensityPage::Impl::RefreshHTImage(bool refresh) -> void {
        const auto NA = ui.htNA->currentData().toDouble();
        if(NA == 0) refresh = false;

        auto stateChanged = (htRefresh != refresh);
        if(!stateChanged) return;
        htRefresh = refresh;

        if (htRefresh) {
            control.ShowHTIlluminationSetupPattern(NA, ui.htLEDIntensity->value());
            StartUpdate(true);
        } else {
            StartUpdate(false);
        }

        ui.htSavedLabel->hide();
    }

    auto SetupIntensityPage::Impl::ChangeHTNA() -> void {
        cafValid = false;
        ui.htPausedBtn->setChecked(true);
        ui.htLiveBtn->setDisabled(true);
        ui.htPausedBtn->setDisabled(true);
        ui.saveHtIntensityBtn->setDisabled(true);
        ui.htSavedLabel->hide();
    }

    auto SetupIntensityPage::Impl::ChangeHTIntensity(int32_t value) -> void {
        if(!htRefresh) return;
        const auto NA = ui.htNA->currentData().toDouble();
        control.ShowHTIlluminationSetupPattern(NA, value);
        ui.htSavedLabel->hide();
    }

    auto SetupIntensityPage::Impl::RunCAF() -> void {
        const auto NA = ui.htNA->currentData().toDouble();
        if(NA == 0) return;

        if(!control.RunCAF(NA)) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("Condenser autofocus is failed"));
            return;
        }

        cafValid = true;
        ui.htLiveBtn->setEnabled(true);
        ui.htPausedBtn->setEnabled(true);
        ui.saveHtIntensityBtn->setEnabled(true);
    }

    auto SetupIntensityPage::Impl::SaveHTIntensity() -> void {
        const auto NA = ui.htNA->currentData().toDouble();
        const auto intensity = ui.htLEDIntensity->value();
        if(NA == 0) return;

        if(!control.SaveHTLEDIntensity(NA, intensity)) {
            TC::MessageDialog::warning(p, tr("Setup"),
                                       tr("Failed to save LED Intensity for NA %1").arg(NA));
            return;
        }

        ui.htSavedLabel->show();
        QTimer::singleShot(2000, [this]() { ui.htSavedLabel->hide(); });
    }

    auto SetupIntensityPage::Impl::StartUpdate(bool start) -> void {
        if(start) updateTimer.start(200);
        else updateTimer.stop();
    }

    auto SetupIntensityPage::Impl::UpdateImage() -> void {
        QImage image;
        if(!control.GetLatestImage(image)) return;

        ui.intensityView->ShowImage(image);

        auto avgIntensity = control.CalculateAverage(image);
        ui.averageIntensity->setValue(avgIntensity);
    }

    SetupIntensityPage::SetupIntensityPage(QWidget* parent) : QWidget(parent), d{new Impl(this)} {
        d->ui.setupUi(this);
        d->InitUi();

        connect(d->ui.tabWidget, &QTabWidget::currentChanged, this, [this](int32_t index) {
            d->ChangeModality(index);
        });

        connect(d->ui.bfLEDIntensity, &QSpinBox::editingFinished, this, [this]() {
            d->ChangeBFIntensity(d->ui.bfLEDIntensity->value());
        });

        connect(d->ui.bfExposure, &QSpinBox::editingFinished, this, [this]() {
            d->ChangeBFExposure(d->ui.bfExposure->value());
        });

        connect(d->ui.bfLiveBtn, &QPushButton::clicked, this, [this]() {
            d->RefreshBFImage(true);
        });

        connect(d->ui.bfPausedBtn, &QPushButton::clicked, this, [this]() {
            d->RefreshBFImage(false);
        });

        connect(d->ui.saveBFIntensityBtn, &QPushButton::clicked, this, [this]() {
            d->SaveBFIntensity();
        });

        connect(d->ui.htLiveBtn, &QPushButton::clicked, this, [this]() {
            d->RefreshHTImage(true);
        });

        connect(d->ui.htPausedBtn, &QPushButton::clicked, this, [this]() {
            d->RefreshHTImage(false);
        });

        connect(d->ui.htNA, &QComboBox::currentTextChanged, this, [=]() {
            d->ChangeHTNA();
        });

        connect(d->ui.runCAFBtn, &QPushButton::clicked, this, [this]() {
            d->RunCAF();
        });

        connect(d->ui.htLEDIntensity, &QSpinBox::editingFinished, this, [this]() {
            d->ChangeHTIntensity(d->ui.htLEDIntensity->value());
        });

        connect(d->ui.saveHtIntensityBtn, &QPushButton::clicked, this, [this]() {
            d->SaveHTIntensity();
        });

        connect(&d->updateTimer, &QTimer::timeout, this, [this]() {
            d->UpdateImage();
        });
    }

    SetupIntensityPage::~SetupIntensityPage() {
    }

    auto SetupIntensityPage::Enter() -> void {
    }

    auto SetupIntensityPage::Leave() -> void {
    }

    void SetupIntensityPage::resizeEvent(QResizeEvent* event) {
        d->ui.intensityView->FitZoom();
        QWidget::resizeEvent(event);
    }
}
