#include <QList>

#include "AcquisitionPositionObserver.h"
#include "AcquisitionPositionUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct AcquisitionPositionUpdater::Impl {
        QList<AcquisitionPositionObserver*> observers;
    };

    AcquisitionPositionUpdater::AcquisitionPositionUpdater() : Interactor::IAcqPositionView(), d{new Impl} {
    }

    AcquisitionPositionUpdater::~AcquisitionPositionUpdater() {
    }

    auto AcquisitionPositionUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new AcquisitionPositionUpdater() };
        return theInstance;
    }

    auto AcquisitionPositionUpdater::AddPosition(LocationIndex locationIdx, WellIndex wellIndex, Location::Pointer loc) -> void {
        for(auto observer : d->observers) {
            observer->AddPosition(locationIdx, wellIndex, loc);
        }
    }

    auto AcquisitionPositionUpdater::RefreshList(WellIndex wellIndex, QMap<LocationIndex, Location::Pointer>& locList) -> void {
        for(auto observer : d->observers) {
            observer->RefreshList(wellIndex, locList);
        }
    }

    auto AcquisitionPositionUpdater::DeletePosition(WellIndex wellIdx, LocationIndex locationIdx) -> void {
        for(auto observer : d->observers) {
            observer->DeletePosition(wellIdx, locationIdx);
        }
    }

    auto AcquisitionPositionUpdater::Register(AcquisitionPositionObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto AcquisitionPositionUpdater::Deregister(AcquisitionPositionObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}
