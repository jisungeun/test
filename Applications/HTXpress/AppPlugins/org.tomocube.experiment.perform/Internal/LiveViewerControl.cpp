#include <InstrumentController.h>
#include <MotionController.h>
#include <LiveviewConfigController.h>
#include <LiveviewConfigPresenter.h>

#include "LiveImageSource.h"
#include "InstrumentUpdater.h"
#include "MotionUpdater.h"
#include "LiveviewConfigUpdater.h"
#include "LiveViewerControl.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct LiveViewerControl::Impl {
        struct {
            int32_t oversaturationThreshold{251}; // oversaturation 사용 시 255 이하 입력
            int32_t undersaturationThreshold{-1}; // undersaturation 사용 시 0 이상 입력
        }const saturationRange;

        auto Convert(const AppEntity::ImagingMode mode) -> QPair<AppEntity::Modality, int32_t>;
    };

    auto LiveViewerControl::Impl::Convert(const AppEntity::ImagingMode mode) -> QPair<AppEntity::Modality, int32_t> {
        auto modality = AppEntity::Modality::HT;
        auto channel = 0;

        switch (mode) {
        case AppEntity::ImagingMode::HT3D:
        case AppEntity::ImagingMode::HT2D:
            modality = AppEntity::Modality::HT;
            channel = 0;
            break;
        case AppEntity::ImagingMode::BFGray:
        case AppEntity::ImagingMode::BFColor:
            modality = AppEntity::Modality::BF;
            channel = 0;
            break;
        case AppEntity::ImagingMode::FLCH0:
            modality = AppEntity::Modality::FL;
            channel = 0;
            break;
        case AppEntity::ImagingMode::FLCH1:
            modality = AppEntity::Modality::FL;
            channel = 1;
            break;
        case AppEntity::ImagingMode::FLCH2:
            modality = AppEntity::Modality::FL;
            channel = 2;
            break;
        }

        return qMakePair(modality, channel);
    }

    LiveViewerControl::LiveViewerControl() : d{ new Impl } {
    }

    LiveViewerControl::~LiveViewerControl() {
    }

    auto LiveViewerControl::GetLatestImage(QImage& image) -> bool {
        return LiveImageSource::GetInstance()->GetLatestImage(image);
    }

    auto LiveViewerControl::MoveSampleStage(int32_t posX, int32_t posY) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveRelativePixels(posX, posY);
    }

    auto LiveViewerControl::SetViewMode(AppEntity::Modality modality, int32_t channel) -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return [&]()->bool {
            switch(modality) {
            case AppEntity::Modality::BF:
                return controller.StartLiveBF();
            case AppEntity::Modality::FL:
                return controller.StartLiveFL(channel);
            }
            return false;
        }();
    }

    auto LiveViewerControl::ChangeLiveImagingMode(const AppEntity::ImagingMode mode) -> bool {
        auto updater = LiveviewConfigUpdater::GetInstance();
        auto presenter = Interactor::LiveviewConfigPresenter(updater.get());
        auto controller = Interactor::LiveviewConfigController(&presenter);
        return controller.SetImagingMode(mode);
    }

    auto LiveViewerControl::ChangeLiveImagingCondition(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain) -> bool {
        auto updater = LiveviewConfigUpdater::GetInstance();
        auto presenter = Interactor::LiveviewConfigPresenter(updater.get());
        auto controller = Interactor::LiveviewConfigController(&presenter);
        if(controller.SetLiveImagingConfig(mode, intensity, exposure, gain)) {
            auto instUpdater = InstrumentUpdater::GetInstance();
            auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
            auto instController = Interactor::InstrumentController(&instPresenter);

            return [&]()->bool {
                auto [modality, channel] = d->Convert(mode);
                switch(modality) {
                case AppEntity::Modality::BF:
                    return instController.ChangeLiveBF();
                case AppEntity::Modality::FL:
                    return instController.ChangeLiveFL(channel);
                }
                return false;
            }();
        }

        return false;
    }

    auto LiveViewerControl::ApplyChannelConditionToLiveCondition(const AppEntity::ImagingMode mode) -> bool {
        auto updater = LiveviewConfigUpdater::GetInstance();
        auto presenter = Interactor::LiveviewConfigPresenter(updater.get());
        auto controller = Interactor::LiveviewConfigController(&presenter);
        if(controller.ApplyChannelCondition(mode)) {
            auto instUpdater = InstrumentUpdater::GetInstance();
            auto instPresenter = Interactor::InstrumentPresenter(instUpdater.get());
            auto instController = Interactor::InstrumentController(&instPresenter);

            return [&]()->bool {
                auto [modality, channel] = d->Convert(mode);
                switch(modality) {
                case AppEntity::Modality::BF:
                    return instController.ChangeLiveBF();
                case AppEntity::Modality::FL:
                    return instController.ChangeLiveFL(channel);
                }
                return false;
            }();
        }

        return false;
    }

    auto LiveViewerControl::ApplyToAcquisitionConfig(const AppEntity::ImagingMode mode) -> bool {
        auto updater = LiveviewConfigUpdater::GetInstance();
        auto presenter = Interactor::LiveviewConfigPresenter(updater.get());
        auto controller = Interactor::LiveviewConfigController(&presenter);
        return controller.ApplyToAcquisitionConfig(mode);
    }

    auto LiveViewerControl::GetSaturationMaxThreshold() const -> int32_t {
        return d->saturationRange.oversaturationThreshold;
    }

    auto LiveViewerControl::GetSaturationMinThreshold() const -> int32_t {
        return d->saturationRange.undersaturationThreshold;
    }
}
