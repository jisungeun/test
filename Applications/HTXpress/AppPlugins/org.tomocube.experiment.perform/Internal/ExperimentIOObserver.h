#pragma once
#include <memory>
#include <QObject>

#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class ExperimentIOObserver : public QObject {
        Q_OBJECT
    public:
        ExperimentIOObserver(QObject* parent);
        ~ExperimentIOObserver() override;

        auto Update(AppEntity::Experiment::Pointer experiment, bool reloaded)->void;
        auto Error(const QString& message) -> void;

    signals:
        void sigUpdate(AppEntity::Experiment::Pointer experiment, bool reloaded);
        void sigError(const QString& message);
    };
}