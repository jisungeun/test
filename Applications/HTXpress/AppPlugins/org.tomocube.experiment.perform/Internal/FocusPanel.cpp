#define LOGGER_TAG "[FocusPanel]"

#include <QVBoxLayout>
#include <QButtonGroup>
#include <QPushButton>

#include <TCLogger.h>
#include <MessageDialog.h>
#include <ZControlPanel.h>

#include "MotionObserver.h"
#include "InstrumentObserver.h"
#include "FocusPanelControl.h"
#include "FocusPanel.h"

#include <QCheckBox>

namespace HTXpress::AppPlugins::Experiment::Perform {
    enum ButtonIndex {
        AutoFocus,
        SetBestFocus,
        AutoFocusStatus
    };

    struct FocusPanel::Impl {
        FocusPanelControl control;
        QButtonGroup* buttons{ nullptr };
        QCheckBox* afCheck{ nullptr };

        MotionObserver* motionObserver{ nullptr };
        InstrumentObserver* instrumentObserver{ nullptr };

        bool neverSet{ true };

        auto AddButton(const QString& label, int32_t index)->QPushButton*;
    };

    auto FocusPanel::Impl::AddButton(const QString& label, int32_t index) -> QPushButton* {
        auto* btn = new QPushButton(label);
        btn->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed) ;
        btn->setFixedSize(46, 34);
        btn->setCheckable(false);
        btn->setFocusPolicy(Qt::FocusPolicy::NoFocus);
        buttons->addButton(btn, index);
        return btn;
    }

    FocusPanel::FocusPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        using ZControlPanel = HTXpress::AppComponents::ZControlPanel::Widget;

        d->buttons = new QButtonGroup(this);
        d->buttons->setExclusive(true);

        auto* zControl = new ZControlPanel(this);
        zControl->SetStepRange(0.1, 50.0);
        zControl->EnableJog(false);
        zControl->setMaximumWidth(46);

        d->afCheck = new QCheckBox(this);
        d->afCheck->setChecked(true);
        d->afCheck->setObjectName("tsx-focuspanel-afcheck");
         
        auto* vboxLayout = new QVBoxLayout(this);
        {
            vboxLayout->addWidget(zControl);

            auto* btn = d->AddButton("Set", ButtonIndex::SetBestFocus);
            btn->setToolTip("Set the current Z position to the best focus position");
            btn->setStyleSheet("background: #FC686C");
            vboxLayout->addWidget(btn);

            btn = d->AddButton("AF", ButtonIndex::AutoFocus);
            btn->setToolTip("Perform auto focus");
            vboxLayout->addWidget(btn);

            vboxLayout->addWidget(d->afCheck);

            vboxLayout->addStretch();
        }
        vboxLayout->setContentsMargins(10, 20, 10, 20);
        vboxLayout->setSpacing(10);
        setLayout(vboxLayout);

        d->motionObserver = new MotionObserver(this);
        d->instrumentObserver = new InstrumentObserver(this);

        connect(d->motionObserver, SIGNAL(sigAFFailed()), this, SLOT(onAutoFocusFailed()));
        connect(d->instrumentObserver, SIGNAL(sigAFFailed()), this, SLOT(onAutoFocusFailed()));
        connect(d->instrumentObserver, SIGNAL(sigAFEnabled(bool)), this, SLOT(onEnableAutofocus(bool)));

        connect(d->buttons, SIGNAL(idClicked(int)), this, SLOT(onSelected(int)));
        connect(zControl, SIGNAL(sigMove(double)), this, SLOT(onMove(double)));
        connect(zControl, SIGNAL(sigStartJog(int)), this, SLOT(onStartJog(int)));
        connect(zControl, SIGNAL(sigStopJog()), this, SLOT(onStopJog()));
        connect(d->afCheck, SIGNAL(stateChanged(int)), this, SLOT(onControlAF(int)));
    }

    FocusPanel::~FocusPanel() {
    }

    void FocusPanel::onAutoFocusFailed() {
        d->control.EnableAutoFocus(false);
    }

    void FocusPanel::onEnableAutofocus(bool enable) {
        d->afCheck->blockSignals(true);
        d->afCheck->setCheckState(enable ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
        d->afCheck->blockSignals(false);
        for(auto button : d->buttons->buttons()) {
            button->setEnabled(enable);
        }
    }

    void FocusPanel::onSelected(int index) {
        switch(index) {
        case ButtonIndex::AutoFocus:
            d->control.PerformAF();
            break;
        case ButtonIndex::SetBestFocus:
            d->control.SetBestFocus();
            if(d->neverSet) {
                const auto style = d->buttons->button(ButtonIndex::AutoFocus)->styleSheet();
                d->buttons->button(index)->setStyleSheet(style);
                d->neverSet = false;
            }
            break;
        }
    }

    void FocusPanel::onMove(double step) {
        d->control.MoveZ(step);
    }

    void FocusPanel::onStartJog(int direction) {
        d->control.StartJog(direction > 0);
    }

    void FocusPanel::onStopJog() {
        d->control.StopJog();
    }

    void FocusPanel::onControlAF(int chkState) {
        auto enableAF = (chkState == Qt::CheckState::Checked);
        d->control.EnableAutoFocus(enableAF);
    }

}
