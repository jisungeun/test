﻿#include <System.h>
#include <SystemStatus.h>

#include <AcqPositionSetupController.h>
#include <AcqPositionPresenter.h>
#include <ImagingSequenceSetupController.h>
#include <ImagingPlanController.h>
#include <AcquireImageController.h>
#include <ExperimentController.h>
#include <ExperimentIOController.h>
#include <MotionController.h>
#include <MarkPositionSetupController.h>
#include <LiveviewConfigController.h>
#include <LiveviewConfigPresenter.h>
#include <InstrumentController.h>

#include "Utility.h"
#include "ImagingPlanUpdater.h"
#include "AcquisitionPositionUpdater.h"
#include "RunExperimentUpdater.h"
#include "ExperimentIOUpdater.h"
#include "MotionUpdater.h"
#include "MarkPositionUpdater.h"
#include "InstrumentUpdater.h"
#include "ControlPanelControl.h"

#include "LiveviewConfigUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct ControlPanelControl::Impl {
        struct {
            double xInUm{ 0 };
            double yInUm{ 0 };
        } fov;
        AppEntity::ScanConfig scanConfig;
        bool loadingExperiment{ false };

        auto CurrentWellIndex() const->AppEntity::WellIndex;
        auto CurrentPosition() const->AppEntity::Position;

        int64_t bytesAvailable{};
    };

    auto ControlPanelControl::Impl::CurrentWellIndex() const -> AppEntity::WellIndex {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        return sysStatus->GetCurrentWell();
    }

    auto ControlPanelControl::Impl::CurrentPosition() const -> AppEntity::Position {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        return Utility::global2well(sysStatus->GetCurrentWell(), sysStatus->GetCurrentGlobalPosition());
    }

    ControlPanelControl::ControlPanelControl() : d{new Impl} {
    }

    ControlPanelControl::~ControlPanelControl() {
    }

    auto ControlPanelControl::LoadingExperiment(const bool loading) -> void {
        d->loadingExperiment = loading;
    }

    auto ControlPanelControl::PerformHTIlluminationCalibration() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return controller.CalibrateHTIllumination();
    }

    auto ControlPanelControl::AddCurrentPosition(const AppEntity::Area& area, bool isTile) -> bool {
        auto updater = AcquisitionPositionUpdater::GetInstance();
        auto presenter = Interactor::AcqPositionPresenter(updater.get());
        auto controller = Interactor::AcqPositionSetupController(&presenter);
        return controller.AddCurrentArea(area, isTile);
    }

    auto ControlPanelControl::AddCurrentTilePosition(double xInMm, double yInMm, const AppEntity::Area& area, bool isTile) -> bool {
        auto updater = AcquisitionPositionUpdater::GetInstance();
        auto presenter = Interactor::AcqPositionPresenter(updater.get());
        auto controller = Interactor::AcqPositionSetupController(&presenter);
        return controller.AddCurrentTileArea(xInMm, yInMm, area, isTile);
    }

    auto ControlPanelControl::AddMarkPosition(double xInMm, double yInMm, const AppEntity::Area& area, bool isTile) -> bool {
       auto updater = MarkPositionUpdater::GetInstance();
       auto presenter = Interactor::MarkPositionPresenter(updater.get());
       auto controller = Interactor::MarkPositionSetupController(&presenter);
       return controller.AddMarkPosition(xInMm, yInMm, area, isTile);
    }

    auto ControlPanelControl::SetTileScanCondition(bool enable, double xInMm, double yInMm, 
                                                   double widthInUm, double heightInUm) -> bool {
        const auto wellIdx = d->CurrentWellIndex();
        if(wellIdx == -1) return false;

        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingPlanController(!d->loadingExperiment, &presenter);
        return controller.SetTileScanArea(enable, wellIdx, xInMm, yInMm, widthInUm, heightInUm);
    }

    auto ControlPanelControl::SetFieldOfView(double xInUm, double yInUm) -> bool {
        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingPlanController(!d->loadingExperiment, &presenter);

        double adjustedXInUm, adjustedYInUm;
        if(!controller.SetFieldOfView(xInUm, yInUm, adjustedXInUm, adjustedYInUm)) return false;

        d->fov.xInUm = adjustedXInUm;
        d->fov.yInUm = adjustedYInUm;

        return true;
    }

    auto ControlPanelControl::SetFLScanConfig(const FLScanConfig& uiConfig) -> bool {
        const auto zResolution = AppEntity::System::GetModel()->AxisResolutionPPM(AppEntity::Model::Axis::Z);
        const auto zPPUm = zResolution / 1000.0;

        auto scanConfig = AppEntity::ScanConfig();
        scanConfig.SetScanMode(uiConfig.scanMode);
        scanConfig.SetRange(uiConfig.scanBottom*zPPUm, uiConfig.scanTop*zPPUm, uiConfig.scanStep*zPPUm);
        scanConfig.SetFocusUm(uiConfig.flFocusOffset);
        scanConfig.SetSteps(uiConfig.scanSteps);

        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingPlanController(!d->loadingExperiment, &presenter);

        d->scanConfig = scanConfig;

        return controller.SetFLScanConfig(scanConfig);
    }

    auto ControlPanelControl::GetFLScanConfig() const -> FLScanConfig {
        const auto zResolution = AppEntity::System::GetModel()->AxisResolutionPPM(AppEntity::Model::Axis::Z);
        const auto zPPUm = zResolution / 1000.0;

        FLScanConfig config;

        config.scanMode = d->scanConfig.GetScanMode();
        config.scanBottom = d->scanConfig.GetBottom() / zPPUm;
        config.scanTop = d->scanConfig.GetTop() / zPPUm;
        config.scanStep = d->scanConfig.GetStep() / zPPUm;
        config.flFocusOffset = d->scanConfig.GetFocusFLOffsetUm();
        config.scanSteps = d->scanConfig.GetSteps();

        return config;
    }

    auto ControlPanelControl::ClearFLChannels() -> void {
        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingPlanController(!d->loadingExperiment, &presenter);

        controller.ClearFLChannels();
    }

    auto ControlPanelControl::SetFLChannel(const int32_t channel, const AppEntity::ChannelConfig& channelConfig) -> bool {
        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingPlanController(!d->loadingExperiment, &presenter);

        return controller.SetFLChannel(channel, channelConfig);
    }

    auto ControlPanelControl::SetHTEnabled(const bool is3D, const bool enabled) -> bool {
        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingPlanController(!d->loadingExperiment, &presenter);
        return controller.SetHTEnabled(is3D, enabled);
    }

    auto ControlPanelControl::SetFLEnabled(const bool is3D, const bool enabled) -> bool {
        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingPlanController(!d->loadingExperiment, &presenter);
        return controller.SetFLEnabled(is3D, enabled);
    }

    auto ControlPanelControl::SetBFEnabled(const bool enabled) -> bool {
        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingPlanController(!d->loadingExperiment, &presenter);
        return controller.SetBFEnabled(enabled);
    }

    auto ControlPanelControl::ReloadExperiment() -> bool {
        auto updater = ExperimentIOUpdater::GetInstance();
        auto presenter = Interactor::ExperimentIOPresenter(updater.get());
        auto controller = Interactor::ExperimentIOController(&presenter);
        return controller.Reload();
    }

    auto ControlPanelControl::ApplySequences(const QList<Sequence>& sequences) -> bool {
        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingSequenceSetupController(&presenter);
        if(!controller.Clear()) {
            return false;
        }

        QList<Interactor::ImagingSequenceSetupController::Sequence> controllerSequences;

        for(const auto& sequence : sequences) {
            auto seqInput = Interactor::ImagingSequenceSetupController::Sequence();

            for (auto& imagingType : sequence.GetImagingTypes()) {
                auto modality = Interactor::ImagingSequenceSetupController::Sequence::Modality();

                switch (imagingType) {
                case AppEntity::ImagingType::HT3D:
                case AppEntity::ImagingType::HT2D:
                    modality.modality = AppEntity::Modality::HT;
                    modality.is3D = (imagingType == +AppEntity::ImagingType::HT3D);
                    break;
                case AppEntity::ImagingType::FL3D:
                case AppEntity::ImagingType::FL2D:
                    modality.modality = AppEntity::Modality::FL;
                    if (imagingType == +AppEntity::ImagingType::FL3D) {
                        modality.is3D = true;
                        modality.channels = sequence.GetFL3DChannels();
                    } else {
                        modality.is3D = false;
                        modality.channels = sequence.GetFL2DChannels();
                    }
                    break;
                case AppEntity::ImagingType::BFGray:
                case AppEntity::ImagingType::BFColor:
                    modality.modality = AppEntity::Modality::BF;
                    modality.isGray = true; // 현재 BF gray 만 지원

                    break;
                }

                 seqInput.AddModality(modality);
            }

            auto startTime = sequence.GetStartTime().ToSeconds();
            auto interval = sequence.GetInterval().ToSeconds();
            auto duration = sequence.GetDuration().ToSeconds();

            seqInput.SetStartTimestamp(startTime);
            seqInput.SetInterval(interval);
            seqInput.SetCount((interval==0) ? 1 : duration / interval + 1);

            controllerSequences << seqInput;
        }

        if(!controller.ApplyImagingSequences(controllerSequences)) {
            return false;
        }

        return true;
    }

    auto ControlPanelControl::ValidateSequences(const QList<Sequence>& sequences,
                                                const UseCase::ImagingTimeTable::Pointer& timeTable,
                                                QList<int32_t>& requiredTime) -> bool {
        bool valid{ true };

        const auto cycleTime = timeTable->GetTimeForCycle();
        const auto sequenceCount = sequences.size();

        for(auto seqIdx=0; seqIdx<sequenceCount; seqIdx++) {
            const auto sequence = sequences.at(seqIdx);
            const auto startTime = sequence.GetStartTime();
            const auto imageTypes = sequence.GetImagingTypes();
            const auto interval = sequence.GetInterval();
            const auto duration = sequence.GetDuration();

            double timeForImaging = 0;
            for(auto imageType : imageTypes) {
                switch(imageType) {
                case AppEntity::ImagingType::HT2D:
                    timeForImaging += timeTable->GetTimeForCondition(AppEntity::Modality::HT, true);
                    break;
                case AppEntity::ImagingType::HT3D:
                    timeForImaging += timeTable->GetTimeForCondition(AppEntity::Modality::HT, false);
                    break;
                case AppEntity::ImagingType::FL2D:
                    timeForImaging += timeTable->GetTimeForCondition(AppEntity::Modality::FL, true);
                    break;
                case AppEntity::ImagingType::FL3D:
                    timeForImaging += timeTable->GetTimeForCondition(AppEntity::Modality::FL, false);
                    break;
                case AppEntity::ImagingType::BFGray:
                    timeForImaging += timeTable->GetTimeForCondition(AppEntity::Modality::BF, true);
                    break;
                case AppEntity::ImagingType::BFColor:
                    timeForImaging += timeTable->GetTimeForCondition(AppEntity::Modality::BF, false);
                    break;
                }
            }

            const auto required = cycleTime + (timeForImaging * timeTable->GetPointCount());
            const auto intervalInSecs = interval.ToSeconds();
            const auto timeSliceCount = (intervalInSecs == 0) ? 1 : (duration.ToSeconds() / intervalInSecs + 1);

            if(intervalInSecs > 0 ) {
                if(required > interval.ToSeconds()) {
                    valid = false;
                }
            }

            if(seqIdx < (sequenceCount - 1)) {
                const auto nextSequence = sequences.at(seqIdx + 1);
                const auto nextStartTime = nextSequence.GetStartTime();

                if((startTime.ToSeconds() + required) > nextStartTime.ToSeconds()) {
                    valid = false;
                }
            }

            for(auto idx=0; idx<timeSliceCount; idx++) {
                requiredTime.push_back(required);
            }
        }

        return valid;
    }

    auto ControlPanelControl::AcquireImage(const QList<ImageType>& images, double roiXInUm, double roiYInUm) -> bool {
        const auto posInWell = d->CurrentPosition();
        return AcquireImage(images, posInWell.toMM().x, posInWell.toMM().y, roiXInUm, roiYInUm);
    }

    auto ControlPanelControl::AcquireImage(const QList<ImageType>& images, 
                                           double xInMm, double yInMm, 
                                           double roiXInUm, double roiYInUm) -> bool {
        auto updater = RunExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter::GetInstance(updater.get());
        auto controller = Interactor::AcquireImageController(presenter.get());

        controller.SetFieldOfView(d->fov.xInUm, d->fov.yInUm);
        controller.SetFLScanConfig(d->scanConfig);

        using ImagingType = Interactor::AcquireImageController::ImagingType;
        QList<ImagingType> imagingTypes;

        const auto systemStatus = AppEntity::SystemStatus::GetInstance();

        QMap<int32_t,AppEntity::ChannelConfig> channelConfigs;
        channelConfigs[0] = *systemStatus->GetChannelConfig(AppEntity::ImagingMode::FLCH0);
        channelConfigs[1] = *systemStatus->GetChannelConfig(AppEntity::ImagingMode::FLCH1);
        channelConfigs[2] = *systemStatus->GetChannelConfig(AppEntity::ImagingMode::FLCH2);

        for(auto image : images) {
            ImagingType imagingType;

            switch(image) {
            case ImageType::HT3D:
                imagingType.modality = AppEntity::Modality::HT;
                imagingType.is3D = true;
                break;
            case ImageType::HT2D:
                imagingType.modality = AppEntity::Modality::HT;
                imagingType.is3D = false;
                break;
            case ImageType::FL3D:
                imagingType.modality = AppEntity::Modality::FL;
                imagingType.is3D = true;
                imagingType.channels = [&]()->QList<int32_t> {
                    QList<int32_t> channels;
                    for(auto channel : channelConfigs.keys()) {
                        if(channelConfigs[channel].GetEnable() == false) continue;
                        channels.push_back(channel);
                    }
                    return channels;
                }();
                break;
            case ImageType::FL2D:
                imagingType.modality = AppEntity::Modality::FL;
                imagingType.is3D = false;
                imagingType.channels = [&]()->QList<int32_t> {
                    QList<int32_t> channels;
                    for(auto channel : channelConfigs.keys()) {
                        if(channelConfigs[channel].GetEnable() == false) continue;
                        channels.push_back(channel);
                    }
                    return channels;
                }();
                break;
            case ImageType::BFGray:
                imagingType.modality = AppEntity::Modality::BF;
                imagingType.isGray = true;
                break;
            case ImageType::BFColor:
                imagingType.modality = AppEntity::Modality::BF;
                imagingType.isGray = false;
                break;
            }
            imagingTypes.push_back(imagingType);
        }

        for(auto channel : channelConfigs.keys()) {
            if(channelConfigs[channel].GetEnable() == false) continue;
            controller.SetFLChannel(channel, channelConfigs[channel]);
        }

        return controller.Acquire(imagingTypes, xInMm, yInMm, roiXInUm, roiYInUm);
    }

    auto ControlPanelControl::TestAcquire() -> bool {
        auto updater = RunExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter::GetInstance(updater.get());
        auto controller = Interactor::ExperimentController(presenter.get());
        return controller.TestExperiment();
    }

    auto ControlPanelControl::RunExperiment() -> bool {
        auto updater = RunExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter::GetInstance(updater.get());
        auto controller = Interactor::ExperimentController(presenter.get());
        return controller.RunExperiment();
    }

    auto ControlPanelControl::StopAcquisition() -> bool {
        auto updater = RunExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter::GetInstance(updater.get());
        auto controller = Interactor::ExperimentController(presenter.get());
        return controller.StopExperiment();
    }

    auto ControlPanelControl::SetAcquisitionLock(bool locked) -> bool {
        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingPlanController(!d->loadingExperiment, &presenter);
        return controller.SetAcquisitionLock(locked);
    }

    auto ControlPanelControl::GetCurrentWellIndex() const -> AppEntity::WellIndex {
        return d->CurrentWellIndex();
    }

    auto ControlPanelControl::GetCurrentPositionInWell() const -> AppEntity::Position {
        return d->CurrentPosition();
    }

    auto ControlPanelControl::MoveZAxis(double posInMm) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveMM(AppEntity::Axis::Z, posInMm);
    }

    auto ControlPanelControl::SetEstimated3DModeInterval(const QList<int32_t>& flEnabledChannels) -> bool {
        Sequence sequence;

        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingSequenceSetupController(&presenter);

        QList<Interactor::ImagingSequenceSetupController::Sequence> controllerSequences;

        auto seqInput = Interactor::ImagingSequenceSetupController::Sequence();
        QList<AppEntity::ImagingType> imagingType3Ds;

        imagingType3Ds << AppEntity::ImagingType::HT3D << AppEntity::ImagingType::BFColor;

        if(!flEnabledChannels.empty()) 
            imagingType3Ds << AppEntity::ImagingType::FL3D;

        sequence.AddImagingTypes(imagingType3Ds);
        sequence.AddFL3DChannels(flEnabledChannels);
        sequence.SetInterval({0});
        sequence.SetDuration({0});
        sequence.SetStartTime({0});

        for (auto& imagingType : sequence.GetImagingTypes()) {
            auto modality = Interactor::ImagingSequenceSetupController::Sequence::Modality();

            switch (imagingType) {
                case AppEntity::ImagingType::HT3D:
                case AppEntity::ImagingType::HT2D: modality.modality = AppEntity::Modality::HT;
                    modality.is3D = (imagingType == +AppEntity::ImagingType::HT3D);
                    break;
                case AppEntity::ImagingType::FL3D:
                case AppEntity::ImagingType::FL2D: modality.modality = AppEntity::Modality::FL;
                    if (imagingType == +AppEntity::ImagingType::FL3D) {
                        modality.is3D = true;
                        modality.channels = sequence.GetFL3DChannels(); // 현재 활성화된 fl channel
                    }
                    else {
                        modality.is3D = false;
                        modality.channels = sequence.GetFL2DChannels();
                    }
                    break;
                case AppEntity::ImagingType::BFGray:
                case AppEntity::ImagingType::BFColor: modality.modality = AppEntity::Modality::BF;
                    modality.isGray = true; // 현재 BF gray 만 지원

                    break;
            }

            qDebug() << "imagingType=" << imagingType << " channels=" << modality.channels;

            seqInput.AddModality(modality);
        }

        auto startTime = sequence.GetStartTime().ToSeconds();
        auto interval = sequence.GetInterval().ToSeconds();
        auto duration = sequence.GetDuration().ToSeconds();

        seqInput.SetStartTimestamp(startTime);
        seqInput.SetInterval(interval);
        seqInput.SetCount((interval == 0) ? 1 : duration / interval + 1);

        controllerSequences << seqInput;

        if (!controller.ApplyImagingSequencesFull3D(controllerSequences)) {
            return false;
        }

        return true;
    }

    
    auto ControlPanelControl::GetEstimated3DModeInterval(const UseCase::ImagingTimeTable::Pointer& timeTable) -> AppComponents::TimelapseImagingPanel::TimelapseImagingTime {
        using TimelapseImagingTime = AppComponents::TimelapseImagingPanel::TimelapseImagingTime;
        const auto cycleTime = timeTable->GetTimeForCycle();
        QList<AppEntity::ImagingType> imageTypes;
        
        imageTypes << AppEntity::ImagingType::FL3D << AppEntity::ImagingType::HT3D << AppEntity::ImagingType::BFColor;
        double timeForImaging = 0;
        for(auto imageType : imageTypes) {
            switch(imageType) {
                case AppEntity::ImagingType::HT3D:
                timeForImaging += timeTable->GetTimeForCondition(AppEntity::Modality::HT, false);
                break;

                case AppEntity::ImagingType::FL3D:
                timeForImaging += timeTable->GetTimeForCondition(AppEntity::Modality::FL, false);
                break;

                case AppEntity::ImagingType::BFColor:
                timeForImaging += timeTable->GetTimeForCondition(AppEntity::Modality::BF, false);
                break;

                default: break;
            }
        }

        auto pointCount = 1;
        if(timeTable->GetPointCount() != 0) {
            pointCount = timeTable->GetPointCount();
        }

        const TimelapseImagingTime estimatedInterval = cycleTime + (timeForImaging * pointCount);
        return estimatedInterval;
    }

    auto ControlPanelControl::ChangeImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode) -> bool {
        auto updater = LiveviewConfigUpdater::GetInstance();
        auto presenter = Interactor::LiveviewConfigPresenter(updater.get());
        auto controller = Interactor::LiveviewConfigController(&presenter);
        return controller.ChangeImagingSettingMode(imagingSettingMode);
    }

    auto ControlPanelControl::UpdateRequiredSpace(const int32_t& acquisitionPoints) -> bool {
        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingSequenceSetupController(&presenter);
        return controller.GetAcquisitionDataRequiredSpace(acquisitionPoints);
    }

    auto ControlPanelControl::SetAvailableStorageSpace(const int64_t& bytesAvailable) -> void {
        d->bytesAvailable = bytesAvailable;
    }

    auto ControlPanelControl::IsSpaceEnough(const int64_t& bytesRequired) const -> bool {
        const auto result = d->bytesAvailable - bytesRequired > 0;
        return result;
    }

    auto ControlPanelControl::TileDeactivation() -> bool {
        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingPlanController(!d->loadingExperiment, &presenter);
        return controller.TileDeactivation();
    }
}
