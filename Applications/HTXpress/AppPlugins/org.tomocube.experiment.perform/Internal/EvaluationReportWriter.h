#pragma once
#include <memory>

#include <QString>
#include <QImage>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationReportWriter {
    public:
        using Pointer = std::shared_ptr<EvaluationReportWriter>;

    protected:
        EvaluationReportWriter();

    public:
        ~EvaluationReportWriter();

        static auto GetInstance()->Pointer;

        auto SetPath(const QString& path)->void;
        auto GetPath() const->QString;

        auto SetTitle(const QString& title)->void;

        auto SaveImage(const QString& name, const QImage& image)->bool;
        auto SaveResults(const QString& name, const QMap<QString,QVariant>& data)->bool;
        auto SaveFile(const QString& name, const QString& sourceFile)->bool;
        auto CopyDirectory(const QString& name, const QString& sourceDirectory)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}