#include "Utility.h"
#include "EvaluationConfig.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationConfig::Impl {
        QString reportPath;
    };

    EvaluationConfig::EvaluationConfig() : d{ std::make_unique<Impl>() } {
    }

    EvaluationConfig::~EvaluationConfig() {
    }

    auto EvaluationConfig::GetInstance() -> Pointer {
        static Pointer theInstance{ new EvaluationConfig() };
        return theInstance;
    }

    auto EvaluationConfig::SetReportFolder(const QString& path) -> void {
        d->reportPath = path;
    }

    auto EvaluationConfig::GetReportFolder() const -> QString {
        return d->reportPath;
    }

    auto EvaluationConfig::GetBeadCropSizeInPixels() const -> int32_t {
        return Utility::um2pixel(50.25);
    }
}
