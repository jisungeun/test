#include <QFileDialog>

#include "EvaluationConfig.h"
#include "ui_EvaluationSetupPage.h"
#include "EvaluationSetupPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationSetupPage::Impl {
        EvaluationSetupPage* p{ nullptr };
        Ui::EvaluationSetupPage ui;

        Impl(EvaluationSetupPage* p) : p{ p } {
        }

        auto SelectReportFolder()->void;
    };

    auto EvaluationSetupPage::Impl::SelectReportFolder() -> void {
        auto str = QFileDialog::getExistingDirectory(p, tr("Select a folder to save evaluation reports"), ui.reportFolder->text());
        if(str.isEmpty()) return;

        auto config = EvaluationConfig::GetInstance();
        config->SetReportFolder(str);

        ui.reportFolder->setText(str);
    }

    EvaluationSetupPage::EvaluationSetupPage(QWidget* parent) : QWidget(parent), d{ std::make_unique<Impl>(this) } {
        d->ui.setupUi(this);

        connect(d->ui.selectReportFolderBtn, &QPushButton::clicked, this, [this]() {
            d->SelectReportFolder();
            emit sigReady();
        });
    }

    EvaluationSetupPage::~EvaluationSetupPage() {
    }

    auto EvaluationSetupPage::Enter() -> void {
        auto config = EvaluationConfig::GetInstance();
        d->ui.reportFolder->setText(config->GetReportFolder());
    }

    auto EvaluationSetupPage::Leave() -> void {
    }
}
