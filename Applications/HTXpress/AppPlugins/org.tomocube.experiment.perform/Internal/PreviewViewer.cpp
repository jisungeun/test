#define LOGGER_TAG "[PreviewViewer]"
#include <QFileDialog>

#include <TCLogger.h>
#include <MessageDialog.h>

#include <System.h>
#include <ProgressDialog.h>
#include <PreviewPanel.h>

#include "Utility.h"
#include "PreviewObserver.h"
#include "MotionObserver.h"
#include "InstrumentObserver.h"
#include "AcquisitionPositionObserver.h"
#include "ImagingPlanObserver.h"
#include "RunExperimentObserver.h"
#include "ExperimentIOObserver.h"

#include "PreviewViewerControl.h"
#include "PreviewViewer.h"
#include "ui_PreviewViewer.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct PreviewViewer::Impl {
        Ui::PreviewViewer ui;
        PreviewViewer*p { nullptr };
        PreviewViewerControl control;
        PreviewObserver* previewObserver{ nullptr };
        MotionObserver* motionObserver{ nullptr };
        InstrumentObserver* instrumentObserver{ nullptr };
        AcquisitionPositionObserver* positionObserver{ nullptr };
        ImagingPlanObserver* imagingPlanObserver{ nullptr};
        RunExperimentObserver* runExperimentObserver{ nullptr };
        ExperimentIOObserver* experimentIOObserver{ nullptr };

        TC::ProgressDialog* procDialog{ nullptr };

        bool lastTileVisibleStatus{};

        Impl(PreviewViewer* p) : p{ p } {}

        auto UpdateImageSize(int32_t xPixels, int32_t yPixels)->void;
        auto UpdatePreviewBlock(int32_t xPos, int32_t yPos, const QImage& tileImage)->void;
        auto ClearImage()->void;
        auto StorePreview()->void;
        auto StopCapture()->void;
    };

    auto PreviewViewer::Impl::UpdateImageSize(int32_t xPixels, int32_t yPixels) -> void {
        control.UpdateImageSize(xPixels, yPixels);
        ui.view->SetImageSize(xPixels, yPixels);
    }

    auto PreviewViewer::Impl::UpdatePreviewBlock(int32_t xPos, int32_t yPos, const QImage& tileImage) -> void {
        ui.view->UpdateBlockImage(xPos, yPos, tileImage);
    }

    auto PreviewViewer::Impl::ClearImage() -> void {
        ui.view->Clear();
    }

    auto PreviewViewer::Impl::StorePreview() -> void {
        const auto& previewImage = ui.view->GetImage();
        if (previewImage.isNull()) {
            return;
        }

        static QString prevPath, prevDefaultPath;
        const auto defaultPath = control.GetDefaultPath();
        const auto openHere = [&]()->QString {
            if(defaultPath == prevDefaultPath) {
                if(prevPath.isEmpty()) return defaultPath;
                return prevPath;
            }
            prevPath.clear();
            return defaultPath;
        }();
        const auto defaultName = control.GetDefaultName(openHere);
        const auto path = QFileDialog::getSaveFileName(p,
                                                       "Saving preview image", 
                                                       QString("%1/%2").arg(openHere).arg(defaultName), "png (*.png)");
        if(path.isEmpty()) return;
        prevPath = QFileInfo(path).absolutePath();
        prevDefaultPath = defaultPath;

        control.StorePreview(path, previewImage);
    }

    auto PreviewViewer::Impl::StopCapture() -> void {
        control.StopCapture();
    }

    PreviewViewer::PreviewViewer(QWidget* parent) : QWidget(parent), d{ new Impl(this) } {
        d->ui.setupUi(this);
        d->previewObserver = new PreviewObserver(this);
        d->motionObserver = new MotionObserver(this);
        d->instrumentObserver = new InstrumentObserver(this);
        d->positionObserver = new AcquisitionPositionObserver(this);
        d->imagingPlanObserver = new ImagingPlanObserver(this);
        d->runExperimentObserver = new RunExperimentObserver(this);
        d->experimentIOObserver = new ExperimentIOObserver(this);

        auto model = AppEntity::System::GetModel();
        d->ui.view->SetUmPerPixel(model->CameraPixelSize() / model->ObjectiveLensMagnification());
        
        connect(d->ui.view, 
                SIGNAL(sigDoPreviewScan(const HTXpress::AppComponents::PreviewPanel::PreviewPanel::PreviewScanType&)), 
                this, 
                SLOT(onCapturePreview(const HTXpress::AppComponents::PreviewPanel::PreviewPanel::PreviewScanType&)));
        connect(d->ui.view, SIGNAL(sigMoveLive(int32_t,int32_t)), this, 
                SLOT(onMoveLive(int32_t,int32_t)));
        connect(d->ui.view, SIGNAL(sigChangeTileImagingArea(double,double,double,double)), this,
                SLOT(onTileImagingAreaChanged(double,double,double,double)));
        connect(d->ui.view, SIGNAL(sigRequestSetPreviewArea()), this, SLOT(onRequestSetPreviewArea()));
        connect(d->ui.view, SIGNAL(sigCancelSetPreviewArea()), this, SLOT(onCancelSetPreviewArea()));
        connect(d->ui.view, &HTXpress::AppComponents::PreviewPanel::PreviewPanel::sigSnapshotRequested, this, [this](){
                d->StorePreview();
                });

        connect(d->previewObserver, SIGNAL(sigUpdateProgress(double)), this, SLOT(onUpdateProgress(double)));
        connect(d->previewObserver, &PreviewObserver::sigUpdateImageSize, this,
                [this](int32_t xPixels, int32_t yPixels) {
                    d->UpdateImageSize(xPixels, yPixels);
                });
        connect(d->previewObserver, &PreviewObserver::sigUpdateBlock, this, 
                [this](int32_t xPos, int32_t yPos, const QImage& blockImage) {
                    d->UpdatePreviewBlock(xPos, yPos, blockImage);
                });
        connect(d->previewObserver, SIGNAL(sigUpdateCustomPreviewArea(double,double,int32_t,int32_t)), 
                this, SLOT(onUpdateCustomPreviewArea(double,double,int32_t,int32_t)));
        connect(d->motionObserver, SIGNAL(sigUpdateCurrentWell(AppEntity::WellIndex)), this,
                SLOT(onVesselChanged(AppEntity::WellIndex)));
        connect(d->motionObserver, SIGNAL(sigUpdatePosition(const AppEntity::WellIndex, const AppEntity::Position&)), this,
                SLOT(onUpdatePosition(const AppEntity::WellIndex, const AppEntity::Position&)));
        connect(d->instrumentObserver, SIGNAL(sigVesselLoaded(bool)), this, SLOT(onUpdateVesselStatus(bool)));

        //Acquisition Positions
        connect(d->positionObserver, SIGNAL(sigAddPosition(AppEntity::LocationIndex, AppEntity::WellIndex, AppEntity::Location::Pointer)), this, 
                SLOT(onAddPosition(AppEntity::LocationIndex, AppEntity::WellIndex, AppEntity::Location::Pointer)));
        connect(d->positionObserver, SIGNAL(sigRefreshList(AppEntity::WellIndex, QMap<AppEntity::LocationIndex, AppEntity::Location::Pointer>&)), this, 
                SLOT(onRefreshList(AppEntity::WellIndex, QMap<AppEntity::LocationIndex, AppEntity::Location::Pointer>&)));
        connect(d->positionObserver, SIGNAL(sigDeletePosition(AppEntity::WellIndex, AppEntity::LocationIndex)), this, 
                SLOT(onDeletePosition(AppEntity::WellIndex, AppEntity::LocationIndex)));

        //Imaging Plan
        connect(d->imagingPlanObserver, SIGNAL(sigUpdateTileScanArea(bool, AppEntity::WellIndex, double, double, double, double)), this,
                SLOT(onUpdateTileScanArea(bool, AppEntity::WellIndex, double, double, double, double)));
        connect(d->imagingPlanObserver, SIGNAL(sigUpdateFOV(const double, const double)), this, 
                SLOT(onUpdateFOV(const double, const double)));

        connect(d->runExperimentObserver, 
                SIGNAL(sigUpdateCurrentPosition(const AppEntity::WellIndex, const AppEntity::Position&)), 
                this,
                SLOT(onUpdatePosition(const AppEntity::WellIndex, const AppEntity::Position&)));
        connect(d->experimentIOObserver, SIGNAL(sigUpdate(AppEntity::Experiment::Pointer, bool)), this,
                SLOT(onUpdateExperiment(AppEntity::Experiment::Pointer, bool)));
        connect(d->runExperimentObserver, &RunExperimentObserver::sigUpdateProgress, this, &PreviewViewer::onUpdateSingleAcquireProgress);
    }

    PreviewViewer::~PreviewViewer() {
    }

    void PreviewViewer::onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool reloaded) {
        Q_UNUSED(experiment)

        if(reloaded) {
            return;
        }

        d->ClearImage();
    }

    void PreviewViewer::onCapturePreview(const AppComponents::PreviewPanel::PreviewPanel::PreviewScanType& type) {
        auto widthInUM = 0;
        auto heightInUM = 0;

        switch (type) {
        case AppComponents::PreviewPanel::PreviewPanel::PreviewScanType::Custom:
            d->control.MoveLensPosition();
            widthInUM = std::get<0>(d->control.GetCustomPreviewSizeInUM());
            heightInUM = std::get<1>(d->control.GetCustomPreviewSizeInUM());
            break;
        case AppComponents::PreviewPanel::PreviewPanel::PreviewScanType::Default:
            widthInUM = std::get<0>(d->control.GetDefaultROI());
            heightInUM = std::get<1>(d->control.GetDefaultROI());
            break;
        }

        QLOG_INFO() << "Capture preview image [w=" << widthInUM << "um, h=" << heightInUM <<"um]";

        d->ClearImage();
        if(!d->control.CapturePreview(widthInUM, heightInUM)) {
            QLOG_ERROR() << "It fails to capture a preview image";
            TC::MessageDialog::warning(this, tr("Preview Image Acquisition"), tr("Failed to capture preview"));
        }

        onUpdatePosition(d->control.CurrentWell(), d->control.CurrentCenterPosition());
    }

    void PreviewViewer::onRequestSetPreviewArea() {
        QLOG_INFO() << "onRequestSetPreviewArea";
        d->control.DoPreviewAreaSetting();
    }

    void PreviewViewer::onCancelSetPreviewArea() {
        QLOG_INFO() << "onCancelSetPreviewArea";
        d->control.CancelPreviewAreaSetting();
    }

    void PreviewViewer::onMoveLive(int32_t posX, int32_t posY) {
        QLOG_INFO() << "Move live view to [relX=" << posX << "px relY=" << posY << "px]";
        if(!d->control.MoveRelativeLive(posX, posY)) {
            QLOG_ERROR() << "It fails to move XY stage";
            TC::MessageDialog::warning(this, tr("Moving Live View"), tr("Failed to move stage"));
        }
    }

    void PreviewViewer::onTileImagingAreaChanged(double xInPixel, double yInPixel, 
                                                 double widthInPixel, double heightInPixel) {
        QLOG_INFO() << "Tile imaging area is update [X=" << xInPixel << " Y=" << yInPixel
                    << " W=" << widthInPixel << " H=" << heightInPixel << "]";
        if(!d->control.ChangeTileImagingArea(xInPixel, yInPixel, widthInPixel, heightInPixel)) {
            QLOG_ERROR() << "It fails to update tile image area information";
            TC::MessageDialog::warning(this, tr("Update Tile Imaging Area"), tr("Failed to update tiling information"));
        }
    }

    void PreviewViewer::onUpdateProgress(double progress) {
        if(d->procDialog == nullptr) {
            d->procDialog = new TC::ProgressDialog(tr("Generating preview."), tr("Stop"), 0, 100, this);
            connect(d->procDialog, &TC::ProgressDialog::canceled, this, [this]() {
                d->StopCapture();
            });
        }

        if(d->procDialog->wasCanceled()) {
            delete d->procDialog;
            d->procDialog = nullptr;
            return;
        }

        const auto step = std::min<int32_t>(progress*100, 100);
        if(d->procDialog->isHidden()) {
            d->procDialog->show();
        }
        d->procDialog->setValue(step);
    }

    void PreviewViewer::onVesselChanged(AppEntity::WellIndex wellIdx) {
        Q_UNUSED(wellIdx)
        d->ClearImage();
        d->ui.view->StopCustomPreviewArea();
    }

    void PreviewViewer::onUpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position) {
        if(wellIdx != d->control.CurrentWell()) {
            return;
        }

        int32_t posX, posY;
        if(!d->control.GetOffsetInPixels(wellIdx, position, posX, posY)) return;
        d->control.SetPositionInPixels(posX, posY);

        const auto fovInPixels = d->control.GetFOVInPixels();

        d->ui.view->SetROI(posX, posY, std::get<0>(fovInPixels), std::get<1>(fovInPixels));
        d->ui.view->SetTileImagingCenter(posX, posY);
    }

    void PreviewViewer::onUpdateVesselStatus(bool loaded) {
        setEnabled(loaded);
    }

    void PreviewViewer::onAddPosition(AppEntity::LocationIndex locationIdx, AppEntity::WellIndex wellIdx, 
                                      AppEntity::Location::Pointer location) {
        //TODO Acquisition Position 추가 구현
        Q_UNUSED(locationIdx)
        Q_UNUSED(wellIdx)
        Q_UNUSED(location)
    }

    void PreviewViewer::onRefreshList(AppEntity::WellIndex wellIdx,
                                      QMap<AppEntity::LocationIndex, AppEntity::Location::Pointer>& locationList) {
        //TODO Acquisition Position 목록 갱신 구현
        Q_UNUSED(wellIdx)
        Q_UNUSED(locationList)
    }

    void PreviewViewer::onDeletePosition(AppEntity::WellIndex wellIdx, AppEntity::LocationIndex locationIdx) {
        //Todo Acquisition Position 삭제 구현
        Q_UNUSED(wellIdx)
        Q_UNUSED(locationIdx)
    }

    void PreviewViewer::onUpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex, 
                                             double xInMm, double yInMm, 
                                             double widthInUm, double heightInUm) {
        if(wellIndex != d->control.CurrentWell()) return;

        int32_t posX{ 0 };
        int32_t posY{ 0 };
        d->control.GetOffsetInPixels(wellIndex, AppEntity::Position::fromMM(xInMm, yInMm, 0), posX, posY);

        d->ui.view->SetTileImagingActivation(enable);
        d->ui.view->SetTileImagingArea(posX, posY, 
                                           Utility::um2pixel(widthInUm), 
                                           Utility::um2pixel(heightInUm));

        d->lastTileVisibleStatus = enable;
    }

    void PreviewViewer::onUpdateFOV(const double xInUm, const double yInUm) {
        const auto fovXPixels = Utility::um2pixel(xInUm);
        const auto fovYPixels = Utility::um2pixel(yInUm);
        d->control.SetFOVInPixels(fovXPixels, fovYPixels);

        const auto posInPixels = d->control.GetPositionInPixels();
        d->ui.view->SetROI(std::get<0>(posInPixels), std::get<1>(posInPixels), fovXPixels, fovYPixels);
    }

    void PreviewViewer::onUpdateCustomPreviewArea(double wellXinMM, double wellYinMM, int32_t widthInUm, int32_t heightInUm) {
        d->control.SetCustomPreviewArea(wellXinMM, wellYinMM, widthInUm, heightInUm);
    }

    void PreviewViewer::onUpdateSingleAcquireProgress(const double progress, const int elapsedSeconds, const int remainSeconds) {
        Q_UNUSED(elapsedSeconds)
        Q_UNUSED(remainSeconds)

        d->ui.view->SetTileImagingActivation(false);
        if(const bool done = progress >= 1.0 ? true : false) {
            d->ui.view->SetTileImagingActivation(d->lastTileVisibleStatus);
        }
    }
}
