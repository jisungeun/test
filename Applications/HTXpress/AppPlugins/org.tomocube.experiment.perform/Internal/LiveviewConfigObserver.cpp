#include "LiveviewConfigUpdater.h"
#include "LiveviewConfigObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    LiveviewConfigObserver::LiveviewConfigObserver(QObject* parent) : QObject(parent) {
        LiveviewConfigUpdater::GetInstance()->Register(this);
    }

    LiveviewConfigObserver::~LiveviewConfigObserver() {
        LiveviewConfigUpdater::GetInstance()->Deregister(this);
    }

    auto LiveviewConfigObserver::UpdateLiveConfig(const uint32_t intensity, const uint32_t exposure, const double gain) -> void {
        emit sigLiveviewConfigChanged(intensity, exposure, gain);
    }

    auto LiveviewConfigObserver::UpdateAcquisitionConfig(const AppEntity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain) -> void {
        emit sigAcquisitionConfigChanged(mode, intensity, exposure, gain);
    }

    auto LiveviewConfigObserver::UpdateImagingSettingMode(const AppEntity::ImagingSettingMode& imagingSettingMode) -> void {
        emit sigImagingSettingModeChanged(imagingSettingMode);
    }

}
