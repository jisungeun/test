#pragma once
#include <memory>

#include <IExperimentView.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class RunExperimentObserver;

    class RunExperimentUpdater : public Interactor::IExperimentView {
    public:
        using Pointer = std::shared_ptr<RunExperimentUpdater>;

    protected:
        RunExperimentUpdater();

    public:
        ~RunExperimentUpdater();

        static auto GetInstance()->Pointer;

        auto UpdateProgress(const UseCase::ExperimentStatus& status) -> void override;
        auto NotifyStopped() -> void override;

    protected:
        auto Register(RunExperimentObserver* observer)->void;
        auto Deregister(RunExperimentObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class RunExperimentObserver;
    };
}