#include <QTimer>
#include <QChart>
#include <QLineSeries>

#include <MessageDialog.h>

#include "SetupPreviewPage.h"
#include "SetupPreviewPageControl.h"
#include "ui_SetupPreviewPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupPreviewPage::Impl {
        SetupPreviewPage* p{ nullptr };
        SetupPreviewPageControl control;
        Ui::SetupPreviewPage ui;

        struct {
            bool exposure{ false };
            bool gain{ false };
        } flag;

        struct {
            QChart* chart{ new QChart() };
            QLineSeries* dataSeries{ nullptr };
            QLineSeries* fittedSeries{ nullptr };
        } exposure;

        struct {
            QChart* chart{ new QChart() };
            QLineSeries* dataSeries{ nullptr };
            QLineSeries* fittedSeries{ nullptr };
        } gain;

        Impl(SetupPreviewPage* p) : p{ p } {
        }

        auto MeasureExposure()->void;
        auto MeasureGain()->void;
        auto Save()->void;

        auto InitUi()->void;
    };

    auto SetupPreviewPage::Impl::InitUi() -> void {
        ui.exposureChartView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.exposureChartView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        ui.gainChartView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.gainChartView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        exposure.chart->legend()->hide();
        ui.exposureChartView->setChart(exposure.chart);
        ui.exposureChartView->setRenderHint(QPainter::Antialiasing);

        gain.chart->legend()->hide();
        ui.gainChartView->setChart(gain.chart);
        ui.gainChartView->setRenderHint(QPainter::Antialiasing);

        ui.savedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.savedLabel->hide();
    }

    auto SetupPreviewPage::Impl::MeasureExposure() -> void {
        QList< SetupPreviewPageControl::Pair> values;
        double coeffP, coeffQ;

        ui.coeffP->setValue(0);
        ui.coeffQ->setValue(0);

        if (!control.MeasureExposure(values, coeffP, coeffQ)) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("Failed to get exposure parameters"));
            return;
        }

        ui.coeffP->setValue(coeffP);
        ui.coeffQ->setValue(coeffQ);
        exposure.chart->removeAllSeries();

        exposure.dataSeries = new QLineSeries();
        auto counts = values.size();
        for (auto idx = 0; idx < counts; idx++) {
            exposure.dataSeries->append(values.at(idx).first, values.at(idx).second);
        }
        exposure.dataSeries->setPen(QPen(QBrush(0x209FDF), 4, Qt::SolidLine));

        auto calcIntensity = [=](int32_t exposure)->double {
            return coeffP * exposure + coeffQ;
        };

        exposure.fittedSeries = new QLineSeries();
        exposure.fittedSeries->append(values.at(0).first, calcIntensity(values.at(0).first));
        exposure.fittedSeries->append(values.at(counts - 1).first, calcIntensity(values.at(counts - 1).first));
        exposure.fittedSeries->setPen(QPen(Qt::red, 2, Qt::DotLine));

        exposure.chart->addSeries(exposure.dataSeries);
        exposure.chart->addSeries(exposure.fittedSeries);
        exposure.chart->createDefaultAxes();
        exposure.chart->axes(Qt::Horizontal).at(0)->setTitleText("Exposure (msec)");
        exposure.chart->axes(Qt::Vertical).at(0)->setTitleText("Intensity");

        ui.tabWidget->setCurrentIndex(0);

        flag.exposure = true;
    }

    auto SetupPreviewPage::Impl::MeasureGain() -> void {
        QList< SetupPreviewPageControl::Pair> values;
        double coeffA, coeffB;

        ui.coeffA->setValue(0);
        ui.coeffB->setValue(0);
        gain.chart->removeAllSeries();

        if (!control.MeasureGain(values, coeffA, coeffB)) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("Failed to get gain parameters"));
            return;
        }

        ui.coeffA->setValue(coeffA);
        ui.coeffB->setValue(coeffB);

        gain.dataSeries = new QLineSeries();
        auto counts = values.size();
        for (auto idx = 0; idx < counts; idx++) {
            gain.dataSeries->append(values.at(idx).first, values.at(idx).second);
        }
        gain.dataSeries->setPen(QPen(QBrush(0x209FDF), 4, Qt::SolidLine));

        auto calcIntensity = [=](int32_t gain)->double {
            return std::min(255.0, coeffA * std::exp(coeffB * gain));
        };

        gain.fittedSeries = new QLineSeries();
        for (auto idx = 0; idx < counts; idx++) {
            gain.fittedSeries->append(values.at(idx).first, calcIntensity(values.at(idx).first));
        }
        gain.fittedSeries->setPen(QPen(Qt::red, 2, Qt::DotLine));


        gain.chart->addSeries(gain.dataSeries);
        gain.chart->addSeries(gain.fittedSeries);
        gain.chart->createDefaultAxes();
        gain.chart->axes(Qt::Horizontal).at(0)->setTitleText("Gain (dB)");
        gain.chart->axes(Qt::Vertical).at(0)->setTitleText("Intensity");
        
        ui.tabWidget->setCurrentIndex(1);

        flag.gain = true;
    }

    auto SetupPreviewPage::Impl::Save() -> void {
        if (!(flag.exposure && flag.gain)) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("Measure parameters first"));
            return;
        }

        if (!control.Save(ui.coeffA->value(), ui.coeffB->value(), ui.coeffP->value(), ui.coeffQ->value())) {
            TC::MessageDialog::warning(p, tr("Setup"),
                tr("Failed to save Preview parameters"));
            return;
        }

        ui.savedLabel->show();
        QTimer::singleShot(2000, [this]() { ui.savedLabel->hide();  });

        flag.exposure = false;
        flag.gain = false;
    }

    SetupPreviewPage::SetupPreviewPage(QWidget* parent) : QWidget(parent), d{ new Impl(this) } {
        d->ui.setupUi(this);
        d->InitUi();

        connect(d->ui.measureExposureBtn, &QPushButton::clicked, this, [this]() {
            d->MeasureExposure();
        });

        connect(d->ui.measureGainBtn, &QPushButton::clicked, this, [this]() {
            d->MeasureGain();
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this]() {
            d->Save();
        });
    }

    SetupPreviewPage::~SetupPreviewPage() {
    }

    auto SetupPreviewPage::Enter() -> void {
        d->flag.exposure = false;
        d->flag.gain = false;
        d->ui.tabWidget->setCurrentIndex(0);
    }

    auto SetupPreviewPage::Leave() -> void {
    }

    void SetupPreviewPage::resizeEvent(QResizeEvent* event) {
        QWidget::resizeEvent(event);
    }
}