#include <QDateTime>
#include <QFile>
#include <QFileInfo>
#include <QTextStream>

#include <FileUtility.h>

#include "EvaluationReportWriter.h"

#include <QDir>

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationReportWriter::Impl {
        QString topDirectory;
        QString title;
        QString timeStamp;

        auto BuildPath(const QString& name, const QString& ext) const ->QString;
        auto BuildDirectory(const QString& name) const->QString;
    };

    auto EvaluationReportWriter::Impl::BuildPath(const QString& name, const QString& ext) const -> QString {
        return QString("%1/%2.%3.%4.%5").arg(topDirectory).arg(title).arg(timeStamp).arg(name).arg(ext);
    }

    auto EvaluationReportWriter::Impl::BuildDirectory(const QString& name) const -> QString {
        auto path = QString("%1/%2.%3.%4.%5").arg(topDirectory).arg(title).arg(timeStamp).arg(name);
        if(!TC::MakeDir(path)) return "";
        return path;
    }

    EvaluationReportWriter::EvaluationReportWriter() : d{ std::make_unique<Impl>() } {
    }

    EvaluationReportWriter::~EvaluationReportWriter() {
    }

    auto EvaluationReportWriter::GetInstance() -> Pointer {
        static Pointer theInstance{ new EvaluationReportWriter() };
        return theInstance;
    }

    auto EvaluationReportWriter::SetPath(const QString& path) -> void {
        d->topDirectory = path;
    }

    auto EvaluationReportWriter::GetPath() const -> QString {
        return d->topDirectory;
    }

    auto EvaluationReportWriter::SetTitle(const QString& title) -> void {
        d->title = title;
        d->timeStamp = QDateTime::currentDateTime().toString("yyyyMMdd.hhmmss");
    }

    auto EvaluationReportWriter::SaveImage(const QString& name, const QImage& image) -> bool {
        if(d->topDirectory.isEmpty()) return false;
        if(d->title.isEmpty()) return false;
        if(image.isNull()) return false;

        return image.save(d->BuildPath(name, "png"));
    }

    auto EvaluationReportWriter::SaveResults(const QString& name, const QMap<QString, QVariant>& data) -> bool {
        if(d->topDirectory.isEmpty()) return false;
        if(d->title.isEmpty()) return false;

        QFile file(d->BuildPath(name, "csv"));
        if(!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)) return false;

        QTextStream out(&file);
        for(auto itr=data.begin(); itr!= data.end(); ++itr) {
            out << itr.key() << "," << itr.value().toString() << "\n";
        }

        return true;
    }

    auto EvaluationReportWriter::SaveFile(const QString& name, const QString& sourceFile) -> bool {
        if(d->topDirectory.isEmpty()) return false;
        if(d->title.isEmpty()) return false;

        auto ext = QFileInfo(sourceFile).completeSuffix();
        auto targetPath = d->BuildPath(name, ext);

        return QFile::copy(sourceFile, targetPath);
    }

    auto EvaluationReportWriter::CopyDirectory(const QString& name, const QString& sourceDirectory) -> bool {
        if(d->topDirectory.isEmpty()) return false;
        if(d->title.isEmpty()) return false;

        auto targetDir = d->BuildDirectory(name);
        if(targetDir.isEmpty()) return false;

        return TC::CopyFiles(sourceDirectory, targetDir, true);
    }
}
