#include <QChart>
#include <QLineSeries>
#include <QPointF>
#include <QTimer>

#include <MessageDialog.h>
#include <TCLogger.h>

#include "SetupAutofocusPageControl.h"
#include "SetupAutofocusPage.h"
#include "ui_SetupAutofocusPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct SetupAutofocusPage::Impl {
        SetupAutofocusPageControl control;
        Ui::SetupAutofocusPage ui;
        SetupAutofocusPage* p{ nullptr };
        QChart* chart{ new QChart() };
        QLineSeries* dataSeries{ nullptr };
        QLineSeries* slopSeries{ nullptr };
        QLineSeries* verLineSeries{ nullptr };
        QLineSeries* horLineSeries{ nullptr };

        struct {
            int32_t flag{ 0 };
            QPointF first;
            QPointF second;
        } measure;

        Impl(SetupAutofocusPage* p) : p{ p } {}
        auto InitUi()->void;
        auto Scan()->void;
        auto StartMeasure()->void;
        auto Measure(const QPointF& point)->void;
        auto Clear()->void;
        auto ReadSensorValue()->void;
        auto SaveParameter()->void;
    };

    auto SetupAutofocusPage::Impl::InitUi() -> void {
        ui.lowerRelPos->setRange(-10, 0);
        ui.upperRelPos->setRange(0, 10);

        ui.chartview->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ui.chartview->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        chart->legend()->hide();
        ui.chartview->setChart(chart);
        ui.chartview->setRenderHint(QPainter::Antialiasing);

        ui.slope->setRange(-std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max());
        ui.slope->setValue(0);

        ui.paramDefaultSensor->setValidator(new QIntValidator(p));

        ui.savedLabel->setStyleSheet("QLabel {color:yellow;}");
        ui.savedLabel->hide();
    }

    auto SetupAutofocusPage::Impl::Scan() -> void {
        Clear();

        auto lowerRelPos = ui.lowerRelPos->value();
        auto upperRelPos = ui.upperRelPos->value();

        auto values = control.Scan(lowerRelPos, upperRelPos);
        if(values.isEmpty()) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("It fails to perform autofocus"));
            return;
        }

        auto minIndex = std::distance(values.begin(), std::min_element(values.begin(), values.end()));
        auto maxIndex = std::distance(values.begin(), std::max_element(values.begin(), values.end()));

        ui.minValue->setValue(values.at(minIndex));
        ui.maxValue->setValue(values.at(maxIndex));

        auto steps = control.GetSteps();
        auto interval = (upperRelPos - lowerRelPos) / (steps - 1);

        dataSeries = new QLineSeries();
        auto counts = values.size();
        for(auto idx=0; idx<counts; idx++) {
            dataSeries->append(lowerRelPos + idx*interval, values.at(idx));
        }
        dataSeries->setPen(QPen(QBrush(0x209FDF), 4, Qt::SolidLine));

        horLineSeries = new QLineSeries();
        horLineSeries->append(lowerRelPos, 0);
        horLineSeries->append(upperRelPos, 0);
        horLineSeries->setPen(QPen(Qt::green, 1, Qt::DashDotDotLine));

        verLineSeries = new QLineSeries();
        verLineSeries->append(0, ui.minValue->value());
        verLineSeries->append(0, ui.maxValue->value());
        verLineSeries->setPen(QPen(Qt::green, 1, Qt::DashDotDotLine));

        chart->removeAllSeries();
        chart->addSeries(dataSeries);
        chart->addSeries(horLineSeries);
        chart->addSeries(verLineSeries);
        chart->createDefaultAxes();
        chart->axes(Qt::Horizontal).at(0)->setTitleText("Z Position (mm)");
        chart->axes(Qt::Vertical).at(0)->setTitleText("Sensor Value");

        connect(dataSeries, &QLineSeries::clicked, p, [this](const QPointF& point) {
            Measure(point);
        });

        ui.measureBtn->setEnabled(true);
    }

    auto SetupAutofocusPage::Impl::StartMeasure() -> void {
        measure.flag = 0;
        if(slopSeries) {
            chart->removeSeries(slopSeries);
            slopSeries = nullptr;
        }
    }

    auto SetupAutofocusPage::Impl::Measure(const QPointF& point) -> void {
        if(measure.flag == 0) {
            measure.first = point;
            measure.flag = 1;
        } else if(measure.flag == 1) {
            measure.second = point;
            measure.flag = 2;

            slopSeries = new QLineSeries();
            slopSeries->append(measure.first.x(), measure.first.y());
            slopSeries->append(measure.second.x(), measure.second.y());
            chart->addSeries(slopSeries);

            slopSeries->attachAxis(chart->axes(Qt::Horizontal).at(0));
            slopSeries->attachAxis(chart->axes(Qt::Vertical).at(0));

            auto dx = std::abs(measure.second.x() - measure.first.x());
            auto dy = (measure.first.x() < measure.second.x()) ? (measure.second.y() - measure.first.y()) : (measure.first.y() - measure.second.y());

            auto slope = control.CalcSlope(dy, dx);
            ui.slope->setValue(slope);
        }
    }

    auto SetupAutofocusPage::Impl::Clear() -> void {
        ui.measureBtn->setDisabled(true);
        ui.slope->setValue(0);
        ui.minValue->setValue(0);
        ui.maxValue->setValue(0);
        chart->removeAllSeries();
        dataSeries = nullptr;
        slopSeries = nullptr;

        auto [minVal, maxVal, divVal] = control.GetParameters();
        ui.paramMinSensor->setValue(minVal);
        ui.paramMaxSensor->setValue(maxVal);
        ui.paramDiv->setValue(divVal);
        ui.paramDefaultSensor->clear();

        ui.savedLabel->hide();
    }

    auto SetupAutofocusPage::Impl::ReadSensorValue() -> void {
        ui.sensorValue->setValue(control.ReadSensorValue());
    }

    auto SetupAutofocusPage::Impl::SaveParameter() -> void {
        if(ui.paramDefaultSensor->text().isEmpty()) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("Enter default sensor value"));
            return;
        }

        if(!control.SaveParameter(ui.paramMinSensor->value(),
                                  ui.paramMaxSensor->value(),
                                  ui.paramDiv->value(),
                                  ui.paramDefaultSensor->text().toInt())) {
            TC::MessageDialog::warning(p, tr("Setup"), tr("It fails to save auto focus parameter"));
            return;
        }

        ui.savedLabel->show();
        QTimer::singleShot(2000, [this]() { ui.savedLabel->hide(); });
    }

    SetupAutofocusPage::SetupAutofocusPage(QWidget* parent) : QWidget(parent), d{ new Impl(this) } {
        d->ui.setupUi(this);
        d->InitUi();

        connect(d->ui.scanBtn, &QPushButton::clicked, this, [this]() {
            d->Scan();
        });

        connect(d->ui.measureBtn, &QPushButton::clicked, this, [this]() {
            d->StartMeasure();
        });

        connect(d->ui.readSensorBtn, &QPushButton::clicked, this, [this]() {
            d->ReadSensorValue();
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this]() {
            d->SaveParameter();
        });

        d->ui.widget->setObjectName("panel");
        d->ui.botWidget->setObjectName("panel");

        for (const auto& label : findChildren<QLabel*>()) {
            if(label->objectName().contains("subtitle")) {
                label->setObjectName("label-h5");
            } else if(label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
        }

        for (const auto& button : findChildren<QPushButton*>()) {
            if(button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            } else {
                button->setObjectName("bt-maintenance-light");
            }
        }

        for (const auto& groupBox : findChildren<QGroupBox*>()) {
            groupBox->setObjectName("gb-maintenance-main");
        }
    }

    SetupAutofocusPage::~SetupAutofocusPage() {
    }

    auto SetupAutofocusPage::Enter() -> void {
        d->Clear();
    }

    auto SetupAutofocusPage::Leave() -> void {
    }

    void SetupAutofocusPage::resizeEvent(QResizeEvent* event) {
        QWidget::resizeEvent(event);
    }
}