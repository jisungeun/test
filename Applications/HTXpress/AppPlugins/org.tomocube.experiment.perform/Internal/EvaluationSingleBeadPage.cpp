#include <QFileDialog>
#include <QTimer>

#include <MessageDialog.h>

#include "LiveviewAnnotationBox.h"
#include "EvaluationConfig.h"
#include "AcquisitionDataObserver.h"
#include "EvaluationBeadObserver.h"
#include "EvaluationReportWriter.h"
#include "EvaluationSingleBeadPageControl.h"
#include "EvaluationSingleBeadPage.h"
#include "ui_EvaluationSingleBeadPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct EvaluationSingleBeadPage::Impl {
        EvaluationSingleBeadPageControl control;
        Ui::EvaluationSingleBeadPage ui;
        EvaluationSingleBeadPage* p{ nullptr };
        LiveviewAnnotationBox* annotation{ nullptr };

        EvaluationBeadObserver* evalObserver{ new EvaluationBeadObserver(p) };
        AcquisitionDataObserver* dataObserver{ new AcquisitionDataObserver(p) };

        Impl(EvaluationSingleBeadPage* p) : p{ p } {}

        auto InitUi()->void;
        auto Clear()->void;
        auto Acquire()->void;
        auto AcquireCompleted()->void;
        auto AcquisitionFailed(const QString& error)->void;
        auto Evaluate()->void;
        auto EvaluateCompleted()->void;
        auto Save()->void;
    };

    auto EvaluationSingleBeadPage::Impl::InitUi() -> void {
        ui.setupUi(p);

        ui.acquireProgressBar->hide();
        ui.evaluateBtn->setDisabled(true);
        ui.evalProgressBar->hide();
        ui.evalProgressBar->setFormat("Evaluation-%p%");
        ui.saveBtn->setDisabled(true);
        ui.savedLabel->hide();
        ui.averageValue->setValue(0);
        ui.resultLabel->hide();

        ui.resultTable->setColumnCount(6);
        ui.resultTable->setHorizontalHeaderLabels({"Well", "Point", "Volume", "Dry Mass", "Mean dRI", "Correlation"});
        ui.resultTable->horizontalHeader()->setStretchLastSection(true);
        ui.resultTable->setColumnWidth(1, 50);
        ui.resultTable->setColumnWidth(2, 50);

        ui.refLabel->setText(QString("(Threshold: %1)").arg(control.GetScoreThreshold(), 0, 'f', 3));

        for (const auto& label : p->findChildren<QLabel*>()) {
            if (label->objectName().contains("subtitle")) {
                label->setObjectName("label-h5");
            }
            else if (label->objectName().contains("title")) {
                label->setObjectName("label-h3");
            }
            else if(label->objectName().contains("ref")) {
                label->setObjectName("label-h6");
            }
        }

        for (const auto& button : p->findChildren<QPushButton*>()) {
            if (button->isCheckable()) {
                button->setObjectName("bt-maintenance-toggle");
            }
            else {
                button->setObjectName("bt-maintenance-light");
            }
        }

        for (const auto& table : p->findChildren<QTableWidget*>()) {
            table->setStyleSheet(QString("QTableWidget{border-bottom: 0px;}"));
            table->verticalHeader()->setStyleSheet("QHeaderView{border-bottom: 0px;}");
            table->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);
        }

        ui.leftWidget->setObjectName("panel");
        ui.rightWidget->setObjectName("panel");
        ui.botWidget->setObjectName("panel");
    }

    auto EvaluationSingleBeadPage::Impl::Clear() -> void {
        control.Clear();

        ui.acquireProgressBar->hide();
        ui.acquireProgressBar->setTextVisible(false);
        ui.evaluateBtn->setDisabled(true);
        ui.evalProgressBar->hide();
        ui.evalProgressBar->setTextVisible(false);
        ui.saveBtn->setDisabled(true);
        ui.savedLabel->hide();
        ui.averageValue->setValue(0);
        ui.resultLabel->hide();
        ui.resultTable->clearContents();

        ui.acquireProgressBar->setValue(0);
        ui.evalProgressBar->setValue(0);

        evalObserver->disconnect(p);
        dataObserver->disconnect(p);
    }

    auto EvaluationSingleBeadPage::Impl::Acquire() -> void {
        Clear();

        ui.acquireProgressBar->setValue(0);
        ui.acquireProgressBar->show();

        connect(evalObserver, &EvaluationBeadObserver::sigUpdateAcquisitionProgress, p, [this](double progress) {
            ui.acquireProgressBar->setValue(progress*100);
            if(progress == 1.0) AcquireCompleted();
        });

        connect(evalObserver, &EvaluationBeadObserver::sigNotifyAcquisitionError, p, [this](const QString& error) {
            AcquisitionFailed(error);
        });

        connect(evalObserver, &EvaluationBeadObserver::sigUpdateEvaluationProgress, p, [this](double progress) {
            ui.evalProgressBar->setValue(progress*100);
        });

        connect(dataObserver, &AcquisitionDataObserver::sigDataAdded, p, 
                [this](const QString& fileFullPath) {
            if(!control.AddDataPath(fileFullPath)) {
                TC::MessageDialog::warning(p, tr("Evaluation"), tr("Evaluation data may not be stored correctly"));
            }
        });

        ui.acquireProgressBar->setFormat("Calibration - 1/2");
        ui.acquireProgressBar->setTextVisible(true);
        if(!control.RunCAF()) {
            TC::MessageDialog::warning(p, tr("Evaluation"), tr("Evaluation is stopped because of calibration failed (1/2)"));
            Clear();
            return;
        }

        ui.acquireProgressBar->setValue(50);
        ui.acquireProgressBar->setFormat("Calibration - 2/2");
        if(!control.RunHTIllumCalibration()) {
            TC::MessageDialog::warning(p, tr("Evaluation"), tr("Evaluation is stopped because of calibration failed (2/2)"));
            Clear();
            return;
        }

        ui.acquireProgressBar->setValue(0);
        ui.acquireProgressBar->setFormat("Acquisition - %p%");
        if(!control.AcquireData()) {
            Clear();
        }
    }

    auto EvaluationSingleBeadPage::Impl::AcquireCompleted() -> void {
        ui.acquireProgressBar->hide();
        ui.evaluateBtn->setEnabled(true);
    }

    auto EvaluationSingleBeadPage::Impl::AcquisitionFailed(const QString& error) -> void {
        Clear();
        TC::MessageDialog::warning(p, tr("Evaluation"), error);
    }

    auto EvaluationSingleBeadPage::Impl::Evaluate() -> void {
        ui.evalProgressBar->setValue(0);
        ui.evalProgressBar->setTextVisible(true);
        ui.evalProgressBar->show();
        control.StartEvaluation();
        EvaluateCompleted();
    }

    auto EvaluationSingleBeadPage::Impl::EvaluateCompleted() -> void {
        ui.evalProgressBar->hide();

        auto [averageValue, result] = control.EvaluationResult();
        ui.averageValue->setValue(averageValue);

        if(result) {
            ui.resultLabel->setText("Pass");
            ui.resultLabel->setStyleSheet("QLabel {color:green; font: bold 14px;}");
            ui.resultLabel->show();
        } else {
            ui.resultLabel->setText("Fail");
            ui.resultLabel->setStyleSheet("QLabel {color:red;  font: bold 14px;}");
            ui.resultLabel->show();
        }

        const auto rowCount = control.GetDataCount();
        ui.resultTable->setRowCount(rowCount);
        for(auto rowIdx=0; rowIdx<rowCount; rowIdx++) {
            auto [well, point] = control.GetDataTitle(rowIdx);
            auto score = control.GetScore(rowIdx);

            auto item = new QTableWidgetItem(well);
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 0, item);

            item = new QTableWidgetItem(point);
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 1, item);

            item = new QTableWidgetItem(QString::number(score.GetVolume(), 'f', 2));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 2, item);

            item = new QTableWidgetItem(QString::number(score.GetDrymass(), 'f', 3));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 3, item);

            item = new QTableWidgetItem(QString::number(score.GetMeanDeltaRI(), 'f', 3));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 4, item);

            item = new QTableWidgetItem(QString::number(score.GetCorrelation(), 'f', 3));
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            ui.resultTable->setItem(rowIdx, 5, item);
        }

        ui.saveBtn->setEnabled(true);
    }

    auto EvaluationSingleBeadPage::Impl::Save() -> void {
        const auto path = EvaluationConfig::GetInstance()->GetReportFolder();
        if(!control.Save(path)) {
            TC::MessageDialog::warning(p, tr("Evaluation"), tr("Failed to save the single bead evaluation report"));
            return;
        }

        ui.savedLabel->show();
        QTimer::singleShot(2000, [this](){ ui.savedLabel->hide(); });
    }

    EvaluationSingleBeadPage::EvaluationSingleBeadPage(QWidget* parent)
        : QWidget(parent)
        , EvaluationPage()
        , d{ std::make_unique<Impl>(this) } {
        d->InitUi();

        auto cropSize = EvaluationConfig::GetInstance()->GetBeadCropSizeInPixels();
        d->annotation = new LiveviewAnnotationBox();
        d->annotation->SetRect(-cropSize/2, -cropSize/2, cropSize, cropSize);

        connect(d->ui.acquireBtn, &QPushButton::clicked, this, [this]() {
            d->Acquire();
        });

        connect(d->ui.evaluateBtn, &QPushButton::clicked, this, [this]() {
            d->Evaluate();
        });

        connect(d->ui.saveBtn, &QPushButton::clicked, this, [this]() {
            d->Save();
        });
    }

    EvaluationSingleBeadPage::~EvaluationSingleBeadPage() {
    }

    auto EvaluationSingleBeadPage::Enter() -> void {
        d->Clear();
        d->annotation->Install(0, 0);
    }

    auto EvaluationSingleBeadPage::Leave() -> void {
        d->Clear();
        d->annotation->Unintall();
    }

    void EvaluationSingleBeadPage::resizeEvent(QResizeEvent* event) {
        QWidget::resizeEvent(event);
    }
}
