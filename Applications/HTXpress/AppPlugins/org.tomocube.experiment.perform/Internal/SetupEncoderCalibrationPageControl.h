#pragma once
#include <memory>
#include <QList>
#include <QImage>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SetupEncoderCalibrationPageControl {
    public:
        enum Position {
            Center = -1,
            P1 = 0,
            P2 = 1,
            P3 = 2,
            P4 = 3,
        };

    public:
        SetupEncoderCalibrationPageControl();
        ~SetupEncoderCalibrationPageControl();

        auto StoreOriginal()->void;
        auto RestoreOriginal()->void;

        auto Clear()->void;
        auto GetCompensation() const->std::tuple<double,double>;
        auto GetLatestImage(QImage& image)->bool;
        auto DisableAutofocus()->bool;
        auto LowerZStage(double distInUm)->bool;
        auto MovePoint(Position index)->bool;
        auto CheckMove(Position index)->bool;
        auto FindCenter(Position index, QImage& image) const->std::tuple<int32_t,int32_t>;
        auto IsAllFound() const->bool;
        auto Compensate()->bool;
        auto Save()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}