#pragma once
#include <memory>
#include <QWidget>

#include "EvaluationPage.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class EvaluationSetupPage : public QWidget, public EvaluationPage {
        Q_OBJECT

    public:
        EvaluationSetupPage(QWidget* parent = nullptr);
        ~EvaluationSetupPage() override;

        auto Enter() -> void override;
        auto Leave() -> void override;

    signals:
        void sigReady();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}