#define LOGGER_TAG "[PreviewViewer]"
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>

#include <QDir>
#include <QPixmap>
#include <QFileInfo>
#include <QMutex>

#include <TCLogger.h>
#include <System.h>
#include <SystemStatus.h>

#include <PreviewAcquisitionController.h>
#include <MotionController.h>
#include <ImagingPlanController.h>
#include <VesselController.h>

#include "Utility.h"
#include "PreviewUpdater.h"
#include "MotionUpdater.h"
#include "ImagingPlanUpdater.h"
#include "PreviewViewerControl.h"



namespace HTXpress::AppPlugins::Experiment::Perform {
    struct PreviewViewerControl::Impl {
        AppEntity::Position centerInWell;

        struct {
            int32_t xInPixels{ 0 };
            int32_t yInPixels{ 0 };
        } fov;

        struct {
            int32_t xInPixels{ 0 };
            int32_t yInPixels{ 0 };
        } position;

        struct {
            double wellX{}; // mm
            double wellY{}; // mm
            int32_t widthInUM{}; // um
            int32_t heightInUM{};// um
        } customPreviewArea;

        struct {
            int32_t xInPixels;
            int32_t yInPixels;
            double xInUm;
            double yInUm;
        } imageSize;

        struct {
            QMutex mutex;
            bool running{ false };
        } status;

        auto GetCurrentWell()->AppEntity::WellIndex;
    };

    auto PreviewViewerControl::Impl::GetCurrentWell() -> AppEntity::WellIndex {
        return AppEntity::SystemStatus::GetInstance()->GetCurrentWell();
    }

    PreviewViewerControl::PreviewViewerControl() : d{new Impl} {
    }

    PreviewViewerControl::~PreviewViewerControl() {
    }

    auto PreviewViewerControl::GetDefaultROI() const -> std::tuple<int32_t, int32_t> {
        //TODO Get Default ROI from system configuration
        return std::make_tuple(600, 600);
    }

    auto PreviewViewerControl::SetCustomPreviewArea(const double& wellXinMM, const double& wellYinMM, const int32_t& widthInUM, const int32_t& heightInUM) -> void {
        d->customPreviewArea = {wellXinMM, wellYinMM, widthInUM, heightInUM};
    }

    auto PreviewViewerControl::GetCustomPreviewSizeInUM() const -> std::tuple<int32_t, int32_t> {
        return {d->customPreviewArea.widthInUM, d->customPreviewArea.heightInUM};
    }

    auto PreviewViewerControl::CapturePreview(int32_t width, int32_t height) -> bool {
        AppEntity::Position currentPosition; 

        auto motionController = Interactor::MotionController(nullptr);
        if(!motionController.GetPosition(d->GetCurrentWell(), currentPosition)) {
            return false;
        }

        d->status.mutex.lock();
        d->status.running = true;
        d->status.mutex.unlock();

        auto presenter = Interactor::PreviewPresenter(PreviewUpdater::GetInstance().get());
        auto controller = Interactor::PreviewAcquisitionController(&presenter);
        if(!controller.CaptureArea(width, height, [this]()->bool {
            QMutexLocker locker(&d->status.mutex);
            return !d->status.running;
        })) {
            return false;
        }

        d->centerInWell = currentPosition;

        return true;
    }

    auto PreviewViewerControl::StopCapture() -> void {
        QMutexLocker locker(&d->status.mutex);
        d->status.running = false;
    }

    auto PreviewViewerControl::MoveRelativeLive(int32_t relPosX, int32_t relPosY) -> bool {
        const auto relPos = AppEntity::Position::fromUM(Utility::pixel2um(relPosX),
                                                        Utility::pixel2um(relPosY),
                                                        0);
        const auto targetPos = d->centerInWell + relPos;

        auto presenter = Interactor::MotionPresenter(MotionUpdater::GetInstance().get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveXY(d->GetCurrentWell(), targetPos.toMM().x, targetPos.toMM().y);
    }

    auto PreviewViewerControl::ChangeTileImagingArea(int32_t xInPixel, int32_t yInPixel, 
                                                     int32_t widthInPixel, int32_t heightInPixel) -> bool {
        const auto relPos = AppEntity::Position::fromUM(Utility::pixel2um(xInPixel),
                                                        Utility::pixel2um(yInPixel),
                                                        0);
        const auto targetPos = d->centerInWell + relPos;
        const auto xInMm = targetPos.toMM().x;
        const auto yInMm = targetPos.toMM().y;
        const auto wellIndex = AppEntity::SystemStatus::GetInstance()->GetCurrentWell();
        const auto widthInUm = Utility::pixel2um(widthInPixel);
        const auto heightInUm = Utility::pixel2um(heightInPixel);

        auto updater = ImagingPlanUpdater::GetInstance();
        auto presenter = Interactor::ImagingPlanPresenter(updater.get());
        auto controller = Interactor::ImagingPlanController(false, &presenter);

        return controller.SetTileScanArea(true, wellIndex, xInMm, yInMm, widthInUm, heightInUm);
    }

    auto PreviewViewerControl::DoPreviewAreaSetting() -> void {
        auto updater = PreviewUpdater::GetInstance();
        auto presenter = Interactor::PreviewPresenter(updater.get());
        auto controller = Interactor::PreviewAcquisitionController(&presenter);

        controller.DoCustomPreviewAreaSetting();
    }

    auto PreviewViewerControl::CancelPreviewAreaSetting() -> void {
        auto updater = PreviewUpdater::GetInstance();
        auto presenter = Interactor::PreviewPresenter(updater.get());
        auto controller = Interactor::PreviewAcquisitionController(&presenter);

        controller.CancelCustomPreviewAreaSetting();
    }

    auto PreviewViewerControl::CurrentWell() const -> AppEntity::WellIndex {
        return d->GetCurrentWell();
    }

    auto PreviewViewerControl::CurrentCenterPosition() const -> AppEntity::Position {
        return d->centerInWell;
    }

    auto PreviewViewerControl::GetOffsetInPixels(const AppEntity::WellIndex wellIdx,
                                                 const AppEntity::Position& position, 
                                                 int32_t& posX, int32_t& posY) -> bool {
        if(wellIdx != d->GetCurrentWell()) return false;
        auto deltaPos = position - d->centerInWell;
        posX = Utility::um2pixel(deltaPos.toUM().x);
        posY = Utility::um2pixel(deltaPos.toUM().y);

        return true;
    }

    auto PreviewViewerControl::SetFOVInPixels(const int32_t xInPixels, const int32_t yInPixels) -> void {
        d->fov.xInPixels = xInPixels;
        d->fov.yInPixels = yInPixels;
    }

    auto PreviewViewerControl::GetFOVInPixels() const -> std::tuple<int32_t, int32_t> {
        return std::make_tuple(d->fov.xInPixels, d->fov.yInPixels);
    }

    auto PreviewViewerControl::SetPositionInPixels(const int32_t posX, const int32_t posY) -> void {
        d->position.xInPixels = posX;
        d->position.yInPixels = posY;
    }

    auto PreviewViewerControl::GetPositionInPixels() const -> std::tuple<int32_t, int32_t> {
        return std::make_tuple(d->position.xInPixels, d->position.yInPixels);
    }

    auto PreviewViewerControl::MoveLensPosition() -> void {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::VesselController(&presenter);

        controller.SetSampleStagePosition(d->GetCurrentWell(),
                                          d->customPreviewArea.wellX,
                                          d->customPreviewArea.wellY);
    }

    auto PreviewViewerControl::UpdateImageSize(int32_t xPixels, int32_t yPixels) -> void {
        auto resolution = AppEntity::System::GetModel()->CameraPixelSize();
        d->imageSize.xInPixels = xPixels;
        d->imageSize.yInPixels = yPixels;
        d->imageSize.xInUm = xPixels * resolution;
        d->imageSize.yInUm = xPixels * resolution;
    }

    auto PreviewViewerControl::GetDefaultPath() const -> QString {
        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto outPath = sysStatus->GetExperimentOutputPath();
        return QString("%1/preview").arg(outPath);
    }

    auto PreviewViewerControl::GetDefaultName(const QString& dirPath) const -> QString {
        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto experiment = sysStatus->GetExperiment();
        const auto vesselIndex = sysStatus->GetCurrentVesselIndex();
        const auto wellName = experiment->GetWellName(vesselIndex, sysStatus->GetCurrentWell());
        const auto posUM = d->centerInWell.toUM();

        const auto index = [=]()->int32_t {
            QDir dir(dirPath);
            const auto filter = QString("V%1_W%2_I*.png").arg(vesselIndex, 1, 10, QLatin1Char('0')).arg(wellName);
            return dir.entryList({filter}, QDir::Filter::Files).length();
        }();

        return QString("V%1_W%2_I%3_X%4_Y%5_W%6_H%7.png")
                       .arg(vesselIndex, 1, 10, QLatin1Char('0'))
                       .arg(wellName)
                       .arg(index+1)
                       .arg(static_cast<int32_t>(posUM.x))
                       .arg(static_cast<int32_t>(posUM.y))
                       .arg(static_cast<int32_t>(d->imageSize.xInUm))
                       .arg(static_cast<int32_t>(d->imageSize.yInUm));
    }

    auto PreviewViewerControl::StorePreview(const QString& path, const QPixmap& pixmap) -> void {
        QFileInfo info(path);
        if(!info.absoluteDir().exists()) QDir().mkpath(info.absolutePath());

        const QImage image = pixmap.toImage().convertToFormat(QImage::Format_Grayscale8);
        cv::Mat cvImage(image.height(), image.width(), CV_8UC1, (uchar*)image.bits(), image.bytesPerLine());
        cv::imwrite(path.toLocal8Bit().constData(), cvImage);
    }
}
