#pragma once
#include <memory>
#include <IAcqPositionView.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class AcquisitionPositionObserver;

    class AcquisitionPositionUpdater : public Interactor::IAcqPositionView {
    public:
        using Pointer = std::shared_ptr<AcquisitionPositionUpdater>;

    protected:
        AcquisitionPositionUpdater();

    public:
        ~AcquisitionPositionUpdater();

        static auto GetInstance()->Pointer;

        auto AddPosition(LocationIndex locationIdx, WellIndex wellIndex, Location::Pointer loc) -> void override;
        auto RefreshList(WellIndex wellIndex, QMap<LocationIndex, Location::Pointer>& locList) -> void override;
        auto DeletePosition(WellIndex wellIdx, LocationIndex locationIdx) -> void override;

    protected:
        auto Register(AcquisitionPositionObserver* observer)->void;
        auto Deregister(AcquisitionPositionObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class AcquisitionPositionObserver;
    };
}