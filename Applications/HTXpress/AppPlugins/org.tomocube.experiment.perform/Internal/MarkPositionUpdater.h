﻿#pragma once

#include <memory>

#include <IMarkPositionView.h>

#include "MarkPositionObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class MarkPositionUpdater : public Interactor::IMarkPositionView {
    public:
        using Pointer = std::shared_ptr<MarkPositionUpdater>;

    protected:
        MarkPositionUpdater();

    public:
        ~MarkPositionUpdater() override;

        static auto GetInstance() -> Pointer;

        auto AddMarkPosition(AppEntity::WellIndex wellIndex, AppEntity::Location::Pointer location) -> void override;

    protected:
        auto Register(MarkPositionObserver* observer) -> void;
        auto Deregister(MarkPositionObserver* observer) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class MarkPositionObserver;
    };
}
