﻿#pragma once
#include <memory>

#include <ISystemStorageView.h>

#include "SystemStorageObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SystemStorageUpdater final : public Interactor::ISystemStorageView {
    public:
        using Self = SystemStorageUpdater;
        using Pointer = std::shared_ptr<Self>;

        ~SystemStorageUpdater() override;
        static auto GetInstance() -> Pointer;

        auto UpdateSystemStorageSpace(const int64_t& totalBytes, const int64_t& availableBytes) -> void override;
        auto UpdateMinRequiredSpace(const int32_t& minRequiredGigabytes) -> void override;

    protected:
        SystemStorageUpdater();

        auto Register(SystemStorageObserver* observer) -> void;
        auto Deregister(SystemStorageObserver* observer) -> void;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class SystemStorageObserver;
    };
}
