#pragma once
#include <memory>
#include <QList>
#include <QImage>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class SetupIntensityPageControl {
    public:
        SetupIntensityPageControl();
        ~SetupIntensityPageControl();

        auto GetNAs() const->QList<double>;
        auto ChangeBFIntensity(int32_t value)->bool;
        auto ChangeBFExposure(int32_t value)->bool;
        auto SaveBFLEDIntensity(int32_t intensity)->bool;
        auto RunCAF(double NA) const->bool;
        auto ShowHTIlluminationSetupPattern(double NA, int32_t intensity) const->bool;
        auto GetLatestImage(QImage& image)->bool;
        auto SaveHTLEDIntensity(double NA, int32_t intensity)->bool;

        auto CalculateAverage(QImage& image)->double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}