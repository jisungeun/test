#include <QList>
#include "AcquisitionImageObserver.h"
#include "AcquisitionImageUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    struct AcquisitionImageUpdater::Impl {
        QList<AcquisitionImageObserver*> observers;
    };

    AcquisitionImageUpdater::AcquisitionImageUpdater() : IAcquireImageView(), d{new Impl} {
    }

    AcquisitionImageUpdater::~AcquisitionImageUpdater() {
    }

    auto AcquisitionImageUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new AcquisitionImageUpdater() };
        return theInstance;
    }

    auto AcquisitionImageUpdater::UpdateProgress(double progress) -> void {
        for(auto observer : d->observers) {
            observer->UpdateProgress(progress);
        }
    }

    auto AcquisitionImageUpdater::Register(AcquisitionImageObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto AcquisitionImageUpdater::Deregister(AcquisitionImageObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}