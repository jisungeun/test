#pragma once
#include <memory>
#include <QMap>

#include <enum.h>

#include <Vessel.h>
#include <Area.h>
#include <PositionGroup.h>
#include <Location.h>
#include <ROI.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentRunner {
    BETTER_ENUM(AcquireType, int32_t, Single, Timelapse, OneCycle);

    class Planner {
    public:
        using WellIndex = AppEntity::WellIndex;
        using Location = AppEntity::Location;
        using LocationIndex = AppEntity::LocationIndex;

    public:
        Planner();
        ~Planner();

        auto SetSystemCenter(const AppEntity::Position& position)->void;
        auto SetVessel(AppEntity::Vessel::Pointer vessel)->void;
        auto SetExperimentOutputPath(const QString& expPath)->void;
        auto SetTitle(const QString& title)->void;
        auto SetWellGroupNames(const QMap<WellIndex, QString>& groupNames)->void;
        auto SetWellNames(const QMap<WellIndex, QString>& wellNames)->void;
        auto SetMaximumFOV(const AppEntity::Area& area)->void;
        auto SetOverlapInUM(uint32_t overlap)->void;
        auto SetLocations(const QMap<WellIndex,QMap<LocationIndex,Location>>& locations)->void;
        auto SetAcquireType(const AcquireType& type)->void;
        void SetRoiAreas(const QMap<AppEntity::WellIndex, QMap<AppEntity::ROIIndex, std::shared_ptr<AppEntity::ROI>>>& roiAreas);

        auto Perform() const->QList<AppEntity::PositionGroup>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
