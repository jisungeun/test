﻿#pragma once
#include <memory>
#include <QString>

#include <PositionGroup.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentRunner {
    class ExperimentProfileCopier {
    public:
        ExperimentProfileCopier();
        ~ExperimentProfileCopier();

        auto SetSampleTypeName(const QString& sampleTypeName)->void;
        auto SetExperimentPath(const QString& experimentPath) -> void;
        auto SetPositions(const QList<AppEntity::PositionGroup>& positions) -> void;

        auto Perform() const -> QStringList;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
