#define LOGGER_TAG "[ExperimentRunner]"

#include <QMutex>
#include <QElapsedTimer>
#include <QDateTime>
#include <QDir>
#include <QStandardPaths>
#include <QCoreApplication>

#include <TCLogger.h>

#include <System.h>
#include <SessionManager.h>
#include <SystemStatus.h>
#include <IInstrument.h>
#include <IImagingSystem.h>
#include <IScanTimeCalculator.h>
#include <IExperimentWriter.h>

#include "Task.h"
#include "ConfigWriter.h"
#include "CilsMetafileWriter.h"
#include "BackgroundCopier.h"
#include "Planner.h"
#include "ExperimentRunner.h"
#include "VesselWriterPlugin.h"
#include "ExperimentProfileCopier.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentRunner {
    class BusyStatusUpdater {
    public:
        BusyStatusUpdater() {}
        ~BusyStatusUpdater() {
            AppEntity::SystemStatus::GetInstance()->SetBusy(false);
        }
    };

    struct Runner::Impl {
        QString projectTitle;
        AppEntity::Experiment::Pointer experiment;
        AppEntity::VesselIndex vesselIndex{ 0 };
        uint32_t overlapInUM{ 0 };
        bool isMultiDishHolder{ false };
        UseCase::IRunExperimentOutputPort* output{ nullptr };
        Runner* p{ nullptr };
        HTXpress::AppPlugins::Experiment::Perform::UseCase::AcquisitionType::_enumerated acquisitionType;
        bool running{ true };
        QMutex runMutex;

        struct {
            bool fail{ false };
            bool stopped{ false };
            bool completed{ false };
            bool error{ false };
            QString errorMessage;
            QString statusMessage;
            int32_t totalTasks{ 1 };
            int32_t currentTask{ 0 };
            int32_t totalSeconds{ 0 };
            double sequenceProgress{ 0 };
            AppEntity::Position currentPosition;
            int32_t elapsedSeconds{ 0 };

            auto Init() {
                fail = false;
                stopped = false;
                completed = false;
                error = false;
                errorMessage.clear();
                statusMessage.clear();
                totalTasks = 1;
                currentTask = 0;
                sequenceProgress = 0;
                elapsedSeconds = 0;
            }
        } status;

        Impl(Runner* parent) : p{ parent } {}

        auto BackupExperiment()->void;
        auto GenerateTitle(const QString& targetDir)->QString;
        auto ConvertFOV(const AppEntity::Area fov)->std::tuple<int32_t, int32_t>;
        auto Build()->QList<QList<Task::Pointer>>;
        auto UpdateProgress()->void;
        auto UpdateTimeTable(UseCase::ImagingTimeTable::Pointer timeTable, 
                             const AppEntity::ImagingScenario::Pointer scenario, 
                             const QList<AppEntity::PositionGroup>& positions)->void;
        auto EstimateTotalTime(UseCase::ImagingTimeTable::Pointer timeTable, int32_t positionCount)->void;
        auto ConvertToLocalAcquireType(const UseCase::AcquisitionType& acquireType) const -> AcquireType;
        auto FocusReadyMM() const->double;
    };

    auto Runner::Impl::BackupExperiment() -> void {
        static auto topPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
        static int32_t index{ 0 };

        auto tempPath = QString("%1/log/experiment.tcxexp.%2.bak").arg(topPath).arg(index++%10);
        auto* writer = UseCase::IExperimentWriter::GetInstance();
        writer->Write(experiment, tempPath);
    }

    auto Runner::Impl::GenerateTitle(const QString& targetDir) -> QString {
        const auto timestamp = QDateTime::currentDateTime().toString("yyMMdd.hhmmss");
        const auto title = QString("%1.%2").arg(timestamp).arg(experiment->GetTitle());

        auto dir = QDir(targetDir);
        const auto filter = {QString("*%1*").arg(experiment->GetTitle())};
        const auto count = dir.entryList(filter, QDir::Filter::Dirs).size();

        return QString("%1.%2").arg(title).arg(count+1, 3, 10, QLatin1Char('0'));
    }

    auto Runner::Impl::ConvertFOV(const AppEntity::Area fov) -> std::tuple<int32_t, int32_t> {
        const auto xInUm = fov.toUM().width;
        const auto yInUm = fov.toUM().height;
        const auto model = AppEntity::System::GetModel();
        const auto pixelSize = model->CameraPixelSize() / model->ObjectiveLensMagnification();
        return std::make_tuple(std::round(xInUm/pixelSize), std::round(yInUm/pixelSize));
    }

    auto Runner::Impl::Build() -> QList<QList<Task::Pointer>> {
        QList<QList<Task::Pointer>> taskGroups;

        const auto scenario = experiment->GetScenario(vesselIndex);
        const auto sequences = scenario->GetCount();

        status.totalTasks = 0;

        for(auto seqIdx=0; seqIdx<sequences; seqIdx++) {
            const auto sequence = scenario->GetSequence(seqIdx);
            const auto startTime = scenario->GetStartTime(seqIdx);
            const auto timeInterval = sequence->GetInterval();
            const auto timeCount = sequence->GetTimeCount();

            QList<Task::Pointer> tasks;

            for(auto timeIndex=0; timeIndex<timeCount; timeIndex++) {
                auto task = std::make_shared<Task>(sequence, timeIndex, (startTime + (timeInterval*timeIndex))*1000);
                tasks.push_back(task);
                status.totalTasks++;
            }

            taskGroups.push_back(tasks);
        }

        QLOG_INFO() << "Sequences:" << sequences << " Tasks:" << status.totalTasks;

        return taskGroups;
    }

    auto Runner::Impl::UpdateProgress() -> void {
        if(!output) return;

        UseCase::ExperimentStatus expStatus;
        expStatus.SetRunningSequence(status.currentTask);
        if(status.completed) expStatus.SetProgress(1.0);
        else if(status.stopped) expStatus.SetStopped();
        else if(status.error) expStatus.SetError(status.errorMessage);
        else {
            const auto completedSequenceRatio = (status.currentTask*1.0)/status.totalTasks;
            const auto currentSequenceRatio = status.sequenceProgress/status.totalTasks;

            const auto progress = completedSequenceRatio+currentSequenceRatio;
            expStatus.SetProgress(std::min(0.99, progress));
        }
        expStatus.SetRemainTime(status.totalSeconds - status.elapsedSeconds);
        expStatus.SetElapsedTime(status.elapsedSeconds);
        expStatus.SetStatusMessage(status.statusMessage);

        const auto wellIndex = p->ConvertGlobal2WellIndex(status.currentPosition);
        const auto position = p->ConvertGlobal2Well(status.currentPosition, wellIndex);
        expStatus.SetCurrentPosition(wellIndex, position);

        output->UpdateProgress(expStatus);
    }

    auto Runner::Impl::UpdateTimeTable(UseCase::ImagingTimeTable::Pointer timeTable,
                                       const AppEntity::ImagingScenario::Pointer scenario, 
                                       const QList<AppEntity::PositionGroup>& positions) -> void {
        auto timeCalc = UseCase::IScanTimeCalculator::GetInstance();

        AppEntity::ImagingCondition::Pointer prevCond{ nullptr };
        const auto seqCount = scenario->GetCount();
        for(auto seqIdx=0; seqIdx<seqCount; seqIdx++) {
            const auto sequence = scenario->GetSequence(seqIdx);
            const auto conds = sequence->GetImagingCondition();
            for(auto cond : conds) {
                timeCalc->CalcInSec(cond, timeTable, prevCond);
                prevCond = cond;
            }
        }

        timeCalc->CalcInSec(positions, timeTable);
    }

    auto Runner::Impl::EstimateTotalTime(UseCase::ImagingTimeTable::Pointer timeTable, int32_t positionCount) -> void {
        const auto scenario = experiment->GetScenario(vesselIndex);
        const auto lastSeqIdx = scenario->GetCount() - 1;
        const auto lastScenarioStartTime = scenario->GetStartTime(lastSeqIdx);
        const auto lastSequence = scenario->GetSequence(lastSeqIdx);
        const auto lastInterval = lastSequence->GetInterval();
        const auto lastTimeCount = lastSequence->GetTimeCount();
        const auto lastStartTime = lastScenarioStartTime + lastInterval * (lastTimeCount - 1);

        double lastCycleImagingTime = 0;

        const auto conds = lastSequence->GetImagingCondition();
        for(auto cond : conds) {
            lastCycleImagingTime += timeTable->GetTimeForCondition(cond->GetModality(), cond->Is2D());
        }

        status.totalSeconds = lastStartTime + lastCycleImagingTime*positionCount + timeTable->GetTimeForCycle();
    }

    auto Runner::Impl::ConvertToLocalAcquireType(const UseCase::AcquisitionType& acquireType) const -> AcquireType {
        if(acquireType== +UseCase::AcquisitionType::Single) return AcquireType::Single;
        if(acquireType == +UseCase::AcquisitionType::Timelapse) return AcquireType::Timelapse;
        return AcquireType::OneCycle;
    }

    auto Runner::Impl::FocusReadyMM() const -> double {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto zReadyMM = sysConfig->GetAutofocusReadyPos();
        const auto afOffset = sysStatus->GetExperiment()->GetVessel()->GetAFOffset();
        return zReadyMM + afOffset/1000;
    }

    Runner::Runner() : d{new Impl(this)} {
    }

    Runner::~Runner() {
    }

    void Runner::SetOutputPort(UseCase::IRunExperimentOutputPort* output) {
        d->output = output;
    }

    auto Runner::SetExperiment(const QString& projectTitle, AppEntity::Experiment::Pointer experiment) -> void {
        d->projectTitle = projectTitle;
        d->experiment = experiment;
    }

    auto Runner::SetVesselIndex(AppEntity::VesselIndex index) -> void {
        d->vesselIndex = index;
    }

    auto Runner::SetOverlapInUM(const uint32_t overlapInUM) -> void {
        d->overlapInUM = overlapInUM;
    }

    auto Runner::SetAcquisitionType(const UseCase::AcquisitionType& type) -> void {
        d->acquisitionType = type;
    }

    auto Runner::SetMultiDishHolder(bool isMultiDishHolder) -> void {
        d->isMultiDishHolder = isMultiDishHolder;
    }

    auto Runner::Run() -> bool {
        AppEntity::SystemStatus::GetInstance()->SetBusy(true);

        QMutexLocker locker(&d->runMutex);
        d->running = true;
        d->status.Init();

        auto scenario = d->experiment->GetScenario(d->vesselIndex);
        d->status.currentTask = 0;

        d->BackupExperiment();

        start();

        return true;
    }

    auto Runner::Stop() -> bool {
        QMutexLocker locker(&d->runMutex);
        d->running = false;
        locker.unlock();

        int32_t index = 0;
        while(!wait(200)) {
            locker.relock();
            d->status.statusMessage = QString("Waiting for the experiment to be stopped. [%1]").arg(QString().fill('=', index++%20));
            d->UpdateProgress();
            QCoreApplication::processEvents(QEventLoop::ProcessEventsFlag::ExcludeUserInputEvents, 100);
            locker.unlock();
        }

        return true;
    }

    void Runner::run() {
        BusyStatusUpdater busyUpdater;

        QLOG_INFO() << "Experiment runner thread is started";

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto* instrument = UseCase::IInstrument::GetInstance();

        instrument->StopLive();
        const auto startPos = instrument->GetAxisPosition();

        auto scenario = d->experiment->GetScenario(d->vesselIndex);

        QLOG_INFO() << "Planning";
        Planner planner;
        planner.SetSystemCenter(AppEntity::System::GetSystemCenter());
        planner.SetVessel(d->experiment->GetVessel());
        planner.SetRoiAreas(d->experiment->GetROIs(d->vesselIndex));
        planner.SetExperimentOutputPath(sysStatus->GetExperimentOutputPath());
        planner.SetTitle(d->GenerateTitle(sysStatus->GetExperimentOutputPath()));
        planner.SetWellGroupNames(d->experiment->GetWellGroupNames(d->vesselIndex));
        planner.SetWellNames(d->experiment->GetWellPositionNames(d->vesselIndex));
        planner.SetMaximumFOV(d->experiment->GetFOV());
        planner.SetOverlapInUM(d->overlapInUM);
        planner.SetLocations(d->experiment->GetAllLocations(d->vesselIndex));
        planner.SetAcquireType(d->ConvertToLocalAcquireType(d->acquisitionType));

        const auto positions = planner.Perform();

        const auto positionCount = [=]()->int32_t {
            int32_t count = 0;
            for(const auto& group : positions) {
                count += group.GetCount();
            }
            return count;
        }();

        auto timeTable = std::make_shared<UseCase::ImagingTimeTable>();
        d->UpdateTimeTable(timeTable, scenario, positions);
        d->EstimateTotalTime(timeTable, positionCount);

        const auto cycleTime = timeTable->GetTimeForCycle();

        auto* imagingSystem = UseCase::IImagingSystem::GetInstance();

        imagingSystem->SetScenario(scenario, sysStatus->GetExperimentOutputPath());
        imagingSystem->SetPositions(positions);

        auto fov = d->ConvertFOV(d->experiment->GetFOV());
        int32_t fovOffsetX{0};
        int32_t fovOffsetY{0};
        instrument->ChangeFOV(std::get<0>(fov), std::get<1>(fov), fovOffsetX, fovOffsetY);
        instrument->InstallImagePort(imagingSystem->GetImagePort());

        auto positionsWithWellCoord = [=]()->QList<AppEntity::PositionGroup> {
            QList<AppEntity::PositionGroup> outList;

            const auto sysCenter = AppEntity::System::GetSystemCenter();
            const auto vessel = d->experiment->GetVessel();

            for(auto input : positions) {
                auto wellIdx = input.GetWellIndex();
                auto wellOffset = vessel->GetWellPosition(wellIdx);

                auto output(input);
                output.SetCenter(input.GetCenter() - wellOffset - sysCenter);

                auto rows = input.GetRows();
                auto cols = input.GetCols();
                for(auto rowIdx=0u; rowIdx<rows; rowIdx++) {
                    for(auto colIdx=0u; colIdx<cols; colIdx++) {
                        output.SetPosition(rowIdx, colIdx, input.GetPosition(rowIdx, colIdx) - wellOffset - sysCenter);
                    }
                }

                outList.push_back(output);
            }

            return outList;
        }();

        ConfigWriter writer;
        writer.SetTitle(d->GenerateTitle(sysStatus->GetExperimentOutputPath()));
        writer.SetOutputPath(sysStatus->GetExperimentOutputPath());
        writer.SetExperiment(d->experiment, d->vesselIndex);
        writer.SetPositions(positionsWithWellCoord);
        writer.SetFOV(std::get<0>(fov), std::get<1>(fov), fovOffsetX, fovOffsetY);
        writer.Perform();

        CilsMetafileWriter cilsWriter;
        cilsWriter.SetOutputPath(sysStatus->GetExperimentOutputPath());
        cilsWriter.SetExperiment(sysStatus->GetProjectTitle(), d->experiment);
        cilsWriter.SetPositions(positionsWithWellCoord);
        cilsWriter.Perform();

        auto bgSource = AppEntity::System::GetBackgroundPath(d->experiment->GetVessel()->GetNA());
        if(QFile::exists(bgSource)) {
            BackgroundCopier bgCopier;
            bgCopier.SetSourcePath(bgSource);
            bgCopier.SetOutputPath(sysStatus->GetExperimentOutputPath());
            bgCopier.SetPositions(positions);
            bgCopier.Perform();
        }

        ExperimentProfileCopier prfCopier;
        prfCopier.SetSampleTypeName(d->experiment->GetSampleTypeName());
        prfCopier.SetExperimentPath(sysStatus->GetExperimentOutputPath());
        prfCopier.SetPositions(positions);
        const auto failedList = prfCopier.Perform();
        if(!failedList.isEmpty()) {
            QLOG_INFO() << "Failed to copy path list:" << failedList;
        }

        auto expWriter = UseCase::IExperimentWriter::GetInstance();
        for(const auto& position : positions) {
            const auto expFullPath = sysStatus->GetExperimentOutputPath()+QDir::separator()+position.GetTitle()+QDir::separator()+".experiment";
            expWriter->Write(d->experiment, expFullPath);
        }

        VesselWriter vesselWriter;
        vesselWriter.SetOutputPath(sysStatus->GetExperimentOutputPath());
        vesselWriter.SetVessel(d->experiment->GetVessel());
        vesselWriter.SetPositions(positions);
        vesselWriter.Perform();

        auto taskGroups = d->Build();

        int32_t seqIdx = 0;
        int32_t taskIdx = -1;
        QElapsedTimer globalTimer;
        for(const auto& tasks : taskGroups) {
            imagingSystem->StartNewAcquisition(seqIdx);
            if(seqIdx == 0) globalTimer.start();
                    
            for(const auto& task : tasks) {
                QElapsedTimer unitTimer;

                const auto taskStartTime = task->GetStartTimeMSec();
                const auto totalElapsed = globalTimer.elapsed();
                const auto overTime = (taskStartTime - totalElapsed) < 0;

                QLOG_INFO() << "[s:" << seqIdx << " t:" << taskIdx + 1 << "] start:" << taskStartTime/1000 << "sec "
                            << "elapsed:" << totalElapsed/1000 << "sec overtime:" << overTime;

                if(!overTime && ((taskIdx + 1)>0)) {
                    while(true) {
                        const auto remain = task->GetStartTimeMSec() - globalTimer.elapsed();
                        if(remain <= 0) break;
                        if(remain > 100) {
                            msleep(100);
                        } else {
                            msleep(remain);
                        }

                        const auto progress = instrument->CheckSequenceProgress();

                        QMutexLocker locker(&d->runMutex);
                        d->status.currentTask = taskIdx;
                        d->status.error = !std::get<0>(progress);
                        d->status.sequenceProgress = std::get<1>(progress);
                        d->status.currentPosition = std::get<2>(progress);
                        d->status.elapsedSeconds = globalTimer.elapsed() / 1000;
                        if(d->status.error) {
                            d->status.errorMessage = instrument->GetErrorMessage();
                        }

                        d->UpdateProgress();

                        if(!d->running) {
                            d->status.stopped = true;
                            QLOG_INFO() << "Stop is triggered";
                            break;
                        } else if(d->status.error) {
                            d->status.stopped = true;
                            QLOG_ERROR() << "Error is occurred during performing experiment";
                            break;
                        }
                    }

                    QMutexLocker locker(&d->runMutex);
                    if(d->status.stopped) break;
                }

                taskIdx += 1;

                const auto& sequence = task->GetImagingSequence();

                const auto imagingTime = timeTable->GetTimeForConditions(sequence->GetImagingCondition());
                int64_t timeoutInMSec = static_cast<int64_t>((cycleTime + imagingTime*positionCount)*1000);
                QLOG_INFO() << "[" << taskIdx+1 << "/" << d->status.totalTasks << "] "
                            << "Cycle time of sequence (" << taskIdx+1 << "/" << d->status.totalTasks << ") = "
                            << timeoutInMSec/1000.0 << " seconds";
                timeoutInMSec = std::max<int32_t>(10, timeoutInMSec);   // At least, 15 seconds...

                const auto curPos = instrument->GetAxisPosition();

                unitTimer.start();
                if(!instrument->RunImagingSequence(sequence, 
                                                   positions,
                                                   ConvertGlobal2WellIndex(curPos),
                                                   d->FocusReadyMM(),
                                                   d->isMultiDishHolder)) {
                    QLOG_ERROR() << "Fails to run experiment";

                    QMutexLocker locker(&d->runMutex);
                    d->status.fail = true;
                    break;
                }

                QLOG_INFO() << "Waiting the end of sequence";

                auto keepWaiting = true;
                auto timeoutCount = 0;

                while(keepWaiting) {
                    msleep(200);

                    const auto progress = instrument->CheckSequenceProgress();
                    static double prevProgress = -1.0;
                    static AppEntity::Position prevPosition;

                    if(prevProgress != std::get<1>(progress)) {
                        auto pos = std::get<2>(progress).toMM();
                        QLOG_INFO() << "[" << taskIdx+1 << "/" << d->status.totalTasks << "] "
                                    << "elapsed=" << unitTimer.elapsed() << "msec / "
                                    << timeoutInMSec << "msec progress=" << std::get<1>(progress)
                                    << " pos=(" << pos.x << "," << pos.y << "," << pos.z << ")";
                        prevProgress = std::get<1>(progress);
                    }

                    QMutexLocker locker(&d->runMutex);
                    d->status.currentTask = taskIdx;
                    d->status.error = !std::get<0>(progress);
                    d->status.sequenceProgress = std::get<1>(progress);
                    d->status.currentPosition = std::get<2>(progress);
                    d->status.elapsedSeconds = globalTimer.elapsed() / 1000;
                    if(d->status.error) {
                        d->status.errorMessage = instrument->GetErrorMessage();
                    }
                    d->UpdateProgress();

                    if(std::get<1>(progress) == 1.0) break;
                    if(!d->running) break;
                    if(d->status.error) {
                        QLOG_INFO() "Stop is triggered because of error";
                        d->status.stopped = true;
                        break;
                    }

                    if(unitTimer.elapsed() <= timeoutInMSec) {
                        if(prevPosition != d->status.currentPosition) timeoutCount++;
                        else timeoutCount = 0;
                    }

                    if(timeoutCount > 50) {
                        keepWaiting = false;
                        QLOG_ERROR() << "Instrument is not busy while it can't finish imaging in time";
                    }

                    prevPosition = d->status.currentPosition;
                }

                QMutexLocker locker(&d->runMutex);
                if(!d->running) {
                    d->status.stopped = true;
                    QLOG_INFO() "Stop is triggered";
                    break;
                } else if(d->status.error) {
                    QLOG_ERROR() << "Error is occurred during performing experiment";
                    break;
                } else {
                    QLOG_INFO() << "[" << taskIdx+1 << "/" << d->status.totalTasks << "] " << "elapsed=" << unitTimer.elapsed() << "msec";
                }
            }

            seqIdx++;

            QMutexLocker locker(&d->runMutex);
            if(d->status.stopped == true) break;
            if(!d->running) {
                d->status.stopped = true;
                QLOG_INFO() "Stop is triggered";
                break;
            }
        }

        if(d->status.stopped) {
            instrument->StopAcquisition();
            d->output->NotifyStopped();
        } else {
            auto remainCount = instrument->ImageCountInBuffer();
            if(remainCount > 0) {
                QLOG_INFO() << "Wait for the end of image transfer [remains=" << remainCount << "]";

                while(remainCount > 0) {
                    msleep(50);
                    remainCount = instrument->ImageCountInBuffer();
                }

                QLOG_INFO() << "All images are transferred";
            } else {
                QLOG_INFO() << "All images are acquired";
            }
        }

        instrument->UninstallImagePort(imagingSystem->GetImagePort());
        instrument->ChangeFullFOV();

        instrument->ResumeLive();

        const auto endPos = instrument->GetAxisPosition();
        if((endPos.X() == startPos.X()) && (endPos.Y() == startPos.Y())) {
            instrument->MoveAxis(AppEntity::Axis::Z, startPos.toMM().z);
        } else {
            instrument->MoveAxis(startPos);
        }

        auto motionStatus = instrument->CheckAxisMotion();
        while(motionStatus.moving) {
            msleep(200);
            motionStatus = instrument->CheckAxisMotion();
        }

        auto imagePort = imagingSystem->GetImagePort();
        if(!imagePort->IsEmpty()) {
            auto index = 0;

            QLOG_INFO() << "Waiting for all images to be saved";
            while(!imagePort->IsEmpty()) {
                QMutexLocker locker(&d->runMutex);
                d->status.statusMessage = tr("Waiting for all images to be saved. [%1]").arg(QString().fill('=', index%10));
                d->UpdateProgress();
                locker.unlock();

                msleep(200);
                index++;
            }
            QLOG_INFO() << "All images are saved";

            QMutexLocker locker(&d->runMutex);
            d->status.statusMessage = tr("All images are saved");
            d->UpdateProgress();
        }

        const auto progress = instrument->CheckSequenceProgress();
        QMutexLocker locker(&d->runMutex);
        d->status.currentPosition = startPos;
        d->status.completed = true;
        d->UpdateProgress();

        QLOG_INFO() << "Experiment runner thread is stopped";
    }
}
