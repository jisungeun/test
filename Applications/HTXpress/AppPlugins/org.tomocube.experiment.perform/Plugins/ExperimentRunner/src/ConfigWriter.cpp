#include <QSettings>
#include <QDebug>
#include <QTextCodec>

#include <System.h>
#include <SystemStatus.h>
#include <SessionManager.h>
#include "ConfigWriter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentRunner {
    struct Color {
        int32_t red{ 0 };
        int32_t green{ 0 };
        int32_t blue{ 0 };

        Color() {
        }

        Color(const AppEntity::ImagingConditionFL::Color& color) {
            red = color.red;
            green = color.green;
            blue = color.blue;
        }

        inline auto toStr()->QString {
            return QString("%1,%2,%3").arg(red).arg(green).arg(blue);
        }
    };

    struct ConfigWriter::Impl {
        QString title;
        AppEntity::Experiment::Pointer experiment;
        int32_t vesselIdx;
        QString outputPath;

        QMap<WellIndex, QMap<LocationIndex,Location>> locations;
        QList<AppEntity::PositionGroup> acqPositions;
        QMap<WellIndex, QString> groupNames;
        QMap<WellIndex, QString> wellNames;
        QString userID;

        struct {
            int32_t ht_3d{ 0 };
            int32_t ht_2d{ 0 };
            int32_t fl_3d[3]{ 0, 0, 0 };
            int32_t fl_2d[3]{ 0, 0, 0 };
            int32_t bf{ 0 };
            int32_t ht_3d_z{ 0 };
            int32_t fl_3d_z{ 0 };
            int32_t bf_channel{ 1 };
        } count;

        struct {
            double ht_zstep{ 0 };
            double fl_zstep{ 0 };
            double fl_exfilter[3]{ 0, 0, 0 };
            double fl_emfilter[3]{ 0, 0, 0 };
            Color fl_colors[3];
            int32_t fl_intensity[3]{0, 0, 0};
            int32_t fl_exposuretime[3]{0, 0, 0};
        } setting;

        struct {
            double pixel{ 0 };
            double magnification{ 0 };
            double objectNA{ 0 };
            double condenserNA{ 0 };
            double mediumRI{ 0 };
            QString deviceSerial;
            QString deviceHost;
            QString softwareVersion;
            bool sampleStageEncoder{ false };
        } device;

        struct {
            double cPos{ 0 };
            double flOffset{ 0 };
        } positions;

        struct {
            int32_t fovX{ 0 };
            int32_t fovY{ 0 };
            int32_t offsetX{ 0 };
            int32_t offsetY{ 0 };
        } image;

        auto Process()->void;
        auto Generate(const QString& path, const AppEntity::WellIndex wellIdx,
            const AppEntity::Position& center, int32_t tilesX, int32_t tilesY,
            const double& acquisitionSizeX, const double& acquisitionSizeY)->void;
    };

    auto ConfigWriter::Impl::Process() -> void {
        auto model = AppEntity::System::GetModel();
        auto PPU = model->AxisResolutionPPM(AppEntity::Model::Axis::Z) / 1000.0;

        userID = AppEntity::SessionManager::GetInstance()->GetID();
        wellNames = experiment->GetWellNames(vesselIdx);
        groupNames = experiment->GetWellGroupNames(vesselIdx);
        locations = experiment->GetAllLocations(vesselIdx);

        const auto vessel = experiment->GetVessel();
        const auto scenario = experiment->GetScenario(vesselIdx);
        const auto seqCount = scenario->GetCount();

        for(int32_t seqIdx=0; seqIdx<seqCount; seqIdx++) {
            auto sequence = scenario->GetSequence(seqIdx);

            const auto timeCount = sequence->GetTimeCount();

            for(auto cond : sequence->GetImagingCondition()) {
                const auto imagingType = cond->GetImagingType();
                switch(imagingType) {
                case AppEntity::ImagingType::HT3D:
                    count.ht_3d += timeCount;
                    count.ht_3d_z = cond->GetSliceCount();
                    setting.ht_zstep =cond->GetSliceStep() / PPU;
                    break;
                case AppEntity::ImagingType::HT2D:
                    count.ht_2d += timeCount;
                    break;
                case AppEntity::ImagingType::FL3D: {
                    const auto flCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(cond);
                    for(auto channel : flCond->GetChannels()) {
                        count.fl_3d[channel] += timeCount;
                        setting.fl_exfilter[channel] = flCond->GetExFilter(channel).GetWaveLength() / 1000.0;
                        setting.fl_emfilter[channel] = flCond->GetEmFilter(channel).GetWaveLength() / 1000.0;
                        setting.fl_colors[channel] = flCond->GetColor(channel);
                        setting.fl_intensity[channel] = flCond->GetIntensity(channel);
                        setting.fl_exposuretime[channel] = flCond->GetExposure(channel);
                    }
                    count.fl_3d_z = flCond->GetSliceCount();
                    setting.fl_zstep = flCond->GetSliceStep() / PPU;
                    positions.flOffset = flCond->GetScanFocusFLOffset();
                    } break;
                case AppEntity::ImagingType::FL2D: {
                    const auto flCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(cond);
                    for(auto channel : flCond->GetChannels()) {
                        count.fl_2d[channel] += timeCount;
                        setting.fl_exfilter[channel] = flCond->GetExFilter(channel).GetWaveLength() / 1000.0;
                        setting.fl_emfilter[channel] = flCond->GetEmFilter(channel).GetWaveLength() / 1000.0;
                        setting.fl_colors[channel] = flCond->GetColor(channel);
                        setting.fl_intensity[channel] = flCond->GetIntensity(channel);
                        setting.fl_exposuretime[channel] = flCond->GetExposure(channel);
                    }
                    } break;
                case AppEntity::ImagingType::BFGray:
                    count.bf += timeCount;
                    count.bf_channel = 1;
                    break;
                case AppEntity::ImagingType::BFColor:
                    count.bf += timeCount;
                    count.bf_channel = 3;
                    break;
                }
            }
        }

        device.deviceSerial = AppEntity::System::GetSystemConfig()->GetSerial();
        device.deviceHost = AppEntity::System::GetHostID();
        device.softwareVersion = AppEntity::System::GetSoftwareVersion();
        device.pixel = model->CameraPixelSize();
        device.magnification = model->ObjectiveLensMagnification();
        device.objectNA = model->ObjectiveLensNA();
        device.condenserNA = vessel->GetNA();
        device.mediumRI = experiment->GetMedium().GetRI();
        device.sampleStageEncoder = AppEntity::SystemStatus::GetInstance()->GetSampleStageEncoderSupported();

        positions.cPos = AppEntity::SystemStatus::GetInstance()->GetCurrentCondensorPosition();
    }

    auto ConfigWriter::Impl::Generate(const QString& path,
                                      const AppEntity::WellIndex wellIdx,
                                      const AppEntity::Position& center,
                                      int32_t tilesX,
                                      int32_t tilesY,
                                      const double& acquisitionSizeX,
                                      const double& acquisitionSizeY) -> void {
        QSettings qs(QString("%1/config.dat").arg(path), QSettings::IniFormat);
        qs.setIniCodec(QTextCodec::codecForName("UTF-8"));

        qs.setValue("JobInfo/Title", title);
        qs.setValue("JobInfo/User_ID", userID);

        qs.setValue("AcquisitionCount/HT_3D_Count", count.ht_3d);
        qs.setValue("AcquisitionCount/HT_2D_Count", count.ht_2d);
        qs.setValue("AcquisitionCount/FL_3D_CH0_Count", count.fl_3d[0]);
        qs.setValue("AcquisitionCount/FL_3D_CH1_Count", count.fl_3d[1]);
        qs.setValue("AcquisitionCount/FL_3D_CH2_Count", count.fl_3d[2]);
        qs.setValue("AcquisitionCount/FL_2D_CH0_Count", count.fl_2d[0]);
        qs.setValue("AcquisitionCount/FL_2D_CH1_Count", count.fl_2d[1]);
        qs.setValue("AcquisitionCount/FL_2D_CH2_Count", count.fl_2d[2]);
        qs.setValue("AcquisitionCount/BF_Count", count.bf);
        qs.setValue("AcquisitionCount/HT_3D_Z_Count", count.ht_3d_z);
        qs.setValue("AcquisitionCount/FL_3D_Z_Count", count.fl_3d_z);
        qs.setValue("AcquisitionCount/BF_Channel", count.bf_channel);

        qs.setValue("AcquisitionSetting/HT_Z_Step_Length_Micrometer", setting.ht_zstep);
        qs.setValue("AcquisitionSetting/FL_Z_Step_Length_Micrometer", setting.fl_zstep);
        qs.setValue("AcquisitionSetting/FL_CH0_Excitation_Wavelength_Micrometer", setting.fl_exfilter[0]);
        qs.setValue("AcquisitionSetting/FL_CH1_Excitation_Wavelength_Micrometer", setting.fl_exfilter[1]);
        qs.setValue("AcquisitionSetting/FL_CH2_Excitation_Wavelength_Micrometer", setting.fl_exfilter[2]);
        qs.setValue("AcquisitionSetting/FL_CH0_Emission_Wavelength_Micrometer", setting.fl_emfilter[0]);
        qs.setValue("AcquisitionSetting/FL_CH1_Emission_Wavelength_Micrometer", setting.fl_emfilter[1]);
        qs.setValue("AcquisitionSetting/FL_CH2_Emission_Wavelength_Micrometer", setting.fl_emfilter[2]);
        qs.setValue("AcquisitionSetting/FL_CH0_Color", setting.fl_colors[0].toStr());
        qs.setValue("AcquisitionSetting/FL_CH1_Color", setting.fl_colors[1].toStr());
        qs.setValue("AcquisitionSetting/FL_CH2_Color", setting.fl_colors[2].toStr());
        qs.setValue("AcquisitionSetting/FL_CH0_Intensity", setting.fl_intensity[0]);
        qs.setValue("AcquisitionSetting/FL_CH1_Intensity", setting.fl_intensity[1]);
        qs.setValue("AcquisitionSetting/FL_CH2_Intensity", setting.fl_intensity[2]);
        qs.setValue("AcquisitionSetting/FL_CH0_Exposure_Time_Millisecond", setting.fl_exposuretime[0]);
        qs.setValue("AcquisitionSetting/FL_CH1_Exposure_Time_Millisecond", setting.fl_exposuretime[1]);
        qs.setValue("AcquisitionSetting/FL_CH2_Exposure_Time_Millisecond", setting.fl_exposuretime[2]);

        qs.setValue("AcquisitionPosition/WellIndex", wellIdx);
        qs.setValue("AcquisitionPosition/Position_X_Millimeter", QString::number(center.toMM().x, 'f', 3));
        qs.setValue("AcquisitionPosition/Position_Y_Millimeter", QString::number(center.toMM().y, 'f', 3));
        qs.setValue("AcquisitionPosition/Position_Z_Millimeter", QString::number(center.toMM().z, 'f', 3));
        qs.setValue("AcquisitionPosition/Position_C_Millimeter", QString::number(positions.cPos, 'f', 3));
        qs.setValue("AcquisitionPosition/FL_Acquisition_Z_Offset_Micrometer", QString::number(positions.flOffset, 'f', 3));

        qs.setValue("AcquisitionSize/Acquisition_Size_X_Micrometer", QString::number(acquisitionSizeX, 'f', 3));
        qs.setValue("AcquisitionSize/Acquisition_Size_Y_Micrometer", QString::number(acquisitionSizeY, 'f', 3));

        qs.setValue("ImageInfo/ROI_Size_X_Pixels", image.fovX);
        qs.setValue("ImageInfo/ROI_Size_Y_Pixels", image.fovY);
        qs.setValue("ImageInfo/ROI_Offset_X_Pixels", image.offsetX);
        qs.setValue("ImageInfo/ROI_Offset_Y_Pixels", image.offsetY);

        qs.setValue("TileInfo/Tile_Number_X", tilesX);
        qs.setValue("TileInfo/Tile_Number_Y", tilesY);

        qs.setValue("DeviceInfo/Pixel_Size_Micrometer", device.pixel);
        qs.setValue("DeviceInfo/Magnification", device.magnification);
        qs.setValue("DeviceInfo/Objective_NA", device.objectNA);
        qs.setValue("DeviceInfo/Condenser_NA", device.condenserNA);
        qs.setValue("DeviceInfo/Medium_RI", device.mediumRI);
        qs.setValue("DeviceInfo/Device_Serial", device.deviceSerial);
        qs.setValue("DeviceInfo/Device_Host", device.deviceHost);
        qs.setValue("DeviceInfo/Device_Software_Version", device.softwareVersion);
        qs.setValue("DeviceInfo/SampleStage_Encoder_Supported", device.sampleStageEncoder);
    }

    ConfigWriter::ConfigWriter() : d{new Impl} {
    }

    ConfigWriter::~ConfigWriter() {
    }

    auto ConfigWriter::SetTitle(const QString& title) -> void {
        d->title = title;
    }

    auto ConfigWriter::SetOutputPath(const QString& outputPath) -> void {
        d->outputPath = outputPath;
    }

    auto ConfigWriter::SetExperiment(const AppEntity::Experiment::Pointer experiment, int32_t vesselIdx) -> void {
        d->experiment = experiment;
        d->vesselIdx = vesselIdx;
    }

    auto ConfigWriter::SetPositions(const QList<AppEntity::PositionGroup>& positions) -> void {
        d->acqPositions = positions;
    }

    auto ConfigWriter::SetFOV(const int32_t fovX, const int32_t fovY, const int32_t offsetX, const int32_t offsetY) -> void {
        d->image.fovX = fovX;
        d->image.fovY = fovY;
        d->image.offsetX = offsetX;
        d->image.offsetY = offsetY;
    }

    auto ConfigWriter::Perform() -> bool {
        d->Process();

        for (auto posIter = d->acqPositions.begin(); posIter != d->acqPositions.end(); posIter++) {
            const auto path = QString("%1/%2").arg(d->outputPath).arg(posIter->GetTitle());

            const auto wellIndex = posIter->GetWellIndex();

            const auto locationIndex = posIter->GetLocationIndex();
            const auto locations = d->locations[wellIndex];
            const auto location = locations[locationIndex];

            d->Generate(path,
                        wellIndex,
                        posIter->GetCenter(),
                        posIter->GetCols(),
                        posIter->GetRows(),
                        location.GetArea().toUM().width,
                        location.GetArea().toUM().height);
        }

        return true;
    }
}