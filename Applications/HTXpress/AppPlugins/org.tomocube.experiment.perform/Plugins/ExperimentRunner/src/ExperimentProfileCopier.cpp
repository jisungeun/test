﻿#include <QDir>
#include <AppEntityDefines.h>
#include <ProfileCopier.h>

#include "ExperimentProfileCopier.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentRunner {
    struct ExperimentProfileCopier::Impl {
        QString sampleTypeName{};
        QString experimentPath{};
        QList<AppEntity::PositionGroup> positions{};
    };

    ExperimentProfileCopier::ExperimentProfileCopier() : d{std::make_unique<Impl>()} {
    }

    ExperimentProfileCopier::~ExperimentProfileCopier() = default;

    auto ExperimentProfileCopier::SetSampleTypeName(const QString& sampleTypeName) -> void {
        d->sampleTypeName = sampleTypeName;
    }

    auto ExperimentProfileCopier::SetExperimentPath(const QString& experimentPath) -> void {
        d->experimentPath = experimentPath;
    }

    auto ExperimentProfileCopier::SetPositions(const QList<AppEntity::PositionGroup>& positions) -> void {
        d->positions = positions;
    }

    auto ExperimentProfileCopier::Perform() const -> QStringList {
        QStringList destinations{};
        const auto sourceFolder = d->experimentPath;
        for (auto it = d->positions.cbegin(); it != d->positions.cend(); ++it) {
            const auto dataFolderPath = d->experimentPath + QDir::separator() + it->GetTitle();
            const auto dataProfileFolderPath = dataFolderPath + QDir::separator() + "profile";
            destinations.push_back(dataProfileFolderPath);
        }

        const AppComponents::ProfileCopier::ProfileCopier copier(d->sampleTypeName, sourceFolder, destinations);
        const auto failedList = copier.Copy();
        return failedList;
    }
}
