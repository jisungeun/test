#pragma once
#include <memory>
#include <QMap>

#include <Vessel.h>
#include <PositionGroup.h>
#include <Location.h>
#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentRunner {
    class BackgroundCopier {
    public:
        using WellIndex = AppEntity::WellIndex;
        using Location = AppEntity::Location;
        using LocationIndex = AppEntity::LocationIndex;

    public:
        BackgroundCopier();
        ~BackgroundCopier();

        auto SetSourcePath(const QString& sourcePath)->void;
        auto SetOutputPath(const QString& outputPath)->void;
        auto SetPositions(const QList<AppEntity::PositionGroup>& positions)->void;

        auto Perform()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}