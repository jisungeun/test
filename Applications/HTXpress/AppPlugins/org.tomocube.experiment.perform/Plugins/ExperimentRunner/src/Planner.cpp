#define LOGGER_TAG "[ExperimentRunner]"
#include <TCLogger.h>

#include <QDir>
#include <QRectF>
#include <QLineF>
#include <QPainterPath>

#include <ScanningPlanner.h>
#include "Planner.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentRunner {
    struct Planner::Impl {
        AppEntity::Position sysCenter;
        AppEntity::Vessel::Pointer vessel;
        AppEntity::Area fov;
        uint32_t overlap{ 0 };
        QMap<WellIndex, QMap<LocationIndex,Location>> locations;
        QMap<WellIndex, QString> groupNames;
        QMap<WellIndex, QString> wellNames;
        QMap<AppEntity::WellIndex, QMap<AppEntity::ROIIndex, std::shared_ptr<AppEntity::ROI>>> roiAreas;
        QString title;
        auto well2global(const AppEntity::WellIndex& wellIdx, 
                                const AppEntity::Position& position)->AppEntity::Position {
            auto wellPosition = vessel->GetWellPosition(wellIdx);
            return sysCenter + wellPosition + position;
        }

        QString experimentPath;
        AcquireType::_enumerated type;

        using SingleCount = int32_t;
        using TimelapseCount = int32_t;
        using OneCycleCount = int32_t;
        auto GetCurrentImagingFolderCounts() const -> std::tuple<SingleCount, TimelapseCount, OneCycleCount>;
        auto GetSpecimenNameByWellIndex(WellIndex wellIndex) -> QString;
        auto GetWellNameByWellIndex(WellIndex wellIndex) -> QString;
        auto GetDataFolderName(const QString& specimen, 
                               const QString& wellName, 
                               const QString& roiName,
                               int32_t pointIndex,
                               int32_t singlePointCount,
                               int32_t timelapsePointCount) const -> QString;
        auto GetRoiIndexName(const WellIndex& wellIndex, const Location& wellLocation) const -> QString;
    };

    auto Planner::Impl::GetCurrentImagingFolderCounts() const -> std::tuple<SingleCount, TimelapseCount, OneCycleCount> {
        SingleCount sc = 0;
        TimelapseCount tc = 0;
        OneCycleCount oc = 0;

        const auto dir = QDir(experimentPath);

        QRegExp rxSingle("[S](\\d+)");
        QRegExp rxTimelapse("[T](\\d+)[P](\\d+)");
        QRegExp rxOneCycle("[T][P](\\d+");

        for(const auto& path : dir.entryList(QDir::Filter::Dirs|QDir::NoDotAndDotDot)) {
            const auto& lastString = path.section(".", -1);
            if(lastString.contains(rxSingle)) {
                sc++;
            }
            else if(lastString.contains(rxTimelapse)) {
                tc++;
            }
            else if(lastString.contains(rxOneCycle)) {
                oc++; // unused
            }
        }

        return {sc, tc, oc};
    }

    auto Planner::Impl::GetSpecimenNameByWellIndex(WellIndex wellIndex) -> QString {
        if(groupNames.contains(wellIndex)) {
            return groupNames[wellIndex];
        }

        return "NA";
    }

    auto Planner::Impl::GetWellNameByWellIndex(WellIndex wellIndex) -> QString {
        if(wellNames.contains(wellIndex)) {
            return wellNames[wellIndex];
        }

        return "NA";
    }

    auto Planner::Impl::GetRoiIndexName(const WellIndex& wellIndex, const Location& wellLocation) const -> QString {
        if (roiAreas[wellIndex].empty()) {
            return {};
        }

        const auto pointX = wellLocation.GetCenter().toMM().x;
        const auto pointY = -wellLocation.GetCenter().toMM().y;
        const auto pointW = wellLocation.GetArea().toMM().width;
        const auto pointH = wellLocation.GetArea().toMM().height;

        QString roiName{};

        for (auto it = roiAreas[wellIndex].cbegin(); it != roiAreas[wellIndex].cend(); ++it) {
            const auto roiIndex = it.key();
            const auto roi = it.value();
            const auto roiShape = roi->GetShape();
            const auto roiX = roi->GetCenter().toMM().x;
            const auto roiY = -roi->GetCenter().toMM().y;
            const auto roiW = roi->GetSize().toMM().width;
            const auto roiH = roi->GetSize().toMM().height;

            QRectF roiRect(QPointF{roiX - roiW / 2, roiY - roiH / 2}, QPointF{roiX + roiW / 2, roiY + roiH / 2});
            QRectF pointRect(QPointF{pointX - pointW / 2, pointY - pointH / 2}, QPointF{pointX + pointW / 2, pointY + pointH / 2});
            QPainterPath roiPath;
            QPainterPath pointPath;
            pointPath.addRect(pointRect);


            if (roiShape == +AppEntity::ROIShape::Rectangle) {
                roiPath.addRect(roiRect);
            }
            else if (roiShape == +AppEntity::ROIShape::Ellipse) {
                roiPath.addEllipse(roiRect);
            }
            if(roiPath.intersects(pointPath)) {
                roiName = QString("R%1").arg(roiIndex);
                break;
            }
        }

        return roiName;
    }

    auto Planner::Impl::GetDataFolderName(const QString& specimen, const QString& wellName, const QString& roiName, int32_t pointIndex, int32_t singleCount, int32_t timelapsCount) const -> QString {
        const auto& prefix = QString("%1.%2.%3").arg(title).arg(specimen).arg(wellName + roiName);
        QString contents = ".TypeError";

        switch (type) {
            case AcquireType::OneCycle:
                contents = QString(".TP%1").arg(pointIndex, 2, 10, QLatin1Char('0'));
                break;
            case AcquireType::Single:
                contents = QString(".S%1").arg((singleCount + 1), 3, 10, QLatin1Char('0'));
                break;
            case AcquireType::Timelapse:
                contents = QString(".T%1P%2").arg((timelapsCount + 1), 3, 10, QLatin1Char('0')).arg(pointIndex, 2, 10, QLatin1Char('0'));
                break;
        }

        return prefix + contents;
    }

    Planner::Planner() : d{new Impl} {
    }

    Planner::~Planner() {
    }

    auto Planner::SetSystemCenter(const AppEntity::Position& position) -> void {
        d->sysCenter = position;
    }

    auto Planner::SetVessel(AppEntity::Vessel::Pointer vessel) -> void {
        d->vessel = vessel;
    }

    auto Planner::SetExperimentOutputPath(const QString& expPath) -> void {
        d->experimentPath = expPath;
    }

    auto Planner::SetTitle(const QString& title) -> void {
        d->title = title;
    }

    auto Planner::SetWellGroupNames(const QMap<WellIndex, QString>& groupNames) -> void {
        d->groupNames = groupNames;
    }

    auto Planner::SetWellNames(const QMap<WellIndex, QString>& wellNames) -> void {
        d->wellNames = wellNames;
    }

    auto Planner::SetMaximumFOV(const AppEntity::Area& area) -> void {
        d->fov = area;
    }

    auto Planner::SetOverlapInUM(uint32_t overlap) -> void {
        d->overlap = overlap;
    }

    auto Planner::SetLocations(const QMap<WellIndex, QMap<LocationIndex, Location>>& locations) -> void {
        d->locations = locations;
    }

    auto Planner::SetAcquireType(const AcquireType& type) -> void {
        d->type = type;
    }

    void Planner::SetRoiAreas(const QMap<AppEntity::WellIndex, QMap<AppEntity::ROIIndex, std::shared_ptr<AppEntity::ROI>>>& roiAreas) {
        d->roiAreas = roiAreas;
    }

    auto Planner::Perform() const -> QList<AppEntity::PositionGroup> {
        auto plan = AppComponents::ScanningPlanner::ScanningPlanner();

        plan.SetMaximumFOV(d->fov);
        plan.SetOverlapInUM(d->overlap);

        QList<Location> globalLocations;

        auto [singleCount, timelapseCount, oneCycleCount] = d->GetCurrentImagingFolderCounts();

        for (auto wellIndex : d->locations.keys()) {
            const auto specimenName = d->GetSpecimenNameByWellIndex(wellIndex);
            const auto wellName = d->GetWellNameByWellIndex(wellIndex);

            int32_t pointIndex = 1;
            for (auto& wellLocation : d->locations[wellIndex]) {
                const auto roiName = d->GetRoiIndexName(wellIndex, wellLocation);

                auto globalPos = d->well2global(wellIndex, wellLocation.GetCenter());
                wellLocation.SetCenter(globalPos);

                const auto title = d->GetDataFolderName(specimenName, wellName, roiName, pointIndex, singleCount, timelapseCount);

                QLOG_INFO() << "title:" << title << " [cx=" << wellLocation.GetCenter().toMM().x << " cy=" << wellLocation.GetCenter().toMM().y << " w=" << wellLocation.GetArea().toMM().width << " h=" << wellLocation.GetArea().toMM().height << "]";
                wellLocation.SetTitle(title);

                pointIndex++;
            }
        }

        plan.SetLocations(d->locations);

        return plan.Perform();
    }
}
