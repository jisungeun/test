#include <QFile>
#include <QTextStream>

#include "CilsMetafileWriter.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentRunner {
    struct CilsMetafileWriter::Impl {
        QString outputPath;
        QString project;
        AppEntity::Experiment::Pointer experiment;
        QList<AppEntity::PositionGroup> acqPositions;

        auto GetParentPath() const->QString;
    };

    auto CilsMetafileWriter::Impl::GetParentPath() const -> QString {
        return QString("%1/%2").arg(project).arg(experiment->GetTitle());
    }

    CilsMetafileWriter::CilsMetafileWriter() : d{new Impl} {
    }

    CilsMetafileWriter::~CilsMetafileWriter() {
    }

    auto CilsMetafileWriter::SetOutputPath(const QString& outputPath) -> void {
        d->outputPath = outputPath;
    }

    auto CilsMetafileWriter::SetExperiment(const QString& project, const AppEntity::Experiment::Pointer experiment) -> void {
        d->project = project;
        d->experiment = experiment;
    }

    auto CilsMetafileWriter::SetPositions(const QList<AppEntity::PositionGroup>& positions) -> void {
        d->acqPositions = positions;
    }

    auto CilsMetafileWriter::Perform() -> bool {
        for(auto posIter = d->acqPositions.begin(); posIter != d->acqPositions.end(); posIter++) {
            QFile parentFile(QString("%1/%2/.parent").arg(d->outputPath).arg(posIter->GetTitle()));
            if (!parentFile.open(QIODevice::WriteOnly | QIODevice::Text))
                return false;

            QTextStream out(&parentFile);
            out << d->GetParentPath();
        }

        return true;
    }
}