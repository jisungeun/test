#pragma once
#include <memory>
#include <QMap>

#include <ImagingSequence.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentRunner {
    class Task {
    public:
        using Pointer = std::shared_ptr<Task>;
        using ImagingSequence = AppEntity::ImagingSequence;
        
    public:
        Task() = delete;
        Task(const Task& other) = delete;
        Task(const ImagingSequence::Pointer& sequence, int32_t timeIndex, int32_t startTimeMSec);
        ~Task();
        
        auto operator=(const Task& other)->Task& = delete;

        auto GetImagingSequence() const->ImagingSequence::Pointer;
        auto GetTimeIndex() const->int32_t;
        auto GetStartTimeMSec() const->int32_t;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}