#pragma once
#include <memory>
#include <QThread>

#include <IExperimentRunner.h>
#include "HTX_Experiment_Perform_ExperimentRunnerExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentRunner {
    class HTX_Experiment_Perform_ExperimentRunner_API Runner : public QThread, public UseCase::IExperimentRunner {
        Q_OBJECT

    public:
        Runner();
        ~Runner() override;

        auto SetOutputPort(UseCase::IRunExperimentOutputPort* output) -> void override;
        auto SetExperiment(const QString& projectTitle, AppEntity::Experiment::Pointer experiment) -> void override;
        auto SetVesselIndex(AppEntity::VesselIndex index) -> void override;
        auto SetOverlapInUM(const uint32_t overlapInUM) -> void override;
        auto SetAcquisitionType(const UseCase::AcquisitionType& type) -> void override;
        auto SetMultiDishHolder(bool isMultiDishHolder) -> void override;

        auto Run() -> bool override;
        auto Stop() ->bool override;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}