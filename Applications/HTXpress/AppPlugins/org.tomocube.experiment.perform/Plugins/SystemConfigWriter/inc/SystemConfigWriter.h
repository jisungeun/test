#pragma once
#include <memory>
#include <QString>

#include <ISystemConfigWriter.h>
#include "HTX_Experiment_Perform_SystemConfigWriterExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::SystemConfigWriter {
    class HTX_Experiment_Perform_SystemConfigWriter_API Writer : public UseCase::ISystemConfigWriter {
    public:
        Writer();
        ~Writer() override;

        auto SetPath(const QString& path)->void;
        auto Write() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
