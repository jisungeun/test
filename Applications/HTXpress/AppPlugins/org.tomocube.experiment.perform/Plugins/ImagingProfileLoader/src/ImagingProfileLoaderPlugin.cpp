#include <SystemStatus.h>
#include <ImagingProfileReader_v0_0_1.h>

#include "ImagingProfileLoaderPlugin.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ImagingProfileLoader {
    Loader::Loader() = default;

    Loader::~Loader() = default;

    auto Loader::GetHtScanParameter() const -> std::optional<std::tuple<int32_t, int32_t>> {
        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto experiment = sysStatus->GetExperiment();
        const auto sampleTypeName = experiment->GetSampleTypeName();
        const auto experimentPath = sysStatus->GetExperimentOutputPath();
        const auto profileFilePath = QString("%1/%2.%3.%4").arg(experimentPath).arg(sampleTypeName).arg(AppEntity::ImagingProfileSuffix).arg(AppEntity::ProfileExtension);
        const auto na = experiment->GetVessel()->GetNA();

        TC::IO::ProfileIO::ImagingProfileReader_v0_0_1 profileReader{};

        profileReader.SetPath(profileFilePath);

        if (profileReader.Read()) {
            const auto profile = profileReader.GetProfile(na);
            return std::tuple{ profile.step, profile.slice };
        }

        return std::nullopt;
    }
}
