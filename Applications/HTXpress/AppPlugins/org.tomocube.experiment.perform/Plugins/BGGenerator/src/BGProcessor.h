#pragma once
#include <memory>
#include <QStringList>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::BGGenerator {
    class BGProcessor {
    public:
        BGProcessor();
        ~BGProcessor();

        auto Process(const QStringList& images, const QString& targetPath)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}