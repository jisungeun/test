#include <QDir>

#include "BGProcessor.h"
#include "BGGeneratorPlugin.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::BGGenerator {
    Plugin::Plugin() : UseCase::IBGGenerator() {
    }

    Plugin::~Plugin() {
    }

    auto Plugin::Process(const QString& sourcePath, const QString& targetPath) -> bool {
        QDir dir(sourcePath);
        auto files = dir.entryList({"*.png"}, QDir::Filter::Files, QDir::SortFlag::Name);

        if(!QFile::exists(targetPath)) {
            QDir().mkpath(targetPath);
        }

        for(auto setIdx=0; setIdx<4; setIdx++) {
            QStringList sourceList;
            QString outPath{ QString("%1/%2.png").arg(targetPath).arg(setIdx) };

            for(auto idx=setIdx; idx<files.length(); idx+=4) {
                auto path = QString("%1/%2").arg(sourcePath).arg(files.at(idx));
                sourceList.push_back(path);
            }

            if(!BGProcessor().Process(sourceList, outPath)) return false;
        }

        return true;
    }
}
