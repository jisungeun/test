#define LOGGER_TAG "[BGGenerator]"

#include <QFile>
#include <QCoreApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <opencv2/opencv.hpp>
#pragma warning(pop)

#include <TCLogger.h>
#include <TCPython.h>

#include "BGProcessor.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::BGGenerator {
    static bool firstPyLoad{ true };

    struct BGProcessor::Impl {
        TC::PythonModule::Pointer py{ TC::PythonModule::GetInstance() };
        const QString script{ "generateBG.py" };
        QString scriptFullPath;

        auto SetupScriptPath()->void;
    };

    auto BGProcessor::Impl::SetupScriptPath() -> void {
        scriptFullPath = QString("%1/pyscript/%2").arg(qApp->applicationDirPath()).arg(script);
        if(QFile::exists(scriptFullPath)) return;

        scriptFullPath = QString("%1/%2").arg(_PYSCRIPT_DIR).arg(script);
        if(QFile::exists(scriptFullPath)) return;

        scriptFullPath.clear();
    }

    BGProcessor::BGProcessor() : d{new Impl} {
        d->SetupScriptPath();

        if(firstPyLoad) {
            d->py->SetPythonHome("C:\\TomoPython");
            d->py->InitPython();
            d->py->ExecuteString(QString("sys.path.insert(0, '%1\\pyscript')").arg(qApp->applicationDirPath()));
            d->py->ExecuteString(QString("sys.path.insert(0, '%1')").arg(_PYSCRIPT_DIR));
            firstPyLoad = false;
        }
    }

    BGProcessor::~BGProcessor() {
    }

    auto BGProcessor::Process(const QStringList& images, const QString& targetPath) -> bool {
        if(d->scriptFullPath.isEmpty()) {
            QLOG_ERROR() << "It fails to find python script";
            return false;
        }

        const auto count = static_cast<uint32_t>(images.size());
        if(count == 0) return false;

        auto cvImage = cv::imread(images.at(0).toLocal8Bit().toStdString(), cv::IMREAD_GRAYSCALE);

        uint32_t dims[] = { static_cast<uint32_t>(count),
                            static_cast<uint32_t>(cvImage.rows),
                            static_cast<uint32_t>(cvImage.cols) };
        const auto imageSize = dims[1] * dims[2];
        const auto arraySize = imageSize * dims[0];

        auto buffer = std::make_unique<char[]>(arraySize);

        for(auto idx=0u; idx<count; idx++) {
            cvImage = cv::imread(images.at(idx).toLocal8Bit().toStdString(), cv::IMREAD_GRAYSCALE);
            memcpy_s(buffer.get() + (imageSize*idx), imageSize, cvImage.data, imageSize);
        }

        try {
            d->py->CopyArrToPy(buffer.get(), 3, dims, "sourceImages", TC::ArrType::arrUBYTE, TC::ArrType::arrDouble);

            if(!d->py->ExecuteString(QString("outputPath = '%1'").arg(targetPath))) {
                QLOG_ERROR() << "It fails to set output path : " << targetPath;
                return false;
            }

            if(!d->py->ExecutePyFile(d->scriptFullPath)) {
                QLOG_ERROR() << "It fails to run the script :" << d->script;
                return false;
            }
        } catch(std::exception& ex) {
            QLOG_ERROR() << ex.what();
            return false;
        } catch(...) {
            QLOG_ERROR() << "Exception while loading python module or running python scrip";
            return false;
        }

        return true;
    }
}