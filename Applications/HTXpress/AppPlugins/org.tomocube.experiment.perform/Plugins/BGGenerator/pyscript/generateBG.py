# -*- coding: utf-8 -*-
def generateBG():
    import numpy as np
    import cv2 as cv
    
    print(outputPath)
    
    stacks = sourceImages.shape[0]
    sumImage = np.zeros((sourceImages.shape[1], sourceImages.shape[2]), np.float64)
    
    for idx in range(stacks):
        img = sourceImages[idx,:,:]
        sumImage = np.add(sumImage, img)
    
    avgImage = np.divide(sumImage, stacks)
    cv.imwrite(outputPath, avgImage)
    
def test(topDir, outDir):
    # -*- coding: utf-8 -*-
    import numpy as np
    import glob
    import cv2 as cv
    
    global outputPath
    global sourceImages
    
    files = glob.glob(topDir + '/*.png')

    stacks = int(len(files)/4)
    img = cv.imread(files[0])
    sourceImages = np.zeros((stacks, img.shape[0], img.shape[1]))

    for setIdx in range(4):
        stackIdx = 0;
        outputPath = outDir + '/bg_' + str(setIdx) + '.png'
        for file in files[setIdx::4]:
            img = cv.imread(file)
            sourceImages[stackIdx,:,:] = img[:,:,0]
            stackIdx = stackIdx + 1;
        generateBG()

if __name__ == '__main__':
    generateBG()
    #test('E:/Temp/20220819 bg/backgrounds/0.38', 'E:/Temp/temp')