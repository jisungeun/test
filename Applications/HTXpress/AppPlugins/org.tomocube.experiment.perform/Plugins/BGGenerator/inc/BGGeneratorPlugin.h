#pragma once
#include <memory>
#include <IBGGenerator.h>

#include "HTX_Experiment_Perform_BGGeneratorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::BGGenerator {
    class HTX_Experiment_Perform_BGGenerator_API Plugin : public UseCase::IBGGenerator {
    public:
        Plugin();
        ~Plugin() override;

        auto Process(const QString& sourcePath, const QString& targetPath) -> bool override;
    };
}