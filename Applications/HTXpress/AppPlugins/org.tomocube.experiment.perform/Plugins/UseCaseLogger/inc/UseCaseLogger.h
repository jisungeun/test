#pragma once
#include <memory>

#include <IUseCaseLogger.h>
#include "HTX_Experiment_Perform_UseCaseLoggerExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::UseCaseLogger {
    class HTX_Experiment_Perform_UseCaseLogger_API Logger : public UseCase::IUseCaseLogger {
    public:
        Logger();
        ~Logger() override;

    protected:
        auto Log(const QString& useCase, const QString& message)->void override;
        auto Error(const QString& useCase, const QString& message)->void override;
    };
}