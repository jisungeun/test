#pragma once
#include <memory>

#include <IScanTimeCalculator.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ScanTimeCalculator {
    class CalculatorDefault : public UseCase::IScanTimeCalculator {
    public:
        using ImagingTimeTable = UseCase::ImagingTimeTable;

    public:
        CalculatorDefault();
        ~CalculatorDefault() override;

        auto SetMaximumFOV(AppEntity::Area area) -> void override;
        auto SetOverlapInUM(uint32_t overlap) -> void override;
        auto SetFilterPosition(AppEntity::Modality modality, int32_t channel, int32_t position) -> void override;
        auto SetUsingMultidishHolder(bool useMultidishHolder) -> void override;

        auto CalcInSec(const ImagingCondition::Pointer& imgCond, 
                       ImagingTimeTable::Pointer& timeTable,
                       const ImagingCondition::Pointer& prevCond) -> void override;
        auto CalcInSec(const QList<Location>& locations, ImagingTimeTable::Pointer& timeTable) -> void override;
        auto CalcInSec(const QList<PositionGroup>& groups, ImagingTimeTable::Pointer& timeTable) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}