#include <ScanTimeCalculator.h>

#include "CalculatorDefault.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ScanTimeCalculator {
    using Component = AppComponents::ScanTimeCalculator::Component;
    using TimeTable = AppComponents::ScanTimeCalculator::ImagingTimeTable;

    struct CalculatorDefault::Impl {
        Component::Pointer calculator{ Component::GetInstance() };
    };

    CalculatorDefault::CalculatorDefault() : UseCase::IScanTimeCalculator(), d{new Impl} {
    }

    CalculatorDefault::~CalculatorDefault() {
    }

    auto CalculatorDefault::SetMaximumFOV(AppEntity::Area area) -> void {
        d->calculator->SetMaximumFOV(area);
    }

    auto CalculatorDefault::SetOverlapInUM(uint32_t overlap) -> void {
        d->calculator->SetOverlapInUM(overlap);
    }

    auto CalculatorDefault::SetFilterPosition(AppEntity::Modality modality, int32_t channel, int32_t position) -> void {
        d->calculator->SetFilterPosition(modality, channel, position);
    }

    auto CalculatorDefault::SetUsingMultidishHolder(bool useMultidishHolder) -> void {
        d->calculator->SetUsingMultidishHolder(useMultidishHolder);
    }

    auto CalculatorDefault::CalcInSec(const ImagingCondition::Pointer& imgCond, 
                                      ImagingTimeTable::Pointer& timeTable,
                                      const ImagingCondition::Pointer& prevCond) -> void {
        TimeTable::Pointer table{ new TimeTable() };
        d->calculator->CalcInSec(imgCond, table, prevCond);
        timeTable->SetTimeForCondition(imgCond->GetModality(), imgCond->Is2D(), 
                                       table->GetTimeForCondition(imgCond->GetModality(), imgCond->Is2D()));
    }

    auto CalculatorDefault::CalcInSec(const QList<Location>& locations,
                                      ImagingTimeTable::Pointer& timeTable) -> void {
        TimeTable::Pointer table{ new TimeTable() };
        d->calculator->CalcInSec(locations, table);
        timeTable->SetTimeForCycle(table->GetTimeForCycle(), table->GetPointCount());
    }

    auto CalculatorDefault::CalcInSec(const QList<PositionGroup>& groups,
                                      ImagingTimeTable::Pointer& timeTable) -> void {
        TimeTable::Pointer table{ new TimeTable() };
        d->calculator->CalcInSec(groups, table);
        timeTable->SetTimeForCycle(table->GetTimeForCycle(), table->GetPointCount());
    }
}
