#include <QList>

#include <catch2/catch.hpp>

#include <System.h>
#include <SystemConfig.h>

#include "../src/CalculatorDefault.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ScanTimeCalculator::Test {
    using CalculatorDefault = ScanTimeCalculator::CalculatorDefault;
    using Position = AppEntity::Position;
    using PositionGroup = AppEntity::PositionGroup;
    using ImagingTimeTable = UseCase::ImagingTimeTable;

    class Model : public AppEntity::Model {
    public:
        Model() {
        }

        ~Model() override {
        }

        //optical specification
        auto ObjectiveLensMagnification() const -> double override { return 1.0; }
        auto ObjectiveLensNA() const -> double override { return 1.0; }
        auto CondenserLensMagnification() const -> double override { return 1.0; }
        auto CondenserLensNA() const -> double override { return 1.0; }
        auto CameraPixelSize() const -> double override { return 1.0; }
        auto CameraPixelsH() const -> uint32_t override { return 1; }
        auto CameraPixelsV() const -> uint32_t override { return 1; }
        auto CameraFOVStepH() const -> uint32_t override { return 1; }
        auto CameraFOVStepV() const -> uint32_t override { return 1; }
        auto MinimumIntervalUSec() const -> int32_t override { return 1; }
        auto MaximumIntervalUSec() const -> int32_t override { return 1; }
        auto MinimumIdleUSec() const -> int32_t override { return 1; }
        auto MinimumExposureUSec() const -> int32_t override { return 1; }
        auto MaximumExpsoureUSec() const -> int32_t override { return 1; }
        auto HTLightChannel() const -> int32_t override{ return 1; }
        auto HTFilterChannel() const -> int32_t override{ return 1; }
        auto BFLightChannel() const -> int32_t override{ return 1; }
        auto BFFilterChannel() const -> int32_t override{ return 1; }
        auto BFLightIntensity() const -> int32_t override { return 1; }
        auto FLMaximumSlices() const -> int32_t override { return 1; }
        auto CondenserAFLightChannel() const -> int32_t override{ return 1; }
        auto ImagingCamera() const -> std::tuple<QString, QString> override { return std::make_tuple("", ""); }
        auto CondenserCamera() const -> std::tuple<QString, QString> override { return std::make_tuple("", ""); }
        auto AxisResolutionPPM(Axis axis) const -> uint32_t override { Q_UNUSED(axis); return 1; }
        auto AxisLowerLimitMM(Axis axis) const -> double override { Q_UNUSED(axis); return 1.0; }
        auto AxisUpperLimitMM(Axis axis) const -> double override { Q_UNUSED(axis); return 1.0; }
        auto AxisMotionTimeSPM(Axis axis) const -> double override { Q_UNUSED(axis); return 1.0; }
        auto SafeZPositionMM() const -> double override { return 1.0; }
        auto LoadingXPositionMM() const -> double override { return 1.0; }
        auto LoadingYPositionMM() const -> double override { return 1.0; }
        auto EmissionFilterPules() const -> QList<int32_t> override { return QList<int32_t>(); }
        auto DripTrayRemovingXPositionMM() const -> double override { return 1.0; }
        auto DripTrayRemovingYPositionMM() const -> double override { return 1.0; }
        auto DripTrayRemovingZPositionMM() const -> double override { return 1.0; }
        auto DripTrayRemovingCPositionMM() const -> double override { return 1.0; }
        auto ParkingXPositionMM() const -> double override { return 1.0; }
        auto ParkingYPositionMM() const -> double override { return 1.0; }
        auto ParkingZPositionMM() const -> double override { return 1.0; }
        auto ParkingCPositionMM() const -> double override { return 1.0; }
        auto FilterChangeTime() const -> std::tuple<double, double> override { return std::make_tuple(0.1, 0.1); }
        auto SampleStageMotionTime() const -> std::tuple<double, double> override { return std::make_tuple(0.1, 0.1); }
        auto ZScanMotionTime() const -> std::tuple<double, double> override { return std::make_tuple(0.1, 0.1); }
        auto TriggerWithMotionAccelerationTime() const -> double override { return 0.1; }
        auto TriggerWithMotionDecelerationTime() const -> double override { return 0.1; }
        auto TriggerWithMotionMarginTime() const -> double override { return 0.1; }
        auto FocusToScanReadyMM() const -> double override { return 0.1; }
        auto BFImagingMarginTime() const -> double override { return 1.0; }
        auto HTTriggerStartOffsetPulse() const -> int32_t override { return 1; }
        auto HTTriggerEndOffsetPulse() const -> int32_t override { return 1; }
        auto HTTriggerPulseWidthUSec() const -> int32_t override { return 1; }
        auto HTTriggerReadOutUSec() const -> int32_t override { return 1; }
        auto HTTriggerExposureUSec() const -> int32_t override { return 1; }
        auto HTTriggerIntervalMarginUSec() const -> int32_t override { return 1; }
        auto HTTriggerAccelerationFactor() const -> int32_t override { return 1; }
        auto CondenserAFScanStartPosPulse() const->int32_t override { return 1; }
        auto CondenserAFScanIntervalPulse() const->int32_t override { return 1; }
        auto CondenserAFScanTriggerSlices() const->int32_t override { return 1; }
        auto CondenserAFScanReadOutUSec() const->int32_t override { return 1; }
        auto CondenserAFScanExposureUSec() const->int32_t override { return 1; }
        auto CondenserAFScanIntervalMarginUSec() const->int32_t override { return 1; }
        auto CondenserAFScanPulseWidthUSec() const->int32_t override { return 1; }
        auto MultiDishHolderThickness() const -> double override { return 2.0; }
        auto GetIlluminationPattern(const AppEntity::PatternIndex& index) const -> int32_t override {
            Q_UNUSED(index)
            return 0;
        }
        auto GetIlluminationPattern(AppEntity::ImagingType imagingType, 
                                    AppEntity::TriggerType triggerType, int32_t channel) const -> int32_t override {
            Q_UNUSED(imagingType)
            Q_UNUSED(triggerType)
            Q_UNUSED(channel)
            return 0;
        }
        auto GetIlluminationPatterns() const -> QList<AppEntity::PatternIndex> override {
            return QList<AppEntity::PatternIndex>();
        }
        auto GetFLIlluminationMap(int32_t channel) const -> AppEntity::IlluminationMapIndex override {
            Q_UNUSED(channel)
            return AppEntity::IlluminationMapIndex();
        }
        auto GetFLIlluminationMapChannel(AppEntity::IlluminationMapIndex mapIndex) const -> int32_t override {
            Q_UNUSED(mapIndex)
            return 0;
        }
        auto GetFLIlluminationMapChannel(const QList<int32_t>& flChannels) const -> std::tuple<int32_t, QList<int32_t>> override {
            Q_UNUSED(flChannels)
            return std::make_tuple(0, QList<int32_t>());
        }
        auto GetFLIlluminationMapChannel(int32_t flChannelIndex) const -> std::tuple<int32_t, int32_t> override {
            Q_UNUSED(flChannelIndex)
            return std::make_tuple(0, 0);
        }
        auto GetFLIlluminationMapChannels() const -> QList<int32_t> override {
            return QList<int32_t>();
        }
        auto GetHTIlluminationSetupPattern(double NA) const -> int32_t {
            Q_UNUSED(NA)
            return 0;
        }
        auto GetHTIlluminationSetupPatternNAs() const -> QList<double> {
            return QList<double>();
        }
        auto EvaluationReference(AppEntity::EvaluationRef item) const -> double override {
            Q_UNUSED(item)
            return 0;
        }
        auto EvaluationReference(AppEntity::EvaluationRef item, double NA) const -> double override {
            Q_UNUSED(item)
            Q_UNUSED(NA)
            return 0;
        }
    };

    TEST_CASE("Cycle Time") {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        sysConfig->SetAutoFocusTime(1.0);

        auto system = AppEntity::System::GetInstance();
        system->SetModel(std::make_shared<Model>());

        SECTION("Two Points") {
            auto calculator = CalculatorDefault();

            const auto model = AppEntity::System::GetModel();
            auto [defaultTime, unitTime] = model->SampleStageMotionTime();
            auto afTime = sysConfig->GetAutoFocusTime();

            QList<PositionGroup> list;
            list.push_back(PositionGroup("", Position::fromMM(0, 0, 0)));
            list.push_back(PositionGroup("", Position::fromMM(1, 0, 0)));

            auto timeTable = std::make_shared<ImagingTimeTable>();
            calculator.CalcInSec(list, timeTable);

            //(0,0) - (1, 0) - (0,0)
            const auto cycleTime = timeTable->GetTimeForCycle();
            REQUIRE(cycleTime == Approx(defaultTime*2 + unitTime*2 + afTime*2));

            REQUIRE(timeTable->GetPointCount() == 2);
        }
    }

    TEST_CASE("Imging Conditions") {
        SECTION("HT 2D") {
            auto cond = std::make_shared<AppEntity::ImagingConditionHT>();
            cond->SetSliceCount(1);
            cond->SetDimension(2);
            cond->SetInterval(1000);

            auto timeTable = std::make_shared<ImagingTimeTable>();

            auto calculator = CalculatorDefault();
            calculator.CalcInSec(cond, timeTable, false);

            const auto imagingTime = timeTable->GetTimeForAllConditions();
            REQUIRE(imagingTime == Approx(0.001));
        }

        SECTION("HT 3D") {
            auto cond = std::make_shared<AppEntity::ImagingConditionHT>();
            cond->SetSliceCount(10);
            cond->SetDimension(3);
            cond->SetInterval(1000);

            const auto model = AppEntity::System::GetModel();
            auto [defaultTime, unitTime] = model->SampleStageMotionTime();

            auto timeTable = std::make_shared<ImagingTimeTable>();

            auto calculator = CalculatorDefault();
            calculator.CalcInSec(cond, timeTable, false);

            const auto imagingTime = timeTable->GetTimeForAllConditions();
            REQUIRE(imagingTime == Approx(0.01 + defaultTime));
        }

        SECTION("FL 2D") {
            auto cond = std::make_shared<AppEntity::ImagingConditionFL>();
            cond->SetSliceCount(1);
            cond->SetDimension(2);
            cond->SetInterval(0, 1000);
            cond->SetInterval(1, 2000);

            auto timeTable = std::make_shared<ImagingTimeTable>();

            auto calculator = CalculatorDefault();
            calculator.CalcInSec(cond, timeTable, false);

            const auto imagingTime = timeTable->GetTimeForAllConditions();
            REQUIRE(imagingTime == Approx(0.003));
        }

        SECTION("FL 3D") {
            auto cond = std::make_shared<AppEntity::ImagingConditionFL>();
            cond->SetSliceCount(10);
            cond->SetDimension(3);
            cond->SetInterval(0, 1000);
            cond->SetInterval(1, 2000);

            const auto model = AppEntity::System::GetModel();
            auto [defaultTime, unitTime] = model->SampleStageMotionTime();

            auto timeTable = std::make_shared<ImagingTimeTable>();

            auto calculator = CalculatorDefault();
            calculator.CalcInSec(cond, timeTable, false);

            const auto imagingTime = timeTable->GetTimeForAllConditions();
            REQUIRE(imagingTime == Approx(0.03 + defaultTime*2));
        }

        SECTION("BF") {
            auto cond = std::make_shared<AppEntity::ImagingConditionBF>();
            cond->SetSliceCount(1);
            cond->SetDimension(2);
            cond->SetColorMode(true);
            cond->SetInterval(1000);

            auto timeTable = std::make_shared<ImagingTimeTable>();

            auto calculator = CalculatorDefault();
            calculator.CalcInSec(cond, timeTable, false);

            const auto imagingTime = timeTable->GetTimeForAllConditions();
            REQUIRE(imagingTime == Approx(0.001 * 3));
        }
    }
}
