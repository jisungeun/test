#pragma once
#include <memory>

#include <IScanTimeCalculator.h>
#include "HTX_Experiment_Perform_ScanTimeCalculatorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ScanTimeCalculator {
    class HTX_Experiment_Perform_ScanTimeCalculator_API Factory : UseCase::IScanTimeCalculator {
    public:
        static auto Build()->IScanTimeCalculator*;
    };
}