#pragma once
#include <memory>
#include <QThread>

#include <IExperimentReader.h>
#include "HTX_Experiment_Perform_ExperimentReaderExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentReader {
    class HTX_Experiment_Perform_ExperimentReader_API Reader : public QThread, public UseCase::IExperimentReader {
        Q_OBJECT

    public:
        Reader();
        ~Reader() override;

        auto Read(const QString& path) const -> AppEntity::Experiment::Pointer override;
    };
}