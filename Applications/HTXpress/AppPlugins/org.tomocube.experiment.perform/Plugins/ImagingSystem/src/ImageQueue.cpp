#include <QList>
#include <QMutex>

#include <ImagingSystem.h>
#include "ImageQueue.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ImagingSystem {
    using Component = AppComponents::ImagingSystem::Component;
    using Queue = AppComponents::ImagingSystem::ImageQueue;

    struct ImageQueue::Impl {
        Queue::Pointer queue{ Component::GetInstance()->GetImageQueue() };
    };

    ImageQueue::ImageQueue() : UseCase::IImagePort(), d{new Impl} {
    }

    ImageQueue::~ImageQueue() {
    }

    auto ImageQueue::Send(AppEntity::RawImage::Pointer image) -> void {
        d->queue->Send(image);
    }

    auto ImageQueue::Clear() -> void {
        //It will not remove any images to save all of them...
    }

    auto ImageQueue::IsEmpty() const -> bool {
        return d->queue->IsEmpty();
    }
}
