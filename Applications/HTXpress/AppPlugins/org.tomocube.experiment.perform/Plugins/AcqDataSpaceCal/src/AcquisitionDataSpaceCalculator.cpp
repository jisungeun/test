#define LOGGER_TAG "[AcquisitionDataSpaceCalculator]"
#include <TCLogger.h>

#include "AcquisitionDataSpaceCalculator.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::AcquisitionDataSpaceCalculator {
    using Modality = UseCase::AcqDataSpaceCal::Modality;

    struct AcquisitionDataSpaceCalculator::Impl {
        int32_t pixels{};
        int32_t points{};
        QMap<Modality, int32_t> modalityCounts{};
        QMap<Modality, int32_t> sliceCounts{};

        const QMap<Modality, float> bppInfo{
            {Modality::HT3D, 0.5},
            {Modality::FL3D, 0.35},
            {Modality::FL2D, 0.35},
            {Modality::BFGray, 0.6}
        };

        auto GetBytesPerPixel(const Modality& modality) const -> float;
    };

    auto AcquisitionDataSpaceCalculator::Impl::GetBytesPerPixel(const Modality& modality) const -> float {
        if(bppInfo.contains(modality)) {
            return bppInfo[modality];
        }
        return 0.;
    }

    AcquisitionDataSpaceCalculator::AcquisitionDataSpaceCalculator() : IAcquisitionDataSpaceCalculator(),
                                                                       d{std::make_unique<Impl>()} {
    }

    AcquisitionDataSpaceCalculator::~AcquisitionDataSpaceCalculator() = default;

    auto AcquisitionDataSpaceCalculator::Clear() -> void {
        d.reset(new Impl);
    }

    auto AcquisitionDataSpaceCalculator::SetPixels(const int32_t& pixelX, const int32_t& pixelY) -> void {
        d->pixels = pixelX * pixelY;
    }

    auto AcquisitionDataSpaceCalculator::SetPoints(const int32_t& points) -> void {
        d->points = points;
    }

    auto AcquisitionDataSpaceCalculator::SetModalityCount(const UseCase::AcqDataSpaceCal::Modality& modality, const int32_t& count) -> void {
        d->modalityCounts[modality] = count;
    }

    auto AcquisitionDataSpaceCalculator::SetSliceCount(const UseCase::AcqDataSpaceCal::Modality& modality, const int32_t& count) -> void {
        d->sliceCounts[modality] = count;
    }

    auto AcquisitionDataSpaceCalculator::GetRequiredSpace() const -> int64_t {
        QMap<Modality, int64_t> sizes;
        int64_t totalSize{};
        for (auto it = d->modalityCounts.cbegin(); it != d->modalityCounts.cend(); ++it) {
            const auto& modality = it.key();
            const auto& modalityCount = it.value();

            sizes[modality] = modalityCount * d->GetBytesPerPixel(modality) * d->pixels * d->points * d->sliceCounts[modality];
            totalSize += sizes[modality];
        }

        return totalSize;
    }
}
