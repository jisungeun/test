#pragma once
#include <memory>

#include <IAcquisitionDataSpaceCalculator.h>
#include "HTX_Experiment_Perform_AcqDataSpaceCalculatorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::AcquisitionDataSpaceCalculator {
    class HTX_Experiment_Perform_AcqDataSpaceCalculator_API AcquisitionDataSpaceCalculator final : public UseCase::IAcquisitionDataSpaceCalculator {
    public:
        AcquisitionDataSpaceCalculator();
        ~AcquisitionDataSpaceCalculator() override;

        auto Clear() -> void override;

        auto SetPixels(const int32_t& pixelX, const int32_t& pixelY) -> void override;
        auto SetPoints(const int32_t& points) -> void override;
        auto SetModalityCount(const UseCase::AcqDataSpaceCal::Modality& modality, const int32_t& count) -> void override;
        auto SetSliceCount(const UseCase::AcqDataSpaceCal::Modality& modality, const int32_t& count) -> void override;

        auto GetRequiredSpace() const -> int64_t override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
