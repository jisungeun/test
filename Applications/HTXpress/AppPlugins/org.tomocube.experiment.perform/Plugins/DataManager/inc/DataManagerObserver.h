#pragma once
#include <memory>
#include <QThread>

#include <IHTXDataManagerObserver.h>

#include "HTX_Experiment_Perform_DataManagerExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::DataManager {
    class HTX_Experiment_Perform_DataManager_API DataManagerObserver : public AppComponents::HTXDataManager::IHTXDataManagerObserver {
        Q_OBJECT
    public:
        DataManagerObserver();
        ~DataManagerObserver() override;

        auto UpdateExperiment(const QString& user, const QString& project, const QString& experiment)->void override;
        auto WatchedAddData(const QString& fileFullPath)->void override;
        auto WatchedDeleteData(const QString& fileFullPath)->void override;
        auto WatchedUpdateData(const QString& fileFullPath)->void override;
        auto WatchedDeleteDataRootFolder(const QString& fileFullPath) -> void override;
    };
}
