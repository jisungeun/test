#pragma once
#include <memory>
#include <QThread>

#include <IDataManager.h>
//#include <IDataOutputPort.h>
#include "HTX_Experiment_Perform_DataManagerExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::DataManager {
    class HTX_Experiment_Perform_DataManager_API DataManager : public QObject, public UseCase::IDataManager {
        Q_OBJECT
    public:
        DataManager();
        ~DataManager() override;

        auto SetScanExperiment(const QString& user, const QString& project, const QString& experiment)->void override;

        auto SetOutputPort(UseCase::IDataOutputPort* outputPort)->void override;
        auto InstallDataScannerOutputPort(UseCase::IDataOutputPort* outputPort) -> void override;
        auto UninstallDataScannerOutputPort(UseCase::IDataOutputPort* outputPort) -> void override;
        auto InstallDataMonitorOutputPort(UseCase::IDataOutputPort* outputPort)->void override;
        auto UninstallDataMonitorOutputPort(UseCase::IDataOutputPort* outputPort)->void override;

    private slots:
        void onScanData(const QString& user, const QString& project, const QString& experiment);
        void onAddData(const QString& fileFullPath);
        void onDeleteData(const QString& fileFullPath);
        void onUpdateData(const QString& fileFullPath);
        void onDeleteDataRootFolder(const QString& fileFullPath);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
