#define LOGGER_TAG "[DataManagerUpdater]"

#include <TCLogger.h>
#include "DataManagerUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::DataManager {
    struct DataManagerUpdater::Impl {
        QList<AppComponents::HTXDataManager::IHTXDataManagerObserver*> observers;
    };

    DataManagerUpdater::DataManagerUpdater() : IHTXDataManagerUpdater(), d{ new Impl } {
    }

    DataManagerUpdater::~DataManagerUpdater() {
    }

    auto DataManagerUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new DataManagerUpdater() };
        return theInstance;
    }

    auto DataManagerUpdater::Register(AppComponents::HTXDataManager::IHTXDataManagerObserver* observer)->void {
        d->observers.push_back(observer);
    }

    auto DataManagerUpdater::Deregister(AppComponents::HTXDataManager::IHTXDataManagerObserver* observer) -> void {
        d->observers.removeOne(observer);
    }

    auto DataManagerUpdater::UpdateExperimentData(const QString& user, const QString& project, const QString& experiment) -> void {
        for (auto* observer : d->observers) {
            observer->UpdateExperiment(user, project, experiment);
        }
    }

    auto DataManagerUpdater::WatchedAddData(const QString& fileFullPath)->void {
        for (auto* observer : d->observers) {
            observer->WatchedAddData(fileFullPath);
        }
    }

    auto DataManagerUpdater::WatchedDeleteData(const QString& fileFullPath)->void {
        for (auto* observer : d->observers) {
            observer->WatchedDeleteData(fileFullPath);
        }
    }

    auto DataManagerUpdater::WatchedUpdateData(const QString& fileFullPath)->void {
        for (auto* observer : d->observers) {
            observer->WatchedUpdateData(fileFullPath);
        }
    }

    auto DataManagerUpdater::WatchedDeleteDataRootFolder(const QString& fileFullPath) -> void {
        for (auto observer : d->observers) {
            observer->WatchedDeleteDataRootFolder(fileFullPath);
        }
    }
}
