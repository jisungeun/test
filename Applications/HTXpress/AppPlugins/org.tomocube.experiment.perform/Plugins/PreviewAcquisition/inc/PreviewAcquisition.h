#pragma once
#include <memory>

#include <IPreviewAcquisition.h>
#include "HTX_Experiment_Perform_PreviewAcquisitionExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::PreviewAcquisition {
    class HTX_Experiment_Perform_PreviewAcquisition_API PreviewAcquisition : UseCase::IPreviewAcquisition {
    public:
        PreviewAcquisition();
        ~PreviewAcquisition();

        auto GetImagePort() const -> UseCase::IImagePort::Pointer override;

        auto SetMaximumFOV(AppEntity::Area area) -> void override;
        auto SetOverlapInUM(uint32_t overlap) -> void override;
        auto SetSingleMoveLimitUm(uint32_t distance) -> void override;
        auto SetArea(AppEntity::Position globalPos, AppEntity::Area area, AppEntity::Position wellPos) -> void override;
        auto StartAcquisition() const -> std::tuple<int32_t, int32_t, QList<Entity::PreviewUnitMove>> override;
        auto StopAcquisition() -> void override;

        auto IsFinished() const -> bool override;
        auto GetPreviewTile(int32_t& xPos, int32_t& yPos, QImage& image) const->bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}