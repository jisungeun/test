#pragma once
#include <memory>

#include <QThread>
#include <QImage>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::PreviewAcquisition {
    class Stitcher : public QThread {
    public:
        Stitcher();
        ~Stitcher() override;

        auto Start(const uint32_t countRows, const uint32_t countCols)->void;
        auto Stop()->void;
        auto IsFinished() const->bool;

        auto GetTile(int32_t& xPos, int32_t& yPos, QImage& image) const->bool;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}