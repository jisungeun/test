#define LOGGER_TAG "[PreviewAcquisition]"

#include <QDir>
#include <QRegularExpression>

#include <TCLogger.h>

#include <System.h>

#include "ImagePort.h"
#include "Stitcher.h"
#include "Planner.h"
#include "PreviewAcquisition.h"

#include <QCoreApplication>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::PreviewAcquisition {
    using Axis = AppEntity::Axis;

    struct PreviewAcquisition::Impl {
        ImagePort::Pointer imagePort{ new ImagePort() };
        Stitcher stitcher;
        Planner planner;

        AppEntity::Area maximumFOV;
        AppEntity::Position positionInWell;
        AppEntity::Area area;
        uint32_t overlap{ 0 };      //overlap between tiles in um

        auto ToPulse(const Axis axis, double positionMM) const -> int32_t;
    };

    auto PreviewAcquisition::Impl::ToPulse(const Axis axis, double positionMM) const -> int32_t {
        const auto model = AppEntity::System::GetModel();
        const auto config = AppEntity::System::GetSystemConfig();

        const auto resolution = model->AxisResolutionPPM(axis);
        const auto compensation = config->GetAxisCompensation(axis);

        return static_cast<int32_t>(resolution * positionMM * compensation);
    }

    PreviewAcquisition::PreviewAcquisition() : UseCase::IPreviewAcquisition(), d{new Impl} {
    }

    PreviewAcquisition::~PreviewAcquisition() {
        d->stitcher.Stop();
    }

    auto PreviewAcquisition::GetImagePort() const -> UseCase::IImagePort::Pointer {
        return d->imagePort;
    }

    auto PreviewAcquisition::SetMaximumFOV(AppEntity::Area area) -> void {
        d->maximumFOV = area;
        d->planner.SetFOV(d->ToPulse(Axis::X, area.toMM().width),
                          d->ToPulse(Axis::Y, area.toMM().height));
    }

    auto PreviewAcquisition::SetOverlapInUM(uint32_t overlap) -> void {
        d->overlap = overlap;
        d->planner.SetOverlap(d->ToPulse(Axis::X, overlap / 1000.0));
    }

    auto PreviewAcquisition::SetSingleMoveLimitUm(uint32_t distance) -> void {
        d->planner.SetSingleMoveLimit(d->ToPulse(Axis::X, distance / 1000.0));
    }

    auto PreviewAcquisition::SetArea(AppEntity::Position globalPos, AppEntity::Area area, AppEntity::Position wellPos) -> void {
        d->positionInWell = wellPos;
        d->area = area;
        d->planner.SetPreviewArea(d->ToPulse(Axis::X, globalPos.toMM().x),
                                  d->ToPulse(Axis::Y, globalPos.toMM().y),
                                  d->ToPulse(Axis::X, area.toMM().width),
                                  d->ToPulse(Axis::Y, area.toMM().height));
        d->planner.SetZPosition(d->ToPulse(Axis::Z, globalPos.toMM().z));
    }

    auto PreviewAcquisition::StartAcquisition() const -> std::tuple<int32_t, int32_t, QList<Entity::PreviewUnitMove>> {
        const auto [tiles, motions] = d->planner.GetMotions();
        d->stitcher.Start(tiles.rows, tiles.cols);
        return std::make_tuple(tiles.rows, tiles.cols, motions);
    }

    auto PreviewAcquisition::StopAcquisition() -> void {
        d->stitcher.Stop();
    }

    auto PreviewAcquisition::IsFinished() const -> bool {
        return d->stitcher.isFinished();
    }

    auto PreviewAcquisition::GetPreviewTile(int32_t& xPos, int32_t& yPos, QImage& image) const -> bool {
        return d->stitcher.GetTile(xPos, yPos, image);
    }
}
