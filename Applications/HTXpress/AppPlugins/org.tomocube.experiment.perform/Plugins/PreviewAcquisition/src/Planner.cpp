#include <QDebug>

#include <System.h>
#include <AppEntityDefines.h>
#include "Planner.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::PreviewAcquisition {
    using Axis = AppEntity::Axis;
    using UnitMove = Entity::PreviewUnitMove;

    struct Planner::Impl {
        struct {
            int32_t cxPulse, cyPulse;
            int32_t widthPulse, heightPulse;
        } previewArea;
        struct {
            int32_t widthPulse;
            int32_t heightPulse;
        } fov;
        int32_t zPosition;
        struct {
            int32_t singleMove;
            int32_t overlap;
        } condition;

        auto ToPulse(const Axis axis, double positionMM) const -> int32_t;
    };

    auto Planner::Impl::ToPulse(const Axis axis, double positionMM) const -> int32_t {
        const auto model = AppEntity::System::GetModel();
        const auto resolution = model->AxisResolutionPPM(axis);
        return static_cast<int32_t>(resolution * positionMM);
    }

    Planner::Planner() : d{new Impl} {
    }

    Planner::~Planner() {
    }

    auto Planner::SetPreviewArea(int32_t cx, int32_t cy, int32_t width, int32_t height) -> void {
        d->previewArea.cxPulse = cx;
        d->previewArea.cyPulse = cy;
        d->previewArea.widthPulse = width;
        d->previewArea.heightPulse = height;
    }

    auto Planner::SetFOV(int32_t width, int32_t height) -> void {
        d->fov.widthPulse = width;
        d->fov.heightPulse = height;
    }

    auto Planner::SetZPosition(int32_t pos) -> void {
        d->zPosition = pos;
    }

    auto Planner::SetSingleMoveLimit(int32_t distance) -> void {
        d->condition.singleMove = distance;
    }

    auto Planner::SetOverlap(int32_t distance) -> void {
        d->condition.overlap = distance;
    }

    auto Planner::GetMotions() const -> std::tuple<Tiles, QList<Entity::PreviewUnitMove>> {
        struct {
            int32_t x0{ 0 };
            int32_t y0{ 0 };
        } start, last;

        const auto overlapPulse = d->condition.overlap;
        const auto singleMove = d->condition.singleMove;

        struct TileSize {
            int32_t width;
            int32_t height;
        };
        const TileSize tileSize{d->fov.widthPulse - overlapPulse, d->fov.heightPulse - overlapPulse };
        const auto singleMoveLength = static_cast<int32_t>(std::floor(singleMove / tileSize.width) + 1) * tileSize.width + overlapPulse;

        Tiles tiles;
        tiles.cols = std::ceil((d->previewArea.widthPulse * 1.0) / tileSize.width);
        tiles.rows = std::ceil((d->previewArea.heightPulse * 1.0) / tileSize.height);
        tiles.totalWidth = tiles.cols * tileSize.width + overlapPulse;
        tiles.totalHeight = tiles.rows * tileSize.height + overlapPulse;
        qDebug() << "Tiles [Rows,Cols]=[" << tiles.rows << ", " << tiles.cols << "] "
                 << "Size [Width,Height]=[" << tileSize.width << ", " << tileSize.height << "] "
                 << "Total Size [Width,Height]=[" << tiles.totalWidth << ", " << tiles.totalHeight << "]";
        qDebug() << "Single Move Length=" << singleMoveLength;

        struct {
            int32_t cols;
            int32_t rows;
        } scanCount;

        scanCount.cols = std::ceil((tiles.totalWidth - overlapPulse) * 1.0 / (singleMoveLength - overlapPulse));
        scanCount.rows = std::ceil((tiles.totalHeight - overlapPulse) * 1.0 / tileSize.height);
        qDebug() << "Scan count [rows,cols]=[" << scanCount.rows << ", " << scanCount.cols << "]";

        start.x0 = static_cast<int32_t>(d->previewArea.cxPulse - (tiles.totalWidth / 2.0));
        start.y0 = static_cast<int32_t>(d->previewArea.cyPulse - (tiles.totalHeight / 2.0));
        last.x0 = start.x0;
        last.y0 = start.y0;

        QList<UnitMove> motions;
        bool forwardDirection = true;

        struct {
            int32_t x0, x1;
            int32_t y0, y1;
        } scanArea{0, 0, 0, 0};

        for(int32_t rowIdx=0; rowIdx<scanCount.rows; ++rowIdx) {
            const auto y0 = last.y0 + (d->fov.heightPulse/2.0);

            scanArea.y0 = last.y0;
            scanArea.y1 = std::min<int32_t>(scanArea.y0 + d->fov.heightPulse,
                                            d->previewArea.cyPulse + tiles.totalHeight/2.0);
            
            for(int32_t colIdx=0; colIdx<scanCount.cols; ++colIdx) {
                if (forwardDirection) {
                    scanArea.x0 = last.x0;
                    scanArea.x1 = std::min<int32_t>(scanArea.x0 + singleMoveLength, 
                                                    d->previewArea.cxPulse + tiles.totalWidth/2.0);
                } else {
                    scanArea.x0 = last.x0;
                    scanArea.x1 = std::max<int32_t>(scanArea.x0 - singleMoveLength, 
                                                    d->previewArea.cxPulse - tiles.totalWidth/2.0);
                }
                
                UnitMove motion;

                motion.motionStartXPulse = [=]()->int32_t {
                    if(forwardDirection) return scanArea.x0 + (d->fov.widthPulse / 2.0);
                    return scanArea.x0 - (d->fov.widthPulse / 2.0);
                }();
                motion.motionStartYPulse = y0;
                motion.motionStartZPulse = d->zPosition;

                motion.forwardDirection = forwardDirection;
                motion.triggerIntervalPulse = tileSize.width;
                motion.triggerCount = [=]()->int32_t {
                    const auto coverLength = std::abs(scanArea.x1 - scanArea.x0);
                    return std::ceil(coverLength / tileSize.width);
                }();

                qDebug() <<"[R,C]=[" << rowIdx << ", " << colIdx << "]"
                         <<"Last=[" << last.x0 << ", " << last.y0 << "] "
                         <<"ScanArea=[" << scanArea.x0 << ", " << scanArea.x1 << ", " << scanArea.y0 << ", " << scanArea.y1 << "] "
                         <<"Count=" << motion.triggerCount << " Interval=" << motion.triggerIntervalPulse;

                motions.push_back(motion);

                if(forwardDirection) {
                    last.x0 = scanArea.x1 - overlapPulse;
                } else {
                    last.x0 = scanArea.x1 + overlapPulse;
                }
            }

            last.x0 = scanArea.x1;
            last.y0 = scanArea.y1 - overlapPulse;
            forwardDirection = !forwardDirection;
        }

        return std::make_tuple(tiles, motions);
    }
}
