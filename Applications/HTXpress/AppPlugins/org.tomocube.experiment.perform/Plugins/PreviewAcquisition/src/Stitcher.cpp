#define LOGGER_TAG "[PreviewStitcher]"
#include <QMutex>

#pragma warning(push)
#pragma warning(disable: 4819)
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#pragma warning(pop)

#include <TCLogger.h>
#include <Image2DProc.h>
#include <ImageQueue.h>
#include <Image.h>

#include "Stitcher.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::PreviewAcquisition {
    struct TileImage {
        int32_t xPos;
        int32_t yPos;
        QImage image;
    };

    struct Stitcher::Impl {
        QMutex mutex;
        bool finished{ false };
        uint32_t countRows{ 1 };
        uint32_t countCols{ 1 };
        uint32_t countProcessed{ 0 };

        cv::Mat merged;
        QList<TileImage> tiles;

        auto AddTile(int32_t xPos, int32_t yPos, cv::Mat& tileImage)->void;
    };

    auto Stitcher::Impl::AddTile(int32_t xPos, int32_t yPos, cv::Mat& tileImage) -> void {
        QMutexLocker locker(&mutex);
        TileImage tile;
        tile.xPos = xPos;
        tile.yPos = yPos;
        tile.image = TC::Processing::Image2DProc::Convert(tileImage)->copy();
        tiles.push_back(tile);
    }

    Stitcher::Stitcher() : QThread(), d{new Impl} {
    }

    Stitcher::~Stitcher() {
    }

    auto Stitcher::Start(const uint32_t countRows, const uint32_t countCols) -> void {
        QMutexLocker locker(&d->mutex);
        d->finished = false;
        d->countProcessed = 0;
        d->countRows = countRows;
        d->countCols = countCols;
        ImageQueue::GetInstance()->Clear();
        d->tiles.clear();
        start();
    }

    auto Stitcher::Stop() -> void {
        QMutexLocker locker(&d->mutex);
        if(d->finished) {
            ImageQueue::GetInstance()->Clear();
            return;
        }

        d->finished = true;
        ImageQueue::GetInstance()->Clear();
        locker.unlock();

        wait();
    }

    auto Stitcher::IsFinished() const -> bool {
        QMutexLocker locker(&d->mutex);
        return d->finished;
    }

    auto Stitcher::GetTile(int32_t& xPos, int32_t& yPos, QImage& image) const -> bool {
        QMutexLocker locker(&d->mutex);
        if(d->tiles.isEmpty()) return false;
        xPos = d->tiles.at(0).xPos;
        yPos = d->tiles.at(0).yPos;
        image = d->tiles.at(0).image;
        d->tiles.pop_front();
        return true;
    }

    void Stitcher::run() {
        QLOG_INFO() << "Stitcher thread is started";

        const auto doStitching = (d->countRows * d->countCols) > 1;
        auto isFinished = [&]()->bool {
            return d->tiles.isEmpty() && (d->countProcessed == (d->countRows * d->countCols));
        };

        while(true) {
            QMutexLocker locker(&d->mutex);
            if(d->finished) break;
            if(isFinished()) {
                d->finished = true;
                break;
            }
            locker.unlock();

            auto imageQueue = ImageQueue::GetInstance();
            if(imageQueue->IsEmpty()) {
                imageQueue->Wait(100);
                continue;
            }

            auto rawImage = imageQueue->Pop();
            if(!rawImage) continue;

            if(doStitching) {
                cv::Mat cvImg(rawImage->GetSizeY(), 
                              rawImage->GetSizeX(), 
                              CV_8UC1, 
                              const_cast<uchar*>(rawImage->GetBuffer().get()), 
                              rawImage->GetSizeX());

                const auto inColId = d->countProcessed % d->countCols;
                const auto inRowId = d->countProcessed / d->countCols;
                const auto oddLine = (inRowId%2) == 0;

                const auto colIdx = oddLine ? inColId : (d->countCols - inColId - 1);
                const auto rowIdx = d->countRows - inRowId - 1;
                const auto startX = colIdx*rawImage->GetSizeX();
                const auto startY = rowIdx*rawImage->GetSizeY();

                d->AddTile(startX, startY, cvImg);
            } else {
                QLOG_INFO() << "No stitching is required";
                cv::Mat cvImg(rawImage->GetSizeY(), 
                              rawImage->GetSizeX(), 
                              CV_8UC1, 
                              const_cast<uchar*>(rawImage->GetBuffer().get()), 
                              rawImage->GetSizeX());
                d->AddTile(0, 0, cvImg);
            }

            d->countProcessed++;
        }

        QLOG_INFO() << "Stitcher thread is stopped";
    }
}
