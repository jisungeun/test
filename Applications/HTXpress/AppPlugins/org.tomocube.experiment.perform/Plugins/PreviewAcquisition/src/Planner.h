#pragma once
#include <memory>

#include "PreviewUnitMove.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::PreviewAcquisition {
    class Planner {
    public:
        struct Tiles {
            int32_t cols;
            int32_t rows;
            int32_t totalWidth;
            int32_t totalHeight;
        };

    public:
        Planner();
        ~Planner();

        auto SetPreviewArea(int32_t cx, int32_t cy, int32_t width, int32_t height)->void;
        auto SetFOV(int32_t width, int32_t height)->void;
        auto SetZPosition(int32_t pos)->void;
        auto SetSingleMoveLimit(int32_t distance)->void;
        auto SetOverlap(int32_t distance)->void;

        auto GetMotions() const->std::tuple<Tiles, QList<Entity::PreviewUnitMove>>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}