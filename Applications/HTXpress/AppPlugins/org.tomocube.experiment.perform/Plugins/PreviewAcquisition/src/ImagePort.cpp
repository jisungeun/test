#include "ImageQueue.h"
#include "ImagePort.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::PreviewAcquisition {
    struct ImagePort::Impl {
    };

    ImagePort::ImagePort() : UseCase::IImagePort(), d{new Impl} {
    }

    ImagePort::~ImagePort() {
    }

    auto ImagePort::Send(AppEntity::RawImage::Pointer image) -> void {
        ImageQueue::GetInstance()->Push(image);
    }

    auto ImagePort::Clear() -> void {
        ImageQueue::GetInstance()->Clear();
    }

    auto ImagePort::IsEmpty() const -> bool {
        return ImageQueue::GetInstance()->IsEmpty();
    }
}
