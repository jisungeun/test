#include <QDebug>

#include <catch2/catch.hpp>

#include <Planner.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::PreviewAcquisition::PlannerTest {
    TEST_CASE("Preview Acquisition Planner Test #1") {

        SECTION("GetMotions") {
            Planner planner;
            planner.SetPreviewArea(1000, 1000, 500, 500);
            planner.SetFOV(100, 100);
            planner.SetZPosition(10);
            planner.SetOverlap(10);
            planner.SetSingleMoveLimit(250);

            struct Expectation {
                int32_t startX;
                int32_t startY;
                int32_t startZ;
                bool forward;
                int32_t interval;
                int32_t count;
            };
            QList<Expectation> expectations = {
                {770,  770,  10, true,  90, 3},
                {1040, 770,  10, true,  90, 3},
                {1230, 860,  10, false, 90, 3},
                {960,  860,  10, false, 90, 3},
                {770,  950,  10, true,  90, 3},
                {1040, 950,  10, true,  90, 3},
                {1230, 1040, 10, false, 90, 3},
                {960,  1040, 10, false, 90, 3},
                {770,  1130, 10, true,  90, 3},
                {1040, 1130, 10, true,  90, 3},
                {1230, 1220, 10, false, 90, 3},
                {960,  1220, 10, false, 90, 3},
            };


            const auto [tiles, motions] = planner.GetMotions();

            int32_t idx = 0;
            for(const auto& motion : motions) {
                qDebug() << "[" << motion.motionStartXPulse << ", " << motion.motionStartYPulse << ", " << motion.motionStartZPulse << "] "
                         << "FWD=" << motion.forwardDirection << " Interval=" << motion.triggerIntervalPulse << " Count=" << motion.triggerCount;

                const auto& expectation = expectations.at(idx++);
                CHECK(motion.motionStartXPulse == expectation.startX);
                CHECK(motion.motionStartYPulse == expectation.startY);
                CHECK(motion.motionStartZPulse == expectation.startZ);
                CHECK(motion.forwardDirection == expectation.forward);
                CHECK(motion.triggerIntervalPulse == expectation.interval);
                CHECK(motion.triggerCount == expectation.count);
            }
        }
    }

    TEST_CASE("Preview Acquisition Planner Test #2") {
        SECTION("GetMotions") {
            Planner planner;
            planner.SetPreviewArea(1000, 1000, 500, 500);
            planner.SetFOV(115, 115);
            planner.SetZPosition(10);
            planner.SetOverlap(13);
            planner.SetSingleMoveLimit(152);

            struct Expectation {
                int32_t startX;
                int32_t startY;
                int32_t startZ;
                bool forward;
                int32_t interval;
                int32_t count;
            };
            QList<Expectation> expectations = {
                {789,   789,  10, true,  102, 2},
                {993,   789,  10, true,  102, 2},
                {1197,  789,  10, true,  102, 1},
                {1210,  891,  10, false, 102, 2},
                {1006,  891,  10, false, 102, 2},
                {802,   891,  10, false, 102, 1},
                {789,   993,  10, true,  102, 2},
                {993,   993,  10, true,  102, 2},
                {1197,  993,  10, true,  102, 1},
                {1210, 1095,  10, false, 102, 2},
                {1006, 1095,  10, false, 102, 2},
                {802,  1095,  10, false, 102, 1},
                {789,  1197,  10, true,  102, 2},
                {993,  1197,  10, true,  102, 2},
                {1197, 1197,  10, true,  102, 1},
            };


            const auto [tiles, motions] = planner.GetMotions();

            int32_t idx = 0;
            for(const auto& motion : motions) {
                qDebug() << "[" << motion.motionStartXPulse << ", " << motion.motionStartYPulse << ", " << motion.motionStartZPulse << "] "
                         << "FWD=" << motion.forwardDirection << " Interval=" << motion.triggerIntervalPulse << " Count=" << motion.triggerCount;

                const auto& expectation = expectations.at(idx++);
                CHECK(motion.motionStartXPulse == expectation.startX);
                CHECK(motion.motionStartYPulse == expectation.startY);
                CHECK(motion.motionStartZPulse == expectation.startZ);
                CHECK(motion.forwardDirection == expectation.forward);
                CHECK(motion.triggerIntervalPulse == expectation.interval);
                CHECK(motion.triggerCount == expectation.count);
            }
        }
    }
}

