#pragma once
#include <memory>
#include <QThread>

#include <RawImage.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::CondenserAutofocus {
    class ImageLogger : public QThread {
        Q_OBJECT

    public:
        using Pointer = std::shared_ptr<ImageLogger>;

    protected:
        ImageLogger();

    public:
        ~ImageLogger();

        static auto GetInstance()->Pointer;

        auto EnableLogging(const bool enable)->void;
        auto GetLogPath() const->QString;

        auto Push(AppEntity::RawImage::Pointer image)->void;
        auto Clear()->void;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}