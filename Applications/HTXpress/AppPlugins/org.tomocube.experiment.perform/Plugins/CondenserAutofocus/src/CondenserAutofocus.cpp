#define LOGGER_TAG "[CondenserAutofocus]"
#include <QThread>
#include <QFile>
#include <QTextStream>

#include "BestFocusFinder.h"
#include "ImagePort.h"
#include "ImageLogger.h"
#include "CondenserAutofocus.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::CondenserAutofocus {
    struct Plugin::Impl {
        ImagePort::Pointer imagePort{ new ImagePort() };
        QList<double> scores;
    };

    Plugin::Plugin() : ICondenserAutofocus(), d{new Impl} {
    }

    Plugin::~Plugin() {
    }

    auto Plugin::GetImagePort() const -> UseCase::IImagePort::Pointer {
        return d->imagePort;
    }

    auto Plugin::EnableLogging(const bool enable) -> void {
        ImageLogger::GetInstance()->EnableLogging(enable);
    }

    auto Plugin::SaveLog(const QString& message) -> void {
        auto path = ImageLogger::GetInstance()->GetLogPath();
        if(path.isEmpty()) return;

        QFile file(QString("%1/log.txt").arg(path));
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;

        QTextStream out(&file);
        out << message;
    }

    auto Plugin::FindBestFocus() const -> std::tuple<bool, int32_t> {
        auto finder = BestFocusFinder();
        finder.start();
        finder.wait();

        const auto found = finder.Found();
        const auto index = finder.GetIndex();

        ImageLogger::GetInstance()->Clear();

        d->scores = finder.GetValues();

        return std::make_tuple(found, index);
    }

    auto Plugin::GetScores() const -> QList<double> {
        return d->scores;
    }
}
