#pragma once
#include <memory>
#include <QThread>
#include <QList>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::CondenserAutofocus {
    class BestFocusFinder : public QThread {
        Q_OBJECT
    public:
        BestFocusFinder();
        ~BestFocusFinder() override;

        auto Found() const->bool;
        auto GetIndex() const->int32_t;
        auto GetValues() const->QList<double>;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}