#pragma warning(push)
#pragma warning(disable: 4819)
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#pragma warning(pop)

#include <QMutexLocker>
#include <QWaitCondition>
#include <QDir>
#include <QSettings>
#include <QStandardPaths>
#include <QDateTime>

#include <LicenseManager.h>

#include "ImageLogger.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::CondenserAutofocus {
    struct ImageLogger::Impl {
        QString loggingPath;
        int32_t loggingIndex{ 0 };

        QStringList paths;
        QList<AppEntity::RawImage::Pointer> images;
        QMutex mutex;
        QWaitCondition wc;
        bool running{ true };

        auto GetLoggingFolder() const->QString;
    };

    auto ImageLogger::Impl::GetLoggingFolder() const -> QString {
        auto hasLicense = LicenseManager::CheckFeature("Misc-Keep_CAF_Log");
        if(!hasLicense) return QString();

        return QSettings().value("Misc/CAF_Log_Folder").toString();
    }

    ImageLogger::ImageLogger() : d{new Impl} {
        start();
    }

    ImageLogger::~ImageLogger() {
        d->mutex.lock();
        d->running = false;
        d->wc.wakeAll();
        d->mutex.unlock();

        wait();
    }

    auto ImageLogger::GetInstance() -> Pointer {
        static Pointer theInstance{ new ImageLogger() };
        return theInstance;
    }

    auto ImageLogger::EnableLogging(const bool enable) -> void {
        QMutexLocker locker(&d->mutex);
        const auto accumulatedLogPath = d->GetLoggingFolder();

        //Keep the latest image
        if(accumulatedLogPath.isEmpty()) {
            if(enable) {
                const auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
                d->loggingPath = QString("%1/condenserCal").arg(appDataPath);
                if(!QFile::exists(d->loggingPath)) QDir().mkpath(d->loggingPath);
            } else {
                d->loggingPath.clear();
            }
            return;
        }

        const QDir dir(accumulatedLogPath);
        const auto timeStamp = QDateTime::currentDateTime().toString("yyyyMMdd.hhmmss");
        d->loggingPath = QString("%1/%2").arg(accumulatedLogPath).arg(timeStamp);
        dir.mkpath(d->loggingPath);
    }

    auto ImageLogger::GetLogPath() const -> QString {
        QMutexLocker locker(&d->mutex);
        return d->loggingPath;
    }

    auto ImageLogger::Push(AppEntity::RawImage::Pointer image) -> void {
        if(d->loggingPath.isEmpty()) return;

        QMutexLocker locker(&d->mutex);
        const auto path = QString("%1/%2.png").arg(d->loggingPath).arg(d->loggingIndex + 1, 3, 10, QLatin1Char('0'));
        d->loggingIndex++;

        d->paths.push_back(path);
        d->images.push_back(image);
        d->wc.wakeAll();
    }

    auto ImageLogger::Clear() -> void {
        QMutexLocker locker(&d->mutex);
        d->loggingIndex = 0;
    }

    void ImageLogger::run() {
        while(d->running) {
            QMutexLocker locker(&d->mutex);
            if(d->images.isEmpty()) {
                d->wc.wait(locker.mutex());
                continue;
            }

            const auto path = d->paths.front();
            const auto image = d->images.front();
            d->paths.pop_front();
            d->images.pop_front();

            locker.unlock();

            cv::Mat cvImg(image->GetSizeY(), 
                          image->GetSizeX(), 
                          CV_8UC1, 
                          const_cast<uchar*>(image->GetBuffer().get()), 
                          image->GetSizeX());
            cv::imwrite(path.toLocal8Bit().constData(), cvImg);
        }
    }
}
