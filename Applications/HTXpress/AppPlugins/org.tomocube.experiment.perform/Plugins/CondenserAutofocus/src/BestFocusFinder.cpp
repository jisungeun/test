#define LOGGER_TAG "[CondenserAutofocus]"

#include <any>
#include <QCoreApplication>
#include <QFile>
#include <QDir>
#include <QSettings>

#include <TCLogger.h>
#include <TCPython.h>

#include "ImageLogger.h"
#include "ImageQueue.h"
#include "BestFocusFinder.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::CondenserAutofocus {
    static bool firstPyLoad{ true };

    struct BestFocusFinder::Impl {
        TC::PythonModule::Pointer py{ TC::PythonModule::GetInstance() };

        const QString script{ "getCondenserFocusIndex.py" };
        QString scriptFullPath;
        QString loggingPath;

        bool found{ false };
        int32_t index{ 0 };
        QList<double> cafScore;

        auto SetupScriptPath()->void;
        auto SaveScores(const QString& title, std::shared_ptr<double[]>& values, int32_t count)->void;
    };

    auto BestFocusFinder::Impl::SetupScriptPath() -> void {
        scriptFullPath = QString("%1/pyscript/%2").arg(qApp->applicationDirPath()).arg(script);
        if(QFile::exists(scriptFullPath)) return;

        scriptFullPath = QString("%1/%2").arg(_PYSCRIPT_DIR).arg(script);
        if(QFile::exists(scriptFullPath)) return;

        scriptFullPath.clear();
    }

    auto BestFocusFinder::Impl::SaveScores(const QString& title, std::shared_ptr<double[]>& values,
                                           int32_t count) -> void {
        auto path = ImageLogger::GetInstance()->GetLogPath();
        if(path.isEmpty()) return;

        auto filePath = QString("%1/%2.txt").arg(path).arg(title);
        if(QFile::exists(filePath)) QFile::remove(filePath);

        QSettings qs(filePath, QSettings::IniFormat);

        qs.beginGroup(title);
        for(auto idx=0; idx<count; idx++) {
            qs.setValue(QString::number(idx), values[idx]);
        }
        qs.endGroup();
    }

    BestFocusFinder::BestFocusFinder() : QThread(), d{new Impl} {
        d->SetupScriptPath();

        if(firstPyLoad) {
            d->py->SetPythonHome("C:\\TomoPython");
            d->py->InitPython();
            d->py->ExecuteString(QString("sys.path.insert(0, '%1\\pyscript')").arg(qApp->applicationDirPath()));
            d->py->ExecuteString(QString("sys.path.insert(0, '%1')").arg(_PYSCRIPT_DIR));
            firstPyLoad = false;
        }
    }

    BestFocusFinder::~BestFocusFinder() {
    }

    auto BestFocusFinder::Found() const -> bool {
        return d->found;
    }

    auto BestFocusFinder::GetIndex() const -> int32_t {
        return d->index;
    }

    auto BestFocusFinder::GetValues() const -> QList<double> {
        return d->cafScore;
    }

    void BestFocusFinder::run() {
        if(d->scriptFullPath.isEmpty()) {
            QLOG_ERROR() << "It fails to find python script";
            return;
        }

        auto queue = ImageQueue::GetInstance();

        if((queue->ImageCount()%2 != 0) && (queue->ImageCount() < 4)) {
            QLOG_ERROR() << "There are only " << queue->ImageCount() << " images";
            return;
        }

        QList<AppEntity::RawImage::Pointer> ringImages, fullImages;
        while(!queue->IsEmpty()) {
            fullImages.push_back(queue->Pop());

            if(queue->IsEmpty()) break;
            ringImages.push_back(queue->Pop());
        }


        const uint32_t ringImagesCount = ringImages.count();
        const uint32_t fullImagesCount = fullImages.count();
        const uint32_t images = ringImagesCount;

        if((ringImagesCount != fullImagesCount) || (images == 0)) {
            QLOG_ERROR() << "Image counts are invalid [ring=" << ringImagesCount << ", full=" << fullImagesCount << "]";
            return;
        }

        const uint32_t sizeX = ringImages.at(0)->GetSizeX();
        const uint32_t sizeY = ringImages.at(0)->GetSizeY();

        uint32_t dims[] = {images, sizeY, sizeX};
        const auto imageSize = dims[1] * dims[2];
        const auto arraySize = imageSize * dims[0];

        auto ringImagesBuffer = std::make_unique<char[]>(arraySize);
        auto fullImagesBuffer = std::make_unique<char[]>(arraySize);

        for(auto idx=0u; idx<dims[0]; idx++) {
            memcpy_s(ringImagesBuffer.get() + (imageSize*idx), imageSize, ringImages[idx]->GetBuffer().get(), imageSize);
            memcpy_s(fullImagesBuffer.get() + (imageSize*idx), imageSize, fullImages[idx]->GetBuffer().get(), imageSize);
        }

        try {
            d->py->CopyArrToPy(ringImagesBuffer.get(), 3, dims, "ringImgListing", TC::ArrType::arrUBYTE, TC::ArrType::arrUBYTE);
            d->py->CopyArrToPy(fullImagesBuffer.get(), 3, dims, "fullMoonImgListing", TC::ArrType::arrUBYTE, TC::ArrType::arrUBYTE);

            if(!d->py->ExecutePyFile(d->scriptFullPath)) {
                QLOG_ERROR() << "It fails to run the script : " << d->script;
            } else {
                std::any cvStore, cvMinIndex, meanStore, meanIntensityStore;
                std::any cafScoreStore, cafScore, cafInd;

                auto[cvStoreDim, cvStoreType] = d->py->CopyArrToCpp(cvStore, "cvStore");
                auto[cvMinIndexDim, cvMinIndexType] = d->py->CopyArrToCpp(cvMinIndex, "cvMinInd");
                auto[meanStoreDim, meanStoreType] = d->py->CopyArrToCpp(meanStore, "meanStore");
                auto[meanIntensityStoreDim, meanIntensityStoreType] = d->py->CopyArrToCpp(meanIntensityStore, "meanIntensityStore");
                auto[cafScoreStoreDim, cafScoreStoreType] = d->py->CopyArrToCpp(cafScoreStore, "cafScoreStore");
                auto[cafScoreDim, cafScoreType] = d->py->CopyArrToCpp(cafScore,"cafScore");
                auto[cafIndDim, cafIndType] = d->py->CopyArrToCpp(cafInd, "cafInd");

                d->found = true;
                d->index = std::any_cast<std::shared_ptr<int32_t[]>>(cafInd)[0];
                auto cafScores = std::any_cast<std::shared_ptr<double[]>>(cafScoreStore);

                QLOG_INFO() << "CAF Index = " << d->index << " (0-based)";
                QLOG_INFO() << "CAF Score = " << std::any_cast<std::shared_ptr<double[]>>(cafScores)[d->index];
                QLOG_INFO() << "CAF Scores = [" << [&]()->QString {
                    QString str;
                    for(auto idx=0u; idx<images; idx++) str.push_back(QString("%1 ").arg(cafScores[idx]));
                    return str;
                }() << "]";

                auto cvStoreArr = std::any_cast<std::shared_ptr<double[]>>(cvStore);
                QLOG_INFO() << "CV Scores = [" << [&]()->QString {
                    QString str;
                    for(auto idx=0u; idx<images; idx++) str.push_back(QString("%1 ").arg(cvStoreArr[idx]));
                    return str;
                }() << "]";

                auto meanStoreArr = std::any_cast<std::shared_ptr<double[]>>(meanStore);
                QLOG_INFO() << "Mean Scores = [" << [&]()->QString {
                    QString str;
                    for(auto idx=0u; idx<images; idx++) str.push_back(QString("%1 ").arg(meanStoreArr[idx]));
                    return str;
                }() << "]";

                auto meanIntensityStoreArr = std::any_cast<std::shared_ptr<double[]>>(meanIntensityStore);
                QLOG_INFO() << "Mean Intensity = [" << [&]()->QString {
                    QString str;
                    for(auto idx=0u; idx<images; idx++) str.push_back(QString("%1 ").arg(meanIntensityStoreArr[idx]));
                    return str;
                }() << "]";

                d->cafScore.clear();
                for(auto idx=0u; idx<images; idx++) {
                    d->cafScore.push_back(cafScores[idx]);
                }

                d->SaveScores("CAFScore", cafScores, images);
                d->SaveScores("CVScore", cvStoreArr, images);
                d->SaveScores("MeanScore", meanStoreArr, images);
                d->SaveScores("MeanIntensity", meanIntensityStoreArr, images);
            }
        } catch(std::exception& ex) {
            QLOG_ERROR() << ex.what();
        } catch(...) {
            QLOG_ERROR() << "Exception while loading python module or running python scrip";
        }

        //For the case where images remain for unknown reason...
        queue->Clear();
    }
}