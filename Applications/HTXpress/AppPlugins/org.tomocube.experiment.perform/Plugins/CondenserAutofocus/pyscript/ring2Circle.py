# -*- coding: utf-8 -*-
def ring2Circle(ringImg):
    import numpy as np
    
    for ii in range(ringImg.shape[0]):
        ind01 = next((i for i, x in enumerate(ringImg[ii,:]) if x==1.0), None)
        if ind01 != None:
            ind02 = next((i for i, x in reversed(list(enumerate(ringImg[ii,:]))) if x==1.0), None)
            ringImg[ii, ind01:ind02] = 1 
    return ringImg