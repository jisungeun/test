# -*- coding: utf-8 -*-
def getCondenserFocusIndex():
    import numpy as np
    import ring2Circle
    
    global cvStore
    global cvMinInd
    global meanStore
    global meanIntensityStore
    global cafScoreStore
    global cafScore
    global cafInd
    
    #generate MIP of ring images
    ringMIPImg = ringImgListing.max(0)
    maxv = np.max(ringMIPImg)
    ringMIPImg = ringMIPImg / maxv
    
    #generate mask
    ringMask = ringMIPImg
    ringMask[ringMIPImg < 0.5] = 0
    ringMask[ringMask > 0] = 1
        
    ringMask = ring2Circle.ring2Circle(ringMask)
    indROI = np.nonzero(ringMask)
    
    #generate mask of disk
    diskMIPImg = fullMoonImgListing.max(0)
    maxv = np.max(diskMIPImg)
    diskMask = diskMIPImg / maxv
    diskMask[diskMask < 0.44] = 0
    diskMask[diskMask > 0] = 1
    
    #compensate ring mask
    ringMask = (ringMask * diskMask) / 2
    
    #normalize fullmoon images
    ringMaskStack = np.repeat(ringMask[np.newaxis, :,:], fullMoonImgListing.shape[0], axis=0)
    fullMoonStack = fullMoonImgListing * ringMaskStack
    maxv = np.max(fullMoonStack)
    fullMoonStack = fullMoonStack / maxv
        
    #calculate
    cvStore = np.zeros(fullMoonStack.shape[0])
    meanStore = np.zeros(fullMoonStack.shape[0])
    meanIntensityStore = np.zeros(fullMoonStack.shape[0])
    cvMinInd = np.zeros(1, dtype=np.int32)
    for idx in range(fullMoonStack.shape[0]):
        img = fullMoonStack[idx,:,:]
        meanv = np.mean(img[indROI])
        stdv = np.std(img[indROI])
        cvStore[idx] = stdv / meanv
        meanStore[idx] = meanv
        meanIntensityStore[idx] = meanv * maxv * 2;
        
    brightnessScore = meanStore / np.max(meanStore)
    uniformityScore = 1 - cvStore
    
    #caf score of each FP image
    cafScoreStore = (brightnessScore + uniformityScore) / 2;
    
    #best score
    cafScore = np.zeros(1, dtype=np.float64)
    cafScore[0] = max(cafScoreStore)
    
    #index of best score
    cafInd = np.zeros(1, dtype=np.int32)
    cafInd[0] = next((index for index, item in enumerate(cafScoreStore) if item == cafScore), -1)
    
    minv = min(cvStore)
    cvMinInd[0] = next((index for index, item in enumerate(cvStore) if item == minv), -1)
    
    
def test(topDir):
    # -*- coding: utf-8 -*-
    import numpy as np
    import matplotlib.pyplot as plt
    import glob
    
    global ringImgListing
    global fullMoonImgListing

    fileIdx = 0
    files = glob.glob(topDir + '/*.png')

    stacks = int(len(files)/2)
    img = plt.imread(files[0])
    ringImgListing = np.zeros((stacks, img.shape[0], img.shape[1]))
    fullMoonImgListing = np.zeros((stacks, img.shape[0], img.shape[1]))

    for file in files:
        img = plt.imread(file)
        stackIdx = int(fileIdx/2)
        if fileIdx%2 == 1:
            ringImgListing[stackIdx,:,:] = img[:,:]*255
        else:
            fullMoonImgListing[stackIdx,:,:] = img[:,:]*255
        fileIdx = fileIdx + 1
    
    getCondenserFocusIndex()

if __name__ == '__main__':
    getCondenserFocusIndex()
    #test('C:/Users/admin/AppData/Local/TomoCube, Inc/TomoStudioX/condenserCal')