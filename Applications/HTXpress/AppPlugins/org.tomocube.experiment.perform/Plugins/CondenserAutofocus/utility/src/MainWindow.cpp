#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include <QImage>

#include "ImageQueue.h"
#include "BestFocusFinder.h"

#include "ui_mainwindow.h"
#include "MainWindowControl.h"
#include "MainWindow.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::CondenserAutofocus::App {
    struct MainWindow::Impl {
        MainWindowControl control;
        Ui::MainWindow ui;
        QStringList files;

        auto Convert(const QImage& image)->AppEntity::RawImage::Pointer;
    };

    auto MainWindow::Impl::Convert(const QImage& image) -> AppEntity::RawImage::Pointer {
        const auto size = image.size();

        auto buffer = std::shared_ptr<uint8_t[]>(new uint8_t[size.width() * size.height()]);
        memcpy_s(buffer.get(), size.width()*size.height(), image.bits(), size.width()*size.height());

        auto rawImage = std::make_shared<AppEntity::RawImage>(size.width(), size.height(), buffer);
        return rawImage;
    }

    MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{new Impl} {
        d->ui.setupUi(this);

        const auto& ver = QString("HTX Condenser Autofocus Test Tool");
        setWindowTitle(ver);

        d->ui.table->setColumnCount(1);
        d->ui.table->setHorizontalHeaderLabels({"Value"});
        d->ui.table->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        d->ui.table->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
        auto* header = d->ui.table->horizontalHeader();
        header->setSectionResizeMode(0, QHeaderView::Stretch);

        connect(d->ui.actionOpen_Directory_O, SIGNAL(triggered()), this, SLOT(onOpenDirectory()));
        connect(d->ui.actionQuit_Q, SIGNAL(triggered()), qApp, SLOT(quit()));
        connect(d->ui.table, SIGNAL(cellClicked(int,int)), this, SLOT(onValueSelected(int,int)));
    }

    MainWindow::~MainWindow() {
    }

    void MainWindow::onOpenDirectory() {
        using RawImage = AppEntity::RawImage;

        const auto str = QFileDialog::getExistingDirectory(this, "Select the directory where images are");
        if(str.isEmpty()) return;

        auto dir = QDir(str);
        const auto files = dir.entryList({"*.png"}, QDir::Filter::Files, QDir::SortFlag::Name);
        if(files.isEmpty()) {
            QMessageBox::warning(this, "Images", "No images exits");
            return;
        }

        d->ui.table->clearContents();
        d->ui.ringImageView->ClearImage();
        d->ui.fullMoonImageView->ClearImage();
        d->files.clear();

        auto queue = ImageQueue::GetInstance();
        for(auto file : files) {
            const auto filePath = QString("%1/%2").arg(str).arg(file);
            const auto image = QImage(filePath);
            queue->Push(d->Convert(image));
            d->files.push_back(filePath);
        }

        BestFocusFinder finder;
        finder.start();
        finder.wait();

        if(!finder.Found()) {
            QMessageBox::warning(this, "Auto focus", "It fails to find the best focus index");
            return;
        }

        const auto bestFocusIndex = finder.GetIndex();

        QMessageBox::information(this, "Auto focus", QString("Best focus index : %1").arg(bestFocusIndex + 1));
        auto values = finder.GetValues();
        auto count = files.size() / 2;

        d->ui.table->setRowCount(count);

        for(auto idx=0; idx<count; ++idx) {
            auto* item = new QTableWidgetItem(QString::number(values[idx]));
		    item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            d->ui.table->setItem(idx, 0, item);
        }

        d->ui.table->setCurrentIndex(d->ui.table->model()->index(bestFocusIndex, 0));
        onValueSelected(bestFocusIndex, 0);
    }

    void MainWindow::onValueSelected(int row, int col) {
        Q_UNUSED(col)

        const auto ringIdx = row * 2;
        const auto moonIdx = row * 2 + 1;

        if(ringIdx >= d->files.size()) return;
        if(moonIdx >= d->files.size()) return;

        d->ui.ringImageView->ShowImage(QImage(d->files.at(ringIdx)));
        d->ui.fullMoonImageView->ShowImage(QImage(d->files.at(moonIdx)));
    }
}
