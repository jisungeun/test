#pragma once
#include <memory>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::CondenserAutofocus::App {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}