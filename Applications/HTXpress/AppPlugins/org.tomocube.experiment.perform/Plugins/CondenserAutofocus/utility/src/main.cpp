#include <QApplication>
#include <QFile>
#include <QStandardPaths>

#include <TCLogger.h>

#include "MainWindow.h"

int main(int argc, char* argv[]) {
	QApplication app(argc, argv);

	app.setApplicationName("HTXCondenserAutofocusTool");
	app.setOrganizationName("Tomocube, Inc.");
	app.setOrganizationDomain("www.tomocube.com");

	QFile f(":qdarkstyle/style.qss");

	if (!f.exists()) {
		printf("Unable to set stylesheet, file not found\n");
	} else {
		f.open(QFile::ReadOnly | QFile::Text);
		QTextStream ts(&f);
		qApp->setStyleSheet(ts.readAll());
	}

	auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
	TC::Logger::Initialize(QString("%1/log/HTXCondenserAutofocus.log").arg(appDataPath), QString("%1/log/HTXCondenserAutofocusError.log").arg(appDataPath));
	TC::Logger::SetTraceLevel();

	HTXpress::AppPlugins::Experiment::Perform::Plugins::CondenserAutofocus::App::MainWindow w;
	w.show();

	return app.exec();
}
