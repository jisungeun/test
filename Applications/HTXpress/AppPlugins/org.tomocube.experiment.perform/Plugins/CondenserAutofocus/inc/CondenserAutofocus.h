#pragma once
#include <memory>
#include <QObject>

#include <ICondenserAutofocus.h>
#include "HTX_Experiment_Perform_CondenserAutofocusExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::CondenserAutofocus {
    class HTX_Experiment_Perform_CondenserAutofocus_API Plugin : QObject, UseCase::ICondenserAutofocus {
        Q_OBJECT

    public:
        Plugin();
        ~Plugin() override;

        auto GetImagePort() const -> UseCase::IImagePort::Pointer override;
        auto EnableLogging(const bool enable) -> void override;
        auto SaveLog(const QString& message) -> void override;
        auto FindBestFocus() const -> std::tuple<bool, int32_t> override;
        auto GetScores() const->QList<double> override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

