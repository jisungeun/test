#pragma once
#include <memory>
#include <QThread>

#include <IExperimentWriter.h>
#include "HTX_Experiment_Perform_ExperimentWriterExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentWriter {
    class HTX_Experiment_Perform_ExperimentWriter_API Writer : public QThread, public UseCase::IExperimentWriter {
        Q_OBJECT

    public:
        Writer();
        ~Writer() override;

        auto Write(AppEntity::Experiment::Pointer experiment, const QString& path) const -> bool override;
    };
}