#define LOGGER_TAG "[ExperimentWriter]"

#include <TCLogger.h>
#include <ExperimentWriter.h>
#include "ExperimentWriterPlugin.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ExperimentWriter {
    Writer::Writer() : UseCase::IExperimentWriter() {
    }

    Writer::~Writer() {
    }

    auto Writer::Write(AppEntity::Experiment::Pointer experiment, const QString& path) const -> bool {
        auto writer = HTXpress::AppComponents::ExperimentIO::ExperimentWriter();
        return writer.Write(path, experiment);
    }
}
