#pragma once
#include <memory>

#include <QString>
#include <QObject>

#include <ISystemStorageManager.h>
#include <ISystemStorageOutputPort.h>

#include "HTX_Experiment_Perform_SystemStorageManagerExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::SystemStorageManager {
    class HTX_Experiment_Perform_SystemStorageManager_API SystemStorageManager final : public QObject, public UseCase::ISystemStorageManager {
        Q_OBJECT
    public:
        using Self = SystemStorageManager;
        using Pointer = std::shared_ptr<Self>;

        explicit SystemStorageManager(QObject *parent = nullptr);
        ~SystemStorageManager() override;

        auto SetPath(const QString& path) -> void override;
        auto SetUpdateInterval(const int32_t& msec) -> void override;

        auto UpdateStorageInfo() -> void override;
        auto UpdateMinRequiredSpace(const int32_t& gb) -> void override;

        auto RegistOutputPort(UseCase::ISystemStorageOutputPort::Pointer output) -> void override;
        auto DeregistOutputPort(UseCase::ISystemStorageOutputPort::Pointer output) -> void override;

    private slots:
        void onUpdateSystemStorage(const int64_t& bytesTotal, const int64_t& bytesAvailable);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}