﻿#pragma once
#include <memory>
#include <IStorageInfoUpdater.h>
#include <IStorageInfoObserver.h>

#include "StorageInformation.h"
#include "StorageInfoObserver.h"
#include "HTX_Experiment_Perform_SystemStorageManagerExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::SystemStorageManager {
    class HTX_Experiment_Perform_SystemStorageManager_API StorageInfoUpdater final : public AppComponents::StorageInfoManager::IStorageInfoUpdater {
    public:
        using Self = StorageInfoUpdater;
        using Pointer = std::shared_ptr<Self>;

    protected:
        StorageInfoUpdater();

    public:
        ~StorageInfoUpdater() override;
        static auto GetInstance() -> Pointer;

        auto Regist(AppComponents::StorageInfoManager::IStorageInfoObserver* observer) -> void;
        auto Deregist(AppComponents::StorageInfoManager::IStorageInfoObserver* observer) -> void;

        auto UpdateStorageInfo(const AppComponents::StorageInfoManager::StorageInformation& storageInformation) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
