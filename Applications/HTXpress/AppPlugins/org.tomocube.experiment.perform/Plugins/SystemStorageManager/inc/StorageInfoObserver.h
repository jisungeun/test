﻿#pragma once
#include <memory>

#include <IStorageInfoObserver.h>

#include "HTX_Experiment_Perform_SystemStorageManagerExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::SystemStorageManager {
    class HTX_Experiment_Perform_SystemStorageManager_API StorageInfoObserver final
    : public QObject, public AppComponents::StorageInfoManager::IStorageInfoObserver {
        Q_OBJECT
    public:
        using Self = StorageInfoObserver;
        using Pointer = std::shared_ptr<Self>;

        StorageInfoObserver();
        ~StorageInfoObserver() override;

        auto UpdateStorageInfo(const AppComponents::StorageInfoManager::StorageInformation& storageInformation) -> void override;

    signals:
        void sigUpdateStorageInfo(const int64_t& bytesTotal, const int64_t& bytesAvailable);
    };
}
