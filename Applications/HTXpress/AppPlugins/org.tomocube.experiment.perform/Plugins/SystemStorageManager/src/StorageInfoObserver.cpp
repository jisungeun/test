﻿#include "StorageInfoObserver.h"
#include "StorageInfoUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::SystemStorageManager {
    StorageInfoObserver::StorageInfoObserver() {
        StorageInfoUpdater::GetInstance()->Regist(this);
    }

    StorageInfoObserver::~StorageInfoObserver() {
        StorageInfoUpdater::GetInstance()->Deregist(this);
    }

    auto StorageInfoObserver::UpdateStorageInfo(const AppComponents::StorageInfoManager::StorageInformation& storageInformation) -> void {
        const auto total = storageInformation.bytesAll;
        const auto free = storageInformation.bytesAvailable;
        emit sigUpdateStorageInfo(total, free);
    }
}
