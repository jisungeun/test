#define LOGGER_TAG "[SystemStorageManager]"

#include <TCLogger.h>
#include <ISystemStorageOutputPort.h>
#include <StorageInfoManager.h>

#include "SystemStorageManager.h"
#include "StorageInfoObserver.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::SystemStorageManager {
    using StorageInfo = AppComponents::StorageInfoManager::StorageInfoManager;
    using Information = AppComponents::StorageInfoManager::StorageInformation;
    using OutputPort = UseCase::ISystemStorageOutputPort;

    struct SystemStorageManager::Impl {
        explicit Impl(SystemStorageManager* self) : self(self){}
        SystemStorageManager* self{};
        StorageInfo::Pointer storageInfo{};
        QList<OutputPort::Pointer> outputPorts{};
        StorageInfoObserver::Pointer observer{};
    };

    SystemStorageManager::SystemStorageManager(QObject* parent) : QObject(parent), UseCase::ISystemStorageManager(), d{std::make_unique<Impl>(this)} {
        d->storageInfo = StorageInfo::GetInstance();
        d->observer = std::make_shared<StorageInfoObserver>();
        connect(d->observer.get(), &StorageInfoObserver::sigUpdateStorageInfo, this, &Self::onUpdateSystemStorage);
    }

    SystemStorageManager::~SystemStorageManager() {
    }

    auto SystemStorageManager::SetPath(const QString& path) -> void {
        d->storageInfo->SetPath(path);
    }

    auto SystemStorageManager::SetUpdateInterval(const int32_t& msec) -> void {
        d->storageInfo->SetInterval(msec);
    }

    auto SystemStorageManager::UpdateMinRequiredSpace(const int32_t& gb) -> void {
        for(const auto& output : d->outputPorts) {
            output->UpdateMinRequiredSpace(gb);
        }
    }

    auto SystemStorageManager::UpdateStorageInfo() -> void {
        const auto total = d->storageInfo->GetBytesTotal();
        const auto available = d->storageInfo->GetBytesAvailable();
        onUpdateSystemStorage(total, available);
    }

    auto SystemStorageManager::RegistOutputPort(UseCase::ISystemStorageOutputPort::Pointer output) -> void {
        d->outputPorts.push_back(output);
    }

    auto SystemStorageManager::DeregistOutputPort(UseCase::ISystemStorageOutputPort::Pointer output) -> void {
        d->outputPorts.removeOne(output);
    }
    
    void SystemStorageManager::onUpdateSystemStorage(const int64_t& bytesTotal, const int64_t& bytesAvailable) {
        for(const auto& output : d->outputPorts) {
            output->UpdateSystemStorageSpace(bytesTotal, bytesAvailable);
        }
    }
}
