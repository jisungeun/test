﻿#include "StorageInfoUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::SystemStorageManager {
    using Component = AppComponents::StorageInfoManager::StorageInformation;
    using Observer = AppComponents::StorageInfoManager::IStorageInfoObserver;

    struct StorageInfoUpdater::Impl {
        QList<Observer*> observers{};
    };

    StorageInfoUpdater::StorageInfoUpdater() : IStorageInfoUpdater(), d{std::make_unique<Impl>()} {
    }

    StorageInfoUpdater::~StorageInfoUpdater() {
    }

    auto StorageInfoUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new StorageInfoUpdater() };
        return theInstance;
    }

    auto StorageInfoUpdater::Regist(AppComponents::StorageInfoManager::IStorageInfoObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto StorageInfoUpdater::Deregist(AppComponents::StorageInfoManager::IStorageInfoObserver* observer) -> void {
        d->observers.removeOne(observer);
    }

    auto StorageInfoUpdater::UpdateStorageInfo(const AppComponents::StorageInfoManager::StorageInformation& storageInformation) -> void {
        for(const auto& observer : d->observers) {
            observer->UpdateStorageInfo(storageInformation);
        }
    }
}
