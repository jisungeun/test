#pragma once
#include <memory>
#include <QString>

#include "HTX_Experiment_Perform_ChannelConfigIOExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ChannelConfigIO {
    class HTX_Experiment_Perform_ChannelConfigIO_API Plugin {
    public:
        Plugin();
        ~Plugin();

        auto SetTopPath(const QString& path)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}