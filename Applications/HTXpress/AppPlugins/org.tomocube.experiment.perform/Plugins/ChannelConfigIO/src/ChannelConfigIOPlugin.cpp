#include "Reader.h"
#include "Writer.h"
#include "ChannelConfigIOPlugin.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::ChannelConfigIO {
    struct Plugin::Impl {
        Reader::Pointer reader{ new Reader() };
        Writer::Pointer writer{ new Writer() };
    };

    Plugin::Plugin() : d{new Impl} {
    }

    Plugin::~Plugin() {
    }

    auto Plugin::SetTopPath(const QString& path) -> void {
        d->reader->SetTopPath(path);
        d->writer->SetTopPath(path);
    }
}
