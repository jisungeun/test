#define LOGGER_TAG "[PreviewCalibrator]"
#include <QElapsedTimer>

#include <TCLogger.h>
#include <Image2DProc.h>

#include <System.h>

#include "ImagePort.h"
#include "ImageQueue.h"
#include "PreviewCalibrator.h"

#include <QCoreApplication>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::PreviewCalibrator {
    struct Plugin::Impl {
        ImagePort::Pointer imagePort{ new ImagePort() };
    };

    Plugin::Plugin() : UseCase::IPreviewCalibrator(), d{ new Impl } {
    }

    Plugin::~Plugin() {
    }

    auto Plugin::GetImagePort() const -> UseCase::IImagePort::Pointer {
        return d->imagePort;
    }

    auto Plugin::GetIntensity() const -> double {
        auto queue = ImageQueue::GetInstance();

        QElapsedTimer timer;
        timer.start();

        while (timer.elapsed() < 1000) {
            if (!queue->IsEmpty()) break;
        }

        if (queue->IsEmpty()) return -1;

        auto image = queue->Pop();
        if (!image) return -1;

        return TC::Processing::Image2DProc::CalculateAverage(image->GetBuffer(), image->GetSizeX(), image->GetSizeY());
    }

    auto Plugin::CalcExposureCoefficients(const QList<Pair>& values)->std::tuple<double, double> {
        const auto count = values.size();

        //Calculate the mean of x and y
        double xMean = 0.0;
        double yMean = 0.0;

        for (int idx = 0; idx < count; idx++) {
            xMean += values.at(idx).first;
            yMean += values.at(idx).second;
        }
        xMean /= count;
        yMean /= count;

        //Calculate the slope and intercept of the regression line
        double numerator = 0.0;
        double denominator = 0.0;
        for (int idx = 0; idx < count; idx++) {
            const auto xi = values.at(idx).first;
            const auto yi = values.at(idx).second;
            numerator += (xi - xMean) * (yi - yMean);
            denominator += (xi - xMean) * (xi - xMean);
        }

        //y(x) = px + q
        const auto coeffP = numerator / denominator;
        const auto coeffQ = yMean - coeffP * xMean;

        return std::make_tuple(coeffP, coeffQ);
    }

    auto Plugin::CalcGainCoefficients(const QList<Pair>& values)->std::tuple<double, double> {
        const auto count = values.size();

        double xSum{ 0 };       //sigma(xi)
        double ySum{ 0 };       //sigma(yi)
        double x2Sum{ 0 };      //sigma(x^2i)
        double xySum{ 0 };      //sigma(xi*yi)

        for (int idx = 0; idx < count; idx++) {
            const auto xi = values.at(idx).first;
            const auto yi = values.at(idx).second;

            xSum = xSum + xi;
            ySum = ySum + std::log(yi);
            x2Sum = x2Sum + std::pow<double>(xi, 2);
            xySum = xySum + xi * std::log(yi);
        }

        // y(z) = ae^bz
        const auto coeffA = std::exp(((x2Sum * ySum) - (xSum * xySum)) / ((count * x2Sum) - (xSum * xSum)));
        const auto coeffB = ((count * xySum) - (xSum * ySum)) / ((count * x2Sum) - (xSum * xSum));

        return std::make_tuple(coeffA, coeffB);
    }
}