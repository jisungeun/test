#pragma once
#include <memory>

#include <RawImage.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::PreviewCalibrator {
    class ImageQueue {
    public:
        using Pointer = std::shared_ptr<ImageQueue>;

    protected:
        ImageQueue();

    public:
        ~ImageQueue();

        static auto GetInstance()->Pointer;

        auto Push(AppEntity::RawImage::Pointer image)->void;
        auto Pop()->AppEntity::RawImage::Pointer;
        auto IsEmpty() const->bool;
        auto Wait(uint64_t timeout = 0) const->bool;
        auto Clear()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}