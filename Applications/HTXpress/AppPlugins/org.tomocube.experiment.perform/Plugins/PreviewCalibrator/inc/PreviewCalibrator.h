#pragma once
#include <memory>

#include <IPreviewCalibrator.h>
#include "HTX_Experiment_Perform_PreviewCalibratorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::PreviewCalibrator {
    class HTX_Experiment_Perform_PreviewCalibrator_API Plugin : UseCase::IPreviewCalibrator {
    public:
        Plugin();
        ~Plugin() override;

        auto GetImagePort() const -> UseCase::IImagePort::Pointer override;

        auto GetIntensity() const -> double override;
        auto CalcExposureCoefficients(const QList<Pair>& values)->std::tuple<double, double>;
        auto CalcGainCoefficients(const QList<Pair>& values)->std::tuple<double, double>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}