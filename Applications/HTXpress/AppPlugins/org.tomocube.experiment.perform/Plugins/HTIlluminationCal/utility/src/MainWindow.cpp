#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QImage>

#include "ImageQueue.h"
#include "AutoCalibrator.h"

#include "ui_mainwindow.h"
#include "MainWindowControl.h"
#include "MainWindow.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::HTIlluminationAutoCalibration::App {
    struct MainWindow::Impl {
        MainWindowControl control;
        Ui::MainWindow ui;
        QStringList files;

        QVector<QRgb> colors;

        auto Convert(const QImage& image)->AppEntity::RawImage::Pointer;
    };

    auto MainWindow::Impl::Convert(const QImage& image) -> AppEntity::RawImage::Pointer {
        const auto size = image.size();

        auto buffer = std::shared_ptr<uint8_t[]>(new uint8_t[size.width() * size.height()]);
        memcpy_s(buffer.get(), size.width()*size.height(), image.bits(), size.width()*size.height());

        auto rawImage = std::make_shared<AppEntity::RawImage>(size.width(), size.height(), buffer);
        return rawImage;
    }

    MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->colors.resize(256);
        for(auto idx=0; idx<255; idx++) {
            d->colors[idx] = qRgb(qRed(idx), qGreen(idx), qBlue(idx));
        }

        const auto& ver = QString("HTX HT Illumination Test Tool");
        setWindowTitle(ver);

        d->ui.table->setColumnCount(3);
        d->ui.table->setHorizontalHeaderLabels({"Set", "Index", "Count"});
        d->ui.table->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        d->ui.table->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
        d->ui.table->setColumnWidth(0, 20);
        auto* header = d->ui.table->horizontalHeader();
        header->setSectionResizeMode(2, QHeaderView::Stretch);

        connect(d->ui.actionOpen_Directory_O, SIGNAL(triggered()), this, SLOT(onOpenDirectory()));
        connect(d->ui.actionQuit_Q, SIGNAL(triggered()), qApp, SLOT(quit()));
        connect(d->ui.table, SIGNAL(cellClicked(int,int)), this, SLOT(onValueSelected(int,int)));
        connect(d->ui.showSaturatedChk, SIGNAL(clicked(bool)), this, SLOT(onShowSaturated(bool)));
    }

    MainWindow::~MainWindow() {
    }

    void MainWindow::onOpenDirectory() {
        using RawImage = AppEntity::RawImage;

        const auto str = QFileDialog::getExistingDirectory(this, "Select the directory where images are");
        if(str.isEmpty()) return;

        auto dir = QDir(str);
        const auto files = dir.entryList({"*.png"}, QDir::Filter::Files, QDir::SortFlag::Name);
        if(files.isEmpty()) {
            QMessageBox::warning(this, "Images", "No images exits");
            return;
        }

        d->ui.table->clearContents();
        d->ui.imageView->ClearImage();
        d->files.clear();

        int32_t prevSetIndex = -1;
        int32_t setCount = 0;

        auto queue = ImageQueue::GetInstance();
        for(auto file : files) {
            const auto filePath = QString("%1/%2").arg(str).arg(file);
            const auto image = QImage(filePath);
            queue->Push(d->Convert(image));
            d->files.push_back(filePath);

            const auto toks = file.split('_');
            const auto setIndex = toks.at(0).toInt();
            if(setIndex != prevSetIndex) {
                prevSetIndex = setIndex;
                setCount++;
            }
        }

        const auto imagesPerSet = files.size() / setCount;
        AutoCalibrator calibrator;
        calibrator.SetImages(setCount, imagesPerSet);
        calibrator.SetOffset(1, 0);
        calibrator.SetThreshold(d->ui.thresholdSpin->value());
        calibrator.SetProcessAll(true);
        calibrator.Start();

        const auto index = calibrator.GetResult();
        const auto& counts = calibrator.GetCounts();

        QMessageBox::information(this, "Calibration", QString("Found index : %1").arg(index + 1));

        d->ui.table->setRowCount(files.size());

        int32_t setIndex=-1;
        int32_t subIndex=0;

        for(auto idx=0; idx<files.size(); ++idx) {
            if(idx%imagesPerSet == 0) {
                setIndex++;
                subIndex = 0;
            }

            auto* item = new QTableWidgetItem(QString::number(setIndex+1));
		    item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            d->ui.table->setItem(idx, 0, item);

            item = new QTableWidgetItem(QString::number(subIndex+1));
		    item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            d->ui.table->setItem(idx, 1, item);

            item = new QTableWidgetItem(QString::number(counts[setIndex][subIndex]));
		    item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            d->ui.table->setItem(idx, 2, item);

            subIndex++;
        }
    }

    void MainWindow::onValueSelected(int row, int col) {
        Q_UNUSED(col)
        const auto show = d->ui.showSaturatedChk->isChecked();

        if(!show) {
            d->ui.imageView->ShowImage(QImage(d->files.at(row)));
        } else {
            auto image = QImage(d->files.at(row));
            image.setColorTable(d->colors);
            image = image.convertToFormat(QImage::Format_RGB30);

            for(auto y=0; y<image.height(); y++) {
                QRgb *line = reinterpret_cast<QRgb*>(image.scanLine(y));
                for(auto x=0; x<image.width(); x++) {
                    QRgb &rgb = line[x];
                    if(qRed(rgb) == 255) {
                        rgb = qRgba(qRed(255), qGreen(0), qBlue(0), qAlpha(rgb));
                    }
                }
            }

            d->ui.imageView->ShowImage(image);
        }
    }

    void MainWindow::onShowSaturated(bool checked) {
        Q_UNUSED(checked)
        auto items = d->ui.table->selectedItems();
        if(items.isEmpty()) return;

        onValueSelected(items.at(0)->row(), items.at(0)->column());
    }
}
