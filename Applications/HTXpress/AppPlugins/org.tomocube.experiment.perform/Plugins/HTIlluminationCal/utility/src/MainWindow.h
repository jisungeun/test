#pragma once
#include <memory>
#include <QMainWindow>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::HTIlluminationAutoCalibration::App {
    class MainWindow : public QMainWindow {
        Q_OBJECT

    public:
        explicit MainWindow(QWidget* parent = nullptr);
        ~MainWindow() override;

    protected slots:
        void onOpenDirectory();
        void onValueSelected(int row, int col);
        void onShowSaturated(bool checked);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}