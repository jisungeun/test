#pragma once
#include <memory>
#include <QObject>

#include <IHTIlluminationAutoCalibration.h>
#include "HTX_Experiment_Perform_HTIlluminationAutoCalibrationExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::HTIlluminationAutoCalibration {
    class HTX_Experiment_Perform_HTIlluminationAutoCalibration_API Plugin : UseCase::IHTIlluminationAutoCalibration {
    public:
        Plugin();
        ~Plugin() override;

        auto GetImagePort() const -> UseCase::IImagePort::Pointer override;
        auto SetImages(int32_t setCount, int32_t imagesPerSet) -> void override;
        auto SetOffset(int32_t imagesPerSlice, int32_t offset) -> void override;
        auto SetThreshold(int32_t threshold) -> void override;
        auto EnableLogging(const bool enable) -> void override;

        auto StartCalibrate() const -> void override;
        auto GetResult() const -> std::tuple<int32_t, std::shared_ptr<AppEntity::RawImage>> override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

