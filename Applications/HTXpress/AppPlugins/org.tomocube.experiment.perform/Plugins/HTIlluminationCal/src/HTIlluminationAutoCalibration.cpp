#define LOGGER_TAG "[HTIlluminationAutoCalibration]"
#include <QThread>

#include "ImageQueue.h"
#include "AutoCalibrator.h"
#include "ImagePort.h"
#include "ImageLogger.h"
#include "HTIlluminationAutoCalibration.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::HTIlluminationAutoCalibration {
    struct Plugin::Impl {
        ImagePort::Pointer imagePort{ new ImagePort() };
        AutoCalibrator calibrator;
        QString logPath;
    };

    Plugin::Plugin() : IHTIlluminationAutoCalibration(), d{new Impl} {
    }

    Plugin::~Plugin() {
    }

    auto Plugin::GetImagePort() const -> UseCase::IImagePort::Pointer {
        return d->imagePort;
    }

    auto Plugin::SetImages(int32_t setCount, int32_t imagesPerSet) -> void {
        d->calibrator.SetImages(setCount, imagesPerSet);
        ImageLogger::GetInstance()->SetSetImages(imagesPerSet);
    }

    auto Plugin::SetOffset(int32_t imagesPerSlice, int32_t offset) -> void {
        d->calibrator.SetOffset(imagesPerSlice, offset);
        ImageLogger::GetInstance()->SetOffset(imagesPerSlice, offset);
    }

    auto Plugin::SetThreshold(int32_t threshold) -> void {
        d->calibrator.SetThreshold(threshold);
    }

    auto Plugin::EnableLogging(const bool enable) -> void {
        ImageLogger::GetInstance()->EnableLogging(enable);
    }

    auto Plugin::StartCalibrate() const -> void {
        ImageQueue::GetInstance()->Clear();
        d->calibrator.Start();
    }

    auto Plugin::GetResult() const -> std::tuple<int32_t, std::shared_ptr<AppEntity::RawImage>> {
        return std::make_tuple(d->calibrator.GetResult(), d->calibrator.GetResultImage());
    }
}
