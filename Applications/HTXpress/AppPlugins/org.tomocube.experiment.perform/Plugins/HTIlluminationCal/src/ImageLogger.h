#pragma once
#include <memory>
#include <QThread>

#include <RawImage.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::HTIlluminationAutoCalibration {
    class ImageLogger : public QThread {
        Q_OBJECT

    public:
        using Pointer = std::shared_ptr<ImageLogger>;

    protected:
        ImageLogger();

    public:
        ~ImageLogger();

        static auto GetInstance()->Pointer;

        auto SetSetImages(int32_t imagesPerSet)->void;
        auto SetOffset(int32_t imagesPerSlice, int32_t offset)->void;
        auto EnableLogging(const bool enable)->void;
        auto Push(AppEntity::RawImage::Pointer image)->void;
        auto Clear()->void;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}