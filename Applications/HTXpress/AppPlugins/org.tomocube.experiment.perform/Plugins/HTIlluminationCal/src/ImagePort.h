#pragma once
#include <memory>

#include <IImagePort.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::HTIlluminationAutoCalibration {
    class ImagePort : public UseCase::IImagePort {
    public:
        ImagePort();
        ~ImagePort();

        auto Send(AppEntity::RawImage::Pointer image) -> void override;
        auto Clear() -> void override;
        auto IsEmpty() const -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}