#define LOGGER_TAG "[HTIlluminationAutoCalibrator]"

#include <any>
#include <QMutex>
#include <QDir>
#include <QMap>

#include <TCLogger.h>

#include "ImageQueue.h"
#include "AutoCalibrator.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::HTIlluminationAutoCalibration {
    struct AutoCalibrator::Impl {
        QString loggingPath;

        int32_t setCount{ 0 };
        int32_t imagesPerSet{ 0 };
        int32_t imagesPerSlice{ 0 };
        int32_t offset{ 0 };
        int32_t threshold{ 0 };
        bool processAll{ false };

        QMap<int32_t,QList<int32_t>> counts;

        QMutex mutex;
        bool running{ false };
        int32_t result{ -1 };
        std::shared_ptr<AppEntity::RawImage> resultImage;

        auto CellExist(const std::vector<double>& stdValues, double lowerThreshold)->bool;
    };

    auto AutoCalibrator::Impl::CellExist(const std::vector<double>& stdValues, double lowerThreshold) -> bool {
        auto minValue = std::min_element(stdValues.begin(), stdValues.end());
        auto maxValue = std::max_element(stdValues.begin(), stdValues.end());

        auto str = std::accumulate(stdValues.begin() + 1, stdValues.end(), std::to_string(stdValues[0]),
				[](const std::string& a, double b){
					return a + ", " + std::to_string(b);
				});
        QLOG_INFO() << "[" << str.c_str() << "]" << " min=" << *minValue << " max=" << *maxValue << " threshold=" << lowerThreshold;

        return std::abs(*maxValue - *minValue) > lowerThreshold;
    }

    AutoCalibrator::AutoCalibrator() : QThread(), d{new Impl} {
    }

    AutoCalibrator::~AutoCalibrator() {
        d->mutex.lock();
        d->running = false;
        d->mutex.unlock();

        wait();
    }

    auto AutoCalibrator::SetImages(int32_t setCount, int32_t imagesPerSet) -> void {
        d->setCount = setCount;
        d->imagesPerSet = imagesPerSet;
    }

    auto AutoCalibrator::SetOffset(int32_t imagesPerSlice, int32_t offset) -> void {
        d->imagesPerSlice = imagesPerSlice;
        d->offset = offset;
    }

    auto AutoCalibrator::SetThreshold(int32_t threshold) -> void {
        d->threshold = threshold;
    }

    auto AutoCalibrator::SetProcessAll(bool all) -> void {
        d->processAll = all;
    }

    auto AutoCalibrator::Start() -> void {
        QMutexLocker locker(&d->mutex);
        d->running = true;
        d->result = -1;
        d->counts.clear();
        start();
    }

    auto AutoCalibrator::GetResult() -> int32_t {
        wait();
        return d->result;
    }

    auto AutoCalibrator::GetResultImage() -> std::shared_ptr<AppEntity::RawImage> {
        wait();
        return d->resultImage;
    }

    auto AutoCalibrator::GetCounts() -> QMap<int32_t, QList<int32_t>> {
        return d->counts;
    }

    void AutoCalibrator::run() {
        int32_t saturatedCount{ 0 };
        std::vector<double> stdValues;

        int32_t index{ 0 };
        int32_t setIndex{ 0 };
        int32_t subIndex{ 0 };

        auto queue = ImageQueue::GetInstance();

        std::shared_ptr<AppEntity::RawImage> mipImage;

        stdValues.resize(d->imagesPerSet);

        do {
            QMutexLocker locker(&d->mutex);
            if(!d->running) break;
            locker.unlock();

            if(queue->IsEmpty()) {
                if(!queue->Wait(1000)) continue;
            }

            auto image = queue->Pop();
            if(image == nullptr) continue;

            const auto offset = index % d->imagesPerSlice;

            if(offset == d->offset) {
                const uint32_t sizeX = image->GetSizeX();
                const uint32_t sizeY = image->GetSizeY();
                const uint32_t imageSize = sizeX * sizeY;

                if(mipImage == nullptr) mipImage = image;

                auto* ptr = image->GetBuffer().get();
                auto* mipPtr = mipImage->GetBuffer().get();

                int64_t sum = 0;
                int32_t count = 0;
                for(auto idx=0u; idx<imageSize; idx++) {
                    const auto value = *(ptr+idx);
                    sum += value;
                    if(value > *(mipPtr+idx)) *(mipPtr+idx) = value;
                    count += value / 255;
                }

                d->counts[setIndex].push_back(count);
                saturatedCount += count;

                subIndex++;

                //calc standard deviation
                double sumVariance = 0;
                const auto meanIntensity = sum / static_cast<double>(imageSize);
                for(auto idx=0u; idx<imageSize; idx++) {
                    const auto value = *(ptr+idx);
                    sumVariance += std::pow(value - meanIntensity, 2);
                }
                stdValues[index] = std::sqrt(sumVariance / imageSize);
            }

            index = index + 1;
            if(index == d->imagesPerSet) {
                const auto cellExist = d->CellExist(stdValues, 0.2);

                QLOG_INFO() << "Set Index=" << setIndex << " Count=" << saturatedCount << " cellExist=" << cellExist;

                if((saturatedCount < d->threshold) && cellExist) {
                    if(d->result==-1) d->result = setIndex;
                    if(!d->processAll) break;
                }

                saturatedCount = 0;
                index = 0;
                subIndex = 0;
                setIndex = setIndex + 1;
                mipImage.reset();
            }

            if(setIndex == d->setCount) break;
        } while (true);

        d->resultImage = mipImage;

        //For the case where images remain for unknown reason...
        queue->Clear();
    }
}