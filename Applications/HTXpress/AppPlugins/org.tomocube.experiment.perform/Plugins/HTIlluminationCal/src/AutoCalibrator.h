#pragma once
#include <memory>
#include <QThread>
#include <QMap>

#include <RawImage.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::HTIlluminationAutoCalibration {
    class AutoCalibrator : public QThread {
        Q_OBJECT
    public:
        AutoCalibrator();
        ~AutoCalibrator() override;

        auto SetImages(int32_t setCount, int32_t imagesPerSet) -> void;
        auto SetOffset(int32_t imagesPerSlice, int32_t offset) -> void;
        auto SetThreshold(int32_t threshold) -> void;
        auto SetProcessAll(bool all) -> void;
        auto Start() -> void;

        auto GetResult() -> int32_t;
        auto GetResultImage() -> std::shared_ptr<AppEntity::RawImage>;
        auto GetCounts() -> QMap<int32_t,QList<int32_t>>;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}