#include <QMutex>
#include <QWaitCondition>

#include "ImageQueue.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::HTIlluminationAutoCalibration {
    struct ImageQueue::Impl {
        QMutex mutex;
        QWaitCondition waitCond;
        QList<AppEntity::RawImage::Pointer> images;
    };

    ImageQueue::ImageQueue() : d{new Impl} {
    }

    ImageQueue::~ImageQueue() {
        d->mutex.lock();
        d->waitCond.wakeAll();
        d->mutex.unlock();

    }

    auto ImageQueue::GetInstance() -> Pointer {
        static Pointer theInstance{ new ImageQueue() };
        return theInstance;
    }

    auto ImageQueue::Push(AppEntity::RawImage::Pointer image) -> void {
        QMutexLocker locker(&d->mutex);
        d->images.push_back(image);
        d->waitCond.wakeAll();
    }

    auto ImageQueue::Pop() -> AppEntity::RawImage::Pointer {
        QMutexLocker locker(&d->mutex);
        if(d->images.isEmpty()) return nullptr;

        auto image = d->images.first();
        d->images.pop_front();

        return image;
    }

    auto ImageQueue::IsEmpty() const -> bool {
        QMutexLocker locker(&d->mutex);
        return d->images.isEmpty();
    }

    auto ImageQueue::Wait(uint64_t timeout) const -> bool {
        QMutexLocker locker(&d->mutex);
        return d->waitCond.wait(&d->mutex, timeout);
    }

    auto ImageQueue::Clear() -> void {
        QMutexLocker locker(&d->mutex);
        d->images.clear();
        d->waitCond.wakeAll();
    }

    auto ImageQueue::ImageCount() const -> int32_t {
        QMutexLocker locker(&d->mutex);
        return d->images.size();
    }
}
