#pragma warning(push)
#pragma warning(disable: 4819)
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#pragma warning(pop)

#include <QMutexLocker>
#include <QWaitCondition>
#include <QDir>
#include <QSettings>
#include <QStandardPaths>

#include <LicenseManager.h>

#include "ImageLogger.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::HTIlluminationAutoCalibration {
    struct ImageLogger::Impl {
        QString loggingPath;
        int32_t loggingIndex{ 0 };
        int32_t setIndex{ 0 };
        int32_t imagesPerSet{ 0 };
        int32_t imagesPerSlice{ 0 };
        int32_t offset{ 0 };

        QStringList paths;
        QList<AppEntity::RawImage::Pointer> images;
        QMutex mutex;
        QWaitCondition wc;
        bool running{ true };

        auto GetLoggingFolder() const->QString;
    };

    auto ImageLogger::Impl::GetLoggingFolder() const -> QString {
        auto hasLicense = LicenseManager::CheckFeature("Misc-Keep_LEDCal_Log");
        if(!hasLicense) return QString();

        return QSettings().value("Misc/LEDCal_Log_Folder").toString();
    }

    ImageLogger::ImageLogger() : d{new Impl} {
        start();
    }

    ImageLogger::~ImageLogger() {
        d->mutex.lock();
        d->running = false;
        d->wc.wakeAll();
        d->mutex.unlock();

        wait();
    }

    auto ImageLogger::GetInstance() -> Pointer {
        static Pointer theInstance{ new ImageLogger() };
        return theInstance;
    }

    auto ImageLogger::SetSetImages(int32_t imagesPerSet) -> void {
        d->imagesPerSet = imagesPerSet;
    }

    auto ImageLogger::SetOffset(int32_t imagesPerSlice, int32_t offset) -> void {
        d->imagesPerSlice = imagesPerSlice;
        d->offset = offset;
    }

    auto ImageLogger::EnableLogging(const bool enable) -> void {
        QMutexLocker locker(&d->mutex);
        const auto accumulatedLogPath = d->GetLoggingFolder();

        d->setIndex = 0;

        //Keep the latest image
        if(accumulatedLogPath.isEmpty()) {
            //Clear old data
            if(!d->loggingPath.isEmpty()) {
                QDir dir(d->loggingPath);
                dir.removeRecursively();
            }

            if(enable) {
                const auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
                d->loggingPath = QString("%1/htIlluminationCal").arg(appDataPath);
                if(!QFile::exists(d->loggingPath)) {
                    QDir().mkpath(d->loggingPath);
                } else {
                     QDir dir(d->loggingPath);
                    dir.removeRecursively();
                    QDir().mkpath(d->loggingPath);
                }
            } else {
                d->loggingPath.clear();
            }
            return;
        }

        const QDir dir(accumulatedLogPath);
        const auto timeStamp = QDateTime::currentDateTime().toString("yyyyMMdd.hhmmss");
        d->loggingPath = QString("%1/%2").arg(accumulatedLogPath).arg(timeStamp);
        dir.mkpath(d->loggingPath);
    }

    auto ImageLogger::Push(AppEntity::RawImage::Pointer image) -> void {
        if(d->loggingPath.isEmpty()) return;

        const auto offset = d->loggingIndex % d->imagesPerSlice;
        if(offset == d->offset) {
            QMutexLocker locker(&d->mutex);
            
            const auto path = QString("%1/%2_%3.png").arg(d->loggingPath)
                                                     .arg(d->setIndex)
                                                     .arg(d->loggingIndex/d->imagesPerSlice + 1, 3, 10, QLatin1Char('0'));

            d->paths.push_back(path);
            d->images.push_back(image);
            d->wc.wakeAll();
        }

        d->loggingIndex++;
        if(d->loggingIndex == d->imagesPerSet) {
            d->setIndex += 1;
            d->loggingIndex = 0;
        }
    }

    auto ImageLogger::Clear() -> void {
        QMutexLocker locker(&d->mutex);
        d->loggingIndex = 0;
    }

    void ImageLogger::run() {
        while(d->running) {
            QMutexLocker locker(&d->mutex);
            if(d->images.isEmpty()) {
                d->wc.wait(locker.mutex());
                continue;
            }

            const auto path = d->paths.front();
            const auto image = d->images.front();
            d->paths.pop_front();
            d->images.pop_front();

            locker.unlock();

            cv::Mat cvImg(image->GetSizeY(), 
                          image->GetSizeX(), 
                          CV_8UC1, 
                          const_cast<uchar*>(image->GetBuffer().get()), 
                          image->GetSizeX());
            cv::imwrite(path.toLocal8Bit().constData(), cvImg);
        }
    }
}
