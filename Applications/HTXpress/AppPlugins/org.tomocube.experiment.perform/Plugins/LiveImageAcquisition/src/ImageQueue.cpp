#include <QList>
#include <QMutex>

#include "ImageQueue.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::LiveImageAcquisition {
    struct ImageQueue::Impl {
        QMutex mutex;
        QList<AppEntity::RawImage::Pointer> images;
        bool pause{ false };
    };

    ImageQueue::ImageQueue() : UseCase::IImagePort(), d{new Impl} {
    }

    ImageQueue::~ImageQueue() {
    }

    auto ImageQueue::Send(AppEntity::RawImage::Pointer image) -> void {
        QMutexLocker locker(&d->mutex);
        if(d->pause) return;
        d->images.push_back(image);
    }

    auto ImageQueue::Clear() -> void {
        QMutexLocker locker(&d->mutex);
        d->images.clear();
    }

    auto ImageQueue::IsEmpty() const -> bool {
        QMutexLocker locker(&d->mutex);
        return d->images.isEmpty();
    }

    auto ImageQueue::PopLatest() -> AppEntity::RawImage::Pointer {
        QMutexLocker locker(&d->mutex);
        if(d->images.isEmpty()) return nullptr;

        auto image = d->images.last();
        d->images.clear();

        return image;
    }

    auto ImageQueue::Pause() -> void {
        QMutexLocker locker(&d->mutex);
        d->pause = true;
        d->images.clear();
    }

    auto ImageQueue::Resume() -> void {
        QMutexLocker locker(&d->mutex);
        d->pause = false;
    }
}
