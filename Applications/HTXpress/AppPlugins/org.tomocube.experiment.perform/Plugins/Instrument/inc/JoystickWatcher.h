#pragma once
#include <memory>
#include <QThread>

#include <IInstrument.h>
#include "HTX_Experiment_Perform_InstrumentExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::Instrument {
    class HTX_Experiment_Perform_Instrument_API JoystickWatcher : public QThread {
    Q_OBJECT
    
    public:
        JoystickWatcher(QObject* parent = nullptr);
        ~JoystickWatcher() override;

        auto Start()->void;
        auto Stop()->void;

    protected:
        void run() override;
        
    signals:
        void sigStart(int axis, bool plusDirection);
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}