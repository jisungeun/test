#pragma once
#include <memory>

#include <IInstrument.h>
#include "HTX_Experiment_Perform_InstrumentExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::Instrument {
    class HTX_Experiment_Perform_Instrument_API Instrument : public UseCase::IInstrument {
    public:
        Instrument();
        ~Instrument() override;

        auto StartInitialize() -> bool override;
        auto GetInitializationProgress() const -> std::tuple<bool,double> override;
        auto IsInitialized() const -> bool override;
        auto UpdateConfiguration() -> bool override;

        auto CleanUp()->void;

        auto InstallImagePort(UseCase::IImagePort::Pointer port) -> void override;
        auto UninstallImagePort(UseCase::IImagePort::Pointer port) -> void override;
        auto ImageCountInBuffer() const -> int32_t override;

        auto ChangeFOV(int32_t xPixels, int32_t yPixels, int32_t& offsetX, int32_t& offsetY) -> bool override;
        auto ChangeFullFOV() -> bool override;
        auto ChangeLiveModality(AppEntity::Modality modality, int32_t channel) -> bool override;
        auto GetCurrentLiveModality() const -> std::tuple<AppEntity::Modality, int32_t> override;

        auto RunImagingSequence(const AppEntity::ImagingSequence::Pointer sequence,
                                const QList<AppEntity::PositionGroup>& positions,
                                const AppEntity::WellIndex startingWellIndex,
                                const double focusReadyMM,
                                const bool useMultiDishHolder) -> bool override;
        auto CheckSequenceProgress() const -> std::tuple<bool,double,AppEntity::Position> override;
        auto StopAcquisition() -> bool override;

        auto MoveAxis(const AppEntity::Position& target) -> bool override;
        auto MoveAxis(const AppEntity::Axis axis, const double targetMM) -> bool override;
        auto MoveZtoSafePos() -> bool override;
        auto CheckAxisMotion() const -> MotionStatus override;
        auto GetAxisPosition() const -> AppEntity::Position override;
        auto GetAxisPositionMM(const AppEntity::Axis axis) const -> double override;

        auto StartJogAxis(const AppEntity::Axis axis, bool plusDirection) -> bool override;
        auto StopJogAxis() -> bool override;

        auto SetJogRange(const AppEntity::Axis axis, const double minPosMM, const double maxPosMM) -> void override;
        auto EnableJoystick() -> bool override;
        auto DisableJoystick() -> bool override;
        auto CheckJoystick() -> JoystickStatus override;

        auto StartLive(AppEntity::Modality modality, int32_t channel, bool onlyUpdateParameter = false) -> bool override;
        auto StopLive() -> bool override;
        auto ResumeLive() -> bool override;

        auto CapturePreviews(int32_t count, double gain)->bool override;
        auto StartPreviewAcquisition(const QList<Entity::PreviewUnitMove>& moves, double bfExposure) -> bool override;

        auto EnableAutoFocus() -> std::tuple<bool,bool> override;
        auto DisableAutoFocus() -> bool override;
        auto AutoFocusEnabled() const -> bool override;
        auto PerformAutoFocus() -> std::tuple<bool,bool> override;
        auto ReadAFSensorValue() -> int32_t override;
        auto SetBestFocusCurrent(int32_t& value) -> bool override;
        auto SetBestFocus(int32_t value) -> bool override;
        auto ScanFocus(double startMm, double distMm, int32_t count) -> QList<int32_t> override;

        auto PerformCondenserAutoFocus(UseCase::IImagePort::Pointer port, int32_t patternIndex, int32_t intensity) -> bool override;
        auto CheckCondenserAutoFocusProgress() const -> std::tuple<bool,double> override;
        auto FinishCondenserAutoFocus(UseCase::IImagePort::Pointer port)->void override;

        auto PerformHTIlluminationCalibration(UseCase::IImagePort::Pointer port, const QList<int32_t>& intensityList) -> bool override;
        auto CheckHTIlluminationCalibrationProgress() const -> std::tuple<bool, double> override;
        auto FinishHTIlluminationCalibration(UseCase::IImagePort::Pointer port) -> void override;
        auto ShowHTIlluminationSetupPattern(int32_t patternIndex, int32_t intensity) -> bool override;

        auto SampleStageEncoderSupported() const -> bool override;

        auto GetErrorMessage() const -> QString override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
