#define LOGGER_TAG "[JoystickWatcher]"

#include <QMutex>

#include <TCLogger.h>

#include <IInstrument.h>
#include "JoystickWatcher.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::Instrument {
    struct JoystickWatcher::Impl {
        bool running{ true };
        QMutex mutex;
    };
    
    JoystickWatcher::JoystickWatcher(QObject* parent)
        : QThread(parent)
        , d{ std::make_unique<Impl>() } {
    }
    
    JoystickWatcher::~JoystickWatcher() {
        d->mutex.lock();
        if(!d->running) return;
        d->mutex.unlock();

        Stop();
    }

    auto JoystickWatcher::Start() -> void {
        start();
    }

    auto JoystickWatcher::Stop() -> void {
        d->mutex.lock();
        d->running = false;
        d->mutex.unlock();

        wait();
    }

    void JoystickWatcher::run() {
        using JoystickStatus = UseCase::IInstrument::JoystickStatus;

        auto* instrument = UseCase::IInstrument::GetInstance();

        while(d->running) {
            if(!instrument->IsInitialized()) {
                msleep(200);
                continue;
            }
            break;
        }

        while(d->running) {
            const auto [flag, axis] = instrument->CheckJoystick();
            if(flag == JoystickStatus::Hold) {
                msleep(200);
                continue;
            }

            const auto plusDirection = (flag == JoystickStatus::PlusDirection);

            QMutexLocker locker(&d->mutex);
            if(!d->running) break;
            emit sigStart(axis._to_integral(), plusDirection);
            msleep(200);
        }

        QLOG_INFO() << "Thread is finished";
    }
}