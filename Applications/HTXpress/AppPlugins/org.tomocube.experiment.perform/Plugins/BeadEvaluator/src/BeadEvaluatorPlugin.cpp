#include <QDir>
#include <QStringList>
#include <QList>

#include "Evaluator.h"
#include "BeadEvaluatorPlugin.h"

#include <QCoreApplication>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::BeadEvaluator {
    struct Center {
        int32_t x;
        int32_t y;
    };

    struct Plugin::Impl {
        UseCase::IQualityCheckOutputPort* output{ nullptr };
        QStringList dataList;
        QList<Center> centerList;
        QString outPath;
        QList<Entity::BeadScore> results;

        QString psfFolder;
        QString psfModule;
        QString evalModule;
        QString appPath;
        QString tempPath;
    };

    Plugin::Plugin() : UseCase::IBeadEvaluator(), d{ std::make_unique<Impl>() } {
    }

    Plugin::~Plugin() {
    }

    auto Plugin::Clear() -> void {
        d->dataList.clear();
        d->centerList.clear();
        d->outPath.clear();
        d->results.clear();
    }

    auto Plugin::InstallOutputPort(UseCase::IQualityCheckOutputPort* output) -> void {
        d->output = output;
    }

    auto Plugin::AddData(const QString& path, int32_t centerXInPixel, int32_t centerYInPixel) -> void {
        d->dataList.push_back(path);
        d->centerList.push_back({centerXInPixel, centerYInPixel});
    }

    auto Plugin::SetOutputPath(const QString& path) -> void {
        d->outPath = path;
    }

    auto Plugin::Evaluate() -> bool {
        d->results.clear();

        const auto count = d->dataList.size();
        for(auto idx=0; idx<count; ++idx) {
            auto evaluator{ Evaluator() };

            evaluator.SetPsfFolder(d->psfFolder);
            evaluator.SetModules(d->psfModule, d->evalModule);
            evaluator.SetEvaluationApp(d->appPath, d->tempPath);

            evaluator.SetSource(d->dataList.at(idx));
            evaluator.SetCenter(d->centerList.at(idx).x, d->centerList.at(idx).y);
            evaluator.SetOutputPath(QString("%1/%2").arg(d->outPath).arg(idx, 3, 10, QLatin1Char('0')));

            evaluator.start();

            while(!evaluator.wait(100)) {
                QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

                if(evaluator.IsFailed()) {
                    return false;
                }
            }

            d->results.append(evaluator.GetResult());

            if(d->output) d->output->UpdateEvaluationProgress((idx+1)*1.0/count);
        }

        return true;
    }

    auto Plugin::GetCount() const -> int32_t {
        return d->dataList.size();
    }

    auto Plugin::GetResult(int32_t index) const -> Entity::BeadScore {
        if(index>=d->results.size()) return Entity::BeadScore();
        return d->results.at(index);
    }

    auto Plugin::SetPSFFolder(const QString& path) -> void {
        d->psfFolder = path;
    }

    auto Plugin::SetMoudlePath(const QString& psfModule, const QString& evalModule) -> void {
        d->psfModule = psfModule;
        d->evalModule = evalModule;
    }

    auto Plugin::SetEvaluationApp(const QString& appPath, const QString& tempPath) -> void {
        d->appPath = appPath;
        d->tempPath = tempPath;
    }
}
