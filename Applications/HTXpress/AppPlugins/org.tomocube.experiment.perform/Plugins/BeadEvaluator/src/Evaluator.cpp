#include <QMutex>
#include <QDir>

#include <BeadEvaluationMatlabApp.h>

#include "Evaluator.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::BeadEvaluator {
    struct Evaluator::Impl {
        QString psfFolder;
        QString psfModule;
        QString evalModule;
        QString appPath;
        QString tempPath;

        QString source;
        int32_t centerXInPixel{ 0 };
        int32_t centerYInPixel{ 0 };
        QString target;

        bool failed{ false };
        Entity::BeadScore result;

        QMutex mutex;
    };

    Evaluator::Evaluator(QObject* parent) : QThread(parent), d{ std::make_unique<Impl>() } {
    }

    Evaluator::~Evaluator() {
    }

    auto Evaluator::SetPsfFolder(const QString& psfFolder) -> void {
        d->psfFolder = psfFolder;
    }

    auto Evaluator::SetModules(const QString& psfModule, const QString& evalModule) -> void {
        d->psfModule = psfModule;
        d->evalModule = evalModule;
    }

    auto Evaluator::SetEvaluationApp(const QString& appPath, const QString& tempPath) -> void {
        d->appPath = appPath;
        d->tempPath = tempPath;
    }

    auto Evaluator::SetSource(const QString& path) -> void {
        d->source = path;
    }

    auto Evaluator::SetCenter(const int32_t& xInPixel, const int32_t& yInPixel) -> void {
        d->centerXInPixel = xInPixel;
        d->centerYInPixel = yInPixel;
    }

    auto Evaluator::SetOutputPath(const QString& path) -> void {
        d->target = path;
        QDir().mkpath(d->target);
    }

    auto Evaluator::IsFailed() const -> bool {
        QMutexLocker locker(&d->mutex);
        return d->failed;
    }

    auto Evaluator::GetResult() const -> Entity::BeadScore {
        QMutexLocker locker(&d->mutex);
        return d->result;
    }

    void Evaluator::run() {
        using App = TC::Processing::BeadEvaluation::BeadEvaluationMatlabApp;
        using Input = TC::Processing::BeadEvaluation::BeadEvaluationInput;
        using Result = TC::Processing::BeadEvaluation::BeadEvaluationResult;

        Input input;
        input.SetRootFolderPath(d->source);
        input.SetPSFFolderPath(d->psfFolder);
        input.SetPSFModuleFilePath(d->psfModule);
        input.SetBeadModuleFilePath(d->evalModule);
        input.SetBeadCenterPosition(d->centerXInPixel, d->centerYInPixel);
        input.SetOutputFolderPath(d->target);

        App app;
        app.SetInput(input);
        app.SetProcessingAppFilePath(d->appPath);
        app.SetProcessingTempFolderPath(d->tempPath);

        if(!app.Evaluate()) {
            QMutexLocker locker(&d->mutex);
            d->failed = true;
            return;
        }

        auto result = app.GetResult();

        d->result.SetVolume(result.GetVolume());
        d->result.SetDrymass(result.GetDryMass());
        d->result.SetCorrelation(result.GetCorrelation());
        d->result.SetMeanDeltaRI(result.GetMeanDeltaRI());
    }
}