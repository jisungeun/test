#pragma once
#include <memory>

#include <QThread>

#include <BeadScore.h>

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::BeadEvaluator {
    class Evaluator : public QThread {
        Q_OBJECT

    public:
        Evaluator(QObject* parent = nullptr);
        ~Evaluator() override;

        auto SetPsfFolder(const QString& psfFolder)->void;
        auto SetModules(const QString& psfModule, const QString& evalModule)->void;
        auto SetEvaluationApp(const QString& appPath, const QString& tempPath)->void;

        auto SetSource(const QString& path)->void;
        auto SetCenter(const int32_t& xInPixel, const int32_t& yInPixel)->void;
        auto SetOutputPath(const QString& path)->void;

        auto IsFailed() const->bool;
        auto GetResult() const->Entity::BeadScore;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}