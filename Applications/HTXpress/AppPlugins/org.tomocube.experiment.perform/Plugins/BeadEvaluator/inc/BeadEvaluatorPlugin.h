#pragma once
#include <memory>
#include <IBeadEvaluator.h>

#include "HTX_Experiment_Perform_BeadEvaluatorExport.h"

namespace HTXpress::AppPlugins::Experiment::Perform::Plugins::BeadEvaluator {
    class HTX_Experiment_Perform_BeadEvaluator_API Plugin : public UseCase::IBeadEvaluator {
    public:
        Plugin();
        ~Plugin() override;

        auto Clear() -> void override;
        auto InstallOutputPort(UseCase::IQualityCheckOutputPort* output) -> void override;
        auto AddData(const QString& path, int32_t centerXInPixel, int32_t centerYInPixel) -> void override;
        auto SetOutputPath(const QString& path) -> void override;
        auto Evaluate() -> bool override;
        auto GetCount() const -> int32_t override;
        auto GetResult(int32_t index) const -> Entity::BeadScore override;

        auto SetPSFFolder(const QString& path)->void;
        auto SetMoudlePath(const QString& psfModule, const QString& evalModule)->void;
        auto SetEvaluationApp(const QString& appPath, const QString& tempPath)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}