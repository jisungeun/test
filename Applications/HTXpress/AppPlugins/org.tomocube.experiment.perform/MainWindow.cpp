#define LOGGER_TAG "[MainWindow]"

#include <QTimer>

#include <TCLogger.h>
#include <MessageDialog.h>

#include <ProgressDialog.h>
#include <AppEvent.h>
#include <MenuEvent.h>
#include <SystemStatus.h>
#include <JoystickWatcher.h>

#include "Internal/LiveviewAnnotator.h"
#include "Internal/ExperimentSelectDialog.h"
#include "Internal/InstrumentObserver.h"
#include "Internal/ExperimentIOObserver.h"
#include "Internal/MotionObserver.h"
#include "Internal/SetupDialog.h"
#include "Internal/EvaluationDialog.h"
#include "MainWindowControl.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"

namespace HTXpress::AppPlugins::Experiment::Perform {
    static auto _id = qRegisterMetaType<AppEntity::WellIndex>("AppEntity::WellIndex");

    using JoystickWatcher = Plugins::Instrument::JoystickWatcher;

    struct MainWindow::Impl {
        Ui::MainWindow ui;
        MainWindowControl control;

        TC::ProgressDialog* instrumentProgress{ nullptr };

        InstrumentObserver* instrumentObserver{ nullptr };
        ExperimentIOObserver* experimentIOObserver{ nullptr };
        MotionObserver* motionObserver{ nullptr };

        JoystickWatcher* joystickWatcher{ nullptr };

        QVariantMap appProperties;

        struct {
            QString project;
            QString experiment;
        } info;

        struct {
            SetupDialog* setupDialog{ nullptr };
            EvaluationDialog* evalDialog{ nullptr };
        } service;

        auto SelectExperiment()->bool;

        auto CallApplication(TC::Framework::AppEvent& event, MainWindow* self) -> void;
    };

    auto MainWindow::Impl::SelectExperiment() -> bool {
        ExperimentSelectDialog dlg;
        dlg.setModal(true);
        if(dlg.exec() != QDialog::Accepted) {
            return false;
        }

        info.project = dlg.GetProject();
        info.experiment = dlg.GetExperiment();
        if(info.experiment.isEmpty()) {
            return false;
        }

        return true;
    }

    auto MainWindow::Impl::CallApplication(TC::Framework::AppEvent& event, MainWindow* self) -> void {
        ui.liveviewPanel->HideLiveControlDialog();
        self->publishSignal(event, "Experiment::Perform");
    }

    MainWindow::MainWindow(QWidget* parent) : IMainWindowHTX("Experiment Perform", "Experiment", parent), d{new Impl} {
        d->ui.setupUi(this);
        d->instrumentObserver = new InstrumentObserver(this);
        d->experimentIOObserver = new ExperimentIOObserver(this);
        d->motionObserver = new MotionObserver(this);
        d->joystickWatcher = new JoystickWatcher(this);

        auto annotator = LiveviewAnnotator::GetInstance();
        annotator->InstallViewer(d->ui.liveviewPanel);

        connect(d->instrumentObserver, SIGNAL(sigProgress(double, const QString&)), this, 
                SLOT(onUpdateProgress(double, const QString&)));
        connect(d->instrumentObserver, SIGNAL(sigFailed(const QString&)), this, SLOT(onInstrumentFailed(const QString&)));
        connect(d->experimentIOObserver, SIGNAL(sigError(const QString&)), this, SLOT(onLoadingExperimentFailed(const QString&)));
        connect(d->motionObserver, SIGNAL(sigReportError(const QString&)), this, SLOT(onReportMotionError(const QString&)));

        connect(d->ui.dataPanel, SIGNAL(sigEndExperiment()), this, SLOT(onEndExperiment()));
        connect(d->ui.dataPanel, SIGNAL(sigGotoExpSetup()), this, SLOT(onGotoExpSetup()));
        connect(d->ui.dataPanel, SIGNAL(sigGotoDataNavigation()), this, SLOT(onGotoDataNavigation()));
        connect(d->ui.controlPanel, SIGNAL(sigRunExperiment()), this, SLOT(onRunExperiment()));
        connect(d->ui.controlPanel, SIGNAL(sigRunExperimentSingle()), this, SLOT(onRunExperimentSingle()));

        connect(d->joystickWatcher, &JoystickWatcher::sigStart, this, [this](int axisNo, bool plusDirection) {
            auto axis = AppEntity::Axis::_from_integral(axisNo);
            d->control.StartJog(axis, plusDirection);
        });

        subscribeEvent("TabChange");
        connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnHandleEvent(ctkEvent)));
    }
    
    MainWindow::~MainWindow() {
        QLOG_INFO() << "Destructor is called";
        d->joystickWatcher->Stop();
        d->control.CleanUp();
    }

    void MainWindow::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
    }

    void MainWindow::onInitialize() {
        d->instrumentProgress->SetContent("Initialization");

        if(!d->control.LoadExperiment(d->info.project, d->info.experiment)) {
            d->instrumentProgress->hide();
            return;
        }

        if(!d->control.StartInitialize()) {
            d->instrumentProgress->hide();
            return;
        }

        d->control.ReadyToLoadVessel();

        auto serviceMode = AppEntity::SystemStatus::GetInstance()->GetServiceMode();
        if((serviceMode == +AppEntity::ServiceMode::Evaluation) && (d->service.evalDialog == nullptr)) {
            d->service.evalDialog = new EvaluationDialog(this);
            d->service.evalDialog->show();
        }

        if((serviceMode == +AppEntity::ServiceMode::Setup) && (d->service.setupDialog == nullptr)){
            d->service.setupDialog = new SetupDialog(this);
            d->service.setupDialog->show();
        }

        d->joystickWatcher->Start();
    }

    void MainWindow::onUpdateProgress(double progress, const QString& message) {
        if(d->instrumentProgress->isHidden()) {
            d->instrumentProgress->open();
            d->instrumentProgress->activateWindow();
        }

        if(!message.isEmpty()) d->instrumentProgress->SetContent(message);

        const auto step = std::min<int32_t>(progress*100, 100);
        d->instrumentProgress->setValue(step);

        if(step == 100) {
            d->instrumentProgress->hide();
        }
    }

    void MainWindow::onInstrumentFailed(const QString& message) {
        d->instrumentProgress->hide();
        d->instrumentProgress->setValue(0);
        TC::MessageDialog::warning(this, tr("Instrument"), tr("Instrument failed:\n%1").arg(message));
    }

    void MainWindow::onLoadingExperimentFailed(const QString& message) {
        TC::MessageDialog::warning(this, tr("Loading Experiment"), tr("Failed to load experiment:\n%1").arg(message));
        //TODO 수동으로 Experiment 파일을 열던가 Setup app으로 가는 옵션을 선택하기 위한 창 띄우기
    }

    void MainWindow::onReportMotionError(const QString& message) {
        TC::MessageDialog::warning(this, tr("Motion"), tr("Motion failure:\n%1").arg(message));
    }

    void MainWindow::onEndExperiment() {
        using StandardButton = TC::MessageDialog::StandardButton;

        if(d->control.IsExperimentRunning()) {
            auto selection = TC::MessageDialog::question(this, tr("Experiment"), tr("Stop the current experiment?"),
                                                         StandardButton::Yes | StandardButton::No, StandardButton::No);
            if(selection != StandardButton::Yes) return;
            //Todo implement stop experiment here
        }

        AppEntity::SystemStatus::GetInstance()->ResetExperiment();

        // windows title bar에 실험 이름 삭제
        TC::Framework::MenuEvent menuEvt(TC::Framework::MenuTypeEnum::TitleBar);
        menuEvt.scriptSet("");
        publishSignal(menuEvt, "Experiment::Perform");

        TC::Framework::AppEvent appEvt(TC::Framework::AppTypeEnum::APP_WITH_ARGS);
        appEvt.setFullName("org.tomocube.general.start");
        appEvt.setAppName("Start");

        d->CallApplication(appEvt, this);
    }

    void MainWindow::onGotoExpSetup() {
        TC::Framework::AppEvent appEvt(TC::Framework::AppTypeEnum::APP_WITH_ARGS);
        appEvt.setFullName("org.tomocube.experiment.setup");
        appEvt.setAppName("Experiment Setup");

        d->CallApplication(appEvt, this);
    }

    void MainWindow::onGotoDataNavigation() {
        TC::Framework::AppEvent appEvt(TC::Framework::AppTypeEnum::APP_WITH_ARGS);
        appEvt.setFullName("org.tomocube.data.navigation");
        appEvt.setAppName("Data Navigation");
        appEvt.addParameter("Project", d->info.project);
        appEvt.addParameter("Experiment", d->info.experiment);

        d->CallApplication(appEvt, this);
    }

    void MainWindow::onRunExperiment() {
        TC::Framework::AppEvent appEvt(TC::Framework::AppTypeEnum::APP_WITH_ARGS);
        appEvt.setFullName("org.tomocube.experiment.timelapse");
        appEvt.setAppName("Experiment Timelapse");
        appEvt.addParameter("Project", d->info.project);
        appEvt.addParameter("Experiment", d->info.experiment);

        QLOG_INFO() << "Run the experiment [" << d->info.project << "/" << d->info.experiment << "]";

        d->CallApplication(appEvt, this);

    }

    void MainWindow::onRunExperimentSingle() {
        TC::Framework::AppEvent appEvt(TC::Framework::AppTypeEnum::APP_WITH_ARGS);
        appEvt.setFullName("org.tomocube.experiment.timelapse");
        appEvt.setAppName("Experiment Timelapse");
        appEvt.addParameter("Project", d->info.project);
        appEvt.addParameter("Experiment", d->info.experiment);
        appEvt.addParameter("SingleRun", "true");

        QLOG_INFO() << "Run the experiment (single) [" << d->info.project << "/" << d->info.experiment << "]";

        d->CallApplication(appEvt, this);

    }

    auto MainWindow::TryActivate() -> bool {
        return true;
    }

    auto MainWindow::TryDeactivate() -> bool {
        return true;
    }

    auto MainWindow::IsActivate() -> bool {
        return true;
    }

    auto MainWindow::GetMetaInfo() -> QVariantMap {
        return d->appProperties;
    }

    auto MainWindow::Execute(const QVariantMap& params) -> bool {
        const auto experimentExist = AppEntity::SystemStatus::GetInstance()->IsExperimentLoaded();
        if(experimentExist) {
            QLOG_INFO() << "Previous experiment exists, so it does not perform initialization";
            return true;
        }

        const auto hasExperiment = params.contains("Project") && params.contains("Experiment");
        if(hasExperiment) {
            d->info.project = params["Project"].toString();
            d->info.experiment = params["Experiment"].toString();
        } else if (d->info.experiment.isEmpty()) {
            using namespace TC::Framework;

            AppEvent appEvt(AppTypeEnum::APP_WITHOUT_ARG);
            appEvt.setFullName("org.tomocube.experiment.setup");
            appEvt.setAppName("Experiment Setup");

            d->CallApplication(appEvt, this);

            TC::MessageDialog::warning(this, tr("Experiment"), tr("Select Experiment"));

            return true;
        } else {
            //It is already initialized and it is called again, so do nothing...
            return true;
        }

        if(!d->instrumentProgress) {
            d->instrumentProgress = new TC::ProgressDialog(tr("Please wait."), tr("Close"), 0, 100, this);
            d->instrumentProgress->setWindowModality(Qt::WindowModal);
        }

        QTimer::singleShot(200, this, SLOT(onInitialize()));
        return true;
    }

    void MainWindow::OnHandleEvent(const ctkEvent& ctkEvent) {
        Q_UNUSED(ctkEvent)
    }
}
