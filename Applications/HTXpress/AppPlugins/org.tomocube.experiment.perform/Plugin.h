#pragma once

#include <IAppActivator.h>

const QString plugin_symbolic_name = "org.tomocube.experiment.perform";

namespace HTXpress::AppPlugins::Experiment::Perform {
    class Plugin : public TC::Framework::IAppActivator {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.experiment.perform")            

    public:
        static Plugin* getInstance();

        Plugin();
        ~Plugin();

        auto start(ctkPluginContext* context)->void override;
        auto stop(ctkPluginContext* context)->void override;
        auto getPluginContext()->ctkPluginContext* const override;                
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}