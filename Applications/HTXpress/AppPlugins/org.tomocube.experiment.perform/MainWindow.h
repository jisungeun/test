#pragma once
#include <memory>
#include <IMainWindowHTX.h>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class MainWindow : public AppComponents::Framework::IMainWindowHTX {
        Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = nullptr);
        virtual ~MainWindow();
                
        auto Execute(const QVariantMap& params) -> bool override;

    protected:
        auto TryActivate() -> bool final;
        auto TryDeactivate() -> bool final;
        auto GetMetaInfo()->QVariantMap final;
        auto IsActivate() -> bool final;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    protected slots:
        void onInitialize();
        void onUpdateProgress(double progress, const QString& message);
        void onInstrumentFailed(const QString& message);
        void onLoadingExperimentFailed(const QString& message);
        void onReportMotionError(const QString& message);
        void onEndExperiment();
        void onGotoExpSetup();
        void onGotoDataNavigation();
        void onRunExperiment();
        void onRunExperimentSingle();
        void OnHandleEvent(const ctkEvent& ctkEvent);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
