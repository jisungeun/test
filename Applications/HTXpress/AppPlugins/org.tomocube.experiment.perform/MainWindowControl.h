#pragma once
#include <memory>

namespace HTXpress::AppPlugins::Experiment::Perform {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

        auto CleanUp()->void;
        auto StartInitialize()->bool;
        auto LoadExperiment(const QString& projectTitle, const QString& experimentTitle)->bool;
        auto ReadyToLoadVessel()->bool;
        auto IsExperimentRunning()->bool;

        auto StartJog(AppEntity::Axis axis, bool plusDirection)->bool;
        auto StopJog()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}