#pragma once

#include <memory>

#include <QDialog>
#include <QString>
#include <QDateTime>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class TemplateSelectionDialog : public QDialog {
        Q_OBJECT

    public:
        TemplateSelectionDialog(QWidget* parent = nullptr);
        ~TemplateSelectionDialog();

        auto AddTemplateData(const QString& title, const QDateTime& lastModified) -> void;
        auto GetSelectedTemplate() const->QString;

    protected slots:
        void onChangeCurrentSelection(int currentRow, int currentColumn, int previousRow, int previousColumn);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
