#include "CenterAlignedComboBox.h"

#include <QMouseEvent>
#include <QListView>
#include <QLineEdit>

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct CenterAlignedComboBox::Impl {
        bool isShowPopup{ false };
    };

    CenterAlignedComboBox::CenterAlignedComboBox(QWidget *parent) : QComboBox(parent), d{ new Impl } {
        auto comboBoxListView = new QListView(this);
        comboBoxListView->installEventFilter(this);
        setView(comboBoxListView);

        auto comboBoxlineEdit = new QLineEdit(this);
		comboBoxlineEdit->setReadOnly(true);
		comboBoxlineEdit->setAlignment(Qt::AlignCenter);
        comboBoxlineEdit->installEventFilter(this);
        setLineEdit(comboBoxlineEdit);
    }

    CenterAlignedComboBox::~CenterAlignedComboBox() {
        
    }

    bool CenterAlignedComboBox::eventFilter(QObject* watched, QEvent* event) {
        const auto eventType = event->type();
        if (watched == lineEdit()) {
            if (eventType == QEvent::MouseButtonRelease) {
                if (isEnabled()) {
                    d->isShowPopup ? hidePopup() : showPopup();
                    d->isShowPopup = !d->isShowPopup;
                    
                    return true;
                }
            }
        } else if (watched == view()) {
            if (eventType == QEvent::FocusOut) {
                hidePopup();
                d->isShowPopup = false;

                return true;
            }
        }

        return QComboBox::eventFilter(watched, event);
    }
}
