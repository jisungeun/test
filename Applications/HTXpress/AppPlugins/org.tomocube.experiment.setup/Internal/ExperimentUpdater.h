#pragma once

#include <memory>

#include <Experiment.h>
#include <ExperimentShortInfo.h>
#include <IExperimentView.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class ExperimentObserver;

    class ExperimentUpdater : public Interactor::IExperimentView {
    public:
        using Pointer = std::shared_ptr<ExperimentUpdater>;

    protected:
        ExperimentUpdater();

    public:
        ~ExperimentUpdater() override;

        static auto GetInstance()->Pointer;

        auto UpdateExperiment(const AppEntity::Experiment::Pointer& experiment) const->void override;
        auto UpdateList(const QList<Entity::ExperimentShortInfo>& data)->void override;
        auto UpdateTitle(const QString& title) const->void override;
		auto UpdateVesselType(const QString& type) const->void override;
		auto UpdateVesselCount(int count) const->void override;
        auto UpdateVessels(const QStringList& vessels) const->void override;
        auto UpdateMedium(const AppEntity::Medium& medium) const -> void override;
        auto UpdateDeleteProgress(int32_t totalCount, int32_t deleteCount) const -> void override;
        auto UpdateSampleTypeName(const AppEntity::SampleTypeName& sampleTypeName) -> void override;

        auto AddWellGroup(const QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>>& groupIndices) const -> void override;
        auto AddWellsToWellGroup(AppEntity::WellGroupIndex groupIndex, const QList<AppEntity::RowColumn>& indices) const -> void override;
        auto MoveWells(AppEntity::WellGroupIndex movingGroupIndex, AppEntity::WellGroupIndex targetGroupIndex, const QList<AppEntity::RowColumn>& wells) const -> void override;
        auto RemoveWells(const QList<AppEntity::RowColumn>& indices) const -> void override;
        auto DeleteWellGroups(const QList<AppEntity::WellGroupIndex>& indices) const -> void override;
        auto ChangeWellGroupName(AppEntity::WellGroupIndex groupIndex, const QString& groupName) -> void override;
        auto ChangeWellName(AppEntity::VesselIndex vesselIndex, AppEntity::WellIndex wellIndex, const QString& wellName) -> void override;
        auto Clear()->void override;
        auto ExperimentSaved(bool saved, const QString& message = QString()) const -> void override;
        auto RoiListUpdated() -> void override;

        auto Error(const QString& message) const -> void override;

        auto Register(ExperimentObserver* observer)->void;
        auto Deregister(ExperimentObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class ExperimentObserver;
    };
}
