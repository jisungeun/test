#pragma once

#include <memory>

#include <QWidget>
#include <QListWidgetItem>

#include <ExperimentShortInfo.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class ExperimentExplorerPanel : public QWidget {
        Q_OBJECT

    public:
        using Self = ExperimentExplorerPanel;

        ExperimentExplorerPanel(QWidget* parent = nullptr);
        ~ExperimentExplorerPanel() override;

    protected slots:
        void onAddExperiment();
        void onAddExprimentFromTemplate();
        void onCopyExperiment();
        void onDeleteExperiment();
        void onChangeExperimentSelection(QListWidgetItem* item);
        void onLoadExperiment(QListWidgetItem* item);
        void onExperimentErrorOccurred(const QString& message);

        void onChangeProject(const QString &title);
        void onUpdateExperimentList(const QList<Entity::ExperimentShortInfo>& infos);
        void onUpdateExperiment(const AppEntity::Experiment::Pointer& experiment);
        void onUpdateUI();
        void onProjectedDeleted(bool isProjectEmpty);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
