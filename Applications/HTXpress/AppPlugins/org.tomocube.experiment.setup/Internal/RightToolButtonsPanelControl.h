#pragma once

#include <memory>

#include <QCoreApplication>
#include <QString>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class RightToolButtonsPanelControl {
        Q_DECLARE_TR_FUNCTIONS(RightToolButtonsPanelControl)

    public:
        RightToolButtonsPanelControl();
        ~RightToolButtonsPanelControl();

        auto SaveAsTemplate(const QString& title) const->bool;
        auto SaveExperiment() const->bool;
        auto RestoreExperiment() const->bool;

        auto LoadedExperiment() const->bool;
        auto HasUnsavedChanges() const->bool;

        auto IsRunningExperiment() const -> bool;
        auto CheckTemplateNameValidation(const QString& name, QString& errorMessage) const->bool;

        auto IsCurrentVesselValid() const->bool;
        auto IsCurrentMediumValid() const->bool;
        auto IsCurrentProfileValid() const->bool;
        auto IsProfilesExistInExperimentFolder() const->bool;

        auto CopyProfileFiles() const -> bool;

        auto GetCurrentSampleTypeName() const->QString; 
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
