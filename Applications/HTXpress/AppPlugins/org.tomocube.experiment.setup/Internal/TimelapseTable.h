#pragma once

#include <memory>

#include <QWidget>

#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class TimelapseTable : public QWidget {
        Q_OBJECT

    public:
        TimelapseTable(QWidget* parent = nullptr);
        ~TimelapseTable();

    protected slots:
        void onChangeProject(const QString& title);
        void onChangeExperiment(const AppEntity::Experiment::Pointer& experiment);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
