#pragma once

#include <memory>

#include <QWidget>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class ProjectPanel : public QWidget {
        Q_OBJECT

    public:
        ProjectPanel(QWidget* parent = nullptr);
        ~ProjectPanel();

        bool eventFilter(QObject* watched, QEvent* event) override;

    protected slots:
        void onUpdateUI();
        void onChangeCurrentProject(int index);
        void onCreateProject();
        void onDeleteProjectButtonClicked();
        void onChangeDescription();

        void onUpdateProjectList(const QList<QString>& projects);
        void onChangeProjectSelection(const QString& title);
        void onUpdateDescription(const QString& text);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
