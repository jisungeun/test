#pragma once

#include <memory>

#include <IProjectView.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class ProjectObserver;

    class ProjectUpdater : public Interactor::IProjectView {
    public:
        using Pointer = std::shared_ptr<ProjectUpdater>;

    protected:
        ProjectUpdater();

    public:
        ~ProjectUpdater();

        static auto GetInstance()->Pointer;

        auto UpdateList(const QList<QString>& projects)->void override;
        auto ChangeProjectSelection(const QString& title)->void override;
        auto UpdateDescription(const QString& description)->void override;
        auto ProjectDeleted(bool isProjectEmpty) -> void override;

        auto Register(ProjectObserver* observer)->void;
        auto Deregister(ProjectObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class ProjectObserver;
    };
}