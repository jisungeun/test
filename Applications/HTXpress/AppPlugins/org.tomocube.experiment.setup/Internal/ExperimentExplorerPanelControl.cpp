#include "ExperimentExplorerPanelControl.h"

#include <SystemStatus.h>
#include <AppData.h>
#include <ExperimentTemplateRepo.h>
#include <ExperimentReaderPort.h>
#include <ExperimentWriterPort.h>
#include <ExperimentController.h>
#include <ExperimentPresenter.h>
#include <DirectoryDeleterPlugin.h>
#include <UserController.h>

#include "ExperimentUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
	struct ExperimentExplorerPanelControl::Impl {
	};

	ExperimentExplorerPanelControl::ExperimentExplorerPanelControl() : d{ new Impl } {
        
    }

	ExperimentExplorerPanelControl::~ExperimentExplorerPanelControl() {
	    
	}

	auto ExperimentExplorerPanelControl::AddExperiment(const QString& title) -> bool {
		auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());

	    auto reader = Plugins::ExperimentIO::ExperimentReaderPort();
		auto writer = Plugins::ExperimentIO::ExperimentWriterPort();

		auto controller = Interactor::ExperimentController(&presenter, &reader, &writer);

		return controller.CreateExperiment(title);
	}

	auto ExperimentExplorerPanelControl::AddExperimentFromTemplate(const QString& title, const QString& templateTitle) -> bool {
		auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());

		auto reader = Plugins::ExperimentIO::ExperimentReaderPort();
		auto writer = Plugins::ExperimentIO::ExperimentWriterPort();

		auto controller = Interactor::ExperimentController(&presenter, &reader, &writer);

		return controller.CreateExperimentFromTemplate(templateTitle, title);
	}

	auto ExperimentExplorerPanelControl::Copy(const QString& title, const QString& copyTitle) -> bool {
		auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());

		auto reader = Plugins::ExperimentIO::ExperimentReaderPort();
		auto writer = Plugins::ExperimentIO::ExperimentWriterPort();

		auto controller = Interactor::ExperimentController(&presenter, &reader, &writer);

		return controller.CopyExperiment(title, copyTitle);
	}

	auto ExperimentExplorerPanelControl::Delete(const QString& title) -> bool {
		auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());
		auto deleter = Plugins::DirectoryDeleter::Deleter();

		auto controller = Interactor::ExperimentController(&presenter, nullptr, nullptr, &deleter);

		return controller.DeleteExperiment(title);
    }

	auto ExperimentExplorerPanelControl::Load(const QString& title) -> bool {
		auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());

		auto reader = Plugins::ExperimentIO::ExperimentReaderPort();

		auto controller = Interactor::ExperimentController(&presenter, &reader);

		return controller.LoadExperiment(title);
    }

	auto ExperimentExplorerPanelControl::ChangeExperimentSelection(const QString& title) -> bool {
		auto controller = Interactor::ExperimentController();
		return controller.ChangeExperimentSelection(title);
	}

	auto ExperimentExplorerPanelControl::GetProjectTitle() const -> QString {
        return Entity::AppData::GetInstance()->GetProjectTitle();
    }

	auto ExperimentExplorerPanelControl::GetLoadedExperimentTitle() const -> QString {
        const auto experiment = Entity::AppData::GetInstance()->GetExperiment();
		if (experiment == nullptr) return QString();

		return experiment->GetTitle();
    }

	auto ExperimentExplorerPanelControl::GetExperimentInfos() const -> QList<Entity::ExperimentShortInfo> {
        return Entity::AppData::GetInstance()->GetExperimentListInfos();
    }

	auto ExperimentExplorerPanelControl::GetTemplateInfos() const -> QMap<QString, QDateTime> {
		QMap<QString, QDateTime> infos;

        const auto repo = Entity::ExperimentTemplateRepo::GetInstance();
		for (auto& name : repo->GetTemplates()) {
		    infos.insert(name, repo->GetTemplateLastModified(name));
		}
		
		return infos;
    }

	auto ExperimentExplorerPanelControl::IsRunningExperiment() const -> bool {
        return (AppEntity::SystemStatus::GetInstance()->GetExperiment() != nullptr);
    }

	auto ExperimentExplorerPanelControl::IsDeletable(const QString& title) const -> bool {
        auto controller = Interactor::ExperimentController();
		return controller.CheckDeletable(title);
    }

	auto ExperimentExplorerPanelControl::CheckExperimentTitleValidation(const QString& title, QString& errorMessage) const -> bool {
        auto infos = Entity::AppData::GetInstance()->GetExperimentListInfos();

		// 이미 존재하는 이름인지 확인
		bool isExist = false;
	    for (auto& info : infos) {
		    if (info.GetTitle().compare(title, Qt::CaseInsensitive) == 0) {
		        isExist = true;
				break;
		    }
		}

		if (isExist) {
			errorMessage = tr("This experiment name already exists.");
		    return false;
		}

		// 이름 유효성 확인
		TC::FileNameValidator validator;
		validator.AddInavlidCharacter('.');
        if (!validator.IsValid(title)) {
            switch (validator.GetError()) {
            case TC::FileNameValid::MaxLenghtOverError:
                errorMessage = tr("This experiment name length exceeded the maximum length.");
                break;
            case TC::FileNameValid::WhitespaceAtTheEndError:
                errorMessage = tr("This experiment name ends in whitespace.");
                break;
            case TC::FileNameValid::InvalidCharacterContainedError:
                errorMessage = tr("This experiment name contains invalid characters.\n The following characters are not allowed: < > : \" / \\ | ? * .");
                break;
            default:
				errorMessage = validator.GetErrorString();
                break;
            }

			return false;
        }

		return true;
    }

    auto ExperimentExplorerPanelControl::CheckPassword(const QString& password) const -> bool {
		auto controller = Interactor::UserController();
		return controller.CheckPassword(password);
    }
}
