#include "TemplateSelectionDialog.h"

#include <QPushButton>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QHeaderView>

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct TemplateSelectionDialog::Impl {
        QTableWidget* templateTable{ nullptr };
        QString selectedTemplate;
    };

    TemplateSelectionDialog::TemplateSelectionDialog(QWidget* parent) : QDialog(parent), d{ new Impl } {
        d->templateTable = new QTableWidget;
        d->templateTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        d->templateTable->setSelectionBehavior(QAbstractItemView::SelectRows);
        d->templateTable->setSelectionMode(QAbstractItemView::SingleSelection);
        d->templateTable->setColumnCount(2);
        d->templateTable->setHorizontalHeaderLabels({tr("Name"), tr("Recent modified")});
        d->templateTable->setColumnWidth(0, 200);
        d->templateTable->horizontalHeader()->setStretchLastSection(true);

        auto okButton = new QPushButton("OK");
        auto cancelButton = new QPushButton("Cancel");

        okButton->setObjectName("tc-templateselectiondialog-ok");
        cancelButton->setObjectName("tc-templateselectiondialog-cancel");

        auto buttonLayout = new QHBoxLayout;
        buttonLayout->addSpacerItem(new QSpacerItem(1,1, QSizePolicy::Expanding));
        buttonLayout->addWidget(okButton);
        buttonLayout->addWidget(cancelButton);

        auto layout = new QVBoxLayout;
        layout->addWidget(d->templateTable, 1);
        layout->addLayout(buttonLayout);

        setLayout(layout);
        setMinimumSize(400, 300);

        connect(d->templateTable, &QTableWidget::currentCellChanged, this, &TemplateSelectionDialog::onChangeCurrentSelection);

        connect(okButton, &QPushButton::clicked, this, &QDialog::accept);
        connect(cancelButton, &QPushButton::clicked, this, &QDialog::reject);
    }

    TemplateSelectionDialog::~TemplateSelectionDialog() {
    }

    auto TemplateSelectionDialog::AddTemplateData(const QString& title, const QDateTime& lastModified) -> void {
        auto newRow = d->templateTable->rowCount();
        d->templateTable->setRowCount(newRow+1);
        d->templateTable->setItem(newRow, 0, new QTableWidgetItem(title));
        d->templateTable->setItem(newRow, 1, new QTableWidgetItem(lastModified.toString("yyyy.MM.dd hh:mm")));
    }

    auto TemplateSelectionDialog::GetSelectedTemplate() const -> QString {
        return d->selectedTemplate;
    }

    void TemplateSelectionDialog::onChangeCurrentSelection(int currentRow, int currentColumn, int previousRow, int previousColumn) {
        Q_UNUSED(currentColumn)
        Q_UNUSED(previousRow)
        Q_UNUSED(previousColumn)

        d->selectedTemplate = d->templateTable->item(currentRow, 0)->text();
    }

}
