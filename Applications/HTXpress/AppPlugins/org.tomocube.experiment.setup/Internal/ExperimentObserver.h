#pragma once

#include <memory>

#include <QObject>

#include <Experiment.h>
#include <ExperimentShortInfo.h>

namespace HTXpress::AppPlugins::Experiment::Setup{
    class ExperimentObserver : public QObject {
        Q_OBJECT

    public:
        using Pointer = std::shared_ptr<ExperimentObserver>;

        ExperimentObserver(QObject* parent = nullptr);
        ~ExperimentObserver() override;

        // Todo: experiment entity 객체 대신 title or id 넘겨서 사용하는 방법
        auto UpdateExperiment(const AppEntity::Experiment::Pointer& experiment)->void;
        auto UpdateList(const QList<Entity::ExperimentShortInfo>& data)->void;
        auto UpdateTitle(const QString& title)->void;
        auto UpdateVesselType(const QString& type)->void;
        auto UpdateVesselCount(int count)->void;
        auto UpdateVessels(const QStringList& vessels)->void;
        auto UpdateMedium(const AppEntity::Medium& medium)->void;
        auto UpdateDeleteProgress(int32_t totalCount, int32_t deleteCount)->void;
        auto UpdateSampleTypeName(const AppEntity::SampleTypeName& sampleTypeName)->void;
        auto AddWellGroup(const QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>>& groupIndices)->void;
        auto AddWellsToWellGroup(AppEntity::WellGroupIndex groupIndex, const QList<AppEntity::RowColumn>& indices)->void;
        auto MoveWells(AppEntity::WellGroupIndex movingGroupIndex, AppEntity::WellGroupIndex targetGroupIndex, const QList<AppEntity::RowColumn>& wells)->void;
        auto RemoveWells(const QList<AppEntity::RowColumn>& indices)->void;
        auto DeleteWellGroups(const QList<AppEntity::WellGroupIndex>& indices)->void;
        auto ChangeWellGroupName(AppEntity::WellGroupIndex groupIndex, const QString& groupName)->void;
        auto Clear()->void;
        auto ExperimentSaved(bool saved, const QString& message) -> void;
        auto ChangeWellName(AppEntity::VesselIndex vesselIndex, AppEntity::WellIndex wellIndex, const QString& wellName)->void;
        auto RoiListUpdated() -> void;
        auto Error(const QString& errorMessage)->void;

    signals:
        void sigExperimentChanged(const AppEntity::Experiment::Pointer& experiment);
        void sigExperimentModified();
        void sigExperimentListChanged(const QList<Entity::ExperimentShortInfo>& data);
        void sigExperimentTitleChanged(const QString& title);
        void sigExperimentVesselTypeChanged(const QString& type);
        void sigExperimentVesselCountChanged(int count);
        void sigExperimentMediumChanged(const AppEntity::Medium& medium);
        void sigExperimentSampleTypeNameChanged(const AppEntity::SampleTypeName& sampleTypeName);
        void sigVesselsUpdated(const QStringList& vessels);
        void sigDeleteProgressUpdated(int totalCount, int deleteCount);
        void sigWellGroupAdded(const QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>>& groupIndices);
        void sigWellsAdded(AppEntity::WellGroupIndex groupIndex, const QList<AppEntity::RowColumn>& indices);
        void sigWellsMoved(AppEntity::WellGroupIndex movingGroupIndex, AppEntity::WellGroupIndex targetGroupIndex, const QList<AppEntity::RowColumn>& wells);
        void sigWellsRemoved(const QList<AppEntity::RowColumn>& indices);
        void sigWellGroupsDeleted(const QList<AppEntity::WellGroupIndex>& indices);
        void sigWellGroupNameChanged(AppEntity::WellGroupIndex groupIndex, const QString& groupName);
        void sigClear();
        void sigExperimentSaved(bool saved, const QString& path);
        void sigRoiListUpdated();
        void sigError(const QString&);
    };
}
