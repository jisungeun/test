#pragma once

#include <memory>

#include <QString>
#include <QColor>

#include <AppEntityDefines.h>
#include <Experiment.h>
#include <VesselMap.h>
#include <ROI.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class VesselMapPanelControl {
    public:
        VesselMapPanelControl();
        ~VesselMapPanelControl();

        auto AddWellsToSeparateGroup(int vesselIndex, const QList<QPair<int, int>>& wells)->bool;
        auto AddWellsToNewGroup(int vesselIndex, const QList<QPair<int, int>>& wells)->bool;
        auto AddWellsToWellGroup(int vesselIndex, int groupIndex, const QList<QPair<int, int>>& wells)->bool;
        auto RemoveExperimentWells(int vesselIndex, const QList<QPair<int, int>>& wells)->bool;
        auto ChangeWellGroupName(int vesselIndex, int groupIndex, const QString& name)->bool;
        auto ChangeWellGroupColor(int vesselIndex, int groupIndex, const QColor& color)->bool;
        auto MoveWellGroup(int vesselIndex, int movingGroupIndex, int targetGroupIndex)->bool;
        auto MoveWell(int vesselIndex, int movingGroupIndex, int targetGroupIndex, const QList<QPair<int, int>>& wells)->bool;
        auto ChangeWellName(int vesselIndex, int wellIndex, const QString& name)->bool;
        auto SetROIs(int vesselIndex, const QMap<AppEntity::WellIndex, QMap<AppEntity::ROIIndex, AppEntity::ROI::Pointer>>& roiList)->bool;

        auto GetLoadedExperiment() const -> AppEntity::Experiment::Pointer;
        auto IsRunningExperiment() const -> bool;
        auto GetNewWellGroupIndex()->AppEntity::WellGroupIndex;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
