#include "ExperimentUpdater.h"

#include "ExperimentObserver.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct ExperimentUpdater::Impl {
        QList<ExperimentObserver*> observers;
    };

    ExperimentUpdater::ExperimentUpdater() : d{ new Impl } {
        
    }

    ExperimentUpdater::~ExperimentUpdater() {
        
    }

    auto ExperimentUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new ExperimentUpdater() };
        return theInstance;
    }

    auto ExperimentUpdater::UpdateExperiment(const AppEntity::Experiment::Pointer& experiment) const -> void {
        for (auto observer : d->observers) {
            observer->UpdateExperiment(experiment);
        }
    }
    auto ExperimentUpdater::UpdateList(const QList<Entity::ExperimentShortInfo>& data) -> void {
        for (auto observer : d->observers) {
            observer->UpdateList(data);
        }
    }

    auto ExperimentUpdater::UpdateTitle(const QString& title) const -> void {
        for (auto observer : d->observers) {
            observer->UpdateTitle(title);
        }
    }

    auto ExperimentUpdater::UpdateVesselType(const QString& type) const -> void {
        for (auto observer : d->observers) {
            observer->UpdateVesselType(type);
        }
    }

    auto ExperimentUpdater::UpdateVesselCount(int count) const -> void {
        for (auto observer : d->observers) {
            observer->UpdateVesselCount(count);
        }
    }

    auto ExperimentUpdater::UpdateVessels(const QStringList& vessels) const -> void {
        for (auto observer : d->observers) {
            observer->UpdateVessels(vessels);
        }
    }

    auto ExperimentUpdater::UpdateMedium(const AppEntity::Medium& medium) const -> void {
        for (auto observer : d->observers) {
            observer->UpdateMedium(medium);
        }
    }

    auto ExperimentUpdater::UpdateDeleteProgress(int32_t totalCount, int32_t deleteCount) const -> void {
        for (auto observer : d->observers) {
            observer->UpdateDeleteProgress(totalCount, deleteCount);
        }
    }

    auto ExperimentUpdater::UpdateSampleTypeName(const AppEntity::SampleTypeName& sampleTypeName) -> void {
        for (auto observer : d->observers) {
            observer->UpdateSampleTypeName(sampleTypeName);
        }
    }

    auto ExperimentUpdater::AddWellGroup(const QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>>& groupIndices) const -> void {
        for (auto observer : d->observers) {
            observer->AddWellGroup(groupIndices);
        }
    }

    auto ExperimentUpdater::AddWellsToWellGroup(AppEntity::WellGroupIndex groupIndex, const QList<AppEntity::RowColumn>& indices) const -> void {
        for (auto observer : d->observers) {
            observer->AddWellsToWellGroup(groupIndex, indices);
        }
    }

    auto ExperimentUpdater::MoveWells(AppEntity::WellGroupIndex movingGroupIndex, AppEntity::WellGroupIndex targetGroupIndex, const QList<AppEntity::RowColumn>& wells) const -> void {
        for (auto observer : d->observers) {
            observer->MoveWells(movingGroupIndex, targetGroupIndex, wells);
        }
    }

    auto ExperimentUpdater::RemoveWells(const QList<AppEntity::RowColumn>& indices) const -> void {
        for (auto observer : d->observers) {
            observer->RemoveWells(indices);
        }
    }

    auto ExperimentUpdater::DeleteWellGroups(const QList<AppEntity::WellGroupIndex>& indices) const -> void {
        for (auto observer : d->observers) {
            observer->DeleteWellGroups(indices);
        }
    }

    auto ExperimentUpdater::ChangeWellGroupName(AppEntity::WellGroupIndex groupIndex, const QString& groupName) -> void {
        for (auto observer : d->observers) {
            observer->ChangeWellGroupName(groupIndex, groupName);
        }
    }

    auto ExperimentUpdater::ChangeWellName(AppEntity::VesselIndex vesselIndex, AppEntity::WellIndex wellIndex, const QString& wellName) -> void {
        for(auto observer : d->observers) {
            observer->ChangeWellName(vesselIndex, wellIndex, wellName);
        }
    }

    auto ExperimentUpdater::Clear() -> void {
        for (auto observer : d->observers) {
            observer->Clear();
        }
    }

    auto ExperimentUpdater::ExperimentSaved(bool saved, const QString& message) const -> void {
        for (auto observer : d->observers) {
            observer->ExperimentSaved(saved, message);
        }
    }

    auto ExperimentUpdater::RoiListUpdated() -> void {
        for (auto observer : d->observers) {
            observer->RoiListUpdated();
        }
    }

    auto ExperimentUpdater::Error(const QString& message) const -> void {
        for (auto observer : d->observers) {
            observer->Error(message);
        }
    }

    auto ExperimentUpdater::Register(ExperimentObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto ExperimentUpdater::Deregister(ExperimentObserver* observer) -> void {
        auto index = d->observers.indexOf(observer);
        if (index < 0) {
            return;
        }

        d->observers.removeAt(index);
    }
}
