#include "ExperimentSettingPanelControl.h"

#include <SystemStatus.h>
#include <AppData.h>
#include <MediumDataRepo.h>
#include <SampleTypeRepo.h>
#include <System.h>

#include <ExperimentWriterPort.h>
#include <ExperimentController.h>
#include <ExperimentPresenter.h>

#include "ExperimentUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
	struct ExperimentSettingPanelControl::Impl {

	};

	ExperimentSettingPanelControl::ExperimentSettingPanelControl() : d{ new Impl } {

    }

	ExperimentSettingPanelControl::~ExperimentSettingPanelControl() {
        
    }

    auto ExperimentSettingPanelControl::UpdateVessels() const -> bool {
        auto updater = ExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter(updater.get());

        auto controller = Interactor::ExperimentController(&presenter);

        return controller.UpdateVessels();
    }

	auto ExperimentSettingPanelControl::ChangeTitle(const QString& title) const -> bool {
        auto updater = ExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter(updater.get());

        auto writer = Plugins::ExperimentIO::ExperimentWriterPort();
        auto controller = Interactor::ExperimentController(&presenter, nullptr, &writer);

        return controller.ChangeExperimentTitle(title);
    }

    auto ExperimentSettingPanelControl::ChangeMedium(const QString& name, double value) const -> bool {
        auto updater = ExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter(updater.get());

        auto controller = Interactor::ExperimentController(&presenter);

        return controller.ChangeExperimentMedium(name, value);
    }

    auto ExperimentSettingPanelControl::ChangeVesselHolder(const QString& vesselTitle) const -> bool {
        auto updater = ExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter(updater.get());

        auto controller = Interactor::ExperimentController(&presenter);

        return controller.ChangeExperimentVesselHolder(vesselTitle);
    }

    auto ExperimentSettingPanelControl::ChangeNumberOfVessel(const int32_t number) const -> bool {
        auto updater = ExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter(updater.get());

        auto controller = Interactor::ExperimentController(&presenter);

        return controller.ChangeExperimentNumberOfVessel(number);
    }
    
    auto ExperimentSettingPanelControl::ChangeSampleType(const QString& sampleTypeName) const -> bool{
        auto updater = ExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter(updater.get());
        auto controller = Interactor::ExperimentController(&presenter);
        return controller.ChangeExperimentSampleType(sampleTypeName);
    }

    auto ExperimentSettingPanelControl::GetCurrentProjectTitle() const -> QString {
        return Entity::AppData::GetInstance()->GetProjectTitle();
    }

    auto ExperimentSettingPanelControl::GetLoadedExperiment() const -> AppEntity::Experiment::Pointer {
        return Entity::AppData::GetInstance()->GetExperiment();
    }

    auto ExperimentSettingPanelControl::GetMediumList() const -> QList<AppEntity::MediumData> {
        return AppEntity::MediumDataRepo::GetInstance()->GetMedia();
    }

    auto ExperimentSettingPanelControl::IsRunningExperiment() const -> bool {
        return (AppEntity::SystemStatus::GetInstance()->GetExperiment() != nullptr);
    }

    auto ExperimentSettingPanelControl::ExistImagingData(const QString& experiment) const -> bool {
        auto controller = Interactor::ExperimentController();
        return controller.ExistImagingData(experiment);
    }

    auto ExperimentSettingPanelControl::CheckExperimentTitleValidation(const QString& title, QString& errorMessage) const -> bool {
        auto infos = Entity::AppData::GetInstance()->GetExperimentListInfos();

		// 이미 존재하는 이름인지 확인
		bool isExist = false;
	    for (auto& info : infos) {
		    if (info.GetTitle().compare(title, Qt::CaseInsensitive) == 0) {
		        isExist = true;
				break;
		    }
		}

		if (isExist) {
			errorMessage = tr("This experiment name already exists.");
		    return false;
		}

		// 이름 유효성 확인
		TC::FileNameValidator validator;
        validator.AddInavlidCharacter('.');
        if (!validator.IsValid(title)) {
            switch (validator.GetError()) {
            case TC::FileNameValid::MaxLenghtOverError:
                errorMessage = tr("This experiment name length exceeded the maximum length.");
                break;
            case TC::FileNameValid::WhitespaceAtTheEndError:
                errorMessage = tr("This experiment name ends in whitespace.");
                break;
            case TC::FileNameValid::InvalidCharacterContainedError:
                errorMessage = tr("This experiment name contains invalid characters.\n The following characters are not allowed: < > : \" / \\ | ? * .");
                break;
            default:
                errorMessage = validator.GetErrorString();
                break;
            }

			return false;
        }

		return true;
    }

    auto ExperimentSettingPanelControl::GetSampleTypeNameList() const -> QStringList {
        QStringList sampleTypeNames;
        const auto sampleTypeRepo = AppEntity::SampleTypeRepo::GetInstance();
        const auto model = AppEntity::System::GetSystemConfig()->GetModel();
        if(sampleTypeRepo->GetSampleTypeNamesByModel(model)) {
            sampleTypeNames = sampleTypeRepo->GetSampleTypeNamesByModel(model).value();
        }
        return sampleTypeNames;
    }

    auto ExperimentSettingPanelControl::GetCurrentProfileName() const -> QString {
        return Entity::AppData::GetInstance()->GetExperiment()->GetSampleTypeName();
    }
}
