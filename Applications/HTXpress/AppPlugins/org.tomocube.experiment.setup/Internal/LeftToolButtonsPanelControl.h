#pragma once

#include <memory>

#include <QString>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class LeftToolButtonsPanelControl final {
    public:
        LeftToolButtonsPanelControl();
        ~LeftToolButtonsPanelControl();

        auto IsRunningExperiment() const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
