#pragma once

#include <memory>

#include <QWidget>

#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
	class ExperimentListItemWidget : public QWidget {
	    Q_OBJECT
	public:
		using Progress = AppEntity::ExperimentProgress;

		ExperimentListItemWidget(const QString& date, const QString& title, Progress progress = Progress::NotStarted, QWidget *parent = nullptr);
		~ExperimentListItemWidget();

		auto GetTitle() const->QString;
		auto SetProgress(Progress progress) const->void;

	private:
		struct Impl;
		std::shared_ptr<Impl> d;
	};
}