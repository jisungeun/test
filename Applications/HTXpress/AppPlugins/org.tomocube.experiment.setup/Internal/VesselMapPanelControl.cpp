#include "VesselMapPanelControl.h"

#include <SystemStatus.h>
#include <AppData.h>
#include <VesselRepo.h>
#include <ExperimentController.h>
#include <ExperimentPresenter.h>

#include "ExperimentUpdater.h"
#include "VesselMapUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct VesselMapPanelControl::Impl {
        auto ConvertVesselShape(AppEntity::VesselShape shape)->TC::VesselShape;
        auto ConvertWellShape(AppEntity::WellShape shape)->TC::WellShape;
        auto ConvertImagingAreaShape(AppEntity::ImagingAreaShape shape)->TC::ImagingAreaShape;
    };

    auto VesselMapPanelControl::Impl::ConvertVesselShape(AppEntity::VesselShape shape) -> TC::VesselShape {
        auto vesselShape = TC::VesselShape::Rectangle;
        if(shape == +AppEntity::VesselShape::Circle) {
            vesselShape = TC::VesselShape::Circle;
        }

        return vesselShape;
    }

    auto VesselMapPanelControl::Impl::ConvertWellShape(AppEntity::WellShape shape) -> TC::WellShape {
        auto wellShape = TC::WellShape::Circle;
        if (shape == +AppEntity::WellShape::Rectangle) {
            wellShape = TC::WellShape::Rectangle;
        }

        return wellShape;
    }

    auto VesselMapPanelControl::Impl::ConvertImagingAreaShape(AppEntity::ImagingAreaShape shape) -> TC::ImagingAreaShape {
        auto imagingAreaShape = TC::ImagingAreaShape::Circle;
        if (shape == +AppEntity::ImagingAreaShape::Rectangle) {
            imagingAreaShape = TC::ImagingAreaShape::Rectangle;
        }

        return imagingAreaShape;
    }

    VesselMapPanelControl::VesselMapPanelControl() : d{ new Impl } {
        
    }

    VesselMapPanelControl::~VesselMapPanelControl() {
        
    }

    auto VesselMapPanelControl::AddWellsToSeparateGroup(int vesselIndex, const QList<QPair<int, int>>& wells) -> bool {
        auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());

        auto controller = Interactor::ExperimentController(&presenter);
        return controller.AddSeparateSpecimen(vesselIndex, wells);
    }

    auto VesselMapPanelControl::AddWellsToNewGroup(int vesselIndex, const QList<QPair<int, int>>& wells) -> bool {
        auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());

        auto controller = Interactor::ExperimentController(&presenter);
        return controller.AddWellsToNewGroup(vesselIndex, wells);
    }

    auto VesselMapPanelControl::AddWellsToWellGroup(int vesselIndex, int groupIndex, const QList<QPair<int, int>>& wells) -> bool {
        auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());

        auto controller = Interactor::ExperimentController(&presenter);
        return controller.AddWells(vesselIndex, groupIndex, wells);
    }

    auto VesselMapPanelControl::RemoveExperimentWells(int vesselIndex, const QList<QPair<int, int>>& wells) -> bool {
        auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());

        auto controller = Interactor::ExperimentController(&presenter);
        return controller.RemoveWells(vesselIndex, wells);
    }

    auto VesselMapPanelControl::ChangeWellGroupName(int vesselIndex, int groupIndex, const QString& name) -> bool {
        auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());
        auto controller = Interactor::ExperimentController(&presenter);
        return controller.SetWellGroupName(vesselIndex, groupIndex, name);
    }

    auto VesselMapPanelControl::ChangeWellGroupColor(int vesselIndex, int groupIndex, const QColor& color) -> bool {
        auto controller = Interactor::ExperimentController();
        return controller.SetWellGroupColor(vesselIndex, groupIndex, color);
    }

    auto VesselMapPanelControl::MoveWellGroup(int vesselIndex, int movingGroupIndex, int targetGroupIndex) -> bool {
        auto controller = Interactor::ExperimentController();
        return controller.MoveWellGroup(vesselIndex, movingGroupIndex, targetGroupIndex);
    }

    auto VesselMapPanelControl::MoveWell(int vesselIndex, int movingGroupIndex, int targetGroupIndex, const QList<QPair<int, int>>& wells) -> bool {
        auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());
        auto controller = Interactor::ExperimentController(&presenter);
        return controller.MoveWell(vesselIndex, movingGroupIndex, targetGroupIndex, wells);
    }

    auto VesselMapPanelControl::ChangeWellName(int vesselIndex, int wellIndex, const QString& name) -> bool {
        auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());
        auto controller = Interactor::ExperimentController(&presenter);
        return controller.SetWellName(vesselIndex, wellIndex, name);
    }

    auto VesselMapPanelControl::SetROIs(int vesselIndex, const QMap<AppEntity::WellIndex, QMap<AppEntity::ROIIndex, AppEntity::ROI::Pointer>>& roiList) -> bool {
        auto updater = ExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter(updater.get());
        auto controller = Interactor::ExperimentController(&presenter);
        return controller.SetRoiList(vesselIndex, roiList);
    }

    auto VesselMapPanelControl::GetLoadedExperiment() const -> AppEntity::Experiment::Pointer {
        return Entity::AppData::GetInstance()->GetExperiment();
    }

    auto VesselMapPanelControl::IsRunningExperiment() const -> bool {
        return (AppEntity::SystemStatus::GetInstance()->GetExperiment() != nullptr);
    }

    auto VesselMapPanelControl::GetNewWellGroupIndex() -> AppEntity::WellGroupIndex {
        auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        return experiment->GetNewWellGroupIndex();
    }

}
