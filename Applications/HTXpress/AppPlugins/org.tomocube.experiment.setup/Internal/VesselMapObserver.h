#pragma once

#include <memory>

#include <QObject>

#include <VesselMap.h>

namespace HTXpress::AppPlugins::Experiment::Setup{
    class VesselMapObserver : public QObject {
        Q_OBJECT

    public:
        using Pointer = std::shared_ptr<VesselMapObserver>;

        VesselMapObserver(QObject* parent = nullptr);
        ~VesselMapObserver();

        auto UpdateVesselModel(const TC::VesselMap::Pointer vesselMap)->void;
        auto ChangeVesselIndices(const QList<int>& indices)->void;
        auto ChangeExperiment(const QString& model, int count)->void;

    signals:
        void sigVesselModelChanged(const TC::VesselMap::Pointer vesselMap);
        void sigVesselIndicesChanged(const QList<int>& indices);
        void sigExperimentChanged(const QString& model, int count);
    };
}