#pragma once

#include <memory>
#include <QStyledItemDelegate>

namespace HTXpress::AppPlugins::Experiment::Setup {
    /**
     * \brief
     * 실험 목록에서 로드된 실험의 QListWidgetItem 배경색을 구분하여 표시하기 위한 item delegate
     * 로드된 실험은 QListWidgetItem에 Qt::UserRole로 true 값이 설정된 것으로 구분한다.
     */
    class ExperimentListItemDelegate : public QStyledItemDelegate {
        Q_OBJECT

    public:
        ExperimentListItemDelegate(QObject *parent = nullptr);
        ~ExperimentListItemDelegate();

        /**
         * \brief 로드된 item을 표시할 색 설정
         * \param color active 색
         */
        auto SetActiveColor(const QColor& color)->void;

        void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class ProjectObserver;
    };
}