#pragma once

#include <memory>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class TimelapseTableControl {
    public:
        TimelapseTableControl();
        ~TimelapseTableControl();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
