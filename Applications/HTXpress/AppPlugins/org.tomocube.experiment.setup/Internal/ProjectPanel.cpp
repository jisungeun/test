#define LOGGER_TAG "[ProjectPanel]"

#include "ProjectPanel.h"

#include <QList>
#include <QLineEdit>
#include <QListView>

#include <TCLogger.h>
#include <UIUtility.h>
#include <MessageDialog.h>
#include <InputDialog.h>
#include <ProjectRepo.h>
#include <MessageDialog.h>

#include "AppUIObserver.h"
#include "ProjectObserver.h"
#include "ExperimentObserver.h"
#include "ProjectPanelControl.h"

#include "ui_ProjectPanel.h"

// TODO: Project 삭제

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct ProjectPanel::Impl {
        Ui::ProjectPanel ui;
        ProjectPanelControl control;
        ProjectPanel* p{ nullptr };

        std::shared_ptr<AppUIObserver> appUIObserver{ nullptr };
        std::shared_ptr<ProjectObserver> projectObserver{ nullptr };
        std::shared_ptr<ExperimentObserver> experimentObserver{ nullptr };

        Impl(ProjectPanel* p) : p{ p } {}
        auto NewProject(const QString& title)->bool;
    };

    auto ProjectPanel::Impl::NewProject(const QString& title) -> bool {
        // TODO: 예외처리 - 존재하는 프로젝트 이름, 파일 또는 폴더 명으로 사용할 수 없는 문자 포함
        if (!control.CreateProject(title)) {
            QString log = tr("Failed to create new project.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(p, tr("Create Project"), log);
            return false;
        }

        if (!control.SetCurrentProject(title)) {
            QString log = tr("Failed to change project.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(p, tr("Change Project"), log);
            return false;
        }

        return true;
    }

    ProjectPanel::ProjectPanel(QWidget* parent) : QWidget(parent), d{ new Impl(this) } {
        d->ui.setupUi(this);

        d->ui.baseWidget->setObjectName("panel-light");
        d->ui.projectsComboBox->setObjectName("combobox-large");
        d->ui.createButton->setObjectName("bt-tsx-add");
        d->ui.deleteButton->setObjectName("bt-tsx-delete-project");

        d->ui.projectsComboBox->setView(new QListView);
        d->ui.projectsComboBox->setInsertPolicy(QComboBox::NoInsert);   // combo-box에 line edit으로 입력 이벤트가 발생하는 경우 자동으로 목록에 추가되지 않도록 설정

        d->ui.descriptionEdit->installEventFilter(this);    // focus out event 감지를 위해 event filter 사용

        d->appUIObserver = std::make_shared<AppUIObserver>(this);
        d->projectObserver = std::make_shared<ProjectObserver>(this);
        d->experimentObserver = std::make_shared<ExperimentObserver>(this);

        connect(d->ui.projectsComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &ProjectPanel::onChangeCurrentProject);  
        connect(d->ui.createButton, &QPushButton::clicked, this, &ProjectPanel::onCreateProject);
        connect(d->ui.deleteButton, &QPushButton::clicked, this, &ProjectPanel::onDeleteProjectButtonClicked);
        connect(d->appUIObserver.get(), &AppUIObserver::sigUpdatingUIRequested, this, &ProjectPanel::onUpdateUI);
        connect(d->projectObserver.get(), &ProjectObserver::sigProjectListChanged, this, &ProjectPanel::onUpdateProjectList);
        connect(d->projectObserver.get(), &ProjectObserver::sigProjectSelectionChanged, this, &ProjectPanel::onChangeProjectSelection);
        connect(d->projectObserver.get(), &ProjectObserver::sigDescriptionChanged, this, &ProjectPanel::onUpdateDescription);
    }

    ProjectPanel::~ProjectPanel() {
        
    }

    bool ProjectPanel::eventFilter(QObject* watched, QEvent* event) {
        if (watched == d->ui.descriptionEdit) {
            if (event->type() == QEvent::FocusOut) {
                onChangeDescription();
            }
        }

        return QWidget::eventFilter(watched, event);
    }

    void ProjectPanel::onUpdateUI() {
        auto project = d->control.GetCurrentProject();
        if (project.isEmpty()) {
            TC::SilentCall(d->ui.projectsComboBox)->setCurrentIndex(-1);
            d->ui.descriptionEdit->clear();
            d->ui.descriptionEdit->setEnabled(false);
        } else {
            TC::SilentCall(d->ui.projectsComboBox)->setCurrentText(project);
            d->ui.descriptionEdit->setPlainText(d->control.GetCurrentProjectDescription());
            d->ui.descriptionEdit->setEnabled(true);
        }

        d->ui.createButton->setEnabled(!d->control.IsRunningExperiment());
        d->ui.deleteButton->setEnabled(!d->control.IsRunningExperiment());
    }

    void ProjectPanel::onChangeCurrentProject(int index) {
        if (d->control.HasExperimentChanges()) {
            const QSignalBlocker blocker(d->ui.projectsComboBox);
            d->ui.projectsComboBox->setCurrentText(d->control.GetCurrentProject());
            TC::MessageDialog::warning(this, tr("Change Project"), tr("There are unsaved changes."));
            return;
        }

        if(index == -1) {
            d->ui.descriptionEdit->clear();
            return;
        }

        const auto selectedProject = d->ui.projectsComboBox->itemText(index);
        if (!d->control.SetCurrentProject(selectedProject)) {
            QString log = tr("It fails to change a project");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Change Project"), log);
        }
    }

    void ProjectPanel::onCreateProject() {
        TC::InputDialog w;
        connect(&w, &TC::InputDialog::textValueChanged, [&w](QString text) {
                                                         if (text.length() > 15) {
                                                             QSignalBlocker s(w);
                                                             w.setTextValue(text.left(15));
                                                         }});
        w.SetTitle(tr("New Project"));
        w.SetContent(tr("Enter new project name"));

        if(QDialog::DialogCode::Accepted != w.exec()) return;

        const auto str = w.textValue();
        if(str.isEmpty()) return;

        // 중복 확인
        if (d->control.IsExistProject(str)) {
            TC::MessageDialog::warning(this, tr("New Project"), tr("This project name already exists."));
            return;
        }

        // 유효한 이름인지 확인
        QString errorMessage;
        auto validationResult = d->control.IsValidName(str, errorMessage);
        if (validationResult != +TC::FileNameValid::NoError) {
            QString message;
            switch (validationResult) {
            case TC::FileNameValid::MaxLenghtOverError:
                message = tr("This project name length exceeded the maximum length.");
                break;
            case TC::FileNameValid::WhitespaceAtTheEndError:
                message = tr("This project name ends in whitespace.");
                break;
            case TC::FileNameValid::InvalidCharacterContainedError:
                message = tr("This project name contains invalid characters.\n The following characters are not allowed: < > : \" / \\ | ? * .");
                break;
            case TC::FileNameValid::EmptyStringError:
                message = tr("Empty string is not allowed.");
                break;
            default:
                message = errorMessage;
                break;
            }

            TC::MessageDialog::warning(this, tr("New Project"), message);
            return;
        }

        if(!d->NewProject(str)) return;

        d->ui.projectsComboBox->setCurrentText(str);
    }

    void ProjectPanel::onDeleteProjectButtonClicked() {
        // Get the selected project title from the combo box.
        const auto projectTitle = d->ui.projectsComboBox->currentText();
        if(projectTitle.isEmpty()) {
            return;
        }

        // Check if there are any busy experiments running.
        const auto busyExperiments = d->control.GetBusyExperiments();
        if (!busyExperiments.isEmpty()) {
            const auto busyList = busyExperiments.join(", ");
            const auto log = tr("You cannot delete this project while the HTXProcessingServer is running.\n"
                                "Experiments currently running on the HTXProcessingServer: %1\n\n"
                                "Please close the program and try again.\n"
                                ).arg(busyList);
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Delete Project"), log);
            return;
        }

        // Ask the user to confirm project deletion.
        const auto answer = TC::MessageDialog::question(
            this,
            tr("Delete Project"),
            tr("Are you sure you want to delete the project \'%1\' and all its contents?").arg(projectTitle),
            TC::MessageDialog::StandardButton::Yes | TC::MessageDialog::StandardButton::No,
            TC::MessageDialog::StandardButton::No
        );

        // Delete the project if the user confirms.
        if (TC::MessageDialog::StandardButton::Yes == answer) {
            const auto password = TC::InputDialog::getText(this, tr("Authentication"), tr("Enter your password"), QLineEdit::Password);
            if (!d->control.CheckPassword(password)) {
                TC::MessageDialog::warning(this, tr("Authentication"), tr("Invalid password is entered"));
                return;
            }

            if(!d->control.DeleteProject(projectTitle)) {
                const auto log = tr("The project \'%1\' was not completely deleted.").arg(projectTitle);
                QLOG_INFO() << log;
                TC::MessageDialog::warning(this, tr("Delete Project"), log);
            }
        }
    }

    void ProjectPanel::onChangeDescription() {
        const auto title = d->ui.projectsComboBox->currentText();
        const auto description = d->ui.descriptionEdit->toPlainText();
        if (!d->control.SetDescription(title, description)) {
            QString log = tr("Failed to change project description.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Change Project Description"), log);
        }
    }

    void ProjectPanel::onUpdateProjectList(const QList<QString>& projects) {
        // combo-box 목록이 변경되면 currentIndexChanged signal이 발생하는 것을 막기위해 blockSignals 사용
        d->ui.projectsComboBox->blockSignals(true);
        d->ui.projectsComboBox->clear();
        d->ui.projectsComboBox->addItems(projects);
        d->ui.projectsComboBox->blockSignals(false);
    }

    void ProjectPanel::onChangeProjectSelection(const QString& title) {
        if (title.isEmpty()) {
            d->ui.descriptionEdit->clear();
            d->ui.descriptionEdit->setEnabled(false);
        } else {
            d->ui.descriptionEdit->setPlainText(d->control.GetCurrentProjectDescription());
            d->ui.descriptionEdit->setEnabled(true);
        }
    }

    void ProjectPanel::onUpdateDescription(const QString& text) {
        d->ui.descriptionEdit->setPlainText(text);
    }
}
