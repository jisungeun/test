#include "RightToolButtonsPanelControl.h"

#include <FileNameValidator.h>

#include <SystemStatus.h>
#include <AppData.h>
#include <ExperimentTemplateRepo.h>
#include <ExperimentController.h>
#include <ExperimentPresenter.h>
#include <ExperimentReaderPort.h>
#include <ExperimentWriterPort.h>
#include <ProfileController.h>
#include <ProfileCopierPlugin.h>

#include "ExperimentUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct RightToolButtonsPanelControl::Impl {
    };

    RightToolButtonsPanelControl::RightToolButtonsPanelControl() : d{ new Impl } {
        
    }

    RightToolButtonsPanelControl::~RightToolButtonsPanelControl() {
        
    }

    auto RightToolButtonsPanelControl::SaveAsTemplate(const QString& title) const -> bool {
		auto writer = Plugins::ExperimentIO::ExperimentWriterPort();
		auto controller = Interactor::ExperimentController(nullptr, nullptr, &writer);

		return controller.ExportToExperimentTemplate(title);
    }

    auto RightToolButtonsPanelControl::SaveExperiment() const -> bool {
        auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());

        auto writer = Plugins::ExperimentIO::ExperimentWriterPort();
		auto controller = Interactor::ExperimentController(&presenter, nullptr, &writer);

		return controller.SaveExperiment();
    }

    auto RightToolButtonsPanelControl::RestoreExperiment() const -> bool {
        auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());

		auto reader = Plugins::ExperimentIO::ExperimentReaderPort();
		auto controller = Interactor::ExperimentController(&presenter, &reader, nullptr);

		return controller.RestoreExperiment();
    }

    auto RightToolButtonsPanelControl::LoadedExperiment() const -> bool {
        auto appData = Entity::AppData::GetInstance();
        if (appData->GetExperiment() == nullptr) return false;
        return true;
    }

    auto RightToolButtonsPanelControl::HasUnsavedChanges() const -> bool {
        auto appData = Entity::AppData::GetInstance();
        auto loaded = appData->GetExperiment();
        auto origin = appData->GetOriginExperiment();

        bool hasUnsavedChanges = true;
        if (loaded && origin) {
            hasUnsavedChanges = !(*appData->GetExperiment() == *appData->GetOriginExperiment());
        }

        return hasUnsavedChanges;
    }
    
    auto RightToolButtonsPanelControl::IsRunningExperiment() const -> bool {
        return (AppEntity::SystemStatus::GetInstance()->GetExperiment() != nullptr);
    }

    auto RightToolButtonsPanelControl::CheckTemplateNameValidation(const QString& name, QString& errorMessage) const -> bool {
		const auto repo = Entity::ExperimentTemplateRepo::GetInstance();
		if (repo->GetTemplates().contains(name)) {
		    errorMessage = tr("This template name already exists.");
		    return false;
		}

		TC::FileNameValidator validator;
		if (!validator.IsValid(name)) {
            switch (validator.GetError()) {
            case TC::FileNameValid::MaxLenghtOverError:
                errorMessage = tr("This template name length exceeded the maximum length.");
                break;
            case TC::FileNameValid::WhitespaceAtTheEndError:
                errorMessage = tr("This template name ends in whitespace.");
                break;
            case TC::FileNameValid::InvalidCharacterContainedError:
                errorMessage = tr("This template name contains invalid characters.\n The following characters are not allowed: < > : \" / \\ | ? *");
                break;
            default:
                errorMessage = validator.GetErrorString();
                break;
            }

			return false;
        }

		return true;
    }

    auto RightToolButtonsPanelControl::IsCurrentVesselValid() const -> bool {
        auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());
        auto controller = Interactor::ExperimentController(&presenter, nullptr, nullptr);

        return controller.IsCurrentVesselValid();
    }

    auto RightToolButtonsPanelControl::IsCurrentMediumValid() const -> bool {
        auto updater = ExperimentUpdater::GetInstance();
		auto presenter = Interactor::ExperimentPresenter(updater.get());
        auto controller = Interactor::ExperimentController(&presenter, nullptr, nullptr);

        return controller.IsCurrentMediumValid();
    }

    auto RightToolButtonsPanelControl::IsCurrentProfileValid() const -> bool {
        auto updater = ExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter(updater.get());
        auto controller = Interactor::ExperimentController(&presenter, nullptr, nullptr);
        return controller.IsCurrentProfileValid();
    }

    auto RightToolButtonsPanelControl::IsProfilesExistInExperimentFolder() const -> bool {
        auto updater = ExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter(updater.get());
        auto controller = Interactor::ExperimentController(&presenter, nullptr, nullptr);
        return controller.IsProfilesExistInExperimentFolder();
    }

    auto RightToolButtonsPanelControl::CopyProfileFiles() const -> bool {
        auto updater = ExperimentUpdater::GetInstance();
        auto copier = Plugins::ProfileCopier::ProfileCopierPlugin();
        auto controller = Interactor::ProfileController(&copier);
        return controller.CopyProfiles(GetCurrentSampleTypeName());
    }

    auto RightToolButtonsPanelControl::GetCurrentSampleTypeName() const -> QString {
        return Entity::AppData::GetInstance()->GetExperiment()->GetSampleTypeName();
    }
}
