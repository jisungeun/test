#pragma once

#include <memory>

#include <IVesselMapView.h>
#include <VesselMap.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class VesselMapObserver;

    class VesselMapUpdater : public Interactor::IVesselMapView {
    public:
        using Pointer = std::shared_ptr<VesselMapUpdater>;

    protected:
        VesselMapUpdater();

    public:
        ~VesselMapUpdater ();

        static auto GetInstance()->Pointer;

        auto UpdateVesselMap(const TC::VesselMap::Pointer vesselMap)->void override;

        auto Register(VesselMapObserver* observer)->void;
        auto Deregister(VesselMapObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class VesselMapObserver;
    };
}