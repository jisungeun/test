#include "ProjectUpdater.h"

#include "ProjectObserver.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct ProjectUpdater::Impl {
        QList<ProjectObserver*> observers;
    };

    ProjectUpdater::ProjectUpdater() : d{ new Impl } {
        
    }

    ProjectUpdater::~ProjectUpdater() {
        
    }

    auto ProjectUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new ProjectUpdater() };
        return theInstance;
    }

    auto ProjectUpdater::UpdateList(const QList<QString>& projects) -> void {
        for (auto observer : d->observers) {
            observer->UpdateList(projects);
        }
    }

    auto ProjectUpdater::ChangeProjectSelection(const QString& title) -> void {
        for (auto observer : d->observers) {
            observer->ChangeProjectSelection(title);
        }
    }

    auto ProjectUpdater::UpdateDescription(const QString& description) -> void {
        for (auto observer : d->observers) {
            observer->UpdateDescription(description);
        }
    }

    auto ProjectUpdater::ProjectDeleted(bool isProjectEmpty) -> void {
        for (auto observer : d->observers) {
            observer->ProjectDeleted(isProjectEmpty);
        }
    }

    auto ProjectUpdater::Register(ProjectObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto ProjectUpdater::Deregister(ProjectObserver* observer) -> void {
        auto index = d->observers.indexOf(observer);
        if (index < 0) {
            return;
        }

        d->observers.removeAt(index);
    }
}