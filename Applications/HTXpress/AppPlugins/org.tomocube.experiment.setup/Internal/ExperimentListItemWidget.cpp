#include "ExperimentListItemWidget.h"

#include <QLabel>
#include <QHBoxLayout>

namespace HTXpress::AppPlugins::Experiment::Setup {
	struct ExperimentListItemWidget::Impl {
	    std::shared_ptr<QLabel> progressLabel{ nullptr };
		std::shared_ptr<QLabel> titleLabel{ nullptr };

		QString date;
		QString title;
		Progress progress {Progress::NotStarted};

		auto SetProgressIcon(Progress progress)->void;
	};

	auto ExperimentListItemWidget::Impl::SetProgressIcon(Progress newProgress) -> void {
		if (progressLabel == nullptr) return;

        auto progressImg = ":/img/tsx/experiment-x";
	    if (newProgress== +AppEntity::ExperimentProgress::InProgress) progressImg = ":/img/tsx/experiment-o";
		else if (newProgress== +AppEntity::ExperimentProgress::Done) progressImg = ":/img/tsx/list-check";

		progressLabel->setPixmap(QPixmap(progressImg));
    }


	ExperimentListItemWidget::ExperimentListItemWidget(const QString& date, const QString& title, Progress progress, QWidget* parent) : QWidget(parent), d{ new Impl } {
		d->date = date;
		d->title = title;
		d->progress = progress;

	    d->progressLabel = std::make_shared<QLabel>(this);
		d->progressLabel->setFixedSize(16, 16);	// TODO: qss에서 object name보고 크기 설정되도록

		d->SetProgressIcon(d->progress);

        d->titleLabel = std::make_shared<QLabel>(d->date + " " + d->title, this);
		d->titleLabel->setFixedHeight(16);

	    auto layout = new QHBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
	    layout->setSpacing(9);
		layout->addWidget(d->progressLabel.get());
		layout->addWidget(d->titleLabel.get());

		this->setLayout(layout);
    }

    ExperimentListItemWidget::~ExperimentListItemWidget() {
	    
	}

	auto ExperimentListItemWidget::GetTitle() const -> QString {
	    return d->title;
	}

	auto ExperimentListItemWidget::SetProgress(Progress progress) const -> void {
		d->progress = progress;
        d->SetProgressIcon(d->progress);
    }

}