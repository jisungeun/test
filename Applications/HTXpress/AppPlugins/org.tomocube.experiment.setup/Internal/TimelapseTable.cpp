#define LOGGER_TAG "[TimelapseTablePanel]"

#include "TimelapseTable.h"

#include <TCLogger.h>

#include "ProjectObserver.h"
#include "ExperimentObserver.h"
#include "TimelapseTableControl.h"
#include "ui_TimelapseTable.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct TimelapseTable::Impl {
        Ui::TimelapseTable ui;
        TimelapseTableControl control;

        std::shared_ptr<ProjectObserver> projectObserver{ nullptr };
        std::shared_ptr<ExperimentObserver> experimentObserver{ nullptr };
    };

    TimelapseTable::TimelapseTable(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->projectObserver = std::make_shared<ProjectObserver>(this);
        connect(d->projectObserver.get(), &ProjectObserver::sigProjectSelectionChanged, this, &TimelapseTable::onChangeProject);

        d->experimentObserver = std::make_shared<ExperimentObserver>(this);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigExperimentChanged, this, &TimelapseTable::onChangeExperiment);
    }

    TimelapseTable::~TimelapseTable() {
    }

    void TimelapseTable::onChangeProject(const QString& title) {
        Q_UNUSED(title)

        d->ui.timelapseTableWidget->Clear();
    }

    void TimelapseTable::onChangeExperiment(const AppEntity::Experiment::Pointer& experiment) {
        // single imaging 설정에 있는 channel 정보로 색 설정
        QMap<int32_t, QColor> flChannelColors;
        auto imagingCondition = experiment->GetSingleImagingCondition(AppEntity::ImagingType::FL2D);
        if (imagingCondition == nullptr) {
            imagingCondition = experiment->GetSingleImagingCondition(AppEntity::ImagingType::FL3D);
        }

        if (imagingCondition) {
            auto flCondition = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(imagingCondition);
            if (flCondition) {
                for (auto channel : flCondition->GetChannels()) {
                auto color = flCondition->GetColor(channel);
                flChannelColors.insert(channel, QColor(color.red, color.green, color.blue));
                }
            }
        }

        d->ui.timelapseTableWidget->SetFLChannelsColor(flChannelColors);

        // TODO: multi-vessel인 경우 vessel index 지정
        d->ui.timelapseTableWidget->SetScenario(experiment->GetScenario(0));
    }

}
