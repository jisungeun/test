#include <SystemStatus.h>

#include "LeftToolButtonsPanelControl.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct LeftToolButtonsPanelControl::Impl {
    };

    LeftToolButtonsPanelControl::LeftToolButtonsPanelControl() : d{std::make_unique<Impl>()} {
    }

    LeftToolButtonsPanelControl::~LeftToolButtonsPanelControl() {
    }

    auto LeftToolButtonsPanelControl::IsRunningExperiment() const -> bool {
        return (AppEntity::SystemStatus::GetInstance()->GetExperiment() != nullptr);
    }
}
