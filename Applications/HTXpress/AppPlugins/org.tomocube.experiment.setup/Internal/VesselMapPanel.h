#pragma once

#include <memory>

#include <QWidget>

#include <Experiment.h>
#include <VesselMap.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class VesselMapPanel : public QWidget {
        Q_OBJECT

    public:
        VesselMapPanel(QWidget* parent = nullptr);
        ~VesselMapPanel() override;

    protected slots:
        // connect to vessel map widget
        void onUpdateVesselIndices(const QList<int>& indices);

        // connect to observers
        void onUpdateUI();
        void onChangeProject(const QString& title);
        void onChangeExperiment(const AppEntity::Experiment::Pointer& experiment);
        void onChangeVesselType(const QString& type);
        void onChangeVesselCount(int count);
        void onAddWellGroupToVesselMap(const QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>>& indices);
        void onAddWellsToVesselMap(AppEntity::WellGroupIndex groupIndex, const QList<AppEntity::RowColumn>& indices);
        void onMoveWells(AppEntity::WellGroupIndex movingGroupIndex, AppEntity::WellGroupIndex targetGroupIndex, const QList<AppEntity::RowColumn>& wells);
        void onRemoveWellsToVesselMap(const QList<AppEntity::RowColumn>& indices);
        void onDeleteWellGroupsToVesselMap(const QList<AppEntity::WellGroupIndex>& indices);
        void onUpdateGroupNameList(AppEntity::WellGroupIndex groupIndex, const QString& groupName);
        void onClear();

        // connect to ui
        void onChangeCurrentVesselIndex(const QString& text);
        void onChangeSpecimenChecked(bool checked);
        void onAddNewWellGroup();
        void onAddWellsToWellGroup();
        void onRemoveWell();
        void onChangeGroupName(int groupIndex, const QString& name);
        void onChangeGroupColor(int groupIndex, const QColor& color);
        void onMoveGroup(int movingGroupIndex, int targetGroupIndex);
        void onChangeWellName(int wellIndex, const QString& name);
        void onShowROISetup();
        void onRoiListUpdated();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
