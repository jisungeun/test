#define LOGGER_TAG "[ExperimentExplorerPanel]"

#include "ExperimentExplorerPanel.h"

#include <QList>
#include <QLabel>
#include <QTableWidget>
#include <QHeaderView>
#include <QVBoxLayout>
#include <QTimer>

#include <TCLogger.h>
#include <UIUtility.h>
#include <MessageDialog.h>
#include <InputDialog.h>
#include <CustomDialog.h>

#include "ExperimentExplorerPanelControl.h"
#include "ExperimentListItemWidget.h"
#include "AppUIObserver.h"
#include "ProjectObserver.h"
#include "ExperimentObserver.h"

#include "ui_ExperimentExplorerPanel.h"

#include "ExperimentListItemDelegate.h"
#include "ExperimentTemplateRepo.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    namespace utility {
        bool SortExperiment(const Entity::ExperimentShortInfo& a, const Entity::ExperimentShortInfo& b) {
            if(a.GetDate() == b.GetDate()) {
                return a.GetTitle() < b.GetTitle();
            }
            return a.GetDate() < b.GetDate();
        }
    }
    struct ExperimentExplorerPanel::Impl {
        explicit Impl(Self* self) : self(self) {}

        Ui::ExperimentExplorerPanel ui;
        ExperimentExplorerPanelControl control;
        Self* self{nullptr};
        AppUIObserver::Pointer appUIObserver{ nullptr };
        ProjectObserver::Pointer projectObserver{ nullptr };
        ExperimentObserver::Pointer experimentObserver{ nullptr };

        auto SetEnableButtons(bool enabled) const -> void;
        auto UpdateExperimentList(const QList<Entity::ExperimentShortInfo>& infos) const -> void;
        auto SelectedExperimentTitle() const -> QString;
    };

    auto ExperimentExplorerPanel::Impl::SetEnableButtons(bool enabled) const -> void {
        ui.newButton->setEnabled(enabled);
        ui.copyButton->setEnabled(enabled);
        ui.newFromTemplateButton->setEnabled(enabled);
        ui.deleteButton->setEnabled(enabled);
    }

    auto ExperimentExplorerPanel::Impl::UpdateExperimentList(const QList<Entity::ExperimentShortInfo>& infos) const -> void {
        ui.experimentListWidget->blockSignals(true);

        ui.experimentListWidget->clear();

        const auto currentExperiment = control.GetLoadedExperimentTitle();
        QListWidgetItem* selectedItem = nullptr;

        // 실험 목록 아이템 추가
        auto experimentList = infos;
        std::sort(experimentList.begin(), experimentList.end(), utility::SortExperiment);

        for (const auto& info : experimentList) {
            auto itemWidget = new ExperimentListItemWidget(
                info.GetDate(),
                info.GetTitle(),
                info.GetProgress(),
                ui.experimentListWidget
            );

            auto item = new QListWidgetItem(ui.experimentListWidget);
            item->setData(Qt::UserRole, currentExperiment == info.GetTitle());
            ui.experimentListWidget->addItem(item);
            ui.experimentListWidget->setItemWidget(item, itemWidget);

            if (info.GetTitle() == currentExperiment) selectedItem = item;
        }

        // 선택 상태 설정
        ui.experimentListWidget->setCurrentItem(selectedItem);

        ui.experimentListWidget->blockSignals(false);
    }

    auto ExperimentExplorerPanel::Impl::SelectedExperimentTitle() const -> QString {
        auto currentItem = ui.experimentListWidget->currentItem();
        if (currentItem == nullptr) {
            return QString();
        }

        auto itemWidget = dynamic_cast<ExperimentListItemWidget*>(ui.experimentListWidget->itemWidget(currentItem));
        if (itemWidget == nullptr) {
            return QString();
        }

        return itemWidget->GetTitle();
    }

    ExperimentExplorerPanel::ExperimentExplorerPanel(QWidget* parent) : QWidget(parent), d{ std::make_unique<Impl>(this) }{
        d->ui.setupUi(this);

        d->ui.baseWidget->setObjectName("panel");
        d->ui.newButton->setObjectName("bt-light");
        d->ui.copyButton->setObjectName("bt-light");
        d->ui.newFromTemplateButton->setObjectName("bt-light");
        d->ui.deleteButton->setObjectName("bt-light");

        auto experimentListItemDelegate = new ExperimentListItemDelegate(d->ui.experimentListWidget);
        experimentListItemDelegate->SetActiveColor(QColor("#27a2bf"));
        d->ui.experimentListWidget->setItemDelegate(experimentListItemDelegate);

        connect(d->ui.newButton, &QPushButton::clicked, this, &ExperimentExplorerPanel::onAddExperiment);
        connect(d->ui.copyButton, &QPushButton::clicked, this, &ExperimentExplorerPanel::onCopyExperiment);
        connect(d->ui.newFromTemplateButton, &QPushButton::clicked, this, &ExperimentExplorerPanel::onAddExprimentFromTemplate);
        connect(d->ui.deleteButton, &QPushButton::clicked, this, &ExperimentExplorerPanel::onDeleteExperiment);
        connect(d->ui.experimentListWidget, &QListWidget::itemClicked, this, &ExperimentExplorerPanel::onChangeExperimentSelection);
        connect(d->ui.experimentListWidget, &QListWidget::itemDoubleClicked, this, &ExperimentExplorerPanel::onLoadExperiment);

        d->appUIObserver = std::make_shared<AppUIObserver>(this);
        d->projectObserver = std::make_shared<ProjectObserver>(this);
        d->experimentObserver = std::make_shared<ExperimentObserver>(this);

        connect(d->projectObserver.get(), &ProjectObserver::sigProjectSelectionChanged, this, &ExperimentExplorerPanel::onChangeProject);
        connect(d->projectObserver.get(), &ProjectObserver::sigProjectDeleted, this, &ExperimentExplorerPanel::onProjectedDeleted);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigExperimentListChanged, this, &ExperimentExplorerPanel::onUpdateExperimentList);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigExperimentChanged, this, &ExperimentExplorerPanel::onUpdateExperiment);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigError, this, &ExperimentExplorerPanel::onExperimentErrorOccurred);
        connect(d->appUIObserver.get(), &AppUIObserver::sigUpdatingUIRequested, this, &ExperimentExplorerPanel::onUpdateUI);
    }

    ExperimentExplorerPanel::~ExperimentExplorerPanel() {
        
    }

    void ExperimentExplorerPanel::onAddExperiment() {
        TC::InputDialog newExpDlg;
        connect(&newExpDlg, &TC::InputDialog::textValueChanged, [&newExpDlg](QString text) {
                                                         if (text.length() > 20) {
                                                             QSignalBlocker signalBlocker(newExpDlg);
                                                             newExpDlg.setTextValue(text.left(20));
                                                         }});
        newExpDlg.SetTitle(tr("New experiment"));
        newExpDlg.SetContent(tr("Enter new experiment name"));
        if(QDialog::DialogCode::Accepted != newExpDlg.exec()) return;

        const auto title = newExpDlg.textValue();
        if(title.isEmpty()) return;

        QString errorMessage;
        if (!d->control.CheckExperimentTitleValidation(title, errorMessage)) {
            QLOG_ERROR() << errorMessage;
            TC::MessageDialog::warning(this, tr("New Experiment"), errorMessage);
            return;
        }

        if (!d->control.AddExperiment(title)) {
            QString log = tr("Failed to create new experiment.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, "New Experiment", log);
        }
    }

    void ExperimentExplorerPanel::onAddExprimentFromTemplate() {
        auto templateTable = new QTableWidget;
        templateTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        templateTable->setSelectionBehavior(QAbstractItemView::SelectRows);
        templateTable->setSelectionMode(QAbstractItemView::SingleSelection);
        templateTable->setAlternatingRowColors(true);
        templateTable->setColumnCount(2);
        templateTable->setHorizontalHeaderLabels({tr("Name"), tr("Recent modified")});
        templateTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        templateTable->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);
        templateTable->verticalHeader()->setVisible(false);

        QString selectedTemplate;
        connect(templateTable, &QTableWidget::currentCellChanged, [templateTable, &selectedTemplate](int currentRow, int currentColumn, int previousRow, int previousColumn) {
            Q_UNUSED(currentColumn)
            Q_UNUSED(previousRow)
            Q_UNUSED(previousColumn)

            selectedTemplate = templateTable->item(currentRow, 0)->text();
        });

        // 실험 템플릿 정보를 테이블에 추가
        auto templateInfoIt = QMapIterator(d->control.GetTemplateInfos());
        while (templateInfoIt.hasNext()) {
            templateInfoIt.next();
            
            auto newRow = templateTable->rowCount();
            templateTable->setRowCount(newRow + 1);

            auto nameItem = new QTableWidgetItem(templateInfoIt.key());
            nameItem->setTextAlignment(Qt::AlignCenter);
            templateTable->setItem(newRow, 0, nameItem);

            auto dateItem = new QTableWidgetItem(templateInfoIt.value().toString("yyyy.MM.dd hh:mm"));
            dateItem->setTextAlignment(Qt::AlignCenter);
            templateTable->setItem(newRow, 1, dateItem);
        }

        // Template 선택 ui 표시
        TC::CustomDialog templateDlg;
        templateDlg.setFixedSize(450, 360);
        templateDlg.SetTitle("Select a template");
        templateDlg.SetContext(templateTable);
        templateDlg.SetStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        templateDlg.SetDefaultButton(QDialogButtonBox::Ok);
        if (!templateDlg.exec()) return;

        // 생성될 experiment 이름 입력
        TC::InputDialog newExpDlg;
        connect(&newExpDlg, &TC::InputDialog::textValueChanged, [&newExpDlg](QString text) {
                                                         if (text.length() > 20) {
                                                             QSignalBlocker signalBlocker(newExpDlg);
                                                             newExpDlg.setTextValue(text.left(20));
                                                         }});
        newExpDlg.SetTitle(tr("Add experiment"));
        newExpDlg.SetContent(tr("Enter new experiment name"));
        if(QDialog::DialogCode::Accepted != newExpDlg.exec()) return;

        const auto title = newExpDlg.textValue();

        QString errorMessage;
        if (!d->control.CheckExperimentTitleValidation(title, errorMessage)) {
            QLOG_ERROR() << errorMessage;
            TC::MessageDialog::warning(this, tr("New Experiment"), errorMessage);
            return;
        }

        if (!d->control.AddExperimentFromTemplate(title, selectedTemplate)) {
            // Todo: error message
        }
    }

    void ExperimentExplorerPanel::onCopyExperiment() {
        TC::InputDialog newExpDlg;
        connect(&newExpDlg, &TC::InputDialog::textValueChanged, [&newExpDlg](QString text) {
                                                         if (text.length() > 20) {
                                                             QSignalBlocker signalBlocker(newExpDlg);
                                                             newExpDlg.setTextValue(text.left(20));
                                                         }});
        newExpDlg.SetTitle(tr("Duplicate experiment"));
        newExpDlg.SetContent(tr("Enter new experiment name"));
        if(QDialog::DialogCode::Accepted != newExpDlg.exec()) return;

        const auto title = newExpDlg.textValue();

        QString errorMessage;
        if (!d->control.CheckExperimentTitleValidation(title, errorMessage)) {
            QLOG_ERROR() << errorMessage;
            TC::MessageDialog::warning(this, tr("Duplicate Experiment"), errorMessage);
            return;
        }

        for (auto index = 0; index < d->ui.experimentListWidget->count(); ++index) {
            auto item = d->ui.experimentListWidget->item(index);
            auto itemWidget = d->ui.experimentListWidget->itemWidget(item);

            auto experimentListItemWidget = qobject_cast<ExperimentListItemWidget*>(itemWidget);
            if (experimentListItemWidget->GetTitle() == title) {
                QString log = tr("Failed to duplicate experiment");
                QLOG_ERROR() << log;
                TC::MessageDialog::warning(this, tr("Duplicate Experiment"), log);
                return;
            }
        }

        if (!d->control.Copy(d->SelectedExperimentTitle(), title)) {
            QString log = tr("Failed to duplicate experiment");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Duplicate Experiment"), log);
        }
    }

    void ExperimentExplorerPanel::onDeleteExperiment() {
        const auto selectedItem = d->ui.experimentListWidget->currentItem();
        if (selectedItem == nullptr) {
            return;
        }

        const auto itemWidget = dynamic_cast<ExperimentListItemWidget*>(d->ui.experimentListWidget->itemWidget(selectedItem));
        if (itemWidget == nullptr) {
            return;
        }

        auto experimentTitle = itemWidget->GetTitle();
        if (!d->control.IsDeletable(experimentTitle)) {
            QString log = tr("Can't delete this experiment while running HTXProcessingServer.\nPlease close the program and try again.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Delete Experiment"), log);
            return;
        }

        const auto answer = TC::MessageDialog::question(
            this,
            tr("Delete Experiment"),
            tr("Are you sure you want to delete the experiment \'%1\' and all its contents?").arg(experimentTitle),
            TC::MessageDialog::StandardButton::Yes | TC::MessageDialog::StandardButton::No,
            TC::MessageDialog::StandardButton::No
        );

        if (TC::MessageDialog::StandardButton::Yes == answer) {
            const auto password = TC::InputDialog::getText(this, tr("Authentication"), tr("Enter your password"), QLineEdit::Password);
            if (!d->control.CheckPassword(password)) {
                TC::MessageDialog::warning(this, tr("Authentication"), tr("Invalid password is entered"));
                return;
            }
            d->control.Delete(experimentTitle);
        }
    }

    void ExperimentExplorerPanel::onChangeExperimentSelection(QListWidgetItem* item) {
        auto itemWidget = d->ui.experimentListWidget->itemWidget(item);
        auto experimentListWidget = dynamic_cast<ExperimentListItemWidget*>(itemWidget);
        if (experimentListWidget == nullptr) {
            return;    
        }

        d->control.ChangeExperimentSelection(experimentListWidget->GetTitle());
    }

    void ExperimentExplorerPanel::onLoadExperiment(QListWidgetItem* item) {
        auto itemWidget = d->ui.experimentListWidget->itemWidget(item);
        auto experimentListWidget = dynamic_cast<ExperimentListItemWidget*>(itemWidget);
        if (experimentListWidget == nullptr) {
            return;    
        }

        if (!d->control.Load(experimentListWidget->GetTitle())) {
            QString log = tr("Failed to open experiment.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Load Experiment"), log);
        }
    }

    void ExperimentExplorerPanel::onExperimentErrorOccurred(const QString& message) {
        TC::MessageDialog::warning(this, tr("Run Experiment"), message, QDialogButtonBox::Ok, QDialogButtonBox::Ok);
    }

    void ExperimentExplorerPanel::onChangeProject(const QString& title) {
        d->SetEnableButtons(!title.isEmpty());
    }

    void ExperimentExplorerPanel::onUpdateExperimentList(const QList<Entity::ExperimentShortInfo>& infos) {
        d->UpdateExperimentList(infos);
    }

    void ExperimentExplorerPanel::onUpdateExperiment(const AppEntity::Experiment::Pointer& experiment) {
        for (auto index = 0; index < d->ui.experimentListWidget->count(); ++index) {
            auto item = d->ui.experimentListWidget->item(index);
            if (item == nullptr) continue;

            auto itemWidget = dynamic_cast<ExperimentListItemWidget*>(d->ui.experimentListWidget->itemWidget(item));
            if (itemWidget == nullptr) continue;

            // 로드된 실험의 item에 userdata로 true 설정
            // delegate에서 로드된 것을 식별하기 위한 용도
            auto loaded = (experiment->GetTitle() == itemWidget->GetTitle());
            item->setData(Qt::UserRole, loaded);
            item->setData(Qt::BackgroundRole, loaded ? QColor("#27a2bf") : QColor(Qt::transparent));
        }
    }

    void ExperimentExplorerPanel::onUpdateUI() {
        d->SetEnableButtons(true);

        if (d->control.IsRunningExperiment()) {
            d->SetEnableButtons(false);
        } else {
            d->ui.experimentListWidget->clear();

            if (d->control.GetProjectTitle().isEmpty()) {
                d->SetEnableButtons(false);
            } else {
                d->UpdateExperimentList(d->control.GetExperimentInfos());
            }
        }
    }

    void ExperimentExplorerPanel::onProjectedDeleted(bool isProjectEmpty) {
        if(isProjectEmpty) {
            d->ui.experimentListWidget->clear();
        }
    }
}
