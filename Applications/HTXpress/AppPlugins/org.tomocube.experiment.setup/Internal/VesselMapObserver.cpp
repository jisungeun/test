#include "VesselMapObserver.h"

#include "VesselMapUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    VesselMapObserver::VesselMapObserver(QObject* parent) : QObject(parent) {
        VesselMapUpdater::GetInstance()->Register(this);
    }

    VesselMapObserver::~VesselMapObserver() {
        VesselMapUpdater::GetInstance()->Deregister(this);
    }

    auto VesselMapObserver::UpdateVesselModel(const TC::VesselMap::Pointer vesselMap) -> void {
        emit sigVesselModelChanged(vesselMap);
    }

    auto VesselMapObserver::ChangeVesselIndices(const QList<int>& indices)->void {
        emit sigVesselIndicesChanged(indices);
    }

    auto VesselMapObserver::ChangeExperiment(const QString& model, int count)->void {
        emit sigExperimentChanged(model, count);
    }
}
