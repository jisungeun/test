#include "ProjectObserver.h"

#include "ProjectUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    ProjectObserver::ProjectObserver(QObject* parent) : QObject(parent) {
        ProjectUpdater::GetInstance()->Register(this);
    }

    ProjectObserver::~ProjectObserver() {
        ProjectUpdater::GetInstance()->Deregister(this);
    }

    auto ProjectObserver::UpdateList(const QList<QString>& projects) -> void {
        emit sigProjectListChanged(projects);
    }

    auto ProjectObserver::ChangeProjectSelection(const QString& title) -> void {
        emit sigProjectSelectionChanged(title);
    }

    auto ProjectObserver::UpdateDescription(const QString& description) -> void {
        emit sigDescriptionChanged(description);
    }

    auto ProjectObserver::ProjectDeleted(bool isProjectEmpty) -> void {
        emit sigProjectDeleted(isProjectEmpty);
    }
}
