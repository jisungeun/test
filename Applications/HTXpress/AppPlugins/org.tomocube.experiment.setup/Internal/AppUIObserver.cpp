#include "AppUIObserver.h"

#include "AppUIUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    AppUIObserver::AppUIObserver(QObject* parent) : QObject(parent) {
        AppUIUpdater::GetInstance()->Register(this);
    }

    AppUIObserver::~AppUIObserver() {
        AppUIUpdater::GetInstance()->Deregister(this);
    }

    auto AppUIObserver::UpdateUI() -> void {
        emit sigUpdatingUIRequested();
    }
}
