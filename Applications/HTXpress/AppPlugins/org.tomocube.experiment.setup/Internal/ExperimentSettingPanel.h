#pragma once

#include <memory>

#include <QWidget>

#include <Experiment.h>
#include <MediumData.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class ExperimentSettingPanel : public QWidget {
        Q_OBJECT

    public:
        using Self = ExperimentSettingPanel;
        using Pointer = std::shared_ptr<Self>;

        explicit ExperimentSettingPanel(QWidget* parent = nullptr);
        ~ExperimentSettingPanel() override;

    protected slots:
        void onChangeTitle();
        void onChangeMedium(int index);
        void onChangeVesselHodler(const QString& vesselHolder);
        void onChangeNumberOfVessel(int value);
        void onChangeSampleType(const QString& sampleType);

        void onUpdateUI();
        void onChangeProject(const QString& title);
        void onUpdateExperiment(const AppEntity::Experiment::Pointer& experiment);
        void onUpdateVesselTypeList(const QStringList& vessels);
        void onDeleteCurrentExperiment();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
