#pragma once
#include <memory>
#include <QWidget>

#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
	class RightToolButtonsPanel : public QWidget {
	    Q_OBJECT

	public:
		RightToolButtonsPanel(QWidget* parent = nullptr);
		~RightToolButtonsPanel();

	protected slots:
		void onSaveAsTemplate();
		void onSaveExperiment();
		void onRestoreExperiment();

		void onUpdateUI();
		void onExperimentChanged(const AppEntity::Experiment::Pointer& experiment);
		void onDeleteCurrentExperiment();
		void onExperimentModified();
		void onExperimentSaved(bool saved, const QString& message);
		void onRunExperiment();

	signals:
		void sigRunExperiment();

	private:
		struct Impl;
		std::shared_ptr<Impl> d;
	};
}