#include "ExperimentObserver.h"
#include "ExperimentUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    ExperimentObserver::ExperimentObserver(QObject* parent) : QObject(parent) {
        ExperimentUpdater::GetInstance()->Register(this);
    }

    ExperimentObserver::~ExperimentObserver() {
        ExperimentUpdater::GetInstance()->Deregister(this);
    }

    auto ExperimentObserver::UpdateExperiment(const AppEntity::Experiment::Pointer& experiment) -> void {
        emit sigExperimentChanged(experiment);
    }

    auto ExperimentObserver::UpdateList(const QList<Entity::ExperimentShortInfo>& data) -> void {
        emit sigExperimentListChanged(data);
    }

    auto ExperimentObserver::UpdateTitle(const QString& title) -> void {
        emit sigExperimentTitleChanged(title);
        emit sigExperimentModified();
    }

    auto ExperimentObserver::UpdateVesselType(const QString& type) -> void {
        emit sigExperimentVesselTypeChanged(type);
        emit sigExperimentModified();
    }

    auto ExperimentObserver::UpdateVesselCount(int count) -> void {
        emit sigExperimentVesselCountChanged(count);
        emit sigExperimentModified();
    }

    auto ExperimentObserver::UpdateVessels(const QStringList& vessels) -> void {
        emit sigVesselsUpdated(vessels);
        emit sigExperimentModified();
    }

    auto ExperimentObserver::UpdateMedium(const AppEntity::Medium& medium) -> void {
        emit sigExperimentMediumChanged(medium);
        emit sigExperimentModified();
    }

    auto ExperimentObserver::UpdateDeleteProgress(int32_t totalCount, int32_t deleteCount) -> void {
        emit sigDeleteProgressUpdated(totalCount, deleteCount);
    }

    auto ExperimentObserver::UpdateSampleTypeName(const AppEntity::SampleTypeName& sampleTypeName) -> void {
        emit sigExperimentSampleTypeNameChanged(sampleTypeName);
        emit sigExperimentModified();
    }

    auto ExperimentObserver::AddWellGroup(const QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>>& groupIndices) -> void {
        emit sigWellGroupAdded(groupIndices);
        emit sigExperimentModified();
    }

    auto ExperimentObserver::AddWellsToWellGroup(AppEntity::WellGroupIndex groupIndex, const QList<AppEntity::RowColumn>& indices) -> void {
        emit sigWellsAdded(groupIndex, indices);
        emit sigExperimentModified();
    }

    auto ExperimentObserver::MoveWells(AppEntity::WellGroupIndex movingGroupIndex, AppEntity::WellGroupIndex targetGroupIndex, const QList<AppEntity::RowColumn>& wells) -> void {
        emit sigWellsMoved(movingGroupIndex, targetGroupIndex, wells);
        emit sigExperimentModified();
    }

    auto ExperimentObserver::RemoveWells(const QList<AppEntity::RowColumn>& indices) -> void {
        emit sigWellsRemoved(indices);
        emit sigExperimentModified();
    }

    auto ExperimentObserver::DeleteWellGroups(const QList<AppEntity::WellGroupIndex>& indices) -> void {
        emit sigWellGroupsDeleted(indices);
        emit sigExperimentModified();
    }

    auto ExperimentObserver::ChangeWellGroupName(AppEntity::WellGroupIndex groupIndex, const QString& groupName) -> void {
        emit sigWellGroupNameChanged(groupIndex, groupName);
        emit sigExperimentModified();
    }

    auto ExperimentObserver::Clear() -> void {
        emit sigClear();
    }

    auto ExperimentObserver::ExperimentSaved(bool saved, const QString& message) -> void {
        emit sigExperimentSaved(saved, message);
    }

    auto ExperimentObserver::ChangeWellName(AppEntity::VesselIndex vesselIndex, AppEntity::WellIndex wellIndex, const QString& wellName) -> void {
        Q_UNUSED(vesselIndex)
        Q_UNUSED(wellIndex)
        Q_UNUSED(wellName)
        emit sigExperimentModified();
    }

    auto ExperimentObserver::RoiListUpdated() -> void {
        emit sigRoiListUpdated();
        emit sigExperimentModified();
    }

    auto ExperimentObserver::Error(const QString& errorMessage) -> void {
        emit sigError(errorMessage);
    }
}
