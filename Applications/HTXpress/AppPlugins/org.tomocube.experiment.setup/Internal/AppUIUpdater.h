#pragma once

#include <memory>

#include <IAppUIView.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class AppUIObserver;

    class AppUIUpdater : public Interactor::IAppUIView {
    public:
        using Pointer = std::shared_ptr<AppUIUpdater>;

    protected:
        AppUIUpdater();

    public:
        ~AppUIUpdater();

        static auto GetInstance()->Pointer;

        auto UpdateUI() const -> void override;

        auto Register(AppUIObserver* observer)->void;
        auto Deregister(AppUIObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class ProjectObserver;
    };
}