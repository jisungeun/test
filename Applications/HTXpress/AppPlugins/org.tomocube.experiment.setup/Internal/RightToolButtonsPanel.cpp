#define LOGGER_TAG "[RightToolButtonsPanel]"

#include "RightToolButtonsPanel.h"

#include <MessageDialog.h>
#include <InputDialog.h>
#include <TCLogger.h>

#include "AppUIObserver.h"
#include "ExperimentObserver.h"
#include "RightToolButtonsPanelControl.h"
#include "ui_RightToolButtonsPanel.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
	struct RightToolButtonsPanel::Impl {
		Ui::RightToolButtonsPanel ui;
		RightToolButtonsPanelControl control;

		AppUIObserver::Pointer appUIObserver;
		ExperimentObserver::Pointer experimentObserver;
	};

	RightToolButtonsPanel::RightToolButtonsPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
		d->ui.setupUi(this);

		d->ui.baseWidget->setObjectName("panel");
		d->ui.saveAsTemplateButton->setObjectName("bt-tsx-saveAsTemplate");
		d->ui.saveExperimentButton->setObjectName("bt-tsx-saveExperiment");
		d->ui.restoreExperimentButton->setObjectName("bt-tsx-restoreExperiment");
		d->ui.runSetupPositionButton->setObjectName("bt-tsx-setupPosition");

		d->appUIObserver = std::make_shared<AppUIObserver>(this);
		d->experimentObserver = std::make_shared<ExperimentObserver>(this);

		d->ui.saveExperimentButton->setDisabled(true);
		d->ui.restoreExperimentButton->setDisabled(true);
		d->ui.runSetupPositionButton->setDisabled(true);
		d->ui.saveAsTemplateButton->setDisabled(true);

		connect(d->ui.saveAsTemplateButton, &QPushButton::clicked, this, &RightToolButtonsPanel::onSaveAsTemplate);
		connect(d->ui.saveExperimentButton, &QPushButton::clicked, this, &RightToolButtonsPanel::onSaveExperiment);
		connect(d->ui.restoreExperimentButton, &QPushButton::clicked, this, &RightToolButtonsPanel::onRestoreExperiment);
		connect(d->ui.runSetupPositionButton, &QPushButton::clicked, this, &RightToolButtonsPanel::onRunExperiment);

        connect(d->appUIObserver.get(), &AppUIObserver::sigUpdatingUIRequested, this, &RightToolButtonsPanel::onUpdateUI);
		connect(d->experimentObserver.get(), &ExperimentObserver::sigExperimentChanged, this, &RightToolButtonsPanel::onExperimentChanged);
		connect(d->experimentObserver.get(), &ExperimentObserver::sigExperimentModified, this, &RightToolButtonsPanel::onExperimentModified);
		connect(d->experimentObserver.get(), &ExperimentObserver::sigExperimentSaved, this, &RightToolButtonsPanel::onExperimentSaved);
		connect(d->experimentObserver.get(), &ExperimentObserver::sigClear, this, &RightToolButtonsPanel::onDeleteCurrentExperiment);
    }

    RightToolButtonsPanel::~RightToolButtonsPanel() {
        
    }

	void RightToolButtonsPanel::onSaveAsTemplate() {
		TC::InputDialog newTemplateNameDialog;
	    newTemplateNameDialog.SetTitle(tr("Save As Template"));
        newTemplateNameDialog.SetContent(tr("Enter new template name"));
        if(QDialog::DialogCode::Accepted != newTemplateNameDialog.exec()) return;

        const auto title = newTemplateNameDialog.textValue();

	    QString errorMessage;
		if (!d->control.CheckTemplateNameValidation(title, errorMessage)) {
		    QLOG_ERROR() << errorMessage;
            TC::MessageDialog::warning(this, tr("Save as Template"), errorMessage);
			return;
		}

	    if (!d->control.SaveAsTemplate(title)) {
	        QString log = tr("Failed to save as template.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Save as Template"), log);
	    }
	}

	void RightToolButtonsPanel::onSaveExperiment() {
		if (!d->control.SaveExperiment()) {
		    QString log = tr("Failed to save experiment.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Save Experiment"), log);
		}

		d->control.CopyProfileFiles();
	}

    void RightToolButtonsPanel::onRestoreExperiment() {
		if(!d->control.RestoreExperiment()) {
			QString log = tr("Failed to restore experiment.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Restore Experiment"), log);
		}
    }

	void RightToolButtonsPanel::onUpdateUI() {
		d->ui.runSetupPositionButton->setText(tr("Run Experiment"));

		d->ui.saveAsTemplateButton->setEnabled(false);
		d->ui.restoreExperimentButton->setEnabled(false);
        d->ui.saveExperimentButton->setEnabled(false);
		d->ui.runSetupPositionButton->setEnabled(false);

		if (d->control.IsRunningExperiment()) {
			d->ui.runSetupPositionButton->setText(tr("Back to Experiment"));
		    d->ui.runSetupPositionButton->setEnabled(true);
		} else {
		    if (d->control.LoadedExperiment()) {
		        d->ui.saveAsTemplateButton->setEnabled(true);

                if (d->control.HasUnsavedChanges()) {
                    d->ui.saveExperimentButton->setEnabled(true);
		            d->ui.restoreExperimentButton->setEnabled(true);
                } else {
		            d->ui.runSetupPositionButton->setEnabled(true);
                }
            }
		}
    }

    void RightToolButtonsPanel::onExperimentChanged(const AppEntity::Experiment::Pointer& experiment) {
		Q_UNUSED(experiment)
		d->ui.saveExperimentButton->setEnabled(false);
		d->ui.restoreExperimentButton->setEnabled(false);
		d->ui.runSetupPositionButton->setEnabled(true);
		d->ui.saveAsTemplateButton->setEnabled(true);
    }

	void RightToolButtonsPanel::onDeleteCurrentExperiment() {
		d->ui.saveAsTemplateButton->setEnabled(false);
        d->ui.saveExperimentButton->setEnabled(false);
		d->ui.restoreExperimentButton->setEnabled(false);
		d->ui.runSetupPositionButton->setEnabled(false);
    }

    void RightToolButtonsPanel::onExperimentModified() {
		d->ui.saveExperimentButton->setEnabled(true);
		d->ui.restoreExperimentButton->setEnabled(true);
		d->ui.runSetupPositionButton->setEnabled(false);
    }

    void RightToolButtonsPanel::onExperimentSaved(bool saved, const QString& message) {
		if(saved) {	    
		    d->ui.saveExperimentButton->setEnabled(false);
		    d->ui.restoreExperimentButton->setEnabled(false);
		    d->ui.runSetupPositionButton->setEnabled(true);
		} else {
			auto log = QString(tr("It fails to save the experiment.\n%1")).arg(message);
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Save Experiment"), log);
		}
    }

	void RightToolButtonsPanel::onRunExperiment() {
		if (!d->control.IsRunningExperiment()) {
		    if(!d->control.IsCurrentVesselValid()) {
			    return;
		    }
			if(!d->control.IsCurrentMediumValid()) {
			    return;
			}
			if(!d->control.IsCurrentProfileValid()) {
			    return;
			}
			if(!d->control.IsProfilesExistInExperimentFolder()) {
			    const auto infoMessage = tr("Because the sample type specified in the current experiment '%1' does not exist in the folder, it will be copied.").arg(d->control.GetCurrentSampleTypeName());
				TC::MessageDialog::information(this, "Run Experiment", infoMessage);
				if(!d->control.CopyProfileFiles()) {
				    return;
				}
			}
		}

        emit sigRunExperiment();
    }
}
