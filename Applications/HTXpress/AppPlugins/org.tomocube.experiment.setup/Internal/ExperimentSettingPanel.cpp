#define LOGGER_TAG "[ExperimentSettingPanel]"

#include "ExperimentSettingPanel.h"

#include <QList>
#include <QListView>
#include <QMouseEvent>
#include <QStringListModel>

#include <TCLogger.h>
#include <MessageDialog.h>
#include <UIUtility.h>

#include "ExperimentSettingPanelControl.h"
#include "AppUIObserver.h"
#include "ProjectObserver.h"
#include "ExperimentObserver.h"

#include "ui_ExperimentSettingPanel.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    const QString defaultSampleTypeName = "Cell";

    struct ExperimentSettingPanel::Impl {
        explicit Impl(Self* self) :self(self) {}
        Self* self{};
        Ui::ExperimentSettingPanel ui;
        ExperimentSettingPanelControl control;
          
        AppUIObserver::Pointer appUIObserver;
        ProjectObserver::Pointer projectObserver;
        ExperimentObserver::Pointer experimentObserver;

        auto InitUi()->void;
        auto SetUIEnabled(bool enabled)->void;
        auto ClearUI()->void;
        auto ConvertMediumDataToText(const AppEntity::MediumData& medium) -> QString;
        auto SetDefaultSampleTypeTop() -> void;
    };

    auto ExperimentSettingPanel::Impl::InitUi() -> void {
        // set styles
        ui.baseWidget->setObjectName("panel");

        ui.experimentNameLabel->setObjectName("label-h5");
        ui.mediumLabel->setObjectName("label-h5");
        ui.vesselTypeLabel->setObjectName("label-h5");
        ui.vesselCountLabel->setObjectName("label-h5");
        ui.sampleTypeLabel->setObjectName("label-h5");

        ui.experimentNameEdit->setReadOnly(true);    // experiment 로드된 경우에만 수정가능
        SetUIEnabled(false);

        // multi-vessel 기능 정식 지원할때까지 숨김 처리
        ui.vesselCountLabel->setHidden(true);
        ui.vesselCountSpinBox->setHidden(true);
    }

    auto ExperimentSettingPanel::Impl::SetUIEnabled(bool enabled) -> void {
        ui.experimentNameEdit->setEnabled(enabled);
        ui.mediumComboBox->setEnabled(enabled);
        ui.vesselTypeComboBox->setEnabled(enabled);
        ui.vesselCountSpinBox->setEnabled(enabled);
        ui.sampleTypeComboBox->setEnabled(enabled);
    }

    auto ExperimentSettingPanel::Impl::ClearUI() -> void {
        TC::SilentCall(ui.experimentNameEdit)->clear();
        TC::SilentCall(ui.mediumComboBox)->setCurrentIndex(-1);
        TC::SilentCall(ui.vesselTypeComboBox)->setCurrentIndex(-1);
        TC::SilentCall(ui.vesselCountSpinBox)->clear();
        TC::SilentCall(ui.sampleTypeComboBox)->setCurrentIndex(-1);
    }

    auto ExperimentSettingPanel::Impl::ConvertMediumDataToText(const AppEntity::MediumData& medium) -> QString {
        return QString("%1(%2)").arg(medium.GetName()).arg(medium.GetRI());
    }

    auto ExperimentSettingPanel::Impl::SetDefaultSampleTypeTop() -> void {
        const auto model = dynamic_cast<QStringListModel*>(ui.sampleTypeComboBox->model());
        if (model) {
            std::sort(model->stringList().begin() + 1, model->stringList().end());
        }

        const auto defaultIndex = ui.sampleTypeComboBox->findText(defaultSampleTypeName);
        if (defaultIndex >= 0) {
            ui.sampleTypeComboBox->removeItem(defaultIndex);
            ui.sampleTypeComboBox->insertItem(0, defaultSampleTypeName);
        }
    }

    ExperimentSettingPanel::ExperimentSettingPanel(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>(this)} {
        d->ui.setupUi(this);
        d->InitUi();
        
        connect(d->ui.experimentNameEdit, &QLineEdit::returnPressed, this, &ExperimentSettingPanel::onChangeTitle);
        connect(d->ui.mediumComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &ExperimentSettingPanel::onChangeMedium);
        connect(d->ui.vesselTypeComboBox, &QComboBox::currentTextChanged, this, &ExperimentSettingPanel::onChangeVesselHodler);
        connect(d->ui.vesselCountSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &ExperimentSettingPanel::onChangeNumberOfVessel);
        connect(d->ui.sampleTypeComboBox, &QComboBox::currentTextChanged, this, &ExperimentSettingPanel::onChangeSampleType);

        d->appUIObserver = std::make_shared<AppUIObserver>(this);
        d->projectObserver = std::make_shared<ProjectObserver>(this);
        d->experimentObserver = std::make_shared<ExperimentObserver>(this);

        connect(d->appUIObserver.get(), &AppUIObserver::sigUpdatingUIRequested, this, &ExperimentSettingPanel::onUpdateUI);
        connect(d->projectObserver.get(), &ProjectObserver::sigProjectSelectionChanged, this, &ExperimentSettingPanel::onChangeProject);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigExperimentChanged, this, &ExperimentSettingPanel::onUpdateExperiment);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigVesselsUpdated, this, &ExperimentSettingPanel::onUpdateVesselTypeList);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigClear, this, &ExperimentSettingPanel::onDeleteCurrentExperiment);
    }

    ExperimentSettingPanel::~ExperimentSettingPanel() {
        
    }

    void ExperimentSettingPanel::onChangeTitle() {
        const auto originTitle = d->control.GetLoadedExperiment()->GetTitle();
        const auto newTitle = d->ui.experimentNameEdit->text();

        if (originTitle.compare(newTitle, Qt::CaseInsensitive) == 0) {
            return;
        }

        if (d->control.ExistImagingData(originTitle)) {
            QString log = tr("Can't rename this experiment.\nThere is imaging data in this experiment.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Change Experiment Name"), log);
            TC::SilentCall(d->ui.experimentNameEdit)->setText(originTitle);
            return;
        }

        QString errorMessage;
        if (!d->control.CheckExperimentTitleValidation(newTitle, errorMessage)) {
            QLOG_ERROR() << errorMessage;
            TC::MessageDialog::warning(this, tr("Change Experiment Name"), errorMessage);
            TC::SilentCall(d->ui.experimentNameEdit)->setText(originTitle);
            return;
        }

        if (!d->control.ChangeTitle(newTitle)) {
            QString log = tr("Failed to change name.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Change Experiment Name"), log);
            
            TC::SilentCall(d->ui.experimentNameEdit)->setText(originTitle);
        }
    }

    void ExperimentSettingPanel::onChangeMedium(int index) {
        auto mediumData = d->ui.mediumComboBox->itemData(index).toJsonObject();
        if (!d->control.ChangeMedium(mediumData["Name"].toString(), mediumData["RI"].toDouble())) {
            QString log = tr("Failed to change medium.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Change Medium"), log);
        }
    }

    void ExperimentSettingPanel::onChangeVesselHodler(const QString& vesselHolder) {
        if (!d->control.ChangeVesselHolder(vesselHolder)) {
            QString log = tr("Failed to change vessel type.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Change Vessel Type"), log);
        }
    }

    void ExperimentSettingPanel::onChangeNumberOfVessel(int value) {
        if (!d->control.ChangeNumberOfVessel(value)) {
            QString log = tr("Failed to change number of vessels.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Change Number Of Vessels"), log);
        }
    }
    
    void ExperimentSettingPanel::onChangeSampleType(const QString& sampleTypeName) {
        if (!d->control.ChangeSampleType(sampleTypeName)) {
            QString log = tr("Failed to change sample type.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Change SampleType"), log);
        }
        else {
            QLOG_INFO() << tr("Sample type changed to %1").arg(sampleTypeName);
        }
    }


    void ExperimentSettingPanel::onUpdateUI() {
        d->ClearUI();

        const auto projectTitle = d->control.GetCurrentProjectTitle();
        if (projectTitle.isEmpty()) {
            d->SetUIEnabled(false);
        } else {
            bool enabled = true;
            // 선택된 실험 설정 표시
            const auto experiment = d->control.GetLoadedExperiment();
            if (experiment == nullptr) {
                enabled = false;
            } else {
                TC::SilentCall(d->ui.experimentNameEdit)->setText(experiment->GetTitle());

                const auto mediumText = d->ConvertMediumDataToText(experiment->GetMedium());
                const auto mediumIndex = d->ui.mediumComboBox->findText(mediumText);
                TC::SilentCall(d->ui.mediumComboBox)->setCurrentIndex((mediumIndex >= 0) ? mediumIndex : -1);

                if (experiment->GetVessel()) {
                    TC::SilentCall(d->ui.vesselTypeComboBox)->setCurrentText(experiment->GetVessel()->GetName());
                }

                TC::SilentCall(d->ui.vesselCountSpinBox)->setValue(experiment->GetVesselCount());
                TC::SilentCall(d->ui.sampleTypeComboBox)->setCurrentText(experiment->GetSampleTypeName());

                // timelapse imaging 중에는 UI 비활성화
                if (d->control.IsRunningExperiment()) enabled = false;
            }

            d->SetUIEnabled(enabled);
        }
    }

    void ExperimentSettingPanel::onChangeProject(const QString& title) {
        Q_UNUSED(title)

        d->ClearUI();
        d->SetUIEnabled(false);
    }

    void ExperimentSettingPanel::onUpdateExperiment(const AppEntity::Experiment::Pointer& experiment) {
        d->ui.experimentNameEdit->setReadOnly(false);
        TC::SilentCall(d->ui.experimentNameEdit)->setText(experiment->GetTitle());

        // medium repo 내용으로 list 갱신
        TC::SilentCall(d->ui.mediumComboBox)->clear();

        const auto currentMedium = experiment->GetMedium();
        const auto currentMediumText = d->ConvertMediumDataToText(currentMedium);

        bool foundMedium = false;
        for (auto& medium : d->control.GetMediumList()) {
            const auto mediumItemText = d->ConvertMediumDataToText(medium);
            TC::SilentCall(d->ui.mediumComboBox)->addItem(
                mediumItemText,
                QJsonObject{ {"Name", medium.GetName()}, {"RI", medium.GetRI()} }
            );

            if (medium == currentMedium) foundMedium = true;
        }

        // experiment의 medium 정보가 repo에 없을 경우 변경하도록 알림
        if (!foundMedium && !currentMedium.GetName().isEmpty()) {
            TC::SilentCall(d->ui.mediumComboBox)->setCurrentIndex(-1);
            if(TC::MessageDialog::information(this, 
                                           tr("Non-Registered Medium"),
                                           tr("%1(%2) of %3 is not registered on the medium list.")
                                           .arg(currentMedium.GetName())
                                           .arg(currentMedium.GetRI())
                                           .arg(experiment->GetTitle()))) {
            d->ui.mediumComboBox->showPopup();
            }
        }

        // experiment의 medium 정보를 current item으로 설정
        const auto mediumIndex = d->ui.mediumComboBox->findText(currentMediumText);
        TC::SilentCall(d->ui.mediumComboBox)->setCurrentIndex((mediumIndex >= 0) ? mediumIndex : -1);

        if(!d->control.UpdateVessels()) {
            QLOG_ERROR() << "Failed to update vessel list";
        }

        // experiment의 vessel로 current vessel 설정
        if (experiment->GetVessel()) {
            TC::SilentCall(d->ui.vesselTypeComboBox)->setCurrentText(experiment->GetVessel()->GetName());
        }
        TC::SilentCall(d->ui.vesselCountSpinBox)->setValue(experiment->GetVesselCount());

        d->ui.sampleTypeComboBox->blockSignals(true);
        d->ui.sampleTypeComboBox->clear();
        d->ui.sampleTypeComboBox->addItems(d->control.GetSampleTypeNameList());
        d->SetDefaultSampleTypeTop();
        d->ui.sampleTypeComboBox->setCurrentText(experiment->GetSampleTypeName());
        d->ui.sampleTypeComboBox->blockSignals(false);

        d->SetUIEnabled(!d->control.IsRunningExperiment());
    }

    void ExperimentSettingPanel::onUpdateVesselTypeList(const QStringList& vessels) {
        d->ui.vesselTypeComboBox->blockSignals(true);
        d->ui.vesselTypeComboBox->clear();
        d->ui.vesselTypeComboBox->addItems(vessels);
        d->ui.vesselTypeComboBox->setCurrentIndex(-1);
        d->ui.vesselTypeComboBox->blockSignals(false);
    }

    void ExperimentSettingPanel::onDeleteCurrentExperiment() {
        d->ClearUI();
        d->SetUIEnabled(false);
    }
}
