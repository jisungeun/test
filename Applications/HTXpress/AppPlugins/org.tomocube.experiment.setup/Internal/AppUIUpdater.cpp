#include "AppUIUpdater.h"

#include "AppUIObserver.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct AppUIUpdater::Impl {
        QList<AppUIObserver*> observers;
    };

    AppUIUpdater::AppUIUpdater() : d{ new Impl } {
        
    }

    AppUIUpdater::~AppUIUpdater() {
        
    }

    auto AppUIUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new AppUIUpdater() };
        return theInstance;
    }

    auto AppUIUpdater::UpdateUI() const -> void {
        for (auto observer : d->observers) {
            observer->UpdateUI();
        }
    }

    auto AppUIUpdater::Register(AppUIObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto AppUIUpdater::Deregister(AppUIObserver* observer) -> void {
        auto index = d->observers.indexOf(observer);
        if (index < 0) {
            return;
        }

        d->observers.removeAt(index);
    }
}