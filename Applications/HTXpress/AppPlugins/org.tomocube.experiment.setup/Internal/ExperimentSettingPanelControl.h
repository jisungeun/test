#pragma once

#include <memory>

#include <QString>

#include <FileNameValidator.h>
#include <Experiment.h>
#include <MediumData.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class ExperimentSettingPanelControl {
        Q_DECLARE_TR_FUNCTIONS(ExperimentSettingPanelControl)

    public:
        ExperimentSettingPanelControl();
        ~ExperimentSettingPanelControl();

        auto UpdateVessels() const->bool;
        auto ChangeTitle(const QString& title) const->bool;
        auto ChangeMedium(const QString& name, double value) const -> bool;
        auto ChangeVesselHolder(const QString& vesselTitle) const -> bool;
        auto ChangeNumberOfVessel(const int32_t number) const -> bool;
        auto ChangeSampleType(const QString& sampleTypeName) const -> bool;

        auto GetCurrentProjectTitle() const -> QString;
        auto GetLoadedExperiment() const -> AppEntity::Experiment::Pointer;
        auto GetMediumList() const -> QList<AppEntity::MediumData>;

        auto IsRunningExperiment() const -> bool;
        auto ExistImagingData(const QString& experiment) const -> bool;
        auto CheckExperimentTitleValidation(const QString& title, QString& errorMessage) const -> bool;

        auto GetSampleTypeNameList() const -> QStringList;
        auto GetCurrentProfileName() const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
