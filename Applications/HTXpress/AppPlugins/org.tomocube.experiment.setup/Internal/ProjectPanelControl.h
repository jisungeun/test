#pragma once

#include <memory>

#include <QString>

#include <FileNameValidator.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class ProjectPanelControl {
    public:
        ProjectPanelControl();
        ~ProjectPanelControl();

        auto CreateProject(const QString& title)->bool;
        auto DeleteProject(const QString& title)->bool;
        auto SetCurrentProject(const QString& title)->bool;
        auto SetDescription(const QString& title, const QString& description)->bool;

        auto GetCurrentProject() const -> QString;
        auto GetCurrentProjectDescription() const -> QString;

        auto IsExistProject(const QString& name) const -> bool;
        auto IsValidName(const QString& name, QString& errorString) const -> TC::FileNameValid;
        auto HasExperimentChanges() const -> bool;

        auto IsRunningExperiment() const -> bool;
        auto GetBusyExperiments() const -> QStringList;
        auto CheckPassword(const QString& password) const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
