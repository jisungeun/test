#define LOGGER_TAG "[LeftToolButtonsPanel]"

#include "LeftToolButtonsPanel.h"

#include <TCLogger.h>
#include <MessageDialog.h>
#include <InputDialog.h>

#include "AppUIObserver.h"
#include "LeftToolButtonsPanelControl.h"
#include "ui_LeftToolButtonsPanel.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct LeftToolButtonsPanel::Impl {
        Ui::LeftToolButtonsPanel ui;
        LeftToolButtonsPanelControl control;
        AppUIObserver::Pointer appUIObserver{};
    };

    LeftToolButtonsPanel::LeftToolButtonsPanel(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui.setupUi(this);

        d->ui.baseWidget->setObjectName("panel");

        d->ui.runDataNavigationButton->setObjectName("bt-tsx-dataNavigation");
        d->ui.goToHome->setObjectName("bt-tsx-home");

        d->appUIObserver = std::make_shared<AppUIObserver>(this);

        connect(d->ui.runDataNavigationButton, &QPushButton::clicked, this, &LeftToolButtonsPanel::sigOpenDataNavigation);
        connect(d->ui.goToHome, &QPushButton::clicked, this, &LeftToolButtonsPanel::sigGoToHome);
        connect(d->appUIObserver.get(), &AppUIObserver::sigUpdatingUIRequested, this, &LeftToolButtonsPanel::onUpdateUI);
    }

    LeftToolButtonsPanel::~LeftToolButtonsPanel() {
    }
    
    void LeftToolButtonsPanel::onUpdateUI() {
        const auto isRunning = d->control.IsRunningExperiment();
        d->ui.goToHome->setEnabled(!isRunning);
    }
}
