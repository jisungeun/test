#include "ProjectPanelControl.h"

#include <AppData.h>
#include <ExperimentPresenter.h>
#include <ExperimentReaderPort.h>
#include <ProjectController.h>
#include <ProjectPresenter.h>
#include <ProjectWriterPlugin.h>
#include <ProjectRepo.h>
#include <FileNameValidator.h>
#include <SystemStatus.h>
#include <DirectoryDeleterPlugin.h>
#include <ExperimentController.h>
#include <UserController.h>

#include "ExperimentUpdater.h"
#include "ProjectUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct ProjectPanelControl::Impl {
    };

    ProjectPanelControl::ProjectPanelControl() : d{ new Impl } {
        
    }

    ProjectPanelControl::~ProjectPanelControl() {
        
    }

    auto ProjectPanelControl::CreateProject(const QString& title) -> bool {
        auto updater = ProjectUpdater::GetInstance();
        auto presenter = Interactor::ProjectPresenter(updater.get());

        Plugins::ProjectIO::ProjectWriter writer;

        auto controller = Interactor::ProjectController(&presenter, nullptr, nullptr, &writer);
        return controller.CreateProject(title);
    }

    auto ProjectPanelControl::DeleteProject(const QString& title) -> bool {
        auto projectUpdater = ProjectUpdater::GetInstance();
        auto projectPresenter = Interactor::ProjectPresenter(projectUpdater.get());

        auto experimentUpdater = ExperimentUpdater::GetInstance();
        auto experimentPresenter = Interactor::ExperimentPresenter(experimentUpdater.get());

        auto directoryDeleter = Plugins::DirectoryDeleter::Deleter();
        auto experimentReader = Plugins::ExperimentIO::ExperimentReaderPort();

        auto controller = Interactor::ProjectController(&projectPresenter, &experimentPresenter, nullptr, nullptr, &experimentReader, &directoryDeleter);

        return controller.DeleteProject(title);
    }

    auto ProjectPanelControl::SetCurrentProject(const QString& title) -> bool {
        auto projectUpdater = ProjectUpdater::GetInstance();
        auto projectPresenter = Interactor::ProjectPresenter(projectUpdater.get());

        auto experimentUpdater = ExperimentUpdater::GetInstance();
        auto experimentPresenter = Interactor::ExperimentPresenter(experimentUpdater.get());

        auto experimentReader = Plugins::ExperimentIO::ExperimentReaderPort();
        auto controller = Interactor::ProjectController(&projectPresenter, &experimentPresenter, nullptr, nullptr, &experimentReader);

        return controller.SetProject(title);
    }

    auto ProjectPanelControl::SetDescription(const QString& title, const QString& description) -> bool {
        // Todo: description view와 edit이 동일한 ui에서 이루어지는 경우 Updater를 통한 UI 갱신이 필요 없을 수 있음
        auto updater = ProjectUpdater::GetInstance();
        auto presenter = Interactor::ProjectPresenter(updater.get());

        auto projectWriter = Plugins::ProjectIO::ProjectWriter();
        auto controller = Interactor::ProjectController(&presenter, nullptr, nullptr, &projectWriter);

        return controller.SetDescription(title, description);
    }

    auto ProjectPanelControl::GetCurrentProject() const -> QString {
        return Entity::AppData::GetInstance()->GetProjectTitle();
    }

    auto ProjectPanelControl::GetCurrentProjectDescription() const -> QString {
        auto title = Entity::AppData::GetInstance()->GetProjectTitle();
        auto project = AppEntity::ProjectRepo::GetInstance()->GetProject(title);
        if (project == nullptr) return QString();

        return project->GetDescription();
    }

    auto ProjectPanelControl::IsExistProject(const QString& name) const -> bool {
        auto repo = AppEntity::ProjectRepo::GetInstance();
        QStringList projects = repo->GetProjectTitles();
        return projects.contains(name, Qt::CaseInsensitive);
    }

    auto ProjectPanelControl::IsValidName(const QString& name, QString& errorString) const -> TC::FileNameValid {
        TC::FileNameValidator validator;
        validator.AddInavlidCharacter('.');
        if (!validator.IsValid(name)) {
            errorString = validator.GetErrorString();
            return validator.GetError();
        }

        return TC::FileNameValid::NoError;
    }

    auto ProjectPanelControl::HasExperimentChanges() const -> bool {
        const auto appData = Entity::AppData::GetInstance();

        const auto experiment = appData->GetExperiment();
        const auto originExpeirment = appData->GetOriginExperiment();
        if (experiment == nullptr || originExpeirment == nullptr) return false;

        return !(*experiment == *originExpeirment);
    }

    auto ProjectPanelControl::IsRunningExperiment() const -> bool {
        return (AppEntity::SystemStatus::GetInstance()->GetExperiment() != nullptr);
    }

    auto ProjectPanelControl::GetBusyExperiments() const -> QStringList {
        QStringList busyExperiments;
        const auto appData = Entity::AppData::GetInstance();
        const auto experimentShortInfos = appData->GetExperimentListInfos();
        auto controller = Interactor::ExperimentController();
        for (const auto& shortInfo: experimentShortInfos) {
            const auto experimentTitle = shortInfo.GetTitle();
            if(!controller.CheckDeletable(experimentTitle)) {
                busyExperiments.push_back(experimentTitle);
            }
        }
        return busyExperiments;
    }

    auto ProjectPanelControl::CheckPassword(const QString& password) const -> bool {
        auto controller = Interactor::UserController();
        return controller.CheckPassword(password);
    }
}
