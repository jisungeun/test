#pragma once

#include <memory>

#include <QString>

#include <FileNameValidator.h>
#include <ExperimentShortInfo.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class ExperimentExplorerPanelControl {
        Q_DECLARE_TR_FUNCTIONS(ExperimentExplorerPanelControl)

    public:
        ExperimentExplorerPanelControl();
        ~ExperimentExplorerPanelControl();

        auto AddExperiment(const QString& title)->bool;
        auto AddExperimentFromTemplate(const QString& title, const QString& templateTitle)->bool;
        auto Copy(const QString& title, const QString& copyTitle)->bool;
        auto Delete(const QString& title)->bool;
        auto Load(const QString& title)->bool;
        auto ChangeExperimentSelection(const QString& title)->bool;

        auto GetProjectTitle() const -> QString;
        auto GetLoadedExperimentTitle() const -> QString;
        auto GetExperimentInfos() const -> QList<Entity::ExperimentShortInfo>;
        auto GetTemplateInfos() const -> QMap<QString, QDateTime>;
        auto IsRunningExperiment() const -> bool;
        auto IsDeletable(const QString& title) const -> bool;
        auto CheckExperimentTitleValidation(const QString& title, QString& errorMessage) const -> bool;
        auto CheckPassword(const QString& password) const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
