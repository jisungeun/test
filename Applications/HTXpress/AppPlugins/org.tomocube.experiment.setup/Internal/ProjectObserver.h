#pragma once

#include <memory>

#include <QObject>
#include <IProjectView.h>

namespace HTXpress::AppPlugins::Experiment::Setup{
    class ProjectObserver : public QObject {
        Q_OBJECT

    public:
        using Pointer = std::shared_ptr<ProjectObserver>;

        explicit ProjectObserver(QObject* parent = nullptr);
        ~ProjectObserver() override;

        auto UpdateList(const QList<QString>& projects)->void;
        auto ChangeProjectSelection(const QString &title)->void;
        auto UpdateDescription(const QString& description)->void;
        auto ProjectDeleted(bool isProjectEmpty)->void;

    signals:
        void sigProjectListChanged(const QList<QString>& projects);
        void sigProjectSelectionChanged(const QString& title);
        void sigDescriptionChanged(const QString& description);
        void sigProjectDeleted(bool isProjectEmpty);
    };
}