#include "VesselMapUpdater.h"

#include "VesselMapObserver.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct VesselMapUpdater::Impl {
        QList<VesselMapObserver*> observers;
    };

    VesselMapUpdater::VesselMapUpdater() : d{ new Impl } {
        
    }

    VesselMapUpdater::~VesselMapUpdater() {
        
    }

    auto VesselMapUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new VesselMapUpdater() };
        return theInstance;
    }

    auto VesselMapUpdater::UpdateVesselMap(const TC::VesselMap::Pointer vesselMap) -> void {
        for (auto observer : d->observers) {
            observer->UpdateVesselModel(vesselMap);
        }
    }

    auto VesselMapUpdater::Register(VesselMapObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto VesselMapUpdater::Deregister(VesselMapObserver* observer) -> void {
        auto index = d->observers.indexOf(observer);
        if (index < 0) {
            return;
        }

        d->observers.removeAt(index);
    }
}