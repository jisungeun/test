#include "ExperimentListItemDelegate.h"

#include <QDebug>
#include <QPainter>

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct ExperimentListItemDelegate::Impl {
        QColor activeColor{Qt::transparent};
    };

    ExperimentListItemDelegate::ExperimentListItemDelegate(QObject *parent) : QStyledItemDelegate(parent), d{ new Impl } {
        
    }

    ExperimentListItemDelegate::~ExperimentListItemDelegate() {
        
    }

    auto ExperimentListItemDelegate::SetActiveColor(const QColor& color) -> void {
        d->activeColor = color;
    }

    void ExperimentListItemDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const {
        if (option.state & QStyle::State_HasFocus ||
            option.state & QStyle::State_KeyboardFocusChange ||
            option.state & QStyle::State_MouseOver ||
            option.state & QStyle::State_Selected) {
            if (index.data(Qt::UserRole).toBool()) {
                painter->fillRect(option.rect, d->activeColor);
                return;
            }
        }

        QStyledItemDelegate::paint(painter, option, index);
    }

}
