#pragma once

#include <memory>

#include <QComboBox>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class CenterAlignedComboBox : public QComboBox {
        Q_OBJECT

    public:
        CenterAlignedComboBox(QWidget *parent = nullptr);
        ~CenterAlignedComboBox() override;

    protected:
        bool eventFilter(QObject* watched, QEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}