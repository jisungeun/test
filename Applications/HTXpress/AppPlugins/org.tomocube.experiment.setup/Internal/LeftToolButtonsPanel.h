#pragma once
#include <memory>
#include <QWidget>

#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class LeftToolButtonsPanel final : public QWidget {
        Q_OBJECT
    public:
        explicit LeftToolButtonsPanel(QWidget* parent = nullptr);
        ~LeftToolButtonsPanel() override;

    signals:
        void sigOpenDataNavigation();
        void sigGoToHome();

    private slots:
        void onUpdateUI();

    private:
        struct Impl;
        std::shared_ptr<Impl> d;
    };
}
