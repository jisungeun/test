#pragma once

#include <memory>

#include <QObject>
#include <IAppUIView.h>

namespace HTXpress::AppPlugins::Experiment::Setup{
    class AppUIObserver : public QObject {
        Q_OBJECT

    public:
        using Pointer = std::shared_ptr<AppUIObserver>;

        AppUIObserver(QObject* parent = nullptr);
        ~AppUIObserver();

        auto UpdateUI()->void;

    signals:
        void sigUpdatingUIRequested();
    };
}