#define LOGGER_TAG "[VesselMapPanel]"

#include "VesselMapPanel.h"

#include <QList>

#include <MessageDialog.h>
#include <TCLogger.h>
#include <UIUtility.h>
#include <VesselMapUtility.h>
#include <RoiSetupWindow.h>
#include <RegionOfInterest.h>
#include <RoiSetupDefines.h>

#include "AppUIObserver.h"
#include "ProjectObserver.h"
#include "ExperimentObserver.h"
#include "VesselMapObserver.h"
#include "VesselMapPanelControl.h"
#include "ui_VesselMapPanel.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    struct VesselMapPanel::Impl {
        Ui::VesselMapPanel ui;
        VesselMapPanelControl control;

        QString currentVesselName;

        std::shared_ptr<AppUIObserver> appUIObserver{ nullptr };
        std::shared_ptr<ProjectObserver> projectObserver{ nullptr };
        std::shared_ptr<ExperimentObserver> experimentObserver{ nullptr };
        std::shared_ptr<VesselMapObserver> vesselMapObserver{ nullptr };

        auto UpdateVesselIndexComboBox()->void;
        auto UpdateWellGroupComboBox()->void;
        auto SetExperiment(const AppEntity::Experiment::Pointer experiment)->void;
        auto SetUIEnabled(bool enabled)->void;
        auto ClearUI()->void;

        auto ConvertVesselMapWellIndexToRowColumns(const QList<TC::WellIndex>& indices) const -> QList<AppEntity::RowColumn>;
        auto ConvertRowColumnsToVesselMapWellIndex(const QList<AppEntity::RowColumn>& rowColumns) const -> QList<TC::WellIndex>;

        auto GetAcquisitionType(bool isTile) -> TC::AcquisitionType;
    };

    auto VesselMapPanel::Impl::UpdateVesselIndexComboBox()->void {
        ui.vesselComboBox->clear();

        auto vesselIndices = ui.vesselMapWidget->GetAllVesselIndices();

        ui.vesselComboBox->blockSignals(true);
        for (auto i = 0; i < vesselIndices.size() ; ++i) {
            ui.vesselComboBox->addItem(QString("Vessel %1").arg(i + 1), vesselIndices[i]);
        }

        ui.vesselComboBox->setCurrentIndex(0);

        ui.vesselComboBox->blockSignals(false);
    }

    auto VesselMapPanel::Impl::UpdateWellGroupComboBox() -> void {
        auto selectedIndex = ui.wellGroupComboBox->currentIndex();

        ui.wellGroupComboBox->clear();

        auto experiment = control.GetLoadedExperiment();
        if (experiment == nullptr) return;

        auto vesselIndex = ui.vesselComboBox->currentData().toInt();

        const auto wellGroupIndices = experiment->GetWellGroupIndices(vesselIndex);
        for (auto& wellGroupIndex : wellGroupIndices) {
            const auto& wellGroup = experiment->GetWellGroup(vesselIndex, wellGroupIndex);
            TC::SilentCall(ui.wellGroupComboBox)->addItem(wellGroup.GetTitle(), wellGroupIndex);
        }

        ui.wellGroupComboBox->setCurrentIndex(selectedIndex);
    }

    auto VesselMapPanel::Impl::SetExperiment(const AppEntity::Experiment::Pointer experiment) -> void {
        using Converter = AppComponents::VesselMapUtility::Converter;

        ClearUI();

        if (experiment == nullptr) return;

        const auto vesselIndex = ui.vesselComboBox->currentData().toInt();
        const auto vessel = experiment->GetVessel();
        const auto wellNames = experiment->GetWellNames(vesselIndex);

        auto newVesselMap = Converter::ToTCVesselMap(vesselIndex, experiment, vessel, wellNames);
        if (newVesselMap == nullptr) {
            QString log = tr("Failed to generate vessel map.");
            QLOG_ERROR() << log;
            return;
        }

        ui.vesselMapWidget->SetVesselMap(newVesselMap);
        UpdateVesselIndexComboBox();

        auto locations = experiment->GetAllLocations(vesselIndex);
        const auto wellList = locations.keys();
        for(auto wellIdx : wellList) {
            auto& locs = locations[wellIdx];
            for(auto iter = locs.begin(); iter!=locs.end(); iter++) {
                auto cetner = iter.value().GetCenter();
                auto area = iter.value().GetArea();
                auto type = GetAcquisitionType(iter.value().IsTile());
                ui.vesselMapWidget->AddAcquisitionLocationByWellPos(wellIdx, iter.key(), type, cetner.toMM().x, cetner.toMM().y,cetner.toMM().z, area.toMM().width, area.toMM().height);
            }
        }

        const auto wellGroupIndices = experiment->GetWellGroupIndices(vesselIndex);
        for (auto& wellGroupIndex : wellGroupIndices) {
            const auto& wellGroup = experiment->GetWellGroup(vesselIndex, wellGroupIndex);
            TC::SilentCall(ui.wellGroupComboBox)->addItem(wellGroup.GetTitle(), wellGroupIndex);

            QList<AppEntity::RowColumn> wellRowColumns;
            auto wells = wellGroup.GetWells();
            for (auto& well : wells) {
                wellRowColumns << AppEntity::RowColumn(well.rowIdx, well.colIdx);
            }

            auto groupName = wellGroup.GetTitle();
            auto groupColor = QColor(
                wellGroup.GetColor().r,
                wellGroup.GetColor().g,
                wellGroup.GetColor().b,
                wellGroup.GetColor().a
            );

            ui.vesselMapWidget->CreateNewGroup(
                wellGroupIndex,
                groupName,
                groupColor,
                ConvertRowColumnsToVesselMapWellIndex(wellRowColumns)
            );
        }

        if (ui.wellGroupComboBox->count() > 0) TC::SilentCall(ui.wellGroupComboBox)->setCurrentIndex(0);
    }

    auto VesselMapPanel::Impl::SetUIEnabled(bool enabled) -> void {
        ui.specimenButton->setEnabled(enabled);
        ui.addWell->setEnabled(enabled);
        ui.removeWell->setEnabled(enabled);
        ui.addToWellGroupButton->setEnabled(enabled);
        ui.wellGroupComboBox->setEnabled(enabled);
        ui.vesselComboBox->setEnabled(enabled);
        ui.vesselMapWidget->setEnabled(enabled);
        ui.RoiSetupButton->setEnabled(enabled);
    }

    auto VesselMapPanel::Impl::ClearUI() -> void {
        ui.wellGroupComboBox->clear();
        ui.vesselComboBox->clear();
        ui.vesselMapWidget->ClearAll();
    }

    auto VesselMapPanel::Impl::ConvertVesselMapWellIndexToRowColumns(const QList<TC::WellIndex>& indices) const -> QList<AppEntity::RowColumn> {
        if (ui.vesselMapWidget == nullptr) {
            return QList<AppEntity::RowColumn>();
        }

        QList<AppEntity::RowColumn> rowColumns;

        std::for_each(indices.begin(), indices.end(), [&rowColumns, widget = ui.vesselMapWidget](const TC::WellIndex index) {
            rowColumns << AppEntity::RowColumn(widget->GetWellRow(index), widget->GetWellColumn(index));
        });

        return rowColumns;
    }

    auto VesselMapPanel::Impl::ConvertRowColumnsToVesselMapWellIndex(const QList<AppEntity::RowColumn>& rowColumns) const -> QList<TC::WellIndex> {
        if (ui.vesselMapWidget == nullptr) {
            return QList<TC::WellIndex>();
        }

        QList<TC::WellIndex> wells;
        for (auto& rc : rowColumns) {
            const auto index = ui.vesselMapWidget->GetWellIndex(rc.first, rc.second);
            if(index != -1) wells << index;        }

        return wells;
    }

    auto VesselMapPanel::Impl::GetAcquisitionType(bool isTile) -> TC::AcquisitionType {
        if(isTile) {
            return TC::AcquisitionType::Tile;
        }
        return TC::AcquisitionType::Point;
    }

    VesselMapPanel::VesselMapPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.baseWidget->setObjectName("panel");
        d->ui.specimenButton->setObjectName("bt-toggle");
        d->ui.addWell->setObjectName("bt-light");
        d->ui.removeWell->setObjectName("bt-light");
        d->ui.addToWellGroupButton->setObjectName("bt-light");

        d->ui.vesselMapWidget->SetViewMode(TC::ViewMode::SetupMode);

        d->appUIObserver = std::make_shared<AppUIObserver>(this);
        connect(d->appUIObserver.get(), &AppUIObserver::sigUpdatingUIRequested, this, &VesselMapPanel::onUpdateUI);

        d->projectObserver = std::make_shared<ProjectObserver>(this);
        connect(d->projectObserver.get(), &ProjectObserver::sigProjectSelectionChanged, this, &VesselMapPanel::onChangeProject);

        d->experimentObserver = std::make_shared<ExperimentObserver>(this);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigExperimentChanged, this, &VesselMapPanel::onChangeExperiment);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigExperimentVesselTypeChanged, this, &VesselMapPanel::onChangeVesselType);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigExperimentVesselCountChanged, this, &VesselMapPanel::onChangeVesselCount);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigWellGroupAdded, this, &VesselMapPanel::onAddWellGroupToVesselMap);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigWellsAdded, this, &VesselMapPanel::onAddWellsToVesselMap);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigWellsMoved, this, &VesselMapPanel::onMoveWells);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigWellsRemoved, this, &VesselMapPanel::onRemoveWellsToVesselMap);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigWellGroupsDeleted, this, &VesselMapPanel::onDeleteWellGroupsToVesselMap);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigWellGroupNameChanged, this, &VesselMapPanel::onUpdateGroupNameList);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigClear, this, &VesselMapPanel::onClear);
        connect(d->experimentObserver.get(), &ExperimentObserver::sigRoiListUpdated, this, &VesselMapPanel::onRoiListUpdated);
        d->vesselMapObserver = std::make_shared<VesselMapObserver>(this);
        connect(d->vesselMapObserver.get(), &VesselMapObserver::sigVesselIndicesChanged, this, &VesselMapPanel::onUpdateVesselIndices);

        connect(d->ui.specimenButton, &QPushButton::clicked, this, &VesselMapPanel::onChangeSpecimenChecked);
        connect(d->ui.vesselComboBox, &QComboBox::currentTextChanged, this, &VesselMapPanel::onChangeCurrentVesselIndex);
        connect(d->ui.addWell, &QPushButton::clicked, this, &VesselMapPanel::onAddNewWellGroup);
        connect(d->ui.addToWellGroupButton, &QPushButton::clicked, this, &VesselMapPanel::onAddWellsToWellGroup);
        connect(d->ui.removeWell, &QPushButton::clicked, this, &VesselMapPanel::onRemoveWell);

        // vessel map widget에서 발생하는 signal(그룹 이름 변경, 그룹 색상 변경 등) 처리
        connect(d->ui.vesselMapWidget, &TC::VesselMapWidget::sigChangedGroupName, this, &VesselMapPanel::onChangeGroupName);
        connect(d->ui.vesselMapWidget, &TC::VesselMapWidget::sigChangedGroupColor, this, &VesselMapPanel::onChangeGroupColor);
        connect(d->ui.vesselMapWidget, &TC::VesselMapWidget::sigMovedGroup, this, &VesselMapPanel::onMoveGroup);
        connect(d->ui.vesselMapWidget, &TC::VesselMapWidget::sigChangedWellName, this, &VesselMapPanel::onChangeWellName);

        connect(d->ui.RoiSetupButton, &QPushButton::clicked, this, &VesselMapPanel::onShowROISetup);

        d->ui.specimenButton->setChecked(true);
    }

    VesselMapPanel::~VesselMapPanel() {
    }

    void VesselMapPanel::onUpdateVesselIndices(const QList<int>& indices) {
        for (auto index : indices) {
            TC::SilentCall(d->ui.vesselComboBox)->addItem(QString("Vessel %1").arg(index), index);
        }
    }

    void VesselMapPanel::onUpdateUI() {
        bool enabled = true;

        const auto experiment = d->control.GetLoadedExperiment();
        if (experiment == nullptr) {
            d->ClearUI();
            enabled = false;
        } else {
            d->SetExperiment(experiment);
            if (d->control.IsRunningExperiment()) {
                enabled = false;
            } 
        }

        d->SetUIEnabled(enabled);
    }

    void VesselMapPanel::onChangeProject(const QString& title) {
        Q_UNUSED(title)

        d->ClearUI();
    }

    void VesselMapPanel::onChangeExperiment(const AppEntity::Experiment::Pointer& experiment) {
        d->SetExperiment(experiment);
        d->SetUIEnabled(!d->control.IsRunningExperiment());
    }

    void VesselMapPanel::onChangeVesselType(const QString& type) {
        d->ui.vesselMapWidget->ClearAll();
        d->ui.wellGroupComboBox->clear();

        d->currentVesselName = type;

        const auto vesselIndex = d->ui.vesselComboBox->currentData().toInt();
        const auto experiment = d->control.GetLoadedExperiment();
        const auto vessel = experiment->GetVessel();
        const auto wellNames = experiment->GetWellNames(vesselIndex);

        auto newVesselMap = AppComponents::VesselMapUtility::Converter::ToTCVesselMap(vesselIndex, experiment, vessel, wellNames);
        if (newVesselMap == nullptr) {
            QString log = tr("Failed to generate vessel map.");
            QLOG_ERROR() << log;
            return;
        }

        if (newVesselMap) {
            d->ui.vesselMapWidget->SetVesselMap(newVesselMap);
        } else {
            QString log = tr("Failed to generate vessel map.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Generate Vessel Map"), log);
        }
    }

    void VesselMapPanel::onChangeVesselCount(int count) {
        // TODO: multi vessel 처리
        Q_UNUSED(count)

        d->ui.vesselMapWidget->ClearAll();

        const auto vesselIndex = d->ui.vesselComboBox->currentData().toInt();
        const auto experiment = d->control.GetLoadedExperiment();
        const auto vessel = experiment->GetVessel();
        const auto wellNames = experiment->GetWellNames(vesselIndex);

        auto newVesselMap = AppComponents::VesselMapUtility::Converter::ToTCVesselMap(vesselIndex, experiment, vessel, wellNames);
        if (newVesselMap) {
            d->ui.vesselMapWidget->SetVesselMap(newVesselMap);
            d->UpdateVesselIndexComboBox();
        } else {
            QString log = tr("Failed to generate vessel map.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Generate Vessel Map"), log);
        }
    }

    void VesselMapPanel::onAddWellGroupToVesselMap(const QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>>& indices) {
        auto experiment = d->control.GetLoadedExperiment();
        auto vesselIndex = d->ui.vesselComboBox->currentData().toInt();

        // TC::VesselMapWidget에 생성된 well group 추가
        QMapIterator it(indices);
        while (it.hasNext()) {
            it.next();

            const auto& wellGroup = experiment->GetWellGroup(vesselIndex, it.key());

            auto groupName = wellGroup.GetTitle();
            auto groupColor = QColor(wellGroup.GetColor().r,
                                     wellGroup.GetColor().g,
                                     wellGroup.GetColor().b,
                                     wellGroup.GetColor().a);

            d->ui.vesselMapWidget->CreateNewGroup(
                it.key(),
                groupName,
                groupColor,
                d->ConvertRowColumnsToVesselMapWellIndex(it.value())
            );

            if (d->ui.wellGroupComboBox->findData(it.key()) < 0) {
                TC::SilentCall(d->ui.wellGroupComboBox)->addItem(wellGroup.GetTitle(), it.key());
            }
        }
    }

    void VesselMapPanel::onAddWellsToVesselMap(AppEntity::WellGroupIndex groupIndex, const QList<AppEntity::RowColumn>& indices) {
        d->ui.vesselMapWidget->AddWellsToGroup(
            groupIndex,
            d->ConvertRowColumnsToVesselMapWellIndex(indices)
        );
    }

    void VesselMapPanel::onMoveWells(AppEntity::WellGroupIndex movingGroupIndex, AppEntity::WellGroupIndex targetGroupIndex, const QList<AppEntity::RowColumn>& wells) {
        Q_UNUSED(movingGroupIndex)

        auto wellIndices = d->ConvertRowColumnsToVesselMapWellIndex(wells);
        d->ui.vesselMapWidget->RemoveWellsFromGroup(wellIndices);
        d->ui.vesselMapWidget->AddWellsToGroup(targetGroupIndex, wellIndices);

        d->UpdateWellGroupComboBox();
    }

    void VesselMapPanel::onRemoveWellsToVesselMap(const QList<AppEntity::RowColumn>& indices) {
        d->ui.vesselMapWidget->RemoveWellsFromGroup(
            d->ConvertRowColumnsToVesselMapWellIndex(indices)
        );
    }

    void VesselMapPanel::onDeleteWellGroupsToVesselMap(const QList<AppEntity::WellGroupIndex>& indices) {
        d->ui.vesselMapWidget->DeleteGroups(indices);

        for (auto groupIndex : indices) {
            auto index = d->ui.wellGroupComboBox->findData(groupIndex);
            if (index < 0) {
                continue;
            }

            TC::SilentCall(d->ui.wellGroupComboBox)->removeItem(index);
        }
    }

    void VesselMapPanel::onUpdateGroupNameList(AppEntity::WellGroupIndex groupIndex, const QString& groupName) {
        Q_UNUSED(groupIndex)
        Q_UNUSED(groupName)

        d->UpdateWellGroupComboBox();
    }

    void VesselMapPanel::onClear() {
        d->ClearUI();
        d->SetUIEnabled(false);
    }

    void VesselMapPanel::onChangeCurrentVesselIndex(const QString& text) {
        if (text.isEmpty()) {
            d->ui.wellGroupComboBox->clear();
            return;
        }

        const auto vesselIndex = d->ui.vesselComboBox->currentData().toInt();
        d->ui.vesselMapWidget->SetCurrentVessel(vesselIndex);

        // well group 목록 업데이트
        d->UpdateWellGroupComboBox();
    }

    void VesselMapPanel::onChangeSpecimenChecked(bool checked) {
        d->ui.specimenButton->setText(checked ? tr("Specimen") : tr("Individual"));
    }

    void VesselMapPanel::onAddNewWellGroup() {
        const auto selectedWells = d->ui.vesselMapWidget->GetSelectedWellIndicesHasNoGroup();
        auto rowColumns = d->ConvertVesselMapWellIndexToRowColumns(selectedWells);

        if (d->ui.specimenButton->isChecked()) {
            if (!d->control.AddWellsToNewGroup(d->ui.vesselMapWidget->GetCurrentVessel(), rowColumns)) {
                QString log = tr("Failed to add well group.");
                QLOG_ERROR() << log;
                TC::MessageDialog::warning(this, tr("Add Well Group"), log);
            }
        } else {
            if (!d->control.AddWellsToSeparateGroup(d->ui.vesselMapWidget->GetCurrentVessel(), rowColumns)) {
                QString log = tr("Failed to add wells.");
                QLOG_ERROR() << log;
                TC::MessageDialog::warning(this, tr("Add Wells"), log);
            }
        }
    }

    void VesselMapPanel::onAddWellsToWellGroup() {
        if (d->ui.wellGroupComboBox->currentIndex() < 0) {
            TC::MessageDialog::warning(this, tr("Move Wells"), tr("Select specimen."));
            return;
        }

        auto experiment = d->control.GetLoadedExperiment();
        if(experiment == nullptr) return;

        const auto vesselIndex = d->ui.vesselComboBox->currentData().toInt();
        const auto wellGroupIndices = experiment->GetWellGroupIndices(vesselIndex);
        const auto targetGroupIndex = d->ui.wellGroupComboBox->currentData().toInt();
        const auto selectedWells = d->ui.vesselMapWidget->GetCurrentSelectedWellIndices();
        
        QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>> moveWells;
        QList<AppEntity::RowColumn> newWells;


        // 선택된 well이 이미 specimen에 포함되어있는지 확인
        for (auto& well : d->ConvertVesselMapWellIndexToRowColumns(selectedWells)) {
            bool foundGroup = false;
            for (auto& groupIndex : wellGroupIndices) {
                if (experiment->GetWellGroup(vesselIndex, groupIndex).ContainsWell(well.first, well.second)) {
                    auto wells = moveWells[groupIndex];
                    wells << well;
                    moveWells[groupIndex] = wells;

                    foundGroup = true;
                    break;
                }
            }

            if (!foundGroup) newWells << well;
        }

        // 다른 specimen으로 이동
        auto moveWellsIt = QMapIterator(moveWells);
        while (moveWellsIt.hasNext()) {
            moveWellsIt.next();
            if(moveWellsIt.key() != targetGroupIndex) {
                if (!d->control.MoveWell(d->ui.vesselMapWidget->GetCurrentVessel(), moveWellsIt.key(), targetGroupIndex, moveWellsIt.value())) {
                    QString log = tr("Failed to move wells.");
                    QLOG_ERROR() << log;
                    TC::MessageDialog::warning(this, tr("Move Wells"), log);
                }
            }
        }

        // 새로운 well 추가
        if (!newWells.isEmpty()) {
            if (!d->control.AddWellsToWellGroup(d->ui.vesselMapWidget->GetCurrentVessel(), targetGroupIndex, newWells)) {
                QString log = tr("Failed to add wells.");
                QLOG_ERROR() << log;
                TC::MessageDialog::warning(this, tr("Add Wells"), log);
            }
        }

        d->UpdateWellGroupComboBox();
    }

    void VesselMapPanel::onRemoveWell() {
        const auto selectedWells = d->ui.vesselMapWidget->GetCurrentSelectedWellIndices();
        auto rowColumns = d->ConvertVesselMapWellIndexToRowColumns(selectedWells);

        if (!d->control.RemoveExperimentWells(d->ui.vesselMapWidget->GetCurrentVessel(), rowColumns)) {
            QString log = tr("Failed to remove wells.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Remove Wells"), log);
        }
    }

    void VesselMapPanel::onChangeGroupName(int groupIndex, const QString& name) {
        if (!d->control.ChangeWellGroupName(d->ui.vesselMapWidget->GetCurrentVessel(), groupIndex, name)) {
            QString log = tr("Failed to change well group name.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Change Well Group Name"), log);
        }
    }

    void VesselMapPanel::onChangeGroupColor(int groupIndex, const QColor& color) {
        if (!d->control.ChangeWellGroupColor(d->ui.vesselMapWidget->GetCurrentVessel(), groupIndex, color)) {
            QString log = tr("Failed to change well group color.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Change Well Group Color"), log);
        }
    }

    void VesselMapPanel::onMoveGroup(int movingGroupIndex, int targetGroupIndex) {
        if (!d->control.MoveWellGroup(d->ui.vesselMapWidget->GetCurrentVessel(), movingGroupIndex, targetGroupIndex)) {
            QString log = tr("Failed to move well group.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Move Well Group"), log);
        }
    }

    void VesselMapPanel::onChangeWellName(int wellIndex, const QString& name) {
        if(!d->control.ChangeWellName(d->ui.vesselMapWidget->GetCurrentVessel(), wellIndex, name)) {
            QString log = tr("Failed to change well name.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("Change Well Name"), log);
        }
    }

    void VesselMapPanel::onShowROISetup() {
        using namespace AppComponents;
        using Converter = VesselMapUtility::Converter;
        
        const auto experiment = d->control.GetLoadedExperiment();

        if(!experiment->GetVessel() || !experiment) {
            const auto log = tr("Failed to open ROI Setup.");
            QLOG_ERROR() << log;
            TC::MessageDialog::warning(this, tr("ROI Setup"), log);
            return;
        }

        const auto entityVessel = experiment->GetVessel();
        const auto vesselIndex = d->ui.vesselComboBox->currentData().toInt();
        const auto wellNames = experiment->GetWellNames(vesselIndex);

        const auto vessel = Converter::ToRoiSetupVesselMap(vesselIndex, experiment, entityVessel, wellNames);

        const auto setupDlg = new RoiSetupWindow::RoiSetupWindow{vessel};
        if (setupDlg->exec()) {
            const auto rois = setupDlg->GetROIs();
            QMap<AppEntity::WellIndex, QMap<AppEntity::ROIIndex, AppEntity::ROI::Pointer>> entityROIs;

            for (auto it = rois.cbegin(); it != rois.cend(); ++it) {
                auto wellIndex = it.key();
                auto componentROIs = it.value();
                for (const auto& componentROI : componentROIs) {
                    auto roiIndex = componentROI->GetIndex();
                    auto roiName = componentROI->GetName();
                    auto roiShape = [componentROI]() {
                        if (componentROI->GetShape() == +RoiSetupDefinitions::ItemShape::Rectangle) {
                            return AppEntity::ROIShape::Rectangle;
                        }
                        if (componentROI->GetShape() == +RoiSetupDefinitions::ItemShape::Ellipse) {
                            return AppEntity::ROIShape::Ellipse;
                        }
                        return AppEntity::ROIShape::Ellipse;
                    }();

                    AppEntity::Position roiPos{static_cast<int32_t>(componentROI->GetPosition().toNM().x), static_cast<int32_t>(componentROI->GetPosition().toNM().y), 0};
                    AppEntity::Area roiSize{static_cast<uint32_t>(componentROI->GetSize().toNM().w), static_cast<uint32_t>(componentROI->GetSize().toNM().h)};

                    auto roi = std::make_shared<AppEntity::ROI>(roiPos, roiSize, roiShape, roiName);
                    entityROIs[wellIndex][roiIndex] = roi;
                }
            }
            d->control.SetROIs(vesselIndex, entityROIs);
        }
        setupDlg->deleteLater();
    }

    void VesselMapPanel::onRoiListUpdated() {
        d->SetExperiment(d->control.GetLoadedExperiment());
    }
}
