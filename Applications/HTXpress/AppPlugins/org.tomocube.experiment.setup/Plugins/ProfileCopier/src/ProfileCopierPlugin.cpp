#include <QStringList>
#include <ProfileCopier.h>
#include "ProfileCopierPlugin.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ProfileCopier {
    struct ProfileCopierPlugin::Impl {
        QString profileName{};
        QString sourceFolder{};
        QStringList destinations{};
        QStringList filters{};
    };

    ProfileCopierPlugin::ProfileCopierPlugin() : IProfileCopier(), d{std::make_unique<Impl>()} {
    }

    ProfileCopierPlugin::~ProfileCopierPlugin() {
    }

    auto ProfileCopierPlugin::SetProfileName(const QString& profileName) -> void {
        d->profileName = profileName;
    }

    auto ProfileCopierPlugin::SetSource(const QString& sourceFolder) -> void {
        d->sourceFolder = sourceFolder;
    }

    auto ProfileCopierPlugin::AddDestination(const QString& destinationFolder) -> void {
        d->destinations.push_back(destinationFolder);
    }

    auto ProfileCopierPlugin::SetFileNameFilters(const QStringList& filters) -> void {
        d->filters = filters;
    }

    auto ProfileCopierPlugin::Copy() const -> bool {
        AppComponents::ProfileCopier::ProfileCopier copier(d->profileName, d->sourceFolder, d->destinations);
        if (d->destinations.isEmpty()) {
            return false;
        }

        copier.SetFileNameFilters(d->filters);

        if (d->destinations.size() == 1) {
            return copier.Copy(d->destinations.first());
        }

        const auto failedList = copier.Copy();
        if (failedList.isEmpty()) {
            return true;
        }
        return false;
    }
}
