#pragma once
#include <memory>
#include <IProfileCopier.h>
#include "HTX_Experiment_Setup_ProfileCopierExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ProfileCopier {
	class HTX_Experiment_Setup_ProfileCopier_API ProfileCopierPlugin final : public UseCase::IProfileCopier {
	public:
		ProfileCopierPlugin();
		~ProfileCopierPlugin() override;

		auto SetProfileName(const QString& profileName) -> void override;
		auto SetSource(const QString& sourceFolder) -> void override;
		auto AddDestination(const QString& destinationFolder) -> void override;
        auto SetFileNameFilters(const QStringList& filters) -> void override;

		auto Copy() const -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
