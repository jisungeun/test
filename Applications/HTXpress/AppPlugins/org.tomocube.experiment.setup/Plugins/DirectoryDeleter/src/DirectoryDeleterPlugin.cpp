#include <DirectoryDeleteDialog.h>
#include "DirectoryDeleterPlugin.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::DirectoryDeleter {
    struct Deleter::Impl {
        QString path{};
    };

    Deleter::Deleter() : d{ std::make_unique<Impl>() } {
    }

    Deleter::~Deleter() {
    }

    auto Deleter::SetRootPath(const QString& rootPath) -> void {
        d->path = rootPath;
    }

    auto Deleter::Delete() const -> bool {
        if (d->path.isEmpty()) return false;

        bool result { false };

        auto* dlg = new AppComponents::DirectoryDeleter::DirectoryDeleteDialog(d->path);
        dlg->Delete();

        if(dlg->exec()) {
            result = true;
        }
        else {
            result = false;
        }

        dlg->deleteLater();

        return result;
    }
}
