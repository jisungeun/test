#pragma once
#include <memory>
#include "HTX_Experiment_Setup_DirectoryDeleterExport.h"
#include <IDirectoryDeleter.h>

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::DirectoryDeleter {
    class HTX_Experiment_Setup_DirectoryDeleter_API Deleter final : public UseCase::IDirectoryDeleter {
    public:
        Deleter();
        ~Deleter() override;

        auto SetRootPath(const QString& rootPath) -> void override;
        auto Delete() const -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
