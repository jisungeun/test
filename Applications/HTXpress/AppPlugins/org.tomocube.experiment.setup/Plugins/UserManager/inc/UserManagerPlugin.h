#pragma once
#include <memory>

#include <IUserManager.h>
#include "HTX_Experiment_Setup_UserManagerExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::UserManager {
    class HTX_Experiment_Setup_UserManager_API Plugin final : public UseCase::IUserManager {
    public:
        Plugin();
        ~Plugin() override;

        auto IsValid(const AppEntity::UserID& id, const QString& password) const -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}