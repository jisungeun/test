#include "ExperimentReaderPort.h"

#include <ExperimentReader.h>

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ExperimentIO {

	ExperimentReaderPort::ExperimentReaderPort() {
	    
	}

	ExperimentReaderPort::~ExperimentReaderPort() {
	    
	}

	auto ExperimentReaderPort::Read(const QString& path, AppEntity::Experiment::Pointer& experiment) const -> bool {
        AppComponents::ExperimentIO::ExperimentReader reader;
		experiment = reader.Read(path);

		return (experiment != nullptr);
    }

	auto ExperimentReaderPort::ReadShortInfo(const QString& path, AppEntity::Experiment::Pointer& experiment) const -> bool {
        AppComponents::ExperimentIO::ExperimentReader reader;
		experiment = reader.ReadShortInfo(path);

		return (experiment != nullptr);
    }
}