#include "ExperimentWriterPort.h"

#include <ExperimentWriter.h>

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ExperimentIO {
	ExperimentWriterPort::ExperimentWriterPort() {
        
    }

    ExperimentWriterPort::~ExperimentWriterPort() {
        
    }

    auto ExperimentWriterPort::Write(const QString& path, const AppEntity::Experiment::Pointer& experiment) const -> bool {
        AppComponents::ExperimentIO::ExperimentWriter writer;
        return writer.Write(path, experiment);
    }
}