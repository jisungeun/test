#pragma once

#include <IExperimentWriterPort.h>

#include "HTX_Experiment_Setup_ExperimentIOExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ExperimentIO {
	class HTX_Experiment_Setup_ExperimentIO_API ExperimentWriterPort : public UseCase::IExperimentWriterPort {
	public:
		ExperimentWriterPort();
		~ExperimentWriterPort();

		auto Write(const QString& path, const AppEntity::Experiment::Pointer& experiment) const -> bool override;
	};
}
