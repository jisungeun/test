#pragma once
#include <memory>

#include <IExperimentReaderPort.h>

#include "HTX_Experiment_Setup_ExperimentIOExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ExperimentIO {
	class HTX_Experiment_Setup_ExperimentIO_API ExperimentReaderPort : public UseCase::IExperimentReaderPort{
	public:
		ExperimentReaderPort();
		~ExperimentReaderPort();

		auto Read(const QString& path, AppEntity::Experiment::Pointer& experiment) const -> bool override;
		auto ReadShortInfo(const QString& path, AppEntity::Experiment::Pointer& experiment) const -> bool override;
	};
}
