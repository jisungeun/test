#pragma once
#include <memory>

#include <IImagingProfileLoader.h>

#include "HTX_Experiment_Setup_ImagingProfileLoaderExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ImagingProfileLoader {
    class HTX_Experiment_Setup_ImagingProfileLoader_API Loader final : public UseCase::IImagingProfileLoader {
    public:
        Loader();
        ~Loader() override;

        auto GetHtScanParameter() const -> std::optional<std::tuple<int32_t, int32_t>> override;
    };
}
