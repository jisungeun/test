#pragma once
#include <memory>
#include <QString>

#include "HTX_Experiment_Setup_ChannelConfigIOExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ChannelConfigIO {
    class HTX_Experiment_Setup_ChannelConfigIO_API Plugin {
    public:
        Plugin();
        ~Plugin();

        auto SetTopPath(const QString& path)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}