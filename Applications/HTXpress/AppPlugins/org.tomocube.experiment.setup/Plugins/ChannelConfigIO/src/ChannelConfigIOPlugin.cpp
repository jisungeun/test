#include "Reader.h"
#include "ChannelConfigIOPlugin.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ChannelConfigIO {
    struct Plugin::Impl {
        Reader::Pointer reader{ new Reader() };
    };

    Plugin::Plugin() : d{new Impl} {
    }

    Plugin::~Plugin() {
    }

    auto Plugin::SetTopPath(const QString& path) -> void {
        d->reader->SetTopPath(path);
    }
}
