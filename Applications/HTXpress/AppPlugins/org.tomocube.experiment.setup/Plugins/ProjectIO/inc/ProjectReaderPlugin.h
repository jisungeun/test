#pragma once

#include <IProjectReaderPort.h>

#include "HTX_Experiment_Setup_ProjectIOExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ProjectIO {
	class HTX_Experiment_Setup_ProjectIO_API ProjectReader : public UseCase::IProjectReaderPort {
	public:
		ProjectReader();
		~ProjectReader();

		auto Read(const QString& path, const AppEntity::Project::Pointer& project) const -> bool override;
	};
}
