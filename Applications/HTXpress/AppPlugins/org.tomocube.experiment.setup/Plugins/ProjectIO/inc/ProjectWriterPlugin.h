#pragma once

#include <IProjectWriterPort.h>

#include "HTX_Experiment_Setup_ProjectIOExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ProjectIO {
	class HTX_Experiment_Setup_ProjectIO_API ProjectWriter : public UseCase::IProjectWriterPort {
	public:
		ProjectWriter();
		~ProjectWriter();

		auto Write(const QString& path, const AppEntity::Project::Pointer& project) const -> bool override;
	};
}
