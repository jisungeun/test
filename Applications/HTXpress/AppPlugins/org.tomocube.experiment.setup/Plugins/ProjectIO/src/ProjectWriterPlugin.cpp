#include "ProjectWriterPlugin.h"

#include <ProjectWriter.h>

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ProjectIO {
    ProjectWriter::ProjectWriter() : UseCase::IProjectWriterPort() {
    }

    ProjectWriter::~ProjectWriter() {
    }

    auto ProjectWriter::Write(const QString& path, const AppEntity::Project::Pointer& project) const -> bool {
        auto writer = AppComponents::ProjectIO::ProjectWriter();
        return writer.Write(path, project);
    }
}