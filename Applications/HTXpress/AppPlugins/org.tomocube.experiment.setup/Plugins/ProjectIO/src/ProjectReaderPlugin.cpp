#include "ProjectReaderPlugin.h"

#include <ProjectReader.h>

namespace HTXpress::AppPlugins::Experiment::Setup::Plugins::ProjectIO {
    ProjectReader::ProjectReader() : UseCase::IProjectReaderPort() {
    }

    ProjectReader::~ProjectReader() {
    }

	auto ProjectReader::Read(const QString& path, const AppEntity::Project::Pointer& project) const -> bool {
        auto reader = AppComponents::ProjectIO::ProjectReader();
        return reader.Read(path, project);
    }
}