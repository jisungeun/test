project(HTX_Experiment_Setup_ProjectIO_Test)

ADD_DEFINITIONS(-D_TEST_DATA=\"${CMAKE_CURRENT_SOURCE_DIR}/data\" )
ADD_DEFINITIONS(-D_TEST_OUTPUT=\"${CMAKE_BINARY_DIR}/test_output\")

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
)

#Sources
set(SOURCES
	src/TestMain.cpp
    src/ProjectIOTest.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

target_include_directories(${PROJECT_NAME}        
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_compile_features(${PROJECT_NAME} 
	PRIVATE 
		cxx_std_17
)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		Catch2::Catch2
        Qt5::Core
		HTXpress::AppEntity
		HTXpress::AppPlugins::experiment.setup::Plugins::ProjectIO
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/HTXpress/AppPlugins/experiment.setup/Tests")