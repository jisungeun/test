#define LOGGER_TAG "[MainWindowControl]"

#include "MainWindowControl.h"

#include <QStandardPaths>

#include <TCLogger.h>
#include <SessionManager.h>
#include <SystemStatus.h>
#include <AppData.h>
#include <AppUIController.h>
#include <AppUIPresenter.h>
#include <ExperimentController.h>
#include <ExperimentPresenter.h>
#include <ExperimentTemplateController.h>
#include <ProjectController.h>
#include <ProjectPresenter.h>
#include <ProjectReaderPlugin.h>
#include <ProjectRepo.h>
#include <ExperimentReaderPort.h>
#include <ChannelConfigIOPlugin.h>
#include <UseCaseLogger.h>
#include <ImagingProfileLoaderPlugin.h>

#include "Internal/AppUIUpdater.h"
#include "Internal/ProjectUpdater.h"
#include "Internal/ExperimentUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Setup {
    using ChannelConfigIOPlugin = Plugins::ChannelConfigIO::Plugin;
    using UseCaseLoggerPlugin = Plugins::UseCaseLogger::Logger;
    using ImagingProfileLoaderPlugin = Plugins::ImagingProfileLoader::Loader;

    struct MainWindowControl::Impl {
        std::shared_ptr<ChannelConfigIOPlugin> channelConfigIO{ new ChannelConfigIOPlugin() };
        std::shared_ptr<UseCaseLoggerPlugin> userCaseLogger{std::make_shared<UseCaseLoggerPlugin>()};
        std::shared_ptr<ImagingProfileLoaderPlugin> imagingProfileLoader{ std::make_shared<ImagingProfileLoaderPlugin>() };
    };

    MainWindowControl::MainWindowControl() : d{new Impl} {
        d->channelConfigIO->SetTopPath(QString("%1/config")
            .arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation)));
    }

    MainWindowControl::~MainWindowControl() {

    }

    auto MainWindowControl::GetCurrentUserId() const -> AppEntity::UserID {
        return AppEntity::SessionManager::GetInstance()->GetID();
    }

    auto MainWindowControl::Initialize() -> bool {
        Entity::AppData::GetInstance()->Clear();

        if (!ScanProjects()) return false;
        if (!SetProject()) return false;
        if (!LoadTemplates()) return false;

        return true;
    }

    auto MainWindowControl::ResetExperiment() -> void {
        AppEntity::SystemStatus::GetInstance()->ResetExperiment();
    }

    auto MainWindowControl::ScanProjects() -> bool {
        auto updater = ProjectUpdater::GetInstance();
        auto presenter = Interactor::ProjectPresenter(updater.get());

        Plugins::ProjectIO::ProjectReader reader;   // TODO: plguin 사용방식 확인

        auto controller = Interactor::ProjectController(&presenter, nullptr, &reader);
        return controller.ScanProjects();
    }

    auto MainWindowControl::SetProject(const QString& project) -> bool {
        auto projectRepo = AppEntity::ProjectRepo::GetInstance();
        if (projectRepo->GetProjectTitles().isEmpty()) {
            auto presenter = Interactor::AppUIPresenter(AppUIUpdater::GetInstance().get());
            auto controller = Interactor::AppUIController(&presenter);
            controller.UpdateUI();
        } else {
            auto changeProject = project;
            if (changeProject.isEmpty()) {
                // if project parameter is empty, it will set a first value in project repository.
                changeProject = projectRepo->GetProjectTitles().first();
            }

            auto projectUpdater = ProjectUpdater::GetInstance();
            auto projectPresenter = Interactor::ProjectPresenter(projectUpdater.get());

            auto experimentUpdater = ExperimentUpdater::GetInstance();
            auto experimentPresenter = Interactor::ExperimentPresenter(experimentUpdater.get());

            Plugins::ExperimentIO::ExperimentReaderPort reader;
            auto controller = Interactor::ProjectController(&projectPresenter, &experimentPresenter, nullptr, nullptr, &reader);
            controller.SetProject(changeProject);
        }

        return true;
    }

    auto MainWindowControl::ReloadExperiment() const -> bool {
        const auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        if (experiment == nullptr) return false;

        Plugins::ExperimentIO::ExperimentReaderPort reader;
        auto presenter = Interactor::ExperimentPresenter(ExperimentUpdater::GetInstance().get());
        auto controller = Interactor::ExperimentController(&presenter, &reader);
        return controller.LoadExperiment(experiment->GetTitle());
    }

    auto MainWindowControl::LoadTemplates() -> bool {
        Plugins::ExperimentIO::ExperimentReaderPort reader;
        auto controller = Interactor::ExperimentTemplateController(&reader);
        return controller.UpdateTemplateRepo();
    }

    auto MainWindowControl::UpdateUI() const -> bool {
        auto presenter = Interactor::AppUIPresenter(AppUIUpdater::GetInstance().get());
        auto controller = Interactor::AppUIController(&presenter);
        return controller.UpdateUI();
    }

    auto MainWindowControl::GetCurrentProject() const -> QString {
        auto appData = Entity::AppData::GetInstance();
        return appData->GetProjectTitle();
    }

    auto MainWindowControl::GetCurrentExperiment() const -> QString {
        auto appData = Entity::AppData::GetInstance();
        auto project = appData->GetExperiment();
        if(!project) return "";
        return project->GetTitle();
    }

    auto MainWindowControl::GetRunningProject() const -> QString {
        return AppEntity::SystemStatus::GetInstance()->GetProjectTitle();
    }

    auto MainWindowControl::GetRunningExperiment() const -> QString {
        QString experiment;
        if (AppEntity::SystemStatus::GetInstance()->GetExperiment()) {
            experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment()->GetTitle();
        }

        return experiment;
    }

    auto MainWindowControl::IsRunningExperiment() const -> bool {
        return (AppEntity::SystemStatus::GetInstance()->GetExperiment() != nullptr);
    }

    auto MainWindowControl::IsRunningTimelapse() const -> bool {
        return AppEntity::SystemStatus::GetInstance()->GetTimelapseRunning();
    }

}
