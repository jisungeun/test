﻿#include <CheckPassword.h>

#include "UserController.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    auto UserController::CheckPassword(const QString& password) const -> bool {
        auto usecase = UseCase::CheckPassword();
        usecase.SetPassword(password);
        if (!usecase.Request()) return false;
        return usecase.Check();
    }
}