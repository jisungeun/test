#include "ExperimentTemplateController.h"

#include <LoadTemplates.h>

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    struct ExperimentTemplateController::Impl {
        const UseCase::IExperimentReaderPort* reader{ nullptr };
    };

    ExperimentTemplateController::ExperimentTemplateController(UseCase::IExperimentReaderPort* reader)
    : d{ new Impl } {
        d->reader = reader;
    }

    ExperimentTemplateController::~ExperimentTemplateController() {
        
    }

    auto ExperimentTemplateController::UpdateTemplateRepo() const -> bool {
        auto usecase = UseCase::LoadTemplates(d->reader);
        return usecase.Request();
    }

}
