#include "AppUIController.h"

#include <UpdateUI.h>

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    struct AppUIController::Impl {
        UseCase::IAppUIOutputPort* presenter{ nullptr };
    };

    AppUIController::AppUIController(UseCase::IAppUIOutputPort* presenter) : d{ new Impl } {
        d->presenter = presenter;
    }

    AppUIController::~AppUIController() {
        
    }

    auto AppUIController::UpdateUI() const -> bool {
        auto usecase = UseCase::UpdateUI(d->presenter);
        return usecase.Request();
    }

}
