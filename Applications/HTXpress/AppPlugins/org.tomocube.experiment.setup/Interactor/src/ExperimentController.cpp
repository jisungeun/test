#include <AppData.h>

#include <UpdateVessels.h>
#include <CreateExperiment.h>
#include <CreateTemplate.h>
#include <CreateExperimentFromTemplate.h>
#include <DuplicateExperiment.h>
#include <DeleteExperiment.h>
#include <LoadExperiment.h>
#include <ChangeExperimentTitle.h>
#include <ChangeMedium.h>
#include <ChangeVesselType.h>
#include <ChangeNumberOfVessel.h>
#include <ChangeExperimentSelection.h>
#include <SaveExperiment.h>
#include <AddGroups.h>
#include <AddWells.h>
#include <RemoveWells.h>
#include <SetWellGroupName.h>
#include <SetWellGroupColor.h>
#include <MoveWellGroup.h>
#include <MoveWell.h>
#include <SetWellName.h>
#include <ExistImagingData.h>
#include <CheckDeletableExperiment.h>
#include <CheckVesselValidation.h>
#include <CheckMediumValidation.h>
#include <SetROIs.h>
#include <ChangeExperimentSampleType.h>
#include <CheckSampleTypeValidation.h>
#include <CheckProfileExistInExperimentFolder.h>

#include "ExperimentController.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    struct ExperimentController::Impl {
        const UseCase::IExperimentOutputPort* presenter{ nullptr };
        const UseCase::IExperimentReaderPort* reader{ nullptr };
        const UseCase::IExperimentWriterPort* writer{ nullptr };
        UseCase::IDirectoryDeleter* directoryDeleter{ nullptr };
    };

    ExperimentController::ExperimentController(
        UseCase::IExperimentOutputPort* presenter,
        UseCase::IExperimentReaderPort* experimentReader,
        UseCase::IExperimentWriterPort* experimentWriter,
        UseCase::IDirectoryDeleter* directoryDeleter)
    : d{ new Impl } {
        d->presenter = presenter;
        d->reader = experimentReader;
        d->writer = experimentWriter;
        d->directoryDeleter = directoryDeleter;
    }

    ExperimentController::~ExperimentController() {
        
    }

    auto ExperimentController::UpdateVessels() const -> bool {
        auto usecase = UseCase::UpdateVessels(d->presenter);
        return usecase.Request();
    }

    auto ExperimentController::CreateExperiment(const QString& experimentTitle) const -> bool {
        auto usecase = UseCase::CreateExperiment(d->presenter, d->writer);
        usecase.SetTitle(experimentTitle);
        return usecase.Request();
    }

    auto ExperimentController::CreateExperimentFromTemplate(const QString& templateTitle, const QString& experimentTitle) const -> bool {
        auto usecase = UseCase::CreateExperimentFromTemplate(d->presenter, d->writer);
        usecase.SetTemplateName(templateTitle);
        usecase.SetNewExperimentName(experimentTitle);
        return usecase.Request();
    }

    auto ExperimentController::CopyExperiment(const QString& experimentTitle, const QString& copyTitle) const -> bool {
        auto usecase = UseCase::DuplicateExperiment(d->presenter, d->reader, d->writer);
        usecase.SetExperiment(experimentTitle, copyTitle);
        return usecase.Request();
    }

    auto ExperimentController::CheckDeletable(const QString& experimentTitle) const -> bool {
        auto usecase = UseCase::CheckDeletableExperiment();
        usecase.SetExperiment(experimentTitle);
        return usecase.Request();
    }

    auto ExperimentController::DeleteExperiment(const QString& experimentTitle) const -> bool {
        auto usecase = UseCase::DeleteExperiment(d->presenter, d->directoryDeleter);
        usecase.SetExperiment(experimentTitle);
        return usecase.Request();
    }

    auto ExperimentController::LoadExperiment(const QString& experimentTitle) const -> bool {
        auto usecase = UseCase::LoadExperiment(d->presenter, d->reader);
        usecase.SetExperiment(experimentTitle);
        return usecase.Request();
    }

    auto ExperimentController::ChangeExperimentTitle(const QString& title) const -> bool {
        auto usecase = UseCase::ChangeExperimentTitle(d->presenter, d->writer);
        usecase.SetTitle(title);
        return usecase.Request();
    }

    auto ExperimentController::ChangeExperimentMedium(const QString& name, double value) const -> bool {
        Q_UNUSED(value)

        auto usecase = UseCase::ChangeMedium(d->presenter);
        usecase.SetMedium(name);
        return usecase.Request();
    }

    auto ExperimentController::ChangeExperimentVesselHolder(const QString& vesselTitle) const -> bool {
        auto usecase = UseCase::ChangeVesselType(d->presenter);
        usecase.SetVesselType(vesselTitle);
        return usecase.Request();
    }

    auto ExperimentController::ChangeExperimentNumberOfVessel(const int32_t number) const -> bool {
        auto usecase = UseCase::ChangeNumberOfVessel(d->presenter);
        usecase.SetNumberOfVessel(number);
        return usecase.Request();
    }

    auto ExperimentController::ChangeExperimentSelection(const QString& title) const -> bool {
        auto usecase = UseCase::ChangeExperimentSelection();
        usecase.SetExperiment(title);
        return usecase.Request();
    }

    auto ExperimentController::ChangeExperimentSampleType(const QString& sampleTypeName) -> bool {
        auto usecase = UseCase::ChangeExperimentSampleType(d->presenter);
        usecase.SetSampleTypeName(sampleTypeName);
        return usecase.Request();
    }

    auto ExperimentController::AddWells(int vesselIndex, int groupIndex, const QList<QPair<int, int>>& wells) const -> bool {
        auto usecase = UseCase::AddWells(d->presenter);
        usecase.Add(vesselIndex, groupIndex, wells);
        return usecase.Request();
    }

    auto ExperimentController::AddWellsToNewGroup(int vesselIndex, const QList<QPair<int, int>>& wells) const -> bool {
        auto usecase = UseCase::AddGroups(d->presenter);
        usecase.SetVesselIndex(vesselIndex);
        usecase.AddWells(wells);

        return usecase.Request();
    }

    auto ExperimentController::AddSeparateSpecimen(int vesselIndex, const QList<QPair<int, int>>& wells) const -> bool {
        auto usecase = UseCase::AddGroups(d->presenter);
        usecase.SetVesselIndex(vesselIndex);
        usecase.AddWells(wells);
        usecase.SetIndividualGroup(true);

        return usecase.Request();
    }

    auto ExperimentController::RemoveWells(int vesselIndex, const QList<QPair<int, int>>& wells) const -> bool {
        auto usecase = UseCase::RemoveWells(d->presenter);
        usecase.SetVesselIndex(vesselIndex);
        usecase.AddRemovalWells(wells);
        return usecase.Request();
    }

    auto ExperimentController::SetWellGroupName(int vesselIndex, int groupIndex, const QString& name) const -> bool {
        auto usecase = UseCase::SetWellGroupName(d->presenter);
        usecase.SetName(vesselIndex, groupIndex, name);
        return usecase.Request();
    }

    auto ExperimentController::SetWellGroupColor(int vesselIndex, int groupIndex, const QColor& color) const -> bool {
        auto usecase = UseCase::SetWellGroupColor();
        usecase.SetColor(vesselIndex, groupIndex, color);
        return usecase.Request();
    }

    auto ExperimentController::SetWellName(int vesselIndex, int wellIndex, const QString& name) const -> bool {
        auto usecase = UseCase::SetWellName(d->presenter);
        usecase.SetName(vesselIndex, wellIndex, name);
        return usecase.Request();
    }

    auto ExperimentController::MoveWellGroup(int vesselIndex, int movingGroupIndex, int targetGroupIndex) const -> bool {
        auto usecase = UseCase::MoveWellGroup();
        usecase.SetVesselIndex(vesselIndex);
        usecase.SetGroupIndex(movingGroupIndex, targetGroupIndex);
        return usecase.Request();
    }

    auto ExperimentController::MoveWell(int vesselIndex, int movingGroupIndex, int targetGroupIndex, const QList<QPair<int, int>>& wells) const -> bool {
        auto usecase = UseCase::MoveWell(d->presenter);
        usecase.SetVesselIndex(vesselIndex);
        usecase.SetWellGroupIndex(movingGroupIndex, targetGroupIndex);
        usecase.AddWells(wells);
        return usecase.Request();
    }

    auto ExperimentController::SaveExperiment() const -> bool {
        auto usecase = UseCase::SaveExperiment(d->presenter, d->writer);
        return usecase.Request();
    }

    auto ExperimentController::RestoreExperiment() const -> bool {
		auto appData = Entity::AppData::GetInstance();
        auto experiment = appData->GetExperiment();
        if(experiment == nullptr) return false;

        auto usecase = UseCase::LoadExperiment(d->presenter, d->reader);
        usecase.SetExperiment(experiment->GetTitle());
        return usecase.Request();
    }

    auto ExperimentController::ExportToExperimentTemplate(const QString& title) const -> bool {
        auto usecase = UseCase::CreateTemplate(d->writer);
        usecase.SetTemplateName(title);
        return usecase.Request();
    }

    auto ExperimentController::ExistImagingData(const QString& experiment) const -> bool {
        auto usecase = UseCase::ExistImagingData();
        usecase.SetExperiment(experiment);
        return usecase.Request();
    }

    auto ExperimentController::IsCurrentVesselValid() const -> bool {
        auto usecase = UseCase::CheckVesselValidation(d->presenter);
        return usecase.Request();
    }

    auto ExperimentController::IsCurrentMediumValid() const -> bool {
        auto usecase = UseCase::CheckMediumValidation(d->presenter);
        return usecase.Request();
    }

    auto ExperimentController::IsCurrentProfileValid() const -> bool {
        auto usecase = UseCase::CheckSampleTypeValidation(d->presenter);
        return usecase.Request();
    }

    auto ExperimentController::IsProfilesExistInExperimentFolder() const -> bool {
        auto usecase = UseCase::CheckProfileExistInExperimentFolder(d->presenter);
        return usecase.Request();
    }

    auto ExperimentController::SetRoiList(AppEntity::VesselIndex vesselIndex, const QMap<AppEntity::WellIndex, QMap<AppEntity::ROIIndex, AppEntity::ROI::Pointer>>& roiList) -> bool {
        auto usecase = UseCase::SetROIs(d->presenter);
        usecase.SetVesselIndex(vesselIndex);
        usecase.SetRoiList(roiList);
        return usecase.Request();
    }
}
