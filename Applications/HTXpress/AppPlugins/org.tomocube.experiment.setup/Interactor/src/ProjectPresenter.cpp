#include "ProjectPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    struct ProjectPresenter::Impl {
        IProjectView* view{ nullptr };
    };

    ProjectPresenter::ProjectPresenter(IProjectView* view) : d{ new Impl } {
        d->view = view;
    }

    ProjectPresenter::~ProjectPresenter() {
        
    }

    auto ProjectPresenter::UpdateList(const QList<QString>& list) const -> void {
        if (d->view) {
            d->view->UpdateList(list);
        }
    }

    auto ProjectPresenter::ChangeProjectSelection(const QString& title) const -> void {
        if (d->view) {
            d->view->ChangeProjectSelection(title);
        }
    }

    auto ProjectPresenter::UpdateInfo(const AppEntity::Project::Pointer& project) const -> void {
        if (d->view) {
            d->view->UpdateDescription(project->GetDescription());
        }
    }

    auto ProjectPresenter::ProjectDeleted(bool isProjectEmpty) const -> void {
        if (d->view) {
            d->view->ProjectDeleted(isProjectEmpty);
        }
    }
}
