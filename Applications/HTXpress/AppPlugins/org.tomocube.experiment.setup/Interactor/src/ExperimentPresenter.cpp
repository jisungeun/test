#include "ExperimentPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    struct ExperimentPresenter::Impl {
        IExperimentView* view{ nullptr };
    };

    ExperimentPresenter::ExperimentPresenter(IExperimentView* view) : d{ new Impl } {
        d->view = view;
    }

    ExperimentPresenter::~ExperimentPresenter() {

    }

    auto ExperimentPresenter::UpdateExperiment(const AppEntity::Experiment::Pointer& data) const -> void {
        if (d->view) {
            d->view->UpdateExperiment(data);
        }
    }

    auto ExperimentPresenter::UpdateList(const QList<Entity::ExperimentShortInfo>& data) const -> void {
        if (d->view) {
            d->view->UpdateList(data);
        }
    }

    auto ExperimentPresenter::UpdateTitle(const QString& title) const -> void {
        if (d->view) {
            d->view->UpdateTitle(title);
        }
    }

    auto ExperimentPresenter::UpdateVesselType(const QString& type) const -> void {
        if (d->view) {
            d->view->UpdateVesselType(type);
        }
    }

    auto ExperimentPresenter::UpdateVesselCount(int count) const -> void {
        if (d->view) {
            d->view->UpdateVesselCount(count);
        }
    }

    auto ExperimentPresenter::UpdateMedium(const AppEntity::Medium& medium) const -> void {
        if (d->view) {
            d->view->UpdateMedium(medium);
        }
    }

    auto ExperimentPresenter::UpdateVessels(const QStringList& vessels) const -> void {
        if (d->view) {
            d->view->UpdateVessels(vessels);
        }    
    }

    auto ExperimentPresenter::UpdateDeleteProgress(int32_t totalCount, int32_t deletedCount) const -> void {
        if (d->view) {
            d->view->UpdateDeleteProgress(totalCount, deletedCount);
        }
    }

    auto ExperimentPresenter::UpdateSampleTypeName(const AppEntity::SampleTypeName& sampleTypeName) const -> void {
        if (d->view) {
            d->view->UpdateSampleTypeName(sampleTypeName);
        }
    }

    auto ExperimentPresenter::AddWellGroup(const QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>>& groupIndices) const -> void {
        if (d->view) {
            d->view->AddWellGroup(groupIndices);
        }
    }

    auto ExperimentPresenter::AddWellsToWellGroup(AppEntity::WellGroupIndex groupIndex, const QList<AppEntity::RowColumn>& indices) const -> void {
        if (d->view) {
            d->view->AddWellsToWellGroup(groupIndex, indices);
        }
    }

    auto ExperimentPresenter::MoveWells(AppEntity::WellGroupIndex movingGroupIndex, AppEntity::WellGroupIndex targetGroupIndex, const QList<AppEntity::RowColumn>& wells) const -> void {
        if (d->view) {
            d->view->MoveWells(movingGroupIndex, targetGroupIndex, wells);
        }
    }

    auto ExperimentPresenter::RemoveWells(const QList<AppEntity::RowColumn>& indices) const -> void {
        if (d->view) {
            d->view->RemoveWells(indices);
        }
    }

    auto ExperimentPresenter::DeleteWellGroups(const QList<AppEntity::WellGroupIndex>& indices) const -> void {
        if (d->view) {
            d->view->DeleteWellGroups(indices);
        }
    }

    auto ExperimentPresenter::ChangeWellGroupName(const AppEntity::WellGroupIndex groupIndex, const QString& groupName) const -> void {
        if (d->view) {
            d->view->ChangeWellGroupName(groupIndex, groupName);
        }
    }

    auto ExperimentPresenter::ChangeWellName(AppEntity::VesselIndex vesselIndex, AppEntity::WellIndex wellIndex, const QString& name) const -> void {
        if (d->view) {
            d->view->ChangeWellName(vesselIndex, wellIndex, name);
        }
    }

    auto ExperimentPresenter::ClearView() const -> void {
        if (d->view) {
            d->view->Clear();
        }
    }

    auto ExperimentPresenter::RoiListUpdated() const -> void {
        if (d->view) {
            d->view->RoiListUpdated();
        }
    }

    auto ExperimentPresenter::ExperimentSaved(bool saved, const QString& message) const -> void {
        if (d->view) {
            d->view->ExperimentSaved(saved, message);
        }
    }

    auto ExperimentPresenter::Error(const QString& message) const -> void {
        if (d->view) {
            d->view->Error(message);
        }
    }

}
