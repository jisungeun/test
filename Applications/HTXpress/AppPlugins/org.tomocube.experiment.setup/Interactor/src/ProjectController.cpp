#include "ProjectController.h"

#include <CreateProject.h>
#include <ScanProjects.h>
#include <ChangeProjectDescription.h>
#include <SelectProject.h>
#include <DeleteProject.h>

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    struct ProjectController::Impl {
        UseCase::IProjectOutputPort* projectPresenter{ nullptr };
        UseCase::IExperimentOutputPort* experimentPresenter{ nullptr };
        UseCase::IProjectReaderPort* projectReader{ nullptr };
        UseCase::IProjectWriterPort* projectWriter{ nullptr };
        UseCase::IExperimentReaderPort* experimentReader{ nullptr };
        UseCase::IDirectoryDeleter* directoryDeleter{ nullptr };
    };

    ProjectController::ProjectController(
        UseCase::IProjectOutputPort* projectPresenter,
        UseCase::IExperimentOutputPort* experimentPresenter,
        UseCase::IProjectReaderPort* projectReader,
        UseCase::IProjectWriterPort* projectWriter,
        UseCase::IExperimentReaderPort* experimentReader,
        UseCase::IDirectoryDeleter* directoryDeleter
    ) : d{ new Impl } {
        d->projectPresenter = projectPresenter;
        d->experimentPresenter = experimentPresenter;
        d->projectReader = projectReader;
        d->projectWriter = projectWriter;
        d->experimentReader = experimentReader;
        d->directoryDeleter = directoryDeleter;
    }

    ProjectController::~ProjectController() {
        
    }

    auto ProjectController::CreateProject(const QString& title) -> bool {
        if (d->projectPresenter == nullptr || d->projectWriter == nullptr) {
            return false;
        }

        auto usecase = UseCase::CreateProject(d->projectPresenter, d->projectWriter);
        usecase.SetNewProjectName(title);
        return usecase.Request();
    }

    auto ProjectController::ScanProjects() -> bool {
        if (d->projectPresenter == nullptr || d->projectReader == nullptr) {
            return false;
        }

        auto usecase = UseCase::ScanProjects(d->projectPresenter, d->projectReader);
        return usecase.Request();
    }

    auto ProjectController::SetDescription(const QString& title, const QString& description) -> bool {
        if (d->projectPresenter == nullptr || d->projectWriter == nullptr) {
            return false;
        }

        auto usecase = UseCase::ChangeProjectDescription(d->projectPresenter, d->projectWriter);
        usecase.SetDescription(title, description);
        return usecase.Request();
    }

    auto ProjectController::SetProject(const QString& title) -> bool {
        auto usecase = UseCase::SelectProject(d->projectPresenter, d->experimentPresenter, d->experimentReader);
        usecase.SetProject(title);
        return usecase.Request();
    }

    auto ProjectController::DeleteProject(const QString& title) -> bool {
        auto usecase = UseCase::DeleteProject(
            d->projectPresenter, 
            d->experimentPresenter, 
            d->experimentReader, 
            d->directoryDeleter);
        usecase.SetProjectTitle(title);
        return usecase.Request();
    }
}
