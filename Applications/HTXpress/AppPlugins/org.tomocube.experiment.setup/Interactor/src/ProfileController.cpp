﻿#include <CopyProfiles.h>

#include "ProfileController.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    struct ProfileController::Impl {
        UseCase::IProfileCopier* copier{};
    };

    ProfileController::ProfileController(UseCase::IProfileCopier* copier) : d{std::make_unique<Impl>()} {
        d->copier = copier;
    }

    ProfileController::~ProfileController() = default;

    auto ProfileController::CopyProfiles(const QString& profileName) const -> bool {
        auto usecase = UseCase::CopyProfiles(d->copier);
        usecase.SetProfileName(profileName);
        return usecase.Request();
    }
}
