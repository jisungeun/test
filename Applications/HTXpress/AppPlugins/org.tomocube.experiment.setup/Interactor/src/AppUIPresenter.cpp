#include "AppUIPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    struct AppUIPresenter::Impl {
        IAppUIView* view{ nullptr };
    };

    AppUIPresenter::AppUIPresenter(IAppUIView* view) : d{ new Impl } {
        d->view = view;
    }

    AppUIPresenter::~AppUIPresenter() {
        
    }

    auto AppUIPresenter::UpdateUI() const -> void {
        if (d->view) {
            d->view->UpdateUI();
        }
    }
}