#pragma once

#include "HTX_Experiment_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    class HTX_Experiment_Setup_Interactor_API IAppUIView {
    public:
        IAppUIView();
        ~IAppUIView();
        
        virtual auto UpdateUI() const -> void = 0;
    };
}