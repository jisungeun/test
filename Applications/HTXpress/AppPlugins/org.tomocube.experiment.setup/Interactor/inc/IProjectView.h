#pragma once

#include <QList>

#include "HTX_Experiment_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    class HTX_Experiment_Setup_Interactor_API IProjectView {
    public:
        IProjectView();
        virtual ~IProjectView();

        /**
         * \brief Project 목록 업데이트
         * \param projects Project 목록
         */
        virtual auto UpdateList(const QList<QString>& projects)->void = 0;

        /**
         * \brief Project 선택 변경
         * \param title 선택된 프로젝트 이름
         */
        virtual auto ChangeProjectSelection(const QString& title)->void = 0;

        /**
         * \brief Project 설명 업데이트
         * \param description 표시될 Project의 설명
         */
        virtual auto UpdateDescription(const QString& description)->void = 0;

        /*
         * \brief Project 삭제 알림
         * \param isProjectEmpty 선택된 프로젝트의 문자열이 empty인지 판단
         */
        virtual auto ProjectDeleted(bool isProjectEmpty)->void = 0;
    };
}