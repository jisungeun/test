﻿#pragma once
#include <memory>
#include <IProfileCopier.h>

#include "HTX_Experiment_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    class HTX_Experiment_Setup_Interactor_API ProfileController final {
    public:
        explicit ProfileController(UseCase::IProfileCopier* copier = nullptr);
        ~ProfileController();

        auto CopyProfiles(const QString& profileName) const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
