#pragma once

#include <QList>
#include <QString>

#include <IProjectOutputPort.h>
#include <Project.h>

#include "IProjectView.h"

#include "HTX_Experiment_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    class HTX_Experiment_Setup_Interactor_API ProjectPresenter : public UseCase::IProjectOutputPort {
    public:
        ProjectPresenter(IProjectView* view);
        ~ProjectPresenter();

        auto UpdateList(const QList<QString>& list) const -> void override;
        auto ChangeProjectSelection(const QString& title) const -> void override;
        auto UpdateInfo(const AppEntity::Project::Pointer& project) const -> void override;
        auto ProjectDeleted(bool isProjectEmpty) const -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
