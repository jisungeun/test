#pragma once

#include <IExperimentReaderPort.h>

#include "HTX_Experiment_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    class HTX_Experiment_Setup_Interactor_API ExperimentTemplateController {
    public:
        ExperimentTemplateController(UseCase::IExperimentReaderPort* experimentReader = nullptr);
        ~ExperimentTemplateController();

        auto UpdateTemplateRepo() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}