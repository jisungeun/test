#pragma once

#include <QList>

#include <VesselMap.h>

#include "HTX_Experiment_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    class HTX_Experiment_Setup_Interactor_API IVesselMapView {
    public:
        IVesselMapView();
        ~IVesselMapView();

        virtual auto UpdateVesselMap(const TC::VesselMap::Pointer vesselMap)->void = 0;
    };
}