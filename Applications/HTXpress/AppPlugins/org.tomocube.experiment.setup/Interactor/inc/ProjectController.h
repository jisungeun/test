#pragma once

#include <IExperimentOutputPort.h>
#include <IExperimentReaderPort.h>
#include <IProjectOutputPort.h>
#include <IProjectReaderPort.h>
#include <IProjectWriterPort.h>
#include <IDirectoryDeleter.h>

#include "HTX_Experiment_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    class HTX_Experiment_Setup_Interactor_API ProjectController {
    public:
        ProjectController(
            UseCase::IProjectOutputPort* projectPresenter = nullptr,
            UseCase::IExperimentOutputPort* experimentPresenter = nullptr,
            UseCase::IProjectReaderPort* projectReader = nullptr,
            UseCase::IProjectWriterPort* projectWriter = nullptr,
            UseCase::IExperimentReaderPort* experimentReader = nullptr,
            UseCase::IDirectoryDeleter* directoryDeleter = nullptr
        );

        ~ProjectController();

        auto CreateProject(const QString& title)->bool;
        auto ScanProjects()->bool;
        auto SetDescription(const QString& title, const QString& description)->bool;
        auto SetProject(const QString& title)->bool;
        auto DeleteProject(const QString& title)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}