#pragma once

#include <QList>

#include <Experiment.h>
#include <ExperimentShortInfo.h>

#include "HTX_Experiment_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    class HTX_Experiment_Setup_Interactor_API IExperimentView {
    public:
        IExperimentView();
        virtual ~IExperimentView();

        virtual auto UpdateExperiment(const AppEntity::Experiment::Pointer& experiment) const->void = 0;
        virtual auto UpdateList(const QList<Entity::ExperimentShortInfo>& data)->void = 0;
        virtual auto UpdateTitle(const QString& title) const -> void = 0;
		virtual auto UpdateVesselType(const QString& type) const -> void = 0;
		virtual auto UpdateVesselCount(int count) const -> void = 0;
        virtual auto UpdateVessels(const QStringList& vessels) const -> void = 0;
        virtual auto UpdateMedium(const AppEntity::Medium& medium) const -> void = 0;
        virtual auto UpdateDeleteProgress(int32_t totalCount, int32_t deleteCount) const -> void = 0;
        virtual auto UpdateSampleTypeName(const AppEntity::SampleTypeName& sampleTypeName) -> void = 0;

        virtual auto AddWellGroup(const QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>>& groupIndices) const -> void = 0;
        virtual auto AddWellsToWellGroup(AppEntity::WellGroupIndex groupIndex, const QList<AppEntity::RowColumn>& indices) const -> void = 0;
        virtual auto MoveWells(AppEntity::WellGroupIndex movingGroupIndex, AppEntity::WellGroupIndex targetGroupIndex, const QList<AppEntity::RowColumn>& wells) const -> void = 0;
        virtual auto RemoveWells(const QList<AppEntity::RowColumn>& indices) const -> void = 0;
        virtual auto DeleteWellGroups(const QList<AppEntity::WellGroupIndex>& indices) const -> void = 0;
        virtual auto ChangeWellGroupName(AppEntity::WellGroupIndex groupIndex, const QString& groupName) -> void = 0;
        virtual auto ChangeWellName(AppEntity::VesselIndex vesseIndex, AppEntity::WellIndex wellIndex, const QString& wellName) -> void = 0;
        virtual auto Clear()->void = 0;
        virtual auto RoiListUpdated()->void = 0;

        virtual auto ExperimentSaved(bool saved, const QString& message) const -> void = 0;
        virtual auto Error(const QString& message) const -> void = 0;
    };
}
