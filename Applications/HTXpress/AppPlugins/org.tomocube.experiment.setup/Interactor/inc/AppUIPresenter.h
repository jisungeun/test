#pragma once

#include <memory>

#include <IAppUIOutputPort.h>

#include "IAppUIView.h"
#include "HTX_Experiment_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    class HTX_Experiment_Setup_Interactor_API AppUIPresenter : public UseCase::IAppUIOutputPort {
    public:
        AppUIPresenter(IAppUIView* view);
        ~AppUIPresenter();

        auto UpdateUI() const -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
