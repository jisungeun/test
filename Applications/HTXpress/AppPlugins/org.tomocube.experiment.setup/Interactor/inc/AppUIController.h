#pragma once

#include <memory>

#include <IAppUIOutputPort.h>

#include "HTX_Experiment_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    class HTX_Experiment_Setup_Interactor_API AppUIController {
    public:
        AppUIController(UseCase::IAppUIOutputPort* presenter);

        ~AppUIController();

        auto UpdateUI() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}