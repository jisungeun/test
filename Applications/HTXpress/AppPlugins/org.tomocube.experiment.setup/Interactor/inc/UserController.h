﻿#pragma once
#include <memory>
#include <QString>
#include "HTX_Experiment_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    class HTX_Experiment_Setup_Interactor_API UserController final {
    public:
        auto CheckPassword(const QString& password) const -> bool;
    };
}
