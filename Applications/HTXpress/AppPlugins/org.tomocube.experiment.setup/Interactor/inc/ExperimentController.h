#pragma once

#include <Experiment.h>
#include <IExperimentOutputPort.h>
#include <IExperimentReaderPort.h>
#include <IExperimentWriterPort.h>
#include <IDirectoryDeleter.h>

#include "HTX_Experiment_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Interactor {
    class HTX_Experiment_Setup_Interactor_API ExperimentController {
    public:
        ExperimentController(
            UseCase::IExperimentOutputPort* presenter = nullptr,
            UseCase::IExperimentReaderPort* experimentReader = nullptr,
            UseCase::IExperimentWriterPort* experimentWriter = nullptr,
            UseCase::IDirectoryDeleter* directoryDeleter = nullptr
        );

        ~ExperimentController();

        auto UpdateVessels() const->bool;

        auto CreateExperiment(const QString& experimentTitle) const->bool;
        auto CreateExperimentFromTemplate(const QString& templateTitle, const QString& experimentTitle) const->bool;
        auto CopyExperiment(const QString& experimentTitle, const QString& copyTitle) const->bool;

        auto CheckDeletable(const QString& experimentTitle) const->bool;
        auto DeleteExperiment(const QString& experimentTitle) const->bool;

        auto LoadExperiment(const QString& experimentTitle) const->bool;

        auto ChangeExperimentTitle(const QString& title) const -> bool;
        auto ChangeExperimentMedium(const QString& name, double value) const -> bool;
        auto ChangeExperimentVesselHolder(const QString& vesselTitle) const -> bool;
        auto ChangeExperimentNumberOfVessel(const int32_t number) const -> bool;
        auto ChangeExperimentSelection(const QString& title) const -> bool;
        auto ChangeExperimentSampleType(const QString& sampleTypeName) -> bool;

        auto AddWells(int vesselIndex, int groupIndex, const QList<QPair<int, int>>& wells) const -> bool;
        auto AddWellsToNewGroup(int vesselIndex, const QList<QPair<int, int>>& wells) const -> bool;
        auto AddSeparateSpecimen(int vesselIndex, const QList<QPair<int, int>>& wells) const -> bool;
        auto RemoveWells(int vesselIndex, const QList<QPair<int, int>>& wells) const -> bool;
        auto SetWellGroupName(int vesselIndex, int groupIndex, const QString& name) const -> bool;
        auto SetWellGroupColor(int vesselIndex, int groupIndex, const QColor& color) const -> bool;
        auto SetWellName(int vesselIndex, int wellIndex, const QString& name) const -> bool;
        auto MoveWellGroup(int vesselIndex, int movingGroupIndex, int targetGroupIndex) const -> bool;
        auto MoveWell(int vesselIndex, int movingGroupIndex, int targetGroupIndex, const QList<QPair<int, int>>& wells) const -> bool;

        auto SaveExperiment() const -> bool;
        auto RestoreExperiment() const -> bool;
        auto ExportToExperimentTemplate(const QString& title) const -> bool;

        auto ExistImagingData(const QString& experiment) const -> bool;

        auto IsCurrentVesselValid() const -> bool;
        auto IsCurrentMediumValid() const -> bool;
        auto IsCurrentProfileValid() const -> bool;
        auto IsProfilesExistInExperimentFolder() const -> bool;

        auto SetRoiList(AppEntity::VesselIndex vesselIndex, const QMap<AppEntity::WellIndex, QMap<AppEntity::ROIIndex, AppEntity::ROI::Pointer>>& roiList) -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}