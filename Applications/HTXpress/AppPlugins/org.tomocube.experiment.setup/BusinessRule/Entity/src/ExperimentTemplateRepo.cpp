#include "ExperimentTemplateRepo.h"

#include <QStandardPaths>
#include <QFileInfo>

#include <System.h>
#include <SessionManager.h>
#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::Experiment::Setup::Entity {
    struct ExperimentTemplateRepoItem {
        AppEntity::Experiment::Pointer data;
        TemplateType type{ TemplateType::System };
        QDateTime lastModified;
    };

    struct ExperimentTemplateRepo::Impl {
        QMap<QString, ExperimentTemplateRepoItem> templates;
    };

    ExperimentTemplateRepo::ExperimentTemplateRepo() : d{ new Impl } {
    }

    ExperimentTemplateRepo::~ExperimentTemplateRepo() {
    }

    auto ExperimentTemplateRepo::GetInstance() -> Pointer {
        static Pointer theInstance{ new ExperimentTemplateRepo() };
        return theInstance;
    }

    auto ExperimentTemplateRepo::SetTemplate(const QString& title, const AppEntity::Experiment::Pointer& temp, TemplateType type) -> void {
        ExperimentTemplateRepoItem item;
        item.data = temp;
        item.type = type;

        QString templateSubPath;
        if (type == +TemplateType::System) {
            templateSubPath = "template";
        } else {
            templateSubPath = "user/" + AppEntity::SessionManager::GetInstance()->GetID();
        }

        auto path = QString("%1/experiments/%2/%3.tcxexp")
        .arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation))
        .arg(templateSubPath)
        .arg(title);

        item.lastModified = QFileInfo(path).lastModified();

        d->templates.insert(title, item);
    }

    auto ExperimentTemplateRepo::GetTemplate(const QString& title) const -> AppEntity::Experiment::Pointer {
        if (d->templates.find(title) == d->templates.end()) {
            return nullptr;
        }

        return d->templates.value(title).data;
    }

    auto ExperimentTemplateRepo::GetTemplates() const -> QStringList {
        return d->templates.keys();
    }

    auto ExperimentTemplateRepo::GetTemplateLastModified(const QString& title) const -> QDateTime {
        if (d->templates.find(title) == d->templates.end()) {
            return QDateTime();
        }

        return d->templates.value(title).lastModified;
    }

    auto ExperimentTemplateRepo::Clear() -> void {
        d->templates.clear();
    }
}
