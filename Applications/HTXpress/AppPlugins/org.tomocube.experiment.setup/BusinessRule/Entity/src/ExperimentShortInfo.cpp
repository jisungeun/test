#include "ExperimentShortInfo.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Entity {
    struct ExperimentShortInfo::Impl {
        AppEntity::ExperimentID id;
        QString title;
        QString date;
        AppEntity::ExperimentProgress progress{ AppEntity::ExperimentProgress::NotStarted };

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other) const->bool;
    };

    auto ExperimentShortInfo::Impl::operator=(const Impl& other) -> Impl& {
        id = other.id;
        title = other.title;
        date = other.date;
        progress = other.progress;

        return *this;
    }

    auto ExperimentShortInfo::Impl::operator==(const Impl& other) const->bool {
        if (id != other.id) return false;
        if (title != other.title) return false;
        if (date != other.date) return false;
        if (progress != other.progress) return false;

        return true;
    }

    ExperimentShortInfo::ExperimentShortInfo() : d{ new Impl } {
        
    }

    ExperimentShortInfo::ExperimentShortInfo(const ExperimentShortInfo& other) : d{ new Impl } {
        *d = *other.d;
    }

    ExperimentShortInfo::ExperimentShortInfo(const AppEntity::Experiment& experiment) : d{ new Impl } {
        d->id = experiment.GetID();
        d->title = experiment.GetTitle();
        d->date = experiment.GetCreatedDate();
        d->progress = experiment.GetProgress();
    }

    ExperimentShortInfo::~ExperimentShortInfo() {
        
    }
    auto ExperimentShortInfo::SetID(const AppEntity::ExperimentID id) -> void {
        d->id = id;
    }

    auto ExperimentShortInfo::GetID() const -> AppEntity::ExperimentID {
        return d->id;
    }

    auto ExperimentShortInfo::SetTitle(const QString& title) -> void {
        d->title = title;
    }

    auto ExperimentShortInfo::GetTitle() const -> QString {
        return d->title;
    }

    auto ExperimentShortInfo::SetDate(const QString& date) -> void {
        d->date = date;
    }

    auto ExperimentShortInfo::GetDate() const -> QString {
        return d->date;
    }

    auto ExperimentShortInfo::SetProgress(const AppEntity::ExperimentProgress progress)-> void {
        d->progress = progress;
    }

    auto ExperimentShortInfo::GetProgress() const -> AppEntity::ExperimentProgress {
        return d->progress;
    }

    auto ExperimentShortInfo::operator=(const ExperimentShortInfo& other) -> ExperimentShortInfo& {
        *d = *other.d;
        return *this;
    }

    auto ExperimentShortInfo::operator==(const ExperimentShortInfo& other) const -> bool {
        return *d == *other.d;
    }

    auto ExperimentShortInfo::operator!=(const ExperimentShortInfo& other) const -> bool {
        return !(*d == *other.d);
    }

}
