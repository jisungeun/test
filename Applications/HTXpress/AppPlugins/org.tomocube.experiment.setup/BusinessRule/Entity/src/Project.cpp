#include "Project.h"

#include <QList>

namespace HTXpress::ExperimentSetup::Entity {
    struct Project::Impl {
        QString title;
        QString description;

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other) const->bool;
    };

    auto Project::Impl::operator=(const Impl& other) -> Impl& {
        title = other.title;
        description = other.description;
        return *this;
    }

    auto Project::Impl::operator==(const Impl& other) const -> bool {
        if (title != other.title) return false;
        if (description != other.description) return false;
        return true;
    }

    Project::Project() : d{ new Impl } {
    }

    Project::Project(const Project& other) : d{ new Impl } {
        *this = other;
    }

    Project::~Project() {
    }

    auto Project::operator=(const Project& other) -> Project& {
        *d = *other.d;
        return *this;
    }

    auto Project::operator==(const Project& other) const -> bool {
        return *d == *other.d;
    }

    auto Project::SetTitle(const QString& title) -> void {
        d->title = title;
    }

    auto Project::GetTitle() const -> QString {
        return d->title;
    }

    auto Project::SetDescription(const QString& description) -> void {
        d->description = description;
    }

    auto Project::GetDescription() const -> QString {
        return d->description;
    }
}
