#include "VesselRepo.h"

#include <QMap>

namespace HTXpress::ExperimentSetup::Entity {
    struct VesselRepo::Impl {
        QMap<QString, AppEntity::Vessel::Pointer> vessels;
        QMap<QString, QString> models;
    };

    VesselRepo::VesselRepo() : d{ new Impl } {
    }

    VesselRepo::~VesselRepo() {
    }

    auto VesselRepo::GetInstance()->Pointer {
        static Pointer theInstance{ new AppEntity::VesselRepo() };
        return theInstance;
    }

    auto VesselRepo::AddVessel(const AppEntity::Vessel::Pointer& holder) -> void {
        d->vessels[holder->GetModel()] = holder;
        d->models[holder->GetName()] = holder->GetModel();
    }

    auto VesselRepo::GetVessels() const->QList<AppEntity::Vessel::Pointer> {
        return d->vessels.values();
    }

    auto VesselRepo::GetVesselByName(const QString& name) const -> AppEntity::Vessel::Pointer {
        const auto& model = d->models[name];
        return d->vessels[model];
    }

    auto VesselRepo::GetVesselByModel(const QString& model) const -> AppEntity::Vessel::Pointer {
        return d->vessels[model];
    }
}
