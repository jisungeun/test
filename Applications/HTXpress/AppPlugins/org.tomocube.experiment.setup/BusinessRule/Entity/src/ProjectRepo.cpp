#include "ProjectRepo.h"

#include <QMap>

namespace HTXpress::ExperimentSetup::Entity {
    struct ProjectRepo::Impl {
        QMap<QString, Project::Pointer> projects;
    };

    ProjectRepo::ProjectRepo() : d{ new Impl } {
    }

    ProjectRepo::~ProjectRepo() {
    }

    auto ProjectRepo::GetInstance()->Pointer {
        static Pointer theInstance{ new ProjectRepo() };
        return theInstance;
    }

    auto ProjectRepo::AddProject(const Project::Pointer& project)->void {
        d->projects[project->GetTitle()] = project;
    }

    auto ProjectRepo::GetProject(const QString& projectName) const->Project::Pointer {
        if (d->projects.find(projectName) == d->projects.end()) {
            return nullptr;
        }

        return d->projects[projectName];
    }

    auto ProjectRepo::GetProjectTitles() const->QList<QString> {
        return  d->projects.keys();
    }

    auto ProjectRepo::ClearAll()->void {
        d->projects.clear();
    }
}