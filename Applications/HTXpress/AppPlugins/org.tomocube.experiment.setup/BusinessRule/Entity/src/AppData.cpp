#include "AppData.h"

#include <System.h>
#include <SessionManager.h>

namespace HTXpress::AppPlugins::Experiment::Setup::Entity {
	struct AppData::Impl {
		QString currentProject;

	    AppEntity::Experiment::Pointer originExperiment{ nullptr };    // 로드된 experiment data의 변경사항 비교용
	    AppEntity::Experiment::Pointer currentExperiment{ nullptr };  // 현재 사용중인 experiment
		AppEntity::ExperimentID selectedExperiment;	// Experiment list ui에서 선택된 experiment id

        QMap<AppEntity::ExperimentID, ExperimentShortInfo> shortInfos;
	};

	AppData::AppData() : d{ new Impl } {
        
    }

	AppData::~AppData() {
        
    }

    auto AppData::Clear() -> void {
        d->currentProject = QString();
        d->originExperiment = nullptr;
        d->currentExperiment = nullptr;
        d->selectedExperiment = QString();
        d->shortInfos.clear();
    }

	auto AppData::GetInstance() -> Pointer {
        static Pointer theInstance{ new AppData() };
		return theInstance;
    }

    auto AppData::SetProjectTitle(const QString& project) -> void {
	    d->currentProject = project;
	}

    auto AppData::GetProjectTitle() const -> QString {
	    return d->currentProject;
	}

	auto AppData::SetExperiment(const AppEntity::Experiment::Pointer experiment) -> void {
        d->originExperiment.reset(new AppEntity::Experiment(*experiment));
        d->currentExperiment = experiment;
    }

    auto AppData::GetExperiment() const -> AppEntity::Experiment::Pointer {
        return d->currentExperiment;
    }

    auto AppData::GetOriginExperiment() const->AppEntity::Experiment::Pointer {
        return d->originExperiment;
    }

    auto AppData::SetSelectedExperiment(const AppEntity::ExperimentID& id) -> void {
	    d->selectedExperiment = id;
	}

	auto AppData::GetSelectedExperiment() const -> Entity::ExperimentShortInfo {
	    return d->shortInfos.value(d->selectedExperiment);
	}

	auto AppData::SetExperimentListInfo(const AppEntity::ExperimentID& id, const ExperimentShortInfo& info) -> void {
        d->shortInfos.insert(id, info);
    }

    auto AppData::GetExperimentListInfo(const AppEntity::ExperimentID& id) const -> ExperimentShortInfo {
        return d->shortInfos.value(id);
    }

    auto AppData::GetExperimentListInfos() const -> QList<ExperimentShortInfo> {
        return d->shortInfos.values();
    }

    auto AppData::RemoveExperimentListInfo(const AppEntity::ExperimentID& id) -> void {
        d->shortInfos.remove(id);
    }

    auto AppData::ClearExperimentListInfo() -> void {
        d->shortInfos.clear();
    }

    auto AppData::GetCurrentProjectPath() -> QString {
        return GetProjectPath(d->currentProject);
    }

    auto AppData::GetCurrentExperimentPath() -> QString {
        if (d->currentExperiment == nullptr) return QString();
        return GetExperimentPath(d->currentProject, d->currentExperiment->GetTitle());
	}

    auto AppData::GetCurrentExperimentFilePath() -> QString {
        if (d->currentExperiment == nullptr) return QString();
        return GetExperimentFilePath(d->currentProject, d->currentExperiment->GetTitle());
	}

    auto AppData::GetProjectPath(const QString& projectTitle) -> QString {
        const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
        const auto dataPath = systemConfig->GetDataDir();
        if (dataPath.isEmpty()) return QString();

        const auto userId = AppEntity::SessionManager::GetInstance()->GetID();
        if (userId.isEmpty()) return QString();

        if (projectTitle.isEmpty()) return QString();

        return dataPath + "/" + userId + "/" + projectTitle;
    }

    auto AppData::GetExperimentPath(const QString& projectTitle, const QString& experimentTitle) -> QString {
        const auto projectPath = GetProjectPath(projectTitle);
        if (projectPath.isEmpty()) return QString();

        if (experimentTitle.isEmpty()) return QString();

        return projectPath + "/" + experimentTitle;
    }

    auto AppData::GetExperimentFilePath(const QString& projectTitle, const QString& experimentTitle) -> QString {
        const auto projectPath = GetProjectPath(projectTitle);
        if (projectPath.isEmpty()) return QString();

        if (experimentTitle.isEmpty()) return QString();

        return projectPath + "/" + experimentTitle + "/" + experimentTitle + "." + AppEntity::ExperimentExtension;
    }

}
