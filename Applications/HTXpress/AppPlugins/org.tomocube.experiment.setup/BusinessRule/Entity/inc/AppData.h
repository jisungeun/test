#pragma once

#include <memory>

#include <QString>

#include <Experiment.h>

#include "ExperimentShortInfo.h"
#include "HTX_Experiment_Setup_EntityExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Entity {
	class HTX_Experiment_Setup_Entity_API AppData {
	public:
		using Pointer = std::shared_ptr<AppData>;

	private:
		AppData();

	public:
		~AppData();

		static auto GetInstance()->Pointer;

		auto Clear()->void;
		auto SetProjectTitle(const QString& project)->void;
        auto GetProjectTitle() const->QString;

		auto SetExperiment(const AppEntity::Experiment::Pointer experiment)->void;
	    auto GetExperiment() const->AppEntity::Experiment::Pointer;
        auto GetOriginExperiment() const->AppEntity::Experiment::Pointer;

		auto SetSelectedExperiment(const AppEntity::ExperimentID& id)->void;
		auto GetSelectedExperiment() const->Entity::ExperimentShortInfo;

		auto SetExperimentListInfo(const AppEntity::ExperimentID& id, const ExperimentShortInfo& info)->void;
        auto GetExperimentListInfo(const AppEntity::ExperimentID& id) const->ExperimentShortInfo;
        auto GetExperimentListInfos() const->QList<ExperimentShortInfo>;
        auto RemoveExperimentListInfo(const AppEntity::ExperimentID& id)->void;
		auto ClearExperimentListInfo()->void;

		auto GetCurrentProjectPath()->QString;
		auto GetCurrentExperimentPath()->QString;
		auto GetCurrentExperimentFilePath()->QString;

		auto GetProjectPath(const QString& projectTitle)->QString;
		auto GetExperimentPath(const QString& projectTitle, const QString& experimentTitle)->QString;
		auto GetExperimentFilePath(const QString& projectTitle, const QString& experimentTitle)->QString;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}