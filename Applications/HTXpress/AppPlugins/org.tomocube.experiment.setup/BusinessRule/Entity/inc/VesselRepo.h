#pragma once

#include <memory>

#include <QList>

#include <Vessel.h>

#include "HTX_Experiment_Setup_EntityExport.h"

namespace HTXpress::ExperimentSetup::Entity {
    class HTX_Experiment_Setup_Entity_API VesselRepo {
    public:
        typedef std::shared_ptr<AppEntity::VesselRepo> Pointer;

    private:
        VesselRepo();

    public:
        ~VesselRepo();

        static auto GetInstance()->Pointer;

        auto AddVessel(const AppEntity::Vessel::Pointer& holder)->void;
        auto GetVessels() const->QList<AppEntity::Vessel::Pointer>;
        auto GetVesselByName(const QString& name) const->AppEntity::Vessel::Pointer;
        auto GetVesselByModel(const QString& model) const->AppEntity::Vessel::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}