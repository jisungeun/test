#pragma once

#include <memory>

#include "AppEntityDefines.h"

#include "HTX_Experiment_Setup_EntityExport.h"

namespace HTXpress::ExperimentSetup::Entity {
    class HTX_Experiment_Setup_Entity_API Project {
    public:
        typedef std::shared_ptr<Project> Pointer;

    public:
        Project();
        Project(const Project& other);
        virtual ~Project();

        auto operator=(const Project& other)->Project&;
        auto operator==(const Project& other) const->bool;

        auto SetTitle(const QString& title)->void;
        auto GetTitle() const->QString;

        auto SetDescription(const QString& description)->void;
        auto GetDescription() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}