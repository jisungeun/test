#pragma once

#include <memory>

#include "Project.h"
#include "HTX_Experiment_Setup_EntityExport.h"

namespace HTXpress::ExperimentSetup::Entity {
    class HTX_Experiment_Setup_Entity_API ProjectRepo {
    public:
        typedef std::shared_ptr<ProjectRepo> Pointer;

    private:
        ProjectRepo();

    public:
        ~ProjectRepo();

        static auto GetInstance()->Pointer;

        auto AddProject(const Project::Pointer& project)->void;
        auto GetProject(const QString& projectName) const->Project::Pointer;
        auto GetProjectTitles() const->QList<QString>;

        auto ClearAll()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}