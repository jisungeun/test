#pragma once

#include <memory>

#include <QDate>

#include <AppEntityDefines.h>
#include <Experiment.h>

#include "HTX_Experiment_Setup_EntityExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Entity {
    class HTX_Experiment_Setup_Entity_API ExperimentShortInfo {
    public:
        using Pointer = std::shared_ptr<ExperimentShortInfo>;

    public:
        ExperimentShortInfo();
        ExperimentShortInfo(const ExperimentShortInfo& other);
        ExperimentShortInfo(const AppEntity::Experiment& experiment);

        ~ExperimentShortInfo();

        auto SetID(const AppEntity::ExperimentID id)->void;
        auto GetID() const->AppEntity::ExperimentID;

        auto SetTitle(const QString& title)->void;
        auto GetTitle() const->QString;

        auto SetDate(const QString& date)->void;
        auto GetDate() const->QString;

        auto SetProgress(const AppEntity::ExperimentProgress progress)->void;
        auto GetProgress() const->AppEntity::ExperimentProgress;

        auto operator=(const ExperimentShortInfo& other)->ExperimentShortInfo&;
        auto operator==(const ExperimentShortInfo& other) const->bool;
        auto operator!=(const ExperimentShortInfo& other) const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}