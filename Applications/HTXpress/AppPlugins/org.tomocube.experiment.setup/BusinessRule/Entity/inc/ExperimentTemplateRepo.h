#pragma once
#include <memory>

#include <enum.h>

#include <Experiment.h>

#include "HTX_Experiment_Setup_EntityExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::Entity {
    BETTER_ENUM(TemplateType, int32_t,
        System,
        User
    );

    class HTX_Experiment_Setup_Entity_API ExperimentTemplateRepo {
    public:
        using Pointer = std::shared_ptr<ExperimentTemplateRepo>;
        
    public:
        ExperimentTemplateRepo();
        ~ExperimentTemplateRepo();

        static auto GetInstance()->Pointer;

        auto SetTemplate(const QString& title, const AppEntity::Experiment::Pointer& temp, TemplateType type)->void;
        auto GetTemplate(const QString& title) const->AppEntity::Experiment::Pointer;
        auto GetTemplates() const->QStringList;

        auto GetTemplateLastModified(const QString& title) const->QDateTime;

        auto Clear()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}