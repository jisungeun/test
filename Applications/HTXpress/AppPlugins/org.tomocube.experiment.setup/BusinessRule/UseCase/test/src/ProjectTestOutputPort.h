#pragma once

#include <IProjectOutputPort.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase::Test {
    class ProjectTestOutputPort final : public UseCase::IProjectOutputPort {
    public:
        ProjectTestOutputPort();
        ~ProjectTestOutputPort() override;

        auto UpdateList(const QList<QString>& list) const -> void override;
        auto ChangeProjectSelection(const QString& title) const -> void override;
        auto UpdateInfo(const AppEntity::Project::Pointer& project) const -> void override;
        auto ProjectDeleted(bool isProjectEmpty) const -> void override;

        auto Projects() const->QList<QString>;
        auto Project() const->AppEntity::Project::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
