#include <catch2/catch.hpp>

#include <QFile>
#include <QDir>

#include <AppData.h>
#include <CreateExperiment.h>
#include <CreateTemplate.h>
#include <CreateExperimentFromTemplate.h>
#include <DuplicateExperiment.h>
#include <LoadExperiment.h>
#include <ChangeExperimentTitle.h>
#include <ChangeMedium.h>
#include <ChangeVesselType.h>
#include <ChangeNumberOfVessel.h>
#include <ChangeExperimentSelection.h>
#include <SaveExperiment.h>
#include <DeleteExperiment.h>
#include <ExperimentReaderPort.h>
#include <ExperimentWriterPort.h>
#include <ExperimentTemplateRepo.h>
#include <SessionManager.h>
#include <System.h>
#include <UserManager.h>
#include <VesselRepo.h>
#include <DirectoryDeleterPlugin.h>

#include "TestDataGenerator.h"
#include "ExperimentTestOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase::Test {
    SCENARIO("Experiment can be created, loaded, saved and deleted") {
        GIVEN("Test environments") {
            TestDataGenerator testData;

            // test용 data path 설정
            auto system = HTXpress::AppEntity::System::GetInstance();
            auto systemConfig = std::make_shared<HTXpress::AppEntity::SystemConfig>();
            systemConfig->SetDataDir(testData.Root());
            system->SetSystemConfig(systemConfig);

            UNSCOPED_INFO(QString("Data path: " + system->GetSystemConfig()->GetDataDir()).toStdString());

            // test user 생성
            auto testUser = std::make_shared<HTXpress::AppEntity::User>();
            testUser->SetUserID(testData.User());
            testUser->SetName(testData.User());

            auto userManager = HTXpress::AppEntity::UserManager::GetInstance();
            userManager->AddUser(testUser);

            // test용 user 로그인
            auto sessionManager = HTXpress::AppEntity::SessionManager::GetInstance();
            sessionManager->Login(testUser);

            UNSCOPED_INFO(QString("Login user: " + sessionManager->GetID()).toStdString());

            auto vesselRepo = AppEntity::VesselRepo::GetInstance();
            for (auto vesselModel : testData.VesselModelNames()) {
                auto vessel = std::make_shared<AppEntity::Vessel>();    
                vessel->SetModel(vesselModel);
                vessel->SetName(vesselModel);
                vesselRepo->AddVessel(vessel);
            }
            
            auto appData = Entity::AppData::GetInstance();
            auto testProjectTitle = testData.Projects().keys()[0];
            appData->SetProjectTitle(testProjectTitle);
            appData->SetExperiment(testData.Experiments(testProjectTitle)[0]);

            ExperimentTestOutputPort outputPort;
            Plugins::ExperimentIO::ExperimentReaderPort reader;
            Plugins::ExperimentIO::ExperimentWriterPort writer;

            // test용 experiment template 등록
            auto templateRepo = Entity::ExperimentTemplateRepo::GetInstance();
            auto testTemplate = std::make_shared<AppEntity::Experiment>();
            if (reader.Read(QString(_TEST_DATA) + "/test-experiment.tcxexp", testTemplate)) {
                templateRepo->SetTemplate(testTemplate->GetTitle(), testTemplate, Entity::TemplateType::User);
            }

            WHEN("New experiment is created") {
                THEN("Empty experiment add to project") {
                    const QString newExperimentTitle("experiment_new");

                    auto usecase = CreateExperiment(&outputPort, &writer);
                    usecase.SetTitle(newExperimentTitle);
                    REQUIRE(usecase.Request());

                    bool found = false;
                    for (auto info : outputPort.ShortInfos()) {
                        if (info.GetTitle() == newExperimentTitle) {
                            found = true;
                            break;
                        }
                    }

                    REQUIRE(found);

                    REQUIRE(outputPort.Experiment()->GetTitle() == newExperimentTitle);
                }

                THEN("Created experiment from template add to project") {
                    const QString newExperimentTitle("experiment_from_template");

                    auto usecase = CreateExperimentFromTemplate(&outputPort, &writer);
                    usecase.SetTemplateName(testTemplate->GetTitle());
                    usecase.SetNewExperimentName(newExperimentTitle);
                    REQUIRE(usecase.Request());

                    bool found = false;
                    for (auto info : outputPort.ShortInfos()) {
                        if (info.GetTitle() == newExperimentTitle) {
                            found = true;
                            break;
                        }
                    }

                    REQUIRE(found);

                    REQUIRE(outputPort.Experiment()->GetTitle() == newExperimentTitle);
                }

                THEN("Experiment duplicate") {
                    auto experiments = testData.Experiments(appData->GetProjectTitle());
                    auto experimentTitle = experiments[0]->GetTitle();
                    auto copyTitle = "copied-experiment";

                    auto usecase = DuplicateExperiment(&outputPort, &reader, &writer);
                    usecase.SetExperiment(experimentTitle, copyTitle);
                    REQUIRE(usecase.Request());

                    bool found = false;
                    for (auto info : outputPort.ShortInfos()) {
                        if (info.GetTitle() == copyTitle) {
                            found = true;
                            break;
                        }
                    }

                    REQUIRE(found);

                    REQUIRE(outputPort.Experiment()->GetTitle() == copyTitle);
                }
            }

            WHEN("Experiment is loaded") {
                THEN("Current experiment data change") {
                    auto experiment = testData.Experiments(appData->GetProjectTitle())[0];

                    auto log = QString("Vessel count: %1").arg(experiment->GetVesselCount());
                    UNSCOPED_INFO(log.toStdString());

                    auto experimentTitle = experiment->GetTitle();

                    auto usecase = LoadExperiment(&outputPort, &reader);
                    usecase.SetExperiment(experimentTitle);
                    REQUIRE(usecase.Request());

                    REQUIRE(*outputPort.Experiment() == *experiment);
                }
            }

            WHEN("Experiment is changed") {
                THEN("Title change") {
                    auto changedTitle = QString("experiment_title_changed");
                    auto usecase = ChangeExperimentTitle(&outputPort, &writer);
                    usecase.SetTitle(changedTitle);
                    REQUIRE(usecase.Request());
                    REQUIRE(appData->GetExperiment()->GetTitle() == changedTitle);
                    REQUIRE(outputPort.Experiment()->GetTitle() == changedTitle);
                }

                THEN("Medium change") {
                    AppEntity::MediumData changedMedium;
                    changedMedium.SetName("medium_changed");
                    changedMedium.SetRI(0.123);

                    auto usecase = ChangeMedium(&outputPort);
                    usecase.SetMedium(changedMedium.GetName());
                    REQUIRE(usecase.Request());
                    REQUIRE(appData->GetExperiment()->GetMedium() == changedMedium);
                    REQUIRE(outputPort.Experiment()->GetMedium() == changedMedium);
                }

                THEN("Vessel model change") {
                    auto changedVesselModel = testData.VesselModelNames()[1];
                    auto usecase = ChangeVesselType(&outputPort);
                    usecase.SetVesselType(changedVesselModel);
                    REQUIRE(usecase.Request());

                    auto vessel = appData->GetExperiment()->GetVessel();
                    REQUIRE(vessel->GetModel() == changedVesselModel);

                    vessel = outputPort.Experiment()->GetVessel();
                    REQUIRE(vessel->GetModel() == changedVesselModel);
                }

                THEN("Vessel count change") {
                    auto changedVesselCount = 5;

                    auto usecase = ChangeNumberOfVessel(&outputPort);
                    usecase.SetNumberOfVessel(changedVesselCount);
                    REQUIRE(usecase.Request());

                    REQUIRE(appData->GetExperiment()->GetVesselCount() == changedVesselCount);
                    REQUIRE(outputPort.Experiment()->GetVesselCount() == changedVesselCount);
                }
            }

            WHEN("Experiment is saved") {
                // save 테스트 용 experiment 추가
                auto usecaseCreate = CreateExperiment(&outputPort, &writer);

                const QString newExperimentTitle("experiment_save_test");
                usecaseCreate.SetTitle(newExperimentTitle);
                REQUIRE(usecaseCreate.Request());


                THEN("Opend experiment save") {
                    auto usecaseSave = SaveExperiment(&outputPort, &writer);
                    REQUIRE(usecaseSave.Request());

                    QString newFilePath = 
                        testData.Root() + "/" + 
                        testData.User() + "/" + 
                        appData->GetProjectTitle() + "/" + 
                        newExperimentTitle + "/" +
                        newExperimentTitle + "." + AppEntity::ExperimentExtension;

                    REQUIRE(QFile::exists(newFilePath));
                }

                THEN("Opened experiment export as template") {
                    const QString templateTitle("template_test");
                    auto usecaseCreateTemplate = CreateTemplate(&writer);
                    usecaseCreateTemplate.SetTemplateName(templateTitle);
                    REQUIRE(usecaseCreateTemplate.Request());

                    REQUIRE(templateRepo->GetTemplate(templateTitle) != nullptr);

                    QString templatePath = 
                        testData.Root() + "/" + 
                        testData.User() + "/" + 
                        templateTitle + "." + AppEntity::ExperimentExtension;
                    REQUIRE(QFile::exists(templatePath));
                }
            }

            WHEN("Experiment is deleted") {
                THEN("Experiment delete") {
                    auto experiments = testData.Experiments(appData->GetProjectTitle());
                    auto deleteExperimentTitle = experiments.last()->GetTitle();
                    auto deleter = Plugins::DirectoryDeleter::Deleter();
                    auto usecase = DeleteExperiment(&outputPort, &deleter);

                    usecase.SetExperiment(deleteExperimentTitle);
                    REQUIRE(usecase.Request());

                    QString templatePath = 
                        testData.Root() + "/" + 
                        testData.User() + "/" + 
                        deleteExperimentTitle;

                    QDir dir(templatePath);
                    REQUIRE_FALSE(dir.exists());

                    deleter.SetRootPath(templatePath);

                    bool found = false;
                    for (auto info : outputPort.ShortInfos()) {
                        if (info.GetTitle() == deleteExperimentTitle) {
                            found = true;
                            break;
                        }
                    }

                    REQUIRE_FALSE(found);
                }
            }
        }
    }
}