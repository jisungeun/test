#pragma once

#include <IExperimentOutputPort.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase::Test {
    class ExperimentTestOutputPort : public UseCase::IExperimentOutputPort {
    public:
        ExperimentTestOutputPort();
        ~ExperimentTestOutputPort() override;

        auto UpdateExperiment(const AppEntity::Experiment::Pointer& data) const -> void override;
		auto UpdateList(const QList<Entity::ExperimentShortInfo>& data) const -> void override;
        auto UpdateTitle(const QString& title) const -> void override;
		auto UpdateVesselType(const QString& type) const -> void override;
		auto UpdateVesselCount(int count) const -> void override;
        auto UpdateVessels(const QStringList& vessels) const -> void override;
        auto UpdateMedium(const AppEntity::Medium& medium) const -> void override;
        auto UpdateDeleteProgress(int32_t totalCount, int32_t deletedCount) const -> void override;
        auto UpdateSampleTypeName(const AppEntity::SampleTypeName& sampleTypeName) const -> void override;
        auto AddWellGroup(const QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>>& groupIndices) const -> void override;
        auto AddWellsToWellGroup(AppEntity::WellGroupIndex groupIndex, const QList<AppEntity::RowColumn>& indices) const -> void override;
        auto MoveWells(AppEntity::WellGroupIndex movingGroupIndex, AppEntity::WellGroupIndex targetGroupIndex, const QList<AppEntity::RowColumn>& wells) const -> void override;
        auto RemoveWells(const QList<AppEntity::RowColumn>& indices) const -> void override;
        auto DeleteWellGroups(const QList<AppEntity::WellGroupIndex>& indices) const -> void override;
        auto ChangeWellGroupName(const AppEntity::WellGroupIndex groupIndex, const QString& groupName) const -> void override;
        auto ChangeWellName(AppEntity::VesselIndex vesselIndex, AppEntity::WellIndex wellIndex, const QString& wellName) const -> void override;
		auto ClearView() const -> void override;
        auto ExperimentSaved(bool saved, const QString& path) const -> void override;
        auto Error(const QString& message) const -> void override;
        auto Experiment() const -> AppEntity::Experiment::Pointer;
        auto ShortInfos() const->QList<Entity::ExperimentShortInfo>;
        auto RoiListUpdated() const -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
