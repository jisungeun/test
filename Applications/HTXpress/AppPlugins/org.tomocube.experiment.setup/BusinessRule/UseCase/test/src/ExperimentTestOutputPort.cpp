#include "ExperimentTestOutputPort.h"

#include <QList>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase::Test {
    struct ExperimentTestOutputPort::Impl {
        AppEntity::Experiment::Pointer experiment{ nullptr };
        QList<Entity::ExperimentShortInfo> shortInfos;
        QString vesselType;
        QStringList vessels;
        QString medium;
        QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>> wellGroups;
    };

    ExperimentTestOutputPort::ExperimentTestOutputPort() : d{ new Impl } {
        
    }

    ExperimentTestOutputPort::~ExperimentTestOutputPort() {
        
    }

    auto ExperimentTestOutputPort::UpdateExperiment(const AppEntity::Experiment::Pointer& data) const -> void {
        d->experiment = data;
    }

    auto ExperimentTestOutputPort::UpdateList(const QList<Entity::ExperimentShortInfo>& data) const -> void {
        d->shortInfos = data;
    }

    auto ExperimentTestOutputPort::UpdateTitle(const QString& title) const -> void {
        d->experiment->SetTitle(title);
    }

    auto ExperimentTestOutputPort::UpdateVesselType(const QString& type) const -> void {
        d->vesselType = type;
    }

    auto ExperimentTestOutputPort::UpdateVesselCount(int count) const -> void {
        d->experiment->SetVesselCount(count);
    }

    auto ExperimentTestOutputPort::UpdateVessels(const QStringList& vessels) const -> void {
        d->vessels = vessels;
    }

    auto ExperimentTestOutputPort::UpdateMedium(const AppEntity::Medium& medium) const -> void {
        d->medium = medium;
    }

    auto ExperimentTestOutputPort::UpdateDeleteProgress(int32_t totalCount, int32_t deletedCount) const -> void {
        Q_UNUSED(totalCount)
        Q_UNUSED(deletedCount)
    }

    auto ExperimentTestOutputPort::UpdateSampleTypeName(const AppEntity::SampleTypeName& sampleTypeName) const -> void {
        Q_UNUSED(sampleTypeName)
    }

    auto ExperimentTestOutputPort::AddWellGroup(const QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>>& groupIndices) const -> void {
        QMapIterator<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>> it(groupIndices);
        while (it.hasNext()) {
            it.next();
            d->wellGroups.insert(it.key(), it.value());
        }
    }

    auto ExperimentTestOutputPort::AddWellsToWellGroup(AppEntity::WellGroupIndex groupIndex, const QList<AppEntity::RowColumn>& indices) const -> void {
        auto wells = d->wellGroups[groupIndex];
        wells << indices;

        d->wellGroups.insert(groupIndex, wells);
    }

    auto ExperimentTestOutputPort::MoveWells(AppEntity::WellGroupIndex movingGroupIndex, AppEntity::WellGroupIndex targetGroupIndex, const QList<AppEntity::RowColumn>& wells) const -> void {
        Q_UNUSED(movingGroupIndex)
        Q_UNUSED(targetGroupIndex)
        Q_UNUSED(wells)
    }

    auto ExperimentTestOutputPort::RemoveWells(const QList<AppEntity::RowColumn>& indices) const -> void {
        Q_UNUSED(indices)
    }

    auto ExperimentTestOutputPort::DeleteWellGroups(const QList<AppEntity::WellGroupIndex>& indices) const -> void {
        Q_UNUSED(indices)
    }

    auto ExperimentTestOutputPort::ChangeWellGroupName(const AppEntity::WellGroupIndex groupIndex, const QString& groupName) const -> void {
        Q_UNUSED(groupIndex)
        Q_UNUSED(groupName)
    }

    auto ExperimentTestOutputPort::ChangeWellName(AppEntity::VesselIndex vesselIndex, AppEntity::WellIndex wellIndex, const QString& wellName) const -> void {
        Q_UNUSED(vesselIndex)
        Q_UNUSED(wellIndex)
        Q_UNUSED(wellName)
    }

    auto ExperimentTestOutputPort::ClearView() const -> void {
        
    }

    auto ExperimentTestOutputPort::ExperimentSaved(bool saved, const QString& path) const -> void {
        Q_UNUSED(saved)
        Q_UNUSED(path)
    }

    auto ExperimentTestOutputPort::Error(const QString& message) const -> void {
        Q_UNUSED(message)
    }

    auto ExperimentTestOutputPort::Experiment() const -> AppEntity::Experiment::Pointer {
        return d->experiment;
    }

    auto ExperimentTestOutputPort::ShortInfos() const -> QList<Entity::ExperimentShortInfo> {
        return d->shortInfos;
    }

    auto ExperimentTestOutputPort::RoiListUpdated() const -> void {
    }
}
