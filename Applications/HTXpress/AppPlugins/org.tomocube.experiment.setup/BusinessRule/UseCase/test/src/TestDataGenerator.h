#pragma once

#include <memory>

#include <QString>

#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase::Test {
    class TestDataGenerator {
    public:
        TestDataGenerator();
        ~TestDataGenerator();

        auto Root() const->QString;
        auto User() const->QString;
        auto Projects() const->QMap<QString, QString>;
        auto Experiments(const QString& projectTitle) const->QList<AppEntity::Experiment::Pointer>;
        auto VesselModelNames() const->QStringList;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
