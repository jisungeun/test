#include <catch2/catch.hpp>

#include <QFile>

#include <ChangeProjectDescription.h>
#include <CreateProject.h>
#include <ScanProjects.h>
#include <SelectProject.h>
#include <ExperimentReaderPort.h>
#include <ProjectReaderPlugin.h>
#include <ProjectWriterPlugin.h>
#include <SessionManager.h>
#include <System.h>
#include <UserManager.h>

#include "TestDataGenerator.h"
#include "ProjectTestOutputPort.h"
#include "ExperimentTestOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase::Test {
    SCENARIO("All projects in user directory can be read and wrote") {
        GIVEN("Test environments") {
            TestDataGenerator testData;

            // test용 data path 설정
            auto system = HTXpress::AppEntity::System::GetInstance();
            auto systemConfig = std::make_shared<HTXpress::AppEntity::SystemConfig>();
            systemConfig->SetDataDir(testData.Root());
            system->SetSystemConfig(systemConfig);

            UNSCOPED_INFO(QString("Data path: " + system->GetSystemConfig()->GetDataDir()).toStdString());

            // test user 생성
            auto testUser = std::make_shared<HTXpress::AppEntity::User>();
            testUser->SetUserID(testData.User());
            testUser->SetName(testData.User());

            auto userManager = HTXpress::AppEntity::UserManager::GetInstance();
            userManager->AddUser(testUser);

            // test용 user 로그인
            auto sessionManager = HTXpress::AppEntity::SessionManager::GetInstance();
            sessionManager->Login(testUser);

            UNSCOPED_INFO(QString("Login user: " + sessionManager->GetID()).toStdString());

            ProjectTestOutputPort projectOutputPort;
            ExperimentTestOutputPort experimentOutputPort;
            Plugins::ProjectIO::ProjectReader reader;
            Plugins::ProjectIO::ProjectWriter writer;
            Plugins::ExperimentIO::ExperimentReaderPort experimentReader;

            WHEN("All projects is read") {
                auto usecase = ScanProjects(&projectOutputPort, &reader);

                THEN("Project repository entity is filled") {
                    REQUIRE(usecase.Request());

                    auto projectRepo = HTXpress::AppEntity::ProjectRepo::GetInstance();
                    REQUIRE_FALSE(projectRepo->GetProjectTitles().isEmpty());

                    REQUIRE(projectOutputPort.Projects() == testData.Projects().keys());
                }
            }

            WHEN("New project is added") {
                THEN("New project is created file and added to project repository") {
                    const QString newProject("project_new");

                    auto usecase = CreateProject(&projectOutputPort, &writer);
                    usecase.SetNewProjectName(newProject);
                    REQUIRE(usecase.Request());

                    auto newProjectPath = testData.Root() + "/" + testData.User() + "/" + newProject + "/" + newProject + "." + AppEntity::ProjectExtension;
                    REQUIRE(QFile::exists(newProjectPath));

                    // project repository에 새로운 프로젝트 추가 확인
                    auto projectRepo = HTXpress::AppEntity::ProjectRepo::GetInstance();
                    REQUIRE(projectRepo->GetProject(newProject));

                    // output port로 전달되는 값 확인
                    REQUIRE(projectOutputPort.Projects().contains(newProject));
                }
            }

            WHEN("Project description is changed") {
                THEN("Changes is applied to file and project repository") {
                    auto projectRepo = HTXpress::AppEntity::ProjectRepo::GetInstance();
                    auto targetTitle = projectRepo->GetProjectTitles()[0];

                    QString newDescription = "This is new description.";

                    auto usecase = ChangeProjectDescription(&projectOutputPort, &writer);
                    usecase.SetDescription(targetTitle, newDescription);
                    REQUIRE(usecase.Request());

                    auto projectPath = testData.Root() + "/" + testData.User() + "/" + targetTitle + "/" + targetTitle + "." + AppEntity::ProjectExtension;

                    // project 파일에 변경사항 적용된지 확인
                    auto readProject = std::make_shared<HTXpress::AppEntity::Project>();
                    REQUIRE(reader.Read(projectPath, readProject));
                    REQUIRE(newDescription == readProject->GetDescription());

                    // project repository에 변경사항 적용 확인
                    REQUIRE(newDescription == projectRepo->GetProject(targetTitle)->GetDescription());

                    // output port로 전달되는 변경사항 확인
                    REQUIRE(newDescription == projectOutputPort.Project()->GetDescription());
                }
            }

            WHEN("Project selection is changed") {
                THEN("Experiments in seleted project is loaded") {
                    auto projectRepo = HTXpress::AppEntity::ProjectRepo::GetInstance();
                    auto selectedTitle = projectRepo->GetProjectTitles()[1];

                    auto usecase = SelectProject(&projectOutputPort, &experimentOutputPort, &experimentReader);
                    usecase.SetProject(selectedTitle);
                    REQUIRE(usecase.Request());

                    // output port로 전달되는 변경사항 확인
                    REQUIRE(selectedTitle == projectOutputPort.Project()->GetTitle());
                }
            }
        }
    }


}