#include "TestDataGenerator.h"

#include <QDir>
#include <QMap>
#include <QString>

#include <Project.h>
#include <ProjectWriterPlugin.h>
#include <ExperimentReaderPort.h>
#include <ExperimentWriterPort.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase::Test {
    struct TestDataGenerator::Impl {
        const QString user{ "test-user" };
        const QString testRoot{ QDir::tempPath() + "/" + "HTX_Test"};
        const QStringList testVesselModels{ "test-vessel-model1", "test-vessel-model2"};
        const int projectCount{ 3 };
        const int experimentCount{ 3 };

        QMap<QString, QString> projects;    // key: project title, value: description
        QMap<QString, QList<AppEntity::Experiment::Pointer>> experimentsByProjects;

        auto Generate()->bool;
        auto Clear()->void;
    };

    auto TestDataGenerator::Impl::Generate() -> bool {
        Plugins::ExperimentIO::ExperimentReaderPort experimentReader;
        Plugins::ExperimentIO::ExperimentWriterPort experimentWriter;

        // temp 경로에 테스트용 user와 project 파일들 생성
        QDir dir;
        if (!dir.mkpath(testRoot + "/" + user)) {
            return false;
        }

        dir.setPath(testRoot + "/" + user);

        auto testExperiment = std::make_shared<AppEntity::Experiment>();
        experimentReader.Read(QString(_TEST_DATA) + "/test-experiment.tcxexp", testExperiment);

        Plugins::ProjectIO::ProjectWriter writer;
        for (auto projectIndex = 0; projectIndex < projectCount; ++projectIndex) {
            QString projectTitle = QString("project_%1").arg(projectIndex + 1);
            QString projectDescription = QString("This is %1").arg(projectTitle);

            dir.mkdir(projectTitle);

            QString projectDir = dir.path() + "/" + projectTitle + "/" + projectTitle + "." + AppEntity::ProjectExtension;

            auto project = std::make_shared<AppEntity::Project>();
            project->SetTitle(projectTitle);
            project->SetDescription(projectDescription);

            writer.Write(projectDir, project);

            projects.insert(projectTitle, projectDescription);

            QList<AppEntity::Experiment::Pointer> experiments;
            for (auto experimentIndex = 0; experimentIndex < experimentCount; ++experimentIndex) {
                auto experimentTitle = QString("experiment_%1-%2").arg(projectIndex + 1).arg(experimentIndex + 1);
                auto experimentPath = dir.path() + "/" + projectTitle + "/" + experimentTitle;

                if (!dir.mkpath(experimentPath)) {
                    continue;
                }

                auto experiment = std::make_shared<AppEntity::Experiment>(*testExperiment);
                experiment->SetID(AppEntity::Experiment::GenerateID(user, experimentTitle));
                experiment->SetTitle(experimentTitle);
                experiment->SetUserID(user);

                experimentWriter.Write(
                    experimentPath + "/" + experimentTitle + "." + AppEntity::ExperimentExtension,
                    experiment
                );

                experiments << experiment;
            }

            experimentsByProjects.insert(projectTitle, experiments);
        }

        return true;
    }

    auto TestDataGenerator::Impl::Clear() -> void {
        QDir dir(testRoot);
        dir.removeRecursively();
    }

    TestDataGenerator::TestDataGenerator() : d{ new Impl } {
        d->Generate();
    }

    TestDataGenerator::~TestDataGenerator() {
        d->Clear();
    }

    auto TestDataGenerator::Root() const -> QString {
        return d->testRoot;
    }

    auto TestDataGenerator::User() const -> QString {
        return d->user;
    }

    auto TestDataGenerator::Projects() const -> QMap<QString, QString> {
        return d->projects;
    }

    auto TestDataGenerator::Experiments(const QString& projectTitle) const -> QList<AppEntity::Experiment::Pointer> {
        return d->experimentsByProjects.value(projectTitle);
    }

    auto TestDataGenerator::VesselModelNames() const->QStringList {
        return d->testVesselModels;
    }
}