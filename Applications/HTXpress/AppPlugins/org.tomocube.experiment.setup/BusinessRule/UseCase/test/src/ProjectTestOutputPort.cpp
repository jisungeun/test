#include "ProjectTestOutputPort.h"

#include <QList>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase::Test {
    struct ProjectTestOutputPort::Impl {
        QList<QString> projects;
        AppEntity::Project::Pointer project{ nullptr };
    };

    ProjectTestOutputPort::ProjectTestOutputPort() : d{ new Impl } {
        
    }

    ProjectTestOutputPort::~ProjectTestOutputPort() {
        
    }

    auto ProjectTestOutputPort::UpdateList(const QList<QString>& list) const -> void {
        d->projects = list;
    }

    auto ProjectTestOutputPort::ChangeProjectSelection(const QString& title) const -> void {
        Q_UNUSED(title)
    }

    auto ProjectTestOutputPort::UpdateInfo(const AppEntity::Project::Pointer& project) const -> void {
        d->project = project;
    }

    auto ProjectTestOutputPort::ProjectDeleted(bool isProjectEmpty) const -> void {
        Q_UNUSED(isProjectEmpty)
    }

    auto ProjectTestOutputPort::Projects() const -> QList<QString> {
        return d->projects;
    }

    auto ProjectTestOutputPort::Project() const -> AppEntity::Project::Pointer {
        return d->project;
    }
}