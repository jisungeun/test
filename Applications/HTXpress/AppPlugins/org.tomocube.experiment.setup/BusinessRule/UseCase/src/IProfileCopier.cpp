#include <QDir>

#include <AppEntityDefines.h>
#include <System.h>
#include "IProfileCopier.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    auto IProfileCopier::AreProfilesExist(const QString& sampleTypeName, const QString& targetFolderPath) -> bool {
        const auto filters = GetFilters(sampleTypeName);

        const auto model = AppEntity::System::GetSystemConfig()->GetModel();
        const auto sampleTypeFolderPath = AppEntity::System::GetSampleTypeFolderPath(model);

        const QDir targetDir(targetFolderPath);
        const QDir sysSampleTypeDir(sampleTypeFolderPath);

        const auto existingProfileFiles = targetDir.entryList(filters, QDir::Files);
        const auto sysProfileFiles = sysSampleTypeDir.entryList(filters, QDir::Files);

        if (existingProfileFiles.size() == sysProfileFiles.size()) {
            return true;
        }

        return false;
    }

    auto IProfileCopier::GetFilters(const QString& sampleTypeName) -> QStringList {
        return QStringList() << QString("%1.*.%3").arg(sampleTypeName).arg(AppEntity::ProfileExtension);
    }
}
