#include "LoadTemplates.h"

#include <QDir>
#include <QDirIterator>
#include <QStandardPaths>

#include <ExperimentTemplateRepo.h>
#include <SessionManager.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
	struct LoadTemplates::Impl {
		const IExperimentReaderPort* reader{ nullptr };
	};

	LoadTemplates::LoadTemplates(const IExperimentReaderPort* reader) : IUseCase("LoadTemplates"), d{ new Impl } {
		d->reader = reader;
    }

	LoadTemplates::~LoadTemplates() {
        
    }

	auto LoadTemplates::Perform() -> bool {
		if (d->reader == nullptr) {
			Error("Experiment reader is unknown.");
			return false;
		}

		auto templateRepo = Entity::ExperimentTemplateRepo::GetInstance();
		templateRepo->Clear();

		auto SetTemplates = [reader = d->reader, repo = templateRepo](const QString& root, const Entity::TemplateType type) {
		    QDirIterator it(root, {"*.tcxexp"}, QDir::Files);
		    while (it.hasNext()) {
			    QFileInfo fi(it.next());

			    auto experimentTemplate = std::make_shared<AppEntity::Experiment>();
                if (!reader->Read(fi.absoluteFilePath(), experimentTemplate)) {
                    continue;
                }

			    repo->SetTemplate(fi.completeBaseName(), experimentTemplate, type);
		    }
		};

		auto templateRoot = QDir(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation) + "/experiments");

	    // system templates
		SetTemplates(templateRoot.absolutePath() + "/template", Entity::TemplateType::System);

		// user templates
		auto userId = AppEntity::SessionManager::GetInstance()->GetID();
		SetTemplates(templateRoot.absolutePath() + "/user/" + userId, Entity::TemplateType::User);

		return true;
	}
}