﻿#include <AppData.h>
#include <SampleTypeRepo.h>
#include <System.h>

#include "ChangeExperimentSampleType.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct ChangeExperimentSampleType::Impl {
        const IExperimentOutputPort* output{};
        QString sampleTypeName{};
    };

    ChangeExperimentSampleType::ChangeExperimentSampleType(const IExperimentOutputPort* output) : IUseCase("ChangeExperimentSampleType"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    ChangeExperimentSampleType::~ChangeExperimentSampleType() {
    }

    auto ChangeExperimentSampleType::SetSampleTypeName(const QString& sampleTypeName) -> void {
        d->sampleTypeName = sampleTypeName;
    }
    
    auto ChangeExperimentSampleType::Perform() -> bool {
        if(d->sampleTypeName.isEmpty()) {
            Error(tr("Sample type name is empty."));
            return false;
        }

        if(!d->output) {
            Error(tr("There is no output port."));
            return false;
        }

        const auto exp = Entity::AppData::GetInstance()->GetExperiment();

        if(!exp) {
            Error(tr("It fails to get experiment."));
            return false;
        }

        const auto repo = AppEntity::SampleTypeRepo::GetInstance();
        const auto model = AppEntity::System::GetSystemConfig()->GetModel();
        if(!repo->GetSampleTypeNamesByModel(model)->contains(d->sampleTypeName)) {
            Error(tr("There is no sample type in %1").arg(model));
            return false;
        }

        exp->SetSampleTypeName(d->sampleTypeName);
        d->output->UpdateSampleTypeName(d->sampleTypeName);

        return true;
    }
}
