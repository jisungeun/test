#include "IProjectWriterPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    IProjectWriterPort::IProjectWriterPort() = default;
    IProjectWriterPort::~IProjectWriterPort() = default;
}