#include "DuplicateExperiment.h"

#include <QDate>

#include <AppData.h>
#include <Experiment.h>
#include <SessionManager.h>
#include <System.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct DuplicateExperiment::Impl {
        const IExperimentOutputPort* output{ nullptr };
        const IExperimentReaderPort* reader{ nullptr };
        const IExperimentWriterPort* writer{ nullptr };

        QString sourceExperimentName;
        QString destExperimentName;
    };

    DuplicateExperiment::DuplicateExperiment(const IExperimentOutputPort* outputPort, const IExperimentReaderPort* reader, const IExperimentWriterPort* writer) : IUseCase("DuplicateExperiment"), d{ new Impl } {
        d->output = outputPort;
        d->reader = reader;
        d->writer = writer;
    }

    DuplicateExperiment::~DuplicateExperiment() {
        
    }

    auto DuplicateExperiment::SetExperiment(const QString& sourceExperimentName, const QString& destExperimentName) -> void {
        d->sourceExperimentName = sourceExperimentName;
        d->destExperimentName = destExperimentName;
    }

    auto DuplicateExperiment::Perform() -> bool {
        Print(QString("SourceExperiment=%1 DestExperiment=%2").arg(d->sourceExperimentName).arg(d->destExperimentName));

        if (d->sourceExperimentName.isEmpty()) {
            Error("Source experiment name is empty.");
            return false;
        }

        if (d->destExperimentName.isEmpty()) {
            Error("New experiment name is empty.");
            return false;
        }

        if (d->reader == nullptr) {
            Error("Experiment reader is unknown.");
            return false;
        }

        auto appData = Entity::AppData::GetInstance();
        auto projectTitle = appData->GetProjectTitle();
        if (projectTitle.isEmpty()) {
            Error("Current project name is empty.");
            return false;
        }

        // source experiment 데이터를 파일에서 읽어옴
        const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
        const auto dataPath = systemConfig->GetDataDir();
        auto user = AppEntity::SessionManager::GetInstance()->GetID();

        // Todo: project or experiment 경로 쉽게 획득하는 방안
        auto experimentFilePath = appData->GetExperimentFilePath(projectTitle, d->sourceExperimentName);
        if (experimentFilePath.isEmpty()) {
            Error("Source experiment path is empty.");
            return false;
        }

        auto experiment = std::make_shared<AppEntity::Experiment>();
        if (!d->reader->Read(experimentFilePath, experiment)) {
            Error("It fails to read source experiment.");
            return false;
        }

        // 읽어온 experiment 데이터를 복제하여 새로운 experiment 생성
        auto newExperiment = std::make_shared<AppEntity::Experiment>(*experiment);
        newExperiment->SetUserID(user);
        newExperiment->SetID(AppEntity::Experiment::GenerateID(user, d->destExperimentName));
        newExperiment->SetCreatedDate(QDate::currentDate().toString("yyyyMMdd"));
        newExperiment->SetTitle(d->destExperimentName);

        // experiment repository에 추가
        appData->SetExperiment(newExperiment);

        auto shortInfo = Entity::ExperimentShortInfo(*newExperiment);
        appData->SetExperimentListInfo(newExperiment->GetID(), shortInfo);

        auto path = appData->GetExperimentFilePath(projectTitle, d->destExperimentName);
        if (path.isEmpty()) {
            Error("New experiment path is empty.");
            return false;
        }

		if (!d->writer->Write(path, newExperiment)) {
            Error("It fails to save new experiment.");
			return false;
		}

        if (d->output) {
            d->output->UpdateList(appData->GetExperimentListInfos());
            d->output->UpdateExperiment(newExperiment);    
        }

        return true;
    }
}