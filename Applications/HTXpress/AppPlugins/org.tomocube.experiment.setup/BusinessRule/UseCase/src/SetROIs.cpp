﻿#include "SetROIs.h"

#include "AppData.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct SetROIs::Impl {
        explicit Impl(Self* self):self(self){}
        Self* self{};
        const IExperimentOutputPort* output{};
        AppEntity::VesselIndex vesselIndex{-1};
        ROIList roiList{};

        auto Error(const QString& message) const -> void;
    };

    auto SetROIs::Impl::Error(const QString& message) const -> void {
        self->Error(message);
        if(output) {
            output->Error(message);
        }
    }

    SetROIs::SetROIs(const IExperimentOutputPort* output) : IUseCase("SetROIs"), d{std::make_unique<Impl>(this)} {
        d->output = output;
    }

    SetROIs::~SetROIs() = default;

    auto SetROIs::SetVesselIndex(const AppEntity::VesselIndex& vesselIndex) -> void {
        d->vesselIndex = vesselIndex;
    }

    auto SetROIs::SetRoiList(const ROIList& roiList) -> void {
        d->roiList = roiList;
    }

    auto SetROIs::Perform() -> bool {
        const auto experiment = Entity::AppData::GetInstance()->GetExperiment();

        if(!d->output) {
            d->Error(tr("Output port is null."));
            return false;
        }

        if(d->vesselIndex == -1) {
            d->Error(tr("Invlaid vessel index."));
            return false;
        }

        experiment->SetROIs(d->vesselIndex, d->roiList);
        d->output->RoiListUpdated();

        return true;
    }
}
