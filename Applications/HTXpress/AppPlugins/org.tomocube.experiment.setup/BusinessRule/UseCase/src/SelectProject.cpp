#include "SelectProject.h"

#include <QDirIterator>

#include <AppData.h>
#include <ProjectRepo.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct SelectProject::Impl {
        const IProjectOutputPort* projectOutput{ nullptr };
        const IExperimentOutputPort* experimentOutput{ nullptr };
        const IExperimentReaderPort* reader{ nullptr };

        QString projectName;
    };

    SelectProject::SelectProject(const IProjectOutputPort* projectOutputPort, const IExperimentOutputPort* experimentOutputPort, const IExperimentReaderPort* experimentReader) : IUseCase("SelectProject"), d{ new Impl } {
        d->projectOutput = projectOutputPort;
        d->experimentOutput = experimentOutputPort;
        d->reader = experimentReader;
    }

    SelectProject::~SelectProject() {
        
    }

    auto SelectProject::SetProject(const QString& title) -> void {
        d->projectName = title;
    }

    auto SelectProject::Perform() -> bool {
        Print(QString("ProjectName=%1").arg(d->projectName));

        if (d->projectName.isEmpty()) {
            Error("Project name is empty.");
            return false;
        }

        if (d->reader == nullptr) {
            Error("Project reader is unknown.");
            return false;
        }

        auto appData = Entity::AppData::GetInstance();
        appData->Clear();

        // Todo: ProjectRepo, Project를 ExperimentSetup::Entity로 변경
        auto project = AppEntity::ProjectRepo::GetInstance()->GetProject(d->projectName);
        if (project == nullptr) {
            Error(QString("It fails to get a project(%1).").arg(d->projectName));
            return false;
        }

        appData->SetProjectTitle(d->projectName);

        // project에 존재하는 모든 experiment의 일부 값만
        auto findFilter = "*." + AppEntity::ExperimentExtension;

        auto projectDir = QDir(appData->GetCurrentProjectPath());
        auto experimentDirs = projectDir.entryList(QDir::Filters(QDir::AllDirs | QDir::NoDotAndDotDot), QDir::SortFlags(QDir::Name | QDir::IgnoreCase));

        for (auto& experimentDir : experimentDirs) {
            QDirIterator experimentFileIt(projectDir.absolutePath() + "/" + experimentDir, {findFilter}, QDir::Files);
            while (experimentFileIt.hasNext()) {
                auto experiment = std::make_shared<AppEntity::Experiment>();
                if (d->reader->ReadShortInfo(experimentFileIt.next(), experiment)) {
                    appData->SetExperimentListInfo(experiment->GetID(), Entity::ExperimentShortInfo(*experiment));
                }
            }
        }
        
        if (d->projectOutput) d->projectOutput->ChangeProjectSelection(d->projectName);
        if (d->experimentOutput) d->experimentOutput->UpdateList(appData->GetExperimentListInfos()); // TODO: 삭제 - project output port를 통해 같이 처리

        return true;
    }
}
