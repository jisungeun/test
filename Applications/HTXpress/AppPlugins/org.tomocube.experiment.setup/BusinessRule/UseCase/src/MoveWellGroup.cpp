#include "MoveWellGroup.h"

#include <AppData.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct MoveWellGroup::Impl {
        int vesselIndex{ -1 };
        int sourceGroupIndex{ -1 };
        int destGroupIndex{ -1 };
    };

    MoveWellGroup::MoveWellGroup() : IUseCase("MoveWellGroup"), d{ new Impl } {
        
    }

    MoveWellGroup::~MoveWellGroup() {

    }

    auto MoveWellGroup::SetVesselIndex(int index) -> void {
        d->vesselIndex = index;
    }

    auto MoveWellGroup::SetGroupIndex(int sourceIndex, int destIndex) -> void {
        d->sourceGroupIndex = sourceIndex;
        d->destGroupIndex = destIndex;
    }

    auto MoveWellGroup::Perform() -> bool {
        Print(QString("VesselIndex=%1 SourceGroupIndex=%2 DestGroupIndex=%3").arg(d->vesselIndex).arg(d->sourceGroupIndex).arg(d->destGroupIndex));

        if (d->vesselIndex < 0 ) {
            Error("Vessel index is less than 0.");
            return false;
        }

        if (d->sourceGroupIndex < 0 ) {
            Error("Source group index is less than 0.");
            return false;
        }

        if (d->destGroupIndex < 0 ) {
            Error("Destination group index is less than 0.");
            return false;
        }

        auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if (experiment == nullptr) {
            Error("It fails to get current experiment.");
            return false;
        }

        auto movingGroup = experiment->GetWellGroup(d->vesselIndex, d->sourceGroupIndex);
        auto targetGroup = experiment->GetWellGroup(d->vesselIndex, d->destGroupIndex);

        for (auto well : movingGroup.GetWells()) {
            targetGroup.AddWell(well);
        }

        experiment->DeleteWellGroup(d->vesselIndex, d->sourceGroupIndex);

        return true;
    }
}