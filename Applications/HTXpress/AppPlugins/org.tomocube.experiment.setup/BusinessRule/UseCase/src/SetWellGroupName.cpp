#include "SetWellGroupName.h"

#include <AppData.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct SetWellGroupName::Impl {
	    const IExperimentOutputPort* output{ nullptr };

        int vesselIndex{ -1 };
        int groupIndex{ -1 };
        QString newWellGroupName;
	};

	SetWellGroupName::SetWellGroupName(const IExperimentOutputPort* outputPort) : IUseCase("SetWellGroupName"), d{ new Impl } {
        d->output = outputPort;
    }

    SetWellGroupName::~SetWellGroupName() {

    }

    auto SetWellGroupName::SetName(int vesselIndex, int groupIndex, const QString& name) -> void {
        d->vesselIndex = vesselIndex;
        d->groupIndex = groupIndex;
        d->newWellGroupName = name;
    }

    auto SetWellGroupName::Perform() -> bool {
        Print(QString("VesselIndex=%1 GroupIndex=%2 NewGroupName=%3").arg(d->vesselIndex).arg(d->groupIndex).arg(d->newWellGroupName));

        if (d->vesselIndex < 0) {
            Error("Vessel index is less than 0.");
            return false;
        }

        if (d->groupIndex < 0) {
            Error("Group index is less than 0.");
            return false;
        }

        if (d->newWellGroupName.isEmpty()) {
            Error("New well group name is empty.");
            return false;
        }

        auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if (experiment == nullptr) {
            Error("It fails to get current experiment.");
            return false;
        }

        experiment->SetWellGroupName(d->vesselIndex, d->groupIndex, d->newWellGroupName);

        if (d->output) d->output->ChangeWellGroupName(d->groupIndex, d->newWellGroupName);

        return true;
    }

}