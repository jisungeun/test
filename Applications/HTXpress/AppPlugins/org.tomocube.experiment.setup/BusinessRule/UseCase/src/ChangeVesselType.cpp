#include "ChangeVesselType.h"

#include <AppData.h>
#include <VesselRepo.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct ChangeVesselType::Impl {
        const IExperimentOutputPort* output{ nullptr };

        QString vessel;
    };

    ChangeVesselType::ChangeVesselType(const IExperimentOutputPort* output) : IUseCase("ChangeVesselType"), d{ new Impl } {
        d->output = output;
    }

    ChangeVesselType::~ChangeVesselType() {
        
    }
    
    auto ChangeVesselType::SetVesselType(const QString& vessel) -> void {
        d->vessel = vessel;
    }

    auto ChangeVesselType::Perform() -> bool {
        if (d->vessel.isEmpty()) {
            Error("Vessel type is empty.");
            return false;
        }

        const auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if (experiment == nullptr) {
            Error("It fails to get experiment.");
            return false;
        }

        auto vesselRepo = AppEntity::VesselRepo::GetInstance();
        auto vessel = vesselRepo->GetVesselByName(d->vessel);
        if(vessel == nullptr) {
            Error(QString("It fails to find vessel(%1)").arg(d->vessel));
            return false;
        }

        auto prevVessel = experiment->GetVessel();
        if (prevVessel) {
            if (prevVessel->GetName() != vessel->GetName()) {
                for (auto i = 0; i < experiment->GetVesselCount(); ++i) {
                    experiment->ClearLocation(i);
                    experiment->ClearWellGroups(i);
                }
            }
        }

        experiment->SetVesselInfo(vessel);

        if (d->output) d->output->UpdateVesselType(d->vessel);

        return true;
    }

}
