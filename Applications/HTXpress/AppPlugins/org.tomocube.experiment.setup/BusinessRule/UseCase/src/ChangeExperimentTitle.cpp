#include "ChangeExperimentTitle.h"

#include <filesystem>

#include <QDir>

#include <AppData.h>
#include <MediumDataRepo.h>
#include <ExperimentShortInfo.h>

namespace fs = std::filesystem;

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct ChangeExperimentTitle::Impl {
        const IExperimentWriterPort* writer{ nullptr };
        const IExperimentOutputPort* output{ nullptr };

        QString title;
    };

    ChangeExperimentTitle::ChangeExperimentTitle(const IExperimentOutputPort* output, const IExperimentWriterPort* writer) : IUseCase("ChangeExperimentTitle"), d{ new Impl } {
        d->writer = writer;
        d->output = output;
    }

    ChangeExperimentTitle::~ChangeExperimentTitle() {
        
    }

    auto ChangeExperimentTitle::SetTitle(const QString& title) -> void {
        d->title = title;

    }

    auto ChangeExperimentTitle::Perform() -> bool {
        if (d->title.isEmpty()) {
            Error("Experiment title is empty.");
            return false;
        }

        if (d->writer == nullptr) {
            Error("Experiment writer is unknown.");
            return false;
        }

        const auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if (experiment == nullptr) {
            return false;
        }
                
		const auto appData = Entity::AppData::GetInstance();
		const auto projectTitle = appData->GetProjectTitle();
        
        const auto prevExperimentPath = appData->GetCurrentExperimentPath();
        const auto newExperimentPath = appData->GetExperimentPath(projectTitle, d->title);

        try {
            // rename experiment folder
            fs::rename(
                prevExperimentPath.toStdWString(),
                newExperimentPath.toStdWString()
            );
            
            // rename experiment file
            fs::rename(
                QString(newExperimentPath + "/" + experiment->GetTitle() + "." + AppEntity::ExperimentExtension).toStdWString(),
                QString(newExperimentPath + "/" + d->title + "." + AppEntity::ExperimentExtension).toStdWString()
            );

            // edit experiment data
            experiment->SetTitle(d->title);
            if (!d->writer->Write(appData->GetExperimentFilePath(projectTitle, d->title), experiment)) {
	        	return false;
	        }
        } catch (fs::filesystem_error const& ex) {
            Error(QString("It fails to rename - %1").arg(ex.what()));
            return false;
        }

        // short info repo에 변경된 title의 experiment 등록
        appData->SetExperiment(experiment);
        appData->SetExperimentListInfo(experiment->GetID(), Entity::ExperimentShortInfo(*experiment));

        if (d->output) d->output->UpdateList(appData->GetExperimentListInfos());

        return true;
    }

}
