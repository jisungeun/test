﻿#include <System.h>
#include <AppData.h>

#include "CopyProfiles.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct CopyProfiles::Impl {
        IProfileCopier* copier{};
        QString profileName{};
    };

    CopyProfiles::CopyProfiles(IProfileCopier* copier) : IUseCase("CopyProfiles"), d{std::make_unique<Impl>()} {
        d->copier = copier;
    }

    CopyProfiles::~CopyProfiles() = default;

    auto CopyProfiles::SetProfileName(const QString& profileName) -> void {
        d->profileName = profileName;
    }

    auto CopyProfiles::Perform() -> bool {
        if (!d->copier) {
            Error(tr("Failed to run copy process: IProfileCopier is nullptr."));
            return false;
        }

        const auto modelName = AppEntity::System::GetInstance()->GetSystemConfig()->GetModel();
        const auto sampleTypeFolderPath = AppEntity::System::GetInstance()->GetSampleTypeFolderPath(modelName);
        const auto experimentFolderPath = Entity::AppData::GetInstance()->GetCurrentExperimentPath();

        const auto exists = IProfileCopier::AreProfilesExist(d->profileName, experimentFolderPath);
        if(exists) {
            Print(tr("Copying did not proceed because the profile '%1' with the same file name already exists in %2").arg(d->profileName).arg(experimentFolderPath));
            return true;
        }

        d->copier->SetProfileName(d->profileName);
        d->copier->SetSource(sampleTypeFolderPath);
        d->copier->AddDestination(experimentFolderPath);
        d->copier->SetFileNameFilters(IProfileCopier::GetFilters(d->profileName));

        return d->copier->Copy();
    }
}
