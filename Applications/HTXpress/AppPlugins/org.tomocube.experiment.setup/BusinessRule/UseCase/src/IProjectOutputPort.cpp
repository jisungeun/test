#include "IProjectOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    IProjectOutputPort::IProjectOutputPort() = default;
    IProjectOutputPort::~IProjectOutputPort() = default;
}