#include "CheckDeletableExperiment.h"

#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <psapi.h>

#include <QDir>

#include <AppData.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
	struct CheckDeletableExperiment::Impl {
		QString experiment;

        auto FindProcessor(const QString& name)->bool;
	};

    auto CheckDeletableExperiment::Impl::FindProcessor(const QString& name) -> bool {
		auto GetProcessorName = [](DWORD processId) -> QString {
		    TCHAR processName[MAX_PATH] = TEXT("<unknown>");

            const HANDLE processHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, processId);
            if (processHandle == nullptr) return QString();

            HMODULE moduleHandle;
            DWORD moduleHandlesBytes;

            if (!EnumProcessModulesEx(processHandle, &moduleHandle, sizeof(moduleHandle), &moduleHandlesBytes, LIST_MODULES_ALL)) return QString();

            GetModuleBaseName(processHandle, moduleHandle, processName, sizeof(processName)/sizeof(TCHAR));

            CloseHandle(processHandle);

            return QString(processName);
		};

        DWORD processes[1024], processesHandlesBytes;
		if (!EnumProcesses(processes, sizeof(processes), &processesHandlesBytes)) return false;

        const auto processCount = processesHandlesBytes / sizeof(DWORD);
        for (unsigned int index = 0; index < processCount; ++index) {
            if(processes[index] == 0) continue;

            if (name == GetProcessorName(processes[index])) return true;
		}

		return false;
    }

	CheckDeletableExperiment::CheckDeletableExperiment() : IUseCase("CheckDeletableExperiment"), d{ new Impl } {
		
	}

	CheckDeletableExperiment::~CheckDeletableExperiment() {
	
	}

	auto CheckDeletableExperiment::SetExperiment(const QString& title) -> void {
		d->experiment = title;
    }

	auto CheckDeletableExperiment::Perform() -> bool {
        if (d->experiment.isEmpty()) {
			Error("Experiment name is empty.");
            return false;
		}

		auto appData = Entity::AppData::GetInstance();
		
		auto experimentPath = appData->GetExperimentPath(appData->GetProjectTitle(), d->experiment);
		if (experimentPath.isEmpty()) return false;

		auto dir = QDir(experimentPath);
		if (dir.isEmpty(QDir::AllDirs | QDir::NoDotAndDotDot)) return true;	// 실험 데이터 폴더가 존재하지 않으면 프로세싱이 진행 중이지 않으므로, 실험 삭제 가능한 상태로 확인.

		// 프로세싱 서버 실행 중인지 확인
		if (d->FindProcessor("HTXProcessingServer.exe")) return false;

        return true;
    }

}