#include "IUseCaseLogger.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    static IUseCaseLogger* theInstance{ nullptr };

    IUseCaseLogger::IUseCaseLogger() {
        theInstance = this;
    }

    IUseCaseLogger::~IUseCaseLogger() {
    }

    auto IUseCaseLogger::PrintLog(const QString& useCase, const QString& message) -> void {
        if (theInstance == nullptr) return;
        theInstance->Log(useCase, message);
    }

    auto IUseCaseLogger::PrintError(const QString& useCase, const QString& message) -> void {
        if (theInstance == nullptr) return;
        theInstance->Error(useCase, message);
    }
}
