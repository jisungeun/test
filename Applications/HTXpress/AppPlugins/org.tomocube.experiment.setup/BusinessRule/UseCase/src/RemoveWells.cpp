#include "RemoveWells.h"

#include <AppData.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
	struct RemoveWells::Impl {
	    const IExperimentOutputPort* output{ nullptr };

        AppEntity::VesselIndex vesselIndex{ -1 };
        QList<QPair<int, int>> wells;
	};

	RemoveWells::RemoveWells(const IExperimentOutputPort* outputPort) : IUseCase("RemoveWells"), d{ new Impl } {
        d->output = outputPort;
    }

	RemoveWells::~RemoveWells() {
        
    }

    auto RemoveWells::SetVesselIndex(AppEntity::VesselIndex index) -> void {
        d->vesselIndex = index;
    }

    auto RemoveWells::AddRemovalWells(const QList<QPair<int, int>>& wells) -> void {
        d->wells = wells;
    }

    auto RemoveWells::Perform() -> bool {
        Print(QString("VesselIndex=%1 RemovalWellsCount=%2").arg(d->vesselIndex).arg(d->wells.count()));

        if (d->vesselIndex < 0) {
            Error("Vessel index is less than 0.");
            return false;
        }

        if (d->wells.isEmpty()) {
            Error("Removal well list is empty.");
            return false;
        }

	    auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if (experiment == nullptr) {
            Error("It fails to get current experiment.");
            return false;
        }

        // well group에서 wells 삭제
        experiment->RemoveWells(d->vesselIndex, d->wells);

        if (d->output) d->output->RemoveWells(d->wells);

        // 삭제한 well이 속한 그룹의 크기가 0이면 그룹 삭제
        auto wellGroupIndices = experiment->GetWellGroupIndices(d->vesselIndex);
        QList<AppEntity::WellGroupIndex> emptyGroupIndices;
        std::for_each(wellGroupIndices.begin(), wellGroupIndices.end(), [&](AppEntity::WellGroupIndex groupIndex) {
            auto wellGroup = experiment->GetWellGroup(d->vesselIndex, groupIndex);
            if (wellGroup.GetWells().isEmpty()) {
                emptyGroupIndices << groupIndex;
            }
        });

        if (!emptyGroupIndices.isEmpty()) {
            for (auto groupIndex : emptyGroupIndices) {
                experiment->DeleteWellGroup(d->vesselIndex, groupIndex);
            }

            if (d->output) d->output->DeleteWellGroups(emptyGroupIndices);
        }

        return true;
    }
}