#include "DeleteExperiment.h"

#include <algorithm>
#include <execution>

#include <QDir>

#include <AppData.h>
#include <SessionManager.h>
#include <System.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct DeleteExperiment::Impl {
        const IExperimentOutputPort* output{ nullptr };
        IDirectoryDeleter* directoryDeleter{ nullptr };
        QString experiment;
    };

    DeleteExperiment::DeleteExperiment(const IExperimentOutputPort* output, IDirectoryDeleter* directoryDeleter) : IUseCase("DeleteExperiment"), d{ new Impl } {
        d->output = output;
        d->directoryDeleter = directoryDeleter;
    }

    DeleteExperiment::~DeleteExperiment() {
        
    }

    auto DeleteExperiment::SetExperiment(const QString& experimentTitle) -> void {
        d->experiment = experimentTitle;
    }

    auto DeleteExperiment::Perform() -> bool {
        if (d->experiment.isEmpty()) {
            Error("Experiment name is empty.");
            return false;
        }

        const auto appData = Entity::AppData::GetInstance();
        auto projectTitle = appData->GetProjectTitle();
        if (projectTitle.isEmpty()) {
            Error("Current project name is empty");
            return false;
        }

        const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
        const auto dataPath = systemConfig->GetDataDir();

        auto user = AppEntity::SessionManager::GetInstance()->GetID();

        AppEntity::ExperimentID id;
        for (auto info : appData->GetExperimentListInfos()) {
            if (info.GetTitle() == d->experiment) {
                id = info.GetID();
                break;
            }
        }

        if (id.isEmpty()) {
            Error("It fails to find experiment in experiment list.");
            return false;
        }

        const auto path = dataPath + "/" + user + "/" + projectTitle + "/" + d->experiment;
        // TODO Delete Interface here
        if(!d->directoryDeleter) {
            Error("Directory Deleter is nullptr");
            return false;
        }

        d->directoryDeleter->SetRootPath(path);
        const auto deleteResult = d->directoryDeleter->Delete();
        if(deleteResult) {
            appData->RemoveExperimentListInfo(id);
            if (d->output) d->output->UpdateList(appData->GetExperimentListInfos());

            auto experiment = appData->GetExperiment();
            if (experiment && experiment->GetTitle() == d->experiment) {
                // 현재 로드된 Experiment를 삭제하는 경우에는 표시 중인 Experiment 정보 삭제
                if (d->output) d->output->ClearView();
            }
        }

        return deleteResult;
    }
}