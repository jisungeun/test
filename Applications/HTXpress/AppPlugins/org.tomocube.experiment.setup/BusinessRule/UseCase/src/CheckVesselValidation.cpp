﻿#include <AppData.h>
#include <System.h>

#include "CheckVesselValidation.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct CheckVesselValidation::Impl {
        CheckVesselValidation* self{ nullptr };
        const IExperimentOutputPort* output{ nullptr };

        explicit Impl(CheckVesselValidation* self) : self(self){}

        auto Error(const QString& message) const -> void;
    };

    auto CheckVesselValidation::Impl::Error(const QString& message) const -> void {
        self->Error(message);
        output->Error(message);
    }

    CheckVesselValidation::CheckVesselValidation(const IExperimentOutputPort* output) : IUseCase("CheckVesselValidation"), d{std::make_unique<Impl>(this)} {
        d->output = output;
    }

    CheckVesselValidation::~CheckVesselValidation() {
    }

    auto CheckVesselValidation::Perform() -> bool {
        const auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto appData = Entity::AppData::GetInstance();
        const auto experiment = appData->GetExperiment();

        if(experiment == nullptr) {
            d->Error(QObject::tr("Experiment is empty."));
            return false;
        }

        bool hasSpecimens = false;
        for (auto vesselIndex = 0; vesselIndex < experiment->GetVesselCount(); ++vesselIndex) {
            if (experiment->GetWellGroupCount(vesselIndex) > 0) {
                hasSpecimens = true;
                break;
            }
        }

        if(!hasSpecimens) {
            d->Error(QObject::tr("It fails to run this experiment.\n"
                                 "Please add specimens."));
            return false;
        }

        const auto na = experiment->GetVessel()->GetNA();

        if(!sysConfig->GetHTScanParameterNAs().contains(na)) {
            d->Error(QObject::tr("No valid HT scan parameters for NA %1.").arg(na));
            return false;
        }

        return true;
    }
}
