﻿#include "IUserManager.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    static IUserManager* theInstance{ nullptr };

    IUserManager::IUserManager() {
        theInstance = this;
    }

    IUserManager::~IUserManager() {
    }

    auto IUserManager::GetInstance() -> IUserManager* {
        return theInstance;
    }
}