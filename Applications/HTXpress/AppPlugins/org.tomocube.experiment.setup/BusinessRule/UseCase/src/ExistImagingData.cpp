#include "ExistImagingData.h"

#include <QDir>
#include <QDirIterator>
#include <QDebug>

#include <AppData.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct ExistImagingData::Impl {
        QString experiment;
    };

    ExistImagingData::ExistImagingData() : IUseCase("ExistImagingData"), d{ new Impl } {
    }

    ExistImagingData::~ExistImagingData() {
        
    }

    auto ExistImagingData::SetExperiment(const QString& experiment) -> void {
        d->experiment = experiment;
    }

    auto ExistImagingData::Perform() -> bool {
        auto appData = Entity::AppData::GetInstance();
        auto path = appData->GetExperimentPath(appData->GetProjectTitle(), d->experiment);

        auto dir= QDir(path);
        auto dataDirs = dir.entryList(QDir::Filters(QDir::AllDirs | QDir::NoDotAndDotDot), QDir::SortFlags(QDir::Name | QDir::IgnoreCase));

        // 실험 폴더 하위에 실험 데이터(*.tcf)가 있는지 확인
        for (auto& dataDir : dataDirs) {
            QDirIterator it(path + "/" + dataDir, {"*.tcf"}, QDir::Files, QDirIterator::Subdirectories);
            if (it.hasNext()) {
                return true;
            }
        }

        return false;
    }

}
