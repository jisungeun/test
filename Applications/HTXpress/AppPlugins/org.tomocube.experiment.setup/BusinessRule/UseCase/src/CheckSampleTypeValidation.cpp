﻿#include <AppData.h>
#include <System.h>
#include <SampleTypeRepo.h>

#include "CheckSampleTypeValidation.h"
#include "IImagingProfileLoader.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct CheckSampleTypeValidation::Impl {
        const IExperimentOutputPort* output{};
    };

    CheckSampleTypeValidation::CheckSampleTypeValidation(const IExperimentOutputPort* output) : IUseCase("CheckSampleTypeValidation"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    CheckSampleTypeValidation::~CheckSampleTypeValidation() = default;

    auto CheckSampleTypeValidation::Perform() -> bool {
        const auto model = AppEntity::System::GetSystemConfig()->GetModel();
        const auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        const auto sampleTypeName = experiment->GetSampleTypeName();
        const auto sampleTypeRepo = AppEntity::SampleTypeRepo::GetInstance();
        const auto sampleTypeNameList = sampleTypeRepo->GetSampleTypeNamesByModel(model);

        if(!sampleTypeNameList) {
            const auto errorMessage = tr("There are no sample types available for the current model (%1), "
                                         "or there are no sample types in the system.\n"
                                         "Please contact to your system administrator for assistance.").arg(model);
            Error(errorMessage);
            d->output->Error(errorMessage);
            return false;
        }

        const auto isExist = sampleTypeNameList.value().contains(sampleTypeName);

        if (!isExist) {
            QString errorMessage;
            if (sampleTypeName.isEmpty()) {
                errorMessage = tr("The experiment cannot be run because the sample type has not been selected.\n" 
                                  "Please select one from the sample type list.");
            }
            else {
                errorMessage = tr("The experiment cannot be run because the sample type list does not contain a sample type with the name '%1'.\n" 
                                  "Please select different one from the sample type list.").arg(sampleTypeName);
            }
            Error(errorMessage);
            d->output->Error(errorMessage);
        }

        const auto isValid = IImagingProfileLoader::GetInstance()->GetHtScanParameter().has_value();

        if (!isValid) {
            const auto errorMessage = tr("The sampleType in this experiment is not valid or properly configured.\n"
                "Please select a different sample type from the sample type list.");
            Error(errorMessage);
            d->output->Error(errorMessage);
        }

        return isValid;
    }
}
