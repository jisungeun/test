﻿#include "IImagingProfileLoader.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    static IImagingProfileLoader* theInstance{};

    auto IImagingProfileLoader::GetInstance() -> IImagingProfileLoader* {
        return theInstance;
    }

    IImagingProfileLoader::IImagingProfileLoader() {
        theInstance = this;
    }
}
