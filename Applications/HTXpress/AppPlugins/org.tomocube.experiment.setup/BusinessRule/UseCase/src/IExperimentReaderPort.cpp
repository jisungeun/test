#include "IExperimentReaderPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    IExperimentReaderPort::IExperimentReaderPort() = default;
    IExperimentReaderPort::~IExperimentReaderPort() = default;
}
