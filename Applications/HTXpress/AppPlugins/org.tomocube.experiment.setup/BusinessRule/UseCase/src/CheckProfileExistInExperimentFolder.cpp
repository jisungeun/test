﻿#include <QDir>

#include <AppData.h>
#include <AppEntityDefines.h>
#include "IProfileCopier.h"

#include "CheckProfileExistInExperimentFolder.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct CheckProfileExistInExperimentFolder::Impl {
        const IExperimentOutputPort* output{};
    };

    CheckProfileExistInExperimentFolder::CheckProfileExistInExperimentFolder(const IExperimentOutputPort* output) : IUseCase("CheckProfileExistInExperimentFolder"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    CheckProfileExistInExperimentFolder::~CheckProfileExistInExperimentFolder() {
    }

    auto CheckProfileExistInExperimentFolder::Perform() -> bool {
        const auto expPath = Entity::AppData::GetInstance()->GetCurrentExperimentPath();
        const auto sampleTypeName = Entity::AppData::GetInstance()->GetExperiment()->GetSampleTypeName();
        
        if (!IProfileCopier::AreProfilesExist(sampleTypeName, expPath)) {
            const auto errorMessage = tr("Sample type '%1' is not exist in the folder.").arg(sampleTypeName);
            Error(errorMessage);
            return false;
        }

        return true;
    }
}
