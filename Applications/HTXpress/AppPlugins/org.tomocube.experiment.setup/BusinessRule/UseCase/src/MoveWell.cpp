#include "MoveWell.h"

#include <AppData.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct MoveWell::Impl {
        const IExperimentOutputPort* output{ nullptr };

        int vesselIndex{ -1 };
        int sourceGroupIndex{ -1 };
        int destGroupIndex{ -1 };
        QList<QPair<int, int>> wells;
    };

    MoveWell::MoveWell(const IExperimentOutputPort* outputPort) : IUseCase("MoveWell"), d{ new Impl } {
        d->output = outputPort;
    }

    MoveWell::~MoveWell() {

    }

    auto MoveWell::SetVesselIndex(int index) -> void {
        d->vesselIndex = index;
    }

    auto MoveWell::SetWellGroupIndex(int sourceIndex, int destIndex) -> void {
        d->sourceGroupIndex = sourceIndex;
        d->destGroupIndex = destIndex;
    }

    auto MoveWell::AddWells(const QList<QPair<int, int>>& wells) -> void {
        d->wells << wells;
    }

    auto MoveWell::Perform() -> bool {
        Print(QString("VesselIndex=%1 SourceGroupIndex=%2 DestGroupIndex=%3 WellCount=%4")
            .arg(d->vesselIndex).arg(d->sourceGroupIndex).arg(d->destGroupIndex).arg(d->wells.count()));

        if (d->vesselIndex < 0 ) {
            Error("Vessel index is less than 0.");
            return false;
        }

        if (d->sourceGroupIndex < 0 ) {
            Error("Source group index is less than 0.");
            return false;
        }

        if (d->destGroupIndex < 0 ) {
            Error("Destination group index is less than 0.");
            return false;
        }

        if (d->wells.isEmpty()) {
            Error("Well list is empty.");
            return false;
        }

        const auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if (experiment == nullptr) {
            Error("It fails to get current experiment.");
            return false;
        }

        // 기존 그룹에서 삭제
        experiment->RemoveWells(d->vesselIndex, d->wells);
        if (experiment->GetWellGroup(d->vesselIndex, d->sourceGroupIndex).GetWells().isEmpty()) {
            experiment->DeleteWellGroup(d->vesselIndex, d->sourceGroupIndex);
        }

        // 새로운 그룹에 추가
        experiment->AddWellsToGroup(d->vesselIndex, d->destGroupIndex, d->wells);

        if (d->output) {
            d->output->MoveWells(d->sourceGroupIndex, d->destGroupIndex, d->wells);
        }

        return true;
    }
}