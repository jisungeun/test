#include "IAppUIOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    IAppUIOutputPort::IAppUIOutputPort() = default;
    IAppUIOutputPort::~IAppUIOutputPort() = default;
}