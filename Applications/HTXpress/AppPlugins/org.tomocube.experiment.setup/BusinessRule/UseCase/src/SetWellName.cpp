#include "SetWellName.h"

#include <AppData.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct SetWellName::Impl {
        const IExperimentOutputPort* output{nullptr};

        int vesselIndex{ -1 };
        int wellIndex{ -1 };
        QString newWellName;
    };

    SetWellName::SetWellName(const IExperimentOutputPort* outputPort) : IUseCase("SetWellName"), d{ new Impl } {
        d->output = outputPort;
    }

    SetWellName::~SetWellName() {

    }

    auto SetWellName::SetName(int vesselIndex, int wellIndex, const QString& name) -> void {
        d->vesselIndex = vesselIndex;
        d->wellIndex = wellIndex;
        d->newWellName = name;
    }

    auto SetWellName::Perform() -> bool {
        Print(QString("VesselIndex=%1 WellIndex=%2 NewWellName=%3").arg(d->vesselIndex).arg(d->wellIndex).arg(d->newWellName));

        if (d->vesselIndex < 0) {
            Error("Vessel index is less than 0.");
            return false;
        }

        if (d->wellIndex < 0) {
            Error("Group index is less than 0.");
            return false;
        }

        if (d->newWellName.isEmpty()) {
            Error("New well group name is empty.");
            return false;
        }

        auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if (experiment == nullptr) {
            Error("It fails to get current experiment.");
            return false;
        }

        experiment->SetWellName(d->vesselIndex, d->wellIndex, d->newWellName);

        if(d->output) d->output->ChangeWellName(d->vesselIndex, d->wellIndex, d->newWellName);

        return true;
    }

}