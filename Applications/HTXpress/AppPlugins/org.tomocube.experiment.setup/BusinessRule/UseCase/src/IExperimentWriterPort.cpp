#include "IExperimentWriterPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    IExperimentWriterPort::IExperimentWriterPort() = default;
    IExperimentWriterPort::~IExperimentWriterPort() = default;
}