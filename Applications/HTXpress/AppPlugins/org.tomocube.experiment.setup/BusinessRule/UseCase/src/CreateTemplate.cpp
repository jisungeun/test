#include "CreateTemplate.h"

#include <QStandardPaths>

#include <AppData.h>
#include <ExperimentTemplateRepo.h>
#include <SessionManager.h>
#include <System.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
	struct CreateTemplate::Impl {
		const IExperimentWriterPort* writer{ nullptr };

		QString templateName;
	};
	
    CreateTemplate::CreateTemplate(const IExperimentWriterPort* writer) : IUseCase("CreateTemplate"), d{ new Impl } {
		d->writer = writer;
    }

	CreateTemplate::~CreateTemplate() {
        
    }

	auto CreateTemplate::SetTemplateName(const QString& title) -> void {
        d->templateName = title;
    }

	auto CreateTemplate::Perform() -> bool {
		if (d->templateName.isEmpty()) {
			Error("Template name is empty.");
		    return false;
		}

        if (d->writer == nullptr) {
			Error("Experiment writer is unknown.");
            return false;
		}

		// 로드된 experiment로 template 생성
		const auto experiment = Entity::AppData::GetInstance()->GetExperiment();
		if (experiment == nullptr) {
			Error("It fails to get current experiment.");
		    return false;
		}

		const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
		const auto dataPath = systemConfig->GetDataDir();
		const auto user = AppEntity::SessionManager::GetInstance()->GetID();

		auto templateExperiment = std::make_shared<AppEntity::Experiment>(*experiment);
		templateExperiment->SetID(0);
		templateExperiment->SetTitle(d->templateName);
		templateExperiment->SetUserID(user);
		templateExperiment->SetCreatedDate(QDate::currentDate().toString("yyyyMMdd"));

		// template을 user directory에 저장
		const auto templateFilePath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation) + "/experiments";
		const auto path = templateFilePath + "/user/" + user + "/" + d->templateName + "." + AppEntity::ExperimentExtension;
		if (!d->writer->Write(path, templateExperiment)) {
			Error("It fails to save new template file.");
			return false;
		}

		const auto templateRepo = Entity::ExperimentTemplateRepo::GetInstance();
		templateRepo->SetTemplate(d->templateName, templateExperiment, Entity::TemplateType::User);

		return true;
    }

}