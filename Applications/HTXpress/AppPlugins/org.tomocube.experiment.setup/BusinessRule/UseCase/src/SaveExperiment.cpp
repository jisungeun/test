#include "SaveExperiment.h"

#include <QStandardPaths>

#include <AppData.h>
#include <ExperimentTemplateRepo.h>
#include <SessionManager.h>
#include <System.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
	constexpr int32_t allowedPathLength = 240;

	struct SaveExperiment::Impl {
		const IExperimentOutputPort* output{ nullptr };
		const IExperimentWriterPort* writer{ nullptr };

		auto CheckPathLength(const int32_t expPathLength, AppEntity::Experiment::Pointer experiment) const->bool;
	};

    auto SaveExperiment::Impl::CheckPathLength(const int32_t expPathLength, AppEntity::Experiment::Pointer experiment) const -> bool {
		const auto expTitleLength = experiment->GetTitle().length();
		const auto specimenLength = [=]()->int32_t {
			int32_t length = 0;
			const auto vesselCount = experiment->GetVesselCount();
			for(auto vesselIdx=0; vesselIdx<vesselCount; vesselIdx++) {
				for(auto specimen : experiment->GetWellGroupNames(vesselIdx)) {
					if(specimen.length() > length) length = specimen.length();
				}
			}
			return length;
		}();

		const auto dataTitleLength = 14 + expTitleLength + 1 + 3 + 1 + specimenLength + 1 + 2 + 1 + 7;

		//[expPathLength                                     ] [dataTitleLength                              ]
		//E:\Temp\HTXpressTest\Default\TestProject\Test_24Well\220706.170314.Test_24Well.007.Group6.B3.T002P02\220706.170314.Test_24Well.007.Group6.B3.T002P02.tcf
		int32_t totalLength = expPathLength;
		totalLength += 1;							//"/"
		totalLength += dataTitleLength;
		totalLength += 1;							//"/"
		totalLength += dataTitleLength;
		totalLength += 4;							//".tcf"

		return totalLength <= allowedPathLength;
    }

    SaveExperiment::SaveExperiment(const IExperimentOutputPort* output, const IExperimentWriterPort* writer) : IUseCase("SaveExperiment"), d{ new Impl } {
		d->output = output;
		d->writer = writer;
    }

	SaveExperiment::~SaveExperiment() {
        
    }
	
	auto SaveExperiment::Perform() -> bool {
        if (d->writer == nullptr) {
			Error("Experiment writer is unknown.");
		    if(d->output) d->output->ExperimentSaved(false);
			return false;
	    }

		const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
		const auto dataPath = systemConfig->GetDataDir();

		const auto user = AppEntity::SessionManager::GetInstance()->GetID();

		const auto appData = Entity::AppData::GetInstance();
		const auto projectTitle = appData->GetProjectTitle();
		const auto experiment = appData->GetExperiment();

		const auto expPath = dataPath + "/" + user + "/" + projectTitle + "/" + experiment->GetTitle();
	    const auto path = expPath + "/" + experiment->GetTitle() + "." + AppEntity::ExperimentExtension;

		if(!d->CheckPathLength(expPath.length(), experiment)) {
			auto message = QString(tr("Data path too long to save or process data: %1")).arg(path);
			Error(message);
			if(d->output) d->output->ExperimentSaved(false, message);
			return false;
		}

		if (!d->writer->Write(path, experiment)) {
			auto message = QString(tr("Error during save: %1")).arg(path);
			Error(message);
		    if(d->output) d->output->ExperimentSaved(false, message);
			return false;
		}

		appData->SetExperiment(experiment);

		if(d->output) d->output->ExperimentSaved(true, path);

		return true;
    }

}