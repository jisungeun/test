#include "CreateProject.h"

#include <QDir>

#include <SessionManager.h>
#include <System.h>
#include <ProjectRepo.h>
#include <FileUtility.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct CreateProject::Impl {
        const IProjectOutputPort* output{ nullptr };
        const IProjectWriterPort* writer{ nullptr };

        QString projectName;
    };

    CreateProject::CreateProject(const IProjectOutputPort* output, const IProjectWriterPort* writer) : IUseCase("CreateProject"), d{ new Impl } {
        d->output = output;
        d->writer = writer;
    }

    CreateProject::~CreateProject() {
        
    }

    auto CreateProject::SetNewProjectName(const QString& name) -> void {
        d->projectName = name;
    }

    auto CreateProject::Perform() -> bool {
        if (d->projectName.isEmpty()) {
            Error("New project name is empty.");
            return false;
        }

        if (d->writer == nullptr) {
            Error("Project writer is unknown.");
            return false;
        }

        auto projectRepo = AppEntity::ProjectRepo::GetInstance();
        if(projectRepo->GetProject(d->projectName) != nullptr) {
            Error("Project name already exists.");
            return false;
        }

        const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
        const auto dataPath = systemConfig->GetDataDir();

        auto user = AppEntity::SessionManager::GetInstance()->GetID();

        auto projectPath = dataPath + "/" + user + "/" + d->projectName + "/" + d->projectName + "." + AppEntity::ProjectExtension;
        if (!TC::MakeDir(QFileInfo(projectPath).absoluteDir().path())) {
            Error("It fails to make new project path.");
            return false;
        }

        // Todo: ProjectRepo, Project�� ExperimentSetup::Entity�� ����
        const auto newProject = std::make_shared<AppEntity::Project>();
        newProject->SetTitle(TC::GetBaseName(projectPath));

        if (!d->writer->Write(projectPath, newProject)) {
            Error("It fails to save new project.");
            return false;
        }
        
        if(!projectRepo->AddProject(newProject)) {
            Error("It fails to add new project into project repository.");
            return false;
        }

        if (d->output) {
            d->output->UpdateList(projectRepo->GetProjectTitles());
            d->output->UpdateInfo(newProject);    
        }

        return true;
    }
}
