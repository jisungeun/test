#include "IExperimentOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    IExperimentOutputPort::IExperimentOutputPort() = default;
    IExperimentOutputPort::~IExperimentOutputPort() = default;
}