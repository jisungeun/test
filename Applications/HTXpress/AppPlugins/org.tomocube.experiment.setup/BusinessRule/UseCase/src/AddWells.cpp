#include "AddWells.h"

#include <QColor>
#include <QRandomGenerator>

#include <AppData.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct AddWells::Impl {
        const IExperimentOutputPort* output{ nullptr };

        int vesselIndex{ -1 };
        int groupIndex{ -1 };
        QList<AppEntity::RowColumn> wells;
    };

    AddWells::AddWells(const IExperimentOutputPort* outputPort) : IUseCase("AddWells"), d{ new Impl } {
        d->output = outputPort;
    }

    AddWells::~AddWells() {
    }

    auto AddWells::Add(int vesselIndex, int groupIndex, const QList<AppEntity::RowColumn>& indices) -> void{
        d->vesselIndex = vesselIndex;
        d->groupIndex = groupIndex;
        d->wells = indices;
    }

    auto AddWells::Perform() -> bool {
        Print(QString("VesselIndex=%1 GroupIndex=%2 WellCount=%3").arg(d->vesselIndex).arg(d->groupIndex).arg(d->wells.count()));
        if (d->vesselIndex < 0) {
            Error("Vessel index is invalid");
            return false;
        }

        if (d->groupIndex < 0) {
            Error("Group index is invalid.");
            return false;
        }

        if (d->wells.isEmpty()) {
            Error("Well indices list is empty.");
            return false;
        }

        auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if (experiment == nullptr) {
            Error("It fails to get experiment.");
            return false;
        }

        if(experiment->GetWellGroup(d->vesselIndex, d->groupIndex).GetWells().isEmpty()) {
            Error("It fails to find well group.");
            return false;
        }

        experiment->AddWellsToGroup(d->vesselIndex, d->groupIndex, d->wells);

        if (d->output) d->output->AddWellsToWellGroup(d->groupIndex, d->wells);

        return true;
    }
}
