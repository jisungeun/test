#include "ChangeMedium.h"

#include <AppData.h>
#include <MediumDataRepo.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct ChangeMedium::Impl {
        const IExperimentOutputPort* output{ nullptr };

        QString medium;
    };

    ChangeMedium::ChangeMedium(const IExperimentOutputPort* output) : IUseCase("ChangeMedium"), d{ new Impl } {
        d->output = output;
    }

    ChangeMedium::~ChangeMedium() {
        
    }

  auto ChangeMedium::SetMedium(const QString& medium) -> void {
        d->medium = medium;
    }

    auto ChangeMedium::Perform() -> bool {
        if (d->medium.isEmpty()) {
            Error("Medium name is empty.");
            return false;
        }

        const auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if (experiment == nullptr) {
            Error("It fails to get experiment.");
            return false;
        }

        const auto mediumRepo = AppEntity::MediumDataRepo::GetInstance();
        AppEntity::MediumData medium;
        if (!mediumRepo->GetMedium(d->medium, medium)) {
            Error(QString("It fails to find medium(%1).").arg(d->medium));
            return false;
        }

        experiment->SetMedium(medium);

        if (d->output) d->output->UpdateMedium(d->medium);

        return true;
    }

}
