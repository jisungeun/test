#include "UpdateUI.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
	struct UpdateUI::Impl {
		const IAppUIOutputPort* output{ nullptr };
	};

	UpdateUI::UpdateUI(const IAppUIOutputPort* outputPort) : IUseCase("UpdateUI"), d{ new Impl } {
		d->output = outputPort;
	}

	UpdateUI::~UpdateUI() {
	
	}

	auto UpdateUI::Perform() -> bool {
		if (d->output) d->output->UpdateUI();

		return true;
	}
}