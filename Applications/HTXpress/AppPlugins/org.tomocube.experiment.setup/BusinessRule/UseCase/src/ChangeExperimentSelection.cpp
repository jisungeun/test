#include "ChangeExperimentSelection.h"

#include <AppData.h>
#include <ExperimentShortInfo.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct ChangeExperimentSelection::Impl {
        QString title;
    };

    ChangeExperimentSelection::ChangeExperimentSelection() : IUseCase("ChangeExperimentSelection"), d{ new Impl } {
    }

    ChangeExperimentSelection::~ChangeExperimentSelection() {
        
    }
    
    auto ChangeExperimentSelection::SetExperiment(const QString& title) -> void {
        d->title = title;
    }

    auto ChangeExperimentSelection::Perform() -> bool {
        if (d->title.isEmpty()) {
            Error("Experiment title is empty.");
            return false;
        }

        auto appData = Entity::AppData::GetInstance();

        AppEntity::ExperimentID id;
        for(auto& info : appData->GetExperimentListInfos()) {
            if (d->title == info.GetTitle()) {
                id = info.GetID();
                break;
            }
        }

        Entity::AppData::GetInstance()->SetSelectedExperiment(id);
        return true;
    }

}
