#include "ScanProjects.h"

#include <QDirIterator>

#include <ProjectRepo.h>
#include <SessionManager.h>
#include <System.h>
#include <AppData.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
	struct ScanProjects::Impl {
		const IProjectOutputPort* output{ nullptr };
		const IProjectReaderPort* reader{ nullptr };
	};

	ScanProjects::ScanProjects(const IProjectOutputPort* output, const IProjectReaderPort* reader) : IUseCase("ScanProjects"), d{ new Impl } {
		d->output = output;
		d->reader = reader;
    }

	ScanProjects::~ScanProjects() {
	    
	}

	auto ScanProjects::Perform() -> bool {
		if (d->reader == nullptr) {
			Error("Project reader is unknown.");
		    return false;
		}

		const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
		const auto dataPath = systemConfig->GetDataDir();

		auto user = AppEntity::SessionManager::GetInstance()->GetID();
		auto userPath = dataPath + "/" + user;

		// Todo: ProjectRepo, Project�� ExperimentSetup::Entity�� ����
		auto projectRepo = AppEntity::ProjectRepo::GetInstance();
		projectRepo->ClearAll();

		auto userDir = QDir(userPath);
		auto projectDirs = userDir.entryList(QDir::Filters(QDir::AllDirs | QDir::NoDotAndDotDot), QDir::SortFlags(QDir::Name | QDir::IgnoreCase));

		auto findFilter = "*." + AppEntity::ProjectExtension;
		for (auto& projectDir : projectDirs) {
		    QDirIterator projectFileIt(userDir.absolutePath() + "/" + projectDir, { findFilter }, QDir::Files);
		    while (projectFileIt.hasNext()) {
			    auto project = std::make_shared<AppEntity::Project>();
			    if (d->reader->Read(projectFileIt.next(), project)) {
				    projectRepo->AddProject(project);
			    }
		    }    
		}

		Entity::AppData::GetInstance()->Clear();

		if (d->output) d->output->UpdateList(projectRepo->GetProjectTitles());

		return true;
	}
}