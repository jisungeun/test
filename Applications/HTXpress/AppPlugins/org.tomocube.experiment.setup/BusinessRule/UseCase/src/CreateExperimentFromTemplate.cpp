#include "CreateExperimentFromTemplate.h"

#include <QDate>

#include <AppData.h>
#include <Experiment.h>
#include <ExperimentTemplateRepo.h>
#include <SessionManager.h>
#include <System.h>
#include <VesselRepo.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct CreateExperimentFromTemplate::Impl {
        const IExperimentOutputPort* output{ nullptr };
        const IExperimentWriterPort* writer{ nullptr };

        QString templateName;
        QString experimentName;
    };

    CreateExperimentFromTemplate::CreateExperimentFromTemplate(const IExperimentOutputPort* outputPort, const IExperimentWriterPort* writer) : IUseCase("CreateExperimentFromTemplate"), d{ new Impl } {
        d->output = outputPort;
        d->writer = writer;
    }

    CreateExperimentFromTemplate::~CreateExperimentFromTemplate() {
        
    }

    auto CreateExperimentFromTemplate::SetTemplateName(const QString& templateName) -> void {
        d->templateName = templateName;
    }

    auto CreateExperimentFromTemplate::SetNewExperimentName(const QString& experimentName) -> void {
        d->experimentName = experimentName;
    }

    auto CreateExperimentFromTemplate::Perform() -> bool {
        Print(QString("Template=%1 NewExperiment=%2").arg(d->templateName).arg(d->experimentName));

        if (d->templateName.isEmpty()) {
            Error("Template name is empty.");
            return false;
        }

        if (d->experimentName.isEmpty()) {
            Error("Experiment name is empty.");
            return false;
        }

        auto templateRepo = Entity::ExperimentTemplateRepo::GetInstance();
        auto tempExperiment = templateRepo->GetTemplate(d->templateName);
        if (tempExperiment == nullptr) {
            Error(QString("It fails to get template(%1).").arg(d->templateName));
            return false;
        }

        auto userId = AppEntity::SessionManager::GetInstance()->GetID();
        auto experimentId = AppEntity::Experiment::GenerateID(userId, d->experimentName);

        auto newExperiment = std::make_shared<AppEntity::Experiment>(*tempExperiment);
        newExperiment->SetUserID(userId);
        newExperiment->SetID(experimentId);
        newExperiment->SetTitle(d->experimentName);
        newExperiment->SetCreatedDate(QDate::currentDate().toString("yyyyMMdd"));
        if (tempExperiment->GetVessel() == nullptr) {
            newExperiment->SetVesselInfo(std::make_shared<AppEntity::Vessel>());
        }

        const auto appData = Entity::AppData::GetInstance();

        const auto path = appData->GetExperimentFilePath(appData->GetProjectTitle(), d->experimentName);
        if (path.isEmpty()) {
            Error("New experiment path is empty.");
            return false;
        }

		if (!d->writer->Write(path, newExperiment)) {
            Error("It fails to create a new experiment.");
			return false;
		}

        // experiment repository�� �߰�
        appData->SetExperiment(newExperiment);
        appData->SetExperimentListInfo(newExperiment->GetID(), Entity::ExperimentShortInfo(*newExperiment));

        if (d->output) {
            d->output->UpdateList(appData->GetExperimentListInfos());
            d->output->UpdateExperiment(newExperiment);
        }

        return true;
    }

}