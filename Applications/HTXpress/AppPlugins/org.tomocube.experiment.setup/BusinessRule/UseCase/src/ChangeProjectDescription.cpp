#include "ChangeProjectDescription.h"

#include <ProjectRepo.h>
#include <SessionManager.h>
#include <System.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct ChangeProjectDescription::Impl {
        const IProjectOutputPort* output;
        const IProjectWriterPort* writer;

        QString project;
        QString description;
    };
    
    ChangeProjectDescription::ChangeProjectDescription(const IProjectOutputPort* outputPort, const IProjectWriterPort* writer) : IUseCase("ChangeProjectDescription"), d{ new Impl } {
        d->output = outputPort;
        d->writer = writer;
    }

    ChangeProjectDescription::~ChangeProjectDescription() {
        
    }

    auto ChangeProjectDescription::SetDescription(const QString& title, const QString& description) -> void {
        d->project = title;
        d->description = description;
    }

    auto ChangeProjectDescription::Perform() -> bool {
        if (d->project.isEmpty()) {
            Error("Project name is empty.");
            return false;
        }

        if (d->writer == nullptr) {
            Error("Project writer is unknown.");
            return false;
        }

        auto project = AppEntity::ProjectRepo::GetInstance()->GetProject(d->project);
        if (project == nullptr) {
            Error(QString("it fails to get project(%1).").arg(d->project));
            return false;
        }

        project->SetDescription(d->description);

        const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
        const auto dataPath = systemConfig->GetDataDir();

        auto user = AppEntity::SessionManager::GetInstance()->GetID();
        auto path = dataPath + "/" + user + "/" + d->project + "/" + d->project + "." + AppEntity::ProjectExtension;
        if (!d->writer->Write(path, project)) {
            Error("It fails to save a project.");
            return false;
        }

        if (d->output) d->output->UpdateInfo(project);

        return true;
    }

}
