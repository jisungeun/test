#include "DeleteProject.h"

#include <QDir>
#include <QDirIterator>
#include <QDebug>

#include <AppData.h>
#include <SessionManager.h>
#include <System.h>
#include <ProjectRepo.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct DeleteProject::Impl {
        const IProjectOutputPort* projectOutput{};
        const IExperimentOutputPort* experimentOutput{};
        IDirectoryDeleter* directoryDeleter{};
        const IExperimentReaderPort* reader{ nullptr };
        QString projectTitle{};

        auto GetExperiments(const QString& projectPath) -> QList<AppEntity::Experiment::Pointer>;
    };

    auto DeleteProject::Impl::GetExperiments(const QString& projectPath) -> QList<AppEntity::Experiment::Pointer> {
        QList<AppEntity::Experiment::Pointer> experiments;
        const auto findFilter = "*." + AppEntity::ExperimentExtension;
        const auto projectDir = QDir(projectPath);
        const auto experimentDirs = projectDir.entryList(QDir::Filters(QDir::AllDirs | QDir::NoDotAndDotDot), QDir::SortFlags(QDir::Name | QDir::IgnoreCase));

        for (const auto& experimentDir : experimentDirs) {
            QDirIterator experimentFileIt(projectDir.absolutePath() + "/" + experimentDir, { findFilter }, QDir::Files);
            while (experimentFileIt.hasNext()) {
                const auto experimentPath = experimentFileIt.next();
                auto experiment = std::make_shared<AppEntity::Experiment>();
                if (reader->ReadShortInfo(experimentPath, experiment)) {
                    experiments.push_back(experiment);
                }
            }
        }
        return experiments;
    }

    DeleteProject::DeleteProject(
        const IProjectOutputPort* projectOutput, 
        const IExperimentOutputPort* experimentOutput, 
        const IExperimentReaderPort* experimentReader,
        IDirectoryDeleter* directoryDeleter)
    : IUseCase("DeleteProject"), d{std::make_unique<Impl>()} {
        d->projectOutput = projectOutput;
        d->experimentOutput = experimentOutput;
        d->directoryDeleter = directoryDeleter;
        d->reader = experimentReader;
    }

    DeleteProject::~DeleteProject() = default;

    auto DeleteProject::SetProjectTitle(const QString& projectTitle) -> void {
        d->projectTitle = projectTitle;
    }

    auto DeleteProject::Perform() -> bool {
        if (d->projectTitle.isEmpty()) {
            Error("Project name is empty.");
            return false;
        }
        if(!d->projectOutput) {
            Error("Project output is nullptr.");
        }

        if (!d->directoryDeleter) {
            Error("Directory deleter is nullptr.");
            return false;
        }

        if(!d->experimentOutput) {
            Error("Experiment output is nullptr.");
        }

        if(!d->reader) {
            Error("Experiment reader is nullptr.");
        }

        const auto appData = Entity::AppData::GetInstance();
        const auto removeTargetProjectPath = appData->GetProjectPath(d->projectTitle);

        const auto experimentsToRemove = d->GetExperiments(removeTargetProjectPath);

        d->directoryDeleter->SetRootPath(removeTargetProjectPath);
        const auto deleteResult = d->directoryDeleter->Delete();

        if (deleteResult) {
            for(const auto& experiment : experimentsToRemove) {
                appData->RemoveExperimentListInfo(experiment->GetID());
            }

            const auto projectRepo = AppEntity::ProjectRepo::GetInstance();
            projectRepo->RemoveProject(d->projectTitle);
            const auto remainProjects = projectRepo->GetProjectTitles();
            QString changedProjectTitle = "";

            if (!remainProjects.isEmpty()) {
                changedProjectTitle = remainProjects.first();
                const auto changedProjectPath = appData->GetProjectPath(changedProjectTitle);
                const auto experiments = d->GetExperiments(changedProjectPath);
                for(const auto& experiment : experiments) {
                    appData->SetExperimentListInfo(experiment->GetID(), Entity::ExperimentShortInfo(*experiment));
                }

                d->experimentOutput->UpdateList(appData->GetExperimentListInfos());
            }

            appData->SetProjectTitle(changedProjectTitle);
            d->projectOutput->UpdateList(remainProjects);
            d->projectOutput->ChangeProjectSelection(changedProjectTitle);
            d->projectOutput->ProjectDeleted(changedProjectTitle.isEmpty());
        }

        return deleteResult;
    }
}
