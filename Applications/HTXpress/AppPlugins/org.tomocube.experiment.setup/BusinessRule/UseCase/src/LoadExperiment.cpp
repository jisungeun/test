#include "LoadExperiment.h"

#include <AppData.h>
#include <SessionManager.h>
#include <System.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
	struct LoadExperiment::Impl {
		const IExperimentOutputPort* output{ nullptr };
		const IExperimentReaderPort* reader{ nullptr };

		QString experiment;
	};

	LoadExperiment::LoadExperiment(const IExperimentOutputPort* output, const IExperimentReaderPort* reader) : IUseCase("LoadExperiment"), d{ new Impl } {
		d->output = output;
		d->reader = reader;
    }

	LoadExperiment::~LoadExperiment() {
        
    }

	auto LoadExperiment::SetExperiment(const QString& experimentTitle) -> void {
        d->experiment = experimentTitle;
    }

	auto LoadExperiment::Perform() -> bool {
        if (d->experiment.isEmpty()) {
            Error("Experiment name is empty.");
			return false;
		}

		if (d->reader == nullptr) {
			Error("Experiment reader is unknown.");
		    return false;
		}

		auto appData = Entity::AppData::GetInstance();
		auto projectTitle = appData->GetProjectTitle();
		if (projectTitle.isEmpty()) {
			Error("Current project name is empty");
			return false;
		}

		const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
		const auto dataPath = systemConfig->GetDataDir();

		auto user = AppEntity::SessionManager::GetInstance()->GetID();
		auto experimentFilePath = dataPath + "/" + user + "/" + projectTitle + "/" + d->experiment + "/" + d->experiment + "." + AppEntity::ExperimentExtension;

		auto experiment = std::make_shared<AppEntity::Experiment>();
		if (!d->reader->Read(experimentFilePath, experiment)) {
			Error(QString("It fails to read experiment(%1).").arg(d->experiment));
		    return false;
		}

		// 로드된 experiment 저장
		appData->SetExperiment(experiment);

		if (d->output) d->output->UpdateExperiment(experiment);

		return true;
    }

}