#include "SetWellGroupColor.h"

#include <AppData.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct SetWellGroupColor::Impl {
        int vesselIndex{ -1 };
        int groupIndex{ -1 };
        QColor color;
    };

    SetWellGroupColor::SetWellGroupColor() : IUseCase("SetWellGroupColor"), d{ new Impl } {
        
    }

    SetWellGroupColor::~SetWellGroupColor() {

    }

    auto SetWellGroupColor::SetColor(int vesselIndex, int groupIndex, const QColor& color) -> void {
        d->vesselIndex = vesselIndex;
        d->groupIndex = groupIndex;
        d->color = color;
    }

    auto SetWellGroupColor::Perform() -> bool {
        Print(QString("VesselIndex=%1 GroupIndex=%2 Color=RGBA(%3,%4,%5,%6)")
            .arg(d->vesselIndex).arg(d->groupIndex).arg(d->color.red()).arg(d->color.green()).arg(d->color.blue()).arg(d->color.alpha()));

        if (d->vesselIndex < 0) {
            Error("Vessel index is less than 0.");
            return false;
        }

        if (d->groupIndex < 0) {
            Error("Group index is less than 0.");
            return false;
        }

        auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if (experiment == nullptr) {
            Error("It fails to get current experiment.");
            return false;
        }

        auto wellGroup = experiment->GetWellGroup(d->vesselIndex, d->groupIndex);
        wellGroup.SetColor({
            static_cast<uint8_t>(d->color.red()),
            static_cast<uint8_t>(d->color.green()),
            static_cast<uint8_t>(d->color.blue()),
            static_cast<uint8_t>(d->color.alpha())
        });

        return true;
    }

}