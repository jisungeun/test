#include "CreateExperiment.h"

#include <QDate>

#include <SystemStatus.h>
#include <AppData.h>
#include <MediumDataRepo.h>
#include <Experiment.h>
#include <SessionManager.h>
#include <System.h>
#include <VesselRepo.h>
#include <IChannelConfigReader.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct CreateExperiment::Impl {
        const IExperimentOutputPort* output{ nullptr };
        const IExperimentWriterPort* writer{ nullptr };

        QString title;
    };

    CreateExperiment::CreateExperiment(const IExperimentOutputPort* outputPort, const IExperimentWriterPort* writer) : IUseCase("CreateExperiment"), d{ new Impl } {
        d->output = outputPort;
        d->writer = writer;
    }

    CreateExperiment::~CreateExperiment() {
        
    }

    auto CreateExperiment::SetTitle(const QString& experimentTitle) -> void {
        d->title = experimentTitle;
    }

    auto CreateExperiment::Perform() -> bool {
        if (d->title.isEmpty()) {
            Error("Experiment name is empty.");
            return false;
        }

        if (d->writer == nullptr) {
            Error("Experiment writer is unknown.");
            return false;
        }

        auto userId = AppEntity::SessionManager::GetInstance()->GetID();
        auto experimentId = AppEntity::Experiment::GenerateID(userId, d->title);

        // experiment 생성
        auto newExperiment = std::make_shared<AppEntity::Experiment>();
        newExperiment->SetUserID(userId);
        newExperiment->SetID(experimentId);
        newExperiment->SetTitle(d->title);
        newExperiment->SetCreatedDate(QDate::currentDate().toString("yyyyMMdd"));

        Print("Maximum FOV is limited by the constraint that the ROI must be square.");
        const auto maxFOV = AppEntity::System::GetMaximumFOV();
        const auto minValue = std::min(maxFOV.Width(), maxFOV.Height());
        const auto adjustedFOV = AppEntity::Area(minValue, minValue);
        newExperiment->SetFOV(adjustedFOV);

        auto vesselRepo = AppEntity::VesselRepo::GetInstance();
        if (vesselRepo->GetVessels().isEmpty()) {
            Error("Vessel repository is empty.");
            return false;
        }

        // TODO: default vessel model 결정되면 수정 필요.
        const auto vessel = vesselRepo->GetVessels().first();
        newExperiment->SetVesselInfo(vessel);
        newExperiment->SetVesselCount(1);

        auto mediumList = AppEntity::MediumDataRepo::GetInstance()->GetMedia();
        if (!mediumList.isEmpty()) {
            newExperiment->SetMedium(mediumList.first());
        }

        // set HT3D imaging default configuration
        const auto htCondition = std::make_shared<AppEntity::ImagingConditionHT>();
        htCondition->SetDimension(3);

        const auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto [htScanStep, htScanSlices] = sysConfig->GetHTScanParameter(vessel->GetNA());
        const auto halfPulses = (htScanStep * htScanSlices) / 2;

        htCondition->SetSliceStart(-halfPulses);
        htCondition->SetSliceCount(htScanSlices);
        htCondition->SetSliceStep(htScanStep);

        const auto channelConfigReader = IChannelConfigReader::GetInstance();
        AppEntity::ChannelConfig config;
        if(channelConfigReader->Read(AppEntity::ImagingMode::HT3D, config)) {
            htCondition->SetExposure(config.GetCameraExposureUSec());
            htCondition->SetInterval(config.GetCameraInternal());
            htCondition->SetIntensity(config.GetLightIntensity());
        } else {
            const auto model = AppEntity::System::GetModel();
            htCondition->SetExposure(model->HTTriggerExposureUSec());
            htCondition->SetInterval(model->HTTriggerExposureUSec() + model->HTTriggerReadOutUSec());
        }

        newExperiment->SetSingleImagingCondition(AppEntity::ImagingType::HT3D, htCondition);
        newExperiment->SetEnableSingleImaging(AppEntity::ImagingType::HT3D, true);

        // create new experiment file
        const auto appData = Entity::AppData::GetInstance();
        const auto path = appData->GetExperimentFilePath(appData->GetProjectTitle(), d->title);
        if (path.isEmpty()) {
            Error("Experiment path is empty.");
            return false;
        }

		if (!d->writer->Write(path, newExperiment)) {
            Error("It fails to save a new experiment.");
			return false;
		}

        // experiment repository에 추가
        appData->SetExperiment(newExperiment);
        appData->SetExperimentListInfo(newExperiment->GetID(), Entity::ExperimentShortInfo(*newExperiment));

        if (d->output) {
            d->output->UpdateList(appData->GetExperimentListInfos());
            d->output->UpdateExperiment(newExperiment);
        }

        return true;
    }

}