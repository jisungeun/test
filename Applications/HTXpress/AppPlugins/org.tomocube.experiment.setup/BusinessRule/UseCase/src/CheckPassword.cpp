﻿#include <SessionManager.h>

#include "IUserManager.h"
#include "CheckPassword.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct CheckPassword::Impl {
        QString password{};
        bool valid{};
    };
    CheckPassword::CheckPassword() : IUseCase("CheckPassword"), d{ std::make_unique<Impl>() } {
    }

    CheckPassword::~CheckPassword() = default;

    auto CheckPassword::SetPassword(const QString& password) -> void {
        d->password = password;
    }

    auto CheckPassword::Check() const -> bool {
        return d->valid;
    }

    auto CheckPassword::Perform() -> bool {
        const auto session = AppEntity::SessionManager::GetInstance();
        const auto userManager = IUserManager::GetInstance();
        
        d->valid = userManager->IsValid(session->GetID(), d->password);

        return true;
    }
}
