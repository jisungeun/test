#include "ChangeNumberOfVessel.h"

#include <AppData.h>
#include <MediumDataRepo.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct ChangeNumberOfVessel::Impl {
        const IExperimentOutputPort* output{ nullptr };

        int32_t vessels{ 0 };
    };

    ChangeNumberOfVessel::ChangeNumberOfVessel(const IExperimentOutputPort* output) : IUseCase("ChangeNumberOfVessel"), d{ new Impl } {
        d->output = output;
    }

    ChangeNumberOfVessel::~ChangeNumberOfVessel() {
        
    }

    auto ChangeNumberOfVessel::SetNumberOfVessel(const int32_t vessels) -> void {
        d->vessels = vessels;
    }

    auto ChangeNumberOfVessel::Perform() -> bool {
        if (d->vessels < 1) {
            Error("Vessel count is less than 1.");
            return false;
        }
        
        const auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if (experiment == nullptr) {
            Error("It fails to get experiment.");
            return false;
        }

        experiment->SetVesselCount(d->vessels);

        if (d->output) d->output->UpdateVesselCount(d->vessels);

        return true;
    }

}
