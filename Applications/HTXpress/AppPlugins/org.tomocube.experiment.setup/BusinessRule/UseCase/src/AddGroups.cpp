#include "AddGroups.h"

#include <QColor>
#include <QRandomGenerator>

#include <AppData.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct AddGroups::Impl {
        const IExperimentOutputPort* output{ nullptr };

        int vesselIndex{ -1 };
        QList<AppEntity::RowColumn> wells;
        bool individual{ false };

        auto GenerateWellGroupTitle(AppEntity::WellGroupIndex index, const QList<QString>& names) const->QString;
        auto GenerateWellGroupColor(int32_t vesselIndex) const->AppEntity::WellGroup::Color;
        auto Convert(const AppEntity::WellGroup::Color& entityColor) const->QColor;
        auto Convert(const QColor& qColor) const->AppEntity::WellGroup::Color;
    };

    auto AddGroups::Impl::GenerateWellGroupTitle(AppEntity::WellGroupIndex index, const QList<QString>& names) const->QString {
        auto newName = QString("Group%1").arg(index+1);
        if (names.contains(newName)) {
            return GenerateWellGroupTitle(index + 1, names);
        }

        return newName;
    }

    auto AddGroups::Impl::GenerateWellGroupColor(int32_t vessel) const->AppEntity::WellGroup::Color {
        QList colors {
            QColor{"#80DEEA"},
            QColor{"#7BAAFF"},
            QColor{"#5894FF"},
            QColor{"#2E77FB"},
            QColor{"#4338F1"},
            QColor{"#5D25F6"},
            QColor{"#7E6FBC"},
            QColor{"#7E57C2"},
            QColor{"#7400FF"},
            QColor{"#5E1CB0"},
            QColor{"#430088"},
            QColor{"#5c0088"},
            QColor{"#AB7D8C"},
            QColor{"#A16076"},
            QColor{"#9E50A4"},
            QColor{"#720D78"},
            QColor{"#0A168C"},
            QColor{"#7E7D7D"},
            QColor{"#585555"},
            QColor{"#665151"},
            QColor{"#75694E"},
            QColor{"#6E5828"},
            QColor{"#966A09"},
            QColor{"#A38926"},
            // extra color
            QColor{"#5C6BC0"},
            QColor{"#3949AB"},
            QColor{"#003991"},
            QColor{"#0111D5"},
            QColor{"#0A168C"}
        };

        QColor newColor;

        auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if(experiment) {
            QList<QColor> existColorList;
            for(const auto& wellGroupIndex : experiment->GetWellGroupIndices(vessel)) {
                const auto existColor = Convert(experiment->GetWellGroup(vessel, wellGroupIndex).GetColor());
                colors.removeOne(existColor);
            }

            if(!colors.isEmpty()) {
                newColor = colors.first();
            }
            else {
                newColor = {
                    QRandomGenerator::global()->bounded(0,255),
                    QRandomGenerator::global()->bounded(0,255),
                    QRandomGenerator::global()->bounded(0,255)
                };
            }
        }

        return Convert(newColor);
    }

    auto AddGroups::Impl::Convert(const AppEntity::WellGroup::Color& entityColor) const -> QColor {
        return {entityColor.r, entityColor.g, entityColor.b, entityColor.a};
    }

    auto AddGroups::Impl::Convert(const QColor& qColor) const -> AppEntity::WellGroup::Color {
        AppEntity::WellGroup::Color color;
        color.r = qColor.red();
        color.g = qColor.green();
        color.b = qColor.blue();
        color.a = qColor.alpha();
        return color;
    }

    AddGroups::AddGroups(const IExperimentOutputPort* outputPort) : IUseCase("AddGroups"), d{ new Impl } {
        d->output = outputPort;
    }

    AddGroups::~AddGroups() {
    }

    auto AddGroups::SetVesselIndex(int vesselIndex) -> void {
        d->vesselIndex = vesselIndex;
    }

    auto AddGroups::AddWells(const QList<AppEntity::RowColumn>& wells) -> void {
        d->wells << wells;
    }

    auto AddGroups::SetIndividualGroup(bool individual) -> void {
        d->individual = individual;
    }
    
    auto AddGroups::Perform() -> bool {
        Print(QString("VesselIndex=%1 WellCount=%2").arg(d->vesselIndex).arg(d->wells.count()));
        if (d->vesselIndex < 0) {
            Error("Vessel index is invalid");
            return false;
        }

        if (d->wells.isEmpty()) {
            Error("Well indices list is empty.");
            return false;
        }

        auto experiment = Entity::AppData::GetInstance()->GetExperiment();
        if (experiment == nullptr) {
            Error("It fails to get experiment.");
            return false;
        }

        auto AddGroup = [=](QList<AppEntity::RowColumn>& wells) -> AppEntity::WellGroupIndex {
            const auto groupNames = experiment->GetWellGroupNames(d->vesselIndex).values();
            auto groupIndex = experiment->GetNewWellGroupIndex();

            AppEntity::WellGroup group;
            group.SetTitle(d->GenerateWellGroupTitle(groupIndex, groupNames));
            group.SetColor(d->GenerateWellGroupColor(d->vesselIndex));

            for (auto well : wells) {
                group.AddWell(well.first, well.second);
            }

            experiment->SetWellGroup(d->vesselIndex, groupIndex, group);

            return groupIndex;
        };

        QMap<AppEntity::WellGroupIndex, QList<AppEntity::RowColumn>> addedGroup;

        if (d->individual) {
            for (auto well : d->wells) {
                QList<AppEntity::RowColumn> groupWells{well};
                auto groupIndex = AddGroup(groupWells);

                addedGroup.insert(groupIndex, groupWells);
            }
        } else {
            auto groupIndex = AddGroup(d->wells);
            addedGroup.insert(groupIndex, d->wells);
        }

        if (d->output) d->output->AddWellGroup(addedGroup);

        return true;
    }

}
