﻿#include <AppData.h>
#include <System.h>
#include <MediumDataRepo.h>

#include "CheckMediumValidation.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct CheckMediumValidation::Impl {
        explicit Impl(CheckMediumValidation* self) : self(self) {}
        const IExperimentOutputPort* output{};
        CheckMediumValidation *self{};
        auto Error(const QString& message) const -> void;
    };

    auto CheckMediumValidation::Impl::Error(const QString& message) const -> void {
        self->Error(message);
        if(output) {
            output->Error(message);
        }
    }

    CheckMediumValidation::CheckMediumValidation(const IExperimentOutputPort* output)
        :IUseCase("CheckMediumValidation"), d{std::make_unique<Impl>(this)} {
        d->output = output;
    }

    CheckMediumValidation::~CheckMediumValidation() = default;

    auto CheckMediumValidation::Perform() -> bool {
        const auto mediumRepo = AppEntity::MediumDataRepo::GetInstance();
        const auto appData = Entity::AppData::GetInstance();
        const auto experiment = appData->GetExperiment();
        const auto medium = experiment->GetMedium();

        const auto isValid = mediumRepo->GetMedia().contains(medium);

        if(!isValid) {
            d->Error(tr("It fails to run this experiment.\n"
                        "Please select a medium on the medium list."));
        }

        return isValid;
    }
}
