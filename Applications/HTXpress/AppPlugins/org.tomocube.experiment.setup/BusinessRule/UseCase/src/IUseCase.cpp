#include <QStringList>

#include "IUseCaseLogger.h"
#include "IUseCase.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    struct IUseCase::Impl {
        QString useCaseTitle;
        
        inline auto Title() const->QString {
            return useCaseTitle;
        }
    };

    IUseCase::IUseCase(const QString& useCaseTitle) : d{ new Impl } {
        d->useCaseTitle = useCaseTitle;
    }

    IUseCase::~IUseCase() {
    }

    auto IUseCase::Request() -> bool {
        Print("> Start");

        if (!Perform()) {
            Print("> Failed");
            return false;
        }

        Print("> Success");
        return true;
    }

    auto IUseCase::Print(const QString& message) -> void {
        IUseCaseLogger::PrintLog(d->Title(), message);
    }
    
    auto IUseCase::Error(const QString& message) -> void {
        IUseCaseLogger::PrintError(d->Title(), message);
    }

}
