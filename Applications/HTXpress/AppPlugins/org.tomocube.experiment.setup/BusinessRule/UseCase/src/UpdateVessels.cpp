#include "UpdateVessels.h"

#include <VesselRepo.h>

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
	struct UpdateVessels::Impl {
		const IExperimentOutputPort* output{ nullptr };
	};

	UpdateVessels::UpdateVessels(const IExperimentOutputPort* outputPort) : IUseCase("UpdateVessels"), d{ new Impl } {
		d->output = outputPort;
	}

	UpdateVessels::~UpdateVessels() {
	
	}

	auto UpdateVessels::Perform() -> bool {
        auto vessels = AppEntity::VesselRepo::GetInstance()->GetVessels();
		QStringList vesselNames;
		for (auto vessel : vessels) {
		    vesselNames << vessel->GetName();
		}

		if (d->output) d->output->UpdateVessels(vesselNames);

		return true;
	}
}