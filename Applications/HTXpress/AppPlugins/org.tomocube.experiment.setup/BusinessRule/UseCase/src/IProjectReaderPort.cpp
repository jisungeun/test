#include "IProjectReaderPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    IProjectReaderPort::IProjectReaderPort() = default;
    IProjectReaderPort::~IProjectReaderPort() = default;
}