﻿#pragma once

#include <QString>

#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API IDirectoryDeleter {
    public:
        virtual ~IDirectoryDeleter() = default;
        virtual auto SetRootPath(const QString& rootPath) -> void = 0;
        virtual auto Delete() const -> bool = 0;
    };
}
