﻿#pragma once
#include <memory>
#include <QCoreApplication>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API CheckPassword final : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(CheckPassword)
    public:
        CheckPassword();
        ~CheckPassword() override;

        auto SetPassword(const QString& password)->void;
        auto Check() const->bool;

    private:
        auto Perform() -> bool override;
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}