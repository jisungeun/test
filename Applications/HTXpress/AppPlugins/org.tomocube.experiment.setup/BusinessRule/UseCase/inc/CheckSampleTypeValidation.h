﻿#pragma once
#include <memory>
#include <QCoreApplication>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"
#include "IExperimentOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API CheckSampleTypeValidation final : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(CheckSampleTypeValidation)
    public:
        explicit CheckSampleTypeValidation(const IExperimentOutputPort* output);
        ~CheckSampleTypeValidation() override;

    private:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
