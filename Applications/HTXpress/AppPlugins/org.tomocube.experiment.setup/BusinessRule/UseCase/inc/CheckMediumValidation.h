﻿#pragma once

#include <memory>
#include <QCoreApplication>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"
#include "IExperimentOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API CheckMediumValidation final : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(CheckMediumValidation)
    public:
        explicit CheckMediumValidation(const IExperimentOutputPort* output);
        ~CheckMediumValidation() override;

    private:
        auto Perform() -> bool override;
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
