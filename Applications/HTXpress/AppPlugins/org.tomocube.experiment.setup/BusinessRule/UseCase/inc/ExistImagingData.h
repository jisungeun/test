#pragma once

#include <QString>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API ExistImagingData : public IUseCase {
    public:
        ExistImagingData();
        ~ExistImagingData() override;

        auto SetExperiment(const QString& experiment) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}