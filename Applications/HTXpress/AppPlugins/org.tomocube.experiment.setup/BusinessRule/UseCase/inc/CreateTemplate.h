#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IExperimentWriterPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API CreateTemplate : public IUseCase {
    public:
        CreateTemplate(const IExperimentWriterPort* writer);
        ~CreateTemplate() override;

        /**
         * \brief 사용중인 experiment를 experiment template으로 저장한다.
         * \param title 저장될 template 이름
         */
        auto SetTemplateName(const QString& title) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
