#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IExperimentOutputPort.h"
#include "IExperimentReaderPort.h"
#include "IExperimentWriterPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API DuplicateExperiment : public IUseCase {
    public:
        DuplicateExperiment(const IExperimentOutputPort* outputPort, const IExperimentReaderPort* reader, const IExperimentWriterPort* writer);
        ~DuplicateExperiment() override;

        auto SetExperiment(const QString& sourceExperimentName, const QString& destExperimentName) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
