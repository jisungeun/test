#pragma once

#include <memory>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API MoveWellGroup : public IUseCase {
    public:
        MoveWellGroup();
        ~MoveWellGroup() override;

        auto SetVesselIndex(int index) -> void;
        auto SetGroupIndex(int sourceIndex, int destIndex) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
