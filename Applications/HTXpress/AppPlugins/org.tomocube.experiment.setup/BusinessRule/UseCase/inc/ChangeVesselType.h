#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IExperimentOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API ChangeVesselType : public IUseCase{
    public:
        ChangeVesselType(const IExperimentOutputPort* output = nullptr);
        ~ChangeVesselType() override;

        auto SetVesselType(const QString& name) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
