﻿#pragma once
#include <QApplication>

#include "HTX_Experiment_Setup_UseCaseExport.h"
#include "IUseCase.h"
#include "IExperimentOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API ChangeExperimentSampleType final : public IUseCase{
        Q_DECLARE_TR_FUNCTIONS(ChangeExperimentSampleType)
    public:
        ChangeExperimentSampleType(const IExperimentOutputPort* output = nullptr);
        ~ChangeExperimentSampleType() override;

        auto SetSampleTypeName(const QString& sampleTypeName) -> void;

    private:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
