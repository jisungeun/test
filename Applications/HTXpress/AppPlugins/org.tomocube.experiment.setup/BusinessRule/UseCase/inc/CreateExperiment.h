#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IExperimentOutputPort.h"
#include "IExperimentWriterPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API CreateExperiment : public IUseCase {
    public:
        CreateExperiment(const IExperimentOutputPort* outputPort, const IExperimentWriterPort* writer);
        ~CreateExperiment() override;

        auto SetTitle(const QString& experimentTitle) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
