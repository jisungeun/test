#pragma once

#include <memory>

#include <IExperimentOutputPort.h>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API AddWells : public IUseCase {
    public:
        AddWells(const IExperimentOutputPort* outputPort = nullptr);
        ~AddWells() override;
        
        auto Add(int vesselIndex, int groupIndex, const QList<AppEntity::RowColumn>& indices) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
