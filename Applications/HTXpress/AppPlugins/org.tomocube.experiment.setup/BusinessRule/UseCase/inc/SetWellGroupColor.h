#pragma once

#include <memory>

#include <QColor>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API SetWellGroupColor : public IUseCase {
    public:
        SetWellGroupColor();
        ~SetWellGroupColor() override;

        auto SetColor(int vesselIndex, int groupIndex, const QColor& name) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
