#pragma once

#include <QString>

#include <IExperimentOutputPort.h>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"


namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API SetWellName : public IUseCase {
    public:
        explicit SetWellName(const IExperimentOutputPort* outputPort = nullptr);
        ~SetWellName() override;

        auto SetName(int vesselIndex, int wellIndex, const QString& name) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
