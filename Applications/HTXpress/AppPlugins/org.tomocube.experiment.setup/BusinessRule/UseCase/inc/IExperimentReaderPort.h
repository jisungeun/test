#pragma once

#include <Experiment.h>

#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API IExperimentReaderPort {
    public:
        IExperimentReaderPort();
        virtual ~IExperimentReaderPort();

        virtual auto Read(const QString& path, AppEntity::Experiment::Pointer& experiment) const -> bool = 0;
        virtual auto ReadShortInfo(const QString& path, AppEntity::Experiment::Pointer& experiment) const -> bool = 0;
    };
}