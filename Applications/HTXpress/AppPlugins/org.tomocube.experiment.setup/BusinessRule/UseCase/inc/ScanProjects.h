#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IProjectOutputPort.h"
#include "IProjectReaderPort.h"
#include "IExperimentReaderPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API ScanProjects : public IUseCase {
    public:
        ScanProjects(const IProjectOutputPort* output, const IProjectReaderPort* reader);
        ~ScanProjects() override;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
