#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IExperimentOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API ChangeNumberOfVessel : public IUseCase{
    public:
        ChangeNumberOfVessel(const IExperimentOutputPort* output = nullptr);
        ~ChangeNumberOfVessel() override;

        auto SetNumberOfVessel(const int32_t vessels) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
