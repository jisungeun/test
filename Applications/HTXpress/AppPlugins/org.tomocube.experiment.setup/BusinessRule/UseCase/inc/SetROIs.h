﻿#pragma once
#include <memory>
#include <QCoreApplication>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"
#include "IExperimentOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API SetROIs final : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(SetROIs)
    public:
        using Self = SetROIs;
        using ROIList = QMap<AppEntity::WellIndex, QMap<AppEntity::ROIIndex, AppEntity::ROI::Pointer>>;
        explicit SetROIs(const IExperimentOutputPort* output);
        ~SetROIs() override;

        auto SetVesselIndex(const AppEntity::VesselIndex& vesselIndex) -> void;
        auto SetRoiList(const ROIList& roiList) -> void;

    private:
        auto Perform() -> bool override;
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
