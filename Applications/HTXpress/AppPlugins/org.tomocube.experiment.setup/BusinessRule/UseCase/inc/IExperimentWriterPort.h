#pragma once

#include <Experiment.h>

#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API IExperimentWriterPort {
    public:
        IExperimentWriterPort();
        virtual ~IExperimentWriterPort();

        virtual auto Write(const QString& path, const AppEntity::Experiment::Pointer& experiment) const -> bool = 0;
    };
}
