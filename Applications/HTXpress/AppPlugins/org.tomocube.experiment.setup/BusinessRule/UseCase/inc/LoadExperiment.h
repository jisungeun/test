#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IExperimentOutputPort.h"
#include "IExperimentReaderPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API LoadExperiment : public IUseCase {
    public:
        LoadExperiment(const IExperimentOutputPort* output, const IExperimentReaderPort* reader);
        ~LoadExperiment() override;

        auto SetExperiment(const QString& experimentTitle) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
