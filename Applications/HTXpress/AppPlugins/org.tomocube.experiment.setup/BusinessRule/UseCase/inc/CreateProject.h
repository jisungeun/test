#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IProjectOutputPort.h"
#include "IProjectWriterPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API CreateProject : public IUseCase {
    public:
        CreateProject(const IProjectOutputPort* output, const IProjectWriterPort* writer);
        ~CreateProject() override;

        auto SetNewProjectName(const QString& name) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
