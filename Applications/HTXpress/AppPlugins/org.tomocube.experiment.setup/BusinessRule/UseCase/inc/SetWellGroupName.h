#pragma once

#include <memory>

#include <QString>

#include <IExperimentOutputPort.h>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API SetWellGroupName : public IUseCase {
    public:
        SetWellGroupName(const IExperimentOutputPort* outputPort);
        ~SetWellGroupName() override;

        auto SetName(int vesselIndex, int groupIndex, const QString& name) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
