#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IDirectoryDeleter.h"
#include "IExperimentOutputPort.h"
#include "IExperimentReaderPort.h"
#include "IProjectOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API DeleteProject final : public IUseCase {
    public:
        DeleteProject(
            const IProjectOutputPort* projectOutput, 
            const IExperimentOutputPort* experimentOutput, 
            const IExperimentReaderPort* experimentReader, 
            IDirectoryDeleter* directoryDeleter);
        ~DeleteProject() override;

        /**
         * \brief Project 경로에 있는 모든 파일을 삭제
         * \param projectTitle 삭제할 Project 이름
         */
        auto SetProjectTitle(const QString& projectTitle) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
