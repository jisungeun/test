#pragma once

#include <memory>

#include <IExperimentOutputPort.h>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API AddGroups : public IUseCase {
    public:
        AddGroups(const IExperimentOutputPort* outputPort = nullptr);
        ~AddGroups() override;

        auto SetVesselIndex(int vesselIndex) -> void;
        auto AddWells(const QList<AppEntity::RowColumn>& wells) -> void;
        auto SetIndividualGroup(bool individual) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
