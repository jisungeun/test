#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IExperimentOutputPort.h"
#include "IExperimentWriterPort.h"
#include "IDirectoryDeleter.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API DeleteExperiment : public IUseCase {
    public:
        DeleteExperiment(const IExperimentOutputPort* output, IDirectoryDeleter* directoryDeleter);
        ~DeleteExperiment() override;

        /**
         * \brief Experiment 경로에 있는 모든 파일을 삭제
         * \param experimentTitle 삭제할 Experiment 이름
         */
        auto SetExperiment(const QString& experimentTitle) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
