#pragma once

#include <memory>

#include "IAppUIOutputPort.h"

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API UpdateUI : public IUseCase {
    public:
        UpdateUI(const IAppUIOutputPort* outputPort);
        ~UpdateUI() override;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
