#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
	class HTX_Experiment_Setup_UseCase_API IAppUIOutputPort {
	public:
		IAppUIOutputPort();
		virtual ~IAppUIOutputPort();

		virtual auto UpdateUI() const -> void = 0;
	};
}