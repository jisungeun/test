#pragma once

#include <memory>

#include <IExperimentOutputPort.h>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API RemoveWells : public IUseCase {
    public:
        RemoveWells(const IExperimentOutputPort* outputPort = nullptr);
        ~RemoveWells() override;

        auto SetVesselIndex(AppEntity::VesselIndex index) -> void;
        auto AddRemovalWells(const QList<QPair<int, int>>& wells) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
