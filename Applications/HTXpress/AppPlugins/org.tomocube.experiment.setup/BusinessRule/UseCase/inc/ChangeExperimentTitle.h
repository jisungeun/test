#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IExperimentWriterPort.h"
#include "IExperimentOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API ChangeExperimentTitle : public IUseCase{
    public:
        ChangeExperimentTitle(const IExperimentOutputPort* output = nullptr, const IExperimentWriterPort* writer = nullptr);
        ~ChangeExperimentTitle() override;

        auto SetTitle(const QString& title) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
