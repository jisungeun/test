﻿#pragma once
#include <QCoreApplication>

#include "HTX_Experiment_Setup_UseCaseExport.h"
#include "IUseCase.h"
#include "IExperimentOutputPort.h"
#include "IProfileCopier.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API CopyProfiles final : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(CopyProfiles)
    public:
        explicit CopyProfiles(IProfileCopier* copier);
        ~CopyProfiles() override;

        auto SetProfileName(const QString& profileName) -> void;

    private:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
