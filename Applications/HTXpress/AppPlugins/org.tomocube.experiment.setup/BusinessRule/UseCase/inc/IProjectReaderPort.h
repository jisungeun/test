#pragma once

#include <Project.h>

#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
	class HTX_Experiment_Setup_UseCase_API IProjectReaderPort {
	public:
		IProjectReaderPort();
		virtual ~IProjectReaderPort();

		virtual auto Read(const QString& path, const AppEntity::Project::Pointer& project) const -> bool = 0;
	};
}