#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IExperimentOutputPort.h"
#include "IExperimentWriterPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API CreateExperimentFromTemplate : public IUseCase {
    public:
        CreateExperimentFromTemplate(const IExperimentOutputPort* outputPort, const IExperimentWriterPort* writer);
        ~CreateExperimentFromTemplate() override;

        auto SetTemplateName(const QString& templateName) -> void;
        auto SetNewExperimentName(const QString& experimentName) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
