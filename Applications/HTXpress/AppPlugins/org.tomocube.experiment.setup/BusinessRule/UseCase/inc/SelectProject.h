#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IExperimentOutputPort.h"
#include "IExperimentReaderPort.h"
#include "IProjectOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API SelectProject : public IUseCase{
    public:
        SelectProject(const IProjectOutputPort* projectOutputPort, const IExperimentOutputPort* experimentOutputPort, const IExperimentReaderPort* experimentReader);
        ~SelectProject() override;

        /**
         * \brief 선택된 프로젝트의 정보와 포함된 Experiment 목록을 읽어들인다.
         * \param title 선택된 프로젝트 이름
         */
        auto SetProject(const QString& title) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
