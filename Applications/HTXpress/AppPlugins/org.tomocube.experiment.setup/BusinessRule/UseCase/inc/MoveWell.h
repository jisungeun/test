#pragma once

#include <memory>

#include <QList>

#include <IExperimentOutputPort.h>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API MoveWell : public IUseCase{
    public:
        MoveWell(const IExperimentOutputPort* outputPort = nullptr);
        ~MoveWell() override;

        auto SetVesselIndex(int index) -> void;
        auto SetWellGroupIndex(int sourceIndex, int destIndex) -> void;
        auto AddWells(const QList<QPair<int, int>>& wells) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
