﻿#pragma once

#include <memory>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"
#include "IExperimentOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API CheckVesselValidation : public IUseCase {
    public:
        explicit CheckVesselValidation(const IExperimentOutputPort* output);
        ~CheckVesselValidation() override;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
