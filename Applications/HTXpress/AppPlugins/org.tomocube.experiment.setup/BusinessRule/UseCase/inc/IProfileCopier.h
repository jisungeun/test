#pragma once
#include <QString>

#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API IProfileCopier {
    public:
        IProfileCopier() = default;
        virtual ~IProfileCopier() = default;

        virtual auto SetProfileName(const QString& profileName) -> void = 0;
        virtual auto SetSource(const QString& sourceFolder) -> void = 0;
        virtual auto AddDestination(const QString& destinationFolder) -> void = 0;
        virtual auto SetFileNameFilters(const QStringList& filters) -> void = 0;
        virtual auto Copy() const -> bool = 0;

        static auto AreProfilesExist(const QString& sampleTypeName, const QString& targetFolderPath) -> bool;
        static auto GetFilters(const QString& sampleTypeName) -> QStringList;
    };
}
