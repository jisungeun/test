#pragma once

#include <ProjectRepo.h>

#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
	class HTX_Experiment_Setup_UseCase_API IProjectOutputPort {
	public:
		IProjectOutputPort();
		virtual ~IProjectOutputPort();

		virtual auto UpdateList(const QList<QString>& list) const -> void = 0;	// Project 목록 업데이트
		virtual auto ChangeProjectSelection(const QString& title) const -> void = 0;	// Project 선택 변경
		virtual auto UpdateInfo(const AppEntity::Project::Pointer& project) const -> void = 0;	// Project 내용이 표시되는 부분 업데이트
		virtual auto ProjectDeleted(bool isProjectEmpty) const -> void = 0; // project 삭제 완료
	};
}