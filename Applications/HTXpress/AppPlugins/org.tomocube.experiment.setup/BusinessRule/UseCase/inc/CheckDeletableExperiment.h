#pragma once

#include <memory>

#include <QString>

#include "IUseCase.h"
#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API CheckDeletableExperiment : public IUseCase {
    public:
        CheckDeletableExperiment();
        ~CheckDeletableExperiment() override;

        auto SetExperiment(const QString& title) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
