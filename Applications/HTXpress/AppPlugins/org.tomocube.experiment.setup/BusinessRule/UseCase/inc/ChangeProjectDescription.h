#pragma once

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IProjectOutputPort.h"
#include "IProjectWriterPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API ChangeProjectDescription : public IUseCase {
    public:
        ChangeProjectDescription(const IProjectOutputPort* outputPort, const IProjectWriterPort* writer);
        ~ChangeProjectDescription() override;

        /**
         * \brief 프로젝트 설명을 수정한다.
         * \param title 수정할 프로젝트 이름
         * \param description 변경되는 설명
         */
        auto SetDescription(const QString& title, const QString& description) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
