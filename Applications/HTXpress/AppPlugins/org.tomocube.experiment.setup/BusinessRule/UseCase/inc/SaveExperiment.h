#pragma once

#include <QCoreApplication>

#include "HTX_Experiment_Setup_UseCaseExport.h"

#include "IUseCase.h"
#include "IExperimentOutputPort.h"
#include "IExperimentWriterPort.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API SaveExperiment : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(SaveExperiment)
    public:
        SaveExperiment(const IExperimentOutputPort* output, const IExperimentWriterPort* writer);
        ~SaveExperiment() override;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
