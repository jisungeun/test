﻿#pragma once
#include <memory>

#include <User.h>

#include "HTX_Experiment_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Setup::UseCase {
    class HTX_Experiment_Setup_UseCase_API IUserManager {
    public:
        using Pointer = std::shared_ptr<IUserManager>;

    protected:
        IUserManager();

    public:
        virtual ~IUserManager();

        static auto GetInstance()->IUserManager*;

        virtual auto IsValid(const AppEntity::UserID& id, const QString& password) const->bool = 0;
    };
}