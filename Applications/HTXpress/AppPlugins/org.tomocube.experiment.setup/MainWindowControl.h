#pragma once

#include <memory>

#include <User.h>

namespace HTXpress::AppPlugins::Experiment::Setup {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

        auto GetCurrentUserId() const-> AppEntity::UserID;
        
        auto Initialize() -> bool;

        auto ResetExperiment() -> void;

        auto ScanProjects()->bool;
        auto SetProject(const QString& project = QString())->bool;

        auto ReloadExperiment() const->bool;
        auto LoadTemplates()->bool;

        auto UpdateUI() const->bool;

        auto GetCurrentProject() const->QString;
        auto GetCurrentExperiment() const->QString;

        auto GetRunningProject() const->QString;
        auto GetRunningExperiment() const->QString;

        auto IsRunningExperiment() const->bool;
        auto IsRunningTimelapse() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}