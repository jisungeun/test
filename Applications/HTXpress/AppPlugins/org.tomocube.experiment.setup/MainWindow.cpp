#define LOGGER_TAG "[MainWindow]"

#include <TCLogger.h>
#include <MessageDialog.h>
#include <AppEvent.h>
#include <MenuEvent.h>
#include <UserManagerPlugin.h>

#include "MainWindow.h"

#include <QTimer>

#include "MainWindowControl.h"
#include "ui_MainWindow.h"

namespace HTXpress::AppPlugins::Experiment::Setup::App {
    struct MainWindow::Impl {
        Ui::MainWindow ui;
        MainWindowControl control;

        QVariantMap appProperties;

        std::shared_ptr<Plugins::UserManager::Plugin> userManager{};
    };
    
    MainWindow::MainWindow(QWidget* parent) : IMainWindowHTX("Experiment Setup", "Setup", parent), d{new Impl} {
        d->ui.setupUi(this);

        subscribeEvent("TabChange");

        d->userManager.reset(new Plugins::UserManager::Plugin());

        connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnHandleEvent(ctkEvent)));

        connect(d->ui.leftToolButtonsPanel, &LeftToolButtonsPanel::sigOpenDataNavigation, this, &MainWindow::onOpenDataNavigation);
        connect(d->ui.leftToolButtonsPanel, &LeftToolButtonsPanel::sigGoToHome, this, &MainWindow::onGoToHome);
        connect(d->ui.rightToolButtonsPanel, &RightToolButtonsPanel::sigRunExperiment, this, &MainWindow::onRunExperiment);
    }
    
    MainWindow::~MainWindow() {        
    }
    
    void MainWindow::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
    }

    auto MainWindow::InitUI()->bool {
        return true;
    }
    auto MainWindow::TryActivate() -> bool {
        return true;
    }
    auto MainWindow::TryDeactivate() -> bool {
        return true;
    }
    auto MainWindow::IsActivate() -> bool {
        return true;
    }
    auto MainWindow::GetMetaInfo() -> QVariantMap {
        return d->appProperties;
    }

    auto MainWindow::Execute(const QVariantMap& params) -> bool {
        Q_UNUSED(params)

        if (d->control.IsRunningExperiment()) {
            // initialize project and experiment to running experiment data
            if (d->control.SetProject(d->control.GetRunningProject())) {
                if (!d->control.ReloadExperiment()) {
                    TC::MessageDialog::warning(this, tr("Reload Experiment"), tr("Failed to reload experiment."));
                }
            } else {
                TC::MessageDialog::warning(this, tr("Init Project"), tr("Failed to initialize project."));
            }
        } else {
            if (!d->control.Initialize()) {
                TC::MessageDialog::warning(this, tr("Initialization"), tr("Failed to initialize."));
            }
        }

        // 각 패널 별로 현재상태에 맞게 UI 업데이트
        d->control.UpdateUI();

        return true;
    }

    void MainWindow::OnHandleEvent(const ctkEvent& ctkEvent) {
        Q_UNUSED(ctkEvent)
    }

    void MainWindow::onOpenDataNavigation() {
        TC::Framework::AppEvent appEvt(TC::Framework::AppTypeEnum::APP_WITH_ARGS);
        appEvt.setFullName("org.tomocube.data.navigation");
        appEvt.setAppName("Data Navigation");
        appEvt.addParameter("Project", d->control.GetCurrentProject());
        appEvt.addParameter("Experiment", d->control.GetCurrentExperiment());

        publishSignal(appEvt, "Experiment::Setup");
    }

    void MainWindow::onRunExperiment() {
        TC::Framework::AppEvent appEvt(TC::Framework::AppTypeEnum::APP_WITH_ARGS);
        appEvt.setFullName("org.tomocube.experiment.perform");
        appEvt.setAppName("Experiment Perform");

        if (d->control.IsRunningExperiment()) {
            if (d->control.IsRunningTimelapse()) {
                appEvt.setFullName("org.tomocube.experiment.timelapse");
                appEvt.setAppName("Experiment Timelapse");

                QLOG_INFO() << "Run the experiment timelapse [" << d->control.GetRunningProject() << "/" << d->control.GetRunningExperiment() << "]";
            }
        } else {
            const auto project = d->control.GetCurrentProject();
            const auto experiment = d->control.GetCurrentExperiment();

            if(project.isEmpty() || experiment.isEmpty()) {
                TC::MessageDialog::warning(this, tr("Run Experiment"), tr("Open an experiment."));
                return;
            }

            // windows title bar에 실험 이름 표시 요청
            TC::Framework::MenuEvent menuEvt(TC::Framework::MenuTypeEnum::TitleBar);
            menuEvt.scriptSet(experiment);
            publishSignal(menuEvt, "Experiment::Setup");

            // 사용할 프로젝트 이름과 실험 이름 전달
            appEvt.addParameter("Project", project);
            appEvt.addParameter("Experiment", experiment);

            QLOG_INFO() << "Run the experiment [" << project << "/" << experiment << "]";
        }

        publishSignal(appEvt, "Experiment::Setup");
    }

    void MainWindow::onGoToHome() {
        if (d->control.IsRunningExperiment()) {
            TC::MessageDialog::warning(this, tr("Unable to go to the home screen"), tr("Cannot go to the home screen while an experiment is in progress."));
            return;
        }

        d->control.ResetExperiment();

        TC::Framework::AppEvent appEvt(TC::Framework::AppTypeEnum::APP_WITH_ARGS);
        appEvt.setFullName("org.tomocube.general.start");
        appEvt.setAppName("Start");

        publishSignal(appEvt, "Experiment::Setup");
    }
}
