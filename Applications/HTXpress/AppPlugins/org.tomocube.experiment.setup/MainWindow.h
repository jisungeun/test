#pragma once
#include <memory>
#include <IMainWindowHTX.h>

namespace HTXpress::AppPlugins::Experiment::Setup::App {
    class MainWindow : public AppComponents::Framework::IMainWindowHTX {
        Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = nullptr);
        ~MainWindow() override;

        auto TryActivate() -> bool final;
        auto TryDeactivate() -> bool final;
        auto IsActivate() -> bool final;
        auto GetMetaInfo()->QVariantMap final;

        auto Execute(const QVariantMap& params) -> bool override;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    protected slots:
        void OnHandleEvent(const ctkEvent& ctkEvent);
        void onOpenDataNavigation();
        void onRunExperiment();
        void onGoToHome();

    private:
        auto InitUI(void)->bool;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
