#include "ExperimentPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    struct ExperimentPresenter::Impl {
        IExperimentView* view{ nullptr };
    };

    ExperimentPresenter::ExperimentPresenter(IExperimentView* view) : UseCase::IRunExperimentOutputPort(), d{new Impl} {
        d->view = view;
    }

    ExperimentPresenter::~ExperimentPresenter() {
    }

    auto ExperimentPresenter::GetInstance(IExperimentView* view) -> Pointer {
        static Pointer theInstance{ new ExperimentPresenter(view) };
        return theInstance;
    }

    auto ExperimentPresenter::UpdateProgress(const UseCase::ExperimentStatus& status) -> void {
        if(d->view) d->view->UpdateProgress(status);
    }

    auto ExperimentPresenter::NotifyStopped() -> void {
        if(d->view) d->view->NotifyStopped();
    }

    auto ExperimentPresenter::UpdateProgressTime(const int32_t& elapsedTimeInSec, const int32_t& remainTimeInSec) -> void {
        if (d->view) d->view->UpdateProgressTime(elapsedTimeInSec, remainTimeInSec);
    }

    auto ExperimentPresenter::UpdateCurrentPosition(const AppEntity::WellIndex& wellIndex, const AppEntity::Position& position) -> void {
        if (d->view) d->view->UpdatePosition(wellIndex, position);
    }
}
