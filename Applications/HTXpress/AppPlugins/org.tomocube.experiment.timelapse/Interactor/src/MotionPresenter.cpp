#include "MotionPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    struct MotionPresenter::Impl {
        IMotionView* view{ nullptr };
    };

    MotionPresenter::MotionPresenter(IMotionView* view) : UseCase::IMotionOutputPort(), d{new Impl} {
        d->view = view;
    }

    MotionPresenter::~MotionPresenter() {
    }

    auto MotionPresenter::UpdateStatus(bool moving) -> void {
        if(d->view) d->view->UpdateStatus(moving);
    }

    auto MotionPresenter::UpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position) -> void {
        if(d->view) d->view->UpdatePosition(wellIdx, position);
    }

    auto MotionPresenter::UpdateGlobalPosition(const AppEntity::Position& position) -> void {
        if(d->view) d->view->UpdateGlobalPosition(position);
    }

    auto MotionPresenter::UpdateSelectedWell(const AppEntity::WellIndex wellIdx) -> void {
        if(d->view) d->view->UpdateSelectedWell(wellIdx);
    }

    auto MotionPresenter::UpdateBestFocus(double posInMm) -> void {
        if(d->view) d->view->UpdateBestFocus(posInMm);
    }

    auto MotionPresenter::ReportError(const QString& message) -> void {
        if(d->view) d->view->ReportError(message);
    }

    auto MotionPresenter::ReportAFFailure() -> void {
        if(d->view) d->view->ReportAFFailure();
    }
}
