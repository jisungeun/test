#include <GetPosition.h>
#include <GetGlobalPosition.h>
#include <MoveSampleStage.h>

#include "MotionController.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    struct MotionController::Impl {
        MotionPresenter* presenter{ nullptr };
    };

    MotionController::MotionController(MotionPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    MotionController::~MotionController() {
    }

    auto MotionController::GetPosition(const AppEntity::WellIndex& wellIdx, AppEntity::Position& position) -> bool {
        auto usecase = UseCase::GetPosition(d->presenter);
        usecase.SetWell(wellIdx);
        if(usecase.Request()) {
            position = usecase.CurrentPosition();
            return true;
        }
        return false;
    }

    auto MotionController::GetGlobalPosition(AppEntity::Position& position) -> bool {
        auto usecase = UseCase::GetGlobalPosition(d->presenter);
        if(!usecase.Request()) return false;
        position = usecase.CurrentPosition();
        return true;
    }

    auto MotionController::GetGlobalPosition(AppEntity::Position& position, AppEntity::WellIndex& wellIdx) -> bool {
        auto usecase = UseCase::GetGlobalPosition(d->presenter);
        if(!usecase.Request()) return false;
        position = usecase.CurrentPosition();
        wellIdx = usecase.CurrentWell();
        return true;
    }

    auto MotionController::MoveXY(const AppEntity::WellIndex wellIdx, const double xInMM, const double yInMM) -> bool {
        auto usecase = UseCase::MoveSampleStage(d->presenter);
        usecase.SetTargetXYInUm(wellIdx, xInMM*1000, yInMM*1000);
        return usecase.Request();
    }

    auto MotionController::MoveMM(const AppEntity::Axis axis, const double posInMm) -> bool {
        auto usecase = UseCase::MoveSampleStage(d->presenter);
        usecase.SetTargetUm(axis, posInMm * 1000);
        return usecase.Request();
    }

    auto MotionController::MoveRelativePixels(int32_t relPosX, int32_t relPosY) -> bool {
        auto usecase = UseCase::MoveSampleStage(d->presenter);
        usecase.SetRelativeTargetPixels(relPosX, relPosY);
        return usecase.Request();
    }

    auto MotionController::MoveRelativeMM(const AppEntity::Axis axis, const double distMm) -> bool {
        auto usecase = UseCase::MoveSampleStage(d->presenter);
        usecase.SetRealtiveTargetUm(axis, distMm * 1000);
        return usecase.Request();
    }

    auto MotionController::MoveRelativeUM(const AppEntity::Axis axis, const double distUm) -> bool {
        auto usecase = UseCase::MoveSampleStage(d->presenter);
        usecase.SetRealtiveTargetUm(axis, distUm);
        return usecase.Request();
    }

    auto MotionController::StartJog(const AppEntity::Axis axis, bool plusDirection) -> bool {
        Q_UNUSED(axis)
        Q_UNUSED(plusDirection)
        return false;
    }

    auto MotionController::StopJog() -> bool {
        return false;
    }
}
