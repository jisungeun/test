#include "InstrumentPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    struct InstrumentPresenter::Impl {
        IInstrumentView* view{ nullptr };
    };

    InstrumentPresenter::InstrumentPresenter(IInstrumentView* view) : UseCase::IInstrumentOutputPort(), d{new Impl} {
        d->view = view;
    }

    InstrumentPresenter::~InstrumentPresenter() {
    }

    auto InstrumentPresenter::UpdateFailed(const QString& message) -> void {
        if(d->view) {
            d->view->UpdateFailed(message);
        }
    }

    auto InstrumentPresenter::UpdateProgress(double progress, const QString& message) -> void {
        if(d->view) {
            d->view->UpdateProgress(progress, message);
        }
    }

    auto InstrumentPresenter::UpdateGlobalPosition(const AppEntity::Position& position) -> void {
        if(d->view) {
            d->view->UpdateGlobalPosition(position);
        }
    }

    auto InstrumentPresenter::ReportAFFailed() -> void {
        if(d->view) {
            d->view->ReportAFFailed();
        }
    }

    auto InstrumentPresenter::UpdateBestFocus(double posInMm) -> void {
        if(d->view) {
            d->view->UpdateBestFocus(posInMm);
        }
    }

    auto InstrumentPresenter::AutoFocusEnabled(bool enable) -> void {
        if(d->view) {
            d->view->AutoFocusEnabled(enable);
        }
    }

    auto InstrumentPresenter::LiveStarted() -> void {
        if(d->view) {
            d->view->LiveStarted();
        }
    }

    auto InstrumentPresenter::LiveStopped() -> void {
        if(d->view) {
            d->view->LiveStopped();
        }
    }

    auto InstrumentPresenter::LiveImagingFailed(const QString& message) -> void {
        if(d->view) {
            d->view->LiveImagingFailed(message);
        }
    }

    auto InstrumentPresenter::VesselLoaded() -> void {
        if(d->view) {
            d->view->VesselLoaded();
        }
    }

    auto InstrumentPresenter::VesselUnloaded() -> void {
        if(d->view) {
            d->view->VesselUnloaded();
        }
    }

}
