#include "AcquisitionDataPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    struct AcquisitionDataPresenter::Impl {
        IAcquisitionDataView* view{ nullptr };
    };

    AcquisitionDataPresenter::AcquisitionDataPresenter(IAcquisitionDataView* view) : UseCase::IDataOutputPort(), d{new Impl} {
        d->view = view;
    }

    AcquisitionDataPresenter::~AcquisitionDataPresenter() {
    }

    auto AcquisitionDataPresenter::ScannedData(const QString& user, const QString& project, const QString& experiment) -> void {
        if (d->view) d->view->ScannedData(user, project, experiment);
    }

    auto AcquisitionDataPresenter::AddedData(const QString& fileFullPath)->void {
        if (d->view) d->view->AddedData(fileFullPath);
    }

    auto AcquisitionDataPresenter::UpdatedData(const QString& fileFullPath)->void {
        if (d->view) d->view->UpdatedData(fileFullPath);
    }

    auto AcquisitionDataPresenter::DeletedData(const QString& fileFullPath)->void {
        if (d->view) d->view->DeletedData(fileFullPath);
    }

    auto AcquisitionDataPresenter::DeletedDataRootFolder(const QString& fileFullPath) -> void {
        if (d->view) d->view->DeletedDataRootFolder(fileFullPath);
    }
}
