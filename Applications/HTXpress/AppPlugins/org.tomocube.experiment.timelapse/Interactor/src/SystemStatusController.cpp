#include "SystemStatusController.h"

#include <SetTimelapseRunning.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    struct SystemStatusController::Impl {
        
    };

    SystemStatusController::SystemStatusController() : d{ new Impl } {
        
    }
    
    SystemStatusController::~SystemStatusController() {
    }

    auto SystemStatusController::SetTimelapseRunning(bool isRunning) -> bool {
        auto usecase = UseCase::SetTimelapseRunning();
        usecase.SetRunning(isRunning);
        return usecase.Request();
    }

}
