#include "ImagingPlanPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    struct ImagingPlanPresenter::Impl {
        IImagingPlanView* view{ nullptr };
    };

    ImagingPlanPresenter::ImagingPlanPresenter(IImagingPlanView* view) : UseCase::IImagingConditionOutputPort(), d{new Impl} {
        d->view = view;
    }

    ImagingPlanPresenter::~ImagingPlanPresenter() {
    }

    auto ImagingPlanPresenter::Update(const UseCase::ImagingTimeTable::Pointer& table) -> void {
        if(d->view) {
            d->view->Update(table);
        }
    }

    auto ImagingPlanPresenter::UpdateFOV(const double xInUm, const double yInUm) -> void {
        if(d->view) {
            d->view->UpdateFOV(xInUm, yInUm);
        }
    }

    auto ImagingPlanPresenter::UpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex, 
                                                  double xInMm, double yInMm, 
                                                  double widthInUm, double heightInUm) -> void {
        if(d->view) {
            d->view->UpdateTileScanArea(enable, wellIndex, xInMm, yInMm, widthInUm, heightInUm);
        }
    }

    auto ImagingPlanPresenter::ClearFLChannels() -> void {
        if(d->view) {
            d->view->ClearFLChannels();
        }
    }

    auto ImagingPlanPresenter::UpdateBFEnabled(bool enabled) -> void {
        if(d->view) {
            d->view->UpdateBFEnabled(enabled);
        }
    }

    auto ImagingPlanPresenter::UpdateHTEnabled(bool enabled) -> void {
        if(d->view) {
            d->view->UpdateHTEnabled(enabled);
        }
    }

    auto ImagingPlanPresenter::UpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types) -> void {
        if(d->view) {
            d->view->UpdateEnabledImagingTypes(types);
        }
    }

    auto ImagingPlanPresenter::UpdateFLChannelConfig(const int32_t channel, ChannelConfig::Pointer config) -> void {
        if(d->view) {
            d->view->UpdateFLChannelConfig(channel, config);
        }
    }

    auto ImagingPlanPresenter::UpdateAcquisitionLock(bool locked) -> void {
        if(d->view) {
            d->view->UpdateAcquisitionLock(locked);
        }
    }

}
