#include <StartLiveView.h>
#include <ConnectLiveView.h>
#include <DisconnectLiveView.h>
#include <EnableAutoFocus.h>
#include <PerformAutofocus.h>
#include <SetBestFocus.h>
#include <LoadVessel.h>
#include <UnloadVessel.h>
#include <ConnectInstrument.h>

#include "InstrumentController.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    struct InstrumentController::Impl {
        InstrumentPresenter* presenter{ nullptr };
    };

    InstrumentController::InstrumentController(InstrumentPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    InstrumentController::~InstrumentController() {
    }

    auto InstrumentController::Connect() -> bool {
        auto usecase = UseCase::ConnectInstrument(d->presenter);
        return usecase.Request();
    }

    auto InstrumentController::StartLiveBF() -> bool {
        auto usecase = UseCase::StartLiveView(d->presenter);
        usecase.SetImageType(AppEntity::Modality::BF);
        return usecase.Request();
    }

    auto InstrumentController::StartLiveFL(int32_t channel) -> bool {
        auto usecase = UseCase::StartLiveView(d->presenter);
        usecase.SetImageType(AppEntity::Modality::FL, channel);
        return usecase.Request();
    }

    auto InstrumentController::LoadVessel() -> bool {
        auto usecase = UseCase::LoadVessel(d->presenter);
        return usecase.Request();
    }

    auto InstrumentController::UnloadVessel() -> bool {
        auto usecase = UseCase::UnloadVessel(d->presenter);
        return usecase.Request();
    }

    auto InstrumentController::PerformAutoFocus() -> bool {
        auto usecase = UseCase::PerformAutofocus(d->presenter);
        return usecase.Request();

    }

    auto InstrumentController::EnableAutoFocus() -> bool {
        auto usecase = UseCase::EnableAutoFocus(d->presenter);
        usecase.SetEnable(true);
        return usecase.Request();
    }

    auto InstrumentController::DisalbeAutoFocus() -> bool {
        auto usecase = UseCase::EnableAutoFocus(d->presenter);
        usecase.SetEnable(false);
        return usecase.Request();
    }

    auto InstrumentController::SetBestFocusCurrent() -> bool {
        auto usecase = UseCase::SetBestFocus(d->presenter);
        usecase.SetCurrent();
        return usecase.Request();
    }

    auto InstrumentController::SetBestFocusTarget(int32_t value) -> bool {
        auto usecase = UseCase::SetBestFocus(d->presenter);
        usecase.SetTarget(value);
        return usecase.Request();
    }

    auto InstrumentController::Recover() -> bool {
        auto usecase = UseCase::StartLiveView(d->presenter);
        usecase.SetLatest();
        return usecase.Request();
    }

    auto InstrumentController::ConnectLive() -> bool {
        auto usecase = UseCase::ConnectLiveView();
        return usecase.Request();
    }

    auto InstrumentController::DisconnectLive() -> bool {
        auto usecase = UseCase::DisconnectLiveView();
        return usecase.Request();
    }
}
