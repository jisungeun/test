#include <LoadExperiment.h>

#include "ExperimentIOController.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    struct ExperimentIOController::Impl {
        ExperimentIOPresenter* presenter{ nullptr };
    };

    ExperimentIOController::ExperimentIOController(ExperimentIOPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    ExperimentIOController::~ExperimentIOController() {
    }

    auto ExperimentIOController::Load(const QString& project, const QString& title, bool singleRun) -> bool {
        auto usecase = UseCase::LoadExperiment(d->presenter);
        usecase.SetExperiment(project, title);
        usecase.SetSingleRun(singleRun);
        return usecase.Request();
    }
}
