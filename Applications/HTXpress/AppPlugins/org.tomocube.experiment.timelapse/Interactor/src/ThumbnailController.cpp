﻿#include <SetImagingPoint.h>
#include <SetThumbnailImage.h>
#include <GetThumbnailImage.h>
#include <SetAcquireStartTimes.h>

#include "ThumbnailController.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    struct ThumbnailController::Impl {
        ThumbnailPresenter *presenter{nullptr};
    };

    ThumbnailController::ThumbnailController(ThumbnailPresenter* presenter) : d{std::make_unique<Impl>()} {
        d->presenter = presenter;
    }

    ThumbnailController::~ThumbnailController() = default;

    auto ThumbnailController::SetCurrentImagingPoint(const QString& wellPosition, const QString& pointID) -> bool {
        auto usecase = UseCase::SetImagingPoint(d->presenter);
        usecase.SetCurrentImagingPoint(wellPosition, pointID);
        return usecase.Request();
    }

    auto ThumbnailController::SetStartTimesInSec(const QMap<int32_t, int32_t>& startTimesInSec) -> bool {
        auto usecase = UseCase::SetAcquireStartTimes(d->presenter);
        usecase.SetStartTimesInSec(startTimesInSec);
        return usecase.Request();
    }

    auto ThumbnailController::SetThumbnailImage(const QString& dataPath, int32_t frameIndex, AppEntity::Modality modality, bool sync) -> bool {
        auto usecase = UseCase::SetThumbnailImage(d->presenter);
        usecase.SetSync(sync);
        usecase.SetDataPath(dataPath);
        usecase.SetFrameIndex(frameIndex);
        usecase.SetModality(modality);
        return usecase.Request();
    }

    auto ThumbnailController::GetThumbnailImage(const QString& dataPath, int32_t frameIndex, AppEntity::Modality modality, bool sync, QImage& outputImage) -> bool {
        auto usecase = UseCase::GetThumbnailImage(d->presenter);
        usecase.SetDataPath(dataPath);
        usecase.SetFrameIndex(frameIndex);
        usecase.SetModality(modality);
        usecase.SetSync(sync);
        if(usecase.Request()) {
            outputImage = usecase.GetImage();
            return true;
        }

        return false;
    }
}
