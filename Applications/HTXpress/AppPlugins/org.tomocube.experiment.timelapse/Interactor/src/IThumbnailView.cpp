﻿#include "IThumbnailView.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    IThumbnailView::IThumbnailView() = default;
    IThumbnailView::~IThumbnailView() = default;
}
