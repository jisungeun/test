﻿#include "ThumbnailPresenter.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    struct ThumbnailPresenter::Impl {
        IThumbnailView* view{nullptr};
    };

    ThumbnailPresenter::ThumbnailPresenter(IThumbnailView* view) : IThumbnailOutputPort(), d{std::make_unique<Impl>()} {
        d->view = view;
    }

    ThumbnailPresenter::~ThumbnailPresenter() = default;

    auto ThumbnailPresenter::UpdateImagingPoint(const QString& wellPosition, const QString& pointID) -> void {
        if (d->view) { d->view->UpdateImagingPoint(wellPosition, pointID); }
    }

    auto ThumbnailPresenter::UpdateThumbnailImage(int32_t frameIndex, AppEntity::Modality modality, const QImage& image) -> void {
        if (d->view) { d->view->UpdateThumbnailImage(frameIndex, modality, image); }
    }

    auto ThumbnailPresenter::UpdateStartTimesInSec(const QMap<int32_t, int32_t>& startTimesInSec) -> void {
        if (d->view) { d->view->UpdateStartTimesInSec(startTimesInSec); }
    }

    auto ThumbnailPresenter::ReportError(const QString& message) -> void {
        if (d->view) { d->view->ReportError(message); }
    }
}
