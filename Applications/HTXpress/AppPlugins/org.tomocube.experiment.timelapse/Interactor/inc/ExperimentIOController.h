#pragma once
#include <memory.h>
#include <QString>

#include "ExperimentIOPresenter.h"
#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API ExperimentIOController {
    public:
        ExperimentIOController(ExperimentIOPresenter* presenter);
        ~ExperimentIOController();

        auto Load(const QString& project, const QString& title, bool singleRun)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}