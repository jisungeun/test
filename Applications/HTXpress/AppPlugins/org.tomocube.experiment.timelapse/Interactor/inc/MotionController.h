#pragma once

#include <Position.h>

#include "MotionPresenter.h"
#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API MotionController {
    public:
        MotionController(MotionPresenter* presenter);
        ~MotionController();

        auto GetPosition(const AppEntity::WellIndex& wellIdx, AppEntity::Position& position)->bool;
        auto GetGlobalPosition(AppEntity::Position& position)->bool;
        auto GetGlobalPosition(AppEntity::Position& position, AppEntity::WellIndex& wellIdx)->bool;

        auto MoveXY(const AppEntity::WellIndex wellIdx, const double xInMM, const double yInMM)->bool;
        auto MoveMM(const AppEntity::Axis axis, const double posInMm)->bool;
        auto MoveRelativePixels(int32_t relPosX, int32_t relPosY)->bool;
        auto MoveRelativeMM(const AppEntity::Axis axis, const double distMm)->bool;
        auto MoveRelativeUM(const AppEntity::Axis axis, const double distUm)->bool;

        auto StartJog(const AppEntity::Axis axis, bool plusDirection)->bool;
        auto StopJog()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}