#pragma once

#include <memory>

#include <IDataOutputPort.h>

#include "IAcquisitionDataView.h"
#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API AcquisitionDataPresenter : public UseCase::IDataOutputPort {
    public:
        AcquisitionDataPresenter(IAcquisitionDataView* view);
        ~AcquisitionDataPresenter() override;

        auto ScannedData(const QString& user, const QString& project, const QString& experiment) -> void override;
        auto AddedData(const QString& fileFullPath)->void override;
        auto UpdatedData(const QString& fileFullPath)->void override;
        auto DeletedData(const QString& fileFullPath)->void override;
        auto DeletedDataRootFolder(const QString& fileFullPath) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}