#pragma once
#include <memory>

#include <Experiment.h>
#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API IExperimentIOView {
    public:
        IExperimentIOView();
        virtual ~IExperimentIOView();

        virtual auto Update(AppEntity::Experiment::Pointer experiment, bool singleRun)->void = 0;
        virtual auto Error(const QString& message)->void = 0;
    };
}