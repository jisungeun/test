#pragma once
#include <memory>

#include <IMotionOutputPort.h>
#include "IMotionView.h"
#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API MotionPresenter : public UseCase::IMotionOutputPort {
    public:
        MotionPresenter(IMotionView* view);
        ~MotionPresenter();

        auto UpdateStatus(bool moving) -> void override;
        auto UpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position) -> void override;
        auto UpdateGlobalPosition(const AppEntity::Position& position) -> void override;
        auto UpdateSelectedWell(const AppEntity::WellIndex wellIdx)->void override;
        auto UpdateBestFocus(double posInMm) -> void override;

        auto ReportError(const QString& message) -> void override;
        auto ReportAFFailure() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}