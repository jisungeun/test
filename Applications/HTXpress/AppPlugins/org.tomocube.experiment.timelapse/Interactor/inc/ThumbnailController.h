﻿#pragma once

#include <QImage>

#include <AppEntityDefines.h>

#include "ThumbnailPresenter.h"
#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API ThumbnailController {
    public:
        explicit ThumbnailController(ThumbnailPresenter* presenter);
        ~ThumbnailController();

        auto SetCurrentImagingPoint(const QString& wellPosition, const QString& pointID) -> bool;
        auto SetStartTimesInSec(const QMap<int32_t, int32_t>& startTimesInSec) -> bool;

        auto SetThumbnailImage(const QString& dataPath, 
                               int32_t frameIndex,
                               AppEntity::Modality modality,
                               bool sync) -> bool;

        auto GetThumbnailImage(const QString& dataPath, 
                               int32_t frameIndex,
                               AppEntity::Modality modality,
                               bool sync,
                               QImage& outputImage) -> bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
