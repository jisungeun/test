#pragma once
#include <memory>

#include <IRunExperimentOutputPort.h>

#include "IExperimentView.h"
#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API ExperimentPresenter : public UseCase::IRunExperimentOutputPort {
    private:
        using Pointer = std::shared_ptr<ExperimentPresenter>;

    protected:
        ExperimentPresenter(IExperimentView* view);

    public:
        ~ExperimentPresenter() override;
        static auto GetInstance(IExperimentView* view)->Pointer;

        auto UpdateProgress(const UseCase::ExperimentStatus& status) -> void override;
        auto NotifyStopped() -> void override;
        auto UpdateProgressTime(const int32_t& elapsedTimeInSec, const int32_t& remainTimeInSec) -> void override;
        auto UpdateCurrentPosition(const AppEntity::WellIndex& wellIndex, const AppEntity::Position& position) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}