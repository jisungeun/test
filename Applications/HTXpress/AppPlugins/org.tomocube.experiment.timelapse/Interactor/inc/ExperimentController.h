#pragma once
#include <memory>

#include "ExperimentPresenter.h"
#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API ExperimentController {
    public:
        ExperimentController(ExperimentPresenter* presenter);
        ~ExperimentController();

        auto RunExperiment()->bool;
        auto TestExperiment()->bool;
        auto StopExperiment()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}