#pragma once
#include <memory>

#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API SystemStatusController {
    public:
        SystemStatusController();
        ~SystemStatusController();

        auto SetTimelapseRunning(bool isRunning)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}