#pragma once
#include <memory>

#include <ExperimentStatus.h>
#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API IExperimentView {
    public:
        IExperimentView();
        virtual ~IExperimentView();

        virtual auto UpdateProgress(const UseCase::ExperimentStatus& status) -> void = 0;
        virtual auto NotifyStopped() -> void = 0;
        virtual auto UpdateProgressTime(const int32_t& elapsedTimeInSec, const int32_t& remainTimeInSec) -> void = 0;
        virtual auto UpdatePosition(const AppEntity::WellIndex& wellIndex, const AppEntity::Position& position) -> void = 0;
    };
}