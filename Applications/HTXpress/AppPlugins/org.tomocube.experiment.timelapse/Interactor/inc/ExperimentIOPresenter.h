#pragma once
#include <memory.h>

#include <IExperimentOutputPort.h>

#include "IExperimentIOView.h"
#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API ExperimentIOPresenter : public UseCase::IExperimentOutputPort {
    public:
        ExperimentIOPresenter(IExperimentIOView* view);
        ~ExperimentIOPresenter() override;

        auto Update(AppEntity::Experiment::Pointer experiment, bool singleRun) -> void override;
        auto Error(const QString& message) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}