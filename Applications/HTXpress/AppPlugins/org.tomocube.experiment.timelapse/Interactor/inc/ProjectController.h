#pragma once
#include <memory>
#include <QStringList>

#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API ProjectController {
    public:
        ProjectController();
        ~ProjectController();

        auto GetProjects() const->QStringList;
        auto GetExperiments(const QString& project) const->QStringList;
    };
}
