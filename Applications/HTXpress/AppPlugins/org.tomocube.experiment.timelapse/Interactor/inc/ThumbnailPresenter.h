﻿#pragma once

#include <memory>

#include <QImage>

#include <IThumbnailOutputPort.h>

#include "IThumbnailView.h"
#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API ThumbnailPresenter : public UseCase::IThumbnailOutputPort {
    public:
        explicit ThumbnailPresenter(IThumbnailView* view);
        ~ThumbnailPresenter() override;

        auto UpdateImagingPoint(const QString& wellPosition, const QString& pointID) -> void override;
        auto UpdateThumbnailImage(int32_t frameIndex, AppEntity::Modality modality, const QImage& image) -> void override;
        auto UpdateStartTimesInSec(const QMap<int32_t, int32_t>& startTimesInSec) -> void override;

        auto ReportError(const QString& message) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
