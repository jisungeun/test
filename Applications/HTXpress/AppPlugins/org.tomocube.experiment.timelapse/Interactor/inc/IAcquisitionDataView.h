#pragma once

#include <QString>

#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API IAcquisitionDataView {
    public:
        IAcquisitionDataView();
        virtual ~IAcquisitionDataView();

        virtual auto ScannedData(const QString& user, const QString& project, const QString& experiment)->void = 0;
        virtual auto AddedData(const QString& fileFullPath)->void = 0;
        virtual auto UpdatedData(const QString& fileFullPath)->void = 0;
        virtual auto DeletedData(const QString& fileFullPath)->void = 0;
        virtual auto DeletedDataRootFolder(const QString& fileFullPath)->void = 0;
    };
}