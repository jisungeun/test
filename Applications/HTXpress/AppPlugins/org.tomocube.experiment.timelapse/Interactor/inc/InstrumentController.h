#pragma once
#include <memory>

#include <InstrumentPresenter.h>
#include "HTX_Experiment_Timelapse_InteractorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Interactor {
    class HTX_Experiment_Timelapse_Interactor_API InstrumentController {
    public:
        InstrumentController(InstrumentPresenter* presenter = nullptr);
        ~InstrumentController();

        auto Connect()->bool;

        auto StartLiveBF()->bool;
        auto StartLiveFL(int32_t channel)->bool;
        auto Recover()->bool;
        auto ConnectLive()->bool;
        auto DisconnectLive()->bool;

        auto LoadVessel()->bool;
        auto UnloadVessel()->bool;

        auto PerformAutoFocus()->bool;
        auto EnableAutoFocus()->bool;
        auto DisalbeAutoFocus()->bool;
        auto SetBestFocusCurrent()->bool;
        auto SetBestFocusTarget(int32_t value)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}