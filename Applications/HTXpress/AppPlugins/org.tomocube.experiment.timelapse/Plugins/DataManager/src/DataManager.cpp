#define LOGGER_TAG "[DataManager]"

#include <TCLogger.h>
#include <HTXDataManager.h>

#include "DataManager.h"
#include "DataManagerObserver.h"

using HTXpress::AppComponents::HTXDataManager::HTXDataManager;

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::DataManager {
    struct DataManager::Impl {
        DataManagerObserver* observer{ nullptr };

        UseCase::IDataOutputPort* outputPort{ nullptr };
        QList<UseCase::IDataOutputPort*> scannerPorts;
        QList<UseCase::IDataOutputPort*> monitorPorts;

        struct DataInfo {
            QString user;
            QString project;
            QString experiment;
        };

        DataInfo scanInfo;
    };

    DataManager::DataManager() : UseCase::IDataManager(), d{ new Impl } {
        d->observer = new DataManagerObserver;

        connect(d->observer, &DataManagerObserver::sigUpdateExperiment, this, &DataManager::onScanData);
        connect(d->observer, &DataManagerObserver::sigDataAdded, this, &DataManager::onAddData);
        connect(d->observer, &DataManagerObserver::sigDataDeleted, this, &DataManager::onDeleteData);
        connect(d->observer, &DataManagerObserver::sigDataUpdated, this, &DataManager::onUpdateData);
        connect(d->observer, &DataManagerObserver::sigDataRootFolderDeleted, this, &DataManager::onDeleteDataRootFolder);
    }

    DataManager::~DataManager() {
    }
    
    auto DataManager::SetScanExperiment(const QString& user, const QString& project, const QString& experiment) -> void {
        if (user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;

        // Experiment Perform에서는 한 번에 하나의 실험 데이터 경로만 확인하므로,
        // 이전에 스캔하던 실험 데이터 경로는 제거하고 새로운 실험 데이터 경로 추가
        AppComponents::HTXDataManager::HTXDataManager::GetInstance()->RemoveScanExperiment(d->scanInfo.user, d->scanInfo.project, d->scanInfo.experiment);
        AppComponents::HTXDataManager::HTXDataManager::GetInstance()->AppendScanExperiment(user, project, experiment);

        AppComponents::HTXDataManager::HTXDataManager::GetInstance()->RemoveMonitorExperiment(d->scanInfo.user, d->scanInfo.project, d->scanInfo.experiment);
        AppComponents::HTXDataManager::HTXDataManager::GetInstance()->AppendMonitorExperiment(user, project, experiment);

        d->scanInfo.user = user;
        d->scanInfo.project = project;
        d->scanInfo.experiment = experiment;
    }

    auto DataManager::SetOutputPort(UseCase::IDataOutputPort* outputPort)->void {
        d->outputPort = outputPort;
    }

    auto DataManager::InstallDataScannerOutputPort(UseCase::IDataOutputPort* outputPort) -> void {
        d->scannerPorts.append(outputPort);
    }

    auto DataManager::UninstallDataScannerOutputPort(UseCase::IDataOutputPort* outputPort) -> void {
        d->scannerPorts.removeOne(outputPort);
    }

    auto DataManager::InstallDataMonitorOutputPort(UseCase::IDataOutputPort* outputPort)->void {
        d->monitorPorts.append(outputPort);
    }

    auto DataManager::UninstallDataMonitorOutputPort(UseCase::IDataOutputPort* outputPort)->void {
        d->monitorPorts.removeOne(outputPort);
    }

    void DataManager::onScanData(const QString& user, const QString& project, const QString& experiment) {
        for(auto output : d->monitorPorts) {
            output->ScannedData(user, project, experiment);
        }
    }

    void DataManager::onAddData(const QString& fileFullPath) {
        for(auto output : d->monitorPorts) {
            output->AddedData(fileFullPath);
        }
    }
    
    void DataManager::onDeleteData(const QString& fileFullPath) {
        for (auto output : d->monitorPorts) {
            output->DeletedData(fileFullPath);
        }
    }

    void DataManager::onUpdateData(const QString& fileFullPath) {
        for (auto output : d->monitorPorts) {
            output->UpdatedData(fileFullPath);
        }
    }

    void DataManager::onDeleteDataRootFolder(const QString& fileFullPath) {
        for (auto output : d->monitorPorts) {
            output->DeletedDataRootFolder(fileFullPath);
        }
    }
}
