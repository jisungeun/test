#define LOGGER_TAG "[LiveImageAcquisition]"

#include <QMutex>
#include <QVector>
#include <QRgb>
#include <QFile>
#include <QDataStream>

#include <TCLogger.h>

#include "ImageQueue.h"
#include "LiveImageAcquisition.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::LiveImageAcquisition {
    struct Plugin::Impl {
        ImageQueue::Pointer queue{ new ImageQueue() };
        QVector<QRgb> colormap{ QVector<QRgb>(256) };
        QMutex mutex;
    };

    Plugin::Plugin() : UseCase::ILiveImageAcquisition(), d{new Impl} {
    }

    Plugin::~Plugin() {
        QLOG_INFO() << "Destroyed";
    }

    auto Plugin::GetImagePort() const -> UseCase::IImagePort::Pointer {
        return d->queue;
    }

    auto Plugin::Pause() -> void {
        QMutexLocker locker(&d->mutex);
        d->queue->Pause();
    }

    auto Plugin::Resume() -> void {
        QMutexLocker locker(&d->mutex);
        d->queue->Resume();
    }

    auto Plugin::SetMaxColor(const QRgb& color) -> void {
        QMutexLocker locker(&d->mutex);

        const auto maxR = static_cast<double>(qRed(color));
        const auto maxG = static_cast<double>(qGreen(color));
        const auto maxB = static_cast<double>(qBlue(color));

        for (int i = 0; i < 256; i++) {
            const auto colorR = std::min<int32_t>(255, (maxR*i)/255);
            const auto colorG = std::min<int32_t>(255, (maxG*i)/255);
            const auto colorB = std::min<int32_t>(255, (maxB*i)/255);
            d->colormap[i] = qRgb(colorR, colorG, colorB);
        }
    }

    auto Plugin::GetLatest(QImage& image) const -> bool {
        QMutexLocker locker(&d->mutex);

        const auto rawImage = d->queue->PopLatest();
        if (!rawImage)
            return false;

        try {
            QImage qImage(rawImage->GetBuffer().get(), rawImage->GetSizeX(), rawImage->GetSizeY(), QImage::Format_Indexed8);
            qImage.setColorTable(d->colormap);
            image = qImage.copy();
        } catch (const std::exception& e) {
            QLOG_ERROR()
                << "[Timelapse] Failed to create or copy QImage:" << e.what()
                << "rawImage's Size/IsBufferNull:" << rawImage->GetSizeX() << "x" << rawImage->GetSizeY() << (rawImage->GetBuffer() == nullptr)
                << ", image's Size/Bytes/Address:" << image.size() << image.sizeInBytes() << &image;
            return false;
        } catch (...) {
            QLOG_ERROR()
                << "[Timelapse] Failed to create or copy QImage!"
                << "rawImage's Size/IsBufferNull:" << rawImage->GetSizeX() << "x" << rawImage->GetSizeY() << (rawImage->GetBuffer() == nullptr)
                << ", image's Size/Bytes/Address:" << image.size() << image.sizeInBytes() << &image;
            return false;
        }

        return true;
    }
}
