#pragma once
#include <memory>
#include <IImagePort.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::LiveImageAcquisition {
    class ImageQueue : public UseCase::IImagePort {
    public:
        using Pointer = std::shared_ptr<ImageQueue>;

    public:
        ImageQueue();
        ~ImageQueue() override;

        auto Send(AppEntity::RawImage::Pointer image) -> void override;
        auto Clear() -> void override;
        auto IsEmpty() const -> bool override;

        auto ResetCount() -> void override;
        auto GetCount() const -> uint64_t override;

        auto PopLatest()->AppEntity::RawImage::Pointer;

        auto Pause()->void;
        auto Resume()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}