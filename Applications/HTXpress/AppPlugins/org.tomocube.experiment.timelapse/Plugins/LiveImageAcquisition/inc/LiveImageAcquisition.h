#pragma once
#include <memory>
#include <QImage>

#include <ILiveImageAcquisition.h>
#include "HTX_Experiment_Timelapse_LiveImageAcquisitionExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::LiveImageAcquisition {
    class HTX_Experiment_Timelapse_LiveImageAcquisition_API Plugin : public UseCase::ILiveImageAcquisition {
    public:
        Plugin();
        ~Plugin() override;

        auto GetImagePort() const -> UseCase::IImagePort::Pointer override;

        auto Pause() -> void override;
        auto Resume() -> void override;

        auto SetMaxColor(const QRgb& color) -> void override;
        auto GetLatest(QImage& image) const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}