#include <AcquisitionSequenceWriterIni.h>

#include "SequenceWriter.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ExperimentRunner {
    using Writer = TC::IO::AcquisitionSequence::AcquisitionSequenceWriterIni;
    using AcqSequence = TC::IO::AcquisitionSequence::AcquisitionSequence;
    using AcqFrame = TC::IO::AcquisitionSequence::AcquisitionFrame;
    using AcqModality = TC::IO::AcquisitionSequence::Modality;
    using AcqType = TC::IO::AcquisitionSequence::AcquisitionType;

    struct SequenceWriter::Impl {
        AppEntity::Experiment::Pointer experiment;
        int32_t vesselIdx;
        QList<AppEntity::PositionGroup> acqPositions;
        QString outputPath;

        Writer writer;
        AcqSequence acqSequence;

        auto Process()->void;
        auto Generate(const QString& path)->void;
    };

    auto SequenceWriter::Impl::Process() -> void {
        const auto scenario = experiment->GetScenario(vesselIdx);
        const auto seqCount = scenario->GetCount();

        int32_t timeFrameIndex = 0;
        for(auto seqIdx=0; seqIdx<seqCount; seqIdx++) {
            const auto sequence = scenario->GetSequence(seqIdx);
            const auto modalityCount = sequence->GetModalityCount();
            const auto timeCount = sequence->GetTimeCount();

            AcqFrame acqFrame;

            for(auto condIdx=0; condIdx < modalityCount; condIdx++) {
                const auto imageCond = sequence->GetImagingCondition(condIdx);
                const auto modality = imageCond->GetModality();

                switch(modality) {
                case AppEntity::Modality::HT:
                    acqFrame.AddInfo({AcqModality::HT, imageCond->Is3D()?AcqType::Dimension3:AcqType::Dimension2});
                    break;
                case AppEntity::Modality::BF: {
                    auto bfCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionBF>(imageCond);
                    acqFrame.AddInfo({AcqModality::BF, bfCond->GetColorMode()?AcqType::Color:AcqType::Gray});
                    }
                    break;
                case AppEntity::Modality::FL: {
                    auto flCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(imageCond);
                    auto channels = flCond->GetChannels();
                    for(auto channel : channels) {
                        auto acqModality = AcqModality::_from_integral(AcqModality::FLCH0 + channel);
                        acqFrame.AddInfo({acqModality, flCond->Is3D()?AcqType::Dimension3:AcqType::Dimension2});
                    }
                    }
                    break;
                }
            }

            for(auto timeIdx=0; timeIdx<timeCount; timeIdx++, timeFrameIndex++) {
                acqSequence.AddFrame(timeFrameIndex, acqFrame);
            }
        }

        acqSequence.SetTimeFrameCount(timeFrameIndex);

        writer.SetAcquisitionSequence(acqSequence);
    }

    auto SequenceWriter::Impl::Generate(const QString& path) -> void {
        writer.SetIniFilePath(path);
        writer.Write();
    }

    SequenceWriter::SequenceWriter() : d{new Impl} {
    }

    SequenceWriter::~SequenceWriter() {
    }

    auto SequenceWriter::SetOutputPath(const QString& outputPath) -> void {
        d->outputPath = outputPath;
    }

    auto SequenceWriter::SetPositions(const QList<AppEntity::PositionGroup>& positions) -> void {
        d->acqPositions = positions;
    }

    auto SequenceWriter::SetExperiment(const AppEntity::Experiment::Pointer experiment, int32_t vesselIdx) -> void {
        d->experiment = experiment;
        d->vesselIdx = vesselIdx;
    }

    auto SequenceWriter::Perform() -> bool {
        d->Process();

        for(auto posIter = d->acqPositions.begin(); posIter != d->acqPositions.end(); posIter++) {
            const auto path = QString("%1/%2/sequence.dat").arg(d->outputPath).arg(posIter->GetTitle());
            d->Generate(path);
        }

        return true;
    }
}
