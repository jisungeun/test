﻿#pragma once
#include <memory>

#include <QString>

#include <Vessel.h>
#include <PositionGroup.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ExperimentRunner {
    class VesselWriter final {
    public:
        VesselWriter();
        ~VesselWriter();

        auto SetOutputPath(const QString& path) -> void;
        auto SetVessel(const AppEntity::Vessel::Pointer& vessel) -> void;
        auto SetPositions(const QList<AppEntity::PositionGroup>& positions) -> void;

        auto Perform() -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
