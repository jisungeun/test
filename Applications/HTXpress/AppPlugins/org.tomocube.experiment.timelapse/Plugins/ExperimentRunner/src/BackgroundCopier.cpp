#include <QDir>
#include <FileUtility.h>

#include "BackgroundCopier.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ExperimentRunner {
    struct BackgroundCopier::Impl {
        QString sourcePath;
        QString outputPath;
        QList<AppEntity::PositionGroup> positions;
    };
    
    BackgroundCopier::BackgroundCopier() : d{new Impl} {
    }
    
    BackgroundCopier::~BackgroundCopier() {
    }
    
    auto BackgroundCopier::SetSourcePath(const QString& sourcePath)->void {
        d->sourcePath = sourcePath;
    }
    
    auto BackgroundCopier::SetOutputPath(const QString& outputPath)->void {
        d->outputPath = outputPath;
    }
    
    auto BackgroundCopier::SetPositions(const QList<AppEntity::PositionGroup>& positions)->void {
        d->positions = positions;
    }

    auto BackgroundCopier::Perform()->bool {
        for(auto posIter = d->positions.begin(); posIter != d->positions.end(); posIter++) {
            const auto path = QString("%1/%2/bgImages").arg(d->outputPath).arg(posIter->GetTitle());
            QDir().mkpath(path);

            TC::CopyFiles(d->sourcePath, path, false);
        }

        return true;
    }
}