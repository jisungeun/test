#include <TCLogger.h>
#include "Task.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ExperimentRunner {
    struct Task::Impl {
        ImagingSequence::Pointer sequence;
        int32_t timeIndex;
        int32_t startTimeMSec;
        
        Impl(const ImagingSequence::Pointer& _sequence, 
             int32_t _timeIndex, 
             int32_t _startTimeMSec)
            : sequence{_sequence}
            , timeIndex{_timeIndex}
            , startTimeMSec{_startTimeMSec} {
         }
    };
    
    Task::Task(const ImagingSequence::Pointer& sequence, 
               int32_t timeIndex, 
               int32_t startTimeMSec)
               : d{ new Impl(sequence, timeIndex, startTimeMSec) } {
    }

    Task::~Task() {
    }

    auto Task::GetImagingSequence() const -> ImagingSequence::Pointer {
        return d->sequence;
    }

    auto Task::GetTimeIndex() const -> int32_t {
        return d->timeIndex;
    }

    auto Task::GetStartTimeMSec() const -> int32_t {
        return d->startTimeMSec;
    }
}
