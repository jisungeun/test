#pragma once
#include <memory>
#include <QString>

#include <Experiment.h>
#include <PositionGroup.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ExperimentRunner {
    class CilsMetafileWriter {
    public:
        CilsMetafileWriter();
        ~CilsMetafileWriter();

        auto SetOutputPath(const QString& outputPath)->void;
        auto SetExperiment(const QString& project, const AppEntity::Experiment::Pointer experiment)->void;
        auto SetPositions(const QList<AppEntity::PositionGroup>& positions) -> void;

        auto Perform()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}