#pragma once
#include <memory>
#include <QMap>

#include <Vessel.h>
#include <Area.h>
#include <PositionGroup.h>
#include <Location.h>
#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ExperimentRunner {
    class ConfigWriter {
    public:
        using WellIndex = AppEntity::WellIndex;
        using Location = AppEntity::Location;
        using LocationIndex = AppEntity::LocationIndex;

    public:
        ConfigWriter();
        ~ConfigWriter();

        auto SetTitle(const QString& title)->void;
        auto SetOutputPath(const QString& outputPath)->void;
        auto SetExperiment(const AppEntity::Experiment::Pointer experiment, int32_t vesselIdx)->void;
        auto SetPositions(const QList<AppEntity::PositionGroup>& positions)->void;
        auto SetFOV(const int32_t fovX, const int32_t fovY, const int32_t offsetX, const int32_t offsetY)->void;

        auto Perform()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}