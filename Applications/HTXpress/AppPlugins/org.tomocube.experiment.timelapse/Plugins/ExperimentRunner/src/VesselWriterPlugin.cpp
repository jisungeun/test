﻿#define LOGGER_TAG "[VesselWriter]"

#include <TCLogger.h>
#include <VesselWriter.h>

#include "VesselWriterPlugin.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ExperimentRunner {
    struct VesselWriter::Impl {
        QString outputPath{};
        AppEntity::Vessel::Pointer vessel{};
        QList<AppEntity::PositionGroup> positions{};

        auto IsCompleted() const -> bool;
    };

    auto VesselWriter::Impl::IsCompleted() const -> bool {
        if (outputPath.isEmpty()) return false;
        if (!vessel) return false;
        if (positions.isEmpty()) return false;

        return true;
    }

    VesselWriter::VesselWriter() : d{std::make_unique<Impl>()} {
    }

    VesselWriter::~VesselWriter() = default;

    auto VesselWriter::SetOutputPath(const QString& path) -> void {
        d->outputPath = path;
    }

    auto VesselWriter::SetVessel(const AppEntity::Vessel::Pointer& vessel) -> void {
        d->vessel = vessel;
    }

    auto VesselWriter::SetPositions(const QList<AppEntity::PositionGroup>& positions) -> void {
        d->positions = positions;
    }

    auto VesselWriter::Perform() -> bool {
        if (!d->IsCompleted()) {
            QLOG_ERROR() << "The operation could not be completed due to incomplete parameter configuration.";
            return false;
        }

        const QString fileName{".vessel"};

        for (const auto& position : d->positions) {
            const AppComponents::VesselIO::VesselWriter writer;
            const auto fullPath = QString("%1/%2/%3").arg(d->outputPath).arg(position.GetTitle()).arg(fileName);

            if (!writer.Write(fullPath, d->vessel)) {
                QLOG_ERROR() << "The vessel file could not be saved to the specified location:" << fullPath;
            }
        }
        return true;
    }
}
