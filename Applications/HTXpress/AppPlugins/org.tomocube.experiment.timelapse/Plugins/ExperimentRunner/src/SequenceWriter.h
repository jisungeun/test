#pragma once
#include <memory>
#include <QString>

#include <PositionGroup.h>
#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ExperimentRunner {
    class SequenceWriter {
    public:
        SequenceWriter();
        ~SequenceWriter();

        auto SetOutputPath(const QString& outputPath)->void;
        auto SetPositions(const QList<AppEntity::PositionGroup>& positions)->void;
        auto SetExperiment(const AppEntity::Experiment::Pointer experiment, int32_t vesselIdx)->void;

        auto Perform()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}