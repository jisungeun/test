#define LOGGER_TAG "[InstrumentPlugin]"
#include <TCLogger.h>
#include <InstrumentFactory.h>
#include <System.h>
#include <SystemStatus.h>
#include <ModelRepo.h>
#include <ImagingParameter.h>

#include "ImageAcquisitionPort.h"
#include "InstrumentPlugin.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::Instrument {
    using InstrumentComponent = AppComponents::Instrument::Instrument;
    using InstrumentConfig = AppComponents::Instrument::Config;
    using RawPosition = AppComponents::Instrument::RawPosition;
    using RawAxis = AppComponents::Instrument::Axis;

    struct Instrument::Impl {
        InstrumentComponent::Pointer instrument{ nullptr };
        ImageAcquisitionPort imageAcqPort;
        QString errorMessage{ "Not specified" };   //TODO 에러 발생하면 메세지 채우도록 하기

        struct {
            int32_t minInterval{ 8000 };
            int32_t maxInterval{ 30000000 };
            int32_t minIdle{ 1000 };
        } condition;

        struct {
            AppEntity::Modality modality{ AppEntity::Modality::BF };
            int32_t channel{ 0 };
        } latestLive;

        auto SetError(const QString& error)->void;
        auto LoadInstrument()->void;
        auto CalcExposure(int32_t exposure)->int32_t;
        auto Adjust(int32_t interval, int32_t exposure)->int32_t;
        auto ActiveFLChannels(const AppEntity::ImagingSequence::Pointer sequence) const->QList<int32_t>;
        auto ConvAxis(const RawAxis& in)->AppEntity::Axis;
        auto ConvAxis(const AppEntity::Axis& axis)->RawAxis;
        auto IsSamePosition(AppEntity::Axis axis, RawPosition lhs, RawPosition rhs)->bool;
    };

    auto Instrument::Impl::SetError(const QString& error) -> void {
        errorMessage = error;
    }

    auto Instrument::Impl::LoadInstrument() -> void {
        using namespace AppComponents::Instrument;
        if(instrument) return;

        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto model = AppEntity::ModelRepo::GetInstance()->GetModel(sysConfig->GetModel());
        instrument = InstrumentFactory::GetInstance();

        //Do not call SetConfig here, instrument instance is initialized outside of this app

        condition.minInterval = model->MinimumIntervalUSec();
        condition.maxInterval = model->MaximumIntervalUSec();
        condition.minIdle = model->MinimumIdleUSec();
    }

    auto Instrument::Impl::CalcExposure(int32_t exposure) -> int32_t {
        auto model = AppEntity::System::GetModel();
        const auto minExposure = model->MinimumExposureUSec();
        return std::max(minExposure, exposure);
    }

    auto Instrument::Impl::Adjust(int32_t interval, int32_t exposure) -> int32_t {
        const auto minInterval = std::max(condition.minInterval, exposure + condition.minIdle);
        const auto maxInterval = std::min(interval, condition.maxInterval);
        return std::max(minInterval, maxInterval);
    }

    auto Instrument::Impl::ActiveFLChannels(const AppEntity::ImagingSequence::Pointer sequence) const -> QList<int32_t> {
        const auto modalityCount = sequence->GetModalityCount();
        for(int idx=0; idx<modalityCount; idx++) {
            const auto cond = sequence->GetImagingCondition(idx);
            if(!cond->CheckModality(AppEntity::Modality::FL)) continue;

            const auto flCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(cond);
            return flCond->GetChannels();
        }

        return QList<int32_t>{};
    }

    auto Instrument::Impl::ConvAxis(const RawAxis& axis) -> AppEntity::Axis {
        switch(axis) {
        case RawAxis::AxisX:
            return AppEntity::Axis::X;
        case RawAxis::AxisY:
            return AppEntity::Axis::Y;
        case RawAxis::AxisZ:
            return AppEntity::Axis::Z;
        case RawAxis::AxisC:
            return AppEntity::Axis::C;
        case RawAxis::AxisD:
            return AppEntity::Axis::D;
        case RawAxis::AxisF:
            return AppEntity::Axis::F;
        }
        return AppEntity::Axis::X;
    }

    auto Instrument::Impl::ConvAxis(const AppEntity::Axis& axis) -> RawAxis {
        switch(axis) {
        case AppEntity::Axis::X:
            return RawAxis::AxisX;
        case AppEntity::Axis::Y:
            return RawAxis::AxisY;
        case AppEntity::Axis::Z:
            return RawAxis::AxisZ;
        case AppEntity::Axis::C:
            return RawAxis::AxisC;
        case AppEntity::Axis::F:
            return RawAxis::AxisF;
        case AppEntity::Axis::D:
            return RawAxis::AxisD;
        }
        return RawAxis::AxisX;
    }

    auto Instrument::Impl::IsSamePosition(AppEntity::Axis axis, RawPosition lhs, RawPosition rhs) -> bool {
        const auto model = AppEntity::System::GetModel();
        const auto resolution = model->AxisResolutionPPM(axis);
        const auto lhsPulses = static_cast<int32_t>(round(lhs * resolution));
        const auto rhsPulses = static_cast<int32_t>(round(rhs * resolution));
        return lhsPulses == rhsPulses;
    }

    Instrument::Instrument() : UseCase::IInstrument(), d{new Impl} {
    }

    Instrument::~Instrument() {
        d->instrument->UninstallImagePort(AppComponents::Instrument::ImageSource::ImagingCamera, &d->imageAcqPort);
        QLOG_INFO() << "Destroyed";
    }

    auto Instrument::Initialize() -> bool {
        d->LoadInstrument();

        d->instrument->InstallImagePort(AppComponents::Instrument::ImageSource::ImagingCamera, &d->imageAcqPort);
        return true;
    }

    auto Instrument::IsInitialized() -> bool {
        return d->instrument->CheckInitialized();
    }

    auto Instrument::CleanUp() -> void {
        if(!d->instrument) return;
        d->instrument->CleanUp();
    }

    auto Instrument::InstallImagePort(UseCase::IImagePort::Pointer port) -> void {
        d->imageAcqPort.InstallImagePort(port);
    }

    auto Instrument::UninstallImagePort(UseCase::IImagePort::Pointer port) -> void {
        d->imageAcqPort.UninstallImagePort(port);
    }

    auto Instrument::ImageCountInBuffer() const -> int32_t {
        return d->instrument->GetRemainCount(AppComponents::Instrument::ImageSource::ImagingCamera);
    }

    auto Instrument::ChangeFOV(int32_t xPixels, int32_t yPixels, int32_t& offsetX, int32_t& offsetY) -> bool {
        return d->instrument->ChangeFOV(xPixels, yPixels, offsetX, offsetY);
    }

    auto Instrument::ChangeFullFOV() -> bool {
        auto model = AppEntity::System::GetModel();
        int32_t offsetX, offsetY;
        return ChangeFOV(model->CameraPixelsH(), model->CameraPixelsV(), offsetX, offsetY);
    }

    auto Instrument::ChangeLiveModality(AppEntity::Modality modality, int32_t channel) -> bool {
        return StartLive(modality, channel);
    }

    auto Instrument::GetCurrentLiveModality() const -> std::tuple<AppEntity::Modality, int32_t> {
        return std::make_tuple(d->latestLive.modality, d->latestLive.channel);
    }

    auto Instrument::RunImagingSequence(const AppEntity::ImagingSequence::Pointer sequence,
                                        const QList<AppEntity::PositionGroup>& positions,
                                        const AppEntity::WellIndex startingWellIndex,
                                        const double focusReadyMM,
                                        const bool useMultiDishHolder) -> bool {
        using Modality = AppEntity::Modality;
        using ImagingType = AppEntity::ImagingType;
        using TriggerType = AppEntity::TriggerType;

        const auto model = AppEntity::System::GetModel();
        const auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto NA = sysStatus->GetNA();
        QLOG_INFO() << "NA: " << NA;
        QLOG_INFO() << "Position groups: " << positions.size();
        QLOG_INFO() << "Imaging conditions: " << sequence->GetModalityCount();
        QLOG_INFO() << "Time count: " << sequence->GetTimeCount() << " Interval=" << sequence->GetInterval();

        const auto htPattern3D = model->GetIlluminationPattern({ImagingType::HT3D, TriggerType::Trigger, sysConfig->GetHTIlluminationPattern(NA)});
        d->instrument->SetAcquisitionPattenIndex(ImagingType::HT3D, 
                                                 TriggerType::Trigger,
                                                 {htPattern3D});

        d->instrument->SetAcquisitionPattenIndex(ImagingType::HT2D, 
                                                 TriggerType::Trigger,
                                                 {htPattern3D});

        d->instrument->SetAcquisitionChannelIndex(ImagingType::HT2D, TriggerType::Trigger, 0, {2});
        d->instrument->SetAcquisitionChannelIndex(ImagingType::HT3D, TriggerType::Trigger, 0, {2});

        const auto bfPattern = model->GetIlluminationPattern({ImagingType::BFGray, TriggerType::Trigger, 2});
        d->instrument->SetAcquisitionPattenIndex(ImagingType::BFGray, TriggerType::Trigger, {bfPattern});

        d->instrument->SetAcquisitionChannelIndex(ImagingType::BFGray, TriggerType::Trigger, 0, {2});

        const auto activeFLChannels = d->ActiveFLChannels(sequence);
        qDebug() << "Active FL Channels = " << activeFLChannels;
        const auto flLightChannels = [=]()->QList<int32_t> {
            QList<int32_t> channels;
            for(auto idx=0; idx<3; idx++) {
                auto mode = AppEntity::ImagingMode::_from_integral(AppEntity::ImagingMode::FLCH0 + idx);
                if(activeFLChannels.contains(idx)) {
                    auto config = sysStatus->GetChannelConfig(mode);
                    const auto channel = config->GetLightChannel();
                    channels.push_back(channel + 1);
                } else {
                    channels.push_back(-1);
                }
                QLOG_INFO() << mode._to_string() << " Ex Filter:" << channels.last() << " (1-based)";
            }

            return channels;
        }();
        const auto [ledChannel, ledChannelOrder] = model->GetFLIlluminationMapChannel(flLightChannels);
        if(ledChannel == -1) {
            d->SetError("Invalid LED channel configuration. Check system configuration");
            return false;
        }

        QList<int32_t> flIllumPatterns2D;
        QList<int32_t> flIllumPatterns3D;
        for(auto idx=0; idx<3; idx++) {
            const auto channel = ledChannelOrder.at(idx);
            const auto pattern2D = model->GetIlluminationPattern(ImagingType::FL2D, TriggerType::Trigger, channel);
            const auto pattern3D = model->GetIlluminationPattern(ImagingType::FL3D, TriggerType::Trigger, channel);
            QLOG_INFO() << "[" << idx << "] LED Set ID:" << ledChannel << " Channel:" << channel << " Pattern 2D:" << pattern2D << " Pattern 3D:" << pattern3D;
            flIllumPatterns2D.push_back(pattern2D);
            flIllumPatterns3D.push_back(pattern3D);
        }
        d->instrument->SetAcquisitionPattenIndex(ImagingType::FL2D, TriggerType::Trigger, flIllumPatterns2D);
        d->instrument->SetAcquisitionPattenIndex(ImagingType::FL3D, TriggerType::Trigger, flIllumPatterns3D);
        d->instrument->SetAcquisitionChannelIndex(ImagingType::FL2D, TriggerType::Trigger, ledChannel, ledChannelOrder);
        d->instrument->SetAcquisitionChannelIndex(ImagingType::FL3D, TriggerType::Trigger, ledChannel, ledChannelOrder);

        auto useAutoFocus = sysStatus->GetAutoFocusEnabled();
        return d->instrument->StartAcquisition(sequence, 
                                               positions, 
                                               useAutoFocus,
                                               startingWellIndex,
                                               focusReadyMM,
                                               useMultiDishHolder);
    }

    auto Instrument::CheckSequenceProgress() const -> std::tuple<bool,double,AppEntity::Position> {
        const auto progress = d->instrument->CheckAcquisitionProgress();
        if(std::get<0>(progress) == false) {
            d->SetError(d->instrument->GetError());
        }
        return progress;
    }

    auto Instrument::StopAcquisition() -> bool {
        return d->instrument->StopAcquisition();
    }

    auto Instrument::MoveAxis(const AppEntity::Position& target) -> bool {
        struct {
            RawPosition x;
            RawPosition y;
            RawPosition z;
        } currentPos;

        if(!d->instrument->GetPositionXYZ(currentPos.x, currentPos.y, currentPos.z)) return false;

        const auto moveX = !d->IsSamePosition(AppEntity::Axis::X, target.toMM().x, currentPos.x);
        const auto moveY = !d->IsSamePosition(AppEntity::Axis::Y, target.toMM().y, currentPos.y);
        const auto moveZ = !d->IsSamePosition(AppEntity::Axis::Z, target.toMM().z, currentPos.z);

        auto targetMM = target.toMM();

        bool bRes = true;
        if(moveX && moveY) bRes = d->instrument->MoveXY(targetMM.x, targetMM.y);
        else if(moveX) bRes = d->instrument->Move(RawAxis::AxisX, targetMM.x);
        else if(moveY) bRes = d->instrument->Move(RawAxis::AxisY, targetMM.y);
        else if(moveZ) bRes = d->instrument->Move(RawAxis::AxisZ, targetMM.z);

        return bRes;
    }

    auto Instrument::MoveAxis(const AppEntity::Axis axis, const double targetMM) -> bool {
        return d->instrument->Move(d->ConvAxis(axis), targetMM);
    }

    auto Instrument::CheckAxisMotion() const -> MotionStatus {
        MotionStatus status;

        const auto componentStatus = d->instrument->IsMoving();
        status.error = componentStatus.error;
        status.moving = componentStatus.moving;
        status.afFailed = componentStatus.afFailed;
        status.message = componentStatus.message;

        return status;
    }

    auto Instrument::GetAxisPosition() const -> AppEntity::Position {
        struct {
            RawPosition x;
            RawPosition y;
            RawPosition z;
        } currentPos;

        if(!d->instrument->GetPositionXYZ(currentPos.x, currentPos.y, currentPos.z)) return AppEntity::Position();

        return AppEntity::Position::fromMM(currentPos.x, currentPos.y, currentPos.z);
    }

    auto Instrument::GetAxisPositionMM(const AppEntity::Axis axis) const -> double {
        RawPosition pos;
        if(!d->instrument->GetPosition(d->ConvAxis(axis), pos)) return 0;
        return pos;
    }

    auto Instrument::StartLive(AppEntity::Modality modality, int32_t channel) -> bool {
        using LiveMode = AppComponents::Instrument::LiveMode;
        using CameraType = AppComponents::Instrument::Camera;
        using ImagingParameter = AppComponents::Instrument::ImagingParameter;
        using ImagingMode = AppEntity::ImagingMode;
        using ImagingType = AppEntity::ImagingType;

        const auto model = AppEntity::System::GetModel();
        const auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto sysStatus = AppEntity::SystemStatus::GetInstance();

        d->latestLive.modality = modality;
        d->latestLive.channel = channel;

        auto liveMode = [=]()->LiveMode {
            if(modality == +AppEntity::Modality::HT) return LiveMode::Holography;
            if(modality == +AppEntity::Modality::BF) return LiveMode::BrightField;
            if(modality == +AppEntity::Modality::FL) return LiveMode::Fluorescence;
            return LiveMode::BrightField;
        }();

        auto imagingMode = [=]()->ImagingMode {
            if(modality == +AppEntity::Modality::HT) return ImagingMode::HT2D;
            if(modality == +AppEntity::Modality::BF) return ImagingMode::BFGray;
            if(modality == +AppEntity::Modality::FL) {
                return ImagingMode::_from_integral(ImagingMode::FLCH0+ channel);
            }
            return ImagingMode::BFGray;
        }();

        auto imagingType = [=]()->ImagingType {
            if(modality == +AppEntity::Modality::HT) return ImagingType::HT2D;
            if(modality == +AppEntity::Modality::BF) return ImagingType::BFGray;
            if(modality == +AppEntity::Modality::FL) return ImagingType::FL2D;
            return ImagingType::BFGray;
        }();

        auto mappedChannel = [=]()->std::tuple<int32_t,int32_t> {   //<pattern offset, led channel>
            if(modality == +AppEntity::Modality::HT) {
                const auto NA = sysStatus->GetNA();
                return std::make_tuple(0, sysConfig->GetHTIlluminationPattern(NA));
            }
            if(modality == +AppEntity::Modality::BF) {
                return std::make_tuple(0, 2);
            }
            if(modality == +AppEntity::Modality::FL) {
                auto mode = AppEntity::ImagingMode::_from_integral(AppEntity::ImagingMode::FLCH0 + channel);
                auto config = sysStatus->GetChannelConfig(mode);
                const auto exChannel = config->GetLightChannel();
                return model->GetFLIlluminationMapChannel(exChannel+1);
            }
            return std::make_tuple(-1, 0);
        }();

        const auto ledChannel = std::get<0>(mappedChannel);
        const auto dmdChannel = std::get<1>(mappedChannel);
        QLOG_INFO() << "Modality=" << modality._to_string() << ":" << channel << "[LED Channel=" << ledChannel << " DMDChannel=" << dmdChannel << "]";

        if(ledChannel == -1) {
            d->SetError("Illumination pattern not detected.");
            return false;
        }

        const auto chConfig = AppEntity::SystemStatus::GetInstance()->GetLiveConfig(imagingMode);

        auto intensity = [=]()->std::tuple<int32_t, int32_t, int32_t> {
            const auto intensity = chConfig->GetLightIntensity();

            int32_t red{ 0 };
            int32_t green{ 0 };
            int32_t blue{ 0 };
            if(dmdChannel == 0) red = intensity;
            if(dmdChannel == 1) green = intensity;
            if(dmdChannel == 2) blue = intensity;

            return std::make_tuple(red, green, blue);
        }();


        const auto illuminationPattern = model->GetIlluminationPattern({imagingType, 
                                                                           AppEntity::TriggerType::FreeRun, 
                                                                           dmdChannel});
        if(illuminationPattern == -1) {
            d->SetError(QString("Failed to find valid illumination pattern. [%1:%2]").arg(modality._to_string()).arg(channel));
            return false;
        }

        ImagingParameter param;
        param.SetExposureUSec(d->CalcExposure(chConfig->GetCameraExposureUSec()));   //3000
        param.SetIntensity(std::get<0>(intensity), std::get<1>(intensity), std::get<2>(intensity));  //40, 40, 45
        param.SetCameraType(chConfig->GetCameraInternal() ? CameraType::Internal : CameraType::External);
        param.SetIntervalUSec(d->Adjust(100000, chConfig->GetCameraExposureUSec()));  //10Hz = 100usec
        param.SetLEDChannel(ledChannel);
        param.SetEmissionFilter(chConfig->GetFilterChannel()); //0
        param.SetSequenceId(illuminationPattern);

        return d->instrument->StartLive(liveMode, param);
    }

    auto Instrument::StopLive() -> bool {
        return d->instrument->StopLive();
    }

    auto Instrument::ResumeLive() -> bool {
        return d->instrument->ResumeLive();
    }

    auto Instrument::EnableAutoFocus() -> std::tuple<bool,bool> {
        return d->instrument->EnableAutoFocus(true);
    }

    auto Instrument::DisableAutoFocus() -> bool {
        auto res = d->instrument->EnableAutoFocus(false);
        return std::get<0>(res);
    }

    auto Instrument::AutoFocusEnabled() const -> bool {
        return d->instrument->AutoFocusEnabled();
    }

    auto Instrument::PerformAutoFocus() -> std::tuple<bool,bool> {
        auto res = d->instrument->PerformAutoFocus();
        if(std::get<0>(res)) {
            auto sysStatus = AppEntity::SystemStatus::GetInstance();
            auto curPos = GetAxisPositionMM(AppEntity::Axis::Z);
            sysStatus->SetFocusZPosition(curPos);
        }

        return res;
    }

    auto Instrument::SetBestFocusCurrent(int32_t& value) -> bool {
        return d->instrument->SetBestFocusCurrent(value);
    }

    auto Instrument::SetBestFocus(int32_t value) -> bool {
        return d->instrument->SetBestFocus(value);
    }

    auto Instrument::GetErrorMessage() const -> QString {
        return d->errorMessage;
    }
}
