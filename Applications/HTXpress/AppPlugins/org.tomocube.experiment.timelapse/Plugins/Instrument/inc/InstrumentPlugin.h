#pragma once
#include <memory>

#include <IInstrument.h>
#include "HTX_Experiment_Timelapse_InstrumentExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::Instrument {
    class HTX_Experiment_Timelapse_Instrument_API Instrument : public UseCase::IInstrument {
    public:
        Instrument();
        ~Instrument() override;

        auto Initialize() -> bool override;
        auto IsInitialized() -> bool override;

        auto CleanUp()->void;

        auto InstallImagePort(UseCase::IImagePort::Pointer port) -> void override;
        auto UninstallImagePort(UseCase::IImagePort::Pointer port) -> void override;
        auto ImageCountInBuffer() const -> int32_t override;

        auto ChangeFOV(int32_t xPixels, int32_t yPixels, int32_t& offsetX, int32_t& offsetY) -> bool override;
        auto ChangeFullFOV() -> bool override;
        auto ChangeLiveModality(AppEntity::Modality modality, int32_t channel) -> bool override;
        auto GetCurrentLiveModality() const -> std::tuple<AppEntity::Modality, int32_t> override;

        auto RunImagingSequence(const AppEntity::ImagingSequence::Pointer sequence,
                                const QList<AppEntity::PositionGroup>& positions,
                                const AppEntity::WellIndex startingWellIndex,
                                const double focusReadyMM,
                                const bool useMultiDishHolder) -> bool override;
        auto CheckSequenceProgress() const -> std::tuple<bool,double,AppEntity::Position> override;
        auto StopAcquisition() -> bool override;

        auto MoveAxis(const AppEntity::Position& target) -> bool override;
        auto MoveAxis(const AppEntity::Axis axis, const double targetMM) -> bool override;
        auto CheckAxisMotion() const -> MotionStatus override;
        auto GetAxisPosition() const -> AppEntity::Position override;
        auto GetAxisPositionMM(const AppEntity::Axis axis) const -> double override;

        auto StartLive(AppEntity::Modality modality, int32_t channel) -> bool override;
        auto StopLive() -> bool override;
        auto ResumeLive() -> bool override;

        auto EnableAutoFocus() -> std::tuple<bool,bool> override;
        auto DisableAutoFocus() -> bool override;
        auto AutoFocusEnabled() const -> bool override;
        auto PerformAutoFocus() -> std::tuple<bool,bool> override;
        auto SetBestFocusCurrent(int32_t& value) -> bool override;
        auto SetBestFocus(int32_t value) -> bool override;

        auto GetErrorMessage() const -> QString override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}