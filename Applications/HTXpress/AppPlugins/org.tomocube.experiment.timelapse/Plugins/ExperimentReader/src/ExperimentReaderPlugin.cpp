#define LOGGER_TAG "[ExperimentReader]"

#include <TCLogger.h>
#include <ExperimentReader.h>
#include "ExperimentReaderPlugin.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ExperimentReader {
    Reader::Reader() : UseCase::IExperimentReader() {
    }

    Reader::~Reader() {
    }

    auto Reader::Read(const QString& path) const -> AppEntity::Experiment::Pointer {
        auto reader = HTXpress::AppComponents::ExperimentIO::ExperimentReader();
        return reader.Read(path);
    }
}
