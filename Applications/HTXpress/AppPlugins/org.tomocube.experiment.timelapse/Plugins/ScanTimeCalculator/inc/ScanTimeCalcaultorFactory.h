#pragma once
#include <memory>

#include <IScanTimeCalculator.h>
#include "HTX_Experiment_Timelapse_ScanTimeCalculatorExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ScanTimeCalculator {
    class HTX_Experiment_Timelapse_ScanTimeCalculator_API Factory : UseCase::IScanTimeCalculator {
    public:
        static auto Build()->IScanTimeCalculator*;
    };
}