#include "CalculatorDefault.h"
#include "ScanTimeCalcaultorFactory.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ScanTimeCalculator {
    //추후 Model 또는 사용 시나리오에 따라서 다른 유형의 Calculator를 사용하도록 구현을 변경할 수 있음
    auto Factory::Build() -> IScanTimeCalculator* {
        return new CalculatorDefault();
    }
}