project(HTXScanTimeCalculatorTest)

ADD_DEFINITIONS(-D_TEST_DATA=\"${CMAKE_CURRENT_SOURCE_DIR}/data\" )

#Header files for external use
set(INTERFACE_HEADERS
)

#Header files for internal use
set(PRIVATE_HEADERS
	../src/CalculatorDefault.h
)

#Sources
set(SOURCES
	src/TestMain.cpp
	src/CalculatorDefaultTest.cpp

	../src/CalculatorDefault.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
)

target_compile_features(${PROJECT_NAME} 
	PRIVATE
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PUBLIC
		/wd4251
	PRIVATE
		/W4 /WX
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
		${CMAKE_CURRENT_SOURCE_DIR}/../src
)

target_link_libraries(${PROJECT_NAME}			
	PRIVATE
		Catch2::Catch2
		Qt5::Core
        TC::Components::TCLogger
		HTXpress::AppEntity
		HTXpress::AppPlugins::experiment.timelapse::BusinessRule::Entity
        HTXpress::AppComponents::ScanningPlanner
        HTXpress::AppPlugins::experiment.timelapse::BusinessRule::UseCase
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/HTXpress/AppPlugins/Tests")

