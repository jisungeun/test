#include <ImagingSystem.h>

#include "ImageQueue.h"
#include "ImagingSystemPlugin.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ImagingSystem {
    using Component = AppComponents::ImagingSystem::Component;

    struct Plugin::Impl {
        ImageQueue::Pointer queue{ new ImageQueue() };
        Component::Pointer system{ Component::GetInstance() };
    };

    Plugin::Plugin() : UseCase::IImagingSystem(), d{new Impl} {
    }

    Plugin::~Plugin() {
    }

    auto Plugin::GetImagePort() const -> UseCase::IImagePort::Pointer {
        return d->queue;
    }

    auto Plugin::SetScenario(AppEntity::ImagingScenario::Pointer scenario, const QString& topPath) -> void {
        d->system->SetScenario(scenario, topPath);
    }

    auto Plugin::SetPositions(const QList<AppEntity::PositionGroup>& positions) -> void {
        d->system->SetPositions(positions);
    }

    auto Plugin::StartNewAcquisition(uint32_t index) -> void {
        d->system->StartNewAcquisition(index);
    }
}
