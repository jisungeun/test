#include <QList>
#include <QMutex>

#include <ImagingSystem.h>
#include "ImageQueue.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ImagingSystem {
    using Component = AppComponents::ImagingSystem::Component;
    using Queue = AppComponents::ImagingSystem::ImageQueue;

    struct ImageQueue::Impl {
        Queue::Pointer queue{ Component::GetInstance()->GetImageQueue() };

        QMutex mutex;
        int64_t count{ 0 };
    };

    ImageQueue::ImageQueue() : UseCase::IImagePort(), d{new Impl} {
    }

    ImageQueue::~ImageQueue() {
    }

    auto ImageQueue::Send(AppEntity::RawImage::Pointer image) -> void {
        QMutexLocker locker(&d->mutex);
        d->count++;
        d->queue->Send(image);
    }

    auto ImageQueue::Clear() -> void {
        //It will not remove any images to save all of them...
    }

    auto ImageQueue::IsEmpty() const -> bool {
        return d->queue->IsEmpty();
    }

    auto ImageQueue::ResetCount() -> void {
        QMutexLocker locker(&d->mutex);
        d->count = 0;
    }

    auto ImageQueue::GetCount() const -> uint64_t {
        QMutexLocker locker(&d->mutex);
        return d->count;
    }
}
