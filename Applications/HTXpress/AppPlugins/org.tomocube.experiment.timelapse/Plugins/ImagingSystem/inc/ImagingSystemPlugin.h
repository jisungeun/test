#pragma once
#include <memory>

#include <IImagingSystem.h>
#include "HTX_Experiment_Timelapse_ImagingSystemExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ImagingSystem {
    class HTX_Experiment_Timelapse_ImagingSystem_API Plugin : public UseCase::IImagingSystem {
    public:
        Plugin();
        ~Plugin() override;

        auto GetImagePort() const -> UseCase::IImagePort::Pointer override;

        auto SetScenario(AppEntity::ImagingScenario::Pointer scenario, const QString& topPath) -> void override;
        auto SetPositions(const QList<AppEntity::PositionGroup>& positions) -> void override;
        auto StartNewAcquisition(uint32_t index) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}