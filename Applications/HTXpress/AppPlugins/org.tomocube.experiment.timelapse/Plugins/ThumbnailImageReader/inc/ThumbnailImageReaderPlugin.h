﻿#pragma once

#include <memory>

#include <IThumbnailImageReader.h>
#include "HTX_Experiment_Timelapse_ThumbnailImageReaderExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ThumbnailImageReader {
    class HTX_Experiment_Timelapse_ThumbnailImageReader_API ThumbnailImageReaderPlugin : public UseCase::IThumbnailImageReader {
    public:
        ThumbnailImageReaderPlugin();
        ~ThumbnailImageReaderPlugin() override;

        auto SetDataPath(const QString& dataPath) -> void override;
        auto SetFrameIndex(const int32_t& frameIndex) -> void override;
        auto SetModality(const AppEntity::Modality& modality) -> void override;

        [[nodiscard]] auto GetThumbnailImage() const -> std::shared_ptr<QImage> override;
        [[nodiscard]] auto GetLastThumbnailImageInfo() const -> std::tuple<FrameIndex, AppEntity::Modality, std::shared_ptr<QImage>> override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
