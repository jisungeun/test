﻿#define LOGGER_TAG "[ThumbnailImageReaderPlugin]"
#include <TCLogger.h>

#include <ThumbnailReader.h>

#include "ThumbnailImageReaderPlugin.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ThumbnailImageReader {
    struct ThumbnailImageReaderPlugin::Impl {
        QString dataPath{};
        AppEntity::Modality::_enumerated modality;
        int32_t frameIndex{};
    };

    ThumbnailImageReaderPlugin::ThumbnailImageReaderPlugin() : IThumbnailImageReader(), d{std::make_unique<Impl>()} {

    }

    ThumbnailImageReaderPlugin::~ThumbnailImageReaderPlugin() = default;

    auto ThumbnailImageReaderPlugin::SetDataPath(const QString& dataPath) -> void {
        d->dataPath = dataPath;
    }

    auto ThumbnailImageReaderPlugin::SetFrameIndex(const int32_t& frameIndex) -> void {
        d->frameIndex = frameIndex;
    }

    auto ThumbnailImageReaderPlugin::SetModality(const AppEntity::Modality& modality) -> void {
        d->modality = modality;
    }

    auto ThumbnailImageReaderPlugin::GetThumbnailImage() const -> std::shared_ptr<QImage> {
        auto reader = AppComponents::ThumbnailIO::ThumbnailReader();
        reader.SetDataPath(d->dataPath);
        reader.SetFrameIndex(d->frameIndex);
        reader.SetModality(d->modality);
        return reader.GetImage();
    }

    auto ThumbnailImageReaderPlugin::GetLastThumbnailImageInfo() const -> std::tuple<FrameIndex, AppEntity::Modality, std::shared_ptr<QImage>> {
        auto reader = AppComponents::ThumbnailIO::ThumbnailReader();
        reader.SetDataPath(d->dataPath);
        return reader.GetLastImageInfo();
    }
}
