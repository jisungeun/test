#pragma once
#include <IImagingProfileLoader.h>

#include "HTX_Experiment_Timelapse_ImagingProfileLoaderExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Plugins::ImagingProfileLoader {
    class HTX_Experiment_Timelapse_ImagingProfileLoader_API Loader final : public UseCase::IImagingProfileLoader {
    public:
        Loader();
        ~Loader() override;

        auto GetHtScanParameter() const -> std::optional<std::tuple<int32_t, int32_t>> override;
    };
}
