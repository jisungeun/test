#define LOGGER_TAG "[MainWindow]"

#include <QTimer>

#include <TCLogger.h>
#include <MessageDialog.h>

#include <AppEvent.h>

#include "Internal/InstrumentObserver.h"
#include "Internal/ExperimentIOObserver.h"
#include "Internal/RunExperimentObserver.h"
#include "MainWindowControl.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct MainWindow::Impl {
        MainWindow* p{ nullptr };
        Ui::MainWindow ui;
        MainWindowControl control;

        InstrumentObserver* instrumentObserver{ nullptr };
        ExperimentIOObserver* experimentIOObserver{ nullptr };
        RunExperimentObserver* experimentObserver{ nullptr };

        QVariantMap appProperties;

        struct {
            QString project;
            QString experiment;
            bool singleRun{ false };
        } info;

        Impl(MainWindow* p) : p{ p } {}

        auto ClearExperiment()->void;

        auto EnteringApp()->void;
        auto LeavingApp(TC::Framework::AppEvent& appEvent)->void;
    };

    auto MainWindow::Impl::ClearExperiment() -> void {
        info.project.clear();
        info.experiment.clear();
    }

    auto MainWindow::Impl::EnteringApp() -> void {
        control.ConnectLiveView();
    }

    auto MainWindow::Impl::LeavingApp(TC::Framework::AppEvent& appEvent) -> void {
        p->publishSignal(appEvent, "Experiment::Timelapse");
        control.DisconnectLiveView();
    }

    MainWindow::MainWindow(QWidget* parent) : IMainWindowHTX("Experiment Timelapse", "Timelapse", parent), d{new Impl(this)} {
        d->ui.setupUi(this);
        d->instrumentObserver = new InstrumentObserver(this);
        d->experimentIOObserver = new ExperimentIOObserver(this);
        d->experimentObserver = new RunExperimentObserver(this);

        connect(d->experimentIOObserver, SIGNAL(sigError(const QString&)), this, SLOT(onLoadingExperimentFailed(const QString&)));

        connect(d->ui.dataPanel, SIGNAL(sigGotoExpSetup()), this, SLOT(onGotoExpSetup()));
        connect(d->ui.dataPanel, SIGNAL(sigGotoDataNavigation()), this, SLOT(onGotoDataNavigation()));
        connect(d->ui.controlPanel, SIGNAL(sigFinished()), this, SLOT(onExperimentFinished()));

        subscribeEvent("TabChange");
        connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnHandleEvent(ctkEvent)));

        d->control.ConnectInstrument();
    }
    
    MainWindow::~MainWindow() {
        QLOG_INFO() << "Destructor is called";
        d->control.CleanUp();
    }

    void MainWindow::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
    }

    void MainWindow::onLoadingExperimentFailed(const QString& message) {
        Q_UNUSED(message)

        TC::MessageDialog::warning(this, tr("Loading Experiment"),
                             tr("Failed to load experiment"));

        using namespace TC::Framework;

        d->control.SetTimelapseRunning(false);
        d->ClearExperiment();

        AppEvent appEvt(AppTypeEnum::APP_WITHOUT_ARG);
        appEvt.setFullName("org.tomocube.experiment.perform");
        appEvt.setAppName("Experiment Perform");

        d->LeavingApp(appEvt);
    }

    void MainWindow::onEndExperiment() {
        using StandardButton = TC::MessageDialog::StandardButton;

        if(d->control.IsExperimentRunning()) {
            auto selection = TC::MessageDialog::question(this, 
                                                         tr("Experiment"), 
                                                         tr("Stop the current experiment?"),
                                                         StandardButton::Yes | StandardButton::No, StandardButton::No);
            if(selection != StandardButton::Yes) return;
            //Todo implement stop experiment here
        }


        TC::Framework::AppEvent appEvt(TC::Framework::AppTypeEnum::APP_WITH_ARGS);
        appEvt.setFullName("org.tomocube.general.start");
        appEvt.setAppName("Start");

        d->LeavingApp(appEvt);
    }

    void MainWindow::onGotoExpSetup() {
        TC::Framework::AppEvent appEvt(TC::Framework::AppTypeEnum::APP_WITH_ARGS);
        appEvt.setFullName("org.tomocube.experiment.setup");
        appEvt.setAppName("Experiment Setup");

        d->LeavingApp(appEvt);
    }

    void MainWindow::onGotoDataNavigation() {
        TC::Framework::AppEvent appEvt(TC::Framework::AppTypeEnum::APP_WITH_ARGS);
        appEvt.setFullName("org.tomocube.data.navigation");
        appEvt.setAppName("Data Navigation");
        appEvt.addParameter("Project", d->info.project);
        appEvt.addParameter("Experiment", d->info.experiment);

        d->LeavingApp(appEvt);
    }

    void MainWindow::onExperimentFinished() {
        using namespace TC::Framework;

        d->control.SetTimelapseRunning(false);
        d->ClearExperiment();

        AppEvent appEvt(AppTypeEnum::APP_WITHOUT_ARG);
        appEvt.setFullName("org.tomocube.experiment.perform");
        appEvt.setAppName("Experiment Perform");

        d->LeavingApp(appEvt);
    }

    auto MainWindow::TryActivate() -> bool {
        return true;
    }

    auto MainWindow::TryDeactivate() -> bool {
        return true;
    }

    auto MainWindow::IsActivate() -> bool {
        return true;
    }

    auto MainWindow::GetMetaInfo() -> QVariantMap {
        return d->appProperties;
    }

    auto MainWindow::Execute(const QVariantMap& params) -> bool {
        d->EnteringApp();

        if (d->control.IsTimelapseRunning()) {
            // timelapse imaging 중 다른 app plugin에서 experiment.timelapse가 실행된 경우,
            // 기존 진행중인 imaging 상태를 표시한다.
            return true;
        } else {
            const auto hasExperiment = params.contains("Project") && params.contains("Experiment");
            if(hasExperiment) {
                d->info.project = params["Project"].toString();
                d->info.experiment = params["Experiment"].toString();
                if(params.contains("SingleRun")) {
                    d->info.singleRun = (params["SingleRun"].toString().compare("true", Qt::CaseInsensitive) == 0);
                } else {
                    d->info.singleRun = false;
                }
            } else if (d->info.experiment.isEmpty()) {
                using namespace TC::Framework;

                AppEvent appEvt(AppTypeEnum::APP_WITHOUT_ARG);
                appEvt.setFullName("org.tomocube.experiment.perform");
                appEvt.setAppName("Experiment Perform");

                d->LeavingApp(appEvt);

                TC::MessageDialog::warning(this, tr("Experiment"), tr("Experiment not specified"));

                return true;
            } else {
                return true;
            }

            d->control.SetTimelapseRunning(true);
            d->control.LoadExperiment(d->info.project, d->info.experiment, d->info.singleRun);
        }

        return true;
    }

    void MainWindow::OnHandleEvent(const ctkEvent& ctkEvent) {
        Q_UNUSED(ctkEvent)
    }
}
