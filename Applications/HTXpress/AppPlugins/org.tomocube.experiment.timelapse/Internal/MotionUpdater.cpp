#include <QList>
#include "MotionObserver.h"
#include "MotionUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct MotionUpdater::Impl {
        QList<MotionObserver*> observers;
    };

    MotionUpdater::MotionUpdater() : Interactor::IMotionView(), d{new Impl} {
    }

    MotionUpdater::~MotionUpdater() {
    }

    auto MotionUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new MotionUpdater() };
        return theInstance;
    }

    auto MotionUpdater::UpdateStatus(const bool moving) -> void {
        for(auto observer : d->observers) {
            observer->UpdateStatus(moving);
        }
    }

    auto MotionUpdater::UpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position) -> void {
        for(auto observer : d->observers) {
            observer->UpdatePosition(wellIdx, position);
        }
    }

    auto MotionUpdater::UpdateGlobalPosition(const AppEntity::Position& position) -> void {
        for(auto observer : d->observers) {
            observer->UpdateGlobalPosition(position);
        }
    }

    auto MotionUpdater::UpdateSelectedWell(const AppEntity::WellIndex wellIdx) -> void {
        for(auto observer : d->observers) {
            observer->UpdateSelectedWell(wellIdx);
        }
    }

    auto MotionUpdater::UpdateBestFocus(double posInMm) -> void {
        for(auto observer : d->observers) {
            observer->UpdateBestFocus(posInMm);
        }
    }

    auto MotionUpdater::ReportError(const QString& message) -> void {
        for(auto observer : d->observers) {
            observer->ReportError(message);
        }
    }

    auto MotionUpdater::ReportAFFailure() -> void {
        for(auto observer : d->observers) {
            observer->ReportAFFailure();
        }
    }

    auto MotionUpdater::Register(MotionObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto MotionUpdater::Deregister(MotionObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}
