#define LOGGER_TAG "[LiveViewer]"

#include <QTimer>
#include <TCLogger.h>

#include <System.h>
#include <SystemStatus.h>

#include "Utility.h"
#include "LiveViewerControl.h"
#include "LiveViewer.h"
#include "LiveControlDialog.h"
#include "ui_LiveViewer.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct LiveViewer::Impl {
        Ui::LiveViewer ui;
        LiveViewerControl control;

        LiveControlDialog* liveControlDialog{ nullptr };

        QTimer timer;

        auto Convert(const AppEntity::Modality modality, const int32_t channel = 0) -> AppEntity::ImagingMode;
        auto Convert(const AppEntity::ImagingMode mode)->QPair<AppEntity::Modality, int32_t>;
        auto Compare(const AppEntity::ChannelConfig::Pointer lhs, const AppEntity::ChannelConfig::Pointer rhs)->bool;
    };

    auto LiveViewer::Impl::Convert(const AppEntity::Modality modality, const int32_t channel) -> AppEntity::ImagingMode {
        auto imagingMode = -1;

        if (modality == +AppEntity::Modality::HT) {
            imagingMode = AppEntity::ImagingMode::HT2D;
        } else if (modality == +AppEntity::Modality::FL) {
            imagingMode = AppEntity::ImagingMode::FLCH0 + channel;
        } else if (modality == +AppEntity::Modality::BF) {
            imagingMode = AppEntity::ImagingMode::BFGray;
        }

        return AppEntity::ImagingMode::_from_integral(imagingMode);
    }

    auto LiveViewer::Impl::Convert(const AppEntity::ImagingMode mode) -> QPair<AppEntity::Modality, int32_t> {
        auto modality = AppEntity::Modality::HT;
        auto channel = 0;

        switch (mode) {
        case AppEntity::ImagingMode::HT3D:
        case AppEntity::ImagingMode::HT2D:
            modality = AppEntity::Modality::HT;
            channel = 0;
            break;
        case AppEntity::ImagingMode::BFGray:
        case AppEntity::ImagingMode::BFColor:
            modality = AppEntity::Modality::BF;
            channel = 0;
            break;
        case AppEntity::ImagingMode::FLCH0:
            modality = AppEntity::Modality::FL;
            channel = 0;
            break;
        case AppEntity::ImagingMode::FLCH1:
            modality = AppEntity::Modality::FL;
            channel = 1;
            break;
        case AppEntity::ImagingMode::FLCH2:
            modality = AppEntity::Modality::FL;
            channel = 2;
            break;
        }

        return qMakePair(modality, channel);
    }

    auto LiveViewer::Impl::Compare(const AppEntity::ChannelConfig::Pointer lhs, const AppEntity::ChannelConfig::Pointer rhs) -> bool {
        if (lhs->GetLightIntensity() != rhs->GetLightIntensity()) return false;
        if (lhs->GetCameraExposureUSec() != rhs->GetCameraExposureUSec()) return false;

        return true;
    }

    LiveViewer::LiveViewer(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.baseWidget->setObjectName("panel");
        d->ui.focusPanel->setDisabled(true);        //disabled while it gets images

        const auto model = AppEntity::System::GetModel();
        d->ui.viewPanel->SetResolution(model->CameraPixelSize() / model->ObjectiveLensMagnification());

        d->liveControlDialog = new LiveControlDialog(this);

        connect(&d->timer, SIGNAL(timeout()), this, SLOT(onUpdateImage()));

        d->timer.start(100);
    }

    LiveViewer::~LiveViewer() {
    }

    void LiveViewer::onModeSelected(AppEntity::Modality modality, int32_t channel) {
        Q_UNUSED(modality)
        Q_UNUSED(channel)
    }
    
    void LiveViewer::onUpdateImage() {
        QImage image;
        if(!d->control.GetLatestImage(image)) return;

        d->ui.viewPanel->SetImage(image);
    }

}
