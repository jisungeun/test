﻿#pragma once

#include <QObject>
#include <QImage>

#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse {
    class ThumbnailObserver : public QObject {
        Q_OBJECT
    public:
        explicit ThumbnailObserver(QObject* parent = nullptr);
        ~ThumbnailObserver() override;

        auto UpdateImagingPoint(const QString& wellPosition, const QString& pointID) -> void;
        auto UpdateStartTimesInSec(const QMap<int32_t, int32_t>& startTimes) -> void;
        auto UpdateThumbnailImage(int32_t frameIndex, AppEntity::Modality modality, const QImage& image) -> void;
        auto ReportError(const QString& message) -> void;

    signals:
        void sigUpdateImagingPoint(const QString& wellPosition, const QString& pointID);
        void sigUpdateThumbnailImage(int32_t frameIndex, AppEntity::Modality modality, const QImage& image);
        void sigUpdateStartTimesInSec(const QMap<int32_t, int32_t>& startTimesInSec);

        void sigReportError(const QString&);
    };
}
