#include "MotionUpdater.h"
#include "MotionObserver.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    MotionObserver::MotionObserver(QObject* parent) : QObject(parent) {
        MotionUpdater::GetInstance()->Register(this);
    }

    MotionObserver::~MotionObserver() {
        MotionUpdater::GetInstance()->Deregister(this);
    }

    auto MotionObserver::UpdateStatus(const bool moving) -> void {
        emit sigUpdateStatus(moving);
    }

    auto MotionObserver::UpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position) -> void {
        emit sigUpdatePosition(wellIdx, position);
    }

    auto MotionObserver::UpdateGlobalPosition(const AppEntity::Position& position) -> void {
        emit sigUpdateGlobalPosition(position);
    }

    auto MotionObserver::UpdateSelectedWell(const AppEntity::WellIndex wellIdx) -> void {
        emit sigUpdateCurrentWell(wellIdx);
    }

    auto MotionObserver::UpdateBestFocus(double posInMm) -> void {
        emit sigUpdateBestFocus(posInMm);
    }

    auto MotionObserver::ReportError(const QString& message) -> void {
        emit sigReportError(message);
    }

    auto MotionObserver::ReportAFFailure() -> void {
        emit sigAFFailed();
    }
}
