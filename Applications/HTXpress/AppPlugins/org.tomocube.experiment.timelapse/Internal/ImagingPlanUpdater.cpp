#include <QList>

#include "ImagingPlanObserver.h"
#include "ImagingPlanUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse {
    struct ImagingPlanUpdater::Impl {
        QList<ImagingPlanObserver*> observers;
    };

    ImagingPlanUpdater::ImagingPlanUpdater() : Interactor::IImagingPlanView(), d{new Impl} {
    }

    ImagingPlanUpdater::~ImagingPlanUpdater() {
    }

    auto ImagingPlanUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new ImagingPlanUpdater() };
        return theInstance;
    }

    auto ImagingPlanUpdater::Update(const UseCase::ImagingTimeTable::Pointer& table) -> void {
        for(auto observer : d->observers) {
            observer->Update(table);
        }
    }

    auto ImagingPlanUpdater::UpdateFOV(const double xInUm, const double yInUm) -> void {
        for(auto observer : d->observers) {
            observer->UpdateFOV(xInUm, yInUm);
        }
    }

    auto ImagingPlanUpdater::UpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex,
                                                double xInMm, double yInMm, 
                                                double widthInUm, double heightInUm) -> void {
        for(auto observer : d->observers) {
            observer->UpdateTileScanArea(enable, wellIndex, xInMm, yInMm, widthInUm, heightInUm);
        }
    }

    auto ImagingPlanUpdater::ClearFLChannels() -> void {
        for(auto observer : d->observers) {
            observer->ClearFLChannels();
        }
    }

    auto ImagingPlanUpdater::UpdateBFEnabled(bool enabled) -> void {
        for(auto observer : d->observers) {
            observer->UpdateBFEnabled(enabled);
        }
    }

    auto ImagingPlanUpdater::UpdateHTEnabled(bool enabled) -> void {
        for(auto observer : d->observers) {
            observer->UpdateHTEnabled(enabled);
        }
    }

    auto ImagingPlanUpdater::UpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types) -> void {
        for(auto observer : d->observers) {
            observer->UpdateEnabledImagingTypes(types);
        }
    }

    auto ImagingPlanUpdater::UpdateFLChannelConfig(const int32_t channel, AppEntity::ChannelConfig::Pointer config) -> void {
        for(auto observer : d->observers) {
            observer->UpdateFLChannelConfig(channel, config);
        }
    }

    auto ImagingPlanUpdater::UpdateAcquisitionLock(bool locked) -> void {
        for(auto observer : d->observers) {
            observer->UpdateAcquisitionLock(locked);
        }
    }

    auto ImagingPlanUpdater::Register(ImagingPlanObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto ImagingPlanUpdater::Deregister(ImagingPlanObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}
