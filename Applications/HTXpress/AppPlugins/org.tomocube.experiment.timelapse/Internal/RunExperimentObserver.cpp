#include "RunExperimentUpdater.h"
#include "RunExperimentObserver.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    RunExperimentObserver::RunExperimentObserver(QObject* parent) : QObject(parent) {
        RunExperimentUpdater::GetInstance()->Register(this);
    }

    RunExperimentObserver::~RunExperimentObserver() {
        RunExperimentUpdater::GetInstance()->Deregister(this);
    }

    auto RunExperimentObserver::UpdateProgress(const UseCase::ExperimentStatus& status) -> void {
        const auto pos = status.GetCurrentPosition();
        const auto sequenceIndex = status.GetRunningSequence();
        
        emit sigUpdateProgress(status.GetProgress(), status.GetElapsedTime(), status.GetRemainTime());
        emit sigUpdateCurrentPosition(std::get<0>(pos), std::get<1>(pos));
        emit sigCurrentRunningSequence(sequenceIndex);

        if(status.IsError()) {
            emit sigError(status.GetErrorMessage());
        } else {
            const auto message = status.GetStatusMessage();
            emit sigUpdateMessage(message);
        }
    }

    auto RunExperimentObserver::NotifyStopped() -> void {
        emit sigStopped();
    }

    auto RunExperimentObserver::UpdateProgressTime(const int32_t& elapsedTime, const int32_t& remainTime) -> void {
        emit sigUpdateProgress(0.0, elapsedTime, remainTime);
    }

    auto RunExperimentObserver::UpdatePosition(const AppEntity::WellIndex& wellIndex, const AppEntity::Position& position) -> void {
        emit sigUpdateCurrentPosition(wellIndex, position);
    }
}
