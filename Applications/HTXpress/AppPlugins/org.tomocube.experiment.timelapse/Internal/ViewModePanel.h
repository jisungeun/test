#pragma once
#include <memory>
#include <QWidget>

#include <AppEntityDefines.h>
#include <ChannelConfig.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class ViewModePanel : public QWidget {
        Q_OBJECT
    public:
        ViewModePanel(QWidget* parent = nullptr);
        ~ViewModePanel();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
