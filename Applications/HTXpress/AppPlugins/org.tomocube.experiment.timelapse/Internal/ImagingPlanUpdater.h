#pragma once
#include <memory>

#include <IImagingPlanView.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse {
    class ImagingPlanObserver;

    class ImagingPlanUpdater : public Interactor::IImagingPlanView {
    public:
        using Pointer = std::shared_ptr<ImagingPlanUpdater>;

    protected:
        ImagingPlanUpdater();

    public:
        ~ImagingPlanUpdater() override;

        static auto GetInstance()->Pointer;

        auto Update(const UseCase::ImagingTimeTable::Pointer& table) -> void override;
        auto UpdateFOV(const double xInUm, const double yInUm) -> void override;
        auto UpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex, double xInMm, double yInMm, double widthInUm, double heightInUm) -> void override;
        auto ClearFLChannels() -> void override;
        auto UpdateBFEnabled(bool enabled) -> void override;
        auto UpdateHTEnabled(bool enabled) -> void override;
        auto UpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types) -> void override;
        auto UpdateFLChannelConfig(const int32_t channel, AppEntity::ChannelConfig::Pointer config) -> void override;
        auto UpdateAcquisitionLock(bool locked) -> void override;

    protected:
        auto Register(ImagingPlanObserver* observer)->void;
        auto Deregister(ImagingPlanObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class ImagingPlanObserver;
    };
}