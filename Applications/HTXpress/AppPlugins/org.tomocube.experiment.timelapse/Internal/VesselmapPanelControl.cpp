#define LOGGER_TAG "[VesselMapPanel]"
#include <TCLogger.h>

#include <MotionController.h>
#include <ThumbnailController.h>

#include "Utility.h"
#include "MotionUpdater.h"
#include "VesselmapPanelControl.h"
#include "SystemStatus.h"
#include "ThumbnailUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct VesselmapPanelControl::Impl {
        AppEntity::WellIndex currentWellIndex{ -1 };
    };

    VesselmapPanelControl::VesselmapPanelControl() : d{new Impl} {
    }

    VesselmapPanelControl::~VesselmapPanelControl() {
    }

    auto VesselmapPanelControl::GetVesselPosition(const AppEntity::WellIndex& wellIdx,
                                                 const AppEntity::Position& position, 
                                                 double& posX, double& posY, double& posZ) -> void {
        auto vesselPos = Utility::well2vessel(wellIdx, position);
        posX = vesselPos.toMM().x;
        posY = vesselPos.toMM().y;
        posZ = vesselPos.toMM().z;
    }

    auto VesselmapPanelControl::GetVesselPosition(const AppEntity::Position& globalPosition, 
                                                  double& posX, double& posY, double& posZ) -> void {
        auto vesselPos = Utility::global2vessel(globalPosition);
        posX = vesselPos.toMM().x;
        posY = vesselPos.toMM().y;
        posZ = vesselPos.toMM().z;
    }

    auto VesselmapPanelControl::GetWellPosition(const AppEntity::Position& globalPosition, 
                                                double& posX, double& posY, double& posZ) -> void {
        auto wellPos = Utility::global2well(d->currentWellIndex, globalPosition);
        posX = wellPos.toMM().x;
        posY = wellPos.toMM().y;
        posZ = wellPos.toMM().z;
    }

    auto VesselmapPanelControl::GetCurrentWell() const -> AppEntity::WellIndex {
        return d->currentWellIndex;
    }

    auto VesselmapPanelControl::RefreshCurrentPosition(AppEntity::WellIndex& wellIndex, AppEntity::Position& globalPosition) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);

        if(!controller.GetGlobalPosition(globalPosition, wellIndex)) {
            d->currentWellIndex = -1;
            return false;
        }

        d->currentWellIndex = wellIndex;

        return true;
    }

    auto VesselmapPanelControl::SetCurrentImagingPoint(const QString& wellPosition, const QString& pointId) -> bool {
        auto updater = ThumbnailUpdater::GetInstance();
        auto presenter = Interactor::ThumbnailPresenter(updater.get());
        auto controller = Interactor::ThumbnailController(&presenter);

        return controller.SetCurrentImagingPoint(wellPosition, pointId);
    }
}
