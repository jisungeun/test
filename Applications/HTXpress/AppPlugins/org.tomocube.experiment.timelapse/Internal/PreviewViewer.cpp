#define LOGGER_TAG "[PreviewViewer]"
#include <QMessageBox>
#include <QProgressDialog>

#include <TCLogger.h>
#include <System.h>

#include "PreviewViewerControl.h"
#include "PreviewViewer.h"
#include "ui_PreviewViewer.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct PreviewViewer::Impl {
        Ui::PreviewViewer ui;
        PreviewViewerControl control;
    };

    PreviewViewer::PreviewViewer(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);
    }

    PreviewViewer::~PreviewViewer() {
    }
}
