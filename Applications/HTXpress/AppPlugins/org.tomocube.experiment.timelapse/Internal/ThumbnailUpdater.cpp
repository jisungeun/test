﻿#include <QList>

#include "ThumbnailUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct ThumbnailUpdater::Impl {
        QList<ThumbnailObserver*> observers;
    };

    ThumbnailUpdater::ThumbnailUpdater() : IThumbnailView(), d{std::make_unique<Impl>()} {
    }

    ThumbnailUpdater::~ThumbnailUpdater() = default;

    auto ThumbnailUpdater::GetInstance() -> Pointer {
        static Pointer theInstance {new ThumbnailUpdater()};
        return theInstance;
    }

    auto ThumbnailUpdater::UpdateImagingPoint(const QString& wellPosition, const QString& pointID) -> void {
        for(auto observer: d->observers) {
            observer->UpdateImagingPoint(wellPosition, pointID);
        }
    }

    auto ThumbnailUpdater::UpdateStartTimesInSec(const QMap<int32_t, int32_t>& startTimes) -> void {
        for(auto observer : d->observers) {
            observer->UpdateStartTimesInSec(startTimes);
        }
    }

    auto ThumbnailUpdater::UpdateThumbnailImage(int32_t frameIndex, AppEntity::Modality modality, const QImage& image) -> void {
        for(auto observer: d->observers) {
            observer->UpdateThumbnailImage(frameIndex, modality, image);
        }
    }

    auto ThumbnailUpdater::ReportError(const QString& message) -> void {
        for(auto observer: d->observers) {
            observer->ReportError(message);
        }
    }

    auto ThumbnailUpdater::Register(ThumbnailObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto ThumbnailUpdater::Deregister(ThumbnailObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}
