#define LOGGER_TAG "[PreviewViewer]"
#include <TCLogger.h>

#include "PreviewViewerControl.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct PreviewViewerControl::Impl {
    };

    PreviewViewerControl::PreviewViewerControl() : d{new Impl} {
    }

    PreviewViewerControl::~PreviewViewerControl() {
    }
}
