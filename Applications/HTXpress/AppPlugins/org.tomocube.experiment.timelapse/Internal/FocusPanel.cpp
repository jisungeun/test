#include <QVBoxLayout>
#include <QButtonGroup>
#include <QPushButton>
#include <QCheckBox>

#include <MessageDialog.h>
#include <ZControlPanel.h>
#include <SystemStatus.h>

#include "MotionObserver.h"
#include "InstrumentObserver.h"
#include "FocusPanelControl.h"
#include "FocusPanel.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    enum ButtonIndex {
        AutoFocus,
        SetBestFocus
    };

    struct FocusPanel::Impl {
        FocusPanelControl control;
        QButtonGroup* buttons{ nullptr };
        QCheckBox* afCheck{ nullptr };

        MotionObserver* motionObserver{ nullptr };
        InstrumentObserver* instrumentObserver{ nullptr };

        bool neverSet{ true };

        auto AddButton(const QString& label, int32_t index)->QPushButton*;
    };

    auto FocusPanel::Impl::AddButton(const QString& label, int32_t index) -> QPushButton* {
        auto* btn = new QPushButton(label);
        btn->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed) ;
        btn->setFixedSize(50, 50);
        btn->setCheckable(false);
        btn->setFocusPolicy(Qt::FocusPolicy::NoFocus);
        buttons->addButton(btn, index);
        return btn;
    }

    FocusPanel::FocusPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        using ZControlPanel = HTXpress::AppComponents::ZControlPanel::Widget;

        d->buttons = new QButtonGroup(this);
        d->buttons->setExclusive(true);

        auto* zControl = new ZControlPanel(this);
        zControl->SetStepRange(0.1, 50.0);
        zControl->EnableJog(false);
        zControl->setMaximumWidth(46);

        d->afCheck = new QCheckBox(this);
        d->afCheck->setChecked(AppEntity::SystemStatus::GetInstance()->GetAutoFocusEnabled());
        d->afCheck->setObjectName("tsx-focuspanel-afcheck");
        d->afCheck->setDisabled(true);

        auto* vboxLayout = new QVBoxLayout(this);
        {
            vboxLayout->addWidget(zControl);

            auto* btn = d->AddButton("Set", ButtonIndex::SetBestFocus);
            btn->setToolTip("Set the current Z position to the best focus position");
            //btn->setStyleSheet("background: #FC686C");   //TODO sync with focus status from experiment.perform app
            vboxLayout->addWidget(btn);

            btn = d->AddButton("AF", ButtonIndex::AutoFocus);
            btn->setToolTip("Perform auto focus");
            vboxLayout->addWidget(btn);

            vboxLayout->addWidget(d->afCheck);

            vboxLayout->addStretch();
        }
        vboxLayout->setContentsMargins(10, 20, 10, 20);
        vboxLayout->setSpacing(10);
        setLayout(vboxLayout);

        d->motionObserver = new MotionObserver(this);
        d->instrumentObserver = new InstrumentObserver(this);

        connect(d->instrumentObserver, SIGNAL(sigAFEnabled(bool)), this, SLOT(onEnableAutofocus(bool)));

        connect(d->buttons, SIGNAL(idClicked(int)), this, SLOT(onSelected(int)));
    }

    FocusPanel::~FocusPanel() {
    }

    void FocusPanel::onEnableAutofocus(bool enable) {
        d->afCheck->blockSignals(true);
        d->afCheck->setCheckState(enable ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
        d->afCheck->blockSignals(false);
    }

    void FocusPanel::onSelected(int index) {
        switch(index) {
        case ButtonIndex::AutoFocus:
            d->control.PerformAF();
            break;
        case ButtonIndex::SetBestFocus:
            d->control.SetBestFocus();
            if(d->neverSet) {
                const auto style = d->buttons->button(ButtonIndex::AutoFocus)->styleSheet();
                d->buttons->button(index)->setStyleSheet(style);
                d->neverSet = false;
            }
            break;
        }
    }
}
