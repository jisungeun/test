#include <QVBoxLayout>
#include <QButtonGroup>
#include <QPushButton>
#include <QMap>

#include <ImagingConfig.h>

#include "ViewModePanelControl.h"
#include "ViewModePanel.h"

#include <QLabel>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct ViewModePanel::Impl {
        ViewModePanelControl control;
        QButtonGroup* buttons{ nullptr };
    };

    ViewModePanel::ViewModePanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->buttons = new QButtonGroup(this);
        d->buttons->setExclusive(true);

        const auto counts = d->control.GetCount();

        auto* vboxLayout = new QVBoxLayout(this);
        {
            for(int idx=0; idx<counts; idx++) {
                auto* btn = new QPushButton(d->control.GetTitle(idx), this);
                btn->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed) ;
                btn->setFixedSize(46, 34);
                btn->setCheckable(true);
                btn->setDisabled(true);
                if (d->control.GetModality(idx) == +AppEntity::Modality::FL) btn->setEnabled(false);
                d->buttons->addButton(btn, idx);
                vboxLayout->addWidget(btn);
            }
            vboxLayout->addStretch();
        }
        vboxLayout->insertSpacing(1, 5);
        vboxLayout->insertWidget(2, new QLabel("FL"), 0, Qt::AlignCenter);
        vboxLayout->setContentsMargins(10, 20, 10, 20);
        vboxLayout->setSpacing(10);
        setLayout(vboxLayout);
    }

    ViewModePanel::~ViewModePanel() {
    }

}
