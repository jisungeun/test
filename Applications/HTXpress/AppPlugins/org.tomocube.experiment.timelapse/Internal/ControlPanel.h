#pragma once
#include <memory>
#include <QWidget>

#include <Position.h>
#include <ImagingTimeTable.h>
#include <Experiment.h>
#include <ImagingConditionPanel.h>
#include <TimelapseImagingPanel.h>
#include <ImagingConfig.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class ControlPanel : public QWidget {
        Q_OBJECT

    public:
        using TimelapseSequence = AppComponents::TimelapseImagingPanel::TimelapseSequence;

        ControlPanel(QWidget* parent = nullptr);
        ~ControlPanel() override;

    private:
        auto FinishImaging()->void;

    protected slots:
        void onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool singleRun);
        void onExperimentStoppedByError(const QString& message);
        void onUpdateMessage(const QString& message);
        void onUpdateTimelapseExperimentProgress(const double progress, const int elapsedSeconds, const int remainSeconds);
        void onStopExperiment();
        void onDoneExperiment();
        void onCurrentRunningSequenceIndex(int currentSequenceIdx);

    signals:
        void sigFinished();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}