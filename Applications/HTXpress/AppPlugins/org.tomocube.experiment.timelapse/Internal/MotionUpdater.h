#pragma once
#include <memory>

#include <IMotionView.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class MotionObserver;

    class MotionUpdater : public Interactor::IMotionView {
    public:
        using Pointer = std::shared_ptr<MotionUpdater>;

    protected:
        MotionUpdater();

    public:
        ~MotionUpdater();

        static auto GetInstance()->Pointer;

        auto UpdateStatus(const bool moving) -> void override;
        auto UpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position) -> void override;
        auto UpdateGlobalPosition(const AppEntity::Position& position) -> void override;
        auto UpdateSelectedWell(const AppEntity::WellIndex wellIdx) -> void override;
        auto UpdateBestFocus(double posInMm) -> void override;

        auto ReportError(const QString& message) -> void override;
        auto ReportAFFailure() -> void override;

    protected:
        auto Register(MotionObserver* observer)->void;
        auto Deregister(MotionObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class MotionObserver;
    };
}