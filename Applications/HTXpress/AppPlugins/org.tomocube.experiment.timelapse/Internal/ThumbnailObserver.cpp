﻿#include "ThumbnailObserver.h"
#include "ThumbnailUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    ThumbnailObserver::ThumbnailObserver(QObject* parent) : QObject(parent){
        ThumbnailUpdater::GetInstance()->Register(this);
    }

    ThumbnailObserver::~ThumbnailObserver() {
        ThumbnailUpdater::GetInstance()->Deregister(this);
    }

    auto ThumbnailObserver::UpdateImagingPoint(const QString& wellPosition, const QString& pointID) -> void {
        emit sigUpdateImagingPoint(wellPosition, pointID);
    }

    auto ThumbnailObserver::UpdateStartTimesInSec(const QMap<int32_t, int32_t>& startTimes) -> void {
        emit sigUpdateStartTimesInSec(startTimes);
    }

    auto ThumbnailObserver::UpdateThumbnailImage(int32_t frameIndex, AppEntity::Modality modality, const QImage& image) -> void {
        emit sigUpdateThumbnailImage(frameIndex, modality, image);
    }

    auto ThumbnailObserver::ReportError(const QString& message) -> void {
        emit sigReportError(message);
    }
}
