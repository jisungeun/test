#pragma once
#include <memory>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class FocusPanelControl {
    public:
        FocusPanelControl();
        ~FocusPanelControl();

        auto PerformAF()->bool;
        auto SetBestFocus()->bool;
        auto MoveZ(double distUm)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}