#include "ExperimentIOUpdater.h"
#include "ExperimentIOObserver.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    ExperimentIOObserver::ExperimentIOObserver(QObject* parent) : QObject(parent) {
        ExperimentIOUpdater::GetInstance()->Register(this);
    }

    ExperimentIOObserver::~ExperimentIOObserver() {
        ExperimentIOUpdater::GetInstance()->Deregister(this);
    }

    auto ExperimentIOObserver::Update(AppEntity::Experiment::Pointer experiment, bool singleRun) -> void {
        emit sigUpdate(experiment, singleRun);
    }

    auto ExperimentIOObserver::Error(const QString& message) -> void {
        emit sigError(message);
    }
}
