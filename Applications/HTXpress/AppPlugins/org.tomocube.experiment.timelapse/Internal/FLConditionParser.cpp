#include <QMap>
#include <System.h>
#include "FLConditionParser.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct FLConditionParser::Impl {
        struct Config {
            QString name;
            int32_t exposure;
            int32_t interval;
            int32_t intensity;
            double gain;

            struct {
                int32_t waveLength;
                int32_t bandWidth;
                int32_t channel;
            } exFilter, emFilter;
        };

        struct Scan {
            AppEntity::FLScanMode mode{ AppEntity::FLScanMode::Default };
            double bottom;
            double top;
            double step;
            int32_t steps;
            double focus;
            double flOffset;
        } scan;

        QMap<int32_t, Config> configs;

        auto Parse(const AppEntity::Experiment::Pointer& experiment)->void;

        auto GetSingleImagingFLCondition(const AppEntity::Experiment::Pointer& experiment)->AppEntity::ImagingConditionFL::Pointer;
        auto GetExFilterChannel(const AppEntity::FLFilter& filter)->int32_t;
        auto GetEmFilterChannel(const AppEntity::FLFilter& filter)->int32_t;
    };

    auto FLConditionParser::Impl::Parse(const AppEntity::Experiment::Pointer& experiment) -> void {
        auto flCond = GetSingleImagingFLCondition(experiment);
        if(!flCond) return;

        for(auto channel : flCond->GetChannels()) {
            if (configs.contains(channel)) continue;

            configs[channel].exposure = flCond->GetExposure(channel);
            configs[channel].interval = flCond->GetInterval(channel);
            configs[channel].intensity = flCond->GetIntensity(channel);
            configs[channel].gain = flCond->GetGain(channel);
            configs[channel].exFilter.channel = GetExFilterChannel(flCond->GetExFilter(channel));
            configs[channel].exFilter.waveLength = flCond->GetExFilter(channel).GetWaveLength();
            configs[channel].exFilter.bandWidth = flCond->GetExFilter(channel).GetBandwidth();
            configs[channel].emFilter.channel = GetEmFilterChannel(flCond->GetEmFilter(channel));
            configs[channel].emFilter.waveLength = flCond->GetEmFilter(channel).GetWaveLength();
            configs[channel].emFilter.bandWidth = flCond->GetEmFilter(channel).GetBandwidth();
            configs[channel].name = flCond->GetName(channel);
        }

        auto model = AppEntity::System::GetModel();
        auto zRes = model->AxisResolutionPPM(AppEntity::Model::Axis::Z) / 1000.0;   //-> Pulses per um

        scan.mode = flCond->GetScanMode();
        scan.bottom = flCond->GetScanRangeBottom() / zRes;
        scan.top = flCond->GetScanRangeTop() / zRes;
        scan.step = flCond->GetScanRangeStep() / zRes;
        scan.steps = flCond->GetSliceCount();
        scan.focus = flCond->GetScanFocusHT();
        scan.flOffset = flCond->GetScanFocusFLOffset();
    }

    auto FLConditionParser::Impl::GetSingleImagingFLCondition(const AppEntity::Experiment::Pointer& experiment) -> AppEntity::ImagingConditionFL::Pointer {
        auto imgCond = experiment->GetSingleImagingCondition(AppEntity::ImagingType::FL3D);
        if(!imgCond) imgCond = experiment->GetSingleImagingCondition(AppEntity::ImagingType::FL2D);
        if(!imgCond) return nullptr;
        return std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(imgCond);
    }

    auto FLConditionParser::Impl::GetExFilterChannel(const AppEntity::FLFilter& filter) -> int32_t {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto exChannels = sysConfig->GetFLActiveExcitations();
        for(auto channel : exChannels) {
            AppEntity::FLFilter exFilter;
            sysConfig->GetFLActiveExcitation(channel, exFilter);
            if(exFilter == filter) return channel;
        }
        return -1;
    }

    auto FLConditionParser::Impl::GetEmFilterChannel(const AppEntity::FLFilter& filter) -> int32_t {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto emChannels = sysConfig->GetFLEmissions();
        for(auto channel : emChannels) {
            AppEntity::FLFilter emFilter;
            sysConfig->GetFLEmission(channel, emFilter);
            if(emFilter == filter) return channel;
        }
        return -1;
    }

    FLConditionParser::FLConditionParser(const AppEntity::Experiment::Pointer& experiment) : d{new Impl} {
        d->Parse(experiment);
    }

    FLConditionParser::~FLConditionParser() {
    }

    auto FLConditionParser::GetChannels() -> QList<int32_t> {
        return d->configs.keys();
    }

    auto FLConditionParser::GetExposure(int32_t channel) const -> int32_t {
        if(!d->configs.contains(channel)) return 0;
        return d->configs[channel].exposure;
    }

    auto FLConditionParser::GetInterval(int32_t channel) const -> int32_t {
        if(!d->configs.contains(channel)) return 0;
        return d->configs[channel].interval;
    }

    auto FLConditionParser::GetIntensity(int32_t channel) const -> int32_t {
        if(!d->configs.contains(channel)) return 0;
        return d->configs[channel].intensity;
    }

    auto FLConditionParser::GetGain(int32_t channel) const -> double {
        if(!d->configs.contains(channel)) return 0;
        return d->configs[channel].gain;
    }

    auto FLConditionParser::GetExFilterChannel(int32_t channel) const -> int32_t {
        if(!d->configs.contains(channel)) return 0;
        return d->configs[channel].exFilter.channel;
    }

    auto FLConditionParser::GetExFilterWaveLength(int32_t channel) const -> int32_t {
        if(!d->configs.contains(channel)) return 0;
        return d->configs[channel].exFilter.waveLength;
    }

    auto FLConditionParser::GetExFilterBandWidth(int32_t channel) const -> int32_t {
        if(!d->configs.contains(channel)) return 0;
        return d->configs[channel].exFilter.bandWidth;
    }

    auto FLConditionParser::GetEmFilterChannel(int32_t channel) const -> int32_t {
        if(!d->configs.contains(channel)) return 0;
        return d->configs[channel].emFilter.channel;
    }

    auto FLConditionParser::GetEmFilterWaveLength(int32_t channel) const -> int32_t {
        if(!d->configs.contains(channel)) return 0;
        return d->configs[channel].emFilter.waveLength;
    }

    auto FLConditionParser::GetEmFIlterBandWidth(int32_t channel) const -> int32_t {
        if(!d->configs.contains(channel)) return 0;
        return d->configs[channel].emFilter.bandWidth;
    }

    auto FLConditionParser::GetName(int32_t channel) const -> QString {
        if(!d->configs.contains(channel)) return "";
        return d->configs[channel].name;
    }

    auto FLConditionParser::GetScanMode() const -> AppEntity::FLScanMode {
        return d->scan.mode;
    }

    auto FLConditionParser::GetScanRangeBottom() const -> double {
        return d->scan.bottom;
    }

    auto FLConditionParser::GetScanRangeTop() const -> double {
        return d->scan.top;
    }

    auto FLConditionParser::GetScanRangeStep() const -> double {
        return d->scan.step;
    }

    auto FLConditionParser::GetScanRangeSteps() const -> int32_t {
        return d->scan.steps;
    }

    auto FLConditionParser::GetScanFocusHT() const -> double {
        return d->scan.focus;
    }

    auto FLConditionParser::GetScanFocusFLOffset() const -> double {
        return d->scan.flOffset;
    }
}
