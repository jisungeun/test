#pragma once
#include <memory>
#include <QWidget>
#include <QMap>

#include <Location.h>
#include <Experiment.h>

#include "ThumbnailPanel.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class ThumbnailViewer : public QWidget {
        Q_OBJECT

    public:
        using Self = ThumbnailViewer;

        ThumbnailViewer(QWidget* parent = nullptr);
        ~ThumbnailViewer() override;

    protected slots:
        void onRequestUpdateImage();
        void onSetImage(int32_t frameIndex, AppEntity::Modality modality, const QImage& image);

        // from thumbnail component
        void onChangeSliderIndex(int32_t sliderIndex);
        void onChangeModality(HTXpress::AppComponents::ThumbnailviewPanel::ModalityIndex modality);
        void onChangeSyncMode(bool sync);

        void onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool singleRun);
        void onAddAcquisitionData(const QString& fileFullPath);

        void onSetCurrentImagingPoint(const QString& wellPosition, const QString& pointID);
        void onSetStartTimes(const QMap<int32_t, int32_t>& startTimes);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
