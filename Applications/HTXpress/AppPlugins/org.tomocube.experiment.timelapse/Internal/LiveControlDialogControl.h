#pragma once

#include <memory>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class LiveControlDialogControl {
    public:
        LiveControlDialogControl();
        ~LiveControlDialogControl();

        auto MinimumExposureMSec() const->int32_t;
        auto MaximumExposureMSec() const->int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}