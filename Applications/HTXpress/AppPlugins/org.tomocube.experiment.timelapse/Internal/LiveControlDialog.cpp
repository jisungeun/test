#define LOGGER_TAG "[LiveControl]"

#include <UIUtility.h>

#include "ui_LiveControlDialog.h"
#include "LiveControlDialog.h"
#include "LiveControlDialogControl.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct LiveControlDialog::Impl {
        Ui::LiveControlDialog ui;
        LiveControlDialogControl control;

        AppEntity::ImagingMode currentMode{ AppEntity::ImagingMode::FLCH0 };

        uint32_t intensity{ 0 };
        uint32_t exposure{ 0 };
        double gain{ 0 };
    };

    LiveControlDialog::LiveControlDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
        d->ui.setupUi(this);

        // TODO: saturation 기능 추가하면 상태 변경
        d->ui.saturationButton->hide();

        // set slider and spin box range
        d->ui.intensitySlider->setRange(0, 100);
        d->ui.intensitySpinBox->setRange(0, 100);

        const auto minExposure = d->control.MinimumExposureMSec();
        const auto maxExposure = d->control.MaximumExposureMSec();
        d->ui.exposureSlider->setRange(minExposure, maxExposure);
        d->ui.exposureSpinBox->setRange(minExposure, maxExposure);

        d->ui.gainSlider->setRange(0, 50 * 10);
        d->ui.gainSpinBox->setDecimals(1);
        d->ui.gainSpinBox->setRange(0, 50);

        //TODO Remove gain control if necessary (HTX-931)
        d->ui.gainSlider->hide();
        d->ui.gainEditLabel->hide();
        d->ui.gainSliderLabel->hide();
        d->ui.gainSpinBox->hide();
        d->ui.boostButton->hide();

        // connect signal/slot
        connect(d->ui.intensitySlider, &QSlider::valueChanged, this, &LiveControlDialog::onChangedSliderValue);
        connect(d->ui.exposureSlider, &QSlider::valueChanged, this, &LiveControlDialog::onChangedSliderValue);
        connect(d->ui.gainSlider, &QSlider::valueChanged, this, &LiveControlDialog::onChangedSliderValue);

        connect(d->ui.intensitySpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &LiveControlDialog::onChangedSpinBoxValue);
        connect(d->ui.exposureSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &LiveControlDialog::onChangedSpinBoxValue);
        connect(d->ui.gainSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &LiveControlDialog::onChangedSpinBoxValue);

        connect(d->ui.boostButton, &QPushButton::clicked, this, &LiveControlDialog::onBoost);
        connect(d->ui.saturationButton, &QPushButton::clicked, this, &LiveControlDialog::onChangedSaturationMode);
        connect(d->ui.importAcqSetButton, &QPushButton::clicked, this, [=]() { emit sigAcquisitionConditionRequested(); });
        connect(d->ui.saveButton, &QPushButton::clicked, this, &LiveControlDialog::sigSaveRequested);
    }

    LiveControlDialog::~LiveControlDialog() {
    }

    auto LiveControlDialog::SetMode(AppEntity::ImagingMode mode) -> void {
        d->currentMode = mode;

        bool isBF = (mode == +AppEntity::ImagingMode::BFGray) || (mode == +AppEntity::ImagingMode::BFColor);

        d->ui.intensitySpinBox->setEnabled(!isBF);
        d->ui.intensitySlider->setEnabled(!isBF);
        d->ui.gainSpinBox->setEnabled(!isBF);
        d->ui.gainSlider->setEnabled(!isBF);

        if (isBF) d->ui.saveButton->hide();
        else d->ui.saveButton->show();
    }

    auto LiveControlDialog::GetMode() -> AppEntity::ImagingMode {
        return d->currentMode;
    }

    auto LiveControlDialog::SetIntensity(uint32_t value) -> void {
        d->intensity = value;

        if (!d->ui.boostButton->isChecked()) {
            TC::SilentCall(d->ui.intensitySlider)->setValue(d->intensity);
            TC::SilentCall(d->ui.intensitySpinBox)->setValue(d->intensity);
        }
    }

    auto LiveControlDialog::GetIntensity() -> uint32_t {
        return d->intensity;
    }

    auto LiveControlDialog::SetExposure(uint32_t value) -> void {
        d->exposure = value;

        if (!d->ui.boostButton->isChecked()) {
            TC::SilentCall(d->ui.exposureSlider)->setValue(d->exposure);
            TC::SilentCall(d->ui.exposureSpinBox)->setValue(d->exposure);
        }
    }

    auto LiveControlDialog::GetExposure() -> uint32_t {
        return d->exposure;
    }

    auto LiveControlDialog::SetGain(double value) -> void {
        d->gain = value;

        if (!d->ui.boostButton->isChecked()) {
            TC::SilentCall(d->ui.gainSlider)->setValue(d->gain * 10);
            TC::SilentCall(d->ui.gainSpinBox)->setValue(d->gain);
        }
    }

    auto LiveControlDialog::GetGain() -> double {
        return d->gain;
    }

    auto LiveControlDialog::SetExposureRange(uint32_t minVal, uint32_t maxVal) -> void {
        TC::SilentCall(d->ui.exposureSlider)->setRange(minVal, maxVal);
        TC::SilentCall(d->ui.exposureSpinBox)->setRange(minVal, maxVal);
    }

    auto LiveControlDialog::ShowBoostMode(bool show) -> void {
        //TODO Remove gain control if necessary (HTX-931)
        //d->ui.boostButton->setVisible(show);
        Q_UNUSED(show)
    }

    auto LiveControlDialog::SetEnabledSaveButton(bool enabled) -> void {
        d->ui.saveButton->setEnabled(enabled);
    }

    void LiveControlDialog::onChangedSliderValue(int value) {
        if (sender() == d->ui.intensitySlider) {
            d->intensity = value;
        } else if (sender() == d->ui.exposureSlider) {
            d->exposure = value;
        } else if (sender() == d->ui.gainSlider) {
            d->gain = value / 10.;
        } else {
            return;
        }

        emit sigLiveImageConditionChanged(d->intensity, d->exposure, d->gain);
    }

    void LiveControlDialog::onChangedSpinBoxValue(double value) {
        if (sender() == d->ui.intensitySpinBox) {
            d->intensity = value;
        } else if (sender() == d->ui.exposureSpinBox) {
            d->exposure = value;
        } else if (sender() == d->ui.gainSpinBox) {
            d->gain = value;
        } else {
            return;
        }

        emit sigLiveImageConditionChanged(d->intensity, d->exposure, d->gain);
    }

    void LiveControlDialog::onBoost(bool checked) {
        uint32_t intensity = 0;
        uint32_t exposure = 0;
        double gain = 0;

        if (checked) {
            intensity = d->ui.intensitySpinBox->minimum();
            exposure = d->ui.exposureSpinBox->minimum();
            gain = d->ui.gainSpinBox->maximum();
        } else {
            intensity = d->intensity;
            exposure = d->exposure;
            gain = d->gain;
        }

        TC::SilentCall(d->ui.intensitySlider)->setValue(intensity);
        TC::SilentCall(d->ui.intensitySpinBox)->setValue(intensity);

        TC::SilentCall(d->ui.exposureSlider)->setValue(exposure);
        TC::SilentCall(d->ui.exposureSpinBox)->setValue(exposure);

        TC::SilentCall(d->ui.gainSlider)->setValue(gain * 10);
        TC::SilentCall(d->ui.gainSpinBox)->setValue(gain);

        // TODO: boost mode 동작 수정
        emit sigLiveImageConditionChanged(d->intensity, d->exposure, d->gain);
    }

    void LiveControlDialog::onChangedSaturationMode(bool checked) {
        emit sigLiveSaturationActivated(checked);
    }

}
