#pragma once
#include <memory>
#include <QImage>

#include <AppEntityDefines.h>
#include <ImagingConfig.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class LiveViewerControl {
    public:
        LiveViewerControl();
        ~LiveViewerControl();

        auto GetLatestImage(QImage& image)->bool;

        auto MoveSampleStage(int32_t posX, int32_t posY)->bool;
        auto SetViewMode(AppEntity::Modality modality, int32_t channel = 0)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}