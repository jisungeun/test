#pragma once

#include <memory>

#include <QMap>

#include <enum.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse {
    BETTER_ENUM(ThumbnailModality, int32_t, HT, FL, BF);

    class ThumbnailViewerControl {
    public:
        using Self = ThumbnailViewerControl;

        using WellPosition = QString;
        using PointID = QString;
        using DataPath = QString;
        using FrameIndex = int32_t;
        using StartTimeInSec = int32_t;

        struct ImagingPoint {
            WellPosition wellPosition{};
            PointID pointID{};

            auto operator==(const ImagingPoint& other) const -> bool {
                if(wellPosition!=other.wellPosition) return false;
                if(pointID != other.pointID) return false;
                return true;
            }
            auto operator<(const ImagingPoint& other) const -> bool {
                if (wellPosition.length() < other.wellPosition.length()) {return true;}
                if (wellPosition.length() > other.wellPosition.length()) {return false;}
                if (wellPosition < other.wellPosition) return true;
                if (wellPosition > other.wellPosition) return false;

                if (pointID.length() < other.pointID.length()) {return true;}
                if (pointID.length() > other.pointID.length()) {return false;}
                if (pointID < other.pointID) return true;
                if (pointID > other.pointID) return false;

                return false;
            }
        };

        ThumbnailViewerControl();
        ~ThumbnailViewerControl();

        auto SetCurrentImagingPoint(const ImagingPoint& imagingPoint) -> void;
        auto GetCurrentImagingPoint() const -> const ImagingPoint&;
        auto ClearCurrentImagingPoint() -> void;

        auto IsPointExist(const ImagingPoint& target) const -> bool;

        auto ClearAcquisitionSequences() -> void;
        auto SetDataPath(const ImagingPoint& target, const DataPath& path) -> void;
        auto GetDataPath(const ImagingPoint& target) const -> DataPath;
        auto GetDataPath() const -> DataPath;

        auto GetAcquisitionSequences() const -> const QMap<ImagingPoint, DataPath>&;

        auto SetStartTimes(const QMap<FrameIndex, StartTimeInSec>& startTimes) -> void;
        auto GetStartTime(const FrameIndex& frameIndex) const -> StartTimeInSec;
        auto GetStartTimeCount() const -> int32_t;

        auto GetAcquisitionDataName(const ImagingPoint& key) const -> QString;
        auto GetAcquisitionDataName() const -> QString;

        auto SetCurrentTimeFrameIndex(const FrameIndex& frameIndex) -> void;
        auto GetCurrentTimeFrameIndex() const -> const FrameIndex&;
        auto SetSyncMode(const bool& sync) -> bool;
        auto GetSyncMode() const -> const bool&;
        auto SetCurrentModality(const ThumbnailModality& modality) -> void;
        auto GetCurrentModality() const -> const ThumbnailModality&;

        auto SetCurrentFrameIndexAndModality(int32_t frameIndex, const ThumbnailModality& modality) -> void;

        auto SetThumbnailImage() -> bool;
        auto GetThumbnailImage() -> std::shared_ptr<QImage>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
