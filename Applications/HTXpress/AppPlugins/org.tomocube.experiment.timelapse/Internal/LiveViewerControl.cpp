#include <InstrumentController.h>
#include <LiveImageAcquisition.h>
#include <MotionController.h>

#include "InstrumentUpdater.h"
#include "MotionUpdater.h"
#include "LiveViewerControl.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    using LiveImagingPlugin = Plugins::LiveImageAcquisition::Plugin;

    struct LiveViewerControl::Impl {
        std::shared_ptr<LiveImagingPlugin> liveImaging{ new LiveImagingPlugin() };
    };

    LiveViewerControl::LiveViewerControl() : d{ new Impl } {
    }

    LiveViewerControl::~LiveViewerControl() {
    }

    auto LiveViewerControl::GetLatestImage(QImage& image) -> bool {
        return d->liveImaging->GetLatest(image);
    }

    auto LiveViewerControl::MoveSampleStage(int32_t posX, int32_t posY) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveRelativePixels(posX, posY);
    }

    auto LiveViewerControl::SetViewMode(AppEntity::Modality modality, int32_t channel) -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return [&]()->bool {
            switch(modality) {
            case AppEntity::Modality::BF:
                return controller.StartLiveBF();
            case AppEntity::Modality::FL:
                return controller.StartLiveFL(channel);
            }
            return false;
        }();
    }
}
