#pragma once
#include <memory>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class PreviewViewerControl {
    public:
        PreviewViewerControl();
        ~PreviewViewerControl();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}