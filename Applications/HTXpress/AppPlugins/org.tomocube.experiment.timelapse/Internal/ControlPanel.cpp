#define LOGGER_TAG "[ControlPanel]"
#include <TCLogger.h>
#include <MessageDialog.h>

#include <System.h>
#include <SystemStatus.h>

#include "ExperimentIOObserver.h"
#include "InstrumentObserver.h"
#include "RunExperimentObserver.h"
#include "ImagingPlanObserver.h"

#include "Utility.h"
#include "FLConditionParser.h"
#include "ControlPanelControl.h"
#include "ControlPanel.h"
#include "ui_ControlPanel.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    using ModalityType = AppComponents::ImagingConditionPanel::ModalityType;
    using FLChannelConfig = AppComponents::ImagingConditionPanel::FLChannelConfig;
    using ImagingStatus = AppComponents::TimelapseImagingPanel::ImagingStatus;

    struct ControlPanel::Impl {
        Ui::ControlPanel ui;
        ControlPanelControl control;

        int32_t currentSequence{};
        bool imagingInProgress{};

        InstrumentObserver* instrumentObserver{ nullptr };
        ExperimentIOObserver* experimentIOObserver{ nullptr };
        RunExperimentObserver* experimentObserver{ nullptr };
        ImagingPlanObserver* planObserver{ nullptr };
    };

    ControlPanel::ControlPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.baseWidget->setObjectName("panel");
        d->ui.stopBtn->setObjectName("bt-tsx-stop");
        d->ui.doneButton->setObjectName("bt-tsx-completed");
        d->ui.divider->setObjectName("line-divider");
        d->ui.progressLabel->setObjectName("label-h5");

        d->instrumentObserver = new InstrumentObserver(this);
        d->experimentIOObserver = new ExperimentIOObserver(this);
        d->experimentObserver = new RunExperimentObserver(this);
        d->planObserver = new ImagingPlanObserver(this);

        d->ui.tabWidget->setCurrentIndex(0);

        d->ui.resumeBtn->hide();
        d->ui.pauseBtn->hide();
        d->ui.statusLabel->hide();

        //Acquisition Panel
        d->ui.acquisitionPanel->SetPanelType(AppComponents::TimelapseImagingPanel::PanelType::Acquisition);

        connect(d->experimentIOObserver, SIGNAL(sigUpdate(AppEntity::Experiment::Pointer, bool)), this,
                SLOT(onUpdateExperiment(AppEntity::Experiment::Pointer, bool)));
        connect(d->experimentObserver, SIGNAL(sigError(const QString&)), this, SLOT(onExperimentStoppedByError(const QString&)));
        connect(d->experimentObserver, SIGNAL(sigUpdateMessage(const QString&)), this, SLOT(onUpdateMessage(const QString&)));
        connect(d->experimentObserver, &RunExperimentObserver::sigCurrentRunningSequence, this, &ControlPanel::onCurrentRunningSequenceIndex);

        connect(d->ui.stopBtn, &QPushButton::clicked, this, &ControlPanel::onStopExperiment);
        connect(d->ui.doneButton, &QPushButton::clicked, this, &ControlPanel::onDoneExperiment);
    }

    ControlPanel::~ControlPanel() {
    }

    auto ControlPanel::FinishImaging() -> void {
        d->imagingInProgress = false;
        disconnect(d->experimentObserver,
                   SIGNAL(sigUpdateProgress(const double, const int, const int)),
                   nullptr,
                   nullptr);

        d->currentSequence = 0;
        d->ui.statusLabel->hide();
        d->ui.progressBar->setValue(0);
        d->ui.progressBar->setMaximum(0);
        emit sigFinished();
    }

    void ControlPanel::onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool singleRun) {
        d->ui.acquisitionControllerStackedWidget->setCurrentWidget(d->ui.acquiringPage);

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto vesselIndex = sysStatus->GetCurrentVesselIndex();

        // single imaging 설정에 있는 channel 정보로 색 설정
        QMap<int32_t, QColor> flChannelColors;
        auto imagingCondition = experiment->GetSingleImagingCondition(AppEntity::ImagingType::FL2D);
        if (imagingCondition == nullptr) {
            imagingCondition = experiment->GetSingleImagingCondition(AppEntity::ImagingType::FL3D);
        }

        if (imagingCondition) {
            auto flCondition = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(imagingCondition);
            if (flCondition) {
                for (auto channel : flCondition->GetChannels()) {
                auto color = flCondition->GetColor(channel);
                flChannelColors.insert(channel, QColor(color.red, color.green, color.blue));
                }
            }
        }

        d->ui.acquisitionPanel->SetFLChannelsColor(flChannelColors);
                        
        d->ui.acquisitionPanel->SetScenario(experiment->GetScenario(vesselIndex));
        d->ui.acquisitionPanel->ResetImagingStatus();

        if(!d->control.SetStartTimesInSec(d->ui.acquisitionPanel->GetStartTimes())) {
            QLOG_INFO() << "Failed to set start times to thumbnail viewer.";
        }

        connect(d->experimentObserver, SIGNAL(sigUpdateProgress(const double, const int, const int)), this, 
                SLOT(onUpdateTimelapseExperimentProgress(const double, const int, const int)));

        if(d->control.RunExperiment(singleRun)) {
            d->ui.progressBar->setMaximum(d->ui.acquisitionPanel->GetTimeframes().size());
            d->imagingInProgress = true;
        }
    }

    void ControlPanel::onExperimentStoppedByError(const QString& message) {
        d->ui.acquisitionPanel->SetImagingStatus(d->currentSequence, ImagingStatus::Failed);

        const auto str = [=]()->QString {
            QString str = "Experiment has stopped:";
            if(message.isEmpty()) return str;
            str.append("\n");
            str.append(message);
            return str;
        }();

        TC::MessageDialog::warning(this, tr("Experiment"), tr("%1").arg(str));

        FinishImaging();
    }

    void ControlPanel::onUpdateMessage(const QString& message) {
        if(d->imagingInProgress) {
            if(d->ui.statusLabel->isHidden()) d->ui.statusLabel->show();
            d->ui.statusLabel->setText(message);
        }
    }

    void ControlPanel::onUpdateTimelapseExperimentProgress(const double progress, const int elapsedSeconds,
                                                           const int remainSeconds) {
        const auto timeFrames = d->ui.acquisitionPanel->GetTimeframes();

        if(progress >= 1.0) {
            d->ui.acquisitionPanel->SetImagingStatus(timeFrames.last(), ImagingStatus::Done);
            d->ui.acquisitionPanel->SetLeftTime(0);
            d->ui.acquisitionControllerStackedWidget->setCurrentWidget(d->ui.donePage);
        } else {
            d->ui.acquisitionPanel->SetAcquisitionTime(elapsedSeconds);
            d->ui.acquisitionPanel->SetLeftTime(remainSeconds);
        }
    }

    void ControlPanel::onStopExperiment() {
        d->control.StopExperiment();
        FinishImaging();
    }

    void ControlPanel::onDoneExperiment() {
        d->ui.acquisitionPanel->SetImagingStatus(d->currentSequence, ImagingStatus::Done);
        FinishImaging();
    }

    void ControlPanel::onCurrentRunningSequenceIndex(int currentSequenceIdx) {
        auto column = currentSequenceIdx + 1;
        if(column != d->currentSequence && d->imagingInProgress) {
            d->ui.acquisitionPanel->ChangeCurrentSequenceColumn(column);
            d->ui.acquisitionPanel->SetImagingStatus(d->currentSequence, ImagingStatus::Done);
            d->ui.acquisitionPanel->SetImagingStatus(column, ImagingStatus::InProgress);

            d->ui.progressBar->setValue(column);

            d->currentSequence = currentSequenceIdx + 1;
        }
    }
}
