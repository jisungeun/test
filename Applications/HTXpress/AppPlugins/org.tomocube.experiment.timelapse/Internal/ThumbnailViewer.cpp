#define LOGGER_TAG "[ThumbnailViewer]"
#include <QMessageBox>
#include <QTimer>

#include <TCLogger.h>
#include <System.h>
#include <SystemStatus.h>
#include <TCFDataRepo.h>
#include <DataMisc.h>
#include <AcquisitionSequenceReaderIni.h>
#include <AcquisitionSequence.h>

#include "ThumbnailViewerControl.h"
#include "ThumbnailViewer.h"

#include "AcquisitionDataObserver.h"
#include "ui_ThumbnailViewer.h"

#include "ExperimentIOObserver.h"
#include "ThumbnailObserver.h"

using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    using ThumbnailPanel = AppComponents::ThumbnailviewPanel::ThumbnailPanel;

    struct ThumbnailViewer::Impl {
        Ui::ThumbnailViewer ui;
        ThumbnailViewerControl control;

        // TODO don't use ExpIOObserver for detecting initializing time
        ExperimentIOObserver *experimentIoObserver{nullptr};
        // TODO don't use AcqDataObserver for getting dataFolder information
        AcquisitionDataObserver *acquisitionDataObserver{nullptr};
        ThumbnailObserver *thumbnailObserver{nullptr};

        QTimer timer;
        const int32_t timerDurationMSec{1000};

        auto Convert(HTXpress::AppComponents::ThumbnailviewPanel::ModalityIndex modality)->ThumbnailModality;
        auto Convert(AppEntity::Modality modality)->HTXpress::AppComponents::ThumbnailviewPanel::ModalityIndex;
        auto Convert(TC::IO::AcquisitionSequence::Modality modality)->ThumbnailModality;
        auto ChangeTimerState(bool resume) -> void;
    };

    ThumbnailViewer::ThumbnailViewer(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui.setupUi(this);

        d->experimentIoObserver = new ExperimentIOObserver(this);
        d->acquisitionDataObserver = new AcquisitionDataObserver(this);
        d->thumbnailObserver = new ThumbnailObserver(this);

        connect(d->experimentIoObserver, &ExperimentIOObserver::sigUpdate, this, &Self::onUpdateExperiment);

        connect(d->acquisitionDataObserver, &AcquisitionDataObserver::sigDataAdded, this, &Self::onAddAcquisitionData);

        connect(d->thumbnailObserver, &ThumbnailObserver::sigUpdateImagingPoint, this, &Self::onSetCurrentImagingPoint);
        connect(d->thumbnailObserver, &ThumbnailObserver::sigUpdateThumbnailImage, this, &Self::onSetImage);
        connect(d->thumbnailObserver, &ThumbnailObserver::sigUpdateStartTimesInSec, this, &Self::onSetStartTimes);

        connect(d->ui.view, &ThumbnailPanel::sigImageIndexChanged, this, &Self::onChangeSliderIndex);
        connect(d->ui.view, &ThumbnailPanel::sigModalityChanged, this, &Self::onChangeModality);
        connect(d->ui.view, &ThumbnailPanel::sigSyncModeChanged, this, &Self::onChangeSyncMode);

        connect(&d->timer, &QTimer::timeout, this, &Self::onRequestUpdateImage);
        connect(this, &Self::deleteLater, &d->timer, &QTimer::deleteLater);

        d->timer.start(d->timerDurationMSec);
    }

    ThumbnailViewer::~ThumbnailViewer() = default;

    void ThumbnailViewer::onRequestUpdateImage() {
        if(*(d->control.GetThumbnailImage()) != d->ui.view->GetCurrentImage()) {
            d->control.SetThumbnailImage();
        }
    }

    void ThumbnailViewer::onSetImage(int32_t frameIndex, AppEntity::Modality modality, const QImage& image) {
        d->ui.view->SetCurrentImageName(d->control.GetAcquisitionDataName());
        const auto sliderIndex = frameIndex+1;
        d->ui.view->SetCurrentSliderIndex(sliderIndex);
        d->ui.view->SetCurrentModality(d->Convert(modality));
        d->ui.view->SetCurrentTimeInSec(d->control.GetStartTime(frameIndex));
        d->ui.view->SetCurrentImage(image, true);
    }

    void ThumbnailViewer::onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool singleRun) {
        Q_UNUSED(singleRun)
        Q_UNUSED(experiment)
        // TODO init here?
        // 여기서 초기화하는 건 위험하다.
        // 데이타 설정이 먼저되고 호출되어 다시 초기화시킬 수 있다.
        d->ui.view->Clear();
        d->control.ClearAcquisitionSequences();
        d->control.ClearCurrentImagingPoint();
    }

    void ThumbnailViewer::onAddAcquisitionData(const QString& fileFullPath) {
        const auto pointID = DataMisc::GetTimelapsePointID(fileFullPath);
        const auto well = DataMisc::GetWellName(fileFullPath);
        const auto folderPath = DataMisc::GetDataFolderPath(fileFullPath);

        if(!d->control.IsPointExist({well,pointID})) {
            d->control.SetDataPath({well, pointID}, folderPath);
        }
    }

    void ThumbnailViewer::onSetCurrentImagingPoint(const QString& wellPosition, const QString& pointID) {
        d->control.SetCurrentImagingPoint({wellPosition, pointID});
        d->ui.view->SetTotalSequenceCount(d->control.GetStartTimeCount());

        if(wellPosition.isEmpty() || pointID.isEmpty()) {
            d->ui.view->Clear(false);
        }
    }

    void ThumbnailViewer::onSetStartTimes(const QMap<int32_t, int32_t>& startTimes) {
        d->control.SetStartTimes(startTimes);
    }

    void ThumbnailViewer::onChangeSliderIndex(int32_t sliderIndex) {
        // frame count : n
        // slider index: 1, 2, 3, 4, ..., n
        // frame index : 0, 1, 2, 3, ..., n-1

        const auto frameIndex = sliderIndex-1;
        d->control.SetCurrentTimeFrameIndex(frameIndex);
    }

    void ThumbnailViewer::onChangeModality(HTXpress::AppComponents::ThumbnailviewPanel::ModalityIndex modality) {
        d->control.SetCurrentModality(d->Convert(modality));
    }

    void ThumbnailViewer::onChangeSyncMode(bool sync) {
        if(!sync) {
            const auto frameIndex = d->ui.view->GetCurrentSliderIndex() - 1;
            const auto modality = d->ui.view->GetCurrentModality();
            d->control.SetCurrentFrameIndexAndModality(frameIndex, d->Convert(modality));
        }
        d->control.SetSyncMode(sync);
    }

    auto ThumbnailViewer::Impl::Convert(HTXpress::AppComponents::ThumbnailviewPanel::ModalityIndex modality) -> ThumbnailModality {
        auto convertedModality = ThumbnailModality::HT;
        switch (modality) {
        case AppComponents::ThumbnailviewPanel::ModalityIndex::BF: 
            convertedModality = ThumbnailModality::BF;
            break;
        case AppComponents::ThumbnailviewPanel::ModalityIndex::HT: 
            convertedModality = ThumbnailModality::HT;
            break;
        case AppComponents::ThumbnailviewPanel::ModalityIndex::FL:
            convertedModality = ThumbnailModality::FL;
            break;
        }
        return convertedModality;
    }

    auto ThumbnailViewer::Impl::Convert(AppEntity::Modality modality) -> HTXpress::AppComponents::ThumbnailviewPanel::ModalityIndex {
        auto convertedModality = AppComponents::ThumbnailviewPanel::ModalityIndex::HT;
        switch (modality) {
            case AppEntity::Modality::HT: convertedModality = AppComponents::ThumbnailviewPanel::ModalityIndex::HT; break;
            case AppEntity::Modality::FL: convertedModality = AppComponents::ThumbnailviewPanel::ModalityIndex::FL; break;
            case AppEntity::Modality::BF: convertedModality = AppComponents::ThumbnailviewPanel::ModalityIndex::BF; break;
        }
        return convertedModality;
    }

    auto ThumbnailViewer::Impl::Convert(TC::IO::AcquisitionSequence::Modality modality) -> ThumbnailModality {
        auto convertedModality = ThumbnailModality::HT;
        switch (modality) {
        case TC::IO::AcquisitionSequence::Modality::BF: 
            convertedModality = ThumbnailModality::BF;
            break;
        case TC::IO::AcquisitionSequence::Modality::HT: 
            convertedModality = ThumbnailModality::HT;
            break;
        case TC::IO::AcquisitionSequence::Modality::FLCH0:
        case TC::IO::AcquisitionSequence::Modality::FLCH1:
        case TC::IO::AcquisitionSequence::Modality::FLCH2:
        case TC::IO::AcquisitionSequence::Modality::FLCH3:
            convertedModality = ThumbnailModality::FL;
            break;
        }
        return convertedModality;
    }

    auto ThumbnailViewer::Impl::ChangeTimerState(bool resume) -> void {
        QLOG_DEBUG() << "Change TimerState" << resume;
        if(resume) {
            timer.start(timerDurationMSec);
        }
        else timer.stop();
    }
}
