#pragma once

#include <memory>

#include <QWidget>

#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class DataExplorer : public QWidget {
        Q_OBJECT

    public:
        DataExplorer(QWidget* parent = nullptr);
        ~DataExplorer();

        auto InitUI()->void;

    protected slots:
        void onUpdateExperiment(AppEntity::Experiment::Pointer experiment);

        void onAddAcquisitionData(const QString& fileFullPath);
        void onUpdateAcquisitionData(const QString& fileFullPath);
        void onDeleteAcquisitionData(const QString& fileFullPath);
        void onDeleteAcquisitionDataFolder(const QString& fileFullPath);

    signals:
        void sigGotoExpSetup();
        void sigGotoDataNavigation();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}