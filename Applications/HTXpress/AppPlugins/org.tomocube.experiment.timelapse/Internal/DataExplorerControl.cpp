#include <SystemStatus.h>
#include <TCFDataRepo.h>
#include <InstrumentController.h>
#include <AcquisitionDataController.h>
#include <AcquisitionDataPresenter.h>
#include <DataMisc.h>
#include <SessionManager.h>

#include "MotionUpdater.h"
#include "InstrumentUpdater.h"
#include "DataExplorerControl.h"
#include "AcquisitionDataUpdater.h"
#include "MotionObserver.h"

using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;
using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct DataExplorerControl::Impl {
        std::shared_ptr<Interactor::AcquisitionDataPresenter> acquisitionDataPresenter;
    };

    DataExplorerControl::DataExplorerControl() : d{new Impl} {
        d->acquisitionDataPresenter.reset(new Interactor::AcquisitionDataPresenter(AcquisitionDataUpdater::GetInstance().get()));
    }

    DataExplorerControl::~DataExplorerControl() {
    }

    auto DataExplorerControl::InstallAcquisitionDataMonitor() -> bool {
        auto controller = Interactor::AcquisitionDataController(d->acquisitionDataPresenter.get());
        return controller.InstallDataMonitor();
    }

    auto DataExplorerControl::ScanAcquisitionData() -> bool {
        auto controller = Interactor::AcquisitionDataController(d->acquisitionDataPresenter.get());
        if (!controller.InstallDataScanner()) return false;
        return controller.ScanExperiment();
    }

    auto DataExplorerControl::GetAcquisitionDataFromDataRepo() -> QList<AcquisitionData> {
        QList<AcquisitionData> acquisitionDataList;

        const auto systemStatus = AppEntity::SystemStatus::GetInstance();
        const auto sessionManager = AppEntity::SessionManager::GetInstance();
        const auto dataRepo = TCFDataRepo::GetInstance();
        auto specimens = dataRepo->GetSpecimenList(sessionManager->GetID(), systemStatus->GetProjectTitle(), systemStatus->GetExperiment()->GetTitle());
        for (auto specimenIt = specimens.begin(); specimenIt != specimens.end(); ++specimenIt) {
            if (specimenIt.key().isEmpty()) continue;

            auto wells = specimenIt.value();
            TCFDataRepo::WellList::iterator wellIt;
            for (wellIt = wells.begin(); wellIt != wells.end(); ++wellIt) {
                if (wellIt.key().isEmpty()) continue;

                auto dataList = wellIt.value();
                TCFDataRepo::DataList::iterator dataIt;
                for (dataIt = dataList.begin(); dataIt != dataList.end(); ++dataIt) {
                    if (dataIt.key().isEmpty() || nullptr == dataIt.value()) continue;

                    acquisitionDataList << std::make_tuple(dataIt.value()->GetPath(), specimenIt.key(), wellIt.key(), dataIt.key(), dataIt.value()->GetStatus());
                }
            }
        }

        return acquisitionDataList;
    }

    auto DataExplorerControl::ParseUserName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetUserName(fileFullPath);
    }

    auto DataExplorerControl::ParseProjectName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetProjectName(fileFullPath);
    }

    auto DataExplorerControl::ParseExperimentName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetExperimentName(fileFullPath);
    }

    auto DataExplorerControl::ParseSpecimenName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetSpecimenName(fileFullPath);
    }

    auto DataExplorerControl::ParseWellName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetWellName(fileFullPath);
    }

    auto DataExplorerControl::ParseDataName(const QString& fileFullPath) const -> QString {
        return DataMisc::GetTcfBaseName(fileFullPath);
    }
}
