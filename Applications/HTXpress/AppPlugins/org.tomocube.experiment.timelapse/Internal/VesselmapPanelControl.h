#pragma once
#include <memory>

#include <Position.h>
#include <Location.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class VesselmapPanelControl {
    public:
        VesselmapPanelControl();
        ~VesselmapPanelControl();

        auto GetVesselPosition(const AppEntity::WellIndex& wellIdx, const AppEntity::Position& position, 
                               double& posX, double& posY, double& posZ)->void;
        auto GetVesselPosition(const AppEntity::Position& globalPosition, double& posX, double& posY, double& posZ)->void;
        auto GetWellPosition(const AppEntity::Position& globalPosition, double& posX, double& posY, double& posZ)->void;

        auto GetCurrentWell() const->AppEntity::WellIndex;

        auto RefreshCurrentPosition(AppEntity::WellIndex& wellIndex, AppEntity::Position& globalPosition)->bool;

        auto SetCurrentImagingPoint(const QString& wellPosition, const QString& pointId) -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}