#pragma once
#include <memory>

#include <Area.h>
#include <TimelapseSequence.h>
#include <ScanConfig.h>
#include <ChannelConfig.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class ControlPanelControl {
    public:
        using Sequence = AppComponents::TimelapseImagingPanel::TimelapseSequence;

        enum class ImageType {
            HT3D,
            HT2D,
            FL3D,
            FL2D,
            BFGray,
            BFColor
        };

        struct FLScanConfig {
            AppEntity::FLScanMode scanMode{ AppEntity::FLScanMode::Default };
            double scanBottom;
            double scanTop;
            double scanStep;
            double flFocusOffset;
            int32_t scanSteps;
        };

    public:
        ControlPanelControl();
        ~ControlPanelControl();

        auto RunExperiment(bool singleRun = false)->bool;
        auto StopExperiment()->bool;
        auto SetStartTimesInSec(const QMap<int32_t, int32_t>& startTimesInSec)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}