#define LOGGER_TAG "[VesselMapPanel]"
#include <QMessageBox>
#include <QDebug>

#include <TCLogger.h>

#include <VesselMapUtility.h>
#include <SystemStatus.h>

#include "ExperimentIOObserver.h"
#include "RunExperimentObserver.h"
#include "MotionObserver.h"
#include "InstrumentObserver.h"

#include "VesselmapPanelControl.h"
#include "VesselmapPanel.h"
#include "ui_VesselmapPanel.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    static auto _id = qRegisterMetaType<AppEntity::WellIndex>("AppEntity::WellIndex");

    struct VesselmapPanel::Impl {
        Ui::VesselmapPanel ui;
        VesselmapPanelControl control;

        ExperimentIOObserver* experimentIOObserver{ nullptr };
        RunExperimentObserver* runExperimentObserver{ nullptr };
        MotionObserver* motionObserver{ nullptr };
        InstrumentObserver* instrumentObserver{ nullptr };

        auto InitUI() -> void;
        auto ConvertRowColumnsToVesselMapWellIndex(const QList<AppEntity::RowColumn>& rowColumns) const -> QList<TC::WellIndex>;
        auto GetAcquisitionType(bool isTile) -> TC::AcquisitionType;
    };

    auto VesselmapPanel::Impl::InitUI() -> void {
        ui.view->SetViewMode(TC::ViewMode::TimelapseMode);

        ui.view->SetAcquisitionLocationDeleteEnabled(false);
    }

    VesselmapPanel::VesselmapPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);
        d->InitUI();

        d->experimentIOObserver = new ExperimentIOObserver(this);
        d->runExperimentObserver = new RunExperimentObserver(this);
        d->motionObserver = new MotionObserver(this);
        d->instrumentObserver = new InstrumentObserver(this);

        //Acquisition Positions
        connect(d->experimentIOObserver, SIGNAL(sigUpdate(AppEntity::Experiment::Pointer, bool)), this,
                SLOT(onUpdateExperiment(AppEntity::Experiment::Pointer, bool)));
        connect(d->runExperimentObserver, 
                SIGNAL(sigUpdateCurrentPosition(const AppEntity::WellIndex, const AppEntity::Position&)), 
                this,
                SLOT(onUpdatePosition(const AppEntity::WellIndex, const AppEntity::Position&)));
        connect(d->motionObserver, SIGNAL(sigUpdatePosition(const AppEntity::WellIndex, const AppEntity::Position&)), this,
                SLOT(onUpdatePosition(const AppEntity::WellIndex, const AppEntity::Position&)));
        connect(d->motionObserver, SIGNAL(sigUpdateGlobalPosition(const AppEntity::Position&)), this,
                SLOT(onUpdateGlobalPosition(const AppEntity::Position&)));
        connect(d->motionObserver, SIGNAL(sigUpdateCurrentWell(const AppEntity::WellIndex)), this,
                SLOT(onUpdateCurrentWell(const AppEntity::WellIndex)));
        connect(d->instrumentObserver, SIGNAL(sigVesselLoaded(bool)), this, SLOT(onUpdateVesselStatus(bool)));
        connect(d->instrumentObserver, SIGNAL(sigUpdateGlobalPosition(const AppEntity::Position&)), this,
                SLOT(onUpdateGlobalPosition(const AppEntity::Position&)));

        connect(d->ui.view, SIGNAL(sigChangeFocusedWell(TC::WellIndex, int32_t, int32_t)), this, 
                SLOT(onSetCurrentWell(TC::WellIndex, int32_t, int32_t)));
        connect(d->ui.view, SIGNAL(sigDoubleClickedWellCanvasPosition(double, double)), this, SLOT(onChangeSampleStagePosition(double, double)));
        connect(d->ui.view, SIGNAL(sigDoubleClickedLocationPosition(double, double, double)), this, SLOT(onChangeSampleStagePosition(double, double, double)));

        connect(d->ui.view, &TC::VesselMapWidget::sigImagingPointSelected, this, &Self::onImagingPointSelected);
    }

    VesselmapPanel::~VesselmapPanel() {
    }

    void VesselmapPanel::onRefreshList(AppEntity::WellIndex wellIdx,
                                       QMap<AppEntity::LocationIndex, AppEntity::Location::Pointer>& locationList) {
        //Todo Acquisition Position 목록 갱신 구현
        Q_UNUSED(wellIdx)
        Q_UNUSED(locationList)
    }

    void VesselmapPanel::onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool singleRun) {
        Q_UNUSED(singleRun)

        using Converter = AppComponents::VesselMapUtility::Converter;

        auto sysStatus = AppEntity::SystemStatus::GetInstance();

        const auto vesselIndex = sysStatus->GetCurrentVesselIndex();
        const auto& vessel = experiment->GetVessel();
        const auto& wellNames = experiment->GetWellNames(vesselIndex);
        const auto vesselmap = Converter::ToTCVesselMap(vesselIndex, experiment, vessel, wellNames);

        d->ui.view->ClearAll();
        d->ui.view->SetVesselMap(vesselmap);

        auto locations = experiment->GetAllLocations(vesselIndex);
        const auto wellList = locations.keys();
        for(auto wellIdx : wellList) {
            auto& locs = locations[wellIdx];
            for(auto iter=locs.begin(); iter!=locs.end(); iter++) {
                auto center = iter.value().GetCenter();
                auto area = iter.value().GetArea();
                auto type = d->GetAcquisitionType(iter.value().IsTile());
                d->ui.view->AddAcquisitionLocationByWellPos(wellIdx, 
                                                            iter.key(),
                                                            type,
                                                            center.toMM().x,
                                                            center.toMM().y,
                                                            center.toMM().z,
                                                            area.toMM().width,
                                                            area.toMM().height);
            }
        }

        const auto wellGroupIndices = experiment->GetWellGroupIndices(vesselIndex);
        for(const auto& wellGroupIndex : wellGroupIndices) {
            const auto wellGroup = experiment->GetWellGroup(vesselIndex, wellGroupIndex);
            QList<AppEntity::RowColumn> wellRowColumns;
            auto wells = wellGroup.GetWells();
            for(const auto &well : wells) {
                wellRowColumns.push_back(AppEntity::RowColumn(well.rowIdx, well.colIdx));
            }

            auto groupName = wellGroup.GetTitle();
            auto groupColor = QColor(wellGroup.GetColor().r,
                                     wellGroup.GetColor().g,
                                     wellGroup.GetColor().b,
                                     wellGroup.GetColor().a);

            d->ui.view->CreateNewGroup(wellGroupIndex, 
                                       groupName,
                                       groupColor,
                                       d->ConvertRowColumnsToVesselMapWellIndex(wellRowColumns));
        }
     }

    void VesselmapPanel::onUpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position) {
        double vesselX, vesselY, vesselZ;
        d->control.GetVesselPosition(wellIdx, position, vesselX, vesselY, vesselZ);
        d->ui.view->SetObjectiveLensPos(vesselX, vesselY, vesselZ);
        d->ui.view->SetTileImagingAreaCenter(position.toMM().x, position.toMM().y, position.toMM().z);
        d->ui.view->SetSelectedWell(wellIdx);
    }

    void VesselmapPanel::onUpdateGlobalPosition(const AppEntity::Position& position) {
        double vesselX, vesselY, vesselZ;
        d->control.GetVesselPosition(position, vesselX, vesselY, vesselZ);
        d->ui.view->SetObjectiveLensPos(vesselX, vesselY, vesselZ);

        double wellX, wellY, wellZ;
        d->control.GetWellPosition(position, wellX, wellY, wellZ);
        d->ui.view->SetTileImagingAreaCenter(wellX, wellY, wellZ);
    }

    void VesselmapPanel::onUpdateCurrentWell(const AppEntity::WellIndex wellIndex) {
        d->ui.view->SetSelectedWell(wellIndex);
    }

    void VesselmapPanel::onUpdateVesselStatus(bool loaded) {
        setEnabled(loaded);

        if(loaded) d->ui.view->SetSelectedWell(0);
    }

    auto VesselmapPanel::Impl::ConvertRowColumnsToVesselMapWellIndex(const QList<AppEntity::RowColumn>& rowColumns) const -> QList<TC::WellIndex> {
        if (ui.view == nullptr) {
            return QList<TC::WellIndex>();
        }

        QList<TC::WellIndex> wells;
        for (auto& rc : rowColumns) {
            const auto index = ui.view->GetWellIndex(rc.first, rc.second);
            if(index != -1) wells << index;
        }

        return wells;
    }

    void VesselmapPanel::onSetCurrentWell(TC::WellIndex wellIndex, int32_t row, int32_t col) {
        Q_UNUSED(wellIndex)
        Q_UNUSED(row)
        Q_UNUSED(col)
    }

    void VesselmapPanel::onChangeSampleStagePosition(double xMM, double yMM) {
        // Todo: 일시정지 상태에서는 스테이지 이동
        Q_UNUSED(xMM)
        Q_UNUSED(yMM)
    }

    void VesselmapPanel::onChangeSampleStagePosition(double xMM, double yMM, double zMM) {
        // Todo: 일시정지 상태에서는 스테이지 이동
        Q_UNUSED(xMM)
        Q_UNUSED(yMM)
        Q_UNUSED(zMM)
    }

    void VesselmapPanel::onImagingPointSelected(const QString& wellPosition, const QString& pointID) {
        if(!d->control.SetCurrentImagingPoint(wellPosition, pointID)) {
            QLOG_INFO() << "Failed to set to selected imaging point";
        }
    }

    auto VesselmapPanel::Impl::GetAcquisitionType(bool isTile) -> TC::AcquisitionType {
        if(isTile) {
            return TC::AcquisitionType::Tile;
        }

        return TC::AcquisitionType::Point;
    }
}
