#include <QList>

#include "InstrumentObserver.h"
#include "InstrumentUpdater.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct InstrumentUpdater::Impl {
        QList<InstrumentObserver*> observers;
    };

    InstrumentUpdater::InstrumentUpdater() : d{new Impl} {
    }

    InstrumentUpdater::~InstrumentUpdater() {
    }

    auto InstrumentUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new InstrumentUpdater() };
        return theInstance;
    }

    auto InstrumentUpdater::UpdateFailed(const QString& message) -> void {
        for(auto observer : d->observers) {
            observer->UpdateFailed(message);
        }
    }

    auto InstrumentUpdater::UpdateProgress(double progress, const QString& message) -> void {
        for(auto observer : d->observers) {
            observer->UpdateProgress(progress, message);
        }
    }

    auto InstrumentUpdater::UpdateGlobalPosition(const AppEntity::Position& position) -> void {
        for(auto observer : d->observers) {
            observer->UpdateGlobalPosition(position);
        }
    }

    auto InstrumentUpdater::ReportAFFailed() -> void {
        for(auto observer : d->observers) {
            observer->ReportAFFailed();
        }
    }

    auto InstrumentUpdater::UpdateBestFocus(double posInMm) -> void {
        for(auto observer : d->observers) {
            observer->UpdateBestFocus(posInMm);
        }
    }

    auto InstrumentUpdater::AutoFocusEnabled(bool enable) -> void {
        for(auto observer : d->observers) {
            observer->EnableAutoFocus(enable);
        }
    }

    auto InstrumentUpdater::LiveStarted() -> void {
        for(auto observer : d->observers) {
            observer->LiveStarted();
        }
    }

    auto InstrumentUpdater::LiveStopped() -> void {
        for(auto observer : d->observers) {
            observer->LiveStopped();
        }
    }

    auto InstrumentUpdater::LiveImagingFailed(const QString& message) -> void {
        for(auto observer : d->observers) {
            observer->LiveImagingFailed(message);
        }
    }

    auto InstrumentUpdater::VesselLoaded() -> void {
        for(auto observer : d->observers) {
            observer->SetVesselLoaded(true);
        }
    }

    auto InstrumentUpdater::VesselUnloaded() -> void {
        for(auto observer : d->observers) {
            observer->SetVesselLoaded(false);
        }
    }

    auto InstrumentUpdater::Register(InstrumentObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto InstrumentUpdater::Deregister(InstrumentObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}
