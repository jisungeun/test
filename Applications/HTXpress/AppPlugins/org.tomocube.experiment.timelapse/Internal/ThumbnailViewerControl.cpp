#define LOGGER_TAG "[ThumbnailViewer]"
#include <TCLogger.h>

#include <QImage>

#include <ThumbnailController.h>
#include <ThumbnailPresenter.h>
#include <ThumbnailImageReaderPlugin.h>

#include "ThumbnailUpdater.h"
#include "ThumbnailObserver.h"

#include "ThumbnailViewerControl.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse {
    using ThumbnailImageReaderPlugin = Plugins::ThumbnailImageReader::ThumbnailImageReaderPlugin;

    struct ThumbnailViewerControl::Impl {
        ThumbnailImageReaderPlugin::Pointer thumbnailImageReader{std::make_shared<ThumbnailImageReaderPlugin>()};

        ImagingPoint currentImagingPoint;
        QMap<ImagingPoint, DataPath> acquisitionSequences;
        QMap<FrameIndex, StartTimeInSec> startTimes;

        struct ThumbnailImageConfig {
            ThumbnailModality modality{ThumbnailModality::HT};
            int32_t frameIndex{};
            int32_t startTimeInSec{};
        };

        bool syncMode{false};
        ThumbnailModality modality{ThumbnailModality::HT};
        int32_t frameIndex{};
        int32_t acquisitionCount{};

        auto GetDataName(const ImagingPoint& key) const -> QString;
        auto GetAcquisitionCountIndex() const -> int32_t;
        auto Convert(const ThumbnailModality& thumbnailModality) -> AppEntity::Modality;
        auto Convert(const AppEntity::Modality& entityModality) -> ThumbnailModality;
    };

    ThumbnailViewerControl::ThumbnailViewerControl() : d{std::make_unique<Impl>()} {
    }

    ThumbnailViewerControl::~ThumbnailViewerControl() = default;

    auto ThumbnailViewerControl::SetCurrentImagingPoint(const ImagingPoint& imagingPoint) -> void {
        d->currentImagingPoint = imagingPoint;
        SetThumbnailImage();
    }

    auto ThumbnailViewerControl::GetCurrentImagingPoint() const -> const ImagingPoint& {
        return d->currentImagingPoint;
    }

    auto ThumbnailViewerControl::ClearCurrentImagingPoint() -> void {
        d->currentImagingPoint = {};
    }

    auto ThumbnailViewerControl::IsPointExist(const ImagingPoint& target) const -> bool {
        return d->acquisitionSequences.contains(target);
    }

    auto ThumbnailViewerControl::ClearAcquisitionSequences() -> void {
        if (!d->acquisitionSequences.empty()) { d->acquisitionSequences.clear(); }
    }

    auto ThumbnailViewerControl::SetDataPath(const ImagingPoint& target, const DataPath& path) -> void {
        d->acquisitionSequences[target] = path;
    }

    auto ThumbnailViewerControl::GetDataPath(const ImagingPoint& target) const -> QString {
        if (IsPointExist(target)) { return d->acquisitionSequences[target]; }
        return "";
    }

    auto ThumbnailViewerControl::GetDataPath() const -> DataPath {
        return GetDataPath(d->currentImagingPoint);
    }

    auto ThumbnailViewerControl::GetAcquisitionSequences() const -> const QMap<ImagingPoint, DataPath>& {
        return d->acquisitionSequences;
    }

    auto ThumbnailViewerControl::SetStartTimes(const QMap<FrameIndex, StartTimeInSec>& startTimes) -> void {
        if (!d->startTimes.empty()) d->startTimes.clear();
        d->startTimes = startTimes;
    }

    auto ThumbnailViewerControl::GetStartTime(const FrameIndex& frameIndex) const -> StartTimeInSec {
        if (d->startTimes.contains(frameIndex)) { return d->startTimes[frameIndex]; }
        return 0;
    }

    auto ThumbnailViewerControl::GetStartTimeCount() const -> int32_t {
        return d->startTimes.size();
    }

    auto ThumbnailViewerControl::GetAcquisitionDataName(const ImagingPoint& key) const -> QString {
        return d->GetDataName(key);
    }

    auto ThumbnailViewerControl::GetAcquisitionDataName() const -> QString {
        return GetAcquisitionDataName(d->currentImagingPoint);
    }

    auto ThumbnailViewerControl::SetCurrentTimeFrameIndex(const FrameIndex& frameIndex) -> void {
        d->frameIndex = frameIndex;
        SetThumbnailImage();
    }

    auto ThumbnailViewerControl::GetCurrentTimeFrameIndex() const -> const FrameIndex& {
        return d->frameIndex;
    }

    auto ThumbnailViewerControl::SetSyncMode(const bool& sync) -> bool {
        if (sync == d->syncMode) return false;
        d->syncMode = sync;
        SetThumbnailImage();
        return true;
    }

    auto ThumbnailViewerControl::GetSyncMode() const -> const bool& {
        return d->syncMode;
    }

    auto ThumbnailViewerControl::SetCurrentModality(const ThumbnailModality& modality) -> void {
        d->modality = modality;
        SetThumbnailImage();
    }

    auto ThumbnailViewerControl::GetCurrentModality() const -> const ThumbnailModality& {
        return d->modality;
    }

    auto ThumbnailViewerControl::SetCurrentFrameIndexAndModality(int32_t frameIndex, const ThumbnailModality& modality) -> void {
        d->frameIndex = frameIndex;
        d->modality = modality;
    }

    auto ThumbnailViewerControl::SetThumbnailImage() -> bool {
        const auto dataFullPath = d->acquisitionSequences[d->currentImagingPoint];

        auto updater = ThumbnailUpdater::GetInstance();
        auto presenter = Interactor::ThumbnailPresenter(updater.get());
        auto controller = Interactor::ThumbnailController(&presenter);

        return controller.SetThumbnailImage(dataFullPath, d->frameIndex, d->Convert(d->modality), d->syncMode);
    }

    auto ThumbnailViewerControl::GetThumbnailImage() -> std::shared_ptr<QImage> {
        const auto dataFullPath = d->acquisitionSequences[d->currentImagingPoint];

        if (!d->syncMode) {
            d->thumbnailImageReader->SetDataPath(dataFullPath);
            d->thumbnailImageReader->SetFrameIndex(d->frameIndex);
            d->thumbnailImageReader->SetModality(d->Convert(d->modality));
            return d->thumbnailImageReader->GetThumbnailImage();
        }

        // sync active일 때
        d->thumbnailImageReader->SetDataPath(dataFullPath);
        auto [i, m, img] = d->thumbnailImageReader->GetLastThumbnailImageInfo(); // 최신 frame index와 modality 정보를 받아옴
        SetCurrentFrameIndexAndModality(i, d->Convert(m));
        return img;
    }

    auto ThumbnailViewerControl::Impl::GetDataName(const ImagingPoint& key) const -> QString {
        // data path format: ~~~~~~.GroupName.WellPosition.T###P###
        // return: GroupName-WellPosition-T###P### format
        const auto fullPath = acquisitionSequences[key];
        auto dataName = fullPath.section(".", -3, -1);
        dataName.replace(".", "-");
        return dataName;
    }

    auto ThumbnailViewerControl::Impl::Convert(const ThumbnailModality& thumbnailModality) -> AppEntity::Modality {
        switch (thumbnailModality) {
            case ThumbnailModality::BF: return AppEntity::Modality::BF;
            case ThumbnailModality::FL: return AppEntity::Modality::FL;
            case ThumbnailModality::HT: return AppEntity::Modality::HT;
        }
        return AppEntity::Modality::HT;
    }

    auto ThumbnailViewerControl::Impl::Convert(const AppEntity::Modality& entityModality) -> ThumbnailModality {
        switch (entityModality) {
            case AppEntity::Modality::BF: return ThumbnailModality::BF;
            case AppEntity::Modality::FL: return ThumbnailModality::FL;
            case AppEntity::Modality::HT: return ThumbnailModality::HT;
        }
        return ThumbnailModality::HT;
    }
}
