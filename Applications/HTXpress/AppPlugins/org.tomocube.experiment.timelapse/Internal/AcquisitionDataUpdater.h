#pragma once

#include <memory>

#include <IAcquisitionDataView.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class AcquisitionDataObserver;

    class AcquisitionDataUpdater : public Interactor::IAcquisitionDataView {
    public:
        using Pointer = std::shared_ptr<AcquisitionDataUpdater>;

    protected:
        AcquisitionDataUpdater();

    public:
        ~AcquisitionDataUpdater() override;

        static auto GetInstance()->Pointer;

        auto ScannedData(const QString& user, const QString& project, const QString& experiment) -> void override;
        auto AddedData(const QString& fileFullPath) -> void override;
        auto UpdatedData(const QString& fileFullPath) -> void override;
        auto DeletedData(const QString& fileFullPath) -> void override;
        auto DeletedDataRootFolder(const QString& fileFullPath) -> void override;

    protected:
        auto Register(AcquisitionDataObserver* observer)->void;
        auto Deregister(AcquisitionDataObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class AcquisitionDataObserver;
    };
}