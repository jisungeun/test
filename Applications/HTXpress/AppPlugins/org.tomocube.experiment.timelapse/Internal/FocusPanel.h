#pragma once
#include <memory>
#include <QWidget>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class FocusPanel : public QWidget {
        Q_OBJECT
    public:
        FocusPanel(QWidget* parent = nullptr);
        ~FocusPanel();

    protected slots:
        void onEnableAutofocus(bool enable);
        void onSelected(int index);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}