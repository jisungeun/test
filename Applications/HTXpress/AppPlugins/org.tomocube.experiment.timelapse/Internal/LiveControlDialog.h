#pragma once

#include <enum.h>
#include <memory>

#include <QDialog>

#include <SystemStatus.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class LiveControlDialog : public QDialog {
        Q_OBJECT
    public:
        LiveControlDialog(QWidget* parent = nullptr);
        ~LiveControlDialog();

        auto SetMode(AppEntity::ImagingMode mode)->void;
        auto GetMode()->AppEntity::ImagingMode;

        auto SetIntensity(uint32_t value)->void;
        auto GetIntensity()->uint32_t;

        auto SetExposure(uint32_t value)->void;
        auto GetExposure()->uint32_t;

        auto SetGain(double value)->void;
        auto GetGain()->double;

        auto SetExposureRange(uint32_t minVal, uint32_t maxVal)->void;
        auto ShowBoostMode(bool show)->void;

        auto SetEnabledSaveButton(bool enabled)->void;

    protected slots:
        void onChangedSliderValue(int value);
        void onChangedSpinBoxValue(double value);
        void onBoost(bool checked);
        void onChangedSaturationMode(bool checked);

    signals:
        void sigLiveImageConditionChanged(const uint32_t intensity, const  uint32_t exposure, const double gain);
        void sigLiveSaturationActivated(bool activated);
        void sigAcquisitionConditionRequested();
        void sigSaveRequested();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}