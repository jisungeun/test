#define LOGGER_TAG "[DataExplorer]"
#include <QMessageBox>

#include <TCLogger.h>

#include <TCFDataRepo.h>
#include <SystemStatus.h>
#include <DataMisc.h>

#include "InstrumentObserver.h"
#include "ExperimentIOObserver.h"
#include "AcquisitionDataObserver.h"
#include "DataExplorerControl.h"
#include "DataExplorer.h"
#include "ui_DataExplorer.h"

using HTXpress::AppComponents::ProcessedDataDefinition::TCFProcessingStatus;
using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;
using HTXpress::AppComponents::HTXDataManager::DataMisc;

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct DataExplorer::Impl {
        Ui::DataExplorer ui;
        DataExplorerControl control;

        InstrumentObserver* instrumentObserver{ nullptr };
        ExperimentIOObserver* experimentObserver{ nullptr };
        AcquisitionDataObserver* acquisitionDataObserver{ nullptr };

        auto GenerateAcquisitionListItem(const QString& tcfFilePath, const QString& specimen, const QString& well, const QString& acqData)-> AppComponents::AcquisitionListPanel::AcquisitionDataIndex;
        auto Convert(TCFProcessingStatus state)->AppComponents::AcquisitionListPanel::ProcessingStatus;
    };

    auto DataExplorer::Impl::GenerateAcquisitionListItem(const QString& tcfFilePath, const QString& specimen, const QString& well, const QString& acqData) -> AppComponents::AcquisitionListPanel::AcquisitionDataIndex {
        AppComponents::AcquisitionListPanel::AcquisitionDataIndex acquisitionData;
        acquisitionData.SetGroupName(specimen);
        acquisitionData.SetWellPositionName(well);
        acquisitionData.SetAcquisitionDataName(acqData);
        acquisitionData.SetAcquisitionDataPath(tcfFilePath);

        const auto subStrings = acqData.section(".", 0, 1);
        if (!subStrings.isEmpty()) {
            acquisitionData.SetTimestamp(subStrings);
        }

        return acquisitionData;
    }

    auto DataExplorer::Impl::Convert(TCFProcessingStatus state) -> AppComponents::AcquisitionListPanel::ProcessingStatus {
        auto result = AppComponents::AcquisitionListPanel::ProcessingStatus::Invalid;
        if (state == +TCFProcessingStatus::NotYet || state == +TCFProcessingStatus::InProgress) {
            result = AppComponents::AcquisitionListPanel::ProcessingStatus::Unprocessed;
        } else if (state == +TCFProcessingStatus::Completed) {
            result = AppComponents::AcquisitionListPanel::ProcessingStatus::Processed;
        }

        return result;
    }

    DataExplorer::DataExplorer(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);

        InitUI();

        d->ui.baseWidget->setObjectName("panel");
        d->ui.vesselLoadBtn->setObjectName("bt-tsx-unloadVessel");
        d->ui.goToExpSetupBtn->setObjectName("bt-tsx-setupExperiment");
        d->ui.endBtn->setObjectName("bt-tsx-end");
        d->ui.goToDataNaviBtn->setObjectName("bt-tsx-dataNavigation");

        d->instrumentObserver = new InstrumentObserver(this);
        d->experimentObserver = new ExperimentIOObserver(this);
        d->acquisitionDataObserver = new AcquisitionDataObserver(this);

        connect(d->ui.goToExpSetupBtn, &QPushButton::clicked, this, &DataExplorer::sigGotoExpSetup);
        connect(d->ui.goToDataNaviBtn, &QPushButton::clicked, this, &DataExplorer::sigGotoDataNavigation);

        connect(d->experimentObserver, &ExperimentIOObserver::sigUpdate, this, &DataExplorer::onUpdateExperiment);

        connect(d->acquisitionDataObserver, &AcquisitionDataObserver::sigDataAdded, this, &DataExplorer::onAddAcquisitionData);
        connect(d->acquisitionDataObserver, &AcquisitionDataObserver::sigDataUpdated, this, &DataExplorer::onUpdateAcquisitionData);
        connect(d->acquisitionDataObserver, &AcquisitionDataObserver::sigDataDeleted, this, &DataExplorer::onDeleteAcquisitionData);
        connect(d->acquisitionDataObserver, &AcquisitionDataObserver::sigDataRootFolderDeleted, this, &DataExplorer::onDeleteAcquisitionDataFolder);

        if (!d->control.InstallAcquisitionDataMonitor()) {
            QLOG_ERROR() << "It fails to install acquisition data monitor";
        }
    }

    DataExplorer::~DataExplorer() {
    }

    auto DataExplorer::InitUI() -> void {
        d->ui.vesselLoadBtn->setDisabled(true);
        d->ui.endBtn->setDisabled(true);
    }

    void DataExplorer::onUpdateExperiment(AppEntity::Experiment::Pointer experiment) {
        Q_UNUSED(experiment)

        // DataRepo에서 현재 experiment의 데이터 목록 가져와서 data list에 표시
        d->ui.dataListView->ClearAll();
        for (auto& elem : d->control.GetAcquisitionDataFromDataRepo()) {
            auto [path, specimen, well, acquisitionData, status] = elem;

            d->ui.dataListView->AddAcquisitionData(
                d->GenerateAcquisitionListItem(path, specimen, well, acquisitionData), d->Convert(status)
            );    
        }

        d->ui.dataListView->SetDefaultTableOrder();
    }
    
    void DataExplorer::onAddAcquisitionData(const QString& fileFullPath) {
        const auto systemStatus = AppEntity::SystemStatus::GetInstance();
        const auto dataRepo = TCFDataRepo::GetInstance();

        const auto user = d->control.ParseUserName(fileFullPath);
        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);

        const auto acqData = dataRepo->GetData(user, project, experiment, specimen, well, dataName);

        if (acqData == nullptr) {
            QLOG_ERROR() << "Failed to add acquisition data."  << "Acquisition data pointer is nullptr";
            return;
        }

        d->ui.dataListView->AddAcquisitionData(d->GenerateAcquisitionListItem(fileFullPath, specimen, well, dataName));
    }

    void DataExplorer::onUpdateAcquisitionData(const QString& fileFullPath) {
        const auto systemStatus = AppEntity::SystemStatus::GetInstance();
        const auto dataRepo = TCFDataRepo::GetInstance();

        const auto user = d->control.ParseUserName(fileFullPath);
        const auto project = d->control.ParseProjectName(fileFullPath);
        const auto experiment = d->control.ParseExperimentName(fileFullPath);
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);

        const auto acqData = dataRepo->GetData(user, project, experiment, specimen, well, dataName);
        if (acqData == nullptr) {
            QLOG_ERROR() << "Failed to update acquisition data."  << "Acquisition data pointer is nullptr";
            return;
        }

        // data 획득 진행 상태 확인하여 list에 적용
        d->ui.dataListView->SetDataProcessingStatus(
            d->GenerateAcquisitionListItem(fileFullPath, specimen, well, dataName),
            d->Convert(acqData->GetStatus())
        );
    }

    void DataExplorer::onDeleteAcquisitionData(const QString& fileFullPath) {
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);
        d->ui.dataListView->SetDataProcessingStatus(d->GenerateAcquisitionListItem(fileFullPath, specimen, well, dataName), 
                                                    d->Convert(TCFProcessingStatus::NotYet));
    }

    void DataExplorer::onDeleteAcquisitionDataFolder(const QString& fileFullPath) {
        const auto specimen = d->control.ParseSpecimenName(fileFullPath);
        const auto well = d->control.ParseWellName(fileFullPath);
        const auto dataName = d->control.ParseDataName(fileFullPath);
        d->ui.dataListView->DeleteAcquisitionData(d->GenerateAcquisitionListItem(fileFullPath, specimen, well, dataName));
    }
}
