#pragma once
#include <memory>
#include <QWidget>
#include <QResizeEvent>

#include <AppEntityDefines.h>
#include <ChannelConfig.h>
#include <ImagingConfig.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class LiveViewer : public QWidget {
        Q_OBJECT

    public:
        LiveViewer(QWidget* parent = nullptr);
        ~LiveViewer() override;

    protected slots:
        void onModeSelected(AppEntity::Modality modality, int32_t channel);
        void onUpdateImage();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}