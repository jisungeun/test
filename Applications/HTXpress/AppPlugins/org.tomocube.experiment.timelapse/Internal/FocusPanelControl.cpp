#include <InstrumentController.h>
#include <MotionController.h>

#include "InstrumentUpdater.h"
#include "MotionUpdater.h"
#include "FocusPanelControl.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct FocusPanelControl::Impl {
    };

    FocusPanelControl::FocusPanelControl() : d{new Impl} {
    }

    FocusPanelControl::~FocusPanelControl() {
    }

    auto FocusPanelControl::PerformAF() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return controller.PerformAutoFocus();
    }

    auto FocusPanelControl::SetBestFocus() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return controller.SetBestFocusCurrent();
    }

    auto FocusPanelControl::MoveZ(double distUm) -> bool {
        auto updater = MotionUpdater::GetInstance();
        auto presenter = Interactor::MotionPresenter(updater.get());
        auto controller = Interactor::MotionController(&presenter);
        return controller.MoveRelativeUM(AppEntity::Axis::Z, distUm);
    }
}
