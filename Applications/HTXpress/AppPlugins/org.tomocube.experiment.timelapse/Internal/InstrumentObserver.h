#pragma once
#include <memory>
#include <QObject>

#include <Position.h>
#include <ImagingTimeTable.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class InstrumentObserver : public QObject {
        Q_OBJECT
    public:
        InstrumentObserver(QObject* parent=nullptr);
        ~InstrumentObserver();

        auto UpdateFailed(const QString& message)->void;
        auto UpdateProgress(double progress, const QString& message = QString())->void;

        auto UpdateGlobalPosition(const AppEntity::Position& position)->void;
        auto ReportAFFailed()->void;
        auto UpdateBestFocus(double posInMm)->void;
        auto EnableAutoFocus(bool enable) -> void;

        auto LiveStarted() -> void;
        auto LiveStopped() -> void;
        auto LiveImagingFailed(const QString& message) -> void;

        auto SetVesselLoaded(bool loaded)->void;

    signals:
        void sigFailed(const QString& message);
        void sigProgress(double progress, const QString& message);

        void sigUpdateGlobalPosition(const AppEntity::Position& position);
        void sigAFFailed();
        void sigUpdateBestFocus(double posInMm);
        void sigAFEnabled(bool enable);

        void sigLiveStarted();
        void sigLiveStopped();
        void sigLiveImagingFailed(const QString& message);

        void sigVesselLoaded(bool loaded);
    };
}