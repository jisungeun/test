#pragma once
#include <memory>
#include <QWidget>
#include <QMap>

#include <Location.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class PreviewViewer : public QWidget {
        Q_OBJECT

    public:
        PreviewViewer(QWidget* parent = nullptr);
        ~PreviewViewer();

    protected slots:

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}