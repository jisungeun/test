﻿#include <System.h>
#include <SystemStatus.h>

#include <ExperimentController.h>
#include <ExperimentIOController.h>
#include <InstrumentController.h>
#include <ImagingPlanPresenter.h>
#include <ThumbnailController.h>

#include "Utility.h"
#include "ImagingPlanUpdater.h"
#include "RunExperimentUpdater.h"
#include "ExperimentIOUpdater.h"
#include "InstrumentUpdater.h"
#include "ThumbnailUpdater.h"

#include "ControlPanelControl.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    struct ControlPanelControl::Impl {
    };

    ControlPanelControl::ControlPanelControl() : d{new Impl} {
    }

    ControlPanelControl::~ControlPanelControl() {
    }

    auto ControlPanelControl::RunExperiment(bool singleRun) -> bool {
        auto updater = RunExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter::GetInstance(updater.get());
        auto controller = Interactor::ExperimentController(presenter.get());
        return singleRun? controller.TestExperiment() : controller.RunExperiment();
    }

    auto ControlPanelControl::StopExperiment() -> bool {
        auto updater = RunExperimentUpdater::GetInstance();
        auto presenter = Interactor::ExperimentPresenter::GetInstance(updater.get());
        auto controller = Interactor::ExperimentController(presenter.get());
        return controller.StopExperiment();
    }

    auto ControlPanelControl::SetStartTimesInSec(const QMap<int32_t, int32_t>& startTimesInSec) -> bool {
        auto updater = ThumbnailUpdater::GetInstance();
        auto presenter = Interactor::ThumbnailPresenter(updater.get());
        auto controller = Interactor::ThumbnailController(&presenter);
        return controller.SetStartTimesInSec(startTimesInSec);
    }
}
