#pragma once

#include <memory>

#include <QList>

#include <TCFData.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse {
    class DataExplorerControl {
        using DataPath = QString;
        using SpecimenName = QString;
        using WellName = QString;
        using DataName = QString;
        using AcquisitionData = std::tuple<DataPath, SpecimenName, WellName, DataName, HTXpress::AppComponents::ProcessedDataDefinition::TCFProcessingStatus>; // <tcfPath, specimen name, well name, data name, processing status>

    public:
        DataExplorerControl();
        ~DataExplorerControl();

        auto InstallAcquisitionDataMonitor() -> bool;
        auto ScanAcquisitionData() -> bool;
        auto GetAcquisitionDataFromDataRepo() -> QList<AcquisitionData>;

        auto ParseUserName(const QString& fileFullPath) const -> QString;
        auto ParseProjectName(const QString& fileFullPath) const -> QString;
        auto ParseExperimentName(const QString& fileFullPath) const -> QString;
        auto ParseSpecimenName(const QString& fileFullPath) const -> QString;
        auto ParseWellName(const QString& fileFullPath) const -> QString;
        auto ParseDataName(const QString& fileFullPath) const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
