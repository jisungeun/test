#pragma once
#include <memory>
#include <QObject>

#include <ExperimentStatus.h>
#include <Position.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class RunExperimentObserver : public QObject{
        Q_OBJECT

    public:
        RunExperimentObserver(QObject* parent = nullptr);
        ~RunExperimentObserver() override;

        auto UpdateProgress(const UseCase::ExperimentStatus& status) -> void;
        auto NotifyStopped() -> void;
        auto UpdateProgressTime(const int32_t& elapsedTime, const int32_t& remainTime) -> void;
        auto UpdatePosition(const AppEntity::WellIndex& wellIndex, const AppEntity::Position& position) -> void;

    signals:
        void sigUpdateProgress(const double progress, const int elapsedSeconds, const int remainSeconds);
        void sigUpdateCurrentPosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position);
        void sigStopped();
        void sigError(const QString& message);
        void sigUpdateMessage(const QString& message);
        void sigCurrentRunningSequence(int);
    };
}
