﻿#pragma once

#include <memory>

#include <IThumbnailView.h>

#include "ThumbnailObserver.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse {
    class ThumbnailUpdater : public Interactor::IThumbnailView{
    public:
        using Pointer = std::shared_ptr<ThumbnailUpdater>;

    protected:
        ThumbnailUpdater();

    public:
        ~ThumbnailUpdater() override;

        static auto GetInstance() -> Pointer;

        auto UpdateImagingPoint(const QString& wellPosition, const QString& pointID) -> void override;
        auto UpdateStartTimesInSec(const QMap<int32_t, int32_t>& startTimes) -> void override;
        auto UpdateThumbnailImage(int32_t frameIndex, AppEntity::Modality modality, const QImage& image) -> void override;

        auto ReportError(const QString& message) -> void override;

    protected:
        auto Register(ThumbnailObserver* observer) -> void;
        auto Deregister(ThumbnailObserver* observer) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class ThumbnailObserver;
    };
}
