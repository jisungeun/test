#pragma once
#include <memory>
#include <QObject>

#include <Experiment.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class ExperimentIOObserver : public QObject {
        Q_OBJECT
    public:
        ExperimentIOObserver(QObject* parent);
        virtual ~ExperimentIOObserver();

        auto Update(AppEntity::Experiment::Pointer experiment, bool singleRun = false)->void;
        auto Error(const QString& message) -> void;

    signals:
        void sigUpdate(AppEntity::Experiment::Pointer experiment, bool singleRun);
        void sigError(const QString& message);
    };
}