#pragma once
#include <memory>
#include <QWidget>
#include <QMap>

#include <Location.h>
#include <Experiment.h>
#include <VesselMapExternalData.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class VesselmapPanel : public QWidget {
        Q_OBJECT

    public:
        using Self = VesselmapPanel;

        VesselmapPanel(QWidget* parent = nullptr);
        ~VesselmapPanel() override;

    protected slots:
        void onRefreshList(AppEntity::WellIndex wellIdx, QMap<AppEntity::LocationIndex, AppEntity::Location::Pointer>& locationList);
        void onUpdateExperiment(AppEntity::Experiment::Pointer experiment, bool singleRun);
        void onUpdatePosition(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position);
        void onUpdateGlobalPosition(const AppEntity::Position& position);
        void onUpdateCurrentWell(const AppEntity::WellIndex wellIndex);
        void onUpdateVesselStatus(bool loaded);
        void onSetCurrentWell(TC::WellIndex wellIndex, int32_t row, int32_t col);
        void onChangeSampleStagePosition(double xMM, double yMM);
        void onChangeSampleStagePosition(double xMM, double yMM, double zMM);

        void onImagingPointSelected(const QString& wellPosition, const QString& pointID);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}