#include "AcquisitionDataUpdater.h"
#include "AcquisitionDataObserver.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    AcquisitionDataObserver::AcquisitionDataObserver(QObject* parent) : QObject(parent) {
        AcquisitionDataUpdater::GetInstance()->Register(this);
    }

    AcquisitionDataObserver::~AcquisitionDataObserver() {
        AcquisitionDataUpdater::GetInstance()->Deregister(this);
    }

    auto AcquisitionDataObserver::ScannedData(const QString& user, const QString& project, const QString& experiment) -> void {
        emit sigDataScanned(user, project, experiment);
    }

    auto AcquisitionDataObserver::AddedData(const QString& fileFullPath) -> void {
        emit sigDataAdded(fileFullPath);
    }

    auto AcquisitionDataObserver::UpdatedData(const QString& fileFullPath) -> void {
        emit sigDataUpdated(fileFullPath);
    }

    auto AcquisitionDataObserver::DeletedData(const QString& fileFullPath) -> void {
        emit sigDataDeleted(fileFullPath);
    }

    auto AcquisitionDataObserver::DeletedDataRootFolder(const QString& fileFullPath) -> void {
        emit sigDataRootFolderDeleted(fileFullPath);
    }
}
