#include <ILiveImageAcquisition.h>
#include <IInstrument.h>
#include <SystemStatus.h>

#include "ConnectLiveView.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    ConnectLiveView::ConnectLiveView() : IUseCase("ConnectLiveView") {
    }

    ConnectLiveView::~ConnectLiveView() {
    }

    auto ConnectLiveView::Perform() -> bool {
        auto* instrument = IInstrument::GetInstance();
        if(!instrument) {
            Error("No instrument exists");
            return false;
        }

        auto* liveAcquisition = ILiveImageAcquisition::GetInstance();
        if(!liveAcquisition) {
            Error("Live acquisition is not available");
            return false;
        }

        const auto [modality, channel] = instrument->GetCurrentLiveModality();
        AppEntity::SystemStatus::GetInstance()->SetCurrentLiveImage(modality, channel);

        liveAcquisition->SetMaxColor();

        auto imagePort = liveAcquisition->GetImagePort();
        instrument->InstallImagePort(imagePort);

        return true;
    }
}