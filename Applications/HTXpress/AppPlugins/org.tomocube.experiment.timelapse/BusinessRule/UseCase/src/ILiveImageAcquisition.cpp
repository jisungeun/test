#include "ILiveImageAcquisition.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    static ILiveImageAcquisition* theInstance{ nullptr };

    ILiveImageAcquisition::ILiveImageAcquisition() {
        theInstance = this;
    }

    ILiveImageAcquisition::~ILiveImageAcquisition() {
    }

    auto ILiveImageAcquisition::GetInstance() -> ILiveImageAcquisition* {
        return theInstance;
    }
}