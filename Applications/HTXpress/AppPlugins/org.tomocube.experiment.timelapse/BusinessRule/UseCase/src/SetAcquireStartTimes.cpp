﻿#include <QDebug>

#include "SetAcquireStartTimes.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct SetAcquireStartTimes::Impl {
        IThumbnailOutputPort* output{nullptr};
        QMap<FrameIndex, StartTimeInSec> startTimes;    
    };

    SetAcquireStartTimes::SetAcquireStartTimes(IThumbnailOutputPort* output)
    : IUseCase("SetAcquireStartTimes"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    SetAcquireStartTimes::~SetAcquireStartTimes() = default;

    auto SetAcquireStartTimes::SetStartTimesInSec(const QMap<FrameIndex, StartTimeInSec>& startTimes) -> void {
        d->startTimes = startTimes;
    }

    auto SetAcquireStartTimes::Perform() -> bool {
        if(d->output) {
            d->output->UpdateStartTimesInSec(d->startTimes);
            return true;
        }

        return false;
    }
}
