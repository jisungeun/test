#include <QElapsedTimer>
#include <QCoreApplication>

#include <System.h>
#include <SystemStatus.h>
#include <IInstrument.h>

#include "Utility.h"
#include "LoadVessel.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct LoadVessel::Impl {
        LoadVessel* p{ nullptr };
        IInstrumentOutputPort* output{ nullptr };

        Impl(LoadVessel* p) : p(p) {}

        auto ReportError(const QString& message)->void;
    };

    auto LoadVessel::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
        output->UpdateFailed(message);
    }

    LoadVessel::LoadVessel(IInstrumentOutputPort* output) : IUseCase("LoadVessel"), d{new Impl(this)} {
        d->output = output;
    }

    LoadVessel::~LoadVessel() {
    }

    auto LoadVessel::Perform() -> bool {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto instrument = IInstrument::GetInstance();
        auto vessel = sysStatus->GetExperiment()->GetVessel();

        const auto startPos = instrument->GetAxisPosition();

        const auto zReadyMM = [=]()->double {
            auto scanReady = sysConfig->GetAutofocusReadyPos();
            auto afOffset = vessel->GetAFOffset();
            return scanReady + afOffset;
        }();
        auto targetPos = well2global(0, AppEntity::Position::fromUM(0, 0, zReadyMM*1000));

        if(d->output) {
            d->output->UpdateProgress(0, "Loading vessel");
        }

        if(!instrument->MoveAxis(targetPos)) {
            d->ReportError(tr("It fails to move sample stage [%1]").arg(instrument->GetErrorMessage()));
            return false;
        }

        QElapsedTimer timer;
        timer.start();

        auto motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < 120000) && motionStatus.moving) {
            if(motionStatus.error) {
                d->ReportError(tr("It fails to move sample stage : %1").arg(motionStatus.message));
                return false;
            }

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

            const auto curPos = instrument->GetAxisPosition();
            AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);

            if(d->output) {
                const auto progressX = (curPos.toMM().x - startPos.toMM().x) / (targetPos.toMM().x - startPos.toMM().x);
                const auto progressY = (curPos.toMM().y - startPos.toMM().y) / (targetPos.toMM().y - startPos.toMM().y);
                d->output->UpdateProgress(std::min(progressX, progressY) * 0.5 + 0.2);
                d->output->UpdateGlobalPosition(curPos);
            }
            motionStatus = instrument->CheckAxisMotion();
        }

        if(!instrument->MoveAxis(AppEntity::Axis::Z, targetPos.toMM().z)) {
            d->ReportError(tr("It fails to move Z axis to the ready position [%1]").arg(instrument->GetErrorMessage()));
            return false;
        }

        timer.start();

        motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < 60000) && motionStatus.moving) {
            if(motionStatus.error) {
                d->ReportError(tr("It fails to move Z axis to the ready position : %1").arg(motionStatus.message));
                return false;
            }

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

            const auto curPos = instrument->GetAxisPosition();
            AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);

            if(d->output) {
                const auto progress = (curPos.toMM().z - startPos.toMM().z) / (targetPos.toMM().z - startPos.toMM().z);
                d->output->UpdateProgress(progress * 0.2 + 0.7);
                d->output->UpdateGlobalPosition(curPos);
            }

            motionStatus = instrument->CheckAxisMotion();
        }

        bool afSuccess = true;
        const auto afResult = instrument->PerformAutoFocus();
        if(!std::get<0>(afResult)) {
            afSuccess = false;
            d->ReportError(tr("Autofocus failure because of an error = %1 . Click \"OK\" to continue vessel unloading.").arg(instrument->GetErrorMessage()));
        } else if (!std::get<1>(afResult)) {
            afSuccess = false;
            d->ReportError(tr("Autofocus failure. Please check that the vessel is properly equipped with the holder and chamber. Click \"OK\" to continue vessel unloading."));
        }

        d->output->AutoFocusEnabled(afSuccess);

        if(!afSuccess) {
            d->output->UpdateProgress(1.0);
            return false;
        }

        const auto curPos = instrument->GetAxisPosition();
        sysStatus->SetCurrentGlobalPosition(curPos);
        d->output->UpdateGlobalPosition(curPos);
        sysStatus->SetCurrentVesselIndex(0);
        sysStatus->SetCurrentWell(0);

        const auto cAxisReadyPos = instrument->GetAxisPositionMM(AppEntity::Axis::C);
        sysStatus->SetCurrentCondensorPosition(cAxisReadyPos);

        d->output->UpdateProgress(1.0);
        d->output->VesselLoaded();

        return true;
    }
}
