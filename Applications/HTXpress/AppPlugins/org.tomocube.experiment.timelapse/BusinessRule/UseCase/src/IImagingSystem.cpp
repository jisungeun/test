#include "IImagingSystem.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    static IImagingSystem* theInstance{ nullptr };

    IImagingSystem::IImagingSystem() {
        theInstance = this;
    }

    IImagingSystem::~IImagingSystem() {
    }

    auto IImagingSystem::GetInstance() -> IImagingSystem* {
        return theInstance;
    }
}
