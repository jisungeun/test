#include <SystemStatus.h>

#include "IInstrument.h"
#include "Utility.h"
#include "GetGlobalPosition.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct GetGlobalPosition::Impl {
        IMotionOutputPort* output{ nullptr };
        AppEntity::WellIndex wellIndex{ -1 };
        AppEntity::Position position;
    };

    GetGlobalPosition::GetGlobalPosition(IMotionOutputPort* outputPort) : IUseCase("GetGlobalPosition"), d{new Impl} {
        d->output = outputPort;
    }

    GetGlobalPosition::~GetGlobalPosition() {
    }

    auto GetGlobalPosition::CurrentPosition() const -> AppEntity::Position {
        return d->position;
    }
    
    auto GetGlobalPosition::CurrentWell() const->AppEntity::WellIndex {
        return d->wellIndex;
    }

    auto GetGlobalPosition::Perform() -> bool {
        auto* instrument = IInstrument::GetInstance();
        if(!instrument) {
            Error("Instrument is not available");
            return false;
        }

        auto status = AppEntity::SystemStatus::GetInstance();

        auto globalPos = instrument->GetAxisPosition();

        d->position = globalPos;
        d->wellIndex = global2wellIndex(globalPos);

        status->SetCurrentGlobalPosition(globalPos);
        status->SetCurrentWell(d->wellIndex);

        if(d->output) d->output->UpdateGlobalPosition(d->position); 

        return true;
    }
}
