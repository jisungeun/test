#include "IInstrumentOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    IInstrumentOutputPort::IInstrumentOutputPort() {
    }

    IInstrumentOutputPort::~IInstrumentOutputPort() {
    }

    static InstrumentNullOutputPort::Pointer nullInstance{ new InstrumentNullOutputPort() };
    auto InstrumentNullOutputPort::GetInstance() -> InstrumentNullOutputPort* {
        return nullInstance.get();
    }
}
