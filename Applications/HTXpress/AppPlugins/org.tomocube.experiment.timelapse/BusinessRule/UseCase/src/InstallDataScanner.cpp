#include "InstallDataScanner.h"

#include <IDataManager.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
	struct InstallDataScanner::Impl {
		IDataOutputPort* output{ nullptr };
	};

    InstallDataScanner::InstallDataScanner() : IUseCase("InstallDataScanner"), d{ new Impl } {
    }

    InstallDataScanner::~InstallDataScanner() {
    }

    auto InstallDataScanner::SetDataOutputPort(IDataOutputPort* outputPort)->void {
        d->output = outputPort;
    }

    auto InstallDataScanner::Perform() -> bool {
        if (d->output == nullptr) {
            Error("Invalid output port.");
            return false;
        }

        IDataManager::GetInstance()->InstallDataScannerOutputPort(d->output);
        return true;
    }
}
