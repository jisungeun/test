#include <SystemStatus.h>

#include "GetImagingCondition.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct GetImagingCondition::Impl {
        QMap<Entity::ImagingMode, Entity::ChannelConfig::Pointer> channelConfigs;
    };

    GetImagingCondition::GetImagingCondition() : IUseCase("GetImagingCondition"), d{new Impl} {
    }

    GetImagingCondition::~GetImagingCondition() {
    }

    auto GetImagingCondition::GetConfig(Entity::ImagingMode mode) -> Entity::ChannelConfig::Pointer {
        return d->channelConfigs.value(mode);
    }

    auto GetImagingCondition::Perform() -> bool {
        auto sysStatus = Entity::SystemStatus::GetInstance();
        d->channelConfigs = sysStatus->GetChannelConfigAll();

        return true;
    }
}
