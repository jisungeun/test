#include <ILiveImageAcquisition.h>
#include <IInstrument.h>

#include "StopLiveView.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct StopLiveView::Impl {
        IInstrumentOutputPort* output{ InstrumentNullOutputPort::GetInstance() };

        auto ReportError(StopLiveView* p, const QString& message)->void;
    };

    auto StopLiveView::Impl::ReportError(StopLiveView* p, const QString& message) -> void {
        p->Error(message);
        output->LiveImagingFailed(message);
    }

    StopLiveView::StopLiveView(IInstrumentOutputPort* output) : IUseCase("StopLiveView"), d{new Impl} {
        d->output = output;
    }

    StopLiveView::~StopLiveView() {
    }

    auto StopLiveView::Perform() -> bool {
        auto* instrument = IInstrument::GetInstance();
        if(!instrument) {
            d->ReportError(this, "No instrument exists");
            return false;
        }

        auto* liveAcquisition = ILiveImageAcquisition::GetInstance();
        if(!liveAcquisition) {
            d->ReportError(this, "Live acquisition is not available");
            return false;
        }

        auto imagePort = liveAcquisition->GetImagePort();
        instrument->UninstallImagePort(imagePort);

        d->output->LiveStopped();

        return true;
    }
}