#include <IInstrument.h>
#include "ConnectInstrument.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct ConnectInstrument::Impl {
        IInstrumentOutputPort* output{ nullptr };

        auto ReportError(ConnectInstrument* p, const QString& message)->void;
    };

    auto ConnectInstrument::Impl::ReportError(ConnectInstrument* p, const QString& message) -> void {
        p->Error(message);
        if(output) output->UpdateFailed(message);
    }

    ConnectInstrument::ConnectInstrument(IInstrumentOutputPort* output) : IUseCase("ConnectInstrument"), d{new Impl} {
        d->output = output;
    }

    ConnectInstrument::~ConnectInstrument() {
    }

    auto ConnectInstrument::Perform() -> bool {
        auto* instrument = IInstrument::GetInstance();
        if(!instrument) {
            Error("No instrument exists");
            return false;
        }

        if(!instrument->Initialize()) {
            const auto msg = QString("Initialization can't be started - %1").arg(instrument->GetErrorMessage());
            d->ReportError(this, msg);
            return false;
        }

        return true;
    }
}