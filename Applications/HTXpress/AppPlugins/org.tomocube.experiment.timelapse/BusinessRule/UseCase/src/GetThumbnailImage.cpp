﻿#include <QDebug>

#include "GetThumbnailImage.h"
#include "IThumbnailImageReader.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct GetThumbnailImage::Impl {
        IThumbnailOutputPort* output{nullptr};

        bool sync{false};

        int32_t acquisitionCountIndex{};
        QString dataPath{};

        int32_t frameIndex{};
        AppEntity::Modality modality{AppEntity::Modality::HT};

        QImage outputImage{};

        auto ReportError(GetThumbnailImage* self, const QString& message) const -> void;
    };

    GetThumbnailImage::GetThumbnailImage(IThumbnailOutputPort* output)
    : IUseCase("GetThumbnailImage"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    GetThumbnailImage::~GetThumbnailImage() = default;

    auto GetThumbnailImage::SetSync(bool sync) -> void {
        d->sync = sync;
    }

    auto GetThumbnailImage::SetDataPath(const QString& path) -> void {
        d->dataPath = path;
    }

    auto GetThumbnailImage::SetFrameIndex(int32_t frameIndex) -> void {
        d->frameIndex = frameIndex;
    }

    auto GetThumbnailImage::SetModality(AppEntity::Modality modality) -> void {
        d->modality = modality;
    }

    auto GetThumbnailImage::GetImage() const -> QImage {
        return d->outputImage;
    }

    auto GetThumbnailImage::Perform() -> bool {
        bool isValid = false;
        auto reader = IThumbnailImageReader::GetInstance();
        if (!reader) {
            d->ReportError(this, "ThumbnailImageReader doesn't exist...");
            return false;
        }

        if(!d->sync) {
            reader->SetDataPath(d->dataPath);
            reader->SetFrameIndex(d->frameIndex);
            reader->SetModality(d->modality);
            d->outputImage = *(reader->GetThumbnailImage());
            isValid = true;
        }
        else {
            reader->SetDataPath(d->dataPath);
            auto [idx, modality, img]= (reader->GetLastThumbnailImageInfo());
            d->outputImage = *img;
            isValid = true;
        }
        return isValid;
    }

    auto GetThumbnailImage::Impl::ReportError(GetThumbnailImage* self, const QString& message) const -> void {
        self->Error(message);
        if (output) output->ReportError(message);
    }
}
