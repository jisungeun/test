#include "ScanAcquisitionData.h"

#include <SessionManager.h>
#include <IDataManager.h>
#include <SystemStatus.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
	struct ScanAcquisitionData::Impl {
		IDataOutputPort* output{ nullptr };
	};

    ScanAcquisitionData::ScanAcquisitionData(IDataOutputPort* output) : IUseCase("ScanAcquisitionData"), d{ new Impl } {
        d->output = output;
    }

    ScanAcquisitionData::~ScanAcquisitionData() {
    }

    auto ScanAcquisitionData::Perform() -> bool {
        const auto user = AppEntity::SessionManager::GetInstance()->GetID();
        if (user.isEmpty()) {
            Error("Failed to get current user information.");
            return false;
        }

        const auto systemStatus = AppEntity::SystemStatus::GetInstance();
        const auto project = systemStatus->GetProjectTitle();
        if (project.isEmpty()) {
            Error("Failed to get an project name.");
            return false;
        }

        const auto experiment = systemStatus->GetExperiment();
        if (experiment == nullptr || experiment->GetTitle().isEmpty()) {
            Error("Failed to get an experiment information.");
            return false;
        }

        auto dataManager = IDataManager::GetInstance();
        if (d->output) dataManager->SetOutputPort(d->output);

        dataManager->SetScanExperiment(user, project, experiment->GetTitle());

        return true;
    }
}
