#include "IScanTimeCalculator.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    static IScanTimeCalculator* theInstance{ nullptr };

    IScanTimeCalculator::IScanTimeCalculator() {
        theInstance = this;
    }

    IScanTimeCalculator::~IScanTimeCalculator() {
    }

    auto IScanTimeCalculator::GetInstance() -> IScanTimeCalculator* {
        return theInstance;
    }
}
