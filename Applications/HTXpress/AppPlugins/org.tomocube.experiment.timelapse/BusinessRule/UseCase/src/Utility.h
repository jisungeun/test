#pragma once
#include <memory>

#include <System.h>
#include <ImagingCondition.h>
#include <SystemStatus.h>

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    inline auto nm2pixels(const double nm)->int32_t {
        auto pixelRes = AppEntity::System::GetCameraResolutionInUM();
        return static_cast<int32_t>(std::round(nm / (pixelRes * 1000)));
    }

    inline auto um2pixels(const double um)->int32_t {
        auto pixelRes = AppEntity::System::GetCameraResolutionInUM();
        return static_cast<int32_t>(std::round(um / pixelRes));
    }

    inline auto pixels2nm(int32_t pixels)->double {
        auto pixelRes = AppEntity::System::GetCameraResolutionInUM();
        return static_cast<double>(pixels * pixelRes * 1000);
    }

    inline auto pixels2um(int32_t pixels)->double {
        auto pixelRes = AppEntity::System::GetCameraResolutionInUM();
        return static_cast<double>(pixels * pixelRes);
    }

    inline auto well2global(const AppEntity::WellIndex wellIdx, const AppEntity::Position& position)->AppEntity::Position {
        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        auto systemCenter = AppEntity::System::GetSystemCenter();
        auto vessel = experiment->GetVessel();
        auto wellPosition = vessel->GetWellPosition(wellIdx);
        return systemCenter + wellPosition + position;
    }

    inline auto well2global(const AppEntity::Position& position)->AppEntity::Position {
        auto status = AppEntity::SystemStatus::GetInstance();
        return well2global(status->GetCurrentWell(), position);
    }

    inline auto global2well(const AppEntity::Position& globalPos, const AppEntity::WellIndex wellIdx)->AppEntity::Position {
        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        auto systemCenter = AppEntity::System::GetSystemCenter();
        auto vessel = experiment->GetVessel();
        auto wellPosition = vessel->GetWellPosition(wellIdx);
        return globalPos - wellPosition - systemCenter;
    }

    inline auto global2well(const AppEntity::Position& globalPos)->AppEntity::Position {
        auto status = AppEntity::SystemStatus::GetInstance();
        return global2well(globalPos, status->GetCurrentWell());
    }

    inline auto global2wellIndex(const AppEntity::Position& globalPos)->AppEntity::WellIndex {
        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        auto systemCenter = AppEntity::System::GetSystemCenter();
        auto vessel = experiment->GetVessel();

        static AppEntity::Vessel latestVessel;
        static std::vector<double> colPositions;
        static std::vector<double> rowPositions;
        static int32_t wellCountCols;
        static int32_t wellCountRows;

        if(!(latestVessel == *vessel)) {
            latestVessel = *vessel;

            wellCountCols = vessel->GetWellCols();
            wellCountRows = vessel->GetWellRows();
            colPositions.clear();
            rowPositions.clear();

            for(auto idx=0; idx<wellCountCols; idx++) colPositions.push_back(vessel->GetWellPositionX(0, idx) + systemCenter.toMM().x);
            for(auto idx=0; idx<wellCountRows; idx++) rowPositions.push_back(vessel->GetWellPositionY(idx, 0) + systemCenter.toMM().y);
            std::sort(rowPositions.begin(), rowPositions.end(), std::less<int>());
        }

        auto findNearest = [=](const std::vector<double>& array, double target)->std::tuple<int32_t,double> {
            if(array.size() == 1) return std::make_tuple(0, array.front());

            auto iter_geq = std::lower_bound(array.begin(), array.end(), target);
            if(iter_geq == array.begin()) return std::make_tuple(-1, array.front());
            if(iter_geq == array.end()) return std::make_tuple(-2, array.back());

            const double a = *(iter_geq -1);
            const double b = *iter_geq;

            if(fabs(target - a) < fabs(target - b)) {
                const auto idx = iter_geq - array.begin() - 1;
                return std::make_tuple(idx, array.at(idx));
            }

            const auto idx = iter_geq - array.begin(); 
            return std::make_tuple(idx, array.at(idx));
        };

        auto adjustIndex = [](const int32_t index, const int32_t count)->int32_t {
            if(index == -1) return 0;
            if(index == -2) return count - 1;
            return index;
        };

        try {
            const auto xpos = globalPos.toMM().x;
            const auto ypos = globalPos.toMM().y;
            auto [colIdx, colNearPos] = findNearest(colPositions, xpos);
            auto [rowIdxReverse, rowNearPos] = findNearest(rowPositions, ypos);

            if(colIdx<0 || rowIdxReverse<0) {
                auto wellXHalf = vessel->GetWellSizeX() / 2.0;
                auto wellYHalf = vessel->GetWellSizeY() / 2.0;
                const auto wellShape = vessel->GetWellShape();

                switch(wellShape) {
                case AppEntity::WellShape::Circle: {
                    const auto dist = std::pow(xpos-colNearPos, 2) + std::pow(ypos-rowNearPos, 2);
                    const auto radius = std::pow(wellXHalf, 2);
                    if( dist < radius) {
                        colIdx = adjustIndex(colIdx, wellCountCols);
                        rowIdxReverse = adjustIndex(rowIdxReverse, wellCountRows);
                    } else {
                        return -1;
                    }}
                    break;
                case AppEntity::WellShape::Rectangle:
                    if((std::abs(xpos - colNearPos) <= wellXHalf) && ((ypos - rowNearPos) <= wellYHalf)) {
                        colIdx = adjustIndex(colIdx, wellCountCols);
                        rowIdxReverse = adjustIndex(rowIdxReverse, wellCountRows);
                    } else {
                        return -1;
                    }
                    break;
                default:
                    return -1;
                }
            }

            const auto rowIdx = wellCountRows - rowIdxReverse - 1;

            const auto wellIdx = vessel->GetWellIndex(rowIdx, colIdx);
            const auto posInWell = global2well(globalPos, wellIdx);

            return vessel->IsInImagingArea(posInWell.toMM().x, posInWell.toMM().y) ? wellIdx : -1;
        } catch(std::exception& ex) {
            qDebug() << ex.what();
            return -1;
        }
    }

    inline auto global2vessel(const AppEntity::Position& globalPos)->AppEntity::Position {
        auto experiment = AppEntity::SystemStatus::GetInstance()->GetExperiment();
        auto systemCenter = AppEntity::System::GetSystemCenter();
        return globalPos - systemCenter;
    }

    inline auto CreateCondHT(bool is3D)->AppEntity::ImagingConditionHT::Pointer {
        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto channelConfig = sysStatus->GetChannelConfig(is3D?AppEntity::ImagingMode::HT3D:AppEntity::ImagingMode::HT2D);
        const auto scanConfig = sysStatus->GetScanConfig(is3D?AppEntity::ImagingMode::HT3D:AppEntity::ImagingMode::HT2D);
        const auto scanSteps = (is3D)?scanConfig->GetSteps():1;

        auto condHT = std::make_shared<AppEntity::ImagingConditionHT>();
        condHT->SetExposure(channelConfig->GetCameraExposureUSec());
        condHT->SetInterval(channelConfig->GetCameraIntervalUSec());
        condHT->SetIntensity(channelConfig->GetLightIntensity());

        condHT->SetSliceStart(scanConfig->GetBottom());
        condHT->SetSliceCount(scanSteps);
        condHT->SetSliceStep(scanConfig->GetStep());

        condHT->SetDimension(is3D ? 3 : 2);

        return condHT;
    }

    inline auto CreateCondFL(bool is3D, const QList<int32_t>& flChannels)->AppEntity::ImagingConditionFL::Pointer {
        if(flChannels.isEmpty()) return nullptr;

        const auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto condFL = std::make_shared<AppEntity::ImagingConditionFL>();

        for(auto flChannelIndex : flChannels) {
            const auto channelConfig = [=]()->AppEntity::ChannelConfig::Pointer {
                auto mode = AppEntity::ImagingMode::_from_integral(AppEntity::ImagingMode::FLCH0 + flChannelIndex);
                return sysStatus->GetChannelConfig(mode);
            }();

            AppEntity::FLChannel flChannel;
            AppEntity::FLFilter exFilter, emFilter;
            if(!sysConfig->GetFLActiveExcitation(channelConfig->GetLightChannel(), exFilter)) return nullptr;
            if(!sysConfig->GetFLEmission(channelConfig->GetFilterChannel(), emFilter)) return nullptr;
            if(!sysConfig->GetFLChannel(channelConfig->GetChannelName(), flChannel)) return nullptr;

            AppEntity::ImagingConditionFL::Color color;
            color.red = flChannel.GetColor().red();
            color.green = flChannel.GetColor().green();
            color.blue = flChannel.GetColor().blue();

            condFL->SetChannel(flChannelIndex,
                               channelConfig->GetCameraExposureUSec(),
                               channelConfig->GetCameraIntervalUSec(),
                               channelConfig->GetLightIntensity(),
                               0,
                               exFilter,
                               emFilter,
                               channelConfig->GetChannelName(),
                               color);
        }

        const auto scanConfig = [=]()->AppEntity::ScanConfig::Pointer {
                auto mode = AppEntity::ImagingMode::_from_integral(AppEntity::ImagingMode::FLCH0 + flChannels.at(0));
                return sysStatus->GetScanConfig(mode);
            }();
        const auto scanSteps = (is3D)?scanConfig->GetSteps():1;

        condFL->SetSliceCount(scanSteps);
        condFL->SetSliceStep(scanConfig->GetStep());
        condFL->SetSliceStart(scanConfig->GetBottom());

        condFL->SetDimension(is3D ? 3 : 2);

        //Additional
        condFL->SetScanMode(scanConfig->GetScanMode());
        condFL->SetScanRange(scanConfig->GetBottom(), scanConfig->GetTop(), scanConfig->GetStep());
        condFL->SetScanFocus(0, scanConfig->GetFocusFLOffsetUm());

        return condFL;
    }

    inline auto CreateCondBF(bool isGray)->AppEntity::ImagingConditionBF::Pointer {
        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto channelConfig = [=]()->AppEntity::ChannelConfig::Pointer {
            if(isGray) {
                return sysStatus->GetChannelConfig(AppEntity::ImagingMode::BFGray);
            }
            return sysStatus->GetChannelConfig(AppEntity::ImagingMode::BFColor);
        }();

        const auto scanConfig = [=]()->AppEntity::ScanConfig::Pointer {
            if(isGray) {
                return sysStatus->GetScanConfig(AppEntity::ImagingMode::BFGray);
            }
            return sysStatus->GetScanConfig(AppEntity::ImagingMode::BFColor);
        }();

        auto condBF = std::make_shared<AppEntity::ImagingConditionBF>();
        condBF->SetExposure(channelConfig->GetCameraExposureUSec());
        condBF->SetInterval(channelConfig->GetCameraIntervalUSec());
        condBF->SetIntensity(channelConfig->GetLightIntensity());
        condBF->SetColorMode(!isGray);

        condBF->SetSliceStart(0);
        condBF->SetSliceCount(1);
        condBF->SetSliceStep(0);

        condBF->SetDimension(2);

        return condBF;
    }

    inline auto GetExChannel(uint32_t waveLength)->int32_t {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto& channelIndexes = sysConfig->GetFLChannels();

        for(auto channelIndex : channelIndexes) {
            AppEntity::FLChannel flChannel;
            sysConfig->GetFLChannel(channelIndex, flChannel);
            if(flChannel.GetExcitation() == waveLength) return channelIndex;
        }

        return -1;
    }

    inline auto GetEmChannel(uint32_t waveLength)->int32_t {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        const auto& channelIndexes = sysConfig->GetFLChannels();

        for(auto channelIndex : channelIndexes) {
            AppEntity::FLChannel flChannel;
            sysConfig->GetFLChannel(channelIndex, flChannel);
            if(flChannel.GetEmission() == waveLength) return channelIndex;
        }

        return -1;
    }

    inline auto GetExcitation(int32_t channel)->int32_t {
        auto sysConfig = AppEntity::System::GetSystemConfig();

        AppEntity::FLChannel flChannel;
        if(!sysConfig->GetFLChannel(channel, flChannel)) return 0;

        return flChannel.GetExcitation();
    }

    inline auto GetEmission(int32_t channel)->int32_t {
        auto sysConfig = AppEntity::System::GetSystemConfig();

        AppEntity::FLChannel flChannel;
        if(!sysConfig->GetFLChannel(channel, flChannel)) return 0;

        return flChannel.GetEmission();
    }
}