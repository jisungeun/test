#include <SystemStatus.h>

#include "SetTimelapseRunning.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct SetTimelapseRunning::Impl {
        SetTimelapseRunning* p{ nullptr };
        
        Impl(SetTimelapseRunning* p) : p(p) {}

        bool isRunning{ false };

        auto ReportError(const QString& message)->void;
    };

    auto SetTimelapseRunning::Impl::ReportError(const QString& message) -> void {
        p->Error(message);        
    }

    SetTimelapseRunning::SetTimelapseRunning() : IUseCase("SetTimelapseRunning"), d{new Impl(this)} {
        
    }

    SetTimelapseRunning::~SetTimelapseRunning() {
    }

    auto SetTimelapseRunning::SetRunning(bool isRunning) -> void {
        d->isRunning = isRunning;
    }

    auto SetTimelapseRunning::Perform() -> bool {
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        sysStatus->SetTimelapseRunning(d->isRunning);

        return true;
    }
}