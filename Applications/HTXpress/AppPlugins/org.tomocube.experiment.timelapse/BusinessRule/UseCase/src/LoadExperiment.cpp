#include <System.h>
#include <SessionManager.h>
#include <SystemStatus.h>

#include "IExperimentReader.h"
#include "IScanTimeCalculator.h"
#include "LoadExperiment.h"

#include "IImagingProfileLoader.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct LoadExperiment::Impl {
        LoadExperiment* p{ nullptr };
        IExperimentOutputPort* output{ nullptr };
        QString projectTitle;
        QString expOutputPath;
        QString path;
        bool singleRun{ false };

        explicit Impl(LoadExperiment* p) : p{ p } {}
        auto GetHtScanParameter() const->std::optional<std::tuple<int32_t, int32_t>>;
        auto Error(const QString& message)->void;
    };

    auto LoadExperiment::Impl::GetHtScanParameter() const -> std::optional<std::tuple<int32_t, int32_t>> {
        using Step = int32_t;
        using Slices = int32_t;

        const auto profileLoader = IImagingProfileLoader::GetInstance();
        const auto sysStatus = AppEntity::SystemStatus::GetInstance();
        const auto sysConfig = AppEntity::System::GetSystemConfig();

        if (profileLoader) {
            if (const auto scanParameterInExperiment = profileLoader->GetHtScanParameter()) {
                if (scanParameterInExperiment.has_value()) {
                    const auto scanParameters = scanParameterInExperiment.value();
                    p->Print(QString("HT Scan Parameters from experiment [Step:%1, Slices:%2]").arg(std::get<0>(scanParameters)).arg(std::get<1>(scanParameters)));
                    return scanParameters;
                }
            }
        }

        return std::nullopt;
    }

    auto LoadExperiment::Impl::Error(const QString& message) -> void {
        p->Error(message);
        if(output) output->Error(message);
    }

    LoadExperiment::LoadExperiment(IExperimentOutputPort* output) : IUseCase("LoadExperiment"), d{std::make_unique<Impl>(this)} {
        d->output = output;
    }

    LoadExperiment::~LoadExperiment() {
    }

    auto LoadExperiment::SetExperiment(const QString& projectTitle, const QString& experimentTitle) -> void {
		const auto systemConfig = AppEntity::System::GetInstance()->GetSystemConfig();
		const auto dataPath = systemConfig->GetDataDir();
		const auto user = AppEntity::SessionManager::GetInstance()->GetID();
        d->projectTitle = projectTitle;
        d->expOutputPath = dataPath + "/" + user + "/" + projectTitle + "/" + experimentTitle;
        d->path = d->expOutputPath + "/" + experimentTitle + "." + AppEntity::ExperimentExtension;
    }

    auto LoadExperiment::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto LoadExperiment::SetSingleRun(bool singleRun) -> void {
        d->singleRun = singleRun;
    }

    auto LoadExperiment::Perform() -> bool {
        auto* reader = IExperimentReader::GetInstance();
        if(!reader) {
            d->Error("No experiment reader exists");
            return false;
        }

        if(d->path.isEmpty()) {
             d->Error("Experiment file path is not specified");
            return false;
        }

        auto experiment = reader->Read(d->path);
        if(!experiment) {
             d->Error(QString("It fails read an experiment from %1").arg(d->path));
            return false;
        }

        if(d->singleRun) {
            const auto sysStatus = AppEntity::SystemStatus::GetInstance();
            auto vesselIdx = sysStatus->GetCurrentVesselIndex();
            auto scenario = experiment->GetScenario(vesselIdx);

            auto imagingTypes = scenario->GetImagingTypes();
            if(imagingTypes.isEmpty()) {
                d->Error("No imaging modality is added to timelapse imaging plan");
                return false;
            }

            auto newExperiment = std::make_shared<AppEntity::Experiment>(*experiment);
            auto newScenario = newExperiment->GetScenario(vesselIdx);
            newScenario->RemoveAll();

            auto newSequence = [&]()->AppEntity::ImagingSequence::Pointer {
                // TODO: FL의 경우 채널 확인도 필요
                // TODO: 같은 채널에 2D, 3D 모두 있는 경우 3D condition 추가

                auto newSequence = std::make_shared<AppEntity::ImagingSequence>();

                AppEntity::ImagingConditionHT::Pointer newHtCond = nullptr;
                AppEntity::ImagingConditionFL::Pointer newFl3dCond = nullptr;
                AppEntity::ImagingConditionFL::Pointer newFl2dCond = nullptr;
                AppEntity::ImagingConditionBF::Pointer newBfCond = nullptr;

                const auto seqCount = scenario->GetCount();
                for(auto seqIdx=0; seqIdx<seqCount; seqIdx++) {
                    auto sequence = scenario->GetSequence(seqIdx);
                    auto modalityCount = sequence->GetModalityCount();
                    for(auto idx=0; idx<modalityCount; idx++) {
                        auto imgCond = sequence->GetImagingCondition(idx);

                        if (imgCond->GetImagingType() == +AppEntity::ImagingType::HT3D) {
                            auto cond = std::dynamic_pointer_cast<AppEntity::ImagingConditionHT>(imgCond);
                            if (cond == nullptr) continue;

                            // HT 3D, 2D 모두 있는 경우 HT3D를 sing run에 사용
                            if (newHtCond == nullptr || 
                                newHtCond->GetImagingType() == +AppEntity::ImagingType::HT2D) {
                                newHtCond = std::make_shared<AppEntity::ImagingConditionHT>(*cond);
                            }
                        } else if (imgCond->GetImagingType() == +AppEntity::ImagingType::HT2D) {
                            auto cond = std::dynamic_pointer_cast<AppEntity::ImagingConditionHT>(imgCond);
                            if (cond == nullptr) continue;

                            if (newHtCond == nullptr) {
                                newHtCond = std::make_shared<AppEntity::ImagingConditionHT>(*cond);
                            }
                        } else if (imgCond->GetImagingType() == +AppEntity::ImagingType::FL3D) {
                            auto cond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(imgCond);
                            if (cond == nullptr) continue;

                            if (newFl3dCond == nullptr) {
                                newFl3dCond = std::make_shared<AppEntity::ImagingConditionFL>(*cond);
                            } else {
                                for (auto channel : cond->GetChannels()) {
                                    if (newFl3dCond->GetChannels().contains(channel)) continue;

                                    auto [exposure, interval, intensity, gain, ex, em, name, color] = cond->GetChannel(channel);
                                    newFl3dCond->SetChannel(channel, exposure, interval, intensity, gain, ex, em, name, color);
                                }
                            }
                        } else if (imgCond->GetImagingType() == +AppEntity::ImagingType::FL2D) {
                            auto cond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(imgCond);
                            if (cond == nullptr) continue;

                            if (newFl2dCond == nullptr) {
                                newFl2dCond = std::make_shared<AppEntity::ImagingConditionFL>(*cond);
                            } else {
                                for (auto channel : cond->GetChannels()) {
                                    if (newFl2dCond->GetChannels().contains(channel)) continue;

                                    auto [exposure, interval, intensity, gain, ex, em, name, color] = cond->GetChannel(channel);
                                    newFl2dCond->SetChannel(channel, exposure, interval, intensity, gain, ex, em, name, color);
                                }
                            }
                        } else if (imgCond->GetImagingType() == +AppEntity::ImagingType::BFGray) {
                            auto cond = std::dynamic_pointer_cast<AppEntity::ImagingConditionBF>(imgCond);
                            if (cond == nullptr) continue;

                            if (newBfCond == nullptr) {
                                newBfCond = std::make_shared<AppEntity::ImagingConditionBF>(*cond);
                            }
                        }
                    }
                }

                // TODO: FL3D에 있는 채널이 FL2D에도 있으면 FL2D에선 삭제
                if (newFl3dCond && newFl2dCond) {
                    auto fl2dChannels = newFl2dCond->GetChannels();
                    for (auto channel : fl2dChannels) {
                        if (newFl3dCond->GetChannels().contains(channel)) {
                            newFl2dCond->DeleteChannel(channel);
                        }
                    }
                }

                if (newHtCond) newSequence->AddImagingCondition(newHtCond);
                if (newFl3dCond && !newFl3dCond->GetChannels().isEmpty()) newSequence->AddImagingCondition(newFl3dCond);
                if (newFl2dCond && !newFl2dCond->GetChannels().isEmpty()) newSequence->AddImagingCondition(newFl2dCond);
                if (newBfCond) newSequence->AddImagingCondition(newBfCond);

                qDebug() << "Imaging types in new seq: " << newSequence->GetImagingTypes();
                for (auto cond : newSequence->GetImagingCondition()) {
                    if (cond->GetImagingType() == +AppEntity::ImagingType::FL3D) {
                        auto fl = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(cond);
                        qDebug() << "FL3D channels: " << fl->GetChannels();

                    } else if (cond->GetImagingType() == +AppEntity::ImagingType::FL2D) {
                        auto fl = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(cond);
                        qDebug() << "FL2D channels: " << fl->GetChannels();
                    }
                }
                return newSequence;
            }();

            newScenario->AddSequence(0, "TestRun", newSequence);

            experiment = newExperiment;
        }

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        sysStatus->SetProjectTitle(d->projectTitle);
        sysStatus->SetExperiment(experiment, d->path);
        sysStatus->SetExperimentOutputPath(d->expOutputPath);

        const auto vesselModel = experiment->GetVessel()->GetModel();
        const auto NA = experiment->GetVessel()->GetNA();
        Print(QString("Vessel Model=%1 NA=%2").arg(vesselModel).arg(NA));

        const auto htScanParam = d->GetHtScanParameter();
        if (!htScanParam.has_value()) {
            d->Error(QString("No valid profile in %1").arg(experiment->GetTitle()));
            return false;
        }

        const auto htScanStep = std::get<0>(*htScanParam);
        const auto htScanSlices = std::get<1>(*htScanParam);
        const auto halfPulses = (htScanStep * htScanSlices) / 2;
        Print(QString("HT Scan Parameter. Step=%1 Slices=%2 [S:%3~ E:%4]")
            .arg(htScanStep)
            .arg(htScanSlices)
            .arg(-halfPulses)
            .arg(halfPulses));

        AppEntity::ScanConfig ht3DConfig;
        ht3DConfig.SetRange(-halfPulses, halfPulses, htScanStep);
        ht3DConfig.SetSteps(htScanSlices);
        sysStatus->SetScanConfig(AppEntity::ImagingMode::HT3D, ht3DConfig);

        Print(QString("Experiment is loaded from %1").arg(d->path));

        auto timeCalculator = IScanTimeCalculator::GetInstance();
        if(timeCalculator) {
            const auto roi = sysStatus->GetExperiment()->GetFOV();
            timeCalculator->SetMaximumFOV(roi);
            timeCalculator->SetOverlapInUM(AppEntity::System::GetTileScanOverlap());
        }

        if(d->output) d->output->Update(experiment, d->singleRun);

        return true;
    }
}
