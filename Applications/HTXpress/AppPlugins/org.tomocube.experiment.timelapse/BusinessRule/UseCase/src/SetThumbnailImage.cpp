﻿#include "SetThumbnailImage.h"
#include "IThumbnailImageReader.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct SetThumbnailImage::Impl {
        IThumbnailOutputPort* output{nullptr};

        bool sync{false};

        int32_t acquisitionCountIndex{};
        QString dataPath{};

        int32_t frameIndex{};
        AppEntity::Modality modality{AppEntity::Modality::HT};

        QImage outputImage{};

        auto ReportError(SetThumbnailImage* self, const QString& message) const -> void;
    };

    SetThumbnailImage::SetThumbnailImage(IThumbnailOutputPort* output)
    : IUseCase("SetThumbnailImage"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    SetThumbnailImage::~SetThumbnailImage() = default;


    auto SetThumbnailImage::SetSync(bool sync) -> void {
        d->sync = sync;
    }

    auto SetThumbnailImage::SetDataPath(const QString& path) -> void {
        d->dataPath = path;
    }

    auto SetThumbnailImage::SetFrameIndex(int32_t frameIndex) -> void {
        d->frameIndex = frameIndex;
    }

    auto SetThumbnailImage::SetModality(AppEntity::Modality modality) -> void {
        d->modality = modality;
    }

    auto SetThumbnailImage::Perform() -> bool {
        if (!d->output) return false;

        auto reader = IThumbnailImageReader::GetInstance();
        if (!reader) {
            d->ReportError(this, "ThumbnailImageReader doesn't exist...");
            return false;
        }

        if (!d->sync) {
            reader->SetDataPath(d->dataPath);
            reader->SetFrameIndex(d->frameIndex);
            reader->SetModality(d->modality);
            d->outputImage = *(reader->GetThumbnailImage());
            d->output->UpdateThumbnailImage(d->frameIndex, d->modality, d->outputImage);
        }
        else {
            reader->SetDataPath(d->dataPath);
            auto [idx, modality, img] = (reader->GetLastThumbnailImageInfo());
            d->output->UpdateThumbnailImage(idx, modality, *img);
        }

        return true;
    }

    auto SetThumbnailImage::Impl::ReportError(SetThumbnailImage* self, const QString& message) const -> void {
        self->Error(message);
        if (output) output->ReportError(message);
    }
}
