#include "IExperimentReader.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    static IExperimentReader* theInstance{ nullptr };

    IExperimentReader::IExperimentReader() {
        theInstance = this;
    }

    IExperimentReader::~IExperimentReader() {
    }

    auto IExperimentReader::GetInstance() -> IExperimentReader* {
        return theInstance;
    }
}