#include <QCoreApplication>
#include <QElapsedTimer>

#include <System.h>
#include <SystemStatus.h>
#include <IInstrument.h>

#include "Utility.h"
#include "MoveSampleStage.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    namespace {
        class CurrentPositionUpdater {
        public:
            CurrentPositionUpdater(IMotionOutputPort* output, const AppEntity::WellIndex& wellIdx)
                : output{ output }
                , wellIdx{ wellIdx} {
            }

            ~CurrentPositionUpdater() {
                auto instrument = IInstrument::GetInstance();
                auto curPos = instrument->GetAxisPosition();
                AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);

                if(output) {
                    auto posInWell = global2well(curPos, wellIdx);
                    output->UpdatePosition(wellIdx, posInWell);
                    if(afPerformed) output->UpdateBestFocus(curPos.toMM().z);
                }
            }

            auto SetAutofocusPeformed(bool performed) {
                afPerformed = performed;
            }

            IMotionOutputPort* output{ nullptr };
            const AppEntity::WellIndex& wellIdx;
            bool afPerformed{ false };
        };

        enum class MotionType {
            Well2Well,
            XYInWell,
            ZInWell
        };
    }

    struct MoveSampleStage::Impl {
        AppEntity::WellIndex wellIdx{ -1 };
        AppEntity::Position targetPosition;
        bool moveXY{ true };
        bool moveZ{ false };
        MotionType motionType{ MotionType::Well2Well };

        IMotionOutputPort* output{ nullptr };
        MoveSampleStage* p{ nullptr };

        Impl(MoveSampleStage* p) : p{ p } {}
        auto ZReadyPosUM() const->double;
        auto ReportError(const QString& message)->void;
        auto IsAutofocusRequired() const->bool;
        auto WaitZMotion(int32_t waitInSeconds)->bool;
    };

    auto MoveSampleStage::Impl::ZReadyPosUM() const -> double {
        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto status = AppEntity::SystemStatus::GetInstance();
        const auto zReadyMM = sysConfig->GetAutofocusReadyPos();
        const auto afOffset = status->GetExperiment()->GetVessel()->GetAFOffset();
        return zReadyMM*1000 + afOffset;
    }

    auto MoveSampleStage::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
        if(output) output->ReportError(message);
    }

    auto MoveSampleStage::Impl::IsAutofocusRequired() const -> bool {
        return true;
    }

    auto MoveSampleStage::Impl::WaitZMotion(int32_t waitInSeconds) -> bool {
        auto instrument = IInstrument::GetInstance();

        QElapsedTimer timer;
        timer.start();

        auto motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < (waitInSeconds*1000)) && motionStatus.moving) {
            if(motionStatus.error) {
                ReportError(tr("It fails to move Z stage : %1").arg(motionStatus.message));
                return false;
            }

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

            const auto curPos = instrument->GetAxisPosition();
            AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);

            if(output) {
                auto posInWell = global2well(curPos, wellIdx);
                output->UpdatePosition(wellIdx, posInWell);
            }

            motionStatus = instrument->CheckAxisMotion();
        }

        return !motionStatus.moving;
    }

    MoveSampleStage::MoveSampleStage(IMotionOutputPort* output) : IUseCase("MoveSampleStage"), d{new Impl(this)} {
        d->output = output;
    }

    MoveSampleStage::~MoveSampleStage() {
    }

    auto MoveSampleStage::SetTargetWell(const AppEntity::WellIndex wellIdx) -> void {
        d->motionType = MotionType::Well2Well;

        auto curPos = AppEntity::SystemStatus::GetInstance()->GetCurrentGlobalPosition();
        d->wellIdx = wellIdx;
        d->targetPosition = well2global(wellIdx, AppEntity::Position::fromUM(0, 0, curPos.toUM().z));
        d->moveZ = true;
    }

    auto MoveSampleStage::SetTargetXInUm(const AppEntity::WellIndex wellIdx, double pos) -> void {
        d->motionType = MotionType::XYInWell;

        auto curPos = AppEntity::SystemStatus::GetInstance()->GetCurrentGlobalPosition();
        d->wellIdx = wellIdx;
        d->moveZ = false;
        d->targetPosition = AppEntity::Position::fromUM(pos, curPos.toUM().y, curPos.toUM().z);
    }

    auto MoveSampleStage::SetTargetYInUm(const AppEntity::WellIndex wellIdx, double pos) -> void {
        d->motionType = MotionType::XYInWell;

        auto curPos = AppEntity::SystemStatus::GetInstance()->GetCurrentGlobalPosition();
        d->wellIdx = wellIdx;
        d->moveZ = false;
        d->targetPosition = AppEntity::Position::fromUM(curPos.toUM().x, pos, curPos.toUM().z);
    }

    auto MoveSampleStage::SetTargetZInUm(const AppEntity::WellIndex wellIdx, double pos) -> void {
        d->motionType = MotionType::ZInWell;

        auto curPos = AppEntity::SystemStatus::GetInstance()->GetCurrentGlobalPosition();
        d->wellIdx = wellIdx;
        d->targetPosition = AppEntity::Position::fromUM(curPos.toUM().x, curPos.toUM().y, pos);
        d->moveXY = false;
        d->moveZ = true;
    }

    auto MoveSampleStage::SetTargetXYInUm(const AppEntity::WellIndex wellIdx, double xUm, double yUm) -> void {
        d->motionType = MotionType::XYInWell;

        auto curPos = AppEntity::SystemStatus::GetInstance()->GetCurrentGlobalPosition();
        auto curPosUm = curPos.toUM();
        d->wellIdx = wellIdx;
        d->moveZ = false;
        d->targetPosition = well2global(wellIdx, AppEntity::Position::fromUM(xUm, yUm, curPosUm.z));
    }

    auto MoveSampleStage::SetRelativeTargetPixels(const int32_t relPosX, const int32_t relPosY) -> void {
        d->motionType = MotionType::XYInWell;

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        d->wellIdx = sysStatus->GetCurrentWell();

        auto curPos = sysStatus->GetCurrentGlobalPosition();
        const auto relPosXUm = pixels2um(relPosX);
        const auto relPosYUm = pixels2um(relPosY);
        const auto relPos = AppEntity::Position::fromUM(relPosXUm, relPosYUm, 0);

        d->moveZ = false;
        d->targetPosition = curPos + relPos;
    }

    auto MoveSampleStage::SetRealtiveTargetUm(const AppEntity::Axis axis, const double distUm) -> void {
        d->motionType = [=]()->MotionType {
            if(axis == +AppEntity::Axis::Z) return MotionType::ZInWell;
            return MotionType::XYInWell;
        }();

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        d->wellIdx = sysStatus->GetCurrentWell();
        auto curPos = sysStatus->GetCurrentGlobalPosition();

        const double relPosXUm = (axis == +AppEntity::Axis::X)?distUm : 0;
        const double relPosYUm = (axis == +AppEntity::Axis::Y)?distUm : 0;
        const double relPosZUm = (axis == +AppEntity::Axis::Z)?distUm : 0;
        const auto relPos = AppEntity::Position::fromUM(relPosXUm, relPosYUm, relPosZUm);

        d->moveXY = (axis == +AppEntity::Axis::X) || (axis == +AppEntity::Axis::Y);
        d->moveZ = axis == +AppEntity::Axis::Z;

        d->targetPosition = curPos + relPos;
    }

    auto MoveSampleStage::SetTargetUm(const AppEntity::Axis axis, const double posInUm) -> void {
        d->motionType = [=]()->MotionType {
            if(axis == +AppEntity::Axis::Z) return MotionType::ZInWell;
            return MotionType::XYInWell;
        }();

        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        auto curPos = sysStatus->GetCurrentGlobalPosition();

        d->wellIdx = sysStatus->GetCurrentWell();
        d->moveXY = false;
        d->moveZ = false;

        if(axis == +AppEntity::Axis::Z) {
            d->moveZ = true;
            d->targetPosition = AppEntity::Position::fromUM(curPos.toUM().x, curPos.toUM().y, posInUm);
        } else if( axis == +AppEntity::Axis::X) {
            d->moveXY = true;
            d->targetPosition = AppEntity::Position::fromUM(posInUm, curPos.toUM().y, curPos.toUM().z);
        } else if( axis == +AppEntity::Axis::Y) {
            d->moveXY = true;
            d->targetPosition = AppEntity::Position::fromUM(curPos.toUM().x, posInUm, curPos.toUM().z);
        } else {
            Error(QString("This use case is not allowed to move axes except XYZ [axis=%1]").arg(axis._to_string()));
            d->targetPosition = curPos;
        }
    }

    auto MoveSampleStage::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();
        if(!instrument->IsInitialized()) return true;

        CurrentPositionUpdater positionUpdater(d->output, d->wellIdx);

        Print(QString("WellIndex=%1 TargetPosition=(%2,%3,%4)mm").arg(d->wellIdx)
                                                              .arg(d->targetPosition.toMM().x)
                                                              .arg(d->targetPosition.toMM().y)
                                                              .arg(d->targetPosition.toMM().z));

        if(d->motionType == MotionType::Well2Well) {
            const auto zReadyMM = d->ZReadyPosUM() / 1000;

            Print(QString("Move Z axis to ready position = %1mm").arg(zReadyMM));

            if(!instrument->MoveAxis(AppEntity::Axis::Z, zReadyMM)) {
                d->ReportError(tr("It fails to move Z stage [%1]").arg(instrument->GetErrorMessage()));
                return false;
            }

            if(!d->WaitZMotion(30)) {
                if(d->output) d->output->ReportAFFailure();
                return false;
            }
        }

        const auto afIsEnabled = instrument->AutoFocusEnabled();
        bool afPerformed = false;
        bool afFailed = false;

        if(d->motionType != MotionType::ZInWell) {
            if(!instrument->MoveAxis(d->targetPosition)) {
                d->ReportError(tr("It fails to move sample stage [%1]").arg(instrument->GetErrorMessage()));
                return false;
            }

            QElapsedTimer timer;
            timer.start();

            auto motionStatus = instrument->CheckAxisMotion();
            while((timer.elapsed() < 60000) && motionStatus.moving) {
                if(motionStatus.error) {
                    d->ReportError(tr("It fails to move sample stage : %1").arg(motionStatus.message));
                    return false;
                }

                QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

                const auto curPos = instrument->GetAxisPosition();
                AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);

                if(d->output) {
                    auto posInWell = global2well(curPos, d->wellIdx);
                    d->output->UpdatePosition(d->wellIdx, posInWell);
                }

                motionStatus = instrument->CheckAxisMotion();
            }

            if(motionStatus.moving) {
                d->ReportError(tr("Timeout is occurred"));
                return false;
            }

            afFailed = motionStatus.afFailed;

            if((motionStatus.afFailed == false) && afIsEnabled) {
                afPerformed = true;
            }
        }

        if(!afFailed) {
            if((d->motionType == MotionType::ZInWell) || 
               (d->motionType == MotionType::Well2Well && !afIsEnabled)) {
                if(!instrument->MoveAxis(AppEntity::Axis::Z, d->targetPosition.toMM().z)) {
                    d->ReportError(tr("It fails to move Z stage [%1]").arg(instrument->GetErrorMessage()));
                    return false;
                }

                if(!d->WaitZMotion(30)) {
                    return false;
                }
            }
        } else {
            d->ReportError(tr("Failed to find best focus position"));
            if(d->output) d->output->ReportAFFailure();

            const auto zReadyMM = d->ZReadyPosUM() / 1000;
            Print(QString("Move Z axis to ready position = %1mm").arg(zReadyMM));

            if(!instrument->MoveAxis(AppEntity::Axis::Z, zReadyMM)) {
                d->ReportError(tr("It fails to move Z stage [%1]").arg(instrument->GetErrorMessage()));
                return false;
            }

            if(!d->WaitZMotion(30)) {
                return false;
            }
        }

        positionUpdater.SetAutofocusPeformed(afPerformed);

        return true;
    }
}
