﻿#include "IThumbnailImageReader.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    static IThumbnailImageReader* theInstance {nullptr};
    IThumbnailImageReader::IThumbnailImageReader() {
        theInstance = this;
    }

    IThumbnailImageReader::~IThumbnailImageReader() {
    }

    auto IThumbnailImageReader::GetInstance() -> Self* {
        return theInstance;
    }
}
