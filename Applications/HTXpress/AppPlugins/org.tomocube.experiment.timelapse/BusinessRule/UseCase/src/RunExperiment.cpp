#include <QCoreApplication>
#include <QElapsedTimer>

#include <SystemStatus.h>

#include "Utility.h"
#include "IInstrument.h"
#include "IImagingSystem.h"
#include "IScanTimeCalculator.h"
#include "IExperimentRunner.h"
#include "RunExperiment.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct RunExperiment::Impl {
        IRunExperimentOutputPort* output{ nullptr };

        AppEntity::Experiment::Pointer experiment{ nullptr };
        AppEntity::VesselIndex vesselIdx{ 0 };

        IImagingSystem* imagingSystem{ nullptr };
        IInstrument* instrument{ nullptr };
        QString outPath;

        auto Ready(RunExperiment* p)->bool;
    };

    auto RunExperiment::Impl::Ready(RunExperiment* p) -> bool {
        imagingSystem = IImagingSystem::GetInstance();
        if(!imagingSystem) {
            p->Error("No imaging system exists");
            return false;
        }

        instrument = IInstrument::GetInstance();
        if(!instrument) {
            p->Error("No instrument exists");
            return false;
        }

        if(!instrument->IsInitialized()) {
            p->Error("Instrument is not initialized");
            return false;
        }

        const auto sysStatus = AppEntity::SystemStatus::GetInstance();

        experiment = sysStatus->GetExperiment();
        if(!experiment) {
            p->Error("No experiment is loaded");
            return false;
        }

        vesselIdx = sysStatus->GetCurrentVesselIndex();

        if(experiment->GetAllLocations(vesselIdx).isEmpty()) {
            p->Print("There is no images on the experiment, so it will run the experiment on the current position");
            experiment = std::make_shared<AppEntity::Experiment>(*experiment);

            const auto wellIdx = sysStatus->GetCurrentWell();
            if(wellIdx == -1) {
                p->Error("Current well is not valid");
                return false;
            }

            const auto pos = instrument->GetAxisPosition();
            const auto posInWell = global2well(pos, wellIdx);

            auto location = AppEntity::Location(posInWell, experiment->GetFOV(), false);
            experiment->AddLocation(vesselIdx, wellIdx, location);
        }

        auto scenario = experiment->GetScenario(vesselIdx);
        if(!scenario) {
            p->Error(QString("No imaging scenario exists for vessel #%1").arg(vesselIdx));
            return false;
        }

        auto sequenceCount = scenario->GetCount();
        if(sequenceCount<1) {
            p->Error("No valid imaging sequences exist");
            return false;
        }

        auto cond = scenario->GetSequence(0)->GetImagingCondition(0);
        if(!cond) {
            p->Error("Invalid imaging condition");
            return false;
        }

        outPath = sysStatus->GetExperimentOutputPath();
        if(outPath.isEmpty()) {
            p->Error("No data path is not specified");
            return false;
        }

        return true;
    }

    RunExperiment::RunExperiment(IRunExperimentOutputPort* output): IUseCase("RunExperiment"), d{new Impl} {
        d->output = output;
    }

    RunExperiment::~RunExperiment() {
    }

    auto RunExperiment::Perform() -> bool {
        if(!d->Ready(this)) {
            Error("System is not ready to run experiment");
            return false;
        }

        const auto sysStatus = AppEntity::SystemStatus::GetInstance();

        auto* runner = IExperimentRunner::GetInstance();
        if(!runner) {
            Error("No experiment runner exists");
            return false;
        }

        runner->SetOutputPort(d->output);
        runner->SetExperiment(sysStatus->GetProjectTitle(), d->experiment);
        runner->SetVesselIndex(d->vesselIdx);
        runner->SetOverlapInUM(AppEntity::System::GetTileScanOverlap());
        runner->SetAcquisitionType(UseCase::AcquisitionType::Timelapse);
        runner->SetMultiDishHolder(d->experiment->GetVessel()->IsMultiDish());

        if(!runner->Run()) {
            Error("Failed to run the experiment runner");
            return false;
        }

        return true;
    }
}