﻿#include "SetImagingPoint.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct SetImagingPoint::Impl {
        IThumbnailOutputPort* output{nullptr};

        QString wellPosition{};
        QString pointID{};

        auto ReportError(SetImagingPoint* self, const QString& message) -> void;
    };

    auto SetImagingPoint::Impl::ReportError(SetImagingPoint* self, const QString& message) -> void {
        self->Error(message);
        if(output) output->ReportError(message);
    }

    SetImagingPoint::SetImagingPoint(IThumbnailOutputPort* output) : IUseCase("SetImagingPoint"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    SetImagingPoint::~SetImagingPoint() = default;

    auto SetImagingPoint::SetCurrentImagingPoint(const QString& wellPosition, const QString& pointID) -> void {
        d->wellPosition = wellPosition;
        d->pointID = pointID;
    }

    auto SetImagingPoint::Perform() -> bool {
        if (d->output) {
            d->output->UpdateImagingPoint(d->wellPosition, d->pointID);
            return true;
        }

        return false;
    }
}
