#include <SystemStatus.h>

#include "IInstrument.h"
#include "Utility.h"
#include "SetBestFocus.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct SetBestFocus::Impl {
        enum Mode {
            Current,
            Target
        } mode{ Current };

        SetBestFocus* p{ nullptr };
        IInstrumentOutputPort* output{ nullptr };
        int32_t value{ 0 };

        Impl(SetBestFocus* p) : p(p) {}
        auto ReportError(const QString& message)->void;
    };

    auto SetBestFocus::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
    }

    SetBestFocus::SetBestFocus(IInstrumentOutputPort* output) : IUseCase("SetBestFocus"), d{new Impl(this)} {
        d->output = output;
    }

    SetBestFocus::~SetBestFocus() {
    }

    auto SetBestFocus::SetCurrent() -> void {
        d->mode = Impl::Mode::Current;
    }

    auto SetBestFocus::SetTarget(double value) -> void {
        d->mode = Impl::Mode::Target;
        d->value = value;
    }

    auto SetBestFocus::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();

        if(d->mode == Impl::Mode::Current) {
            if(!instrument->SetBestFocusCurrent(d->value)) {
                d->ReportError("It fails to set the best focus target value as the current");
                return false;
            }
        } else {
            if(!instrument->SetBestFocus(d->value)) {
                d->ReportError("It fails to set the best focus target value");
                return false;
            }
        }

        AppEntity::SystemStatus::GetInstance()->SetCurrentFocusTarget(d->value);

        auto curPos = instrument->GetAxisPosition();
        auto sysStatus = AppEntity::SystemStatus::GetInstance();
        sysStatus->SetCurrentGlobalPosition(curPos);
        sysStatus->SetFocusZPosition(curPos.toMM().z);

        if(d->output) {
            d->output->UpdateGlobalPosition(curPos);
            d->output->UpdateBestFocus(curPos.toMM().z);
        }

        return true;
    }
}
