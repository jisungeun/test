#include <QMap>

#include <System.h>
#include "ImagingTimeTable.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct ImagingTimeTable::Impl {
        struct Time {
            double time2D{ 0 };
            double time3D{ 0 };
        };

        QMap<AppEntity::Modality, Time> condTable;
        double cycleTime{ 0 };
        int32_t points{ 0 };
    };

    ImagingTimeTable::ImagingTimeTable() : d{new Impl} {
    }

    ImagingTimeTable::~ImagingTimeTable() {
    }

    auto ImagingTimeTable::SetTimeForCondition(AppEntity::Modality modality, bool is2D, double timeInSec) -> void {
        if(is2D) d->condTable[modality].time2D = timeInSec;
        else d->condTable[modality].time3D = timeInSec;
    }

    auto ImagingTimeTable::GetTimeForCondition(AppEntity::Modality modality, bool is2D) const -> double {
        if(!d->condTable.contains(modality)) return 0;
        return (is2D)?d->condTable[modality].time2D:d->condTable[modality].time3D;
    }

    auto ImagingTimeTable::GetTimeForAllConditions() const -> double {
        double totalTime{ 0 };

        for(auto key : d->condTable.keys()) {
            const auto& time = d->condTable[key];
            totalTime += time.time2D + time.time3D;
        }

        return totalTime;
    }

    auto ImagingTimeTable::GetTimeForConditions(const QList<AppEntity::ImagingCondition::Pointer>& conds) -> double {
        double allTime = 0;
        for(auto cond : conds) {
            const auto condTime = GetTimeForCondition(cond->GetModality(), cond->Is2D());
            allTime += condTime;
        }

        return allTime;
    }

    auto ImagingTimeTable::SetTimeForCycle(double timeInSec, int32_t points) -> void {
        d->cycleTime = timeInSec;
        d->points = points;
    }

    auto ImagingTimeTable::GetTimeForCycle() const -> double {
        return d->cycleTime;
    }

    auto ImagingTimeTable::GetPointCount() const -> int32_t {
        return d->points;
    }
}
