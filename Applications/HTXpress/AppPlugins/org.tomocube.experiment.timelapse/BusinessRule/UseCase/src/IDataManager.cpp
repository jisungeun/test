#include "IDataManager.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    static IDataManager* theInstance{ nullptr };

    IDataManager::IDataManager() {
        theInstance = this;
    }

    IDataManager::~IDataManager() {
    }

    auto IDataManager::GetInstance() -> IDataManager* {
        return theInstance;
    }
}
