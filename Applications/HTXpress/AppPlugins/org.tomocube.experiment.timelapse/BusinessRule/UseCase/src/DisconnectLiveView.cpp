#include <ILiveImageAcquisition.h>
#include <IInstrument.h>

#include "DisconnectLiveView.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    DisconnectLiveView::DisconnectLiveView() : IUseCase("DisconnectLiveView") {
    }

    DisconnectLiveView::~DisconnectLiveView() {
    }

    auto DisconnectLiveView::Perform() -> bool {
        auto* instrument = IInstrument::GetInstance();
        if(!instrument) {
            Error("No instrument exists");
            return false;
        }

        auto* liveAcquisition = ILiveImageAcquisition::GetInstance();
        if(!liveAcquisition) {
            Error("Live acquisition is not available");
            return false;
        }

        auto imagePort = liveAcquisition->GetImagePort();
        instrument->UninstallImagePort(imagePort);

        return true;
    }
}
