#include <QElapsedTimer>
#include <SystemStatus.h>

#include "IInstrument.h"
#include "EnableAutoFocus.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct EnableAutoFocus::Impl {
        EnableAutoFocus* p{ nullptr };
        IInstrumentOutputPort* output{ nullptr };
        bool enable{ false };

        Impl(EnableAutoFocus* p) : p(p) {}

        auto ReportError(const QString& message)->void;
        auto WaitZMotion(int32_t waitInSeconds)->bool;
    };

    auto EnableAutoFocus::Impl::ReportError(const QString& message) -> void {
        p->Error(message);        
    }

    auto EnableAutoFocus::Impl::WaitZMotion(int32_t waitInSeconds) -> bool {
        auto instrument = IInstrument::GetInstance();

        QElapsedTimer timer;
        timer.start();

        auto motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < (waitInSeconds*1000)) && motionStatus.moving) {
            if(motionStatus.error) {
                ReportError(tr("It fails to move Z stage : %1").arg(motionStatus.message));
                return false;
            }

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

            const auto curPos = instrument->GetAxisPosition();
            AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);

            if(output) {
                output->UpdateGlobalPosition(curPos);
            }

            motionStatus = instrument->CheckAxisMotion();
        }

        return !motionStatus.moving;
    }

    EnableAutoFocus::EnableAutoFocus(IInstrumentOutputPort* output) : IUseCase("EnableAutoFocus"), d{new Impl(this)} {
        d->output = output;
    }

    EnableAutoFocus::~EnableAutoFocus() {
    }

    auto EnableAutoFocus::SetEnable(bool enable) -> void {
        d->enable = enable;
    }

    auto EnableAutoFocus::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();

        auto zStartPosInMM = instrument->GetAxisPositionMM(AppEntity::Axis::Z);

        auto [res, afSuccess] = [&]()->std::tuple<bool, bool> {
            if(d->enable) return instrument->EnableAutoFocus();
            return std::make_tuple(instrument->DisableAutoFocus(), true);
        }();

        if(!res) {
            if(d->enable) d->ReportError(tr("It fails to enable auto focus"));
            else d->ReportError(tr("It fails to disable auto focus"));
            return false;
        }

        AppEntity::SystemStatus::GetInstance()->SetAutoFocusEnabled(d->enable);
        d->output->AutoFocusEnabled(d->enable);

        if(d->enable) {
            if(!afSuccess) {
                if(!instrument->MoveAxis(AppEntity::Axis::Z, zStartPosInMM)) {
                    d->ReportError(tr("It fails to move Z stage [%1]").arg(instrument->GetErrorMessage()));
                    return false;
                }

                if(!d->WaitZMotion(30)) {
                    return false;
                }
            }

            auto sysStatus = AppEntity::SystemStatus::GetInstance();
            const auto curPos = instrument->GetAxisPosition();
            sysStatus->SetCurrentGlobalPosition(curPos);

            d->output->UpdateGlobalPosition(curPos);
        }

        return true;
    }
}


