#include "IExperimentWriter.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    static IExperimentWriter* theInstance{ nullptr };

    IExperimentWriter::IExperimentWriter() {
        theInstance = this;
    }

    IExperimentWriter::~IExperimentWriter() {
    }

    auto IExperimentWriter::GetInstance() -> IExperimentWriter* {
        return theInstance;
    }
}
