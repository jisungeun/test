#include <QElapsedTimer>
#include <QCoreApplication>

#include <System.h>
#include <SystemStatus.h>
#include <IInstrument.h>

#include "UnloadVessel.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    struct UnloadVessel::Impl {
        UnloadVessel* p{ nullptr };
        IInstrumentOutputPort* output{ nullptr };

        Impl(UnloadVessel* p) : p(p) {}

        auto ReportError(const QString& message)->void;
    };

    auto UnloadVessel::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
        output->UpdateFailed(message);
    }

    UnloadVessel::UnloadVessel(IInstrumentOutputPort* output) : IUseCase("UnloadVessel"), d{new Impl(this)} {
        d->output = output;
    }

    UnloadVessel::~UnloadVessel() {
    }

    auto UnloadVessel::Perform() -> bool {
        auto model = AppEntity::System::GetModel();
        auto instrument = IInstrument::GetInstance();

        const auto startPos = instrument->GetAxisPosition();

        const auto xPos = model->LoadingXPositionMM();
        const auto yPos = model->LoadingYPositionMM();
        const auto zPos = model->SafeZPositionMM();

        if(d->output) {
            d->output->UpdateProgress(0, "Unloading Vessel");
        }

        if(!instrument->DisableAutoFocus()) {
            d->ReportError("Disable Auto focus");
            return false;
        }

        if(!instrument->MoveAxis(AppEntity::Axis::Z, zPos)) {
            d->ReportError(QString("It fails to move Z axis to the safe position [%1]").arg(instrument->GetErrorMessage()));
            return false;
        }

        QElapsedTimer timer;
        timer.start();

        auto motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < 60000) && motionStatus.moving) {
            if(motionStatus.error) {
                d->ReportError(QString("It fails to move Z axis to the safe position : %1").arg(motionStatus.message));
                return false;
            }

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

            const auto curPos = instrument->GetAxisPosition();
            AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);

            if(d->output) {
                const auto progress = (curPos.toMM().z - startPos.toMM().z) / (zPos - startPos.toMM().z);
                d->output->UpdateProgress(progress * 0.3);
                d->output->UpdateGlobalPosition(curPos);
            }

            motionStatus = instrument->CheckAxisMotion();
        }

        if(!instrument->MoveAxis(AppEntity::Axis::C, 0)) {
            d->ReportError(QString("It fails to move C axis to the origin [%1]").arg(instrument->GetErrorMessage()));
            return false;
        }

        timer.start();

        motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < 60000) && motionStatus.moving) {
            if(motionStatus.error) {
                d->ReportError(QString("It fails to move C axis to the origin : %1").arg(motionStatus.message));
                return false;
            }

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

            if(d->output) {
                d->output->UpdateProgress(std::min((timer.elapsed()/200)*0.001 + 0.3, 0.4));
            }

            motionStatus = instrument->CheckAxisMotion();
        }

        if(!instrument->MoveAxis(AppEntity::Position::fromMM(xPos, yPos, zPos))) {
            d->ReportError(QString("It fails to move sample stage to unloading position [%1]").arg(instrument->GetErrorMessage()));
            return false;
        }
        
        timer.start();

        motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < 120000) && motionStatus.moving) {
            if(motionStatus.error) {
                d->ReportError(QString("It fails to move sample stage to unloading position : %1").arg(motionStatus.message));
                return false;
            }

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

            const auto curPos = instrument->GetAxisPosition();
            AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);

            if(d->output) {
                const auto progressX = (curPos.toMM().x - startPos.toMM().x) / (xPos - startPos.toMM().x);
                const auto progressY = (curPos.toMM().y - startPos.toMM().y) / (yPos - startPos.toMM().y);
                d->output->UpdateProgress(std::min(progressX, progressY) * 0.4 + 0.4);
                d->output->UpdateGlobalPosition(curPos);
            }

            motionStatus = instrument->CheckAxisMotion();
        }
                
        const auto curPos = instrument->GetAxisPosition();
        AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);
        d->output->UpdateGlobalPosition(curPos);

        d->output->UpdateProgress(1.0);
        d->output->VesselUnloaded();

        return true;
    }
}