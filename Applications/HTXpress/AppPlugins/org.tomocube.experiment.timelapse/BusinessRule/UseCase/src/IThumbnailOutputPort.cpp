﻿#include "IThumbnailOutputPort.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    IThumbnailOutputPort::IThumbnailOutputPort() = default;
    IThumbnailOutputPort::~IThumbnailOutputPort() = default;
}
