#pragma once

#include "IDataOutputPort.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IDataManager {
    public:
        using Pointer = std::shared_ptr<IDataManager>;

    protected:
        IDataManager();

    public:
        virtual ~IDataManager();

        static auto GetInstance()->IDataManager*;

        virtual auto SetScanExperiment(const QString& user, const QString& project, const QString& experiment)->void = 0;

        virtual auto SetOutputPort(UseCase::IDataOutputPort* outputPort)->void = 0;
        virtual auto InstallDataScannerOutputPort(UseCase::IDataOutputPort* outputPort)->void = 0;
        virtual auto UninstallDataScannerOutputPort(UseCase::IDataOutputPort* outputPort)->void = 0;
        virtual auto InstallDataMonitorOutputPort(UseCase::IDataOutputPort* outputPort)->void = 0;
        virtual auto UninstallDataMonitorOutputPort(UseCase::IDataOutputPort* outputPort)->void = 0;
    };
}