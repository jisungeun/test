﻿#pragma once

#include <memory>

#include <QMap>

#include "IUseCase.h"
#include "IThumbnailOutputPort.h"

#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API SetAcquireStartTimes : public IUseCase{
    public:
        using FrameIndex = int32_t;
        using StartTimeInSec = int32_t;

        explicit SetAcquireStartTimes(IThumbnailOutputPort* output = nullptr);
        ~SetAcquireStartTimes() override;

        auto SetStartTimesInSec(const QMap<FrameIndex, StartTimeInSec>& startTimes) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
