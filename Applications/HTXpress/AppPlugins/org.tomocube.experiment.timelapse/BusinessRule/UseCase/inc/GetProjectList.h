#pragma once
#include <memory>
#include <QStringList>

#include "IUseCase.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API GetProjectList : public IUseCase {
    public:
        GetProjectList();
        ~GetProjectList();

        auto GetList() const->QStringList;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
        