#pragma once
#include <memory>

#include <PositionGroup.h>
#include <ImagingScenario.h>

#include <ChannelConfig.h>

#include "IImagePort.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IInstrument {
    public:
        using Pointer = std::shared_ptr<IInstrument>;

        struct MotionStatus {
            bool error{ false };
            bool moving{ false };
            bool afFailed{ false };
            QString message;
        };

    protected:
        IInstrument();

    public:
        virtual ~IInstrument();

        static auto GetInstance()->IInstrument*;

        virtual auto Initialize()->bool = 0;
        virtual auto IsInitialized()->bool = 0;

        virtual auto InstallImagePort(IImagePort::Pointer port)->void = 0;
        virtual auto UninstallImagePort(IImagePort::Pointer port)->void = 0;
        virtual auto ImageCountInBuffer() const->int32_t = 0;

        virtual auto ChangeFOV(int32_t xPixels, int32_t yPixels, int32_t& offsetX, int32_t& offsetY)->bool = 0;
        virtual auto ChangeFullFOV()->bool = 0;
        virtual auto ChangeLiveModality(AppEntity::Modality modality, int32_t channel = 0)->bool = 0;
        virtual auto GetCurrentLiveModality() const->std::tuple<AppEntity::Modality, int32_t> = 0;

        virtual auto RunImagingSequence(const AppEntity::ImagingSequence::Pointer sequence, 
                                        const QList<AppEntity::PositionGroup>& positions,
                                        const AppEntity::WellIndex startingWellIndex,
                                        const double focusReadyMM,
                                        const bool useMultiDishHolder)->bool = 0;
        virtual auto CheckSequenceProgress() const->std::tuple<bool,double,AppEntity::Position> = 0;
        virtual auto StopAcquisition()->bool = 0;

        virtual auto MoveAxis(const AppEntity::Position& target)->bool = 0;
        virtual auto MoveAxis(const AppEntity::Axis axis, const double targetMM)->bool = 0;
        virtual auto CheckAxisMotion() const->MotionStatus = 0;
        virtual auto GetAxisPosition() const->AppEntity::Position = 0;
        virtual auto GetAxisPositionMM(const AppEntity::Axis axis) const->double = 0;

        virtual auto StartLive(AppEntity::Modality modality, int32_t channel=0)->bool = 0;
        virtual auto StopLive()->bool = 0;
        virtual auto ResumeLive()->bool = 0;

        virtual auto EnableAutoFocus()->std::tuple<bool,bool> = 0;
        virtual auto DisableAutoFocus()->bool = 0;
        virtual auto AutoFocusEnabled() const->bool = 0;
        virtual auto PerformAutoFocus()->std::tuple<bool,bool> = 0;
        virtual auto SetBestFocusCurrent(int32_t& value)->bool = 0;
        virtual auto SetBestFocus(int32_t value)->bool = 0;

        virtual auto GetErrorMessage() const->QString = 0;
    };
}