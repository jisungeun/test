#pragma once
#include <memory>

#include <Experiment.h>
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IExperimentReader {
    public:
        using Pointer = std::shared_ptr<IExperimentReader>;

    protected:
        IExperimentReader();

    public:
        virtual ~IExperimentReader();

        static auto GetInstance()->IExperimentReader*;

        virtual auto Read(const QString& path) const->AppEntity::Experiment::Pointer = 0;
    };
}
