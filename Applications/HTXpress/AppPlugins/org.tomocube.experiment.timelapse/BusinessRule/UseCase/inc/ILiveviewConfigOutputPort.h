#pragma once

#include <ImagingConfig.h>

#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API ILiveviewConfigOutputPort {
    public:
        ILiveviewConfigOutputPort();
        virtual ~ILiveviewConfigOutputPort();

        virtual auto UpdateLiveConfig(const uint32_t intensity, const uint32_t exposure, const double gain)->void = 0;
        virtual auto UpdateAcquisitionConfig(const Entity::ImagingMode mode, const uint32_t intensity, const uint32_t exposure, const double gain)->void = 0;
    };
}