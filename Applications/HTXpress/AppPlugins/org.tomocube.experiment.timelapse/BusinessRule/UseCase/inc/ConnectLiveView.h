#pragma once
#include <memory>

#include "IUseCase.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API ConnectLiveView : public IUseCase {
    public:
        ConnectLiveView();
        ~ConnectLiveView() override;

    protected:
        auto Perform() -> bool override;
    };
}