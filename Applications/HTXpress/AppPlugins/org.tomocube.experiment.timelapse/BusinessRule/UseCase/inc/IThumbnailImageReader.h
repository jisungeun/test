﻿#pragma once

#include <memory>

#include <QString>
#include <QImage>

#include <AppEntityDefines.h>

#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IThumbnailImageReader {
    public:
        using Self = IThumbnailImageReader;
        using Pointer = std::shared_ptr<Self>;

        using FrameIndex = int32_t;

    protected:
        IThumbnailImageReader();

    public:
        virtual ~IThumbnailImageReader();

        static auto GetInstance()->Self*;

        virtual auto SetDataPath(const QString& dataPath) -> void = 0;
        virtual auto SetFrameIndex(const int32_t& frameIndex) -> void = 0;
        virtual auto SetModality(const AppEntity::Modality& modality) -> void = 0;

        [[nodiscard]] virtual auto GetThumbnailImage() const -> std::shared_ptr<QImage> = 0;
        [[nodiscard]] virtual auto GetLastThumbnailImageInfo() const -> std::tuple<FrameIndex, AppEntity::Modality, std::shared_ptr<QImage>> = 0;
    };
}
