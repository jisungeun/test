#pragma once
#include <memory>
#include <QString>

#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IDataOutputPort {
    public:
        IDataOutputPort();
        virtual ~IDataOutputPort();

        virtual auto ScannedData(const QString& user, const QString& project, const QString& experiment)->void = 0;
        virtual auto AddedData(const QString& fileFullPath)->void = 0;
        virtual auto UpdatedData(const QString& fileFullPath)->void = 0;
        virtual auto DeletedData(const QString& fileFullPath)->void = 0;
        virtual auto DeletedDataRootFolder(const QString& fileFullPath)->void = 0;
    };
}