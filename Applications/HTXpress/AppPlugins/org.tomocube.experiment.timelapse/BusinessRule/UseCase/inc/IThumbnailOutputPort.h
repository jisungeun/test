﻿#pragma once

#include <QString>
#include <QImage>

#include <AppEntityDefines.h>

#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IThumbnailOutputPort {
    public:
        IThumbnailOutputPort();
        virtual ~IThumbnailOutputPort();

        virtual auto UpdateImagingPoint(const QString& wellPosition, const QString& pointID) -> void = 0;
        virtual auto UpdateThumbnailImage(int32_t frameIndex, AppEntity::Modality modality, const QImage& image) -> void = 0;
        virtual auto UpdateStartTimesInSec(const QMap<int32_t, int32_t>& startTimesInSec) -> void = 0;

        virtual auto ReportError(const QString& message) -> void = 0;
    };
}
