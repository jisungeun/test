#pragma once
#include <memory>
#include <QString>

#include "IUseCase.h"
#include "IExperimentOutputPort.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API LoadExperiment : public IUseCase {
    public:
        LoadExperiment(IExperimentOutputPort* output = nullptr);
        ~LoadExperiment();

        auto SetExperiment(const QString& projectTitle, const QString& experimentTitle)->void;
        auto SetPath(const QString& path)->void;
        auto SetSingleRun(bool singleRun)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}