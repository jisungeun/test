#pragma once
#include <memory>

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "IInstrumentOutputPort.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API StartLiveView : public IUseCase {
    public:
        StartLiveView(IInstrumentOutputPort* output);
        ~StartLiveView() override;

        auto SetImageType(AppEntity::Modality modality, int32_t channel = 0)->void;
        auto SetLatest()->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}