#pragma once
#include <memory>

#include <Experiment.h>
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IExperimentWriter {
    public:
        using Pointer = std::shared_ptr<IExperimentWriter>;

    protected:
        IExperimentWriter();

    public:
        virtual ~IExperimentWriter();

        static auto GetInstance()->IExperimentWriter*;

        virtual auto Write(AppEntity::Experiment::Pointer experiment, const QString& path) const->bool = 0;
    };
}
