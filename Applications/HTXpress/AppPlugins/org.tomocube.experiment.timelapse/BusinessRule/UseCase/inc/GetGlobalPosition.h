#pragma once
#include <memory>

#include "IUseCase.h"
#include "IMotionOutputPort.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API GetGlobalPosition : public IUseCase {
    public:
        GetGlobalPosition(IMotionOutputPort* outputPort);
        ~GetGlobalPosition();

        auto CurrentPosition() const->AppEntity::Position;
        auto CurrentWell() const->AppEntity::WellIndex;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
