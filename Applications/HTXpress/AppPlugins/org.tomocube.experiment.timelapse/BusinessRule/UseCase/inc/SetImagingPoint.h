﻿#pragma once

#include <memory>

#include "IUseCase.h"
#include "IThumbnailOutputPort.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API SetImagingPoint : public IUseCase{
    public:
        explicit SetImagingPoint(IThumbnailOutputPort* output = nullptr);
        ~SetImagingPoint() override;

        auto SetCurrentImagingPoint(const QString& wellPosition, const QString& pointID) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
