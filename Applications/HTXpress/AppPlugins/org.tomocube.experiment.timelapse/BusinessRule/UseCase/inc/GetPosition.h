#pragma once
#include <memory>

#include "IUseCase.h"
#include "IMotionOutputPort.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API GetPosition : public IUseCase {
    public:
        GetPosition(IMotionOutputPort* outputPort);
        ~GetPosition();

        auto SetWell(const AppEntity::WellIndex& wellIdx)->void;
        auto CurrentPosition() const->AppEntity::Position;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
