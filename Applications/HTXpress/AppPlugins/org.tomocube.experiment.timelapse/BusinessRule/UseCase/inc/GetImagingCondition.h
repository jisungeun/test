#pragma once
#include <memory>

#include <ImagingConfig.h>

#include "IUseCase.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API GetImagingCondition : public IUseCase {
    public:
        GetImagingCondition();
        ~GetImagingCondition();

        auto GetConfig(Entity::ImagingMode mode)-> Entity::ChannelConfig::Pointer;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
