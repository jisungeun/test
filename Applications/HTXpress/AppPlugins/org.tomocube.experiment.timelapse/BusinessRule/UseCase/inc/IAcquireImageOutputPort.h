#pragma once
#include <memory>

#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IAcquireImageOutputPort {
    public:
        IAcquireImageOutputPort();
        virtual ~IAcquireImageOutputPort();

        virtual auto UpdateProgress(double progress)->void = 0;
    };
}