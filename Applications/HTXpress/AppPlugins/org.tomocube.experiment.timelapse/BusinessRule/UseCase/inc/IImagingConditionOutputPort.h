#pragma once
#include <memory>

#include "ImagingTimeTable.h"
#include <Position.h>
#include <AppEntityDefines.h>
#include <ChannelConfig.h>

#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IImagingConditionOutputPort {
    public:
        using ChannelConfig = AppEntity::ChannelConfig;

    public:
        IImagingConditionOutputPort();
        virtual ~IImagingConditionOutputPort();

        virtual auto Update(const ImagingTimeTable::Pointer& table)->void = 0;
        virtual auto UpdateFOV(const double xInUm, const double yInUm)->void = 0;
        virtual auto UpdateTileScanArea(bool enable, AppEntity::WellIndex wellIndex,
                                        double xInMm, double yInMm, 
                                        double widthInUm, double heightInUm)->void = 0;
        virtual auto ClearFLChannels()->void = 0;
        virtual auto UpdateBFEnabled(bool enabled)->void = 0;
        virtual auto UpdateHTEnabled(bool enabled)->void = 0;
        virtual auto UpdateEnabledImagingTypes(const QList<AppEntity::ImagingType>& types)->void = 0;
        virtual auto UpdateFLChannelConfig(const int32_t channel, const ChannelConfig::Pointer config)->void = 0;
        virtual auto UpdateAcquisitionLock(bool locked)->void = 0;
    };
}