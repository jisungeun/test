#pragma once
#include <memory>

#include "ExperimentStatus.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IRunExperimentOutputPort {
    public:
        IRunExperimentOutputPort();
        virtual ~IRunExperimentOutputPort();

        virtual auto UpdateProgress(const ExperimentStatus& status)->void = 0;
        virtual auto NotifyStopped()->void = 0;
        virtual auto UpdateProgressTime(const int32_t& elapsedTimeInSec, const int32_t& remainTimeInSec) -> void = 0;
        virtual auto UpdateCurrentPosition(const AppEntity::WellIndex& wellIndex, const AppEntity::Position& position) -> void = 0;
    };
}