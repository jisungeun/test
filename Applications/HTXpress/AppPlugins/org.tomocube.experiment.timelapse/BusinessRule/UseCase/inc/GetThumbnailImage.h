﻿#pragma once

#include <memory>

#include <QImage>
#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "IThumbnailOutputPort.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API GetThumbnailImage : public IUseCase {
    public:
        explicit GetThumbnailImage(IThumbnailOutputPort* output = nullptr);
        ~GetThumbnailImage() override;

        auto SetSync(bool sync) -> void;
        auto SetDataPath(const QString& path) -> void;
        auto SetFrameIndex(int32_t frameIndex) -> void;
        auto SetModality(AppEntity::Modality modality) -> void;
        auto GetImage() const -> QImage;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
