#pragma once
#include <memory>
#include <QRgb>

#include "IImagePort.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API ILiveImageAcquisition {
    public:
        using Pointer = std::shared_ptr<ILiveImageAcquisition>;

    protected:
        ILiveImageAcquisition();

    public:
        virtual ~ILiveImageAcquisition();

        static auto GetInstance()->ILiveImageAcquisition*;

        virtual auto Pause()->void = 0;
        virtual auto Resume()->void = 0;

        virtual auto SetMaxColor(const QRgb& color = qRgb(255,255,255))->void = 0;
        virtual auto GetImagePort() const->IImagePort::Pointer = 0;
    };
}