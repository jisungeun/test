#pragma once

#include <AppEntityDefines.h>

#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IVesselOutputPort {
    public:
        IVesselOutputPort();
        virtual ~IVesselOutputPort();

        virtual auto UpdateSelected(AppEntity::WellIndex wellIdx)->void = 0;
    };
}