#pragma once
#include <memory>

#include <Experiment.h>
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IExperimentOutputPort {
    public:
        IExperimentOutputPort();
        virtual ~IExperimentOutputPort();

        virtual auto Update(AppEntity::Experiment::Pointer experiment, bool singleRun = false)->void = 0;
        virtual auto Error(const QString& message)->void = 0;
    };
}