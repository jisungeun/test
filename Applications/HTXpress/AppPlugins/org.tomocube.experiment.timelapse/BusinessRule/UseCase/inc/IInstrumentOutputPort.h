#pragma once
#include <memory>
#include <QString>

#include <Position.h>
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IInstrumentOutputPort {
    public:
        IInstrumentOutputPort();
        virtual ~IInstrumentOutputPort();

        virtual auto UpdateFailed(const QString& message)->void = 0;
        virtual auto UpdateProgress(double progress, const QString& message=QString())->void = 0;

        virtual auto UpdateGlobalPosition(const AppEntity::Position& position)->void = 0;
        virtual auto ReportAFFailed()->void = 0;
        virtual auto UpdateBestFocus(double posInMm)->void = 0;
        virtual auto AutoFocusEnabled(bool enable)->void = 0;

        virtual auto LiveStarted()->void = 0;
        virtual auto LiveStopped()->void = 0;
        virtual auto LiveImagingFailed(const QString& message)->void = 0;

        virtual auto VesselLoaded()->void = 0;
        virtual auto VesselUnloaded()->void = 0;
    };

    class InstrumentNullOutputPort : public IInstrumentOutputPort {
    public:
        using Pointer = std::shared_ptr<InstrumentNullOutputPort>;

    public:
        InstrumentNullOutputPort() : IInstrumentOutputPort() {}
        ~InstrumentNullOutputPort() override {}

        static auto GetInstance()->InstrumentNullOutputPort*;

        auto UpdateFailed(const QString& message) -> void override {
            Q_UNUSED(message)
        }

        auto UpdateProgress(double progress, const QString& message) -> void override {
            Q_UNUSED(progress)
            Q_UNUSED(message)
        }

        auto UpdateGlobalPosition(const AppEntity::Position& position) -> void override {
            Q_UNUSED(position)
        }

        auto UpdateBestFocus(double posInMm) -> void override {
            Q_UNUSED(posInMm)
        }

        auto AutoFocusEnabled(bool enable) -> void override {
            Q_UNUSED(enable)
        }

        auto ReportAFFailed() -> void override {
        }

        auto LiveStarted() -> void override {
        }

        auto LiveStopped() -> void override {
        }

        auto LiveImagingFailed(const QString& message) -> void override {
            Q_UNUSED(message)
        }

        auto VesselLoaded() -> void override {
        }

        auto VesselUnloaded() -> void override {
        }
    };
}