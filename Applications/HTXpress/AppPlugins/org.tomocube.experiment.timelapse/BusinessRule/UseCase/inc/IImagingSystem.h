#pragma once
#include <memory>
#include <QString>

#include <Experiment.h>
#include <PositionGroup.h>

#include "IImagePort.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IImagingSystem {
    public:
        using Pointer = std::shared_ptr<IImagingSystem>;

    protected:
        IImagingSystem();

    public:
        virtual ~IImagingSystem();

        static auto GetInstance()->IImagingSystem*;

        virtual auto GetImagePort() const->IImagePort::Pointer = 0;

        //실험을 설정함
        //@param scenario 실험 시나리오
        //@param topPath 실험 결과가 저장될 곳의 최상위 경로
        virtual auto SetScenario(AppEntity::ImagingScenario::Pointer scenario, const QString& topPath)->void = 0;

        //이미지를 획득한 위치 그룹을 설정함
        //@param positions 위치 그룹 목록
        virtual auto SetPositions(const QList<AppEntity::PositionGroup>& positions)->void = 0;

        //새로운 데이타 획득을 시작함
        //@param index index 번째 Sequence에 해당하는 이미지를 획득할 것임
        virtual auto StartNewAcquisition(uint32_t index)->void = 0;
    };
}