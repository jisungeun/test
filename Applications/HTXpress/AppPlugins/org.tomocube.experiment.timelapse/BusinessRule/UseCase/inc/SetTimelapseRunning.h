#pragma once
#include <memory>

#include "IUseCase.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API SetTimelapseRunning : public IUseCase {
    public:
        SetTimelapseRunning();
        ~SetTimelapseRunning() override;

        auto SetRunning(bool isRunning)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}