#pragma once
#include <memory>

#include <RawImage.h>

#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API IImagePort {
    public:
        using Pointer = std::shared_ptr<IImagePort>;

    public:
        IImagePort();
        virtual ~IImagePort();

		virtual auto Send(AppEntity::RawImage::Pointer image)->void = 0;
        virtual auto Clear()->void = 0;
        virtual auto IsEmpty() const->bool = 0;

        virtual auto ResetCount()->void = 0;
        virtual auto GetCount() const->uint64_t = 0;
    };
}