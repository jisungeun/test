#pragma once

#include <memory>

#include <IDataOutputPort.h>

#include "IUseCase.h"
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API InstallDataScanner : public IUseCase {
    public:
        InstallDataScanner();
        ~InstallDataScanner();

        auto SetDataOutputPort(IDataOutputPort* outputPort)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
