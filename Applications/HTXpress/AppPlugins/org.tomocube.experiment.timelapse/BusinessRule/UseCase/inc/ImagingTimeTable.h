#pragma once
#include <memory>

#include <AppEntityDefines.h>
#include <ImagingCondition.h>
#include "HTX_Experiment_Timelapse_UseCaseExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::UseCase {
    class HTX_Experiment_Timelapse_UseCase_API ImagingTimeTable {
    public:
        using Pointer = std::shared_ptr<ImagingTimeTable>;

    public:
        ImagingTimeTable();
        ~ImagingTimeTable();

        auto SetTimeForCondition(AppEntity::Modality modality, bool is2D, double timeInSec)->void;
        auto GetTimeForCondition(AppEntity::Modality modality, bool is2D) const->double;
        auto GetTimeForAllConditions() const->double;
        auto GetTimeForConditions(const QList<AppEntity::ImagingCondition::Pointer>& conds)->double;

        auto SetTimeForCycle(double timeInSec, int32_t points)->void;
        auto GetTimeForCycle() const->double;
        auto GetPointCount() const->int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
