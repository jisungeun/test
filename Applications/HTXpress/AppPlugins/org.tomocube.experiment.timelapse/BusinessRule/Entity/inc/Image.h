#pragma once
#include <memory>
#include <QImage>

#include "HTX_Experiment_Timelapse_EntityExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Entity {
    class HTX_Experiment_Timelapse_Entity_API Image {
    public:
        typedef std::shared_ptr<Image> Pointer;

    public:
        Image();
        Image(uint32_t width, uint32_t height, uint32_t size, uint32_t bitDepth);
        Image(uint32_t width, uint32_t height, uint32_t size, uint32_t bitDepth, void* data);
        Image(uint32_t width, uint32_t height, uint32_t size, uint32_t bitDepth, const std::shared_ptr<uint8_t>& buffer);
        virtual ~Image();

        auto IsValid() const->bool;
        auto GetWidth() const->uint32_t;
        auto GetHeight() const->uint32_t;
        auto GetBufferSize() const->uint32_t;
        auto GetBitDepth() const->uint32_t;
        auto GetData() const->std::shared_ptr<uint8_t>;
        auto GetScanline(uint32_t row) const->uint8_t*;

        auto SetIndex(uint64_t index)->void;
        auto GetIndex() const->uint64_t;

        auto ToQImage()->QImage;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}