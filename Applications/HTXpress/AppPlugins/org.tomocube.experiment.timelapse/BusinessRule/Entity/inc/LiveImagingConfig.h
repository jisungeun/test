#pragma once
#include <memory>

#include <enum.h>

#include <ChannelConfig.h>
#include "HTX_Experiment_Timelapse_EntityExport.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Entity {
    BETTER_ENUM(LiveImagingMode, int32_t,
        FLCH0,
        FLCH1,
        FLCH2,
        BFGray
    );

    class HTX_Experiment_Timelapse_Entity_API LiveImagingConfig {
    public:
        using Pointer = std::shared_ptr<LiveImagingConfig>;
        using ChannelConfig = AppEntity::ChannelConfig;

    public:
        LiveImagingConfig();
        LiveImagingConfig(const LiveImagingConfig& other);
        ~LiveImagingConfig();

        auto operator=(LiveImagingConfig& other)->LiveImagingConfig&;

        auto SetChannelConfig(LiveImagingMode mode, const ChannelConfig& config)->void;
        auto GetChannelConfig(LiveImagingMode mode)->ChannelConfig::Pointer;
        auto GetChannelConfigAll()->QMap<LiveImagingMode, ChannelConfig::Pointer>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}