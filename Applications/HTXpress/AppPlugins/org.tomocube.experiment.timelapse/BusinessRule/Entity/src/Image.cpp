#include "Image.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Entity {
    static QVector<QRgb> g_grayTable;

    struct Image::Impl {
        uint32_t width{ 0 };
        uint32_t height{ 0 };
        uint32_t size{ 0 };
        uint32_t bitDepth{ 0 };
        std::shared_ptr<uint8_t> data;
        uint64_t index{ 0 };

        auto CreateGrayTable()->void;
    };

    auto Image::Impl::CreateGrayTable() -> void {
        for (int i = 0; i < 256; i++) {
            g_grayTable.push_back(qRgb(i, i, i));
        }
    }

    Image::Image() : d{ new Impl } {
    }

    Image::Image(uint32_t width, uint32_t height, uint32_t size, uint32_t bitDepth) : d{new Impl} {
        d->width = width;
        d->height = height;
        d->size = size;
        d->bitDepth = bitDepth;

        d->data.reset(new uint8_t[d->size]);
    }

    Image::Image(uint32_t width, uint32_t height, uint32_t size, uint32_t bitDepth, void* data) : d{ new Impl } {
        d->width = width;
        d->height = height;
        d->size = size;
        d->bitDepth = bitDepth;

        d->data.reset(new uint8_t[d->size]);
        memcpy_s(d->data.get(), d->size, data, d->size);
    }

    Image::Image(uint32_t width, uint32_t height, uint32_t size, uint32_t bitDepth, const std::shared_ptr<uint8_t>& buffer) : d{new Impl} {
        d->width = width;
        d->height = height;
        d->size = size;
        d->bitDepth = bitDepth;
        d->data = buffer;
    }

    Image::~Image() {
    }

    auto Image::IsValid() const -> bool {
        return (d->data != nullptr);
    }

    auto Image::GetWidth() const -> uint32_t {
        return d->width;
    }

    auto Image::GetHeight() const -> uint32_t {
        return d->height;
    }

    auto Image::GetBufferSize() const -> uint32_t {
        return d->size;
    }

    auto Image::GetBitDepth() const -> uint32_t {
        return d->bitDepth;
    }

    auto Image::GetData() const -> std::shared_ptr<uint8_t> {
        return d->data;
    }

    auto Image::GetScanline(uint32_t row) const -> uint8_t* {
        return d->data.get() + (row*d->width);
    }

    auto Image::SetIndex(uint64_t index) -> void {
        d->index = index;
    }

    auto Image::GetIndex() const -> uint64_t {
        return d->index;
    }

    auto Image::ToQImage() -> QImage {
        if(g_grayTable.isEmpty()) {
            d->CreateGrayTable();
        }

        QImage qImage(d->width, d->height, QImage::Format_Indexed8);
        qImage.setColorTable(g_grayTable);
        const size_t rowSize = d->width * (d->bitDepth/8);
        for (uint32_t row = 0; row < d->height; row++) {
            memcpy_s(qImage.scanLine(row), rowSize, d->data.get()+(row*rowSize), rowSize);
        }
        return qImage.copy();
    }
}
