#include <QMap>

#include "LiveImagingConfig.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse::Entity {
    struct LiveImagingConfig::Impl {
        QMap<LiveImagingMode, ChannelConfig::Pointer> channelConfigs = {
            {LiveImagingMode::FLCH0, std::make_shared<ChannelConfig>()},
            {LiveImagingMode::FLCH1, std::make_shared<ChannelConfig>()},
            {LiveImagingMode::FLCH2, std::make_shared<ChannelConfig>()},
            {LiveImagingMode::BFGray, std::make_shared<ChannelConfig>()}
        };

        auto operator=(const Impl& other)->Impl& {
            channelConfigs.clear();

            for(auto key : other.channelConfigs.keys()) {
                channelConfigs[key] = std::make_shared<ChannelConfig>(*other.channelConfigs[key]);
            }

            return *this;
        }
    };

    LiveImagingConfig::LiveImagingConfig() : d{ new Impl } {
    }

    LiveImagingConfig::LiveImagingConfig(const LiveImagingConfig& other) {
        *d = *other.d;
    }

    LiveImagingConfig::~LiveImagingConfig() {
    }

    auto LiveImagingConfig::operator=(LiveImagingConfig& other) -> LiveImagingConfig& {
        *d = *other.d;
        return *this;
    }

    auto LiveImagingConfig::SetChannelConfig(LiveImagingMode mode, const ChannelConfig& config) -> void {
        d->channelConfigs[mode] = std::make_shared<ChannelConfig>(config);
    }

    auto LiveImagingConfig::GetChannelConfig(LiveImagingMode mode) -> ChannelConfig::Pointer {
        return d->channelConfigs[mode];
    }

    auto LiveImagingConfig::GetChannelConfigAll() -> QMap<LiveImagingMode, ChannelConfig::Pointer> {
        return d->channelConfigs;
    }

}
