#define LOGGER_TAG "[MainControl]"
#include <QStandardPaths>

#include <TCLogger.h>
#include <System.h>
#include <SystemStatus.h>
#include <SessionManager.h>
#include <UserManager.h>
#include <Model.h>

#include <InstrumentController.h>
#include <ExperimentIOController.h>
#include <ExperimentController.h>
#include <SystemStatusController.h>

#include <InstrumentPlugin.h>
#include <ExperimentRunner.h>
#include <ScanTimeCalcaultorFactory.h>
#include <ExperimentReaderPlugin.h>
#include <ExperimentWriterPlugin.h>
#include <DataManager.h>
#include <ImagingSystem.h>
#include <ThumbnailImageReaderPlugin.h>
#include <ImagingSystemPlugin.h>
#include <ImagingProfileLoaderPlugin.h>
#include <UseCaseLogger.h>

#include "Internal/InstrumentUpdater.h"
#include "Internal/ExperimentIOUpdater.h"
#include "Internal/RunExperimentUpdater.h"
#include "MainWindowControl.h"

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    using InstrumentPlugin = Plugins::Instrument::Instrument;
    using ExpRunnerPlugin = Plugins::ExperimentRunner::Runner;
    using ScanTimeCalculatorFactory = Plugins::ScanTimeCalculator::Factory;
    using ScanTimeCalculatorPlugin = UseCase::IScanTimeCalculator;
    using UseCaseLoggerPlugin = Plugins::UseCaseLogger::Logger;
    using ExperimentReaderPlugin = Plugins::ExperimentReader::Reader;
    using ExperimentWriterPlugin = Plugins::ExperimentWriter::Writer;
    using ImagingSystemPlugin = Plugins::ImagingSystem::Plugin;
    using DataManagerPlugin = Plugins::DataManager::DataManager;
    using ThumbnailReaderPlugin = Plugins::ThumbnailImageReader::ThumbnailImageReaderPlugin;
    using ImagingProfileLoaderPlugin = Plugins::ImagingProfileLoader::Loader;

    struct MainWindowControl::Impl {
        std::shared_ptr<InstrumentPlugin> instrument{ new InstrumentPlugin() };
        std::shared_ptr<ExpRunnerPlugin> experimentRunner{ new ExpRunnerPlugin() };
        std::shared_ptr<ScanTimeCalculatorPlugin> scanTimeCalculator{ ScanTimeCalculatorFactory::Build() };
        std::shared_ptr<UseCaseLoggerPlugin> usecaseLogger{ new UseCaseLoggerPlugin() };
        std::shared_ptr<ExperimentReaderPlugin> experimentReader{ new ExperimentReaderPlugin() };
        std::shared_ptr<ExperimentWriterPlugin> experimentWriter{ new ExperimentWriterPlugin() };
        std::shared_ptr<ImagingSystemPlugin> imagingSystem{ new ImagingSystemPlugin() };
        std::shared_ptr<DataManagerPlugin> dataManager{ new DataManagerPlugin() };
        std::shared_ptr<ThumbnailReaderPlugin> thumbnailReader{ new ThumbnailReaderPlugin() };
        std::shared_ptr<ImagingProfileLoaderPlugin> imagingProfileLoader{ new ImagingProfileLoaderPlugin() };
    };

    MainWindowControl::MainWindowControl() : d{new Impl} {
    }

    MainWindowControl::~MainWindowControl() {
        QLOG_INFO() << "Clean up";
    }

    auto MainWindowControl::CleanUp() -> void {
    }

    auto MainWindowControl::ConnectInstrument() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);
        return controller.Connect();
    }

    auto MainWindowControl::LoadExperiment(const QString& projectTitle, const QString& experimentTitle, bool singleRun) -> bool {
        auto updater = ExperimentIOUpdater::GetInstance();
        auto presenter = Interactor::ExperimentIOPresenter(updater.get());
        auto controller = Interactor::ExperimentIOController(&presenter);

        return controller.Load(projectTitle, experimentTitle, singleRun);
    }

    auto MainWindowControl::ReadyToLoadVessel() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);
        return controller.UnloadVessel();
    }

    auto MainWindowControl::IsExperimentRunning() -> bool {
        return AppEntity::SystemStatus::GetInstance()->GetBusy();
    }

    auto MainWindowControl::SetTimelapseRunning(bool isRunning) -> bool {
        auto controller = Interactor::SystemStatusController();
        return controller.SetTimelapseRunning(isRunning);
    }

    auto MainWindowControl::IsTimelapseRunning() -> bool {
        return AppEntity::SystemStatus::GetInstance()->GetTimelapseRunning();
    }

    auto MainWindowControl::ConnectLiveView() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return controller.ConnectLive();
    }

    auto MainWindowControl::DisconnectLiveView() -> bool {
        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return controller.DisconnectLive();
    }
}
