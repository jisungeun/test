#pragma once
#include <memory>

namespace HTXpress::AppPlugins::Experiment::Timelapse{
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

        auto CleanUp()->void;

        auto ConnectInstrument()->bool;
        auto LoadExperiment(const QString& projectTitle, const QString& experimentTitle, bool singleRun = false)->bool;
        auto ReadyToLoadVessel()->bool;
        auto IsExperimentRunning()->bool;
        auto SetTimelapseRunning(bool isRunning)->bool;
        auto IsTimelapseRunning()->bool;

        auto ConnectLiveView()->bool;
        auto DisconnectLiveView()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}