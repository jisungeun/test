﻿#include "IMediumConfigWriter.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    static IMediumConfigWriter* theInstance{nullptr};
    IMediumConfigWriter::IMediumConfigWriter() {
        theInstance = this;
    }

    IMediumConfigWriter::~IMediumConfigWriter() {
    }

    auto IMediumConfigWriter::GetInstance() -> IMediumConfigWriter* {
        return theInstance;
    }
}
