﻿#include "IFLConfigWriter.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    static IFLConfigWriter* theInstance{nullptr};
    IFLConfigWriter::IFLConfigWriter() {
        theInstance = this;
    }

    IFLConfigWriter::~IFLConfigWriter() {
    }

    auto IFLConfigWriter::GetInstance() -> IFLConfigWriter* {
        return theInstance;
    }
}