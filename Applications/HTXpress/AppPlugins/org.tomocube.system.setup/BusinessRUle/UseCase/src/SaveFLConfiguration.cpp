﻿#include "IFLConfigWriter.h"
#include "SaveFLConfiguration.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    struct SaveFLConfiguration::Impl {
        
    };

    SaveFLConfiguration::SaveFLConfiguration() : IUseCase("SaveFLConfiguration"), d{std::make_unique<Impl>()} {
    }

    SaveFLConfiguration::~SaveFLConfiguration() {
    }

    auto SaveFLConfiguration::Perform() -> bool {
        auto* writer = IFLConfigWriter::GetInstance();
        if(!writer) {
            Error("No valid fl configuration writer exists");
            return false;
        }

        if(!writer->Write()) {
            Error("It fails to write fl configuration");
            return false;
        }

        return true;
    }
}
