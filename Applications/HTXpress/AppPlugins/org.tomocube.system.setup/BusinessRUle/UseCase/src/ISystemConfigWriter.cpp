#include "ISystemConfigWriter.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    static ISystemConfigWriter* theInstance{ nullptr };
    ISystemConfigWriter::ISystemConfigWriter() {
        theInstance = this;
    }

    ISystemConfigWriter::~ISystemConfigWriter() {
    }

    auto ISystemConfigWriter::GetInstance() -> ISystemConfigWriter* {
        return theInstance;
    }
}
