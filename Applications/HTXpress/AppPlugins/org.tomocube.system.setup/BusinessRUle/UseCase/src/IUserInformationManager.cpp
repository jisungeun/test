﻿#include "IUserInformationManager.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    static IUserInformationManager* theInstance{nullptr};
    IUserInformationManager::IUserInformationManager() {
        theInstance = this;
    }

    IUserInformationManager::~IUserInformationManager() {
    }

    auto IUserInformationManager::GetInstance() -> IUserInformationManager* {
        return theInstance;
    }
}
