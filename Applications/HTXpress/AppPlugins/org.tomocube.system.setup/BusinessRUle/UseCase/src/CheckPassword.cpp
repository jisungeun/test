﻿#include "IUserInformationManager.h"
#include "CheckPassword.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    struct CheckPassword::Impl {
        AppEntity::UserID id{};
        QString password{};
    };

    CheckPassword::CheckPassword() : IUseCase("CheckPassword"), d{std::make_unique<Impl>()} {
    }

    CheckPassword::~CheckPassword() {
    }

    auto CheckPassword::SetAccount(const AppEntity::UserID& id, const QString& password) -> void {
        d->id = id;
        d->password = password;
    }

    auto CheckPassword::Perform() -> bool {
        auto manager = IUserInformationManager::GetInstance();
        return manager->CheckPassword(d->id, d->password);
    }
}
