﻿#include "SaveMediumConfiguration.h"
#include "IMediumConfigWriter.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    struct SaveMediumConfiguration::Impl {
        
    };

    SaveMediumConfiguration::SaveMediumConfiguration() : IUseCase("SaveMediumConfiguration"), d{std::make_unique<Impl>()} {
    }

    SaveMediumConfiguration::~SaveMediumConfiguration() {
    }

    auto SaveMediumConfiguration::Perform() -> bool {
        auto* writer = IMediumConfigWriter::GetInstance();
        if(!writer) {
            Error("No valid medium configuration writer exists");
            return false;
        }

        if(!writer->Write()) {
            Error("It fails to write medium configuration");
            return false;
        }

        return true;
    }
}
