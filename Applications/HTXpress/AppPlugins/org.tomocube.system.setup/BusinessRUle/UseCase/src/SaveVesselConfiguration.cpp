﻿#include "SaveVesselConfiguration.h"
#include "IVesselConfigWriter.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    struct SaveVesselConfiguration::Impl {
        
    };

    SaveVesselConfiguration::SaveVesselConfiguration() : IUseCase("SaveVesselConfiguration"), d{std::make_unique<Impl>()} {
    }

    SaveVesselConfiguration::~SaveVesselConfiguration() {
    }

    auto SaveVesselConfiguration::Perform() -> bool {
        auto* writer = IVesselConfigWriter::GetInstance();
        if(!writer) {
            Error("No valid vessel config writer exists");
            return false;
        }

        if(!writer->Write()) {
            Error("It fails to write vessel config");
            return false;
        }

        return true;
    }
}
