﻿#include "AddUser.h"
#include "IUserInformationManager.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    struct AddUser::Impl {
        AppEntity::User::Pointer user;
        QString password;
    };

    AddUser::AddUser() : IUseCase("AddUser"), d{new Impl} {
    }

    AddUser::~AddUser() {
    }

    auto AddUser::Add(const AppEntity::User::Pointer& user, const QString& password) -> void {
        d->user = user;
        d->password = password;
    }

    auto AddUser::Perform() -> bool {
        return IUserInformationManager::GetInstance()->AddUser(d->user, d->password);
    }
}
