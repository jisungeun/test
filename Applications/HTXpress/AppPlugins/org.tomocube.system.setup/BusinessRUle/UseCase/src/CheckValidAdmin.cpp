#include "IUserInformationManager.h"
#include "CheckValidAdmin.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    struct CheckValidAdmin::Impl {
        AppEntity::UserID id;
        QString password;
    };

    CheckValidAdmin::CheckValidAdmin() : IUseCase("CheckValidAdmin"), d{new Impl} {
    }

    CheckValidAdmin::~CheckValidAdmin() {
    }

    auto CheckValidAdmin::Set(const AppEntity::UserID& id, const QString& password) -> void {
        d->id = id;
        d->password = password;
    }

    auto CheckValidAdmin::Perform() -> bool {
        auto manager = IUserInformationManager::GetInstance();
        if(!manager->CheckPassword(d->id, d->password)) return false;

        auto user = manager->GetUser(d->id);
        auto profile = user->GetProfile();
        return profile != +AppEntity::Profile::Operator;
    }
}
