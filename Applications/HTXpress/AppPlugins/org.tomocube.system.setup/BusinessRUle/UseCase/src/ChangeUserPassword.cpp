﻿#include "ChangeUserPassword.h"
#include "IUserInformationManager.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    struct ChangeUserPassword::Impl {
        struct {
            AppEntity::UserID id;
            QString pw;
        } user;    
    };

    ChangeUserPassword::ChangeUserPassword() : IUseCase("ChangeUserPassword"), d{std::make_unique<Impl>()} {
    }

    ChangeUserPassword::~ChangeUserPassword() {
    }

    auto ChangeUserPassword::ChangePassword(const AppEntity::UserID& id, const QString& pw) -> void {
        d->user.id = id;
        d->user.pw = pw;
    }

    auto ChangeUserPassword::Perform() -> bool {
        return IUserInformationManager::GetInstance()->ChangePassword(d->user.id, d->user.pw);
    }
}
