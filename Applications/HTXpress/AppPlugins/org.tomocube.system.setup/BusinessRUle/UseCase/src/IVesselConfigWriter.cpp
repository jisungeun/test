﻿#include "IVesselConfigWriter.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    static IVesselConfigWriter* theInstance{nullptr};

    IVesselConfigWriter::IVesselConfigWriter() {
        theInstance = this;
    }

    IVesselConfigWriter::~IVesselConfigWriter() {
    }

    auto IVesselConfigWriter::GetInstance() -> IVesselConfigWriter* {
        return theInstance;
    }
}
