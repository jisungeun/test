﻿#include <QFile>

#include "DeleteVesselConfiguration.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    struct DeleteVesselConfiguration::Impl {
        QStringList files;
    };

    DeleteVesselConfiguration::DeleteVesselConfiguration(const QStringList& files) : IUseCase("DeleteVesselConfiguration"), d{std::make_unique<Impl>()}{
        d->files = files;
    }

    DeleteVesselConfiguration::~DeleteVesselConfiguration() {
    }

    auto DeleteVesselConfiguration::Perform() -> bool {
        for(const auto &file : d->files) {
            if(QFile::remove(file))
                Print(QString("File %1 has been deleted.").arg(file));
            else
                Error(QString("File %1 does not exist or has not been deleted.").arg(file));
        }

        return true;
    }
}
