﻿#include "ModifyUser.h"
#include "IUserInformationManager.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    struct ModifyUser::Impl {
        AppEntity::User::Pointer user;
    };

    ModifyUser::ModifyUser() : IUseCase("ModifyUser"), d{new Impl} {
    }

    ModifyUser::~ModifyUser() {
    }

    auto ModifyUser::Modify(const AppEntity::User::Pointer& user) -> void {
        d->user = user;
    }

    auto ModifyUser::Perform() -> bool {
        return IUserInformationManager::GetInstance()->EditUser(d->user);
    }
}
