﻿#include <SessionManager.h>

#include "DeleteUser.h"
#include "IUserInformationManager.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    struct DeleteUser::Impl {
        AppEntity::UserID id;
    };

    DeleteUser::DeleteUser() : IUseCase("DeleteUser"), d{new Impl} {
    }

    DeleteUser::~DeleteUser() {
    }

    auto DeleteUser::Delete(const AppEntity::UserID& id) -> void {
        d->id = id;
    }

    auto DeleteUser::Perform() -> bool {
        auto session = AppEntity::SessionManager::GetInstance();
        if(!session->LoggedIn()) return false;

        const auto userID = session->GetID();
        if(userID.compare(d->id, Qt::CaseInsensitive) == 0) return false;

        return IUserInformationManager::GetInstance()->DeleteUser(d->id);
    }
}
