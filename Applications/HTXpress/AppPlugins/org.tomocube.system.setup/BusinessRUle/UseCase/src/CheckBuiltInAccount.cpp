﻿#include "CheckBuiltInAccount.h"
#include "IUserInformationManager.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    struct CheckBuiltInAccount::Impl {
        AppEntity::UserID id{};
    };

    CheckBuiltInAccount::CheckBuiltInAccount(): IUseCase("CheckBuiltInAccount"), d{std::make_unique<Impl>()} {
    }

    CheckBuiltInAccount::~CheckBuiltInAccount() {
    }

    auto CheckBuiltInAccount::SetID(const AppEntity::UserID& id) -> void {
        d->id = id;
    }

    auto CheckBuiltInAccount::Perform() -> bool {
        const auto manager = IUserInformationManager::GetInstance();
        return manager->IsBuiltInAccount(d->id);
    }
}
