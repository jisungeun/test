#pragma once
#include <memory>
#include <QString>

#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API IUseCase {
    public:
        IUseCase(const QString& title);
        virtual ~IUseCase();

        auto Request()->bool;

    protected:
        auto Print(const QString& message)->void;
        auto Error(const QString& error)->void;
        
        virtual auto Perform()->bool = 0;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}