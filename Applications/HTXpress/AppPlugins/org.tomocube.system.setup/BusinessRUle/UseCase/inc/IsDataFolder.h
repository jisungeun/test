#pragma once
#include <memory>

#include "IUseCase.h"
#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API IsDataFolder : public IUseCase {
    public:
        IsDataFolder();
        ~IsDataFolder() override;

        auto IsValid(const QString& path)->bool;

    protected:
        auto Perform() -> bool override;
    };
}