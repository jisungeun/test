#pragma once
#include <QString>

#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API IUseCaseLogger {
    public:
        IUseCaseLogger();
        virtual ~IUseCaseLogger();

        static auto PrintLog(const QString& useCase, const QString& message)->void;
        static auto PrintError(const QString& useCase, const QString& message)->void;

    protected:
        virtual auto Log(const QString& useCase, const QString& message)->void = 0;
        virtual auto Error(const QString& useCase, const QString& message)->void = 0;
    };
}