﻿#pragma once

#include <memory>

#include <User.h>
#include <UserManager.h>
#include <AppEntityDefines.h>

#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API IUserInformationManager {
    public:
        using Pointer = std::shared_ptr<IUserInformationManager>;

    protected:
        IUserInformationManager();

    public:
        virtual ~IUserInformationManager();

        static auto GetInstance() -> IUserInformationManager*;

        virtual auto GetUser(const AppEntity::UserID& id) const->AppEntity::User::Pointer = 0;
        virtual auto AddUser(const AppEntity::User::Pointer& user, const QString& password) -> bool = 0;
        virtual auto DeleteUser(const AppEntity::UserID& id) -> bool = 0;
        virtual auto EditUser(const AppEntity::User::Pointer& user) -> bool = 0;
        virtual auto ChangePassword(const AppEntity::UserID& id, const QString& newPw) -> bool = 0;
        virtual auto CheckPassword(const AppEntity::UserID& id, const QString& password) -> bool = 0;
        virtual auto IsBuiltInAccount(const AppEntity::UserID& id) -> bool = 0;
    };
}

