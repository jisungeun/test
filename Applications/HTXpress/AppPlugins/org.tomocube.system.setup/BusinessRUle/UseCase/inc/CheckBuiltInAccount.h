﻿#pragma once
#include <memory>

#include <User.h>

#include "IUseCase.h"
#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API CheckBuiltInAccount : public IUseCase{
    public:
        CheckBuiltInAccount();
        ~CheckBuiltInAccount() override;

        auto SetID(const AppEntity::UserID& id) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
