﻿#pragma once

#include <AppEntityDefines.h>
#include <User.h>

#include "IUseCase.h"
#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API ModifyUser : public IUseCase {
    public:
        ModifyUser();
        ~ModifyUser() override;

        auto Modify(const AppEntity::User::Pointer& user) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
