﻿#pragma once

#include <memory>

#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API IMediumConfigWriter {
    public:
        using Pointer = std::shared_ptr<IMediumConfigWriter>;

    protected:
        IMediumConfigWriter();

    public:
        virtual ~IMediumConfigWriter();
        static auto GetInstance() -> IMediumConfigWriter*;
        virtual auto Write() -> bool = 0;
    };
}
