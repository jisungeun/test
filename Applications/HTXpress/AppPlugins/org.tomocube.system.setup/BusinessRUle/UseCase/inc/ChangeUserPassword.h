﻿#pragma once

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API ChangeUserPassword : public IUseCase {
    public:
        ChangeUserPassword();
        ~ChangeUserPassword() override;

        auto ChangePassword(const AppEntity::UserID& id, const QString& pw) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
