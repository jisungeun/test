#pragma once
#include <memory>
#include <User.h>

#include "IUseCase.h"
#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API CheckValidAdmin : public IUseCase {
    public:
        CheckValidAdmin();
        ~CheckValidAdmin() override;

        auto Set(const AppEntity::UserID& id, const QString& password)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
