﻿#pragma once
#include "IUseCase.h"
#include "HTX_System_Setup_UseCaseExport.h"
#include "AppEntityDefines.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API CheckPassword final : public IUseCase {
    public:
        CheckPassword();
        ~CheckPassword() override;

        auto SetAccount(const AppEntity::UserID& id, const QString& password) -> void;

    private:
        auto Perform() -> bool override;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
