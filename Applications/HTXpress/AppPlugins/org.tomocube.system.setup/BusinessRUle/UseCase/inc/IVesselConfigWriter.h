﻿#pragma once

#include <memory>

#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API IVesselConfigWriter {
    public:
        using Pointer = std::shared_ptr<IVesselConfigWriter>;

    protected:
        IVesselConfigWriter();

    public:
        virtual ~IVesselConfigWriter();
        static auto GetInstance() -> IVesselConfigWriter*;
        virtual auto Write() -> bool = 0;
    };
}
