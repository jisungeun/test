﻿#pragma once

#include <memory>

#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API IFLConfigWriter {
    public:
        using Pointer = std::shared_ptr<IFLConfigWriter>;

    protected:
        IFLConfigWriter();

    public:
        virtual ~IFLConfigWriter();

        static auto GetInstance()->IFLConfigWriter*;

        virtual auto Write()->bool = 0;
    };
}
