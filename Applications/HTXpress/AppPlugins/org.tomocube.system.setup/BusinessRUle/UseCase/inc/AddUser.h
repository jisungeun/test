﻿#pragma once

#include <User.h>

#include "IUseCase.h"
#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API AddUser : public IUseCase {
    public:
        AddUser();
        ~AddUser() override;

        auto Add(const AppEntity::User::Pointer& user, const QString& password) -> void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
