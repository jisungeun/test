﻿#pragma once

#include <memory>

#include <QStringList>

#include "IUseCase.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API DeleteVesselConfiguration : public IUseCase{
    public:
        explicit DeleteVesselConfiguration(const QStringList& files);
        ~DeleteVesselConfiguration() override;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
