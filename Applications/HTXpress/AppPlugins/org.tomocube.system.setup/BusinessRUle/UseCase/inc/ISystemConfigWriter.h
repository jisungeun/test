#pragma once
#include <memory>
#include <QString>

#include "HTX_System_Setup_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Setup::UseCase {
    class HTX_System_Setup_UseCase_API ISystemConfigWriter {
    public:
        using Pointer = std::shared_ptr<ISystemConfigWriter>;

    protected:
        ISystemConfigWriter();

    public:
        virtual ~ISystemConfigWriter();

        static auto GetInstance()->ISystemConfigWriter*;

        virtual auto Write()->bool = 0;
    };
}
