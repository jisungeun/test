#pragma once
#include <memory>
#include <QString>

#include <IVesselConfigWriter.h>
#include "HTX_System_Setup_VesselConfigWriterExport.h"

namespace HTXpress::AppPlugins::System::Setup::Plugins::VesselConfigWriter {
    class HTX_System_Setup_VesselConfigWriter_API Writer : public UseCase::IVesselConfigWriter {
    public:
        Writer();
        ~Writer() override;

        auto SetPath(const QString& sysConfigPath)->void;
        auto Write() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
