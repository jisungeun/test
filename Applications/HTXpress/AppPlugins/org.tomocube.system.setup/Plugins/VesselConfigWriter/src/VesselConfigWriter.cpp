#define LOGGER_TAG "[VesselConfigWriter]"
#include <TCLogger.h>
#include <VesselRepo.h>
#include <VesselWriter.h>
#include <QFileInfo>

#include "VesselConfigWriter.h"

namespace HTXpress::AppPlugins::System::Setup::Plugins::VesselConfigWriter {
    struct Writer::Impl {
        QString systemConfigPath;
    };

    Writer::Writer() : UseCase::IVesselConfigWriter(), d{std::make_unique<Impl>()} {
    }

    Writer::~Writer() {
    }

    auto Writer::SetPath(const QString& sysConfigPath) -> void {
        d->systemConfigPath = sysConfigPath;
    }

    auto Writer::Write() -> bool {
        const auto writer = AppComponents::VesselIO::VesselWriter();
        const auto repo = AppEntity::VesselRepo::GetInstance();
        return writer.Write(d->systemConfigPath, repo);
    }
}
