#pragma once
#include <memory>
#include <QString>

#include <IMediumConfigWriter.h>
#include "HTX_System_Setup_MediumConfigWriterExport.h"

namespace HTXpress::AppPlugins::System::Setup::Plugins::MediumConfigWriter {
    class HTX_System_Setup_MediumConfigWriter_API Writer : public UseCase::IMediumConfigWriter {
    public:
        Writer();
        ~Writer() override;

        auto SetPath(const QString& sysConfigPath)->void;
        auto Write() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
