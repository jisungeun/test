#define LOGGER_TAG "[MediumConfigWriter]"
#include <TCLogger.h>
#include <MediumDataRepo.h>
#include <MediumIOWriter.h>
#include <QFileInfo>

#include "MediumConfigWriter.h"

namespace HTXpress::AppPlugins::System::Setup::Plugins::MediumConfigWriter {
    struct Writer::Impl {
        QString systemConfigPath;
    };

    Writer::Writer() : UseCase::IMediumConfigWriter(), d{std::make_unique<Impl>()} {
    }

    Writer::~Writer() {
    }

    auto Writer::SetPath(const QString& sysConfigPath) -> void {
        d->systemConfigPath = sysConfigPath;
    }

    auto Writer::Write() -> bool {
        const auto writer = AppComponents::MediumIO::Writer();
        const auto mediumRepo = AppEntity::MediumDataRepo::GetInstance();

        return writer.Write(d->systemConfigPath, mediumRepo);
    }
}
