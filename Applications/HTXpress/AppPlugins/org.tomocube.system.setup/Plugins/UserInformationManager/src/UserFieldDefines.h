﻿#pragma once

namespace HTXpress::AppPlugins::System::Setup::Plugins::UserInformationManager {
    namespace Field {
        const char id[] = "id";
        const char name[] = "name";
        const char profile[] = "profile";
    }
}