#define LOGGER_TAG "[UserInformationManager]"
#include <QDate>

#include <UserDatabase.h>
#include <UserManager.h>
#include <TCLogger.h>
#include <OtpGenerator.h>

#include "UserInformationManager.h"
#include "UserInformationIO.h"

namespace HTXpress::AppPlugins::System::Setup::Plugins::UserInformationManager {
    using UserDatbase = AppComponents::UserDatabase::Component;
    using Manager = AppEntity::UserManager;

    struct UserInformationWriter::Impl {
        auto CheckServiceLogin(const QString& password) const->bool;
    };

    auto UserInformationWriter::Impl::CheckServiceLogin(const QString& password) const -> bool {
        const int otpLength = 8;
        const QString otpKey = "9z$C&F)J@NcRfUjXn2r5u7x!A%D*G-KaPdSgVkYp3s6v9y/B?E(H+MbQeThWmZq4";

        const auto year = QDate::currentDate().year();
        auto generator = TC::OtpGenerator(otpLength, otpKey);
        auto otp = generator.Generate(QString::number(year));

        return password.compare(otp, Qt::CaseInsensitive) == 0;
    }

    UserInformationWriter::UserInformationWriter() : IUserInformationManager(), d{std::make_unique<Impl>()} {
    }

    UserInformationWriter::~UserInformationWriter() {
    }

    auto UserInformationWriter::SetDatabase(const QString& path) -> bool {
        Q_UNUSED(path)
        return false;
    }

    auto UserInformationWriter::GetUser(const AppEntity::UserID& id) const -> AppEntity::User::Pointer {
        auto manager = Manager::GetInstance();
        return manager->GetUser(id);
    }

    auto UserInformationWriter::AddUser(const AppEntity::User::Pointer& user, const QString& password) -> bool {
        auto database = UserDatbase::GetInstance();
        return database->AddUser(user, password);
    }

    auto UserInformationWriter::DeleteUser(const AppEntity::UserID& id) -> bool {
        auto database = UserDatbase::GetInstance();
        return database->DeleteUser(id);
    }

    auto UserInformationWriter::EditUser(const AppEntity::User::Pointer& user) -> bool {
        auto database = UserDatbase::GetInstance();
        return database->UpdateUser(user);
    }

    auto UserInformationWriter::ChangePassword(const AppEntity::UserID& id, const QString& newPw) -> bool {
        auto database = UserDatbase::GetInstance();
        return database->ChangePassword(id, newPw);
    }

    auto UserInformationWriter::CheckPassword(const AppEntity::UserID& id, const QString& password) -> bool {
        auto manager = Manager::GetInstance();
        auto user = manager->GetUser(id);
        if(user == nullptr) return false;

        if(user->GetProfile() == +AppEntity::Profile::ServiceEngineer) return d->CheckServiceLogin(password);

        return UserDatbase::GetInstance()->IsValid(id, password);
    }

    auto UserInformationWriter::IsBuiltInAccount(const AppEntity::UserID& id) -> bool {
        const auto manager = Manager::GetInstance();
        return manager->IsBuiltInAccount(id);
    }
}
