#pragma once
#include <memory>
#include <QString>

#include <IUserInformationManager.h>
#include "HTX_System_Setup_UserInformationManagerExport.h"

namespace HTXpress::AppPlugins::System::Setup::Plugins::UserInformationManager {
    class HTX_System_Setup_UserInformationManager_API UserInformationWriter: public UseCase::IUserInformationManager {
    public:
        UserInformationWriter();
        ~UserInformationWriter() override;

        auto SetDatabase(const QString& path) -> bool;

        auto GetUser(const AppEntity::UserID& id) const -> AppEntity::User::Pointer override;
        auto AddUser(const AppEntity::User::Pointer& user, const QString& password) -> bool override;
        auto DeleteUser(const AppEntity::UserID& id) -> bool override;
        auto EditUser(const AppEntity::User::Pointer& user) -> bool override;
        auto ChangePassword(const AppEntity::UserID& id, const QString& newPw) -> bool override;
        auto CheckPassword(const AppEntity::UserID& id, const QString& password) -> bool override;
        auto IsBuiltInAccount(const AppEntity::UserID& id) -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
