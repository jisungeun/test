#pragma once
#include <memory>
#include <QString>

#include <IFLConfigWriter.h>
#include "HTX_System_Setup_FLConfigWriterExport.h"

namespace HTXpress::AppPlugins::System::Setup::Plugins::FLConfigWriter {
    class HTX_System_Setup_FLConfigWriter_API Writer : public UseCase::IFLConfigWriter {
    public:
        Writer();
        ~Writer() override;

        auto SetPath(const QString& sysConfigPath)->void;
        auto Write() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
