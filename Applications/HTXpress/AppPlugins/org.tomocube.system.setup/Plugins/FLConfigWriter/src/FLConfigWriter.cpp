#define LOGGER_TAG "[FLConfigWriter]"
#include <TCLogger.h>
#include <System.h>
#include <FLLightIOWriter.h>
#include <FLEmissionIOWriter.h>
#include <QFileInfo>

#include "FLConfigWriter.h"


namespace HTXpress::AppPlugins::System::Setup::Plugins::FLConfigWriter {
    struct Writer::Impl {
        QString systemConfigPath;
    };

    Writer::Writer() : UseCase::IFLConfigWriter(), d{std::make_unique<Impl>()} {
    }

    Writer::~Writer() {
    }

    auto Writer::SetPath(const QString& sysConfigPath) -> void {
        d->systemConfigPath = sysConfigPath;
    }

    auto Writer::Write() -> bool {
        const auto emissionWriter = AppComponents::FLEmissionIO::Writer();
        const auto excitationWriter = AppComponents::FLLightIO::Writer();

        const auto sysConfig = AppEntity::System::GetSystemConfig();

        return (emissionWriter.Write(d->systemConfigPath, sysConfig) && excitationWriter.Write(d->systemConfigPath, sysConfig));
    }
}
