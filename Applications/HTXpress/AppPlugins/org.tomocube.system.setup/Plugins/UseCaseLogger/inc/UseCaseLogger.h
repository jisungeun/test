#pragma once
#include <memory>

#include <IUseCaseLogger.h>
#include "HTX_System_Setup_UseCaseLoggerExport.h"

namespace HTXpress::AppPlugins::System::Setup::Plugins::UseCaseLogger {
    class HTX_System_Setup_UseCaseLogger_API Logger : public UseCase::IUseCaseLogger {
    public:
        Logger();
        ~Logger() override;

    protected:
        auto Log(const QString& useCase, const QString& message)->void override;
        auto Error(const QString& useCase, const QString& message)->void override;
    };
}