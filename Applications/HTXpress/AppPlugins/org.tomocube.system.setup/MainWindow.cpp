#include <QPushButton>
#include <QButtonGroup>

#include <MessageDialog.h>
#include <AppEvent.h>

#include "internal/FluorescencePage.h"
#include "internal/GeneralPage.h"
#include "internal/UserPage.h"
#include "internal/VesselPage.h"
#include "internal/MediumPage.h"

#include "MainWindowControl.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    using namespace TC::Framework;
    struct MainWindow::Impl {
        Ui::MainWindow ui;
        MainWindow* window{ nullptr };
        MainWindowControl control;

        QVariantMap appProperties;

        QVBoxLayout* menuLayout{ nullptr };
        QButtonGroup* menuButtons{ nullptr };

        Impl(MainWindow* p);
        auto InitGUI()->void;
        auto AddPage(const QString& title, ISetupPage* page)->void;
        auto CurrentPage() const->ISetupPage*;
        auto ShowEmptyPage() const->void;
    };

    MainWindow::Impl::Impl(MainWindow* p) : window{p} {
    }

    auto MainWindow::Impl::InitGUI() -> void {
        menuLayout = new QVBoxLayout(ui.menuGroup);
        ui.menuGroup->setLayout(menuLayout);
        menuLayout->addStretch();

        for(const auto& groupBox : window->findChildren<QGroupBox*>()) {
            groupBox->setObjectName("gb-setup-main");
        }

        for(const auto& button : window->findChildren<QPushButton*>()) {
            button->setObjectName("bt-setup-main");
        }

        ui.contentPanel->setObjectName("panel");
    }

    auto MainWindow::Impl::AddPage(const QString& title, ISetupPage* page) -> void {
        int32_t idx = ui.contentPanel->addWidget(page);
        auto* btn = new QPushButton(title);
        btn->setCheckable(true);
        btn->setObjectName("bt-setup-menu");

        menuButtons->addButton(btn, idx);
        menuButtons->setExclusive(true);

        menuLayout->insertWidget(menuLayout->count()-1, btn);
        connect(btn, &QAbstractButton::clicked, window, 
                [this, idx]() {
                    auto* page = CurrentPage();
                    if(page && page->IsModified()) {
                        TC::MessageDialog::information(window, "Preference", "Changes are not saved.");
                        const auto previousContentIndex = ui.contentPanel->currentIndex();
                        menuButtons->button(previousContentIndex)->setChecked(true);
                        return;
                    }

                    this->ui.contentPanel->setCurrentIndex(idx);
                    page = CurrentPage();
                    if(!page) return;

                    ui.restoreBtn->setVisible(page->IsRestorable());
                    ui.saveBtn->setVisible(page->IsRestorable());

                    page->Initialize();
                });
    }

    auto MainWindow::Impl::CurrentPage() const -> ISetupPage* {
        return qobject_cast<ISetupPage*>(ui.contentPanel->currentWidget());
    }

    auto MainWindow::Impl::ShowEmptyPage() const -> void {
        menuButtons->setExclusive(false);
        for(const auto& btn : menuButtons->buttons()) {
            btn->setChecked(false);
        }
        menuButtons->setExclusive(true);
        ui.contentPanel->setCurrentIndex(0);
    }

    MainWindow::MainWindow(QWidget* parent) : IMainWindowHTX("System Setup", "System Setup", parent), d{new Impl(this)} {
        d->ui.setupUi(this);
        InitUI();

        subscribeEvent("TabChange");
        connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(onHandleEvent(ctkEvent)));

        connect(d->ui.restoreBtn, SIGNAL(clicked()), this, SLOT(onRestore()));
        connect(d->ui.saveBtn, SIGNAL(clicked()), this, SLOT(onSave()));
        connect(d->ui.homeBtn, SIGNAL(clicked()), this, SLOT(onGotoHome()));
    }
    
    MainWindow::~MainWindow() {
    }

    auto MainWindow::TryActivate() -> bool {
        return true;
    }
    auto MainWindow::TryDeactivate() -> bool {
        return true;
    }
    auto MainWindow::IsActivate() -> bool {
        return true;
    }
    auto MainWindow::GetMetaInfo() -> QVariantMap {
        return d->appProperties;
    }

    auto MainWindow::Execute(const QVariantMap& params) -> bool {
        Q_UNUSED(params)
        d->ShowEmptyPage();
        return false;
    }

    void MainWindow::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
    }

    auto MainWindow::InitUI()->bool {
        d->InitGUI();

        d->menuButtons = new QButtonGroup(this);

        d->AddPage("General", new GeneralPage(this));
        d->AddPage("Fluorescence", new FluorescencePage(this));
        d->AddPage("Medium", new MediumPage(this));
        d->AddPage("Vessel", new VesselPage(d->control.GetVesselFolderPath(), this));
        d->AddPage("User", new UserPage(this));
       
        d->ui.contentPanel->setCurrentIndex(0);

        return true;
    }

    void MainWindow::onHandleEvent(const ctkEvent& ctkEvent) {
        Q_UNUSED(ctkEvent)   
    }

    void MainWindow::onRestore() {
        auto* page = d->CurrentPage();
        if(!page) return;
        page->Restore();
    }

    void MainWindow::onSave() {
        auto* page = d->CurrentPage();
        if(!page) return;
        page->Save();
    }

    void MainWindow::onGotoHome() {
        using StandardButton = TC::MessageDialog::StandardButton;

        auto* page = d->CurrentPage();
        if(page && page->IsModified()) {
            const auto selection = TC::MessageDialog::question(this, "Preference", 
                                                               tr("Do you want to ignore changes that are not saved?"),
                                                               StandardButton::Yes | StandardButton::No, StandardButton::No);
            if(selection == StandardButton::Yes) {
                onRestore();
            }
            else return;
        }

        AppEvent appEvt(AppTypeEnum::APP_WITHOUT_ARG);
        appEvt.setFullName("org.tomocube.general.start");
        appEvt.setAppName("Start");

        publishSignal(appEvt, "System Setup");
    }
}
