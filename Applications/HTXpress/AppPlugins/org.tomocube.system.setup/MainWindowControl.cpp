#include <QStandardPaths>

#include <SystemConfigWriter.h>
#include <FLConfigWriter.h>
#include <MediumConfigWriter.h>
#include <VesselConfigWriter.h>
#include <UserInformationManager.h>

#include "MainWindowControl.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    using SystemConfigWriter = Plugins::SystemConfigWriter::Writer;
    using FLConfigWriter = Plugins::FLConfigWriter::Writer;
    using MediumWriter = Plugins::MediumConfigWriter::Writer;
    using VesselWriter = Plugins::VesselConfigWriter::Writer;
    using UserWriter = Plugins::UserInformationManager::UserInformationWriter;

    struct MainWindowControl::Impl {
        std::shared_ptr<SystemConfigWriter> systemConfigWriter{ new SystemConfigWriter() };
        std::shared_ptr<FLConfigWriter> flConfigWriter{ new FLConfigWriter() };
        std::shared_ptr<MediumWriter> mediumWriter{new MediumWriter()};
        std::shared_ptr<VesselWriter> vesselWriter{new VesselWriter()};
        std::shared_ptr<UserWriter> userWriter{new UserWriter()};

        QString configPath{""};
        QString vesselFolderPath{""};
    };
    
    MainWindowControl::MainWindowControl() : d{new Impl } {
        const auto topPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
        const auto configFolder = QString("%1/config").arg(topPath);
        const auto vesselFolder = QString("%1/vessels").arg(configFolder);

        d->configPath = QString("%1/system.ini").arg(configFolder);
        d->vesselFolderPath = vesselFolder;

        d->systemConfigWriter->SetPath(d->configPath);
        d->flConfigWriter->SetPath(configFolder);
        d->mediumWriter->SetPath(configFolder);
        d->vesselWriter->SetPath(vesselFolder);
    }
    
    MainWindowControl::~MainWindowControl() {
    }

    auto MainWindowControl::GetVesselFolderPath() const -> const QString& {
        return d->vesselFolderPath;
    }
}
