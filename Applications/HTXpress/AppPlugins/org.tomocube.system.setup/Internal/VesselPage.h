#pragma once
#include <memory>

#include "ISetupPage.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class VesselPage : public ISetupPage {
        Q_OBJECT
    public:
        using Self = VesselPage;
        VesselPage(const QString& vesselFolderPath, QWidget* parent = nullptr);
        ~VesselPage() override;

        auto Initialize() -> bool override;
        auto IsModified() const -> bool override;
        auto IsRestorable() const -> bool override;
        auto Restore() -> bool override;
        auto Save() -> bool override;

    private slots:
        void onAddVessel();
        void onEditVessel();
        void onDeleteVessel();
        void onCopyVessel();
        void onUpdatePreview();
        void onImportVessel();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}