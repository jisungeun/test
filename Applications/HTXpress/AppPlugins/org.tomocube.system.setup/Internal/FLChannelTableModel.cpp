﻿#include <QFont>
#include <QBrush>
#include <QRegularExpression>

#include "FLChannelTableModel.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct FLChannelTableModel::Impl {
        QList<FLChannelData> channels;
        QList<bool> validations;
    };

    FLChannelTableModel::FLChannelTableModel(QObject* parent) : QAbstractTableModel(parent), d{std::make_unique<Impl>()} {
    }

    FLChannelTableModel::~FLChannelTableModel() {
    }

    auto FLChannelTableModel::rowCount(const QModelIndex& parent) const -> int {
        Q_UNUSED(parent)
        return d->channels.size();
    }

    auto FLChannelTableModel::columnCount(const QModelIndex& parent) const -> int {
        Q_UNUSED(parent)
        return _NumOfColumns;
    }

    auto FLChannelTableModel::data(const QModelIndex& index, int role) const -> QVariant {
        if (!index.isValid()) return QVariant();
        if (index.row() >= d->channels.size() || index.row() < 0) return QVariant();

        const auto row = index.row();
        const auto col = static_cast<Columns>(index.column());
        const auto& channel = d->channels.at(row);

        if (role == Qt::DisplayRole) {
            switch (col) {
                case Index: {
                    return QString::number(channel.index);
                }
                case Excitation: {
                    return QString("%1/%2").arg(channel.excitation.wavelength).arg(channel.excitation.bandwidth);
                }
                case Emission: {
                    if (channel.emission.wavelength != 0 || channel.emission.bandwidth != 0) return QString("%1/%2").arg(channel.emission.wavelength).arg(channel.emission.bandwidth);
                    return "void";
                }
                case Name: {
                    return channel.name;
                }
                case Color: {
                    return channel.color.name();
                }
                default: ;
            }
        }

        if (role == Qt::EditRole) {
            switch (col) {
                case Index: {
                    return QString::number(channel.index);
                }
                case Excitation: {
                    return QVariant::fromValue(QPair(channel.excitation.wavelength, channel.excitation.bandwidth));
                }
                case Emission: {
                    return QVariant::fromValue(QPair(channel.emission.wavelength, channel.emission.bandwidth));
                }
                case Name: {
                    return channel.name;
                }
                case Color: {
                    return channel.color.name();
                }
                default: ;
            }
        }

        if (role == Qt::ToolTipRole) {
            if (col == Color) {
                return channel.color.name();
            }
        }

        if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }

        if (role == Qt::ForegroundRole) {
            switch (col) {
                case Name:
                case Emission:
                case Excitation:
                    return QColor(Qt::red);
                default:
                    return QVariant();
            }
        }

        return QVariant();
    }

    auto FLChannelTableModel::headerData(int section, Qt::Orientation orientation, int role) const -> QVariant {
        if (role == Qt::DisplayRole) {
            if (orientation == Qt::Horizontal) {
                const auto column = static_cast<Columns>(section);
                switch (column) {
                    case Index: return "Index";
                    case Name: return "Channel Name";
                    case Excitation: return "Exitation";
                    case Emission: return "Emission";
                    case Color: return "Color";
                    default: ;
                }
            }

            if (orientation == Qt::Vertical) {
                return section + 1;
            }
        }

        if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }

        if (role == Qt::ForegroundRole && orientation == Qt::Vertical) {
            if(!d->validations.at(section))
                return QColor(Qt::red);

            return QVariant();
        }

        return QVariant();
    }

    auto FLChannelTableModel::setData(const QModelIndex& index, const QVariant& value, int role) -> bool {
        if(!index.isValid()) return false;

        const auto row = index.row();
        const auto col = static_cast<Columns>(index.column());
        if(role == Qt::EditRole) {
            auto channel = d->channels.value(row);

            switch (col) {
                case Index: {
                    channel.index = value.toInt();
                }
                break;
                case Name: {
                    channel.name = value.toString();
                }
                break;
                case Excitation: {
                    const auto ex = value.toString().split(QRegularExpression("/"));
                    channel.excitation.wavelength = ex.at(0).toInt();
                    channel.excitation.bandwidth = ex.at(1).toInt();
                }
                break;
                case Emission: {
                    const auto em = value.toString().split(QRegularExpression("/"));
                    channel.emission.wavelength = em.at(0).toInt();
                    channel.emission.bandwidth = em.at(1).toInt();
                }
                break;
                case Color: {
                    channel.color = QColor(value.toString());
                }
                break;
                default: break;
            }

            d->channels.replace(row, channel);
            emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});

            return true;
        }

        if(role == Qt::UserRole) {
            switch (col) {
                case Index: {
                    d->validations[row] = value.toBool();
                    break;
                }
                default:
                    break;
            }

            emit layoutChanged();
            return true;
        }

        return false;
    }

    auto FLChannelTableModel::flags(const QModelIndex& index) const -> Qt::ItemFlags {
        if (!index.isValid()) return Qt::NoItemFlags;
        return QAbstractTableModel::flags(index);
    }

    auto FLChannelTableModel::insertRows(int row, int count, const QModelIndex& parent) -> bool {
        Q_UNUSED(parent)

        beginInsertRows(QModelIndex(), row + count - 1, row);
        for (int r = count; r > 0; --r) {
            d->channels.insert(row, {});
            d->validations.insert(row, true);
        }
        endInsertRows();
        return true;
    }

    auto FLChannelTableModel::removeRows(int row, int count, const QModelIndex& parent) -> bool {
        Q_UNUSED(parent)

        beginRemoveRows(QModelIndex(), row, row + count - 1);
        for (int r = 0; r < count; ++r) {
            d->channels.removeAt(row);
            d->validations.removeAt(row);
        }
        endRemoveRows();
        return true;
    }

    auto FLChannelTableModel::GetChannels() const -> const QList<FLChannelData>& {
        return d->channels;
    }

    auto FLChannelTableModel::IsValid() const -> bool {
        if(d->validations.contains(false)) {
            return false;
        }
        return true;
    }

    auto FLChannelTableModel::GetInvalidRows() const -> QList<int32_t> {
        QList<int32_t> invalidRows;
        for(auto row = 0; row < d->validations.size(); ++row) {
            if(d->validations.at(row) == false) {
                invalidRows.push_back(row);
            }
        }
        return invalidRows;
    }
}
