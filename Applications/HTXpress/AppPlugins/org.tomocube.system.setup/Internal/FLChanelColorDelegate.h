﻿#pragma once
#include <memory>

#include <QStyledItemDelegate>

namespace HTXpress::AppPlugins::System::Setup::App {
    class FLChanelColorDelegate : public QStyledItemDelegate {
        Q_OBJECT
    public:
        explicit FLChanelColorDelegate(QObject* parent = nullptr);
        ~FLChanelColorDelegate() override;

        auto paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const -> void override;
  };
}
