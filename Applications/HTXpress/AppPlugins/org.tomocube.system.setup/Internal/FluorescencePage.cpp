#include <QButtonGroup>

#include <MessageDialog.h>

#include "FluorescencePage.h"
#include "ui_FluorescencePage.h"
#include "FluorescencePageControl.h"
#include "FLChannelTableModel.h"
#include "FLChannelEditDialog.h"
#include "TableViewEventFilter.h"
#include "FLChanelColorDelegate.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct FluorescencePage::Impl {
        explicit Impl(Self* self) : self(self){ ui.setupUi(self); }
        Self* self{};
        Ui::FluorescencePage ui;
        FluorescencePageControl control;
        FLChannelTableModel tableModel;
        TableViewEventFilter* tableEventFilter{nullptr};
        int32_t newIndex{0};

        struct LightSourceWidgets {
            QSpinBox* wavelength{nullptr};
            QSpinBox* bandwidth{nullptr};
            FilterState state{Use};
        };

        QMap<FilterType, QMap<FilterIndex, LightSourceWidgets>> filters;

        QButtonGroup* lightSelectGroup{nullptr};

        auto ApplyStyleSheet() -> void;

        auto UpdateGUI(const FluorescencePageControl::Config& config) -> void;
        auto UpdateConfig() const -> FluorescencePageControl::Config;
        auto UpdateValidation() -> void;
        auto CheckFLValidation() -> bool;

        auto SetVoidFilter(FilterType filterType, FilterIndex filterIdx) -> void;
        auto SetUnusedFilter(FilterType filterType, FilterIndex filterIdx) -> void;

        auto InitSubwidgets() -> void;
        auto SetValuesNonEditable() -> void;
        auto Connect() -> void;

        auto GetFLFilterList(FilterType type) const -> QStringList;
    };

    FluorescencePage::FluorescencePage(QWidget* parent) : ISetupPage(parent), d{std::make_unique<Impl>(this)} {
        d->InitSubwidgets();
        d->Connect();
        d->ApplyStyleSheet();
    }

    FluorescencePage::~FluorescencePage() {
    }

    auto FluorescencePage::Initialize() -> bool {
        d->control.Initalize();
        const auto config = d->control.GetConfig();
        d->UpdateGUI(config);

        if (d->control.GetConfig().flChannels.isEmpty() == false) 
            d->newIndex = d->control.GetConfig().flChannels.lastKey() + 1;

        d->ui.editExcitation->setChecked(false);
        d->ui.editEmission->setChecked(false);

        d->SetVoidFilter(Emission, CH1);
        d->SetValuesNonEditable();

        d->ui.editExcitation->setVisible(false);
        d->ui.editEmission->setVisible(false);

        d->ui.addChannel->setEnabled(IsAdmin());
        d->ui.editChannel->setEnabled(IsAdmin());
        d->ui.deleteChannel->setEnabled(IsAdmin());

        d->UpdateValidation();

        return true;
    }

    auto FluorescencePage::IsModified() const -> bool {
        if(!d->CheckFLValidation()) {
            return true;
        }

        const auto config = d->UpdateConfig();
        return d->control.IsModified(config);
    }

    auto FluorescencePage::IsRestorable() const -> bool {
        return true;
    }

    auto FluorescencePage::Restore() -> bool {
        const auto config = d->control.GetConfig();
        d->UpdateGUI(config);
        d->UpdateValidation();
        return true;
    }

    auto FluorescencePage::Save() -> bool {
        if(!d->CheckFLValidation()) {
            return false;
        }

        d->ui.editExcitation->setChecked(false);
        d->ui.editEmission->setChecked(false);

        const auto config = d->UpdateConfig();
        return d->control.SetConfig(config);
    }

    auto FluorescencePage::onAddChannel() -> void {
        const auto currentExcFilter = static_cast<FilterType>(d->lightSelectGroup->checkedId());
        const auto targetRow = d->tableModel.rowCount();

        const auto dlg = new FLChannelEditDialog(d->newIndex, this);
        dlg->SetTitle(tr("Add Fluorescene"));
        dlg->SetEmissionList(d->GetFLFilterList(Emission));
        dlg->SetExcitationList(d->GetFLFilterList(currentExcFilter));

        QStringList existNames;
        for(auto r = 0; r < d->tableModel.rowCount(); r++) {
            existNames.push_back(d->tableModel.index(r, FLChannelTableModel::Columns::Name).data().toString());
        }

        dlg->SetCurrentExistNames(existNames);

        if (dlg->exec()) {
            d->tableModel.insertRows(targetRow, 1, QModelIndex());
            QModelIndex index = d->tableModel.index(targetRow, FLChannelTableModel::Columns::Index);
            d->tableModel.setData(index, dlg->GetIndex(), Qt::EditRole);
            index = d->tableModel.index(targetRow, FLChannelTableModel::Columns::Name);
            d->tableModel.setData(index, dlg->GetName(), Qt::EditRole);
            index = d->tableModel.index(targetRow, FLChannelTableModel::Columns::Excitation);
            d->tableModel.setData(index, dlg->GetExcitation(), Qt::EditRole);
            index = d->tableModel.index(targetRow, FLChannelTableModel::Columns::Emission);
            d->tableModel.setData(index, dlg->GetEmission(), Qt::EditRole);
            index = d->tableModel.index(targetRow, FLChannelTableModel::Columns::Color);
            d->tableModel.setData(index, dlg->GetColor(), Qt::EditRole);

            d->newIndex++; // increase new index value
        }

        dlg->deleteLater();
    }

    auto FluorescencePage::onEditChannel() -> void {
        if (!d->ui.flChannelList->currentIndex().isValid()) return;

        const auto currentExcFilter = static_cast<FilterType>(d->lightSelectGroup->checkedId());
        const auto modelIndex = d->ui.flChannelList->currentIndex();

        const auto row = modelIndex.row();

        d->ui.flChannelList->selectRow(row);

        const auto chIdx = d->tableModel.index(row, FLChannelTableModel::Columns::Index).data().toInt();
        const auto name = d->tableModel.index(row, FLChannelTableModel::Columns::Name).data().toString();
        const auto exc = d->tableModel.index(row, FLChannelTableModel::Columns::Excitation).data().toString();
        const auto em = d->tableModel.index(row, FLChannelTableModel::Columns::Emission).data().toString();
        const auto color = QColor(d->tableModel.index(row, FLChannelTableModel::Columns::Color).data().toString());

        const auto dlg = new FLChannelEditDialog(chIdx, this);
        dlg->SetTitle(tr("Edit Fluorescence"));
        dlg->SetEmissionList(d->GetFLFilterList(Emission));
        dlg->SetExcitationList(d->GetFLFilterList(currentExcFilter));
        QStringList existNames;
        for(auto r = 0; r < d->tableModel.rowCount(); r++) {
            if(r != row) {
                existNames.push_back(d->tableModel.index(r, FLChannelTableModel::Columns::Name).data().toString());
            }
        }
        dlg->SetCurrentExistNames(existNames);
        dlg->SetCurrentData(name, exc, em, color);

        if (dlg->exec()) {
            QModelIndex index = d->tableModel.index(row, FLChannelTableModel::Columns::Index);
            d->tableModel.setData(index, dlg->GetIndex(), Qt::EditRole);
            index = d->tableModel.index(row, FLChannelTableModel::Columns::Name);
            d->tableModel.setData(index, dlg->GetName(), Qt::EditRole);
            index = d->tableModel.index(row, FLChannelTableModel::Columns::Excitation);
            d->tableModel.setData(index, dlg->GetExcitation(), Qt::EditRole);
            index = d->tableModel.index(row, FLChannelTableModel::Columns::Emission);
            d->tableModel.setData(index, dlg->GetEmission(), Qt::EditRole);
            index = d->tableModel.index(row, FLChannelTableModel::Columns::Color);
            d->tableModel.setData(index, dlg->GetColor(), Qt::EditRole);
        }

        dlg->deleteLater();

        d->UpdateValidation();
    }

    auto FluorescencePage::onDeleteChannel() -> void {
        const auto selectionModel = d->ui.flChannelList->selectionModel();
        const auto indexes = selectionModel->selectedRows(); // 전체 column이 선택된 row만 해당된다.
        QList<int32_t> rows;

        for (const auto& idx : indexes) {
            auto row = idx.row();
            rows.push_back(row);
        }

        std::sort(rows.begin(), rows.end(), [](const int32_t a, const int32_t b)-> bool {
            return a > b; // list element sorting
        });

        for (const auto row : rows) {
            d->tableModel.removeRows(row, 1, QModelIndex());
        }
    }

    void FluorescencePage::onEmissionEditButtonToggled(bool editable) {
        for (const auto& source : d->filters[Emission]) {
            source.wavelength->setReadOnly(!editable);
            source.bandwidth->setReadOnly(!editable);
        }

        if (!editable) {
            d->UpdateValidation();
        }
    }

    void FluorescencePage::onExcitationEditButtonToggled(bool editable) {
        for (const auto& source : d->filters[Internal]) {
            source.wavelength->setReadOnly(!editable);
            source.bandwidth->setReadOnly(!editable);
        }

        for (const auto& source : d->filters[External]) {
            source.wavelength->setReadOnly(!editable);
            source.bandwidth->setReadOnly(!editable);
        }

        d->ui.useInternalLight->setEnabled(editable);
        d->ui.useExternalLight->setEnabled(editable);

        if (!editable) {
            d->UpdateValidation();
        }
    }

    auto FluorescencePage::Impl::ApplyStyleSheet() -> void {
        // table
        ui.flChannelList->setStyleSheet(QString("QTableView{border-bottom: 0px;}"));
        ui.flChannelList->verticalHeader()->setStyleSheet(QString("QHeaderView{border-bottom: 0px;}"));

        // base widget
        ui.baseWidget->setObjectName("panel");

        // button
        for (const auto& button : self->findChildren<QPushButton*>()) {
            if(button->isCheckable()) {
                button->setObjectName("bt-setup-toggle");
            }
            else {
                button->setObjectName("bt-setup-light");
            }
        }

        for (const auto& label : self->findChildren<QLabel*>()) {
            if (label->objectName().contains("titleLabel")) {
                label->setObjectName("label-h2");
            }
            else if(label->objectName().contains("subTitle")) {
                label->setObjectName("label-h3");
            }
            else if(label->objectName().contains("subItem")) {
                label->setObjectName("label-h5");
            }
        }

        for (const auto& line : self->findChildren<QFrame*>()) {
            if (line->frameShape() == QFrame::VLine) {
                line->setObjectName("line-divider");
                line->setFixedWidth(1);
            }
            else if (line->frameShape() == QFrame::HLine) {
                line->setObjectName("line-divider");
                line->setFixedHeight(1);
            }
        }
    }

    auto FluorescencePage::Impl::UpdateGUI(const FluorescencePageControl::Config& config) -> void {
        if (tableModel.rowCount() != 0) tableModel.removeRows(0, tableModel.rowCount(), QModelIndex());

        config.excFilter == AppEntity::ExcFilter::Internal ? ui.useInternalLight->setChecked(true) : ui.useExternalLight->setChecked(true);

        QMapIterator itInternalLight(config.flInternalExcitations);
        while (itInternalLight.hasNext()) {
            const auto item = itInternalLight.next();
            const auto chIdx = item.key();
            const auto wl = item.value().GetWaveLength();
            const auto bw = item.value().GetBandwidth();
            filters[Internal].value(static_cast<FilterIndex>(chIdx)).wavelength->setValue(wl);
            filters[Internal].value(static_cast<FilterIndex>(chIdx)).bandwidth->setValue(bw);
        }

        QMapIterator itExternalLight(config.flExternalExcitations);
        while (itExternalLight.hasNext()) {
            const auto item = itExternalLight.next();
            const auto chIdx = item.key();
            const auto wl = item.value().GetWaveLength();
            const auto bw = item.value().GetBandwidth();
            filters[External].value(static_cast<FilterIndex>(chIdx)).wavelength->setValue(wl);
            filters[External].value(static_cast<FilterIndex>(chIdx)).bandwidth->setValue(bw);
        }

        QMapIterator itEmission(config.flEmissions);
        while (itEmission.hasNext()) {
            const auto item = itEmission.next();
            const auto chIdx = item.key();
            const auto wl = item.value().GetWaveLength();
            const auto bw = item.value().GetBandwidth();
            filters[Emission].value(static_cast<FilterIndex>(chIdx)).wavelength->setValue(wl);
            filters[Emission].value(static_cast<FilterIndex>(chIdx)).bandwidth->setValue(bw);
        }

        QMapIterator itChannel(config.flChannels);
        while (itChannel.hasNext()) {
            const auto idx = itChannel.next().key();
            const auto name = itChannel.value().GetName();
            const auto exWL = itChannel.value().GetExcitation();
            const auto exBW = itChannel.value().GetExcitationBandwidth();
            const auto emWL = itChannel.value().GetEmission();
            const auto emBW = itChannel.value().GetEmissionBandwidth();
            const auto color = itChannel.value().GetColor();

            tableModel.insertRows(tableModel.rowCount(), 1, QModelIndex());
            QModelIndex index = tableModel.index(tableModel.rowCount() - 1, FLChannelTableModel::Columns::Index, QModelIndex());
            tableModel.setData(index, idx, Qt::EditRole);
            index = tableModel.index(tableModel.rowCount() - 1, FLChannelTableModel::Columns::Name, QModelIndex());
            tableModel.setData(index, name, Qt::EditRole);
            index = tableModel.index(tableModel.rowCount() - 1, FLChannelTableModel::Columns::Excitation, QModelIndex());
            tableModel.setData(index, QString("%1/%2").arg(exWL).arg(exBW), Qt::EditRole);
            index = tableModel.index(tableModel.rowCount() - 1, FLChannelTableModel::Columns::Emission, QModelIndex());
            tableModel.setData(index, QString("%1/%2").arg(emWL).arg(emBW), Qt::EditRole);
            index = tableModel.index(tableModel.rowCount() - 1, FLChannelTableModel::Columns::Color, QModelIndex());
            tableModel.setData(index, color, Qt::EditRole);
        }
    }

    auto FluorescencePage::Impl::UpdateConfig() const -> FluorescencePageControl::Config {
        FluorescencePageControl::Config config;

        config.excFilter = AppEntity::ExcFilter::_from_integral(lightSelectGroup->checkedId());
        QMapIterator it(filters);
        while (it.hasNext()) {
            auto type = it.next().key();
            switch (type) {
                case Emission: {
                    for (int i = 0; i < it.value().size(); ++i) {
                        config.flEmissions[i].SetWaveLength(it.value().value(static_cast<FilterIndex>(i)).wavelength->value());
                        config.flEmissions[i].SetBandwidth(it.value().value(static_cast<FilterIndex>(i)).bandwidth->value());
                    }
                    break;
                }
                case Internal: {
                    for (int i = 0; i < it.value().size(); ++i) {
                        config.flInternalExcitations[i].SetWaveLength(it.value().value(static_cast<FilterIndex>(i)).wavelength->value());
                        config.flInternalExcitations[i].SetBandwidth(it.value().value(static_cast<FilterIndex>(i)).bandwidth->value());
                    }
                    break;
                }
                case External: {
                    for (int i = 0; i < it.value().size(); ++i) {
                        config.flExternalExcitations[i].SetWaveLength(it.value().value(static_cast<FilterIndex>(i)).wavelength->value());
                        config.flExternalExcitations[i].SetBandwidth(it.value().value(static_cast<FilterIndex>(i)).bandwidth->value());
                    }
                    break;
                }
            }
        }

        // fl channels
        for (auto [index,name,ex,em,color] : tableModel.GetChannels()) {
            AppEntity::FLChannel flChannel;
            flChannel.SetName(name);
            flChannel.SetExcitation(ex.wavelength);
            flChannel.SetExcitationBandwidth(ex.bandwidth);
            flChannel.SetEmission(em.wavelength);
            flChannel.SetEmissionBandwidth(em.bandwidth);
            flChannel.SetColor(color);
            config.flChannels[index] = flChannel;
        }

        return config;
    }

    auto FluorescencePage::Impl::UpdateValidation() -> void {
        const auto config = UpdateConfig();
        for(int row = 0; row < tableModel.rowCount(); ++row) {
            const auto modelIndex = tableModel.index(row, FLChannelTableModel::Columns::Index, QModelIndex());
            const auto indexNumber = modelIndex.data().toInt();
            tableModel.setData(modelIndex, control.IsValid(config, indexNumber), Qt::UserRole);
        }
    }

    auto FluorescencePage::Impl::CheckFLValidation() -> bool {
        if(!tableModel.IsValid()) {
            QString txt{"Invalid row"};
            const auto& rowList = tableModel.GetInvalidRows();
            if(rowList.size() > 1) {
                txt.append("s");
            } 

            QString rows{};
            for(auto r = 0; r < rowList.size(); ++r) {
                rows.append(QString::number(rowList.at(r)+1));
                if(r != rowList.size()-1) {
                    rows.append(", ");
                }
            }
            TC::MessageDialog::warning(self, tr("Invalid FL Channel"), tr("Please check excitation and emission of FL Channel below.\n"
                                                                          "%1: %2").arg(txt).arg(rows));
            return false;
        }

        return true;
    }

    auto FluorescencePage::Impl::SetVoidFilter(FilterType filterType, FilterIndex filterIdx) -> void {
        auto& filter = filters[filterType];
        filter[filterIdx].state = Void;
        filter[filterIdx].wavelength->setValue(0);
        filter[filterIdx].wavelength->setDisabled(true);
        filter[filterIdx].wavelength->setSpecialValueText("Void");
        filter[filterIdx].wavelength->setButtonSymbols(QAbstractSpinBox::NoButtons);
        filter[filterIdx].wavelength->setAlignment(Qt::AlignCenter);
        filter[filterIdx].wavelength->setSuffix("");

        filter[filterIdx].bandwidth->setValue(0);
        filter[filterIdx].bandwidth->setDisabled(true);
        filter[filterIdx].bandwidth->setSpecialValueText("Void");
        filter[filterIdx].bandwidth->setButtonSymbols(QAbstractSpinBox::NoButtons);
        filter[filterIdx].bandwidth->setAlignment(Qt::AlignCenter);
        filter[filterIdx].bandwidth->setSuffix("");
    }

    auto FluorescencePage::Impl::SetUnusedFilter(FilterType filterType, FilterIndex filterIdx) -> void {
        auto& filter = filters[filterType];
        filter[filterIdx].state = NoUse;
        filter[filterIdx].wavelength->setValue(0);
        filter[filterIdx].wavelength->setDisabled(true);
        filter[filterIdx].wavelength->setSpecialValueText("Unused");
        filter[filterIdx].wavelength->setAlignment(Qt::AlignCenter);
        filter[filterIdx].wavelength->setButtonSymbols(QAbstractSpinBox::NoButtons);
        filter[filterIdx].wavelength->setSuffix("");

        filter[filterIdx].bandwidth->setValue(0);
        filter[filterIdx].bandwidth->setDisabled(true);
        filter[filterIdx].bandwidth->setSpecialValueText("Unused");
        filter[filterIdx].bandwidth->setButtonSymbols(QAbstractSpinBox::NoButtons);
        filter[filterIdx].bandwidth->setAlignment(Qt::AlignCenter);
        filter[filterIdx].bandwidth->setSuffix("");
    }

    auto FluorescencePage::Impl::InitSubwidgets() -> void {
        ui.flChannelList->setModel(&tableModel);
        // selection behavior 변경 시 delete 함수 로직 수정 필요
        ui.flChannelList->setSelectionBehavior(QAbstractItemView::SelectRows);
        ui.flChannelList->setEditTriggers(QAbstractItemView::NoEditTriggers);
        ui.flChannelList->setSelectionMode(QAbstractItemView::ExtendedSelection);
        ui.flChannelList->setColumnHidden(FLChannelTableModel::Columns::Index, true);
        ui.flChannelList->setItemDelegateForColumn(FLChannelTableModel::Columns::Color, new FLChanelColorDelegate());
        ui.flChannelList->horizontalHeader()->setSectionResizeMode(FLChannelTableModel::Columns::Color, QHeaderView::ResizeToContents);
        tableEventFilter = new TableViewEventFilter(ui.flChannelList);
        ui.flChannelList->viewport()->installEventFilter(tableEventFilter);

        QMap<FilterIndex, LightSourceWidgets> internalLights;
        internalLights[CH1] = {ui.internalLightCh1Wavelen, ui.internalLightCh1Bandwidth};
        internalLights[CH2] = {ui.internalLightCh2Wavelen, ui.internalLightCh2Bandwidth};
        internalLights[CH3] = {ui.internalLightCh3Wavelen, ui.internalLightCh3Bandwidth};
        internalLights[CH4] = {ui.internalLightCh4Wavelen, ui.internalLightCh4Bandwidth};
        internalLights[CH5] = {ui.internalLightCh5Wavelen, ui.internalLightCh5Bandwidth};

        QMap<FilterIndex, LightSourceWidgets> externalLights;
        externalLights[CH1] = {ui.externalLightCh1Wavelen, ui.externalLightCh1Bandwidth};
        externalLights[CH2] = {ui.externalLightCh2Wavelen, ui.externalLightCh2Bandwidth};
        externalLights[CH3] = {ui.externalLightCh3Wavelen, ui.externalLightCh3Bandwidth};

        QMap<FilterIndex, LightSourceWidgets> emissions;
        emissions[CH1] = {ui.emissionCh1Wavelen, ui.emissionCh1Bandwidth};
        emissions[CH2] = {ui.emissionCh2Wavelen, ui.emissionCh2Bandwidth};
        emissions[CH3] = {ui.emissionCh3Wavelen, ui.emissionCh3Bandwidth};
        emissions[CH4] = {ui.emissionCh4Wavelen, ui.emissionCh4Bandwidth};
        emissions[CH5] = {ui.emissionCh5Wavelen, ui.emissionCh5Bandwidth};
        emissions[CH6] = {ui.emissionCh6Wavelen, ui.emissionCh6Bandwidth};

        filters[Internal] = internalLights;
        filters[External] = externalLights;
        filters[Emission] = emissions;

        // select radio button
        lightSelectGroup = new QButtonGroup(self);
        lightSelectGroup->addButton(ui.useInternalLight, Internal);
        lightSelectGroup->addButton(ui.useExternalLight, External);

        ui.editExcitation->setCheckable(true);
        ui.editEmission->setCheckable(true);
        ui.useInternalLight->setDisabled(true);
        ui.useExternalLight->setDisabled(true);
    }

    auto FluorescencePage::Impl::SetValuesNonEditable() -> void {
        for (auto& sources : filters) {
            for (const auto& source : sources) {
                source.wavelength->setReadOnly(true);
                source.bandwidth->setReadOnly(true);
            }
        }
    }

    auto FluorescencePage::Impl::Connect() -> void {
        connect(ui.editEmission, &QPushButton::toggled, self, &Self::onEmissionEditButtonToggled);
        connect(ui.editExcitation, &QPushButton::toggled, self, &Self::onExcitationEditButtonToggled);

        connect(ui.addChannel, &QPushButton::clicked, self, &Self::onAddChannel);
        connect(ui.editChannel, &QPushButton::clicked, self, &Self::onEditChannel);
        connect(ui.deleteChannel, &QPushButton::clicked, self, &Self::onDeleteChannel);
    }

    auto FluorescencePage::Impl::GetFLFilterList(FilterType type) const -> QStringList {
        QStringList strings;
        for (const auto& source : filters[type]) {
            if (source.state != NoUse) {
                QString str = "";
                if (source.state == Void) {
                    str = "void";
                }
                else {
                    str = QString("%1/%2").arg(source.wavelength->value()).arg(source.bandwidth->value());
                }
                strings << str;
            }
        }
        return strings;
    }
}
