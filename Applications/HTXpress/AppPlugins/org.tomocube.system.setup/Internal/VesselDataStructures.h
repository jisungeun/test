﻿#pragma once

#include <memory>
#include <QList>
#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::System::Setup::App {
    enum class SpacingMode {AutoSpacing, ManualSpacing};

    namespace VesselPageUtil {
        inline auto doubleComp = [](double left, double right)-> bool {
            return std::fabs(left - right) <= std::numeric_limits<double>::epsilon();
        };

        inline auto ConvertRowIndexToChar(int32_t row) -> QString {
            char rowString[50];
            int32_t i = 0;
            int32_t rowPlusOne = row + 1;
            while (rowPlusOne > 0) {
                const int32_t remainder = rowPlusOne % 26;
                if (remainder == 0) {
                    rowString[i++] = 'Z';
                    rowPlusOne = (rowPlusOne / 26) - 1;
                }
                else {
                    rowString[i++] = (remainder - 1) + 'A';
                    rowPlusOne /= 26;
                }
            }

            rowString[i] = '\0';

            std::reverse(rowString, rowString + strlen(rowString));

            return QString::fromLatin1(rowString);
        }

        inline auto SetDefaultWellName(int32_t row, int32_t column) -> QString {
            return ConvertRowIndexToChar(row) + QString::number(column + 1);
        }
    }

    class VesselDataStructure {
    public:
        struct WellProp {
            AppEntity::WellShape::_enumerated shape{AppEntity::WellShape::Circle};
            int32_t rows{1};
            int32_t cols{1};
            double w{0.0};
            double h{0.0};
            double spacingH{0.0};
            double spacingV{0.0};

            auto operator=(const WellProp& other) -> WellProp& {
                shape = other.shape;
                rows = other.rows;
                cols = other.cols;
                w = other.w;
                h = other.h;
                spacingH = other.spacingH;
                spacingV = other.spacingV;
                return (*this);
            }

            auto operator==(const WellProp& other) const -> bool {
                if (shape != other.shape) return false;
                if (rows != other.rows) return false;
                if (cols != other.cols) return false;
                if (!VesselPageUtil::doubleComp(w, other.w)) return false;
                if (!VesselPageUtil::doubleComp(h, other.h)) return false;
                if (!VesselPageUtil::doubleComp(spacingH, other.spacingH)) return false;
                if (!VesselPageUtil::doubleComp(spacingV, other.spacingV)) return false;
                return true;
            }

            auto operator!=(const WellProp& other) const -> bool {
                return !(*this == other);
            }
        };

        struct Well {
            int32_t rowIdx{0};
            int32_t colIdx{0};
            QString label{""};
            double x{0.0};
            double y{0.0};

            auto operator=(const Well& other) -> Well& {
                rowIdx = other.rowIdx;
                colIdx = other.colIdx;
                label = other.label;
                x = other.x;
                y = other.y;
                return *this;
            }

            auto operator==(const Well& other) const -> bool {
                if (rowIdx != other.rowIdx) return false;
                if (colIdx != other.colIdx) return false;
                if (label != other.label) return false;
                if (!VesselPageUtil::doubleComp(x, other.x)) return false;
                if (!VesselPageUtil::doubleComp(y, other.y)) return false;
                return true;
            }

            auto operator!=(const Well& other) const -> bool {
                return !(*this == other);

            }
        };

        struct ImagingArea {
            AppEntity::ImagingAreaShape::_enumerated shape{AppEntity::ImagingAreaShape::Circle};
            double x{0.0};
            double y{0.0};
            double w{0.0};
            double h{0.0};

            auto operator=(const ImagingArea& other) -> ImagingArea& {
                shape = other.shape;
                x = other.x;
                y = other.y;
                w = other.w;
                h = other.h;
                return *this;
            }

            auto operator==(const ImagingArea& other) const -> bool {
                if (shape != other.shape) return false;
                if (!VesselPageUtil::doubleComp(x, other.x)) return false;
                if (!VesselPageUtil::doubleComp(y, other.y)) return false;
                if (!VesselPageUtil::doubleComp(w, other.w)) return false;
                if (!VesselPageUtil::doubleComp(h, other.h)) return false;
                return true;
            }

            auto operator!=(const ImagingArea& other) const -> bool {
                return !(*this == other);

            }
        };

        struct Vessel {
            QString model{""};
            QString name{""};

            double na{0.0};
            int32_t afOffset{}; // um

            double w{0.0};
            double h{0.0};

            bool useMultiDishHolder{ false };

            ImagingArea imgArea;

            WellProp wellProp;
            QList<Well> wells;

            auto operator=(const Vessel& other) -> Vessel& {
                model = other.model;
                name = other.name;
                w = other.w;
                h = other.h;
                useMultiDishHolder = other.useMultiDishHolder;
                imgArea = other.imgArea;
                wellProp = other.wellProp;
                wells = other.wells;
                na = other.na;
                afOffset = other.afOffset;

                return *this;
            }

            auto operator==(const Vessel& other) const -> bool {
                if (model != other.model) return false;
                if (name != other.name) return false;
                if (!VesselPageUtil::doubleComp(w, other.w)) return false;
                if (!VesselPageUtil::doubleComp(h, other.h)) return false;
                if (useMultiDishHolder != other.useMultiDishHolder) return false;
                if (imgArea != other.imgArea) return false;
                if (wellProp != other.wellProp) return false;
                if (wells != other.wells) return false;
                if (!VesselPageUtil::doubleComp(na, other.na)) return false;
                if (afOffset != other.afOffset) return false;

                return true;
            }

            auto operator!=(const Vessel& other) const -> bool {
                return !(*this == other);
            }
        };

        using Vessels = QList<Vessel>;
    };

}
