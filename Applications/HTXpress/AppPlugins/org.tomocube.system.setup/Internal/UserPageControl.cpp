#include <SessionManager.h>
#include <UserManager.h>
#include <User.h>
#include <UserInformationController.h>

#include "UserPageControl.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct UserPageControl::Impl {
    };

    UserPageControl::UserPageControl() : d{new Impl} {
    }

    UserPageControl::~UserPageControl() {
    }

    auto UserPageControl::AddUser(const AppEntity::User::Pointer& newUser, const QString& password) -> bool {
        auto controller = Interactor::UserInformationController();
        return controller.Add(newUser, password);
    }

    auto UserPageControl::EditUser(const AppEntity::User::Pointer& editedUser) -> bool {
        auto controller = Interactor::UserInformationController();
        return controller.Modify(editedUser);
    }

    auto UserPageControl::DeleteUser(const AppEntity::UserID& id) -> bool {
        auto controller = Interactor::UserInformationController();
        return controller.Delete(id);
    }

    auto UserPageControl::ChangePassword(const AppEntity::UserID& id, const QString& pw) -> bool {
        auto controller = Interactor::UserInformationController();
        return controller.ChangePassword(id, pw);
    }

    auto UserPageControl::GetUsers() const -> Users {
        const auto userManager = AppEntity::UserManager::GetInstance();
        return userManager->GetUsers();
    }

    auto UserPageControl::GetUserIDs() const -> QStringList {
        QStringList idList{};
        const auto userManager = AppEntity::UserManager::GetInstance();

        for(const auto& id : userManager->GetUsers()) {
            idList << id->GetUserID();
        }

        return idList;
    }

    auto UserPageControl::IsValidAdmin(const QString& password) -> bool {
        auto session = AppEntity::SessionManager::GetInstance();
        auto id = session->GetID();

        auto controller = Interactor::UserInformationController();
        return controller.IsValidAdmin(id, password);
    }

    auto UserPageControl::IsBuiltInAccount(const QString& id) const -> bool {
        auto controller = Interactor::UserInformationController();
        return controller.IsBuiltInAccount(id);
    }
}
