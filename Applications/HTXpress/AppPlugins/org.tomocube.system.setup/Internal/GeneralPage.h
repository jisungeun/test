#pragma once
#include <memory>

#include "ISetupPage.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class GeneralPage : public ISetupPage {
        Q_OBJECT
    public:
        using Self = GeneralPage;

        GeneralPage(QWidget* parent = nullptr);
        ~GeneralPage() override;

        auto Initialize() -> bool override;
        auto IsModified() const -> bool override;
        auto IsRestorable() const -> bool override;
        auto Restore() -> bool override;
        auto Save() -> bool override;

    protected slots:
        void onChangeDataFolder();
        void onShowLicenseDialog();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}