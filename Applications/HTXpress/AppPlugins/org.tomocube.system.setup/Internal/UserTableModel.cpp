﻿#include "UserTableModel.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct UserTableModel::Impl {
        Users users;
    };

    UserTableModel::UserTableModel(QObject* parent) : QAbstractTableModel(parent), d{std::make_unique<Impl>()} {
    }

    UserTableModel::~UserTableModel() {
    }

    auto UserTableModel::columnCount(const QModelIndex& parent) const -> int {
        Q_UNUSED(parent)
        return _NumOfCols;
    }

    auto UserTableModel::rowCount(const QModelIndex& parent) const -> int {
        Q_UNUSED(parent)
        return d->users.size();
    }

    auto UserTableModel::headerData(int section, Qt::Orientation orientation, int role) const -> QVariant {
        if (role == Qt::DisplayRole) {
            if (orientation == Qt::Vertical) {
                return section + 1;
            }

            if (orientation == Qt::Horizontal) {
                const auto column = static_cast<Columns>(section);
                switch (column) {
                    case ID: return "ID";
                    case Name: return "Name";
                    case Type: return "Type";
                    default: ;
                }
            }
        }

        if (role == Qt::TextAlignmentRole) {
            if (orientation == Qt::Vertical) {
                return Qt::AlignCenter;
            }
        }

        return QVariant();
    }

    auto UserTableModel::data(const QModelIndex& index, int role) const -> QVariant {
        if (!index.isValid()) return QVariant();
        if (index.row() >= d->users.size() || index.row() < 0) return QVariant();

        const auto row = index.row();
        const auto col = static_cast<Columns>(index.column());
        const auto& user = d->users.at(row);

        if (role == Qt::DisplayRole) {
            switch (col) {
                case Name: {
                    return user->GetName();
                }
                case ID: {
                    return user->GetUserID();
                }
                case Type: {
                    return user->GetProfile()._to_string();
                }
                default: ;
            }
        }

        if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }

        return QVariant();
    }

    auto UserTableModel::setData(const QModelIndex& index, const QVariant& value, int role) -> bool {
        if (index.isValid() && role == Qt::EditRole) {
            const auto row = index.row();
            const auto col = static_cast<Columns>(index.column());
            auto user = d->users.value(row);

            switch (col) {
                case Name: user->SetName(value.toString());
                    break;
                case ID: user->SetUserID(value.toString());
                    break;
                case Type: user->SetProfile(AppEntity::Profile::_from_string(value.toString().toLatin1()));
                    break;
                default: break;
            }

            d->users.replace(row, user);
            emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});

            return true;
        }

        return false;
    }

    auto UserTableModel::insertRows(int row, int count, const QModelIndex& parent) -> bool {
        Q_UNUSED(parent)
        beginInsertRows(QModelIndex(), row + count - 1, row);
        for (int r = count; r > 0; --r) {
            d->users.insert(row, std::make_shared<AppEntity::User>());
        }
        endInsertRows();
        return true;
    }

    auto UserTableModel::removeRows(int row, int count, const QModelIndex& parent) -> bool {
        Q_UNUSED(parent)
        beginRemoveRows(QModelIndex(), row, row + count - 1);
        for (int r = 0; r < count; ++r) {
            d->users.removeAt(row);
        }
        endRemoveRows();
        return true;
    }

    auto UserTableModel::flags(const QModelIndex& index) const -> Qt::ItemFlags {
        if (!index.isValid()) return Qt::NoItemFlags;
        return QAbstractTableModel::flags(index);
    }


    auto UserTableModel::GetUsers() const -> const Users& {
        return d->users;
    }
}
