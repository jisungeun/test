#pragma once
#include <memory>

#include "ISetupPage.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class FluorescencePage : public ISetupPage {
        Q_OBJECT
    public:
        enum FilterIndex {CH1 = 0, CH2, CH3, CH4, CH5, CH6};
        enum FilterType {Internal, External, Emission};
        enum FilterState {Use, NoUse, Void};

        using Self = FluorescencePage;

        FluorescencePage(QWidget* parent = nullptr);
        ~FluorescencePage() override;

        auto Initialize() -> bool override;
        auto IsModified() const -> bool override;
        auto IsRestorable() const -> bool override;
        auto Restore() -> bool override;
        auto Save() -> bool override;

    private slots:
        void onAddChannel();
        void onEditChannel();
        void onDeleteChannel();
        void onEmissionEditButtonToggled(bool editable);
        void onExcitationEditButtonToggled(bool editable);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
