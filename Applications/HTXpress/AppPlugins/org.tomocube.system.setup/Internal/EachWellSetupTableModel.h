﻿#pragma once

#include <memory>
#include <QAbstractTableModel>

namespace HTXpress::AppPlugins::System::Setup::App {
    class EachWellSetupTableModel : public QAbstractTableModel {
        Q_OBJECT
    public:
        enum Columns { RowIndex, ColumnIndex, Name, X, Y, _NumOfColumns };

        struct WellTableData {
            int32_t row{-1};
            int32_t col{-1};
            QString name{""};
            double x{0.0};
            double y{0.0};

            auto operator==(const WellTableData& other) const -> bool {
                if (row != other.row) return false;
                if (col != other.col) return false;
                if (name != other.name) return false;
                if (fabs(x - other.x) >= std::numeric_limits<double>::epsilon()) return false;
                if (fabs(y - other.y) >= std::numeric_limits<double>::epsilon()) return false;

                return true;
            }
        };

        using Self = EachWellSetupTableModel;

        explicit EachWellSetupTableModel(QObject* parent = nullptr);
        ~EachWellSetupTableModel() override;

        auto rowCount(const QModelIndex& parent = QModelIndex()) const -> int override;
        auto columnCount(const QModelIndex& parent = QModelIndex()) const -> int override;
        auto data(const QModelIndex& index, int role) const -> QVariant override;
        auto headerData(int section, Qt::Orientation orientation, int role) const -> QVariant override;
        auto setData(const QModelIndex& index, const QVariant& value, int role) -> bool override;
        auto flags(const QModelIndex& index) const -> Qt::ItemFlags override;
        auto insertRows(int row, int count, const QModelIndex& parent) -> bool override;
        auto removeRows(int row, int count, const QModelIndex& parent) -> bool override;
        auto GetWells() const -> const QList<WellTableData>&;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
