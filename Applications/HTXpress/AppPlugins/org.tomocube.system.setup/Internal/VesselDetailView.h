﻿#pragma once

#include <memory>

#include <QWidget>

#include "VesselDataStructures.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class VesselDetailView : public QWidget {
    public:
        using Self = VesselDetailView;

        VesselDetailView(QWidget* parent);
        ~VesselDetailView() override;

        auto SetVessel(const VesselDataStructure::Vessel& vessel) -> void;
        auto GetVessel() const -> VesselDataStructure::Vessel&;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
