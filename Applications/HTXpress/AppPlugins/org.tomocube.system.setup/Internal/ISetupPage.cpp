#include <AppEntityDefines.h>
#include <SessionManager.h>

#include "ISetupPage.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    ISetupPage::ISetupPage(QWidget* parent) : QWidget(parent) {
    }

    ISetupPage::~ISetupPage() {
    }

    auto ISetupPage::GetUserLevel() const -> UserLevel {
        auto session = AppEntity::SessionManager::GetInstance();
        auto profile = session->GetProfile();

        switch(profile) {
        case AppEntity::Profile::Operator:
            return UserLevel::User;
        case AppEntity::Profile::Administrator:
            return UserLevel::Admin;
        case AppEntity::Profile::ServiceEngineer:
            return UserLevel::ServiceEngineer;
        }
        return UserLevel::User;
    }

    auto ISetupPage::HasAuthority(const UserLevel& level) const -> bool {
        return GetUserLevel() >= level;
    }

    auto ISetupPage::IsServiceEngineer() const -> bool {
        return HasAuthority(UserLevel::ServiceEngineer);
    }

    auto ISetupPage::IsAdmin() const -> bool {
        return HasAuthority(UserLevel::Admin);
    }
}
