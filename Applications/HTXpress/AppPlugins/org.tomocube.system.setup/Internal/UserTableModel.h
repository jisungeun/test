﻿#pragma once
#include <memory>
#include <QAbstractTableModel>
#include <User.h>

namespace HTXpress::AppPlugins::System::Setup::App {
    class UserTableModel : public QAbstractTableModel {
        Q_OBJECT
    public:
        using Users = QList<AppEntity::User::Pointer>;

    public:
        enum Columns { ID = 0, Name, Type, _NumOfCols };

        explicit UserTableModel(QObject* parent = nullptr);
        ~UserTableModel() override;

        auto columnCount(const QModelIndex& parent = QModelIndex()) const -> int override;
        auto rowCount(const QModelIndex& parent = QModelIndex()) const -> int override;
        auto headerData(int section, Qt::Orientation orientation, int role) const -> QVariant override;
        auto data(const QModelIndex& index, int role) const -> QVariant override;
        auto setData(const QModelIndex& index, const QVariant& value, int role) -> bool override;

        auto insertRows(int row, int count, const QModelIndex& parent) -> bool override;
        auto removeRows(int row, int count, const QModelIndex& parent) -> bool override;
        auto flags(const QModelIndex& index) const -> Qt::ItemFlags override;

        auto GetUsers() const -> const Users&;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
