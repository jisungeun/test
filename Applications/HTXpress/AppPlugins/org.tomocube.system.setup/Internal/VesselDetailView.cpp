﻿#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QLayout>

#include "VesselDetailView.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct VesselDetailView::Impl {
        QTreeWidget* tree{nullptr};
        VesselDataStructure::Vessel vessel;
    };

    VesselDetailView::VesselDetailView(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->tree = new QTreeWidget(this);
        auto mainLayout = new QGridLayout;
        mainLayout->addWidget(d->tree);
        mainLayout->setSpacing(0);
        mainLayout->setMargin(0);
        mainLayout->setContentsMargins(0,0,0,0);
        setLayout(mainLayout);
    }

    VesselDetailView::~VesselDetailView() {
    }

    auto VesselDetailView::SetVessel(const VesselDataStructure::Vessel& vessel) -> void {
        if(d->vessel != vessel) {
            // modify tree item
        }
        d->vessel = vessel;
    }

    auto VesselDetailView::GetVessel() const -> VesselDataStructure::Vessel& {
        return d->vessel;
    }
}
