#pragma once
#include <memory>
#include <QMap>

#include <AppEntityDefines.h>
#include <FLChannel.h>
#include <FLFilter.h>

namespace HTXpress::AppPlugins::System::Setup::App {
    class FluorescencePageControl {
    public:
        using Self = FluorescencePageControl;
        using Pointer = std::shared_ptr<Self>;

        struct Config {
            AppEntity::ExcFilter::_enumerated excFilter;

            QMap<int32_t, AppEntity::FLChannel> flChannels;
            QMap<int32_t, AppEntity::FLFilter> flInternalExcitations;
            QMap<int32_t, AppEntity::FLFilter> flExternalExcitations;
            QMap<int32_t, AppEntity::FLFilter> flEmissions;

            auto operator=(const Config& rhs) -> Config&;
            auto operator==(const Config& rhs) const -> bool;
            auto operator!=(const Config& rhs) const -> bool;
        };

        FluorescencePageControl();
        ~FluorescencePageControl();

        auto Initalize() -> void;
        auto GetConfig() const -> Config;
        auto SetConfig(const Config& config) -> bool;
        auto IsModified(const Config& config) const -> bool;

        auto IsValid(const Config& config, const int32_t& index) -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
