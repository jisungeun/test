#pragma once
#include <memory>

#include "ISetupPage.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class UserPage : public ISetupPage {
        Q_OBJECT
    public:
        using Self = UserPage;
        UserPage(QWidget* parent = nullptr);
        ~UserPage() override;

        auto Initialize() -> bool override;
        auto IsModified() const -> bool override;
        auto IsRestorable() const -> bool override;
        auto Restore() -> bool override;
        auto Save() -> bool override;

    private slots:
        void onAddUser();
        void onEditUser();
        void onDeleteUser();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}