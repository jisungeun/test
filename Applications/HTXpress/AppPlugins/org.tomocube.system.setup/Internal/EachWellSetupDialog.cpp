﻿#include <QLineEdit>
#include <QMouseEvent>

#include <MessageDialog.h>

#include "EachWellSetupDialog.h"
#include "EachWellSetupTableModel.h"
#include "ui_EachWellSetupDialog.h"

namespace HTXpress::AppPlugins::System::Setup::App {
	struct EachWellSetupDialog::Impl {
		QFrame* frame = nullptr;
		Ui::EachWellSetupDialog ui;
		EachWellSetupTableModel tableModel;

		const int32_t margin = 50;

		QPair<int32_t, int32_t> lastWellCount; // rows, columns
		QPair<double, double> lastStartPos; // x, y
		QPair<double, double> lastSpacing; // hor, ver

		auto Init(const Inputs& parameters) -> void;
		auto RecreateWellTable() -> void;
		auto ChangeWellPosition() -> void;
		auto ShouldWellCoordsBeChanged() -> bool;
		auto ConvertWellTableDataToWells(const QList<EachWellSetupTableModel::WellTableData>& wellTableDataList, QList<VesselDataStructure::Well>& wells) -> void;

		auto ApplyStyleSheet(const Self* self) -> void;
	};

	auto EachWellSetupDialog::Impl::ShouldWellCoordsBeChanged() -> bool {
		// Items affecting well coordinate changes
		// 1. well count
		// 2. first well's pos
		// 3. spacing between wells
		if (lastWellCount.first != ui.wellRows->value()) return true;
		if (lastWellCount.second != ui.wellColumns->value()) return true;
		if (fabs(lastStartPos.first - ui.wellX->value()) >= std::numeric_limits<double>::epsilon()) return true;
		if (fabs(lastStartPos.second - ui.wellY->value()) >= std::numeric_limits<double>::epsilon()) return true;
		if (fabs(lastSpacing.first - ui.spacingHor->value()) >= std::numeric_limits<double>::epsilon()) return true;
		if (fabs(lastSpacing.second - ui.spacingVer->value()) >= std::numeric_limits<double>::epsilon()) return true;

		return false;
	}

	EachWellSetupDialog::EachWellSetupDialog(const Inputs& parameters, QWidget* parent) : CustomDialog(parent), d{ new Impl } {
		d->frame = new QFrame(this);
		d->ui.setupUi(d->frame);
		SetContext(d->frame);
		SetTitle("Well Setting");

		SetStandardButtons(StandardButton::Ok | StandardButton::Cancel);
		SetDefaultButton(StandardButton::Ok);

		d->ui.table->setModel(&d->tableModel);
		d->ui.table->horizontalHeader()->setSectionResizeMode(EachWellSetupTableModel::Columns::RowIndex, QHeaderView::ResizeToContents);
		d->ui.table->horizontalHeader()->setSectionResizeMode(EachWellSetupTableModel::Columns::ColumnIndex, QHeaderView::ResizeToContents);
		d->ui.table->horizontalHeader()->setSectionResizeMode(EachWellSetupTableModel::Columns::Name, QHeaderView::Stretch);
		d->ui.table->horizontalHeader()->setSectionResizeMode(EachWellSetupTableModel::Columns::X, QHeaderView::Stretch);
		d->ui.table->horizontalHeader()->setSectionResizeMode(EachWellSetupTableModel::Columns::Y, QHeaderView::Stretch);
		d->ui.table->setItemDelegateForColumn(EachWellSetupTableModel::Columns::Name, new LineEditDelegate());
		d->ui.table->setItemDelegateForColumn(EachWellSetupTableModel::Columns::X, new SpinBoxDelegate());
		d->ui.table->setItemDelegateForColumn(EachWellSetupTableModel::Columns::Y, new SpinBoxDelegate());

		d->Init(parameters);

		connect(d->ui.changeCount, &QPushButton::toggled, this, &Self::onChangeCount);
		connect(d->ui.changeSpacing, &QPushButton::toggled, this, &Self::onChangeSpacing);
		connect(d->ui.changeAppearance, &QPushButton::toggled, this, &Self::onChangeApperance);

		// TODO 현재 사용하지 않음.
		d->ui.spacingManual->setVisible(false);

		d->ApplyStyleSheet(this);
		UpdateSize();
	}

	EachWellSetupDialog::~EachWellSetupDialog() {}

	auto EachWellSetupDialog::GetDlgParam() const -> Inputs {
		Inputs parm;
		QList<VesselDataStructure::Well> wells;
		d->ConvertWellTableDataToWells(d->tableModel.GetWells(), wells);

		parm.wells = wells;
		parm.rows = d->ui.wellRows->value();
		parm.cols = d->ui.wellColumns->value();
		parm.spacingMode = d->ui.spacingAuto->isChecked() ? SpacingMode::AutoSpacing : SpacingMode::ManualSpacing;
		parm.startX = d->ui.wellX->value();
		parm.startY = d->ui.wellY->value();
		parm.spacingHorizontal = d->ui.spacingHor->value();
		parm.spacingVertical = d->ui.spacingVer->value();
		parm.shape = (d->ui.wellShape->currentIndex() == 0) ? AppEntity::WellShape::Circle : AppEntity::WellShape::Rectangle;
		parm.width = d->ui.wellWidth->value();
		parm.height = d->ui.wellHeight->value();

		return parm;
	}

	int EachWellSetupDialog::GetMinimumWidth() const {
		return d->frame->minimumWidth();
	}

	auto EachWellSetupDialog::accept() -> void {
		if (d->ui.changeSpacing->isChecked() || d->ui.changeAppearance->isChecked() || d->ui.changeCount->isChecked()) {
			TC::MessageDialog::warning(this, "Warning", "You cannot close this window until you have completed your changes.");
			return;
		}
		CustomDialog::accept();
	}

	auto EachWellSetupDialog::reject() -> void {
		CustomDialog::reject();
	}

	auto EachWellSetupDialog::onChangeCount(bool toggled) -> void {
		d->ui.wellRows->setReadOnly(!toggled);
		d->ui.wellColumns->setReadOnly(!toggled);
		if (!toggled) {
			if (d->ShouldWellCoordsBeChanged()) {
				const auto answer = TC::MessageDialog::question(this, "Change well configuration", "The configuration of the well is changed and the information of the well will be initialized. Do you want to proceed?");
				if (answer == TC::MessageDialog::StandardButton::Yes) {
					// row or column count changed
					// recreate table data (well)
					d->RecreateWellTable();
				} else {
					d->ui.wellRows->setValue(d->lastWellCount.first);
					d->ui.wellColumns->setValue(d->lastWellCount.second);
				}
			}
		}

		d->lastWellCount = { d->ui.wellRows->value(), d->ui.wellColumns->value() };
	}

	auto EachWellSetupDialog::onChangeSpacing(bool toggled) -> void {
		d->ui.wellX->setReadOnly(!toggled);
		d->ui.wellY->setReadOnly(!toggled);
		d->ui.spacingHor->setReadOnly(!toggled);
		d->ui.spacingVer->setReadOnly(!toggled);
		d->ui.spacingAuto->setDisabled(!toggled);
		d->ui.spacingManual->setDisabled(!toggled);

		if (!toggled) {
			if (d->ShouldWellCoordsBeChanged() && d->ui.spacingAuto->isChecked()) {
				const auto answer = TC::MessageDialog::question(this, "Change spacing configuration", "The configuration of the spacing is changed and the information of the well will be changed. Do you want to proceed?");
				if (answer == TC::MessageDialog::StandardButton::Yes) {
					// when start pos or spacing value changed
					d->ChangeWellPosition();
				} else {
					d->ui.spacingHor->setValue(d->lastSpacing.first);
					d->ui.spacingVer->setValue(d->lastSpacing.second);
					d->ui.wellX->setValue(d->lastStartPos.first);
					d->ui.wellY->setValue(d->lastStartPos.second);
				}

				d->lastSpacing = { d->ui.spacingHor->value(), d->ui.spacingVer->value() };
				d->lastStartPos = { d->ui.wellX->value(), d->ui.wellY->value() };
			}
		}
	}

	void EachWellSetupDialog::onChangeApperance(bool toggled) {
		d->ui.wellWidth->setReadOnly(!toggled);
		d->ui.wellHeight->setReadOnly(!toggled);
		d->ui.wellShape->setDisabled(!toggled);
	}

	auto EachWellSetupDialog::Impl::Init(const Inputs& parameters) -> void {
		using Columns = EachWellSetupTableModel::Columns;

		const auto wells = parameters.wells;
		const auto count = wells.size();
		for (auto c = 0; c < count; ++c) {
			const auto newRow = tableModel.rowCount();

			tableModel.insertRows(newRow, 1, QModelIndex());
			QModelIndex index = tableModel.index(newRow, Columns::RowIndex);
			tableModel.setData(index, wells.at(c).rowIdx, Qt::EditRole);

			index = tableModel.index(newRow, Columns::ColumnIndex);
			tableModel.setData(index, wells.at(c).colIdx, Qt::EditRole);

			index = tableModel.index(newRow, Columns::Name);
			tableModel.setData(index, wells.at(c).label, Qt::EditRole);

			index = tableModel.index(newRow, Columns::X);
			tableModel.setData(index, wells.at(c).x, Qt::EditRole);

			index = tableModel.index(newRow, Columns::Y);
			tableModel.setData(index, wells.at(c).y, Qt::EditRole);
		}

		// set init value
		ui.wellWidth->setValue(parameters.width);
		ui.wellHeight->setValue(parameters.height);
		parameters.shape == AppEntity::WellShape::Circle ? ui.wellShape->setCurrentIndex(0) : ui.wellShape->setCurrentIndex(1);
		ui.wellRows->setValue(parameters.rows);
		ui.wellColumns->setValue(parameters.cols);
		ui.wellX->setValue(parameters.startX);
		ui.wellY->setValue(parameters.startY);
		ui.spacingHor->setValue(parameters.spacingHorizontal);
		ui.spacingVer->setValue(parameters.spacingVertical);
		parameters.spacingMode == SpacingMode::AutoSpacing ? ui.spacingAuto->setChecked(true) : ui.spacingManual->setChecked(true);

		// set non-editable
		ui.wellRows->setReadOnly(true);
		ui.wellColumns->setReadOnly(true);
		ui.wellX->setReadOnly(true);
		ui.wellY->setReadOnly(true);
		ui.spacingHor->setReadOnly(true);
		ui.spacingVer->setReadOnly(true);
		ui.spacingAuto->setDisabled(true);
		ui.spacingManual->setDisabled(true);
		ui.wellWidth->setReadOnly(true);
		ui.wellHeight->setReadOnly(true);
		ui.wellShape->setDisabled(true);

		lastWellCount = { ui.wellRows->value(), ui.wellColumns->value() };
		lastSpacing = { ui.spacingHor->value(), ui.spacingVer->value() };
		lastStartPos = { ui.wellX->value(), ui.wellY->value() };
	}

	auto EachWellSetupDialog::Impl::RecreateWellTable() -> void {
		if (tableModel.rowCount() != 0) tableModel.removeRows(0, tableModel.rowCount(), QModelIndex());

		const auto rows = ui.wellRows->value();
		const auto cols = ui.wellColumns->value();
		const auto startX = ui.wellX->value();
		const auto startY = ui.wellY->value();
		const auto spacingH = ui.spacingHor->value();
		const auto spacingV = ui.spacingVer->value();
		const auto spacingMode = ui.spacingAuto->isChecked() ? SpacingMode::AutoSpacing : SpacingMode::ManualSpacing;

		for (auto rowIdx = 0; rowIdx < rows; rowIdx++) {
			for (auto colIdx = 0; colIdx < cols; colIdx++) {
				const auto tableRow = (rowIdx * cols) + colIdx;
				tableModel.insertRows(tableRow, 1, QModelIndex());
				const auto x = spacingMode == SpacingMode::AutoSpacing ? startX + (colIdx * spacingH) : 0.0;
				const auto y = spacingMode == SpacingMode::AutoSpacing ? startY - (rowIdx * spacingV) : 0.0;

				auto modelIndex = tableModel.index(tableRow, EachWellSetupTableModel::Columns::RowIndex);
				tableModel.setData(modelIndex, rowIdx, Qt::EditRole);
				modelIndex = tableModel.index(tableRow, EachWellSetupTableModel::Columns::ColumnIndex);
				tableModel.setData(modelIndex, colIdx, Qt::EditRole);
				modelIndex = tableModel.index(tableRow, EachWellSetupTableModel::Columns::Name);
				tableModel.setData(modelIndex, VesselPageUtil::SetDefaultWellName(rowIdx, colIdx), Qt::EditRole);

				modelIndex = tableModel.index(tableRow, EachWellSetupTableModel::Columns::X);
				tableModel.setData(modelIndex, x, Qt::EditRole);
				modelIndex = tableModel.index(tableRow, EachWellSetupTableModel::Columns::Y);
				tableModel.setData(modelIndex, y, Qt::EditRole);
			}
		}
	}

	auto EachWellSetupDialog::Impl::ChangeWellPosition() -> void {
		const auto startX = ui.wellX->value();
		const auto startY = ui.wellY->value();
		const auto spacingH = ui.spacingHor->value();
		const auto spacingV = ui.spacingVer->value();

		for (int r = 0; r < tableModel.rowCount(); r++) {
			auto modelIndex = tableModel.index(r, EachWellSetupTableModel::Columns::X);

			// model에서 data display role에 실제 데이타에 +1해서 보여주므로 받을 때 -1 처리
			// TODO 실제 데이타는 0부터 시작하고, 보여줄 때는 1부터 보여줄 때 이 방법말고 더 나은 방법은 없나?
			const auto rowIdx = modelIndex.sibling(r, EachWellSetupTableModel::Columns::RowIndex).data().toInt() - 1;
			const auto colIdx = modelIndex.sibling(r, EachWellSetupTableModel::Columns::ColumnIndex).data().toInt() - 1;
			const auto x = startX + (colIdx * spacingH);
			const auto y = startY - (rowIdx * spacingV);
			tableModel.setData(modelIndex, x, Qt::EditRole);
			modelIndex = tableModel.index(r, EachWellSetupTableModel::Columns::Y);
			tableModel.setData(modelIndex, y, Qt::EditRole);
		}
	}

	auto EachWellSetupDialog::Impl::ConvertWellTableDataToWells(const QList<EachWellSetupTableModel::WellTableData>& wellTableDataList, QList<VesselDataStructure::Well>& wells) -> void {
		for (auto w : wellTableDataList) {
			VesselDataStructure::Well well;
			well.rowIdx = w.row;
			well.colIdx = w.col;
			well.label = w.name;
			well.x = w.x;
			well.y = w.y;
			wells.push_back(well);
		}
	}

	auto EachWellSetupDialog::Impl::ApplyStyleSheet(const Self* self) -> void {
		ui.table->setStyleSheet(QString("QTableView{border-bottom: 0px;}"));
		ui.table->verticalHeader()->setStyleSheet(QString("QHeaderView{border-bottom: 0px;}"));

		for (const auto& button : self->findChildren<QPushButton*>()) {
			if (button->isCheckable()) {
				button->setObjectName("bt-setup-toggle");
			} else {
				button->setObjectName("bt-setup-light");
			}
		}

		for (const auto& label : self->findChildren<QLabel*>()) {
			if (label->objectName().contains("titleLabel")) {
				label->setObjectName("label-h3");
			} else if (label->objectName().contains("itemLabel")) {
				label->setObjectName("label-h5");
			}
		}

		// line divider
		for (const auto& line : self->findChildren<QFrame*>()) {
			if (line->frameShape() == QFrame::VLine) {
				line->setObjectName("line-divider");
				line->setFixedWidth(1);
			} else if (line->frameShape() == QFrame::HLine) {
				line->setObjectName("line-divider");
				line->setFixedHeight(1);
			}
		}
	}

	/// <summary>
	/// SpingBoxDelegate for editing well position on well table
	/// </summary>
	SpinBoxDelegate::SpinBoxDelegate(QObject* parent) : QStyledItemDelegate(parent) {}

	auto SpinBoxDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const -> QWidget* {
		Q_UNUSED(option)
			Q_UNUSED(index)
			auto editor = new QDoubleSpinBox(parent);
		editor->setFrame(false);
		editor->setMinimum(-9999);
		editor->setMaximum(9999);

		return editor;
	}

	auto SpinBoxDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const -> void {
		auto value = index.model()->data(index, Qt::DisplayRole).toDouble();
		auto spinBox = dynamic_cast<QDoubleSpinBox*>(editor);
		spinBox->setValue(value);
	}

	auto SpinBoxDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const -> void {
		auto spinBox = dynamic_cast<QDoubleSpinBox*>(editor);
		spinBox->interpretText();
		auto value = spinBox->value();

		model->setData(index, value, Qt::EditRole);
	}

	auto SpinBoxDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const -> void {
		Q_UNUSED(index)
			editor->setGeometry(option.rect);
	}

	/// <summary>
	/// LineEditDelegate for editing well name on well table
	/// </summary>
	LineEditDelegate::LineEditDelegate(QObject* parent) : QStyledItemDelegate(parent) {}

	QWidget* LineEditDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const {
		Q_UNUSED(option)
			Q_UNUSED(index)
			auto editor = new QLineEdit(parent);
		return editor;
	}

	void LineEditDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const {
		auto value = index.model()->data(index, Qt::DisplayRole).toString();
		auto lineEdit = dynamic_cast<QLineEdit*>(editor);
		lineEdit->setText(value);
	}

	void LineEditDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const {
		auto lineEdit = dynamic_cast<QLineEdit*>(editor);
		auto value = lineEdit->text();
		model->setData(index, value, Qt::EditRole);
	}

	void LineEditDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const {
		Q_UNUSED(index)
			editor->setGeometry(option.rect);
	}
}
