﻿#pragma once

#include <memory>

namespace HTXpress::AppPlugins::System::Setup::App {
    class MediumPageControl {
    public:
        struct Medium {
            QString name{""};
            double ri{0.0};

            auto operator=(const Medium& other) -> Medium&;
            auto operator==(const Medium& other) const -> bool;
            auto operator!=(const Medium& other) const -> bool;
        };
        using Media = QList<Medium>;

        MediumPageControl();
        ~MediumPageControl();

        auto Initialize() -> void;
        auto SetMedia(const Media& media) -> bool;
        auto GetMedia() const -> Media&;
        auto IsModified(const Media& media) const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
