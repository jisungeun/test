﻿#pragma once
#include <memory>
#include <CustomDialog.h>
#include <QFrame>

#include "VesselPageControl.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class VesselImportResultDialog : public TC::CustomDialog {
        Q_OBJECT
    public:
        using Self = VesselImportResultDialog;

        VesselImportResultDialog(const QMap<QString, VesselPageControl::ImportResult>& results, QWidget* parent = nullptr);
        ~VesselImportResultDialog() override;

    protected:
        auto GetMinimumWidth() const -> int override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    namespace vesselImportResult {
        class Frame: public QFrame {
            Q_OBJECT
        public:
            enum Column {
                File = 0,
                Result,
                Model,
                Nickname,
                Remarks,
                _NumOfCols
            };
            Frame(const QMap<QString, VesselPageControl::ImportResult>& results, QWidget* parent = nullptr);
            ~Frame() override;

        private:
            struct Impl;
            std::unique_ptr<Impl> d;
        };
    }
}