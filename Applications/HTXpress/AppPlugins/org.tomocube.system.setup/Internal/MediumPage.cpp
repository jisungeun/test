﻿#include "MediumPage.h"
#include "ui_MediumPage.h"
#include "MediumPageControl.h"
#include "MediumTableModel.h"
#include "TableViewEventFilter.h"
#include "MediumEditDialog.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct MediumPage::Impl {
        Ui::MediumPage ui;
        MediumPageControl control;
        MediumTableModel model;
        TableViewEventFilter* tableFilter{nullptr};

        auto UpdateGUI(const MediumPageControl::Media& media) -> void;
        auto UpdateMedia() const -> MediumPageControl::Media;

        auto ApplyStyleSheet(const Self* self) -> void;
    };

    MediumPage::MediumPage(QWidget* parent) : ISetupPage(parent), d{std::make_unique<Impl>()} {
        d->ui.setupUi(this);

        d->ui.mediumTable->setModel(&d->model);
        d->tableFilter = new TableViewEventFilter(d->ui.mediumTable);
        d->ui.mediumTable->viewport()->installEventFilter(d->tableFilter);
        d->ui.mediumTable->setSelectionBehavior(QAbstractItemView::SelectRows);

        connect(d->ui.addMedium, &QPushButton::clicked, this, &Self::onAddMedium);
        connect(d->ui.editMedium, &QPushButton::clicked, this, &Self::onEditMedium);
        connect(d->ui.deleteMedium, &QPushButton::clicked, this, &Self::onDeleteMedium);

        d->ApplyStyleSheet(this);
    }

    MediumPage::~MediumPage() {
    }

    auto MediumPage::Initialize() -> bool {
        d->control.Initialize();
        const auto media = d->control.GetMedia();
        d->UpdateGUI(media);

        d->ui.addMedium->setEnabled(IsServiceEngineer()||IsAdmin());
        d->ui.editMedium->setEnabled(IsServiceEngineer()||IsAdmin());
        d->ui.deleteMedium->setEnabled(IsServiceEngineer()||IsAdmin());

        return true;
    }

    auto MediumPage::IsModified() const -> bool {
        const auto media = d->UpdateMedia();
        return d->control.IsModified(media);
    }

    auto MediumPage::IsRestorable() const -> bool {
        return true;
    }

    auto MediumPage::Restore() -> bool {
        const auto media = d->control.GetMedia();
        d->UpdateGUI(media);
        return true;
    }

    auto MediumPage::Save() -> bool {
        const auto media = d->UpdateMedia();
        return d->control.SetMedia(media);
    }

    auto MediumPage::onAddMedium() -> void {
        MediumEditDialog dlg(this);
        const auto newRow = d->model.rowCount();

        dlg.SetTitle(tr("Add Medium"));
        QStringList existNames;
        for(auto r = 0; r < d->model.rowCount(); r++) {
                existNames.push_back(d->model.index(r, MediumTableModel::Columns::Name).data().toString());
        }
        dlg.SetCurrentExistNames(existNames);

        if(dlg.exec()) {
            d->model.insertRows(newRow, 1, QModelIndex());
            QModelIndex index = d->model.index(newRow, MediumTableModel::Columns::Name);
            d->model.setData(index, dlg.GetName(), Qt::EditRole);
            index = d->model.index(newRow, MediumTableModel::Columns::RI);
            d->model.setData(index, dlg.GetRI(), Qt::EditRole);
        }
    }

    auto MediumPage::onEditMedium() -> void {
        if(!d->ui.mediumTable->currentIndex().isValid()) return;

        MediumEditDialog dlg(this);
        const auto selIndex = d->ui.mediumTable->currentIndex();
        const auto currRow = selIndex.row();
        d->ui.mediumTable->selectRow(currRow);
        const auto name = d->model.index(currRow, MediumTableModel::Columns::Name).data().toString();
        const auto ri = d->model.index(currRow, MediumTableModel::Columns::RI).data().toDouble();

        dlg.SetTitle(tr("Edit Medium"));
        dlg.SetName(name);
        dlg.SetRI(ri);

        QStringList existNames;
        for(auto r = 0; r < d->model.rowCount(); r++) {
            if(r != currRow) {
                existNames.push_back(d->model.index(r, MediumTableModel::Columns::Name).data().toString());
            }
        }
        dlg.SetCurrentExistNames(existNames);

        if(dlg.exec()) {
            QModelIndex index = d->model.index(currRow, MediumTableModel::Columns::Name);
            d->model.setData(index, dlg.GetName(), Qt::EditRole);
            index = d->model.index(currRow, MediumTableModel::Columns::RI);
            d->model.setData(index, dlg.GetRI(), Qt::EditRole);
        }
    }

    auto MediumPage::onDeleteMedium() -> void {
        const auto selectionModel = d->ui.mediumTable->selectionModel();
        const auto indexes = selectionModel->selectedRows();
        QList<int32_t> sortedRows;

        for (const auto& idx : indexes) {
            if (idx.isValid()) {
                auto row = idx.row();
                sortedRows.push_back(row);
            }
        }

        std::sort(sortedRows.begin(), sortedRows.end(), [](const int32_t a, const int32_t b)-> bool {
            return a < b;
        });

        for (int i = sortedRows.count() - 1; i >= 0; --i) {
            const auto curr = sortedRows[i];
            d->model.removeRows(curr, 1, QModelIndex());
        }
    }
    
    auto MediumPage::Impl::UpdateGUI(const MediumPageControl::Media& media) -> void {
        if (model.rowCount() != 0) model.removeRows(0, model.rowCount(), QModelIndex());

        for (const auto& medium : media) {
            model.insertRows(model.rowCount(), 1, QModelIndex());
            auto index = model.index(model.rowCount() - 1, MediumTableModel::Columns::Name, QModelIndex());
            model.setData(index, medium.name, Qt::EditRole);
            index = model.index(model.rowCount() - 1, MediumTableModel::Columns::RI, QModelIndex());
            model.setData(index, medium.ri, Qt::EditRole);
        }
    }

    auto MediumPage::Impl::UpdateMedia() const -> MediumPageControl::Media {
        MediumPageControl::Media media;

        for (auto [name, ri] : model.GetMedia()) {
            MediumPageControl::Medium medium;
            medium.name = name;
            medium.ri = ri;
            media.push_back(medium);
        }

        return media;
    }

    auto MediumPage::Impl::ApplyStyleSheet(const Self* self) -> void {
        ui.mediumTable->setStyleSheet(QString("QTableView{border-bottom: 0px;}"));
        ui.mediumTable->verticalHeader()->setStyleSheet(QString("QHeaderView{border-bottom: 0px;}"));

        for(const auto& button : self->findChildren<QPushButton*>()) {
            if(button->isCheckable()) {
                button->setObjectName("bt-setup-toggle");
            }
            else {
                button->setObjectName("bt-setup-light");
            }
        }

        for(const auto& label : self->findChildren<QLabel*>()) {
            if(label->objectName().contains("title")) {
                label->setObjectName("label-h2");
            }
        }
    }
}
