﻿#include <QFrame>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QLayout>
#include <QLabel>
#include <QPushButton>
#include <QHeaderView>

#include "VesselImportResultDialog.h"

#include <QFileInfo>

namespace HTXpress::AppPlugins::System::Setup::App {
    struct VesselImportResultDialog::Impl {
        vesselImportResult::Frame* frame{nullptr};
    };

    VesselImportResultDialog::VesselImportResultDialog(const QMap<QString, VesselPageControl::ImportResult>& results, QWidget* parent) : CustomDialog(parent), d{std::make_unique<Impl>()} {
        d->frame = new vesselImportResult::Frame(results, this);
        SetContext(d->frame);
        SetTitle(tr("Import Results"));
        SetStandardButtons(StandardButton::Ok);
        SetDefaultButton(StandardButton::Ok);
        
        UpdateSize();
    }

    VesselImportResultDialog::~VesselImportResultDialog() {
    }

    int VesselImportResultDialog::GetMinimumWidth() const {
        return d->frame->maximumWidth();
    }
}

namespace HTXpress::AppPlugins::System::Setup::App::vesselImportResult {
    struct Frame::Impl {
        explicit Impl(Frame* frame) {
            this->self = frame;
        }

        Frame* self;
        QTableWidget* table{nullptr};
        QLabel* summary{nullptr};

        int32_t maxRowsPerPage = 20;

        struct TableData{
            int32_t pageIndex{-1};
            QString originalFile{};
            QString modelName{};
            QString nickname{};
            QString reamrks{};
            bool result;
        };

        QList<std::shared_ptr<TableData>> list;

        QMap<QString, VesselPageControl::ImportResult> reuslts;

        auto InitUI() -> void;
        auto CreateTableItems(int32_t row, const std::shared_ptr<TableData>& tableData) -> void;
        auto GetFlagString(const QString& model, const QString& nickname, const QString& extraInfo, const VesselPageControl::ImportResultFlags& flags) const -> QString;
        auto ConvertToTableData(const QMap<QString, VesselPageControl::ImportResult>& results) -> void;
    };
    
    Frame::Frame(const QMap<QString, VesselPageControl::ImportResult>& results, QWidget* parent) : QFrame(parent), d{std::make_unique<Impl>(this)} {
        d->table = new QTableWidget(this);
        d->summary = new QLabel(this);

        auto layout = new QGridLayout(this);
        layout->addWidget(d->table);
        layout->addWidget(d->summary);
        layout->setContentsMargins(0,0,0,0);

        this->setLayout(layout);

        d->ConvertToTableData(results);
        d->InitUI();
    }

    Frame::~Frame() {
    }

    auto Frame::Impl::InitUI() -> void {
        table->setStyleSheet(QString("QTableWidget{border-bottom: 0px;}"
                                     "QTableWidget::item {padding: 2px;}"));
        table->verticalHeader()->setStyleSheet(QString("QHeaderView{border-bottom: 0px;}"));
        table->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);
        table->setSelectionBehavior(QAbstractItemView::SelectRows);
        table->setEditTriggers(QAbstractItemView::NoEditTriggers);
        table->setSelectionMode(QAbstractItemView::SingleSelection);
        table->setSortingEnabled(true);
        table->setColumnCount(_NumOfCols);
        table->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        QStringList columnHeaderTitles{
            "Source File",
            "Result",
            "Vessel Model",
            "Vessel Name",
            "Remarks"
        };

        table->setHorizontalHeaderLabels(columnHeaderTitles);
        table->horizontalHeader()->setStretchLastSection(true);
        table->horizontalHeader()->setMaximumSectionSize(200);

        auto height = [&]()->int32_t {
            if(list.size() > maxRowsPerPage) return 750;
            return list.size()*35+50;
        }();

        table->setRowCount(list.size());
        int32_t row = 0;
        int32_t successCount = 0;
        for(const auto& tableData : list) {
            CreateTableItems(row, tableData);
            if(tableData->result) {
                successCount++;
            }
            row++;
        }

        table->setMinimumHeight(height);
        table->sortByColumn(Result, Qt::DescendingOrder); // Success -> Fail
        table->resizeColumnToContents(File);

        summary->setTextFormat(Qt::RichText);
        summary->setText(QString("Import has been completed.<br><b>%1</b> of %2 vessel file(s) imported.").arg(successCount).arg(list.size()));
    }

    auto Frame::Impl::CreateTableItems(int32_t row, const std::shared_ptr<TableData>& tableData) -> void {
        QFileInfo fileInfo(tableData->originalFile);

        const auto& srcFileName = fileInfo.fileName();
        const auto& importResult = tableData->result;
        const auto& modelName = tableData->modelName;
        const auto& nickname = tableData->nickname;
        const auto& remarks = tableData->reamrks;

        for(auto col = 0; col < _NumOfCols; ++col) {
            const auto item = new QTableWidgetItem(QTableWidgetItem::UserType);
        switch (col) {
            case File:
                item->setText(srcFileName);
                item->setToolTip(tableData->originalFile);
                break;
            case Model: 
                item->setText(modelName);
                item->setToolTip(modelName);
                break;
            case Nickname: 
                item->setText(nickname);
                item->setToolTip(nickname);
                break;
            case Result: {
                QString resultString;
                auto font = item->font();
                font.setBold(importResult);
                font.setItalic(!importResult);
                item->setFont(font);

                if(importResult) {
                    resultString = "Success";
                }
                else {
                    resultString = "Fail";
                }

                item->setText(resultString);
                item->setTextAlignment(Qt::AlignCenter);
            }
                break;
            case Remarks:
                item->setText(remarks);
                item->setToolTip(remarks);
                break;
            default: ;
        }

            table->setItem(row, col, item);
        }
    }

    auto Frame::Impl::GetFlagString(const QString& model, const QString& nickname, const QString& extraInfo, const VesselPageControl::ImportResultFlags& flags) const -> QString {
        Q_UNUSED(nickname)

        QStringList remarks;
        if(flags & VesselPageControl::HasExtraInfo) {
            remarks.append(extraInfo);
        }
        if (flags & VesselPageControl::DuplicateNickname && flags & VesselPageControl::Normal) {
            remarks.append(tr("Vessel name changed to [%1].").arg(model));
        }
        if (flags & VesselPageControl::DuplicateNameBetweenFiles && flags & VesselPageControl::Normal) {
            remarks.append(tr("Vessel name changed to [%1].").arg(model));
        }
        if(flags & VesselPageControl::InvalidNickname && flags & VesselPageControl::Normal) {
            remarks.append(tr("Vessel name changed to [%1].").arg(model));
        }
        if(flags & VesselPageControl::DuplicateModelBetweenFiles) {
            remarks.append(tr("[%1] already exists above.").arg(model));
        }
        if (flags & VesselPageControl::DuplicateModelName) {
            remarks.append(tr("[%1] already exists in the list.").arg(model));
        }
        if (flags & VesselPageControl::UnknownError) {
            remarks.append(tr("Unknown error occurred."));
        }

        return remarks.join(" ");
    }

    auto Frame::Impl::ConvertToTableData(const QMap<QString, VesselPageControl::ImportResult>& results) -> void {
        auto count = 0;
        for (auto it = results.cbegin(); it != results.cend(); ++it) {
            auto tableData = std::make_shared<TableData>();
            tableData->pageIndex = count / maxRowsPerPage;
            tableData->modelName = it->modelName;
            tableData->nickname = it->nickname;
            tableData->originalFile = it.key();
            tableData->result = it->result;
            tableData->reamrks = GetFlagString(it->modelName, it->nickname, it->extraInfo, it->flags);
            list.push_back(tableData);
            count++;
        }
    }
}
