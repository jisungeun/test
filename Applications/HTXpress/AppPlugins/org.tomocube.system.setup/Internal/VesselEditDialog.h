﻿#pragma once

#include <memory>

#include "CustomDialog.h"

#include "VesselDataStructures.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class VesselEditDialog : public TC::CustomDialog {
        Q_OBJECT
    public:
        using Self = VesselEditDialog;

        VesselEditDialog(const VesselDataStructure::Vessel& vessel, 
                         const QStringList& nicknames, 
                         const QList<double>& NAs,
                         QWidget* parent = nullptr);
        ~VesselEditDialog() override;

        auto Vessel() const -> VesselDataStructure::Vessel;
        auto SetMultidishChangeable(bool enable) -> void;

    public slots:
        void accept() override;
        void reject() override;
        void onShowWellSetup();

    protected:
        auto GetMinimumWidth() const -> int override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
