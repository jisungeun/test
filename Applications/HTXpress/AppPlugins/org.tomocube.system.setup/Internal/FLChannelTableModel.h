﻿#pragma once

#include <memory>

#include <QAbstractTableModel>
#include <QColor>

namespace HTXpress::AppPlugins::System::Setup::App {
    class FLChannelTableModel : public QAbstractTableModel{
        Q_OBJECT
    public:
        enum Columns {Index=0, Name, Excitation, Emission, Color, _NumOfColumns};

        struct LightSource {
            int32_t wavelength;
            int32_t bandwidth;
        };

        struct FLChannelData {
            int32_t index;
            QString name;
            LightSource excitation; // wavelen, bandwidth
            LightSource emission; // wavelen, bandwidth
            QColor color;

            auto operator==(const FLChannelData &other) const -> bool {
                return (index==other.index && name == other.name && 
                        excitation.bandwidth == other.excitation.bandwidth&&
                        excitation.wavelength == other.excitation.wavelength && 
                        emission.bandwidth == other.emission.bandwidth&&
                        emission.wavelength == other.emission.wavelength&&
                        color == other.color);
            }
        };

        using Self = FLChannelTableModel;
        using Pointer = std::shared_ptr<Self>;

        explicit FLChannelTableModel(QObject*parent = nullptr);
        ~FLChannelTableModel() override;

        auto rowCount(const QModelIndex& parent = QModelIndex()) const -> int override;
        auto columnCount(const QModelIndex& parent = QModelIndex()) const -> int override;
        auto data(const QModelIndex& index, int role) const -> QVariant override;
        auto headerData(int section, Qt::Orientation orientation, int role) const -> QVariant override;
        auto setData(const QModelIndex& index, const QVariant& value, int role) -> bool override;
        auto flags(const QModelIndex& index) const -> Qt::ItemFlags override;

        auto insertRows(int row, int count, const QModelIndex& parent) -> bool override;
        auto removeRows(int row, int count, const QModelIndex& parent) -> bool override;
        
        auto GetChannels() const -> const QList<FLChannelData>&;
        auto IsValid() const -> bool;
        auto GetInvalidRows() const -> QList<int32_t>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
Q_DECLARE_METATYPE(HTXpress::AppPlugins::System::Setup::App::FLChannelTableModel::LightSource);
