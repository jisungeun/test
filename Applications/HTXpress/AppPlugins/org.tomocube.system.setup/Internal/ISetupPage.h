#pragma once
#include <QWidget>

namespace HTXpress::AppPlugins::System::Setup::App {
    class ISetupPage : public QWidget {
        Q_OBJECT
    protected:
        enum class UserLevel {
            User,
            Admin,
            ServiceEngineer
        };

    public:
        ISetupPage(QWidget* parent = nullptr);
        ~ISetupPage() override;

        auto GetUserLevel() const->UserLevel;
        auto HasAuthority(const UserLevel& level) const->bool;
        auto IsServiceEngineer() const->bool;
        auto IsAdmin() const->bool;

        virtual auto Initialize()->bool = 0;
        virtual auto IsModified() const->bool = 0;
        virtual auto IsRestorable() const->bool = 0;
        virtual auto Restore()->bool = 0;
        virtual auto Save()->bool = 0;
    };
}
