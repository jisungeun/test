﻿#pragma once

#include <memory>

#include <QDialog>
#include <QLabel>

#include "CustomDialog.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class FLChannelEditDialog : public TC::CustomDialog {
        Q_OBJECT
    public:
        enum WidgetColumns {
            Name = 0, Excitation, Emission, Color
        };

        FLChannelEditDialog(int32_t index, QWidget* parent = nullptr);
        ~FLChannelEditDialog() override;

        auto SetExcitationList(const QStringList& list) -> void;
        auto SetEmissionList(const QStringList& list) -> void;
        auto SetCurrentExistNames(const QStringList& names) -> void;

        auto SetCurrentData(const QString& name, const QString& ex, const QString& em, const QColor& color) -> void;

        auto GetIndex() const -> int32_t;
        auto GetName() const -> QString;
        auto GetExcitation() -> QString;
        auto GetEmission() -> QString;
        auto GetColor() const -> QColor;

    protected:
        auto eventFilter(QObject* watched, QEvent* event) -> bool override;
        auto GetMinimumWidth() const -> int override;

    public slots:
        void accept() override;
        void reject() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
