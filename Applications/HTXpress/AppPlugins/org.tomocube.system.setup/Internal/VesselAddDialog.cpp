﻿#include <QMouseEvent>
#include <QPushButton>

#include <MessageDialog.h>
#include <TCSpinBox.h>
#include <FileNameValidator.h>

#include "VesselAddDialog.h"
#include "ui_VesselAddDialog.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct VesselAddDialog::Impl {
        QFrame* frame = nullptr;
        Ui::VesselAddDialog ui;
        const int32_t margin = 50;

        QStringList existModels{}; // set when create a new vessel
        QStringList existNicknames{}; // set when create a new vessel

        auto ApplyStyleSheet(Self* self) -> void;
    };

    auto VesselAddDialog::Impl::ApplyStyleSheet(Self* self) -> void {
        self->SetTitle("Create a New Vessel");

        for (const auto& button : self->findChildren<QPushButton*>()) {
            if (button->isCheckable()) {
                button->setObjectName("bt-setup-toggle");
            }
            else {
                button->setObjectName("bt-setup-light");
            }
        }

        for (const auto& label : self->findChildren<QLabel*>()) {
            if (label->objectName().contains("titleLabel")) {
                label->setObjectName("label-h2");
            }
            else if (label->objectName().contains("itemLabel")) {
                label->setObjectName("label-h3");
            }
            else if (label->objectName().contains("subItemLabel")) {
                label->setObjectName("label-h5");
            }
        }

        // line divider
        for (const auto& line : self->findChildren<QFrame*>()) {
            if (line->frameShape() == QFrame::VLine) {
                line->setObjectName("line-divider");
                line->setFixedWidth(1);
            }
            else if (line->frameShape() == QFrame::HLine) {
                line->setObjectName("line-divider");
                line->setFixedHeight(1);
            }
        }
    }

    VesselAddDialog::VesselAddDialog(const QStringList& models, const QStringList& nicknames, const QList<double>& NAs, QWidget* parent) : CustomDialog(parent), d{std::make_unique<Impl>()} {
        d->frame = new QFrame(this);
        d->ui.setupUi(d->frame);
        SetContext(d->frame);

        d->existModels = models;
        d->existNicknames = nicknames;

        for(const auto& na : NAs) {
            d->ui.naCombo->addItem(QString::number(na, 'g', 3), na);
        }

        SetStandardButtons(StandardButton::Ok | StandardButton::Cancel);
        SetDefaultButton(StandardButton::Ok);

        d->ApplyStyleSheet(this);
    }

    VesselAddDialog::~VesselAddDialog() {
    }

    auto VesselAddDialog::Vessel() const -> VesselDataStructure::Vessel {
        VesselDataStructure::Vessel vessel;

        vessel.model = d->ui.vesselModel->text();
        vessel.name = d->ui.vesselName->text();
        vessel.na = d->ui.naCombo->currentData().toDouble();
        vessel.afOffset = d->ui.vesselAFOffset->value();
        vessel.w = d->ui.vesselWidth->value();
        vessel.h = d->ui.vesselHeight->value();
        vessel.useMultiDishHolder = d->ui.useMultiDishHolder->isChecked();
        vessel.imgArea.shape = AppEntity::ImagingAreaShape::_from_integral(d->ui.imgShape->currentIndex());
        vessel.imgArea.x = d->ui.imgX->value();
        vessel.imgArea.y = d->ui.imgY->value();
        vessel.imgArea.w = d->ui.imgWidth->value();
        vessel.imgArea.h = d->ui.imgHeight->value();

        const auto wellWidth = d->ui.wellWidth->value();
        const auto wellHeight = d->ui.wellHeight->value();
        const auto wellRows = d->ui.wellRows->value();
        const auto wellCols = d->ui.wellColumns->value();
        const auto spacingH = d->ui.spacingHor->value();
        const auto spacingV = d->ui.spacingVer->value();
        const auto startX = d->ui.wellX->value();
        const auto startY = d->ui.wellY->value();

        vessel.wellProp.shape = AppEntity::WellShape::_from_integral(d->ui.wellShape->currentIndex());
        vessel.wellProp.w = wellWidth;
        vessel.wellProp.h = wellHeight;
        vessel.wellProp.rows = wellRows;
        vessel.wellProp.cols = wellCols;
        vessel.wellProp.spacingH = spacingH;
        vessel.wellProp.spacingV = spacingV;

        QList<VesselDataStructure::Well> wells;
            for (int row = 0; row < wellRows; row++) {
                for (int col = 0; col < wellCols; col++) {
                    VesselDataStructure::Well well;
                    well.rowIdx = row;
                    well.colIdx = col;
                    well.x = startX + (col * spacingH);
                    well.y = startY - (row * spacingV);
                    well.label = VesselPageUtil::SetDefaultWellName(row, col); // TODO int(row) to alphabet
                    wells.push_back(well);
                }
            }

        vessel.wells = wells;
        return vessel;
    }
    
    auto VesselAddDialog::accept() -> void {
        if (d->existModels.contains(d->ui.vesselModel->text())) {
            TC::MessageDialog::warning(this, "Model Duplication", QString("%1 is already exists.\n").arg(d->ui.vesselModel->text()));
            d->ui.vesselModel->setFocus();
            return;
        }

        if (d->existNicknames.contains(d->ui.vesselName->text())) {
            TC::MessageDialog::warning(this, "Name Duplication", QString("%1 is already exists.\n").arg(d->ui.vesselName->text()));
            d->ui.vesselName->setFocus();
            return; 
        }

        TC::FileNameValidator validator;
        validator.SetMaxLength(32);
        if (!validator.IsValid(d->ui.vesselModel->text())) {
            const auto error = validator.GetErrorString();
            TC::MessageDialog::warning(this, tr("Invalid model name"), error);
            d->ui.vesselModel->setFocus();
            return;
        }

        if (d->ui.vesselName->text().isEmpty()) {
            TC::MessageDialog::warning(this, "Warning", QString("You must enter the model name."));
            if (d->ui.vesselModel->text().isEmpty()) d->ui.vesselModel->setFocus();
            else d->ui.vesselName->setFocus();
            return;
        }

        CustomDialog::accept();
    }

    void VesselAddDialog::reject() {
        const auto answer = TC::MessageDialog::question(this, "Close", "Do you want to close?",
                                                        StandardButton::Yes | StandardButton::No, 
                                                        StandardButton::Yes);
        if (answer == StandardButton::Yes) {
            CustomDialog::reject();
        }
    }

    int VesselAddDialog::GetMinimumWidth() const {
        return d->frame->maximumWidth();
    }
}
