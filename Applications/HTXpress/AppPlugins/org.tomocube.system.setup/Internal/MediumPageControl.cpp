﻿#include <MediumDataRepo.h>
#include <MediumConfigController.h>

#include "MediumPageControl.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct MediumPageControl::Impl {
        Media media;

        auto Restore() -> void;
        auto Store() -> void;
    };

    auto MediumPageControl::Impl::Restore() -> void {
        auto repo = AppEntity::MediumDataRepo::GetInstance();
        Media list;

        for(const auto& medium : repo->GetMedia()) {
            list.push_back({medium.GetName(), medium.GetRI()});
        }
        media = list;
    }

    auto MediumPageControl::Impl::Store() -> void {
        auto repo = AppEntity::MediumDataRepo::GetInstance();
        AppEntity::MediumDataRepo::List list;
        for(const auto& medium : media) {
            AppEntity::MediumData md;
            md.SetName(medium.name);
            md.SetRI(medium.ri);
            list.push_back(md);
        }
        repo->SetMedia(list);
    }

    auto MediumPageControl::Medium::operator=(const Medium& other) -> Medium& {
        name = other.name;
        ri = other.ri;
        return *this;
    }

    auto MediumPageControl::Medium::operator==(const Medium& other) const -> bool {
        return(name==other.name && fabs(ri-other.ri)<std::numeric_limits<double>::epsilon());
    }

    auto MediumPageControl::Medium::operator!=(const Medium& other) const -> bool {
        return !(*this == other);
    }

    MediumPageControl::MediumPageControl() : d{std::make_unique<Impl>()} {
    }

    MediumPageControl::~MediumPageControl() {
    }

    auto MediumPageControl::Initialize() -> void {
        d->Restore();
    }

    auto MediumPageControl::SetMedia(const Media& media) -> bool {
        d->media = media;
        d->Store();

        auto controller = Interactor::MediumConfigController();
        return controller.Save();
    }

    auto MediumPageControl::GetMedia() const -> Media& {
        return d->media;
    }
    auto MediumPageControl::IsModified(const Media& media) const -> bool {
        return d->media != media;
    }
}
