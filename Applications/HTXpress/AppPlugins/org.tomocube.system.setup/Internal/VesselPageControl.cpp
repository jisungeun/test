#include <QFileInfo>

#include <System.h>
#include <VesselConfigController.h>
#include <VesselRepo.h>
#include <VesselReader.h>
#include <FileNameValidator.h>
#include <VesselMapUtility.h>

#include "VesselPageControl.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct VesselPageControl::Impl {
        VesselDataStructure::Vessels original;
        VesselDataStructure::Vessels temp;

        QStringList deleteTargets;
        QString vesselFolderPath{""};

        const int32_t maxModelLength{32};

        auto Restore() -> void;
        auto Store() -> void;

        auto ConvertWellShape(AppEntity::WellShape shape)->TC::WellShape;
        auto ConvertImagingAreaShape(AppEntity::ImagingAreaShape shape)->TC::ImagingAreaShape;
        auto ConvertVessel(const AppEntity::Vessel::Pointer& entity) -> VesselDataStructure::Vessel;

        auto IsValidModelName(const QString& modelName, QString& reason) -> bool;
        auto IsValidNickname(const QString& nickname) -> bool;
    };

    auto VesselPageControl::Impl::IsValidModelName(const QString& modelName, QString& reason) -> bool {
        TC::FileNameValidator validator;

        validator.SetMaxLength(maxModelLength);
        const auto& isValid = validator.IsValid(modelName);
        if(validator.GetError() == +TC::FileNameValid::EmptyStringError) {
            reason = tr("Model is empty.");   
        }
        else {
            reason = validator.GetErrorString();
        }
        return isValid;
    }

    auto VesselPageControl::Impl::IsValidNickname(const QString& nickname) -> bool {
        const auto& isValid = !nickname.isEmpty();
        return isValid;
    }

    VesselPageControl::VesselPageControl() : d{new Impl} {
    }

    VesselPageControl::~VesselPageControl() {
    }

    auto VesselPageControl::SetVesselFolderPath(const QString& vesselFolderPath) -> void {
        d->vesselFolderPath = vesselFolderPath;
    }

    auto VesselPageControl::Initialize() -> void {
        d->Restore();
    }

    auto VesselPageControl::SetVessels() -> bool {
        // delete target vessel file
        Delete();

        d->original = d->temp;
        d->Store();

        auto controller = Interactor::VesselConfigController();
        return controller.Save();
    }

    auto VesselPageControl::Delete() -> bool {
        auto controller = Interactor::VesselConfigController();
        return controller.Delete(d->deleteTargets);
    }

    auto VesselPageControl::GetVessels() const -> VesselDataStructure::Vessels& {
        return d->original;
    }

    auto VesselPageControl::IsModified() const -> bool {
        return d->original != d->temp;
    }

    auto VesselPageControl::AddVessel(const VesselDataStructure::Vessel& newVessel) -> void {
        d->temp.push_back(newVessel);
    }

    auto VesselPageControl::EditVessel(const QString& editTargetModelName, const VesselDataStructure::Vessel& editedVessel) -> void {
        auto found = std::find_if(d->temp.begin(), d->temp.end(), [editTargetModelName](const VesselDataStructure::Vessel& v) {
            return v.model == editTargetModelName;
        });
        if (found != d->temp.end()) {
            *found = editedVessel;
        }
    }

    auto VesselPageControl::DeleteVessel(const QString& deleteTargetModelName) -> void {
        auto found = std::find_if(d->temp.begin(), d->temp.end(), [deleteTargetModelName](const VesselDataStructure::Vessel& v) {
            return v.model == deleteTargetModelName;
        });

        if (found != d->temp.end()) {
            const auto fileFullPath = QString("%1/%2.vessel").arg(d->vesselFolderPath).arg(found->model);
            d->deleteTargets << fileFullPath;
            d->temp.removeOne(*found);
        }
    }

    auto VesselPageControl::ImportVessels(QMap<QString, ImportResult>& importResults) -> VesselDataStructure::Vessels {
        const auto reader = AppComponents::VesselIO::VesselReader();
        VesselDataStructure::Vessels output;

        for (auto it = importResults.cbegin(); it != importResults.cend(); ++it) {
            const auto& vesselFile = it.key();
            const auto& entityVessel = reader.Read(vesselFile);

            if (entityVessel == nullptr) {
                importResults[vesselFile].flags.setFlag(UnknownError);
                continue;
            }

            const auto& modelName = entityVessel->GetModel();
            const auto& nickname = entityVessel->GetName();

            importResults[vesselFile].modelName = modelName;
            importResults[vesselFile].nickname = nickname;

            QString reason;
            if(!d->IsValidModelName(modelName, reason)) {
                importResults[vesselFile].flags.setFlag(InvalidModelName);
                importResults[vesselFile].flags.setFlag(HasExtraInfo);
                importResults[vesselFile].extraInfo = reason;
                continue;
            }

            if(!d->IsValidNickname(nickname)) {
                importResults[vesselFile].flags.setFlag(InvalidNickname);
                importResults[vesselFile].nickname = modelName;
                entityVessel->SetName(modelName);
            }

            for (const auto& v : d->temp) {
                if (v.model == modelName) {
                    importResults[vesselFile].flags.setFlag(DuplicateModelName);
                    break;
                }

                if (v.name == nickname) {
                    importResults[vesselFile].flags.setFlag(DuplicateNickname);
                    importResults[vesselFile].nickname = modelName;
                    entityVessel->SetName(modelName);
                }
            }

            if (false == importResults[vesselFile].flags.testFlag(DuplicateModelName) &&
                false == importResults[vesselFile].flags.testFlag(InvalidModelName)) {
                for (auto iterator = importResults.cbegin(); iterator != importResults.cend(); ++iterator) {
                    if (iterator.key() == vesselFile) continue;

                    // import file들 중에서 model 중복되는 경우
                    if (iterator.value().modelName == entityVessel->GetModel()) {
                        importResults[vesselFile].flags.setFlag(DuplicateModelBetweenFiles);
                        break;
                    }

                    // import file들 중에서 name 중복되는 경우
                    if (iterator.value().nickname == entityVessel->GetName()) {
                        importResults[vesselFile].flags.setFlag(DuplicateNameBetweenFiles);
                        importResults[vesselFile].nickname = modelName;
                        entityVessel->SetName(modelName);
                    }
                }

                if (!importResults[vesselFile].flags.testFlag(DuplicateModelBetweenFiles)) {
                    importResults[vesselFile].flags.setFlag(Normal);
                    importResults[vesselFile].result = true;
                    output.push_back(d->ConvertVessel(entityVessel));
                }
            }
        }
        return output;
    }

    auto VesselPageControl::GetTempVessel(const QString& model, VesselDataStructure::Vessel& vessel) -> bool {
        auto found = std::find_if(d->temp.begin(), d->temp.end(), [model](const VesselDataStructure::Vessel& v) {
            return v.model == model;
        });

        if (found != d->temp.end()) {
            vessel = (*found);
            return true;
        }

        return false;
    }

    auto VesselPageControl::GetTempVessels() const -> VesselDataStructure::Vessels& {
        return d->temp;
    }

    auto VesselPageControl::Convert(const VesselDataStructure::Vessel& vessel) const -> std::shared_ptr<TC::VesselMap> {
        using Converter = AppComponents::VesselMapUtility::Converter;
        auto entityVessel = std::make_shared<AppEntity::Vessel>();
        QMap<AppEntity::WellIndex, QString> wellNames;

        entityVessel->SetSize(vessel.w, vessel.h);
        entityVessel->SetWellCount(vessel.wellProp.rows, vessel.wellProp.cols);
        entityVessel->SetWellSize(vessel.wellProp.w, vessel.wellProp.h);
        entityVessel->SetWellShape(vessel.wellProp.shape);

        for(const auto& well : vessel.wells) {
            entityVessel->AddWell(well.rowIdx, well.colIdx, well.x, well.y, well.label);
            auto wellIndex = entityVessel->GetWellIndex(well.rowIdx, well.colIdx);
            wellNames[wellIndex] = well.label;
        }
        entityVessel->SetImagingAreaSize(vessel.imgArea.w, vessel.imgArea.h);
        entityVessel->SetImagingAreaPosition(vessel.imgArea.x, vessel.imgArea.y);
        entityVessel->SetImagingAreaShape(vessel.imgArea.shape);

        return Converter::ToTCVesselMap(entityVessel, wellNames);
    }

    auto VesselPageControl::GetNAs() const -> QList<double> {
        const auto sysConfig = AppEntity::System::GetSystemConfig();
        return sysConfig->GetIlluminationCalibrationParameterNAs();
    }

    auto VesselPageControl::Impl::Restore() -> void {
        auto repo = AppEntity::VesselRepo::GetInstance();
        VesselDataStructure::Vessels list;

        for (const auto& vessel : repo->GetVessels()) {
            // convert entity vessel to local vessel
            VesselDataStructure::Vessel newVessel;

            VesselDataStructure::WellProp wellProp;
            wellProp.shape = vessel->GetWellShape();
            wellProp.rows = vessel->GetWellRows();
            wellProp.cols = vessel->GetWellCols();
            wellProp.w = vessel->GetWellSizeX();
            wellProp.h = vessel->GetWellSizeY();
            wellProp.spacingH = vessel->GetWellSpacingHorizontal();
            wellProp.spacingV = vessel->GetWellSpacingVertical();

            VesselDataStructure::ImagingArea imgArea;
            imgArea.shape = vessel->GetImagingAreaShape();
            imgArea.x = vessel->GetImagingAreaPositionX();
            imgArea.y = vessel->GetImagingAreaPositionY();
            imgArea.w = vessel->GetImagingAreaSizeX();
            imgArea.h = vessel->GetImagingAreaSizeY();

            newVessel.model = vessel->GetModel();
            newVessel.name = vessel->GetName();
            newVessel.na = vessel->GetNA();
            newVessel.afOffset = vessel->GetAFOffset();
            newVessel.w = vessel->GetSizeX();
            newVessel.h = vessel->GetSizeY();
            newVessel.useMultiDishHolder = vessel->IsMultiDish();

            newVessel.wellProp = wellProp;
            newVessel.imgArea = imgArea;

            for (const auto& wellIndex : vessel->GetWellIndices()) {
                VesselDataStructure::Well well;
                well.rowIdx = vessel->GetWellRowIndex(wellIndex);
                well.colIdx = vessel->GetWellColIndex(wellIndex);
                well.label = vessel->GetWellLabel(wellIndex);
                well.x = vessel->GetWellPositionX(wellIndex);
                well.y = vessel->GetWellPositionY(wellIndex);
                newVessel.wells.push_back(well);
            }

            list.push_back(newVessel);
        }

        original = temp = list;
        deleteTargets.clear();
    }

    auto VesselPageControl::Impl::Store() -> void {
        deleteTargets.clear();

        const auto repo = AppEntity::VesselRepo::GetInstance();
        QList<AppEntity::Vessel::Pointer> list;

        for (const auto& vessel : original) {
            auto newVessel = std::make_shared<AppEntity::Vessel>();

            newVessel->SetModel(vessel.model);
            newVessel->SetName(vessel.name);
            newVessel->SetNA(vessel.na);
            newVessel->SetAFOffset(vessel.afOffset);
            newVessel->SetSize(vessel.w, vessel.h);
            newVessel->SetAFOffset(vessel.afOffset);
            newVessel->SetMultiDish(vessel.useMultiDishHolder);

            newVessel->SetWellCount(vessel.wellProp.rows, vessel.wellProp.cols);
            newVessel->SetWellShape(vessel.wellProp.shape);
            newVessel->SetWellSize(vessel.wellProp.w, vessel.wellProp.h);
            newVessel->SetWellSpacing(vessel.wellProp.spacingH, vessel.wellProp.spacingV);

            for (const auto& w : vessel.wells) {
                newVessel->AddWell(w.rowIdx, w.colIdx, w.x, w.y, w.label);
            }

            newVessel->SetImagingAreaShape(vessel.imgArea.shape);
            newVessel->SetImagingAreaPosition(vessel.imgArea.x, vessel.imgArea.y);
            newVessel->SetImagingAreaSize(vessel.imgArea.w, vessel.imgArea.h);

            list.push_back(newVessel);
        }

        repo->SetVessels(list);
    }

    auto VesselPageControl::Impl::ConvertWellShape(AppEntity::WellShape shape) -> TC::WellShape {
        auto wellShape = TC::WellShape::Circle;
        if (shape == +AppEntity::WellShape::Rectangle) {
            wellShape = TC::WellShape::Rectangle;
        }

        return wellShape;
    }

    auto VesselPageControl::Impl::ConvertImagingAreaShape(AppEntity::ImagingAreaShape shape) -> TC::ImagingAreaShape {
        auto imagingAreaShape = TC::ImagingAreaShape::Circle;
        if (shape == +AppEntity::ImagingAreaShape::Rectangle) {
            imagingAreaShape = TC::ImagingAreaShape::Rectangle;
        }

        return imagingAreaShape;
    }

    auto VesselPageControl::Impl::ConvertVessel(const AppEntity::Vessel::Pointer& entity) -> VesselDataStructure::Vessel {
        VesselDataStructure::Vessel vessel;

        vessel.model = entity->GetModel();
        vessel.name = entity->GetName();
        vessel.na = entity->GetNA();
        vessel.afOffset = entity->GetAFOffset();
        vessel.w = entity->GetSizeX();
        vessel.h = entity->GetSizeY();
        vessel.useMultiDishHolder = entity->IsMultiDish();
        vessel.imgArea.shape = entity->GetImagingAreaShape();
        vessel.imgArea.x = entity->GetImagingAreaPositionX();
        vessel.imgArea.y = entity->GetImagingAreaPositionY();
        vessel.imgArea.w = entity->GetImagingAreaSizeX();
        vessel.imgArea.h = entity->GetImagingAreaSizeY();

        const auto wellWidth = entity->GetWellSizeX();
        const auto wellHeight = entity->GetWellSizeY();
        const auto wellRows = entity->GetWellRows();
        const auto wellCols = entity->GetWellCols();
        const auto spacingH = entity->GetWellSpacingHorizontal();
        const auto spacingV = entity->GetWellSpacingVertical();

        vessel.wellProp.shape = entity->GetWellShape();
        vessel.wellProp.w = wellWidth;
        vessel.wellProp.h = wellHeight;
        vessel.wellProp.rows = wellRows;
        vessel.wellProp.cols = wellCols;
        vessel.wellProp.spacingH = spacingH;
        vessel.wellProp.spacingV = spacingV;

        QList<VesselDataStructure::Well> wells;
            for (int row = 0; row < wellRows; row++) {
                for (int col = 0; col < wellCols; col++) {
                    VesselDataStructure::Well well;
                    const auto wellIndex = entity->GetWellIndex(row, col);
                    well.rowIdx = entity->GetWellRowIndex(wellIndex);
                    well.colIdx = entity->GetWellColIndex(wellIndex);
                    well.x = entity->GetWellPosition(wellIndex).toMM().x;
                    well.y = entity->GetWellPosition(wellIndex).toMM().y;
                    well.label = VesselPageUtil::SetDefaultWellName(row, col);
                    wells.push_back(well);
                }
            }

        vessel.wells = wells;
        return vessel;
    }
}
