#pragma once

#include <memory>

#include <QStandardItemModel>
#include <QCoreApplication>

#include <VesselMap.h>

#include "VesselDataStructures.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class VesselPageControl {
        Q_DECLARE_TR_FUNCTIONS(VesselPageControl)

    public:
        VesselPageControl();
        ~VesselPageControl();

        enum ImportRemarkFlag {
            Normal = 1,
            UnknownError = 2,
            InvalidModelName = 4,
            InvalidNickname = 8,
            DuplicateModelName = 16,
            DuplicateNickname = 32,
            DuplicateModelBetweenFiles = 64,
            DuplicateNameBetweenFiles = 128,
            HasExtraInfo = 256
        };
        Q_DECLARE_FLAGS(ImportResultFlags, ImportRemarkFlag)

        struct ImportResult {
            QString modelName{};
            QString nickname{};
            bool result{}; // success: true, fail: fail
            ImportResultFlags flags{};
            QString extraInfo{};
        };

        auto SetVesselFolderPath(const QString& vesselFolderPath) -> void;

        auto Initialize() -> void;
        auto SetVessels() -> bool;

        auto Delete() -> bool;
        auto GetVessels() const -> VesselDataStructure::Vessels&;
        auto IsModified() const -> bool;

        // save to temp vessel container
        auto AddVessel(const VesselDataStructure::Vessel& newVessel) -> void;
        auto EditVessel(const QString& editTargetModelName, const VesselDataStructure::Vessel& editedVessel) -> void;
        auto DeleteVessel(const QString& deleteTargetModelName) -> void;
        auto ImportVessels(QMap<QString, ImportResult>& importResults) -> VesselDataStructure::Vessels;

        auto GetTempVessel(const QString& model, VesselDataStructure::Vessel& vessel) -> bool;
        auto GetTempVessels() const -> VesselDataStructure::Vessels&;

        auto Convert(const VesselDataStructure::Vessel& vessel) const -> std::shared_ptr<TC::VesselMap>;

        auto GetNAs() const -> QList<double>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

Q_DECLARE_OPERATORS_FOR_FLAGS(HTXpress::AppPlugins::System::Setup::App::VesselPageControl::ImportResultFlags)