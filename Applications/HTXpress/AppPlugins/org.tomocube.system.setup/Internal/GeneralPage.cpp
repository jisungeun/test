#define LOGGER_TAG "[Preference-General]"

#include <QFileDialog>
#include <QComboBox>
#include <QTimer>

#include <MessageDialog.h>
#include <InputDialog.h>
#include <LicenseDialog.h>
#include <TCLogger.h>

#include "GeneralPageControl.h"
#include "GeneralPage.h"
#include "ui_GeneralPage.h"
 
namespace HTXpress::AppPlugins::System::Setup::App {
    struct GeneralPage::Impl {
        Ui::GeneralPage ui;
        GeneralPageControl control;

        auto ApplyStyleSheet(const Self* self) -> void;
        auto UpdateGUI(const GeneralPageControl::Config& config) -> void;
        auto UpdateConfig() const -> GeneralPageControl::Config;
    };

    auto GeneralPage::Impl::ApplyStyleSheet(const Self* self) -> void {
        // base widget
        ui.baseWidget->setObjectName("panel");

        // button
        for(const auto& button : self->findChildren<QPushButton*>()) {
            if(button->isCheckable()) {
                button->setObjectName("bt-setup-toggle");
            }
            else {
                button->setObjectName("bt-setup-light");
            }
        }

        // label
        for(const auto& label : self->findChildren<QLabel*>()) {
            if(label->objectName().contains("Title", Qt::CaseSensitive)) {
                label->setObjectName("label-h3");
            }
            else if(label->objectName().contains("Subtitle")) {
                label->setObjectName("label-h5");
            }
        }

        // line divider
        for (const auto& line : self->findChildren<QFrame*>()) {
            if (line->frameShape() == QFrame::VLine) {
                line->setObjectName("line-divider");
                line->setFixedWidth(1);
            }
            else if (line->frameShape() == QFrame::HLine) {
                line->setObjectName("line-divider");
                line->setFixedHeight(1);
            }
        }
    }

    auto GeneralPage::Impl::UpdateGUI(const GeneralPageControl::Config& config) -> void {
        ui.dataFolderEdit->setText(config.dataFolder);
        ui.minSpaceSpinBox->setValue(config.minSpace);
    }

    auto GeneralPage::Impl::UpdateConfig() const -> GeneralPageControl::Config {
        GeneralPageControl::Config config;

        config.dataFolder = ui.dataFolderEdit->text();
        config.minSpace = ui.minSpaceSpinBox->value();

        return config;
    }

    GeneralPage::GeneralPage(QWidget* parent) : ISetupPage(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ApplyStyleSheet(this);

        d->ui.dataFolderEdit->setReadOnly(true);
        d->ui.minSpaceSpinBox->setRange(100, INT32_MAX);
        d->ui.minSpaceSpinBox->setSingleStep(10);

        connect(d->ui.changeDataFolderBtn, &QAbstractButton::clicked, this, &Self::onChangeDataFolder);
        connect(d->ui.btnLicense, &QAbstractButton::clicked, this, &Self::onShowLicenseDialog);
    }

    GeneralPage::~GeneralPage() {
    }

    auto GeneralPage::Initialize() -> bool {
        d->control.Initialize();
        const auto config = d->control.GetConfig();
        d->UpdateGUI(config);

        d->ui.tabWidget->setCurrentIndex(0);

        d->ui.changeDataFolderBtn->setEnabled(IsAdmin());
        d->ui.minSpaceSpinBox->setEnabled(IsAdmin());
        d->ui.btnLicense->setEnabled(IsAdmin());

        return true;
    }

    auto GeneralPage::IsModified() const -> bool {
        const auto config = d->UpdateConfig();
        return d->control.IsModified(config);
    }

    auto GeneralPage::IsRestorable() const -> bool {
        return true;
    }

    auto GeneralPage::Restore() -> bool {
        const auto config = d->control.GetConfig();
        d->UpdateGUI(config);
        return true;
    }

    auto GeneralPage::Save() -> bool {
        const auto config = d->UpdateConfig();
        return d->control.SetConfig(config);
    }

    void GeneralPage::onChangeDataFolder() {
        const auto currentPath = d->ui.dataFolderEdit->text();
        const auto path = QFileDialog::getExistingDirectory(this, tr("Select a folder to save data"), currentPath);
        if (path == currentPath || path.isEmpty()) { return; }

        if(!d->control.IsUsedDataFolder(path)) {
            QDir dir(path);
            const auto entryList = dir.entryList(QDir::AllEntries | QDir::Hidden | QDir::System | QDir::NoDotAndDotDot);
            if (!entryList.isEmpty()) {
                TC::MessageDialog::warning(this, tr("Data folder"), tr("Select an empty folder"));
                return;
            }
        }

        d->ui.dataFolderEdit->setText(path);
    }

    void GeneralPage::onShowLicenseDialog() {
        bool okBtnClicked{};
        const auto password = TC::InputDialog::getText(this, 
                                                       tr("Password"), 
                                                       tr("Enter your password to open license deactivator"), 
                                                       QLineEdit::EchoMode::Password, {}, &okBtnClicked);

        if (!okBtnClicked) {
            return;
        }

        if (!d->control.IsCorrectPassword(password)) {
            TC::MessageDialog::warning(this, tr("Failed to open"), tr("Wrong password"));
            return;
        }

        LicenseDialog licenseDialog;
        licenseDialog.SetCloseOnDeactivation(true);
        licenseDialog.exec();

        if (d->control.IsLicenseDeactivated()) {
            TC::MessageDialog messageBox(tr("Program closing"),
                                         tr("This program will close in a few seconds because the license has been deactivated."),
                                         this);

            messageBox.SetStandardButtons(QDialogButtonBox::Ok);
            messageBox.SetDefaultButton(QDialogButtonBox::Ok);

            connect(&messageBox, &TC::MessageDialog::accepted, this, []() {
                QLOG_INFO() << "The program has closed because the license has been deactivated.";
                QApplication::quit();
                });

            QTimer::singleShot(3000, &messageBox, &TC::MessageDialog::accept);

            messageBox.exec();
        }
    }
}
