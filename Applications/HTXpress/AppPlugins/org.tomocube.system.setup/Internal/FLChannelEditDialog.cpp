﻿#include <QPushButton>
#include <QColor>
#include <QColorDialog>
#include <QMouseEvent>

#include <FileNameValidator.h>
#include <MessageDialog.h>

#include "FLChannelEditDialog.h"
#include "ui_FLChannelEditDialog.h"

// 상당 부분을 TCQtWidgets_API InputDialog 참조하여 수정함
namespace HTXpress::AppPlugins::System::Setup::App {
    struct FLChannelEditDialog::Impl {
        QFrame* frame = nullptr;
        Ui::FLChannelEditDialog* ui{nullptr};
        const int32_t margin = 50;

        QStringList existNames{};

        int32_t index{};
        QColor color{};

        auto UpdateColor(const QColor& color) -> void; // tooltip, stylesheet
    };

    FLChannelEditDialog::FLChannelEditDialog(int32_t index, QWidget* parent) : CustomDialog(parent), d{new Impl} {
        d->frame = new QFrame(this);
        d->ui = new Ui::FLChannelEditDialog();
        d->ui->setupUi(d->frame);
		SetContext(d->frame);

        d->index = index;
        SetStandardButtons(StandardButton::Ok | StandardButton::Close);
        SetDefaultButton(StandardButton::Ok);

        d->ui->nameEdit->setObjectName("tc-setupdialog-lineedit");
        d->ui->excitationCombo->setObjectName("tc-setupdialog-combobox");
        d->ui->emissionCombo->setObjectName("tc-setupdialog-combobox");

        d->ui->nameEdit->setMinimumWidth(130);
        d->ui->colorBtn->setObjectName("bt-setup-light");
        
        for(const auto& label : this->findChildren<QLabel*>()) {
            if(label->objectName().contains("itemLabel")) {
                label->setObjectName("label-h3");
            }
        }

        d->ui->colorBtn->setToolTip("Press here to change the color.");
        d->ui->colorBtn->setStyleSheet(QString("QPushButton{background-color:rgb(%1,%2,%3);}").arg(d->color.red()).arg(d->color.green()).arg(d->color.blue()));

        connect(d->ui->colorBtn, &QPushButton::clicked, this, [this] {
            const auto newColor = QColorDialog::getColor(d->color, this, "Colors");
            if (newColor.isValid()) {
                d->UpdateColor(newColor);
            }
        });

        d->ui->nameEdit->setFocus();

        this->installEventFilter(this);
    }

    FLChannelEditDialog::~FLChannelEditDialog() {
        delete d->ui;
    }

    auto FLChannelEditDialog::SetExcitationList(const QStringList& list) -> void {
        d->ui->excitationCombo->addItems(list);
    }

    auto FLChannelEditDialog::SetEmissionList(const QStringList& list) -> void {
        d->ui->emissionCombo->addItems(list);
    }

    auto FLChannelEditDialog::SetCurrentExistNames(const QStringList& names) -> void {
        // TODO 중복 채널 이름 허용 안될 경우 추가 구현
        d->existNames = names;
    }

    auto FLChannelEditDialog::GetIndex() const -> int32_t {
        return d->index;
    }

    auto FLChannelEditDialog::GetName() const -> QString {
        return d->ui->nameEdit->text();
    }

    auto FLChannelEditDialog::GetExcitation() -> QString {
        return d->ui->excitationCombo->currentText();
    }

    auto FLChannelEditDialog::GetEmission() -> QString {
        if (d->ui->emissionCombo->currentText() == "void") return "0/0";
        return d->ui->emissionCombo->currentText();
    }

    auto FLChannelEditDialog::GetColor() const -> QColor {
        return d->color;
    }

    auto FLChannelEditDialog::eventFilter(QObject* watched, QEvent* event) -> bool {
        if (event->type() == QEvent::KeyPress) {
            const auto keyEvent = dynamic_cast<QKeyEvent*>(event);
            switch (keyEvent->key()) {
                case Qt::Key_Return:
                case Qt::Key_Enter: {
                    if (focusWidget() == d->ui->nameEdit || 
                        focusWidget() == d->ui->emissionCombo || 
                        focusWidget() == d->ui->excitationCombo) {
                        accept();
                    }
                    return true;
                }
                default: ;
            }
        }
        return QDialog::eventFilter(watched, event);
    }

    int FLChannelEditDialog::GetMinimumWidth() const {
        return d->frame->minimumWidth();
    }

    void FLChannelEditDialog::accept() {
        if (d->ui->nameEdit->text().isEmpty() || d->ui->excitationCombo->currentText().isEmpty() || d->ui->emissionCombo->currentText().isEmpty()) {
            TC::MessageDialog::warning(this, 
                                       tr("Fill in the blank"), 
                                       tr("All fileds must be filled!"));
            d->ui->nameEdit->setFocus();
            return;
        }

        if (d->existNames.contains(d->ui->nameEdit->text())) {
            TC::MessageDialog::warning(this, 
                                       tr("Name duplication"), 
                                       tr("\'%1\' name is already existed.").arg(d->ui->nameEdit->text()));
            d->ui->nameEdit->setFocus();
            return;
        }

        QDialog::accept();
    }

    void FLChannelEditDialog::reject() {
        QDialog::reject();
    }

    auto FLChannelEditDialog::SetCurrentData(const QString& name, const QString& ex, const QString& em, const QColor& color) -> void {
        d->ui->nameEdit->setText(name);
        d->ui->excitationCombo->setCurrentIndex(d->ui->excitationCombo->findText(ex));
        d->ui->emissionCombo->setCurrentIndex(d->ui->emissionCombo->findText(em));
        d->UpdateColor(color);
    }

    auto FLChannelEditDialog::Impl::UpdateColor(const QColor& newColor) -> void {
        color = newColor;
        ui->colorBtn->setStyleSheet(QString("QPushButton{background-color:rgb(%1,%2,%3)};").arg(color.red()).arg(color.green()).arg(color.blue()));
    }
}
