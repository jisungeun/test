#include <System.h>
#include <ConfigController.h>
#include <FLConfigController.h>

#include "FluorescencePageControl.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct FluorescencePageControl::Impl {
        Config original;

        auto Restore() -> void;
        auto Store() -> void;
    };

    auto FluorescencePageControl::Impl::Restore() -> void {
        const auto sysConfig = AppEntity::System::GetSystemConfig();

        original.excFilter = sysConfig->GetExcitationFilter();

        for (const auto& key : sysConfig->GetFLChannels()) {
            AppEntity::FLChannel channel;
            if (true == sysConfig->GetFLChannel(key, channel)) original.flChannels[key] = channel;
        }

        for (const auto& key : sysConfig->GetFLInternalExcitations()) {
            AppEntity::FLFilter exc;
            if (sysConfig->GetFLInternalExcitation(key, exc)) 
                original.flInternalExcitations[key] = exc;
        }

        for (const auto& key : sysConfig->GetFLExternalExcitations()) {
            AppEntity::FLFilter exc;
            if (true == sysConfig->GetFLExternalExcitation(key, exc)) 
                original.flExternalExcitations[key] = exc;
        }

        for (const auto& key : sysConfig->GetFLEmissions()) {
            AppEntity::FLFilter emission;
            if (true == sysConfig->GetFLEmission(key, emission)) 
                original.flEmissions[key] = emission;
        }
    }

    auto FluorescencePageControl::Impl::Store() -> void {
        const auto sysConfig = AppEntity::System::GetSystemConfig();

        sysConfig->SetExcitationFilter(original.excFilter);
        sysConfig->SetFLChannels(original.flChannels);
        sysConfig->SetFLInternalExcitations(original.flInternalExcitations);
        sysConfig->SetFLExternalExcitations(original.flExternalExcitations);
        sysConfig->SetFLEmissions(original.flEmissions);
    }

    auto FluorescencePageControl::Config::operator=(const Config& rhs) -> Config& {
        excFilter = rhs.excFilter;
        flChannels = rhs.flChannels;
        flInternalExcitations = rhs.flInternalExcitations;
        flExternalExcitations = rhs.flExternalExcitations;
        flEmissions = rhs.flEmissions;

        return *this;
    }

    auto FluorescencePageControl::Config::operator==(const Config& rhs) const -> bool {
        if (excFilter != rhs.excFilter) return false;
        if (flChannels != rhs.flChannels) return false;
        if (flInternalExcitations != rhs.flInternalExcitations) return false;
        if (flExternalExcitations != rhs.flExternalExcitations) return false;
        if (flEmissions != rhs.flEmissions) return false;

        return true;
    }

    auto FluorescencePageControl::Config::operator!=(const Config& rhs) const -> bool {
        return !(*this == rhs);
    }

    FluorescencePageControl::FluorescencePageControl() : d{new Impl} {
    }

    FluorescencePageControl::~FluorescencePageControl() {
    }

    auto FluorescencePageControl::Initalize() -> void {
        d->Restore();
    }

    auto FluorescencePageControl::GetConfig() const -> Config {
        return d->original;
    }

    auto FluorescencePageControl::SetConfig(const Config& config) -> bool {
        d->original = config;
        d->Store();

        auto flConfigController = Interactor::FLConfigController();
        auto configController = Interactor::ConfigController();

        return (flConfigController.Save() && configController.Save());
    }

    auto FluorescencePageControl::IsModified(const Config& config) const -> bool {
        return d->original != config;
    }

    auto FluorescencePageControl::IsValid(const Config& config, const int32_t& index) -> bool {
        const AppEntity::FLFilter excitation = {
            static_cast<int32_t>(config.flChannels[index].GetExcitation()),
            static_cast<int32_t>(config.flChannels[index].GetExcitationBandwidth())
        };
        const AppEntity::FLFilter emission = {
            static_cast<int32_t>(config.flChannels[index].GetEmission()),
            static_cast<int32_t>(config.flChannels[index].GetEmissionBandwidth())
        };

        if (config.excFilter == AppEntity::ExcFilter::External) {
            if (!config.flExternalExcitations.values().contains(excitation)) {
                return false;
            }
        } else {
            if (!config.flInternalExcitations.values().contains(excitation)) {
                return false;
            }
        }

        if (!config.flEmissions.values().contains(emission)) {
            return false;
        }

        return true;
    }
}
