﻿#pragma once

#include <memory>

#include <enum.h>
#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::System::Setup::App {
    struct User {
        AppEntity::UserID id{""};
        QString name{""};
        AppEntity::Profile type{ AppEntity::Profile::Operator };
    };

    using Users = QList<User>;
}
