﻿#include "MediumTableModel.h"

#include <QFontMetrics>

namespace HTXpress::AppPlugins::System::Setup::App {
    struct MediumTableModel::Impl {
        QList<Medium> media;
    };

    MediumTableModel::MediumTableModel(QObject* parent) : QAbstractTableModel(parent), d{std::make_unique<Impl>()} {
    }

    MediumTableModel::~MediumTableModel() {
    }

    auto MediumTableModel::rowCount(const QModelIndex& parent) const -> int {
        Q_UNUSED(parent)
        return d->media.size();
    }

    auto MediumTableModel::columnCount(const QModelIndex& parent) const -> int {
        Q_UNUSED(parent)
        return _NumOfColumns;
    }

    auto MediumTableModel::headerData(int section, Qt::Orientation orientation, int role) const -> QVariant {
        if (role == Qt::DisplayRole) {
            if (orientation == Qt::Horizontal) {
                const auto column = static_cast<Columns>(section);
                switch (column) {
                    case Name: return "Name";
                    case RI: return "RI";
                    default: ;
                }
            }

            if(orientation == Qt::Vertical) {
                return section+1;
            }
        }

        if (role == Qt::TextAlignmentRole) {
            if(orientation == Qt::Vertical) {
                return Qt::AlignCenter;
            }
        }


        return QVariant();
    }

    auto MediumTableModel::data(const QModelIndex& index, int role) const -> QVariant {
        if (!index.isValid()) return QVariant();
        if (index.row() >= d->media.size() || index.row() < 0) return QVariant();

        const auto row = index.row();
        const auto col = static_cast<Columns>(index.column());
        const auto& medium = d->media.at(row);

        if (role == Qt::DisplayRole) {
            switch (col) {
                case Name: {
                    return medium.name;
                }
                case RI: {
                    return QString::number(medium.ri, 'f', 4);
                }
                default: ;
            }
        }

        if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }

        return QVariant();
    }

    auto MediumTableModel::setData(const QModelIndex& index, const QVariant& value, int role) -> bool {
        if (index.isValid() && role == Qt::EditRole) {
            const auto row = index.row();
            const auto col = static_cast<Columns>(index.column());
            auto medium = d->media.value(row);

            switch (col) {
                case Name: {
                    medium.name = value.toString();
                }
                break;
                case RI: {
                    medium.ri = value.toDouble();
                }
                break;
                default: break;
            }

            d->media.replace(row, medium);
            emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});

            return true;
        }

        return false;
    }

    auto MediumTableModel::insertRows(int row, int count, const QModelIndex& parent) -> bool {
        Q_UNUSED(parent)

        beginInsertRows(QModelIndex(), row + count - 1, row);
        for (int r = count; r > 0; --r) {
            d->media.insert(row, {"", 0.0});
        }
        endInsertRows();
        return true;
    }

    auto MediumTableModel::removeRows(int row, int count, const QModelIndex& parent) -> bool {
        Q_UNUSED(parent)

        beginRemoveRows(QModelIndex(), row, row + count - 1);
        for (int r = 0; r < count; ++r) {
            d->media.removeAt(row);
        }
        endRemoveRows();
        return true;
    }

    auto MediumTableModel::flags(const QModelIndex& index) const -> Qt::ItemFlags {
        if(!index.isValid()) return Qt::NoItemFlags;
        return QAbstractTableModel::flags(index);
    }

    auto MediumTableModel::GetMedia() const -> const QList<Medium>& {
        return d->media;
    }
}
