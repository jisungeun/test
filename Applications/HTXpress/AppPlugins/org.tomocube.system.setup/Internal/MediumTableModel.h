﻿#pragma once

#include <memory>

#include <QAbstractTableModel>

namespace HTXpress::AppPlugins::System::Setup::App {
    class MediumTableModel : public QAbstractTableModel {
        Q_OBJECT
    public:
        enum Columns { Name = 0, RI, _NumOfColumns };

        struct Medium {
            QString name{""};
            double ri{0.0};

            auto operator==(const Medium& other) const -> bool {
                return (name == other.name && fabs(ri - other.ri) < std::numeric_limits<double>::epsilon());
            }
        };

        using Self = MediumTableModel;
        using Pointer = std::shared_ptr<Self>;

        explicit MediumTableModel(QObject* parent = nullptr);
        ~MediumTableModel() override;

        auto rowCount(const QModelIndex& parent = QModelIndex()) const -> int override;
        auto columnCount(const QModelIndex& parent = QModelIndex()) const -> int override;
        auto headerData(int section, Qt::Orientation orientation, int role) const -> QVariant override;
        auto data(const QModelIndex& index, int role) const -> QVariant override;
        auto setData(const QModelIndex& index, const QVariant& value, int role) -> bool override;

        auto insertRows(int row, int count, const QModelIndex& parent) -> bool override;
        auto removeRows(int row, int count, const QModelIndex& parent) -> bool override;

        auto flags(const QModelIndex& index) const -> Qt::ItemFlags override;

        auto GetMedia() const -> const QList<Medium>&;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
