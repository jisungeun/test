#pragma once
#include <memory>
#include <QList>
#include <User.h>

namespace HTXpress::AppPlugins::System::Setup::App {
    class UserPageControl {
    public:
        using Users = QList<AppEntity::User::Pointer>;

    public:
        UserPageControl();
        ~UserPageControl();

        auto AddUser(const AppEntity::User::Pointer& newUser, const QString& password) -> bool;
        auto EditUser(const AppEntity::User::Pointer& editedUser) -> bool;
        auto DeleteUser(const AppEntity::UserID& id) -> bool;
        auto ChangePassword(const AppEntity::UserID& id, const QString& pw) -> bool;
        auto GetUsers() const -> Users;
        auto GetUserIDs() const -> QStringList;
        auto IsValidAdmin(const QString& password)->bool;
        auto IsBuiltInAccount(const QString& id) const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
