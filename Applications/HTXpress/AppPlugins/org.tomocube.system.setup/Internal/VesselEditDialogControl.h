﻿#pragma once

#include <memory>

#include "VesselDataStructures.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class VesselEditDialogControl {
    public:
        VesselEditDialogControl();
        ~VesselEditDialogControl();

        auto SetShape(AppEntity::WellShape shape) -> void;
        auto GetShape() const -> AppEntity::WellShape;

        auto SetSize(double w, double h) -> void;
        auto GetWidth() const -> double;
        auto GetHeight() const -> double;

        auto SetRows(int32_t rows) -> void;
        auto GetRows() const -> int32_t;

        auto SetColumns(int32_t cols) -> void;
        auto GetColumns() const -> int32_t;

        auto SetSpacingMode(SpacingMode mode) -> void;
        auto GetSpacingMode() const -> SpacingMode;

        auto SetSpacingHorizontal(double spacingRow) -> void;
        auto GetSpacingHorizontal() const -> double;

        auto SetSpacingVertical(double spacingCol) -> void;
        auto GetSpacingVertical() const -> double;

        auto SetStartX(double x) -> void;
        auto GetStartX() const -> double;

        auto SetStartY(double y) -> void;
        auto GetStartY() const -> double;

        auto SetWells(const QList<VesselDataStructure::Well>& wells) -> void;
        auto GetWells() const -> QList<VesselDataStructure::Well>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
