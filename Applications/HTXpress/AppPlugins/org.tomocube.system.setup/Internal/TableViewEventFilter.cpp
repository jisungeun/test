﻿#include <QMouseEvent>
#include <QEvent>

#include "TableViewEventFilter.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct TableViewEventFilter::Impl {
        QTableView* table{nullptr};
    };

    TableViewEventFilter::TableViewEventFilter(QTableView* table, QObject* parent) : QObject(parent), d{std::make_unique<Impl>()} {
        d->table = table;
    }

    TableViewEventFilter::~TableViewEventFilter() {
    }

    bool TableViewEventFilter::eventFilter(QObject* watched, QEvent* event) {
        if (d->table == nullptr) return QObject::eventFilter(watched, event);

        if (event->type() == QEvent::MouseButtonPress) {
            auto mouseEvent = static_cast<QMouseEvent*>(event);
            if (!d->table->indexAt(mouseEvent->pos()).isValid()) {
                d->table->setCurrentIndex(QModelIndex());
            }
            return false;
        }

        return false;
    }
}
