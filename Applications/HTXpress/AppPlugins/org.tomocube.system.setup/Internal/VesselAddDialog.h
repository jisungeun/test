﻿#pragma once

#include <memory>

#include "CustomDialog.h"

#include "VesselDataStructures.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class VesselAddDialog final : public TC::CustomDialog {
        Q_OBJECT
    public:
        using Self = VesselAddDialog;

        VesselAddDialog(const QStringList& models, 
                        const QStringList& nicknames, 
                        const QList<double>& NAs,
                        QWidget* parent = nullptr);
        ~VesselAddDialog() override;

        auto Vessel() const -> VesselDataStructure::Vessel;

    public slots:
        void accept() override;
        void reject() override;

    protected:
        auto GetMinimumWidth() const -> int override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
