﻿#include <MessageDialog.h>
#include <QMouseEvent>
#include <QAbstractButton>
#include <QPushButton>

#include <FileNameValidator.h>

#include "MediumEditDialog.h"
#include "ui_MediumEditDialog.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct MediumEditDialog::Impl {
        QFrame* frame = nullptr;
        Ui::MediumEditDialog* ui{nullptr};
        const int32_t margin = 50;

        QStringList existNames{};

        auto SetTitle(const QString& title, Self* self) -> void;
    };

    auto MediumEditDialog::Impl::SetTitle(const QString& title, Self* self) -> void {
        self->SetTitle(title);
    }

    MediumEditDialog::MediumEditDialog(QWidget* parent) : CustomDialog(parent), d{new Impl} {
        d->frame = new QFrame(this);
        d->ui = new Ui::MediumEditDialog;
        d->ui->setupUi(d->frame);
		SetContext(d->frame);
        
        d->SetTitle(QString("Edit Fluorescene Channel"), this);
        SetStandardButtons(StandardButton::Ok | StandardButton::Close);
        SetDefaultButton(StandardButton::Ok);

        d->ui->name->setObjectName("tc-setupdialog-lineedit");
        d->ui->ri->setObjectName("tc-setupdialog-spinbox");
        d->ui->ri->setRange(0.0000, 9.9999);

        for(const auto& label : this->findChildren<QLabel*>()) {
            if(label->objectName().contains("itemLabel")) {
                label->setObjectName("label-h3");
            }
        }

        this->installEventFilter(this);
    }

    MediumEditDialog::~MediumEditDialog() {
        delete d->ui;
    }

    auto MediumEditDialog::SetCurrentExistNames(const QStringList& names) -> void {
        d->existNames = names;
    }

    auto MediumEditDialog::SetName(const QString& name) -> void {
        d->ui->name->setText(name);
    }

    auto MediumEditDialog::GetName() const -> QString {
        return d->ui->name->text();
    }

    auto MediumEditDialog::SetRI(double ri) -> void {
        d->ui->ri->setValue(ri);
    }

    auto MediumEditDialog::GetRI() const -> double {
        return d->ui->ri->value();
    }

    bool MediumEditDialog::eventFilter(QObject* watched, QEvent* event) {
        if (event->type() == QEvent::KeyPress) {
            const auto keyEvent = dynamic_cast<QKeyEvent*>(event);
            switch (keyEvent->key()) {
                case Qt::Key_Return:
                case Qt::Key_Enter: {
                    if (focusWidget() == d->ui->name || 
                        focusWidget() == d->ui->ri) {
                        accept();
                    }
                    return true;
                }
                default: ;
            }
        }
        return CustomDialog::eventFilter(watched, event);
    }

    int MediumEditDialog::GetMinimumWidth() const {
        return d->frame->minimumWidth();
    }

    void MediumEditDialog::accept() {
        if(d->ui->name->text().isEmpty()) {
            TC::MessageDialog::warning(this, tr("Warning"), tr("Fill in the name field."));
            return;
        }

        if (d->existNames.contains(d->ui->name->text())) {
            TC::MessageDialog::warning(this, 
                                       tr("Name duplication"), 
                                       tr("\'%1\' name is already existed.").arg(d->ui->name->text()));
            d->ui->name->setFocus();
            return;
        }
        
        CustomDialog::accept();
    }

    void MediumEditDialog::reject() {
        CustomDialog::reject();
    }
}
