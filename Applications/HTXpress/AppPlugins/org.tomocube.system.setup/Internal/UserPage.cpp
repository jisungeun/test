#include <InputDialog.h>
#include <MessageDialog.h>
#include <UserEditDialog.h>

#include "TableViewEventFilter.h"
#include "UserTableModel.h"
#include "UserDefines.h"
#include "UserPageControl.h"
#include "ui_UserPage.h"
#include "UserPage.h"

#include "InputDialog.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct UserPage::Impl {
        Ui::UserPage ui;
        UserPageControl control;
        UserTableModel model;
        TableViewEventFilter* tableEventFilter{nullptr};
        auto UpdateGUI(const UserPageControl::Users& users) -> void;
        auto ApplyStyleSheet(const Self* self) -> void;
    };

    auto UserPage::Impl::UpdateGUI(const UserPageControl::Users& users) -> void {
        if (model.rowCount() != 0) model.removeRows(0, model.rowCount(), QModelIndex());

        using Column = UserTableModel::Columns;
        for (const auto& user : users) {
            model.insertRows(model.rowCount(), 1, QModelIndex());
            auto rowIndex = model.rowCount() - 1;

            auto index = model.index(rowIndex, Column::ID);
            model.setData(index, user->GetUserID(), Qt::EditRole);

            index = model.index(rowIndex, Column::Name);
            model.setData(index, user->GetName(), Qt::EditRole);

            index = model.index(rowIndex, Column::Type);
            model.setData(index, user->GetProfile()._to_string(), Qt::EditRole);
        }
    }

    auto UserPage::Impl::ApplyStyleSheet(const Self* self) -> void {
        ui.table->setStyleSheet(QString("QTableView{border-bottom: 0px;}"));
        ui.table->verticalHeader()->setStyleSheet(QString("QHeaderView{border-bottom: 0px;}"));

        ui.titleLabel->setObjectName("label-h2");
        for(const auto& button : self->findChildren<QPushButton*>()) {
            if(button->isCheckable()) {
                button->setObjectName("bt-setup-toggle");
            }
            else {
                button->setObjectName("bt-setup-light");
            }
        }
    }

    UserPage::UserPage(QWidget* parent) : ISetupPage(parent), d{new Impl} {
        d->ui.setupUi(this);
        d->ui.table->setModel(&d->model);
        d->ui.table->setSelectionBehavior(QAbstractItemView::SelectRows);
        d->ui.table->setEditTriggers(QAbstractItemView::NoEditTriggers);
        d->ui.table->setSelectionMode(QAbstractItemView::SingleSelection);
        d->ui.table->horizontalHeader()->setMinimumWidth(150);

        connect(d->ui.registerBtn, &QPushButton::clicked, this, &Self::onAddUser);
        connect(d->ui.deleteBtn, &QPushButton::clicked, this, &Self::onDeleteUser);
        connect(d->ui.editBtn, &QPushButton::clicked, this, &Self::onEditUser);

        d->ApplyStyleSheet(this);
    }

    UserPage::~UserPage() {
    }

    auto UserPage::Initialize() -> bool {
        d->UpdateGUI(d->control.GetUsers());

        d->ui.registerBtn->setEnabled(IsAdmin());
        d->ui.editBtn->setEnabled(IsAdmin());
        d->ui.deleteBtn->setEnabled(IsAdmin());

        return true;
    }

    auto UserPage::IsModified() const -> bool {
        return false;
    }

    auto UserPage::IsRestorable() const -> bool {
        return false;
    }

    auto UserPage::Restore() -> bool {
        return true;
    }

    auto UserPage::Save() -> bool {
        return true;
    }

    void UserPage::onAddUser() {
        using Dialog = HTXpress::AppComponents::UserEdit::Dialog;

        Dialog dialog(this);
        dialog.SetExistIDs(d->control.GetUserIDs());
        if(QDialog::Accepted != dialog.exec()) return;

        auto user = std::make_shared<AppEntity::User>();
        user->SetName(dialog.GetName());
        user->SetUserID(dialog.GetID());
        user->SetProfile(dialog.GetPrivilege());
        auto password = dialog.GetPassword();

        d->control.AddUser(user, password);

        d->UpdateGUI(d->control.GetUsers());
    }

    void UserPage::onEditUser() {
        using Column = UserTableModel::Columns;
        using Dialog = HTXpress::AppComponents::UserEdit::Dialog;

        auto selectedRows = d->ui.table->selectionModel()->selectedRows(Column::ID);
        if(selectedRows.isEmpty()) return;
        auto id = selectedRows.value(0).data().toString();

        if(d->control.IsBuiltInAccount(id)) {
            TC::MessageDialog::warning(this, tr("Access Denied"), tr("'%1' cannot be edited because it is a built-in account.").arg(id));
            return;
        }

        selectedRows = d->ui.table->selectionModel()->selectedRows(Column::Name);
        if(selectedRows.isEmpty()) return;
        auto name = selectedRows.value(0).data().toString();

        selectedRows = d->ui.table->selectionModel()->selectedRows(Column::Type);
        if(selectedRows.isEmpty()) return;
        auto profile = AppEntity::Profile::_from_string(selectedRows.value(0).data().toString().toLatin1());

        Dialog dialog(this, Dialog::Mode::Edit);
        dialog.SetID(id);
        dialog.SetName(name);
        dialog.SetPrivilege(profile);

        if(QDialog::Accepted != dialog.exec()) return;

        auto user = std::make_shared<AppEntity::User>();
        user->SetName(dialog.GetName());
        user->SetUserID(dialog.GetID());
        user->SetProfile(dialog.GetPrivilege());

        d->control.EditUser(user);

        auto password = dialog.GetPassword();
        if(!password.isEmpty()) {
            d->control.ChangePassword(user->GetUserID(), password);
        }

        d->UpdateGUI(d->control.GetUsers());
    }

    void UserPage::onDeleteUser() {
        using Column = UserTableModel::Columns;

        auto selectedRows = d->ui.table->selectionModel()->selectedRows(Column::ID);
        if(selectedRows.isEmpty()) return;

        auto id = selectedRows.value(0).data().toString();
        bool accepted{};

        if(d->control.IsBuiltInAccount(id)) {
            TC::MessageDialog::warning(this, tr("Access Denied"), tr("'%1' cannot be deleted because it is a built-in account.").arg(id));
            return;
        }

        auto password = TC::InputDialog::getText(this, tr("Delete User"),
                                                 tr("Enter your password to delete %1").arg(id),
                                                 QLineEdit::EchoMode::Password,
                                                 {},
                                                 &accepted);

        if(!accepted) {
            return;
        }

        if(!d->control.IsValidAdmin(password)) {
            TC::MessageDialog::warning(this, tr("Delete User"), tr("Wrong password"));
            return;
        }

        if(!d->control.DeleteUser(id)) {
            TC::MessageDialog::warning(this, tr("Delete User"), tr("It fails to delete %1").arg(id));
            return;
        }

        d->UpdateGUI(d->control.GetUsers());
    }
}
