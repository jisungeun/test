﻿#include <QModelIndex>

#include "EachWellSetupTableModel.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct EachWellSetupTableModel::Impl {
        QList<WellTableData> wells;
    };

    EachWellSetupTableModel::EachWellSetupTableModel(QObject* parent) : QAbstractTableModel(parent), d{std::make_unique<Impl>()} {
    }

    EachWellSetupTableModel::~EachWellSetupTableModel() {
    }

    auto EachWellSetupTableModel::rowCount(const QModelIndex& parent) const -> int {
        Q_UNUSED(parent)
        return d->wells.size();
    }

    auto EachWellSetupTableModel::columnCount(const QModelIndex& parent) const -> int {
        Q_UNUSED(parent)
        return _NumOfColumns;
    }

    auto EachWellSetupTableModel::data(const QModelIndex& index, int role) const -> QVariant {
        if (!index.isValid()) return QVariant();
        if (index.row() >= d->wells.size() || index.row() < 0) return QVariant();

        const auto row = index.row();
        const auto col = static_cast<Columns>(index.column());
        const auto& well = d->wells.at(row);

        if (role == Qt::DisplayRole) {
            switch (col) {
                case RowIndex: return QString::number(well.row+1);
                case ColumnIndex: return QString::number(well.col+1);
                case Name: return well.name;
                case X: return QString::number(well.x, 'f', 3);
                case Y: return QString::number(well.y, 'f', 3);
                default: ;
            }
        }

        if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }

        return QVariant();
    }

    auto EachWellSetupTableModel::headerData(int section, Qt::Orientation orientation, int role) const -> QVariant {
        if (role == Qt::DisplayRole) {
            if (orientation == Qt::Horizontal) {
                const auto col = static_cast<Columns>(section);
                switch (col) {
                    case RowIndex: return "Row";
                    case ColumnIndex: return "Column";
                    case Name: return "Name";
                    case X: return "X";
                    case Y: return "Y";
                    default: ;
                }
            }

            if (orientation == Qt::Vertical) {
                return section + 1;
            }
        }
        if (role == Qt::TextAlignmentRole) {
            if (orientation == Qt::Vertical) {
                return Qt::AlignCenter;
            }
        }

        return QVariant();
    }

    auto EachWellSetupTableModel::setData(const QModelIndex& index, const QVariant& value, int role) -> bool {
        if (index.isValid() && role == Qt::EditRole) {
            const auto row = index.row();
            const auto col = static_cast<Columns>(index.column());
            auto well = d->wells.value(row);

            switch (col) {
                case RowIndex: well.row = value.toInt();
                    break;
                case ColumnIndex: well.col = value.toInt();
                    break;
                case Name: well.name = value.toString();
                    break;
                case X: well.x = value.toDouble();
                    break;
                case Y: well.y = value.toDouble();
                    break;
                default: ;
            }

            d->wells.replace(row, well);
            emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});

            return true;
        }
        return false;
    }

    auto EachWellSetupTableModel::flags(const QModelIndex& index) const -> Qt::ItemFlags {
        if (!index.isValid()) return Qt::NoItemFlags;
        auto flags = QAbstractTableModel::flags(index);
        if (index.column() == Name || index.column() == X || index.column() == Y) {
            flags |= Qt::ItemIsEditable;
        }
        return flags;
    }

    auto EachWellSetupTableModel::insertRows(int row, int count, const QModelIndex& parent) -> bool {
        Q_UNUSED(parent)

        beginInsertRows(QModelIndex(), row, row + count - 1);
        for (int r = 0; r < count; ++r) d->wells.insert(row, WellTableData());
        endInsertRows();

        return true;
    }

    auto EachWellSetupTableModel::removeRows(int row, int count, const QModelIndex& parent) -> bool {
        Q_UNUSED(parent)

        beginRemoveRows(QModelIndex(), row, row+count-1);
        d->wells.clear();
        endRemoveRows();

        return true;
    }

    auto EachWellSetupTableModel::GetWells() const -> const QList<WellTableData>& {
        return d->wells;
    }
}
