#include <QtSerialPort/QSerialPortInfo>

#include <ModelRepo.h>
#include <System.h>
#include <ConfigController.h>
#include <UserInformationController.h>
#include <SessionManager.h>
#include <LicenseManager.h>

#include "GeneralPageControl.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct GeneralPageControl::Impl {
        Config original;

        auto Restore() -> void;
        auto Store() -> void;
    };

    auto GeneralPageControl::Impl::Restore() -> void {
        auto sysConfig = AppEntity::System::GetSystemConfig();

        original.dataFolder = sysConfig->GetDataDir();
        original.minSpace = sysConfig->GetMinRequiredSpace();
    }

    auto GeneralPageControl::Impl::Store() -> void {
        auto sysConfig = AppEntity::System::GetSystemConfig();

        sysConfig->SetDataDir(original.dataFolder);
        sysConfig->SetMinRequiredSpace(original.minSpace);
    }

    auto GeneralPageControl::Config::operator=(const Config& rhs) -> Config& {
        dataFolder = rhs.dataFolder;
        minSpace = rhs.minSpace;
        return *this;
    }

    auto GeneralPageControl::Config::operator==(const Config& rhs) const -> bool {
        if (dataFolder != rhs.dataFolder) return false;
        if (minSpace != rhs.minSpace) return false;
        return true;
    }

    auto GeneralPageControl::Config::operator!=(const Config& rhs) const -> bool {
        return !(*this == rhs);
    }

    GeneralPageControl::GeneralPageControl() : d{new Impl} {
    }

    GeneralPageControl::~GeneralPageControl() {
    }

    auto GeneralPageControl::Initialize() -> void {
        d->Restore();
    }

    auto GeneralPageControl::GetConfig() const -> Config {
        return d->original;
    }

    auto GeneralPageControl::SetConfig(const Config& config) -> bool {
        d->original = config;
        d->Store();

        auto controller = Interactor::ConfigController();
        return controller.Save();
    }

    auto GeneralPageControl::IsModified(const Config& config) const -> bool {
        return d->original != config;
    }

    auto GeneralPageControl::IsUsedDataFolder(const QString& path) const -> bool {
        auto controller = Interactor::ConfigController();
        return controller.IsUsedDataFolder(path);
    }

    auto GeneralPageControl::IsCorrectPassword(const QString& password) const -> bool {
        const auto session = AppEntity::SessionManager::GetInstance();
        const auto id = session->GetID();

        auto controller = Interactor::UserInformationController();
        return controller.IsCorrectPassword(id, password);
    }

    auto GeneralPageControl::IsLicenseDeactivated() const -> bool {
        return !LicenseManager::CheckLicense();
    }
}
