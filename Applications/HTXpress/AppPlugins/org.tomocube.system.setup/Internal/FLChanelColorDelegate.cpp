﻿#include <QPainter>

#include "FLChanelColorDelegate.h"


namespace HTXpress::AppPlugins::System::Setup::App {
    FLChanelColorDelegate::FLChanelColorDelegate(QObject* parent) : QStyledItemDelegate(parent) {
    }

    FLChanelColorDelegate::~FLChanelColorDelegate() {
    }

    void FLChanelColorDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const {
        if(!index.isValid()) QStyledItemDelegate::paint(painter, option, index);

        const auto color = QColor(index.data(Qt::DisplayRole).toString());
        if(color.isValid()) {
            painter->fillRect(option.rect, color);
        }
    }
}
