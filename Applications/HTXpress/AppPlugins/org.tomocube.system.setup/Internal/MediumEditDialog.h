﻿#pragma once

#include <memory>

#include "CustomDialog.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class MediumEditDialog : public TC::CustomDialog {
        Q_OBJECT
    public:
        using Self = MediumEditDialog;

        explicit MediumEditDialog(QWidget* parent = nullptr);
        ~MediumEditDialog() override;
        
        auto SetCurrentExistNames(const QStringList& names) -> void;

        auto SetName(const QString& name) -> void;
        auto GetName() const -> QString;

        auto SetRI(double ri) -> void;
        auto GetRI() const -> double;

    protected:
        auto eventFilter(QObject* watched, QEvent* event) -> bool override;
        auto GetMinimumWidth() const -> int override;

    public slots:
        void accept() override;
        void reject() override;
    
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
