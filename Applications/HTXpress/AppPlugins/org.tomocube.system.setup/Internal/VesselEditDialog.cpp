﻿#include <QMouseEvent>
#include <QDebug>

#include <MessageDialog.h>

#include "VesselEditDialog.h"
#include "ui_VesselEditDialog.h"
#include "EachWellSetupDialog.h"
#include "VesselEditDialogControl.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct VesselEditDialog::Impl {
        explicit Impl(Self* self) : self(self) {}
        Self* self{};
        QFrame* frame = nullptr;
        Ui::VesselEditDialog ui;
        VesselEditDialogControl control;
        const int32_t margin = 50;

        QStringList nicknames{};

        auto InitValues(const VesselDataStructure::Vessel& vessel) -> void;
        auto GetIndexByNA(const double& na) -> int32_t;

        auto ApplyStyleSheet() -> void;
    };

    VesselEditDialog::VesselEditDialog(const VesselDataStructure::Vessel& vessel, const QStringList& nicknames, const QList<double>& NAs, QWidget* parent)
    : CustomDialog(parent), d{std::make_unique<Impl>(this)} {
        d->frame = new QFrame(this);
        d->ui.setupUi(d->frame);
        SetContext(d->frame);

        d->ui.vesselModel->setReadOnly(true);
        d->ui.wellSetup->setVisible(true);

        for(const auto& na : NAs) {
            d->ui.naCombo->addItem(QString::number(na, 'g', 3), na);
        }

        SetTitle(QString("Edit Vessel (%1)").arg(vessel.model));

        SetStandardButtons(StandardButton::Ok | StandardButton::Cancel);
        SetDefaultButton(StandardButton::Ok);

        connect(d->ui.wellSetup, &QPushButton::clicked, this, &Self::onShowWellSetup);

        d->ui.vesselName->setFocus();
        d->ui.useMultiDishHolder->setEnabled(false);

        d->ApplyStyleSheet();

        d->nicknames = nicknames;

        d->InitValues(vessel);
    }

    VesselEditDialog::~VesselEditDialog() {
    }

    auto VesselEditDialog::Vessel() const -> VesselDataStructure::Vessel {
        VesselDataStructure::Vessel vessel;

        vessel.model = d->ui.vesselModel->text();
        vessel.name = d->ui.vesselName->text();
        vessel.na = d->ui.naCombo->currentData().toDouble();
        vessel.afOffset = d->ui.vesselAFOffset->value();
        vessel.w = d->ui.vesselWidth->value();
        vessel.h = d->ui.vesselHeight->value();
        vessel.useMultiDishHolder = d->ui.useMultiDishHolder->isChecked();
        vessel.imgArea.shape = AppEntity::ImagingAreaShape::_from_integral(d->ui.imgShape->currentIndex());
        vessel.imgArea.x = d->ui.imgX->value();
        vessel.imgArea.y = d->ui.imgY->value();
        vessel.imgArea.w = d->ui.imgWidth->value();
        vessel.imgArea.h = d->ui.imgHeight->value();

        // from control
        const auto wellWidth = d->control.GetWidth();
        const auto wellHeight = d->control.GetHeight();
        const auto wellRows = d->control.GetRows();
        const auto wellCols = d->control.GetColumns();
        const auto spacingH = d->control.GetSpacingHorizontal();
        const auto spacingV = d->control.GetSpacingVertical();

        vessel.wellProp.w = wellWidth;
        vessel.wellProp.h = wellHeight;
        vessel.wellProp.shape = AppEntity::WellShape::_from_integral(d->control.GetShape());
        vessel.wellProp.rows = wellRows;
        vessel.wellProp.cols = wellCols;
        vessel.wellProp.spacingH = spacingH;
        vessel.wellProp.spacingV = spacingV;

        vessel.wells = d->control.GetWells();

        return vessel;
    }

    auto VesselEditDialog::SetMultidishChangeable(bool enable) -> void {
        d->ui.useMultiDishHolder->setEnabled(enable);
    }

    auto VesselEditDialog::accept() -> void {
        if (d->ui.vesselName->text().isEmpty()) {
            TC::MessageDialog::warning(this, "Warning", QString("You must enter the model name."));
            d->ui.vesselName->setFocus();
            return;
        }

        if(d->nicknames.contains(d->ui.vesselName->text())) {
            TC::MessageDialog::warning(this, "Name Duplication", QString("%1 is already exists.\n").arg(d->ui.vesselName->text()));
            d->ui.vesselName->setFocus();
            return;
        }

        if(d->ui.naCombo->currentIndex() == -1) {
            TC::MessageDialog::warning(this, tr("Select NA"), tr("Please select NA"));
            d->ui.naCombo->setFocus();
            d->ui.naCombo->showPopup();
            return;
        }

        CustomDialog::accept();
    }

    void VesselEditDialog::reject() {
        const auto answer = TC::MessageDialog::question(this, "Close", "Do you want to close?",
                                                        StandardButton::Yes | StandardButton::No, StandardButton::Yes);
        if (answer == StandardButton::Yes) CustomDialog::reject();
    }

    void VesselEditDialog::onShowWellSetup() {
        EachWellSetupDialog::Inputs dlgParam;
        dlgParam.shape = d->control.GetShape();
        dlgParam.width = d->control.GetWidth();
        dlgParam.height = d->control.GetHeight();
        dlgParam.wells = d->control.GetWells();
        dlgParam.rows = d->control.GetRows();
        dlgParam.cols = d->control.GetColumns();
        dlgParam.spacingMode = d->control.GetSpacingMode();
        dlgParam.spacingHorizontal = d->control.GetSpacingHorizontal();
        dlgParam.spacingVertical = d->control.GetSpacingVertical();
        dlgParam.startX = d->control.GetStartX();
        dlgParam.startY = d->control.GetStartY();

        EachWellSetupDialog dlg(dlgParam, this);
        if(dlg.exec()) {
            const auto parm = dlg.GetDlgParam();
            d->control.SetShape(parm.shape);
            d->control.SetSize(parm.width, parm.height);
            d->control.SetRows(parm.rows);
            d->control.SetColumns(parm.cols);
            d->control.SetSpacingMode(parm.spacingMode);
            d->control.SetSpacingHorizontal(parm.spacingHorizontal);
            d->control.SetSpacingVertical(parm.spacingVertical);
            d->control.SetStartX(parm.startX);
            d->control.SetStartY(parm.startY);
            d->control.SetWells(parm.wells);
        }
    }

    int VesselEditDialog::GetMinimumWidth() const {
        return d->frame->minimumWidth();
    }

    auto VesselEditDialog::Impl::InitValues(const VesselDataStructure::Vessel& vessel) -> void {
        ui.vesselModel->setText(vessel.model);
        ui.vesselName->setText(vessel.name);
        ui.naCombo->setCurrentIndex(GetIndexByNA(vessel.na));
        ui.vesselAFOffset->setValue(vessel.afOffset);
        ui.vesselWidth->setValue(vessel.w);
        ui.vesselHeight->setValue(vessel.h);
        ui.useMultiDishHolder->setChecked(vessel.useMultiDishHolder);

        ui.imgShape->setCurrentIndex(vessel.imgArea.shape);
        ui.imgX->setValue(vessel.imgArea.x);
        ui.imgY->setValue(vessel.imgArea.y);
        ui.imgWidth->setValue(vessel.imgArea.w);
        ui.imgHeight->setValue(vessel.imgArea.h);

        // TODO Well values to control
        control.SetShape(vessel.wellProp.shape);
        control.SetSize(vessel.wellProp.w, vessel.wellProp.h);
        control.SetWells(vessel.wells);
        control.SetRows(vessel.wellProp.rows);
        control.SetColumns(vessel.wellProp.cols);
        control.SetSpacingMode(SpacingMode::AutoSpacing); // TODO currently not using
        control.SetSpacingHorizontal(vessel.wellProp.spacingH);
        control.SetSpacingVertical(vessel.wellProp.spacingV);
        control.SetStartX(vessel.wells.first().x);
        control.SetStartY(vessel.wells.first().y);
    }

    auto VesselEditDialog::Impl::GetIndexByNA(const double& na) -> int32_t {
        const auto value = QString::number(na, 'g', 3);
        const auto index = ui.naCombo->findData(value);
        return index;
    }

    auto VesselEditDialog::Impl::ApplyStyleSheet() -> void {
        self->SetTitle("Edit Vessel");

        for (const auto& button : self->findChildren<QPushButton*>()) {
            if (button->isCheckable()) {
                button->setObjectName("bt-setup-toggle");
            }
            else {
                button->setObjectName("bt-setup-light");
            }
        }

        for (const auto& label : self->findChildren<QLabel*>()) {
            if (label->objectName().contains("titleLabel")) {
                label->setObjectName("label-h2");
            }
            else if (label->objectName().contains("itemLabel")) {
                label->setObjectName("label-h3");
            }
            else if (label->objectName().contains("subItemLabel")) {
                label->setObjectName("label-h5");
            }
        }

        // line divider
        for (const auto& line : self->findChildren<QFrame*>()) {
            if (line->frameShape() == QFrame::VLine) {
                line->setObjectName("line-divider");
                line->setFixedWidth(1);
            }
            else if (line->frameShape() == QFrame::HLine) {
                line->setObjectName("line-divider");
                line->setFixedHeight(1);
            }
        }
    }
}
