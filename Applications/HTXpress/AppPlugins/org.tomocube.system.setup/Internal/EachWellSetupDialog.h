﻿#pragma once

#include <memory>

#include <QStyledItemDelegate>

#include "CustomDialog.h"

#include "VesselDataStructures.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class EachWellSetupDialog : public TC::CustomDialog {
        Q_OBJECT
    public:
        using Self = EachWellSetupDialog;

        struct Inputs {
            AppEntity::WellShape::_enumerated shape;
            double width;
            double height;
            int32_t rows;
            int32_t cols;
            SpacingMode spacingMode;
            double spacingHorizontal;
            double spacingVertical;
            double startX;
            double startY;
            QList<VesselDataStructure::Well> wells;
        };

        EachWellSetupDialog(const Inputs& parameters, QWidget* parent = nullptr);
        ~EachWellSetupDialog() override;

        auto GetDlgParam() const -> Inputs;

    protected:
        auto GetMinimumWidth() const -> int override;
        
    private slots:
        void accept() override;
        void reject() override;

        void onChangeCount(bool toggled);
        void onChangeSpacing(bool toggled);
        void onChangeApperance(bool toggled);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class SpinBoxDelegate : public QStyledItemDelegate {
        Q_OBJECT
    public:
        explicit SpinBoxDelegate(QObject* parent = nullptr);
        auto createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const -> QWidget* override;
        auto setEditorData(QWidget* editor, const QModelIndex& index) const -> void override;
        auto setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const -> void override;
        auto updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const -> void override;
    };

    class LineEditDelegate : public QStyledItemDelegate {
        Q_OBJECT
    public:
        explicit LineEditDelegate(QObject* parent = nullptr);
        auto createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const -> QWidget* override;
        auto setEditorData(QWidget* editor, const QModelIndex& index) const -> void override;
        auto setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const -> void override;
        auto updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const -> void override;
    };
}
