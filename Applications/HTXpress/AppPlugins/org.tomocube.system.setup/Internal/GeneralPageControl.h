#pragma once
#include <memory>
#include <QStringList>

namespace HTXpress::AppPlugins::System::Setup::App {
    class GeneralPageControl {
    public:
        struct Config {
            QString dataFolder{};
            int32_t minSpace{};

            auto operator=(const Config& rhs)->Config&;
            auto operator==(const Config& rhs) const->bool;
            auto operator!=(const Config& rhs) const->bool;
        };

    public:
        GeneralPageControl();
        ~GeneralPageControl();

        auto Initialize()->void;
        auto GetConfig() const->Config;
        auto SetConfig(const Config& config)->bool;
        auto IsModified(const Config& config) const->bool;
        auto IsUsedDataFolder(const QString& path) const->bool;
        auto IsCorrectPassword(const QString& password) const-> bool;
        auto IsLicenseDeactivated() const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}