﻿#pragma once

#include <memory>

#include "ISetupPage.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    class MediumPage : public ISetupPage {
        Q_OBJECT
    public:
        using Self = MediumPage;

        explicit MediumPage(QWidget* parent = nullptr);
        ~MediumPage() override;

        auto Initialize() -> bool override;
        auto IsModified() const -> bool override;
        auto IsRestorable() const -> bool override;
        auto Restore() -> bool override;
        auto Save() -> bool override;

    private slots:
        void onAddMedium();
        void onEditMedium();
        void onDeleteMedium();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
