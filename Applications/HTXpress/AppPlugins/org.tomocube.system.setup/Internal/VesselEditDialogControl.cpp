﻿#include "VesselEditDialogControl.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct VesselEditDialogControl::Impl {
        AppEntity::WellShape::_enumerated shape;
        QPair<double, double> size; // width, height
        QPair<int32_t, int32_t> count; // rows, cols
        QPair<double, double> spacing; // hor, ver
        QPair<double, double> startPos; // cen_x, cen_y
        SpacingMode spacingMode;
        QList<VesselDataStructure::Well> wells;
    };

    VesselEditDialogControl::VesselEditDialogControl() : d{std::make_unique<Impl>()} {

    }

    VesselEditDialogControl::~VesselEditDialogControl() {
    }

    auto VesselEditDialogControl::SetShape(AppEntity::WellShape shape) -> void {
        d->shape = shape;
    }

    auto VesselEditDialogControl::GetShape() const -> AppEntity::WellShape {
        return d->shape;
    }

    auto VesselEditDialogControl::SetSize(double w, double h) -> void {
        d->size = {w, h};
    }

    auto VesselEditDialogControl::GetWidth() const -> double {
        return d->size.first;
    }

    auto VesselEditDialogControl::GetHeight() const -> double {
        return d->size.second;
    }

    auto VesselEditDialogControl::SetRows(int32_t rows) -> void {
        d->count.first = rows;
    }

    auto VesselEditDialogControl::GetRows() const -> int32_t {
        return d->count.first;
    }

    auto VesselEditDialogControl::SetColumns(int32_t cols) -> void {
        d->count.second = cols;
    }

    auto VesselEditDialogControl::GetColumns() const -> int32_t {
        return d->count.second;
    }

    auto VesselEditDialogControl::SetSpacingMode(SpacingMode mode) -> void {
        d->spacingMode = mode;
    }

    auto VesselEditDialogControl::GetSpacingMode() const -> SpacingMode {
        return d->spacingMode;
    }

    auto VesselEditDialogControl::SetSpacingHorizontal(double spacingRow) -> void {
        d->spacing.first = spacingRow;
    }

    auto VesselEditDialogControl::GetSpacingHorizontal() const -> double {
        return d->spacing.first;
    }

    auto VesselEditDialogControl::SetSpacingVertical(double spacingCol) -> void {
        d->spacing.second = spacingCol;
    }

    auto VesselEditDialogControl::GetSpacingVertical() const -> double {
        return d->spacing.second;
    }

    auto VesselEditDialogControl::SetStartX(double x) -> void {
        d->startPos.first = x;
    }

    auto VesselEditDialogControl::GetStartX() const -> double {
        return d->startPos.first;
    }

    auto VesselEditDialogControl::SetStartY(double y) -> void {
        d->startPos.second = y;
    }

    auto VesselEditDialogControl::GetStartY() const -> double {
        return d->startPos.second;
    }

    auto VesselEditDialogControl::SetWells(const QList<VesselDataStructure::Well>& wells) -> void {
        d->wells = wells;
    }

    auto VesselEditDialogControl::GetWells() const -> QList<VesselDataStructure::Well> {
        return d->wells;
    }
}
