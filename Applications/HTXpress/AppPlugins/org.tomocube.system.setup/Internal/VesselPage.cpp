#include <QStandardItemModel>
#include <QFileDialog>
#include <QFileInfo>

#include <MessageDialog.h>
#include <InputDialog.h>
#include <Vessel.h>
#include <VesselMapWidget.h>
#include <FileNameValidator.h>

#include "VesselPage.h"
#include "ui_VesselPage.h"
#include "VesselPageControl.h"
#include "VesselEditDialog.h"
#include "VesselAddDialog.h"
#include "TableViewEventFilter.h"
#include "VesselDataStructures.h"
#include "VesselImportResultDialog.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    struct VesselPage::Impl {
        explicit Impl(VesselPage* self) {
            this->self = self;
        }

        enum VesselColumns { Model = 0, Name, _NumOfCols };

        VesselPage* self{};
        Ui::VesselPage ui;
        VesselPageControl control;
        TableViewEventFilter* tableFilter{nullptr};
        TC::VesselMapWidget *preview{nullptr};

        auto UpdateGUI(const VesselDataStructure::Vessels& vessels) -> void;
        auto TableModel() const -> QStandardItemModel*;

        auto ApplyStyleSheet() -> void;
        auto ShowImportResult(const QMap<QString, VesselPageControl::ImportResult>& importResults) -> void;
    };

    auto VesselPage::Impl::UpdateGUI(const VesselDataStructure::Vessels& vessels) -> void {
        TableModel()->setRowCount(vessels.size());

        int32_t row = 0;
        for (auto vessel : vessels) {
            QModelIndex modelIdx = TableModel()->index(row, Model);
            TableModel()->setData(modelIdx, vessel.model);
            modelIdx = TableModel()->index(row, Name);
            TableModel()->setData(modelIdx, vessel.name);
            row++;
        }
        ui.table->setCurrentIndex(QModelIndex());
    }

    auto VesselPage::Impl::TableModel() const -> QStandardItemModel* {
        return qobject_cast<QStandardItemModel*>(ui.table->model());
    }

    auto VesselPage::Impl::ApplyStyleSheet(/*Self* self*/) -> void {
        ui.baseWidget->setObjectName("panel");

        for(const auto& button : self->findChildren<QPushButton*>()) {
            if(button->isCheckable()) {
                button->setObjectName("bt-setup-toggle");
            }
            else {
                button->setObjectName("bt-setup-light");
            }
        }

        for(const auto& label : self->findChildren<QLabel*>()) {
            if(label->objectName().contains("title")) {
                label->setObjectName("label-h2");
            }
        }

        ui.table->setStyleSheet(QString("QTableView{border-bottom: 0px;}"));
        ui.table->verticalHeader()->setStyleSheet(QString("QHeaderView{border-bottom: 0px;}"));
        ui.table->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);
    }

    auto VesselPage::Impl::ShowImportResult(const QMap<QString, VesselPageControl::ImportResult>& importResults) -> void {
        VesselImportResultDialog dlg(importResults, self);
        dlg.exec();
    }

    VesselPage::VesselPage(const QString& vesselFolderPath, QWidget* parent) : ISetupPage(parent), d{std::make_unique<Impl>(this)} {
        d->ui.setupUi(this);

        d->tableFilter = new TableViewEventFilter(d->ui.table);
        d->ui.table->viewport()->installEventFilter(d->tableFilter);
        d->ui.table->setSelectionBehavior(QAbstractItemView::SelectRows);
        d->ui.table->setSelectionMode(QAbstractItemView::SingleSelection);
        d->ui.table->setEditTriggers(QAbstractItemView::NoEditTriggers);
        d->ui.table->setModel(new QStandardItemModel(this));
        d->TableModel()->insertColumns(0, Impl::_NumOfCols);
        d->TableModel()->setHeaderData(Impl::VesselColumns::Model, Qt::Horizontal, "Model");
        d->TableModel()->setHeaderData(Impl::VesselColumns::Name, Qt::Horizontal, "Name");
        d->ui.table->horizontalHeader()->setMinimumSectionSize(200);

        d->control.SetVesselFolderPath(vesselFolderPath);

        const auto layout = new QGridLayout;
        d->preview = new TC::VesselMapWidget(this);
        d->preview->SetViewMode(TC::ViewMode::PreviewMode);

        layout->addWidget(d->preview);
        layout->setMargin(3);
        layout->setSpacing(3);
        layout->setContentsMargins(6, 6, 6, 6);
        d->ui.previewFrame->setLayout(layout);

        d->ui.previewFrame->setObjectName("panel");

        connect(d->ui.add, &QPushButton::clicked, this, &Self::onAddVessel);
        connect(d->ui.edit, &QPushButton::clicked, this, &Self::onEditVessel);
        connect(d->ui.remove, &QPushButton::clicked, this, &Self::onDeleteVessel);
        connect(d->ui.copy, &QPushButton::clicked, this, &Self::onCopyVessel);
        connect(d->ui.btnImport, &QPushButton::clicked, this, &Self::onImportVessel);

        connect(d->ui.table->selectionModel(), &QItemSelectionModel::currentRowChanged, this, &Self::onUpdatePreview);

        d->ApplyStyleSheet(/*this*/);
    }

    VesselPage::~VesselPage() {
    }

    auto VesselPage::Initialize() -> bool {
        d->control.Initialize();
        d->UpdateGUI(d->control.GetVessels());

        d->ui.add->setEnabled(IsServiceEngineer());
        d->ui.copy->setEnabled(IsServiceEngineer()||IsAdmin());
        d->ui.edit->setEnabled(IsServiceEngineer()||IsAdmin());
        d->ui.remove->setEnabled(IsServiceEngineer());
        d->ui.btnImport->setEnabled(IsServiceEngineer()||IsAdmin());

        return true;
    }

    auto VesselPage::IsModified() const -> bool {
        return d->control.IsModified();
    }

    auto VesselPage::IsRestorable() const -> bool {
        return true;
    }

    auto VesselPage::Restore() -> bool {
        Initialize();
        return true;
    }

    auto VesselPage::Save() -> bool {
        return d->control.SetVessels();
    }

    auto VesselPage::onAddVessel() -> void {
        QStringList models;
        QStringList nicknames;
        const auto NAs = d->control.GetNAs();

        for (int i = 0; i < d->TableModel()->rowCount(); ++i) {
            models << d->TableModel()->index(i, Impl::VesselColumns::Model).data().toString();
            nicknames << d->TableModel()->index(i, Impl::VesselColumns::Name).data().toString();
        }

        VesselAddDialog dlg(models, nicknames, NAs, this);

        if (dlg.exec()) {
            const auto newVessel = dlg.Vessel();
            d->control.AddVessel(newVessel);

            const auto row = d->TableModel()->rowCount();
            d->TableModel()->insertRow(row, QModelIndex());
            QModelIndex modelIdx = d->TableModel()->index(row, Impl::VesselColumns::Model);
            d->TableModel()->setData(modelIdx, newVessel.model);
            modelIdx = d->TableModel()->index(row, Impl::VesselColumns::Name);
            d->TableModel()->setData(modelIdx, newVessel.name);

            d->ui.table->selectRow(row); // to focus current new vessel
        }
    }

    auto VesselPage::onEditVessel() -> void {
        if (!d->ui.table->currentIndex().isValid()) return;
        const auto selIndex = d->ui.table->currentIndex();
        const auto selRow = selIndex.row();
        d->ui.table->selectRow(selRow);

        const auto vesselModel = d->TableModel()->index(selRow, Impl::VesselColumns::Model).data().toString();
        VesselDataStructure::Vessel vessel;
        const auto result = d->control.GetTempVessel(vesselModel, vessel);

        if (!result) return;

        QStringList nicknames;
        for (int i = 0; i < d->TableModel()->rowCount(); ++i) {
            if(selRow != i) {
                nicknames << d->TableModel()->index(i, Impl::VesselColumns::Name).data().toString();
            }
        }

        const auto NAs = d->control.GetNAs();

        VesselEditDialog dlg(vessel, nicknames, NAs, this);
        const auto multidishChangeable = IsServiceEngineer();
        dlg.SetMultidishChangeable(multidishChangeable);

        if (dlg.exec()) {
            const auto modVessel = dlg.Vessel();
            d->control.EditVessel(modVessel.model, modVessel);

            // table
            const auto modelIndex = d->TableModel()->index(selRow, Impl::VesselColumns::Name);
            d->TableModel()->setData(modelIndex, modVessel.name);

            onUpdatePreview();
        }
    }

    auto VesselPage::onDeleteVessel() -> void {
        const auto selRow = d->ui.table->currentIndex().row();
        const auto vesselModel = d->TableModel()->index(selRow, Impl::VesselColumns::Model).data().toString();

        d->control.DeleteVessel(vesselModel);
        d->TableModel()->removeRow(selRow, QModelIndex());
    }

    auto VesselPage::onCopyVessel() -> void {
        if (d->ui.table->currentIndex().row() == -1) return;

        auto dlg = new TC::InputDialog();
        dlg->setInputMode(TC::InputDialog::InputMode::TextInput);
        dlg->SetContent("Enter a new vessel model:");
        dlg->SetTitle("Copy vessel");
        const auto originalModelname = d->TableModel()->index(d->ui.table->currentIndex().row(), Impl::VesselColumns::Model).data().toString();
        dlg->setTextValue(originalModelname + "_copy");

        QStringList models;
        for (int i = 0; i < d->TableModel()->rowCount(); ++i) {
            models << d->TableModel()->index(i, Impl::VesselColumns::Model).data().toString();
        }

        if (QDialog::Accepted == dlg->exec()) {
            bool result = true;
            if (models.contains(dlg->textValue())) {
                TC::MessageDialog::warning(this, tr("Model Duplication"), QString("%1 is already exists.\n").arg(dlg->textValue()));
                result = false;
            }

            TC::FileNameValidator validator;
            validator.SetMaxLength(32);
            if (!validator.IsValid(dlg->textValue())) {
                TC::MessageDialog::warning(this, tr("Invalid model name"), validator.GetErrorString());
                result = false;
            }

            if (!result) {
                onCopyVessel();
            }
            else { // copy
                VesselDataStructure::Vessel cloneVessel;
                d->control.GetTempVessel(originalModelname, cloneVessel);
                cloneVessel.model = dlg->textValue(); // set copied model name
                cloneVessel.name = dlg->textValue(); // copied vessel's default name == copied vessel's model name

                // same as onAddVessel() process
                d->control.AddVessel(cloneVessel);

                const auto row = d->TableModel()->rowCount();
                d->TableModel()->insertRow(row, QModelIndex());
                QModelIndex modelIdx = d->TableModel()->index(row, Impl::VesselColumns::Model);
                d->TableModel()->setData(modelIdx, cloneVessel.model);
                modelIdx = d->TableModel()->index(row, Impl::VesselColumns::Name);
                d->TableModel()->setData(modelIdx, cloneVessel.name); 

                d->ui.table->selectRow(row); // to focus current new vessel
            }
        }
    }

    auto VesselPage::onUpdatePreview() -> void {
        const auto selRow = d->ui.table->currentIndex().row();
        const auto vesselModel = d->TableModel()->index(selRow, Impl::VesselColumns::Model).data().toString();
        VesselDataStructure::Vessel vessel;
        if(d->control.GetTempVessel(vesselModel, vessel)) {
            const auto tcVesselMap = d->control.Convert(vessel);
            if(tcVesselMap != nullptr) {
                d->preview->SetVesselMap(tcVesselMap);
            }
        }
        else {
            d->preview->ClearAll();
        }
    }

    void VesselPage::onImportVessel() {
        const auto entryList = QFileDialog::getOpenFileNames(this, tr("Select vessel files for import"), "", tr("Vessel Files (*.vessel)"));
        if(entryList.isEmpty()) {
            return;
        }

        QMap<QString, VesselPageControl::ImportResult> importResults;
        for(const auto& fileName : entryList) {
            importResults[fileName] = {};
        }

        const auto importSuccessList = d->control.ImportVessels(importResults);

        for(const auto& v: importSuccessList) {
            // same as onAddVessel() process
            d->control.AddVessel(v);
            const auto row = d->TableModel()->rowCount();
            d->TableModel()->insertRow(row, QModelIndex());
            QModelIndex modelIdx = d->TableModel()->index(row, Impl::VesselColumns::Model);
            d->TableModel()->setData(modelIdx, v.model);
            modelIdx = d->TableModel()->index(row, Impl::VesselColumns::Name);
            d->TableModel()->setData(modelIdx, v.name);

            d->ui.table->selectRow(row); // to focus current new vessel
        }

        d->ShowImportResult(importResults);

    }
}
