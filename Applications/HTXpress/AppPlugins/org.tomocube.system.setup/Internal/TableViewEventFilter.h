﻿#pragma once

#include <memory>
#include <QObject>
#include <QTableView>

namespace HTXpress::AppPlugins::System::Setup::App {
    class TableViewEventFilter : public QObject {
        Q_OBJECT
    public:
        using Self = TableViewEventFilter;
        using Pointer = std::shared_ptr<Self>;

        TableViewEventFilter(QTableView* table, QObject *parent = nullptr);
        ~TableViewEventFilter() override;

    protected:
        auto eventFilter(QObject* watched, QEvent* event) -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
