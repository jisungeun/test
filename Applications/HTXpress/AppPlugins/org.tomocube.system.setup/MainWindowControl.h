#pragma once
#include <memory>

namespace HTXpress::AppPlugins::System::Setup::App {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

        auto GetVesselFolderPath() const -> const QString&;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}