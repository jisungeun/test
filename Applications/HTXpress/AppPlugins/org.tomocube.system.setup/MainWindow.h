#pragma once
#include <memory>
#include <IMainWindowHTX.h>

namespace HTXpress::AppPlugins::System::Setup::App {
    class MainWindow : public AppComponents::Framework::IMainWindowHTX {
        Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = nullptr);
        ~MainWindow() override;

        auto TryActivate() -> bool final;
        auto TryDeactivate() -> bool final;
        auto IsActivate() -> bool final;
        auto GetMetaInfo()->QVariantMap final;

        auto Execute(const QVariantMap& params) -> bool override;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    signals:

    private:
        auto InitUI(void)->bool;
        
    protected slots:
        void onHandleEvent(const ctkEvent& ctkEvent);
        void onRestore();
        void onSave();
        void onGotoHome();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
