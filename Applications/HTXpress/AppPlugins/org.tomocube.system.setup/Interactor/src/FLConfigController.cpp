﻿#include <SaveFLConfiguration.h>

#include "FLConfigController.h"

namespace HTXpress::AppPlugins::System::Setup::Interactor {
    struct FLConfigController::Impl {
        
    };

    FLConfigController::FLConfigController() : d{std::make_unique<Impl>()} {
    }

    FLConfigController::~FLConfigController() {
    }

    auto FLConfigController::Save() -> bool {
        auto usecase = UseCase::SaveFLConfiguration();
        return usecase.Request();
    }
}
