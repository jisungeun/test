#include <SaveConfiguration.h>
#include <IsDataFolder.h>

#include "ConfigController.h"

namespace HTXpress::AppPlugins::System::Setup::Interactor {
    struct ConfigController::Impl {
    };

    ConfigController::ConfigController() : d{new Impl} {
    }

    ConfigController::~ConfigController() {
    }

    auto ConfigController::Save() -> bool {
        auto usecase = UseCase::SaveConfiguration();
        return usecase.Request();
    }

    auto ConfigController::IsUsedDataFolder(const QString& path) -> bool {
        auto usecase = UseCase::IsDataFolder();
        return usecase.IsValid(path);
    }
}
