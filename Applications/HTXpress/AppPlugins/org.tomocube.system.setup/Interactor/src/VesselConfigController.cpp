﻿#include <SaveVesselConfiguration.h>
#include <DeleteVesselConfiguration.h>

#include "VesselConfigController.h"

namespace HTXpress::AppPlugins::System::Setup::Interactor {
    struct VesselConfigController::Impl {
        
    };

    VesselConfigController::VesselConfigController() : d{std::make_unique<Impl>()} {
    }

    VesselConfigController::~VesselConfigController() {
    }

    auto VesselConfigController::Save() -> bool {
        auto usecase = UseCase::SaveVesselConfiguration();
        return usecase.Request();
    }

    auto VesselConfigController::Delete(const QStringList& files) -> bool {
        auto usecase = UseCase::DeleteVesselConfiguration(files);
        return usecase.Request();
    }
}
