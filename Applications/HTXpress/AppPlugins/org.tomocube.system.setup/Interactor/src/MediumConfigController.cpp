#include <SaveMediumConfiguration.h>

#include "MediumConfigController.h"

namespace HTXpress::AppPlugins::System::Setup::Interactor {
    struct MediumConfigController::Impl {
        
    };

    MediumConfigController::MediumConfigController() : d{std::make_unique<Impl>()} {
    }

    MediumConfigController::~MediumConfigController() {
    }

    auto MediumConfigController::Save() -> bool {
        auto usecase = UseCase::SaveMediumConfiguration();
        return usecase.Request();
    }
}
