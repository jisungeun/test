﻿#include <AddUser.h>
#include <DeleteUser.h>
#include <ModifyUser.h>
#include <ChangeUserPassword.h>
#include <CheckValidAdmin.h>
#include <CheckBuiltInAccount.h>
#include <CheckPassword.h>

#include "UserInformationController.h"

namespace HTXpress::AppPlugins::System::Setup::Interactor {
    struct UserInformationController::Impl {
        
    };

    UserInformationController::UserInformationController() : d{std::make_unique<Impl>()} {
    }

    UserInformationController::~UserInformationController() {
    }

    auto UserInformationController::Add(const AppEntity::User::Pointer& user, const QString& password) -> bool {
        auto usecase = UseCase::AddUser();
        usecase.Add(user, password);
        return usecase.Request();
    }

    auto UserInformationController::Delete(const AppEntity::UserID& id) -> bool {
        auto usecase = UseCase::DeleteUser();
        usecase.Delete(id);
        return usecase.Request();
    }

    auto UserInformationController::Modify(const AppEntity::User::Pointer& user) -> bool {
        auto usecase = UseCase::ModifyUser();
        usecase.Modify(user);
        return usecase.Request();
    }

    auto UserInformationController::ChangePassword(const AppEntity::UserID& id, const QString& pw) -> bool {
        auto usecase = UseCase::ChangeUserPassword();
        usecase.ChangePassword(id, pw);
        return usecase.Request();
    }

    auto UserInformationController::IsValidAdmin(const AppEntity::UserID& id, const QString& password) -> bool {
        auto usecase = UseCase::CheckValidAdmin();
        usecase.Set(id, password);
        return usecase.Request();
    }

    auto UserInformationController::IsBuiltInAccount(const AppEntity::UserID& id) const -> bool {
        auto usecase = UseCase::CheckBuiltInAccount();
        usecase.SetID(id);
        return usecase.Request();
    }

    auto UserInformationController::IsCorrectPassword(const AppEntity::UserID& id, const QString& password) const -> bool {
        auto usecase = UseCase::CheckPassword();
        usecase.SetAccount(id, password);
        return usecase.Request();
    }
}
