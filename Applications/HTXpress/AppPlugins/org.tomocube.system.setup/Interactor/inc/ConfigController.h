#pragma once
#include <memory>

#include "HTX_System_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::System::Setup::Interactor {
    class HTX_System_Setup_Interactor_API ConfigController {
    public:
        ConfigController();
        ~ConfigController();

        auto Save()->bool;

        auto IsUsedDataFolder(const QString& path)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}