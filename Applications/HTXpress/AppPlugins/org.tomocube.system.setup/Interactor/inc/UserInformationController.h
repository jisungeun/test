﻿#pragma once

#include <memory>

#include <User.h>

#include "HTX_System_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::System::Setup::Interactor {
    class HTX_System_Setup_Interactor_API UserInformationController {
    public:
        UserInformationController();
        ~UserInformationController();

        auto Add(const AppEntity::User::Pointer& user, const QString& password) -> bool;
        auto Delete(const AppEntity::UserID& id) -> bool;
        auto Modify(const AppEntity::User::Pointer& user) -> bool;
        auto ChangePassword(const AppEntity::UserID& id, const QString& pw) -> bool;

        auto IsValidAdmin(const AppEntity::UserID& id, const QString& password) -> bool;
        auto IsBuiltInAccount(const AppEntity::UserID& id) const -> bool;
        auto IsCorrectPassword(const AppEntity::UserID& id, const QString& password) const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
