﻿#pragma once

#include <memory>

#include "HTX_System_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::System::Setup::Interactor {
    class HTX_System_Setup_Interactor_API VesselConfigController {
    public:
        VesselConfigController();
        ~VesselConfigController();

        auto Save() -> bool;
        auto Delete(const QStringList& files) -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
