#pragma once

#include <memory>

#include "HTX_System_Setup_InteractorExport.h"

namespace HTXpress::AppPlugins::System::Setup::Interactor {
    class HTX_System_Setup_Interactor_API MediumConfigController {
    public:
        MediumConfigController();
        ~MediumConfigController();

        auto Save() -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}