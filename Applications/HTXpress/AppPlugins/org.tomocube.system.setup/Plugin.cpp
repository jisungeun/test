#include <QtPlugin>

#include <ctkPluginFrameworkLauncher.h>
#include <service/event/ctkEvent.h>

#include <AppEvent.h>
#include <AppInterfaceHTX.h>

#include "MainWindow.h"
#include "Plugin.h"

namespace HTXpress::AppPlugins::System::Setup::App {
    static Plugin* theInstance = nullptr;

    struct Plugin::Impl {
        ctkPluginContext* context;        
        MainWindow* mainWindow{ nullptr };
        AppInterfaceHTX* appInterface{ nullptr };
    };

    Plugin* Plugin::getInstance() {
        return theInstance;
    }

    Plugin::Plugin() : d(new Impl) {
        d->mainWindow = new MainWindow();
        d->appInterface = new AppInterfaceHTX(d->mainWindow);
    }
    
    Plugin::~Plugin() {
        
    }

    auto Plugin::start(ctkPluginContext* context)->void {
        theInstance = this;
        d->context = context;

        namespace HTXFramework = HTXpress::AppComponents::Framework;        
        registerService(context, d->appInterface, plugin_symbolic_name);
    }
    
    auto Plugin::stop(ctkPluginContext* context)->void {
        Q_UNUSED(context)        
    }
    
    auto Plugin::getPluginContext()->ctkPluginContext* const {
        return d->context;
    }    
}
