#pragma once
#include <memory>

namespace HTXpress::AppPlugins::General::Start::App {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();
        
        auto LoadConfigurations()->bool;
        auto LoadUserDatabase()->bool;
        auto Login(const QString& id, const QString& password, AppEntity::Profile privilege = AppEntity::Profile::Operator)->bool;
        auto LogOff()->bool;
        auto AddUser(const QString& name, const QString& id, const QString& password, AppEntity::Profile profile)->bool;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}