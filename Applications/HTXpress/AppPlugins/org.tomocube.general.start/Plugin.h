#pragma once

#include <IAppActivator.h>

const QString plugin_symbolic_name = "org.tomocube.general.start";

namespace HTXpress::AppPlugins::General::Start::App {
    class Plugin : public TC::Framework::IAppActivator {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.general.start")
                    
    public:
        static Plugin* getInstance();

        Plugin();
        ~Plugin();

        auto start(ctkPluginContext* context)->void override;
        auto stop(ctkPluginContext* context)->void override;
        auto getPluginContext()->ctkPluginContext* const override;            

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}