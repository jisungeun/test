#include "ISystemConfigLoader.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    static ISystemConfigLoader* theInstance{ nullptr };
    ISystemConfigLoader::ISystemConfigLoader() {
        theInstance = this;
    }

    ISystemConfigLoader::~ISystemConfigLoader() {
    }

    auto ISystemConfigLoader::GetInstance() -> ISystemConfigLoader* {
        return theInstance;
    }
}
