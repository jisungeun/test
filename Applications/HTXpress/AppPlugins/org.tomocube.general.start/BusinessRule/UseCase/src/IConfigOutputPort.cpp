#include "IConfigOutputPort.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    IConfigOutputPort::IConfigOutputPort() {
    }

    IConfigOutputPort::~IConfigOutputPort() {
    }

    static IConfigNullOutputPort::Pointer nullInstance{ new IConfigNullOutputPort() };
    auto IConfigNullOutputPort::GetInstance() -> IConfigNullOutputPort* {
        return nullInstance.get();
    }
}
