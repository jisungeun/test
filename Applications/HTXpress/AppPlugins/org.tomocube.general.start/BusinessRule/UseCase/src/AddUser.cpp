#include "IUserManager.h"
#include "AddUser.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    struct AddUser::Impl {
        AddUser* p{ nullptr };
        IUserOutputPort* output{ nullptr };
        QString name;
        QString id;
        QString password;
        AppEntity::Profile profile{ AppEntity::Profile::Operator };

        Impl(AddUser* p) : p{ p } {}
        auto SetError(const QString& message)->void;
    };

    auto AddUser::Impl::SetError(const QString& message) -> void {
        p->Error(message);
        if(output) output->AddUserFailed(message);
    }

    AddUser::AddUser(IUserOutputPort* output) : IUseCase("AddUser"), d{ new Impl(this) } {
        d->output = output;
    }

    AddUser::~AddUser() {
    }

    auto AddUser::Set(const QString& name, const QString& id, const QString& password,
                      AppEntity::Profile profile) -> void {
        d->name = name;
        d->id = id;
        d->password = password;
        d->profile = profile;
    }

    auto AddUser::Perform() -> bool {
        auto* manager = IUserManager::GetInstance();
        if(manager == nullptr) {
            d->SetError(tr("It can't add new user because of internal error"));
            return false;
        }

        if(d->id.isEmpty()) {
            d->SetError(tr("User ID is blank"));
            return false;
        }

        if(d->password.isEmpty()) {
            d->SetError(tr("Password is blank"));
            return false;
        }

        auto user = manager->GetUser(d->id);
        if(user != nullptr) {
            d->SetError(tr("%1 is existing user").arg(user->GetUserID()));
            return false;
        }

        user = std::make_shared<AppEntity::User>();
        user->SetName(d->name);
        user->SetUserID(d->id);
        user->SetProfile(d->profile);

        if(!manager->AddUser(user, d->password)) {
            d->SetError(tr("It fails to add new user"));
            return false;
        }

        return true;
    }
}