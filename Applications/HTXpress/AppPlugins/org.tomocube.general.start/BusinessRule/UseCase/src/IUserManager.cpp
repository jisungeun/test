#include "IUserManager.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    static IUserManager* theInstance{ nullptr };

    IUserManager::IUserManager() {
        theInstance = this;
    }

    IUserManager::~IUserManager() {
    }

    auto IUserManager::GetInstance() -> IUserManager* {
        return theInstance;
    }
}