#include <QCoreApplication>

#include <System.h>

#include "ISystemConfigLoader.h"
#include "LoadConfiguration.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    struct LoadConfiguration::Impl {
        LoadConfiguration* p{ nullptr };
        IConfigOutputPort* output{ IConfigNullOutputPort::GetInstance() };

        Impl(LoadConfiguration* p) : p(p) {}

        auto SetError(const QString& message)->void;
    };

    auto LoadConfiguration::Impl::SetError(const QString& message) -> void {
        p->Error(message);
        output->NotifyLoadingFailure(message);
    }

    LoadConfiguration::LoadConfiguration(IConfigOutputPort* output) : IUseCase("LoadConfiguration"), d{new Impl(this)} {
        d->output = output;
    }

    LoadConfiguration::~LoadConfiguration() {
    }

    auto LoadConfiguration::Perform() -> bool {
        auto* loader = ISystemConfigLoader::GetInstance();
        if(!loader) {
            d->SetError("No valid system configuration loader exists");
            return false;
        }

        if(!loader->Read()) {
            d->SetError(QString("It fails to load system configuration. \n %1").arg(loader->GetErrorMessage()));
            d->output->UpdateSystemInfo("System is not properly installed");
            return false;
        }

        auto model = AppEntity::System::GetModel();
        d->output->UpdateSystemInfo(model->Name());

        return true;
    }
}