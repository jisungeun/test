#include <SystemStatus.h>
#include <SessionManager.h>

#include "LogOff.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    struct LogOff::Impl {
        LogOff* p{ nullptr };
        IUserOutputPort* output{ nullptr };

        Impl(LogOff* p) : p{ p } {}
        auto SetReport(const QString& message)->void;
    };

    auto LogOff::Impl::SetReport(const QString& message) -> void {
        p->Error(message);
        if(output) output->LogOffFailed(message);
    }

    LogOff::LogOff(IUserOutputPort* output) : IUseCase("LogOff"), d{ new Impl(this) } {
        d->output = output;
    }

    LogOff::~LogOff() {
    }

    auto LogOff::Perform() -> bool {
        auto session = AppEntity::SessionManager::GetInstance();
        auto status = AppEntity::SystemStatus::GetInstance();

        if(status->GetBusy()) {
            d->SetReport(tr("You can't log off while microscope is under experiment"));
            return false;
        }

        session->Logout();
        if(d->output) d->output->LogOffSuccess();

        return true;
    }
}
