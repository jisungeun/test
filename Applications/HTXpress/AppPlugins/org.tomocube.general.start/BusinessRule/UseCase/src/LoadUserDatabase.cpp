#include "IUserManager.h"
#include "LoadUserDatabase.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    LoadUserDatabase::LoadUserDatabase() : IUseCase("LoadUserDatabase") {
    }

    LoadUserDatabase::~LoadUserDatabase() {
    }

    auto LoadUserDatabase::Perform() -> bool {
        auto* manager = IUserManager::GetInstance();
        if(manager == nullptr) {
            Error(tr("It fails to load user database because of internal error"));
            return false;
        }

        if(!manager->Load()) {
            Error(tr("It fails to load user database"));
            return false;
        }

        return true;
    }
}