#include <SessionManager.h>
#include <SystemStatus.h>

#include "IUserManager.h"
#include "Login.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    struct Login::Impl {
        Login* p{ nullptr };

        QString id;
        QString password;
        AppEntity::Profile privilege{ AppEntity::Profile::Operator };
        IUserOutputPort* output{ nullptr };

        Impl(Login* p) : p{ p } {
        }

        auto SetError(const QString& message)->void;
    };

    auto Login::Impl::SetError(const QString& message) -> void {
        p->Error(message);
        if(output) output->LoginFailed(message);
    }

    Login::Login(IUserOutputPort* output) : IUseCase("Login"), d{ new Impl(this) } {
        d->output = output;
    }

    Login::~Login() {
    }

    auto Login::Set(const QString& id, const QString& password, AppEntity::Profile privilege) -> void {
        d->id = id;
        d->password = password;
        d->privilege = privilege;
    }

    auto Login::Perform() -> bool {
        auto* manager = IUserManager::GetInstance();
        if(manager == nullptr) {
            d->SetError(tr("It can't access user database."));
            return false;
        }

        if(!manager->GetUser(d->id)) {
            d->SetError(tr("ID '%1' does not exist.").arg(d->id));
            return false;
        }

        if(!manager->IsValid(d->id, d->password)) {
            d->SetError(tr("The password is incorrect. Try again."));
            return false;
        }

        auto user = manager->GetUser(d->id);
        if(user->GetProfile() < d->privilege) {
            d->SetError(tr("%1 has no %2 privilege.").arg(d->id).arg(d->privilege._to_string()));
            return false;
        }

        auto session = AppEntity::SessionManager::GetInstance();
        session->Login(user);

        auto status = AppEntity::SystemStatus::GetInstance();
        if(user->GetProfile() == +AppEntity::Profile::ServiceEngineer) {
            status->SetServiceMode(AppEntity::ServiceMode::Configure);
        } else {
            status->SetServiceMode(AppEntity::ServiceMode::None);
        }

        if(d->output) {
            d->output->LoginSuccess();
        }

        return true;
    }
};
