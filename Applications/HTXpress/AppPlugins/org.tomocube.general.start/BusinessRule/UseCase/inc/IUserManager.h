#pragma once
#include <memory>

#include <User.h>

#include "HTX_General_Start_UseCaseExport.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    class HTX_General_Start_UseCase_API IUserManager {
    public:
        using Pointer = std::shared_ptr<IUserManager>;

    protected:
        IUserManager();

    public:
        virtual ~IUserManager();

        static auto GetInstance()->IUserManager*;

        virtual auto Load()->bool = 0;
        virtual auto IsValid(const AppEntity::UserID& id, const QString& password) const->bool = 0;
        virtual auto GetUser(const AppEntity::UserID& id) const->AppEntity::User::Pointer = 0;
        virtual auto AddUser(const AppEntity::User::Pointer user, const QString& password)->bool = 0;
    };
}