#pragma once
#include <memory>

#include "IUseCase.h"
#include "IConfigOutputPort.h"
#include "HTX_General_Start_UseCaseExport.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    class HTX_General_Start_UseCase_API LoadConfiguration : public IUseCase {
    public:
        LoadConfiguration(IConfigOutputPort* output);
        ~LoadConfiguration() override;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}