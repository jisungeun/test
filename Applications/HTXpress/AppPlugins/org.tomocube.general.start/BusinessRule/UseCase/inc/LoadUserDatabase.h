#pragma once
#include <memory>
#include <QCoreApplication>

#include "IUseCase.h"
#include "HTX_General_Start_UseCaseExport.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    class HTX_General_Start_UseCase_API LoadUserDatabase : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(LoadUserDatabase)

    public:
        LoadUserDatabase();
        ~LoadUserDatabase() override;

    protected:
        auto Perform() -> bool override;
    };
}