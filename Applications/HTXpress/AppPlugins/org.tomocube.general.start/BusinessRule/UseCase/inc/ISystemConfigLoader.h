#pragma once
#include <memory>
#include <QString>

#include "HTX_General_Start_UseCaseExport.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    class HTX_General_Start_UseCase_API ISystemConfigLoader {
    public:
        using Pointer = std::shared_ptr<ISystemConfigLoader>;

    protected:
        ISystemConfigLoader();

    public:
        virtual ~ISystemConfigLoader();

        static auto GetInstance()->ISystemConfigLoader*;

        virtual auto Read()->bool = 0;
        virtual auto GetErrorMessage() const->QString = 0;
    };
}