#pragma once
#include <memory>
#include <QString>

#include "HTX_General_Start_UseCaseExport.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    class HTX_General_Start_UseCase_API IUserOutputPort {
    public:
        IUserOutputPort();
        virtual ~IUserOutputPort();

        virtual auto LoginFailed(const QString& message)->void = 0;
        virtual auto LoginSuccess()->void = 0;
        virtual auto LogOffFailed(const QString& message)->void = 0;
        virtual auto LogOffSuccess()->void = 0;
        virtual auto AddUserFailed(const QString& message)->void = 0;
    };
}