#pragma once
#include <memory>
#include <QString>
#include <QCoreApplication>

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "IUserOutputPort.h"
#include "HTX_General_Start_UseCaseExport.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    class HTX_General_Start_UseCase_API Login : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(Login)

    public:
        Login(IUserOutputPort* output);
        ~Login() override;

        auto Set(const QString& id, const QString& password, AppEntity::Profile privilege)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}