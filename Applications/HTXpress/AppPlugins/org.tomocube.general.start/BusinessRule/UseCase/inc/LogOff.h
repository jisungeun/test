#pragma once
#include <memory>
#include <QString>
#include <QCoreApplication>

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "IUserOutputPort.h"
#include "HTX_General_Start_UseCaseExport.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    class HTX_General_Start_UseCase_API LogOff : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(LogOff)

    public:
        LogOff(IUserOutputPort* output);
        ~LogOff() override;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}