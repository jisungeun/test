#pragma once
#include <memory>
#include <QString>

#include "HTX_General_Start_UseCaseExport.h"

namespace HTXpress::AppPlugins::General::Start::UseCase {
    class HTX_General_Start_UseCase_API IConfigOutputPort {
    public:
        IConfigOutputPort();
        virtual ~IConfigOutputPort();

        virtual auto UpdateSystemInfo(const QString& info)->void = 0;
        virtual auto NotifyLoadingFailure(const QString& message)->void = 0;
    };

    class IConfigNullOutputPort : public IConfigOutputPort {
    public:
        using Pointer = std::shared_ptr<IConfigNullOutputPort>;

    public:
        IConfigNullOutputPort() : IConfigOutputPort() {}
        ~IConfigNullOutputPort() override {}

        static auto GetInstance()->IConfigNullOutputPort*;

        auto UpdateSystemInfo(const QString& info) -> void override {
            Q_UNUSED(info)
        }

        auto NotifyLoadingFailure(const QString& message) -> void override {
            Q_UNUSED(message)
        }
    };
}