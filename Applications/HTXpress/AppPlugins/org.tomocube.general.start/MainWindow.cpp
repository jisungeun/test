#include <QTimer>

#include <MessageDialog.h>
#include <AppEvent.h>
#include <MenuEvent.h>
#include <System.h>
#include <SessionManager.h>
#include <UserEditDialog.h>
#include <RegistrySignature.h>

#include "Internal/ConfigObserver.h"
#include "Internal/UserObserver.h"
#include "Internal/LoginPanel.h"
#include "ui_MainWindow.h"
#include "MainWindowControl.h"
#include "MainWindow.h"

namespace HTXpress::AppPlugins::General::Start::App {
    using namespace TC::Framework;
    struct MainWindow::Impl {
        Ui::MainWindow ui;
        MainWindow* p{ nullptr };
        bool firstLoad{ true };

        MainWindowControl control;
        ConfigObserver* configObserver{ nullptr };
        UserObserver* userObserver{ nullptr };

        QVariantMap appProperties;

        struct {
            QTimer timer;
            TC::RegistrySignature isService{"Tomocube, Inc.", "TomoStudioX", "signatures/service", AppEntity::System::GetSoftwareVersion(), 3600};
        } registry;

        QDialog* dummyDialog{ nullptr };    //This dialog is only for preventing CTK-related exception while it closes SW.

        Impl(MainWindow* p) : p{ p } {}
        auto loadConfiguration()->void;
    };

    auto MainWindow::Impl::loadConfiguration() -> void {
        if(!firstLoad) return;
        firstLoad = false;

        dummyDialog->close();
        delete dummyDialog;

        control.LoadConfigurations();

        if(!control.LoadUserDatabase()) {
            TC::MessageDialog::warning(p, "User Database", tr("It fails to load user database"));
            p->onSetupSystem();
        }
    }

    MainWindow::MainWindow(QWidget* parent) : IMainWindowHTX("Start", "Start", parent), d{new Impl(this)} {
        d->ui.setupUi(this);

        d->ui.versionLabel->setObjectName("bt-tsx-versionLabel");
        d->ui.setupBtn->setObjectName("bt-tsx-serviceBtn");
        d->ui.serviceBtn->setObjectName("bt-tsx-administratorBtn");
        d->ui.registerBtn->setObjectName("bt-tsx-registerBtn");

        d->ui.loginPanelStack->setCurrentIndex(0);
        d->ui.serviceBtn->setVisible(false);

        InitUI();

        d->ui.centralwidget->setObjectName("homeCentralWidget");
        d->ui.centralwidget->setStyleSheet("QWidget#homeCentralWidget {background-image: url(:/appPlugin/general.start/images/home.png);}");

        d->ui.tomocubeLogo->setPixmap(QPixmap(":/appPlugin/general.start/images/logo-tomocube.png"));
        d->ui.tomoStudioXLogo->setPixmap(QPixmap(":/appPlugin/general.start/images/logo-tomostudiox.svg"));
        d->ui.versionLabel->setText(QString("ver.%1").arg(AppEntity::System::GetSoftwareVersion()));

        d->configObserver = new ConfigObserver(this);
        d->userObserver = new UserObserver(this);

        connect(d->ui.loginPanel, SIGNAL(sigLogin(const QString&, const QString&)), this,
                SLOT(onLogin(const QString&, const QString&)));
        connect(d->ui.loggedInPanel, SIGNAL(sigLogoff()), this, SLOT(onLogOff()));
        connect(d->ui.loggedInPanel, SIGNAL(sigStart()), this, SLOT(onStart()));
        connect(d->ui.setupBtn, SIGNAL(clicked()), this, SLOT(onSetupSystem()));
        connect(d->ui.serviceBtn, SIGNAL(clicked()), this, SLOT(onMaintenance()));
        connect(d->ui.registerBtn, SIGNAL(clicked()), this, SLOT(onRegister()));
        connect(d->configObserver, SIGNAL(sigError(const QString&)), this, SLOT(onSystemConfigError(const QString&)));
        connect(d->userObserver, SIGNAL(sigLoginFailed(const QString&)), this, SLOT(onLoginFailed(const QString&)));
        connect(d->userObserver, SIGNAL(sigLoginSuccess()), this, SLOT(onLoginSuccess()));
        connect(d->userObserver, SIGNAL(sigLogOffFailed(const QString&)), this, SLOT(onLogOffFailed(const QString&)));
        connect(d->userObserver, SIGNAL(sigLogOffSuccess()), this, SLOT(onLogOffSuccess()));
        connect(d->userObserver, SIGNAL(sigAddUserFailed(const QString&)), this, SLOT(onAddUserFailed(const QString&)));

        connect(&d->registry.timer, SIGNAL(timeout()), this, SLOT(onUpdateRegistry()));

        subscribeEvent("TabChange");
        connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(onHandleEvent(ctkEvent)));

        d->dummyDialog = new QDialog(this);
        d->dummyDialog->setModal(false);
        d->dummyDialog->show();
        d->dummyDialog->hide();

        d->registry.timer.start(60000);
    }
    
    MainWindow::~MainWindow() {        
    }

    auto MainWindow::TryActivate() -> bool {
        return true;
    }
    auto MainWindow::TryDeactivate() -> bool {
        return true;
    }
    auto MainWindow::IsActivate() -> bool {
        return true;
    }
    auto MainWindow::GetMetaInfo() -> QVariantMap {
        return d->appProperties;
    }

    auto MainWindow::Execute(const QVariantMap& params) -> bool {
        d->ui.loginPanel->setFocus(Qt::FocusReason::TabFocusReason);
        d->loadConfiguration();
        return false;
    }

    void MainWindow::resizeEvent(QResizeEvent* event) {
    }

    auto MainWindow::InitUI()->bool {
        return true;
    }

    void MainWindow::onHandleEvent(const ctkEvent& ctkEvent) {
        
    }

    auto MainWindow::onLogin(const QString& id, const QString& password) -> void {
        d->control.Login(id, password);
    }

    void MainWindow::onLogOff() {
        d->control.LogOff();
    }

    void MainWindow::onStart() {
        AppEvent appEvt(AppTypeEnum::APP_WITHOUT_ARG);
        appEvt.setFullName("org.tomocube.experiment.setup");
        appEvt.setAppName("Experiment Setup");

        publishSignal(appEvt, "Start");
    }

    auto MainWindow::onSetupSystem() -> void {
        auto session = AppEntity::SessionManager::GetInstance();
        if(!session->LoggedIn()) {
            d->ui.loginPanel->setFocus(Qt::FocusReason::TabFocusReason);
            d->ui.loginPanel->ShowError(tr("Please log in first"));
            return;
        }

        AppEvent appEvt(AppTypeEnum::APP_WITHOUT_ARG);
        appEvt.setFullName("org.tomocube.system.setup");
        appEvt.setAppName("System Setup");

        publishSignal(appEvt, "Start");
    }

    auto MainWindow::onMaintenance() -> void {
        AppEvent appEvt(AppTypeEnum::APP_WITHOUT_ARG);
        appEvt.setFullName("org.tomocube.system.maintenance");
        appEvt.setAppName("System Maintenance");

        publishSignal(appEvt, "Start");
    }

    auto MainWindow::onRegister() -> void {
        using Dialog = HTXpress::AppComponents::UserEdit::Dialog;

        auto session = AppEntity::SessionManager::GetInstance();
        if(!session->LoggedIn()) {
            d->ui.loginPanel->setFocus(Qt::FocusReason::TabFocusReason);
            d->ui.loginPanel->ShowError(tr("Please log in as an administrator first."));
            return;
        } else {
            auto profile = session->GetProfile();
            if(profile == +AppEntity::Profile::Operator) {
                TC::MessageDialog::information(this, "Register", tr("Operator can't register new user."));
                return;
            }
        }

        Dialog dialog(this);
        if(QDialog::Accepted != dialog.exec()) return;

        const auto name = dialog.GetName();
        const auto id = dialog.GetID();
        const auto password = dialog.GetPassword();
        const auto privilege = dialog.GetPrivilege();

        d->control.AddUser(name, id, password, privilege);
    }

    auto MainWindow::onSystemConfigError(const QString& message) -> void {
        TC::MessageDialog::warning(this, "System Configuration", message);
        onSetupSystem();
    }

    auto MainWindow::onLoginFailed(const QString& message) -> void {
        d->ui.loginPanel->ShowError(message);
    }

    auto MainWindow::onLoginSuccess() -> void {
        d->ui.loginPanel->ClearAll();
        d->ui.loginPanelStack->setCurrentIndex(1);

        auto session = AppEntity::SessionManager::GetInstance();
        bool serviceEngineer = session->GetProfile() == +AppEntity::Profile::ServiceEngineer;
        d->ui.serviceBtn->setVisible(serviceEngineer);
        if(serviceEngineer) d->registry.isService.Sign();
        else d->registry.isService.Unsign();
    }

    auto MainWindow::onLogOffFailed(const QString& message) -> void {
        TC::MessageDialog::warning(this, "Log off", message);
    }

    auto MainWindow::onLogOffSuccess() -> void {
        d->ui.loginPanelStack->setCurrentIndex(0);
        d->ui.serviceBtn->setVisible(false);
        d->registry.isService.Unsign();
    }

    auto MainWindow::onAddUserFailed(const QString& message) -> void {
        TC::MessageDialog::warning(this, "Add User", message);
    }

    auto MainWindow::onUpdateRegistry() -> void {
        auto session = AppEntity::SessionManager::GetInstance();
        bool serviceEngineer = session->GetProfile() == +AppEntity::Profile::ServiceEngineer;
        if(serviceEngineer) d->registry.isService.Sign();
        else d->registry.isService.Unsign();
    }
}
