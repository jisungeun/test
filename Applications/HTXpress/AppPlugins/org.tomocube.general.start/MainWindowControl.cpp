#include <QStandardPaths>

#include <System.h>
#include <SessionManager.h>
#include <UserManager.h>
#include <SystemConfigLoader.h>
#include <UserManagerPlugin.h>

#include <ConfigController.h>
#include <UserController.h>

#include "Internal/ConfigUpdater.h"
#include "Internal/UserUpdater.h"
#include "MainWindowControl.h"

namespace HTXpress::AppPlugins::General::Start::App {
    using SystemConfigLoaderPlugin = Plugins::SytemConfigLoader::Loader;
    using UserManagerPlugin = Plugins::UserManager::Plugin;

    struct MainWindowControl::Impl {
        std::shared_ptr<SystemConfigLoaderPlugin> systemConfigLoader{ new SystemConfigLoaderPlugin() };
        std::shared_ptr<UserManagerPlugin> userManager{ new UserManagerPlugin() };
    };
    
    MainWindowControl::MainWindowControl() : d{new Impl } {
        auto system = AppEntity::System::GetInstance();

        const auto topPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
        const auto configPath = QString("%1/config/system.ini").arg(topPath);
        const auto userdbPath = QString("%1/config/userdatabase.ini").arg(topPath);
        const auto backgroundPath = QString("%1/background").arg(topPath);
        const auto sampleTypeFolderPath = QString("%1/config/sampleType").arg(topPath);
        d->systemConfigLoader->SetPath(configPath);
        d->userManager->SetPath(userdbPath);
        system->SetBackgroundPath(backgroundPath);
        system->SetSampleTypeFolderPath(sampleTypeFolderPath);
    }
    
    MainWindowControl::~MainWindowControl() {
    }
    
    auto MainWindowControl::LoadConfigurations() -> bool {
        auto updater = ConfigUpdater::GetInstance();
        auto& presenter = Interactor::ConfigPresenter(updater.get());
        auto& controller = Interactor::ConfigController(&presenter);
        return controller.Load();
    }

    auto MainWindowControl::LoadUserDatabase() -> bool {
        auto controller = Interactor::UserController(nullptr);
        return controller.Load();
    }

    auto MainWindowControl::Login(const QString& id, const QString& password, AppEntity::Profile privilege) -> bool {
        auto updater = UserUpdater::GetInstance();
        auto presenter = Interactor::UserPresenter(updater.get());
        auto controller = Interactor::UserController(&presenter);
        return controller.Login(id, password, privilege);
    }

    auto MainWindowControl::LogOff() -> bool {
        auto updater = UserUpdater::GetInstance();
        auto presenter = Interactor::UserPresenter(updater.get());
        auto controller = Interactor::UserController(&presenter);
        return controller.LogOff();
    }

    auto MainWindowControl::AddUser(const QString& name, const QString& id, 
                                    const QString& password,
                                    AppEntity::Profile profile) -> bool {
        auto updater = UserUpdater::GetInstance();
        auto presenter = Interactor::UserPresenter(updater.get());
        auto controller = Interactor::UserController(&presenter);
        return controller.AddUser(name, id, password, profile);
    }
}
