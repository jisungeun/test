#pragma once
#include <memory>
#include <QString>

#include "HTX_General_Start_InteractorExport.h"

namespace HTXpress::AppPlugins::General::Start::Interactor {
    class HTX_General_Start_Interactor_API IUserView {
    public:
        IUserView();
        virtual ~IUserView();

        virtual auto LoginFailed(const QString& message)->void = 0;
        virtual auto LoginSuccess()->void = 0;
        virtual auto LogOffFailed(const QString& message)->void = 0;
        virtual auto LogOffSuccess()->void = 0;
        virtual auto AddUserFailed(const QString& message)->void = 0;
    };
}