#pragma once
#include <memory>

#include <IConfigOutputPort.h>
#include "IConfigView.h"
#include "HTX_General_Start_InteractorExport.h"

namespace HTXpress::AppPlugins::General::Start::Interactor {
    class HTX_General_Start_Interactor_API ConfigPresenter : public UseCase::IConfigOutputPort {
    public:
        ConfigPresenter(IConfigView* view);
        ~ConfigPresenter() override;

        auto UpdateSystemInfo(const QString& info) -> void override;
        auto NotifyLoadingFailure(const QString& message) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
