#pragma once
#include <memory>

#include "ConfigPresenter.h"
#include "HTX_General_Start_InteractorExport.h"

namespace HTXpress::AppPlugins::General::Start::Interactor {
    class HTX_General_Start_Interactor_API ConfigController {
    public:
        ConfigController(ConfigPresenter* presenter);
        ~ConfigController();

        auto Load()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}