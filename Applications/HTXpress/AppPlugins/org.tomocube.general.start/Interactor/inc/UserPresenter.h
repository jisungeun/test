#pragma once
#include <memory>

#include <IUserOutputPort.h>
#include "IUserView.h"
#include "HTX_General_Start_InteractorExport.h"

namespace HTXpress::AppPlugins::General::Start::Interactor {
    class HTX_General_Start_Interactor_API UserPresenter : public UseCase::IUserOutputPort {
    public:
        UserPresenter(IUserView* view);
        ~UserPresenter() override;

        auto LoginFailed(const QString& message) -> void override;
        auto LoginSuccess() -> void override;
        auto LogOffFailed(const QString& message) -> void override;
        auto LogOffSuccess() -> void override;
        auto AddUserFailed(const QString& message) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}