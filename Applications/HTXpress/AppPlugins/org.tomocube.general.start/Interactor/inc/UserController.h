#pragma once
#include <memory>

#include <AppEntityDefines.h>

#include "UserPresenter.h"
#include "HTX_General_Start_InteractorExport.h"

namespace HTXpress::AppPlugins::General::Start::Interactor {
    class HTX_General_Start_Interactor_API UserController {
    public:
        UserController(UserPresenter* presenter);
        ~UserController();

        auto Load()->bool;
        auto Login(const QString& id, const QString& password, AppEntity::Profile privilege)->bool;
        auto LogOff()->bool;
        auto AddUser(const QString& name, const QString& id, const QString& password, AppEntity::Profile profile)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}