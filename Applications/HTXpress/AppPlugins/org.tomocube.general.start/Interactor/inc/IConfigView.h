#pragma once
#include <memory>
#include <QString>

#include "HTX_General_Start_InteractorExport.h"

namespace HTXpress::AppPlugins::General::Start::Interactor {
    class HTX_General_Start_Interactor_API IConfigView {
    public:
        IConfigView();
        virtual ~IConfigView();

        virtual auto UpdateSystemInfo(const QString& info)->void = 0;
        virtual auto NotifyLoadingFailure(const QString& message)->void = 0;
    };
}