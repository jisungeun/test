#include "UserPresenter.h"

namespace HTXpress::AppPlugins::General::Start::Interactor {
    struct UserPresenter::Impl {
        IUserView* view{ nullptr };
    };

    UserPresenter::UserPresenter(IUserView* view) : UseCase::IUserOutputPort(), d{new Impl} {
        d->view = view;
    }

    UserPresenter::~UserPresenter() {
    }

    auto UserPresenter::LoginFailed(const QString& message) -> void {
        if(d->view) d->view->LoginFailed(message);
    }

    auto UserPresenter::LoginSuccess() -> void {
        if(d->view) d->view->LoginSuccess();
    }

    auto UserPresenter::LogOffFailed(const QString& message) -> void {
        if(d->view) d->view->LogOffFailed(message);
    }

    auto UserPresenter::LogOffSuccess() -> void {
        if(d->view) d->view->LogOffSuccess();
    }

    auto UserPresenter::AddUserFailed(const QString& message) -> void {
        if(d->view) d->view->AddUserFailed(message);
    }
}
