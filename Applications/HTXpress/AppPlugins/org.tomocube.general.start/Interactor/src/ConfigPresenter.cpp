#include "ConfigPresenter.h"

namespace HTXpress::AppPlugins::General::Start::Interactor {
    struct ConfigPresenter::Impl {
        IConfigView* view{ nullptr };
    };

    ConfigPresenter::ConfigPresenter(IConfigView* view) : UseCase::IConfigOutputPort(), d{new Impl} {
        d->view = view;
    }

    ConfigPresenter::~ConfigPresenter() {
    }

    auto ConfigPresenter::UpdateSystemInfo(const QString& info) -> void {
        d->view->UpdateSystemInfo(info);
    }

    auto ConfigPresenter::NotifyLoadingFailure(const QString& message) -> void {
        d->view->NotifyLoadingFailure(message);
    }
}
