#include <LoadConfiguration.h>

#include "ConfigController.h"

namespace HTXpress::AppPlugins::General::Start::Interactor {
    struct ConfigController::Impl {
        ConfigPresenter* presenter{ nullptr };
    };

    ConfigController::ConfigController(ConfigPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    ConfigController::~ConfigController() {
    }

    auto ConfigController::Load() -> bool {
        auto usecase = UseCase::LoadConfiguration(d->presenter);
        return usecase.Request();
    }
}