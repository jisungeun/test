#include <LoadUserDatabase.h>
#include <Login.h>
#include <LogOff.h>
#include <AddUser.h>

#include "UserController.h"

namespace HTXpress::AppPlugins::General::Start::Interactor {
    struct UserController::Impl {
        UserPresenter* presenter{ nullptr };
    };

    UserController::UserController(UserPresenter* presenter) : d{new Impl} {
        d->presenter = presenter;
    }

    UserController::~UserController() {
    }

    auto UserController::Load() -> bool {
        auto usecase = UseCase::LoadUserDatabase();
        return usecase.Request();
    }

    auto UserController::Login(const QString& id, const QString& password, AppEntity::Profile privilege) -> bool {
        auto usecase = UseCase::Login(d->presenter);
        usecase.Set(id, password, privilege);
        return usecase.Request();
    }

    auto UserController::LogOff() -> bool {
        auto usecase = UseCase::LogOff(d->presenter);
        return usecase.Request();
    }

    auto UserController::AddUser(const QString& name, const QString& id, 
                                 const QString& password,
                                 AppEntity::Profile profile) -> bool {
        auto usecase = UseCase::AddUser(d->presenter);
        usecase.Set(name, id, password, profile);
        return usecase.Request();
    }
}
