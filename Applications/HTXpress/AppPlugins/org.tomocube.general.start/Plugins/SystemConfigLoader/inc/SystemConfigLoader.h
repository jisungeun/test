#pragma once
#include <memory>
#include <QString>

#include <ISystemConfigLoader.h>
#include "HTX_General_Start_SystemConfigLoaderExport.h"

namespace HTXpress::AppPlugins::General::Start::Plugins::SytemConfigLoader {
    class HTX_General_Start_SystemConfigLoader_API Loader : public UseCase::ISystemConfigLoader {
    public:
        Loader();
        ~Loader() override;

        auto SetPath(const QString& path)->void;

        auto Read() -> bool override;
        auto GetErrorMessage() const -> QString override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
