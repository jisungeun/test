#define LOGGER_TAG "[SytemConfig]"
#include <QDir>
#include <QFileInfo>
#include <QSettings>
#include <QDateTime>

#include <TCLogger.h>
#include <System.h>
#include <ModelRepo.h>
#include <ModelLoader.h>
#include <VesselLoader.h>
#include <SystemConfigIOReader.h>
#include <FLLightIOReader.h>
#include <FLEmissionIOReader.h>
#include <MediumDataRepo.h>
#include <MediumIOReader.h>
#include <ImagingProfileLoader.h>

#include "SystemConfigLoader.h"

namespace HTXpress::AppPlugins::General::Start::Plugins::SytemConfigLoader {
    struct Loader::Impl {
        const char* metaFile = ".datafolder.tsx";

        QString path;
        QString error;

        auto SetError(const QString& message)->void;
        auto IsDataFolder(const QString& dataPath)->bool;
        auto SetDataFolder(const QString& dataPath)->void;
    };

    auto Loader::Impl::SetError(const QString& message) -> void {
        error = message;
        QLOG_ERROR() << message;
    }

    auto Loader::Impl::IsDataFolder(const QString& dataPath) -> bool {
        QDir dir(dataPath);
        return dir.exists(metaFile);
    }

    auto Loader::Impl::SetDataFolder(const QString& dataPath) -> void {
        if(IsDataFolder(dataPath)) return;

        QFile file(QString("%1/%2").arg(dataPath).arg(metaFile));
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return;
        QTextStream out(&file);
        out << QDateTime::currentDateTime().toString("yyyyMMdd.hhmmss");
    }


    Loader::Loader() : UseCase::ISystemConfigLoader(), d{new Impl} {
    }
    
    Loader::~Loader() {
    }

    auto Loader::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto Loader::Read() -> bool {
        auto config = std::make_shared<AppEntity::SystemConfig>();
        auto reader = AppComponents::SystemConfigIO::Reader();
        if(!reader.Read(d->path, config)) {
            d->SetError(reader.GetError());
            return false;
        }

        QFileInfo finfo(d->path);
        const auto configFolder = finfo.absolutePath();

        // fl lights
        if(!AppComponents::FLLightIO::Reader().Read(configFolder, config)) {
            d->SetError(QString("It fails to read FL light channel information. %1").arg(configFolder));
            return false;
        }

        // fl emission
        if(!AppComponents::FLEmissionIO::Reader().Read(configFolder, config)) {
            d->SetError(QString("It fails to read FL emission filter channel information. %1").arg(configFolder));
            return false;
        }

        // media
        auto mediumRepo = AppEntity::MediumDataRepo::GetInstance();
        if(!AppComponents::MediumIO::Reader().Read(configFolder, mediumRepo)) {
            d->SetError(QString("It fails to read medium information. %1").arg(configFolder));
            return false;
        }

        //Load models
        auto modelLoader = AppComponents::ModelLoader::Loader();
        if(!modelLoader.Load(configFolder)) {
            d->SetError(QString("It fails to load model files. \n%1").arg(modelLoader.GetError()));
            return false;
        }

        auto model = AppEntity::ModelRepo::GetInstance()->GetModel(config->GetModel());
        if(!model) {
            d->SetError(QString("No model exists - Model: %1").arg(config->GetModel()));
            config->SetModel("Invalid");
            return false;
        } else {
            QLOG_INFO() << "Model: " << model->Name();
        }

        //Load vessels
        const auto vesselFolder = [=]()->QString {
            const auto path = QString("%1/vessels").arg(configFolder);
            if(QFile::exists(path)) return path;
            return configFolder;
        }();
        AppComponents::VesselLoader::Loader::Load(vesselFolder);

        auto system = AppEntity::System::GetInstance();

        auto imagingProfileLoader = AppComponents::ImagingProfileLoader::ImagingProfileLoader();
        imagingProfileLoader.AddToProfileRepo(system->GetSampleTypeFolderPath());

        system->SetSystemConfig(config);
        system->SetModel(model);

        QLOG_INFO() << "Data directory: " << config->GetDataDir();
        if(config->GetDataDir().isEmpty()) {
            d->SetError("No data directory path is specified in system configuration.");
            return false;
        }

        finfo = QFileInfo(config->GetDataDir());
        if(finfo.isRelative()) {
            d->SetError(QString("Data directory should be absolute path. %1").arg(config->GetDataDir()));
            return false;
        }

        if(!QFileInfo::exists(config->GetDataDir())) {
            if(!QDir().mkpath(config->GetDataDir())) {
                d->SetError(QString("It fails to create data directory : %1").arg(config->GetDataDir()));
                return false;
            }
        }

        auto dir = QDir(config->GetDataDir());
        if(!dir.exists("Default")) {
            if(!dir.mkdir("Default")) {
                d->SetError(QString("It fails to to create default user directory : %1/Default").arg(config->GetDataDir()));
                return false;
            }
        }

        d->SetDataFolder(config->GetDataDir());

        QLOG_INFO() << "All system configurations are loaded";

        return true;
    }

    auto Loader::GetErrorMessage() const -> QString {
        return d->error;
    }
}
