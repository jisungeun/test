#include <QDate>

#include <UserDatabase.h>
#include <UserManager.h>
#include <OtpGenerator.h>

#include "UserManagerPlugin.h"

namespace HTXpress::AppPlugins::General::Start::Plugins::UserManager {
    using UserDatbase = AppComponents::UserDatabase::Component;
    using Manager = AppEntity::UserManager;

    struct Plugin::Impl {
        QString path;

        auto CheckServiceLogin(const QString& password) const->bool;
    };

    auto Plugin::Impl::CheckServiceLogin(const QString& password) const -> bool {
        const int otpLength = 8;
        const QString otpKey = "9z$C&F)J@NcRfUjXn2r5u7x!A%D*G-KaPdSgVkYp3s6v9y/B?E(H+MbQeThWmZq4";

        const auto year = QDate::currentDate().year();
        auto generator = TC::OtpGenerator(otpLength, otpKey);
        auto otp = generator.Generate(QString::number(year));

        return password.compare(otp, Qt::CaseInsensitive) == 0;
    }

    Plugin::Plugin() : UseCase::IUserManager(), d{new Impl} {
    }

    Plugin::~Plugin() {
    }

    auto Plugin::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto Plugin::Load() -> bool {
        return UserDatbase::GetInstance()->Load(d->path);
    }

    auto Plugin::IsValid(const AppEntity::UserID& id, const QString& password) const -> bool {
        auto manager = Manager::GetInstance();
        auto user = manager->GetUser(id);
        if(user == nullptr) return false;

        if(user->GetProfile() == +AppEntity::Profile::ServiceEngineer) return d->CheckServiceLogin(password);

        return UserDatbase::GetInstance()->IsValid(id, password);
    }

    auto Plugin::GetUser(const AppEntity::UserID& id) const -> AppEntity::User::Pointer {
        auto manager = Manager::GetInstance();
        return manager->GetUser(id);
    }

    auto Plugin::AddUser(const AppEntity::User::Pointer user, const QString& password) -> bool {
        auto database = UserDatbase::GetInstance();
        return database->AddUser(user, password);
    }
}
