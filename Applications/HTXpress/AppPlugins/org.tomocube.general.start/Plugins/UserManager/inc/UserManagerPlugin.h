#pragma once
#include <memory>

#include <IUserManager.h>
#include "HTX_General_Start_UserManagerExport.h"

namespace HTXpress::AppPlugins::General::Start::Plugins::UserManager {
    class HTX_General_Start_UserManager_API Plugin : public UseCase::IUserManager {
    public:
        Plugin();
        ~Plugin() override;

        auto SetPath(const QString& path)->void;

        auto Load() -> bool override;
        auto IsValid(const AppEntity::UserID& id, const QString& password) const -> bool override;
        auto GetUser(const AppEntity::UserID& id) const -> AppEntity::User::Pointer override;
        auto AddUser(const AppEntity::User::Pointer user, const QString& password) -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}