#pragma once
#include <memory>

#include <IUseCaseLogger.h>
#include "HTX_General_Start_UseCaseLoggerExport.h"

namespace HTXpress::AppPlugins::General::Start::Plugins::UseCaseLogger {
    class HTX_General_Start_UseCaseLogger_API Logger : public UseCase::IUseCaseLogger {
    public:
        Logger();
        ~Logger() override;

    protected:
        auto Log(const QString& useCase, const QString& message)->void override;
        auto Error(const QString& useCase, const QString& message)->void override;
    };
}