#include "UserUpdater.h"
#include "UserObserver.h"

namespace HTXpress::AppPlugins::General::Start::App {
    UserObserver::UserObserver(QObject* parent) : QObject(parent) {
        UserUpdater::GetInstance()->Register(this);
    }

    UserObserver::~UserObserver() {
        UserUpdater::GetInstance()->Deregister(this);
    }

    auto UserObserver::UpdateLoginFailed(const QString& message) -> void {
        emit sigLoginFailed(message);
    }

    auto UserObserver::UpdateLoginSuccess() -> void {
        emit sigLoginSuccess();
    }

    auto UserObserver::UpdateLogOffFailed(const QString& message) -> void {
        emit sigLogOffFailed(message);
    }

    auto UserObserver::UpdateLogOffSuccess() -> void {
        emit sigLogOffSuccess();
    }

    auto UserObserver::UpdateAddUserFailed(const QString& message) -> void {
        emit sigAddUserFailed(message);
    }
}
