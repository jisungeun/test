#include <QCloseEvent>
#include <QKeyEvent>

#include "ui_NewUserDialog.h"
#include "NewUserDialog.h"

namespace HTXpress::AppPlugins::General::Start::App {
    struct NewUserDialog::Impl {
        Ui::NewUserDialog ui;
    };

    NewUserDialog::NewUserDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
		setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

        d->ui.setupUi(this);
        d->ui.password->setEchoMode(QLineEdit::EchoMode::Password);
        d->ui.password2->setEchoMode(QLineEdit::EchoMode::Password);
        d->ui.message->setText("");
        d->ui.title->setObjectName("tc-newuserdialog-title");
        d->ui.labelName->setObjectName("tc-newuserdialog-label");
        d->ui.labelID->setObjectName("tc-newuserdialog-label");
        d->ui.labelPassword->setObjectName("tc-newuserdialog-label");
        d->ui.labelPrivilege->setObjectName("tc-newuserdialog-label");
        d->ui.message->setObjectName("tc-newuserdialog-message");

        d->ui.name->SetPlaceholderOpacity(0.3);
        d->ui.id->SetPlaceholderOpacity(0.3);
        d->ui.password->SetPlaceholderOpacity(0.3);
        d->ui.password2->SetPlaceholderOpacity(0.3);

        for(auto profile : AppEntity::Profile::_values()) {
            if(profile == +AppEntity::Profile::ServiceEngineer) continue;
            d->ui.privilege->addItem(profile._to_string(), profile._to_integral());
        }
    }

    NewUserDialog::~NewUserDialog() {
    }

    auto NewUserDialog::GetName() const -> QString {
        return d->ui.name->text();
    }

    auto NewUserDialog::GetID() const -> QString {
        return d->ui.id->text();
    }

    auto NewUserDialog::GetPassword() const -> QString {
        return d->ui.password->text();
    }

    auto NewUserDialog::GetPrivilege() const -> AppEntity::Profile {
        return AppEntity::Profile::_from_integral(d->ui.privilege->currentData().toInt());
    }

    void NewUserDialog::done(int retCode) {
        if(retCode == QDialog::Accepted) {
            if(d->ui.id->text().isEmpty()) {
                d->ui.message->setText(tr("User ID is blank"));
                return;
            }

            if(d->ui.password->text().isEmpty()) {
                d->ui.message->setText(tr("Password is blank"));
                return;
            }

            if(d->ui.password->text() != d->ui.password2->text()) {
                d->ui.message->setText(tr("Please enter password again"));
                return;
            }
        }

        QDialog::done(retCode);
    }

    void NewUserDialog::keyPressEvent(QKeyEvent* event) {
        switch (event->key()) {
		case Qt::Key_Enter:
		case Qt::Key_Return:
            return;
		}
        QDialog::keyPressEvent(event);
    }

    void NewUserDialog::focusOutEvent(QFocusEvent* event) {
        d->ui.name->setFocus(Qt::FocusReason::TabFocusReason);
    }
}
