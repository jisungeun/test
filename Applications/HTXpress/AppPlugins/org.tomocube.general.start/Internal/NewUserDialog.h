#pragma once
#include <memory>
#include <QDialog>

#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::General::Start::App {
    class NewUserDialog : public QDialog {
        Q_OBJECT

    public:
        NewUserDialog(QWidget* parent);
        ~NewUserDialog() override;

        auto GetName() const->QString;
        auto GetID() const->QString;
        auto GetPassword() const->QString;
        auto GetPrivilege() const->AppEntity::Profile;

    public slots:
        void done(int retCode) override;

    protected:
        void keyPressEvent(QKeyEvent* event) override;
        void focusOutEvent(QFocusEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}