#include "ConfigUpdater.h"
#include "ConfigObserver.h"

namespace HTXpress::AppPlugins::General::Start::App {
    ConfigObserver::ConfigObserver(QObject* parent) : QObject(parent) {
        ConfigUpdater::GetInstance()->Register(this);
    }

    ConfigObserver::~ConfigObserver() {
        ConfigUpdater::GetInstance()->Deregister(this);
    }

    auto ConfigObserver::UpdateSystemInfo(const QString& info) -> void {
        emit sigUpdateSystemInfo(info);
    }

    auto ConfigObserver::NotifyLoadingFailure(const QString& message) -> void {
        emit sigError(message);
    }
}
