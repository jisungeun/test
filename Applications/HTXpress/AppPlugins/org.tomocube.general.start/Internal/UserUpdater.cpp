#include <QList>

#include "UserObserver.h"
#include "UserUpdater.h"

namespace HTXpress::AppPlugins::General::Start::App {
    struct UserUpdater::Impl {
        QList<UserObserver*> observers;
    };

    UserUpdater::UserUpdater() : Interactor::IUserView(), d{new Impl} {
    }

    UserUpdater::~UserUpdater() {
    }

    auto UserUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new UserUpdater() };
        return theInstance;
    }

    auto UserUpdater::LoginFailed(const QString& message) -> void {
        for(auto observer : d->observers) {
            observer->UpdateLoginFailed(message);
        }
    }

    auto UserUpdater::LoginSuccess() -> void {
        for(auto observer : d->observers) {
            observer->UpdateLoginSuccess();
        }
    }

    auto UserUpdater::LogOffFailed(const QString& message) -> void {
        for(auto observer : d->observers) {
            observer->UpdateLogOffFailed(message);
        }
    }

    auto UserUpdater::LogOffSuccess() -> void {
        for(auto observer : d->observers) {
            observer->UpdateLogOffSuccess();
        }
    }

    auto UserUpdater::AddUserFailed(const QString& message) -> void {
        for(auto observer : d->observers) {
            observer->UpdateAddUserFailed(message);
        }
    }

    auto UserUpdater::Register(UserObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto UserUpdater::Deregister(UserObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}