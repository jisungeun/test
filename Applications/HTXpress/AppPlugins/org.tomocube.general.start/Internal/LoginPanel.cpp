#include "ui_LoginPanel.h"
#include "LoginPanel.h"

namespace HTXpress::AppPlugins::General::Start::App {
    struct LoginPanel::Impl {
        Ui::LoginPanel ui;
    };

    LoginPanel::LoginPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);
        d->ui.message->setText("");
        d->ui.password->setEchoMode(QLineEdit::EchoMode::Password);

        d->ui.title->setObjectName("tc-loginpanel-title");
        d->ui.id->setObjectName("tc-loginpanel-id");
        d->ui.password->setObjectName("tc-loginpanel-password");
        d->ui.message->setObjectName("tc-loginpanel-message");
        d->ui.loginBtn->setObjectName("tc-loginpanel-loginbtn");

        d->ui.id->SetPlaceholderOpacity(0.3);
        d->ui.password->SetPlaceholderOpacity(0.3);

        connect(d->ui.loginBtn, SIGNAL(clicked()), this, SLOT(onLogin()));
        connect(d->ui.id, &QLineEdit::returnPressed, this, [=](){ d->ui.password->setFocus(Qt::FocusReason::TabFocusReason); });
        connect(d->ui.password, SIGNAL(returnPressed()), this, SLOT(onLogin()));
    }

    LoginPanel::~LoginPanel() {
    }

    auto LoginPanel::ShowError(const QString& message) -> void {
        d->ui.message->setText(message);
    }

    auto LoginPanel::ClearAll() -> void {
        d->ui.id->clear();
        d->ui.password->clear();
    }

    void LoginPanel::focusInEvent(QFocusEvent* event) {
        d->ui.id->setFocus(Qt::FocusReason::TabFocusReason);
    }

    void LoginPanel::onLogin() {
        d->ui.message->setText("");
        QCoreApplication::processEvents(QEventLoop::ProcessEventsFlag::ExcludeUserInputEvents, 20);
        emit sigLogin(d->ui.id->text(), d->ui.password->text());
    }
}