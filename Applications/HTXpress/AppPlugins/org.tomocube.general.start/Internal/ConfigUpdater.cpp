#include <QList>

#include "ConfigObserver.h"
#include "ConfigUpdater.h"

namespace HTXpress::AppPlugins::General::Start::App {
    struct ConfigUpdater::Impl {
        QList<ConfigObserver*> observers;
    };

    ConfigUpdater::ConfigUpdater() : Interactor::IConfigView(), d{new Impl} {
    }

    ConfigUpdater::~ConfigUpdater() {
    }

    auto ConfigUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new ConfigUpdater() };
        return theInstance;
    }

    auto ConfigUpdater::UpdateSystemInfo(const QString& info) -> void {
        for(auto* observer : d->observers) {
            observer->UpdateSystemInfo(info);
        }
    }

    auto ConfigUpdater::NotifyLoadingFailure(const QString& message) -> void {
        for(auto* observer : d->observers) {
            observer->NotifyLoadingFailure(message);
        }
    }

    auto ConfigUpdater::Register(ConfigObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto ConfigUpdater::Deregister(ConfigObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}