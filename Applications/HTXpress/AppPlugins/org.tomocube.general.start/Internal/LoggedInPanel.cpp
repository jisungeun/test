#include <SessionManager.h>

#include "Internal/UserObserver.h"

#include "ui_LoggedInPanel.h"
#include "LoggedInPanel.h"

namespace HTXpress::AppPlugins::General::Start::App {
    struct LoggedInPanel::Impl {
        Ui::LoggedInPanel ui;
        UserObserver* userObserver{ nullptr };
    };

    LoggedInPanel::LoggedInPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);

        d->ui.title->setObjectName("tc-loggedinpanel-title");
        d->ui.startBtn->setObjectName("tc-loggedinpanel-button");
        d->ui.logoffBtn->setObjectName("tc-loggedinpanel-button");

        d->userObserver = new UserObserver(this);

        connect(d->ui.startBtn, SIGNAL(clicked()), this, SIGNAL(sigStart()));
        connect(d->ui.logoffBtn, SIGNAL(clicked()), this, SIGNAL(sigLogoff()));
        connect(d->userObserver, SIGNAL(sigLoginSuccess()), this, SLOT(onLoginSuccess()));
    }

    LoggedInPanel::~LoggedInPanel() {
    }

    auto LoggedInPanel::onLoginSuccess() -> void {
        auto session = AppEntity::SessionManager::GetInstance();
        d->ui.title->setText(tr("Welcome %1").arg(session->GetName()));
    }
}
