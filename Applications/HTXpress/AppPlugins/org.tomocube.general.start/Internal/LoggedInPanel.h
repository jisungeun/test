#pragma once
#include <memory>
#include <QWidget>

namespace HTXpress::AppPlugins::General::Start::App {
    class LoggedInPanel : public QWidget {
        Q_OBJECT

    public:
        LoggedInPanel(QWidget* parent);
        ~LoggedInPanel() override;

    protected slots:
        auto onLoginSuccess()->void;

    signals:
        void sigLogoff();
        void sigStart();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}