#pragma once
#include <memory>
#include <QObject>

namespace HTXpress::AppPlugins::General::Start::App {
    class UserObserver : public QObject {
        Q_OBJECT

    public:
        UserObserver(QObject* parent);
        ~UserObserver() override;

        auto UpdateLoginFailed(const QString& message)->void;
        auto UpdateLoginSuccess()->void;
        auto UpdateLogOffFailed(const QString& message)->void;
        auto UpdateLogOffSuccess()->void;
        auto UpdateAddUserFailed(const QString& message)->void;

    signals:
        void sigLoginFailed(const QString& message);
        void sigLoginSuccess();
        void sigLogOffFailed(const QString& message);
        void sigLogOffSuccess();
        void sigAddUserFailed(const QString& message);
    };
}