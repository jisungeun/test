#pragma once
#include <memory>
#include <QWidget>

namespace HTXpress::AppPlugins::General::Start::App {
    class LoginPanel : public QWidget {
        Q_OBJECT

    public:
        LoginPanel(QWidget* parent);
        ~LoginPanel() override;

        auto ShowError(const QString& message)->void;
        auto ClearAll()->void;

    protected:
        void focusInEvent(QFocusEvent* event) override;

    protected slots:
        void onLogin();

    signals:
        void sigLogin(const QString& id, const QString& password);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}