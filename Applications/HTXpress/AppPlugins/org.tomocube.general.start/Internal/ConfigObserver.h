#pragma once
#include <memory>
#include <QObject>

namespace HTXpress::AppPlugins::General::Start::App {
    class ConfigObserver : public QObject {
        Q_OBJECT

    public:
        ConfigObserver(QObject* parent);
        ~ConfigObserver() override;

        auto UpdateSystemInfo(const QString& info)->void;
        auto NotifyLoadingFailure(const QString& message)->void;

    signals:
        void sigUpdateSystemInfo(const QString& info);
        void sigError(const QString& message);
    };
}