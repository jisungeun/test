#pragma once
#include <memory>

#include <IUserView.h>

namespace HTXpress::AppPlugins::General::Start::App {
    class UserObserver;

    class UserUpdater : public Interactor::IUserView {
    public:
        using Pointer = std::shared_ptr<UserUpdater>;

    public:
        UserUpdater();
        ~UserUpdater() override;

        static auto GetInstance()->Pointer;

        auto LoginFailed(const QString& message) -> void override;
        auto LoginSuccess() -> void override;
        auto LogOffFailed(const QString& message) -> void override;
        auto LogOffSuccess() -> void override;
        auto AddUserFailed(const QString& message) -> void override;

    protected:
        auto Register(UserObserver* observer)->void;
        auto Deregister(UserObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class UserObserver;
    };
}
