#pragma once
#include <memory>

#include <IConfigView.h>

namespace HTXpress::AppPlugins::General::Start::App {
    class ConfigObserver;

    class ConfigUpdater : public Interactor::IConfigView {
    public:
        using Pointer = std::shared_ptr<ConfigUpdater>;

    public:
        ConfigUpdater();
        ~ConfigUpdater() override;

        static auto GetInstance()->Pointer;

        auto UpdateSystemInfo(const QString& info) -> void override;
        auto NotifyLoadingFailure(const QString& message) -> void override;

    protected:
        auto Register(ConfigObserver* observer)->void;
        auto Deregister(ConfigObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class ConfigObserver;
    };
}