#pragma once
#include <memory>
#include <IMainWindowHTX.h>

namespace HTXpress::AppPlugins::General::Start::App {
    class MainWindow : public AppComponents::Framework::IMainWindowHTX {
        Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = nullptr);
        virtual ~MainWindow();

        auto TryActivate() -> bool final;
        auto TryDeactivate() -> bool final;
        auto IsActivate() -> bool final;
        auto GetMetaInfo()->QVariantMap final;

        auto Execute(const QVariantMap& params) -> bool override;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    signals:
        void sigNewExperiment();
        void sigOpenExperiment();

    private:
        auto InitUI(void)->bool;
        
    protected slots:
        void onHandleEvent(const ctkEvent& ctkEvent);
        void onLogin(const QString& id, const QString& password);
        void onLogOff();
        void onStart();
        auto onSetupSystem()->void;
        auto onMaintenance()->void;
        auto onRegister()->void;
        auto onSystemConfigError(const QString& message)->void;
        auto onLoginFailed(const QString& message)->void;
        auto onLoginSuccess()->void;
        auto onLogOffFailed(const QString& message)->void;
        auto onLogOffSuccess()->void;
        auto onAddUserFailed(const QString& message)->void;
        auto onUpdateRegistry()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
