project(org_tomocube_general_start)

find_package(CTK REQUIRED)

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE	${CMAKE_BINARY_DIR}/bin/Release/PluginApp)
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG	${CMAKE_BINARY_DIR}/bin/Debug/PluginApp)
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY_RELEASE	${CMAKE_BINARY_DIR}/lib/Release/PluginApp)
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY_DEBUG	${CMAKE_BINARY_DIR}/lib/Debug/PluginApp)
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY_RELEASE	${CMAKE_BINARY_DIR}/lib/Release/PluginApp)
set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY_DEBUG	${CMAKE_BINARY_DIR}/lib/Debug/PluginApp)

set(PLUGIN_export_directive "${PROJECT}_EXPORT")

set(PLUGIN_HEADERS
   MainWindowControl.h
)

set(PLUGIN_SRCS
	Plugin.cpp
	MainWindow.cpp
    MainWindowControl.cpp
)

set(PLUGIN_MOC_SRCS
	Plugin.h
	MainWindow.h
)

set(PLUGIN_UI_FORMS
	MainWindow.ui
    
    #Login Panels
    internal/LoginPanel.ui
    internal/LoggedInPanel.ui
)

set(PLUGIN_resources
    internal/resource/images.qrc
)

set(INTERNAL_HEADERS
    #Config
    internal/ConfigUpdater.h
    internal/ConfigObserver.h
    
    #User
    internal/UserUpdater.h
    internal/UserObserver.h
    
    #Login Panels
    internal/LoginPanel.h
    internal/LoggedInPanel.h
)

set(INTERNAL_SOURCES
    #Config
    internal/ConfigUpdater.cpp
    internal/ConfigObserver.cpp
    
    #User
    internal/UserUpdater.cpp
    internal/UserObserver.cpp
    
    #Login Panels
    internal/LoginPanel.cpp
    internal/LoggedInPanel.cpp
)

ctkFunctionGetTargetLibraries(PLUGIN_target_libraries)

ctkMacroBuildPlugin(
  NAME ${PROJECT_NAME}
  EXPORT_DIRECTIVE ${PLUGIN_export_directive}
  SRCS ${PLUGIN_HEADERS} ${PLUGIN_SRCS} ${PLUGIN_MOC_SRCS} ${INTERNAL_HEADERS} ${INTERNAL_SOURCES}
  MOC_SRCS ${PLUGIN_MOC_SRCS}
  UI_FORMS ${PLUGIN_UI_FORMS}
  RESOURCES ${PLUGIN_resources}      
  TARGET_LIBRARIES 
    ${PLUGIN_target_libraries} 
    ${QT_LIBRARIES} ${CTK_LIBRARIES} 
    TC::Components::RegistryJwt
    HTXpress::AppComponents::Framework 
    HTXpress::AppComponents::UserEditDialog
    HTXpress::AppPlugins::general.start::Interactor
    HTXpress::AppPlugins::general.start::Plugins::SystemConfigLoader
    HTXpress::AppPlugins::general.start::Plugins::UserManager
 )

target_include_directories(${PROJECT_NAME}    
    PRIVATE     
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_CURRENT_SOURCE_DIR}/src   
        ${CTK_INCLUDE_DIRS} 
)

target_link_directories(${PROJECT_NAME}
	PRIVATE		
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/HTXpress/AppPlugins/general.start/App") 

install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR}/PluginApp COMPONENT application_htx)
	
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

add_subdirectory(BusinessRule)
add_subdirectory(Interactor)
add_subdirectory(Plugins)