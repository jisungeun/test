#include <QStandardPaths>

#include <InstrumentPlugin.h>
#include <ChannelConfigIOPlugin.h>
#include <UseCaseLogger.h>
#include <SystemConfigWriter.h>

#include <InstrumentController.h>

#include "MainWindowControl.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    using InstrumentPlugin = Plugins::Instrument::Instrument;
    using ChannelConfigIOPlugin = Plugins::ChannelConfigIO::Plugin;
    using UseCaseLoggerPlugin = Plugins::UseCaseLogger::Logger;
    using SystemConfigWriter = Plugins::SystemConfigWriter::Writer;

    struct MainWindowControl::Impl {
        std::shared_ptr<InstrumentPlugin> instrument{ new InstrumentPlugin() };
        std::shared_ptr<ChannelConfigIOPlugin> channelConfigIO{ new ChannelConfigIOPlugin() };
        std::shared_ptr<UseCaseLoggerPlugin> usecaseLogger{ new UseCaseLoggerPlugin() };
        std::shared_ptr<SystemConfigWriter> sysConfigWriter{ new SystemConfigWriter() };
    };
    
    MainWindowControl::MainWindowControl() : d{new Impl } {
        const auto topPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
        const auto configFolder = QString("%1/config").arg(topPath);

        d->sysConfigWriter->SetPath(QString("%1/system.ini").arg(configFolder));
        d->channelConfigIO->SetTopPath(configFolder);
    }
    
    MainWindowControl::~MainWindowControl() {
    }

    auto MainWindowControl::CleanUp() -> void {
        d->instrument->CleanUp();
    }

    auto MainWindowControl::SetCondenserPosition(double position) -> bool {
        auto controller = Interactor::InstrumentController();
        return controller.SetCondenserPosition(position);
    }

    auto MainWindowControl::SetHTIntensity(int32_t intensity) -> bool {
        auto controller = Interactor::InstrumentController();
        return controller.SetHTIntensity(intensity);
    }
}
