#pragma once
#include <memory>

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

        auto CleanUp()->void;

        auto SetCondenserPosition(double position)->bool;
        auto SetHTIntensity(int32_t intensity)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}