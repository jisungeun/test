#pragma once
#include <QWidget>

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class ISubPage : public QWidget {
        Q_OBJECT
    public:
        ISubPage(QWidget* parent = nullptr);
        ~ISubPage() override;

        virtual auto Initialize()->bool = 0;
        virtual auto IsClosable()->bool = 0;
        virtual auto IsModified() const->bool = 0;
        virtual auto IsRestorable() const->bool = 0;
        virtual auto Restore()->bool = 0;
        virtual auto Save()->bool = 0;
    };
}
