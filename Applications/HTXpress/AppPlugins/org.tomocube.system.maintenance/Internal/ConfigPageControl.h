#pragma once
#include <memory>
#include <QStringList>
#include <QSet>
#include <QMap>

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class ConfigPageControl {
    public:
        struct Config {
            struct General {
                QString model;
                QString serial;
                QString dataFolder;

                struct ComInfo {
                    int32_t port;
                    int32_t baudrate;
                } mcu;
            } general;

            struct Parameters {
                struct VesselCenter {
                    double x;
                    double y;

                    auto operator==(const VesselCenter& rhs) const->bool {
                        if(x != rhs.x) return false;
                        if(y != rhs.y) return false;
                        return true;
                    }

                    auto operator!=(const VesselCenter& rhs) const->bool {
                        return !(*this == rhs);
                    }
                } vesselCenter;

                double overlap;

                struct FLRange {
                    int32_t min;
                    int32_t max;

                    auto operator==(const FLRange& rhs) const->bool {
                        if(min != rhs.min) return false;
                        if(max != rhs.max) return false;
                        return true;
                    }

                    auto operator!=(const FLRange& rhs) const->bool {
                        return !(*this == rhs);
                    }
                } flOutputRange;
            } parameters;

            struct HTScan {
                QList<double> NAs;
                QMap<double, int32_t> patternIndexes;

                struct ScanParameter {
                    int32_t step;
                    int32_t slices;

                    auto operator==(const ScanParameter& rhs) const->bool {
                        if(step != rhs.step) return false;
                        if(slices != rhs.slices) return false;
                        return true;
                    }

                    auto operator!=(const ScanParameter& rhs) const->bool {
                        return !(*this == rhs);
                    }
                };
                QMap<double, ScanParameter> scanParameters;

                struct CalParameter {
                    int32_t start;
                    int32_t step;
                    int32_t threshold;

                    auto operator==(const CalParameter& rhs) const->bool {
                        if(start != rhs.start) return false;
                        if(step != rhs.step) return false;
                        if(threshold != rhs.threshold) return false;
                        return true;
                    };

                    auto operator!=(const CalParameter& rhs) const->bool {
                        return !(*this == rhs);
                    }
                };
                QMap<double, CalParameter> calParameters;
            } htScan;

            struct AutoFocus {
                int32_t lensId;
                int32_t inFocusRange;
                int32_t direction;
                int32_t loopInterval;
                int32_t maxTrialCount;
                int32_t sensorValueMin;
                int32_t sensorValueMax;
                int32_t resolutionMul;
                int32_t resolutionDiv;
                int32_t defaultTargetValue;
                int32_t maxCompensationPulse;
                double scanReadyMm;

                auto operator==(const AutoFocus& rhs) const->bool {
                    if(lensId != rhs.lensId) return false;
                    if(inFocusRange != rhs.inFocusRange) return false;
                    if(direction != rhs.direction) return false;
                    if(loopInterval != rhs.loopInterval) return false;
                    if(maxTrialCount != rhs.maxTrialCount) return false;
                    if(sensorValueMin != rhs.sensorValueMin) return false;
                    if(sensorValueMax != rhs.sensorValueMax) return false;
                    if(resolutionMul != rhs.resolutionMul) return false;
                    if(resolutionDiv != rhs.resolutionDiv) return false;
                    if(defaultTargetValue != rhs.defaultTargetValue) return false;
                    if(maxCompensationPulse != rhs.maxCompensationPulse) return false;
                    if(scanReadyMm != rhs.scanReadyMm) return false;
                    return true;
                }

                auto operator!=(const AutoFocus& rhs) const->bool {
                    return !(*this == rhs);
                }
            } autoFocus;

            struct CAFParameter {
                int32_t patternIndex;
                int32_t intensity;
                double zCompensationOffset;

                auto operator==(const CAFParameter& rhs) const->bool {
                    if(patternIndex != rhs.patternIndex) return false;
                    if(intensity != rhs.intensity) return false;
                    if(zCompensationOffset != rhs.zCompensationOffset) return false;
                    return true;
                }

                auto operator!=(const CAFParameter& rhs) const->bool {
                    return !(*this == rhs);
                }
            };
            QMap<double, CAFParameter> cafParameters;

            struct CAFScanParameter {
                bool overriden{ false };
                int32_t startPos{ 0 };
                int32_t interval{ 0 };
                int32_t slices{ 0 };

                auto operator==(const CAFScanParameter& rhs) const->bool {
                    if(overriden !=rhs.overriden) return false;
                    if(overriden) {
                        if(startPos != rhs.startPos) return false;
                        if(interval != rhs.interval) return false;
                        if(slices != rhs.slices) return false;
                    }
                    return true;
                }

                auto operator!=(const CAFScanParameter& rhs) const->bool {
                    return !(*this == rhs);
                }
            } cafScanParameters;

            struct PreviewParameter {
                int32_t intensity{ 0 };
                double coeffA{ 0 };
                double coeffB{ 0 };
                double coeffP{ 0 };
                double coeffQ{ 0 };

                auto operator==(const PreviewParameter& rhs) const->bool {
                    if (intensity != rhs.intensity) return false;
                    if (coeffA != rhs.coeffA) return false;
                    if (coeffB != rhs.coeffB) return false;
                    if (coeffP != rhs.coeffP) return false;
                    if (coeffQ != rhs.coeffQ) return false;
                    return true;
                }

                auto operator!=(const PreviewParameter& rhs) const->bool {
                    return !(*this == rhs);
                }
            } previewParameters;

            auto operator==(const Config& rhs) const->bool;
            auto operator!=(const Config& rhs) const->bool;
        };

    public:
        ConfigPageControl();
        ~ConfigPageControl();

        auto Initialize()->void;
        auto GetConfig() const->Config;
        auto SetConfig(const Config& config)->bool;
        auto IsModified(const Config& config) const->bool;

        auto GetAvailableModels()->QStringList;
        auto GetAvailablePorts()->QStringList;
        auto GetAvailableBaudrates()->QStringList;

        auto IsUsedDataFolder(const QString& path) const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
