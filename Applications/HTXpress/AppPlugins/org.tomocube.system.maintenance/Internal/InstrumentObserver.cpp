#include "InstrumentUpdater.h"
#include "InstrumentObserver.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    InstrumentObserver::InstrumentObserver(QObject* parent) : QObject(parent) {
        InstrumentUpdater::GetInstance()->Register(this);
    }

    InstrumentObserver::~InstrumentObserver() {
        InstrumentUpdater::GetInstance()->Deregister(this);
    }

    auto InstrumentObserver::UpdateFailed(const QString& message) -> void {
        emit sigFailed(message);
    }

    auto InstrumentObserver::UpdateProgress(double progress, const QString& message) -> void {
        emit sigProgress(progress, message);
    }

    auto InstrumentObserver::UpdateGlobalPosition(AppEntity::Axis axis, double posInMm) -> void {
        emit sigUpdateGlobalPosition(axis, posInMm);
    }
}
