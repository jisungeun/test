#include "EvaluationPageControl.h"
#include "EvaluationPage.h"
#include "ui_EvaluationPage.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    struct EvaluationPage::Impl {
        EvaluationPageControl control;
        Ui::EvaluationPage ui;

        auto Start()->void;
    };

    auto EvaluationPage::Impl::Start() -> void {
        control.Start();
    }

    EvaluationPage::EvaluationPage(QWidget* parent) : ISubPage(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.label->setObjectName("label-body");
        d->ui.startBtn->setObjectName("bt-maintenance-main");

        connect(d->ui.startBtn, &QPushButton::clicked, this,
                [this]() {
                    d->Start();
                    emit sigStart();
                });
    }

    EvaluationPage::~EvaluationPage() {
    }

    auto EvaluationPage::Initialize() -> bool {
        return true;
    }

    auto EvaluationPage::IsClosable() -> bool {
        return !d->control.IsStarted();
    }

    auto EvaluationPage::IsModified() const -> bool {
        return false;
    }

    auto EvaluationPage::IsRestorable() const -> bool {
        return true;
    }

    auto EvaluationPage::Restore() -> bool {
        return true;
    }

    auto EvaluationPage::Save() -> bool {
        return true;
    }
}