#include <MessageDialog.h>

#include "InstrumentObserver.h"
#include "MaintenanceStagePanelControl.h"
#include "MaintenanceStagePanel.h"
#include "ui_MaintenanceStagePanel.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    struct MaintenanceStagePanel::Impl {
        MaintenanceStagePanelControl control;
        Ui::MaintenanceStagePanel ui;
        MaintenanceStagePanel* p{ nullptr };

        InstrumentObserver* observer{ nullptr };

        Impl(MaintenanceStagePanel* p) : p{ p } {}

        auto InitUi()->void;
        auto UpdatePosition(AppEntity::Axis axis, double posInMm)->void;
        auto MoveToDripTrayRemovalPosition()->void;
        auto MoveToParkingPosition()->void;
    };

    auto MaintenanceStagePanel::Impl::InitUi() -> void {
        ui.setupUi(p);

        for (const auto& label : p->findChildren<QLabel*>()) {
            label->setObjectName("label-h5");
        }

        for (const auto& button : p->findChildren<QPushButton*>()) {
            button->setObjectName("bt-maintenance-light");
        }

        for (const auto& groupBox : p->findChildren<QGroupBox*>()) {
            groupBox->setObjectName("gb-maintenance-main");
        }
    }

    auto MaintenanceStagePanel::Impl::UpdatePosition(AppEntity::Axis axis, double posInMm) -> void {
        switch(axis) {
        case AppEntity::Axis::X:
            ui.xPos->setValue(posInMm);
            break;
        case AppEntity::Axis::Y:
            ui.yPos->setValue(posInMm);
            break;
        case AppEntity::Axis::Z:
            ui.zPos->setValue(posInMm);
            break;
        case AppEntity::Axis::C:
            ui.cPos->setValue(posInMm);
            break;
        }
    }

    auto MaintenanceStagePanel::Impl::MoveToDripTrayRemovalPosition() -> void {
        if(!control.MoveToDripTrayRemovalPosition()) {
            TC::MessageDialog::warning(p, tr("Maintenance"), tr("It fails to move the drip tray removal position"));
            return;
        }
    }

    auto MaintenanceStagePanel::Impl::MoveToParkingPosition() -> void {
        if(!control.MoveToParkingPosition()) {
            TC::MessageDialog::warning(p, tr("Maintenance"), tr("It fails to move the parking position"));
            return;
        }
    }

    MaintenanceStagePanel::MaintenanceStagePanel(QWidget* parent) : ISubPanel(parent), d{ std::make_unique<Impl>(this) } {
        d->InitUi();

        d->observer = new InstrumentObserver(this);

        connect(d->ui.moveToDripTrayRemovalBtn, &QPushButton::clicked, this, [this]() {
            d->MoveToDripTrayRemovalPosition();
        });

        connect(d->ui.moveToParkingBtn, &QPushButton::clicked, this, [this]() {
            d->MoveToParkingPosition();
        });

        connect(d->observer, &InstrumentObserver::sigUpdateGlobalPosition, this, [this](AppEntity::Axis axis, double posInMm) {
            d->UpdatePosition(axis, posInMm);
        });
    }

    MaintenanceStagePanel::~MaintenanceStagePanel() {
    }

    auto MaintenanceStagePanel::Initialize() -> bool {
        d->ui.xPos->setValue(d->control.GetPosition(AppEntity::Axis::X));
        d->ui.yPos->setValue(d->control.GetPosition(AppEntity::Axis::Y));
        d->ui.zPos->setValue(d->control.GetPosition(AppEntity::Axis::Z));
        d->ui.cPos->setValue(d->control.GetPosition(AppEntity::Axis::C));

        return true;
    }
}