#pragma once
#include <memory>

#include <IInstrumentView.h>

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class InstrumentObserver;

    class InstrumentUpdater : public Interactor::IInstrumentView {
    public:
        using Pointer = std::shared_ptr<InstrumentUpdater>;

    protected:
        InstrumentUpdater();

    public:
        ~InstrumentUpdater() override;

        static auto GetInstance()->Pointer;

        auto UpdateFailed(const QString& message) -> void override;
        auto UpdateProgress(double progress, const QString& message) -> void override;
        auto UpdateGlobalPosition(AppEntity::Axis axis, double posInMm) -> void override;

    protected:
        auto Register(InstrumentObserver* observer)->void;
        auto Deregister(InstrumentObserver* observer)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class InstrumentObserver;
    };
}