#include <QTabWidget>

#include "ModuleTestPageControl.h"
#include "ModuleTestPage.h"

#include <QTabBar>

#include "ui_ModuleTestPage.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    struct ModuleTestPage::Impl {
        ModuleTestPageControl control;
        Ui::ModuleTestPage ui;

        auto Start()->void;
    };

    auto ModuleTestPage::Impl::Start() -> void {
        control.Start();
        ui.mainTab->tabBar()->show();
        ui.mainTab->tabBar()->removeTab(0);
    }

    ModuleTestPage::ModuleTestPage(QWidget* parent) : ISubPage(parent), d{new Impl} {
        d->ui.setupUi(this);
        d->ui.mainTab->tabBar()->hide();

        d->ui.label->setObjectName("label-body");
        d->ui.startBtn->setObjectName("bt-maintenance-main");

        connect(d->ui.startBtn, &QPushButton::clicked, this,
                [this]() {
                    d->Start();
                });
    }

    ModuleTestPage::~ModuleTestPage() {
    }

    auto ModuleTestPage::Initialize() -> bool {
        return true;
    }

    auto ModuleTestPage::IsClosable() -> bool {
        return !d->control.IsStarted();
    }

    auto ModuleTestPage::IsModified() const -> bool {
        return false;
    }

    auto ModuleTestPage::IsRestorable() const -> bool {
        return true;
    }

    auto ModuleTestPage::Restore() -> bool {
        return true;
    }

    auto ModuleTestPage::Save() -> bool {
        return true;
    }
}