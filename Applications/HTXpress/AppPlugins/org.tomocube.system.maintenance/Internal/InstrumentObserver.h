#pragma once
#include <memory>
#include <QObject>

#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class InstrumentObserver : public QObject {
        Q_OBJECT

    public:
        InstrumentObserver(QObject* parent = nullptr);
        ~InstrumentObserver() override;

        auto UpdateFailed(const QString& message)->void;
        auto UpdateProgress(double progress, const QString& message = QString())->void;
        auto UpdateGlobalPosition(AppEntity::Axis axis, double posInMm)->void;

    signals:
        void sigFailed(const QString& message);
        void sigProgress(double progress, const QString& message);
        void sigUpdateGlobalPosition(AppEntity::Axis axis, double posInMm);
    };
}