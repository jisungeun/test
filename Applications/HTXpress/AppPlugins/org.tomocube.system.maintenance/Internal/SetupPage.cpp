#include <QFileDialog>
#include <QDesktopServices>

#include <MessageDialog.h>

#include "SetupPageControl.h"
#include "SetupPage.h"
#include "ui_SetupPage.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    struct SetupPage::Impl {
        Ui::SetupPage ui;
        SetupPageControl control;

        auto Start()->void;
    };

    auto SetupPage::Impl::Start() -> void {
        control.Start();
    }

    SetupPage::SetupPage(QWidget* parent) : ISubPage(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.label->setObjectName("label-body");
        d->ui.startBtn->setObjectName("bt-maintenance-main");

        connect(d->ui.startBtn, &QPushButton::clicked, this,
                [this]() {
                    d->Start();
                    emit sigStart();
                });
    }

    SetupPage::~SetupPage() {
    }

    auto SetupPage::Initialize() -> bool {
        return true;
    }

    auto SetupPage::IsClosable() -> bool {
        return !d->control.IsStarted();
    }

    auto SetupPage::IsModified() const -> bool {
        return false;
    }

    auto SetupPage::IsRestorable() const -> bool {
        return false;
    }

    auto SetupPage::Restore() -> bool {
        return true;
    }

    auto SetupPage::Save() -> bool {
        return true;
    }
}
