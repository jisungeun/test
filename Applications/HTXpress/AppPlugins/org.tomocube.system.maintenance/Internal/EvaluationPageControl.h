#pragma once
#include <memory>

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class EvaluationPageControl {
    public:
        EvaluationPageControl();
        ~EvaluationPageControl();

        auto Start()->void;
        auto IsStarted() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
