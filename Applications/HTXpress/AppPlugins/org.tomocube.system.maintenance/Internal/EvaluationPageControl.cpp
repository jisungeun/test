#include <SystemStatus.h>

#include "EvaluationPageControl.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    struct EvaluationPageControl::Impl {
        bool started{ false };
    };

    EvaluationPageControl::EvaluationPageControl() : d{new Impl} {
    }

    EvaluationPageControl::~EvaluationPageControl() {
    }

    auto EvaluationPageControl::Start() -> void {
        AppEntity::SystemStatus::GetInstance()->SetServiceMode(AppEntity::ServiceMode::Evaluation);
        d->started = true;
    }

    auto EvaluationPageControl::IsStarted() const -> bool {
        return d->started;
    }

}
