#include <SystemStatus.h>

#include "ModuleTestPageControl.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    struct ModuleTestPageControl::Impl {
        bool started{ false };
    };

    ModuleTestPageControl::ModuleTestPageControl() : d{new Impl} {
    }

    ModuleTestPageControl::~ModuleTestPageControl() {
    }

    auto ModuleTestPageControl::Start() -> void {
        AppEntity::SystemStatus::GetInstance()->SetServiceMode(AppEntity::ServiceMode::ModuleTest);
        d->started = true;
    }

    auto ModuleTestPageControl::IsStarted() const -> bool {
        return d->started;
    }
}
