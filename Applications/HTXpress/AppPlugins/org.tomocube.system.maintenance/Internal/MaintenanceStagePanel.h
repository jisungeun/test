#pragma once
#include <memory>

#include "ISubPanel.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class MaintenanceStagePanel : public ISubPanel {
    public:
        MaintenanceStagePanel(QWidget* parent = nullptr);
        ~MaintenanceStagePanel() override;

        auto Initialize() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}