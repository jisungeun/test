#include <System.h>
#include <InstrumentController.h>

#include "InstrumentUpdater.h"
#include "MaintenanceStagePanelControl.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    MaintenanceStagePanelControl::MaintenanceStagePanelControl() {
    }

    MaintenanceStagePanelControl::~MaintenanceStagePanelControl() {
    }

    auto MaintenanceStagePanelControl::GetPosition(AppEntity::Axis axis) const -> double {
        auto controller = Interactor::InstrumentController();
        return controller.GetPosition(axis);
    }

    auto MaintenanceStagePanelControl::MoveToDripTrayRemovalPosition() -> bool {
        auto model = AppEntity::System::GetModel();
        auto xTarget = model->DripTrayRemovingXPositionMM();
        auto yTarget = model->DripTrayRemovingYPositionMM();
        auto zTarget = model->DripTrayRemovingZPositionMM();
        auto cTarget = model->DripTrayRemovingCPositionMM();

        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        if(!controller.MoveAxisMM(AppEntity::Axis::C, cTarget)) return false;
        if(!controller.MoveAxisMM(AppEntity::Axis::Z, zTarget)) return false;
        return controller.MoveXY(xTarget, yTarget);
    }

    auto MaintenanceStagePanelControl::MoveToParkingPosition() -> bool {
        auto model = AppEntity::System::GetModel();
        auto xTarget = model->ParkingXPositionMM();
        auto yTarget = model->ParkingYPositionMM();
        auto zTarget = model->ParkingZPositionMM();
        auto cTarget = model->ParkingCPositionMM();

        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        if(!controller.MoveAxisMM(AppEntity::Axis::C, cTarget)) return false;
        if(!controller.MoveAxisMM(AppEntity::Axis::Z, zTarget)) return false;
        return controller.MoveXY(xTarget, yTarget);
    }
}
