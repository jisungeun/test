#pragma once
#include <memory>

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class ModuleTestPageControl {
    public:
        ModuleTestPageControl();
        ~ModuleTestPageControl();

        auto Start()->void;
        auto IsStarted() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
