#pragma once
#include <memory>

#include <AppEntityDefines.h>

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class MaintenanceStagePanelControl {
    public:
        MaintenanceStagePanelControl();
        ~MaintenanceStagePanelControl();

        auto GetPosition(AppEntity::Axis axis) const->double;

        auto MoveToDripTrayRemovalPosition()->bool;
        auto MoveToParkingPosition()->bool;
    };
}