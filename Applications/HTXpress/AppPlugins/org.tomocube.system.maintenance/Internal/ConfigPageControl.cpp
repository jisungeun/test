#include <QtSerialPort/QSerialPortInfo>

#include <ModelRepo.h>
#include <System.h>
#include <ConfigController.h>
#include "ConfigPageControl.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    struct ConfigPageControl::Impl {
        Config original;

        auto Restore() -> void;
        auto Store() -> void;
    };

    auto ConfigPageControl::Impl::Restore() -> void {
        auto sysConfig = AppEntity::System::GetSystemConfig();

        original.general.model = sysConfig->GetModel();
        original.general.serial = sysConfig->GetSerial();
        original.general.dataFolder = sysConfig->GetDataDir();

        original.general.mcu.port = sysConfig->GetMCUPort();
        original.general.mcu.baudrate = sysConfig->GetMCUBaudrate();

        original.parameters.vesselCenter.x = sysConfig->GetSystemCenter().toMM().x;
        original.parameters.vesselCenter.y = sysConfig->GetSystemCenter().toMM().y;

        original.parameters.overlap = sysConfig->GetTileScanOverlap();

        original.parameters.flOutputRange.min = sysConfig->GetFlOutputRangeMin();
        original.parameters.flOutputRange.max = sysConfig->GetFlOutputRangeMax();

        original.htScan.NAs = sysConfig->GetHTIlluminationNAs();
        std::sort(original.htScan.NAs.begin(), original.htScan.NAs.end());
        for(auto NA : original.htScan.NAs) {
            original.htScan.patternIndexes[NA] = sysConfig->GetHTIlluminationPattern(NA);

            auto [step, slices] = sysConfig->GetHTScanParameter(NA);
            original.htScan.scanParameters[NA] = {step, slices};

            auto [start, calStep, threshold] = sysConfig->GetIlluminationCalibrationParameter(NA);
            original.htScan.calParameters[NA] = {start, calStep, threshold};

            auto [cafPatternIndex, cafIntensity, cafOffset] = sysConfig->GetCondenserAFParameter(NA);
            original.cafParameters[NA] = {cafPatternIndex, cafIntensity, cafOffset};
        }

        original.cafScanParameters.overriden = sysConfig->IsCondenserAFScanParameterOverriden();
        original.cafScanParameters.startPos = sysConfig->GetCondenserAFScanStartPosPulse();
        original.cafScanParameters.interval = sysConfig->GetCondenserAFScanIntervalPulse();
        original.cafScanParameters.slices = sysConfig->GetCondenserAFScanTriggerSlices();

        original.autoFocus.lensId = sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::LensID);
        original.autoFocus.inFocusRange = sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::InFocusRange);
        original.autoFocus.direction = sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::Direction);
        original.autoFocus.loopInterval = sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::LoopInterval);
        original.autoFocus.maxTrialCount = sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::MaxTrialCount);
        original.autoFocus.sensorValueMin = sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::MinSensorValue);
        original.autoFocus.sensorValueMax = sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::MaxSensorValue);
        original.autoFocus.resolutionMul = sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::ResolutionMul);
        original.autoFocus.resolutionDiv = sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::ResolutionDiv);
        original.autoFocus.defaultTargetValue = sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::DefaultTargetValue);
        original.autoFocus.maxCompensationPulse = sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::MaxCompensationPulse);
        original.autoFocus.scanReadyMm = sysConfig->GetAutofocusReadyPos();

        const auto [coeffA, coeffB, coeffP, coeffQ] = sysConfig->GetPreviewGainCoefficient();
        original.previewParameters.intensity = sysConfig->GetPreviewLightIntensity();
        original.previewParameters.coeffA = coeffA;
        original.previewParameters.coeffB = coeffB;
        original.previewParameters.coeffP = coeffP;
        original.previewParameters.coeffQ = coeffQ;
    }

    auto ConfigPageControl::Impl::Store() -> void {
        auto sysConfig = AppEntity::System::GetSystemConfig();

        sysConfig->SetModel(original.general.model);
        sysConfig->SetSerial(original.general.serial);
        sysConfig->SetDataDir(original.general.dataFolder);

        sysConfig->SetMCUComPort(original.general.mcu.port, original.general.mcu.baudrate);

        sysConfig->SetSystemCenter(AppEntity::Position::fromMM(original.parameters.vesselCenter.x,
                                                               original.parameters.vesselCenter.y,
                                                               0));
        sysConfig->SetTileScanOverlap(original.parameters.overlap);

        sysConfig->SetFlOutputRange(original.parameters.flOutputRange.min, original.parameters.flOutputRange.max);

        for(auto NA : original.htScan.NAs) {
            sysConfig->SetHTIlluminationPattern(NA, original.htScan.patternIndexes[NA]);
            sysConfig->SetHTScanParameter(NA, original.htScan.scanParameters[NA].step, original.htScan.scanParameters[NA].slices);
            sysConfig->SetIlluminationCalibrationParameter(NA,
                                                           original.htScan.calParameters[NA].start,
                                                           original.htScan.calParameters[NA].step,
                                                           original.htScan.calParameters[NA].threshold);
            sysConfig->SetCondenserAFParameter(NA,
                                               original.cafParameters[NA].patternIndex,
                                               original.cafParameters[NA].intensity,
                                               original.cafParameters[NA].zCompensationOffset);
        }

        if(original.cafScanParameters.overriden) {
            sysConfig->SetCondenserAFScanParameter(original.cafScanParameters.startPos,
                                                   original.cafScanParameters.interval,
                                                   original.cafScanParameters.slices);
        } else {
            sysConfig->ClearCondenserAFScanParameter();
        }

        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::LensID, original.autoFocus.lensId);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::InFocusRange, original.autoFocus.inFocusRange);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::Direction, original.autoFocus.direction);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::LoopInterval, original.autoFocus.loopInterval);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::MaxTrialCount, original.autoFocus.maxTrialCount);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::MinSensorValue, original.autoFocus.sensorValueMin);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::MaxSensorValue, original.autoFocus.sensorValueMax);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::ResolutionMul, original.autoFocus.resolutionMul);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::ResolutionDiv, original.autoFocus.resolutionDiv);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::DefaultTargetValue, original.autoFocus.defaultTargetValue);
        sysConfig->SetAutofocusParameter(AppEntity::AutoFocusParameter::MaxCompensationPulse, original.autoFocus.maxCompensationPulse);
        sysConfig->SetAutofocusReadyPos(original.autoFocus.scanReadyMm);

        sysConfig->SetPreviewLightIntensity(original.previewParameters.intensity);
        sysConfig->SetPreviewGainCoefficient(original.previewParameters.coeffA,
                                             original.previewParameters.coeffB,
                                             original.previewParameters.coeffP,
                                             original.previewParameters.coeffQ);
    }

    auto ConfigPageControl::Config::operator==(const Config& rhs) const -> bool {
        if(general.model != rhs.general.model) return false;
        if(general.serial != rhs.general.serial) return false;
        if(general.dataFolder != rhs.general.dataFolder) return false;
        if(general.mcu.port != rhs.general.mcu.port) return false;
        if(general.mcu.baudrate != rhs.general.mcu.baudrate) return false;

        if(parameters.vesselCenter != rhs.parameters.vesselCenter) return false;
        if(parameters.overlap != rhs.parameters.overlap) return false;
        if(parameters.flOutputRange != rhs.parameters.flOutputRange) return false;

        //It is not necessary if this container is always sorted...
        auto lhsNAs = htScan.NAs;
        std::sort(lhsNAs.begin(), lhsNAs.end());
        auto rhsNAs = rhs.htScan.NAs;
        std::sort(rhsNAs.begin(), rhsNAs.end());

        if(htScan.NAs != rhsNAs) return false;
        if(htScan.patternIndexes != rhs.htScan.patternIndexes) return false;
        if(htScan.scanParameters != rhs.htScan.scanParameters) return false;
        if(htScan.calParameters != rhs.htScan.calParameters) return false;

        if(autoFocus != rhs.autoFocus) return false;

        if(cafParameters != rhs.cafParameters) return false;

        if(cafScanParameters != rhs.cafScanParameters) return false;

        if (previewParameters != rhs.previewParameters) return false;

        return true;
    }

    auto ConfigPageControl::Config::operator!=(const Config& rhs) const -> bool {
        return !(*this == rhs);
    }

    ConfigPageControl::ConfigPageControl() : d{new Impl} {
    }

    ConfigPageControl::~ConfigPageControl() {
    }

    auto ConfigPageControl::Initialize() -> void {
        d->Restore();
    }

    auto ConfigPageControl::GetConfig() const -> Config {
        return d->original;
    }

    auto ConfigPageControl::SetConfig(const Config& config) -> bool {
        d->original = config;
        d->Store();

        auto controller = Interactor::ConfigController();
        return controller.Save();
    }

    auto ConfigPageControl::IsModified(const Config& config) const -> bool {
        return d->original != config;
    }

    auto ConfigPageControl::GetAvailableModels() -> QStringList {
        auto repo = AppEntity::ModelRepo::GetInstance();
        auto models = repo->GetModels();

        QStringList list;
        for (auto model : models) {
            list.push_back(model->Name());
        }

        return list;
    }

    auto ConfigPageControl::GetAvailablePorts() -> QStringList {
        auto ports = QSerialPortInfo::availablePorts();

        QStringList list;
        for (auto port : ports) {
            list.push_back(port.portName());
        }

        return list;
    }

    auto ConfigPageControl::GetAvailableBaudrates() -> QStringList {
        auto baudRates = QSerialPortInfo::standardBaudRates();

        QStringList list;
        for (auto baudrate : baudRates) {
            list.push_back(QString::number(baudrate));
        }

        return list;
    }

    auto ConfigPageControl::IsUsedDataFolder(const QString& path) const -> bool {
        auto controller = Interactor::ConfigController();
        return controller.IsUsedDataFolder(path);
    }
}

