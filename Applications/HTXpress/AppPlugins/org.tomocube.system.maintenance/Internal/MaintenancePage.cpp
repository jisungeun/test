#include "MaintenanceStagePanel.h"
#include "MaintenancePageControl.h"
#include "MaintenancePage.h"

#include <QTabBar>

#include "ui_MaintenancePage.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    struct MaintenancePage::Impl {
        MaintenancePageControl control;
        Ui::MaintenancePage ui;
        MaintenancePage*p { nullptr };

        Impl(MaintenancePage* p) : p{ p } {}

        auto InitUi()->void;
        auto Start()->void;
        auto InitializePanel(int32_t index)->void;
    };

    auto MaintenancePage::Impl::InitUi() -> void {
        ui.setupUi(p);
        ui.tabWidget->tabBar()->hide();

        ui.label->setObjectName("label-body");
        ui.startBtn->setObjectName("bt-maintenance-main");
    }

    auto MaintenancePage::Impl::Start() -> void {
        control.Start();

        ui.tabWidget->addTab(new MaintenanceStagePanel(p), "Stage");
        ui.tabWidget->setCurrentIndex(1);
        ui.tabWidget->setTabVisible(0, false);
        ui.tabWidget->tabBar()->show();
    }

    auto MaintenancePage::Impl::InitializePanel(int32_t index) -> void {
        auto* panel = qobject_cast<ISubPanel*>(ui.tabWidget->widget(index));
        if(panel == nullptr) return;
        panel->Initialize();
    }

    MaintenancePage::MaintenancePage(QWidget* parent) : ISubPage(parent), d{std::make_unique<Impl>(this)} {
        d->InitUi();

        connect(d->ui.startBtn, &QPushButton::clicked, this, [this]() {
            d->Start();
        });

        connect(d->ui.tabWidget, &QTabWidget::currentChanged, this, [this](int32_t index) {
            d->InitializePanel(index);
        });
    }

    MaintenancePage::~MaintenancePage() {
    }

    auto MaintenancePage::Initialize() -> bool {
        return true;
    }

    auto MaintenancePage::IsClosable() -> bool {
        return !d->control.IsStarted();
    }

    auto MaintenancePage::IsModified() const -> bool {
        return false;
    }

    auto MaintenancePage::IsRestorable() const -> bool {
        return false;
    }

    auto MaintenancePage::Restore() -> bool {
        return true;
    }

    auto MaintenancePage::Save() -> bool {
        return true;
    }
}