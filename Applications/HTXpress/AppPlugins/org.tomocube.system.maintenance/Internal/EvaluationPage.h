#pragma once
#include <memory>

#include "ISubPage.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class EvaluationPage : public ISubPage {
        Q_OBJECT
    public:
        EvaluationPage(QWidget* parent = nullptr);
        ~EvaluationPage() override;

        auto Initialize() -> bool override;
        auto IsClosable() -> bool override;
        auto IsModified() const -> bool override;
        auto IsRestorable() const -> bool override;
        auto Restore() -> bool override;
        auto Save() -> bool override;

    signals:
        void sigStart();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}