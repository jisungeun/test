#include <QList>

#include "InstrumentObserver.h"
#include "InstrumentUpdater.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    struct InstrumentUpdater::Impl {
        QList<InstrumentObserver*> observers;
    };

    InstrumentUpdater::InstrumentUpdater() : Interactor::IInstrumentView(), d{ std::make_unique<Impl>() } {
    }

    InstrumentUpdater::~InstrumentUpdater() {
    }

    auto InstrumentUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new InstrumentUpdater() };
        return theInstance;
    }

    auto InstrumentUpdater::UpdateFailed(const QString& message) -> void {
        for(auto observer : d->observers) {
            observer->UpdateFailed(message);
        }
    }

    auto InstrumentUpdater::UpdateProgress(double progress, const QString& message) -> void {
        for(auto observer : d->observers) {
            observer->UpdateProgress(progress, message);
        }
    }

    auto InstrumentUpdater::UpdateGlobalPosition(AppEntity::Axis axis, double posInMm) -> void {
        for(auto observer : d->observers) {
            observer->UpdateGlobalPosition(axis, posInMm);
        }
    }

    auto InstrumentUpdater::Register(InstrumentObserver* observer) -> void {
        d->observers.push_back(observer);
    }

    auto InstrumentUpdater::Deregister(InstrumentObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}