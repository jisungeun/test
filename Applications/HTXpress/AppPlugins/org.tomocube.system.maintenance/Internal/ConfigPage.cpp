#include <QFileDialog>

#include <MessageDialog.h>
#include <InputDialog.h>

#include "ConfigPageControl.h"
#include "ConfigPage.h"
#include "ui_ConfigPage.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    struct ConfigPage::Impl {
        ConfigPage* p{ nullptr };
        ConfigPageControl control;
        Ui::ConfigPage ui;

        Impl(ConfigPage* p) : p{p} {}

        auto AddPort(QComboBox* combo, int32_t port)->void;
        auto AddBaudrate(QComboBox* combo, int32_t baudrate)->void;

        auto InitUi()->void;
        auto AddRow(QTableWidget* table, int32_t rowIndex, double NA, const QList<QVariant>& values)->void;
        auto AddRow(QTableWidget* table, int32_t rowIndex, int32_t value)->void;
        auto AddRows(QTableWidget* table, int32_t colIdx, const QList<QVariant>& values)->void;
        auto UpdateGUI(const ConfigPageControl::Config& config)->void;
        auto UpdateConfig() const->ConfigPageControl::Config;
        auto ChangeModel()->void;
        auto ChangeSerial()->void;
        auto ChangeDataFolder()->void;
        auto ApplyStyleSheet()->void;
    };

    auto ConfigPage::Impl::AddPort(QComboBox* combo, int32_t port) -> void {
        const auto strPort = QString("COM%1").arg(port);
        if (combo->findText(strPort) >= 0) {
            combo->setCurrentText(strPort);
        } else {
            if(combo->findText("Invalid") == -1) {
                combo->insertItem(0, "Invalid");
            }
            combo->setCurrentIndex(0);
        }
    }

    auto ConfigPage::Impl::AddBaudrate(QComboBox* combo, int32_t baudrate) -> void {
        const auto strBaudrate = QString::number(baudrate);
        if (combo->findText(strBaudrate) >= 0) {
            combo->setCurrentText(strBaudrate);
        } else {
            combo->insertItem(0, strBaudrate);
            combo->setCurrentIndex(0);
        }
    }

    auto ConfigPage::Impl::InitUi() -> void {
        ui.tabWidget->setCurrentIndex(0);

        ui.model->setReadOnly(true);
        ui.serial->setReadOnly(true);
        ui.dataFolder->setReadOnly(true);

        ui.mcuPort->addItems(control.GetAvailablePorts());
        ui.mcuBaudrate->addItems(control.GetAvailableBaudrates());

        ui.htIllumPatternIndexTable->setColumnCount(2);
        ui.htIllumPatternIndexTable->setHorizontalHeaderLabels({"NA", "Pattern Index"});
        ui.htIllumPatternIndexTable->setColumnWidth(0, 60);
        ui.htIllumPatternIndexTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeMode::Fixed);
        ui.htIllumPatternIndexTable->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeMode::Stretch);

        ui.htScanParamTable->setColumnCount(3);
        ui.htScanParamTable->setHorizontalHeaderLabels({"NA", "Scan Step", "Slices"});
        ui.htScanParamTable->setColumnWidth(0, 60);
        ui.htScanParamTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeMode::Fixed);
        ui.htScanParamTable->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeMode::Stretch);
        ui.htScanParamTable->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeMode::Stretch);

        ui.htIllumCalParamTable->setColumnCount(4);
        ui.htIllumCalParamTable->setHorizontalHeaderLabels({"NA", "Start", "Step", "Threshold"});
        ui.htIllumCalParamTable->setColumnWidth(0, 60);
        ui.htIllumCalParamTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeMode::Fixed);
        ui.htIllumCalParamTable->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeMode::Stretch);
        ui.htIllumCalParamTable->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeMode::Stretch);
        ui.htIllumCalParamTable->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeMode::Stretch);

        ui.afTable->setColumnCount(1);
        ui.afTable->setHorizontalHeaderLabels({"Value"});
        ui.afTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeMode::Stretch);

        ui.afTable->setRowCount(12);
        ui.afTable->setVerticalHeaderLabels({"Lens ID",
                                             "In-focus Range",
                                             "Direction",
                                             "Loop Interval",
                                             "Max Trial Count",
                                             "Min Sensor Value",
                                             "Max Sensor Value",
                                             "Resolution Multiple",
                                             "Resolution Division",
                                             "Default Target Value",
                                             "ScanReady Position (mm)",
                                             "Max Compensation (pulse)"});

        ui.condenserAfTable->setColumnCount(4);
        ui.condenserAfTable->setHorizontalHeaderLabels({"NA", "Pattern Index", "Intensity", "Z Offset (mm)"});
        ui.condenserAfTable->setColumnWidth(0, 60);
        ui.condenserAfTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeMode::Fixed);
        ui.condenserAfTable->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeMode::Stretch);
        ui.condenserAfTable->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeMode::Stretch);
        ui.condenserAfTable->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeMode::Stretch);

        ui.cafScanParameterTable->setColumnCount(1);
        ui.cafScanParameterTable->setHorizontalHeaderLabels({"Value"});
        ui.cafScanParameterTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeMode::Stretch);
        ui.cafScanParameterTable->setRowCount(3);
        ui.cafScanParameterTable->setVerticalHeaderLabels({"Trigger Start Pos (pulse)",
                                                           "Trigger Interval (pulse)",
                                                           "Trigger Slices"});
        ui.cafScanParameterTable->setDisabled(true);
        connect(ui.overrideCAFScanParameter, &QCheckBox::stateChanged, [=](int state) {
            ui.cafScanParameterTable->setEnabled(state == Qt::Checked);
        });
    }

    auto ConfigPage::Impl::AddRow(QTableWidget* table, int32_t rowIndex, double NA, const QList<QVariant>& values) -> void {
        auto* item = new QTableWidgetItem(QString::number(NA, 'f', 3));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
        table->setItem(rowIndex, 0, item);

        for(auto idx=0; idx<values.size(); idx++) {
            item = [=]()->QTableWidgetItem* {
                if(values.at(idx).type() == QVariant::Type::Int) {
                    return new QTableWidgetItem(QString::number(values.at(idx).toInt()));
                }
                return new QTableWidgetItem(QString::number(values.at(idx).toDouble(), 'f', 3));
            }();
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            table->setItem(rowIndex, idx+1, item);
        }
    }

    auto ConfigPage::Impl::AddRow(QTableWidget* table, int32_t rowIndex, int32_t value) -> void {
        const auto item = new QTableWidgetItem(QString::number(value));
        item->setTextAlignment(Qt::AlignCenter);
        table->setItem(rowIndex, 0, item);
    }

    auto ConfigPage::Impl::AddRows(QTableWidget* table, int32_t colIdx, const QList<QVariant>& values) -> void {
        for(auto idx=0; idx<values.size(); idx++) {
            auto* item = [=]()->QTableWidgetItem* {
                if(values.at(idx).type() == QVariant::Type::Int) {
                    return new QTableWidgetItem(QString::number(values.at(idx).toInt()));
                }
                return new QTableWidgetItem(QString::number(values.at(idx).toDouble(), 'f', 3));
            }();
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
            table->setItem(idx, colIdx, item);
        }
    }

    auto ConfigPage::Impl::UpdateGUI(const ConfigPageControl::Config& config) -> void {
        ui.model->setText(config.general.model);
        ui.serial->setText(config.general.serial);
        ui.dataFolder->setText(config.general.dataFolder);
        AddPort(ui.mcuPort, config.general.mcu.port);
        AddBaudrate(ui.mcuBaudrate, config.general.mcu.baudrate);

        ui.vesselCenterX->setValue(config.parameters.vesselCenter.x);
        ui.vesselCenterY->setValue(config.parameters.vesselCenter.y);
        ui.overlap->setValue(config.parameters.overlap);
        ui.flOutputRangeMin->setValue(config.parameters.flOutputRange.min);
        ui.flOutputRangeMax->setValue(config.parameters.flOutputRange.max);

        auto NACount = config.htScan.NAs.size();
        ui.htIllumPatternIndexTable->setRowCount(NACount);
        ui.htScanParamTable->setRowCount(NACount);
        ui.htIllumCalParamTable->setRowCount(NACount);
        ui.condenserAfTable->setRowCount(NACount);

        for(auto idx=0; idx<NACount; idx++) {
            auto NA = config.htScan.NAs.at(idx);
            AddRow(ui.htIllumPatternIndexTable, idx, NA, {config.htScan.patternIndexes[NA]});
            AddRow(ui.htScanParamTable, idx, NA, 
                   {config.htScan.scanParameters[NA].step, config.htScan.scanParameters[NA].slices});
            AddRow(ui.htIllumCalParamTable, idx, NA,
                   {config.htScan.calParameters[NA].start,
                    config.htScan.calParameters[NA].step,
                    config.htScan.calParameters[NA].threshold});
            AddRow(ui.condenserAfTable, idx, NA,
                   {config.cafParameters[NA].patternIndex,
                    config.cafParameters[NA].intensity,
                    config.cafParameters[NA].zCompensationOffset});
        }

        ui.overrideCAFScanParameter->setChecked(config.cafScanParameters.overriden);
        AddRow(ui.cafScanParameterTable, 0, config.cafScanParameters.startPos);
        AddRow(ui.cafScanParameterTable, 1, config.cafScanParameters.interval);
        AddRow(ui.cafScanParameterTable, 2, config.cafScanParameters.slices);

        AddRows(ui.afTable, 0,
                {config.autoFocus.lensId,
                 config.autoFocus.inFocusRange,
                 config.autoFocus.direction,
                 config.autoFocus.loopInterval,
                 config.autoFocus.maxTrialCount,
                 config.autoFocus.sensorValueMin,
                 config.autoFocus.sensorValueMax,
                 config.autoFocus.resolutionMul,
                 config.autoFocus.resolutionDiv,
                 config.autoFocus.defaultTargetValue,
                 config.autoFocus.scanReadyMm,
                 config.autoFocus.maxCompensationPulse});

        ui.previewIntensity->setValue(config.previewParameters.intensity);
        ui.previewCoeffA->setValue(config.previewParameters.coeffA);
        ui.previewCoeffB->setValue(config.previewParameters.coeffB);
        ui.previewCoeffP->setValue(config.previewParameters.coeffP);
        ui.previewCoeffQ->setValue(config.previewParameters.coeffQ);
    }

    auto ConfigPage::Impl::UpdateConfig() const -> ConfigPageControl::Config {
        auto parsePort = [](const QString& text)-> int32_t {
            if (text.left(3).compare("COM", Qt::CaseInsensitive) != 0) return 0;
            return text.mid(3).toInt();
        };

        auto parseBaudrate = [](const QString& text)-> int32_t {
            if (text.isEmpty()) return 0;
            return text.toInt();
        };

        ConfigPageControl::Config config;

        config.general.model = ui.model->text();
        config.general.serial = ui.serial->text();
        config.general.dataFolder = ui.dataFolder->text();
        config.general.mcu.port = parsePort(ui.mcuPort->currentText());
        config.general.mcu.baudrate = parseBaudrate(ui.mcuBaudrate->currentText());

        config.parameters.vesselCenter.x = ui.vesselCenterX->value();
        config.parameters.vesselCenter.y = ui.vesselCenterY->value();
        config.parameters.overlap = ui.overlap->value();
        config.parameters.flOutputRange.min = ui.flOutputRangeMin->value();
        config.parameters.flOutputRange.max = ui.flOutputRangeMax->value();

        auto NACount = ui.htIllumPatternIndexTable->rowCount();
        for(auto idx=0; idx<NACount; idx++) {
            auto NA = ui.htIllumPatternIndexTable->item(idx, 0)->text().toDouble();
            config.htScan.NAs.push_back(NA);

            config.htScan.patternIndexes[NA] = ui.htIllumPatternIndexTable->item(idx, 1)->text().toInt();
            config.htScan.scanParameters[NA] = {ui.htScanParamTable->item(idx, 1)->text().toInt(),
                                                ui.htScanParamTable->item(idx, 2)->text().toInt()};
            config.htScan.calParameters[NA] = {ui.htIllumCalParamTable->item(idx, 1)->text().toInt(),
                                               ui.htIllumCalParamTable->item(idx, 2)->text().toInt(),
                                               ui.htIllumCalParamTable->item(idx, 3)->text().toInt()};
            config.cafParameters[NA] = {ui.condenserAfTable->item(idx, 1)->text().toInt(),
                                        ui.condenserAfTable->item(idx, 2)->text().toInt(),
                                        ui.condenserAfTable->item(idx, 3)->text().toDouble()};
        }

        config.cafScanParameters.overriden = ui.overrideCAFScanParameter->isChecked();
        config.cafScanParameters.startPos = ui.cafScanParameterTable->item(0, 0)->text().toInt();
        config.cafScanParameters.interval = ui.cafScanParameterTable->item(1, 0)->text().toInt();
        config.cafScanParameters.slices = ui.cafScanParameterTable->item(2, 0)->text().toInt();

        config.autoFocus.lensId = ui.afTable->item(0, 0)->text().toInt();
        config.autoFocus.inFocusRange = ui.afTable->item(1, 0)->text().toInt();
        config.autoFocus.direction = ui.afTable->item(2, 0)->text().toInt();
        config.autoFocus.loopInterval = ui.afTable->item(3, 0)->text().toInt();
        config.autoFocus.maxTrialCount = ui.afTable->item(4, 0)->text().toInt();
        config.autoFocus.sensorValueMin = ui.afTable->item(5, 0)->text().toInt();
        config.autoFocus.sensorValueMax = ui.afTable->item(6, 0)->text().toInt();
        config.autoFocus.resolutionMul = ui.afTable->item(7, 0)->text().toInt();
        config.autoFocus.resolutionDiv = ui.afTable->item(8, 0)->text().toInt();
        config.autoFocus.defaultTargetValue = ui.afTable->item(9, 0)->text().toInt();
        config.autoFocus.scanReadyMm = ui.afTable->item(10, 0)->text().toDouble();
        config.autoFocus.maxCompensationPulse = ui.afTable->item(11, 0)->text().toInt();

        config.previewParameters.intensity = ui.previewIntensity->value();
        config.previewParameters.coeffA = ui.previewCoeffA->value();
        config.previewParameters.coeffB = ui.previewCoeffB->value();
        config.previewParameters.coeffP = ui.previewCoeffP->value();
        config.previewParameters.coeffQ = ui.previewCoeffQ->value();

        return config;
    }

    auto ConfigPage::Impl::ChangeModel() -> void {
        const auto models = control.GetAvailableModels();

        bool ok{};
        auto item = TC::InputDialog::getItem(p, tr("Models"), tr("Model: "), models, 0, false, &ok);
        if (ok) {
            if(item.isEmpty()) {
                TC::MessageDialog::warning(p, tr("Model"), tr("Select a model"));
            } else {
                ui.model->setText(item);
            }
        }
    }

    auto ConfigPage::Impl::ChangeSerial() -> void {
        bool ok{};

        auto serial = TC::InputDialog::getText(p, tr("Serial"), tr("Enter new serial"), 
                                               QLineEdit::Normal, ui.serial->text(), &ok);
        serial = serial.trimmed();
        if(!ok || serial.isEmpty()) return;

        ui.serial->setText(serial);
    }

    auto ConfigPage::Impl::ChangeDataFolder() -> void {
        const auto currentPath = ui.dataFolder->text();
        const auto path = QFileDialog::getExistingDirectory(p, tr("Select a folder to save data"), currentPath);
        if (path == currentPath || path.isEmpty()) { return; }

        if(!control.IsUsedDataFolder(path)) {
            QDir dir(path);
            const auto entryList = dir.entryList(QDir::AllEntries | QDir::Hidden | QDir::System | QDir::NoDotAndDotDot);
            if (!entryList.isEmpty()) {
                TC::MessageDialog::warning(p, tr("Data folder"), tr("Select an empty folder"));
                return;
            }
        }

        ui.dataFolder->setText(path);
    }

    auto ConfigPage::Impl::ApplyStyleSheet() -> void {
        for (const auto& button : p->findChildren<QPushButton*>()) {
            button->setObjectName("bt-maintenance-light");
        }

        for (const auto& label : p->findChildren<QLabel*>()) {
            if (label->objectName().contains("Title", Qt::CaseSensitive)) {
                label->setObjectName("label-h3");
            }
            else if (label->objectName().contains("Subtitle", Qt::CaseSensitive)) {
                label->setObjectName("label-h5");
            }
        }

        for (const auto& table : p->findChildren<QTableWidget*>()) {
            table->setStyleSheet(QString("QTableWidget{border-bottom: 0px;}"));
            table->verticalHeader()->setStyleSheet("QHeaderView{border-bottom: 0px;}");
            if(table == ui.afTable || table == ui.cafScanParameterTable) {
                table->verticalHeader()->setDefaultAlignment(Qt::AlignLeft | Qt::AlignVCenter);
            } else {
                table->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);
            }
        }

        for (const auto& line : p->findChildren<QFrame*>()) {
            if (line->frameShape() == QFrame::VLine) {
                line->setObjectName("line-divider");
                line->setFixedWidth(1);
            }
            else if (line->frameShape() == QFrame::HLine) {
                line->setObjectName("line-divider");
                line->setFixedHeight(1);
            }
        }
    }

    ConfigPage::ConfigPage(QWidget* parent) : ISubPage(parent), d{new Impl(this)} {
        d->ui.setupUi(this);
        d->InitUi();
        d->ApplyStyleSheet();

        connect(d->ui.changeModelBtn, &QAbstractButton::clicked, this, [this](){
            d->ChangeModel();
        });
        connect(d->ui.changeSerialBtn, &QAbstractButton::clicked, this, [this](){
            d->ChangeSerial();
        });
        connect(d->ui.changeDataFolderBtn, &QAbstractButton::clicked, this, [this]() {
            d->ChangeDataFolder();
        });
    }

    ConfigPage::~ConfigPage() {
    }

    auto ConfigPage::Initialize() -> bool {
        d->control.Initialize();
        const auto config = d->control.GetConfig();
        d->UpdateGUI(config);

        d->ui.tabWidget->setCurrentIndex(0);

        return true;
    }

    auto ConfigPage::IsClosable() -> bool {
        return true;
    }

    auto ConfigPage::IsModified() const -> bool {
        auto config = d->UpdateConfig();
        return d->control.IsModified(config);
    }

    auto ConfigPage::IsRestorable() const -> bool {
        return true;
    }

    auto ConfigPage::Restore() -> bool {
        auto config = d->control.GetConfig();
        d->UpdateGUI(config);
        return true;
    }

    auto ConfigPage::Save() -> bool {
        auto config = d->UpdateConfig();
        return d->control.SetConfig(config);
    }
}