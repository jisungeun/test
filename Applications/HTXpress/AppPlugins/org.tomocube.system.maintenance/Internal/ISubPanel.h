#pragma once
#include <QWidget>

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class ISubPanel : public QWidget {
        Q_OBJECT
    public:
        ISubPanel(QWidget* parent = nullptr);
        ~ISubPanel() override;

        virtual auto Initialize()->bool = 0;
    };
}