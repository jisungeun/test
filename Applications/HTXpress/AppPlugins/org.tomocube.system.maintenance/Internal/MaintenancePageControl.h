#pragma once
#include <memory>

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class MaintenancePageControl {
    public:
        MaintenancePageControl();
        ~MaintenancePageControl();

        auto Start()->bool;
        auto IsStarted() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
