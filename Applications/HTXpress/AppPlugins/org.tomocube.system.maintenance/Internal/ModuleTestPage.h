#pragma once
#include <memory>

#include "ISubPage.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    class ModuleTestPage : public ISubPage {
        Q_OBJECT
    public:
        ModuleTestPage(QWidget* parent = nullptr);
        ~ModuleTestPage() override;

        auto Initialize() -> bool override;
        auto IsClosable() -> bool override;
        auto IsModified() const -> bool override;
        auto IsRestorable() const -> bool override;
        auto Restore() -> bool override;
        auto Save() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}