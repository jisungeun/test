#include <SystemStatus.h>

#include "SetupPageControl.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    struct SetupPageControl::Impl {
        bool started{ false };
    };

    SetupPageControl::SetupPageControl() : d{new Impl} {
    }

    SetupPageControl::~SetupPageControl() {
    }

    auto SetupPageControl::Start() -> void {
        AppEntity::SystemStatus::GetInstance()->SetServiceMode(AppEntity::ServiceMode::Setup);
        d->started = true;
    }

    auto SetupPageControl::IsStarted() const -> bool {
        return d->started;
    }

}
