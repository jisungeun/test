#include <SystemStatus.h>
#include <InstrumentController.h>

#include "InstrumentUpdater.h"
#include "MaintenancePageControl.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    struct MaintenancePageControl::Impl {
        bool started{ false };
    };

    MaintenancePageControl::MaintenancePageControl() : d{std::make_unique<Impl>()} {
    }

    MaintenancePageControl::~MaintenancePageControl() {
    }

    auto MaintenancePageControl::Start() -> bool {
        AppEntity::SystemStatus::GetInstance()->SetServiceMode(AppEntity::ServiceMode::Maintenance);
        d->started = true;

        auto updater = InstrumentUpdater::GetInstance();
        auto presenter = Interactor::InstrumentPresenter(updater.get());
        auto controller = Interactor::InstrumentController(&presenter);

        return controller.StartInitialize();
    }

    auto MaintenancePageControl::IsStarted() const -> bool {
        return d->started;
    }
}
