#include "IInstrument.h"
#include "GetPosition.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    struct GetPosition::Impl {
        IInstrumentOutputPort* output{ nullptr };
        AppEntity::Axis axis{ AppEntity::Axis::X };
        double value{ 0 };
    };

    GetPosition::GetPosition(IInstrumentOutputPort* output) : IUseCase("GetPosition"), d{ std::make_unique<Impl>() } {
        d->output = output;
    }

    GetPosition::~GetPosition() {
    }

    auto GetPosition::SetAxis(const AppEntity::Axis axis) -> void {
        d->axis = axis;
    }

    auto GetPosition::Get() const -> double {
        return d->value;
    }

    auto GetPosition::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();
        if(!instrument->IsInitialized()) return true;

        d->value = instrument->GetAxisPositionMM(d->axis);

        return true;
    }
}