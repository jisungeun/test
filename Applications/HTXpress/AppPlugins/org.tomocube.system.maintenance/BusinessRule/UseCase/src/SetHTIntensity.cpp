#include <IChannelConfigReader.h>
#include <IChannelConfigWriter.h>

#include "IInstrument.h"
#include "SetHTIntensity.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    struct SetHTIntensity::Impl {
        int32_t intensity;
    };

    SetHTIntensity::SetHTIntensity() : IUseCase("SetHTIntensity"), d{new Impl} {
    }

    SetHTIntensity::~SetHTIntensity() {
    }

    auto SetHTIntensity::SetIntensity(int32_t intensity) -> void {
        d->intensity = intensity;
    }

    auto SetHTIntensity::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();
        if(!instrument) {
            Error("No instrument instance is created");
            return false;
        }

        if(!instrument->IsInitialized()) {
            Error("Instrument is not initialized");
            return false;
        }

        if(!instrument->ChangeHTIntensity(d->intensity)) {
            Error("It fails to set HT intensity");
            return false;
        }

        AppEntity::ChannelConfig config;
        auto modes = {AppEntity::ImagingMode::HT3D, AppEntity::ImagingMode::HT2D};

        auto reader = IChannelConfigReader::GetInstance();
        auto writer = IChannelConfigWriter::GetInstance();

        for(auto mode : modes) {
            if(reader->Read(mode, config)) {
                config.SetLightIntensity(d->intensity);

                if(!writer->Write(mode, config)) {
                    Error("It fails to write channel config to file");
                    return false;
                }
            } else {
                Error("It fails to read channel config file");
                return false;
            }
        }

        return true;
    }
}