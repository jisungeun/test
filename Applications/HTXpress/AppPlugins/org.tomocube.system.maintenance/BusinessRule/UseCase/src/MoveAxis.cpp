#include <QElapsedTimer>

#include <SystemStatus.h>

#include "IInstrument.h"
#include "MoveAxis.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    struct MoveAxis::Impl {
        enum class Mode {
            NotDefined,
            SingleAxis,
            XYAxis
        };

        IInstrumentOutputPort* output{ nullptr };
        Mode mode{ Mode::NotDefined };
        MoveAxis* p{ nullptr };

        Impl(MoveAxis* p) : p{ p } {}
        auto ReportError(const QString& message)->void;

        struct {
            AppEntity::Axis axis{ AppEntity::Axis::X };
            double posInMm{ 0 };
        } singleAxis;

        struct {
            double xPosInMm{ 0 };
            double yPosInMm{ 0 };
        } xyAxis;
    };

    auto MoveAxis::Impl::ReportError(const QString& message) -> void {
        p->Error(message);
        if(output) output->UpdateFailed(message);
    }

    MoveAxis::MoveAxis(IInstrumentOutputPort* output) : IUseCase("MoveAxis"), d{ std::make_unique<Impl>(this) } {
        d->output = output;
    }

    MoveAxis::~MoveAxis() {
    }

    auto MoveAxis::SetTarget(const AppEntity::Axis axis, const double posInMm) -> void {
        d->mode = Impl::Mode::SingleAxis;
        d->singleAxis.axis = axis;
        d->singleAxis.posInMm = posInMm;
    }

    auto MoveAxis::SetTargetXY(const double xPosInMm, const double yPosInMm) -> void {
        d->mode = Impl::Mode::XYAxis;
        d->xyAxis.xPosInMm = xPosInMm;
        d->xyAxis.yPosInMm = yPosInMm;
    }

    auto MoveAxis::Perform() -> bool {
        using Axis = AppEntity::Axis;

        if(d->mode == Impl::Mode::NotDefined) return false;

        auto instrument = IInstrument::GetInstance();
        if(!instrument->IsInitialized()) return true;

        QMap<Axis, double> axisPos, axisStartPos, axisTargetPos;
        axisPos[Axis::X] = instrument->GetAxisPositionMM(Axis::X);
        axisPos[Axis::Y] = instrument->GetAxisPositionMM(Axis::Y);
        axisPos[Axis::Z] = instrument->GetAxisPositionMM(Axis::Z);
        axisPos[Axis::C] = instrument->GetAxisPositionMM(Axis::C);

        axisStartPos = axisPos;
        axisTargetPos = axisPos;

        auto calDist = [&](const QMap<Axis, double>& target, const QMap<Axis, double>& origin)->double {
            auto xDist = target[Axis::X] - origin[Axis::X];
            auto yDist = target[Axis::Y] - origin[Axis::Y];
            auto zDist = target[Axis::Z] - origin[Axis::Z];
            auto cDist = target[Axis::C] - origin[Axis::C];
            return std::sqrt(std::pow(xDist,2) + std::pow(yDist,2) + std::pow(zDist,2) + std::pow(cDist,2));
        };

        if(d->output) d->output->UpdateProgress(0, "Moving");

        if(!instrument->DisableAutoFocus()) {
            d->ReportError(tr("Failed to disable auto-focus before moving axis"));
            return false;
        }

        double totalDistance = 1.0;
        if(d->mode == Impl::Mode::SingleAxis) {
            axisTargetPos[d->singleAxis.axis] = d->singleAxis.posInMm;
            totalDistance = calDist(axisTargetPos, axisStartPos);

            if(!instrument->MoveAxis(d->singleAxis.axis, d->singleAxis.posInMm)) {
                d->ReportError(tr("It fails to move %1 to %2mm [%3]").arg(d->singleAxis.axis._to_string())
                               .arg(d->singleAxis.posInMm).arg(instrument->GetErrorMessage()));
                return false;
            }
        } else {
            axisTargetPos[Axis::X] = d->xyAxis.xPosInMm;
            axisTargetPos[Axis::Y] = d->xyAxis.yPosInMm;
            totalDistance = calDist(axisTargetPos, axisStartPos);

            if(!instrument->MoveAxisXY(d->xyAxis.xPosInMm, d->xyAxis.yPosInMm)) {
                d->ReportError(tr("It fails to move sample stage [%1]").arg(instrument->GetErrorMessage()));
                return false;
            }
        }

        QElapsedTimer timer;
        timer.start();

        auto motionStatus = instrument->CheckAxisMotion();
        while((timer.elapsed() < 60000) && motionStatus.moving) {
            if(motionStatus.error) {
                d->ReportError(tr("It fails to move sample stage : %1").arg(motionStatus.message));
                return false;
            }

            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);

            const auto curPos = instrument->GetAxisPosition();
            AppEntity::SystemStatus::GetInstance()->SetCurrentGlobalPosition(curPos);

            if(d->output) {
                auto updatePosition = [&](AppEntity::Axis axis, double posInMM)->void {
                    if(axisPos[axis] != posInMM) {
                        axisPos[axis] = posInMM;
                        d->output->UpdateGlobalPosition(axis, posInMM);
                    }
                };

                updatePosition(Axis::X, curPos.toMM().x);
                updatePosition(Axis::Y, curPos.toMM().y);
                updatePosition(Axis::Z, curPos.toMM().z);
                updatePosition(Axis::C, instrument->GetAxisPositionMM(AppEntity::Axis::C));

                const auto remainDistance = calDist(axisPos, axisTargetPos);

                d->output->UpdateProgress(std::min<double>(0.99, 1.0 - (remainDistance/totalDistance)));
            }

            motionStatus = instrument->CheckAxisMotion();
        }

        if(motionStatus.moving) {
            d->ReportError(tr("Timeout is occurred"));
            return false;
        }

        if(d->output) d->output->UpdateProgress(1.0);

        return true;
    }
}