#include "IInstrument.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    static IInstrument* theInstance{ nullptr };

    IInstrument::IInstrument() {
        theInstance = this;
    }

    IInstrument::~IInstrument() {
    }

    auto IInstrument::GetInstance() -> IInstrument* {
        return theInstance;
    }
}