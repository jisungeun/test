#include "ISystemConfigWriter.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    static ISystemConfigWriter* theInstance{ nullptr };
    ISystemConfigWriter::ISystemConfigWriter() {
        theInstance = this;
    }

    ISystemConfigWriter::~ISystemConfigWriter() {
    }

    auto ISystemConfigWriter::GetInstance() -> ISystemConfigWriter* {
        return theInstance;
    }
}
