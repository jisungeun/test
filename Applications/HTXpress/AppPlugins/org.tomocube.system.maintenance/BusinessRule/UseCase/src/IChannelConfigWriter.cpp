#include "IChannelConfigWriter.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    static IChannelConfigWriter* theInstance{ nullptr };

    IChannelConfigWriter::IChannelConfigWriter() {
        theInstance = this;
    }

    IChannelConfigWriter::~IChannelConfigWriter() {
    }

    auto IChannelConfigWriter::GetInstance() -> IChannelConfigWriter* {
        return theInstance;
    }
}