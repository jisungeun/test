#include <QDir>

#include "IsDataFolder.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    IsDataFolder::IsDataFolder() : IUseCase("IsDataFolder") {
    }

    IsDataFolder::~IsDataFolder() {
    }

    auto IsDataFolder::IsValid(const QString& path) -> bool {
        const char* metaFile = ".datafolder.tsx";
        QDir dir(path);
        return dir.exists(metaFile);
    }

    auto IsDataFolder::Perform() -> bool {
        return true;
    }
}