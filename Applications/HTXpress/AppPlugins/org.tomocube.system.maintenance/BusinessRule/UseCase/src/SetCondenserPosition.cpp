#include "IInstrument.h"
#include "SetCondenserPosition.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    struct SetCondenserPosition::Impl {
        double position;
    };

    SetCondenserPosition::SetCondenserPosition() : IUseCase("SetCondenserPosition"), d{new Impl} {
    }

    SetCondenserPosition::~SetCondenserPosition() {
    }

    auto SetCondenserPosition::SetPositionMM(double position) -> void {
        d->position = position;
    }

    auto SetCondenserPosition::Perform() -> bool {
        auto instrument = IInstrument::GetInstance();
        if(!instrument->IsInitialized()) {
            Error("Instrument is not initialized");
            return false;
        }

        if(!instrument->MoveAxis(AppEntity::Axis::C, d->position)) {
            Error("It fails to move C axis");
            return false;
        }

        return true;
    }
}