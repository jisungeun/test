#include "IChannelConfigReader.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    static IChannelConfigReader* theInstance{ nullptr };

    IChannelConfigReader::IChannelConfigReader() {
        theInstance = this;
    }

    IChannelConfigReader::~IChannelConfigReader() {
    }

    auto IChannelConfigReader::GetInstance() -> IChannelConfigReader* {
        return theInstance;
    }
}