#include <QCoreApplication>

#include <System.h>
#include <SystemStatus.h>

#include "IInstrument.h"

#include "StartInitialize.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    struct StartInitialize::Impl {
        IInstrumentOutputPort* output{ nullptr };

        auto ReportError(StartInitialize* p, const QString& message)->void;
    };

    auto StartInitialize::Impl::ReportError(StartInitialize* p, const QString& message) -> void {
        p->Error(message);
        output->UpdateFailed(message);
    }

    StartInitialize::StartInitialize(IInstrumentOutputPort* output) : IUseCase("StartInialize"), d{std::make_unique<Impl>()} {
        d->output = output;
    }

    StartInitialize::~StartInitialize() {
    }

    auto StartInitialize::Perform() -> bool {
        auto status = AppEntity::SystemStatus::GetInstance();
        auto sysConfig = AppEntity::System::GetSystemConfig();

        auto instrument = IInstrument::GetInstance();
        if(!instrument) {
            Error("No instrument instance exists");
            return false;
        }

        if(instrument->IsInitialized()) {
            Print("Instrument is already initialized");
        } else {
            d->output->UpdateProgress(0, "Instrument Initialization");

            if(!instrument->StartInitialize()) {
                const auto msg = QString("Initialization can't be started - %1").arg(instrument->GetErrorMessage());
                d->ReportError(this, msg);
                return false;
            }

            double progress = 0;
            while(progress < 1.0) {
                QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents, 200);
                auto resp = instrument->GetInitializationProgress();
                progress = std::get<1>(resp);
                d->output->UpdateProgress(progress * 0.9);
                if(std::get<0>(resp) == false) {
                    const auto msg = QString("Initialization is failed - %1").arg(instrument->GetErrorMessage());
                    d->ReportError(this, msg);
                    return false;
                }
            }
        }

        const auto curPos = instrument->GetAxisPosition();
        status->SetCurrentGlobalPosition(curPos);

        const auto cPos = instrument->GetAxisPositionMM(AppEntity::Axis::C);
        status->SetCurrentCondensorPosition(cPos);
        Print(QString("Current position (X,Y,Z,C)=(%1,%2,%3,%4)").arg(curPos.toMM().x).arg(curPos.toMM().y)
                                                                 .arg(curPos.toMM().z).arg(cPos));

        d->output->UpdateProgress(1.0);

        return true;
    }
}