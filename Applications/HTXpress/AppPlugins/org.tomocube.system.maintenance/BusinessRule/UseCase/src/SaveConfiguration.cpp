#include "ISystemConfigWriter.h"
#include "SaveConfiguration.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    struct SaveConfiguration::Impl {
    };

    SaveConfiguration::SaveConfiguration() : IUseCase("SaveConfiguration"), d{new Impl} {
    }

    SaveConfiguration::~SaveConfiguration() {
    }

    auto SaveConfiguration::Perform() -> bool {
        auto* writer = ISystemConfigWriter::GetInstance();
        if(!writer) {
            Error("No valid system configuration writer exists");
            return false;
        }

        if(!writer->Write()) {
            Error("It fails to write system configuration");
            return false;
        }

        return true;
    }
}