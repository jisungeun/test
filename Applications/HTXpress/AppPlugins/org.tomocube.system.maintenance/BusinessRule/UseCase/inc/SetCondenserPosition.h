#pragma once
#include <memory>

#include "IUseCase.h"
#include "HTX_System_Maintenance_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    class HTX_System_Maintenance_UseCase_API SetCondenserPosition : public IUseCase {
    public:
        SetCondenserPosition();
        ~SetCondenserPosition() override;

        auto SetPositionMM(double position)->void;

    protected:
        auto Perform() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}