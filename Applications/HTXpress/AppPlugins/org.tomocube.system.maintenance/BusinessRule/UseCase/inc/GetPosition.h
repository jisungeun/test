#pragma once
#include <memory>
#include <QCoreApplication>

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "IInstrumentOutputPort.h"
#include "HTX_System_Maintenance_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    class HTX_System_Maintenance_UseCase_API GetPosition : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(GetPosition)

    public:
        GetPosition(IInstrumentOutputPort* output = nullptr);
        ~GetPosition() override;
        
        auto SetAxis(const AppEntity::Axis axis)->void;
        auto Get() const->double;
        
    protected:
        auto Perform() -> bool override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}