#pragma once
#include <memory>
#include <QCoreApplication>

#include <AppEntityDefines.h>

#include "IUseCase.h"
#include "IInstrumentOutputPort.h"
#include "HTX_System_Maintenance_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    class HTX_System_Maintenance_UseCase_API MoveAxis : public IUseCase {
        Q_DECLARE_TR_FUNCTIONS(MoveAxis)

    public:
        MoveAxis(IInstrumentOutputPort* output = nullptr);
        ~MoveAxis() override;
        
        auto SetTarget(const AppEntity::Axis axis, const double posInMm)->void;
        auto SetTargetXY(const double xPosInMm, const double yPosInMm)->void;
        
    protected:
        auto Perform() -> bool override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}