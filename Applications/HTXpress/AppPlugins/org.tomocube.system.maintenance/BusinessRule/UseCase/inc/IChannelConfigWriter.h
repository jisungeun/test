#pragma once
#include <memory>

#include <ChannelConfig.h>

#include "HTX_System_Maintenance_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    class HTX_System_Maintenance_UseCase_API IChannelConfigWriter {
    public:
        using Pointer = std::shared_ptr<IChannelConfigWriter>;
        using ImagingMode = AppEntity::ImagingMode;
        using ChannelConfig = AppEntity::ChannelConfig;

    protected:
        IChannelConfigWriter();

    public:
        virtual ~IChannelConfigWriter();

        static auto GetInstance()->IChannelConfigWriter*;

        virtual auto Write(ImagingMode mode, const ChannelConfig& config)->bool = 0;
    };
}