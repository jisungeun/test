#pragma once
#include <memory>
#include <QString>

#include "HTX_System_Maintenance_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    class HTX_System_Maintenance_UseCase_API ISystemConfigWriter {
    public:
        using Pointer = std::shared_ptr<ISystemConfigWriter>;

    protected:
        ISystemConfigWriter();

    public:
        virtual ~ISystemConfigWriter();

        static auto GetInstance()->ISystemConfigWriter*;

        virtual auto Write()->bool = 0;
    };
}
