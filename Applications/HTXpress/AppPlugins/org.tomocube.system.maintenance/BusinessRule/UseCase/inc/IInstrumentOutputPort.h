#pragma once
#include <memory>
#include <QString>

#include <AppEntityDefines.h>

#include "HTX_System_Maintenance_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    class HTX_System_Maintenance_UseCase_API IInstrumentOutputPort {
    public:
        IInstrumentOutputPort();
        virtual ~IInstrumentOutputPort();

        virtual auto UpdateFailed(const QString& message)->void = 0;
        virtual auto UpdateProgress(double progress, const QString& message=QString())->void = 0;
        virtual auto UpdateGlobalPosition(AppEntity::Axis axis, double posInMm)->void = 0;
    };
}