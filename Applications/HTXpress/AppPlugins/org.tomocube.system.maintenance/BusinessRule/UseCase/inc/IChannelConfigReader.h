#pragma once
#include <memory>

#include <AppEntityDefines.h>
#include <ChannelConfig.h>

#include "HTX_System_Maintenance_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    class HTX_System_Maintenance_UseCase_API IChannelConfigReader {
    public:
        using Pointer = std::shared_ptr<IChannelConfigReader>;
        using ImagingMode = AppEntity::ImagingMode;
        using ChannelConfig = AppEntity::ChannelConfig;

    protected:
        IChannelConfigReader();

    public:
        virtual ~IChannelConfigReader();

        static auto GetInstance()->IChannelConfigReader*;

        virtual auto Read(ImagingMode mode, ChannelConfig& config) const->bool = 0;
    };
}