#pragma once
#include <memory>

#include "IUseCase.h"
#include "HTX_System_Maintenance_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    class HTX_System_Maintenance_UseCase_API IsDataFolder : public IUseCase {
    public:
        IsDataFolder();
        ~IsDataFolder() override;

        auto IsValid(const QString& path)->bool;

    protected:
        auto Perform() -> bool override;
    };
}