#pragma once
#include <memory>

#include <AppEntityDefines.h>
#include <Position.h>

#include "IImagePort.h"
#include "HTX_System_Maintenance_UseCaseExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::UseCase {
    class HTX_System_Maintenance_UseCase_API IInstrument {
    public:
        using Pointer = std::shared_ptr<IInstrument>;

        struct MotionStatus {
            bool error{ false };
            bool moving{ false };
            bool afFailed{ false };
            QString message;
        };

    protected:
        IInstrument();

    public:
        virtual ~IInstrument();

        static auto GetInstance()->IInstrument*;

        virtual auto StartInitialize()->bool = 0;
        virtual auto GetInitializationProgress() const->std::tuple<bool,double> = 0;
        virtual auto IsInitialized() const->bool = 0;

        virtual auto InstallImagePort(IImagePort::Pointer port)->void = 0;
        virtual auto UninstallImagePort(IImagePort::Pointer port)->void = 0;

        virtual auto MoveAxis(const AppEntity::Axis axis, const double targetMM) -> bool = 0;
        virtual auto MoveAxisXY(const double xTargetMM, const double yTargetMM) -> bool = 0;
        virtual auto CheckAxisMotion() const->MotionStatus = 0;
        virtual auto GetAxisPosition() const->AppEntity::Position = 0;
        virtual auto GetAxisPositionMM(const AppEntity::Axis axis) const -> double = 0;

        virtual auto DisableAutoFocus()->bool = 0;

        virtual auto ChangeHTIntensity(int32_t intensity) -> bool = 0;
        virtual auto GetErrorMessage() const -> QString = 0;
    };
}