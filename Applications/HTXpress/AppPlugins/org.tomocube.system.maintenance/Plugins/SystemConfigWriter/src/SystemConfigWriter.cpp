#define LOGGER_TAG "[SytemConfigWriter]"
#include <TCLogger.h>
#include <System.h>
#include <SystemConfigIOWriter.h>

#include "SystemConfigWriter.h"

namespace HTXpress::AppPlugins::System::Maintenance::Plugins::SystemConfigWriter {
    struct Writer::Impl {
        QString path;
    };

    Writer::Writer() : UseCase::ISystemConfigWriter(), d{new Impl} {
    }

    Writer::~Writer() {
    }

    auto Writer::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto Writer::Write() -> bool {
        auto writer = AppComponents::SystemConfigIO::Writer();
        auto sysConfig = AppEntity::System::GetSystemConfig();
        return writer.Write(d->path, sysConfig);
    }
}