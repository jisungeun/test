#define LOGGER_TYPE "[UseCase]"

#include <TCLogger.h>

#include "UseCaseLogger.h"

namespace HTXpress::AppPlugins::System::Maintenance::Plugins::UseCaseLogger {
    Logger::Logger() : UseCase::IUseCaseLogger() {
    }

    Logger::~Logger() {
    }

    auto Logger::Log(const QString& useCase, const QString& message)->void {
        QLOG_INFO() << "[" << useCase << "]" << message;
    }

    auto Logger::Error(const QString& useCase, const QString& message)->void {
        QLOG_ERROR() << "[" << useCase << "]" << message;
    }
}