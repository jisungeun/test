#pragma once
#include <memory>
#include <QString>

#include "HTX_System_Maintenance_ChannelConfigIOExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::Plugins::ChannelConfigIO {
    class HTX_System_Maintenance_ChannelConfigIO_API Plugin {
    public:
        Plugin();
        ~Plugin();

        auto SetTopPath(const QString& path)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}