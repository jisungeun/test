#include <QSettings>
#include <QFile>

#include <ChannelConfigReader.h>
#include "Reader.h"

namespace HTXpress::AppPlugins::System::Maintenance::Plugins::ChannelConfigIO {
    struct Reader::Impl {
        AppComponents::ChannelConfigIO::Reader reader;
    };

    Reader::Reader() : UseCase::IChannelConfigReader(), d{new Impl} {
    }

    Reader::~Reader() {
    }

    auto Reader::SetTopPath(const QString& path) -> void {
        d->reader.SetTopPath(path);
    }

    auto Reader::Read(ImagingMode mode, ChannelConfig& config) const -> bool {
        return d->reader.Read(mode, config);
    }
}
