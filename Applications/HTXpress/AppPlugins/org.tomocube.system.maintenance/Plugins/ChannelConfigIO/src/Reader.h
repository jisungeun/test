#pragma once
#include <memory>

#include <IChannelConfigReader.h>

namespace HTXpress::AppPlugins::System::Maintenance::Plugins::ChannelConfigIO {
    class Reader : public UseCase::IChannelConfigReader {
    public:
        using Pointer = std::shared_ptr<Reader>;

    public:
        Reader();
        ~Reader();

        auto SetTopPath(const QString& path)->void;
        auto Read(ImagingMode mode, ChannelConfig& config) const -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}