#pragma once
#include <memory>

#include <IChannelConfigWriter.h>

namespace HTXpress::AppPlugins::System::Maintenance::Plugins::ChannelConfigIO {
    class Writer : public UseCase::IChannelConfigWriter {
    public:
        using Pointer = std::shared_ptr<Writer>;

    public:
        Writer();
        ~Writer();

        auto SetTopPath(const QString& path)->void;
        auto Write(ImagingMode mode, const ChannelConfig& config) -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}