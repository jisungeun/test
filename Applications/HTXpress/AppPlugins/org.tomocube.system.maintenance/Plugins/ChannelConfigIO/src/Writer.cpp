#include <QSettings>

#include <ChannelConfigWriter.h>
#include "Writer.h"

namespace HTXpress::AppPlugins::System::Maintenance::Plugins::ChannelConfigIO {
    struct Writer::Impl {
        AppComponents::ChannelConfigIO::Writer writer;
    };

    Writer::Writer() : UseCase::IChannelConfigWriter(), d{new Impl} {
    }

    Writer::~Writer() {
    }

    auto Writer::SetTopPath(const QString& path) -> void {
        d->writer.SetTopPath(path);
    }

    auto Writer::Write(ImagingMode mode, const ChannelConfig& config) -> bool {
        return d->writer.Write(mode, config);
    }
}
