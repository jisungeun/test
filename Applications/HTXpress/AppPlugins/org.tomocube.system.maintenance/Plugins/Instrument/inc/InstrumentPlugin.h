#pragma once
#include <memory>

#include <IInstrument.h>
#include "HTX_System_Maintenance_InstrumentExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::Plugins::Instrument {
    class HTX_System_Maintenance_Instrument_API Instrument : public UseCase::IInstrument {
    public:
        Instrument();
        ~Instrument() override;

        auto StartInitialize() -> bool override;
        auto GetInitializationProgress() const -> std::tuple<bool, double> override;
        auto IsInitialized() const -> bool override;

        auto CleanUp() -> void;

        auto InstallImagePort(UseCase::IImagePort::Pointer port) -> void override;
        auto UninstallImagePort(UseCase::IImagePort::Pointer port) -> void override;

        auto MoveAxis(const AppEntity::Axis axis, const double targetMM) -> bool override;
        auto MoveAxisXY(const double xTargetMM, const double yTargetMM) -> bool override;
        auto CheckAxisMotion() const -> MotionStatus override;
        auto GetAxisPosition() const -> AppEntity::Position override;
        auto GetAxisPositionMM(const AppEntity::Axis axis) const -> double override;

        auto DisableAutoFocus() -> bool override;

        auto ChangeHTIntensity(int32_t intensity) -> bool override;
        auto GetErrorMessage() const -> QString override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}