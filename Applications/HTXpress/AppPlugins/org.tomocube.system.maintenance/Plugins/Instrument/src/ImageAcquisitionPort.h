    #pragma once
#include <memory>

#include <ImagePort.h>
#include <IImagePort.h>

namespace HTXpress::AppPlugins::System::Maintenance::Plugins::Instrument {
    class Instrument;

    class ImageAcquisitionPort : public AppComponents::Instrument::ImagePort {
    public:
        ImageAcquisitionPort();
        ~ImageAcquisitionPort() override;

        auto Send(AppComponents::Instrument::Image::Pointer image) -> void override;
        auto Clear() -> void override;

    protected:
        auto InstallImagePort(UseCase::IImagePort::Pointer port)->void;
        auto UninstallImagePort(UseCase::IImagePort::Pointer port)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

        friend class Instrument;
    };
}