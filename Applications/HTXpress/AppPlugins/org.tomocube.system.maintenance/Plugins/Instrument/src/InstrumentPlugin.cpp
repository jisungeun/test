#define LOGGER_TAG "[InstrumentPlugin]"
#include <TCLogger.h>
#include <InstrumentFactory.h>
#include <System.h>
#include <ModelRepo.h>
#include <ImagingParameter.h>

#include "ImageAcquisitionPort.h"
#include "InstrumentPlugin.h"

namespace HTXpress::AppPlugins::System::Maintenance::Plugins::Instrument {
    using InstrumentComponent = AppComponents::Instrument::Instrument;
    using InstrumentConfig = AppComponents::Instrument::Config;
    using RawPosition = AppComponents::Instrument::RawPosition;
    using RawAxis = AppComponents::Instrument::Axis;

    struct Instrument::Impl {
        InstrumentComponent::Pointer instrument{ nullptr };
        ImageAcquisitionPort imageAcqPort;
        ImageAcquisitionPort condenserImageAcqPort;
        QString errorMessage{ "Not specified" };

        struct {
            int32_t minInterval{ 8000 };
            int32_t maxInterval{ 30000000 };
            int32_t minIdle{ 1000 };
        } condition;

        auto SetError(const QString& error)->void;
        auto LoadInstrument()->void;
        auto ConvAxis(const RawAxis& in)->AppEntity::Axis;
        auto ConvAxis(const AppEntity::Axis& axis)->RawAxis;
    };

    auto Instrument::Impl::SetError(const QString& error) -> void {
        errorMessage = error;
    }

    auto Instrument::Impl::LoadInstrument() -> void {
        using namespace AppComponents::Instrument;
        if(instrument) return;

        auto sysConfig = AppEntity::System::GetSystemConfig();
        auto model = AppEntity::ModelRepo::GetInstance()->GetModel(sysConfig->GetModel());

        if(sysConfig->GetSimulation()) {
            instrument = InstrumentFactory::GetInstance(InstrumentType::Simulator);
        } else {
            instrument = InstrumentFactory::GetInstance(InstrumentType::HTX);
        }

        auto instConfig = InstrumentConfig();
        instConfig.SetMCUComm(sysConfig->GetMCUPort(), sysConfig->GetMCUBaudrate());
        for(auto axis : RawAxis::_values()) {
            const auto axisEntity = ConvAxis(axis);
            instConfig.SetAxisResolutionPPM(axis, model->AxisResolutionPPM(axisEntity));
            instConfig.SetAxisLimitMM(axis, 
                                      model->AxisLowerLimitMM(axisEntity), 
                                      model->AxisUpperLimitMM(axisEntity));
            instConfig.SetAxisMotionTimeSPM(axis, model->AxisMotionTimeSPM(axisEntity));
        }

        const auto [xSafeMM, ySafeMM] = model->SafeXYRangeMM();
        const auto sysCenter = sysConfig->GetSystemCenter().toMM();
        instConfig.SetAxisSafeRangeMM(RawAxis::AxisX, sysCenter.x - xSafeMM/2, sysCenter.x + xSafeMM/2);
        instConfig.SetAxisSafeRangeMM(RawAxis::AxisY, sysCenter.y - ySafeMM/2, sysCenter.y + ySafeMM/2);

        instConfig.SetAxisCompensation(RawAxis::AxisX, sysConfig->GetAxisCompensation(ConvAxis(RawAxis::AxisX)));
        instConfig.SetAxisCompensation(RawAxis::AxisY, sysConfig->GetAxisCompensation(ConvAxis(RawAxis::AxisY)));

        instConfig.SetSafeZPositionMM(model->SafeZPositionMM());
        instConfig.SetLoadingPositionMM(model->LoadingXPositionMM(), 
                                        model->LoadingYPositionMM());

        auto emissionChannels = sysConfig->GetFLEmissions();
        for(auto channel : emissionChannels) {
            AppEntity::FLFilter filter;
            if(!sysConfig->GetFLEmission(channel, filter)) {
                QLOG_WARN() << "FL emission filter configuration may be invalid [channel=" << channel << "]";
                continue;
            }
            instConfig.SetEmissionFilter(channel, filter);
        }

        const auto emissionFilterPositions = model->EmissionFilterPules();
        for(int32_t idx=0; idx<emissionFilterPositions.length(); idx++) {
            instConfig.SetEmissionFilterPosition(idx, emissionFilterPositions.at(idx));
        }

        const auto afMaxTrials = sysConfig->GetAutofocusParameter(AppEntity::AutoFocusParameter::MaxTrialCount);
        instConfig.SetAFMaximumTrials(afMaxTrials);

        instConfig.SetHTTriggerScanOffsetPulse(model->HTTriggerStartOffsetPulse(), model->HTTriggerEndOffsetPulse());
        instConfig.SetHTTriggerPulseWidthUSec(model->HTTriggerPulseWidthUSec());
        instConfig.SetHTTriggerReadOutUSec(model->HTTriggerReadOutUSec());
        instConfig.SetHTTriggerExposureUSec(model->HTTriggerExposureUSec());
        instConfig.SetHTTriggerIntervalMarginUSec(model->HTTriggerIntervalMarginUSec());
        instConfig.SetHTTriggerAccelerationFactor(model->HTTriggerAccelerationFactor());

        instConfig.SetImagingCamera(sysConfig->GetImagingCameraSerial());
        instConfig.SetCondenserCamera(sysConfig->GetCondenserCameraSerial());

        instConfig.SetCondenserAFImagingChannels(model->CondenserAFLightChannel());
        if(sysConfig->IsCondenserAFScanParameterOverriden()) {
            instConfig.SetCondenserAFScanCondition(sysConfig->GetCondenserAFScanStartPosPulse(),
                                                   sysConfig->GetCondenserAFScanIntervalPulse(),
                                                   sysConfig->GetCondenserAFScanTriggerSlices());
        } else {
            instConfig.SetCondenserAFScanCondition(model->CondenserAFScanStartPosPulse(),
                                                   model->CondenserAFScanIntervalPulse(),
                                                   model->CondenserAFScanTriggerSlices());
        }
        instConfig.SetCondenserAFPatternCondition(model->CondenserAFScanReadOutUSec(),
                                                  model->CondenserAFScanExposureUSec(),
                                                  model->CondenserAFScanIntervalMarginUSec(),
                                                  model->CondenserAFScanPulseWidthUSec());
        instConfig.SetMultiDishHolderThicknessMM(model->MultiDishHolderThickness());
        instConfig.SetFlOutputRange(sysConfig->GetFlOutputRangeMin(),
                                    sysConfig->GetFlOutputRangeMax());

        using InstPreviewParam = AppComponents::Instrument::PreviewParam;
        using PreviewParam = AppEntity::PreviewParam;

        instConfig.SetPreviewParameter(InstPreviewParam::StartEndMarginPulse, 
                                       model->PreviewParameter(PreviewParam::StartEndMarginPulse));
        instConfig.SetPreviewParameter(InstPreviewParam::ForwardDelayCompensationPulse,
                                       model->PreviewParameter(PreviewParam::ForwardDelayCompensationPulse));
        instConfig.SetPreviewParameter(InstPreviewParam::BackwardDelayCompensationPulse,
                                       model->PreviewParameter(PreviewParam::BackwardDelayCompensationPulse));
        instConfig.SetPreviewParameter(InstPreviewParam::LedIntensity,
                                       sysConfig->GetPreviewLightIntensity());
        instConfig.SetPreviewParameter(InstPreviewParam::ExposureUSec,
                                       model->PreviewParameter(PreviewParam::ExposureUSec));
        instConfig.SetPreviewParameter(InstPreviewParam::ReadoutUSec,
                                       model->PreviewParameter(PreviewParam::ReadoutUSec));
        instConfig.SetPreviewParameter(InstPreviewParam::TriggerPulseWidth,
                                       model->PreviewParameter(PreviewParam::TriggerPulseWidth));
        instConfig.SetPreviewParameter(InstPreviewParam::AccelerationFactor,
                                       model->PreviewParameter(PreviewParam::AccelerationFactor));
        instConfig.SetPreviewParameter(InstPreviewParam::LedChannel,
                                       model->PreviewParameter(PreviewParam::LedChannel));
        instConfig.SetPreviewParameter(InstPreviewParam::FilterChannel,
                                       model->PreviewParameter(PreviewParam::FilterChannel));
        instConfig.SetPreviewParameter(InstPreviewParam::OverlapMM,
                                       model->PreviewParameter(PreviewParam::OverlapMM));
        instConfig.SetPreviewParameter(InstPreviewParam::SingleMoveMM,
                                       model->PreviewParameter(PreviewParam::SingleMoveMM));

        const auto [coeffA, coeffB, coeffP, coeffQ] = sysConfig->GetPreviewGainCoefficient();
        instConfig.SetPreviewParameter(InstPreviewParam::GainCoefficientA, coeffA);
        instConfig.SetPreviewParameter(InstPreviewParam::GainCoefficientB, coeffB);
        instConfig.SetPreviewParameter(InstPreviewParam::GainCoefficientP, coeffP);
        instConfig.SetPreviewParameter(InstPreviewParam::GainCoefficientQ, coeffQ);

        instrument->SetConfig(instConfig);

        condition.minInterval = model->MinimumIntervalUSec();
        condition.maxInterval = model->MaximumIntervalUSec();
        condition.minIdle = model->MinimumIdleUSec();
    }

    auto Instrument::Impl::ConvAxis(const RawAxis& axis) -> AppEntity::Axis {
        switch(axis) {
        case RawAxis::AxisX:
            return AppEntity::Axis::X;
        case RawAxis::AxisY:
            return AppEntity::Axis::Y;
        case RawAxis::AxisZ:
            return AppEntity::Axis::Z;
        case RawAxis::AxisC:
            return AppEntity::Axis::C;
        case RawAxis::AxisD:
            return AppEntity::Axis::D;
        case RawAxis::AxisF:
            return AppEntity::Axis::F;
        }
        return AppEntity::Axis::X;
    }

    auto Instrument::Impl::ConvAxis(const AppEntity::Axis& axis) -> RawAxis {
        switch(axis) {
        case AppEntity::Axis::X:
            return RawAxis::AxisX;
        case AppEntity::Axis::Y:
            return RawAxis::AxisY;
        case AppEntity::Axis::Z:
            return RawAxis::AxisZ;
        case AppEntity::Axis::C:
            return RawAxis::AxisC;
        case AppEntity::Axis::F:
            return RawAxis::AxisF;
        case AppEntity::Axis::D:
            return RawAxis::AxisD;
        }
        return RawAxis::AxisX;
    }

    Instrument::Instrument() : UseCase::IInstrument(), d{new Impl} {
    }

    Instrument::~Instrument() {
        if(!d->instrument) return;

        d->instrument->UninstallImagePort(AppComponents::Instrument::ImageSource::ImagingCamera, &d->imageAcqPort);
        d->instrument->UninstallImagePort(AppComponents::Instrument::ImageSource::CondenserCamera, &d->condenserImageAcqPort);
        QLOG_INFO() << "Destroyed";
    }

    auto Instrument::StartInitialize() -> bool {
        d->LoadInstrument();

        d->instrument->InstallImagePort(AppComponents::Instrument::ImageSource::ImagingCamera, &d->imageAcqPort);
        d->instrument->InstallImagePort(AppComponents::Instrument::ImageSource::CondenserCamera, &d->condenserImageAcqPort);

        if(!d->instrument->CheckInitialized()) {
            return d->instrument->Initialize();
        }
        return true;
    }

    auto Instrument::GetInitializationProgress() const -> std::tuple<bool, double> {
        const auto resp = d->instrument->GetInitializationProgress();
        if(std::get<0>(resp) == false) {
            d->SetError(d->instrument->GetError());
        }
        return resp;
    }

    auto Instrument::IsInitialized() const -> bool {
        d->LoadInstrument();
        return d->instrument->CheckInitialized();
    }

    auto Instrument::CleanUp() -> void {
        if(!d->instrument) return;
        d->instrument->CleanUp();
    }

    auto Instrument::InstallImagePort(UseCase::IImagePort::Pointer port) -> void {
        d->imageAcqPort.InstallImagePort(port);
    }

    auto Instrument::UninstallImagePort(UseCase::IImagePort::Pointer port) -> void {
        d->imageAcqPort.UninstallImagePort(port);
    }


    auto Instrument::MoveAxis(const AppEntity::Axis axis, const double targetMM) -> bool {
        return d->instrument->Move(d->ConvAxis(axis), targetMM);
    }

    auto Instrument::MoveAxisXY(const double xTargetMM, const double yTargetMM) -> bool {
        return d->instrument->MoveXY(xTargetMM, yTargetMM);
    }

    auto Instrument::CheckAxisMotion() const -> MotionStatus {
        MotionStatus status;

        const auto componentStatus = d->instrument->IsMoving();
        status.error = componentStatus.error;
        status.moving = componentStatus.moving;
        status.afFailed = componentStatus.afFailed;
        status.message = componentStatus.message;

        return status;
    }

    auto Instrument::GetAxisPosition() const -> AppEntity::Position {
        struct {
            RawPosition x;
            RawPosition y;
            RawPosition z;
        } currentPos;

        if(!d->instrument->GetPositionXYZ(currentPos.x, currentPos.y, currentPos.z)) return AppEntity::Position();

        return AppEntity::Position::fromMM(currentPos.x, currentPos.y, currentPos.z);
    }

    auto Instrument::GetAxisPositionMM(const AppEntity::Axis axis) const -> double {
        RawPosition pos;
        if(!d->instrument->GetPosition(d->ConvAxis(axis), pos)) return 0;
        return pos;
    }

    auto Instrument::DisableAutoFocus() -> bool {
        auto res = d->instrument->EnableAutoFocus(false);
        return std::get<0>(res);
    }

    auto Instrument::ChangeHTIntensity(int32_t intensity) -> bool {
        Q_UNUSED(intensity)
        return true;
    }

    auto Instrument::GetErrorMessage() const -> QString {
        return d->errorMessage;
    }
}
