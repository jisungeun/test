#include <QList>

#include "ImageAcquisitionPort.h"

namespace HTXpress::AppPlugins::System::Maintenance::Plugins::Instrument {
    using SourceImage = AppComponents::Instrument::Image;

    struct ImageAcquisitionPort::Impl {
        QList<UseCase::IImagePort::Pointer> imagePorts;

        auto Convert(SourceImage::Pointer image)->AppEntity::RawImage::Pointer;
    };

    auto ImageAcquisitionPort::Impl::Convert(SourceImage::Pointer image) -> AppEntity::RawImage::Pointer {
        auto outImage = std::make_shared<AppEntity::RawImage>(image->GetSizeX(), 
                                                              image->GetSizeY(), 
                                                              image->GetBuffer());
        outImage->SetTimestamp(image->GetTimestamp());
        return outImage;
    }

    ImageAcquisitionPort::ImageAcquisitionPort() : ImagePort(), d{new Impl} {
    }

    ImageAcquisitionPort::~ImageAcquisitionPort() {
    }

    auto ImageAcquisitionPort::Send(AppComponents::Instrument::Image::Pointer image) -> void {
        for(auto port : d->imagePorts) {
            port->Send(d->Convert(image));
        }
    }

    auto ImageAcquisitionPort::Clear() -> void {
        for(auto port : d->imagePorts) {
            port->Clear();
        }
    }

    auto ImageAcquisitionPort::InstallImagePort(UseCase::IImagePort::Pointer port) -> void {
        if(!d->imagePorts.contains(port)) {
            d->imagePorts.push_back(port);
        }
    }

    auto ImageAcquisitionPort::UninstallImagePort(UseCase::IImagePort::Pointer port) -> void {
        d->imagePorts.removeOne(port);
    }
}
