#include "InstrumentPresenter.h"

namespace HTXpress::AppPlugins::System::Maintenance::Interactor {
    struct InstrumentPresenter::Impl {
        IInstrumentView* view{ nullptr };
    };

    InstrumentPresenter::InstrumentPresenter(IInstrumentView* view)
        : UseCase::IInstrumentOutputPort()
        , d{ std::make_unique<Impl>() } {
        d->view = view;
    }

    InstrumentPresenter::~InstrumentPresenter() {
    }

    auto InstrumentPresenter::UpdateFailed(const QString& message) -> void {
        if(d->view) {
            d->view->UpdateFailed(message);
        }
    }

    auto InstrumentPresenter::UpdateProgress(double progress, const QString& message) -> void {
        if(d->view) {
            d->view->UpdateProgress(progress, message);
        }
    }

    auto InstrumentPresenter::UpdateGlobalPosition(AppEntity::Axis axis, double posInMm) -> void {
        if(d->view) {
            d->view->UpdateGlobalPosition(axis, posInMm);
        }
    }
}
