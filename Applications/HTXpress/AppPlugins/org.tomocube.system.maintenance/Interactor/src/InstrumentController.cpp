#include <StartInitialize.h>
#include <SetCondenserPosition.h>
#include <SetHTIntensity.h>
#include <MoveAxis.h>
#include <GetPosition.h>

#include "InstrumentController.h"

namespace HTXpress::AppPlugins::System::Maintenance::Interactor {
    struct InstrumentController::Impl {
        InstrumentPresenter* presenter{ nullptr };
    };

    InstrumentController::InstrumentController(InstrumentPresenter* presenter) : d{new Impl} {
        d-> presenter = presenter;
    }

    InstrumentController::~InstrumentController() {
    }

    auto InstrumentController::StartInitialize() -> bool {
        auto usecase = UseCase::StartInitialize(d->presenter);
        return usecase.Request();
    }

    auto InstrumentController::SetCondenserPosition(double position) -> bool {
        auto usecase = UseCase::SetCondenserPosition();
        usecase.SetPositionMM(position);
        return usecase.Request();
    }

    auto InstrumentController::SetHTIntensity(int32_t intensity) -> bool {
        auto usecase = UseCase::SetHTIntensity();
        usecase.SetIntensity(intensity);
        return usecase.Request();
    }

    auto InstrumentController::GetPosition(const AppEntity::Axis axis) const -> double {
        auto usecase = UseCase::GetPosition(d->presenter);
        usecase.SetAxis(axis);
        if(!usecase.Request()) return 0;
        return usecase.Get();
    }

    auto InstrumentController::MoveAxisMM(const AppEntity::Axis axis, const double posInMm) -> bool {
        auto usecase = UseCase::MoveAxis(d->presenter);
        usecase.SetTarget(axis, posInMm);
        return usecase.Request();
    }

    auto InstrumentController::MoveXY(const double xPosInMm, const double yPosInMm) -> bool {
        auto usecase = UseCase::MoveAxis(d->presenter);
        usecase.SetTargetXY(xPosInMm, yPosInMm);
        return usecase.Request();
    }
}
