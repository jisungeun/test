#pragma once
#include <memory>
#include <QString>

#include <AppEntityDefines.h>

#include "HTX_System_Maintenance_InteractorExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::Interactor {
    class HTX_System_Maintenance_Interactor_API IInstrumentView {
    public:
        IInstrumentView();
        virtual ~IInstrumentView();

        virtual auto UpdateFailed(const QString& message)->void = 0;
        virtual auto UpdateProgress(double progress, const QString& message=QString())->void = 0;
        virtual auto UpdateGlobalPosition(AppEntity::Axis axis, double posInMm) -> void = 0;
    };
}