#pragma once
#include <memory>

#include <IInstrumentOutputPort.h>
#include "IInstrumentView.h"
#include "HTX_System_Maintenance_InteractorExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::Interactor {
    class HTX_System_Maintenance_Interactor_API InstrumentPresenter : public UseCase::IInstrumentOutputPort {
    public:
        InstrumentPresenter(IInstrumentView* view = nullptr);
        virtual ~InstrumentPresenter();

        auto UpdateFailed(const QString& message) -> void override;
        auto UpdateProgress(double progress, const QString& message) -> void override;
        auto UpdateGlobalPosition(AppEntity::Axis axis, double posInMm) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}