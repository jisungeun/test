#pragma once
#include <memory>

#include <AppEntityDefines.h>

#include "InstrumentPresenter.h"
#include "HTX_System_Maintenance_InteractorExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::Interactor {
    class HTX_System_Maintenance_Interactor_API InstrumentController {
    public:
        InstrumentController(InstrumentPresenter* presenter = nullptr);
        ~InstrumentController();

        auto StartInitialize()->bool;

        auto SetCondenserPosition(double position)->bool;
        auto SetHTIntensity(int32_t intensity)->bool;

        auto GetPosition(const AppEntity::Axis xis) const->double;
        auto MoveAxisMM(const AppEntity::Axis axis, const double posInMm)->bool;
        auto MoveXY(const double xPosInMm, const double yPosInMm)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
