#pragma once
#include <memory>

#include "HTX_System_Maintenance_InteractorExport.h"

namespace HTXpress::AppPlugins::System::Maintenance::Interactor {
    class HTX_System_Maintenance_Interactor_API ConfigController {
    public:
        ConfigController();
        ~ConfigController();

        auto Save()->bool;

        auto IsUsedDataFolder(const QString& path)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}