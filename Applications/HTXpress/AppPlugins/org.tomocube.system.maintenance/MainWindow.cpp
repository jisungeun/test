#include <QBoxLayout>
#include <QButtonGroup>

#include <MessageDialog.h>
#include <AppEvent.h>
#include <ProgressDialog.h>

#include "internal/ConfigPage.h"
#include "internal/MaintenancePage.h"
#include "internal/ModuleTestPage.h"
#include "internal/SetupPage.h"
#include "internal/EvaluationPage.h"
#include "Internal/InstrumentObserver.h"

#include "MainWindowControl.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"

namespace HTXpress::AppPlugins::System::Maintenance::App {
    using namespace TC::Framework;
    struct MainWindow::Impl {
        Ui::MainWindow ui;
        MainWindow* window{ nullptr };
        MainWindowControl control;

        TC::ProgressDialog* instrumentProgress{ nullptr };

        InstrumentObserver* instrumentObserver{ nullptr };

        QVariantMap appProperties;

        QVBoxLayout* menuLayout{ nullptr };
        QButtonGroup* menuButtons{ nullptr };

        Impl(MainWindow* p);

        auto InitGUI(const MainWindow* self)->void;
        auto AddPage(const QString& title, ISubPage* page)->void;
        auto CurrentPage() const->ISubPage*;

        auto UpdateProgress(double progress, const QString& message)->void;
        auto InstrumentFailed(const QString& message)->void;
    };

    MainWindow::Impl::Impl(MainWindow* p) : window{p} {
    }

    auto MainWindow::Impl::InitGUI(const MainWindow* self) -> void {
        menuLayout = new QVBoxLayout(ui.menuGroup);
        ui.menuGroup->setLayout(menuLayout);
        menuLayout->addStretch();

        for(const auto& groupBox : self->findChildren<QGroupBox*>()) {
            groupBox->setObjectName("gb-setup-main");
        }

        for(const auto& button : self->findChildren<QPushButton*>()) {
            button->setObjectName("bt-setup-main");
        }

        ui.contentPanel->setObjectName("panel");

        instrumentProgress = new TC::ProgressDialog(tr("Please wait."), tr("Close"), 0, 100, window);
        instrumentProgress->setWindowModality(Qt::WindowModal);
    }

    auto MainWindow::Impl::AddPage(const QString& title, ISubPage* page) -> void {
        int32_t idx = ui.contentPanel->addWidget(page);
        auto* btn = new QPushButton(title);
        btn->setCheckable(true);
        btn->setObjectName("bt-maintenance-menu");

        menuButtons->addButton(btn, idx);
        menuButtons->setExclusive(true);

        menuLayout->insertWidget(menuLayout->count()-1, btn);
        connect(btn, &QAbstractButton::clicked, window, 
                [this, idx]() {
                    auto* page = CurrentPage();
                    if(page && !page->IsClosable()) {
                        TC::MessageDialog::information(window, "Service", "It is not allowed to leave this menu.");
                        const auto previousContentIndex = ui.contentPanel->currentIndex();
                        menuButtons->button(previousContentIndex)->setChecked(true);
                        return;
                    }

                    if(page && page->IsModified()) {
                        TC::MessageDialog::information(window, "Service", "Changes are not saved.");
                        const auto previousContentIndex = ui.contentPanel->currentIndex();
                        menuButtons->button(previousContentIndex)->setChecked(true);
                        return;
                    }

                    this->ui.contentPanel->setCurrentIndex(idx);
                    page = CurrentPage();
                    if(!page) return;

                    page->Initialize();
                });
    }

    auto MainWindow::Impl::CurrentPage() const -> ISubPage* {
        return qobject_cast<ISubPage*>(ui.contentPanel->currentWidget());
    }

    auto MainWindow::Impl::UpdateProgress(double progress, const QString& message) -> void {
        if(instrumentProgress->isHidden()) {
            instrumentProgress->open();
            instrumentProgress->activateWindow();
        }

        if(!message.isEmpty()) instrumentProgress->SetContent(message);

        const auto step = std::min<int32_t>(progress*100, 100);
        instrumentProgress->setValue(step);

        if(step == 100) {
            instrumentProgress->hide();
        }
    }

    auto MainWindow::Impl::InstrumentFailed(const QString& message) -> void {
        instrumentProgress->hide();
        instrumentProgress->setValue(0);
        TC::MessageDialog::warning(window, tr("Instrument"), tr("Instrument failed:\n%1").arg(message));
    }

    MainWindow::MainWindow(QWidget* parent) : IMainWindowHTX("System Maintenance", "System Maintenance", parent), d{new Impl(this)} {
        d->ui.setupUi(this);
        d->InitGUI(this);

        d->instrumentObserver = new InstrumentObserver(this);

        d->menuButtons = new QButtonGroup(this);

        d->AddPage("Config", new ConfigPage(this));
        d->AddPage("Maintenance", new MaintenancePage(this));
        d->AddPage("Module Test", new ModuleTestPage(this));

        auto* setupPage = new SetupPage();
        d->AddPage("Setup", setupPage);
        connect(setupPage, SIGNAL(sigStart()), this, SLOT(onGotoExperimentSetup()));

        auto* evalPage = new EvaluationPage(this);
        d->AddPage("Evaluation", evalPage);
        connect(evalPage, SIGNAL(sigStart()), this, SLOT(onGotoExperimentSetup()));

        subscribeEvent("TabChange");
        connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(onHandleEvent(ctkEvent)));

        connect(d->ui.restoreBtn, SIGNAL(clicked()), this, SLOT(onRestore()));
        connect(d->ui.saveBtn, SIGNAL(clicked()), this, SLOT(onSave()));
        connect(d->ui.homeBtn, SIGNAL(clicked()), this, SLOT(onGotoHome()));

        connect(d->instrumentObserver, &InstrumentObserver::sigProgress, this, [this](double progress, const QString&message) {
            d->UpdateProgress(progress, message);
        });
        connect(d->instrumentObserver, &InstrumentObserver::sigFailed, this, [this](const QString& message) {
            d->InstrumentFailed(message);
        });
    }
    
    MainWindow::~MainWindow() {
        d->control.CleanUp();
    }

    auto MainWindow::TryActivate() -> bool {
        return true;
    }
    auto MainWindow::TryDeactivate() -> bool {
        return true;
    }
    auto MainWindow::IsActivate() -> bool {
        return true;
    }
    auto MainWindow::GetMetaInfo() -> QVariantMap {
        return d->appProperties;
    }

    auto MainWindow::Execute(const QVariantMap& params) -> bool {
        Q_UNUSED(params)
        return false;
    }

    void MainWindow::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
    }

    void MainWindow::onHandleEvent(const ctkEvent& ctkEvent) {
        Q_UNUSED(ctkEvent)   
    }

    void MainWindow::onRestore() {
        auto* page = d->CurrentPage();
        if(!page) return;
        page->Restore();
    }

    void MainWindow::onSave() {
        auto* page = d->CurrentPage();
        if(!page) return;
        page->Save();
    }

    void MainWindow::onGotoHome() {
        using StandardButton = TC::MessageDialog::StandardButton;

        auto* page = d->CurrentPage();
        if(page && !page->IsClosable()) {
            TC::MessageDialog::information(this, "Service", "It is not allowed to leave this menu.");
            const auto previousContentIndex = d->ui.contentPanel->currentIndex();
            d->menuButtons->button(previousContentIndex)->setChecked(true);
            return;
        }

        if(page && page->IsModified()) {
            const auto selection = TC::MessageDialog::question(this, "Preference", 
                                                               tr("Do you want to ignore changes that are not saved?"),
                                                               StandardButton::Yes | StandardButton::No, StandardButton::No);
            if(selection == StandardButton::Yes) {
                onRestore();
            }
            else return;
        }

        AppEvent appEvt(AppTypeEnum::APP_WITHOUT_ARG);
        appEvt.setFullName("org.tomocube.general.start");
        appEvt.setAppName("Start");

        publishSignal(appEvt, "System Maintenance");
    }

    void MainWindow::onGotoExperimentSetup() {
        using StandardButton = TC::MessageDialog::StandardButton;

        auto* page = d->CurrentPage();
        if(page && page->IsModified()) {
            const auto selection = TC::MessageDialog::question(this, "Preference", 
                                                               tr("Do you want to ignore changes that are not saved?"),
                                                               StandardButton::Yes | StandardButton::No, StandardButton::No);
            if(selection == StandardButton::Yes) {
                onRestore();
            }
            else return;
        }

        AppEvent appEvt(AppTypeEnum::APP_WITHOUT_ARG);
        appEvt.setFullName("org.tomocube.experiment.setup");
        appEvt.setAppName("Experiment Setup");

        publishSignal(appEvt, "System Maintenance");
    }
}
