#include <QFrame>
#include <QThread>
#include <QPushButton>

#include "DirectoryDeleteDialog.h"
#include "ui_DirectoryDeleteDialog.h"
#include "DirectoryDeleter.h"

namespace HTXpress::AppComponents::DirectoryDeleter {
    struct DirectoryDeleteDialog::Impl {
        explicit Impl(Self* self) : self(self) {
            frame = new QFrame(self);
            frame->setMinimumWidth(350);
            ui.setupUi(frame);
            self->SetContext(frame);
            self->SetStandardButtons(StandardButton::Cancel);
            self->SetClosable(false);
            self->adjustSize();
            self->SetTitle("Delete Files");
        }

        Self* self{};
        QString path{};
        QFrame* frame{};
        DirectoryDeleter* directoryDeleter{};

        Ui::DirectoryDeleteDialog ui{};
    };

    DirectoryDeleteDialog::DirectoryDeleteDialog(const QString& path, QWidget* parent) : CustomDialog(parent), d{std::make_unique<Impl>(this)} {
        d->path = path;

        d->ui.progressBar->setValue(0);

        auto button = GetButton(StandardButton::Cancel);
        if (button) {
            button->setText("Stop");
        }
    }

    DirectoryDeleteDialog::~DirectoryDeleteDialog() {
    }

    auto DirectoryDeleteDialog::Delete() -> void {
        d->directoryDeleter = new DirectoryDeleter(d->path);
        const auto workerThread = new QThread();

        d->directoryDeleter->moveToThread(workerThread);

        connect(workerThread, &QThread::started, d->directoryDeleter, &DirectoryDeleter::onDeleteDirectory);
        connect(d->directoryDeleter, &DirectoryDeleter::sigFinished, workerThread, &QThread::quit);
        connect(d->directoryDeleter, &DirectoryDeleter::sigFinished, d->directoryDeleter, &DirectoryDeleter::deleteLater);
        connect(d->directoryDeleter, &DirectoryDeleter::sigFinished, this, &Self::onFinished);
        connect(workerThread, &QThread::finished, workerThread, &QThread::deleteLater);
        connect(d->directoryDeleter, &DirectoryDeleter::sigProgress, d->ui.progressBar, &QProgressBar::setValue);
        connect(GetButton(StandardButton::Cancel), &QPushButton::clicked, this, &Self::onCancel);

        workerThread->start();
    }

    auto DirectoryDeleteDialog::GetMinimumWidth() const -> int {
        return minimumWidth();
    }

    void DirectoryDeleteDialog::onFinished(bool completed) {
        if (completed) {
            accept();
        }
        else {
            reject();
        }
    }

    void DirectoryDeleteDialog::onCancel() {
        d->ui.progressLabel->setText("Stop deleting...");
        d->directoryDeleter->Stop();
    }
}
