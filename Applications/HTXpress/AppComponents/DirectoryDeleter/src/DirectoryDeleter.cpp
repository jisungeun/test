#define LOGGER_TAG "[HTXDirectoryDeleter]"

#include <QDir>
#include <QFileInfo>

#include <TCLogger.h>

#include "DirectoryDeleter.h"

namespace HTXpress::AppComponents::DirectoryDeleter {
    struct DirectoryDeleter::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        Self* self{};

        QString dirPath{};
        QAtomicInt stopFlag{};

        auto DeleteDirectory(const QString& path, int32_t totalCount, int32_t& deletedCount) -> bool;
        auto GetTotalFileCount(const QString& path) -> int32_t;
    };

    DirectoryDeleter::DirectoryDeleter(const QString& path, QObject* parent) : QObject(parent), d{std::make_unique<Impl>(this)} {
        d->dirPath = path;
    }

    DirectoryDeleter::~DirectoryDeleter() {
    }

    void DirectoryDeleter::onDeleteDirectory() {
        const auto totalCount = d->GetTotalFileCount(d->dirPath);
        auto deletedCount = 0;
        auto result = true;

        if (d->DeleteDirectory(d->dirPath, totalCount, deletedCount)) {
            QLOG_INFO() << "Folder and its contents have been deleted successfully.";
        }
        else {
            QLOG_ERROR() << "Failed to delete the folder or its contents.";
            result = false;
        }

        emit sigFinished(result);
    }

    void DirectoryDeleter::Stop() {
        d->stopFlag.storeRelaxed(1);
    }

    auto DirectoryDeleter::Impl::DeleteDirectory(const QString& path, int32_t totalCount, int32_t& deletedCount) -> bool {
        if (stopFlag.loadRelaxed()) {
            return false;
        }

        QDir dir(path);
        if (!dir.exists()) {
            return false;
        }

        for (const QFileInfo& info : dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::AllDirs | QDir::Files, 
                                                       QDir::DirsFirst)) {
            if (info.isDir()) {
                if (!DeleteDirectory(info.absoluteFilePath(), totalCount, deletedCount)) {
                    return false;
                }
            }
            else {
                if (!dir.remove(info.absoluteFilePath())) {
                    return false;
                }
                ++deletedCount;
                emit self->sigProgress(static_cast<int>(static_cast<float>(deletedCount) / totalCount * 100));
            }
        }

        return dir.rmdir(path);
    }

    auto DirectoryDeleter::Impl::GetTotalFileCount(const QString& path) -> int32_t {
        int32_t count{};
        const QDir dir(path);

        for (const QFileInfo& info : dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                count += GetTotalFileCount(info.absoluteFilePath());
            }
            else {
                ++count;
            }
        }
        return count;
    }
}
