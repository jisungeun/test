#pragma once
#include <memory>
#include <QObject>

namespace HTXpress::AppComponents::DirectoryDeleter {
    class DirectoryDeleter final : public QObject{
        Q_OBJECT
    public:
        using Self = DirectoryDeleter;
        using Pointer = std::shared_ptr<Self>;

        explicit DirectoryDeleter(const QString& path, QObject* parent = nullptr);
        ~DirectoryDeleter() override;

        auto Stop() -> void;

    signals:
        void sigFinished(bool);
        void sigProgress(int);

    public slots:
        void onDeleteDirectory();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}