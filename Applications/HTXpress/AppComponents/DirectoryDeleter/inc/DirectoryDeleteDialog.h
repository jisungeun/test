#pragma once
#include <memory>

#include <QDialog>
#include <CustomDialog.h>

#include "HTXDirectoryDeleterExport.h"

namespace HTXpress::AppComponents::DirectoryDeleter {
    class HTXDirectoryDeleter_API DirectoryDeleteDialog final : public TC::CustomDialog {
        Q_OBJECT
    public:
        using Self = DirectoryDeleteDialog;
        using Pointer = std::shared_ptr<Self>;

        explicit DirectoryDeleteDialog(const QString& path, QWidget* parent = nullptr);
        ~DirectoryDeleteDialog() override;

        auto Delete() -> void;

    private:
        auto GetMinimumWidth() const -> int override;

    private slots:
        void onFinished(bool completed);
        void onCancel();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
