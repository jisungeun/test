project(HTXAcquisitionListPanel)

#Header files for external use
set(INTERFACE_HEADERS
    inc/AcquisitionListPanel.h
    inc/AcquisitionDataIndex.h
)

#Header files for internal use
set(PRIVATE_HEADERS
    src/AcquisitionListPanelControl.h
    src/AcquisitionDataRepo.h
    src/DataListTable.h
    src/DataListTableControl.h
    src/DataListModel.h
    src/DataListProxyModel.h
    src/TableDefines.h
    src/DataListTableConfig.h
    src/DataListTableHeader.h
    src/DataListTableFilterMenu.h
    src/FilterMenuManager.h
    src/ProcessingStatusDelegate.h
    src/DataListTableContextMenu.h
    src/DataSelectDelegate.h
)

#Sources
set(SOURCES
    src/AcquisitionListPanel.cpp
    src/AcquisitionDataIndex.cpp
    src/AcquisitionListPanelControl.cpp
    src/AcquisitionDataRepo.cpp
    src/DataListTable.cpp
    src/DataListTableControl.cpp
    src/DataListModel.cpp
    src/DataListProxyModel.cpp
    src/DataListTableConfig.cpp
    src/DataListTableHeader.cpp
    src/DataListTableFilterMenu.cpp
    src/FilterMenuManager.cpp
    src/ProcessingStatusDelegate.cpp
    src/DataListTableContextMenu.cpp
    src/DataSelectDelegate.cpp
)

#Qt UI files
set(UI_FORMS
    src/AcquisitionListPanel.ui
)

#Resource
set(RESOURCE
    resource/AcquisitionListPanel.qrc
)

add_library(${PROJECT_NAME} SHARED
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
    ${UI_FORMS}
	${SOURCES}
    ${RESOURCE}
)

add_library(HTXpress::AppComponents::AcquisitionListPanel ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<INSTALL_INTERFACE:inc>
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_libraries(${PROJECT_NAME}
	PUBLIC
        Qt5::Core
        Qt5::Widgets
        Extern::BetterEnums
        HTXpress::AppEntity
        TC::Components::TCLogger
        TC::Components::QtWidgets
)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
    PUBLIC
        /wd4251
    PRIVATE
        /W4 /WX
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/HTXpress/AppComponents")

generate_export_header(${PROJECT_NAME}
    EXPORT_MACRO_NAME ${PROJECT_NAME}_API
    EXPORT_FILE_NAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Export.h
)

# 'make install' to the correct locations
install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Config
    ARCHIVE  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    LIBRARY  DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT dev
    RUNTIME  DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT application_htx)
install(DIRECTORY inc/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT dev)

# This makes the project importable from the install directory
# Put config file in per-project dir (name MUST match), can also
# just go into 'cmake'.
install(EXPORT ${PROJECT_NAME}Config DESTINATION share/${PROJECT_NAME}/cmake COMPONENT dev)

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)

add_subdirectory(testUI)
add_subdirectory(testUnit)