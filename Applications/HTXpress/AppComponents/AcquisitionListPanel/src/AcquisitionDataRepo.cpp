﻿#include <QMap>

#include "AcquisitionDataRepo.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    //static AcquisitionDataRepo::UniqueKey gKeyIndex = 0; // unique key index

    struct AcquisitionDataRepo::Impl {
        UniqueKey keyIndex = 0;
        DataContainer dataList;
        auto IsExist(const AcquisitionDataIndex& dataIndex) -> bool;
    };

    auto AcquisitionDataRepo::Impl::IsExist(const AcquisitionDataIndex& dataIndex) -> bool {
        for (auto& it : dataList) {
            if (it.first == dataIndex) return true;
        }

        return false;
    }

    AcquisitionDataRepo::AcquisitionDataRepo() : d{std::make_unique<Impl>()} {
    }

    AcquisitionDataRepo::~AcquisitionDataRepo() = default;

    auto AcquisitionDataRepo::ClearDataList() -> void {
        d->dataList.clear();
        d->keyIndex = 0;
    }

    auto AcquisitionDataRepo::GetData(UniqueKey key) -> Value& {
        return d->dataList[key];
    }

    auto AcquisitionDataRepo::GetDataList() const -> DataContainer& {
        return d->dataList;
    }

    auto AcquisitionDataRepo::AddData(AcquisitionDataIndex index, ProcessingStatus status) -> UniqueKey {
        if (d->IsExist(index) == false) {
            d->keyIndex++;
            d->dataList[d->keyIndex] = {index, status};
            return d->keyIndex;
        }

        return GetUniqueKey(index);
    }

    auto AcquisitionDataRepo::ChangeProcessingStatus(const AcquisitionDataIndex& index, ProcessingStatus status) -> UniqueKey {
        auto findResult = std::find_if(d->dataList.begin(), d->dataList.end(), [&](const Value& value) {
            return value.first == index;
        });

        if(findResult != d->dataList.end()) {
            findResult.value().second = status;
            return findResult.key();
        }

        return -1;
    }

    auto AcquisitionDataRepo::GetUniqueKey(const AcquisitionDataIndex& index) const -> UniqueKey {
        auto it = d->dataList.constBegin();
        while(it != d->dataList.constEnd()) {
            if(index == it.value().first) {
                return it.key();
            }
            ++it;
        }
        return -1;
    }

    auto AcquisitionDataRepo::DeleteData(const UniqueKey& key) -> void {
        d->dataList.remove(key);
    }

    auto AcquisitionDataRepo::IsValid(UniqueKey key) -> bool {
        return d->dataList.contains(key);
    }
}
