﻿#pragma once

#include <memory>

#include <QString>

#include "TableDefines.h"
#include "AcquisitionDataIndex.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class DataListTableConfig {
    public:
        using Self = DataListTableConfig;
        using Pointer = std::shared_ptr<Self>;

        DataListTableConfig();
        DataListTableConfig(const DataListTableConfig& other);
        ~DataListTableConfig();

        auto operator=(const DataListTableConfig& other) -> DataListTableConfig&;

        auto operator==(const DataListTableConfig& other) const -> bool;
        auto operator!=(const DataListTableConfig& other) const -> bool;
        auto operator<(const DataListTableConfig& other) const -> bool;

        auto SetChecked(bool select) -> void;
        auto IsChecked() const -> bool;

        auto SetSpecimen(const QString& specimen) -> void;
        auto GetSpecimen() const -> QString;

        auto SetWellPosition(const QString& wellPosition) -> void;
        auto GetWellPosition() const -> QString;

        auto SetProcessingStatus(ProcessingStatus status) -> void;
        auto GetProcessingStatus() const -> ProcessingStatus;

        auto SetImagingType(const QString& type) -> void;
        auto GetImagingType() const -> QString;

        auto SetAcquisitionID(const QString& id) -> void;
        auto GetAcquisitionID() const -> QString;

        auto SetTimestamp(const QString& timeString) -> void;
        auto GetTimestamp() const -> QString;

        auto SetDataKey(int32_t key) -> void;
        auto GetDataKey() const -> int32_t;

        auto SetDataPath(const QString& path) -> void;
        auto GetDataPath() const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
