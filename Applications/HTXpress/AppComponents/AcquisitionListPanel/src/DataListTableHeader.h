﻿#pragma once

#include <QHeaderView>

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class DataListTableHeader : public QHeaderView {
        Q_OBJECT
    public:
        using Self = DataListTableHeader;
        using Pointer = std::shared_ptr<Self>;

        explicit DataListTableHeader(QWidget* parent = nullptr);
        ~DataListTableHeader() override;

        auto paintSection(QPainter* painter, const QRect& rect, int logicalIndex) const -> void override;

    protected:
        auto mousePressEvent(QMouseEvent *e) -> void override;
        auto mouseReleaseEvent(QMouseEvent *e) -> void override;

    signals:
        void sigShowFilterMenu(int32_t section);
        void sigSortByColumn(int32_t section);

    public slots:
        void onClickedSection(int32_t section);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
