﻿#include "FilterMenuManager.h"
#include "DataListTableFilterMenu.h"
#include "DataListTable.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct FilterMenuManager::Impl {
        QMap<int32_t, DataListTableFilterMenu*> menus;
        DataListTable* table{nullptr};
    };

    FilterMenuManager::FilterMenuManager(QTableView* table, QObject* parent) : QObject(parent), d{std::make_unique<Impl>()} {
        d->table = qobject_cast<DataListTable*>(table);
    }

    FilterMenuManager::~FilterMenuManager() = default;

    auto FilterMenuManager::Clear() -> void {
        QMapIterator it(d->menus);
        while (it.hasNext()) {
            it.next().value()->ClearMenu();
        }
    }

    auto FilterMenuManager::AttachMenuToColumns(QList<int32_t> columns) -> void {
        for (const auto& column : columns) {
            d->menus[column] = new DataListTableFilterMenu(column, d->table);
            if (d->menus[column] != nullptr) {
                connect(d->menus[column], &DataListTableFilterMenu::sigUpdateUncheckedFilters, d->table, &DataListTable::onUpdateUncheckedFilters);
            }
        }
    }

    auto FilterMenuManager::AddMenu(int32_t column, const QString& title) -> void {
        d->menus[column]->AddMenu(title);
    }

    auto FilterMenuManager::SetMenus(int32_t column, const QStringList& titles) -> void {
        for (const auto& s : titles) {
            AddMenu(column, s);
        }
    }

    auto FilterMenuManager::DeleteMenu(int32_t column, const QString& title) -> void {
        d->menus[column]->DeleteMenu(title);
    }

    auto FilterMenuManager::ExecuteMenu(int32_t column, const QPoint& menuPos) -> void {
        d->menus[column]->exec(menuPos);
    }
}
