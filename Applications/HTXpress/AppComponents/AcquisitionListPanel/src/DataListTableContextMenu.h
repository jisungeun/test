﻿#pragma once

#include <memory>

#include <QMenu>

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class DataListTableContextMenu : public QMenu {
        Q_OBJECT
    public:
        using Self = DataListTableContextMenu;
        using Pointer = std::shared_ptr<Self>;

        explicit DataListTableContextMenu(QWidget* parent = nullptr);
        ~DataListTableContextMenu() override;

        auto SetDataPath(const QString& path) -> void;

    private slots:
        void onOpenFolder();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
