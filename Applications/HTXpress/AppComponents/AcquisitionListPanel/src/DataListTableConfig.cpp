﻿
#include "DataListTableConfig.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct DataListTableConfig::Impl {
        bool checked{};
        QString specimen{};
        QString wellPosition{};
        ProcessingStatus::_enumerated status;
        QString imagingType{};
        QString acquisitionID{};
        QString timestamp{};
        int32_t uniqueKey{};
        QString path{};

        auto operator==(const Impl& other) const -> bool;
        auto operator<(const Impl& other) const -> bool;
    };

    auto DataListTableConfig::Impl::operator==(const Impl& other) const -> bool {
        if(specimen != other.specimen) return false;
        if(wellPosition != other.wellPosition) return false;
        if(acquisitionID != other.acquisitionID) return false;
        if(timestamp != other.timestamp) return false;
        if(path != other.path) return false;

        return true;
    }

    auto DataListTableConfig::Impl::operator<(const Impl& other) const -> bool {
        if(specimen < other.specimen) return true;
        if(wellPosition < other.wellPosition) return true;
        if(acquisitionID < other.acquisitionID) return true;
        if(timestamp < other.timestamp) return true;
        if(path < other.path) return true;

        return false;
    }

    DataListTableConfig::DataListTableConfig() : d{std::make_unique<Impl>()} {
    }

    DataListTableConfig::DataListTableConfig(const DataListTableConfig& other): d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    DataListTableConfig::~DataListTableConfig() = default;

    auto DataListTableConfig::operator=(const DataListTableConfig& other) -> DataListTableConfig& {
        *d = *other.d;
        return *this;
    }

    auto DataListTableConfig::operator==(const DataListTableConfig& other) const -> bool {
        return *d == *other.d;

    }

    auto DataListTableConfig::operator!=(const DataListTableConfig& other) const -> bool {
        return !(*d == *other.d);
    }

    auto DataListTableConfig::operator<(const DataListTableConfig& other) const -> bool {
        return *d < *other.d;
    }

    auto DataListTableConfig::SetChecked(bool select) -> void {
        d->checked = select;
    }

    auto DataListTableConfig::IsChecked() const -> bool {
        return d->checked;
    }

    auto DataListTableConfig::SetSpecimen(const QString& specimen) -> void {
        d->specimen = specimen;
    }

    auto DataListTableConfig::GetSpecimen() const -> QString {
        return d->specimen;
    }

    auto DataListTableConfig::SetWellPosition(const QString& wellPosition) -> void {
        d->wellPosition = wellPosition;
    }

    auto DataListTableConfig::GetWellPosition() const -> QString {
        return d->wellPosition;
    }

    auto DataListTableConfig::SetProcessingStatus(ProcessingStatus status) -> void {
        d->status = status;
    }

    auto DataListTableConfig::GetProcessingStatus() const -> ProcessingStatus {
        return d->status;
    }

    auto DataListTableConfig::SetImagingType(const QString& type) -> void {
        d->imagingType = type;
    }

    auto DataListTableConfig::GetImagingType() const -> QString {
        return d->imagingType;
    }

    auto DataListTableConfig::SetAcquisitionID(const QString& id) -> void {
        d->acquisitionID = id;
    }

    auto DataListTableConfig::GetAcquisitionID() const -> QString {
        return d->acquisitionID;
    }

    auto DataListTableConfig::SetTimestamp(const QString& timeString) -> void {
        d->timestamp = timeString;
    }

    auto DataListTableConfig::GetTimestamp() const -> QString {
        return d->timestamp;
    }
    
    auto DataListTableConfig::SetDataKey(int32_t key) -> void {
        d->uniqueKey = key;
    }

    auto DataListTableConfig::GetDataKey() const -> int32_t {
        return d->uniqueKey;
    }

    auto DataListTableConfig::SetDataPath(const QString& path) -> void {
        d->path = path;
    }

    auto DataListTableConfig::GetDataPath() const -> QString {
        return d->path;
    }
}
