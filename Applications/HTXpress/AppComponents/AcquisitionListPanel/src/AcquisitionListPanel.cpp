﻿#include "AcquisitionListPanel.h"
#include "ui_AcquisitionListPanel.h"
#include "AcquisitionListPanelControl.h"
#include "DataListTable.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    using Request = DataListTable::Request;

    struct AcquisitionListPanel::Impl {
        Ui::AcquisitionListPanel ui;
        AcquisitionListPanelControl control;

        auto Connect(const Self* self) -> void;
    };

    AcquisitionListPanel::AcquisitionListPanel(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui.setupUi(this);
        d->Connect(this);

        d->ui.dataTable->SetDataRepo(d->control.GetDataRepo());
    }

    AcquisitionListPanel::~AcquisitionListPanel() = default;

    auto AcquisitionListPanel::ClearAll() -> void {
        d->control.ClearAll();
        d->ui.dataTable->UpdateRequest(Request::Clear);
    }

    auto AcquisitionListPanel::SetAcquisitionDataList(const QList<AcquisitionDataIndex>& list) -> void {
        d->control.SetAcquisitionDataList(list);
        d->ui.dataTable->UpdateRequest(Request::SetDataList);
    }

    auto AcquisitionListPanel::AddAcquisitionData(const AcquisitionDataIndex& index, ProcessingStatus status) -> void {
        const auto uniqueKey = d->control.AddAcquisitionData(index, status);
        if (uniqueKey != -1) {
            d->ui.dataTable->UpdateRequest(Request::AddData, uniqueKey);
        }
    }

    auto AcquisitionListPanel::SetDataProcessingStatus(const AcquisitionDataIndex& index, ProcessingStatus status) -> void {
        const auto uniqueKey = d->control.SetDataProcessingStatus(index, status);
        if (uniqueKey != -1) {
            d->ui.dataTable->UpdateRequest(Request::ChangeStatus, uniqueKey);
        }
    }

    auto AcquisitionListPanel::DeleteAcquisitionData(const AcquisitionDataIndex& index) -> void {
        const auto uniqueKey = d->control.GetUniqueKeyByIndex(index);
        if(uniqueKey != -1) {
            d->control.DeleteAcquisitionData(uniqueKey);
            d->ui.dataTable->UpdateRequest(Request::DeleteData, uniqueKey);
        }
    }

    auto AcquisitionListPanel::SetSelectionMode(const QAbstractItemView::SelectionMode& mode) const ->void {
        d->ui.dataTable->SetSelectionMode(mode);
    }

    auto AcquisitionListPanel::GetSelectedList() const ->QList<QString> {
        return d->ui.dataTable->GetSelectedList();
    }

    auto AcquisitionListPanel::SetSelectRow(const int& row) const ->void {
        d->ui.dataTable->SetSelectRow(row);
    }

    auto AcquisitionListPanel::SetCheckVisibility(bool visibility) -> void {
        d->ui.dataTable->SetCheckVisibility(visibility);
    }

    auto AcquisitionListPanel::GetCheckVisibility() const -> bool {
        return d->ui.dataTable->GetCheckVisibility();
    }

    auto AcquisitionListPanel::SetDefaultTableOrder() -> void {
        d->ui.dataTable->UpdateRequest(Request::DefaultTableOrder);
    }

    auto AcquisitionListPanel::Impl::Connect(const Self* self) -> void {
        connect(ui.dataTable, &DataListTable::sigSelectedDataIndex, self, &Self::sigSelectedDataIndex);
    }
}
