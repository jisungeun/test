﻿#pragma once

#include <QStyledItemDelegate>

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class ProcessingStatusDelegate : public QStyledItemDelegate {
        Q_OBJECT
    public:
        explicit ProcessingStatusDelegate(QObject* parent = nullptr);
        ~ProcessingStatusDelegate() override;

        auto paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const -> void override;
        
    protected:
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
