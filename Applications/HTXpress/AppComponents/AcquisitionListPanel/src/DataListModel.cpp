﻿#include <QList>
#include <QIcon>

#include "DataListModel.h"
#include "DataListTableConfig.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct DataListModel::Impl {
        QStringList headers;
        const QIcon unprocessedIcon{":/img/ic-check.svg"};
        const QIcon processedIcon{":/img/ic-check-s.svg"};
    };

    DataListModel::DataListModel(QObject* parent) : QStandardItemModel(parent), d{std::make_unique<Impl>()} {
        for (const auto col : Columns::_values()) {
            d->headers.push_back(col._to_string());
        }
        setHorizontalHeaderLabels(d->headers);
    }

    DataListModel::~DataListModel() = default;

    auto DataListModel::Clear() -> void {
        setRowCount(0);
        emit sigItemModelClear();
    }

    auto DataListModel::SetDataList(std::any dataList) -> void {
        auto configs = std::any_cast<QList<DataListTableConfig>>(dataList);
        QMap<int32_t, QList<QStandardItem*>> tableItems;

        for (const auto& config : configs) {
            AddData(config);
        }
    }

    auto DataListModel::AddData(std::any tableConfig) -> void {
        auto config = std::any_cast<DataListTableConfig>(tableConfig);

        QList<QStandardItem*> items;

        const auto checked = config.IsChecked();
        const auto specimen = config.GetSpecimen();
        const auto wellPos = config.GetWellPosition();
        const auto processingStatus = config.GetProcessingStatus();
        const auto imagingType = config.GetImagingType(); // get type using acqdataname;
        const auto acquisitionID = config.GetAcquisitionID(); // get id using acqdataname;
        const auto timestamp = config.GetTimestamp();
        const auto dataKey = config.GetDataKey();

        const auto keyItem = new QStandardItem;
        keyItem->setData(dataKey, Qt::EditRole);
        const auto chkItem = new QStandardItem;
        chkItem->setData(checked, Qt::EditRole);
        items << keyItem << chkItem;
        foreach(const QString& text,
                QStringList() << specimen << wellPos << QString::number(processingStatus) << imagingType << acquisitionID << timestamp) {
            items << new QStandardItem(text);
        }
        appendRow(items);
    }

    auto DataListModel::DeleteData(int32_t dataKey) -> void {
        for(int32_t r = 0; r < rowCount(); r++) {
            if(item(r, +Columns::Key)->data(Qt::EditRole) == dataKey) {
                removeRows(r, 1);
                break;
            }
        }
    }

    auto DataListModel::ChangeStatusData(std::any tableConfig) -> void {
        const auto config = std::any_cast<DataListTableConfig>(tableConfig);
        for (const auto& keyItem : findItems(QString::number(config.GetDataKey()), Qt::MatchExactly, Columns::Key)) {
            const auto statusItem = item(keyItem->row(), Columns::Status);
            statusItem->setData(QString::number(config.GetProcessingStatus()), Qt::EditRole);
        }
        emit dataChanged({}, {});
    }

    auto DataListModel::data(const QModelIndex& index, int role) const -> QVariant {
        if (!index.isValid()) return QVariant();

        if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }

        if (index.column() == Columns::Status) {
            if (role == Qt::DecorationRole) {
                const auto status = ProcessingStatus::_from_integral(index.siblingAtColumn(Columns::Status).data(Qt::EditRole).toInt());
                QPixmap pixmap(16, 16);
                switch (status) {
                    case ProcessingStatus::Unprocessed:
                    case ProcessingStatus::Invalid: {
                        return d->unprocessedIcon;
                    }
                    case ProcessingStatus::Processed: {
                        return d->processedIcon;
                    }
                }
            }

            if (role == Qt::DisplayRole) {
                return QVariant();
            }
        }

        return QStandardItemModel::data(index, role);
    }

    auto DataListModel::flags(const QModelIndex& index) const -> Qt::ItemFlags {
        Q_UNUSED(index);
        Qt::ItemFlags flags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;

        return flags;
    }

    auto DataListModel::headerData(int section, Qt::Orientation orientation, int role) const -> QVariant {
        if (orientation == Qt::Horizontal) {
            if (role == Qt::DisplayRole) {
                switch (Columns::_from_integral(section)) {
                case Columns::ID: return "ID";
                case Columns::Check: return "";
                case Columns::Key: return "Key";
                case Columns::Specimen: return "Specimen";
                case Columns::Status: return "";
                case Columns::Timestamp: return "T";
                case Columns::Type: return "Type";
                case Columns::Well: return "Well";
                }
            }
        }
        return QVariant();
    }

    auto DataListModel::sort(int column, Qt::SortOrder order) -> void {
        QStandardItemModel::sort(column, order);
    }
}
