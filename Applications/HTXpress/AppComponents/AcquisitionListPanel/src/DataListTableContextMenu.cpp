﻿#include <QAction>
#include <QDesktopServices>
#include <QUrl>
#include <QDir>

#include <MessageDialog.h>

#include "DataListTableContextMenu.h"

#include <QFileInfo>

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct DataListTableContextMenu::Impl {
        struct {
            QString dataPath{};
        } config;

        QAction* openFolder{nullptr};

        auto GetFolderFromTcfFilePath(const QString& tcfFullPath) -> QString;
    };

    auto DataListTableContextMenu::Impl::GetFolderFromTcfFilePath(const QString& tcfFullPath) -> QString {
        return QFileInfo(tcfFullPath).dir().absolutePath();
    }

    DataListTableContextMenu::DataListTableContextMenu(QWidget* parent) : QMenu(parent), d{std::make_unique<Impl>()} {
        d->openFolder = new QAction(this);
        d->openFolder->setText(tr("Show in Explorer"));

        addAction(d->openFolder);

        connect(d->openFolder, &QAction::triggered, this, &Self::onOpenFolder);
    }

    DataListTableContextMenu::~DataListTableContextMenu() {
    }

    auto DataListTableContextMenu::SetDataPath(const QString& path) -> void {
        d->config.dataPath = path; // tcf's full path
    }

    void DataListTableContextMenu::onOpenFolder() {
        const auto folderPath = d->GetFolderFromTcfFilePath(d->config.dataPath);
        const auto result = QDesktopServices::openUrl(QUrl::fromLocalFile(folderPath));

        if (!result) {
            TC::MessageDialog::warning(nullptr, tr("Failed to open"), tr("%1 is no exist.").arg(folderPath));
        }
    }
}
