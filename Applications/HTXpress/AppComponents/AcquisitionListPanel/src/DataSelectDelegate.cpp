﻿#include <QApplication>
#include <QPainter>
#include <QMouseEvent>

#include "DataSelectDelegate.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct DataSelectDelegate::Impl {
        const QColor oddRowColor{"#1B2629"};
        const QColor evenRowColor{"#243135"};

        auto GetCheckboxRect(const QStyleOptionViewItem& option) const -> QRect;
    };

    auto DataSelectDelegate::Impl::GetCheckboxRect(const QStyleOptionViewItem& option) const -> QRect {
        QStyleOptionButton opt;
        opt.QStyleOption::operator=(option);
        const QRect chkRect = QApplication::style()->subElementRect(QStyle::SE_ItemViewItemCheckIndicator, &opt);

        QRect rect = option.rect;
        const int32_t dx = (rect.width() - chkRect.width()) / 2;
        const int32_t dy = (rect.height() - chkRect.height()) / 2;

        rect.setTopLeft(rect.topLeft() + QPoint(dx, dy));
        rect.setWidth(chkRect.width());
        rect.setHeight(chkRect.height());

        return rect;
    }

    DataSelectDelegate::DataSelectDelegate(QObject* parent) : QItemDelegate(parent), d{std::make_unique<Impl>()} {

    }

    DataSelectDelegate::~DataSelectDelegate() {
    }

    void DataSelectDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const {
        if (index.row() % 2 == 0) {
            painter->fillRect(option.rect, d->evenRowColor);
        }
        else {
            painter->fillRect(option.rect, d->oddRowColor);
        }

        auto chkOption = option;
        chkOption.rect = d->GetCheckboxRect(option);

        if(chkOption.state & QStyle::State_Selected) {
            drawCheck(painter, chkOption, chkOption.rect, Qt::Checked);
        }
        else {
            drawCheck(painter, chkOption, chkOption.rect, Qt::Unchecked);
        }
    }

    bool DataSelectDelegate::editorEvent(QEvent* event, QAbstractItemModel* model, const QStyleOptionViewItem& option, const QModelIndex& index) {
        if (event->type() == QEvent::MouseButtonPress) {
            const auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
            if (mouseEvent->button() == Qt::LeftButton) {
                const auto cbRect = d->GetCheckboxRect(option);
                if (cbRect.contains(mouseEvent->pos())) {
                    emit sigSelectionChanged(index.row());
                }
            }
        }

        return QItemDelegate::editorEvent(event, model, option, index);
    }
}
