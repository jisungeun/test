﻿#pragma once

#include <memory>

#include <QMenu>
#include <QWidgetAction>

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class DataListTableFilterMenu : public QMenu {
        Q_OBJECT
    public:
        using Self = DataListTableFilterMenu;
        using Pointer = std::shared_ptr<Self>;

        explicit DataListTableFilterMenu(int32_t column, QWidget* parent = nullptr);
        ~DataListTableFilterMenu() override;

        auto AddMenu(const QString& name, bool status = true) -> void;
        auto DeleteMenu(const QString& name) -> void;
        auto ClearMenu() -> void;

        auto setVisible(bool visible) -> void override;

    protected:
        void mousePressEvent(QMouseEvent* event) override;

    signals:
        void sigUpdateUncheckedFilters(int32_t section, const QStringList& filters);

    private slots:
        void onTriggered(QAction* action);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
