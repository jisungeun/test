﻿#include <QList>
#include <QMap>

#include "AcquisitionListPanelControl.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct AcquisitionListPanelControl::Impl {
        AcquisitionDataRepo::Pointer dataRepo{nullptr};
    };

    AcquisitionListPanelControl::AcquisitionListPanelControl() : d{std::make_unique<Impl>()} {
        d->dataRepo = std::make_shared<AcquisitionDataRepo>();
    }

    AcquisitionListPanelControl::~AcquisitionListPanelControl() = default;

    auto AcquisitionListPanelControl::SetAcquisitionDataList(const QList<AcquisitionDataIndex>& list) -> void {
        d->dataRepo->ClearDataList();

        for (const auto& a : list) {
            d->dataRepo->AddData(a, ProcessingStatus::Unprocessed);
        }
    }

    auto AcquisitionListPanelControl::AddAcquisitionData(const AcquisitionDataIndex& index, ProcessingStatus status) -> AcquisitionDataRepo::UniqueKey {
        // check valid/invalid here
        return d->dataRepo->AddData(index, status);
    }

    auto AcquisitionListPanelControl::SetDataProcessingStatus(const AcquisitionDataIndex& index, ProcessingStatus status) -> AcquisitionDataRepo::UniqueKey {
        // check valid/invalid here
        return d->dataRepo->ChangeProcessingStatus(index, status);
    }

    auto AcquisitionListPanelControl::GetUniqueKeyByIndex(const AcquisitionDataIndex& index) const -> AcquisitionDataRepo::UniqueKey {
        return d->dataRepo->GetUniqueKey(index);
    }

    auto AcquisitionListPanelControl::DeleteAcquisitionData(const AcquisitionDataRepo::UniqueKey& key) -> void {
        d->dataRepo->DeleteData(key);
    }

    auto AcquisitionListPanelControl::ClearAll() -> void {
        d->dataRepo->ClearDataList();
    }

    auto AcquisitionListPanelControl::GetDataRepo() const -> const AcquisitionDataRepo::Pointer& {
        return d->dataRepo;
    }
}
