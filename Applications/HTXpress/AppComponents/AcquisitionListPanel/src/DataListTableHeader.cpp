﻿#include <QPainter>
#include <QComboBox>
#include <QMouseEvent>
#include <QApplication>

#include "DataListTableHeader.h"
#include "TableDefines.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct DataListTableHeader::Impl {
        bool leftButtonPressed{false};
        QMap<int32_t, bool> pressedIndices;
        QPoint pressedPos{};
        const QPixmap dropdownPixmap{":/img/ic-drop-down.svg"};
        const int32_t filterButtonBaseSize{16};
    };

    DataListTableHeader::DataListTableHeader(QWidget* parent) : QHeaderView(Qt::Horizontal, parent), d{std::make_unique<Impl>()} {
        setSectionsClickable(true);
        setSectionsMovable(false);
        setSortIndicatorShown(false);
        connect(this, &QHeaderView::sectionClicked, this, &DataListTableHeader::onClickedSection);

        setMinimumSectionSize(20);
    }

    DataListTableHeader::~DataListTableHeader() = default;

    auto DataListTableHeader::paintSection(QPainter* painter, const QRect& rect, int logicalIndex) const -> void {
        painter->save();
        QHeaderView::paintSection(painter, rect, logicalIndex);
        painter->restore();
        
        if (logicalIndex == Columns::Specimen || logicalIndex == Columns::Well || logicalIndex == Columns::Type) {
            QStyleOptionSpinBox option;
            option.rect = QRect(rect.right() - d->filterButtonBaseSize, 
                                rect.center().y() - d->filterButtonBaseSize/2, 
                                d->filterButtonBaseSize, 
                                d->filterButtonBaseSize);
            if (d->leftButtonPressed) {
                d->pressedIndices[logicalIndex] = option.rect.contains(d->pressedPos);
            }

            style()->drawItemPixmap(painter, option.rect, Qt::AlignCenter, d->dropdownPixmap);
        }
    }

    auto DataListTableHeader::mousePressEvent(QMouseEvent* e) -> void {
        if (e->button() == Qt::LeftButton) {
            d->pressedPos = e->pos();
            d->leftButtonPressed = true;
        }
        QHeaderView::mousePressEvent(e);
    }

    auto DataListTableHeader::mouseReleaseEvent(QMouseEvent* e) -> void {
        d->leftButtonPressed = false;
        QHeaderView::mouseReleaseEvent(e);
    }

    auto DataListTableHeader::onClickedSection(int32_t section) -> void {
        if (d->pressedIndices[section]) {
            emit sigShowFilterMenu(section);
        }
        else {
            // TODO 정렬 기능을 헤더 선택을 통해 한다면 주석을 풀고 proxy model sort 재정의
            emit sigSortByColumn(section);
        }
    }
}
