﻿#include <QMouseEvent>
#include <QDebug>

#include "DataListTable.h"
#include "DataListModel.h"
#include "DataListTableControl.h"
#include "DataListProxyModel.h"
#include "DataListTableHeader.h"
#include "DataListTableContextMenu.h"
#include "FilterMenuManager.h"
#include "DataSelectDelegate.h"
#include "ProcessingStatusDelegate.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct DataListTable::Impl {
        Self* self{};
        DataListTableControl control;
        DataListModel* model{};
        DataListProxyModel* proxyModel{};
        DataListTableHeader* tableHeader{};
        FilterMenuManager* filterMenuManager{};

        auto InitModels() -> void;
        auto InitTableProperty() -> void;
        auto InitTableHeader() -> void;
        auto InitMenus() -> void;
        auto Connect() -> void;
        auto SetMenus() -> void;

        auto DoClearAll() -> void;
        auto DoChangeStatus(int32_t key) -> void;
        auto DoSetDataList() -> void;
        auto DoAddData(int32_t key) -> void;
        auto DoDeleteData(int32_t key) -> void;
        auto DoSetDeafultTableOrder() -> void;

        auto GetTooltip(int32_t key, int32_t column) -> QPair<QString, int32_t>;
        auto UpdateColumnSize() -> void;
        auto IsExistKey(const int32_t& key) -> bool;

        bool isCheckVisible = false;
    };

    DataListTable::DataListTable(QWidget* parent) : QTableView(parent), d{std::make_unique<Impl>()} {
        d->self = this;

        d->InitTableProperty();
        d->InitModels();
        d->InitTableHeader();
        d->InitMenus();
        onHideColumnHeader();
        d->Connect();
        d->UpdateColumnSize();
    }

    DataListTable::~DataListTable() = default;

    auto DataListTable::SetDataRepo(const AcquisitionDataRepo::Pointer& dataRepo) -> void {
        d->control.SetDataRepo(dataRepo);
    }

    auto DataListTable::UpdateRequest(Request type, std::any value) -> void {
        switch (type) {
            case Request::AddData: {
                const auto key = std::any_cast<int32_t>(value);
                if(!d->IsExistKey(key)) {
                    d->DoAddData(key);
                    d->UpdateColumnSize();
                }
                break;
            }
            case Request::Clear: {
                d->DoClearAll();
                d->UpdateColumnSize();
                break;
            }
            case Request::ChangeStatus: {
                const auto key = std::any_cast<int32_t>(value);
                if(d->IsExistKey(key)) {
                    d->DoChangeStatus(key);
                    this->viewport()->update();
                }
                break;
            }
            case Request::SetDataList: {
                d->DoClearAll();
                d->DoSetDataList();
                d->UpdateColumnSize();
                break;
            }
            case Request::DeleteData: {
                const auto key = std::any_cast<int32_t>(value);
                if(d->IsExistKey(key)) {
                    d->DoDeleteData(key);
                    d->UpdateColumnSize();
                }
                break;
            }
            case Request::DefaultTableOrder: {
                d->DoSetDeafultTableOrder();
                break;
            }
        }
    }

    auto DataListTable::SetSelectionMode(const SelectionMode& mode)->void {
        setSelectionMode(mode);
    }

    auto DataListTable::GetSelectedList() const ->QList<QString> {
        QList<QString> list;

        const auto model = selectionModel();
        auto rows = model->selectedRows();
        for(const auto& index : rows) {
            if (index.isValid()) {
                const auto key = index.siblingAtColumn(Columns::Key).data().toInt();
                list.append(d->control.GetDataRepoValue(key).first.GetAcquisitionDataPath());
            }
        }

        return list;
    }

    auto DataListTable::SetSelectRow(const int& row)->void {
        const auto model = this->model();
        if (nullptr == model) return;
        this->selectRow(row);
        this->doubleClicked(model->index(row, 0));
    }

    auto DataListTable::SetCheckVisibility(bool visibility) -> void {
        d->isCheckVisible = visibility;
        onHideColumnHeader();
    }

    auto DataListTable::GetCheckVisibility() const -> bool {
        return d->isCheckVisible;
    }

    auto DataListTable::mousePressEvent(QMouseEvent* event) -> void {
        if (!indexAt(event->pos()).isValid()) {
            selectionModel()->clear();
        }

        QTableView::mousePressEvent(event);
    }

    void DataListTable::mouseMoveEvent(QMouseEvent* event) {
        if(indexAt(event->pos()).isValid()) {
            const auto modelIndex = indexAt(event->pos());
            const auto column = Columns::_from_integral(modelIndex.column());
            bool ok{};
            const auto key = modelIndex.siblingAtColumn(Columns::Key).data().toInt(&ok);
            if(ok) {
                const auto tooltip = d->GetTooltip(key, column);
                setToolTip(tooltip.first);
                setToolTipDuration(tooltip.second);
            }
        }

        QTableView::mouseMoveEvent(event);
    }

    void DataListTable::contextMenuEvent(QContextMenuEvent* event) {
        if(indexAt(event->pos()).isValid()) {
            const auto modelIndex = indexAt(event->pos());
            bool ok{};
            const auto key = modelIndex.siblingAtColumn(Columns::Key).data().toInt(&ok);
            if(ok) {
                const auto contextMenu = std::make_unique<DataListTableContextMenu>(this);
                const auto path = d->control.GetDataRepoValue(key).first.GetAcquisitionDataPath();
                contextMenu->SetDataPath(path);
                contextMenu->move(QCursor::pos());
                contextMenu->exec();
            }
        }
        QTableView::contextMenuEvent(event);
    }

    void DataListTable::onShowFilterMenu(int32_t section) {
        const auto menuPos = QCursor::pos();
        d->filterMenuManager->ExecuteMenu(section, menuPos);
    }

    void DataListTable::onUpdateUncheckedFilters(int32_t section, const QStringList filters) {
        d->proxyModel->ClearFilters(section);
        if (section == Columns::Specimen) {
            d->proxyModel->SetSpecimenList(filters);
        }
        else if (section == Columns::Well) {
            d->proxyModel->SetWellList(filters);
        }
        else if (section == Columns::Type) {
            d->proxyModel->SetTypeList(filters);
        }
        // filtered row count: d->proxyModel->rowCount();
        // total row count: d->model->rowCount();
    }

    void DataListTable::onSort(int32_t section) {
        d->proxyModel->sort(section, {});
    }

    void DataListTable::onSelectionChanged(int32_t row) {
        Q_UNUSED(row)
    }

    void DataListTable::onMouseDoubleClicked(const QModelIndex& index) {
        if (index.isValid()) {
            const auto key = index.siblingAtColumn(Columns::Key).data().toInt();
            emit sigSelectedDataIndex(d->control.GetDataRepoValue(key).first);
        }
    }

    void DataListTable::onHideColumnHeader() {
        setColumnHidden(Columns::Key, true);
        setColumnHidden(Columns::Timestamp, true);
        setColumnHidden(Columns::Check, !d->isCheckVisible);
    }

    auto DataListTable::Impl::InitModels() -> void {
        model = new DataListModel(self);

        proxyModel = new DataListProxyModel(self);
        proxyModel->setSourceModel(model);

        self->setModel(proxyModel);
    }

    auto DataListTable::Impl::InitTableProperty() -> void {
        self->setSortingEnabled(false);
        self->setTextElideMode(Qt::ElideMiddle);
        self->setSelectionBehavior(SelectRows);
        self->setSelectionMode(SingleSelection);
        self->setMouseTracking(true);
        self->setItemDelegateForColumn(Columns::Status, new ProcessingStatusDelegate(self));

        const auto selectDeleagate = new DataSelectDelegate(self);
        self->setItemDelegateForColumn(Columns::Check, selectDeleagate);
        connect(selectDeleagate, &DataSelectDelegate::sigSelectionChanged, self, &Self::onSelectionChanged);
    }

    auto DataListTable::Impl::InitTableHeader() -> void {
        self->verticalHeader()->hide();

        tableHeader = new DataListTableHeader(self);
        self->setHorizontalHeader(tableHeader);
    }

    auto DataListTable::Impl::InitMenus() -> void {
        filterMenuManager = new FilterMenuManager(self);
        filterMenuManager->AttachMenuToColumns(QList<int32_t>() << Columns::Specimen << Columns::Well << Columns::Type);
    }

    auto DataListTable::Impl::Connect() -> void {
        connect(model, &DataListModel::sigItemModelClear, self, &Self::onHideColumnHeader);
        connect(self, &Self::doubleClicked, self, &Self::onMouseDoubleClicked);

        connect(tableHeader, &DataListTableHeader::sigShowFilterMenu, self, &Self::onShowFilterMenu);
        connect(tableHeader, &DataListTableHeader::sigSortByColumn, self, &Self::onSort);
    }

    auto DataListTable::Impl::SetMenus() -> void {
        filterMenuManager->SetMenus(Columns::Specimen, control.GetMenuTitles(Columns::Specimen));
        filterMenuManager->SetMenus(Columns::Well, control.GetMenuTitles(Columns::Well));
        filterMenuManager->SetMenus(Columns::Type, control.GetMenuTitles(Columns::Type));
    }

    auto DataListTable::Impl::DoClearAll() -> void {
        control.Clear();
        model->Clear();
        proxyModel->ClearFilters();
        filterMenuManager->Clear();
    }

    auto DataListTable::Impl::DoChangeStatus(int32_t key) -> void {
        control.ChangeProcessingStatus(key);
        model->ChangeStatusData(control.GetTableConfig(key));
    }

    auto DataListTable::Impl::DoSetDataList() -> void {
        control.CreateTableConfigs();
        model->SetDataList(control.GetTableConfigs());
        SetMenus();
    }

    auto DataListTable::Impl::DoAddData(int32_t key) -> void {
        control.AddTableConfig(key);

        const auto conf = control.GetTableConfig(key);
        model->AddData(conf);

        filterMenuManager->AddMenu(Columns::Specimen, conf.GetSpecimen());
        filterMenuManager->AddMenu(Columns::Well, conf.GetWellPosition());
        filterMenuManager->AddMenu(Columns::Type, conf.GetImagingType());
    }

    auto DataListTable::Impl::DoDeleteData(int32_t key) -> void {
        const auto conf = control.GetTableConfig(key);

        // ex) specimen1이 테이블에 여러 개 -> specimen1 삭제 요청 -> 헤더 칼럼 메뉴에선 유지
        // ex) specimen1이 테이블에 1개 -> specimen1 삭제 요청 -> 메뉴에서도 삭제
        if(control.IsDeleteTarget(DataListTableControl::ConfigType::Specimen, conf.GetSpecimen())) 
            filterMenuManager->DeleteMenu(Columns::Specimen, conf.GetSpecimen());
        if(control.IsDeleteTarget(DataListTableControl::ConfigType::Well, conf.GetWellPosition())) 
            filterMenuManager->DeleteMenu(Columns::Well, conf.GetWellPosition());
        if(control.IsDeleteTarget(DataListTableControl::ConfigType::ImagingType, conf.GetImagingType())) 
            filterMenuManager->DeleteMenu(Columns::Type, conf.GetImagingType());

        model->DeleteData(key);

        control.DeleteTableConfig(key);
    }

    auto DataListTable::Impl::DoSetDeafultTableOrder() -> void {
        self->onSort(Columns::Timestamp);
    }

    auto DataListTable::Impl::GetTooltip(int32_t key, int32_t column) -> QPair<QString, int32_t> {
        using Message = QString;
        using DurationMSec = int32_t;

        QPair<Message, DurationMSec> tooltip{};

        switch (column) {
            case Columns::ID: 
                tooltip.first = "Creation Date: " + control.GetDataRepoValue(key).first.GetTimestamp();
                tooltip.second = 3000;
                break;
            case Columns::Specimen: 
                tooltip.first = control.GetDataRepoValue(key).first.GetGroupName();
                tooltip.second = 2000;
                break;
            default: 
                break;
        }

        return tooltip;
    }

    auto DataListTable::Impl::UpdateColumnSize() -> void {
        // TODO 숫자 빼자
        tableHeader->resizeSection(Columns::Specimen, QHeaderView::Interactive);
        const auto groupColumnWidth = self->columnWidth(Columns::Specimen);
        if(groupColumnWidth < 99) self->setColumnWidth(Columns::Specimen, 99);
        else if(groupColumnWidth > 116) self->setColumnWidth(Columns::Specimen, 116);

        self->setColumnWidth(Columns::Check, 25);
        self->setColumnWidth(Columns::Specimen, 99);
        self->setColumnWidth(Columns::Well, 60);
        self->setColumnWidth(Columns::Status, 20);
        self->setColumnWidth(Columns::Type, 60);
        self->setColumnWidth(Columns::ID, 40);

        tableHeader->setSectionResizeMode(Columns::Check, QHeaderView::Fixed);
        tableHeader->setSectionResizeMode(Columns::Type, QHeaderView::Fixed);
        tableHeader->setSectionResizeMode(Columns::ID, QHeaderView::Fixed);
        tableHeader->setSectionResizeMode(Columns::Status, QHeaderView::Fixed);
        tableHeader->setSectionResizeMode(Columns::Well, QHeaderView::Fixed);
        tableHeader->setSectionResizeMode(Columns::Specimen, QHeaderView::Stretch);
    }

    auto DataListTable::Impl::IsExistKey(const int32_t& key) -> bool {
        const auto exist = std::any_of(control.GetTableConfigs().cbegin(), 
                                       control.GetTableConfigs().cend(), 
                                       [key](const DataListTableConfig& c) {
            return c.GetDataKey() == key;
        });

        return exist;
    }
}
