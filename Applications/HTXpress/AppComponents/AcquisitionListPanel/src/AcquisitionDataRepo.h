﻿#pragma once

#include <memory>

#include "AcquisitionDataIndex.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class AcquisitionDataRepo {
    public:
        using Self = AcquisitionDataRepo;
        using Pointer = std::shared_ptr<Self>;
        using Value = QPair<AcquisitionDataIndex, ProcessingStatus::_enumerated>;
        using UniqueKey = int32_t;
        using DataContainer = QMap<UniqueKey, Value>;

        AcquisitionDataRepo();
        ~AcquisitionDataRepo();

        auto ClearDataList() -> void;

        auto AddData(AcquisitionDataIndex index, ProcessingStatus status) -> UniqueKey;
        auto ChangeProcessingStatus(const AcquisitionDataIndex& index, ProcessingStatus status) -> UniqueKey;

        auto GetUniqueKey(const AcquisitionDataIndex& index) const -> UniqueKey;
        auto DeleteData(const UniqueKey& key) -> void;

        auto IsValid(UniqueKey key) -> bool;
        auto GetData(UniqueKey key) -> Value&;
        auto GetDataList() const -> DataContainer&;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
