﻿#include "DataListProxyModel.h"
#include "TableDefines.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct DataListProxyModel::Impl {
        QStringList specimens;
        QStringList wells;
        QStringList types;

        QMap<Columns, CustomOrder> tableOrders{
            {Columns::Specimen, CustomOrder::Ascending},
            {Columns::Well, CustomOrder::Ascending}
        };

        auto ChangeOrder(CustomOrder& currentOrder) -> void;
    };

    auto DataListProxyModel::Impl::ChangeOrder(CustomOrder& currentOrder) -> void {
        if (currentOrder == CustomOrder::Ascending) {
            currentOrder = CustomOrder::Descending;
        }
        else if (currentOrder == CustomOrder::Descending) {
            currentOrder = CustomOrder::FirstInput;
        }
        else if (currentOrder == CustomOrder::FirstInput) {
            currentOrder = /*LastInput*/CustomOrder::Ascending;
        }
    }

    DataListProxyModel::DataListProxyModel(QObject* parent) : QSortFilterProxyModel(parent), d{std::make_unique<Impl>()} {
    }

    DataListProxyModel::~DataListProxyModel() = default;

    auto DataListProxyModel::ClearFilters(int32_t section) -> void {
        if (section == Columns::Specimen) {
            d->specimens.clear();

        }
        else if (section == Columns::Well) {
            d->wells.clear();

        }
        else if (section == Columns::Type) {
            d->types.clear();

        }
        else {
            d->specimens.clear();
            d->wells.clear();
            d->types.clear();
        }

        invalidateFilter();
    }

    auto DataListProxyModel::SetSpecimenList(const QStringList& list) -> void {
        d->specimens = list;
        invalidateFilter();
    }

    auto DataListProxyModel::SetWellList(const QStringList& list) -> void {
        d->wells = list;
        invalidateFilter();
    }

    auto DataListProxyModel::SetTypeList(const QStringList& list) -> void {
        d->types = list;
        invalidateFilter();
    }

    auto DataListProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const -> bool {
        const auto indexS = sourceModel()->index(sourceRow, Columns::Specimen, sourceParent);
        const auto indexW = sourceModel()->index(sourceRow, Columns::Well, sourceParent);
        const auto indexT = sourceModel()->index(sourceRow, Columns::Type, sourceParent);

        return !(d->specimens.contains(indexS.data().toString()) || d->wells.contains(indexW.data().toString()) || d->types.contains(indexT.data().toString()));
    }

    auto DataListProxyModel::sort(int column, Qt::SortOrder order) -> void {
        Q_UNUSED(order)
        const auto col = Columns::_from_integral(column);

        if (col == +Columns::Specimen || col == +Columns::Well) {
            switch(d->tableOrders[col]) {
                case CustomOrder::Ascending:
                    QSortFilterProxyModel::sort(col, Qt::AscendingOrder);
                    break;
                case CustomOrder::Descending:
                    QSortFilterProxyModel::sort(col, Qt::DescendingOrder);
                    break;
                case CustomOrder::FirstInput:
                    QSortFilterProxyModel::sort(Columns::Timestamp, Qt::AscendingOrder);
                    break;
            }
            d->ChangeOrder(d->tableOrders[col]);
        }
        else if(col == +Columns::Timestamp) {
            QSortFilterProxyModel::sort(Columns::Timestamp, Qt::AscendingOrder);
        }
    }

    auto DataListProxyModel::lessThan(const QModelIndex& sourceLeft, const QModelIndex& sourceRight) const -> bool {
        // 참고: https://doc.qt.io/qt-5/qsortfilterproxymodel.html#details
        const auto left = sourceModel()->data(sourceLeft).toString();
        const auto right = sourceModel()->data(sourceRight).toString();

        const auto col = Columns::_from_integral(sourceLeft.column());

        switch (col) {
            case Columns::Specimen:
                if(left.length() < right.length()) return true;
                if(left.length() > right.length()) return false;
                break;
            case Columns::Timestamp: {
                if (left != right) break;
                const auto leftWellPosName = sourceLeft.siblingAtColumn(Columns::Well).data().toString();
                const auto rightWellPosName = sourceRight.siblingAtColumn(Columns::Well).data().toString();
                if (leftWellPosName == rightWellPosName) {
                    const auto leftPointID = sourceLeft.siblingAtColumn(Columns::ID).data().toString();
                    const auto rightPointID = sourceRight.siblingAtColumn(Columns::ID).data().toString();
                    if(leftPointID.length() == rightPointID.length()) {
                        return QString::localeAwareCompare(leftPointID, rightPointID) < 0;
                    }
                    return leftPointID.length() < rightPointID.length();
                }
                if(leftWellPosName.length() == rightWellPosName.length()) {
                    return QString::localeAwareCompare(leftWellPosName, rightWellPosName) < 0;
                }
                return leftWellPosName.length() < rightWellPosName.length();
            }
            default: break;
        }
        
        return QString::localeAwareCompare(left, right) < 0;
    }
}
