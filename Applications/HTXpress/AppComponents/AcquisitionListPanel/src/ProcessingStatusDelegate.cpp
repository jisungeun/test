﻿#include <QPainter>

#include "ProcessingStatusDelegate.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct ProcessingStatusDelegate::Impl {
        const QColor oddRowColor{"#1B2629"};
        const QColor evenRowColor{"#243135"};
        const QColor selectedColor{"#27A2BF"};
    };

    ProcessingStatusDelegate::ProcessingStatusDelegate(QObject* parent) : QStyledItemDelegate(parent), d{std::make_unique<Impl>()} {
    }

    ProcessingStatusDelegate::~ProcessingStatusDelegate() = default;

    auto ProcessingStatusDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const -> void {
        auto opt = option;

        if(opt.state & QStyle::State_Selected) {
            painter->fillRect(option.rect, d->selectedColor);
        }
        else {
            if (index.row() % 2 == 0) painter->fillRect(option.rect, d->evenRowColor);
            else painter->fillRect(option.rect, d->oddRowColor);
        }

        const auto icon = qvariant_cast<QIcon>(index.data(Qt::DecorationRole));
        icon.paint(painter, option.rect);
    }
}
