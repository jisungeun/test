﻿#pragma once

#include <QItemDelegate>

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class DataSelectDelegate : public QItemDelegate {
        Q_OBJECT
    public:
        explicit DataSelectDelegate(QObject* parent = nullptr);
        ~DataSelectDelegate() override;

        void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
        bool editorEvent(QEvent* event, QAbstractItemModel* model, const QStyleOptionViewItem& option, const QModelIndex& index) override;

    signals:
        void sigSelectionChanged(int32_t row);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

    };
}
