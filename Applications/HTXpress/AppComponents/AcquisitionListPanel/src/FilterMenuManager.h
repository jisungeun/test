﻿#pragma once

#include <QObject>

class QTableView;

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class FilterMenuManager : public QObject {
    public:
        using Self = FilterMenuManager;
        using Pointer = std::shared_ptr<Self>;

        explicit FilterMenuManager(QTableView *table, QObject* parent = nullptr);
        ~FilterMenuManager() override;

        auto Clear() -> void;
        auto AttachMenuToColumns(QList<int32_t> columns) -> void;
        auto AddMenu(int32_t column, const QString& title) -> void;
        auto SetMenus(int32_t column, const QStringList& titles) -> void;
        auto DeleteMenu(int32_t column, const QString& title) -> void;
        auto ExecuteMenu(int32_t column, const QPoint& menuPos) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
