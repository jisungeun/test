﻿#include "AcquisitionDataIndex.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct AcquisitionDataIndex::Impl {
        QString groupName;
        QString wellName;
        QString acquisitionDataName;
        QString acquisitionDataPath;
        QString timestamp;

        auto operator==(const Impl& other) const -> bool;
        auto operator<(const Impl& other) const -> bool;

        auto ConvertFullNameToShortDataName(const QString& txt) const -> QString;
    };

    AcquisitionDataIndex::AcquisitionDataIndex() : d{std::make_unique<Impl>()} {
    }

    AcquisitionDataIndex::AcquisitionDataIndex(const QString& group, const QString& well, const QString& acquisitionData, const QString& timestamp) : d{std::make_unique<Impl>()} {
        d->groupName = group;
        d->wellName = well;
        d->acquisitionDataName = acquisitionData;
        d->timestamp = timestamp;
    }

    AcquisitionDataIndex::AcquisitionDataIndex(const AcquisitionDataIndex& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    AcquisitionDataIndex::~AcquisitionDataIndex() = default;

    auto AcquisitionDataIndex::operator=(const AcquisitionDataIndex& other) -> AcquisitionDataIndex& {
        *d = *other.d;
        return *this;
    }

    auto AcquisitionDataIndex::operator==(const AcquisitionDataIndex& other) const -> bool {
        return *d == *other.d;
    }

    auto AcquisitionDataIndex::operator!=(const AcquisitionDataIndex& other) const -> bool {
        return !(*d == *other.d);
    }

    auto AcquisitionDataIndex::operator<(const AcquisitionDataIndex& other) const -> bool {
        return *d < *other.d;
    }

    auto AcquisitionDataIndex::SetGroupName(const QString& groupName) -> void {
        d->groupName = groupName;
    }

    auto AcquisitionDataIndex::GetGroupName() const -> QString {
        return d->groupName;
    }

    auto AcquisitionDataIndex::SetWellPositionName(const QString& positionName) -> void {
        d->wellName = positionName;
    }

    auto AcquisitionDataIndex::GetWellPositionName() const -> QString {
        return d->wellName;
    }

    auto AcquisitionDataIndex::SetAcquisitionDataName(const QString& dataFullName) -> void {
        // dataFullName ex) 220729.095552.new2.001.Group1.A3.T001P01
        // dataShortName == lastString of dataFullName, ex) T001P01
        const auto dataName = d->ConvertFullNameToShortDataName(dataFullName);
        d->acquisitionDataName = dataName;
    }

    auto AcquisitionDataIndex::GetAcquisitionDataName() const -> QString {
        return d->acquisitionDataName;
    }

    auto AcquisitionDataIndex::SetAcquisitionDataPath(const QString& dataPath) const -> void {
        // dataPath == tcf file's full path
        // ex) path = E:/Temp/HTXpressTest/Service/asdf/Exp3/220729.094807.Exp3.001.Group1.A1.S001/220729.094807.Exp3.001.Group1.A1.S001.TCF
        d->acquisitionDataPath = dataPath;
    }

    auto AcquisitionDataIndex::GetAcquisitionDataPath() const ->QString {
        return d->acquisitionDataPath;
    }

    auto AcquisitionDataIndex::SetTimestamp(const QString& timestamp) -> void {
        d->timestamp = timestamp;
    }

    auto AcquisitionDataIndex::GetTimestamp() const -> QString {
        return d->timestamp;
    }

    auto AcquisitionDataIndex::Impl::operator==(const Impl& other) const -> bool {
        if (groupName != other.groupName) return false;
        if (wellName != other.wellName) return false;
        if (acquisitionDataName != other.acquisitionDataName) return false;
        if (timestamp != other.timestamp) return false;
        return true;
    }

    auto AcquisitionDataIndex::Impl::operator<(const Impl& other) const -> bool {
        if (groupName.length() < other.groupName.length()) return true;
        if (groupName.length() > other.groupName.length()) return false;
        if (groupName < other.groupName) return true;
        if (groupName > other.groupName) return false;
        if (wellName.length() < other.wellName.length()) return true;
        if (wellName.length() > other.wellName.length()) return false;
        if (wellName < other.wellName) return true;
        if (wellName > other.wellName) return false;
        if (acquisitionDataName.length() < other.acquisitionDataName.length()) return true;
        if (acquisitionDataName.length() > other.acquisitionDataName.length()) return false;
        if (acquisitionDataName < other.acquisitionDataName) return true;
        if (acquisitionDataName > other.acquisitionDataName) return false;
        if (timestamp.length() < other.timestamp.length()) return true;
        if (timestamp.length() > other.timestamp.length()) return false;
        if (timestamp < other.timestamp) return true;
        if (timestamp > other.timestamp) return false;

        return false;
    }

    auto AcquisitionDataIndex::Impl::ConvertFullNameToShortDataName(const QString& txt) const -> QString {
        // 폴더명 마지막 .(dot) 다음이 acquisitoinDataName index를 가리킨다.
        return txt.section(".", -1);
    }
}
