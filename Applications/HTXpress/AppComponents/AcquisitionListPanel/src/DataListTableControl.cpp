﻿#include <QMap>
#include <QList>
#include <QRegularExpression>

#include "DataListTableControl.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct DataListTableControl::Impl {
        AcquisitionDataRepo::Pointer dataRepo{nullptr};
        Configs configs;

        auto ClearConfigs() -> void;

        auto GetImagingType(const QString& acquisitionName) -> QString;
        auto GetAcquisitionID(const QString& acquisitionName) -> QString;
    };

    DataListTableControl::DataListTableControl() : d{std::make_unique<Impl>()} {
    }

    DataListTableControl::~DataListTableControl() = default;

    auto DataListTableControl::SetDataRepo(const AcquisitionDataRepo::Pointer& dataRepo) -> void {
        d->dataRepo = dataRepo;
    }

    auto DataListTableControl::Clear() -> void {
        d->ClearConfigs();
    }

    auto DataListTableControl::CreateTableConfigs() -> void {
        auto dataList = d->dataRepo->GetDataList();
        for (auto it = dataList.begin(); it != dataList.end(); ++it) {
            const auto dataIndex = it.value().first;
            const auto status = it.value().second;

            const auto specimen = dataIndex.GetGroupName();
            const auto wellPos = dataIndex.GetWellPositionName();
            const auto processingStatus = status;
            const auto imagingType = d->GetImagingType(dataIndex.GetAcquisitionDataName()); // get type using acqdataname;
            const auto acquisitionID = d->GetAcquisitionID(dataIndex.GetAcquisitionDataName()); // get id using acqdataname;
            const auto timestamp = dataIndex.GetTimestamp();
            const auto dataPath = dataIndex.GetAcquisitionDataPath();

            const auto dataKey = it.key();

            DataListTableConfig config;

            config.SetChecked(false);
            config.SetSpecimen(specimen);
            config.SetWellPosition(wellPos);
            config.SetProcessingStatus(processingStatus);
            config.SetImagingType(imagingType);
            config.SetAcquisitionID(acquisitionID);
            config.SetTimestamp(timestamp);
            config.SetDataKey(dataKey);
            config.SetDataPath(dataPath);

            d->configs.push_back(config);
        }
    }

    auto DataListTableControl::GetTableConfigs() -> Configs& {
        return d->configs;
    }

    auto DataListTableControl::AddTableConfig(int32_t newKey) -> void {
        const auto newData = d->dataRepo->GetData(newKey);
        const auto dataIndex = newData.first;
        const auto status = newData.second;

        const auto specimen = dataIndex.GetGroupName();
        const auto wellPos = dataIndex.GetWellPositionName();
        const auto processingStatus = status;
        const auto imagingType = d->GetImagingType(dataIndex.GetAcquisitionDataName()); // get type using acqdataname;
        const auto acquisitionID = d->GetAcquisitionID(dataIndex.GetAcquisitionDataName()); // get id using acqdataname;
        const auto timestamp = dataIndex.GetTimestamp();
        const auto dataKey = newKey;
        const auto dataPath = dataIndex.GetAcquisitionDataPath();

        DataListTableConfig config;

        config.SetChecked(false);
        config.SetSpecimen(specimen);
        config.SetWellPosition(wellPos);
        config.SetProcessingStatus(processingStatus);
        config.SetImagingType(imagingType);
        config.SetAcquisitionID(acquisitionID);
        config.SetTimestamp(timestamp);
        config.SetDataKey(dataKey);
        config.SetDataPath(dataPath);

        d->configs.push_back(config);
    }

    auto DataListTableControl::GetTableConfig(int32_t key) const -> DataListTableConfig& {
        for (auto& config : d->configs) {
            if (config.GetDataKey() == key) {
                return config;
            }
        }
        throw std::logic_error("key " + std::to_string(key) + " does not exist....");
    }

    auto DataListTableControl::DeleteTableConfig(int32_t key) -> void {
        for(auto& config : d->configs) {
            if(config.GetDataKey() == key) {
                d->configs.removeOne(config);
                return;
            }
        }
    }

    auto DataListTableControl::GetDataRepoValue(int32_t key) -> AcquisitionDataRepo::Value& {
        // first: AcquisitionDataIndex
        return d->dataRepo->GetData(key);
    }

    auto DataListTableControl::ChangeProcessingStatus(int32_t key) -> void {
        const auto status = d->dataRepo->GetData(key).second;
        for (auto& config : d->configs) {
            if (config.GetDataKey() == key) {
                config.SetProcessingStatus(status);
            }
        }
    }

    auto DataListTableControl::IsDeleteTarget(const ConfigType& type, const QString& txt) const -> bool {
        int32_t count = 0;
        switch (type) {
            case ConfigType::Specimen: {
                for (const auto& config : d->configs) {
                    if (config.GetSpecimen() == txt) { count++; }
                    if (count > 1) { return false; } 
                }
                break;
            }
            case ConfigType::Well: {
                for (const auto& config : d->configs) {
                    if (config.GetWellPosition() == txt) { count++; }
                    if (count > 1) { return false; }
                }
                break;
            }
            case ConfigType::ImagingType: {
                for (const auto& config : d->configs) {
                    if (config.GetImagingType() == txt) { count++; }
                    if (count > 1) { return false; }
                }
                break;
            }
        }

        return true;
    }

    auto DataListTableControl::GetMenuTitles(int32_t column) -> QStringList {
        QStringList menuTitles;

        switch (column) {
            case Columns::Specimen: {
                for (const auto& config : d->configs) {
                    menuTitles.push_back(config.GetSpecimen());
                }
                break;
            }
            case Columns::Well: {
                for (const auto& config : d->configs) {
                    menuTitles.push_back(config.GetWellPosition());
                }
                break;
            }
            case Columns::Type: {
                for (const auto& config : d->configs) {
                    menuTitles.push_back(config.GetImagingType());
                }
                break;
            }
            default: break;
        }

        if (!menuTitles.isEmpty()) menuTitles.removeDuplicates();

        return menuTitles;
    }

    auto DataListTableControl::Impl::GetImagingType(const QString& acquisitionName) -> QString {
        /*
            current file name convention (#:number)
            Single: S### -> ID아님, 단순 카운팅 숫자임. 등록되지 않은 point에서도 획득할 수 있기 때문에 맞지 않음
            Timelaps: T###P$$ -> 맨 뒤가 ID
        */

        QString typeName;

        if (acquisitionName.data()[0] == 'S') {
            typeName = "S";
        }
        if (acquisitionName.data()[0] == 'T') {
            typeName = "T";
            int i = 1;
            while (acquisitionName.data()[i] != 'P' && i < acquisitionName.size()) {
                typeName.append(acquisitionName.data()[i]);
                i++;
            }
        }
        return typeName;
    }

    auto DataListTableControl::Impl::GetAcquisitionID(const QString& acquisitionName) -> QString {
        /*
            acquisition ID는 acquisitionName의 마지막 숫자이다.
            Single type의 마지막 숫자는 실제로 ID와 매핑되지 않는다.
        */
        const QRegularExpression re(QLatin1String("[^0-9]+"));
        auto id = acquisitionName.section(re, -1);
        return id;
    }

    auto DataListTableControl::Impl::ClearConfigs() -> void {
        configs.clear();
    }
}
