﻿#pragma once

#include <any>
#include <memory>

#include <QTableWidget>
#include <QTableView>

#include "AcquisitionDataIndex.h"
#include "AcquisitionDataRepo.h"
#include "DataListTableFilterMenu.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class DataListTable : public QTableView {
        Q_OBJECT
    public:
        enum class Request {
            SetDataList,
            AddData,
            Clear,
            ChangeStatus,
            DeleteData,
            DefaultTableOrder
        };

        using Self = DataListTable;
        using Pointer = std::shared_ptr<Self>;

        explicit DataListTable(QWidget* parent = nullptr);
        ~DataListTable() override;

        auto SetDataRepo(const AcquisitionDataRepo::Pointer& dataRepo) -> void;

        auto UpdateRequest(Request type, std::any value = 0) -> void;

        auto SetSelectionMode(const SelectionMode& mode)->void;
        auto GetSelectedList() const ->QList<QString>;

        auto SetSelectRow(const int& row)->void;

        auto SetCheckVisibility(bool visibility) -> void;
        auto GetCheckVisibility() const -> bool;

    protected:
        auto mousePressEvent(QMouseEvent* event) -> void override;
        auto mouseMoveEvent(QMouseEvent* event) -> void override;
        auto contextMenuEvent(QContextMenuEvent* event) -> void override;

    signals:
        void sigSelectedDataIndex(const AcquisitionDataIndex&);

    public slots:
        void onShowFilterMenu(int32_t section);
        void onUpdateUncheckedFilters(int32_t section, const QStringList filters);
        void onSort(int32_t section);

    private slots:
        void onSelectionChanged(int32_t row);
        void onMouseDoubleClicked(const QModelIndex& index);
        void onHideColumnHeader();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

