﻿#pragma once

#include <enum.h>

namespace HTXpress::AppComponents::AcquisitionListPanel {
    BETTER_ENUM(Columns, int32_t, Key = 0, Check, Specimen, Well, Status, Type, ID, Timestamp);
    BETTER_ENUM(ImagingType, int32_t, Single = 0, Timelapse, Test, Unknown);
}
