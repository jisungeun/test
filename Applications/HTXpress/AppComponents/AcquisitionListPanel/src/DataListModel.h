﻿#pragma once

#include <any>
#include <memory>

#include <QStandardItemModel>

#include "TableDefines.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class DataListModel : public QStandardItemModel {
        Q_OBJECT
    public:
        using Self = DataListModel;
        using Pointer = std::shared_ptr<Self>;

        explicit DataListModel(QObject* parent = nullptr);
        ~DataListModel() override;

        auto Clear() -> void;

        auto SetDataList(std::any dataList) -> void;
        auto AddData(std::any tableConfig) -> void;
        auto DeleteData(int32_t dataKey) -> void;
        auto ChangeStatusData(std::any tableConfig) -> void;

        auto data(const QModelIndex& index, int role) const -> QVariant override;
        auto flags(const QModelIndex& index) const -> Qt::ItemFlags override;

        auto headerData(int section, Qt::Orientation orientation, int role) const -> QVariant override;
        auto sort(int column, Qt::SortOrder order) -> void override;

    protected:

    signals:
        void sigItemModelClear();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
