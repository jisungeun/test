﻿#pragma once

#include <memory>

#include "AcquisitionDataIndex.h"
#include "AcquisitionDataRepo.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class AcquisitionListPanelControl {
    public:
        using Self = AcquisitionListPanelControl;
        using Pointer = std::shared_ptr<Self>;

        AcquisitionListPanelControl();
        ~AcquisitionListPanelControl();

        auto SetAcquisitionDataList(const QList<AcquisitionDataIndex>& list) -> void;

        auto AddAcquisitionData(const AcquisitionDataIndex& index, ProcessingStatus status) -> AcquisitionDataRepo::UniqueKey;
        auto SetDataProcessingStatus(const AcquisitionDataIndex& index, ProcessingStatus status) -> AcquisitionDataRepo::UniqueKey;

        auto GetUniqueKeyByIndex(const AcquisitionDataIndex& index) const -> AcquisitionDataRepo::UniqueKey;
        auto DeleteAcquisitionData(const AcquisitionDataRepo::UniqueKey& key) -> void;

        auto ClearAll() -> void;

        auto GetDataRepo() const -> const AcquisitionDataRepo::Pointer&;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
