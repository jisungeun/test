﻿#pragma once

#include <any>
#include <memory>

#include <QStringList>

#include "TableDefines.h"
#include "AcquisitionDataRepo.h"
#include "DataListTableConfig.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class DataListTableControl {
    public:
        enum class ConfigType {Specimen, Well, ImagingType};

        using Self = DataListTableControl;
        using Pointer = std::shared_ptr<Self>;
        using Configs = QList<DataListTableConfig>;

        DataListTableControl();
        ~DataListTableControl();

        auto SetDataRepo(const AcquisitionDataRepo::Pointer& dataRepo) -> void;

        auto Clear() -> void;
        auto CreateTableConfigs() -> void;
        auto GetTableConfigs() -> Configs&;

        auto AddTableConfig(int32_t newKey) -> void;
        auto GetTableConfig(int32_t key) const -> DataListTableConfig&;
        auto DeleteTableConfig(int32_t key) -> void;
        auto GetDataRepoValue(int32_t key) -> AcquisitionDataRepo::Value&;
        auto ChangeProcessingStatus(int32_t key) -> void;

        auto IsDeleteTarget(const ConfigType& type, const QString& txt) const -> bool;
        auto GetMenuTitles(int32_t column) -> QStringList;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
