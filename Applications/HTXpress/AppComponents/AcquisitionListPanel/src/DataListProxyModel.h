﻿#pragma once

#include <memory>

#include <QSortFilterProxyModel>

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class DataListProxyModel : public QSortFilterProxyModel {
        Q_OBJECT
    public:
        enum class CustomOrder {
            Ascending, Descending, FirstInput, /*LastInput*/
        };
        using Self = DataListProxyModel;
        using Pointer = std::shared_ptr<Self>;

        explicit DataListProxyModel(QObject* parent = nullptr);
        ~DataListProxyModel() override;

        auto ClearFilters(int32_t section = -1) -> void;

        auto SetSpecimenList(const QStringList& list) -> void;
        auto SetWellList(const QStringList& list) -> void;
        auto SetTypeList(const QStringList& list) -> void;

        auto sort(int column, Qt::SortOrder order) -> void override;

    protected:
        auto filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const -> bool override;
        auto lessThan(const QModelIndex& sourceLeft, const QModelIndex& sourceRight) const -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
