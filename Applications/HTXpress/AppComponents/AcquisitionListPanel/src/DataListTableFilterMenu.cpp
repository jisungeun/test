﻿#include <QApplication>
#include <QMouseEvent>

#include "DataListTableFilterMenu.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    struct DataListTableFilterMenu::Impl {
        explicit Impl(Self* self) : self(self){}
        Self* self{};

        int32_t column{-1};
        QPoint pressedPos{};
        QAction* initCheckAction{nullptr};
        QAction* initUnCheckAction{nullptr};
        int32_t defaultMenuType{999};
        int32_t userInputDataType{1000};
        int32_t defaultMenuSize{};

        auto DontHideCondition() const -> bool;
        auto IsDuplicateText(const QString& name) -> bool;
        auto AddInitAction() -> void;
        auto IsNewStringLengthLessThanExistOne(const QString& existActionName, const QString& newActionName) -> bool;
        auto UpdateCheckActionAvailable() -> void;
    };

    auto DataListTableFilterMenu::Impl::DontHideCondition() const -> bool {
        return self->rect().contains(pressedPos);
    }

    auto DataListTableFilterMenu::Impl::IsDuplicateText(const QString& name) -> bool {
        if (self->actions().isEmpty()) return false;

        for (const auto& a : self->actions()) {
            if (name == a->text() && 
                a->data().toInt() != defaultMenuType) return true;
        }
        return false;
    }

    auto DataListTableFilterMenu::Impl::AddInitAction() -> void {
        if (initCheckAction == nullptr) initCheckAction = new QAction(" Check All ");
        if (initUnCheckAction == nullptr) initUnCheckAction = new QAction(" Uncheck All ");

        initCheckAction->setData(defaultMenuType); // default menu type
        initUnCheckAction->setData(defaultMenuType); // default menu type

        self->addAction(initCheckAction);
        self->addAction(initUnCheckAction);
        self->addSeparator();

        defaultMenuSize = self->actions().count();
    }

    auto DataListTableFilterMenu::Impl::IsNewStringLengthLessThanExistOne(const QString& existActionName, const QString& newActionName) -> bool {
        if (existActionName.length() == newActionName.length()) {
            return existActionName > newActionName;
        }
        return existActionName.length() > newActionName.length();
    }

    auto DataListTableFilterMenu::Impl::UpdateCheckActionAvailable() -> void {
        if(initCheckAction && initUnCheckAction) {
            const bool enabled = self->actions().count() == defaultMenuSize ? false : true;
            initCheckAction->setEnabled(enabled);
            initUnCheckAction->setEnabled(enabled);
        }
    }

    DataListTableFilterMenu::DataListTableFilterMenu(int32_t column, QWidget* parent)
        : QMenu(parent), d{std::make_unique<Impl>(this)} {
        d->column = column;
        connect(this, &Self::triggered, this, &Self::onTriggered);

        d->AddInitAction();
        d->UpdateCheckActionAvailable();
    }

    DataListTableFilterMenu::~DataListTableFilterMenu() = default;

    auto DataListTableFilterMenu::AddMenu(const QString& name, bool status) -> void {
        if(d->IsDuplicateText(name)) return;

        const auto newAction = new QAction(name);
        newAction->setData(d->userInputDataType);
        newAction->setCheckable(true);
        newAction->setChecked(status);
        if (actions().isEmpty()) {
            addAction(newAction);
        }
        else {
            bool found = false;
            for (const auto& existAction : actions()) {
                if (existAction->data().toInt() == d->userInputDataType) {
                    if (d->IsNewStringLengthLessThanExistOne(existAction->text(), newAction->text())) {
                        insertAction(existAction, newAction);
                        found = true;
                        break;
                    }
                }
            }
            if (!found) {
                addAction(newAction);
            }
        }

        d->UpdateCheckActionAvailable();
    }

    auto DataListTableFilterMenu::DeleteMenu(const QString& name) -> void {
        for(const auto &action : actions()) {
            if(action->text() == name && action->data().toInt() == d->userInputDataType) {
                this->removeAction(action);
                break;
            }
        }

        d->UpdateCheckActionAvailable();
    }

    auto DataListTableFilterMenu::ClearMenu() -> void {
        clear(); // Removes all the menu's actions. Actions owned by the menu and not shown in any other widget are deleted.
        d->AddInitAction();
        d->UpdateCheckActionAvailable();
    }

    void DataListTableFilterMenu::setVisible(bool visible) {
        // don't hide menu
        if (!visible && activeAction()) {
            if (d->DontHideCondition()) return;
        }

        QMenu::setVisible(visible);
    }

    void DataListTableFilterMenu::mousePressEvent(QMouseEvent* event) {
        d->pressedPos = event->pos();
        QMenu::mousePressEvent(event);
    }

    void DataListTableFilterMenu::onTriggered(QAction* action) {
        if(action == d->initCheckAction) {
            for (auto a : actions()) {
                if (!a->isChecked() && a->data().toInt() == d->userInputDataType) {
                    a->setChecked(true);
                }
            }
        }
        else if(action == d->initUnCheckAction) {
            for (auto a : actions()) {
                if (a->isChecked() && a->data().toInt() == d->userInputDataType) {
                    a->setChecked(false);
                }
            }
        }

        
        QStringList uncheckedList;
        for (auto a : actions()) {
            if (!a->isChecked() && a->data().toInt() == d->userInputDataType) {
                uncheckedList.push_back(a->text());
            }
        }

        emit sigUpdateUncheckedFilters(d->column, uncheckedList);
    }
}
