#include <catch2/catch.hpp>

#include <QString>
#include <QDebug>
#include <QDir>
#include <QStringList>
#include <QRegularExpression>
#include <QDebug>

#include "AcquisitionDataIndex.h"
#include "AcquisitionListPanelControl.h"
#include "AcquisitionDataRepo.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    TEST_CASE("QString split test") {
        QString str = "123/12";
        QString asd = "30/1";

        auto list = str.split(QRegularExpression("/"));
        CHECK(list.size() == 2);
        CHECK(list.at(0).toInt() == 123);
        CHECK(list.at(1).toInt() == 12);
    }

    TEST_CASE("QString operator test") {
        AcquisitionDataIndex idx1;
        AcquisitionDataIndex idx2;

        idx1.SetGroupName("4");
        idx1.SetWellPositionName("2");
        idx1.SetAcquisitionDataName("0");
        idx1.SetTimestamp("0");

        idx2.SetGroupName("s3");
        idx2.SetWellPositionName("1");
        idx2.SetAcquisitionDataName("0");
        idx2.SetTimestamp("0");

        bool groupNameResult = idx1.GetGroupName() < idx2.GetGroupName();
        bool posResult = idx1.GetWellPositionName() < idx2.GetWellPositionName();
        bool acqResult = idx1.GetAcquisitionDataName() < idx2.GetAcquisitionDataName();
        bool timeResult = idx1.GetTimestamp() <= idx2.GetTimestamp();
        bool totalResult = idx1 < idx2;

        CHECK(groupNameResult == true);
        CHECK(posResult == false);
        CHECK(acqResult == false);
        CHECK(timeResult == true);
        CHECK(totalResult == true);
    }

    //TEST_CASE("QRegExpTest") {
    //    QString testProjectPath = "E:\\Temp\\Test";
    //    SECTION("Find SinglePoint folder") {
    //        const auto dir = QDir(testProjectPath);
    //        int32_t count = 0;
    //        QRegExp rx("[S](\\d+)");
    //        for (const auto& path : dir.entryList(QDir::Filter::Dirs | QDir::NoDotAndDotDot)) {
    //            const auto a = path.section(".", -1); // 폴더명 마지막 . 이후의 string만 남김
    //            if (a.contains(rx)) {
    //                count++;
    //            }
    //        }
    //        CHECK(count == 2);
    //    }
    //    SECTION("Find TP folder") {
    //        const auto dir = QDir(testProjectPath);
    //        int32_t count = 0;
    //        QRegExp rx("[T][P](\\d+)");
    //        for (const auto& path : dir.entryList(QDir::Filter::Dirs | QDir::NoDotAndDotDot)) {
    //            const auto a = path.section(".", -1); // 폴더명 마지막 . 이후의 string만 남김
    //            if (a.contains(rx)) {
    //                count++;
    //            }
    //        }
    //        CHECK(count == 5);
    //    }

    //    SECTION("Find T___P___ folder") {
    //        const auto dir = QDir(testProjectPath);
    //        int32_t count = 0;
    //        QRegExp rx("[T](\\d+)[P](\\d+)");
    //        for (const auto& path : dir.entryList(QDir::Filter::Dirs | QDir::NoDotAndDotDot)) {
    //            const auto a = path.section(".", -1); // 폴더명 마지막 . 이후의 string만 남김
    //            qDebug() << "name:" << a;
    //            if (a.contains(rx)) {
    //                count++;
    //            }
    //        }
    //        CHECK(count == 4);
    //    }
    //}

    TEST_CASE("Int to Text function test") {
        auto ConvertNumberIndexToTextIndex = [](int32_t numberIndex) -> QString {
            QString text;

            while (numberIndex > 0) {
                const int32_t remainder = numberIndex % 26;
                if (remainder == 0) {
                    text.append('Z');
                    numberIndex = (numberIndex / 26) - 1;
                }
                else {
                    text.append((remainder - 1) + 'A');
                    numberIndex /= 26;
                }
            }

            std::reverse(text.begin(), text.end());
            return text;
        };

        SECTION("int to text") {
            for (int i = 0; i < 1000; i++) {
                qDebug() << i << "->" << ConvertNumberIndexToTextIndex(i + 1);
                if (i == 0) {
                    CHECK(QString("A") == QString('A'));
                    CHECK(ConvertNumberIndexToTextIndex(i+1)==QString("A"));
                    CHECK(ConvertNumberIndexToTextIndex(i+1)==QString('A'));
                }
                if (i == 25) {
                    CHECK(ConvertNumberIndexToTextIndex(i+1)==QString('Z'));
                }
                if (i == 730) {
                    CHECK(ConvertNumberIndexToTextIndex(i+1)==QString("ABC"));
                    CHECK(ConvertNumberIndexToTextIndex(i+1).at(0)==QString('A'));
                    CHECK(ConvertNumberIndexToTextIndex(i+1).at(1)==QString('B'));
                    CHECK(ConvertNumberIndexToTextIndex(i+1).at(2)==QString('C'));
                }
            }
        }
    }

    TEST_CASE("parsing text from acq data folder name") {
        QString tempDataFolder3 = "220622.104539.EXP.001.GROUP1.A2.S003";
        QString tempDataFolder4 = "220622.104601.EXP.001.GROUP1.A1.TP01";
        QString tempDataFolder6 = "220622.104399.EXP.001.GROUP1.A1.T001P02";


        SECTION("get timestamp yyMMdd.hhmmss") {
            QString tempDataFolder1 = "220622.104537.EXP.001.GROUP1.A1.S001";
            const auto before = tempDataFolder1.split("."); // QStringList type
            const auto after = tempDataFolder1.section(".", 0, 1); // QString type

            qDebug() << "timestampIndex" << before << "," << after;

            CHECK("220622" == before.at(0));
            CHECK("104537" == before.at(1));
            CHECK("220622.104537" == after);
        }
        SECTION("get acq data index from data folder") {
            QString acqDataIdx_single = "S001";
            QString acqDataIdx_timelapse = "T001P02";
            QString acqDataIdx_test = "TP01";

            QString tempDataFolder2 = "220622.104538.EXP.001.GROUP1.A1.S002";
            auto before = tempDataFolder2.split(".").last();
            auto after = tempDataFolder2.section(".", -1);

            auto testSingle = acqDataIdx_single.section(".", -1);
            auto testTimelapse = acqDataIdx_timelapse.section(".", -1);
            auto testOneCycle = acqDataIdx_test.section(".", -1);

            qDebug() << "test string:" << testSingle << testTimelapse << testOneCycle;

            auto testSingle2 = acqDataIdx_single.split(".").last();
            auto testTimelapse2 = acqDataIdx_timelapse.split(".").last();
            auto testOneCycle2 = acqDataIdx_test.split(".").last();

            qDebug() << "test string2:" << testSingle2 << testTimelapse2 << testOneCycle2;

            qDebug() << "acqDataIndex" << before << "," << after;

            CHECK(testSingle == acqDataIdx_single);
            CHECK(testTimelapse == acqDataIdx_timelapse);
            CHECK(testOneCycle == acqDataIdx_test);

            CHECK(testSingle2 == acqDataIdx_single);
            CHECK(testTimelapse2 == acqDataIdx_timelapse);
            CHECK(testOneCycle2 == acqDataIdx_test);

            CHECK(before == after);
        }
        SECTION("get P## when T###P##") {
            auto rx = QRegExp("[T](\\d+)");
            QString acqDataIdx_timelapse = "T001P02";
            auto txt = acqDataIdx_timelapse.remove(rx);
            qDebug() << "point id?" << txt;
            CHECK(txt == QString("P02"));
        }
        SECTION("Get thumbnail data name") {
            QString tempDataFolder5 = "220622.104700.EXP.001.GROUP1.A1.T001P01";
            auto dataName = tempDataFolder5.section(".", -3, -1);
            qDebug() << "result" << dataName;
            CHECK(dataName == "GROUP1.A1.T001P01");
            dataName.replace(".", "-");
            qDebug() << ". replaced" << dataName;
            CHECK(dataName == "GROUP1-A1-T001P01");
        }
        SECTION("Get last number from acquisition name") {
            QString acqDataIdx_single = "S001";
            QString acqDataIdx_timelapse = "T001P02";
            QString acqDataIdx_test = "TP03";

            QRegularExpression re(QLatin1String("[^0-9]+"));
            const auto parts1 = acqDataIdx_single.split(re);
            const auto parts2 = acqDataIdx_timelapse.split(re);
            const auto parts3 = acqDataIdx_test.split(re);
            qDebug() << parts1.last().toInt() << "of" << parts1;
            qDebug() << parts2.last().toInt() << "of" << parts2;
            qDebug() << parts3.last().toInt() << "of" << parts3;
            CHECK(parts1.last().toInt() == 1);
            CHECK(parts2.last().toInt() == 2);
            CHECK(parts3.last().toInt() == 3);

            const auto part1 = acqDataIdx_single.section(re, -1);
            const auto part2 = acqDataIdx_timelapse.section(re, -1);
            const auto part3 = acqDataIdx_test.section(re, -1);

            qDebug() << part1 << part2 << part3;

            CHECK(part1.toInt() == 1);
            CHECK(part2.toInt() == 2);
            CHECK(part3.toInt() == 3);

            CHECK(part1 == "001");
            CHECK(part2 == "02");
            CHECK(part3 == "03");
        }
    }
}
