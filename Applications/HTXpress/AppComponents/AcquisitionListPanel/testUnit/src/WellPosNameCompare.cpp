﻿#include <catch2/catch.hpp>

#include <QString>
#include <QStringList>
#include <QDebug>
#include <QCollator>

namespace HTXpress::AppComponents::AcquisitionListPanel {
    static bool IsSameResult(const int32_t& lacResult, const bool& opResult) {
        // left == right
        if(lacResult == 0 && !opResult) return true;
        if(lacResult < 0 && opResult) return true;
        if(lacResult > 0 && !opResult) return true;

        return false;
    }

    TEST_CASE("qstring compare") {
        SECTION("Normal well pos name comapre") {
            QStringList s;
            s
            << "A1" << "A2" << "A3" << "A4" << "A5" << "A6"
            << "B1" << "B2" << "B3" << "B4" << "B5" << "B6"
            << "C1" << "C2" << "C3" << "C4" << "C5" << "C6"
            << "D1" << "D2" << "D3" << "D4" << "D5" << "D6";

            CHECK(s.size() == 24);

            for(int i = 0; i < s.size(); ++i) {
                for(int j = 0; j < s.size(); ++j) {
                    const auto& left = s.at(i);
                    const auto& right = s.at(j);
                    const auto lac = QString::localeAwareCompare(left, right);
                    const auto op = (left < right);
                    if(!IsSameResult(lac, op)) {
                        qDebug() << "diff result:" << left << right << "result:" << lac << ","<< op;
                    }
                    if(i==j) {
                        CHECK(lac == 0);
                        CHECK(op == false);
                    }
                    else if(i>j) {
                        CHECK(lac == 1);
                        CHECK(op == false);
                    }
                    else if(i<j) {
                        CHECK(lac == -1);
                        CHECK(op == true);
                    }

                }
            }
        }
        SECTION("Speciment upper/lower case compare") {
            QStringList s {
                "abc", "abC", "aBc", "Abc", "ABc", "AbC", "aBC", "ABC"
            };
            QStringList s2 {
                "abc1", "abC1", "aBc1", "Abc1", "ABc1", "AbC1", "aBC1", "ABC1"
            };

            QCollator coll1;
            coll1.setNumericMode(true);
            coll1.setCaseSensitivity(Qt::CaseInsensitive);

            QCollator coll2; // use this one
            coll2.setNumericMode(true);
            coll2.setCaseSensitivity(Qt::CaseSensitive);

            QCollator coll3;
            coll2.setNumericMode(false);
            coll2.setCaseSensitivity(Qt::CaseInsensitive);

            QCollator coll4;
            coll2.setNumericMode(false);
            coll2.setCaseSensitivity(Qt::CaseSensitive);

            for(int i = 0; i < s.size(); ++i) {
                for(int j = 0; j < s.size(); ++j) {
                    const auto& lhs = s.at(i);
                    const auto& rhs = s.at(j);
                    qDebug() << "--> localeAwareCompare   :" << lhs << rhs << QString::localeAwareCompare(lhs, rhs);
                    qDebug() << "--> QCollator(numT,caseI):" << lhs << rhs << coll1.compare(lhs, rhs);
                    qDebug() << "--> QCollator(numT,caseS):" << lhs << rhs << coll2.compare(lhs, rhs);
                    qDebug() << "--> QCollator(numF,caseI):" << lhs << rhs << coll3.compare(lhs, rhs);
                    qDebug() << "--> QCollator(numF,caseS):" << lhs << rhs << coll4.compare(lhs, rhs);
                    if(i==j) {
                        CHECK(coll2.compare(lhs,rhs) == 0);
                    }
                    else {
                        CHECK(coll2.compare(lhs,rhs) != 0);
                    }
                }
            }

            qDebug() << "=========================================================================================================";

            for(int i = 0; i < s2.size(); ++i) {
                for(int j = 0; j < s2.size(); ++j) {
                    const auto& lhs = s2.at(i);
                    const auto& rhs = s2.at(j);
                    qDebug() << "++> localeAwareCompare   :" << lhs << rhs << QString::localeAwareCompare(lhs, rhs);
                    qDebug() << "++> QCollator(numT,caseI):" << lhs << rhs << coll1.compare(lhs, rhs);
                    qDebug() << "++> QCollator(numT,caseS):" << lhs << rhs << coll2.compare(lhs, rhs);
                    qDebug() << "++> QCollator(numF,caseI):" << lhs << rhs << coll3.compare(lhs, rhs);
                    qDebug() << "++> QCollator(numF,caseS):" << lhs << rhs << coll4.compare(lhs, rhs);
                    if(i==j) {
                        CHECK(coll2.compare(lhs,rhs) == 0);
                    }
                    else {
                        CHECK(coll2.compare(lhs,rhs) != 0);
                    }
                }
            }
        }
    }
}
