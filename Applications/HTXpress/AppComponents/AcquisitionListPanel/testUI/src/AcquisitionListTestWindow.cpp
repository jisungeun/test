#include <QtGui/QtGui>
#include <QDebug>
#include <QDateTime>
#include <QMessageBox>
#include <QRandomGenerator>

#include "AcquisitionListTestWindow.h"
#include "ui_AcquisitionListTestWindow.h"
#include "AcquisitionListPanel.h"

namespace HTXpress::AppComponents::AcquisitionListPanel::TEST {
    struct AcquisitionListTestWindow::Impl {
        Ui::TestWindow ui;
        AcquisitionListPanel* widget{nullptr};

        auto Connect(const Self* self) -> void;

        auto GetRandomSpecimen() -> QString;
        auto GetRandomWellPosition() -> QString;
        auto GetRandomAcqData() -> QString;
        auto GetRandomDate() -> QString;
    };

    AcquisitionListTestWindow::AcquisitionListTestWindow(QWidget* parent) : QMainWindow(parent), d{std::make_unique<Impl>()} {
        d->ui.setupUi(this);

        d->widget = new AcquisitionListPanel(this);
        auto layout = new QGridLayout;
        layout->addWidget(d->widget);
        layout->setSpacing(0);
        layout->setMargin(0);
        layout->setContentsMargins(0, 0, 0, 0);
        d->ui.frame->setLayout(layout);

        d->Connect(this);
    }

    AcquisitionListTestWindow::~AcquisitionListTestWindow() {
    }

    void AcquisitionListTestWindow::onSetAcquisitionDataList() {
        qDebug() << Q_FUNC_INFO << "onSetAcquisitionDataList";
        QList<AcquisitionDataIndex> list;
        for (int row = 0; row < d->ui.testTable->rowCount(); ++row) {
            AcquisitionDataIndex dataIndex;
            for (int col = 0; col < d->ui.testTable->columnCount() - 1; ++col) {
                auto item = d->ui.testTable->item(row, col);
                switch (col) {
                    case 0: dataIndex.SetGroupName(item->text());
                        break;
                    case 1: dataIndex.SetWellPositionName(item->text());
                        break;
                    case 2: dataIndex.SetAcquisitionDataName(item->text());
                        break;
                    case 3: dataIndex.SetTimestamp(item->text());
                        break;
                    default: ;
                }
            }
            list.push_back(dataIndex);
        }

        d->widget->SetAcquisitionDataList(list);
    }

    void AcquisitionListTestWindow::onSetManyAcquisitionDataList() {
        QList<AcquisitionDataIndex> list;
        for (int i = 0; i < d->ui.spTestNum->value(); ++i) {
            AcquisitionDataIndex dataIndex;
            const auto groupName = d->GetRandomSpecimen();
            const auto wellName = d->GetRandomWellPosition();
            const auto dataName = d->GetRandomAcqData();
            const auto timestamp = d->GetRandomDate();

            dataIndex.SetGroupName(groupName);
            dataIndex.SetWellPositionName(wellName);
            dataIndex.SetAcquisitionDataName(dataName);
            dataIndex.SetTimestamp(timestamp);

            list.push_back(dataIndex);
        }
        d->widget->SetAcquisitionDataList(list);
    }

    void AcquisitionListTestWindow::onAddAcquisitionData() {
        qDebug() << Q_FUNC_INFO << "onAddAcquisitionData";
        QString groupName = d->ui.editGroupName->text();
        QString wellName = d->ui.editWellName->text();
        QString dataName = d->ui.editDataName->text();
        QString timestamp = d->ui.editTimestamp->text();

        AcquisitionDataIndex dataIndex;

        dataIndex.SetGroupName(groupName);
        dataIndex.SetWellPositionName(wellName);
        dataIndex.SetAcquisitionDataName(dataName);
        dataIndex.SetTimestamp(timestamp);

        auto status = ProcessingStatus::_from_index(d->ui.comboProcessingStatus->currentIndex());

        d->widget->AddAcquisitionData(dataIndex, status);
    }

    void AcquisitionListTestWindow::onSetDataProcessingStatus() {
        qDebug() << Q_FUNC_INFO << "onSetDataProcessingStatus";
        QString groupName = d->ui.editGroupName->text();
        QString wellName = d->ui.editWellName->text();
        QString dataName = d->ui.editDataName->text();
        QString timestamp = d->ui.editTimestamp->text();

        AcquisitionDataIndex dataIndex;

        dataIndex.SetGroupName(groupName);
        dataIndex.SetWellPositionName(wellName);
        dataIndex.SetAcquisitionDataName(dataName);
        dataIndex.SetTimestamp(timestamp);

        auto status = ProcessingStatus::_from_index(d->ui.comboProcessingStatus->currentIndex());

        d->widget->SetDataProcessingStatus(dataIndex, status);
    }

    void AcquisitionListTestWindow::onDeleteAcquisitionData() {
        qDebug() << "onDeleteAcquisitionData";
        QString groupName = d->ui.editGroupName->text();
        QString wellName = d->ui.editWellName->text();
        QString dataName = d->ui.editDataName->text();
        QString timestamp = d->ui.editTimestamp->text();

        AcquisitionDataIndex dataIndex;

        dataIndex.SetGroupName(groupName);
        dataIndex.SetWellPositionName(wellName);
        dataIndex.SetAcquisitionDataName(dataName);
        dataIndex.SetTimestamp(timestamp);

        d->widget->DeleteAcquisitionData(dataIndex);
    }


    void AcquisitionListTestWindow::onClearAll() {
        qDebug() << Q_FUNC_INFO << "onClearAll";
        d->widget->ClearAll();
    }

    void AcquisitionListTestWindow::onRecvSelectedDataIndex(const AcquisitionDataIndex& dataIndex) {
        QMessageBox::information(this, "Recv dataIndex info", dataIndex.GetGroupName() + ", " + dataIndex.GetWellPositionName() + ", " + dataIndex.GetAcquisitionDataName() + ", " + dataIndex.GetTimestamp());
        d->ui.editGroupName->setText(dataIndex.GetGroupName());
        d->ui.editWellName->setText(dataIndex.GetWellPositionName());
        d->ui.editDataName->setText(dataIndex.GetAcquisitionDataName());
        d->ui.editTimestamp->setText(dataIndex.GetTimestamp());
    }

    void AcquisitionListTestWindow::onGenerateTimestamp() {
        const auto timestamp = QDateTime::currentDateTime().toString("yyMMdd");
        d->ui.editTimestamp->setText(timestamp);
    }

    void AcquisitionListTestWindow::onDeleteTestData() {
        if (d->ui.testTable->selectedItems().isEmpty() == false) {
            d->ui.testTable->removeRow(d->ui.testTable->selectedItems().last()->row());
        }
    }

    void AcquisitionListTestWindow::onClearTestTable() {
        d->ui.testTable->setRowCount(0);
    }

    auto AcquisitionListTestWindow::Impl::Connect(const Self* self) -> void {
        connect(ui.btnSetAcqDataList, &QPushButton::clicked, self, &Self::onSetAcquisitionDataList);
        connect(ui.btnAddAcqData, &QPushButton::clicked, self, &Self::onAddAcquisitionData);
        connect(ui.btnChangeProcStatus, &QPushButton::clicked, self, &Self::onSetDataProcessingStatus);
        connect(ui.btnClearAcqData, &QPushButton::clicked, self, &Self::onClearAll);
        connect(ui.btnDeleteAcqData, &QPushButton::clicked, self, &Self::onDeleteAcquisitionData);
        
        connect(ui.btnGenerateTimestamp, &QToolButton::clicked, self, &Self::onGenerateTimestamp);
        connect(ui.btnTDeleteTableItem, &QPushButton::clicked, self, &Self::onDeleteTestData);
        connect(ui.btnTDeleteAll, &QPushButton::clicked, self, &Self::onClearTestTable);

        connect(ui.btnSetTestDataList, &QPushButton::clicked, self, &Self::onSetManyAcquisitionDataList);
        
        connect(widget, &AcquisitionListPanel::sigSelectedDataIndex, self, &Self::onRecvSelectedDataIndex);
    }

    auto AcquisitionListTestWindow::Impl::GetRandomSpecimen() -> QString {
        QString randomString;

        const QString possibleChars("AB012");

        const int32_t randomStrLength = QRandomGenerator::global()->bounded(1, 3);

        for (int i = 0; i < randomStrLength; i++) {
            auto index = QRandomGenerator::global()->bounded(0, possibleChars.length());
            auto nextChar = possibleChars.at(index);
            randomString.append(nextChar);
        }

        return "specimen" + randomString;
    }

    auto AcquisitionListTestWindow::Impl::GetRandomWellPosition() -> QString {
        const QString possibleChars("ABCD");
        const QString possibleNums("123456");

        QString randomString;

        auto index = QRandomGenerator::global()->bounded(0, possibleChars.length());
        QChar nextChar = possibleChars.at(index);
        randomString.append(nextChar);

        index = QRandomGenerator::global()->bounded(0, possibleNums.length());
        nextChar = possibleNums.at(index);
        randomString.append(nextChar);

        return randomString;
    }

    auto AcquisitionListTestWindow::Impl::GetRandomAcqData() -> QString {
        QString randomString;

        const QString possibleFirstChar("ST");
        const QString possibleNextChars("P012");

        auto index = QRandomGenerator::global()->bounded(0, possibleFirstChar.length());
        QChar nextChar = possibleFirstChar.at(index);

        randomString.append(nextChar);

        if (nextChar == 'S') {
            for (int i = 0; i < 3; ++i) {
                index = QRandomGenerator::global()->bounded(1, possibleNextChars.length());
                nextChar = possibleNextChars.at(index);
                randomString.append(nextChar);
            }
        }
        else {
            index = QRandomGenerator::global()->bounded(0, possibleNextChars.length());
            nextChar = possibleNextChars.at(index);
            randomString.append(nextChar);

            if (nextChar == 'P') {
                for (int i = 0; i < 3; ++i) {
                    index = QRandomGenerator::global()->bounded(1, possibleNextChars.length());
                    nextChar = possibleNextChars.at(index);
                    randomString.append(nextChar);
                }
            }
            else {
                for (int i = 0; i < 2; ++i) {
                    index = QRandomGenerator::global()->bounded(1, possibleNextChars.length());
                    nextChar = possibleNextChars.at(index);
                    randomString.append(nextChar);
                }
                randomString.append('P');
                for (int i = 0; i < 3; ++i) {
                    index = QRandomGenerator::global()->bounded(1, possibleNextChars.length());
                    nextChar = possibleNextChars.at(index);
                    randomString.append(nextChar);
                }
            }
        }

        return randomString;
    }

    auto AcquisitionListTestWindow::Impl::GetRandomDate() -> QString {
        QString randomDate;
        randomDate = QString("%1%2%3").arg(QRandomGenerator::global()->bounded(2021, 2023)).arg(QRandomGenerator::global()->bounded(10, 12)).arg(QRandomGenerator::global()->bounded(15, 25));
        return randomDate;
    }
}
