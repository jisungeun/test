#include "AcquisitionListTestWindow.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HTXpress::AppComponents::AcquisitionListPanel::TEST::AcquisitionListTestWindow w;
    w.show();
    return a.exec();
}
