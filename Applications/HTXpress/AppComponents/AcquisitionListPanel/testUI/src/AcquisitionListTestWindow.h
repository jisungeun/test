#pragma once

#include <memory>
#include <QtWidgets/QMainWindow>

#include "AcquisitionDataIndex.h"

namespace HTXpress::AppComponents::AcquisitionListPanel::TEST {
	class AcquisitionListTestWindow : public QMainWindow {
		Q_OBJECT
	public:
		using Self = AcquisitionListTestWindow;
		explicit AcquisitionListTestWindow(QWidget* parent = nullptr);
		~AcquisitionListTestWindow() override;

	public slots:
		void onSetAcquisitionDataList();
		void onSetManyAcquisitionDataList();
		void onAddAcquisitionData();
		void onSetDataProcessingStatus();
		void onDeleteAcquisitionData();
		void onClearAll();
		void onRecvSelectedDataIndex(const AcquisitionDataIndex& dataIndex);

	private slots:
		void onGenerateTimestamp();
		void onDeleteTestData();
		void onClearTestTable();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

}
