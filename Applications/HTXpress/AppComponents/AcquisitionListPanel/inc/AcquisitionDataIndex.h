﻿#pragma once

#include <memory>

#include <QDate>
#include <QString>

#include <enum.h>
#include "HTXAcquisitionListPanelExport.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    BETTER_ENUM(ProcessingStatus, int32_t, Unprocessed, Processed, Invalid);

    class HTXAcquisitionListPanel_API AcquisitionDataIndex {
    public:
        using Self = AcquisitionDataIndex;
        using Pointer = std::shared_ptr<Self>;

        AcquisitionDataIndex();
        AcquisitionDataIndex(const QString& group, const QString& well, const QString& acquisitionData, const QString& timestamp);
        AcquisitionDataIndex(const AcquisitionDataIndex& other);
        ~AcquisitionDataIndex();

        auto operator=(const AcquisitionDataIndex& other) -> AcquisitionDataIndex&;

        auto operator==(const AcquisitionDataIndex& other) const -> bool;
        auto operator!=(const AcquisitionDataIndex& other) const -> bool;
        auto operator<(const AcquisitionDataIndex& other) const -> bool;

        auto SetGroupName(const QString& groupName) -> void;
        auto GetGroupName() const -> QString;

        auto SetWellPositionName(const QString& positionName) -> void;
        auto GetWellPositionName() const -> QString;

        auto SetAcquisitionDataName(const QString& dataFullName) -> void;
        auto GetAcquisitionDataName() const -> QString;

        auto SetAcquisitionDataPath(const QString& dataPath) const -> void;
        auto GetAcquisitionDataPath() const ->QString;

        auto SetTimestamp(const QString& timestamp) -> void;
        auto GetTimestamp() const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

/*
    - 획득 데이타 상태는 2가지로 구분
        1. unprocessed : TCF 파일이 생성되지 않은 상태
        2. processed : TCF 파일 생성 완료된 상태
    - Single FOV와 Tile 데이타는 명명상 구분하지 않음
    - index 값은 well 별로 따로 매겨짐 -> 타임랩스만 해당
        *** new update *** single imaging acquisition data에 맨 뒤 숫자는 well id와 관계없다.
    - Acquisition Data List 표는 데이타별로 다음 정보를 표시한다.
        1. Specimen
        2. Well Position
        3. Processed/Unprocessed status
        4. Data Type (S, T, T002, T002, ...)
    - 칼럼 헤더를 이용해 specimen/well pos / data type 별로 분류해서 데이타를 확인할 수 있어야 한다.
*/