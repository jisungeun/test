﻿#pragma once

#include <memory>

#include <QWidget>
#include <QAbstractItemView>

#include "HTXAcquisitionListPanelExport.h"
#include "AcquisitionDataIndex.h"

namespace HTXpress::AppComponents::AcquisitionListPanel {
    class HTXAcquisitionListPanel_API AcquisitionListPanel : public QWidget {
        Q_OBJECT
	public:
        using Self = AcquisitionListPanel;
        using Pointer = std::shared_ptr<Self>;

        explicit AcquisitionListPanel(QWidget* parent = nullptr);
        ~AcquisitionListPanel() override;

        auto ClearAll() -> void;

        auto SetAcquisitionDataList(const QList<HTXpress::AppComponents::AcquisitionListPanel::AcquisitionDataIndex>& list) -> void;

        auto AddAcquisitionData(const HTXpress::AppComponents::AcquisitionListPanel::AcquisitionDataIndex& index, 
                                HTXpress::AppComponents::AcquisitionListPanel::ProcessingStatus status = ProcessingStatus::Unprocessed) -> void;
        auto SetDataProcessingStatus(const HTXpress::AppComponents::AcquisitionListPanel::AcquisitionDataIndex& index, 
                                     HTXpress::AppComponents::AcquisitionListPanel::ProcessingStatus status) -> void;
        auto DeleteAcquisitionData(const HTXpress::AppComponents::AcquisitionListPanel::AcquisitionDataIndex& index) -> void;

        auto SetSelectionMode(const QAbstractItemView::SelectionMode& mode) const ->void;
        auto GetSelectedList() const ->QList<QString>;

        auto SetSelectRow(const int& row) const ->void;

        auto SetCheckVisibility(bool visibility) -> void;
        auto GetCheckVisibility() const -> bool;

        auto SetDefaultTableOrder() -> void;
    signals:
        /*
            List에서 double click한 timestamp의 dataIndex 전송
            @param dataIndex - 선택한 time stamp의 dataIndex(group,well,acqData,timestamp) 값
        */
        void sigSelectedDataIndex(const HTXpress::AppComponents::AcquisitionListPanel::AcquisitionDataIndex& dataIndex);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
