#include <QtGui/QtGui>
#include <QtWidgets/QtWidgets>
#include <QDebug>

#include "ui_SingleImagingTestWindow.h"

#include "SingleImagingTestWindow.h"
#include "ImagingConditionPanel.h"
#include "ImagingConditionDefines.h"
#include "FLChannelConfig.h"

namespace HTXpress::AppComponents::ImagingConditionPanel::TEST {
    struct SingleImagingTestWindow::Impl {
        explicit Impl(Self* self):self(self){}
        Self* self{};
        Ui::Window ui;
        ImagingConditionPanel* panel{nullptr};
        auto ConnectZStack() -> void;
        auto Connect() -> void;
        auto ConnectLiveControl() -> void;
    };

    SingleImagingTestWindow::SingleImagingTestWindow(QWidget* parent) : QMainWindow(parent), d{std::make_unique<Impl>(this)} {
        d->ui.setupUi(this);
        d->panel = new ImagingConditionPanel(this);
        d->ui.viewFrame->layout()->addWidget(d->panel);
        d->ui.tabWidget->setCurrentIndex(0);
        d->ConnectZStack();
        d->Connect();
        d->ConnectLiveControl();
    }

    SingleImagingTestWindow::~SingleImagingTestWindow() {
    }

    void SingleImagingTestWindow::onSetLinkEnable() {
        qDebug() << Q_FUNC_INFO;
    }

    void SingleImagingTestWindow::onSetCurrentZ() {
        qDebug() << Q_FUNC_INFO;
        d->panel->SetCurrentZ(d->ui.spCurrZ->value());
    }

    void SingleImagingTestWindow::onSetScanParameter() {
        qDebug() << Q_FUNC_INFO;
        FLZStackMode mode = (d->ui.zStackDefault->isChecked() ? FLZStackMode::Default : FLZStackMode::FLFocus);
        const auto top = d->ui.spScanRangeTop->value();
        const auto bot = d->ui.spScanRangeBot->value();
        const auto step = d->ui.spScanRangeStep->value();

        d->panel->SetFLZStackValues(mode, top, bot, 0, step);
    }

    void SingleImagingTestWindow::onSetStepUnit() {
        qDebug() << Q_FUNC_INFO << d->ui.spStepUnit->value();
        const auto unit = d->ui.spStepUnit->value();
        d->panel->SetZResolutionUnit(unit);
    }

    void SingleImagingTestWindow::onGetZStackMode() {
        qDebug() << Q_FUNC_INFO;
        const auto mode = d->panel->GetZStackMode();
        if (mode._to_integral() == FLZStackMode::Default) {
            d->ui.zStackDefault->setChecked(true);
        }
        else {
            d->ui.zStackFLFocus->setChecked(true);
        }
    }

    void SingleImagingTestWindow::onGetScanTop() {
        qDebug() << Q_FUNC_INFO;
        const auto val = d->panel->GetFLScanTop();
        d->ui.spScanRangeTop->setValue(val);
    }

    void SingleImagingTestWindow::onGetScanBottom() {
        qDebug() << Q_FUNC_INFO;
        const auto val = d->panel->GetFLScanBottom();
        d->ui.spScanRangeBot->setValue(val);
    }

    void SingleImagingTestWindow::onGetScanStep() {
        qDebug() << Q_FUNC_INFO;
        const auto val = d->panel->GetFLScanStep();
        d->ui.spScanRangeStep->setValue(val);
    }

    void SingleImagingTestWindow::onGetScanRange() {
        qDebug() << Q_FUNC_INFO;
    }

    void SingleImagingTestWindow::onGetScanSlices() {
        qDebug() << Q_FUNC_INFO;
        const auto val = d->panel->GetFLScanSlices();
        d->ui.editScanSlices->setText(QString::number(val));
    }

    void SingleImagingTestWindow::onRecvApplyFLScanCondtion() {
        qDebug() << Q_FUNC_INFO;
    }

    void SingleImagingTestWindow::onRecvUndoFLScanCondition() {
        qDebug() << Q_FUNC_INFO;
    }

    void SingleImagingTestWindow::onRecvChangedZStackZPos(double z) {
        qDebug() << Q_FUNC_INFO << z;
    }

    void SingleImagingTestWindow::onSetMaxFOV() {
        d->panel->SetMaximumFOV(d->ui.fovMaxW->value(), d->ui.fovMaxH->value());
    }

    void SingleImagingTestWindow::onSetFOV() {
        d->panel->SetFOV(d->ui.fovW->value(), d->ui.fovH->value());
    }

    void SingleImagingTestWindow::onGetFOV() {
        d->ui.fovW->setValue(d->panel->GetFovX());
        d->ui.fovH->setValue(d->panel->GetFovY());
    }

    void SingleImagingTestWindow::onRecvFov(double w, double h) {
        d->ui.testLog->append(QString("FOV - %1, %2").arg(w).arg(h));
        d->ui.fovW->setValue(w);
        d->ui.fovH->setValue(h);
    }

    void SingleImagingTestWindow::onSetModalities() {
        QList<ModalityType> list;
        if (d->ui.ht3d->isChecked()) list.append(ModalityType::HT3D);
        if (d->ui.ht2d->isChecked()) list.append(ModalityType::HT2D);
        if (d->ui.fl3d->isChecked()) list.append(ModalityType::FL3D);
        if (d->ui.fl2d->isChecked()) list.append(ModalityType::FL2D);
        if (d->ui.bf->isChecked()) list.append(ModalityType::BF);
        d->panel->SetActiveModality(list);
    }

    void SingleImagingTestWindow::onGetModalities() {
        const auto list = d->panel->GetActiveModality();
        d->ui.ht3d->setChecked(list.contains(ModalityType::HT3D));
        d->ui.ht2d->setChecked(list.contains(ModalityType::HT2D));
        d->ui.fl3d->setChecked(list.contains(ModalityType::FL3D));
        d->ui.fl2d->setChecked(list.contains(ModalityType::FL2D));
        d->ui.bf->setChecked(list.contains(ModalityType::BF));
    }

    void SingleImagingTestWindow::onRecvModalities(/*const QList<ModalityType>& modalities*/) {
        auto modalities = d->panel->GetActiveModality();
        d->ui.ht3d->setChecked(modalities.contains(ModalityType::HT3D));
        d->ui.ht2d->setChecked(modalities.contains(ModalityType::HT2D));
        d->ui.fl3d->setChecked(modalities.contains(ModalityType::FL3D));
        d->ui.fl2d->setChecked(modalities.contains(ModalityType::FL2D));
        d->ui.bf->setChecked(modalities.contains(ModalityType::BF));
        QStringList strModals;
        QString log = "Modality - ";
        for (const auto& m : modalities) {
            log += m._to_string();
            log += " ";
        }
        d->ui.testLog->append(log);
    }

    void SingleImagingTestWindow::onSetTileActivation() {
        d->panel->SetTileImagingActivation(d->ui.tileActivation->isChecked());
    }

    void SingleImagingTestWindow::onSetTileOverlap() {
        d->panel->SetMinimumOverlapArea(d->ui.overlapHor->value(), d->ui.overlapVer->value());
    }

    void SingleImagingTestWindow::onSetTileSize() {
        d->panel->SetTileSize(d->ui.tileW->value(), d->ui.tileH->value());
    }

    void SingleImagingTestWindow::onSetTileCenterPos() {
        d->panel->SetCenterPosition(d->ui.wellX->value(), d->ui.wellY->value());
    }

    void SingleImagingTestWindow::onRecvAcquirePoint() {
        d->ui.testLog->append("Acquire Point");
    }

    void SingleImagingTestWindow::onRecvAcquireTile() {
        d->ui.testLog->append("Acquire Tile");
    }

    void SingleImagingTestWindow::onRecvAddPoint() {
        d->ui.testLog->append("Add Point");
    }

    void SingleImagingTestWindow::onRecvAddTile(double w, double h) {
        d->ui.testLog->append(QString("Add Tile - %1, %2").arg(w).arg(h));
    }

    void SingleImagingTestWindow::onClearFLConditionTable() {
        d->panel->ClearFLAcquisitionTable();
    }

    void SingleImagingTestWindow::onSetFLConditionLock(bool lock) {
        d->panel->SetFLAcquisitionTableLock(lock);
    }

    void SingleImagingTestWindow::onSetFLChannelEnable(bool enable) {
        if (sender() == d->ui.ch1Enable) d->panel->SetFLEnable(0, enable);
        else if (sender() == d->ui.ch2Enable) d->panel->SetFLEnable(1, enable);
        else if (sender() == d->ui.ch3Enable) d->panel->SetFLEnable(2, enable);
    }

    void SingleImagingTestWindow::onSetFLChannelConfig() {
        auto channelIndex = -1;
        if (d->ui.ch1Index->isChecked()) channelIndex = 0;
        else if (d->ui.ch2Index->isChecked()) channelIndex = 1;
        else if (d->ui.ch3Index->isChecked()) channelIndex = 2;

        if (channelIndex == -1) {
            QMessageBox::warning(this, "ChannelIndex == -1", "Please select channel");
            d->ui.ch1Index->setFocus();
            return;
        }

        FLChannelConfig config;
        config.SetChannelName(d->ui.configName->text());
        config.SetExcitation(d->ui.configExWL->value(), d->ui.configExBW->value());
        config.SetEmission(d->ui.configEmWL->value(), d->ui.configEmBW->value());
        config.SetIntensity(d->ui.configIntensity->value());
        config.SetExposure(d->ui.configExposure->value());
        config.SetGain(d->ui.configGain->value());
        d->panel->SetFLChannel(channelIndex, config);
    }

    void SingleImagingTestWindow::onGetFLChannelConfig() {
        auto channelIndex = -1;
        if (d->ui.ch1Index->isChecked()) channelIndex = 0;
        else if (d->ui.ch2Index->isChecked()) channelIndex = 1;
        else if (d->ui.ch3Index->isChecked()) channelIndex = 2;

        if (channelIndex == -1) {
            QMessageBox::warning(this, "ChannelIndex == -1", "Please select channel");
            d->ui.ch1Index->setFocus();
            return;
        }

        auto config = d->panel->GetFLChannel(channelIndex);
        QMessageBox::information(this, "Config Information", QString("Channel Index(row): [%1]\n" "Channel Name: [%2]\n" "Excitation: [%3]/[%4]\n" "Emission: [%5]/[%6]\n" "Intensity: [%7]\n" "Exposure: [%8]\n" "Gain: [%9]").arg(channelIndex).arg(config.GetChannelName()).arg(config.GetExcitationWaveLength()).arg(config.GetExcitationBandWidth()).arg(config.GetEmissionWaveLength()).arg(config.GetEmissionBandWidth()).arg(config.GetIntensity()).arg(config.GetExposure()).arg(config.GetGain()));
    }

    void SingleImagingTestWindow::onSetFLChannels() {
        QList<FLChannelConfig> list;
        for (int row = 0; row < d->ui.acqTable->rowCount(); ++row) {
            FLChannelConfig config;
            const auto name = d->ui.acqTable->item(row, 0)->data(Qt::DisplayRole).toString();
            const auto exWL = d->ui.acqTable->item(row, 1)->data(Qt::DisplayRole).toInt();
            const auto exBW = d->ui.acqTable->item(row, 2)->data(Qt::DisplayRole).toInt();
            const auto emWL = d->ui.acqTable->item(row, 3)->data(Qt::DisplayRole).toInt();
            const auto emBW = d->ui.acqTable->item(row, 4)->data(Qt::DisplayRole).toInt();
            const auto inte = d->ui.acqTable->item(row, 5)->data(Qt::DisplayRole).toInt();
            const auto expo = d->ui.acqTable->item(row, 6)->data(Qt::DisplayRole).toInt();
            const auto gain = d->ui.acqTable->item(row, 7)->data(Qt::DisplayRole).toDouble();
            config.SetChannelName(name);
            config.SetExcitation(exWL, exBW);
            config.SetEmission(emWL, emBW);
            config.SetIntensity(inte);
            config.SetExposure(expo);
            config.SetGain(gain);
            list.push_back(config);
        }
        d->panel->SetFLChannels(list);
    }

    void SingleImagingTestWindow::onRecvTableDataChanged(int32_t row, const FLChannelConfig& config) {
        d->ui.testLog->append(QString("Table data changed [%1] - Name[%2] Ex[%3/%4] Em[%5/%6] I[%7] E[%8] G[%9]").arg(row).arg(config.GetChannelName()).arg(config.GetExcitationWaveLength()).arg(config.GetExcitationBandWidth()).arg(config.GetEmissionWaveLength()).arg(config.GetEmissionBandWidth()).arg(config.GetIntensity()).arg(config.GetExposure()).arg(config.GetGain()));
    }

    void SingleImagingTestWindow::onLiveControlChangedValue() {
        auto ch = d->ui.liveCH1->isChecked() ? 0 : d->ui.liveCH2->isChecked() ? 1 : d->ui.liveCH3->isChecked() ? 2 : -1;

        if (ch == -1) {
            qDebug() << "Choose a channel index first";
            return;
        }
        const auto i = d->ui.liveIntensity->value();
        const auto e = d->ui.liveExposure->value();
        const auto g = d->ui.liveGain->value();
        d->panel->ChangeFLConfigValues(ch, i, e, g);
    }

    void SingleImagingTestWindow::onLiveControlImportValueFromTable() {
        auto ch = d->ui.liveCH1->isChecked() ? 0 : d->ui.liveCH2->isChecked() ? 1 : d->ui.liveCH3->isChecked() ? 2 : -1;
        if (ch == -1) {
            qDebug() << "Choose a channel index first";
            return;
        }
        const auto config = d->panel->GetFLChannel(ch);
        d->ui.liveIntensity->setValue(config.GetIntensity());
        d->ui.liveExposure->setValue(config.GetExposure());
        d->ui.liveGain->setValue(config.GetGain());
    }

    void SingleImagingTestWindow::onLiveControlSaturation() {
        // saturation activation
    }

    void SingleImagingTestWindow::onLiveControlBoostPressed() {
        // I, E 최소값, G는 최대값으로 bleaching 없이 형광 탐색할 때 사용
    }

    auto SingleImagingTestWindow::Impl::ConnectZStack() -> void {
        connect(ui.btnGetZStackMode, &QToolButton::clicked, self, &Self::onGetZStackMode);
        connect(ui.btnAutoFocusZ, &QToolButton::toggled, self, &Self::onSetLinkEnable);
        connect(ui.btnSetCurrZ, &QToolButton::clicked, self, &Self::onSetCurrentZ);
        connect(ui.btnSetStepUnit, &QToolButton::clicked, self, &Self::onSetStepUnit);
        connect(ui.btnSetScanParms, &QPushButton::clicked, self, &Self::onSetScanParameter);
        connect(ui.btnGetTop, &QToolButton::clicked, self, &Self::onGetScanTop);
        connect(ui.btnGetBottom, &QToolButton::clicked, self, &Self::onGetScanBottom);
        connect(ui.btnGetStep, &QToolButton::clicked, self, &Self::onGetScanStep);
        connect(ui.btnGetRange, &QToolButton::clicked, self, &Self::onGetScanRange);
        connect(ui.btnGetSlices, &QToolButton::clicked, self, &Self::onGetScanSlices);
        connect(ui.spCurrZ, qOverload<double>(&QDoubleSpinBox::valueChanged), [&](double value) {
            panel->SetCurrentZ(value);
        });
        connect(ui.spHTZFocus, qOverload<double>(&QDoubleSpinBox::valueChanged), self, [=](double val) {
            panel->SetZFocus(val);
        });
        connect(panel, &ImagingConditionPanel::sigApplyFLScanCondition, self, &Self::onRecvApplyFLScanCondtion);
        connect(panel, &ImagingConditionPanel::sigUndoFLScanCondition, self, &Self::onRecvUndoFLScanCondition);
        connect(panel, &ImagingConditionPanel::sigZStackZPosPressed, self, &Self::onRecvChangedZStackZPos);
    }

    auto SingleImagingTestWindow::Impl::Connect() -> void {
        connect(ui.setMaxFOV, &QPushButton::clicked, self, &Self::onSetMaxFOV);
        connect(ui.setFOV, &QPushButton::clicked, self, &Self::onSetFOV);
        connect(ui.getFOV, &QPushButton::clicked, self, &Self::onGetFOV);
        connect(ui.setModalities, &QPushButton::clicked, self, &Self::onSetModalities);
        connect(ui.getModalities, &QPushButton::clicked, self, &Self::onGetModalities);
        connect(ui.setTileActivation, &QPushButton::clicked, self, &Self::onSetTileActivation);
        connect(ui.setOverlap, &QPushButton::clicked, self, &Self::onSetTileOverlap);
        connect(ui.setTileSize, &QPushButton::clicked, self, &Self::onSetTileSize);
        connect(ui.setCenterPos, &QPushButton::clicked, self, &Self::onSetTileCenterPos);

        connect(panel, &ImagingConditionPanel::sigChangeFov, self, &Self::onRecvFov);
        connect(panel, &ImagingConditionPanel::sigChangeActiveModality, self, &Self::onRecvModalities);
        connect(panel, &ImagingConditionPanel::sigAcquirePoint, self, &Self::onRecvAcquirePoint);
        connect(panel, &ImagingConditionPanel::sigAcquireTile, self, &Self::onRecvAcquireTile);
        connect(panel, &ImagingConditionPanel::sigAddAcquisitionPoint, self, &Self::onRecvAddPoint);
        connect(panel, &ImagingConditionPanel::sigAddAcquisitionTile, self, &Self::onRecvAddTile);

        connect(ui.acqTableLock, &QCheckBox::toggled, self, &Self::onSetFLConditionLock);
        connect(ui.ch1Enable, &QCheckBox::toggled, self, &Self::onSetFLChannelEnable);
        connect(ui.ch2Enable, &QCheckBox::toggled, self, &Self::onSetFLChannelEnable);
        connect(ui.ch3Enable, &QCheckBox::toggled, self, &Self::onSetFLChannelEnable);
        connect(ui.setFLChannel, &QPushButton::clicked, self, &Self::onSetFLChannelConfig);
        connect(ui.getFLChannel, &QPushButton::clicked, self, &Self::onGetFLChannelConfig);
        connect(ui.setFLChannels, &QPushButton::clicked, self, &Self::onSetFLChannels);
        connect(ui.clearAcqTable, &QPushButton::clicked, self, &Self::onClearFLConditionTable);

        connect(panel, &ImagingConditionPanel::sigChangeAcquisitionTableData, self, &Self::onRecvTableDataChanged);

        connect(ui.rangeI, &QToolButton::clicked, self, [this] {
            const auto min = ui.configMin->value();
            const auto max = ui.configMax->value();
            panel->SetIntensityRange(min, max);
        });

        connect(ui.rangeE, &QToolButton::clicked, self, [this] {
            const auto min = ui.configMin->value();
            const auto max = ui.configMax->value();
            panel->SetExposureRange(min, max);
        });

        connect(ui.rangeG, &QToolButton::clicked, self, [this] {
            const auto min = ui.configMin->value();
            const auto max = ui.configMax->value();
            panel->SetGainRange(min, max);
        });
    }

    auto SingleImagingTestWindow::Impl::ConnectLiveControl() -> void {
        connect(ui.liveIntensity, qOverload<int32_t>(&QSpinBox::valueChanged), self, &Self::onLiveControlChangedValue);
        connect(ui.liveExposure, qOverload<int32_t>(&QSpinBox::valueChanged), self, &Self::onLiveControlChangedValue);
        connect(ui.liveGain, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onLiveControlChangedValue);
        connect(ui.liveImport, &QPushButton::clicked, self, &Self::onLiveControlImportValueFromTable);
    }
}
