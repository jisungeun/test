#pragma once

#include <memory>
#include <QtWidgets/QMainWindow>

#include "ImagingConditionDefines.h"
#include "FLChannelConfig.h"

namespace HTXpress::AppComponents::ImagingConditionPanel::TEST {
    class SingleImagingTestWindow : public QMainWindow {
        Q_OBJECT
    public:
        using Self = SingleImagingTestWindow;
        explicit SingleImagingTestWindow(QWidget* parent = nullptr);
        ~SingleImagingTestWindow() override;

    public slots:
        void onSetLinkEnable();
        void onSetCurrentZ();
        void onSetScanParameter();
        void onSetStepUnit();
        void onGetZStackMode();
        void onGetScanTop();
        void onGetScanBottom();
        void onGetScanStep();
        void onGetScanRange();
        void onGetScanSlices();
        void onRecvApplyFLScanCondtion();
        void onRecvUndoFLScanCondition();
        void onRecvChangedZStackZPos(double z);

        void onSetMaxFOV();
        void onSetFOV();
        void onGetFOV();
        void onRecvFov(double w, double h);

        void onSetModalities();
        void onGetModalities();
        void onRecvModalities(/*const QList<ModalityType>& modalities*/);

        void onSetTileActivation();
        void onSetTileOverlap();
        void onSetTileSize();
        void onSetTileCenterPos();

        void onRecvAcquirePoint();
        void onRecvAcquireTile();
        void onRecvAddPoint();
        void onRecvAddTile(double w, double h);

        void onClearFLConditionTable();
        void onSetFLConditionLock(bool lock);
        void onSetFLChannelEnable(bool enable);
        void onSetFLChannelConfig();
        void onGetFLChannelConfig();
        void onSetFLChannels();

        void onRecvTableDataChanged(int32_t row, const FLChannelConfig& config);

        void onLiveControlChangedValue();
        void onLiveControlImportValueFromTable();
        void onLiveControlSaturation();
        void onLiveControlBoostPressed();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
