#include "SingleImagingTestWindow.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HTXpress::AppComponents::ImagingConditionPanel::TEST::SingleImagingTestWindow w;
    w.show();
    return a.exec();
}
