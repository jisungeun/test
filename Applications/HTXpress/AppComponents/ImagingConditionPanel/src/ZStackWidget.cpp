﻿#include <QDebug>

#include <MessageDialog.h>

#include "ZStackWidget.h"
#include "ui_ZStackWidget.h"
#include "ZStackWidgetControl.h"
#include "ZStackDiagramWidget.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    using Config = ZStackWidgetControl::ZStackConfig;

    struct ZStackWidget::Impl {
        Self* self{nullptr};
        Ui::ZStackWidget* ui{nullptr};
        ZStackWidgetControl control;
        
        struct {
            QString slice;
        } style;

        enum class StackedWidgetIndex {Default = 0, FLFocus = 1};

        auto Initialize() -> void;
        auto Connect() -> void;
        auto GetZStackConfigFromUI() const->Config;

        auto UpdateZStackMode() -> void;
        auto UpdateDiagram() const -> void;
        auto UpdateSpinBox() const -> void;
        auto UpdateApplyButtonStatus() const -> void;
        auto UpdateSingleStep() -> void;
        auto BlockSpinBoxSignals(bool block) const -> void;

        auto UpdateSpinBoxDecimalPrecision() -> void;
        auto ConvertMMtoUM(double mm) -> double;
    };

    ZStackWidget::ZStackWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->self = this;
        d->ui = new Ui::ZStackWidget();
        d->ui->setupUi(this);
        d->Initialize();
        d->Connect();
        d->ui->lineAboveStepValue->setStyleSheet(QString("QFrame{border:1px solid #394c53;}"));
        d->ui->lineBelowTitle->setStyleSheet(QString("QFrame{border:1px solid #394c53;}"));

        d->style.slice = d->ui->spSlices->styleSheet();
    }

    ZStackWidget::~ZStackWidget() {
        delete d->ui;
    }

    auto ZStackWidget::IsZStackConfigChanged() const -> bool {
        return d->control.IsZStackConfigChanged(d->GetZStackConfigFromUI());
    }

    auto ZStackWidget::ClearZStackSetting() -> void {
        d->ui->defaultMode->setChecked(true);
        d->ui->spTop->setValue(0.0);
        d->ui->spBottom->setValue(0.0);
        d->ui->spFlFocus->setValue(0.0);
        d->ui->spStep->setValue(0.5);
        d->ui->spRange->setValue(0.0);
        d->ui->spSlices->setValue(1);
    }

    auto ZStackWidget::SetZResolutionUnit(double zResUnit) -> void {
        d->control.SetZResolution(zResUnit);
        d->ui->spStep->setMinimum(zResUnit);

        d->UpdateSpinBoxDecimalPrecision();

        d->BlockSpinBoxSignals(true);
        d->UpdateSpinBox();
        d->UpdateDiagram();
        d->UpdateApplyButtonStatus();
        d->BlockSpinBoxSignals(false);
    }

    auto ZStackWidget::SetHTZFocus(double htzMM) -> void {
        d->control.SetHTZFocus(d->ConvertMMtoUM(htzMM));
        d->UpdateDiagram();
    }

    auto ZStackWidget::SetCurrentZ(double currZMM) -> void {
        d->control.SetCurrentZPos(d->ConvertMMtoUM(currZMM));
    }

    auto ZStackWidget::SetStep(double stepInUm) -> void {
        d->control.SetStep(stepInUm);

        d->BlockSpinBoxSignals(true);
        d->UpdateSpinBox();
        d->control.SetCurrentZStackConfig(d->GetZStackConfigFromUI());
        d->UpdateApplyButtonStatus();
        d->UpdateSingleStep();
        d->BlockSpinBoxSignals(false);
    }

    auto ZStackWidget::SetFLZStackValues(FLZStackMode mode, double topInUm, double bottomInUm, double flOffsetInUm, double stepInUm) -> void {
        d->control.SetMode(mode);

        switch(mode) {
            case FLZStackMode::Default:
                d->control.SetUserInputTop(topInUm);
                d->control.SetUserInputBottom(bottomInUm);
                break;
            case FLZStackMode::FLFocus:
                d->control.SetUserInputRange(topInUm-bottomInUm);
                d->control.SetUserInputFLFocus(flOffsetInUm);
                break;
        }

        d->control.SetTop(topInUm);
        d->control.SetBottom(bottomInUm);
        d->control.SetStep(stepInUm);

        d->BlockSpinBoxSignals(true);
        d->UpdateZStackMode();
        d->UpdateSpinBox();
        d->UpdateDiagram();
        d->control.SetCurrentZStackConfig(d->GetZStackConfigFromUI());
        d->UpdateApplyButtonStatus();
        d->UpdateSingleStep();
        d->BlockSpinBoxSignals(false);
    }

    auto ZStackWidget::SetFLMaximumSlices(int32_t slices) -> void {
        d->control.SetMaximumSlices(slices);
    }

    auto ZStackWidget::GetZStackMode() const -> FLZStackMode {
        return d->control.GetMode();
    }

    auto ZStackWidget::GetFLScanTop() const -> double {
        return d->control.GetTop();
    }

    auto ZStackWidget::GetFLScanBottom() const -> double {
        return d->control.GetBottom();
    }

    auto ZStackWidget::GetFLScanStep() const -> double {
        return d->control.GetStep();
    }

    auto ZStackWidget::GetFLScanSlices() const -> uint32_t {
        return d->control.GetSlices();
    }

    auto ZStackWidget::GetFLFocusOffset() const -> double {
        return d->control.GetFLFocus();
    }

    auto ZStackWidget::SetSafeZRange(double minMM, double maxMM) -> void {
        // TODO safe z range 범위 설정 후 z value가 범위를 벗어나는 경우 고려
        Q_UNUSED(minMM)
        Q_UNUSED(maxMM)
    }

    void ZStackWidget::onApplyAllButtonClicked() {
        const auto inRange = (d->control.GetSlices() <= d->control.GetMaximumSlices());
        if(!inRange) {
            TC::MessageDialog::warning(nullptr,
                                       tr("Apply to all"),
                                       tr("The slices can't be larger than %1.").arg(d->control.GetMaximumSlices()));
            return;
        }

        const auto answer = TC::MessageDialog::question(nullptr, 
                                                        tr("Apply to all"), 
                                                        tr("The Z stack parameter will apply to all the locations across the plate.\n"
                                                           "Do you want to proceed?"));
        if(answer == TC::MessageDialog::StandardButton::Yes) {
            d->control.SetCurrentZStackConfig(d->GetZStackConfigFromUI());
            d->UpdateApplyButtonStatus();
            emit sigApplyFLScanCondition();
        } else {
            TC::MessageDialog::information(nullptr, tr("Cancel"), tr("Not applied."));
        }
    }

    void ZStackWidget::onUndoButtonClicked() {
        emit sigUndoFLScanCondition();
    }

    void ZStackWidget::onDiagramPosButtonClicked(double z) {
        emit sigZStackZPosPressed(z);
    }

    void ZStackWidget::onSetTopButtonClicked() {
        double topVal = 0.;
        double botVal = 0.;
        if(!d->control.IsTopValueValid(topVal, botVal)) {
            TC::MessageDialog::warning(this, 
                                 tr("Failed to set the Top position."), 
                                 tr("Top and bottom values not set correctly: The current position [%1] is lower than the Bottom position [%2].")
                                 .arg(d->control.GetCurrentZPosInMM()).arg(d->ui->diagram->GetAbsoluteBottom()));
            return;
        }

        d->control.SetTopToDiffValueBetweenCurrZandHTZ();

        d->BlockSpinBoxSignals(true);
        d->UpdateSpinBox();
        d->UpdateDiagram();
        d->UpdateApplyButtonStatus();
        d->BlockSpinBoxSignals(false);
    }

    void ZStackWidget::onSetBottomButtonClicked() {
        double topVal = 0.;
        double botVal = 0.;
        if(!d->control.IsBottomValueValid(topVal, botVal)) {
            TC::MessageDialog::warning(this, 
                                 tr("Failed to set the Bottom position."), 
                                 tr("Top and bottom values not set correctly: The current position [%1] is higher than the Top position [%2].")
                                 .arg(d->control.GetCurrentZPosInMM()).arg(d->ui->diagram->GetAbsoluteTop()));
            return;
        }

        d->control.SetBottomToDiffValueBetweenCurrZandHTZ();

        d->BlockSpinBoxSignals(true);
        d->UpdateSpinBox();
        d->UpdateDiagram();
        d->UpdateApplyButtonStatus();
        d->BlockSpinBoxSignals(false);
    }

    void ZStackWidget::onSetFLFocusButtonClicked() {
        d->control.SetFLFocusToDiffValueBetweenCurrZandHTZ();

        d->BlockSpinBoxSignals(true);
        d->UpdateSpinBox();
        d->UpdateDiagram();
        d->UpdateApplyButtonStatus();
        d->BlockSpinBoxSignals(false);
    }

    void ZStackWidget::onDefaultModeButtonToggled(bool defaultModeOn) {
        if(defaultModeOn) {
            d->control.SetMode(FLZStackMode::Default);
        } else {
            d->control.SetMode(FLZStackMode::FLFocus);
        }

        d->BlockSpinBoxSignals(true);
        d->UpdateZStackMode();
        d->UpdateSpinBox();
        d->UpdateDiagram();
        d->UpdateApplyButtonStatus();
        d->BlockSpinBoxSignals(false);
    }

    void ZStackWidget::onTopValueChanged(double val) {
        d->control.SetUserInputTop(val);
        d->control.SetTop(val);
        d->BlockSpinBoxSignals(true);
        d->UpdateSpinBox();
        d->UpdateDiagram();
        d->UpdateApplyButtonStatus();
        d->BlockSpinBoxSignals(false);
    }

    void ZStackWidget::onBottomValueChanged(double val) {
        d->control.SetUserInputBottom(val);
        d->control.SetBottom(val);
        d->BlockSpinBoxSignals(true);
        d->UpdateSpinBox();
        d->UpdateDiagram();
        d->UpdateApplyButtonStatus();
        d->BlockSpinBoxSignals(false);
    }

    void ZStackWidget::onFLValueChanged(double val) {
        d->control.SetUserInputFLFocus(val);
        d->control.SetFLFocus(val);
        d->BlockSpinBoxSignals(true);
        d->UpdateSpinBox();
        d->UpdateDiagram();
        d->UpdateApplyButtonStatus();
        d->BlockSpinBoxSignals(false);
    }

    void ZStackWidget::onStepValueChanged(double val) {
        d->control.SetStep(val);
        d->BlockSpinBoxSignals(true);
        d->UpdateSpinBox();
        d->UpdateDiagram();
        d->UpdateApplyButtonStatus();
        d->UpdateSingleStep();
        d->BlockSpinBoxSignals(false);
    }

    void ZStackWidget::onRangeValueChanged(double val) {
        d->control.SetUserInputRange(val);
        d->control.SetRange(val);
        d->BlockSpinBoxSignals(true);
        d->UpdateSpinBox();
        d->UpdateDiagram();
        d->UpdateApplyButtonStatus();
        d->BlockSpinBoxSignals(false);
    }

    auto ZStackWidget::Impl::Initialize() -> void {
        ui->label_9->setObjectName("label-h2");  // FL Z-stack setting title label
        ui->label_2->setObjectName("label-h7"); // top value unit label
        ui->label_3->setObjectName("label-h7"); // bottom value unit label
        ui->label_8->setObjectName("label-h7"); // focus value unit label
        ui->label->setObjectName("label-h5");   // step label
        ui->label_4->setObjectName("label-h7");   // step unit label
        ui->label_6->setObjectName("label-h5");   // range label
        ui->label_7->setObjectName("label-h7");   // range unit label
        ui->label_5->setObjectName("label-h5");   // slices label
        ui->applyBtn->setObjectName("tc-enabled-highlight");
        ui->applyBtn->setIconSize(QSize(16,16));
        ui->applyBtn->AddIcon(":/img/ic-check.svg");
        ui->applyBtn->AddIcon(":/img/ic-check-d.svg", QIcon::Disabled);
        ui->undoBtn->setObjectName("bt-light");
        ui->undoBtn->setIconSize(QSize(16,16));
        ui->undoBtn->AddIcon(":/img/ic-undo.svg");
        ui->undoBtn->AddIcon(":/img/ic-undo-d.svg", QIcon::Disabled);

        // init으로 default mode
        // fl focus 모드와 range symbol이 다르다.
        // zstack diagram fl focus button이 hide 상태이다.

        ui->defaultMode->setChecked(true);
        ui->spRange->setEnabled(false);
        ui->spSlices->setEnabled(false);

        const auto initSingleStep = control.GetZResolution();
        ui->spTop->setSingleStep(initSingleStep);
        ui->spBottom->setSingleStep(initSingleStep);
        ui->spFlFocus->setSingleStep(initSingleStep);
        ui->spStep->setSingleStep(initSingleStep);
        ui->spStep->setMinimum(initSingleStep);
        ui->spRange->setSingleStep(initSingleStep*2);

        ui->spTop->setRange(-9999999, 9999999);
        ui->spBottom->setRange(-9999999, 9999999);
        ui->spFlFocus->setRange(-9999999, 9999999);
        ui->spStep->setRange(0, 9999999);
        ui->spRange->setRange(0, 9999999);
        ui->spSlices->setRange(0, 9999999);

        control.SetCurrentZStackConfig(GetZStackConfigFromUI());
        UpdateApplyButtonStatus();
    }

    auto ZStackWidget::Impl::Connect() -> void {
        connect(ui->defaultMode, &QRadioButton::toggled, self, &Self::onDefaultModeButtonToggled);

        connect(ui->spTop, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onTopValueChanged);
        connect(ui->spBottom, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onBottomValueChanged);
        connect(ui->spFlFocus, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onFLValueChanged);
        connect(ui->spStep, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onStepValueChanged);
        connect(ui->spRange, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onRangeValueChanged);

        connect(ui->diagram, &ZStackDiagramWidget::sigDiagramFocusItemPressed, self, &Self::onDiagramPosButtonClicked);
        connect(ui->applyBtn, &QPushButton::clicked, self, &Self::onApplyAllButtonClicked);
        connect(ui->undoBtn, &QPushButton::clicked, self, &Self::onUndoButtonClicked);

        connect(ui->setTopBtn, &QPushButton::clicked, self, &Self::onSetTopButtonClicked);
        connect(ui->setBottomBtn, &QPushButton::clicked, self, &Self::onSetBottomButtonClicked);
        connect(ui->setFlFocusBtn, &QPushButton::clicked, self, &Self::onSetFLFocusButtonClicked);
    }

    auto ZStackWidget::Impl::GetZStackConfigFromUI() const -> Config {
        Config config;
        config.mode = ui->flFocusMode->isChecked() ? FLZStackMode::FLFocus : FLZStackMode::Default;
        config.top = ui->spTop->value();
        config.bottom = ui->spBottom->value();
        config.flfocus = ui->spFlFocus->value();
        config.step = ui->spStep->value();
        config.slices = ui->spSlices->value();
        return config;

    }

    auto ZStackWidget::Impl::UpdateZStackMode() -> void {
        if(control.GetMode() == +FLZStackMode::Default) {
            ui->diagram->SetMode(FLZStackMode::Default);
            ui->defaultMode->setChecked(true);
            ui->stackedWidget->setCurrentIndex(static_cast<int32_t>(StackedWidgetIndex::Default));
            ui->spRange->setEnabled(false);
        } else {
            ui->diagram->SetMode(FLZStackMode::FLFocus);
            ui->flFocusMode->setChecked(true);
            ui->stackedWidget->setCurrentIndex(static_cast<int32_t>(StackedWidgetIndex::FLFocus));
            ui->spRange->setEnabled(true);
        }
    }

    auto ZStackWidget::Impl::UpdateDiagram() const -> void {
        ui->diagram->updateGeometry();
        ui->diagram->SetAbsoluteHTZfocus(control.GetHTZFocusUM());
        ui->diagram->SetRelativeTop(control.GetTop());
        ui->diagram->SetRelativeBottom(control.GetBottom());
        ui->diagram->SetRelativeFLFocus(control.GetFLFocus());
        ui->diagram->update();
    }

    auto ZStackWidget::Impl::UpdateSpinBox() const -> void {
        ui->spTop->setMinimum(ui->spBottom->value());
        ui->spBottom->setMaximum(ui->spTop->value());
        ui->spTop->setValue(control.GetTop());
        ui->spBottom->setValue(control.GetBottom());
        ui->spFlFocus->setValue(control.GetFLFocus());
        ui->spStep->setValue(control.GetStep());
        ui->spRange->setValue(control.GetRange());
        ui->spSlices->setValue(control.GetSlices());

        bool slicesOutOfRange = (control.GetSlices() > control.GetMaximumSlices());
        if(slicesOutOfRange) ui->spSlices->setStyleSheet("background: #FC686C");
        else ui->spSlices->setStyleSheet(style.slice);

        ui->spTop->setToolTip(QString::number(control.GetTop()));
        ui->spBottom->setToolTip(QString::number(control.GetBottom()));
        ui->spFlFocus->setToolTip(QString::number(control.GetFLFocus()));
        ui->spStep->setToolTip(QString::number(control.GetStep()));
        ui->spRange->setToolTip(QString::number(control.GetRange()));

        if(slicesOutOfRange) {
            ui->spSlices->setToolTip(tr("The maximum # of slices is %1").arg(control.GetMaximumSlices()));
        } else {
            ui->spSlices->setToolTip(QString::number(control.GetSlices()));
        }
    }
    
    auto ZStackWidget::Impl::UpdateApplyButtonStatus() const -> void {
        ui->applyBtn->SetEnabled(control.IsZStackConfigChanged(GetZStackConfigFromUI()));
    }

    auto ZStackWidget::Impl::UpdateSingleStep() -> void {
        auto singleStep = control.GetStep();
        ui->spTop->setSingleStep(singleStep);
        ui->spBottom->setSingleStep(singleStep);
        ui->spFlFocus->setSingleStep(singleStep);
        ui->spRange->setSingleStep(singleStep*2);
    }

    auto ZStackWidget::Impl::BlockSpinBoxSignals(bool block) const -> void {
        ui->spTop->blockSignals(block);
        ui->spBottom->blockSignals(block);
        ui->spFlFocus->blockSignals(block);
        ui->spStep->blockSignals(block);
        ui->spRange->blockSignals(block);
        ui->spSlices->blockSignals(block);
    }

    auto ZStackWidget::Impl::UpdateSpinBoxDecimalPrecision() -> void {
        const auto decimal = control.GetDeciamlPrecision();
        ui->spStep->setDecimals(decimal);
        ui->spTop->setDecimals(decimal);
        ui->spBottom->setDecimals(decimal);
        ui->spFlFocus->setDecimals(decimal);
        ui->spRange->setDecimals(decimal);
    }

    auto ZStackWidget::Impl::ConvertMMtoUM(double mm) -> double {
        return mm*1000;
    }
}
