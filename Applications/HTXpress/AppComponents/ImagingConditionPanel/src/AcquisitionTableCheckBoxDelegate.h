﻿#pragma once

#include <QItemDelegate>

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class AcquisitionTableCheckBoxDelegate final : public QItemDelegate {
        Q_OBJECT
    public:
        explicit AcquisitionTableCheckBoxDelegate(QObject* parent = nullptr);
        ~AcquisitionTableCheckBoxDelegate() override;

        auto paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const -> void override;
        auto editorEvent(QEvent* event, QAbstractItemModel* model, const QStyleOptionViewItem& option, const QModelIndex& index) -> bool override;

    signals:
        void sigCheckBoxClicked(int32_t row);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
