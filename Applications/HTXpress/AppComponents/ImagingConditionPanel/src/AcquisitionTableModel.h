﻿#pragma once

#include <memory>
#include <QAbstractTableModel>

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class AcquisitionTableModel : public QAbstractTableModel {
        Q_OBJECT
    public:
        enum Columns { Enable=0, Name, Excitation, Emission, Intensity, Exposure, Gain, Clear, _NumOfCols };

        struct Light {
            int32_t wavelen; // wavelength
            int32_t bw; // bandwidth
        };
        struct Data {
            bool enable{false};
            QString channel{""};
            Light excitation{0, 0};
            Light emission{0, 0};
            int32_t intensity{0};
            int32_t exposure{0};
            double gain{0.0};
        };

        using Self = AcquisitionTableModel;
        using Pointer = std::shared_ptr<Self>;

        explicit AcquisitionTableModel(QObject* parent = nullptr);
        ~AcquisitionTableModel() override;

        auto rowCount(const QModelIndex& parent = QModelIndex()) const -> int override;
        auto columnCount(const QModelIndex& parent = QModelIndex()) const -> int override;

        auto headerData(int section, Qt::Orientation orientation, int role) const -> QVariant override;
        auto data(const QModelIndex& index, int role) const -> QVariant override;
        auto setData(const QModelIndex& index, const QVariant& value, int role) -> bool override;

        auto flags(const QModelIndex& index) const -> Qt::ItemFlags override;

        auto TableData() const -> QList<Data>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
Q_DECLARE_METATYPE(HTXpress::AppComponents::ImagingConditionPanel::AcquisitionTableModel::Light)

