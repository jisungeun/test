﻿#include <QDoubleSpinBox>
#include <QPushButton>
#include <QKeyEvent>

#include "AcquisitionTableSlider.h"
#include "ui_AcquisitionTableSlider.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    struct AcquisitionTableSlider::Impl {
        Ui::AcquisitionTableSlider ui;
        SliderType type;
        int32_t row{-1};
        double initValue;

        QString windowTitle = "";

        double spinVal = 0.0;
        double spinMin = 0.0;
        double spinMax = 100.0;

        const int32_t kDoubleValSlideRatio = 10;
    };

    AcquisitionTableSlider::AcquisitionTableSlider(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui.setupUi(this);

        this->setAttribute(Qt::WA_DeleteOnClose);
        this->setWindowFlag(Qt::Popup, true);
        this->setWindowFlag(Qt::WindowCloseButtonHint, false);
        this->setWindowFlag(Qt::WindowStaysOnTopHint, false);
        this->setWindowFlag(Qt::MSWindowsFixedSizeDialogHint, false);
    }

    AcquisitionTableSlider::~AcquisitionTableSlider() {
    }

    auto AcquisitionTableSlider::SetRow(int32_t row) -> void {
        d->row = row;
    }

    auto AcquisitionTableSlider::SetSliderType(SliderType type) -> void {
        d->type = type;
    }

    auto AcquisitionTableSlider::SetCurrentValue(double value) -> void {
        d->initValue = value;
    }

    auto AcquisitionTableSlider::SetRange(double min, double max) -> void {
        d->spinMin = min;
        d->spinMax = max;

        d->ui.minLabel->setText(QString::number(min));
        d->ui.maxLabel->setText(QString::number(max));
    }

    auto AcquisitionTableSlider::Initialize() -> void {
        QString windowTitle = "";

        int32_t sliderVal = d->initValue;
        int32_t sliderMin = d->spinMin;
        int32_t sliderMax = d->spinMax;
        int32_t sliderStep = 1;

        int32_t spinbDecimals = 0;
        double spinStep = 1.0;

        switch (d->type) {
            case Intensity: d->spinVal = static_cast<int32_t>(d->initValue);
                windowTitle = QString("Intensity (CH%1)").arg(d->row + 1);
                break;
            case Exposure: d->spinVal = static_cast<int32_t>(d->initValue);
                sliderStep = spinStep = 10;
                windowTitle = QString("Exposure (CH%1)").arg(d->row + 1);
                break;
            case Gain: d->spinVal = d->initValue;
                spinStep = 0.1;
                spinbDecimals = 1;

                sliderVal = d->initValue * d->kDoubleValSlideRatio;
                sliderMin = d->spinMin * d->kDoubleValSlideRatio;
                sliderMax = d->spinMax * d->kDoubleValSlideRatio;
                sliderStep = 0.1 * d->kDoubleValSlideRatio;

                windowTitle = QString("Gain (CH%1)").arg(d->row + 1);
                break;
        }

        d->ui.slider->setRange(sliderMin, sliderMax);
        d->ui.slider->setPageStep(sliderStep);
        d->ui.slider->setSingleStep(sliderStep);
        d->ui.slider->setTickInterval(sliderMax / 10);
        d->ui.slider->setTickPosition(QSlider::NoTicks);
        d->ui.slider->setValue(sliderVal);

        d->ui.spinbox->setRange(d->spinMin, d->spinMax);
        d->ui.spinbox->setSingleStep(spinStep);
        d->ui.spinbox->setDecimals(spinbDecimals);
        d->ui.spinbox->setValue(d->spinVal);

        connect(d->ui.slider, &QSlider::valueChanged, this, &Self::onSliderValueChanged);
        connect(d->ui.spinbox, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &Self::onDoubleSpinBoxValueChanged);

        setWindowTitle(windowTitle);
        d->ui.spinbox->setFocus();
        d->ui.spinbox->selectAll();
    }

    auto AcquisitionTableSlider::GetValue() const -> QVariant {
        return d->ui.spinbox->value();
    }

    auto AcquisitionTableSlider::keyPressEvent(QKeyEvent* event) -> void {
        if(event->key() == Qt::Key_Return) {
            this->close();
            return;
        }
        QWidget::keyPressEvent(event);
    }

    void AcquisitionTableSlider::onSliderValueChanged(int value) {
        d->ui.spinbox->blockSignals(true);
        if (d->type == Gain) {
            const double doubleValue = value / 10.0;
            d->ui.spinbox->setValue(doubleValue);
            emit sigValueChanged(doubleValue);
        }
        else {
            d->ui.spinbox->setValue(value);
            emit sigValueChanged(value);
        }
        d->ui.spinbox->blockSignals(false);
    }

    void AcquisitionTableSlider::onDoubleSpinBoxValueChanged(double value) {
        d->ui.slider->blockSignals(true);
        if (d->type == Gain) {
            d->ui.slider->setValue(value * d->kDoubleValSlideRatio);
        }
        else {
            d->ui.slider->setValue(value);
        }
        emit sigValueChanged(value);
        d->ui.slider->blockSignals(false);
    }
}
