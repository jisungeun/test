﻿#pragma once

#include <memory>

#include <QList>

#include "FLChannelConfig.h"
#include "ImagingConditionDefines.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class ImagingConditionPanelControl {
    public:
        ImagingConditionPanelControl();
        ~ImagingConditionPanelControl();

        auto SetMaximumFOV(double xInUm, double yInUm) -> void;
        auto GetMaximumFovWidth() const -> double;
        auto GetMaximumFovHeight() const -> double;
        auto GetFovRangeSmallerValue() const -> double;

        auto SetFovSize(double xInUM, double yInUM) -> void;
        auto SetFovWidth(double xInUM) -> void;
        auto SetFovHeight(double yInUM) -> void;
        auto GetFovWidth() const -> double;
        auto GetFovHeight() const -> double;
        auto GetFovSmallerValue() const -> double;

        auto SetTileSize(double xInUM, double yInUM) -> void;
        auto SetTileWidth(double xInUM) -> void;
        auto SetTileHeight(double yInUM) -> void;
        auto GetTileWidth() const -> double;
        auto GetTileHeight() const -> double;

        auto SetOverlap(double horizontal, double vertical) -> void;
        auto GetHorizontalOverlap() const -> double;
        auto GetVerticalOverlap() const -> double;

        auto SetFLChannels(const QList<FLChannelConfig>& list) -> void;
        auto GetFLChannels() const -> QList<FLChannelConfig>;

        auto SetFLChanenlIndexInUse(const QList<int32_t>& indices) const -> void;
        auto IsFLChannelInUse(const int32_t& channelIndex) const -> bool;

        auto ChangeIntensity(const QString& name, int32_t intensity) -> void;
        auto ChangeExposure(const QString& name, int32_t exposure) -> void;
        auto ChangeGain(const QString& name, double gain) -> void;

        auto GetFLChannelNames() -> QStringList;
        auto GetFLChannelConfig(const QString& name) -> FLChannelConfig;

        auto GetTileCountRowColumn()const->std::tuple<int32_t, int32_t>;

        auto SetIntensityRange(int32_t min, int32_t max) -> void;
        auto GetIntensityMin() const -> int32_t;
        auto GetIntensityMax() const -> int32_t;

        auto SetExposureRange(int32_t min, int32_t max) -> void;
        auto GetExposureMin() const -> int32_t;
        auto GetExposureMax() const -> int32_t;

        auto SetGainRange(double min, double max) -> void;
        auto GetGainMin() const -> double;
        auto GetGainMax() const -> double;

        auto SetSafeMovingRange(Axis axis, double min, double max) -> void;
        auto GetSafeMovingValue(Axis axis, double input) const -> double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
