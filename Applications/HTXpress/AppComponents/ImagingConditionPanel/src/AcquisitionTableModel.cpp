﻿#include "AcquisitionTableModel.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    struct AcquisitionTableModel::Impl {
        QVector<Data> data;
    };

    AcquisitionTableModel::AcquisitionTableModel(QObject* parent) : QAbstractTableModel(parent), d{std::make_unique<Impl>()} {
        d->data.resize(3);
    }

    AcquisitionTableModel::~AcquisitionTableModel() {
    }

    auto AcquisitionTableModel::rowCount(const QModelIndex& parent) const -> int {
        Q_UNUSED(parent)
        return 3;
    }

    auto AcquisitionTableModel::columnCount(const QModelIndex& parent) const -> int {
        Q_UNUSED(parent)
        return _NumOfCols;
    }

    auto AcquisitionTableModel::headerData(int section, Qt::Orientation orientation, int role) const -> QVariant {
        if(role == Qt::DisplayRole) {
            if (orientation == Qt::Horizontal) {
                const auto column = static_cast<Columns>(section);
                switch (column) {
                    case Enable: return "  ";
                    case Name: return "Channel";
                    case Excitation: return "Excitation";
                    case Emission: return "Emission";
                    case Intensity: return "Intensity";
                    case Exposure: return "Exposure";
                    case Gain: return "Gain";
                    case Clear: return"";
                    default: ;
                }
            }

            if (orientation == Qt::Vertical) {
                return QString("Ch%1").arg(section + 1);
            }
        }

        if(role == Qt::TextAlignmentRole) {
            if(orientation == Qt::Vertical) {
                return Qt::AlignCenter;
            }
        }

        return QVariant();
    }

    auto AcquisitionTableModel::data(const QModelIndex& index, int role) const -> QVariant {
        if (!index.isValid()) return QVariant();
        if (index.row() >= d->data.size() || index.row() < 0) return QVariant();

        const auto row = index.row();
        const auto col = static_cast<Columns>(index.column());
        const auto& data = d->data.at(row);

        if (role == Qt::DisplayRole) {
            switch (col) {
                case Enable: return data.enable;
                case Name: return data.channel;
                case Excitation: 
                    if(data.channel.isEmpty()) {
                        return "";
                    }
                    return QString("%1/%2").arg(data.excitation.wavelen).arg(data.excitation.bw);
                case Emission: 
                    if(data.channel.isEmpty()) {
                        return "";
                    }
                    return QString("%1/%2").arg(data.emission.wavelen).arg(data.emission.bw);
                case Intensity: 
                    if(data.channel.isEmpty()) {
                        return "";
                    }
                    return data.intensity;
                case Exposure: 
                    if(data.channel.isEmpty()) {
                        return "";
                    }
                    return data.exposure;
                case Gain: 
                    if(data.channel.isEmpty()) {
                        return "";
                    }
                    return data.gain;
                default: return QVariant();
            }
        }

        if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }

        if(role == Qt::ToolTipRole) {
            switch(col) {
                case Name: return data.channel;
                case Excitation: return QString("%1/%2").arg(data.excitation.wavelen).arg(data.excitation.bw);
                case Emission: return QString("%1/%2").arg(data.emission.wavelen).arg(data.emission.bw);
                case Intensity: return data.intensity;
                case Exposure: return data.exposure;
                case Gain: return data.gain;
                default: return QVariant();
            }
        }

        return QVariant();
    }

    auto AcquisitionTableModel::setData(const QModelIndex& index, const QVariant& value, int role) -> bool {
        if (index.isValid() && role == Qt::EditRole) {
            const auto row = index.row();
            const auto col = static_cast<Columns>(index.column());
            auto data = d->data.value(row);

            switch (col) {
                case Enable: data.enable = value.toBool();
                    break;
                case Name: data.channel = value.toString();
                    break;
                case Excitation: {
                    const auto light = value.value<Light>();
                    data.excitation.wavelen = light.wavelen;
                    data.excitation.bw = light.bw;
                    break;
                }
                case Emission: {
                    const auto light = value.value<Light>();
                    data.emission.wavelen = light.wavelen;
                    data.emission.bw = light.bw;
                    break;
                }
                case Intensity: data.intensity = value.toInt();
                    break;
                case Exposure: data.exposure = value.toInt();
                    break;
                case Gain: data.gain = value.toDouble();
                    break;
                default: 
                    break;
            }

            d->data.replace(row, data);
            emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});
            return true;
        }

        return false;
    }

    auto AcquisitionTableModel::flags(const QModelIndex& index) const -> Qt::ItemFlags {
        if (!index.isValid()) return Qt::NoItemFlags;

        const auto& column = static_cast<Columns>(index.column());

        if (d->data[index.row()].enable == false) {
            switch (column) {
                case Name:
                case Excitation:
                case Emission:
                case Intensity:
                case Exposure:
                case Gain:
                case Clear: return Qt::NoItemFlags;
                default: ;
            }
        }
        else {
            switch(column) {
            case Clear: return Qt::NoItemFlags;
            default: ;
            }
        }
        

        return QAbstractTableModel::flags(index);
    }

    auto AcquisitionTableModel::TableData() const -> QList<Data> {
        return d->data.toList();
    }
}
