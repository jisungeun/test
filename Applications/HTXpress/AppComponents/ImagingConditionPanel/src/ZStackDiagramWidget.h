﻿#pragma once

#include <QWidget>

#include "ImagingConditionDefines.h"
#include "ZStackDiagramDefines.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class ZStackDiagramWidget : public QWidget {
        Q_OBJECT
	public:
        using Self = ZStackDiagramWidget;
        using Pointer = std::shared_ptr<Self>;

        explicit ZStackDiagramWidget(QWidget* parent = nullptr);
        ~ZStackDiagramWidget() override;

        auto SetMode(FLZStackMode mode) -> void;
        auto SetRelativeTop(double top) -> void;
        auto SetRelativeBottom(double bottom) -> void;
        auto SetRelativeFLFocus(double fl) -> void;
        auto SetAbsoluteHTZfocus(double htzUM) -> void;

        auto GetAbsoluteTop() const -> double;
        auto GetAbsoluteBottom() const -> double;

    protected:
        auto paintEvent(QPaintEvent* event) -> void override;

    signals:
        void sigDiagramFocusItemPressed(double z);

    private slots:
        void onDiagramItemClicked(DiagramType type);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
