﻿#pragma once
#pragma once

#include <enum.h>

#include <QColor>

namespace HTXpress::AppComponents::ImagingConditionPanel {
    const QColor odDiagramText("#172023"); // darkgray
    const QColor odDiagramVerticalLine("#5F6F7A"); // gray
    const QColor odDiagramFocusFL("#FFE900"); // gold
    const QColor odDiagramFocusTB("#27A2BF"); // skyblue
    const QColor odDiagramFocusHT("#00FF00"); // green

    BETTER_ENUM(DiagramType, int32_t,
                Top,
                Bottom,
                HTZ,
                FL)
}
