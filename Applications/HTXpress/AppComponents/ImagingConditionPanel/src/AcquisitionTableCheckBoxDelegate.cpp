﻿#include <QCheckBox>
#include <QTableView>
#include <QMouseEvent>
#include <QPainter>
#include <QApplication>

#include "AcquisitionTableCheckBoxDelegate.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    struct AcquisitionTableCheckBoxDelegate::Impl {
        const QColor oddRowColor{"#1B2629"};
        const QColor evenRowColor{"#243135"};

        [[nodiscard]] auto GetCheckboxRect(const QStyleOptionViewItem& option) const -> QRect;
    };

    auto AcquisitionTableCheckBoxDelegate::Impl::GetCheckboxRect(const QStyleOptionViewItem& option) const -> QRect {
        QStyleOptionButton opt;
        opt.QStyleOption::operator=(option);
        const QRect chkRect = QApplication::style()->subElementRect(QStyle::SE_ItemViewItemCheckIndicator, &opt);

        QRect rect = option.rect;
        const int32_t dx = (rect.width() - chkRect.width()) / 2;
        const int32_t dy = (rect.height() - chkRect.height()) / 2;

        rect.setTopLeft(rect.topLeft() + QPoint(dx, dy));
        rect.setWidth(chkRect.width());
        rect.setHeight(chkRect.height());

        return rect;
    }

    AcquisitionTableCheckBoxDelegate::AcquisitionTableCheckBoxDelegate(QObject* parent) : QItemDelegate(parent), d{std::make_unique<Impl>()} {
    }

    AcquisitionTableCheckBoxDelegate::~AcquisitionTableCheckBoxDelegate() = default;

    auto AcquisitionTableCheckBoxDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const -> void {
        if (index.row() % 2 == 0) painter->fillRect(option.rect, d->evenRowColor);
        else painter->fillRect(option.rect, d->oddRowColor);

        auto chkOption = option;
        chkOption.rect = d->GetCheckboxRect(option);

        if (index.data().toBool()) {
            drawCheck(painter, chkOption, chkOption.rect, Qt::Checked);
        }
        else {
            drawCheck(painter, chkOption, chkOption.rect, Qt::Unchecked);
        }

        drawFocus(painter, chkOption, chkOption.rect);
    }

    auto AcquisitionTableCheckBoxDelegate::editorEvent(QEvent* event, QAbstractItemModel* model, const QStyleOptionViewItem& option, const QModelIndex& index) -> bool {
        if (event->type() == QEvent::MouseButtonPress) {
            const auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
            if (mouseEvent->button() == Qt::LeftButton) {
                const auto cbRect = d->GetCheckboxRect(option);
                if (cbRect.contains(mouseEvent->pos())) {
                    emit sigCheckBoxClicked(index.row());
                    return true;
                }
            }
        }

        return QItemDelegate::editorEvent(event, model, option, index);
    }
}
