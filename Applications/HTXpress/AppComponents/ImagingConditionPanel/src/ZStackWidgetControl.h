﻿#pragma once

#include <memory>

#include "ImagingConditionDefines.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class ZStackWidgetControl {
    public:
        using Self = ZStackWidgetControl;
        using Pointer = std::shared_ptr<Self>;

        struct ZStackConfig {
            FLZStackMode mode{FLZStackMode::Default};
            double top{0.0};
            double bottom{0.0};
            double flfocus{0.0};
            double step{0.0};
            int32_t slices{0};

            auto operator==(const ZStackConfig& other) const -> bool {
                auto IsSame = [](const double& d1, const double& d2) -> bool {
                    return fabs(d1 - d2) < std::numeric_limits<double>::epsilon();
                };

                if(mode != other.mode) return false;
                if(slices != other.slices) return false;
                if(!IsSame(top, other.top)) return false;
                if(!IsSame(bottom, other.bottom)) return false;
                if(!IsSame(flfocus, other.flfocus)) return false;
                if(!IsSame(step, other.step)) return false;

                return true;
            }

            auto operator!=(const ZStackConfig& other) const -> bool {
                return !(*this == other);
            }
        };

        explicit ZStackWidgetControl();
        ~ZStackWidgetControl();

        auto SetHTZFocus(double htz) -> void;
        auto GetHTZFocusUM() const -> double;

        auto SetCurrentZPos(double currentZ) -> void;
        auto GetCurrentZPosInUM() const -> double;
        auto GetCurrentZPosInMM() const -> double;

        auto SetZResolution(double zResolution) -> void;
        auto GetZResolution() const -> double;

        auto SetMaximumSlices(int32_t slices) -> void;
        auto GetMaximumSlices() const -> int32_t;

        auto SetMode(FLZStackMode mode) -> void;
        auto GetMode() const->FLZStackMode;
        auto SetTop(double top) -> void;
        auto GetTop() const -> double;
        auto SetBottom(double bottom) -> void;
        auto GetBottom() const -> double;
        auto SetFLFocus(double flFocus) -> void;
        auto GetFLFocus() const -> double;
        auto SetRange(double range) -> void;
        auto GetRange() const -> double;
        auto SetStep(double step) -> void;
        auto GetStep() const -> double;
        auto SetSlices(int32_t slices) -> void;
        auto GetSlices() const->int32_t;

        auto SetCurrentZStackConfig(const ZStackConfig& config) -> void;
        auto GetCurrentZStackConfig() const->ZStackConfig;
        auto IsZStackConfigChanged(const ZStackConfig& config) const -> bool;

        auto SetTopToDiffValueBetweenCurrZandHTZ() -> void;
        auto SetBottomToDiffValueBetweenCurrZandHTZ() -> void;
        auto SetFLFocusToDiffValueBetweenCurrZandHTZ() -> void;

        auto IsTopValueValid(double &topValue, double &botValue) -> bool;
        auto IsBottomValueValid(double& topValue, double &botValue) -> bool;

        auto SetUserInputTop(double top) -> void;
        auto SetUserInputBottom(double bottom) -> void;
        auto SetUserInputFLFocus(double flfocus) -> void;
        auto SetUserInputRange(double range) -> void;

        auto GetDeciamlPrecision() -> int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
