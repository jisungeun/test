﻿#include <QEvent>
#include <QMouseEvent>

#include "AcquisitoinTableEventFilter.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    struct AcquisitoinTableEventFilter::Impl {
        QTableView* table{nullptr};
    };

    AcquisitoinTableEventFilter::AcquisitoinTableEventFilter(QTableView* table, QObject* parent) : QObject(parent), d{std::make_unique<Impl>()} {
        d->table = table;
    }

    AcquisitoinTableEventFilter::~AcquisitoinTableEventFilter() {
    }

    auto AcquisitoinTableEventFilter::eventFilter(QObject* watched, QEvent* event) -> bool {
        if (d->table == nullptr) return QObject::eventFilter(watched, event);

        if (event->type() == QEvent::MouseButtonPress) {
            const auto mouseEvent = static_cast<QMouseEvent*>(event);
            if (!d->table->indexAt(mouseEvent->pos()).isValid()) {
                d->table->setCurrentIndex(QModelIndex());
            }
            return false;
        }

        return false;
    }
}
