﻿#pragma once

#include <memory>

#include <QWidget>

#include "ImagingConditionDefines.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class ZStackWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = ZStackWidget;
        using Pointer = std::shared_ptr<Self>;

        explicit ZStackWidget(QWidget* parent = nullptr);
        ~ZStackWidget() override;

        auto IsZStackConfigChanged() const -> bool;

        auto ClearZStackSetting() -> void;

        auto SetZResolutionUnit(double zResUnit) -> void; // z res unit
        auto SetHTZFocus(double htzMM) -> void;
        auto SetCurrentZ(double currZMM) -> void;
        auto SetStep(double stepInUm) -> void;
        auto SetFLZStackValues(FLZStackMode mode, double topInUm, double bottomInUm, 
                               double foOffsetInUm, double stepInUm) -> void;
        auto SetFLMaximumSlices(int32_t slices) -> void;

        auto GetZStackMode() const -> FLZStackMode; // default mode / FL focus mode
        auto GetFLScanTop() const -> double;
        auto GetFLScanBottom() const -> double;
        auto GetFLScanStep() const -> double;
        auto GetFLScanSlices() const -> uint32_t;
        auto GetFLFocusOffset() const -> double;

        auto SetSafeZRange(double minMM, double maxMM) -> void;

    signals:
        void sigZStackZPosPressed(double z);
        void sigApplyFLScanCondition();
        void sigUndoFLScanCondition();

    private slots:
        void onApplyAllButtonClicked();
        void onUndoButtonClicked();
        void onDiagramPosButtonClicked(double z);

        // zstack set button slots
        void onSetTopButtonClicked();
        void onSetBottomButtonClicked();
        void onSetFLFocusButtonClicked();

        // zstack mode toggle
        void onDefaultModeButtonToggled(bool defaultModeOn);

        // zstack spinbox value changed
        void onTopValueChanged(double val);
        void onBottomValueChanged(double val);
        void onFLValueChanged(double val);
        void onStepValueChanged(double val);
        void onRangeValueChanged(double val);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
