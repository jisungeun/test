﻿#pragma once

#include <memory>

#include <QWidget>

#include "ZStackDiagramDefines.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class ZStackDiagramItem : public QWidget {
        Q_OBJECT
	public:
        using Self = ZStackDiagramItem;
        using Pointer = std::shared_ptr<Self>;

        explicit ZStackDiagramItem(DiagramType type, QWidget* parent = nullptr);
        ~ZStackDiagramItem() override;

        auto SetValues(double relativeValue, double absoluteValue) -> void;
        auto UpdatePosAndSize(int32_t posY, int32_t width) -> void;

    protected:
        auto resizeEvent(QResizeEvent* event) -> void override;

    signals:
        void sigDiagramItemButtonClicked(DiagramType type);

    private slots:
        void onDiagramButtonClicked();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
