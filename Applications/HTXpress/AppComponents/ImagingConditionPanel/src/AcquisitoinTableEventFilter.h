﻿#pragma once

#include <memory>

#include <QObject>
#include <QTableView>

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class AcquisitoinTableEventFilter : public QObject {
        Q_OBJECT
    public:
        AcquisitoinTableEventFilter(QTableView* table, QObject*parent=nullptr);
        ~AcquisitoinTableEventFilter() override;

    protected:
        auto eventFilter(QObject* watched, QEvent* event) -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
