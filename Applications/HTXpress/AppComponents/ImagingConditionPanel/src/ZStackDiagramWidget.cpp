﻿#include <QLabel>
#include <QResizeEvent>

#include "ZStackDiagramWidget.h"
#include "ImagingConditionDefines.h"
#include "ZStackDiagramWidgetControl.h"
#include "ZStackDiagramItem.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    struct ZStackDiagramWidget::Impl {
        QWidget* parent{nullptr};

        ZStackDiagramWidgetControl control;

        ZStackDiagramItem* topItem{nullptr};
        ZStackDiagramItem* bottomItem{nullptr};
        ZStackDiagramItem* htzItem{nullptr};
        ZStackDiagramItem* flItem{nullptr};

        const int32_t offset = 5;

        auto InitItems(Self* self) -> void;
        auto UpdatePosition(int32_t w, int32_t h) const -> void;
    };

    ZStackDiagramWidget::ZStackDiagramWidget(QWidget* parent) : d{std::make_unique<Impl>()} {
        d->parent = parent;
        d->InitItems(this);
    }

    ZStackDiagramWidget::~ZStackDiagramWidget() {
    }

    auto ZStackDiagramWidget::SetMode(FLZStackMode mode) -> void {
        d->control.SetMode(mode);
        const bool flVisible = (d->control.GetMode() == +FLZStackMode::Default ? false : true);
        d->flItem->setVisible(flVisible);
    }

    auto ZStackDiagramWidget::SetRelativeTop(double top) -> void {
        d->control.SetRelativeTop(top);
        d->topItem->SetValues(d->control.GetRelativeTop(), d->control.GetAbsoluteTop());
    }

    auto ZStackDiagramWidget::SetRelativeBottom(double bottom) -> void {
        d->control.SetRelativeBottom(bottom);
        d->bottomItem->SetValues(d->control.GetRelativeBottom(), d->control.GetAbsoluteBottom());
    }

    auto ZStackDiagramWidget::SetRelativeFLFocus(double fl) -> void {
        d->control.SetRelativeFLFocus(fl);
        d->flItem->SetValues(d->control.GetRelativeFLFocus(), d->control.GetAbsoluteFLFocus());
    }

    auto ZStackDiagramWidget::SetAbsoluteHTZfocus(double htzUM) -> void {
        d->control.SetAbsoluteHTZFocus(htzUM);
        d->htzItem->SetValues(d->control.GetRelativeHtzFocus(), d->control.GetAbsoluteHTZFocus());
    }

    auto ZStackDiagramWidget::GetAbsoluteTop() const -> double {
        return d->control.GetAbsoluteTop();
    }

    auto ZStackDiagramWidget::GetAbsoluteBottom() const -> double {
        return d->control.GetAbsoluteBottom();
    }

    auto ZStackDiagramWidget::paintEvent(QPaintEvent* event) -> void {
        Q_UNUSED(event)
        d->UpdatePosition(geometry().width(), geometry().height());
    }

    void ZStackDiagramWidget::onDiagramItemClicked(DiagramType type) {
        double pos = 0.0;
        switch (type) {
        case DiagramType::Top:
            pos = d->control.GetAbsoluteTop();
            break;
        case DiagramType::Bottom:
            pos = d->control.GetAbsoluteBottom();
            break;
        case DiagramType::FL:
            pos = d->control.GetAbsoluteFLFocus();
            break;
        case DiagramType::HTZ:
            pos = d->control.GetAbsoluteHTZFocus();
            break;
        }
        emit sigDiagramFocusItemPressed(pos);
    }

    auto ZStackDiagramWidget::Impl::InitItems(Self* self) -> void {
        topItem = new ZStackDiagramItem(DiagramType::Top, self);
        bottomItem = new ZStackDiagramItem(DiagramType::Bottom, self);
        htzItem = new ZStackDiagramItem(DiagramType::HTZ, self);
        flItem = new ZStackDiagramItem(DiagramType::FL, self);

        connect(topItem, &ZStackDiagramItem::sigDiagramItemButtonClicked, self, &Self::onDiagramItemClicked);
        connect(bottomItem, &ZStackDiagramItem::sigDiagramItemButtonClicked, self, &Self::onDiagramItemClicked);
        connect(htzItem, &ZStackDiagramItem::sigDiagramItemButtonClicked, self, &Self::onDiagramItemClicked);
        connect(flItem, &ZStackDiagramItem::sigDiagramItemButtonClicked, self, &Self::onDiagramItemClicked);

        flItem->setVisible(false);
    }

    auto ZStackDiagramWidget::Impl::UpdatePosition(int32_t w, int32_t h) const -> void {
        const auto itemHeight = topItem->height();

        control.SetWidgetBottomLocOffset(h, itemHeight);

        // TODO set X pos for each item
        topItem->UpdatePosAndSize(control.GetItemPosY(DiagramType::Top), w);
        bottomItem->UpdatePosAndSize(control.GetItemPosY(DiagramType::Bottom), w);
        htzItem->UpdatePosAndSize(control.GetItemPosY(DiagramType::HTZ), w);
        flItem->UpdatePosAndSize(control.GetItemPosY(DiagramType::FL), w);
    }
}