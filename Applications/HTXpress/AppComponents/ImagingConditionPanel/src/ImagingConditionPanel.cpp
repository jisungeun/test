#include <QButtonGroup>
#include <QDebug>
#include <QToolButton>

#include <UIUtility.h>
#include <MessageDialog.h>
#include <InputDialog.h>

#include "ImagingConditionPanel.h"
#include "ui_ImagingConditionPanel.h"
#include "ImagingConditionPanelControl.h"
#include "AcquisitionTableModel.h"
#include "HiddenWidgetSizeFixer.h"
#include "AcquisitoinTableEventFilter.h"
#include "AcquisitionTableCheckBoxDelegate.h"
#include "AcquisitionTableSlider.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    using Column = AcquisitionTableModel::Columns;
    using Light = AcquisitionTableModel::Light;
    using TableData = AcquisitionTableModel::Data;

    struct ImagingConditionPanel::Impl {
        enum class FovModeIndex { Square = 255, Free};
        enum class StackedWidgetIndex { DefaultModeIndex = 0, FLFocusModeIndex = 1 };
        using StackedWidgetIndex = StackedWidgetIndex;

        explicit Impl(ImagingConditionPanel* p) : self(p) {}

        Self* const self{nullptr};
        Ui::ImagingConditionPanel ui;
        ImagingConditionPanelControl control;
        AcquisitionTableModel* acqModel{nullptr};
        AcquisitoinTableEventFilter* tableEventFilter{nullptr}; // for clear selection
        QButtonGroup* modalityGroup{nullptr};
        QPoint lastPopUpPos;

        auto Init() -> void;

        auto Connect() -> void;
        auto ConnectZStack() -> void;

        auto DeselectModality(QAbstractButton* btn) -> void;
        auto SetModalitySingalBlocked(bool block) -> void;

        auto UpdateFLWidgetsVisibility() -> void;

        auto IsColumnHasNoPopup(int32_t column) -> bool;
        auto ConvertTableDataToFLChannelConfig(QList<TableData>::const_reference tableData) -> FLChannelConfig;
        auto IsChannelNameDuplicate(const QString& channelName) -> bool;
        auto IsChannelEmpty(int32_t row) -> bool;
        auto ShowChannelList(const int32_t& row, bool isRecursive = false) -> void;
        auto UpdateTileCount() -> void;
        auto RefreshTable(const int32_t& row) const -> void;
        auto IsAcquireSignalAvailable() const -> bool;
        auto InitTableColumnSizePolicy() const -> void;
        auto ChangeFovRange(FovModeIndex fovModeIndex) -> void;
        auto AddClearButton() -> void;
        auto IsChannelClearAvailable(const int32_t& row) -> bool;
        auto GetCurrentCursorPosition() const -> QPoint;
        [[nodiscard]] auto CreateSlider(const QModelIndex& index, AcquisitionTableSlider::SliderType sliderType, int32_t minVal, int32_t maxVal) -> AcquisitionTableSlider*;
    };

    ImagingConditionPanel::ImagingConditionPanel(QWidget* parent) : QWidget(parent), d{new Impl(this)} {
        d->ui.setupUi(this);

        // TODO Hide fov mode widget until Rectangle mode is supported
        d->ui.fovModeCombo->setVisible(false);

        d->ui.baseWidget->setObjectName("panel");
        d->ui.bottomWidget->setObjectName("panel");
        d->ui.flChannelWidget->setObjectName("panel");
        d->ui.zstackWidget->setObjectName("panel");
        d->ui.modalityWidget->setObjectName("panel-light");

        d->ui.line->setObjectName("line-divider");
        d->ui.line_2->setObjectName("line-divider");
        d->ui.flLine->setObjectName("line-divider");

        d->ui.labelROI->setObjectName("label-h3"); // ROI title label
        d->ui.labelRoiX->setObjectName("label-h5");  // ROI X label
        d->ui.labelRoiXUnit->setObjectName("label-h7");  // ROI X unit label
        d->ui.labelRoiY->setObjectName("label-h5");  // ROI Y label
        d->ui.labelRoiYUnit->setObjectName("label-h7");  // ROI Y unit label
        d->ui.labelFlCondTitle->setObjectName("label-h2");   // FL imaging condition title label
        d->ui.labelTileSizeXText->setObjectName("label-h3");   // FL imaging size 'X' label
        d->ui.labelTileWidthUnit->setObjectName("label-h7");  // Tile imaging width unit label
        d->ui.labelTileHeightUnit->setObjectName("label-h7");  // Tile imaging height unit label
        d->ui.labelTileCountXText->setObjectName("label-h3");   // FL imaging grid 'X' label
        d->ui.labelTileColumnUnit->setObjectName("label-h7");  // Tile imaging column count label
        d->ui.labelTileRowUnit->setObjectName("label-h7");  // Tile imaging row count label
        d->ui.labelCenterText->setObjectName("label-h7");  // Tile imaging center point label
        d->ui.labelCommaText->setObjectName("label-h7");  // Tile imaging center point label
        d->ui.labelCloseParenthesesText->setObjectName("label-h7");  // Tile imaging center point label

        d->Init();
        d->Connect();
        d->ConnectZStack();

        HiddenWidgetSizeFixer::SetRetainSize(d->ui.flChannelWidget);
        HiddenWidgetSizeFixer::SetRetainSize(d->ui.zstackWidget);

        // 2d HT chkbox hide
        d->ui.ht2d->setChecked(false);
        d->ui.ht2d->setCheckable(false);
        d->ui.ht2d->hide();

        d->ui.acqTable->setStyleSheet(QString("QTableView{border-bottom: 0px;}"));
        d->ui.acqTable->verticalHeader()->setStyleSheet(QString("QHeaderView{border-bottom: 0px;}"));
        d->ui.flLine->setStyleSheet(QString("QFrame{border:1px solid #394c53;}"));

        SetTileSize(300,300);
    }

    ImagingConditionPanel::~ImagingConditionPanel() {
    }

    auto ImagingConditionPanel::SetSafeMovingRange(const Axis& axis, double minMM, double maxMM) -> void {
        d->control.SetSafeMovingRange(axis, minMM, maxMM);
        switch (axis) {
            case Axis::Z:
                d->ui.zstackWidget->SetSafeZRange(minMM, maxMM);
                break;
            default:
                break;
        }
    }

    auto ImagingConditionPanel::InitFovMode(ROIMode mode) -> void {
        auto index = [=]()->int32_t {
            int32_t ret{ 0 };
            switch(mode) {
            case ROIMode::Square:
                ret = 0;
                break;
            case ROIMode::Free:
                ret = 1;
                break;
            }
            return ret;
        }();
        d->ui.fovModeCombo->setCurrentIndex(index);
        onChangeFovModeIndex(index);
    }

    auto ImagingConditionPanel::SetMaximumFOV(double xInUm, double yInUm) -> void {
        d->control.SetMaximumFOV(xInUm, yInUm);
        d->ChangeFovRange(static_cast<Impl::FovModeIndex>(d->ui.fovModeCombo->currentData().toInt()));
    }

    auto ImagingConditionPanel::SetFOV(double xInUm, double yInUm) -> void {
        d->control.SetFovSize(xInUm, yInUm);

        TC::SilentCall(d->ui.fovWidth)->setValue(xInUm);
        TC::SilentCall(d->ui.fovHeight)->setValue(yInUm);

        d->UpdateTileCount();

        d->ui.fovWidth->setToolTip(QString::number(d->control.GetFovWidth(), 'f', 3));
        d->ui.fovHeight->setToolTip(QString::number(d->control.GetFovHeight(), 'f', 3));
    }

    auto ImagingConditionPanel::GetFovX() const -> double {
        return d->control.GetFovWidth();
    }

    auto ImagingConditionPanel::GetFovY() const -> double {
        return d->control.GetFovHeight();
    }

    auto ImagingConditionPanel::SetActiveModality(const QList<ModalityType>& modalities) -> void {
        //Todo Modality 체크 상태 바꾸기 (signal block 후 변경)
        d->SetModalitySingalBlocked(true);
        for (auto& btn : d->modalityGroup->buttons()) {
            btn->setChecked(false);
        }

        for (const auto& modal : modalities) {
            d->modalityGroup->button(modal._to_integral())->setChecked(true);
        }
        d->UpdateFLWidgetsVisibility();
        d->SetModalitySingalBlocked(false);
    }

    auto ImagingConditionPanel::GetActiveModality() const -> QList<ModalityType> {
        //Todo 활성 상태인 모달리티 목록 반환
        QList<ModalityType> modalities;
        for (const auto& btn : d->modalityGroup->buttons()) {
            if (btn->isChecked()) {
                modalities.push_back(ModalityType::_from_integral(d->modalityGroup->id(btn)));
            }
        }
        return modalities;
    }

    auto ImagingConditionPanel::SetFLAcquisitionTableLock(bool lock) -> void {
        d->ui.acqLockToggle->setChecked(lock);
    }

    auto ImagingConditionPanel::IsFLAcquisitionTableLocked() -> bool {
        return d->ui.acqLockToggle->isChecked();
    }

    auto ImagingConditionPanel::SetFLChannels(const QList<FLChannelConfig>& configs) -> void {
        d->control.SetFLChannels(configs);
    }

    auto ImagingConditionPanel::GetFLChannel(int32_t channel) const -> FLChannelConfig {
        const auto config = d->ConvertTableDataToFLChannelConfig(d->acqModel->TableData().at(channel));
        return config;
    }

    auto ImagingConditionPanel::SetFLChannel(int32_t channel, const FLChannelConfig& config) -> void {
        if (d->IsChannelNameDuplicate(config.GetChannelName())) {
            TC::MessageDialog::warning(this, tr("Error"), tr("Duplicates with existing channel names."));
            return;
        }

        auto intensity = config.GetIntensity();
        auto exposure = config.GetExposure();
        auto gain = config.GetGain();

        if (intensity < d->control.GetIntensityMin()) intensity = d->control.GetIntensityMin();
        if (exposure < d->control.GetExposureMin()) exposure = d->control.GetExposureMin();
        if (gain < d->control.GetGainMin()) gain = d->control.GetGainMin();

        auto index = d->acqModel->index(channel, Column::Name);
        d->acqModel->setData(index, config.GetChannelName(), Qt::EditRole);
        index = index.sibling(channel, Column::Excitation);
        Light ex = {config.GetExcitationWaveLength(), config.GetExcitationBandWidth()};
        d->acqModel->setData(index, QVariant::fromValue(ex), Qt::EditRole);
        index = index.sibling(channel, Column::Emission);
        Light em = {config.GetEmissionWaveLength(), config.GetEmissionBandWidth()};
        d->acqModel->setData(index, QVariant::fromValue(em), Qt::EditRole);
        index = index.sibling(channel, Column::Intensity);
        d->acqModel->setData(index, intensity, Qt::EditRole);
        index = index.sibling(channel, Column::Exposure);
        d->acqModel->setData(index, exposure, Qt::EditRole);
        index = index.sibling(channel, Column::Gain);
        d->acqModel->setData(index, gain, Qt::EditRole);

        emit sigChangeAcquisitionTableData(channel, config);
    }

    auto ImagingConditionPanel::SetFLEnable(int32_t channel, bool enable) -> void {
        const auto index = d->acqModel->index(channel, Column::Enable);
        if (enable && d->IsChannelEmpty(channel)) d->ShowChannelList(channel);
        else {
            d->acqModel->blockSignals(true);
            d->acqModel->setData(index, enable, Qt::EditRole);
            d->acqModel->blockSignals(false);
        }
        d->RefreshTable(channel);

        emit sigChangeChannelEnabled(channel, d->acqModel->data(index, Qt::DisplayRole).toBool());
    }

    auto ImagingConditionPanel::GetFLEnabled(int32_t channel) const -> bool {
        const auto enabled = d->acqModel->index(channel, Column::Enable).data(Qt::DisplayRole).toBool();
        return enabled;
    }

    auto ImagingConditionPanel::ClearFLAcquisitionTable() -> void {
        const auto minIntensity = d->control.GetIntensityMin();
        const auto minExposure = d->control.GetExposureMin();
        const auto minGain = d->control.GetGainMin();

        for (int32_t row = 0; row < d->acqModel->rowCount(); ++ row) {
            auto index = d->acqModel->index(row, Column::Enable);
            d->acqModel->setData(index, false, Qt::EditRole);
            index = d->acqModel->index(row, Column::Name);
            d->acqModel->setData(index, "", Qt::EditRole);
            index = d->acqModel->index(row, Column::Excitation);
            d->acqModel->setData(index, QVariant::fromValue(Light{0, 0}), Qt::EditRole);
            index = d->acqModel->index(row, Column::Emission);
            d->acqModel->setData(index, QVariant::fromValue(Light{0, 0}), Qt::EditRole);
            index = d->acqModel->index(row, Column::Intensity);
            d->acqModel->setData(index, minIntensity, Qt::EditRole);
            index = d->acqModel->index(row, Column::Exposure);
            d->acqModel->setData(index, minExposure, Qt::EditRole);
            index = d->acqModel->index(row, Column::Gain);
            d->acqModel->setData(index, minGain, Qt::EditRole);

            emit sigChangeChannelEnabled(row, false);
        }
    }

    auto ImagingConditionPanel::ChangeFLConfigValues(int32_t channel, int32_t intensity, int32_t exposure, double gain, bool ignoreLock) -> void {
        if (!ignoreLock && d->ui.acqLockToggle->isChecked()) {
            return;
        }

        const auto name = d->acqModel->index(channel, Column::Name).data().toString();

        if (name.isEmpty()) {
            return;
        }

        if (intensity < d->control.GetIntensityMin()) intensity = d->control.GetIntensityMin();
        if (exposure < d->control.GetExposureMin()) exposure = d->control.GetExposureMin();
        if (gain < d->control.GetGainMin()) gain = d->control.GetGainMin();

        d->control.ChangeIntensity(name, intensity);
        d->control.ChangeExposure(name, exposure);
        d->control.ChangeGain(name, gain);

        const auto config = d->control.GetFLChannelConfig(name);
        auto index = d->acqModel->index(channel, Column::Name);
        d->acqModel->setData(index, config.GetChannelName(), Qt::EditRole);
        index = index.sibling(channel, Column::Excitation);
        Light ex = {config.GetExcitationWaveLength(), config.GetExcitationBandWidth()};
        d->acqModel->setData(index, QVariant::fromValue(ex), Qt::EditRole);
        index = index.sibling(channel, Column::Emission);
        Light em = {config.GetEmissionWaveLength(), config.GetEmissionBandWidth()};
        d->acqModel->setData(index, QVariant::fromValue(em), Qt::EditRole);
        index = index.sibling(channel, Column::Intensity);
        d->acqModel->setData(index, config.GetIntensity(), Qt::EditRole);
        index = index.sibling(channel, Column::Exposure);
        d->acqModel->setData(index, config.GetExposure(), Qt::EditRole);
        index = index.sibling(channel, Column::Gain);
        d->acqModel->setData(index, config.GetGain(), Qt::EditRole);

        emit sigChangeAcquisitionTableData(channel, config);
    }

    auto ImagingConditionPanel::GetTileActivation() const -> bool {
        return d->ui.tileImagingActivation->isChecked();
    }

    auto ImagingConditionPanel::GetTileCenterInMM() const -> std::tuple<double, double> {
        return std::make_tuple(d->ui.tilePosX->value(), d->ui.tilePosY->value());
    }

    auto ImagingConditionPanel::SetZResolutionUnit(double zResUnit) -> void {
        d->ui.zstackWidget->SetZResolutionUnit(zResUnit);
    }

    auto ImagingConditionPanel::GetZStackMode() const -> FLZStackMode {
        return d->ui.zstackWidget->GetZStackMode();
    }

    auto ImagingConditionPanel::SetFLZStackValues(FLZStackMode mode, double topInUm, double bottomInUm, double flOffsetInUm, double stepInUm) -> void {
        d->ui.zstackWidget->SetFLZStackValues(mode, topInUm, bottomInUm, flOffsetInUm, stepInUm);
    }

    auto ImagingConditionPanel::SetFLMaximumSlices(int32_t slices) -> void {
        d->ui.zstackWidget->SetFLMaximumSlices(slices);
    }

    auto ImagingConditionPanel::GetFLScanTop() const -> double {
        return d->ui.zstackWidget->GetFLScanTop();
    }

    auto ImagingConditionPanel::GetFLScanBottom() const -> double {
        return d->ui.zstackWidget->GetFLScanBottom();
    }

    auto ImagingConditionPanel::GetFLScanStep() const -> double {
        return d->ui.zstackWidget->GetFLScanStep();
    }

    auto ImagingConditionPanel::GetFLScanSlices() const -> uint32_t {
        return d->ui.zstackWidget->GetFLScanSlices();
    }

    auto ImagingConditionPanel::GetFLFocusOffset() const -> double {
        return d->ui.zstackWidget->GetFLFocusOffset();
    }

    auto ImagingConditionPanel::EnableAcquire(bool enable, const QString& disableMessage) -> void {
        const QString msg = enable ? "" : disableMessage;

        d->ui.acquireBtn->setEnabled(enable);
        d->ui.addPointBtn->setEnabled(enable);
        d->ui.zstackWidget->setEnabled(enable);

        d->ui.acquireBtn->setToolTip(msg);
        d->ui.addPointBtn->setToolTip(msg);
        d->ui.zstackWidget->setToolTip(msg);
    }

    auto ImagingConditionPanel::SetZFocus(double posZ) -> void {
        //Todo need to use function to save the best focus position
        //     HTZFocus should be changed to HT Z Offset which is not the same as Z focus position
        d->ui.zstackWidget->SetHTZFocus(posZ); // mm unit
    }

    auto ImagingConditionPanel::SetCurrentZ(double z) -> void {
        d->ui.zstackWidget->SetCurrentZ(z); // mm unit
    }

    auto ImagingConditionPanel::InitStepValue(double stepInUm) -> void {
        d->ui.zstackWidget->SetStep(stepInUm);
    }

    auto ImagingConditionPanel::SetMinimumOverlapArea(double horizontal, double vertical) -> void {
        d->control.SetOverlap(horizontal, vertical);
        d->UpdateTileCount();
    }

    auto ImagingConditionPanel::SetTileImagingActivation(bool active) -> void {
        d->ui.tileImagingActivation->setChecked(active);
    }

    auto ImagingConditionPanel::SetTileSize(double widthInUm, double heightInUm) -> void {
        d->control.SetTileSize(widthInUm, heightInUm);

        TC::SilentCall(d->ui.tileWidth)->setValue(widthInUm);
        TC::SilentCall(d->ui.tileHeight)->setValue(heightInUm);

        d->UpdateTileCount();

        d->ui.tileWidth->setToolTip(QString::number(d->control.GetTileWidth(), 'f', 3));
        d->ui.tileHeight->setToolTip(QString::number(d->control.GetTileHeight(), 'f', 3));
    }

    auto ImagingConditionPanel::SetCenterPosition(double centerX, double centerY) -> void {
        d->ui.tilePosX->blockSignals(true);
        d->ui.tilePosY->blockSignals(true);
        d->ui.tilePosX->setValue(centerX);
        d->ui.tilePosY->setValue(centerY);
        d->ui.tilePosX->blockSignals(false);
        d->ui.tilePosY->blockSignals(false);
    }

    auto ImagingConditionPanel::GetTileSizeInUM() const -> std::tuple<double, double> {
        return std::make_tuple(d->control.GetTileWidth(), d->control.GetTileHeight());
    }

    auto ImagingConditionPanel::SetIntensityRange(int32_t min, int32_t max) -> void {
        d->control.SetIntensityRange(min, max);
    }

    auto ImagingConditionPanel::SetExposureRange(int32_t min, int32_t max) -> void {
        d->control.SetExposureRange(min, max);
    }

    auto ImagingConditionPanel::SetGainRange(double min, double max) -> void {
        d->control.SetGainRange(min, max);
    }

    auto ImagingConditionPanel::SetFLChannelIndexInUse(const QList<int32_t>& indices) const -> void {
        d->control.SetFLChanenlIndexInUse(indices);
    }

    auto ImagingConditionPanel::ClearZStackSetting() -> void {
        d->ui.zstackWidget->ClearZStackSetting();
    }

    void ImagingConditionPanel::onFovWidthValueChanged(double val) {
        d->control.SetFovWidth(val);

        if(d->ui.fovModeCombo->currentData().toInt()==static_cast<int32_t>(Impl::FovModeIndex::Square)) {
            d->control.SetFovHeight(val);
            TC::SilentCall(d->ui.fovHeight)->setValue(val);
        }
    }

    void ImagingConditionPanel::onFovHeightValueChanged(double val) {
        d->control.SetFovHeight(val);

        if(d->ui.fovModeCombo->currentData().toInt()==static_cast<int32_t>(Impl::FovModeIndex::Square)) {
            d->control.SetFovWidth(val);
            TC::SilentCall(d->ui.fovWidth)->setValue(val);
        }
    }

    void ImagingConditionPanel::onEditingFovFinished() {
        emit sigChangeFov(d->control.GetFovWidth(), d->control.GetFovHeight());
        d->UpdateTileCount();
    }

    void ImagingConditionPanel::onAcquisitionTableDblClicked(const QModelIndex& index, bool isRecursive) {
        if (!index.isValid() || d->IsColumnHasNoPopup(index.column())) return;
        const auto row = index.row();
        const auto col = static_cast<Column>(index.column());
        const auto name = d->acqModel->index(row, Column::Name).data().toString();

        switch (col) {
            case AcquisitionTableModel::Name: {
                if (!isRecursive) {
                    d->lastPopUpPos = QCursor::pos();
                    auto rect = this->geometry();
                    rect.moveTopLeft(parentWidget()->mapToGlobal(rect.topLeft()));
                    d->lastPopUpPos.setY(rect.top());
                }

                const auto currentChannelName = index.data(Qt::DisplayRole).toString();
                auto nameList = d->control.GetFLChannelNames();

                nameList.push_front(""); // add blank for deselect

                const auto popup = new TC::InputDialog(this);
                popup->SetTitle(tr("FL channels"));
                popup->SetContent(tr("Channel Name:"));
                popup->setInputMode(TC::InputDialog::InputMode::ComboInput);
                popup->setComboBoxItems(nameList);
                popup->setComboBoxEditable(false);
                popup->setTextValue(currentChannelName);
                popup->adjustSize();
                popup->move(d->lastPopUpPos);
                if (popup->exec()) {
                    const auto selectedChannelName = popup->textValue();
                    if (selectedChannelName != currentChannelName) {
                        if (!d->IsChannelNameDuplicate(selectedChannelName)) {
                            if (selectedChannelName.isEmpty()) {
                                if (d->control.IsFLChannelInUse(row)) {
                                    TC::MessageDialog::warning(popup, tr("Failure in Deselecting Channel"), 
                                                               tr("Channel%1 is registered in the timelapse schedule.\n"
                                                                  "Please deselect the channel in the timelapse beforehand.").arg(row + 1));
                                }
                                else {
                                    SetFLEnable(row, false);
                                    SetFLChannel(row, d->control.GetFLChannelConfig(selectedChannelName));
                                }
                            }
                            else {
                                SetFLChannel(row, d->control.GetFLChannelConfig(selectedChannelName));
                            }
                        }
                        else {
                            TC::MessageDialog::warning(popup, 
                                                       tr("Error"),
                                                       tr("Duplicates with existing channel names."));
                            onAcquisitionTableDblClicked(index, true);
                        }
                    }
                }
                popup->deleteLater();
                break;
            }
            case AcquisitionTableModel::Intensity: {
                const auto slider = d->CreateSlider(index, AcquisitionTableSlider::Intensity, d->control.GetIntensityMin(), d->control.GetIntensityMax());
                if (slider) {
                    d->acqModel->setData(index, slider->GetValue().toInt(), Qt::EditRole);
                    slider->show();
                    connect(slider, &AcquisitionTableSlider::sigValueChanged, this, [this, name, row, index](const QVariant& value) {
                        d->acqModel->setData(index, value.toInt(), Qt::EditRole);
                        d->control.ChangeIntensity(name, value.toInt());
                        emit sigChangeAcquisitionTableData(row, d->control.GetFLChannelConfig(name));
                    });
                }
                break;
            }
            case AcquisitionTableModel::Exposure: {
                const auto slider = d->CreateSlider(index, AcquisitionTableSlider::Exposure, d->control.GetExposureMin(), d->control.GetExposureMax());
                if (slider) {
                    d->acqModel->setData(index, slider->GetValue().toInt(), Qt::EditRole);
                    slider->show();
                    connect(slider, &AcquisitionTableSlider::sigValueChanged, this, [this, name, row, index](const QVariant& value) {
                        d->acqModel->setData(index, value.toInt(), Qt::EditRole);
                        d->control.ChangeExposure(name, value.toInt());
                        emit sigChangeAcquisitionTableData(row, d->control.GetFLChannelConfig(name));
                    });
                }
                break;
            }
            case AcquisitionTableModel::Gain: [[fallthrough]];
            default: 
                break;
        }
    }

    void ImagingConditionPanel::onAddPointButtonClicked() {
        if (d->ui.tileImagingActivation->isChecked()) {
            emit sigAddAcquisitionTile(d->ui.tileWidth->value(), d->ui.tileHeight->value());
        }
        else {
            emit sigAddAcquisitionPoint();
        }
    }

    void ImagingConditionPanel::onAcquireButtonClicked() {
        if (!d->IsAcquireSignalAvailable()) {
            TC::MessageDialog::warning(this, 
                                       tr("Failed to start to Acquire"), 
                                       tr("Select imaging modalities."));
            return;
        }

        // zstack 값 변경 됐는지 확인 flzstack apply 버튼 누르면 현재 값 저장 후 acquire 시 변경됐는지 확인
        if (d->ui.fl3d->isChecked()) {
            if (d->ui.zstackWidget->IsZStackConfigChanged()) {
                TC::MessageDialog::warning(this, tr("Acquire request failed"), tr("Apply the changed z-stacking parameters."));
                return;
            }
        }

        if (d->ui.tileImagingActivation->isChecked()) {
            emit sigAcquireTile();
        }
        else {
            emit sigAcquirePoint();
        }
    }

    void ImagingConditionPanel::onChangeChannelEnable(int32_t row) {
        const auto lastEnableStatus = !d->acqModel->index(row, Column::Enable).data().toBool();
        SetFLEnable(row, lastEnableStatus);
    }

    void ImagingConditionPanel::onChangeFovModeIndex(int32_t index) {
        Q_UNUSED(index)

        const auto mode = static_cast<Impl::FovModeIndex>(d->ui.fovModeCombo->currentData().toInt());
        d->ChangeFovRange(mode);

        switch (mode) {
            case Impl::FovModeIndex::Square: {
                onEditingFovFinished();
                break;
            }
            case Impl::FovModeIndex::Free: {
                break;
            }
        }
    }

    void ImagingConditionPanel::onTileCenterSpinBoxEditingFinished() {
        const auto who = qobject_cast<TC::TCDoubleSpinBox*>(sender());
        bool ok{};
        const auto pos = Axis::_from_integral(who->property("AxisIndex").toInt(&ok));
        if(ok) {
            const auto inputVal = who->value();
            const auto safeVal = d->control.GetSafeMovingValue(pos, inputVal);
            TC::SilentCall(who)->setValue(safeVal);
            emit sigTileImagingChanged();
        }
    }

    void ImagingConditionPanel::onChannelClearButtonClicked() {
        const auto& btn = qobject_cast<QToolButton*>(sender());
        bool ok{};
        const auto& row = btn->property("rowIndex").toInt(&ok);
        if (d->control.IsFLChannelInUse(row)) {
            TC::MessageDialog::warning(this, 
                                       tr("Failure in Deselecting Channel"), 
                                       tr("Channel%1 is registered in the timelapse schedule.\n" 
                                          "Please deselect the channel in the timelapse beforehand.").arg(row + 1));

            return;
        }

        if(d->IsChannelClearAvailable(row) && ok) {
            SetFLEnable(row, false);
            SetFLChannel(row, d->control.GetFLChannelConfig(""));
            d->ui.acqTable->clearSelection();
        }
    }

    void ImagingConditionPanel::onChangeTableLockStatus(bool toggled) {
        if (!toggled) {
            const auto answer = TC::MessageDialog::question(this, 
                                                      tr("FL Condition Table Unlock"), 
                                                      tr("This will unlock the FL parameter table. Do you want to proceed?\n"));
            if (answer == TC::MessageDialog::StandardButton::NoButton || answer == TC::MessageDialog::StandardButton::No) {
                d->ui.acqLockToggle->AddIcon(":/lock/ic-lock-normal.svg");
                d->ui.acqLockToggle->AddIcon(":/lock/ic-lock-active.svg", QIcon::Active);
                d->ui.acqLockToggle->blockSignals(true);
                d->ui.acqLockToggle->setChecked(true);
                d->ui.acqLockToggle->blockSignals(false);
                return;
            }

        }
        if (d->ui.acqLockToggle->isChecked()) {
            d->ui.acqLockToggle->AddIcon(":/lock/ic-lock-normal.svg");
            d->ui.acqLockToggle->AddIcon(":/lock/ic-lock-active.svg", QIcon::Active);
        }
        else {
            d->ui.acqLockToggle->AddIcon(":/lock/ic-unlock-normal.svg");
            d->ui.acqLockToggle->AddIcon(":/lock/ic-unlock-active.svg", QIcon::Active);
        }

        emit sigChangeAcquisitionTableLock(toggled);
    }

    void ImagingConditionPanel::onChangeModality(int32_t id, bool toggled) {
        if (toggled) {
            const auto modalType = ModalityType::_from_integral(id);
            switch (modalType) {
                case ModalityType::FL2D: {
                    d->DeselectModality(d->ui.fl3d);
                    break;
                }
                case ModalityType::FL3D: {
                    d->DeselectModality(d->ui.fl2d);
                    break;
                }
                case ModalityType::HT2D: {
                    d->DeselectModality(d->ui.ht3d);
                    break;
                }
                case ModalityType::HT3D: {
                    d->DeselectModality(d->ui.ht2d);
                    break;
                }
                case ModalityType::BF: {
                    break;
                }
            }
        }

        d->UpdateFLWidgetsVisibility();
        emit sigChangeActiveModality();
    }

    void ImagingConditionPanel::onChangeTileImagingActivation(bool active) {
        d->ui.tileWidth->setEnabled(active);
        d->ui.tileHeight->setEnabled(active);
        d->ui.tilePosX->setEnabled(active);
        d->ui.tilePosY->setEnabled(active);
        d->ui.tileRows->setEnabled(active);
        d->ui.tileColumns->setEnabled(active);
        emit sigTileImagingChanged();
    }

    void ImagingConditionPanel::onTileWidthValueChanged(double val) {
        d->control.SetTileWidth(val);
        d->ui.tileWidth->setToolTip(QString::number(d->control.GetTileWidth()));
    }

    void ImagingConditionPanel::onTileHeightValueChanged(double val) {
        d->control.SetTileHeight(val);
        d->ui.tileHeight->setToolTip(QString::number(d->control.GetTileHeight()));
    }

    auto ImagingConditionPanel::Impl::Init() -> void {
        ui.fovWidth->setValue(0.0);
        ui.fovHeight->setValue(0.0);

        ui.ht2d->setChecked(false);
        ui.ht3d->setChecked(false);
        ui.fl2d->setChecked(false);
        ui.fl3d->setChecked(false);
        ui.bf->setChecked(false);
        ui.tileImagingActivation->setChecked(false);
        self->onChangeTileImagingActivation(ui.tileImagingActivation->isChecked());
        ui.tileWidth->setValue(0.0);
        ui.tileHeight->setValue(0.0);
        ui.tileWidth->setMinimum(0.0);
        ui.tileHeight->setMinimum(0.0);
        ui.tilePosX->setValue(0.0);
        ui.tilePosY->setValue(0.0);
        ui.tileRows->setValue(1);
        ui.tileColumns->setValue(1);

        UpdateFLWidgetsVisibility();

        acqModel = new AcquisitionTableModel();
        ui.acqTable->setModel(acqModel);
        ui.acqTable->setSelectionBehavior(QAbstractItemView::SelectItems);
        ui.acqTable->setSelectionMode(QAbstractItemView::SingleSelection);
        tableEventFilter = new AcquisitoinTableEventFilter(ui.acqTable);
        ui.acqTable->viewport()->installEventFilter(tableEventFilter);
        const auto chkboxDelegate = new AcquisitionTableCheckBoxDelegate(ui.acqTable);
        ui.acqTable->setItemDelegateForColumn(Column::Enable, chkboxDelegate);
        ui.acqTable->setColumnHidden(Column::Gain, true);
        connect(chkboxDelegate, &AcquisitionTableCheckBoxDelegate::sigCheckBoxClicked, self, &Self::onChangeChannelEnable);
        InitTableColumnSizePolicy();
        AddClearButton();

        modalityGroup = new QButtonGroup(self);
        modalityGroup->addButton(ui.fl2d, ModalityType::FL2D);
        modalityGroup->addButton(ui.fl3d, ModalityType::FL3D);
        modalityGroup->addButton(ui.ht2d, ModalityType::HT2D);
        modalityGroup->addButton(ui.ht3d, ModalityType::HT3D);
        modalityGroup->addButton(ui.bf, ModalityType::BF);
        modalityGroup->setExclusive(false);

        ui.addPointBtn->setObjectName("bt-tsx-addPoint");
        ui.acquireBtn->setObjectName("bt-tsx-acquire");

        ui.acqLockToggle->setCheckable(true);
        ui.acqLockToggle->setChecked(true);
        ui.acqLockToggle->setObjectName("bt-light");
        ui.acqLockToggle->setStyleSheet("QPushButton{border:none; background-color:transparent;}");
        ui.acqLockToggle->AddIcon(":/lock/ic-lock-normal.svg");
        ui.acqLockToggle->AddIcon(":/lock/ic-lock-active.svg", QIcon::Active);

        ui.fovModeCombo->insertItem(0, "Square", static_cast<int32_t>(FovModeIndex::Square));
        ui.fovModeCombo->insertItem(1, "Rectangle", static_cast<int32_t>(FovModeIndex::Free));
        self->onChangeFovModeIndex(0);

        ui.tilePosX->setProperty("AxisIndex", Axis::X);
        ui.tilePosY->setProperty("AxisIndex", Axis::Y);
    }

    auto ImagingConditionPanel::Impl::Connect() -> void {
        connect(ui.acquireBtn, &QPushButton::clicked, self, &Self::onAcquireButtonClicked);
        connect(ui.addPointBtn, &QPushButton::clicked, self, &Self::onAddPointButtonClicked);

        connect(modalityGroup, &QButtonGroup::idToggled, self, &Self::onChangeModality);

        connect(ui.fovWidth, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onFovWidthValueChanged);
        connect(ui.fovHeight, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onFovHeightValueChanged);
        connect(ui.fovWidth, &QDoubleSpinBox::editingFinished, self, &Self::onEditingFovFinished);
        connect(ui.fovHeight, &QDoubleSpinBox::editingFinished, self, &Self::onEditingFovFinished);

        connect(ui.tileWidth, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onTileWidthValueChanged);
        connect(ui.tileHeight, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onTileHeightValueChanged);
        connect(ui.tileWidth, &QDoubleSpinBox::editingFinished, self, &Self::onEditingTileSizeFinished);
        connect(ui.tileHeight, &QDoubleSpinBox::editingFinished, self, &Self::onEditingTileSizeFinished);

        connect(ui.tilePosX, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onChangeTileCenterPos);
        connect(ui.tilePosY, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onChangeTileCenterPos);
        connect(ui.tilePosX, &QDoubleSpinBox::editingFinished, self, &Self::onTileCenterSpinBoxEditingFinished);
        connect(ui.tilePosY, &QDoubleSpinBox::editingFinished, self, &Self::onTileCenterSpinBoxEditingFinished);

        connect(ui.tileImagingActivation, &QCheckBox::toggled, self, &Self::onChangeTileImagingActivation);

        connect(ui.acqTable, &QTableView::doubleClicked, self, [this](const QModelIndex& index) {
            self->onAcquisitionTableDblClicked(index, false);
        });
        connect(ui.acqLockToggle, &QPushButton::toggled, self, &Self::onChangeTableLockStatus);

        connect(ui.fovModeCombo, qOverload<int32_t>(&QComboBox::currentIndexChanged), self, &Self::onChangeFovModeIndex);
    }

    auto ImagingConditionPanel::Impl::ConnectZStack() -> void {
        connect(ui.zstackWidget, &ZStackWidget::sigZStackZPosPressed, self, &Self::sigZStackZPosPressed);
        connect(ui.zstackWidget, &ZStackWidget::sigApplyFLScanCondition, self, &Self::sigApplyFLScanCondition);
        connect(ui.zstackWidget, &ZStackWidget::sigUndoFLScanCondition, self, &Self::sigUndoFLScanCondition);
    }

    auto ImagingConditionPanel::Impl::DeselectModality(QAbstractButton* btn) -> void {
        if (!btn->isChecked()) return;
        SetModalitySingalBlocked(true);
        btn->setChecked(false);
        SetModalitySingalBlocked(false);
    }

    void ImagingConditionPanel::onEditingTileSizeFinished() {
        d->UpdateTileCount();
        emit sigTileImagingChanged();
    }

    void ImagingConditionPanel::onChangeTileCenterPos() {
        emit sigTileImagingChanged();
    }

    auto ImagingConditionPanel::Impl::SetModalitySingalBlocked(bool block) -> void {
        ui.bf->blockSignals(block);
        ui.fl2d->blockSignals(block);
        ui.fl3d->blockSignals(block);
        ui.ht2d->blockSignals(block);
        ui.ht3d->blockSignals(block);
        modalityGroup->blockSignals(block);
    }

    auto ImagingConditionPanel::Impl::UpdateFLWidgetsVisibility() -> void {
        ui.flChannelWidget->setVisible(ui.fl2d->isChecked() || ui.fl3d->isChecked());
        ui.zstackWidget->setVisible(ui.fl3d->isChecked());
    }

    auto ImagingConditionPanel::Impl::IsColumnHasNoPopup(int32_t column) -> bool {
        switch (static_cast<Column>(column)) {
            case Column::Enable:
            case Column::Excitation:
            case Column::Emission: return true;

            case Column::Name:
            case Column::Intensity:
            case Column::Exposure:
            case Column::Gain: return false;

            default: return true;
        }
    }

    auto ImagingConditionPanel::Impl::ConvertTableDataToFLChannelConfig(QList<AcquisitionTableModel::Data>::const_reference tableData) -> FLChannelConfig {
        FLChannelConfig config;
        config.SetChannelName(tableData.channel);
        config.SetExcitation(tableData.excitation.wavelen, tableData.excitation.bw);
        config.SetEmission(tableData.emission.wavelen, tableData.emission.bw);
        config.SetIntensity(tableData.intensity);
        config.SetExposure(tableData.exposure);
        config.SetGain(tableData.gain);
        return config;
    }

    auto ImagingConditionPanel::Impl::IsChannelNameDuplicate(const QString& channelName) -> bool {
        if (channelName.isEmpty()) return false;
        for (const auto& a : acqModel->TableData()) {
            if (a.channel == channelName) return true;
        }
        return false;
    }

    auto ImagingConditionPanel::Impl::IsChannelEmpty(int32_t row) -> bool {
        return acqModel->index(row, Column::Name).data().toString().isEmpty();
    }

    auto ImagingConditionPanel::Impl::ShowChannelList(const int32_t& row, bool isRecursive) -> void {
        // TODO onAcquisitionTableDblClicked name column case와 중복되는 코드 많음.
        acqModel->blockSignals(true);
        auto index = acqModel->index(row, Column::Name);
        if (!isRecursive) {
            lastPopUpPos = QCursor::pos();
            auto rect = self->geometry();
            rect.moveTopLeft(self->parentWidget()->mapToGlobal(rect.topLeft()));
            lastPopUpPos.setY(rect.top());
        }

        const auto currentChannelName = index.data(Qt::DisplayRole).toString();
        auto nameList = control.GetFLChannelNames();
        nameList.push_front(""); // add blank for deselect

        const auto popup = new TC::InputDialog(self);
        popup->SetTitle(tr("FL channels"));
        popup->SetContent(tr("Channel Name:"));
        popup->setInputMode(TC::InputDialog::InputMode::ComboInput);
        popup->setComboBoxItems(nameList);
        popup->setComboBoxEditable(false);
        popup->setTextValue(currentChannelName);
        popup->adjustSize();
        popup->move(lastPopUpPos);

        if (popup->exec()) {
            const auto selectedChannelName = popup->textValue();

            if (selectedChannelName != currentChannelName && !selectedChannelName.isEmpty()) {
                if (!IsChannelNameDuplicate(selectedChannelName)) {
                    self->SetFLChannel(row, control.GetFLChannelConfig(selectedChannelName));
                    bool enable = true;

                    if (selectedChannelName.isEmpty()) {
                        if(control.IsFLChannelInUse(row)) {
                            TC::MessageDialog::warning(popup, tr("Failed to clear"), 
                            tr("Ch%1 is currently in use on timelapse imaging schedule."
                               "Channels registered in Time Lapse cannot be cleared.").arg(row + 1));
                        }
                        else {
                            enable = false;
                        }
                    }
                    acqModel->setData(index.siblingAtColumn(Column::Enable), enable, Qt::EditRole);
                }
                else {
                    TC::MessageDialog::warning(popup, 
                                               tr("Error"), 
                                               tr("Duplicates with existing channel names."));
                    ShowChannelList(row, true);
                }
            }
            else {
                acqModel->setData(index.siblingAtColumn(Column::Enable), false, Qt::EditRole);
            }
        }
        acqModel->blockSignals(false);
    }

    auto ImagingConditionPanel::Impl::UpdateTileCount() -> void {
        const auto [tileRowCount, tileColumnCount] = control.GetTileCountRowColumn();

        ui.tileRows->setValue(tileRowCount);
        ui.tileColumns->setValue(tileColumnCount);
    }

    auto ImagingConditionPanel::Impl::RefreshTable(const int32_t& row) const -> void {
        const auto index = acqModel->index(row, Column::Enable);
        for (int i = 0; i < Column::_NumOfCols; ++i) {
            ui.acqTable->update(index.siblingAtColumn(i));
        }
    }

    auto ImagingConditionPanel::Impl::IsAcquireSignalAvailable() const -> bool {
        if (!modalityGroup) return false;
        if (modalityGroup->checkedId() == -1) return false;

        bool res = false;
        if (modalityGroup->button(ModalityType::FL3D)->isChecked() || modalityGroup->button(ModalityType::FL2D)->isChecked()) {
            for (auto row = 0; row < acqModel->rowCount(); ++row) {
                if (acqModel->index(row, Column::Enable).data(Qt::DisplayRole).toBool()) {
                    res = true;
                    break;
                }
            }
        }
        else {
            res = true;
        }

        return res;
    }

    auto ImagingConditionPanel::Impl::InitTableColumnSizePolicy() const -> void {
        const auto header = ui.acqTable->horizontalHeader();
        
        for (int32_t c = 0; c < header->count(); ++c) {
            if (header->isSectionHidden(c)) continue;

            switch(c) {
                case Column::Enable: 
                    header->setSectionResizeMode(c, QHeaderView::Fixed);
                    header->setMaximumSectionSize(24);
                    break;
                case Column::Clear: 
                    header->setSectionResizeMode(c, QHeaderView::Fixed);
                    header->setMaximumSectionSize(24);
                    break;
                default: 
                    header->setSectionResizeMode(c, QHeaderView::Stretch);
                    break;
            }
        }
    }

    auto ImagingConditionPanel::Impl::ChangeFovRange(FovModeIndex fovModeIndex) -> void {
        switch (fovModeIndex) {
        case FovModeIndex::Square:
            self->SetFOV(control.GetFovSmallerValue(), control.GetFovSmallerValue());
            ui.fovWidth->setRange(0.0, control.GetFovRangeSmallerValue());
            ui.fovHeight->setRange(0.0, control.GetFovRangeSmallerValue());
            break;
        case FovModeIndex::Free:
            ui.fovWidth->setRange(0.0, control.GetMaximumFovWidth());
            ui.fovHeight->setRange(0.0, control.GetMaximumFovHeight());
            break;
        }
    }

    auto ImagingConditionPanel::Impl::AddClearButton() -> void {
        for(auto r = 0; r < acqModel->rowCount(); r++) {
            const auto btn = new QToolButton();
            btn->setProperty("rowIndex", r);
            btn->setText("Clear");
            btn->setIcon(QIcon(":img/ic-float-delete.svg"));
            btn->setIconSize({24,24});
            btn->setStyleSheet("QToolButton {border:0px none; background-color:transparent;}");
            ui.acqTable->setIndexWidget(acqModel->index(r, Column::Clear), btn);
            connect(btn, &QToolButton::clicked, self, &Self::onChannelClearButtonClicked);
        }
    }

    auto ImagingConditionPanel::Impl::IsChannelClearAvailable(const int32_t& row) -> bool {
        if(IsChannelEmpty(row)) return false;
        if(control.IsFLChannelInUse(row)) return false;

        return true;
    }

    auto ImagingConditionPanel::Impl::GetCurrentCursorPosition() const -> QPoint {
        auto pos = QCursor::pos();
        auto rect = self->geometry();
        rect.moveTopLeft(self->parentWidget()->mapToGlobal(rect.topLeft()));
        pos.setY(rect.top());
        return pos;
    }

    auto ImagingConditionPanel::Impl::CreateSlider(const QModelIndex& index, AcquisitionTableSlider::SliderType sliderType, int32_t minVal, int32_t maxVal) -> AcquisitionTableSlider* {
        const auto row = index.row();
        const auto name = acqModel->index(row, Column::Name).data().toString();

        if (name.isEmpty()) {
            return nullptr;
        }

        const auto currentValue = index.data().toInt();

        const auto slider = new AcquisitionTableSlider();
        slider->SetRow(row);
        slider->SetSliderType(sliderType);
        slider->SetCurrentValue(currentValue);
        slider->SetRange(minVal, maxVal);
        slider->Initialize();
        slider->move(GetCurrentCursorPosition());

        return slider;
    }
}
