﻿#include <QFrame>
#include <QLabel>
#include <QToolButton>
#include <QLayout>
#include <QList>

#include "ZStackDiagramItem.h"
#include "ImagingConditionDefines.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    struct ZStackDiagramItem::Impl {
        Impl(Self* self):self(self){}
        Self* self{};
        QLabel* relValueLabel{nullptr}; // relative value
        QLabel* absValueLabel{nullptr}; // absolute value
        QToolButton* txtBtn{nullptr};
        DiagramType::_enumerated type;
        QList<QFrame*> horLines;
        QColor color;

        auto InitLabel() -> void;
        auto InitButton() -> void;
        auto InitLine() -> void;
        auto Layout() -> QLayout*;
    };

    ZStackDiagramItem::ZStackDiagramItem(DiagramType type, QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>(this)} {
        d->type = type;
        switch (d->type) {
            case DiagramType::Top: d->color = odDiagramFocusTB;
                break;
            case DiagramType::Bottom: d->color = odDiagramFocusTB;
                break;
            case DiagramType::FL: d->color = odDiagramFocusFL;
                break;
            case DiagramType::HTZ: d->color = odDiagramFocusHT;
                break;
        }

        d->InitLabel();
        d->InitButton();
        d->InitLine();

        setLayout(d->Layout());
        setFixedHeight(16);
    }

    ZStackDiagramItem::~ZStackDiagramItem() {
    }

    auto ZStackDiagramItem::SetValues(double relativeValue, double absoluteValue) -> void {
        const auto relativeTxt = QString::number(relativeValue, 'f', 1);
        const auto absoluteTxt = QString::number(absoluteValue, 'f', 3);

        d->relValueLabel->setText(relativeTxt);
        d->absValueLabel->setText(absoluteTxt);
        d->relValueLabel->setToolTip(d->txtBtn->text() + ": " + relativeTxt);
        d->absValueLabel->setToolTip(d->txtBtn->text() + ": " + absoluteTxt);
    }

    auto ZStackDiagramItem::UpdatePosAndSize(int32_t posY, int32_t width) -> void {
        updateGeometry();
        this->setGeometry(x(), posY, width, height());
    }

    auto ZStackDiagramItem::resizeEvent(QResizeEvent* event) -> void {
        updateGeometry();
        QWidget::resizeEvent(event);
    }

    void ZStackDiagramItem::onDiagramButtonClicked() {
        sigDiagramItemButtonClicked(d->type);
    }

    auto ZStackDiagramItem::Impl::InitButton() -> void {
        txtBtn = new QToolButton(self);

        QString btnName;

        switch (type) {
            case DiagramType::Top: btnName = "T";
                break;
            case DiagramType::Bottom: btnName = "B";
                break;
            case DiagramType::FL: btnName = "F";
                break;
            case DiagramType::HTZ: btnName = "H";
                break;
        }

        txtBtn->setText(btnName);
        txtBtn->setFixedSize(16, 16);

        const QString styleSheet = QString("QToolButton{font-weight: bold; background-color: %1; color: %2; border-radius: %3px;}")
        .arg(color.name(QColor::HexRgb)).arg(odDiagramText.name(QColor::HexRgb)).arg(txtBtn->width() / 2);
        txtBtn->setStyleSheet(styleSheet);

        connect(txtBtn, &QToolButton::clicked, self, &Self::onDiagramButtonClicked);
    }

    auto ZStackDiagramItem::Impl::InitLabel() -> void {
        QFontMetrics fm(self->font());
        auto relPxMinWidth = fm.horizontalAdvance("0.0");
        auto relPxMaxWidth = fm.horizontalAdvance("+00000.0");

        auto absPxMinWidth = fm.horizontalAdvance("0.000");
        auto absPxMaxWidth = fm.horizontalAdvance("+000.000");

        relValueLabel = new QLabel("0.0", self);
        absValueLabel = new QLabel("0.000", self);

        relValueLabel->setMinimumWidth(relPxMinWidth);
        relValueLabel->setMaximumWidth(relPxMaxWidth);

        absValueLabel->setMinimumWidth(absPxMinWidth);
        absValueLabel->setMaximumWidth(absPxMaxWidth);

        const QString styleSheet = QString("QLabel{color: %1}").arg(color.name(QColor::HexRgb));

        relValueLabel->setStyleSheet(styleSheet);
        absValueLabel->setStyleSheet(styleSheet);
    }

    auto ZStackDiagramItem::Impl::InitLine() -> void {
        const QString styleSheet = QString("QFrame{border: 1px solid %1}").arg(color.name(QColor::HexRgb));
        for (int i = 0; i < 5; ++i) {
            auto line = new QFrame(self);
            line->setFrameShape(QFrame::HLine);
            line->setFrameShadow(QFrame::Sunken);
            line->setLineWidth(2);
            line->setMaximumHeight(2);
            line->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
            line->setContentsMargins(0, 0, 0, 0);
            line->setStyleSheet(styleSheet);
            horLines.push_back(line);
        }
    }

    auto ZStackDiagramItem::Impl::Layout() -> QLayout* {
        const auto layout = new QHBoxLayout(self);
        layout->addWidget(relValueLabel, 3, Qt::AlignCenter);
        switch (type) {
            case DiagramType::Top: {
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(txtBtn, 0, Qt::AlignCenter);
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                break;
            }
            case DiagramType::FL: {
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(txtBtn, 0, Qt::AlignCenter);
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                break;
            }
            case DiagramType::HTZ: {
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(txtBtn, 0, Qt::AlignCenter);
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                break;
            }
            case DiagramType::Bottom: {
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                layout->addWidget(txtBtn, 0, Qt::AlignCenter);
                layout->addWidget(horLines.takeLast(), 1, Qt::Alignment());
                break;
            }
        }
        layout->addWidget(absValueLabel, 3, Qt::AlignCenter);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setMargin(0);
        layout->setSpacing(0);
        return layout;
    }
}
