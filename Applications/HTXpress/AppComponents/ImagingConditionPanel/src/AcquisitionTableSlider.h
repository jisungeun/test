﻿#pragma once

#include <memory>

#include <QWidget>

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class AcquisitionTableSlider : public QWidget {
        Q_OBJECT
    public:
        using Self = AcquisitionTableSlider;

        enum SliderType {Intensity, Exposure, Gain};
        explicit AcquisitionTableSlider(QWidget* parent = nullptr);
        ~AcquisitionTableSlider() override;

        auto SetRow(int32_t row) -> void;
        auto SetSliderType(SliderType type) -> void;
        auto SetCurrentValue(double value) -> void;
        auto SetRange(double min, double max) -> void;

        auto Initialize() -> void;

        auto GetValue() const -> QVariant;

    protected:
        auto keyPressEvent(QKeyEvent* event) -> void override;

    signals:
        void sigValueChanged(QVariant value);

    private slots:
        void onSliderValueChanged(int value);
        void onDoubleSpinBoxValueChanged(double value);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
