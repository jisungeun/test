﻿#pragma once

class QWidget;

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class HiddenWidgetSizeFixer {
    public:
        static auto SetRetainSize(QWidget* widget) -> void;
    };
}
