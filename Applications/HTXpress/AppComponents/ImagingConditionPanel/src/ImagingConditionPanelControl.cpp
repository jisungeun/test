﻿#include <ScanningPlanner.h>

#include "ImagingConditionPanelControl.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    struct ImagingConditionPanelControl::Impl {
        QList<FLChannelConfig> flConfigs;
        QList<int32_t> channelsInUse;

        struct {
            int32_t min;
            int32_t max;
        } intensityRange{0, 100}, exposureRange{1, 1000};

        struct {
            double min;
            double max;
        } gainRange{0.0, 50.0};

        struct {
            double maximumWidth;
            double maximumHeight;
        } maximumRoi;

        struct zstackUI {
            FLZStackMode mode{FLZStackMode::Default};
            double top{0.0};
            double bottom{0.0};
            double fl{0.0};
            double step{0.0};
            double range{0.0};
            int32_t slices{1};
        };

        struct {
            double width;
            double height;
        } roi{0.0, 0.0}, areaSize{0.0, 0.0};

        struct {
            double horizontal{};
            double vertical{};
        } overlap;

        struct Range {
            double min; // mm
            double max; // mm
        };
        QMap<Axis, Range> safeRanges;
    };

    ImagingConditionPanelControl::ImagingConditionPanelControl() : d{new Impl} {
    }

    ImagingConditionPanelControl::~ImagingConditionPanelControl() {
    }

    auto ImagingConditionPanelControl::SetMaximumFOV(double xInUm, double yInUm) -> void {
        d->maximumRoi = {xInUm, yInUm};
    }

    auto ImagingConditionPanelControl::GetMaximumFovWidth() const -> double {
        return d->maximumRoi.maximumWidth;
    }

    auto ImagingConditionPanelControl::GetMaximumFovHeight() const -> double {
        return d->maximumRoi.maximumHeight;
    }

    auto ImagingConditionPanelControl::GetFovRangeSmallerValue() const -> double {
        if(d->maximumRoi.maximumWidth < d->maximumRoi.maximumHeight) {
            return d->maximumRoi.maximumWidth;
        }
        return d->maximumRoi.maximumHeight;
    }

    auto ImagingConditionPanelControl::SetFovSize(double xInUM, double yInUM) -> void {
        d->roi = {xInUM, yInUM};
    }

    auto ImagingConditionPanelControl::SetFovWidth(double xInUM) -> void {
        d->roi.width = xInUM;
    }

    auto ImagingConditionPanelControl::SetFovHeight(double yInUM) -> void {
        d->roi.height = yInUM;
    }

    auto ImagingConditionPanelControl::GetFovWidth() const -> double {
        return d->roi.width;
    }

    auto ImagingConditionPanelControl::GetFovHeight() const -> double {
        return d->roi.height;
    }

    auto ImagingConditionPanelControl::GetFovSmallerValue() const -> double {
        if(d->roi.width < d->roi.height) {
            return d->roi.width;
        }
        return d->roi.height;
    }

    auto ImagingConditionPanelControl::SetTileSize(double xInUM, double yInUM) -> void {
        d->areaSize = {xInUM, yInUM};
    }

    auto ImagingConditionPanelControl::SetTileWidth(double xInUM) -> void {
        d->areaSize.width = xInUM;
    }

    auto ImagingConditionPanelControl::SetTileHeight(double yInUM) -> void {
        d->areaSize.height = yInUM;
    }

    auto ImagingConditionPanelControl::GetTileWidth() const -> double {
        return d->areaSize.width;
    }

    auto ImagingConditionPanelControl::GetTileHeight() const -> double {
        return d->areaSize.height;
    }

    auto ImagingConditionPanelControl::SetOverlap(double horizontal, double vertical) -> void {
        d->overlap = {horizontal, vertical};
    }

    auto ImagingConditionPanelControl::GetHorizontalOverlap() const -> double {
        return d->overlap.horizontal;
    }

    auto ImagingConditionPanelControl::GetVerticalOverlap() const -> double {
        return d->overlap.vertical;
    }

    auto ImagingConditionPanelControl::GetTileCountRowColumn() const -> std::tuple<int32_t, int32_t> {
        const auto areaSizeXNanometer = static_cast<uint32_t>(d->areaSize.width * 1000);
        const auto areaSizeYNanometer = static_cast<uint32_t>(d->areaSize.height * 1000);

        const auto roiSizeXNanometer = static_cast<uint32_t>(d->roi.width * 1000);
        const auto roiSizeYNanometer = static_cast<uint32_t>(d->roi.height * 1000);

        const auto overlapSizeXNanometer = static_cast<uint32_t>(d->overlap.horizontal * 1000);
        const auto overlapSizeYNanometer = static_cast<uint32_t>(d->overlap.vertical * 1000);

        const auto [tileCountColumn, tileCountRow] = ScanningPlanner::ScanningPlanner::CalculateTileCountXY(areaSizeXNanometer, 
            areaSizeYNanometer, roiSizeXNanometer, roiSizeYNanometer, overlapSizeXNanometer, overlapSizeYNanometer);

        return { static_cast<int32_t>(tileCountRow), static_cast<int32_t>(tileCountColumn) };
    }

    auto ImagingConditionPanelControl::SetIntensityRange(int32_t min, int32_t max) -> void {
        d->intensityRange = {min, max};
    }

    auto ImagingConditionPanelControl::SetExposureRange(int32_t min, int32_t max) -> void {
        d->exposureRange = {min, max};
    }

    auto ImagingConditionPanelControl::SetGainRange(double min, double max) -> void {
        d->gainRange = {min, max};
    }

    auto ImagingConditionPanelControl::GetIntensityMin() const -> int32_t {
        return d->intensityRange.min;
    }

    auto ImagingConditionPanelControl::GetIntensityMax() const -> int32_t {
        return d->intensityRange.max;
    }

    auto ImagingConditionPanelControl::GetExposureMin() const -> int32_t {
        return d->exposureRange.min;
    }

    auto ImagingConditionPanelControl::GetExposureMax() const -> int32_t {
        return d->exposureRange.max;
    }

    auto ImagingConditionPanelControl::GetGainMin() const -> double {
        return d->gainRange.min;
    }

    auto ImagingConditionPanelControl::GetGainMax() const -> double {
        return d->gainRange.max;
    }

    auto ImagingConditionPanelControl::GetFLChannelNames() -> QStringList {
        QStringList list;
        for (const auto& a : qAsConst(d->flConfigs)) {
            list.append(a.GetChannelName());
        }
        return list;
    }

    auto ImagingConditionPanelControl::GetFLChannelConfig(const QString& name) -> FLChannelConfig {
        for (const auto& a : qAsConst(d->flConfigs)) {
            if (a.GetChannelName() == name) return a;
        }

        FLChannelConfig noExistChNameConfig;
        noExistChNameConfig.SetChannelName("");
        noExistChNameConfig.SetEmission(0, 0);
        noExistChNameConfig.SetExcitation(0, 0);
        noExistChNameConfig.SetIntensity(d->intensityRange.min);
        noExistChNameConfig.SetExposure(d->exposureRange.min);
        noExistChNameConfig.SetGain(d->gainRange.min);
        return noExistChNameConfig;
    }

    auto ImagingConditionPanelControl::SetFLChannels(const QList<FLChannelConfig>& list) -> void {
        d->flConfigs = list;
    }

    auto ImagingConditionPanelControl::GetFLChannels() const -> QList<FLChannelConfig> {
        return d->flConfigs;
    }

    auto ImagingConditionPanelControl::SetFLChanenlIndexInUse(const QList<int32_t>& indices) const -> void {
        d->channelsInUse = indices;
    }

    auto ImagingConditionPanelControl::IsFLChannelInUse(const int32_t& channelIndex) const -> bool {
        return d->channelsInUse.contains(channelIndex);
    }

    auto ImagingConditionPanelControl::ChangeIntensity(const QString& name, int32_t intensity) -> void {
        for (auto& config : d->flConfigs) {
            if (name == config.GetChannelName()) {
                config.SetIntensity(intensity);
                break;
            }
        }
    }

    auto ImagingConditionPanelControl::ChangeExposure(const QString& name, int32_t exposure) -> void {
        for (auto& config : d->flConfigs) {
            if (name == config.GetChannelName()) {
                config.SetExposure(exposure);
                break;
            }
        }
    }

    auto ImagingConditionPanelControl::ChangeGain(const QString& name, double gain) -> void {
        for (auto& config : d->flConfigs) {
            if (name == config.GetChannelName()) {
                config.SetGain(gain);
                break;
            }
        }
    }

    auto ImagingConditionPanelControl::SetSafeMovingRange(Axis axis, double min, double max) -> void {
        d->safeRanges[axis] = {min, max};
    }

    auto ImagingConditionPanelControl::GetSafeMovingValue(Axis axis, double input) const -> double {
        if(!d->safeRanges.contains(axis)) return input;

        if(input <= d->safeRanges[axis].min) return d->safeRanges[axis].min;
        if(input >= d->safeRanges[axis].max) return d->safeRanges[axis].max;
        return input;
    }

}
