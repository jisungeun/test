﻿#include <limits>

#include "ZStackDiagramWidgetControl.h"
#include "ImagingConditionDefines.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    struct ZStackDiagramWidgetControl::Impl {
        FLZStackMode mode{FLZStackMode::Default};

        double relativeTop{0.0}; // um
        double relativeBottom{0.0}; // um
        double relativeFLFocus{0.0}; // um
        double absoluteHtz{0.0}; // mm

        const double relativeHtz{0.0}; // always 0
        const int32_t offset{5}; // widget geometry offset value
        int32_t bottomLocationOffset{0};

        auto GetMaxValue() const -> double;
        auto GetMinValue() const -> double;

        auto ConvertUMtoMM(double um) const -> double;
    };

    auto ZStackDiagramWidgetControl::Impl::GetMaxValue() const -> double {
        double max = std::numeric_limits<double>::lowest();

        QList values{relativeBottom, relativeHtz, relativeTop};
        if (mode == +FLZStackMode::FLFocus) {
            values.push_back(relativeFLFocus);
        }

        for (const auto val : values) {
            if (val > max) {
                max = val;
            }
        }

        return max;
    }

    auto ZStackDiagramWidgetControl::Impl::GetMinValue() const -> double {
        double min = std::numeric_limits<double>::max();

        QList values{relativeBottom, relativeHtz, relativeTop};
        if (mode == +FLZStackMode::FLFocus) {
            values.push_back(relativeFLFocus);
        }

        for (const auto val : values) {
            if (val < min) {
                min = val;
            }
        }

        return min;
    }

    auto ZStackDiagramWidgetControl::Impl::ConvertUMtoMM(double um) const -> double {
        return um/1000;
    }

    ZStackDiagramWidgetControl::ZStackDiagramWidgetControl() : d{std::make_unique<Impl>()} {
    }

    ZStackDiagramWidgetControl::~ZStackDiagramWidgetControl() {
    }

    auto ZStackDiagramWidgetControl::SetMode(FLZStackMode mode) -> void {
        d->mode = mode;
    }

    auto ZStackDiagramWidgetControl::GetMode() const -> FLZStackMode {
        return d->mode;
    }

    auto ZStackDiagramWidgetControl::SetRelativeTop(double top) -> void {
        d->relativeTop = top;
    }

    auto ZStackDiagramWidgetControl::GetRelativeTop() const -> double {
        return d->relativeTop;
    }

    auto ZStackDiagramWidgetControl::GetAbsoluteTop() const -> double {
        return d->absoluteHtz + d->ConvertUMtoMM(d->relativeTop);
    }

    auto ZStackDiagramWidgetControl::SetRelativeBottom(double bot) -> void {
        d->relativeBottom = bot;
    }

    auto ZStackDiagramWidgetControl::GetRelativeBottom() const -> double {
        return d->relativeBottom;
    }

    auto ZStackDiagramWidgetControl::GetAbsoluteBottom() const -> double {
        return d->absoluteHtz + d->ConvertUMtoMM(d->relativeBottom);
    }

    auto ZStackDiagramWidgetControl::SetRelativeFLFocus(double fl) -> void {
        d->relativeFLFocus = fl;
    }

    auto ZStackDiagramWidgetControl::GetRelativeFLFocus() const -> double {
        return d->relativeFLFocus;
    }

    auto ZStackDiagramWidgetControl::GetAbsoluteFLFocus() const -> double {
        return d->absoluteHtz + d->ConvertUMtoMM(d->relativeFLFocus);
    }

    auto ZStackDiagramWidgetControl::SetAbsoluteHTZFocus(double htz) -> void {
        // convert input htz um -> mm
        d->absoluteHtz = d->ConvertUMtoMM(htz);
    }

    auto ZStackDiagramWidgetControl::GetAbsoluteHTZFocus() const -> double {
        return d->absoluteHtz;
    }

    auto ZStackDiagramWidgetControl::GetRelativeHtzFocus() const -> double {
        return d->relativeHtz;
    }

    auto ZStackDiagramWidgetControl::GetItemPosY(DiagramType type) const -> int32_t {
        double relativeValue = 0.0;
        switch (type) {
            case DiagramType::Top: relativeValue = d->relativeTop;
                break;
            case DiagramType::Bottom: relativeValue = d->relativeBottom;
                break;
            case DiagramType::FL: relativeValue = d->relativeFLFocus;
                break;
            case DiagramType::HTZ: relativeValue = d->relativeHtz;
                break;
        }

        int32_t y = d->bottomLocationOffset / 2;
        const auto maximum = d->GetMaxValue();
        const auto minimum = d->GetMinValue();
        const auto denominator = maximum-minimum;

        if(denominator > std::numeric_limits<double>::epsilon()) {
            const auto ratio = (d->bottomLocationOffset - d->offset) / (maximum - minimum);
            y = d->offset + static_cast<int32_t>((maximum - relativeValue) * ratio);
        }

        return y;

    }

    auto ZStackDiagramWidgetControl::SetWidgetBottomLocOffset(int32_t mainWidgetHeight, int32_t itemHeight) const -> void {
        d->bottomLocationOffset = mainWidgetHeight - itemHeight - d->offset;
    }
}
