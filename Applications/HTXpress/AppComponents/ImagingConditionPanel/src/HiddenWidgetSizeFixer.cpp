﻿#include <QWidget>

#include "HiddenWidgetSizeFixer.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    auto HiddenWidgetSizeFixer::SetRetainSize(QWidget* widget) -> void {
        auto spRetain = widget->sizePolicy();
        spRetain.setRetainSizeWhenHidden(true);
        widget->setSizePolicy(spRetain);
    }
}
