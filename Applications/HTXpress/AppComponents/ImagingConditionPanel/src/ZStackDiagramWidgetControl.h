﻿#pragma once

#include <memory>

#include "ImagingConditionDefines.h"
#include "ZStackDiagramDefines.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class ZStackDiagramWidgetControl {
    public:
        using Self = ZStackDiagramWidgetControl;
        using Pointer = std::shared_ptr<Self>;

        ZStackDiagramWidgetControl();
        ~ZStackDiagramWidgetControl();

        auto SetMode(FLZStackMode mode) -> void;
        auto GetMode() const -> FLZStackMode;

        auto SetRelativeTop(double top) -> void;
        auto GetRelativeTop() const -> double;
        auto GetAbsoluteTop() const -> double;

        auto SetRelativeBottom(double bot) -> void;
        auto GetRelativeBottom() const -> double;
        auto GetAbsoluteBottom() const -> double;

        auto SetRelativeFLFocus(double fl) -> void;
        auto GetRelativeFLFocus() const -> double;
        auto GetAbsoluteFLFocus() const -> double;

        auto SetAbsoluteHTZFocus(double htz) -> void;
        auto GetAbsoluteHTZFocus() const -> double;
        auto GetRelativeHtzFocus() const -> double;

        auto GetMaximumValue() const -> double;
        auto GetMinimumValue() const -> double;

        auto GetItemPosY(DiagramType type) const -> int32_t;
        auto SetWidgetBottomLocOffset(int32_t mainWidgetHeight, int32_t itemHeight) const -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
