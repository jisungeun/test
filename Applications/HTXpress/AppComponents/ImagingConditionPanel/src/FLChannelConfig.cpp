#include <QMetaType>

#include "FLChannelConfig.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    static auto _id = qRegisterMetaType<FLChannelConfig>();

    struct FLChannelConfig::Impl {
        QString name;
        struct {
            int32_t waveLength{ 0 };
            int32_t bandWidth{ 0 };
        } excitation, emission;
        int32_t intensity{0};
        int32_t exposure{0};
        double gain{0.0};
    };

    FLChannelConfig::FLChannelConfig() : d{std::make_unique<Impl>()} {
    }

    FLChannelConfig::FLChannelConfig(const FLChannelConfig& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    FLChannelConfig::~FLChannelConfig() {
    }

    auto FLChannelConfig::operator=(const FLChannelConfig& other) -> FLChannelConfig& {
        *d = *other.d;
        return *this;
    }

    auto FLChannelConfig::SetChannelName(const QString& name) -> void {
        d->name = name;
    }

    auto FLChannelConfig::GetChannelName() const -> QString {
        return d->name;
    }

    auto FLChannelConfig::SetExcitation(int32_t waveLength, int32_t bandWidth) -> void {
        d->excitation.waveLength = waveLength;
        d->excitation.bandWidth = bandWidth;
    }

    auto FLChannelConfig::GetExcitationWaveLength() const -> int32_t {
        return d->excitation.waveLength;
    }

    auto FLChannelConfig::GetExcitationBandWidth() const -> int32_t {
        return d->excitation.bandWidth;
    }

    auto FLChannelConfig::SetEmission(int32_t waveLength, int32_t bandWidth) -> void {
        d->emission.waveLength = waveLength;
        d->emission.bandWidth = bandWidth;
    }

    auto FLChannelConfig::GetEmissionWaveLength() const -> int32_t {
        return d->emission.waveLength;
    }

    auto FLChannelConfig::GetEmissionBandWidth() const -> int32_t {
        return d->emission.bandWidth;
    }

    auto FLChannelConfig::SetIntensity(int32_t intensity) -> void {
        d->intensity = intensity;
    }

    auto FLChannelConfig::GetIntensity() const -> int32_t {
        return d->intensity;
    }

    auto FLChannelConfig::SetExposure(int32_t exposure) -> void {
        d->exposure = exposure;
    }

    auto FLChannelConfig::GetExposure() const -> int32_t {
        return d->exposure;
    }

    auto FLChannelConfig::SetGain(double gain) -> void {
        d->gain = gain;
    }

    auto FLChannelConfig::GetGain() const -> double {
        return d->gain;
    }

    auto FLChannelConfig::ToStr() const -> QString {
        QString str;
        QTextStream os(&str);

        os << "Ex=[" << d->excitation.waveLength << "," << d->excitation.bandWidth << "]"
           << " Em=[" << d->emission.waveLength << "," << d->emission.bandWidth << "]"
           << " Intensity=" << d->intensity << " Exposure=" << d->exposure
           << " Gain=" << d->gain;

        return str;
    }
}
