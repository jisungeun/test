﻿#include "ZStackWidgetControl.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    struct ZStackWidgetControl::Impl {
        double zResolution{0.1};
        double htZinUM{0.0}; // um
        double currZinUM{0.0}; // um

        FLZStackMode mode{FLZStackMode::Default};

        int32_t maximumSlices{ 60 };

        double top{};
        double bottom{};
        double flFocus{};
        double range{};
        double step{0.1};
        int32_t slices{1};

        ZStackConfig config;

        double inputTop{};
        double inputBottom{};
        double inputFlFocus{};
        double inputRange{};

        auto CalculateRange() -> double;
        auto CalculateSlices()->int32_t;
        auto CalculateTopBottomInFLFocusMode() -> void;
        auto CalculateCurrZandHTZDiff() -> double;
        auto CalculateFitValInZResolution(double value) -> double;
        auto CalculateFitValInStep(double value) -> double;
        auto CalculateRangeValueFitInStep(double _range) -> double;
    };

    ZStackWidgetControl::ZStackWidgetControl() : d{std::make_unique<Impl>()} {
    }

    ZStackWidgetControl::~ZStackWidgetControl() {
    }

    auto ZStackWidgetControl::SetHTZFocus(double htz) -> void {
        d->htZinUM = htz; // um
    }

    auto ZStackWidgetControl::GetHTZFocusUM() const -> double {
        return d->htZinUM;
    }

    auto ZStackWidgetControl::SetCurrentZPos(double currentZ) -> void {
        d->currZinUM = currentZ; // um
    }

    auto ZStackWidgetControl::GetCurrentZPosInUM() const -> double {
        return d->currZinUM;
    }

    auto ZStackWidgetControl::GetCurrentZPosInMM() const -> double {
        return QString::number(d->currZinUM/1000., 'g', 6).toDouble();
    }

    auto ZStackWidgetControl::SetZResolution(double zResolution) -> void {
        d->zResolution = zResolution;

        if(d->mode == +FLZStackMode::Default) {
            d->top = d->CalculateFitValInStep(d->top);
            d->bottom = d->CalculateFitValInStep(d->bottom);
            d->flFocus = d->CalculateFitValInStep(d->flFocus);
            d->range = d->CalculateRange();
        } else {
            d->flFocus = d->CalculateFitValInStep(d->flFocus);
            d->range = d->CalculateRangeValueFitInStep(d->range);
            d->CalculateTopBottomInFLFocusMode();
        }

        d->step = d->CalculateFitValInZResolution(d->step);
        d->slices = d->CalculateSlices();
    }

    auto ZStackWidgetControl::GetZResolution() const -> double {
        return d->zResolution;
    }

    auto ZStackWidgetControl::SetMaximumSlices(int32_t slices) -> void {
        d->maximumSlices = slices;
    }

    auto ZStackWidgetControl::GetMaximumSlices() const -> int32_t {
        return d->maximumSlices;
    }

    auto ZStackWidgetControl::SetMode(FLZStackMode mode) -> void {
        d->mode = mode;
        switch(mode) {
        case FLZStackMode::Default: {
                break;
            }
            case FLZStackMode::FLFocus: {
                d->flFocus = d->CalculateFitValInStep(d->flFocus);
                d->range = d->CalculateRangeValueFitInStep(d->range);
                d->CalculateTopBottomInFLFocusMode();
                break;
            }
        }
        d->step = d->CalculateFitValInZResolution(d->step);
        d->slices = d->CalculateSlices();
    }

    auto ZStackWidgetControl::GetMode() const -> FLZStackMode {
        return d->mode;
    }

    auto ZStackWidgetControl::SetTop(double top) -> void {
        d->top = d->CalculateFitValInStep(top);
        d->range = d->CalculateRange();
        d->slices = d->CalculateSlices();

        SetFLFocus((d->bottom + d->top)/2.0);
    }

    auto ZStackWidgetControl::GetTop() const -> double {
        return d->top;
    }

    auto ZStackWidgetControl::SetBottom(double bottom) -> void {
        d->bottom = d->CalculateFitValInStep(bottom);
        d->range = d->CalculateRange();
        d->slices = d->CalculateSlices();

        SetFLFocus((d->bottom + d->top)/2.0);
    }

    auto ZStackWidgetControl::GetBottom() const -> double {
        return d->bottom;
    }

    auto ZStackWidgetControl::SetFLFocus(double flFocus) -> void {
        d->flFocus = d->CalculateFitValInStep(flFocus);
        if(d->mode == +FLZStackMode::FLFocus) {
            d->CalculateTopBottomInFLFocusMode();
        }
        d->slices = d->CalculateSlices();
    }

    auto ZStackWidgetControl::GetFLFocus() const -> double {
        return d->flFocus;
    }

    auto ZStackWidgetControl::SetRange(double range) -> void {
        if(d->mode == +FLZStackMode::Default) {
            d->range = d->CalculateFitValInStep(range);
        } else {
            d->range = d->CalculateRangeValueFitInStep(range);
            d->CalculateTopBottomInFLFocusMode();
            d->slices = d->CalculateSlices();
        }
    }

    auto ZStackWidgetControl::GetRange() const -> double {
        return d->range;
    }

    auto ZStackWidgetControl::SetStep(double step) -> void {
        if(step < d->zResolution) step = d->zResolution;
        d->step = d->CalculateFitValInZResolution(step);
        if(d->mode == +FLZStackMode::Default) {
            d->top = d->CalculateFitValInStep(d->inputTop);
            d->bottom = d->CalculateFitValInStep(d->inputBottom);
            d->range = d->CalculateRange();
        } else {
            d->flFocus = d->CalculateFitValInStep(d->inputFlFocus);
            d->range = d->CalculateRangeValueFitInStep(d->inputRange);
            d->CalculateTopBottomInFLFocusMode();
        }
        d->slices = d->CalculateSlices();
    }

    auto ZStackWidgetControl::GetStep() const -> double {
        return d->step;
    }

    auto ZStackWidgetControl::SetSlices(int32_t slices) -> void {
        d->slices = slices;
    }

    auto ZStackWidgetControl::GetSlices() const -> int32_t {
        return d->slices;
    }

    auto ZStackWidgetControl::SetCurrentZStackConfig(const ZStackConfig& config) -> void {
        d->config = config;
    }

    auto ZStackWidgetControl::GetCurrentZStackConfig() const -> ZStackConfig {
        return d->config;
    }

    auto ZStackWidgetControl::IsZStackConfigChanged(const ZStackConfig& config) const -> bool {
        return d->config != config;
    }

    auto ZStackWidgetControl::SetTopToDiffValueBetweenCurrZandHTZ() -> void {
        SetUserInputTop(d->CalculateCurrZandHTZDiff());
        this->SetTop(d->CalculateCurrZandHTZDiff());
    }

    auto ZStackWidgetControl::SetBottomToDiffValueBetweenCurrZandHTZ() -> void {
        SetUserInputBottom(d->CalculateCurrZandHTZDiff());
        this->SetBottom(d->CalculateCurrZandHTZDiff());
    }

    auto ZStackWidgetControl::SetFLFocusToDiffValueBetweenCurrZandHTZ() -> void {
        SetUserInputFLFocus(d->CalculateCurrZandHTZDiff());
        this->SetFLFocus(d->flFocus = d->CalculateCurrZandHTZDiff());
    }

    auto ZStackWidgetControl::IsTopValueValid(double& topValue, double& botValue) -> bool {
        topValue = d->CalculateCurrZandHTZDiff();
        botValue = d->bottom;
        return topValue >= botValue;
    }

    auto ZStackWidgetControl::IsBottomValueValid(double& topValue, double& botValue) -> bool {
        topValue = d->top;
        botValue = d->CalculateCurrZandHTZDiff();
        return topValue >= botValue;
    }

    auto ZStackWidgetControl::SetUserInputTop(double top) -> void {
        d->inputTop = top;
        if(d->mode == +FLZStackMode::Default) {
            SetUserInputFLFocus((d->inputTop+d->inputBottom)/2.0);
            SetUserInputRange(fabs(d->inputTop-d->inputBottom));
        }
    }

    auto ZStackWidgetControl::SetUserInputBottom(double bottom) -> void {
        d->inputBottom = bottom;
        if(d->mode == +FLZStackMode::Default) {
            SetUserInputFLFocus((d->inputTop+d->inputBottom)/2.0);
            SetUserInputRange(fabs(d->inputTop-d->inputBottom));
        }
    }

    auto ZStackWidgetControl::SetUserInputFLFocus(double flfocus) -> void {
        d->inputFlFocus = flfocus;
        if(d->mode == +FLZStackMode::FLFocus) {
            const auto top = d->inputFlFocus + (d->inputRange / 2);
            const auto bot = d->inputFlFocus - (d->inputRange / 2);
            SetUserInputTop(top);
            SetUserInputBottom(bot);
        }
    }

    auto ZStackWidgetControl::SetUserInputRange(double range) -> void {
        d->inputRange = range;
        if(d->mode == +FLZStackMode::FLFocus) {
            const auto top = d->inputFlFocus + (d->inputRange / 2);
            const auto bot = d->inputFlFocus - (d->inputRange / 2);
            SetUserInputTop(top);
            SetUserInputBottom(bot);
        }
    }

    auto ZStackWidgetControl::GetDeciamlPrecision() -> int32_t {
        QString strVal = QString::number(d->zResolution);
        QStringList splitted = strVal.split('.');

        if(splitted.size() == 2) {
            auto decimals = splitted.takeLast().length();
            if(decimals > 0) {
                return decimals;
            }
        }
        return 1;
    }

    auto ZStackWidgetControl::Impl::CalculateRange() -> double {
        return fabs(top - bottom);
    }

    auto ZStackWidgetControl::Impl::CalculateSlices() -> int32_t {
        if(step < 0.0001 ) return 1;

        int32_t ret = ceil(((top * 1000) - (bottom * 1000)) / (step * 1000)) + 1;
        if(top < 0) {
            ret += 1;
        }
        if(bottom > 0) {
            ret += 1;
        }

        return ret;
    }

    auto ZStackWidgetControl::Impl::CalculateTopBottomInFLFocusMode() -> void {
        top = flFocus + (range / 2);
        bottom = flFocus - (range / 2);
    }

    auto ZStackWidgetControl::Impl::CalculateCurrZandHTZDiff() -> double {
        return (currZinUM - htZinUM);
    }

    auto ZStackWidgetControl::Impl::CalculateFitValInZResolution(double value) -> double {
        constexpr double ratio = 100.000;
        const auto mod = fmod(value * ratio, zResolution * ratio);
        if(fabs(mod) < 0.0001) {
            return value;
        }
        if(mod > 0) {
            return value + (zResolution - (mod / ratio)); // for positive value
        }
        return value - (zResolution + (mod / ratio)); // for negative value
    }

    auto ZStackWidgetControl::Impl::CalculateFitValInStep(double value) -> double {
        constexpr double ratio = 100.000;
        const auto mod = fmod(value * ratio, step * ratio);
        if(fabs(mod) < 0.0001) {
            return value;
        }
        if(mod > 0) {
            return value + (step - (mod / ratio));
        }
        return value - (step + (mod / ratio));
    }

    auto ZStackWidgetControl::Impl::CalculateRangeValueFitInStep(double _range) -> double {
        constexpr double ratio = 100.000;
        const auto mod = fmod(_range * ratio, step * ratio * 2);
        if(fabs(mod) < 0.0001) {
            return _range;
        }
        return _range + (step * 2) - (mod / ratio);
    }
}
