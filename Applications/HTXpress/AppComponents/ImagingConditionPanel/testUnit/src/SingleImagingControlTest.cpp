﻿#include <catch2/catch.hpp>

#include <QApplication>
#include <QList>
#include <QDebug>
#include <QRandomGenerator>
#include <QFile>
#include <QTextStream>

#include <ScanningPlanner.h>

#include "ImagingConditionPanelControl.h"
#include "FLChannelConfig.h"

namespace HTXpress::AppComponents::ImagingConditionPanel::Test {
    //int32_t arg = 0;
    //QApplication app(arg, nullptr);
    TEST_CASE("FLChannelConfig test") {
        SECTION("Interface") {
            FLChannelConfig config1;
            config1.SetChannelName("channel_1");
            config1.SetExcitation(500, 50);
            config1.SetEmission(600, 60);
            config1.SetIntensity(450);
            config1.SetExposure(2000);
            config1.SetGain(35.5);
            FLChannelConfig config2(config1);

            CHECK(config1.GetChannelName() == "channel_1");
            CHECK(config1.GetExcitationWaveLength() == 500);
            CHECK(config1.GetExcitationBandWidth() == 50);
            CHECK(config1.GetEmissionWaveLength() == 600);
            CHECK(config1.GetEmissionBandWidth() == 60);
            CHECK(config1.GetIntensity() == 450);
            CHECK(config1.GetExposure() == 2000);
            CHECK(config1.GetGain() == 35.5);

            CHECK(config1.GetChannelName() == config2.GetChannelName());
            CHECK(config1.GetExcitationWaveLength() == config2.GetExcitationWaveLength());
            CHECK(config1.GetExcitationBandWidth() == config2.GetExcitationBandWidth());
            CHECK(config1.GetEmissionWaveLength() == config2.GetEmissionWaveLength());
            CHECK(config1.GetEmissionBandWidth() == config2.GetEmissionBandWidth());
            CHECK(config1.GetIntensity() == config2.GetIntensity());
            CHECK(config1.GetExposure() == config2.GetExposure());
            CHECK(config1.GetGain() == config2.GetGain());
        }
    }

    TEST_CASE("ImagingConditionPanelControl test") {
        SECTION("Control Interface") {
            ImagingConditionPanelControl control;

            control.SetFovSize(100.0, 50.0);
            CHECK(control.GetFovWidth() == 100.0);
            CHECK(control.GetFovHeight() == 50.0);

            control.SetFovWidth(200.0);
            control.SetFovHeight(150.0);
            CHECK(control.GetFovWidth() == 200.0);
            CHECK(control.GetFovHeight()==150.0);

            control.SetTileSize(1000.0, 1500.0);
            CHECK(control.GetTileWidth() == 1000.0);
            CHECK(control.GetTileHeight() == 1500.0);

            control.SetTileWidth(1100.0);
            control.SetTileHeight(1400.0);
            CHECK(control.GetTileWidth() == 1100.0);
            CHECK(control.GetTileHeight() == 1400.0);

            control.SetOverlap(50.0, 30.0);
            CHECK(control.GetHorizontalOverlap()==50.0);
            CHECK(control.GetVerticalOverlap()==30.0);

            SECTION("FLChannelConfig List test") {
                FLChannelConfig config1;
                config1.SetChannelName("channel_1");
                config1.SetExcitation(500, 50);
                config1.SetEmission(600, 60);
                config1.SetIntensity(450);
                config1.SetExposure(2000);
                config1.SetGain(35.5);

                FLChannelConfig config2;
                config2.SetChannelName("channel_2");
                config2.SetExcitation(450, 45);
                config2.SetEmission(650, 65);
                config2.SetIntensity(200);
                config2.SetExposure(900);
                config2.SetGain(12.0);

                FLChannelConfig config3;
                config3.SetChannelName("channel_3");
                config3.SetExcitation(700, 40);
                config3.SetEmission(630, 80);
                config3.SetIntensity(250);
                config3.SetExposure(9900);
                config3.SetGain(48.2);

                QList<FLChannelConfig> channelConfigs;
                channelConfigs.push_back(config1);
                channelConfigs.push_back(config2);
                channelConfigs.push_back(config3);

                CHECK(channelConfigs.at(0).GetChannelName() == "channel_1");
                CHECK(channelConfigs.at(1).GetChannelName() == "channel_2");
                CHECK(channelConfigs.at(2).GetChannelName() == "channel_3");

                control.SetFLChannels(channelConfigs);
                CHECK(control.GetFLChannels().size() == 3);

                for (auto i = 0; i < control.GetFLChannels().size(); ++i) {
                    CHECK(channelConfigs.at(i).GetChannelName() == control.GetFLChannels().at(i).GetChannelName());
                    CHECK(channelConfigs.at(i).GetExcitationWaveLength() == control.GetFLChannels().at(i).GetExcitationWaveLength());
                    CHECK(channelConfigs.at(i).GetExcitationBandWidth() == control.GetFLChannels().at(i).GetExcitationBandWidth());
                    CHECK(channelConfigs.at(i).GetEmissionWaveLength() == control.GetFLChannels().at(i).GetEmissionWaveLength());
                    CHECK(channelConfigs.at(i).GetEmissionBandWidth() == control.GetFLChannels().at(i).GetEmissionBandWidth());
                    CHECK(channelConfigs.at(i).GetIntensity() == control.GetFLChannels().at(i).GetIntensity());
                    CHECK(channelConfigs.at(i).GetExposure() == control.GetFLChannels().at(i).GetExposure());
                    CHECK(channelConfigs.at(i).GetGain() == control.GetFLChannels().at(i).GetGain());
                }

                control.ChangeIntensity("channel_1", 199);
                CHECK(control.GetFLChannelConfig("channel_1").GetIntensity() == 199);

                control.ChangeExposure("channel_2", 1999);
                CHECK(control.GetFLChannelConfig("channel_2").GetExposure() == 1999);

                control.ChangeGain("channel_3", 19.9);
                CHECK(control.GetFLChannelConfig("channel_3").GetGain() == 19.9);

                auto names = control.GetFLChannelNames();
                CHECK(names.at(0) == "channel_1");
                CHECK(names.at(0) == config1.GetChannelName());
                CHECK(names.at(1) == "channel_2");
                CHECK(names.at(1) == config2.GetChannelName());
                CHECK(names.at(2) == "channel_3");
                CHECK(names.at(2) == config3.GetChannelName());

                SECTION("Invalid name") {
                    control.SetIntensityRange(1, 9000);
                    CHECK(control.GetIntensityMin() == 1);
                    CHECK(control.GetIntensityMax() == 9000);

                    control.SetExposureRange(10, 50000);
                    CHECK(control.GetExposureMin() == 10);
                    CHECK(control.GetExposureMax() == 50000);

                    control.SetGainRange(0.5, 99.9);
                    CHECK(control.GetGainMin() == 0.5);
                    CHECK(control.GetGainMax() == 99.9);

                    auto config = control.GetFLChannelConfig("invalid_channel");
                    CHECK(config.GetChannelName() == "");
                    CHECK(config.GetExcitationWaveLength() == 0);
                    CHECK(config.GetExcitationBandWidth() == 0);
                    CHECK(config.GetEmissionWaveLength() == 0);
                    CHECK(config.GetEmissionBandWidth() == 0);
                    CHECK(config.GetIntensity() == 1);
                    CHECK(config.GetIntensity() == control.GetIntensityMin());
                    CHECK(config.GetExposure() == 10);
                    CHECK(config.GetExposure() == control.GetExposureMin());
                    CHECK(config.GetGain() == 0.5);
                    CHECK(config.GetGain() == control.GetGainMin());
                }
            }
        }
    }

    TEST_CASE("Control Interface - Tile count detail") {
        SECTION("overlap didn't set") {
            ImagingConditionPanelControl control;
            control.SetFovSize(500.0, 100.0);
            control.SetTileSize(2000.0, 1000.0);

            const auto [tileCountRow, tileCountColumn] = control.GetTileCountRowColumn();

            CHECK(tileCountRow == 11);
            CHECK(tileCountColumn == 5);
        }
        SECTION("overlap 0") {
            ImagingConditionPanelControl control;
            control.SetFovSize(500.0, 100.0);
            control.SetTileSize(2000.0, 1000.0);
            control.SetOverlap(0.0, 0.0);

            const auto [tileCountRow, tileCountColumn] = control.GetTileCountRowColumn();

            CHECK(tileCountRow == 11);
            CHECK(tileCountColumn == 5);
        }
        SECTION("overlap 0 (2)") {
            ImagingConditionPanelControl control;
            control.SetFovSize(500.5, 100.5);
            control.SetTileSize(2000.0, 1000.0);
            control.SetOverlap(0.0, 0.0);

            const auto [tileCountRow, tileCountColumn] = control.GetTileCountRowColumn();

            CHECK(tileCountRow == 11);
            CHECK(tileCountColumn == 5);
        }
        SECTION("overlap non zero") {
            ImagingConditionPanelControl control;
            control.SetFovSize(500.0, 100.0);
            control.SetTileSize(2000.0, 1000.0);
            control.SetOverlap(111.0, 11.0);

            const auto [tileCountRow, tileCountColumn] = control.GetTileCountRowColumn();

            CHECK(tileCountRow == 12);
            CHECK(tileCountColumn == 5);
        }

        SECTION("roi size of two axis is same with area (fit)") {
            ImagingConditionPanelControl control;
            control.SetFovSize(200, 200);
            control.SetTileSize(200, 1000);
            control.SetOverlap(20, 20);

            const auto [tileCountRow, tileCountColumn] = control.GetTileCountRowColumn();

            CHECK(tileCountRow == 6);
            CHECK(tileCountColumn == 2);
        }

        SECTION("roi size of two axis is same with area (smaller area)") {
            ImagingConditionPanelControl control;
            control.SetFovSize(200, 200);
            control.SetTileSize(100, 1000);
            control.SetOverlap(20, 20);

            const auto [tileCountRow, tileCountColumn] = control.GetTileCountRowColumn();

            CHECK(tileCountRow == 6);
            CHECK(tileCountColumn == 1);
        }

        SECTION("formula test") {
            ImagingConditionPanelControl control;

            QFile file("out.utr");
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                return;
            QTextStream out(&file);

            for (int i = 0; i < 10000; i++) {
                uint32_t randomFOV = QRandomGenerator::global()->bounded(100000, 164700);
                uint32_t randomTileW = QRandomGenerator::global()->bounded(100000, 3000000);
                uint32_t randomTileH = QRandomGenerator::global()->bounded(100000, 3000000);
                uint32_t randomOverlapHor = QRandomGenerator::global()->bounded(0, 15000);
                uint32_t randomOverlapVer = QRandomGenerator::global()->bounded(0, 15000);

                control.SetFovSize(randomFOV, randomFOV);
                control.SetTileSize(randomTileW, randomTileH);
                control.SetOverlap(randomOverlapHor, randomOverlapVer);
                const auto [ctrRowCount, ctrColCount] = control.GetTileCountRowColumn();

                const auto tileWidth = control.GetTileWidth();
                const auto tileHeight = control.GetTileHeight();
                const auto fovWidth = control.GetFovWidth();
                const auto fovHeight = control.GetFovHeight();
                const auto overlapHor = control.GetHorizontalOverlap();
                const auto overlapVer = control.GetVerticalOverlap();

                const auto [plannerColCount, plannerRowCount] = ScanningPlanner::ScanningPlanner::CalculateTileCountXY(tileWidth*1000, tileHeight * 1000, fovWidth * 1000, fovHeight * 1000, overlapHor * 1000, overlapVer * 1000);

                out << "generated[fw,fh,tw,th,oh,ov]: " << randomFOV << ", "<< randomFOV<< ", " << randomTileW<< ", " << randomTileH<< ", " << randomOverlapHor<< ", " << randomOverlapVer << "\n";
                out << "control result[r,c]: " << ctrRowCount <<", " << ctrColCount << "\n";
                out << "planner result[r,c]: " << plannerRowCount << ", " << plannerColCount << "\n";
                if(ctrRowCount != plannerRowCount || ctrColCount != plannerColCount) {
                    out << "\n   Different Result!!!!   \n\n";
                }

                CHECK(ctrRowCount == plannerRowCount);
                CHECK(ctrColCount == plannerColCount);
            }
        }
    }
}
