﻿#include <catch2/catch.hpp>

#include "ImagingConditionPanel.h"

using namespace HTXpress::AppComponents::ImagingConditionPanel;

namespace HTXImagingCondtionPanelTest {
	TEST_CASE("FL Scan Setting", "[FLScan]") {
		auto panel = std::make_unique<ImagingConditionPanel>();
		double top = 40;
		double bot = 10;
		double step = 5;

		SECTION("Set FL Scan Range") {
			panel->SetFLScanRange(top, bot, step);

			REQUIRE(top > bot);
			CHECK(top > bot);
		}
		SECTION("asdf") {
			bool b = true;
			CHECK(b == false);
		}
	}
}
