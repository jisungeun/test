﻿#include <catch2/catch.hpp>

#include <QApplication>
#include <QString>

#include "ImagingConditionPanel.h"
#include "ImagingConditionDefines.h"
#include "FLChannelConfig.h"

namespace HTXpress::AppComponents::ImagingConditionPanel::Test {
    auto isSameDouble(double a, double b) -> bool {
        return fabs(a - b) < 1e-9;
    }

    TEST_CASE("QString Test") {
        int32_t arg = 0;
        QApplication app(arg, nullptr);
        SECTION("double 0 padding expression test") {
            double d0 = 1;
            double d1 = 1.1;
            double d2 = 1.12;
            double d3 = 1.123;
            double d4 = 1.1234;
            double d5 = 1.12345;
            double d6 = 1.123456;
            double d7 = 1.1234567;

            CHECK(QString::number(d0, 'f', 3) == "1.000");
            CHECK(QString::number(d1, 'f', 3) == "1.100");
            CHECK(QString::number(d2, 'f', 3) == "1.120");
            CHECK(QString::number(d3, 'f', 3) == "1.123");
            CHECK(QString::number(d4, 'f', 3) == "1.123");
            CHECK(QString::number(d5, 'f', 3) == "1.123");
            CHECK(QString::number(d6, 'f', 3) == "1.123");
            CHECK(QString::number(d7, 'f', 3) == "1.123");
        }
        SECTION("double 0 padding expression test (round)") {
            double d0 = 1;
            double d1 = 1.9;
            double d2 = 1.99;
            double d3 = 1.999;
            double d4 = 1.9999;
            double d5 = 1.99999;
            double d6 = 1.999999;
            double d7 = 1.9999999;

            CHECK(QString::number(d0, 'f', 3) == "1.000");
            CHECK(QString::number(d1, 'f', 3) == "1.900");
            CHECK(QString::number(d2, 'f', 3) == "1.990");
            CHECK(QString::number(d3, 'f', 3) == "1.999");
            CHECK(QString::number(d4, 'f', 3) == "2.000");
            CHECK(QString::number(d5, 'f', 3) == "2.000");
            CHECK(QString::number(d6, 'f', 3) == "2.000");
            CHECK(QString::number(d7, 'f', 3) == "2.000");
        }
    }


}
