project(HTXImagingConditionSettingUnitTest)

#Header files for external use
set(INTERFACE_HEADERS
	
)
	
set(TEST_SOURCE
	../src/ZStackWidgetControl.h
	../src/ZStackWidgetControl.cpp
	../src/ZStackDiagramWidgetControl.h
	../src/ZStackDiagramWidgetControl.cpp
	../src/ImagingConditionPanelControl.h
	../src/ImagingConditionPanelControl.cpp
)

#Header files for internal use
set(PRIVATE_HEADERS

)

#Sources
set(SOURCES
	src/main_test.cpp
	src/ZStackCalculateTest.cpp
	src/SingleImagingControlTest.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
	${TEST_SOURCE}
)

target_compile_features(${PROJECT_NAME} 
	PRIVATE
		cxx_std_17
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
		${CMAKE_CURRENT_SOURCE_DIR}/../src
)

target_link_libraries(${PROJECT_NAME}			
	PRIVATE
		Catch2::Catch2
		Qt5::Core
		HTXpress::AppComponents::ImagingConditionPanel
		HTXpress::AppComponents::ScanningPlanner
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/HTXpress/AppComponents/Tests") 	

foreach(FILE ${TEST_SOURCE})
    # Remove common directory prefix to make the group
    string(REPLACE "src" "" GROUP "TEST_SOURCE")

    # Make sure we are using windows slashes
    string(REPLACE "/" "\\" GROUP "${GROUP}")
	
	source_group("${GROUP}" FILES "${FILE}")
endforeach()