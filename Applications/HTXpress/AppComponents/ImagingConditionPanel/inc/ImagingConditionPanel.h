#pragma once
#include <memory>

#include <QWidget>

#include "ImagingConditionDefines.h"
#include "FLChannelConfig.h"
#include "HTXImagingConditionPanelExport.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class HTXImagingConditionPanel_API ImagingConditionPanel : public QWidget {
        Q_OBJECT
	public:
        using Self = ImagingConditionPanel;
        using Pointer = std::shared_ptr<Self>;

        explicit ImagingConditionPanel(QWidget* parent = nullptr);
        ~ImagingConditionPanel() override;

        auto SetSafeMovingRange(const Axis& axis, double minMM, double maxMM) -> void;

        // FOV
        auto InitFovMode(ROIMode mode = ROIMode::Square) -> void;
        auto SetMaximumFOV(double xInUm, double yInUm) -> void;
        auto SetFOV(double xInUm, double yInUm) -> void;
        auto GetFovX() const -> double;
        auto GetFovY() const -> double;

        // Modality Menu
        auto SetActiveModality(const QList<ModalityType>& modalities) -> void;
        auto GetActiveModality() const -> QList<ModalityType>;

        // Tile Imaging Setting
        auto SetMinimumOverlapArea(double horizontal, double vertical) -> void;
        auto SetTileImagingActivation(bool active) -> void;
        auto SetTileSize(double widthInUm, double heightInUm) -> void;
        auto SetCenterPosition(double centerX, double centerY) -> void;

        // FL Acquisition Table Setting, channel1 = 0, channel2 = 1, channel3 = 2
        auto ClearFLAcquisitionTable() -> void;
        auto SetFLAcquisitionTableLock(bool lock) -> void;
        auto IsFLAcquisitionTableLocked()->bool;
        auto SetFLChannels(const QList<FLChannelConfig>& configs) -> void;
        auto SetFLChannel(int32_t channel, const FLChannelConfig& config) -> void;
        auto GetFLChannel(int32_t channel) const -> FLChannelConfig;
        auto SetFLEnable(int32_t channel, bool enable) -> void;
        auto GetFLEnabled(int32_t channel) const -> bool;
        auto ChangeFLConfigValues(int32_t channel, int32_t intensity, int32_t exposure, double gain, bool ignoreLock = false) -> void;

        auto GetTileActivation() const->bool;
        auto GetTileCenterInMM() const->std::tuple<double, double>;
        auto GetTileSizeInUM() const->std::tuple<double, double>;

        auto SetIntensityRange(int32_t min, int32_t max) -> void;
        auto SetExposureRange(int32_t min, int32_t max) -> void;
        auto SetGainRange(double min, double max) -> void;

        auto SetFLChannelIndexInUse(const QList<int32_t>& indices) const -> void;

        // FL Scan Condition Setting
        auto ClearZStackSetting() -> void;
        auto SetZResolutionUnit(double zResUnit) -> void; // z res unit

        auto SetZFocus(double posZ) -> void;              //Set the best focus z position
        auto SetCurrentZ(double posZ) -> void;            //Set the current Z position

        auto InitStepValue(double stepInUm) -> void;
        auto SetFLZStackValues(FLZStackMode mode, double topInUm, double bottomInUm, double flOffsetInUm, double stepInUm) -> void;

        auto SetFLMaximumSlices(int32_t slices) -> void;

        auto GetZStackMode() const -> FLZStackMode; // default mode / FL focus mode
        auto GetFLScanTop() const -> double;
        auto GetFLScanBottom() const -> double;
        auto GetFLScanStep() const -> double;
        auto GetFLScanSlices() const -> uint32_t;
        auto GetFLFocusOffset() const -> double;

        auto EnableAcquire(bool enable, const QString& disableMessage)->void;

    signals:
        void sigChangeFov(double width, double height);
        void sigChangeActiveModality();
        void sigTileImagingChanged();

        void sigAcquirePoint();
        void sigAcquireTile();

        void sigAddAcquisitionPoint();
        void sigAddAcquisitionTile(double width, double height);

        void sigChangeAcquisitionTableLock(bool locked);
        void sigChangeAcquisitionTableData(int32_t row, 
                                           const HTXpress::AppComponents::ImagingConditionPanel::FLChannelConfig& config);
        void sigChangeChannelEnabled(int32_t row, bool enabled);

        // ZStack Diagram에서 Focus Item 클릭 시 발생
        // @param z 클릭한 FocusItem의 z 값 (절대값, not 상대값)
        void sigZStackZPosPressed(double z);
        void sigApplyFLScanCondition();
        void sigUndoFLScanCondition();

        // internal use only
    private slots:
        void onFovWidthValueChanged(double val);
        void onFovHeightValueChanged(double val);
        void onEditingFovFinished();
        void onChangeModality(int32_t id, bool toggled);
        void onChangeTileImagingActivation(bool active);
        void onTileWidthValueChanged(double val);
        void onTileHeightValueChanged(double val);
        void onEditingTileSizeFinished();
        void onChangeTileCenterPos();
        void onChangeTableLockStatus(bool toggled);
        void onAcquisitionTableDblClicked(const QModelIndex& index, bool isRecursive = false);
        void onAddPointButtonClicked();
        void onAcquireButtonClicked();
        void onChangeChannelEnable(int32_t row);
        void onChangeFovModeIndex(int32_t index);
        void onTileCenterSpinBoxEditingFinished();
        void onChannelClearButtonClicked();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
