#pragma once
#include <memory>
#include <QString>
#include <QTextStream>

#include "HTXImagingConditionPanelExport.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    class HTXImagingConditionPanel_API FLChannelConfig {
    public:
        FLChannelConfig();
        FLChannelConfig(const FLChannelConfig& other);
        ~FLChannelConfig();

        auto operator=(const FLChannelConfig& other) -> FLChannelConfig&;

        auto SetChannelName(const QString& name) -> void;
        auto GetChannelName() const -> QString;

        auto SetExcitation(int32_t waveLength, int32_t bandWidth) -> void;
        auto GetExcitationWaveLength() const -> int32_t;
        auto GetExcitationBandWidth() const -> int32_t;

        auto SetEmission(int32_t emission, int32_t bandWidth) -> void;
        auto GetEmissionWaveLength() const -> int32_t;
        auto GetEmissionBandWidth() const -> int32_t;

        auto SetIntensity(int32_t intensity) -> void;
        auto GetIntensity() const -> int32_t;

        auto SetExposure(int32_t exposure) -> void;
        auto GetExposure() const -> int32_t;

        auto SetGain(double gain) -> void;
        auto GetGain() const -> double;

        auto ToStr() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

Q_DECLARE_METATYPE(HTXpress::AppComponents::ImagingConditionPanel::FLChannelConfig)
