#pragma once
#include <memory>
#include <QList>

#include <enum.h>

#include "HTXImagingConditionPanelExport.h"

namespace HTXpress::AppComponents::ImagingConditionPanel {
    BETTER_ENUM(ModalityType, int32_t, HT3D, HT2D, FL3D, FL2D, BF);

    BETTER_ENUM(FLZStackMode, int32_t, Default, FLFocus);

    BETTER_ENUM(ROIMode, int32_t, Square, Free);

    BETTER_ENUM(Axis, int32_t, X, Y, Z, Invalid);
}
