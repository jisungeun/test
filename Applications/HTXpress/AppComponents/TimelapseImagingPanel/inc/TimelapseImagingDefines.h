#pragma once

#include <enum.h>

namespace HTXpress::AppComponents::TimelapseImagingPanel {
	typedef int32_t TimelapseSequenceIndex;

    BETTER_ENUM(PanelType, int32_t,
		View,
		Edit,
		Acquisition
	);

	BETTER_ENUM(Dimension, int32_t,
		Dimension2,
		Dimension3
	);

	BETTER_ENUM(Modality, int32_t,
		HT,
		FL,
		FLCH1,
		FLCH2,
		FLCH3,
		BF
	);

	BETTER_ENUM(ImagingStatus, int32_t,
		Ready,
		InProgress,
		Done,
		Failed
	);
}