#pragma once

#include <memory>

#include <QString>

#include "HTXTimelapseImagingPanelExport.h"

namespace HTXpress::AppComponents::TimelapseImagingPanel {
	class HTXTimelapseImagingPanel_API TimelapseImagingTime {
	public:
		TimelapseImagingTime();
		TimelapseImagingTime(int hour, int minute, int second);
		TimelapseImagingTime(int seconds);
		TimelapseImagingTime(const TimelapseImagingTime& other);
		~TimelapseImagingTime();

		auto operator=(const TimelapseImagingTime& other)->TimelapseImagingTime&;
		auto operator==(const TimelapseImagingTime& other)->bool;
		auto operator!=(const TimelapseImagingTime& other)->bool;

		auto GetHour() const->int;
		auto GetMinute() const->int;
		auto GetSecond() const->int;

		auto ToSeconds() const->int;

		auto ToString() const->QString;

		static auto ToSeconds(const TimelapseImagingTime& time)->int;
		static auto ToSeconds(int hour, int minute, int second)->int;

	    static auto ToString(const TimelapseImagingTime& time)->QString;
		static auto ToString(int hour, int minute, int second)->QString;

		static auto FromString(const QString& text)->TimelapseImagingTime;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
