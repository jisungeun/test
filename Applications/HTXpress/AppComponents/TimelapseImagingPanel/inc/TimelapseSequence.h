#pragma once

#include <memory>

#include <AppEntityDefines.h>

#include "HTXTimelapseImagingPanelExport.h"
#include "TimelapseImagingDefines.h"
#include "TimelapseImagingTime.h"

namespace HTXpress::AppComponents::TimelapseImagingPanel {
	class HTXTimelapseImagingPanel_API TimelapseSequence {
	public:
		TimelapseSequence();
		TimelapseSequence(const TimelapseSequence& other);
		~TimelapseSequence();

		auto operator=(const TimelapseSequence& other)->TimelapseSequence&;
        auto operator==(const TimelapseSequence& other) const->bool;
        auto operator!=(const TimelapseSequence& other) const->bool;

		auto AddImagingType(AppEntity::ImagingType type)->void;
		auto AddImagingTypes(const QList<AppEntity::ImagingType>& types)->void;
		auto GetImagingTypes() const->QList<AppEntity::ImagingType>;
		auto RemoveImagingType(AppEntity::ImagingType type)->void;

		auto AddFL3DChannel(int32_t channel)->void;
		auto AddFL3DChannels(const QList<int32_t>& channels)->void;
		auto GetFL3DChannels() const->QList<int32_t>;
		auto Remove3DChannels(int32_t channel)->void;

		auto AddFL2DChannel(int32_t channel)->void;
		auto AddFL2DChannels(const QList<int32_t>& channels)->void;
		auto GetFL2DChannels() const->QList<int32_t>;
		auto Remove2DChannels(int32_t channel)->void;

		auto SetInterval(const TimelapseImagingTime& time)->void;
		auto SetInterval(int sec)->void;
		auto GetInterval() const->TimelapseImagingTime;

		auto SetDuration(const TimelapseImagingTime& time)->void;
		auto SetDuration(int sec)->void;
		auto GetDuration() const->TimelapseImagingTime;

		auto SetStartTime(const TimelapseImagingTime& time)->void;
		auto SetStartTime(int sec)->void;
		auto GetStartTime() const->TimelapseImagingTime;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
