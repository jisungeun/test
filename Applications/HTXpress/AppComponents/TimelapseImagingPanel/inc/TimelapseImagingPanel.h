#pragma once

#include <memory>

#include <QWidget>

#include <ImagingScenario.h>

#include "HTXTimelapseImagingPanelExport.h"
#include "TimelapseImagingDefines.h"
#include "TimelapseSequence.h"

namespace HTXpress::AppComponents::TimelapseImagingPanel {
	class HTXTimelapseImagingPanel_API TimelapseImagingPanel : public QWidget {
		Q_OBJECT
	public:
		using Self = TimelapseImagingPanel;

		explicit TimelapseImagingPanel(QWidget* parent = nullptr);
		~TimelapseImagingPanel() override;

		auto SetPanelType(PanelType type) const->void;
		auto SetFLEnabledChannels(Dimension dimension, QList<int32_t> channels)->void;
		auto SetFLChannelsColor(QMap<int32_t, QColor> colors)->void;
		auto SetScenario(const AppEntity::ImagingScenario::Pointer scenario)->void;

		auto GetTimeframes() const -> QList<TimelapseImagingTime>;

		auto AddSequence(const TimelapseSequence& sequence)->void;
	    auto GetSequences() const->QList<TimelapseSequence>;
		auto SetValidationError(const QList<int32_t> columns)->void;

		auto SetAcquisitionTime(const TimelapseImagingTime& time)->void;
		auto SetLeftTime(const TimelapseImagingTime& time)->void;
		auto SetImagingStatus(const TimelapseImagingTime& time, ImagingStatus status)->void;
		auto SetImagingStatus(const int32_t& column, ImagingStatus status)->void;
		auto ResetImagingStatus()->void;

		auto Clear()->void;

		auto ResetTable()->void;

		auto InitEstimatedList() -> void;
		auto SetEstimatedEndTimes(const QMap<int32_t, QPair<TimelapseImagingTime, TimelapseImagingTime>>& plans)->void;
		auto RefreshEstimatedEndTimes() -> void;

		auto GetStartTimes() const -> QMap<int32_t, int32_t>;

		auto IsFLChannelInUse(int32_t channelIndex) const -> bool;
	    auto ChangeCurrentSequenceColumn(const int32_t& column) -> void;

	protected slots:
		void onFLChannelCollapse(bool expanded);
		void onUpdateCounts(const TimelapseImagingTime& time);
		void onSequenceAdded();
		void onDeleted();
		void onCellClicked(int row, int column);

	signals:
		void sigSequencesChanged(const QList<TimelapseSequence>& sequences);

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
