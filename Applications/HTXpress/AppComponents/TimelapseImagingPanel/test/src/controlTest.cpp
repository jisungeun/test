﻿#include <catch2/catch.hpp>

#include <QMap>
#include <QString>
#include <QPair>
#include <QDebug>

namespace HTXpress::AppComponents::TimelapseImagingPanel::UnitTest {
    using TestMap = QMap<int32_t, QPair<QString, QString>>;

    auto ExistKey(const TestMap& map, int32_t key) -> bool {
        if(map.count(key) == 0) return false;
        return true;

    }

    auto GetFirstString(const TestMap& map, int32_t key) -> QString {
        if(!ExistKey(map, key)) return {};
        return map[key].first;
    }

    auto GetSecondString(const TestMap& map, int32_t key) -> QString {
        if(!ExistKey(map, key)) return {};
        return map[key].second;
    }

    TEST_CASE("QMap test") {
        SECTION("Normal use") {
            TestMap map;

            CHECK(map.count(1) == 0);
            CHECK(ExistKey(map, 1) == false);
            CHECK(GetFirstString(map, 1).isNull() == true);
            CHECK(GetSecondString(map, 1).isNull() == true);
            CHECK(map.size() == 0);

            map[1] = {"Hello", "World"};
            CHECK(map.count(1) == 1);

            CHECK(GetFirstString(map, 1) == "Hello");
            CHECK(GetSecondString(map, 1) == "World");

            CHECK(map.size() == 1);
        }

        SECTION("Map clear") {
            TestMap map;
            map[0] = {"aaa", "a00"};
            map[1] = {"bbb", "b11"};
            map[2] = {"ccc", "c22"};
            map[3] = {"ddd", "d33"};

            CHECK(map.size() == 4);
            CHECK(map.empty() == false);
            CHECK(map.isEmpty() == false);

            map.clear();
            CHECK(map.size() == 0);
            CHECK(map.empty() == true);
            CHECK(map.isEmpty() == true);
        }
    }

    TEST_CASE("QString Test") {
        SECTION("Normal case") {
            QString text = "hello\nworld";
            auto splits = text.split("\n");

            CHECK(text.contains("\n")==true);
            CHECK(splits.size() == 2);
            CHECK(splits.first() == "hello");
            CHECK(splits.last() == "world");

        }

        SECTION("There is no cr") {
            QString text = "hello";
            auto splits = text.split("\n");

            CHECK(text.contains("\n")==false);
            CHECK(splits.size() == 1);
            CHECK(splits.first()=="hello");
        }

        SECTION("Replace certain text") {
            QString text = "12:34:56\nab:cd:ef";
            QString after = "gh:ij:kl";
            QString expectedResult = "12:34:56\ngh:ij:kl";

            auto idx = text.indexOf("\n");
            CHECK(text.at(idx+1) == 'a');
            CHECK(idx == 8);
            text.replace(idx+1, after.length(), after);

            qDebug() << "replaced text" << text;

            CHECK(text.length() == expectedResult.length());
            CHECK(text.toStdString() == expectedResult.toStdString());
        }
    }
}
