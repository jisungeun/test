#pragma once

#include <memory>

#include <QMainWindow>

#include <TimelapseSequence.h>

namespace HTXpress::AppComponents::TimelapseImagingPanel::Test {
	class TestMainWindow : public QMainWindow {
		Q_OBJECT

	public:
		TestMainWindow(QWidget* parent = nullptr);
		~TestMainWindow() override;

	public slots:
		void onOpenExperiment();
		void onChangePanelType(int index);
		void onGetSequences();
		void onApplyAcquisitionTime();
		void onApplyLeftTime();
		void onApplyStatus();

	    void onUpdateSequences(const QList<TimelapseSequence>& sequences);

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

}
