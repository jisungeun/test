#include <QFileDialog>
#include <QMessageBox>
#include <QStandardPaths>

#include <System.h>
#include <SessionManager.h>
#include <UserManager.h>
#include <ModelRepo.h>
#include <ExperimentReader.h>
#include <TimelapseImagingPanel.h>
#include <SystemConfigIOReader.h>
#include <ModelLoader.h>
#include <VesselLoader.h>

#include "TestMainWindow.h"
#include "ui_TestMainWindow.h"

namespace HTXpress::AppComponents::TimelapseImagingPanel::Test {
	struct TestMainWindow::Impl {
		Ui::TestMainWindowClass ui;
		AppEntity::SystemConfig::Pointer sysConfig{new AppEntity::SystemConfig() };

		TimelapseImagingPanel* timelapseImagingPanel{ nullptr };

		auto Init()->void;
		auto SetTestScenario(AppEntity::Experiment::Pointer experiment)->void;
	};

    auto TestMainWindow::Impl::Init() -> void {
		auto system = AppEntity::System::GetInstance();
        auto session = AppEntity::SessionManager::GetInstance();
        session->Login(AppEntity::UserManager::GetInstance()->GetUser("Default"));

		const auto topPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
		const auto str = QFileDialog::getOpenFileName(nullptr, "Select TSX system config file", topPath, "INI (*.ini)");

		auto reader = SystemConfigIO::Reader();
		reader.Read(str, sysConfig);

        QFileInfo finfo(str);

		//Load models
        auto modelLoader = AppComponents::ModelLoader::Loader();
        if(!modelLoader.Load(finfo.absolutePath())) {
			QMessageBox::warning(nullptr, "Init", "It fails to load model file");
            return;
        }

        auto model = AppEntity::ModelRepo::GetInstance()->GetModel(sysConfig->GetModel());
        if(!model) {
            sysConfig->SetModel("Invalid");
            return;
        }

        //Load vessels
        AppComponents::VesselLoader::Loader::Load(finfo.absolutePath());

        system->SetSystemConfig(sysConfig);
        system->SetModel(model);
    }

	auto TestMainWindow::Impl::SetTestScenario(AppEntity::Experiment::Pointer experiment) -> void {
	    timelapseImagingPanel->SetScenario(experiment->GetScenario(0));
	}

	TestMainWindow::TestMainWindow(QWidget* parent) : QMainWindow(parent), d{new Impl} {
		d->ui.setupUi(this);

		d->Init();

		d->timelapseImagingPanel = new TimelapseImagingPanel(d->ui.widget);
		d->timelapseImagingPanel->SetFLEnabledChannels(Dimension::Dimension3, {0,1,2});
		
		auto layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->addWidget(d->timelapseImagingPanel);
		d->ui.widget->setLayout(layout);

		d->ui.panelTypeComboBox->addItem("View", PanelType::View);
		d->ui.panelTypeComboBox->addItem("Edit", PanelType::Edit);
		d->ui.panelTypeComboBox->addItem("Acquisition", PanelType::Acquisition);

		d->ui.sequencesTableWidget->setColumnCount(3);
		d->ui.sequencesTableWidget->setHorizontalHeaderLabels(QStringList{"Start", "Duration", "Interval"});

		d->ui.statusComboBox->addItem("Ready", ImagingStatus::Ready);
		d->ui.statusComboBox->addItem("In Progress", ImagingStatus::InProgress);
		d->ui.statusComboBox->addItem("Done", ImagingStatus::Done);
		d->ui.statusComboBox->addItem("Failed", ImagingStatus::Failed);

		connect(d->ui.actionOpen_Experiment_O, &QAction::triggered, this, &TestMainWindow::onOpenExperiment);
		connect(d->ui.actionQuit_Q, &QAction::triggered, qApp, &QCoreApplication::quit);
		connect(d->ui.panelTypeComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
			this, &TestMainWindow::onChangePanelType);

		connect(d->ui.getSequencesButton, &QPushButton::clicked, this, &TestMainWindow::onGetSequences);

		connect(d->ui.applyAcquisitionTimeButton, &QPushButton::clicked, this, &TestMainWindow::onApplyAcquisitionTime);
		connect(d->ui.applyLeftTimeButton, &QPushButton::clicked, this, &TestMainWindow::onApplyLeftTime);
		connect(d->ui.applyStatusButton, &QPushButton::clicked, this, &TestMainWindow::onApplyStatus);

		connect(d->timelapseImagingPanel, &TimelapseImagingPanel::sigSequencesChanged, this, &TestMainWindow::onGetSequences);
	}

	TestMainWindow::~TestMainWindow() {
	}

    void TestMainWindow::onOpenExperiment() {
		static QString prevDir = QString();
		const auto str = QFileDialog::getOpenFileName(this, "Select a experiment file", prevDir, "Experiment (*.tcxexp)");
		if(str.isEmpty()) return;
		prevDir = str;

		auto experiment = ExperimentIO::ExperimentReader().Read(str);
		if(experiment == nullptr) {
			QMessageBox::warning(this, "Open experiment", "It fails to open the experiment file");
			return;
		}

		d->SetTestScenario(experiment);
    }

    void TestMainWindow::onChangePanelType(int index) {
		auto panelType = d->ui.panelTypeComboBox->itemData(index).toInt();
        d->timelapseImagingPanel->SetPanelType(PanelType::_from_integral(panelType));

		d->ui.testControlStackedWidget->setCurrentIndex(index);
    }

	void TestMainWindow::onGetSequences() {
        auto sequences = d->timelapseImagingPanel->GetSequences();

		d->ui.sequencesTableWidget->clear();
		d->ui.sequencesTableWidget->setHorizontalHeaderLabels(QStringList{"Start", "Duration", "Interval"});
		d->ui.sequencesTableWidget->setRowCount(sequences.count());

		for (auto index = 0; index < sequences.count(); ++index) {
			auto seq = sequences.at(index);
			
		    d->ui.sequencesTableWidget->setItem(index, 0, new QTableWidgetItem(seq.GetStartTime().ToString()));
		    d->ui.sequencesTableWidget->setItem(index, 1, new QTableWidgetItem(seq.GetDuration().ToString()));
		    d->ui.sequencesTableWidget->setItem(index, 2, new QTableWidgetItem(seq.GetInterval().ToString()));
		}
    }

	void TestMainWindow::onApplyAcquisitionTime() {
		TimelapseImagingTime time(
            d->ui.acquisitionTimeHourSpinBox->value(),
            d->ui.acquisitionTimeMinuteSpinBox->value(),
            d->ui.acquisitionTimeSecondSpinBox->value()
		);

		d->timelapseImagingPanel->SetAcquisitionTime(time);
    }

	void TestMainWindow::onApplyLeftTime() {
        TimelapseImagingTime time(
            d->ui.leftTimeHourSpinBox->value(),
            d->ui.leftTimeMinuteSpinBox->value(),
            d->ui.leftTimeSecondSpinBox->value()
		);

		d->timelapseImagingPanel->SetLeftTime(time);
    }

	void TestMainWindow::onApplyStatus() {
        TimelapseImagingTime time(
            d->ui.statusTimeHourSpinBox->value(),
            d->ui.statusTimeMinuteSpinBox->value(),
            d->ui.statusTimeSecondSpinBox->value()
		);

		d->timelapseImagingPanel->SetImagingStatus(
			time,
			ImagingStatus::_from_integral(d->ui.statusComboBox->currentData().toInt())
		);
    }

	void TestMainWindow::onUpdateSequences(const QList<TimelapseSequence>& sequences) {
        d->ui.sequencesTableWidget->clear();
		d->ui.sequencesTableWidget->setHorizontalHeaderLabels(QStringList{"Start", "Duration", "Interval"});
		d->ui.sequencesTableWidget->setRowCount(sequences.count());

		for (auto index = 0; index < sequences.count(); ++index) {
			auto seq = sequences.at(index);
			
		    d->ui.sequencesTableWidget->setItem(index, 0, new QTableWidgetItem(seq.GetStartTime().ToString()));
		    d->ui.sequencesTableWidget->setItem(index, 1, new QTableWidgetItem(seq.GetDuration().ToString()));
		    d->ui.sequencesTableWidget->setItem(index, 2, new QTableWidgetItem(seq.GetInterval().ToString()));
		}
    }
}
