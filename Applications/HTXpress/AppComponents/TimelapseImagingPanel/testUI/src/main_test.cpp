#include <QApplication>
#include <QFile>
#include <QTextStream>

#include "TestMainWindow.h"

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    QFile f(":qdarkstyle/style.qss");

	if (!f.exists()) {
		printf("Unable to set stylesheet, file not found\n");
	} else {
		f.open(QFile::ReadOnly | QFile::Text);
		QTextStream ts(&f);
		qApp->setStyleSheet(ts.readAll());
	}

    HTXpress::AppComponents::TimelapseImagingPanel::Test::TestMainWindow w;
    w.show();
    return a.exec();
}
