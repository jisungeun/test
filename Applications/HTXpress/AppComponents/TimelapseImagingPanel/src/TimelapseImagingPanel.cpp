#include "TimelapseImagingPanel.h"

#include <numeric>
#include <QButtonGroup>
#include <QFont>
#include <QDebug>
#include <QScrollBar>
#include <QMap>

#include <UIUtility.h>
#include <MessageDialog.h>

#include "ToggleCellWidget.h"
#include "ModalityCellWidget.h"
#include "TimeChunk.h"
#include "TimeChunksBuilder.h"
#include "TimeSequencesBuilder.h"
#include "ui_TimelapseImagingPanel.h"

namespace HTXpress::AppComponents::TimelapseImagingPanel {
	struct TimelapseImagingPanel::Impl {
		explicit Impl(Self* self) : self(self) {}
		Self* self{};
	    Ui::TimelapseImagingPanel ui;

		const QString startHeaderTitle{"Start time"}; // column 0 title
		const QString startEndDivider{"\n"};
        QMap<int32_t, QString> endTimeColums; // column 0 end time title 포함

		const QMap<Dimension, QString> dimensions{
			{ Dimension::Dimension2, "2D" },
			{ Dimension::Dimension3, "3D" }
		};

		const QMap<Modality, QString> modalities{
		    { Modality::HT, "HT" },
		    { Modality::FL, "FL" },
		    { Modality::FLCH1, "Ch1" },
		    { Modality::FLCH2, "Ch2" },
		    { Modality::FLCH3, "Ch3" },
		    { Modality::BF, "BF" },
		};

		const QMap<ImagingStatus, QColor> imagingStatusColor{
			{ ImagingStatus::Ready,			QColor("#243135") },
			{ ImagingStatus::InProgress,	QColor("#64bc6a") },
			{ ImagingStatus::Done,			QColor("#314b33") },
			{ ImagingStatus::Failed,		QColor("#d55c56") }
		};

		QMap<Dimension, QList<int32_t>> flChannelState{
			{ Dimension::Dimension2, QList<int32_t>() },
			{ Dimension::Dimension3, QList<int32_t>() }
		};

		QMap<int32_t, QColor> flColor;

		std::unique_ptr<QButtonGroup> dimensionButtons{ nullptr };
		std::unique_ptr<QButtonGroup> channelButtons{ nullptr };
		std::unique_ptr<QButtonGroup> colorButtons{ nullptr };

		QMap<TimelapseSequenceIndex, TimelapseSequence> sequences;
		QList<TimelapseImagingTime> imagingTimeFrames;
		QMap<int, TimeChunk> timeChunks;

		int32_t runningSequenceColumn{};

		auto UpdateModalityList()->void;
		auto UpdateModalityOptionUI()->void;
		auto UpdateFLChannelUI()->void;
		auto UpdateModalityCellColor()->void;

		auto GetNewTimelapseSequenceIndex()->TimelapseSequenceIndex;
		auto GetModalityRow(Modality modality)->int32_t;
		auto GetTimeTable()->TimeTable;

		auto BuildTimeChunks()->void;
		auto PrintTimechucks(const QMap<int,TimeChunk>& chunks)->void;
		auto PrintSequences(const QMap<TimelapseSequenceIndex, TimelapseSequence>& sequences)->void;
		auto UpdateTable()->void;
		auto CheckValidation()->QList<int32_t>;
		auto FindSequenceTime(const TimelapseSequence& sequence)->int32_t;	// interval, duration, start time이 같은 sequence의 index 반환

		auto ConvertImagingTypeToRow(AppEntity::ImagingType type, int32_t channel = 0)->int32_t;

		auto CalculateEndTimeInSec(int32_t startInSec, int32_t requiredInSec)->int32_t;
	    [[nodiscard]] auto GetStartTimeTextOnHeader(int32_t column) const -> TimelapseImagingTime;
		[[nodiscard]] auto GetEndTimeTextOnHeader(int32_t column) const -> TimelapseImagingTime;
        auto UpdateEstimatedEndTimeToHeader() -> void;

		auto ConnectTimeInputFrameTabOrder()->void;

		auto ClearContents() -> void;
        auto ScrollToColumn(int32_t column) -> void;
    };

	auto TimelapseImagingPanel::Impl::CalculateEndTimeInSec(int32_t startInSec, int32_t requiredInSec) -> int32_t {	
		return startInSec + requiredInSec;
	}

	auto TimelapseImagingPanel::Impl::GetStartTimeTextOnHeader(int32_t column) const -> TimelapseImagingTime {
        const auto horizontalHeaderItem = ui.timelapseTableWidget->horizontalHeaderItem(column);
        const auto list = horizontalHeaderItem->text().split(startEndDivider);
        return TimelapseImagingTime::FromString(list.first());
    }

    auto TimelapseImagingPanel::Impl::GetEndTimeTextOnHeader(int32_t column) const -> TimelapseImagingTime {
        auto horizontalHeaderItem = ui.timelapseTableWidget->horizontalHeaderItem(column);
        auto list = horizontalHeaderItem->text().split(startEndDivider);
        if(list.size() != 2) {
            return {};
        }
        return TimelapseImagingTime::FromString(list.last());
    }

    auto TimelapseImagingPanel::Impl::UpdateEstimatedEndTimeToHeader() -> void {
        for(int c = 0; c < ui.timelapseTableWidget->columnCount(); ++c) {
            const auto headerItem = ui.timelapseTableWidget->horizontalHeaderItem(c);
            if(headerItem->text().contains(startEndDivider) == false) {
                auto currText = ui.timelapseTableWidget->horizontalHeaderItem(c)->text();
                ui.timelapseTableWidget->horizontalHeaderItem(c)->setText(currText + startEndDivider + endTimeColums[c]);
            } 
			else {
				if(c==0) continue;

                auto headerText = headerItem->text();
                auto idx = headerItem->text().indexOf(startEndDivider);
                headerText.replace(idx + 1, endTimeColums[c].length(), endTimeColums[c]);
                headerItem->setText(headerText);
            }
        }
    }

    auto TimelapseImagingPanel::Impl::ConnectTimeInputFrameTabOrder() -> void {
		connect(ui.intervalInput, &TimeInputFrame::sigNextHourEdit, ui.durationInput, &TimeInputFrame::onSetHourEditFocus);
		connect(ui.durationInput, &TimeInputFrame::sigNextHourEdit, ui.startTimeInput, &TimeInputFrame::onSetHourEditFocus);

	    connect(ui.startTimeInput, &TimeInputFrame::sigPreviousSecondEdit, ui.durationInput, &TimeInputFrame::onSetSecondEditFocus);
		connect(ui.durationInput, &TimeInputFrame::sigPreviousSecondEdit, ui.intervalInput, &TimeInputFrame::onSetSecondEditFocus);
    }

    auto TimelapseImagingPanel::Impl::ClearContents() -> void {
		sequences.clear();
		timeChunks.clear();

		self->ResetTable();

	    ui.acquisitionTimeInput->Clear();
		ui.leftTimeInput->Clear();

		ui.intervalInput->Clear();
		ui.durationInput->Clear();
		ui.startTimeInput->Clear();

		endTimeColums.clear();
    }

    auto TimelapseImagingPanel::Impl::ScrollToColumn(int32_t column) -> void {
        const auto tableWidgetItem = ui.timelapseTableWidget->item(0, column);
        if (tableWidgetItem) {
            ui.timelapseTableWidget->scrollToItem(tableWidgetItem, QAbstractItemView::PositionAtCenter);
        }
    }

    auto TimelapseImagingPanel::Impl::UpdateModalityList() -> void {
	    ui.modalityComboBox->blockSignals(true);

		const auto currentDimension = dimensionButtons->checkedId();
		const auto currentModality = ui.modalityComboBox->currentData();

		ui.modalityComboBox->clear();

	    for (auto key : modalities.keys()) {
		    if (key == +Modality::FLCH1 || key == +Modality::FLCH2 || key == +Modality::FLCH3) {
		        continue;
		    }

			// 2D 선택된 경우 modality combobox에서 HT 항목 제외
			if ( currentDimension == Dimension::Dimension2 && key == +Modality::HT ) {
				continue;
			}

			if (currentDimension == Dimension::Dimension3 && key == +Modality::BF) {
				continue;
			}

		    ui.modalityComboBox->addItem(modalities.value(key), key._to_integral());
	    }

		// 선택되어있던 modality가 사라졌으면 첫 아이템으로 선택 변경
		if (ui.modalityComboBox->findData(currentModality) < 0) {
			ui.modalityComboBox->setCurrentIndex(0);
		}

		ui.modalityComboBox->blockSignals(false);
    }

	auto TimelapseImagingPanel::Impl::UpdateModalityOptionUI() -> void {
        if (ui.modalityComboBox->currentData() == Modality::HT) {
			ui.modalityOptionsWidget->setCurrentWidget(ui.emptyPage);
		} else if (ui.modalityComboBox->currentData() == Modality::FL) {
			ui.modalityOptionsWidget->setCurrentWidget(ui.channelPage);
			UpdateFLChannelUI();
		} else if (ui.modalityComboBox->currentData() == Modality::BF) {
			// BF modality일 때 항상 Gray로 촬영되도록 설정. 추후 Gray/Color 설정 필요하면 사용. 
			//d->ui.modalityOptionsWidget->setCurrentWidget(d->ui.colorPage);
			ui.modalityOptionsWidget->setCurrentWidget(ui.emptyPage);
		}
    }

	auto TimelapseImagingPanel::Impl::UpdateFLChannelUI() -> void {
		if (ui.modalityComboBox->currentData() != Modality::FL) return;

		auto dimension = dimensionButtons->checkedId();
		auto channels = flChannelState.value(Dimension::_from_index(dimension));

		QList<int> prevChecked;
		for (auto channelButton : channelButtons->buttons()) {
			if (channelButton->isChecked()) prevChecked << channelButtons->id(channelButton);

			ui.flCh1Label->setEnabled(false);
			ui.flCh2Label->setEnabled(false);
			ui.flCh3Label->setEnabled(false);

			channelButton->blockSignals(true);
		    channelButton->setEnabled(false);
			channelButton->setChecked(false);
		    channelButton->blockSignals(false);
		}

		for (auto enabledChannel : channels) {
			const auto channelCheckbox = channelButtons->button(enabledChannel);
			if (channelCheckbox == nullptr) continue;

			if (enabledChannel == 0) ui.flCh1Label->setEnabled(true);
			else if (enabledChannel == 1) ui.flCh2Label->setEnabled(true);
			else if (enabledChannel == 2) ui.flCh3Label->setEnabled(true);

		    channelCheckbox->setEnabled(true);
			if (prevChecked.contains(enabledChannel)) channelCheckbox->setChecked(true);
		}
    }

	auto TimelapseImagingPanel::Impl::UpdateModalityCellColor() -> void {
		for (auto row = 0; row < ui.timelapseTableWidget->rowCount(); ++row) {
			const auto cellItem = ui.timelapseTableWidget->item(row, 0);
			if (cellItem == nullptr) continue;

			cellItem->setBackground(QColor("#2d3e43"));

			const auto modality = cellItem->data(Qt::UserRole).toInt();
			if (modality == +Modality::FLCH1 || 
				modality == +Modality::FLCH2 ||
				modality == +Modality::FLCH3) {
				const auto modalityCellWidget = qobject_cast<ModalityCellWidget*>(ui.timelapseTableWidget->cellWidget(row, 0));
				if (modalityCellWidget == nullptr) continue;

				modalityCellWidget->SetChannelColor(flColor.value(modality - Modality::FLCH1));
			}
		}
    }

	auto TimelapseImagingPanel::Impl::GetNewTimelapseSequenceIndex()->TimelapseSequenceIndex {
		auto indices = sequences.keys();
		if (indices.isEmpty()) return 0;

		auto result = std::minmax_element(indices.begin(), indices.end());
		return *result.second + 1;
	}

	auto TimelapseImagingPanel::Impl::GetModalityRow(Modality modality)->int32_t {
		auto row = 0;
	    for (; row < ui.timelapseTableWidget->rowCount(); ++row) {
	        auto cellItem = ui.timelapseTableWidget->item(row, 0);
			if (cellItem == nullptr) continue;

			if (cellItem->data(Qt::UserRole).toInt() == modality) return row;
	    }

		return -1;
	}

	auto TimelapseImagingPanel::Impl::GetTimeTable() -> TimeTable {
        // TimeSequencesBuilder에 전달할 time table 정보 생성
		TimeTable timeTable;

		// 유효한 timestamp(column) 목록 구성
		QList<int32_t> validColumns;
		for (auto column = 1; column < ui.timelapseTableWidget->columnCount(); ++column) {
			if (ui.timelapseTableWidget->isColumnHidden(column)) continue;

			bool emptyColumn = true;
			for (auto row = 0; row < ui.timelapseTableWidget->rowCount(); ++row) {
				auto cellItem = ui.timelapseTableWidget->item(row, column);
				if (cellItem == nullptr) continue;

				if (!cellItem->text().isEmpty()) {
					emptyColumn = false;
					break;
				}
			}

			if (!emptyColumn) {
				validColumns << column;
				timeTable.timeStamps << GetStartTimeTextOnHeader(column);
			}
		}

		// TimeTable 생성
		for (auto row = 0; row < ui.timelapseTableWidget->rowCount(); ++row) {
			auto cellItem = ui.timelapseTableWidget->item(row, 0);
			if (cellItem == nullptr) continue;
			if (cellItem->data(Qt::UserRole).toInt() == Modality::FL) continue;

			timeTable.modalities << Modality::_from_integral(cellItem->data(Qt::UserRole).toInt());

			QList<TimeTable::ValueType> cellValues;
			for (auto &column: validColumns) {
				TimeTable::ValueType value = TimeTable::Empty;
				auto cellLabel = ui.timelapseTableWidget->item(row, column)->text();
				if (cellLabel == "2D") {
					value = TimeTable::Dim2;
				} else if (cellLabel == "3D") {
					value = TimeTable::Dim3;
				}

				cellValues << value;
			}

			timeTable.cells << cellValues;
		}

		return timeTable;
    }

	auto TimelapseImagingPanel::Impl::BuildTimeChunks()->void {
		TimeChunksBuilder builder(sequences);
		timeChunks = builder.Build();
	}

    auto TimelapseImagingPanel::Impl::PrintTimechucks(const QMap<int, TimeChunk>& chunks) -> void {
		const auto keys = chunks.keys();
		for(const auto key : keys) {
			const auto& chunk = chunks[key];
			qDebug() << "[" << key << "] s:" << chunk.GetStart() << " e: " << chunk.GetEnd() << " i:" << chunk.GetInterval() << " d:" << chunk.GetDuration();
		}
    }

    auto TimelapseImagingPanel::Impl::PrintSequences(const QMap<TimelapseSequenceIndex, TimelapseSequence>& seqs) -> void {
		qDebug() << "  sequences=" << seqs.count();
	    QMapIterator sequencesIt(seqs);
		int32_t idx = 0;
		while (sequencesIt.hasNext()) {
		    sequencesIt.next();
			auto sequence = sequencesIt.value();
			qDebug() << "  [" << idx++ << "] s:" << sequence.GetStartTime().ToString() << " i:" << sequence.GetInterval().ToString() << " d:" << sequence.GetDuration().ToString() << " - " << sequence.GetImagingTypes();
		}
    }

    auto TimelapseImagingPanel::Impl::UpdateTable() -> void {
		// TODO: optimization and refactoring

		// time frames 추가
	    QStringList timeframes;

	    QMapIterator timeChunksIt(timeChunks);
		while (timeChunksIt.hasNext()) {
		    timeChunksIt.next();

			const auto timeChunk = timeChunksIt.value();
			auto counts = [&]()->int32_t {
			    if (timeChunk.GetInterval() == 0) return 1;
			    return timeChunk.GetDuration() / timeChunk.GetInterval() + 1;
			}();

			for (auto index = 0; index < counts; ++index) {
				auto timeLabel = TimelapseImagingTime(timeChunk.GetStart() + timeChunk.GetInterval() * index).ToString();
				if (!timeframes.contains(timeLabel)) {
				    timeframes << timeLabel;
				}
			}
		}

		timeframes.insert(0, startHeaderTitle);

		ui.timelapseTableWidget->setColumnCount(timeframes.count());
		ui.timelapseTableWidget->setHorizontalHeaderLabels(timeframes);

		// 모든 cell에 cell item을 할당
		// cell을 선택하지 않은 상태와 빈 cell을 선택한 상태를 구분하기 위한 용도
		// QTableWidget은 cell item이 없는 cell이 선택된 경우도 아무것도 선택되지 않은 것으로 간주함
		for (auto row = 0; row < ui.timelapseTableWidget->rowCount(); ++row) {
		    for (auto column = 1; column < ui.timelapseTableWidget->columnCount(); ++column) {
		        auto cellItem = new QTableWidgetItem("");

				auto font = cellItem->font();
				font.setBold(true);
				font.setPixelSize(10);
				cellItem->setFont(font);

				cellItem->setData(Qt::UserRole, -1);
				cellItem->setTextAlignment(Qt::AlignCenter);
				cellItem->setBackground(imagingStatusColor.value(ImagingStatus::Ready));
				
		        ui.timelapseTableWidget->setItem(row, column, cellItem);
				ui.timelapseTableWidget->showColumn(column);
		    }
		}

		QList<int> hiddenColumns;
		for (auto column = 1; column < ui.timelapseTableWidget->columnCount(); ++column) {
		    hiddenColumns << column;
		}

		auto SetCellItem = [&](AppEntity::ImagingType type, int32_t row, int32_t column, const QString& label, const TimelapseSequenceIndex sequenceIndex) {
			auto tableItem = ui.timelapseTableWidget->item(row, column);
			if (tableItem == nullptr) {
				tableItem = new QTableWidgetItem;
			}

			tableItem->setText(label);
			tableItem->setData(Qt::UserRole, sequenceIndex);

			if (hiddenColumns.contains(column)) {
				hiddenColumns.removeOne(column);
			}

			// FL cell의 3D/2D 표시 결정
			auto flItem = ui.timelapseTableWidget->item(Modality::FL, column);
			if (flItem == nullptr) return;

			if (type == +AppEntity::ImagingType::FL3D) {
			    flItem->setText("3D");
			} else if (type == +AppEntity::ImagingType::FL2D) {
				bool found3D = false;
				for (auto ch : {Modality::FLCH1, Modality::FLCH2, Modality::FLCH3}) {
					auto channelItem = ui.timelapseTableWidget->item(ch, column);
					if (channelItem && channelItem->text() == "3D") {
						found3D = true;
					    break;
					}
				}

				flItem->setText(found3D ? "3D" : "2D");
			}
		};

		// cell 값 추가
		QMapIterator sequenceIt(sequences);
		while (sequenceIt.hasNext()) {
		    sequenceIt.next();
			auto sequence = sequenceIt.value();

			const auto startTime = sequence.GetStartTime().ToSeconds();
			const auto duration = sequence.GetDuration().ToSeconds();
			const auto interval = sequence.GetInterval().ToSeconds();

			auto imagingTypes = sequence.GetImagingTypes();

			const auto timeCounts = (interval==0) ? 1 : duration / interval + 1;
			for (auto index = 0; index < timeCounts; ++index) {
			    auto timeLabel = TimelapseImagingTime(startTime + interval * index).ToString();
			    const auto column = timeframes.indexOf(timeLabel);
				if (column < 0) {
				    continue;
				}

				// sequence에 포함된 모든 imaging type의 time frame을 cell에 표시
				for (auto& imagingType : imagingTypes) {
					QString cellLabel;
				    switch (imagingType) {
						case AppEntity::ImagingType::HT3D:
						case AppEntity::ImagingType::FL3D:
						    cellLabel = "3D";
							break;
						case AppEntity::ImagingType::HT2D:
						case AppEntity::ImagingType::FL2D:
						case AppEntity::ImagingType::BFColor:
						case AppEntity::ImagingType::BFGray:
						    cellLabel = "2D";
							break;
                    }

					if (imagingType == +AppEntity::ImagingType::FL3D ||
						imagingType == +AppEntity::ImagingType::FL2D) {

						QList<int32_t> channels = (imagingType == +AppEntity::ImagingType::FL3D) ? sequence.GetFL3DChannels() : sequence.GetFL2DChannels();
						if (channels.isEmpty()) {
							continue;
						}

						for (auto channel : channels) {
						    int32_t row  = ConvertImagingTypeToRow(imagingType, channel);
							if (row < 0) {
							    continue;
							}

							SetCellItem(imagingType, row, column, cellLabel, sequenceIt.key());
						}

					} else {
						int32_t row = ConvertImagingTypeToRow(imagingType);
                        if (row < 0) {
							continue;
						}

						SetCellItem(imagingType, row, column, cellLabel, sequenceIt.key());
					}
				}
			}
		}

		// imaging이 없는 timeframe(column) 숨김 처리
		for (auto column : hiddenColumns) {
			ui.timelapseTableWidget->hideColumn(column);
		}

		// table에 보이는 timeframe 목록 업데이트
		imagingTimeFrames.clear();
		for (auto column = 1; column < ui.timelapseTableWidget->columnCount(); ++column) {
		    if (ui.timelapseTableWidget->isColumnHidden(column)) continue;

			auto headerItem = ui.timelapseTableWidget->horizontalHeaderItem(column);
			if (headerItem == nullptr) return;

            imagingTimeFrames << TimelapseImagingTime::FromString(headerItem->text());
		}

		// check validation
		
	}

	auto TimelapseImagingPanel::Impl::CheckValidation() -> QList<int32_t> {
		auto findSequence = [=](const TimelapseImagingTime& time)->TimelapseSequenceIndex {
		    TimelapseSequenceIndex index = -1;

			auto seqIt = QMapIterator(sequences);
			while (seqIt.hasNext()) {
			    seqIt.next();

				auto seq = seqIt.value();
				auto seqStart = seq.GetStartTime().ToSeconds();
				auto seqDuration = seq.GetDuration().ToSeconds();

			    if (seqStart == time.GetSecond() || 
					seqStart + seqDuration >= time.ToSeconds()) {
				    index = seqIt.key();
					break;
				}
			}

			return index;
		};

		QList<int32_t> invalidColumns;
		auto columCount = ui.timelapseTableWidget->columnCount();
		for (auto col = 1; col < columCount - 1; ++col) {
			auto endTime = GetEndTimeTextOnHeader(col);
			auto nextStartTime = GetStartTimeTextOnHeader(col+1);
			if (endTime.ToSeconds() > nextStartTime.ToSeconds()) {
			    invalidColumns << col;
			}
		}

		return invalidColumns;
    }

	auto TimelapseImagingPanel::Impl::FindSequenceTime(const TimelapseSequence& sequence) -> TimelapseSequenceIndex {
	    auto sequencesIt = QMapIterator(sequences);
		while (sequencesIt.hasNext()) {
		    sequencesIt.next();

			auto hasSequence = sequencesIt.value();
			if (sequence.GetInterval() == hasSequence.GetInterval() &&
				sequence.GetDuration() == hasSequence.GetDuration() &&
				sequence.GetStartTime() == hasSequence.GetStartTime()) {
			    return sequencesIt.key();
			}
		}

		return -1;
	}

	auto TimelapseImagingPanel::Impl::ConvertImagingTypeToRow(AppEntity::ImagingType type, int32_t channel) -> int32_t {
		int32_t row = -1;

        switch (type) {
		case AppEntity::ImagingType::HT3D:
		case AppEntity::ImagingType::HT2D:
			row = Modality(Modality::HT)._to_integral();
			break;
		case AppEntity::ImagingType::FL3D:
		case AppEntity::ImagingType::FL2D:
			row = Modality(Modality::FL)._to_integral();
			row += channel + 1;
			break;
		case AppEntity::ImagingType::BFGray:
		case AppEntity::ImagingType::BFColor:
			row = Modality(Modality::BF)._to_integral();
			break;
        }

		return row;
    }

	TimelapseImagingPanel::TimelapseImagingPanel(QWidget* parent) : QWidget(parent), d{ std::make_unique<Impl>(this) } {
        d->ui.setupUi(this);
		d->ConnectTimeInputFrameTabOrder();

		d->ui.baseWidget->setObjectName("panel");
		d->ui.timelapseLabel->setObjectName("label-h3");
		d->ui.flCh1Label->setObjectName("label-h5");
		d->ui.flCh2Label->setObjectName("label-h5");
		d->ui.flCh3Label->setObjectName("label-h5");
		d->ui.intervalLabel->setObjectName("label-h5");
		d->ui.durationLabel->setObjectName("label-h5");
		d->ui.countsLabel->setObjectName("label-h5");
		d->ui.startLabel->setObjectName("label-h5");

		d->ui.acquisitionTimeLabel->setObjectName("label-h3");
		d->ui.leftTimeLabel->setObjectName("label-h3");

		d->ui.timelapseTableWidget->setObjectName("table-timelapse");
		d->ui.dimension3DButton->setObjectName("bt-toggle-left");
		d->ui.dimension2DButton->setObjectName("bt-toggle-right");
		d->ui.deleteButton->setObjectName("bt-tsx-delete");
		d->ui.addButton->setObjectName("bt-tsx-add-sequence");

		d->ui.countsEdit->setText("000");
        d->ui.countsEdit->setStyleSheet(QString("background: #172023;" 
												"border: 1px solid #2d3e43;" 
												"border-radius: 6px;" 
												"font-size: 10px;"));

	    for (auto pageIndex = 0; pageIndex < d->ui.parametersStackedWidget->count(); ++pageIndex) {
	        auto pageWidget = d->ui.parametersStackedWidget->widget(pageIndex);
			if (pageWidget) pageWidget->setObjectName("panel-light");
	    }

		for (auto pageIndex = 0; pageIndex < d->ui.parametersStackedWidget->count(); ++pageIndex) {
	        auto pageWidget = d->ui.parametersStackedWidget->widget(pageIndex);
			if (pageWidget) pageWidget->setObjectName("panel-light");
	    }

		d->ui.timelapseTableWidget->verticalHeader()->hide();

		// 2D/3D 버튼
		d->dimensionButtons = std::make_unique<QButtonGroup>();
		d->dimensionButtons->setExclusive(true);
		d->dimensionButtons->addButton(d->ui.dimension3DButton, Dimension::Dimension3);
		d->dimensionButtons->addButton(d->ui.dimension2DButton, Dimension::Dimension2);

		connect(d->dimensionButtons.get(), QOverload<QAbstractButton *>::of(&QButtonGroup::buttonClicked), [=](QAbstractButton *button) {
		    Q_UNUSED(button);
			d->UpdateModalityList();
			d->UpdateModalityOptionUI();
		});

		connect(d->ui.modalityComboBox, &QComboBox::currentTextChanged, [=](const QString& text) {
			Q_UNUSED(text)
			d->UpdateModalityOptionUI();
		});

		d->ui.dimension3DButton->setChecked(true);
		d->UpdateModalityList();

		d->ui.modalityComboBox->setCurrentText(d->modalities.value(Modality::HT));

		// modality 별 옵션(channel, color)
		d->channelButtons = std::make_unique<QButtonGroup>();
		d->channelButtons->addButton(d->ui.ch1CheckBox, 0);
		d->channelButtons->addButton(d->ui.ch2CheckBox, 1);
		d->channelButtons->addButton(d->ui.ch3CheckBox, 2);
		d->channelButtons->setExclusive(false);

		d->colorButtons = std::make_unique<QButtonGroup>();
		d->colorButtons->addButton(d->ui.grayRadioButton);
		d->colorButtons->addButton(d->ui.colorRadioButton);

		connect(d->ui.checkAutoScroll, &QCheckBox::toggled, this, [this](bool toggled) {
			if(toggled) {
				d->ScrollToColumn(d->runningSequenceColumn);
			}
		});

		connect(d->ui.intervalInput, &TimeInputFrame::sigTimeChanged, this, &TimelapseImagingPanel::onUpdateCounts);
		connect(d->ui.durationInput, &TimeInputFrame::sigTimeChanged, this, &TimelapseImagingPanel::onUpdateCounts);

		connect(d->ui.addButton, &QPushButton::clicked, this, &TimelapseImagingPanel::onSequenceAdded);
		connect(d->ui.deleteButton, &QPushButton::clicked, this, &TimelapseImagingPanel::onDeleted);

		connect(d->ui.timelapseTableWidget, &QTableWidget::cellClicked, this, &TimelapseImagingPanel::onCellClicked);
		d->ui.acquisitionTimeInput->SetReadOnly(true);
		d->ui.leftTimeInput->SetReadOnly(true);

		ResetTable();

	    SetPanelType(PanelType::View);

		// init row/col section size
		for(int i = 0; i < d->ui.timelapseTableWidget->rowCount(); ++i) {
		    d->ui.timelapseTableWidget->setRowHeight(i, 28);
		}
		d->ui.timelapseTableWidget->horizontalHeader()->setDefaultSectionSize(65);
		d->ui.timelapseTableWidget->setColumnWidth(0, 85);
    }

	TimelapseImagingPanel::~TimelapseImagingPanel() {
        
    }

	auto TimelapseImagingPanel::SetPanelType(PanelType type) const -> void {
		if (type == +PanelType::View) {
		    d->ui.parametersStackedWidget->hide();
		} else {
			d->ui.parametersStackedWidget->setCurrentIndex(type._to_integral() - 1);
		    d->ui.parametersStackedWidget->show();
		}

		if (type != +PanelType::Acquisition) {
			for (auto row = 0; row < d->ui.timelapseTableWidget->rowCount(); ++row) {
			    for (auto column = 1; column < d->ui.timelapseTableWidget->columnCount(); ++column) {
					if (const auto cellItem = d->ui.timelapseTableWidget->item(row, column)) {
			            cellItem->setBackground(QColor(Qt::white));
					}
			    }
			}
		}
    }

	auto TimelapseImagingPanel::SetFLEnabledChannels(Dimension dimension, QList<int32_t> channels) -> void {
		d->flChannelState.insert(dimension, channels);
		d->UpdateFLChannelUI();
    }

	auto TimelapseImagingPanel::SetFLChannelsColor(QMap<int32_t, QColor> colors) -> void {
		d->flColor = colors;
		d->UpdateModalityCellColor();
    }

	auto TimelapseImagingPanel::SetScenario(const AppEntity::ImagingScenario::Pointer scenario) -> void {
		Clear();

		if (scenario == nullptr) {
		    return;
		}

		// ImagingSequence에 포함된 Modalities를 개별 TimelapseSequence로 다룬다.
		// FL imaging condition의 경우 사용되는 채널도 모두 각 TimelapseSequence로 다룬다.

		QList<TimelapseSequence> tableTimeSequences;

		qDebug() << "sequence count = " << scenario->GetCount();

	    for (auto sequenceIndex = 0; sequenceIndex < scenario->GetCount(); ++sequenceIndex) {
		    const auto sequence = scenario->GetSequence(sequenceIndex);
			const auto startTime = scenario->GetStartTime(sequenceIndex);
			const auto interval = sequence->GetInterval();

			qDebug() << "[" << sequenceIndex << "] " << "modalities=" << sequence->GetModalityCount() << " startTime=" << startTime << " interval=" << interval;
			
		    for (auto conditionIndex = 0; conditionIndex < sequence->GetModalityCount(); ++conditionIndex) {
				TimelapseSequence timelapseSequence;
		        timelapseSequence.SetStartTime(startTime);
				timelapseSequence.SetInterval(interval);
				timelapseSequence.SetDuration((sequence->GetTimeCount() - 1) * interval);

				auto timelapseSequenceIndex = d->FindSequenceTime(timelapseSequence);
				if (timelapseSequenceIndex < 0) {
				    timelapseSequenceIndex = d->GetNewTimelapseSequenceIndex();
				} else {
				    timelapseSequence = d->sequences.value(sequenceIndex);
				}

		        auto condition = sequence->GetImagingCondition(conditionIndex);

				switch (condition->GetModality()) {
				case AppEntity::Modality::HT: {
				    const auto htCondition = std::dynamic_pointer_cast<AppEntity::ImagingConditionHT>(condition);

					timelapseSequence.AddImagingType(htCondition->Is3D() ? AppEntity::ImagingType::HT3D : AppEntity::ImagingType::HT2D);
					d->sequences.insert(timelapseSequenceIndex, timelapseSequence);
					break;
				}
				case AppEntity::Modality::FL: {
					const auto flCondition = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(condition);

					const auto is3D = flCondition->Is3D();
					timelapseSequence.AddImagingType(is3D ? AppEntity::ImagingType::FL3D : AppEntity::ImagingType::FL2D);

				    for (auto channel : flCondition->GetChannels()) {
						if (channel < 0 || channel > 2) continue; // FL 채널은 0~2 사용

						if (is3D) timelapseSequence.AddFL3DChannel(channel);
				        else timelapseSequence.AddFL2DChannel(channel);

				        d->sequences.insert(timelapseSequenceIndex, timelapseSequence);
				    }

					break;
				}
				case AppEntity::Modality::BF: {
				    timelapseSequence.AddImagingType(AppEntity::ImagingType::BFGray);
                    d->sequences.insert(timelapseSequenceIndex, timelapseSequence);
					break;
				}
				}
		    }
		}

	    d->BuildTimeChunks();
		d->UpdateTable();
		d->sequences = TimeSequencesBuilder(d->GetTimeTable()).Build();
    }

	auto TimelapseImagingPanel::GetTimeframes() const -> QList<TimelapseImagingTime> {
        return d->imagingTimeFrames;
    }

	auto TimelapseImagingPanel::AddSequence(const TimelapseSequence& sequence) -> void {
		auto index = d->FindSequenceTime(sequence);
		if (index < 0) {
			// 같은 시간조건의 sequence가 없으면 새로 추가
		    d->sequences.insert(d->GetNewTimelapseSequenceIndex(), sequence);
		} else {
			// 같은 시간조건의 sequence가 있으면 기존 sequence에 내용 추가
		    auto hasSequence = d->sequences.value(index);
			hasSequence.AddImagingTypes(sequence.GetImagingTypes());
			hasSequence.AddFL3DChannels(sequence.GetFL3DChannels());
			hasSequence.AddFL2DChannels(sequence.GetFL2DChannels());

			d->sequences.insert(index, hasSequence);
		}

		d->BuildTimeChunks();
		d->UpdateTable();
		d->sequences = TimeSequencesBuilder(d->GetTimeTable()).Build();

		// scroll to added item
	    auto startTime = sequence.GetStartTime();
		for (auto column = 1 ; column < d->ui.timelapseTableWidget->columnCount(); ++column) {
			if (startTime != d->GetStartTimeTextOnHeader(column)) continue;

			if (const auto* item = d->ui.timelapseTableWidget->item(0, column)) {
			    d->ui.timelapseTableWidget->scrollToItem(item, QAbstractItemView::PositionAtCenter);
				break;
		    }
		}

		emit sigSequencesChanged(GetSequences());
	}

	auto TimelapseImagingPanel::GetSequences() const -> QList<TimelapseSequence> {
		if (d->sequences.isEmpty()) return QList<TimelapseSequence>();
		return d->sequences.values();
    }

	auto TimelapseImagingPanel::SetValidationError(const QList<int32_t> columns) -> void {
		for (auto row = 0; row < d->ui.timelapseTableWidget->rowCount(); ++row) {
		    for (auto column = 1; column < d->ui.timelapseTableWidget->columnCount(); ++column) {
				auto color = d->imagingStatusColor.value(ImagingStatus::Ready);
				if (columns.contains(column)) {
				    color = d->imagingStatusColor.value(ImagingStatus::Failed);
				}

				auto item = d->ui.timelapseTableWidget->item(row, column);
				if (item == nullptr) continue;

				item->setBackground(color);
			}
		}
    }

	auto TimelapseImagingPanel::SetAcquisitionTime(const TimelapseImagingTime& time) -> void {
        d->ui.acquisitionTimeInput->SetTime(time);
    }

	auto TimelapseImagingPanel::SetLeftTime(const TimelapseImagingTime& time) -> void {
        d->ui.leftTimeInput->SetTime(time);
    }

	auto TimelapseImagingPanel::SetImagingStatus(const TimelapseImagingTime& time, ImagingStatus status) -> void {
		auto SetColumnColor = [=](int column, QColor color) {
		    for (auto row = 0; row < d->ui.timelapseTableWidget->rowCount(); ++row) {
				d->ui.timelapseTableWidget->item(row, column)->setBackground(color);
			}
		};

		const auto columnCount = d->ui.timelapseTableWidget->columnCount();
		for (auto column = 1; column < columnCount; ++column) {
			auto headerTime = d->GetStartTimeTextOnHeader(column);
			if (headerTime != time) continue;

			SetColumnColor(column, d->imagingStatusColor.value(status));
			break;
		}
    }

    auto TimelapseImagingPanel::SetImagingStatus(const int32_t& column, ImagingStatus status) -> void {
		if(column < 1 || column >= d->ui.timelapseTableWidget->columnCount()) {
			return;
		}

		for (auto row = 0; row < d->ui.timelapseTableWidget->rowCount(); ++row) {
			const auto tableWidgetItem = d->ui.timelapseTableWidget->item(row, column);
			if(tableWidgetItem) {
			    tableWidgetItem->setBackground(d->imagingStatusColor[status]);
			}
		}
	}

    auto TimelapseImagingPanel::ResetImagingStatus() -> void {
        for (auto row = 0; row < d->ui.timelapseTableWidget->rowCount(); ++row) {
			for (auto column = 1; column < d->ui.timelapseTableWidget->columnCount(); ++column) {
			    const auto cellItem = d->ui.timelapseTableWidget->item(row, column);
			    if (cellItem == nullptr) continue;

			    cellItem->setBackground(d->imagingStatusColor.value(ImagingStatus::Ready));
			}
		}
		d->ui.checkAutoScroll->setChecked(true);
		d->runningSequenceColumn = 0;
    }

	auto TimelapseImagingPanel::Clear() -> void {
		d->flChannelState = {
			{ Dimension::Dimension2, QList<int32_t>() },
			{ Dimension::Dimension3, QList<int32_t>() }
		};
		d->ClearContents();
	}

	auto TimelapseImagingPanel::ResetTable()->void {
		d->ui.timelapseTableWidget->clear();

	    d->ui.timelapseTableWidget->setRowCount(6);
		d->ui.timelapseTableWidget->setColumnCount(1);

		QStringList horizontalHeaderLabels;
		for (auto index = 0; index < d->ui.timelapseTableWidget->columnCount(); ++index) {
			if (index == 0) {
				horizontalHeaderLabels << d->startHeaderTitle;
			} else {
			    horizontalHeaderLabels << "";
			}
		}

		d->ui.timelapseTableWidget->setHorizontalHeaderLabels(horizontalHeaderLabels);

		QMapIterator modalityIt(d->modalities);
		while (modalityIt.hasNext()) {
		    modalityIt.next();

			const auto modality = modalityIt.key();
			const QString cellLabel = modalityIt.value();
			const QColor cellColor = QColor("#2d3e43");

			if (modality == +Modality::FL) {
				const auto flCellWidget = new ToggleCellWidget(cellLabel);
			    d->ui.timelapseTableWidget->setCellWidget(modality, 0, flCellWidget);

				connect(flCellWidget, &ToggleCellWidget::toggled, this, &TimelapseImagingPanel::onFLChannelCollapse);
			} else {
			    const auto modalityCellWidget = new ModalityCellWidget;
				modalityCellWidget->SetText(cellLabel);
				if (modalityIt.key() == +Modality::FLCH1 || 
					modalityIt.key() == +Modality::FLCH2 ||
					modalityIt.key() == +Modality::FLCH3) {
					modalityCellWidget->SetChannelColor(d->flColor.value(modalityIt.key() - Modality::FLCH1));
				}

			    d->ui.timelapseTableWidget->setCellWidget(modalityIt.key(), 0, modalityCellWidget);
			}

			const auto cellItem = new QTableWidgetItem;
			cellItem->setData(Qt::UserRole, modality._to_integral());
			cellItem->setBackground(cellColor);

		    d->ui.timelapseTableWidget->setItem(modalityIt.key(), 0, cellItem);
		}
	}

	void TimelapseImagingPanel::onFLChannelCollapse(bool expanded) {
		const auto table = d->ui.timelapseTableWidget;

		const QList<Modality> flChannels = { Modality::FLCH1, Modality::FLCH2, Modality::FLCH3 };

		for (auto row = 0; row < table->rowCount(); ++row) {
		    const auto cellItem = table->item(row, 0);
			if (cellItem == nullptr) continue;
			if (!flChannels.contains(Modality::_from_integral(cellItem->data(Qt::UserRole).toInt()))) continue;

			if (expanded) {
				d->ui.timelapseTableWidget->showRow(row);
			} else {
				d->ui.timelapseTableWidget->hideRow(row);
			}
		}
	}

	void TimelapseImagingPanel::onUpdateCounts(const TimelapseImagingTime& time) {
        Q_UNUSED(time)

		auto interval = d->ui.intervalInput->GetTime().ToSeconds();
		auto duration = d->ui.durationInput->GetTime().ToSeconds();

	    int32_t count = 0;
	    if (interval == 0) {
			count = 0;
		} else if (interval == duration) {
			count = 2;
		} else if (interval < duration) {
			count = duration / interval + 1;
		} else {
			count = 0;
		}

		d->ui.countsEdit->setText(QString("%1").arg(count, 3, 10, QChar('0')));
    }

	void TimelapseImagingPanel::onSequenceAdded() {
		if (d->ui.intervalInput->GetHour() == 0 &&
			d->ui.intervalInput->GetMinute() == 0 &&
			d->ui.intervalInput->GetSecond() == 0) {
		    TC::MessageDialog::warning(this, tr("Add Sequence"), tr("Input number greater than 0."));
			return;
		}

		TimelapseSequence sequence;

		sequence.SetInterval(TimelapseImagingTime(
			d->ui.intervalInput->GetHour(),
			d->ui.intervalInput->GetMinute(),
			d->ui.intervalInput->GetSecond()
		));
		
        sequence.SetDuration(TimelapseImagingTime(
			d->ui.durationInput->GetHour(),
			d->ui.durationInput->GetMinute(),
			d->ui.durationInput->GetSecond()
		));

		sequence.SetStartTime(TimelapseImagingTime(
			d->ui.startTimeInput->GetHour(),
			d->ui.startTimeInput->GetMinute(),
			d->ui.startTimeInput->GetSecond()
		));

		const auto currentDimension = d->dimensionButtons->checkedId();
		switch (d->ui.modalityComboBox->currentData().toInt()) {
		case Modality::HT:
			sequence.AddImagingType((currentDimension == Dimension::Dimension3) ? AppEntity::ImagingType::HT3D : AppEntity::ImagingType::HT2D);
			break;
		case Modality::FL:
			if (currentDimension == Dimension::Dimension3){
			    sequence.AddImagingType(AppEntity::ImagingType::FL3D);

				if (d->ui.ch1CheckBox->isChecked()) sequence.AddFL3DChannel(0);
				if (d->ui.ch2CheckBox->isChecked()) sequence.AddFL3DChannel(1);
				if (d->ui.ch3CheckBox->isChecked()) sequence.AddFL3DChannel(2);
			} else {
			    sequence.AddImagingType(AppEntity::ImagingType::FL2D);

				if (d->ui.ch1CheckBox->isChecked()) sequence.AddFL2DChannel(0);
				if (d->ui.ch2CheckBox->isChecked()) sequence.AddFL2DChannel(1);
				if (d->ui.ch3CheckBox->isChecked()) sequence.AddFL2DChannel(2);
			}
			break;
		case Modality::BF:
			sequence.AddImagingType(AppEntity::ImagingType::BFGray);
			break;
		}

		AddSequence(sequence);
	}

	void TimelapseImagingPanel::onDeleted() {
		// TODO: optimization and refactoring

        const auto selectedItems = d->ui.timelapseTableWidget->selectedItems();

        if (selectedItems.isEmpty()) {
            d->ClearContents();
        } else {
			// 선택된 cell들의 text와 cell data 삭제
			for (auto& cellItem : selectedItems) {
				if (cellItem->column() == 0) continue;	// skip modality column

				cellItem->setText("");
				cellItem->setData(Qt::UserRole, -1);

				auto modalityCell = d->ui.timelapseTableWidget->item(cellItem->row(), 0);
				if (modalityCell == nullptr) continue;

				// FL 행의 cell이 삭제된 경우, 같은 column의 FL 채널 cell들도 삭제
				// FL 채널행의 cell이 삭제된 경우, 같은 column의 FL cell 삭제
				switch (modalityCell->data(Qt::UserRole).toInt()) {
				case Modality::FL:
					for (auto &channel : {Modality::FLCH1, Modality::FLCH2, Modality::FLCH3}) {
					    auto flItem = d->ui.timelapseTableWidget->item(d->GetModalityRow(channel), cellItem->column());
						if (flItem == nullptr) continue;
						flItem->setText("");
						flItem->setData(Qt::UserRole, -1);
					}
					break;
				case Modality::FLCH1:
				case Modality::FLCH2:
				case Modality::FLCH3:
				    {
					    auto hasFL = false;
					    for (auto &channel : {Modality::FLCH1, Modality::FLCH2, Modality::FLCH3}) {
							auto flChannelItem = d->ui.timelapseTableWidget->item(d->GetModalityRow(channel), cellItem->column());
						    if (flChannelItem == nullptr) continue;

					        if (flChannelItem->data(Qt::UserRole).toInt() >= 0) {
					            hasFL = true;
								break;
					        }
					    }

					    if (!hasFL) {
					        auto flItem = d->ui.timelapseTableWidget->item(d->GetModalityRow(Modality::FL), cellItem->column());
						    if (flItem == nullptr) continue;
						    flItem->setText("");
						    flItem->setData(Qt::UserRole, -1);
					    }
				    }
					break;
				}
			}

            d->sequences = TimeSequencesBuilder(d->GetTimeTable()).Build();
            d->BuildTimeChunks();
            d->UpdateTable();
        }

		emit sigSequencesChanged(GetSequences());
    }

	void TimelapseImagingPanel::onCellClicked(int row, int column) {
		// modality column의 cell을 선택한 경우 해당 row 전체 선택
	    if (column != 0)  return;

		const auto cellItem = d->ui.timelapseTableWidget->item(row, column);
		if (cellItem == nullptr) return;

		const auto clickedModality = Modality::_from_integral(cellItem->data(Qt::UserRole).toInt());
		if (clickedModality == +Modality::FL) {
			// FL cell item 선택시 channel row 모두 선택
			const auto selectionModel = d->ui.timelapseTableWidget->selectionModel();
			TC::SilentCall(d->ui.timelapseTableWidget)->selectRow(row);

			auto itemSelection = selectionModel->selection();

			for (auto rowIndex = 0; rowIndex < d->ui.timelapseTableWidget->rowCount(); ++rowIndex) {
				const auto item = d->ui.timelapseTableWidget->item(rowIndex, 0);
				if (item == nullptr) continue;

				const auto cellModality = Modality::_from_integral(item->data(Qt::UserRole).toInt());
				if (cellModality == +Modality::FLCH1 || 
					cellModality == +Modality::FLCH2 || 
					cellModality == +Modality::FLCH3) {
					TC::SilentCall(d->ui.timelapseTableWidget)->selectRow(rowIndex);
					itemSelection.merge(selectionModel->selection(), QItemSelectionModel::Select);
				}
			}

			selectionModel->clearSelection();
            selectionModel->select(itemSelection, QItemSelectionModel::Select);
			
		} else {
			TC::SilentCall(d->ui.timelapseTableWidget)->selectRow(row);    
		}
    }	
	auto TimelapseImagingPanel::InitEstimatedList() -> void {
		d->endTimeColums.clear();
	}

	auto TimelapseImagingPanel::SetEstimatedEndTimes(const QMap<int32_t, QPair<TimelapseImagingTime, TimelapseImagingTime>>& plans) -> void {
		d->endTimeColums[0] = "End time"; // header title(col 0)
        for(auto it = plans.begin(); it != plans.end(); ++it) {
			const auto column = it.key();
			const auto startTime = it.value().first.ToSeconds();
			const auto requiredTime = it.value().second.ToSeconds();
			const TimelapseImagingTime estimatedEndTime(d->CalculateEndTimeInSec(startTime, requiredTime));
		    d->endTimeColums[column] = estimatedEndTime.ToString();
        }
		d->UpdateEstimatedEndTimeToHeader();

		// show validation error
		SetValidationError(d->CheckValidation());
	}

    auto TimelapseImagingPanel::RefreshEstimatedEndTimes() -> void {
		emit sigSequencesChanged(GetSequences());
    }

    auto TimelapseImagingPanel::GetStartTimes() const -> QMap<int32_t, int32_t> {
		using FrameIndex = int32_t;
		using StartTimeInSec = int32_t;

		QMap<FrameIndex, StartTimeInSec> startTimes;

	    for(auto col = 1; col < d->ui.timelapseTableWidget->columnCount(); ++col) {
			const auto frameIndex = col-1;
		    startTimes[frameIndex] = d->GetStartTimeTextOnHeader(col).ToSeconds();
		}
		return startTimes;
    }

    auto TimelapseImagingPanel::IsFLChannelInUse(int32_t channelIndex) const -> bool {
		if(d->sequences.isEmpty()) return false;

		for(auto it = d->sequences.cbegin(); it != d->sequences.cend(); ++it) {
		    if(it.value().GetFL2DChannels().contains(channelIndex)) return true;
			if(it.value().GetFL3DChannels().contains(channelIndex)) return true;
		}

		return false;
    }

    auto TimelapseImagingPanel::ChangeCurrentSequenceColumn(const int32_t& column) -> void {
		if(d->ui.checkAutoScroll->isChecked()) {
		    d->ScrollToColumn(column);
		}
		d->runningSequenceColumn = column;
	}
}
