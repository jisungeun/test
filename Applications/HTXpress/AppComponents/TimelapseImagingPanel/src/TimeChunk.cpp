#include "TimeChunk.h"

namespace HTXpress::AppComponents::TimelapseImagingPanel {
    struct TimeChunk::Impl {
		int start{ 0 };
		int end{ 0 };
		int interval{ 0 };
    };

    TimeChunk::TimeChunk() : d{ new Impl } {
    }

	TimeChunk::TimeChunk(int start, int end, int interval) : d{ new Impl } {
		d->start = start;
		d->end = end;
		d->interval = interval;
    }

    TimeChunk::TimeChunk(const TimeChunk& other) : d{ new Impl } {
        *d = *other.d;
    }

    TimeChunk::~TimeChunk() {
        
    }

    auto TimeChunk::operator=(const TimeChunk& other) -> TimeChunk& {
        *d = *other.d;
        return *this;
    }

    auto TimeChunk::SetStart(int start) -> void {
        d->start = start;
    }

	auto TimeChunk::GetStart() const -> int {
        return d->start;
    }

    auto TimeChunk::SetEnd(int end) -> void {
        d->end = end;
    }

	auto TimeChunk::GetEnd() const -> int {
        return d->end;
    }

    auto TimeChunk::SetInterval(int interval) -> void {
        d->interval = interval;
    }

    auto TimeChunk::GetInterval() const -> int {
        return d->interval;
    }

    auto TimeChunk::GetDuration() const -> int {
        return d->end - d->start;
    }
}