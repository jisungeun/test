#pragma once

#include <QLabel>

namespace HTXpress::AppComponents::TimelapseImagingPanel {
	class ModalityCellWidget : public QWidget {
		Q_OBJECT
	public:
		explicit ModalityCellWidget(QWidget* parent = nullptr);
		~ModalityCellWidget();

	public:
		auto SetText(const QString& text) -> void;
		auto SetChannelColor(const QColor& color) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
