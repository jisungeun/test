#pragma once
#include <memory>
#include <QMap>

#include <TimelapseSequence.h>
#include <TimeChunk.h>

namespace HTXpress::AppComponents::TimelapseImagingPanel {
    class TimeChunksBuilder {
    public:
        using SeqIndex = TimelapseSequenceIndex;
        using Sequence = TimelapseSequence;

    public:
        explicit TimeChunksBuilder(const QMap<SeqIndex, Sequence>& sequences);
        ~TimeChunksBuilder();

        auto Build() const->QMap<int32_t, TimeChunk>;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}