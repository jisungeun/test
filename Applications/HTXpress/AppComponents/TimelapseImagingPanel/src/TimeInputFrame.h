﻿#pragma once

#include <memory>

#include <QFrame>
#include <QLineEdit>

#include "TimelapseImagingTime.h"

namespace HTXpress::AppComponents::TimelapseImagingPanel {
    class TimeInputFrame : public QFrame {
        Q_OBJECT
    public:
        using Self = TimeInputFrame;

        explicit TimeInputFrame(QWidget *parent = nullptr);
        ~TimeInputFrame() override;

        auto SetTime(const TimelapseImagingTime& time) const->void;

		auto GetTime() const->TimelapseImagingTime;
		auto GetHour() const->int;
		auto GetMinute() const->int;
		auto GetSecond() const->int;

		auto SetReadOnly(bool readOnly) const->void;

	    auto Clear() const->void;

	signals:
		void sigTimeChanged(const TimelapseImagingTime& time);
        void sigNextHourEdit();
        void sigPreviousSecondEdit();

    public slots:
        void onSetHourEditFocus();
        void onSetSecondEditFocus();

	private slots:
		void onChangeValue(int32_t value);
        void onIgnoreSplitterSelection();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    // TimeInputEdit class
    class TimeInputEdit : public QLineEdit {
        Q_OBJECT
    public:
        enum class TimeType {Hour, Minute, Second};
        using Self = TimeInputEdit;

        explicit TimeInputEdit(TimeType type, QWidget* parent = nullptr);
        ~TimeInputEdit() override;

    protected:
        auto focusInEvent(QFocusEvent* event) -> void override;
        auto keyPressEvent(QKeyEvent* event) -> void override;
        auto mouseReleaseEvent(QMouseEvent* event) -> void override;
        bool event(QEvent* event) override;

    signals:
        void sigMoveNext();
        void sigMovePrevious();
        void sigValueChanged(int32_t value);
        
    public slots:
        void onSetFocus();

    private slots:
        void onAddLeadingZero();
        void onTextChanged(const QString& text);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
