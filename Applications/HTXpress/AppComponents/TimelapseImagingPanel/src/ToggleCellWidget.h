#pragma once

#include <QWidget>

namespace HTXpress::AppComponents::TimelapseImagingPanel {
	class ToggleCellWidget : public QWidget {
		Q_OBJECT
	public:
		ToggleCellWidget(const QString& label, QWidget* parent = nullptr);
		~ToggleCellWidget();

		auto Text()->QString;

	protected:
		void resizeEvent(QResizeEvent* event) override;

	signals:
		void toggled(bool checked);

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
