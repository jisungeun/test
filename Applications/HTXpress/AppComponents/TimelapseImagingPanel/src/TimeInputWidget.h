#pragma once

#include <memory>

#include <QWidget>

#include "TimelapseImagingTime.h"

namespace HTXpress::AppComponents::TimelapseImagingPanel {
	class TimeInputWidget : public QWidget {
		Q_OBJECT
	public:
		TimeInputWidget(QWidget* parent = nullptr);
		~TimeInputWidget();

		auto SetTime(const TimelapseImagingTime& time) const->void;

		auto GetTime() const->TimelapseImagingTime;
		auto GetHour() const->int;
		auto GetMinute() const->int;
		auto GetSecond() const->int;

		auto SetReadOnly(bool readOnly) const->void;

	    auto Clear() const->void;

	protected slots:
		void onChangeValue(int value);

	signals:
		void sigTimeChanged(const TimelapseImagingTime& time);

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
