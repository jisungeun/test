#include "TimeChunksBuilder.h"

namespace HTXpress::AppComponents::TimelapseImagingPanel {
    class TimeRow {
    public:
        TimeRow(int32_t start, int32_t interval, int32_t end, int32_t value)
            : start(start), interval(interval), end(end), value(value) {}

        auto Get(int32_t time) const->int32_t {
            if(interval == 0) {
                if(start == time) return value;
                return 0;
            }

            const auto diff = time - start;
            if((diff % interval ) == 0) return value;
            return 0;
        }
    private:
        int32_t start;
        int32_t interval;
        int32_t end;
        int32_t value;
    };

    struct TimeChunksBuilder::Impl {
        const QMap<SeqIndex, Sequence>& sequences;

        explicit Impl(const QMap<SeqIndex, Sequence>& sequences) : sequences(sequences) {}

        auto TypeValue(AppEntity::ImagingType type, int32_t channel = 0)->int32_t;
        auto SequenceValue(const Sequence& sequence)->int32_t;
    };

    auto TimeChunksBuilder::Impl::TypeValue(AppEntity::ImagingType type, int32_t channel) -> int32_t {
        int32_t value = 0;

        switch(type) {
        case AppEntity::ImagingType::HT3D:
            value = 0x01;
            break;
        case AppEntity::ImagingType::HT2D:
            value = 0x02;
            break;
        case AppEntity::ImagingType::FL3D:
            value = 0x11 << channel;
            break;
        case AppEntity::ImagingType::FL2D:
            value = 0x21 << channel;
            break;
        case AppEntity::ImagingType::BFGray:
            value = 0x30;
            break;
        case AppEntity::ImagingType::BFColor:
            value = 0x40;
        }

        return value;
    }

    auto TimeChunksBuilder::Impl::SequenceValue(const Sequence& sequence) -> int32_t {
        int32_t value{ 0 };

        const auto imagingTypes = sequence.GetImagingTypes();
        for(auto type : imagingTypes) {
            switch(type) {
            case AppEntity::ImagingType::HT2D:
            case AppEntity::ImagingType::HT3D:
            case AppEntity::ImagingType::BFGray:
            case AppEntity::ImagingType::BFColor:
                value += TypeValue(type);
                break;
            case AppEntity::ImagingType::FL3D:
                [&]()->void {
                    const auto channels = sequence.GetFL3DChannels();
                    for(auto channel : channels) {
                        value += TypeValue(type, channel);
                    }
                }();
                break;
            case AppEntity::ImagingType::FL2D:
                [&]()->void {
                    const auto channels = sequence.GetFL2DChannels();
                    for(auto channel : channels) {
                        value += TypeValue(type, channel);
                    }
                }();
                break;
            }
        }

        return value;
    }

    TimeChunksBuilder::TimeChunksBuilder(const QMap<SeqIndex, Sequence>& sequences) : d{new Impl(sequences)} {
    }

    TimeChunksBuilder::~TimeChunksBuilder() {
    }

    auto TimeChunksBuilder::Build() const -> QMap<int32_t, TimeChunk> {
        QMap<int, TimeChunk> timeChunks;
        QList<TimeRow> timeRows;
        QList<int32_t> timeStamps;

	    QMapIterator sequencesIt(d->sequences);
        while(sequencesIt.hasNext()) {
            sequencesIt.next();
            const auto& sequence = sequencesIt.value();

            const auto start = sequence.GetStartTime().ToSeconds();
            const auto interval = sequence.GetInterval().ToSeconds();
            const auto duration = sequence.GetDuration().ToSeconds();
            const auto value = d->SequenceValue(sequence);
            if(value == 0) continue;

            timeRows.push_back(TimeRow(start, interval, start+duration, value));

            if(interval == 0) {
                if(!timeStamps.contains(start)) timeStamps.push_back(start);
            } else {
                for(auto time=start; time<=(start+duration); time+=interval) {
                    if(timeStamps.contains(time)) continue;
                    timeStamps.push_back(time);
                }
            }
        }

        std::sort(timeStamps.begin(), timeStamps.end(), std::less<>());

        int32_t preValue = -1;
        int32_t preInterval = 0;

        for(auto timeStamp : timeStamps) {
            int32_t value = 0;
            for(auto timeRow : timeRows) {
                value += timeRow.Get(timeStamp);
            }

            if(preValue == -1) {
                timeChunks[timeStamp] = TimeChunk(timeStamp, 0, 0);
                continue;
            }

            const auto interval = value - preValue;

            if((value == preValue) && (interval == preInterval)) {
                preInterval = interval;
            } else {
                timeChunks.last().SetInterval(preInterval);
                timeChunks.last().SetEnd(preValue);

                timeChunks[timeStamp] = TimeChunk(timeStamp, 0, 0);
                preInterval = 0;
            }

            preValue = timeStamp;
        }

        return timeChunks;
    }
}