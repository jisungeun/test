#include "TimeSequencesBuilder.h"

namespace HTXpress::AppComponents::TimelapseImagingPanel {
    struct TimeSequencesBuilder::Impl {
        const TimeTable& table;
        const int32_t rowCount;
        const int32_t colCount;

        Impl(const TimeTable& table)
            : table(table)
            , rowCount(table.modalities.count())
            , colCount(table.timeStamps.count()) {
        }

        auto GetValues(int32_t columnIndex) const->QList<TimeTable::ValueType>;
        auto CreateSequence(const QList<TimeTable::ValueType>& cellValues) const->TimelapseSequence;
        auto Convert(Modality modality, bool is3D) const->AppEntity::ImagingType;
        auto IsConditionChanged(int32_t columnIndex) const->bool;
    };

    auto TimeSequencesBuilder::Impl::GetValues(int32_t columnIndex) const -> QList<TimeTable::ValueType> {
        QList<TimeTable::ValueType> values;

        for(const auto& list : table.cells) {
            if(list.isEmpty()) continue;
            values.push_back(list.at(columnIndex));
        }

        return values;
    }

    auto TimeSequencesBuilder::Impl::CreateSequence(const QList<TimeTable::ValueType>& cellValues) const -> TimelapseSequence {
        using ValueType = TimeTable::ValueType;

        TimelapseSequence sequence;

        for(auto rowIdx=0; rowIdx<rowCount; rowIdx++) {
            const auto& cellValue = cellValues.at(rowIdx);
            if(cellValue == ValueType::Empty) continue;
            const auto& modality = table.modalities.at(rowIdx);

            const auto is3D = (cellValue == ValueType::Dim3);
            const auto [isFL, flChannel] = [&]()->std::tuple<bool,int32_t> {
                bool fl{ false };
                int32_t channel{ 0 };
                switch(modality) {
                case Modality::FLCH1:
                    fl = true;
                    channel = 0;
                    break;
                case Modality::FLCH2:
                    fl = true;
                    channel = 1;
                    break;
                case Modality::FLCH3:
                    fl = true;
                    channel = 2;
                    break;
                default:
                    fl = false;
                }
                return std::make_tuple(fl, channel);
            }();

            sequence.AddImagingType(Convert(modality, is3D));
            if(isFL) {
                if(is3D) sequence.AddFL3DChannel(flChannel);
                else sequence.AddFL2DChannel(flChannel);
            }
        }

        return sequence;
    }

    auto TimeSequencesBuilder::Impl::Convert(Modality modality, bool is3D) const -> AppEntity::ImagingType {
        using ValueType = TimeTable::ValueType;
        using ImagingType = AppEntity::ImagingType;

        AppEntity::ImagingType type{ ImagingType::HT3D };

        switch(modality) {
        case Modality::HT:
            type = is3D ? ImagingType::HT3D : ImagingType::HT2D;
            break;
        case Modality::BF:
            type = ImagingType::BFGray;
            break;
        case Modality::FLCH1:
        case Modality::FLCH2:
        case Modality::FLCH3:
            type = is3D ? ImagingType::FL3D : ImagingType::FL2D;
            break;
        }

        return type;
    }

    auto TimeSequencesBuilder::Impl::IsConditionChanged(int32_t columnIndex) const -> bool {
        auto intervalChanged = [&]()->bool {
            if(columnIndex < 2) return false;
            auto prevInterval = table.timeStamps.at(columnIndex-1).ToSeconds()
                                - table.timeStamps.at(columnIndex-2).ToSeconds();
            auto curInterval = table.timeStamps.at(columnIndex).ToSeconds()
                               - table.timeStamps.at(columnIndex-1).ToSeconds();
            return prevInterval != curInterval;
        }();

        auto modalityChanged = [&]()->bool {
            if(columnIndex < 1) return false;
            auto prevValues = GetValues(columnIndex - 1);
            auto curValues = GetValues(columnIndex);
            return prevValues != curValues;
        }();

        return intervalChanged || modalityChanged;
    }

    TimeSequencesBuilder::TimeSequencesBuilder(const TimeTable& table) : d{ new Impl(table) } {
    }

    TimeSequencesBuilder::~TimeSequencesBuilder() {
    }

    auto TimeSequencesBuilder::Build() const -> QMap<TimelapseSequenceIndex, TimelapseSequence> {
        QMap<TimelapseSequenceIndex, TimelapseSequence> sequences;
        TimelapseSequenceIndex seqIndex = 0;

        if(d->table.IsEmpty()) return sequences;

        //Create the first sequence
        TimelapseSequence sequence{ d->CreateSequence(d->GetValues(0)) };
        sequence.SetStartTime(d->table.timeStamps.at(0));
        int32_t startIndex = 0;

        auto fillSequence = [&](int32_t endColIdx)->void {
            sequence.SetStartTime(d->table.timeStamps.at(startIndex));
            sequence.SetDuration(d->table.timeStamps.at(endColIdx).ToSeconds() - d->table.timeStamps.at(startIndex).ToSeconds());
            if(startIndex == endColIdx) sequence.SetInterval(0);
            else {
                auto interval = d->table.timeStamps.at(endColIdx).ToSeconds() - d->table.timeStamps.at(endColIdx-1).ToSeconds();
                sequence.SetInterval(interval);
            }

            sequences[seqIndex++] = sequence;
        };

        if (d->colCount == 1) {
            fillSequence(0);
        } else {
            for(auto colIdx=1; colIdx<d->colCount; colIdx++) {
                const auto lastColumn = colIdx == d->colCount - 1;
                const auto isChanged = d->IsConditionChanged(colIdx);
                if(!lastColumn && !isChanged) continue;

                if(lastColumn) {
                    if(isChanged) {
                        fillSequence(colIdx -1);

                        sequence = d->CreateSequence(d->GetValues(colIdx));
                        startIndex = colIdx;
                    }
                    fillSequence(colIdx);
                } else {
                    fillSequence(colIdx - 1);
                    sequence = d->CreateSequence(d->GetValues(colIdx));
                    startIndex = colIdx;
                }
            }
        }

        return sequences;
    }
}