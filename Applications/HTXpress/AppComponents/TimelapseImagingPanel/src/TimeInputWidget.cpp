#include "TimeInputWidget.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QSpinBox>

#include <TCSpinBox.h>

namespace HTXpress::AppComponents::TimelapseImagingPanel {
    struct TimeInputWidget::Impl {
        TC::TCSpinBox* hourInputBox{ nullptr };
        TC::TCSpinBox*minuteInputBox{ nullptr };
        TC::TCSpinBox* secondInputBox{ nullptr };
    };

    TimeInputWidget::TimeInputWidget(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->hourInputBox = new TC::TCSpinBox;
        d->hourInputBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        d->hourInputBox->setKeyboardTracking(false);
        d->hourInputBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        d->hourInputBox->setRange(0, 999);

        d->minuteInputBox = new TC::TCSpinBox;
        d->minuteInputBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        d->minuteInputBox->setKeyboardTracking(false);
        d->minuteInputBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        d->minuteInputBox->setRange(0, 59);

        d->secondInputBox = new TC::TCSpinBox;
        d->secondInputBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        d->secondInputBox->setKeyboardTracking(false);
        d->secondInputBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        d->secondInputBox->setRange(0, 59);

        auto layout = new QHBoxLayout;
        layout->addWidget(d->hourInputBox, 1);
        layout->addWidget(new QLabel(":"));
        layout->addWidget(d->minuteInputBox, 1);
        layout->addWidget(new QLabel(":"));
        layout->addWidget(d->secondInputBox, 1);
        layout->setContentsMargins(6,3,6,3);
        layout->setSpacing(6);
        setLayout(layout);

        connect(d->hourInputBox, QOverload<int>::of(&TC::TCSpinBox::valueChanged), this, &TimeInputWidget::onChangeValue);
        connect(d->minuteInputBox, QOverload<int>::of(&TC::TCSpinBox::valueChanged), this, &TimeInputWidget::onChangeValue);
        connect(d->secondInputBox, QOverload<int>::of(&TC::TCSpinBox::valueChanged), this, &TimeInputWidget::onChangeValue);
    }

	TimeInputWidget::~TimeInputWidget() {
    }

    auto TimeInputWidget::SetTime(const TimelapseImagingTime& time) const -> void {
        d->hourInputBox->setValue(time.GetHour());
        d->minuteInputBox->setValue(time.GetMinute());
        d->secondInputBox->setValue(time.GetSecond());
    }

    auto TimeInputWidget::GetTime() const -> TimelapseImagingTime {
        return TimelapseImagingTime(
            d->hourInputBox->value(),
            d->minuteInputBox->value(),
            d->secondInputBox->value()
        );
    }

    auto TimeInputWidget::GetHour() const -> int {
        return d->hourInputBox->value();
    }

	auto TimeInputWidget::GetMinute() const -> int {
        return d->minuteInputBox->value();
    }

	auto TimeInputWidget::GetSecond() const -> int {
        return d->secondInputBox->value();
    }

    auto TimeInputWidget::SetReadOnly(bool readOnly) const -> void {
        d->hourInputBox->setReadOnly(readOnly);
        d->minuteInputBox->setReadOnly(readOnly);
        d->secondInputBox->setReadOnly(readOnly);
    }

    auto TimeInputWidget::Clear() const -> void {
        d->hourInputBox->setValue(0);
        d->minuteInputBox->setValue(0);
        d->secondInputBox->setValue(0);
    }

    void TimeInputWidget::onChangeValue(int value) {
        Q_UNUSED(value)

        emit sigTimeChanged(GetTime());
    }

}