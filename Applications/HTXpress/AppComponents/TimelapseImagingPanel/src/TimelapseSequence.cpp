#include "TimelapseSequence.h"

#include <QList>

namespace HTXpress::AppComponents::TimelapseImagingPanel {
	struct TimelapseSequence::Impl {
        QList<AppEntity::ImagingType> types; // key: imaging type, value: channels(for FL use only)
        QList<int32_t> channels3D;
        QList<int32_t> channels2D;

		TimelapseImagingTime interval{ TimelapseImagingTime(0, 0, 0) };
		TimelapseImagingTime duration{ TimelapseImagingTime(0, 0, 0) };
		TimelapseImagingTime startTime{ TimelapseImagingTime(0, 0, 0) };

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other)->bool;
        auto operator!=(const Impl& other)->bool;
	};

    auto TimelapseSequence::Impl::operator=(const Impl& other)->Impl& {
        types = other.types;
        channels3D = other.channels3D;
        channels2D = other.channels2D;
        interval = other.interval;
        duration = other.duration;
        startTime = other.startTime;

        return *this;
    }

    auto TimelapseSequence::Impl::operator==(const Impl& other)->bool {
        if (types != other.types) return false;
        if (channels3D != other.channels3D) return false;
        if (channels2D != other.channels2D) return false;
        if (interval != other.interval) return false;
        if (duration != other.duration) return false;
        if (startTime != other.startTime) return false;

        return true;
    }

    auto TimelapseSequence::Impl::operator!=(const Impl& other)->bool {
        if (types == other.types) return false;
        if (channels3D == other.channels3D) return false;
        if (channels2D == other.channels2D) return false;
        if (interval == other.interval) return false;
        if (duration == other.duration) return false;
        if (startTime == other.startTime) return false;

        return true;
    }

	TimelapseSequence::TimelapseSequence() : d{ new Impl } {
        
    }

    TimelapseSequence::TimelapseSequence(const TimelapseSequence& other) : d{ new Impl } {
        *d = *other.d;    
    }

	TimelapseSequence::~TimelapseSequence() {
        
    }

    auto TimelapseSequence::operator=(const TimelapseSequence& other) -> TimelapseSequence& {
        *d = *other.d;
        return *this;
    }

    auto TimelapseSequence::operator==(const TimelapseSequence& other) const -> bool {
        return *d == *other.d;
    }

    auto TimelapseSequence::operator!=(const TimelapseSequence& other) const -> bool {
        return *d != *other.d;
    }

    auto TimelapseSequence::AddImagingType(AppEntity::ImagingType type) -> void {
        if (!d->types.contains(type)) {
            d->types << type;
        }
    }

    auto TimelapseSequence::AddImagingTypes(const QList<AppEntity::ImagingType>& types) -> void {
        for (auto type : types) {
            AddImagingType(type);
        }
    }

    auto TimelapseSequence::GetImagingTypes() const -> QList<AppEntity::ImagingType> {
        return d->types;
    }

    auto TimelapseSequence::RemoveImagingType(AppEntity::ImagingType type) -> void {
        d->types.removeAll(type);
    }

    auto TimelapseSequence::AddFL3DChannel(int32_t channel) -> void {
        if (!d->channels3D.contains(channel)) {
            d->channels3D << channel;
        }
    }

    auto TimelapseSequence::AddFL3DChannels(const QList<int32_t>& channels) -> void {
        for (auto channel : channels) {
            AddFL3DChannel(channel);
        }
    }

    auto TimelapseSequence::GetFL3DChannels() const -> QList<int32_t> {
        return d->channels3D;
    }

    auto TimelapseSequence::Remove3DChannels(int32_t channel) -> void {
        d->channels3D.removeAll(channel);
    }

    auto TimelapseSequence::AddFL2DChannel(int32_t channel) -> void {
        if (!d->channels2D.contains(channel)) {
            d->channels2D << channel;
        }
    }

    auto TimelapseSequence::AddFL2DChannels(const QList<int32_t>& channels) -> void {
        for (auto channel : channels) {
            AddFL2DChannel(channel);
        }
    }

    auto TimelapseSequence::GetFL2DChannels() const -> QList<int32_t> {
        return d->channels2D;
    }

    auto TimelapseSequence::Remove2DChannels(int32_t channel) -> void {
        d->channels2D.removeAll(channel);
    }

	auto TimelapseSequence::SetInterval(const TimelapseImagingTime& time) -> void {
        d->interval = time;
    }

    auto TimelapseSequence::SetInterval(int sec) -> void {
        d->interval = TimelapseImagingTime(sec);
    }

	auto TimelapseSequence::GetInterval() const -> TimelapseImagingTime {
        return d->interval;
    }

    auto TimelapseSequence::SetDuration(const TimelapseImagingTime& time) -> void {
        d->duration = time;
    }

    auto TimelapseSequence::SetDuration(int sec) -> void {
        d->duration = TimelapseImagingTime(sec);
    }

	auto TimelapseSequence::GetDuration() const -> TimelapseImagingTime {
        return d->duration;
    }

    auto TimelapseSequence::SetStartTime(const TimelapseImagingTime& time) -> void {
        d->startTime = time;
    }

    auto TimelapseSequence::SetStartTime(int sec) -> void {
        d->startTime = TimelapseImagingTime(sec);
    }

	auto TimelapseSequence::GetStartTime() const -> TimelapseImagingTime {
        return d->startTime;
    }
}