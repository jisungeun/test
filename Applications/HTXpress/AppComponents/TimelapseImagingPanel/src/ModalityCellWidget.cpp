#include "ModalityCellWidget.h"

#include <QHBoxLayout>
#include <QLabel>

namespace HTXpress::AppComponents::TimelapseImagingPanel {
    struct ModalityCellWidget::Impl {
        QLabel *textLabel{ nullptr };
        QLabel *iconLabel{ nullptr };
    };

    ModalityCellWidget::ModalityCellWidget (QWidget* parent) : QWidget(parent), d{ std::make_unique<Impl>()} {
        d->textLabel = new QLabel(this);
        d->textLabel->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
        d->textLabel->setIndent(10);

        d->iconLabel = new QLabel(this);
        d->iconLabel->setFixedSize(16, 16);
        d->iconLabel->hide();

        auto layout = new QHBoxLayout;
        layout->setContentsMargins(4, 0, 4, 0);
        layout->addWidget(d->textLabel);
        layout->addWidget(d->iconLabel);
        setLayout(layout);
    }

    ModalityCellWidget::~ModalityCellWidget () {
        
    }

    auto ModalityCellWidget::SetText(const QString& text) -> void {
        d->textLabel->setText(text);
    }

    auto ModalityCellWidget::SetChannelColor(const QColor& color) -> void {
        d->iconLabel->setStyleSheet(QString("border: 1px solid #1b2629; background-color: %1;"). arg(color.name()));
        d->iconLabel->setVisible(color != QColor());
    }

}
