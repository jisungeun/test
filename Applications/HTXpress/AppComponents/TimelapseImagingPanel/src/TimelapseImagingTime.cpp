#include "TimelapseImagingTime.h"

#include <QStringList>

namespace HTXpress::AppComponents::TimelapseImagingPanel {
    struct TimelapseImagingTime::Impl {
        int hour{ 0 };
		int minute{ 0 };
		int second{ 0 };

		auto operator=(const Impl& other)->Impl&;
		auto operator==(const Impl& other)->bool;
		auto operator!=(const Impl& other)->bool;
    };

    auto TimelapseImagingTime::Impl::operator=(const Impl& other)->Impl& {
		hour = other.hour;
		minute = other.minute;
		second = other.second;

		return *this;
	}

	auto TimelapseImagingTime::Impl::operator==(const Impl &other)->bool {
		if (hour != other.hour) return false;
		if (minute != other.minute) return false;
		if (second != other.second) return false;

		return true;
	}

	auto TimelapseImagingTime::Impl::operator!=(const Impl& other)->bool {
		if (hour != other.hour || second != other.second || minute != other.minute) {
			return true;
		}

		return false;
	}

    TimelapseImagingTime::TimelapseImagingTime() : TimelapseImagingTime(0, 0, 0) {

    }
    
	TimelapseImagingTime::TimelapseImagingTime(int hour, int minute, int second) : d{ new Impl } {
        d->hour = hour;
        d->minute = minute;
        d->second = second;
    }

	TimelapseImagingTime::TimelapseImagingTime(int seconds) : d{ new Impl } {
		d->hour = seconds / 3600;
		d->minute = (seconds % 3600) / 60;
		d->second = (seconds % 3600) % 60;
	}

	TimelapseImagingTime::TimelapseImagingTime(const TimelapseImagingTime& other) : d{ new Impl } {
		*d = *other.d;
	}

    TimelapseImagingTime::~TimelapseImagingTime() {
        
    }

	auto TimelapseImagingTime::GetHour() const -> int {
		return d->hour;
	}

	auto TimelapseImagingTime::GetMinute() const -> int {
		return d->minute;
	}

	auto TimelapseImagingTime::GetSecond() const -> int {
		return d->second;
	}

    auto TimelapseImagingTime::operator=(const TimelapseImagingTime& other) -> TimelapseImagingTime& {
		*d = *other.d;
		return *this;
	}

	auto TimelapseImagingTime::operator==(const TimelapseImagingTime& other) -> bool {
		return *d == *other.d;
	}

	auto TimelapseImagingTime::operator!=(const TimelapseImagingTime& other) -> bool {
		return *d != *other.d;
	}

	auto TimelapseImagingTime::ToSeconds() const -> int {
        return TimelapseImagingTime::ToSeconds(*this);
    }

	auto TimelapseImagingTime::ToString() const -> QString {
        return TimelapseImagingTime::ToString(*this);
    }

	auto TimelapseImagingTime::ToSeconds(const TimelapseImagingTime& time) -> int {
		int seconds = 0;
		seconds += time.GetHour() * 3600;
		seconds += time.GetMinute() * 60;
		seconds += time.GetSecond();

		return seconds;
	}

	auto TimelapseImagingTime::ToSeconds(int hour, int minute, int second) -> int {
		int seconds = 0;
		seconds += hour * 3600;
		seconds += minute * 60;
		seconds += second;

		return seconds;
	}

	auto TimelapseImagingTime::ToString(const TimelapseImagingTime& time) -> QString {
        return QString("%1:%2:%3")
        .arg(time.GetHour(), 2, 10, QLatin1Char('0'))
		.arg(time.GetMinute(), 2, 10, QLatin1Char('0'))
		.arg(time.GetSecond(), 2, 10, QLatin1Char('0'));
    }

	auto TimelapseImagingTime::ToString(int hour, int minute, int second) -> QString {
        return QString("%1:%2:%3")
        .arg(hour, 2, 10, QLatin1Char('0'))
		.arg(minute, 2, 10, QLatin1Char('0'))
		.arg(second, 2, 10, QLatin1Char('0'));
    }

	auto TimelapseImagingTime::FromString(const QString& text) -> TimelapseImagingTime {
        auto splitted = text.split(":");
		if (splitted.size() != 3) {
		    return TimelapseImagingTime();
		}

		return TimelapseImagingTime(splitted[0].toInt(), splitted[1].toInt(), splitted[2].toInt());
    }
}