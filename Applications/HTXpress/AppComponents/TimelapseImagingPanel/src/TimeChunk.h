#pragma once

#include <memory>

namespace HTXpress::AppComponents::TimelapseImagingPanel {
	class TimeChunk {
	public:
		TimeChunk();
		TimeChunk(int start, int end, int interval);
		TimeChunk(const TimeChunk& other);
		~TimeChunk();

		auto operator=(const TimeChunk& other)->TimeChunk&;

		auto SetStart(int start)->void;
		auto GetStart() const->int;

		auto SetEnd(int end)->void;
		auto GetEnd() const->int;

		auto SetInterval(int interval)->void;
		auto GetInterval() const->int;

		auto GetDuration() const->int;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
