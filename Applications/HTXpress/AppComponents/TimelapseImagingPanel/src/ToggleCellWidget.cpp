#include "ToggleCellWidget.h"

#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QResizeEvent>

namespace HTXpress::AppComponents::TimelapseImagingPanel {
    struct ToggleCellWidget::Impl {
        QLabel* label{ nullptr };
        QPushButton* toggleButton{ nullptr };
    };

    ToggleCellWidget::ToggleCellWidget(const QString& label, QWidget* parent) : QWidget(parent), d{ new Impl } {
        setObjectName("widget-tsx-toggleCellWidget");

        d->label = new QLabel(label, this);
        d->label->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
        d->label->setIndent(10);

        d->toggleButton = new QPushButton(this);
        d->toggleButton->setObjectName("bt-tsx-cell-collapsibleButton");
        d->toggleButton->setFixedSize(16, 16);
        d->toggleButton->setCheckable(true);
        d->toggleButton->setChecked(true);

        connect(d->toggleButton, &QPushButton::toggled, [=](bool checked) { emit toggled(checked); });

        auto layout = new QHBoxLayout;
        layout->setContentsMargins(4, 0, 4, 0);
        layout->addWidget(d->label);

        setLayout(layout);
    }

    ToggleCellWidget::~ToggleCellWidget() {
        
    }

    auto ToggleCellWidget::Text() -> QString {
        return d->label->text();
    }

    void ToggleCellWidget::resizeEvent(QResizeEvent* event) {
        QWidget::resizeEvent(event);

        auto size = event->size();
        auto x = size.width() - d->toggleButton->width() - 2/* right padding */;
        auto y = size.height() / 2 - d->toggleButton->height() / 2;
        d->toggleButton->move(x, y);
    }

}
