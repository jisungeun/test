#pragma once
#include <memory>
#include <QMap>

#include <TimelapseSequence.h>

namespace HTXpress::AppComponents::TimelapseImagingPanel {
    struct TimeTable {
        enum ValueType {
            Empty,
            Dim2,
            Dim3
        };

        auto IsEmpty() const->bool {
            if(cells.isEmpty()) return true;
            for(auto& list : cells) {
                if(!list.isEmpty()) return false;
            }
            return true;
        }

        QList<TimelapseImagingTime> timeStamps;
        QList<Modality> modalities;
        QList<QList<ValueType>> cells;
    };

    class TimeSequencesBuilder {
    public:
        TimeSequencesBuilder(const TimeTable& table);
        ~TimeSequencesBuilder();

        auto Build() const->QMap<TimelapseSequenceIndex, TimelapseSequence>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}