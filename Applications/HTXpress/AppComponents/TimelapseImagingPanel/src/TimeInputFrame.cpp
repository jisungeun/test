﻿#include <QHBoxLayout>
#include <QRegExpValidator>
#include <QKeyEvent>
#include <QDebug>

#include "TimeInputFrame.h"

namespace HTXpress::AppComponents::TimelapseImagingPanel {
    struct TimeInputFrame::Impl {
        TimeInputEdit* hourInputBox{nullptr};
        TimeInputEdit* minuteInputBox{nullptr};
        TimeInputEdit* secondInputBox{nullptr};

        static auto SetCustomStyle(QLineEdit* line) -> void;
        auto GetTimeSpliter(Self* self) -> QLineEdit*;
    };

    TimeInputFrame::TimeInputFrame(QWidget* parent) : QFrame(parent), d{std::make_unique<Impl>()} {
        setContentsMargins(0, 0, 0, 0);
        setStyleSheet("QFrame{border: 1px solid #2D3E43; border-radius: 6px; background-color:#172023;}\n QFrame:hover{border: 1px solid #00c2d3}");
        d->hourInputBox = new TimeInputEdit(TimeInputEdit::TimeType::Hour, this);
        d->minuteInputBox = new TimeInputEdit(TimeInputEdit::TimeType::Minute, this);
        d->secondInputBox = new TimeInputEdit(TimeInputEdit::TimeType::Second, this);

        auto mainLayout = new QGridLayout;
        mainLayout->setSpacing(0);
        mainLayout->setContentsMargins(0, 0, 0, 0);

        d->SetCustomStyle(d->hourInputBox);
        d->SetCustomStyle(d->minuteInputBox);
        d->SetCustomStyle(d->secondInputBox);

        mainLayout->addWidget(d->hourInputBox, 0, 0);
        mainLayout->addWidget(d->GetTimeSpliter(this), 0, 1);
        mainLayout->addWidget(d->minuteInputBox, 0, 2);
        mainLayout->addWidget(d->GetTimeSpliter(this), 0, 3);
        mainLayout->addWidget(d->secondInputBox, 0, 4);

        mainLayout->setColumnStretch(0, 3);
        mainLayout->setColumnStretch(1, 0);
        mainLayout->setColumnStretch(2, 3);
        mainLayout->setColumnStretch(3, 0);
        mainLayout->setColumnStretch(4, 3);

        setLayout(mainLayout);

        connect(d->hourInputBox, &TimeInputEdit::sigMoveNext, d->minuteInputBox, &TimeInputEdit::onSetFocus);
        connect(d->hourInputBox, &TimeInputEdit::sigMovePrevious, this, &Self::sigPreviousSecondEdit);

        connect(d->minuteInputBox, &TimeInputEdit::sigMoveNext, d->secondInputBox, &TimeInputEdit::onSetFocus);
        connect(d->minuteInputBox, &TimeInputEdit::sigMovePrevious, d->hourInputBox, &TimeInputEdit::onSetFocus);

        connect(d->secondInputBox, &TimeInputEdit::sigMovePrevious, d->minuteInputBox, &TimeInputEdit::onSetFocus);
        connect(d->secondInputBox, &TimeInputEdit::sigMoveNext, this, &Self::sigNextHourEdit);

        connect(d->hourInputBox, &TimeInputEdit::sigValueChanged, this, &Self::onChangeValue);
        connect(d->minuteInputBox, &TimeInputEdit::sigValueChanged, this, &Self::onChangeValue);
        connect(d->secondInputBox, &TimeInputEdit::sigValueChanged, this, &Self::onChangeValue);
    }

    TimeInputFrame::~TimeInputFrame() {
    }

    auto TimeInputFrame::SetTime(const TimelapseImagingTime& time) const -> void {
        d->hourInputBox->setText(QString::number(time.GetHour()));
        d->minuteInputBox->setText(QString::number(time.GetMinute()));
        d->secondInputBox->setText(QString::number(time.GetSecond()));
    }

    auto TimeInputFrame::GetTime() const -> TimelapseImagingTime {
        TimelapseImagingTime time(d->hourInputBox->text().toInt(), d->minuteInputBox->text().toInt(), d->secondInputBox->text().toInt());
        return time;
    }

    auto TimeInputFrame::GetHour() const -> int {
        return d->hourInputBox->text().toInt();
    }

    auto TimeInputFrame::GetMinute() const -> int {
        return d->minuteInputBox->text().toInt();
    }

    auto TimeInputFrame::GetSecond() const -> int {
        return d->secondInputBox->text().toInt();
    }

    auto TimeInputFrame::SetReadOnly(bool readOnly) const -> void {
        d->hourInputBox->setReadOnly(readOnly);
        d->minuteInputBox->setReadOnly(readOnly);
        d->secondInputBox->setReadOnly(readOnly);
    }

    auto TimeInputFrame::Clear() const -> void {
        d->hourInputBox->clear();
        d->minuteInputBox->clear();
        d->secondInputBox->clear();
    }

    void TimeInputFrame::onSetHourEditFocus() {
        d->hourInputBox->onSetFocus();
    }

    void TimeInputFrame::onSetSecondEditFocus() {
        d->secondInputBox->onSetFocus();
    }

    void TimeInputFrame::onChangeValue(int32_t value) {
        Q_UNUSED(value)
        emit sigTimeChanged(GetTime());
    }

    void TimeInputFrame::onIgnoreSplitterSelection() {
        const auto& splitter = qobject_cast<QLineEdit*>(sender());
        splitter->setSelection(0, 0);
    }

    auto TimeInputFrame::Impl::SetCustomStyle(QLineEdit* line) -> void {
        line->setContentsMargins(0, 0, 0, 0);
        line->setStyleSheet("QLineEdit { padding: 0px 0px; border: 0px none; background-color:transparent;}");
        line->setFrame(false);
        line->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    }

    auto TimeInputFrame::Impl::GetTimeSpliter(Self* self) -> QLineEdit* {
        const auto splitter = new QLineEdit(":", self);
        SetCustomStyle(splitter);
        splitter->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        splitter->setAlignment(Qt::AlignCenter);
        splitter->setMaximumWidth(8);
        splitter->setReadOnly(true);
        splitter->setFocusPolicy(Qt::NoFocus);
        connect(splitter, &QLineEdit::selectionChanged, self, &Self::onIgnoreSplitterSelection);
        return splitter;
    }

    // TimeInputEdit class
    struct TimeInputEdit::Impl {
        bool selectOnMouseRelease{false};
        TimeType type;
        int32_t moveNextThreshold{ 10 };
    };

    TimeInputEdit::TimeInputEdit(TimeType type, QWidget* parent) : QLineEdit(parent), d{std::make_unique<Impl>()} {
        d->type = type;
        setPlaceholderText("00");

        // init reg expression
        QString regExp;
        switch (type) {
            case TimeType::Hour: regExp = "[0-9]|[0-9][0-9][0-9]";
                d->moveNextThreshold = 100;
                setPlaceholderText("000");
                setAlignment(Qt::AlignCenter);
                break;
            case TimeType::Minute: regExp = "[0-9]|[0-5][0-9]";
                setAlignment(Qt::AlignCenter);
                break;
            case TimeType::Second: regExp = "[0-9]|[0-5][0-9]";
                setAlignment(Qt::AlignCenter);
                break;
        }
        auto validRegExp = new QRegExpValidator(QRegExp(regExp));
        setValidator(validRegExp);

        connect(this, &Self::editingFinished, this, &Self::onAddLeadingZero);
        connect(this, &Self::textChanged, this, &Self::onTextChanged);
    }

    TimeInputEdit::~TimeInputEdit() {
    }

    auto TimeInputEdit::focusInEvent(QFocusEvent* event) -> void {
        QLineEdit::focusInEvent(event);
        d->selectOnMouseRelease = true;
    }

    auto TimeInputEdit::keyPressEvent(QKeyEvent* event) -> void {
        auto key = event->key();
        auto cursorPos = cursorPosition();
        
        if (cursorPos == 0) {
            if (key == Qt::Key_Left) {
                emit sigMovePrevious();
                event->accept();
                return;
            }
        }

        if (cursorPos == text().count()) {
            if (key == Qt::Key_Right) {
                emit sigMoveNext();
                event->accept();
                return;
            }
        }

        QLineEdit::keyPressEvent(event);

        const auto freshCurPos = cursorPosition();

        if (d->type == TimeType::Minute || d->type == TimeType::Second) {
            if (freshCurPos == 1 && text().toInt() > 5 && text().count() == 1) {
                emit sigMoveNext();
            }
        }
    }

    auto TimeInputEdit::mouseReleaseEvent(QMouseEvent* event) -> void {
        if (!d->selectOnMouseRelease) return;

        d->selectOnMouseRelease = false;
        selectAll();
        QLineEdit::mouseReleaseEvent(event);
    }

    bool TimeInputEdit::event(QEvent* event) {
        if (event->type() == QEvent::KeyPress) {
            auto keyEvent = dynamic_cast<QKeyEvent*>(event);
            if (keyEvent->key() == Qt::Key_Tab) {
                emit sigMoveNext();
                event->accept();
                return true;
            }
        }
        return QLineEdit::event(event);
    }

    void TimeInputEdit::onSetFocus() {
        setFocus();
        d->selectOnMouseRelease = false;
        selectAll();
    }

    void TimeInputEdit::onAddLeadingZero() {
        if(text().toInt() == 0) clear();

        const auto digit = QString::number(d->moveNextThreshold).size();
        setText(QString("%1").arg(text().toInt(), digit, 10, QChar('0')));
    }

    void TimeInputEdit::onTextChanged(const QString& text) {
        const auto num = text.toInt();
        emit sigValueChanged(text.toInt());
        if (num >= d->moveNextThreshold) {
            emit sigMoveNext();
        }
    }
}
