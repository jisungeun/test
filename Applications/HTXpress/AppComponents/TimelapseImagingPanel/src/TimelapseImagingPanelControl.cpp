#include "TimelapseImagingPanelControl.h"

namespace HTXpress::AppComponents::TimelapseImagingPanel {
    struct TimelapseImagingPanelControl::Impl {
        
    };

    TimelapseImagingPanelControl::TimelapseImagingPanelControl() : d{ new Impl } {
        
    }

    TimelapseImagingPanelControl::~TimelapseImagingPanelControl() {
        
    }
}