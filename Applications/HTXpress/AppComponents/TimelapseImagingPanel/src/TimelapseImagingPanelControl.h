#pragma once

#include <memory>

namespace HTXpress::AppComponents::TimelapseImagingPanel {
	class TimelapseImagingPanelControl {
	public:
		TimelapseImagingPanelControl();
		~TimelapseImagingPanelControl();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
