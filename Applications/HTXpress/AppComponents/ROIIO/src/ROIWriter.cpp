#define LOGGER_TAG "[ROIWriter]"

#include <algorithm>

#include <QDir>
#include <QFile>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QRegularExpression>

#include <TCLogger.h>
#include <SystemConfig.h>

#include "ROIWriter.h"
#include "ROIIODefines.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::ROIIO {
    struct ROIWriter::Impl {
        QString fileFullPath{};
        QString fileName{};

        QString vesselName{};
        QList<RegionOfInterest::Pointer> roiList{};

        auto IsNameValid() const -> bool;
        auto RoiToJsonObject(const RegionOfInterest::Pointer& roi) -> QJsonObject;
        auto RoiListToJsonArray() -> QJsonArray;
    };

    auto ROIWriter::Impl::IsNameValid() const -> bool {
        const QRegularExpression re{"^[^\\\\/:*?\"<>|]+$"};
        return re.match(fileName).hasMatch();
    }

    auto ROIWriter::Impl::RoiToJsonObject(const RegionOfInterest::Pointer& roi) -> QJsonObject {
        QJsonObject jsonObj;
        jsonObj[Key::roiIndex] = roi->GetIndex();
        jsonObj[Key::roiName] = roi->GetName();
        jsonObj[Key::roiPosition] = QJsonObject{{Key::roiPositionX, roi->GetX()}, {Key::roiPositionY, roi->GetY()}};
        jsonObj[Key::roiSize] = QJsonObject{{Key::roiSizeWidth, roi->GetWidth()}, {Key::roiSizeHeight, roi->GetHeight()}};
        jsonObj[Key::roiShape] = roi->GetShape()._to_string();

        return jsonObj;
    }

    auto ROIWriter::Impl::RoiListToJsonArray() -> QJsonArray {
        QJsonArray jsonArray;

        std::sort(roiList.begin(), roiList.end(), [](const auto& a, const auto& b) {
            return a->GetIndex() < b->GetIndex();
        });

        for (const auto& roi : roiList) {
            jsonArray.append(RoiToJsonObject(roi));
        }

        return jsonArray;
    }

    ROIWriter::ROIWriter(const QString& roiFileName) : d{std::make_unique<Impl>()} {
        d->fileName = roiFileName;
        d->fileFullPath = QString("%1/%2.%3").arg(GetPresetFolderPath()).arg(roiFileName).arg(GetFileExtension());
    }

    ROIWriter::~ROIWriter() = default;

    auto ROIWriter::SetVesselName(const QString& vesselName) -> void {
        d->vesselName = vesselName;
    }

    auto ROIWriter::SetRoiList(const QList<RegionOfInterest::Pointer>& roiList) -> void {
        d->roiList = roiList;
    }

    auto ROIWriter::Write() -> WriteResult {
        const QDir dir(GetPresetFolderPath());
        if (!dir.exists()) {
            const auto mkpath = dir.mkpath(GetPresetFolderPath());
            if (!mkpath) {
                QLOG_ERROR() << "Failed to create directory: " << d->fileFullPath;
                return WriteResult::Fail_MakeDirectory;
            }
        }

        if (!d->IsNameValid()) {
            QLOG_ERROR() << "Invalid file name: " << d->fileName;
            return WriteResult::Fail_InvalidName;
        }

        QFile file(d->fileFullPath);
        if (!file.open(QIODevice::WriteOnly)) {
            return WriteResult::Fail_Open;
        }

        QJsonObject jsonObj;
        jsonObj[Key::vesselName] = d->vesselName;
        jsonObj[Key::roiPresetTitle] = d->RoiListToJsonArray();

        const QJsonDocument jsonDoc(jsonObj);
        const auto bytesWritten = file.write(jsonDoc.toJson());
        file.close();
        if (bytesWritten > 0) {
            return WriteResult::Success;
        }

        return WriteResult::Fail_Open;
    }

    auto ROIWriter::GetPresetFolderPath() -> QString {
        return presetFolderPath;
    }

    auto ROIWriter::GetFileExtension() -> QString {
        return extension;
    }
}
