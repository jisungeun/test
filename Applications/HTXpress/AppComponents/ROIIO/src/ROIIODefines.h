#pragma once

#include <QStandardPaths>
#include <QString>

namespace HTXpress::AppComponents::ROIIO {
    const QString extension = "roi";
    const QString presetFolderPath = 
        QString("%1/config/preset/roi").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));

    namespace Key {
        constexpr char vesselName[] = "vesselName";
        constexpr char roiPresetTitle[] = "roiList";
        constexpr char roiIndex[] = "index";
        constexpr char roiName[] = "name";
        constexpr char roiPosition[] = "position";
        constexpr char roiPositionX[] = "y";
        constexpr char roiPositionY[] = "x";
        constexpr char roiShape[] = "shape";
        constexpr char roiSize[] = "size";
        constexpr char roiSizeWidth[] = "w";
        constexpr char roiSizeHeight[] = "h";
    }
}
