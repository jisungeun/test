#define LOGGER_TAG "[ROIReader]"

#include <QFile>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QList>

#include "ROIReader.h"
#include "ROIIODefines.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::ROIIO {
    struct ROIReader::Impl {
        QString fileFullPath{};

        auto JsonArrayToRoiList(const QJsonArray& jsonArr) const -> QList<RegionOfInterest::Pointer>;
    };

    auto ROIReader::Impl::JsonArrayToRoiList(const QJsonArray& jsonArr) const -> QList<RegionOfInterest::Pointer> {
        QList<RegionOfInterest::Pointer> roiList;

        for (const auto& jsonValue : jsonArr) {
            const auto jsonObj = jsonValue.toObject();
            const auto positionObj = jsonObj[Key::roiPosition].toObject();
            const auto sizeObj = jsonObj[Key::roiSize].toObject();

            auto roi = std::make_shared<RegionOfInterest>();
            roi->SetIndex(jsonObj[Key::roiIndex].toInt());
            roi->SetName(jsonObj[Key::roiName].toString());
            roi->SetPosition(positionObj[Key::roiPositionX].toDouble(), positionObj[Key::roiPositionY].toDouble());
            roi->SetSize(sizeObj[Key::roiSizeWidth].toDouble(), sizeObj[Key::roiSizeHeight].toDouble());
            roi->SetShape(ItemShape::_from_string(jsonObj[Key::roiShape].toString().toStdString().c_str()));

            roiList.append(roi);
        }

        return roiList;
    }

    ROIReader::ROIReader(const QString& fileFullPath) : d{std::make_unique<Impl>()} {
        d->fileFullPath = fileFullPath;
    }

    ROIReader::~ROIReader() = default;

    auto ROIReader::Read() -> std::optional<QList<RegionOfInterest::Pointer>> {
        QFile file(d->fileFullPath);
        if (!file.open(QIODevice::ReadOnly)) {
            return std::nullopt;
        }

        const auto jsonDoc(QJsonDocument::fromJson(file.readAll()));
        const auto jsonObj(jsonDoc.object());
        const auto roiListJsonArr = jsonObj[Key::roiPresetTitle].toArray();
        const auto roiList = d->JsonArrayToRoiList(roiListJsonArr);

        file.close();

        return roiList;
    }
}
