#pragma once
#include <memory>
#include <QString>

#include <RegionOfInterest.h>

#include "HTXROIIOExport.h"

namespace HTXpress::AppComponents::ROIIO {
    class HTXROIIO_API ROIWriter final {
    public:
        enum class WriteResult {
            Success,
            Fail_InvalidName,
            Fail_Open,
            Fail_Write,
            Fail_MakeDirectory,
            Fail_FileExists,
            Fail_Unknown
        };

        explicit ROIWriter(const QString& roiFileName);
        ~ROIWriter();

        auto SetVesselName(const QString& vesselName) -> void;
        auto SetRoiList(const QList<RoiSetupDefinitions::RegionOfInterest::Pointer>& roiList) -> void;

        auto Write() -> WriteResult;

        static auto GetPresetFolderPath() -> QString;
        static auto GetFileExtension() -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
