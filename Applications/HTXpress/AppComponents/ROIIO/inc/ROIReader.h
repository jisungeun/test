#pragma once
#include <memory>
#include <optional>

#include <RoiSetupDefines.h>
#include <RegionOfInterest.h>

#include "HTXROIIOExport.h"

namespace HTXpress::AppComponents::ROIIO {
    class HTXROIIO_API ROIReader final {
    public:
        explicit ROIReader(const QString& fileFullPath);
        ~ROIReader();

        auto Read() -> std::optional<QList<RoiSetupDefinitions::RegionOfInterest::Pointer>>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
