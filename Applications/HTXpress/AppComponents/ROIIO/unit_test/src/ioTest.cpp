﻿#include <catch2/catch.hpp>

#include <QString>
#include <QDir>

#include <RegionOfInterest.h>
#include <ROIWriter.h>
#include <ROIReader.h>

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::ROIIO::Test {
    TEST_CASE("ROIWriter and ROIReader") {
        const QString vesselName = "Test Vessel";
        const QString roiFileName = "test_roi_file";
        const QString roiFileFullPath = QString("%1/%2.%3").arg(ROIWriter::GetPresetFolderPath()).arg(roiFileName).arg(ROIWriter::GetFileExtension());

        QList<RegionOfInterest::Pointer> roiList;

        auto roi1 = std::make_shared<RegionOfInterest>();
        roi1->SetIndex(1);
        roi1->SetName("ROI 1");
        roi1->SetPosition(10, 20);
        roi1->SetSize(100, 200);
        roi1->SetShape(ItemShape::Rectangle);
        roiList.append(roi1);

        auto roi2 = std::make_shared<RegionOfInterest>();
        roi2->SetIndex(2);
        roi2->SetName("ROI 2");
        roi2->SetPosition(30, 40);
        roi2->SetSize(300, 400);
        roi2->SetShape(ItemShape::Ellipse);
        roiList.append(roi2);

        SECTION("Write and read ROI list") {
            ROIWriter writer(roiFileName);
            writer.SetVesselName(vesselName);
            writer.SetRoiList(roiList);

            auto writeResult = writer.Write();
            REQUIRE(writeResult == ROIWriter::WriteResult::Success);

            ROIReader reader(roiFileFullPath);
            auto optionalReadList = reader.Read();
            REQUIRE(optionalReadList.has_value());

            auto readRoiList = optionalReadList.value();
            REQUIRE(readRoiList.size() == roiList.size());

            for (int i = 0; i < roiList.size(); ++i) {
                REQUIRE(readRoiList[i]->GetIndex() == roiList[i]->GetIndex());
                REQUIRE(readRoiList[i]->GetName() == roiList[i]->GetName());
                REQUIRE(readRoiList[i]->GetPosition() == roiList[i]->GetPosition());
                REQUIRE(readRoiList[i]->GetSize() == roiList[i]->GetSize());
                REQUIRE(readRoiList[i]->GetShape() == roiList[i]->GetShape());
            }
        }

        // Remove test file
        QFile file(roiFileFullPath);
        REQUIRE(file.remove());
    }
}
