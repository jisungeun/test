#pragma once
#include <memory>

#include <QWidget>

#include "Sequence.h"
#include "HTXSequenceTableExport.h"

namespace HTXpress::AppComponents::SequenceTable {
    class HTXSequenceTable_API SequenceTable : public QWidget {
        Q_OBJECT

    public:
        explicit SequenceTable(QWidget* parent = nullptr);
        ~SequenceTable();

        auto GetSequences() const->QList<Sequence>;

    signals:
        void sigApply();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}