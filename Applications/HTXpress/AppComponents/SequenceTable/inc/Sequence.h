#pragma once
#include <memory>
#include <QMetaType>
#include <QList>

#include <AppEntityDefines.h>
#include "HTXSequenceTableExport.h"

namespace HTXpress::AppComponents::SequenceTable {
    class HTXSequenceTable_API Sequence {
    public:
        struct Item {
            AppEntity::Modality modality{ AppEntity::Modality::HT };
            bool is3D{ true };
            bool isGray{ true };
            QList<int32_t> channels;
        };

    public:
        Sequence();
        Sequence(const Sequence& other);
        ~Sequence();

        auto operator=(const Sequence& other)->Sequence&;

        auto AddItem(const Item& item)->void;
        auto GetItems() const->QList<Item>;

        auto SetStartTimestamp(const uint32_t seconds)->void;
        auto GetStartTimestamp() const->uint32_t;

        auto SetInterval(const uint32_t seconds)->void;
        auto GetInterval() const->uint32_t;

        auto SetCount(uint32_t count)->void;
        auto GetCount() const->uint32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
