#include "SequenceTable.h"
#include "SequenceTableControl.h"
#include "ui_SequenceTable.h"

namespace HTXpress::AppComponents::SequenceTable {
    struct SequenceTable::Impl {
        Ui::SequenceTable ui;
        SequenceTableControl control;
    };

    SequenceTable::SequenceTable(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);
    }

    SequenceTable::~SequenceTable() {
    }

    auto SequenceTable::GetSequences() const -> QList<Sequence> {
        QList<Sequence> sequences;

        //Todo 사용자 입력 사항으로부터 sequence 목록을 생성하는 코드 구현

        return sequences;
    }
}
