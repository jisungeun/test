#pragma once
#include <memory>

namespace HTXpress::AppComponents::SequenceTable {
    class SequenceTableControl {
    public:
        SequenceTableControl();
        ~SequenceTableControl();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}