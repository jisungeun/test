#include "Sequence.h"

namespace HTXpress::AppComponents::SequenceTable {
    struct Sequence::Impl {
        QList<Item> items;
        uint32_t startTimestamp{ 0 };
        uint32_t interval{ 0 };
        uint32_t count{ 1 };

        auto operator=(const Impl& other)->Impl&;
    };

    auto Sequence::Impl::operator=(const Impl& other) -> Impl& {
        items = other.items;
        startTimestamp = other.startTimestamp;
        interval = other.interval;
        count = other.count;
        return *this;
    }

    Sequence::Sequence() : d{new Impl} {
    }

    Sequence::Sequence(const Sequence& other) : d{new Impl} {
        *d = *other.d;
    }

    Sequence::~Sequence() {
    }

    auto Sequence::operator=(const Sequence& other) -> Sequence& {
        *d = *other.d;
        return *this;
    }

    auto Sequence::AddItem(const Item& item) -> void {
        d->items.push_back(item);
    }

    auto Sequence::GetItems() const -> QList<Item> {
        return d->items;
    }

    auto Sequence::SetStartTimestamp(const uint32_t seconds) -> void {
        d->startTimestamp = seconds;
    }

    auto Sequence::GetStartTimestamp() const -> uint32_t {
        return d->startTimestamp;
    }

    auto Sequence::SetInterval(const uint32_t seconds) -> void {
        d->interval = seconds;
    }

    auto Sequence::GetInterval() const -> uint32_t {
        return d->interval;
    }

    auto Sequence::SetCount(uint32_t count) -> void {
        d->count = count;
    }

    auto Sequence::GetCount() const -> uint32_t {
        return d->count;
    }
}
