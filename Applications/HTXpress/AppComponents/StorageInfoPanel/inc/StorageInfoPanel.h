﻿#pragma once

#include <memory>

#include <QWidget>

#include "HTXStorageInfoPanelExport.h"

namespace HTXpress::AppComponents::StorageInfoPanel {
    class HTXStorageInfoPanel_API StorageInfoPanel final : public QWidget {
        Q_OBJECT
    public:
        using Self = StorageInfoPanel;
        using Pointer = std::shared_ptr<Self>;
        using Byte = int64_t;

        explicit StorageInfoPanel(QWidget* parent = nullptr);
        ~StorageInfoPanel() override;

        auto SetStorageInfo(const int64_t& bytesTotal, const int64_t& bytesAvailable) -> void;
        auto SetMinRequiredSpace(const int32_t& gb) -> void;

    signals:
        void sigRefresh();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
