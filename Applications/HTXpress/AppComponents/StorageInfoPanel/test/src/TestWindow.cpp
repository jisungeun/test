#include <QtGui/QtGui>
#include <QDebug>
#include <QFileDialog>
#include <QStorageInfo>
#include <QPushButton>
#include <QTime>

#include <StorageInfoPanel.h>
#include <SystemStorageManager.h>
#include <StorageInfoObserver.h>

#include "TestWindow.h"
#include "ui_TestWindow.h"

namespace HTXpress::AppComponents::StorageInfoPanel::TEST {
    using StorageManager = AppPlugins::Experiment::Perform::Plugins::SystemStorageManager::SystemStorageManager;
    using StorageObserver = AppPlugins::Experiment::Perform::Plugins::SystemStorageManager::StorageInfoObserver;
    struct TestWindow::Impl {
        explicit Impl(TestWindow* self) : self(self) {
            ui.setupUi(self);
        }
        Ui::TestWindow ui{};
        TestWindow* self{};

        StorageInfoPanel* storageInfoPanel{};
        StorageManager::Pointer storageManager{std::make_shared<StorageManager>()};
        StorageObserver::Pointer observer{std::make_shared<StorageObserver>()};
    };

    TestWindow::TestWindow(QWidget* parent) : QMainWindow(parent), d{std::make_unique<Impl>(this)} {
        d->storageInfoPanel = new StorageInfoPanel();

        QGridLayout layout;
        layout.addWidget(d->storageInfoPanel);
        layout.setContentsMargins(0,0,0,0);
        layout.setMargin(0);
        layout.setSpacing(0);

        d->ui.frame->setLayout(&layout);
        d->ui.intervalSpinBox->setMinimum(1000); // 1sec
        d->ui.intervalSpinBox->setMaximum(600*1000); // 10min

        connect(d->ui.selectFolderButton, &QPushButton::clicked, this, &Self::onSelectFolder);
        connect(d->observer.get(), &StorageObserver::sigUpdateStorageInfo, this, &Self::OnUpdateStorageInfo);

        connect(d->storageInfoPanel, &StorageInfoPanel::sigRefresh, this, &Self::onRefresh);
        connect(d->ui.applyIntervalButton, &QPushButton::clicked, this, &Self::onChangeInterval);
        connect(d->ui.applyMinSpaceButton, &QPushButton::clicked, this, &Self::onChangeMinRequiredSpace);
    }

    TestWindow::~TestWindow() {
    }

    void TestWindow::onSelectFolder() {
        const auto dir = QFileDialog::getExistingDirectory(this, "", "");
        d->storageManager->SetPath(dir);
        d->ui.folderPath->setText(dir);
    }

    void TestWindow::OnUpdateStorageInfo(const int64_t& total, const int64_t& free) {
        qDebug() << "Update: " << QTime::currentTime().toString();
        d->storageInfoPanel->SetStorageInfo(total, free);        
    }

    void TestWindow::onRefresh() {
        d->storageManager->UpdateStorageInfo();
    }

    void TestWindow::onChangeInterval() {
        const auto ms = d->ui.intervalSpinBox->value();
        d->storageManager->SetUpdateInterval(ms);
    }

    void TestWindow::onChangeMinRequiredSpace() {
        const auto gb = d->ui.minSpaceSpinBox->value();
        d->storageInfoPanel->SetMinRequiredSpace(gb);
    }
}
