#pragma once
#include <memory>

#include <QtWidgets/QMainWindow>

namespace HTXpress::AppComponents::StorageInfoPanel::TEST {
	class TestWindow : public QMainWindow {
		Q_OBJECT
	public:
		using Self = TestWindow;
		explicit TestWindow(QWidget* parent = nullptr);
		~TestWindow() override;

	private slots:
		void onSelectFolder();
		void OnUpdateStorageInfo(const int64_t& total, const int64_t& free);
		void onRefresh();
		void onChangeInterval();
		void onChangeMinRequiredSpace();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

}
