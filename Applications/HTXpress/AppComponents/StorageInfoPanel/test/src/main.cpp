#include "TestWindow.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HTXpress::AppComponents::StorageInfoPanel::TEST::TestWindow w;
    w.show();
    return a.exec();
}
