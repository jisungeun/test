﻿#include <QDebug>

#include "StorageInfoPanelControl.h"

namespace HTXpress::AppComponents::StorageInfoPanel {
    constexpr int64_t KB = 1024;
    constexpr int64_t MB = 1048576;
    constexpr int64_t GB = 1073741824;
    constexpr int64_t TB = 1099511627776;

    struct StorageInfoPanelControl::Impl {
        using UnitName = QString;
        using Denominator = int64_t;

        int64_t bytesTotal{};
        int64_t bytesAvailable{};

        int64_t bytesRequired{};

        auto IsValid() const -> bool;
        auto IsEnough() const -> bool;
        auto GetUnit() const -> QPair<UnitName, Denominator>;
    };

    StorageInfoPanelControl::StorageInfoPanelControl() : d{std::make_unique<Impl>()} {
    }

    StorageInfoPanelControl::~StorageInfoPanelControl() {
    }

    auto StorageInfoPanelControl::SetBytesTotal(const int64_t& bytes) -> void {
        d->bytesTotal = bytes;
    }

    auto StorageInfoPanelControl::SetBytesAvailable(const int64_t& bytes) -> void {
        d->bytesAvailable = bytes;
    }

    auto StorageInfoPanelControl::GetAvailableSpace() const -> QString {
        if (!d->IsValid()) return "Invalid";

        const auto& pair = d->GetUnit();
        const auto unit = pair.first;
        const auto total = QString::number(static_cast<double>(d->bytesTotal) / pair.second, 'f', 3);
        const auto free = QString::number(static_cast<double>(d->bytesAvailable) / pair.second, 'f', 3);

        auto txt = QString("%1 %2 free of %3 %2").arg(free).arg(unit).arg(total);
        if(!d->IsEnough()) {
            txt.replace(QRegExp(txt),
                        "<span style=\"color:#D55C56; font-weight:bold;\">" + txt + "</span>");
        }
        return txt;
    }

    auto StorageInfoPanelControl::SetGigabytesMinRequired(const int32_t& gb) -> void {
        d->bytesRequired = gb * GB;
    }

    auto StorageInfoPanelControl::Impl::IsValid() const -> bool {
        if (bytesTotal <= 0 || bytesAvailable < 0) return false;
        if (bytesAvailable > bytesTotal) return false;
        return true;
    }

    auto StorageInfoPanelControl::Impl::IsEnough() const -> bool {
        return bytesAvailable > bytesRequired;
    }

    auto StorageInfoPanelControl::Impl::GetUnit() const -> QPair<UnitName, Denominator> {
        QPair<UnitName, Denominator> pair;
        if (bytesAvailable > TB) {
            pair.first = "TB";
            pair.second = TB;
        }
        else if (bytesAvailable > GB) {
            pair.first = "GB";
            pair.second = GB;
        }
        else if (bytesAvailable > MB) {
            pair.first = "MB";
            pair.second = MB;
        }
        else {
            pair.first = "KB";
            pair.second = KB;
        }

        return pair;
    }
}
