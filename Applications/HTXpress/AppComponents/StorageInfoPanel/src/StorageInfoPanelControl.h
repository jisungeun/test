﻿#pragma once
#include <memory>

#include <QString>

namespace HTXpress::AppComponents::StorageInfoPanel {
    class StorageInfoPanelControl final {
    public:

        using Self = StorageInfoPanelControl;
        using Pointer = std::shared_ptr<Self>;

        StorageInfoPanelControl();
        ~StorageInfoPanelControl();

        auto SetBytesTotal(const int64_t& bytes) -> void;
        auto SetBytesAvailable(const int64_t& bytes) -> void;
        auto SetGigabytesMinRequired(const int32_t& gb) -> void;

        auto GetAvailableSpace() const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
