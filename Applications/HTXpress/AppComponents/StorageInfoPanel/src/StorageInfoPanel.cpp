﻿#include "StorageInfoPanel.h"
#include "ui_StorageInfoPanel.h"
#include "StorageInfoPanelControl.h"

namespace HTXpress::AppComponents::StorageInfoPanel {
    struct StorageInfoPanel::Impl {
        explicit Impl(StorageInfoPanel* self) : self(self) {
            ui.setupUi(self);
        }

        StorageInfoPanel* self{};
        Ui::Widget ui{};
        StorageInfoPanelControl control{};

        auto InitUI() const -> void;
        auto UpdateUI() const -> void;
    };

    StorageInfoPanel::StorageInfoPanel(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>(this)} {
        d->InitUI();

        connect(d->ui.refreshButton, &QToolButton::clicked, this, &Self::sigRefresh);
    }

    StorageInfoPanel::~StorageInfoPanel() {
    }

    auto StorageInfoPanel::SetStorageInfo(const int64_t& bytesTotal, const int64_t& bytesAvailable) -> void {
        d->control.SetBytesTotal(bytesTotal);
        d->control.SetBytesAvailable(bytesAvailable);

        d->UpdateUI();
    }

    auto StorageInfoPanel::SetMinRequiredSpace(const int32_t& gb) -> void {
        d->control.SetGigabytesMinRequired(gb);

        d->UpdateUI();
    }

    auto StorageInfoPanel::Impl::InitUI() const -> void {
        self->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);

        ui.availableSpaceLabel->setAlignment(Qt::AlignRight|Qt::AlignVCenter);

        const auto btnStyleSheet = QString("QToolButton {border:0px none; background-color:transparent;}");
        ui.refreshButton->setStyleSheet(btnStyleSheet);
        ui.refreshButton->setIcon(QIcon(":/icon/refresh"));
        ui.refreshButton->setFixedSize(24, 24);
    }

    auto StorageInfoPanel::Impl::UpdateUI() const -> void {
        ui.availableSpaceLabel->setText(control.GetAvailableSpace());
    }
}
