#pragma once
#include <memory>
#include <QString>

namespace HTXpress::AppComponents::ImagingSystem {
    class FolderGenerator {
    public:
        using Pointer = std::shared_ptr<FolderGenerator>;

    protected:
        FolderGenerator();

    public:
        ~FolderGenerator();

        static auto GetInstance()->Pointer;

        static auto Create(const QString& path)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}