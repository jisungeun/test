#pragma once
#include <memory>
#include <QThread>

namespace HTXpress::AppComponents::ImagingSystem {
    class LogicalTimeStampLogger : public QThread {
        Q_OBJECT
    public:
        using Pointer = std::shared_ptr<LogicalTimeStampLogger>;

    protected:
        LogicalTimeStampLogger();

    public:
        ~LogicalTimeStampLogger();

        static auto GetInstance()->Pointer; 

        auto SetTopPath(const QString& path)->void;
        auto Add(const QString& path, int32_t timeInSec)->void;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}