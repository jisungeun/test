#pragma once
#include <memory>
#include <QString>
#include <QList>

#include <Experiment.h>
#include <PositionGroup.h>

namespace HTXpress::AppComponents::ImagingSystem {
    class ImagingStateMachine {
    public:
        ImagingStateMachine();
        ~ImagingStateMachine();
        
        auto SetScenario(AppEntity::ImagingScenario::Pointer scenario)->void;
        auto SetPath(const QString& topPath)->void;
        auto SetPositions(const QList<AppEntity::PositionGroup>& positions)->void;
        
        auto Start(int32_t index)->void;
        auto Next() const->QString;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}