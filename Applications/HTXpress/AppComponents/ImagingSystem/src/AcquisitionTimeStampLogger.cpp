#include <QString>
#include <QMap>
#include <QMutex>
#include <QWaitCondition>
#include <QTextStream>
#include <QDir>
#include <QFileInfo>

#include "FolderGenerator.h"
#include "AcquisitionTimeStampLogger.h"

namespace HTXpress::AppComponents::ImagingSystem {
    struct AcquisitionTimeStampLogger::Impl {
        QMap<QString, QDateTime> items;
        QList<QString> history;

        QMutex mutex;
        QWaitCondition waitCond;
        bool running{ true };
    };

    AcquisitionTimeStampLogger::AcquisitionTimeStampLogger() : QThread(), d{new Impl} {
        start();
    }

    AcquisitionTimeStampLogger::~AcquisitionTimeStampLogger() {
        d->mutex.lock();
        d->running = false;
        d->waitCond.wakeOne();
        d->mutex.unlock();

        wait();
    }

    auto AcquisitionTimeStampLogger::GetInstance() -> Pointer {
        static Pointer theInstance{ new AcquisitionTimeStampLogger() };
        return theInstance;
    }

    auto AcquisitionTimeStampLogger::Start() -> void {
        d->history.clear();
    }

    auto AcquisitionTimeStampLogger::Add(const QString& path, const QDateTime& timestamp) -> void {
        QMutexLocker locker(&d->mutex);
        if(d->history.contains(path)) return;
        d->history.push_back(path);
        d->items[path] = timestamp;
        d->waitCond.wakeOne();
    }

    void AcquisitionTimeStampLogger::run() {
        while(d->running) {
            QMutexLocker locker(&d->mutex);
            if(d->items.isEmpty()) {
                d->waitCond.wait(locker.mutex());
                continue;
            }

            const auto path = d->items.firstKey();
            const auto timestamp = d->items.first();
            d->items.remove(path);
            locker.unlock();

            auto fullPath = QString("%1/acquisition_timestamp.txt").arg(path);

            QFileInfo info(fullPath);
            FolderGenerator::Create(info.absolutePath());

            QFile file(fullPath);
            if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
                continue;

            QTextStream out(&file);
            out << timestamp.toString("yyyyMMddHHmmsszzz");
        }
    }
    
}