#include <QMap>
#include <QMutex>
#include <QWaitCondition>
#include <QSettings>
#include <QFileInfo>

#include "FolderGenerator.h"
#include "PositionLogger.h"

namespace HTXpress::AppComponents::ImagingSystem {
    struct PositionLogger::Impl {
        QMap<QString, Position> items;
        QList<QString> history;
        QString topPath;

        QMutex mutex;
        QWaitCondition waitCond;
        bool running{ true };
    };

    PositionLogger::PositionLogger() : QThread(), d{new Impl} {
        start();
    }

    PositionLogger::~PositionLogger() {
        d->mutex.lock();
        d->running = false;
        d->waitCond.wakeOne();
        d->mutex.unlock();

        wait();
    }

    auto PositionLogger::GetInstance() -> Pointer {
        static Pointer theInstance{ new PositionLogger() };
        return theInstance;
    }

    auto PositionLogger::SetTop(const QString& path) -> void {
        d->topPath = path;
        d->history.clear();
    }

    auto PositionLogger::Add(const QString& path, const Position& position) -> void {
        QMutexLocker locker(&d->mutex);
        if(d->history.contains(path)) return;
        d->history.push_back(path);
        d->items[path] = position;
        d->waitCond.wakeOne();
    }

    void PositionLogger::run() {
        while(d->running) {
            QMutexLocker locker(&d->mutex);
            if(d->items.isEmpty()) {
                d->waitCond.wait(locker.mutex());
                continue;
            }

            const auto path = d->items.firstKey();
            const auto position = d->items.first();
            d->items.remove(path);
            locker.unlock();

            const auto fullPath = QString("%1/%2/position.ini").arg(d->topPath).arg(path);

            QFileInfo info(fullPath);
            FolderGenerator::Create(info.absolutePath());

            QSettings qs(fullPath, QSettings::IniFormat);
            qs.setValue("X", position.x);
            qs.setValue("Y", position.y);
            qs.setValue("Z", position.z);
            qs.setValue("C", position.c);
        }
    }
}