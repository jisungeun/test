#include <QList>

#include "ImagingStates.h"
#include "ImagingStateMachine.h"

namespace HTXpress::AppComponents::ImagingSystem {
    struct ImagingStateMachine::Impl {
        AppEntity::ImagingScenario::Pointer scenario;
        QString topPath;
        QList<AppEntity::PositionGroup> positions;

        AppEntity::ImagingSequence::Pointer currentSequence{ nullptr };
        int32_t timeSlice{ 0 };

        State::Pointer topState;
    };

    ImagingStateMachine::ImagingStateMachine() : d{new Impl} {
    }

    ImagingStateMachine::~ImagingStateMachine() {
    }

    auto ImagingStateMachine::SetScenario(AppEntity::ImagingScenario::Pointer scenario) -> void {
        d->scenario = scenario;
    }

    auto ImagingStateMachine::SetPath(const QString& topPath) -> void {
        d->topPath = topPath;
    }

    auto ImagingStateMachine::SetPositions(const QList<AppEntity::PositionGroup>& positions) -> void {
        d->positions = positions;
    }

    auto ImagingStateMachine::Start(int32_t index) -> void {
        if(index==0) d->timeSlice = 0;
        d->currentSequence = d->scenario->GetSequence(index);

        auto timeSliceState = std::make_shared<TimeSliceState>(d->currentSequence->GetTimeCount(), d->timeSlice);
        timeSliceState->SetTimestamp(d->scenario->GetStartTime(index), d->currentSequence->GetInterval());
        d->topState = timeSliceState;

        auto positionState = std::make_shared<PositionState>(d->positions);
        timeSliceState->SetNext(positionState);

        auto modalityState = std::make_shared<ModalityState>(d->currentSequence->GetImagingCondition());
        positionState->SetNext(modalityState);

        d->timeSlice = d->timeSlice + d->currentSequence->GetTimeCount();
    }

    auto ImagingStateMachine::Next() const -> QString {
        return d->topPath + "/" + d->topState->Handle();
    }
}
