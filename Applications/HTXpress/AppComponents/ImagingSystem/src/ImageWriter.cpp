#define LOGGER_TAG "[ImageWriter]"

#include <QMutex>
#include <QWaitCondition>
#include <QList>
#include <QFileInfo>

#pragma warning(push)
#pragma warning(disable: 4819)
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#pragma warning(pop)

#include <TCLogger.h>

#include "FolderGenerator.h"
#include "ImageWriter.h"

#include <QDir>

namespace HTXpress::AppComponents::ImagingSystem {
    struct ImageWriter::Impl {
        QMutex mutex;
        QWaitCondition wait;
        bool running{ true };

        QList<AppEntity::RawImage::Pointer> images;
        QList<QString> pathes;
    };

    ImageWriter::ImageWriter() : QThread(), d{new Impl} {
        start();
    }

    ImageWriter::~ImageWriter() {
        d->mutex.lock();
        d->running = false;
        d->wait.wakeOne();
        d->mutex.unlock();

        wait();
    }

    auto ImageWriter::AddImage(AppEntity::RawImage::Pointer image, const QString& path) -> void {
        QMutexLocker locker(&d->mutex);
        d->images.push_back(image);
        d->pathes.push_back(path);
    }

    auto ImageWriter::IsEmpty() const -> bool {
        QMutexLocker locker(&d->mutex);
        return d->images.isEmpty();
    }

    void ImageWriter::run() {
        QLOG_INFO() << "ImageWriter thread is started";

        while(d->running) {
            QMutexLocker locker(&d->mutex);
            if(d->images.isEmpty()) {
                d->wait.wait(&d->mutex, 200);
                continue;
            }

            auto image = d->images.front();
            auto path = d->pathes.front();

            cv::Mat cvImg(image->GetSizeY(),
                          image->GetSizeX(),
                          CV_8UC1,
                          const_cast<uchar*>(image->GetBuffer().get()),
                          image->GetSizeX());

            QFileInfo info(path);
            FolderGenerator::Create(info.absolutePath());

            try {
                if(!cv::imwrite(path.toStdString(), cvImg)) {
                    QLOG_ERROR() << "It fails to save image to " << path;
                }
            } catch(cv::Exception& ex) {
                QLOG_ERROR() << ex.what();
            } catch(std::exception& ex) {
                QLOG_ERROR() << ex.what();
            } catch(...) {
                QLOG_ERROR() << "Unknown exception";
            }

            d->images.pop_front();
            d->pathes.pop_front();
        }

        QLOG_INFO() << "ImageWriter thread is stopped";
    }
}
