#pragma once
#include <memory>
#include <QThread>
#include <QString>

#include <Position.h>

namespace HTXpress::AppComponents::ImagingSystem {
    class PositionLogger : public QThread {
        Q_OBJECT

    public:
        using Pointer = std::shared_ptr<PositionLogger>;

        struct Position {
            double x, y, z, c;
        };

    protected:
        PositionLogger();

    public:
        ~PositionLogger() override;

        static auto GetInstance()->Pointer;

        auto SetTop(const QString& path)->void;
        auto Add(const QString& path, const Position& position)->void;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}