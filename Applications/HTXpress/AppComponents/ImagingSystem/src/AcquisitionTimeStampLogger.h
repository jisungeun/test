#pragma once
#include <memory>
#include <QThread>
#include <QDateTime>

namespace HTXpress::AppComponents::ImagingSystem {
    class AcquisitionTimeStampLogger : public QThread {
        Q_OBJECT
    public:
        using Pointer = std::shared_ptr<AcquisitionTimeStampLogger>;

    protected:
        AcquisitionTimeStampLogger();

    public:
        ~AcquisitionTimeStampLogger();

        static auto GetInstance()->Pointer; 

        auto Start()->void;
        auto Add(const QString& path, const QDateTime& timestamp)->void;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}