#define LOGGER_TAG "[ImageQueue]"

#include <QList>
#include <QMutex>

#include <TCLogger.h>

#include "ImageQueue.h"

namespace HTXpress::AppComponents::ImagingSystem {
    struct ImageQueue::Impl {
        QMutex mutex;
        QList<AppEntity::RawImage::Pointer> images;
        QList<int32_t> indexes;
        bool pause{ false };
        int32_t index{ 0 };
    };

    ImageQueue::ImageQueue() : d{new Impl} {
    }

    ImageQueue::~ImageQueue() {
        d->pause = true;
    }

    auto ImageQueue::SetIndex(int32_t index) -> void {
        d->index = index;
    }

    auto ImageQueue::Send(AppEntity::RawImage::Pointer image) -> void {
        QMutexLocker locker(&d->mutex);
        if(d->pause) return;
        d->images.push_back(image);
        d->indexes.push_back(d->index);
    }

    auto ImageQueue::IsEmpty() const -> bool {
        QMutexLocker locker(&d->mutex);
        return d->images.isEmpty();
    }

    auto ImageQueue::PopFirst() -> std::tuple<int32_t,AppEntity::RawImage::Pointer> {
        QMutexLocker locker(&d->mutex);
        if(d->images.isEmpty()) return std::make_tuple(-1, nullptr);

        auto image = d->images.first();
        d->images.pop_front();

        auto index = d->indexes.first();
        d->indexes.pop_front();

        return std::make_tuple(index, image);
    }
}
