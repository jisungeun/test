#pragma once
#include <memory>
#include <QString>

#include <ImagingCondition.h>
#include <PositionGroup.h>

namespace HTXpress::AppComponents::ImagingSystem {
    class State {
    public:
        using Pointer = std::shared_ptr<State>;

    public:
        State();
        virtual ~State();

        auto SetNext(State::Pointer state)->void;
        auto Handle(const QString& input = QString())->QString;

    protected:
        virtual auto GetResult(const QString& input, const bool increment)->QString = 0;
        virtual auto Completed() const->bool = 0;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
    
    class ModalityState : public State {
    public:
        explicit ModalityState(const QList<AppEntity::ImagingCondition::Pointer>& conditions);
        ~ModalityState();

    protected:
        auto GetResult(const QString& input, const bool increment) -> QString override;
        auto Completed() const -> bool override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
    
    class PositionState : public State {
    public:
        explicit PositionState(const QList<AppEntity::PositionGroup>& positions);
        ~PositionState();

    protected:
        auto GetResult(const QString& input, const bool increment) -> QString override;
        auto Completed() const -> bool override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
    
    class TimeSliceState : public State {
    public:
        TimeSliceState(int32_t count, int32_t offset = 0);
        ~TimeSliceState();

        auto SetTimestamp(int32_t startTimeInMSec, int32_t intervalInMSec)->void;

    protected:
        auto GetResult(const QString& input, const bool increment) -> QString override;
        auto Completed() const -> bool override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}