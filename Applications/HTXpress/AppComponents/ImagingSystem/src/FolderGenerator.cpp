#include <QMutex>
#include <QDir>

#include "FolderGenerator.h"

namespace HTXpress::AppComponents::ImagingSystem {
    struct FolderGenerator::Impl {
        QMutex mutex;
    };

    FolderGenerator::FolderGenerator() : d{new Impl} {
    }

    FolderGenerator::~FolderGenerator() {
    }

    auto FolderGenerator::GetInstance() -> Pointer {
        static Pointer theInstance{ new FolderGenerator() };
        return theInstance;
    }

    auto FolderGenerator::Create(const QString& path) -> void {
        QMutexLocker locker(&GetInstance()->d->mutex);
        auto dir = QDir(path);
        if(!dir.exists()) {
            dir.mkpath(path);
        }
    }
}