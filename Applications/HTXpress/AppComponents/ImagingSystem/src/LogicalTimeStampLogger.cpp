#include <QString>
#include <QMap>
#include <QMutex>
#include <QWaitCondition>
#include <QTextStream>
#include <QDir>
#include <QFileInfo>

#include "FolderGenerator.h"
#include "LogicalTimeStampLogger.h"

namespace HTXpress::AppComponents::ImagingSystem {
    struct LogicalTimeStampLogger::Impl {
        QMap<QString, int32_t> items;
        QList<QString> history;
        QString topPath;

        QMutex mutex;
        QWaitCondition waitCond;
        bool running{ true };
    };

    LogicalTimeStampLogger::LogicalTimeStampLogger() : QThread(), d{new Impl} {
        start();
    }

    LogicalTimeStampLogger::~LogicalTimeStampLogger() {
        d->mutex.lock();
        d->running = false;
        d->waitCond.wakeOne();
        d->mutex.unlock();

        wait();
    }

    auto LogicalTimeStampLogger::GetInstance() -> Pointer {
        static Pointer theInstance{ new LogicalTimeStampLogger() };
        return theInstance;
    }

    auto LogicalTimeStampLogger::SetTopPath(const QString& path) -> void {
        d->topPath = path;
        d->history.clear();
    }

    auto LogicalTimeStampLogger::Add(const QString& path, int32_t timeInSec) -> void {
        QMutexLocker locker(&d->mutex);
        if(d->history.contains(path)) return;
        d->history.push_back(path);
        d->items[path] = timeInSec;
        d->waitCond.wakeOne();
    }

    void LogicalTimeStampLogger::run() {
        while(d->running) {
            QMutexLocker locker(&d->mutex);
            if(d->items.isEmpty()) {
                d->waitCond.wait(locker.mutex());
                continue;
            }

            const auto path = d->items.firstKey();
            const auto timestamp = d->items.first();
            d->items.remove(path);
            locker.unlock();

            auto fullPath = QString("%1/%2/logical_timestamp.txt").arg(d->topPath).arg(path);

            QFileInfo info(fullPath);
            FolderGenerator::Create(info.absolutePath());

            QFile file(fullPath);
            if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
                continue;

            QTextStream out(&file);
            out << timestamp;
        }
    }
    
}