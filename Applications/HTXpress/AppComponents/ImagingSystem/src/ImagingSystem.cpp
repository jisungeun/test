#define LOGGER_TAG "[ImagingSystem]"

#include <QMutex>
#include <QWaitCondition>
#include <QFileInfo>

#include <TCLogger.h>

#include "AcquisitionTimeStampLogger.h"
#include "LogicalTimeStampLogger.h"
#include "PositionLogger.h"
#include "ImageQueue.h"
#include "ImagingStateMachine.h"
#include "ImageWriter.h"
#include "ImagingSystem.h"

namespace HTXpress::AppComponents::ImagingSystem {
    struct Component::Impl {
        ImageQueue::Pointer queue{ new ImageQueue() };
        QVector<QRgb> colormap{ QVector<QRgb>(256) };
        ImagingStateMachine stateMachine;
        ImageWriter writer;
        int32_t sequenceIndex{ -1 };

        QWaitCondition wait;
        QMutex mutex;
        bool running{ true };
    };

    Component::Component() : QThread(), d{new Impl} {
        start();
    }

    Component::~Component() {
        d->mutex.lock();
        d->running = false;
        d->wait.wakeOne();
        d->mutex.unlock();

        wait();

        QLOG_INFO() << "Destroyed";
    }
    
    auto Component::GetInstance() -> Pointer {
        static Pointer theInstance{ new Component() };
        return theInstance;
    }

    auto Component::GetImageQueue() const -> ImageQueue::Pointer {
        return d->queue;
    }

    auto Component::SetScenario(ImagingScenario::Pointer scenario, const QString& topPath) -> void {
        d->stateMachine.SetScenario(scenario);
        d->stateMachine.SetPath(topPath);
        PositionLogger::GetInstance()->SetTop(topPath);
        LogicalTimeStampLogger::GetInstance()->SetTopPath(topPath);
        AcquisitionTimeStampLogger::GetInstance()->Start();
        d->sequenceIndex = -1;
    }

    auto Component::SetPositions(const QList<PositionGroup>& positions) -> void {
        d->stateMachine.SetPositions(positions);
    }

    auto Component::StartNewAcquisition(uint32_t index) -> void {
        d->queue->SetIndex(index);
    }

    void Component::run() {
        QLOG_INFO() << "Component thread is started";

        while(d->running) {
            QMutexLocker lock(&d->mutex);
            if(d->queue->IsEmpty()) {
                d->wait.wait(&d->mutex, 200);
                continue;
            }

            auto [index, image] = d->queue->PopFirst();
            if(index != d->sequenceIndex) {
                d->stateMachine.Start(index);
                d->sequenceIndex = index;
            }
            auto path = d->stateMachine.Next();

            AcquisitionTimeStampLogger::GetInstance()->Add(QFileInfo(path).absolutePath(), image->GetTimestamp());
            d->writer.AddImage(image, path);
        }

        QLOG_INFO() << "Component thread is stopped";
    }
}
