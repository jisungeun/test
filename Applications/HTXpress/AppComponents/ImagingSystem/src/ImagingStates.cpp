#include <QFileInfo>

#include <TCLogger.h>
#include <SystemStatus.h>

#include "LogicalTimeStampLogger.h"
#include "PositionLogger.h"
#include "ImagingStates.h"

namespace HTXpress::AppComponents::ImagingSystem {
    struct State::Impl {
        Pointer next{ nullptr };
    };

    State::State() : d{new Impl} {
    }

    State::~State() {
    }

    auto State::SetNext(State::Pointer state) -> void {
        d->next = state;
    }

    auto State::Handle(const QString& input) -> QString {
        if(d->next) {
            auto output = d->next->Handle(input);
            auto completed = d->next->Completed();
            return GetResult(output, completed);
        }

        return GetResult(input, true);
    }

    struct ModalityState::Impl {
        struct Cond {
            QString strModality;
            int32_t count;
        };
        QList<Cond> conds;

        int32_t nthCond{ 0 };
        int32_t subIndex{ 0 };
        bool completed{ false };
    };

    ModalityState::ModalityState(const QList<AppEntity::ImagingCondition::Pointer>& conditions) : State(), d{new Impl} {
        for(auto condition : conditions) {
            auto modality = condition->GetModality();
            auto is3D = condition->Is3D();
            auto images = condition->GetImageCount();

            Impl::Cond cond;

            switch(modality) {
            case AppEntity::Modality::HT:
                if(is3D) cond.strModality = "HT3D";
                else cond.strModality = "HT2D";
                cond.count = images;
                d->conds.push_back(cond);
                break;
            case AppEntity::Modality::FL:
                {
                    auto flCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(condition);
                    auto channels = flCond->GetChannels();
                    cond.count = images / channels.size();                    
                    for(auto channel : channels) {
                        if(is3D) cond.strModality = QString("FL3D/CH%1").arg(channel);
                        else cond.strModality = QString("FL3D/CH%1").arg(channel);
                        d->conds.push_back(cond);
                    }
                }
                break;
            case AppEntity::Modality::BF:
                cond.strModality = "BF";
                cond.count = images;
                d->conds.push_back(cond);
                break;
            }

            QLOG_INFO() << "modality:" << modality._to_string() << " conds=" << d->conds.size();
        }
    }

    ModalityState::~ModalityState() {
    }

    auto ModalityState::GetResult(const QString& input, const bool increment) -> QString {
        Q_UNUSED(input)
        Q_UNUSED(increment)

        d->completed = false;

        auto cond = d->conds.at(d->nthCond);        
        const auto str = QString("%1/%2.png").arg(cond.strModality).arg(d->subIndex, 4, 10, QLatin1Char('0'));
        d->subIndex = d->subIndex + 1;

        if(d->subIndex >= cond.count) {
            d->subIndex = 0;
            d->nthCond = d->nthCond + 1;
            if(d->nthCond >= d->conds.length()) {
                d->nthCond = 0;
                d->completed = true;
            }
        }

        return str;
    }

    auto ModalityState::Completed() const -> bool {
        return d->completed;
    }

    struct PositionState::Impl {
        const QList<AppEntity::PositionGroup>& positions;
        int32_t groupIndex{ 0 };
        int32_t tileIndex{ 0 };
        bool completed{ false };

        explicit Impl(const QList<AppEntity::PositionGroup>& _positions) : positions(_positions) {}
    };

    PositionState::PositionState(const QList<AppEntity::PositionGroup>& positions) : State(), d{new Impl(positions)} {
    }

    PositionState::~PositionState() {
    }

    auto PositionState::GetResult(const QString& input, const bool increment) -> QString {
        d->completed = false;

        auto group = d->positions.at(d->groupIndex);
        const auto tileDir = QString("%1/data/%2").arg(group.GetTitle()).arg(d->tileIndex, 4, 10, QLatin1Char('0'));
        const auto str = QString("%1/%2").arg(tileDir).arg(input);

        auto logger = PositionLogger::GetInstance();
        const auto pos = d->positions.at(d->groupIndex).GetPositions().at(d->tileIndex).toMM();
        const auto cpos = AppEntity::SystemStatus::GetInstance()->GetCurrentCondensorPosition();
        logger->Add(tileDir, {pos.x, pos.y, pos.z, cpos});

        if(increment) {
            d->tileIndex = d->tileIndex + 1;
            if(d->tileIndex >= static_cast<int32_t>(group.GetCount())) {
                d->tileIndex = 0;
                d->groupIndex = d->groupIndex + 1;
            }

            if(d->groupIndex >= d->positions.length()) {
                d->groupIndex = 0;
                d->completed = true;
            }
        }

        return str;
    }

    auto PositionState::Completed() const -> bool {
        return d->completed;
    }

    struct TimeSliceState::Impl {
        int32_t count;
        int32_t offset;
        bool completed{ false };

        int32_t sratTimeInMSec;
        int32_t intervalInMSec;

        int32_t index{ 0 };
    };

    TimeSliceState::TimeSliceState(int32_t count, int32_t offset) : State(), d{new Impl} {
        d->count = count;
        d->offset = offset;
    }

    TimeSliceState::~TimeSliceState() {
    }

    auto TimeSliceState::SetTimestamp(int32_t startTimeInMSec, int32_t intervalInMSec) -> void {
        d->sratTimeInMSec = startTimeInMSec;
        d->intervalInMSec = intervalInMSec;
    }

    auto TimeSliceState::GetResult(const QString& input, const bool increment) -> QString {

        d->completed = false;

        const auto slashIndex = input.lastIndexOf("/");
        const auto str = input.left(slashIndex + 1) + QString("%1").arg(d->index + d->offset, 4, 10, QLatin1Char('0')) + "/" + input.mid(slashIndex + 1);

        LogicalTimeStampLogger::GetInstance()->Add(QFileInfo(str).path(), d->sratTimeInMSec + (d->intervalInMSec * d->index));

        if(increment) {
            d->index = d->index + 1;
            if(d->index >= d->count) {
                d->index = 0;
                d->completed = true;
            }
        }

        return str;
    }

    auto TimeSliceState::Completed() const -> bool {
        return d->completed;
    }

}
