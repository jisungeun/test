#pragma once
#include <memory>
#include <QString>
#include <QThread>

#include <RawImage.h>

namespace HTXpress::AppComponents::ImagingSystem {
    class ImageWriter : public QThread {
        Q_OBJECT

    public:
        ImageWriter();
        ~ImageWriter();

        auto AddImage(AppEntity::RawImage::Pointer image, const QString& path)->void;
        auto IsEmpty() const->bool;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}