#pragma once
#include <memory>
#include <RawImage.h>

#include "HTXImagingSystemExport.h"

namespace HTXpress::AppComponents::ImagingSystem {
    class HTXImagingSystem_API ImageQueue {
    public:
        using Pointer = std::shared_ptr<ImageQueue>;

    public:
        ImageQueue();
        ~ImageQueue();

        auto SetIndex(int32_t index) -> void;
        auto Send(AppEntity::RawImage::Pointer image) -> void;
        auto Clear() = delete;      //All images should be saved
        auto IsEmpty() const->bool;

        auto PopFirst()->std::tuple<int32_t, AppEntity::RawImage::Pointer>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}