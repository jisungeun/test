#pragma once
#include <memory>
#include <QImage>
#include <QThread>

#include <ImagingScenario.h>
#include <PositionGroup.h>

#include "ImageQueue.h"
#include "HTXImagingSystemExport.h"

namespace HTXpress::AppComponents::ImagingSystem {
    class HTXImagingSystem_API Component : public QThread {
        Q_OBJECT
        
    public:
        using Pointer = std::shared_ptr<Component>;
        using ImagingScenario = AppEntity::ImagingScenario;
        using PositionGroup = AppEntity::PositionGroup;
    
        Component();

    public:
        virtual ~Component();
        
        static auto GetInstance() -> Pointer;

        auto GetImageQueue() const -> ImageQueue::Pointer;

        auto SetScenario(ImagingScenario::Pointer scenario, const QString& topPath) -> void;
        auto SetPositions(const QList<PositionGroup>& positions) -> void;
        auto StartNewAcquisition(uint32_t index) -> void;

    protected:
        void run() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}