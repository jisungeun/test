#include <catch2/catch.hpp>

#include <ImagingStates.h>

namespace HTXpress::AppComponents::ImagingSystem::Test {
    using ImagingCondition = AppEntity::ImagingCondition;
    using ImagingConditionHT = AppEntity::ImagingConditionHT;
    using ImagingConditionFL = AppEntity::ImagingConditionFL;
    using ImagingConditionBF = AppEntity::ImagingConditionBF;
    using PositionGroup = AppEntity::PositionGroup;
    using Position = AppEntity::Position;

    TEST_CASE("Individual ImagingStates") {
        SECTION("ModalityState") {
            QList<ImagingCondition::Pointer> conditions;

            auto ht3D = std::make_shared<ImagingConditionHT>();
            ht3D->SetSliceCount(4);
            ht3D->SetDimension(3);
            conditions.push_back(ht3D);

            auto fl3D = std::make_shared<ImagingConditionFL>();
            fl3D->SetSliceCount(10);
            fl3D->SetDimension(3);
            fl3D->SetChannel(0, 0, 0, 0, 0, AppEntity::FLFilter(0, 0), AppEntity::FLFilter(0, 0), "CH0", {255, 0, 0});
            fl3D->SetChannel(1, 0, 0, 0, 0, AppEntity::FLFilter(0, 0), AppEntity::FLFilter(0, 0), "CH1", {0, 255, 0});
            conditions.push_back(fl3D);

            auto bfColor = std::make_shared<ImagingConditionBF>();
            bfColor->SetColorMode(true);
            conditions.push_back(bfColor);

            auto state = ModalityState(conditions);

            const auto htImages = static_cast<int32_t>(ht3D->GetImageCount());
            REQUIRE(htImages == 16);

            for(int32_t idx=0; idx<htImages; idx++) {
                const auto str = state.Handle();
                const auto ans = QString("HT3D/%1.png").arg(idx);

                REQUIRE(str == ans);
            }

            const auto flImages = static_cast<int32_t>(fl3D->GetImageCount());
            const auto channels = fl3D->GetChannels().size();

            REQUIRE(flImages == 20);
            REQUIRE(channels == 2);

            for(int32_t idx=0; idx<flImages; idx++) {
                const auto str = state.Handle();
                const auto ans = QString("FL3D/CH%1/%2.png").arg(idx%channels).arg(idx/channels);

                REQUIRE(str == ans);
            }

            const auto bfImages = static_cast<int32_t>(bfColor->GetImageCount());

            REQUIRE(bfImages == 3);

            for(int32_t idx=0; idx<bfImages; idx++) {
                const auto str = state.Handle();
                const auto ans = QString("BF/%1.png").arg(idx);

                REQUIRE(str == ans);
            }
        }

        SECTION("PositionState") {
            QList<PositionGroup> positions;

            const QList<int32_t> columnCounts{1, 1, 2, 1};

            int32_t groupIdx = 0;
            for(auto colCount : columnCounts) {
                auto group = PositionGroup(QString("Group_%1").arg(groupIdx));
                group.SetSize(colCount, colCount);
                positions.push_back(group);
                groupIdx = groupIdx + 1;
            }

            auto state = PositionState(positions);

            int32_t totalPositions = 0;
            std::for_each(columnCounts.begin(), columnCounts.end(), [&totalPositions](int32_t count) {
                totalPositions += count * count;
            });

            auto calcGroup = [&](int32_t posIdx)->int32_t {
                int32_t sum = 0;
                int32_t group = 0;

                for(auto colCount : columnCounts) {
                    sum += colCount * colCount;
                    if(posIdx < sum) return group;
                    group++;
                }

                return 0;
            };

            auto calcTileIdx = [&](int32_t posIdx)->int32_t {
                int32_t sum = 0;
                int32_t group = 0;

                for(auto colCount : columnCounts) {
                    sum += colCount * colCount;
                    if(posIdx < sum) {
                        const auto base = sum - (colCount * colCount);
                        return posIdx - base;
                    }
                    group++;
                }

                return 0;
            };

            for(int32_t posIdx=0; posIdx < totalPositions; posIdx++) {
                const auto str = state.Handle();
                const auto ans = QString("Group_%1/data/%2/").arg(calcGroup(posIdx)).arg(calcTileIdx(posIdx));

                REQUIRE(str == ans);
            }
        }

        SECTION("TimeSliceState") {
            auto state = TimeSliceState(10, 4);

            for(int32_t idx=0; idx<10; idx++) {
                const auto str = state.Handle("Group/data/0/HT3D/0.png");
                const auto ans = QString("Group/data/0/HT3D/%1/0.png").arg(idx+4);

                REQUIRE(str == ans);
            }
        }
    }
}
