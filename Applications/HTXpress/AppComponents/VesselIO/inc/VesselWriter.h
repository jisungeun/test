#pragma once

#include <memory>

#include <QString>

#include <Vessel.h>
#include <VesselRepo.h>

#include "HTXVesselIOExport.h"

namespace HTXpress::AppComponents::VesselIO {
	class HTXVesselIO_API VesselWriter {
	public:
		VesselWriter();
		~VesselWriter();

		auto Write(const QString& path, const AppEntity::Vessel::Pointer& vessel) const->bool;
		auto Write(const QString& dirPath, const AppEntity::VesselRepo::Pointer& repo) const->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}