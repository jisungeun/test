#pragma once

#include <memory>

#include <QString>

#include <Vessel.h>

#include "HTXVesselIOExport.h"

namespace HTXpress::AppComponents::VesselIO {
	class HTXVesselIO_API VesselReader {
	public:
		VesselReader();
		~VesselReader();

		auto Read(const QString& path) const->AppEntity::Vessel::Pointer;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}