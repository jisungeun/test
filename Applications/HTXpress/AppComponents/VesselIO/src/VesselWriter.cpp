#include "VesselWriter.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFileInfo>
#include <QDir>

#include "VesselIODefines.h"

namespace HTXpress::AppComponents::VesselIO {
	struct VesselWriter::Impl {
	    auto GetVesselJsonObject(const AppEntity::Vessel::Pointer& vessel)->QJsonObject;
	    auto GetWellJsonObject(const AppEntity::Vessel::Pointer& vessel)->QJsonObject;
	};


	auto VesselWriter::Impl::GetVesselJsonObject(const AppEntity::Vessel::Pointer& vessel) -> QJsonObject {
	    QJsonObject vesselObj;

		vesselObj[Key::Name] = vessel->GetName();
		vesselObj[Key::Model] = vessel->GetModel();
		vesselObj[Key::NA] = vessel->GetNA();
		vesselObj[Key::AfOffset] = vessel->GetAFOffset();
		vesselObj[Key::MultiDish] = vessel->IsMultiDish();
		vesselObj[Key::Size] = QJsonArray({vessel->GetSizeX(), vessel->GetSizeY()});

		return vesselObj;
	}

	auto VesselWriter::Impl::GetWellJsonObject(const AppEntity::Vessel::Pointer& vessel) -> QJsonObject {
	    QJsonObject wellObj;

		wellObj[Key::Size] = QJsonArray({vessel->GetWellSizeX(), vessel->GetWellSizeY()});
		wellObj[Key::Shape] = vessel->GetWellShape()._to_integral();
		wellObj[Key::Rows] = vessel->GetWellRows();
		wellObj[Key::Columns] = vessel->GetWellCols();
		wellObj[Key::Spacing] = QJsonArray({vessel->GetWellSpacingHorizontal(), vessel->GetWellSpacingVertical()});

		QJsonArray wellItemArray;
		for (auto row = 0; row < vessel->GetWellRows(); ++row) {
		    for (auto column = 0; column < vessel->GetWellCols(); ++column) {
				QJsonObject wellItemObj;
				wellItemObj[Key::Row] = row;
				wellItemObj[Key::Column] = column;
				wellItemObj[Key::Position] = QJsonArray({vessel->GetWellPositionX(row, column), vessel->GetWellPositionY(row, column)});
				wellItemObj[Key::Label] = vessel->GetWellLabel(row, column);

				wellItemArray << wellItemObj;
		    }
		}

		wellObj[Key::WellItems] = wellItemArray;

		QJsonObject imagingAreaObj;
		imagingAreaObj[Key::Size] = QJsonArray({vessel->GetImagingAreaSizeX(), vessel->GetImagingAreaSizeY()});
		imagingAreaObj[Key::Shape] = vessel->GetImagingAreaShape()._to_integral();
		imagingAreaObj[Key::Position] = QJsonArray({vessel->GetImagingAreaPositionX(), vessel->GetImagingAreaPositionY()});

		wellObj[Key::ImagingArea] = imagingAreaObj;

		return wellObj;
	}

	VesselWriter::VesselWriter() {

	}

	VesselWriter::~VesselWriter() {
	}

	auto VesselWriter::Write(const QString& path, const AppEntity::Vessel::Pointer& vessel) const -> bool {
		if (path.isEmpty() || vessel == nullptr) {
            return false;
        }

		QJsonObject obj;
		obj[Key::Vessel] = d->GetVesselJsonObject(vessel);
		obj[Key::Well] = d->GetWellJsonObject(vessel);

		// 파일로 저장
        QFileInfo fileInfo(path);
        QDir fileDir(fileInfo.absolutePath());
        if (!fileDir.exists()) {
            fileDir.mkpath(fileInfo.absolutePath());
        }

        QFile file(path);
        if (!file.open(QIODevice::WriteOnly)) {
            return false;
        }

        if (file.write(QJsonDocument(obj).toJson()) < -1) {
            return false;
        }

		return true;
	}

    auto VesselWriter::Write(const QString& dirPath, const AppEntity::VesselRepo::Pointer& repo) const -> bool  {
		// path = folder path
		for(const auto& vessel : repo->GetVessels()) {
		    const auto fileName = vessel->GetModel();
			const auto path = dirPath + "/" + fileName +".vessel";
			const bool writeResult = Write(path, vessel);
			if(!writeResult)
				return false;
		}

        return true;
    }
}
