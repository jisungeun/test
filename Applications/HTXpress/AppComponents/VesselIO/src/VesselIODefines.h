#pragma once

namespace HTXpress::AppComponents::VesselIO {
    namespace Key  {
		constexpr char Vessel[] = "vessel";
	    constexpr char Name[] = "name";
	    constexpr char Model[] = "model";
	    constexpr char NA[] = "NA";
		constexpr char AfOffset[] = "AFOffset";
		constexpr char MultiDish[] = "MultiDish";
	    constexpr char Well[] = "well";
	    constexpr char WellItems[] = "wellItems";
	    constexpr char ImagingArea[] = "imagingArea";
	    constexpr char Size[] = "size";
	    constexpr char Shape[] = "shape";
	    constexpr char Index[] = "index";
	    constexpr char Position[] = "position";
	    constexpr char Rows[] = "rows";
	    constexpr char Row[] = "row";
	    constexpr char Columns[] = "columns";
	    constexpr char Column[] = "column";
	    constexpr char Label[] = "label";
		constexpr char Spacing[] = "spacing";
    }
}