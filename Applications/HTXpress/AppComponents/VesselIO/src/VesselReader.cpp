#include "VesselReader.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

#include "VesselIODefines.h"

namespace HTXpress::AppComponents::VesselIO {
    struct VesselReader::Impl {
        AppEntity::Vessel::Pointer vessel{ nullptr };

        auto ParseVesselObject(const QJsonObject& obj)->void;
        auto ParseWellObject(const QJsonObject& obj)->void;
    };

    auto VesselReader::Impl::ParseVesselObject(const QJsonObject& obj) -> void {
        if (obj.contains(Key::Name) && obj[Key::Name].isString()) {
            vessel->SetName(obj[Key::Name].toString());
        }

        if (obj.contains(Key::Model) && obj[Key::Model].isString()) {
            vessel->SetModel(obj[Key::Model].toString());
        }

        if (obj.contains(Key::NA)) {
            vessel->SetNA(obj[Key::NA].toDouble());
        }

        if (obj.contains(Key::AfOffset)) {
            vessel->SetAFOffset(obj[Key::AfOffset].toInt());
        }

        if (obj.contains(Key::MultiDish)) {
            vessel->SetMultiDish(obj[Key::MultiDish].toBool());
        } else {
            vessel->SetMultiDish(false);
        }

        if (obj.contains(Key::Size) && obj[Key::Size].isArray()) {
            auto sizeArray = obj[Key::Size].toArray();
            if (sizeArray.size() == 2) {
                vessel->SetSize(sizeArray[0].toDouble(), sizeArray[1].toDouble());
            }
        }
    }

    auto VesselReader::Impl::ParseWellObject(const QJsonObject& obj) -> void {
        if (obj.contains(Key::Size) && obj[Key::Size].isArray()) {
            auto sizeArray = obj[Key::Size].toArray();
            if (sizeArray.size() == 2) {
                vessel->SetWellSize(sizeArray[0].toDouble(), sizeArray[1].toDouble());
            }
        }

        if (obj.contains(Key::Shape)) {
            vessel->SetWellShape(AppEntity::WellShape::_from_integral(obj[Key::Shape].toInt()));
        }

        int rows = 0, columns = 0;
        if (obj.contains(Key::Rows)) {
            rows = obj[Key::Rows].toInt();
        }

        if (obj.contains(Key::Columns)) {
            columns = obj[Key::Columns].toInt();
        }

        vessel->SetWellCount(rows, columns);

        if (obj.contains(Key::Spacing) && obj[Key::Spacing].isArray()) {
            auto spacingArray = obj[Key::Spacing].toArray();
            if (spacingArray.size() == 2) {
                vessel->SetWellSpacing(spacingArray[0].toDouble(), spacingArray[1].toDouble());
            }
        }

        // well items
        if (obj.contains(Key::WellItems) && obj[Key::WellItems].isArray()) {
            auto wellItemArray = obj[Key::WellItems].toArray();
            for (auto wellItem : wellItemArray) {
                auto wellItemObj = wellItem.toObject();

                int row = 0, column = 0;
                double x = 0., y = 0.;
                QString label;

                if (wellItemObj.contains(Key::Row)) {
                    row = wellItemObj[Key::Row].toInt();
                }

                if (wellItemObj.contains(Key::Column)) {
                    column = wellItemObj[Key::Column].toInt();
                }

                if (wellItemObj.contains(Key::Position) && wellItemObj[Key::Position].isArray()) {
                    auto wellPositionArray = wellItemObj[Key::Position].toArray();
                    if (wellPositionArray.size() == 2) {
                        x = wellPositionArray[0].toDouble();
                        y = wellPositionArray[1].toDouble();
                    }
                }

                if (wellItemObj.contains(Key::Label) && wellItemObj[Key::Label].isString()) {
                    label = wellItemObj[Key::Label].toString();
                }

                vessel->AddWell(row, column, x, y, label);
            }
        }

        // imaging area object
        if (obj.contains(Key::ImagingArea) && obj[Key::ImagingArea].isObject()) {
            auto imagingAreaObj = obj[Key::ImagingArea].toObject();

            if (imagingAreaObj.contains(Key::Size) && imagingAreaObj[Key::Size].isArray()) {
                auto imagingAreaSizeArray = imagingAreaObj[Key::Size].toArray();
                if (imagingAreaSizeArray.size() == 2) {
                    vessel->SetImagingAreaSize(imagingAreaSizeArray[0].toDouble(), imagingAreaSizeArray[1].toDouble());
                }
            }

            if (imagingAreaObj.contains(Key::Shape)) {
                vessel->SetImagingAreaShape(AppEntity::ImagingAreaShape::_from_integral(imagingAreaObj[Key::Shape].toInt()));
            }

            if (imagingAreaObj.contains(Key::Position) && imagingAreaObj[Key::Position].isArray()) {
                auto imagingAreaOffsetArray = imagingAreaObj[Key::Position].toArray();
                if (imagingAreaOffsetArray.size() == 2) {
                    vessel->SetImagingAreaPosition(imagingAreaOffsetArray[0].toDouble(), imagingAreaOffsetArray[1].toDouble());
                }
            }
        }
    }

    VesselReader::VesselReader() : d{ new Impl } {
        
    }

    VesselReader::~VesselReader() {
        
    }

    auto VesselReader::Read(const QString& path) const -> AppEntity::Vessel::Pointer {
        if (path.isEmpty()) {
            return nullptr;
        }

        QFile file(path);
        if (!file.open(QIODevice::ReadOnly)) {
            return nullptr;
        }

        const auto readData = file.readAll();
        const QJsonDocument jsonDoc(QJsonDocument::fromJson(readData));

        const auto json = jsonDoc.object();

        d->vessel = std::make_shared<AppEntity::Vessel>();

        // vessel object
        if (json.contains(Key::Vessel) && json[Key::Vessel].isObject()) {
            auto vesselObj = json[Key::Vessel].toObject();
            d->ParseVesselObject(vesselObj);
        }

        // well object
        if (json.contains(Key::Well) && json[Key::Well].isObject()) {
            auto wellObj = json[Key::Well].toObject();
            d->ParseWellObject(wellObj);
        }

        return d->vessel;
    }

}
