#include "TestDataGenerator.h"

#include <QDir>
#include <QString>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>

namespace HTXpress::AppComponents::VesselIO::Test {
    struct TestDataGenerator::Impl {
        QString testRoot{ QDir::tempPath() };

        HTXpress::AppEntity::Vessel::Pointer vessel{ nullptr };

        auto Generate()->bool;
        auto Clear()->void;
    };

    auto TestDataGenerator::Impl::Generate() -> bool {
        vessel = std::make_shared<AppEntity::Vessel>();

        vessel->SetModel("test-model");
        vessel->SetName("test-name");
        vessel->SetSize(1., 2.);

        vessel->SetWellShape(AppEntity::WellShape::Circle);
        vessel->SetWellSize(3., 4.);
        vessel->SetWellCount(1, 2);

        for (auto row = 0; row < vessel->GetWellRows(); ++row) {
            for (auto column = 0; column < vessel->GetWellCols(); ++column) {
                vessel->AddWell(row, column, row, column, QString("Well-%1-%2").arg(row).arg(column));
            }
        }

        for(auto idx : vessel->GetWellIndices()) {
            qDebug() << idx << ": " << vessel->GetWellPositionX(idx) << ", " << vessel->GetWellPositionY(idx);
        }

        vessel->SetImagingAreaShape(AppEntity::ImagingAreaShape::Circle);
        vessel->SetImagingAreaSize(7., 8.);
        vessel->SetImagingAreaPosition(9., 10.);

        for (auto row = 0; row < vessel->GetWellRows(); ++row) {
            for (auto column = 0; column < vessel->GetWellCols(); ++column) {
                qDebug() << "(" << row << "," << column << "): " << vessel->GetWellPositionX(row, column) << ", " << vessel->GetWellPositionY(row, column);
            }
        }


        return true;
    }

    auto TestDataGenerator::Impl::Clear() -> void {
        QDir dir(testRoot);
        dir.removeRecursively();
    }

    TestDataGenerator::TestDataGenerator() : d{ new Impl } {
        d->Generate();
    }

    TestDataGenerator::~TestDataGenerator() {
        d->Clear();
    }

    auto TestDataGenerator::Root() const -> QString {
        return d->testRoot;
    }

    auto TestDataGenerator::Vessel() const -> HTXpress::AppEntity::Vessel::Pointer {
        return d->vessel;
    }

}