#include <catch2/catch.hpp>

#include <QVector>

#include <VesselReader.h>
#include <VesselWriter.h>

#include "TestDataGenerator.h"

namespace HTXpress::AppComponents::VesselIO::Test {
    TEST_CASE("VesselMap Write/Read") {
        SECTION("Write/Read") {
            TestDataGenerator testData;
            auto originVessel = testData.Vessel();

            HTXpress::AppComponents::VesselIO::VesselWriter writer;

            QString testFile = "vesselmap_test.json";
            QString path = testData.Root() + "/" + testFile;

            REQUIRE(writer.Write(path, originVessel));

            SECTION("Read") {
                VesselReader reader;
                auto readVessel = reader.Read(path);
                REQUIRE(readVessel != nullptr);

                REQUIRE(*originVessel == *readVessel);
            }
        }
    }
}
