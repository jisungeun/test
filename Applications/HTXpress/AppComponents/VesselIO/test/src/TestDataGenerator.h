#pragma once

#include <memory>

#include <QString>

#include <Vessel.h>

namespace HTXpress::AppComponents::VesselIO::Test {
    class TestDataGenerator {
    public:
        TestDataGenerator();
        ~TestDataGenerator();

        auto Root() const->QString;
        auto Vessel() const->HTXpress::AppEntity::Vessel::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
