﻿#include <catch2/catch.hpp>

#include <QList>

#include <enum.h>
#include <Vessel.h>
#include <AppEntityDefines.h>

namespace HTXpress::AppComponents::VesselIO::Test {
    TEST_CASE("Vessel Entity Test") {
        SECTION("Interface Test") {
            auto vessel = std::make_shared<AppEntity::Vessel>();

            const QString model = "Model";
            const QString name = "Name";
            const double na = 1.1;
            const int32_t afOffset = 1;
            const bool multidish = true;
            const double sizeX = 111.111;
            const double sizeY = 99.99;
            const AppEntity::WellShape wellShape = AppEntity::WellShape::Circle;
            const double wellSpacingH = 33.33;
            const double wellSpacingV = 44.44;
            const double wellSizeX = 11.11;
            const double wellSizeY = 22.22;
            const int32_t wellRows = 3;
            const int32_t wellCols = 2;

            vessel->SetModel(model);
            vessel->SetName(name);
            vessel->SetNA(na);
            vessel->SetAFOffset(afOffset);
            vessel->SetMultiDish(multidish);
            vessel->SetSize(sizeX, sizeY);
            vessel->SetWellShape(wellShape);
            vessel->SetWellSpacing(wellSpacingH, wellSpacingV);
            vessel->SetWellSize(wellSizeX, wellSizeY);
            vessel->SetWellCount(wellRows, wellCols);

            for (auto row = 0; row < wellRows; ++row) {
                for (auto col = 0; col < wellCols; ++col) {
                    const double posX = 10 * col;
                    const double posY = 5 * row;
                    const QString label = QString("Well%1%2").arg(row).arg(col);
                    vessel->AddWell(row, col, posX, posY, label);
                }
            }

            const AppEntity::ImagingAreaShape imgShape = AppEntity::ImagingAreaShape::Rectangle;
            const double imgSizeX = 4.4;
            const double imgSizeY = 5.5;
            const double imgPosX = 2.2;
            const double imgPosY = 3.3;

            vessel->SetImagingAreaShape(imgShape);
            vessel->SetImagingAreaSize(imgSizeX, imgSizeY);
            vessel->SetImagingAreaPosition(imgPosX, imgPosY);

            SECTION("Vessel") {
                CHECK(vessel->GetModel() == model);
                CHECK(vessel->GetName() == name);
                CHECK(vessel->GetNA() == na);
                CHECK(vessel->GetAFOffset() == afOffset);
                CHECK(vessel->IsMultiDish() == multidish);
                CHECK(vessel->GetSizeX() == sizeX);
                CHECK(vessel->GetSizeY() == sizeY);
            }

            SECTION("Well") {
                CHECK(vessel->GetWellShape() == wellShape);
                CHECK(vessel->GetWellSpacingHorizontal() == wellSpacingH);
                CHECK(vessel->GetWellSpacingVertical() == wellSpacingV);
                CHECK(vessel->GetWellSizeX() == wellSizeX);
                CHECK(vessel->GetWellSizeY() == wellSizeY);
                CHECK(vessel->GetWellRows() == wellRows);
                CHECK(vessel->GetWellCols() == wellCols);
                CHECK(vessel->GetWellCount() == (wellRows * wellCols));

                for (auto row = 0; row < wellRows; ++row) {
                    for (auto col = 0; col < wellCols; ++col) {
                        const auto posX = 10 * col;
                        const auto posY = 5 * row;
                        const auto label = QString("Well%1%2").arg(row).arg(col);
                        CHECK(vessel->GetWellLabel(row, col) == label);
                        CHECK(vessel->GetWellPositionX(row, col) == posX);
                        CHECK(vessel->GetWellPositionY(row, col) == posY);
                    }
                }
            }

            SECTION("Imaging Area") {
                CHECK(vessel->GetImagingAreaShape() == imgShape);
                CHECK(vessel->GetImagingAreaSizeX() == imgSizeX);
                CHECK(vessel->GetImagingAreaSizeY() == imgSizeY);
                CHECK(vessel->GetImagingAreaPositionX() == imgPosX);
                CHECK(vessel->GetImagingAreaPositionY() == imgPosY);
            }

            SECTION("Operator") {
                AppEntity::Vessel::Pointer vessel2 = std::make_shared<AppEntity::Vessel>(*vessel);

                CHECK(*vessel == *vessel2);

                SECTION("Vessel") {
                    CHECK(vessel2->GetModel() == model);
                    CHECK(vessel2->GetName() == name);
                    CHECK(vessel2->GetNA() == na);
                    CHECK(vessel2->GetAFOffset() == afOffset);
                    CHECK(vessel2->IsMultiDish() == multidish);
                    CHECK(vessel2->GetSizeX() == sizeX);
                    CHECK(vessel2->GetSizeY() == sizeY);
                }

                SECTION("Well") {
                    CHECK(vessel2->GetWellShape() == wellShape);
                    CHECK(vessel2->GetWellSpacingHorizontal() == wellSpacingH);
                    CHECK(vessel2->GetWellSpacingVertical() == wellSpacingV);
                    CHECK(vessel2->GetWellSizeX() == wellSizeX);
                    CHECK(vessel2->GetWellSizeY() == wellSizeY);
                    CHECK(vessel2->GetWellRows() == wellRows);
                    CHECK(vessel2->GetWellCols() == wellCols);
                    CHECK(vessel2->GetWellCount() == (wellRows * wellCols));

                    for (auto row = 0; row < wellRows; ++row) {
                        for (auto col = 0; col < wellCols; ++col) {
                            const auto posX = 10 * col;
                            const auto posY = 5 * row;
                            const auto label = QString("Well%1%2").arg(row).arg(col);
                            CHECK(vessel2->GetWellLabel(row, col) == label);
                            CHECK(vessel2->GetWellPositionX(row, col) == posX);
                            CHECK(vessel2->GetWellPositionY(row, col) == posY);
                        }
                    }
                }

                SECTION("Imaging Area") {
                    CHECK(vessel2->GetImagingAreaShape() == imgShape);
                    CHECK(vessel2->GetImagingAreaSizeX() == imgSizeX);
                    CHECK(vessel2->GetImagingAreaSizeY() == imgSizeY);
                    CHECK(vessel2->GetImagingAreaPositionX() == imgPosX);
                    CHECK(vessel2->GetImagingAreaPositionY() == imgPosY);
                }

                SECTION("Compare after changing imaging area size") {
                    vessel->SetImagingAreaSize(1.1,2.2);
                    vessel2->SetImagingAreaSize(1.1,2.1);
                    CHECK((vessel == vessel2) == false);
                }
            }
        }
    }
}
