#define LOGGER_TAG "[ScanningPlanner]"
#include <cmath>

#include <TCLogger.h>
#include "ScanningPlanner.h"

namespace HTXpress::AppComponents::ScanningPlanner {
    struct ScanningPlanner::Impl {
        AppEntity::Area maximumFOV;
        uint32_t overlap{ 1 };                  //overlap amount in um
        QMap<WellIndex, QMap<LocationIndex,Location>> locations;
    };

    ScanningPlanner::ScanningPlanner() : d{new Impl} {
    }

    ScanningPlanner::~ScanningPlanner() {
    }

    auto ScanningPlanner::SetMaximumFOV(Area fov) -> void {
        d->maximumFOV = fov;
    }

    auto ScanningPlanner::SetOverlapInUM(uint32_t overlap) -> void {
        d->overlap = overlap;
    }

    auto ScanningPlanner::SetLocations(const QMap<WellIndex, QMap<LocationIndex,Location>>& locations) -> void {
        d->locations = locations;
    }

    auto ScanningPlanner::Perform() -> QList<PositionGroup> {
        using Position = AppEntity::Position;

        QList<AppEntity::PositionGroup> list;

        auto largerThanFOV = [=](const AppEntity::Location& location)->bool {
            const auto areaWidth = location.GetArea().Width();
            const auto areaHeight = location.GetArea().Height();

            if(areaWidth > d->maximumFOV.Width()) return true;
            if(areaHeight > d->maximumFOV.Height()) return true;
            return false;
        };

        for(auto locationsIter = d->locations.begin(); locationsIter!=d->locations.end(); locationsIter++) {
            const auto wellIndex = locationsIter.key();
            const auto& locations = locationsIter.value();

            for (auto locationIter = locations.begin(); locationIter != locations.end(); ++locationIter){
                const auto locationIndex = locationIter.key();
                const auto location = locationIter.value();
                
                if(!largerThanFOV(location)) {
                    auto group = PositionGroup(location.GetTitle(), location.GetCenter());
                    group.SetWellIndex(wellIndex);
                    group.SetLocationIndex(locationIndex);
                    list.push_back(group);
                } else {
                    PositionGroup group(location.GetTitle());
                    group.SetWellIndex(wellIndex);
                    group.SetLocationIndex(locationIndex);

                    const auto& area = location.GetArea();
                    const auto [cols, rows] = CalculateTileCountXY(area.Width(), area.Height(), d->maximumFOV.Width(), d->maximumFOV.Height(), d->overlap * 1000, d->overlap * 1000);
                    //const auto cols = CalculateTileCount(area.Width(), d->maximumFOV.Width(), d->overlap*1000);
                    //const auto rows = CalculateTileCount(area.Height(), d->maximumFOV.Height(), d->overlap*1000);
                    group.SetSize(rows, cols);

                    const auto fovUM = d->maximumFOV.toUM();

                    //delta movement of each direction
                    const auto dx = (fovUM.width - d->overlap);
                    const auto dy = (fovUM.height - d->overlap);
                    const auto halfX = fovUM.width / 2.0;
                    const auto halfY = fovUM.height / 2.0;

                    //offset to center
                    const auto halfAreaX = ((fovUM.width - d->overlap) * cols + d->overlap) / 2.0;
                    const auto halfAreaY = ((fovUM.height - d->overlap) * rows + d->overlap) / 2.0;
                    const auto offsetX = location.GetCenter().toUM().x - halfAreaX;
                    const auto offsetY = location.GetCenter().toUM().y - halfAreaY;

                    QLOG_INFO() << "halfArea=(" << halfAreaX << "," << halfAreaY << ")";
                    QLOG_INFO() << "center=(" << location.GetCenter().toUM().x << "," << location.GetCenter().toUM().y << ")";
                    QLOG_INFO() << "offsetX=(" << offsetX << "," << offsetY << ")";

                    group.SetCenter(location.GetCenter());

                    for(uint32_t rowIdx=0; rowIdx<rows; rowIdx++) {
                        for(uint32_t colIdx=0; colIdx<cols; colIdx++) {
                            const auto oX = dx * colIdx;
                            const auto oY = dy * rowIdx;

                            const auto centerX = oX + halfX + offsetX;
                            const auto centerY = oY + halfY + offsetY;
                            QLOG_INFO() << "center=(" << centerX << "," << centerY <<")";

                            const auto pos = Position::fromUM(centerX, centerY, location.GetCenter().toUM().z);
                            group.SetPosition(rowIdx, colIdx, pos);
                        }
                    }

                    list.push_back(group);
                }
            }
        }

        return list;
    }

    auto CalculateTileCount(const uint32_t& areaSize, const uint32_t& roi, const uint32_t& overlap) -> uint32_t {
        constexpr uint32_t overlapMargin = 4000; //nanometer

        if (areaSize <= roi) {
            return 1;
        }
        return static_cast<uint32_t>(std::ceil(static_cast<double>(areaSize - (overlap + overlapMargin)) / static_cast<double>(roi - (overlap + overlapMargin))));
    }

    auto CalculateLowAreaAxisTileCount(const uint32_t& areaSize, const uint32_t& roi, const uint32_t& overlap, const uint32_t& largerAxisTileCount)->uint32_t {
        const auto tileCount = CalculateTileCount(areaSize, roi, overlap);

        constexpr uint32_t extraMarginUnitLength = 4000; //nanometer
        const auto extraMarginTotal = (largerAxisTileCount - tileCount) * extraMarginUnitLength;

        const auto smallerAreaAxisTileCount = CalculateTileCount(areaSize + extraMarginTotal, roi, overlap);

        return smallerAreaAxisTileCount;
    }

    auto ScanningPlanner::CalculateTileCountXY(const uint32_t& areaSizeX, const uint32_t& areaSizeY,
        const uint32_t& roiX, const uint32_t& roiY, const uint32_t& overlapX,
        const uint32_t& overlapY) -> std::tuple<uint32_t, uint32_t> {

        const auto precalculatedTileCountX = CalculateTileCount(areaSizeX, roiX, overlapX);
        const auto precalculatedTileCountY = CalculateTileCount(areaSizeY, roiY, overlapY);

        //this algorithm assume that
        //the stage error of x,y axis is same magnification when stage moves.

        const auto tileCountXIsHigh = precalculatedTileCountX > precalculatedTileCountY;

        const auto lowAreaSize = tileCountXIsHigh ? areaSizeY : areaSizeX;
        const auto roiSizeOfLowArea = tileCountXIsHigh ? roiY : roiX;
        const auto overlapSizeOfLowArea = tileCountXIsHigh ? overlapY : overlapX;
        
        const auto tileCountOfHighCount = tileCountXIsHigh ? precalculatedTileCountX : precalculatedTileCountY;
        const auto tileCountOfLowCount = CalculateLowAreaAxisTileCount(lowAreaSize, roiSizeOfLowArea, overlapSizeOfLowArea, tileCountOfHighCount);
        
        const auto tileCountX = tileCountXIsHigh? tileCountOfHighCount : tileCountOfLowCount;
        const auto tileCountY = tileCountXIsHigh? tileCountOfLowCount : tileCountOfHighCount;

        return { tileCountX, tileCountY };
    }
}
