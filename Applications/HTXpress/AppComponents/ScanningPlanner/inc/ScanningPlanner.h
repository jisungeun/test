#pragma once
#include <memory>
#include <QList>

#include <Area.h>
#include <Location.h>
#include <PositionGroup.h>

#include "HTXScanningPlannerExport.h"

namespace HTXpress::AppComponents::ScanningPlanner {
    class HTXScanningPlanner_API ScanningPlanner {
    public:
        using WellIndex = AppEntity::WellIndex;
        using Location = AppEntity::Location;
        using LocationIndex = AppEntity::LocationIndex;
        using Area = AppEntity::Area;
        using PositionGroup = AppEntity::PositionGroup;

    public:
        ScanningPlanner();
        ~ScanningPlanner();

        auto SetMaximumFOV(Area fov)->void;
        auto SetOverlapInUM(uint32_t overlap)->void;
        auto SetLocations(const QMap<WellIndex, QMap<LocationIndex,Location>>& locations)->void;

        auto Perform()->QList<PositionGroup>;

        static auto CalculateTileCountXY(const uint32_t& areaSizeX, const uint32_t& areaSizeY, 
            const uint32_t& roiX, const uint32_t& roiY, const uint32_t& overlapX, const uint32_t& overlapY)
            ->std::tuple<uint32_t, uint32_t>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}