#include <catch2/catch.hpp>

#include <QMap>
#include <QDebug>

#include <ScanningPlanner.h>

namespace HTXpress::AppComponents::ScanningPlanner::Test {
    using Location = AppEntity::Location;
    using LocationIndex = AppEntity::LocationIndex;

    TEST_CASE("Scanning Planning") {
        SECTION("No Stitching Required") {
            QMap<AppEntity::WellIndex, QMap<LocationIndex,Location>> locations;
            locations[0] = {{0,AppEntity::Location()}};
            auto&location = locations[0][0];

            location.SetCenter({10, 10, 0});
            location.SetArea({50, 50}, false);

            ScanningPlanner planner;
            planner.SetMaximumFOV({100, 100});
            planner.SetOverlapInUM(1);
            planner.SetLocations(locations);

            auto posGroups = planner.Perform();
            CHECK(posGroups.length() == 1);
            CHECK(posGroups.at(0).GetRows() == 1);
            CHECK(posGroups.at(0).GetCols() == 1);

            auto pos = posGroups.at(0).GetPosition(0, 0);
            CHECK(pos.X() == 10);
            CHECK(pos.Y() == 10);
        }

        SECTION("Stitching Required") {
            QMap<AppEntity::WellIndex, QMap<LocationIndex,Location>> locations;
            locations[0] = {{0,AppEntity::Location()}};
            auto&location = locations[0][0];

            location.SetCenter({0, 0, 0});
            location.SetArea({50, 50}, true);

            ScanningPlanner planner;
            planner.SetMaximumFOV({20, 20});
            planner.SetOverlapInUM(5);
            planner.SetLocations(locations);

            auto posGroup = planner.Perform();
            CHECK(posGroup.length() == 1);
            CHECK(posGroup.at(0).GetRows() == 3);
            CHECK(posGroup.at(0).GetCols() == 3);

            for(auto Idx=0; Idx<3; Idx++) {
                CHECK(posGroup.at(0).GetPosition(Idx, 0).X() == -15);
                CHECK(posGroup.at(0).GetPosition(Idx, 1).X() ==   0);
                CHECK(posGroup.at(0).GetPosition(Idx, 2).X() ==  15);
                CHECK(posGroup.at(0).GetPosition(0, Idx).Y() == -15);
                CHECK(posGroup.at(0).GetPosition(1, Idx).Y() ==   0);
                CHECK(posGroup.at(0).GetPosition(2, Idx).Y() ==  15);
            }
        }
    }
}