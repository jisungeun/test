﻿#include <MessageDialog.h>

#include "ImagingPointCopy.h"
#include "ImagingPointCopyDialog.h"
#include "ImagingPointCopyControl.h"

namespace HTXpress::AppComponents::ImagingPointCopyDialog {
    using Dialog = ImagingPointCopyDialog;

    struct ImagingPointCopy::Impl {
        ImagingPointCopyControl control{};
    };

    ImagingPointCopy::ImagingPointCopy(QObject* parent) : QObject(parent), d{std::make_unique<Impl>()} {
    }

    ImagingPointCopy::~ImagingPointCopy() {
    }

    auto ImagingPointCopy::SetInputData(const Input::Pointer& input) -> void {
        d->control.SetSourceIWellIndex(input->sourceWellIndex);
        d->control.SetTargetPoints(input->targetPoints);
        d->control.SetVesselMap(input->vesselMap);
        d->control.SetWellGroups(input->wellGroups);
    }

    auto ImagingPointCopy::GetOutputData() const -> Output::Pointer {
        return d->control.GetOutput();
    }

    auto ImagingPointCopy::Execute() -> int32_t {
        Dialog dlg;
        connect(&dlg, &Dialog::sigChangeCount, this, [this](const int32_t& rows, const int32_t& cols) {
            d->control.SetCount(rows, cols);
        });

        connect(&dlg, &Dialog::sigChangeSpacing, this, [this](const double& hor, const double& ver) {
            d->control.SetSpacing(hor, ver);
        });

        connect(&dlg, &Dialog::sigChangeTargetWells, this, [this](const QList<TC::WellIndex>& targetWells) {
            d->control.SetTargetWells(targetWells);
        });
        
        connect(&dlg, &Dialog::sigChangeType, this, [this](const Type& type) {
            d->control.SetType(type);
        });

        dlg.Init(d->control.GetInput());
        const auto [rows, cols] = d->control.GetCount();
        const auto [minHor, minVer] = d->control.GetMinSpacing();
        dlg.SetMatrixValues(rows, cols, minHor, minVer, d->control.IsMatrixEnable());
        dlg.SetCopyEnable(d->control.IsCopyEnable());

        if(!d->control.IsMatrixEnable() && !d->control.IsCopyEnable()) {
            TC::MessageDialog::warning(nullptr, tr("Warning"), tr("If there is only one well, please select only one imaging point."));
            return -1;
        }

        return dlg.exec();
    }
}
