﻿#pragma once

#include <QWidget>

namespace HTXpress::AppComponents::ImagingPointCopyDialog {
    class MatrixWidget final : public QWidget {
        Q_OBJECT
    public:
        explicit MatrixWidget(QWidget* parent = nullptr);
        ~MatrixWidget() override;

        auto SetRows(const int32_t& rows) -> void;
        auto SetColumns(const int32_t& cols) -> void;
        auto SetMinHorSpacing(const double& horizontal) -> void;
        auto SetMinVerSpacing(const double& vertical) -> void;
        
        auto GetCount() const -> std::pair<int32_t, int32_t>;
        auto GetSpacing() const -> std::pair<double, double>;

    signals:
        void sigSelectAll();
        void sigDeselectAll();
        void sigCountChanged(const int32_t& rows, const int32_t& cols);
        void sigSpacingChanged(const double& hor, const double& ver);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
