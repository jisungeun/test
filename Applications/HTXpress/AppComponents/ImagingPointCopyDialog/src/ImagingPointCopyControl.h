﻿#pragma once
#include <memory>

#include "ImagingPointCopyDefines.h"

namespace HTXpress::AppComponents::ImagingPointCopyDialog {
    class ImagingPointCopyControl final {
    public:
        using Self = ImagingPointCopyControl;
        using Pointer = std::shared_ptr<Self>;
        using Rows = int32_t;
        using Colums = int32_t;
        using Horizontal = double;
        using Vertical = double;

        ImagingPointCopyControl();
        ~ImagingPointCopyControl();

        auto SetSourceIWellIndex(const TC::WellIndex& sourceWellIndex) -> void;
        auto SetTargetPoints(const Locations& targetPoints) -> void;
        auto SetVesselMap(const TC::VesselMap::Pointer& vesselMap) -> void;
        auto SetWellGroups(const WellGroups& wellGroups) -> void;

        auto GetMinSpacing() const -> std::pair<Horizontal, Vertical>;

        auto SetType(const Type& type) -> void;
        auto GetType() const -> Type;
        auto SetSpacing(const Horizontal& hor, const Vertical& ver) -> void;
        auto GetSpacing() const -> std::pair<Horizontal, Vertical>;
        auto SetCount(const Rows& rows, const Colums& cols) -> void;
        auto GetCount() const -> std::pair<Rows, Colums>;

        auto SetTargetWells(const QList<TC::WellIndex>& targetWells) -> void;

        auto GetInput() const -> Input::Pointer;
        auto GetOutput() const -> Output::Pointer;

        auto IsMatrixEnable() const -> bool;
        auto IsCopyEnable() const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}