#include <QResizeEvent>

#include <VesselMapWidget.h>

#include "ui_ImagingPointCopyDialog.h"
#include "ImagingPointCopyDialog.h"

namespace HTXpress::AppComponents::ImagingPointCopyDialog {
    struct ImagingPointCopyDialog::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        Self* self{};
        QFrame* frame{};
        Ui::ImagingPointCopyDialog ui{};

        TC::WellIndex sourceWellIndex{};
        QList<TC::WellIndex> selectableWells{};
        Type currentType{Type::Copy};
        Locations initLocations{};

        auto GetSeletableWells(const QList<TC::WellIndex>& selectedWells) -> QList<TC::WellIndex>;
        auto Convert(const int32_t& tabIndex, bool* ok = nullptr) const -> Type;
        auto ClearSelection() -> void;
        auto UpdatePreview() -> void;
    };

    ImagingPointCopyDialog::ImagingPointCopyDialog(QWidget* parent)
    : CustomDialog(parent), d{std::make_unique<Impl>(this)} {
        d->frame = new QFrame(this);
        SetContext(d->frame);
        SetTitle("Copy Imaging Point");
        d->ui.setupUi(d->frame);

        d->frame->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        d->frame->setFixedSize(600,350);
        d->ui.vesselMapWidget->SetViewMode(TC::ViewMode::CopyDlgMode);
        d->ui.tabWidget->setCurrentIndex(CopyTab);

        connect(d->ui.tabWidget, &QTabWidget::currentChanged, this, &Self::onChangeTabIndex);

        connect(d->ui.copyWidget, &CopyWidget::sigSelectAll, this, &Self::onSelectAll);
        connect(d->ui.copyWidget, &CopyWidget::sigDeselctAll, this, &Self::onDeselectAll);
        connect(d->ui.matrixWidget, &MatrixWidget::sigSelectAll, this, &Self::onSelectAll);
        connect(d->ui.matrixWidget, &MatrixWidget::sigDeselectAll, this, &Self::onDeselectAll);
        connect(d->ui.matrixWidget, &MatrixWidget::sigCountChanged, this, &Self::onChangeCount);
        connect(d->ui.matrixWidget, &MatrixWidget::sigSpacingChanged, this, &Self::onChangeSpacing);
        connect(d->ui.vesselMapWidget, &TC::VesselMapWidget::sigSelectedWellIndices, this, &Self::onChangeSelectedWells);

        UpdateSize();
    }

    ImagingPointCopyDialog::~ImagingPointCopyDialog() {
    }

    auto ImagingPointCopyDialog::Init(const Input::Pointer& initData) -> void{
        const auto sourceWellIndex = initData->sourceWellIndex;
        d->sourceWellIndex = sourceWellIndex;
        d->ui.vesselMapWidget->SetVesselMap(initData->vesselMap);
        d->initLocations = initData->targetPoints;

        for(const auto& location : initData->targetPoints) {
            d->ui.vesselMapWidget->AddAcquisitionLocationByWellPos(sourceWellIndex,
                                                                   location.index,
                                                                   location.type,
                                                                   location.centerPos.x,
                                                                   location.centerPos.y,
                                                                   location.centerPos.z,
                                                                   location.size.width,
                                                                   location.size.height);
        }
        for(auto it = initData->wellGroups.cbegin(); it != initData->wellGroups.cend(); ++it) {
            d->ui.vesselMapWidget->CreateNewGroup(it.key(),
                                                  "",
                                                  it.value().first,
                                                  it.value().second);
            d->selectableWells.append(it.value().second);
        }

        d->ui.vesselMapWidget->SetSelectedWell(sourceWellIndex);
        d->ui.vesselMapWidget->FitWellCanvas();
    }

    auto ImagingPointCopyDialog::SetMatrixValues(const int32_t& rows, const int32_t& cols, const double& horSpacing, const double& verSpacing, bool enable) -> void {
        d->ui.matrixWidget->SetRows(rows);
        d->ui.matrixWidget->SetColumns(cols);
        d->ui.matrixWidget->SetMinHorSpacing(horSpacing);
        d->ui.matrixWidget->SetMinVerSpacing(verSpacing);
        d->ui.tabWidget->setTabEnabled(MatrixTab, enable);
    }

    auto ImagingPointCopyDialog::SetCopyEnable(const bool& enable) -> void {
        d->ui.tabWidget->setTabEnabled(CopyTab, enable);
    }

    auto ImagingPointCopyDialog::resizeEvent(QResizeEvent* event) -> void {
        d->frame->setFixedSize(event->size().width(), 350);
        d->ui.tabWidget->setFixedSize(d->frame->width(), 85);
        d->ui.vesselMapWidget->setFixedSize(d->frame->width()-8, 265);
        UpdateSize();
    }

    void ImagingPointCopyDialog::onChangeTabIndex(const int32_t& tabIndex) {
        const auto type = d->Convert(tabIndex);
        d->currentType = type;
        d->ClearSelection();
        d->ui.vesselMapWidget->SetMatrixItemVisible(d->currentType==+Type::Matrix);
        emit sigChangeType(type);
    }

    void ImagingPointCopyDialog::onChangeCount(const int32_t& rows, const int32_t& cols) {
        emit sigChangeCount(rows, cols);
        d->UpdatePreview();
    }

    void ImagingPointCopyDialog::onChangeSelectedWells(const QList<TC::WellIndex>& selectedWells) {
        const auto wells = d->GetSeletableWells(selectedWells);
        d->ui.vesselMapWidget->blockSignals(true);
        d->ui.vesselMapWidget->SetSelectedWellIndices(wells);
        d->ui.vesselMapWidget->blockSignals(false);
        emit sigChangeTargetWells(wells);
    }

    void ImagingPointCopyDialog::onChangeSpacing(const double& hor, const double& ver) {
        emit sigChangeSpacing(hor, ver);
        d->UpdatePreview();
    }

    void ImagingPointCopyDialog::onSelectAll() {
        onChangeSelectedWells(d->selectableWells);
    }

    void ImagingPointCopyDialog::onDeselectAll() {
        onChangeSelectedWells({});
    }

    auto ImagingPointCopyDialog::Impl::GetSeletableWells(const QList<TC::WellIndex>& selectedWells) -> QList<TC::WellIndex> {
        QList<TC::WellIndex> result;
        for(const auto& wellIndex : selectedWells) {
            if(selectableWells.contains(wellIndex)) {
                result.push_back(wellIndex);
            }
        }

        if(currentType == +Type::Copy) {
            if(result.contains(sourceWellIndex)) {
                result.removeOne(sourceWellIndex);
            }
        }
        else if(currentType == +Type::Matrix) {
            if(!result.contains(sourceWellIndex)) {
                result.push_back(sourceWellIndex);
            }
        }

        return result;
    }

    auto ImagingPointCopyDialog::Impl::Convert(const int32_t& tabIndex, bool* ok) const -> Type {
        bool tempOK{};
        Type::_enumerated convertedType{};
        switch (tabIndex) {
            case CopyTab: 
                tempOK = true;
                convertedType = Type::Copy;
                break;
            case MatrixTab: 
                tempOK = true;
                convertedType = Type::Matrix;
                break;
            default: 
                tempOK = false;
                convertedType = Type::Copy;
                break;
        }

        if (ok != nullptr) {
            *ok = tempOK;
        }

        return convertedType;
    }

    auto ImagingPointCopyDialog::Impl::ClearSelection() -> void {
        self->onDeselectAll();
    }

    auto ImagingPointCopyDialog::Impl::UpdatePreview() -> void {
        // col : right to left
        // row : top to bottom
        const auto[rows, cols] = ui.matrixWidget->GetCount();
        const auto[hor, ver] = ui.matrixWidget->GetSpacing();
        const auto& location = initLocations.first();
        const auto startX = location.centerPos.x;
        const auto startY = location.centerPos.y;
        const auto cx = std::floor((cols-1)/2);
        const auto cy = std::floor((rows-1)/2);

        QList<QPair<double, double>> positions;
        for(int r = 0; r < rows; r++) {
            auto y = startY + (r - cy)*ver;
            for(int c = 0; c < cols; c++) {
                auto x = startX + (c - cx)*hor;
                positions.push_back({x,y});
            }
        }

        ui.vesselMapWidget->SetMatrixItems(positions);
    }
}
