﻿#include "ImagingPointCopyControl.h"

namespace HTXpress::AppComponents::ImagingPointCopyDialog {
    struct ImagingPointCopyControl::Impl {
        Input::Pointer input{};
        Output::Pointer output{};
    };

    ImagingPointCopyControl::ImagingPointCopyControl() : d{std::make_unique<Impl>()} {
        d->input = std::make_shared<Input>();
        d->output = std::make_shared<Output>();
    }

    ImagingPointCopyControl::~ImagingPointCopyControl() {
    }

    auto ImagingPointCopyControl::SetSourceIWellIndex(const TC::WellIndex& sourceWellIndex) -> void {
        d->input->sourceWellIndex = sourceWellIndex;
        d->output->sourceWellIndex = sourceWellIndex;
    }

    auto ImagingPointCopyControl::SetTargetPoints(const Locations& targetPoints) -> void {
        d->input->targetPoints = targetPoints;
        for(const auto& location : targetPoints) {
            d->output->targetPointIndices.push_back(location.index);
        }
    }

    auto ImagingPointCopyControl::SetVesselMap(const TC::VesselMap::Pointer& vesselMap) -> void {
        d->input->vesselMap = vesselMap;
    }

    auto ImagingPointCopyControl::SetWellGroups(const WellGroups& wellGroups) -> void {
        d->input->wellGroups = wellGroups;
    }

    auto ImagingPointCopyControl::GetMinSpacing() const -> std::pair<Horizontal, Vertical> {
        auto minHor = 0.;
        auto minVer = 0.;
        if(IsMatrixEnable()) {
            minHor = d->input->targetPoints.first().size.width;
            minVer = d->input->targetPoints.first().size.height;
        }

        return {minHor, minVer};
    }

    auto ImagingPointCopyControl::SetType(const Type& type) -> void {
        d->output->type = type;
        
        if(type == +Type::Copy) {
            if(d->output->targetWells.contains(d->input->sourceWellIndex)) {
                d->output->targetWells.removeOne(d->input->sourceWellIndex);
            }
        }
        else if(type == +Type::Matrix) {
            if(!d->output->targetWells.contains(d->input->sourceWellIndex)) {
                d->output->targetWells.push_back(d->input->sourceWellIndex);
            }
        }
    }

    auto ImagingPointCopyControl::GetType() const -> Type {
        return d->output->type;
    }

    auto ImagingPointCopyControl::GetSpacing() const -> std::pair<Horizontal, Vertical> {
        return {d->output->spacing.hor, d->output->spacing.ver};
    }

    auto ImagingPointCopyControl::GetCount() const -> std::pair<Rows, Colums> {
        return {d->output->count.row, d->output->count.col};
    }

    auto ImagingPointCopyControl::SetSpacing(const double& hor, const double& ver) -> void {
        d->output->spacing = {hor, ver};
    }

    auto ImagingPointCopyControl::SetCount(const int32_t& rows, const int32_t& cols) -> void {
        d->output->count = {rows, cols};
    }

    auto ImagingPointCopyControl::SetTargetWells(const QList<TC::WellIndex>& targetWells) -> void {
        d->output->targetWells = targetWells;
    }

    auto ImagingPointCopyControl::GetInput() const -> Input::Pointer {
        return d->input;
    }

    auto ImagingPointCopyControl::GetOutput() const -> Output::Pointer {
        return d->output;
    }

    auto ImagingPointCopyControl::IsMatrixEnable() const -> bool {
        return d->input->targetPoints.size() == 1;
    }

    auto ImagingPointCopyControl::IsCopyEnable() const -> bool {
        return d->input->vesselMap->GetWells().size() > 1;
    }
}
