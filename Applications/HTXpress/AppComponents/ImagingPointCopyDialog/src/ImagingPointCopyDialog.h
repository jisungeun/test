#pragma once
#include <memory>

#include <CustomDialog.h>
#include <VesselMap.h>

#include "ImagingPointCopyDefines.h"

namespace HTXpress::AppComponents::ImagingPointCopyDialog {
    class ImagingPointCopyDialog final : public TC::CustomDialog {
        Q_OBJECT
    public:
        using Self = ImagingPointCopyDialog;
        using Pointer = std::shared_ptr<Self>;

        enum TabIndex {
            CopyTab = 0,
            MatrixTab,
        };

        explicit ImagingPointCopyDialog(QWidget* parent = nullptr);
        ~ImagingPointCopyDialog() override;

        auto Init(const Input::Pointer& initData) -> void;
        auto SetMatrixValues(const int32_t& rows, const int32_t& cols, const double& horSpacing, const double& verSpacing, bool enable) -> void;
        auto SetCopyEnable(const bool& enable) -> void;

    protected:
        auto resizeEvent(QResizeEvent* event) -> void override;

    signals:
        void sigChangeType(const HTXpress::AppComponents::ImagingPointCopyDialog::Type& type);
        void sigChangeCount(const int32_t& rows, const int32_t& cols);
        void sigChangeSpacing(const double& hor, const double& ver);
        void sigChangeTargetWells(const QList<TC::WellIndex>& targetWells);

    private slots:
        void onChangeTabIndex(const int32_t& tabIndex);
        void onChangeCount(const int32_t& rows, const int32_t& cols);
        void onChangeSelectedWells(const QList<TC::WellIndex>& selectedWells);
        void onChangeSpacing(const double& hor, const double& ver);
        void onSelectAll();
        void onDeselectAll();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
