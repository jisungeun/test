﻿#pragma once
#include <QWidget>

namespace HTXpress::AppComponents::ImagingPointCopyDialog {
    class CopyWidget final : public QWidget {
        Q_OBJECT
    public:
        explicit CopyWidget(QWidget* parent = nullptr);
        ~CopyWidget() override;

    signals:
        void sigSelectAll();
        void sigDeselctAll();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
