﻿#include "CopyWidget.h"
#include "ui_CopyWidget.h"

namespace HTXpress::AppComponents::ImagingPointCopyDialog {
    struct CopyWidget::Impl {
        explicit Impl(CopyWidget* self) : self(self) {
            ui.setupUi(self);
        }
        CopyWidget* self{};
        Ui::CopyWidget ui;
    };
    CopyWidget::CopyWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>(this)}{
        d->ui.selectAllBtn->setObjectName("bt-setup-light");
        d->ui.deselectAllBtn->setObjectName("bt-setup-light");

        connect(d->ui.selectAllBtn, &QPushButton::clicked, this, [=] {
            emit sigSelectAll();
        });
        connect(d->ui.deselectAllBtn, &QPushButton::clicked, this, [=] {
            emit sigDeselctAll();
        });
    }

    CopyWidget::~CopyWidget() {
    }
}
