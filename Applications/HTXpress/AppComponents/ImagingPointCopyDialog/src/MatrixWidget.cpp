﻿#include "MatrixWidget.h"
#include "ui_MatrixWidget.h"

namespace HTXpress::AppComponents::ImagingPointCopyDialog {
    struct MatrixWidget::Impl {
        explicit Impl(MatrixWidget* self) : self(self) {
            ui.setupUi(self);
        }

        MatrixWidget* self{};
        Ui::MatrixWidget ui{};
    };

    MatrixWidget::MatrixWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>(this)} {
        d->ui.selectAllBtn->setObjectName("bt-setup-light");
        d->ui.deselectAllBtn->setObjectName("bt-setup-light");
        for (const auto& label : findChildren<QLabel*>()) {
            if (label->objectName().contains("subTitle")) {
                label->setObjectName("label-h3");
            } else if (label->objectName().contains("subText")) {
                label->setObjectName("label-h5");
            }
        }
        connect(d->ui.selectAllBtn, &QPushButton::clicked, this, [this] {
            emit sigSelectAll();
        });

        connect(d->ui.deselectAllBtn, &QPushButton::clicked, this, [this] {
            emit sigDeselectAll();
        });

        connect(d->ui.colSpin, qOverload<int>(&QSpinBox::valueChanged), this, [this](int column) {
            emit sigCountChanged(d->ui.rowSpin->value(), column);
        });

        connect(d->ui.rowSpin, qOverload<int>(&QSpinBox::valueChanged), this, [this](int row) {
            emit sigCountChanged(row, d->ui.colSpin->value());
        });

        connect(d->ui.horSpin, qOverload<double>(&QDoubleSpinBox::valueChanged), this, [this](double hor) {
            emit sigSpacingChanged(hor, d->ui.verSpin->value());
        });

        connect(d->ui.verSpin, qOverload<double>(&QDoubleSpinBox::valueChanged), this, [this](double ver) {
            emit sigSpacingChanged(d->ui.horSpin->value(), ver);
        });
    }

    MatrixWidget::~MatrixWidget() {
    }

    auto MatrixWidget::SetRows(const int32_t& rows) -> void {
        d->ui.rowSpin->setValue(rows);
    }

    auto MatrixWidget::SetColumns(const int32_t& cols) -> void {
        d->ui.colSpin->setValue(cols);
    }

    auto MatrixWidget::SetMinHorSpacing(const double& horizontal) -> void {
        d->ui.horSpin->setMinimum(horizontal);
    }

    auto MatrixWidget::SetMinVerSpacing(const double& vertical) -> void {
        d->ui.verSpin->setMinimum(vertical);
    }

    auto MatrixWidget::GetCount() const -> std::pair<int32_t, int32_t> {
        return {d->ui.rowSpin->value(), d->ui.colSpin->value()};
    }

    auto MatrixWidget::GetSpacing() const -> std::pair<double, double> {
        return {d->ui.horSpin->value(), d->ui.verSpin->value()};
    }
}
