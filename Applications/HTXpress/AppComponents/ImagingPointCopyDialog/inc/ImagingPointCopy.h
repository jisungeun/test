﻿#pragma once
#include <memory>
#include <QObject>
#include <VesselMap.h>

#include "HTXImagingPointCopyDialogExport.h"
#include "ImagingPointCopyDefines.h"

namespace HTXpress::AppComponents::ImagingPointCopyDialog {
    class HTXImagingPointCopyDialog_API ImagingPointCopy final : public QObject {
        Q_OBJECT
    public:
        using Self = ImagingPointCopy;
        using Pointer = std::shared_ptr<Self>;

        explicit ImagingPointCopy(QObject* parent = nullptr);
        ~ImagingPointCopy() override;

        auto SetInputData(const Input::Pointer& input) -> void;
        auto GetOutputData() const -> Output::Pointer;
        auto Execute() -> int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
