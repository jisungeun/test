﻿#pragma once
#include <QMap>
#include <QColor>

#include <VesselMap.h>
#include <enum.h>

#include "HTXImagingPointCopyDialogExport.h"

namespace HTXpress::AppComponents::ImagingPointCopyDialog {
    BETTER_ENUM(Type, int32_t, Copy, Matrix);
    struct HTXImagingPointCopyDialog_API Location {
        TC::AcquisitionIndex index{};
        TC::AcquisitionType::_enumerated type{};
        struct {
            double x{};
            double y{};
            double z{};
        } centerPos;
        struct {
            double width{};
            double height{};
        } size;
    };

    using WellGroups = QMap<TC::GroupIndex, QPair<QColor, QList<TC::WellIndex>>>;
    using Locations = QList<Location>;

    struct HTXImagingPointCopyDialog_API Input {
        using Pointer = std::shared_ptr<HTXpress::AppComponents::ImagingPointCopyDialog::Input>;

        TC::WellIndex sourceWellIndex{};
        Locations targetPoints{};

        TC::VesselMap::Pointer vesselMap{};
        WellGroups wellGroups{};
    };

    struct HTXImagingPointCopyDialog_API Output {
        using Pointer = std::shared_ptr<HTXpress::AppComponents::ImagingPointCopyDialog::Output>;

        Type::_enumerated type{};
        TC::WellIndex sourceWellIndex{};
        QList<TC::AcquisitionIndex> targetPointIndices{};
        QList<TC::WellIndex> targetWells{};

        struct {
            int32_t row{1};
            int32_t col{1};
        } count;

        struct {
            double hor{};
            double ver{};
        } spacing;
    };

}
