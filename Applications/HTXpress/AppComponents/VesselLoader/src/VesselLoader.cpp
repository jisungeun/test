#define LOGGER_TAG "[Vessel Loader]"

#include <QDir>

#include <TCLogger.h>
#include <VesselRepo.h>

#include "VesselReader.h"
#include "VesselLoader.h"

namespace HTXpress::AppComponents::VesselLoader {
    auto Loader::Load(const QString& path) -> void {
        auto repo = AppEntity::VesselRepo::GetInstance();

        QDir dir(path);
        auto files = dir.entryList({"*.vessel"}, QDir::Filter::Files, QDir::SortFlag::Name);

        if(files.length() == 0) {
            QLOG_ERROR() << "No vessel files exist at " << path;
            return;
        }

        auto reader = VesselIO::VesselReader();
        for(auto file : files) {
            const auto vesselPath = QString("%1/%2").arg(path).arg(file);

            auto vessel = reader.Read(vesselPath);
            repo->AddVessel(vessel);
        }
    }
}