#pragma once
#include <memory>
#include <QStringList>

#include "HTXVesselLoaderExport.h"

namespace HTXpress::AppComponents::VesselLoader {
	class HTXVesselLoader_API Loader {
    public:
        static auto Load(const QString& path)->void;
    };
}