#pragma once
#include <QDialog>

#include <RoiSetupDefines.h>
#include <RoiSetupVessel.h>
#include <RegionOfInterest.h>

#include "HTXROISetupWindowExport.h"

namespace HTXpress::AppComponents::RoiSetupWindow {
    class HTXROISetupWindow_API RoiSetupWindow final : public QDialog {
        Q_OBJECT
    public:
        using Self = RoiSetupWindow;

        explicit RoiSetupWindow(const RoiSetupDefinitions::RoiSetupVessel::Pointer& vessel, QWidget* parent = nullptr);
        ~RoiSetupWindow() override;

        auto GetROIs() const -> RoiSetupDefinitions::ROIs;

    private:
        auto keyPressEvent(QKeyEvent* event) -> void override;

    private slots:
        void accept() override;
        void reject() override;

        void onViewSelectionChanged(const QList<RoiSetupDefinitions::ROIIndex>& selectedROIs);
        void onMouseScenePosChanged(const QPointF& position);
        void onViewDrawingCompleted();
        void onRoiNameVisibilityChanged(bool show);

        void onWellIndexChanged(const RoiSetupDefinitions::WellIndex& wellIndex);
        void onTableSelectionChanged(const QList<QPair<RoiSetupDefinitions::WellIndex, RoiSetupDefinitions::ROIIndex>>& indexPairs);
        void onUpdate(const QList<RoiSetupDefinitions::RegionOfInterest::Pointer>& rois);

        void onRoiNameChanged(const RoiSetupDefinitions::WellIndex& wellIndex, const RoiSetupDefinitions::ROIIndex& roiIndex, const QString& name);
        void onRoiPosChanged(const RoiSetupDefinitions::WellIndex& wellIndex, const RoiSetupDefinitions::ROIIndex& roiIndex, const double& x, const double& y);
        void onRoiSizeChanged(const RoiSetupDefinitions::WellIndex& wellIndex, const RoiSetupDefinitions::ROIIndex& roiIndex, const double& w, const double& h);

        void onSaveButtonClicked();

        void onPresetSelected(int index);
        void onPresetHovered(const QString& preset);
        void onPresetPopupHide();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
