﻿#pragma once
#include <QTableView>
#include <QStyledItemDelegate>

#include <RegionOfInterest.h>
#include <RoiSetupDefines.h>

namespace HTXpress::AppComponents::RoiSetupWindow {
    class TableView final : public QTableView {
        Q_OBJECT
    public:
        using Self = TableView;

        explicit TableView(QWidget* parent = nullptr);
        ~TableView() override;

        auto Init() -> void;
        auto SetCurrentWell(const RoiSetupDefinitions::WellIndex& wellIndex) -> void;
        auto SetPosXRange(const RoiSetupDefinitions::Range& range) -> void;
        auto SetPosYRange(const RoiSetupDefinitions::Range& range) -> void;
        auto SetWidthRange(const RoiSetupDefinitions::Range& range) -> void;
        auto SetHeightRange(const RoiSetupDefinitions::Range& range) -> void;
        auto SetWellPositionNames(const QMap<RoiSetupDefinitions::WellIndex, QString>& wellPosNames) -> void;
        auto Sort() -> void;
        auto SetItemDelegates() -> void;

        auto UpdateROIs(const RoiSetupDefinitions::ROIs& rois) -> void;

        auto SetSelectedRois(const RoiSetupDefinitions::WellIndex& wellIndex, const QList<RoiSetupDefinitions::ROIIndex>& roiIndices) -> void;
        auto Add(const RoiSetupDefinitions::ROIs& rois) -> void;

    private:
        auto contextMenuEvent(QContextMenuEvent* event) -> void override;

    signals:
        void sigSelectionChanged(const QList<QPair<RoiSetupDefinitions::WellIndex, RoiSetupDefinitions::ROIIndex>>& selected);
        void sigNameChanged(const RoiSetupDefinitions::WellIndex&, const RoiSetupDefinitions::ROIIndex&, const QString&);
        void sigPosChanged(const RoiSetupDefinitions::WellIndex&, const RoiSetupDefinitions::ROIIndex&, const double&, const double&);
        void sigSizeChanged(const RoiSetupDefinitions::WellIndex&, const RoiSetupDefinitions::ROIIndex&, const double&, const double&);
        void sigChangeWellIndex(const RoiSetupDefinitions::WellIndex&);
        void sigShowProperty();

    private slots:
        void onSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
        void onDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles);
        void onDoubleClicked(const QModelIndex& modelIndex);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
