﻿#include <QFont>

#include "TableModel.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::RoiSetupWindow {
    struct TableModel::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        Self* self{};
        TableROIs rois;

        WellIndex currentWellIndex{};
    };

    TableModel::TableModel(QObject* parent) : QAbstractTableModel(parent), d{std::make_unique<Impl>(this)} {
    }

    TableModel::~TableModel() = default;

    auto TableModel::columnCount(const QModelIndex& parent) const -> int {
        Q_UNUSED(parent)
        return ColumnHeader::_NumOfHeaders;
    }

    auto TableModel::rowCount(const QModelIndex& parent) const -> int {
        Q_UNUSED(parent)
        return d->rois.size();
    }

    auto TableModel::headerData(int section, Qt::Orientation orientation, int role) const -> QVariant {
        if (role == Qt::DisplayRole) {
            if (orientation == Qt::Horizontal) {
                const auto column = ColumnHeader::_from_integral(section);
                switch (column) {
                    case ColumnHeader::WellIndex: return "Well";
                    case ColumnHeader::WellPosName: return "Well";
                    case ColumnHeader::RoiIndex: return "Index";
                    case ColumnHeader::Name: return "Name";
                    case ColumnHeader::Shape: return "Shape";
                    case ColumnHeader::CenterX: return "X";
                    case ColumnHeader::CenterY: return "Y";
                    case ColumnHeader::Width: return "Width";
                    case ColumnHeader::Height: return "Height";
                    default: return {};
                }
            }
        }

        if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }

        return {};
    }

    auto TableModel::flags(const QModelIndex& index) const -> Qt::ItemFlags {
        if (!index.isValid()) {
            return Qt::ItemIsEnabled;
        }
        const auto column = ColumnHeader::_from_integral(index.column());
        const auto row = index.row();
        const auto& roi = d->rois.at(row);
        const auto wellIndex = roi.wellIndex;

        if(wellIndex == d->currentWellIndex) {
            switch (column) {
                case ColumnHeader::CenterX: [[fallthrough]];
                case ColumnHeader::CenterY: [[fallthrough]];
                case ColumnHeader::Height: [[fallthrough]];
                case ColumnHeader::Name: [[fallthrough]];
                case ColumnHeader::Width:
                    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
                default:
                    break;
            }
        }
        return QAbstractTableModel::flags(index);
    }

    auto TableModel::insertRows(int row, int count, const QModelIndex& parent) -> bool {
        Q_UNUSED(parent)
        beginInsertRows(QModelIndex(), row, row + count - 1);
        for (auto r = 0; r < count; ++r) {
            d->rois.insert(r, {});
        }
        endInsertRows();
        return true;
    }

    auto TableModel::removeRows(int row, int count, const QModelIndex& parent) -> bool {
        Q_UNUSED(parent)
        beginRemoveRows(QModelIndex(), row, row + count - 1);
        for (auto r = 0; r < count; ++r) {
            d->rois.removeAt(row);
        }
        endRemoveRows();
        return true;
    }

    auto TableModel::SetCurrentWellIndex(const WellIndex& wellIndex) -> void {
        d->currentWellIndex = wellIndex;
    }

    auto TableModel::GetCurrentWellIndex() const -> WellIndex {
        return d->currentWellIndex;
    }

    auto TableModel::data(const QModelIndex& index, int role) const -> QVariant {
        if (!index.isValid()) return {};
        if (index.row() >= d->rois.size() || index.row() < 0) return {};

        const auto row = index.row();
        const auto col = ColumnHeader::_from_integral(index.column());
        const auto& roi = d->rois.at(row);
        if (role == Qt::DisplayRole) {
            switch (col) {
                case ColumnHeader::WellIndex: return roi.wellIndex;
                case ColumnHeader::WellPosName: return roi.wellPosName;
                case ColumnHeader::RoiIndex: return roi.roiIndex;
                case ColumnHeader::Name: return roi.name;
                case ColumnHeader::Shape: return QString::fromLatin1(ItemShape::_from_integral(roi.shape)._to_string());
                case ColumnHeader::CenterX: return QString::number(roi.centerX, 'f', 3);
                case ColumnHeader::CenterY: return QString::number(roi.centerY, 'f', 3);
                case ColumnHeader::Width: return QString::number(roi.width, 'f', 3);
                case ColumnHeader::Height: return QString::number(roi.height, 'f', 3);
                default: return {};
            }
        }

        if (role == Qt::TextAlignmentRole) {
            return Qt::AlignCenter;
        }

        if (role == Qt::ToolTipRole) {
            if (col == +ColumnHeader::Name) {
                return roi.name;
            }
        }

        if (role == Qt::FontRole) {
            if (d->currentWellIndex == roi.wellIndex) {
                if (col == +ColumnHeader::WellPosName) {
                    QFont font;
                    font.setBold(true);
                    return font;
                }
            }
        }

        return {};
    }

    auto TableModel::setData(const QModelIndex& index, const QVariant& value, int role) -> bool {
        if (!index.isValid())
            return false;

        const auto row = index.row();
        const auto col = ColumnHeader::_from_integral(index.column());

        if (role == Qt::EditRole) {
            auto roi = d->rois.value(row);
            switch (col) {
                case ColumnHeader::WellIndex:
                    roi.wellIndex = value.toInt();
                    break;
                case ColumnHeader::WellPosName:
                    roi.wellPosName = value.toString();
                    break;
                case ColumnHeader::RoiIndex:
                    roi.roiIndex = value.toInt();
                    break;
                case ColumnHeader::Name:
                    roi.name = value.toString();
                    break;
                case ColumnHeader::Shape:
                    roi.shape = ItemShape::_from_integral(value.toInt());
                    break;
                case ColumnHeader::CenterX:
                    roi.centerX = value.toDouble();
                    break;
                case ColumnHeader::CenterY:
                    roi.centerY = value.toDouble();
                    break;
                case ColumnHeader::Width:
                    roi.width = value.toDouble();
                    break;
                case ColumnHeader::Height:
                    roi.height = value.toDouble();
                    break;
                default:
                    break;
            }
            d->rois.replace(row, roi);
            emit dataChanged(index, index, {Qt::DisplayRole, Qt::EditRole});
            return true;
        }

        return false;
    }

    auto TableModel::GetRois() const -> TableROIs {
        return d->rois;
    }

    auto TableModel::GetIndexPair(const int32_t& row) const -> QPair<WellIndex, ROIIndex> {
        return {d->rois.at(row).wellIndex, d->rois.at(row).roiIndex};
    }

    ProxyModel::ProxyModel(QObject* parent) : QSortFilterProxyModel(parent) {
    }

    QVariant ProxyModel::headerData(int section, Qt::Orientation orientation, int role) const {
        if (role == Qt::DisplayRole) {
            if (orientation == Qt::Vertical) {
                return section + 1;
            }
            return QSortFilterProxyModel::headerData(section, orientation, role);
        }
        return QSortFilterProxyModel::headerData(section, orientation, role);
    }
}
