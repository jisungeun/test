﻿#pragma once
#include <QGraphicsRectItem>
#include <QObject>

#include <RoiSetupDefines.h>

namespace HTXpress::AppComponents::RoiSetupWindow {
    class GraphicsRoiItem : public QObject, public QGraphicsRectItem {
        Q_OBJECT
    public:
        using Self = GraphicsRoiItem;

        enum { Type = UserType + 1 };

        enum ResizeHandle {
            NoHandle,
            TopLeft,
            TopRight,
            BottomLeft,
            BottomRight,
            Top,
            Bottom,
            Left,
            Right
        };

        GraphicsRoiItem(const int32_t& index, const QString& name, const RoiSetupDefinitions::ItemShape& shape, const QRectF& rect, QGraphicsItem* parent = nullptr);
        auto Clone() const -> GraphicsRoiItem*;
        ~GraphicsRoiItem() override;

        auto type() const -> int override;
        auto shape() const -> QPainterPath override;

        auto SetIndex(const int32_t& index) -> void;
        auto GetIndex() const -> int32_t;

        auto SetNameVisible(bool show = true) -> void;
        auto SetName(const QString& name) -> void;
        auto GetName() const -> QString;

        auto SetShape(const RoiSetupDefinitions::ItemShape& shape) -> void;
        auto GetShape() const -> RoiSetupDefinitions::ItemShape;

        auto itemChange(GraphicsItemChange change, const QVariant& value) -> QVariant override;
        static auto GetLastSelectedItemIndex() -> RoiSetupDefinitions::ROIIndex;

    signals:
        void sigSizeChanged(const QRectF& oldRect);
        void sigNameChanged(const QString& oldName);

    protected:
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
        auto mouseMoveEvent(QGraphicsSceneMouseEvent* event) -> void override;
        auto mousePressEvent(QGraphicsSceneMouseEvent* event) -> void override;
        auto mouseReleaseEvent(QGraphicsSceneMouseEvent* event) -> void override;
        auto hoverMoveEvent(QGraphicsSceneHoverEvent* event) -> void override;
        auto mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class GraphicsTempRoiItem : public GraphicsRoiItem {
        Q_OBJECT
    public:
        enum { Type = UserType + 3 };

        GraphicsTempRoiItem(const int32_t& index, const QString& name, const RoiSetupDefinitions::ItemShape& shape, const QRectF& rect, QGraphicsItem* parent = nullptr);
        auto type() const -> int override;

    private:
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
    };
}
