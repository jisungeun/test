﻿#pragma once
#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include <QList>

#include <enum.h>

#include <RoiSetupDefines.h>

namespace HTXpress::AppComponents::RoiSetupWindow {
    BETTER_ENUM(ColumnHeader, int32_t, WellIndex, WellPosName, RoiIndex, Name, Shape, CenterX, CenterY, Width, Height, _NumOfHeaders);

    class TableModel final : public QAbstractTableModel {
        Q_OBJECT
    public:
        using Self = TableModel;

        struct TableROI {
            TableROI() = default;
            TableROI(RoiSetupDefinitions::WellIndex wellIndex, RoiSetupDefinitions::ROIIndex roiIndex)
            : wellIndex(wellIndex), roiIndex(roiIndex) {
            }

            TableROI(const TableROI& other) = default;
            auto operator=(const TableROI& other) -> TableROI& = default;

            RoiSetupDefinitions::WellIndex wellIndex{};
            QString wellPosName{};
            RoiSetupDefinitions::ROIIndex roiIndex{};
            QString name{};
            RoiSetupDefinitions::ItemShape::_enumerated shape{};
            double centerX{}; // mm
            double centerY{}; // mm
            double width{}; // mm
            double height{}; // mm

            auto operator==(const TableROI& other) const -> bool {
                return wellIndex == other.wellIndex && roiIndex == other.roiIndex;
            }
        };

        using TableROIs = QList<TableROI>;

        explicit TableModel(QObject* parent = nullptr);
        ~TableModel() override;

        auto rowCount(const QModelIndex& parent = QModelIndex()) const -> int override;
        auto columnCount(const QModelIndex& parent = QModelIndex()) const -> int override;
        auto data(const QModelIndex& index, int role) const -> QVariant override;
        auto headerData(int section, Qt::Orientation orientation, int role) const -> QVariant override;
        auto flags(const QModelIndex& index) const -> Qt::ItemFlags override;
        auto setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) -> bool override;
        auto insertRows(int row, int count, const QModelIndex& parent = QModelIndex()) -> bool override;
        auto removeRows(int row, int count, const QModelIndex& parent = QModelIndex()) -> bool override;

        auto SetCurrentWellIndex(const RoiSetupDefinitions::WellIndex& wellIndex) -> void;
        auto GetCurrentWellIndex() const ->RoiSetupDefinitions::WellIndex;

        auto GetRois() const -> TableROIs;
        auto GetIndexPair(const int32_t& row) const -> QPair<RoiSetupDefinitions::WellIndex, RoiSetupDefinitions::ROIIndex>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class ProxyModel final : public QSortFilterProxyModel {
        Q_OBJECT
    public:
        explicit ProxyModel(QObject* parent = nullptr);
        auto headerData(int section, Qt::Orientation orientation, int role) const -> QVariant override;
    };
}
