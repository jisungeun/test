﻿#pragma once
#include <QScrollBar>
#include <QPainter>
#include <QPaintEvent>

namespace HTXpress::AppComponents::RoiSetupWindow {
    constexpr int32_t RulerSize{16};

    class Ruler final : public QWidget {
        Q_OBJECT
    public:
        using Self = Ruler;
        Ruler(Qt::Orientation direction, QGraphicsView* view, QWidget* parent = nullptr);
        ~Ruler() override;

        auto UpdateCursorPos(const QPoint& viewPos) -> void;
        auto UpdateSelectedRoiPos(const QPair<QPoint, QPoint>& pos) -> void;
        auto SetScenePosOffset(const double& offset) -> void;
        
    private:
        auto paintEvent(QPaintEvent* event) -> void override;

        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class RulerLeftTopWidget final : public QWidget {
        Q_OBJECT
    public:
        explicit RulerLeftTopWidget(QWidget* parent);

    protected:
        auto paintEvent(QPaintEvent*) -> void override;
    };
}
