﻿#include "PropertyDialogControl.h"
#include "RoiSetupHelper.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::RoiSetupWindow {
    struct PropertyDialogControl::Impl {
        EditView* preview{};
        GraphicsRoiItem* tempItem{};
        GraphicsRoiItem* compareItem{};
        auto WellPosition() const -> Position;
    };

    auto PropertyDialogControl::Impl::WellPosition() const -> Position {
        return RoiSetupHelper::GetWellPosFromScenePos(tempItem->mapToScene(tempItem->rect().center()));
    }

    PropertyDialogControl::PropertyDialogControl() : d{std::make_unique<Impl>()} {
    }

    PropertyDialogControl::~PropertyDialogControl() = default;

    auto PropertyDialogControl::InitPreview(GraphicsRoiItem* item, EditView* view) -> void {
        d->preview = view;
        d->tempItem = item;
        d->compareItem = item->Clone();
        d->tempItem->SetNameVisible(true);
        d->preview->scene()->addItem(d->tempItem);
    }

    auto PropertyDialogControl::GetViewPos() const -> QPoint {
        return d->preview->mapToGlobal(d->preview->geometry().topRight());
    }

    auto PropertyDialogControl::RemoveTempItem() -> void {
        d->preview->scene()->removeItem(d->tempItem);
    }

    auto PropertyDialogControl::GetIndex() const -> QString {
        return QString::number(d->tempItem->GetIndex());
    }

    auto PropertyDialogControl::GetShape() const -> QString {
        return QString::fromLatin1(ItemShape::_from_integral(d->tempItem->GetShape())._to_string());
    }

    auto PropertyDialogControl::GetName() const -> QString {
        return d->tempItem->GetName();
    }

    auto PropertyDialogControl::GetItemRect() const -> QRectF {
        return d->tempItem->rect();
    }

    auto PropertyDialogControl::GetItemPos() const -> QPointF {
        return d->tempItem->pos();
    }

    auto PropertyDialogControl::GetWellPosX() const -> double {
        return d->WellPosition().x;
    }

    auto PropertyDialogControl::GetWellPosY() const -> double {
        return d->WellPosition().y;
    }

    auto PropertyDialogControl::GetWidth() const -> double {
        return d->tempItem->rect().width();
    }

    auto PropertyDialogControl::GetHeight() const -> double {
        return d->tempItem->rect().height();
    }

    auto PropertyDialogControl::ChangeName(const QString& name) -> void {
        d->tempItem->SetName(name);
    }

    auto PropertyDialogControl::GetPosXRange() const -> QPair<double, double> {
        return {d->preview->sceneRect().left(), d->preview->sceneRect().right()};
    }

    auto PropertyDialogControl::GetPosYRange() const -> QPair<double, double> {
        return {d->preview->sceneRect().top(), d->preview->sceneRect().bottom()};
    }

    auto PropertyDialogControl::GetWidthRange() const -> QPair<double, double> {
        return {0, d->preview->sceneRect().width()};
    }

    auto PropertyDialogControl::GetHeightRange() const -> QPair<double, double> {
        return {0, d->preview->sceneRect().height()};
    }

    auto PropertyDialogControl::MoveItemX(const double& wellPosX) -> void {
        const auto dX = wellPosX - d->WellPosition().x;
        const auto posX = d->tempItem->pos().x();
        d->tempItem->setX(posX + dX);
    }

    auto PropertyDialogControl::MoveItemY(const double& wellPosY) -> void {
        const auto dY = -(wellPosY - d->WellPosition().y);
        const auto posY = d->tempItem->pos().y();
        d->tempItem->setY(posY + dY);
    }

    auto PropertyDialogControl::ChangeWidth(const double& w) -> void {
        auto rect = d->tempItem->rect();
        const auto dW = w - rect.width();
        rect.setLeft(rect.left() - dW / 2);
        rect.setRight(rect.right() + dW / 2);
        rect.setWidth(w);
        d->tempItem->setRect(rect);
    }

    auto PropertyDialogControl::ChangeHeight(const double& h) -> void {
        auto rect = d->tempItem->rect();
        const auto dH = h - rect.height();
        rect.setTop(rect.top() - dH / 2);
        rect.setBottom(rect.bottom() + dH / 2);
        d->tempItem->setRect(rect);
    }

    auto PropertyDialogControl::IsChanged() const -> bool {
        if (d->tempItem->GetName() != d->compareItem->GetName()) { return true; }
        if (d->tempItem->pos() != d->compareItem->pos()) { return true; }
        if (d->tempItem->rect() != d->compareItem->rect()) { return true; }

        return false;
    }
}
