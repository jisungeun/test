﻿#include <QApplication>
#include <QMenu>
#include <QRandomGenerator>

#include "EditView.h"
#include "Commands.h"
#include "EditScene.h"
#include "GraphicsGridItem.h"
#include "GraphicsRoiItem.h"
#include "RoiSetupHelper.h"
#include "Ruler.h"
#include "PropertyDialog.h"
#include "ZoomWidget.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::RoiSetupWindow {
    struct EditView::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        struct CopyValue {
            QRectF rect{};
            ItemShape::_enumerated shape{};
            QPointF pos{};
        };

        Self* self{};

        bool ready{};
        bool roiNameVisible{};
        ItemShape::_enumerated selectedShape{};

        QPointF cursorPos{};
        QPoint mouseMovePos{};
        QUndoStack* undoStack{};

        EditScene* scene{};
        double defaultRoiSize{};
        QList<CopyValue> copyTargtes{};

        QMenu menu{};
        QList<QAction*> menuItems{};

        Ruler* rulerH{};
        Ruler* rulerV{};
        RulerLeftTopWidget* rulerLTop{};
        ZoomWidget* zoomWidget{};

        double zoomFitM11Value{};

        auto SetDefaultSetting(const ItemShape& shape, const QRectF& rect) -> void;

        auto ConvertShape(const EditViewRequest& request) const -> ItemShape;
        auto ReadyToDrawRoiItem() -> void;

        [[nodiscard]] auto CreateItem(const ItemShape& shape, const QPointF& initPos, bool addUndo = true) -> GraphicsRoiItem*;
        [[nodiscard]] auto CreateItem(const ItemShape& shape, const QRectF& rect, const QPointF& initPos, bool addUndo = true) -> GraphicsRoiItem*;
        [[nodiscard]] auto CreateItem(const int32_t& index, const QString& name, const ItemShape& shape, const QRectF& rect, const QPointF& initPos, bool addUndo = true, bool presetUndo = false) -> GraphicsRoiItem*;

        auto InitContextMenu() -> void;
        auto ShowContextMenu(const QPoint& pos) -> void;
        auto SetPointerMode() -> void;

        auto GetNewIndex() const -> int32_t;
        auto GetDefaultRect() const -> QRectF;
        auto GetDefaultName(const ROIIndex& roiIndex = -1) const -> QString;
        auto GetPastedPos(const QRectF& rect, const QPointF& pos) const -> QPointF;

        auto InitRuler() -> void;
        auto IsDrawableArea(const QRectF& rect) const -> bool;
        auto IsDrawableArea() const -> bool;

        auto UpdateRois() const -> void;

        auto BeginPropertyEdit(const ROIIndex& target) -> void;
        auto EndPropertyEdit(const ROIIndex& target) -> void;

        auto GetRoiOutlinePos() const -> QPair<QPoint, QPoint>;
        auto UpdateRulerPosIndicator() const -> void;

        auto InitZoomWidget() -> void;
    };

    EditView::EditView(QWidget* parent) : QGraphicsView(parent), d{std::make_unique<Impl>(this)} {
        setMouseTracking(true);
        setDragMode(RubberBandDrag);
        setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
        setTransformationAnchor(AnchorUnderMouse);
        setResizeAnchor(AnchorViewCenter);
        setViewportUpdateMode(FullViewportUpdate);
        setOptimizationFlag(DontAdjustForAntialiasing);

        d->scene = new EditScene(this);
        setScene(d->scene);
        connect(d->scene, &EditScene::sigItemsMoved, this, &Self::onItemMoved);
        connect(d->scene, &EditScene::selectionChanged, this, &Self::onSelectionChanged);

        d->undoStack = new QUndoStack(this);
        d->undoStack->setUndoLimit(50);

        d->InitContextMenu();
        d->InitRuler();
        d->InitZoomWidget();
    }

    EditView::~EditView() = default;

    auto EditView::ClearAll() -> void {
        d->undoStack->clear();
        d->scene->clearSelection();
        d->scene->clear();
    }

    auto EditView::SetWellSize(const ItemShape& shape, const QRectF& rect) -> void {
        assert(d->scene && "scene is null, cannot continue!");
        setSceneRect(rect);

        QPen pen;
        pen.setColor(Qt::red);
        pen.setWidthF(0.1);

        const auto gridItem = new GraphicsGridItem(shape, rect);
        d->scene->addItem(gridItem);

        if (shape == +ItemShape::Ellipse) {
            d->scene->addEllipse(rect, pen);
        } else {
            d->scene->addRect(rect, pen);
        }

        d->rulerH->SetScenePosOffset(rect.width() / 2);
        d->rulerV->SetScenePosOffset(rect.height() / 2);
    }

    auto EditView::SetImagingAreaSize(const ItemShape& shape, const QRectF& rect) -> void {
        assert(d->scene && "scene is null, cannot continue!");
        d->SetDefaultSetting(shape, rect);

        QPen pen;
        pen.setColor(Qt::yellow);
        pen.setWidthF(0.1);

        if (shape == +ItemShape::Ellipse) {
            d->scene->addEllipse(rect, pen);
        } else {
            d->scene->addRect(rect, pen);
        }
    }

    auto EditView::SetRoiItems(const QList<RegionOfInterest::Pointer>& rois) -> void {
        for (const auto& roi : rois) {
            const auto index = roi->GetIndex();
            const auto size = roi->GetSize();
            const auto pos = roi->GetPosition();
            const auto name = roi->GetName();
            const auto shape = roi->GetShape();
            const auto item = d->CreateItem(index, name, shape, {-size.w / 2, -size.h / 2, size.w, size.h}, {pos.x, -pos.y}, false);
            Q_UNUSED(item)
        }
        d->UpdateRois();
    }

    auto EditView::SetRoiNameVisible(bool visible) -> void {
        d->roiNameVisible = visible;
        for (const auto& item : d->scene->items()) {
            if (item->type() == GraphicsRoiItem::Type) {
                const auto roi = qgraphicsitem_cast<GraphicsRoiItem*>(item);
                roi->SetNameVisible(visible);
            }
        }
    }

    auto EditView::SelectRoiItems(const QList<ROIIndex>& roiIndices) -> void {
        d->scene->clearSelection();
        for (const auto& roiIndex : roiIndices) {
            for (const auto& item : d->scene->items()) {
                if (item->type() == GraphicsRoiItem::Type) {
                    const auto roiItem = qgraphicsitem_cast<GraphicsRoiItem*>(item);
                    if (roiItem->GetIndex() == roiIndex) {
                        roiItem->setSelected(true);
                        break;
                    }
                }
            }
        }
    }

    auto EditView::ChangeRoiName(const ROIIndex& index, const QString& newName) -> void {
        if (const auto roiItem = d->scene->GetRoiItem(index)) {
            const auto oldName = roiItem->GetName();
            roiItem->SetName(newName);
            d->undoStack->push(new CommandNameChange(roiItem, oldName));
        }
    }

    auto EditView::ChangeRoiPosition(const ROIIndex& index, const double& x, const double& y) -> void {
        if (const auto roiItem = d->scene->GetRoiItem(index)) {
            const auto oldPos = roiItem->pos();

            const auto pos = roiItem->pos();
            const auto wellPos = RoiSetupHelper::GetWellPosFromScenePos(roiItem->mapToScene(roiItem->rect().center()));
            const auto dx = x - wellPos.x;
            const auto dy = -(y - wellPos.y);
            roiItem->setPos(pos.x() + dx, pos.y() + dy);

            QMap<GraphicsRoiItem*, QPointF> map;
            map[roiItem] = oldPos;
            d->undoStack->push(new CommandMove(d->scene, map));
            d->UpdateRulerPosIndicator();
        }
    }

    auto EditView::ChangeRoiSize(const ROIIndex& index, const double& w, const double& h) -> void {
        if (const auto roiItem = d->scene->GetRoiItem(index)) {
            const auto oldRect = roiItem->rect();

            auto rect = roiItem->rect();
            const auto dw = w - rect.width();
            const auto dh = h - rect.height();
            rect.setTopLeft({rect.left() - dw / 2, rect.top() - dh / 2});
            rect.setBottomRight({rect.right() + dw / 2, rect.bottom() + dh / 2});
            roiItem->setRect(rect);

            d->undoStack->push(new CommandResize(roiItem, oldRect));
            d->UpdateRulerPosIndicator();
        }
    }

    auto EditView::SetTempRoiItemsFromPreset(const QList<RegionOfInterest::Pointer>& presetInfo) -> void {
        for (const auto& tempRoi : presetInfo) {
            const auto index = tempRoi->GetIndex();
            const auto size = tempRoi->GetSize();
            const auto pos = tempRoi->GetPosition();
            const auto name = tempRoi->GetName();
            const auto shape = tempRoi->GetShape();
            const auto rect = QRectF{-size.w / 2, -size.h / 2, size.w, size.h};
            const auto initPos = QPointF{pos.x, -pos.y};
            const auto item = new GraphicsTempRoiItem(index, name, shape, rect);
            item->SetNameVisible(false);
            item->setPos(initPos);
            d->scene->addItem(item);
        }
    }

    auto EditView::DeleteTempRoiItems() -> void {
        for (const auto& item : d->scene->items()) {
            if (item->type() == GraphicsTempRoiItem::Type) {
                d->scene->removeItem(item);
            }
        }
    }

    auto EditView::ApplyRoiPreset(const QList<RegionOfInterest::Pointer>& rois) -> void {
        const bool dontUseAddUndoCommandFlag = false;
        const bool usePresetUndoCommandFlag = true;
        const auto prevItems = d->scene->GetAllRoiItems();

        QList<GraphicsRoiItem*> currItems;
        for (const auto& roi : rois) {
            const auto index = roi->GetIndex();
            const auto size = roi->GetSize();
            const auto pos = roi->GetPosition();
            const auto name = roi->GetName();
            const auto shape = roi->GetShape();
            const auto item = d->CreateItem(index, name, shape, {-size.w / 2, -size.h / 2, size.w, size.h}, {pos.x, -pos.y}, dontUseAddUndoCommandFlag, usePresetUndoCommandFlag);
            currItems.push_back(item);
        }

        d->undoStack->push(new CommandApplyPreset(d->scene, prevItems, currItems));
        d->UpdateRois();
    }

    auto EditView::HideCurrentRoiItems() -> void {
        for (const auto& item : d->scene->GetAllRoiItems()) {
            item->hide();
        }
    }

    auto EditView::ShowCurrentRoiItems() -> void {
        for (const auto& item : d->scene->GetAllRoiItems()) {
            item->show();
        }
    }

    auto EditView::onReadyToDrawRoiItem(int32_t shapeIndex) -> void {
        const auto shape = EditViewRequest::_from_integral(shapeIndex);
        d->selectedShape = d->ConvertShape(shape);

        switch (shape) {
            case EditViewRequest::DrawEllipse: [[fallthrough]];
            case EditViewRequest::DrawRectangle: {
                d->ReadyToDrawRoiItem();
                break;
            }
            case EditViewRequest::Pointer: {
                d->SetPointerMode();
                break;
            }
            default:
                break;
        }
    }

    void EditView::onUndo() {
        d->undoStack->undo();
        d->UpdateRois();
        SetRoiNameVisible(d->roiNameVisible);
    }

    void EditView::onRedo() {
        d->undoStack->redo();
        d->UpdateRois();
        SetRoiNameVisible(d->roiNameVisible);
    }

    void EditView::onItemMoved(const QMap<GraphicsRoiItem*, QPointF>& movedItems) {
        d->undoStack->push(new CommandMove(d->scene, movedItems));
        d->UpdateRois();
    }

    void EditView::onItemResized(const QRectF& oldRect) {
        const auto roiItem = qobject_cast<GraphicsRoiItem*>(sender());
        d->undoStack->push(new CommandResize(roiItem, oldRect));
        d->UpdateRois();
    }

    void EditView::onItemNameChanged(const QString& oldName) {
        const auto roiItem = qobject_cast<GraphicsRoiItem*>(sender());
        d->undoStack->push(new CommandNameChange(roiItem, oldName));
        d->UpdateRois();
    }

    void EditView::onDeleteSelectedRoiItems() {
        if (d->scene->selectedItems().isEmpty()) {
            return;
        }

        d->undoStack->push(new CommandDelete(d->scene, d->scene->selectedItems()));
        d->UpdateRois();
    }

    void EditView::onDeleteAll() {
        for (const auto& item : d->scene->items()) {
            if (item->type() == GraphicsRoiItem::Type) {
                item->setSelected(true);
            }
        }
        onDeleteSelectedRoiItems();
    }

    void EditView::onCopy() {
        d->copyTargtes.clear();
        const auto oldCursor = cursor().shape();
        setCursor(Qt::BusyCursor);
        for (const auto& roi : d->scene->GetSelectedRoiItems()) {
            Impl::CopyValue copyValue;
            copyValue.shape = roi->GetShape();
            copyValue.rect = roi->rect();
            copyValue.pos = roi->pos();
            d->copyTargtes.push_back(copyValue);
        }
        setCursor(oldCursor);
    }

    void EditView::onCut() {
        onCopy();
        onDeleteSelectedRoiItems();
    }

    void EditView::onPaste() {
        QList<GraphicsRoiItem*> pasted;
        for (const auto& roi : d->copyTargtes) {
            const auto shape = roi.shape;
            const auto rect = roi.rect;
            const auto pos = d->GetPastedPos(roi.rect, roi.pos);
            const auto item = d->CreateItem(shape, rect, pos);
            item->setSelected(true);
            pasted.push_back(item);
        }

        for (const auto& item : pasted) {
            item->setSelected(true);
        }

        d->UpdateRois();
    }

    void EditView::onSelectAll() {
        for (const auto& item : d->scene->items()) {
            if (item->type() == GraphicsRoiItem::Type) {
                item->setSelected(true);
            }
        }
    }

    void EditView::onDeselectAll() {
        for (const auto& item : d->scene->items()) {
            if (item->type() == GraphicsRoiItem::Type) {
                item->setSelected(false);
            }
        }
    }

    void EditView::onZoom(int32_t zoom) {
        const auto ratio = transform().m11() / d->zoomFitM11Value;
        switch (zoom) {
            case ZoomIn: {
                if (ratio < 8) {
                    scale(1.125, 1.125);
                    d->UpdateRulerPosIndicator();
                }
            }
            break;
            case ZoomOut: {
                if (ratio > 0.3) {
                    scale(1 / 1.125, 1 / 1.125);
                    d->UpdateRulerPosIndicator();
                }
            }
            break;
            case ZoomFit: {
                fitInView(sceneRect(), Qt::KeepAspectRatio);
                d->zoomFitM11Value = transform().m11();
                d->UpdateRulerPosIndicator();
            }
            break;

            default: ;
        }
    }

    void EditView::onShowProperty() {
        // TODO property dialog here
        if (d->scene->GetSelectedRoiItems().isEmpty()) {
            return;
        }
        const auto lastItemIndex = GraphicsRoiItem::GetLastSelectedItemIndex();
        const auto lastItem = d->scene->GetRoiItem(lastItemIndex);
        if (lastItem) {
            const auto idx = lastItem->GetIndex();
            const auto shape = lastItem->GetShape();
            const auto oldName = lastItem->GetName();
            const auto oldPos = lastItem->pos();
            const auto oldRect = lastItem->rect();

            d->BeginPropertyEdit(idx);
            const auto dlg = new PropertyDialog(lastItem->Clone(), this);
            if (dlg->exec()) {
                const auto [name, rect, pos] = dlg->GetProperty();
                lastItem->SetName(name);
                lastItem->setRect(rect);
                lastItem->setPos(pos);
                d->undoStack->push(new CommandPropertyUpdate(lastItem, oldName, oldPos, oldRect));
                d->UpdateRois();
            }
            d->EndPropertyEdit(idx);
            dlg->deleteLater();
        }
    }

    void EditView::onSelectionChanged() {
        QList<ROIIndex> selected;
        for (const auto& roiItem : d->scene->GetSelectedRoiItems()) {
            selected.push_back(roiItem->GetIndex());
        }

        d->UpdateRulerPosIndicator();
        emit sigSelectionChanged(selected);
    }

    auto EditView::onAddActionTriggered(QAction* action) -> void {
        if (!action) {
            return;
        }

        bool ok = false;
        const auto shape = ItemShape::_from_integral(action->data().toInt(&ok));
        if (ok) {
            const auto item = d->CreateItem(shape, mapToScene(d->mouseMovePos));
            item->setSelected(true);
            d->UpdateRois();
        }
    }

    auto EditView::mousePressEvent(QMouseEvent* event) -> void {
        if (event->button() == Qt::LeftButton) {
            QGraphicsView::mousePressEvent(event);
        }
    }

    auto EditView::mouseReleaseEvent(QMouseEvent* event) -> void {
        if (event->button() == Qt::LeftButton) {
            if (d->ready) {
                const auto item = d->CreateItem(d->selectedShape, d->cursorPos);
                d->ready = false;
                item->setSelected(true);
                d->UpdateRois();

                emit sigDrawCompleted();
                setCursor(Qt::ArrowCursor);
            }
            QGraphicsView::mouseReleaseEvent(event);
        }
    }

    auto EditView::mouseMoveEvent(QMouseEvent* event) -> void {
        if (d->ready) {
            d->cursorPos = mapToScene(event->pos());
            if (d->IsDrawableArea()) {
                setCursor(Qt::CrossCursor);
            } else {
                setCursor(Qt::ForbiddenCursor);
            }
            viewport()->update();
        }

        d->mouseMovePos = event->pos();
        d->UpdateRulerPosIndicator();

        emit sigUpdateScenePos(mapToScene(event->pos()));
        QGraphicsView::mouseMoveEvent(event);
    }

    void EditView::mouseDoubleClickEvent(QMouseEvent* event) {
        if (event->button() == Qt::RightButton) {
            onZoom(ZoomFit);
        }
        QGraphicsView::mouseDoubleClickEvent(event);
    }

    void EditView::wheelEvent(QWheelEvent* event) {
        if (QApplication::keyboardModifiers() == Qt::ControlModifier) {
            if (event->angleDelta().y() > 0) {
                onZoom(ZoomIn);
            } else if (event->angleDelta().y() < 0) {
                onZoom(ZoomOut);
            }
            return;
        }

        QGraphicsView::wheelEvent(event);
    }

    void EditView::contextMenuEvent(QContextMenuEvent* event) {
        if (event->modifiers() == Qt::ControlModifier) {
            return;
        }

        QList<QGraphicsItem*> selectedRoiItems;
        for (const auto& item : d->scene->selectedItems()) {
            if (item->type() == GraphicsRoiItem::Type) {
                selectedRoiItems.push_back(item);
            }
        }

        if (selectedRoiItems.size() < 2) {
            d->scene->clearSelection();
        }

        const auto item = itemAt(event->pos());
        if (item != nullptr && item->type() == GraphicsRoiItem::Type) {
            item->setSelected(true);
        }

        d->ShowContextMenu(event->globalPos());
        QGraphicsView::contextMenuEvent(event);
    }

    void EditView::resizeEvent(QResizeEvent* event) {
        QGraphicsView::resizeEvent(event);

        this->setViewportMargins(RulerSize - 1, RulerSize - 1, 0, 0);
        d->rulerH->resize(this->size().width() - RulerSize - 1, RulerSize);
        d->rulerH->move(RulerSize, 0);
        d->rulerV->resize(RulerSize, this->size().height() - RulerSize - 1);
        d->rulerV->move(0, RulerSize);
        d->rulerLTop->resize(RulerSize, RulerSize);
        d->rulerLTop->move(0, 0);
        d->UpdateRulerPosIndicator();

        d->zoomWidget->UpdateGeometry(this->size());
    }

    void EditView::scrollContentsBy(int dx, int dy) {
        QGraphicsView::scrollContentsBy(dx, dy);
        d->UpdateRulerPosIndicator();
    }

    void EditView::drawForeground(QPainter* painter, const QRectF& rect) {
        Q_UNUSED(rect)
        if (d->ready) {
            QPen pen = painter->pen();
            pen.setWidthF(0.1);
            pen.setStyle(Qt::DotLine);

            QBrush brush = painter->brush();

            auto r = d->GetDefaultRect();
            r.moveCenter(d->cursorPos);

            if (!d->IsDrawableArea(r)) {
                pen.setColor(Qt::red);
                brush.setStyle(Qt::SolidPattern);
                brush.setColor({255, 0, 0, 55});
            } else {
                pen.setColor(Qt::white);
            }

            painter->setPen(pen);
            painter->setBrush(brush);
            if (d->selectedShape == ItemShape::Rectangle) {
                painter->drawRect(r);
            } else if (d->selectedShape == ItemShape::Ellipse) {
                painter->drawEllipse(r);
            }
        }
    }

    void EditView::showEvent(QShowEvent* event) {
        onZoom(ZoomFit);
        QGraphicsView::showEvent(event);
    }

    auto EditView::Impl::SetDefaultSetting(const ItemShape& shape, const QRectF& rect) -> void {
        RoiSetupHelper::GetInstance()->SetDrawableArea({shape, rect});
        const auto minSize = rect.width() < rect.height() ? rect.width() : rect.height();
        defaultRoiSize = minSize * 0.3;
    }

    auto EditView::Impl::ConvertShape(const EditViewRequest& request) const -> ItemShape {
        switch (request) {
            case EditViewRequest::DrawEllipse: {
                return ItemShape::Ellipse;
            }
            case EditViewRequest::DrawRectangle: {
                return ItemShape::Rectangle;
            }
            case EditViewRequest::Pointer: {
                return ItemShape::None;
            }
            default:
                return ItemShape::None;
        }
    }

    auto EditView::Impl::ReadyToDrawRoiItem() -> void {
        ready = true;
    }

    auto EditView::Impl::CreateItem(const ItemShape& shape, const QPointF& initPos, bool addUndo) -> GraphicsRoiItem* {
        const auto index = GetNewIndex();
        const auto name = GetDefaultName(index);
        const auto rect = GetDefaultRect();
        return CreateItem(index, name, shape, rect, initPos, addUndo);
    }

    auto EditView::Impl::CreateItem(const ItemShape& shape, const QRectF& rect, const QPointF& initPos, bool addUndo) -> GraphicsRoiItem* {
        const auto index = GetNewIndex();
        const auto name = GetDefaultName(index);
        return CreateItem(index, name, shape, rect, initPos, addUndo);
    }

    auto EditView::Impl::CreateItem(const int32_t& index, const QString& name, const ItemShape& shape, const QRectF& rect, const QPointF& initPos, bool addUndo, bool presetUndo) -> GraphicsRoiItem* {
        const auto item = new GraphicsRoiItem(index, name, shape, rect);
        item->SetNameVisible(roiNameVisible);
        item->setPos(initPos);

        connect(item, &GraphicsRoiItem::sigSizeChanged, self, &Self::onItemResized);
        connect(item, &GraphicsRoiItem::sigNameChanged, self, &Self::onItemNameChanged);

        if (!presetUndo) {
            if (addUndo) {
                undoStack->push(new CommandAdd(scene, item));
            } else {
                scene->addItem(item);
            }
        }

        return item;
    }

    auto EditView::Impl::InitContextMenu() -> void {
        const auto addMenu = menu.addMenu("&Add");
        const auto addRect = addMenu->addAction("&Rectangle");
        addRect->setData(ItemShape::Rectangle);
        const auto addEllipse = addMenu->addAction("&Ellipse");
        addEllipse->setData(ItemShape::Ellipse);
        menu.addSeparator();
        const auto del = menu.addAction("Delete");
        menu.addSeparator();
        const auto copy = menu.addAction("Copy");
        const auto cut = menu.addAction("Cut");
        const auto paste = menu.addAction("Paste");
        menu.addSeparator();
        const auto prop = menu.addAction("Property");

        copy->setData(static_cast<int32_t>(MenuItem::Copy));
        paste->setData(static_cast<int32_t>(MenuItem::Paste));
        cut->setData(static_cast<int32_t>(MenuItem::Cut));
        prop->setData(static_cast<int32_t>(MenuItem::Property));

        copy->setShortcut(QKeySequence::Copy);
        cut->setShortcut(QKeySequence::Cut);
        paste->setShortcut(QKeySequence::Paste);
        del->setShortcut(QKeySequence::Delete);
        prop->setShortcut(Qt::CTRL | Qt::Key_Comma);

        const auto addActionGroup = new QActionGroup(self);
        addActionGroup->addAction(addRect);
        addActionGroup->addAction(addEllipse);

        connect(addActionGroup, &QActionGroup::triggered, self, &Self::onAddActionTriggered);
        connect(copy, &QAction::triggered, self, &Self::onCopy);
        connect(paste, &QAction::triggered, self, &Self::onPaste);
        connect(cut, &QAction::triggered, self, &Self::onCut);
        connect(del, &QAction::triggered, self, &Self::onDeleteSelectedRoiItems);
        connect(prop, &QAction::triggered, self, &Self::onShowProperty);

        menu.addActions(menuItems);
    }

    auto EditView::Impl::ShowContextMenu(const QPoint& pos) -> void {
        QAction* paste = nullptr;
        QAction* copy = nullptr;
        QAction* cut = nullptr;
        for (const auto& item : menu.actions()) {
            if (item->data() == static_cast<int32_t>(MenuItem::Paste)) {
                paste = item;
            } else if (item->data() == static_cast<int32_t>(MenuItem::Copy)) {
                copy = item;
            } else if (item->data() == static_cast<int32_t>(MenuItem::Cut)) {
                cut = item;
            }
        }

        assert(paste);
        assert(copy);
        assert(cut);

        copy->setEnabled(!scene->selectedItems().isEmpty());
        cut->setEnabled(!copyTargtes.isEmpty());
        paste->setEnabled(!copyTargtes.isEmpty());

        menu.exec(pos);
    }

    auto EditView::Impl::SetPointerMode() -> void {
        ready = false;
        self->setCursor(Qt::ArrowCursor);
        scene->update();
        emit self->sigDrawCompleted();
    }

    auto EditView::Impl::GetNewIndex() const -> int32_t {
        QVector<int32_t> indices;
        for (const auto& item : scene->items()) {
            if (item->type() == GraphicsRoiItem::Type) {
                const auto roi = qgraphicsitem_cast<GraphicsRoiItem*>(item);
                indices.push_back(roi->GetIndex());
            }
        }
        return 1 + *std::max_element(indices.cbegin(), indices.cend());
    }

    auto EditView::Impl::GetDefaultRect() const -> QRectF {
        return {-defaultRoiSize / 2, -defaultRoiSize / 2, defaultRoiSize, defaultRoiSize};
    }

    auto EditView::Impl::GetDefaultName(const ROIIndex& roiIndex) const -> QString {
        if (roiIndex == -1) {
            return "ROI";
        }
        return QString("ROI %1").arg(roiIndex);
    }

    auto EditView::Impl::GetPastedPos(const QRectF& rect, const QPointF& pos) const -> QPointF {
        for (const auto& item : scene->items()) {
            if (item->type() == GraphicsRoiItem::Type) {
                const auto roi = qgraphicsitem_cast<GraphicsRoiItem*>(item);
                if (fabs(roi->rect().width() - rect.width()) <= std::numeric_limits<double>::epsilon() && fabs(roi->rect().height() - rect.height()) <= std::numeric_limits<double>::epsilon() && fabs(roi->pos().x() - pos.x()) <= std::numeric_limits<double>::epsilon() && fabs(roi->pos().y() - pos.y()) <= std::numeric_limits<double>::epsilon()) {
                    return GetPastedPos(rect, {pos.x() + 1, pos.y() + 1});
                }
            }
        }

        return pos;
    }

    auto EditView::Impl::InitRuler() -> void {
        rulerV = new Ruler(Qt::Vertical, self, self);
        rulerH = new Ruler(Qt::Horizontal, self, self);
        rulerLTop = new RulerLeftTopWidget(self);
    }

    auto EditView::Impl::IsDrawableArea(const QRectF& rect) const -> bool {
        return RoiSetupHelper::GetInstance()->IsDrawableArea({selectedShape, rect});
    }

    auto EditView::Impl::IsDrawableArea() const -> bool {
        auto r = GetDefaultRect();
        r.moveCenter(cursorPos);
        return IsDrawableArea(r);
    }

    auto EditView::Impl::UpdateRois() const -> void {
        QList<RegionOfInterest::Pointer> list;
        QList<ROIIndex> selectedIndices;
        for (const auto& item : scene->items()) {
            if (item->type() == GraphicsRoiItem::Type) {
                const auto roiItem = qgraphicsitem_cast<GraphicsRoiItem*>(item);
                auto index = roiItem->GetIndex();
                auto name = roiItem->GetName();
                auto shape = roiItem->GetShape();
                auto posMM = RoiSetupHelper::GetWellPosFromScenePos(roiItem->mapToScene(roiItem->rect().center()));
                auto size = roiItem->rect().size();

                auto roi = std::make_shared<RegionOfInterest>();
                roi->SetIndex(index);
                roi->SetName(name);
                roi->SetPosition(posMM);
                roi->SetSize(size.width(), size.height());
                roi->SetShape(shape);
                list.push_back(roi);

                if (roiItem->isSelected()) {
                    selectedIndices.push_back(index);
                }
            }
        }
        emit self->sigUpdate(list);
        self->onSelectionChanged();
    }

    auto EditView::Impl::BeginPropertyEdit(const ROIIndex& target) -> void {
        scene->clearSelection();
        for (const auto& item : scene->GetAllRoiItems()) {
            if (item->GetIndex() == target) {
                item->setOpacity(0.7);
            } else {
                item->setOpacity(0.4);
            }
            item->SetNameVisible(false);
        }
    }

    auto EditView::Impl::EndPropertyEdit(const ROIIndex& target) -> void {
        for (const auto& item : scene->GetAllRoiItems()) {
            item->setOpacity(1.0);
            item->SetNameVisible(roiNameVisible);
            if (item->GetIndex() == target) {
                item->setSelected(true);
            }
        }
    }

    auto EditView::Impl::GetRoiOutlinePos() const -> QPair<QPoint, QPoint> {
        if (scene->GetSelectedRoiItems().empty()) {
            return {{INT_MAX,INT_MAX}, {INT_MAX,INT_MAX}};
        }
        QGraphicsItemGroup group;
        const auto& item = scene->GetSelectedRoiItems().constFirst();
        const auto paddingW = (item->boundingRect().width() - item->rect().width()) / 2;
        const auto paddingH = (item->boundingRect().height() - item->rect().height()) / 2;
        for (const auto& roi : scene->GetSelectedRoiItems()) {
            const auto temp = roi->Clone();
            group.addToGroup(temp);
        }
        const auto boundingRect = group.boundingRect();
        const auto topLeft = self->mapFromScene(boundingRect.topLeft().x() + paddingW, boundingRect.topLeft().y() + paddingH);
        const auto bottomRight = self->mapFromScene(boundingRect.bottomRight().x() - paddingH, boundingRect.bottomRight().y() - paddingH);

        return {topLeft, bottomRight};
    }

    auto EditView::Impl::UpdateRulerPosIndicator() const -> void {
        if (!scene) {
            return;
        }
        rulerH->UpdateCursorPos(mouseMovePos);
        rulerV->UpdateCursorPos(mouseMovePos);
        rulerH->UpdateSelectedRoiPos(GetRoiOutlinePos());
        rulerV->UpdateSelectedRoiPos(GetRoiOutlinePos());
        rulerH->update();
        rulerV->update();
    }

    auto EditView::Impl::InitZoomWidget() -> void {
        zoomWidget = new ZoomWidget(self);
        connect(zoomWidget, &ZoomWidget::sigZoomInButtonClicked, self, [this] { self->onZoom(ZoomIn); });
        connect(zoomWidget, &ZoomWidget::sigZoomOutButtonClicked, self, [this] { self->onZoom(ZoomOut); });
        connect(zoomWidget, &ZoomWidget::sigZoomFitButtonClicked, self, [this] { self->onZoom(ZoomFit); });
    }
}
