#pragma once
#include <memory>

#include <QGraphicsScene>

#include "GraphicsRoiItem.h"

namespace HTXpress::AppComponents::RoiSetupWindow {
    class EditScene final : public QGraphicsScene {
        Q_OBJECT
    public:
        using Self = EditScene;

        explicit EditScene(QObject* parent = nullptr);
        ~EditScene() override;

        auto SetDrawableArea(const QRectF& drawableRect) -> void;
        auto GetTopLeft() const -> QPointF;
        auto GetSelectedRoiItems() const -> QList<GraphicsRoiItem*>;
        auto GetRoiItem(const RoiSetupDefinitions::ROIIndex& index) -> GraphicsRoiItem*;
        auto GetAllRoiItems() const -> QList<GraphicsRoiItem*>;

    signals:
        void sigItemsMoved(QMap<GraphicsRoiItem*, QPointF>);

    private:
        auto mousePressEvent(QGraphicsSceneMouseEvent* event) -> void override;
        auto mouseReleaseEvent(QGraphicsSceneMouseEvent* event) -> void override;

    private slots:
        void onSelectionChanged();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
