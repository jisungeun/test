﻿#include <QHeaderView>
#include <QMenu>
#include <QContextMenuEvent>
#include <QAction>

#include "TableView.h"
#include "TableModel.h"
#include "DelegateDoubleSpinBox.h"
#include "DelegateLineEdit.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::RoiSetupWindow {
    struct TableView::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        Self* self{};
        TableModel* model{};
        ProxyModel* proxyModel{};
        QMap<WellIndex, QString> wellPosNames{};
        QMap<ColumnHeader, Range> ranges;
    };

    TableView::TableView(QWidget* parent) : QTableView(parent), d{std::make_unique<Impl>(this)} {
        connect(this, &Self::doubleClicked, this, &Self::onDoubleClicked);
    }

    TableView::~TableView() = default;

    auto TableView::Init() -> void {
        d->model = new TableModel(this);
        d->proxyModel = new ProxyModel(this);
        d->proxyModel->setSourceModel(d->model);
        setModel(d->proxyModel);

        setSelectionBehavior(SelectRows);
        horizontalHeader()->setSectionResizeMode(ColumnHeader::WellIndex, QHeaderView::Fixed);
        horizontalHeader()->setSectionResizeMode(ColumnHeader::RoiIndex, QHeaderView::ResizeToContents);
        horizontalHeader()->setSectionResizeMode(ColumnHeader::Name, QHeaderView::Interactive);
        horizontalHeader()->setSectionResizeMode(ColumnHeader::Shape, QHeaderView::Stretch);
        horizontalHeader()->setSectionResizeMode(ColumnHeader::CenterX, QHeaderView::Fixed);
        horizontalHeader()->setSectionResizeMode(ColumnHeader::CenterY, QHeaderView::Fixed);
        horizontalHeader()->setSectionResizeMode(ColumnHeader::Width, QHeaderView::Fixed);
        horizontalHeader()->setSectionResizeMode(ColumnHeader::Height, QHeaderView::Fixed);
        horizontalHeader()->setSectionResizeMode(ColumnHeader::WellPosName, QHeaderView::ResizeToContents);
        horizontalHeader()->hideSection(ColumnHeader::WellIndex);
        setSelectionMode(ExtendedSelection);
        setSortingEnabled(true);

        setColumnWidth(ColumnHeader::CenterX, 70);
        setColumnWidth(ColumnHeader::CenterY, 70);
        setColumnWidth(ColumnHeader::Width, 70);
        setColumnWidth(ColumnHeader::Height, 70);
        setColumnWidth(ColumnHeader::WellIndex, 38);
        setColumnWidth(ColumnHeader::RoiIndex, 38);
        setColumnWidth(ColumnHeader::WellPosName, 48);
        setColumnWidth(ColumnHeader::Shape, 65);

        connect(selectionModel(), &QItemSelectionModel::selectionChanged, this, &Self::onSelectionChanged);
        connect(d->model, &TableModel::dataChanged, this, &Self::onDataChanged);
        horizontalHeader()->setSortIndicatorShown(true);
    }

    auto TableView::SetCurrentWell(const WellIndex& wellIndex) -> void {
        d->model->SetCurrentWellIndex(wellIndex);
    }

    auto TableView::SetPosXRange(const Range& range) -> void {
        d->ranges[ColumnHeader::CenterX] = range;
    }

    auto TableView::SetPosYRange(const Range& range) -> void {
        d->ranges[ColumnHeader::CenterY] = range;
    }

    auto TableView::SetWidthRange(const Range& range) -> void {
        d->ranges[ColumnHeader::Width] = range;
    }

    auto TableView::SetHeightRange(const Range& range) -> void {
        d->ranges[ColumnHeader::Height] = range;
    }

    auto TableView::SetWellPositionNames(const QMap<WellIndex, QString>& wellPosNames) -> void {
        d->wellPosNames = wellPosNames;
    }

    auto TableView::Sort() -> void {
        sortByColumn(ColumnHeader::RoiIndex, Qt::AscendingOrder);
        sortByColumn(ColumnHeader::WellPosName, Qt::AscendingOrder);
    }

    auto TableView::SetItemDelegates() -> void {
        setItemDelegateForColumn(ColumnHeader::CenterX, new DelegateDoubleSpinBox(d->ranges[ColumnHeader::CenterX], this));
        setItemDelegateForColumn(ColumnHeader::CenterY, new DelegateDoubleSpinBox(d->ranges[ColumnHeader::CenterY], this));
        setItemDelegateForColumn(ColumnHeader::Width, new DelegateDoubleSpinBox(d->ranges[ColumnHeader::Width], this));
        setItemDelegateForColumn(ColumnHeader::Height, new DelegateDoubleSpinBox(d->ranges[ColumnHeader::Height], this));
        setItemDelegateForColumn(ColumnHeader::Name, new DelegateLineEdit(this));
    }

    auto TableView::UpdateROIs(const ROIs& rois) -> void {
        if (d->model->rowCount() > 0) {
            d->model->removeRows(0, d->model->rowCount());
        }
        Add(rois);
    }

    auto TableView::SetSelectedRois(const WellIndex& wellIndex, const QList<ROIIndex>& roiIndices) -> void {
        auto modelIndexList = d->proxyModel->match(d->proxyModel->index(0, ColumnHeader::WellIndex), Qt::DisplayRole, wellIndex, -1);
        for (const auto& modelIndex : modelIndexList) {
            if (roiIndices.contains(modelIndex.siblingAtColumn(ColumnHeader::RoiIndex).data().toInt())) {
                QItemSelection selection;
                selection.select(d->proxyModel->index(modelIndex.row(), 0), d->proxyModel->index(modelIndex.row(), d->proxyModel->columnCount() - 1));
                selectionModel()->select(selection, QItemSelectionModel::Select);
            }
        }
    }

    auto TableView::Add(const ROIs& rois) -> void {
        for (auto it = rois.cbegin(); it != rois.cend(); ++it) {
            const auto wellIndex = it.key();
            const auto roiList = it.value();
            for (const auto& a : roiList) {
                const auto roiIndex = a->GetIndex();
                const auto roiName = a->GetName();
                const auto roiShape = a->GetShape();
                const auto roiCenter = a->GetPosition();
                const auto roiSize = a->GetSize();

                TableModel::TableROI roi;
                roi.wellIndex = wellIndex;
                roi.wellPosName = d->wellPosNames[wellIndex];
                roi.roiIndex = roiIndex;
                roi.name = roiName;
                roi.shape = roiShape;
                roi.centerX = roiCenter.x;
                roi.centerY = roiCenter.y;
                roi.width = roiSize.w;
                roi.height = roiSize.h;

                d->model->insertRows(0, 1);
                auto index = d->model->index(0, ColumnHeader::WellIndex);
                d->model->setData(index, roi.wellIndex);
                index = d->model->index(0, ColumnHeader::RoiIndex);
                d->model->setData(index, roi.roiIndex);
                index = d->model->index(0, ColumnHeader::Name);
                d->model->setData(index, roi.name);
                index = d->model->index(0, ColumnHeader::Shape);
                d->model->setData(index, roi.shape);
                index = d->model->index(0, ColumnHeader::CenterX);
                d->model->setData(index, roi.centerX);
                index = d->model->index(0, ColumnHeader::CenterY);
                d->model->setData(index, roi.centerY);
                index = d->model->index(0, ColumnHeader::Width);
                d->model->setData(index, roi.width);
                index = d->model->index(0, ColumnHeader::Height);
                d->model->setData(index, roi.height);
                index = d->model->index(0, ColumnHeader::WellPosName);
                d->model->setData(index, roi.wellPosName);
            }
        }
    }

    auto TableView::contextMenuEvent(QContextMenuEvent* event) -> void {
        const auto modelIndex = indexAt(event->pos());
        if(!modelIndex.isValid()) {
            return;
        } 
        if(modelIndex.siblingAtColumn(ColumnHeader::WellIndex).data().toInt() != d->model->GetCurrentWellIndex()) {
            return;
        }

        QMenu menu(this);
        const auto porpertyAction = menu.addAction("Property");
        porpertyAction->setShortcut(Qt::CTRL | Qt::Key_Comma);

        connect(porpertyAction, &QAction::triggered, this, [this] {
            emit sigShowProperty();
        });

        menu.exec(event->globalPos());
    }

    auto TableView::onSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected) -> void {
        Q_UNUSED(selected)
        Q_UNUSED(deselected)
        QList<QPair<WellIndex, ROIIndex>> indexPairs;
        for (const auto& wellModelIndex : selectionModel()->selectedRows(ColumnHeader::WellIndex)) {
            const auto wellIndex = wellModelIndex.data().toInt();
            const auto roiIndex = wellModelIndex.siblingAtColumn(ColumnHeader::RoiIndex).data().toInt();
            indexPairs.push_back({wellIndex, roiIndex});
        }

        emit sigSelectionChanged(indexPairs);
    }

    void TableView::onDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>& roles) {
        Q_UNUSED(bottomRight)
        if (!topLeft.isValid()) {
            return;
        }
        if (!roles.contains(Qt::DisplayRole)) {
            return;
        }

        const auto modelIndex = topLeft;
        const auto column = ColumnHeader::_from_integral(modelIndex.column());
        const auto wellIndex = modelIndex.siblingAtColumn(ColumnHeader::WellIndex).data().toInt();
        const auto roiIndex = modelIndex.siblingAtColumn(ColumnHeader::RoiIndex).data().toInt();

        switch (column) {
            case ColumnHeader::CenterX: [[fallthrough]];
            case ColumnHeader::CenterY: {
                const auto x = modelIndex.siblingAtColumn(ColumnHeader::CenterX).data().toDouble();
                const auto y = modelIndex.siblingAtColumn(ColumnHeader::CenterY).data().toDouble();
                sigPosChanged(wellIndex, roiIndex, x, y);
                break;
            }
            case ColumnHeader::Width: [[fallthrough]];
            case ColumnHeader::Height: {
                const auto w = modelIndex.siblingAtColumn(ColumnHeader::Width).data().toDouble();
                const auto h = modelIndex.siblingAtColumn(ColumnHeader::Height).data().toDouble();
                sigSizeChanged(wellIndex, roiIndex, w, h);
                break;
            }
            case ColumnHeader::Name: {
                const auto name = modelIndex.data().toString();
                sigNameChanged(wellIndex, roiIndex, name);
                break;
            }
            default:
                break;
        }
    }

    void TableView::onDoubleClicked(const QModelIndex& modelIndex) {
        if(!modelIndex.isValid()) {
            return;
        }

        const auto wellIndex = modelIndex.siblingAtColumn(ColumnHeader::WellIndex).data().toInt();
        SetCurrentWell(wellIndex);
        emit sigChangeWellIndex(wellIndex);
    }
}
