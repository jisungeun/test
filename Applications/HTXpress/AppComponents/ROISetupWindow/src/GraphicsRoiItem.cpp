﻿#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QInputDialog>

#include "GraphicsRoiItem.h"
#include "RoiSetupHelper.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::RoiSetupWindow {
    struct GraphicsRoiItem::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        Self* self{};
        int32_t index{};
        QString name{};
        bool nameVisible{};
        ItemShape::_enumerated shape{};

        bool pressed{};

        double resizeHandleSize{0.1};
        ResizeHandle resizeHandle{NoHandle};
        QMap<ResizeHandle, Qt::CursorShape> cursorShapes{};

        QRectF oldRect{};
        bool drawable{false};

        static QList<ROIIndex> SelectedOrder;

        auto DrawRoI(QPainter* painter) -> void;
        auto DrawResizeHandles(QPainter* painter) -> void;
        auto DrawText(QPainter* painter) -> void;
        auto ShowChangeName() const -> void;
        auto IsDrawable() const -> bool;
        auto ChangeIndices(bool selected) -> void;
    };

    QList<ROIIndex> GraphicsRoiItem::Impl::SelectedOrder{};

    GraphicsRoiItem::GraphicsRoiItem(const int32_t& index, const QString& name, const ItemShape& shape, const QRectF& rect, QGraphicsItem* parent) : QGraphicsRectItem(rect, parent), d{std::make_unique<Impl>(this)} {
        d->index = index;
        d->name = name;
        d->shape = shape;

        setFlag(ItemIsMovable);
        setFlag(ItemIsSelectable);
        setFlag(ItemSendsGeometryChanges);
        setAcceptedMouseButtons(Qt::LeftButton);
        setAcceptHoverEvents(true);

        d->cursorShapes[NoHandle] = Qt::ArrowCursor;
        d->cursorShapes[TopLeft] = Qt::SizeFDiagCursor;
        d->cursorShapes[TopRight] = Qt::SizeBDiagCursor;
        d->cursorShapes[BottomLeft] = Qt::SizeBDiagCursor;
        d->cursorShapes[BottomRight] = Qt::SizeFDiagCursor;
        // TODO add more resize handles
        d->cursorShapes[Top] = Qt::SizeVerCursor;
        d->cursorShapes[Bottom] = Qt::SizeVerCursor;
        d->cursorShapes[Left] = Qt::SizeHorCursor;
        d->cursorShapes[Right] = Qt::SizeHorCursor;
    }

    auto GraphicsRoiItem::Clone() const -> GraphicsRoiItem* {
        const auto cloned = new GraphicsRoiItem(d->index, d->name, d->shape, rect());
        cloned->setPos(pos());
        return cloned;
    }

    GraphicsRoiItem::~GraphicsRoiItem() = default;

    auto GraphicsRoiItem::type() const -> int {
        return Type;
    }

    auto GraphicsRoiItem::shape() const -> QPainterPath {
        if (d->shape == ItemShape::Ellipse && !isSelected()) {
            QPainterPath path;
            path.addEllipse(rect());
            return path;
        }
        return QGraphicsRectItem::shape();
    }

    auto GraphicsRoiItem::SetIndex(const int32_t& index) -> void {
        d->index = index;
    }

    auto GraphicsRoiItem::GetIndex() const -> int32_t {
        return d->index;
    }

    auto GraphicsRoiItem::SetNameVisible(bool show) -> void {
        d->nameVisible = show;
        update();
    }

    auto GraphicsRoiItem::SetName(const QString& name) -> void {
        d->name = name;
        update();
    }

    auto GraphicsRoiItem::GetName() const -> QString {
        return d->name;
    }

    auto GraphicsRoiItem::SetShape(const ItemShape& shape) -> void {
        d->shape = shape;
    }

    auto GraphicsRoiItem::GetShape() const -> ItemShape {
        return d->shape;
    }

    auto GraphicsRoiItem::itemChange(GraphicsItemChange change, const QVariant& value) -> QVariant {
        if (change == ItemSelectedHasChanged) {
            d->ChangeIndices(value.toBool());
        }
        return QGraphicsRectItem::itemChange(change, value);
    }

    auto GraphicsRoiItem::GetLastSelectedItemIndex() -> ROIIndex {
        if (!Impl::SelectedOrder.isEmpty()) return Impl::SelectedOrder.last();

        return -1;
    }

    auto GraphicsRoiItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(widget)
        Q_UNUSED(option)
        d->DrawRoI(painter);
        if (isSelected()) {
            d->DrawResizeHandles(painter);
        }
        if (d->nameVisible) {
            d->DrawText(painter);
        }
    }

    auto GraphicsRoiItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event) -> void {
        if (d->pressed) {
            if (d->resizeHandle != NoHandle) {
                QRectF r = rect();
                QPointF pos = event->pos();

                if (d->resizeHandle == TopLeft) {
                    if (pos.x() < r.right() && pos.y() < r.bottom()) {
                        r.setTopLeft(pos);
                    }
                }
                else if (d->resizeHandle == TopRight) {
                    if (pos.x() > r.left() && pos.y() < r.bottom()) {
                        r.setTopRight(pos);
                    }
                }
                else if (d->resizeHandle == BottomLeft) {
                    if (pos.x() < r.right() && pos.y() > r.top()) {
                        r.setBottomLeft(pos);
                    }
                }
                else if (d->resizeHandle == BottomRight) {
                    if (pos.x() > r.left() && pos.y() > r.top()) {
                        r.setBottomRight(pos);
                    }
                }
                setRect(r);
                return;
            }
        }
        QGraphicsRectItem::mouseMoveEvent(event);
    }

    auto GraphicsRoiItem::mousePressEvent(QGraphicsSceneMouseEvent* event) -> void {
        if (event->button() == Qt::LeftButton) {
            d->pressed = true;
            d->oldRect = rect();
        }
        QGraphicsRectItem::mousePressEvent(event);
    }

    auto GraphicsRoiItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* event) -> void {
        if (d->pressed) {
            if (d->oldRect != rect()) {
                emit sigSizeChanged(d->oldRect);
            }
        }
        d->pressed = false;
        QGraphicsRectItem::mouseReleaseEvent(event);
    }

    auto GraphicsRoiItem::hoverMoveEvent(QGraphicsSceneHoverEvent* event) -> void {
        if (isSelected()) {
            const QPointF pos = event->pos();
            const QRectF itemRect = rect();
            const QRectF rTL = RoiSetupHelper::GetItemRect(itemRect.topLeft(), d->resizeHandleSize, d->resizeHandleSize);
            const QRectF rTR = RoiSetupHelper::GetItemRect(itemRect.topRight(), d->resizeHandleSize, d->resizeHandleSize);
            const QRectF rBL = RoiSetupHelper::GetItemRect(itemRect.bottomLeft(), d->resizeHandleSize, d->resizeHandleSize);
            const QRectF rBR = RoiSetupHelper::GetItemRect(itemRect.bottomRight(), d->resizeHandleSize, d->resizeHandleSize);

            if (rTL.contains(pos)) {
                d->resizeHandle = TopLeft;
            }
            else if (rTR.contains(pos)) {
                d->resizeHandle = TopRight;
            }
            else if (rBL.contains(pos)) {
                d->resizeHandle = BottomLeft;
            }
            else if (rBR.contains(pos)) {
                d->resizeHandle = BottomRight;
            }
            else {
                d->resizeHandle = NoHandle;
            }
        }
        else {
            d->resizeHandle = NoHandle;
        }

        setCursor(d->cursorShapes[d->resizeHandle]);

        QGraphicsRectItem::hoverMoveEvent(event);
    }

    auto GraphicsRoiItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) -> void {
        if (event->button() == Qt::LeftButton && event->modifiers() & Qt::ControlModifier) {
            d->ShowChangeName();
        }
        QGraphicsRectItem::mouseDoubleClickEvent(event);
    }

    auto GraphicsRoiItem::Impl::DrawResizeHandles(QPainter* painter) -> void {
        painter->save();
        auto r = self->rect();
        auto pen = painter->pen();
        pen.setColor(Qt::lightGray);
        pen.setStyle(Qt::DotLine);
        pen.setWidthF(0.1);
        painter->setPen(pen);
        painter->setBrush(Qt::NoBrush);
        painter->drawRect(r);

        resizeHandleSize = 5 / QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform());

        painter->setPen(Qt::NoPen);
        painter->setBrush(Qt::green);
        painter->drawEllipse(r.topLeft(), resizeHandleSize, resizeHandleSize);
        painter->drawEllipse(r.topRight(), resizeHandleSize, resizeHandleSize);
        painter->drawEllipse(r.bottomLeft(), resizeHandleSize, resizeHandleSize);
        painter->drawEllipse(r.bottomRight(), resizeHandleSize, resizeHandleSize);
        painter->restore();
    }

    auto GraphicsRoiItem::Impl::DrawText(QPainter* painter) -> void {
        painter->save();
        auto r = self->boundingRect();
        auto font = painter->font();
        font.setPixelSize(1);
        painter->setFont(font);
        auto pen = painter->pen();
        pen.setColor(Qt::white);
        pen.setWidthF(0.1);

        QTextOption textOption;
        textOption.setAlignment(Qt::AlignCenter);
        textOption.setWrapMode(QTextOption::WordWrap);

        QFontMetrics fm(painter->font());

        painter->setPen(pen);
        painter->drawText(QRectF(r.bottomLeft(), QSizeF{r.width(), static_cast<double>(fm.height())}), name, textOption);
        painter->drawText(r, QString::number(index), textOption);
        painter->restore();
    }

    auto GraphicsRoiItem::Impl::ShowChangeName() const -> void {
        bool ok{};
        const auto oldName = name;
        const auto newName = QInputDialog::getText(nullptr, "ROI Name", "Name", QLineEdit::Normal, oldName, &ok);
        if (ok && !newName.isEmpty() && newName != oldName) {
            self->SetName(newName);
            emit self->sigNameChanged(oldName);
        }
    }

    auto GraphicsRoiItem::Impl::IsDrawable() const -> bool {
        return RoiSetupHelper::GetInstance()->IsDrawableArea({shape, self->mapRectToScene(self->rect())});
    }

    auto GraphicsRoiItem::Impl::ChangeIndices(bool selected) -> void {
        if (selected) {
            SelectedOrder.push_back(index);
        }
        else {
            SelectedOrder.removeAll(index);
        }
    }

    auto GraphicsRoiItem::Impl::DrawRoI(QPainter* painter) -> void {
        painter->save();
        auto pen = painter->pen();
        if (IsDrawable()) {
            pen.setColor(Qt::blue);
        }
        else {
            pen.setColor(Qt::red);
        }
        pen.setWidthF(0.1);
        painter->setPen(pen);

        QBrush brush;
        brush.setStyle(Qt::SolidPattern);
        auto brushColor = pen.color();
        brushColor.setAlpha(55);
        brush.setColor(brushColor);
        painter->setBrush(brush);

        switch (shape) {
            case ItemShape::Ellipse: painter->drawEllipse(self->rect());
                break;
            case ItemShape::Rectangle: painter->drawRect(self->rect());
                break;
            default: break;
        }
        painter->restore();
    }

    GraphicsTempRoiItem::GraphicsTempRoiItem(const int32_t& index, const QString& name, const ItemShape& shape, const QRectF& rect, QGraphicsItem* parent)
    : GraphicsRoiItem(index, name, shape, rect, parent) {
        setFlag(ItemIsMovable, false);
        setFlag(ItemIsSelectable, false);
    }

    auto GraphicsTempRoiItem::type() const -> int {
        return Type;
    }

    auto GraphicsTempRoiItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        GraphicsRoiItem::paint(painter, option, widget);
    }
}
