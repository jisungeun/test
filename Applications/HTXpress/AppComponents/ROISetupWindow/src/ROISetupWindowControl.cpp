#include <QDir>
#include <QFileInfo>
#include <QVariantMap>

#include <ROIWriter.h>
#include <ROIReader.h>
#include <RegionOfInterest.h>
#include <RoiSetupDefines.h>

#include "RoiSetupWindowControl.h"
#include "RoiSetupHelper.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::RoiSetupWindow {
    struct RoiSetupWindowControl::Impl {
        RoiSetupVessel::Pointer vessel{};

        WellIndex currentWellIndex{};
        ROIs lastROIs{};

        QMap<QString, QList<RegionOfInterest::Pointer>> loadedPresets{};

        auto GetWell(const WellIndex& wellIndex) const -> std::shared_ptr<Well>;
    };

    auto RoiSetupWindowControl::Impl::GetWell(const WellIndex& wellIndex) const -> std::shared_ptr<Well> {
        for (const auto& well : vessel->GetWells()) {
            if (wellIndex == well->index)
                return well;
        }

        return nullptr;
    }
    
    RoiSetupWindowControl::RoiSetupWindowControl() : d{std::make_unique<Impl>()} {
    }

    RoiSetupWindowControl::~RoiSetupWindowControl() = default;

    auto RoiSetupWindowControl::SetVessel(const RoiSetupVessel::Pointer& vessel) -> void {
        d->vessel = vessel;
        for (const auto& well : d->vessel->GetWells()) {
            d->lastROIs[well->index] = well->rois;
        }
    }

    auto RoiSetupWindowControl::SetCurrentWellIndex(const WellIndex& wellIndex) -> void {
        d->currentWellIndex = wellIndex;
    }

    auto RoiSetupWindowControl::GetCurrentWellIndex() const -> WellIndex {
        return d->currentWellIndex;
    }

    auto RoiSetupWindowControl::UpdateRois(QList<RegionOfInterest::Pointer> rois) -> void {
        const auto well = d->GetWell(d->currentWellIndex);
        if (well != nullptr) {
            well->rois = std::move(rois);
        }
    }

    auto RoiSetupWindowControl::GetCurrentRois(const WellIndex& wellIndex) const -> QList<RegionOfInterest::Pointer> {
        const auto well = d->GetWell(wellIndex);
        if (well != nullptr) {
            return well->rois;
        }
        return {};
    }

    auto RoiSetupWindowControl::GetCurrentRois() const -> QList<RegionOfInterest::Pointer> {
        return GetCurrentRois(d->currentWellIndex);
    }

    auto RoiSetupWindowControl::GetAllRois() const -> ROIs {
        ROIs output;
        if (d->vessel == nullptr) {
            return {};
        }
        const auto wells = d->vessel->GetWells();
        for (const auto& well : wells) {
            output[well->index] = well->rois;
        }

        return output;
    }

    auto RoiSetupWindowControl::GetCurrentWellShape() const -> ItemShape {
        const auto well = d->GetWell(d->currentWellIndex);
        if (well != nullptr) {
            return d->GetWell(d->currentWellIndex)->shape;
        }
        return ItemShape::Ellipse;
    }

    auto RoiSetupWindowControl::GetWellRect() const -> QRectF {
        const auto well = d->GetWell(d->currentWellIndex);
        if (well != nullptr) {
            const auto x = -well->size.w / 2;
            const auto y = -well->size.h / 2;
            const auto w = well->size.w;
            const auto h = well->size.h;
            return {x, y, w, h};
        }
        return {};
    }

    auto RoiSetupWindowControl::GetImagingAreaShape() const -> ItemShape {
        const auto imagingArea = d->vessel->GetImagingArea();
        if (imagingArea != nullptr) {
            return imagingArea->shape;
        }
        return ItemShape::Ellipse;
    }

    auto RoiSetupWindowControl::GetImagingAreaRect() const -> QRectF {
        const auto imagingArea = d->vessel->GetImagingArea();
        if (imagingArea != nullptr) {
            const auto pos = RoiSetupHelper::GetScenePosFromWellPos(imagingArea->position);
            const auto x = -(imagingArea->size.w / 2) + pos.x();
            const auto y = -(imagingArea->size.h / 2) + pos.y();
            const auto w = imagingArea->size.w;
            const auto h = imagingArea->size.h;
            return {x, y, w, h};
        }
        return {};
    }

    auto RoiSetupWindowControl::GetPosXRange() const -> Range {
        const auto centerX = d->vessel->GetImagingArea()->position.x;
        const auto width = d->vessel->GetImagingArea()->size.w;
        return {centerX - width / 2, centerX + width / 2};
    }

    auto RoiSetupWindowControl::GetPosYRange() const -> Range {
        const auto centerY = d->vessel->GetImagingArea()->position.y;
        const auto height = d->vessel->GetImagingArea()->size.h;
        return {centerY - height / 2, centerY + height / 2};
    }

    auto RoiSetupWindowControl::GetWidthRange() const -> Range {
        const auto width = d->vessel->GetImagingArea()->size.w;
        return {0., width};
    }

    auto RoiSetupWindowControl::GetHeightRange() const -> Range {
        const auto height = d->vessel->GetImagingArea()->size.h;
        return {0., height};
    }

    auto RoiSetupWindowControl::GetWellPositionNames() const -> QMap<WellIndex, QString> {
        QMap<WellIndex, QString> posNames;

        for (const auto& well : d->vessel->GetWells()) {
            const auto index = well->index;
            char rowString[50];
            auto i = 0;
            auto row = well->array.row + 1;
            const auto remainder = row % 26;
            if (remainder == 0) {
                rowString[i++] = 'Z';
                row = (row / 26) - 1;
            } else {
                rowString[i++] = (remainder - 1) + 'A';
                row /= 26;
            }
            rowString[i] = '\0';
            std::reverse(rowString, rowString + strlen(rowString));

            const QString posName = QString::fromLatin1(rowString) + QString::number(well->array.col + 1);
            posNames[index] = posName;
        }

        return posNames;
    }

    auto RoiSetupWindowControl::GetWellCount() const -> int32_t {
        return d->vessel->GetWells().size();
    }

    auto RoiSetupWindowControl::AreAllROIsInDrawableArea() const -> bool {
        const auto wells = d->vessel->GetWells();
        for (const auto& well : wells) {
            for (const auto& roi : well->rois) {
                const auto roiRect = RoiSetupHelper::GetItemRect({RoiSetupHelper::GetScenePosFromWellPos(roi->GetPosition())}, roi->GetWidth() / 2, roi->GetHeight() / 2);
                const auto shape = roi->GetShape();
                if (!RoiSetupHelper::GetInstance()->IsDrawableArea({shape, roiRect})) {
                    return false;
                }
            }
        }
        return true;
    }

    auto RoiSetupWindowControl::ChangeRoiName(const WellIndex& wellIndex, const ROIIndex& roiIndex, const QString& name) -> void {
        if (const auto well = d->GetWell(wellIndex)) {
            for (const auto& roi : well->rois) {
                if (roi->GetIndex() == roiIndex) {
                    roi->SetName(name);
                }
            }
        }
    }

    auto RoiSetupWindowControl::ChangeRoiPosition(const WellIndex& wellIndex, const ROIIndex& roiIndex, const double& x, const double& y) -> void {
        if (const auto well = d->GetWell(wellIndex)) {
            for (const auto& roi : well->rois) {
                if (roi->GetIndex() == roiIndex) {
                    roi->SetPosition(x, y);
                }
            }
        }
    }

    auto RoiSetupWindowControl::ChangeRoiSize(const WellIndex& wellIndex, const ROIIndex& roiIndex, const double& w, const double& h) -> void {
        if (const auto well = d->GetWell(wellIndex)) {
            for (const auto& roi : well->rois) {
                if (roi->GetIndex() == roiIndex) {
                    roi->SetSize(w, h);
                }
            }
        }
    }

    auto RoiSetupWindowControl::SavePreset(const QString& presetName) -> QPair<bool, QString> {
        QString failReason{};
        bool result{};

        ROIIO::ROIWriter writer(presetName);
        const auto vesselName = d->vessel->GetVessel()->name;
        const auto roiList = GetCurrentRois();
        writer.SetVesselName(vesselName);
        writer.SetRoiList(roiList);
        const auto writeResult = writer.Write();

        switch (writeResult) {
            case ROIIO::ROIWriter::WriteResult::Success:
                result = true;
                break;
            case ROIIO::ROIWriter::WriteResult::Fail_FileExists:
                failReason = "File name already exists.";
                break;
            case ROIIO::ROIWriter::WriteResult::Fail_InvalidName:
                failReason = "Invalid characters in file name.";
                break;
            case ROIIO::ROIWriter::WriteResult::Fail_Unknown:
                failReason = "Unknown error occurred.";
                break;
            case ROIIO::ROIWriter::WriteResult::Fail_Open:
                failReason = "Failed to open file.";
                break;
            case ROIIO::ROIWriter::WriteResult::Fail_Write:
                failReason = "Failed to write to file.";
                break;
            default:
                failReason = "Undefined error occurred.";
                break;
        }

        return {result, failReason};
    }

    auto RoiSetupWindowControl::LoadPresetRoiList(const QString& presetFullPath) const -> std::optional<QList<RegionOfInterest::Pointer>> {
        ROIIO::ROIReader reader(presetFullPath);
        return reader.Read();
    }

    auto RoiSetupWindowControl::GetPresetFolderPath() const -> QString {
        return ROIIO::ROIWriter::GetPresetFolderPath();
    }

    auto RoiSetupWindowControl::GetPresetExtension() const -> QString {
        return ROIIO::ROIWriter::GetFileExtension();
    }

    auto RoiSetupWindowControl::GetPresetNames() const -> QStringList {
        const auto presetFolder = GetPresetFolderPath();
        const auto extension = GetPresetExtension();
        QDir dir(presetFolder);
        QStringList filters;
        filters << "*." + extension;
        dir.setNameFilters(filters);
        auto entryInfoList = dir.entryInfoList(QDir::Files, QDir::Name | QDir::IgnoreCase);

        QStringList names;
        for (const auto& preset : entryInfoList) {
            names << preset.completeBaseName();
        }

        return names;
    }

    auto RoiSetupWindowControl::GetPresetRois(const QString& preset) const -> std::optional<QList<RegionOfInterest::Pointer>> {
        const auto presetFullPath = QString("%1/%2.%3").arg(GetPresetFolderPath()).arg(preset).arg(GetPresetExtension());
        const auto presetRoi = LoadPresetRoiList(presetFullPath);
        if (presetRoi) {
            d->loadedPresets[preset] = *presetRoi;
            return presetRoi;
        }
        return std::nullopt;
    }

    auto RoiSetupWindowControl::IsCurrentRoiDiffWithPreset(const QString& preset) const -> bool {
        if (!d->loadedPresets.contains(preset)) {
            return false;
        }

        auto loadedPreset = d->loadedPresets[preset];
        auto currentRois = d->GetWell(d->currentWellIndex)->rois;

        if (loadedPreset.size() != currentRois.size()) {
            return true;
        }

        std::sort(loadedPreset.begin(), loadedPreset.end(), [](const auto& a, const auto& b) {
            return *a < *b;
        });
        std::sort(currentRois.begin(), currentRois.end(), [](const auto& a, const auto& b) {
            return *a < *b;
        });

        for (int i = 0; i < loadedPreset.size(); ++i) {
            if (*loadedPreset[i] != *currentRois[i]) {
                return true;
            }
        }

        return false;
    }
}
