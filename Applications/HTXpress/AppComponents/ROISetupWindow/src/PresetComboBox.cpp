﻿#include <QAbstractItemView>

#include "PresetComboBox.h"

namespace HTXpress::AppComponents::RoiSetupWindow {
    struct PresetComboBox::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        Self* self{};
        bool different{};
        int32_t lastSelectedIndex{};
        const QString modifiedIndicator = "* ";

        auto UpdateText(const int32_t& index = -1) -> void;
    };

    PresetComboBox::PresetComboBox(QWidget* parent) : QComboBox(parent), d{std::make_unique<Impl>(this)} {
        addItem("");
        d->lastSelectedIndex = currentIndex();

        connect(view(), &QAbstractItemView::entered, this, &PresetComboBox::onItemHovered);
        connect(this, qOverload<int>(&PresetComboBox::currentIndexChanged), this, [this](int index) {
            d->different = false;
            d->UpdateText(d->lastSelectedIndex);
            d->lastSelectedIndex = index;
        });
    }

    PresetComboBox::~PresetComboBox() = default;

    auto PresetComboBox::SetRoiDiffWithCurrentPreset(bool diff) -> void {
        d->different = diff;
        d->UpdateText();
    }

    auto PresetComboBox::Impl::UpdateText(const int32_t& index) -> void {
        int32_t targetIndex{};

        if (index == -1) {
            targetIndex = self->currentIndex();
        } else {
            targetIndex = index;
        }

        if (different) {
            self->setItemText(targetIndex, modifiedIndicator + self->itemData(targetIndex).toString());
        } else {
            self->setItemText(targetIndex, self->itemData(targetIndex).toString());
        }
    }

    auto PresetComboBox::hidePopup() -> void {
        QComboBox::hidePopup();
        emit sigHidePopup();
    }

    void PresetComboBox::onItemHovered(const QModelIndex& index) {
        const auto hoveredIndex = index.row();
        const auto hoveredText = itemData(hoveredIndex).toString();
        emit sigHoveredText(hoveredText);
    }
}
