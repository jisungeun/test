﻿#pragma once
#include <QUndoCommand>
#include <QGraphicsScene>

#include "GraphicsRoiItem.h"

namespace HTXpress::AppComponents::RoiSetupWindow {
    class CommandMove final : public QUndoCommand {
    public:
        CommandMove(QGraphicsScene* scene, const QMap<GraphicsRoiItem*, QPointF>& oldPosItems, QUndoCommand* parent = nullptr);
        ~CommandMove() override;

        auto undo() -> void override;
        auto redo() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class CommandAdd final : public QUndoCommand {
    public:
        CommandAdd(QGraphicsScene* scene, QGraphicsItem* item, QUndoCommand* parent = nullptr);
        ~CommandAdd() override;

        auto undo() -> void override;
        auto redo() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class CommandDelete final : public QUndoCommand {
    public:
        CommandDelete(QGraphicsScene* scene, const QList<QGraphicsItem*>& items, QUndoCommand* parent = nullptr);
        ~CommandDelete() override;

        auto undo() -> void override;
        auto redo() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class CommandResize final : public QUndoCommand {
    public:
        CommandResize(GraphicsRoiItem* item, const QRectF& oldRect, QUndoCommand* parent = nullptr);
        ~CommandResize() override;

        auto undo() -> void override;
        auto redo() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class CommandNameChange final : public QUndoCommand {
    public:
        CommandNameChange(GraphicsRoiItem* item, const QString& oldName, QUndoCommand* parent = nullptr);
        ~CommandNameChange() override;

        auto undo() -> void override;
        auto redo() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class CommandPropertyUpdate final : public QUndoCommand {
    public:
        CommandPropertyUpdate(GraphicsRoiItem* item, const QString& oldName, const QPointF& oldPos, const QRectF& oldRect, QUndoCommand* parent = nullptr);
        ~CommandPropertyUpdate() override;

        auto undo() -> void override;
        auto redo() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class CommandApplyPreset final : public QUndoCommand {
    public:
        CommandApplyPreset(QGraphicsScene* scene, QList<GraphicsRoiItem*> prevItems, QList<GraphicsRoiItem*> currentItems, QUndoCommand* parent = nullptr);
        ~CommandApplyPreset() override;

        auto undo() -> void override;
        auto redo() -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
