﻿#pragma once
#include <QStyledItemDelegate>

#include <RoiSetupDefines.h>

namespace HTXpress::AppComponents::RoiSetupWindow {
    class DelegateDoubleSpinBox final : public QStyledItemDelegate {
        Q_OBJECT
    public:
        explicit DelegateDoubleSpinBox(const RoiSetupDefinitions::Range& range, QObject* parent = nullptr);
        ~DelegateDoubleSpinBox() override;

        auto createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const -> QWidget* override;

        auto setEditorData(QWidget* editor, const QModelIndex& index) const -> void override;
        auto setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const -> void override;

        auto updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
