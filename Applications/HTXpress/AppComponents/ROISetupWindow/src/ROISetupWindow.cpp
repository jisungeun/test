#define LOGGER_TAG "[RoiSetupWindow]"
#include <optional>

#include <QButtonGroup>
#include <QCloseEvent>
#include <QTimer>
#include <QFileDialog>

#include <TCLogger.h>
#include <MessageDialog.h>
#include <InputDialog.h>
#include "RoiSetupWindow.h"
#include "ui_RoiSetupWindow.h"
#include "RoiSetupWindowControl.h"
#include "EditView.h"
#include "RoiSetupHelper.h"
#include "TableView.h"
#include "PresetComboBox.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::RoiSetupWindow {

    struct RoiSetupWindow::Impl {
        explicit Impl(Self* self) : self(self) {
            ui.setupUi(self);
        }

        Self* self{};
        Ui::RoiSetupWindow ui{};
        RoiSetupWindowControl control{};
        QButtonGroup* roiButtons{};

        auto ApplyStylesheet() -> void;
        auto InitTools() -> void;
        auto InitEditView() -> void;
        auto InitTableView() -> void;
        auto InitWellComboBox() -> void;
        auto InitPresetList() -> void;

        auto Connect() -> void;
        auto Request(int button) -> void;
        auto GetPresetName(const QString& defaultName = "") const -> std::optional<QString>;
        auto ApplyPreset(const QList<RegionOfInterest::Pointer>& rois) -> void;
    };

    RoiSetupWindow::RoiSetupWindow(const RoiSetupVessel::Pointer& vessel, QWidget* parent) : QDialog(parent), d{std::make_unique<Impl>(this)} {
        setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

        d->control.SetVessel(vessel);

        d->ApplyStylesheet();
        d->InitTools();
        d->InitEditView();
        d->InitTableView();
        d->InitWellComboBox();
        d->InitPresetList();
        d->Connect();

        d->ui.chkRoiTextVisible->setChecked(true);
    }

    RoiSetupWindow::~RoiSetupWindow() {
    }

    auto RoiSetupWindow::GetROIs() const -> ROIs {
        return d->control.GetAllRois();
    }

    auto RoiSetupWindow::keyPressEvent(QKeyEvent* event) -> void {
        if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) {
            return;
        }

        switch (event->key()) {
            case Qt::Key_Escape: {
                d->ui.roiEditView->onReadyToDrawRoiItem(EditViewRequest::Pointer);
                return;
            }
            case Qt::Key_Delete: {
                d->ui.roiEditView->onDeleteSelectedRoiItems();
                return;
            }
            default:
                break;
        }

        if (event->modifiers() == Qt::ControlModifier) {
            d->Request(event->key());
        }

        QDialog::keyPressEvent(event);
    }

    auto RoiSetupWindow::accept() -> void {
        if (!d->control.AreAllROIsInDrawableArea()) {
            TC::MessageDialog::warning(this, tr("Failed to Apply"), tr("Some of the current ROI areas are out of the imaging area."));
            return;
        }

        const auto answer = TC::MessageDialog::question(this, 
                                                        tr("Apply Changes"), 
                                                        tr("Do you want to apply the changes?\n" 
                                                           "(Please press the Save Experiment button after closing the current dialog and before starting the experiment.)"), 
                                                        QDialogButtonBox::Yes | QDialogButtonBox::No, QDialogButtonBox::Yes);
        if (answer == QDialogButtonBox::Yes) {
            QDialog::accept();
        }
    }

    auto RoiSetupWindow::reject() -> void {
        const auto answer = TC::MessageDialog::question(this, 
                                                        tr("Discard Changes"), 
                                                        tr("Are you sure you want to discard the changes and close the dialog?"), 
                                                        QDialogButtonBox::Yes | QDialogButtonBox::No, QDialogButtonBox::No);
        if (answer == QDialogButtonBox::Yes) {
            QDialog::reject();
        }
    }

    auto RoiSetupWindow::onViewSelectionChanged(const QList<ROIIndex>& selectedROIs) -> void {
        d->ui.roiTable->blockSignals(true);
        d->ui.roiTable->clearSelection();
        d->ui.roiTable->SetSelectedRois(d->control.GetCurrentWellIndex(), selectedROIs);
        d->ui.roiTable->blockSignals(false);
    }

    void RoiSetupWindow::onMouseScenePosChanged(const QPointF& position) {
        const auto pos = RoiSetupHelper::GetWellPosFromScenePos(position);
        const auto txt = QString("X: %1, Y: %2").arg(pos.x, 0, 'f', 3).arg(pos.y, 0, 'f', 3);
        d->ui.labelCurrPos->setText(txt);
    }

    void RoiSetupWindow::onViewDrawingCompleted() {
        d->roiButtons->button(EditViewRequest::Pointer)->setChecked(true);
    }

    void RoiSetupWindow::onRoiNameVisibilityChanged(bool show) {
        d->ui.roiEditView->SetRoiNameVisible(show);
    }

    auto RoiSetupWindow::onTableSelectionChanged(const QList<QPair<WellIndex, ROIIndex>>& indexPairs) -> void {
        QList<ROIIndex> roiIndices;
        for (const auto& pair : indexPairs) {
            if (pair.first == d->control.GetCurrentWellIndex()) {
                roiIndices.push_back(pair.second);
            }
        }
        d->ui.roiEditView->blockSignals(true);
        d->ui.roiEditView->SelectRoiItems(roiIndices);
        d->ui.roiEditView->blockSignals(false);
    }

    auto RoiSetupWindow::onUpdate(const QList<RegionOfInterest::Pointer>& rois) -> void {
        d->control.UpdateRois(rois);
        const auto who = qobject_cast<QWidget*>(sender());
        if (!who) {
            return;
        }

        if (who == d->ui.roiEditView) {
            d->ui.roiTable->blockSignals(true);
            d->ui.roiTable->UpdateROIs(d->control.GetAllRois());
            d->ui.roiTable->blockSignals(false);
        }

        d->ui.comboPreset->SetRoiDiffWithCurrentPreset(d->control.IsCurrentRoiDiffWithPreset(d->ui.comboPreset->currentData().toString()));
    }

    void RoiSetupWindow::onRoiNameChanged(const WellIndex& wellIndex, const ROIIndex& roiIndex, const QString& name) {
        d->control.ChangeRoiName(wellIndex, roiIndex, name);
        if (wellIndex == d->control.GetCurrentWellIndex()) {
            d->ui.roiEditView->ChangeRoiName(roiIndex, name);
        }
        d->ui.comboPreset->SetRoiDiffWithCurrentPreset(d->control.IsCurrentRoiDiffWithPreset(d->ui.comboPreset->currentData().toString()));
    }

    void RoiSetupWindow::onRoiPosChanged(const WellIndex& wellIndex, const ROIIndex& roiIndex, const double& x, const double& y) {
        d->control.ChangeRoiPosition(wellIndex, roiIndex, x, y);
        if (wellIndex == d->control.GetCurrentWellIndex()) {
            d->ui.roiEditView->ChangeRoiPosition(roiIndex, x, y);
        }
        d->ui.comboPreset->SetRoiDiffWithCurrentPreset(d->control.IsCurrentRoiDiffWithPreset(d->ui.comboPreset->currentData().toString()));
    }

    void RoiSetupWindow::onRoiSizeChanged(const WellIndex& wellIndex, const ROIIndex& roiIndex, const double& w, const double& h) {
        d->control.ChangeRoiSize(wellIndex, roiIndex, w, h);
        if (wellIndex == d->control.GetCurrentWellIndex()) {
            d->ui.roiEditView->ChangeRoiSize(roiIndex, w, h);
        }
        d->ui.comboPreset->SetRoiDiffWithCurrentPreset(d->control.IsCurrentRoiDiffWithPreset(d->ui.comboPreset->currentData().toString()));
    }

    void RoiSetupWindow::onSaveButtonClicked() {
        const auto selectedPresetName = d->ui.comboPreset->currentData().toString();
        QString newPresetName{};

        if (!selectedPresetName.isEmpty()) {
            TC::MessageDialog msgBox;
            msgBox.SetTitle(tr("The file already exists"));
            msgBox.SetContent(tr("Do you want to overwrite the existing file '%1'?" 
                                 "\nYes - overwrite the existing ROI preset." 
                                 "\nNo - save as new ROI preset.").arg(selectedPresetName));
            msgBox.SetStandardButtons(QDialogButtonBox::Yes | QDialogButtonBox::No);
            msgBox.SetDefaultButton(QDialogButtonBox::Yes);
            msgBox.exec();
            const auto ret = msgBox.GetLastClickedButton();
            if (ret == QDialogButtonBox::Yes) {
                newPresetName = selectedPresetName;
            }
        }

        if (newPresetName.isEmpty()) {
            const auto presetName = d->GetPresetName();
            if (presetName) {
                newPresetName = *presetName;
            }
        }

        if (selectedPresetName != newPresetName) {
            if (-1 != d->ui.comboPreset->findData(newPresetName)) {
                TC::MessageDialog msgBox;
                msgBox.SetTitle(tr("The file already exists"));
                msgBox.SetContent(tr("Do you want to overwrite the existing file '%1'?").arg(newPresetName));
                msgBox.SetStandardButtons(QDialogButtonBox::Yes | QDialogButtonBox::Cancel);
                msgBox.SetDefaultButton(QDialogButtonBox::Yes);
                msgBox.exec();
                const auto ret = msgBox.GetLastClickedButton();

                if (ret == QDialogButtonBox::Cancel) {
                    return;
                }
            }
        }

        if (!newPresetName.isEmpty()) {
            const auto saveResult = d->control.SavePreset(newPresetName);
            const auto writeSuccess = saveResult.first;
            if (writeSuccess) {
                const auto msgBox = new TC::MessageDialog(this);
                msgBox->SetTitle(tr("Preset Saved"));
                msgBox->SetContent(tr("Preset saved successfully."));
                msgBox->SetStandardButtons(QDialogButtonBox::Ok);
                msgBox->SetDefaultButton(QDialogButtonBox::Ok);
                msgBox->setAttribute(Qt::WA_DeleteOnClose);
                QTimer::singleShot(1000, msgBox, &QMessageBox::accept);
                msgBox->exec();

                if (-1 == d->ui.comboPreset->findData(newPresetName)) {
                    d->ui.comboPreset->addItem(newPresetName, newPresetName);
                }

                const auto savedPreset = d->control.GetPresetRois(newPresetName);
                if (savedPreset) {
                    d->ui.comboPreset->SetRoiDiffWithCurrentPreset(d->control.IsCurrentRoiDiffWithPreset(newPresetName));
                }
            } else {
                const auto reason = saveResult.second;
                const auto answer = TC::MessageDialog::warning(this, 
                                                               tr("Save Error"), 
                                                               tr("%1\n" "Do you want to retry saving with a different name?").arg(reason), 
                                                               QDialogButtonBox::Retry | QDialogButtonBox::Close,
                                                               QDialogButtonBox::Retry);
                if (answer == QDialogButtonBox::Retry) {
                    onSaveButtonClicked();
                }
            }
        }
    }

    void RoiSetupWindow::onPresetSelected(int index) {
        const auto preset = d->ui.comboPreset->itemData(index).toString();
        const auto rois = d->control.GetPresetRois(preset);
        if (rois) {
            d->ApplyPreset(*rois);
        }
    }

    void RoiSetupWindow::onPresetHovered(const QString& preset) {
        const auto presetRois = d->control.GetPresetRois(preset);
        if (presetRois) {
            d->ui.roiEditView->HideCurrentRoiItems();
            d->ui.roiEditView->DeleteTempRoiItems();
            d->ui.roiEditView->SetTempRoiItemsFromPreset(*presetRois);
        } else {
            d->ui.roiEditView->HideCurrentRoiItems();
            d->ui.roiEditView->DeleteTempRoiItems();
        }
    }

    void RoiSetupWindow::onPresetPopupHide() {
        d->ui.roiEditView->DeleteTempRoiItems();
        d->ui.roiEditView->ShowCurrentRoiItems();
    }

    void RoiSetupWindow::onWellIndexChanged(const WellIndex& wellIndex) {
        if (wellIndex != d->control.GetCurrentWellIndex()) {
            d->control.SetCurrentWellIndex(wellIndex);
            d->InitEditView();
            d->InitTableView();
            d->ui.comboWell->blockSignals(true);
            d->ui.comboWell->setCurrentIndex(wellIndex);
            d->ui.comboWell->blockSignals(false);
        }
    }

    auto RoiSetupWindow::Impl::ApplyStylesheet() -> void {
        for (const auto& child : self->findChildren<QWidget*>()) {
            if (const auto button = qobject_cast<QPushButton*>(child)) {
                if (button->isCheckable()) {
                    button->setObjectName("bt-setup-toggle");
                } else {
                    button->setObjectName("bt-setup-light");
                }
            }
            if (const auto buttonBox = qobject_cast<QDialogButtonBox*>(child)) {
                for (const auto btn : buttonBox->buttons()) {
                    btn->setMinimumWidth(90);
                    btn->setObjectName("bt-setup-light");
                }
            } else if (const auto label = qobject_cast<QLabel*>(child)) {
                label->setObjectName("label-h2");
            } else if (const auto table = qobject_cast<QTableView*>(child)) {
                table->setStyleSheet(QString("QTableView{border-bottom: 0px;}"));
                table->verticalHeader()->setStyleSheet(QString("QHeaderView{border-bottom: 0px;}"));
                table->verticalHeader()->setDefaultAlignment(Qt::AlignCenter);
            } else if (const auto line = qobject_cast<QFrame*>(child)) {
                if (line->frameShape() == QFrame::VLine) {
                    line->setObjectName("line-divider");
                    line->setFixedWidth(1);
                } else if (line->frameShape() == QFrame::HLine) {
                    line->setObjectName("line-divider");
                    line->setFixedHeight(1);
                }
            }
        }
    }

    auto RoiSetupWindow::Impl::InitTools() -> void {
        roiButtons = new QButtonGroup(self);
        roiButtons->addButton(ui.btnPointer, EditViewRequest::Pointer);
        roiButtons->addButton(ui.btnEllipse, EditViewRequest::DrawEllipse);
        roiButtons->addButton(ui.btnRect, EditViewRequest::DrawRectangle);
        for (const auto& btn : roiButtons->buttons()) {
            btn->setCheckable(true);
        }

        ui.btnPointer->setVisible(false);
        ui.btnRect->setText("");
        ui.btnEllipse->setText("");
        ui.btnRect->setIcon(QIcon(":/icon/rectangle"));
        ui.btnEllipse->setIcon(QIcon(":/icon/ellipse"));

        ui.btnSave->setText("Save as a preset");
        ui.btnSave->setIcon(QIcon(":/icon/save"));

        ui.btnUndo->setText("");
        ui.btnRedo->setText("");
        ui.btnUndo->setIcon(QIcon(":/icon/undo"));
        ui.btnRedo->setIcon(QIcon(":/icon/redo"));

        roiButtons->button(EditViewRequest::Pointer)->setChecked(true);

        ui.btnEllipse->setShortcut(Qt::CTRL | Qt::Key_E);
        ui.btnRect->setShortcut(Qt::CTRL | Qt::Key_R);

        ui.chkRoiTextVisible->setShortcut(QKeySequence(Qt::Key_F8));
        ui.btnSave->setShortcut(QKeySequence::Save);
        ui.btnUndo->setShortcut(QKeySequence::Undo);
        ui.btnRedo->setShortcut(QKeySequence::Redo);

        ui.btnEllipse->setToolTip(tr("Add ellipse-shaped ROI (%1)").arg(ui.btnEllipse->shortcut().toString()));
        ui.btnRect->setToolTip(tr("Add rectangle-shaped ROI (%1)").arg(ui.btnRect->shortcut().toString()));
        ui.btnSave->setToolTip(tr("Save current ROI setting as a preset."));

        ui.closeButtons->button(QDialogButtonBox::Ok)->setText("Apply");
    }

    auto RoiSetupWindow::Impl::InitEditView() -> void {
        ui.roiEditView->ClearAll();
        ui.roiEditView->SetWellSize(control.GetCurrentWellShape(), control.GetWellRect());
        ui.roiEditView->SetImagingAreaSize(control.GetImagingAreaShape(), control.GetImagingAreaRect());
        ui.roiEditView->SetRoiItems(control.GetCurrentRois());
    }

    auto RoiSetupWindow::Impl::InitTableView() -> void {
        ui.roiTable->Init();
        ui.roiTable->SetCurrentWell(control.GetCurrentWellIndex());
        ui.roiTable->SetPosXRange(control.GetPosXRange());
        ui.roiTable->SetPosYRange(control.GetPosYRange());
        ui.roiTable->SetWidthRange(control.GetWidthRange());
        ui.roiTable->SetHeightRange(control.GetHeightRange());
        ui.roiTable->SetWellPositionNames(control.GetWellPositionNames());
        ui.roiTable->SetItemDelegates();
        ui.roiTable->UpdateROIs(control.GetAllRois());
        ui.roiTable->Sort();
    }

    auto RoiSetupWindow::Impl::InitWellComboBox() -> void {
        ui.comboWell->addItems(control.GetWellPositionNames().values());
    }

    auto RoiSetupWindow::Impl::InitPresetList() -> void {
        const auto presets = control.GetPresetNames();
        for (const auto& preset : presets) {
            ui.comboPreset->addItem(preset, preset);
        }
    }

    auto RoiSetupWindow::Impl::Connect() -> void {
        connect(ui.closeButtons, &QDialogButtonBox::accepted, self, &Self::accept);
        connect(ui.closeButtons, &QDialogButtonBox::rejected, self, &Self::reject);

        connect(ui.roiEditView, &EditView::sigSelectionChanged, self, &Self::onViewSelectionChanged);
        connect(ui.roiEditView, &EditView::sigUpdate, self, &Self::onUpdate);
        connect(ui.roiEditView, &EditView::sigUpdateScenePos, self, &Self::onMouseScenePosChanged);
        connect(ui.roiEditView, &EditView::sigDrawCompleted, self, &Self::onViewDrawingCompleted);

        connect(ui.chkRoiTextVisible, &QCheckBox::toggled, self, &Self::onRoiNameVisibilityChanged);

        connect(ui.roiTable, &TableView::sigSelectionChanged, self, &Self::onTableSelectionChanged);
        connect(ui.roiTable, &TableView::sigNameChanged, self, &Self::onRoiNameChanged);
        connect(ui.roiTable, &TableView::sigPosChanged, self, &Self::onRoiPosChanged);
        connect(ui.roiTable, &TableView::sigSizeChanged, self, &Self::onRoiSizeChanged);
        connect(ui.roiTable, &TableView::sigShowProperty, ui.roiEditView, &EditView::onShowProperty);
        connect(ui.roiTable, &TableView::sigChangeWellIndex, self, &Self::onWellIndexChanged);

        connect(ui.comboWell, qOverload<int>(&QComboBox::currentIndexChanged), self, &Self::onWellIndexChanged);

        connect(roiButtons, &QButtonGroup::idClicked, ui.roiEditView, &EditView::onReadyToDrawRoiItem);

        connect(ui.btnSave, &QPushButton::clicked, self, &Self::onSaveButtonClicked);

        connect(ui.comboPreset, qOverload<int>(&PresetComboBox::activated), self, &Self::onPresetSelected);
        connect(ui.comboPreset, &PresetComboBox::sigHoveredText, self, &Self::onPresetHovered);
        connect(ui.comboPreset, &PresetComboBox::sigHidePopup, self, &Self::onPresetPopupHide);

        connect(ui.btnUndo, &QPushButton::clicked, ui.roiEditView, &EditView::onUndo);
        connect(ui.btnRedo, &QPushButton::clicked, ui.roiEditView, &EditView::onRedo);
    }

    auto RoiSetupWindow::Impl::Request(int button) -> void {
        switch (button) {
            case Qt::Key_A: {
                ui.roiEditView->onSelectAll();
                break;
            }
            case Qt::Key_C: {
                ui.roiEditView->onCopy();
                break;
            }
            case Qt::Key_V: {
                ui.roiEditView->onPaste();
                break;
            }
            case Qt::Key_X: {
                ui.roiEditView->onCut();
                break;
            }
            case Qt::Key_Y: {
                ui.roiEditView->onRedo();
                break;
            }
            case Qt::Key_Z: {
                ui.roiEditView->onUndo();
                break;
            }
            case Qt::Key_Comma: {
                ui.roiEditView->onShowProperty();
                break;
            }
            default:
                break;
        }
    }

    auto RoiSetupWindow::Impl::GetPresetName(const QString& defaultName) const -> std::optional<QString> {
        bool ok{};
        const auto presetName = TC::InputDialog::getText(self, 
                                                         tr("Save ROI Preset"), 
                                                         tr("Enter a name for the ROI Preset:"), 
                                                         QLineEdit::Normal,
                                                         defaultName,
                                                         &ok);
        if (ok) {
            return presetName;
        }

        return std::nullopt;
    }

    auto RoiSetupWindow::Impl::ApplyPreset(const QList<RegionOfInterest::Pointer>& rois) -> void {
        ui.roiEditView->ApplyRoiPreset(rois);
    }
}
