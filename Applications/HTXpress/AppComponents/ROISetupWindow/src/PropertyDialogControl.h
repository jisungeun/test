﻿#pragma once
#include <memory>
#include <QString>
#include <QRectF>

#include "EditView.h"
#include "GraphicsRoiItem.h"

namespace HTXpress::AppComponents::RoiSetupWindow {
    class PropertyDialogControl {
    public:
        using Self = PropertyDialogControl;

        PropertyDialogControl();
        ~PropertyDialogControl();

        auto InitPreview(GraphicsRoiItem* item, EditView* view) -> void;
        auto GetViewPos() const -> QPoint;
        auto RemoveTempItem() -> void;

        auto GetIndex() const -> QString;
        auto GetShape() const -> QString;
        auto GetName() const -> QString;
        auto GetItemRect() const -> QRectF;
        auto GetItemPos() const -> QPointF;

        auto GetPosXRange() const -> QPair<double, double>;
        auto GetPosYRange() const -> QPair<double, double>;
        auto GetWidthRange() const -> QPair<double, double>;
        auto GetHeightRange() const -> QPair<double, double>;

        auto GetWellPosX() const -> double;
        auto GetWellPosY() const -> double;
        auto GetWidth() const -> double;
        auto GetHeight() const -> double;

        auto ChangeName(const QString& name) -> void;
        auto MoveItemX(const double& wellPosX) -> void;
        auto MoveItemY(const double& wellPosY) -> void;
        auto ChangeWidth(const double& w) -> void;
        auto ChangeHeight(const double& h) -> void;

        auto IsChanged() const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
