﻿#pragma once
#include <QGraphicsRectItem>

#include <RoiSetupDefines.h>

namespace HTXpress::AppComponents::RoiSetupWindow {
    class GraphicsGridItem final : public QGraphicsRectItem {
    public:
        using Self = GraphicsGridItem;
        enum {Type = UserType + 2};

        GraphicsGridItem(const RoiSetupDefinitions::ItemShape& shape, const QRectF& rect, QGraphicsItem* parent = nullptr);
        ~GraphicsGridItem() override;

        auto type() const -> int override;
        auto shape() const -> QPainterPath override;

        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
