﻿#include <QDoubleSpinBox>

#include "DelegateDoubleSpinBox.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::RoiSetupWindow {
    struct DelegateDoubleSpinBox::Impl {
        Range range{}; // first: min, second: max
    };

    DelegateDoubleSpinBox::DelegateDoubleSpinBox(const Range& range, QObject* parent) : QStyledItemDelegate(parent), d{std::make_unique<Impl>()} {
        d->range = range;
    }

    DelegateDoubleSpinBox::~DelegateDoubleSpinBox() {
    }

    auto DelegateDoubleSpinBox::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const -> QWidget* {
        Q_UNUSED(option)
        Q_UNUSED(index)
        const auto editor = new QDoubleSpinBox(parent);
        editor->setDecimals(3);
        editor->setRange(d->range.first, d->range.second);
        editor->setFrame(false);
        return editor;
    }

    auto DelegateDoubleSpinBox::setEditorData(QWidget* editor, const QModelIndex& index) const -> void {
        const auto value = index.model()->data(index, Qt::DisplayRole).toDouble();
        const auto spinBox = qobject_cast<QDoubleSpinBox*>(editor);
        spinBox->setValue(value);
    }

    auto DelegateDoubleSpinBox::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const -> void {
        const auto spinBox = qobject_cast<QDoubleSpinBox*>(editor);
        spinBox->interpretText();
        const auto value = spinBox->value();
        model->setData(index, value, Qt::EditRole);
    }

    auto DelegateDoubleSpinBox::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const -> void {
        Q_UNUSED(index)
        editor->setGeometry(option.rect);
    }
}
