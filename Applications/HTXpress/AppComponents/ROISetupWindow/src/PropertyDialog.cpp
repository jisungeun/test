﻿#include <QPushButton>
#include <MessageDialog.h>
#include <QKeyEvent>

#include "PropertyDialog.h"
#include "ui_PropertyDialog.h"
#include "RoiSetupHelper.h"
#include "PropertyDialogControl.h"

namespace HTXpress::AppComponents::RoiSetupWindow {
    struct PropertyDialog::Impl {
        explicit Impl(Self* self): self(self) {
            ui.setupUi(self);
        }

        Self* self{};
        Ui::PropertyDialog ui{};
        PropertyDialogControl control{};

        auto ApplyStylesheet() -> void;
    };

    auto PropertyDialog::Impl::ApplyStylesheet() -> void {
        for (const auto& child : self->findChildren<QWidget*>()) {
            if (auto button = qobject_cast<QPushButton*>(child)) {
                if (button->isCheckable()) {
                    button->setObjectName("bt-setup-toggle");
                }
                else {
                    button->setObjectName("bt-setup-light");
                }
            }
            if (auto buttonBox = qobject_cast<QDialogButtonBox*>(child)) {
                for (auto btn : buttonBox->buttons()) {
                    btn->setMinimumWidth(90);
                    btn->setObjectName("bt-setup-light");
                }
            }
            else if (auto label = qobject_cast<QLabel*>(child)) {
                if(label->objectName().contains("Title")) {
                    label->setObjectName("label-h2");
                }
                else if(label->objectName().contains("Sub")) {
                    label->setObjectName("label-h3");
                }
            }
            else if (auto line = qobject_cast<QFrame*>(child)) {
                if (line->frameShape() == QFrame::VLine) {
                    line->setObjectName("line-divider");
                    line->setFixedWidth(1);
                }
                else if (line->frameShape() == QFrame::HLine) {
                    line->setObjectName("line-divider");
                    line->setFixedHeight(1);
                }
            }
        }
    }

    PropertyDialog::PropertyDialog(GraphicsRoiItem* targetItem, EditView* previewCanvas, QWidget* parent) : QDialog(parent), d{std::make_unique<Impl>(this)} {
        setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

        d->control.InitPreview(targetItem, previewCanvas);
        d->ApplyStylesheet();

        d->ui.editIndex->setReadOnly(true);
        d->ui.editShape->setReadOnly(true);

        d->ui.editIndex->setText(d->control.GetIndex());
        d->ui.editShape->setText(d->control.GetShape());
        d->ui.editName->setText(d->control.GetName());
        d->ui.editName->setFocus();

        const auto rangeX = d->control.GetPosXRange();
        const auto rangeY = d->control.GetPosYRange();
        const auto rangeW = d->control.GetWidthRange();
        const auto rangeH = d->control.GetHeightRange();

        d->ui.spinX->setRange(rangeX.first, rangeX.second);
        d->ui.spinY->setRange(rangeY.first, rangeY.second);
        d->ui.spinW->setRange(rangeW.first, rangeW.second);
        d->ui.spinH->setRange(rangeH.first, rangeH.second);

        d->ui.spinX->setValue(d->control.GetWellPosX());
        d->ui.spinY->setValue(d->control.GetWellPosY());
        d->ui.spinW->setValue(d->control.GetWidth());
        d->ui.spinH->setValue(d->control.GetHeight());

        // init connect
        connect(d->ui.closeButtons, &QDialogButtonBox::accepted, this, &Self::accept);
        connect(d->ui.closeButtons, &QDialogButtonBox::rejected, this, &Self::reject);

        connect(d->ui.editName, &QLineEdit::textChanged, this, &Self::onChangeName);
        connect(d->ui.spinX, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &Self::onChangeX);
        connect(d->ui.spinY, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &Self::onChangeY);
        connect(d->ui.spinW, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &Self::onChangeW);
        connect(d->ui.spinH, qOverload<double>(&QDoubleSpinBox::valueChanged), this, &Self::onChangeH);
    }

    PropertyDialog::~PropertyDialog() {
    }

    auto PropertyDialog::GetProperty() const -> std::tuple<QString, QRectF, QPointF> {
        const auto name = d->control.GetName();
        const auto rect = d->control.GetItemRect();
        const auto pos = d->control.GetItemPos();

        return {name, rect, pos};
    }

    auto PropertyDialog::showEvent(QShowEvent* event) -> void {
        QDialog::showEvent(event);
        auto widgetPos = d->control.GetViewPos();
        move(widgetPos.x(), widgetPos.y());
    }

    auto PropertyDialog::keyPressEvent(QKeyEvent* event) -> void {
        if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) {
            return;
        }
        QDialog::keyPressEvent(event);
    }

    void PropertyDialog::accept() {
        const auto answer = TC::MessageDialog::question(this, tr("Apply Changes"), tr("Do you want to apply the changes?"), QDialogButtonBox::Yes | QDialogButtonBox::No, QDialogButtonBox::Yes);

        if (answer == QDialogButtonBox::Yes) {
            d->control.RemoveTempItem();
            QDialog::accept();
        }
    }

    void PropertyDialog::reject() {
        const auto answer = TC::MessageDialog::question(this, tr("Discard Changes"), tr("Are you sure you want to discard the changes?"), QDialogButtonBox::Yes | QDialogButtonBox::No, QDialogButtonBox::No);

        if (answer == QDialogButtonBox::Yes) {
            d->control.RemoveTempItem();
            QDialog::reject();
        }
    }

    void PropertyDialog::onChangeName(const QString& text) {
        d->control.ChangeName(text);
    }

    void PropertyDialog::onChangeX(double value) {
        d->control.MoveItemX(value);
    }

    void PropertyDialog::onChangeY(double value) {
        d->control.MoveItemY(value);
    }

    void PropertyDialog::onChangeW(double value) {
        d->control.ChangeWidth(value);
    }

    void PropertyDialog::onChangeH(double value) {
        d->control.ChangeHeight(value);
    }
}
