﻿#pragma once
#include <memory>

#include <QDialog>
#include "GraphicsRoiItem.h"
#include "EditView.h"

namespace HTXpress::AppComponents::RoiSetupWindow {
    class PropertyDialog final : public QDialog {
    public:
        using Self = PropertyDialog;

        PropertyDialog(GraphicsRoiItem* targetItem, EditView* previewCanvas, QWidget* parent = nullptr);
        ~PropertyDialog() override;

        auto GetProperty() const -> std::tuple<QString, QRectF, QPointF>;

    private:
        auto showEvent(QShowEvent* event) -> void override;
        auto keyPressEvent(QKeyEvent* event) -> void override;

    private slots:
        void accept() override;
        void reject() override;

        void onChangeName(const QString& text);
        void onChangeX(double value);
        void onChangeY(double value);
        void onChangeW(double value);
        void onChangeH(double value);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

    };
}