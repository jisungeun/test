﻿#include <QPainterPath>

#include "RoiSetupHelper.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::RoiSetupWindow {

    struct RoiSetupHelper::Impl {
        QPainterPath drawablePath{};
    };

    RoiSetupHelper::RoiSetupHelper() : d{std::make_unique<Impl>()} {}
    RoiSetupHelper::~RoiSetupHelper() = default;

    auto RoiSetupHelper::GetInstance() -> Pointer {
        static Pointer theInstance{};
        if (!theInstance) {
            theInstance.reset(new Self());
        }
        return theInstance;
    }

    auto RoiSetupHelper::SetDrawableArea(const ShapeRect& drawableArea) -> void {
        d->drawablePath.clear();
        if (drawableArea.shape == ItemShape::Ellipse) {
            d->drawablePath.addEllipse(drawableArea.rect);
        }
        else {
            d->drawablePath.addRect(drawableArea.rect);
        }
    }

    auto RoiSetupHelper::IsDrawableArea(const ShapeRect& area) const -> bool {
        QPainterPath areaPath;
        if (area.shape == ItemShape::Ellipse) {
            areaPath.addEllipse(area.rect);
        }
        else {
            areaPath.addRect(area.rect);
        }
        return d->drawablePath.contains(areaPath);
    }

    auto RoiSetupHelper::GetItemRect(const QPointF& center, const double& rx, const double& ry) -> QRectF {
        return {center.x() - rx, center.y() - ry, rx * 2, ry * 2};
    }

    auto RoiSetupHelper::GetWellPosFromScenePos(const QPointF& sceneCoordSystem) -> Position {
        return {sceneCoordSystem.x(), -sceneCoordSystem.y()};
    }

    auto RoiSetupHelper::GetScenePosFromWellPos(const Position& wellCoordSystem) -> QPointF {
        return {wellCoordSystem.x, -wellCoordSystem.y};
    }

    auto RoiSetupHelper::IsDrawableArea(const ShapeRect& drawable, const ShapeRect& roi) -> bool {
        QPainterPath drawablePath;
        QPainterPath roiPath;
        if (drawable.shape == ItemShape::Ellipse) {
            drawablePath.addEllipse(drawable.rect);
        }
        else {
            drawablePath.addRect(drawable.rect);
        }
        if (roi.shape == ItemShape::Rectangle) {
            roiPath.addEllipse(roi.rect);
        }
        else {
            roiPath.addRect(roi.rect);
        }
        return drawablePath.contains(roiPath);
    }
}
