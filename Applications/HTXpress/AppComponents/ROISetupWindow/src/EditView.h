﻿#pragma once
#include <QGraphicsView>
#include <QWidget>

#include <enum.h>

#include <RegionOfInterest.h>

#include "GraphicsRoiItem.h"

namespace HTXpress::AppComponents::RoiSetupWindow {
    BETTER_ENUM(EditViewRequest, int32_t, Pointer, DrawRectangle, DrawEllipse)

    class EditView final : public QGraphicsView {
        Q_OBJECT
    public:
        using Self = EditView;

        enum class MenuItem {
            Copy,
            Paste,
            Cut,
            Delete,
            ClearAll,
            AddRect,
            AddEllipse,
            Property
        };

        enum Zoom {
            ZoomIn,
            ZoomOut,
            ZoomFit
        };

        explicit EditView(QWidget* parent = nullptr);
        ~EditView() override;

        auto ClearAll() -> void;

        auto SetWellSize(const RoiSetupDefinitions::ItemShape& shape, const QRectF& rect) -> void;
        auto SetImagingAreaSize(const RoiSetupDefinitions::ItemShape& shape, const QRectF& rect) -> void;
        auto SetRoiItems(const QList<RoiSetupDefinitions::RegionOfInterest::Pointer>& rois) -> void;
        auto SetRoiNameVisible(bool visible) -> void;
        auto SelectRoiItems(const QList<RoiSetupDefinitions::ROIIndex>& roiIndices) -> void;

        auto ChangeRoiName(const RoiSetupDefinitions::ROIIndex& index, const QString& newName) -> void;
        auto ChangeRoiPosition(const RoiSetupDefinitions::ROIIndex& index, const double& x, const double& y) -> void;
        auto ChangeRoiSize(const RoiSetupDefinitions::ROIIndex& index, const double& w, const double& h) -> void;

        auto SetTempRoiItemsFromPreset(const QList<RoiSetupDefinitions::RegionOfInterest::Pointer>& presetInfo) -> void;
        auto DeleteTempRoiItems() -> void;
        auto ApplyRoiPreset(const QList<RoiSetupDefinitions::RegionOfInterest::Pointer>& rois) -> void;

        auto HideCurrentRoiItems() -> void;
        auto ShowCurrentRoiItems() -> void;

    private:
        auto mousePressEvent(QMouseEvent* event) -> void override;
        auto mouseReleaseEvent(QMouseEvent* event) -> void override;
        auto mouseMoveEvent(QMouseEvent* event) -> void override;
        auto mouseDoubleClickEvent(QMouseEvent* event) -> void override;
        auto wheelEvent(QWheelEvent* event) -> void override;
        auto contextMenuEvent(QContextMenuEvent* event) -> void override;
        auto resizeEvent(QResizeEvent* event) -> void override;
        auto scrollContentsBy(int dx, int dy) -> void override;
        auto drawForeground(QPainter* painter, const QRectF& rect) -> void override;
        auto showEvent(QShowEvent* event) -> void override;

    signals:
        void sigDrawCompleted();
        void sigUpdate(const QList<RoiSetupDefinitions::RegionOfInterest::Pointer>&);
        void sigUpdateScenePos(const QPointF& scenePos);
        void sigSelectionChanged(const QList<RoiSetupDefinitions::ROIIndex>& selectedRoiIndices);

    public slots:
        void onReadyToDrawRoiItem(int32_t shapeIndex);
        void onUndo();
        void onRedo();
        void onItemResized(const QRectF& oldRect);
        void onItemMoved(const QMap<GraphicsRoiItem*, QPointF>& movedItems);
        void onItemNameChanged(const QString& oldName);
        void onDeleteSelectedRoiItems();
        void onDeleteAll();
        void onCopy();
        void onCut();
        void onPaste();
        void onSelectAll();
        void onDeselectAll();
        void onZoom(int32_t zoomID);
        void onShowProperty();

    private slots:
        void onSelectionChanged();
        void onAddActionTriggered(QAction* action);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
