﻿#pragma once
#include <QRectF>

#include <RoiSetupDefines.h>

namespace HTXpress::AppComponents::RoiSetupWindow {
    class RoiSetupHelper final {
    public:
        using Self = RoiSetupHelper;
        using Pointer = std::shared_ptr<Self>;

        struct ShapeRect{
            RoiSetupDefinitions::ItemShape::_enumerated shape{};
            QRectF rect{};
        };

        ~RoiSetupHelper();

        static auto GetInstance() -> Pointer;

        auto SetDrawableArea(const ShapeRect& drawableArea) -> void;
        auto IsDrawableArea(const ShapeRect& area) const -> bool;

        static auto GetItemRect(const QPointF& center, const double& rx, const double& ry) -> QRectF;
        static auto GetWellPosFromScenePos(const QPointF& sceneCoordSystem) ->RoiSetupDefinitions::Position;
        static auto GetScenePosFromWellPos(const RoiSetupDefinitions::Position& wellCoordSystem) -> QPointF;
        static auto IsDrawableArea(const ShapeRect& drawable, const ShapeRect& roi) -> bool;

    private:
        RoiSetupHelper();
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
