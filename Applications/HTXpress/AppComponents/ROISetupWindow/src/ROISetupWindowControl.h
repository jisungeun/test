#pragma once
#include <memory>

#include <QRectF>

#include <RoiSetupDefines.h>
#include <RoiSetupVessel.h>
#include <RegionOfInterest.h>

namespace HTXpress::AppComponents::RoiSetupWindow {
    class RoiSetupWindowControl final {
    public:
        using Self = RoiSetupWindowControl;
        using Pointer = std::shared_ptr<Self>;

        RoiSetupWindowControl();
        ~RoiSetupWindowControl();

        auto SetVessel(const RoiSetupDefinitions::RoiSetupVessel::Pointer& vessel) -> void;

        auto SetCurrentWellIndex(const RoiSetupDefinitions::WellIndex& wellIndex) -> void;
        auto GetCurrentWellIndex() const -> RoiSetupDefinitions::WellIndex;

        auto UpdateRois(QList<RoiSetupDefinitions::RegionOfInterest::Pointer> rois) -> void;
        auto GetCurrentRois(const RoiSetupDefinitions::WellIndex& wellIndex) const -> QList<RoiSetupDefinitions::RegionOfInterest::Pointer>;
        auto GetCurrentRois() const -> QList<RoiSetupDefinitions::RegionOfInterest::Pointer>;
        auto GetAllRois() const ->RoiSetupDefinitions::ROIs;

        auto GetCurrentWellShape() const ->RoiSetupDefinitions::ItemShape;
        auto GetWellRect() const -> QRectF;

        auto GetImagingAreaShape() const ->RoiSetupDefinitions::ItemShape;
        auto GetImagingAreaRect() const -> QRectF;

        auto GetPosXRange() const -> RoiSetupDefinitions::Range;
        auto GetPosYRange() const -> RoiSetupDefinitions::Range;
        auto GetWidthRange() const ->RoiSetupDefinitions::Range;
        auto GetHeightRange() const ->RoiSetupDefinitions::Range;

        auto GetWellPositionNames() const -> QMap<RoiSetupDefinitions::WellIndex, QString>;
        auto GetWellCount() const -> int32_t;

        auto AreAllROIsInDrawableArea() const -> bool;

        auto ChangeRoiName(const RoiSetupDefinitions::WellIndex& wellIndex, const RoiSetupDefinitions::ROIIndex& roiIndex, const QString& name) -> void;
        auto ChangeRoiPosition(const RoiSetupDefinitions::WellIndex& wellIndex, const RoiSetupDefinitions::ROIIndex& roiIndex, const double& x, const double& y) -> void;
        auto ChangeRoiSize(const RoiSetupDefinitions::WellIndex& wellIndex, const RoiSetupDefinitions::ROIIndex& roiIndex, const double& w, const double& h) -> void;

        auto SavePreset(const QString& presetName) -> QPair<bool, QString>;
        auto LoadPresetRoiList(const QString& presetFullPath) const -> std::optional<QList<RoiSetupDefinitions::RegionOfInterest::Pointer>>;

        auto GetPresetFolderPath() const -> QString;
        auto GetPresetExtension() const -> QString;
        auto GetPresetNames() const -> QStringList;
        auto GetPresetRois(const QString& preset) const -> std::optional<QList<RoiSetupDefinitions::RegionOfInterest::Pointer>>;
        auto IsCurrentRoiDiffWithPreset(const QString& preset) const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
