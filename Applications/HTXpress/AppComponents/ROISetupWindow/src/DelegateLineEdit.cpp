﻿#include <QLineEdit>

#include "DelegateLineEdit.h"

namespace HTXpress::AppComponents::RoiSetupWindow {
    DelegateLineEdit::DelegateLineEdit(QObject* parent) : QStyledItemDelegate(parent){
    }

    DelegateLineEdit::~DelegateLineEdit() {
    }

    auto DelegateLineEdit::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const -> QWidget* {
        Q_UNUSED(option)
        Q_UNUSED(index)
        const auto editor = new QLineEdit(parent);
        editor->setFrame(false);
        editor->setMaxLength(20);
        return editor;
    }

    auto DelegateLineEdit::setEditorData(QWidget* editor, const QModelIndex& index) const -> void {
        const auto value = index.model()->data(index, Qt::DisplayRole).toString();
        const auto widget = qobject_cast<QLineEdit*>(editor);
        widget->setText(value);
    }

    auto DelegateLineEdit::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const -> void {
        const auto widget = qobject_cast<QLineEdit*>(editor);
        const auto value = widget->text();
        model->setData(index, value, Qt::EditRole);
    }

    auto DelegateLineEdit::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const -> void {
        Q_UNUSED(index)
        editor->setGeometry(option.rect);
    }
}
