﻿#pragma once
#include <memory>

#include <QComboBox>

namespace HTXpress::AppComponents::RoiSetupWindow {
    class PresetComboBox final : public QComboBox {
        Q_OBJECT
    public:
        using Self = PresetComboBox;

        explicit PresetComboBox(QWidget* parent = nullptr);
        ~PresetComboBox() override;

        auto SetRoiDiffWithCurrentPreset(bool diff) -> void;

    protected:
        void hidePopup() override;

    signals:
        void sigHoveredText(const QString&);
        void sigHidePopup();

    private slots:
        void onItemHovered(const QModelIndex& index);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
