﻿#pragma once
#include <memory>

#include <QWidget>
#include <QGraphicsView>

namespace HTXpress::AppComponents::RoiSetupWindow {
    class ZoomWidget final : public QWidget {
        Q_OBJECT
    public:
        using Self = ZoomWidget;

        explicit ZoomWidget(QWidget* parent = nullptr);
        ~ZoomWidget() override;

        auto UpdateGeometry(const QSize& viewSize) -> void;

    signals:
        void sigZoomInButtonClicked();
        void sigZoomOutButtonClicked();
        void sigZoomFitButtonClicked();

    private:
        auto enterEvent(QEvent* event) -> void override;
        auto leaveEvent(QEvent* event) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
