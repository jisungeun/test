﻿#include <QPainterPath>
#include <QGraphicsView>

#include "Ruler.h"

namespace HTXpress::AppComponents::RoiSetupWindow {
    namespace ruler {
        QColor colorBackground{QColor("#bbbbbb")};
    }

    struct Ruler::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        Self* self{};

        Qt::Orientation orientation{};
        QPoint viewPos{};
        QPair<QPoint, QPoint> roiPos{};
        QGraphicsView* view{};

        QColor colorTick{QColor("#172023")};
        QColor colorCursor{QColor("#ff0000")};
        QColor colorRoiOutline{QColor("#0000ff")};

        int32_t offset{};

        auto DrawTicker(QPainter* painter) -> void;
        auto DrawCurrPosition(QPainter* painter) -> void;
        auto DrawRoiOutline(QPainter* painter) -> void;
    };

    Ruler::Ruler(Qt::Orientation direction, QGraphicsView* view, QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>(this)} {
        d->view = view;
        d->orientation = direction;

        QFont f = font();
        f.setPixelSize(10);
        setFont(f);
    }

    Ruler::~Ruler() = default;

    auto Ruler::UpdateCursorPos(const QPoint& viewPos) -> void {
        d->viewPos = viewPos;
    }

    auto Ruler::UpdateSelectedRoiPos(const QPair<QPoint, QPoint>& pos) -> void {
        d->roiPos = pos;
    }

    auto Ruler::SetScenePosOffset(const double& offset) -> void {
        d->offset = static_cast<int32_t>(offset);
    }

    auto Ruler::paintEvent(QPaintEvent* event) -> void {
        Q_UNUSED(event)
        QPainter painter(this);
        painter.fillRect(rect(), ruler::colorBackground);
        d->DrawTicker(&painter);
        d->DrawCurrPosition(&painter);
        d->DrawRoiOutline(&painter);
    }

    auto Ruler::Impl::DrawTicker(QPainter* painter) -> void {
        painter->setPen(colorTick);
        QFontMetrics fm(self->font());

        if (orientation == Qt::Horizontal) {
            for (auto sceneX = -offset; sceneX <= offset; ++sceneX) {
                auto viewX = view->mapFromScene(QPointF{static_cast<double>(sceneX), 0}).x();
                if (sceneX % 5 == 0) {
                    const auto txt = QString::number(sceneX);
                    const auto w = fm.horizontalAdvance(txt);
                    painter->drawLine(viewX, 0, viewX, self->rect().height());
                    painter->drawText(viewX + 2, self->rect().top(), w, RulerSize, Qt::AlignLeft | Qt::AlignBottom, txt);
                }
                else {
                    painter->drawLine(viewX, 0, viewX, self->rect().height() / 3);
                }
            }
        }
        else if (orientation == Qt::Vertical) {
            for (auto sceneY = -offset; sceneY <= offset; ++sceneY) {
                auto viewY = view->mapFromScene(QPointF{0, static_cast<double>(sceneY)}).y();
                if (sceneY % 5 == 0) {
                    const auto txt = QString::number(-sceneY);
                    const auto w = fm.horizontalAdvance(txt);
                    const auto txtRect = QRect{-2 / w, -RulerSize / 2, w, RulerSize};
                    painter->drawLine(0, viewY, self->rect().width(), viewY);
                    painter->save();
                    painter->translate(8, (viewY + w / 2));
                    painter->rotate(90);
                    painter->drawText(txtRect, Qt::AlignLeft, txt);
                    painter->restore();
                }
                else {
                    painter->drawLine(0, viewY, self->rect().width() / 3, viewY);
                }
            }
        }
    }

    auto Ruler::Impl::DrawCurrPosition(QPainter* painter) -> void {
        painter->save();
        auto pen = painter->pen();
        pen.setColor(colorCursor);
        pen.setWidthF(painter->pen().widthF() * 2);
        painter->setPen(pen);

        if (orientation == Qt::Horizontal) {
            painter->drawLine(viewPos.x(), 0, viewPos.x(), self->rect().height());
        }
        else {
            painter->drawLine(0, viewPos.y(), self->rect().width(), viewPos.y());
        }
        painter->restore();
    }

    auto Ruler::Impl::DrawRoiOutline(QPainter* painter) -> void {
        painter->save();
        auto pen = painter->pen();
        pen.setColor(colorRoiOutline);
        pen.setWidthF(painter->pen().widthF() * 3);
        painter->setPen(pen);

        if (orientation == Qt::Horizontal) {
            painter->drawLine(roiPos.first.x(), 0, roiPos.first.x(), self->rect().height());
            painter->drawLine(roiPos.second.x(), 0, roiPos.second.x(), self->rect().height());
        }
        else {
            painter->drawLine(0, roiPos.first.y(), self->rect().width(), roiPos.first.y());
            painter->drawLine(0, roiPos.second.y(), self->rect().width(), roiPos.second.y());
        }
        painter->restore();
    }

    RulerLeftTopWidget::RulerLeftTopWidget(QWidget* parent) : QWidget(parent) {
    }

    auto RulerLeftTopWidget::paintEvent(QPaintEvent* event) -> void {
        Q_UNUSED(event)
        QPainter painter(this);
        painter.fillRect(rect(), ruler::colorBackground);
    }
}
