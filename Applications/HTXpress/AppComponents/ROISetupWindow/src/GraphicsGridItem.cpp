﻿#include <QPainter>

#include "GraphicsGridItem.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::RoiSetupWindow {

    struct GraphicsGridItem::Impl {
        ItemShape::_enumerated shape{};

        const int32_t step{1};

        struct PenProperty {
            double width{};
            QColor color{};
        } centerPen{}, normalPen{}, thinPen{};

        auto IsCircle(double w, double h) -> bool;
    };

    auto GraphicsGridItem::Impl::IsCircle(double w, double h) -> bool {
        return fabs(w - h) < std::numeric_limits<double>::epsilon();
    }

    GraphicsGridItem::GraphicsGridItem(const ItemShape& shape, const QRectF& rect, QGraphicsItem* parent) : QGraphicsRectItem(rect, parent), d{std::make_unique<Impl>()} {
        setFlag(ItemIsMovable, false);
        setFlag(ItemIsSelectable, false);
        d->shape = shape;
        d->centerPen = {0.025, Qt::white};
        d->normalPen = {0.015, Qt::lightGray};
        d->thinPen = {0.005, Qt::gray};
    }

    GraphicsGridItem::~GraphicsGridItem() {
    }

    auto GraphicsGridItem::type() const -> int {
        return Type;
    }

    auto GraphicsGridItem::shape() const -> QPainterPath {
        if (d->shape == ItemShape::Ellipse) {
            QPainterPath path;
            path.addEllipse(rect());
            return path;
        }
        return QGraphicsRectItem::shape();
    }

    auto GraphicsGridItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(widget)
        Q_UNUSED(option)
        for (auto x = static_cast<int32_t>(rect().left()); x <= rect().right(); x += d->step) {
            auto pen = QPen(d->thinPen.color, d->thinPen.width);
            if (x == static_cast<int32_t>(rect().center().x())) {
                pen = {d->centerPen.color, d->centerPen.width};
            }
            else if (x % 5 == 0) {
                pen = {d->normalPen.color, d->normalPen.width};
            }

            painter->setPen(pen);
            painter->drawLine(QPointF(x, rect().top()), QPointF(x, rect().bottom()));
        }

        for (auto y = static_cast<int32_t>(rect().top()); y <= rect().bottom(); y += d->step) {
            auto pen = QPen(d->thinPen.color, d->thinPen.width);
            if (y == static_cast<int32_t>(rect().center().y())) {
                pen = {d->centerPen.color, d->centerPen.width};
            }
            else if (y % 5 == 0) {
                pen = {d->normalPen.color, d->normalPen.width};
            }

            painter->setPen(pen);
            painter->drawLine(QPointF(rect().left(), y), QPointF(rect().right(), y));
        }
    }
}
