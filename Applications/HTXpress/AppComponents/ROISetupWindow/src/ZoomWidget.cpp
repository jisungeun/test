﻿#include <QGraphicsOpacityEffect>
#include <QToolButton>

#include "ZoomWidget.h"
#include "ui_ZoomWidget.h"

namespace HTXpress::AppComponents::RoiSetupWindow {
    struct ZoomWidget::Impl {
        explicit Impl(Self* self): self{self} {
            ui.setupUi(self);
        }

        Self* self{};
        Ui::ZoomWidget ui{};
        static constexpr int32_t margin{ 15 };
    };

    ZoomWidget::ZoomWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>(this)} {
        setWindowFlag(Qt::FramelessWindowHint);
        setAttribute(Qt::WA_NoSystemBackground);
        setAttribute(Qt::WA_TranslucentBackground);
        setAttribute(Qt::WA_PaintOnScreen);

        for (const auto& btn : findChildren<QToolButton*>()) {
            btn->setStyleSheet("QToolButton{border:none; background-color:transparent;}");
            btn->setText("");
            btn->setIconSize({24, 24});
            btn->setFixedSize({24, 24});
        }

        d->ui.btnZoomIn->setIcon(QIcon(":icon/zoomIn"));
        d->ui.btnZoomOut->setIcon(QIcon(":icon/zoomOut"));
        d->ui.btnZoomFit->setIcon(QIcon(":icon/zoomFit"));

        setFixedWidth(d->ui.btnZoomOut->width());

        auto* oe = new QGraphicsOpacityEffect(this);
        oe->setOpacity(0.2);
        setGraphicsEffect(oe);
        setAutoFillBackground(true);

        connect(d->ui.btnZoomIn, &QToolButton::clicked, this, &Self::sigZoomInButtonClicked);
        connect(d->ui.btnZoomOut, &QToolButton::clicked, this, &Self::sigZoomOutButtonClicked);
        connect(d->ui.btnZoomFit, &QToolButton::clicked, this, &Self::sigZoomFitButtonClicked);
    }

    ZoomWidget::~ZoomWidget() = default;

    auto ZoomWidget::UpdateGeometry(const QSize& viewSize) -> void {
        const auto myWidth = this->width();
        const auto myHeight = this->height();

        const auto viewWidth = viewSize.width();
        const auto viewHeight = viewSize.height();

        const auto left = viewWidth - myWidth - d->margin;
        const auto top = viewHeight - myHeight - d->margin;

        setGeometry(left, top, myWidth, myHeight);
    }

    void ZoomWidget::enterEvent(QEvent* event) {
        auto* oe = new QGraphicsOpacityEffect(this);
        oe->setOpacity(1.0);
        setGraphicsEffect(oe);
        QWidget::enterEvent(event);
    }

    void ZoomWidget::leaveEvent(QEvent* event) {
        auto* oe = new QGraphicsOpacityEffect(this);
        oe->setOpacity(0.2);
        setGraphicsEffect(oe);
        QWidget::leaveEvent(event);
    }
}
