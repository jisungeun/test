﻿#include "Commands.h"

namespace HTXpress::AppComponents::RoiSetupWindow {
    struct CommandMove::Impl {
        struct Positions {
            QPointF oldPos{};
            QPointF newPos{};
        };

        QMap<GraphicsRoiItem*, Positions> positions{};
        QGraphicsScene* scene{};
    };

    CommandMove::CommandMove(QGraphicsScene* scene, const QMap<GraphicsRoiItem*, QPointF>& oldPosItems, QUndoCommand* parent) : QUndoCommand(parent), d{std::make_unique<Impl>()} {
        d->scene = scene;
        for (auto it = oldPosItems.cbegin(); it != oldPosItems.cend(); ++it) {
            auto item = it.key();
            const auto oldPos = it.value();
            const auto newPos = it.key()->pos();
            d->positions[item] = {oldPos, newPos};
        }
    }

    CommandMove::~CommandMove() = default;

    auto CommandMove::undo() -> void {
        for (auto it = d->positions.cbegin(); it != d->positions.cend(); ++it) {
            it.key()->setPos(it.value().oldPos);
        }
        d->scene->update();
    }

    auto CommandMove::redo() -> void {
        for (auto it = d->positions.cbegin(); it != d->positions.cend(); ++it) {
            it.key()->setPos(it.value().newPos);
        }
        d->scene->update();
    }

    struct CommandAdd::Impl {
        QGraphicsScene* scene{};
        QGraphicsItem* item{};
    };

    CommandAdd::CommandAdd(QGraphicsScene* scene, QGraphicsItem* item, QUndoCommand* parent) : QUndoCommand(parent), d{std::make_unique<Impl>()} {
        d->scene = scene;
        d->item = item;
    }

    CommandAdd::~CommandAdd() = default;

    auto CommandAdd::undo() -> void {
        d->scene->removeItem(d->item);
    }

    auto CommandAdd::redo() -> void {
        d->scene->addItem(d->item);
        d->scene->clearSelection();
        d->scene->update();
    }

    struct CommandDelete::Impl {
        QGraphicsScene* scene{};
        QList<QGraphicsItem*> items{};
    };

    CommandDelete::CommandDelete(QGraphicsScene* scene, const QList<QGraphicsItem*>& items, QUndoCommand* parent) : QUndoCommand(parent), d{std::make_unique<Impl>()} {
        d->scene = scene;
        d->items = items;
    }

    CommandDelete::~CommandDelete() = default;

    auto CommandDelete::undo() -> void {
        for (const auto& a : d->items) {
            d->scene->addItem(a);
        }
        d->scene->clearSelection();
        d->scene->update();
    }

    auto CommandDelete::redo() -> void {
        for (const auto& a : d->items) {
            d->scene->removeItem(a);
        }
    }

    struct CommandResize::Impl {
        QRectF oldRect{};
        QRectF newRect{};
        GraphicsRoiItem* item;
    };

    CommandResize::CommandResize(GraphicsRoiItem* item, const QRectF& oldRect, QUndoCommand* parent) : QUndoCommand(parent), d{std::make_unique<Impl>()} {
        d->item = item;
        d->oldRect = oldRect;
        d->newRect = item->rect();
    }

    CommandResize::~CommandResize() = default;

    auto CommandResize::undo() -> void {
        d->item->setRect(d->oldRect);
        d->item->scene()->update();
    }

    auto CommandResize::redo() -> void {
        d->item->setRect(d->newRect);
        d->item->scene()->update();
    }

    struct CommandNameChange::Impl {
        GraphicsRoiItem* item{};
        QString newName{};
        QString oldName{};
    };

    CommandNameChange::CommandNameChange(GraphicsRoiItem* item, const QString& oldName, QUndoCommand* parent) : QUndoCommand(parent), d{std::make_unique<Impl>()} {
        d->item = item;
        d->newName = item->GetName();
        d->oldName = oldName;
    }

    CommandNameChange::~CommandNameChange() = default;

    auto CommandNameChange::undo() -> void {
        d->item->SetName(d->oldName);
        d->item->scene()->update();
    }

    auto CommandNameChange::redo() -> void {
        d->item->SetName(d->newName);
        d->item->scene()->update();
    }

    struct CommandPropertyUpdate::Impl {
        GraphicsRoiItem* item{};
        QString oldName{};
        QPointF oldPos{};
        QRectF oldRect{};
        QString newName{};
        QPointF newPos{};
        QRectF newRect{};
    };

    CommandPropertyUpdate::CommandPropertyUpdate(GraphicsRoiItem* item, const QString& oldName, const QPointF& oldPos, const QRectF& oldRect, QUndoCommand* parent) : QUndoCommand(parent), d{std::make_unique<Impl>()} {
        d->item = item;
        d->oldName = oldName;
        d->oldPos = oldPos;
        d->oldRect = oldRect;

        d->newName = item->GetName();
        d->newPos = item->pos();
        d->newRect = item->rect();
    }

    CommandPropertyUpdate::~CommandPropertyUpdate() {
    }

    auto CommandPropertyUpdate::undo() -> void {
        d->item->setRect(d->oldRect);
        d->item->setPos(d->oldPos);
        d->item->SetName(d->oldName);
    }

    auto CommandPropertyUpdate::redo() -> void {
        d->item->setRect(d->newRect);
        d->item->setPos(d->newPos);
        d->item->SetName(d->newName);
    }

    struct CommandApplyPreset::Impl {
        QGraphicsScene* scene{};
        QList<GraphicsRoiItem*> currItems{};
        QList<GraphicsRoiItem*> prevItems{};
    };

    CommandApplyPreset::CommandApplyPreset(QGraphicsScene* scene, QList<GraphicsRoiItem*> prevItems, QList<GraphicsRoiItem*> currentItems, QUndoCommand* parent) : QUndoCommand(parent), d{std::make_unique<Impl>()} {
        d->scene = scene;
        d->currItems = currentItems;
        d->prevItems = prevItems;
    }

    CommandApplyPreset::~CommandApplyPreset() = default;

    void CommandApplyPreset::undo() {
        for (const auto& a : d->currItems) {
            d->scene->removeItem(a);
        }

        for (const auto& a : d->prevItems) {
            d->scene->addItem(a);
        }

        d->scene->clearSelection();
        d->scene->update();
    }

    void CommandApplyPreset::redo() {
        for (const auto& a : d->prevItems) {
            d->scene->removeItem(a);
        }

        for (const auto& a : d->currItems) {
            d->scene->addItem(a);
        }

        d->scene->clearSelection();
        d->scene->update();
    }
}
