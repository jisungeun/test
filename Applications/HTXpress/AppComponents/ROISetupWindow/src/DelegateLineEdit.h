﻿#pragma once
#include <QStyledItemDelegate>

// just keep showing previous value when editing
namespace HTXpress::AppComponents::RoiSetupWindow {
    class DelegateLineEdit final : public QStyledItemDelegate {
    public:
        DelegateLineEdit(QObject* parent = nullptr);
        ~DelegateLineEdit() override;

        auto createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const -> QWidget* override;

        auto setEditorData(QWidget* editor, const QModelIndex& index) const -> void override;
        auto setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const -> void override;

        auto updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const -> void override;
    };
}
