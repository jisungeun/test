#include <QGraphicsSceneMouseEvent>
#include <QPainter>

#include "EditScene.h"
#include "GraphicsRoiItem.h"
#include "GraphicsGridItem.h"

using namespace HTXpress::AppComponents::RoiSetupDefinitions;

namespace HTXpress::AppComponents::RoiSetupWindow {
    struct EditScene::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        Self* self{};
        QList<GraphicsRoiItem*> selectedItems{};
        QMap<GraphicsRoiItem*, QPointF> oldPositions{};

        bool pressed{};

        auto Update() const -> void;
        auto SaveOldPos() -> void;
    };

    EditScene::EditScene(QObject* parent) : QGraphicsScene(parent), d{std::make_unique<Impl>(this)} {
        connect(this, &Self::selectionChanged, this, &Self::onSelectionChanged);
    }

    EditScene::~EditScene() {
        blockSignals(true);
        clearSelection();
    }

    auto EditScene::GetTopLeft() const -> QPointF {
        for (const auto& item : items()) {
            if (item->type() == GraphicsGridItem::Type) {
                return item->sceneBoundingRect().topLeft();
            }
        }
        return sceneRect().topLeft();
    }

    auto EditScene::GetSelectedRoiItems() const -> QList<GraphicsRoiItem*> {
        return d->selectedItems;
    }

    auto EditScene::GetRoiItem(const ROIIndex& index) -> GraphicsRoiItem* {
        for (const auto& roiItem : d->selectedItems) {
            if (roiItem->GetIndex() == index) {
                return roiItem;
            }
        }
        return nullptr;
    }

    auto EditScene::GetAllRoiItems() const -> QList<GraphicsRoiItem*> {
        QList<GraphicsRoiItem*> allRoiItems;
        for (const auto& item : items()) {
            if (item->type() == GraphicsRoiItem::Type) {
                allRoiItems.push_back(qgraphicsitem_cast<GraphicsRoiItem*>(item));
            }
        }
        return allRoiItems;
    }

    auto EditScene::mousePressEvent(QGraphicsSceneMouseEvent* event) -> void {
        if (event->button() == Qt::LeftButton) {
            d->SaveOldPos();
            d->pressed = true;
        }
        QGraphicsScene::mousePressEvent(event);
    }

    auto EditScene::mouseReleaseEvent(QGraphicsSceneMouseEvent* event) -> void {
        if (d->pressed) {
            d->Update();
        }
        d->pressed = false;
        QGraphicsScene::mouseReleaseEvent(event);
    }

    void EditScene::onSelectionChanged() {
        d->oldPositions.clear();
        d->selectedItems.clear();

        QList<GraphicsRoiItem*> roiItems;
        int count{1};
        for (const auto& item : selectedItems()) {
            if (item->type() == GraphicsRoiItem::Type) {
                const auto roi = qgraphicsitem_cast<GraphicsRoiItem*>(item);
                roiItems.push_back(roi);
            }
            count++;
        }

        if (roiItems.isEmpty()) {
            return;
        }

        d->selectedItems = roiItems;

        d->SaveOldPos();
    }

    auto EditScene::Impl::Update() const -> void {
        if (!oldPositions.isEmpty()) {
            QMap<GraphicsRoiItem*, QPointF> movedItems{};

            for (auto it = oldPositions.cbegin(); it != oldPositions.cend(); ++it) {
                const auto oldPos = it.value();
                const auto newPos = it.key()->pos();
                if (oldPos != newPos) {
                    movedItems[it.key()] = oldPos;
                }
            }

            if (!movedItems.isEmpty()) {
                emit self->sigItemsMoved(movedItems);
            }
        }
    }

    auto EditScene::Impl::SaveOldPos() -> void {
        for (const auto& roi : selectedItems) {
            oldPositions[roi] = roi->pos();
        }
    }
}
