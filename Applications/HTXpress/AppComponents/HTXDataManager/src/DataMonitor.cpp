#include "DataMonitor.h"
#include "TCFDataRepo.h"
#include "DataMisc.h"

#include <windows.h>
#include <QDir>
#include <strsafe.h>

using HTXpress::AppComponents::ProcessedDataDefinition::TCFProcessingStatus;

namespace HTXpress::AppComponents::HTXDataManager {
    #define BUFFER_SIZE 2048

    const int EVENT_INDEX_FILECHANGE = 0;
    const int EVENT_INDEX_TERMINATE = 1;

    struct DataMonitor::Impl {
        bool canceled = false;
        QString watchPath;
        HANDLE handle[2];
    };

    DataMonitor::DataMonitor() : d{ new Impl } {
        d->handle[EVENT_INDEX_FILECHANGE] = INVALID_HANDLE_VALUE;
        d->handle[EVENT_INDEX_TERMINATE] = INVALID_HANDLE_VALUE;
    }

    DataMonitor::~DataMonitor() {
        if (INVALID_HANDLE_VALUE != d->handle[EVENT_INDEX_TERMINATE]) {
            CloseHandle(d->handle[EVENT_INDEX_TERMINATE]);
            d->handle[EVENT_INDEX_TERMINATE] = INVALID_HANDLE_VALUE;
        }

        if (INVALID_HANDLE_VALUE != d->handle[EVENT_INDEX_FILECHANGE]) {
            CloseHandle(d->handle[EVENT_INDEX_FILECHANGE]);
            d->handle[EVENT_INDEX_FILECHANGE] = INVALID_HANDLE_VALUE;
        }
    }

    auto DataMonitor::Cancel()->void {
        d->canceled = true;

        if (INVALID_HANDLE_VALUE != d->handle[EVENT_INDEX_TERMINATE]) {
            ::SetEvent(d->handle[EVENT_INDEX_TERMINATE]);
        }
    }

    auto DataMonitor::SetWatchDirectory(const QString& path)->void {
        d->watchPath = path;
    }

    auto DataMonitor::GetWatchDirectory()->QString {
        return d->watchPath;
    }

    void DataMonitor::run() {
        HANDLE monitoredDirHandle = ::CreateFileW(d->watchPath.toStdWString().c_str(),
            FILE_LIST_DIRECTORY,
            FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
            NULL, OPEN_EXISTING,
            FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED, NULL);

        if (INVALID_HANDLE_VALUE == monitoredDirHandle) {
            // error Failed to call CrateFile function
            return;
        }

        OVERLAPPED overlapped;
        memset(&overlapped, NULL, sizeof(overlapped));
        d->handle[EVENT_INDEX_FILECHANGE] = CreateEvent(NULL, FALSE, FALSE, NULL);
        d->handle[EVENT_INDEX_TERMINATE] = CreateEvent(NULL, FALSE, FALSE, NULL);
        overlapped.hEvent = d->handle[EVENT_INDEX_FILECHANGE];

        char buffer[BUFFER_SIZE];
        DWORD bytesRead = 0;
        bool bStop = false;

        DWORD notifyFilter = FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_SIZE | FILE_NOTIFY_CHANGE_DIR_NAME;

        while (false == d->canceled || true == this->isInterruptionRequested()) {
            if (FALSE == ::ReadDirectoryChangesW(monitoredDirHandle, buffer, BUFFER_SIZE, TRUE, notifyFilter, &bytesRead, &overlapped, NULL)) {
                break;
            }

            DWORD waitRes = ::WaitForMultipleObjects(2, d->handle, FALSE, INFINITE);

            switch (waitRes) {
            case EVENT_INDEX_FILECHANGE: {
                if (TRUE == ::GetOverlappedResult(monitoredDirHandle, &overlapped, &bytesRead, TRUE)) {
                    auto fileNotifyInfo = (PFILE_NOTIFY_INFORMATION)buffer;

                    char notifyFilename[BUFFER_SIZE] = { 0 };
                    int nCnt = WideCharToMultiByte(CP_UTF8, NULL, fileNotifyInfo->FileName, fileNotifyInfo->FileNameLength / sizeof(WCHAR), notifyFilename, sizeof(notifyFilename) / sizeof(char), NULL, NULL);
                    notifyFilename[nCnt] = '\0';

                    auto name = QString::fromUtf8(notifyFilename);
                    name.replace("\\", "/");
                    auto filePath = QString("%1/%2").arg(d->watchPath).arg(name);
                    filePath.replace("\\", "/");

                    const auto parentFolder = QFileInfo(filePath).dir().path();
                    // watched config.dat
                    if (QFileInfo(name).fileName().contains("config.dat", Qt::CaseInsensitive)) {
                        if (FILE_ACTION_ADDED == fileNotifyInfo->Action) {
                            QFileInfo fileInfo(filePath);
                            auto dirName = fileInfo.dir().dirName();
                            auto tcf = dirName + ".TCF";
                            auto tcfPath = QDir::cleanPath(fileInfo.dir().path() + QDir::separator() + tcf);

                            if (false == QFileInfo(tcfPath).exists()) {
                                emit sigDataAdded(tcfPath, +TCFProcessingStatus::NotYet);
                            }
                        }
                    }
                    // watched tcf
                    else if (0 == DataMisc::GetTcfExtension(name).compare("tcf", Qt::CaseInsensitive)) {
                        if (FILE_ACTION_ADDED == fileNotifyInfo->Action) {
                            // if exist config.dat file, update status
                            QFileInfo fileInfo(filePath);
                            auto fileName = DataMisc::GetTcfBaseName(name);
                            auto dirName = fileInfo.dir().dirName();

                            if (fileName == dirName) {
                                auto configPath = QDir::cleanPath(fileInfo.dir().path() + QDir::separator() + "config.dat");
                                if (QFileInfo(configPath).exists()) {
                                    emit sigDataModified(filePath, +TCFProcessingStatus::Completed);
                                    break;
                                }
                            }

                            emit sigDataAdded(filePath, +TCFProcessingStatus::Completed);
                        }
                        else if (FILE_ACTION_REMOVED == fileNotifyInfo->Action) {
                            emit sigDataRemoved(filePath);
                        }
                        else if (FILE_ACTION_MODIFIED == fileNotifyInfo->Action) {
                            emit sigDataModified(filePath, +TCFProcessingStatus::Unknown);
                        }
                    }
                    else if (parentFolder == d->watchPath) {
                        if (FILE_ACTION_REMOVED == fileNotifyInfo->Action) {
                            const auto tcf = name + ".TCF";
                            const auto tcfPath = QDir::cleanPath(filePath + QDir::separator() + tcf);
                            emit sigDataRootFolderDeleted(tcfPath);
                        }
                    }
                }
                break;
            }
            default:
                bStop = true;
                break;
            }

            if (true == bStop)
                break;
        }

        ::CloseHandle(d->handle[EVENT_INDEX_TERMINATE]);
        d->handle[EVENT_INDEX_TERMINATE] = INVALID_HANDLE_VALUE;

        ::CloseHandle(d->handle[EVENT_INDEX_FILECHANGE]);
        d->handle[EVENT_INDEX_FILECHANGE] = INVALID_HANDLE_VALUE;

        ::CloseHandle(monitoredDirHandle);
    }
}


