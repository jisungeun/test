#include "IHTXDataManagerUpdater.h"
#include <HTXDataManager.h>

namespace HTXpress::AppComponents::HTXDataManager {
    IHTXDataManagerUpdater::IHTXDataManagerUpdater() {
        HTXDataManager::GetInstance()->RegisterUpdater(this);
    }

    IHTXDataManagerUpdater::~IHTXDataManagerUpdater() {
        HTXDataManager::GetInstance()->DeregisterUpdater(this);
    }
}