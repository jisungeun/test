#include "DataMisc.h"

#include <QFileInfo>
#include <QStringList>
#include <QRegularExpression>

#include <System.h>

namespace HTXpress::AppComponents::HTXDataManager {
    struct DataMisc::Impl {
        static const QString extension;

        static auto SplitPath(const QString& path) -> QStringList;
        static auto SplitTcfBaseName(const QString& path) -> QStringList;
        static auto GetElement(const QStringList& list, PathIndex index) -> QString;
        static auto GetElement(const QStringList& list, TcfNameIndex index) -> QString;
    };

    const QString DataMisc::Impl::extension = ".TCF";

    DataMisc::DataMisc() : d{std::make_unique<Impl>()} {
    }

    DataMisc::~DataMisc() = default;

    auto DataMisc::Impl::SplitPath(const QString& path) -> QStringList {
        const auto rootPath = AppEntity::System::GetSystemConfig()->GetDataDir();
        const auto relativePath = path.mid(rootPath.length() + 1); // 1을 더하는 이유: 경로 구분자('/') 길이때문
        return relativePath.split('/');
    }

    auto DataMisc::Impl::SplitTcfBaseName(const QString& path) -> QStringList {
        const auto dataFile = GetTcfBaseName(path);
        return dataFile.split('.');
    }

    auto DataMisc::Impl::GetElement(const QStringList& list, TcfNameIndex index) -> QString {
        if (list.size() > static_cast<int>(index)) {
            return list.at(static_cast<int>(index));
        }
        return {};
    }

    auto DataMisc::Impl::GetElement(const QStringList& list, PathIndex index) -> QString {
        if (list.size() > static_cast<int>(index)) {
            return list.at(static_cast<int>(index));
        }
        return {};
    }

    auto DataMisc::GetUserName(const QString& path) -> QString {
        const auto elements = Impl::SplitPath(path);
        return Impl::GetElement(elements, PathIndex::User);
    }

    auto DataMisc::GetTcfExtension(const QString& fileName) -> QString {
        const QFileInfo fileInfo(fileName);
        return fileInfo.suffix();
    }

    auto DataMisc::GetTcfBaseName(const QString& path) -> QString {
        auto name = path.split("/").last();
        name.chop(4);
        return name;
    }

    auto DataMisc::GetTcfTypeInfo(const QString& path) -> QString {
        const auto elements = Impl::SplitTcfBaseName(path);
        return Impl::GetElement(elements, TcfNameIndex::Type);
    }

    auto DataMisc::GetProjectName(const QString& path) -> QString {
        const auto elements = Impl::SplitPath(path);
        return Impl::GetElement(elements, PathIndex::Project);
    }

    auto DataMisc::GetExperimentName(const QString& path) -> QString {
        const auto elements = Impl::SplitPath(path);
        return Impl::GetElement(elements, PathIndex::Experiment);
    }

    auto DataMisc::GetSpecimenName(const QString& path) -> QString {
        const auto elements = Impl::SplitTcfBaseName(path);
        return Impl::GetElement(elements, TcfNameIndex::Specimen);
    }

    auto DataMisc::GetWellName(const QString& path) -> QString {
        const auto elements = Impl::SplitTcfBaseName(path);
        return Impl::GetElement(elements, TcfNameIndex::Well);
    }

    auto DataMisc::GetRawDataGeneratedDate(const QString& fileFullPath) -> QString {
        const auto elements = Impl::SplitTcfBaseName(fileFullPath);
        return Impl::GetElement(elements, TcfNameIndex::Date);
    }

    auto DataMisc::GetRawDataGeneratedTime(const QString& fileFullPath) -> QString {
        const auto elements = Impl::SplitTcfBaseName(fileFullPath);
        return Impl::GetElement(elements, TcfNameIndex::Time);
    }

    auto DataMisc::GetDataFolderPath(const QString& fileFullPath) -> QString {
        const QFileInfo fileInfo(fileFullPath);
        return fileInfo.absolutePath();
    }

    auto DataMisc::GetTimelapsePointID(const QString& fileFullPath) -> QString {
        const auto re = QRegularExpression("[T](\\d+)");
        auto typeID = GetTcfTypeInfo(fileFullPath);
        return typeID.remove(re);
    }
}
