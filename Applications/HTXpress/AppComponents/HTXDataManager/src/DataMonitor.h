#pragma once
#include <memory>
#include <QThread>
#include <QString>
#include <TCFData.h>

namespace HTXpress::AppComponents::HTXDataManager {
    class DataMonitor : public QThread {
		Q_OBJECT

	public:
		DataMonitor();
		~DataMonitor() override;

		auto Cancel()->void;

		auto SetWatchDirectory(const QString& path)->void;
		auto GetWatchDirectory()->QString;

    signals:
		void sigDataAdded(QString fileFullPath, int status);
		void sigDataRemoved(QString fileFullPath);
		void sigDataModified(QString fileFullPath, int status);
		void sigDataRootFolderDeleted(QString fileFullPath);

	protected:
		void run() override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}