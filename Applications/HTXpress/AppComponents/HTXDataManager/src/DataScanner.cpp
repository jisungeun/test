#include "DataScanner.h"
#include "TCFDataRepo.h"
#include "DataMisc.h"

#include <QDirIterator>
#include <System.h>

using HTXpress::AppComponents::ProcessedDataDefinition::TCFProcessingStatus;

namespace HTXpress::AppComponents::HTXDataManager {
    struct DataScanner::Impl {
        QString path{};
    };

    DataScanner::DataScanner() : d{std::make_unique<Impl>()} {
    }

    DataScanner::~DataScanner() {
    }

    auto DataScanner::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto DataScanner::GetPath() const -> QString {
        return d->path;
    }

    void DataScanner::run() {
        if (d->path.isEmpty())
            return;

        ScanFolder(d->path);
    }

    auto DataScanner::ScanFolder(const QString& path) -> bool {
        if (path.isEmpty()) {
            return false;
        }

        const QDir expDir(path);
        if (false == expDir.exists()) {
            return false;
        }

        QFileInfoList expEntries = expDir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
        
        int count{};
        int totalCount = expEntries.size();

        emit sigSetTotalCount(totalCount);

        for (const auto& expEntry : expEntries) {
            QDir acqDir(expEntry.absoluteFilePath());
            QFileInfoList acqEntries = acqDir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot);

            for (const auto& acqEntry : acqEntries) {
                if (isInterruptionRequested()) {
                    break;
                }

                const auto fileName = acqEntry.fileName();

                if (0 == fileName.compare("config.dat", Qt::CaseInsensitive)) {
                    const auto acqName = acqDir.dirName();
                    const auto tcf = acqName + ".TCF";
                    const auto tcfFullPath = QDir::cleanPath(acqDir.path() + QDir::separator() + tcf);

                    TCFProcessingStatus tcfStatus = TCFProcessingStatus::NotYet;
                    if (QFileInfo(tcfFullPath).exists()) {
                        tcfStatus = TCFProcessingStatus::Completed;
                    }
                    emit sigDataScanned(tcfFullPath, tcfStatus._to_integral());
                } else if (0 == DataMisc::GetTcfExtension(fileName).compare("TCF", Qt::CaseInsensitive)) {
                    const auto tcfFullPath = QDir::cleanPath(acqDir.path() + QDir::separator() + fileName);
                    emit sigDataScanned(tcfFullPath, +TCFProcessingStatus::Completed);
                }
            }

            count++;
            emit sigSetCurrentCount(count);
        }

        return true;
    }
}
