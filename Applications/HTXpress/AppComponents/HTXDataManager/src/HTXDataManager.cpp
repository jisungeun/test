#define LOGGER_TAG "[HTXDataManager]"

#include "HTXDataManager.h"

#include <TCFDataRepo.h>
#include <ProgressDialog.h>

#include "DataScanner.h"
#include "DataMonitor.h"
#include "DataMisc.h"
#include "System.h"

#include <QVariant>

using HTXpress::AppComponents::ProcessedDataDefinition::TCFDataRepo;
using HTXpress::AppComponents::ProcessedDataDefinition::TCFData;
using HTXpress::AppComponents::ProcessedDataDefinition::TCFProcessingStatus;

namespace HTXpress::AppComponents::HTXDataManager {
    struct HTXDataManager::Impl {
        DataScanner* scanner{ nullptr };
        QList<DataScanner*> scanners;
        QList<DataMonitor*> monitors;

        QList<IHTXDataManagerUpdater*> updaters;

        auto GenerateExperimentPath(const QString& user, const QString& project, const QString& experiment)->QString;
    };

    auto HTXDataManager::Impl::GenerateExperimentPath(const QString& user, const QString& project, const QString& experiment)->QString {
        return AppEntity::System::GetInstance()->GetSystemConfig()->GetDataDir() + "/" + user + "/" + project + "/" + experiment;
    }

    HTXDataManager::HTXDataManager() : d{ new Impl } {
        d->scanner = new DataScanner;
        connect(d->scanner, SIGNAL(sigDataScanned(QString, int)), this, SLOT(onAddData(QString, int)));
    }

    HTXDataManager::~HTXDataManager() {
    }

    auto HTXDataManager::GetInstance() -> Pointer {
        static Pointer theInstance{ new HTXDataManager() };
        return theInstance;
    }

    auto HTXDataManager::AppendScanExperiment(const QString& user, const QString& project, const QString& experiment)->void {
        if (user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;

        const auto path = d->GenerateExperimentPath(user, project, experiment);
        if (path.isEmpty()) return;

        AppendScanner(path, user, project, experiment);
    }

    auto HTXDataManager::RemoveScanExperiment(const QString& user, const QString& project, const QString& experiment)->void {
        if (user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;

        const auto path = d->GenerateExperimentPath(user, project, experiment);
        if (path.isEmpty()) return;

        RemoveScanner(path);
    }

    auto HTXDataManager::AppendMonitorExperiment(const QString& user, const QString& project, const QString& experiment)->void {
        if (user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;

        const auto path = d->GenerateExperimentPath(user, project, experiment);
        if (path.isEmpty()) return;

        AppendMonitor(path);
    }

    auto HTXDataManager::RemoveMonitorExperiment(const QString& user, const QString& project, const QString& experiment)->void {
        if (user.isEmpty()) return;
        if (project.isEmpty()) return;
        if (experiment.isEmpty()) return;

        const auto path = d->GenerateExperimentPath(user, project, experiment);
        if (path.isEmpty()) return;

        RemoveMonitor(path);
    }

    auto HTXDataManager::RegisterUpdater(IHTXDataManagerUpdater* updater)->void {
        d->updaters.push_back(updater);
    }

    auto HTXDataManager::DeregisterUpdater(IHTXDataManagerUpdater* updater)->void {
        d->updaters.removeOne(updater);
    }

    void HTXDataManager::onAddData(QString fileFullPath, int status) {
        if (fileFullPath.isEmpty()) return;

        auto name = DataMisc::GetTcfBaseName(fileFullPath);
        if (name.isEmpty()) return;

        const auto user = DataMisc::GetUserName(fileFullPath);
        const auto project = DataMisc::GetProjectName(fileFullPath);
        const auto experiment = DataMisc::GetExperimentName(fileFullPath);
        const auto specimen = DataMisc::GetSpecimenName(fileFullPath);
        const auto well = DataMisc::GetWellName(fileFullPath);

        auto newData = std::make_shared<TCFData>();
        newData->SetPath(fileFullPath);
        newData->SetName(name);
        newData->SetStatus(TCFProcessingStatus::_from_integral(status));
        TCFDataRepo::GetInstance()->AddData(user, project, experiment, specimen, well, newData);
    }

    void HTXDataManager::onScannerFinished() {
        QObject* obj = sender();
        if (nullptr == obj) return;

        for(auto scanner : d->scanners) {
            if(scanner != obj) continue;

            auto s = qobject_cast<DataScanner*>(sender());
            const auto user = s->property("user").toString();
            const auto project = s->property("project").toString();
            const auto experiment = s->property("experiment").toString();

            if (!user.isEmpty() && !project.isEmpty() && !experiment.isEmpty()) {
                for (auto updater : d->updaters) {
                    if (updater) updater->UpdateExperimentData(user, project, experiment);
                }
            }
            return;
        }        
    }

    void HTXDataManager::onWatchAddData(QString fileFullPath, int status) {
        if (fileFullPath.isEmpty()) return;

        const auto name = DataMisc::GetTcfBaseName(fileFullPath);
        if (name.isEmpty()) return;

        const auto user = DataMisc::GetUserName(fileFullPath);
        const auto project = DataMisc::GetProjectName(fileFullPath);
        const auto experiment = DataMisc::GetExperimentName(fileFullPath);
        const auto specimen = DataMisc::GetSpecimenName(fileFullPath);
        const auto well = DataMisc::GetWellName(fileFullPath);

        auto newData = std::make_shared<TCFData>();
        newData->SetPath(fileFullPath);
        newData->SetName(name);
        newData->SetStatus(TCFProcessingStatus::_from_integral(status));

        const auto dataRepo = TCFDataRepo::GetInstance();
        if (dataRepo->IsExist(user, project, experiment, specimen, well, name)) return;

        dataRepo->AddData(user, project, experiment, specimen, well, newData);

        for (auto updater : d->updaters) {
            if (updater) updater->WatchedAddData(fileFullPath);
        }
    }

    void HTXDataManager::onWatchDeleteData(QString fileFullPath) {
        if (fileFullPath.isEmpty()) return;

        const auto name = DataMisc::GetTcfBaseName(fileFullPath);
        if (name.isEmpty()) return;

        const auto user = DataMisc::GetUserName(fileFullPath);
        const auto project = DataMisc::GetProjectName(fileFullPath);
        const auto experiment = DataMisc::GetExperimentName(fileFullPath);
        const auto specimen = DataMisc::GetSpecimenName(fileFullPath);
        const auto well = DataMisc::GetWellName(fileFullPath);

        for (auto updater : d->updaters) {
            if (updater) updater->WatchedDeleteData(fileFullPath);
        }

        TCFDataRepo::GetInstance()->RemoveData(user, project, experiment, specimen, well, name);
    }

    void HTXDataManager::onWatchUpdateData(QString fileFullPath, int status) {
        if (fileFullPath.isEmpty()) return;

        const auto name = DataMisc::GetTcfBaseName(fileFullPath);
        if (name.isEmpty()) return;

        const auto user = DataMisc::GetUserName(fileFullPath);
        const auto project = DataMisc::GetProjectName(fileFullPath);
        const auto experiment = DataMisc::GetExperimentName(fileFullPath);
        const auto specimen = DataMisc::GetSpecimenName(fileFullPath);
        const auto well = DataMisc::GetWellName(fileFullPath);

        auto data = TCFDataRepo::GetInstance()->GetData(user, project, experiment, specimen, well, name);
        if(nullptr == data) {
            onWatchAddData(fileFullPath, status);
            return;
        }

        if(status != +TCFProcessingStatus::Unknown) {
            data->SetStatus(TCFProcessingStatus::_from_integral(status));
        }

        for (auto updater : d->updaters) {
            if (updater) updater->WatchedUpdateData(fileFullPath);
        }
    }

    void HTXDataManager::onWatchDeleteDataRootFolder(QString fileFullPath) {
        if (fileFullPath.isEmpty()) return;

        const auto name = DataMisc::GetTcfBaseName(fileFullPath);
        if (name.isEmpty()) return;

        const auto user = DataMisc::GetUserName(fileFullPath);
        const auto project = DataMisc::GetProjectName(fileFullPath);
        const auto experiment = DataMisc::GetExperimentName(fileFullPath);
        const auto specimen = DataMisc::GetSpecimenName(fileFullPath);
        const auto well = DataMisc::GetWellName(fileFullPath);

        for(auto updater : d->updaters) {
            if(updater) {
                updater->WatchedDeleteDataRootFolder(fileFullPath);
            }
        }

        TCFDataRepo::GetInstance()->DeleteDataRootFolder(user, project, experiment, specimen, well, name);
    }

    auto HTXDataManager::AppendScanner(const QString& path, const QString& user, const QString& project, const QString& experiment)->void {
        if (path.isEmpty()) return;

        auto progressDialog = new TC::ProgressDialog;
        progressDialog->SetTitle(tr("Progress"));
        progressDialog->SetContent(tr("Loading %1's data, please wait...").arg(experiment));
        progressDialog->SetStandardButtons(QDialogButtonBox::NoButton);
        progressDialog->setRange(0, 0);
        progressDialog->setModal(true);
        progressDialog->GetBar()->setFormat("%v / %m");
        progressDialog->show();

        auto scanner = new DataScanner;
        scanner->SetPath(path);
        scanner->setProperty("user", user);
        scanner->setProperty("project", project);
        scanner->setProperty("experiment", experiment);
        d->scanners.append(scanner);

        connect(scanner, SIGNAL(sigDataScanned(QString, int)), this, SLOT(onAddData(QString, int)));
        connect(scanner, SIGNAL(finished()), this, SLOT(onScannerFinished()));

        connect(scanner, &DataScanner::sigSetCurrentCount, progressDialog, &TC::ProgressDialog::setValue);
        connect(scanner, &DataScanner::sigSetTotalCount, progressDialog, &TC::ProgressDialog::setMaximum);
        connect(scanner, SIGNAL(finished()), progressDialog, SLOT(close()));
        connect(scanner, SIGNAL(finished()), progressDialog, SLOT(deleteLater()));

        scanner->start();
    }

    auto HTXDataManager::RemoveScanner(const QString& path)->void {
        if (path.isEmpty()) return;

        for (auto scanner : d->scanners) {
            if (nullptr == scanner)
                continue;

            if (path == scanner->GetPath()) {
                scanner->requestInterruption();
                scanner->wait();
                scanner->quit();

                disconnect(scanner, SIGNAL(sigDataScanned(QString, int)), this, SLOT(onAddData(QString, int)));
                disconnect(scanner, SIGNAL(finished()), this, SLOT(onScannerFinished()));

                d->scanners.removeOne(scanner);

                delete(scanner);
                scanner = nullptr;

                return;
            }
        }
    }

    auto HTXDataManager::AppendMonitor(const QString& path)->void {
        if (path.isEmpty()) return;

        auto monitor = new DataMonitor;
        monitor->SetWatchDirectory(path);
        d->monitors.append(monitor);

        connect(monitor, SIGNAL(sigDataAdded(QString, int)), this, SLOT(onWatchAddData(QString, int)));
        connect(monitor, SIGNAL(sigDataRemoved(QString)), this, SLOT(onWatchDeleteData(QString)));
        connect(monitor, SIGNAL(sigDataModified(QString, int)), this, SLOT(onWatchUpdateData(QString, int)));
        connect(monitor, SIGNAL(sigDataRootFolderDeleted(QString)), this, SLOT(onWatchDeleteDataRootFolder(QString)));

        monitor->start();
    }

    auto HTXDataManager::RemoveMonitor(const QString& path)->void {
        if (path.isEmpty()) return;

        for (auto monitor : d->monitors) {
            if (nullptr == monitor)
                continue;

            if (path == monitor->GetWatchDirectory()) {
                monitor->Cancel();
                monitor->requestInterruption();
                monitor->wait();
                monitor->quit();

                disconnect(monitor, SIGNAL(sigDataAdded(QString, int)), this, SLOT(onWatchAddData(QString, int)));
                disconnect(monitor, SIGNAL(sigDataRemoved(QString)), this, SLOT(onWatchDeleteData(QString)));
                disconnect(monitor, SIGNAL(sigDataModified(QString, int)), this, SLOT(onWatchUpdateData(QString, int)));
                disconnect(monitor, SIGNAL(sigDataRootFolderDeleted(QString)), this, SLOT(onWatchDeleteDataRootFolder(QString)));

                d->monitors.removeOne(monitor);

                delete(monitor);
                monitor = nullptr;

                return;
            }
        }
    }
}
