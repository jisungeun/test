#pragma once
#include <memory>
#include <QThread>
#include <QString>

namespace HTXpress::AppComponents::HTXDataManager {
    class DataScanner final : public QThread {
		Q_OBJECT

	public:
		DataScanner();
		~DataScanner() override;

		auto SetPath(const QString& path)->void;
		auto GetPath() const->QString;

		auto ScanFolder(const QString& path)->bool;

	signals:
		void sigDataScanned(QString name, int status);
		void sigSetTotalCount(int);
		void sigSetCurrentCount(int);

	protected:
		void run() override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}