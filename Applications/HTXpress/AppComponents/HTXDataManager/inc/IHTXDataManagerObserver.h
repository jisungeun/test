#pragma once

#include <memory>
#include <QObject>
#include <QString>

#include "HTXDataManagerExport.h"

namespace HTXpress::AppComponents::HTXDataManager {
    class HTXDataManager_API IHTXDataManagerObserver : public QObject {
        Q_OBJECT
    public:
        explicit IHTXDataManagerObserver(QObject* parent = nullptr);
        ~IHTXDataManagerObserver() override;

        virtual auto UpdateExperiment(const QString& user, const QString& project, const QString& experiment) -> void = 0;
        virtual auto WatchedAddData(const QString& fileFullPath) -> void = 0;
        virtual auto WatchedDeleteData(const QString& fileFullPath) -> void = 0;
        virtual auto WatchedUpdateData(const QString& fileFullPath) -> void = 0;
        virtual auto WatchedDeleteDataRootFolder(const QString& fileFullPath) -> void = 0;

    signals:
        void sigUpdateExperiment(const QString& user, const QString& project, const QString& experiment);
        void sigDataAdded(const QString& fileFullPath);
        void sigDataDeleted(const QString& fileFullPath);
        void sigDataUpdated(const QString& fileFullPath);
        void sigDataRootFolderDeleted(const QString& fileFullPath);
    };
}
