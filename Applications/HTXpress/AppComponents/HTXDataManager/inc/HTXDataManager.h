#pragma once

#include <memory>
#include <QString>

#include <TCFData.h>

#include "IHTXDataManagerUpdater.h"
#include "HTXDataManagerExport.h"

namespace HTXpress::AppComponents::HTXDataManager {
	class HTXDataManager_API HTXDataManager : QObject {
		Q_OBJECT

	public:
		using Self = HTXDataManager;
		using Pointer = std::shared_ptr<Self>;

	protected:
		HTXDataManager();

	public:
		~HTXDataManager();

		static auto GetInstance()->Pointer;

		auto AppendScanExperiment(const QString& user, const QString& project, const QString& experiment)->void;
		auto RemoveScanExperiment(const QString& user, const QString& project, const QString& experiment)->void;

		auto AppendMonitorExperiment(const QString& user, const QString& project, const QString& experiment)->void;
		auto RemoveMonitorExperiment(const QString& user, const QString& project, const QString& experiment)->void;

		auto RegisterUpdater(IHTXDataManagerUpdater* updater)->void;
		auto DeregisterUpdater(IHTXDataManagerUpdater* updater)->void;


	private slots:
		void onAddData(QString fileFullPath, int status);
		void onScannerFinished();

		void onWatchAddData(QString fileFullPath, int status);
		void onWatchDeleteData(QString fileFullPath);
		void onWatchUpdateData(QString fileFullPath, int status);
		void onWatchDeleteDataRootFolder(QString fileFullPath);

	private:
		auto AppendScanner(const QString& path, const QString& user, const QString& project, const QString& experiment = "")->void;
		auto RemoveScanner(const QString& path)->void;

		auto AppendMonitor(const QString& path)->void;
		auto RemoveMonitor(const QString& path)->void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}

