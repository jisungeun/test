#pragma once

#include <memory>
#include <QString>

#include "IHTXDataManagerObserver.h"
#include "HTXDataManagerExport.h"

namespace HTXpress::AppComponents::HTXDataManager {
	class HTXDataManager_API IHTXDataManagerUpdater {
	public:
		IHTXDataManagerUpdater();
		virtual ~IHTXDataManagerUpdater();

		virtual auto UpdateExperimentData(const QString& user, const QString& project, const QString& experiment) -> void = 0;

		virtual auto WatchedAddData(const QString& fileFullPath)->void = 0;
		virtual auto WatchedDeleteData(const QString& fileFullPath)->void = 0;
		virtual auto WatchedUpdateData(const QString& fileFullPath)->void = 0;
		virtual auto WatchedDeleteDataRootFolder(const QString& fileFullPath)->void = 0;
	};
}
