#pragma once
#include <memory>

#include <QString>

#include "HTXDataManagerExport.h"

namespace HTXpress::AppComponents::HTXDataManager {
    class HTXDataManager_API DataMisc {
    private:
        DataMisc();
        ~DataMisc();

    public:
        enum class PathIndex {
            User,
            Project,
            Experiment,
            Data
        };

        enum class TcfNameIndex {
            Date,
            Time,
            Experiment,
            Count,
            Specimen,
            Well,
            Type,
            Extension
        };

        static auto GetUserName(const QString& fileFullPath) -> QString;
        static auto GetProjectName(const QString& fileFullPath) -> QString;
        static auto GetExperimentName(const QString& fileFullPath) -> QString;
        static auto GetSpecimenName(const QString& fileFullPath) -> QString;
        static auto GetWellName(const QString& fileFullPath) -> QString;

        static auto GetRawDataGeneratedDate(const QString& fileFullPath) -> QString;
        static auto GetRawDataGeneratedTime(const QString& fileFullPath) -> QString;
        static auto GetTcfExtension(const QString& fileFullPath) -> QString;
        static auto GetTcfBaseName(const QString& fileFullPath) -> QString;
        static auto GetTcfTypeInfo(const QString& fileFullPath) -> QString;

        static auto GetDataFolderPath(const QString& fileFullPath) -> QString;
        static auto GetTimelapsePointID(const QString& fileFullPath) -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
