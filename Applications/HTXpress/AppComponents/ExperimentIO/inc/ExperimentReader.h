#pragma once

#include <memory>

#include <QString>

#include <Experiment.h>

#include "HTXExperimentIOExport.h"

namespace HTXpress::AppComponents::ExperimentIO {
	class HTXExperimentIO_API ExperimentReader {
	public:
		ExperimentReader();
		~ExperimentReader();

		/**
		 * \brief Experiment 파일 내용 획득
		 * \param path Experiment 파일 경로
		 */
		auto Read(const QString& path) const->AppEntity::Experiment::Pointer;

		/**
		 * \brief Experiment 파일 내용중 간략한 정보(id, title, created date, progress)만 획득
		 * \param path Experiment 파일 경로
		 */
		auto ReadShortInfo(const QString& path) const->AppEntity::Experiment::Pointer;

		/**
		 * \brief Vessel Repo(SystemConfig\vessels폴더)에 존재하지 않는 vessel의 정보를 experiment에 설정
		 * \param filePath Vessel 파일의 파일 이름이 포함된 전체 경로
		 */
		auto SetVesselFilePath(const QString& filePath) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
