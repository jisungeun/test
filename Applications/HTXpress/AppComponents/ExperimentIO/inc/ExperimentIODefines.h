#pragma once

namespace HTXpress::AppComponents::ExperimentIO {
    namespace Key {
		constexpr char CreatedDate[] = "createdDate";
		constexpr char Status[] = "experimentStatus";
		constexpr char ID[] = "experimentId";
		constexpr char Title[] = "experimentTitle";
		constexpr char User[] = "user";
		constexpr char Medium[] = "medium";
		constexpr char FOV[] = "fieldOfView";
		constexpr char Vessel[] = "vessel";
		constexpr char ExperimentSettings[] = "experimentSettings";
		constexpr char SampleType[] = "sampleType";

		// Medium
		constexpr char MediumName[] = "mediumName";
		constexpr char MediumRI[] = "mediumRI";
        
		// Vessel
		constexpr char VesselIndex[] = "vesselIndex";
		constexpr char VesselModel[] = "vesselModel";
		constexpr char VesselTitle[] = "vesselTitle";
		constexpr char VesselSize[] = "vesselSize";
		constexpr char WellShape[] = "wellShape";
		constexpr char WellSize[] = "wellSize";
		constexpr char WellCount[] = "wellCount";
		constexpr char Wells[] = "wells";
		constexpr char ImagingAreaShape[] = "imagingAreaShape";
		constexpr char ImagingAreaSize[] = "imagingAreaSize";
		constexpr char ImagingAreaPosition[] = "imagingAreaPosition";

		// Well
		constexpr char WellGroupTitle[] = "wellGroupTitle";
		constexpr char WellGroupColor[] = "color";
		//constexpr char Wells[] = "wells";	// Vessel - Wells key�� ����
		constexpr char WellGroups[] = "wellGroups";
		constexpr char WellNames[] = "wellNames";
		constexpr char WellName[] = "wellName";
		constexpr char WellIndex[] = "wellIndex";
		constexpr char WellPosition[] = "wellPosition";
		constexpr char WellLabel[] = "wellLabel";
		constexpr char WellTitle[] = "wellTitle";
		constexpr char ImagingLocations[] = "imaginglocations";
		constexpr char Locations[] = "locations";
		constexpr char Row[] = "row";
		constexpr char Column[] = "column";
		constexpr char LastLocationIndex[] = "lastLocationIndex";

		// Location
		constexpr char CenterPosition[] = "centerPosition";
		constexpr char AcquisitionArea[] = "acquisitionArea";
		constexpr char TileType[] = "tile";
		constexpr char LocationIndex[] = "locationIndex";

		// Imaging Sequence
		constexpr char ImagingScenarios[] = "imagingScenarios";
		constexpr char ImagingScenario[] = "imagingScenario";
		constexpr char ImagingSequences[] = "imagingSequences";
		constexpr char SequenceTitles[] = "seqTitles";
		constexpr char SequenceStartTimes[] = "seqStartTimes";
		constexpr char TimelapseInterval[] = "timelapseInterval";
		constexpr char TimelapseCount[] = "timelapseCount";
		constexpr char ImagingConditions[] = "imagingConditions";
		constexpr char SingleImagingConditions[] = "singleImagingConditions";
        constexpr char ImagingModality[] = "modality";

        // Single Imaging
		constexpr char ImagingType[] = "ImagingType";
		constexpr char ImagingCondition[] = "imagingCondition";
		constexpr char Enable[] = "enable";

		// ROI
		constexpr char ROIListTitle[] = "ROIs";
		constexpr char ROIWellIndex[] = "wellIndex";
		constexpr char ROIList[] = "roiList";
		constexpr char ROIIndex[] = "index";
		constexpr char ROIShape[] = "shape";
		constexpr char ROIName[] = "name";
		constexpr char ROIPosition[] = "position";
		constexpr char ROISize[] = "size";
    }
}
