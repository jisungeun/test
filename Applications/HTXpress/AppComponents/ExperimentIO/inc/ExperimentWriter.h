#pragma once

#include <QString>

#include <Experiment.h>

#include "HTXExperimentIOExport.h"

namespace HTXpress::AppComponents::ExperimentIO {
	class HTXExperimentIO_API ExperimentWriter {
	public:
		ExperimentWriter();
		~ExperimentWriter();

		/**
		 * \brief Experiment entity 내용을 파일로 저장
		 * \param path 저장할 파일 경로
		 * \param 저장할 Experiment
		 */
		auto Write(const QString& path, const AppEntity::Experiment::Pointer& experiment) const -> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
