#include <catch2/catch.hpp>

#include <QDir>
#include <QString>
#include <QDebug>

#include <Experiment.h>
#include <Vessel.h>
#include <ExperimentReader.h>
#include <ExperimentWriter.h>

#include "TestDataGenerator.h"

namespace HTXpress::AppComponents::ExperimentIO::Test {
    TEST_CASE("Experiment File IO") {
        TestDataGenerator testData;

        ExperimentWriter writer;
        ExperimentReader reader;

        QString testFilePath = testData.ExperimentPath();

        SECTION("Write failure") {
            REQUIRE_FALSE(writer.Write("", testData.Experiment()));
            REQUIRE_FALSE(writer.Write(testFilePath, nullptr));
        }

        SECTION("Write success") {
            REQUIRE(writer.Write(testFilePath, testData.Experiment()));

            SECTION("Read failure") {
                auto readExperiment = reader.Read("/notexistpath");
                REQUIRE(readExperiment == nullptr);
            }

            SECTION("Read success") {
                auto doubleComp = [](double left, double right)->bool {
                    return std::fabs(left - right) <= std::numeric_limits<double>::epsilon();
                };

                auto experiment = reader.Read(testFilePath);
                REQUIRE(experiment != nullptr);

                SECTION("Compare experiment ID") {
                    CHECK(testData.Experiment()->GetID() == experiment->GetID());
                }

                SECTION("Compare experiment title") {
                    CHECK(testData.Experiment()->GetTitle() == experiment->GetTitle());
                }

                SECTION("Compare user ID") {
                    CHECK(testData.Experiment()->GetUserID() == experiment->GetUserID());
                }

                SECTION("Compare medium") {
                    CHECK(testData.Experiment()->GetMedium() == experiment->GetMedium());
                }

                SECTION("Compare created date") {
                    CHECK(testData.Experiment()->GetCreatedDate() == experiment->GetCreatedDate());
                }

                SECTION("Compare progress") {
                    CHECK(testData.Experiment()->GetProgress() == experiment->GetProgress());
                }

                SECTION("Compare vessel model") {
                    auto originVessel = testData.Experiment()->GetVessel();
                    auto readVessel = experiment->GetVessel();
                    CHECK(originVessel->GetModel() == readVessel->GetModel());
                }

                SECTION("Compare well names") {
                    for (auto vesselIndex = 0; vesselIndex < testData.Experiment()->GetVesselCount(); ++vesselIndex) {
                        auto originWellNames = testData.Experiment()->GetWellNames(vesselIndex);
                        auto readWellNames = experiment->GetWellNames(vesselIndex);

                        CHECK(originWellNames == readWellNames);
                    }
                }

                SECTION("Compare well groups") {
                    for (auto vesselIndex = 0; vesselIndex < testData.Experiment()->GetVesselCount(); ++vesselIndex) {
                        auto wellGroupCountComparison = testData.Experiment()->GetWellGroupCount(vesselIndex) == experiment->GetWellGroupCount(vesselIndex);
                        CHECK(wellGroupCountComparison);
                        if (!wellGroupCountComparison) {
                            break;
                        }

                        for (auto groupIndex = 0; groupIndex < testData.Experiment()->GetWellGroupCount(vesselIndex); ++groupIndex) {
                            auto originGroup = testData.Experiment()->GetWellGroup(vesselIndex, groupIndex);
                            auto readGroup = experiment->GetWellGroup(vesselIndex, groupIndex);

                            auto groupComparison = originGroup == readGroup;
                            CHECK(groupComparison);
                            if (!groupComparison) {
                                break;
                            }
                        }
                    }
                }

                SECTION("Compare locations") {
                    for (auto vesselIndex = 0; vesselIndex < testData.Experiment()->GetVesselCount(); ++vesselIndex) {
                        auto originLocations = testData.Experiment()->GetAllLocations(vesselIndex);
                        auto readLocations = experiment->GetAllLocations(vesselIndex);

                        auto locationComparison = originLocations == readLocations;
                        CHECK(locationComparison);
                        if (!locationComparison) {
                            break;
                        }
                    }
                }

                SECTION("Compare scenario") {
                    for (auto vesselIndex = 0; vesselIndex < testData.Experiment()->GetVesselCount(); ++vesselIndex) {
                        auto originScenario = testData.Experiment()->GetScenario(vesselIndex);
                        auto readScenario = experiment->GetScenario(vesselIndex);

                        auto scenarioCountComparison = originScenario->GetCount() == readScenario->GetCount();
                        CHECK(scenarioCountComparison);
                        if (!scenarioCountComparison) {
                            break;
                        }

                        for (auto sequenceIndex = 0; sequenceIndex < originScenario->GetCount(); ++sequenceIndex) {
                            auto originSequence = originScenario->GetSequence(sequenceIndex);
                            auto readSequence = readScenario->GetSequence(sequenceIndex);

                            for (auto modalIndex = 0; modalIndex < originSequence->GetModalityCount(); ++modalIndex) {
                                auto originCondition = originSequence->GetImagingCondition(modalIndex);
                                auto readCondition = readSequence->GetImagingCondition(modalIndex);

                                CHECK(originCondition->GetSliceCount() == readCondition->GetSliceCount());
                                CHECK(originCondition->GetSliceStep() == readCondition->GetSliceStep());

                                auto modal = originCondition->GetModality();
                                if (modal == +HTXpress::AppEntity::Modality::HT) {
                                    auto originHT = dynamic_cast<HTXpress::AppEntity::ImagingConditionHT*>(originCondition.get());
                                    auto readHT = dynamic_cast<HTXpress::AppEntity::ImagingConditionHT*>(readCondition.get());

                                    CHECK(originHT->GetExposure() == readHT->GetExposure());
                                    CHECK(originHT->GetInterval() == readHT->GetInterval());

                                } else if (modal == +HTXpress::AppEntity::Modality::FL) {
                                    auto originFL = dynamic_cast<HTXpress::AppEntity::ImagingConditionFL*>(originCondition.get());
                                    auto readFL = dynamic_cast<HTXpress::AppEntity::ImagingConditionFL*>(readCondition.get());

                                    auto originChannels = originFL->GetChannels();
                                    auto readChannels = readFL->GetChannels();
                                    auto channelsComparison = originChannels == readChannels;
                                    CHECK(channelsComparison);
                                    if (!channelsComparison) {
                                        break;
                                    }

                                    for (auto channel : originChannels) {
                                        CHECK(originFL->GetExposure(channel) == readFL->GetExposure(channel));
                                        CHECK(originFL->GetInterval(channel) == readFL->GetInterval(channel));
                                        CHECK(originFL->GetIntensity(channel) == readFL->GetIntensity(channel));
                                        CHECK(originFL->GetGain(channel) == readFL->GetGain(channel));
                                        CHECK(originFL->GetExFilter(channel) == readFL->GetExFilter(channel));
                                        CHECK(originFL->GetEmFilter(channel) == readFL->GetEmFilter(channel));
                                        CHECK(originFL->GetName(channel) == readFL->GetName(channel));
                                    }

                                } else if (modal == +HTXpress::AppEntity::Modality::BF) {
                                    auto originBF = dynamic_cast<HTXpress::AppEntity::ImagingConditionBF*>(originCondition.get());
                                    auto readBF = dynamic_cast<HTXpress::AppEntity::ImagingConditionBF*>(readCondition.get());

                                    CHECK(originBF->GetColorMode() == readBF->GetColorMode());
                                    CHECK(originBF->GetExposure() == readBF->GetExposure());
                                    CHECK(originBF->GetInterval() == readBF->GetInterval());
                                }
                            }
                        }
                    }
                }

                SECTION("Compare Single Imaging Conditions") {
                    for(auto type : AppEntity::ImagingType::_values()) {
                        auto originCond = testData.Experiment()->GetSingleImagingCondition(type);
                        if(!originCond) continue;

                        auto testCond = experiment->GetSingleImagingCondition(type);
                        CHECK(testCond != nullptr);
                        CHECK(*originCond == *testCond);

                        auto originEnable = testData.Experiment()->GetEnableSingleImaging(type);
                        auto testEnable = experiment->GetEnableSingleImaging(type);
                        CHECK(originEnable == testEnable);
                    }
                }
            }
        }

        if (QFile::exists(testFilePath)) {
            QFile::remove(testFilePath);
        }
    }
}