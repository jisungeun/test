#include "TestDataGenerator.h"

#include <QDir>
#include <QString>

#include <VesselRepo.h>

namespace HTXpress::AppComponents::ExperimentIO::Test {
    struct TestDataGenerator::Impl {
        const QString user{ "test-user" };
        const QString testRoot{ QDir::tempPath() };
        const QString title{ "test-experiment"};
        const QString testVessel{ "test-Vessel" };

        AppEntity::Experiment::Pointer experiment{ nullptr };

        auto InitSystemConfig()->void;
        auto Generate()->bool;
        auto Clear()->void;
    };

    auto TestDataGenerator::Impl::InitSystemConfig() -> void {
        auto vessel = std::make_shared<AppEntity::Vessel>();
        vessel->SetModel(testVessel);
        vessel->SetName(testVessel);

        auto vesselRepo = AppEntity::VesselRepo::GetInstance();
        vesselRepo->AddVessel(vessel);
    }

    auto TestDataGenerator::Impl::Generate() -> bool {
        // set experiment data
        experiment = std::make_shared<HTXpress::AppEntity::Experiment>();
        experiment->SetID(HTXpress::AppEntity::Experiment::GenerateID(user, title));
        experiment->SetTitle(title);
        experiment->SetUserID(user);
        experiment->SetCreatedDate("19891114");
        experiment->SetProgress(HTXpress::AppEntity::ExperimentProgress::NotStarted);

        AppEntity::MediumData medium;
        medium.SetName("Medium-Test");
        medium.SetRI(0.123);
        experiment->SetMedium(medium);

        // generate vessel data
        const int vesselCount = 2;
        const int wellRowCount = 2;
        const int wellColumnCount = 2;

        auto vessel = std::make_shared<HTXpress::AppEntity::Vessel>();
        vessel->SetModel(testVessel);
        vessel->SetName(testVessel);
        vessel->SetSize(100., 200.);
        vessel->SetWellShape(HTXpress::AppEntity::WellShape::Rectangle);
        vessel->SetWellSize(10., 20.);
        vessel->SetWellCount(wellRowCount, wellColumnCount);

        for (auto row = 0; row < wellRowCount; ++row) {
            for (auto column = 0; column < wellColumnCount; ++column) {
                auto posX = 1.;
                auto posY = 2.;

                vessel->AddWell(row, column, posX, posY);
            }
        }

        vessel->SetImagingAreaShape(HTXpress::AppEntity::ImagingAreaShape::Rectangle);
        vessel->SetImagingAreaSize(2., 3.);
        vessel->SetImagingAreaPosition(4., 5.);

        experiment->SetVesselInfo(vessel);

        // add well groups
        for (auto vesselIndex = 0; vesselIndex < vesselCount; ++vesselIndex) {
            for (auto row = 0; row < wellRowCount; ++row) {
                HTXpress::AppEntity::WellGroup wellGroup;
                wellGroup.SetTitle(QString("Group-%1-%2").arg(vesselIndex).arg(row));

                HTXpress::AppEntity::WellGroup::Color color;
                color.r = 6;
                color.g = 7;
                color.b = 8;
                color.a = 9;
                wellGroup.SetColor(color);

                for (auto column = 0; column < wellColumnCount; ++column) {
                    wellGroup.AddWell(row, column);
                }

                experiment->SetWellGroup(vesselIndex, experiment->GetNewWellGroupIndex(), wellGroup);
            }
        }

        // add well names
        for (auto vesselIndex = 0; vesselIndex < experiment->GetVesselCount(); ++vesselIndex) {
            experiment->SetWellName(vesselIndex, 0, "well-0");
            experiment->SetWellName(vesselIndex, 1, "well-1");
        }

        // add locations
        double locationTestValue = 0.;
        for (auto vesselIndex = 0; vesselIndex < experiment->GetVesselCount(); ++vesselIndex) {
            for (auto wellIndex : vessel->GetWellIndices()) {
                HTXpress::AppEntity::Location location;
                location.SetCenter(HTXpress::AppEntity::Position(locationTestValue, locationTestValue + 1, locationTestValue + 2));
                location.SetArea(HTXpress::AppEntity::Area(locationTestValue + 10, locationTestValue + 11), false);

                experiment->AddLocation(vesselIndex, wellIndex, location);
                locationTestValue++;
            }
        }

        // add sequences
        for (auto vesselIndex = 0; vesselIndex < experiment->GetVesselCount(); ++vesselIndex) {
            auto scenario = std::make_shared<HTXpress::AppEntity::ImagingScenario>();
            auto sequnece = std::make_shared<HTXpress::AppEntity::ImagingSequence>();

            auto conditionHT = std::make_shared<HTXpress::AppEntity::ImagingConditionHT>();
            conditionHT->SetExposure(10);
            conditionHT->SetInterval(20);

            auto conditionFL = std::make_shared<HTXpress::AppEntity::ImagingConditionFL>();
            conditionFL->SetChannel(0, 1, 2, 3, 4, AppEntity::FLFilter(5, 1), AppEntity::FLFilter(6, 1), "FITC", {255, 0, 0});
            conditionFL->SetChannel(1, 2, 3, 4, 5, AppEntity::FLFilter(6, 1), AppEntity::FLFilter(7, 1), "mCherry", {0, 255, 0});

            auto conditionBF = std::make_shared<HTXpress::AppEntity::ImagingConditionBF>();
            conditionBF->SetColorMode(true);
            conditionBF->SetExposure(30);
            conditionBF->SetInterval(40);

            sequnece->AddImagingCondition(conditionHT);
            sequnece->AddImagingCondition(conditionFL);
            sequnece->AddImagingCondition(conditionBF);

            scenario->AddSequence(0, "seq-1", sequnece);
                
            experiment->SetScenario(vesselIndex, scenario);
        }

        //add single imaging conditions
        for(auto type : AppEntity::ImagingType::_values()) {
            switch(type) {
            case AppEntity::ImagingType::HT3D:
                experiment->SetSingleImagingCondition(type,
                                                      std::make_shared<AppEntity::ImagingConditionHT>());
                experiment->SetEnableSingleImaging(type, true);
                break;
            case AppEntity::ImagingType::HT2D:
                experiment->SetSingleImagingCondition(type,
                                                      std::make_shared<AppEntity::ImagingConditionHT>());
                experiment->SetEnableSingleImaging(type, false);
                break;
            case AppEntity::ImagingType::FL3D:
                experiment->SetSingleImagingCondition(type,
                                                      std::make_shared<AppEntity::ImagingConditionFL>());
                experiment->SetEnableSingleImaging(type, true);
                break;
            case AppEntity::ImagingType::FL2D:
                experiment->SetSingleImagingCondition(type,
                                                      std::make_shared<AppEntity::ImagingConditionFL>());
                experiment->SetEnableSingleImaging(type, false);
                break;
            case AppEntity::ImagingType::BFGray:
                experiment->SetSingleImagingCondition(type,
                                                      std::make_shared<AppEntity::ImagingConditionBF>());
                experiment->SetEnableSingleImaging(type, true);
                break;
            case AppEntity::ImagingType::BFColor:
                experiment->SetSingleImagingCondition(type,
                                                      std::make_shared<AppEntity::ImagingConditionBF>());
                experiment->SetEnableSingleImaging(type, true);
                break;
            }
        }

        return true;
    }

    auto TestDataGenerator::Impl::Clear() -> void {
        QDir dir(testRoot);
        dir.removeRecursively();
    }

    TestDataGenerator::TestDataGenerator() : d{ new Impl } {
        d->InitSystemConfig();
        d->Generate();
    }

    TestDataGenerator::~TestDataGenerator() {
        d->Clear();
    }

    auto TestDataGenerator::Root() const -> QString {
        return d->testRoot;
    }

    auto TestDataGenerator::User() const -> QString {
        return d->user;
    }

    auto TestDataGenerator::Experiment() const -> AppEntity::Experiment::Pointer {
        return d->experiment;
    }

    auto TestDataGenerator::ExperimentPath() const -> QString {
        return QString(d->testRoot + "/" + d->title + "." + AppEntity::ExperimentExtension);
    }
}