#pragma once

#include <memory>

#include <QString>

#include <Experiment.h>

namespace HTXpress::AppComponents::ExperimentIO::Test {
    class TestDataGenerator {
    public:
        TestDataGenerator();
        ~TestDataGenerator();

        auto Root() const->QString;
        auto User() const->QString;
        auto Experiment() const->AppEntity::Experiment::Pointer;
        auto ExperimentPath() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
