#include "ExperimentWriter.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "ExperimentIODefines.h"

namespace HTXpress::AppComponents::ExperimentIO {
    struct ExperimentWriter::Impl {
        AppEntity::Experiment::Pointer experiment{ nullptr };

        auto TransformExperimentSettingsIntoJson() const->QJsonArray;
        auto TransformImagingScenarioIntoJson(int vesselIndex) const->QJsonObject;
        auto TransformSingleImagingConditionIntoJson() const->QJsonArray;
        auto TransformROIListIntoJson(const int32_t& vesselIndex) const->QJsonArray;
    };

    auto ExperimentWriter::Impl::TransformExperimentSettingsIntoJson() const -> QJsonArray {
        QJsonArray settingArray;

        const auto vessel = experiment->GetVessel();

        for (auto vesselIndex = 0; vesselIndex < experiment->GetVesselCount(); ++vesselIndex) {
            QJsonObject settingObj;
            settingObj[Key::VesselIndex] = vesselIndex;

            // add well groups
            QJsonArray wellGroupArray;
            auto wellGroupIndices = experiment->GetWellGroupIndices(vesselIndex);
            std::for_each(wellGroupIndices.begin(), wellGroupIndices.end(), [&](AppEntity::WellGroupIndex groupIndex) {
                auto wellGroup = experiment->GetWellGroup(vesselIndex, groupIndex);
                auto wells = wellGroup.GetWells();
                if (wells.isEmpty()) return;

                QJsonObject wellGroupObj;
                wellGroupObj[Key::WellGroupTitle] = wellGroup.GetTitle();
                wellGroupObj[Key::WellGroupColor] = QJsonArray({ wellGroup.GetColor().r, wellGroup.GetColor().g, wellGroup.GetColor().b, wellGroup.GetColor().a });

                QJsonArray wellArray;
                for (auto& well : wells) {
                    wellArray << QJsonArray({ static_cast<int>(well.rowIdx), static_cast<int>(well.colIdx) });
                }

                wellGroupObj[Key::Wells] = wellArray;

                wellGroupArray << wellGroupObj;
            });

            settingObj[Key::WellGroups] = wellGroupArray;

            // add well names
            QJsonArray wellNameArray;

            auto wellNames = experiment->GetWellNames(vesselIndex);
            QMapIterator<AppEntity::WellIndex, QString> wellNamesIt(wellNames);
            while (wellNamesIt.hasNext()) {
                wellNamesIt.next();

                QJsonObject wellNameObj;
                wellNameObj[Key::WellIndex] = wellNamesIt.key();
                wellNameObj[Key::WellName] = wellNamesIt.value();

                wellNameArray << wellNameObj;
            }

            settingObj[Key::WellNames] = wellNameArray;

            // add imaging locations
            QJsonArray imagingLocationArray;

            auto allLocations = experiment->GetAllLocations(vesselIndex);
            auto wellIndices = allLocations.keys();

            for (auto wellIndex : wellIndices) {
                auto wellLocations = allLocations.value(wellIndex);
                QJsonArray locationArray;
                QList<AppEntity::LocationIndex> locIndices;

                for(auto it = wellLocations.cbegin(); it != wellLocations.cend(); ++it) {
                    const auto locIndex = it.key();
                    const auto centerPosition = it.value().GetCenter();
                    const auto area = it.value().GetArea();
                    const auto isTile = it.value().IsTile();

                    QJsonObject locationObj;
                    locationObj[Key::CenterPosition] = QJsonArray({ centerPosition.X(), centerPosition.Y(), centerPosition.Z() });
                    locationObj[Key::AcquisitionArea] = QJsonArray({ area.Width(), area.Height() });
                    locationObj[Key::TileType] = isTile;
                    locationObj[Key::LocationIndex] = locIndex;

                    locIndices.push_back(locIndex);

                    locationArray << locationObj;
                }

                QJsonObject imagingLocationObj;
                imagingLocationObj[Key::WellIndex] = wellIndex;
                imagingLocationObj[Key::Locations] = locationArray;

                auto maxIndex = std::max_element(locIndices.cbegin(), locIndices.cend());
                const auto lastLocIndex = experiment->GetLastLocationIndex(vesselIndex, wellIndex);

                if(*maxIndex != lastLocIndex) {
                    imagingLocationObj[Key::LastLocationIndex] = *maxIndex;
                }
                else {
                    imagingLocationObj[Key::LastLocationIndex] = lastLocIndex;
                }

                imagingLocationArray << imagingLocationObj;
            }

            settingObj[Key::ImagingLocations] = imagingLocationArray;

            // add imaging scenario
            settingObj[Key::ImagingScenario] = TransformImagingScenarioIntoJson(vesselIndex);

            // single imaging conditions
            settingObj[Key::SingleImagingConditions] = TransformSingleImagingConditionIntoJson();

            // ROI list
            settingObj[Key::ROIListTitle] = TransformROIListIntoJson(vesselIndex);

            settingArray << settingObj;
        }

        return settingArray;
    }

    auto ExperimentWriter::Impl::TransformImagingScenarioIntoJson(int vesselIndex) const->QJsonObject {
        QJsonObject scenarioObj;

        auto imagingScenario = experiment->GetScenario(vesselIndex);
        if (imagingScenario == nullptr) {
            return QJsonObject();
        }

        QJsonArray seqArray;
        QJsonArray startTimeArray;
        QJsonArray titleArray;

        auto seqCount = imagingScenario->GetCount();
        for (auto seqIndex = 0; seqIndex < seqCount; ++seqIndex) {
            titleArray << imagingScenario->GetTitle(seqIndex);
            startTimeArray << static_cast<int>(imagingScenario->GetStartTime(seqIndex));

            QJsonObject seqObj;
            auto seq = imagingScenario->GetSequence(seqIndex);
            if (seq) {
                seqObj[Key::TimelapseInterval] = seq->GetInterval();
                seqObj[Key::TimelapseCount] = seq->GetTimeCount();

                QJsonArray conditionArray;
                auto modalCount = seq->GetModalityCount();
                for (auto modalIndex = 0; modalIndex < modalCount; ++modalIndex) {
                    auto condition = seq->GetImagingCondition(modalIndex);
                    if (condition == nullptr) continue;
                    conditionArray << condition->ToJsonObject();
                }

                seqObj[Key::ImagingConditions] = conditionArray;
            }

            seqArray << seqObj;
        }

        scenarioObj[Key::ImagingSequences] = seqArray;
        scenarioObj[Key::SequenceStartTimes] = startTimeArray;
        scenarioObj[Key::SequenceTitles] = titleArray;

        return scenarioObj;
    }

    auto ExperimentWriter::Impl::TransformSingleImagingConditionIntoJson() const -> QJsonArray {
        QJsonArray jsonArray;

        for(auto type : AppEntity::ImagingType::_values()) {
            auto imagingCondition = experiment->GetSingleImagingCondition(type);
            if(!imagingCondition) continue;

            QJsonObject object;

            object[Key::ImagingType] = type._to_string();
            object[Key::ImagingCondition] = imagingCondition->ToJsonObject();
            object[Key::Enable] = experiment->GetEnableSingleImaging(type);

            jsonArray << object;
        }

        return jsonArray;
    }

    auto ExperimentWriter::Impl::TransformROIListIntoJson(const int32_t& vesselIndex) const -> QJsonArray {
        // add ROI
        QJsonArray jsonArray;
        auto allROIs = experiment->GetROIs(vesselIndex);
        for (auto allIt = allROIs.cbegin(); allIt != allROIs.cend(); ++allIt) {
            QJsonObject jsonObject;

            const auto wellIndex = allIt.key();
            const auto roiList = allIt.value();

            QJsonArray roiListJsonArray;
            for (auto listIt = roiList.cbegin(); listIt != roiList.cend(); ++listIt) {
                QJsonObject obj;
                const auto roiIndex = listIt.key();
                const auto roi = listIt.value();

                assert(roi!=nullptr);

                obj[Key::ROIIndex] = roiIndex;
                obj[Key::ROIPosition] = QJsonArray({roi->GetCenter().X(), roi->GetCenter().Y()});
                obj[Key::ROISize] = QJsonArray({roi->GetSize().Width(), roi->GetSize().Height()});
                obj[Key::ROIName] = roi->GetName();
                obj[Key::ROIShape] = roi->GetShape()._to_string();

                roiListJsonArray << obj;
            }

            jsonObject[Key::ROIWellIndex] = wellIndex; 
            jsonObject[Key::ROIList] = roiListJsonArray; 

            jsonArray << jsonObject;
        }

        return jsonArray;
    }

    ExperimentWriter::ExperimentWriter() : d{ new Impl } {
    }

    ExperimentWriter::~ExperimentWriter() {
    }

    auto ExperimentWriter::Write(const QString& path, const AppEntity::Experiment::Pointer& experiment) const -> bool {
        if (path.isEmpty() || experiment == nullptr) {
            return false;
        }

        d->experiment = experiment;

        // AppEntity::Experiment 정보를 json 포맷으로 변환
        QJsonObject json;
        json[Key::ID] = d->experiment->GetID();
        json[Key::Title] = d->experiment->GetTitle();
        json[Key::User] = d->experiment->GetUserID();
        json[Key::CreatedDate] = d->experiment->GetCreatedDate();
        json[Key::Status] = d->experiment->GetProgress()._to_integral();
        json[Key::SampleType] = d->experiment->GetSampleTypeName();

        QJsonObject mediumObj;
        auto medium = d->experiment->GetMedium();
        mediumObj[Key::MediumName] = medium.GetName();
        mediumObj[Key::MediumRI] = medium.GetRI();
            
        json[Key::Medium] = mediumObj;

        const auto fov = d->experiment->GetFOV();
        json[Key::FOV] = QJsonArray({ fov.Width(), fov.Height() });

        const auto vessel = d->experiment->GetVessel();
        if (vessel) {
            json[Key::VesselModel] = vessel->GetModel();
        }

        // add settings
        json[Key::ExperimentSettings] = d->TransformExperimentSettingsIntoJson();

        // 파일로 저장
        QFileInfo fileInfo(path);
        QDir fileDir(fileInfo.absolutePath());
        if (!fileDir.exists()) {
            fileDir.mkpath(fileInfo.absolutePath());
        }

        QFile file(path);
        if (!file.open(QIODevice::WriteOnly)) {
            return false;
        }

        if (file.write(QJsonDocument(json).toJson()) < -1) {
            return false;
        }

        return true;
    }
}