#define LOGGER_TAG "[ExperimentIO]"

#include "ExperimentReader.h"

#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <TCLogger.h>
#include <ImagingCondition.h>
#include <VesselRepo.h>
#include <MediumData.h>
#include <VesselReader.h>

#include "ExperimentIODefines.h"

namespace HTXpress::AppComponents::ExperimentIO {
    struct ExperimentReader::Impl {
        AppEntity::Experiment::Pointer experiment{ nullptr };
        QString vesselFilePath{};

        auto ParseAll(const QJsonObject& json)->bool;
        auto ParseShortInfo(const QJsonObject& json)->bool;

        auto ParseExperimentSettings(const QJsonArray& settingArray)->void;
        auto ParseExperimentSetting(const QJsonObject& json)->void;
        auto ParseWellGroups(int vesselIndex, const QJsonArray& json)->void;
        auto ParseLocations(int vesselIndex, const QJsonArray& json)->void;
        auto ParseImagingScenario(int vesselIndex, const QJsonObject& json)->void;
        auto ParseSingleImagingConditions(int vesselIndex, const QJsonArray& jsonArray)->void;
        auto ParseROIList(int vesselIndex, const QJsonArray& jsonArray)->void;
    };

    auto ExperimentReader::Impl::ParseAll(const QJsonObject& json) -> bool {
        if (!ParseShortInfo(json)) {
            return false;
        }

        if (json.contains(Key::User) && json[Key::User].isString()) {
            experiment->SetUserID(json[Key::User].toString());
        } else {
            return false;
        }

        if (json.contains(Key::Medium) && json[Key::Medium].isObject()) {
            auto mediumObj = json[Key::Medium].toObject();

            if (mediumObj.contains(Key::MediumName) && mediumObj.contains(Key::MediumRI)) {
                AppEntity::MediumData medium;
                medium.SetName(mediumObj[Key::MediumName].toString());
                medium.SetRI(mediumObj[Key::MediumRI].toDouble());    
                experiment->SetMedium(medium);
            } else {
                QLOG_ERROR() << "It fails to get medium name and RI value.";
                return false;
            }
        } else {
            QLOG_ERROR() << "It fails to get medium.";
            return false;
        }

        if (json.contains(Key::FOV) && json[Key::FOV].isArray()) {
            auto fov = json[Key::FOV].toArray();
            if (fov.size() == 2) {
                //TODO ROI restriction will be removed after the system supports rectangular shape of ROI
                if(fov[0].toInt() != fov[1].toInt()) {
                    auto minVal = std::min(fov[0].toInt(), fov[1].toInt());
                    QLOG_INFO() << "ROI is adjusted from (" << fov[0].toInt() << "," << fov[1].toInt() << ") to (" << minVal << "," << minVal << ")";
                    const auto fovArea = AppEntity::Area(minVal, minVal);
                    experiment->SetFOV(fovArea);
                } else {
                    const auto fovArea = AppEntity::Area(fov[0].toInt(), fov[1].toInt());
                    experiment->SetFOV(fovArea);
                }
            } else {
                return false;
            }
        } else {
            experiment->SetFOV(AppEntity::Area::fromUM(100, 100));
        }

        if (json.contains(Key::VesselModel) && json[Key::VesselModel].isString()) {
            AppEntity::Vessel::Pointer vessel{};
            if(!vesselFilePath.isEmpty()) {
                const VesselIO::VesselReader vesselReader;
                vessel = vesselReader.Read(vesselFilePath);
            }
            else {
                auto repo = AppEntity::VesselRepo::GetInstance();
                vessel = repo->GetVesselByModel(json[Key::VesselModel].toString());
            }
            if(!vessel) {
                QLOG_ERROR() << "Vessel file can't be found : " << json[Key::VesselModel].toString();
                return false;
            }
            experiment->SetVesselInfo(vessel);
        } else {
            QLOG_ERROR() << "Vessel model information is missing";
            return false;
        }

        // parse settings
        if (json.contains(Key::ExperimentSettings) && json[Key::ExperimentSettings].isArray()) {
            ParseExperimentSettings(json[Key::ExperimentSettings].toArray());
        }

        if (json.contains(Key::SampleType) && json[Key::SampleType].isString()) {
            experiment->SetSampleTypeName(json[Key::SampleType].toString());
        }

        return true;
    }

    auto ExperimentReader::Impl::ParseShortInfo(const QJsonObject& json) -> bool {
        if (json.contains(Key::CreatedDate) && json[Key::CreatedDate].isString()) {
            experiment->SetCreatedDate(json[Key::CreatedDate].toString());
        }

        if (json.contains(Key::Status)) {
            experiment->SetProgress(AppEntity::ExperimentProgress::_from_integral(json[Key::Status].toInt()));
        }

        if (json.contains(Key::ID) && json[Key::ID].isString()) {
            experiment->SetID(json[Key::ID].toString());
        }

        if (json.contains(Key::Title) && json[Key::Title].isString()) {
            experiment->SetTitle(json[Key::Title].toString());
        }

        return true;
    }

    auto ExperimentReader::Impl::ParseExperimentSettings(const QJsonArray& settingArray) -> void {
        for (auto settingValue : settingArray) {
            ParseExperimentSetting(settingValue.toObject());
        }
    }

    auto ExperimentReader::Impl::ParseExperimentSetting(const QJsonObject& json) -> void {
        if (!json.contains(Key::VesselIndex)) return;

        const auto vesselIndex = json[Key::VesselIndex].toInt();

        // set well names
        if (json.contains(Key::WellNames) && json[Key::WellNames].isArray()) {
            auto nameArray = json[Key::WellNames].toArray();
            for (auto nameValue : nameArray) {
                auto nameObj = nameValue.toObject();
                if (nameObj.contains(Key::WellIndex) && nameObj.contains(Key::WellName)) {
                    experiment->SetWellName(vesselIndex,
                        nameObj[Key::WellIndex].toInt(),
                        nameObj[Key::WellName].toString()
                    );
                }
            }
        }

        // add well groups
        if (json.contains(Key::WellGroups) && json[Key::WellGroups].isArray()) {
            auto wellGroupArray = json[Key::WellGroups].toArray();
            ParseWellGroups(vesselIndex, wellGroupArray);
        }

        // add locations
        if (json.contains(Key::ImagingLocations) && json[Key::ImagingLocations].isArray()) {
            auto locationArray = json[Key::ImagingLocations].toArray();
            ParseLocations(vesselIndex, locationArray);
        }

        // set scenario
        if (json.contains(Key::ImagingScenario) && json[Key::ImagingScenario].isObject()) {
            auto scenarioObj = json[Key::ImagingScenario].toObject();
            ParseImagingScenario(vesselIndex, scenarioObj);
        }

        // set single imaging conditions
        if (json.contains(Key::SingleImagingConditions) && json[Key::SingleImagingConditions].isArray()) {
            auto jsonArray = json[Key::SingleImagingConditions].toArray();
            ParseSingleImagingConditions(vesselIndex, jsonArray);
        }

        // set ROI
        if (json.contains(Key::ROIListTitle) && json[Key::ROIListTitle].isArray()) {
            auto jsonArray = json[Key::ROIListTitle].toArray();
            ParseROIList(vesselIndex, jsonArray);
        }
    }

    auto ExperimentReader::Impl::ParseWellGroups(int vesselIndex, const QJsonArray& json)->void {
        auto GetWells = [](const QJsonArray& jsonArray) -> QList<AppEntity::WellGroup::Index> {
            QList<AppEntity::WellGroup::Index> wells;

            for (auto wellValue : jsonArray) {
                auto wellPosition = wellValue.toArray();
                if (wellPosition.size() != 2) {
                    continue;
                }

                wells << AppEntity::WellGroup::Index(wellPosition[0].toInt(), wellPosition[1].toInt());
            }

            return wells;
        };

        for (auto wellGroupValue : json) {
            auto wellGroupObj = wellGroupValue.toObject();

            AppEntity::WellGroup wellGroup;

            // set well group title
            if (wellGroupObj.contains(Key::WellGroupTitle) && wellGroupObj[Key::WellGroupTitle].isString()) {
                wellGroup.SetTitle(wellGroupObj[Key::WellGroupTitle].toString());
            }

            // set well group color
            if (wellGroupObj.contains(Key::WellGroupColor) && wellGroupObj[Key::WellGroupColor].isArray()) {
                auto wellColorArray = wellGroupObj[Key::WellGroupColor].toArray();
                if (wellColorArray.size() != 4) {
                    continue;
                }

                AppEntity::WellGroup::Color color;
                color.r = wellColorArray[0].toInt();
                color.g = wellColorArray[1].toInt();
                color.b = wellColorArray[2].toInt();
                color.a = wellColorArray[3].toInt();

                wellGroup.SetColor(color);
            }

            // add wells
            if (wellGroupObj.contains(Key::Wells) && wellGroupObj[Key::Wells].isArray()) {
                const auto wells = GetWells(wellGroupObj[Key::Wells].toArray());
                if (!wells.isEmpty()) {
                    wellGroup.AddWells(wells);
                }
            }

            experiment->SetWellGroup(vesselIndex, experiment->GetNewWellGroupIndex(), wellGroup);
        }
    }

    auto ExperimentReader::Impl::ParseROIList(int vesselIndex, const QJsonArray& jsonArray) -> void {
        for (const auto& roiListVal : jsonArray) {
            const auto object = roiListVal.toObject();
            const auto wellIndex = object[Key::ROIWellIndex].toInt();

            if (object.contains(Key::ROIList) && object[Key::ROIList].isArray()) {
                auto roiArray = object[Key::ROIList].toArray();
                for (const auto& roiVal : roiArray) {
                    auto roiObject = roiVal.toObject();
                    const auto roiIndex = roiObject[Key::ROIIndex].toInt();
                    const auto shape = AppEntity::ROIShape::_from_string(roiObject[Key::ROIShape].toString().toLatin1());
                    const auto name = roiObject[Key::ROIName].toString({});
                    const auto posArr = roiObject[Key::ROIPosition].toArray();
                    const auto sizeArr = roiObject[Key::ROISize].toArray();

                    if (posArr.size() == 2 && sizeArr.size() == 2) {
                        const auto pos = AppEntity::Position(posArr[0].toInt(), posArr[1].toInt(), 0);
                        const auto size = AppEntity::Area(sizeArr[0].toInt(), sizeArr[1].toInt());
                        const auto roi = std::make_shared<AppEntity::ROI>(pos, size, shape, name);
                        experiment->AddROI(vesselIndex, wellIndex, roiIndex, roi);
                    }
                }
            }
        }
    }

    auto ExperimentReader::Impl::ParseLocations(int vesselIndex, const QJsonArray& json)->void {
        for (auto imagingLocationValue : json) {
            auto imagingLocationObj = imagingLocationValue.toObject();
            if (!imagingLocationObj.contains(Key::WellIndex)) {
                continue;
            }

            const auto wellIndex = imagingLocationObj[Key::WellIndex].toInt();

            if (imagingLocationObj.contains(Key::Locations) && imagingLocationObj[Key::Locations].isArray()) {
                auto locationArray = imagingLocationObj[Key::Locations].toArray();
                const auto lastLocationIndex = imagingLocationObj[Key::LastLocationIndex].toInt(-1);
                AppEntity::LocationIndex locIndex = 0;

                for (auto locationValue : locationArray) {
                    auto locationObj = locationValue.toObject();

                    AppEntity::Location location;
                    if (locationObj.contains(Key::CenterPosition) && locationObj[Key::CenterPosition].isArray()) {
                        auto centerPosition = locationObj[Key::CenterPosition].toArray();
                        if (centerPosition.size() == 3) {
                            location.SetCenter(AppEntity::Position(
                                centerPosition[0].toDouble(),
                                centerPosition[1].toDouble(),
                                centerPosition[2].toDouble()
                            ));
                        }
                    }

                    if (locationObj.contains(Key::AcquisitionArea) && locationObj[Key::AcquisitionArea].isArray()) {
                        auto isTile = [=]()->bool {
                            if(!locationObj.contains(Key::TileType)) return false;
                            return locationObj[Key::TileType].toBool();
                        }();

                        auto acquisitionArea = locationObj[Key::AcquisitionArea].toArray();
                        if (acquisitionArea.size() == 2) {
                            location.SetArea(AppEntity::Area(
                                acquisitionArea[0].toDouble(),
                                acquisitionArea[1].toDouble()
                            ), isTile);
                        }
                    }

                    experiment->SetLastLocationIndex(vesselIndex, wellIndex, lastLocationIndex);

                    if(locationObj.contains(Key::LocationIndex)) {
                        const auto locationIndex = locationObj[Key::LocationIndex].toInt(); 
                        experiment->AddLocation(vesselIndex, wellIndex, locationIndex, location);
                    }
                    else {
                        experiment->AddLocation(vesselIndex, wellIndex, ++locIndex, location);
                    }
                }
            }
        }
    }

    auto ExperimentReader::Impl::ParseImagingScenario(int vesselIndex,const QJsonObject& json)->void {
        QList<AppEntity::ImagingSequence::Pointer> imagingSequences;
        if (json.contains(Key::ImagingSequences) && json[Key::ImagingSequences].isArray()) {
            auto imagingSequenceArray = json[Key::ImagingSequences].toArray();
            for (auto imagingSequenceValue : imagingSequenceArray) {
                auto imagingSequence = std::make_shared<AppEntity::ImagingSequence>();

                auto imagingSequenceObj = imagingSequenceValue.toObject();
                if (imagingSequenceObj.contains(Key::TimelapseInterval) && imagingSequenceObj.contains(Key::TimelapseCount)) {
                    imagingSequence->SetTimelapseCondition(
                        imagingSequenceObj[Key::TimelapseInterval].toInt(),
                        imagingSequenceObj[Key::TimelapseCount].toInt()
                    );
                }

                if (imagingSequenceObj.contains(Key::ImagingConditions) && imagingSequenceObj[Key::ImagingConditions].isArray()) {
                    auto imagingConditionArray = imagingSequenceObj[Key::ImagingConditions].toArray();
                    for (auto imagingConditionValue : imagingConditionArray) {
                        auto imagingConditionObj = imagingConditionValue.toObject();
                        if (!imagingConditionObj.contains(Key::ImagingModality)) {
                            continue;
                        }

                        auto modality = AppEntity::Modality::_from_string(imagingConditionObj[Key::ImagingModality].toString().toLatin1());

                        // set others condition values each modality
                        if (modality == +AppEntity::Modality::HT) {
                            imagingSequence->AddImagingCondition(std::make_shared<AppEntity::ImagingConditionHT>(imagingConditionObj));

                        } else if (modality == +AppEntity::Modality::FL) {
                            imagingSequence->AddImagingCondition(std::make_shared<AppEntity::ImagingConditionFL>(imagingConditionObj));

                        } else if (modality == +AppEntity::Modality::BF) {
                            imagingSequence->AddImagingCondition(std::make_shared<AppEntity::ImagingConditionBF>(imagingConditionObj));
                        }
                    }
                }

                imagingSequences << imagingSequence;
            }
        }

        QList<int> startTimes;
        if (json.contains(Key::SequenceStartTimes) && json[Key::SequenceStartTimes].isArray()) {
            auto startTimeArray = json[Key::SequenceStartTimes].toArray();
            for (auto startTimeValue : startTimeArray) {
                startTimes << startTimeValue.toInt();
            }
        }

        QList<QString> titles;
        if (json.contains(Key::SequenceTitles) && json[Key::SequenceTitles].isArray()) {
            auto titleArray = json[Key::SequenceTitles].toArray();
            for (auto titleValue : titleArray) {
                titles << titleValue.toString();
            }
        }

        auto scenario = std::make_shared<AppEntity::ImagingScenario>();
        for (auto index = 0; index < imagingSequences.size(); ++index) {
            if (index >= startTimes.size() || index >= titles.size()) {
                break;
            }

            scenario->AddSequence(startTimes[index], titles[index], imagingSequences[index]);
        }

        experiment->SetScenario(vesselIndex, scenario);
    }

    auto ExperimentReader::Impl::ParseSingleImagingConditions(int vesselIndex, const QJsonArray& jsonArray) -> void {
        Q_UNUSED(vesselIndex)
        for(auto object : jsonArray) {
            auto imagingType = AppEntity::ImagingType::_from_string(object[Key::ImagingType].toString().toLatin1());
            experiment->SetEnableSingleImaging(imagingType, object[Key::Enable].toBool());

            auto condition = [=]()->AppEntity::ImagingCondition::Pointer {
                switch(imagingType) {
                case AppEntity::ImagingType::HT3D:
                case AppEntity::ImagingType::HT2D:
                    return std::make_shared<AppEntity::ImagingConditionHT>(object[Key::ImagingCondition].toObject());
                case AppEntity::ImagingType::FL3D:
                case AppEntity::ImagingType::FL2D:
                    return std::make_shared<AppEntity::ImagingConditionFL>(object[Key::ImagingCondition].toObject());
                case AppEntity::ImagingType::BFGray:
                case AppEntity::ImagingType::BFColor:
                    return std::make_shared<AppEntity::ImagingConditionBF>(object[Key::ImagingCondition].toObject());
                }
                return nullptr;
            }();
            experiment->SetSingleImagingCondition(imagingType, condition);
        }
    }

    ExperimentReader::ExperimentReader() : d{ new Impl } {
    }

    ExperimentReader::~ExperimentReader() {
    }

    auto ExperimentReader::Read(const QString& path) const -> AppEntity::Experiment::Pointer {
        if (path.isEmpty()) {
            QLOG_ERROR() << "Experiment file path is not specified";
            return nullptr;
        }

        QFile file(path);
        if (!file.open(QIODevice::ReadOnly)) {
            QLOG_ERROR() << "Experiment file can't be opened : " << path;
            return nullptr;
        }

        d->experiment = std::make_shared<AppEntity::Experiment>();

        const auto readData = file.readAll();
        const QJsonDocument jsonDoc(QJsonDocument::fromJson(readData));
        const auto json = jsonDoc.object();

        if (!d->ParseAll(json)) {
            return nullptr;
        }

        return d->experiment;
    }

    auto ExperimentReader::ReadShortInfo(const QString& path) const -> AppEntity::Experiment::Pointer {
        if (path.isEmpty()) {
            return nullptr;
        }

        QFile file(path);
        if (!file.open(QIODevice::ReadOnly)) {
            return nullptr;
        }

        d->experiment = std::make_shared<AppEntity::Experiment>();

        const auto readData = file.readAll();
        const QJsonDocument jsonDoc(QJsonDocument::fromJson(readData));
        const auto json = jsonDoc.object();

        if (!d->ParseShortInfo(json)) {
            return nullptr;
        }

        return d->experiment;
    }

    auto ExperimentReader::SetVesselFilePath(const QString& filePath) -> void {
        d->vesselFilePath = filePath;
    }
}
