#pragma once
#include <memory>
#include <QMap>
#include <QString>

#include <Experiment.h>
#include <VesselMap.h>
#include <Vessel.h>
#include <RoiSetupVessel.h>

#include "HTXVesselMapUtilityExport.h"

namespace HTXpress::AppComponents::VesselMapUtility {
	class HTXVesselMapUtility_API Converter {
    public:
        static auto ToTCVesselMap(const AppEntity::Vessel::Pointer& vessel, 
                                  const QMap<AppEntity::WellIndex, QString>& wellNames)->TC::VesselMap::Pointer;
        static auto ToTCVesselMap(const int32_t& vesselIndex,
                                  const AppEntity::Experiment::Pointer& experiment,
                                  const AppEntity::Vessel::Pointer& vessel, 
                                  const QMap<AppEntity::WellIndex, QString>& wellNames)->TC::VesselMap::Pointer;
        static auto ToRoiSetupVesselMap(const int32_t& vesselIndex, 
                                        const AppEntity::Experiment::Pointer& experiment, 
                                        const AppEntity::Vessel::Pointer& vessel, 
                                        const QMap<AppEntity::WellIndex, QString>& wellNames)->RoiSetupDefinitions::RoiSetupVessel::Pointer;
    };
}