#include <AppEntityDefines.h>
#include <RoiSetupDefines.h>
#include <RegionOfInterest.h>

#include "VesselMapUtility.h"

namespace HTXpress::AppComponents::VesselMapUtility {
    auto Converter::ToTCVesselMap(const AppEntity::Vessel::Pointer& vessel,
                                const QMap<AppEntity::WellIndex, QString>& wellNames)->TC::VesselMap::Pointer {
        auto vesselmap = std::make_shared<TC::VesselMap>();
        vesselmap->SetHolderSize(vessel->GetSizeX(), vessel->GetSizeY());
        vesselmap->SetVesselSize(vessel->GetSizeX(), vessel->GetSizeY());
        vesselmap->SetVesselShape(TC::VesselShape::Rectangle);
        vesselmap->SetVessel(0, 0, 0, vessel->GetWellRows(), vessel->GetWellCols());

        vesselmap->SetWellSize(vessel->GetWellSizeX(), vessel->GetWellSizeY());
        vesselmap->SetWellShape([&vessel]()->TC::WellShape {
            TC::WellShape shape{ TC::WellShape::Circle };
            switch(vessel->GetWellShape()) {
            case AppEntity::WellShape::Circle:
                shape = TC::WellShape::Circle;
                break;
            case AppEntity::WellShape::Rectangle:
                shape = TC::WellShape::Rectangle;
                break;
            }
            return shape;
        }());
        
        auto wellIndices = vessel->GetWellIndices();
        std::for_each(wellIndices.begin(), wellIndices.end(), [=](AppEntity::WellIndex wellIndex) {
            auto row = vessel->GetWellRowIndex(wellIndex);
            auto column = vessel->GetWellColIndex(wellIndex);

            vesselmap->SetWell(
                wellIndex,
                row,
                column,
                vessel->GetWellPositionX(row, column),
                vessel->GetWellPositionY(row, column),
                wellNames[wellIndex]
            );
        });

        TC::ImagingAreaShape shape{ TC::ImagingAreaShape::Circle };
        switch(vessel->GetImagingAreaShape()) {
        case AppEntity::ImagingAreaShape::Circle:
            shape = TC::ImagingAreaShape::Circle;
            break;
        case AppEntity::ImagingAreaShape::Rectangle:
            shape = TC::ImagingAreaShape::Rectangle;
            break;
        }

        vesselmap->SetImagingArea(
            shape, 
            vessel->GetImagingAreaPositionX(),
            vessel->GetImagingAreaPositionY(), 
            vessel->GetImagingAreaSizeX(), 
            vessel->GetImagingAreaSizeY());

        return vesselmap;
    }

    auto Converter::ToTCVesselMap(const int32_t& vesselIndex, const AppEntity::Experiment::Pointer& experiment, const AppEntity::Vessel::Pointer& vessel, const QMap<AppEntity::WellIndex, QString>& wellNames) -> TC::VesselMap::Pointer {
        auto vesselMap = ToTCVesselMap(vessel, wellNames);
        const auto rois = experiment->GetROIs(vesselIndex);
        const auto tcRoiShape = [=](AppEntity::ROIShape entityShape) {
            switch(entityShape) {
            case AppEntity::ROIShape::Ellipse: return TC::RoiShape::Ellipse;
            case AppEntity::ROIShape::Rectangle: return TC::RoiShape::Rectangle;
                default: return TC::RoiShape::Ellipse;
            }
        };

        for(auto wIt = rois.cbegin(); wIt != rois.cend(); ++wIt) {
            const auto wellIndex = wIt.key();
            const auto roiMap = wIt.value();
            for(auto rIt = roiMap.cbegin(); rIt != roiMap.cend(); ++rIt) {
                const auto roiIndex = rIt.key();
                const auto name = rIt.value()->GetName();
                const auto shape = tcRoiShape(rIt.value()->GetShape());
                const auto pos = rIt.value()->GetCenter().toMM();
                const auto size = rIt.value()->GetSize().toMM();

                vesselMap->AddRoi(wellIndex, roiIndex, name, shape, pos.x, pos.y, size.width, size.height);
            }
        }
        return vesselMap;
    }

    auto Converter::ToRoiSetupVesselMap(const int32_t& vesselIndex, const AppEntity::Experiment::Pointer& experiment, const AppEntity::Vessel::Pointer& vessel, const QMap<AppEntity::WellIndex, QString>& wellNames) -> RoiSetupDefinitions::RoiSetupVessel::Pointer {
        using Shape = RoiSetupDefinitions::ItemShape;

        auto convertedVesselMap = std::make_shared<RoiSetupDefinitions::RoiSetupVessel>();

        const auto convertedVessel = std::make_shared<RoiSetupDefinitions::Vessel>();
        convertedVessel->name = vessel->GetModel();
        convertedVessel->position = {0, 0};
        convertedVessel->shape = Shape::Rectangle;
        convertedVessel->size = {vessel->GetSizeX(), vessel->GetSizeY()};

        QList<std::shared_ptr<RoiSetupDefinitions::Well>> convertedWells;
        const auto wellWidth = vessel->GetWellSizeX();
        const auto wellHeight = vessel->GetWellSizeY();
        const auto wellShape = [&vessel]()-> Shape {
            Shape shape{Shape::Ellipse};
            switch (vessel->GetWellShape()) {
                case AppEntity::WellShape::Circle: shape = Shape::Ellipse;
                    break;
                case AppEntity::WellShape::Rectangle: shape = Shape::Rectangle;
                    break;
                default: break;
            }
            return shape;
        }();

        auto roiMap = experiment->GetROIs(vesselIndex);
        for (const auto& wellIndex : vessel->GetWellIndices()) {
            const auto row = vessel->GetWellRowIndex(wellIndex);
            const auto column = vessel->GetWellColIndex(wellIndex);
            const auto x = vessel->GetWellPositionX(row, column);
            const auto y = vessel->GetWellPositionY(row, column);
            const auto name = wellNames[wellIndex];

            auto well = std::make_shared<RoiSetupDefinitions::Well>();
            well->index = wellIndex;
            well->array = {row, column};
            well->shape = wellShape;
            well->size = {wellWidth, wellHeight};
            well->position = {x, y};
            well->name = name;

            QList<std::shared_ptr<RoiSetupDefinitions::RegionOfInterest>> rois;
            for(auto it = roiMap[wellIndex].cbegin(); it != roiMap[wellIndex].cend(); ++it) {
                auto roi = std::make_shared<RoiSetupDefinitions::RegionOfInterest>();
                auto roiIndex = it.key();
                auto roiX = it.value()->GetCenter().toMM().x;
                auto roiY = it.value()->GetCenter().toMM().y;
                auto roiW = it.value()->GetSize().toMM().width;
                auto roiH = it.value()->GetSize().toMM().height;
                auto roiName = it.value()->GetName();
                auto roiShape = [&it]()->Shape {
                    auto entityShape = it.value()->GetShape();
                    auto convertedShape = Shape::Ellipse;

                    if(entityShape == +AppEntity::ROIShape::Ellipse) {
                        convertedShape = Shape::Ellipse;
                    }
                    else if(entityShape == +AppEntity::ROIShape::Rectangle) {
                        convertedShape = Shape::Rectangle;
                    }
                    return convertedShape;
                }();

                roi->SetIndex(roiIndex);
                roi->SetPosition({roiX,roiY});
                roi->SetSize({roiW,roiH});
                roi->SetName(roiName);
                roi->SetShape(roiShape);
                rois.push_back(roi);
            }
            well->rois = rois;
            convertedWells.push_back(well);
        }

        const auto convertedImagingArea = std::make_shared<RoiSetupDefinitions::ImagingArea>();
        const auto imagingAreaShape = [&vessel]()-> Shape {
            Shape shape{Shape::Ellipse};
            switch (vessel->GetImagingAreaShape()) {
                case AppEntity::ImagingAreaShape::Circle: shape = Shape::Ellipse;
                    break;
                case AppEntity::ImagingAreaShape::Rectangle: shape = Shape::Rectangle;
                    break;
                default: break;
            }
            return shape;
        }();

        convertedImagingArea->position = {vessel->GetImagingAreaPositionX(), vessel->GetImagingAreaPositionY()};
        convertedImagingArea->size = {vessel->GetImagingAreaSizeX(), vessel->GetImagingAreaSizeY()};

        convertedImagingArea->shape = imagingAreaShape;

        convertedVesselMap->SetVessel(convertedVessel);
        convertedVesselMap->SetWells(convertedWells);
        convertedVesselMap->SetImagingArea(convertedImagingArea);

        return convertedVesselMap;
    }
}
