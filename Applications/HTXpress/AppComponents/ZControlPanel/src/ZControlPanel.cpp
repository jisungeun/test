#include <QTimer>

#include "ui_ZControlPanel.h"
#include "ZControlPanel.h"

namespace HTXpress::AppComponents::ZControlPanel {
    struct Widget::Impl {
        Ui::ZControlPanel ui;
        Widget* p{ nullptr };
        QTimer timer;
        int32_t direction{ 1 };
        bool jogTriggered{ false };
        bool enableJog{ true };

        explicit Impl(Widget* p) : p{ p } {}

        auto SetJogStatus(bool started)->void;
    };

    auto Widget::Impl::SetJogStatus(bool started) -> void {
        if(started) {
            disconnect(ui.upBtn, SIGNAL(clicked()), nullptr, nullptr);
            disconnect(ui.downBtn, SIGNAL(clicked()), nullptr, nullptr);
        } else {
            connect(ui.upBtn, SIGNAL(clicked()), p, SLOT(onMoveUp()));
            connect(ui.downBtn, SIGNAL(clicked()), p, SLOT(onMoveDown()));
        }
    }

    Widget::Widget(QWidget* parent) : QWidget(parent), d{new Impl(this)} {
        d->ui.setupUi(this);

        d->ui.upBtn->setObjectName("tsx-zcontrolpanel-upbtn");
        d->ui.downBtn->setObjectName("tsx-zcontrolpanel-downbtn");
        d->ui.step->setObjectName("tsx-zcontrolpanel-step");

        connect(&d->timer, SIGNAL(timeout()), this, SLOT(onTimeout()));

        connect(d->ui.upBtn, SIGNAL(pressed()), this, SLOT(onMoveUp()));
        connect(d->ui.downBtn, SIGNAL(pressed()), this, SLOT(onMoveDown()));
        connect(d->ui.upBtn, SIGNAL(released()), this, SLOT(onStopUp()));
        connect(d->ui.downBtn, SIGNAL(released()), this, SLOT(onStopDown()));
    }

    Widget::~Widget() {
    }

    auto Widget::SetStepRange(double minValue, double maxValue)->void {
        d->ui.step->setRange(minValue, maxValue);
    }

    auto Widget::EnableJog(bool enable) -> void {
        d->enableJog = enable;
    }

    auto Widget::onTimeout()->void {
        emit sigStartJog(d->direction);
        d->timer.stop();
        d->jogTriggered = true;
    }

    auto Widget::onMoveUp() -> void {
        d->direction = 1;
        if(d->enableJog) d->timer.start(200);
    }

    auto Widget::onMoveDown() -> void {
        d->direction = -1;
        if(d->enableJog) d->timer.start(200);
    }

    auto Widget::onStopUp() -> void {
        if(!d->jogTriggered) {
            if(d->enableJog) d->timer.stop();
            emit sigMove(d->ui.step->value());
        } else {
            emit sigStopJog();
        }
        d->jogTriggered = false;
    }

    auto Widget::onStopDown() -> void {
        if(!d->jogTriggered) {
            if(d->enableJog) d->timer.stop();
            emit sigMove(0 - d->ui.step->value());
        } else {
            emit sigStopJog();
        }
        d->jogTriggered = false;
    }
}