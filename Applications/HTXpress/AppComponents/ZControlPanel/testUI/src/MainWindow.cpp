#include <QString>

#include "MainWindow.h"
#include "ui_MainWindow.h"

namespace HTXpress::AppComponents::ZControlPanel::Test {
	struct MainWindow::Impl {
		Ui::MainWindow ui;
	};

	MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{new Impl} {
		d->ui.setupUi(this);

		connect(d->ui.zcontrol, SIGNAL(sigMove(double)), this, SLOT(onMove(double)));
		connect(d->ui.zcontrol, SIGNAL(sigStartJog(int)), this, SLOT(onStartJog(int)));
		connect(d->ui.zcontrol, SIGNAL(sigStopJog()), this, SLOT(onStopJog()));
	}

	MainWindow::~MainWindow() {
	}

	auto MainWindow::onMove(double step) -> void {
		auto rows = d->ui.eventLogs->count();
		d->ui.eventLogs->insertItem(rows, QString("Move %1").arg(step));
		d->ui.eventLogs->scrollToItem(d->ui.eventLogs->item(rows+1));
    }

	auto MainWindow::onStartJog(int direction) -> void {
		auto rows = d->ui.eventLogs->count();
		d->ui.eventLogs->insertItem(rows, QString("Start Jog %1").arg(direction>0?"+":"-"));
		d->ui.eventLogs->scrollToItem(d->ui.eventLogs->item(rows+1));
    }

	auto MainWindow::onStopJog() -> void {
		auto rows = d->ui.eventLogs->count();
		d->ui.eventLogs->insertItem(rows, "Stop Jog");
		d->ui.eventLogs->scrollToItem(d->ui.eventLogs->item(rows+1));
    }
}
