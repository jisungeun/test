#pragma once

#include <memory>

#include <QMainWindow>

namespace HTXpress::AppComponents::ZControlPanel::Test {
	class MainWindow : public QMainWindow {
		Q_OBJECT

	public:
        explicit MainWindow(QWidget* parent = nullptr);
		~MainWindow() override;

	protected slots:
		auto onMove(double step)->void;
		auto onStartJog(int direction)->void;
		auto onStopJog()->void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

}
