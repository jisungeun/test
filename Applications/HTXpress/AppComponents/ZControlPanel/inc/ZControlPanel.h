#pragma once
#include <memory>
#include <QWidget>

#include "HTXZControlPanelExport.h"

namespace HTXpress::AppComponents::ZControlPanel {
    class HTXZControlPanel_API Widget : public QWidget {
        Q_OBJECT

    public:
        explicit Widget(QWidget* parent = nullptr);
        ~Widget() override;

        auto SetStepRange(double minValue, double maxValue)->void;
        auto EnableJog(bool enable)->void;

    protected slots:
        auto onTimeout()->void;
        auto onMoveUp()->void;
        auto onMoveDown()->void;
        auto onStopUp()->void;
        auto onStopDown()->void;

    signals:
        void sigMove(double step);
        void sigStartJog(int direction);
        void sigStopJog();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}