#define LOGGER_TAG "[HTXProfileCopier]"

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QDirIterator>
#include <QFileInfo>

#include <AppEntityDefines.h>
#include <TCLogger.h>

#include "ProfileCopier.h"

namespace HTXpress::AppComponents::ProfileCopier {
    struct ProfileCopier::Impl {
        QString sampleTypeName{};
        QString source{};
        QStringList destinations{};
        QStringList filters{};

        auto RemovePreviousProfiles(const QString& destination) const -> void;
    };

    ProfileCopier::ProfileCopier(const QString& sampleTypeName, const QString& source, const QStringList& destinations) : d{std::make_unique<Impl>()} {
        d->sampleTypeName = sampleTypeName;
        d->source = source;
        d->destinations = destinations;
        d->filters = QStringList() << QString("%1.*.%2").arg(sampleTypeName).arg(AppEntity::ProfileExtension);
    }

    ProfileCopier::~ProfileCopier() {
    }

    auto ProfileCopier::SetFileNameFilters(const QStringList& filters) -> void {
        d->filters = filters;
    }

    auto ProfileCopier::Copy(const QString& destination) const -> bool {
        const QDir dir(d->source);

        const auto profileFiles = dir.entryList(d->filters, QDir::Files);

        d->RemovePreviousProfiles(destination);

        if (!QDir().exists(destination)) {
            const auto isCreated = QDir().mkpath(destination);
            if (!isCreated) {
                QLOG_ERROR() << "Failed to create folder:" << destination;
                return false;
            }
        }

        for (const auto& profileFile : profileFiles) {
            const auto srcProfileFile = d->source + QDir::separator() + profileFile;
            const auto dstProfileFile = destination + QDir::separator() + profileFile;
            if (!QFile::copy(srcProfileFile, dstProfileFile)) {
                QLOG_ERROR() << "Failed to copy" << srcProfileFile << "to" << dstProfileFile;
                return false;
            }
        }

        return true;
    }

    auto ProfileCopier::Copy() const -> QStringList {
        QStringList copyFailedDestinations;

        for (const auto& destination : d->destinations) {
            if (!Copy(destination)) {
                copyFailedDestinations << destination;
            }
        }

        return copyFailedDestinations;
    }

    auto ProfileCopier::Impl::RemovePreviousProfiles(const QString& destination) const -> void {
        const auto prfFilter = QStringList() << QString("*.%1").arg(AppEntity::ProfileExtension);
        QDirIterator it(destination, prfFilter, QDir::Files);
        while (it.hasNext()) {
            auto filePath = it.next();
            QFileInfo fileInfo(filePath);
            if (fileInfo.exists() && fileInfo.isFile()) {
                if(QFile::remove(filePath)) {
                    QLOG_INFO() << "Previous profile" << filePath << "has been removed.";
                }
            }
        }
    }
}
