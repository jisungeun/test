#pragma once
#include <memory>
#include <QString>
#include "HTXProfileCopierExport.h"

namespace HTXpress::AppComponents::ProfileCopier {
    class HTXProfileCopier_API ProfileCopier {
    public:
        using Self = ProfileCopier;
        using Pointer = std::shared_ptr<Self>;

        ProfileCopier(const QString& sampleTypeName, const QString& source, const QStringList& destinations);
        ~ProfileCopier();

        auto SetFileNameFilters(const QStringList& filters) -> void;

        auto Copy(const QString& destination) const -> bool;
        auto Copy() const -> QStringList;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
