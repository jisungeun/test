#pragma once
#include <memory>

#include "HTXROISetupDefinitionsExport.h"
#include "RoiSetupDefines.h"

namespace HTXpress::AppComponents::RoiSetupDefinitions {
    class HTXROISetupDefinitions_API RegionOfInterest final {
    public:
        using Self = RegionOfInterest;
        using Pointer = std::shared_ptr<Self>;

        RegionOfInterest();
        ~RegionOfInterest();

        bool operator<(const RegionOfInterest& other) const;
        bool operator<=(const RegionOfInterest& other) const;
        bool operator>(const RegionOfInterest& other) const;
        bool operator>=(const RegionOfInterest& other) const;

        auto operator==(const RegionOfInterest& other) const -> bool;
        auto operator!=(const RegionOfInterest& other) const -> bool;

        auto SetIndex(const int32_t& index) -> void;
        auto GetIndex() const -> int32_t;

        auto SetPosition(const Position& position) -> void;
        auto SetPosition(const double& x, const double& y) -> void; // well's center position in mm;
        auto GetPosition() const -> Position;
        auto GetX() const -> double;
        auto GetY() const -> double;

        auto SetSize(const double& w, const double& h) -> void;
        auto SetSize(const Size& size) -> void;
        auto GetSize() const -> Size;
        auto GetWidth() const -> double;
        auto GetHeight() const -> double;

        auto SetShape(const ItemShape& shape) -> void;
        auto GetShape() const -> ItemShape;

        auto SetName(const QString& name) -> void;
        auto GetName() const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
