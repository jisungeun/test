﻿#pragma once
#include <memory>

#include "HTXROISetupDefinitionsExport.h"
#include "RoiSetupDefines.h"

namespace HTXpress::AppComponents::RoiSetupDefinitions {
    class HTXROISetupDefinitions_API RoiSetupVessel final {
    public:
        using Self = RoiSetupVessel;
        using Pointer = std::shared_ptr<Self>;

        RoiSetupVessel();
        ~RoiSetupVessel();

        auto SetVessel(const std::shared_ptr<Vessel>& vessel) -> void;
        auto SetWells(const QList<std::shared_ptr<Well>>& wells) -> void;
        auto SetImagingArea(const std::shared_ptr<ImagingArea>& imagingArea) -> void;

        auto GetVessel() const -> std::shared_ptr<Vessel>;
        auto GetWells() const -> QList<std::shared_ptr<Well>>&;
        auto GetImagingArea() const -> std::shared_ptr<ImagingArea>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
