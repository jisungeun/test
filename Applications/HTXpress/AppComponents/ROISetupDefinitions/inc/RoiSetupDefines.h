﻿#pragma once
#include <memory>

#include <QMap>
#include <QList>

#include <enum.h>

#include "HTXROISetupDefinitionsExport.h"

namespace HTXpress::AppComponents::RoiSetupDefinitions {
    BETTER_ENUM(ItemShape, int32_t, None, Rectangle, Ellipse);
    class RegionOfInterest;

    using WellIndex = int32_t;
    using ROIIndex = int32_t;
    using WellPosName = QString;

    using ROIs = QMap<WellIndex, QList<std::shared_ptr<RegionOfInterest>>>;
    using Range = QPair<double, double>; // QPair<minVal, maxVal>

    struct HTXROISetupDefinitions_API Position {
        double x{};
        double y{};

        auto operator==(const Position& other) const -> bool {
            const double epsilon = 1e-9;
            return std::abs(x - other.x) < epsilon && 
                std::abs(y - other.y) < epsilon;
        }

        auto operator!=(const Position& other) const -> bool {
            return !(*this == other);
        }

        auto toUM() const -> Position {
            return {x * 1'000., y * 1'000.};
        }

        auto toNM() const -> Position {
            return {x * 1'000'000., y * 1'000'000.};
        }
    };

    struct HTXROISetupDefinitions_API Size {
        double w{};
        double h{};

        auto operator==(const Size& other) const -> bool {
            const double epsilon = 1e-9;
            return std::abs(w - other.w) < epsilon && 
                std::abs(h - other.h) < epsilon;
        }

        auto operator!=(const Size& other) const -> bool {
            return !(*this == other);
        }

        auto toUM() const -> Size {
            return {w * 1'000., h * 1'000.};
        }

        auto toNM() const -> Size {
            return {w * 1'000'000., h * 1'000'000.};
        }
    };

    struct HTXROISetupDefinitions_API ArrayIndex {
        int32_t row{};
        int32_t col{};
    };

    struct HTXROISetupDefinitions_API Vessel {
        QString name{};
        Position position{};
        Size size{};
        ItemShape::_enumerated shape{};
    };

    struct HTXROISetupDefinitions_API Well {
        WellIndex index{};
        ArrayIndex array{}; // row, column
        Position position{};
        Size size{};
        ItemShape::_enumerated shape{};
        QString name{};
        QList<std::shared_ptr<RegionOfInterest>> rois{};
    };

    struct HTXROISetupDefinitions_API ImagingArea {
        Position position{};
        Size size{};
        ItemShape::_enumerated shape{};
    };
}
