﻿#include "RoiSetupVessel.h"

namespace HTXpress::AppComponents::RoiSetupDefinitions {
    struct RoiSetupVessel::Impl {
        std::shared_ptr<Vessel> vessel{};
        QList<std::shared_ptr<Well>> wells{};
        std::shared_ptr<ImagingArea> imagingArea{};
    };

    RoiSetupVessel::RoiSetupVessel() : d{std::make_unique<Impl>()} {
    }

    RoiSetupVessel::~RoiSetupVessel() {
    }

    auto RoiSetupVessel::SetVessel(const std::shared_ptr<Vessel>& vessel) -> void {
        d->vessel = vessel;
    }

    auto RoiSetupVessel::SetWells(const QList<std::shared_ptr<Well>>& wells) -> void {
        d->wells = wells;
    }

    auto RoiSetupVessel::SetImagingArea(const std::shared_ptr<ImagingArea>& imagingArea) -> void {
        d->imagingArea = imagingArea;
    }

    auto RoiSetupVessel::GetVessel() const -> std::shared_ptr<Vessel> {
        return d->vessel;
    }

    auto RoiSetupVessel::GetWells() const -> QList<std::shared_ptr<Well>>& {
        return d->wells;
    }

    auto RoiSetupVessel::GetImagingArea() const -> std::shared_ptr<ImagingArea> {
        return d->imagingArea;
    }
}
