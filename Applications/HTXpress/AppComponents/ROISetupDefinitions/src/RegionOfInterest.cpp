#include "RegionOfInterest.h"

namespace HTXpress::AppComponents::RoiSetupDefinitions {
    struct RegionOfInterest::Impl {
        int32_t index{};
        Position position{};
        Size size{};
        ItemShape::_enumerated shape{};
        QString name{};
    };

    RegionOfInterest::RegionOfInterest() : d{std::make_unique<Impl>()} {
    }

    RegionOfInterest::~RegionOfInterest() = default;

    bool RegionOfInterest::operator<(const RegionOfInterest& other) const {
        return d->index < other.GetIndex();
    }

    bool RegionOfInterest::operator<=(const RegionOfInterest& other) const {
        return d->index <= other.GetIndex();
    }

    bool RegionOfInterest::operator>(const RegionOfInterest& other) const {
        return d->index > other.GetIndex();
    }

    bool RegionOfInterest::operator>=(const RegionOfInterest& other) const {
        return d->index >= other.GetIndex();
    }

    auto RegionOfInterest::operator==(const RegionOfInterest& other) const -> bool {
        return d->index == other.d->index && 
            d->position == other.d->position && 
            d->size == other.d->size && 
            d->shape == other.d->shape && 
            d->name == other.d->name;
    }

    auto RegionOfInterest::operator!=(const RegionOfInterest& other) const -> bool {
        return !(*this == other);
    }

    auto RegionOfInterest::SetIndex(const int32_t& index) -> void {
        d->index = index;
    }

    auto RegionOfInterest::GetIndex() const -> int32_t {
        return d->index;
    }

    auto RegionOfInterest::SetPosition(const Position& position) -> void {
        d->position = position;
    }

    auto RegionOfInterest::SetPosition(const double& x, const double& y) -> void {
        d->position = {x, y};
    }

    auto RegionOfInterest::GetPosition() const -> Position {
        return d->position;
    }

    auto RegionOfInterest::GetX() const -> double {
        return d->position.x;
    }

    auto RegionOfInterest::GetY() const -> double {
        return d->position.y;
    }

    auto RegionOfInterest::SetSize(const double& w, const double& h) -> void {
        d->size = {w, h};
    }

    auto RegionOfInterest::SetSize(const Size& size) -> void {
        d->size = size;
    }

    auto RegionOfInterest::GetSize() const -> Size {
        return d->size;
    }

    auto RegionOfInterest::GetWidth() const -> double {
        return d->size.w;
    }

    auto RegionOfInterest::GetHeight() const -> double {
        return d->size.h;
    }

    auto RegionOfInterest::SetShape(const ItemShape& shape) -> void {
        d->shape = shape;
    }

    auto RegionOfInterest::GetShape() const -> ItemShape {
        return d->shape;
    }

    auto RegionOfInterest::SetName(const QString& name) -> void {
        d->name = name;
    }

    auto RegionOfInterest::GetName() const -> QString {
        return d->name;
    }
}
