﻿#include <catch2/catch.hpp>

#include <RegionOfInterest.h>

namespace HTXpress::AppComponents::RoiSetupDefinitions::Test {
    TEST_CASE("RegionOfInterest basic interface") {
        RegionOfInterest roi;

        roi.SetIndex(1);
        REQUIRE(roi.GetIndex() == 1);

        roi.SetPosition(10.0, 20.0);
        REQUIRE(roi.GetPosition().x == 10.0);
        REQUIRE(roi.GetPosition().y == 20.0);

        roi.SetSize(100.0, 200.0);
        REQUIRE(roi.GetSize().w == 100.0);
        REQUIRE(roi.GetSize().h == 200.0);

        roi.SetShape(ItemShape::Ellipse);
        REQUIRE(roi.GetShape() == +ItemShape::Ellipse);

        roi.SetName("Sample ROI");
        REQUIRE(roi.GetName() == "Sample ROI");
    }
    TEST_CASE("RegionOfInterest operator") {
        RegionOfInterest roi1;
        roi1.SetIndex(1);
        roi1.SetPosition(10.0, 20.0);
        roi1.SetSize(100.0, 200.0);
        roi1.SetShape(ItemShape::Ellipse);
        roi1.SetName("Sample ROI 1");

        RegionOfInterest roi2;
        roi2.SetIndex(2);
        roi2.SetPosition(30.0, 40.0);
        roi2.SetSize(300.0, 400.0);
        roi2.SetShape(ItemShape::Rectangle);
        roi2.SetName("Sample ROI 2");

        REQUIRE(roi1 < roi2);
        REQUIRE_FALSE(roi1 > roi2);
        REQUIRE(roi1 <= roi2);
        REQUIRE_FALSE(roi1 >= roi2);
        REQUIRE_FALSE(roi1 == roi2);
        REQUIRE(roi1 != roi2);
    }
}