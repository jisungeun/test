#include <catch2/catch.hpp>

#include <QDir>
#include <QList>
#include <QString>
#include <QTemporaryFile>

#include <ProjectReader.h>
#include <ProjectWriter.h>

using namespace HTXpress::AppComponents::ProjectIO;

namespace ProjectIOTest {
    constexpr char readFileName[] = "projectIOTest.json";

    constexpr char answerTitle[] = "TestProject";
    constexpr char answerDescription[] = "This is a project for testing";

    TEST_CASE("Project File IO") {
        auto originProject = std::make_shared<HTXpress::AppEntity::Project>();
        originProject->SetTitle(answerTitle);
        originProject->SetDescription(answerDescription);

        auto testFilePath = QDir::tempPath() + "/" + readFileName;

        ProjectWriter writer;
        ProjectReader reader;

        SECTION("Write failure") {
            REQUIRE_FALSE(writer.Write("", originProject));
            REQUIRE_FALSE(writer.Write(testFilePath, nullptr));
        }

        SECTION("Write success") {
            REQUIRE(writer.Write(testFilePath, originProject));

            SECTION("Read failure") {
                auto readProject = std::make_shared<HTXpress::AppEntity::Project>();

                QList<std::pair<QString, HTXpress::AppEntity::Project::Pointer>> failureData;
                failureData << std::make_pair("/notexistpath", nullptr);
                failureData << std::make_pair("/notexistpath", readProject);
                failureData << std::make_pair(testFilePath, nullptr);

                for (auto data : failureData) {
                    CHECK_FALSE(reader.Read(data.first, data.second));
                }
            }

            SECTION("Read success") {
                auto project = std::make_shared<HTXpress::AppEntity::Project>();

                bool result = reader.Read(testFilePath, project);
                REQUIRE(result);

                CHECK(project->GetTitle() == originProject->GetTitle());
                CHECK(project->GetDescription() == originProject->GetDescription());
            }
        }

        if (QFile::exists(testFilePath)) {
            QFile::remove(testFilePath);
        }
    }
}