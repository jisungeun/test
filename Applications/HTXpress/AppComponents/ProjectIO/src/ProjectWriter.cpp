#include "ProjectWriter.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include "ProjectIODefines.h"

namespace HTXpress::AppComponents::ProjectIO {
    struct ProjectWriter::Impl {
    };

    ProjectWriter::ProjectWriter() : d{ new Impl } {
    }

    ProjectWriter::~ProjectWriter() {
    }

    auto ProjectWriter::Write(const QString& path, const AppEntity::Project::Pointer& project) -> bool {
        if (path.isEmpty() || project == nullptr) {
            return false;
        }

        QJsonObject json;
        json[Key::Title] = project->GetTitle();
        json[Key::Description] = project->GetDescription();

        QFile file(path);
        if (!file.open(QIODevice::WriteOnly)) {
            return false;
        }

        if (file.write(QJsonDocument(json).toJson()) < -1) {
            return false;
        }

        return true;
    }
}