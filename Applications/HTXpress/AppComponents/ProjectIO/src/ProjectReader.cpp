#include "ProjectReader.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include "ProjectIODefines.h"

namespace HTXpress::AppComponents::ProjectIO {
    struct ProjectReader::Impl {
    };

    ProjectReader::ProjectReader() : d{ new Impl } {
    }

    ProjectReader::~ProjectReader() {
    }

	auto ProjectReader::Read(const QString& path, const AppEntity::Project::Pointer& project) -> bool {
        if (path.isEmpty() || project == nullptr) {
            return false;
        }

        QFile file(path);
        if (!file.open(QIODevice::ReadOnly)) {
            return false;
        }

        const auto readData = file.readAll();
        const QJsonDocument jsonDoc(QJsonDocument::fromJson(readData));
        
        const auto json = jsonDoc.object();

        if (json.contains(Key::Title) && json[Key::Title].isString()) {
            project->SetTitle(json[Key::Title].toString());
        }

        if (json.contains(Key::Description) && json[Key::Description].isString()) {
            project->SetDescription(json[Key::Description].toString());
        }

        return true;
    }
}