#pragma once

namespace HTXpress::AppComponents::ProjectIO {
    namespace Key {
        constexpr char Title[] = "projectTitle";
        constexpr char Description[] = "projectDescription";
    }
}
