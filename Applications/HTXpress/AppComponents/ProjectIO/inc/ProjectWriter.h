#pragma once

#include <memory>

#include <QString>

#include <Project.h>

#include "HTXProjectIOExport.h"

namespace HTXpress::AppComponents::ProjectIO {
	class HTXProjectIO_API ProjectWriter {
	public:
		ProjectWriter();
		~ProjectWriter();

		auto Write(const QString& path, const AppEntity::Project::Pointer& project) ->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
