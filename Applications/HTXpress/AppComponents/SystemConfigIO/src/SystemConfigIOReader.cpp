#define LOGGER_TAG "[SytemConfigReader]"
#include <QSettings>
#include <QFileInfo>
#include <QColor>

#include <TCLogger.h>
#include <AppEntityDefines.h>

#include "SystemConfigIODefines.h"
#include "SystemConfigIOReader.h"

namespace HTXpress::AppComponents::SystemConfigIO {
    auto CheckField(QSettings& qs, const char key[]) ->bool {
        return qs.contains(key);
    }

    auto ReadField(QSettings& qs, const char key[])->QVariant {
        if(!qs.contains(key)) throw std::runtime_error(std::string("Missing key in system config: ") + key);
        return qs.value(key);
    }

    auto BeginReadArray(QSettings& qs, const char key[])->int32_t {
        const auto size = qs.beginReadArray(key);
        if(size == 0) throw std::runtime_error(std::string("Missing parameters in system config: ") + key);
        return size;
    }

    auto CreateSample(const QString& path)->void {
        const QString samplePath{ QString("%1.sample").arg(path) };
        QFile::remove(samplePath);

        QSettings qs(samplePath, QSettings::IniFormat);
        qs.setValue(Key::dataDir, "D:/Temp");
        qs.setValue(Key::model, "SampleModel");
        qs.setValue(Key::serial, "NoSerial");
        qs.setValue(Key::minSpace, 200);

        qs.setValue(Key::mcuPort, 1);
        qs.setValue(Key::mcuBaudrate, 115200);

        qs.setValue(Key::imagingCamSerial, "2110682");
        qs.setValue(Key::condenserCamSerial, "1002321");

        qs.setValue(Key::systemCenterX, 1.0);
        qs.setValue(Key::systemCenterY, 1.0);
        qs.setValue(Key::tileOverlap, 1);
        qs.setValue(Key::afTime, 1.0);
        qs.setValue(Key::flOutputRangeMin, 0);
        qs.setValue(Key::flOutputRangeMax, 255);
        qs.setValue(Key::bfLightIntensity, 11);
        qs.setValue(Key::previewLightIntensity, 95);
        qs.setValue(Key::previewGainCoeffA, 14.377);
        qs.setValue(Key::previewGainCoeffB, 0.1152);
        qs.setValue(Key::previewGainCoeffP, 12.735);
        qs.setValue(Key::previewGainCoeffQ, -2.5133);
        qs.setValue(Key::axisCompensationX, 1.0);
        qs.setValue(Key::axisCompensationY, 1.0);

        qs.beginWriteArray(Key::flChannel);
        for(auto idx=0; idx<3; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(Key::flChannelIdx, idx);
            qs.setValue(Key::flChannelName, QString("Channel_%1").arg(idx+1));
            qs.setValue(Key::flChannelExcitationWavelength, 500+idx*10);
            qs.setValue(Key::flChannelExcitationBandwidth, 50+idx*10);
            qs.setValue(Key::flChannelEmissionWavelength, 400+idx*10);
            qs.setValue(Key::flChannelEmissionBandwidth, 40+idx*10);
            qs.setValue(Key::flChannelColor, QColor(100+idx*10,150+idx*10,40+idx*10));
        }
        qs.endArray();

        qs.beginWriteArray(Key::htIlluminations);
        for(auto idx=0; idx<3; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(Key::NA, 0.5+idx*0.05);
            qs.setValue(Key::patternIndex, idx);
        }
        qs.endArray();

        qs.setValue(Key::afLensID, 6);
        qs.setValue(Key::afInFocusRange, 350);
        qs.setValue(Key::afDirection, 1);
        qs.setValue(Key::afLoopInterval, 40000);
        qs.setValue(Key::afMaxTrialCount, 20);
        qs.setValue(Key::afSensorValueMin, -70000);
        qs.setValue(Key::afSensorValueMax, 85000);
        qs.setValue(Key::afResolutionMul, 2);
        qs.setValue(Key::afResolutionDiv, 230);
        qs.setValue(Key::afDefaultTarget, -15348);
        qs.setValue(Key::afScanReady, 1.4);
        qs.setValue(Key::afMaxCompPulse, 1000);

        qs.beginWriteArray(Key::htScanParameter);
        for(auto idx=0; idx<4; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(Key::htScanNA, 0.11*idx);
            qs.setValue(Key::htScanStep, 20);
            qs.setValue(Key::htScanSlices, 34);
        }
        qs.endArray();

        qs.beginWriteArray(Key::conAfParameter);
        for(auto idx=0; idx<4; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(Key::conAfNA, 0.11*idx);
            qs.setValue(Key::conAfPatternIndex, 20+idx);
            qs.setValue(Key::conAfIntensity, 50);
            qs.setValue(Key::conAfZOffset, 0);
        }
        qs.endArray();

        qs.beginWriteArray(Key::illumCalParameter);
        for(auto idx=0; idx<4; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(Key::illumCalNA, 0.11*idx);
            qs.setValue(Key::illumCalIntensityStart, 85);
            qs.setValue(Key::illumCalIntensityStep, -10);
            qs.setValue(Key::illumCalThreshold, 120);
        }
        qs.endArray();

        qs.setValue(Key::cafScanStartPos, 320000);
        qs.setValue(Key::cafScanInterval, 2133);
        qs.setValue(Key::cafScanSlices, 40);
    }

    struct Reader::Impl {
        QString error;

        auto SetError(const QString& message)->void;
    };

    auto Reader::Impl::SetError(const QString& message) -> void {
        error = message;
        QLOG_ERROR() << message;
    }

    Reader::Reader() : d{new Impl} {
    }

    Reader::~Reader() {
    }

    auto Reader::Read(const QString& path, AppEntity::SystemConfig::Pointer& config) const -> bool {
        if(path.isEmpty()) {
            d->SetError("No system configuration file path is specified");
            return false;
        }

        if(!QFile::exists(path)) {
            d->SetError(QString("System configuration file does not exist at %1").arg(path));
            CreateSample(path);
            return false;
        }

        QSettings qs(path, QSettings::IniFormat);

        try {
            config->SetDataDir(ReadField(qs, Key::dataDir).toString());
            config->SetModel(ReadField(qs, Key::model).toString());
            config->SetSerial(ReadField(qs, Key::serial).toString());
            config->SetMinRequiredSpace(ReadField(qs, Key::minSpace).toInt());

            config->SetMCUComPort(ReadField(qs, Key::mcuPort).toInt(),
                                  ReadField(qs, Key::mcuBaudrate).toInt());

            config->SetImagingCameraSerial(ReadField(qs, Key::imagingCamSerial).toString());
            config->SetCondenserCameraSerial(ReadField(qs, Key::condenserCamSerial).toString());

            const auto centerPosition = AppEntity::Position::fromMM(ReadField(qs, Key::systemCenterX).toDouble(),
                                                                    ReadField(qs, Key::systemCenterY).toDouble(),
                                                                    0);
            config->SetSystemCenter(centerPosition);
            config->SetTileScanOverlap(ReadField(qs, Key::tileOverlap).toUInt());
            config->SetAutoFocusTime(ReadField(qs, Key::afTime).toDouble());
            config->SetFlOutputRange(ReadField(qs, Key::flOutputRangeMin).toInt(),
                                     ReadField(qs, Key::flOutputRangeMax).toInt());
            config->SetBFLightIntensity(ReadField(qs, Key::bfLightIntensity).toInt());
            config->SetPreviewLightIntensity(ReadField(qs, Key::previewLightIntensity).toInt());
            config->SetPreviewGainCoefficient(ReadField(qs, Key::previewGainCoeffA).toDouble(),
                                              ReadField(qs, Key::previewGainCoeffB).toDouble(),
                                              ReadField(qs, Key::previewGainCoeffP).toDouble(),
                                              ReadField(qs, Key::previewGainCoeffQ).toDouble());
            config->SetAxisCompensation(AppEntity::Axis::X, ReadField(qs, Key::axisCompensationX).toDouble());
            config->SetAxisCompensation(AppEntity::Axis::Y, ReadField(qs, Key::axisCompensationY).toDouble());

            const auto flChannels = BeginReadArray(qs, Key::flChannel);
            for(auto arrIndex=0; arrIndex < flChannels; arrIndex++) {
                AppEntity::FLChannel flChannel;

                qs.setArrayIndex(arrIndex);
                flChannel.SetName(ReadField(qs, Key::flChannelName).toString());
                flChannel.SetExcitation(ReadField(qs, Key::flChannelExcitationWavelength).toUInt());
                flChannel.SetExcitationBandwidth(ReadField(qs, Key::flChannelExcitationBandwidth).toUInt());
                flChannel.SetEmission(ReadField(qs, Key::flChannelEmissionWavelength).toUInt());
                flChannel.SetEmissionBandwidth(ReadField(qs, Key::flChannelEmissionBandwidth).toUInt());
                flChannel.SetColor(ReadField(qs, Key::flChannelColor).value<QColor>());

                config->SetFLChannel(ReadField(qs, Key::flChannelIdx).toInt(), flChannel);
            }
            qs.endArray();

            const auto htPatterns = BeginReadArray(qs, Key::htIlluminations);
            for(auto ptnIndex=0; ptnIndex<htPatterns; ptnIndex++) {
                qs.setArrayIndex(ptnIndex);
                const auto NA = ReadField(qs, Key::NA).toDouble();
                const auto patternIndex = ReadField(qs, Key::patternIndex).toInt();
                config->SetHTIlluminationPattern(NA, patternIndex);
            }
            qs.endArray();

            using AFParam = AppEntity::AutoFocusParameter;
            config->SetAutofocusParameter(AFParam::LensID, ReadField(qs, Key::afLensID).toInt());
            config->SetAutofocusParameter(AFParam::InFocusRange, ReadField(qs, Key::afInFocusRange).toInt());
            config->SetAutofocusParameter(AFParam::Direction, ReadField(qs, Key::afDirection).toInt());
            config->SetAutofocusParameter(AFParam::LoopInterval, ReadField(qs, Key::afLoopInterval).toInt());
            config->SetAutofocusParameter(AFParam::MaxTrialCount, ReadField(qs, Key::afMaxTrialCount).toInt());
            config->SetAutofocusParameter(AFParam::MinSensorValue, ReadField(qs, Key::afSensorValueMin).toInt());
            config->SetAutofocusParameter(AFParam::MaxSensorValue, ReadField(qs, Key::afSensorValueMax).toInt());
            config->SetAutofocusParameter(AFParam::ResolutionMul, ReadField(qs, Key::afResolutionMul).toInt());
            config->SetAutofocusParameter(AFParam::ResolutionDiv, ReadField(qs, Key::afResolutionDiv).toInt());
            config->SetAutofocusParameter(AFParam::DefaultTargetValue, ReadField(qs, Key::afDefaultTarget).toInt());
            config->SetAutofocusReadyPos(ReadField(qs, Key::afScanReady).toDouble());
            config->SetAutofocusParameter(AFParam::MaxCompensationPulse, ReadField(qs, Key::afMaxCompPulse).toInt());

            const auto htScanParameters = BeginReadArray(qs, Key::htScanParameter);
            for(auto idx=0; idx<htScanParameters; idx++) {
                qs.setArrayIndex(idx);
                const auto NA = ReadField(qs, Key::htScanNA).toDouble();
                const auto step = ReadField(qs, Key::htScanStep).toInt();
                const auto slices = ReadField(qs, Key::htScanSlices).toInt();
                config->SetHTScanParameter(NA, step, slices);
            }
            qs.endArray();

            const auto condAfParameters = BeginReadArray(qs, Key::conAfParameter);
            for(auto idx=0; idx<condAfParameters; idx++) {
                qs.setArrayIndex(idx);
                const auto NA = ReadField(qs, Key::conAfNA).toDouble();
                const auto pattenIndex = ReadField(qs, Key::conAfPatternIndex).toInt();
                const auto intensity = ReadField(qs, Key::conAfIntensity).toInt();
                const auto zOffset = [&]()->double {
                    //TODO Remove this temp treat after all of versions before TSX-1.3.0 are deprecated... 
                    //To support previous versions. Refer to HTX-2168
                    if(CheckField(qs, Key::conAfZOffset)) return ReadField(qs, Key::conAfZOffset).toDouble();
                    return 0;
                }();
                config->SetCondenserAFParameter(NA, pattenIndex, intensity, zOffset);
            }
            qs.endArray();

            const auto illumCalParameters = BeginReadArray(qs, Key::illumCalParameter);
            for(auto idx=0; idx<illumCalParameters; idx++) {
                qs.setArrayIndex(idx);
                const auto NA = ReadField(qs, Key::illumCalNA).toDouble();
                const auto start = ReadField(qs, Key::illumCalIntensityStart).toInt();
                const auto step = ReadField(qs, Key::illumCalIntensityStep).toInt();
                const auto threshold = ReadField(qs, Key::illumCalThreshold).toInt();
                config->SetIlluminationCalibrationParameter(NA, start, step, threshold);
            }
            qs.endArray();

            //TODO Refer to HTX-2507/HTXIS-189
            try {
                const auto start = ReadField(qs, Key::cafScanStartPos).toInt();
                const auto interval = ReadField(qs, Key::cafScanInterval).toInt();
                const auto slices = ReadField(qs, Key::cafScanSlices).toInt();
                config->SetCondenserAFScanParameter(start, interval, slices);
                QLOG_INFO() << "Condenser AF Scan parameters are overriden by system configuration";
            } catch(...) {
                config->ClearCondenserAFScanParameter();
                QLOG_INFO() << "There are no AF Scan parameters in system configuration file";
            }
        } catch(std::exception& ex) {
            d->SetError(QString("It fails to read system configuration. Error=%1").arg(ex.what()));
            CreateSample(path);
            return false;
        }

        return true;
    }

    auto Reader::GetError() const -> QString {
        return d->error;
    }
}
