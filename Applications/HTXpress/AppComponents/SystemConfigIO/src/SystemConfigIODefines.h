#pragma once

namespace HTXpress::AppComponents::SystemConfigIO {
    namespace Key {
        constexpr char dataDir[] = "General/DataDirectory";
        constexpr char model[] = "General/Model";
        constexpr char serial[] = "General/Serial";
        constexpr char minSpace[] = "General/MinRequiredSpaceGB";

        constexpr char mcuPort[] = "System/MCU_Port";
        constexpr char mcuBaudrate[] = "System/MCU_Baudrate";

        constexpr char imagingCamSerial[] = "System/ImagingCamera/Serial";
        constexpr char condenserCamSerial[] = "System/CondenserCamera/Serial";

        constexpr char systemCenterX[] = "Parameters/CenterPositionXInMm";
        constexpr char systemCenterY[] = "Parameters/CenterPositionYInMm";
        constexpr char tileOverlap[] = "Parameters/TileOverlapInUm";
        constexpr char afTime[] = "Parameters/AutoFocusTime";
        constexpr char flOutputRangeMin[] = "Parameters/FlOutputRange/Min";
        constexpr char flOutputRangeMax[] = "Parameters/FlOutputRange/Max";
        constexpr char bfLightIntensity[] = "Parameters/BF/LightIntensity";
        constexpr char previewLightIntensity[] = "Parameters/Preview/LightIntensity";
        constexpr char previewGainCoeffA[] = "Parameters/Preview/GainCoefficientA";
        constexpr char previewGainCoeffB[] = "Parameters/Preview/GainCoefficientB";
        constexpr char previewGainCoeffP[] = "Parameters/Preview/GainCoefficientP";
        constexpr char previewGainCoeffQ[] = "Parameters/Preview/GainCoefficientQ";
        constexpr char axisCompensationX[] = "Parameters/AxisCompensation/X";
        constexpr char axisCompensationY[] = "Parameters/AxisCompensation/Y";

        constexpr char flChannel[] = "FL Channel";
        constexpr char flChannelIdx[] = "Index";
        constexpr char flChannelName[] = "Name";
        constexpr char flChannelExcitationWavelength[] = "ExcitationWavelength";
        constexpr char flChannelExcitationBandwidth[] = "ExcitationBandwidth";
        constexpr char flChannelEmissionWavelength[] = "EmissionWavelength";
        constexpr char flChannelEmissionBandwidth[] = "EmissionBandwidth";
        constexpr char flChannelColor[] = "Color";

        constexpr char htIlluminations[] = "HT Illumination Pattern Index";
        constexpr char NA[] = "NA";
        constexpr char patternIndex[] = "PatternIndex";

        constexpr char afLensID[] = "AutoFocus/LensID";
        constexpr char afInFocusRange[] = "AutoFocus/InFocusRange";
        constexpr char afDirection[] = "AutoFocus/Direction";
        constexpr char afLoopInterval[] = "AutoFocus/LoopInterval";
        constexpr char afMaxTrialCount[] = "AutoFocus/MaxTrialCount";
        constexpr char afSensorValueMin[] = "AutoFocus/SensorValue/Min";
        constexpr char afSensorValueMax[] = "AutoFocus/SensorValue/Max";
        constexpr char afResolutionMul[] = "AutoFocus/Resolution/Mul";
        constexpr char afResolutionDiv[] = "AutoFocus/Resolution/Div";
        constexpr char afDefaultTarget[] = "AutoFocus/DefaultTargetValue";
        constexpr char afScanReady[] = "AutoFocus/ScanReadyMm";
        constexpr char afMaxCompPulse[] = "AutoFocus/MaxCompensationPulse";

        constexpr char htScanParameter[] = "HT Scan Parameter";
        constexpr char htScanNA[] = "NA";
        constexpr char htScanStep[] = "Step";
        constexpr char htScanSlices[] = "Slices";

        constexpr char conAfParameter[] = "Condenser AF Parameter";
        constexpr char conAfNA[] = "NA";
        constexpr char conAfPatternIndex[] = "PatternIndex";
        constexpr char conAfIntensity[] = "Intensity";
        constexpr char conAfZOffset[] = "ZOffsetMM";

        constexpr char illumCalParameter[] = "HT Illumination Calibration Parameter";
        constexpr char illumCalNA[] = "NA";
        constexpr char illumCalIntensityStart[] = "Start";
        constexpr char illumCalIntensityStep[] = "Step";
        constexpr char illumCalThreshold[] = "Threshold";

        constexpr char cafScanStartPos[] = "CondenserAFScanPerSystem/TriggerStartPosPulse";
        constexpr char cafScanInterval[] = "CondenserAFScanPerSystem/TriggerIntervalPulse";
        constexpr char cafScanSlices[] = "CondenserAFScanPerSystem/TriggerSlices";
    };
}
