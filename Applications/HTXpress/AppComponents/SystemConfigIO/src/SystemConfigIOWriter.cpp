#define LOGGER_TAG "[SytemConfigReader]"
#include <QSettings>
#include <QDir>

#include <TCLogger.h>

#include "SystemConfigIODefines.h"
#include "SystemConfigIOWriter.h"

namespace HTXpress::AppComponents::SystemConfigIO {
    Writer::Writer() {
    }

    Writer::~Writer() {
    }

    auto Writer::Write(const QString& path,
                                     const AppEntity::SystemConfig::Pointer& config) const -> bool {
        if(!path.isEmpty()) {
            QLOG_INFO() << "The existing system settings file is replaced with the new system settings file." << path;
            QDir().rename(path, path+".bak");
            QDir().remove(path);
        }

        QSettings qs(path, QSettings::IniFormat);

        qs.setValue(Key::dataDir, config->GetDataDir());
        qs.setValue(Key::model, config->GetModel());
        qs.setValue(Key::serial, config->GetSerial());
        qs.setValue(Key::minSpace, config->GetMinRequiredSpace());

        qs.setValue(Key::mcuPort, config->GetMCUPort());
        qs.setValue(Key::mcuBaudrate, config->GetMCUBaudrate());

        qs.setValue(Key::imagingCamSerial, config->GetImagingCameraSerial());
        qs.setValue(Key::condenserCamSerial, config->GetCondenserCameraSerial());

        const auto center = config->GetSystemCenter().toMM();
        qs.setValue(Key::systemCenterX, center.x);
        qs.setValue(Key::systemCenterY, center.y);
        qs.setValue(Key::tileOverlap, config->GetTileScanOverlap());
        qs.setValue(Key::afTime, config->GetAutoFocusTime());
        qs.setValue(Key::flOutputRangeMin, config->GetFlOutputRangeMin());
        qs.setValue(Key::flOutputRangeMax, config->GetFlOutputRangeMax());
        qs.setValue(Key::bfLightIntensity, config->GetBFLightIntensity());
        qs.setValue(Key::previewLightIntensity, config->GetPreviewLightIntensity());
        const auto [coeffA, coeffB, coeffP, coeffQ] = config->GetPreviewGainCoefficient();
        qs.setValue(Key::previewGainCoeffA, coeffA);
        qs.setValue(Key::previewGainCoeffB, coeffB);
        qs.setValue(Key::previewGainCoeffP, coeffP);
        qs.setValue(Key::previewGainCoeffQ, coeffQ);
        qs.setValue(Key::axisCompensationX, config->GetAxisCompensation(AppEntity::Axis::X));
        qs.setValue(Key::axisCompensationY, config->GetAxisCompensation(AppEntity::Axis::Y));

        qs.beginWriteArray(Key::flChannel);

        int32_t arrayIndex = 0;
        for(auto flChannelIdx : config->GetFLChannels()) {
            AppEntity::FLChannel flChannel;
            if(!config->GetFLChannel(flChannelIdx, flChannel)) continue;
            qs.setArrayIndex(arrayIndex++);
            qs.setValue(Key::flChannelName, flChannel.GetName());
            qs.setValue(Key::flChannelIdx, flChannelIdx);
            qs.setValue(Key::flChannelExcitationWavelength, flChannel.GetExcitation());
            qs.setValue(Key::flChannelExcitationBandwidth, flChannel.GetExcitationBandwidth());
            qs.setValue(Key::flChannelEmissionWavelength, flChannel.GetEmission());
            qs.setValue(Key::flChannelEmissionBandwidth, flChannel.GetEmissionBandwidth());
            qs.setValue(Key::flChannelColor, flChannel.GetColor());
        }

        qs.endArray();

        qs.beginWriteArray(Key::htIlluminations);
        arrayIndex = 0;
        for(auto key : config->GetHTIlluminationNAs()) {
            auto patternIndex = config->GetHTIlluminationPattern(key);
            if(patternIndex == -1) continue;
            qs.setArrayIndex(arrayIndex++);
            qs.setValue(Key::NA, key);
            qs.setValue(Key::patternIndex, patternIndex);
        }
        qs.endArray();

        using AFParam = AppEntity::AutoFocusParameter;
        qs.setValue(Key::afLensID, config->GetAutofocusParameter(AFParam::LensID));
        qs.setValue(Key::afInFocusRange, config->GetAutofocusParameter(AFParam::InFocusRange));
        qs.setValue(Key::afDirection, config->GetAutofocusParameter(AFParam::Direction));
        qs.setValue(Key::afLoopInterval, config->GetAutofocusParameter(AFParam::LoopInterval));
        qs.setValue(Key::afMaxTrialCount, config->GetAutofocusParameter(AFParam::MaxTrialCount));
        qs.setValue(Key::afSensorValueMin, config->GetAutofocusParameter(AFParam::MinSensorValue));
        qs.setValue(Key::afSensorValueMax, config->GetAutofocusParameter(AFParam::MaxSensorValue));
        qs.setValue(Key::afResolutionMul, config->GetAutofocusParameter(AFParam::ResolutionMul));
        qs.setValue(Key::afResolutionDiv, config->GetAutofocusParameter(AFParam::ResolutionDiv));
        qs.setValue(Key::afDefaultTarget, config->GetAutofocusParameter(AFParam::DefaultTargetValue));
        qs.setValue(Key::afScanReady, config->GetAutofocusReadyPos());
        qs.setValue(Key::afMaxCompPulse, config->GetAutofocusParameter(AFParam::MaxCompensationPulse));

        qs.beginWriteArray(Key::htScanParameter);
        const auto htScanNAs = config->GetHTScanParameterNAs();
        arrayIndex = 0;
        for(auto NA : htScanNAs) {
            auto param = config->GetHTScanParameter(NA);
            qs.setArrayIndex(arrayIndex++);
            qs.setValue(Key::htScanNA, NA);
            qs.setValue(Key::htScanStep, std::get<0>(param));
            qs.setValue(Key::htScanSlices, std::get<1>(param));
        }
        qs.endArray();

        qs.beginWriteArray(Key::conAfParameter);
        const auto condAfNAs = config->GetCondenserAFParameterNAs();
        arrayIndex = 0;
        for(auto NA : condAfNAs) {
            auto [patternIndx, intensity, zOffset] = config->GetCondenserAFParameter(NA);
            qs.setArrayIndex(arrayIndex++);
            qs.setValue(Key::conAfNA, NA);
            qs.setValue(Key::conAfPatternIndex, patternIndx);
            qs.setValue(Key::conAfIntensity, intensity);
            qs.setValue(Key::conAfZOffset, zOffset);
        }
        qs.endArray();

        qs.beginWriteArray(Key::illumCalParameter);
        const auto illumCalNAs = config->GetIlluminationCalibrationParameterNAs();
        arrayIndex = 0;
        for(auto NA :illumCalNAs) {
            auto [start, step, threshold] = config->GetIlluminationCalibrationParameter(NA);
            qs.setArrayIndex(arrayIndex++);
            qs.setValue(Key::illumCalIntensityStart, start);
            qs.setValue(Key::illumCalIntensityStep, step);
            qs.setValue(Key::illumCalThreshold, threshold);
            qs.setValue(Key::illumCalNA, NA);
        }
        qs.endArray();

        //TODO Refer to HTX-2507/HTXIS-189
        if(config->IsCondenserAFScanParameterOverriden()) {
            qs.setValue(Key::cafScanStartPos, config->GetCondenserAFScanStartPosPulse());
            qs.setValue(Key::cafScanInterval, config->GetCondenserAFScanIntervalPulse());
            qs.setValue(Key::cafScanSlices, config->GetCondenserAFScanTriggerSlices());
        }

        return true;
    }
}
