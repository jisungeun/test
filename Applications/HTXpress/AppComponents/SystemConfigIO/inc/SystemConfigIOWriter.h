#pragma once
#include <memory>
#include <QString>

#include <SystemConfig.h>

#include "HTXSystemConfigIOExport.h"

namespace HTXpress::AppComponents::SystemConfigIO {
	class HTXSystemConfigIO_API Writer {
	public:
		Writer();
		~Writer();

		auto Write(const QString& path, const AppEntity::SystemConfig::Pointer& config) const -> bool;
	};
}
