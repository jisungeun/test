#pragma once
#include <memory>
#include <QString>

#include <SystemConfig.h>

#include "HTXSystemConfigIOExport.h"

namespace HTXpress::AppComponents::SystemConfigIO {
	class HTXSystemConfigIO_API Reader {
	public:
		Reader();
		~Reader();

		auto Read(const QString& path, AppEntity::SystemConfig::Pointer& config) const->bool;
		auto GetError() const->QString;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
