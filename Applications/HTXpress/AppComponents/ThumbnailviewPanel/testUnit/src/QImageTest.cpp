﻿#include <catch2/catch.hpp>

#include <QImage>

namespace HTXThumbnailUnitTest {
	TEST_CASE("QImage") {
		SECTION("Class Size") {
			SECTION("") {
				QImage image1;
				
				QImage* image2 = new QImage();
				QImage* image3 = new QImage(100,200,QImage::Format_ARGB32);
				QImage* image4 = new QImage(100,200,QImage::Format_Mono);
				QImage image5(150,200,QImage::Format_ARGB32);
				int size1 = sizeof(image1);

				CHECK(sizeof(QImage) == sizeof(image1));
				CHECK(sizeof(&image2) == sizeof(&image3));
				CHECK(sizeof(&image2) == sizeof(&image4));
				CHECK(sizeof(&image3) == sizeof(&image4));
				CHECK(sizeof(&image2) == sizeof(image5));

				delete image4;
				delete image3;
				delete image2;
			}
		}
	}
}