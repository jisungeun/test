﻿#pragma once

#include <memory>

#include <QWidget>

#include <enum.h>

#include "HTXThumbnailviewPanelExport.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    BETTER_ENUM(ModalityIndex, int32_t, HT, FL, BF);

    class HTXThumbnailviewPanel_API ThumbnailPanel : public QWidget {
        Q_OBJECT
    public:
        using Self = ThumbnailPanel;
        using Pointer = std::shared_ptr<Self>;

        explicit ThumbnailPanel(QWidget* parent = nullptr);
        ~ThumbnailPanel() override;

        auto Clear(bool isInit = true) -> void;

        auto SetCurrentImage(const QImage& image, bool fitInView = false) -> void;
        auto GetCurrentImage() const -> const QImage&;

        auto SetCurrentSyncMode(bool sync) -> void;
        auto GetCurrentSyncMode() const -> bool;

        auto SetCurrentTimeInSec(int32_t seconds) -> void;
        auto GetCurrentTimeInSec() const -> int32_t;

        auto SetCurrentSliderIndex(int32_t index) -> void;
        auto GetCurrentSliderIndex() const -> int32_t;

        auto SetCurrentModality(ModalityIndex modality) -> void;
        auto GetCurrentModality() const -> ModalityIndex;

        auto SetCurrentImageName(const QString& name) -> void;
        auto SetTotalSequenceCount(int32_t count) -> void;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    signals:
        void sigImageIndexChanged(int32_t imageIndex); // from slider
        void sigModalityChanged(HTXpress::AppComponents::ThumbnailviewPanel::ModalityIndex modalityIndex); // from modality
        void sigSyncModeChanged(bool sync);

    private slots:
        void onModalityChanged();
        void onImageIndexChanged(int32_t index);
        void onSyncModeChanged(bool sync);
        void onZoomRequest(int32_t zoom);
        void onUpdateSubwidgetPos();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}