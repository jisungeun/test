#include "ThumbnailViewTestWindow.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HTXpress::AppComponents::ThumbnailviewPanel::Test::ThumbnailViewTestWindow w;
    w.show();
    return a.exec();
}
