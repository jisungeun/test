#pragma once

#include <memory>
#include <QtWidgets/QMainWindow>

#include "ThumbnailPanel.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel::Test {
	class ThumbnailViewTestWindow : public QMainWindow {
		Q_OBJECT
	public:
		using Self = ThumbnailViewTestWindow;
		explicit ThumbnailViewTestWindow(QWidget* parent = nullptr);
		~ThumbnailViewTestWindow() override;

	public slots:
		// from panel
		void onGetCurrentImageIndex(int32_t imageIndex);
		void onGetCurrentModality(ModalityIndex modalityIndex);
		void onSyncModeChanged(bool sync);

		// to panel
		void onSetImage();
		void onClearAll();
		void onSetImageCount();
		void onSetCurrentImageIndex();
	    void onSetTime();
	    void onSetDataName();
	private:
		struct Impl;
		std::unique_ptr<Impl> d;

		auto CreateCentralWidget() -> void;
		auto ConnectUIs() -> void;
	};

}
