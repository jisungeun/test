#include <QtGui/QtGui>
#include <QDebug>
#include <QGridLayout>
#include <QFileDialog>

#include "ThumbnailViewTestWindow.h"
#include "ui_ThumbnailViewTestWindow.h"

#include "ThumbnailPanel.h"

namespace  HTXpress::AppComponents::ThumbnailviewPanel::Test {
    static int TEMP_INDEX = 0;

    struct ThumbnailViewTestWindow::Impl {
		Ui::TestWindow ui;
		ThumbnailPanel* widget{nullptr};
	};

	ThumbnailViewTestWindow::ThumbnailViewTestWindow(QWidget* parent) : QMainWindow(parent), d{new Impl} {
		d->ui.setupUi(this);
		CreateCentralWidget();
		ConnectUIs();
	}

	ThumbnailViewTestWindow::~ThumbnailViewTestWindow() {
	}

	void ThumbnailViewTestWindow::onGetCurrentImageIndex(int32_t imageIndex) {
		qDebug() << "image index:" << imageIndex;
		d->ui.labelCurrentIndex->setText(QString::number(imageIndex));
	}

	void ThumbnailViewTestWindow::onGetCurrentModality(ModalityIndex modalityIndex) {
		qDebug() << "modality index:" << modalityIndex;
		d->ui.labelCurrentModalityIndex->setText(QString::number(modalityIndex));
	}

    void ThumbnailViewTestWindow::onSyncModeChanged(bool sync) {
		qDebug() << "sync mode:" << sync;
		QString txt = "on";
		if(!sync) txt = "off";

		d->ui.labelSyncMode->setText(txt);
    }

    void ThumbnailViewTestWindow::onSetImage() {
		int32_t idx = -999;
		if (d->ui.editImageIndex->text().length() > 0) {
			idx = d->ui.editImageIndex->text().toInt();
		}

		const QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"));
		const QImage image(fileName);

		if (idx == -999) {
			d->widget->SetCurrentImage(image);
		}
		else {
			d->widget->SetCurrentImage(image, idx);
		}
	}

	void ThumbnailViewTestWindow::onClearAll() {
		d->widget->SetCurrentImage({});
		d->widget->SetTotalSequenceCount(0);
	}

	void ThumbnailViewTestWindow::onSetImageCount() {
		const int count = d->ui.spinImageCount->value();
		d->widget->SetTotalSequenceCount(count);

		d->ui.labelIndexCount->setText(QString::number(count));
	}

    void ThumbnailViewTestWindow::onSetCurrentImageIndex() {
		const auto idx = d->ui.spinCurrImageIndex->value();
		d->widget->SetCurrentSliderIndex(idx);

		d->ui.labelCurrentIndex->setText(QString::number(idx));
    }

    void ThumbnailViewTestWindow::onSetTime() {
		const auto seconds = d->ui.secEdit->text().toInt();
		d->widget->SetCurrentTimeInSec(seconds);
    }

    void ThumbnailViewTestWindow::onSetDataName() {
		const auto name = d->ui.dataNameEdit->text();
		d->widget->SetCurrentImageName(name);
    }

    void ThumbnailViewTestWindow::CreateCentralWidget() {
		d->widget = new ThumbnailPanel();
		QGridLayout* layout = new QGridLayout;
		layout->addWidget(d->widget);
		layout->setContentsMargins(0, 0, 0, 0);
		d->ui.viewFrame->setContentsMargins(0, 0, 0, 0);
		d->ui.viewFrame->setLayout(layout);
	}

	auto ThumbnailViewTestWindow::ConnectUIs() -> void {
		connect(d->widget, &ThumbnailPanel::sigImageIndexChanged, this, &Self::onGetCurrentImageIndex);
		connect(d->widget, &ThumbnailPanel::sigModalityChanged, this, &Self::onGetCurrentModality);
		connect(d->widget, &ThumbnailPanel::sigSyncModeChanged, this, &Self::onSyncModeChanged);

		connect(d->ui.btnLoadImage, &QPushButton::clicked, this, &Self::onSetImage);
		connect(d->ui.btnClearAll, &QPushButton::clicked, this, &Self::onClearAll);

		connect(d->ui.btnApplyImageCount, &QPushButton::clicked, this, &Self::onSetImageCount);
		connect(d->ui.btnApplyImageIndex, &QPushButton::clicked, this, &Self::onSetCurrentImageIndex);

		connect(d->ui.btnApplyTime, &QPushButton::clicked, this, &Self::onSetTime);
		connect(d->ui.btnApplyDataName, &QPushButton::clicked, this, &Self::onSetDataName);
	}
}
