﻿#pragma once
#pragma once

#include <memory>

#include <QObject>

/// <summary>
/// (default modifier)ctrl + mouse wheel = zoom in/out
///	mouse right btn double clicked = fit in view
/// </summary>

class QGraphicsView;

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class CanvasZoomSupport : public QObject {
        Q_OBJECT
    public:
        using Self = CanvasZoomSupport;
        using Pointer = std::shared_ptr<Self>;

        explicit CanvasZoomSupport(QGraphicsView* view);
        ~CanvasZoomSupport() override;

        auto SetCurrentLod(const double& lod) -> void;

        auto ZoomIn(bool onMousePos = false) const -> void;
        auto ZoomOut(bool onMousePos = false) const -> void;
        auto ZoomFit() const -> void;

    protected:
        auto eventFilter(QObject* watched, QEvent* event) -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
