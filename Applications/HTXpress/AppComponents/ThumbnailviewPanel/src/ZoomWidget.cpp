﻿#include <QButtonGroup>
#include <QToolButton>
#include <QVBoxLayout>
#include <QGraphicsOpacityEffect>

#include "ZoomWidget.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct ZoomWidget::Impl {
        const QString btnStyleSheet = "QToolButton {border:0px none; background-color:transparent;}";
        QButtonGroup* buttonGroup{nullptr};
        QToolButton* zoomIn{nullptr};
        QToolButton* zoomOut{nullptr};
        QToolButton* zoomFit{nullptr};
    };

    ZoomWidget::ZoomWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->zoomIn = new QToolButton(this);
        d->zoomOut = new QToolButton(this);
        d->zoomFit = new QToolButton(this);

        d->zoomIn->setIcon(QIcon(":/thumbnail/ic-sub-zoomin.svg"));
        d->zoomOut->setIcon(QIcon(":/thumbnail/ic-sub-zoomout.svg"));
        d->zoomFit->setIcon(QIcon(":/thumbnail/ic-sub-zoomfit.svg"));

        d->buttonGroup = new QButtonGroup(this);
        d->buttonGroup->addButton(d->zoomFit, static_cast<int32_t>(ZoomButtonRole::ZoomFit));
        d->buttonGroup->addButton(d->zoomIn, static_cast<int32_t>(ZoomButtonRole::ZoomIn));
        d->buttonGroup->addButton(d->zoomOut, static_cast<int32_t>(ZoomButtonRole::ZoomOut));

        connect(d->buttonGroup, &QButtonGroup::idClicked, this, &Self::onZoomingRequested);

        const auto layout = new QVBoxLayout;
        for (const auto& btn : d->buttonGroup->buttons()) {
            btn->setStyleSheet(d->btnStyleSheet);
            btn->setIconSize({24, 24});
            layout->addWidget(btn, 0, Qt::AlignCenter);
        }
        layout->setSpacing(6);
        layout->setContentsMargins(0, 0, 0, 0);

        setLayout(layout);
        setFixedSize(24, 24 * 3 + 6 * 2);

        auto* oe = new QGraphicsOpacityEffect(this);
        oe->setOpacity(0.2);
        setGraphicsEffect(oe);
        setAutoFillBackground(true);
    }

    ZoomWidget::~ZoomWidget() = default;

    auto ZoomWidget::enterEvent(QEvent* event) -> void {
        auto* oe = new QGraphicsOpacityEffect(this);
        oe->setOpacity(1.0);
        setGraphicsEffect(oe);
        QWidget::enterEvent(event);
    }

    auto ZoomWidget::leaveEvent(QEvent* event) -> void {
        auto* oe = new QGraphicsOpacityEffect(this);
        oe->setOpacity(0.2);
        setGraphicsEffect(oe);
        QWidget::leaveEvent(event);
    }

    void ZoomWidget::onZoomingRequested(int32_t which) {
        emit sigZooming(which);
    }
}
