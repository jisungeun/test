﻿#pragma once
#include <memory>
#include <QWidget>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class ImageIndexSliderWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = ImageIndexSliderWidget;
        using Pointer = std::shared_ptr<Self>;

        ImageIndexSliderWidget(QWidget* parent = nullptr);
        ~ImageIndexSliderWidget() override;

        auto SetImageCount(const int32_t& val) -> void;
        auto SetCurrentImageIndex(const int32_t& index) -> void;

    signals:
        void sigCurrentImageIndex(int32_t);

    private slots:
        void onAutoModeCheckBoxToggled(bool isAuto);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
