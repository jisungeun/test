﻿#include "ImageView.h"

#include <QGraphicsScene>
#include <QDebug>
#include <QResizeEvent>
#include <QScrollBar>

#include "ImageView.h"
#include "GraphicsImageItem.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct ImageView::Impl {
        QGraphicsScene* scene{nullptr};
        GraphicsImageItem* imageItem{nullptr};

        int32_t horizontalScrollBarPadding{0};
        int32_t verticalScrollBarPadding{0};

        auto Initialize(Self* self) -> void;
        auto InitFitInView(Self* self) -> void;
    };

    ImageView::ImageView(QWidget* parent) : QGraphicsView(parent), d{std::make_unique<Impl>()} {
        setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
        setOptimizationFlags(DontSavePainterState);
        setViewportUpdateMode(SmartViewportUpdate);
        setTransformationAnchor(AnchorUnderMouse);
        setDragMode(ScrollHandDrag);
        setMouseTracking(true);

        d->Initialize(this);
    }

    ImageView::~ImageView() = default;

    auto ImageView::SetImageItem(const QImage& image, bool fitInView) -> void {
        const auto pixmap = QPixmap::fromImage(image);
        const auto sceneX = -image.width() / 2.;
        const auto sceneY = -image.height() / 2.;
        const auto sceneW = image.width();
        const auto sceneH = image.height();
        this->resetTransform();
        d->scene->setSceneRect(sceneX, sceneY, sceneW, sceneH);
        d->imageItem->setPos(sceneX, sceneY);
        d->imageItem->setPixmap(pixmap);

        if (fitInView) { d->InitFitInView(this); }
    }

    auto ImageView::GetHorizontalScrollBarHeightOffset() const -> int32_t {
        return d->horizontalScrollBarPadding;
    }

    auto ImageView::GetVerticalScrollBarWidthOffset() const -> int32_t {
        return d->verticalScrollBarPadding;
    }

    void ImageView::onSetHorizontalScrollbarPaddingOffset(int32_t padding) {
        d->horizontalScrollBarPadding = padding;
        emit sigChangeHorizontalScrollBarVisiblility();
    }

    void ImageView::onSetVerticalScrollbarPaddingOffset(int32_t padding) {
        d->verticalScrollBarPadding = padding;
        emit sigChangeVerticalScrollBarVisiblility();
    }

    auto ImageView::Impl::Initialize(Self* self) -> void {
        scene = new QGraphicsScene();
        imageItem = new GraphicsImageItem();
        scene->addItem(imageItem);
        scene->setBackgroundBrush(Qt::black);
        self->setScene(scene);

        const auto horizontalScrollBar = new ImageViewScrollBar(Qt::Horizontal, self);
        const auto verticalScrollBar = new ImageViewScrollBar(Qt::Vertical, self);

        self->setHorizontalScrollBar(horizontalScrollBar);
        self->setVerticalScrollBar(verticalScrollBar);

        connect(horizontalScrollBar, &ImageViewScrollBar::sigUpdatePaddingOffset, self, &Self::onSetHorizontalScrollbarPaddingOffset);
        connect(verticalScrollBar, &ImageViewScrollBar::sigUpdatePaddingOffset, self, &Self::onSetVerticalScrollbarPaddingOffset);
    }

    auto ImageView::Impl::InitFitInView(Self* self) -> void {
        self->fitInView(imageItem, Qt::KeepAspectRatio);
    }

    // ImageViewScrollBar
    ImageViewScrollBar::ImageViewScrollBar(Qt::Orientation ori, QWidget* parent) : QScrollBar(ori, parent) {
    }

    ImageViewScrollBar::~ImageViewScrollBar() = default;

    void ImageViewScrollBar::showEvent(QShowEvent* event) {
        if (orientation() == Qt::Horizontal) {
            emit sigUpdatePaddingOffset(height());
        }
        else if (orientation() == Qt::Vertical) {
            emit sigUpdatePaddingOffset(width());
        }
        QScrollBar::showEvent(event);
    }

    void ImageViewScrollBar::hideEvent(QHideEvent* event) {
        if (orientation() == Qt::Horizontal) {
            emit sigUpdatePaddingOffset(height());
        }
        else if (orientation() == Qt::Vertical) {
            emit sigUpdatePaddingOffset(width());
        }
        QScrollBar::hideEvent(event);
    }
}
