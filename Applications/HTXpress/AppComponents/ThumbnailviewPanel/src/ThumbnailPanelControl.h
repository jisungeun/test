﻿#pragma once

#include <memory>
#include <qimage.h>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class ThumbnailPanelControl {
    public:
        using Self = ThumbnailPanelControl;
        using Pointer = std::shared_ptr<Self>;

        ThumbnailPanelControl();
        ~ThumbnailPanelControl();

        // canvas update
        auto SetImage(const QImage& image) -> void;
        auto SetImageWithIndex(const QImage& image, const int32_t& imageIndex) -> void;

        // slider update
        auto SetImageCount(int32_t imageCount = 1) -> void;

        // modality update
        auto SetModalities(const QMap<int32_t, QString>& modalities) -> void;

        auto GetImage() const -> const QImage&;
        auto GetImageIndex() const -> int32_t;
        auto GetImageCount() const -> int32_t;
        auto GetModalityList() const -> QMap<int32_t, QString>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
