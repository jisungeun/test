﻿#pragma once

#include <memory>

#include <QGraphicsPixmapItem>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class GraphicsImageItem : public QGraphicsPixmapItem {
    public:
        using Self = GraphicsImageItem;
        using Pointer = std::shared_ptr<Self>;

        explicit GraphicsImageItem(QGraphicsItem* parent = nullptr);
        ~GraphicsImageItem() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
