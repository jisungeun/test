﻿#include <QGraphicsView>
#include <QMap>

#include "SubWidgetPosUpdater.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct SubWidgetPosUpdater::Impl {
        struct Padding {
            int32_t horizontalPadding{14};
            int32_t verticalPadding{14};
        };

        QWidget* mainWidget{nullptr};
        QMap<QWidget*, QPair<SubWidgetGeomertry, Padding>> widgets;

        auto UpdatePosition(QWidget* widget) const -> void;
    };

    auto SubWidgetPosUpdater::Impl::UpdatePosition(QWidget* widget) const -> void {
        auto subWidgetWidth = widget->geometry().width();
        const auto subWidgetHeight = widget->geometry().height();

        const auto parentWidth = mainWidget->geometry().width();
        const auto parentHeight = mainWidget->geometry().height();
        const auto [horizontal, vertical] = widgets[widget].second;
        const auto where = widgets[widget].first;

        int32_t left = 0;
        int32_t top = 0;

        switch (where) {
            case SubWidgetGeomertry::TopLeft: {
                left = vertical;
                top = horizontal;
            }
            break;
            case SubWidgetGeomertry::TopCenter: {
                left = (parentWidth - subWidgetWidth) / 2;
                top = horizontal;
            }
            break;
            case SubWidgetGeomertry::TopRight: {
                left = parentWidth - subWidgetWidth - vertical;
                top = horizontal;
            }
            break;
            case SubWidgetGeomertry::BottomLeft: {
                left = vertical;
                top = parentHeight - subWidgetHeight - horizontal;
            }
            break;
            case SubWidgetGeomertry::BottomCenter: {
                subWidgetWidth = parentWidth * 0.8; // resize widget's width located bottomCenter
                left = (parentWidth - subWidgetWidth) / 2;
                top = parentHeight - subWidgetHeight - horizontal;
            }
            break;
            case SubWidgetGeomertry::BottomRight: {
                left = parentWidth - subWidgetWidth - vertical;
                top = parentHeight - subWidgetHeight - horizontal;
            }
            break;
        }
        widget->setGeometry(left, top, subWidgetWidth, subWidgetHeight);
    }

    SubWidgetPosUpdater::SubWidgetPosUpdater(QWidget* mainWidget) : d{std::make_unique<Impl>()} {
        d->mainWidget = mainWidget;
    }

    SubWidgetPosUpdater::~SubWidgetPosUpdater() = default;

    auto SubWidgetPosUpdater::SetSubwidget(QWidget* subwidget, SubWidgetGeomertry where, HorizontalPadding hor, VerticalPadding ver) -> void {
        subwidget->setWindowFlags(Qt::FramelessWindowHint);
        subwidget->setAttribute(Qt::WA_NoSystemBackground);
        subwidget->setAttribute(Qt::WA_TranslucentBackground);
        subwidget->setAttribute(Qt::WA_PaintOnScreen);

        d->widgets[subwidget] = {where, {hor, ver}};
    }

    auto SubWidgetPosUpdater::ChangeHorizontalPadding(QWidget* subwidget, HorizontalPadding padding) -> void {
        d->widgets[subwidget].second.horizontalPadding = padding;
    }

    auto SubWidgetPosUpdater::ChangeVerticalPadding(QWidget* subwidget, VerticalPadding padding) -> void {
        d->widgets[subwidget].second.verticalPadding = padding;
    }

    void SubWidgetPosUpdater::onUpdatePosition() {
        for (auto it = d->widgets.cbegin(); it != d->widgets.cend(); ++it) {
            d->UpdatePosition(it.key());
        }
    }
}
