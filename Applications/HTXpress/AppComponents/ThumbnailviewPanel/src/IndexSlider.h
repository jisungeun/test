﻿#pragma once

#include <memory>

#include <QSlider>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class IndexSlider : public QSlider {
        Q_OBJECT
    public:
        using Self = IndexSlider;
        using Pointer = std::shared_ptr<Self>;

        IndexSlider(Qt::Orientation ori, QWidget* parent = nullptr);
        ~IndexSlider() override;

        auto SetAutoMode(bool isAuto) -> void;
        auto IsAutoMode() const -> bool;
        auto SetImageIndexCount(const int32_t& count) -> void;

    public slots:
        void onSetCurrentIndex(int32_t currentIndex);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
