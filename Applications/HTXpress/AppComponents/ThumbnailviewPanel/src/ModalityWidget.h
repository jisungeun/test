﻿#pragma once

#include <QWidget>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class ModalityWidget : public QWidget{
        Q_OBJECT
    public:
        using Self = ModalityWidget;

        enum class ModalityType {
            HT = 1, FL, BF
        };
        Q_ENUM(ModalityType)

        explicit ModalityWidget(QWidget* parent = nullptr);
        ~ModalityWidget() override;

        auto SetModality(const ModalityType& modality) -> void;
        auto GetCurrentModality() const -> ModalityType;

    signals:
        void sigModalityChanged();

    private slots:
        void onModalitySelected(int32_t which, bool selected);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

Q_DECLARE_METATYPE(HTXpress::AppComponents::ThumbnailviewPanel::ModalityWidget::ModalityType)
