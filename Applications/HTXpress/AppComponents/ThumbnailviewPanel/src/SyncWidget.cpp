﻿#include <QToolButton>
#include <QLayout>
#include <QVBoxLayout>
#include <QEvent>

#include "SyncWidget.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct SyncWidget::Impl {
        SyncButton* syncButton{nullptr};
    };

    SyncWidget::SyncWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->syncButton = new SyncButton(this);
        connect(d->syncButton, &QToolButton::toggled, this, &Self::sigSyncModeChanged);

        const auto layout = new QVBoxLayout;
        layout->addWidget(d->syncButton, 0, Qt::AlignCenter);
        layout->setSpacing(6);
        layout->setContentsMargins(0, 0, 0, 0);
        setLayout(layout);
        setFixedSize(36, 24 * 1 + 6 * 0);
    }

    SyncWidget::~SyncWidget() = default;

    auto SyncWidget::SetSyncMode(bool sync) -> void {
        if (sync != d->syncButton->isChecked()) {
            d->syncButton->setChecked(sync);
        }
    }

    auto SyncWidget::GetSyncMode() const -> bool {
        return d->syncButton->isChecked();
    }

    struct SyncButton::Impl {
        const QIcon syncOnIcon{":/thumbnail/ic-sub-refresh.svg"};
        const QIcon syncOffIcon{":/thumbnail/ic-sub-refreshoff.svg"};
        const QString btnStyleSheet{"QToolButton {border:0px none; background-color:transparent;}"};
    };

    SyncButton::SyncButton(QWidget* parent) : QToolButton(parent), d{std::make_unique<Impl>()} {
        setStyleSheet(d->btnStyleSheet);
        setCheckable(true);
        setChecked(false);
        setIcon(d->syncOffIcon);
        setIconSize({24, 24});

        connect(this, &Self::toggled, this, [this](bool toggled) {
            if (toggled) {
                setIcon(d->syncOnIcon);
            } else {
                setIcon(d->syncOffIcon);
            }
        });
    }

    SyncButton::~SyncButton() = default;
}
