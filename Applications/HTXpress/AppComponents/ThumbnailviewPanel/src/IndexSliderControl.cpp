﻿#include <QtCore>
#include <QSlider>

#include "IndexSliderControl.h"

#include "ThumbnailCustomDataType.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct IndexSliderControl::Impl {
        bool autoMode{true};

        int32_t minimum{-1};
        int32_t maximum{-1};
        int32_t current{-1};

        int32_t tickInterval{1};
        int32_t singleStep{1};
        QSlider::TickPosition tickPosition{QSlider::TicksBelow};
        Qt::Orientation orientation{Qt::Horizontal};

        auto CalculateTickInterval(int32_t count) -> int32_t;
    };


    IndexSliderControl::IndexSliderControl() : d{new Impl} {
    }

    IndexSliderControl::~IndexSliderControl() {
    }

    auto IndexSliderControl::SetAutoMode(bool isAuto) -> void {
        d->autoMode = isAuto;
    }

    auto IndexSliderControl::IsAutoMode() const -> bool {
        return d->autoMode;
    }

    auto IndexSliderControl::MinimumValue() -> int32_t {
        return d->minimum;
    }

    auto IndexSliderControl::SetMaximumValue(const int32_t& max) -> void {
        if (d->minimum == 0) {
            d->maximum = max - 1;
        }
        else {
            d->maximum = max;
        }
    }

    auto IndexSliderControl::MaximumValue() const -> int32_t {
        return d->maximum;
    }

    auto IndexSliderControl::SetCurrentValue(const int32_t& curr) -> void {
        int32_t value = curr;
        if (value > d->maximum || value == kLatestImageIndex) {
            value = d->maximum;
        }

        d->current = value;
    }

    auto IndexSliderControl::CurrentValue() const -> int32_t {
        return d->current;
    }

    auto IndexSliderControl::SetTickIntervalByTotalCount(const int32_t& totalCount) -> void {
        // calculate
        int32_t interval = d->CalculateTickInterval(totalCount);

        d->tickInterval = interval;
    }

    auto IndexSliderControl::TickInterval() const -> int32_t {
        return d->tickInterval;
    }

    auto IndexSliderControl::SingleStep() const -> int32_t {
        return d->singleStep;
    }

    auto IndexSliderControl::TickPosition() const -> QSlider::TickPosition {
        return d->tickPosition;
    }

    auto IndexSliderControl::Orientation() const -> Qt::Orientation {
        return d->orientation;
    }

    auto IndexSliderControl::Impl::CalculateTickInterval(int32_t count) -> int32_t {
        // TODO tick interval을 어떻게 설정해줄지
        if (count < 100) {
            return 1;
        }
        if (count > 100) {
            return 2;
        }
        if (count > 200) {
            return 3;
        }
        if (count > 300) {
            return 4;
        }
        return 5;
    }
}
