﻿#pragma once

#include <QWidget>
#include <QToolButton>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class SyncWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = SyncWidget;
        explicit SyncWidget(QWidget* parent = nullptr);
        ~SyncWidget() override;

        auto SetSyncMode(bool sync) -> void;
        auto GetSyncMode() const -> bool;

    signals:
        void sigSyncModeChanged(bool sync);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class SyncButton final : public QToolButton {
        Q_OBJECT
    public:
        using Self = SyncButton;
        explicit SyncButton(QWidget* parent = nullptr);
        ~SyncButton() override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
