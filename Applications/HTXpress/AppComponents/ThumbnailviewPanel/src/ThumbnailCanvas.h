﻿#pragma once

#include <memory>

#include <QGraphicsView>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class ThumbnailCanvas : public QGraphicsView {
        Q_OBJECT
    public:
        using Self = ThumbnailCanvas;
        using Pointer = std::shared_ptr<Self>;

        ThumbnailCanvas(QWidget* parent = nullptr);
        ~ThumbnailCanvas() override;

        auto SetImageItem(const QImage& image) -> void;

    protected:
        auto resizeEvent(QResizeEvent* event) -> void override;
        
    signals:
        void sigCanvasWidthResized();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
