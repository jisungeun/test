﻿#include "GraphicsImageItem.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct GraphicsImageItem::Impl {

    };

    GraphicsImageItem::GraphicsImageItem(QGraphicsItem* parent) : QGraphicsPixmapItem(parent), d{std::make_unique<Impl>()} {
    }

    GraphicsImageItem::~GraphicsImageItem() {
    }
}
