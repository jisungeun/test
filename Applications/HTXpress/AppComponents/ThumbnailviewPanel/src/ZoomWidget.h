﻿#pragma once

#include <QWidget>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class ZoomWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = ZoomWidget;

        enum class ZoomButtonRole {
            ZoomIn = 77,
            ZoomOut,
            ZoomFit,
        };

        Q_ENUM(ZoomButtonRole)

        explicit ZoomWidget(QWidget* parent = nullptr);
        ~ZoomWidget() override;

    protected:
        auto enterEvent(QEvent* event) -> void override;
        auto leaveEvent(QEvent* event) -> void override;

    signals:
        void sigZooming(int32_t);

    private slots:
        void onZoomingRequested(int32_t which);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
