﻿#pragma once
#include <memory>
#include <QWidget>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class ModalityMenuWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = ModalityMenuWidget;
        using Pointer = std::shared_ptr<Self>;

        ModalityMenuWidget(QWidget* parent = nullptr);
        ~ModalityMenuWidget() override;

        auto SetModalityMenu(QMap<int32_t, QString> modalities) -> void;

    signals:
        void sigCurrentModalityIndex(int32_t);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
