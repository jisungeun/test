﻿#include <QMenu>
#include <QAction>
#include <QPushButton>
#include <QGridLayout>

#include "ModalityMenuWidget.h"
#include "ModalityMenuCreator.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct ModalityMenuWidget::Impl {
        QPushButton* menuBtn{nullptr};

        auto Initialize(QWidget* parent = nullptr) -> void;
        auto Layout() const -> QGridLayout*;
        auto MakeConnect(ModalityMenuWidget* me) -> void;

        ModalityMenuCreator::Pointer menuCreator{nullptr};
    };

    auto ModalityMenuWidget::Impl::Initialize(QWidget* parent) -> void {
        menuBtn = new QPushButton("Modality", parent);
        menuCreator = std::make_shared<ModalityMenuCreator>();
    }

    auto ModalityMenuWidget::Impl::Layout() const -> QGridLayout* {
        auto layout = new QGridLayout;
        layout->setSpacing(0);
        layout->setMargin(0);
        layout->setContentsMargins(0, 0, 0, 0);

        layout->addWidget(menuBtn);
        return layout;
    }

    auto ModalityMenuWidget::Impl::MakeConnect(ModalityMenuWidget* me) -> void {
        connect(menuCreator.get(), &ModalityMenuCreator::sigModalityIndex, me, &Self::sigCurrentModalityIndex);
    }

    ModalityMenuWidget::ModalityMenuWidget(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->Initialize(this);
        d->MakeConnect(this);

        setLayout(d->Layout());
    }

    ModalityMenuWidget::~ModalityMenuWidget() {
    }

    auto ModalityMenuWidget::SetModalityMenu(QMap<int32_t, QString> modalities) -> void {
        if (modalities.isEmpty() != true) {
            const auto menu = d->menuCreator->MakeModalityMenu(modalities);
            d->menuBtn->setMenu(menu);
        }
        else {
            d->menuBtn->setMenu(nullptr);
        }
    }
}
