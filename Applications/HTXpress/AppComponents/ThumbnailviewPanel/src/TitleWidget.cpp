﻿#include "TitleWidget.h"
#include "ui_TitleWidget.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct TitleWidget::Impl {
        Ui::TitleWidgetUI *ui{nullptr};
    };

    TitleWidget::TitleWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()}{
        d->ui = new Ui::TitleWidgetUI();
        d->ui->setupUi(this);

        d->ui->titleLabel->setObjectName("label-h2");
        d->ui->dataLabel->setObjectName("label-h4");
    }

    TitleWidget::~TitleWidget() {
        delete d->ui;
    }

    auto TitleWidget::ClearDataLabel() -> void {
        d->ui->dataLabel->clear();
    }

    auto TitleWidget::SetDataLabelText(const QString& name) -> void {
        d->ui->dataLabel->setText(name);
    }
}
