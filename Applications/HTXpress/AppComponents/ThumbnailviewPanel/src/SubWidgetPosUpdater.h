﻿#pragma once
#include <memory>

#include <QObject>

namespace HTXpress::AppComponents::ThumbnailviewPanel {

    class SubWidgetPosUpdater : public QObject {
        Q_OBJECT
    public:
        using Self = SubWidgetPosUpdater;
        using Pointer = std::shared_ptr<Self>;
        using HorizontalPadding = int32_t;
        using VerticalPadding = int32_t;
        enum class SubWidgetGeomertry {
            TopLeft,
            TopCenter,
            TopRight,
            BottomLeft,
            BottomCenter,
            BottomRight
        };

        explicit SubWidgetPosUpdater(QWidget* mainWidget);
        ~SubWidgetPosUpdater() override;

        auto SetSubwidget(QWidget* subwidget, SubWidgetGeomertry where, HorizontalPadding hor, VerticalPadding ver) -> void;

        auto ChangeHorizontalPadding(QWidget* subwidget, HorizontalPadding padding) -> void;
        auto ChangeVerticalPadding(QWidget* subwidget, VerticalPadding padding) -> void;

    public slots:
        void onUpdatePosition();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
