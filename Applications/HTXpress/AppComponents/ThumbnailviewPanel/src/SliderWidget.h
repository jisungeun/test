﻿#pragma once

#include <QWidget>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class SliderWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = SliderWidget;

        explicit SliderWidget(QWidget* parent = nullptr);
        ~SliderWidget() override;

        auto SetCurrentTime(const QString& time) -> void;
        auto GetCurrentTime() const -> QString;

        auto SetCurrentSequenceIndex(int32_t index) -> void;
        auto GetCurrentSequenceIndex() const -> int32_t;

        auto SetSequenceCount(int32_t count) -> void;

    protected:
        auto enterEvent(QEvent* event) -> void override;
        auto leaveEvent(QEvent* event) -> void override;

    signals:
        void sigSliderIndexChanged(int32_t);

    private slots:
        void onSliderIndexChanged(int32_t index);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
