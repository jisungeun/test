﻿#include <QGraphicsOpacityEffect>

#include "SliderWidget.h"
#include "ui_SliderWidget.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct SliderWidget::Impl {
        Ui::Slider* ui{nullptr};

        auto Init() -> void;
        auto SetSliderOpacity(double value) -> void;
    };

    SliderWidget::SliderWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui = new Ui::Slider();
        d->ui->setupUi(this);
        d->Init();

        connect(d->ui->indexSlider, &QSlider::valueChanged, this, &Self::onSliderIndexChanged);
    }

    SliderWidget::~SliderWidget() {
        delete d->ui;
    }

    auto SliderWidget::SetCurrentTime(const QString& time) -> void {
        d->ui->labelTime->setText(time);
    }

    auto SliderWidget::GetCurrentTime() const -> QString {
        return d->ui->labelTime->text();
    }

    auto SliderWidget::SetCurrentSequenceIndex(int32_t index) -> void {
        d->ui->labelCurrentIndex->setText(QString("%1").arg(index, 3, 10, QChar('0')));

        d->ui->indexSlider->blockSignals(true);
        d->ui->indexSlider->setValue(index);
        d->ui->indexSlider->blockSignals(false);
    }

    auto SliderWidget::GetCurrentSequenceIndex() const -> int32_t {
        return d->ui->indexSlider->value();
    }

    auto SliderWidget::SetSequenceCount(int32_t count) -> void {
        d->ui->labelCount->setText(QString("%1").arg(count, 3, 10, QChar('0')));
        if (count != 0) { d->ui->indexSlider->setRange(1, count); }
        else { d->ui->indexSlider->setRange(0, 0); }
    }

    void SliderWidget::enterEvent(QEvent* event) {
        d->SetSliderOpacity(1.0);
        QWidget::enterEvent(event);
    }

    void SliderWidget::leaveEvent(QEvent* event) {
        d->SetSliderOpacity(0.6);
        QWidget::leaveEvent(event);
    }

    void SliderWidget::onSliderIndexChanged(int32_t index) {
        d->ui->labelCurrentIndex->setText(QString("%1").arg(index, 3, 10, QChar('0')));
        emit sigSliderIndexChanged(index);
    }

    auto SliderWidget::Impl::Init() -> void {
        ui->indexSlider->setRange(0, 0);
        ui->indexSlider->setValue(0);
        ui->indexSlider->setPageStep(1);
        ui->indexSlider->setSingleStep(1);

        ui->labelCount->setText("000");
        ui->labelCurrentIndex->setText("000");

        ui->labelCount->setObjectName("label-h5");
        ui->labelBracket1->setObjectName("label-h5");
        ui->labelBracket2->setObjectName("label-h5");
        ui->labelSlash->setObjectName("label-h5");
        ui->labelTime->setObjectName("label-h5");
        ui->labelCurrentIndex->setObjectName("label-h5");

        ui->indexSlider->setStyleSheet("QSlider{background:transparent;}");

        SetSliderOpacity(0.6);
    }

    auto SliderWidget::Impl::SetSliderOpacity(double value) -> void {
        auto* oe = new QGraphicsOpacityEffect(ui->indexSlider);
        oe->setOpacity(value);
        ui->indexSlider->setGraphicsEffect(oe);
        ui->indexSlider->setAutoFillBackground(true);
    }
}
