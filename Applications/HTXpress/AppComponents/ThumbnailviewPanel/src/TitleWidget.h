﻿#pragma once

#include <QWidget>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class TitleWidget : public QWidget{
        Q_OBJECT
    public:
        explicit TitleWidget(QWidget* parent = nullptr);    
        ~TitleWidget() override;

        auto ClearDataLabel() -> void;
        auto SetDataLabelText(const QString& name) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}