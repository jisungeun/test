﻿#include <QGraphicsScene>
#include <QDebug>

#include "ThumbnailCanvas.h"

#include <QResizeEvent>

#include "GraphicsImageItem.h"
#include "CanvasZoomSupport.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct ThumbnailCanvas::Impl {
        std::shared_ptr<QGraphicsScene> scene{nullptr};
        GraphicsImageItem::Pointer imageItem{nullptr};
        CanvasZoomSupport::Pointer zoomSupport{nullptr};

        auto Initialize(QWidget* parent = nullptr) -> void;
        auto EnableZoom(Self* me);
        auto InitFitInView(Self* me) -> void;
    };

    auto ThumbnailCanvas::Impl::Initialize(QWidget* parent) -> void {
        Q_UNUSED(parent)
        scene = std::make_shared<QGraphicsScene>();
        imageItem = std::make_shared<GraphicsImageItem>();
        scene->addItem(imageItem.get());
    }

    auto ThumbnailCanvas::Impl::EnableZoom(Self* me) {
        zoomSupport = std::make_shared<CanvasZoomSupport>(me);
    }

    auto ThumbnailCanvas::Impl::InitFitInView(Self* me) -> void {
        me->fitInView(imageItem.get(), Qt::KeepAspectRatio);
    }

    ThumbnailCanvas::ThumbnailCanvas(QWidget* parent) : QGraphicsView(parent), d{new Impl} {
        setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
        setOptimizationFlags(DontSavePainterState);
        setViewportUpdateMode(SmartViewportUpdate);
        setTransformationAnchor(AnchorUnderMouse);
        setDragMode(RubberBandDrag);
        setMouseTracking(true);

        d->Initialize(this);
        d->EnableZoom(this);
        setScene(d->scene.get());
    }

    ThumbnailCanvas::~ThumbnailCanvas() {
    }

    auto ThumbnailCanvas::SetImageItem(const QImage& image) -> void {
        const auto pixmap = QPixmap::fromImage(image);
        const auto sceneX = -image.width() / 2.;
        const auto sceneY = -image.height() / 2.;
        const auto sceneW = image.width();
        const auto sceneH = image.height();
        d->scene->setSceneRect(sceneX, sceneY, sceneW, sceneH);
        d->imageItem->setPos(sceneX, sceneY);
        d->imageItem->setPixmap(pixmap);
        d->InitFitInView(this);
    }

    auto ThumbnailCanvas::resizeEvent(QResizeEvent* event) -> void {
        emit sigCanvasWidthResized();
        QGraphicsView::resizeEvent(event);
    }
}
