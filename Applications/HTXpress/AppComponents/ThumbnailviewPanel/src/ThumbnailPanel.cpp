﻿#include <QGridLayout>
#include <QTime>
#include <QResizeEvent>

#include "ThumbnailPanel.h"
#include "ui_ThumbnailPanel.h"
#include "ThumbnailPanelControl.h"
#include "ModalityWidget.h"
#include "SyncWidget.h"
#include "ImageView.h"
#include "SubWidgetPosUpdater.h"
#include "SliderWidget.h"
#include "TitleWidget.h"
#include "CanvasZoomSupport.h"
#include "ZoomWidget.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct ThumbnailPanel::Impl {
        Ui::ThumbnailPanel* ui{nullptr};
        ThumbnailPanelControl control;

        std::shared_ptr<SubWidgetPosUpdater> posUpdater{nullptr};
        std::shared_ptr<CanvasZoomSupport> zoomSupport{nullptr};

        TitleWidget* titleWidget{nullptr};
        SliderWidget* sliderWidget{nullptr};
        ZoomWidget* zoomWidget{nullptr};

        const QImage noImage{":/thumbnail/ic-noimage.svg"};
        auto Convert(ModalityWidget::ModalityType modality) -> ModalityIndex;
        auto Convert(ModalityIndex modality) -> ModalityWidget::ModalityType;
    };

    ThumbnailPanel::ThumbnailPanel(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui = new Ui::ThumbnailPanel;
        d->ui->setupUi(this);

        d->zoomSupport = std::make_shared<CanvasZoomSupport>(d->ui->canvas);
        d->zoomWidget = new ZoomWidget(d->ui->canvas);

        d->posUpdater = std::make_shared<SubWidgetPosUpdater>(d->ui->canvas);
        d->titleWidget = new TitleWidget(this);
        d->sliderWidget = new SliderWidget(this);

        d->posUpdater->SetSubwidget(d->titleWidget, SubWidgetPosUpdater::SubWidgetGeomertry::TopLeft, 0, 0);
        d->posUpdater->SetSubwidget(d->sliderWidget, SubWidgetPosUpdater::SubWidgetGeomertry::BottomLeft, 0, 0);
        d->posUpdater->SetSubwidget(d->zoomWidget, SubWidgetPosUpdater::SubWidgetGeomertry::BottomRight, 0, 14);

        connect(d->ui->modalityWidget, &ModalityWidget::sigModalityChanged, this, &Self::onModalityChanged);
        connect(d->sliderWidget, &SliderWidget::sigSliderIndexChanged, this, &Self::onImageIndexChanged);
        connect(d->ui->syncWidget, &SyncWidget::sigSyncModeChanged, this, &Self::onSyncModeChanged);
        connect(d->zoomWidget, &ZoomWidget::sigZooming, this, &Self::onZoomRequest);
        connect(d->ui->canvas, &ImageView::sigChangeHorizontalScrollBarVisiblility, this, &Self::onUpdateSubwidgetPos);
        connect(d->ui->canvas, &ImageView::sigChangeVerticalScrollBarVisiblility, this, &Self::onUpdateSubwidgetPos);

        d->ui->rightPanel->setObjectName("panel");
    }

    ThumbnailPanel::~ThumbnailPanel() {
        delete d->ui;
    }

    auto ThumbnailPanel::Clear(bool isInit) -> void {
        d->ui->canvas->SetImageItem({}, false);
        d->titleWidget->ClearDataLabel();
        d->sliderWidget->SetCurrentSequenceIndex(0);
        d->sliderWidget->SetCurrentTime("00:00:00");
        d->sliderWidget->SetSequenceCount(0);

        if(isInit) {
            d->ui->syncWidget->SetSyncMode(false);
            d->ui->modalityWidget->SetModality(ModalityWidget::ModalityType::HT);
        }
    }

    auto ThumbnailPanel::SetCurrentImage(const QImage& image, bool fitInView) -> void {
        d->control.SetImage(image);

        if(image.isNull()) {
            d->ui->canvas->SetImageItem(d->noImage, false);
        }
        else d->ui->canvas->SetImageItem(image, fitInView);

    }

    auto ThumbnailPanel::GetCurrentImage() const -> const QImage& {
        return d->control.GetImage();
    }

    auto ThumbnailPanel::SetCurrentSyncMode(bool sync) -> void {
        d->ui->syncWidget->SetSyncMode(sync);
    }

    auto ThumbnailPanel::GetCurrentSyncMode() const -> bool {
        return d->ui->syncWidget->GetSyncMode();
    }

    auto ThumbnailPanel::SetCurrentTimeInSec(int32_t seconds) -> void {
        const auto time = QDateTime::fromTime_t(seconds).toUTC().toString("hh:mm:ss");
        d->sliderWidget->SetCurrentTime(time);
    }

    auto ThumbnailPanel::GetCurrentTimeInSec() const -> int32_t {
        const auto timeInSec = static_cast<int32_t>(QDateTime::fromString(d->sliderWidget->GetCurrentTime(), "hh:mm:ss").toTime_t());
        return timeInSec;
    }

    auto ThumbnailPanel::SetCurrentSliderIndex(int32_t index) -> void {
        d->sliderWidget->SetCurrentSequenceIndex(index);
    }

    auto ThumbnailPanel::GetCurrentSliderIndex() const -> int32_t {
        return d->sliderWidget->GetCurrentSequenceIndex();
    }

    auto ThumbnailPanel::SetCurrentModality(ModalityIndex modality) -> void {
        d->ui->modalityWidget->SetModality(d->Convert(modality));
    }

    auto ThumbnailPanel::GetCurrentModality() const -> ModalityIndex {
        return d->Convert(d->ui->modalityWidget->GetCurrentModality());
    }

    auto ThumbnailPanel::SetCurrentImageName(const QString& name) -> void {
        d->titleWidget->SetDataLabelText(name);
    }

    auto ThumbnailPanel::SetTotalSequenceCount(int32_t count) -> void {
        d->sliderWidget->SetSequenceCount(count);
    }

    void ThumbnailPanel::resizeEvent(QResizeEvent* event) {
        onUpdateSubwidgetPos();
        QWidget::resizeEvent(event);
    }

    void ThumbnailPanel::onModalityChanged() {
        const auto modality = d->Convert(d->ui->modalityWidget->GetCurrentModality());
        emit sigModalityChanged(modality);
    }

    void ThumbnailPanel::onImageIndexChanged(int32_t index) {
        emit sigImageIndexChanged(index);
    }

    void ThumbnailPanel::onSyncModeChanged(bool sync) {
        emit sigSyncModeChanged(sync);
    }

    void ThumbnailPanel::onZoomRequest(int32_t zoom) {
        const auto request = static_cast<ZoomWidget::ZoomButtonRole>(zoom);
        switch (request) {
            case ZoomWidget::ZoomButtonRole::ZoomFit: d->zoomSupport->ZoomFit(); break;
            case ZoomWidget::ZoomButtonRole::ZoomIn: d->zoomSupport->ZoomIn(); break;
            case ZoomWidget::ZoomButtonRole::ZoomOut: d->zoomSupport->ZoomOut(); break;
        }
    }

    void ThumbnailPanel::onUpdateSubwidgetPos() {
        const auto sliderHeight = d->sliderWidget->height();
        const auto scrollbarHeightOffset = d->ui->canvas->GetHorizontalScrollBarHeightOffset();
        const auto scrollbarWidthOffset = d->ui->canvas->GetVerticalScrollBarWidthOffset();

        d->posUpdater->ChangeHorizontalPadding(d->zoomWidget, sliderHeight+scrollbarHeightOffset);
        d->posUpdater->ChangeHorizontalPadding(d->sliderWidget, scrollbarHeightOffset);
        d->sliderWidget->setFixedWidth(d->ui->canvas->width()-scrollbarWidthOffset);

        d->posUpdater->onUpdatePosition();
    }

    auto ThumbnailPanel::Impl::Convert(ModalityWidget::ModalityType modality) -> ModalityIndex {
        switch (modality) {
            case ModalityWidget::ModalityType::HT: return ModalityIndex::HT;
            case ModalityWidget::ModalityType::FL: return ModalityIndex::FL;
            case ModalityWidget::ModalityType::BF: return ModalityIndex::BF;
        }
        return ModalityIndex::HT;
    }

    auto ThumbnailPanel::Impl::Convert(ModalityIndex modality) -> ModalityWidget::ModalityType {
        switch (modality) {
            case ModalityIndex::HT: return ModalityWidget::ModalityType::HT;
            case ModalityIndex::FL: return ModalityWidget::ModalityType::FL;
            case ModalityIndex::BF: return ModalityWidget::ModalityType::BF;
        }
        return ModalityWidget::ModalityType::HT;
    }
}
