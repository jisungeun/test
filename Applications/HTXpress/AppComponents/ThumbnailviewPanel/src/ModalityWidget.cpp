﻿#include <QButtonGroup>
#include <QPushButton>
#include <QVBoxLayout>

#include "ModalityWidget.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct ModalityWidget::Impl {
        ModalityType modality;
        QButtonGroup* buttonGroup{nullptr};

        QPushButton* btnHT{nullptr};
        QPushButton* btnFL{nullptr};
        QPushButton* btnBF{nullptr};
    };

    ModalityWidget::ModalityWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->btnHT = new QPushButton("HT", this);
        d->btnFL = new QPushButton("FL", this);
        d->btnBF = new QPushButton("BF", this);

        d->buttonGroup = new QButtonGroup(this);
        d->buttonGroup->addButton(d->btnHT, static_cast<int32_t>(ModalityType::HT));
        d->buttonGroup->addButton(d->btnFL, static_cast<int32_t>(ModalityType::FL));
        d->buttonGroup->addButton(d->btnBF, static_cast<int32_t>(ModalityType::BF));

        const auto layout = new QVBoxLayout;
        for(const auto& btn : d->buttonGroup->buttons()) {
            btn->setCheckable(true);
            btn->setAutoExclusive(true);
            btn->setFixedSize(36,34);
            btn->setObjectName("bt-toggle");
            layout->addWidget(btn, 0, Qt::AlignCenter);
        }
        layout->setContentsMargins(0,0,0,0);
        layout->setSpacing(6);

        setLayout(layout);
        setFixedSize(36, 34*3 + 6*2);

        connect(d->buttonGroup, &QButtonGroup::idToggled, this, &Self::onModalitySelected);

        // init activated modality
        d->btnHT->setChecked(true);
    }

    ModalityWidget::~ModalityWidget() = default;

    auto ModalityWidget::SetModality(const ModalityType& modality) -> void {
        if(d->buttonGroup->checkedButton() != d->buttonGroup->button(static_cast<int32_t>(modality))) {
            d->buttonGroup->button(static_cast<int32_t>(modality))->blockSignals(true);
            d->buttonGroup->button(static_cast<int32_t>(modality))->setChecked(true);
            d->buttonGroup->button(static_cast<int32_t>(modality))->blockSignals(false);
        }
    }

    auto ModalityWidget::GetCurrentModality() const -> ModalityType {
        return d->modality;
    }

    void ModalityWidget::onModalitySelected(int32_t which, bool selected) {
        if(selected) {
            d->modality = static_cast<ModalityType>(which);
            emit sigModalityChanged();
        }
    }
}
