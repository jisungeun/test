﻿#pragma once

#include <memory>

#include <QGraphicsView>
#include <QScrollBar>

#include <enum.h>

#include "ZoomWidget.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class ImageView : public QGraphicsView {
        Q_OBJECT
    public:
        using Self = ImageView;
        using Pointer = std::shared_ptr<Self>;

        explicit ImageView(QWidget* parent = nullptr);
        ~ImageView() override;

        auto SetImageItem(const QImage& image, bool fitInView) -> void;

        auto GetHorizontalScrollBarHeightOffset() const -> int32_t;
        auto GetVerticalScrollBarWidthOffset() const -> int32_t;

    signals:
        void sigChangeHorizontalScrollBarVisiblility();
        void sigChangeVerticalScrollBarVisiblility();

    private slots:
        void onSetHorizontalScrollbarPaddingOffset(int32_t padding);
        void onSetVerticalScrollbarPaddingOffset(int32_t padding);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class ImageViewScrollBar : public QScrollBar {
        Q_OBJECT
    public:
        explicit ImageViewScrollBar(Qt::Orientation ori, QWidget *parent);
        ~ImageViewScrollBar() override;

    protected:
        void showEvent(QShowEvent* event) override;
        void hideEvent(QHideEvent* event) override;

    signals:
        void sigUpdatePaddingOffset(int32_t);
    };
}
