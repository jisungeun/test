﻿#include "ThumbnailPanelControl.h"
#include <QImage>
#include <QMap>

#include "ThumbnailCustomDataType.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct ThumbnailPanelControl::Impl {
        QImage image{};
    };

    ThumbnailPanelControl::ThumbnailPanelControl() : d{std::make_unique<Impl>()} {
    }

    ThumbnailPanelControl::~ThumbnailPanelControl() = default;

    auto ThumbnailPanelControl::SetImage(const QImage& image) -> void {
        d->image = image;
    }

    auto ThumbnailPanelControl::GetImage() const -> const QImage& {
        return d->image;
    }
}
