﻿#pragma once

#include <memory>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class IndexSliderControl {
    public:
        using Self = IndexSliderControl;
        using Pointer = std::shared_ptr<Self>;

        IndexSliderControl();
        ~IndexSliderControl();

        auto SetAutoMode(bool isAuto) -> void;
        auto IsAutoMode() const -> bool;

        auto MinimumValue() -> int32_t;

        auto SetMaximumValue(const int32_t& max) -> void;
        auto MaximumValue() const -> int32_t;

        auto SetCurrentValue(const int32_t& curr) -> void;
        auto CurrentValue() const -> int32_t;

        auto SetTickIntervalByTotalCount(const int32_t& totalCount) -> void;
        auto TickInterval() const -> int32_t;
        auto SingleStep() const -> int32_t;
        auto TickPosition() const -> QSlider::TickPosition;
        auto Orientation() const -> Qt::Orientation;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
