﻿#pragma once
#include <cstdint>

namespace HTXpress::AppComponents::ThumbnailviewPanel {
	constexpr int32_t kLatestImageIndex = -1;
}
