﻿#include <QGridLayout>
#include <QDebug>
#include <QCheckBox>
#include <QGraphicsView>
#include <QLabel>

#include "ImageIndexSliderWidget.h"
#include "IndexSlider.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct ImageIndexSliderWidget::Impl {
        IndexSlider::Pointer slider{nullptr};

        std::shared_ptr<QCheckBox> checkBox{nullptr};

        auto Initialize(QWidget* parent = nullptr) -> void;
        auto Layout() -> QLayout*;

        auto MakeConnect(Self* me) -> void;
    };

    auto ImageIndexSliderWidget::Impl::Initialize(QWidget* parent) -> void {
        slider = std::make_shared<IndexSlider>(Qt::Horizontal, parent);
        checkBox = std::make_shared<QCheckBox>("Auto Mode", parent);
        checkBox->setChecked(slider->IsAutoMode());
    }

    auto ImageIndexSliderWidget::Impl::Layout() -> QLayout* {
        auto sublayout = new QHBoxLayout;
        sublayout->setSpacing(10);
        sublayout->setMargin(5);
        sublayout->setContentsMargins(1, 1, 1, 1);
        sublayout->addWidget(slider.get());
        sublayout->addWidget(checkBox.get());

        auto layout = new QGridLayout;
        layout->setSpacing(0);
        layout->setMargin(0);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSizeConstraint(QLayout::SetMinAndMaxSize);
        layout->addLayout(sublayout, 0, 0, Qt::AlignCenter);
        return layout;
    }

    auto ImageIndexSliderWidget::Impl::MakeConnect(Self* me) -> void {
        connect(slider.get(), &IndexSlider::valueChanged, me, &Self::sigCurrentImageIndex);
        connect(checkBox.get(), &QCheckBox::toggled, me, &Self::onAutoModeCheckBoxToggled);
    }

    ImageIndexSliderWidget::ImageIndexSliderWidget(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->Initialize(this);
        d->MakeConnect(this);
        setLayout(d->Layout());
    }

    ImageIndexSliderWidget::~ImageIndexSliderWidget() {
    }

    auto ImageIndexSliderWidget::SetImageCount(const int32_t& val) -> void {
        d->slider->SetImageIndexCount(val);
    }

    auto ImageIndexSliderWidget::SetCurrentImageIndex(const int32_t& index) -> void {
        d->slider->onSetCurrentIndex(index);
    }

    void ImageIndexSliderWidget::onAutoModeCheckBoxToggled(bool isAuto) {
        d->slider->SetAutoMode(isAuto);
    }
}
