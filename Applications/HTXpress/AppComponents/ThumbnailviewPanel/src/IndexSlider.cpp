﻿#include "IndexSlider.h"
#include "IndexSliderControl.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct IndexSlider::Impl {
        IndexSliderControl::Pointer control{std::make_shared<IndexSliderControl>()};

        auto UpdateCurrentValue(IndexSlider* me) -> void;
        auto UpdateMaximumValue(IndexSlider* me) -> void;
        auto UpdateSliderProperty(IndexSlider* me) -> void;
        auto UpdateImageCount(IndexSlider* me) -> void;
        auto UpdateSliderRange(IndexSlider* me) -> void;
        auto UpdateTickInterval(IndexSlider* me) -> void;
        auto MakeConnect(IndexSlider* me) -> void;
    };

    IndexSlider::IndexSlider(Qt::Orientation ori, QWidget* parent) : QSlider(ori, parent), d{new Impl} {
        d->UpdateMaximumValue(this);
        d->UpdateCurrentValue(this);
        d->UpdateSliderProperty(this);
        d->MakeConnect(this);

        setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        connect(this, &Self::valueChanged, [this](int value) {
            setToolTip(QString::number(value));
        });
    }

    IndexSlider::~IndexSlider() {
    }

    auto IndexSlider::SetAutoMode(bool isAuto) -> void {
        d->control->SetAutoMode(isAuto);
    }

    auto IndexSlider::IsAutoMode() const -> bool {
        return d->control->IsAutoMode();
    }

    auto IndexSlider::SetImageIndexCount(const int32_t& count) -> void {
        d->control->SetMaximumValue(count);
        d->UpdateImageCount(this);
        d->UpdateTickInterval(this);
    }

    void IndexSlider::onSetCurrentIndex(int32_t currentIndex) {
        d->control->SetCurrentValue(currentIndex);
        d->UpdateCurrentValue(this);
    }

    auto IndexSlider::Impl::MakeConnect(IndexSlider* me) -> void {
        connect(me, &Self::valueChanged, me, &Self::onSetCurrentIndex);
    }

    auto IndexSlider::Impl::UpdateCurrentValue(IndexSlider* me) -> void {
        me->setValue(control->CurrentValue());
    }

    auto IndexSlider::Impl::UpdateMaximumValue(IndexSlider* me) -> void {
        me->setMaximum(control->MaximumValue());
    }

    auto IndexSlider::Impl::UpdateSliderProperty(IndexSlider* me) -> void {
        me->setTickInterval(control->TickInterval());
        me->setTickPosition(control->TickPosition());
        me->setSingleStep(control->SingleStep());
        me->setOrientation(control->Orientation());
    }

    auto IndexSlider::Impl::UpdateImageCount(IndexSlider* me) -> void {
        UpdateSliderRange(me);

        if (control->IsAutoMode()) {
            control->SetCurrentValue(control->MaximumValue());
            UpdateCurrentValue(me);
        }
    }

    auto IndexSlider::Impl::UpdateSliderRange(IndexSlider* me) -> void {
        me->setRange(control->MinimumValue(), control->MaximumValue());
    }

    auto IndexSlider::Impl::UpdateTickInterval(IndexSlider* me) -> void {
        const auto& count = control->MaximumValue();
        control->SetTickIntervalByTotalCount(count);
        me->setTickInterval(control->TickInterval());
    }
}
