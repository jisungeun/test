﻿#include <QApplication>
#include <QGraphicsView>
#include <QMouseEvent>
#include <qmath.h>

#include "CanvasZoomSupport.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
     struct CanvasZoomSupport::Impl {
        QGraphicsView* view{nullptr};

        const Qt::KeyboardModifiers zoomKeyModifiers{Qt::ControlModifier};
        double currLod{1};
        QPointF targetScenePos;

        struct {
            double maxZoomScale{20};
            double minZoomScale{0.01};
        } zoomRange;

        auto IsZoomInAvailable() -> bool;
        auto IsZoomOutAvailable() -> bool;
    };

    auto CanvasZoomSupport::Impl::IsZoomInAvailable() -> bool {
        if (currLod > zoomRange.maxZoomScale) return false;
        return true;
    }

    auto CanvasZoomSupport::Impl::IsZoomOutAvailable() -> bool {
        if (currLod < zoomRange.minZoomScale) return false;
        return true;
    }

    CanvasZoomSupport::CanvasZoomSupport(QGraphicsView* view) : d{std::make_unique<Impl>()} {
        d->view = view;
        d->view->viewport()->installEventFilter(this);
        d->view->setMouseTracking(true);
    }

    CanvasZoomSupport::~CanvasZoomSupport() {
    }

    auto CanvasZoomSupport::SetCurrentLod(const double& lod) -> void {
        d->currLod = lod;
    }

    auto CanvasZoomSupport::ZoomIn(bool onMousePos) const -> void {
        if (!d->IsZoomInAvailable()) return;
        d->view->scale(1.13, 1.13);
        if (onMousePos) {
            d->view->centerOn(d->targetScenePos);
        }
        else {
            d->view->centerOn(0,0);
        }
    }

    auto CanvasZoomSupport::ZoomOut(bool onMousePos) const -> void {
        if (!d->IsZoomOutAvailable()) return;
        d->view->scale(0.89, 0.89);
        if (onMousePos) {
            d->view->centerOn(d->targetScenePos);
        }
        else {
            d->view->centerOn(0,0);
        }
    }

    auto CanvasZoomSupport::ZoomFit() const -> void {
        d->view->fitInView(d->view->sceneRect(), Qt::KeepAspectRatio);
    }

    auto CanvasZoomSupport::eventFilter(QObject* watched, QEvent* event) -> bool {
        Q_UNUSED(watched)

        if (event->type() == QEvent::MouseMove) {
            const auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
            d->targetScenePos = d->view->mapToScene(mouseEvent->pos());
        }
        else if (event->type() == QEvent::Wheel) {
            const auto wheelEvent = dynamic_cast<QWheelEvent*>(event);
            if (QApplication::keyboardModifiers() == d->zoomKeyModifiers) {
                const double angle = wheelEvent->angleDelta().y();
                if (angle > 0) {
                    ZoomIn(true);
                }
                else {
                    ZoomOut(true);
                }
                return true;
            }
        }
        else if (event->type() == QEvent::MouseButtonDblClick) {
            const auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
            if (mouseEvent->button() == Qt::RightButton) {
                ZoomFit();
                return true;
            }
        }

        return false;
    }
}
