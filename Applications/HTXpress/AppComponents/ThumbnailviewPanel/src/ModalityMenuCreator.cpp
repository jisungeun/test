﻿#include <QMenu>

#include "ModalityMenuCreator.h"

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    struct ModalityMenuCreator::Impl {

    };

    ModalityMenuCreator::ModalityMenuCreator(QObject* parent) : QObject(parent), d{new Impl} {
    }

    ModalityMenuCreator::~ModalityMenuCreator() {
    }

    auto ModalityMenuCreator::MakeModalityMenu(const QMap<int32_t, QString>& modalities) -> QMenu* {
        const auto menu = new QMenu();

        auto it = modalities.cbegin();
        while (it != modalities.cend()) {
            const auto action = new QAction(it.value());
            action->setData(it.key());
            menu->addAction(action);
            ++it;
        }

        connect(menu, &QMenu::triggered, [this](QAction* action) {
            const int32_t modalityIndex = action->data().toInt();
            emit sigModalityIndex(modalityIndex);
        });

        return menu;
    }
}
