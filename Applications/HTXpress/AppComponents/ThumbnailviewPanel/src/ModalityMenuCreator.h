﻿#pragma once
#include <memory>

#include <QObject>

class QMenu;

namespace HTXpress::AppComponents::ThumbnailviewPanel {
    class ModalityMenuCreator : public QObject {
        Q_OBJECT
    public:
        using Self = ModalityMenuCreator;
        using Pointer = std::shared_ptr<Self>;

        ModalityMenuCreator(QObject* parent = nullptr);
        ~ModalityMenuCreator() override;

        auto MakeModalityMenu(const QMap<int32_t, QString>& modalities) -> QMenu*;

    signals:
        void sigModalityIndex(int32_t);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;

    };
}
