﻿#pragma once
#include "MenuItem.h"

namespace HTXpress::AppComponents::ViewerOptionMenu {
    class MenuItemShowCenterMark : public MenuItem{
        Q_OBJECT
    public:
        explicit MenuItemShowCenterMark(QObject* parent = nullptr);
        ~MenuItemShowCenterMark() override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
