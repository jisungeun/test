#include <QPushButton>
#include <QVBoxLayout>
#include <QGraphicsOpacityEffect>
#include <QMenu>

#include "ViewerOptionMenuWidget.h"
#include "MenuItem.h"

namespace HTXpress::AppComponents::ViewerOptionMenu {
    struct ViewerOptionMenuWidget::Impl {
        QPushButton* menuButton{nullptr};
        QMenu *menu{nullptr};

        auto SetOpacity(qreal opacity, Self* self) -> void;
        auto GetMenuItem(const MenuIndex& menuIndex) const -> MenuItem*;
    };

    auto ViewerOptionMenuWidget::Impl::SetOpacity(qreal opacity, Self* self) -> void {
        auto* oe = new QGraphicsOpacityEffect(self);
        oe->setOpacity(opacity);
        self->setGraphicsEffect(oe);
    }

    auto ViewerOptionMenuWidget::Impl::GetMenuItem(const MenuIndex& menuIndex) const -> MenuItem* {
        for(const auto& action : menu->actions()) {
            if(action->data().toInt() == menuIndex) {
                return qobject_cast<MenuItem*>(action);
            }
        }
        return nullptr;
    }

    ViewerOptionMenuWidget::ViewerOptionMenuWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->menuButton = new QPushButton(this);
        d->menuButton->setStyleSheet("QPushButton {border:0px none; background-color:transparent;}\n"
                                     "QPushButton::menu-indicator{width:0px;}");
        d->menuButton->setFixedSize({24,24});
        d->menuButton->setIconSize({24,24});
        d->menuButton->setIcon(QIcon(":img/ic-float-img.svg"));

        d->menu = new QMenu(this);
        d->menuButton->setMenu(d->menu);

        connect(d->menu, &QMenu::triggered, this, &Self::onMenuTriggered);

        const auto layout = new QVBoxLayout(this);
        layout->addWidget(d->menuButton, 0, Qt::AlignCenter);
        layout->setContentsMargins(0, 0, 0, 0);

        setLayout(layout);
        setFixedSize(24,24);

        setAutoFillBackground(true);
        d->SetOpacity(0.2, this);
    }

    ViewerOptionMenuWidget::~ViewerOptionMenuWidget() {
    }

    auto ViewerOptionMenuWidget::AddMenu(const MenuIndex& menuIndex) -> void {
        const auto action = MenuItem::CreateAction(menuIndex, this);
        if(action) {
            d->menu->addAction(action);
        }
    }

    auto ViewerOptionMenuWidget::SetScalebarVisible(bool show) -> void {
        const auto action = d->GetMenuItem(MenuIndex::ShowScalebar);
        if(action) {
            action->setChecked(show);
            emit sigMenuTriggered(MenuIndex::ShowScalebar);
        }
    }

    auto ViewerOptionMenuWidget::GetScalebarVisible() const -> bool {
        const auto action = d->GetMenuItem(MenuIndex::ShowScalebar);
        if(action) {
            return action->isChecked();
        }

        return false;
    }

    auto ViewerOptionMenuWidget::SetCenterMarkVisible(bool show) -> void {
        const auto action = d->GetMenuItem(MenuIndex::ShowCenterMark);
        if(action) {
            action->setChecked(show);
            emit sigMenuTriggered(MenuIndex::ShowCenterMark);
        }
    }

    auto ViewerOptionMenuWidget::GetCenterMarkVisible() const -> bool {
        const auto action = d->GetMenuItem(MenuIndex::ShowCenterMark);
        if(action) {
            return action->isChecked();
        }
        return false;
    }

    void ViewerOptionMenuWidget::enterEvent(QEvent* event) {
        d->SetOpacity(1.0, this);
        QWidget::enterEvent(event);
    }

    void ViewerOptionMenuWidget::leaveEvent(QEvent* event) {
        d->SetOpacity(0.2, this);
        QWidget::leaveEvent(event);
    }

    void ViewerOptionMenuWidget::onMenuTriggered(QAction* action) {
        if (!action) {
            return;
        }

        bool ok = false;
        const auto value = action->data().toInt(&ok);
        if (ok) {
            emit sigMenuTriggered(MenuIndex::_from_integral(value));
        }
    }
}
