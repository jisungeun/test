﻿#pragma once
#include "MenuItem.h"

namespace HTXpress::AppComponents::ViewerOptionMenu {
    class MenuItemShowScalebar : public MenuItem{
        Q_OBJECT
    public:
        explicit MenuItemShowScalebar(QObject* parent = nullptr);
        ~MenuItemShowScalebar() override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
