﻿#pragma once
#include <memory>

#include <QObject>
#include <QAction>

#include "ViewerOptionDefines.h"

namespace HTXpress::AppComponents::ViewerOptionMenu {
    class MenuItem : public QAction{
        Q_OBJECT
    public:
        explicit MenuItem(QObject* parent = nullptr);
        ~MenuItem() override;

        [[nodiscard]] static auto CreateAction(const MenuIndex& menuIndex, QObject* parent) -> MenuItem*;
    };
}
