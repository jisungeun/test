﻿#include "MenuItemShowScalebar.h"

namespace HTXpress::AppComponents::ViewerOptionMenu {
    struct MenuItemShowScalebar::Impl {
        
    };

    MenuItemShowScalebar::MenuItemShowScalebar(QObject* parent) : MenuItem(parent), d{std::make_unique<Impl>()} {
        setCheckable(true);
        setText("Show scalebar");
        setData(MenuIndex::ShowScalebar);
    }

    MenuItemShowScalebar::~MenuItemShowScalebar() {
    }
}
