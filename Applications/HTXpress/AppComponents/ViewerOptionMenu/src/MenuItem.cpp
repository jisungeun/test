﻿#include "MenuItem.h"
#include "MenuItemShowScalebar.h"
#include "MenuItemShowCenterMark.h"

namespace HTXpress::AppComponents::ViewerOptionMenu {
    MenuItem::MenuItem(QObject* parent) : QAction(parent) {
    }

    MenuItem::~MenuItem() {
    }

    auto MenuItem::CreateAction(const MenuIndex& menuIndex, QObject* parent) -> MenuItem* {
        switch(menuIndex) {
        case MenuIndex::ShowScalebar:
            return new MenuItemShowScalebar(parent);
        case MenuIndex::ShowCenterMark:
            return new MenuItemShowCenterMark(parent);
        default:
            return nullptr;
        }
    }
}
