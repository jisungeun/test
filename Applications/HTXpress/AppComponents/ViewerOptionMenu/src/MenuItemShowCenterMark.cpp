﻿#include "MenuItemShowCenterMark.h"

namespace HTXpress::AppComponents::ViewerOptionMenu {
    struct MenuItemShowCenterMark::Impl {
    };

    MenuItemShowCenterMark::MenuItemShowCenterMark(QObject* parent) : MenuItem(parent), d{std::make_unique<Impl>()} {
        setCheckable(true);
        setText("Show center mark");
        setData(MenuIndex::ShowCenterMark);
    }

    MenuItemShowCenterMark::~MenuItemShowCenterMark() {
    }
}
