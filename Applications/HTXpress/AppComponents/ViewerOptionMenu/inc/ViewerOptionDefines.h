﻿#pragma once

#include <enum.h>

namespace HTXpress::AppComponents::ViewerOptionMenu {
    BETTER_ENUM(MenuIndex, int32_t, 
                ShowScalebar = 71,
                ShowCenterMark = 72
    );
}
