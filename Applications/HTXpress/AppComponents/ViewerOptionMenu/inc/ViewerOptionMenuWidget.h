#pragma once
#include <memory>

#include <QWidget>

#include <enum.h>

#include "HTXViewerOptionMenuExport.h"
#include "ViewerOptionDefines.h"

namespace HTXpress::AppComponents::ViewerOptionMenu {
    class HTXViewerOptionMenu_API ViewerOptionMenuWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = ViewerOptionMenuWidget;
        using Pointer = std::shared_ptr<Self>;

        explicit ViewerOptionMenuWidget(QWidget* parent = nullptr);
        ~ViewerOptionMenuWidget() override;

        auto AddMenu(const MenuIndex& menuIndex) -> void;

        auto SetScalebarVisible(bool show) -> void;
        auto GetScalebarVisible() const -> bool;

        auto SetCenterMarkVisible(bool show) -> void;
        auto GetCenterMarkVisible() const -> bool;

    protected:
        auto enterEvent(QEvent* event) -> void override;
        auto leaveEvent(QEvent* event) -> void override;

    signals:
        void sigMenuTriggered(const MenuIndex&);

    private slots:
        void onMenuTriggered(QAction* action);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
