#define LOGGER_TAG "[FLLightReader]"
#include <QSettings>
#include <QFileInfo>

#include <TCLogger.h>
#include <AppEntityDefines.h>

#include "FLLightIODefines.h"
#include "FLLightIOReader.h"

namespace HTXpress::AppComponents::FLLightIO {
    auto ReadField(const QSettings& qs, const char key[])->QVariant {
        if(!qs.contains(key)) throw std::runtime_error(std::string("Missing key in fl light config: ") + key);
        return qs.value(key);
    }

    auto CreateSample(const QString& path)->void {
        const QString samplePath{ QString("%1.sample").arg(path) };
        QSettings qs(samplePath, QSettings::IniFormat);

        qs.setValue(Key::currentExcFilter,AppEntity::ExcFilter::Internal);

        qs.beginWriteArray(Key::internalFlExc);
        for(auto idx = 0; idx < 5; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(Key::internalFlExcChannelIdx, idx);
            qs.setValue(Key::internalFlExcWaveLength, 200+idx*5);
            qs.setValue(Key::internalFlExcBandwidth, 20+idx*5);
        }
        qs.endArray();

        qs.beginWriteArray(Key::externalFlExc);
        for(auto idx = 0; idx < 3; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(Key::externalFlExcChannelIdx, idx);
            qs.setValue(Key::externalFlExcWaveLength, 300+idx*7);
            qs.setValue(Key::externalFlExcBandwidth, 50+idx*7);
        }
        qs.endArray();
    }

    Reader::Reader() {
    }

    Reader::~Reader() {
    }

    auto Reader::Read(const QString& dirPath, const AppEntity::SystemConfig::Pointer& config) const -> bool {
        const auto filePath = QString("%1/%2").arg(dirPath).arg(kFileName);

        if(filePath.isEmpty()) {
            QLOG_ERROR() << "No FL Light configuration file path is specified";
            return false;
        }

        if(!QFile::exists(filePath)) {
            QLOG_ERROR() << "FL Light configuration file does not exist at " << filePath;
            CreateSample(filePath);
            return false;
        }

        QSettings qs(filePath, QSettings::IniFormat);

        try {
            //current excitation filter
            config->SetExcitationFilter(AppEntity::ExcFilter::_from_integral(ReadField(qs, Key::currentExcFilter).toInt()));

            //internal lights
            const auto internalLights = qs.beginReadArray(Key::internalFlExc);
            for(auto idx = 0; idx < internalLights; idx++) {
                AppEntity::FLFilter ex;
                qs.setArrayIndex(idx);
                ex.SetWaveLength(ReadField(qs, Key::internalFlExcWaveLength).toUInt());
                ex.SetBandwidth(ReadField(qs, Key::internalFlExcBandwidth).toUInt());

                config->SetFLInternalExcitation(ReadField(qs, Key::internalFlExcChannelIdx).toInt(), ex);
            }
            qs.endArray();

            //external lights
            const auto externalLights = qs.beginReadArray(Key::externalFlExc);
            for (auto idx = 0; idx < externalLights; idx++) {
                AppEntity::FLFilter ex;
                qs.setArrayIndex(idx);
                ex.SetWaveLength(ReadField(qs, Key::externalFlExcWaveLength).toUInt());
                ex.SetBandwidth(ReadField(qs, Key::externalFlExcBandwidth).toUInt());

                config->SetFLExternalExcitation(ReadField(qs, Key::externalFlExcChannelIdx).toInt(), ex);
            }
            qs.endArray();

        } catch(std::exception& ex) {
            QLOG_ERROR() << "It fails to read fl light configuration. Error=" << ex.what();
            CreateSample(dirPath);
            return false;
        }

        return true;
    }
}