#define LOGGER_TAG "[FLLightWriter]"
#include <QSettings>
#include <QDir>

#include <TCLogger.h>

#include "FLLightIODefines.h"
#include "FLLightIOWriter.h"

namespace HTXpress::AppComponents::FLLightIO {
    Writer::Writer() {
    }

    Writer::~Writer() {
    }

    auto Writer::Write(const QString& dirPath, const AppEntity::SystemConfig::Pointer& config) const -> bool {
        const auto filePath = QString("%1/%2").arg(dirPath).arg(kFileName);

        if (!filePath.isEmpty()) {
            QLOG_INFO() << "A new FLLight setting file is created in" << filePath;
            QDir().remove(filePath);
        }

        QSettings qs(filePath, QSettings::IniFormat);

        //current filter
        qs.setValue(Key::currentExcFilter, config->GetExcitationFilter()._to_integral());

        //internal lights
        qs.beginWriteArray(Key::internalFlExc);
        for (auto internalIdx : config->GetFLInternalExcitations()) {
            AppEntity::FLFilter excitation;
            if (!config->GetFLInternalExcitation(internalIdx, excitation)) continue;

            qs.setArrayIndex(internalIdx);

            qs.setValue(Key::internalFlExcChannelIdx, internalIdx);
            qs.setValue(Key::internalFlExcWaveLength, excitation.GetWaveLength());
            qs.setValue(Key::internalFlExcBandwidth, excitation.GetBandwidth());
        }
        qs.endArray();

        //external lights
        qs.beginWriteArray(Key::externalFlExc);
        for (auto externalIdx : config->GetFLExternalExcitations()) {
            AppEntity::FLFilter excitation;
            if (!config->GetFLExternalExcitation(externalIdx, excitation)) continue;

            qs.setArrayIndex(externalIdx);

            qs.setValue(Key::externalFlExcChannelIdx, externalIdx);
            qs.setValue(Key::externalFlExcWaveLength, excitation.GetWaveLength());
            qs.setValue(Key::externalFlExcBandwidth, excitation.GetBandwidth());
        }
        qs.endArray();

        return true;
    }
}
