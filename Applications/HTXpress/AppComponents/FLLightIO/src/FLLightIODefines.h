#pragma once

namespace HTXpress::AppComponents::FLLightIO {
    constexpr char kFileName[] = "flExcitation.ini";

    namespace Key {
        constexpr char currentExcFilter[] = "ExcitationFilter/CurrentFilter";

        constexpr char internalFlExc[] = "InternalFLExcitation";
        constexpr char internalFlExcChannelIdx[] = "Index";
        constexpr char internalFlExcWaveLength[] = "WaveLength";
        constexpr char internalFlExcBandwidth[] = "Bandwidth";

        constexpr char externalFlExc[] = "ExternalFLExcitation";
        constexpr char externalFlExcChannelIdx[] = "Index";
        constexpr char externalFlExcWaveLength[] = "WaveLength";
        constexpr char externalFlExcBandwidth[] = "Bandwidth";
    };
}
