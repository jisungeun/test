#pragma once
#include <memory>
#include <QString>

#include <SystemConfig.h>

#include "HTXFLLightIOExport.h"

namespace HTXpress::AppComponents::FLLightIO {
	class HTXFLLightIO_API Reader {
	public:
		Reader();
		~Reader();

		auto Read(const QString& dirPath, const AppEntity::SystemConfig::Pointer& config) const->bool;
	};
}
