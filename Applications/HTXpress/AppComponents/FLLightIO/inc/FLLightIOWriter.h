#pragma once
#include <memory>
#include <QString>

#include <SystemConfig.h>

#include "HTXFLLightIOExport.h"

namespace HTXpress::AppComponents::FLLightIO {
    class HTXFLLightIO_API Writer {
    public:
        Writer();
        ~Writer();

        auto Write(const QString& dirPath, const AppEntity::SystemConfig::Pointer& config) const -> bool;
    };
}
