#define LOGGER_TAG "[Model Reader]"
#include <QSettings>

#include <TCLogger.h>
#include "Fields.h"
#include "Reader.h"

namespace HTXpress::AppComponents::ModelLoader {

    auto readValue(QSettings& qs, const char key[])->QVariant {
        if(!qs.contains(key)) throw std::runtime_error(std::string("Missing key in model file: ") + key);
        return qs.value(key);
    }

    auto beginReadArray(QSettings& qs, const char key[])->int32_t {
        const auto size = qs.beginReadArray(key);
        if(size == 0) throw std::runtime_error(std::string("Missing parameters in model file: ") + key);
        return size;
    }

    struct Reader::Impl {
        QString error;

        auto SetError(const QString& message)->void;
    };

    auto Reader::Impl::SetError(const QString& message) -> void {
        error = message;
        QLOG_ERROR() << message;
    }

    Reader::Reader() : d{new Impl} {
    }
    
    Reader::~Reader() {
    }
    
    auto Reader::Read(const QString& path, ModelInstance& model) -> bool {
        QSettings qs(path, QSettings::IniFormat);

        try {
            model.SetObjectiveMagnification(readValue(qs, Key::ObjectiveLensMagnification).toDouble());
            model.SetObjectiveLensNA(readValue(qs, Key::ObjectiveLensNA).toDouble());
            model.SetCondenserLensMagnification(readValue(qs, Key::CondenserLensMagnification).toDouble());
            model.SetCondenserLensNA(readValue(qs, Key::CondenserLensNA).toDouble());
            model.SetCameraPixelSize(readValue(qs, Key::CameraPixelSize).toDouble());
            model.SetCameraPixels(readValue(qs, Key::CameraPixelsH).toUInt(),
                                  readValue(qs, Key::CameraPixelsV).toUInt());
            model.SetCameraFOVStep(readValue(qs, Key::CameraFOVStepH).toUInt(),
                                   readValue(qs, Key::CameraFOVStepV).toUInt());

            model.SetMinimumIntervalUSec(readValue(qs, Key::ImagingMinInterval).toInt());
            model.SetMaximumIntervalUSec(readValue(qs, Key::ImagingMaxInterval).toInt());
            model.SetMinimumIdleUSec(readValue(qs, Key::ImagingMinIdle).toInt());
            model.SetMinimumExposureUSec(readValue(qs, Key::ImagingMinExposure).toInt());
            model.SetMaximumExposureUSec(readValue(qs, Key::ImagingMaxExposure).toInt());
            model.SetHTLightChannel(readValue(qs, Key::ImagingHTLightChannel).toInt());
            model.SetHTFilterChannel(readValue(qs, Key::ImagingHTFilterChannel).toInt());
            model.SetBFLightChannel(readValue(qs, Key::ImagingBFLightChannel).toInt());
            model.SetBFFilterChannel(readValue(qs, Key::ImagingBFFilterChannel).toInt());
            model.SetFLMaximumSlices(readValue(qs, Key::ImagingFLMaximumSlices).toInt());
            model.SetCondenserAFLightChannel(readValue(qs, Key::ImagingCAFLightChannel).toInt());
            model.SetSafeXYRangeMM(readValue(qs, Key::ImagingSafeRangeX).toDouble(),
                                   readValue(qs, Key::ImagingSafeRangeY).toDouble());

            const auto axisCount = qs.beginReadArray("Axis");
            for(auto idx=0; idx<axisCount; idx++) {
                qs.setArrayIndex(idx);

                const auto axisName = readValue(qs, Key::AxisName).toString();
                if(axisName.isEmpty()) return false;

                const auto axis = AppEntity::Axis::_from_string(axisName.toLatin1());
                model.SetAxisResolutionPPM(axis, readValue(qs, Key::AxisResolution).toUInt());
                model.SetAxisLowerLimitMM(axis, readValue(qs, Key::AxisLowerLimit).toDouble());
                model.SetAxisUpperLimitMM(axis, readValue(qs, Key::AxisUpperLimit).toDouble());
                model.SetAxisMotionTimeSPM(axis, readValue(qs, Key::AxisMotionTime).toDouble());
            }
            qs.endArray();

            model.SetSafeZPositionMM(readValue(qs, Key::PositionsSafeZ).toDouble());
            model.SetLoadingPositionMM(readValue(qs, Key::PositionsLoadingX).toDouble(),
                                       readValue(qs, Key::PositionsLoadingY).toDouble());

            if(qs.contains(Key::PositionsEmissionFilters)) {
                auto strValue = readValue(qs, Key::PositionsEmissionFilters).toString();
                auto strPositions = strValue.split(',');
                QList<int32_t> positions;
                for(auto strPosition : strPositions) positions.push_back(strPosition.toInt());
                model.SetEmissionFilterPositions(positions);
            }

            model.SetDripTrayRemovingPositionMM(readValue(qs, Key::PositionsDripTrayX).toDouble(),
                                                readValue(qs, Key::PositionsDripTrayY).toDouble(),
                                                readValue(qs, Key::PositionsDripTrayZ).toDouble(),
                                                readValue(qs, Key::PositionsDripTrayC).toDouble());

            model.SetParkingPositionMM(readValue(qs, Key::PositionsParkingX).toDouble(),
                                       readValue(qs, Key::PositionsParkingY).toDouble(),
                                       readValue(qs, Key::PositionsParkingZ).toDouble(),
                                       readValue(qs, Key::PositionsParkingC).toDouble());

            model.SetFilterChangeTime(readValue(qs, Key::timeEstimationFilterChangeDefault).toDouble(),
                                      readValue(qs, Key::timeEstimationFilterChangePerStep).toDouble());
            model.SetSampleStageMotionTime(readValue(qs, Key::timeEstimationSampleStageDefault).toDouble(),
                                           readValue(qs, Key::timeEstimationSampleStagePerMM).toDouble());
            model.SetZScanMotionTime(readValue(qs, Key::timeEstimationZScanDefault).toDouble(),
                                     readValue(qs, Key::timeEstimationZScanPerMM).toDouble());
            model.SetTriggerWithMotionAccelerationTime(readValue(qs, Key::timeEstimationTWMAccelTime).toDouble());
            model.SetTriggerWithMotionDecelerationTime(readValue(qs, Key::timeEstimationTWMDecelTime).toDouble());
            model.SetTriggerWithMotionMarginTime(readValue(qs, Key::timeEstimationTWMMarginTime).toDouble());
            model.SetFocusToScanReadyMM(readValue(qs, Key::timeEstimationFocusToScanReady).toDouble());
            model.SetBFImagingMarginTime(readValue(qs, Key::timeEstimationBFImagingMargin).toDouble());

            model.SetHTTriggerStartOffsetPulse(readValue(qs, Key::htTriggerStartOffset).toInt());
            model.SetHTTriggerEndOffsetPulse(readValue(qs, Key::htTriggerEndOffset).toInt());
            model.SetHTTriggerPulseWidthUSec(readValue(qs, Key::htTriggerPulseWidth).toInt());
            model.SetHTTriggerReadOutUSec(readValue(qs, Key::htTriggerReadOut).toInt());
            model.SetHTTriggerExposureUSec(readValue(qs, Key::htTriggerExposure).toInt());
            model.SetHTTriggerIntervalMarginUSec(readValue(qs, Key::htTriggerIntervalMargin).toInt());
            model.SetHTTriggerAccelerationFactor(readValue(qs, Key::htTriggerAccelFactor).toInt());

            model.SetCondenserAFScanStartPosPulse(readValue(qs, Key::condenserAFStartPos).toInt());
            model.SetCondenserAFScanIntervalPulse(readValue(qs, Key::condenserAFTriggerInterval).toInt());
            model.SetCondenserAFScanTriggerSlices(readValue(qs, Key::condenserAFTriggerSlices).toInt());
            model.SetCondenserAFScanReadOutUSec(readValue(qs, Key::condenserAFReadout).toInt());
            model.SetCondenserAFScanExposureUSec(readValue(qs, Key::condenserAFExposure).toInt());
            model.SetCondenserAFScanIntervalMarginUSec(readValue(qs, Key::condenserAFIntervalMargin).toInt());
            model.SetCondenserAFScanPulseWidthUSec(readValue(qs, Key::condenserAFPulseWidth).toInt());

            model.SetMultiDishHolderThickness(readValue(qs, Key::multiDishHolderThickness).toDouble());

            const auto patterns = beginReadArray(qs, Key::illumPatterns);
            for(auto ptnIndex=0; ptnIndex<patterns; ptnIndex++) {
                qs.setArrayIndex(ptnIndex);
                const auto imagingType = AppEntity::ImagingType::_from_string(readValue(qs, Key::imagingType).toString().toLatin1());
                const auto triggerType = AppEntity::TriggerType::_from_string(readValue(qs, Key::triggerType).toString().toLatin1());
                const auto channel = readValue(qs, Key::channel).toInt();
                const auto patternIndex = readValue(qs, Key::patternIndex).toInt();
                model.SetIlluminationPattern(imagingType, triggerType, channel, patternIndex);
            }
            qs.endArray();

            const auto flLedChannels = beginReadArray(qs, Key::flIlluminationMap);
            for(auto ledIndex=0; ledIndex<flLedChannels; ledIndex++) {
                qs.setArrayIndex(ledIndex);
                const auto red = readValue(qs, Key::channelRed).toInt();
                const auto green = readValue(qs, Key::channelGreen).toInt();
                const auto blue = readValue(qs, Key::channelBlue).toInt();
                const auto ledChannel = readValue(qs, Key::ledChannel).toInt();
                model.SetFLIlluminationMap(ledChannel, {red, green, blue});
            }
            qs.endArray();

            const auto htIllumSetupPatterns = beginReadArray(qs, Key::htIllumSetupPatterns);
            for(auto ptnIndex=0; ptnIndex<htIllumSetupPatterns; ptnIndex++) {
                qs.setArrayIndex(ptnIndex);
                const auto NA = readValue(qs, Key::htIllumSetupPatternsNA).toDouble();
                const auto patternIndex = readValue(qs, Key::htIllumSetupPatternsPatternIndex).toInt();
                model.SetHTIlluminationSetupPattern(NA, patternIndex);
            }
            qs.endArray();

            using SetupRef = AppEntity::SetupRef;

            model.SetSetupReference(SetupRef::EncoderCalTargetDistanceX,
                                    readValue(qs, Key::setupRefEncoderCalDistX).toDouble());
            model.SetSetupReference(SetupRef::EncoderCalTargetDistanceY,
                                    readValue(qs, Key::setupRefEncoderCalDistY).toDouble());

            using EvaluationRef = AppEntity::EvaluationRef;

            model.SetEvaluationReference(EvaluationRef::IntensityUniformityCV,
                                         readValue(qs, Key::evalRefIntensityUniformityCV).toDouble());

            const auto evalRefCAFCount = beginReadArray(qs, Key::evalRefCAF);
            for(auto idx=0; idx<evalRefCAFCount; idx++) {
                qs.setArrayIndex(idx);
                auto NA = readValue(qs, Key::evalRefCAFNA).toDouble();
                model.SetEvaluationReference(EvaluationRef::CAFScore, NA, 
                                             readValue(qs, Key::evalRefCAFScore).toDouble());
                model.SetEvaluationReference(EvaluationRef::CAFBrightness, NA,
                                             readValue(qs, Key::evalRefCAFBrightness).toDouble());
            }
            qs.endArray();

            model.SetEvaluationReference(EvaluationRef::AFPositionBelow,
                                         readValue(qs, Key::evalRefAFPositionBelow).toDouble());
            model.SetEvaluationReference(EvaluationRef::AFPositionAbove,
                                         readValue(qs, Key::evalRefAFPositionAbove).toDouble());
            model.SetEvaluationReference(EvaluationRef::AFDistanceGap,
                                         readValue(qs, Key::evalRefAFDistanceGap).toDouble());
            model.SetEvaluationReference(EvaluationRef::BeadScoreThreshold,
                                         readValue(qs, Key::evalRefBeadScoreThreshold).toDouble());
            model.SetEvaluationReference(EvaluationRef::BeadRepeatabilityCV,
                                         readValue(qs, Key::evalRefBeadRepeatabilityCV).toDouble());
            model.SetEvaluationReference(EvaluationRef::BeadRepeatabilityCount,
                                         readValue(qs, Key::evalRefBeadRepeatabilityCount).toDouble());
            model.SetEvaluationReference(EvaluationRef::BeadReproducibilityCV,
                                         readValue(qs, Key::evalRefBeadReproducibilityCV).toDouble());
            model.SetEvaluationReference(EvaluationRef::BeadReproducibilityMatrix,
                                         readValue(qs, Key::evalRefBeadReproducibilityMatrix).toDouble());
            model.SetEvaluationReference(EvaluationRef::BeadReproducibilityGapX,
                                         readValue(qs, Key::evalRefBeadReproducibilityGapX).toDouble());
            model.SetEvaluationReference(EvaluationRef::BeadReproducibilityGapY,
                                         readValue(qs, Key::evalRefBeadReproducibilityGapY).toDouble());

            using PreviewParam = AppEntity::PreviewParam;
            model.SetPreviewParameter(PreviewParam::StartEndMarginPulse,
                                      readValue(qs, Key::previewStartEndMarginPulse));
            model.SetPreviewParameter(PreviewParam::ForwardDelayCompensationPulse,
                                      readValue(qs, Key::previewFwdDelayCompensationPulse));
            model.SetPreviewParameter(PreviewParam::BackwardDelayCompensationPulse,
                                      readValue(qs, Key::previewBwdDelayCompensationPulse));
            model.SetPreviewParameter(PreviewParam::ExposureUSec,
                                      readValue(qs, Key::previewExposureUSec));
            model.SetPreviewParameter(PreviewParam::ReadoutUSec,
                                      readValue(qs, Key::previewReadoutUSec));
            model.SetPreviewParameter(PreviewParam::IntensityScaleFactor,
                                      readValue(qs, Key::previewIntensityScaleFactor));
            model.SetPreviewParameter(PreviewParam::TriggerPulseWidth,
                                      readValue(qs, Key::previewTriggerPulseWidth));
            model.SetPreviewParameter(PreviewParam::AccelerationFactor,
                                      readValue(qs, Key::previewAccelerationFactor));
            model.SetPreviewParameter(PreviewParam::LedChannel,
                                      readValue(qs, Key::previewLedChannel));
            model.SetPreviewParameter(PreviewParam::FilterChannel,
                                      readValue(qs, Key::previewFilterChannel));
            model.SetPreviewParameter(PreviewParam::OverlapMM,
                                      readValue(qs, Key::previewOverlapMM));
            model.SetPreviewParameter(PreviewParam::SingleMoveMM,
                                      readValue(qs, Key::previewSingleMoveMM));
        } catch(std::exception& ex) {
            d->SetError(QString("%1 - %2").arg(ex.what()).arg(path));
            return false;
        }

        return true;
    }

    auto Reader::GetError() const -> QString {
        return d->error;
    }
}
