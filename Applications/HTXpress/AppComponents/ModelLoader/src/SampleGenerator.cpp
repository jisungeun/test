#include <QFile>
#include <QSettings>

#include <AppEntityDefines.h>

#include "Fields.h"
#include "SampleGenerator.h"

namespace HTXpress::AppComponents::ModelLoader {
    auto SampleGenerator::Save(const QString& path) -> void {
        QFile::remove(path);

        QSettings qs(path, QSettings::IniFormat);

        qs.setValue(Key::ObjectiveLensMagnification, 20.0);
        qs.setValue(Key::ObjectiveLensNA, 0.9);
        qs.setValue(Key::CondenserLensMagnification, 20.0);
        qs.setValue(Key::CondenserLensNA, 0.9);
        qs.setValue(Key::CameraPixelSize, 4.5);
        qs.setValue(Key::CameraPixelsH, 1024);
        qs.setValue(Key::CameraPixelsV, 1024);
        qs.setValue(Key::CameraFOVStepH, 1);
        qs.setValue(Key::CameraFOVStepV, 1);

        qs.setValue(Key::ImagingMinInterval, 8000);
        qs.setValue(Key::ImagingMaxInterval, 30000000);
        qs.setValue(Key::ImagingMinIdle, 1000);
        qs.setValue(Key::ImagingMinExposure, 3000);
        qs.setValue(Key::ImagingMaxExposure, 20000);
        qs.setValue(Key::ImagingHTFilterChannel, 0);
        qs.setValue(Key::ImagingHTLightChannel, 2);
        qs.setValue(Key::ImagingBFFilterChannel, 0);
        qs.setValue(Key::ImagingBFLightChannel, 2);
        qs.setValue(Key::ImagingFLMaximumSlices, 60);
        qs.setValue(Key::ImagingCAFLightChannel, 2);

        qs.setValue(Key::ImagingSafeRangeX, 124.0);
        qs.setValue(Key::ImagingSafeRangeY, 84.0);

        auto index = 0;
        qs.beginWriteArray("Axis");
        for(auto axis : AppEntity::Axis::_values()) {
            qs.setArrayIndex(index++);

            qs.setValue(Key::AxisName, axis._to_string());
            qs.setValue(Key::AxisResolution, 1000);
            qs.setValue(Key::AxisLowerLimit, 0.0);
            qs.setValue(Key::AxisUpperLimit, 100.0);
            qs.setValue(Key::AxisMotionTime, 1.0);
        }
        qs.endArray();

        qs.setValue(Key::PositionsSafeZ, 0);
        qs.setValue(Key::PositionsLoadingX, 0);
        qs.setValue(Key::PositionsLoadingY, 0);
        qs.setValue(Key::PositionsEmissionFilters, "800,1850,2950,4000,5100,6150");
        qs.setValue(Key::PositionsDripTrayX, 0);
        qs.setValue(Key::PositionsDripTrayY, 0);
        qs.setValue(Key::PositionsDripTrayZ, 0);
        qs.setValue(Key::PositionsDripTrayC, 0);
        qs.setValue(Key::PositionsParkingX, 0);
        qs.setValue(Key::PositionsParkingY, 0);
        qs.setValue(Key::PositionsParkingZ, 0);
        qs.setValue(Key::PositionsParkingC, 0);

        qs.setValue(Key::timeEstimationFilterChangeDefault, 0.1);
        qs.setValue(Key::timeEstimationFilterChangePerStep, 0.1);
        qs.setValue(Key::timeEstimationSampleStageDefault, 0.1);
        qs.setValue(Key::timeEstimationSampleStagePerMM, 2.0);
        qs.setValue(Key::timeEstimationZScanDefault, 0.1);
        qs.setValue(Key::timeEstimationZScanPerMM, 2.0);
        qs.setValue(Key::timeEstimationTWMAccelTime, 0.1);
        qs.setValue(Key::timeEstimationTWMDecelTime, 0.1);
        qs.setValue(Key::timeEstimationTWMMarginTime, 0.1);
        qs.setValue(Key::timeEstimationFocusToScanReady, 0.1);
        qs.setValue(Key::timeEstimationBFImagingMargin, 1.0);

        qs.setValue(Key::htTriggerStartOffset, 150);
        qs.setValue(Key::htTriggerEndOffset, 100);
        qs.setValue(Key::htTriggerPulseWidth, 100);
        qs.setValue(Key::htTriggerReadOut, 8000);
        qs.setValue(Key::htTriggerExposure, 3000);
        qs.setValue(Key::htTriggerIntervalMargin, 3000);
        qs.setValue(Key::htTriggerAccelFactor, 10);

        qs.setValue(Key::condenserAFStartPos, 0);
        qs.setValue(Key::condenserAFTriggerInterval, 320);
        qs.setValue(Key::condenserAFTriggerSlices, 70);
        qs.setValue(Key::condenserAFReadout, 8000);
        qs.setValue(Key::condenserAFExposure, 3000);
        qs.setValue(Key::condenserAFIntervalMargin, 3000);
        qs.setValue(Key::condenserAFPulseWidth, 100);

        qs.setValue(Key::previewStartEndMarginPulse, 15);
        qs.setValue(Key::previewFwdDelayCompensationPulse, 13);
        qs.setValue(Key::previewBwdDelayCompensationPulse, -13);
        qs.setValue(Key::previewExposureUSec, 250);
        qs.setValue(Key::previewReadoutUSec, 8000);
        qs.setValue(Key::previewIntensityScaleFactor, 8.6);
        qs.setValue(Key::previewTriggerPulseWidth, 100);
        qs.setValue(Key::previewAccelerationFactor, 0.1);
        qs.setValue(Key::previewLedChannel, 2);
        qs.setValue(Key::previewFilterChannel, 0);
        qs.setValue(Key::previewOverlapMM, 0.010);
        qs.setValue(Key::previewSingleMoveMM, 1.0);
        qs.setValue(Key::previewGainCoeffA, 14.377);
        qs.setValue(Key::previewGainCoeffB, 0.1152);
        qs.setValue(Key::previewGainCoeffP, 12.735);
        qs.setValue(Key::previewGainCoeffQ, -2.5133);

        qs.setValue(Key::multiDishHolderThickness, 2.0);

        qs.beginWriteArray(Key::illumPatterns);
        for(auto idx=0; idx<3; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(Key::imagingType, AppEntity::ImagingType::_from_integral(idx%3)._to_string());
            qs.setValue(Key::triggerType, AppEntity::TriggerType::_from_integral(idx%2)._to_string());
            qs.setValue(Key::channel, idx%3);
            qs.setValue(Key::patternIndex, idx%3);
        }
        qs.endArray();

        qs.beginWriteArray(Key::flIlluminationMap);
        for(auto idx=0; idx<3; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(Key::channelRed, 3);
            qs.setValue(Key::channelGreen, 2);
            qs.setValue(Key::channelBlue, 1);
            qs.setValue(Key::ledChannel, idx);
        }
        qs.endArray();

        qs.beginWriteArray(Key::htIllumSetupPatterns);
        for(auto idx=0; idx<3; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(Key::htIllumSetupPatternsNA, 0.2*(idx+1));
            qs.setValue(Key::htIllumSetupPatternsPatternIndex, 23+idx);
        }
        qs.endArray();

        qs.setValue(Key::evalRefIntensityUniformityCV, 0.4);

        qs.beginWriteArray(Key::evalRefCAF);
        for(auto idx=0; idx<4; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(Key::evalRefCAFNA, 0.2*(idx+1));
            qs.setValue(Key::evalRefCAFScore, 0.5);
            qs.setValue(Key::evalRefCAFBrightness, 120);
        }
        qs.endArray();

        qs.setValue(Key::setupRefEncoderCalDistX, 90.0);
        qs.setValue(Key::setupRefEncoderCalDistY, 50.0);

        qs.setValue(Key::evalRefAFPositionBelow, -0.500);
        qs.setValue(Key::evalRefAFPositionAbove, 0.100);
        qs.setValue(Key::evalRefAFDistanceGap, 1.0);
        qs.setValue(Key::evalRefBeadScoreThreshold, 0.9);
        qs.setValue(Key::evalRefBeadRepeatabilityCV, 0.1);
        qs.setValue(Key::evalRefBeadRepeatabilityCount, 5);
        qs.setValue(Key::evalRefBeadReproducibilityCV, 0.1);
        qs.setValue(Key::evalRefBeadReproducibilityMatrix, 3);
        qs.setValue(Key::evalRefBeadReproducibilityGapX, 50);
        qs.setValue(Key::evalRefBeadReproducibilityGapY, 50);
    }
}
