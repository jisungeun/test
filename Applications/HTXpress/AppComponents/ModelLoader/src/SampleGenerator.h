#pragma once
#include <memory>
#include <QString>

namespace HTXpress::AppComponents::ModelLoader {
    class SampleGenerator {
    public:
        static auto Save(const QString& path)->void;
    };
}