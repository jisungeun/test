#pragma once
#include <memory>
#include <QString>

#include "ModelInstance.h"

namespace HTXpress::AppComponents::ModelLoader {
    class Reader {
    public:
        Reader();
        ~Reader();
        
        auto Read(const QString& path, ModelInstance& model)->bool;
        auto GetError() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}