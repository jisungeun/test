#define LOGGER_TAG "[Model Loader]"

#include <QDir>

#include <TCLogger.h>
#include <ModelRepo.h>

#include "ModelInstance.h"
#include "SampleGenerator.h"
#include "Reader.h"
#include "ModelLoader.h"

namespace HTXpress::AppComponents::ModelLoader {
    struct Loader::Impl {
        QStringList errors;

        auto SetError(const QString& message)->void;
    };

    auto Loader::Impl::SetError(const QString& message) -> void {
        errors.push_back(message);
        QLOG_ERROR() << message;
    }

    Loader::Loader() :d {new Impl} {
    }

    Loader::~Loader() {
        
    }

    auto Loader::Load(const QString& path) -> bool {
        bool success = true;

        auto repo = AppEntity::ModelRepo::GetInstance();

        QDir dir(path);
        auto files = dir.entryList({"*.model"}, QDir::Filter::Files, QDir::SortFlag::Name);

        for(auto file : files) {
            const auto modelPath = QString("%1/%2").arg(path).arg(file);
            auto model = std::make_shared<ModelInstance>(file.remove(".model"));

            Reader reader;
            if(!reader.Read(modelPath, *model)) {
                d->SetError(reader.GetError());
                success = false;
                continue;
            }

            repo->AddModel(model);
        }

        if(repo->GetModels().isEmpty()) {
            d->SetError(QString("No valid model files exist at %1, so it creates a sample model file.").arg(path));
            SampleGenerator::Save(QString("%1/Model.model.sample").arg(path));
            success = false;
        }

        return success;
    }

    auto Loader::GetError() const -> QString {
        return d->errors.join("\n");
    }
}