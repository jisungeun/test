#pragma once
#include <memory>

namespace HTXpress::AppComponents::ModelLoader {
    namespace Key {
        constexpr char ObjectiveLensMagnification[] = "ObjectiveLens/Magnification";
        constexpr char ObjectiveLensNA[] = "ObjectiveLens/NA";
        constexpr char CondenserLensMagnification[] = "CondenserLens/Magnification";
        constexpr char CondenserLensNA[] = "CondenserLens/NA";
        constexpr char CameraPixelSize[] = "Camera/PixelSize";
        constexpr char CameraPixelsH[] = "Camera/Pixels/H";
        constexpr char CameraPixelsV[] = "Camera/Pixels/V";
        constexpr char CameraFOVStepH[] = "Camera/FOVStep/H";
        constexpr char CameraFOVStepV[] = "Camera/FOVStep/V";

        constexpr char ImagingMinInterval[] = "Imaging/MinimumInterval";
        constexpr char ImagingMaxInterval[] = "Imaging/MaximumInterval";
        constexpr char ImagingMinIdle[] = "Imaging/MinimumIdle";
        constexpr char ImagingMinExposure[] = "Imaging/MinimumExposure";
        constexpr char ImagingMaxExposure[] = "Imaging/MaximumExposure";
        constexpr char ImagingHTLightChannel[] = "Imaging/HT/LightChannel";
        constexpr char ImagingHTFilterChannel[] = "Imaging/HT/FilterChannel";
        constexpr char ImagingBFLightChannel[] = "Imaging/BF/LightChannel";
        constexpr char ImagingBFFilterChannel[] = "Imaging/BF/FilterChannel";
        constexpr char ImagingBFLightIntensity[] = "Imaging/BF/LightIntensity";
        constexpr char ImagingFLMaximumSlices[] = "Imaging/FL/MaximumSlices";
        constexpr char ImagingCAFLightChannel[] = "Imaging/CondenserAF/LightChannel";
        constexpr char ImagingSafeRangeX[] = "Imaging/SafeRange/X";
        constexpr char ImagingSafeRangeY[] = "Imaging/SafeRange/Y";

        constexpr char AxisName[] = "Name";
        constexpr char AxisResolution[] = "Resolution";
        constexpr char AxisLowerLimit[] = "LowerLimit";
        constexpr char AxisUpperLimit[] = "UpperLimit";
        constexpr char AxisMotionTime[] = "SecondsPerMM";

        constexpr char PositionsSafeZ[] = "Positions/SafeZ";
        constexpr char PositionsLoadingX[] = "Positions/Loading/X";
        constexpr char PositionsLoadingY[] = "Positions/Loading/Y";
        constexpr char PositionsEmissionFilters[] = "Positions/EmissionFilters";
        constexpr char PositionsDripTrayX[] = "Positions/DripTray/X";
        constexpr char PositionsDripTrayY[] = "Positions/DripTray/Y";
        constexpr char PositionsDripTrayZ[] = "Positions/DripTray/Z";
        constexpr char PositionsDripTrayC[] = "Positions/DripTray/C";
        constexpr char PositionsParkingX[] = "Positions/Parking/X";
        constexpr char PositionsParkingY[] = "Positions/Parking/Y";
        constexpr char PositionsParkingZ[] = "Positions/Parking/Z";
        constexpr char PositionsParkingC[] = "Positions/Parking/C";

        constexpr char timeEstimationFilterChangeDefault[] = "TimeEstimation/FilterChangeTime/Default";
        constexpr char timeEstimationFilterChangePerStep[] = "TimeEstimation/FilterChangeTime/PerStep";
        constexpr char timeEstimationSampleStageDefault[] = "TimeEstimation/SampleStageMotionTime/Default";
        constexpr char timeEstimationSampleStagePerMM[] = "TimeEstimation/SampleStageMotionTime/PerMM";
        constexpr char timeEstimationZScanDefault[] = "TimeEstimation/ZScanMotionTime/Default";
        constexpr char timeEstimationZScanPerMM[] = "TimeEstimation/ZScanMotionTime/PerMM";
        constexpr char timeEstimationTWMAccelTime[] = "TimeEstimation/TriggerWithMotionTime/Acceleration";
        constexpr char timeEstimationTWMDecelTime[] = "TimeEstimation/TriggerWithMotionTime/Deceleration";
        constexpr char timeEstimationTWMMarginTime[] = "TimeEstimation/TriggerWithMotionTime/Margin";
        constexpr char timeEstimationFocusToScanReady[] = "TimeEstimation/Distance/FocusToScanReady";
        constexpr char timeEstimationBFImagingMargin[] = "TimeEstimation/BF/ImagingTimeMargin";

        constexpr char htTriggerStartOffset[] = "HTTrigger/StartOffsetPulse";
        constexpr char htTriggerEndOffset[] = "HTTrigger/EndOffsetPulse";
        constexpr char htTriggerPulseWidth[] = "HTTrigger/PulseWidthUSec";
        constexpr char htTriggerReadOut[] = "HTTrigger/ReadoutUSec";
        constexpr char htTriggerExposure[] = "HTTrigger/ExposureUSec";
        constexpr char htTriggerIntervalMargin[] = "HTTrigger/IntervalMarginUSec";
        constexpr char htTriggerAccelFactor[] = "HTTrigger/AccelerationFactor";

        constexpr char condenserAFStartPos[] = "CondenserAFScan/TriggerStartPosPulse";
        constexpr char condenserAFTriggerInterval[] = "CondenserAFScan/TriggerIntervalPulse";
        constexpr char condenserAFTriggerSlices[] = "CondenserAFScan/TriggerSlices";
        constexpr char condenserAFReadout[] = "CondenserAFScan/ReadoutUSec";
        constexpr char condenserAFExposure[] = "CondenserAFScan/ExposureUSec";
        constexpr char condenserAFIntervalMargin[] = "CondenserAFScan/IntervalMarginUSec";
        constexpr char condenserAFPulseWidth[] = "CondenserAFScan/PulseWidthUSec";

        constexpr char multiDishHolderThickness[] = "MultiDishHolder/ThicknessMM";

        constexpr char illumPatterns[] = "Illumination Patterns";
        constexpr char imagingType[] = "ImagingType";
        constexpr char triggerType[] = "TriggerType";
        constexpr char channel[] = "Channel";
        constexpr char patternIndex[] = "PatternIndex";

        constexpr char flIlluminationMap[] = "FL Illumination Map";
        constexpr char channelRed[] = "Channel_R";
        constexpr char channelGreen[] = "Channel_G";
        constexpr char channelBlue[] = "Channel_B";
        constexpr char ledChannel[] = "LED_Channel";

        constexpr char htIllumSetupPatterns[] = "HT Illumination Intensity Setup Patterns";
        constexpr char htIllumSetupPatternsNA[] = "NA";
        constexpr char htIllumSetupPatternsPatternIndex[] = "PatternIndex";

        constexpr char setupRefEncoderCalDistX[] = "SetupReference/EncoderCalibration/XDistMm";
        constexpr char setupRefEncoderCalDistY[] = "SetupReference/EncoderCalibration/YDistMm";

        constexpr char evalRefIntensityUniformityCV[] = "EvaluationReference/IntensityUniformityCV";
        constexpr char evalRefCAF[] = "EvaluationReference/CAF";
        constexpr char evalRefCAFNA[] = "NA";
        constexpr char evalRefCAFScore[] = "Score";
        constexpr char evalRefCAFBrightness[] = "Brightness";
        constexpr char evalRefAFPositionBelow[] = "EvaluationReference/AFRelativeBelowMM";
        constexpr char evalRefAFPositionAbove[] = "EvaluationReference/AFRelativeAboveMM";
        constexpr char evalRefAFDistanceGap[] = "EvaluationReference/AFDistanceGapUM";
        constexpr char evalRefBeadScoreThreshold[] = "EvaluationReference/BeadScoreThreshold";
        constexpr char evalRefBeadRepeatabilityCV[] = "EvaluationReference/BeadRepeatabilityCV";
        constexpr char evalRefBeadRepeatabilityCount[] = "EvaluationReference/BeadRepeatabilityCount";
        constexpr char evalRefBeadReproducibilityCV[] = "EvaluationReference/BeadReproducibilityCV";
        constexpr char evalRefBeadReproducibilityMatrix[] = "EvaluationReference/BeadReproducibilityMatrix";
        constexpr char evalRefBeadReproducibilityGapX[] = "EvaluationReference/BeadReproducibilityGapXUM";
        constexpr char evalRefBeadReproducibilityGapY[] = "EvaluationReference/BeadReproducibilityGapYUM";

        constexpr char previewStartEndMarginPulse[] = "PreviewParameter/StartEndMarginPulse";
        constexpr char previewFwdDelayCompensationPulse[] = "PreviewParameter/ForwardDelayCompensationPulse";
        constexpr char previewBwdDelayCompensationPulse[] = "PreviewParameter/BackwardDelayCompensationPulse";
        constexpr char previewExposureUSec[] = "PreviewParameter/ExposureUSec";
        constexpr char previewReadoutUSec[] = "PreviewParameter/ReadoutUSec";
        constexpr char previewIntensityScaleFactor[] = "PreviewParameter/IntensityScaleFactor";
        constexpr char previewTriggerPulseWidth[] = "PreviewParameter/TriggerPulseWidth";
        constexpr char previewAccelerationFactor[] = "PreviewParameter/AccelerationFactor";
        constexpr char previewLedChannel[] = "PreviewParameter/LedChannel";
        constexpr char previewFilterChannel[] = "PreviewParameter/FilterChannel";
        constexpr char previewOverlapMM[] = "PreviewParameter/OverlapMM";
        constexpr char previewSingleMoveMM[] = "PreviewParameter/SingleMoveMM";
        constexpr char previewGainCoeffA[] = "PreviewParameter/GainCoefficientA";
        constexpr char previewGainCoeffB[] = "PreviewParameter/GainCoefficientB";
        constexpr char previewGainCoeffP[] = "PreviewParameter/GainCoefficientP";
        constexpr char previewGainCoeffQ[] = "PreviewParameter/GainCoefficientQ";
    }
}
