#include <QMap>
#include <QDebug>

#include "ModelInstance.h"

namespace HTXpress::AppComponents::ModelLoader {
    struct ModelInstance::Impl {
        struct {
            double magnification{ 1.0 };
            double NA{ 0.9 };
        } objective;
        
        struct {
            double magnification{ 1.0 };
            double NA{ 0.9 };
        } condenser;
        
        struct {
            double pixelSize{ 4.5 };
            uint32_t pixelsH{ 1024 };
            uint32_t pixelsV{ 1024 };
            uint32_t fovStepH{ 1 };
            uint32_t fovStepV{ 1 };
        } camera;

        struct {
            int32_t minInterval{ 8000 };
            int32_t maxInterval{ 30000000 };
            int32_t minIdle{ 1000 };
            int32_t minExposure{ 3000 };
            int32_t maxExposure{ 20000 };

            struct {
                int32_t lightChannel{ 2 };
                int32_t filterChannel{ 0 };
            } ht;

            struct {
                int32_t lightChannel{ 2 };
                int32_t filterChannel{ 0 };
            } bf;

            struct {
                int32_t maximumSlices{ 60 };
            } fl;

            struct {
                int32_t lightChannel{ 2 };
            } condenserAF;

            struct {
                double x{ 0 };
                double y{ 0 };
            } safeRange;
        } imaging;
        
        struct AxisInfo {
            uint32_t pulsesPerMM{ 1000 };       //pulses per mm
            double lowerLimit{ -10.0 };
            double upperLimit{ 10.0 };
            double secsPerMM{ 1.0 };            //seconds per mm
        };
        QMap<Axis, AxisInfo> axisInfos;

        struct {
            double safeZ{ 0 };
            double loadingX{ 0 };
            double loadingY{ 0 };
            QList<int32_t> emissionFilters;
            double dripTrayX{ 0 };
            double dripTrayY{ 0 };
            double dripTrayZ{ 0 };
            double dripTrayC{ 0 };
            double parkingX{ 0 };
            double parkingY{ 0 };
            double parkingZ{ 0 };
            double parkingC{ 0 };
        } positions;

        struct {
            struct {
                double default{ 0.1 };
                double perStep{ 0.1 };
            } filterChange;
            struct {
                double default{ 0.1 };
                double perMM{ 2.0 };
            } sampleStageMotion;
            struct {
                double default{ 0.1 };
                double perMM{ 2.0 };
            } zScanMotion;
            struct {
                double acceleration{ 0.1 };
                double deceleration{ 0.1 };
                double margin{ 0.1 };
            } triggerWithMotion;
            struct {
                double focusToScanReady{ 0.1 };
            } distance;
            struct {
                double marginTime{ 1.0 };
            } bf;
        } timeEstimation;

        struct {
            int32_t startOffset{ 0 };
            int32_t endOffset{ 0 };
            int32_t pulseWidth{ 0 };
            int32_t readOut{ 0 };
            int32_t exposure{ 0 };
            int32_t intervalMargin{ 0 };
            int32_t accelerationFactor{ 1 };
        } htTrigger;

        struct {
            int32_t startPos{ 0 };
            int32_t triggerInterval{ 0 };
            int32_t slices{ 0 };
            int32_t readOut{ 0 };
            int32_t exposure{ 0 };
            int32_t intervalMargin{ 0 };
            int32_t pulseWidth{ 0 };
        } condenserAFScan;

        struct {
            double thickness{ 2.0 };
        } multiDishHolder;

        struct {
            QMap<PatternIndex, int32_t> patterns;
        } illumination;

        struct {
            QMap<IlluminationMapIndex, int32_t> map;
            QMap<int32_t, IlluminationMapIndex> mapReversed;
        } flIllumination;

        QMap<double, int32_t> htIllumSetupPattern;

        QMap<AppEntity::SetupRef, double> setupReferences;
        QMap<AppEntity::EvaluationRef, double> evalReferences;
        QMap<QPair<AppEntity::EvaluationRef, double>, double> evalReferencesNA;

        QMap<AppEntity::PreviewParam, QVariant> previewParameters;
    };
    
    ModelInstance::ModelInstance(const QString& name) : AppEntity::Model(name), d{new Impl} {
    }
    
    ModelInstance::~ModelInstance() {
    }
    
    auto ModelInstance::ObjectiveLensMagnification() const -> double {
        return d->objective.magnification;
    }
    
    auto ModelInstance::ObjectiveLensNA() const -> double {
        return d->objective.NA;
    }
    
    auto ModelInstance::CondenserLensMagnification() const -> double {
        return d->condenser.magnification;
    }
    
    auto ModelInstance::CondenserLensNA() const -> double {
        return d->condenser.NA;
    }
    
    auto ModelInstance::CameraPixelSize() const -> double {
        return d->camera.pixelSize;
    }
    
    auto ModelInstance::CameraPixelsH() const -> uint32_t {
        return d->camera.pixelsH;
    }
    
    auto ModelInstance::CameraPixelsV() const -> uint32_t {
        return d->camera.pixelsV;
    }

    auto ModelInstance::CameraFOVStepH() const -> uint32_t {
        return d->camera.fovStepH;
    }

    auto ModelInstance::CameraFOVStepV() const -> uint32_t {
        return d->camera.fovStepV;
    }

    auto ModelInstance::MinimumIntervalUSec() const -> int32_t {
        return d->imaging.minInterval;
    }

    auto ModelInstance::MaximumIntervalUSec() const -> int32_t {
        return d->imaging.maxInterval;
    }

    auto ModelInstance::MinimumIdleUSec() const -> int32_t {
        return d->imaging.minIdle;
    }

    auto ModelInstance::MinimumExposureUSec() const -> int32_t {
        return d->imaging.minExposure;
    }

    auto ModelInstance::MaximumExposureUSec() const -> int32_t {
        return d->imaging.maxExposure;
    }

    auto ModelInstance::HTLightChannel() const -> int32_t {
        return d->imaging.ht.lightChannel;
    }

    auto ModelInstance::HTFilterChannel() const -> int32_t {
        return d->imaging.ht.filterChannel;
    }

    auto ModelInstance::BFLightChannel() const -> int32_t {
        return d->imaging.bf.lightChannel;
    }

    auto ModelInstance::BFFilterChannel() const -> int32_t {
        return d->imaging.bf.filterChannel;
    }

    auto ModelInstance::FLMaximumSlices() const -> int32_t {
        return d->imaging.fl.maximumSlices;
    }

    auto ModelInstance::CondenserAFLightChannel() const -> int32_t {
        return d->imaging.condenserAF.lightChannel;
    }

    auto ModelInstance::SafeXYRangeMM() const -> std::tuple<double, double> {
        return std::make_tuple(d->imaging.safeRange.x, d->imaging.safeRange.y);
    }

    auto ModelInstance::AxisResolutionPPM(Axis axis) const -> uint32_t {
        return d->axisInfos[axis].pulsesPerMM;
    }
    
    auto ModelInstance::AxisLowerLimitMM(Axis axis) const -> double {
        return d->axisInfos[axis].lowerLimit;
    }
    
    auto ModelInstance::AxisUpperLimitMM(Axis axis) const -> double {
        return d->axisInfos[axis].upperLimit;
    }

    auto ModelInstance::AxisMotionTimeSPM(Axis axis) const -> double {
        return d->axisInfos[axis].secsPerMM;
    }

    auto ModelInstance::SafeZPositionMM() const -> double {
        return d->positions.safeZ;
    }

    auto ModelInstance::LoadingXPositionMM() const -> double {
        return d->positions.loadingX;
    }

    auto ModelInstance::LoadingYPositionMM() const -> double {
        return d->positions.loadingY;
    }

    auto ModelInstance::EmissionFilterPules() const -> QList<int32_t> {
        return d->positions.emissionFilters;
    }

    auto ModelInstance::DripTrayRemovingXPositionMM() const -> double {
        return d->positions.dripTrayX;
    }

    auto ModelInstance::DripTrayRemovingYPositionMM() const -> double {
        return d->positions.dripTrayY;
    }

    auto ModelInstance::DripTrayRemovingZPositionMM() const -> double {
        return d->positions.dripTrayZ;
    }

    auto ModelInstance::DripTrayRemovingCPositionMM() const -> double {
        return d->positions.dripTrayC;
    }

    auto ModelInstance::ParkingXPositionMM() const -> double {
        return d->positions.parkingX;
    }

    auto ModelInstance::ParkingYPositionMM() const -> double {
        return d->positions.parkingY;
    }

    auto ModelInstance::ParkingZPositionMM() const -> double {
        return d->positions.parkingZ;
    }

    auto ModelInstance::ParkingCPositionMM() const -> double {
        return d->positions.parkingC;
    }

    auto ModelInstance::FilterChangeTime() const -> std::tuple<double, double> {
        return std::make_tuple(d->timeEstimation.filterChange.default, d->timeEstimation.filterChange.perStep);
    }

    auto ModelInstance::SampleStageMotionTime() const -> std::tuple<double, double> {
        return std::make_tuple(d->timeEstimation.sampleStageMotion.default, d->timeEstimation.sampleStageMotion.perMM);
    }

    auto ModelInstance::ZScanMotionTime() const -> std::tuple<double, double> {
        return std::make_tuple(d->timeEstimation.zScanMotion.default, d->timeEstimation.zScanMotion.perMM);
    }

    auto ModelInstance::TriggerWithMotionAccelerationTime() const -> double {
        return d->timeEstimation.triggerWithMotion.acceleration;
    }

    auto ModelInstance::TriggerWithMotionDecelerationTime() const -> double {
        return d->timeEstimation.triggerWithMotion.deceleration;
    }

    auto ModelInstance::TriggerWithMotionMarginTime() const -> double {
        return d->timeEstimation.triggerWithMotion.margin;
    }

    auto ModelInstance::FocusToScanReadyMM() const -> double {
        return d->timeEstimation.distance.focusToScanReady;
    }

    auto ModelInstance::BFImagingMarginTime() const -> double {
        return d->timeEstimation.bf.marginTime;
    }

    auto ModelInstance::HTTriggerStartOffsetPulse() const -> int32_t {
        return d->htTrigger.startOffset;
    }

    auto ModelInstance::HTTriggerEndOffsetPulse() const -> int32_t {
        return d->htTrigger.endOffset;
    }

    auto ModelInstance::HTTriggerPulseWidthUSec() const -> int32_t {
        return d->htTrigger.pulseWidth;
    }

    auto ModelInstance::HTTriggerReadOutUSec() const -> int32_t {
        return d->htTrigger.readOut;
    }

    auto ModelInstance::HTTriggerExposureUSec() const -> int32_t {
        return d->htTrigger.exposure;
    }

    auto ModelInstance::HTTriggerIntervalMarginUSec() const -> int32_t {
        return d->htTrigger.intervalMargin;
    }

    auto ModelInstance::HTTriggerAccelerationFactor() const -> int32_t {
        return d->htTrigger.accelerationFactor;
    }

    auto ModelInstance::CondenserAFScanStartPosPulse() const -> int32_t {
        return d->condenserAFScan.startPos;
    }

    auto ModelInstance::CondenserAFScanIntervalPulse() const -> int32_t {
        return d->condenserAFScan.triggerInterval;
    }

    auto ModelInstance::CondenserAFScanTriggerSlices() const -> int32_t {
        return d->condenserAFScan.slices;
    }

    auto ModelInstance::CondenserAFScanReadOutUSec() const -> int32_t {
        return d->condenserAFScan.readOut;
    }

    auto ModelInstance::CondenserAFScanExposureUSec() const -> int32_t {
        return d->condenserAFScan.exposure;
    }

    auto ModelInstance::CondenserAFScanIntervalMarginUSec() const -> int32_t {
        return d->condenserAFScan.intervalMargin;
    }

    auto ModelInstance::CondenserAFScanPulseWidthUSec() const -> int32_t {
        return d->condenserAFScan.pulseWidth;
    }

    auto ModelInstance::MultiDishHolderThickness() const -> double {
        return d->multiDishHolder.thickness;
    }

    auto ModelInstance::GetIlluminationPattern(ImagingType imagingType, TriggerType triggerType,
                                               int32_t channel) const -> int32_t {
        const auto itr = d->illumination.patterns.find({imagingType, triggerType, channel});
        if(itr == d->illumination.patterns.end()) return -1;
        return itr.value();
    }

    auto ModelInstance::GetIlluminationPattern(const PatternIndex& index) const -> int32_t {
        const auto itr = d->illumination.patterns.find(index);
        if(itr == d->illumination.patterns.end()) return -1;
        return itr.value();
    }

    auto ModelInstance::GetIlluminationPatterns() const -> QList<PatternIndex> {
        return d->illumination.patterns.keys();
    }

    auto ModelInstance::GetFLIlluminationMap(int32_t channel) const -> IlluminationMapIndex {
        return d->flIllumination.mapReversed[channel];
    }

    auto ModelInstance::GetFLIlluminationMapChannel(IlluminationMapIndex mapIndex) const -> int32_t {
        const auto itr = d->flIllumination.map.find(mapIndex);
        if(itr == d->flIllumination.map.end()) return 0;
        return itr.value();
    }

    auto ModelInstance::GetFLIlluminationMapChannel(const QList<int32_t>& flChannels) const -> std::tuple<int32_t, QList<int32_t>> {
        auto remains = [=]()->QList<int32_t> {
            QList<int32_t> channels{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            std::for_each(flChannels.begin(), flChannels.end(), [&](int32_t val) {
                channels.removeOne(val);});
            return channels;
        }();

        qDebug() << "flChannels=" << flChannels;
        qDebug() << "remains=" << remains;

        QList<int32_t> channels;
        for(int idx=0; idx<3; idx++) {
            const auto val = flChannels[idx];
            if(val == -1) {
                channels.push_back(remains.at(0));
                remains.pop_front();
            } else if(channels.contains(val)) {
                channels.push_back(remains.at(0));
                remains.pop_front();
            } else {
                channels.push_back(val);
            }
        }
        qDebug() << "channels=" << channels;

        std::sort(channels.begin(), channels.end(), std::greater<>());  //from the first, R/G/B

        IlluminationMapIndex index(channels);
        const auto itr = d->flIllumination.map.find(index);
        if(itr == d->flIllumination.map.end()) return std::make_tuple(-1, QList<int32_t>{});

        const auto ledChannel = itr.value();

        QList<int32_t> order;
        for(auto idx=0; idx<3; idx++) {
            order.push_back(channels.indexOf(flChannels.at(idx)));
        }

        return std::make_tuple(ledChannel, order);
    }

    auto ModelInstance::GetFLIlluminationMapChannel(int32_t flChannelIndex) const -> std::tuple<int32_t, int32_t> {
        for(auto mapIndex : d->flIllumination.map.keys()) {
            if(mapIndex.GetChannelRed() == flChannelIndex) return std::make_tuple(d->flIllumination.map[mapIndex], 0);
            if(mapIndex.GetChannelGreen() == flChannelIndex) return std::make_tuple(d->flIllumination.map[mapIndex], 1);
            if(mapIndex.GetChannelBlue() == flChannelIndex) return std::make_tuple(d->flIllumination.map[mapIndex], 2);
        }
        return std::make_tuple(0, 0);
    }

    auto ModelInstance::GetFLIlluminationMapChannels() const -> QList<int32_t> {
        return d->flIllumination.mapReversed.keys();
    }

    auto ModelInstance::GetHTIlluminationSetupPattern(double NA) const -> int32_t {
        auto itr = d->htIllumSetupPattern.find(NA);
        if(itr == d->htIllumSetupPattern.end()) return -1;
        return itr.value();
    }

    auto ModelInstance::GetHTIlluminationSetupPatternNAs() const -> QList<double> {
        return d->htIllumSetupPattern.keys();
    }

    auto ModelInstance::SetupReference(AppEntity::SetupRef item) const -> double {
        return d->setupReferences[item];
    }

    auto ModelInstance::EvaluationReference(AppEntity::EvaluationRef item) const -> double {
        return d->evalReferences[item];
    }

    auto ModelInstance::EvaluationReference(AppEntity::EvaluationRef item, double NA) const -> double {
        return d->evalReferencesNA[{item, NA}];
    }

    auto ModelInstance::PreviewParameter(AppEntity::PreviewParam item) const -> QVariant {
        return d->previewParameters[item];
    }

    auto ModelInstance::SetObjectiveMagnification(const double value) -> void {
        d->objective.magnification = value;
    }
    
    auto ModelInstance::SetObjectiveLensNA(const double value)->void {
        d->objective.NA = value;
    }
    
    auto ModelInstance::SetCondenserLensMagnification(const double value)->void {
        d->condenser.magnification = value;
    }
    
    auto ModelInstance::SetCondenserLensNA(const double value)->void {
        d->condenser.NA = value;
    }
    
    auto ModelInstance::SetCameraPixelSize(const double value)->void {
        d->camera.pixelSize = value;
    }
    
    auto ModelInstance::SetCameraPixels(uint32_t pixelsH, uint32_t pixelsV)->void {
        d->camera.pixelsH = pixelsH;
        d->camera.pixelsV = pixelsV;
    }

    auto ModelInstance::SetCameraFOVStep(uint32_t stepH, uint32_t stepV) -> void {
        d->camera.fovStepH = stepH;
        d->camera.fovStepV = stepV;
    }

    auto ModelInstance::SetMinimumIntervalUSec(int32_t value) -> void {
        d->imaging.minInterval = value;
    }

    auto ModelInstance::SetMaximumIntervalUSec(int32_t value) -> void {
        d->imaging.maxInterval = value;
    }

    auto ModelInstance::SetMinimumIdleUSec(int32_t value) -> void {
        d->imaging.minIdle = value;
    }

    auto ModelInstance::SetMinimumExposureUSec(int32_t value) -> void {
        d->imaging.minExposure = value;
    }

    auto ModelInstance::SetMaximumExposureUSec(int32_t value) -> void {
        d->imaging.maxExposure = value;
    }

    auto ModelInstance::SetHTLightChannel(int32_t value) -> void {
        d->imaging.ht.lightChannel = value;
    }

    auto ModelInstance::SetHTFilterChannel(int32_t value) -> void {
        d->imaging.ht.filterChannel = value;
    }

    auto ModelInstance::SetBFLightChannel(int32_t value) -> void {
        d->imaging.bf.lightChannel = value;
    }

    auto ModelInstance::SetBFFilterChannel(int32_t value) -> void {
        d->imaging.bf.filterChannel = value;
    }

    auto ModelInstance::SetFLMaximumSlices(int32_t value) -> void {
        d->imaging.fl.maximumSlices = value;
    }

    auto ModelInstance::SetCondenserAFLightChannel(int32_t value) -> void {
        d->imaging.condenserAF.lightChannel = value;
    }

    auto ModelInstance::SetSafeXYRangeMM(double xRange, double yRange) -> void {
        d->imaging.safeRange.x = xRange;
        d->imaging.safeRange.y = yRange;
    }

    auto ModelInstance::SetAxisResolutionPPM(const Axis axis, const uint32_t value)->void {
        d->axisInfos[axis].pulsesPerMM = value;
    }
    
    auto ModelInstance::SetAxisLowerLimitMM(const Axis axis, const double value)->void {
        d->axisInfos[axis].lowerLimit = value;
    }
    
    auto ModelInstance::SetAxisUpperLimitMM(const Axis axis, const double value)->void {
        d->axisInfos[axis].upperLimit = value;
    }

    auto ModelInstance::SetAxisMotionTimeSPM(const Axis axis, const double value) -> void {
        d->axisInfos[axis].secsPerMM = value;
    }

    auto ModelInstance::SetSafeZPositionMM(const double value) -> void {
        d->positions.safeZ = value;
    }

    auto ModelInstance::SetLoadingPositionMM(const double xValue, const double yValue) -> void {
        d->positions.loadingX = xValue;
        d->positions.loadingY = yValue;
    }

    auto ModelInstance::SetEmissionFilterPositions(const QList<int32_t>& positions) -> void {
        d->positions.emissionFilters = positions;
    }

    auto ModelInstance::SetDripTrayRemovingPositionMM(double xValue, double yValue, double zValue, double cValue) -> void {
        d->positions.dripTrayX = xValue;
        d->positions.dripTrayY = yValue;
        d->positions.dripTrayZ = zValue;
        d->positions.dripTrayC = cValue;
    }

    auto ModelInstance::SetParkingPositionMM(double xValue, double yValue, double zValue, double cValue) -> void {
        d->positions.parkingX = xValue;
        d->positions.parkingY = yValue;
        d->positions.parkingZ = zValue;
        d->positions.parkingC = cValue;
    }

    auto ModelInstance::SetFilterChangeTime(const double default, const double perStep) -> void {
        d->timeEstimation.filterChange.default = default;
        d->timeEstimation.filterChange.perStep = perStep;
    }

    auto ModelInstance::SetSampleStageMotionTime(const double default, const double perMM) -> void {
        d->timeEstimation.sampleStageMotion.default = default;
        d->timeEstimation.sampleStageMotion.perMM = perMM;
    }

    auto ModelInstance::SetZScanMotionTime(const double default, const double perMM) -> void {
        d->timeEstimation.zScanMotion.default = default;
        d->timeEstimation.zScanMotion.perMM = perMM;
    }

    auto ModelInstance::SetTriggerWithMotionAccelerationTime(const double value) -> void {
        d->timeEstimation.triggerWithMotion.acceleration = value;
    }

    auto ModelInstance::SetTriggerWithMotionDecelerationTime(const double value) -> void {
        d->timeEstimation.triggerWithMotion.deceleration = value;
    }

    auto ModelInstance::SetTriggerWithMotionMarginTime(const double value) -> void {
        d->timeEstimation.triggerWithMotion.margin = value;
    }

    auto ModelInstance::SetFocusToScanReadyMM(const double value) -> void {
        d->timeEstimation.distance.focusToScanReady = value;
    }

    auto ModelInstance::SetBFImagingMarginTime(const double value) -> void {
        d->timeEstimation.bf.marginTime = value;
    }

    auto ModelInstance::SetHTTriggerStartOffsetPulse(int32_t pulse) -> void {
        d->htTrigger.startOffset = pulse;
    }

    auto ModelInstance::SetHTTriggerEndOffsetPulse(int32_t pulse) -> void {
        d->htTrigger.endOffset = pulse;
    }

    auto ModelInstance::SetHTTriggerPulseWidthUSec(int32_t pulseWidth) -> void {
        d->htTrigger.pulseWidth = pulseWidth;
    }

    auto ModelInstance::SetHTTriggerReadOutUSec(int32_t readout) -> void {
        d->htTrigger.readOut = readout;
    }

    auto ModelInstance::SetHTTriggerExposureUSec(int32_t exposure) -> void {
        d->htTrigger.exposure = exposure;
    }

    auto ModelInstance::SetHTTriggerIntervalMarginUSec(int32_t intervalMargin) -> void {
        d->htTrigger.intervalMargin = intervalMargin;
    }

    auto ModelInstance::SetHTTriggerAccelerationFactor(int32_t factor) -> void {
        d->htTrigger.accelerationFactor = factor;
    }

    auto ModelInstance::SetCondenserAFScanStartPosPulse(int32_t pulse) -> void {
        d->condenserAFScan.startPos = pulse;
    }

    auto ModelInstance::SetCondenserAFScanIntervalPulse(int32_t pulse) -> void {
        d->condenserAFScan.triggerInterval = pulse;
    }

    auto ModelInstance::SetCondenserAFScanTriggerSlices(int32_t slices) -> void {
        d->condenserAFScan.slices = slices;
    }

    auto ModelInstance::SetCondenserAFScanReadOutUSec(int32_t readout) -> void {
        d->condenserAFScan.readOut = readout;
    }

    auto ModelInstance::SetCondenserAFScanExposureUSec(int32_t exposure) -> void {
        d->condenserAFScan.exposure = exposure;
    }

    auto ModelInstance::SetCondenserAFScanIntervalMarginUSec(int32_t intervalMargin) -> void {
        d->condenserAFScan.intervalMargin = intervalMargin;
    }

    auto ModelInstance::SetCondenserAFScanPulseWidthUSec(int32_t pulseWidth) -> void {
        d->condenserAFScan.pulseWidth = pulseWidth;
    }

    auto ModelInstance::SetMultiDishHolderThickness(double thickness) -> void {
        d->multiDishHolder.thickness = thickness;
    }

    auto ModelInstance::SetIlluminationPattern(ImagingType imagingType, TriggerType triggerType,
                                               int32_t channel, int32_t patternIndex) -> void {
        d->illumination.patterns[{imagingType, triggerType, channel}] = patternIndex;
    }

    auto ModelInstance::SetFLIlluminationMap(int32_t channel, IlluminationMapIndex mapIndex) -> void {
        qDebug() << "FL Illumination Map [" << channel << "] = R:" << mapIndex.GetChannelRed() << " G:" << mapIndex.GetChannelGreen() << " B:" << mapIndex.GetChannelBlue();
        d->flIllumination.map[mapIndex] = channel;
        d->flIllumination.mapReversed[channel] = mapIndex;
    }

    auto ModelInstance::SetHTIlluminationSetupPattern(double NA, int32_t patternIndex) -> void {
        d->htIllumSetupPattern[NA] = patternIndex;
    }

    auto ModelInstance::SetSetupReference(AppEntity::SetupRef item, double value) -> void {
        d->setupReferences[item] = value;
    }

    auto ModelInstance::SetEvaluationReference(AppEntity::EvaluationRef item, double value) -> void {
        d->evalReferences[item] = value;
    }

    auto ModelInstance::SetEvaluationReference(AppEntity::EvaluationRef item, double NA, double value) -> void {
        d->evalReferencesNA[{item, NA}] = value;
    }

    auto ModelInstance::SetPreviewParameter(AppEntity::PreviewParam item, const QVariant& value) -> void {
        d->previewParameters[item] = value;
    }

}
