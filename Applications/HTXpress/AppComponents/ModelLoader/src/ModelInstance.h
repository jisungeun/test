#pragma once
#include <memory>

#include <Model.h>

namespace HTXpress::AppComponents::ModelLoader {
    class Reader;
    
    class ModelInstance : public AppEntity::Model {
    public:
        using Pointer = std::shared_ptr<ModelInstance>;
        using Axis = AppEntity::Axis;
        using ImagingType = AppEntity::ImagingType;
        using PatternIndex = AppEntity::PatternIndex;
        using TriggerType = AppEntity::TriggerType;
        using IlluminationMapIndex = AppEntity::IlluminationMapIndex;
        
    public:
        explicit ModelInstance(const QString& name);
        ~ModelInstance() override;
        
        //optical specification
        auto ObjectiveLensMagnification() const->double override;
        auto ObjectiveLensNA() const->double override;
        auto CondenserLensMagnification() const->double override;
        auto CondenserLensNA() const->double override;
        auto CameraPixelSize() const->double override;
        auto CameraPixelsH() const->uint32_t override;
        auto CameraPixelsV() const->uint32_t override;
        auto CameraFOVStepH() const -> uint32_t override;
        auto CameraFOVStepV() const -> uint32_t override;

        //imaging specification
        auto MinimumIntervalUSec() const -> int32_t override;
        auto MaximumIntervalUSec() const -> int32_t override;
        auto MinimumIdleUSec() const -> int32_t override;
        auto MinimumExposureUSec() const -> int32_t override;
        auto MaximumExposureUSec() const -> int32_t override;
        auto HTLightChannel() const -> int32_t override;
        auto HTFilterChannel() const -> int32_t override;
        auto BFLightChannel() const -> int32_t override;
        auto BFFilterChannel() const -> int32_t override;
        auto FLMaximumSlices() const -> int32_t override;
        auto CondenserAFLightChannel() const -> int32_t override;
        auto SafeXYRangeMM() const->std::tuple<double,double> override;

        //motion specification
        auto AxisResolutionPPM(Axis axis) const->uint32_t override;
        auto AxisLowerLimitMM(Axis axis) const->double override;
        auto AxisUpperLimitMM(Axis axis) const->double override;
        auto AxisMotionTimeSPM(Axis axis) const -> double override;

        //predefined positions
        auto SafeZPositionMM() const -> double override;
        auto LoadingXPositionMM() const -> double override;
        auto LoadingYPositionMM() const -> double override;
        auto EmissionFilterPules() const -> QList<int32_t> override;
        auto DripTrayRemovingXPositionMM() const -> double override;
        auto DripTrayRemovingYPositionMM() const -> double override;
        auto DripTrayRemovingZPositionMM() const -> double override;
        auto DripTrayRemovingCPositionMM() const -> double override;
        auto ParkingXPositionMM() const -> double override;
        auto ParkingYPositionMM() const -> double override;
        auto ParkingZPositionMM() const -> double override;
        auto ParkingCPositionMM() const -> double override;

        //Time estimation parameters
        auto FilterChangeTime() const -> std::tuple<double, double> override;
        auto SampleStageMotionTime() const -> std::tuple<double, double> override;
        auto ZScanMotionTime() const -> std::tuple<double, double> override;
        auto TriggerWithMotionAccelerationTime() const -> double override;
        auto TriggerWithMotionDecelerationTime() const -> double override;
        auto TriggerWithMotionMarginTime() const -> double override;
        auto FocusToScanReadyMM() const -> double override;
        auto BFImagingMarginTime() const -> double override;

        //HT scan parameters
        auto HTTriggerStartOffsetPulse() const -> int32_t override;
        auto HTTriggerEndOffsetPulse() const -> int32_t override;
        auto HTTriggerPulseWidthUSec() const -> int32_t override;
        auto HTTriggerReadOutUSec() const -> int32_t override;
        auto HTTriggerExposureUSec() const -> int32_t override;
        auto HTTriggerIntervalMarginUSec() const -> int32_t override;
        auto HTTriggerAccelerationFactor() const -> int32_t override;

        //Condenser AF Scan Parameters
        auto CondenserAFScanStartPosPulse() const -> int32_t override;
        auto CondenserAFScanIntervalPulse() const -> int32_t override;
        auto CondenserAFScanTriggerSlices() const -> int32_t override;
        auto CondenserAFScanReadOutUSec() const -> int32_t override;
        auto CondenserAFScanExposureUSec() const -> int32_t override;
        auto CondenserAFScanIntervalMarginUSec() const -> int32_t override;
        auto CondenserAFScanPulseWidthUSec() const -> int32_t override;

        //multi-dish holder
        auto MultiDishHolderThickness() const -> double override;

        //illumination patterns
        auto GetIlluminationPattern(ImagingType imagingType, TriggerType triggerType, int32_t channel) const -> int32_t override;
        auto GetIlluminationPattern(const PatternIndex& index) const -> int32_t override;
        auto GetIlluminationPatterns() const -> QList<PatternIndex> override;

        //FL Illumination Map
        auto GetFLIlluminationMap(int32_t channel) const -> IlluminationMapIndex override;
        auto GetFLIlluminationMapChannel(IlluminationMapIndex mapIndex) const -> int32_t override;
        auto GetFLIlluminationMapChannel(const QList<int32_t>& flChannels) const -> std::tuple<int32_t, QList<int32_t>> override;
        auto GetFLIlluminationMapChannel(int32_t flChannelIndex) const -> std::tuple<int32_t, int32_t> override;
        auto GetFLIlluminationMapChannels() const -> QList<int32_t> override;

        //HT Illumination Intensity Setup Patterns
        auto GetHTIlluminationSetupPattern(double NA) const -> int32_t override;
        auto GetHTIlluminationSetupPatternNAs() const -> QList<double> override;

        //Setup references
        auto SetupReference(AppEntity::SetupRef item) const -> double override;

        //Evaluation references
        auto EvaluationReference(AppEntity::EvaluationRef item) const -> double override;
        auto EvaluationReference(AppEntity::EvaluationRef item, double NA) const -> double override;

        //Preview parameters
        auto PreviewParameter(AppEntity::PreviewParam item) const -> QVariant override;

    protected:
        auto SetObjectiveMagnification(const double value)->void;
        auto SetObjectiveLensNA(const double value)->void;
        auto SetCondenserLensMagnification(const double value)->void;
        auto SetCondenserLensNA(const double value)->void;
        auto SetCameraPixelSize(const double value)->void;
        auto SetCameraPixels(uint32_t pixelsH, uint32_t pixelsV)->void;
        auto SetCameraFOVStep(uint32_t stepH, uint32_t stepV)->void;

        auto SetMinimumIntervalUSec(int32_t value)->void;
        auto SetMaximumIntervalUSec(int32_t value)->void;
        auto SetMinimumIdleUSec(int32_t value)->void;
        auto SetMinimumExposureUSec(int32_t value)->void;
        auto SetMaximumExposureUSec(int32_t value)->void;
        auto SetHTLightChannel(int32_t value)->void;
        auto SetHTFilterChannel(int32_t value)->void;
        auto SetBFLightChannel(int32_t value)->void;
        auto SetBFFilterChannel(int32_t value)->void;
        auto SetFLMaximumSlices(int32_t value)->void;
        auto SetCondenserAFLightChannel(int32_t value)->void;
        auto SetSafeXYRangeMM(double xRange, double yRange)->void;
        
        auto SetAxisResolutionPPM(const Axis axis, const uint32_t value)->void;
        auto SetAxisLowerLimitMM(const Axis axis, const double value)->void;
        auto SetAxisUpperLimitMM(const Axis axis, const double value)->void;
        auto SetAxisMotionTimeSPM(const Axis axis, const double value)->void;

        auto SetSafeZPositionMM(const double value)->void;
        auto SetLoadingPositionMM(const double xValue, const double yValue)->void;
        auto SetEmissionFilterPositions(const QList<int32_t>& positions)->void;
        auto SetDripTrayRemovingPositionMM(double xValue, double yValue, double zValue, double cValue)->void;
        auto SetParkingPositionMM(double xValue, double yValue, double zValue, double cValue)->void;

        auto SetFilterChangeTime(const double default, const double perStep)->void;
        auto SetSampleStageMotionTime(const double default, const double perMM)->void;
        auto SetZScanMotionTime(const double default, const double perMM)->void;
        auto SetTriggerWithMotionAccelerationTime(const double value)->void;
        auto SetTriggerWithMotionDecelerationTime(const double value)->void;
        auto SetTriggerWithMotionMarginTime(const double value)->void;
        auto SetFocusToScanReadyMM(const double value)->void;
        auto SetBFImagingMarginTime(const double value)->void;

        auto SetHTTriggerStartOffsetPulse(int32_t pulse)->void;
        auto SetHTTriggerEndOffsetPulse(int32_t pulse)->void;
        auto SetHTTriggerPulseWidthUSec(int32_t pulseWidth)->void;
        auto SetHTTriggerReadOutUSec(int32_t readout)->void;
        auto SetHTTriggerExposureUSec(int32_t exposure)->void;
        auto SetHTTriggerIntervalMarginUSec(int32_t intervalMargin)->void;
        auto SetHTTriggerAccelerationFactor(int32_t factor)->void;

        auto SetCondenserAFScanStartPosPulse(int32_t pulse) -> void;
        auto SetCondenserAFScanIntervalPulse(int32_t pulse) -> void;
        auto SetCondenserAFScanTriggerSlices(int32_t slices) -> void;
        auto SetCondenserAFScanReadOutUSec(int32_t readout) -> void;
        auto SetCondenserAFScanExposureUSec(int32_t exposure) -> void;
        auto SetCondenserAFScanIntervalMarginUSec(int32_t intervalMargin) -> void;
        auto SetCondenserAFScanPulseWidthUSec(int32_t pulseWidth) -> void;

        auto SetMultiDishHolderThickness(double thickness) -> void;

        auto SetIlluminationPattern(ImagingType imagingType, TriggerType triggerType, 
                                    int32_t channel, int32_t patternIndex) -> void;

        auto SetFLIlluminationMap(int32_t channel, IlluminationMapIndex mapIndex)->void;

        auto SetHTIlluminationSetupPattern(double NA, int32_t patternIndex) -> void;

        auto SetSetupReference(AppEntity::SetupRef item, double value) -> void;

        auto SetEvaluationReference(AppEntity::EvaluationRef item, double value) -> void;
        auto SetEvaluationReference(AppEntity::EvaluationRef item, double NA, double value) -> void;

        auto SetPreviewParameter(AppEntity::PreviewParam item, const QVariant& value) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
        
        friend class Reader;
    };
}