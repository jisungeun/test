#pragma once
#include <memory>
#include <QStringList>

#include "HTXModelLoaderExport.h"

namespace HTXpress::AppComponents::ModelLoader {
	class HTXModelLoader_API Loader {
    public:
        Loader();
        ~Loader();

        auto Load(const QString& path)->bool;
        auto GetError() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}