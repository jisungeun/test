#pragma once
#include <memory>
#include <QString>

#include <SystemConfig.h>

#include "HTXFLEmissionIOExport.h"

namespace HTXpress::AppComponents::FLEmissionIO {
    class HTXFLEmissionIO_API Writer {
    public:
        Writer();
        ~Writer();

        auto Write(const QString& dirPath, const AppEntity::SystemConfig::Pointer& config) const -> bool;
    };
}
