#pragma once
#include <memory>
#include <QString>

#include <SystemConfig.h>

#include "HTXFLEmissionIOExport.h"

namespace HTXpress::AppComponents::FLEmissionIO {
	class HTXFLEmissionIO_API Reader {
	public:
		Reader();
		~Reader();

		auto Read(const QString& dirPath, const AppEntity::SystemConfig::Pointer& config) const->bool;
	};
}
