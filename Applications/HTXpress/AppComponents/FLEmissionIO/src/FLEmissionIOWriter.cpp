#define LOGGER_TAG "[FLEmissionWriter]"
#include <QSettings>
#include <QDir>

#include <TCLogger.h>

#include "FLEmissionIODefines.h"
#include "FLEmissionIOWriter.h"

namespace HTXpress::AppComponents::FLEmissionIO {
    Writer::Writer() {
    }

    Writer::~Writer() {
    }

    auto Writer::Write(const QString& dirPath, const AppEntity::SystemConfig::Pointer& config) const -> bool {
        const auto filePath = QString("%1/%2").arg(dirPath).arg(kFileName);

        if (!filePath.isEmpty()) {
            QLOG_INFO() << "A new FLEmission setting file is created in" << filePath;
            QDir().remove(filePath);
        }

        QSettings qs(filePath, QSettings::IniFormat);

        //emission
        qs.beginWriteArray(Key::title);
        for (auto channelIdx : config->GetFLEmissions()) {
            AppEntity::FLFilter emission;
            if (!config->GetFLEmission(channelIdx, emission)) continue;

            qs.setArrayIndex(channelIdx);

            qs.setValue(Key::channelIdx, channelIdx);
            qs.setValue(Key::bandwidth, emission.GetBandwidth());
            qs.setValue(Key::wavelength, emission.GetWaveLength());
        }
        qs.endArray();

        return true;
    }
}
