#pragma once

namespace HTXpress::AppComponents::FLEmissionIO {
    constexpr char kFileName[] = "flEmission.ini";

    namespace Key {
        constexpr char title[] = "FLEmission";
        constexpr char channelIdx[] = "Index";
        constexpr char wavelength[] = "WaveLength";
        constexpr char bandwidth[] = "Bandwidth";
    };
}
