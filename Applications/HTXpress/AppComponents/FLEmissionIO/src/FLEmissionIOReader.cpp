#define LOGGER_TAG "[FLEmissionReader]"
#include <QSettings>
#include <QFileInfo>

#include <TCLogger.h>
#include <AppEntityDefines.h>
#include <FLFilter.h>

#include "FLEmissionIODefines.h"
#include "FLEmissionIOReader.h"

namespace HTXpress::AppComponents::FLEmissionIO {
    auto ReadField(const QSettings& qs, const char key[]) -> QVariant {
        if (!qs.contains(key)) throw std::runtime_error(std::string("Missing key in fl emission config: ") + key);
        return qs.value(key);
    }

    auto CreateSample(const QString& path) -> void {
        const QString samplePath{QString("%1.sample").arg(path)};
        QSettings qs(samplePath, QSettings::IniFormat);

        qs.beginWriteArray(Key::title);
        for (auto idx = 0; idx < 6; idx++) {
            qs.setArrayIndex(idx);
            if (idx != 0) {
                qs.setValue(Key::channelIdx, idx);
                qs.setValue(Key::wavelength, 300 + idx * 7);
                qs.setValue(Key::bandwidth, 50 + idx * 7);
            }
            else {
                // emission ch1(idx0) must be void(has no value) -> set 0
                qs.setValue(Key::channelIdx, 0);
                qs.setValue(Key::wavelength, 0);
                qs.setValue(Key::bandwidth, 0);
            }
        }
        qs.endArray();
    }

    Reader::Reader() {
    }

    Reader::~Reader() {
    }

    auto Reader::Read(const QString& dirPath, const AppEntity::SystemConfig::Pointer& config) const -> bool {
        const auto filePath = QString("%1/%2").arg(dirPath).arg(kFileName);

        if (filePath.isEmpty()) {
            QLOG_ERROR() << "No FL Emission configuration file path is specified";
            return false;
        }

        if (!QFile::exists(filePath)) {
            QLOG_ERROR() << "FL Emission configuration file does not exist at " << filePath;
            CreateSample(filePath);
            return false;
        }

        QSettings qs(filePath, QSettings::IniFormat);

        try {
            const auto emissions = qs.beginReadArray(Key::title);
            for (auto idx = 0; idx < emissions; idx++) {
                AppEntity::FLFilter emission;
                qs.setArrayIndex(idx);
                emission.SetWaveLength(ReadField(qs, Key::wavelength).toUInt());
                emission.SetBandwidth(ReadField(qs, Key::bandwidth).toUInt());

                config->SetFLEmission(ReadField(qs, Key::channelIdx).toInt(), emission);
            }
            qs.endArray();
        }
        catch (std::exception& ex) {
            QLOG_ERROR() << "It fails to read fl emission configuration. Error=" << ex.what();
            CreateSample(dirPath);
            return false;
        }

        return true;
    }
}
