#pragma once
#include <memory>

#include <Area.h>
#include <Location.h>
#include <PositionGroup.h>
#include <ImagingCondition.h>

#include "ImagingTimeTable.h"
#include "HTXScanTimeCalculatorExport.h"

namespace HTXpress::AppComponents::ScanTimeCalculator {
    class HTXScanTimeCalculator_API Component {
    public:
        using Pointer = std::shared_ptr<Component>;
        using ImagingCondition = AppEntity::ImagingCondition;
        using Location = AppEntity::Location;
        using PositionGroup = AppEntity::PositionGroup;
        
    protected:
        Component();
        
    public:
        ~Component();
        
        static auto GetInstance()->Pointer;

        auto SetMaximumFOV(AppEntity::Area area) -> void;
        auto SetOverlapInUM(uint32_t overlap) -> void;
        auto SetFilterPosition(AppEntity::Modality modality, int32_t channel, int32_t position) -> void;
        auto SetUsingMultidishHolder(bool useMultidishHolder) -> void;

        auto CalcInSec(const ImagingCondition::Pointer& imgCond, 
                       ImagingTimeTable::Pointer& timeTable,
                       const ImagingCondition::Pointer& prevCond) -> void;
        auto CalcInSec(const QList<Location>& locations, ImagingTimeTable::Pointer& timeTable) -> void;
        auto CalcInSec(const QList<PositionGroup>& groups, ImagingTimeTable::Pointer& timeTable) -> void;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}