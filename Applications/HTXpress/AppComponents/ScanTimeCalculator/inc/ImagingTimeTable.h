#include <Area.h>
#include <ImagingCondition.h>

#include "HTXScanTimeCalculatorExport.h"

namespace HTXpress::AppComponents::ScanTimeCalculator {
    class HTXScanTimeCalculator_API ImagingTimeTable {
    public:
        using Pointer = std::shared_ptr<ImagingTimeTable>;

    public:
        ImagingTimeTable();
        ~ImagingTimeTable();

        auto SetTimeForCondition(AppEntity::Modality modality, bool is2D, double timeInSec)->void;
        auto GetTimeForCondition(AppEntity::Modality modality, bool is2D) const->double;
        auto GetTimeForAllConditions() const->double;
        auto GetTimeForConditions(const QList<AppEntity::ImagingCondition::Pointer>& conds)->double;

        auto SetTimeForCycle(double timeInSec, int32_t points)->void;
        auto GetTimeForCycle() const->double;
        auto GetPointCount() const->int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}