#define LOGGER_TAG "[ScanTimeCalculator]"

#include <QSettings>

#include <TCLogger.h>
#include <AppEntityDefines.h>
#include <System.h>
#include <ScanningPlanner.h>

#include "ScanTimeCalculator.h"

namespace HTXpress::AppComponents::ScanTimeCalculator {
    struct Component::Impl {
        AppEntity::Area maximumFOV;
        uint32_t overlapInUm{ 0 };
        QMap<QPair<AppEntity::Modality, int32_t>, int32_t> filterPositions;
        bool isMultiDishHolder{ false };
        bool enableLog{ false };

        auto Init()->void;

        inline auto CalcHTImaging(const ImagingCondition::Pointer& imgCond, 
                                  ImagingTimeTable::Pointer& timeTable,
                                  const int32_t prevFilterPosition)->void;
        inline auto CalcFLImaging(const ImagingCondition::Pointer& imgCond, 
                                  ImagingTimeTable::Pointer& timeTable,
                                  const int32_t prevFilterPosition)->void;
        inline auto CalcBFImaging(const ImagingCondition::Pointer& imgCond, 
                                  ImagingTimeTable::Pointer& timeTable,
                                  const int32_t prevFilterPosition)->void;
    };

    auto Component::Impl::Init() -> void {
        enableLog = QSettings().value("Misc/ScanTime_Calculator_Log", false).toBool();
    }

    auto Component::Impl::CalcHTImaging(const ImagingCondition::Pointer& imgCond,
                                                ImagingTimeTable::Pointer& timeTable,
                                                const int32_t prevFilterPosition) -> void {
        using Axis = AppEntity::Model::Axis;

        const auto modality = AppEntity::Modality::HT;
        if(imgCond->GetModality() != +modality) return;

        const auto model = AppEntity::System::GetModel();
        auto [zscanDefaultTime, zscanUnitTime] = model->ZScanMotionTime();
        auto [filterDefaultTime, filterUnitTime] = model->FilterChangeTime();

        const auto readOut = model->HTTriggerReadOutUSec();
        const auto exposure = model->HTTriggerExposureUSec();
        const auto intervalMargin = model->HTTriggerIntervalMarginUSec();
        const auto zOffsetPulse = model->HTTriggerStartOffsetPulse();
        const auto zResolution = model->AxisResolutionPPM(Axis::Z);
        const auto zScanAccelTime = model->TriggerWithMotionAccelerationTime();
        const auto zScanDecelTime = model->TriggerWithMotionDecelerationTime();
        const auto zScanMarginTime = model->TriggerWithMotionMarginTime();
        const auto startOffset = model->HTTriggerStartOffsetPulse();
        const auto endOffset = model->HTTriggerEndOffsetPulse();

        auto htCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionHT>(imgCond);
        const auto slices = htCond->GetSliceCount();
        const auto startPos = htCond->GetSliceStart();
        const auto stepDist = std::max<int32_t>(1, htCond->GetSliceStep());
        const auto motionSpeed = (stepDist * 1000000) / ((readOut + exposure) * 4 + intervalMargin);
        const auto motionStartPos = startPos - stepDist - startOffset;
        const auto motionEndPos = startPos + (stepDist * (slices - 1)) + 1 + endOffset;
        const auto zScanTime = (motionEndPos - motionStartPos) / (motionSpeed * 1.0);
        const auto zReturnTime = zscanDefaultTime + (1.0 * motionEndPos / zResolution) * zscanUnitTime;
        const auto filterSteps = std::abs(prevFilterPosition - filterPositions[{modality, 0}]);
        const auto filterTime = (filterSteps>0) ? filterDefaultTime + (filterUnitTime * filterSteps) : 0;
        const auto movingToHTStartPosTime = zscanDefaultTime +  (1.0 * std::abs(motionStartPos) / zResolution) * zscanUnitTime;

        const auto timeInSec = filterTime + 
                               movingToHTStartPosTime + 
                               zScanAccelTime +
                               zScanTime + 
                               zScanDecelTime +
                               zReturnTime +
                               zScanMarginTime;

        if(enableLog) {
            QLOG_INFO() << "Slices=" << slices
                        << ", Step dist=" << stepDist
                        << ", Motion speed=" << motionSpeed
                        << ", Motion start=" << motionStartPos
                        << ", Motion end=" << motionEndPos
                        << ", Filter steps=" << filterSteps
                        << ", Filter time=" << filterTime
                        << ", ToStartPos time=" << movingToHTStartPosTime
                        << ", Accel time=" << zScanAccelTime
                        << ", Scan time=" << zScanTime
                        << ", Decel time=" << zScanDecelTime
                        << ", Return time=" << zReturnTime
                        << ", Margin time=" << zScanMarginTime
                        << ", Total time=" << timeInSec;
        }

        timeTable->SetTimeForCondition(modality, slices==1, timeInSec);
    }

    auto Component::Impl::CalcFLImaging(const ImagingCondition::Pointer& imgCond,
                                                ImagingTimeTable::Pointer& timeTable,
                                                const int32_t prevFilterPosition) -> void {
        using Axis = AppEntity::Model::Axis;

        const auto modality = AppEntity::Modality::FL;
        if(imgCond->GetModality() != +modality) return;

        const auto model = AppEntity::System::GetModel();
        auto [zscanDefaultTime, zscanUnitTime] = model->ZScanMotionTime();
        auto [filterDefaultTime, filterUnitTime] = model->FilterChangeTime();
        const auto zOffsetPulse = model->HTTriggerStartOffsetPulse();
        const auto zResolution = model->AxisResolutionPPM(Axis::Z);

        auto flCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(imgCond);
        const auto channels = flCond->GetChannels();
        auto intervalInUSec = 0;
        for(auto channel : channels) {
            intervalInUSec += flCond->GetExposure(channel) + 8000;
        }
        const auto slices = flCond->GetSliceCount();
        const auto startPos = flCond->GetSliceStart();
        const auto stepDist = std::max<int32_t>(1, flCond->GetSliceStep());
        const auto motionEndPos = startPos + (stepDist * (slices - 1)) + 1;
        const auto zMotionTime = (zscanDefaultTime + (1.0 * stepDist / zResolution) * zscanUnitTime) * slices;
        const auto zReturnTime = zscanDefaultTime + (1.0 * motionEndPos / zResolution) * zscanUnitTime;
        const auto filterSteps = [=]()->int32_t {
            int32_t total = 0;
            int32_t prev = prevFilterPosition;
            for(auto channel : channels) {
                total += std::abs(prev - filterPositions[{modality, channel}]);
                prev = filterPositions[{modality, channel}];
            }
            return total;
        }();
        const auto filterTime = (filterSteps>0) ? filterDefaultTime + (filterUnitTime*filterSteps) : 0 ;
        const auto movingToFLStartPosTime = zscanDefaultTime + (1.0 * std::abs(startPos) / zResolution) * zscanUnitTime;

        const auto timeInSec = filterTime + 
                               (movingToFLStartPosTime + zMotionTime + zReturnTime) * channels.size() +
                               intervalInUSec/1000000.0 * slices;

        if(enableLog) {
            QLOG_INFO() << "Slices=" << slices
                        << ", Step dist=" << stepDist
                        << ", Channels=" << channels.size()
                        << ", Interval=" << intervalInUSec
                        << ", Motion end=" << motionEndPos
                        << ", Filter steps=" << filterSteps
                        << ", Filter time=" << filterTime
                        << ", ToStartPos time=" << movingToFLStartPosTime
                        << ", Scan time=" << zMotionTime
                        << ", Return time=" << zReturnTime
                        << ", Total time=" << timeInSec;
        }

        timeTable->SetTimeForCondition(modality, slices==1, timeInSec);
    }

    auto Component::Impl::CalcBFImaging(const ImagingCondition::Pointer& imgCond,
                                                ImagingTimeTable::Pointer& timeTable,
                                                const int32_t prevFilterPosition) -> void {
        const auto modality = AppEntity::Modality::BF;
        if(imgCond->GetModality() != +modality) return;

        const auto model = AppEntity::System::GetModel();
        auto [filterDefaultTime, filterUnitTime] = model->FilterChangeTime();
        auto bfImagingMarginTime = model->BFImagingMarginTime();

        auto bfCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionBF>(imgCond);
        const auto intervalInUSec = bfCond->GetInterval();
        const auto imagesPerSlice = bfCond->GetColorMode()? 3 : 1;
        const auto slices = bfCond->GetSliceCount();
        const auto filterSteps = std::abs(prevFilterPosition - filterPositions[{modality, 0}]);
        const auto filterTime = (filterSteps>0) ? filterDefaultTime + (filterUnitTime*filterSteps) : 0;

        const auto timeInSec = bfImagingMarginTime + filterTime + (intervalInUSec * imagesPerSlice * slices) / 1000000.0;

        if(enableLog) {
            QLOG_INFO() << "Slices=" << slices
                        << ", Images per slice=" << imagesPerSlice
                        << ", Interval=" << intervalInUSec
                        << ", Filter steps=" << filterSteps
                        << ", filter time=" << filterTime
                        << ", margin time=" << bfImagingMarginTime
                        << ", Total time=" << timeInSec;
        }

        timeTable->SetTimeForCondition(modality, slices==1, timeInSec);
    }
    
    Component::Component() : d{new Impl} {
        d->Init();
    }
    
    Component::~Component() {
    }
    
    auto Component::GetInstance() -> Pointer {
        static Pointer theInstance{ new Component() };
        return theInstance;
    }

    auto Component::SetMaximumFOV(AppEntity::Area area) -> void {
        d->maximumFOV = area;
    }

    auto Component::SetOverlapInUM(uint32_t overlap) -> void {
        d->overlapInUm = overlap;
    }

    auto Component::SetFilterPosition(AppEntity::Modality modality, int32_t channel, int32_t position) -> void {
        QLOG_INFO() << "Filter Positions [" << modality._to_string() << ", " << channel << "] = " << position;
        d->filterPositions[{modality, channel}] = position;
    }

    auto Component::SetUsingMultidishHolder(bool useMultidishHolder) -> void {
        d->isMultiDishHolder = useMultidishHolder;
    }

    auto Component::CalcInSec(const ImagingCondition::Pointer& imgCond, 
                                      ImagingTimeTable::Pointer& timeTable,
                                      const ImagingCondition::Pointer& prevCond) -> void {
        using Modality = AppEntity::Modality;

        const auto prevFilterPos = [=]()->int32_t {
            if(prevCond == nullptr) return 0;
            if(prevCond->CheckModality(Modality::BF) || prevCond->CheckModality(Modality::HT)) {
                return d->filterPositions[{prevCond->GetModality(), 0}];
            }

            auto flCond = std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(prevCond);
            auto lastChannel = flCond->GetChannels().last();
            return d->filterPositions[{prevCond->GetModality(), lastChannel}];
        }();

        d->CalcHTImaging(imgCond, timeTable, prevFilterPos);
        d->CalcFLImaging(imgCond, timeTable, prevFilterPos);
        d->CalcBFImaging(imgCond, timeTable, prevFilterPos);
    }

    auto Component::CalcInSec(const QList<Location>& locations,
                                      ImagingTimeTable::Pointer& timeTable) -> void {
        using LocationIndex = AppEntity::LocationIndex;
        using WellIndex = AppEntity::WellIndex;

        auto plan = AppComponents::ScanningPlanner::ScanningPlanner();
        plan.SetMaximumFOV(d->maximumFOV);
        plan.SetOverlapInUM(d->overlapInUm);

        QMap<LocationIndex, Location> tempLocs;
        for(LocationIndex idx=0; idx<locations.size(); idx++) {
            tempLocs[idx] = locations.at(idx);
        }

        QMap<WellIndex, QMap<LocationIndex, Location>> convertedLocations{ {0, tempLocs} };  //Use dummy well index
        plan.SetLocations(convertedLocations);

        auto groups = plan.Perform();
        if(groups.length() == 0) return;

        CalcInSec(groups, timeTable);
    }

    auto Component::CalcInSec(const QList<PositionGroup>& groups,
                              ImagingTimeTable::Pointer& timeTable) -> void {
        using PositionGroup = AppEntity::PositionGroup;
        using Position = AppEntity::Position;

        const auto model = AppEntity::System::GetModel();
        const auto sysConfig = AppEntity::System::GetSystemConfig();

        const auto [defaultTime, unitTime] = model->SampleStageMotionTime();
        const auto [defaultTimeZ, unitTimeZ] = model->ZScanMotionTime();
        const auto afTime = sysConfig->GetAutoFocusTime();
        const auto focusToScanReady = model->FocusToScanReadyMM();

        if(groups.isEmpty()) {
            timeTable->SetTimeForCycle(0, 0);
            return;
        }

        AppEntity::PositionGroup prevGroup = groups.at(0);
        AppEntity::Position prevPosition = prevGroup.GetPosition(0, 0);

        auto calcDistInMM = [&](const AppEntity::Position& nextPosition)->double {
            const auto target = nextPosition.toMM();
            const auto start = prevPosition.toMM();
            const auto dist = std::max(std::abs(target.x-start.x), std::abs(target.y-start.y));
            prevPosition = nextPosition;
            return dist;
        };

        double posMovingTime = 0;

        int well2WellMovingCount = 0;
        int positionCount = 0;
        double distAll = 0;

        for(const auto& group : groups) {
            auto positions = group.GetPositions();

            const auto wellChanged = (prevGroup.GetWellIndex() != group.GetWellIndex());
            if(wellChanged) well2WellMovingCount++;

            for(const auto& position : positions) {
                const auto dist = calcDistInMM(position);
                if(dist>0) posMovingTime += defaultTime + (dist * unitTime);

                positionCount++;
                distAll += dist;
            }

            prevGroup = group;
        }

        const auto MultiDishThickness = d->isMultiDishHolder ? model->MultiDishHolderThickness() : 0;
        const auto well2wellZDist = (focusToScanReady + MultiDishThickness) * 2; 
        const auto well2wellTime = (defaultTimeZ + (well2wellZDist * unitTimeZ) + afTime) * well2WellMovingCount;
        const auto totalAFTime = (positionCount - well2WellMovingCount - 1) * afTime;

        const auto returnDist = calcDistInMM(groups.at(0).GetPosition(0, 0));
        const auto returnTime = defaultTime + (returnDist * unitTime);

        auto timeInSec = posMovingTime + well2wellTime + totalAFTime + returnTime;

        if(d->enableLog) {
            QLOG_INFO() << "Total points=" << positionCount
                        << ", Well2Well=" << well2WellMovingCount
                        << ", Distance=" << distAll
                        << ", Return=" << returnDist
                        << ", Moving time=" << posMovingTime
                        << ", Well2Well time=" << well2wellTime
                        << ", InsideWell time=" << totalAFTime
                        << ", Return time=" << returnTime
                        << ", Total time=" << timeInSec;
        }
        timeTable->SetTimeForCycle(timeInSec, positionCount);
    }

}