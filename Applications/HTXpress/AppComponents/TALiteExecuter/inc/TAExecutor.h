#pragma once
#include <memory>

#include <QCoreApplication>

#include "HTXTomoAnalysisExecutorExport.h"

namespace HTXpress::AppComponents::TomoaAnalysisExecutor {
    class HTXTomoAnalysisExecutor_API TAExecutor final {
        Q_DECLARE_TR_FUNCTIONS(TALiteExecuter)
    public:
        enum class ErrorCode {
            REG_READ_FAIL,
            FILE_CREATION_FAIL,
            NO_FILE_IN_DEFAULT_PATH,
            RUN_TA_FAIL
        };
        explicit TAExecutor();
        ~TAExecutor();

        auto Write(const QStringList& tcfList) -> bool;
        auto Execute() -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}