#define LOGGER_TAG "[TomoaAnalysisExecutor]"
#include <QProcess>
#include <QDir>
#include <QSettings>

#include <TCLogger.h>
#include <System.h>
#include <MessageDialog.h>

#include "TAExecutor.h"
#include "JsonWriter.h"

namespace HTXpress::AppComponents::TomoaAnalysisExecutor {
    struct TAExecutor::Impl {
        JsonWriter::Pointer jsonWriter{std::make_shared<JsonWriter>()};

        const QString orgName{QCoreApplication::organizationName()};
        const QString taAppName{"TomoAnalysis"};
        const QString taDefaultFilePath{"C:\\Program Files\\TomoAnalysis\\bin\\TomoAnalysis.exe"};

        auto GetTomoAnalysisPath() const -> QFileInfo;
        auto Error(const ErrorCode& errorCode) const -> QString;
    };

    auto TAExecutor::Impl::GetTomoAnalysisPath() const -> QFileInfo {
        const QSettings settings(orgName, taAppName);
        const auto taPath = settings.value("PATH", "").toString();
        return QFileInfo{taPath};
    }

    auto TAExecutor::Impl::Error(const ErrorCode& errorCode) const -> QString {
        QString content{};
        switch (errorCode) {
            case ErrorCode::FILE_CREATION_FAIL:
                content = tr("File creation failed.");
                break;
            case ErrorCode::REG_READ_FAIL:
                content = tr("Can't find registry key.\n" "Please check the registry or reinstall TomoAnalysis to restore the missing key.");
                break;
            case ErrorCode::NO_FILE_IN_DEFAULT_PATH:
                content = tr("TomoAnalysis could not be found in the default installation path.\n" "Please install TomoAnalysis and try again.");
                break;
            case ErrorCode::RUN_TA_FAIL:
                content = tr("TomoAnalysis execution failed.\n" "Please reinstall TomoAnalysis and try again.");
                break;
            default:
                content = tr("An unknown error has occurred.");
                break;
        }
        QLOG_ERROR() << content;
        return content;
    }

    TAExecutor::TAExecutor() : d{std::make_unique<Impl>()} {
    }

    TAExecutor::~TAExecutor() {
    }

    auto TAExecutor::Write(const QStringList& tcfList) -> bool {
        if (!d->jsonWriter->Write(tcfList)) {
            const auto errorMessage = d->Error(ErrorCode::FILE_CREATION_FAIL);
            TC::MessageDialog::warning(nullptr, tr("Error"), errorMessage);
            return false;
        }
        return true;
    }

    auto TAExecutor::Execute() -> bool {
        auto pathInfo = d->GetTomoAnalysisPath();
        if (pathInfo.fileName().isEmpty()) {
            if (QFile::exists(d->taDefaultFilePath)) {
                pathInfo = QFileInfo(d->taDefaultFilePath);
            } else {
                auto errorMessage = d->Error(ErrorCode::NO_FILE_IN_DEFAULT_PATH);
                errorMessage.append("\nDefault installation path: ");
                errorMessage.append(QString("\"%1\"").arg(d->taDefaultFilePath));
                TC::MessageDialog::warning(nullptr, tr("Error"), errorMessage);
                return false;
            }
        }

        const auto title = QString("Tomostudio X - %1").arg(AppEntity::System::GetSoftwareVersion());
        const auto tcfListFilePath = d->jsonWriter->GetPath();

        const auto arguments = QStringList() << "-title" << title << "-viewlist" << tcfListFilePath;

        QProcess process;
        process.setWorkingDirectory(pathInfo.absolutePath());
        process.setArguments(arguments);
        process.setProgram(pathInfo.absoluteFilePath());

        if (!process.startDetached()) {
            auto errorMessage = d->Error(ErrorCode::RUN_TA_FAIL);
            errorMessage.append("\nTomoAnalysis path: ");
            errorMessage.append(QString("\"%1\"").arg(pathInfo.absoluteFilePath()));
            TC::MessageDialog::warning(nullptr, tr("Error"), errorMessage);
            return false;
        }

        QLOG_INFO() << "TomoAnalysis run from" << pathInfo.absoluteFilePath();
        return true;
    }
}
