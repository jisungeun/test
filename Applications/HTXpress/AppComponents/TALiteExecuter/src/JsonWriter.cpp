#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonObject>
#include <QStandardPaths>
#include <QDateTime>
#include <QJsonDocument>

#include "JsonWriter.h"

namespace HTXpress::AppComponents::TomoaAnalysisExecutor {
    struct JsonWriter::Impl {
        QString fileName{""};
        const QString tempPath{QStandardPaths::writableLocation(QStandardPaths::TempLocation)+QDir::separator()+"tsx.viewerqueue"};
        
        auto GetFullPath() const -> QString;
    };

    auto JsonWriter::Impl::GetFullPath() const -> QString {
        return QFileInfo(tempPath, fileName).absoluteFilePath();
    }

    JsonWriter::JsonWriter() : d{std::make_unique<Impl>()} {
    }

    JsonWriter::~JsonWriter() {
    }

    auto JsonWriter::Write(const QStringList& tcfFilePathList) -> bool {
        QJsonObject json;
        QJsonArray tcfPathArray;

        for (const auto& tcfFilePath : tcfFilePathList) {
            QJsonObject tcfObject;
            tcfObject["Path"] = tcfFilePath;
            tcfPathArray.push_back(tcfObject);
        }
        json["TcfList"] = tcfPathArray;

        QDir dir;
        dir.setPath(d->tempPath);
        if(!dir.exists()) {
            if(!dir.mkdir(d->tempPath))
                return false;
        }
        d->fileName = QString("tmp_%1.json").arg(QDateTime::currentDateTime().toString("yyMMddhhmmss"));
        QFile file(d->GetFullPath());
        if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
            return false;
        }
        if (file.write(QJsonDocument(json).toJson()) < 0) {
            return false;
        }
        return true;
    }

    auto JsonWriter::GetPath() const -> QString {
        return d->GetFullPath();
    }
}
