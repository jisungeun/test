#pragma once
#include <memory>

namespace HTXpress::AppComponents::TomoaAnalysisExecutor {
    class JsonWriter final {
    public:
        using Pointer = std::shared_ptr<JsonWriter>;

        explicit JsonWriter();
        ~JsonWriter();

        auto Write(const QStringList& tcfFilePathList) -> bool;
        auto GetPath() const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
