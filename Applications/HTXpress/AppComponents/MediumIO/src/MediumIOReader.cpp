#define LOGGER_TAG "[MediumReader]"
#include <QSettings>
#include <QFileInfo>

#include <TCLogger.h>

#include "MediumIODefines.h"
#include "MediumIOReader.h"

namespace HTXpress::AppComponents::MediumIO {
    auto ReadField(const QSettings& qs, const char key[]) -> QVariant {
        if (!qs.contains(key)) throw std::runtime_error(std::string("Missing key in medium config: ") + key);
        return qs.value(key);
    }

    auto CreateSample(const QString& path) -> void {
        const QString samplePath{QString("%1.sample").arg(path)};
        QSettings qs(samplePath, QSettings::IniFormat);

        qs.beginWriteArray(Key::title);
        for (auto idx = 0; idx < 10; idx++) {
            qs.setArrayIndex(idx);
            qs.setValue(Key::name, QString("Medium_%1").arg(idx + 1));
            qs.setValue(Key::ri, 1.3333 + (idx * 0.2));
        }
        qs.endArray();
    }

    Reader::Reader() {
    }

    Reader::~Reader() {
    }

    auto Reader::Read(const QString& dirPath, const AppEntity::MediumDataRepo::Pointer& config) const -> bool {
        const auto filePath = QString("%1/%2").arg(dirPath).arg(kFileName);

        if (filePath.isEmpty()) {
            QLOG_ERROR() << "No medium configuration file path is specified";
            return false;
        }

        if (!QFile::exists(filePath)) {
            QLOG_ERROR() << "Medium configuration file does not exist at " << filePath;
            CreateSample(filePath);
            return false;
        }

        QSettings qs(filePath, QSettings::IniFormat);

        try {
            QList<AppEntity::MediumData> media;
            const auto count = qs.beginReadArray(Key::title);
            for (auto idx = 0; idx < count; idx++) {
                AppEntity::MediumData medium;
                qs.setArrayIndex(idx);
                medium.SetName(ReadField(qs, Key::name).toString());
                medium.SetRI(ReadField(qs, Key::ri).toDouble());
                media.push_back(medium);
            }
            qs.endArray();

            config->SetMedia(media);
        }
        catch (std::exception& ex) {
            QLOG_ERROR() << "It fails to read medium configuration. Error=" << ex.what();
            CreateSample(dirPath);
            return false;
        }

        return true;
    }
}
