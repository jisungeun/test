#define LOGGER_TAG "[MediumWriter]"
#include <QSettings>
#include <QDir>

#include <TCLogger.h>

#include "MediumIODefines.h"
#include "MediumIOWriter.h"

namespace HTXpress::AppComponents::MediumIO {
    Writer::Writer() {
    }

    Writer::~Writer() {
    }

    auto Writer::Write(const QString& dirPath, const AppEntity::MediumDataRepo::Pointer& mediumRepo) const -> bool {
        const auto filePath = QString("%1/%2").arg(dirPath).arg(kFileName);

        if (!filePath.isEmpty()) {
            QLOG_INFO() << "A new Medium setting file is created in" << filePath;
            QDir().remove(filePath);
        }

        QSettings qs(filePath, QSettings::IniFormat);

        qs.beginWriteArray(Key::title);
        for(int i = 0; i < mediumRepo->GetMedia().size(); ++i) {
            qs.setArrayIndex(i);
            qs.setValue(Key::name, mediumRepo->GetMedia().at(i).GetName());
            qs.setValue(Key::ri, mediumRepo->GetMedia().at(i).GetRI());
        }
        qs.endArray();

        return true;
    }
}
