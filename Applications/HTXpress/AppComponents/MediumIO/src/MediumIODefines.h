#pragma once

namespace HTXpress::AppComponents::MediumIO {
    constexpr char kFileName[] = "media.ini";

    namespace Key {
        constexpr char title[] = "Media";
        constexpr char name[] = "Name";
        constexpr char ri[] = "RI";
    };
}
