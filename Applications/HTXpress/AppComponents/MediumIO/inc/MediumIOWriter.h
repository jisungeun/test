#pragma once
#include <memory>
#include <QString>

#include <MediumDataRepo.h>

#include "HTXMediumIOExport.h"

namespace HTXpress::AppComponents::MediumIO {
    class HTXMediumIO_API Writer {
    public:
        Writer();
        ~Writer();

        auto Write(const QString& dirPath, const AppEntity::MediumDataRepo::Pointer& mediumRepo) const -> bool;
    };
}
