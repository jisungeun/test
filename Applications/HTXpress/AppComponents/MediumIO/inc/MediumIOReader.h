#pragma once
#include <memory>
#include <QString>

#include <MediumDataRepo.h>

#include "HTXMediumIOExport.h"

namespace HTXpress::AppComponents::MediumIO {
	class HTXMediumIO_API Reader {
	public:
		Reader();
		~Reader();

		auto Read(const QString& dirPath, const AppEntity::MediumDataRepo::Pointer& config) const->bool;
	};
}
