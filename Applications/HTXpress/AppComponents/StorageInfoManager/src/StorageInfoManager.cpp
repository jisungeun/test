﻿#include <QList>
#include <QTimer>
#include <QStorageInfo>

#include "StorageInfoManager.h"
#include "StorageInformation.h"

namespace HTXpress::AppComponents::StorageInfoManager {
    struct StorageInfoManager::Impl {
        QTimer timer{};
        QStorageInfo storage{};
        Gigabyte minRequiredStroage{};

        QList<IStorageInfoUpdater*> updaters{};

        auto GetStorageInformation() const -> StorageInformation;
        auto IsRootChanged(const QString& lastRoot) const -> bool;
        auto Update() const -> void;
    };

    auto StorageInfoManager::Impl::GetStorageInformation() const -> StorageInformation {
        StorageInformation info;
        info.bytesAvailable = storage.bytesAvailable();
        info.bytesAll = storage.bytesTotal();
        info.displayName = storage.displayName();
        info.rootDrive = storage.rootPath();
        return info;
    }

    auto StorageInfoManager::Impl::IsRootChanged(const QString& lastRoot) const -> bool {
        return lastRoot != storage.rootPath();
    }

    auto StorageInfoManager::Impl::Update() const -> void {
        const auto info = GetStorageInformation();
        for(const auto& updater : updaters) {
            updater->UpdateStorageInfo(info);
        }
    }

    StorageInfoManager::StorageInfoManager(QObject* parent) : QObject(parent), d{std::make_unique<Impl>()} {
        d->timer.setInterval(600*1000);

        connect(&d->timer, &QTimer::timeout, this, [this] {
            d->Update();
        });

        d->timer.start();
    }

    StorageInfoManager::~StorageInfoManager() {
    }

    auto StorageInfoManager::GetInstance() -> Pointer {
        static Pointer theInstance {new StorageInfoManager(nullptr)};
        return theInstance;
    }

    auto StorageInfoManager::SetPath(const QString& path) -> void {
        const auto lastRoot = d->storage.rootPath();
        d->storage.setPath(path);
        if(d->IsRootChanged(lastRoot)) {
            d->Update();
        }
    }

    auto StorageInfoManager::SetInterval(const int32_t& msec) -> void {
        d->timer.setInterval(msec);
    }

    auto StorageInfoManager::GetBytesTotal() const -> int64_t {
        return d->storage.bytesTotal();
    }

    auto StorageInfoManager::GetBytesAvailable() const -> int64_t {
        return d->storage.bytesAvailable();
    }

    auto StorageInfoManager::Regist(IStorageInfoUpdater* updater) -> void {
        d->updaters.push_back(updater);
    }

    auto StorageInfoManager::Deregist(IStorageInfoUpdater* updater) -> void {
        d->updaters.removeOne(updater);
    }

    auto StorageInfoManager::SetMinRequiredSpace(const Gigabyte& space) -> void {
        d->minRequiredStroage = space;
    }

    auto StorageInfoManager::GetMinRequiredSpace() const -> Gigabyte {
        return d->minRequiredStroage;
    }
}
