﻿#include "IStorageInfoUpdater.h"
#include "StorageInfoManager.h"

namespace HTXpress::AppComponents::StorageInfoManager {
    IStorageInfoUpdater::IStorageInfoUpdater() {
        StorageInfoManager::GetInstance()->Regist(this);
    }

    IStorageInfoUpdater::~IStorageInfoUpdater() {
        StorageInfoManager::GetInstance()->Deregist(this);
    }
}
