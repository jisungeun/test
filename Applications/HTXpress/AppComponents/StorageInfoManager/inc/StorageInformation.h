﻿#pragma once

#include <QtCore/QtCore>
#include <QMetaType>

#include "HTXStorageInfoManagerExport.h"

namespace HTXpress::AppComponents::StorageInfoManager {
    struct HTXStorageInfoManager_API StorageInformation {
        int64_t bytesAll{};
        int64_t bytesAvailable{};
        QString rootDrive{};
        QString displayName{};
    };
}

Q_DECLARE_METATYPE(HTXpress::AppComponents::StorageInfoManager::StorageInformation);
