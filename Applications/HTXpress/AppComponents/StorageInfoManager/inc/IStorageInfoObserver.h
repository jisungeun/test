﻿#pragma once
#include <memory>

#include "HTXStorageInfoManagerExport.h"
#include "StorageInformation.h"

namespace HTXpress::AppComponents::StorageInfoManager {
    class HTXStorageInfoManager_API IStorageInfoObserver {
    public:
        using Self = IStorageInfoObserver;
        using Pointer = std::shared_ptr<Self>;

        virtual ~IStorageInfoObserver() = default;

        virtual auto UpdateStorageInfo(const StorageInformation& storageInformation) -> void = 0;
    };
}
