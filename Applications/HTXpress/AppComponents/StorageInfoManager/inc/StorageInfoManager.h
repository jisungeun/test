﻿#pragma once

#include <QObject>

#include "HTXStorageInfoManagerExport.h"
#include "IStorageInfoUpdater.h"

namespace HTXpress::AppComponents::StorageInfoManager {
    class HTXStorageInfoManager_API StorageInfoManager final : public QObject {
        Q_OBJECT
    public:
        using Self = StorageInfoManager;
        using Pointer = std::shared_ptr<Self>;
        using Gigabyte = int32_t;

        ~StorageInfoManager() override;

        static auto GetInstance() -> Pointer;

        auto Regist(IStorageInfoUpdater* updater) -> void;
        auto Deregist(IStorageInfoUpdater* updater) -> void;

        auto SetPath(const QString& path) -> void;
        auto SetInterval(const int32_t& msec) -> void;

        auto GetBytesTotal() const -> int64_t;
        auto GetBytesAvailable() const -> int64_t;

        auto SetMinRequiredSpace(const Gigabyte& space) -> void;
        auto GetMinRequiredSpace() const -> Gigabyte;

    private:
        explicit StorageInfoManager(QObject *parent = nullptr);
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}