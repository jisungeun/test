﻿#pragma once
#include <QObject>

#include "HTXStorageInfoManagerExport.h"
#include "StorageInformation.h"

namespace HTXpress::AppComponents::StorageInfoManager {
    class HTXStorageInfoManager_API IStorageInfoUpdater {
    public:
        using Self = IStorageInfoUpdater;
        using Pointer = std::shared_ptr<Self>;

        IStorageInfoUpdater();
        virtual ~IStorageInfoUpdater();

        virtual auto UpdateStorageInfo(const StorageInformation& storageInformation) -> void = 0;
    };
}
