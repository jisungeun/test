#include <QCloseEvent>
#include <QKeyEvent>
#include <QFocusEvent>
#include <QDebug>

#include <FileNameValidator.h>

#include "ui_UserEditDialog.h"
#include "UserEditDialog.h"

namespace HTXpress::AppComponents::UserEdit {
    struct Dialog::Impl {
        QFrame* frame;
        Ui::UserEditDialog ui;
        Mode mode{ Mode::New };

        QStringList existIDs{};
        auto IsValidID(const QString& id) const -> bool;
    };

    Dialog::Dialog(QWidget* parent, Mode mode) : CustomDialog(parent), d{new Impl} {
        d->frame = new QFrame(this);
        d->mode = mode;

        SetContext(d->frame);
        SetTitle("Register");

        d->ui.setupUi(d->frame);
        d->ui.password->setEchoMode(QLineEdit::EchoMode::Password);
        d->ui.password2->setEchoMode(QLineEdit::EchoMode::Password);
        d->ui.message->setText("");
        d->ui.labelName->setObjectName("tc-useredit-dialog-label");
        d->ui.labelID->setObjectName("tc-useredit-dialog-label");
        d->ui.labelPassword->setObjectName("tc-useredit-dialog-label");
        d->ui.labelPrivilege->setObjectName("tc-useredit-dialog-label");
        d->ui.message->setObjectName("tc-useredit-dialog-message");

        d->ui.name->SetPlaceholderOpacity(0.3);
        d->ui.id->SetPlaceholderOpacity(0.3);
        d->ui.password->SetPlaceholderOpacity(0.3);
        d->ui.password2->SetPlaceholderOpacity(0.3);

        d->ui.id->setReadOnly(mode == Mode::Edit);
        d->ui.password2->setReadOnly(true);

        for(auto profile : AppEntity::Profile::_values()) {
            if(profile == +AppEntity::Profile::ServiceEngineer) continue;
            d->ui.privilege->addItem(profile._to_string(), profile._to_integral());
        }

        connect(d->ui.password, &QLineEdit::textChanged, this, 
                [&](const QString& text) { d->ui.password2->setReadOnly(text.isEmpty()); }
        );
    }

    Dialog::~Dialog() {
    }

    auto Dialog::SetName(const QString& name) -> void {
        d->ui.name->setText(name);
    }

    auto Dialog::GetName() const -> QString {
        return d->ui.name->text();
    }

    auto Dialog::SetID(const QString& id) -> void {
        d->ui.id->setText(id);
    }

    auto Dialog::GetID() const -> QString {
        return d->ui.id->text();
    }

    auto Dialog::GetPassword() const -> QString {
        return d->ui.password->text();
    }

    auto Dialog::SetPrivilege(const AppEntity::Profile profile) -> void {
        d->ui.privilege->setCurrentText(profile._to_string());
    }

    auto Dialog::GetPrivilege() const -> AppEntity::Profile {
        return AppEntity::Profile::_from_integral(d->ui.privilege->currentData().toInt());
    }

    auto Dialog::SetExistIDs(const QStringList& existIDs) -> void {
        d->existIDs = existIDs;
    }

    void Dialog::done(int retCode) {
        if(retCode == QDialog::Accepted) {
            if(!d->IsValidID(d->ui.id->text())) {
                d->ui.id->setFocus();
                return;
            }

            bool confirmPassword = false;
            if(d->mode == Mode::New) {
                if(d->ui.password->text().isEmpty()) {
                    d->ui.message->setText(tr("Password is blank"));
                    return;
                }

                confirmPassword = true;
            } else {
                confirmPassword = !d->ui.password->text().isEmpty();
            }

            if(confirmPassword) {
                if(d->ui.password->text() != d->ui.password2->text()) {
                    d->ui.message->setText(tr("Please enter password again"));
                    return;
                }
            }
        }

        QDialog::done(retCode);
    }

    void Dialog::keyPressEvent(QKeyEvent* event) {
        switch (event->key()) {
		case Qt::Key_Enter:
		case Qt::Key_Return:
            return;
		}
        CustomDialog::keyPressEvent(event);
    }

    void Dialog::focusInEvent(QFocusEvent* event) {
        Q_UNUSED(event)
        d->ui.name->setFocus(Qt::FocusReason::TabFocusReason);
    }
    
    auto Dialog::Impl::IsValidID(const QString& id) const -> bool {
        using Validator = TC::FileNameValidator;
        using ErrorReason = TC::FileNameValid;

        QString errorMessage{};
        bool result{true};

        Validator validator;
        validator.AddInavlidCharacter('.');
        validator.SetMinLength(1);
        validator.SetMaxLength(16);

        if (!validator.IsValid(id)) {
            result = false;

            switch (validator.GetError()) {
                case ErrorReason::EmptyStringError: 
                    errorMessage = tr("User ID is blank.");
                    break;
                case ErrorReason::MinLengthError: 
                    errorMessage = tr("Minimum length is %1.").arg(validator.GetMinLength());
                    break;
                case ErrorReason::MaxLenghtOverError:
                    errorMessage = tr("Maximum length is %1.").arg(validator.GetMaxLength());
                    break;
                case ErrorReason::ReservedNameError:
                    errorMessage = tr("The specified device name is invalid.");
                    break;
                case ErrorReason::InvalidCharacterContainedError: {
                    QString tmp = validator.GetInvalidCharacters();
                    tmp.replace(QString("\\"), QString(""));
                    QString invalidChars{};
                    for(const auto& c : tmp) {
                        invalidChars.append(c);
                        invalidChars.append(' ');
                    }
                    errorMessage = tr("ID can't contain following characters:\n%1").arg(invalidChars);
                    break;
                }
                case ErrorReason::WhitespaceAtTheEndError:
                    errorMessage = tr("Remove the whitespace at the end.");
                    break;
                case ErrorReason::UnknownError:
                    errorMessage = tr("An unknown error occurred.\nError code: %1").arg(ErrorReason::UnknownError);
                    break;
                default:
                    errorMessage = tr("An unknown error occurred.\nError code: %1").arg(ErrorReason::UnknownError+1);
                    break;
            }
        }
        else {
            const auto exist = existIDs.contains(id, Qt::CaseInsensitive);
            if(exist) {
                result = false;
                errorMessage = tr("'%1' already exists.").arg(id);
            }
        }

        if (!errorMessage.isEmpty()) {
            ui.message->setText(errorMessage);
        }

        return result;
    }
}
