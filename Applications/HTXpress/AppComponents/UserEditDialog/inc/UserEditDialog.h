#pragma once
#include <memory>
#include <QDialog>

#include <User.h>

#include "CustomDialog.h"
#include "HTXUserEditDialogExport.h"

namespace HTXpress::AppComponents::UserEdit {
    class HTXUserEditDialog_API Dialog : public TC::CustomDialog {
        Q_OBJECT

    public:
        enum class Mode {
            New,
            Edit
        };

    public:
        Dialog(QWidget* parent, Mode mode = Mode::New);
        ~Dialog() override;

        auto SetName(const QString& name)->void;
        auto GetName() const->QString;

        auto SetID(const QString& id)->void;
        auto GetID() const->QString;

        auto GetPassword() const->QString;

        auto SetPrivilege(const AppEntity::Profile profile)->void;
        auto GetPrivilege() const->AppEntity::Profile;

        auto SetExistIDs(const QStringList& existIDs)->void;

    public slots:
        void done(int retCode) override;

    protected:
        void keyPressEvent(QKeyEvent* event) override;
        void focusInEvent(QFocusEvent* event) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}