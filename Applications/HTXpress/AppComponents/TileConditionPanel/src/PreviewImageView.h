﻿#pragma once

#include <memory>

#include <QGraphicsView>

namespace HTXpress::AppComponents::TileConditionPanel {
    class PreviewImageView : public QGraphicsView{
        Q_OBJECT
    public:
        using Self = PreviewImageView;
        using Pointer = std::shared_ptr<Self>;

        explicit PreviewImageView(QWidget* parent = nullptr);
        ~PreviewImageView() override;

        auto Init() -> void;

    protected:
        auto resizeEvent(QResizeEvent* event) -> void override;

    public slots:
        void onShowGrid(bool show);
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
