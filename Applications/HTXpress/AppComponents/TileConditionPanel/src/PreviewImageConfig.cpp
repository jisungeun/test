﻿#include <QImage>

#include "PreviewImageConfig.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    struct PreviewImageConfig::Impl {
        QImage image;
        double width{0.0};
        double height{0.0};
        double x{0.0};
        double y{0.0};
    };

    PreviewImageConfig::PreviewImageConfig() : d{std::make_unique<Impl>()} {
    }

    PreviewImageConfig::PreviewImageConfig(const PreviewImageConfig& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    PreviewImageConfig::~PreviewImageConfig() {
    }

    auto PreviewImageConfig::operator=(const PreviewImageConfig& other) -> PreviewImageConfig& {
        *d = *other.d;
        return *this;
    }

    auto PreviewImageConfig::SetPreviewImage(const QImage& image) -> void {
        d->image = image;
    }

    auto PreviewImageConfig::GetPreviewImage() const -> QImage& {
        return d->image;
    }

    auto PreviewImageConfig::SetSize(const double& width, const double& height) -> void {
        if (width < 0 && height < 0) { return; }

        d->width = width;
        d->height = height;
    }

    auto PreviewImageConfig::GetSize() const -> std::pair<double, double> {
        return {d->width, d->height};
    }

    auto PreviewImageConfig::GetWidth() const -> double {
        return d->width;
    }

    auto PreviewImageConfig::GetHeight() const -> double {
        return d->height;
    }

    auto PreviewImageConfig::SetPos(const double& x, const double& y) -> void {
        d->x = x;
        d->y = y;
    }

    auto PreviewImageConfig::GetPos() const -> std::pair<double, double> {
        return {d->x, d->y};
    }

    auto PreviewImageConfig::GetX() const -> double {
        return d->x;
    }

    auto PreviewImageConfig::GetY() const -> double {
        return d->y;
    }
}
