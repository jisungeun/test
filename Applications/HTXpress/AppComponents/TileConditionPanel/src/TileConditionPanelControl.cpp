#include <any>

#include <QImage>
#include <QDebug>

#include "TileConditionPanelControl.h"
#include "TileConditionPanelDataRepo.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    struct TileConditionPanelControl::Impl {
        TileConditionPanelDataRepo::Pointer dataRepo{TileConditionPanelDataRepo::GetInstnace()};

        struct TableInformation {
            QMap<int32_t, QPair<QString, QVariant>> values;
            const int32_t dataCount{1};
        } tableInfo;

        auto InitTileInfo() -> void;
    };

    auto TileConditionPanelControl::Impl::InitTileInfo() -> void {
        const auto tile = dataRepo->GetTileConfig();
        tableInfo.values[0] = {"ID", tile.GetID()};
        tableInfo.values[1] = {"Size", QString(QString::number(tile.GetWidth()) + " x " + QString::number(tile.GetHeight()))};
        tableInfo.values[2] = {"Tile #", QString(QString::number(tile.GetColumns()) + " x " + QString::number(tile.GetRows()))};
        tableInfo.values[3] = {"X", QString::number(tile.GetX())};
        tableInfo.values[4] = {"Y", QString::number(tile.GetY())};
        tableInfo.values[5] = {"Z", QString::number(tile.GetZ())};
    }

    TileConditionPanelControl::TileConditionPanelControl() : d{std::make_unique<Impl>()} {
    }

    TileConditionPanelControl::~TileConditionPanelControl() {
    }

    auto TileConditionPanelControl::SetConfigs(const TileConditionConfig& tile, const PreviewImageConfig& preview) -> void {
        d->dataRepo->SetTileConfig(tile);
        d->dataRepo->SetPreviewConfig(preview);

        d->InitTileInfo();
    }

    auto TileConditionPanelControl::GetTableColumnCount() const -> int32_t {
        return d->tableInfo.values.size();
    }

    auto TileConditionPanelControl::GetTableHeaderLabels() const -> QStringList {
        QStringList labels;
        for (const auto pair : d->tableInfo.values) {
            labels << pair.first;
        }
        return labels;
    }

    auto TileConditionPanelControl::GetTableRowCount() const -> int32_t {
        return d->tableInfo.dataCount;
    }

    auto TileConditionPanelControl::GetTableValue(int32_t column) const -> QString {
        for (auto it = d->tableInfo.values.begin(); it != d->tableInfo.values.end(); ++it) {
            if (it.key() == column) {
                return it.value().second.toString();
            }
        }
        return {};
    }

    auto TileConditionPanelControl::IsImageNull() -> bool {
        return d->dataRepo->GetPreviewConfig().GetPreviewImage().isNull();
    }
}
