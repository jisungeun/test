﻿#pragma once

#include <any>
#include <memory>
#include <QGraphicsItem>

namespace HTXpress::AppComponents::TileConditionPanel {
    struct ItemValues;
    struct GridItemValues;

    class GraphicsItemCreator {
    public:
        enum {
            ImageItem = QGraphicsItem::UserType + 1,
            GridItem
        };

        using Self = GraphicsItemCreator;
        using Pointer = std::shared_ptr<Self>;

        GraphicsItemCreator();
        ~GraphicsItemCreator();

        [[nodiscard]] auto CreateGraphicsItem(int32_t type, std::any data) -> QGraphicsItem*;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    struct PreviewItemValues {
        QPixmap pixmap;
        QRectF boundingRect;
    };

    struct GridItemValues {
        QRectF boundingRect;
        QPointF pos;
        double penWidth{1.0};
    };
}
