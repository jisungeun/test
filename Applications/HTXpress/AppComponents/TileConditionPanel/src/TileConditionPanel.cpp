#include <QDebug>

#include "TileConditionPanel.h"

#include <QLabel>

#include "ui_TileConditionPanel.h"

#include "TileConditionPanelControl.h"
#include "PreviewImageView.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    struct TileConditionPanel::Impl {
        Ui::TileConditionPanel ui;
        TileConditionPanelControl control;

        PreviewImageView* view{nullptr};

        auto InitWidget(Self* me) -> void;
        auto Update() -> void;

        // update
        auto InitGraphicsView() -> void;
        auto InitNoImageView() -> void;
        auto UpdateTable() -> void;
    };

    TileConditionPanel::TileConditionPanel(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->InitWidget(this);
    }

    TileConditionPanel::TileConditionPanel(TileConditionConfig tile, PreviewImageConfig preview, QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->InitWidget(this);
        ShowTileCondition(tile, preview);
    }

    TileConditionPanel::~TileConditionPanel() {
    }

    auto TileConditionPanel::ShowTileCondition(TileConditionConfig tile, PreviewImageConfig preview) -> void {
        const auto title = "Tile Condition - " + tile.GetID();
        setWindowTitle(title);

        d->control.SetConfigs(tile, preview);
        d->Update();

        show();
    }

    auto TileConditionPanel::Impl::InitWidget(Self* me) -> void {
        ui.setupUi(me);
        me->setAttribute(Qt::WA_DeleteOnClose);
        connect(ui.btnClose, &QPushButton::clicked, [=]() { me->close(); });
    }

    auto TileConditionPanel::Impl::InitGraphicsView() -> void {
        view = new PreviewImageView();
        auto layout = new QGridLayout;
        layout->addWidget(view);
        layout->setContentsMargins(12, 8, 12, 0);
        ui.viewFrame->setLayout(layout);

        view->Init();
        connect(ui.showSingleTile, &QCheckBox::toggled, view, &PreviewImageView::onShowGrid);
    }

    auto TileConditionPanel::Impl::InitNoImageView() -> void {
        auto layout = new QGridLayout;
        layout->addWidget(new QLabel("There is no preview image..."));
        ui.viewFrame->setLayout(layout);
    }

    auto TileConditionPanel::Impl::Update() -> void {
        UpdateTable();
        if (false == control.IsImageNull()) {
            InitGraphicsView();
        }
        else {
            InitNoImageView();
        }
    }

    auto TileConditionPanel::Impl::UpdateTable() -> void {
        Q_ASSERT(control.GetTableRowCount() == 1);

        ui.tileInformationTable->setColumnCount(control.GetTableColumnCount());
        ui.tileInformationTable->setHorizontalHeaderLabels(control.GetTableHeaderLabels());
        ui.tileInformationTable->setRowCount(control.GetTableRowCount());
        ui.tileInformationTable->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        ui.tileInformationTable->verticalHeader()->setHidden(true);
        ui.tileInformationTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);

        for (int32_t col = 0; col < ui.tileInformationTable->columnCount(); ++col) {
            auto item = new QTableWidgetItem(control.GetTableValue(col));
            item->setFlags(Qt::ItemIsEnabled);
            item->setTextAlignment(Qt::AlignCenter);
            ui.tileInformationTable->setItem(0, col, item);
        }
    }
}
