﻿#include "TileConditionPanelDataRepo.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    struct TileConditionPanelDataRepo::Impl {
        TileConditionConfig tileConfig;
        PreviewImageConfig previewConfig;
    };

    TileConditionPanelDataRepo::TileConditionPanelDataRepo() : d{std::make_unique<Impl>()} {
    }

    auto TileConditionPanelDataRepo::GetInstnace() -> Pointer {
        static Pointer theInstance{new TileConditionPanelDataRepo()};
        return theInstance;
    }

    TileConditionPanelDataRepo::~TileConditionPanelDataRepo() {
    }

    auto TileConditionPanelDataRepo::SetTileConfig(const TileConditionConfig& tile) -> void {
        d->tileConfig = tile;
    }

    auto TileConditionPanelDataRepo::GetTileConfig() const -> TileConditionConfig& {
        return d->tileConfig;
    }

    auto TileConditionPanelDataRepo::SetPreviewConfig(const PreviewImageConfig& preview) -> void {
        d->previewConfig = preview;
    }

    auto TileConditionPanelDataRepo::GetPreviewConfig() const -> PreviewImageConfig& {
        return d->previewConfig;
    }
}
