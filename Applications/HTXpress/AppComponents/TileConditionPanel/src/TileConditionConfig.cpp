﻿#include <QString>

#include "TileConditionConfig.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    struct TileConditionConfig::Impl {
        QString id{""};

        double width{0.0};
        double height{0.0};

        double x{0.0};
        double y{0.0};
        double z{0.0};

        int32_t rows{0};
        int32_t columns{0};

        double overlap{0.0};
    };

    TileConditionConfig::TileConditionConfig() : d{std::make_unique<Impl>()} {
    }

    TileConditionConfig::TileConditionConfig(const QString& id, double width, double height, int32_t rows, int32_t columns, double x, double y, double z, double overlap) : d{std::make_unique<Impl>()} {
        d->id = id;

        if (width > 0 && height > 0) {
            d->width = width;
            d->height = height;
        }

        d->x = x;
        d->y = y;
        d->z = z;

        if (rows > 0 && columns > 0) {
            d->rows = rows;
            d->columns = columns;
        }

        d->overlap = overlap;
    }

    TileConditionConfig::TileConditionConfig(const TileConditionConfig& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    TileConditionConfig::~TileConditionConfig() {
    }

    auto TileConditionConfig::operator=(const TileConditionConfig& other) -> TileConditionConfig& {
        *d = *other.d;
        return *this;
    }

    auto TileConditionConfig::SetID(const QString& id) -> void {
        d->id = id;
    }

    auto TileConditionConfig::GetID() const -> QString& {
        return d->id;
    }

    auto TileConditionConfig::SetPos(double x, double y, double z) -> void {
        d->x = x;
        d->y = y;
        d->z = z;
    }

    auto TileConditionConfig::GetPos() const -> Position3D {
        return {d->x, d->y, d->z};
    }

    auto TileConditionConfig::GetX() const -> double {
        return d->x;
    }

    auto TileConditionConfig::GetY() const -> double {
        return d->y;
    }

    auto TileConditionConfig::GetZ() const -> double {
        return d->z;
    }

    auto TileConditionConfig::SetSize(double w, double h) -> void {
        if (w > 0 && h > 0) {
            d->width = w;
            d->height = h;
        }
    }

    auto TileConditionConfig::GetSize() const -> std::pair<double, double> {
        return {d->width, d->height};
    }

    auto TileConditionConfig::GetWidth() const -> double {
        return d->width;
    }

    auto TileConditionConfig::GetHeight() const -> double {
        return d->height;
    }

    auto TileConditionConfig::SetCount(int32_t rows, int32_t columns) -> void {
        if (rows > 0 && columns > 0) {
            d->rows = rows;
            d->columns = columns;
        }
    }

    auto TileConditionConfig::GetCount() const -> std::pair<int32_t, int32_t> {
        return {d->rows, d->columns};
    }

    auto TileConditionConfig::GetRows() const -> int32_t {
        return d->rows;
    }

    auto TileConditionConfig::GetColumns() const -> int32_t {
        return d->columns;
    }

    auto TileConditionConfig::SetOverlap(double overlap) -> void {
        d->overlap = overlap;
    }

    auto TileConditionConfig::GetOverlap() const -> double {
        return d->overlap;
    }
}
