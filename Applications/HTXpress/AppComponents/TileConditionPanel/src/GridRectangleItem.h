﻿#pragma once

#include <QGraphicsRectItem>

namespace HTXpress::AppComponents::TileConditionPanel {
    class GridRectangleItem : public QGraphicsRectItem {
    public:
        enum {
            GridRectangleType = UserType+2
        };

        using Self = GridRectangleItem;
        using Pointer = std::shared_ptr<Self>;

        GridRectangleItem(const QRectF& rect, QGraphicsItem* parent = nullptr);
        ~GridRectangleItem() override;

        auto SetType(int32_t type) -> void;
        auto SetPenWidth(double width) -> void;

    protected:
        auto type() const -> int override;
        auto boundingRect() const -> QRectF override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
