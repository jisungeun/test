﻿#pragma once

#include <memory>

#include <QPixmap>
#include <QCoreApplication>

#include "GraphicsItemCreator.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    class PreviewImageViewControl {
        Q_DECLARE_TR_FUNCTIONS(PreviewImageViewControl)

    public:
        using Self = PreviewImageViewControl;
        using Pointer = std::shared_ptr<Self>;

        PreviewImageViewControl();
        ~PreviewImageViewControl();

        auto InitData() -> void;
        auto GetPixmapImage() const -> QPixmap&;
        auto GetImageRect() const -> QRectF;

        auto GetTileRowCount() const -> int32_t;
        auto GetTileColumnCount() const -> int32_t;
        auto GetTilePos(int32_t row, int32_t col) const -> QPointF;
        auto GetWholeTileRect() -> QRectF;
        auto GetTileRect() -> QRectF;
        auto GetGridPenWidth() const -> double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
