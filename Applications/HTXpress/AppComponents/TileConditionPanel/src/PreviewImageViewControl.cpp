﻿#define LOGGER_TAG "[TileConditionPanel_PreviewImageViewControl]"
#include <TCLogger.h>
#include <MessageDialog.h>

#include "PreviewImageViewControl.h"
#include "TileConditionPanelDataRepo.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    struct PreviewImageViewControl::Impl {
        TileConditionPanelDataRepo::Pointer dataRepo{TileConditionPanelDataRepo::GetInstnace()};
        QPixmap pixmap;
        auto CalculateBoundRect(double x, double y, double w, double h) const -> QRectF;
        auto SetTilePositions() -> void;

        struct TilePosition {
            int32_t row;
            int32_t col;
            QPointF pos;
        };

        QPair<double, double> ratio; // w ratio, h ratio
        QPair<int32_t, int32_t> tileCount; // rows, columns
        QPair<double, double> tileCenterPos; // x, y
        QPair<double, double> tileWholeSize; // width, height
        QPair<double, double> tileOneSize; // width, height
        QList<TilePosition> tilePositions;

        QRectF oneTileRect;
        QRectF wholeTIleRect;
    };

    auto PreviewImageViewControl::Impl::CalculateBoundRect(double x, double y, double w, double h) const -> QRectF {
        const auto left = -w / 2 + x;
        const auto top = -h / 2 - y;
        return {left, top, w, h};
    }

    auto PreviewImageViewControl::Impl::SetTilePositions() -> void {
        const auto leftX = wholeTIleRect.left() + tileOneSize.first / 2;
        const auto topY = wholeTIleRect.top() + tileOneSize.second / 2;
        const auto rightX = wholeTIleRect.right() - tileOneSize.first / 2;
        const auto bottomY = wholeTIleRect.bottom() - tileOneSize.second / 2;

        for (auto row = 0; row < tileCount.first; row++) {
            for (auto col = 0; col < tileCount.second; col++) {
                const auto xDenom = (tileCount.second-1 == 0)? 1 : tileCount.second-1;
                const auto yDenom = (tileCount.first-1 == 0) ? 1 : tileCount.first-1;
                const auto x = leftX + (col * ((rightX - leftX) / xDenom));
                const auto y = topY - (row * ((topY - bottomY) / yDenom));
                tilePositions.push_back({row, col, {x, y}});
            }
        }
    }

    PreviewImageViewControl::PreviewImageViewControl() : d{std::make_unique<Impl>()} {
    }

    PreviewImageViewControl::~PreviewImageViewControl() {
    }

    auto PreviewImageViewControl::InitData() -> void {
        const auto preview = d->dataRepo->GetPreviewConfig();
        const auto tile = d->dataRepo->GetTileConfig();

        d->tileCount = {tile.GetRows(), tile.GetColumns()};

        const auto img = preview.GetPreviewImage();
        d->pixmap = QPixmap::fromImage(img);

        d->ratio = {img.width() / preview.GetWidth(), img.height() / preview.GetHeight()};

        const auto overlapWidth = (tile.GetColumns()==1) ? 0 : tile.GetOverlap() * d->ratio.first;
        const auto overlapHeight = (tile.GetRows()==1) ? 0 : tile.GetOverlap() * d->ratio.second;

        const auto tileW = tile.GetWidth() * d->ratio.first;
        const auto tileH = tile.GetHeight() * d->ratio.second;

        d->tileWholeSize = {tileW, tileH};
        d->tileOneSize = {(tileW / d->tileCount.second)+overlapWidth, (tileH / d->tileCount.first)+overlapHeight};

        const auto tileX = ((tile.GetX() - preview.GetX())) * d->ratio.first;
        const auto tileY = ((tile.GetY() - preview.GetY())) * d->ratio.second;

        d->tileCenterPos = {tileX, tileY};
        d->wholeTIleRect = d->CalculateBoundRect(d->tileCenterPos.first, d->tileCenterPos.second, d->tileWholeSize.first, d->tileWholeSize.second);
        d->oneTileRect = d->CalculateBoundRect(0, 0, d->tileOneSize.first, d->tileOneSize.second);
        d->SetTilePositions();
    }

    auto PreviewImageViewControl::GetPixmapImage() const -> QPixmap& {
        return d->pixmap;
    }

    auto PreviewImageViewControl::GetImageRect() const -> QRectF {
        return d->CalculateBoundRect(0, 0, d->pixmap.width(), d->pixmap.height());
    }

    auto PreviewImageViewControl::GetTileRowCount() const -> int32_t {
        return d->tileCount.first;
    }

    auto PreviewImageViewControl::GetTileColumnCount() const -> int32_t {
        return d->tileCount.second;
    }

    auto PreviewImageViewControl::GetTilePos(int32_t row, int32_t col) const -> QPointF {
        for (const auto& tilePos : d->tilePositions) {
            if (tilePos.row == row && tilePos.col == col) {
                return tilePos.pos;
            }
        }
        QLOG_ERROR() << QString("Failed to get tile position. Invalid RowColIndex: row %1, col %2.").arg(row).arg(col);
        TC::MessageDialog::warning(nullptr, tr("Invalid index"), tr("Index well index(r:%1, c:%2)").arg(row).arg(col));

        // invalid value return
        return {DBL_MAX, DBL_MAX};
    }

    auto PreviewImageViewControl::GetWholeTileRect() -> QRectF {
        return d->wholeTIleRect;
    }

    auto PreviewImageViewControl::GetTileRect() -> QRectF {
        return d->oneTileRect;
    }

    auto PreviewImageViewControl::GetGridPenWidth() const -> double {
        return d->ratio.first > d->ratio.second ? d->ratio.first : d->ratio.second;
    }
}
