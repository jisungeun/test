﻿#pragma once

#include <memory>

#include "TileConditionConfig.h"
#include "PreviewImageConfig.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    class TileConditionPanelDataRepo {
    public:
        using Self = TileConditionPanelDataRepo;
        using Pointer = std::shared_ptr<Self>;

    protected:
        TileConditionPanelDataRepo();

    public:
        static auto GetInstnace() -> Pointer;
        ~TileConditionPanelDataRepo();

        auto SetTileConfig(const TileConditionConfig& tile) -> void;
        auto GetTileConfig() const -> TileConditionConfig&;

        auto SetPreviewConfig(const PreviewImageConfig& preview) -> void;
        auto GetPreviewConfig() const -> PreviewImageConfig&;

        auto IsImageNull() const -> bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
