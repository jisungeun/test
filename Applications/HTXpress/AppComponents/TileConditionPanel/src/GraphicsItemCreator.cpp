﻿#include "GraphicsItemCreator.h"
#include "PreviewImageItem.h"
#include "GridRectangleItem.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    struct GraphicsItemCreator::Impl {

    };

    GraphicsItemCreator::GraphicsItemCreator() : d{std::make_unique<Impl>()} {
    }

    GraphicsItemCreator::~GraphicsItemCreator() {
    }

    auto GraphicsItemCreator::CreateGraphicsItem(int32_t type, std::any data) -> QGraphicsItem* {
        switch (type) {
            case ImageItem: {
                const auto previewData = std::any_cast<PreviewItemValues>(data);
                const auto item = new PreviewImageItem();

                item->SetPixmap(previewData.pixmap);
                item->SetBoundingRect(previewData.boundingRect);
                item->SetType(ImageItem);

                return item;
            }
            case GridItem : {
                const auto gridData = std::any_cast<GridItemValues>(data);
                const auto item = new GridRectangleItem(gridData.boundingRect);

                item->SetType(GridItem);
                item->SetPenWidth(gridData.penWidth);
                item->setPos(gridData.pos);

                return item;
            }
            default: ;
        }
        return nullptr;
    }
}
