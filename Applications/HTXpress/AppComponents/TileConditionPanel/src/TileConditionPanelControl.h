#pragma once

#include <memory>

#include "PreviewImageConfig.h"
#include "TileConditionConfig.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    class TileConditionPanelControl {
    public:
        using Self = TileConditionPanelControl;
        using Pointer = std::shared_ptr<Self>;

        TileConditionPanelControl();
        ~TileConditionPanelControl();

        auto SetConfigs(const TileConditionConfig& tile, const PreviewImageConfig& preview) -> void;
        auto GetTableColumnCount() const -> int32_t;
        auto GetTableHeaderLabels() const -> QStringList;
        auto GetTableRowCount() const -> int32_t;
        auto GetTableValue(int32_t column) const -> QString;

        auto IsImageNull() -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
