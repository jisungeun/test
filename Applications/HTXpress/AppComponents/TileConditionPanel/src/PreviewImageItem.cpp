﻿#include <QDebug>
#include <QPainter>

#include "PreviewImageItem.h"

#include <QRandomGenerator>

namespace HTXpress::AppComponents::TileConditionPanel {
    struct PreviewImageItem::Impl {
        QRectF boundingRect{0, 0, 0, 0};
        QPixmap pixmap;
        int32_t type{-1};
    };

    PreviewImageItem::PreviewImageItem(QGraphicsItem* parent) : QGraphicsItem(parent), d{std::make_unique<Impl>()} {

    }

    PreviewImageItem::PreviewImageItem(const QPixmap& pixmap, QGraphicsItem* parent) : QGraphicsItem(parent), d{std::make_unique<Impl>()} {
        d->pixmap = pixmap;
    }

    PreviewImageItem::~PreviewImageItem() {
    }

    auto PreviewImageItem::SetPixmap(const QPixmap& pixmap) -> void {
        d->pixmap = pixmap;
    }

    auto PreviewImageItem::SetBoundingRect(const QRectF& rectF) -> void {
        d->boundingRect = rectF;
    }

    auto PreviewImageItem::SetType(int32_t type) -> void {
        d->type = type;
    }

    void PreviewImageItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
        Q_UNUSED(option)
        Q_UNUSED(widget)
        painter->drawPixmap(boundingRect().toRect(), d->pixmap);
    }

    QRectF PreviewImageItem::boundingRect() const {
        return d->boundingRect;
    }

    auto PreviewImageItem::type() const -> int {
        return d->type;
    }
}
