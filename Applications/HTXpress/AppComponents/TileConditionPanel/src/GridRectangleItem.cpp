﻿#include "GridRectangleItem.h"

#include <QPainter>

namespace HTXpress::AppComponents::TileConditionPanel {
    struct GridRectangleItem::Impl {
        QRectF rect;
        double penWidth{1};
        int32_t type{-1};
    };

    GridRectangleItem::GridRectangleItem(const QRectF& rect, QGraphicsItem* parent) : QGraphicsRectItem(parent), d{std::make_unique<Impl>()} {
        d->rect = rect;
        setVisible(false);
    }

    GridRectangleItem::~GridRectangleItem() {
    }

    auto GridRectangleItem::SetType(int32_t type) -> void {
        d->type = type;
    }

    auto GridRectangleItem::SetPenWidth(double width) -> void {
        d->penWidth = width;
    }

    int GridRectangleItem::type() const {
        return d->type;
    }

    QRectF GridRectangleItem::boundingRect() const {
        return d->rect;
    }

    void GridRectangleItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
        Q_UNUSED(option)
        Q_UNUSED(widget)

        painter->setPen(QPen(Qt::yellow, d->penWidth, Qt::DashLine));
        painter->drawRect(boundingRect());
    }
}
