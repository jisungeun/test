﻿#pragma once

#include <memory>

#include <QGraphicsItem>

namespace HTXpress::AppComponents::TileConditionPanel {
    class PreviewImageItem : public QGraphicsItem {
    public:
        using Self = PreviewImageItem;
        using Pointer = std::shared_ptr<Self>;

        explicit PreviewImageItem(QGraphicsItem* parent = nullptr);
        PreviewImageItem(const QPixmap& pixmap, QGraphicsItem* parent = nullptr);
        ~PreviewImageItem() override;

        auto SetPixmap(const QPixmap& pixmap) -> void;
        auto SetBoundingRect(const QRectF& rectF) -> void;
        auto SetType(int32_t type) -> void;

    protected:
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
        auto boundingRect() const -> QRectF override;
        auto type() const -> int override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
