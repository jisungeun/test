﻿#include <QResizeEvent>

#include "PreviewImageView.h"
#include "PreviewImageViewControl.h"
#include "GraphicsItemCreator.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    struct PreviewImageView::Impl {
        PreviewImageViewControl control;
        QGraphicsScene* scene{nullptr};
        bool showGrid{false};

        auto SetImageItem() -> void;
        auto FitInView(Self* me) -> void;
    };

    PreviewImageView::PreviewImageView(QWidget* parent) : QGraphicsView(parent), d{std::make_unique<Impl>()} {
        d->scene = new QGraphicsScene();
        d->scene->setSceneRect(-50, -50, 100, 100);
        setScene(d->scene);

        setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
        setOptimizationFlags(DontSavePainterState);
        setViewportUpdateMode(SmartViewportUpdate);
    }

    PreviewImageView::~PreviewImageView() {
    }

    auto PreviewImageView::Init() -> void {
        d->control.InitData();
        d->SetImageItem();
        d->FitInView(this);
    }

    void PreviewImageView::resizeEvent(QResizeEvent* event) {
        d->FitInView(this);
        QGraphicsView::resizeEvent(event);
    }

    void PreviewImageView::onShowGrid(bool show) {
        for (const auto item : d->scene->items()) {
            if (item->type() == GraphicsItemCreator::GridItem) {
                item->setVisible(show);
            }
        }
    }

    auto PreviewImageView::Impl::SetImageItem() -> void {
        GraphicsItemCreator creator;
        PreviewItemValues previewItemValues;
        previewItemValues.pixmap = control.GetPixmapImage();
        previewItemValues.boundingRect = control.GetImageRect();
        scene->addItem(creator.CreateGraphicsItem(GraphicsItemCreator::ImageItem, previewItemValues));
        scene->setSceneRect(control.GetImageRect());

        for (int row = 0; row < control.GetTileRowCount(); ++row) {
            for (int col = 0; col < control.GetTileColumnCount(); ++ col) {
                GridItemValues gridItemValues;
                gridItemValues.penWidth = control.GetGridPenWidth();
                gridItemValues.boundingRect = control.GetTileRect();
                gridItemValues.pos = control.GetTilePos(row, col);
                scene->addItem(creator.CreateGraphicsItem(GraphicsItemCreator::GridItem, gridItemValues));
            }
        }
    }

    auto PreviewImageView::Impl::FitInView(Self* me) -> void {
        me->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);
    }
}
