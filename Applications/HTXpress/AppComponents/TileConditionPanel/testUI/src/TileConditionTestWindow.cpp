#include <QtGui/QtGui>
#include <QFileDialog>

#include "TileConditionTestWindow.h"
#include "ui_TileConditionTestWindow.h"
#include "TileConditionPanel.h"
#include "TileConditionConfig.h"
#include "PreviewImageConfig.h"

namespace HTXpress::AppComponents::TileConditionPanel::TEST {
    const QString TestWindowPrefix = "TestWindow:";

    struct TileConditionTestWindow::Impl {
        explicit Impl(Self* self) : self(self) {
            ui.setupUi(self);
        }

        Self* self{};
        Ui::TestWindow ui;

        auto Connect() -> void;
        auto GetPreviewImageConfig() -> PreviewImageConfig;
        auto GetTileConditionConfig() -> TileConditionConfig;
    };

    auto TileConditionTestWindow::Impl::GetTileConditionConfig() -> TileConditionConfig {
        const auto id = ui.tileID->text();
        const auto x = ui.tileX->value();
        const auto y = ui.tileY->value();
        const auto z = ui.tileZ->value();
        const auto w = ui.tileW->value();
        const auto h = ui.tileH->value();
        const auto r = ui.tileRows->value();
        const auto c = ui.tileCols->value();
        const auto overlap = ui.tileMin->value();

        qDebug() << TestWindowPrefix << "TileConfig" << "id" << id << "w" << w << "h" << h << "r" << r << "c" << c << "x" << x << "y" << y << "z" << z << "overlap" << overlap;

        return TileConditionConfig(id, w, h, r, c, x, y, z, overlap);
    }

    auto TileConditionTestWindow::Impl::Connect() -> void {
        connect(ui.btnShow, &QPushButton::clicked, self, &Self::onShowTileCondition);
        connect(ui.btnLoadImage, &QPushButton::clicked, self, &Self::onLoadImage);
    }

    auto TileConditionTestWindow::Impl::GetPreviewImageConfig() -> PreviewImageConfig {
        PreviewImageConfig previewConfig;
        QSize imageSize{0, 0};

        const auto fileName = ui.imageFilePath->text();
        const QImage image(fileName);
        if (image.isNull() == false) {
            imageSize = image.size();
        }
        const auto x = ui.previewX->value();
        const auto y = ui.previewY->value();
        const auto w = ui.previewW->value();
        const auto h = ui.previewH->value();

        ui.textEdit->append("RequestShowTileCondition");
        ui.imageFilePath->setText(fileName);

        qDebug() << TestWindowPrefix << "imageSize:" << imageSize << "setPreviewValue(x,y,w,h)" << x << y << w << h;

        previewConfig.SetPreviewImage(image);
        previewConfig.SetPos(x, y);
        previewConfig.SetSize(w, h);

        return previewConfig;
    }

    TileConditionTestWindow::TileConditionTestWindow(QWidget* parent) : QMainWindow(parent), d{std::make_unique<Impl>(this)} {
        d->Connect();
    }

    TileConditionTestWindow::~TileConditionTestWindow() {
    }

    void TileConditionTestWindow::onShowTileCondition() {
        auto panel = new TileConditionPanel();
        panel->ShowTileCondition(d->GetTileConditionConfig(), d->GetPreviewImageConfig());
    }

    void TileConditionTestWindow::onLoadImage() {
        const auto fileName = QFileDialog::getOpenFileName(this, "Open", "C:\\Users\\HP\\Pictures", tr("All files (*.*);;BMP (*.bmp);;CUR (*.cur);;GIF (*.gif);;ICNS (*.icns);;ICO (*.ico);;JPEG (*.jpeg);;JPG (*.jpg);;PBM (*.pbm);;PGM (*.pgm);;PNG (*.png);;PPM (*.ppm);;SVG (*.svg);;SVGZ (*.svgz);;TGA (*.tga);;TIF (*.tif);;TIFF (*.tiff);;WBMP (*.wbmp);;WEBP (*.webp);;XBM (*.xbm);;XPM (*.xpm)"));
        d->ui.imageFilePath->setText(fileName);
    }
}
