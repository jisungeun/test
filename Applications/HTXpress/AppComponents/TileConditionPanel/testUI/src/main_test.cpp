#include "TileConditionTestWindow.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HTXpress::AppComponents::TileConditionPanel::TEST::TileConditionTestWindow w;
    w.show();
    return a.exec();
}
