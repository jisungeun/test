#pragma once

#include <memory>
#include <QtWidgets/QMainWindow>

namespace HTXpress::AppComponents::TileConditionPanel::TEST {
	class TileConditionTestWindow : public QMainWindow {
		Q_OBJECT
	public:
		using Self = TileConditionTestWindow;
		explicit TileConditionTestWindow(QWidget* parent = nullptr);
		~TileConditionTestWindow() override;

	private slots:
	    void onShowTileCondition();
		void onLoadImage();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
