#include <catch2/catch.hpp>

#include <QString>
#include <QDebug>
#include <QImage>
#include <QPixmap>

#include "PreviewImageConfig.h"
#include "TileConditionConfig.h"
#include "TileConditionPanel.h"

namespace HTXpress::AppComponents::TileConditionPanel::TEST {
    TEST_CASE("PreviewImage data set test") {
        PreviewImageConfig previewImageConfig;
        TileConditionConfig tileConditionConfig;
        QImage image(3, 3, QImage::Format_RGB32);
        QRgb value;

        value = qRgb(189, 149, 39); // 0xffbd9527
        image.setPixel(1, 1, value);

        value = qRgb(122, 163, 39); // 0xff7aa327
        image.setPixel(0, 1, value);
        image.setPixel(1, 0, value);

        value = qRgb(237, 187, 51); // 0xffedba31
        image.setPixel(2, 1, value);

        previewImageConfig.SetPreviewImage(image);
        previewImageConfig.SetPos(10.12, -20.2);
        previewImageConfig.SetSize(255.5, 128.2);

        qDebug() << Q_FUNC_INFO<<"previewImageConfig:" << previewImageConfig.GetSize() << previewImageConfig.GetPos();

        QImage img = previewImageConfig.GetPreviewImage();
        CHECK(img == image);
    }
}
