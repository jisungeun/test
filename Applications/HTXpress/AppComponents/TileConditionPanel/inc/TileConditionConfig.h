﻿#pragma once

#include <memory>
#include <iostream>
#include <QDebug>

#include "HTXTileConditionPanelExport.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    struct Position3D;

    class HTXTileConditionPanel_API TileConditionConfig {
    public:
        using Self = TileConditionConfig;
        using Pointer = std::shared_ptr<Self>;

        TileConditionConfig();
        TileConditionConfig(const QString& id, double width, double height, int32_t rows, int32_t columns, double x, double y, double z, double overlap);
        TileConditionConfig(const TileConditionConfig& other);
        ~TileConditionConfig();

        auto operator=(const TileConditionConfig& other) -> TileConditionConfig&;

        auto SetID(const QString& id) -> void;
        auto GetID() const -> QString&;

        auto SetPos(double x, double y, double z) -> void;
        auto GetPos() const -> Position3D;
        auto GetX() const -> double;
        auto GetY() const -> double;
        auto GetZ() const -> double;

        auto SetSize(double w, double h) -> void;
        auto GetSize() const -> std::pair<double, double>;
        auto GetWidth() const -> double;
        auto GetHeight() const -> double;

        auto SetCount(int32_t rows, int32_t columns) -> void;
        auto GetCount() const -> std::pair<int32_t, int32_t>;
        auto GetRows() const -> int32_t;
        auto GetColumns() const -> int32_t;

        auto SetOverlap(double overlap) -> void;
        auto GetOverlap() const -> double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    struct Position3D {
        double x;
        double y;
        double z;
    };
}
