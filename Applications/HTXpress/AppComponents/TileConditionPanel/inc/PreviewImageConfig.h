﻿#pragma once

#include <memory>

#include "HTXTileConditionPanelExport.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    class HTXTileConditionPanel_API PreviewImageConfig {
    public:
        using Self = PreviewImageConfig;
        using Pointer = std::shared_ptr<Self>;

        PreviewImageConfig();
        PreviewImageConfig(const PreviewImageConfig& other);
        ~PreviewImageConfig();

        auto operator=(const PreviewImageConfig& other) -> PreviewImageConfig&;

        auto SetPreviewImage(const QImage& image) -> void;
        auto GetPreviewImage() const -> QImage&;

        auto SetSize(const double& width, const double& height) -> void;
        auto GetSize() const -> std::pair<double, double>;
        auto GetWidth() const -> double;
        auto GetHeight() const -> double;

        auto SetPos(const double& x, const double& y) -> void;
        auto GetPos() const -> std::pair<double, double>;
        auto GetX() const -> double;
        auto GetY() const -> double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
