#pragma once

#include <memory>

#include <QWidget>

#include "HTXTileConditionPanelExport.h"
#include "PreviewImageConfig.h"
#include "TileConditionConfig.h"

namespace HTXpress::AppComponents::TileConditionPanel {
    class HTXTileConditionPanel_API TileConditionPanel : public QWidget {
        Q_OBJECT
    public:
        using Self = TileConditionPanel;
        using Pointer = std::shared_ptr<Self>;

        explicit TileConditionPanel(QWidget* parent = nullptr);
        TileConditionPanel(TileConditionConfig tile, PreviewImageConfig preview = {}, QWidget* parent = nullptr);
        ~TileConditionPanel() override;

        /**
         * \brief 파라미터값을 입력 후 호출하게되면 Tile Condition View를 띄운다.
         * \param tile - 타일 정보와 관련된 TileConditionConfig
         * \param preview - 프리뷰 이미지 정보와 관련된 PreviewImageConfig
         * \return void
         */
        auto ShowTileCondition(TileConditionConfig tile, PreviewImageConfig preview = {}) -> void;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
