#include "IMainWindowHTX.h"

namespace HTXpress::AppComponents::Framework {
    struct IMainWindowHTX::Impl {
        QString displyTitle;
        QString shortTitle;
    };

    IMainWindowHTX::IMainWindowHTX(const QString& displayTitle, const QString& shortTitle, QWidget* parent) : d{new Impl} {
        d->displyTitle = displayTitle;
        d->shortTitle = shortTitle;
    }

    IMainWindowHTX::~IMainWindowHTX() {
    }

    auto IMainWindowHTX::GetDisplayTitle() const -> QString {
        return d->displyTitle;
    }

    auto IMainWindowHTX::GetShortTitle() const -> QString {
        return d->shortTitle;
    }
}
