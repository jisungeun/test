#include "AppManagerHTX.h"
#include "AppInterfaceHTX.h"
#include "PackageManagerHTX.h"

//ctk
#include <ctkPluginFrameworkLauncher.h>
#include <ctkPluginContext.h>
#include <service/event/ctkEventAdmin.h>

#define LOGGER_TAG "[AppManagerHTX]"

#include <TCLogger.h>

namespace HTXpress::AppComponents::Framework {
	struct AppManagerHTX::Impl {
		PackageManagerHTX::Pointer package{nullptr};
	};
	AppManagerHTX::AppManagerHTX() : d{ new Impl } {
		d->package = std::make_shared<PackageManagerHTX>();
	}
	AppManagerHTX::~AppManagerHTX() {

	}
	IAppManager::Pointer AppManagerHTX::GetInstance() {
		static Pointer theInstance(new AppManagerHTX());
		return theInstance;
	}	
	auto AppManagerHTX::GetPackageManager() -> IPackageManager::Pointer {
		return d->package;
    }
}