#include "IMainWindowHTX.h"
#include "AppInterfaceHTX.h"

namespace HTXpress::AppComponents::Framework {
    struct AppInterfaceHTX::Impl {
        IMainWindowHTX* window{ nullptr };
    };
    
    AppInterfaceHTX::AppInterfaceHTX(IMainWindowHTX* window) : IAppInterface(), d{ new Impl } {
        d->window = window;
    }

    AppInterfaceHTX::~AppInterfaceHTX() {
    }

    auto AppInterfaceHTX::GetDisplayTitle() const -> QString {
        return d->window->GetDisplayTitle();
    }

    auto AppInterfaceHTX::GetShortTitle() const -> QString {
        return d->window->GetShortTitle();
    }

    auto AppInterfaceHTX::GetInterfaceWidget() -> IMainWindow* {
        return d->window;
    }

    auto AppInterfaceHTX::Open(const QString& path)->bool {
        return false;
    }
    
    auto AppInterfaceHTX::Execute(const QVariantMap& params)->bool {
        return d->window->Execute(params);
    }    
}