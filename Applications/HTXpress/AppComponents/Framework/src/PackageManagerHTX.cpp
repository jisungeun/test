#include "PackageManagerHTX.h"

#include <QStringList>

namespace HTXpress::AppComponents::Framework {
    struct PackageManagerHTX::Impl {
        
    };
    PackageManagerHTX::PackageManagerHTX() : d{ new Impl } {
        
    }
    PackageManagerHTX::~PackageManagerHTX() {
        
    }
    auto PackageManagerHTX::AcquirePackage() -> bool {
        return true;
    }
    auto PackageManagerHTX::GetActivation(const QString& key) -> bool {
        return true;
    }
    auto PackageManagerHTX::GetPackageAppList() -> QStringList {
        return QStringList();
    }
}