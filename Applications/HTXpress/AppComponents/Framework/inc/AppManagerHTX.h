#include <IAppManager.h>

#include "HTXFrameworkExport.h"

namespace HTXpress::AppComponents::Framework {
    class HTXFramework_API AppManagerHTX : public TC::Framework::IAppManager {
    public:        
        ~AppManagerHTX();

        static Pointer GetInstance();        

    protected:
        AppManagerHTX();
        auto GetPackageManager() -> IPackageManager::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}