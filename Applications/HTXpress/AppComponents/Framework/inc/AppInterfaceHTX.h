#pragma once

#include <memory>
#include <IAppInterface.h>

#include "HTXFrameworkExport.h"

namespace HTXpress::AppComponents::Framework {
    class IMainWindowHTX;

    class HTXFramework_API AppInterfaceHTX : public TC::Framework::IAppInterface {
        Q_OBJECT
        Q_INTERFACES(IAppInterface)
        
    public:
        AppInterfaceHTX(IMainWindowHTX* window);
        ~AppInterfaceHTX();

        auto GetDisplayTitle() const -> QString override;
        auto GetShortTitle() const -> QString override;
        auto GetInterfaceWidget()->IMainWindow* override;
        auto Open(const QString& path)->bool override;
        auto Execute(const QVariantMap& params)->bool override;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}
using namespace HTXpress::AppComponents::Framework;
Q_DECLARE_INTERFACE(AppInterfaceHTX, "AppInterfaceHTX");
