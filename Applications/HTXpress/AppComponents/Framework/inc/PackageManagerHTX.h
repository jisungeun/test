#pragma once

#include <IPackageManager.h>

#include "HTXFrameworkExport.h"

namespace HTXpress::AppComponents::Framework {
    class HTXFramework_API PackageManagerHTX : public TC::Framework::IPackageManager {
    public:
        PackageManagerHTX();
        ~PackageManagerHTX();
        auto GetPackageAppList() -> QStringList override;

    protected:
        auto GetActivation(const QString& key) -> bool override;
        auto AcquirePackage() -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;        
    };
}