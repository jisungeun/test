#pragma once
#include <memory>

#include <IMainWindow.h>

#include "HTXFrameworkExport.h"

namespace HTXpress::AppComponents::Framework {
	class HTXFramework_API IMainWindowHTX: public TC::Framework::IMainWindow {
		Q_OBJECT

	public:
		IMainWindowHTX(const QString& displayTitle, const QString& shortTitle, QWidget* parent = nullptr);
		virtual ~IMainWindowHTX();

		auto GetDisplayTitle() const->QString;
		auto GetShortTitle() const->QString;

		virtual auto Execute(const QVariantMap& params)->bool = 0;

	protected:
		auto TryActivate() -> bool override = 0;
		auto TryDeactivate() -> bool override = 0;
		auto IsActivate() -> bool override = 0;
		auto GetMetaInfo()->QVariantMap override = 0;


	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}