﻿#include <QMap>

#include "TCFDataRepo.h"

namespace HTXpress::AppComponents::ProcessedDataDefinition {
    struct TCFDataRepo::Impl {
        TCFList tcfList;
    };

    TCFDataRepo::TCFDataRepo() : d{std::make_unique<Impl>()} {
    }

    TCFDataRepo::~TCFDataRepo() = default;

    auto TCFDataRepo::GetInstance() -> Pointer {
        static Pointer theInstance{new TCFDataRepo()};
        return theInstance;
    }

    auto TCFDataRepo::AddData(const UserName& user, const ProjectName& project, const ExperimentName& experiment, const SpecimenName& specimen, const WellName& well, const TCFData::Pointer& data) const -> void {
        d->tcfList[user][project][experiment][specimen][well][data->GetName()] = data;
    }

    auto TCFDataRepo::RemoveData(const UserName& user, const ProjectName& project, const ExperimentName& experiment, const SpecimenName& specimen, const WellName& well, const QString& dataName) -> void {
        if (IsExist(user, project, experiment, specimen, well, dataName)) {
            d->tcfList[user][project][experiment][specimen][well][dataName]->SetStatus(TCFProcessingStatus::NotYet);
        }
    }

    auto TCFDataRepo::DeleteDataRootFolder(const UserName& user, const ProjectName& project, const ExperimentName& experiment, const SpecimenName& specimen, const WellName& well, const QString& dataName) -> void {
        if (IsExist(user, project, experiment, specimen, well, dataName)) {
            d->tcfList[user][project][experiment][specimen][well].remove(dataName);
        }
    }

    auto TCFDataRepo::GetExperimentList(const UserName& user, const ProjectName& project) const -> ExperimentList {
        if (false == d->tcfList.contains(user)) {
            return {};
        }
        if (false == d->tcfList[user].contains(project)) {
            return {};
        }

        return d->tcfList[user][project];
    }

    auto TCFDataRepo::GetSpecimenList(const UserName& user, const ProjectName& project, const ExperimentName& experiment) const -> SpecimenList {
        if (false == d->tcfList.contains(user)) {
            return {};
        }
        if (false == d->tcfList[user].contains(project)) {
            return {};
        }
        if (false == d->tcfList[user][project].contains(experiment)) {
            return {};
        }

        return d->tcfList[user][project][experiment];
    }

    auto TCFDataRepo::GetData(const UserName& user, const ProjectName& project, const ExperimentName& experiment, const SpecimenName& specimen, const WellName& well, const QString& dataName) const -> TCFData::Pointer {
        if (!IsExist(user, project, experiment, specimen, well, dataName)) {
            return {};
        }

        return d->tcfList[user][project][experiment][specimen][well][dataName];
    }

    auto TCFDataRepo::IsExist(const UserName& user, const ProjectName& project, const ExperimentName& experiment, const SpecimenName& specimen, const WellName& well, const QString& dataName) const -> bool {
        if (false == d->tcfList.contains(user)) {
            return false;
        }
        if (false == d->tcfList[user].contains(project)) {
            return false;
        }
        if (false == d->tcfList[user][project].contains(experiment)) {
            return false;
        }
        if (false == d->tcfList[user][project][experiment].contains(specimen)) {
            return false;
        }
        if (false == d->tcfList[user][project][experiment][specimen].contains(well)) {
            return false;
        }
        if (false == d->tcfList[user][project][experiment][specimen][well].contains(dataName)) {
            return false;
        }

        return true;
    }

    auto TCFDataRepo::Clear() -> void {
        d->tcfList.clear();
    }
}
