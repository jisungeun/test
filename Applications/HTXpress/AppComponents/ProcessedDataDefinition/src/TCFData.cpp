#include "TCFData.h"

#include <QDirIterator>
#include <QFileInfo>

namespace HTXpress::AppComponents::ProcessedDataDefinition {
    struct TCFData::Impl {
        QString path;
        QString name; // without file extension
        TCFProcessingStatus status = TCFProcessingStatus::NotYet;

        auto operator=(const Impl& other) -> Impl&;
        auto operator==(const Impl& other) const -> bool;
    };

    auto TCFData::Impl::operator=(const Impl& other) -> Impl& {
        path = other.path;
        name = other.name;
        status = other.status;
        return *this;
    }

    auto TCFData::Impl::operator==(const Impl& other) const -> bool {
        if (path != other.path)
            return false;
        if (name != other.name)
            return false;
        return true;
    }

    TCFData::TCFData() : d{new Impl} {
    }

    TCFData::~TCFData() {
    }

    auto TCFData::operator=(const TCFData& other) -> TCFData& {
        *d = *other.d;
        return *this;
    }

    auto TCFData::operator==(const TCFData& other) const -> bool {
        return *d == *other.d;
    }

    auto TCFData::SetPath(const QString& path) -> void {
        d->path = path;
    }

    auto TCFData::GetPath() const -> QString {
        return d->path;
    }

    auto TCFData::SetName(const QString& name) -> void {
        d->name = name;
    }

    auto TCFData::GetName() const -> QString {
        return d->name;
    }

    auto TCFData::SetStatus(const TCFProcessingStatus& status) const -> void {
        d->status = status;
    }

    auto TCFData::GetStatus() const -> TCFProcessingStatus {
        return d->status;
    }
}
