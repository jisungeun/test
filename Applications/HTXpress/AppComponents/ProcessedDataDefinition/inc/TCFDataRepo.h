﻿#pragma once

#include <memory>

#include <QList>

#include "HTXProcessedDataDefinitionExport.h"
#include "TCFData.h"

namespace HTXpress::AppComponents::ProcessedDataDefinition {
    class HTXProcessedDataDefinition_API TCFDataRepo {
    public:
        using Self = TCFDataRepo;
        using Pointer = std::shared_ptr<Self>;

        using UserName = QString;
        using ProjectName = QString;
        using ExperimentName = QString;
        using SpecimenName = QString;
        using WellName = QString;
        using DataName = QString;
        
        using DataList = QMap<DataName, TCFData::Pointer>;
        using WellList = QMap<WellName, DataList>;
        using SpecimenList = QMap<SpecimenName, WellList>;
        using ExperimentList = QMap<ExperimentName, SpecimenList>;
        using ProjectList = QMap<ProjectName, ExperimentList>;
        using TCFList = QMap<UserName, ProjectList>;

    private:
        TCFDataRepo();

    public:
        ~TCFDataRepo();

        static auto GetInstance() -> Pointer;

        auto AddData(const UserName& user, const ProjectName& project, const ExperimentName& experiment, const SpecimenName& specimen, const WellName& well, const TCFData::Pointer& data) const -> void;
        auto RemoveData(const UserName& user, const ProjectName& project, const ExperimentName& experiment, const SpecimenName& specimen, const WellName& well, const QString& dataName) -> void;
        auto DeleteDataRootFolder(const UserName& user, const ProjectName& project, const ExperimentName& experiment, const SpecimenName& specimen, const WellName& well, const QString& dataName) -> void;

        auto GetExperimentList(const UserName& user, const ProjectName& project) const -> ExperimentList;
        auto GetSpecimenList(const UserName& user, const ProjectName& project, const ExperimentName& experiment) const -> SpecimenList;
        auto GetData(const UserName& user, const ProjectName& project, const ExperimentName& experiment, const SpecimenName& specimen, const WellName& well, const QString& dataName) const -> TCFData::Pointer;

        auto IsExist(const UserName& user, const ProjectName& project, const ExperimentName& experiment, const SpecimenName& specimen, const WellName& well, const QString& dataName) const -> bool;

        auto Clear() -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
