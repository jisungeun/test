#pragma once
#include <memory>

#include <enum.h>

#include <QMetaType>

#include "HTXProcessedDataDefinitionExport.h"

namespace HTXpress::AppComponents::ProcessedDataDefinition {
    BETTER_ENUM(TCFProcessingStatus, uint8_t, Unknown, NotYet, InProgress, Completed)
    BETTER_ENUM(TCFFileType, int32_t, Single, Timelapse, OneCycle)

    class HTXProcessedDataDefinition_API TCFData {
    public:
        using Pointer = std::shared_ptr<TCFData>;

        TCFData();
        virtual ~TCFData();

        auto operator=(const TCFData& other) -> TCFData&;
        auto operator==(const TCFData& other) const -> bool;

        auto SetPath(const QString& path) -> void;
        auto GetPath() const -> QString;

        auto SetName(const QString& name) -> void;
        auto GetName() const -> QString;

        auto SetStatus(const TCFProcessingStatus& status) const -> void;
        auto GetStatus() const -> TCFProcessingStatus;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
