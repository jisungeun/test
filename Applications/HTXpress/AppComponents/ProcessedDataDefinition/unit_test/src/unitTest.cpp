#include <catch2/catch.hpp>

#include <memory>

#include <QMap>
#include <QString>

#include "TCFDataRepo.h"
#include "TCFData.h"

namespace HTXpress::AppComponents::ProcessedDataDefinition::UnitTest {
    TEST_CASE("TCFData interfaces") {
        TCFData data1;
        SECTION("SetPath and GetPath") {
            data1.SetPath("C:/path/data");
            REQUIRE(data1.GetPath() == "C:/path/data");
        }

        SECTION("SetName and GetName") {
            data1.SetName("DataName");
            REQUIRE(data1.GetName() == "DataName");
        }

        SECTION("SetStatus and GetStatus") {
            for (const auto& status : TCFProcessingStatus::_values()) {
                data1.SetStatus(status);
                REQUIRE(data1.GetStatus() == status);
            }
        }

        SECTION("Operator= and Operator==") {
            TCFData data2;
            data2 = data1;
            REQUIRE(data1 == data2);
        }
    }

    TEST_CASE("TCFDataRepo interfaces") {
        auto repo = TCFDataRepo::GetInstance();

        SECTION("Clear") {
            TCFData::Pointer data = std::make_shared<TCFData>();
            data->SetPath("C:/path/data");
            data->SetName("DataName");
            data->SetStatus(TCFProcessingStatus::InProgress);

            repo->AddData("user", "project", "experiment", "specimen", "well", data);
            REQUIRE(repo->IsExist("user", "project", "experiment", "specimen", "well", data->GetName()) == true);

            repo->Clear();
            REQUIRE(repo->IsExist("user", "project", "experiment", "specimen", "well", data->GetName()) == false);
        }

        SECTION("AddData and GetData") {
            TCFData::Pointer data = std::make_shared<TCFData>();
            data->SetPath("C:/path/data");
            data->SetName("DataName");
            data->SetStatus(TCFProcessingStatus::Completed);

            repo->AddData("user", "project", "experiment", "specimen", "well", data);

            auto dataFromRepo = repo->GetData("user", "project", "experiment", "specimen", "well", data->GetName());
            REQUIRE(dataFromRepo != nullptr);
            REQUIRE(*data == *dataFromRepo);

            repo->Clear();
        }

        SECTION("IsExist") {
            TCFData::Pointer data = std::make_shared<TCFData>();
            data->SetPath("C:/path/data");
            data->SetName("DataName");
            data->SetStatus(TCFProcessingStatus::Completed);

            repo->AddData("user", "project", "experiment", "specimen", "well", data);

            REQUIRE(repo->IsExist("user", "project", "experiment", "specimen", "well", data->GetName()) == true);

            repo->Clear();
        }

        SECTION("RemoveData and DeleteDataRootFolder") {
            TCFData::Pointer data = std::make_shared<TCFData>();
            data->SetPath("C:/path/data");
            data->SetName("DataName");
            data->SetStatus(TCFProcessingStatus::Completed);

            repo->AddData("user", "project", "experiment", "specimen", "well", data);

            repo->RemoveData("user", "project", "experiment", "specimen", "well", data->GetName());
            REQUIRE(repo->IsExist("user", "project", "experiment", "specimen", "well", data->GetName()) == true);
            REQUIRE(repo->GetData("user", "project", "experiment", "specimen", "well", data->GetName())->GetStatus() == +TCFProcessingStatus::NotYet);
            REQUIRE(data->GetStatus() == +TCFProcessingStatus::NotYet);

            repo->DeleteDataRootFolder("user", "project", "experiment", "specimen", "well", data->GetName());
            REQUIRE(repo->IsExist("user", "project", "experiment", "specimen", "well", data->GetName()) == false);

            repo->Clear();
        }

        SECTION("GetExperimentList and GetSpecimenList") {
            TCFData::Pointer data1 = std::make_shared<TCFData>();
            data1->SetPath("C:/path/data1");
            data1->SetName("DataName1");
            data1->SetStatus(TCFProcessingStatus::NotYet);

            TCFData::Pointer data2 = std::make_shared<TCFData>();
            data2->SetPath("C:/path/data2");
            data2->SetName("DataName2");
            data2->SetStatus(TCFProcessingStatus::Completed);

            repo->AddData("user", "project", "experiment1", "specimen1", "well", data1);
            repo->AddData("user", "project", "experiment1", "specimen2", "well", data2);
            repo->AddData("user", "project", "experiment2", "specimen1", "well", data1);

            auto experimentList = repo->GetExperimentList("user", "project");
            REQUIRE(experimentList.size() == 2);
            REQUIRE(experimentList.contains("experiment1") == true);
            REQUIRE(experimentList.contains("experiment2") == true);

            auto specimenList1 = repo->GetSpecimenList("user", "project", "experiment1");
            REQUIRE(specimenList1.size() == 2);
            REQUIRE(specimenList1.contains("specimen1") == true);
            REQUIRE(specimenList1.contains("specimen2") == true);

            auto specimenList2 = repo->GetSpecimenList("user", "project", "experiment2");
            REQUIRE(specimenList2.size() == 1);
            REQUIRE(specimenList2.contains("specimen1") == true);
            REQUIRE(specimenList2.contains("specimen2") == false);

            repo->Clear();
        }
    }
}
