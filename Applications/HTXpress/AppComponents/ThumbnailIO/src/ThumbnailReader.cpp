#include <QDir>
#include <QRegularExpression>
#include <QDateTime>

#include "ThumbnailReader.h"

namespace HTXpress::AppComponents::ThumbnailIO {
    struct ThumbnailReader::Impl {
        QString dataPath{};
        AppEntity::Modality::_enumerated modality;
        int32_t frameIndex{};

        QRegularExpression validExpression{R"(^(\w+\w+_\d\d\d\d)$)"};

        auto GetFrameIndexFromFileName(const QString& fileName) -> int32_t;
        auto GetModalityFromFileName(const QString& fileName) -> QString;
        auto Convert(const QString& modalityString) -> AppEntity::Modality;
        auto GetImageFile() const -> QString; // image file's full path
        auto GetThumbnailDirectory() const -> QString;
    };

    ThumbnailReader::ThumbnailReader() : d{std::make_unique<Impl>()} {
    }

    ThumbnailReader::~ThumbnailReader() = default;

    auto ThumbnailReader::SetDataPath(const QString& dataPath) -> void {
        d->dataPath = dataPath;
    }

    auto ThumbnailReader::SetFrameIndex(int32_t frameIndex) -> void {
        d->frameIndex = frameIndex;
    }

    auto ThumbnailReader::SetModality(AppEntity::Modality modality) -> void {
        d->modality = modality;
    }

    auto ThumbnailReader::GetImage() const -> std::shared_ptr<QImage> {
        auto image = std::make_shared<QImage>(d->GetImageFile());
        return image;
    }

    auto ThumbnailReader::GetLastImageInfo() const -> std::tuple<int32_t, AppEntity::Modality, std::shared_ptr<QImage>> {
        const QDir dir(d->GetThumbnailDirectory());

        if (dir.isEmpty()) return {0, AppEntity::Modality::HT, std::make_shared<QImage>()};

        QFileInfo theLatestFileInfo;
        for (const auto& file : dir.entryInfoList(QDir::Files)) {
            if (d->validExpression.match(file.baseName(), 0).hasMatch()) {
                if (theLatestFileInfo.lastModified() < file.lastModified()) {
                    theLatestFileInfo = file;
                }
            }
        }
        auto image = std::make_shared<QImage>(theLatestFileInfo.filePath());
        auto frameIndex = d->GetFrameIndexFromFileName(theLatestFileInfo.baseName());
        auto modality = d->Convert(d->GetModalityFromFileName(theLatestFileInfo.baseName()));

        return {frameIndex, modality, image};
    }

        auto ThumbnailReader::Impl::GetFrameIndexFromFileName(const QString& fileName) -> int32_t {
        return fileName.split("_").last().toInt();
    }

    auto ThumbnailReader::Impl::GetModalityFromFileName(const QString& fileName) -> QString {
        return fileName.split("_").first();
    }

    auto ThumbnailReader::Impl::Convert(const QString& modalityString) -> AppEntity::Modality {
        if (modalityString == "BF") return AppEntity::Modality::BF;
        if (modalityString == "HT") return AppEntity::Modality::HT;
        if (modalityString == "FL") return AppEntity::Modality::FL;

        return AppEntity::Modality::HT;
    }

    auto ThumbnailReader::Impl::GetImageFile() const -> QString {
        auto IndexString = [](int32_t _index) -> QString {
            return QString("%1").arg(_index, 4, 10, QChar('0'));
        };

        auto ModalityString = [](AppEntity::Modality _modality) -> QString {
            switch (_modality) {
                case AppEntity::Modality::BF: return "BF";
                case AppEntity::Modality::FL: return "FL";
                case AppEntity::Modality::HT: return "HT";
            }
            return "";
        };

        return GetThumbnailDirectory() + "/" +  ModalityString(modality) + "_"+ IndexString(frameIndex) + ".png"; 
    }

    auto ThumbnailReader::Impl::GetThumbnailDirectory() const -> QString {
        return dataPath + "/thumbnail";
    }
}
