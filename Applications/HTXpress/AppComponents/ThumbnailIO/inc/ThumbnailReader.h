#pragma once

#include <memory>

#include <QImage>
#include <QString>

#include <AppEntityDefines.h>

#include "HTXThumbnailIOExport.h"

namespace HTXpress::AppComponents::ThumbnailIO {
    class HTXThumbnailIO_API ThumbnailReader {
    public:
        ThumbnailReader();
        ~ThumbnailReader();

        auto SetDataPath(const QString& dataPath) -> void;
        auto SetFrameIndex(int32_t frameIndex) -> void;
        auto SetModality(AppEntity::Modality modality) -> void;

        [[nodiscard]] auto GetImage(/*const QString& path*/) const -> std::shared_ptr<QImage>;
        [[nodiscard]] auto GetLastImageInfo(/*const QString& dataDirPath*/) const -> std::tuple<int32_t, AppEntity::Modality, std::shared_ptr<QImage>>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
