#pragma once
#include <memory>

#include <AppEntityDefines.h>
#include <ChannelConfig.h>

#include "HTXChannelConfigIOExport.h"

namespace HTXpress::AppComponents::ChannelConfigIO {
    class HTXChannelConfigIO_API Reader {
    public:
        using Pointer = std::shared_ptr<Reader>;
        using ImagingMode = AppEntity::ImagingMode;
        using ChannelConfig = AppEntity::ChannelConfig;

    public:
        Reader();
        ~Reader();

        auto SetTopPath(const QString& path)->void;
        auto Read(ImagingMode mode, ChannelConfig& config) const -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}