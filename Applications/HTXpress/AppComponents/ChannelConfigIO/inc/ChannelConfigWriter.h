#pragma once
#include <memory>
#include <AppEntityDefines.h>
#include <ChannelConfig.h>

#include "HTXChannelConfigIOExport.h"

namespace HTXpress::AppComponents::ChannelConfigIO {
    class HTXChannelConfigIO_API Writer {
    public:
        using Pointer = std::shared_ptr<Writer>;
        using ImagingMode = AppEntity::ImagingMode;
        using ChannelConfig = AppEntity::ChannelConfig;

    public:
        Writer();
        ~Writer();

        auto SetTopPath(const QString& path)->void;
        auto Write(ImagingMode mode, const ChannelConfig& config) -> bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}