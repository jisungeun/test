#include <QSettings>
#include <QFile>

#include "ChannelConfigDefines.h"
#include "ChannelConfigReader.h"

namespace HTXpress::AppComponents::ChannelConfigIO {
    struct Reader::Impl {
        QString topPath;
    };

    Reader::Reader() : d{new Impl} {
    }

    Reader::~Reader() {
    }

    auto Reader::SetTopPath(const QString& path) -> void {
        d->topPath = path;
    }

    auto Reader::Read(ImagingMode mode, ChannelConfig& config) const -> bool {
        const auto path = QString("%1/%2_%3.ini").arg(d->topPath).arg(Key::prefix).arg(mode._to_string());
        if(!QFile::exists(path)) return false;

        QSettings qs(path, QSettings::IniFormat);

        config.SetEnable(qs.value(Key::enable).toBool());
        config.SetCameraFov(qs.value(Key::fovX).toInt(), qs.value(Key::fovY).toInt());
        config.SetCameraExposureUSec(qs.value(Key::exposure).toInt());
        config.SetCameraIntervalUSec(qs.value(Key::interval).toInt());
        config.SetCameraInternal(qs.value(Key::internalCamera).toBool());
        config.SetLightChannel(qs.value(Key::lightChannel).toInt());
        config.SetLightIntensity(qs.value(Key::lightIntensity).toInt());
        config.SetLightInternal(qs.value(Key::internalLight).toBool());
        config.SetFilterChannel(qs.value(Key::filterChannel).toInt());
        config.SetChannelName(qs.value(Key::channelName).toString());

        return true;
    }
}
