#pragma once
#include <memory>

namespace HTXpress::AppComponents::ChannelConfigIO::Key {
    constexpr char* prefix = "ChannelConfig";
    constexpr char* enable = "Enable";
    constexpr char* fovX = "Fov/X";
    constexpr char* fovY = "Fov/Y";
    constexpr char* exposure = "Exposure";
    constexpr char* interval = "Interval";
    constexpr char* internalCamera = "InternalCamera";
    constexpr char* lightChannel = "LightChannel";
    constexpr char* lightIntensity = "LightIntensity";
    constexpr char* internalLight = "InternalLight";
    constexpr char* filterChannel = "FilterChannel";
    constexpr char* channelName = "ChannelName";
}