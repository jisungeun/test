#include <QSettings>

#include "ChannelConfigDefines.h"
#include "ChannelConfigWriter.h"

namespace HTXpress::AppComponents::ChannelConfigIO {
    struct Writer::Impl {
        QString topPath;
    };

    Writer::Writer() : d{new Impl} {
    }

    Writer::~Writer() {
    }

    auto Writer::SetTopPath(const QString& path) -> void {
        d->topPath = path;
    }

    auto Writer::Write(ImagingMode mode, const ChannelConfig& config) -> bool {
        const auto path = QString("%1/%2_%3.ini").arg(d->topPath).arg(Key::prefix).arg(mode._to_string());

        QSettings qs(path, QSettings::IniFormat);

        qs.setValue(Key::enable, config.GetEnable());
        qs.setValue(Key::fovX, config.GetCameraFovX());
        qs.setValue(Key::fovY, config.GetCameraFovY());
        qs.setValue(Key::exposure, config.GetCameraExposureUSec());
        qs.setValue(Key::interval, config.GetCameraIntervalUSec());
        qs.setValue(Key::internalCamera, config.GetCameraInternal());
        qs.setValue(Key::lightChannel, config.GetLightChannel());
        qs.setValue(Key::lightIntensity, config.GetLightIntensity());
        qs.setValue(Key::internalLight, config.GetLightInternal());
        qs.setValue(Key::filterChannel, config.GetFilterChannel());
        qs.setValue(Key::channelName, config.GetChannelName());

        return true;
    }
}
