#pragma once
#include <memory>

#include <QWidget>
#include <QImage>

#include "HTXPreviewPanelExport.h"

namespace HTXpress::AppComponents::PreviewPanel {

    class HTXPreviewPanel_API PreviewPanel : public QWidget {
        Q_OBJECT
    public:
        enum class PreviewScanType {
            Default, Custom
        }; Q_ENUMS(PreviewScanType)

        using Self = PreviewPanel;

        explicit PreviewPanel(QWidget* parent = nullptr);
        ~PreviewPanel() override;

        auto SetImageSize(int32_t xPixels, int32_t yPixels) -> void;
        auto UpdateBlockImage(int32_t xPos, int32_t yPos, const QImage& blockImage) -> void;

        auto SetROI(double x, double y, double w, double h) -> void;

        auto SetUmPerPixel(const double& umPerPixel) -> void;
        
        auto SetTileImagingActivation(bool active) -> void;
        auto SetTileImagingArea(double x, double y, double w, double h) -> void;
        auto SetTileImagingCenter(double xInPixel, double yInPixel)->void;

        auto StopCustomPreviewArea() -> void;

        auto Clear() -> void;

        auto GetImage() const->QPixmap&;

    signals:
        void sigDoPreviewScan(const HTXpress::AppComponents::PreviewPanel::PreviewPanel::PreviewScanType&); // image setting request
        void sigRequestSetPreviewArea();
        void sigCancelSetPreviewArea();
        void sigMoveLive(int32_t x, int32_t y);
        void sigChangeTileImagingArea(double x, double y, double width, double height);
        void sigSnapshotRequested();

        // internal use only
    private slots:
        void onCanvasDblClicked(int32_t x, int32_t y);
        void onPreviewScanClicked();
        void onCustomPreviewScanAreaToggled(bool toggled);
        void onRecvTileImagingAreaFromCanvas(double x, double y, double width, double height);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

Q_DECLARE_METATYPE(HTXpress::AppComponents::PreviewPanel::PreviewPanel::PreviewScanType)