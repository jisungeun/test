project(HTXPreviewWidgetUnitTest)

#Header files for external use
set(INTERFACE_HEADERS
)

set(TEST_SOURCE

)

#Header files for internal use
set(PRIVATE_HEADERS

)

#Sources
set(SOURCES
	src/main_test.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${PRIVATE_HEADERS}
	${SOURCES}
	${TEST_SOURCE}
)

target_compile_features(${PROJECT_NAME} 
	PRIVATE
		cxx_std_17
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
		${CMAKE_CURRENT_SOURCE_DIR}/../src
)

target_link_libraries(${PROJECT_NAME}			
	PRIVATE
		Catch2::Catch2
		Qt5::Core
		HTXpress::AppComponents::PreviewPanel	
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Components/TestCases") 	

foreach(FILE ${TEST_SOURCE})
    # Remove common directory prefix to make the group
    string(REPLACE "src" "" GROUP "TEST_SOURCE")

    # Make sure we are using windows slashes
    string(REPLACE "/" "\\" GROUP "${GROUP}")
	
	source_group("${GROUP}" FILES "${FILE}")
endforeach()