#include <QImage>

#include "PreviewPanelControl.h"
#include "PreviewDataRepo.h"

namespace HTXpress::AppComponents::PreviewPanel {
    struct PreviewPanelControl::Impl {
        PreviewDataRepo::Pointer repo{PreviewDataRepo::GetInstance()};
        QSize imageSize;
    };

    PreviewPanelControl::PreviewPanelControl() : d{new Impl} {
    }

    PreviewPanelControl::~PreviewPanelControl() {
    }

    auto PreviewPanelControl::SetROI(double x, double y, double w, double h) -> void {
        d->repo->SetROI({x,y,w,h});
    }

    auto PreviewPanelControl::SetTimeImagingArea(double x, double y, double w, double h) -> void {
        d->repo->SetTileImagingArea({x,y,w,h});
    }

    auto PreviewPanelControl::SetTileImagingCenter(double xInPixel, double yInPixel) -> void {
        const auto geo = d->repo->GetTileImagingArea();
        d->repo->SetTileImagingArea({xInPixel, yInPixel, geo.w, geo.h});
    }

    auto PreviewPanelControl::GetTileImagingArea() const -> Geometry {
        return d->repo->GetTileImagingArea();
    }

    auto PreviewPanelControl::SetAreaVisible(bool show) -> void {
        d->repo->SetAreaVisible(show);
    }

    auto PreviewPanelControl::Clear() -> void {
        d->repo->Clear();
    }
}
