﻿#include <QMouseEvent>
#include <QBoxLayout>

#include "PreviewCanvas.h"
#include "CanvasEventFilter.h"
#include "PreviewCanvasControl.h"
#include "GraphicsTileImagingItem.h"
#include "GraphicsRoiItem.h"
#include "GraphicsPreviewImageItem.h"
#include "ScalebarWidget.h"
#include "ZoomWidget.h"
#include "SubWidgetPosUpdater.h"
#include "TitleWidget.h"
#include "SnapshotWidget.h"
#include "SubWidgetPosUpdater.h"

namespace HTXpress::AppComponents::PreviewPanel {
    struct PreviewCanvas::Impl {
        explicit Impl(Self* self):self(self){}

        Self* self{nullptr};
        PreviewCanvasControl control;

        GraphicsTileImagingItem* tileImagingItem{nullptr};
        GraphicsRoiItem* roiItem{nullptr};
        GraphicsPreviewImageItem* imageItem{nullptr};
        QPixmap pixmap;

        ScalebarWidget* scalebarWidget{nullptr};
        ZoomWidget* zoomWidget{nullptr};
        TitleWidget* titleWidget{nullptr};
        SnapshotWidget* snapshotWidget{ nullptr };
        QWidget *bottomRightWidget{nullptr}; // including snapshot, zoom

        CanvasEventFilter* eventFilter{nullptr};

        SubWidgetPosUpdater::Pointer posUpdater{nullptr};

        bool leftButtonPressed{};
        bool lastLeftButtonPressed{};

        auto InitBottomRightWidget() -> void;
        auto IsTileImagingSelected() -> bool;
    };

    auto PreviewCanvas::Impl::InitBottomRightWidget() -> void {
        bottomRightWidget = new QWidget(self);
        bottomRightWidget->setStyleSheet("QWidget{background-color:transparent;}");
        snapshotWidget = new SnapshotWidget(bottomRightWidget);
        zoomWidget = new ZoomWidget(bottomRightWidget);

        const auto layout = new QVBoxLayout();
        layout->addWidget(snapshotWidget, 0, Qt::AlignCenter);
        layout->addWidget(zoomWidget, 0, Qt::AlignCenter);
        layout->setSpacing(6);
        layout->setContentsMargins(0,0,0,0);

        bottomRightWidget->setLayout(layout);
        bottomRightWidget->setFixedWidth(24);
        bottomRightWidget->setMinimumHeight(24*4+6*3);
    }

    auto PreviewCanvas::Impl::IsTileImagingSelected() -> bool {
        if(tileImagingItem) {
            return tileImagingItem->isSelected();
        }
        return false;
    }

    PreviewCanvas::PreviewCanvas(QWidget* parent) : QGraphicsView(parent), d{std::make_unique<Impl>(this)} {
        d->scalebarWidget = new ScalebarWidget(this);
        d->scalebarWidget->setVisible(false);

        d->InitBottomRightWidget();

        d->titleWidget = new TitleWidget(this);

        d->posUpdater = std::make_shared<SubWidgetPosUpdater>(this);
        d->posUpdater->SetSubwidget(d->scalebarWidget, SubWidgetPosUpdater::SubWidgetGeomertry::BottomLeft);
        d->posUpdater->SetSubwidget(d->bottomRightWidget, SubWidgetPosUpdater::SubWidgetGeomertry::BottomRight);
        d->posUpdater->SetSubwidget(d->titleWidget, SubWidgetPosUpdater::SubWidgetGeomertry::TopLeft, 0, 0);
        if (nullptr == scene()) this->setScene(new QGraphicsScene(this));
        scene()->setSceneRect(-300, -300, 600, 600);

        setMouseTracking(true);
        setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);
        setViewportUpdateMode(SmartViewportUpdate);
        setTransformationAnchor(NoAnchor);
        setDragMode(NoDrag);
        setOptimizationFlags(DontSavePainterState);
        setBackgroundBrush(Qt::black);

        d->eventFilter = new CanvasEventFilter(this);
        connect(d->zoomWidget, &ZoomWidget::sigZoomIn, this, [=]() { d->eventFilter->ZoomIn(); });
        connect(d->zoomWidget, &ZoomWidget::sigZoomOut, this, [=]() { d->eventFilter->ZoomOut(); });
        connect(d->zoomWidget, &ZoomWidget::sigZoomFit, this, [=]() { d->eventFilter->ZoomFit(); });

        connect(d->snapshotWidget, &SnapshotWidget::sigSnapshotRequest, this, &PreviewCanvas::sigSnapshotRequested);
    }

    PreviewCanvas::~PreviewCanvas() {
    }

    auto PreviewCanvas::Clear() -> void {
        scene()->clear();
        d->tileImagingItem = nullptr;
        d->roiItem = nullptr;
        d->imageItem = nullptr;

        d->scalebarWidget->setVisible(false);
        d->leftButtonPressed = false;
        d->titleWidget->ClearSizeLabel();

        d->bottomRightWidget->setVisible(false);
        d->bottomRightWidget->setEnabled(false);
    }

    auto PreviewCanvas::SetImageSize(int32_t xPixels, int32_t yPixels) -> void {
        auto imageSize = QSize(xPixels, yPixels);
        d->control.SetImageSize(imageSize);

        d->pixmap = QPixmap(xPixels, yPixels);

        if (nullptr == d->imageItem) {
            d->imageItem = new GraphicsPreviewImageItem(d->pixmap);
            scene()->addItem(d->imageItem);
            connect(d->imageItem->GetLodGetterObject(), &PreviewImageLodGetter::sigLodChanged, this, &PreviewCanvas::onLodChanged);
            connect(d->imageItem, &GraphicsPreviewImageItem::sigItemDoubleClicked, this, &PreviewCanvas::onMoveROI);
        } else {
            d->imageItem->setPixmap(d->pixmap);
        }

        const auto sceneRect = [=]()->QRectF {
            auto w = imageSize.width() * 3;
            auto h = imageSize.height() * 3;
            return QRectF(-w/2, -h/2, w, h);
        }();

        const auto imageRect = [=]()->QRectF {
            auto w = imageSize.width();
            auto h = imageSize.height();
            return QRectF(-w / 2, -h / 2, w, h);
        }();

        scene()->setSceneRect(sceneRect);
        d->imageItem->setPos(imageRect.topLeft().x(), imageRect.topLeft().y());
        d->imageItem->SetLineWidthF(d->control.GetLineWidthF());

        d->scalebarWidget->setVisible(true);

        d->eventFilter->SetZoomFitRect(imageRect);
        d->eventFilter->ZoomFit();

        UpdateROI();
        UpdateTileImagingArea();
    }

    auto PreviewCanvas::UpdateBlock(int32_t xPos, int32_t yPos, const QImage& blockImage) -> void {
        if(!d->imageItem) return;
        const auto& offset = d->control.GetImageSize() / 2;

        auto* pixmapItem = new QGraphicsPixmapItem(QPixmap::fromImage(blockImage));
        pixmapItem->setPos(xPos - offset.width(), yPos - offset.height());
        scene()->addItem(pixmapItem);
        viewport()->update();

        QPainter painter(&d->pixmap);
        painter.drawImage(xPos, yPos, blockImage);

        d->bottomRightWidget->setEnabled(true);
        d->bottomRightWidget->setVisible(true);
    }

    auto PreviewCanvas::UpdateROI() -> void {
        if (nullptr == d->roiItem) {
            d->roiItem = new GraphicsRoiItem();
            scene()->addItem(d->roiItem);
        }

        d->roiItem->setPos(d->control.GetRoiPos());
        d->roiItem->setRect(d->control.GetRoiRect());

        if (nullptr == d->imageItem) {
            d->roiItem->setVisible(false);
        } else {
            d->roiItem->setVisible(true);
        }

        scene()->update();
    }

    auto PreviewCanvas::UpdateTileImagingArea() -> void {
        if (nullptr == d->tileImagingItem) {
            d->tileImagingItem = new GraphicsTileImagingItem();
            scene()->addItem(d->tileImagingItem);
        }

        d->tileImagingItem->setPos(d->control.GetTileImagingPos());
        d->tileImagingItem->setRect(d->control.GetTileImagingRect());

        if (nullptr == d->imageItem) {
            d->tileImagingItem->setVisible(false);
        } else {
            d->tileImagingItem->setVisible(d->control.GetAreaVisible());
        }
        scene()->update();
    }

    auto PreviewCanvas::UpdateAreaVisible() -> void {
        if (nullptr != d->tileImagingItem) {
            d->tileImagingItem->setVisible(d->control.GetAreaVisible());
        }
        UpdateTileImagingArea();
    }

    auto PreviewCanvas::UpdateSizeTextItem() -> void {
        if (!d->imageItem) return;

        const auto imgWidth = d->control.GetImageSize().width() * d->control.GetUmPerPixel();
        const auto imgHeight = d->control.GetImageSize().height() * d->control.GetUmPerPixel();
        d->titleWidget->SetImageSizeLabel(imgWidth, imgHeight);
    }

    auto PreviewCanvas::SetUmPerPixel(const double& umPerPixel) -> void {
        d->control.SetUmPerPixel(umPerPixel);
        d->scalebarWidget->SetUmPerPixel(umPerPixel);
    }

    auto PreviewCanvas::IsImageItemNull() const -> bool {
        return d->imageItem == nullptr;
    }

    auto PreviewCanvas::GetImage() const -> QPixmap& {
        return d->pixmap;
    }

    auto PreviewCanvas::resizeEvent(QResizeEvent* event) -> void {
        d->posUpdater->onUpdatePosition();
        QGraphicsView::resizeEvent(event);
    }

    auto PreviewCanvas::mousePressEvent(QMouseEvent* event) -> void {
        if (event->button() == Qt::LeftButton) {
            d->leftButtonPressed = true;
            setDragMode(ScrollHandDrag);
        }

        QGraphicsView::mousePressEvent(event);
    }

    auto PreviewCanvas::mouseReleaseEvent(QMouseEvent* event) -> void {
        if (d->leftButtonPressed && d->IsTileImagingSelected()) {
            const Geometry geometry = {d->tileImagingItem->mapToScene(d->tileImagingItem->rect().center()).x(), -d->tileImagingItem->mapToScene(d->tileImagingItem->rect().center()).y(), d->tileImagingItem->GetSelectorFrameBounds().size().width(), d->tileImagingItem->GetSelectorFrameBounds().size().height()};
            emit sigSendCurrentTileImagingArea(geometry.x, geometry.y, geometry.w, geometry.h);
        }
        d->leftButtonPressed = false;
        setDragMode(NoDrag);
        QGraphicsView::mouseReleaseEvent(event);
    }

    void PreviewCanvas::mouseMoveEvent(QMouseEvent* event) {
        if (!d->IsTileImagingSelected()) {
            if (d->leftButtonPressed && d->leftButtonPressed != d->lastLeftButtonPressed) {
                setDragMode(ScrollHandDrag);
                const auto pressEvent = new QMouseEvent(QEvent::GraphicsSceneMousePress, event->pos(),
                                                        Qt::MouseButton::LeftButton, Qt::MouseButton::LeftButton,
                                                        Qt::KeyboardModifier::NoModifier);
                mousePressEvent(pressEvent);
            }
        }
        d->lastLeftButtonPressed = d->leftButtonPressed;
        QGraphicsView::mouseMoveEvent(event);
    }

    void PreviewCanvas::onLodChanged(const double& lod) {
        d->eventFilter->SetCurrentLod(lod);
        d->scalebarWidget->onRecvLodValueFromWidget(lod);
    }

    void PreviewCanvas::onMoveROI(const QPointF& scenePos) {
        if(d->tileImagingItem) d->tileImagingItem->setSelected(false);
        const auto pos = d->control.ConvertClickedPos(scenePos).toPoint();
        emit sigMouseDoubleClicked(pos.x(), pos.y());
    }
}
