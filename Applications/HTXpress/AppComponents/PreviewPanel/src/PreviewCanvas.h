﻿#pragma once

#include <memory>

#include <QGraphicsView>

namespace HTXpress::AppComponents::PreviewPanel {
    class PreviewCanvas : public QGraphicsView {
        Q_OBJECT
    public:
        using Self = PreviewCanvas;

        explicit PreviewCanvas(QWidget* parent = nullptr);
        ~PreviewCanvas() override;

        auto Clear() -> void;

        auto SetImageSize(int32_t xPixels, int32_t yPixels) -> void;
        auto UpdateBlock(int32_t xPos, int32_t yPos, const QImage& blockImage) -> void;
        auto UpdateROI() -> void;

        auto UpdateTileImagingArea() -> void;
        auto UpdateAreaVisible() -> void;

        auto UpdateSizeTextItem() -> void;

        auto SetUmPerPixel(const double& umPerPixel) -> void;

        auto IsImageItemNull() const -> bool;

        auto GetImage() const->QPixmap&;

    protected:
        auto resizeEvent(QResizeEvent* event) -> void override;
        auto mousePressEvent(QMouseEvent* event) -> void override;
        auto mouseReleaseEvent(QMouseEvent* event) -> void override;
        auto mouseMoveEvent(QMouseEvent* event) -> void override;

    signals:
        void sigMouseDoubleClicked(int32_t x, int32_t y);
        void sigSendCurrentTileImagingArea(double x, double y, double w, double h);
        void sigSnapshotRequested();

    private slots:
        void onLodChanged(const double& lod);
        void onMoveROI(const QPointF& scenePos);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    }; 
}


