﻿#pragma once
#include <memory>
#include <QGraphicsRectItem>

namespace HTXpress::AppComponents::PreviewPanel {
    class GraphicsRoiItem final : public QGraphicsRectItem {
    public:
        explicit GraphicsRoiItem(QGraphicsItem* parent = nullptr);
        ~GraphicsRoiItem() override;

        auto type() const -> int override;

    protected:
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
    };
}
