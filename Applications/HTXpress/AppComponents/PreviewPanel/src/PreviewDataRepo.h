﻿#pragma once

#include <memory>

#include <QImage>

#include "PreviewPanelDefines.h"

namespace HTXpress::AppComponents::PreviewPanel {
    class PreviewDataRepo {
    public:
        using Self = PreviewDataRepo;
        using Pointer = std::shared_ptr<Self>;

    protected:
        PreviewDataRepo();

    public:
        static auto GetInstance() -> Pointer;
        virtual ~PreviewDataRepo();

        auto SetROI(const Geometry& geo) -> void;
        auto GetROI() const -> Geometry;

        auto SetTileImagingArea(const Geometry& geo) -> void;
        auto GetTileImagingArea() const -> Geometry;

        auto SetAreaVisible(bool show) -> void;
        auto IsAreaVisible() const -> bool;

        auto Clear() -> void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
