﻿#pragma once

#include <memory>
#include <QGraphicsRectItem>

namespace HTXpress::AppComponents::PreviewPanel {
    class ResizeHandleItem : public QGraphicsEllipseItem {
    public:
        using Self = ResizeHandleItem;

        enum class Position {
            TopLeft = 0,
            TopRight,
            BottomLeft,
            BottomRight
        };

        explicit ResizeHandleItem(Position position);

        auto SetLimits(double x, double y) -> void;

    protected:
        auto mouseMoveEvent(QGraphicsSceneMouseEvent* event) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
