#include "PreviewPanel.h"
#include "PreviewPanelControl.h"
#include "ui_PreviewPanel.h"
#include "PreviewCanvas.h"

namespace HTXpress::AppComponents::PreviewPanel {
    struct PreviewPanel::Impl {
        Ui::PreviewPanel* ui{nullptr};
        PreviewPanelControl control;

        auto Connect(const Self* self) -> void;
    };

    PreviewPanel::PreviewPanel(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui = new Ui::PreviewPanel();
        d->ui->setupUi(this);

        d->ui->canvas->setObjectName("view-tsx-canvas");
        d->ui->panelWidget->setObjectName("panel");
        d->Connect(this);
    }

    PreviewPanel::~PreviewPanel() {
        delete d->ui;
    }

    auto PreviewPanel::SetImageSize(int32_t xPixels, int32_t yPixels) -> void {
        d->ui->canvas->SetImageSize(xPixels, yPixels);
        d->ui->canvas->UpdateSizeTextItem();
        d->ui->canvas->repaint();
    }

    auto PreviewPanel::UpdateBlockImage(int32_t xPos, int32_t yPos, const QImage& blockImage) -> void {
        d->ui->canvas->UpdateBlock(xPos, yPos, blockImage);
    }

    auto PreviewPanel::SetROI(double x, double y, double w, double h) -> void {
        d->control.SetROI(x, y, w, h);
        d->ui->canvas->UpdateROI();
    }

    auto PreviewPanel::SetUmPerPixel(const double& umPerPixel) -> void {
        d->ui->canvas->SetUmPerPixel(umPerPixel);
        d->ui->canvas->UpdateSizeTextItem();
    }

    auto PreviewPanel::SetTileImagingArea(double x, double y, double w, double h) -> void {
        d->control.SetTimeImagingArea(x, y, w, h);
        d->ui->canvas->UpdateTileImagingArea();
    }

    auto PreviewPanel::SetTileImagingCenter(double xInPixel, double yInPixel) -> void {
        d->control.SetTileImagingCenter(xInPixel, yInPixel);
        d->ui->canvas->UpdateTileImagingArea();
    }

    auto PreviewPanel::StopCustomPreviewArea() -> void {
        if(d->ui->customSize->isChecked()) {
            d->ui->customSize->setChecked(false);
        }
    }

    auto PreviewPanel::SetTileImagingActivation(bool active) -> void {
        d->control.SetAreaVisible(active);
        d->ui->canvas->UpdateAreaVisible();
    }

    auto PreviewPanel::Clear() -> void {
        d->control.Clear();
        d->ui->canvas->Clear();
        StopCustomPreviewArea();
    }

    auto PreviewPanel::GetImage() const -> QPixmap& {
        return d->ui->canvas->GetImage();
    }

    void PreviewPanel::onCanvasDblClicked(int32_t x, int32_t y) {
        if(!d->ui->canvas->IsImageItemNull()) {
            emit sigMoveLive(x, y);
        }
    }

    void PreviewPanel::onPreviewScanClicked() {
        auto previewRequestType = PreviewScanType::Default;
        if (d->ui->customSize->isChecked()) {
            d->ui->customSize->blockSignals(true);
            d->ui->customSize->setChecked(false);
            d->ui->customSize->blockSignals(false);

            previewRequestType = PreviewScanType::Custom;
        }
        emit sigDoPreviewScan(previewRequestType);
    }

    void PreviewPanel::onCustomPreviewScanAreaToggled(bool toggled) {
        if (toggled) {
            emit sigRequestSetPreviewArea();
        }
        else {
            emit sigCancelSetPreviewArea();
        }
    }

    void PreviewPanel::onRecvTileImagingAreaFromCanvas(double x, double y, double width, double height) {
        Geometry recvVal{x, y, width, height};
        if (recvVal != d->control.GetTileImagingArea()) {
            d->control.SetTimeImagingArea(x, y, width, height);
            emit sigChangeTileImagingArea(x, y, width, height);
        }
    }

    auto PreviewPanel::Impl::Connect(const Self* self) -> void {
        connect(ui->previewScan, &QAbstractButton::clicked, self, &Self::onPreviewScanClicked);
        connect(ui->customSize, &QAbstractButton::toggled, self, &Self::onCustomPreviewScanAreaToggled);

        connect(ui->canvas, &PreviewCanvas::sigMouseDoubleClicked, self, &Self::onCanvasDblClicked);
        connect(ui->canvas, &PreviewCanvas::sigSendCurrentTileImagingArea, self, &Self::onRecvTileImagingAreaFromCanvas);
        connect(ui->canvas, &PreviewCanvas::sigSnapshotRequested, self, &Self::sigSnapshotRequested);
    }
}
