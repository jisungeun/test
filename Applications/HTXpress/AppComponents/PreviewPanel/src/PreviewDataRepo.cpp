﻿#include "PreviewDataRepo.h"

namespace HTXpress::AppComponents::PreviewPanel {
    struct PreviewDataRepo::Impl {
        bool showTileImagingArea{false};
        QImage image;
        Geometry roiArea{0.0, 0.0, 0.0, 0.0};
        Geometry tileImagingArea{0.0, 0.0, 0.0, 0.0};
    };

    PreviewDataRepo::PreviewDataRepo() : d{std::make_unique<Impl>()} {
    }

    PreviewDataRepo::~PreviewDataRepo() {
    }

    auto PreviewDataRepo::GetInstance() -> Pointer {
        static Pointer theInstance{new PreviewDataRepo};
        return theInstance;
    }
    auto PreviewDataRepo::SetROI(const Geometry& geo) -> void {
        d->roiArea = geo;
    }

    auto PreviewDataRepo::GetROI() const -> Geometry {
        return d->roiArea;
    }

    auto PreviewDataRepo::SetTileImagingArea(const Geometry& geo) -> void {
        d->tileImagingArea = geo;
    }

    auto PreviewDataRepo::GetTileImagingArea() const -> Geometry {
        return d->tileImagingArea;
    }

    auto PreviewDataRepo::SetAreaVisible(bool show) -> void {
        d->showTileImagingArea = show;
    }

    auto PreviewDataRepo::IsAreaVisible() const -> bool {
        return d->showTileImagingArea;
    }

    auto PreviewDataRepo::Clear() -> void {
        d->image = QImage();
    }
}
