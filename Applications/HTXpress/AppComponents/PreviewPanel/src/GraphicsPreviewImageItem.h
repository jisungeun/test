﻿#pragma once

#include <memory>

#include <QGraphicsPixmapItem>

#include "PreviewImageLodGetter.h"

namespace HTXpress::AppComponents::PreviewPanel {
    class GraphicsPreviewImageItem : public QObject, public QGraphicsPixmapItem {
        Q_OBJECT
    public:
        explicit GraphicsPreviewImageItem(const QPixmap& pixmap);
        ~GraphicsPreviewImageItem() override;

        auto SetLineWidthF(double lineWidth) -> void;
        auto GetLodGetterObject() const -> PreviewImageLodGetter*;

    protected:
        auto type() const -> int override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
        auto mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) -> void override;

    signals:
        void sigItemDoubleClicked(const QPointF&);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
