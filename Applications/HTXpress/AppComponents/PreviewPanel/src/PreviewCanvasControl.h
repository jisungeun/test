﻿#pragma once

#include <memory>

#include <QImage>

#include "PreviewPanelDefines.h"

namespace HTXpress::AppComponents::PreviewPanel {
    class PreviewCanvasControl {
    public:
        PreviewCanvasControl();
        ~PreviewCanvasControl();

        auto SetImageSize(const QSize& imageSize) -> void;
        auto GetImageSize() const -> QSize;

        auto GetRoiPos() const -> QPointF;
        auto GetRoiRect() const -> QRectF;

        auto GetTileImagingPos() const -> QPointF;
        auto GetTileImagingRect() const -> QRectF;
        auto GetAreaVisible() -> bool;

        auto ConvertClickedPos(const QPointF& mapToScenePos) -> QPointF;

        auto GetLineWidthF() const -> double;

        auto SetUmPerPixel(double umPerPixel) -> void;
        auto GetUmPerPixel() const -> double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
