﻿#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

#include "GraphicsRoiItem.h"
#include "PreviewPanelDefines.h"
#include "GraphicsItemHelper.h"

namespace HTXpress::AppComponents::PreviewPanel {
    GraphicsRoiItem::GraphicsRoiItem(QGraphicsItem* parent) : QGraphicsRectItem(parent) {
        QPen p = pen();
        p.setColor(roiBorderColor);
        setPen(p);
        setZValue(100);
        setFlags(ItemIsFocusScope);
    }

    GraphicsRoiItem::~GraphicsRoiItem() {
    }

    auto GraphicsRoiItem::type() const -> int {
        return ItemType::ROI;
    }

    auto GraphicsRoiItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(option)
        Q_UNUSED(widget)

        painter->setRenderHint(QPainter::Antialiasing);
        QPen p = pen();
        double width = 1.0;

        if (painter->transform().isScaling()) {
            width = GraphicsItemHelper::GetConsistentPenWidth(painter, 2.0);
        }

        p.setWidthF(width);
        painter->setPen(p);
        painter->drawRect(rect());
    }
}
