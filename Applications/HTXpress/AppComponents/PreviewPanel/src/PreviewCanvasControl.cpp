﻿#include <QPixmap>

#include "PreviewCanvasControl.h"
#include "PreviewDataRepo.h"
#include "PreviewPanelDefines.h"

namespace HTXpress::AppComponents::PreviewPanel {
    struct PreviewCanvasControl::Impl {
        PreviewDataRepo::Pointer repo{PreviewDataRepo::GetInstance()};        
        double umPerPixel{};
        QSize imageSize{0, 0};
    };

    PreviewCanvasControl::PreviewCanvasControl() : d{std::make_unique<Impl>()}{
    }

    PreviewCanvasControl::~PreviewCanvasControl() {
    }

    auto PreviewCanvasControl::SetImageSize(const QSize& imageSize) -> void {
        d->imageSize = imageSize;
    }

    auto PreviewCanvasControl::GetImageSize() const -> QSize {
        return d->imageSize;
    }

    auto PreviewCanvasControl::GetRoiPos() const -> QPointF {
        return QPointF(d->repo->GetROI().x, -d->repo->GetROI().y);
    }

    auto PreviewCanvasControl::GetRoiRect() const -> QRectF {
        const auto roiArea = d->repo->GetROI();
        const auto w = roiArea.w;
        const auto h = roiArea.h;
        const auto x = -(w / 2);
        const auto y = -(h / 2);
        return QRectF(x,y,w,h);
    }

    auto PreviewCanvasControl::GetTileImagingPos() const -> QPointF {
        return QPointF(d->repo->GetTileImagingArea().x, -d->repo->GetTileImagingArea().y);
    }

    auto PreviewCanvasControl::GetTileImagingRect() const -> QRectF {
        const auto area = d->repo->GetTileImagingArea();
        const auto w = area.w;
        const auto h = area.h;
        const auto x = -(w/2);
        const auto y = -(h/2);
        return QRectF(x,y,w,h);
    }

    auto PreviewCanvasControl::GetAreaVisible() -> bool {
        return d->repo->IsAreaVisible();
    }

    auto PreviewCanvasControl::ConvertClickedPos(const QPointF& mapToScenePos) -> QPointF {
        return {mapToScenePos.x(), -mapToScenePos.y()};
    }

    auto PreviewCanvasControl::GetLineWidthF() const -> double {
        return d->imageSize.width()/250;
    }

    auto PreviewCanvasControl::SetUmPerPixel(double umPerPixel) -> void {
        d->umPerPixel = umPerPixel;
    }

    auto PreviewCanvasControl::GetUmPerPixel() const -> double {
        return d->umPerPixel;
    }
}
