﻿#include <QGraphicsScene>
#include <QMenu>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

#include "GraphicsTileImagingItem.h"
#include "GraphicsItemHelper.h"
#include "PreviewPanelDefines.h"

namespace HTXpress::AppComponents::PreviewPanel {
    GraphicsTileImagingItem::GraphicsTileImagingItem() : QGraphicsRectItem() {
        QPen p = pen();
        p.setColor(tileBorderColor);
        setPen(p);

        SetOwnerItem(this);
        setZValue(1000);
        setFlags(ItemIsMovable | ItemIsSelectable);
        setVisible(false);
    }

    GraphicsTileImagingItem::~GraphicsTileImagingItem() {
    }

    auto GraphicsTileImagingItem::boundingRect() const -> QRectF {
        const auto penWidth = pen().widthF();
        return GetSelectorFrameBounds().adjusted(-penWidth, -penWidth, penWidth, penWidth);
    }

    auto GraphicsTileImagingItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(widget)

        painter->setRenderHint(QPainter::Antialiasing);

        QPen p = pen();
        auto width = 1.0;

        if(painter->transform().isScaling()) {
            width = GraphicsItemHelper::GetConsistentPenWidth(painter, 2.0);
        }

        p.setWidthF(width);
        painter->setPen(p);

        QColor innerColor = Qt::transparent;
        if(option->state & QStyle::State_Selected){
            innerColor = p.color();
            innerColor.setAlpha(40);
        }

        painter->setBrush(innerColor);
        painter->drawRect(rect());

        SetCurrentLOD(QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform()));
        DrawHandlesIfNeeded();
    }

    auto GraphicsTileImagingItem::type() const -> int {
        return ItemType::TileImaging;
    }

    auto GraphicsTileImagingItem::GetSelectorFrameBounds() const -> QRectF {
        return rect();
    }

    auto GraphicsTileImagingItem::SetSelectorFrameBounds(const QRectF& boundsRect) -> void {
        prepareGeometryChange();
        setRect(boundsRect);
        update();
    }
}
