﻿#include "TitleWidget.h"

#include <QLineEdit>

#include "ui_TitleWidget.h"

namespace HTXpress::AppComponents::PreviewPanel {
    struct TitleWidget::Impl {
        Ui::PreviewTitle *ui{nullptr};
    };

    TitleWidget::TitleWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui = new Ui::PreviewTitle();
        d->ui->setupUi(this);

        d->ui->titleLabel->setObjectName("label-h2");
        d->ui->sizeInfoLabel->setObjectName("label-h4");

        setAttribute(Qt::WA_TransparentForMouseEvents);
    }

    TitleWidget::~TitleWidget() {
        delete d->ui;
    }

    auto TitleWidget::ClearSizeLabel() -> void {
        d->ui->sizeInfoLabel->clear();
        d->ui->sizeInfoLabel->setToolTip("");
    }

    auto TitleWidget::SetImageSizeLabel(const double& widthUM, const double& heightUM) -> void {
        const auto displayText = QString("%1 %2m X %3 %2m").arg(widthUM).arg(QChar(0x00B5)).arg(heightUM);
        d->ui->sizeInfoLabel->setText(displayText);
        d->ui->sizeInfoLabel->setToolTip(displayText);
    }
}
