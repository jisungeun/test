﻿#pragma once

#include <QWidget>

namespace HTXpress::AppComponents::PreviewPanel {
    class TitleWidget : public QWidget{
        Q_OBJECT
    public:
        explicit TitleWidget(QWidget* parent = nullptr);    
        ~TitleWidget() override;

        auto ClearSizeLabel() -> void;
        auto SetImageSizeLabel(const double& widthUM, const double& heightUM) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
