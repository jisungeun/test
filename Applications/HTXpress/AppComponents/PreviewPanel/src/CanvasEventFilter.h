﻿#pragma once

#include <memory>

#include <QObject>
#include <QGraphicsView>

/// <summary>
/// (default modifier)ctrl + mouse wheel = zoom in/out
///	mouse right btn double clicked = fit in view
/// </summary>

namespace HTXpress::AppComponents::PreviewPanel {
    class CanvasEventFilter : public QObject {
        Q_OBJECT
    public:
        using Self = CanvasEventFilter;
        using Pointer = std::shared_ptr<Self>;

        explicit CanvasEventFilter(QGraphicsView* view);
        ~CanvasEventFilter() override;

        auto SetCurrentLod(const double& lod) -> void;
        auto SetZoomFitRect(const QRectF& rect) -> void;

        auto ZoomIn(bool anchorMouse = false) const -> void;
        auto ZoomOut(bool anchorMouse = false) const -> void;
        auto ZoomFit() const -> void;

    protected:
        auto eventFilter(QObject* watched, QEvent* event) -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
