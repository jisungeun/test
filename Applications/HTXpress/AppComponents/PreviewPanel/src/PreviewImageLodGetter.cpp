﻿#include "PreviewImageLodGetter.h"

namespace HTXpress::AppComponents::PreviewPanel {
    PreviewImageLodGetter::PreviewImageLodGetter(QObject* parent) : QObject(parent) {
    }

    auto PreviewImageLodGetter::SetLOD(const double& _lod) -> void {
        if(fabs(_lod-lod)>std::numeric_limits<double>::epsilon())
            emit sigLodChanged(_lod);

        lod = _lod;
    }

    auto PreviewImageLodGetter::GetLOD() const -> double {
        return lod;
    }
}
