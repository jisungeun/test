﻿#include <QApplication>
#include <QEvent>
#include <QtMath>
#include <QWheelEvent>

#include "CanvasEventFilter.h"

namespace HTXpress::AppComponents::PreviewPanel {
    struct CanvasEventFilter::Impl {
        QGraphicsView* view{nullptr};

        const Qt::KeyboardModifiers zoomKeyModifiers{Qt::ControlModifier};
        double currLod{1};
        const double zoomFactorValue{1.1};
        QRectF zoomFitRect{};

        auto CanZoomIn() const -> bool;
        auto CanZoomOut() const -> bool;
    };

    auto CanvasEventFilter::Impl::CanZoomIn() const -> bool {
        // TODO zoom in 제한이 필요한 경우 추가 구현
        return true;
    }

    auto CanvasEventFilter::Impl::CanZoomOut() const -> bool {
        const auto viewSize = view->size();
        const auto sceneRect = zoomFitRect;

        double targetViewSize{};
        double targetSceneRectSize{};

        if (sceneRect.width() > sceneRect.height()) {
            targetSceneRectSize = sceneRect.width();
            targetViewSize = viewSize.width();
        } else {
            targetSceneRectSize = sceneRect.height();
            targetViewSize = viewSize.height();
        }

        return (targetViewSize / 3 < targetSceneRectSize * currLod);
    }

    CanvasEventFilter::CanvasEventFilter(QGraphicsView* view) : d{std::make_unique<Impl>()} {
        d->view = view;
        d->zoomFitRect = d->view->sceneRect();
        d->view->viewport()->installEventFilter(this);
        d->view->setMouseTracking(true);
    }

    CanvasEventFilter::~CanvasEventFilter() {
    }

    auto CanvasEventFilter::SetCurrentLod(const double& lod) -> void {
        d->currLod = lod;
    }

    auto CanvasEventFilter::SetZoomFitRect(const QRectF& rect) -> void {
        d->zoomFitRect = rect;
    }

    auto CanvasEventFilter::ZoomIn(bool anchorMouse) const -> void {
        if (d->CanZoomIn() == false) {
            return;
        }

        if (anchorMouse) {
            d->view->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
        } else {
            d->view->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
        }

        d->view->scale(d->zoomFactorValue, d->zoomFactorValue);
    }

    auto CanvasEventFilter::ZoomOut(bool anchorMouse) const -> void {
        if (d->CanZoomOut() == false) {
            return;
        }

        if (anchorMouse) {
            d->view->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
        } else {
            d->view->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
        }

        d->view->scale(1 / d->zoomFactorValue, 1 / d->zoomFactorValue);
    }

    auto CanvasEventFilter::ZoomFit() const -> void {
        const auto noWhiteSpaceRect = d->zoomFitRect;
        d->view->fitInView(noWhiteSpaceRect.x(), noWhiteSpaceRect.y(), noWhiteSpaceRect.width(), noWhiteSpaceRect.height(), Qt::KeepAspectRatio);
    }

    auto CanvasEventFilter::eventFilter(QObject* watched, QEvent* event) -> bool {
        Q_UNUSED(watched)

        const auto mouseEvent = [event] {
            return dynamic_cast<QMouseEvent*>(event);
        }();
        const auto wheelEvent = [event] {
            return dynamic_cast<QWheelEvent*>(event);
        }();

        if (event->type() == QEvent::Wheel) {
            if (QApplication::keyboardModifiers() == d->zoomKeyModifiers) {
                const double angle = wheelEvent->angleDelta().y();
                if (angle > 0) {
                    ZoomIn(true);
                } else {
                    ZoomOut(true);
                }
                return true;
            }
        } else if (event->type() == QEvent::MouseButtonDblClick) {
            if (mouseEvent->button() == Qt::RightButton) {
                ZoomFit();
                return true;
            }
        }

        return false;
    }
}
