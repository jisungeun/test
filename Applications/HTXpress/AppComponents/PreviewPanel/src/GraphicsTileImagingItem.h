﻿#pragma once

#include <memory>
#include <QGraphicsRectItem>

#include "ResizableItemHandler.h"

namespace HTXpress::AppComponents::PreviewPanel {
    class GraphicsTileImagingItem final : public QGraphicsRectItem, public ResizableItemHandler {
    public:
        explicit GraphicsTileImagingItem();
        ~GraphicsTileImagingItem() override;

        auto SetSelectorFrameBounds(const QRectF& boundRect) -> void override;
        auto GetSelectorFrameBounds() const -> QRectF override;

        auto type() const -> int override;

    protected:
        auto boundingRect() const -> QRectF override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
    };
}
