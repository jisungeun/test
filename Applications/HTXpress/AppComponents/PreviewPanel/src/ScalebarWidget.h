﻿#pragma once

#include <memory>
#include <QWidget>

namespace HTXpress::AppComponents::PreviewPanel {
    class ScalebarWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = ScalebarWidget;

        explicit ScalebarWidget(QWidget* parent = nullptr);
        ~ScalebarWidget() override;

        auto SetUmPerPixel(const double& umPerPixel) -> void;

    public slots:
        void onRecvLodValueFromWidget(const double& lod);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
