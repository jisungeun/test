﻿#pragma once

#include <QPainter>

namespace HTXpress::AppComponents::PreviewPanel {
    class GraphicsItemHelper final {
    public:
        static auto GetConsistentPenWidth(QPainter* painter, const double& desiredWidth) -> double;
    };
}