﻿#include <QButtonGroup>
#include <QGraphicsOpacityEffect>

#include "ZoomWidget.h"
#include "ui_ZoomWidget.h"

namespace HTXpress::AppComponents::PreviewPanel {
    struct ZoomWidget::Impl {
        Ui::Form* ui{nullptr};
        QButtonGroup* zoomBtnGroup{nullptr};
    };

    ZoomWidget::ZoomWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui = new Ui::Form();
        d->ui->setupUi(this);

        setFixedWidth(24);
        setStyleSheet("QWidget{background-color:transparent;}");

        d->zoomBtnGroup = new QButtonGroup(this);
        d->zoomBtnGroup->addButton(d->ui->zoomIn, static_cast<int32_t>(ZoomButtonRole::ZoomIn));
        d->zoomBtnGroup->addButton(d->ui->zoomOut, static_cast<int32_t>(ZoomButtonRole::ZoomOut));
        d->zoomBtnGroup->addButton(d->ui->zoomFit, static_cast<int32_t>(ZoomButtonRole::ZoomFit));

        connect(d->zoomBtnGroup, &QButtonGroup::idClicked, this, &Self::onZoomingRequested);

        auto* oe = new QGraphicsOpacityEffect(this);
	    oe->setOpacity(0.2);
	    setGraphicsEffect(oe);
	    setAutoFillBackground(true);
    }

    ZoomWidget::~ZoomWidget() {
        delete d->ui;
    }

    void ZoomWidget::enterEvent(QEvent* event) {
        auto* oe = new QGraphicsOpacityEffect(this);
	    oe->setOpacity(1.0);
	    setGraphicsEffect(oe);
        QWidget::enterEvent(event);
    }

    void ZoomWidget::leaveEvent(QEvent* event) {
        auto* oe = new QGraphicsOpacityEffect(this);
	    oe->setOpacity(0.2);
	    setGraphicsEffect(oe);
        QWidget::leaveEvent(event);
    }


    void ZoomWidget::onZoomingRequested(int32_t which) {
        switch (static_cast<ZoomButtonRole>(which)) {
            case ZoomButtonRole::ZoomIn: 
                emit sigZoomIn();
                break;
            case ZoomButtonRole::ZoomOut: 
                emit sigZoomOut();
                break;
            case ZoomButtonRole::ZoomFit: 
                emit sigZoomFit();
                break;
            default: break;
        }
    }
}
