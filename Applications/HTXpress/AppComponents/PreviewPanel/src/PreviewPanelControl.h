#pragma once
#include <memory>

#include "PreviewPanelDefines.h"

namespace HTXpress::AppComponents::PreviewPanel {
    class PreviewPanelControl {
    public:
        using Self = PreviewPanelControl;
        using Pointer = std::shared_ptr<Self>;

        PreviewPanelControl();
        ~PreviewPanelControl();

        auto SetROI(double x, double y, double w, double h) -> void;
        auto SetTimeImagingArea(double x, double y, double w, double h) -> void;
        auto SetTileImagingCenter(double xInPixel, double yInPixel)->void;
        auto GetTileImagingArea() const -> Geometry;

        auto SetAreaVisible(bool show) -> void;

        auto Clear() -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
