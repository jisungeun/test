﻿#pragma once

#include <memory>

#include <QRectF>
#include <QGraphicsItem>

namespace HTXpress::AppComponents::PreviewPanel {
    class ResizableItemHandler {
    public:
        using Self = ResizableItemHandler;
        ResizableItemHandler();
        virtual ~ResizableItemHandler();

        auto SetCurrentLOD(const double& lod) -> void;
        auto SetCurrentPenWidth(const double& penWidth) -> void;

        auto DrawHandlesIfNeeded() -> void;
        auto SetOwnerItem(QGraphicsItem* owner) -> void;

        virtual auto GetSelectorFrameBounds() const -> QRectF = 0;
        virtual auto SetSelectorFrameBounds(const QRectF& boundRect) -> void = 0;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
