﻿#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

#include "GraphicsPreviewImageItem.h"
#include "PreviewImageLodGetter.h"
#include "PreviewPanelDefines.h"

namespace HTXpress::AppComponents::PreviewPanel {
    struct GraphicsPreviewImageItem::Impl {
        std::unique_ptr<PreviewImageLodGetter> lodGetter{};
        double borderLineWidth{};
    };

    GraphicsPreviewImageItem::GraphicsPreviewImageItem(const QPixmap& pixmap) : QGraphicsPixmapItem(pixmap), d{std::make_unique<Impl>()} {
        setZValue(-1000);
        d->lodGetter = std::make_unique<PreviewImageLodGetter>();
    }

    GraphicsPreviewImageItem::~GraphicsPreviewImageItem() {
    }

    auto GraphicsPreviewImageItem::SetLineWidthF(double lineWidth) -> void {
        d->borderLineWidth = lineWidth;
    }

    auto GraphicsPreviewImageItem::GetLodGetterObject() const -> PreviewImageLodGetter* {
        return d->lodGetter.get();
    }

    auto GraphicsPreviewImageItem::type() const -> int {
        return ItemType::PreviewImage;
    }

    auto GraphicsPreviewImageItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        d->lodGetter->SetLOD(QStyleOptionGraphicsItem::levelOfDetailFromTransform(painter->worldTransform()));

        painter->setPen(QPen(imageBorderColor, d->borderLineWidth, Qt::DotLine));
        painter->drawRect(boundingRect()); // border

        QGraphicsPixmapItem::paint(painter, option, widget);
    }

    auto GraphicsPreviewImageItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) -> void {
        emit sigItemDoubleClicked(mapToScene(event->pos()));
        QGraphicsPixmapItem::mouseDoubleClickEvent(event);
    }
}
