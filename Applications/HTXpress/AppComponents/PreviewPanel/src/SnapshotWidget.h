﻿#pragma once
#include <memory>
#include <QWidget>

namespace HTXpress::AppComponents::PreviewPanel {
    class SnapshotWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = SnapshotWidget;

        explicit SnapshotWidget(QWidget* parent = nullptr);
        ~SnapshotWidget() override;

    protected:
        void enterEvent(QEvent* event) override;
        void leaveEvent(QEvent* event) override;

    signals:
        void sigSnapshotRequest();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}