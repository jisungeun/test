﻿#pragma once

#include <QGraphicsItem>

#include <enum.h>

namespace HTXpress::AppComponents::PreviewPanel {
    namespace odcolor {
        const QColor odGray900("#172023");
        const QColor odGray800("#1B2629"); // TSX 배경
        const QColor odGray700("#243135");
        const QColor odGray600("#2D3E43");
        const QColor odGray500("#334449"); // default well color
        const QColor odGray400("#394C53");
        const QColor odGray300("#5f6F7A");
        const QColor odGray200("#6F8290"); // label color
        const QColor odWhite("#FFFFFF");
        const QColor odWhiteGray("#BBBBBB");
        const QColor odBlack("#000000");
        const QColor odBackground("#12191B");
        const QColor odPrimary("#00C2D3"); // 하늘색(토모큐브 로고색)
        const QColor odPrimarySolid("#27A2BF"); // 좀 진한 하늘색
        const QColor odSecondary("#5B8B97"); // 좀 짙은 하늘색
        const QColor odRed("#D55C56");
        const QColor odGreen("#314B33"); // 짙은 초록
        const QColor odGreenFocus("#64BC6A"); // 짙은 초록
    }

    struct Geometry {
        Geometry() = default;
        Geometry(double x, double y, double w, double h) {this->x = x; this->y = y; this->w = w; this->h = h;}

        auto operator==(const Geometry& other) const -> bool {
            if(fabs(x-other.x) >= std::numeric_limits<double>::epsilon()) return false;
            if(fabs(y-other.y) >= std::numeric_limits<double>::epsilon()) return false;
            if(fabs(w-other.w) >= std::numeric_limits<double>::epsilon()) return false;
            if(fabs(h-other.h) >= std::numeric_limits<double>::epsilon()) return false;
            return true;
        }

        auto operator!=(const Geometry& other) const -> bool {
            return !(*this==other);
        }

        double x{0.0};
        double y{0.0};
        double w{0.0};
        double h{0.0};
    };

    BETTER_ENUM(ItemType, int32_t,
                PreviewImage = QGraphicsItem::UserType,
                ROI,
                TileImaging)

    const QColor resizeHandleColor("#00FF00");
    const QColor tileBorderColor("#FF8522");
    const QColor imageBorderColor(odcolor::odWhite);
    const QColor roiBorderColor("#FFC000");
}
