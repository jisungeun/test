﻿#include <QGraphicsSceneMouseEvent>
#include <QPen>
#include <QBrush>

#include "ResizeHandleItem.h"
#include "PreviewPanelDefines.h"
#include "ResizableItemHandler.h"

namespace HTXpress::AppComponents::PreviewPanel {
    struct ResizeHandleItem::Impl {
        Position position;
        struct {
            double x;
            double y;
        } limitPos;
    };

    ResizeHandleItem::ResizeHandleItem(Position position) : d{std::make_unique<Impl>()}{
        setFlags(ItemIsMovable);
        d->position = position;

        setPen(Qt::NoPen);
        setBrush(QBrush(resizeHandleColor));
    }

    auto ResizeHandleItem::SetLimits(double x, double y) -> void {
        d->limitPos = {x, y};
    }

    void ResizeHandleItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event) {
        switch (d->position) {
            case Position::TopLeft: {
                if (const auto rectItem = dynamic_cast<ResizableItemHandler*>(parentItem())) {
                    QRectF boundingFrameRect = rectItem->GetSelectorFrameBounds();
                    if (d->limitPos.y > event->pos().y()) {
                        boundingFrameRect.setTop(event->pos().y());
                    }
                    if (d->limitPos.x > event->pos().x()) {
                        boundingFrameRect.setLeft(event->pos().x());
                    }
                    rectItem->SetSelectorFrameBounds(boundingFrameRect.normalized());
                }
            }
            break;

            case Position::TopRight: {
                if (const auto rectItem = dynamic_cast<ResizableItemHandler*>(parentItem())) {
                    QRectF boundingFrameRect = rectItem->GetSelectorFrameBounds();
                    if (d->limitPos.y > event->pos().y()) {
                        boundingFrameRect.setTop(event->pos().y());
                    }
                    if (d->limitPos.x < event->pos().x()) {
                        boundingFrameRect.setRight(event->pos().x());
                    }
                    rectItem->SetSelectorFrameBounds(boundingFrameRect.normalized());
                }
            }
            break;

            case Position::BottomLeft: {
                if (const auto rectItem = dynamic_cast<ResizableItemHandler*>(parentItem())) {
                    QRectF boundingFrameRect = rectItem->GetSelectorFrameBounds();
                    if (d->limitPos.y < event->pos().y()) {
                        boundingFrameRect.setBottom(event->pos().y());
                    }
                    if (d->limitPos.x > event->pos().x()) {
                        boundingFrameRect.setLeft(event->pos().x());
                    }
                    rectItem->SetSelectorFrameBounds(boundingFrameRect.normalized());
                }
            }
            break;

            case Position::BottomRight: {
                if (const auto rectItem = dynamic_cast<ResizableItemHandler*>(parentItem())) {
                    QRectF boundingFrameRect = rectItem->GetSelectorFrameBounds();
                    if (d->limitPos.y < event->pos().y()) {
                        boundingFrameRect.setBottom(event->pos().y());
                    }
                    if (d->limitPos.x < event->pos().x()) {
                        boundingFrameRect.setRight(event->pos().x());
                    }
                    rectItem->SetSelectorFrameBounds(boundingFrameRect.normalized());
                }
            }
            break;
        }
    }
}
