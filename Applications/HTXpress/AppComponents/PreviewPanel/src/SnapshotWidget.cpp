﻿#include <QToolButton>
#include <QLayout>
#include <QFileDialog>
#include <QDateTime>
#include <QStandardPaths>
#include <QGraphicsOpacityEffect>

#include "SnapshotWidget.h"

namespace HTXpress::AppComponents::PreviewPanel {
    struct SnapshotWidget::Impl {
        QToolButton* snapshot{nullptr};
    };

    SnapshotWidget::SnapshotWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->snapshot = new QToolButton(this);
        d->snapshot->setStyleSheet("QToolButton {border:0px none; background-color:transparent;}");
        d->snapshot->setFixedSize(24, 24);
        d->snapshot->setIconSize({24, 24});
        d->snapshot->setIcon(QIcon(":/img/ic-sub-cam.svg"));
        d->snapshot->setToolTip("Capture");

        connect(d->snapshot, &QToolButton::clicked, this, &Self::sigSnapshotRequest);

        const auto layout = new QVBoxLayout(this);
        layout->addWidget(d->snapshot, 0, Qt::AlignCenter);
        layout->setContentsMargins(0, 0, 0, 0);

        setLayout(layout);

        auto* oe = new QGraphicsOpacityEffect(this);
	    oe->setOpacity(0.2);
	    setGraphicsEffect(oe);
	    setAutoFillBackground(true);
    }

    SnapshotWidget::~SnapshotWidget() {
    }

    void SnapshotWidget::enterEvent(QEvent* event) {
        auto* oe = new QGraphicsOpacityEffect(this);
	    oe->setOpacity(1.0);
	    setGraphicsEffect(oe);
        QWidget::enterEvent(event);
    }

    void SnapshotWidget::leaveEvent(QEvent* event) {
        auto* oe = new QGraphicsOpacityEffect(this);
	    oe->setOpacity(0.2);
	    setGraphicsEffect(oe);
        QWidget::leaveEvent(event);
    }

}
