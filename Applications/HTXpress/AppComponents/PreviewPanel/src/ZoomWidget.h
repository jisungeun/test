﻿#pragma once

#include <QWidget>

namespace HTXpress::AppComponents::PreviewPanel {
    class ZoomWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = ZoomWidget;

        enum class ZoomButtonRole {
            ZoomIn = 77,
            ZoomOut,
            ZoomFit,
        };
        Q_ENUM(ZoomButtonRole)

        explicit ZoomWidget(QWidget* parent = nullptr);
        ~ZoomWidget() override;

    protected:
        void enterEvent(QEvent* event) override;
        void leaveEvent(QEvent* event) override;

    signals:
        void sigZoomIn();
        void sigZoomOut();
        void sigZoomFit();

    private slots:
        void onZoomingRequested(int32_t which);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
