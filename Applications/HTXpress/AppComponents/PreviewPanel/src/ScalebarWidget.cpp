﻿#include <QLabel>
#include <QLayout>
#include <QPainter>

#include "ScalebarWidget.h"

namespace HTXpress::AppComponents::PreviewPanel {
    struct ScalebarWidget::Impl {
        QLabel* text;
        QLabel* scale;
        const int32_t scaleLabelWidth{50};
        const int32_t scaleLabelHeight{5};
        double umPerPixel{1};
        double currLod{1};
        const QString suffix = QString(" %1m").arg(QChar(0x00B5));

        auto UpdateScaleText() -> void;
    };

    auto ScalebarWidget::Impl::UpdateScaleText() -> void {
        if(currLod < 0.000001) {
            text->setText("?");
        } else {
            const auto scaleValue = [=]()->int32_t {
                auto value = scaleLabelWidth * umPerPixel / currLod;
                if(value < 3.0) return static_cast<int32_t>(std::round(value));
                return static_cast<int32_t>(std::round(value / 5.0) * 5);
            }();
            text->setText(QString::number(scaleValue, 'g', 4) + suffix);

            const auto width = scaleValue * currLod / umPerPixel;
            scale->setFixedSize(width, scaleLabelHeight);
        }
    }

    ScalebarWidget::ScalebarWidget(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        setAttribute(Qt::WA_TransparentForMouseEvents);

        d->text = new QLabel(this);
        d->text->setStyleSheet("QLabel { background-color : transparent; color : white; font: bold;}");
        d->text->setAlignment(Qt::AlignLeft);

        d->scale = new QLabel(this);
        d->scale->setFixedSize(d->scaleLabelWidth, d->scaleLabelHeight);
        d->scale->setStyleSheet("QLabel { background-color : white;}");
        d->scale->setAlignment(Qt::AlignLeft);

        const auto layout = new QVBoxLayout();
        layout->addWidget(d->text);
        layout->addWidget(d->scale);
        layout->setMargin(0);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(1);
        layout->setSizeConstraint(QLayout::SetFixedSize);
        setLayout(layout);
    }

    ScalebarWidget::~ScalebarWidget() {
    }

    auto ScalebarWidget::SetUmPerPixel(const double& umPerPixel) -> void {
        d->umPerPixel = umPerPixel;
        d->UpdateScaleText();
    }

    void ScalebarWidget::onRecvLodValueFromWidget(const double& lod) {
        d->currLod = lod;
        d->UpdateScaleText();
    }
}
