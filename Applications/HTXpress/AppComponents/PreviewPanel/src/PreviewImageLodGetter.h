﻿#pragma once
#include <QObject>

namespace HTXpress::AppComponents::PreviewPanel {
    class PreviewImageLodGetter : public QObject {
        Q_OBJECT
    public:
        PreviewImageLodGetter(const PreviewImageLodGetter&) = delete;
        auto operator=(const PreviewImageLodGetter& other) -> PreviewImageLodGetter& = delete;
        PreviewImageLodGetter(PreviewImageLodGetter&&) = delete;
        auto operator=(PreviewImageLodGetter&&) -> PreviewImageLodGetter& = delete;

    public:
        explicit PreviewImageLodGetter(QObject* parent = nullptr);
        ~PreviewImageLodGetter() override = default;

        auto GetLOD() const -> double;
        auto SetLOD(const double& _lod) -> void;

    signals:
        void sigLodChanged(const double& lod);

    private:
        double lod{0};
    };
}
