#include <QtGui/QtGui>
#include <QDebug>
#include <QGridLayout>
#include <QFileDialog>
#include <QButtonGroup>
#include <QInputDialog>

#include "PreviewTestMainWindow.h"
#include "ui_PreviewTestMainWindow.h"
#include "PreviewPanel.h"

namespace HTXpress::AppComponents::PreviewPanel::TEST {
    struct PreviewTestMainWindow::Impl {
        explicit Impl(Self* self) : self(self){}
        Self* self{};
        Ui::TestMainWindowClass ui;
        PreviewPanel* preview{nullptr};

        auto SetTestWidget() -> void;
        auto Connect() -> void;
    };

    auto PreviewTestMainWindow::Impl::SetTestWidget() -> void {
        preview = new PreviewPanel(self);
        const auto layout = new QGridLayout;
        layout->addWidget(preview);
        layout->setContentsMargins(0, 0, 0, 0);
        ui.viewFrame->setContentsMargins(0, 0, 0, 0);
        ui.viewFrame->setLayout(layout);
    }

    auto PreviewTestMainWindow::Impl::Connect() -> void {
        connect(ui.loadPreviewImage, &QPushButton::pressed, self, &Self::onLoadPreviewImage);
        connect(ui.clearPreview, &QPushButton::pressed, self, &Self::onClearPreview);
        connect(ui.tileImaging, &QCheckBox::toggled, self, &Self::onTileImagingEnabled);

        connect(ui.fovX, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onChangeFov);
        connect(ui.fovY, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onChangeFov);
        connect(ui.fovW, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onChangeFov);
        connect(ui.fovH, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onChangeFov);

        connect(ui.acqX, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onChangeTileArea);
        connect(ui.acqY, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onChangeTileArea);
        connect(ui.acqW, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onChangeTileArea);
        connect(ui.acqH, qOverload<double>(&QDoubleSpinBox::valueChanged), self, &Self::onChangeTileArea);

        connect(ui.setUmPerPixelBtn, &QPushButton::clicked, self, &Self::onSetUmPerPixel);

        connect(preview, &PreviewPanel::sigDoPreviewScan, self, &Self::onRecvPreviewScan);
        connect(preview, &PreviewPanel::sigRequestSetPreviewArea, self, &Self::onRecvCustomPreviewScan);
        connect(preview, &PreviewPanel::sigCancelSetPreviewArea, self, &Self::onRecvCanceCustomPreviewScan);
        connect(preview, &PreviewPanel::sigChangeTileImagingArea, self, &Self::onRecvChangeAcquisitionArea);
        connect(preview, &PreviewPanel::sigMoveLive, self, &Self::onRecvChangeCurrentFOV);
    }

    PreviewTestMainWindow::PreviewTestMainWindow(QWidget* parent) : QMainWindow(parent), d{std::make_unique<Impl>(this)} {
        d->ui.setupUi(this);
        d->ui.tabWidget->setCurrentIndex(0);
        d->ui.viewFrame->setMinimumSize(500, 500);
        d->SetTestWidget();
        d->Connect();
    }

    PreviewTestMainWindow::~PreviewTestMainWindow() {
    }

    void PreviewTestMainWindow::onLoadPreviewImage() {
        const QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"));
        const QImage image(fileName);
        //TODO Implement this part. Not update it at once...
    }

    void PreviewTestMainWindow::onClearPreview() {
        d->preview->Clear();
    }

    void PreviewTestMainWindow::onChangeFov() {
        d->preview->SetROI(d->ui.fovX->value(), d->ui.fovY->value(), d->ui.fovW->value(), d->ui.fovH->value());
    }

    void PreviewTestMainWindow::onChangeTileArea() {
        d->preview->SetTileImagingArea(d->ui.acqX->value(), d->ui.acqY->value(), d->ui.acqW->value(), d->ui.acqH->value());
    }

    void PreviewTestMainWindow::onTileImagingEnabled(bool checked) {
        d->ui.addAcquisitionArea->setEnabled(checked);
        d->preview->SetTileImagingActivation(checked);
        if (checked) {
            d->preview->SetTileImagingArea(d->ui.acqX->value(), d->ui.acqY->value(), d->ui.acqW->value(), d->ui.acqH->value());
        }
        else {

        }
    }

    void PreviewTestMainWindow::onSetUmPerPixel() {
        d->preview->SetUmPerPixel(d->ui.spUmPerPixel->value());
    }

    void PreviewTestMainWindow::onRecvChangeAcquisitionArea(double x, double y, double w, double h) {
        d->ui.log->append(QString("sigChangeTileImagingArea(%1,%2,%3,%4").arg(x).arg(y).arg(w).arg(h));

        d->ui.acqX->blockSignals(true);
        d->ui.acqY->blockSignals(true);
        d->ui.acqW->blockSignals(true);
        d->ui.acqH->blockSignals(true);

        d->ui.acqX->setValue(x);
        d->ui.acqY->setValue(y);
        d->ui.acqW->setValue(w);
        d->ui.acqH->setValue(h);

        d->ui.acqX->blockSignals(false);
        d->ui.acqY->blockSignals(false);
        d->ui.acqW->blockSignals(false);
        d->ui.acqH->blockSignals(false);
    }

    void PreviewTestMainWindow::onRecvChangeCurrentFOV(double x, double y) {
        d->ui.log->append(QString("sigMoveLive(%1,%2)").arg(x).arg(y));
        d->ui.fovX->blockSignals(true);
        d->ui.fovY->blockSignals(true);

        d->ui.fovX->setValue(x);
        d->ui.fovY->setValue(y);

        d->ui.fovX->blockSignals(false);
        d->ui.fovY->blockSignals(false);

        onChangeFov();
    }

    void PreviewTestMainWindow::onRecvPreviewScan(const PreviewPanel::PreviewScanType& type) {
        Q_UNUSED(type)
        d->ui.log->append("sigDoPreviewScan");
    }

    void PreviewTestMainWindow::onRecvCustomPreviewScan() {
        d->ui.log->append("sigRequestSetPreviewArea");
    }

    void PreviewTestMainWindow::onRecvCanceCustomPreviewScan() {
        d->ui.log->append("sigCancelSetPreviewArea");
    }
}
