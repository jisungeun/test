#pragma once

#include <memory>
#include <QtWidgets/QMainWindow>

#include <PreviewPanel.h>

namespace HTXpress::AppComponents::PreviewPanel::TEST {
	class PreviewTestMainWindow : public QMainWindow {
		Q_OBJECT
	public:
		using Self = PreviewTestMainWindow;
		explicit PreviewTestMainWindow(QWidget* parent = nullptr);
		~PreviewTestMainWindow() override;

	public slots:
		void onLoadPreviewImage();
		void onClearPreview();
		void onChangeFov();
		void onChangeTileArea();
		void onTileImagingEnabled(bool checked);
	    void onSetUmPerPixel();

		void onRecvChangeAcquisitionArea(double x, double y, double w, double h);
		void onRecvChangeCurrentFOV(double x, double y);
		void onRecvPreviewScan(const PreviewPanel::PreviewScanType& type);
		void onRecvCustomPreviewScan();
	    void onRecvCanceCustomPreviewScan();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

    
}
