#include "PreviewTestMainWindow.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HTXpress::AppComponents::PreviewPanel::TEST::PreviewTestMainWindow w;
    w.show();
    return a.exec();
}
