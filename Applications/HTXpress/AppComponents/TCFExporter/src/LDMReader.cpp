#include <TcfH5GroupRoiLdmDataReader.h>

#include "LDMReader.h"

namespace HTXpress::AppComponents::TCFExporter {
    struct LDMReader::Impl {
        H5::H5File h5File;
        QString path;
        bool isOpen{ false };
    };

    LDMReader::LDMReader() : IReader(), d{new Impl} {
    }

    LDMReader::~LDMReader() {
    }

    auto LDMReader::Open(const QString& path) -> bool {
        try {
            if(d->isOpen && (d->path != path)) {
                d->h5File.close();
                d->isOpen = false;
            }

            if(!d->isOpen) {
                d->h5File.openFile(path.toLatin1().constData(), H5F_ACC_RDONLY);
                d->isOpen = true;
                d->path = path;
            }
        } catch(H5::Exception& ex) {
            Q_UNUSED(ex)
            return false;
        }

        return true;
    }

    auto LDMReader::Read(const QString& dataId,
                         int32_t timeSlice,
                         Type type,
                         const std::vector<int32_t>& offset, 
                         const std::vector<int32_t>& count) const -> TC::IO::MemoryChunk::Pointer {
        Q_UNUSED(type)

        if(offset.size() < 2) return nullptr;
        const auto offsetPoint = [&offset]()->TC::IO::Point {
            if(offset.size() == 2) return TC::IO::Point(offset.at(0), offset.at(1));
            return TC::IO::Point(offset.at(0), offset.at(1), offset.at(2));
        }();

        if(count.size() != offset.size()) return nullptr;
        const auto pointCount = [&count]()->TC::IO::Count {
            if(count.size() == 2) return TC::IO::Count(count.at(0), count.at(1));
            return TC::IO::Count(count.at(0), count.at(1), count.at(2));
        }();

        const auto strGroup = QString("/Data/%1/%2").arg(dataId).arg(timeSlice, 6, 10, QLatin1Char('0'));
        auto group = d->h5File.openGroup(strGroup.toLatin1().constData());

        TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
        reader.SetLdmDataGroup(group);
        reader.SetReadingRoi({offsetPoint, pointCount});
        reader.SetSamplingStep(1);
        if (dataId == "BF") {
            reader.SetLdmDataIs2DStack(true);
        }

        return reader.Read();
    }
}
