#pragma once
#include <memory>
#include <vector>

#include "Defines.h"
#include "TCFExporter.h"

#include <MemoryChunk.h>

namespace HTXpress::AppComponents::TCFExporter {
    class TCFWriter {
    public:
        explicit TCFWriter(Exporter::Extension extension);
        ~TCFWriter();

        auto Write(TC::IO::MemoryChunk::Pointer memoryChunk, 
                   const std::vector<float>& spacing, 
                   const QString& path)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
