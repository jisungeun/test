#include <TCRawWriter.h>

#include "RawWriter.h"

namespace HTXpress::AppComponents::TCFExporter {
    RawWriter::RawWriter() : IWriter() {
    }

    RawWriter::~RawWriter() {
    }

    auto RawWriter::Write(TC::IO::MemoryChunk::Pointer memoryChunk,
                          const std::vector<float>& spacing,
                          const QString& path) -> bool {
        using DataType = TC::IO::RawWriter::Writer::Type;

        const auto dimension = memoryChunk->GetDimension();
        const auto sizeInBytes = memoryChunk->GetSizeInBytes();

        auto type = [&]()->DataType {
            DataType type{ DataType::NOTDEFINED };

            switch(memoryChunk->GetDataType()) {
            case TC::IO::ChunkDataType::Int8Type:
                type = DataType::INT8;
                break;
            case TC::IO::ChunkDataType::UInt8Type:
                type = DataType::UINT8;
                break;
            case TC::IO::ChunkDataType::Int16Type:
                type = DataType::INT16;
                break;
            case TC::IO::ChunkDataType::UInt16Type:
                type = DataType::UINT16;
                break;
            case TC::IO::ChunkDataType::Int32Type:
                type = DataType::INT32;
                break;
            case TC::IO::ChunkDataType::UInt32Type:
                type = DataType::UINT32;
                break;
            case TC::IO::ChunkDataType::FloatType:
                type = DataType::FLOAT;
                break;
            case TC::IO::ChunkDataType::DoubleType:
                type = DataType::DOUBLE;
                break;
            }

            return type;
        }();

        if(type == DataType::NOTDEFINED) return false;

        auto writer = TC::IO::RawWriter::Writer(path, 
                                                {dimension.X(), dimension.Y(), dimension.Z()},
                                                spacing,
                                                type);
        return writer.Append(static_cast<char*>(memoryChunk->GetDataPointer()), 
                             sizeInBytes);
    }
}