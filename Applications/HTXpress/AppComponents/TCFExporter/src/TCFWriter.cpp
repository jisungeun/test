#include "RawWriter.h"
#include "TCFWriter.h"

namespace HTXpress::AppComponents::TCFExporter {
    struct TCFWriter::Impl {
        std::shared_ptr<IWriter> writer{ nullptr };
    };

    TCFWriter::TCFWriter(Exporter::Extension extension) : d{new Impl} {
        switch(extension) {
        case Exporter::Extension::RAW:
            d->writer.reset(new RawWriter());
            break;
        }
    }

    TCFWriter::~TCFWriter() {
    }

    auto TCFWriter::Write(TC::IO::MemoryChunk::Pointer memoryChunk, const std::vector<float>& spacing, const QString& path) -> bool {
        if(d->writer == nullptr) return false;
        return d->writer->Write(memoryChunk, spacing, path);
    }
}