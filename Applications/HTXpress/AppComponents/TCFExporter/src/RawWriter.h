#pragma once
#include <memory>

#include "IWriter.h"

namespace HTXpress::AppComponents::TCFExporter {
    class RawWriter : public IWriter {
    public:
        RawWriter();
        ~RawWriter() override;

        auto Write(TC::IO::MemoryChunk::Pointer memoryChunk,
                   const std::vector<float>& spacing,
                   const QString& path) -> bool override;
    };
}