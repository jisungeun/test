#include <TCFMetaReader.h>

#include <QMap>
#include <QList>

#include "IReader.h"
#include "LDMReader.h"
#include "NonLDMReader.h"

#include "TCFReader.h"

namespace HTXpress::AppComponents::TCFExporter {
    struct TCFReader::Impl {
        std::unique_ptr<IReader> reader{ nullptr };
        TC::IO::TCFMetaReader::Meta::Pointer meta{ nullptr };
        Type dataType{ Type::HT3D };

        auto ReadMetaData(const QString& path)->bool;
    };

    auto TCFReader::Impl::ReadMetaData(const QString& path) -> bool {
        auto metaReader = TC::IO::TCFMetaReader();

        const auto isLDM = metaReader.ReadIsLDM(path);
        if(isLDM) reader.reset(new LDMReader());
        else reader.reset(new NonLDMReader());

        if(!reader) return false;

        meta = metaReader.Read(path);

        return true;
    }

    TCFReader::TCFReader() : d{new Impl} {
    }

    TCFReader::~TCFReader() {
    }

    auto TCFReader::Open(const QString& path, Type type) -> bool {
        if(!d->ReadMetaData(path)) {
            return false;
        }

        d->dataType = type;

        if(!d->reader->Open(path)) return false;
        return true;
    }

    auto TCFReader::GetChannels() const -> std::vector<int32_t> {
        switch(d->dataType) {
        case Type::FL3D:
            return d->meta->data.data3DFL.channelList;
        case Type::FL2DMIP:
            return d->meta->data.data2DFLMIP.channelList;
        }
        return {0};
    }

    auto TCFReader::GetTimeCount(int32_t channel) const -> int32_t {
        switch(d->dataType) {
        case Type::HT3D:
            return d->meta->data.data3D.timePoints.size();
        case Type::HT2DMIP:
            return d->meta->data.data2DMIP.timePoints.size();
        case Type::FL3D:
            return d->meta->data.data3DFL.timePoints[channel].size();
        case Type::FL2DMIP:
            return d->meta->data.data2DFLMIP.timePoints[channel].size();
        case Type::BF:
            return d->meta->data.dataBF.timePoints.size();
        }
        return 0;
    }

    auto TCFReader::GetSize() const -> std::vector<int32_t> {
        switch(d->dataType) {
        case Type::HT3D:
            return {d->meta->data.data3D.sizeX, d->meta->data.data3D.sizeY, d->meta->data.data3D.sizeZ};
        case Type::HT2DMIP:
            return {d->meta->data.data2DMIP.sizeX, d->meta->data.data2DMIP.sizeY};
        case Type::FL3D:
            return {d->meta->data.data3DFL.sizeX, d->meta->data.data3DFL.sizeY, d->meta->data.data3DFL.sizeZ};
        case Type::FL2DMIP:
            return {d->meta->data.data2DFLMIP.sizeX, d->meta->data.data2DFLMIP.sizeY};
        case Type::BF:
            return {d->meta->data.dataBF.sizeX, d->meta->data.dataBF.sizeY};
        }
        return {};
    }

    auto TCFReader::GetResolution() const -> std::vector<double> {
        switch(d->dataType) {
        case Type::HT3D:
            return {d->meta->data.data3D.resolutionX, d->meta->data.data3D.resolutionY, d->meta->data.data3D.resolutionZ};
        case Type::HT2DMIP:
            return {d->meta->data.data2DMIP.resolutionX, d->meta->data.data2DMIP.resolutionY};
        case Type::FL3D:
            return {d->meta->data.data3DFL.resolutionX, d->meta->data.data3DFL.resolutionY, d->meta->data.data3DFL.resolutionZ};
        case Type::FL2DMIP:
            return {d->meta->data.data2DFLMIP.resolutionX, d->meta->data.data2DFLMIP.resolutionY};
        case Type::BF:
            return {d->meta->data.dataBF.resolutionX, d->meta->data.dataBF.resolutionY};
        }
        return {};
    }

    auto TCFReader::Read(int32_t timeSlice, int32_t channel) const -> TC::IO::MemoryChunk::Pointer {
        if(d->reader == nullptr) return nullptr;

        const auto strGroup = [&]()->QString {
            QString str;

            switch(d->dataType) {
            case Type::HT3D:
                str = "3D";
                break;
            case Type::HT2DMIP:
                str = "2DMIP";
                break;
            case Type::FL3D:
                str = QString("3DFL/CH%1").arg(channel);
                break;
            case Type::FL2DMIP:
                str = QString("2DFLMIP/CH%1").arg(channel);
                break;
            case Type::BF:
                str = "BF";
                break;
            }

            return str;
        }();

        const auto size = GetSize();
        const auto offset = [&]()->std::vector<int32_t> {
            if(size.size() == 2) return {0, 0};
            return {0, 0, 0};
        }();

        return d->reader->Read(strGroup, timeSlice, d->dataType, offset, GetSize());
    }

    auto TCFReader::GetMinValue(int32_t timeSlice, int32_t channel) const -> float {
        float minValue;

        switch (d->dataType) {
        case Type::HT3D:
            minValue = d->meta->data.data3D.riMinList[timeSlice];
            break;
        case Type::HT2DMIP:
            minValue = d->meta->data.data2DMIP.riMinList[timeSlice];
            break;
        case Type::FL3D: 
            minValue = d->meta->data.data3DFL.minIntensityList[channel][timeSlice];
            break;
        case Type::FL2DMIP:
            minValue = d->meta->data.data2DFLMIP.minIntensityList[channel][timeSlice];
            break;
        case Type::BF:
            minValue = 0;
            break;
        default:
            minValue = 0;
        }

        return minValue;
    }
}
