#define LOGGER_TAG "[NonLDMReader]"
#include <TCLogger.h>
#include <TCFSimpleReader.h>

#include "NonLDMReader.h"

namespace HTXpress::AppComponents::TCFExporter {
    struct NonLDMReader::Impl {
        QString path;

        auto GetChannelFromDataID(const QString& dataID) const -> int32_t;
    };

    auto NonLDMReader::Impl::GetChannelFromDataID(const QString& dataID) const -> int32_t {
        bool ok{};
        const auto channel = dataID.right(1).toInt(&ok);
        if(!ok) {
            QLOG_ERROR() << "Failed to get channel index from" << dataID;
        }

        return channel;
    }

    NonLDMReader::NonLDMReader() : IReader(), d{new Impl} {
    }

    NonLDMReader::~NonLDMReader() {
    }

    auto NonLDMReader::Open(const QString& path) -> bool {
        d->path = path;
        return true;
    }

    auto NonLDMReader::Read(const QString& dataId,
                            int32_t timeSlice,
                            Type type,
                            const std::vector<int32_t>& offset,
                            const std::vector<int32_t>& count) const -> TC::IO::MemoryChunk::Pointer {
        using DataType = TC::IO::VolumeData::Type;

        Q_UNUSED(offset)
        Q_UNUSED(count)

        auto volumeData = [&]()->TC::IO::VolumeData {
            TC::IO::TCFSimpleReader reader(d->path);

            switch(type) {
            case Type::HT3D:
                return reader.ReadHT3D(timeSlice);
            case Type::HT2DMIP:
                return reader.ReadHTMIP(timeSlice);
            case Type::FL3D:
                return reader.ReadFL3D(d->GetChannelFromDataID(dataId), timeSlice);
            case Type::FL2DMIP:
                return reader.ReadFLMIP(d->GetChannelFromDataID(dataId), timeSlice);
            case Type::BF:
                return reader.ReadBF(timeSlice);
            }
            return TC::IO::VolumeData();
        }();

        if(!volumeData.IsValid()) return nullptr;

        const auto [dimX, dimY, dimZ] = volumeData.GetDimension();
        const auto dimension = TC::IO::Dimension(dimX, dimY, dimZ);

        TC::IO::MemoryChunk::Pointer memoryChunck{ new TC::IO::MemoryChunk() };
        switch(volumeData.GetDataType()) {
        case DataType::Int8Type:
            memoryChunck->SetData(volumeData.GetDataAsInt8(), dimension);
            break;
        case DataType::UInt8Type:
            memoryChunck->SetData(volumeData.GetDataAsUInt8(), dimension);
            break;
        case DataType::Int16Type:
            memoryChunck->SetData(volumeData.GetDataAsInt16(), dimension);
            break;
        case DataType::UInt16Type:
            memoryChunck->SetData(volumeData.GetDataAsUInt16(), dimension);
            break;
        case DataType::Int32Type:
            memoryChunck->SetData(volumeData.GetDataAsInt32(), dimension);
            break;
        case DataType::UInt32Type:
            memoryChunck->SetData(volumeData.GetDataAsUInt32(), dimension);
            break;
        case DataType::FloatType:
            memoryChunck->SetData(volumeData.GetDataAsFloat(), dimension);
            break;
        case DataType::DoubleType:
            memoryChunck->SetData(volumeData.GetDataAsDouble(), dimension);
            break;
        }

        return memoryChunck;
    }
}