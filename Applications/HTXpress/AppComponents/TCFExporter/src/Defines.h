#pragma once

namespace HTXpress::AppComponents::TCFExporter {
    enum class Type {
        HT3D,
        HT2DMIP,
        FL3D,
        FL2DMIP,
        BF
    };
}