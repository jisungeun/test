#pragma once
#include <memory>

#include "IReader.h"

namespace HTXpress::AppComponents::TCFExporter {
    class LDMReader : public IReader {
    public:
        LDMReader();
        ~LDMReader() override;

        auto Open(const QString& path) -> bool override;
        auto Read(const QString& dataId,
                  int32_t timeSlice,
                  Type type,
                  const std::vector<int32_t>& offset, 
                  const std::vector<int32_t>& count) const -> TC::IO::MemoryChunk::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}