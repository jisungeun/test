#pragma once
#include <memory>
#include <vector>
#include <QString>

#include <MemoryChunk.h>

#include "Defines.h"

namespace HTXpress::AppComponents::TCFExporter {
    class IReader {
    public:
        IReader();
        virtual ~IReader();

        virtual auto Open(const QString& path)->bool = 0;
        virtual auto Read(const QString& dataID,
                          int32_t timeSlice,
                          Type type,
                          const std::vector<int32_t>& offset, 
                          const std::vector<int32_t>& count) const->TC::IO::MemoryChunk::Pointer = 0;
    };
}