#define LOGGER_TAG "[TCFExporter]"
#include <QFileInfo>

#include <TCLogger.h>

#include "TCFReader.h"
#include "TCFWriter.h"
#include "TCFExporter.h"
 
namespace HTXpress::AppComponents::TCFExporter {
    struct Exporter::Impl {
        QString sourcePath;
        QString dataID;
        QString targetPath;
        Type dataType{ Type::HT3D };
        QString strExt{ "raw" };
        Extension ext{ Extension::RAW };

        bool isFL{ false };
        QString baseName;

        QString lastError;

        auto SetType(const QString& strDataId)->void {
            isFL = false;

            if(strDataId == "3D") dataType = Type::HT3D;
            if(strDataId == "2DMIP") dataType = Type::HT2DMIP;
            if(strDataId == "BF") dataType = Type::BF;

            if(strDataId == "3DFL") {
                dataType = Type::FL3D;
                isFL = true;
            }

            if(strDataId == "2DFLMIP") {
                dataType = Type::FL2DMIP;
                isFL = true;
            }
        }

        auto GetTypeStr(Type type) const->QString {
            QString str{ "ND" };

            switch(type) {
            case Type::HT3D:
                str = "HT";
                break;
            case Type::HT2DMIP:
                str = "HTMIP";
                break;
            case Type::FL3D:
                str = "FL";
                break;
            case Type::FL2DMIP:
                str = "FLMIP";
                break;
            case Type::BF:
                str = "BF";
                break;
            }

            return str;
        }

        auto SetError(const QString& str)->void {
            lastError = str;
            QLOG_ERROR() << str;
        }

        auto BuildTargetPath(int32_t timeIndex, int32_t channel) const->QString {

            if(isFL) {
                return QString("%1/%2_CH%3_%4.%5").arg(targetPath)
                                                  .arg(baseName)
                                                  .arg(channel)
                                                  .arg(timeIndex, 3, 10, QLatin1Char('0'))
                                                  .arg(strExt);
            }

            return QString("%1/%2_%3.%4").arg(targetPath)
                                         .arg(baseName)
                                         .arg(timeIndex, 3, 10, QLatin1Char('0'))
                                         .arg(strExt);
        }

        static auto ConvertRIValuesToFloat(const TC::IO::MemoryChunk::Pointer& riChunk, const float& minValue)->TC::IO::MemoryChunk::Pointer;
    };

    auto Exporter::Impl::ConvertRIValuesToFloat(const TC::IO::MemoryChunk::Pointer& riChunk, const float& minValue) -> TC::IO::MemoryChunk::Pointer {
        const auto dataDimension = riChunk->GetDimension();
        const auto numberOfElements = dataDimension.GetNumberOfElements();

        auto riFloatChunk = std::make_shared<TC::IO::MemoryChunk>();
        const std::shared_ptr<float[]> convertedData{ new float[numberOfElements]() };
        if (riChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type) {
            const auto riUShortDataPointer = static_cast<uint16_t*>(riChunk->GetDataPointer());

            for (size_t index = 0; index < numberOfElements; ++index) {
                convertedData.get()[index] = static_cast<float>(riUShortDataPointer[index]) / 10000;
            }

            riFloatChunk->SetData(convertedData, dataDimension);
        } else if (riChunk->GetDataType() == +TC::IO::ChunkDataType::UInt8Type) {
            const auto riUCharDataPointer = static_cast<uint8_t*>(riChunk->GetDataPointer());

            for (size_t index = 0; index < numberOfElements; ++index) {
                convertedData.get()[index] = static_cast<float>(riUCharDataPointer[index]) / 1000 + minValue;
            }

            riFloatChunk->SetData(convertedData, dataDimension);
        }

        return riFloatChunk;
    }

    Exporter::Exporter(QObject* parent) : QObject(parent), d{new Impl} {
    }

    Exporter::~Exporter() {
    }

    auto Exporter::SetSource(const QString& path, const QString& dataID) -> void {
        d->sourcePath = path;
        d->dataID = dataID;
        d->SetType(dataID);

        QFileInfo finfo(d->sourcePath);
        auto filename = finfo.fileName();
        filename = filename.remove("." + finfo.suffix());
        d->baseName = QString("%1_%2").arg(filename).arg(d->GetTypeStr(d->dataType));
    }

    auto Exporter::SetTarget(const QString& path, Extension ext) -> void {
        d->targetPath = path;

        switch(ext) {
        case Extension::RAW:
            d->strExt = "raw";
            break;
        }

        d->ext = ext;
    }

    auto Exporter::Export() -> bool {
        auto reader = TCFReader();

        if(!reader.Open(d->sourcePath, d->dataType)) {
            const auto strError = QString("It fails to access data [path:%1]").arg(d->sourcePath);
            d->SetError(strError);
            return false;
        }

        const auto channels = reader.GetChannels();
        auto totalTimeCount = 0;

        for(const auto channel : channels) {
            totalTimeCount += reader.GetTimeCount(channel);
        }

        emit sigTotalCount(totalTimeCount);

        int32_t count{};
        for(const auto channel : channels) {
            const auto timeCount = reader.GetTimeCount(channel);
            for(auto timeIdx=0; timeIdx<timeCount; timeIdx++) {
                auto memoryChunk = reader.Read(timeIdx, channel);
                if(memoryChunk == nullptr) {
                    const auto strError = QString("It fails to read data [path:%1 id:%2 channel:%3 timeIndex:%4]")
                        .arg(d->sourcePath).arg(d->dataID).arg(channel).arg(timeIdx);
                    d->SetError(strError);
                    return false;
                }

                if ((d->dataType == Type::HT3D) || (d->dataType == Type::HT2DMIP)) {
                    memoryChunk = d->ConvertRIValuesToFloat(memoryChunk, reader.GetMinValue(timeIdx, channel));
                }
                
                const auto outPath = d->BuildTargetPath(timeIdx, channel);

                auto writer = TCFWriter(d->ext);
                if(!writer.Write(memoryChunk, {1, 1, 1}, outPath)) {
                    const auto strError = QString("It fails to write image to %1").arg(outPath);
                    d->SetError(strError);
                    return false;
                }

                emit sigCurrentCount(++count);
            }
        }

        return true;
    }

    auto Exporter::GetLastError() const -> QString {
        return d->lastError;
    }
}
