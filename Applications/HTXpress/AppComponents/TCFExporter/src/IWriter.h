#pragma once
#include <memory>
#include <vector>

#include <QString>

#include <MemoryChunk.h>

namespace HTXpress::AppComponents::TCFExporter {
    class IWriter {
    public:
        IWriter();
        virtual ~IWriter();

        virtual auto Write(TC::IO::MemoryChunk::Pointer memoryChunk, 
                           const std::vector<float>& spacing, 
                           const QString& path)->bool = 0;
    };
}
