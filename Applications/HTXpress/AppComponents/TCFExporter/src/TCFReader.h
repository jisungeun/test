#pragma once
#include <memory>
#include <vector>

#include "Defines.h"
#include <MemoryChunk.h>

namespace HTXpress::AppComponents::TCFExporter {
    class TCFReader {
    public:
        TCFReader();
        ~TCFReader();

        auto Open(const QString& path, Type type)->bool;

        auto GetChannels() const->std::vector<int32_t>;
        auto GetTimeCount(int32_t channel) const->int32_t; 
        auto GetSize() const->std::vector<int32_t>;
        auto GetResolution() const->std::vector<double>;

        auto Read(int32_t timeSlice, int32_t channel = 0) const->TC::IO::MemoryChunk::Pointer;
        auto GetMinValue(int32_t timeSlice, int32_t channel = 0)const->float;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}