#include <QThread>

#include <TCFExporter.h>
#include "ExportWorker.h"

namespace HTXpress::AppComponents::TCFExporter::App {
    struct ExportWorker::Impl {
        QStringList pathList;
        QList<DataType> dataTypes;
        QString outputPath;
        ImageType imageType;
        QString error;

        auto GetDataID(DataType dataType) const->QString;
        auto GetExtension(ImageType imageType) const->Exporter::Extension;
        auto Process(const QString& path, DataType dataType)->bool;
    };

    auto ExportWorker::Impl::GetDataID(DataType dataType) const -> QString {
        QString dataId;

        switch(dataType) {
        case DataType::RITomogram:
            dataId = "3D";
            break;
        case DataType::RIMIP:
            dataId = "2DMIP";
            break;
        case DataType::FLTomogram:
            dataId = "3DFL";
            break;
        case DataType::FLMIP:
            dataId = "2DFLMIP";
            break;
        case DataType::BrightField:
            dataId = "BF";
            break;
        }

        return dataId;
    }

    auto ExportWorker::Impl::GetExtension(ImageType imageType) const -> Exporter::Extension {
        Exporter::Extension ext{ Exporter::Extension::RAW };

        switch(imageType) {
        case ImageType::Raw:
            ext = Exporter::Extension::RAW;
            break;
        }

        return ext;
    }

    auto ExportWorker::Impl::Process(const QString& path, DataType dataType)->bool {
        Exporter exporter;

        error.clear();

        exporter.SetSource(path, GetDataID(dataType));
        exporter.SetTarget(outputPath, GetExtension(imageType));
        exporter.Export();

        error = exporter.GetLastError();

        return true;
    }

    ExportWorker::ExportWorker() : QObject(), d{new Impl} {
    }

    auto ExportWorker::SetData(const QStringList& pathList, const QList<DataType>& dataTypes) -> void {
        d->pathList = pathList;
        d->dataTypes = dataTypes;
    }

    auto ExportWorker::SetOutput(const QString& outputPath, ImageType imageType) -> void {
        d->outputPath = outputPath;
        d->imageType = imageType;
    }

    void ExportWorker::onProcess() {
        for(const auto& path : d->pathList) {
            for(const auto& dataType : d->dataTypes) {
                if(d->Process(path, dataType)) {
                    emit sigFailed(path, d->error);
                } else {
                    emit sigProcessed(path);
                }
            }
        }
    }
}