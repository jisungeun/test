#define LOGGER_TAG "[UI]"
#include <QFileDialog>
#include <QFileInfo>
#include <QThread>

#include <TCLogger.h>
#include <FileUtility.h>

#include "Version.h"
#include "Defines.h"
#include "ExportWorker.h"
#include "ui_mainwindow.h"
#include "MainWindowControl.h"
#include "MainWindow.h"

namespace HTXpress::AppComponents::TCFExporter::App {
    struct MainWindow::Impl {
        Ui::MainWindow ui;
        MainWindowControl control;
        QStringList files;
        QThread exportThread;
        int32_t processedCount{ 0 };
        ExportWorker *exportWorker{ nullptr };

        auto UpdateUI()->void;
        auto UpdateTable()->void;
        auto SelectedRows() const->QList<int32_t>;
        auto SelectedDataTypes() const->QList<DataType>;
        auto SelectedImageType() const->ImageType;
    };

    auto MainWindow::Impl::UpdateUI() -> void {
        ui.startBtn->setEnabled(false);

        if(ui.sourceDirEdit->text().isEmpty()) return;
        if(ui.targetDirEdit->text().isEmpty()) return;
        if(SelectedDataTypes().isEmpty()) return;
        if(SelectedRows().isEmpty()) return;

        ui.startBtn->setEnabled(true);
    }

    auto MainWindow::Impl::UpdateTable() -> void {
        int32_t idx = 0;

        ui.filesTable->clearSelection();
        ui.filesTable->clearContents();

        foreach(auto file, files) {
		    QFileInfo finfo(file);

		    ui.filesTable->insertRow(idx);

		    QTableWidgetItem *item = new QTableWidgetItem(finfo.fileName());
		    item->setFlags(item->flags() ^ Qt::ItemIsEditable);
		    ui.filesTable->setItem(idx, 0, item);

		    item = new QTableWidgetItem(QIcon(":/images/process_uncompleted.png"), "");
		    item->setFlags(item->flags() ^ Qt::ItemIsEditable);
		    ui.filesTable->setItem(idx, 1, item);

		    idx = idx + 1;
	    }
    }

    auto MainWindow::Impl::SelectedRows() const -> QList<int32_t> {
        QList<int32_t> list;

        auto selectedItems = ui.filesTable->selectedItems();
        for(auto* item : selectedItems) {
            if(item->column() != 0) continue;
            list.push_back(item->row());
        }

        return list;
    }

    auto MainWindow::Impl::SelectedDataTypes() const -> QList<DataType> {
        QList<DataType> types;

        if(ui.riTomogramChk->isChecked()) types.push_back(DataType::RITomogram);
        if(ui.riMIPChk->isChecked()) types.push_back(DataType::RIMIP);
        if(ui.flTomogramChk->isChecked()) types.push_back(DataType::FLTomogram);
        if(ui.flMIPChk->isChecked()) types.push_back(DataType::FLMIP);
        if(ui.bfChk->isChecked()) types.push_back(DataType::BrightField);

        return types;
    }

    auto MainWindow::Impl::SelectedImageType() const -> ImageType {
        return ImageType::Raw;
    }

    MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{ new Impl } {
        d->ui.setupUi(this);

        const auto& ver = QString("HTX TCF Exporter %1").arg(PROJECT_REVISION);
        QLOG_INFO() << ver;

        setWindowTitle(ver);

        d->ui.filesTable->setColumnCount(2);
        d->ui.filesTable->setHorizontalHeaderLabels({"File", ""});
        d->ui.filesTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        d->ui.filesTable->setColumnWidth(1, 16);
        auto* header = d->ui.filesTable->horizontalHeader();
        header->setSectionResizeMode(0, QHeaderView::Stretch);
        header->setSectionResizeMode(1, QHeaderView::Fixed);

        d->ui.startBtn->setEnabled(false);
        d->ui.tiffFormatBtn->setHidden(true);

        d->exportWorker = new ExportWorker;
        d->exportWorker->moveToThread(&d->exportThread);
        d->exportThread.start();

        connect(d->ui.openSourceBtn, SIGNAL(clicked()), this, SLOT(onOpenSource()));
        connect(d->ui.openTargetBtn, SIGNAL(clicked()), this, SLOT(onOpenTarget()));
        connect(d->ui.removeBtn, SIGNAL(clicked()), this, SLOT(onRemoveSelectedFiles()));
        connect(d->ui.selectAllBtn, SIGNAL(clicked()), this, SLOT(onSelectAllFiles()));
        connect(d->ui.deselectAllBtn, SIGNAL(clicked()), this, SLOT(onDeselectAllFiles()));
        connect(d->ui.startBtn, SIGNAL(clicked()), this, SLOT(onStart()));

        connect(&d->exportThread, &QThread::finished, d->exportWorker, &QObject::deleteLater);
        connect(this, &MainWindow::sigProcess, d->exportWorker, &ExportWorker::onProcess);
        connect(d->exportWorker, &ExportWorker::sigProcessed, this, &MainWindow::onProcessed);
        connect(d->exportWorker, &ExportWorker::sigFailed, this, &MainWindow::onProcessFailed);
    }

    MainWindow::~MainWindow() {
        d->exportThread.quit();
        d->exportThread.wait();
    }

    void MainWindow::onOpenSource() {
        const auto str = QFileDialog::getExistingDirectory(this, "Select a directory where TCF files are");
        if(str.isEmpty()) {
            d->ui.sourceDirEdit->clear();
            d->ui.filesTable->clear();
            d->UpdateUI();
            return;
        }

        d->ui.sourceDirEdit->setText(str);

        d->files = TC::FindFiles(str, "*.TCF");
        if(d->files.isEmpty()) {
            d->UpdateTable();
            d->UpdateUI();
            return;
        }

        d->UpdateTable();
        d->UpdateUI();
    }

    void MainWindow::onOpenTarget() {
        const auto str = QFileDialog::getExistingDirectory(this, "Select a directory where exported images are saved");
        if(str.isEmpty()) {
            d->ui.targetDirEdit->clear();
            d->UpdateUI();
            return;
        }

        d->ui.targetDirEdit->setText(str);
        d->UpdateUI();
    }

    void MainWindow::onRemoveSelectedFiles() {
        const auto selectedRows = d->SelectedRows();
        for(auto itr = selectedRows.rbegin(); itr != selectedRows.rend(); ++itr) {
            d->files.removeAt(*itr);
        }

        d->UpdateTable();
        d->UpdateUI();
    }

    void MainWindow::onSelectAllFiles() {
        d->ui.filesTable->selectAll();
        d->UpdateUI();
    }

    void MainWindow::onDeselectAllFiles() {
        d->ui.filesTable->clearSelection();
        d->UpdateUI();
    }

    void MainWindow::onStart() {
        const auto selectedRows = d->SelectedRows();
        if(selectedRows.isEmpty()) return;

        const auto selectedTypes = d->SelectedDataTypes();
        if(selectedTypes.isEmpty()) return;

        QStringList pathList;
        for(auto row : selectedRows) {
            pathList.push_back(d->files.at(row));
        }

        d->ui.progressBar->setValue(0);
        d->ui.progressBar->setRange(0, selectedRows.size());
        d->processedCount = 0;

        d->exportWorker->SetData(pathList, selectedTypes);
        d->exportWorker->SetOutput(d->ui.targetDirEdit->text(), d->SelectedImageType());

        emit sigProcess();
    }

    void MainWindow::onProcessed(const QString& path) {
        d->processedCount++;
        d->ui.progressBar->setValue(d->processedCount);

        const auto rowIdx = d->files.indexOf(path);
        auto* item = d->ui.filesTable->item(rowIdx, 1);
        if(item) {
            item->setIcon(QIcon(":/images/process_completed.png"));
        }
    }

    void MainWindow::onProcessFailed(const QString& path, const QString& error) {
        d->processedCount++;
        d->ui.progressBar->setValue(d->processedCount);

        const auto rowIdx = d->files.indexOf(path);
        auto* item = d->ui.filesTable->item(rowIdx, 1);
        if(item) {
            item->setIcon(QIcon(":/images/process_failed.png"));
        }

        d->ui.statusbar->showMessage(error, 5000);
    }
}
