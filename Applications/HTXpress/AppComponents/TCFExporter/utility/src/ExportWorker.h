#pragma once
#include <memory>
#include <QObject>

#include "Defines.h"

namespace HTXpress::AppComponents::TCFExporter::App {
    class ExportWorker : public QObject {
        Q_OBJECT

    public:
        ExportWorker();

        auto SetData(const QStringList& pathList, const QList<DataType>& dataTypes)->void;
        auto SetOutput(const QString& outputPath, ImageType imageType)->void;

    public slots:
        void onProcess();

    signals:
        void sigProcessed(const QString& path);
        void sigFailed(const QString& path, const QString& error);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}