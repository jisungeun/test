#pragma once
#include <memory>
#include <QMainWindow>

#include "Defines.h"

namespace HTXpress::AppComponents::TCFExporter::App {
    class MainWindow : public QMainWindow {
		Q_OBJECT

	public:
		explicit MainWindow(QWidget* parent = nullptr);
		~MainWindow() override;

	protected slots:
		void onOpenSource();
		void onOpenTarget();
		void onRemoveSelectedFiles();
		void onSelectAllFiles();
		void onDeselectAllFiles();
		void onStart();
		void onProcessed(const QString& path);
		void onProcessFailed(const QString& path, const QString& error);

	signals:
		void sigProcess();

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}