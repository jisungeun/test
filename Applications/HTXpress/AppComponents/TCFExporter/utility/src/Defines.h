#pragma once
#include <memory>

namespace HTXpress::AppComponents::TCFExporter::App {
    enum class DataType {
        RITomogram,
        RIMIP,
        FLTomogram,
        FLMIP,
        BrightField
    };

    enum class ImageType {
        Raw,
        Tiff
    };
}
