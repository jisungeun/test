#pragma once
#include <memory>

namespace HTXpress::AppComponents::TCFExporter::App {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}