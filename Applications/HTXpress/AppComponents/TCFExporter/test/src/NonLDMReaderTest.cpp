#include <catch2/catch.hpp>

#include <QString>
#include "NonLDMReader.h"
#include "H5Cpp.h"

namespace NonLDMReaderTest {
    auto ReadFL3D(const QString& filePath, const QString& dataPath, const int32_t& sizeX, const int32_t& sizeY, const int32_t& sizeZ) -> std::shared_ptr<uint16_t[]> {
        const H5::H5File file(filePath.toStdString(), H5F_ACC_RDONLY);

        const auto flGroup = file.openGroup(dataPath.toStdString());
        const auto dataSet = flGroup.openDataSet("000000");

        const auto numberOfElements = sizeX * sizeY * sizeZ;
        std::shared_ptr<uint16_t[]> fl3DMemory{new uint16_t[numberOfElements]()};

        dataSet.read(fl3DMemory.get(), H5::PredType::NATIVE_UINT16);

        return fl3DMemory;
    }

    auto CompareArray(const uint16_t* result, const uint16_t* answer, const size_t& numberOfElements) -> bool {
        for (size_t i = 0; i < numberOfElements; ++i) {
            if (result[i] != answer[i]) {
                return false;
            }
        }
        return true;
    }

    TEST_CASE("NonLDMReader : unit test") {
        //TODO Implement unit test
    }

    TEST_CASE("NonLDMReader : practical test") {
        //TODO Impement practical test
        SECTION("FL Channel 3") {
            HTXpress::AppComponents::TCFExporter::NonLDMReader reader;
            const QString tcfFilePath = "F:/TempFdrive/DataPath/admin/adpro/adExp/221021.155344.adExp.002.Group1.A1.S002/221021.155344.adExp.002.Group1.A1.S002.TCF";

            reader.Open(tcfFilePath);

            SECTION("3DFL/CH0") {
                const QString dataID = "3DFL/CH0";
                constexpr auto timeSlice = 0;
                constexpr auto type = HTXpress::AppComponents::TCFExporter::Type::FL3D;
                const auto resultMemoryChunk = reader.Read(dataID, timeSlice, type, {}, {});

                const auto resultDimension = resultMemoryChunk->GetDimension();
                CHECK(resultDimension.GetDimensionality() == 3);
                CHECK(resultDimension.GetNumberOfElements() == 39*1416*1416);
                CHECK(resultDimension.X() == 1416);
                CHECK(resultDimension.Y() == 1416);
                CHECK(resultDimension.Z() == 39);

                CHECK(resultMemoryChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type);

                const auto resultMemoryPointer = resultMemoryChunk->GetDataPointerAs<uint16_t>();
                const auto answerMemory = ReadFL3D(tcfFilePath, "/Data/3DFL/CH0", 1416, 1416, 39);

                CHECK(CompareArray(resultMemoryPointer,answerMemory.get(), 1416*1416*39));
            }

            SECTION("3DFL/CH1") {
                const QString dataID = "3DFL/CH1";
                constexpr auto timeSlice = 0;
                constexpr auto type = HTXpress::AppComponents::TCFExporter::Type::FL3D;
                const auto resultMemoryChunk = reader.Read(dataID, timeSlice, type, {}, {});

                const auto resultDimension = resultMemoryChunk->GetDimension();
                CHECK(resultDimension.GetDimensionality() == 3);
                CHECK(resultDimension.GetNumberOfElements() == 39*1416*1416);
                CHECK(resultDimension.X() == 1416);
                CHECK(resultDimension.Y() == 1416);
                CHECK(resultDimension.Z() == 39);

                CHECK(resultMemoryChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type);

                const auto resultMemoryPointer = resultMemoryChunk->GetDataPointerAs<uint16_t>();
                const auto answerMemory = ReadFL3D(tcfFilePath, "/Data/3DFL/CH1", 1416, 1416, 39);

                CHECK(CompareArray(resultMemoryPointer, answerMemory.get(), 1416*1416*39));
            }

            SECTION("3DFL/CH2") {
                const QString dataID = "3DFL/CH2";
                constexpr auto timeSlice = 0;
                constexpr auto type = HTXpress::AppComponents::TCFExporter::Type::FL3D;
                const auto resultMemoryChunk = reader.Read(dataID, timeSlice, type, {}, {});

                const auto resultDimension = resultMemoryChunk->GetDimension();
                CHECK(resultDimension.GetDimensionality() == 3);
                CHECK(resultDimension.GetNumberOfElements() == 39*1416*1416);
                CHECK(resultDimension.X() == 1416);
                CHECK(resultDimension.Y() == 1416);
                CHECK(resultDimension.Z() == 39);

                CHECK(resultMemoryChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type);

                const auto resultMemoryPointer = resultMemoryChunk->GetDataPointerAs<uint16_t>();
                const auto answerMemory = ReadFL3D(tcfFilePath, "/Data/3DFL/CH2", 1416, 1416, 39);

                CHECK(CompareArray(resultMemoryPointer, answerMemory.get(), 1416*1416*39));
            }

            SECTION("2DFL/CH0") {
                const QString dataID = "2DFLMIP/CH0";
                constexpr auto timeSlice = 0;
                constexpr auto type = HTXpress::AppComponents::TCFExporter::Type::FL2DMIP;
                const auto resultMemoryChunk = reader.Read(dataID, timeSlice, type, {}, {});

                const auto resultDimension = resultMemoryChunk->GetDimension();
                CHECK(resultDimension.GetDimensionality() == 3);
                CHECK(resultDimension.GetNumberOfElements() == 1*1416*1416);
                CHECK(resultDimension.X() == 1416);
                CHECK(resultDimension.Y() == 1416);
                CHECK(resultDimension.Z() == 1);

                CHECK(resultMemoryChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type);

                const auto resultMemoryPointer = resultMemoryChunk->GetDataPointerAs<uint16_t>();
                const auto answerMemory = ReadFL3D(tcfFilePath, "/Data/2DFLMIP/CH0", 1416, 1416, 1);

                CHECK(CompareArray(resultMemoryPointer,answerMemory.get(), 1416*1416*1));
            }

            SECTION("2DFL/CH1") {
                const QString dataID = "2DFLMIP/CH1";
                constexpr auto timeSlice = 0;
                constexpr auto type = HTXpress::AppComponents::TCFExporter::Type::FL2DMIP;
                const auto resultMemoryChunk = reader.Read(dataID, timeSlice, type, {}, {});

                const auto resultDimension = resultMemoryChunk->GetDimension();
                CHECK(resultDimension.GetDimensionality() == 3);
                CHECK(resultDimension.GetNumberOfElements() == 1*1416*1416);
                CHECK(resultDimension.X() == 1416);
                CHECK(resultDimension.Y() == 1416);
                CHECK(resultDimension.Z() == 1);

                CHECK(resultMemoryChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type);

                const auto resultMemoryPointer = resultMemoryChunk->GetDataPointerAs<uint16_t>();
                const auto answerMemory = ReadFL3D(tcfFilePath, "/Data/2DFLMIP/CH1", 1416, 1416, 1);

                CHECK(CompareArray(resultMemoryPointer, answerMemory.get(), 1416*1416*1));
            }

            SECTION("2DFL/CH2") {
                const QString dataID = "2DFLMIP/CH2";
                constexpr auto timeSlice = 0;
                constexpr auto type = HTXpress::AppComponents::TCFExporter::Type::FL2DMIP;
                const auto resultMemoryChunk = reader.Read(dataID, timeSlice, type, {}, {});

                const auto resultDimension = resultMemoryChunk->GetDimension();
                CHECK(resultDimension.GetDimensionality() == 3);
                CHECK(resultDimension.GetNumberOfElements() == 1*1416*1416);
                CHECK(resultDimension.X() == 1416);
                CHECK(resultDimension.Y() == 1416);
                CHECK(resultDimension.Z() == 1);

                CHECK(resultMemoryChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type);

                const auto resultMemoryPointer = resultMemoryChunk->GetDataPointerAs<uint16_t>();
                const auto answerMemory = ReadFL3D(tcfFilePath, "/Data/2DFLMIP/CH2", 1416, 1416, 1);

                CHECK(CompareArray(resultMemoryPointer, answerMemory.get(), 1416*1416*1));
            }
        }

        SECTION("FL Channel 1") {
            HTXpress::AppComponents::TCFExporter::NonLDMReader reader;

            const QString tcfFilePath = "F:/TempFdrive/DataPath/admin/adpro/adExp/221021.154503.adExp.001.Group1.A1.S001/221021.154503.adExp.001.Group1.A1.S001.TCF";
            reader.Open(tcfFilePath);

            SECTION("3DFL/CH2") {
                const QString dataID = "3DFL/CH2";
                constexpr auto timeSlice = 0;
                constexpr auto type = HTXpress::AppComponents::TCFExporter::Type::FL3D;
                const auto resultMemoryChunk = reader.Read(dataID, timeSlice, type, {}, {});

                const auto resultDimension = resultMemoryChunk->GetDimension();
                CHECK(resultDimension.GetDimensionality() == 3);
                CHECK(resultDimension.GetNumberOfElements() == 17*2767*2778);
                CHECK(resultDimension.X() == 2778);
                CHECK(resultDimension.Y() == 2767);
                CHECK(resultDimension.Z() == 17);

                CHECK(resultMemoryChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type);

                const auto resultMemoryPointer = resultMemoryChunk->GetDataPointerAs<uint16_t>();
                const auto answerMemory = ReadFL3D(tcfFilePath, "/Data/3DFL/CH2", 2778, 2767, 17);

                CHECK(CompareArray(resultMemoryPointer, answerMemory.get(), 17*2767*2778));
            }
            
            SECTION("2DFL/CH2") {
                const QString dataID = "2DFLMIP/CH2";
                constexpr auto timeSlice = 0;
                constexpr auto type = HTXpress::AppComponents::TCFExporter::Type::FL2DMIP;
                const auto resultMemoryChunk = reader.Read(dataID, timeSlice, type, {}, {});

                const auto resultDimension = resultMemoryChunk->GetDimension();
                CHECK(resultDimension.GetDimensionality() == 3);
                CHECK(resultDimension.GetNumberOfElements() == 1*2767*2778);
                CHECK(resultDimension.X() == 2778);
                CHECK(resultDimension.Y() == 2767);
                CHECK(resultDimension.Z() == 1);

                CHECK(resultMemoryChunk->GetDataType() == +TC::IO::ChunkDataType::UInt16Type);

                const auto resultMemoryPointer = resultMemoryChunk->GetDataPointerAs<uint16_t>();
                const auto answerMemory = ReadFL3D(tcfFilePath, "/Data/2DFLMIP/CH2", 2778, 2767, 1);

                CHECK(CompareArray(resultMemoryPointer, answerMemory.get(), 1*2767*2778));
            }
        }
    }
}
