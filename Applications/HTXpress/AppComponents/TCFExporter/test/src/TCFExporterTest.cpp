#include <catch2/catch.hpp>

#include "TCFExporter.h"

namespace TCFExporterTest {
    using namespace HTXpress::AppComponents::TCFExporter;

    TEST_CASE("TCFExporter") {
        SECTION("LDM") {
            const QString tcfFilePath = "E:/00_Data/20230302 LDM Converting sample/data/noStaged/data.TCF";
            const QString resultFolderPath = "D:/temp2/exportResult";
            constexpr auto exportType = Exporter::Extension::RAW;

            Exporter exporter;
            exporter.SetSource(tcfFilePath, "3D");
            exporter.SetTarget(resultFolderPath, exportType);

            CHECK(exporter.Export() == true);
        }

        SECTION("Non LDM") {
            const QString tcfFilePath = "E:/00_Data/20230208 TBNK z slice/221215.194415.20221215_ICI.020.Group1.A1.T011P01/221215.194415.20221215_ICI.020.Group1.A1.T011P01.TCF";
            const QString resultFolderPath = "D:/temp2/exportResult";
            constexpr auto exportType = Exporter::Extension::RAW;

            Exporter exporter;
            exporter.SetSource(tcfFilePath, "3D");
            exporter.SetTarget(resultFolderPath, exportType);

            CHECK(exporter.Export() == true);

            CHECK(exporter.GetLastError().toStdString() == "");
        }
    }
}