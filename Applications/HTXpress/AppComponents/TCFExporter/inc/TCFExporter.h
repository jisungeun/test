#pragma once
#include <memory>
#include <QString>
#include <QObject>

#include "HTXTCFExporterExport.h"

namespace HTXpress::AppComponents::TCFExporter {
    class HTXTCFExporter_API Exporter final : public QObject {
        Q_OBJECT
    public:
        enum class Extension {
            RAW
        };

    public:
        explicit Exporter(QObject* parent = nullptr);
        ~Exporter() override;

        auto SetSource(const QString& path, const QString& dataID)->void;
        auto SetTarget(const QString& path, Extension ext)->void;
        
        auto Export() ->bool;
        auto GetLastError() const->QString;

    signals:
        void sigTotalCount(const int&);
        void sigCurrentCount(const int&);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}