#include <QDir>
#include <QFileInfo>

#include <SampleTypeRepo.h>

#include "ImagingProfileLoader.h"

namespace HTXpress::AppComponents::ImagingProfileLoader {
    struct ImagingProfileLoader::Impl {
    };

    ImagingProfileLoader::ImagingProfileLoader() : d{std::make_unique<Impl>()} {
    }

    ImagingProfileLoader::~ImagingProfileLoader() = default;

    auto ImagingProfileLoader::AddToProfileRepo(const QString& profileFolderPath) -> void {
        const auto ext = QString("*.%1.%2").arg(AppEntity::ImagingProfileSuffix).arg(AppEntity::ProfileExtension);
        const auto repo = AppEntity::SampleTypeRepo::GetInstance();

        QStringList filters;
        filters << ext;

        const QDir dir(profileFolderPath);
        for (const auto& subDir : dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
            const auto modelName = subDir;
            QDir modelFolder(profileFolderPath + QDir::separator() + subDir);
            for (const auto& fileInfo : modelFolder.entryInfoList(filters, QDir::Files)) {
                AppEntity::SampleTypeName profileName{};
                profileName = fileInfo.baseName();
                repo->AddSampleType(modelName, profileName);
            }
        }
    }
}
