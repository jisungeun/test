﻿#include <catch2/catch.hpp>

#include <QDir>
#include <QFileInfo>
#include <QStandardPaths>
#include <QSettings>
#include <QDebug>
#include <QVector>

#include <ImagingProfileLoader.h>
#include <ImagingProfileRepo.h>
#include <ImagingProfile.h>

namespace HTXpress::AppComponents::ImagingProfileLoader::Test {
    constexpr auto FolderName = "profile";
    constexpr auto TestModelName = "TestModel";
    constexpr auto TestProfileName = "TestImagingProfile";
    namespace Key {
        constexpr auto HTScanParameter = "HTScanParameter";
        constexpr auto NA = "NA";
        constexpr auto Step = "Step";
        constexpr auto Slices = "Slices";
    }

    QList NAs {1.1, 2.2, 3.3, 4.4, 5.5};
    QList Steps {10, 20, 30, 40, 50};
    QList Slices {100, 200, 300, 400, 500};

    auto WriteSample(const QString& tempConfigFolder) -> void {
        QSettings qs(tempConfigFolder + "/" + FolderName + "/" +TestModelName + "/" + QString("%1.%2.%3").arg(TestProfileName).arg(AppEntity::ImagingProfileSuffix).arg(AppEntity::ProfileExtension), QSettings::IniFormat);

        qs.beginWriteArray(Key::HTScanParameter);
        for (int i = 0; i < NAs.size(); i++) {
            qs.setArrayIndex(i);
            qs.setValue(Key::NA, NAs[i]);
            qs.setValue(Key::Step, Steps[i]);
            qs.setValue(Key::Slices, Slices[i]);
        }
        qs.endArray();
    }

    auto DeleteSample(const QString& tempConfigFolder) -> void {
        QDir().rmdir(tempConfigFolder);
    }

    TEST_CASE("Profile load test") {
        const auto testConfigFolder = QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/TestConfig";

        SECTION("Repo without files") {
            DeleteSample(testConfigFolder);
            const auto repo = AppEntity::ImagingProfileRepo::GetInstance();
            auto profiles = repo->GetImagingProfilesByModelName(TestModelName);
            CHECK(profiles.has_value() == false);
        }

        SECTION("Repo with files") {
            ImagingProfileLoader loader;
            WriteSample(testConfigFolder);
            loader.Load(testConfigFolder);
            const auto repo = AppEntity::ImagingProfileRepo::GetInstance();
            auto profiles = repo->GetImagingProfilesByModelName(TestModelName);
            CHECK(profiles.has_value() == true);
            for(const auto& profile : *profiles) {
                CHECK(profile->GetProfileName() == TestProfileName);

                for(int i = 0; i < NAs.size(); i++) {
                    CHECK(profile->GetHTParameters(NAs[i]).has_value() == true);
                    CHECK(profile->GetHTParameters(NAs[i]).value().step == Steps[i]);
                    CHECK(profile->GetHTParameters(NAs[i]).value().slices == Slices[i]);
                }

                CHECK(profile->GetHTParameters(99.9).has_value() == false);
            }
        }
    }
}
