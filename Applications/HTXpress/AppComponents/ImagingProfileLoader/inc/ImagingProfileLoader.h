#pragma once
#include <memory>
#include <QString>

#include "HTXImagingProfileLoaderExport.h"

namespace HTXpress::AppComponents::ImagingProfileLoader {
	class HTXImagingProfileLoader_API ImagingProfileLoader final {
	public:
		ImagingProfileLoader();
		~ImagingProfileLoader();

		auto AddToProfileRepo(const QString& profileFolderPath) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}