#pragma once
#include <memory>

#include "InstrumentDefines.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandEnableAutoFocus : public Command {
    public:
        CommandEnableAutoFocus(bool enable, int32_t maxTrials = 10);
        ~CommandEnableAutoFocus() override;

        auto Perform() -> bool override;
        auto GetResponse() -> Response::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}