#pragma once
#include <memory>
#include <QString>

namespace HTXpress::AppComponents::Instrument {
    class CondenserAutofocusProgress {
    public:
        using Pointer = std::shared_ptr<CondenserAutofocusProgress>;

    public:
        CondenserAutofocusProgress();
        ~CondenserAutofocusProgress();

        auto SetProgress(double progress)->void;
        auto GetProgress() const->std::tuple<bool, double>;

        auto SetError(const QString& message)->void;
        auto GetError() const->bool;
        auto GetErrorMessage() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}