#define LOGGER_TAG "[InstrumentHTX]"

#include <QMutexLocker>
#include <QWaitCondition>
#include <QElapsedTimer>
#include <QCoreApplication>

#include <TCLogger.h>

#include <System.h>
#include <CameraControlFactory.h>
#include <MCUFactory.h>

#include "CommandMotion.h"
#include "CommandMotionXY.h"
#include "CommandCheckMotion.h"
#include "CommandInitialize.h"
#include "CommandGetPosition.h"
#include "CommandStopAxis.h"
#include "CommandAcquisition.h"
#include "CommandLive.h"
#include "CommandLiveWithTrigger.h"
#include "CommandStopLive.h"
#include "CommandEnableAutoFocus.h"
#include "CommandAutoFocus.h"
#include "CommandSetBestFocus.h"
#include "CommandCondenserAutoFocus.h"
#include "CommandScanFocus.h"
#include "CommandReadAFSensorValue.h"
#include "CommandHTIlluminationCalibration.h"
#include "CommandPreviewAcquisition.h"
#include "ResponsePosition.h"
#include "ResponseMotion.h"
#include "JoystickMonitor.h"
#include "ImageSink.h"
#include "StatusUpdater.h"
#include "InitializeProgress.h"
#include "AcquisitionProgress.h"
#include "CondenserAutofocusProgress.h"
#include "HTIlluminationCalibrationProgress.h"
#include "PositionAdjuster.h"
#include "EmissionFilters.h"
#include "IlluminationPatterns.h"
#include "AcquisitionChannels.h"
#include "CameraManager.h"
#include "InstrumentHTX.h"
#include "FeatureMap.h"
#include "ImagePort.h"

namespace HTXpress::AppComponents::Instrument {
    using CameraControl = TC::CameraControl::CameraControl;
    using CameraSystem = TC::CameraControl::CameraSystem;

    constexpr int32_t noWait = 0;

    struct InstrumentHTX::Impl {
        CameraManager::Pointer cameraManager{ nullptr };

        JoystickMonitor::Pointer joystick{ JoystickMonitor::GetInstance() };
        StatusUpdater::Pointer updater{ StatusUpdater::GetInstance() };
        PositionAdjuster::Pointer adjuster{ PositionAdjuster::GetInstance() };

        bool running{ false };
        Config config;

        QString error;

        struct {
            std::shared_ptr<CommandInitialize> initCmd;
            InitializeProgress::Pointer initProgress;
            AcquisitionProgress::Pointer acqProgress;
            CondenserAutofocusProgress::Pointer cafProgress;
            HTIlluminationCalibrationProgress::Pointer htIllumCalProgress;
        } status;

        struct {
            bool enabled{ false };
        } autofocus;

        struct {
            LiveMode mode{ LiveMode::BrightField };
            ImagingParameter param;
        } live;

        QMap<Axis, PositionRange> jogRange;

        QList<Command::Pointer> commands;
        QMutex cmdMutex;
        QWaitCondition cmdWait;

        QMutex respMutex;
        QWaitCondition respWait;

        auto ToPulse(Axis axis, RawPosition positionMM)->int32_t;
        auto ToMM(Axis axis, int32_t pulses)->RawPosition;

        auto InSafeArea(Axis axis, int32_t targetPulses)->bool;
        auto IsZAtSafePosition()->bool;

        auto CurrentPositionsInPulse(QVector<int32_t>& pos)->bool;

        auto GetCommand()->Command::Pointer;
        auto RunAndWait(Command::Pointer cmd, uint32_t waitSeconds = 60)->bool;

        auto SetError(const QString& message)->void;
    };

    auto InstrumentHTX::Impl::ToPulse(Axis axis, RawPosition positionMM) -> int32_t {
        const auto resolution = config.AxisResolutionPPM(axis);
        const auto compensation = config.AxisCompensation(axis);
        return static_cast<int32_t>(resolution * positionMM * compensation);
    }

    auto InstrumentHTX::Impl::ToMM(Axis axis, int32_t pulses) -> RawPosition {
        const auto resolution = config.AxisResolutionPPM(axis);
        const auto compensation = config.AxisCompensation(axis);
        return static_cast<RawPosition>(pulses) / resolution / compensation;
    }

    auto InstrumentHTX::Impl::InSafeArea(Axis axis, int32_t targetPulses) -> bool {
        const auto lower = config.AxisSafeRangeLowerPulse(axis);
        const auto upper = config.AxisSafeRangeUpperPulse(axis);
        return (targetPulses >= lower) && (targetPulses <= upper);
    }

    auto InstrumentHTX::Impl::IsZAtSafePosition() -> bool {
        auto getPosCmd = std::make_shared<CommandGetPosition>();
        if(!RunAndWait(getPosCmd)) return false;

        auto resp = std::dynamic_pointer_cast<ResponsePosition>(getPosCmd->GetResponse());
        const auto posPulse = resp->GetPosition(Axis::AxisZ);
        const auto posInMM = ToMM(Axis::AxisZ, posPulse);

        return posInMM < (config.SafeZPositionMM() + 0.010);
    }

    auto InstrumentHTX::Impl::CurrentPositionsInPulse(QVector<int32_t>& pos) -> bool {
        auto getPosCmd = std::make_shared<CommandGetPosition>();
        if(!RunAndWait(getPosCmd)) return false;

        auto resp = std::dynamic_pointer_cast<ResponsePosition>(getPosCmd->GetResponse());
        pos = {resp->GetPosition(Axis::AxisX), resp->GetPosition(Axis::AxisY), resp->GetPosition(Axis::AxisZ)};

        return true;
    }

    auto InstrumentHTX::Impl::GetCommand() -> Command::Pointer {
        Command::Pointer lastCommand{ nullptr };
        if(!commands.isEmpty()) {
            lastCommand = commands.first();
            commands.clear();
        }
        return lastCommand;
    }

    auto InstrumentHTX::Impl::RunAndWait(Command::Pointer cmd, uint32_t waitSeconds) -> bool {
        cmdMutex.lock();
        commands.push_back(cmd);
        cmdWait.wakeOne();

        QMutexLocker respLock(&respMutex);
        cmdMutex.unlock();

        if(waitSeconds==noWait) return true;

        return respWait.wait(&respMutex, waitSeconds*1000);
    }

    auto InstrumentHTX::Impl::SetError(const QString& message) -> void {
        error = message;
        QLOG_ERROR() << error;
    }

    InstrumentHTX::InstrumentHTX() : QThread(), Instrument(), d{new Impl} {
        using CameraType = TC::CameraControl::CameraControlFactory::Controller;

        d->cameraManager = CameraManager::GetInstance(CameraType::PointGrey);

        d->running = true;
        start();
    }

    InstrumentHTX::~InstrumentHTX() {
        d->cmdMutex.lock();
        if(d->running) {
            d->running = false;
            d->cmdWait.wakeOne();
            d->cmdMutex.unlock();
            wait();
        }
    }

    auto InstrumentHTX::SetConfig(const Config& config) -> void {
        d->config = config;
        d->adjuster->SetConfig(config);

        d->cameraManager->SetImagingCamera(config.ImagingCamera());
        d->cameraManager->SetCondenserCamera(config.CondenserCamera());
    }

    auto InstrumentHTX::Initialize() -> bool {
        d->status.initProgress.reset(new InitializeProgress());
        d->status.initCmd = std::make_shared<CommandInitialize>(d->config, d->status.initProgress);
        if(!d->RunAndWait(d->status.initCmd, noWait)) {
            d->SetError("It fails to start the instrument initialization");
            return false;
        }

        return true;
    }

    auto InstrumentHTX::GetInitializationProgress() const -> std::tuple<bool,double> {
        const auto error = d->status.initProgress->GetError();
        if(error) d->SetError(d->status.initProgress->GetErrorMessage());
        return std::make_tuple(!error, d->status.initProgress->GetProgress());
    }

    auto InstrumentHTX::CheckInitialized() const -> bool {
        if(d->status.initProgress == nullptr) return false;
        return (d->status.initProgress->GetProgress() == 1.0);
    }

    auto InstrumentHTX::CleanUp() -> void {
        d->cmdMutex.lock();
        if(!d->running) {
            d->cmdMutex.unlock();
            return;
        }
        d->cmdMutex.unlock();

        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();
        if(mcuControl) {
            //mcuControl->StopMacroStreaming();
            StopLive();
        }

        d->cmdMutex.lock();
        if(d->running) {
            d->running = false;
            d->cmdWait.wakeOne();
            d->cmdMutex.unlock();
            wait();
        } else {
            d->cmdMutex.unlock();
        }

        d->cameraManager->CleanUp();
    }

    auto InstrumentHTX::InstallImagePort(ImageSource source, ImagePort* port) -> void {
        CameraManager::GetInstance()->InstallImagePort(source, port);
    }

    auto InstrumentHTX::UninstallImagePort(ImageSource source, ImagePort* port) -> void {
        CameraManager::GetInstance()->UninstallImagePort(source, port);
    }

    auto InstrumentHTX::SetFLEmission(const QMap<AppEntity::FLFilter, int32_t>& filters) -> void {
        EmissionFilters::Set(filters);
    }

    auto InstrumentHTX::Move(Axis axis, RawPosition targetMM) -> bool {
        auto model = AppEntity::System::GetModel();

        auto getPosCmd = std::make_shared<CommandGetPosition>();
        if(!d->RunAndWait(getPosCmd)) return false;

        auto resp = std::dynamic_pointer_cast<ResponsePosition>(getPosCmd->GetResponse());
        const auto adjustedTargetPulse = d->adjuster->Adjust(axis, resp->GetPosition(axis), d->ToPulse(axis, targetMM));
        auto targetPulse = adjustedTargetPulse;

        if((axis == +Axis::AxisX) || (axis == +Axis::AxisY)) {
            if(!d->InSafeArea(axis, adjustedTargetPulse)) {
                QLOG_INFO() << axis._to_string() << " is about to move out of the safe area, so it moves Z axis to the safe position";
                Move(Axis::AxisZ, d->config.SafeZPositionMM());

                if(!d->IsZAtSafePosition()) {
                    d->SetError("Z axis is not at safe position");
                    return false;
                }
            }
        } else if(axis == +Axis::AxisZ) {
            const bool xInSafe = d->InSafeArea(Axis::AxisX, resp->GetPosition(Axis::AxisX));
            const bool yInSafe = d->InSafeArea(Axis::AxisY, resp->GetPosition(Axis::AxisY));
            if(!xInSafe || !yInSafe) {
                QLOG_INFO() << "Sample stage is out of the safe area, so the target position of Z axis is changed to the safe position";
                targetPulse = d->config.SafeZPositionPulse();
            }
        }

        auto curPos = d->ToMM(axis, resp->GetPosition(axis));
        auto noMove = [=]()->bool {
            return std::abs(targetMM - curPos) <= 0.0000001;
        }();

        if(noMove) {
            QLOG_INFO() << "Target position of " << axis._to_string() << " axis is the same as the current position " << curPos << "mm";
            return true;
        } else {
            QLOG_INFO() << QString("Move(%1)  %2mm to %3mm").arg(axis._to_string()).arg(curPos).arg(targetMM);
        }

        auto moveCmd = std::make_shared<CommandMotion>(axis, targetPulse);
        if(!d->RunAndWait(moveCmd, 10)) {
            d->SetError(QString("Failed to start motion (%1)").arg(moveCmd->GetResponse()->GetMessage()));
            return false;
        }

        return true;
    }

    auto InstrumentHTX::MoveXY(RawPosition targetXMM, RawPosition targetYMM) -> bool {
        auto model = AppEntity::System::GetModel();

        auto getPosCmd = std::make_shared<CommandGetPosition>();
        if(!d->RunAndWait(getPosCmd)) return false;

        auto resp = std::dynamic_pointer_cast<ResponsePosition>(getPosCmd->GetResponse());
        const auto adjustedTargetPulseX = d->adjuster->Adjust(Axis::AxisX, 
                                                             resp->GetPosition(Axis::AxisX), 
                                                             d->ToPulse(Axis::AxisX, targetXMM));
        const auto adjustedTargetPulseY = d->adjuster->Adjust(Axis::AxisY, 
                                                             resp->GetPosition(Axis::AxisY), 
                                                             d->ToPulse(Axis::AxisY, targetYMM));

        if(!d->InSafeArea(Axis::AxisX, adjustedTargetPulseX) || !d->InSafeArea(Axis::AxisY, adjustedTargetPulseY)) {
            QLOG_INFO() << "Sample stage is about to move out of the safe area, so it moves Z axis to safe position";
            MoveZtoSafePos();

            if(!d->IsZAtSafePosition()) {
                d->SetError("Z axis is not at safe position");
                return false;
            }
        }

        QLOG_INFO() << QString("MoveXY  (%1,%2)mm to (%3,%4)mm")
            .arg(d->ToMM(Axis::AxisX, resp->GetPosition(Axis::AxisX)))
            .arg(d->ToMM(Axis::AxisY, resp->GetPosition(Axis::AxisY)))
            .arg(targetXMM).arg(targetYMM);

        auto moveCmd = std::make_shared<CommandMotionXY>(adjustedTargetPulseX, adjustedTargetPulseY);
        if(!d->RunAndWait(moveCmd, 10)) {
            d->SetError(QString("Failed to start motion (%1)").arg(moveCmd->GetResponse()->GetMessage()));
            return false;
        }

        return true;
    }

    auto InstrumentHTX::MoveZtoSafePos() -> bool {
        QLOG_INFO() << QString("Move Z axis to safe position (%1mm)").arg(d->config.SafeZPositionMM());

        auto targetPulse = d->config.SafeZPositionPulse();
        auto moveCmd = std::make_shared<CommandMotion>(Axis::AxisZ, targetPulse);
        if(!d->RunAndWait(moveCmd, 10)) {
            d->SetError(QString("Failed to start motion (%1)").arg(moveCmd->GetResponse()->GetMessage()));
            return false;
        }

        return true;
    }

    auto InstrumentHTX::IsMoving() const -> MotionStatus {
        MotionStatus status;

        auto cmd = std::make_shared<CommandCheckMotion>();
        if(!d->autofocus.enabled) cmd->SetAFDisabled();
        if(!d->RunAndWait(cmd)) return status;

        auto resp = std::dynamic_pointer_cast<ResponseMotion>(cmd->GetResponse());
        status.error = !resp->GetResult();
        status.moving = resp->GetMoving();
        status.afFailed = resp->GetAFFailed();
        if(status.error) status.message = resp->GetMessage();

        return status;
    }

    auto InstrumentHTX::GetPosition(Axis axis, RawPosition& pos) -> bool {
        auto getPosCmd = std::make_shared<CommandGetPosition>();
        if(!d->RunAndWait(getPosCmd)) return false;

        auto resp = std::dynamic_pointer_cast<ResponsePosition>(getPosCmd->GetResponse());
        const auto posPulse = resp->GetPosition(axis);
        pos = d->ToMM(axis, posPulse);

        return true;
    }

    auto InstrumentHTX::GetPositionXY(RawPosition& xPos, RawPosition& yPos) -> bool {
        auto getPosCmd = std::make_shared<CommandGetPosition>();
        if(!d->RunAndWait(getPosCmd)) return false;

        auto resp = std::dynamic_pointer_cast<ResponsePosition>(getPosCmd->GetResponse());
        xPos = d->ToMM(Axis::AxisX, resp->GetPosition(Axis::AxisX));
        yPos = d->ToMM(Axis::AxisY, resp->GetPosition(Axis::AxisY));

        return true;
    }

    auto InstrumentHTX::GetPositionXYZ(RawPosition& xPos, RawPosition& yPos, RawPosition& zPos) -> bool {
        auto getPosCmd = std::make_shared<CommandGetPosition>();
        if(!d->RunAndWait(getPosCmd)) return false;

        auto resp = std::dynamic_pointer_cast<ResponsePosition>(getPosCmd->GetResponse());
        if(resp->GetError()) {
            d->SetError(tr("It fails to get the current axis positions"));
            return false;
        }

        xPos = d->ToMM(Axis::AxisX, resp->GetPosition(Axis::AxisX));
        yPos = d->ToMM(Axis::AxisY, resp->GetPosition(Axis::AxisY));
        zPos = d->ToMM(Axis::AxisZ, resp->GetPosition(Axis::AxisZ));

        return true;
    }

    auto InstrumentHTX::StartJog(Axis axis, bool plusDirection) -> bool {
        auto rangeIter = d->jogRange.find(axis);
        if(rangeIter == d->jogRange.end()) return nullptr;

        auto limitPos = [=]()->int32_t {
            if(plusDirection) return d->ToPulse(axis, rangeIter->maxPos);
            return d->ToPulse(axis, rangeIter->minPos);
        }();

        QLOG_INFO() << "Start joystick motion is triggered. Axis=" << axis._to_string() << " Target=" << limitPos;
        auto moveCmd = std::make_shared<CommandMotion>(axis, limitPos);
        if(!d->RunAndWait(moveCmd, 10)) {
            d->SetError(QString("Failed to start jog motion (%1)").arg(moveCmd->GetResponse()->GetMessage()));
            return false;
        }

        return true;
    }

    auto InstrumentHTX::StopJog() -> bool {
        auto stopCmd = std::make_shared<CommandStopAxis>();
        if(!d->RunAndWait(stopCmd, 10)) {
            d->SetError("Failed to stop jog motion with joystick");
            return false;
        }
        return true;
    }

    auto InstrumentHTX::SetJogRange(Axis axis, const PositionRange& range) -> void {
        d->jogRange[axis] = range;
    }

    auto InstrumentHTX::EnableJoystick() -> bool {
        d->joystick->SetEnable();
        return true;
    }

    auto InstrumentHTX::DisableJoystick() -> bool {
        d->joystick->SetDisable();
        return true;
    }

    auto InstrumentHTX::CheckJoystick() -> JoystickStatus {
        auto joystickStatus = d->joystick->CheckJoystick();

        if(joystickStatus.status == JoystickTriggerStatus::Stop) {
            auto stopCmd = std::make_shared<CommandStopAxis>();
            if(!d->RunAndWait(stopCmd, 10)) {
                d->SetError("Failed to stop jog motion with joystick");
            }
            return JoystickStatus();
        }

        return joystickStatus;
    }

    auto InstrumentHTX::ChangeFOV(int32_t xPixels, int32_t yPixels, int32_t& offsetX, int32_t& offsetY) -> bool {
        auto imagingCamera = d->cameraManager->GetImagingCamera();
        return imagingCamera->ChangeFOV(xPixels, yPixels, offsetX, offsetY);
    }

    auto InstrumentHTX::StartAcquisition(const AppEntity::ImagingSequence::Pointer sequence,
                                         const QList<AppEntity::PositionGroup>& positions,
                                         bool useAutofocus,
                                         AppEntity::WellIndex startingWellIndex,
							             double focusReadyMM,
							             bool useMultiDishHolder) -> bool {
        d->status.acqProgress.reset(new AcquisitionProgress());

        auto pos = [&]()->AppEntity::Position {
            RawPosition xInMM, yInMM, zInMM;
            GetPositionXYZ(xInMM, yInMM, zInMM);
            return AppEntity::Position::fromMM(xInMM, yInMM, zInMM);
        }();
        d->status.acqProgress->SetPosition(pos);

        auto acqCmd = std::make_shared<CommandAcquisition>(sequence, positions, useAutofocus, d->status.acqProgress);
        acqCmd->SetConfig(d->config);
        acqCmd->SetStartingWellIndex(startingWellIndex);
        acqCmd->SetFocusReadyMM(focusReadyMM);
        if(useMultiDishHolder) acqCmd->UseMultiDishHolder();
        if(!d->RunAndWait(acqCmd, noWait)) {
            d->SetError(QString("Failed to start acquisition (%1)").arg(acqCmd->GetResponse()->GetMessage()));
            return false;
        }
        return true;
    }

    auto InstrumentHTX::StopAcquisition() -> bool {
        if(d->status.acqProgress == nullptr) return true;

        d->status.acqProgress->StopAcquisition();

        bool stopped = false;
        while(!stopped) {
            QThread::msleep(200);
            stopped = d->status.acqProgress->GetStopped();
        }

        return true;
    }

    auto InstrumentHTX::CheckAcquisitionProgress() const -> std::tuple<bool,double,AppEntity::Position> {
        if(d->status.acqProgress->GetError()) {
            d->SetError(d->status.acqProgress->GetErrorMessage());
        }
        return d->status.acqProgress->GetProgress();
    }

    auto InstrumentHTX::GetRemainCount(ImageSource source) const -> int32_t {
        auto camera = [&]()->CameraControl::Pointer {
            if(source == +ImageSource::ImagingCamera) {
                return d->cameraManager->GetImagingCamera();
            }
            return d->cameraManager->GetCondenserCamera();
        }();

        return camera->GetNoAcquiredCount() + camera->GetNotDeliveredCount();
    }

    auto InstrumentHTX::StartLive(LiveMode mode, const ImagingParameter& param, bool onlyUpdateParameter) -> bool {
        QVector<int32_t> pos;
        if(!d->CurrentPositionsInPulse(pos)) {
            d->SetError("It fails to get current position");
            return false;
        }

        d->live.mode = mode;
        d->live.param = param;

        auto liveCmd = std::make_shared<CommandLive>(mode, param, pos);
        liveCmd->SetUpdateParmaeterOnly(onlyUpdateParameter);
        if(mode == +LiveMode::BrightField) {
            liveCmd->SetIntensityRange(0, 255);
        } else if(mode == +LiveMode::Fluorescence) {
            liveCmd->SetIntensityRange(d->config.GetFlOutputRangeMin(), 
                                       d->config.GetFlOutputRangeMax());
        } else {
            //not adjust intensity range
        }

        if(!d->RunAndWait(liveCmd, 30)) {
            d->SetError(QString("Failed to start live (%1)").arg(liveCmd->GetResponse()->GetMessage()));
            return false;
        }
        return true;
    }

    auto InstrumentHTX::StopLive() -> bool {
        auto stopCmd = std::make_shared<CommandStopLive>();
        if(!d->RunAndWait(stopCmd, 10)) {
            d->SetError(QString("Failed to stop live (%1)").arg(stopCmd->GetResponse()->GetMessage()));
            return false;
        }

        auto imageSink = CameraManager::GetInstance()->GetImageSink(ImageSource::ImagingCamera);
        imageSink->Clear();

        return true;
    }

    auto InstrumentHTX::ResumeLive() -> bool {
        return StartLive(d->live.mode, d->live.param);
    }

    auto InstrumentHTX::StartLiveWithTrigger(const ImagingParameter& param,
                                             int32_t triggerCount, int32_t triggerInterval, int32_t pulseWidth,
                                             bool useHCG, double gain) -> bool {
        QVector<int32_t> pos;
        if (!d->CurrentPositionsInPulse(pos)) {
            d->SetError("It fails to get current position");
            return false;
        }

        auto liveCmd = std::make_shared<CommandLiveWithTrigger>(param, pos, triggerCount, triggerInterval, pulseWidth);
        liveCmd->UseHighConversionGain(useHCG, gain);

        if (!d->RunAndWait(liveCmd, 30 + (triggerCount * triggerInterval / 1000000))) {
            d->SetError(QString("Failed to start live (%1)").arg(liveCmd->GetResponse()->GetMessage()));
            return false;
        }

        return true;
    }

    auto InstrumentHTX::StartPreviewAcquisition(const QList<UnitMotion>& motions, double bfExposureMSec) -> bool {
        d->status.acqProgress.reset(new AcquisitionProgress());

        auto previewCmd = std::make_shared<CommandPreviewAcquisition>(d->config, d->status.acqProgress);
        previewCmd->SetMotions(motions);
        previewCmd->SetBFExposure(bfExposureMSec);
        if(!d->RunAndWait(previewCmd, noWait)) {
            d->SetError(QString("Failed to start preview acquisition (%1)").arg(previewCmd->GetResponse()->GetMessage()));
            return false;
        }
        return true;
    }

    auto InstrumentHTX::EnableAutoFocus(bool enable) -> std::tuple<bool,bool> {
        auto afCmd = std::make_shared<CommandEnableAutoFocus>(enable, d->config.GetAFMaxTrials());
        if(!d->RunAndWait(afCmd, 10)) {
            d->SetError(QString("Failed to %1 autofocus").arg(enable?"enable":"disable"));
            return std::make_tuple(false, false);
        }
        d->autofocus.enabled = enable;

        auto resp = afCmd->GetResponse();
        return std::make_tuple(true, resp->GetResult());
    }

    auto InstrumentHTX::AutoFocusEnabled() const -> bool {
        return d->autofocus.enabled;
    }

    auto InstrumentHTX::PerformAutoFocus() -> std::tuple<bool,bool> {
        auto afCmd = std::make_shared<CommandAutoFocus>(true, true, d->config.GetAFMaxTrials());
        if(!d->RunAndWait(afCmd, 30)) {
            d->SetError(afCmd->GetResponse()->GetMessage());
            return std::make_tuple(false, false);
        }

        d->autofocus.enabled = true;

        auto resp = afCmd->GetResponse();
        return std::make_tuple(true, resp->GetResult());
    }

    auto InstrumentHTX::ReadAFSensorValue() -> int32_t {
        auto afCmd = std::make_shared<CommandReadAFSensorValue>();
        if(!d->RunAndWait(afCmd, 10)) {
            d->SetError(afCmd->GetResponse()->GetMessage());
            return 0;
        }

        return afCmd->GetValue();
    }

    auto InstrumentHTX::SetBestFocusCurrent(int32_t& value) -> bool {
        auto afCmd = std::make_shared<CommandSetBestFocus>();
        if(!d->RunAndWait(afCmd, 10)) {
            d->SetError(QString("Failed to set the best focus value"));
            return false;
        }

        value = afCmd->GetNewTarget();

        return true;
    }

    auto InstrumentHTX::SetBestFocus(int32_t value) -> bool {
        auto afCmd = std::make_shared<CommandSetBestFocus>(value);
        if(!d->RunAndWait(afCmd, 10)) {
            d->SetError(QString("Failed to set the best focus value"));
            return false;
        }
        return true;
    }

    auto InstrumentHTX::ScanFocus(double startMm, double distMm, int32_t count) -> QList<int32_t> {
        const auto ppm = d->config.AxisResolutionPPM(Axis::AxisZ);
        const auto startPulse = static_cast<int32_t>(startMm * ppm);
        const auto endPulse = static_cast<int32_t>((startMm + distMm) * ppm);
        const auto interval = static_cast<int32_t>(distMm * ppm / count);
        const auto delayUSec = 100000;

        auto cmd = std::make_shared<CommandScanFocus>(startPulse, endPulse, interval, delayUSec,
                                                      d->config.GetAxisInPositionInPulse(Axis::AxisZ));
        if(!d->RunAndWait(cmd)) {
            auto resp = cmd->GetResponse();
            d->SetError(resp->GetMessage());
            return QList<int32_t>();
        }
        return cmd->GetValues();
    }

    auto InstrumentHTX::SetAcquisitionPattenIndex(const AppEntity::ImagingType imagingType,
                                                  const AppEntity::TriggerType trigger,
                                                  const QList<int32_t>& index) -> void {
        IlluminationPatterns::Set(imagingType, trigger, index);
    }

    auto InstrumentHTX::SetAcquisitionChannelIndex(const AppEntity::ImagingType imagingType,
                                                   const AppEntity::TriggerType trigger,
                                                   const int32_t ledChannel,
                                                   const QList<int32_t>& index) -> void {
        AcquisitionChannels::Set(imagingType, trigger, ledChannel, index);
    }

    auto InstrumentHTX::StartCondenserAutoFocus(int32_t patternIndex, int32_t intensity) -> bool {
        using TriggerSource = TC::CameraControl::TriggerSource;

        bool result = true;

        d->status.cafProgress.reset(new CondenserAutofocusProgress());

        //auto imgCamera = d->cameraManager->GetImagingCamera();
        auto conCamera = d->cameraManager->GetCondenserCamera();

        //let the imaging camera ignores trigger signal, but the condenser camera accepts it
        //imgCamera->SetTriggerSource(TriggerSource::Software);
        conCamera->SetTriggerSource(TriggerSource::Line_2, false);     //TODO This parameter should be from model information

        RawPosition xpos, ypos, zpos;
        GetPositionXYZ(xpos, ypos, zpos);

        auto cmd = std::make_shared<CommandCondenserAutoFocus>(d->status.cafProgress);
        cmd->SetConfig(d->config);
        cmd->SetPatternIndex(patternIndex);
        cmd->SetIntensity(intensity);
        cmd->SetPositions({xpos, ypos, zpos});
        if(!d->RunAndWait(cmd, noWait)) {
            d->SetError(QString("Failed to find best condenser position"));
            result = false;
        }

        return result;
    }

    auto InstrumentHTX::CheckCondenserAutoFocusProgress() const -> std::tuple<bool, double> {
        return d->status.cafProgress->GetProgress();
    }

    auto InstrumentHTX::FinishCondenserAutoFocus() -> void {
        using TriggerSource = TC::CameraControl::TriggerSource;

        //auto imgCamera = d->cameraManager->GetImagingCamera();
        auto conCamera = d->cameraManager->GetCondenserCamera();

        //let the imaging camera accepts trigger signal, but the condenser camera ignores it
        //imgCamera->SetTriggerSource(TriggerSource::Line_2);     //TODO This parameter should be from model information
        conCamera->SetTriggerSource(TriggerSource::Software, false);
    }

    auto InstrumentHTX::StartHTIlluminationCalibration(const QList<int32_t>& intensityList,
                                                       const AppEntity::ImagingConditionHT::Pointer imgCond) -> bool {
        d->status.htIllumCalProgress.reset(new HTIlluminationCalibrationProgress());

        RawPosition xpos, ypos, zpos;
        GetPositionXYZ(xpos, ypos, zpos);

        auto acqCmd = std::make_shared<CommandHTIlluminationCalibration>(intensityList,
                                                                         imgCond,
                                                                         QList{xpos, ypos, zpos},
                                                                         d->config, 
                                                                         d->status.htIllumCalProgress);
        if(!d->RunAndWait(acqCmd, noWait)) {
            d->SetError(QString("Failed to start ht illumination calibration (%1)").arg(acqCmd->GetResponse()->GetMessage()));
            return false;
        }
        return true;
    }

    auto InstrumentHTX::CheckHTIlluminationCalibrationProgress() const -> std::tuple<bool, double> {
        return d->status.htIllumCalProgress->GetProgress();
    }

    auto InstrumentHTX::FinishHTIlluminationCalibration() -> void {
        //nothing to do for now...
    }

    auto InstrumentHTX::IsSupported(InstrumentFeature feature) const -> bool {
        return FeatureMap::IsSupported(feature);
    }

    auto InstrumentHTX::GetError() const -> QString {
        return d->error;
    }

    void InstrumentHTX::run() {
        QLOG_INFO() << "InstrumentHTX thread is started";

        while(true) {
            QMutexLocker locker(&d->cmdMutex);
            if(!d->running) break;

            auto command = d->GetCommand();

            //If no command is waiting...
            if(command == nullptr) {
                d->cmdWait.wait(locker.mutex(), 300);
                continue;
            }

            QMutexLocker respLocker(&d->respMutex);
            locker.unlock();

            QLOG_INFO() << "Perform : " << command->GetTitle();

            if(!command->Perform()) {
                auto resp = command->GetResponse();
                d->SetError(QString("It fails to perform command (%1) : %2").arg(command->GetTitle()).arg(resp->GetMessage()));
            }

            d->respWait.wakeOne();
        }

        QLOG_INFO() << "InstrumentHTX thread is finished";
    }
}
