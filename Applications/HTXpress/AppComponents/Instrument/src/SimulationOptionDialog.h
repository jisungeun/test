#pragma once
#include <memory>

#include <QDialog>

namespace HTXpress::AppComponents::Instrument {
    class SimulationOptionDialog : public QDialog {
        Q_OBJECT

    public:
        explicit SimulationOptionDialog(QWidget* parent = nullptr);
        ~SimulationOptionDialog() override;

    protected slots:
        void onOpenImagePath();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}