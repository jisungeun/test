#include <QList>

#include "MacroGroup.h"

namespace HTXpress::AppComponents::Instrument {
    struct MacroGroup::Impl {
        int32_t groupID{ 0 };
        QList<Macro::Pointer> list;
    };

    MacroGroup::MacroGroup() {
    }

    MacroGroup::MacroGroup(int32_t groupID) {
        d->groupID = groupID;
    }

    MacroGroup::MacroGroup(const MacroGroup& other) {
        d->groupID = other.d->groupID;
        d->list = other.d->list;
    }

    MacroGroup::~MacroGroup() {
    }

    auto MacroGroup::SetGroupID(int32_t id) -> void {
        d->groupID = id;

        for(auto& macro : d->list) {
            macro->SetGroupID(id);
        }
    }

    auto MacroGroup::GetGroupID() const -> int32_t {
        return d->groupID;
    }

    auto MacroGroup::AppendMacro(Macro::Pointer& macro) -> void {
        const auto count = d->list.size();
        macro->SetIndex(count);
        d->list.append(macro);
    }

    auto MacroGroup::AppendMacroGroup(MacroGroup::Pointer& macroGroup) -> void {
        const auto count = macroGroup->GetMacroCount();
        for(int32_t idx=0; idx<count; ++idx) {
            d->list.append(macroGroup->GetMacro(idx));
        }
    }

    auto MacroGroup::GetMacroCount() const -> int32_t {
        return d->list.size();
    }

    auto MacroGroup::GetMacro(int32_t index) const -> Macro::Pointer {
        if(index >= d->list.size()) return nullptr;
        return d->list.at(index);
    }

    auto MacroGroup::Clear() {
        d->list.clear();
    }
}
