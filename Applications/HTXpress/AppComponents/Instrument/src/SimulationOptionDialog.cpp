#include <QSettings>
#include <QFileDialog>

#include "SimulationOptions.h"
#include "ui_SimulationOptionDialog.h"
#include "SimulationOptionDialog.h"

namespace HTXpress::AppComponents::Instrument {
    struct SimulationOptionDialog::Impl {
        Ui::SimulationOptionDialog ui;
    };

    SimulationOptionDialog::SimulationOptionDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
        d->ui.setupUi(this);

        connect(d->ui.pathBtn, SIGNAL(clicked()), this, SLOT(onOpenImagePath()));
        connect(d->ui.closeBtn, SIGNAL(clicked()), this, SLOT(accept()));
    }

    SimulationOptionDialog::~SimulationOptionDialog() {
    }

    void SimulationOptionDialog::onOpenImagePath() {
        QSettings qs;

        auto path = qs.value("Simulation/ImagePath", "D:/").toString();
        path = QFileDialog::getExistingDirectory(this, "Select a simulation image directory", path);
        d->ui.pathEdit->setText(path);

        SimulationOptions::GetInstance()->SetImagePath(path);
    }
}
