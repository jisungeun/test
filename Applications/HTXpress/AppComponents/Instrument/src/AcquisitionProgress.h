#pragma once
#include <memory>

#include <Position.h>
#include "InstrumentDefines.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class AcquisitionProgress {
    public:
        using Pointer = std::shared_ptr<AcquisitionProgress>;

    public:
        AcquisitionProgress();
        ~AcquisitionProgress();

        auto SetProgress(double progress)->void;
        auto SetPosition(const AppEntity::Position& position)->void;
        auto GetProgress() const->std::tuple<bool,double,AppEntity::Position>;

        auto StopAcquisition()->void;
        auto IsStopRequested() const->bool;

        auto SetStopped()->void;
        auto GetStopped() const->bool;

        auto SetCompleted()->void;
        auto GetCompleted() const->bool;

        auto SetError(const QString& message)->void;
        auto GetError() const->bool;
        auto GetErrorMessage() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}