#pragma once
#include <memory>

#include "InstrumentDefines.h"
#include "Response.h"

namespace HTXpress::AppComponents::Instrument {

    class ResponseMotion :public Response {
    public:
        typedef std::shared_ptr<ResponseMotion> Pointer;

    public:
        ResponseMotion();
        virtual ~ResponseMotion();

        auto SetMoving(bool moving)->void;
        auto GetMoving() const->bool;

        auto SetAFFailed(bool fail)->void;
        auto GetAFFailed() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}