#include <QThread>
#include <QElapsedTimer>

#include <MCUFactory.h>
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    struct Command::Impl {
        QString title;

        auto CheckMoving(const TC::MCUControl::MCUResponse& response)->bool;
    };

    auto Command::Impl::CheckMoving(const TC::MCUControl::MCUResponse& response) -> bool {
        using ResponseCode = TC::MCUControl::Response;
        QVector<ResponseCode> codes{ ResponseCode::AxisXMoving, ResponseCode::AxisYMoving, ResponseCode::AxisZMoving,
                                     ResponseCode::AxisUMoving, ResponseCode::AxisVMoving, ResponseCode::AxisWMoving };
        for(auto code : codes) {
            if(response.GetValueAsFlag(code)) {
                return true;
            }
        }

        if(response.GetValue(ResponseCode::AfOn)==1) {
            return true;
        }

        return false;
    }

    Command::Command(const QString& title) : d{new Impl} {
        d->title = title;
    }

    Command::~Command() {
    }

    auto Command::GetTitle() const -> QString {
        return d->title;
    }

    auto Command::WaitForMotionStop(uint32_t msec, uint32_t interval) const -> bool {
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        QElapsedTimer timer;

        timer.start();
        while(timer.elapsed() < msec) {
            TC::MCUControl::MCUResponse status;
            mcuControl->CheckStatus(status);
            if(!d->CheckMoving(status)) return true;
            QThread::msleep(interval);
        }

        return false;
    }
}
