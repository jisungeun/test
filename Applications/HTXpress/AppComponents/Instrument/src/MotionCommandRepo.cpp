#include <QMap>

#include "MotionCommandRepo.h"

namespace HTXpress::AppComponents::Instrument {
    struct MatrixKey {
        int32_t countX { 1 };
        int32_t countY { 1 };
        int32_t distX { 0 };
        int32_t distY { 0 };

        MatrixKey(int32_t _countX, int32_t _countY, int32_t _distX, int32_t _distY) {
            countX = _countX;
            countY = _countY;
            distX = _distX;
            distY = _distY;
        }

        auto IsSingle() const->bool { return (countX*countY) == 1; }

        auto operator==(const MatrixKey& other) const->bool {
            if(countX != other.countX) return false;
            if(countY != other.countY) return false;
            if(distX != other.distX) return false;
            if(distY != other.distY) return false;
            return true;
        }

        auto operator<(const MatrixKey& other) const->bool {
            if(countX < other.countX) return true;
            if(countY < other.countY) return true;
            if(distX < other.distX) return true;
            if(distY < other.distY) return true;
            return false;
        }
    };

    struct MotionCommandRepo::Impl {
        QMap<MotionCommandName, Command::Pointer> commands;
        QMap<MatrixKey, Command::Pointer> matrixCommands;
    };

    MotionCommandRepo::MotionCommandRepo() : d{new Impl} {
    }

    MotionCommandRepo::~MotionCommandRepo() {
    }

    auto MotionCommandRepo::GetInstance() -> Pointer {
        static Pointer theInstance{ new MotionCommandRepo() };
        return theInstance;
    }

    auto MotionCommandRepo::SetCommand(MotionCommandName type, Command::Pointer cmd) -> void {
        d->commands[type] = cmd;
    }

    auto MotionCommandRepo::SetMatrixCommand(Command::Pointer cmd) -> void {
        auto* matrixCmd = dynamic_cast<MotionCommand::RunMacroMatrix*>(cmd.get());
        if(matrixCmd == nullptr) return;

        MatrixKey key(matrixCmd->GetCountX(), matrixCmd->GetCountY(), matrixCmd->GetDistanceX(), matrixCmd->GetDistanceY());
        d->matrixCommands[key] = cmd;

        if(key.IsSingle()) cmd->SetCommandID(MotionCommandName::RunMacroGroupSingle);
        else {
            MotionCommandName base = MotionCommandName::RunMacroGroup1;
            auto name = MotionCommandName::_from_integral(base._to_integral() + 0);
            cmd->SetCommandID(name);
        }

        d->commands[cmd->GetName()] = cmd;
    }

    auto MotionCommandRepo::GetNames() const -> QList<MotionCommandName> {
        return d->commands.keys();
    }

    auto MotionCommandRepo::GetCommand(MotionCommandName type) -> Command::Pointer {
        return d->commands[type];
    }

    auto MotionCommandRepo::GetCommands() -> QList<Command::Pointer> {
        return d->commands.values();
    }

    auto MotionCommandRepo::GetRunMacroMatrix(int32_t countX, int32_t countY, int32_t distX,
        int32_t distY) -> Command::Pointer {
        return d->matrixCommands[MatrixKey(countX, countY, distX, distY)];
    }

    auto MotionCommandRepo::Clear() -> void {
        d->commands.clear();
        d->matrixCommands.clear();
    }

    auto MotionCommandRepo::ClearMacroMatrix() -> void {
        d->matrixCommands.clear();
    }
}
