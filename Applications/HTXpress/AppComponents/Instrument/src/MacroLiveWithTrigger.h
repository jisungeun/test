#pragma once
#include <memory>
#include <QVector>
#include <QList>

#include "StreamingMacro.h"
#include "ImagingParameter.h"

namespace HTXpress::AppComponents::Instrument::MacroLiveWithTrigger {
    auto Start(const ImagingParameter& param, const QVector<int32_t>& pos,
               int32_t triggerCount, int32_t triggerInterval, int32_t pulseWidth)->QList<StreamingMacro::Pointer>;
}