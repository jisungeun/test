#define LOGGER_TAG "[CommandHTIlluminationCalibration]"
#include <TCLogger.h>
#include <MCUFactory.h>

#include "CameraManager.h"
#include "AcquisitionChannels.h"
#include "MacroHTIlluminationCalibration.h"
#include "CommandHTIlluminationCalibration.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandHTIlluminationCalibration::Impl {
        CommandHTIlluminationCalibration* p{nullptr};
        Progress::Pointer progress{ nullptr };
        Response::Pointer resp{ nullptr };

        QList<int32_t> intensityList;
        Config config;
        AppEntity::ImagingConditionHT::Pointer imageCond;
        QList<RawPosition> positions;

        Impl(CommandHTIlluminationCalibration* p,
             const QList<int32_t>& intensityList,
             const ImagingConditionHT::Pointer imageCond,
             QList<RawPosition> positions,
             const Config& config,
             Progress::Pointer progress)
                 : p(p)
                 , intensityList(intensityList)
                 , imageCond(imageCond)
                 , positions(positions)
                 , config(config)
                 , progress(progress) {
            resp = std::make_shared<Response>();
            resp->SetResult(false);
        }

        auto CountImages(const QList<StreamingMacro::Pointer>& macros)->int32_t;
        auto RunMacro(const QList<StreamingMacro::Pointer>& macros)->bool;
        auto SetError(const QString& message)->void;
    };

    auto CommandHTIlluminationCalibration::Impl::CountImages(const QList<StreamingMacro::Pointer>& macros) -> int32_t {
        int32_t images = 0;

        for(auto macro : macros) {
            images += macro->GetImageCount();
        }

        return images;
    }

    auto CommandHTIlluminationCalibration::Impl::RunMacro(const QList<StreamingMacro::Pointer>& macros) -> bool {
        using ImagingType = AppEntity::ImagingType;
        using TriggerType = AppEntity::TriggerType;

        auto camera = CameraManager::GetInstance()->GetImagingCamera();
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();
#if 1
        int32_t idx = 0;
        for(auto macro : macros) {
            QLOG_INFO() << "[" << ++idx << "]" << macro->Str();
        }
#endif
        camera->StopAcquisition();

        if(!p->WaitForMotionStop()) {
            QLOG_ERROR() << "Motion is not stopped";
            progress->SetError("Motion is not stopped");
            return false;
        }

        mcuControl->SetLEDChannel(AcquisitionChannels::GetLedChannel(ImagingType::FL3D, TriggerType::Trigger));

        camera->StartAcquisition(CountImages(macros));

        if(!mcuControl->StartMacroStreaming()) {
            QLOG_ERROR() << "It fails to start macro streaming";
            progress->SetError("It fails to start macro streaming");
            return false;
        }

        const auto macroCount = macros.length();
        int32_t macroIndex = 0;

        TC::MCUControl::MCUResponse status;
        while(mcuControl->CheckStatus(status)) {
            const auto completed = status.GetValue(TC::MCUControl::Response::MacroStreamingIndex);
            const auto halfEmpty = status.GetValue(TC::MCUControl::Response::MacroStreamingHalfEmpty);
            const auto error = status.GetValue(TC::MCUControl::Response::MacroStreamingError);

            progress->SetProgress((completed*1.0)/macroCount);

            if(error == 1) {
                QLOG_ERROR() << "Error is occurred during streaming";
                progress->SetError("Error is occurred during streaming");
                return false;
            }

            if(completed == macroCount) {
                QLOG_INFO() << "All macros are completed";
                break;
            }

            if(halfEmpty && (macroIndex < macroCount)) {
                QLOG_INFO() << "Macro completed:" << completed << " sent:" << macroIndex << " total:" << macroCount;
                const auto lastIndex = std::min(macroIndex+200, macroCount);
                for(; macroIndex<lastIndex; macroIndex++) {
                    auto macro = macros.at(macroIndex);
                    if(!mcuControl->SendMacrosToStreamBuffer({QList<int32_t>::fromVector(macro->GetPacket())})) {
                        QLOG_ERROR() << "It fails to send streaming packets";
                        progress->SetError("It fails to send streaming packets");

                        mcuControl->StopMacroStreaming();
                        return false;
                    }
                }
            }

            QThread::msleep(500);
        }

        if(!mcuControl->StopMacroStreaming()) {
            QLOG_ERROR() << "It fails to stop macro streaming";
            progress->SetError("It fails to stop macro streaming");
            return false;
        }

        mcuControl->SetLEDChannel(AcquisitionChannels::GetLedChannel(ImagingType::BFGray, TriggerType::Trigger));

        return true;
    }

    auto CommandHTIlluminationCalibration::Impl::SetError(const QString& message) -> void {
        progress->SetError(message);
    }

    CommandHTIlluminationCalibration::CommandHTIlluminationCalibration(const QList<int32_t>& intensityList,
                                                                       ImagingConditionHT::Pointer imageCond,
                                                                       const QList<RawPosition>& positions,
                                                                       const Config& config,
                                                                       Progress::Pointer progress)
        : Command("HTIlluminationCalibration")
        , d{new Impl(this, intensityList, imageCond, positions, config, progress)} {
    }

    CommandHTIlluminationCalibration::~CommandHTIlluminationCalibration() {
    }

    auto CommandHTIlluminationCalibration::Perform() -> bool {
        d->resp->SetResult(false);

        auto macros = MacroHTIlluminationCalibration::Setup(d->intensityList, d->positions, d->imageCond, d->config);
        if(!d->RunMacro(macros)) {
            d->SetError("It fails to run acquisition macros");
            return false;
        }

        d->resp->SetResult(true);
        return true;
    }

    auto CommandHTIlluminationCalibration::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}