#pragma once
#include <memory>

#include "MacroHandler.h"

namespace HTXpress::AppComponents::Instrument {
    class MacroHandlerBF : public MacroHandler {
    public:
        MacroHandlerBF();
        ~MacroHandlerBF() override;

        auto Handle(const ImagingCondition::Pointer& cond) const -> MacroGroup::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}