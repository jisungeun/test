#pragma once
#include <memory>

#include <QList>

namespace HTXpress::AppComponents::Instrument {
    class Macro {
    public:
        typedef std::shared_ptr<Macro> Pointer;

    public:
        Macro();
        Macro(int32_t groupID);
        Macro(const Macro& other);
        ~Macro();

        auto operator=(const Macro& other)->Macro&;

        auto SetGroupID(int32_t id)->void;
        auto GetGroupID() const->int32_t;

        auto SetIndex(int32_t index)->void;
        auto GetIndex() const->int32_t;

        auto SetCommandID(int32_t commandID)->void;
        auto GetCommandID() const->int32_t;

        auto SetPosition(int32_t posX, int32_t posY, int32_t posZ)->void;
        auto GetPositionX() const->int32_t;
        auto GetPositionY() const->int32_t;
        auto GetPositionZ() const->int32_t;

        auto SetAutoFocusMode(int32_t mode)->void;
        auto GetAutoFocusMode() const->int32_t;

        auto GetParameters() const->QList<int32_t>;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}