#define LOGGER_TAG "[CommandMotion]"
#include <QElapsedTimer>

#include <TCLogger.h>
#include <MCUFactory.h>
#include "CommandMotion.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandMotion::Impl {
        Axis axis{ Axis::AxisX };
        int32_t pos{ 0 };
        Response::Pointer resp{ nullptr };
    };

    CommandMotion::CommandMotion(Axis axis, int32_t pos) : Command("MoveAxis"), d{new Impl} {
        d->axis = axis;
        d->pos = pos;
        d->resp = std::make_shared<Response>();
    }

    CommandMotion::~CommandMotion() {
    }

    bool CommandMotion::Perform() {
        using Flag = TC::MCUControl::Flag;
        auto axis = [=]()->TC::MCUControl::Axis {
            switch(d->axis) {
            case Axis::AxisX:
                return TC::MCUControl::Axis::X;
            case Axis::AxisY:
                return TC::MCUControl::Axis::Y;
            case Axis::AxisZ:
                return TC::MCUControl::Axis::Z;
            case Axis::AxisC:
                return TC::MCUControl::Axis::U;
            case Axis::AxisF:
                return TC::MCUControl::Axis::V;
            case Axis::AxisD:
                return TC::MCUControl::Axis::W;
            }
            return TC::MCUControl::Axis::X;
        }();

        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        if(!WaitForMotionStop()) {
            QLOG_ERROR() << "Motion is not stopped";
            return false;
        }

        if(!mcuControl->Move(axis, d->pos)) {
            const auto error = mcuControl->GetLastError();
            d->resp->SetMessage(QString("It fails to move %1 to %2 [%3]").arg(d->axis._to_string()).arg(d->pos).arg(error.GetText()));
            d->resp->SetResult(false);
            return false;
        }

        d->resp->SetResult(true);
        return true;
    }

    auto CommandMotion::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}
