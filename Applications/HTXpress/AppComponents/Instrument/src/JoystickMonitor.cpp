#define LOGGER_TAG "[JoystickMonitor]"
#include <TCLogger.h>

#include <JoystickControlFactory.h>
#include "JoystickMonitor.h"

#include "JoystickControlFactory.h"

namespace HTXpress::AppComponents::Instrument {
    using JoystickPtr = TC::JoystickControl::JoystickControl::Pointer;
    using joystickFactory = TC::JoystickControl::JoystickControlFactory;

    struct JoystickMonitor::Impl {
        JoystickPtr joystick{ joystickFactory::Create(joystickFactory::Controller::SpaceMouse) };

        bool initialized{ false };
        bool enable{ false };
        bool running{ true };
    };

    JoystickMonitor::JoystickMonitor() : d{new Impl} {
    }

    JoystickMonitor::~JoystickMonitor() {
    }

    auto JoystickMonitor::GetInstance() -> Pointer {
        static Pointer theInstance{ new JoystickMonitor() };
        return theInstance;
    }

    auto JoystickMonitor::SetEnable() -> void {
        d->enable = true;
    }

    auto JoystickMonitor::SetDisable() -> void {
        d->enable = false;
    }

    auto JoystickMonitor::CheckJoystick() -> JoystickStatus {
        JoystickStatus status;
        if(!d->enable) return status;

        if(!d->initialized) {
            d->initialized = d->joystick->Initialize();
        }
        if(!d->initialized) return status;

        auto resp = d->joystick->Read();
        status.status = [=]() -> JoystickTriggerStatus {
            using RawStatus = TC::JoystickControl::JoystickControl::Status;
            using RawDirection = TC::JoystickControl::JoystickControl::Direction;

            switch(resp.status) {
            case RawStatus::Hold:
                return JoystickTriggerStatus::Hold;
            case RawStatus::Stop:
                return JoystickTriggerStatus::Stop;
            }

            switch(resp.direction) {
            case RawDirection::Plus:
                return JoystickTriggerStatus::PlusDirection;
            case RawDirection::Minus:
                return JoystickTriggerStatus::MinusDirection;
            }

            return JoystickTriggerStatus::Hold;
        }();

        status.axis = [=]() -> JoystickAxis {
            using RawAxis = TC::JoystickControl::JoystickControl::Axis;

            switch(resp.axis) {
            case RawAxis::XAxis:
                return JoystickAxis::AxisX;
            case RawAxis::YAxis:
                return JoystickAxis::AxisY;
            case RawAxis::ZAxis:
                return JoystickAxis::AxisZ;
            }

            return JoystickAxis::AxisX;
        }();

        return status;
    }
}
