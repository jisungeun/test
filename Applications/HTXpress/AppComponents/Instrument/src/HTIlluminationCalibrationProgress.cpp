#include <QMutex>
#include "HTIlluminationCalibrationProgress.h"

namespace HTXpress::AppComponents::Instrument {
    struct HTIlluminationCalibrationProgress::Impl {
        double progress{ 0 };
        bool error{ false };
        QString errorMessage;
        QMutex mutex;
    };

    HTIlluminationCalibrationProgress::HTIlluminationCalibrationProgress() : d{new Impl} {
    }

    HTIlluminationCalibrationProgress::~HTIlluminationCalibrationProgress() {
    }

    auto HTIlluminationCalibrationProgress::SetProgress(double progress) -> void {
        QMutexLocker locker(&d->mutex);
        d->progress = progress;
    }

    auto HTIlluminationCalibrationProgress::GetProgress() const -> std::tuple<bool, double> {
        QMutexLocker locker(&d->mutex);
        return std::make_tuple(d->error, d->progress);
    }

    auto HTIlluminationCalibrationProgress::SetError(const QString& message) -> void {
        QMutexLocker locker(&d->mutex);
        d->error = true;
        d->errorMessage = message;
    }

    auto HTIlluminationCalibrationProgress::GetError() const -> bool {
        QMutexLocker locker(&d->mutex);
        return d->error;
    }

    auto HTIlluminationCalibrationProgress::GetErrorMessage() const -> QString {
        QMutexLocker locker(&d->mutex);
        return d->errorMessage;
    }
}
