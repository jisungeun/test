#include <MCUFactory.h>

#include "JoystickMonitor.h"
#include "CommandGetPosition.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandGetPosition::Impl {
        ResponsePosition::Pointer response{ new ResponsePosition() };

        auto Convert(TC::MCUControl::Axis axis)->Axis;
    };

    auto CommandGetPosition::Impl::Convert(TC::MCUControl::Axis axis) -> Axis {
        switch(axis) {
        case TC::MCUControl::Axis::X:
            return Axis::AxisX;
        case TC::MCUControl::Axis::Y:
            return Axis::AxisY;
        case TC::MCUControl::Axis::Z:
            return Axis::AxisZ;
        case TC::MCUControl::Axis::U:
            return Axis::AxisC;
        case TC::MCUControl::Axis::V:
            return Axis::AxisF;
        case TC::MCUControl::Axis::W:
            return Axis::AxisD;
        }
        return Axis::AxisX;
    }

    CommandGetPosition::CommandGetPosition() : Command("GetPosition"), d{new Impl} {
    }

    CommandGetPosition::~CommandGetPosition() {
    }

    auto CommandGetPosition::Perform() -> bool {
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        const auto [success, positions] = mcuControl->GetPositions();
        if(!success) {
            d->response->SetError();
            return false;
        }

        for(auto iter = positions.begin(); iter!=positions.end(); ++iter) {
            const auto& axis = iter.key();
            const auto& pos = iter.value();

            d->response->SetPosition(d->Convert(axis), pos);
        }

        return true;
    }

    auto CommandGetPosition::GetResponse() -> Response::Pointer {
        return d->response;
    }
}
