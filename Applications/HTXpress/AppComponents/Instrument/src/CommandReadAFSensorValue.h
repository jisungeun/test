#pragma once
#include <memory>
#include <QList>

#include "InstrumentDefines.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandReadAFSensorValue : public Command {
    public:
        CommandReadAFSensorValue();
        ~CommandReadAFSensorValue();

        auto Perform() -> bool override;
        auto GetResponse() -> Response::Pointer override;

        auto GetValue() const -> int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}