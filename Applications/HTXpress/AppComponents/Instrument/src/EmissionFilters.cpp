#include "EmissionFilters.h"

namespace HTXpress::AppComponents::Instrument {
    struct EmissionFilters::Impl {
        QMap<AppEntity::FLFilter, int32_t> filters;
    };

    EmissionFilters::EmissionFilters() : d{new Impl} {
    }

    EmissionFilters::~EmissionFilters() {
    }

    auto EmissionFilters::GetInstance() -> Pointer {
        static Pointer theInstance{ new EmissionFilters() };
        return theInstance;
    }

    auto EmissionFilters::Set(const int32_t channel, const AppEntity::FLFilter& filter) -> void {
        GetInstance()->d->filters[filter] = channel;
    }

    auto EmissionFilters::Set(const QMap<AppEntity::FLFilter, int32_t>& filters) -> void {
        GetInstance()->d->filters = filters;
    }

    auto EmissionFilters::Get(const AppEntity::FLFilter& filter) -> int32_t {
        const auto iter = GetInstance()->d->filters.find(filter);
        if(iter == GetInstance()->d->filters.end()) return -1;
        return iter.value();
    }

    auto EmissionFilters::GetVoidChannel() -> int32_t {
        return Get({0, 0});
    }
}
