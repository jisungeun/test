#define LOGGER_TAG "[CommandMotion]"
#include <QElapsedTimer>
#include <QThread>

#include <TCLogger.h>
#include <MCUFactory.h>
#include "CommandMotionXY.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandMotionXY::Impl {
        int32_t posX{ 0 };
        int32_t posY{ 0 };
        Response::Pointer resp{ nullptr };
    };

    CommandMotionXY::CommandMotionXY(int32_t targetXPulse, int32_t targetYPulse) : Command("MoveAxisXY"), d{new Impl} {
        d->posX = targetXPulse;
        d->posY = targetYPulse;
        d->resp = std::make_shared<Response>();
    }

    CommandMotionXY::~CommandMotionXY() {
    }

    bool CommandMotionXY::Perform() {
        using Flag = TC::MCUControl::Flag;
        using Axis = TC::MCUControl::Axis;

        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        if(!WaitForMotionStop()) {
            QLOG_ERROR() << "Motion is not stopped";
            return false;
        }

        if(!mcuControl->MoveXY(d->posX, d->posY)) {
            const auto error = mcuControl->GetLastError();
            d->resp->SetMessage(QString("It fails to move sample stage [%1]").arg(error.GetText()));
            d->resp->SetResult(false);
            return false;
        }

        d->resp->SetResult(true);
        return true;
    }

    auto CommandMotionXY::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}
