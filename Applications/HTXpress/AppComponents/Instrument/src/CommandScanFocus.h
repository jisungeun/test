#pragma once
#include <memory>
#include <QList>

#include "InstrumentDefines.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandScanFocus : public Command {
    public:
        CommandScanFocus(int32_t startPulse, int32_t endPulse, int32_t internval, int32_t delayUSec, int32_t zInPositionRange);
        ~CommandScanFocus() override;

        auto Perform() -> bool override;
        auto GetResponse() -> Response::Pointer override;

        auto GetValues() const -> QList<int32_t>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}