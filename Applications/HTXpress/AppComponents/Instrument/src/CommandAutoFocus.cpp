#define LOGGER_TAG "[CommandAutoFocus]"
#include <QElapsedTimer>

#include <TCLogger.h>
#include <MCUFactory.h>

#include "CommandAutoFocus.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandAutoFocus::Impl {
        CommandAutoFocus* p{ nullptr };
        bool enable;
        bool staticAF;
        int32_t maximumTrial{ 10 };
        Response::Pointer resp{ nullptr };

        Impl(CommandAutoFocus* p, bool enable, bool staticAF, int32_t maxTrials)
            : p(p), enable(enable), staticAF(staticAF), maximumTrial(maxTrials) {
            resp = std::make_shared<Response>();
        }
    };

    CommandAutoFocus::CommandAutoFocus(bool enable, bool staticAF, int32_t maximumTrial)
        : Command("AutoFocus")
        , d{new Impl(this, enable, staticAF, maximumTrial)} {
    }

    CommandAutoFocus::~CommandAutoFocus() {
    }

    auto CommandAutoFocus::Perform() -> bool {
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        d->resp->SetResult(false);

        if(d->staticAF == false) {
            d->resp->SetMessage("Tracking auto focus is not implemented");
            return false;
        }

        if(d->enable == false) {
            d->resp->SetMessage("Disable auto focus is not implemented");
            return false;
        }

        if(!mcuControl->EnableAF(0)) {
            d->resp->SetMessage("It fails to disable auto focus");
            return false;
        }

        if(!mcuControl->EnableAF(1)) {
            d->resp->SetMessage("It fails to start static auto focus");
            return false;
        }

        constexpr auto timeoutMSec = 60000;
        int32_t lastTrial = -1;

        QElapsedTimer timer;
        timer.start();
        while(timer.elapsed() < timeoutMSec) {
            int32_t status, result, trialCount;
            if(!mcuControl->CheckAFStatus(status, result, trialCount)) {
                d->resp->SetMessage("It fails to read auto focus status");
                return false;
            }

            if(lastTrial != trialCount) {
                QLOG_INFO() << "status=" << status << " result=" << result << " trials=" << trialCount;
                lastTrial = trialCount;
            }

            if(status == -1) {
                d->resp->SetMessage(QString("It fails to find best focus [result=%1 trials=%2]").arg(result).arg(trialCount));
                break;
            } else if(status == 0) {
                if(trialCount < d->maximumTrial) {
                    d->resp->SetResult(true);
                }
                break;
            }
        }

        if(d->resp->GetResult() == false && timer.elapsed() >= timeoutMSec) {
            QLOG_ERROR() << "Timeout";
        }

        QList<int32_t> adcDiff, zComp;
        if(mcuControl->ReadAFLog(adcDiff, zComp)) {
            QStringList strAdcDiff, strZComp;
            std::for_each(adcDiff.begin(), adcDiff.end(), [&strAdcDiff](int32_t value) {
                strAdcDiff.push_back(QString::number(value));
            });
            std::for_each(zComp.begin(), zComp.end(), [&strZComp](int32_t value) {
                strZComp.push_back(QString::number(value));
            });

            QLOG_INFO() << "AF Log - ADC: " << strAdcDiff.join(',') << " ZComp:" << strZComp.join(',');
        }

        return true;
    }

    auto CommandAutoFocus::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}