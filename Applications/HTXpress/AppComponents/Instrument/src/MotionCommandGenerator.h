#pragma once
#include <memory>

#include "InstrumentDefines.h"

namespace HTXpress::AppComponents::Instrument {
    class MotionCommandGenerator {
    public:
        MotionCommandGenerator();
        ~MotionCommandGenerator();

        //Live
        auto StopAcquisition()->bool;
        auto StartBFLive(int32_t intensityR, int32_t intensityG, int32_t intensityB, int32_t exposure, int32_t interval)->bool;
        auto StartFLLive(int32_t channel, int32_t intensityR, int32_t intensityG, int32_t intensityB, 
                         int32_t exposure, int32_t interval)->bool;

        //FL Acquisition
        auto ChangeFilter(int32_t channel)->bool;
        auto StartFLAcquisition(int32_t channel, int32_t intensityR, int32_t intensityG, int32_t intensityB, 
                                int32_t exposure, int32_t interval)->bool;
        auto AcquireFL3D(Axis axis, int32_t motionStart, int32_t motionEnd, 
                         int32_t triggerStart, int32_t triggerEnd, int32_t interval, int32_t pulseWidth,
                         int32_t speed, int32_t acceleration)->bool;
        auto AcquireFL2D(int32_t triggerCount, int32_t interval, int32_t pulseWidth)->bool;

        //HT Acquisition
        auto StartHTAcquisition(int32_t NAIdx, int32_t intensityR, int32_t intensityG, int32_t intensityB, 
                                int32_t exposure, int32_t interval)->bool;
        auto AcquireHT3D(Axis axis, int32_t motionStart, int32_t motionEnd, 
                         int32_t triggerStart, int32_t triggerEnd, int32_t interval, int32_t pulseWidth,
                         int32_t speed, int32_t acceleration)->bool;
        auto AcquireHT2D(int32_t triggerCount, int32_t interval, int32_t pulseWidth)->bool;

        //BF Acquisition
        auto StartMonoBFAcquisition(int32_t intensityR, int32_t intensityG, int32_t intensityB, int32_t exposure, 
                                    int32_t interval)->bool;
        auto AcquireMonoBF(int32_t triggerCount, int32_t interval, int32_t pulseWidth)->bool;
        auto StartColorBFAcquisition(int32_t intensityR, int32_t intensityG, int32_t intensityB, int32_t exposure, 
                                     int32_t interval)->bool;
        auto AcquireColorBF(int32_t triggerCount, int32_t interval, int32_t pulseWidth)->bool;

        //Macro Matrix
        auto ClearMacroMatrix()->void;
        auto SetMacroMatrix(int32_t countX, int32_t countY, int32_t distX, int32_t distY, 
                            int32_t macroGroup, int32_t scanDirection)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}