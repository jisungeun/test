#pragma once
#include <memory>
#include <QVector>

#include "InstrumentDefines.h"
#include "ImagingParameter.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandLiveWithTrigger : public Command {
    public:
        CommandLiveWithTrigger(const ImagingParameter& param, const QVector<int32_t>& pos,
            int32_t triggerCount, int32_t triggerInterval, int32_t pulseWidth);
        ~CommandLiveWithTrigger() override;

        auto UseHighConversionGain(bool useHCG, double gain)->void;

        auto Perform() -> bool override;
        auto GetResponse()->Response::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}