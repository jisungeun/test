#pragma once
#include <memory>
#include <QVariant>

#include "InstrumentDefines.h"

namespace HTXpress::AppComponents::Instrument {
    class FeatureMap {
    public:
        using Pointer = std::shared_ptr<FeatureMap>;

    protected:
        FeatureMap();

    public:
        ~FeatureMap();

        static auto GetInstance()->Pointer;
        static auto Set(InstrumentFeature feature, const QVariant& value)->void;
        static auto Get(InstrumentFeature feature)->QVariant;
        static auto IsSupported(InstrumentFeature feature)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}