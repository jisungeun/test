#pragma once
#include <memory>

#include <ImagingCondition.h>
#include <PositionGroup.h>

#include "InstrumentDefines.h"
#include "InstrumentConfig.h"
#include "HTIlluminationCalibrationProgress.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandHTIlluminationCalibration : public Command {
    public:
        using Progress = HTIlluminationCalibrationProgress;
        using ImagingConditionHT = AppEntity::ImagingConditionHT;
        using Position = AppEntity::Position;

    public:
        CommandHTIlluminationCalibration(const QList<int32_t>& intensityList,
                                         const ImagingConditionHT::Pointer imageCond,
                                         const QList<RawPosition>& positions,
                                         const Config& config,
                                         Progress::Pointer progress);
        ~CommandHTIlluminationCalibration() override;

        auto Perform() -> bool override;
        auto GetResponse()->Response::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}