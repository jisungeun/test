#define LOGGER_TAG "[CommandAcquisition]"
#include <QThread>
#include <QSettings>

#include <TCLogger.h>

#include <MCUFactory.h>

#include "CameraManager.h"
#include "AcquisitionChannels.h"
#include "MacroAcquisition.h"
#include "CommandAcquisition.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandAcquisition::Impl {
        CommandAcquisition* p{ nullptr };
        AppEntity::ImagingSequence::Pointer sequence;
        QList<AppEntity::PositionGroup> positions;
        bool useAutoFocus;
        AcquisitionProgress::Pointer progress{ nullptr };
        Response::Pointer resp{ nullptr };
        Config config;
        bool useMultiDishHolder{ false };
        AppEntity::WellIndex startingWellIndex{ 0 };
        double focusReadyMM{ 0 };
        bool profilingLog{ false };

        Impl(CommandAcquisition* p, 
             const AppEntity::ImagingSequence::Pointer& sequence,
             const QList<AppEntity::PositionGroup>& positions,
             const bool useAutoFocus,
             AcquisitionProgress::Pointer progress)
            : p(p), sequence(sequence), positions(positions), useAutoFocus(useAutoFocus), progress(progress)
        {
            resp = std::make_shared<Response>();

            profilingLog = QSettings().value("Misc/Acquisition_Profiling_Log", false).toBool();
        }

        auto CountImages(const QList<StreamingMacro::Pointer>& macros)->int32_t;
        auto RunMacro(const QList<StreamingMacro::Pointer>& macros)->bool;
        auto SetError(const QString& message)->void;
        auto GetPosition(TC::MCUControl::MCUResponse& status)->AppEntity::Position;
        auto ToMM(Axis axis, int32_t pulses) -> RawPosition;
    };

    auto CommandAcquisition::Impl::CountImages(const QList<StreamingMacro::Pointer>& macros) -> int32_t {
        int32_t images = 0;

        for(auto macro : macros) {
            images += macro->GetImageCount();
        }

        return images;
    }

    auto CommandAcquisition::Impl::RunMacro(const QList<StreamingMacro::Pointer>& macros) -> bool {
        using ImagingType = AppEntity::ImagingType;
        using TriggerType = AppEntity::TriggerType;

        auto camera = CameraManager::GetInstance()->GetImagingCamera();
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();
#if 1
        int32_t idx = 0;
        for(auto macro : macros) {
            QLOG_INFO() << "[" << ++idx << "]" << macro->Str();
        }
#endif
        camera->StopAcquisition();

        if(!p->WaitForMotionStop()) {
            QLOG_ERROR() << "Motion is not stopped";
            progress->SetError("Motion is not stopped");
            return false;
        }

        mcuControl->SetLEDChannel(AcquisitionChannels::GetLedChannel(ImagingType::FL3D, TriggerType::Trigger));

        camera->StartAcquisition(CountImages(macros));

        if(!mcuControl->StartMacroStreaming()) {
            QLOG_ERROR() << "It fails to start macro streaming";
            progress->SetError("It fails to start macro streaming");
            return false;
        }

        const auto macroCount = macros.length();
        int32_t macroIndex = 0;

        TC::MCUControl::MCUResponse status;
        while(mcuControl->CheckStatus(status)) {
            const auto completed = status.GetValue(TC::MCUControl::Response::MacroStreamingIndex);
            const auto halfEmpty = status.GetValue(TC::MCUControl::Response::MacroStreamingHalfEmpty);
            const auto error = status.GetValue(TC::MCUControl::Response::MacroStreamingError);

            progress->SetProgress((completed*1.0)/macroCount);
            progress->SetPosition(GetPosition(status));

            if(profilingLog) {
                const auto pos = GetPosition(status).toMM();
                QLOG_INFO() << "completed=" << completed << " progress=" << (completed*1.0)/macroCount
                            << " position=[" << pos.x << "," << pos.y << "," << pos.z << "]";
            }

            if(error == 1) {
                QLOG_ERROR() << "Error is occurred during streaming";
                progress->SetError("Error is occurred during streaming");
                return false;
            }

            if(completed == macroCount) {
                QLOG_INFO() << "All macros are completed";
                break;
            }

            if(progress->IsStopRequested()) {
                progress->SetStopped();
                QLOG_INFO() << "Stop acquisition is requested";
                break;
            }

            if(halfEmpty && (macroIndex < macroCount)) {
                QLOG_INFO() << "Macro completed:" << completed << " sent:" << macroIndex << " total:" << macroCount;
                const auto lastIndex = std::min(macroIndex+200, macroCount);
                for(; macroIndex<lastIndex; macroIndex++) {
                    auto macro = macros.at(macroIndex);
                    if(!mcuControl->SendMacrosToStreamBuffer({QList<int32_t>::fromVector(macro->GetPacket())})) {
                        QLOG_ERROR() << "It fails to send streaming packets";
                        progress->SetError("It fails to send streaming packets");

                        mcuControl->StopMacroStreaming();
                        return false;
                    }
                }
            }

            if(profilingLog) {
                QThread::msleep(300);
            } else {
                QThread::msleep(500);
            }
        }

        if(!mcuControl->StopMacroStreaming()) {
            QLOG_ERROR() << "It fails to stop macro streaming";
            progress->SetError("It fails to stop macro streaming");
            return false;
        }

        mcuControl->SetLEDChannel(AcquisitionChannels::GetLedChannel(ImagingType::BFGray, TriggerType::Trigger));

        return true;
    }

    auto CommandAcquisition::Impl::SetError(const QString& message) -> void {
        resp->SetMessage(message);
        progress->SetError(message);
    }

    auto CommandAcquisition::Impl::GetPosition(TC::MCUControl::MCUResponse& status) -> AppEntity::Position {
        const auto xPosMM = ToMM(Axis::AxisX, status.GetValue(TC::MCUControl::Response::AxisXPosition));
        const auto yPosMM = ToMM(Axis::AxisY, status.GetValue(TC::MCUControl::Response::AxisYPosition));
        const auto zPosMM = ToMM(Axis::AxisZ, status.GetValue(TC::MCUControl::Response::AxisZPosition));
        return AppEntity::Position::fromMM(xPosMM, yPosMM, zPosMM);
    }

    auto CommandAcquisition::Impl::ToMM(Axis axis, int32_t pulses) -> RawPosition {
        auto resolution = config.AxisResolutionPPM(axis);
        return static_cast<RawPosition>(pulses) / resolution;
    }

    CommandAcquisition::CommandAcquisition(const AppEntity::ImagingSequence::Pointer& sequence,
                                           const QList<AppEntity::PositionGroup>& positions,
                                           const bool useAutoFocus,
                                           AcquisitionProgress::Pointer progress)
        : Command("Acquisition")
        , d{ new Impl(this, sequence, positions, useAutoFocus, progress) }
    {
    }

    CommandAcquisition::~CommandAcquisition() {
        if(d->progress) d->progress->SetCompleted();
    }

    auto CommandAcquisition::SetConfig(const Config& config) -> void {
        d->config = config;
    }

    auto CommandAcquisition::SetStartingWellIndex(AppEntity::WellIndex wellIndex) -> void {
        d->startingWellIndex = wellIndex;
    }

    auto CommandAcquisition::SetFocusReadyMM(double posMM) -> void {
        d->focusReadyMM = posMM;
    }

    auto CommandAcquisition::UseMultiDishHolder() -> void {
        d->useMultiDishHolder = true;
    }

    auto CommandAcquisition::Perform() -> bool {
        d->resp->SetResult(false);

        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();
        const auto [success, positions] = mcuControl->GetPositions();
        if(!success) {
            d->SetError("It fails to get axis positions");
            return false;
        }

        auto macros = MacroAcquisition::Setup(d->sequence, 
                                              d->positions, 
                                              d->useAutoFocus, 
                                              d->config, 
                                              positions,
                                              d->startingWellIndex,
                                              d->focusReadyMM,
                                              d->useMultiDishHolder ? d->config.GetMultiDishHolderThicknessMM() : 0
                                              );
        if(!d->RunMacro(macros)) {
            d->SetError("It fails to run acquisition macros");
            return false;
        }

        d->resp->SetResult(true);
        return true;
    }

    auto CommandAcquisition::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}
