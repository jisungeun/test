#pragma once
#include <memory>
#include <QMap>

#include <PositionGroup.h>
#include <ImagingScenario.h>
#include <FLFilter.h>

#include "InstrumentDefines.h"
#include "MacroGroup.h"

namespace HTXpress::AppComponents::Instrument {
    using ImagingScenario = AppEntity::ImagingScenario;
    using Position = AppEntity::Position;
    using PositionGroup = AppEntity::PositionGroup;
    using FLFilterMap = QMap<AppEntity::FLFilter, int32_t>;

    class MacroGenerator {
    public:
        MacroGenerator(const FLFilterMap& exFilters = FLFilterMap(),
                       const FLFilterMap& emFilters = FLFilterMap());
        ~MacroGenerator();

        auto SetImagingScenario(const ImagingScenario::Pointer& scenario)->void;

        auto GenerateSinglePointMacro(const uint32_t index, const Position& Position) const->MacroGroup::Pointer;
        auto GenerateMultiPointsMacro(const QList<PositionGroup>& positionGroups) const->MacroGroup::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
    