#include <MCUFactory.h>

#include "CommandReadAFSensorValue.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandReadAFSensorValue::Impl {
        CommandReadAFSensorValue* p{ nullptr };
        Response::Pointer resp{ nullptr };

        int32_t value{ 0 };

        explicit Impl(CommandReadAFSensorValue* p) : p{ p } {
            resp = std::make_shared<Response>();
        }
    };

    CommandReadAFSensorValue::CommandReadAFSensorValue() : Command("ReadAFSensorValue"), d{ new Impl(this) } {
    }

    CommandReadAFSensorValue::~CommandReadAFSensorValue() {
    }

    auto CommandReadAFSensorValue::Perform() -> bool {
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();
        d->resp->SetResult(false);

        if(!mcuControl->SetManualAF(true)) {
            d->resp->SetMessage("It fails to turn on AF laser");
            return false;
        }

        if(!mcuControl->ReadAFMValue(d->value)) {
            d->resp->SetMessage("It fails to read AF Sensor Value");
            return false;
        }

        if(!mcuControl->SetManualAF(false)) {
            d->resp->SetMessage("It fails to turn off AF laser");
            return false;
        }

        d->resp->SetResult(true);
        return true;
    }

    auto CommandReadAFSensorValue::GetResponse() -> Response::Pointer {
        return d->resp;
    }

    auto CommandReadAFSensorValue::GetValue() const -> int32_t {
        return d->value;
    }
}
