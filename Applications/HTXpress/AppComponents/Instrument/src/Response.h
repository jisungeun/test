#pragma once
#include <memory>
#include <QString>

namespace HTXpress::AppComponents::Instrument {

    class Response {
	public:
		typedef std::shared_ptr<Response> Pointer;

    public:
        Response();
        virtual ~Response();

        auto SetResult(bool result)->void;
        auto GetResult() const->bool;

        auto SetMessage(const QString& message)->void;
        auto GetMessage() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}