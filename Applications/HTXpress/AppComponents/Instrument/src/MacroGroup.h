#pragma once
#include <memory>

#include "Macro.h"

namespace HTXpress::AppComponents::Instrument {
    class MacroGroup {
    public:
        typedef std::shared_ptr<MacroGroup> Pointer;

    public:
        MacroGroup();
        MacroGroup(int32_t groupID);
        MacroGroup(const MacroGroup& other);
        ~MacroGroup();

        auto SetGroupID(int32_t id)->void;
        auto GetGroupID() const->int32_t;

        auto AppendMacro(Macro::Pointer& macro)->void;
        auto AppendMacroGroup(MacroGroup::Pointer& macroGroup)->void;

        auto GetMacroCount() const->int32_t;
        auto GetMacro(int32_t index) const->Macro::Pointer;

        auto Clear();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}