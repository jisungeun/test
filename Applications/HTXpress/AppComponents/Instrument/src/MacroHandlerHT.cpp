#include "MotionCommandRepo.h"
#include "Macro.h"

#include "MacroHandlerHT.h"

namespace HTXpress::AppComponents::Instrument {
    struct MacroHandlerHT::Impl {
        MotionCommandRepo::Pointer repo{ MotionCommandRepo::GetInstance() };

        auto CanHandle(const ImagingCondition::Pointer& cond)->bool;
    };

    auto MacroHandlerHT::Impl::CanHandle(const ImagingCondition::Pointer& cond) -> bool {
        return cond->CheckModality(AppEntity::Modality::HT);
    }

    MacroHandlerHT::MacroHandlerHT() : d{new Impl} {
    }

    MacroHandlerHT::~MacroHandlerHT() {
    }

    auto MacroHandlerHT::Handle(const ImagingCondition::Pointer& cond) const -> MacroGroup::Pointer {
        MacroGroup::Pointer macros;

        if(!d->CanHandle(cond)) {
            macros = MacroHandler::Handle(cond);
        } else {
            const auto* condHT = dynamic_cast<const AppEntity::ImagingConditionHT*>(cond.get());
            if(condHT == nullptr) return nullptr;

            auto startMacro = std::make_shared<Macro>();

            //TODO NA 조건에 따라서 다른 HTStartSequence를 호출해야함
            auto cmd = d->repo->GetCommand(MotionCommandName::HTStartSequence1);
            if(cmd == nullptr) return nullptr;
            startMacro->SetCommandID(cmd->GetCommandID());
            //TODO ptnSeqMacro->SetAutoFocusMode(...)
            macros->AppendMacro(startMacro);

            auto trigMacro = std::make_shared<Macro>();
            cmd = [=]()->MotionCommand::Command::Pointer {
                if(condHT->Is3D()) return d->repo->GetCommand(MotionCommandName::HT3DTrigger);
                return d->repo->GetCommand(MotionCommandName::HT2DTrigger);
            }();
            if(cmd == nullptr) return nullptr;
            trigMacro->SetCommandID(cmd->GetCommandID());
            macros->AppendMacro(trigMacro);

            auto stopMacro = std::make_shared<Macro>();
            cmd = d->repo->GetCommand(MotionCommandName::StopSequence);
            if(cmd == nullptr) return nullptr;
            stopMacro->SetCommandID(cmd->GetCommandID());
        }

        return macros;
    }
}