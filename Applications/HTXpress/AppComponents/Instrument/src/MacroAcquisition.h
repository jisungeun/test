#pragma once
#include <memory>
#include <QList>

#include <ImagingSequence.h>
#include <PositionGroup.h>

#include "InstrumentConfig.h"
#include "StreamingMacro.h"
#include "ImagingParameter.h"

namespace HTXpress::AppComponents::Instrument::MacroAcquisition {
    using ImagingSequence = AppEntity::ImagingSequence;
    using PositionGroup = AppEntity::PositionGroup;
    using MCUAxis = TC::MCUControl::Axis;

    auto Setup(const ImagingSequence::Pointer& sequence, 
               const QList<PositionGroup>& positions,
               const bool useAutoFocus,
               const Config& config,
               const QMap<MCUAxis,int32_t>& currentPositions,
               const AppEntity::WellIndex startingWellIndex,
               const double focusReadyMM,
               const double multiWellOffsetMM) -> QList<StreamingMacro::Pointer>;
}