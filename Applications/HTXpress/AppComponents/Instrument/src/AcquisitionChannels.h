#pragma once
#include <memory>
#include <QMap>
#include <QList>

#include <AppEntityDefines.h>

namespace HTXpress::AppComponents::Instrument {
    class AcquisitionChannels {
    public:
        using Pointer = std::shared_ptr<AcquisitionChannels>;

    protected:
        AcquisitionChannels();

    public:
        ~AcquisitionChannels();

        static auto GetInstance()->Pointer;
        static auto Set(const AppEntity::ImagingType imagingType,
                        const AppEntity::TriggerType trigger,
                        const int32_t ledChannel,
                        const QList<int32_t>& channels)->void;
        static auto Get(const AppEntity::ImagingType imagingType,
                        const AppEntity::TriggerType trigger, 
                        int32_t channel)->int32_t;
        static auto GetLedChannel(const AppEntity::ImagingType imagingType,
                                  const AppEntity::TriggerType trigger)->int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}