#include <QMap>
#include "MotionCommand.h"

namespace HTXpress::AppComponents::Instrument::MotionCommand {
    BETTER_ENUM(ParamField, int32_t,
                //--> EnablePatternSequence
                Enable,
                SequenceID,
                IntensityRed,
                IntensityGreen,
                IntensityBlue,
                Exposure,
                Interval,
                CameraType,
                //--> TriggerWithMotion
                Axis,
                MotionStart,
                MotionEnd,
                TriggerStart,
                TriggerEnd,
                //interval,
                PulseWidth,
                Speed,
                Acceleration,
                //--> TriggerOnly
                Count,
                //interval,
                //pulseWidth,
                //--> FilterWheel
                Position,
                //--> RunMacroMatrix
                CountX,
                CountY,
                DistanceX,
                DistanceY,
                MacroGroupID,
                ScanDirection
    );

    struct Command::Impl {
        MotionCommandName name{MotionCommandName::Invalid};
        int32_t commandID{ 0 };
    };

    Command::Command() : d{new Impl} {
    }

    Command::Command(MotionCommandName name) : d{new Impl} {
        d->name = name;
    }

    Command::Command(const Command& other) : d{new Impl} {
        d->name = other.d->name;
        d->commandID = other.d->commandID;
    }

    Command::~Command() {
    }

    auto Command::operator==(const Command& other) -> bool {
        if(d->name != other.d->name) return false;
        if(d->commandID != other.d->commandID) return false;
        return true;
    }

    auto Command::GetName() const -> MotionCommandName {
        return d->name;
    }

    auto Command::GetNameAsString() const -> QString {
        return d->name._to_string();
    }

    auto Command::HasSameName(const Command& other) const -> bool {
        return d->name == other.d->name;
    }

    auto Command::SetCommandID(int32_t id) -> void {
        d->commandID = id;
    }

    auto Command::GetCommandID() const -> int32_t {
        return d->commandID;
    }

    struct EnablePatternSequence::Impl {
        QMap<ParamField, int32_t> params {
            {ParamField::Enable, false},
            {ParamField::SequenceID, -1},
            {ParamField::IntensityRed, 0},
            {ParamField::IntensityGreen, 0},
            {ParamField::IntensityBlue, 0},
            {ParamField::Exposure, 0},
            {ParamField::Interval, 0},
            {ParamField::CameraType, 1}
        };
    };

    EnablePatternSequence::EnablePatternSequence() : Command(), d{new Impl} {
    }

    EnablePatternSequence::EnablePatternSequence(MotionCommandName type) : Command(type), d{new Impl} {
    }

    EnablePatternSequence::EnablePatternSequence(const EnablePatternSequence& other) : Command(other), d{new Impl} {
        d->params = other.d->params;
    }

    EnablePatternSequence::~EnablePatternSequence() {
    }

    auto EnablePatternSequence::operator==(const EnablePatternSequence& other) -> bool {
        if(d->params != other.d->params) return false;
        return Command::operator==(other);
    }

    auto EnablePatternSequence::SetEnable(bool enable) -> void {
        d->params[ParamField::Enable] = enable;
    }

    auto EnablePatternSequence::GetEnable() const -> bool {
        return d->params[ParamField::Enable];
    }

    auto EnablePatternSequence::SetSequenceID(int32_t seqID) -> void {
        d->params[ParamField::SequenceID] = seqID;
    }

    auto EnablePatternSequence::GetSequenceID() const -> int32_t {
        return d->params[ParamField::SequenceID];
    }

    auto EnablePatternSequence::SetIntensity(int32_t red, int32_t green, int32_t blue) -> void {
        d->params[ParamField::IntensityRed] = red;
        d->params[ParamField::IntensityGreen] = green;
        d->params[ParamField::IntensityBlue] = blue;
    }

    auto EnablePatternSequence::GetIntensityRed() const -> int32_t {
        return d->params[ParamField::IntensityRed];
    }

    auto EnablePatternSequence::GetIntensityGreen() const -> int32_t {
        return d->params[ParamField::IntensityGreen];
    }

    auto EnablePatternSequence::GetIntensityBlue() const -> int32_t {
        return d->params[ParamField::IntensityBlue];
    }

    auto EnablePatternSequence::SetExposure(int32_t exposure) -> void {
        d->params[ParamField::Exposure] = exposure;
    }

    auto EnablePatternSequence::GetExposure() const -> int32_t {
        return d->params[ParamField::Exposure];
    }

    auto EnablePatternSequence::SetInterval(int32_t interval) -> void {
        d->params[ParamField::Interval] = interval;
    }

    auto EnablePatternSequence::GetInterval() const -> int32_t {
        return d->params[ParamField::Interval];
    }

    auto EnablePatternSequence::SetCameraType(int32_t type) -> void {
        d->params[ParamField::CameraType] = type;
    }

    auto EnablePatternSequence::CameraType() const -> int32_t {
        return d->params[ParamField::CameraType];
    }

    auto EnablePatternSequence::GetCommandType() const -> int32_t {
        return 3;
    }

    auto EnablePatternSequence::GetCommandTypeAsString() const -> QString {
        return "EnablePatternSequence";
    }

    auto EnablePatternSequence::SetParameter(const QString& field, int32_t value) -> bool {
        d->params[ParamField::_from_string(field.toLatin1())] = value;
        return true;
    }

    auto EnablePatternSequence::GetNames() const -> QVector<QString> {
        auto keys = d->params.keys();
        auto keyStr = QVector<QString>(keys.length());

        std::transform(keys.begin(), keys.end(), keyStr.begin(),
                       [](ParamField field) { return field._to_string(); });

        return keyStr;
    }

    auto EnablePatternSequence::GetParameters() const -> QList<int32_t> {
        return d->params.values();
    }
    
    struct TriggerWithMotion::Impl {
        QMap<ParamField,int32_t> params{
            {ParamField::Axis, 0},
            {ParamField::MotionStart, 0},
            {ParamField::MotionEnd, 0},
            {ParamField::TriggerStart, 0},
            {ParamField::TriggerEnd, 0},
            {ParamField::Interval, 0},
            {ParamField::PulseWidth, 0},
            {ParamField::Speed, 0},
            {ParamField::Acceleration, 0}
        };
    };

    TriggerWithMotion::TriggerWithMotion() : Command(), d{new Impl} {
    }

    TriggerWithMotion::TriggerWithMotion(MotionCommandName type) : Command(type), d{new Impl} {
    }

    TriggerWithMotion::TriggerWithMotion(TriggerWithMotion& other) : Command(other), d{new Impl} {
        d->params = other.d->params;
    }

    TriggerWithMotion::~TriggerWithMotion() {
    }

    auto TriggerWithMotion::operator==(const TriggerWithMotion& other) -> bool {
        if(d->params != other.d->params) return false;
        return Command::operator==(other);
    }

    auto TriggerWithMotion::SetAxis(int32_t axis) -> void {
        d->params[ParamField::Axis] = axis;
    }

    auto TriggerWithMotion::GetAxis() const -> int32_t {
        return d->params[ParamField::Axis];
    }

    auto TriggerWithMotion::SetMotionRange(int32_t startPos, int32_t endPos) -> void {
        d->params[ParamField::MotionStart] = startPos;
        d->params[ParamField::MotionEnd] = endPos;
    }

    auto TriggerWithMotion::GetMotionRangeStart() const -> int32_t {
        return d->params[ParamField::MotionStart];
    }

    auto TriggerWithMotion::GetMotionRangeEnd() const -> int32_t {
        return d->params[ParamField::MotionEnd];
    }

    auto TriggerWithMotion::SetTriggerRange(int32_t startPos, int32_t endPos) -> void {
        d->params[ParamField::TriggerStart] = startPos;
        d->params[ParamField::TriggerEnd] = endPos;
    }

    auto TriggerWithMotion::GetTriggerRangeStart() const -> int32_t {
        return d->params[ParamField::TriggerStart];
    }

    auto TriggerWithMotion::GetTriggerRangeEnd() const -> int32_t {
        return d->params[ParamField::TriggerEnd];
    }

    auto TriggerWithMotion::SetTriggerInterval(int32_t interval) -> void {
        d->params[ParamField::Interval] = interval;
    }

    auto TriggerWithMotion::GetTriggerInterval() const -> int32_t {
        return d->params[ParamField::Interval];
    }

    auto TriggerWithMotion::SetTriggerWidth(int32_t width) -> void {
        d->params[ParamField::PulseWidth] = width;
    }

    auto TriggerWithMotion::GetTriggerWidth() const -> int32_t {
        return d->params[ParamField::PulseWidth];
    }

    auto TriggerWithMotion::SetMotion(int32_t speed, int32_t accel) -> void {
        d->params[ParamField::Speed] = speed;
        d->params[ParamField::Acceleration] = accel;
    }

    auto TriggerWithMotion::GetMotionSpeed() const -> int32_t {
        return d->params[ParamField::Speed];
    }

    auto TriggerWithMotion::GetMotionAcceleration() const -> int32_t {
        return d->params[ParamField::Acceleration];
    }

    auto TriggerWithMotion::GetCommandType() const -> int32_t {
        return 0;
    }

    auto TriggerWithMotion::GetCommandTypeAsString() const -> QString {
        return "TriggerWithMotion";
    }

    auto TriggerWithMotion::SetParameter(const QString& field, int32_t value) -> bool {
        d->params[ParamField::_from_string(field.toLatin1())] = value;
        return true;
    }

    auto TriggerWithMotion::GetNames() const -> QVector<QString> {
        auto keys = d->params.keys();
        auto keyStr = QVector<QString>(keys.length());

        std::transform(keys.begin(), keys.end(), keyStr.begin(),
                       [](ParamField field) { return field._to_string(); });

        return keyStr;
    }

    auto TriggerWithMotion::GetParameters() const -> QList<int32_t> {
        return d->params.values();
    }

    struct TriggerOnly::Impl {
        QMap<ParamField, int32_t> params{
            {ParamField::Count, 0},
            {ParamField::PulseWidth, 0},
            {ParamField::Interval, 0}
        };
    };

    TriggerOnly::TriggerOnly() : Command(), d{new Impl} {
    }

    TriggerOnly::TriggerOnly(MotionCommandName type) : Command(type), d{new Impl} {
    }

    TriggerOnly::TriggerOnly(const TriggerOnly& other) : Command(other), d{new Impl} {
        d->params = other.d->params;
    }

    TriggerOnly::~TriggerOnly() {
    }

    auto TriggerOnly::operator==(const TriggerOnly& other) -> bool {
        if(d->params != other.d->params) return false;
        return Command::operator==(other);
    }

    auto TriggerOnly::SetTriggerCount(int32_t count) -> void {
        d->params[ParamField::Count] = count;
    }

    auto TriggerOnly::GetTriggerCount() const -> int32_t {
        return d->params[ParamField::Count];
    }

    auto TriggerOnly::SetTriggerInterval(int32_t interval) -> void {
        d->params[ParamField::Interval] = interval;
    }

    auto TriggerOnly::GetTriggerInterval() const -> int32_t {
        return d->params[ParamField::Interval];
    }

    auto TriggerOnly::SetTriggerWidth(int32_t width) -> void {
        d->params[ParamField::PulseWidth] = width;
    }

    auto TriggerOnly::GetTriggerWidth() const -> int32_t {
        return d->params[ParamField::PulseWidth];
    }

    auto TriggerOnly::GetCommandType() const -> int32_t {
        return 2;
    }

    auto TriggerOnly::GetCommandTypeAsString() const -> QString {
        return "TriggerOnly";
    }

    auto TriggerOnly::SetParameter(const QString& field, int32_t value) -> bool {
        d->params[ParamField::_from_string(field.toLatin1())] = value;
        return true;
    }

    auto TriggerOnly::GetNames() const -> QVector<QString> {
        auto keys = d->params.keys();
        auto keyStr = QVector<QString>(keys.length());

        std::transform(keys.begin(), keys.end(), keyStr.begin(),
                       [](ParamField field) { return field._to_string(); });

        return keyStr;
    }

    auto TriggerOnly::GetParameters() const -> QList<int32_t> {
        return d->params.values();
    }

    struct FilterWheel::Impl {
        QMap<ParamField, int32_t> params{
            {ParamField::Position, 0}
        };
    };

    FilterWheel::FilterWheel() : Command(), d{new Impl} {
    }

    FilterWheel::FilterWheel(MotionCommandName type) : Command(type), d{new Impl} {
    }

    FilterWheel::FilterWheel(const FilterWheel& other) : Command(other), d{new Impl} {
        d->params = other.d->params;
    }

    FilterWheel::~FilterWheel() {
    }

    auto FilterWheel::operator==(const FilterWheel& other) -> bool {
        if(d->params != other.d->params) return false;
        return Command::operator==(other);
    }

    auto FilterWheel::SetFilter(int32_t position) -> void {
        d->params[ParamField::Position] = position;
    }

    auto FilterWheel::GetFilter() const -> int32_t {
        return d->params[ParamField::Position];
    }

    auto FilterWheel::GetCommandType() const -> int32_t {
        return 4;
    }

    auto FilterWheel::GetCommandTypeAsString() const -> QString {
        return "FilterWheel";
    }

    auto FilterWheel::SetParameter(const QString& field, int32_t value) -> bool {
        d->params[ParamField::_from_string(field.toLatin1())] = value;
        return true;
    }

    auto FilterWheel::GetNames() const -> QVector<QString> {
        auto keys = d->params.keys();
        auto keyStr = QVector<QString>(keys.length());

        std::transform(keys.begin(), keys.end(), keyStr.begin(),
                       [](ParamField field) { return field._to_string(); });

        return keyStr;
    }

    auto FilterWheel::GetParameters() const -> QList<int32_t> {
        return d->params.values();
    }

    struct RunMacroMatrix::Impl {
        QMap<ParamField, int32_t> params{
            {ParamField::CountX, 0},
            {ParamField::CountY, 0},
            {ParamField::DistanceX, 0},
            {ParamField::DistanceY, 0},
            {ParamField::MacroGroupID, 0},
            {ParamField::ScanDirection, 0}
        };
    };

    RunMacroMatrix::RunMacroMatrix() : Command(), d{new Impl} {
    }

    RunMacroMatrix::RunMacroMatrix(MotionCommandName type) : Command(type), d{new Impl} {
    }

    RunMacroMatrix::RunMacroMatrix(const RunMacroMatrix& other) : Command(other), d{new Impl} {
        d->params = other.d->params;
    }

    RunMacroMatrix::~RunMacroMatrix() {
    }

    auto RunMacroMatrix::operator==(const RunMacroMatrix& other) -> bool {
        if(d->params != other.d->params) return false;
        return Command::operator==(other);
    }

    auto RunMacroMatrix::SetCount(int32_t countX, int32_t countY) -> void {
        d->params[ParamField::CountX] = countX;
        d->params[ParamField::CountY] = countY;
    }

    auto RunMacroMatrix::GetCountX() const -> int32_t {
        return d->params[ParamField::CountX];
    }

    auto RunMacroMatrix::GetCountY() const -> int32_t {
        return d->params[ParamField::CountY];
    }

    auto RunMacroMatrix::SetDistance(int32_t distX, int32_t distY) -> void {
        d->params[ParamField::DistanceX] = distX;
        d->params[ParamField::DistanceY] = distY;
    }

    auto RunMacroMatrix::GetDistanceX() const -> int32_t {
        return d->params[ParamField::DistanceX];
    }

    auto RunMacroMatrix::GetDistanceY() const -> int32_t {
        return d->params[ParamField::DistanceY];
    }

    auto RunMacroMatrix::SetMacroGroupID(int32_t id) -> void {
        d->params[ParamField::MacroGroupID] = id;
    }

    auto RunMacroMatrix::GetMacroGroupID() const -> int32_t {
        return d->params[ParamField::MacroGroupID];
    }

    auto RunMacroMatrix::SetScanDirection(int32_t direction) -> void {
        d->params[ParamField::ScanDirection] = direction;
    }

    auto RunMacroMatrix::GetScanDirection() const -> int32_t {
        return d->params[ParamField::ScanDirection];
    }

    auto RunMacroMatrix::GetCommandType() const -> int32_t {
        return 5;
    }

    auto RunMacroMatrix::GetCommandTypeAsString() const -> QString {
        return "RunMacroMatrix";
    }

    auto RunMacroMatrix::SetParameter(const QString& field, int32_t value) -> bool {
        d->params[ParamField::_from_string(field.toLatin1())] = value;
        return true;
    }

    auto RunMacroMatrix::GetNames() const -> QVector<QString> {
        auto keys = d->params.keys();
        auto keyStr = QVector<QString>(keys.length());

        std::transform(keys.begin(), keys.end(), keyStr.begin(),
                       [](ParamField field) { return field._to_string(); });

        return keyStr;
    }

    auto RunMacroMatrix::GetParameters() const -> QList<int32_t> {
        return d->params.values();
    }
}
