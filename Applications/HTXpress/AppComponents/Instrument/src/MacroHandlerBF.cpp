#include "MotionCommandRepo.h"
#include "Macro.h"

#include "MacroHandlerBF.h"

namespace HTXpress::AppComponents::Instrument {
    struct MacroHandlerBF::Impl {
        MotionCommandRepo::Pointer repo{ MotionCommandRepo::GetInstance() };

        auto CanHandle(const ImagingCondition::Pointer& cond)->bool;
    };

    auto MacroHandlerBF::Impl::CanHandle(const ImagingCondition::Pointer& cond) -> bool {
        return cond->CheckModality(AppEntity::Modality::BF);
    }

    MacroHandlerBF::MacroHandlerBF() : d{new Impl} {
    }

    MacroHandlerBF::~MacroHandlerBF() {
    }

    auto MacroHandlerBF::Handle(const ImagingCondition::Pointer& cond) const -> MacroGroup::Pointer {
        MacroGroup::Pointer macros;

        if(!d->CanHandle(cond)) {
            macros = MacroHandler::Handle(cond);
        } else {
            const auto* condBF = dynamic_cast<const AppEntity::ImagingConditionBF*>(cond.get());
            if(condBF == nullptr) return nullptr;

            auto startMacro = std::make_shared<Macro>();

            auto cmd = [=]()->MotionCommand::Command::Pointer {
                if(condBF->GetColorMode()) return d->repo->GetCommand(MotionCommandName::ColorBFStartSequence);
                return d->repo->GetCommand(MotionCommandName::MonoBFStartSequence);
            }();
            if(cmd == nullptr) return nullptr;
            startMacro->SetCommandID(cmd->GetCommandID());
            //TODO ptnSeqMacro->SetAutoFocusMode(...)
            macros->AppendMacro(startMacro);

            auto trigMacro = std::make_shared<Macro>();
            cmd = [=]()->MotionCommand::Command::Pointer {
                if(condBF->GetColorMode()) return d->repo->GetCommand(MotionCommandName::ColorBFTrigger);
                return d->repo->GetCommand(MotionCommandName::MonoBFTrigger);
            }();
            if(cmd == nullptr) return nullptr;
            trigMacro->SetCommandID(cmd->GetCommandID());
            macros->AppendMacro(trigMacro);

            auto stopMacro = std::make_shared<Macro>();
            cmd = d->repo->GetCommand(MotionCommandName::StopSequence);
            if(cmd == nullptr) return nullptr;
            stopMacro->SetCommandID(cmd->GetCommandID());
        }

        return macros;
    }
}