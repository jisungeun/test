#include "MotionCommand.h"
#include "MacroLive.h"

namespace HTXpress::AppComponents::Instrument::MacroLiveWithTrigger {
    auto BuildPatternSequence(const ImagingParameter& param, bool enable)->QVector<int32_t> {
        QVector<int32_t> cmdParams(8);
        cmdParams[0] = enable;
        cmdParams[1] = param.SequenceId();
        cmdParams[2] = param.IntensityRed();
        cmdParams[3] = param.IntensityGreen();
        cmdParams[4] = param.IntensityBlue();
        cmdParams[5] = param.ExposureUSec();
        cmdParams[6] = param.IntervalUSec();
        cmdParams[7] = param.CameraType();
        return cmdParams;
    }
    
    auto BuildTriggerOnly(int32_t count, int32_t interval, int32_t pulseWidth) -> QVector<int32_t> {
        QVector<int32_t> cmdParams(3);

        cmdParams[0] = count;                         //Trigger Count
        cmdParams[1] = interval;                      //Trigger Interval (usec)
        cmdParams[2] = pulseWidth;                    //Trigger Pulse Width (usec)

        return cmdParams;
    }

    auto Start(const ImagingParameter& param, const QVector<int32_t>& pos,
               int32_t triggerCount, int32_t triggerInterval, int32_t pulseWidth) -> QList<StreamingMacro::Pointer> {
        QList<StreamingMacro::Pointer> macros;

        macros.push_back([=]()->StreamingMacro::Pointer {
            auto macro = std::make_shared<StreamingMacro>();
            macro->SetPosition(pos[0], pos[1], pos[2]);
            macro->SetAutoFoucs(false);
            macro->SetCommand(StreamingMacro::CommandType::ChangeFilter, {param.EmissionFilter()});
            return macro;
        }());

        macros.push_back([=]()->StreamingMacro::Pointer {
            auto macro = std::make_shared<StreamingMacro>();
            macro->SetPosition(pos[0], pos[1], pos[2]);
            macro->SetAutoFoucs(false);
            macro->SetCommand(StreamingMacro::CommandType::PatternSequence, BuildPatternSequence(param, true));
            return macro;
        }());
        
        macros.push_back([=]()->StreamingMacro::Pointer {
            auto macro = std::make_shared<StreamingMacro>();
            macro->SetPosition(pos[0], pos[1], pos[2]);
            macro->SetAutoFoucs(false);
            macro->SetCommand(StreamingMacro::CommandType::TriggerOnly, 
                              { BuildTriggerOnly(triggerCount, triggerInterval, pulseWidth) });
            macro->SetImageCount(triggerCount);
            return macro;
        }());

        return macros;
    }
}
