#pragma once
#include <memory>
#include <QMap>

#include <FLFilter.h>

namespace HTXpress::AppComponents::Instrument {
    class EmissionFilters {
    public:
        using Pointer = std::shared_ptr<EmissionFilters>;

    protected:
        EmissionFilters();

    public:
        ~EmissionFilters();

        static auto GetInstance()->Pointer;
        static auto Set(const int32_t channel, const AppEntity::FLFilter& filter)->void;
        static auto Set(const QMap<AppEntity::FLFilter, int32_t>& filters)->void;
        static auto Get(const AppEntity::FLFilter& filter) -> int32_t;
        static auto GetVoidChannel() -> int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}