#include "Image.h"

namespace HTXpress::AppComponents::Instrument {
    struct Image::Impl {
        int32_t sizeX{ 0 };
        int32_t sizeY{ 0 };
        std::shared_ptr<uint8_t[]> buffer;
        QDateTime timestamp;
    };

    Image::Image() : d{new Impl} {
    }

    Image::Image(int32_t sizeX, int32_t sizeY, const std::shared_ptr<uint8_t[]> buffer) : d{new Impl} {
        SetImage(sizeX, sizeY, buffer);
    }

    Image::~Image() {
    }

    auto Image::SetImage(int32_t sizeX, int32_t sizeY, const std::shared_ptr<uint8_t[]> buffer) -> void {
        d->sizeX = sizeX;
        d->sizeY = sizeY;
        d->buffer = buffer;
    }

    auto Image::GetSizeX() const -> int32_t {
        return d->sizeX;
    }

    auto Image::GetSizeY() const -> int32_t {
        return d->sizeY;
    }

    auto Image::GetBuffer() const -> std::shared_ptr<uint8_t[]> {
        return d->buffer;
    }

    auto Image::SetTimestamp(const QDateTime& timestamp) -> void {
        d->timestamp = timestamp;
    }

    auto Image::GetTimestamp() const -> QDateTime {
        return d->timestamp;
    }

}
