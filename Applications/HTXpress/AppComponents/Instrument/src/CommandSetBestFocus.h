#pragma once
#include <memory>

#include "InstrumentDefines.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandSetBestFocus : public Command {
    public:
        CommandSetBestFocus();
        explicit CommandSetBestFocus(int32_t value);
        ~CommandSetBestFocus() override;

        auto Perform() -> bool override;
        auto GetResponse() -> Response::Pointer override;
        auto GetNewTarget() const->int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}