#pragma once
#include <memory>

#include <QString>

namespace HTXpress::AppComponents::Instrument {
    class SimulationOptions {
    public:
        using Pointer = std::shared_ptr<SimulationOptions>;

    protected:
        SimulationOptions();

    public:
        ~SimulationOptions();

        static auto GetInstance()->Pointer;

        auto SetImagePath(const QString& path)->void;
        auto GetImagePath() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}