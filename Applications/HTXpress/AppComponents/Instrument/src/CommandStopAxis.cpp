#define LOGGER_TAG "[CommandStopAxis]"

#include <TCLogger.h>
#include "MCUFactory.h"
#include "CommandStopAxis.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandStopAxis::Impl {
        CommandStopAxis* p{ nullptr };
        Response::Pointer resp{ nullptr };

        explicit Impl(CommandStopAxis* p) : p(p) {
            resp = std::make_shared<Response>();
        }
    };

    CommandStopAxis::CommandStopAxis()
        : Command("StopAxis")
        , d{ std::make_unique<Impl>(this) }  {
    }

    CommandStopAxis::~CommandStopAxis() {
    }

    auto CommandStopAxis::Perform() -> bool {
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        if(!mcuControl->StopMotion()) {
            const auto error = mcuControl->GetLastError();
            d->resp->SetMessage(QString("It fails to stop motion [%1]").arg(error.GetText()));
            d->resp->SetResult(false);
            return false;
        }

        if(!WaitForMotionStop()) {
            QLOG_ERROR() << "Motion is not stopped";
            d->resp->SetResult(false);
            return false;
        }

        d->resp->SetResult(true);

        return true;
    }

    auto CommandStopAxis::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}
