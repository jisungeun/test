#define LOGGER_TAG "[CommandCheckMotion]"
#include <QVector>

#include <TCLogger.h>
#include <MCUFactory.h>

#include "ResponseMotion.h"
#include "CommandCheckMotion.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandCheckMotion::Impl {
        bool isMoving{ false };
        bool afDisabled{ false };
        ResponseMotion::Pointer resp{ new ResponseMotion() };
    };

    CommandCheckMotion::CommandCheckMotion() : Command("CheckMotion"), d{new Impl} {
    }

    CommandCheckMotion::~CommandCheckMotion() {
    }

    auto CommandCheckMotion::SetAFDisabled() -> void {
        d->afDisabled = true;
    }

    auto CommandCheckMotion::Perform() -> bool {
        using Axis = TC::MCUControl::Axis;
        using Flag = TC::MCUControl::Flag;
        using MCUResponse = TC::MCUControl::MCUResponse;
        using ResponseCode = TC::MCUControl::Response;
        using MCUState = TC::MCUControl::MCUState;

        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        MCUResponse mcuResponse;
        if(!mcuControl->CheckStatus(mcuResponse)) {
            auto error = mcuControl->GetLastError();
            d->resp->SetMessage(QString("It fails to check MCU status [%1]").arg(error.GetText()));
            d->resp->SetResult(false);
            return false;
        }

        d->resp->SetMoving(false);
        d->resp->SetResult(true);

        const auto errorState = (mcuResponse.GetValue(ResponseCode::StateMachineId) == static_cast<int32_t>(+MCUState::Error));
        if(errorState) {
            auto error = mcuControl->GetLastError();
            d->resp->SetMessage(QString("MCU is in error state [%1]").arg(error.GetText()));
            d->resp->SetResult(false);
            return false;
        }

        QVector<ResponseCode> codes{ ResponseCode::AxisXMoving, ResponseCode::AxisYMoving, ResponseCode::AxisZMoving,
                                     ResponseCode::AxisUMoving, ResponseCode::AxisVMoving, ResponseCode::AxisWMoving };
        for(auto code : codes) {
            if(mcuResponse.GetValueAsFlag(code)) {
                d->resp->SetMoving(true);
                return true;
            }
        }

        if(mcuResponse.GetValue(ResponseCode::AfOn) == 1) {
            d->resp->SetMoving(true);
            return true;
        }

        //Check AF status only when AF is enabled...
        if(!d->afDisabled) {
            int32_t afStatus, afResult, afTrialCount;
            if(!mcuControl->CheckAFStatus(afStatus, afResult, afTrialCount)) {
                auto error = mcuControl->GetLastError();
                d->resp->SetMessage("It fails to check autofocus status");
                d->resp->SetResult(false);
                return false;
            }

            if(afResult==-1) {
                d->resp->SetAFFailed(true);
            }

            QList<int32_t> adcDiff, zComp;
            if(mcuControl->ReadAFLog(adcDiff, zComp)) {
                QStringList strAdcDiff, strZComp;
                std::for_each(adcDiff.begin(), adcDiff.end(), [&strAdcDiff](int32_t value) {
                    strAdcDiff.push_back(QString::number(value));
                });
                std::for_each(zComp.begin(), zComp.end(), [&strZComp](int32_t value) {
                    strZComp.push_back(QString::number(value));
                });

                QLOG_INFO() << "AF Log - ADC: " << strAdcDiff.join(',') << " ZComp:" << strZComp.join(',');
            }
        }

        return true;
    }

    auto CommandCheckMotion::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}