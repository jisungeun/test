#pragma once
#include <memory>

#include "InstrumentDefines.h"
#include "InstrumentConfig.h"

namespace HTXpress::AppComponents::Instrument {
    class PositionAdjuster {
    public:
        using Pointer = std::shared_ptr<PositionAdjuster>;

    protected:
        PositionAdjuster();

    public:
        ~PositionAdjuster();

        static auto GetInstance()->Pointer;

        auto SetConfig(const Config& config)->void;
        auto Adjust(const Axis axis, int32_t currentPulses, int32_t targetPulses) const->int32_t;
        auto AdjustPR(const Axis axis, int32_t currentPulses, RawPosition targetMM) const->int32_t;
        auto AdjustRR(const Axis axis, RawPosition currentMM, RawPosition targetMM) const->int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}