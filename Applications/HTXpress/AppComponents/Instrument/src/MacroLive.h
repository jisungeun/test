#pragma once
#include <memory>
#include <QVector>
#include <QList>

#include "StreamingMacro.h"
#include "ImagingParameter.h"

namespace HTXpress::AppComponents::Instrument::MacroLive {
    auto Start(const ImagingParameter& param, const QVector<int32_t>& pos, int32_t afMode)->QList<StreamingMacro::Pointer>;
    auto Stop(const ImagingParameter& param, const QVector<int32_t>& pos, int32_t afMode)->QList<StreamingMacro::Pointer>;
    auto Stop()->QList<StreamingMacro::Pointer>;
}