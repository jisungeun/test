#pragma once
#include <memory>
#include <QThread>

#include <IImageSink.h>

#include "Image.h"
#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument {
	class ImagePort;

	class HTXInstrument_API ImageSink : public QThread, public TC::CameraControl::IImageSink {
		Q_OBJECT

	public:
		using Pointer = std::shared_ptr<ImageSink>;
		using RawImage = TC::CameraControl::Image;

	public:
        explicit ImageSink(const QString& title = QString());
		~ImageSink();

		auto Send(const RawImage::Pointer rawImage)->bool override;
		auto Clear() -> void override;

		auto RemainCount() const->int32_t override;

	protected:
		auto Register(ImagePort* port)->void;
		auto Deregister(ImagePort* port)->void;

		void run() override;

	private:
		friend class CameraManager;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}