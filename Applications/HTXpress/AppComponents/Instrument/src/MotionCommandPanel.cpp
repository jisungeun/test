#include <QMap>
#include <QLineEdit>
#include <QItemDelegate>

#include <MotionCommandFactory.h>

#include "MotionCommandPanelControl.h"
#include "ui_MotionCommandPanel.h"
#include "MotionCommandPanel.h"

namespace HTXpress::AppComponents::Instrument {
    class IntegerDelegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/,
                              const QModelIndex& /*index*/) const override {
            QLineEdit* lineEdit = new QLineEdit(parent);
            QIntValidator* validator = new QIntValidator(lineEdit);
            lineEdit->setValidator(validator);
            return lineEdit;
        }
    };

    struct MotionCommandPanel::Impl {
        Ui::MotionCommandPanel ui;
        MotionCommandPanelControl control;
        int32_t selectedRow{ -1 };

        auto SetEditMode(bool bEdit)->void;
    };

    auto MotionCommandPanel::Impl::SetEditMode(bool bEdit) -> void {
        ui.restoreBtn->setEnabled(bEdit);
        ui.applyBtn->setEnabled(bEdit);
        ui.editBtn->setEnabled(!bEdit);
        ui.listTable->setDisabled(bEdit);

        const auto rows = ui.commandTable->rowCount();
        for(int32_t idx=0; idx<rows; ++idx) {
            auto* item = ui.commandTable->item(idx, 1);
            if(item==nullptr) continue;

            if(bEdit) {
                item->setFlags(item->flags() | Qt::ItemIsEditable);
                continue;
            }

            item->setFlags(item->flags() & ~QFlags(Qt::ItemIsEditable));
        }
    }

    MotionCommandPanel::MotionCommandPanel(QWidget* parent) : QWidget(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->ui.listTable->setColumnCount(3);
        d->ui.listTable->setRowCount(static_cast<int32_t>(MotionCommandName::_size()) - 1); //ignore the last one

        d->ui.listTable->setHorizontalHeaderLabels({"Index", "Command Title", "Command Type"});
        d->ui.listTable->setColumnWidth(0, 50);
        d->ui.listTable->setColumnWidth(1, 200);
        d->ui.listTable->horizontalHeader()->setStretchLastSection(true);
        d->ui.listTable->verticalHeader()->setVisible(false);
        d->ui.listTable->setItemDelegateForColumn(0, new IntegerDelegate());

        d->ui.commandTable->setColumnCount(2);
        d->ui.commandTable->setRowCount(12);

        d->ui.commandTable->setHorizontalHeaderLabels({"Title", "Value"});
        d->ui.commandTable->setColumnWidth(1, 50);
        d->ui.commandTable->horizontalHeader()->setStretchLastSection(true);
        d->ui.commandTable->verticalHeader()->setVisible(false);
        d->ui.commandTable->setItemDelegateForColumn(1, new IntegerDelegate());

        d->ui.restoreBtn->setDisabled(true);
        d->ui.applyBtn->setDisabled(true);
        d->ui.editBtn->setDisabled(true);

        connect(d->ui.listTable, SIGNAL(cellClicked(int,int)), this, SLOT(onCommandSeleteced(int,int)));
        connect(d->ui.listTable, SIGNAL(cellChanged(int,int)), this, SLOT(onCommandIndexChanged(int,int)));
        connect(d->ui.editBtn, SIGNAL(clicked()), this, SLOT(onEnableEditCommand()));
        connect(d->ui.restoreBtn, SIGNAL(clicked()), this, SLOT(onRestoreCommand()));
        connect(d->ui.applyBtn, SIGNAL(clicked()), this, SLOT(onApplyCommandChanges()));
    }

    MotionCommandPanel::~MotionCommandPanel() {
    }

    auto MotionCommandPanel::SetCommand(MotionCommandName name, MotionCommand::Command::Pointer cmd) -> void {
        d->control.SetCommand(name, cmd);

        const auto rowIdx = static_cast<int32_t>(name._to_index());

        d->ui.listTable->blockSignals(true);

        auto* item = new QTableWidgetItem(QString::number(cmd->GetCommandID()));
        item->setTextAlignment(Qt::AlignHCenter);
        d->ui.listTable->setItem(rowIdx, 0, item);

        item = new QTableWidgetItem(QString::fromStdString(name._to_string()));
        item->setFlags(item->flags() | Qt::ItemIsEditable);
        d->ui.listTable->setItem(rowIdx, 1, item);

        item = new QTableWidgetItem(cmd->GetCommandTypeAsString());
        item->setFlags(item->flags() | Qt::ItemIsEditable);
        d->ui.listTable->setItem(rowIdx, 2, item);

        d->ui.listTable->blockSignals(false);
    }

    auto MotionCommandPanel::GetCommand(MotionCommandName name) const -> MotionCommand::Command::Pointer {
        return d->control.GetCommand(name);
    }

    auto MotionCommandPanel::GetCommands() const -> QList<MotionCommand::Command::Pointer> {
        return d->control.GetCommands();
    }

    auto MotionCommandPanel::IsModified() const -> bool {
        return d->control.IsModified();
    }

    auto MotionCommandPanel::GetModifiedList() const -> QList<MotionCommandName> {
        return d->control.GetModifiedList();
    }

    auto MotionCommandPanel::Clear() -> void {
        d->ui.listTable->clearSelection();
        d->ui.listTable->clearContents();
        d->ui.commandTable->clearSelection();
        d->ui.commandTable->clearContents();
    }

    void MotionCommandPanel::onCommandSeleteced(int row, int /*col*/) {
        d->selectedRow = row;

        auto cmd = d->control.GetCommandByIndex(row);

        d->ui.commandTable->clearContents();
        const auto names = cmd->GetNames();
        const auto values = cmd->GetParameters();

        for(int32_t idx=0; idx<names.length(); idx++) {
            const auto name = names.at(idx);
            const auto value = values.at(idx);

            auto* item = new QTableWidgetItem(name);
            item->setFlags(item->flags() & ~QFlags(Qt::ItemIsEditable));
            d->ui.commandTable->setItem(idx, 0, item);

            item = new QTableWidgetItem(QString::number(value));
            item->setFlags(item->flags() & ~QFlags(Qt::ItemIsEditable));
            item->setTextAlignment(Qt::AlignRight);
            d->ui.commandTable->setItem(idx, 1, item);
        }

        d->SetEditMode(false);
    }

    void MotionCommandPanel::onCommandIndexChanged(int row, int col) {
        if(col != 0) return;

        auto* item = d->ui.listTable->item(row, 0);

        auto cmd = d->control.GetCommandByIndex(row);
        cmd->SetCommandID(item->text().toInt());
    }

    void MotionCommandPanel::onEnableEditCommand() {
        d->SetEditMode(true);
    }

    void MotionCommandPanel::onRestoreCommand() {
        d->SetEditMode(false);
        onCommandSeleteced(d->selectedRow, 0);
    }

    void MotionCommandPanel::onApplyCommandChanges() {
        d->SetEditMode(false);

        auto cmd = d->control.GetCommandByIndex(d->selectedRow);

        const auto rows = d->ui.commandTable->rowCount();
        for(int32_t idx=0; idx<rows; ++idx) {
            auto* item = d->ui.commandTable->item(idx, 0);
            if(item==nullptr) continue;

            const auto field = item->text();
            if(field.isEmpty()) continue;

            item = d->ui.commandTable->item(idx, 1);
            const auto value = item->text().toInt();

            cmd->SetParameter(field, value);
        }
    }
}
