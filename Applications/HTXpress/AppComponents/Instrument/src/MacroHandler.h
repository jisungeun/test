#pragma once
#include <memory>

#include <ImagingCondition.h>
#include "MacroGroup.h"

namespace HTXpress::AppComponents::Instrument {
    using ImagingCondition = AppEntity::ImagingCondition;

    class MacroHandler {
    public:
        typedef std::shared_ptr<MacroHandler> Pointer;

    public:
        MacroHandler();
        virtual ~MacroHandler();

        auto SetNext(Pointer handler)->void;
        virtual auto Handle(const ImagingCondition::Pointer& cond) const->MacroGroup::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}