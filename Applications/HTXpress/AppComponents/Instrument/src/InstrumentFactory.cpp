#include "InstrumentSimulator.h"
#include "InstrumentHTX.h"
#include "InstrumentFactory.h"

namespace HTXpress::AppComponents::Instrument {
    auto InstrumentFactory::GetInstance(InstrumentType type) -> Instrument::Pointer {
        static Instrument::Pointer theInstance = [type]()->Instrument::Pointer {
            Instrument::Pointer instance{ nullptr };
            switch(type) {
            case InstrumentType::Simulator:
                instance.reset(new InstrumentSimulator());
                break;
            case InstrumentType::HTX:
                instance.reset(new InstrumentHTX());
                break;
            default:
                break;
            }
            return instance;
        }();
        return theInstance;
    }
}