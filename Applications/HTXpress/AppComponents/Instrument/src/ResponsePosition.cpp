#include <QMap>
#include "ResponsePosition.h"

namespace HTXpress::AppComponents::Instrument {
    struct ResponsePosition::Impl {
        QMap<Axis,int32_t> pos;
        bool error{ false };
    };

    ResponsePosition::ResponsePosition() : d{new Impl} {
    }

    ResponsePosition::~ResponsePosition() {
    }

    auto ResponsePosition::SetPosition(Axis axis, int32_t value) -> void {
        d->pos[axis] = value;
    }

    auto ResponsePosition::GetPosition(Axis axis) const -> int32_t {
        return d->pos[axis];
    }

    auto ResponsePosition::SetError() -> void {
        d->error = true;
    }

    auto ResponsePosition::GetError() const -> bool {
        return d->error;
    }
}
