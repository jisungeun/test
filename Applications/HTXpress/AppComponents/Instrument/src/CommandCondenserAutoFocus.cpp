#define LOGGER_TAG "[CommandCondenserAutoFocus]"
#include <TCLogger.h>

#include <MCUFactory.h>

#include "CameraManager.h"
#include "AcquisitionChannels.h"
#include "MacroCondenserAutofocus.h"
#include "CommandCondenserAutoFocus.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandCondenserAutoFocus::Impl {
        CommandCondenserAutoFocus* p{ nullptr };
        Response::Pointer resp{ nullptr };
        CondenserAutofocusProgress::Pointer progress;
        Config config;
        int32_t patternIndex;
        int32_t intensity;
        QList<RawPosition> positions;

        explicit Impl(CommandCondenserAutoFocus* p) : p{ p } {
            resp = std::make_shared<Response>();
        }

        auto CountImages(const QList<StreamingMacro::Pointer>& macros)->int32_t;
        auto RunMacro(const QList<StreamingMacro::Pointer>& macros)->bool;
        auto SetError(const QString& message)->void;
    };

    auto CommandCondenserAutoFocus::Impl::CountImages(const QList<StreamingMacro::Pointer>& macros) -> int32_t {
        int32_t images = 0;

        for(auto macro : macros) {
            images += macro->GetImageCount();
        }

        return images;
    }

    auto CommandCondenserAutoFocus::Impl::RunMacro(const QList<StreamingMacro::Pointer>& macros) -> bool {
        using ImagingType = AppEntity::ImagingType;
        using TriggerType = AppEntity::TriggerType;

        auto camera = CameraManager::GetInstance()->GetCondenserCamera();
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        int32_t startPulse{ 0 };
        int32_t endPulse{ 0 };
        int32_t idx = 0;
        for(auto macro : macros) {
#if 1
            QLOG_INFO() << "[" << ++idx << "]" << macro->Str();
#endif
            if(macro->GetCommandType() == +TC::MCUControl::MotionCommandType::TriggerWithStop) {
                startPulse = macro->GetParameter(1);
                endPulse = macro->GetParameter(2);
            }
        }
        camera->StopAcquisition();

        if(!p->WaitForMotionStop()) {
            QLOG_ERROR() << "Motion is not stopped";
            return false;
        }

        mcuControl->SetLEDChannel(config.GetCondenserAFLightChannel());

        camera->StartAcquisition(CountImages(macros));

        if(!mcuControl->StartMacroStreaming()) {
            QLOG_ERROR() << "It fails to start macro streaming";
            return false;
        }

        const auto macroCount = macros.length();
        int32_t macroIndex = 0;

        TC::MCUControl::MCUResponse status;
        while(mcuControl->CheckStatus(status)) {
            const auto completed = status.GetValue(TC::MCUControl::Response::MacroStreamingIndex);
            const auto halfEmpty = status.GetValue(TC::MCUControl::Response::MacroStreamingHalfEmpty);
            const auto error = status.GetValue(TC::MCUControl::Response::MacroStreamingError);

            if(error == 1) {
                QLOG_ERROR() << "Error is occurred during streaming";
                return false;
            }

            if(completed == macroCount) {
                QLOG_INFO() << "All macros are completed";
                break;
            }

            if(halfEmpty && (macroIndex < macroCount)) {
                QLOG_INFO() << "Macro completed:" << completed << " sent:" << macroIndex << " total:" << macroCount;
                const auto lastIndex = std::min(macroIndex+200, macroCount);
                for(; macroIndex<lastIndex; macroIndex++) {
                    auto macro = macros.at(macroIndex);
                    if(!mcuControl->SendMacrosToStreamBuffer({QList<int32_t>::fromVector(macro->GetPacket())})) {
                        QLOG_ERROR() << "It fails to send streaming packets";

                        mcuControl->StopMacroStreaming();
                        return false;
                    }
                }
            }

            const auto cAxisPos = status.GetValue(TC::MCUControl::Response::AxisUPosition);
            progress->SetProgress((std::max(cAxisPos,startPulse) - startPulse) * 1.0 / (endPulse - startPulse));

            QThread::msleep(500);
        }

        if(!mcuControl->StopMacroStreaming()) {
            QLOG_ERROR() << "It fails to stop macro streaming";
            return false;
        }

        mcuControl->SetLEDChannel(AcquisitionChannels::GetLedChannel(ImagingType::BFGray, TriggerType::Trigger));

        return true;
    }

    auto CommandCondenserAutoFocus::Impl::SetError(const QString& message) -> void {
        resp->SetMessage(message);
        progress->SetError(message);
    }

    CommandCondenserAutoFocus::CommandCondenserAutoFocus(CondenserAutofocusProgress::Pointer progress) : Command("CondenserAutoFocus"), d{new Impl(this) } {
        d->progress = progress;
    }

    CommandCondenserAutoFocus::~CommandCondenserAutoFocus() {
    }

    auto CommandCondenserAutoFocus::SetConfig(const Config& config) -> void {
        d->config = config;
    }

    auto CommandCondenserAutoFocus::SetPatternIndex(int32_t pattenIndex) -> void {
        d->patternIndex = pattenIndex;
    }

    auto CommandCondenserAutoFocus::SetIntensity(int32_t intensity) -> void {
        d->intensity = intensity;
    }

    auto CommandCondenserAutoFocus::SetPositions(const QList<RawPosition>& positions) -> void {
        d->positions = positions;
    }

    auto CommandCondenserAutoFocus::Perform() -> bool {
        d->resp->SetResult(false);
        d->progress->SetProgress(0.0);

        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        auto macros = MacroCondenserAutofocus::Setup(d->positions, d->patternIndex, d->intensity, d->config);
        if(!d->RunMacro(macros)) {
            d->SetError("It fails to run condenser autofocus macro");
            return false;
        }

        d->progress->SetProgress(1.0);
        d->resp->SetResult(true);
        return true;
    }

    auto CommandCondenserAutoFocus::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}
