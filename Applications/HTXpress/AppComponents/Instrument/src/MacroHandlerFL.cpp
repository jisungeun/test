#include "MotionCommandRepo.h"
#include "Macro.h"

#include "MacroHandlerFL.h"

namespace HTXpress::AppComponents::Instrument {
    struct MacroHandlerFL::Impl {
        MotionCommandRepo::Pointer repo{ MotionCommandRepo::GetInstance() };
        QMap<AppEntity::FLFilter, int32_t> exFilters;
        QMap<AppEntity::FLFilter, int32_t> emFilters;

        auto CanHandle(const ImagingCondition::Pointer& cond)->bool;
        auto GetEmissionChannel(const AppEntity::FLFilter& filter)->int32_t;
    };

    auto MacroHandlerFL::Impl::CanHandle(const ImagingCondition::Pointer& cond) -> bool {
        return cond->CheckModality(AppEntity::Modality::FL);
    }

    auto MacroHandlerFL::Impl::GetEmissionChannel(const AppEntity::FLFilter& filter) -> int32_t {
        const auto itr = emFilters.find(filter);
        if(itr == emFilters.end()) return -1;
        return itr.value();
    }

    MacroHandlerFL::MacroHandlerFL(const FLFilterMap& exFilters, const FLFilterMap& emFilters) : d{new Impl} {
        d->exFilters = exFilters;
        d->emFilters = emFilters;
    }

    MacroHandlerFL::~MacroHandlerFL() {
    }

    auto MacroHandlerFL::Handle(const ImagingCondition::Pointer& cond) const -> MacroGroup::Pointer {
        MacroGroup::Pointer macros;

        if(!d->CanHandle(cond)) {
            macros = MacroHandler::Handle(cond);
        } else {
            const auto* condFL = dynamic_cast<const AppEntity::ImagingConditionFL*>(cond.get());
            if(condFL == nullptr) return nullptr;

            auto channels = condFL->GetChannels();
            for(auto channel : channels) {
                auto startMacro = std::make_shared<Macro>();

                //TODO NA 조건에 따라서 다른 HTStartSequence를 호출해야함
                auto cmd = [=]()->MotionCommand::Command::Pointer {
                    MotionCommand::Command::Pointer cmd;
                    switch(channel) {
                    case 0:
                        cmd = d->repo->GetCommand(MotionCommandName::FLCH0StartSequence);
                        break;
                    case 1:
                        cmd = d->repo->GetCommand(MotionCommandName::FLCH1StartSequence);
                        break;
                    case 2:
                        cmd = d->repo->GetCommand(MotionCommandName::FLCH2StartSequence);
                        break;
                    }
                    return cmd;
                }();
                if(cmd == nullptr) return nullptr;
                startMacro->SetCommandID(cmd->GetCommandID());
                //TODO ptnSeqMacro->SetAutoFocusMode(...)
                macros->AppendMacro(startMacro);

                auto filterMacro = std::make_shared<Macro>();
                cmd = [=]()->MotionCommand::Command::Pointer {
                    MotionCommand::Command::Pointer cmd{ nullptr };
                    switch(d->GetEmissionChannel(condFL->GetEmFilter(channel))) {
                    case 0:
                        cmd = d->repo->GetCommand(MotionCommandName::ChangeFilter1);
                        break;
                    case 1:
                        cmd = d->repo->GetCommand(MotionCommandName::ChangeFilter2);
                        break;
                    case 2:
                        cmd = d->repo->GetCommand(MotionCommandName::ChangeFilter3);
                        break;
                    case 3:
                        cmd = d->repo->GetCommand(MotionCommandName::ChangeFilter4);
                        break;
                     case 4:
                        cmd = d->repo->GetCommand(MotionCommandName::ChangeFilter5);
                        break;
                    case 5:
                        cmd = d->repo->GetCommand(MotionCommandName::ChangeFilter6);
                        break;
                    }
                    return cmd;
                }();
                if(cmd == nullptr) return nullptr;
                filterMacro->SetCommandID(cmd->GetCommandID());
                macros->AppendMacro(filterMacro);

                auto trigMacro = std::make_shared<Macro>();
                cmd = [=]()->MotionCommand::Command::Pointer {
                    if(condFL->Is3D()) return d->repo->GetCommand(MotionCommandName::FL3DTrigger);
                    return d->repo->GetCommand(MotionCommandName::FL2DTrigger);
                }();
                if(cmd == nullptr) return nullptr;
                trigMacro->SetCommandID(cmd->GetCommandID());
                macros->AppendMacro(trigMacro);

                auto stopMacro = std::make_shared<Macro>();
                cmd = d->repo->GetCommand(MotionCommandName::StopSequence);
                if(cmd == nullptr) return nullptr;
                stopMacro->SetCommandID(cmd->GetCommandID());
                macros->AppendMacro(stopMacro);
            }
        }

        return macros;
    }
}