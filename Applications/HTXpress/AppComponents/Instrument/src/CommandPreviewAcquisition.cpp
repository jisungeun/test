#define LOGGER_TAG "[CommandPreviewAcquisition]"

#include <TCLogger.h>
#include <MCUFactory.h>
#include <QSettings>

#include "CameraManager.h"
#include "AcquisitionChannels.h"
#include "MacroPreviewAcquisition.h"
#include "CommandPreviewAcquisition.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandPreviewAcquisition::Impl {
        CommandPreviewAcquisition* p{ nullptr };
        Config config;
        AcquisitionProgress::Pointer progress{ nullptr };
        Response::Pointer resp{ nullptr };

        QList<UnitMotion> motions;
        double previewGain{ 0 };
        double origianlGain{ 0 };

        Impl(CommandPreviewAcquisition* p) : p{ p } {}
        ~Impl() {
            RestoreGainMode();
        }
        
        auto ToPulse(const Axis axis, const RawPosition positionMM) const -> int32_t;
        auto ToMM(Axis axis, int32_t pulses) -> RawPosition;
        auto GetPosition()->QMap<Axis, int32_t>;
        auto GetPosition(TC::MCUControl::MCUResponse& status)->AppEntity::Position;
        auto CountImages(const QList<StreamingMacro::Pointer>& macros)->int32_t;
        auto RunMacro(const QList<StreamingMacro::Pointer>& macros)->bool;
        auto SetError(const QString& message)->void;

        auto SetHighGainMode()->void;
        auto RestoreGainMode()->void;
    };

    auto CommandPreviewAcquisition::Impl::ToPulse(const Axis axis, const RawPosition positionMM) const -> int32_t {
        const auto resolution = config.AxisResolutionPPM(axis);
        return static_cast<int32_t>(resolution * positionMM);
    }

    auto CommandPreviewAcquisition::Impl::ToMM(Axis axis, int32_t pulses) -> RawPosition {
        auto resolution = config.AxisResolutionPPM(axis);
        return static_cast<RawPosition>(pulses) / resolution;
    }

    auto CommandPreviewAcquisition::Impl::GetPosition() -> QMap<Axis, int32_t> {
        using MCUAxis = TC::MCUControl::Axis;

        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();
        const auto [success, mcuPositions] = mcuControl->GetPositions();

        QMap<Axis, int32_t> positions;
        positions[Axis::AxisX] = mcuPositions[MCUAxis::X];
        positions[Axis::AxisY] = mcuPositions[MCUAxis::Y];
        positions[Axis::AxisZ] = mcuPositions[MCUAxis::Z];

        return positions;
    }

    auto CommandPreviewAcquisition::Impl::GetPosition(TC::MCUControl::MCUResponse& status) -> AppEntity::Position {
        const auto xPosMM = ToMM(Axis::AxisX, status.GetValue(TC::MCUControl::Response::AxisXPosition));
        const auto yPosMM = ToMM(Axis::AxisY, status.GetValue(TC::MCUControl::Response::AxisYPosition));
        const auto zPosMM = ToMM(Axis::AxisZ, status.GetValue(TC::MCUControl::Response::AxisZPosition));
        return AppEntity::Position::fromMM(xPosMM, yPosMM, zPosMM);
    }

    auto CommandPreviewAcquisition::Impl::CountImages(const QList<StreamingMacro::Pointer>& macros) -> int32_t {
        int32_t images = 0;

        for(const auto macro : macros) {
            images += macro->GetImageCount();
        }

        return images;
    }

    auto CommandPreviewAcquisition::Impl::RunMacro(const QList<StreamingMacro::Pointer>& macros) -> bool {
        using ImagingType = AppEntity::ImagingType;
        using TriggerType = AppEntity::TriggerType;

        const auto profilingLog = QSettings().value("Misc/Preview_Profiling_Log", false).toBool();

        auto camera = CameraManager::GetInstance()->GetImagingCamera();
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

#if 1
        int32_t idx = 0;
        for(auto macro : macros) {
            QLOG_INFO() << "[" << ++idx << "]" << macro->Str();
        }
#endif
        SetHighGainMode();

        camera->StopAcquisition();

        if(!p->WaitForMotionStop()) {
            QLOG_ERROR() << "Motion is not stopped";
            progress->SetError("Motion is not stopped");
            return false;
        }

        mcuControl->SetLEDChannel(AcquisitionChannels::GetLedChannel(ImagingType::FL3D, TriggerType::Trigger));
        camera->StartAcquisition(CountImages(macros));

        if(!mcuControl->StartMacroStreaming()) {
            QLOG_ERROR() << "It fails to start macro streaming";
            progress->SetError("It fails to start macro streaming");
            return false;
        }

        const auto macroCount = macros.length();
        int32_t macroIndex = 0;

        TC::MCUControl::MCUResponse status;
        while(mcuControl->CheckStatus(status)) {
            const auto completed = status.GetValue(TC::MCUControl::Response::MacroStreamingIndex);
            const auto halfEmpty = status.GetValue(TC::MCUControl::Response::MacroStreamingHalfEmpty);
            const auto error = status.GetValue(TC::MCUControl::Response::MacroStreamingError);

            progress->SetProgress((completed*1.0)/macroCount);
            progress->SetPosition(GetPosition(status));

            if(profilingLog) {
                const auto pos = GetPosition(status).toMM();
                QLOG_INFO() << "completed=" << completed << " progress=" << (completed*1.0)/macroCount
                            << " position=[" << pos.x << "," << pos.y << "," << pos.z << "]";
            }

            if(error == 1) {
                QLOG_ERROR() << "Error is occurred during streaming";
                progress->SetError("Error is occurred during streaming");
                return false;
            }

            if(completed == macroCount) {
                QLOG_INFO() << "All macros are completed";
                break;
            }

            if(progress->IsStopRequested()) {
                progress->SetStopped();
                QLOG_INFO() << "Stop acquisition is requested";
                break;
            }

            if(halfEmpty && (macroIndex < macroCount)) {
                QLOG_INFO() << "Macro completed:" << completed << " sent:" << macroIndex << " total:" << macroCount;
                const auto lastIndex = std::min(macroIndex+200, macroCount);
                for(; macroIndex<lastIndex; macroIndex++) {
                    auto macro = macros.at(macroIndex);
                    if(!mcuControl->SendMacrosToStreamBuffer({QList<int32_t>::fromVector(macro->GetPacket())})) {
                        QLOG_ERROR() << "It fails to send streaming packets";
                        progress->SetError("It fails to send streaming packets");

                        mcuControl->StopMacroStreaming();
                        return false;
                    }
                }
            }

            if(profilingLog) {
                QThread::msleep(300);
            } else {
                QThread::msleep(500);
            }
        }

        if(!mcuControl->StopMacroStreaming()) {
            QLOG_ERROR() << "It fails to stop macro streaming";
            progress->SetError("It fails to stop macro streaming");
            return false;
        }

        mcuControl->SetLEDChannel(AcquisitionChannels::GetLedChannel(ImagingType::BFGray, TriggerType::Trigger));

        return true;
    }

    auto CommandPreviewAcquisition::Impl::SetError(const QString& message) -> void {
        resp->SetMessage(message);
        progress->SetError(message);
    }

    auto CommandPreviewAcquisition::Impl::SetHighGainMode() -> void {
        auto camera = CameraManager::GetInstance()->GetImagingCamera();
        origianlGain = camera->GetGain();
        camera->SetGainConversionMode(TC::CameraControl::GainConversion::HCG);
        camera->SetGain(previewGain);
    }

    auto CommandPreviewAcquisition::Impl::RestoreGainMode() -> void {
        auto camera = CameraManager::GetInstance()->GetImagingCamera();
        camera->SetGainConversionMode(TC::CameraControl::GainConversion::LCG);
        camera->SetGain(origianlGain);
    }

    CommandPreviewAcquisition::CommandPreviewAcquisition(const Config& config, AcquisitionProgress::Pointer progress)
        : Command("PreviewAcquisition")
        , d{ std::make_unique<Impl>(this) } {
        d->progress = progress;
        d->resp = std::make_shared<Response>();

        d->config = config;
    }

    CommandPreviewAcquisition::~CommandPreviewAcquisition() {
    }

    auto CommandPreviewAcquisition::SetMotions(const QList<UnitMotion>& motions) -> void {
        d->motions = motions;
    }

    auto CommandPreviewAcquisition::SetBFExposure(double bfExposureMSec) -> void {
        using Param = PreviewParam;
        const auto gainCoeffA = d->config.GetPreviewParameter(Param::GainCoefficientA).toDouble();
        const auto gainCoeffB = d->config.GetPreviewParameter(Param::GainCoefficientB).toDouble();
        const auto gainCoeffP = d->config.GetPreviewParameter(Param::GainCoefficientP).toDouble();
        const auto gainCoeffQ = d->config.GetPreviewParameter(Param::GainCoefficientQ).toDouble();

        try {
            d->previewGain = (std::log(gainCoeffP*bfExposureMSec + gainCoeffQ) - std::log(gainCoeffA)) / gainCoeffB;
            QLOG_INFO() << "Preview gain = " << d->previewGain << "dB (BF Exposure=" << bfExposureMSec << "msec)";
        } catch(std::exception& ex) {
            QLOG_ERROR() << "Failed to calculate the camera gain value for preview acquisition. [a="
                         << gainCoeffA <<", b=" << gainCoeffB << ", p=" << gainCoeffP << ", q=" << gainCoeffQ << "] "
                         << " So the gain is forced to 20 dB. [Exception=" << ex.what() << "]";
            d->previewGain = 20.0;
        }
    }

    auto CommandPreviewAcquisition::Perform() -> bool {
        d->resp->SetResult(false);

        using Parameter = MacroPreviewAcquisition::Parameter;
        using Param = PreviewParam;

        Parameter param;
        param.startEndMarginPulse = d->config.GetPreviewParameter(Param::StartEndMarginPulse).toInt();
        param.forwardDelayCompensationPulse = d->config.GetPreviewParameter(Param::ForwardDelayCompensationPulse).toInt();
        param.backwardDelayCompensationPulse = d->config.GetPreviewParameter(Param::BackwardDelayCompensationPulse).toInt();
        param.ledIntensity = std::min<int32_t>(255, d->config.GetPreviewParameter(Param::LedIntensity).toInt()*2.55);
        param.exposureUSec = d->config.GetPreviewParameter(Param::ExposureUSec).toInt();
        param.readoutUSec = d->config.GetPreviewParameter(Param::ReadoutUSec).toInt();
        param.triggerPulseWidth = d->config.GetPreviewParameter(Param::TriggerPulseWidth).toInt();
        param.accelFactor = d->config.GetPreviewParameter(Param::AccelerationFactor).toDouble();
        param.ledChannel = d->config.GetPreviewParameter(Param::LedChannel).toInt();
        param.filterChannel = d->config.GetPreviewParameter(Param::FilterChannel).toInt();

        auto macros = MacroPreviewAcquisition::Setup(d->motions, param);
        if(!d->RunMacro(macros)) {
            d->SetError("It fails to run acquisition macros");
            return false;
        }

        d->resp->SetResult(true);
        return true;
    }

    auto CommandPreviewAcquisition::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}
