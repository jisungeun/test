#define LOGGER_TAG "[CommandInitialize]"
#include <QElapsedTimer>
#include <QThread>

#include <TCLogger.h>
#include <System.h>
#include <MCUFactory.h>
#include <CameraControlFactory.h>

#include "CameraManager.h"
#include "ImageSink.h"
#include "JoystickMonitor.h"
#include "EmissionFilters.h"
#include "FeatureMap.h"
#include "CommandInitialize.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandInitialize::Impl {
        Response::Pointer resp{ nullptr };
        InitializeProgress::Pointer progress{ nullptr };
        TC::MCUControl::IMCUControl::Pointer mcuControl;
        Config config;

        auto SetError(const QString& message)->void {
            resp->SetMessage(message);
            resp->SetResult(false);
            if(progress) progress->SetError(message);
        }

        auto UpdageProgress(double value)->void {
            if(progress) progress->SetProgress(value);
        }

        auto ReadMCULog()->QStringList;

        auto GetAxisLimits() const ->QList<int32_t> {
            QList<int32_t> limits;

            for(auto axis : Axis::_values()) {
                limits.push_back(config.AxisUpperLimitPulse(axis));
            }

            return limits;
        }

        auto CheckMoving(const TC::MCUControl::MCUResponse& response)->bool {
            using ResponseCode = TC::MCUControl::Response;
            QVector<ResponseCode> codes{ ResponseCode::AxisXMoving, ResponseCode::AxisYMoving, ResponseCode::AxisZMoving,
                                         ResponseCode::AxisUMoving, ResponseCode::AxisVMoving, ResponseCode::AxisWMoving };
            for(auto code : codes) {
                if(response.GetValueAsFlag(code)) {
                    return true;
                }
            }

            return false;
        }

        auto UpdateFeatures()->bool;
    };

    auto CommandInitialize::Impl::ReadMCULog() -> QStringList {
        QString logAll;

        while (true) {
            auto log = mcuControl->ReadMCULog();
            if(log.isEmpty()) break;
            logAll.append(log);
        }

        QList<QString> output;

        auto toks = logAll.split("0A");
        for(auto tok : toks) {
            QString str;
            const auto len = tok.length();
            for(int32_t idx=0; idx<(len-1); idx+=2) {
                const auto sub = tok.mid(idx, 2);
                bool ok;
                auto hex = sub.toInt(&ok, 16);
                str.push_back(hex);
            }
            output.push_back(str);
        }

        return output;

    }

    auto CommandInitialize::Impl::UpdateFeatures() -> bool {
        QList<int32_t> data;
        if(!mcuControl->ReadFlash(data)) return false;

        enum Address {
            XAxisEncoderExist = 34,
            YAxisEncoderExist = 38,
            TotalCount
        };

        if(data.length() < TotalCount) return false;

        FeatureMap::Set(InstrumentFeature::SupportSampleStageEncoder, (data[XAxisEncoderExist]>0) && (data[YAxisEncoderExist]>0));

        return true;
    }

    CommandInitialize::CommandInitialize(const Config& config) : Command("Initialize"), d{new Impl} {
        d->config = config;
    }

    CommandInitialize::CommandInitialize(const Config& config, InitializeProgress::Pointer progress)
        : Command("Initialize"), d{new Impl} {
        d->config = config;
        d->progress = progress;
        d->resp = std::make_shared<Response>();
        d->resp->SetResult(false);
    }

    CommandInitialize::~CommandInitialize() {
    }

    auto CommandInitialize::Perform() -> bool {
        d->UpdageProgress(0);

        //1. init Camera
        auto imagingCamera = CameraManager::GetInstance()->GetImagingCamera();\
        if(!imagingCamera) { 
            d->SetError("No imaging camera exists");
            return false;
        }

        if(!imagingCamera->Initialize()) {
            const auto error = imagingCamera->GetLastError();
            d->SetError(QString("Imaging camera initialization is failed [%1]").arg(error._to_string()));
            return false;
        }

        auto condenserCamera = CameraManager::GetInstance()->GetCondenserCamera();
        if(!condenserCamera) {
            d->SetError("No condenser camera exits");
            return false;
        }

        if(!condenserCamera->Initialize()) {
            const auto error = imagingCamera->GetLastError();
            d->SetError(QString("Condenser camera initialization is failed [%1]").arg(error._to_string()));
            return false;
        }

        //2. init MCU
        d->mcuControl = TC::MCUControl::MCUFactory::CreateControl(TC::MCUControl::Model::HTX);

        TC::MCUControl::MCUConfig mcuConfig;
        mcuConfig.SetPort(d->config.GetMCUCommPort());
        mcuConfig.SetBaudrate(d->config.GetMCUCommBaudrate());
        mcuConfig.SetSafeZPosition(d->config.SafeZPositionPulse());
        mcuConfig.SetResolution(TC::MCUControl::Axis::X, d->config.AxisResolutionPPM(Axis::AxisX));
        mcuConfig.SetResolution(TC::MCUControl::Axis::Y, d->config.AxisResolutionPPM(Axis::AxisX));
        mcuConfig.SetResolution(TC::MCUControl::Axis::Z, d->config.AxisResolutionPPM(Axis::AxisZ));
        mcuConfig.SetResolution(TC::MCUControl::Axis::U, d->config.AxisResolutionPPM(Axis::AxisC));
        mcuConfig.SetResolution(TC::MCUControl::Axis::V, d->config.AxisResolutionPPM(Axis::AxisF));
        mcuConfig.SetResolution(TC::MCUControl::Axis::W, d->config.AxisResolutionPPM(Axis::AxisD));
        d->mcuControl->SetConfig(mcuConfig);

        auto emFilterChannels = d->config.GetEmissionFilterChannels();
        for(auto channel : emFilterChannels) {
            EmissionFilters::Set(channel, d->config.GetEmissionFilter(channel));
        }

        if(!d->mcuControl->OpenPort()) {
            const auto code = d->mcuControl->GetLastError();
            d->SetError(QString("It fails to open MCU Comm port [%1]").arg(code.GetText()));
            return false;
        }

        //Check MCU status at the first...
        bool mcuInitRequired{ true };

        TC::MCUControl::MCUResponse mcuResponse;
        d->mcuControl->CheckStatus(mcuResponse);
        auto mcuState = mcuResponse.GetState();
        if(mcuState == +TC::MCUControl::MCUState::Testing) {
            if(d->mcuControl->StopMacroStreaming()) {
                mcuInitRequired = false;
            } else {
                d->SetError("It fails to stop the macro streaming at the beginning of initialization.");
                return false;
            }
        } else if(mcuState == +TC::MCUControl::MCUState::Ready) {
            mcuInitRequired = false;
        } else if(mcuState == +TC::MCUControl::MCUState::Error) {
            const auto logs = d->ReadMCULog();
            QLOG_WARN() << "=== Dump MCU Logs ===";
            for(auto log : logs) {
                QLOG_WARN() << log;
            }

            if(d->mcuControl->Recover()) {
                QElapsedTimer timer;
                timer.start();
                do {
                    d->mcuControl->CheckStatus(mcuResponse);
                    mcuState = mcuResponse.GetState();
                    QThread::msleep(200);
                    const auto isReadyState = (mcuState == +TC::MCUControl::MCUState::Ready);
                    const auto isMoving = d->CheckMoving(mcuResponse);
                    if(isReadyState && !isMoving) break;
                } while(timer.elapsed() < 60000);

                QLOG_INFO() << "MCU State After Recover = " << mcuState._to_string();
                if(mcuState != +TC::MCUControl::MCUState::Ready) {
                    const auto code = d->mcuControl->GetLastError();
                    d->SetError(QString("It fails to recover MCU from error state [%1]").arg(code.GetText()));
                    return false;
                }
                mcuInitRequired = false;
            } else {
                const auto code = d->mcuControl->GetLastError();
                d->SetError(QString("It fails to recover MCU from error state [%1]").arg(code.GetText()));
                return false;
            }
        }

        if(d->mcuControl->ReadMCUFirmwareVersion().isEmpty()) {
            d->SetError("It fails to read MCU firmware version");
            return false;
        }

#if 0
        int32_t dlpcFWVer, dlpcSWVer;
        if(!d->mcuControl->GetDLPCVersion(dlpcFWVer, dlpcSWVer)) {
            d->SetError("It fails to read DLCP version");
            return false;
        }
#endif

        if(!d->mcuControl->SetSWLimitPosition(d->GetAxisLimits())) {
            const auto code = d->mcuControl->GetLastError();
            d->SetError(QString("It fails to set axis sw limits [%1]").arg(code.GetText()));
            return false;
        }

        const auto emissionFilterPositions = d->config.GetEmissionFilterPositions();
        if(emissionFilterPositions.length() > 0) {
            if(!d->mcuControl->SetFilterPositions(emissionFilterPositions)) {
                const auto code = d->mcuControl->GetLastError();
                d->SetError(QString("It fails to set emission filter positions [%1]").arg(code.GetText()));
                return false;
            }
        }

        if(!d->UpdateFeatures()) {
            d->SetError(QString("It fails to update MCU features"));
            return false;
        }

        if(mcuInitRequired) {
            if(!d->mcuControl->StartInitialization()) {
                const auto code = d->mcuControl->GetLastError();
                d->SetError(QString("It fails to start MCU initialization [%1]").arg(code.GetText()));
                return false;
            }

            d->UpdageProgress(0.1);

            //TODO HTX-673 MCUState 코드를 갱신하고 적당한 상태로 변경하기
            QElapsedTimer mcuInitTimer;
            mcuState = TC::MCUControl::MCUState::Error;
            int32_t mcuInitProgress = 0;
            mcuInitTimer.start();
            while(!mcuInitTimer.hasExpired(360000) && mcuState != +TC::MCUControl::MCUState::Ready) {
                QThread::msleep(200);
                d->mcuControl->CheckInitialization(mcuInitProgress);
                d->mcuControl->CheckStatus(mcuResponse);
                mcuState = mcuResponse.GetState();
                d->UpdageProgress(0.1 + 0.6*mcuInitProgress/100.0);
            }
        }

        if(!d->mcuControl->MoveToSafeZPos()) {
            d->SetError(QString("It fails to move Z to safe position"));
            return false;
        }

        if(!WaitForMotionStop(60000, 100)) {
            d->SetError(QString("It fails to reach safe Z position"));
            return false;
        }

        d->UpdageProgress(0.7);

        d->mcuControl->CheckStatus(mcuResponse);
        if(mcuResponse.GetValue(TC::MCUControl::Response::AfOn)==1) {
            d->mcuControl->EnableAF(0);
        }

        auto sysConfig = AppEntity::System::GetSystemConfig();

        //Check AF status first...
        int32_t afStatus, afResult, afTrialCount;
        d->mcuControl->CheckAFStatus(afStatus, afResult, afTrialCount);
        if(afStatus == -1) {
            using AFInitState = TC::MCUControl::AFInitState;

            QLOG_INFO() << "It initializes auto focus module because it is in the error state";

            d->mcuControl->InitAF();

            AFInitState afInitStatus{ AFInitState::Initializing };

            QElapsedTimer afInitTimer;
            afInitTimer.start();
            while(!afInitTimer.hasExpired(60000) && (afInitStatus == +AFInitState::Initializing)) {
                d->mcuControl->CheckAFInitialization(afInitStatus);
            }
        }

        using AFParam = AppEntity::AutoFocusParameter;

        TC::MCUControl::MCUAFParam afParm;
        afParm.SetLensID(sysConfig->GetAutofocusParameter(AFParam::LensID));
        afParm.SetInFocusRange(sysConfig->GetAutofocusParameter(AFParam::InFocusRange));
        afParm.SetDirection(sysConfig->GetAutofocusParameter(AFParam::Direction));
        afParm.SetLoopInterval(sysConfig->GetAutofocusParameter(AFParam::LoopInterval));
        afParm.SetMaxTrialCount(sysConfig->GetAutofocusParameter(AFParam::MaxTrialCount));
        afParm.SetSensorValue(sysConfig->GetAutofocusParameter(AFParam::MinSensorValue), 
                              sysConfig->GetAutofocusParameter(AFParam::MaxSensorValue));
        afParm.SetResolution(sysConfig->GetAutofocusParameter(AFParam::ResolutionMul), 
                             sysConfig->GetAutofocusParameter(AFParam::ResolutionDiv));
        afParm.SetMaxCompensationPulse(sysConfig->GetAutofocusParameter(AFParam::MaxCompensationPulse));
        if(!d->mcuControl->SetAFParameter(afParm)) {
            d->SetError("It fails to set auto focus parameter");
            return false;
        }

        if(!d->mcuControl->EnableAF(0)) {
            d->SetError("It fails to disable auto focus");
            return false;
        }

        const auto defaultFocus = sysConfig->GetAutofocusParameter(AFParam::DefaultTargetValue);
        int32_t targetValueOnAFM{ 0 };
        if(!d->mcuControl->SetAFTarget(0, defaultFocus, targetValueOnAFM)) {
            d->SetError(QString("It fails to set default focus target value as %1").arg(defaultFocus));
            return false;
        }

        d->UpdageProgress(0.75);
        
        //3. init Joystick
        auto joystick = JoystickMonitor::GetInstance();
        joystick->SetEnable();

        d->UpdageProgress(0.85);

        //4. init Chamber
        //TODO Environmental chamber 초기화 구현

        d->UpdageProgress(0.9);

        d->resp->SetResult(true);

        d->UpdageProgress(1.0);

        return d->resp->GetResult();
    }

    auto CommandInitialize::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}
