#pragma once
#include <memory>
#include <QVector>

#include "InstrumentDefines.h"
#include "ImagingParameter.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandLive : public Command {
    public:
        CommandLive(LiveMode mode, const ImagingParameter& param, const QVector<int32_t>& pos);
        ~CommandLive() override;

        auto SetUpdateParmaeterOnly(bool updateOnly)->void;
        auto SetIntensityRange(int32_t minVal, int32_t maxVal)->void;

        auto Perform() -> bool override;
        auto GetResponse()->Response::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}