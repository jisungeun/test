#include <QMutex>
#include "CondenserAutofocusProgress.h"

namespace HTXpress::AppComponents::Instrument {
    struct CondenserAutofocusProgress::Impl {
        double progress{ 0 };
        bool error{ false };
        QString errorMessage;
        QMutex mutex;
    };

    CondenserAutofocusProgress::CondenserAutofocusProgress() : d{new Impl} {
    }

    CondenserAutofocusProgress::~CondenserAutofocusProgress() {
    }

    auto CondenserAutofocusProgress::SetProgress(double progress) -> void {
        QMutexLocker locker(&d->mutex);
        d->progress = progress;
    }

    auto CondenserAutofocusProgress::GetProgress() const -> std::tuple<bool, double> {
        QMutexLocker locker(&d->mutex);
        return std::make_tuple(d->error, d->progress);
    }

    auto CondenserAutofocusProgress::SetError(const QString& message) -> void {
        QMutexLocker locker(&d->mutex);
        d->error = true;
        d->errorMessage = message;
    }

    auto CondenserAutofocusProgress::GetError() const -> bool {
        QMutexLocker locker(&d->mutex);
        return d->error;
    }

    auto CondenserAutofocusProgress::GetErrorMessage() const -> QString {
        QMutexLocker locker(&d->mutex);
        return d->errorMessage;
    }
}
