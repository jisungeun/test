#include <QMutex>
#include "AcquisitionProgress.h"

namespace HTXpress::AppComponents::Instrument {
    struct AcquisitionProgress::Impl {
        double progress{ 0 };
        AppEntity::Position position;
        bool stopRequested{ false };
        bool stopped{ false };
        bool error{ false };
        bool completed{ false };
        QString errorMessage;
        QMutex mutex;
    };

    AcquisitionProgress::AcquisitionProgress() : d{new Impl} {
    }

    AcquisitionProgress::~AcquisitionProgress() {
    }

    auto AcquisitionProgress::SetProgress(double progress) -> void {
        QMutexLocker locker(&d->mutex);
        d->progress = progress;
    }

    auto AcquisitionProgress::SetPosition(const AppEntity::Position& position) -> void {
        d->position = position;
    }

    auto AcquisitionProgress::GetProgress() const -> std::tuple<bool,double,AppEntity::Position> {
        QMutexLocker locker(&d->mutex);
        return std::make_tuple(!d->error, d->progress, d->position);
    }

    auto AcquisitionProgress::StopAcquisition() -> void {
        QMutexLocker locker(&d->mutex);
        d->stopRequested = true;
    }

    auto AcquisitionProgress::IsStopRequested() const -> bool {
        QMutexLocker locker(&d->mutex);
        return d->stopRequested;
    }

    auto AcquisitionProgress::SetStopped() -> void {
        QMutexLocker locker(&d->mutex);
        d->stopped = true;
    }

    auto AcquisitionProgress::GetStopped() const -> bool {
        QMutexLocker locker(&d->mutex);
        return d->stopped || d->completed;
    }

    auto AcquisitionProgress::SetCompleted() -> void {
        QMutexLocker locker(&d->mutex);
        d->completed = true;
    }

    auto AcquisitionProgress::GetCompleted() const -> bool {
        QMutexLocker locker(&d->mutex);
        return d->completed;
    }

    auto AcquisitionProgress::SetError(const QString& message) -> void {
        d->error = true;
        d->errorMessage = message;
    }

    auto AcquisitionProgress::GetError() const -> bool {
        return d->error;
    }

    auto AcquisitionProgress::GetErrorMessage() const -> QString {
        return d->errorMessage;
    }

}
