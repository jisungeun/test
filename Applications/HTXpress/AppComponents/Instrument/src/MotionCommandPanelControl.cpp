#include "MotionCommandPanelControl.h"

namespace HTXpress::AppComponents::Instrument {
    struct MotionCommandPanelControl::Impl {
        QMap<MotionCommandName, MotionCommand::Command::Pointer> motionCommands;
        QMap<MotionCommandName, bool> modifiedFlags;
    };

    MotionCommandPanelControl::MotionCommandPanelControl() : d{new Impl} {
    }

    MotionCommandPanelControl::~MotionCommandPanelControl() {
    }

    auto MotionCommandPanelControl::SetCommand(MotionCommandName name, MotionCommand::Command::Pointer cmd) -> void {
        d->motionCommands[name] = cmd;
        d->modifiedFlags[name] = false;
    }

    auto MotionCommandPanelControl::GetCommand(MotionCommandName name) const -> MotionCommand::Command::Pointer {
        return d->motionCommands[name];
    }

    auto MotionCommandPanelControl::GetCommands() const -> QList<MotionCommand::Command::Pointer> {
        return d->motionCommands.values();
    }

    auto MotionCommandPanelControl::GetCommandByIndex(int32_t index) -> MotionCommand::Command::Pointer {
        return d->motionCommands[MotionCommandName::_from_index(index)];
    }

    auto MotionCommandPanelControl::IsModified() const -> bool {
        if (std::any_of(d->modifiedFlags.begin(), d->modifiedFlags.end(), [](bool modified) { return modified; })) {
            return true;
        }
        return false;
    }

    auto MotionCommandPanelControl::GetModifiedList() const -> QList<MotionCommandName> {
        QList<MotionCommandName> list;

        for(auto iter = d->modifiedFlags.begin(); iter!=d->modifiedFlags.end(); ++iter) {
            if(iter.value()) list.push_back(iter.key());
        }

        return list;
    }
}
