#include <QDebug>
#include "PositionAdjuster.h"

namespace HTXpress::AppComponents::Instrument {
    struct PositionAdjuster::Impl {
        Config config;

        auto ToPulse(Axis axis, RawPosition positionMM) -> int32_t;
    };

    auto PositionAdjuster::Impl::ToPulse(Axis axis, RawPosition positionMM) -> int32_t {
        const auto resolution = config.AxisResolutionPPM(axis);
        const auto compensation = config.AxisCompensation(axis);
        return static_cast<int32_t>(resolution * positionMM * compensation);
    }

    PositionAdjuster::PositionAdjuster() : d{new Impl} {
    }

    PositionAdjuster::~PositionAdjuster() {
    }

    auto PositionAdjuster::GetInstance() -> Pointer {
        static Pointer theInstance{ new PositionAdjuster() };
        return theInstance;
    }

    auto PositionAdjuster::SetConfig(const Config& config) -> void {
        d->config = config;
    }

    auto PositionAdjuster::Adjust(const Axis axis, int32_t currentPulses, int32_t targetPulses) const -> int32_t {
        const auto plusDirection = (targetPulses > currentPulses);
        if(plusDirection) {
            const auto upper = d->config.AxisUpperLimitPulse(axis);
            return std::min(targetPulses, upper);
        }

        const auto lower = d->config.AxisLowerLimitPulse(axis);
        return std::max(targetPulses, lower);
    }

    auto PositionAdjuster::AdjustPR(const Axis axis, int32_t currentPulses, RawPosition targetMM) const -> int32_t {
        return Adjust(axis, currentPulses, d->ToPulse(axis, targetMM));
    }

    auto PositionAdjuster::AdjustRR(const Axis axis, RawPosition currentMM, RawPosition targetMM) const -> int32_t {
        return Adjust(axis, d->ToPulse(axis, currentMM), d->ToPulse(axis, targetMM));
    }
}
