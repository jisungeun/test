#pragma once
#include <memory>

#include "InstrumentDefines.h"

namespace HTXpress::AppComponents::Instrument {
	class JoystickMonitor {
	public:
		typedef std::shared_ptr<JoystickMonitor> Pointer;

	private:
		JoystickMonitor();

	public:
		~JoystickMonitor();

        static auto GetInstance()->Pointer;

        auto SetEnable()->void;
        auto SetDisable()->void;
        auto CheckJoystick(void)->JoystickStatus;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
	};
}