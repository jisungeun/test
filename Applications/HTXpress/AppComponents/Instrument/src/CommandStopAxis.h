#pragma once
#include <memory>

#include "InstrumentDefines.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandStopAxis : public Command {
    public:
        CommandStopAxis();
        virtual ~CommandStopAxis();

        auto Perform() -> bool override;
        auto GetResponse() -> Response::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}