#include "StatusUpdater.h"
#include "StatusObserver.h"

namespace HTXpress::AppComponents::Instrument {
    StatusObserver::StatusObserver() {
        StatusUpdater::GetInstance()->Register(this);
    }

    StatusObserver::~StatusObserver() {
        StatusUpdater::GetInstance()->Unregister(this);
    }
}