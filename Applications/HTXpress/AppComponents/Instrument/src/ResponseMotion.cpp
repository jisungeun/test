#include "ResponseMotion.h"

namespace HTXpress::AppComponents::Instrument {
    struct ResponseMotion::Impl {
        bool moving{ false };
        bool afFailed{ false };
    };

    ResponseMotion::ResponseMotion() : d{new Impl} {
    }

    ResponseMotion::~ResponseMotion() {
    }

    auto ResponseMotion::SetMoving(bool moving) -> void {
        d->moving = moving;
    }

    auto ResponseMotion::GetMoving() const -> bool {
        return d->moving;
    }

    auto ResponseMotion::SetAFFailed(bool fail) -> void {
        d->afFailed = fail;
    }

    auto ResponseMotion::GetAFFailed() const -> bool {
        return d->afFailed;
    }
}

