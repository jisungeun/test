#define LOGGER_TAG "[ImageSink]"
#include <QList>
#include <QMutexLocker>
#include <QWaitCondition>

#include <TCLogger.h>

#include "ImagePort.h"
#include "ImageSink.h"

namespace HTXpress::AppComponents::Instrument {
    struct ImageSink::Impl {
        QList<ImagePort*> ports;
        QMutex mutex;
        QWaitCondition waitCond;
        bool running{ true };
        QList<RawImage::Pointer> imageBuffer;
        QString title;
    };

    ImageSink::ImageSink(const QString& title) :d {new Impl} {
        d->title = title;
        start();
    }

    ImageSink::~ImageSink() {
        d->mutex.lock();
        d->running = false;
        d->waitCond.wakeAll();
        d->mutex.unlock();

        wait();
    }

    auto ImageSink::Send(const RawImage::Pointer rawImage) -> bool {
        QMutexLocker locker(&d->mutex);        
        d->imageBuffer.push_back(rawImage);
        d->waitCond.wakeOne();
        return true;
    }

    auto ImageSink::Clear() -> void {
        QMutexLocker locker(&d->mutex);
        d->imageBuffer.clear();
    }

    auto ImageSink::RemainCount() const -> int32_t {
        QMutexLocker locker(&d->mutex);
        return d->imageBuffer.size();
    }

    auto ImageSink::Register(ImagePort* port) -> void {
        QMutexLocker locker(&d->mutex);
        if(d->ports.contains(port)) return;
        d->ports.append(port);
    }

    auto ImageSink::Deregister(ImagePort* port) -> void {
        QMutexLocker locker(&d->mutex);
        d->ports.removeAll(port);
    }

    void ImageSink::run() {
        QLOG_INFO() << "ImageSink(" << d->title << ") thread is started";

        while(d->running) {
            QMutexLocker locker(&d->mutex);
            if(d->imageBuffer.empty()) {
                d->waitCond.wait(locker.mutex());
                continue;
            }

            auto rawImage = d->imageBuffer.front();
            d->imageBuffer.pop_front();
            locker.unlock();

            auto image = std::make_shared<Image>(rawImage->GetWidth(),
                                                 rawImage->GetHeight(),
                                                 rawImage->GetData());
            image->SetTimestamp(rawImage->GetTimestamp());

            for(auto port : d->ports) {
                port->Send(image);
            }
        }

        QLOG_INFO() << "ImageSink(" << d->title << ") thread is finished";
    }
}
