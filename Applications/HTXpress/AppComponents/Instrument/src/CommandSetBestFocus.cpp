#include <MCUFactory.h>

#include "CommandSetBestFocus.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandSetBestFocus::Impl {
        CommandSetBestFocus* p{ nullptr };
        Response::Pointer resp{ nullptr };

        enum Mode {
            Current,
            Target
        } mode{ Mode::Current };
        int32_t target{ 0 };
        int32_t targetReturend{ 0 };

        explicit Impl(CommandSetBestFocus*p) : p(p) {
            resp = std::make_shared<Response>();
        }

        auto SetError(const QString& message)->void;
    };

    auto CommandSetBestFocus::Impl::SetError(const QString& message) -> void {
        resp->SetMessage(message);
    }

    CommandSetBestFocus::CommandSetBestFocus() : Command("BestFocus"), d{new Impl(this)} {
        d->mode = Impl::Mode::Current;
    }

    CommandSetBestFocus::CommandSetBestFocus(int32_t value) : Command("BestFocus"), d{new Impl(this)} {
        d->mode = Impl::Mode::Target;
        d->target = value;
    }

    CommandSetBestFocus::~CommandSetBestFocus() {
    }

    auto CommandSetBestFocus::Perform() -> bool {
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();
        d->resp->SetResult(false);

        if(d->mode == Impl::Mode::Current) {
            if(!mcuControl->SetAFTarget(1, d->target, d->targetReturend)) {
                d->SetError("It fails to set the best focus value");
                return false;
            }
        } else {
            if(!mcuControl->SetAFTarget(0, d->target, d->targetReturend)) {
                d->SetError("It fails to set the best focus value");
                return false;
            }
            d->targetReturend = d->target;
        }

        d->resp->SetResult(true);
        return true;
    }

    auto CommandSetBestFocus::GetResponse() -> Response::Pointer {
        return d->resp;
    }

    auto CommandSetBestFocus::GetNewTarget() const -> int32_t {
        return d->targetReturend;
    }

}
