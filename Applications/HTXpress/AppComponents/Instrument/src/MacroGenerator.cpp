#include "MacroHandlerHT.h"
#include "MacroHandlerBF.h"
#include "MacroHandlerFL.h"
#include "MotionCommandRepo.h"

#include "MacroGenerator.h"

namespace HTXpress::AppComponents::Instrument {
    struct MacroGenerator::Impl {
        ImagingScenario::Pointer scenario;
        MacroHandler::Pointer macroHandler;

        MotionCommandRepo::Pointer repo{ MotionCommandRepo::GetInstance() };
    };

    MacroGenerator::MacroGenerator(const FLFilterMap& exFilters, const FLFilterMap& emFilters) : d{new Impl} {
        auto handlerHT = std::make_shared<MacroHandlerHT>();
        auto handlerFL = std::make_shared<MacroHandlerFL>(exFilters, emFilters);
        auto handlerBF = std::make_shared<MacroHandlerBF>();

        d->macroHandler = handlerHT;
        handlerHT->SetNext(handlerBF);
        handlerBF->SetNext(handlerFL);
    }

    MacroGenerator::~MacroGenerator() {
    }

    auto MacroGenerator::SetImagingScenario(const AppEntity::ImagingScenario::Pointer& scenario) -> void {
        d->scenario = scenario;
    }

    auto MacroGenerator::GenerateSinglePointMacro(const uint32_t index, const Position& position) const -> MacroGroup::Pointer {
        if(d->scenario == nullptr) return nullptr;

        auto sequence = d->scenario->GetSequence(index);
        if(sequence == nullptr) return nullptr;

        MacroGroup::Pointer macros;

        const auto modalityCount = sequence->GetModalityCount();
        for(auto idx=0; idx<modalityCount; ++idx) {
            auto cond = sequence->GetImagingCondition(idx);

            auto macroPerModality = d->macroHandler->Handle(cond);
            if(macroPerModality == nullptr) return nullptr;

            macros->AppendMacroGroup(macroPerModality);
        }

        const auto count = macros->GetMacroCount();
        for(auto idx=0; idx<count; ++idx) {
            auto macro = macros->GetMacro(idx);
            macro->SetPosition(position.X(), position.Y(), position.Z());
        }

        return macros;
    }

    auto MacroGenerator::GenerateMultiPointsMacro(const QList<PositionGroup>& positionGroups) const -> MacroGroup::Pointer {
        MacroGroup::Pointer macros;

        auto res = std::all_of(positionGroups.begin(), positionGroups.end(), [&](const PositionGroup& positionGroup) {
            const auto colCount = positionGroup.GetCols();
            const auto rowCount = positionGroup.GetRows();

            auto cmd = d->repo->GetRunMacroMatrix(colCount, rowCount, 0, 0);
            if(cmd == nullptr) return false;

            for(uint32_t idx=0; idx<(colCount * rowCount); idx++) {
                const auto rowIdx = idx / colCount;
                const auto colIdx = [=]()->uint32_t {
                    if(rowIdx%2==0) return idx % colCount;
                    return colCount - (idx%colCount) - 1;
                }();

                auto macro = std::shared_ptr<Macro>();
                macro->SetCommandID(cmd->GetCommandID());

                const auto& position = positionGroup.GetPosition(rowIdx, colIdx);
                macro->SetPosition(position.X(), position.Y(), position.Z());

                macros->AppendMacro(macro);
            }

            return true;
        });
        if(!res) return nullptr;

        return macros;
    }
}
