#include <QList>

#include "StatusObserver.h"
#include "StatusUpdater.h"

namespace HTXpress::AppComponents::Instrument {
    struct StatusUpdater::Impl {
        QList<StatusObserver*> observers;
    };

    StatusUpdater::StatusUpdater() : d{new Impl} {
    }

    StatusUpdater::~StatusUpdater() {
    }

    auto StatusUpdater::GetInstance() -> Pointer {
        static Pointer theInstance{ new StatusUpdater() };
        return theInstance;
    }

    auto StatusUpdater::UpdateCurrentPosition(int32_t posX, int32_t posY, int32_t posZ, int32_t posC) -> void {
        for(auto observer : d->observers) {
            observer->UpdateCurrentPosition(posX, posY, posZ, posC);
        }
    }

    auto StatusUpdater::Register(StatusObserver* observer) -> void {
        d->observers.append(observer);
    }

    auto StatusUpdater::Unregister(StatusObserver* observer) -> void {
        d->observers.removeOne(observer);
    }
}
