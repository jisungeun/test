#define LOGGER_TAG "[EnableAutoFocus]"
#include <QElapsedTimer>

#include <TCLogger.h>
#include <MCUFactory.h>

#include "CommandEnableAutoFocus.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandEnableAutoFocus::Impl {
        CommandEnableAutoFocus* p{nullptr};
        bool enable{ false };
        int32_t maximumTrial{ 10 };
        Response::Pointer resp{ nullptr};

        Impl(CommandEnableAutoFocus* p, bool enable, int32_t maxTrials) : p(p), enable(enable), maximumTrial(maxTrials) {
            resp = std::make_shared<Response>();
        }

        auto SetError(const QString& message)->void;
    };

    auto CommandEnableAutoFocus::Impl::SetError(const QString& message) -> void {
        resp->SetMessage(message);
    }

    CommandEnableAutoFocus::CommandEnableAutoFocus(bool enable, int32_t maxTrials)
        : Command("EnableAutoFocus")
        , d{new Impl(this, enable, maxTrials)} {
    }

    CommandEnableAutoFocus::~CommandEnableAutoFocus() {
    }

    auto CommandEnableAutoFocus::Perform() -> bool {
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        d->resp->SetResult(false);

        if(d->enable) {
            if(mcuControl->EnableAF(1)) {
                int32_t lastTrial = -1;

                QElapsedTimer timer;
                timer.start();
                while(timer.elapsed() < 10000) {
                    int32_t status, result, trialCount;
                    if(!mcuControl->CheckAFStatus(status, result, trialCount)) {
                        d->resp->SetMessage("It fails to read auto focus status");
                        break;
                    }

                    if(lastTrial != trialCount) {
                        QLOG_INFO() << "status=" << status << " result=" << result << " trials=" << trialCount;
                        lastTrial = trialCount;
                    }

                    if(status == -1) {
                        d->resp->SetMessage(QString("It fails to find best focus [result=%1 trials=%2]").arg(result).arg(trialCount));
                        break;
                    } else if(status == 0) {
                        if(trialCount < d->maximumTrial) {
                            d->resp->SetResult(true);
                        }
                        break;
                    }
                }

                QList<int32_t> adcDiff, zComp;
                if(mcuControl->ReadAFLog(adcDiff, zComp)) {
                    QStringList strAdcDiff, strZComp;
                    std::for_each(adcDiff.begin(), adcDiff.end(), [&strAdcDiff](int32_t value) {
                        strAdcDiff.push_back(QString::number(value));
                    });
                    std::for_each(zComp.begin(), zComp.end(), [&strZComp](int32_t value) {
                        strZComp.push_back(QString::number(value));
                    });

                    QLOG_INFO() << "AF Log - ADC: " << strAdcDiff.join(',') << " ZComp:" << strZComp.join(',');
                }
            }
        } else {
            if(mcuControl->EnableAF(0)) d->resp->SetResult(true);
        }

        return true;
    }

    auto CommandEnableAutoFocus::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}
