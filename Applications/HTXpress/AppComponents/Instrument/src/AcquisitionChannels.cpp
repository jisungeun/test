#include <QDebug>
#include <QPair>

#include "AcquisitionChannels.h"

namespace HTXpress::AppComponents::Instrument {
    using Index = QPair<AppEntity::ImagingType, AppEntity::TriggerType>;

    struct AcquisitionChannels::Impl {
        QMap<Index, QList<int32_t>> channels;
        QMap<Index, int32_t> ledChannel;
    };

    AcquisitionChannels::AcquisitionChannels() : d{new Impl} {
    }

    AcquisitionChannels::~AcquisitionChannels() {
    }

    auto AcquisitionChannels::GetInstance() -> Pointer {
        static Pointer theInstance{ new AcquisitionChannels() };
        return theInstance;
    }

    auto AcquisitionChannels::Set(const AppEntity::ImagingType imagingType, 
                                  const AppEntity::TriggerType trigger,
                                  const int32_t ledChannel,
                                  const QList<int32_t>& channels) -> void {
        GetInstance()->d->channels[{imagingType, trigger}] = channels;
        GetInstance()->d->ledChannel[{imagingType, trigger}] = ledChannel;
        qDebug() << "imagingType=" << imagingType._to_string()
                 << " trigger=" << trigger._to_string()
                 << " ledChannel=" << ledChannel
                 << " channels=" << channels;
    }

    auto AcquisitionChannels::Get(const AppEntity::ImagingType imagingType, 
                                  const AppEntity::TriggerType trigger, 
                                  int32_t channel) -> int32_t {
        const auto itr = GetInstance()->d->channels.find({imagingType, trigger});
        if(itr == GetInstance()->d->channels.end()) return -1;

        const auto& channels = itr.value();
        if((channel < 0) || (channel >= channels.length())) { //for HT/BF
            return channels.at(0);
        }

        return channels.at(channel);
    }

    auto AcquisitionChannels::GetLedChannel(const AppEntity::ImagingType imagingType,
                                            const AppEntity::TriggerType trigger) -> int32_t {
        const auto itr = GetInstance()->d->ledChannel.find({imagingType, trigger});
        if(itr == GetInstance()->d->ledChannel.end()) return -1;
        return itr.value();
    }
}
