#include <QSettings>

#include "MotionCommandRepo.h"
#include "MotionCommandWriter.h"

namespace HTXpress::AppComponents::Instrument {
    MotionCommandWriter::MotionCommandWriter() {
    }

    MotionCommandWriter::~MotionCommandWriter() {
    }

    auto MotionCommandWriter::Write(const QString& path) -> bool {
        auto repo = MotionCommandRepo::GetInstance();
        return Write(path, repo->GetCommands());
    }

    auto MotionCommandWriter::Write(const QString& path, const QList<MotionCommand::Command::Pointer>& commands) -> bool {
        QSettings qs(path, QSettings::IniFormat);

        qs.beginWriteArray("MotionCommand");
        int cmdIdx = 0;

        for(auto& cmd : commands) {
            qs.setArrayIndex(cmdIdx++);
            qs.setValue("Name", cmd->GetNameAsString());
            qs.setValue("ID", cmd->GetCommandID());

            auto names = cmd->GetNames();
            auto params = cmd->GetParameters();

            for(int paramIdx=0; paramIdx<names.length(); paramIdx++) {
                qs.setValue(names[paramIdx], params[paramIdx]);
            }
        }

        qs.endArray();

        return true;
    }
}
