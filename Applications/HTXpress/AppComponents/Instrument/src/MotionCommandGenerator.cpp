#define LOGGER_TAG "[Instrument]"

#include <TCLogger.h>
#include <MCUFactory.h>

#include "MotionCommandRepo.h"
#include "MotionCommandFactory.h"
#include "MotionCommandGenerator.h"

namespace HTXpress::AppComponents::Instrument {
    struct MotionCommandGenerator::Impl {
        MotionCommandRepo::Pointer repo{ MotionCommandRepo::GetInstance() };
        TC::MCUControl::IMCUControl::Pointer mcuControl;

        auto UploadMotionCommand(MotionCommandName name)->bool;

        auto EnableSequence(MotionCommandName commandName, int32_t intensityR, int32_t intensityG,
                               int32_t intensityB, int32_t exposure, int32_t interval) -> bool;
        auto TriggerWithMotion(MotionCommandName commandName, Axis axis, int32_t motionStart, int32_t motionEnd, 
                               int32_t triggerStart, int32_t triggerEnd, int32_t interval, int32_t pulseWidth, 
                               int32_t speed, int32_t acceleration) -> bool;
        auto TriggerOnly(MotionCommandName commandName, int32_t triggerCount, int32_t interval, int32_t pulseWidth)->bool;
    };

    auto MotionCommandGenerator::Impl::UploadMotionCommand(MotionCommandName name) -> bool {
        auto cmd = repo->GetCommand(name);
        if(cmd == nullptr) return false;

        const auto cmdName = cmd->GetName();

        return mcuControl->SetCommand(cmd->GetCommandID(), cmd->GetCommandType(), cmd->GetParameters());
    }

    auto MotionCommandGenerator::Impl::EnableSequence(MotionCommandName commandName, int32_t intensityR, int32_t intensityG,
                                                      int32_t intensityB, int32_t exposure, int32_t interval) -> bool {
        auto refCmd = repo->GetCommand(commandName);
        if(refCmd->GetName() != +commandName) {
            QLOG_ERROR() << "Motion command type is invalid (" << refCmd->GetNameAsString() << ")";
            return false;
        }

        const auto* refCmdPtr = dynamic_cast<MotionCommand::EnablePatternSequence*>(refCmd.get());
        if(refCmdPtr == nullptr) return false;

        auto cmd = std::make_shared<MotionCommand::EnablePatternSequence>(*refCmdPtr);
        cmd->SetIntensity(intensityR, intensityG, intensityB);
        cmd->SetExposure(exposure);
        cmd->SetInterval(interval);

        if(cmd->operator==(*refCmdPtr)) return true;

        repo->SetCommand(commandName, cmd);

        if(!UploadMotionCommand(commandName)) {
            QLOG_ERROR() << "It fails to upload motion command to MCU";
            return false;
        }

        return true;
    }

     auto MotionCommandGenerator::Impl::TriggerWithMotion(MotionCommandName commandName, Axis axis, 
                                                          int32_t motionStart, int32_t motionEnd, 
                                                          int32_t triggerStart, int32_t triggerEnd, 
                                                          int32_t interval, int32_t pulseWidth, 
                                                          int32_t speed, int32_t acceleration) -> bool {
        auto cmd = std::make_shared<MotionCommand::TriggerWithMotion>();
        cmd->SetAxis(axis);
        cmd->SetMotionRange(motionStart, motionEnd);
        cmd->SetTriggerRange(triggerStart, triggerEnd);
        cmd->SetTriggerInterval(interval);
        cmd->SetTriggerWidth(pulseWidth);
        cmd->SetMotion(speed, acceleration);

        auto refCmd = repo->GetCommand(commandName);
        if(!refCmd->HasSameName(*cmd)) {
            QLOG_ERROR() << "Motion command type is invalid";
            return false;
        }

        const auto* refCmdPtr = dynamic_cast<MotionCommand::TriggerWithMotion*>(refCmd.get());
        if(cmd->operator==(*refCmdPtr)) return true;

        repo->SetCommand(commandName, cmd);

        if(!UploadMotionCommand(commandName)) {
            QLOG_ERROR() << "It fails to upload motion command to MCU";
            return false;
        }

        return true;
    }

    auto MotionCommandGenerator::Impl::TriggerOnly(MotionCommandName commandName, 
                                                   int32_t triggerCount, 
                                                   int32_t interval, 
                                                   int32_t pulseWidth) -> bool {
        auto cmd = std::make_shared<MotionCommand::TriggerOnly>();
        cmd->SetTriggerCount(triggerCount);
        cmd->SetTriggerInterval(interval);
        cmd->SetTriggerWidth(pulseWidth);

        auto refCmd = repo->GetCommand(commandName);
        if(!refCmd->HasSameName(*cmd)) {
            QLOG_ERROR() << "Motion command type is invalid";
            return false;
        }

        const auto* refCmdPtr = dynamic_cast<MotionCommand::TriggerOnly*>(refCmd.get());
        if(cmd->operator==(*refCmdPtr)) return true;

        repo->SetCommand(commandName, cmd);

        if(!UploadMotionCommand(commandName)) {
            QLOG_ERROR() << "It fails to upload motion command to MCU";
            return false;
        }

        return true;
    }

    MotionCommandGenerator::MotionCommandGenerator() : d{new Impl} {
    }

    MotionCommandGenerator::~MotionCommandGenerator() {
    }

    auto MotionCommandGenerator::StopAcquisition() -> bool {
        const auto commandName{ MotionCommandName::StopSequence };

        auto cmd = std::make_shared<MotionCommand::EnablePatternSequence>();
        cmd->SetEnable(false);

        auto refCmd = d->repo->GetCommand(commandName);
        if(!refCmd->HasSameName(*cmd)) {
            QLOG_ERROR() << "Motion command type is invalid";
            return false;
        }

        const auto* refCmdPtr = dynamic_cast<MotionCommand::EnablePatternSequence*>(refCmd.get());
        if(cmd->operator==(*refCmdPtr)) return true;

        d->repo->SetCommand(commandName, cmd);

        if(!d->UploadMotionCommand(commandName)) {
            QLOG_ERROR() << "It fails to upload motion command to MCU";
            return false;
        }

        return true;
    }

    auto MotionCommandGenerator::StartBFLive(int32_t intensityR, int32_t intensityG, int32_t intensityB, int32_t exposure,
        int32_t interval) -> bool {
        constexpr auto commandName{ MotionCommandName::MonoBFStartSequence };
        return d->EnableSequence(commandName, intensityR, intensityG, intensityB, exposure, interval);
    }

    auto MotionCommandGenerator::StartFLLive(int32_t channel, int32_t intensityR, int32_t intensityG,
                                             int32_t intensityB, int32_t exposure, int32_t interval) -> bool {
        if((channel < 0) || (channel>2)) {
            QLOG_ERROR() << "Invalid channel = " << channel;
            return false;
        }

        MotionCommandName baseType = MotionCommandName::FLLiveCH0StartSequence;
        MotionCommandName commandName{ MotionCommandName::_from_integral(baseType._to_integral() + channel) };
        return d->EnableSequence(commandName, intensityR, intensityG, intensityB, exposure, interval);
    }

    auto MotionCommandGenerator::ChangeFilter(int32_t channel) -> bool {
        if((channel < 0) || (channel>5)) {
            QLOG_ERROR() << "Invalid channel = " << channel;
            return false;
        }

        MotionCommandName baseType = MotionCommandName::ChangeFilter1;
        MotionCommandName commandName{ MotionCommandName::_from_integral(baseType._to_integral() + channel) };

        auto refCmd = d->repo->GetCommand(commandName);
        if(refCmd->GetName() != +commandName) {
            QLOG_ERROR() << "Motion command type is invalid (" << refCmd->GetNameAsString() << ")";
            return false;
        }

        const auto* refCmdPtr = dynamic_cast<MotionCommand::FilterWheel*>(refCmd.get());
        if(refCmdPtr == nullptr) return false;

        auto cmd = std::make_shared<MotionCommand::FilterWheel>(*refCmdPtr);
        cmd->SetFilter(channel);

        if(cmd->operator==(*refCmdPtr)) return true;

        d->repo->SetCommand(commandName, cmd);

        if(!d->UploadMotionCommand(commandName)) {
            QLOG_ERROR() << "It fails to upload motion command to MCU";
            return false;
        }

        return true;
    }

    auto MotionCommandGenerator::StartFLAcquisition(int32_t channel, int32_t intensityR, int32_t intensityG,
                                                    int32_t intensityB, int32_t exposure, int32_t interval) -> bool {
        if((channel < 0) || (channel>2)) {
            QLOG_ERROR() << "Invalid channel = " << channel;
            return false;
        }

        MotionCommandName baseType = MotionCommandName::FLCH0StartSequence;
        MotionCommandName commandName{ MotionCommandName::_from_integral(baseType._to_integral() + channel) };
        return d->EnableSequence(commandName, intensityR, intensityG, intensityB, exposure, interval);
    }

    auto MotionCommandGenerator::AcquireFL3D(Axis axis, int32_t motionStart, int32_t motionEnd, int32_t triggerStart,
        int32_t triggerEnd, int32_t interval, int32_t pulseWidth, int32_t speed, int32_t acceleration) -> bool {
        const auto commandName{ MotionCommandName::FL3DTrigger };
        return d->TriggerWithMotion(commandName, axis, motionStart, motionEnd, triggerStart, triggerEnd, interval, pulseWidth,
                                    speed, acceleration);
    }

    auto MotionCommandGenerator::AcquireFL2D(int32_t triggerCount, int32_t interval, int32_t pulseWidth) -> bool {
        const auto commandName{ MotionCommandName::FL2DTrigger };
        return d->TriggerOnly(commandName, triggerCount, interval, pulseWidth);
    }

    auto MotionCommandGenerator::StartHTAcquisition(int32_t NAIdx, int32_t intensityR, int32_t intensityG,
                                                    int32_t intensityB, int32_t exposure, int32_t interval) -> bool {
        if((NAIdx < 0) || (NAIdx>3)) {
            QLOG_ERROR() << "Invalid NA Index = " << NAIdx;
            return false;
        }

        MotionCommandName baseType = MotionCommandName::HTStartSequence1;
        MotionCommandName commandName{ MotionCommandName::_from_integral(baseType._to_integral() + NAIdx) };
        return d->EnableSequence(commandName, intensityR, intensityG, intensityB, exposure, interval);
    }

    auto MotionCommandGenerator::AcquireHT3D(Axis axis, int32_t motionStart, int32_t motionEnd, int32_t triggerStart,
        int32_t triggerEnd, int32_t interval, int32_t pulseWidth, int32_t speed, int32_t acceleration) -> bool {
        const auto commandName{ MotionCommandName::HT3DTrigger };
        return d->TriggerWithMotion(commandName, axis, motionStart, motionEnd, triggerStart, triggerEnd,
                                    interval, pulseWidth, speed, acceleration);
    }

    auto MotionCommandGenerator::AcquireHT2D(int32_t triggerCount, int32_t interval, int32_t pulseWidth) -> bool {
        const auto commandName{ MotionCommandName::HT2DTrigger };
        return d->TriggerOnly(commandName, triggerCount, interval, pulseWidth);
    }

    auto MotionCommandGenerator::StartMonoBFAcquisition(int32_t intensityR, int32_t intensityG, int32_t intensityB,
                                                        int32_t exposure, int32_t interval) -> bool {
        const auto commandName{ MotionCommandName::MonoBFStartSequence };
        return d->EnableSequence(commandName, intensityR, intensityG, intensityB, exposure, interval);
    }

    auto MotionCommandGenerator::AcquireMonoBF(int32_t triggerCount, int32_t interval, int32_t pulseWidth) -> bool {
        const auto commandName{ MotionCommandName::MonoBFTrigger };
        return d->TriggerOnly(commandName, triggerCount, interval, pulseWidth);
    }

    auto MotionCommandGenerator::StartColorBFAcquisition(int32_t intensityR, int32_t intensityG, int32_t intensityB,
                                                         int32_t exposure, int32_t interval) -> bool {
        const auto commandName{ MotionCommandName::ColorBFStartSequence };
        return d->EnableSequence(commandName, intensityR, intensityG, intensityB, exposure, interval);
    }

    auto MotionCommandGenerator::AcquireColorBF(int32_t triggerCount, int32_t interval, int32_t pulseWidth) -> bool {
        const auto commandName{ MotionCommandName::ColorBFTrigger };
        return d->TriggerOnly(commandName, triggerCount, interval, pulseWidth);
    }

    auto MotionCommandGenerator::ClearMacroMatrix() -> void {
        d->repo->ClearMacroMatrix();
    }

    auto MotionCommandGenerator::SetMacroMatrix(int32_t countX, int32_t countY, int32_t distX, int32_t distY,
        int32_t macroGroup, int32_t scanDirection) -> bool {
        auto cmd = std::make_shared<MotionCommand::RunMacroMatrix>();
        cmd->SetCount(countX, countY);
        cmd->SetDistance(distX, distY);
        cmd->SetMacroGroupID(macroGroup);
        cmd->SetScanDirection(scanDirection);

        d->repo->SetMatrixCommand(cmd);

        return true;
    }
}
