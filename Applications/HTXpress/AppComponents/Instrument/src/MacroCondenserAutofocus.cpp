#include "MacroCondenserAutofocus.h"

namespace HTXpress::AppComponents::Instrument::MacroCondenserAutofocus {

    auto BuildChangeFilter(int32_t filterIndex) -> QVector<int32_t> {
        QVector<int32_t> cmdParams(1);
        cmdParams[0] = filterIndex;
        return cmdParams;
    }

    auto BuildPatternSequence(int32_t patternIndex, int32_t ledChannel, int32_t ledIntensity,
                              int32_t exposure, int32_t readout, bool enable) -> QVector<int32_t> {
        QVector<int32_t> cmdParams(8);
        cmdParams[0] = enable;                                              //enable/disable
        cmdParams[1] = patternIndex;                                        //sequence id
        cmdParams[2] = (ledChannel != 0) ? 0 : ledIntensity;                //intensity of Red LED on DLPC
        cmdParams[3] = (ledChannel != 1) ? 0 : ledIntensity;                //intensity of Green LED on DLPC
        cmdParams[4] = (ledChannel != 2) ? 0 : ledIntensity;                //intensity of Blue LED on DLPC
        cmdParams[5] = exposure;                                            //exposure time
        cmdParams[6] = exposure + readout;                                  //interval time, 8msec = transfer time....
        cmdParams[7] = Camera::Internal;

        return cmdParams;
    }

    auto BuildTriggerWithStop(int32_t startPos, 
                              int32_t stepDist, 
                              int32_t slices,
                              int32_t pulseWidth,
                              int32_t triggerDelay) -> std::tuple<QVector<int32_t>, int32_t> {
        QVector<int32_t> cmdParams(6);

        const auto endPos = startPos + (stepDist * slices) - 1;

        cmdParams[0] = 4;               //C axis (4 = U axis, 0-based) - FIXED
        cmdParams[1] = startPos;        //Start pos (relative to the current)
        cmdParams[2] = endPos;          //End pos (relative to the current)
        cmdParams[3] = stepDist;        //Step amount
        cmdParams[4] = pulseWidth;      //Pulse width
        cmdParams[5] = triggerDelay;

        return std::make_tuple(cmdParams, slices * 2);
    }

    auto Setup(const QList<RawPosition>& positions, 
               const int32_t patternIndex,
               const int32_t intensity,
               const Config& config) -> QList<StreamingMacro::Pointer> {
        using CommandType = StreamingMacro::CommandType;

        struct Command {
            CommandType command;
            QVector<int32_t> params;
            int32_t imageCount{ 0 };
        };
        QList<Command> patternSeqCommands;

        const auto ledChannel = config.GetCondenserAFLightChannel();
        const auto exposure = config.GetCondenserAFExposureUSec();
        const auto readout = config.GetCondenserAFReadouseUSec();
        const auto startPos = config.GetCondenserAFStartPosPulse();
        const auto stepDist = config.GetCondenserAFTriggerIntervalPulse();
        const auto slices = config.GetCondenserAFSlices();
        const auto pulseWidth = config.GetCondenserAFPulseWidthUSec();
        const auto triggerMargin = config.GetCondenserAFIntervalMarginUSec();
        const auto triggerDelay = (exposure + readout) * 2 + triggerMargin;

        patternSeqCommands.push_back({CommandType::PatternSequence, BuildPatternSequence(patternIndex,
                                                                                         ledChannel,
                                                                                         intensity,
                                                                                         exposure,
                                                                                         readout,
                                                                                         true)});
        auto [cmdParams, imgCount] = BuildTriggerWithStop(startPos,
                                                          stepDist,
                                                          slices,
                                                          pulseWidth,
                                                          triggerDelay);
        patternSeqCommands.push_back({CommandType::TriggerWithStop, cmdParams, imgCount});

        patternSeqCommands.push_back({CommandType::PatternSequence, BuildPatternSequence(patternIndex,
                                                                                         ledChannel,
                                                                                         intensity,
                                                                                         exposure,
                                                                                         readout,
                                                                                         false)});

        auto convert = [config](Axis axis, RawPosition rawPos)->int32_t {
            auto resolution = config.AxisResolutionPPM(axis);
            auto compensation = config.AxisCompensation(axis);
            return static_cast<int32_t>(resolution * rawPos * compensation);
        };

        const auto xPulses = convert(Axis::AxisX, positions.at(0));
        const auto yPulses = convert(Axis::AxisY, positions.at(1));
        const auto zPulses = convert(Axis::AxisZ, positions.at(2));

        QList<StreamingMacro::Pointer> macros;
        for(const auto& cmd : patternSeqCommands) {
            auto macro = std::make_shared<StreamingMacro>();
            macro->SetPosition(xPulses, yPulses, zPulses);
            macro->SetAutoFoucs(0);
            macro->SetCommand(cmd.command, cmd.params);
            macro->SetImageCount(cmd.imageCount);
            macros.push_back(macro);
        }

        return macros;
    }
}