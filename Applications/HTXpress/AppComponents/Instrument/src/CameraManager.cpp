#include <QMutexLocker>

#include "ImageSink.h"
#include "CameraManager.h"

namespace HTXpress::AppComponents::Instrument {
    using CameraSystem = TC::CameraControl::CameraSystem;
    using CameraControl = TC::CameraControl::CameraControl;

    struct CameraManager::Impl {
        CameraSystem::Pointer cameraSystem{ nullptr };
        CameraControl::Pointer imagingCamera{ nullptr };
        CameraControl::Pointer condenserCamera{ nullptr };

        struct Info {
            QString serial;
            ImageSink::Pointer sink{ nullptr };
        } imagingCameraInfo, condenserCameraInfo;

        bool cleanUpCalled{ false };

        QMutex mutex;
    };

    CameraManager::CameraManager(CameraType type) : d{new Impl} {
        using CameraFactory = TC::CameraControl::CameraControlFactory;
        d->cameraSystem = CameraFactory::Create(type);
    }

    CameraManager::~CameraManager() {
    }

    auto CameraManager::GetInstance(CameraType type) -> Pointer {
        static Pointer theInstance{ new CameraManager(type) };
        return theInstance;
    }

    auto CameraManager::CleanUp() -> void {
        QMutexLocker locker(&d->mutex);
        d->cameraSystem->CleanUp();
        d->imagingCamera.reset();
        d->condenserCamera.reset();
        d->cleanUpCalled = true;
    }

    auto CameraManager::SetImagingCamera(const QString& serial) -> void {
        QMutexLocker locker(&d->mutex);
        if(d->imagingCameraInfo.serial == serial) return;

        ImageSink::Pointer sink{new ImageSink("Imaging")};
        d->imagingCameraInfo = {serial, sink };
    }

    auto CameraManager::SetCondenserCamera(const QString& serial) -> void {
        QMutexLocker locker(&d->mutex);
        if(d->condenserCameraInfo.serial == serial) return;

        ImageSink::Pointer sink{new ImageSink("Condenser")};
        d->condenserCameraInfo = {serial, sink };
    }

    auto CameraManager::GetImagingCamera() const -> CameraControl::Pointer {
        QMutexLocker locker(&d->mutex);
        if((d->imagingCamera == nullptr) && (d->cleanUpCalled == false)) {
            d->imagingCamera = d->cameraSystem->GetCamera(d->imagingCameraInfo.serial,
                                                          d->imagingCameraInfo.sink.get(),
                                                          "Imaging");
        }
        return d->imagingCamera;
    }

    auto CameraManager::GetCondenserCamera() const -> CameraControl::Pointer {
        QMutexLocker locker(&d->mutex);
        if((d->condenserCamera == nullptr) && (d->cleanUpCalled == false)) {
            d->condenserCamera = d->cameraSystem->GetCamera(d->condenserCameraInfo.serial,
                                                            d->condenserCameraInfo.sink.get(),
                                                            "Condenser");
        }
        return d->condenserCamera;
    }

    auto CameraManager::GetImageSink(ImageSource source) const -> ImageSink::Pointer {
        QMutexLocker locker(&d->mutex);
        if(source == +ImageSource::ImagingCamera) return d->imagingCameraInfo.sink;
        return d->condenserCameraInfo.sink;
    }

    auto CameraManager::InstallImagePort(ImageSource source, ImagePort* port) -> void{
        QMutexLocker locker(&d->mutex);
        switch(source) {
        case ImageSource::ImagingCamera:
            d->imagingCameraInfo.sink->Register(port);
            break;
        case ImageSource::CondenserCamera:
            d->condenserCameraInfo.sink->Register(port);
            break;
        }
    }

    auto CameraManager::UninstallImagePort(ImageSource source, ImagePort* port) -> void {
        QMutexLocker locker(&d->mutex);
        switch(source) {
        case ImageSource::ImagingCamera:
            d->imagingCameraInfo.sink->Deregister(port);
            break;
        case ImageSource::CondenserCamera:
            d->condenserCameraInfo.sink->Deregister(port);
            break;
        }
    }
}
