#define LOGGER_TAG "[MacroAcquisition]"
#include <TCLogger.h>
#include <MCUFactory.h>

#include "InstrumentDefines.h"
#include "PositionAdjuster.h"
#include "EmissionFilters.h"
#include "IlluminationPatterns.h"
#include "AcquisitionChannels.h"
#include "MacroAcquisition.h"

namespace HTXpress::AppComponents::Instrument::MacroAcquisition {
    using ImagingCondition = AppEntity::ImagingCondition;
    using ImagingConditionBF = AppEntity::ImagingConditionBF;
    using ImagingConditionFL = AppEntity::ImagingConditionFL;
    using Modality = AppEntity::Modality;
    using ImagingType = AppEntity::ImagingType;

    constexpr auto Start = true;
    constexpr auto Stop = false;

    struct PosPulses {
        int32_t x;
        int32_t y;
        int32_t z;
        bool wellChanged{ false };
    };

    auto BuildChangeFilter(const ImagingCondition::Pointer& condition, int32_t channel) -> QVector<int32_t> {
        QVector<int32_t> cmdParams(1);

        auto modality = condition->GetModality();
        if((modality == +Modality::HT) || (modality == +Modality::BF)) {
            cmdParams[0] = EmissionFilters::GetVoidChannel();
        } else {
            auto flCond = std::dynamic_pointer_cast<ImagingConditionFL>(condition);
            auto emFilter = flCond->GetEmFilter(channel);
            cmdParams[0] = EmissionFilters::Get(emFilter);
        }

        return cmdParams;
    }

    auto BuildPatternSequence(const ImagingCondition::Pointer& condition, int32_t channel, bool enable,
                              bool adjustIntensity = false,
                              int32_t intensityOutMin = 0,
                              int32_t intensityOutMax = 255) -> QVector<int32_t> {
        using TriggerType = AppEntity::TriggerType;

        const auto getSequenceId = [=](ImagingType imagingType, int32_t channel)->int32_t {
            return IlluminationPatterns::Get(imagingType, AppEntity::TriggerType::Trigger, channel);
        };

        const auto imagingType = condition->GetImagingType();
        const auto seqId = getSequenceId(imagingType, channel);
        const auto acqCh = AcquisitionChannels::Get(imagingType, TriggerType::Trigger, channel);
        const auto acqIntensity = [=]()->int32_t {
            const auto inputVal = condition->GetIntensity(channel);
            if((adjustIntensity == false) || (inputVal == 0)) return inputVal;
            return intensityOutMin + (intensityOutMax - intensityOutMin) * (inputVal / 100.0);
        }();

        QVector<int32_t> cmdParams(8);
        cmdParams[0] = enable;                                              //enable/disable
        cmdParams[1] = seqId;                                               //sequence id
        cmdParams[2] = (acqCh != 0) ? 0 : acqIntensity;                     //intensity of Red LED on DLPC
        cmdParams[3] = (acqCh != 1) ? 0 : acqIntensity;                     //intensity of Green LED on DLPC
        cmdParams[4] = (acqCh != 2) ? 0 : acqIntensity;                     //intensity of Blue LED on DLPC
        cmdParams[5] = condition->GetExposure(channel);                     //exposure time
        cmdParams[6] = condition->GetExposure(channel) + 8000;              //interval time, 8msec = transfer time....
        cmdParams[7] = Camera::Internal;            //TODO It should support external camera also....

        return cmdParams;
    }

    auto BuildTrigger(const ImagingCondition::Pointer& condition, int32_t channel) -> std::tuple<QVector<int32_t>, int32_t> {
        const auto modality = condition->GetModality();

        const auto triggerDelay = [=]() -> int32_t {
            switch(modality) {
            case Modality::HT:
                return condition->GetInterval(channel) * 4 + 3000;
            case Modality::BF:
            case Modality::FL:
                return condition->GetExposure(channel) + 8000;
            }
            return 0;
        }();

        QVector<int32_t> cmdParams(3);

        cmdParams[0] = 1;               //Trigger Count        
        cmdParams[1] = triggerDelay;    //Trigger Interval
        cmdParams[2] = 100;             //Trigger Pulse Width : 100usec - FIXED

        return std::make_tuple(cmdParams, (modality == +Modality::HT) ? 4 : 1);
    }

    auto BuildTriggerWithStop(const ImagingCondition::Pointer& condition, int32_t channel, const Config& config) -> std::tuple<QVector<int32_t>, int32_t> {
        const auto modality = condition->GetModality();

        const auto triggerDelay = [=]() -> int32_t {
            switch(modality) {
            case Modality::HT:
                return condition->GetInterval(channel) * 4 + config.GetHTTirggerIntervalMarginUSec();
            case Modality::BF:
            case Modality::FL:
                return condition->GetExposure(channel) + config.GetHTTriggerReadOutUSec();
            }
            return 0;
        }();

        const auto stepDist = std::max<int32_t>(1, condition->GetSliceStep());
        const auto stepCount = condition->GetSliceCount();
        const auto startPos = condition->GetSliceStart();
        const auto endPos = startPos + (stepDist * (stepCount - 1)) + 1; 

        QVector<int32_t> cmdParams(6);

        cmdParams[0] = 2;               //Z axis - FIXED
        cmdParams[1] = startPos;        //Start pos (relative to the current)
        cmdParams[2] = endPos;          //End pos (relative to the current)
        cmdParams[3] = stepDist;        //Step amount
        cmdParams[4] = 100;             //100usec - FIXED
        cmdParams[5] = triggerDelay;

        return std::make_tuple(cmdParams, (modality == +Modality::HT) ? stepCount*4 : stepCount);
    }

    auto BuildTriggerWithMotion(const ImagingCondition::Pointer& condition, const Config& config) -> std::tuple<QVector<int32_t>, int32_t> {
        const auto modality = condition->GetModality();
        if(modality != +Modality::HT) return std::make_tuple(QVector<int32_t>(), 0);

        const auto stepDist = std::max<int32_t>(1, condition->GetSliceStep());
        const auto stepCount = condition->GetSliceCount();
        const auto startPos = condition->GetSliceStart();

        const auto startOffset = config.GetHTTriggerStartOffsetPulse();
        const auto endOffset = config.GetHTTriggerEndOffsetPusle();
        const auto triggerInterval = std::max<int32_t>(1, condition->GetSliceStep());
        const auto pulseWidth = config.GetHTTriggerPulseWidthUSec();
        const auto readOut = config.GetHTTriggerReadOutUSec();
        const auto exposure = config.GetHTTriggerExposureUSec();
        const auto intervalMargin = config.GetHTTirggerIntervalMarginUSec();
        const auto accelFactor = config.GetHTTriggerAccelerationFactor();

        const auto motionSpeed = (stepDist * 1000000) / ((readOut + exposure) * 4 + intervalMargin);

        const auto triggerStartPos = startPos;
        const auto triggerEndPos = startPos + (stepDist * (stepCount - 1)) + 1;

        QVector<int32_t> cmdParams(9);

        cmdParams[0] = 2;                                   //Z axis - FIXED
        cmdParams[1] = triggerStartPos - startOffset;       //Motion start pos (relative to the current)
        cmdParams[2] = triggerEndPos + endOffset;           //Motion end pos (relative to the current)
        cmdParams[3] = triggerStartPos;                     //Start pos (relative to the current)
        cmdParams[4] = triggerEndPos;                       //End pos (relative to the current)
        cmdParams[5] = triggerInterval;                     //Step amount
        cmdParams[6] = pulseWidth;                          //Pulse width (usec)
        cmdParams[7] = motionSpeed;                         //Motion speed (pulse per second)
        cmdParams[8] = motionSpeed * accelFactor;           //Motion acceleration (pulse per second^2)

        return std::make_tuple(cmdParams, stepCount*4);
    };
    
    auto Convert(const QList<PositionGroup>& groups, 
                 const QMap<MCUAxis,int32_t>& currentPositions,
                 const AppEntity::WellIndex startingWellIndex)->QList<PosPulses> {
        auto adjuster = PositionAdjuster::GetInstance();

        QList<PosPulses> pulses;

        AppEntity::WellIndex curWellIndex{ startingWellIndex };

        for(const auto& group : groups) {
            for(const auto& position : group.GetPositions()) {
                const auto posMM = position.toMM();
                const auto xPulses = adjuster->AdjustPR(Axis::AxisX, currentPositions[MCUAxis::X], posMM.x);
                const auto yPulses = adjuster->AdjustPR(Axis::AxisY, currentPositions[MCUAxis::Y], posMM.y);
                const auto zPulses = adjuster->AdjustPR(Axis::AxisZ, currentPositions[MCUAxis::Z], posMM.z);

                QLOG_INFO() << "Convert: (" << posMM.x << "," << posMM.y << "," << posMM.z << ")mm to (" << xPulses << "," << yPulses << "," << zPulses << ")pulses";
                pulses.push_back({xPulses, yPulses, zPulses, curWellIndex != group.GetWellIndex()});

                curWellIndex = group.GetWellIndex();
            }
        }

        return pulses;
    }
    
    auto Setup(const ImagingSequence::Pointer& sequence, 
               const QList<PositionGroup>& positions,
               const bool useAutoFocus,
               const Config& config,
               const QMap<MCUAxis,int32_t>& currentPositions,
               const AppEntity::WellIndex startingWellIndex,
               const double focusReadyMM,
               const double multiWellOffsetMM) -> QList<StreamingMacro::Pointer> {
        using CommandType = StreamingMacro::CommandType;

        QList<StreamingMacro::Pointer> macros;

        const auto posPulses = Convert(positions, currentPositions, startingWellIndex);

        struct Command {
            int32_t groupIndex{ 0 };
            CommandType command;
            QVector<int32_t> params;
            int32_t imageCount{ 0 };
        };
        QList<Command> patternSeqCommands;

        const auto condCount = sequence->GetModalityCount();
        for(auto idx=0; idx<condCount; idx++) {
            const auto cond = sequence->GetImagingCondition(idx);
            const auto modality = cond->GetModality();
            const auto imagingType = cond->GetImagingType();
            const auto isSingleStep = (cond->GetSliceCount() == 1);

            if(modality == +Modality::BF) {
                patternSeqCommands.push_back({idx, CommandType::ChangeFilter, BuildChangeFilter(cond, 0)});
                patternSeqCommands.push_back({idx, CommandType::PatternSequence, BuildPatternSequence(cond, 2, Start, 
                                                                                                      true, 
                                                                                                      0,
                                                                                                      255)});
                if(isSingleStep) {
                    auto params = BuildTrigger(cond, 2);
                    patternSeqCommands.push_back({idx, CommandType::TriggerOnly, std::get<0>(params), std::get<1>(params)});
                } else {
                    auto [cmdParams, imgCount] = BuildTriggerWithStop(cond, 2, config);
                    patternSeqCommands.push_back({idx, CommandType::TriggerWithStop, cmdParams, imgCount});
                }
                patternSeqCommands.push_back({idx, CommandType::PatternSequence, BuildPatternSequence(cond, 2, Stop,
                                                                                                      true,
                                                                                                      0,
                                                                                                      255)});
            } else if(modality == +Modality::HT) {
                patternSeqCommands.push_back({idx, CommandType::ChangeFilter, BuildChangeFilter(cond, 0)});
                patternSeqCommands.push_back({idx, CommandType::PatternSequence, BuildPatternSequence(cond, 2, Start)});
                if(isSingleStep) {
                    auto [cmdParams, imgCount] = BuildTrigger(cond, 2);
                    patternSeqCommands.push_back({idx, CommandType::TriggerOnly, cmdParams, imgCount});
                } else {
                    auto [cmdParams, imgCount] = BuildTriggerWithMotion(cond, config);
                    patternSeqCommands.push_back({idx, CommandType::TriggerWithMotion, cmdParams, imgCount});
                }
                patternSeqCommands.push_back({idx, CommandType::PatternSequence, BuildPatternSequence(cond, 2, Stop)});
            } else {
                const auto flCond = std::dynamic_pointer_cast<ImagingConditionFL>(cond);
                const auto channels = flCond->GetChannels();
                for(const auto channel : channels) {
                    patternSeqCommands.push_back({idx, CommandType::ChangeFilter, BuildChangeFilter(cond, channel)});
                    patternSeqCommands.push_back({idx, CommandType::PatternSequence, BuildPatternSequence(cond, channel, Start,
                                                                                                          true,
                                                                                                          config.GetFlOutputRangeMin(),
                                                                                                          config.GetFlOutputRangeMax())});
                    if(isSingleStep) {
                        auto params = BuildTrigger(cond, channel);
                        patternSeqCommands.push_back({idx, CommandType::TriggerOnly, std::get<0>(params), std::get<1>(params)});
                    } else {
                        auto [cmdParams, imgCount] = BuildTriggerWithStop(cond, channel, config);
                        patternSeqCommands.push_back({idx, CommandType::TriggerWithStop, cmdParams, imgCount});
                    }
                    patternSeqCommands.push_back({idx, CommandType::PatternSequence, BuildPatternSequence(cond, channel, Stop,
                                                                                                          true,
                                                                                                          config.GetFlOutputRangeMin(),
                                                                                                          config.GetFlOutputRangeMax())});
                }
            }
        }

        auto adjuster = PositionAdjuster::GetInstance();
        auto focusReadyPulse = adjuster->AdjustPR(Axis::AxisZ, currentPositions[MCUAxis::Z], focusReadyMM);
        auto multiWellOffsetPulse = adjuster->AdjustPR(Axis::AxisZ, currentPositions[MCUAxis::Z], multiWellOffsetMM);
        auto safeZPosPulse = focusReadyPulse - multiWellOffsetPulse;

        for(const auto& position : posPulses) {
            auto wellChanged = position.wellChanged;

            for(const auto& cmd : patternSeqCommands) {
                auto afMode = [&]()->int32_t {
                    if(!useAutoFocus) return 0;
                    return 1;
                }();

                auto zPos = [&]()->int32_t {
                    if(!useAutoFocus) return position.z;
                    return position.z;
                    //zPos will be -1 if it will use the latest best focus position
                }();

                //Add additional commands to avoid the objective lens colliding with the sample stage
                if(wellChanged) {
                    auto macro = std::make_shared<StreamingMacro>();
                    macro->SetPosition(position.x, position.y, zPos);
                    macro->SetAutoFoucs(false);
                    macro->SetCommand(CommandType::ChangePosition, {safeZPosPulse - zPos});
                    macro->SetImageCount(0);
                    macros.push_back(macro);

                    wellChanged = false;
                }

                auto macro = std::make_shared<StreamingMacro>();
                macro->SetPosition(position.x, position.y, zPos);
                macro->SetAutoFoucs(afMode);
                macro->SetCommand(cmd.command, cmd.params);
                macro->SetImageCount(cmd.imageCount);   //Only trigger commands have image count
                macros.push_back(macro);
            }
        }

        //Move to the first position
        if(posPulses.size() > 1) {
            auto& pos = posPulses.at(0);

            auto afMode = [&]()->int32_t {
                    if(!useAutoFocus) return 0;
                    return 1;
            }();

            auto macro = std::make_shared<StreamingMacro>();
            macro->SetPosition(pos.x, pos.y, pos.z);
            macro->SetAutoFoucs(afMode);
            macro->SetCommand(CommandType::ChangePosition, {safeZPosPulse - pos.z});
            macro->SetImageCount(0);
            macros.push_back(macro);
        }

        return macros;
    }
}