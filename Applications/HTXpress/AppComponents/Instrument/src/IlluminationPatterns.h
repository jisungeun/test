#pragma once
#include <memory>
#include <QMap>
#include <QList>

#include <AppEntityDefines.h>

namespace HTXpress::AppComponents::Instrument {
    class IlluminationPatterns {
    public:
        using Pointer = std::shared_ptr<IlluminationPatterns>;

    protected:
        IlluminationPatterns();

    public:
        ~IlluminationPatterns();

        static auto GetInstance()->Pointer;
        static auto Set(const AppEntity::ImagingType imagingType,
                        const AppEntity::TriggerType trigger, 
                        const QList<int32_t>& patterns)->void;
        static auto Get(const AppEntity::ImagingType imagingType,
                        const AppEntity::TriggerType trigger, 
                        int32_t channel)->int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}