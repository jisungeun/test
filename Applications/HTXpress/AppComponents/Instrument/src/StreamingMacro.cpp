#include <QTextStream>

#include "StreamingMacro.h"

#pragma warning(push)
#pragma warning(disable:4127)

namespace HTXpress::AppComponents::Instrument {
    struct StreamingMacro::Impl {
        struct {
            int32_t x{ 0 };
            int32_t y{ 0 };
            int32_t z{ 0 };
        } position;
        int32_t afMode{ 0 };
        CommandType commandType{ CommandType::ChangeFilter };
        QVector<int32_t> parameters;

        struct {
            int32_t count{ 0 };
        } extra;
    };

    StreamingMacro::StreamingMacro() : d{new Impl} {
    }

    StreamingMacro::~StreamingMacro() {
    }

    auto StreamingMacro::SetPosition(int32_t xpos, int32_t ypos, int32_t zpos) -> void {
        d->position.x = xpos;
        d->position.y = ypos;
        d->position.z = zpos;
    }

    auto StreamingMacro::SetAutoFoucs(int32_t mode) -> void {
        d->afMode = mode;
    }

    auto StreamingMacro::SetCommand(CommandType type, const QVector<int32_t>& params) -> void {
        d->commandType = type;
        d->parameters = params;
    }

    auto StreamingMacro::GetCommandType() const -> CommandType {
        return d->commandType;
    }

    auto StreamingMacro::GetParameter(int32_t index) const -> int32_t {
        if(index >= d->parameters.size()) return -1;
        return d->parameters.at(index);
    }

    auto StreamingMacro::GetPacket() const -> QVector<int32_t> {
        QVector<int32_t> packet(6);
        packet[0] = d->position.x;
        packet[1] = d->position.y;
        packet[2] = d->position.z;
        packet[3] = d->afMode;
        packet[4] = d->commandType._to_integral();
        packet[5] = d->parameters.size();
        packet.append(d->parameters);
        return packet;
    }

    auto StreamingMacro::Str() const -> QString {
        QString params;
        QTextStream stream(&params);
        for(auto value : d->parameters) {
            stream << value << " ";
        }
        params.remove(-1);

        return QString("Pos[%1, %2, %3] Af[%4] Command[%5] Params[%6]").arg(d->position.x).arg(d->position.y).arg(d->position.z)
                                                                       .arg(d->afMode).arg(d->commandType._to_string())
                                                                       .arg(params);
    }

    auto StreamingMacro::SetImageCount(int32_t count) -> void {
        d->extra.count = count;
    }

    auto StreamingMacro::GetImageCount() const -> int32_t {
        return d->extra.count;
    }

}

#pragma warning(pop)