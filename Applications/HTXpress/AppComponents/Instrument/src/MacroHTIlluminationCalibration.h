#pragma once
#include <memory>
#include <QList>

#include <Position.h>
#include <ImagingCondition.h>

#include "InstrumentConfig.h"
#include "StreamingMacro.h"

namespace HTXpress::AppComponents::Instrument::MacroHTIlluminationCalibration {
    using Position = AppEntity::Position;
    using ImagingConditionHT = AppEntity::ImagingConditionHT;

    auto Setup(const QList<int32_t>& intensityList, 
               const QList<RawPosition>& positions,
               const ImagingConditionHT::Pointer imgCond,
               const Config& config) -> QList<StreamingMacro::Pointer>;
}