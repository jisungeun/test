#pragma once
#include <memory>
#include <QList>

#include "InstrumentDefines.h"
#include "MotionCommand.h"

namespace HTXpress::AppComponents::Instrument {
    using Command = MotionCommand::Command;

    class MotionCommandRepo {
    public:
        typedef std::shared_ptr<MotionCommandRepo> Pointer;

    private:
        MotionCommandRepo();

    public:
        ~MotionCommandRepo();

        static auto GetInstance()->Pointer;

        auto SetCommand(MotionCommandName type, Command::Pointer cmd)->void;
        auto SetMatrixCommand(Command::Pointer cmd)->void;

        auto GetNames() const->QList<MotionCommandName>;
        auto GetCommand(MotionCommandName type)->Command::Pointer;
        auto GetCommands()->QList<Command::Pointer>;

        auto GetRunMacroMatrix(int32_t countX, int32_t countY, int32_t distX, int32_t distY)->Command::Pointer;

        auto Clear()->void;
        auto ClearMacroMatrix()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
};