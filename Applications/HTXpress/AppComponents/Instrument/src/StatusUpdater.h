#pragma once
#include <memory>

namespace HTXpress::AppComponents::Instrument {
	class StatusObserver;

	class StatusUpdater {
	public:
		typedef std::shared_ptr<StatusUpdater> Pointer;

	private:
		StatusUpdater();

	public:
		~StatusUpdater();

		static auto GetInstance()->Pointer;

		auto UpdateCurrentPosition(int32_t posX, int32_t posY, int32_t posZ, int32_t posC)->void;

	protected:
		auto Register(StatusObserver* observer)->void;
		auto Unregister(StatusObserver* observer)->void;

	private:
		friend class StatusObserver;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}