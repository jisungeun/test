#include <QMap>
#include "InstrumentConfig.h"

namespace HTXpress::AppComponents::Instrument {
    struct Config::Impl {
        struct {
            int32_t port;
            int32_t baudrate;
        } mcuComm;

        struct {
            QString serial;
        } imagingCamera, condenserCamera;

        struct AxisInfo {
            uint32_t pulsesPerMM{ 1000 };       //pulses per mm
            double lowerLimit{ -10.0 };
            double upperLimit{ 10.0 };
            double secsPerMM{ 1.0 };            //seconds per mm
            double safeLower{ 0.0 };
            double safeUpper{ 0.0 };
            double compensationFactor{ 1.0 };
            int32_t inPositionPulse{ 4 };
        };
        QMap<Axis, AxisInfo> axisInfos;

        struct {
            double safeZ{ 0 };
            double loadingX{ 0 };
            double loadingY{ 0 };
        } positions;

        QMap<int32_t, AppEntity::FLFilter> emissionFilters;
        QMap<int32_t, int32_t> emissionFilterPositions;

        struct {
            int32_t maxTrials{ 10 };
        } autofocus;

        struct {
            int32_t startOffset{ 0 };
            int32_t endOffset{ 0 };
            int32_t pulseWidth{ 0 };
            int32_t readout{ 0 };
            int32_t exposure{ 0 };
            int32_t intervalMargin{ 0 };
            int32_t accelerationFactor{ 1 };
        } htScan;

        struct {
            int32_t lightChannel{ 0 };
            int32_t startPos{ 0 };
            int32_t triggerInterval{ 0 };
            int32_t slices{ 0 };
            int32_t readout{ 0 };
            int32_t exposre{ 0 };
            int32_t intervalMargin{ 0 };
            int32_t pulseWidth{ 0 };
        } condenserAF;

        struct {
            double thickness { 2.0 };
        } multiDishHolder;

        struct {
            int32_t minVal{ 0 };
            int32_t maxVal{ 255 };
        } flOutputRange;

        QMap<PreviewParam, QVariant> previewParams;
    };

    Config::Config() : d{new Impl} {
    }

    Config::Config(const Config& other) : d{new Impl} {
        *d = *other.d;
    }

    Config::~Config() {
    }

    auto Config::operator=(const Config& rhs) -> Config& {
        *d = *rhs.d;
        return *this;
    }

    auto Config::SetMCUComm(int32_t port, int32_t baudrate) -> void {
        d->mcuComm.port = port;
        d->mcuComm.baudrate = baudrate;
    }

    auto Config::GetMCUCommPort() const -> int32_t {
        return d->mcuComm.port;
    }

    auto Config::GetMCUCommBaudrate() const -> int32_t {
        return d->mcuComm.baudrate;
    }

    auto Config::SetImagingCamera(const QString& serial) -> void {
        d->imagingCamera.serial = serial;
    }

    auto Config::SetCondenserCamera(const QString& serial) -> void {
        d->condenserCamera.serial = serial;
    }

    auto Config::ImagingCamera() const -> QString {
        return d->imagingCamera.serial;
    }

    auto Config::CondenserCamera() const -> QString {
        return d->condenserCamera.serial;
    }

    auto Config::SetAxisResolutionPPM(const Axis axis, const uint32_t value) -> void {
        d->axisInfos[axis].pulsesPerMM = value;
    }

    auto Config::AxisResolutionPPM(Axis axis) const -> uint32_t {
        return d->axisInfos[axis].pulsesPerMM;
    }

    auto Config::SetAxisLimitMM(const Axis axis, const double lower, const double upper) -> void {
        d->axisInfos[axis].lowerLimit = lower;
        d->axisInfos[axis].upperLimit = upper;
    }

    auto Config::AxisLowerLimitMM(Axis axis) const -> double {
        return d->axisInfos[axis].lowerLimit;
    }

    auto Config::AxisUpperLimitMM(Axis axis) const -> double {
        return d->axisInfos[axis].upperLimit;
    }

    auto Config::AxisLowerLimitPulse(Axis axis) const -> int32_t {
        const auto ppm = AxisResolutionPPM(axis);
        const auto limit = AxisLowerLimitMM(axis);
        return static_cast<int32_t>(ppm * limit);
    }

    auto Config::AxisUpperLimitPulse(Axis axis) const -> int32_t {
        const auto ppm = AxisResolutionPPM(axis);
        const auto limit = AxisUpperLimitMM(axis);
        return static_cast<int32_t>(ppm * limit);
    }

    auto Config::SetAxisSafeRangeMM(const Axis axis, const double lower, const double upper) -> void {
        d->axisInfos[axis].safeLower = lower;
        d->axisInfos[axis].safeUpper = upper;
    }

    auto Config::AxisSafeRangeLowerMM(Axis axis) const -> double {
        return d->axisInfos[axis].safeLower;
    }

    auto Config::AxisSafeRangeUpperMM(Axis axis) const -> double {
        return d->axisInfos[axis].safeUpper;
    }

    auto Config::AxisSafeRangeLowerPulse(Axis axis) const -> int32_t {
        const auto ppm = AxisResolutionPPM(axis);
        const auto lower = AxisSafeRangeLowerMM(axis);
        return static_cast<int32_t>(ppm * lower);
    }

    auto Config::AxisSafeRangeUpperPulse(Axis axis) const -> int32_t {
        const auto ppm = AxisResolutionPPM(axis);
        const auto upper = AxisSafeRangeUpperMM(axis);
        return static_cast<int32_t>(ppm * upper);
    }

    auto Config::SetAxisMotionTimeSPM(const Axis axis, const double value) -> void {
        d->axisInfos[axis].secsPerMM = value;
    }

    auto Config::AxisMotionTimeSPM(Axis axis) const -> double {
        return d->axisInfos[axis].secsPerMM;
    }

    auto Config::SetAxisCompensation(Axis axis, double ratio) -> void {
        d->axisInfos[axis].compensationFactor = ratio;
    }

    auto Config::AxisCompensation(Axis axis) const -> double {
        return d->axisInfos[axis].compensationFactor;
    }

    auto Config::SetAxisInPositionInPulse(Axis axis, int32_t pulse) -> void {
        d->axisInfos[axis].inPositionPulse = pulse;
    }

    auto Config::GetAxisInPositionInPulse(Axis axis) const -> int32_t {
        return d->axisInfos[axis].inPositionPulse;
    }

    auto Config::SetSafeZPositionMM(const double value) -> void {
        d->positions.safeZ = value;
    }

    auto Config::SafeZPositionMM() const -> double {
        return d->positions.safeZ;
    }

    auto Config::SafeZPositionPulse() const -> int32_t {
        const auto ppm = AxisResolutionPPM(Axis::AxisZ);
        const auto pos = SafeZPositionMM();
        return static_cast<int32_t>(ppm * pos);
    }

    auto Config::SetLoadingPositionMM(const double xValue, const double yValue) -> void {
        d->positions.loadingX = xValue;
        d->positions.loadingY = yValue;
    }

    auto Config::LoadingXPositionMM() const -> double {
        return d->positions.loadingX;
    }

    auto Config::LoadingYPositionMM() const -> double {
        return d->positions.loadingY;
    }

    auto Config::LoadingXPositionPulse() const -> int32_t {
        const auto ppm = AxisResolutionPPM(Axis::AxisX);
        const auto pos = LoadingXPositionMM();
        return static_cast<int32_t>(ppm * pos);
    }

    auto Config::LoadingYPositionPulse() const -> int32_t {
        const auto ppm = AxisResolutionPPM(Axis::AxisY);
        const auto pos = LoadingYPositionMM();
        return static_cast<int32_t>(ppm * pos);
    }

    auto Config::SetEmissionFilter(const int32_t channel, const AppEntity::FLFilter& filter) -> void {
        d->emissionFilters[channel] = filter;
    }

    auto Config::GetEmissionFilter(const int32_t channel) const -> AppEntity::FLFilter {
        const auto itr = d->emissionFilters.find(channel);
        if(itr == d->emissionFilters.end()) return {0, 0};
        return itr.value();
    }

    auto Config::GetEmissionFilterChannel(const AppEntity::FLFilter filter) const -> int32_t {
        for(auto itr=d->emissionFilters.begin(); itr!= d->emissionFilters.end(); ++itr) {
            if(itr.value() == filter) return itr.key();
        }
        return -1;
    }

    auto Config::GetEmissionFilterChannels() const -> QList<int32_t> {
        return d->emissionFilters.keys();
    }

    auto Config::SetEmissionFilterPosition(const int32_t channel, int32_t pulse) -> void {
        d->emissionFilterPositions[channel] = pulse;
    }

    auto Config::GetEmissionFilterPosition(const int32_t channel) const -> int32_t {
        const auto itr = d->emissionFilterPositions.find(channel);
        if(itr == d->emissionFilterPositions.end()) return -1;
        return itr.value();
    }

    auto Config::GetEmissionFilterPositions() const -> QList<int32_t> {
        return d->emissionFilterPositions.values();
    }

    auto Config::SetAFMaximumTrials(int32_t maxTrials) -> void {
        d->autofocus.maxTrials = maxTrials;
    }

    auto Config::GetAFMaxTrials() const -> int32_t {
        return d->autofocus.maxTrials;
    }

    auto Config::SetHTTriggerScanOffsetPulse(int32_t startOffset, int32_t endOffset) -> void {
        d->htScan.startOffset = startOffset;
        d->htScan.endOffset = endOffset;
    }

    auto Config::GetHTTriggerStartOffsetPulse() const -> int32_t {
        return d->htScan.startOffset;
    }

    auto Config::GetHTTriggerEndOffsetPusle() const -> int32_t {
        return d->htScan.endOffset;
    }

    auto Config::SetHTTriggerPulseWidthUSec(int32_t pulseWidth) -> void {
        d->htScan.pulseWidth = pulseWidth;
    }

    auto Config::GetHTTriggerPulseWidthUSec() const -> int32_t {
        return d->htScan.pulseWidth;
    }

    auto Config::SetHTTriggerReadOutUSec(int32_t readout) -> void {
        d->htScan.readout = readout;
    }

    auto Config::GetHTTriggerReadOutUSec() const -> int32_t {
        return d->htScan.readout;
    }

    auto Config::SetHTTriggerExposureUSec(int32_t exposure) -> void {
        d->htScan.exposure = exposure;
    }

    auto Config::GetHTTriggerExposureUSec() const -> int32_t {
        return d->htScan.exposure;
    }

    auto Config::SetHTTriggerIntervalMarginUSec(int32_t intervalMargin) -> void {
        d->htScan.intervalMargin = intervalMargin;
    }

    auto Config::GetHTTirggerIntervalMarginUSec() const -> int32_t {
        return d->htScan.intervalMargin;
    }

    auto Config::SetHTTriggerAccelerationFactor(int32_t factor) -> void {
        d->htScan.accelerationFactor = factor;
    }

    auto Config::GetHTTriggerAccelerationFactor() const -> int32_t {
        return d->htScan.accelerationFactor;
    }

    auto Config::SetCondenserAFImagingChannels(int32_t lightChannel) -> void {
        d->condenserAF.lightChannel = lightChannel;
    }

    auto Config::GetCondenserAFLightChannel() const -> int32_t {
        return d->condenserAF.lightChannel;
    }

    auto Config::SetCondenserAFScanCondition(int32_t startPulse, int32_t intervalPusle, int32_t slices) -> void {
        d->condenserAF.startPos = startPulse;
        d->condenserAF.triggerInterval = intervalPusle;
        d->condenserAF.slices = slices;
    }

    auto Config::GetCondenserAFStartPosPulse() const -> int32_t {
        return d->condenserAF.startPos;
    }

    auto Config::GetCondenserAFTriggerIntervalPulse() const -> int32_t {
        return d->condenserAF.triggerInterval;
    }

    auto Config::GetCondenserAFSlices() const -> int32_t {
        return d->condenserAF.slices;
    }

    auto Config::SetCondenserAFPatternCondition(int32_t readoutUSec, int32_t exposureUSec, 
                                                 int32_t intervalMarginUSec, int32_t pulseWidthUSec) -> void {
        d->condenserAF.readout = readoutUSec;
        d->condenserAF.exposre = exposureUSec;
        d->condenserAF.intervalMargin = intervalMarginUSec;
        d->condenserAF.pulseWidth = pulseWidthUSec;
    }

    auto Config::GetCondenserAFReadouseUSec() const -> int32_t {
        return d->condenserAF.readout;
    }

    auto Config::GetCondenserAFExposureUSec() const -> int32_t {
        return d->condenserAF.exposre;
    }

    auto Config::GetCondenserAFIntervalMarginUSec() const -> int32_t {
        return d->condenserAF.intervalMargin;
    }

    auto Config::GetCondenserAFPulseWidthUSec() const -> int32_t {
        return d->condenserAF.pulseWidth;
    }

    auto Config::SetMultiDishHolderThicknessMM(double thickness) -> void {
        d->multiDishHolder.thickness = thickness;
    }

    auto Config::GetMultiDishHolderThicknessMM() const -> double {
        return d->multiDishHolder.thickness;
    }

    auto Config::SetFlOutputRange(int32_t minVal, int32_t maxVal) -> void {
        d->flOutputRange.minVal = minVal;
        d->flOutputRange.maxVal = maxVal;
    }

    auto Config::GetFlOutputRangeMin() const -> int32_t {
        return d->flOutputRange.minVal;
    }

    auto Config::GetFlOutputRangeMax() const -> int32_t {
        return d->flOutputRange.maxVal;
    }

    auto Config::SetPreviewParameter(PreviewParam param, const QVariant& value) -> void {
        d->previewParams[param] = value;
    }

    auto Config::GetPreviewParameter(PreviewParam param) const -> QVariant {
        return d->previewParams[param];
    }
}
