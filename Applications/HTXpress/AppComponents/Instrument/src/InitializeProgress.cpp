#include <cmath>
#include <QMutex>

#include "InitializeProgress.h"

namespace HTXpress::AppComponents::Instrument {
    struct InitializeProgress::Impl {
        double progress{ 0 };
        QString strError;
        bool error{ false };
        QMutex mutex;
    };

    InitializeProgress::InitializeProgress() : d{new Impl} {
    }

    InitializeProgress::~InitializeProgress() {
    }

    auto InitializeProgress::SetProgress(double progress) -> void {
        QMutexLocker locker(&d->mutex);
        d->progress = std::round(progress*1000)/1000.0;
    }

    auto InitializeProgress::GetProgress() const -> double {
        QMutexLocker locker(&d->mutex);
        return d->progress;
    }

    auto InitializeProgress::SetError(const QString& message) -> void {
        d->error = true;
        d->strError = message;
    }

    auto InitializeProgress::GetError() const -> bool {
        return d->error;
    }

    auto InitializeProgress::GetErrorMessage() const -> QString {
        return d->strError;
    }
}
