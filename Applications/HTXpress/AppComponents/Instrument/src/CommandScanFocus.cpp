#include <QElapsedTimer>
#include <QThread>
#include <MCUFactory.h>

#include "CommandScanFocus.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandScanFocus::Impl {
        CommandScanFocus* p{ nullptr };
        Response::Pointer resp{ nullptr };

        struct {
            int32_t start;
            int32_t end;
            int32_t interval;
            int32_t delay;
        } param;

        struct {
            int32_t zInPositionRange;
        } config;

        QList<int32_t> values;

        explicit Impl(CommandScanFocus* p) : p{ p } {
            resp = std::make_shared<Response>();
        }

        auto IsArrived(const int32_t startPosition, const int32_t seconds)->bool;
        auto IsCompleted(int32_t seconds)->bool;
        
    };

    auto CommandScanFocus::Impl::IsArrived(const int32_t startPosition, const int32_t seconds) -> bool {
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        int32_t currentPosition{ -1 };

        QElapsedTimer timer;
        timer.start();

        int32_t arrivedCount = 0;

        bool arrived = false;

        while(timer.elapsed() < (seconds*1000)) {
            mcuControl->GetPosition(TC::MCUControl::Axis::Z, currentPosition);
            if(std::abs(startPosition - currentPosition) <= config.zInPositionRange) arrivedCount += 1;
            else arrivedCount = 0;
            if (arrivedCount >= 2) {
                arrived = true;
                break;
            }
            QThread::msleep(500);
        }


        return arrived;
    }

    auto CommandScanFocus::Impl::IsCompleted(int32_t seconds) -> bool {
        using Flag = TC::MCUControl::Flag;
        using MCUResponse = TC::MCUControl::MCUResponse;
        using ResponseCode = TC::MCUControl::Response;
        using MCUState = TC::MCUControl::MCUState;

        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        QElapsedTimer timer;

        timer.start();
        while(timer.elapsed() < (seconds*1000)) {
            MCUResponse mcuResponse;
            if(!mcuControl->CheckStatus(mcuResponse)) return false;
            if(mcuResponse.GetValue(ResponseCode::StateMachineId) == static_cast<int32_t>(+MCUState::Error)) return false;
            if(mcuResponse.GetValueAsFlag(ResponseCode::AxisZMoving) == +Flag::NotMoving) return true;

            QThread::msleep(200);
        }

        return false;
    }

    CommandScanFocus::CommandScanFocus(int32_t startPulse, int32_t endPulse, int32_t internval, int32_t delayUSec,
                                       int32_t zInPositionRange)
        : Command("ScanFocus"), d{ new Impl(this) } {
        d->param.start = startPulse;
        d->param.end = endPulse;
        d->param.interval = internval;
        d->param.delay = delayUSec;
        d->config.zInPositionRange = zInPositionRange;
    }

    CommandScanFocus::~CommandScanFocus() {
    }

    auto CommandScanFocus::Perform() -> bool {
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();
        d->resp->SetResult(false);

        int32_t startPosition;
        mcuControl->GetPosition(TC::MCUControl::Axis::Z, startPosition);

        if(!mcuControl->SetManualAF(true)) {
            d->resp->SetMessage("It fails to turn on AF laser");
            return false;
        }

        if(!mcuControl->ScanZForAFM(d->param.start,
                                    d->param.end,
                                    d->param.interval,
                                    d->param.delay)) {
            d->resp->SetMessage("It fails to scan focus");
            return false;
        }

        QThread::msleep(500);

        if(!d->IsArrived(startPosition, 60)) {
            d->resp->SetMessage("It fails to finish scanning focus");
            return false;
        }

        if(!d->IsCompleted(10)) {
            d->resp->SetMessage("Motion status is not changed to stopped");
            return false;
        }

        if(!mcuControl->DumpZScanForAFM(d->values)) {
            d->resp->SetMessage("It fails to get focus sensor values");
            return false;
        }

        if(!mcuControl->SetManualAF(false)) {
            d->resp->SetMessage("It fails to turn off AF laser");
            return false;
        }

        if(mcuControl->Move(TC::MCUControl::Axis::Z, startPosition)) {
            if(!d->IsCompleted(60)) {
                d->resp->SetMessage("It fails to move back the starting position");
                return false;
            }
        } else {
            d->resp->SetMessage("It fails to start moving back the starting position");
            return false;
        }

        d->resp->SetResult(true);
        return true;
    }

    auto CommandScanFocus::GetResponse() -> Response::Pointer {
        return d->resp;
    }

    auto CommandScanFocus::GetValues() const -> QList<int32_t> {
        return d->values;
    }
}
