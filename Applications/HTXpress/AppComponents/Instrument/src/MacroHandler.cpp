#include "MacroHandler.h"

namespace HTXpress::AppComponents::Instrument {
    struct MacroHandler::Impl {
        Pointer next;
    };

    MacroHandler::MacroHandler() : d{new Impl} {
    }

    MacroHandler::~MacroHandler() {
    }

    auto MacroHandler::SetNext(Pointer handler) -> void {
        d->next = handler;
    }

    auto MacroHandler::Handle(const ImagingCondition::Pointer& cond) const -> MacroGroup::Pointer {
        MacroGroup::Pointer macros;

        if(d->next) macros = d->next->Handle(cond);

        return macros;
    }
}
