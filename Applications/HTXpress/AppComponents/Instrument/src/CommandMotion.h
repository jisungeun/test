#pragma once
#include <memory>

#include "InstrumentDefines.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandMotion : public Command {
    public:
        CommandMotion(Axis axis, int32_t pos);
        virtual ~CommandMotion();

        auto Perform() -> bool override;
        auto GetResponse()->Response::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}