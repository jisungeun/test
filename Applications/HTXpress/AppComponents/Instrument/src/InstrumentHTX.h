#pragma once
#include <memory>
#include <QThread>

#include <ImagingSequence.h>
#include <PositionGroup.h>

#include "InstrumentDefines.h"
#include "Instrument.h"

namespace HTXpress::AppComponents::Instrument {
	class InstrumentHTX : public QThread, public Instrument {
		Q_OBJECT

	public:
		InstrumentHTX();
		~InstrumentHTX() override;

		auto SetConfig(const Config& config)->void override;
		auto Initialize()->bool override;
		auto GetInitializationProgress() const->std::tuple<bool,double> override;
		auto CheckInitialized() const->bool override;
		auto CleanUp() -> void override;

		auto InstallImagePort(ImageSource source, ImagePort* port) -> void override;
		auto UninstallImagePort(ImageSource source, ImagePort* port) -> void override;

		auto SetFLEmission(const QMap<AppEntity::FLFilter, int32_t>& filters) -> void override;

		auto Move(Axis axis, RawPosition targetMM)->bool override;
		auto MoveXY(RawPosition targetXMM, RawPosition targetYMM)->bool override;
		auto MoveZtoSafePos() -> bool override;
		auto IsMoving() const->MotionStatus override;

		auto GetPosition(Axis axis, RawPosition& pos)->bool override;
		auto GetPositionXY(RawPosition& xPos, RawPosition& yPos)->bool override;
		auto GetPositionXYZ(RawPosition& xPos, RawPosition& yPos, RawPosition& zPos)->bool override;

		auto StartJog(Axis axis, bool plusDirection)->bool override;
		auto StopJog()->bool override;

		auto SetJogRange(Axis axis, const PositionRange& range) -> void override;
		auto EnableJoystick() -> bool override;
		auto DisableJoystick() -> bool override;
		auto CheckJoystick() -> JoystickStatus override;

		auto ChangeFOV(int32_t xPixels, int32_t yPixels, int32_t& offsetX, int32_t& offsetY) -> bool override;
		auto StartAcquisition(const AppEntity::ImagingSequence::Pointer sequence,
							  const QList<AppEntity::PositionGroup>& positions,
							  bool useAutofocus,
							  AppEntity::WellIndex startingWellIndex,
							  double focusReadyMM,
							  bool useMultiDishHolder) -> bool override;
		auto StopAcquisition()->bool override;
		auto CheckAcquisitionProgress() const->std::tuple<bool,double,AppEntity::Position> override;
		auto GetRemainCount(ImageSource source) const -> int32_t override;

		auto StartLive(LiveMode mode, const ImagingParameter& param, bool onlyUpdateParameter = false)->bool override;
		auto StopLive()->bool override;
		auto ResumeLive() -> bool override;
		auto StartLiveWithTrigger(const ImagingParameter& param,
								  int32_t triggerCount, int32_t triggerInterval, int32_t pulseWidth,
								  bool useHCG = false, double gain = 0)->bool override;

		auto StartPreviewAcquisition(const QList<UnitMotion>& motions, double bfExposureMSec) -> bool override;

		auto EnableAutoFocus(bool enable) -> std::tuple<bool,bool> override;
		auto AutoFocusEnabled() const -> bool override;
		auto PerformAutoFocus() -> std::tuple<bool,bool> override;
		auto ReadAFSensorValue() -> int32_t override;
		auto SetBestFocusCurrent(int32_t& value) -> bool override;
		auto SetBestFocus(int32_t value) -> bool override;
		auto ScanFocus(double startMm, double distMm, int32_t count) -> QList<int32_t> override;

		auto SetAcquisitionPattenIndex(const AppEntity::ImagingType imagingType,
									   const AppEntity::TriggerType trigger,
									   const QList<int32_t>& index) -> void override;
		auto SetAcquisitionChannelIndex(const AppEntity::ImagingType imagingType, 
										const AppEntity::TriggerType trigger,
										const int32_t ledChannel,
										const QList<int32_t>& index) -> void override;

		auto StartCondenserAutoFocus(int32_t patternIndex, int32_t intensity) -> bool override;
		auto CheckCondenserAutoFocusProgress() const -> std::tuple<bool, double> override;
		auto FinishCondenserAutoFocus() -> void override;

		auto StartHTIlluminationCalibration(const QList<int32_t>& intensityList,
											const AppEntity::ImagingConditionHT::Pointer imgCond) -> bool override;
		auto CheckHTIlluminationCalibrationProgress() const -> std::tuple<bool, double> override;
		auto FinishHTIlluminationCalibration() -> void override;

		auto IsSupported(InstrumentFeature feature) const -> bool override;

		auto GetError() const -> QString override;

	protected:
		void run() override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}