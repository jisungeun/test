#pragma once
#include <memory>
#include <QList>

#include "InstrumentConfig.h"
#include "StreamingMacro.h"

namespace HTXpress::AppComponents::Instrument::MacroCondenserAutofocus {
    auto Setup(const QList<RawPosition>& positions,
               const int32_t patternIndex,
               const int32_t intensity,
               const Config& config) -> QList<StreamingMacro::Pointer>;
}