#pragma once
#include <memory>
#include <QString>
#include "Response.h"

namespace HTXpress::AppComponents::Instrument {
    class Command {
	public:
		typedef std::shared_ptr<Command> Pointer;

    public:
		Command(const QString& title);
		virtual ~Command();

		auto GetTitle() const->QString;

		virtual auto Perform()->bool = 0;
        virtual auto GetResponse()->Response::Pointer = 0;

    protected:
		auto WaitForMotionStop(uint32_t msec = 10000, uint32_t interval = 100) const->bool;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}