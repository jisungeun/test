#include <QSettings>

#include "MotionCommandRepo.h"
#include "MotionCommandFactory.h"
#include "MotionCommandLoader.h"

namespace HTXpress::AppComponents::Instrument {
    MotionCommandLoader::MotionCommandLoader() {
    }

    MotionCommandLoader::~MotionCommandLoader() {
    }

    auto MotionCommandLoader::Load(const QString& path) -> bool {
        QList<MotionCommand::Command::Pointer> commands;
        if(!Load(path, commands)) return false;

        auto repo = MotionCommandRepo::GetInstance();
        repo->Clear();

        for(auto& cmd : commands) {
            repo->SetCommand(cmd->GetName(), cmd);
        }

        return true;
    }

    auto MotionCommandLoader::Load(const QString& path, QList<MotionCommand::Command::Pointer>& commands) -> bool {
        QSettings qs(path, QSettings::IniFormat);

        const auto cmdCount = qs.beginReadArray("MotionCommand");

        for(int cmdIdx = 0; cmdIdx < cmdCount; cmdIdx++) {
            qs.setArrayIndex(cmdIdx);

            const auto name = MotionCommandName::_from_string(qs.value("Name").toString().toLatin1());
            auto cmd = MotionCommandFactory::Build(name);
            if(cmd == nullptr) return false;

            cmd->SetCommandID(qs.value("ID").toInt());

            auto keys = qs.childKeys();
            for(const auto& key : keys) {
                if(key == "Name") continue;
                if(key == "ID") continue;

                cmd->SetParameter(key, qs.value(key).toInt());
            }

            commands.push_back(cmd);
        }

        qs.endArray();

        return true;
    }
}
