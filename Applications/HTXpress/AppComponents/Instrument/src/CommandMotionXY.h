#pragma once
#include <memory>

#include "InstrumentDefines.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandMotionXY : public Command {
    public:
        CommandMotionXY(int32_t targetXPulse, int32_t targetYPulse);
        virtual ~CommandMotionXY();

        auto Perform() -> bool override;
        auto GetResponse()->Response::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}