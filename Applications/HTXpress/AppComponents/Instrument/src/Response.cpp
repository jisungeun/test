#include "Response.h"

namespace HTXpress::AppComponents::Instrument {
    struct Response::Impl {
        bool result{ false };
        QString message;
    };

    Response::Response() : d{new Impl} {
    }

    Response::~Response() {
    }

    auto Response::SetResult(bool result) -> void {
        d->result = result;
    }

    auto Response::GetResult() const -> bool {
        return d->result;
    }

    auto Response::SetMessage(const QString& message) -> void {
        d->message = message;
    }

    auto Response::GetMessage() const -> QString {
        return d->message;
    }
}
