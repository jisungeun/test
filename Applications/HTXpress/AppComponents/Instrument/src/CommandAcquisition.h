#pragma once
#include <memory>

#include <ImagingSequence.h>
#include <PositionGroup.h>

#include "InstrumentDefines.h"
#include "InstrumentConfig.h"
#include "AcquisitionProgress.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandAcquisition : public Command {
    public:
        CommandAcquisition(const AppEntity::ImagingSequence::Pointer& sequence,
                           const QList<AppEntity::PositionGroup>& positions,
                           const bool useAutoFocus,
                           AcquisitionProgress::Pointer progress = nullptr);
        ~CommandAcquisition() override;
        auto SetConfig(const Config& config)->void;
        auto SetStartingWellIndex(AppEntity::WellIndex wellIndex)->void;
        auto SetFocusReadyMM(double posMM)->void;
        auto UseMultiDishHolder()->void;

        auto Perform() -> bool override;
        auto GetResponse()->Response::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}