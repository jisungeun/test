#include <QMap>

#include "Macro.h"

namespace HTXpress::AppComponents::Instrument {
    struct Macro::Impl {
        enum {
            GroupID,
            Index,
            CommandID,
            PositionX,
            PositionY,
            PositionZ,
            AFMode
        };

        QMap<int32_t, int32_t> params{
            {GroupID, 0},
            {Index, 0},
            {CommandID, 0},
            {PositionX, 0},
            {PositionY, 0},
            {PositionZ, 0},
            {AFMode, 0}
        };
    };

    Macro::Macro() : d{new Impl} {
    }

    Macro::Macro(int32_t groupID) : d{new Impl} {
        d->params[Impl::GroupID] = groupID;
    }

    Macro::Macro(const Macro& other) : d{new Impl} {
        d->params = other.d->params;
    }

    Macro::~Macro() {
    }

    auto Macro::operator=(const Macro& other) -> Macro& {
        d->params = other.d->params;
        return *this;
    }

    auto Macro::SetGroupID(int32_t id) -> void {
        d->params[Impl::GroupID] = id;
    }

    auto Macro::GetGroupID() const -> int32_t {
        return d->params[Impl::GroupID];
    }

    auto Macro::SetIndex(int32_t index) -> void {
        d->params[Impl::Index] = index;
    }

    auto Macro::GetIndex() const -> int32_t {
        return d->params[Impl::Index];
    }

    auto Macro::SetCommandID(int32_t commandID) -> void {
        d->params[Impl::CommandID] = commandID;
    }

    auto Macro::GetCommandID() const -> int32_t {
        return d->params[Impl::CommandID];
    }

    auto Macro::SetPosition(int32_t posX, int32_t posY, int32_t posZ) -> void {
        d->params[Impl::PositionX] = posX;
        d->params[Impl::PositionY] = posY;
        d->params[Impl::PositionZ] = posZ;
    }

    auto Macro::GetPositionX() const -> int32_t {
        return d->params[Impl::PositionX];
    }

    auto Macro::GetPositionY() const -> int32_t {
        return d->params[Impl::PositionY];
    }

    auto Macro::GetPositionZ() const -> int32_t {
        return d->params[Impl::PositionZ];
    }

    auto Macro::SetAutoFocusMode(int32_t mode) -> void {
        d->params[Impl::AFMode] = mode;
    }

    auto Macro::GetAutoFocusMode() const -> int32_t {
        return d->params[Impl::AFMode];
    }

    auto Macro::GetParameters() const -> QList<int32_t> {
        return d->params.values();
    }
}
