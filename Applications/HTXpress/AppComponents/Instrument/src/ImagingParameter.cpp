#include "ImagingParameter.h"

namespace HTXpress::AppComponents::Instrument {
    struct ImagingParameter::Impl {
        int32_t sequenceId;
        struct {
            int32_t red;
            int32_t green;
            int32_t blue;
        } intensity;
        int32_t exposure;
        int32_t interval;
        Camera cameraType{ Camera::Internal };
        int32_t ledChannel;
        int32_t emissionFilter;

        auto operator=(const Impl& rhs)->Impl&;
    };

    auto ImagingParameter::Impl::operator=(const Impl& rhs) -> Impl& {
        sequenceId = rhs.sequenceId;
        intensity = rhs.intensity;
        exposure = rhs.exposure;
        interval = rhs.interval;
        cameraType = rhs.cameraType;
        ledChannel = rhs.ledChannel;
        emissionFilter = rhs.emissionFilter;
        return *this;
    }

    ImagingParameter::ImagingParameter() : d{new Impl} {
    }

    ImagingParameter::ImagingParameter(const ImagingParameter& other) : d{new Impl} {
        *d = *other.d;
    }

    ImagingParameter::~ImagingParameter() {
    }

    auto ImagingParameter::operator=(const ImagingParameter& rhs) -> ImagingParameter& {
        *d = *rhs.d;
        return *this;
    }

    auto ImagingParameter::SetSequenceId(int32_t id) -> void {
        d->sequenceId = id;
    }

    auto ImagingParameter::SetIntensity(int32_t red, int32_t green, int32_t blue) -> void {
        d->intensity.red = red;
        d->intensity.green = green;
        d->intensity.blue = blue;
    }

    auto ImagingParameter::SetExposureUSec(int32_t exposure) -> void {
        d->exposure = exposure;
    }

    auto ImagingParameter::SetIntervalUSec(int32_t interval) -> void {
        d->interval = interval;
    }

    auto ImagingParameter::SetCameraType(Instrument::Camera type) -> void {
        d->cameraType = type;
    }

    auto ImagingParameter::SetLEDChannel(int32_t channel) -> void {
        d->ledChannel = channel;
    }

    auto ImagingParameter::SetEmissionFilter(int32_t channel) -> void {
        d->emissionFilter = channel;
    }

    auto ImagingParameter::SequenceId() const -> int32_t {
        return d->sequenceId;
    }

    auto ImagingParameter::IntensityRed() const -> int32_t {
        return d->intensity.red;
    }

    auto ImagingParameter::IntensityGreen() const -> int32_t {
        return d->intensity.green;
    }

    auto ImagingParameter::IntensityBlue() const -> int32_t {
        return d->intensity.blue;
    }

    auto ImagingParameter::ExposureUSec() const -> int32_t {
        return d->exposure;
    }

    auto ImagingParameter::IntervalUSec() const -> int32_t {
        return d->interval;
    }

    auto ImagingParameter::CameraType() const -> Instrument::Camera {
        return d->cameraType;
    }

    auto ImagingParameter::LEDChannel() const -> int32_t {
        return d->ledChannel;
    }

    auto ImagingParameter::EmissionFilter() const -> int32_t {
        return d->emissionFilter;
    }
}
