#pragma once
#include <memory>
#include <QVector>

#include <MCUDefines.h>

namespace HTXpress::AppComponents::Instrument {
    class StreamingMacro {
    public:
        using Pointer = std::shared_ptr<StreamingMacro>;
        using CommandType = TC::MCUControl::MotionCommandType;

    public:
        StreamingMacro();
        ~StreamingMacro();

        auto SetPosition(int32_t xpos, int32_t ypos, int32_t zpos)->void;
        auto SetAutoFoucs(int32_t mode)->void;
        auto SetCommand(CommandType type, const QVector<int32_t>& params)->void;

        auto GetCommandType() const->CommandType;
        auto GetParameter(int32_t index) const->int32_t;
        auto GetPacket() const->QVector<int32_t>;
        auto Str() const->QString;

        //Extra information
        auto SetImageCount(int32_t count)->void;
        auto GetImageCount() const->int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}