#include <QMap>
#include "FeatureMap.h"

namespace HTXpress::AppComponents::Instrument {
    struct FeatureMap::Impl {
        QMap<InstrumentFeature, QVariant> features;
    };

    FeatureMap::FeatureMap() : d{ std::make_unique<Impl>() } {
    }

    FeatureMap::~FeatureMap() {
    }

    auto FeatureMap::GetInstance() -> Pointer {
        static Pointer theInstance{ new FeatureMap() };
        return theInstance;
    }

    auto FeatureMap::Set(InstrumentFeature feature, const QVariant& value) -> void {
        GetInstance()->d->features[feature] = value;
    }

    auto FeatureMap::Get(InstrumentFeature feature) -> QVariant {
        auto itr = GetInstance()->d->features.find(feature);
        if(itr == GetInstance()->d->features.end()) return QVariant();
        return *itr;
    }

    auto FeatureMap::IsSupported(InstrumentFeature feature) -> bool {
        auto itr = GetInstance()->d->features.find(feature);
        if(itr == GetInstance()->d->features.end()) return false;
        if(itr->type() != QMetaType::Bool) return false;
        return itr->toBool();
    }
}
