#pragma once
#include <memory>

#include <Area.h>
#include <PositionGroup.h>

#include "InstrumentConfig.h"
#include "AcquisitionProgress.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandPreviewAcquisition : public Command {
    public:
        using Area = AppEntity::Area;
        using Position = AppEntity::Position;

    public:
        CommandPreviewAcquisition(const Config& config, AcquisitionProgress::Pointer progress = nullptr);
        ~CommandPreviewAcquisition() override;

        auto SetMotions(const QList<UnitMotion>& motions)->void;
        auto SetBFExposure(double bfExposureMSec)->void;

        auto Perform() -> bool override;
        auto GetResponse() -> Response::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}