#pragma once
#include <memory>

#include "CondenserAutofocusProgress.h"
#include "InstrumentConfig.h"
#include "InstrumentDefines.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandCondenserAutoFocus : public Command {
    public:
        explicit CommandCondenserAutoFocus(CondenserAutofocusProgress::Pointer progress);
        ~CommandCondenserAutoFocus() override;

        auto SetConfig(const Config& config)->void;
        auto SetPatternIndex(int32_t pattenIndex)->void;
        auto SetIntensity(int32_t intensity)->void;
        auto SetPositions(const QList<RawPosition>& positions)->void;

        auto Perform() -> bool override;
        auto GetResponse() -> Response::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}