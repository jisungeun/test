#include <QDebug>
#include <QPair>

#include "IlluminationPatterns.h"

namespace HTXpress::AppComponents::Instrument {
    using Index = QPair<AppEntity::ImagingType, AppEntity::TriggerType>;

    struct IlluminationPatterns::Impl {
        QMap<Index, QList<int32_t>> patterns;
    };

    IlluminationPatterns::IlluminationPatterns() : d{new Impl} {
    }

    IlluminationPatterns::~IlluminationPatterns() {
    }

    auto IlluminationPatterns::GetInstance() -> Pointer {
        static Pointer theInstance{ new IlluminationPatterns() };
        return theInstance;
    }

    auto IlluminationPatterns::Set(const AppEntity::ImagingType imagingType, const AppEntity::TriggerType trigger, const QList<int32_t>& patterns) -> void {
        GetInstance()->d->patterns[{imagingType, trigger}] = patterns;
        qDebug() << "imagingType=" << imagingType._to_string()
                 << " trigger=" << trigger._to_string()
                 << " patterns=" << patterns;
    }

    auto IlluminationPatterns::Get(const AppEntity::ImagingType imagingType, const AppEntity::TriggerType trigger, int32_t channel) -> int32_t {
        const auto itr = GetInstance()->d->patterns.find({imagingType, trigger});
        if(itr == GetInstance()->d->patterns.end()) return -1;

        const auto& patterns = itr.value();
        if((channel < 0) || (channel >= patterns.length())) { //for HT/BF
            return patterns.at(0);
        }

        return patterns.at(channel);
    }
}