#define LOGGER_TAG "[Instrument]"

#include <QMap>
#include <QElapsedTimer>
#include <QCoreApplication>
#include <QTimer>
#include <QMutex>
#include <QWaitCondition>

#include <TCLogger.h>

#include "CameraManager.h"
#include "SimulationFeeder.h"
#include "SimulationOptionDialog.h"
#include "SimulationOptions.h"
#include "InstrumentSimulator.h"

namespace HTXpress::AppComponents::Instrument {
    enum class ImagingMode {
        Idle,
        Live,
        Acquisition,
        Preview
    };

    class Progress {
    public:
        Progress() {}
        Progress(const AppEntity::ImagingSequence::Pointer& sequence, const int32_t positionCount) {
            const auto timeCount = sequence->GetTimeCount();
            const auto modalityCount = sequence->GetModalityCount();
            for(int idx=0; idx<modalityCount; idx++) {
                auto cond = sequence->GetImagingCondition(idx);
                totalCount += cond->GetImageCount();
            }

            totalCount = totalCount * positionCount;
        }

        Progress(int32_t count) {
            totalCount = count;
        }

        auto Update() -> void {
            currentIndex = currentIndex + 1;
        }

        auto Check() const -> double {
            return std::min<double>(1.0, currentIndex / static_cast<double>(totalCount));
        }

        auto AcquiredCount() const->int32_t {
            return currentIndex;
        }

        auto TotalCount() const->int32_t {
            return totalCount;
        }

    private:
        int32_t totalCount{ 0 };
        int32_t currentIndex{ 0 };
    };

    struct InstrumentSimulator::Impl {
        CameraManager::Pointer cameraManager{ nullptr };

        struct {
            QElapsedTimer timer;
            bool initialized{ false };
        } init;

        QMap<Axis,RawPosition> axisCurrentPosition{
            {Axis::AxisX, 0},
            {Axis::AxisY, 0},
            {Axis::AxisZ, 0},
            {Axis::AxisC, 0}
        };
        bool axisMoving{ false };

        QMap<Axis, bool> axisJogActive {
            {Axis::AxisX, false},
            {Axis::AxisY, false},
            {Axis::AxisZ, false},
            {Axis::AxisC, false}
        };

        QMap<Axis, int32_t> axisJogDirection {
            {Axis::AxisX, 1},
            {Axis::AxisY, 1},
            {Axis::AxisZ, 1},
            {Axis::AxisC, 1}
        };
        QTimer jogTimer;

        SimulationFeeder imageFeeder;

        struct {
            AppEntity::ImagingSequence::Pointer sequence{ nullptr };
            QList<AppEntity::PositionGroup> positions;
            int32_t modalityIndex{ 0 };
            int32_t modalityCount{ 1 };
            int32_t posIndex{ 0 };
            uint32_t positionCount{ 1 };
            int32_t imageIndex{ 0 };
            int32_t imageCount{ 1000 };
            int32_t timeIndex{ 0 };
            int32_t timeCount{ 1 };
            Progress progress;
        } acquisition;

        struct {
            bool enabled{ false };
        } af;

        struct {
            LiveMode mode{ LiveMode::BrightField };
            ImagingParameter parameter;
        } live;

        QMap<AppEntity::FLFilter, int32_t> emFilters;
        SimulationOptionDialog optionDialog;
        QMutex mutex;
        QWaitCondition waitCond;
        ImagingMode imagingMode{ ImagingMode::Idle };
        bool running{ true };

        Config config;

        auto showOptionDialog()->void;
        auto setImagePath()->bool;
        auto getPosition(int32_t index, AppEntity::Position& position)->bool;
    };

    auto InstrumentSimulator::Impl::showOptionDialog() -> void {
        optionDialog.exec();
    }

    auto InstrumentSimulator::Impl::setImagePath() -> bool {
        auto path = SimulationOptions::GetInstance()->GetImagePath();
        if(path.isEmpty()) {
            showOptionDialog();
            path = SimulationOptions::GetInstance()->GetImagePath();
            if(path.isEmpty()) return false;
        }

        imageFeeder.SetSimulationPath(path);
        return true;
    }

    auto InstrumentSimulator::Impl::getPosition(int32_t index, AppEntity::Position& position)->bool {
        int32_t totalCount = 0;
        for(auto posGroup : acquisition.positions) {
            auto posCount = posGroup.GetCount();
            if(index >= static_cast<int32_t>(totalCount + posCount)) {
                totalCount += posCount;
                continue;
            }

            position = posGroup.GetPositions().at(index - totalCount);
            return true;;
        }

        return false;
    }

    InstrumentSimulator::InstrumentSimulator() : QThread(), Instrument(), d{new Impl} {
        d->cameraManager = CameraManager::GetInstance(CameraType::NoCamera);

        start();

        connect(&d->jogTimer, SIGNAL(timeout()), this, SLOT(onUpdateJog()));
    }

    InstrumentSimulator::~InstrumentSimulator() {
        d->mutex.lock();
        d->running = false;
        d->waitCond.wakeAll();
        d->mutex.unlock();

        wait();
    }

    auto InstrumentSimulator::SetConfig(const Config& config) -> void {
        d->config = config;

        d->showOptionDialog();

        d->cameraManager->SetImagingCamera("000000");
        d->cameraManager->SetCondenserCamera("000001");
    }

    auto InstrumentSimulator::Initialize() -> bool {
        if(d->init.initialized) return true;
        d->init.timer.start();
        return true;
    }

    auto InstrumentSimulator::GetInitializationProgress() const -> std::tuple<bool,double> {
        const auto elapsed = static_cast<double>(d->init.timer.elapsed());
        const auto progress = (std::min(5000.0, elapsed) / 5000.0);
        d->init.initialized = (progress >= 1.0);
        return std::make_tuple(true, progress);
    }

    auto InstrumentSimulator::CheckInitialized() const -> bool {
        return d->init.initialized;
    }

    auto InstrumentSimulator::CleanUp() -> void {
    }

    auto InstrumentSimulator::InstallImagePort(ImageSource source, ImagePort* port) -> void {
        d->cameraManager->InstallImagePort(source, port);
    }

    auto InstrumentSimulator::UninstallImagePort(ImageSource source, ImagePort* port) -> void {
        d->cameraManager->UninstallImagePort(source, port);
    }

    auto InstrumentSimulator::SetFLEmission(const QMap<AppEntity::FLFilter, int32_t>& filters) -> void {
        d->emFilters = filters;
    }

    auto InstrumentSimulator::Move(Axis axis, RawPosition targetMM) -> bool {
        const auto dist = targetMM - d->axisCurrentPosition[axis];
        const auto timeoutMSec = static_cast<int64_t>((std::abs(dist)/20.0)*1000);
        const auto unitTimeMSec = 200;
        const auto unitDist = dist / unitTimeMSec;

        d->axisMoving = true;

        QElapsedTimer timer;
        timer.start();
        while(timer.elapsed() < timeoutMSec) {
            QThread::msleep(200);
            d->axisCurrentPosition[axis] = d->axisCurrentPosition[axis] + unitDist;
        }

        d->axisMoving = false;

        d->axisCurrentPosition[axis] = targetMM;

        return true;
    }

    auto InstrumentSimulator::MoveXY(RawPosition targetXMM, RawPosition targetYMM) -> bool {
        QLOG_INFO() << "MoveXY Target (X,Y) = (" << targetXMM << "," << targetYMM << ")mm";
        const auto distX = targetXMM - d->axisCurrentPosition[Axis::AxisX];
        const auto distY = targetYMM - d->axisCurrentPosition[Axis::AxisY];
        QLOG_INFO() << "> Dist (X,Y) = (" << distX << "," << distY << ")mm";
        const auto timeoutMSec = static_cast<int64_t>((std::max(std::abs(distX),std::abs(distY))/20.0)*1000);
        const auto unitTimeMSec = 200;
        const auto unitDistX = distX / unitTimeMSec;
        const auto unitDistY = distY / unitTimeMSec;
        QLOG_INFO() << "> timeout = " << timeoutMSec << "msec";
        QLOG_INFO() << "> moving distance per " << unitTimeMSec << "msec is (X,Y) = (" << unitDistX << "," << unitDistY << ")mm";

        d->axisMoving = true;

        QElapsedTimer timer;
        timer.start();
        while(timer.elapsed() < timeoutMSec) {
            QThread::msleep(200);
            d->axisCurrentPosition[Axis::AxisX] = d->axisCurrentPosition[Axis::AxisX] + unitDistX;
            d->axisCurrentPosition[Axis::AxisY] = d->axisCurrentPosition[Axis::AxisY] + unitDistY;
        }

        d->axisMoving = false;

        d->axisCurrentPosition[Axis::AxisX] = targetXMM;
        d->axisCurrentPosition[Axis::AxisY] = targetYMM;

        return true;
    }

    auto InstrumentSimulator::MoveZtoSafePos() -> bool {
        return Move(Axis::AxisZ, d->config.SafeZPositionMM());
    }

    auto InstrumentSimulator::IsMoving() const -> MotionStatus {
        MotionStatus status;
        status.moving = d->axisMoving;
        return status;
    }

    auto InstrumentSimulator::GetPosition(Axis axis, RawPosition& pos) -> bool {
        pos = d->axisCurrentPosition[axis];
        return true;
    }

    auto InstrumentSimulator::GetPositionXY(RawPosition& xPos, RawPosition& yPos) -> bool {
        xPos = d->axisCurrentPosition[Axis::AxisX];
        yPos = d->axisCurrentPosition[Axis::AxisY];
        return true;
    }

    auto InstrumentSimulator::GetPositionXYZ(RawPosition& xPos, RawPosition& yPos, RawPosition& zPos) -> bool {
        xPos = d->axisCurrentPosition[Axis::AxisX];
        yPos = d->axisCurrentPosition[Axis::AxisY];
        zPos = d->axisCurrentPosition[Axis::AxisZ];
        return true;
    }

    auto InstrumentSimulator::StartJog(Axis axis, bool plusDirection) -> bool {
        d->axisJogDirection[axis] = (plusDirection)?1:-1;
        d->axisJogActive[axis] = true;
        return true;
    }

    auto InstrumentSimulator::StopJog() -> bool {
        for(auto axisID : Axis::_values()) {
            d->axisJogActive[axisID] = false;
        }
        return true;
    }

    auto InstrumentSimulator::SetJogRange(Axis axis, const PositionRange& range) -> void {
        Q_UNUSED(axis)
        Q_UNUSED(range)
    }

    auto InstrumentSimulator::EnableJoystick() -> bool {
        return true;
    }

    auto InstrumentSimulator::DisableJoystick() -> bool {
        return true;
    }

    auto InstrumentSimulator::CheckJoystick() -> JoystickStatus {
        return JoystickStatus();
    }

    auto InstrumentSimulator::ChangeFOV(int32_t xPixels, int32_t yPixels, int32_t& offsetX, int32_t& offsetY) -> bool {
        Q_UNUSED(xPixels)
        Q_UNUSED(yPixels)
        Q_UNUSED(offsetX)
        Q_UNUSED(offsetY)
        return true;
    }

    auto InstrumentSimulator::StartAcquisition(const AppEntity::ImagingSequence::Pointer sequence,
                                               const QList<AppEntity::PositionGroup>& positions,
                                               bool useAutoFocus,
                                               AppEntity::WellIndex startingWellIndex,
                                               double focusReadyMM,
                                               bool useMultiDishHolder) -> bool {
        Q_UNUSED(useAutoFocus)
        Q_UNUSED(startingWellIndex)
        Q_UNUSED(focusReadyMM)
        Q_UNUSED(useMultiDishHolder)

        QMutexLocker locker(&d->mutex);
        d->imagingMode = ImagingMode::Acquisition;
        d->acquisition.sequence = sequence;
        d->acquisition.positions = positions;
        d->acquisition.positionCount = 0;
        d->acquisition.modalityCount = sequence->GetModalityCount();
        d->acquisition.timeCount = sequence->GetTimeCount();
        d->acquisition.modalityIndex = 0;
        d->acquisition.posIndex = 0;
        d->acquisition.timeIndex = 0;
        d->waitCond.wakeOne();

        std::for_each(positions.begin(), positions.end(), [&](const AppEntity::PositionGroup& posGroup) {
            d->acquisition.positionCount += posGroup.GetCount();
        });

        d->acquisition.progress = Progress(sequence, d->acquisition.positionCount);

        QLOG_INFO() << "Start Acquisition - Sequences: " << sequence->ToStr() << " Positions: " << d->acquisition.positionCount;

        auto firstGroup = d->acquisition.positions.at(0);
        QLOG_INFO() << "Move to the first pos = (" << firstGroup.GetPosition(0, 0).toMM().x << "," << firstGroup.GetPosition(0, 0).toMM().y << ")";

        return d->setImagePath();
    }

    auto InstrumentSimulator::StopAcquisition() -> bool {
        QMutexLocker locker(&d->mutex);
        d->imagingMode = ImagingMode::Idle;
        return true;
    }

    auto InstrumentSimulator::CheckAcquisitionProgress() const -> std::tuple<bool,double,AppEntity::Position> {
        QMutexLocker locker(&d->mutex);
        return std::make_tuple(true, d->acquisition.progress.Check(),
                               AppEntity::Position::fromMM(d->axisCurrentPosition[Axis::AxisX],
                                                           d->axisCurrentPosition[Axis::AxisY],
                                                           d->axisCurrentPosition[Axis::AxisZ]));
    }

    auto InstrumentSimulator::GetRemainCount(ImageSource source) const -> int32_t {
        Q_UNUSED(source)
        return 0;
    }

    auto InstrumentSimulator::StartLive(LiveMode mode, const ImagingParameter& param, bool onlyUpdateParameter) -> bool {
        Q_UNUSED(onlyUpdateParameter)

        QMutexLocker locker(&d->mutex);
        d->imagingMode = ImagingMode::Live;
        d->waitCond.wakeOne();

        d->live.mode = mode;
        d->live.parameter = param;

        if(!d->setImagePath()) return false;
        if(mode==+LiveMode::BrightField) d->imageFeeder.StartLiveBF();
        else if(mode==+LiveMode::Holography) d->imageFeeder.StartAcquisitionHT();
        else {
            const auto channel = [&]()->int32_t {
                if(param.IntensityRed() > 0) return 0;
                if(param.IntensityGreen() > 0) return 1;
                if(param.IntensityBlue() > 0) return 2;
                return 0;
            }();
            d->imageFeeder.StartLiveFL(channel);
        }

        return true;
    }

    auto InstrumentSimulator::StopLive() -> bool {
        QMutexLocker locker(&d->mutex);
        d->imagingMode = ImagingMode::Idle;
        return true;
    }

    auto InstrumentSimulator::ResumeLive() -> bool {
        return StartLive(d->live.mode, d->live.parameter);
    }

    auto InstrumentSimulator::StartLiveWithTrigger(const ImagingParameter& param,
                                                   int32_t triggerCount, int32_t triggerInterval, int32_t pulseWidth,
                                                   bool useHCG, double gain) -> bool {
        Q_UNUSED(param)
        Q_UNUSED(triggerCount)
        Q_UNUSED(triggerInterval)
        Q_UNUSED(pulseWidth)
        Q_UNUSED(useHCG)
        Q_UNUSED(gain)

        QMutexLocker locker(&d->mutex);
        d->imagingMode = ImagingMode::Preview;
        d->acquisition.progress = Progress(100);
        d->waitCond.wakeOne();

        d->live.mode = LiveMode::BrightField;

        if (!d->setImagePath()) return false;
        d->imageFeeder.StartLiveBF();

        return true;
    }

    auto InstrumentSimulator::StartPreviewAcquisition(const QList<UnitMotion>& motions, double bfExposure) -> bool {
        Q_UNUSED(motions)
        Q_UNUSED(bfExposure)

        QMutexLocker locker(&d->mutex);
        d->imagingMode = ImagingMode::Preview;
        d->acquisition.progress = Progress(100);
        d->waitCond.wakeOne();

        d->live.mode = LiveMode::BrightField;

        if(!d->setImagePath()) return false;
        d->imageFeeder.StartLiveBF();

        return true;
    }

    auto InstrumentSimulator::EnableAutoFocus(bool enable) -> std::tuple<bool,bool> {
        d->af.enabled = enable;
        return std::make_tuple(true, true);
    }

    auto InstrumentSimulator::AutoFocusEnabled() const -> bool {
        return d->af.enabled;
    }

    auto InstrumentSimulator::PerformAutoFocus() -> std::tuple<bool,bool> {
        return std::make_tuple(true, true);
    }

    auto InstrumentSimulator::ReadAFSensorValue() -> int32_t {
        return 0;
    }

    auto InstrumentSimulator::SetBestFocusCurrent(int32_t& value) -> bool {
        Q_UNUSED(value)
        return true;
    }

    auto InstrumentSimulator::SetBestFocus(int32_t value) -> bool {
        Q_UNUSED(value)
        return true;
    }

    auto InstrumentSimulator::ScanFocus(double startMm, double distMm, int32_t count) -> QList<int32_t> {
        Q_UNUSED(startMm)
        Q_UNUSED(distMm)

        QList<int32_t> values;
        for(auto idx=0; idx<count; idx++) {
            values.push_back(idx);
        }
        return values;
    }

    auto InstrumentSimulator::SetAcquisitionPattenIndex(const AppEntity::ImagingType imagingType,
                                                        const AppEntity::TriggerType trigger,
                                                        const QList<int32_t>& index) -> void {
        Q_UNUSED(imagingType)
        Q_UNUSED(trigger)
        Q_UNUSED(index)
    }

    auto InstrumentSimulator::SetAcquisitionChannelIndex(const AppEntity::ImagingType imagingType,
                                                         const AppEntity::TriggerType trigger,
                                                         const int32_t ledChannel,
                                                         const QList<int32_t>& index) -> void {
        Q_UNUSED(imagingType)
        Q_UNUSED(trigger)
        Q_UNUSED(ledChannel)
        Q_UNUSED(index)
    }

    auto InstrumentSimulator::StartCondenserAutoFocus(int32_t patternIndex, int32_t intensity) -> bool {
        Q_UNUSED(patternIndex)
        Q_UNUSED(intensity)
        return true;
    }

    auto InstrumentSimulator::CheckCondenserAutoFocusProgress() const -> std::tuple<bool, double> {
        return std::make_tuple(false, 1.0);
    }

    auto InstrumentSimulator::FinishCondenserAutoFocus() -> void {
    }

    auto InstrumentSimulator::StartHTIlluminationCalibration(const QList<int32_t>& intensityList,
                                                             const AppEntity::ImagingConditionHT::Pointer imgCond) -> bool {
        Q_UNUSED(intensityList)
        Q_UNUSED(imgCond)
        return true;
    }

    auto InstrumentSimulator::CheckHTIlluminationCalibrationProgress() const -> std::tuple<bool, double> {
        return std::make_tuple(false, 1.0);
    }

    auto InstrumentSimulator::FinishHTIlluminationCalibration() -> void {
    }

    auto InstrumentSimulator::IsSupported(InstrumentFeature feature) const -> bool {
        Q_UNUSED(feature)
        return true;
    }

    auto InstrumentSimulator::GetError() const -> QString {
        return "Not specified (simulation)";
    }

    void InstrumentSimulator::onUpdateJog() {
        for(auto axis : Axis::_values()) {
            if(!d->axisJogActive[axis]) continue;
            d->axisCurrentPosition[axis] = d->axisCurrentPosition[axis] + (4.0 * d->axisJogDirection[axis]);
        }
    }

    void InstrumentSimulator::run() {
        while(d->running) {
            QMutexLocker locker(&d->mutex);
            if(d->imagingMode == ImagingMode::Idle) {
                d->waitCond.wait(&d->mutex);
                continue;
            }

            if(d->imagingMode == ImagingMode::Live) {
                auto imageSink = d->cameraManager->GetImageSink(ImageSource::ImagingCamera);
                imageSink->Send(d->imageFeeder.FeedImage());
                d->waitCond.wait(&d->mutex, 100);
                continue;
            }

            if(d->imagingMode == ImagingMode::Preview) {
                auto imageSink = d->cameraManager->GetImageSink(ImageSource::ImagingCamera);
                imageSink->Send(d->imageFeeder.FeedImage());
                d->waitCond.wait(&d->mutex, 100);
                d->acquisition.progress.Update();
                continue;
            }

            if(d->imagingMode == ImagingMode::Acquisition) {
                if(d->axisMoving) {
                    d->waitCond.wait(&d->mutex, 100);
                    continue;
                }

                //첫 번째 이미지 획득 하기 전에 Imaging Condition 확인하기
                if(d->acquisition.imageIndex==0) {
                    const auto cond = d->acquisition.sequence->GetImagingCondition(d->acquisition.modalityIndex);
                    const auto modality = cond->GetModality();
                    switch(modality) {
                    case AppEntity::Modality::HT:
                        d->imageFeeder.StartAcquisitionHT();
                        break;
                    case AppEntity::Modality::FL:
                        d->imageFeeder.StartAcquisitionFL(std::dynamic_pointer_cast<AppEntity::ImagingConditionFL>(cond)->GetChannels());
                        break;
                    case AppEntity::Modality::BF:
                        d->imageFeeder.StartAcquisitionBF(
                            std::dynamic_pointer_cast<AppEntity::ImagingConditionBF>(cond)->GetColorMode());
                        break;
                    }

                    d->acquisition.imageCount = cond->GetImageCount();

                    QLOG_INFO() << "Acquisition Modality:" << modality._to_string() << " ImageCount:" << d->acquisition.imageCount;
                }

                auto imageSink = d->cameraManager->GetImageSink(ImageSource::ImagingCamera);
                imageSink->Send(d->imageFeeder.FeedImage());
                d->acquisition.imageIndex += 1;

                d->acquisition.progress.Update();

                const auto acquired = d->acquisition.progress.AcquiredCount();
                if((acquired%10) == 0) {
                    QLOG_INFO() << "Images acquired = " << d->acquisition.progress.AcquiredCount()
                                << "/" << d->acquisition.progress.TotalCount()
                                << " (" << d->acquisition.progress.Check()*100 << "%)";
                }

                if(d->acquisition.imageIndex == d->acquisition.imageCount) {
                    d->acquisition.imageIndex = 0;
                    d->acquisition.modalityIndex += 1;
                } else {
                    continue;
                }

                if(d->acquisition.modalityIndex == d->acquisition.modalityCount) {
                    d->acquisition.modalityIndex = 0;
                    d->acquisition.posIndex += 1;

                    AppEntity::Position pos;
                    if(d->getPosition(d->acquisition.posIndex, pos)) {
                        QLOG_INFO() << "Move to the next pos = (" << pos.toMM().x << "," << pos.toMM().y << ")";
                        MoveXY(pos.toMM().x, pos.toMM().y);
                    }
                } else {
                    continue;
                }

                if(d->acquisition.posIndex == static_cast<int32_t>(d->acquisition.positionCount)) {
                    d->acquisition.posIndex = 0;
                    d->acquisition.timeIndex += 1;
                } else {
                    continue;
                }

                if(d->acquisition.timeIndex == d->acquisition.timeCount) {
                    d->acquisition.timeIndex = 0;
                    d->imagingMode = ImagingMode::Live;
                } else {
                    QLOG_INFO() << "Pause acquisition for " << 2 << " seconds";
                    QThread::msleep(2000);
                }
            }
        }
    }
}
