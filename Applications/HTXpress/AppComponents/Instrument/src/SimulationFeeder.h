#pragma once
#include <memory>
#include <QList>

#include <CameraImage.h>

//Simulation Folder Layout
//
//Top
//  - Config
//  - Live
//    - BF
//    - FL
//      - CH0
//      - CH1
//      - CH2
//  - Acquisition
//    - HT
//    - BF
//      - Gray
//      - Color
//    - FL
//       - CH0
//       - CH1
//       - CH2

namespace HTXpress::AppComponents::Instrument {
    class SimulationFeeder {
    public:
        SimulationFeeder();
        ~SimulationFeeder();

        auto SetSimulationPath(const QString& path)->void;

        auto StartLiveBF()->void;
        auto StartLiveFL(int32_t channel)->void;

        auto StartAcquisitionHT()->int32_t;
        auto StartAcquisitionFL(const QList<int32_t>& channels)->int32_t;
        auto StartAcquisitionBF(bool colorMode)->int32_t;

        auto FeedImage() const->TC::CameraControl::Image::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}