#pragma once
#include <memory>
#include <QString>

namespace HTXpress::AppComponents::Instrument {
    class InitializeProgress {
    public:
        using Pointer = std::shared_ptr<InitializeProgress>;

    public:
        InitializeProgress();
        ~InitializeProgress();

        auto SetProgress(double progress)->void;
        auto GetProgress() const->double;

        auto SetError(const QString& message)->void;
        auto GetError() const->bool;
        auto GetErrorMessage() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}