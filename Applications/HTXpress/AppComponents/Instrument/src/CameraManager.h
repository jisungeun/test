#pragma once
#include <memory>

#include <CameraControlFactory.h>

#include "ImageSink.h"
#include "InstrumentDefines.h"

namespace HTXpress::AppComponents::Instrument {
    using CameraType = TC::CameraControl::CameraControlFactory::Controller;
    using IImageSink = TC::CameraControl::IImageSink;
    using CameraControl = TC::CameraControl::CameraControl;

    class CameraManager {
    public:
		using Pointer = std::shared_ptr<CameraManager>;

    protected:
        explicit CameraManager(CameraType type);

    public:
        ~CameraManager();

        static auto GetInstance(CameraType type = CameraType::PointGrey)->Pointer;

        auto CleanUp()->void;

        auto SetImagingCamera(const QString& serial)->void;
        auto SetCondenserCamera(const QString& serial)->void;

        auto GetImagingCamera() const->CameraControl::Pointer;
        auto GetCondenserCamera() const->CameraControl::Pointer;

        auto GetImageSink(ImageSource source) const->ImageSink::Pointer;

        auto InstallImagePort(ImageSource source, ImagePort* port)->void;
        auto UninstallImagePort(ImageSource source, ImagePort* port)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}


        