#include "MotionCommand.h"
#include "MacroLive.h"

namespace HTXpress::AppComponents::Instrument::MacroLive {

    static struct {
        ImagingParameter param;
        QVector<int32_t> pos{0, 0, 0};
        int32_t afMode;
    } latest;

    auto Build(const ImagingParameter& param, bool enable)->QVector<int32_t> {
        QVector<int32_t> cmdParams(8);
        cmdParams[0] = enable;
        cmdParams[1] = param.SequenceId();
        cmdParams[2] = param.IntensityRed();
        cmdParams[3] = param.IntensityGreen();
        cmdParams[4] = param.IntensityBlue();
        cmdParams[5] = param.ExposureUSec();
        cmdParams[6] = param.IntervalUSec();
        cmdParams[7] = param.CameraType();
        return cmdParams;
    }

    auto Start(const ImagingParameter& param, const QVector<int32_t>& pos, int32_t afMode) -> QList<StreamingMacro::Pointer> {
        latest.param = param;
        latest.pos = pos;
        latest.afMode = afMode;

        QList<StreamingMacro::Pointer> macros;

        macros.push_back([=]()->StreamingMacro::Pointer {
            auto macro = std::make_shared<StreamingMacro>();
            macro->SetPosition(pos[0], pos[1], pos[2]);
            macro->SetAutoFoucs(afMode);
            macro->SetCommand(StreamingMacro::CommandType::ChangeFilter, {param.EmissionFilter()});
            return macro;
        }());

        macros.push_back([=]()->StreamingMacro::Pointer {
            auto macro = std::make_shared<StreamingMacro>();
            macro->SetPosition(pos[0], pos[1], pos[2]);
            macro->SetAutoFoucs(afMode);
            macro->SetCommand(StreamingMacro::CommandType::PatternSequence, Build(param, true));
            return macro;
        }());

        return macros;
    }

    auto Stop(const ImagingParameter& param, const QVector<int32_t>& pos, int32_t afMode) -> QList<StreamingMacro::Pointer> {
        QList<StreamingMacro::Pointer> macros;

        auto macro = std::make_shared<StreamingMacro>();
        macro->SetPosition(pos[0], pos[1], pos[2]);
        macro->SetAutoFoucs(afMode);
        macro->SetCommand(StreamingMacro::CommandType::PatternSequence, Build(param, false));
        macros.push_back(macro);

        return macros;

    }

    auto Stop() -> QList<StreamingMacro::Pointer> {
        return Stop(latest.param, latest.pos, latest.afMode);
    }
}
