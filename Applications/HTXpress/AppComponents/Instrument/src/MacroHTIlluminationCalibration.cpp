#define LOGGER_TAG "[MacroHTIlluminationCalibration]"
#include <TCLogger.h>
#include <MCUFactory.h>

#include "PositionAdjuster.h"
#include "EmissionFilters.h"
#include "IlluminationPatterns.h"
#include "AcquisitionChannels.h"
#include "MacroHTIlluminationCalibration.h"

namespace HTXpress::AppComponents::Instrument::MacroHTIlluminationCalibration {
    using ImagingType = AppEntity::ImagingType;

    constexpr auto Start = true;
    constexpr auto Stop = false;
    constexpr auto LedChannel = 2;

    auto BuildChangeFilter() -> QVector<int32_t> {
        QVector<int32_t> cmdParams(1);
        cmdParams[0] = EmissionFilters::GetVoidChannel();
        return cmdParams;
    }

    auto BuildPatternSequence(const ImagingConditionHT::Pointer& condition,
                              const int32_t acqIntensity,
                              bool enable) -> QVector<int32_t> {
        using TriggerType = AppEntity::TriggerType;

        const auto seqId = IlluminationPatterns::Get(ImagingType::HT3D, AppEntity::TriggerType::Trigger, LedChannel);
        const auto acqCh = AcquisitionChannels::Get(ImagingType::HT3D, TriggerType::Trigger, LedChannel);

        QVector<int32_t> cmdParams(8);
        cmdParams[0] = enable;                                              //enable/disable
        cmdParams[1] = seqId;                                               //sequence id
        cmdParams[2] = (acqCh != 0) ? 0 : acqIntensity;                     //intensity of Red LED on DLPC
        cmdParams[3] = (acqCh != 1) ? 0 : acqIntensity;                     //intensity of Green LED on DLPC
        cmdParams[4] = (acqCh != 2) ? 0 : acqIntensity;                     //intensity of Blue LED on DLPC
        cmdParams[5] = condition->GetExposure(LedChannel);                  //exposure time
        cmdParams[6] = condition->GetExposure(LedChannel) + 8000;           //interval time, 8msec = transfer time....
        cmdParams[7] = Camera::Internal;

        return cmdParams;
    }

    auto BuildTriggerWithMotion(const ImagingConditionHT::Pointer& condition, const Config& config) -> std::tuple<QVector<int32_t>, int32_t> {
        const auto stepDist = std::max<int32_t>(1, condition->GetSliceStep());
        const auto stepCount = condition->GetSliceCount();
        const auto startPos = condition->GetSliceStart();

        const auto startOffset = config.GetHTTriggerStartOffsetPulse();
        const auto endOffset = config.GetHTTriggerEndOffsetPusle();
        const auto triggerInterval = std::max<int32_t>(1, condition->GetSliceStep());
        const auto pulseWidth = config.GetHTTriggerPulseWidthUSec();
        const auto readOut = config.GetHTTriggerReadOutUSec();
        const auto exposure = config.GetHTTriggerExposureUSec();
        const auto intervalMargin = config.GetHTTirggerIntervalMarginUSec();
        const auto accelFactor = config.GetHTTriggerAccelerationFactor();

        const auto motionSpeed = (stepDist * 1000000) / ((readOut + exposure) * 4 + intervalMargin);

        const auto triggerStartPos = startPos;
        const auto triggerEndPos = startPos + (stepDist * (stepCount - 1)) + 1;

        QVector<int32_t> cmdParams(9);

        cmdParams[0] = 2;                                   //Z axis - FIXED
        cmdParams[1] = triggerStartPos - startOffset;       //Motion start pos (relative to the current)
        cmdParams[2] = triggerEndPos + endOffset;           //Motion end pos (relative to the current)
        cmdParams[3] = triggerStartPos;                     //Start pos (relative to the current)
        cmdParams[4] = triggerEndPos;                       //End pos (relative to the current)
        cmdParams[5] = triggerInterval;                     //Step amount
        cmdParams[6] = pulseWidth;                          //Pulse width (usec)
        cmdParams[7] = motionSpeed;                         //Motion speed (pulse per second)
        cmdParams[8] = motionSpeed * accelFactor;           //Motion acceleration (pulse per second^2)

        return std::make_tuple(cmdParams, stepCount*4);
    }

    auto Setup(const QList<int32_t>& intensityList,
               const QList<RawPosition>& positions,
               const ImagingConditionHT::Pointer imgCond,
               const Config& config) -> QList<StreamingMacro::Pointer> {
        using CommandType = StreamingMacro::CommandType;

        QList<StreamingMacro::Pointer> macros;

        struct Command {
            int32_t groupIndex{ 0 };
            CommandType command;
            QVector<int32_t> params;
            int32_t imageCount{ 0 };
        };
        QList<Command> patternSeqCommands;

        int32_t idx = 0;

        patternSeqCommands.push_back({idx, CommandType::ChangeFilter, BuildChangeFilter()});
        for(auto intensity : intensityList) {
            patternSeqCommands.push_back({idx, CommandType::PatternSequence, BuildPatternSequence(imgCond, intensity, Start)});
            auto [cmdParams, imgCount] = BuildTriggerWithMotion(imgCond, config);
            patternSeqCommands.push_back({idx, CommandType::TriggerWithMotion, cmdParams, imgCount});
            patternSeqCommands.push_back({idx, CommandType::PatternSequence, BuildPatternSequence(imgCond, intensity, Stop)});
            idx++;
        }

        auto convert = [config](Axis axis, RawPosition rawPos)->int32_t {
            auto resolution = config.AxisResolutionPPM(axis);
            auto compensation = config.AxisCompensation(axis);
            return static_cast<int32_t>(resolution * rawPos * compensation);
        };

        const auto xPulses = convert(Axis::AxisX, positions.at(0));
        const auto yPulses = convert(Axis::AxisY, positions.at(1));
        const auto zPulses = convert(Axis::AxisZ, positions.at(2));

        for(const auto& cmd : patternSeqCommands) {
            auto afMode = [&]()->int32_t {
                if(cmd.groupIndex != 0) return 0;
                return 1;
            }();

            auto zPos = [&]()->int32_t {
                if(cmd.groupIndex != 0) return -1;
                return zPulses;
            }();

            auto macro = std::make_shared<StreamingMacro>();
            macro->SetPosition(xPulses, yPulses, zPos);
            macro->SetAutoFoucs(afMode);
            macro->SetCommand(cmd.command, cmd.params);
            macro->SetImageCount(cmd.imageCount);   //Only trigger commands have image count
            macros.push_back(macro);
        }

        return macros;
    }
}