#include <QStringList>
#include <QDir>
#include <QImage>
#include <QMutex>

#include "SimulationFeeder.h"

namespace HTXpress::AppComponents::Instrument {
    struct SimulationFeeder::Impl {
        QString topPath;

        enum class Mode {
            Live,
            Acquisition
        } mode{ Mode::Live };

        struct {
            int32_t index{ 0 };
            int32_t count{ 0 };
            int32_t channels{ 1 };
            QList<QStringList> files;   //QStringList per channel
            QMutex mutex;
        } status;

        auto UpdateImageList(const QStringList& path)->void;
        auto LoadImage(int32_t index)->TC::CameraControl::Image::Pointer;
    };

    auto SimulationFeeder::Impl::UpdateImageList(const QStringList& pathList) -> void {
        QMutexLocker locker(&status.mutex);

        status.files.clear();
        status.count = 0;

        for(auto path : pathList) {
            auto dir = QDir(path);
            auto files = dir.entryList({"*.png"}, QDir::Files, QDir::SortFlag::Name);

            status.files.append(QStringList());
            auto& channelFileList = status.files.last();

            std::for_each(files.begin(), files.end(), [&](const QString& file){
                channelFileList.push_back(QString("%1/%2").arg(path).arg(file));
            });

            status.count += files.length();
        }

        status.channels = status.files.length();
    }

    auto SimulationFeeder::Impl::LoadImage(int32_t index) -> TC::CameraControl::Image::Pointer {
        QMutexLocker locker(&status.mutex);
        if(status.files.isEmpty()) return nullptr;

        const auto channel = index % status.channels;
        const auto imgIdx = index / status.channels;

        auto image = QImage(status.files.at(channel).at(imgIdx));
        const auto sizeX = image.width();
        const auto sizeY = image.height();
        const auto buffSize = sizeX * sizeY;

        std::shared_ptr<uint8_t> buffer{ new uint8_t[buffSize] };
        for(int rowIdx=0; rowIdx<sizeY; rowIdx++) {
            memcpy_s(buffer.get()+(rowIdx*sizeX), sizeX, image.scanLine(rowIdx), sizeX);
        }
        return std::make_shared<TC::CameraControl::Image>(sizeX, sizeY, sizeX*sizeY, 8, buffer.get());
    }

    SimulationFeeder::SimulationFeeder() :d{new Impl} {
    }

    SimulationFeeder::~SimulationFeeder() {
    }

    auto SimulationFeeder::SetSimulationPath(const QString& path) -> void {
        d->topPath = path;
    }

    auto SimulationFeeder::StartLiveBF() -> void {
        d->status.index = 0;
        d->mode = Impl::Mode::Live;
        d->UpdateImageList({QString("%1/Live/BF").arg(d->topPath)});
    }

    auto SimulationFeeder::StartLiveFL(int32_t channel) -> void {
        d->status.index = 0;
        d->mode = Impl::Mode::Live;
        d->UpdateImageList({QString("%1/Live/FL/CH%2").arg(d->topPath).arg(channel)});
    }

    auto SimulationFeeder::StartAcquisitionHT() -> int32_t {
        d->status.index = 0;
        d->mode = Impl::Mode::Acquisition;
        d->UpdateImageList({QString("%1/Acquisition/HT").arg(d->topPath)});
        return d->status.count;
    }

    auto SimulationFeeder::StartAcquisitionFL(const QList<int32_t>& channels) -> int32_t {
        QStringList pathList;
        std::for_each(channels.begin(), channels.end(), [&](const int32_t& channel){
            pathList.push_back(QString("%1/Acquisition/FL/CH%2").arg(d->topPath).arg(channel));
        });
        d->status.index = 0;
        d->mode = Impl::Mode::Acquisition;
        d->UpdateImageList(pathList);
        return d->status.count;
    }

    auto SimulationFeeder::StartAcquisitionBF(bool colorMode) -> int32_t {
        d->status.index = 0;
        d->mode = Impl::Mode::Acquisition;
        d->UpdateImageList({QString("%1/Acquisition/BF/%2").arg(d->topPath).arg(colorMode?"Color":"Gray")});
        return d->status.count;
    }

    auto SimulationFeeder::FeedImage() const -> TC::CameraControl::Image::Pointer {
        auto image = d->LoadImage(d->status.index);
        d->status.index = d->status.index + 1;
        if(d->status.index >= d->status.count) d->status.index = 0;
        return image;
    }
}
