#include "EmissionFilters.h"
#include "IlluminationPatterns.h"
#include "AcquisitionChannels.h"
#include "InstrumentDefines.h"

#include "MacroPreviewAcquisition.h"

namespace HTXpress::AppComponents::Instrument::MacroPreviewAcquisition {
    using Modality = AppEntity::Modality;

    constexpr auto Start = true;
    constexpr auto Stop = false;

    auto BuildChangeFilter(int32_t channel) -> QVector<int32_t> {
        QVector<int32_t> cmdParams(1);
        cmdParams[0] = channel;
        return cmdParams;
    }

    auto BuildPatternSequence(int32_t channel,
                              bool enableSequence,
                              int32_t ledIntensity,
                              int32_t exposureTime,
                              int32_t readOutTime) -> QVector<int32_t> {
        using TriggerType = AppEntity::TriggerType;
        using ImagingType = AppEntity::ImagingType;

        const auto getSequenceId = [=](ImagingType imagingType, int32_t channel)->int32_t {
            return IlluminationPatterns::Get(imagingType, AppEntity::TriggerType::Trigger, channel);
        };

        const auto imagingType = ImagingType::PreviewBF;
        const auto seqId = getSequenceId(imagingType, channel);
        const auto acqCh = AcquisitionChannels::Get(imagingType, TriggerType::Trigger, channel);
        const auto acqIntensity = std::min<int32_t>(255, ledIntensity / 100.0 * 255);

        QVector<int32_t> cmdParams(8);
        cmdParams[0] = enableSequence;                                              //enable/disable
        cmdParams[1] = seqId;                                               //sequence id
        cmdParams[2] = (acqCh != 0) ? 0 : ledIntensity;                     //intensity of Red LED on DLPC
        cmdParams[3] = (acqCh != 1) ? 0 : ledIntensity;                     //intensity of Green LED on DLPC
        cmdParams[4] = (acqCh != 2) ? 0 : ledIntensity;                     //intensity of Blue LED on DLPC
        cmdParams[5] = exposureTime;                                        //exposure time
        cmdParams[6] = exposureTime + readOutTime;                          //interval time = exposure + readout
        cmdParams[7] = Camera::Internal;

        return cmdParams;
    }

    auto BuildTriggerWithMotion(const UnitMotion& motion, const Parameter& param) -> QVector<int32_t> {
        const auto forward = motion.forwardDirection;

        const auto triggerStartPos = [=]()->int32_t {
            if(forward) return param.forwardDelayCompensationPulse;
            return param.backwardDelayCompensationPulse;
        }();

        const auto triggerEndPos = [=]()->int32_t {
            if(forward) return triggerStartPos + (motion.triggerCount - 1)*motion.triggerIntervalPulse + 5;
            return triggerStartPos - (motion.triggerCount - 1)*motion.triggerIntervalPulse - 5;
        }();

        const auto motionStartPos = [=]()->int32_t {
            if(forward) return triggerStartPos - param.startEndMarginPulse;
            return triggerStartPos + param.startEndMarginPulse;
        }();

        const auto motionEndPos = [=]()->int32_t {
            if(forward) return triggerEndPos + param.startEndMarginPulse;
            return triggerEndPos - param.startEndMarginPulse;
        }();

        const auto motionSpeed = motion.triggerIntervalPulse * std::pow(10, 6) / (param.exposureUSec + param.readoutUSec);

        QVector<int32_t> cmdParams(9);

        cmdParams[0] = 0;                                   //X axis - FIXED
        cmdParams[1] = motionStartPos;                      //Motion start pos (relative to the current)
        cmdParams[2] = motionEndPos;                        //Motion end pos (relative to the current)
        cmdParams[3] = triggerStartPos;                     //Start pos (relative to the current)
        cmdParams[4] = triggerEndPos;                       //End pos (relative to the current)
        cmdParams[5] = motion.triggerIntervalPulse;         //Step amount
        cmdParams[6] = param.triggerPulseWidth;             //Pulse width (usec)
        cmdParams[7] = motionSpeed;                         //Motion speed (pulse per second)
        cmdParams[8] = motionSpeed * param.accelFactor;     //Motion acceleration (pulse per second^2)

        return cmdParams;
    }

    auto Setup(const QList<UnitMotion>& motions, const Parameter& parameter) -> QList<StreamingMacro::Pointer> {
        using CommandType = StreamingMacro::CommandType;

        QList<StreamingMacro::Pointer> macros;
        if(motions.isEmpty()) return macros;

        macros.push_back([=]()->std::shared_ptr<StreamingMacro> {
            auto macro = std::make_shared<StreamingMacro>();
            macro->SetPosition(motions.at(0).motionStartXPulse,
                               motions.at(0).motionStartYPulse,
                               motions.at(0).motionStartZPulse);
            macro->SetAutoFoucs(0);
            macro->SetCommand(CommandType::ChangeFilter, {BuildChangeFilter(parameter.filterChannel)});
            macro->SetImageCount(0);
            return macro;
        }());

        macros.push_back([=]()->std::shared_ptr<StreamingMacro> {
            auto macro = std::make_shared<StreamingMacro>();
            macro->SetPosition(motions.at(0).motionStartXPulse,
                               motions.at(0).motionStartYPulse,
                               motions.at(0).motionStartZPulse);
            macro->SetAutoFoucs(0);
            macro->SetCommand(CommandType::PatternSequence,
                              {BuildPatternSequence(0, 
                                                    Start, 
                                                    parameter.ledIntensity,
                                                    parameter.exposureUSec,
                                                    parameter.readoutUSec)});
            macro->SetImageCount(0);
            return macro;
        }());

        for(const auto& motion : motions) {
            auto macro = std::make_shared<StreamingMacro>();
            macro->SetPosition(motion.motionStartXPulse, motion.motionStartYPulse, motion.motionStartZPulse);
            macro->SetAutoFoucs(1);
            macro->SetCommand(CommandType::TriggerWithMotion, {BuildTriggerWithMotion(motion, parameter)});
            macro->SetImageCount(motion.triggerCount);
            macros.push_back(macro);
        }

        macros.push_back([=]()->std::shared_ptr<StreamingMacro> {
            auto macro = std::make_shared<StreamingMacro>();
            macro->SetPosition(motions.at(0).motionStartXPulse,
                               motions.at(0).motionStartYPulse,
                               motions.at(0).motionStartZPulse);
            macro->SetAutoFoucs(0);
            macro->SetCommand(CommandType::PatternSequence,
                              {BuildPatternSequence(0, 
                                                    Stop, 
                                                    parameter.ledIntensity,
                                                    parameter.exposureUSec,
                                                    parameter.readoutUSec)});
            macro->SetImageCount(0);
            return macro;
        }());

        return macros;
    }
}