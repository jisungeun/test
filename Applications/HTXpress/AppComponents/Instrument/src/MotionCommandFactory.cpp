#include "MotionCommandFactory.h"

namespace HTXpress::AppComponents::Instrument {
    auto MotionCommandFactory::Build(MotionCommandName name) -> MotionCommand::Command::Pointer {
        MotionCommand::Command::Pointer command;

        switch(name) {
        case MotionCommandName::ChangeFilter1:
        case MotionCommandName::ChangeFilter2:
        case MotionCommandName::ChangeFilter3:
        case MotionCommandName::ChangeFilter4:
        case MotionCommandName::ChangeFilter5:
        case MotionCommandName::ChangeFilter6:
            command = std::make_shared<MotionCommand::FilterWheel>(name);
            break;
        case MotionCommandName::ColorBFStartSequence:
        case MotionCommandName::FLCH0StartSequence:
        case MotionCommandName::FLCH1StartSequence:
        case MotionCommandName::FLCH2StartSequence:
        case MotionCommandName::FLLiveCH0StartSequence:
        case MotionCommandName::FLLiveCH1StartSequence:
        case MotionCommandName::FLLiveCH2StartSequence:
        case MotionCommandName::HTStartSequence1:
        case MotionCommandName::HTStartSequence2:
        case MotionCommandName::HTStartSequence3:
        case MotionCommandName::HTStartSequence4:
        case MotionCommandName::MonoBFStartSequence:
        case MotionCommandName::StopSequence:
            command = std::make_shared<MotionCommand::EnablePatternSequence>(name);
            break;
        case MotionCommandName::FL2DTrigger:
        case MotionCommandName::HT2DTrigger:
        case MotionCommandName::ColorBFTrigger:
        case MotionCommandName::MonoBFLiveTrigger:
        case MotionCommandName::MonoBFTrigger:
            command = std::make_shared<MotionCommand::TriggerOnly>(name);
            break;
        case MotionCommandName::FL3DTrigger:
        case MotionCommandName::HT3DTrigger:
            command = std::make_shared<MotionCommand::TriggerWithMotion>(name);
            break;
        case MotionCommandName::RunMacroGroupSingle:
        case MotionCommandName::RunMacroGroup1:
        case MotionCommandName::RunMacroGroup2:
        case MotionCommandName::RunMacroGroup3:
        case MotionCommandName::RunMacroGroup4:
        case MotionCommandName::RunMacroGroup5:
        case MotionCommandName::RunMacroGroup6:
        case MotionCommandName::RunMacroGroup7:
        case MotionCommandName::RunMacroGroup8:
        case MotionCommandName::RunMacroGroup9:
        case MotionCommandName::RunMacroGroup10:
        case MotionCommandName::RunMacroGroup11:
        case MotionCommandName::RunMacroGroup12:
        case MotionCommandName::RunMacroGroup13:
        case MotionCommandName::RunMacroGroup14:
        case MotionCommandName::RunMacroGroup15:
        case MotionCommandName::RunMacroGroup16:
        case MotionCommandName::RunMacroGroup17:
        case MotionCommandName::RunMacroGroup18:
        case MotionCommandName::RunMacroGroup19:
            command = std::make_shared<MotionCommand::RunMacroMatrix>(name);
            break;
        case MotionCommandName::Invalid:
            break;
        };

        return command;
    }
}
