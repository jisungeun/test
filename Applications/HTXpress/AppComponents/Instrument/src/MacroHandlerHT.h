#pragma once
#include <memory>

#include "MacroHandler.h"

namespace HTXpress::AppComponents::Instrument {
    class MacroHandlerHT : public MacroHandler {
    public:
        MacroHandlerHT();
        ~MacroHandlerHT() override;

        auto Handle(const ImagingCondition::Pointer& cond) const -> MacroGroup::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}