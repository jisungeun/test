#pragma once
#include <memory>

#include "InstrumentDefines.h"
#include "ResponsePosition.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandGetPosition : public Command {
    public:
        CommandGetPosition();
        virtual ~CommandGetPosition();

        auto Perform() -> bool override;
        auto GetResponse()->Response::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}