#define LOGGER_TAG "[CommandLiveWithTrigger]"
#include <QThread>

#include <TCLogger.h>
#include <CameraControlFactory.h>
#include <MCUFactory.h>

#include "CameraManager.h"
#include "MacroLiveWithTrigger.h"
#include "CommandLiveWithTrigger.h"


namespace HTXpress::AppComponents::Instrument {
    using CameraControl = TC::CameraControl::CameraControl;

    struct CommandLiveWithTrigger::Impl {
        CommandLiveWithTrigger* p{ nullptr };
        ImagingParameter param;
        const QVector<int32_t>& pos;
        int32_t triggerCount;
        int32_t triggerInterval;
        int32_t pulseWidth;
        bool useHCG{ false };
        double gain{ 0 };
        Response::Pointer resp{ nullptr };

        Impl(CommandLiveWithTrigger* p,
             const ImagingParameter& param,
             const QVector<int32_t>& pos,
             int32_t triggerCount,
             int32_t triggerInterval,
             int32_t pulseWidth) 
            : p(p)
            , param(param)
            , pos(pos)
            , triggerCount(triggerCount)
            , triggerInterval(triggerInterval)
            , pulseWidth(pulseWidth) {
            resp = std::make_shared<Response>();
        }

        auto RunMacro(TC::MCUControl::IMCUControl::Pointer mcuControl, const QList<StreamingMacro::Pointer>& macros)->bool;
    };

    auto CommandLiveWithTrigger::Impl::RunMacro(TC::MCUControl::IMCUControl::Pointer mcuControl,
                                     const QList<StreamingMacro::Pointer>& macros) -> bool {
#if 1
        for(auto macro : macros) {
            QLOG_INFO() << macro->Str();
        }
#endif
        if(!p->WaitForMotionStop()) {
            QLOG_ERROR() << "Motion is not stopped";
            return false;
        }

        if(!mcuControl->StartMacroStreaming()) return false;
        for(auto macro : macros) {
            if(!mcuControl->SendMacrosToStreamBuffer({QList<int32_t>::fromVector(macro->GetPacket())})) return false;
        }

        TC::MCUControl::MCUResponse status;
        while(mcuControl->CheckStatus(status)) {
            const auto completed = status.GetValue(TC::MCUControl::Response::MacroStreamingIndex);
            const auto error = status.GetValue(TC::MCUControl::Response::MacroStreamingError);

            if(error == 1) return false;
            if(completed == macros.length()) break;

            QThread::msleep(100);
        }

        if(!mcuControl->StopMacroStreaming()) return false;

        return true;
    }

    CommandLiveWithTrigger::CommandLiveWithTrigger(const ImagingParameter& param, const QVector<int32_t>& pos,
        int32_t triggerCount, int32_t triggerInterval, int32_t pulseWidth)
        : Command("LiveWithTrigger")
        , d{new Impl(this, param, pos, triggerCount, triggerInterval, pulseWidth)} {
    }

    CommandLiveWithTrigger::~CommandLiveWithTrigger() {
    }

    auto CommandLiveWithTrigger::UseHighConversionGain(bool useHCG, double gain)->void {
        d->useHCG = useHCG;
        d->gain = gain;
    }

    auto CommandLiveWithTrigger::Perform() -> bool {
        auto camera = CameraManager::GetInstance()->GetImagingCamera();
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        d->resp->SetResult(false);

        camera->StopAcquisition();

        double prevGain = 0;
        if (d->useHCG) {
            prevGain = camera->GetGain();
            camera->SetGainConversionMode(TC::CameraControl::GainConversion::HCG);
            camera->SetGain(d->gain);
        }

        mcuControl->SetLEDChannel(d->param.LEDChannel());

        bool success = true;

        do {
            if (!camera->StartAcquisition(0)) {
                const auto error = camera->GetLastError();
                d->resp->SetMessage(QString("It fails to start acquisition : %1").arg(error._to_string()));
                success = false;
                break;
            }

            auto startLiveMacro = MacroLiveWithTrigger::Start(d->param, d->pos, d->triggerCount, d->triggerInterval, d->pulseWidth);
            if (!d->RunMacro(mcuControl, startLiveMacro)) {
                d->resp->SetMessage("It fails to stop and start live view");
                success = false;
                break;
            }
        } while (false);

        d->resp->SetResult(success);

        if (d->useHCG) {
            camera->SetGainConversionMode(TC::CameraControl::GainConversion::LCG);
            camera->SetGain(prevGain);
        }

        return success;
    }

    auto CommandLiveWithTrigger::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}
