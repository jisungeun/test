#pragma once
#include <memory>

#include "InstrumentDefines.h"
#include "Response.h"

namespace HTXpress::AppComponents::Instrument {

    class ResponsePosition :public Response {
    public:
        typedef std::shared_ptr<ResponsePosition> Pointer;

    public:
        ResponsePosition();
        virtual ~ResponsePosition();

        auto SetPosition(Axis axis, int32_t value)->void;
        auto GetPosition(Axis axis) const->int32_t;

        auto SetError()->void;
        auto GetError() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}