#pragma once
#include <memory>
#include <QList>

#include <AppEntityDefines.h>

#include "StreamingMacro.h"

namespace HTXpress::AppComponents::Instrument::MacroPreviewAcquisition {
    struct Parameter {
        int32_t startEndMarginPulse{ 15 };
        int32_t forwardDelayCompensationPulse{ 13 };
        int32_t backwardDelayCompensationPulse{ -13 };

        int32_t ledIntensity{ 130 };
        int32_t exposureUSec{ 250 };
        int32_t readoutUSec{ 8000 };

        int32_t triggerPulseWidth{ 100 };
        double accelFactor{ 0.1 };

        int32_t ledChannel{ 2 };
        int32_t filterChannel{ 0 };
    };

    auto Setup(const QList<UnitMotion>& motions, const Parameter& parameter) -> QList<StreamingMacro::Pointer>;
}