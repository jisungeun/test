#define LOGGER_TAG "[CommandLive]"
#include <QThread>

#include <TCLogger.h>
#include <CameraControlFactory.h>
#include <MCUFactory.h>

#include "CameraManager.h"
#include "MacroLive.h"
#include "CommandLive.h"


namespace HTXpress::AppComponents::Instrument {
    using CameraControl = TC::CameraControl::CameraControl;

    struct CommandLive::Impl {
        CommandLive* p{ nullptr };
        LiveMode mode{ LiveMode::BrightField };
        ImagingParameter param;
        const QVector<int32_t>& pos;
        bool updateOnly{ false };
        struct {
            bool enable{ false };
            int32_t minVal{ 0 };
            int32_t maxVal{ 255 };
        } intensityAdjust;
        Response::Pointer resp{ nullptr };

        Impl(CommandLive* p, LiveMode mode, const ImagingParameter& param, const QVector<int32_t>& pos) : p(p), mode(mode), param(param), pos(pos) {
            resp = std::make_shared<Response>();
        }

        auto RunMacro(TC::MCUControl::IMCUControl::Pointer mcuControl, const QList<StreamingMacro::Pointer>& macros)->bool;
    };

    auto CommandLive::Impl::RunMacro(TC::MCUControl::IMCUControl::Pointer mcuControl,
                                     const QList<StreamingMacro::Pointer>& macros) -> bool {
#if 1
        for(auto macro : macros) {
            QLOG_INFO() << macro->Str();
        }
#endif
        if(!p->WaitForMotionStop()) {
            QLOG_ERROR() << "Motion is not stopped";
            return false;
        }

        if(!mcuControl->StartMacroStreaming()) return false;
        for(auto macro : macros) {
            if(!mcuControl->SendMacrosToStreamBuffer({QList<int32_t>::fromVector(macro->GetPacket())})) return false;
        }

        TC::MCUControl::MCUResponse status;
        while(mcuControl->CheckStatus(status)) {
            const auto completed = status.GetValue(TC::MCUControl::Response::MacroStreamingIndex);
            const auto error = status.GetValue(TC::MCUControl::Response::MacroStreamingError);

            if(error == 1) return false;
            if(completed == macros.length()) break;

            QThread::msleep(100);
        }

        if(!mcuControl->StopMacroStreaming()) return false;

        return true;
    }

    CommandLive::CommandLive(LiveMode mode, const ImagingParameter& param, const QVector<int32_t>& pos)
        : Command("Live")
        , d{new Impl(this, mode, param, pos)} {
    }

    CommandLive::~CommandLive() {
    }

    auto CommandLive::SetUpdateParmaeterOnly(bool updateOnly) -> void {
        d->updateOnly = updateOnly;
    }

    auto CommandLive::SetIntensityRange(int32_t minVal, int32_t maxVal) -> void {
        d->intensityAdjust.enable = true;
        d->intensityAdjust.minVal = minVal;
        d->intensityAdjust.maxVal = maxVal;
    }

    auto CommandLive::Perform() -> bool {
        auto camera = CameraManager::GetInstance()->GetImagingCamera();
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        d->resp->SetResult(false);

        if(!d->updateOnly) {
            camera->StopAcquisition();
        }

        mcuControl->SetLEDChannel(d->param.LEDChannel());

        if(d->intensityAdjust.enable) {
            const auto red = d->param.IntensityRed();
            const auto green = d->param.IntensityGreen();
            const auto blue = d->param.IntensityBlue();

            auto adjust = [=](int32_t inputVal)->int32_t {
                if(inputVal == 0) return inputVal;
                return d->intensityAdjust.minVal + (d->intensityAdjust.maxVal - d->intensityAdjust.minVal) * (inputVal / 100.0);
            };

            d->param.SetIntensity(adjust(red), adjust(green), adjust(blue));
        }

        auto stopLiveMacro = MacroLive::Stop(d->param, d->pos, 0);
        auto startLiveMacro = MacroLive::Start(d->param, d->pos, 0);    //TODO AF Mode is not specified...
        if(!d->RunMacro(mcuControl, stopLiveMacro + startLiveMacro)) {
            d->resp->SetMessage("It fails to stop and start live view");
            return false;
        }

        if(!d->updateOnly) {
            if(!camera->StartAcquisition(0)) {
                const auto error = camera->GetLastError();
                d->resp->SetMessage(QString("It fails to start acquisition : %1").arg(error._to_string()));
                return false;
            }
        }

        d->resp->SetResult(true);

        return true;
    }

    auto CommandLive::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}
