#pragma once
#include <memory>

#include "InstrumentDefines.h"
#include "InitializeProgress.h"
#include "InstrumentConfig.h"
#include "Response.h"
#include "Command.h"

namespace HTXpress::AppComponents::Instrument {
    class CommandInitialize : public Command {
    public:
        explicit CommandInitialize(const Config& config);
        CommandInitialize(const Config& config, InitializeProgress::Pointer progress);
        virtual ~CommandInitialize();

        auto Perform() -> bool override;
        auto GetResponse()->Response::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}