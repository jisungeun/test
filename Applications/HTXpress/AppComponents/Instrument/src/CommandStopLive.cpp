#define LOGGER_TAG "[CommandStopLive]"
#include <QThread>

#include <TCLogger.h>
#include <CameraControlFactory.h>
#include <MCUFactory.h>

#include "CameraManager.h"
#include "MacroLive.h"
#include "CommandStopLive.h"

#include "IMCUControl.h"

namespace HTXpress::AppComponents::Instrument {
    struct CommandStopLive::Impl {
        CommandStopLive* p{ nullptr };
        Response::Pointer resp{ nullptr };

        explicit Impl(CommandStopLive* p) : p(p) {
            resp = std::make_shared<Response>();
        }

        auto RunMacro(TC::MCUControl::IMCUControl::Pointer mcuControl, const QList<StreamingMacro::Pointer>& macros)->bool;
    };

    auto CommandStopLive::Impl::RunMacro(TC::MCUControl::IMCUControl::Pointer mcuControl, const QList<StreamingMacro::Pointer>& macros) -> bool {
#if 1
        for(auto macro : macros) {
            QLOG_INFO() << macro->Str();
        }
#endif
        if(!p->WaitForMotionStop()) {
            QLOG_ERROR() << "Motion is not stopped";
            return false;
        }

        if(!mcuControl->StartMacroStreaming()) return false;
        for(auto macro : macros) {
            if(!mcuControl->SendMacrosToStreamBuffer({QList<int32_t>::fromVector(macro->GetPacket())})) return false;
        }

        TC::MCUControl::MCUResponse status;
        while(mcuControl->CheckStatus(status)) {
            const auto completed = status.GetValue(TC::MCUControl::Response::MacroStreamingIndex);
            const auto error = status.GetValue(TC::MCUControl::Response::MacroStreamingError);

            if(error == 1) return false;
            if(completed == macros.length()) break;

            QThread::msleep(100);
        }

        if(!mcuControl->StopMacroStreaming()) return false;

        return true;
    }

    CommandStopLive::CommandStopLive()
        : Command("StopLive")
        , d{new Impl(this)} {
    }

    CommandStopLive::~CommandStopLive() {
    }

    auto CommandStopLive::Perform() -> bool {
        auto camera = CameraManager::GetInstance()->GetImagingCamera();
        auto mcuControl = TC::MCUControl::MCUFactory::CreateControl();

        d->resp->SetResult(false);

        camera->StopAcquisition();

        auto stopLiveMacro = MacroLive::Stop();
        if(!d->RunMacro(mcuControl, stopLiveMacro)) {
            d->resp->SetMessage("It fails to stop live view");
            return false;
        }

        d->resp->SetResult(true);

        return true;
    }

    auto CommandStopLive::GetResponse() -> Response::Pointer {
        return d->resp;
    }
}
