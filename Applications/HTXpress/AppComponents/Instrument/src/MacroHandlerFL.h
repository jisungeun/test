#pragma once
#include <memory>
#include <QMap>

#include <FLFilter.h>
#include "MacroHandler.h"

namespace HTXpress::AppComponents::Instrument {
    class MacroHandlerFL : public MacroHandler {
        using FLFilterMap = QMap<AppEntity::FLFilter, int32_t>;

    public:
        MacroHandlerFL(const FLFilterMap& exFilters = FLFilterMap(),
                       const FLFilterMap& emFilters = FLFilterMap());
        ~MacroHandlerFL() override;

        auto Handle(const ImagingCondition::Pointer& cond) const -> MacroGroup::Pointer override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}