#pragma once
#include <memory>
#include <QMap>

#include "InstrumentDefines.h"
#include "MotionCommand.h"

namespace HTXpress::AppComponents::Instrument {
	class MotionCommandPanelControl {
	public:
		MotionCommandPanelControl();
		~MotionCommandPanelControl();

	    auto SetCommand(MotionCommandName name, MotionCommand::Command::Pointer cmd)->void;
	    auto GetCommand(MotionCommandName name) const -> MotionCommand::Command::Pointer;
		auto GetCommands() const->QList<MotionCommand::Command::Pointer>;
		auto GetCommandByIndex(int32_t index)->MotionCommand::Command::Pointer;

		auto IsModified() const->bool;
	    auto GetModifiedList() const->QList<MotionCommandName>;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}