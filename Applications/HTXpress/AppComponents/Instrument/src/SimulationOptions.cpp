#include "SimulationOptions.h"

namespace HTXpress::AppComponents::Instrument {
    struct SimulationOptions::Impl {
        QString imagePath;
    };

    SimulationOptions::SimulationOptions() : d{new Impl} {
    }

    SimulationOptions::~SimulationOptions() {
    }

    auto SimulationOptions::GetInstance() -> Pointer {
        static Pointer theInstance{ new SimulationOptions() };
        return theInstance;
    }

    auto SimulationOptions::SetImagePath(const QString& path) -> void {
        d->imagePath = path;
    }

    auto SimulationOptions::GetImagePath() const -> QString {
        return d->imagePath;
    }
}
