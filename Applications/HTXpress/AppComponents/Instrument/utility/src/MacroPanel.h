#pragma once
#include <memory>
#include <QWidget>

#include "Defines.h"

namespace HTXpress::AppComponents::Instrument::App {
    class MacroPanel : public QWidget {
		Q_OBJECT

    public:
        MacroPanel(QWidget* parent = nullptr);
        ~MacroPanel();

        auto Reset()->void;
        auto Save(const QString& path)->bool;
        auto Open(const QString& path)->bool;

    protected slots:
        void onCreateNewScenario(int32_t start, QString title);
        void onSequenceSelected(int32_t index);
        void onSequenceChanged(int32_t row, int32_t start, QString title);
        void onNewModality(AppEntity::Modality modality, bool is3D);
        void onDeleteModality(int32_t index);
        void onChangeModality(int32_t index, AppEntity::Modality modality, bool is3D);
        void onApplyImagingCondition();
        void onAddLocation(const LocationInput& location);
        void onDeleteLocation(int32_t index);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}