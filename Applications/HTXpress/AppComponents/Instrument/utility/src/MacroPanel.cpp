#define LOGGER_TAG "[UI]"

#include <QList>
#include <QMessageBox>
#include <TCLogger.h>

#include "MacroRepo.h"
#include "ImagingSequenceDialog.h"
#include "MacroPanelControl.h"
#include "MacroWriter.h"
#include "MacroLoader.h"
#include "ui_MacroPanel.h"
#include "MacroPanel.h"

namespace HTXpress::AppComponents::Instrument::App {
    struct MacroPanel::Impl {
        MacroPanelControl control;
        Ui::MacroPanel ui;

        auto FillCondition(AppEntity::ImagingCondition::Pointer cond)->void;
    };

    auto MacroPanel::Impl::FillCondition(AppEntity::ImagingCondition::Pointer cond) -> void {
        AppEntity::ImagingCondition::Pointer newCond;
        switch(cond->GetModality()) {
        case AppEntity::Modality::HT:
            newCond = [=]()->AppEntity::ImagingCondition::Pointer {
                if(cond->Is3D()) return ui.conditionPanel->GetHT3DCondition();
                return ui.conditionPanel->GetHT2DCondition();
            }();
            break;
        case AppEntity::Modality::FL:
            newCond = [=]()->AppEntity::ImagingCondition::Pointer {
                if(cond->Is3D()) return ui.conditionPanel->GetFL3DCondition();
                return ui.conditionPanel->GetFL2DCondition();
            }();
            break;
        case AppEntity::Modality::BF:
            newCond = ui.conditionPanel->GetBFCondition();
            break;
        }

        cond->Set(newCond);
    }

    MacroPanel::MacroPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);

        connect(d->ui.scenarioPanel, SIGNAL(sigNewScenario(int32_t,QString)), this, SLOT(onCreateNewScenario(int32_t,QString)));
        connect(d->ui.scenarioPanel, SIGNAL(sigSelected(int32_t)), this, SLOT(onSequenceSelected(int32_t)));
        connect(d->ui.scenarioPanel, SIGNAL(sigSequenceChanged(int32_t,int32_t,QString)), this, 
                SLOT(onSequenceChanged(int32_t,int32_t,QString)));
        connect(d->ui.sequencePanel, SIGNAL(sigAddModality(AppEntity::Modality, bool)), this, 
                SLOT(onNewModality(AppEntity::Modality, bool)));
        connect(d->ui.sequencePanel, SIGNAL(sigDeleteModality(int32_t)), this, SLOT(onDeleteModality(int32_t)));
        connect(d->ui.sequencePanel, SIGNAL(sigChangeModality(int32_t,AppEntity::Modality,bool)), this, 
                SLOT(onChangeModality(int32_t,AppEntity::Modality,bool)));
        connect(d->ui.conditionPanel, SIGNAL(sigApply()), this, SLOT(onApplyImagingCondition()));
        connect(d->ui.locationPanel, SIGNAL(sigAddLocation(const LocationInput&)), this, SLOT(onAddLocation(const LocationInput&)));
        connect(d->ui.locationPanel, SIGNAL(sigDeleteLocation(int32_t)), this, SLOT(onDeleteLocation(int32_t)));
    }

    MacroPanel::~MacroPanel() {
    }

    auto MacroPanel::Reset() -> void {
        d->control.ClearAll();
        d->ui.scenarioPanel->ClearAll();
        d->ui.sequencePanel->ClearAll();
        d->ui.locationPanel->ClearAll();
        d->ui.conditionPanel->ClearAll();
    }

    auto MacroPanel::Save(const QString& path) -> bool {
        auto repo = MacroRepo::GetInstance();
        auto scenario = repo->GetImagingScenario();

        MacroWriter writer;
        writer.SetImagingScenario(scenario);
        writer.SetLocations(d->ui.locationPanel->Get());
        return writer.Write(path);
    }

    auto MacroPanel::Open(const QString& path) -> bool {

        MacroLoader loader;
        if(!loader.Load(path)) return false;

        auto scenario = loader.GetImagingScenario();
        if(scenario == nullptr) {
            QMessageBox::warning(this, "Load macro", "Scenario is not loaded properly");
            return false;
        }

        d->ui.locationPanel->Set(loader.GetLocations());
        d->ui.conditionPanel->SetHT3DCondition(loader.GetImagingCondition(AppEntity::Modality::HT, true));
        d->ui.conditionPanel->SetHT2DCondition(loader.GetImagingCondition(AppEntity::Modality::HT, false));
        d->ui.conditionPanel->SetFL3DCondition(loader.GetImagingCondition(AppEntity::Modality::FL, true));
        d->ui.conditionPanel->SetFL2DCondition(loader.GetImagingCondition(AppEntity::Modality::FL, false));
        d->ui.conditionPanel->SetBFCondition(loader.GetImagingCondition(AppEntity::Modality::BF, false));

        const auto count = scenario->GetCount();

        for(int idx=0; idx<count; idx++) {
            d->ui.scenarioPanel->AddSequence(scenario->GetStartTime(idx), scenario->GetTitle(idx));
        }

        return true;
    }

    void MacroPanel::onCreateNewScenario(int32_t start, QString title) {
        ImagingSequenceDialog dlg;
        auto res = dlg.exec();
        if(res == QDialog::Rejected) {
            QMessageBox::warning(this, "Add sequence", "Adding imaging sequence is canceled.");
            return;
        }

        auto sequenceInput = dlg.Get();
        auto count = sequenceInput.length();

        if(count == 0 ) {
            QMessageBox::warning(this, "New sequence", "No imaging modality is specified.");
            return;
        }

        auto sequence = d->control.CreateSequence(sequenceInput);

        for(int idx=0; idx<count; idx++) {
            auto& cond = sequence->GetImagingCondition(idx);
            d->FillCondition(cond);
        }

        auto repo = MacroRepo::GetInstance();
        auto scenario = repo->GetImagingScenario();
        scenario->AddSequence(start, title, sequence);

        d->ui.scenarioPanel->AddSequence(start, title);
        //d->ui.sequencePanel->Set(sequenceInput);
    }

    void MacroPanel::onSequenceSelected(int32_t index) {
        auto repo = MacroRepo::GetInstance();
        auto scenario = repo->GetImagingScenario();
        auto sequence = scenario->GetSequence(index);
        if(sequence == nullptr) return;

        QList<SequenceInput> sequenceInput;

        const auto count = sequence->GetModalityCount();
        for(int32_t idx=0; idx<count; idx++) {
            auto& cond = sequence->GetImagingCondition(idx);
            auto modality = cond->GetModality();
            auto is3D = cond->GetSliceCount() > 1;
            sequenceInput.append({modality, is3D});
        }

        d->ui.sequencePanel->Set(sequenceInput);
    }

    void MacroPanel::onSequenceChanged(int32_t row, int32_t start, QString title) {
        auto sequenceIndex = d->ui.scenarioPanel->SelectedIndex();
        if(sequenceIndex < 0) {
            QMessageBox::warning(this, "New modality", "No sequence is selected.");
            return;
        }

        auto repo = MacroRepo::GetInstance();
        auto scenario = repo->GetImagingScenario();

        scenario->EditSequence(row, start, title);
    }

    void MacroPanel::onNewModality(AppEntity::Modality modality, bool is3D) {
        auto sequenceIndex = d->ui.scenarioPanel->SelectedIndex();
        if(sequenceIndex < 0) {
            QMessageBox::warning(this, "New modality", "No sequence is selected.");
            return;
        }

        auto repo = MacroRepo::GetInstance();
        auto scenario = repo->GetImagingScenario();
        auto sequence = scenario->GetSequence(sequenceIndex);

        auto newCond = d->control.CreateCondition(modality, is3D);
        d->FillCondition(newCond);
        sequence->AddImagingCondition(newCond);

        d->ui.sequencePanel->Add({ modality, is3D });
    }

    void MacroPanel::onDeleteModality(int32_t index) {
        auto sequenceIndex = d->ui.scenarioPanel->SelectedIndex();
        if(sequenceIndex < 0) {
            QMessageBox::warning(this, "Delete modality", "No sequence is selected.");
            return;
        }

        auto repo = MacroRepo::GetInstance();
        auto scenario = repo->GetImagingScenario();
        auto sequence = scenario->GetSequence(sequenceIndex);

        sequence->RemoveImagingCondition(index);

        d->ui.sequencePanel->Remove(index);
    }

    void MacroPanel::onChangeModality(int32_t index, AppEntity::Modality modality, bool is3D) {
        auto sequenceIndex = d->ui.scenarioPanel->SelectedIndex();
        if(sequenceIndex < 0) {
            QMessageBox::warning(this, "Change modality", "No sequence is selected.");
            return;
        }

        auto repo = MacroRepo::GetInstance();
        auto scenario = repo->GetImagingScenario();
        auto sequence = scenario->GetSequence(sequenceIndex);

        auto newCond = d->control.CreateCondition(modality, is3D);
        d->FillCondition(newCond);
        sequence->SetImagingCondition(index, newCond);
    }

    void MacroPanel::onApplyImagingCondition() {
        auto repo = MacroRepo::GetInstance();
        auto scenario = repo->GetImagingScenario();

        const auto sequenceCount = scenario->GetCount();
        for(int sequenceIndex=0; sequenceIndex < sequenceCount; sequenceIndex++) {
            auto sequence = scenario->GetSequence(sequenceIndex);
            const auto modalityCount = sequence->GetModalityCount();
            for(int modalityIndex=0; modalityIndex<modalityCount; modalityIndex++) {
                auto cond = sequence->GetImagingCondition(modalityIndex);
                d->FillCondition(cond);
            }
        }
    }

    void MacroPanel::onAddLocation(const LocationInput& location) {
        d->ui.locationPanel->Add(location);
    }

    void MacroPanel::onDeleteLocation(int32_t index) {
        d->ui.locationPanel->Remove(index);
    }
}
