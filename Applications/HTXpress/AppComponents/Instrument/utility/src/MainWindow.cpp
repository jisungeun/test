#define LOGGER_TAG "[UI]"
#include <QStandardPaths>
#include <QDesktopServices>
#include <QUrl>
#include <QMessageBox>
#include <QFileDialog>
#include <QSettings>
#include <QTimer>
#include <QFileDialog>

#include <TCLogger.h>

#include "ui_mainwindow.h"
#include "Version.h"
#include "MainWindowControl.h"
#include "MainWindow.h"

namespace HTXpress::AppComponents::Instrument::App {
    struct MainWindow::Impl {
        Ui::MainWindow ui;
        MainWindowControl control;

        QString lastDir = "D:/";
    };

    MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), d{ new Impl } {
        d->ui.setupUi(this);

        const auto& ver = QString("HTX Macro Console %1").arg(PROJECT_REVISION);
        QLOG_INFO() << ver;

        setWindowTitle(ver);

        connect(d->ui.actionNew, SIGNAL(triggered()), this, SLOT(onNew()));
        connect(d->ui.actionSave, SIGNAL(triggered()), this, SLOT(onSave()));
        connect(d->ui.actionOpen, SIGNAL(triggered()), this, SLOT(onOpen()));
        connect(d->ui.actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));

        connect(d->ui.actionNewMacro, SIGNAL(triggered()), this, SLOT(onNewMacro()));
        connect(d->ui.actionSaveMacro, SIGNAL(triggered()), this, SLOT(onSaveMacro()));
        connect(d->ui.actionOpenMacro, SIGNAL(triggered()), this, SLOT(onOpenMacro()));
    }

    MainWindow::~MainWindow() {
    }

    void MainWindow::onNew() {
        d->ui.cmdPanel->Clear();

        auto& commands = d->control.CreateNew();

        for(auto& command : commands) {
            QLOG_INFO() << command->GetNameAsString() << " - " << command->GetCommandTypeAsString();
            d->ui.cmdPanel->SetCommand(command->GetName(), command);
        }
    }

    void MainWindow::onSave() {
        const auto path = QFileDialog::getSaveFileName(this, "Select a file to save motion commands", QString(), 
                                                       "Motion Commands (*.mcmd)");

        auto& commands = d->ui.cmdPanel->GetCommands();
        for(auto& command : commands) {
            QLOG_INFO() << command->GetNameAsString() << " - " << command->GetCommandTypeAsString();
            d->ui.cmdPanel->SetCommand(command->GetName(), command);
        }

        if(!d->control.Save(path)) {
            QMessageBox::warning(this, "Save", "Fails to save motion commands");
        }
    }

    void MainWindow::onOpen() {
        const auto path = QFileDialog::getOpenFileName(this, "Select a file to open motion commands", QString(),
                                                       "Motion Commands (*.mcmd)");
        auto commands = d->control.Load(path);
        if(commands.isEmpty()) {
            QMessageBox::warning(this, "Load", "No motion commands exists");
            return;
        }

        d->ui.cmdPanel->Clear();

        for(auto& command : commands) {
            QLOG_INFO() << command->GetNameAsString() << " - " << command->GetCommandTypeAsString();
            d->ui.cmdPanel->SetCommand(command->GetName(), command);
        }
    }

    void MainWindow::onNewMacro() {
        d->ui.macroPanel->Reset();
    }

    void MainWindow::onSaveMacro() {
        const auto str = QFileDialog::getSaveFileName(this, "Select a file to save macro", d->lastDir, "Macro (*.mac)");
        if(str.isEmpty()) return;
        d->lastDir = str;

        if(!d->ui.macroPanel->Save(str)) {
            QMessageBox::warning(this, "Save Macro", "It fails to save macro to a file");
        }
    }

    void MainWindow::onOpenMacro() {
        const auto str = QFileDialog::getOpenFileName(this, "Select a macro file", d->lastDir, "Macro (*.mac)");
        if(str.isEmpty()) return;
        d->lastDir = str;

        if(!d->ui.macroPanel->Open(str)) {
            QMessageBox::warning(this, "Open Macro", "It fails to open a macro file");
        }
    }
}
