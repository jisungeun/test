#include <QMap>

#include "ImagingConditionPanelControl.h"

namespace HTXpress::AppComponents::Instrument::App {
    struct ImagingConditionPanelControl::Impl {
        QMap<Type, AppEntity::ImagingCondition::Pointer> conditions;
    };

    ImagingConditionPanelControl::ImagingConditionPanelControl() : d{new Impl} {
    }

    ImagingConditionPanelControl::~ImagingConditionPanelControl() {
    }

    auto ImagingConditionPanelControl::Set(Type type, const AppEntity::ImagingCondition::Pointer cond) -> void {
        if(cond == nullptr) return;
        d->conditions[type] = cond->Clone();
    }

    auto ImagingConditionPanelControl::Get(Type type) const -> AppEntity::ImagingCondition::Pointer {
        return d->conditions[type];
    }

    auto ImagingConditionPanelControl::Reset() -> void {
        for(auto& condition : d->conditions) {
            condition->Clear();
        }
    }

}
