#pragma once
#include <memory>
#include <QWidget>

#include <ImagingCondition.h>

namespace HTXpress::AppComponents::Instrument::App {
    class ImagingConditionPanel : public QWidget {
		Q_OBJECT

    public:
        ImagingConditionPanel(QWidget* parent = nullptr);
        ~ImagingConditionPanel() override;

        auto SetHT3DCondition(AppEntity::ImagingCondition::Pointer cond)->void;
        auto SetHT2DCondition(AppEntity::ImagingCondition::Pointer cond)->void;
        auto SetFL3DCondition(AppEntity::ImagingCondition::Pointer cond)->void;
        auto SetFL2DCondition(AppEntity::ImagingCondition::Pointer cond)->void;
        auto SetBFCondition(AppEntity::ImagingCondition::Pointer cond)->void;

        auto GetHT3DCondition() const->AppEntity::ImagingCondition::Pointer;
        auto GetHT2DCondition() const->AppEntity::ImagingCondition::Pointer;
        auto GetFL3DCondition() const->AppEntity::ImagingCondition::Pointer;
        auto GetFL2DCondition() const->AppEntity::ImagingCondition::Pointer;
        auto GetBFCondition() const->AppEntity::ImagingCondition::Pointer;

        auto ClearAll()->void;

    protected slots:
        void onEdit();
        void onApply();
        void onRestore();

    signals:
        void sigApply();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}