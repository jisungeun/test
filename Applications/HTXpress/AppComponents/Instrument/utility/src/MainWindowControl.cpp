#include <MotionCommandFactory.h>
#include <MotionCommandLoader.h>
#include <MotionCommandWriter.h>

#include "MainWindowControl.h"

namespace HTXpress::AppComponents::Instrument::App {
    struct MainWindowControl::Impl {
        QMap<MotionCommandName, MotionCommand::Command::Pointer> commands;
    };

    MainWindowControl::MainWindowControl() : d{ new Impl } {
    }

    MainWindowControl::~MainWindowControl() {
    }

    auto MainWindowControl::CreateNew() -> QMap<MotionCommandName, MotionCommand::Command::Pointer>& {
        d->commands.clear();

        for(auto name : MotionCommandName::_values()) {
            auto cmd = MotionCommandFactory::Build(name);
            if(cmd == nullptr) continue;

            d->commands[name] = cmd;
        }

        return d->commands;
    }

    auto MainWindowControl::Save(const QString& path) -> bool {
        MotionCommandWriter writer;
        return writer.Write(path, d->commands.values());
    }

    auto MainWindowControl::Load(const QString& path) -> QList<MotionCommand::Command::Pointer> {
        MotionCommandLoader loader;

        QList<MotionCommand::Command::Pointer> commands;
        loader.Load(path, commands);

        d->commands.clear();
        for(auto& command : commands) {
            d->commands[command->GetName()] = command;
        }

        return commands;
    }
}
