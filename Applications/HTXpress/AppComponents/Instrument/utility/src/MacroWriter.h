#pragma once
#include <memory>
#include <QString>

#include <ImagingScenario.h>

#include "Defines.h"


namespace HTXpress::AppComponents::Instrument::App {
    class MacroWriter {
    public:
        MacroWriter();
        ~MacroWriter();

        auto SetImagingScenario(const AppEntity::ImagingScenario::Pointer scenario)->void;
        auto SetLocations(const QList<LocationInput>& list)->void;

        auto Write(const QString& path)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}