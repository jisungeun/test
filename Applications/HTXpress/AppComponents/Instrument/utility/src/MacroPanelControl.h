#pragma once
#include <memory>
#include <QList>

#include <ImagingSequence.h>

#include "Defines.h"

namespace HTXpress::AppComponents::Instrument::App {
    class MacroPanelControl {
    public:
        MacroPanelControl();
        ~MacroPanelControl();

        auto CreateSequence(const QList<SequenceInput>& inputList) const->AppEntity::ImagingSequence::Pointer;
        auto CreateCondition(AppEntity::Modality modality, bool is3D) const->AppEntity::ImagingCondition::Pointer;

        auto ClearAll()->void;
    };
}