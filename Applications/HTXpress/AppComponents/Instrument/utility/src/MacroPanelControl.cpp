#define LOGGER_TAG "[UI]"
#include <TCLogger.h>

#include "MacroRepo.h"
#include "MacroPanelControl.h"

namespace HTXpress::AppComponents::Instrument::App {
    MacroPanelControl::MacroPanelControl() {
    }

    MacroPanelControl::~MacroPanelControl() {
    }

    auto MacroPanelControl::CreateSequence(
        const QList<SequenceInput>& inputList) const -> AppEntity::ImagingSequence::Pointer {
        auto sequence = std::make_shared<AppEntity::ImagingSequence>();

        for(const auto& input : inputList) {
            sequence->AddImagingCondition(CreateCondition(input.modality, input.is3D));
        }

        return sequence;
    }

    auto MacroPanelControl::CreateCondition(AppEntity::Modality modality,
        bool is3D) const -> AppEntity::ImagingCondition::Pointer {
        AppEntity::ImagingCondition::Pointer cond;
        cond->SetDimension(is3D? 3 : 2);

        switch(modality) {
        case AppEntity::Modality::HT:
            cond = [=]()->AppEntity::ImagingCondition::Pointer {
                auto htCond = std::make_shared<AppEntity::ImagingConditionHT>();
                htCond->SetSliceCount(is3D? 2 : 1);
                return htCond;
                }();
            break;
        case AppEntity::Modality::FL:
            cond =[=]()->AppEntity::ImagingCondition::Pointer {
                auto flCond = std::make_shared<AppEntity::ImagingConditionFL>();
                flCond->SetSliceCount(is3D? 2 : 1);
                return flCond;
                }();
            break;
        case AppEntity::Modality::BF:
            cond = std::make_shared<AppEntity::ImagingConditionBF>();
            break;
        }

        return cond;
    }

    auto MacroPanelControl::ClearAll() -> void {
        auto repo = MacroRepo::GetInstance();
        auto scenario = repo->GetImagingScenario();
        scenario->RemoveAll();
    }
}
