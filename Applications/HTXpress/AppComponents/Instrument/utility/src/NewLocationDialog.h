#pragma once
#include <memory>
#include <QDialog>

#include "Defines.h"

namespace HTXpress::AppComponents::Instrument::App {
    class NewLocationDialog : public QDialog {
        Q_OBJECT
    public:
        NewLocationDialog(QWidget* parent = nullptr);
        ~NewLocationDialog();

        auto GetLocation() const->LocationInput;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}