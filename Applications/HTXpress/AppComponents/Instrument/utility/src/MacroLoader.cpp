#define LOGGER_TAG "[UI]"

#include <QFile>
#include <QSettings>
#include <TCLogger.h>

#include "MacroFields.h"
#include "MacroLoader.h"

namespace HTXpress::AppComponents::Instrument::App {
    struct MacroLoader::Impl {
        AppEntity::ImagingScenario::Pointer scenario;
        QList<AppEntity::ImagingCondition::Pointer> conditions;
        QList<LocationInput> locations;

        auto GetValue(QSettings& qs, const QString& parentKey, const QString& key) const->QVariant;
        auto GetImagingCondition(AppEntity::Modality modality, bool is3D)->AppEntity::ImagingCondition::Pointer;

        auto LoadScenario(QSettings& qs)->void;
        auto LoadImagingCondition(QSettings& qs)->void;
        auto LoadLocations(QSettings& qs)->void;
    };

    auto MacroLoader::Impl::GetValue(QSettings& qs, const QString& parentKey, const QString& key) const -> QVariant {
        const QString wholeKey = QString("%1/%2").arg(parentKey).arg(key);
        return qs.value(wholeKey);
    }

    auto MacroLoader::Impl::GetImagingCondition(AppEntity::Modality modality,
        bool is3D) -> AppEntity::ImagingCondition::Pointer {
        for(auto cond : conditions) {
            if(!cond->CheckModality(modality)) continue;
            if(cond->Is3D() != is3D) continue;
            return cond;
        }
        return nullptr;
    }

    auto MacroLoader::Impl::LoadScenario(QSettings& qs) -> void {
        scenario = std::make_shared<AppEntity::ImagingScenario>();

        qs.beginGroup(GROUP_SCENARIO);
        auto children = qs.childGroups();
        for(auto& child : children) {
            auto startTime = GetValue(qs, child, FIELD_STARTTIME).toInt();
            auto title = GetValue(qs, child, FIELD_TITLE).toString();

            auto sequence = std::make_shared<AppEntity::ImagingSequence>();
            const QString strTimelapse = QString("%1/%2").arg(child).arg(FIELD_TIMELAPSE);
            const auto timelapseCount = GetValue(qs, strTimelapse, FIELD_COUNT).toInt();
            const auto timelapseInterval = GetValue(qs, strTimelapse, FIELD_INTERVAL).toInt();
            sequence->SetTimelapseCondition(timelapseInterval, timelapseCount);

            qs.beginGroup(QString("%1/%2").arg(child).arg(FIELD_MODALITY));
            auto modalityGroups = qs.childGroups();
            for(auto& strModality : modalityGroups) {
                auto modality = AppEntity::Modality::_from_string(GetValue(qs, strModality, FIELD_MODALITY).toString().toLatin1());
                auto is3D = GetValue(qs, strModality, FIELD_IS3D).toBool();

                auto cond = GetImagingCondition(modality, is3D);
                if(cond == nullptr) return;

                sequence->AddImagingCondition(cond);
            }

            scenario->AddSequence(startTime, title, sequence);
            qs.endGroup();
        }
        qs.endGroup();
    }

    auto MacroLoader::Impl::LoadImagingCondition(QSettings& qs) -> void{
        qs.beginGroup(GROUP_CONDITION);
        auto children = qs.childGroups();
        for(auto child : children) {
            auto modality = AppEntity::Modality::_from_string(GetValue(qs, child, FIELD_MODALITY).toString().toLatin1());

            AppEntity::ImagingCondition::Pointer cond;
            switch(modality) {
            case AppEntity::Modality::HT:
                cond = [&]()->AppEntity::ImagingCondition::Pointer {
                    auto htCond = std::make_shared<AppEntity::ImagingConditionHT>();
                    auto valueMap = htCond->GetValueMap();
                    for(auto itr=valueMap.begin(); itr!=valueMap.end(); ++itr) {
                        const auto& key = itr.key();
                        auto value = GetValue(qs, child, itr.key());
                        itr.value() = value;
                    }
                    htCond->SetValueMap(valueMap);
                    return htCond;
                }();
                break;
            case AppEntity::Modality::FL:
                cond = [&]()->AppEntity::ImagingCondition::Pointer {
                    auto flCond = std::make_shared<AppEntity::ImagingConditionFL>();
                    auto valueMap = flCond->GetValueMap();
                    for(auto itr=valueMap.begin(); itr!=valueMap.end(); ++itr) {
                        const auto& key = itr.key();
                        auto value = GetValue(qs, child, itr.key());
                        itr.value() = value;
                    }
                    flCond->SetValueMap(valueMap);
                    return flCond;
                }();
                break;
            case AppEntity::Modality::BF:
                cond = [&]()->AppEntity::ImagingCondition::Pointer {
                    auto bfCond = std::make_shared<AppEntity::ImagingConditionBF>();
                    auto valueMap = bfCond->GetValueMap();
                    for(auto itr=valueMap.begin(); itr!=valueMap.end(); ++itr) {
                        const auto& key = itr.key();
                        auto value = GetValue(qs, child, itr.key());
                        itr.value() = value;
                    }
                    bfCond->SetValueMap(valueMap);
                    return bfCond;
                }();
                break;
            }
            conditions.append(cond);
        }
        qs.endGroup();
    }

    auto MacroLoader::Impl::LoadLocations(QSettings& qs) -> void {
        const auto count = qs.beginReadArray(GROUP_LOCATION);
        for(auto idx=0; idx<count; idx++) {
            qs.setArrayIndex(idx);

            LocationInput input;

            input.values[LocationInput::posX] = qs.value(FIELD_POSX).toInt();
            input.values[LocationInput::posY] = qs.value(FIELD_POSY).toInt();
            input.values[LocationInput::posZ] = qs.value(FIELD_POSZ).toInt();
            input.values[LocationInput::countX] = qs.value(FIELD_COUNTX).toInt();
            input.values[LocationInput::countY] = qs.value(FIELD_COUNTY).toInt();
            input.values[LocationInput::gapX] = qs.value(FIELD_GAPX).toInt();
            input.values[LocationInput::gapY] = qs.value(FIELD_GAPY).toInt();

            locations.append(input);
        }
        qs.endArray();
    }

    MacroLoader::MacroLoader() : d{new Impl} {
    }

    MacroLoader::~MacroLoader() {
    }

    auto MacroLoader::Load(const QString& path) -> bool {
        if(!QFile::exists(path)) return false;

        QSettings qs(path, QSettings::IniFormat);

        d->LoadLocations(qs);
        d->LoadImagingCondition(qs);
        d->LoadScenario(qs);

        return true;
    }

    auto MacroLoader::GetImagingScenario() const -> AppEntity::ImagingScenario::Pointer {
        return d->scenario;
    }

    auto MacroLoader::GetImagingCondition(AppEntity::Modality modality,
        bool is3D) -> AppEntity::ImagingCondition::Pointer {
        return d->GetImagingCondition(modality, is3D);
    }

    auto MacroLoader::GetLocations() const -> QList<LocationInput> {
        return d->locations;
    }
};
