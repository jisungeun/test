#include "ui_NewLocationDialog.h"
#include "NewLocationDialog.h"

namespace HTXpress::AppComponents::Instrument::App {
    struct NewLocationDialog::Impl {
        Ui::NewLocationDialog ui;
    };

    NewLocationDialog::NewLocationDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
        d->ui.setupUi(this);

        connect(d->ui.cancelBtn, SIGNAL(clicked()), this, SLOT(reject()));
        connect(d->ui.doneBtn, SIGNAL(clicked()), this, SLOT(accept()));
    }

    NewLocationDialog::~NewLocationDialog() {
    }

    auto NewLocationDialog::GetLocation() const -> LocationInput {
        LocationInput input;

        input.values[LocationInput::posX] = d->ui.posXSpin->value();
        input.values[LocationInput::posY] = d->ui.posYSpin->value();
        input.values[LocationInput::posZ] = d->ui.posZSpin->value();
        input.values[LocationInput::countX] = d->ui.countXSpin->value();
        input.values[LocationInput::countY] = d->ui.countYSpin->value();
        input.values[LocationInput::gapX] = d->ui.gapXSpin->value();
        input.values[LocationInput::gapY] = d->ui.gapYSpin->value();

        return input;
    }
}