#pragma once
#include <memory>
#include <QWidget>
#include <QList>

#include "Defines.h"

namespace HTXpress::AppComponents::Instrument::App {
    class LocationPanel : public QWidget {
		Q_OBJECT

    public:
        LocationPanel(QWidget* parent = nullptr);
        ~LocationPanel();

        auto Set(const QList<LocationInput>& list)->void;
        auto Get() const->QList<LocationInput>;

        auto Add(const LocationInput& location)->void;
        auto Remove(int32_t index)->void;

        auto ClearAll()->void;

    protected slots:
        void onAdd();
        void onDelete();

    signals:
        void sigAddLocation(const LocationInput& location);
        void sigDeleteLocation(int32_t index);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}