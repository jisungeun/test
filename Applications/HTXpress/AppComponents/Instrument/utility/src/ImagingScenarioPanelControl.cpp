#include <QList>

#include "ImagingScenarioPanelControl.h"

namespace HTXpress::AppComponents::Instrument::App {
    struct ImagingScenarioPanelControl::Impl {
    };

    ImagingScenarioPanelControl::ImagingScenarioPanelControl() : d{new Impl} {
    }

    ImagingScenarioPanelControl::~ImagingScenarioPanelControl() {
    }
}
