#pragma once
#include <memory>

#include "QMainWindow"

namespace HTXpress::AppComponents::Instrument::App {
    class MainWindow : public QMainWindow {
		Q_OBJECT

	public:
		explicit MainWindow(QWidget* parent = nullptr);
		virtual ~MainWindow();

	protected slots:
		void onNew();
		void onSave();
		void onOpen();
		void onNewMacro();
		void onSaveMacro();
		void onOpenMacro();

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}