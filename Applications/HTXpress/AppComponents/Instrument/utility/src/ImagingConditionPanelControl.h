#pragma once
#include <memory>

#include <ImagingCondition.h>

namespace HTXpress::AppComponents::Instrument::App {
    class ImagingConditionPanelControl {
    public:
        enum Type {
            HT3D,
            HT2D,
            FL3D,
            FL2D,
            BF
        };

    public:
        ImagingConditionPanelControl();
        ~ImagingConditionPanelControl();

        auto Set(Type type, const AppEntity::ImagingCondition::Pointer cond)->void;
        auto Get(Type type) const->AppEntity::ImagingCondition::Pointer;

        auto Reset()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}