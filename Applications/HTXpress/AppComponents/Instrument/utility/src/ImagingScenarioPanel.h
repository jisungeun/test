#pragma once
#include <memory>
#include <QWidget>
#include <QList>

#include <ImagingScenario.h>

#include "Defines.h"

namespace HTXpress::AppComponents::Instrument::App {
    class ImagingScenarioPanel : public QWidget {
		Q_OBJECT

    public:
        ImagingScenarioPanel(QWidget* parent = nullptr);
        ~ImagingScenarioPanel();

        auto AddSequence(int32_t start, QString title)->void;
        auto SelectedIndex() const->int32_t;
        auto ClearAll()->void;

    protected slots:
        void onAdd();
        void onDelete();
        void onSequencChanged(int row, int column);
        void onSequenceSelected();

    signals:
        void sigNewScenario(int32_t start, QString title);
        void sigSequenceChanged(int32_t row, int32_t start, QString title);
        void sigSelected(int32_t index);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}