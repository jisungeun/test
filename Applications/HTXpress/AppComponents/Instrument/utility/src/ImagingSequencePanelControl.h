#pragma once
#include <memory>

#include <Experiment.h>

namespace HTXpress::AppComponents::Instrument::App {
    using Modality = AppEntity::Modality;

    class ImagingSequencePanelControl {
    public:
        ImagingSequencePanelControl();
        ~ImagingSequencePanelControl();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}