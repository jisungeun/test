#pragma once
#include <memory>

#include <Experiment.h>

namespace HTXpress::AppComponents::Instrument::App {
    struct SequenceInput {
        AppEntity::Modality modality{ AppEntity::Modality::HT };
        bool is3D{ false };
    };

    struct LocationInput {
        enum {
            posX,
            posY,
            posZ,
            countX,
            countY,
            gapX,
            gapY,
            _count
        };
        int32_t values[_count];
    };
}