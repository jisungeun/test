#define LOGGER_TAG "[UI]"

#include <QItemDelegate>
#include <QComboBox>

#include <TCLogger.h>

#include "NewSequenceDialog.h"

#include "ImagingSequencePanelControl.h"
#include "ui_ImagingSequencePanel.h"
#include "ImagingSequencePanel.h"

namespace HTXpress::AppComponents::Instrument::App {
    class ModalityDelegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/,
                              const QModelIndex& /*index*/) const override {
            auto* item = new QComboBox(parent);
            item->addItem("HT", 0);
            item->addItem("FL", 1);
            item->addItem("BF", 2);
            return item;
        }
    };

    class TypeDelegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/,
                              const QModelIndex& /*index*/) const override {
            auto* item = new QComboBox(parent);
            item->addItem("False", 0);
            item->addItem("True", 1);
            return item;
        }
    };

    struct ImagingSequencePanel::Impl {
        ImagingSequencePanelControl control;
        Ui::ImagingSequencePanel ui;

        QMap<Modality, QString> modality2str{
            {Modality::HT, "HT"},
            {Modality::FL, "FL"},
            {Modality::BF, "BF"}
        };

        QMap<QString, int32_t> str2modality;

        auto CreateRows(int32_t rowCount)->void;
        auto AddRow()->void;
        auto AddRow(AppEntity::Modality modality, bool is3D)->void;
    };

    auto ImagingSequencePanel::Impl::CreateRows(int32_t rowCount) -> void {
        ui.table->blockSignals(true);

        ui.table->clearContents();
        ui.table->setRowCount(rowCount);

        for(int32_t idx=0; idx<rowCount; idx++) {

            auto* item = new QTableWidgetItem("HT");
            item->setTextAlignment(Qt::AlignCenter);
            ui.table->setItem(idx, 0, item);

            item = new QTableWidgetItem("True");
            item->setTextAlignment(Qt::AlignCenter);
            ui.table->setItem(idx, 1, item);
        }

        ui.table->blockSignals(false);
    }

    auto ImagingSequencePanel::Impl::AddRow() -> void {
        const auto rowIdx = ui.table->rowCount();

        ui.table->blockSignals(true);

        ui.table->setRowCount(rowIdx + 1);

        auto* item = new QTableWidgetItem("HT");
        item->setTextAlignment(Qt::AlignCenter);
        ui.table->setItem(rowIdx, 0, item);

        item = new QTableWidgetItem("True");
        item->setTextAlignment(Qt::AlignCenter);
        ui.table->setItem(rowIdx, 1, item);

        ui.table->blockSignals(false);
    }

    auto ImagingSequencePanel::Impl::AddRow(AppEntity::Modality modality, bool is3D) -> void {
        AddRow();

        const auto rowIdx = ui.table->rowCount() - 1;
        ui.table->item(rowIdx, 0)->setText(modality2str[modality]);
        ui.table->item(rowIdx, 1)->setText(is3D? "True" : "False");
    }

    ImagingSequencePanel::ImagingSequencePanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);

        d->ui.table->setColumnCount(2);

        d->ui.table->setHorizontalHeaderLabels({"Modality", "3D"});
        d->ui.table->setColumnWidth(0, 120);
        d->ui.table->horizontalHeader()->setStretchLastSection(true);
        d->ui.table->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        d->ui.table->setSelectionBehavior(QAbstractItemView::SelectItems);
        d->ui.table->setSelectionMode(QAbstractItemView::SingleSelection);
        d->ui.table->setItemDelegateForColumn(0, new ModalityDelegate());
        d->ui.table->setItemDelegateForColumn(1, new TypeDelegate());

        connect(d->ui.addBtn, SIGNAL(clicked()), this, SLOT(onAdd()));
        connect(d->ui.delBtn, SIGNAL(clicked()), this, SLOT(onDelete()));
        connect(d->ui.table, SIGNAL(cellClicked(int,int)), this, SLOT(onSequenceSelected(int,int)));
        connect(d->ui.table, SIGNAL(cellChanged(int,int)), this, SLOT(onSequenceChanged(int,int)));

        for(auto itr=d->modality2str.begin(); itr!=d->modality2str.end(); ++itr) {
            d->str2modality[itr.value()] = itr.key()._to_integral();
        }
    }

    ImagingSequencePanel::~ImagingSequencePanel() {
    }

    auto ImagingSequencePanel::Set(const QList<SequenceInput>& sequences) -> void {
        const auto count = sequences.length();
        d->CreateRows(count);

        for(int32_t idx=0; idx<count; idx++) {
            const auto& seq = sequences.at(idx);
            d->ui.table->item(idx, 0)->setText(d->modality2str[seq.modality]);
            d->ui.table->item(idx, 1)->setText(seq.is3D?"True":"False");
        }
    }

    auto ImagingSequencePanel::Get() const -> QList<SequenceInput> {
        QList<SequenceInput> inputs;

        const auto rowCount = d->ui.table->rowCount();
        for(int32_t idx=0; idx<rowCount; idx++) {
            auto modality = Modality::_from_integral(d->str2modality[d->ui.table->item(idx, 0)->text()]);
            auto is3D = d->ui.table->item(idx, 1)->text() == "True";
            inputs.append({modality, is3D});
        }

        return inputs;
    }

    auto ImagingSequencePanel::Add(const SequenceInput& sequence) -> void {
        d->AddRow(sequence.modality, sequence.is3D);
    }

    auto ImagingSequencePanel::Remove(int32_t index) -> void {
        d->ui.table->removeRow(index);
    }

    auto ImagingSequencePanel::ClearAll() -> void {
        d->ui.table->blockSignals(true);
        d->ui.table->setRowCount(0);
        d->ui.table->blockSignals(false);
    }

    void ImagingSequencePanel::onAdd() {
        NewSequenceDialog dlg;
        if(dlg.exec() == QDialog::Rejected) return;

        auto modality = dlg.GetModality();
        auto is3D = dlg.Is3D();

        emit sigAddModality(modality, is3D);
    }

    void ImagingSequencePanel::onDelete() {
        auto ranges = d->ui.table->selectedRanges();
        auto rowIdx = ranges.at(0).topRow();
        emit sigDeleteModality(rowIdx);
    }

    void ImagingSequencePanel::onSequenceSelected(int row, int column) {
        emit sigSelected(row);
    }

    void ImagingSequencePanel::onSequenceChanged(int row, int column) {
        auto modality = Modality::_from_string(d->ui.table->item(row, 0)->text().toLatin1());
        auto is3D = (d->ui.table->item(row, 1)->text().compare("True") == 0);

        emit sigChangeModality(row, modality, is3D);
    }
}
