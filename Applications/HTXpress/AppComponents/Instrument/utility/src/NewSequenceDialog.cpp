#include <QMap>
#include <QButtonGroup>

#include "ui_NewSequenceDialog.h"
#include "NewSequenceDialog.h"

namespace HTXpress::AppComponents::Instrument::App {
    struct NewSequenceDialog::Impl {
        Ui::NewSequenceDialog ui;

        QMap<QString, int32_t> str2modality {
            {"HT", AppEntity::Modality::HT},
            {"FL", AppEntity::Modality::FL},
            {"BF", AppEntity::Modality::BF}
        };

        QButtonGroup* typeGroup{ nullptr };
    };

    NewSequenceDialog::NewSequenceDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
        d->ui.setupUi(this);

        d->typeGroup = new QButtonGroup(this);
        d->typeGroup->addButton(d->ui.type2DBtn, 0);
        d->typeGroup->addButton(d->ui.type3DBtn, 1);
        d->typeGroup->setExclusive(true);

        d->ui.modalityCombo->addItems(d->str2modality.keys());
        onModalitySelected(d->ui.modalityCombo->currentText());

        connect(d->ui.modalityCombo, SIGNAL(currentTextChanged(const QString&)), this, SLOT(onModalitySelected(const QString&)));
        connect(d->ui.cancelBtn, SIGNAL(clicked()), this, SLOT(reject()));
        connect(d->ui.doneBtn, SIGNAL(clicked()), this, SLOT(accept()));
    }

    NewSequenceDialog::~NewSequenceDialog() {
    }

    auto NewSequenceDialog::GetModality() const -> AppEntity::Modality {
        return AppEntity::Modality::_from_integral(d->str2modality[d->ui.modalityCombo->currentText()]);
    }

    auto NewSequenceDialog::Is3D() const -> bool {
        return (d->typeGroup->checkedId() == 1);
    }

    void NewSequenceDialog::onModalitySelected(const QString& strModality) {
        auto modality = AppEntity::Modality::_from_string(strModality.toLatin1());
        auto isBF = (modality == +AppEntity::Modality::BF);

        d->ui.type2DBtn->setChecked(isBF);
        d->ui.type3DBtn->setChecked(!isBF);
        d->ui.type2DBtn->setDisabled(isBF);
        d->ui.type3DBtn->setDisabled(isBF);
    }
}
