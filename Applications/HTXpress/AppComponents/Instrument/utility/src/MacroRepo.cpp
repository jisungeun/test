#include "MacroRepo.h"

namespace HTXpress::AppComponents::Instrument::App {
    struct MacroRepo::Impl {
        AppEntity::ImagingScenario::Pointer scenario;
    };

    MacroRepo::MacroRepo() : d{new Impl} {
        d->scenario = std::make_shared<AppEntity::ImagingScenario>();
    }

    MacroRepo::~MacroRepo() {
    }

    auto MacroRepo::GetInstance() -> Pointer {
        static Pointer theInstance{ new MacroRepo() };
        return theInstance;
    }

    auto MacroRepo::GetImagingScenario() const -> AppEntity::ImagingScenario::Pointer {
        return d->scenario;
    }
}