#pragma once
#include <memory>
#include <QMap>

#include <MotionCommand.h>

namespace HTXpress::AppComponents::Instrument::App {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

        auto CreateNew()->QMap<MotionCommandName, MotionCommand::Command::Pointer>&;
        auto Save(const QString& path)->bool;
        auto Load(const QString& path)->QList<MotionCommand::Command::Pointer>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}