#pragma once
#include <memory>
#include <QDialog>

#include <Experiment.h>

#include "Defines.h"

namespace HTXpress::AppComponents::Instrument::App {
    class NewSequenceDialog : public QDialog {
		Q_OBJECT
    public:
        NewSequenceDialog(QWidget* parent = nullptr);
        ~NewSequenceDialog();

        auto GetModality() const->AppEntity::Modality;
        auto Is3D() const->bool;

    protected slots:
        void onModalitySelected(const QString& strModality);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}