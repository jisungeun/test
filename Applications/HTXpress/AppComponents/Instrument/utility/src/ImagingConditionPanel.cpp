#include <QTableWidget>
#include <QItemDelegate>
#include <QLineEdit>
#include <QHeaderView>
#include <QMap>

#include "ImagingConditionPanelControl.h"

#include "ui_ImagingConditionPanel.h"
#include "ImagingConditionPanel.h"

namespace HTXpress::AppComponents::Instrument::App {
    class IntegerDelegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/,
                              const QModelIndex& /*index*/) const override {
            auto* lineEdit = new QLineEdit(parent);
            const auto* validator = new QIntValidator(lineEdit);
            lineEdit->setValidator(validator);
            return lineEdit;
        }
    };

    struct ImagingConditionPanel::Impl {
        ImagingConditionPanelControl control;
        Ui::ImagingConditionPanel ui;

        enum TabIndex {
            HT3D,
            HT2D,
            FL3D,
            FL2D,
            BF
        };

        enum HTParam {
            htFovX,
            htFovY,
            htSliceCount,
            htSliceStep,
            htExposure,
            htInterval,
            _htCount
        };

        enum FLParam {
            flFovX,
            flFovY,
            flSliceCount,
            flSliceStep,
            flCh0Use,
            flCh0Exposure,
            flCh0Interval,
            flCh0Intensity,
            flCh0Gain,
            flCh0ExChannel,
            flCh0EmFilter,
            flCh0ChName,
            flCh1Use,
            flCh1Exposure,
            flCh1Interval,
            flCh1Intensity,
            flCh1Gain,
            flCh1ExChannel,
            flCh1EmFilter,
            flCh1ChName,
            flCh2Use,
            flCh2Exposure,
            flCh2Interval,
            flCh2Intensity,
            flCh2Gain,
            flCh2ExChannel,
            flCh2EmFilter,
            flCh2ChName,
            _flCount
        };

        enum BFParam {
            bfFovX,
            bfFovY,
            bfColorMode,
            bfExposure,
            bfInterval,
            _bfCount
        };


        QMap<TabIndex, QTableWidget*> tabs;

        auto CreateTable(QWidget* parent)->QTableWidget*;
        auto CreateRow(QTableWidget* table, int32_t rowIdx, const QString& name, int32_t value = 0)->void;
        auto SetValue(QTableWidget* table, int32_t rowIdx, int32_t value)->void;
        auto SetValue(QTableWidget* table, int32_t rowIdx, const QString& value)->void;
        auto SetValue(QTableWidget* table, int32_t rowIdx, const AppEntity::FLFilter& filter)->void;
        auto GetValue(QTableWidget* table, int32_t rowIdx) const->int32_t;
        auto GetString(QTableWidget* table, int32_t rowIdx) const->QString;
        auto GetFLFilter(QTableWidget* table, int32_t rowIdx) const->AppEntity::FLFilter;

        auto InitHT(QTableWidget* table)->void;
        auto InitFL(QTableWidget* table)->void;
        auto InitBF(QTableWidget* table)->void;

        auto EnableEditing(bool enable)->void;
    };

    auto ImagingConditionPanel::Impl::CreateTable(QWidget* parent) -> QTableWidget* {
        auto* table = new QTableWidget(parent);

        table->setColumnCount(2);

        table->setHorizontalHeaderLabels({"Param", "Value"});
        table->setColumnWidth(0, 150);
        table->horizontalHeader()->setStretchLastSection(true);
        table->setSelectionBehavior(QAbstractItemView::SelectItems);
        table->setSelectionMode(QAbstractItemView::SingleSelection);
        table->setItemDelegateForColumn(1, new IntegerDelegate());

        return table;
    }

    auto ImagingConditionPanel::Impl::CreateRow(QTableWidget* table, int32_t rowIdx, const QString& name, int32_t value) -> void {
        auto* item = new QTableWidgetItem(name);
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(rowIdx, 0, item);

        item = new QTableWidgetItem(QString::number(value));
        item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        table->setItem(rowIdx, 1, item);
    }

    auto ImagingConditionPanel::Impl::SetValue(QTableWidget* table, int32_t rowIdx, int32_t value) -> void {
        auto* item = table->item(rowIdx, 1);
        item->setText(QString::number(value));
    }

    auto ImagingConditionPanel::Impl::SetValue(QTableWidget* table, int32_t rowIdx, const QString& value) -> void {
        auto* item = table->item(rowIdx, 1);
        item->setText(value);
    }

    auto ImagingConditionPanel::Impl::SetValue(QTableWidget* table, int32_t rowIdx,
                                               const AppEntity::FLFilter& filter) -> void {
        SetValue(table, rowIdx, QString("%1/%2").arg(filter.GetWaveLength()).arg(filter.GetBandwidth()));
    }

    auto ImagingConditionPanel::Impl::GetValue(QTableWidget* table, int32_t rowIdx) const -> int32_t {
        auto* item = table->item(rowIdx, 1);
        return item->text().toInt();
    }

    auto ImagingConditionPanel::Impl::GetString(QTableWidget* table, int32_t rowIdx) const -> QString {
        auto* item = table->item(rowIdx, 1);
        return item->text();
    }

    auto ImagingConditionPanel::Impl::GetFLFilter(QTableWidget* table, int32_t rowIdx) const -> AppEntity::FLFilter {
        const auto str = GetString(table, rowIdx);
        const auto toks = str.split("/");
        if(toks.size() != 2) return AppEntity::FLFilter();
        return AppEntity::FLFilter(toks.at(0).toInt(), toks.at(1).toInt());
    }

    auto ImagingConditionPanel::Impl::InitHT(QTableWidget* table) -> void {
        table->setRowCount(HTParam::_htCount);

        CreateRow(table, HTParam::htFovX, "FOV X");
        CreateRow(table, HTParam::htFovY, "FOV Y");
        CreateRow(table, HTParam::htSliceCount, "Slice Count", 1);
        CreateRow(table, HTParam::htSliceStep, "Slice Step");
        CreateRow(table, HTParam::htExposure, "Exposure");
        CreateRow(table, HTParam::htInterval, "Interval");
    }

    auto ImagingConditionPanel::Impl::InitFL(QTableWidget* table) -> void {
        table->setRowCount(FLParam::_flCount);

        CreateRow(table, FLParam::flFovX, "FOV X");
        CreateRow(table, FLParam::flFovY, "FOV Y");
        CreateRow(table, FLParam::flSliceCount, "Slice Count", 1);
        CreateRow(table, FLParam::flSliceStep, "Slice Step");
        CreateRow(table, FLParam::flCh0Use, "CH0 Use");
        CreateRow(table, FLParam::flCh0Exposure, "CH0 Exposure");
        CreateRow(table, FLParam::flCh0Interval, "CH0 Interval");
        CreateRow(table, FLParam::flCh0Intensity, "CH0 Intensity");
        CreateRow(table, FLParam::flCh0Gain, "CH0 Gain");
        CreateRow(table, FLParam::flCh0ExChannel, "CH0 Excitation");
        CreateRow(table, FLParam::flCh0EmFilter, "CH0 Emission");
        CreateRow(table, FLParam::flCh1Use, "CH1 Use");
        CreateRow(table, FLParam::flCh1Exposure, "CH1 Exposure");
        CreateRow(table, FLParam::flCh1Interval, "CH1 Interval");
        CreateRow(table, FLParam::flCh1Intensity, "CH1 Intensity");
        CreateRow(table, FLParam::flCh1Gain, "CH0 Gain");
        CreateRow(table, FLParam::flCh1ExChannel, "CH1 Excitation");
        CreateRow(table, FLParam::flCh1EmFilter, "CH1 Emission");
        CreateRow(table, FLParam::flCh2Use, "CH2 Use");
        CreateRow(table, FLParam::flCh2Exposure, "CH2 Exposure");
        CreateRow(table, FLParam::flCh2Interval, "CH2 Interval");
        CreateRow(table, FLParam::flCh2Intensity, "CH2 Intensity");
        CreateRow(table, FLParam::flCh2Gain, "CH0 Gain");
        CreateRow(table, FLParam::flCh2ExChannel, "CH2 Excitation");
        CreateRow(table, FLParam::flCh2EmFilter, "CH2 Emission");
    }

    auto ImagingConditionPanel::Impl::InitBF(QTableWidget* table) -> void {
        table->setRowCount(BFParam::_bfCount);

        CreateRow(table, BFParam::bfFovX, "FOV X");
        CreateRow(table, BFParam::bfFovY, "FOV Y");
        CreateRow(table, BFParam::bfColorMode, "Color Mode");
        CreateRow(table, BFParam::bfExposure, "Exposure");
        CreateRow(table, BFParam::bfInterval, "Interval");
    }

    auto ImagingConditionPanel::Impl::EnableEditing(bool enable) -> void {
        ui.editBtn->setEnabled(!enable);
        ui.applyBtn->setEnabled(enable);
        ui.restoreBtn->setEnabled(enable);

        for(auto* table : tabs) {
            const auto rows = table->rowCount();
            for(auto idx=0; idx<rows; idx++) {
                auto* item = table->item(idx, 1);
                if(enable) {
                    item->setFlags(item->flags() | Qt::ItemIsEditable);
                } else {
                    item->setFlags(item->flags() & ~QFlags(Qt::ItemIsEditable));
                }
            }
        }
    }

    ImagingConditionPanel::ImagingConditionPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);

        d->tabs[Impl::TabIndex::HT3D] = d->CreateTable(d->ui.tabWidget);
        d->tabs[Impl::TabIndex::HT2D] = d->CreateTable(d->ui.tabWidget);
        d->tabs[Impl::TabIndex::FL3D] = d->CreateTable(d->ui.tabWidget);
        d->tabs[Impl::TabIndex::FL2D] = d->CreateTable(d->ui.tabWidget);
        d->tabs[Impl::TabIndex::BF] = d->CreateTable(d->ui.tabWidget);


        d->ui.tabWidget->addTab(d->tabs[Impl::TabIndex::HT3D], "HT 3D");
        d->ui.tabWidget->addTab(d->tabs[Impl::TabIndex::HT2D], "HT 2D");
        d->ui.tabWidget->addTab(d->tabs[Impl::TabIndex::FL3D], "FL 3D");
        d->ui.tabWidget->addTab(d->tabs[Impl::TabIndex::FL2D], "FL 2D");
        d->ui.tabWidget->addTab(d->tabs[Impl::TabIndex::BF], "BF");

        d->InitHT(d->tabs[Impl::TabIndex::HT3D]);
        d->InitHT(d->tabs[Impl::TabIndex::HT2D]);
        d->InitFL(d->tabs[Impl::TabIndex::FL3D]);
        d->InitFL(d->tabs[Impl::TabIndex::FL2D]);
        d->InitBF(d->tabs[Impl::TabIndex::BF]);

        d->ui.tabWidget->setCurrentIndex(0);

        onApply();
        d->EnableEditing(false);

        connect(d->ui.editBtn, SIGNAL(clicked()), this, SLOT(onEdit()));
        connect(d->ui.applyBtn, SIGNAL(clicked()), this, SLOT(onApply()));
        connect(d->ui.restoreBtn, SIGNAL(clicked()), this, SLOT(onRestore()));
    }

    ImagingConditionPanel::~ImagingConditionPanel() {
    }

    auto ImagingConditionPanel::SetHT3DCondition(AppEntity::ImagingCondition::Pointer cond) -> void {
        d->control.Set(ImagingConditionPanelControl::Type::HT3D, cond);

        auto* condHT = dynamic_cast<AppEntity::ImagingConditionHT*>(cond.get());
        if(condHT == nullptr) return;

        d->SetValue(d->tabs[Impl::TabIndex::HT3D], Impl::HTParam::htSliceCount, condHT->GetSliceCount());
        d->SetValue(d->tabs[Impl::TabIndex::HT3D], Impl::HTParam::htSliceStep, condHT->GetSliceStep());
        d->SetValue(d->tabs[Impl::TabIndex::HT3D], Impl::HTParam::htExposure, condHT->GetExposure());
        d->SetValue(d->tabs[Impl::TabIndex::HT3D], Impl::HTParam::htInterval, condHT->GetInterval());
    }

    auto ImagingConditionPanel::SetHT2DCondition(AppEntity::ImagingCondition::Pointer cond) -> void {
        d->control.Set(ImagingConditionPanelControl::Type::HT2D, cond);

        auto* condHT = dynamic_cast<AppEntity::ImagingConditionHT*>(cond.get());
        if(condHT == nullptr) return;

        d->SetValue(d->tabs[Impl::TabIndex::HT2D], Impl::HTParam::htSliceCount, condHT->GetSliceCount());
        d->SetValue(d->tabs[Impl::TabIndex::HT2D], Impl::HTParam::htSliceStep, condHT->GetSliceStep());
        d->SetValue(d->tabs[Impl::TabIndex::HT2D], Impl::HTParam::htExposure, condHT->GetExposure());
        d->SetValue(d->tabs[Impl::TabIndex::HT2D], Impl::HTParam::htInterval, condHT->GetInterval());
    }

    auto ImagingConditionPanel::SetFL3DCondition(AppEntity::ImagingCondition::Pointer cond) -> void {
        d->control.Set(ImagingConditionPanelControl::Type::FL3D, cond);

        auto* condFL = dynamic_cast<AppEntity::ImagingConditionFL*>(cond.get());
        if(condFL == nullptr) return;

        d->SetValue(d->tabs[Impl::TabIndex::FL3D], Impl::FLParam::flSliceCount, condFL->GetSliceCount());
        d->SetValue(d->tabs[Impl::TabIndex::FL3D], Impl::FLParam::flSliceStep, condFL->GetSliceStep());

        const auto channels = condFL->GetChannels();
        for(auto channel : channels) {
            auto use = Impl::FLParam::flCh0Use + (channel*8);
            auto exposure = Impl::FLParam::flCh0Exposure + (channel*8);
            auto interval = Impl::FLParam::flCh0Interval + (channel*8);
            auto intensity = Impl::FLParam::flCh0Intensity + (channel*8);
            auto gain = Impl::FLParam::flCh0Gain + (channel*8);
            auto exFilter = Impl::FLParam::flCh0ExChannel + (channel*8);
            auto emFilter = Impl::FLParam::flCh0EmFilter + (channel*8);
            auto name = Impl::FLParam::flCh0EmFilter + (channel*8);

            d->SetValue(d->tabs[Impl::TabIndex::FL3D], use, 1);
            d->SetValue(d->tabs[Impl::TabIndex::FL3D], exposure, condFL->GetExposure(channel));
            d->SetValue(d->tabs[Impl::TabIndex::FL3D], interval, condFL->GetInterval(channel));
            d->SetValue(d->tabs[Impl::TabIndex::FL3D], intensity, condFL->GetIntensity(channel));
            d->SetValue(d->tabs[Impl::TabIndex::FL3D], gain, condFL->GetGain(channel));
            d->SetValue(d->tabs[Impl::TabIndex::FL3D], exFilter, condFL->GetExFilter(channel));
            d->SetValue(d->tabs[Impl::TabIndex::FL3D], emFilter, condFL->GetEmFilter(channel));
            d->SetValue(d->tabs[Impl::TabIndex::FL3D], name, condFL->GetName(channel));
        }
    }

    auto ImagingConditionPanel::SetFL2DCondition(AppEntity::ImagingCondition::Pointer cond) -> void {
        d->control.Set(ImagingConditionPanelControl::Type::FL2D, cond);

        auto* condFL = dynamic_cast<AppEntity::ImagingConditionFL*>(cond.get());
        if(condFL == nullptr) return;

        d->SetValue(d->tabs[Impl::TabIndex::FL2D], Impl::FLParam::flSliceCount, condFL->GetSliceCount());
        d->SetValue(d->tabs[Impl::TabIndex::FL2D], Impl::FLParam::flSliceStep, condFL->GetSliceStep());

        const auto channels = condFL->GetChannels();
        for(auto channel : channels) {
            auto use = Impl::FLParam::flCh0Use + (channel*8);
            auto exposure = Impl::FLParam::flCh0Exposure + (channel*8);
            auto interval = Impl::FLParam::flCh0Interval + (channel*8);
            auto intensity = Impl::FLParam::flCh0Intensity + (channel*8);
            auto gain = Impl::FLParam::flCh0Gain + (channel*8);
            auto exFilter = Impl::FLParam::flCh0ExChannel + (channel*8);
            auto emFilter = Impl::FLParam::flCh0EmFilter + (channel*8);
            auto name = Impl::FLParam::flCh0EmFilter + (channel*8);

            d->SetValue(d->tabs[Impl::TabIndex::FL2D], use, 1);
            d->SetValue(d->tabs[Impl::TabIndex::FL2D], exposure, condFL->GetExposure(channel));
            d->SetValue(d->tabs[Impl::TabIndex::FL2D], interval, condFL->GetInterval(channel));
            d->SetValue(d->tabs[Impl::TabIndex::FL2D], intensity, condFL->GetIntensity(channel));
            d->SetValue(d->tabs[Impl::TabIndex::FL2D], gain, condFL->GetGain(channel));
            d->SetValue(d->tabs[Impl::TabIndex::FL2D], exFilter, condFL->GetExFilter(channel));
            d->SetValue(d->tabs[Impl::TabIndex::FL2D], emFilter, condFL->GetEmFilter(channel));
            d->SetValue(d->tabs[Impl::TabIndex::FL2D], name, condFL->GetName(channel));
        }
    }

    auto ImagingConditionPanel::SetBFCondition(AppEntity::ImagingCondition::Pointer cond) -> void {
        d->control.Set(ImagingConditionPanelControl::Type::BF, cond);

        auto* condBF = dynamic_cast<AppEntity::ImagingConditionBF*>(cond.get());
        if(condBF == nullptr) return;

        d->SetValue(d->tabs[Impl::TabIndex::BF], Impl::BFParam::bfColorMode, condBF->GetColorMode());
        d->SetValue(d->tabs[Impl::TabIndex::BF], Impl::BFParam::bfExposure, condBF->GetExposure());
        d->SetValue(d->tabs[Impl::TabIndex::BF], Impl::BFParam::bfInterval, condBF->GetInterval());
    }

    auto ImagingConditionPanel::GetHT3DCondition() const -> AppEntity::ImagingCondition::Pointer {
        auto cond = std::make_shared<AppEntity::ImagingConditionHT>();

        cond->SetSliceCount(d->GetValue(d->tabs[Impl::TabIndex::HT3D], Impl::HTParam::htSliceCount));
        cond->SetSliceStep(d->GetValue(d->tabs[Impl::TabIndex::HT3D], Impl::HTParam::htSliceStep));
        cond->SetExposure(d->GetValue(d->tabs[Impl::TabIndex::HT3D], Impl::HTParam::htExposure));
        cond->SetInterval(d->GetValue(d->tabs[Impl::TabIndex::HT3D], Impl::HTParam::htInterval));

        if(cond->GetSliceCount() < 2) {
            cond->SetSliceCount(2);
        }

        return cond;
    }

    auto ImagingConditionPanel::GetHT2DCondition() const -> AppEntity::ImagingCondition::Pointer {
        auto cond = std::make_shared<AppEntity::ImagingConditionHT>();

        cond->SetSliceCount(d->GetValue(d->tabs[Impl::TabIndex::HT2D], Impl::HTParam::htSliceCount));
        cond->SetSliceStep(d->GetValue(d->tabs[Impl::TabIndex::HT2D], Impl::HTParam::htSliceStep));
        cond->SetExposure(d->GetValue(d->tabs[Impl::TabIndex::HT2D], Impl::HTParam::htExposure));
        cond->SetInterval(d->GetValue(d->tabs[Impl::TabIndex::HT2D], Impl::HTParam::htInterval));

        cond->SetSliceCount(1);

        return cond;
    }

    auto ImagingConditionPanel::GetFL3DCondition() const -> AppEntity::ImagingCondition::Pointer {
        auto cond = std::make_shared<AppEntity::ImagingConditionFL>();

        cond->SetSliceCount(d->GetValue(d->tabs[Impl::TabIndex::FL3D], Impl::FLParam::flSliceCount));
        cond->SetSliceStep(d->GetValue(d->tabs[Impl::TabIndex::FL3D], Impl::FLParam::flSliceStep));

        for(int32_t channel=0; channel<3; channel++) {
            auto use = Impl::FLParam::flCh0Use + (channel*8);
            auto exposure = Impl::FLParam::flCh0Exposure + (channel*8);
            auto interval = Impl::FLParam::flCh0Interval + (channel*8);
            auto intensity = Impl::FLParam::flCh0Intensity + (channel*8);
            auto gain = Impl::FLParam::flCh0Gain + (channel*8);
            auto exChannel = Impl::FLParam::flCh0ExChannel + (channel*8);
            auto emFilter = Impl::FLParam::flCh0EmFilter + (channel*8);
            auto chName = Impl::FLParam::flCh0ChName + (channel*8);

            if(d->GetValue(d->tabs[Impl::TabIndex::FL3D], use) == 0) continue;

            cond->SetChannel(channel,
                            d->GetValue(d->tabs[Impl::TabIndex::FL3D], exposure),
                            d->GetValue(d->tabs[Impl::TabIndex::FL3D], interval),
                            d->GetValue(d->tabs[Impl::TabIndex::FL3D], intensity),
                            d->GetValue(d->tabs[Impl::TabIndex::FL3D], gain),
                            d->GetFLFilter(d->tabs[Impl::TabIndex::FL3D], exChannel),
                            d->GetFLFilter(d->tabs[Impl::TabIndex::FL3D], emFilter),
                            d->GetString(d->tabs[Impl::TabIndex::FL3D], chName));
        }

        if(cond->GetSliceCount() < 2) {
            cond->SetSliceCount(2);
        }

        return cond;
    }

    auto ImagingConditionPanel::GetFL2DCondition() const -> AppEntity::ImagingCondition::Pointer {
        auto cond = std::make_shared<AppEntity::ImagingConditionFL>();

        cond->SetSliceCount(d->GetValue(d->tabs[Impl::TabIndex::FL2D], Impl::FLParam::flSliceCount));
        cond->SetSliceStep(d->GetValue(d->tabs[Impl::TabIndex::FL2D], Impl::FLParam::flSliceStep));

        for(int32_t channel=0; channel<3; channel++) {
            auto use = Impl::FLParam::flCh0Use + (channel*8);
            auto exposure = Impl::FLParam::flCh0Exposure + (channel*8);
            auto interval = Impl::FLParam::flCh0Interval + (channel*8);
            auto intensity = Impl::FLParam::flCh0Intensity + (channel*8);
            auto gain = Impl::FLParam::flCh0Gain + (channel*8);
            auto exChannel = Impl::FLParam::flCh0ExChannel + (channel*8);
            auto emFilter = Impl::FLParam::flCh0EmFilter + (channel*8);
            auto chName = Impl::FLParam::flCh0ChName + (channel*8);

            if(d->GetValue(d->tabs[Impl::TabIndex::FL2D], use) == 0) continue;

            cond->SetChannel(channel,
                            d->GetValue(d->tabs[Impl::TabIndex::FL2D], exposure),
                            d->GetValue(d->tabs[Impl::TabIndex::FL2D], interval),
                            d->GetValue(d->tabs[Impl::TabIndex::FL2D], intensity),
                            d->GetValue(d->tabs[Impl::TabIndex::FL2D], gain),
                            d->GetFLFilter(d->tabs[Impl::TabIndex::FL2D], exChannel),
                            d->GetFLFilter(d->tabs[Impl::TabIndex::FL2D], emFilter),
                            d->GetString(d->tabs[Impl::TabIndex::FL2D], chName));
        }

        cond->SetSliceCount(1);

        return cond;
    }

    auto ImagingConditionPanel::GetBFCondition() const -> AppEntity::ImagingCondition::Pointer {
        auto cond = std::make_shared<AppEntity::ImagingConditionBF>();

        cond->SetColorMode(d->GetValue(d->tabs[Impl::TabIndex::BF], Impl::BFParam::bfColorMode));
        cond->SetExposure(d->GetValue(d->tabs[Impl::TabIndex::BF], Impl::BFParam::bfExposure));
        cond->SetInterval(d->GetValue(d->tabs[Impl::TabIndex::BF], Impl::BFParam::bfInterval));

        cond->SetSliceCount(1);

        return cond;
    }

    auto ImagingConditionPanel::ClearAll() -> void {
        d->control.Reset();
        onRestore();
    }

    void ImagingConditionPanel::onEdit() {
        d->EnableEditing(true);
    }

    void ImagingConditionPanel::onApply() {
        using Type = ImagingConditionPanelControl::Type;

        d->EnableEditing(false);

        d->control.Set(Type::HT3D, GetHT3DCondition());
        d->control.Set(Type::HT2D, GetHT2DCondition());
        d->control.Set(Type::FL3D, GetFL3DCondition());
        d->control.Set(Type::FL2D, GetFL2DCondition());
        d->control.Set(Type::BF, GetBFCondition());

        emit sigApply();
    }

    void ImagingConditionPanel::onRestore() {
        using Type = ImagingConditionPanelControl::Type;

        d->EnableEditing(false);

        SetHT3DCondition(d->control.Get(Type::HT3D));
        SetHT2DCondition(d->control.Get(Type::HT2D));
        SetFL3DCondition(d->control.Get(Type::FL3D));
        SetFL2DCondition(d->control.Get(Type::FL2D));
        SetBFCondition(d->control.Get(Type::BF));
    }
}
