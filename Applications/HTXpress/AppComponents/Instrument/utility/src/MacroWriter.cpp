#include <QFile>
#include <QSettings>

#include "MacroFields.h"
#include "MacroWriter.h"

namespace HTXpress::AppComponents::Instrument::App {
    struct MacroWriter::Impl {
        AppEntity::ImagingScenario::Pointer scenario;
        QList<LocationInput> locations;

        auto SetValue(QSettings& qs, const QString& parentKey, const QString& key, const QVariant& value)->QString;
        auto WriteScenario(QSettings& qs)->void;
        auto WriteImagingCondition(QSettings& qs)->void;
        auto WriteLocations(QSettings& qs)->void;
    };

    auto MacroWriter::Impl::SetValue(QSettings& qs, const QString& parentKey, const QString& key,
        const QVariant& value) -> QString {
        QString wholeKey = QString("%1/%2").arg(parentKey).arg(key);
        qs.setValue(wholeKey, value);
        return wholeKey;
    }

    auto MacroWriter::Impl::WriteScenario(QSettings& qs) -> void {
        const auto count = scenario->GetCount();

        qs.beginGroup(GROUP_SCENARIO);
        for(auto idx=0; idx<count; idx++) {
            const QString strSequenceIndex = QString::number(idx);
            SetValue(qs, strSequenceIndex, FIELD_STARTTIME, scenario->GetStartTime(idx));
            SetValue(qs, strSequenceIndex, FIELD_TITLE, scenario->GetTitle(idx));

            const auto sequence = scenario->GetSequence(idx);
            const QString strTimelapse = QString("%1/%2").arg(strSequenceIndex).arg(FIELD_TIMELAPSE);
            SetValue(qs, strTimelapse, FIELD_COUNT, sequence->GetTimeCount());
            SetValue(qs, strTimelapse, FIELD_INTERVAL, sequence->GetInterval());

            const auto modalityCount = sequence->GetModalityCount();
            for(auto modalityIdx=0; modalityIdx<modalityCount; modalityIdx++) {
                const QString strModalityIndex = QString("%1/%2/%3").arg(strSequenceIndex).arg(FIELD_MODALITY).arg(modalityIdx);

                auto cond = sequence->GetImagingCondition(modalityIdx);
                SetValue(qs, strModalityIndex, FIELD_MODALITY, cond->GetModality()._to_string());
                SetValue(qs, strModalityIndex, FIELD_IS3D, cond->Is3D());
            }
        }
        qs.endGroup();
    }

    auto MacroWriter::Impl::WriteImagingCondition(QSettings& qs) -> void {
        QList<AppEntity::ImagingCondition::Pointer> conditions;

        auto findCondition = [=](AppEntity::Modality modality, int32_t sliceCount)->bool {
            for(auto condRef : conditions) {
                if(condRef->GetModality() != modality) continue;
                if(condRef->GetSliceCount() != sliceCount) continue;
                return true;
            }
            return false;
        };

        const auto count = scenario->GetCount();
        for(auto idx=0; idx<count; idx++) {
            auto sequence = scenario->GetSequence(idx);

            const auto modalityCount = sequence->GetModalityCount();
            for(auto modalityIdx=0; modalityIdx<modalityCount; modalityIdx++) {
                auto cond = sequence->GetImagingCondition(modalityIdx);
                auto modality = cond->GetModality();
                auto sliceCount = cond->GetSliceCount();

                if (std::any_of(conditions.cbegin(), conditions.cend(),
                                [=](const AppEntity::ImagingCondition::Pointer& condRef) {
                                    return *condRef == *cond;
                                })) {
                    continue;
                }

                conditions.push_back(cond);
            }
        }

        qs.beginGroup(GROUP_CONDITION);
        int32_t condIdx = 0;
        for(auto cond : conditions) {
            const auto map = cond->GetValueMap();
            for(auto itr=map.begin(); itr!=map.end(); ++itr) {
                const auto strCondIdx = QString::number(condIdx);
                SetValue(qs, strCondIdx, itr.key(), itr.value());
            }
            condIdx = condIdx + 1;
        }
        qs.endGroup();
    }

    auto MacroWriter::Impl::WriteLocations(QSettings& qs) -> void {
        qs.beginWriteArray(GROUP_LOCATION);
        int32_t locIdx = 0;
        for(auto& location : locations) {
            qs.setArrayIndex(locIdx++);

            qs.setValue(FIELD_POSX, location.values[LocationInput::posX]);
            qs.setValue(FIELD_POSY, location.values[LocationInput::posY]);
            qs.setValue(FIELD_POSZ, location.values[LocationInput::posZ]);
            qs.setValue(FIELD_COUNTX, location.values[LocationInput::countX]);
            qs.setValue(FIELD_COUNTY, location.values[LocationInput::countY]);
            qs.setValue(FIELD_GAPX, location.values[LocationInput::gapX]);
            qs.setValue(FIELD_GAPY, location.values[LocationInput::gapY]);
        }
        qs.endArray();
    }

    MacroWriter::MacroWriter() : d{new Impl} {
    }

    MacroWriter::~MacroWriter() {
    }

    auto MacroWriter::SetImagingScenario(const AppEntity::ImagingScenario::Pointer scenario) -> void {
        d->scenario = scenario;
    }

    auto MacroWriter::SetLocations(const QList<LocationInput>& list) -> void {
        d->locations = list;
    }

    auto MacroWriter::Write(const QString& path) -> bool {
        if(QFile::exists(path)) QFile::remove(path);

        QSettings qs(path, QSettings::IniFormat);

        d->WriteScenario(qs);
        d->WriteImagingCondition(qs);
        d->WriteLocations(qs);

        return true;
    }
}