#include <QList>

#include "ImagingSequencePanelControl.h"

namespace HTXpress::AppComponents::Instrument::App {
    struct ImagingSequencePanelControl::Impl {
    };

    ImagingSequencePanelControl::ImagingSequencePanelControl() : d{new Impl} {
    }

    ImagingSequencePanelControl::~ImagingSequencePanelControl() {
    }
}
