#include <QItemDelegate>
#include <QLineEdit>

#include <InputDialog.h>

#include "ImagingScenarioPanelControl.h"

#include "ui_ImagingScenarioPanel.h"
#include "ImagingScenarioPanel.h"

namespace HTXpress::AppComponents::Instrument::App {
    class IntegerDelegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/,
                              const QModelIndex& /*index*/) const override {
            auto* lineEdit = new QLineEdit(parent);
            const auto* validator = new QIntValidator(lineEdit);
            lineEdit->setValidator(validator);
            return lineEdit;
        }
    };

    struct ImagingScenarioPanel::Impl {
        ImagingScenarioPanelControl control;
        Ui::ImagingScenarioPanel ui;
    };

    ImagingScenarioPanel::ImagingScenarioPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);

        d->ui.table->setColumnCount(2);

        d->ui.table->setHorizontalHeaderLabels({"Start", "Title"});
        d->ui.table->setColumnWidth(0, 50);
        d->ui.table->horizontalHeader()->setStretchLastSection(true);
        d->ui.table->setSelectionBehavior(QAbstractItemView::SelectItems);
        d->ui.table->setSelectionMode(QAbstractItemView::SingleSelection);
        d->ui.table->setItemDelegateForColumn(0, new IntegerDelegate());

        connect(d->ui.addBtn, SIGNAL(clicked()), this, SLOT(onAdd()));
        connect(d->ui.delBtn, SIGNAL(clicked()), this, SLOT(onDelete()));
        connect(d->ui.table, SIGNAL(cellChanged(int,int)), this, SLOT(onSequencChanged(int,int)));
        connect(d->ui.table, SIGNAL(itemSelectionChanged()), this, SLOT(onSequenceSelected()));
    }

    ImagingScenarioPanel::~ImagingScenarioPanel() {
    }

    auto ImagingScenarioPanel::AddSequence(int32_t start, QString title) -> void {
        const auto rowIdx = d->ui.table->rowCount();

        d->ui.table->setRowCount(rowIdx + 1);

        d->ui.table->blockSignals(true);

        auto* item = new QTableWidgetItem(QString::number(start));
        item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
        d->ui.table->setItem(rowIdx, 0, item);

        item = new QTableWidgetItem(title);
        d->ui.table->setItem(rowIdx, 1, item);

        d->ui.table->blockSignals(false);

        auto modelIndex = d->ui.table->model()->index(rowIdx, 0);
        d->ui.table->setCurrentIndex(modelIndex);
    }

    auto ImagingScenarioPanel::SelectedIndex() const -> int32_t {
        auto& ranges = d->ui.table->selectedRanges();
        if(ranges.count() == 0) return -1;
        return ranges.at(0).topRow();
    }

    auto ImagingScenarioPanel::ClearAll() -> void {
        d->ui.table->blockSignals(true);
        d->ui.table->setRowCount(0);
        d->ui.table->blockSignals(false);
    }

    void ImagingScenarioPanel::onAdd() {
        const auto start = TC::InputDialog::getInt(this, "Add Scenario", "Start time");
        const auto title = TC::InputDialog::getText(this, "Add Scenario", "Title");

        emit sigNewScenario(start, title);
    }

    void ImagingScenarioPanel::onDelete() {
        auto ranges = d->ui.table->selectedRanges();
        auto rowIdx = ranges.at(0).topRow();
        d->ui.table->removeRow(rowIdx);
    }

    void ImagingScenarioPanel::onSequencChanged(int row, int column) {
        int32_t start= d->ui.table->item(row, 0)->text().toInt();
        QString title = d->ui.table->item(row, 1)->text();

        emit sigSequenceChanged(row, start, title);
    }

    void ImagingScenarioPanel::onSequenceSelected() {
        emit sigSelected(d->ui.table->currentRow());
    }
}
