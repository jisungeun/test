#include "ImagingSequenceDialogControl.h"
#include "ui_ImagingSequenceDialog.h"
#include "ImagingSequenceDialog.h"

namespace HTXpress::AppComponents::Instrument::App {
    struct ImagingSequenceDialog::Impl {
        Ui::ImagingSequenceDialog ui;
        ImagingSequenceDialogControl control;
    };

    ImagingSequenceDialog::ImagingSequenceDialog(QWidget* parent) : QDialog(parent), d{new Impl} {
        d->ui.setupUi(this);

        connect(d->ui.sequencePanel, SIGNAL(sigAddModality(AppEntity::Modality, bool)), this, 
                SLOT(onNewModality(AppEntity::Modality, bool)));
        connect(d->ui.sequencePanel, SIGNAL(sigDeleteModality(int32_t)), this, SLOT(onDeleteModality(int32_t)));
        connect(d->ui.cancelBtn, SIGNAL(clicked()), this, SLOT(reject()));
        connect(d->ui.doneBtn, SIGNAL(clicked()), this, SLOT(accept()));
    }

    ImagingSequenceDialog::~ImagingSequenceDialog() {
    }

    auto ImagingSequenceDialog::Get() const -> QList<SequenceInput> {
        return d->ui.sequencePanel->Get();
    }

    void ImagingSequenceDialog::onNewModality(AppEntity::Modality modality, bool is3D) {
        d->ui.sequencePanel->Add({modality, is3D}); 
    }

    void ImagingSequenceDialog::onDeleteModality(int32_t index) {
        d->ui.sequencePanel->Remove(index);
    }
}
