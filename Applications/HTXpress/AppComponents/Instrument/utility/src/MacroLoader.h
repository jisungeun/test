#pragma once
#include <memory>
#include <QString>

#include <ImagingScenario.h>

#include "Defines.h"


namespace HTXpress::AppComponents::Instrument::App {
    class MacroLoader {
    public:
        MacroLoader();
        ~MacroLoader();

        auto Load(const QString& path)->bool;

        auto GetImagingScenario() const->AppEntity::ImagingScenario::Pointer;
        auto GetImagingCondition(AppEntity::Modality modality, bool is3D)->AppEntity::ImagingCondition::Pointer;
        auto GetLocations() const->QList<LocationInput>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}