#include <QApplication>
#include <QFile>
#include <QStandardPaths>

#include <TCLogger.h>

#include "MainWindow.h"

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);

	app.setApplicationName("HTXMacroConsole");
	app.setOrganizationName("Tomocube, Inc.");
	app.setOrganizationDomain("www.tomocube.com");

	QFile f(":qdarkstyle/style.qss");

	if (!f.exists()) {
		printf("Unable to set stylesheet, file not found\n");
	} else {
		f.open(QFile::ReadOnly | QFile::Text);
		QTextStream ts(&f);
		qApp->setStyleSheet(ts.readAll());
	}

	auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
	TC::Logger::Initialize(QString("%1/log/HTXMacroConsole.log").arg(appDataPath), QString("%1/log/HTXMacroConsoleError.log").arg(appDataPath));
	TC::Logger::SetTraceLevel();

	HTXpress::AppComponents::Instrument::App::MainWindow w;
	w.show();

	return app.exec();
}
