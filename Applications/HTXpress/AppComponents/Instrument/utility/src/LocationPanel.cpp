#include <QItemDelegate>
#include <QLineEdit>

#include "NewLocationDialog.h"
#include "LocationPanelControl.h"
#include "ui_LocationPanel.h"
#include "LocationPanel.h"

namespace HTXpress::AppComponents::Instrument::App {
    class IntegerDelegate : public QItemDelegate {
    public:
        QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/,
                              const QModelIndex& /*index*/) const override {
            auto* lineEdit = new QLineEdit(parent);
            const auto* validator = new QIntValidator(lineEdit);
            lineEdit->setValidator(validator);
            return lineEdit;
        }
    };

    struct LocationPanel::Impl {
        LocationPanelControl control;
        Ui::LocationPanel ui;

        auto CreateRows(int32_t rowCount)->void;
        auto AddRow()->void;
        auto AddRow(const LocationInput& location)->void;
    };

    auto LocationPanel::Impl::CreateRows(int32_t rowCount) -> void {
        ui.table->blockSignals(true);

        ui.table->clearContents();
        ui.table->setRowCount(rowCount);

        const auto colCount = ui.table->columnCount();
        for(int32_t rowIdx=0; rowIdx<rowCount; rowIdx++) {
            for(int32_t colIdx=0; colIdx<colCount; colIdx++) {
                auto* item = new QTableWidgetItem("0");
                item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
                ui.table->setItem(rowIdx, colIdx, item);
            }
        }

        ui.table->blockSignals(false);
    }

    auto LocationPanel::Impl::AddRow() -> void {
        const auto rowIdx = ui.table->rowCount();

        ui.table->blockSignals(true);

        ui.table->setRowCount(rowIdx + 1);

        const auto colCount = ui.table->columnCount();
        for(int32_t colIdx=0; colIdx<colCount; colIdx++) {
            auto* item = new QTableWidgetItem("0");
            item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            ui.table->setItem(rowIdx, colIdx, item);
        }

        ui.table->blockSignals(false);
    }

    auto LocationPanel::Impl::AddRow(const LocationInput& location) -> void {
        AddRow();

        const auto rowIdx = ui.table->rowCount() - 1;
        const auto colCount = ui.table->columnCount();

        for(auto colIdx=0; colIdx<colCount; colIdx++) {
            ui.table->item(rowIdx, colIdx)->setText(QString::number(location.values[colIdx]));
        }
    }

    LocationPanel::LocationPanel(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui.setupUi(this);

        d->ui.table->setColumnCount(7);

        d->ui.table->setHorizontalHeaderLabels({"X", "Y", "Z", "CX", "CY", "DX", "DY"});
        d->ui.table->setColumnWidth(0, 50);
        d->ui.table->setColumnWidth(1, 50);
        d->ui.table->setColumnWidth(2, 50);
        d->ui.table->setColumnWidth(3, 50);
        d->ui.table->setColumnWidth(4, 50);
        d->ui.table->setColumnWidth(5, 50);
        d->ui.table->setColumnWidth(6, 50);
        d->ui.table->horizontalHeader()->setStretchLastSection(true);
        d->ui.table->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        d->ui.table->setSelectionBehavior(QAbstractItemView::SelectItems);
        d->ui.table->setSelectionMode(QAbstractItemView::SingleSelection);
        d->ui.table->setItemDelegate(new IntegerDelegate());

        connect(d->ui.addBtn, SIGNAL(clicked()), this, SLOT(onAdd()));
        connect(d->ui.delBtn, SIGNAL(clicked()), this, SLOT(onDelete()));
    }

    LocationPanel::~LocationPanel() {
    }

    auto LocationPanel::Set(const QList<LocationInput>& list) -> void {
        const auto count = list.length();
        d->CreateRows(count);

        for(auto idx=0; idx<count; idx++) {
            const auto& item = list.at(idx);
            d->ui.table->item(idx, 0)->setText(QString::number(item.values[LocationInput::posX]));
            d->ui.table->item(idx, 1)->setText(QString::number(item.values[LocationInput::posY]));
            d->ui.table->item(idx, 2)->setText(QString::number(item.values[LocationInput::posZ]));
            d->ui.table->item(idx, 3)->setText(QString::number(item.values[LocationInput::countX]));
            d->ui.table->item(idx, 4)->setText(QString::number(item.values[LocationInput::countY]));
            d->ui.table->item(idx, 5)->setText(QString::number(item.values[LocationInput::gapX]));
            d->ui.table->item(idx, 6)->setText(QString::number(item.values[LocationInput::gapY]));
        }
    }

    auto LocationPanel::Get() const -> QList<LocationInput> {
        QList<LocationInput> inputs;

        const auto rowCount = d->ui.table->rowCount();
        const auto colCount = d->ui.table->columnCount();

        for(auto rowIdx=0; rowIdx<rowCount; rowIdx++) {
            LocationInput input;
            for(auto colIdx=0; colIdx<colCount; colIdx++) {
                input.values[colIdx] = d->ui.table->item(rowIdx, colIdx)->text().toInt();                
            }
            inputs.append(input);
        }

        return inputs;
    }

    auto LocationPanel::Add(const LocationInput& location) -> void {
        d->AddRow(location);
    }

    auto LocationPanel::Remove(int32_t index) -> void {
        d->ui.table->removeRow(index);
    }

    auto LocationPanel::ClearAll() -> void {
        d->ui.table->blockSignals(true);
        d->ui.table->setRowCount(0);
        d->ui.table->blockSignals(false);
    }

    void LocationPanel::onAdd() {
        NewLocationDialog dlg;
        if(dlg.exec() == QDialog::Rejected) return;

        auto location = dlg.GetLocation();
        emit sigAddLocation(location);
    }

    void LocationPanel::onDelete() {
        auto ranges = d->ui.table->selectedRanges();
        if(ranges.length() == 0 ) return;

        auto rowIdx = ranges.at(0).topRow();
        emit sigDeleteLocation(rowIdx);
    }
}
