#pragma once
#include <memory>

#include <ImagingScenario.h>

namespace HTXpress::AppComponents::Instrument::App {
    class MacroRepo {
    public:
        typedef std::shared_ptr<MacroRepo> Pointer;

    private:
        MacroRepo();

    public:
        ~MacroRepo();

        static auto GetInstance()->Pointer;

        auto GetImagingScenario() const->AppEntity::ImagingScenario::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}