#pragma once
#include <memory>
#include <QWidget>
#include <QList>

#include <ImagingSequence.h>

#include "Defines.h"

namespace HTXpress::AppComponents::Instrument::App {
    class ImagingSequencePanel : public QWidget {
		Q_OBJECT

    public:
        ImagingSequencePanel(QWidget* parent = nullptr);
        ~ImagingSequencePanel();

        auto Set(const QList<SequenceInput>& sequences)->void;
        auto Get() const->QList<SequenceInput>;

        auto Add(const SequenceInput& sequence)->void;
        auto Remove(int32_t index)->void;

        auto ClearAll()->void;

    protected slots:
        void onAdd();
        void onDelete();
        void onSequenceSelected(int row, int column);
        void onSequenceChanged(int row, int column);

    signals:
        void sigSelected(int index);
        void sigAddModality(AppEntity::Modality modality, bool is3D);
        void sigDeleteModality(int32_t index);
        void sigChangeModality(int32_t index, AppEntity::Modality modality, bool is3D);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}