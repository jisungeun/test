#pragma once
#include <memory>
#include <QString>

namespace HTXpress::AppComponents::Instrument::App {
    class ImagingScenarioPanelControl {
    public:
        ImagingScenarioPanelControl();
        ~ImagingScenarioPanelControl();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}