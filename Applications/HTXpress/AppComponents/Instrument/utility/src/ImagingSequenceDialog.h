#pragma once
#include <memory>
#include <QDialog>

#include <ImagingSequence.h>

#include "Defines.h"

namespace HTXpress::AppComponents::Instrument::App {
    class ImagingSequenceDialog: public QDialog {
		Q_OBJECT
    public:
        ImagingSequenceDialog(QWidget* parent = nullptr);
        ~ImagingSequenceDialog();

        auto Get() const->QList<SequenceInput>;

    protected slots:
        void onNewModality(AppEntity::Modality modality, bool is3D);
        void onDeleteModality(int32_t index);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
