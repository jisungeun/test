#pragma once
#include <memory>
#include <QString>

#include "MotionCommand.h"
#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument {
	class HTXInstrument_API MotionCommandWriter {
	public:
		MotionCommandWriter();
		~MotionCommandWriter();

		auto Write(const QString& path)->bool;
		auto Write(const QString& path, const QList<MotionCommand::Command::Pointer>& commands)->bool;
	};
}