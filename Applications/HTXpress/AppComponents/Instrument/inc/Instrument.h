#pragma once
#include <memory>
#include <QThread>
#include <QString>

#include <ImagingSequence.h>
#include <PositionGroup.h>
#include <FLFilter.h>

#include "InstrumentDefines.h"
#include "InstrumentConfig.h"
#include "ImagingParameter.h"
#include "ImagePort.h"
#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument {
	class HTXInstrument_API Instrument {
	public:
		typedef std::shared_ptr<Instrument> Pointer;

	public:
		Instrument();
		virtual ~Instrument();

		virtual auto SetConfig(const Config& config)->void = 0;
		virtual auto Initialize()->bool = 0;
		virtual auto GetInitializationProgress() const->std::tuple<bool,double> = 0;
		virtual auto CheckInitialized() const->bool = 0;
		virtual auto CleanUp()->void = 0;

		virtual auto InstallImagePort(ImageSource source, ImagePort* port)->void = 0;
		virtual auto UninstallImagePort(ImageSource source, ImagePort* port)->void = 0;

		virtual auto SetFLEmission(const QMap<AppEntity::FLFilter, int32_t>& filters)->void = 0;

		virtual auto Move(Axis axis, RawPosition targetMM)->bool = 0;
		virtual auto MoveXY(RawPosition targetXMM, RawPosition targetYMM)->bool = 0;
		virtual auto MoveZtoSafePos()->bool = 0;
		virtual auto IsMoving() const->MotionStatus = 0;

		virtual auto GetPosition(Axis axis, RawPosition& pos)->bool = 0;
		virtual auto GetPositionXY(RawPosition& xPos, RawPosition& yPos)->bool = 0;
		virtual auto GetPositionXYZ(RawPosition& xPos, RawPosition& yPos, RawPosition& zPos)->bool = 0;

		virtual auto StartJog(Axis axis, bool plusDirection)->bool = 0;
		virtual auto StopJog()->bool = 0;

		virtual auto SetJogRange(Axis axis, const PositionRange& range)->void = 0;
		virtual auto EnableJoystick()->bool = 0;
		virtual auto DisableJoystick()->bool = 0;
		virtual auto CheckJoystick()->JoystickStatus = 0;

		virtual auto ChangeFOV(int32_t xPixels, int32_t yPixels, int32_t& offsetX, int32_t& offsetY)->bool = 0;
		virtual auto StartAcquisition(const AppEntity::ImagingSequence::Pointer sequence,
									  const QList<AppEntity::PositionGroup>& positions,
									  bool useAutofocus,
									  AppEntity::WellIndex startingWellIndex,
									  double focusReadyMM,
									  bool useMultiDishHolder) -> bool = 0;
		virtual auto StopAcquisition()->bool = 0;
		virtual auto CheckAcquisitionProgress() const->std::tuple<bool,double,AppEntity::Position> = 0;
		virtual auto GetRemainCount(ImageSource source) const->int32_t = 0;

		virtual auto StartLive(LiveMode mode, const ImagingParameter& param, bool onlyUpdateParameter = false)->bool = 0;
		virtual auto StopLive()->bool = 0;
		virtual auto ResumeLive()->bool = 0;
		virtual auto StartLiveWithTrigger(const ImagingParameter& param,
										  int32_t triggerCount, int32_t triggerInterval, int32_t pulseWidth,
									      bool useHCG = false, double gain = 0)->bool = 0;

		virtual auto StartPreviewAcquisition(const QList<UnitMotion>& motions, double bfExposureMSec)->bool = 0;


		virtual auto EnableAutoFocus(bool enable)->std::tuple<bool,bool> = 0;
		virtual auto AutoFocusEnabled() const->bool = 0;
		virtual auto PerformAutoFocus()->std::tuple<bool,bool> = 0;
		virtual auto ReadAFSensorValue()->int32_t = 0;
		virtual auto SetBestFocusCurrent(int32_t& value)->bool = 0;
		virtual auto SetBestFocus(int32_t value)->bool = 0;
        virtual auto ScanFocus(double startMm, double distMm, int32_t count) -> QList<int32_t> = 0;

		virtual auto SetAcquisitionPattenIndex(const AppEntity::ImagingType imagingType,
											   const AppEntity::TriggerType trigger,
											   const QList<int32_t>& index)->void = 0;
		virtual auto SetAcquisitionChannelIndex(const AppEntity::ImagingType imagingType,
											    const AppEntity::TriggerType trigger,
												const int32_t ledChannel,
											    const QList<int32_t>& index)->void = 0;

		virtual auto StartCondenserAutoFocus(int32_t patternIndex, int32_t intensity)->bool = 0;
		virtual auto CheckCondenserAutoFocusProgress() const->std::tuple<bool, double> = 0;
		virtual auto FinishCondenserAutoFocus()->void = 0;

		virtual auto StartHTIlluminationCalibration(const QList<int32_t>& intensityList,
													const AppEntity::ImagingConditionHT::Pointer imgCond)->bool = 0;
		virtual auto CheckHTIlluminationCalibrationProgress() const->std::tuple<bool, double> = 0;
		virtual auto FinishHTIlluminationCalibration()->void = 0;

		virtual auto IsSupported(InstrumentFeature feature) const->bool = 0;

		virtual auto GetError() const->QString = 0;
	};
}