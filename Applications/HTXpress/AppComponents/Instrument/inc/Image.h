#pragma once
#include <memory>
#include <QDateTime>

#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument {
	class HTXInstrument_API Image {
	public:
		typedef std::shared_ptr<Image> Pointer;

		Image();
		Image(const Image& other) = delete;
		Image(int32_t sizeX, int32_t sizeY, const std::shared_ptr<uint8_t[]> buffer);
		~Image();

		void operator=(const Image& other) = delete;

		auto SetImage(int32_t sizeX, int32_t sizeY, const std::shared_ptr<uint8_t[]> buffer)->void;
		auto GetSizeX() const->int32_t;
		auto GetSizeY() const->int32_t;
		auto GetBuffer() const->std::shared_ptr<uint8_t[]>;

		auto SetTimestamp(const QDateTime& timestamp)->void;
		auto GetTimestamp() const->QDateTime;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}