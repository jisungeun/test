#pragma once
#include <memory>
#include <QString>
#include <QList>
#include <QSettings>

#include "InstrumentDefines.h"

#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument::MotionCommand {
    class HTXInstrument_API Command {
    public:
        typedef std::shared_ptr<Command> Pointer;

    public:
        Command();
        Command(MotionCommandName name);
        Command(const Command& other);
        virtual ~Command();

        auto operator==(const Command& other)->bool;

        auto GetName() const->MotionCommandName;
        auto GetNameAsString() const->QString;
        auto HasSameName(const Command& other) const->bool;

        auto SetCommandID(int32_t id)->void;
        auto GetCommandID() const->int32_t;

        virtual auto GetCommandType() const->int32_t = 0;
        virtual auto GetCommandTypeAsString() const->QString = 0;
        virtual auto SetParameter(const QString& field, int32_t value)->bool = 0;
        virtual auto GetNames() const->QVector<QString> = 0;
        virtual auto GetParameters() const->QList<int32_t> = 0;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class HTXInstrument_API EnablePatternSequence : public Command {
    public:
        EnablePatternSequence();
        EnablePatternSequence(MotionCommandName type);
        EnablePatternSequence(const EnablePatternSequence& other);
        ~EnablePatternSequence() override;

        auto operator==(const EnablePatternSequence& other)->bool;

        auto SetEnable(bool enable)->void;
        auto GetEnable() const->bool;

        auto SetSequenceID(int32_t seqID)->void;
        auto GetSequenceID() const->int32_t;

        auto SetIntensity(int32_t red, int32_t green, int32_t blue)->void;
        auto GetIntensityRed() const->int32_t;
        auto GetIntensityGreen() const->int32_t;
        auto GetIntensityBlue() const->int32_t;

        auto SetExposure(int32_t exposure)->void;
        auto GetExposure() const->int32_t;

        auto SetInterval(int32_t interval)->void;
        auto GetInterval() const->int32_t;

        auto SetCameraType(int32_t type)->void;
        auto CameraType() const->int32_t;

        auto GetCommandType() const->int32_t override;
        auto GetCommandTypeAsString() const->QString override;
        auto SetParameter(const QString& field, int32_t value)->bool override;
        auto GetNames() const->QVector<QString> override;
        auto GetParameters() const->QList<int32_t> override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class HTXInstrument_API TriggerWithMotion : public Command {
    public:
        TriggerWithMotion();
        TriggerWithMotion(MotionCommandName type);
        TriggerWithMotion(TriggerWithMotion& other);
        ~TriggerWithMotion() override;

        auto operator==(const TriggerWithMotion& other)->bool;

        auto SetAxis(int32_t axis)->void;
        auto GetAxis() const->int32_t;

        auto SetMotionRange(int32_t startPos, int32_t endPos)->void;
        auto GetMotionRangeStart() const->int32_t;
        auto GetMotionRangeEnd() const->int32_t;

        auto SetTriggerRange(int32_t startPos, int32_t endPos)->void;
        auto GetTriggerRangeStart() const->int32_t;
        auto GetTriggerRangeEnd() const->int32_t;

        auto SetTriggerInterval(int32_t interval)->void;
        auto GetTriggerInterval() const->int32_t;

        auto SetTriggerWidth(int32_t width)->void;
        auto GetTriggerWidth() const->int32_t;

        auto SetMotion(int32_t speed, int32_t accel)->void;
        auto GetMotionSpeed() const->int32_t;
        auto GetMotionAcceleration() const->int32_t;

        auto GetCommandType() const->int32_t override;
        auto GetCommandTypeAsString() const->QString override;
        auto SetParameter(const QString& field, int32_t value)->bool override;
        auto GetNames() const->QVector<QString> override;
        auto GetParameters() const -> QList<int32_t> override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class HTXInstrument_API TriggerOnly : public Command {
    public:
        TriggerOnly();
        TriggerOnly(MotionCommandName type);
        TriggerOnly(const TriggerOnly& other);
        ~TriggerOnly() override;

        auto operator==(const TriggerOnly& other)->bool;

        auto SetTriggerCount(int32_t count)->void;
        auto GetTriggerCount() const->int32_t;

        auto SetTriggerInterval(int32_t interval)->void;
        auto GetTriggerInterval() const->int32_t;

        auto SetTriggerWidth(int32_t width)->void;
        auto GetTriggerWidth() const->int32_t;

        auto GetCommandType() const->int32_t override;
        auto GetCommandTypeAsString() const->QString override;
        auto SetParameter(const QString& field, int32_t value)->bool override;
        auto GetNames() const->QVector<QString> override;
        auto GetParameters() const -> QList<int32_t> override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class HTXInstrument_API FilterWheel : public Command {
    public:
        FilterWheel();
        FilterWheel(MotionCommandName type);
        FilterWheel(const FilterWheel& other);
        ~FilterWheel() override;

        auto operator==(const FilterWheel& other)->bool;

        auto SetFilter(int32_t position)->void;
        auto GetFilter() const->int32_t;

        auto GetCommandType() const->int32_t override;
        auto GetCommandTypeAsString() const->QString override;
        auto SetParameter(const QString& field, int32_t value)->bool override;
        auto GetNames() const->QVector<QString> override;
        auto GetParameters() const -> QList<int32_t> override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class HTXInstrument_API RunMacroMatrix : public Command {
    public:
        RunMacroMatrix();
        RunMacroMatrix(MotionCommandName type);
        RunMacroMatrix(const RunMacroMatrix& other);
        ~RunMacroMatrix() override;

        auto operator==(const RunMacroMatrix& other)->bool;

        auto SetCount(int32_t countX, int32_t countY)->void;
        auto GetCountX() const->int32_t;
        auto GetCountY() const->int32_t;

        auto SetDistance(int32_t distX, int32_t distY)->void;
        auto GetDistanceX() const->int32_t;
        auto GetDistanceY() const->int32_t;

        auto SetMacroGroupID(int32_t id)->void;
        auto GetMacroGroupID() const->int32_t;

        auto SetScanDirection(int32_t direction)->void;
        auto GetScanDirection() const->int32_t;

        auto GetCommandType() const->int32_t override;
        auto GetCommandTypeAsString() const->QString override;
        auto SetParameter(const QString& field, int32_t value) -> bool override;
        auto GetNames() const -> QVector<QString> override;
        auto GetParameters() const -> QList<int32_t> override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}