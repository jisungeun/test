#pragma once
#include <memory>

#include <Instrument.h>

#include "InstrumentDefines.h"
#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument {
	class HTXInstrument_API InstrumentFactory {
	public:
		static auto GetInstance(InstrumentType type = InstrumentType::HTX)->Instrument::Pointer;
	};
}