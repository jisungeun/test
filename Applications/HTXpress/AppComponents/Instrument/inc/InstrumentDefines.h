#pragma once
#include <memory>
#include <enum.h>
#include <QString>

namespace HTXpress::AppComponents::Instrument {
	using RawPosition = double;

    BETTER_ENUM(InstrumentType, int32_t,
                Simulator,
                HTX
    );

    BETTER_ENUM(Axis, int32_t,
                AxisX,
                AxisY,
                AxisZ,
                AxisC,      //Condenser Axis
                AxisF,      //Filter Axis
                AxisD       //Door Axis
    );

    BETTER_ENUM(LiveMode, int32_t,
                Holography,
                BrightField,
                Fluorescence
    );

    BETTER_ENUM(Camera, int32_t,
                Internal = 1,
                External = 2,
                Both
    );

    BETTER_ENUM(ImageSource, int32_t,
                ImagingCamera,
                CondenserCamera
    );

    BETTER_ENUM(MotionCommandName, int32_t,
                StopSequence,
                MonoBFLiveTrigger,
                FLLiveCH0StartSequence,
                FLLiveCH1StartSequence,
                FLLiveCH2StartSequence,
                ChangeFilter1,
                ChangeFilter2,
                ChangeFilter3,
                ChangeFilter4,
                ChangeFilter5,
                ChangeFilter6,
                FLCH0StartSequence,
                FLCH1StartSequence,
                FLCH2StartSequence,
                FL3DTrigger,
                FL2DTrigger,
                HTStartSequence1,
                HTStartSequence2,
                HTStartSequence3,
                HTStartSequence4,
                HT3DTrigger,
                HT2DTrigger,
                MonoBFStartSequence,
                MonoBFTrigger,
                ColorBFStartSequence,
                ColorBFTrigger,
                RunMacroGroupSingle,
                RunMacroGroup1,
                RunMacroGroup2,
                RunMacroGroup3,
                RunMacroGroup4,
                RunMacroGroup5,
                RunMacroGroup6,
                RunMacroGroup7,
                RunMacroGroup8,
                RunMacroGroup9,
                RunMacroGroup10,
                RunMacroGroup11,
                RunMacroGroup12,
                RunMacroGroup13,
                RunMacroGroup14,
                RunMacroGroup15,
                RunMacroGroup16,
                RunMacroGroup17,
                RunMacroGroup18,
                RunMacroGroup19,
                Invalid
    );

    BETTER_ENUM(InstrumentFeature, int32_t,
                SupportSampleStageEncoder
    );

    enum class JoystickTriggerStatus {
            Hold,
            Stop,
            PlusDirection,
            MinusDirection
    };

    enum class JoystickAxis {
        AxisX,
        AxisY,
        AxisZ
    };

    struct JoystickStatus {
        JoystickTriggerStatus status{ JoystickTriggerStatus::Hold };
        JoystickAxis axis{ JoystickAxis::AxisX };
    };

    struct MotionStatus {
        bool error{ false };
        bool moving{ false };
        bool afFailed{ false };
        QString message;
    };

    struct PositionRange {
        RawPosition minPos;
        RawPosition maxPos;
    };

    struct UnitMotion {
        int32_t motionStartXPulse;
        int32_t motionStartYPulse;
        int32_t motionStartZPulse;

        bool forwardDirection;        
        int32_t triggerIntervalPulse;
        int32_t triggerCount;
    };
}