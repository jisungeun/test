#pragma once
#include <memory>
#include <QString>

#include "MotionCommand.h"
#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument {
	class HTXInstrument_API MotionCommandLoader {
	public:
		MotionCommandLoader();
		~MotionCommandLoader();

		auto Load(const QString& path)->bool;
		auto Load(const QString& path, QList<MotionCommand::Command::Pointer>& commands)->bool;
	};
}