#pragma once
#include <memory>
#include <QWidget>
#include <QList>

#include "InstrumentDefines.h"
#include "MotionCommand.h"
#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument {
	class HTXInstrument_API MotionCommandPanel : public QWidget {
		Q_OBJECT

	public:
		MotionCommandPanel(QWidget* parent=nullptr);
		~MotionCommandPanel() override;

        auto SetCommand(MotionCommandName name, MotionCommand::Command::Pointer cmd)->void;
		auto GetCommand(MotionCommandName name) const->MotionCommand::Command::Pointer;
		auto GetCommands() const->QList<MotionCommand::Command::Pointer>;

		auto IsModified() const->bool;
		auto GetModifiedList() const->QList<MotionCommandName>;

		auto Clear()->void;

	protected slots:
		void onCommandSeleteced(int row, int col);
		void onCommandIndexChanged(int row, int col);
		void onEnableEditCommand();
		void onRestoreCommand();
		void onApplyCommandChanges();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}