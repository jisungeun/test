#pragma once
#include <memory>
#include <QList>

#include "InstrumentDefines.h"
#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument {
	class HTXInstrument_API ImagingParameter {
	public:
		ImagingParameter();
		ImagingParameter(const ImagingParameter& other);
		virtual ~ImagingParameter();

		auto operator=(const ImagingParameter& rhs)->ImagingParameter&;

		auto SetSequenceId(int32_t id)->void;
		auto SetIntensity(int32_t red, int32_t green, int32_t blue)->void;
		auto SetExposureUSec(int32_t exposure)->void;
		auto SetIntervalUSec(int32_t interval)->void;
		auto SetCameraType(Camera  type)->void;
		auto SetLEDChannel(int32_t channel)->void;
		auto SetEmissionFilter(int32_t channel)->void;

		auto SequenceId() const->int32_t;
		auto IntensityRed() const->int32_t;
		auto IntensityGreen() const->int32_t;
		auto IntensityBlue() const->int32_t;
		auto ExposureUSec() const->int32_t;
		auto IntervalUSec() const->int32_t;
		auto CameraType() const->Camera;
		auto LEDChannel() const->int32_t;
		auto EmissionFilter() const->int32_t;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}