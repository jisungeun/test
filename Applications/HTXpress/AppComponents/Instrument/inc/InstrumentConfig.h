#pragma once
#include <memory>
#include <QList>
#include <QVariant>

#include "InstrumentDefines.h"
#include "FLFilter.h"
#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument {
    BETTER_ENUM(PreviewParam, int32_t,
                OverlapMM,
                SingleMoveMM,
                StartEndMarginPulse,
                ForwardDelayCompensationPulse,
                BackwardDelayCompensationPulse,
                LedIntensity,
                ExposureUSec,
                ReadoutUSec,
                TriggerPulseWidth,
                AccelerationFactor,
                LedChannel,
                FilterChannel,
                GainCoefficientA,
                GainCoefficientB,
                GainCoefficientP,
                GainCoefficientQ
    );

	class HTXInstrument_API Config {
	public:
		typedef std::shared_ptr<Config> Pointer;

	public:
		Config();
        Config(const Config& other);
		~Config();

        auto operator=(const Config& rhs)->Config&;

        //mcu communication
        auto SetMCUComm(int32_t port, int32_t baudrate)->void;
        auto GetMCUCommPort() const->int32_t;
        auto GetMCUCommBaudrate() const->int32_t;

        //camera models
        auto SetImagingCamera(const QString& serial)->void;
        auto SetCondenserCamera(const QString& serial)->void;
        auto ImagingCamera() const->QString;
        auto CondenserCamera() const->QString;

		//motion specification
        auto SetAxisResolutionPPM(const Axis axis, const uint32_t value)->void;
        auto AxisResolutionPPM(Axis axis) const->uint32_t;

        auto SetAxisLimitMM(const Axis axis, const double lower, const double upper)->void;
        auto AxisLowerLimitMM(Axis axis) const->double;
        auto AxisUpperLimitMM(Axis axis) const->double;

        auto AxisLowerLimitPulse(Axis axis) const->int32_t;
        auto AxisUpperLimitPulse(Axis axis) const->int32_t;

        auto SetAxisSafeRangeMM(const Axis axis, const double lower, const double upper)->void;
        auto AxisSafeRangeLowerMM(Axis axis) const->double;
        auto AxisSafeRangeUpperMM(Axis axis) const->double;

        auto AxisSafeRangeLowerPulse(Axis axis) const->int32_t;
        auto AxisSafeRangeUpperPulse(Axis axis) const->int32_t;

        auto SetAxisMotionTimeSPM(const Axis axis, const double value)->void;
        auto AxisMotionTimeSPM(Axis axis) const->double;

        auto SetAxisCompensation(Axis axis, double ratio)->void;
        auto AxisCompensation(Axis axis) const->double;

        auto SetAxisInPositionInPulse(Axis axis, int32_t pulse)->void;
        auto GetAxisInPositionInPulse(Axis axis) const->int32_t;

        //predefined positions
        auto SetSafeZPositionMM(const double value)->void;
        auto SafeZPositionMM() const->double;
        auto SafeZPositionPulse() const->int32_t;

        auto SetLoadingPositionMM(const double xValue, const double yValue)->void;
        auto LoadingXPositionMM() const->double;
        auto LoadingYPositionMM() const->double;
        auto LoadingXPositionPulse() const->int32_t;
        auto LoadingYPositionPulse() const->int32_t;

        //Emission configuration
        auto SetEmissionFilter(const int32_t channel, const AppEntity::FLFilter& filter)->void;
        auto GetEmissionFilter(const int32_t channel) const->AppEntity::FLFilter;
        auto GetEmissionFilterChannel(const AppEntity::FLFilter filter) const->int32_t;
        auto GetEmissionFilterChannels() const->QList<int32_t>;
        auto SetEmissionFilterPosition(const int32_t channel, int32_t pulse)->void;
        auto GetEmissionFilterPosition(const int32_t channel) const->int32_t;
        auto GetEmissionFilterPositions() const->QList<int32_t>;

        //Auto focus configuration
        auto SetAFMaximumTrials(int32_t maxTrials)->void;
        auto GetAFMaxTrials() const->int32_t;

        //HT scan parameters
        auto SetHTTriggerScanOffsetPulse(int32_t startOffset, int32_t endOffset)->void;
        auto GetHTTriggerStartOffsetPulse() const->int32_t;
        auto GetHTTriggerEndOffsetPusle() const->int32_t;
        auto SetHTTriggerPulseWidthUSec(int32_t pulseWidth)->void;
        auto GetHTTriggerPulseWidthUSec() const->int32_t;
        auto SetHTTriggerReadOutUSec(int32_t readout)->void;
        auto GetHTTriggerReadOutUSec() const->int32_t;
        auto SetHTTriggerExposureUSec(int32_t exposure)->void;
        auto GetHTTriggerExposureUSec() const->int32_t;
        auto SetHTTriggerIntervalMarginUSec(int32_t intervalMargin)->void;
        auto GetHTTirggerIntervalMarginUSec() const->int32_t;
        auto SetHTTriggerAccelerationFactor(int32_t factor)->void;
        auto GetHTTriggerAccelerationFactor() const->int32_t;

        //Condenser AF parameters
        auto SetCondenserAFImagingChannels(int32_t lightChannel)->void;
        auto GetCondenserAFLightChannel() const->int32_t;
        auto SetCondenserAFScanCondition(int32_t startPulse, int32_t intervalPusle, int32_t slices)->void;
        auto GetCondenserAFStartPosPulse() const->int32_t;
        auto GetCondenserAFTriggerIntervalPulse() const->int32_t;
        auto GetCondenserAFSlices() const->int32_t;
        auto SetCondenserAFPatternCondition(int32_t readoutUSec, int32_t exposureUSec,
                                            int32_t intervalMarginUSec, int32_t pulseWidthUSec)->void;
        auto GetCondenserAFReadouseUSec() const->int32_t;
        auto GetCondenserAFExposureUSec() const->int32_t;
        auto GetCondenserAFIntervalMarginUSec() const->int32_t;
        auto GetCondenserAFPulseWidthUSec() const->int32_t;

        //Multi-Dish Holder
        auto SetMultiDishHolderThicknessMM(double thickness)->void;
        auto GetMultiDishHolderThicknessMM() const->double;

        //FL Output Range
        auto SetFlOutputRange(int32_t minVal, int32_t maxVal)->void;
        auto GetFlOutputRangeMin() const->int32_t;
        auto GetFlOutputRangeMax() const->int32_t;

        //Preview
        auto SetPreviewParameter(PreviewParam param, const QVariant& value)->void;
        auto GetPreviewParameter(PreviewParam param) const->QVariant;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}