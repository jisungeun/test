#pragma once
#include <memory>

#include <enum.h>
#include "MotionCommand.h"
#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument {
    class HTXInstrument_API MotionCommandFactory {
    public:
        static auto Build(MotionCommandName type)->MotionCommand::Command::Pointer;
    };
}