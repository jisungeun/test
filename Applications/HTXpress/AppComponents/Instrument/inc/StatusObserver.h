#pragma once
#include <memory>

#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument {
	class HTXInstrument_API StatusObserver {
	public:
		StatusObserver();
		virtual ~StatusObserver();

		virtual auto UpdateCurrentPosition(int32_t posX, int32_t posY, int32_t posZ, int32_t posC)->void = 0;
	};
}