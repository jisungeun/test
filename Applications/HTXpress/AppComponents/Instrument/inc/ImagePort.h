#pragma once
#include <memory>

#include "Image.h"
#include "HTXInstrumentExport.h"

namespace HTXpress::AppComponents::Instrument {
	class HTXInstrument_API ImagePort {
	public:
		ImagePort();
		virtual ~ImagePort();

		virtual auto Send(Image::Pointer image)->void = 0;
		virtual auto Clear()->void = 0;
	};
}