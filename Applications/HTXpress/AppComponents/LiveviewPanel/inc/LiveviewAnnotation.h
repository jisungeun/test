#pragma once
#include <memory>
#include <QGraphicsItem>

#include "HTXLiveviewPanelExport.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    class HTXLiveviewPanel_API Annotation {
    public:
        Annotation();
        virtual ~Annotation();
        
        virtual auto GetGraphicsItem() const->QGraphicsItem* = 0;
    };
}