#pragma once
#include <memory>

#include <QWidget>
#include <QImage>
#include <QColor>

#include "LiveviewAnnotation.h"
#include "HTXLiveviewPanelExport.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    class HTXLiveviewPanel_API LiveviewPanel : public QWidget {
        Q_OBJECT
    public:
        using Self = LiveviewPanel;
        using Pointer = std::shared_ptr<Self>;

        enum class ZoomType {ZoomIn, ZoomOut, ZoomFit};

        explicit LiveviewPanel(QWidget* parent = nullptr);
        ~LiveviewPanel() override;

        auto SetResolution(double UmPerPixel) -> void;
        auto SetImage(const QImage& image) -> void;
        auto SetROI(double xInPixel, double yInPixel) -> void;

        auto ShowScalebar(bool show) -> void;
        auto ShowCenterMark(bool show) -> void;
        
        auto AddAnnotation(Annotation* annotation, int32_t xInPixel, int32_t yInPixel)->void;
        auto RemoveAnnotation(Annotation* annotation)->void;

    signals:
        //Live view의 중심을 더블 클릭한 위치로 이동하기 위한 시그널
        //@param posX/posY 이미지 중심으로 부터 더블 클릭한 곳까지의 위치 차이 (단위: 화소)
        void sigDoubleClicked(int32_t posX, int32_t posY);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
