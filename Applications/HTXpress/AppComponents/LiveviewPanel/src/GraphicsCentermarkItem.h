#pragma once
#include <memory>

#include <QGraphicsRectItem>

namespace HTXpress::AppComponents::LiveviewPanel {
    class GraphicsCentermarkItem final : public QGraphicsRectItem {
    public:
        explicit GraphicsCentermarkItem(QGraphicsItem* parent = nullptr);

    protected:
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
    };
}
