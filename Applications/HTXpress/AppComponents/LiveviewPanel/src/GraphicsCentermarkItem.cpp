#include <QPen>
#include <QPainter>

#include <QPen>

#include "GraphicsCentermarkItem.h"
#include "LiveviewDefines.h"
#include "GraphicsItemHelper.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    GraphicsCentermarkItem::GraphicsCentermarkItem(QGraphicsItem* parent) : QGraphicsRectItem(parent) {
        QPen p = pen();
        p.setColor(CenterPosColor);
        setPen(p);

        setFlag(ItemIsMovable, false);
        setFlag(ItemIsSelectable, false);
        setFlag(ItemIsFocusable, false);
        setZValue(100);
    }

    auto GraphicsCentermarkItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(option)
        Q_UNUSED(widget)

        painter->setRenderHint(QPainter::Antialiasing);
        QPen p = pen();
        double width = 1.0;

        if (painter->transform().isScaling()) {
            width = GraphicsItemHelper::GetConsistentPenWidth(painter, 2);
        }

        p.setWidthF(width);
        painter->setPen(p);

        const auto center = QPointF(0, 0);
        const auto halfSize = GraphicsItemHelper::GetConsistentLineLength(painter, 25) / 2.0;

        const auto y0 = center.y() - halfSize;
        const auto y1 = center.y() + halfSize;
        const auto x0 = center.x() - halfSize;
        const auto x1 = center.x() + halfSize;

        painter->drawLine(center.x(), y0, center.x(), y1);
        painter->drawLine(x0, center.y(), x1, center.y());
    }
}
