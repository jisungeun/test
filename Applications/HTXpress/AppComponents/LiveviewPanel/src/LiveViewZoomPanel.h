﻿#pragma once

#include <QMetaType>
#include <QWidget>
#include <QPushButton>

namespace HTXpress::AppComponents::LiveviewPanel {
    class LiveviewZoomPanel : public QWidget{
        Q_OBJECT
    public:
        using Self = LiveviewZoomPanel;

        enum class ZoomButtonID {ZoomIn, ZoomOut, ZoomFit};

        explicit LiveviewZoomPanel(QWidget *parent = nullptr);
        ~LiveviewZoomPanel() override;

    protected:
        void enterEvent(QEvent * event) override;
        void leaveEvent(QEvent * event) override;
        
    signals:
        void sigRequestZoom(ZoomButtonID zoom);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

Q_DECLARE_METATYPE(HTXpress::AppComponents::LiveviewPanel::LiveviewZoomPanel::ZoomButtonID)
