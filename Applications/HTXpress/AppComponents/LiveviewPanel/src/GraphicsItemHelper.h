﻿#pragma once
#include <QPainter>

namespace HTXpress::AppComponents::LiveviewPanel {
    class GraphicsItemHelper final {
    public:
        static auto GetConsistentPenWidth(QPainter* painter, const double& desiredWidth) -> double;
        static auto GetConsistentLineLength(QPainter* painter, const double& desiredLength) -> double;
    };
}
