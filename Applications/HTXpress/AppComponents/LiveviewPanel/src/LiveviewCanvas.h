﻿#pragma once

#include <memory>

#include <QGraphicsView>
#include <ViewerOptionMenuWidget.h>

#include "LiveviewAnnotation.h"
#include "LiveViewZoomPanel.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    class LiveviewCanvas : public QGraphicsView {
        Q_OBJECT
    public:
        using Self = LiveviewCanvas;
        using Pointer = std::shared_ptr<Self>;

        explicit LiveviewCanvas(QWidget* parent = nullptr);
        ~LiveviewCanvas() override;

        auto ShowImage(const QImage& image) -> void;
        auto ShowScalebar(bool show) -> void;
        auto ShowCenterMark(bool show) -> void;
        auto ShowROI(bool show) -> void;
        auto SetROI(int32_t x, int32_t y, double w, double h) -> void;

        auto AddAnnotation(Annotation* annotation, int32_t xInPixel, int32_t yInPixel) -> void;
        auto RemoveAnnotation(Annotation* annotation) -> void;

        auto SetUmPerPixel(const double& umPerPixel) -> void;
        auto ZoomFit() -> void;

    protected:
        auto resizeEvent(QResizeEvent* event) -> void override;
        auto mousePressEvent(QMouseEvent* event) -> void override;
        auto mouseReleaseEvent(QMouseEvent* event) -> void override;
        auto mouseMoveEvent(QMouseEvent* event) -> void override;

    signals:
        void sigCanvasMouseDoubleClicked(int32_t posX, int32_t posY);

    private slots:
        void onLodChanged(const double& lod);
        void onRecvZoomRequest(LiveviewZoomPanel::ZoomButtonID buttonID);
        void onRecvSnapshotRequest();
        void onMenuClicked(const ViewerOptionMenu::MenuIndex& menu);
        void onImageItemDoubleClicked(const QPointF& clickedScenePos);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
