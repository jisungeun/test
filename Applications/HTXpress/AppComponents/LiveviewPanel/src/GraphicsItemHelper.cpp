﻿#include "GraphicsItemHelper.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    auto GraphicsItemHelper::GetConsistentPenWidth(QPainter* painter, const double& desiredWidth) -> double {
        const auto m11 = painter->transform().m11();
        const auto m22 = painter->transform().m22();
        const auto scaleFactor = 1.0 / std::max(m11, m22);
        return desiredWidth * scaleFactor;
    }

    auto GraphicsItemHelper::GetConsistentLineLength(QPainter* painter, const double& desiredLength) -> double {
        const auto m11 = painter->transform().m11();
        const auto m22 = painter->transform().m22();
        const auto scaleFactor = 1.0 / std::max(m11, m22);
        return desiredLength * scaleFactor;
    }
}
