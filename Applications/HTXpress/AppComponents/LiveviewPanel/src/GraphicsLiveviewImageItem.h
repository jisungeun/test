﻿#pragma once

#include <memory>

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QPixmap>

#include "GraphicsLiveviewImageLodGetter.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    class GraphicsLiveviewImageItem : public QObject, public QGraphicsPixmapItem {
        Q_OBJECT
    public:
        enum { LiveviewImage = UserType + 99 };

        GraphicsLiveviewImageItem(const QPixmap& pixmap);
        ~GraphicsLiveviewImageItem() override;

        auto GetLodGetterObject() const -> GraphicsLiveviewImageLodGetter*;

    protected:
        auto type() const -> int override;
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
        auto mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) -> void override;

    signals:
        void sigImageItemDoubleClicked(const QPointF&);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
