﻿#pragma once

#include <QColor>

namespace HTXpress::AppComponents::LiveviewPanel {
    const QColor RoiBorderColor("#FFC000");
    const QColor CenterPosColor("#E0EEF9");
}