﻿#include <QToolButton>
#include <QLayout>
#include <QButtonGroup>
#include <QGraphicsOpacityEffect>

#include "LiveviewZoomPanel.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    struct LiveviewZoomPanel::Impl {
        QToolButton *zoomInBtn{nullptr};
        QToolButton *zoomOutBtn{nullptr};
        QToolButton *zoomFitBtn{nullptr};
    };

    LiveviewZoomPanel::LiveviewZoomPanel(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()}{
        d->zoomInBtn = new QToolButton(this);
        d->zoomOutBtn = new QToolButton(this);
        d->zoomFitBtn = new QToolButton(this);

        for(const auto& btn : findChildren<QToolButton*>()) {
            btn->setStyleSheet("QToolButton {border:none; background-color:transparent;}");
            btn->setFixedSize(24,24);
            btn->setIconSize({24,24});
        }

        d->zoomInBtn->setIcon(QIcon(":/img/ic-sub-zoomin.svg"));
        d->zoomOutBtn->setIcon(QIcon(":/img/ic-sub-zoomout.svg"));
        d->zoomFitBtn->setIcon(QIcon(":/img/ic-sub-zoomfit.svg"));

        d->zoomInBtn->setToolTip("Zoom in");
        d->zoomOutBtn->setToolTip("Zoom out");
        d->zoomFitBtn->setToolTip("Fit in view");

        const auto layout = new QVBoxLayout;
        layout->addWidget(d->zoomFitBtn, 0, Qt::AlignCenter);
        layout->addWidget(d->zoomInBtn, 0, Qt::AlignCenter);
        layout->addWidget(d->zoomOutBtn, 0, Qt::AlignCenter);
        layout->setSpacing(6);
        layout->setContentsMargins(0,0,0,0);

        const auto zoomGroup = new QButtonGroup();
        zoomGroup->addButton(d->zoomInBtn, static_cast<int32_t>(ZoomButtonID::ZoomIn));
        zoomGroup->addButton(d->zoomOutBtn, static_cast<int32_t>(ZoomButtonID::ZoomOut));
        zoomGroup->addButton(d->zoomFitBtn, static_cast<int32_t>(ZoomButtonID::ZoomFit));

        connect(zoomGroup, &QButtonGroup::idClicked, [this](int id) {
            emit sigRequestZoom(static_cast<ZoomButtonID>(id));
        });

        setLayout(layout);
        setFixedSize(24, 24*3+6*2);

        auto* oe = new QGraphicsOpacityEffect(this);
	    oe->setOpacity(0.2);
	    setGraphicsEffect(oe);
	    setAutoFillBackground(true);
    }

    LiveviewZoomPanel::~LiveviewZoomPanel() {
    }

    void LiveviewZoomPanel::enterEvent(QEvent* event) {
        auto* oe = new QGraphicsOpacityEffect(this);
	    oe->setOpacity(1.0);
	    setGraphicsEffect(oe);
        QWidget::enterEvent(event);
    }

    void LiveviewZoomPanel::leaveEvent(QEvent* event) {
        auto* oe = new QGraphicsOpacityEffect(this);
	    oe->setOpacity(0.2);
	    setGraphicsEffect(oe);
        QWidget::leaveEvent(event);
    }
}
