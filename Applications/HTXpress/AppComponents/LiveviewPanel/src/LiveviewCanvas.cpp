﻿#include <QMouseEvent>
#include <QScrollBar>
#include <QVBoxLayout>

#include <MessageDialog.h>

#include "LiveviewCanvas.h"

#include "CanvasZoomSupport.h"
#include "LiveviewCanvasControl.h"
#include "LiveviewSnapshotPanel.h"
#include "LiveviewZoomPanel.h"
#include "ScalebarWidget.h"
#include "SubWidgetPosUpdater.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    struct LiveviewCanvas::Impl {
        explicit Impl(Self* self) : self(self) {
        }

        Self* self{};
        LiveviewCanvasControl control{};
        bool firstUpdate{true};
        bool leftMouseButtonPressed{false};
        bool lastLeftMouseButtonPressed{false};

        QGraphicsScene* scene{nullptr};
        CanvasZoomSupport* canvasEventFilter{nullptr};
        ScalebarWidget* scalebar{nullptr};
        QWidget* bottomRightWidget{nullptr}; // including snapshot, zoom
        LiveviewSnapshotPanel* snapshotPanel{nullptr};
        LiveviewZoomPanel* zoomPanel{nullptr};
        ViewerOptionMenu::ViewerOptionMenuWidget* optionWidget{nullptr};

        SubWidgetPosUpdater* posUpdater{nullptr};

        auto InitScalebar() -> void;
        auto InitCanvasProperty() -> void;
        auto InitScene() -> void;
        auto InitEventFilter() -> void;
        auto InitGraphicsItem() -> void;
        auto InitBottomRightWidget() -> void;
        auto InitOptionWidget() -> void;
        auto InitSubWidgetPosUpdater() -> void;

        auto UpdateSceneRect() -> void;
        auto UpdateScalebar() -> void;
        auto UpdateCenterMark() -> void;
    };

    LiveviewCanvas::LiveviewCanvas(QWidget* parent) : QGraphicsView(parent), d{std::make_unique<Impl>(this)} {
        d->InitScalebar();
        d->InitBottomRightWidget();
        d->InitOptionWidget();
        d->InitSubWidgetPosUpdater();
        d->InitEventFilter();
        d->InitScene();
        d->InitGraphicsItem();
        d->InitCanvasProperty();
    }

    LiveviewCanvas::~LiveviewCanvas() = default;

    auto LiveviewCanvas::ShowImage(const QImage& image) -> void {
        d->control.SetImageItem(image);

        const auto imageW = static_cast<double>(image.size().width());
        const auto imageH = static_cast<double>(image.size().height());
        const auto sceneW = imageW * 3;
        const auto sceneH = imageH * 3;

        d->canvasEventFilter->SetZoomFitRect({-imageW / 2, -imageH / 2, imageW, imageH});
        d->control.SetSceneRectSize(sceneW, sceneH);
        d->UpdateSceneRect();
    }

    auto LiveviewCanvas::ShowScalebar(bool show) -> void {
        d->optionWidget->SetScalebarVisible(show);
    }

    auto LiveviewCanvas::ShowCenterMark(bool show) -> void {
        d->optionWidget->SetCenterMarkVisible(show);
    }

    auto LiveviewCanvas::ShowROI(bool show) -> void {
        d->control.ShowRoiItem(show);
    }

    auto LiveviewCanvas::SetROI(int32_t x, int32_t y, double w, double h) -> void {
        d->control.SetRoiItem(x, y, w, h);
    }

    auto LiveviewCanvas::AddAnnotation(Annotation* annotation, int32_t xInPixel, int32_t yInPixel) -> void {
        auto* item = annotation->GetGraphicsItem();
        if (!item) {
            return;
        }

        d->scene->addItem(item);
        item->setPos(xInPixel, yInPixel);
    }

    auto LiveviewCanvas::RemoveAnnotation(Annotation* annotation) -> void {
        auto* item = annotation->GetGraphicsItem();
        if (!item) {
            return;
        }

        d->scene->removeItem(item);
    }

    auto LiveviewCanvas::SetUmPerPixel(const double& umPerPixel) -> void {
        d->scalebar->SetUmPerPixel(umPerPixel);
    }

    auto LiveviewCanvas::ZoomFit() -> void {
        d->canvasEventFilter->ZoomFit();
    }

    void LiveviewCanvas::resizeEvent(QResizeEvent* event) {
        d->posUpdater->onUpdatePosition();

        if (this->horizontalScrollBar() && this->verticalScrollBar()) {
            const auto horPadding = this->horizontalScrollBar()->height();
            const auto verPadding = this->verticalScrollBar()->width();
            d->posUpdater->ChangeAllSubWidgetsPadding(horPadding, verPadding);
        }

        QGraphicsView::resizeEvent(event);
    }

    auto LiveviewCanvas::mousePressEvent(QMouseEvent* event) -> void {
        if (event->button() == Qt::LeftButton) {
            d->leftMouseButtonPressed = true;
        }

        QGraphicsView::mousePressEvent(event);
    }

    auto LiveviewCanvas::mouseReleaseEvent(QMouseEvent* event) -> void {
        d->leftMouseButtonPressed = false;
        setDragMode(NoDrag);
        QGraphicsView::mouseReleaseEvent(event);
    }

    auto LiveviewCanvas::mouseMoveEvent(QMouseEvent* event) -> void {
        if (d->leftMouseButtonPressed && d->leftMouseButtonPressed != d->lastLeftMouseButtonPressed) {
            setDragMode(ScrollHandDrag);
            const auto pressEvent = new QMouseEvent(QEvent::GraphicsSceneMousePress, event->pos(),
                                                    Qt::MouseButton::LeftButton, Qt::MouseButton::LeftButton,
                                                    Qt::KeyboardModifier::NoModifier);
            mousePressEvent(pressEvent);
        }
        d->lastLeftMouseButtonPressed = d->leftMouseButtonPressed;
        QGraphicsView::mouseMoveEvent(event);
    }

    auto LiveviewCanvas::onLodChanged(const double& lod) -> void {
        d->canvasEventFilter->SetCurrentLod(lod);
        d->scalebar->SetLod(lod);
    }

    void LiveviewCanvas::onRecvZoomRequest(LiveviewZoomPanel::ZoomButtonID buttonID) {
        switch (buttonID) {
            case LiveviewZoomPanel::ZoomButtonID::ZoomIn:
                d->canvasEventFilter->ZoomIn();
                break;
            case LiveviewZoomPanel::ZoomButtonID::ZoomOut:
                d->canvasEventFilter->ZoomOut();
                break;
            case LiveviewZoomPanel::ZoomButtonID::ZoomFit:
                d->canvasEventFilter->ZoomFit();
                break;
        }
    }

    void LiveviewCanvas::onRecvSnapshotRequest() {
        if (d->control.GetImageItem()->pixmap().isNull()) {
            TC::MessageDialog::warning(nullptr, tr("Capture failed"), tr("Live image unavailable."));
        } else {
            const auto captureResult = d->snapshotPanel->SaveFile(d->control.GetImageItem()->pixmap());
            Q_UNUSED(captureResult)
        }
    }

    void LiveviewCanvas::onMenuClicked(const ViewerOptionMenu::MenuIndex& menu) {
        switch (menu) {
            case ViewerOptionMenu::MenuIndex::ShowScalebar:
                d->UpdateScalebar();
                break;
            case ViewerOptionMenu::MenuIndex::ShowCenterMark:
                d->UpdateCenterMark();
                break;
            default:
                break;
        }
    }

    void LiveviewCanvas::onImageItemDoubleClicked(const QPointF& clickedScenePos) {
        // HTX 시스템 좌표계는 원점을 중심으로 Y는 위로 갈수록 증가하지만, canvas는 감소한다. (서로 부호가 반대)
        const auto x = clickedScenePos.toPoint().x();
        const auto y = -clickedScenePos.toPoint().y();
        emit sigCanvasMouseDoubleClicked(x, y);
    }

    auto LiveviewCanvas::Impl::InitScalebar() -> void {
        scalebar = new ScalebarWidget(self);
        scalebar->setVisible(false);
    }

    auto LiveviewCanvas::Impl::InitCanvasProperty() -> void {
        self->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
        self->setOptimizationFlags(DontSavePainterState);
        self->setViewportUpdateMode(SmartViewportUpdate);
        self->setTransformationAnchor(NoAnchor);
        self->setDragMode(NoDrag);
        self->setMouseTracking(true);
    }

    auto LiveviewCanvas::Impl::InitScene() -> void {
        const QSizeF size{256., 256.};
        scene = new QGraphicsScene(self);
        scene->setSceneRect(size.width() / 2., size.height() / 2., size.width(), size.height());
        scene->setBackgroundBrush(Qt::black);
        self->setScene(scene);
    }

    auto LiveviewCanvas::Impl::InitEventFilter() -> void {
        canvasEventFilter = new CanvasZoomSupport(self);
    }

    auto LiveviewCanvas::Impl::InitGraphicsItem() -> void {
        scene->addItem(control.GetImageItem());
        scene->addItem(control.GetRoiItem());
        scene->addItem(control.GetCenterMarkItem());
        connect(control.GetImageItem()->GetLodGetterObject(), &GraphicsLiveviewImageLodGetter::sigLodChanged, self, &Self::onLodChanged);
        connect(control.GetImageItem(), &GraphicsLiveviewImageItem::sigImageItemDoubleClicked, self, &Self::onImageItemDoubleClicked);
    }

    auto LiveviewCanvas::Impl::InitBottomRightWidget() -> void {
        bottomRightWidget = new QWidget(self);
        bottomRightWidget->setStyleSheet("QWidget{background-color:transparent;}");
        snapshotPanel = new LiveviewSnapshotPanel(bottomRightWidget);
        zoomPanel = new LiveviewZoomPanel(bottomRightWidget);

        const auto layout = new QVBoxLayout();
        layout->addWidget(snapshotPanel, 0, Qt::AlignCenter);
        layout->addWidget(zoomPanel, 0, Qt::AlignCenter);
        layout->setSpacing(6);
        layout->setContentsMargins(0, 0, 0, 0);

        bottomRightWidget->setLayout(layout);
        bottomRightWidget->setFixedWidth(24);
        bottomRightWidget->setMinimumHeight(24 * 4 + 6 * 3);

        connect(snapshotPanel, &LiveviewSnapshotPanel::sigSnapshotRequest, self, &Self::onRecvSnapshotRequest);
        connect(zoomPanel, &LiveviewZoomPanel::sigRequestZoom, self, &Self::onRecvZoomRequest);
    }

    auto LiveviewCanvas::Impl::InitOptionWidget() -> void {
        optionWidget = new ViewerOptionMenu::ViewerOptionMenuWidget(self);
        optionWidget->AddMenu(ViewerOptionMenu::MenuIndex::ShowScalebar);
        optionWidget->AddMenu(ViewerOptionMenu::MenuIndex::ShowCenterMark);

        connect(optionWidget, &ViewerOptionMenu::ViewerOptionMenuWidget::sigMenuTriggered, self, &Self::onMenuClicked);
    }

    auto LiveviewCanvas::Impl::InitSubWidgetPosUpdater() -> void {
        posUpdater = new SubWidgetPosUpdater(self);

        // debug mode
        Q_ASSERT(scalebar!=nullptr);
        Q_ASSERT(bottomRightWidget!=nullptr);
        Q_ASSERT(optionWidget!=nullptr);

        posUpdater->SetSubwidget(scalebar, SubWidgetPosUpdater::SubWidgetGeomertry::BottomLeft);
        posUpdater->SetSubwidget(bottomRightWidget, SubWidgetPosUpdater::SubWidgetGeomertry::BottomRight);
        posUpdater->SetSubwidget(optionWidget, SubWidgetPosUpdater::SubWidgetGeomertry::TopRight);
    }

    auto LiveviewCanvas::Impl::UpdateSceneRect() -> void {
        scene->setSceneRect(control.GetSceneRect());
        scene->update();
    }

    auto LiveviewCanvas::Impl::UpdateScalebar() -> void {
        scalebar->setVisible(optionWidget->GetScalebarVisible());
    }

    auto LiveviewCanvas::Impl::UpdateCenterMark() -> void {
        control.ShowCenterMark(optionWidget->GetCenterMarkVisible());
    }
}
