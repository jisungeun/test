#include <QDebug>

#include "ui_LiveviewPanel.h"
#include "LiveviewPanel.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    struct LiveviewPanel::Impl {
        Ui::LiveviewPanel* ui{nullptr};
        bool firstUpdate{true};
        QList<Annotation*> annotations;
    };

    LiveviewPanel::LiveviewPanel(QWidget* parent) : QWidget(parent), d{std::make_unique<Impl>()} {
        d->ui = new Ui::LiveviewPanel();
        d->ui->setupUi(this);

        d->ui->canvas->setObjectName("view-tsx-canvas");
        connect(d->ui->canvas, &LiveviewCanvas::sigCanvasMouseDoubleClicked, this, &Self::sigDoubleClicked);
    }

    LiveviewPanel::~LiveviewPanel() {
        delete d->ui;
    };

    auto LiveviewPanel::SetResolution(double UmPerPixel) -> void {
        d->ui->canvas->SetUmPerPixel(UmPerPixel);
    }

    auto LiveviewPanel::SetImage(const QImage& image) -> void {
        d->ui->canvas->ShowImage(image);
        if(d->firstUpdate && !image.isNull()) {
            d->ui->canvas->ShowScalebar(true);
            d->ui->canvas->ShowROI(true);
            d->ui->canvas->ShowCenterMark(true);
            d->ui->canvas->ZoomFit();
            d->firstUpdate = false;
        }
    }

    auto LiveviewPanel::SetROI(double xInPixel, double yInPixel) -> void {
        const auto centerX = 0;
        const auto centerY = 0;
        const auto roiWidth = xInPixel;
        const auto roiHeight = yInPixel;

        d->ui->canvas->SetROI(centerX, centerY, roiWidth, roiHeight);
    }

    auto LiveviewPanel::ShowScalebar(bool show) -> void {
        d->ui->canvas->ShowScalebar(show);
    }

    auto LiveviewPanel::ShowCenterMark(bool show) -> void {
        d->ui->canvas->ShowCenterMark(show);
    }

    auto LiveviewPanel::AddAnnotation(Annotation* annotation, int32_t xInPixel, int32_t yInPixel) -> void {
        if(!annotation) return;
        d->annotations.push_back(annotation);
        d->ui->canvas->AddAnnotation(annotation, xInPixel, yInPixel);
    }

    auto LiveviewPanel::RemoveAnnotation(Annotation* annotation) -> void {
        d->annotations.removeOne(annotation);
        d->ui->canvas->RemoveAnnotation(annotation);
    }
}
