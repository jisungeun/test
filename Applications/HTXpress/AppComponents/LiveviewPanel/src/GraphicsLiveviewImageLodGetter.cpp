﻿#include "GraphicsLiveviewImageLodGetter.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    struct GraphicsLiveviewImageLodGetter::Impl {
        double lod{1};
    };

    GraphicsLiveviewImageLodGetter::GraphicsLiveviewImageLodGetter(QObject* parent) : QObject(parent), d{ new Impl } {
    }

    auto GraphicsLiveviewImageLodGetter::SetLOD(const double& lod) -> void {
        if(fabs(lod-d->lod)>std::numeric_limits<double>::epsilon())
            emit sigLodChanged(lod);

        d->lod = lod;
    }

    auto GraphicsLiveviewImageLodGetter::GetLOD() const -> double {
        return d->lod;
    }
}
