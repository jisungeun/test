﻿#pragma once

#include <QWidget>

namespace HTXpress::AppComponents::LiveviewPanel {
    class LiveviewSnapshotPanel : public QWidget {
        Q_OBJECT
    public:
        using Self = LiveviewSnapshotPanel;

        explicit LiveviewSnapshotPanel(QWidget* parent = nullptr);
        ~LiveviewSnapshotPanel() override;

        auto SaveFile(const QPixmap& pixmap) -> bool;

    protected:
        void enterEvent(QEvent* event) override;
        void leaveEvent(QEvent* event) override;

    signals:
        void sigSnapshotRequest();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}