﻿#include <QGraphicsRectItem>
#include <QDebug>
#include <QPen>

#include "GraphicsCentermarkItem.h"
#include "GraphicsRoiItem.h"
#include "LiveviewCanvasControl.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    struct LiveviewCanvasControl::Impl {
        GraphicsRoiItem* roiItem{nullptr};
        GraphicsLiveviewImageItem* imageItem{nullptr};
        GraphicsCentermarkItem* centerMark{ nullptr };
        QImage image;

        double sceneRectW{};
        double sceneRectH{};

        QPen roiPen{};

        auto InitRoiItem() -> void;
        auto InitImageItem() -> void;
        auto InitCenterMarkItem() -> void;
    };

    LiveviewCanvasControl::LiveviewCanvasControl() : d{std::make_unique<Impl>()} {
        d->InitRoiItem();
        d->InitImageItem();
        d->InitCenterMarkItem();
    }

    LiveviewCanvasControl::LiveviewCanvasControl(const LiveviewCanvasControl& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    LiveviewCanvasControl::~LiveviewCanvasControl() {
    }

    auto LiveviewCanvasControl::operator=(const LiveviewCanvasControl& other) -> LiveviewCanvasControl& {
        *d = *other.d;
        return *this;
    }

    auto LiveviewCanvasControl::SetImageItem(const QImage& newImage) -> void {
        Q_ASSERT(d->imageItem != nullptr);

        const auto w = newImage.size().width();
        const auto h = newImage.size().height();
        const auto pixmap = QPixmap::fromImage(newImage);

        QBrush brush(pixmap);

        d->imageItem->setPixmap(pixmap);
        d->imageItem->setPos(-w / 2, -h / 2); // warning: possible loss of precision
    }

    auto LiveviewCanvasControl::GetImageItem() const -> GraphicsLiveviewImageItem* {
        return d->imageItem;
    }

    auto LiveviewCanvasControl::ShowImageItem(bool show) -> void {
        d->imageItem->setVisible(show);
    }

    auto LiveviewCanvasControl::SetRoiItem(int32_t x, int32_t y, double w, double h) -> void {
        Q_ASSERT(d->roiItem != nullptr);

        d->roiItem->setRect(-w / 2., -h / 2., w, h);
        d->roiItem->setPos(x, -y);
    }

    auto LiveviewCanvasControl::GetRoiItem() const -> QGraphicsRectItem* {
        return d->roiItem;
    }

    auto LiveviewCanvasControl::ShowRoiItem(bool show) -> void {
        d->roiItem->setVisible(show);
    }

    auto LiveviewCanvasControl::GetCenterMarkItem() const -> QGraphicsItem* {
        return d->centerMark;
    }

    auto LiveviewCanvasControl::ShowCenterMark(bool show) -> void {
        d->centerMark->setVisible(show);
    }

    auto LiveviewCanvasControl::SetSceneRectSize(double w, double h) -> void {
        d->sceneRectW = w;
        d->sceneRectH = h;
    }

    auto LiveviewCanvasControl::GetSceneRect() const -> QRectF {
        const auto x = -(d->sceneRectW / 2.);
        const auto y = -(d->sceneRectH / 2.);

        const QRectF sceneRect = {x, y, d->sceneRectW, d->sceneRectH};

        return sceneRect;
    }

    auto LiveviewCanvasControl::Impl::InitRoiItem() -> void {
        roiItem = new GraphicsRoiItem();
        roiItem->setVisible(false);
    }

    auto LiveviewCanvasControl::Impl::InitImageItem() -> void {
        imageItem = new GraphicsLiveviewImageItem(QPixmap::fromImage(image));
    }

    auto LiveviewCanvasControl::Impl::InitCenterMarkItem() -> void {
        centerMark = new GraphicsCentermarkItem();
        centerMark->setPos(0, 0);
        centerMark->setVisible(true);
    }
}
