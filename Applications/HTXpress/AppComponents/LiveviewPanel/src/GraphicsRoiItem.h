﻿#pragma once
#include <QGraphicsRectItem>

namespace HTXpress::AppComponents::LiveviewPanel {
    class GraphicsRoiItem final : public QGraphicsRectItem {
    public:
        explicit GraphicsRoiItem(QGraphicsItem* parent = nullptr);

    protected:
        auto paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void override;
    };
}
