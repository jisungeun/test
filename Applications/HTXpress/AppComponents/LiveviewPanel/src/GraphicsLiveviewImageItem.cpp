﻿#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

#include "GraphicsLiveviewImageItem.h"
#include "GraphicsLiveviewImageLodGetter.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    struct GraphicsLiveviewImageItem::Impl {
        GraphicsLiveviewImageLodGetter* lodGetter{nullptr};
    };

    GraphicsLiveviewImageItem::GraphicsLiveviewImageItem(const QPixmap& pixmap) : QGraphicsPixmapItem(pixmap), d{new Impl} {
        setZValue(-1000);
        d->lodGetter = new GraphicsLiveviewImageLodGetter();
    }

    GraphicsLiveviewImageItem::~GraphicsLiveviewImageItem() {
    }

    auto GraphicsLiveviewImageItem::GetLodGetterObject() const -> GraphicsLiveviewImageLodGetter* {
        return d->lodGetter;
    }

    auto GraphicsLiveviewImageItem::type() const -> int {
        return LiveviewImage;
    }

    auto GraphicsLiveviewImageItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        d->lodGetter->SetLOD(option->levelOfDetailFromTransform(painter->worldTransform()));
        QGraphicsPixmapItem::paint(painter, option, widget);
    }

    void GraphicsLiveviewImageItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event) {
        emit sigImageItemDoubleClicked(mapToScene(event->pos()));
        QGraphicsPixmapItem::mouseDoubleClickEvent(event);
    }
}
