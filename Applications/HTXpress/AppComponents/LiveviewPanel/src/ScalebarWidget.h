﻿#pragma once

#include <memory>
#include <QWidget>

namespace HTXpress::AppComponents::LiveviewPanel {
    class ScalebarWidget : public QWidget {
        Q_OBJECT
    public:
        using Self = ScalebarWidget;

        ScalebarWidget(QWidget* parent = nullptr);
        ~ScalebarWidget() override;

        auto SetUmPerPixel(const double& umPerPixel) -> void;
        auto SetLod(const double& lod) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
