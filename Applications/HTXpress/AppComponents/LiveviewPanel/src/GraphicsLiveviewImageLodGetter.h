﻿#pragma once
#include <QObject>

namespace HTXpress::AppComponents::LiveviewPanel {
    class GraphicsLiveviewImageLodGetter : public QObject {
        Q_OBJECT
    public:
        explicit GraphicsLiveviewImageLodGetter(QObject* parent = nullptr);
        ~GraphicsLiveviewImageLodGetter() override = default;

        auto GetLOD() const -> double;
        auto SetLOD(const double& lod) -> void;

    signals:
        void sigLodChanged(const double& lod);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
