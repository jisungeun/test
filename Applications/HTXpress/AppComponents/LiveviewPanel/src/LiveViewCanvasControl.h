﻿#pragma once

#include <memory>

#include "GraphicsLiveviewImageItem.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    class LiveviewCanvasControl final {
    public:
        using Self = LiveviewCanvasControl;
        using Pointer = std::shared_ptr<Self>;

        LiveviewCanvasControl();
        LiveviewCanvasControl(const LiveviewCanvasControl& other);
        ~LiveviewCanvasControl();

        auto operator=(const LiveviewCanvasControl& other) -> LiveviewCanvasControl&;

        auto SetImageItem(const QImage& newImage) -> void;
        auto GetImageItem() const -> GraphicsLiveviewImageItem*;
        auto ShowImageItem(bool show) -> void;

        auto SetRoiItem(int32_t x, int32_t y, double w, double h) -> void;
        auto GetRoiItem() const -> QGraphicsRectItem*;
        auto ShowRoiItem(bool show) -> void;

        auto GetCenterMarkItem() const -> QGraphicsItem*;
        auto ShowCenterMark(bool show) -> void;

        auto SetSceneRectSize(double w, double h) -> void;
        auto GetSceneRect() const -> QRectF;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
