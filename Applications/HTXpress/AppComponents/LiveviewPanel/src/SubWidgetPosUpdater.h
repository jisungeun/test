﻿#pragma once
#include <memory>

#include <QObject>

namespace HTXpress::AppComponents::LiveviewPanel {
    class SubWidgetPosUpdater : public QObject {
        Q_OBJECT
    public:
        using Self = SubWidgetPosUpdater;
        using Pointer = std::shared_ptr<Self>;
        using HorizontalPadding = int32_t;
        using VerticalPadding = int32_t;
        enum class SubWidgetGeomertry {
            TopLeft,
            TopCenter,
            TopRight,
            BottomLeft,
            BottomCenter,
            BottomRight
        };

        explicit SubWidgetPosUpdater(QWidget* mainWidget);
        ~SubWidgetPosUpdater() override;

        auto SetSubwidget(QWidget* subwidget, SubWidgetGeomertry where, HorizontalPadding hor = 14, VerticalPadding ver = 14) -> void;

        auto ChangeHorizontalPadding(QWidget* subwidget, HorizontalPadding padding) -> void;
        auto ChangeVerticalPadding(QWidget* subwidget, VerticalPadding padding) -> void;

        auto ChangeAllSubWidgetsPadding(HorizontalPadding h, VerticalPadding v) -> void;

    public slots:
        void onUpdatePosition();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
