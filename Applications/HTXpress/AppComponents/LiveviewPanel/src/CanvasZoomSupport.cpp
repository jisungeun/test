﻿#include <QApplication>
#include <QEvent>
#include <QWheelEvent>
#include <QtMath>
#include <QGraphicsItem>

#include "CanvasZoomSupport.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    struct CanvasZoomSupport::Impl {
        QGraphicsView* view{ nullptr };

        const Qt::KeyboardModifiers zoomKeyModifiers{ Qt::ControlModifier };
        double currLod{ 1 };
        const double zoomFactorValue{ 1.1 };
        QRectF zoomFitRect{};
        QSize lastViewSize{};

        QEvent::Type ZoomFitEventType = static_cast<QEvent::Type>(QEvent::User + 1);

        auto CanZoomIn() const -> bool;
        auto CanZoomOut() const -> bool;
    };

    auto CanvasZoomSupport::Impl::CanZoomIn() const -> bool {
        const auto viewSize = view->size();
        const auto sceneRect = zoomFitRect;

        double targetViewSize{};
        double targetSceneRectSize{};

        if (sceneRect.width() > sceneRect.height()) {
            targetSceneRectSize = sceneRect.width();
            targetViewSize = viewSize.width();
        } else {
            targetSceneRectSize = sceneRect.height();
            targetViewSize = viewSize.height();
        }
        return (targetViewSize * 4 > targetSceneRectSize * currLod);
    }

    auto CanvasZoomSupport::Impl::CanZoomOut() const -> bool {
        const auto viewSize = view->size();
        const auto sceneRect = zoomFitRect;

        double targetViewSize{};
        double targetSceneRectSize{};

        if (sceneRect.width() > sceneRect.height()) {
            targetSceneRectSize = sceneRect.width();
            targetViewSize = viewSize.width();
        } else {
            targetSceneRectSize = sceneRect.height();
            targetViewSize = viewSize.height();
        }
        return (targetViewSize / 3 < targetSceneRectSize * currLod);
    }

    CanvasZoomSupport::CanvasZoomSupport(QGraphicsView* view) : d{ std::make_unique<Impl>() } {
        d->view = view;
        d->zoomFitRect = d->view->sceneRect();
        d->view->viewport()->installEventFilter(this);
        d->view->setMouseTracking(true);
        installEventFilter(this);
    }

    CanvasZoomSupport::~CanvasZoomSupport() {
    }

    auto CanvasZoomSupport::SetCurrentLod(const double& lod) -> void {
        d->currLod = lod;
    }

    auto CanvasZoomSupport::SetZoomFitRect(const QRectF& rect) -> void {
        d->zoomFitRect = rect;
    }

    auto CanvasZoomSupport::ZoomIn(bool onMousePos) const -> void {
        if (!d->CanZoomIn()) {
            return;
        }

        if (onMousePos) {
            d->view->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
        } else {
            d->view->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
        }

        d->view->scale(d->zoomFactorValue, d->zoomFactorValue);
    }

    auto CanvasZoomSupport::ZoomOut(bool onMousePos) const -> void {
        if (!d->CanZoomOut()) {
            return;
        }

        if (onMousePos) {
            d->view->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
        } else {
            d->view->setTransformationAnchor(QGraphicsView::AnchorViewCenter);
        }

        d->view->scale(1 / d->zoomFactorValue, 1 / d->zoomFactorValue);
    }

    auto CanvasZoomSupport::ZoomFit() -> void {
        d->lastViewSize = d->view->viewport()->size();

        d->view->fitInView(d->zoomFitRect, Qt::KeepAspectRatio);

        const auto zoomFitEvent = new QEvent(d->ZoomFitEventType);
        QCoreApplication::postEvent(this, zoomFitEvent);
    }

    auto CanvasZoomSupport::eventFilter(QObject* watched, QEvent* event) -> bool {
        if (watched == d->view->viewport()) {
            if (event->type() == QEvent::Wheel) {
                const auto wheelEvent = dynamic_cast<QWheelEvent*>(event);
                if (QApplication::keyboardModifiers() == d->zoomKeyModifiers) {
                    const double angle = wheelEvent->angleDelta().y();
                    if (angle > 0) {
                        ZoomIn(true);
                    } else {
                        ZoomOut(true);
                    }
                    return true;
                }
            } else if (event->type() == QEvent::MouseButtonDblClick) {
                const auto mouseEvent = dynamic_cast<QMouseEvent*>(event);
                if (mouseEvent->button() == Qt::RightButton) {
                    ZoomFit();
                    return true;
                }
            }
        } else if (watched == this) {
            if (event->type() == d->ZoomFitEventType) {
                if (d->lastViewSize != d->view->viewport()->size()) {
                    ZoomFit();
                }

                return true;
            }
        }

        return false;
    }
}
