﻿#include "GraphicsRoiItem.h"
#include "LiveviewDefines.h"
#include "GraphicsItemHelper.h"

namespace HTXpress::AppComponents::LiveviewPanel {
    GraphicsRoiItem::GraphicsRoiItem(QGraphicsItem* parent) : QGraphicsRectItem(parent) {
        QPen p = pen();
        p.setColor(RoiBorderColor);
        setPen(p);

        setFlag(ItemIsMovable, false);
        setFlag(ItemIsSelectable, false);
        setFlag(ItemIsFocusable, false);
        setZValue(50);
    }

    auto GraphicsRoiItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) -> void {
        Q_UNUSED(option)
        Q_UNUSED(widget)

        painter->setRenderHint(QPainter::Antialiasing);
        QPen p = pen();
        double width = 1.0;

        if (painter->transform().isScaling()) {
            width = GraphicsItemHelper::GetConsistentPenWidth(painter, 2.0);
        }

        p.setWidthF(width);
        painter->setPen(p);
        painter->drawRect(rect());
    }
}
