#include <QDebug>
#include <QGridLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QColorDialog>
#include <QCheckBox>
#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QImage>

#include <LiveviewPanel.h>

#include "LiveviewTestWindow.h"
#include "ui_LiveviewTestWindow.h"

#include <Image2DProc.h>

namespace HTXpress::AppComponents::LiveviewPanel::TEST {
    struct LiveviewTestWindow::Impl {
        Ui::TestMainWindowClass ui;
        QImage image;
        LiveviewPanel* liveview{nullptr};
    };

    LiveviewTestWindow::LiveviewTestWindow(QWidget* parent) : QMainWindow(parent), d{std::make_unique<Impl>()} {
        d->ui.setupUi(this);
        d->liveview = new LiveviewPanel();

        auto layout = new QGridLayout;
        layout->addWidget(d->liveview);
        layout->setContentsMargins(0, 0, 0, 0);
        d->ui.viewFrame->setContentsMargins(0, 0, 0, 0);
        d->ui.viewFrame->setLayout(layout);
        d->ui.viewFrame->setStyleSheet("QFrame{background-color:black;}");
        d->ui.chkColorMap->setChecked(true);
        d->ui.chkHistogram->setChecked(true);
        d->ui.chkSaturation->setChecked(true);
        d->ui.chkScaleBar->setChecked(true);

        ConnectUIs();
    }

    LiveviewTestWindow::~LiveviewTestWindow() {
    }

    void LiveviewTestWindow::onLoadImage() {
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"));
        d->image = QImage(fileName);
        qDebug() << "image.isNull()?" << d->image.isNull();
        if (d->image.isNull()) {
            d->ui.lbFilePath->clear();
            //	return;
        }
        else {
            d->ui.lbFilePath->setText(fileName);
        }

        d->liveview->SetImage(d->image);
    }

    void LiveviewTestWindow::onClearImage() {
        d->liveview->SetImage({});
    }

    void LiveviewTestWindow::onSetResolution() {
        double resolution = d->ui.spResolution->value();
        d->liveview->SetResolution(resolution);
    }

    void LiveviewTestWindow::onSetCenterPos(int32_t x, int32_t y) {
        d->ui.spPosX->setValue(x);
        d->ui.spPosY->setValue(y);
    }

    void LiveviewTestWindow::onShowScalebar(bool show) {
        d->liveview->ShowScalebar(show);
    }

    void LiveviewTestWindow::onSetLiveViewFOV() {
        const auto x = d->ui.spFovX->value();
        const auto y = d->ui.spFovY->value();
        d->liveview->SetROI(x, y);
    }

    auto LiveviewTestWindow::ConnectUIs() -> void {
        connect(d->ui.btnLoadImage, &QPushButton::clicked, this, &Self::onLoadImage);
        connect(d->ui.btnClearImage, &QPushButton::clicked, this, &Self::onClearImage);

        connect(d->ui.btnSetResolution, &QPushButton::clicked, this, &Self::onSetResolution);

        connect(d->liveview, &LiveviewPanel::sigDoubleClicked, this, &Self::onSetCenterPos);

        connect(d->ui.chkScaleBar, &QCheckBox::toggled, this, &Self::onShowScalebar);

        connect(d->ui.btnSetFOV, &QPushButton::clicked, this, &Self::onSetLiveViewFOV);
    }
}
