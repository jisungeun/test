#include "LiveviewTestWindow.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HTXpress::AppComponents::LiveviewPanel::TEST::LiveviewTestWindow w;
    w.show();
    return a.exec();
}
