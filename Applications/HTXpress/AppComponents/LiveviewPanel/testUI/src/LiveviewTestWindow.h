#pragma once

#include <memory>
#include <QtWidgets/QMainWindow>

namespace HTXpress::AppComponents::LiveviewPanel::TEST {
	class LiveviewTestWindow : public QMainWindow {
		Q_OBJECT
	public:
		using Self = LiveviewTestWindow;
		explicit LiveviewTestWindow(QWidget* parent = nullptr);
		~LiveviewTestWindow() override;

	public slots:
		void onLoadImage();
		void onClearImage();
		void onSetResolution();
		void onSetCenterPos(int32_t x, int32_t y);

		void onShowScalebar(bool show);
		void onSetLiveViewFOV();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;

		auto ConnectUIs() -> void;
	};
}
