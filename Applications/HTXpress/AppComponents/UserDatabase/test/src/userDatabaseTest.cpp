#include <catch2/catch.hpp>

#include <random>
#include <list>
#include <algorithm>

#include <QDir>
#include <QDebug>
#include <QDesktopServices>
#include <QUrl>
#include <QApplication>

#include <User.h>
#include <UserManager.h>

#include <UserDatabase.h>

#include "TestFunction.h"

using namespace HTXpress::AppComponents::UserDatabase;
using namespace HTXpress::AppEntity;

namespace UserDatabaseTest {
    TEST_CASE("UserDatabase function test") {
        int argc{};
        char* argv[] = {""};
        QApplication app(argc, argv);

        const auto manager = UserManager::GetInstance();
        const auto component = Component::GetInstance();
        const auto path = QDir::currentPath();
        const auto file = path + QDir::separator() + "usertestdb.ini";

        QDesktopServices::openUrl(QUrl::fromLocalFile(path));

        const auto start = std::chrono::system_clock::now();
        component->Load(file);
        const auto end = std::chrono::system_clock::now();
        const auto sec = std::chrono::duration_cast<std::chrono::seconds>(end-start);
        qDebug() << "load() elapsed time:" << sec.count() << "sec(s).";

        constexpr int32_t TestAddCount{100};
        constexpr int32_t TestRemoveCount{25};

        SECTION("Add") {
            const auto start = std::chrono::system_clock::now();
            for(int32_t i = 0; i < TestAddCount; i++) {
                const auto user = std::make_shared<User>();
                const auto id = QString("id%1").arg(i);
                user->SetUserID(id);
                user->SetName(QString("name%1").arg(i));
                user->SetProfile(Profile::Administrator);
                component->AddUser(user, "pw");
            }
            const auto end = std::chrono::system_clock::now();

            const auto sec = std::chrono::duration_cast<std::chrono::seconds>(end-start);
            qDebug() << "add() elapsed time:" << sec.count() << "sec(s), TestAddCount:" << TestAddCount;

            auto usersOnFile = getUsers(file);
            for(const auto& a : usersOnFile) {
                qDebug() <<"inManager:" << a->GetUserID() << a->GetName() << a->GetProfile();
                const auto& user = manager->GetUser(a->GetUserID());
                CHECK(user->GetUserID() == a->GetUserID());
                CHECK(user->GetName() == a->GetName());
                CHECK(user->GetProfile() == a->GetProfile());
            }

            // except 2 IDs (default, service)
            CHECK(usersOnFile.size() == manager->GetUsers().size()-2);
        }

        SECTION("Remove") {
            std::list<UserID> removed;

            std::random_device rd;
            std::mt19937_64 gen(rd());

            int32_t i = 0;

            const auto start = std::chrono::system_clock::now();
            while(i < TestRemoveCount) {
                std::uniform_int_distribution<int32_t> dis(0,TestAddCount-1);
                const auto randomValue = dis(gen);
                qDebug() << "random value:" << randomValue;
                const auto id = QString("id%1").arg(randomValue);
                component->DeleteUser(id);
                removed.emplace_back(id);
                i++;
            }
            const auto end = std::chrono::system_clock::now();

            const auto sec = std::chrono::duration_cast<std::chrono::seconds>(end-start);
            qDebug() << "remove() elapsed time:" << sec.count() << "sec(s), TestRemoveCount:" << TestRemoveCount;

            for(const auto& a : manager->GetUsers()) {
                // validates all IDs are not nullptr
                CHECK(manager->GetUser(a->GetUserID()) != nullptr);
            }

            for(const auto& a : removed) {
                // check removed IDs are nullptr
                CHECK(manager->GetUser(a) == nullptr);
            }
        }
    }
}

