﻿#pragma once
#include <QList>
#include <QDebug>
#include <QSettings>

#include <User.h>

using namespace HTXpress::AppEntity;

namespace UserDatabaseTest {
    QList<User::Pointer> getUsers(const QString& file) {
        QList<User::Pointer> users;

        QSettings settings(file, QSettings::IniFormat);
        auto size = settings.beginReadArray("User");
        for(int32_t i = 0; i < size; ++i) {
            auto user = std::make_shared<User>();
            settings.setArrayIndex(i);
            const auto id = settings.value("ID").toString();
            const auto name = settings.value("Name").toString();
            const auto type = settings.value("Privilege").toString();

            user->SetUserID(id);
            user->SetName(name);
            user->SetProfile(Profile::_from_string(type.toLatin1()));
            users.push_back(user);
        }
        return users;
    }
}