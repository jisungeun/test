#pragma once
#include <memory>
#include <QCoreApplication>

#include <User.h>
#include "HTXUserDatabaseExport.h"

namespace HTXpress::AppComponents::UserDatabase {
    class HTXUserDatabase_API Component {
        Q_DECLARE_TR_FUNCTIONS(Component)

    public:
        using Pointer = std::shared_ptr<Component>;
        
    protected:
        Component();
        
    public:
        ~Component();
        
        static auto GetInstance()->Pointer;
        
        auto Load(const QString& path)->bool;
        
        auto AddUser(const AppEntity::User::Pointer& user, const QString& password)->bool;
        auto IsValid(const AppEntity::UserID& id, const QString& password) const->bool;
        auto DeleteUser(const AppEntity::UserID& id)->bool;
        auto UpdateUser(const AppEntity::User::Pointer& user)->bool;
        auto ChangePassword(const AppEntity::UserID& id, const QString& password) const->bool;
        
    
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}