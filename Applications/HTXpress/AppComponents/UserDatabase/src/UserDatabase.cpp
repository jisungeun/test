#define LOGGER_TAG "[UserDabase]"
#include <QSettings>
#include <QFileInfo>
#include <QDir>

#include <TCLogger.h>
#include <TCKeyChain.h>
#include <UserManager.h>

#include "UserDatabase.h"

namespace HTXpress::AppComponents::UserDatabase {
    namespace Field {
        constexpr char id[] = "ID";
        constexpr char name[] = "Name";
        constexpr char privilege[] = "Privilege";
    }

    struct Component::Impl {
        QString path;
        std::unique_ptr<TC::KeyChain::KeyChain> keyChain{ nullptr };

        auto Write(const QList<AppEntity::User::Pointer>& users)->void;
    };

    auto Component::Impl::Write(const QList<AppEntity::User::Pointer>& users) -> void {
        auto qs = QSettings(path, QSettings::IniFormat);

        auto index = 0;

        qs.clear();

        qs.beginWriteArray("User");
        for(auto user : users) {
            const auto id = user->GetUserID();
            if(id.compare("Default", Qt::CaseInsensitive) == 0) continue;
            if(id.compare("Service", Qt::CaseInsensitive) == 0) continue;

            qs.setArrayIndex(index++);
            qs.setValue(Field::id, user->GetUserID());
            qs.setValue(Field::name, user->GetName());
            qs.setValue(Field::privilege, user->GetProfile()._to_string());
        }
        qs.endArray();
    }

    auto ReadField(QSettings& qs, const char key[]) -> QVariant {
        if (!qs.contains(key)) throw std::runtime_error(std::string("Missing key in user database: ") + key);
        return qs.value(key);
    }
    
    Component::Component() : d{new Impl} {
    }
    
    Component::~Component() {
    }
    
    auto Component::GetInstance() -> Pointer {
        static Pointer theInstance{ new Component() };
        return theInstance;
    }
    
    auto Component::Load(const QString& path) -> bool {
        d->path = path;

        auto qs = QSettings(path, QSettings::IniFormat);

        auto manager = AppEntity::UserManager::GetInstance();

        try {
            const auto count = qs.beginReadArray("User");
            for(auto idx=0; idx<count; idx++) {
                qs.setArrayIndex(idx);

                auto user = std::make_shared<AppEntity::User>();
                user->SetUserID(ReadField(qs, Field::id).toString());
                user->SetName(ReadField(qs, Field::name).toString());

                auto profile = AppEntity::Profile::_from_string(ReadField(qs, Field::privilege).toString().toLatin1());
                user->SetProfile(profile);

                manager->AddUser(user);
            }
            qs.endArray();
        } catch(std::exception& ex) {
            QLOG_ERROR() << "It fails to read user database. Error=" << ex.what();
            qs.endArray();
            return false;
        }

        auto finfo = QFileInfo(d->path);
        auto dirPath = finfo.absolutePath();

        d->keyChain.reset(new TC::KeyChain::KeyChain("TomoStudioX", QString("%1/password").arg(dirPath)));

        return true;
    }
    
    auto Component::AddUser(const AppEntity::User::Pointer& newUser, const QString& password) -> bool {
        if(d->keyChain == nullptr) return false;

        if(!d->keyChain->Write(newUser->GetUserID(), password)) return false;

        auto manager = AppEntity::UserManager::GetInstance();
        if(!manager->AddUser(newUser)) return false;

        auto users = manager->GetUsers();
        d->Write(users);

        return true;
    }
    
    auto Component::IsValid(const AppEntity::UserID& id, const QString& password) const -> bool {
        auto manager = AppEntity::UserManager::GetInstance();
        auto user = manager->GetUser(id);
        if(user == nullptr) return false;

        if(d->keyChain == nullptr) return false;

        if(id.compare("Default", Qt::CaseInsensitive) == 0) return true;
        return d->keyChain->Check(id, password);
    }

    auto Component::DeleteUser(const AppEntity::UserID& id) -> bool {
        if(d->keyChain == nullptr) return false;

        if(id.compare("Default", Qt::CaseInsensitive) == 0) return false;

        auto manager = AppEntity::UserManager::GetInstance();
        auto user = manager->GetUser(id);
        if(user == nullptr) return false;

        auto profile = user->GetProfile();
        if(profile == +AppEntity::Profile::ServiceEngineer) return false;

        if(!d->keyChain->Delete(id)) return false;
        manager->DeleteUser(id);

        auto users = manager->GetUsers();
        d->Write(users);

        return true;
    }

    auto Component::UpdateUser(const AppEntity::User::Pointer& user) -> bool {
        auto manager = AppEntity::UserManager::GetInstance();
        auto oldUser = manager->GetUser(user->GetUserID());
        if(oldUser == nullptr) return false;

        manager->DeleteUser(user->GetUserID());
        manager->AddUser(user);

        auto users = manager->GetUsers();
        d->Write(users);

        return true;
    }

    auto Component::ChangePassword(const AppEntity::UserID& id, const QString& password) const -> bool {
        if(d->keyChain == nullptr) return false;

        auto manager = AppEntity::UserManager::GetInstance();
        auto user = manager->GetUser(id);
        if(user == nullptr) return false;

        return d->keyChain->Write(id, password);
    }
}
