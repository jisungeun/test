#pragma once
#include <memory>

#include "User.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API UserManager {
    public:
        typedef std::shared_ptr<UserManager> Pointer;

    private:
        UserManager();

    public:
        ~UserManager();

        static auto GetInstance()->Pointer;

        auto AddUser(const User::Pointer user)->bool;
        auto GetUser(UserID id) const->User::Pointer;
        auto GetUsers() const->QList<User::Pointer>;

        auto DeleteUser(UserID id)->void;

        auto IsBuiltInAccount(const UserID& id) const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}