#pragma once
#include <memory>

#include "Position.h"
#include "Area.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API Location {
    public:
        typedef std::shared_ptr<Location> Pointer;
        
    public:
        Location(const QString& title = QString());
        Location(const Location& other);
        Location(const Position& position, const Area& area, const bool isTile, const QString& title = QString());
        ~Location();
        
        auto operator==(const Location& other) const->bool;
        auto operator=(const Location& other)->Location&;

        auto SetCenter(const Position& position)->void;
        auto GetCenter() const->Position;

        auto SetArea(const Area& area, bool isTile)->void;
        auto GetArea() const->Area;
        auto IsTile() const->bool;

        auto SetTitle(const QString& title)->void;
        auto GetTitle() const->QString;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
