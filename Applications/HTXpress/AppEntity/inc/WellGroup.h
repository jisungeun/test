#pragma once
#include <memory>
#include <QString>
#include <QList>

#include "AppEntityDefines.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API WellGroup {
    public:
        typedef std::shared_ptr<WellGroup> Pointer;

        typedef struct Color {
            uint8_t r;
            uint8_t g;
            uint8_t b;
            uint8_t a;

            auto operator=(const Color& other)->Color& = default;
            auto operator==(const Color& other) const->bool {
                if(r != other.r) return false;
                if(g != other.g) return false;
                if(b != other.b) return false;
                if(a != other.a) return false;
                return true;
            }
        } Color;

        typedef struct Index {
            uint32_t rowIdx{ 0 };
            uint32_t colIdx{ 0 };

            Index(uint32_t row, uint32_t col) {
                rowIdx = row;
                colIdx = col;
            }

            auto operator=(const Index& other)->Index& = default;
            auto operator==(const Index& other) const->bool {
                if(rowIdx != other.rowIdx) return false;
                if(colIdx != other.colIdx) return false;
                return true;
            }
        } Index;

    public:
        WellGroup();
        WellGroup(const WellGroup& other);
        virtual ~WellGroup();

        auto operator=(const WellGroup& other)->WellGroup&;
        auto operator==(const WellGroup& other) const->bool;

        auto SetTitle(const QString& title)->void;
        auto GetTitle() const->QString;

        auto SetColor(const Color& color)->void;
        auto GetColor() const->Color;

        auto AddWell(int32_t rowIdx, int32_t colIdx)->void;
        auto AddWell(const Index& index)->void;
        auto AddWells(const QList<Index>& indices)->void;
        auto RemoveWells(const QList<Index>& indices)->void;
        auto GetWells() const->QList<Index>;
        auto ContainsWell(int32_t rowIdx, int32_t colIdx)->bool;
        auto ContainsWell(const Index& index)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}