#pragma once
#include <memory>

#include "Position.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API PositionGroup {
    public:
        PositionGroup(const QString& title = QString(""));
        PositionGroup(const PositionGroup& other);
        PositionGroup(const QString& title, const Position& position);
        ~PositionGroup();

        auto operator=(const PositionGroup& other)->PositionGroup&;
        auto operator==(const PositionGroup& other) const->bool;
        auto operator!=(const PositionGroup& other) const->bool;

        auto GetTitle() const->QString;

        auto SetSize(uint32_t rowCount, uint32_t colCount)->void;
        auto GetRows() const->uint32_t;
        auto GetCols() const->uint32_t;
        auto GetCount() const->uint32_t;

        auto SetWellIndex(const WellIndex& wellIdx)->void;
        auto GetWellIndex() const->WellIndex;

        auto SetLocationIndex(const LocationIndex& locationIndex)->void;
        auto GetLocationIndex()const->LocationIndex;

        auto SetCenter(const Position& center)->void;
        auto GetCenter() const->Position;

        auto SetPosition(uint32_t rowIdx, uint32_t colIdx, const Position& position)->bool;
        auto GetPosition(uint32_t rowIdx, uint32_t colIdx) const->Position;

        auto GetPositions() const->QList<Position>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}