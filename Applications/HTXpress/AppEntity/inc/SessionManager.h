#pragma once
#include <memory>

#include "User.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API SessionManager {
    public:
        typedef std::shared_ptr<SessionManager> Pointer;

    private:
        SessionManager();

    public:
        ~SessionManager();

        static auto GetInstance()->Pointer;

        auto Login(const User::Pointer& user)->void;
        auto Logout()->void;
        auto LoggedIn() const->bool;

        auto GetID() const->UserID;
        auto GetName() const->QString;
        auto GetProfile() const->Profile;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
