#pragma once
#include <memory>
#include <QDateTime>

#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
	class HTXAppEntity_API RawImage {
	public:
		typedef std::shared_ptr<RawImage> Pointer;

		RawImage();
		RawImage(const RawImage& other) = delete;
		RawImage(int32_t sizeX, int32_t sizeY, std::shared_ptr<uint8_t[]> buffer);
		~RawImage();

		void operator=(const RawImage& other) = delete;

		auto SetImage(int32_t sizeX, int32_t sizeY, std::shared_ptr<uint8_t[]> buffer)->void;
		auto GetSizeX() const->int32_t;
		auto GetSizeY() const->int32_t;
		auto GetBuffer() const->std::shared_ptr<uint8_t[]>;

		auto SetTimestamp(const QDateTime& timestamp)->void;
		auto GetTimestamp() const->QDateTime;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}