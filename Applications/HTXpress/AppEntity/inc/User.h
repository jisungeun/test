#pragma once
#include <memory>
#include <QString>

#include "AppEntityDefines.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API User {
    public:
        typedef std::shared_ptr<User> Pointer;

    public:
        User();
        User(const User& other);
        virtual ~User();

        auto operator=(const User& other)->User&;
        auto operator==(const User& other) const->bool;

        auto SetUserID(UserID id)->void;
        auto GetUserID() const->UserID;

        auto SetName(const QString& name)->void;
        auto GetName() const->QString;

        auto SetProfile(Profile profile)->void;
        auto GetProfile() const->Profile;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}