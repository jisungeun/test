#pragma once
#include <memory>
#include <QString>

#include "AppEntityDefines.h"
#include "Project.h"
#include "Experiment.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API ExperimentResult {
    public:
        typedef std::shared_ptr<ExperimentResult> Pointer;

    public:
        ExperimentResult();
        ExperimentResult(const ExperimentResult& other);
        virtual ~ExperimentResult();

        auto operator=(const ExperimentResult& other)->ExperimentResult&;
        auto operator==(const ExperimentResult& other) const->bool;

        auto SetExperimentPath(const QString& path)->void;
        auto GetExperimentPath() const->QString;

        //! [[YYYYMMDD].[project].[experiment].[acq_index]].[specimen].[well].P[index]
        auto SetPrefix(const QString& prefix)->void;
        auto GetPrefix() const->QString;

        auto SetProject(const Project& project)->void;
        auto GetProject() const->Project;

        auto SetExperiment(const Experiment& experiment)->void;
        auto GetExperiment() const->Experiment;

        auto SetDataPath(const QStringList& list)->void;
        auto GetDataPath() const->QStringList;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}