#pragma once
#include <memory>
#include <QList>

#include "ExperimentResult.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API ExperimentResultRepo {
    public:
        typedef std::shared_ptr<ExperimentResultRepo> Pointer;

    private:
        ExperimentResultRepo();

    public:
        ~ExperimentResultRepo();

        static auto GetInstance()->Pointer;

        auto AddResults(const QString& project, const QString& expPrefix, const QList<ExperimentResult::Pointer>& results)->void;
        auto GetResults(const QString& project, const QString& expPrefix) const->QList<ExperimentResult::Pointer>;

        auto GetExperiments(const QString& project) const->QStringList;
        auto GetProjects() const->QStringList;

        auto ClearAll()->void;
        auto ClearExperiment(const QString& project, const QString& expPrefix)->void;
        auto ClearProject(const QString&project)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}