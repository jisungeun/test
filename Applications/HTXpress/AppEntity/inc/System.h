#pragma once
#include <memory>

#include "Model.h"
#include "SystemConfig.h"
#include "Position.h"
#include "Area.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API System {
    public:
        typedef std::shared_ptr<System> Pointer;

    private:
        System();

    public:
        virtual ~System();

        static auto GetInstance()->Pointer;

        static auto SetHostID(const QString& id)->void;
        static auto GetHostID()->QString;

        static auto SetSoftwareVersion(const QString& version)->void;
        static auto GetSoftwareVersion()->QString;

        static auto SetModel(const Model::Pointer& model)->void;
        static auto GetModel()->Model::Pointer;

        static auto SetSystemConfig(const SystemConfig::Pointer& config)->void;
        static auto GetSystemConfig()->SystemConfig::Pointer;

        static auto GetMaximumFOV()->Area;
        static auto GetCameraResolutionInUM()->double;

        static auto GetSystemCenter()->Position;
        static auto GetTileScanOverlap()->uint32_t;

        static auto SetBackgroundPath(const QString& path)->void;
        static auto GetBackgroundPath()->QString;
        static auto GetBackgroundPath(double NA)->QString;

        static auto SetSampleTypeFolderPath(const QString& path)->void;
        static auto GetSampleTypeFolderPath()->QString;
        static auto GetSampleTypeFolderPath(const QString& model)->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}