#pragma once
#include <memory>

#include <enum.h>

#include "Position.h"
#include "Experiment.h"

#include "ImagingConfig.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API SystemStatus {
    public:
        using Pointer = std::shared_ptr<SystemStatus>;

    protected:
        SystemStatus();

    public:
        ~SystemStatus();

        static auto GetInstance()->Pointer;

        auto SetCurrentGlobalPosition(const Position& position)->void;
        auto GetCurrentGlobalPosition() const->Position;

        auto SetCurrentCondensorPosition(const double posInMM)->void;
        auto GetCurrentCondensorPosition() const->double;

        auto SetCurrentVesselIndex(VesselIndex vesselIndex)->void;
        auto GetCurrentVesselIndex() const->VesselIndex;

        auto SetCurrentWell(WellIndex wellIndex)->void;
        auto GetCurrentWell() const->WellIndex;

        auto SetChannelConfig(ImagingMode mode, const ChannelConfig& config)->void;
        auto GetChannelConfig(ImagingMode mode)->ChannelConfig::Pointer;
        auto GetChannelConfigAll()->QMap<ImagingMode,ChannelConfig::Pointer>;

        auto SetScanConfig(ImagingMode mode, const ScanConfig& config)->void;
        auto GetScanConfig(ImagingMode mode)->ScanConfig::Pointer;

        auto SetLiveConfig(const ImagingMode mode, const ChannelConfig& config)->void;
        auto GetLiveConfig(const ImagingMode mode)->ChannelConfig::Pointer;
        auto GetCurrentLiveConfigMode() const->ImagingMode;

        auto SetProjectTitle(const QString& title)->void;
        auto GetProjectTitle() const->QString;
        
        auto SetExperiment(const Experiment::Pointer experiment, const QString& path)->void;
        auto GetExperiment() const->Experiment::Pointer;
        auto GetExperimentPath() const->QString;
        auto ResetExperiment()->void;
        auto IsExperimentLoaded() const->bool;

        auto SetBusy(bool busy)->void;
        auto GetBusy() const->bool;

        auto SetAutoFocusEnabled(bool enable)->void;
        auto GetAutoFocusEnabled() const->bool;

        auto SetTimelapseRunning(bool isRunning)->void;
        auto GetTimelapseRunning() const->bool;

        auto GetNA() const->double;
        
        auto SetExperimentOutputPath(const QString& path)->void;
        auto GetExperimentOutputPath() const->QString;

        auto SetCurrentLiveImage(Modality modality, int32_t channel = 0)->void;
        auto GetCurrentLiveImage() const->std::tuple<Modality, int32_t>;

        auto SetCurrentFocusTarget(const int32_t value)->void;
        auto GetCurrentFocusTarget() const->int32_t;

        auto SetFocusZPosition(const double position)->void;
        auto GetFocusZPosition() const->double;

        auto SetServiceMode(ServiceMode mode)->void;
        auto GetServiceMode() const->ServiceMode;

        auto SetHTIlluminationCalibrated(const bool calibrated)->void;
        auto GetHTIlluminationCalibrated() const->bool;

        auto SetSampleStageEncoderSupported(const bool supported)->void;
        auto GetSampleStageEncoderSupported() const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}