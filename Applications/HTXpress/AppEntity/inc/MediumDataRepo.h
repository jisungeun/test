﻿#pragma once

#include <memory>

#include <QList>

#include "HTXAppEntityExport.h"
#include "AppEntityDefines.h"
#include "MediumData.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API MediumDataRepo {
    public:
        using Self = MediumDataRepo;
        using Pointer = std::shared_ptr<Self>;
        using List = QList<MediumData>;

    private:
        MediumDataRepo();

    public:
        virtual ~MediumDataRepo();

        static auto GetInstance() -> Pointer;

        auto SetMedia(const List& media) -> void;
        auto GetMedia() const -> List&;

        auto GetMedium(const QString& name, MediumData& medium) -> bool; // get mediumData by name

        auto Clear() const -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
