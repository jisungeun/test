#pragma once
#include <memory>
#include <QString>

#include "Project.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API ProjectRepo {
    public:
        typedef std::shared_ptr<ProjectRepo> Pointer;

    private:
        ProjectRepo();

    public:
        ~ProjectRepo();

        static auto GetInstance()->Pointer;

        auto AddProject(const Project::Pointer& project)->bool;
        auto GetProject(const QString& projectName) const->Project::Pointer;
        auto GetProjectTitles() const->QList<QString>;

        auto RemoveProject(const QString& projectTitle)->void;

        auto ClearAll()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}