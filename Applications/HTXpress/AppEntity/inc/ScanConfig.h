#pragma once
#include <memory>

#include "AppEntityDefines.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API ScanConfig {
    public:
        using Pointer = std::shared_ptr<ScanConfig>;

    public:
        ScanConfig();
        ScanConfig(const ScanConfig& other);
        ~ScanConfig();

        auto operator=(const ScanConfig& other)->ScanConfig&;

        auto SetScanMode(FLScanMode mode)->void;
        auto GetScanMode() const->FLScanMode;

        //botoomInPuse, topInPusle : Relative value
        auto SetRange(int32_t bottomInPulse, int32_t topInPulse, int32_t stepInPulse)->void;
        auto GetBottom() const->int32_t;
        auto GetTop() const->int32_t;
        auto GetStep() const->int32_t;

        auto SetSteps(int32_t steps)->void;
        auto GetSteps() const->int32_t;

        //htFocus : Relative value
        auto SetFocusUm(double flFocusOffsetInUm)->void;
        auto GetFocusFLOffsetUm() const->double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
