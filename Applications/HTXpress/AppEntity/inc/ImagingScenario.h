#pragma once
#include <memory>
#include <QString>
#include <QSet>

#include "AppEntityDefines.h"
#include "ImagingSequence.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API ImagingScenario {
    public:
        typedef std::shared_ptr<ImagingScenario> Pointer;

    public:
        ImagingScenario();
        ImagingScenario(const ImagingScenario& other);
        virtual ~ImagingScenario();

        auto operator=(const ImagingScenario& other)->ImagingScenario&;
        auto operator==(const ImagingScenario& other) const->bool;

        //! Unif of startTime = msec
        auto AddSequence(uint32_t startTime, const QString& title, const ImagingSequence::Pointer& sequence)->void;
        auto EditSequence(uint32_t index, uint32_t startTime, const QString& title)->void;
        auto GetSequence(uint32_t index) const->ImagingSequence::Pointer;
        auto GetStartTime(uint32_t index)->uint32_t;
        auto GetTitle(uint32_t index) const->QString;
        auto GetCount() const->int32_t;
        auto RemoveSequence(int32_t index)->void;
        auto RemoveAll()->void;

        auto GetImagingTypes()->QSet<ImagingType>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}