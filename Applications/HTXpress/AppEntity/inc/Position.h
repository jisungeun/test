#pragma once
#include <memory>
#include <QMetaType>
#include <QString>

#include "AppEntityDefines.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API Position {
    public:
        struct RealPos {
            double x{ 0 };
            double y{ 0 };
            double z{ 0 };
        };

    public:
        Position();
        Position(const Position& other);
        Position(int32_t x, int32_t y, int32_t z);
        ~Position();

        auto operator=(const Position& other)->Position&;
        auto operator==(const Position& other) const->bool;
        auto operator!=(const Position& other) const->bool;

        auto operator+(const Position& other) const->Position;
        auto operator-(const Position& other) const->Position;

        auto X() const->int32_t;
        auto Y() const->int32_t;
        auto Z() const->int32_t;

        auto toUM() const->RealPos;
        auto toMM() const->RealPos;

        static auto fromUM(double x, double y, double z)->Position;
        static auto fromMM(double x, double y, double z)->Position;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

Q_DECLARE_METATYPE(HTXpress::AppEntity::Position)