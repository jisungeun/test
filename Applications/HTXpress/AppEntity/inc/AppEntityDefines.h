#pragma once
#include <enum.h>
#include <QString>
#include <QPair>
#include <QMetaType>

namespace HTXpress::AppEntity {
    typedef QString UserID;
    typedef QString ExperimentID;
    typedef int32_t WellIndex;
    typedef int32_t VesselIndex;
    typedef QString VesselModel;
    typedef QString VesselHolderModel;
    typedef QString Medium;
    typedef int32_t LocationIndex;
    typedef int32_t WellGroupIndex;
    typedef QPair<int, int> RowColumn;
    typedef int32_t ROIIndex;
    typedef QString SampleTypeName;

    BETTER_ENUM(Profile, int32_t,
                Operator,
                Administrator,
                ServiceEngineer
    );

    BETTER_ENUM(ExperimentProgress, int32_t,
                NotStarted,
                InProgress,
                Done
    );

    BETTER_ENUM(VesselShape, int32_t,
                Circle,
                Rectangle
    );

    BETTER_ENUM(WellShape, int32_t,
                Circle,
                Rectangle
    );

    BETTER_ENUM(ImagingAreaShape, int32_t,
                Circle,
                Rectangle
    );

    BETTER_ENUM(Modality, int32_t,
                HT,
                FL,
                BF
    );

    BETTER_ENUM(ImagingType, int32_t,
                HT3D,
                HT2D,
                FL3D,
                FL2D,
                BFGray,
                BFColor,
                PreviewBF
    );

    BETTER_ENUM(ImagingMode, int32_t,
                HT3D,
                HT2D,
                BFGray,
                BFColor,
                FLCH0,
                FLCH1,
                FLCH2
                );

    BETTER_ENUM(TriggerType, int32_t,
                FreeRun,
                Trigger
    );

    BETTER_ENUM(Axis, int32_t,
                X,      //Sample Stage X Axis
                Y,      //Sample Stage Y Axis
                Z,      //Sample Stage Z Axis
                C,      //Condenser Axis
                F,      //Filter Axis
                D       //Door Axis
    );

    BETTER_ENUM(FLScanMode, int32_t,
                Default,
                FLFocus
    );

    BETTER_ENUM(ExcFilter, int32_t,
                Internal,
                External
    );

    BETTER_ENUM(AutoFocusParameter, int32_t,
                LensID,
                InFocusRange,
                Direction,
                LoopInterval,
                MaxTrialCount,
                MinSensorValue,
                MaxSensorValue,
                ResolutionMul,
                ResolutionDiv,
                DefaultTargetValue,
                MaxCompensationPulse
    );

    BETTER_ENUM(ImagingSettingMode, int32_t,
                 SingleImaging,
                 TimelapseImaging
    );

    BETTER_ENUM(ServiceMode, int32_t,
                None,
                Configure,
                ModuleTest,
                Maintenance,
                Setup,
                Evaluation
    );

    BETTER_ENUM(SetupRef, int32_t,
                EncoderCalTargetDistanceX,
                EncoderCalTargetDistanceY
    );

    BETTER_ENUM(EvaluationRef, int32_t,
                IntensityUniformityCV,
                CAFScore,
                CAFBrightness,
                AFPositionBelow,
                AFPositionAbove,
                AFDistanceGap,
                BeadScoreThreshold,
                BeadRepeatabilityCV,
                BeadRepeatabilityCount,
                BeadReproducibilityCV,
                BeadReproducibilityMatrix,
                BeadReproducibilityGapX,
                BeadReproducibilityGapY
    );

    BETTER_ENUM(ROIShape, int32_t, 
                Ellipse, 
                Rectangle
    );

    BETTER_ENUM(PreviewParam, int32_t,
                StartEndMarginPulse,
                ForwardDelayCompensationPulse,
                BackwardDelayCompensationPulse,
                ExposureUSec,
                ReadoutUSec,
                IntensityScaleFactor,
                TriggerPulseWidth,
                AccelerationFactor,
                LedChannel,
                FilterChannel,
                OverlapMM,
                SingleMoveMM
    );

    const QString ProjectExtension = "tcxpro";
    const QString ExperimentExtension = "tcxexp";

    const QString ProfileExtension = "prf";
    const QString ImagingProfileSuffix = "img";
}

Q_DECLARE_METATYPE(HTXpress::AppEntity::WellIndex);