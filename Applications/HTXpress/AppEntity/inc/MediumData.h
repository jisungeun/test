﻿#pragma once

#include <memory>
#include <QString>

#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API MediumData {
    public:
        using Self = MediumData;
        using Pointer = std::shared_ptr<Self>;

        MediumData();
        MediumData(const MediumData& other);
        ~MediumData();

        auto operator=(const MediumData& other) -> MediumData&;
        auto operator==(const MediumData& other) const -> bool;
        auto operator!=(const MediumData& other) const -> bool;

        auto SetName(const QString& name) -> void;
        auto GetName() const -> QString;

        auto SetRI(double ri) -> void;
        auto GetRI() const -> double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
