#pragma once
#include <memory>

#include <QString>

#include "AppEntityDefines.h"
#include "Position.h"
#include "FLFilter.h"
#include "FLChannel.h"

#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API SystemConfig {
    public:
        typedef std::shared_ptr<SystemConfig> Pointer;

    public:
        SystemConfig();
        SystemConfig(const SystemConfig& other);
        virtual ~SystemConfig();

        auto operator=(const SystemConfig & other)->SystemConfig&;

        auto GetSimulation() const->bool;

        auto SetDataDir(const QString& dir)->void;
        auto GetDataDir() const->QString;

        auto SetModel(const QString& model)->void;
        auto GetModel() const->QString;

        auto SetSerial(const QString& serial)->void;
        auto GetSerial() const->QString;

        auto SetMCUComPort(int32_t port, int32_t baudrate)->void;
        auto GetMCUPort() const->int32_t;
        auto GetMCUBaudrate() const->int32_t;

        auto SetImagingCameraSerial(const QString& serial)->void;
        auto GetImagingCameraSerial() const->QString;
        auto SetCondenserCameraSerial(const QString& serial)->void;
        auto GetCondenserCameraSerial() const->QString;

        auto SetSystemCenter(const Position& position)->void;
        auto GetSystemCenter() const->Position;

        auto SetFlOutputRange(int32_t minVal, int32_t maxVal)->void;
        auto GetFlOutputRangeMin() const->int32_t;
        auto GetFlOutputRangeMax() const->int32_t;

        auto SetBFLightIntensity(int32_t value)->void;
        auto GetBFLightIntensity() const->int32_t;

        auto SetPreviewLightIntensity(int32_t value)->void;
        auto GetPreviewLightIntensity() const->int32_t;
        auto SetPreviewGainCoefficient(double coeffA, double coeffB, double coeffP, double coeffQ)->void;
        auto GetPreviewGainCoefficient() const->std::tuple<double, double, double, double>;

        auto SetAxisCompensation(Axis axis, double ratio)->void;
        auto GetAxisCompensation(Axis axis) const->double;

        auto SetAutofocusParameter(AutoFocusParameter param, int32_t value)->void;
        auto GetAutofocusParameter(AutoFocusParameter param) const->int32_t;

        auto SetAutofocusReadyPos(double posMm)->void;
        auto GetAutofocusReadyPos() const->double;

        auto SetTileScanOverlap(const uint32_t overlapUm)->void;
        auto GetTileScanOverlap() const->uint32_t;

        auto SetAutoFocusTime(const double second)->void;
        auto GetAutoFocusTime() const->double;

        auto SetFLChannel(int32_t channelIdx, const FLChannel& channel)->void;
        auto SetFLChannels(const QMap<int32_t, FLChannel>& channels)->void;
        auto GetFLChannel(int32_t channelIdx, FLChannel& channel) const->bool;
        auto GetFLChannel(const QString& name, FLChannel& channel) const->bool;
        auto GetFLChannels() const->QList<int32_t>;
        auto RemoveFLChannel(int32_t channelIdx)->void;

        auto SetExcitationFilter(ExcFilter filter) -> void;
        auto GetExcitationFilter() const -> ExcFilter;

        auto SetFLInternalExcitation(int32_t channel, const FLFilter& excitation) -> void;
        auto SetFLInternalExcitations(const QMap<int32_t, FLFilter>& excitations) -> void;
        auto GetFLInternalExcitation(int32_t channel, FLFilter& excitation) -> bool;
        auto GetFLInternalExcitations() const -> QList<int32_t>;
        auto RemoveFLInternalExcitation(int32_t channel) -> void;

        auto SetFLExternalExcitation(int32_t channel, const FLFilter& excitation) -> void;
        auto SetFLExternalExcitations(const QMap<int32_t, FLFilter>& excitations) -> void;
        auto GetFLExternalExcitation(int32_t channel, FLFilter& excitation) -> bool;
        auto GetFLExternalExcitations() const -> QList<int32_t>;
        auto RemoveFLExternalExcitation(int32_t channel) -> void;

        auto GetFLActiveExcitation(int32_t channel, FLFilter& filter)->bool;
        auto GetFLActiveExcitations() const->QList<int32_t>;

        auto SetFLEmission(int32_t channel, const FLFilter& emission) -> void;
        auto SetFLEmissions(const QMap<int32_t, FLFilter>& emissions) -> void;
        auto GetFLEmission(int32_t channel, FLFilter& emission) -> bool;
        auto GetFLEmissions() const -> QList<int32_t>;

        auto RemoveFLEmission(int32_t channel) -> void;

        auto SetHTIlluminationPattern(double NA, int32_t index)->void;
        auto GetHTIlluminationPattern(double NA) const->int32_t;
        auto GetHTIlluminationNAs() const->QList<double>;

        auto SetHTScanParameter(double NA, int32_t step, int32_t slices)->void;
        auto GetHTScanParameter(double NA) const->std::tuple<int32_t, int32_t>;
        auto GetHTScanParameterNAs() const->QList<double>;

        auto SetCondenserAFParameter(double NA, int32_t patternIndex, int32_t intensity, double zOffset)->void;
        auto GetCondenserAFParameter(double NA) const->std::tuple<int32_t, int32_t, double>;
        auto GetCondenserAFParameterNAs() const->QList<double>;

        auto SetIlluminationCalibrationParameter(double NA, int32_t intensityStart, int32_t intensityStep, int32_t threshold)->void;
        auto GetIlluminationCalibrationParameter(double NA) const->std::tuple<int32_t, int32_t, int32_t>;
        auto GetIlluminationCalibrationParameterNAs() const->QList<double>;

        auto SetMinRequiredSpace(int32_t spaceInGB) -> void;
        auto GetMinRequiredSpace() const -> int32_t;

        auto SetCondenserAFScanParameter(int32_t startPosPulse, int32_t intervalPulse, int32_t triggerSlices)->void;
        auto IsCondenserAFScanParameterOverriden() const->bool;
        auto GetCondenserAFScanStartPosPulse() const->int32_t;
        auto GetCondenserAFScanIntervalPulse() const->int32_t;
        auto GetCondenserAFScanTriggerSlices() const->int32_t;
        auto ClearCondenserAFScanParameter()->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
