﻿#pragma once
#include <memory>
#include <QString>

#include <enum.h>
#include "HTXAppEntityExport.h"
#include "AppEntityDefines.h"
#include "Position.h"
#include "Area.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API ROI {
    public:
        using Self = ROI;
        using Pointer = std::shared_ptr<Self>;

        ROI(const Position& centerPos, const Area& size, const ROIShape& shape, const QString& name = QString());
        ROI(const ROI& other);
        ROI(ROI&& other) noexcept;
        ~ROI();

        auto operator=(const ROI& other) -> ROI&;
        auto operator=(ROI&& other) noexcept -> ROI&;

        auto SetCenter(const Position& center) -> void; // center position in mm;
        auto GetCenter() const -> Position;

        auto SetSize(const Area& size) -> void;
        auto GetSize() const -> Area;

        auto SetShape(const ROIShape& shape) -> void;
        auto GetShape() const -> ROIShape;

        auto SetName(const QString& name) -> void;
        auto GetName() const -> QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
