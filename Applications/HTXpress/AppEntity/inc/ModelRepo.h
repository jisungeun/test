#pragma once
#include <memory>
#include <QList>

#include "Model.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API ModelRepo {
    public:
        typedef std::shared_ptr<ModelRepo> Pointer;

    private:
        ModelRepo();

    public:
        ~ModelRepo();

        static auto GetInstance()->ModelRepo::Pointer;

        auto AddModel(const Model::Pointer model)->void;
        auto GetModels() const->QList<Model::Pointer>;
        auto GetModel(const QString name) const->Model::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}