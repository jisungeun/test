#pragma once
#include <memory>
#include <QList>

#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API IlluminationMapIndex {
    public:
        IlluminationMapIndex();
        IlluminationMapIndex(const IlluminationMapIndex& other);
        IlluminationMapIndex(int32_t red, int32_t green, int32_t blue);
        explicit IlluminationMapIndex(const QList<int32_t>& values);
        ~IlluminationMapIndex();

        auto operator=(const IlluminationMapIndex& rhs)->IlluminationMapIndex&;
        auto operator==(const IlluminationMapIndex& rhs) const->bool;
        auto operator<(const IlluminationMapIndex& rhs) const->bool;

        auto GetChannelRed() const->int32_t;
        auto GetChannelGreen() const->int32_t;
        auto GetChannelBlue() const->int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
