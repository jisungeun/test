#pragma once
#include <memory>
#include <QString>

#include "AppEntityDefines.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API Area {
    public:
        struct RealArea {
            double width{ 0 };
            double height{ 0 };
        };

    public:
        Area();
        Area(const Area& other);
        Area(uint32_t width, uint32_t height);
        ~Area();

        auto operator=(const Area& other)->Area&;
        auto operator==(const Area& other) const->bool;
        auto operator!=(const Area& other) const->bool;

        auto Width() const->int32_t;
        auto Height() const->int32_t;

        auto Expand(int32_t width, int32_t height) const->Area;
        auto ExpandInUM(double width, double height) const->Area;
        auto ExpandInMM(double width, double height) const->Area;

        auto toUM() const->RealArea;
        auto toMM() const->RealArea;

        static auto fromUM(double width, double height)->Area;
        static auto fromMM(double width, double height)->Area;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}