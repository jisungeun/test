#pragma once
#include <memory>

#include "AppEntityDefines.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API ChannelConfig {
    public:
        using Pointer = std::shared_ptr<ChannelConfig>;

    public:
        ChannelConfig();
        ChannelConfig(const ChannelConfig& other);
        ~ChannelConfig();

        auto operator=(const ChannelConfig& other)->ChannelConfig&;
        auto operator==(const ChannelConfig& other) const->bool;
        auto operator!=(const ChannelConfig& other) const->bool;

        auto SetEnable(bool bEnable)->void;
        auto GetEnable() const->bool;

        auto SetCameraFov(uint32_t xPixels, uint32_t yPixels)->void;
        auto GetCameraFovX() const->uint32_t;
        auto GetCameraFovY() const->uint32_t;

        auto SetCameraExposureUSec(uint32_t value)->void;
        auto GetCameraExposureUSec() const->uint32_t;

        auto SetCameraIntervalUSec(uint32_t interval)->void;
        auto GetCameraIntervalUSec() const->uint32_t;

        auto SetCameraInternal(bool internal)->void;
        auto GetCameraInternal() const->bool;

        auto SetLightChannel(int32_t index)->void;
        auto GetLightChannel() const->int32_t;

        auto SetLightIntensity(uint32_t value)->void;
        auto GetLightIntensity() const->uint32_t;

        auto SetLightInternal(bool internal)->void;
        auto GetLightInternal() const->bool;

        auto SetFilterChannel(int32_t index)->void;
        auto GetFilterChannel() const->int32_t;

        auto SetChannelName(const QString& name)->void;
        auto GetChannelName() const->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
