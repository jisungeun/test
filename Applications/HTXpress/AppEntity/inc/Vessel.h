#pragma once
#include <memory>
#include <QString>

#include "AppEntityDefines.h"
#include "Position.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API Vessel {
    public:
        typedef std::shared_ptr<Vessel> Pointer;

    public:
        Vessel();
        Vessel(const Vessel& other);
        virtual ~Vessel();

        auto operator=(const Vessel& other)->Vessel&;
        auto operator==(const Vessel& other) const->bool;

        auto SetModel(VesselModel model)->void;
        auto GetModel() const->VesselModel;

        auto SetName(const QString& name)->void;
        auto GetName() const->QString;

        auto SetNA(double na) -> void;
        auto GetNA() const -> double;

        auto SetAFOffset(int32_t offsetUm)->void;
        auto GetAFOffset() const->int32_t;

        auto SetMultiDish(bool multiDish)->void;
        auto IsMultiDish() const->bool;

        auto SetSize(double sizeX, double sizeY)->void;
        auto GetSizeX() const->double;
        auto GetSizeY() const->double;

        auto SetWellShape(WellShape shape)->void;
        auto GetWellShape() const->WellShape;

        auto SetWellSpacing(double hor, double ver)->void;
        auto GetWellSpacingHorizontal() const->double;
        auto GetWellSpacingVertical() const->double;

        auto SetWellSize(double sizeX, double sizeY)->void;
        auto GetWellSizeX() const->double;
        auto GetWellSizeY() const->double;

        auto SetWellCount(int32_t rows, int32_t cols)->void;
        auto GetWellRows() const->int32_t;
        auto GetWellCols() const->int32_t;

        auto AddWell(int32_t rowIdx, int32_t colIdx, double posX, double posY, const QString& label=QString())->void;
        auto GetWellCount() const->int32_t;
        auto GetWellIndices() const->QList<WellIndex>;
        auto GetWellIndex(int32_t rowIdx, int32_t colIdx)->WellIndex;
        auto GetWellRowIndex(WellIndex index) const->int32_t;
        auto GetWellColIndex(WellIndex index) const->int32_t;
        auto GetWellPositionX(int32_t rowIdx, int32_t colIdx) const->double;
        auto GetWellPositionY(int32_t rowIdx, int32_t colIdx) const->double;
        auto GetWellPosition(int32_t rowIdx, int32_t colIdx) const->Position;
        auto GetWellLabel(int32_t rowIdx, int32_t colIdx) const->QString;
        auto GetWellPositionX(WellIndex index) const->double;
        auto GetWellPositionY(WellIndex index) const->double;
        auto GetWellPosition(WellIndex index) const->Position;
        auto GetWellLabel(WellIndex index) const->QString;

        auto SetImagingAreaShape(ImagingAreaShape shape)->void;
        auto GetImagingAreaShape() const->ImagingAreaShape;

        auto SetImagingAreaSize(double sizeX, double sizeY)->void;
        auto GetImagingAreaSizeX() const->double;
        auto GetImagingAreaSizeY() const->double;

        auto SetImagingAreaPosition(double posX, double posY)->void;
        auto GetImagingAreaPositionX() const->double;
        auto GetImagingAreaPositionY() const->double;

        auto IsInImagingArea(const double& posXInWell, const double& posYInWell) const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}