#pragma once
#include <memory>
#include <QColor>
#include <QString>

#include "AppEntityDefines.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API FLChannel {
    public:
        FLChannel();
        FLChannel(const FLChannel& other);
        ~FLChannel();

        auto operator=(const FLChannel& other)->FLChannel&;
        auto operator==(const FLChannel& other)const->bool;
        auto operator!=(const FLChannel& other)const->bool;

        auto SetName(const QString& name)->void;
        auto GetName() const->QString;

        auto SetExcitation(uint32_t value)->void;
        auto GetExcitation() const->uint32_t;

        auto SetExcitationBandwidth(uint32_t value) -> void;
        auto GetExcitationBandwidth() const -> uint32_t;

        auto SetEmission(uint32_t value)->void;
        auto GetEmission() const->uint32_t;

        auto SetEmissionBandwidth(uint32_t value)->void;
        auto GetEmissionBandwidth() const -> uint32_t;

        auto SetColor(const QColor& color) -> void;
        auto GetColor() const -> QColor;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}