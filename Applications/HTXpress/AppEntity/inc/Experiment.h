#pragma once
#include <memory>
#include <QString>

#include "AppEntityDefines.h"
#include "MediumData.h"
#include "Vessel.h"
#include "WellGroup.h"
#include "Location.h"
#include "ImagingScenario.h"
#include "HTXAppEntityExport.h"
#include "ROI.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API Experiment {
    public:
        typedef std::shared_ptr<Experiment> Pointer;

    public:
        Experiment();
        Experiment(const Experiment& other);
        virtual ~Experiment();

        auto operator=(const Experiment& other)->Experiment&;
        auto operator==(const Experiment& other) const->bool;

        auto SetID(ExperimentID id)->void;
        auto GetID() const->ExperimentID;

        auto SetTitle(const QString& title)->void;
        auto GetTitle() const->QString;

        auto SetUserID(UserID id)->void;
        auto GetUserID() const->UserID;

        auto SetMedium(const MediumData medium)->void;
        auto GetMedium() const->MediumData;

        auto SetCreatedDate(const QString& date)->void;
        auto GetCreatedDate() const->QString;

        auto SetProgress(ExperimentProgress progress)->void;
        auto GetProgress() const->ExperimentProgress;

        auto SetVesselInfo(Vessel::Pointer vessel)->void;
        auto GetVessel() const->Vessel::Pointer;

        auto SetFOV(const AppEntity::Area& fov)->void;
        auto GetFOV() const->AppEntity::Area;
        
        //VesselIndex는 하나의 실험에 여러개의 Vessel을 연속적으로 사용하는 시나리오에서 n번째 Vessel을
        //지정하는 목적으로 사용됨
        //각 Vessel은 독립적으로 Well Name, Well Group, Acquisition Point, Imaging Scenario 를 포함함
        auto SetWellName(VesselIndex vesselIdx, WellIndex wellIdx, const QString& name)->void;
        auto GetWellName(VesselIndex vesselIdx, WellIndex wellIdx) const->QString;
        auto GetWellNames(VesselIndex vesselIdx) const->QMap<WellIndex, QString>;
        auto GetWellPositionNames(VesselIndex vesselIdx) const->QMap<WellIndex, QString>;

        auto SetWellGroup(VesselIndex vesselIdx, WellGroupIndex groupIdx, const WellGroup& group)->void;
        auto GetWellGroupCount(VesselIndex vesselIdx) const->int32_t;
        auto GetWellGroup(VesselIndex vesselIdx, WellGroupIndex groupIdx) const->WellGroup;
        auto GetWellGroupIndices(VesselIndex vesselIdx) const->QList<WellGroupIndex>;
        auto SetWellGroupName(VesselIndex vesselIdx, WellGroupIndex groupIdx, const QString& groupName)->void;
        auto GetWellGroupName(VesselIndex vesselIdx, WellIndex) const->QString;
        auto GetWellGroupNames(VesselIndex vesselIdx) const->QMap<WellIndex, QString>;
        auto DeleteWellGroup(VesselIndex vesselIdx, WellGroupIndex groupIdx)->void;
        auto ClearWellGroups(VesselIndex vesselIdx)->void;
        auto GetNewWellGroupIndex()->WellGroupIndex;

        auto AddWellsToGroup(VesselIndex vesselIdx, WellGroupIndex groupIdx, const QList<QPair<int, int>>& indices)->void;
        auto RemoveWells(VesselIndex vesselIdx, const QList<QPair<int, int>>& indices)->void;

        auto SetVesselCount(int32_t count)->void;
        auto GetVesselCount() const->int32_t;

        auto SetLastLocationIndex(VesselIndex vesselIndex, WellIndex wellIndex, LocationIndex lastLocationIndex)->void;
        auto GetLastLocationIndex(VesselIndex vesselIndex, WellIndex wellIndex)const->VesselIndex;

        auto AddLocation(VesselIndex vesselIdx, WellIndex wellIdx, Location location)->LocationIndex;
        auto AddLocation(VesselIndex vesselIdx, WellIndex wellIdx, LocationIndex locIndex, Location location)->void;
        auto GetLocationCount(VesselIndex vesselIdx, WellIndex wellIdx) const->int32_t;
        auto GetLocation(VesselIndex vesselIdx, WellIndex wellIdx, LocationIndex index) const->Location&;
        auto GetAllLocations(VesselIndex vesselIndex) const->QMap<WellIndex,QMap<LocationIndex, Location>>&;
        auto GetAllLocationsInVessel(VesselIndex vesselIndex) const->QList<Location>;
        auto DeleteLocation(VesselIndex vesselIdx, WellIndex wellIdx, LocationIndex index)->bool;
        auto ClearLocation(VesselIndex vesselIdx)->void;

        auto SetScenario(VesselIndex vesselIdx, ImagingScenario::Pointer scenario)->void;
        auto GetScenario(VesselIndex vesselIdx) const->ImagingScenario::Pointer;

        auto SetSingleImagingCondition(ImagingType type, ImagingCondition::Pointer condition)->void;
        auto GetSingleImagingCondition(ImagingType type) const->ImagingCondition::Pointer;
        auto SetEnableSingleImaging(ImagingType type, bool enable)->void;
        auto GetEnableSingleImaging(ImagingType type) const->bool;
        auto GetEnabledImagingTypes() const->QList<ImagingType>;

        auto AddROI(VesselIndex vesselIdx, WellIndex wellIdx, const ROI::Pointer& roi) -> void;
        auto AddROI(VesselIndex vesselIdx, WellIndex wellIdx, ROIIndex roiIdx, const ROI::Pointer& roi) -> void;
        auto SetROIs(VesselIndex vesselIdx, QMap<WellIndex, QMap<ROIIndex, ROI::Pointer>> rois)->void;
        auto DeleteROI(VesselIndex vesselIdx, WellIndex wellIdx, ROIIndex roiIdx)->bool;
        auto GetROIs(VesselIndex vesselIdx)const->QMap<WellIndex, QMap<ROIIndex, ROI::Pointer>>&;
        auto GetROIs(VesselIndex vesselIdx, WellIndex wellIdx) const->QMap<ROIIndex, ROI::Pointer>;

        auto SetSampleTypeName(const SampleTypeName& sampleTypeName)->void;
        auto GetSampleTypeName() const -> SampleTypeName;

        static auto GenerateID(const QString& userId, const QString &title)->QString;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
