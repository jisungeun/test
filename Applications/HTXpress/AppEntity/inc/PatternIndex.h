#pragma once
#include <memory>

#include "AppEntityDefines.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API PatternIndex {
    public:
        PatternIndex();
        PatternIndex(const PatternIndex& other);
        PatternIndex(ImagingType imagingType, TriggerType triggerType, int32_t channel);
        ~PatternIndex();

        auto operator=(const PatternIndex& rhs)->PatternIndex&;
        auto operator<(const PatternIndex& rhs) const->bool;

        auto GetImagingType() const->ImagingType;
        auto GetTriggerType() const->TriggerType;
        auto GetChannel() const->int32_t;
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}