﻿#pragma once

#include <memory>

#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API FLFilter {
    public:
        using Self = FLFilter;
        using Pointer = std::shared_ptr<Self>;

        FLFilter();
        FLFilter(const FLFilter& other);
        FLFilter(int32_t wavelength, int32_t bandwidth);
        ~FLFilter();

        auto operator=(const FLFilter& other) -> FLFilter&;
        auto operator==(const FLFilter& other) const -> bool;
        auto operator!=(const FLFilter& other) const -> bool;
        auto operator<(const FLFilter& other) const -> bool;

        auto SetWaveLength(int32_t waveLength) -> void;
        auto GetWaveLength() const -> int32_t;

        auto SetBandwidth(int32_t bandwidth) -> void;
        auto GetBandwidth() const -> int32_t;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
