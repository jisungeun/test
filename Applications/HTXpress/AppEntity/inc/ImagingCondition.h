#pragma once
#include <memory>
#include <QList>
#include <QMap>
#include <QVariant>
#include <QJsonObject>

#include "AppEntityDefines.h"
#include "FLFilter.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API ImagingCondition {
    public:
        typedef std::shared_ptr<ImagingCondition> Pointer;

    public:
        ImagingCondition(Modality modality=Modality::HT);
        ImagingCondition(const ImagingCondition& other);
        ImagingCondition(const QJsonObject& object);
        virtual ~ImagingCondition();

        auto operator=(const ImagingCondition& other)->ImagingCondition&;
        auto operator==(const ImagingCondition& other) const->bool;

        virtual auto Set(const ImagingCondition::Pointer other)->void;
        virtual auto Clone()->ImagingCondition::Pointer = 0;
        virtual auto Clear()->void;
        virtual auto GetValueMap() const->QMap<QString,QVariant>;
        virtual auto SetValueMap(const QMap<QString,QVariant>& map)->void;
        virtual auto ToJsonObject(QJsonObject object = QJsonObject()) const->QJsonObject;

        virtual auto GetImageCount() const->uint32_t = 0;
        virtual auto GetImagingType() const->ImagingType = 0;

        auto GetModality() const->Modality;
        auto CheckModality(Modality modality) const->bool;

        auto SetDimension(int32_t dimension)->void;
        auto Is2D() const->bool;
        auto Is3D() const->bool;

        //! Unit of SliceStart - pulse, It is relative to the current position
        auto SetSliceStart(int32_t pos)->void;
        auto GetSliceStart() const->int32_t;

        auto SetSliceCount(int32_t count)->void;
        auto GetSliceCount() const->int32_t;

        //! Unit of Step - pulse
        auto SetSliceStep(int32_t step)->void;
        auto GetSliceStep() const->int32_t;

        //! Unit of Exposure - usec
        virtual auto SetExposure(int32_t channel, int32_t exposure)->void = 0;
        virtual auto GetExposure(int32_t channel) const->int32_t = 0;

        //! Unit of Interval = usec
        virtual auto SetInterval(int32_t channel, int32_t interval)->void = 0;
        virtual auto GetInterval(int32_t channel) const->int32_t = 0;

        //! Unit of intensity - No unit
        virtual auto SetIntensity(int32_t channel, int32_t intensity)->void = 0;
        virtual auto GetIntensity(int32_t channel) const->int32_t = 0;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class HTXAppEntity_API ImagingConditionHT : public ImagingCondition {
    public:
        typedef std::shared_ptr<ImagingConditionHT> Pointer;

    public:
        ImagingConditionHT();
        ImagingConditionHT(const ImagingConditionHT& other);
        ImagingConditionHT(const QJsonObject& object);
        ~ImagingConditionHT() override;

        auto operator=(const ImagingConditionHT& other)->ImagingConditionHT&;
        auto operator==(const ImagingConditionHT& other) const->bool;

        auto Set(const ImagingCondition::Pointer other)->void override;
        auto Clone()->ImagingCondition::Pointer override;
        auto Clear() -> void override;
        auto GetValueMap() const->QMap<QString,QVariant> override;
        auto SetValueMap(const QMap<QString,QVariant>& map)->void override;
        auto ToJsonObject(QJsonObject object) const -> QJsonObject override;

        auto GetImageCount() const -> uint32_t override;
        auto GetImagingType() const -> ImagingType override;

        //! Unit of Exposure - usec
        auto SetExposure(int32_t exposure)->void;
        auto SetExposure(int32_t channel, int32_t exposure) -> void override;
        auto GetExposure() const->int32_t;
        auto GetExposure(int32_t channel) const -> int32_t override;

        //! Unit of Interval = usec
        auto SetInterval(int32_t interval)->void;
        auto SetInterval(int32_t channel, int32_t interval) -> void override;
        auto GetInterval() const->int32_t;
        auto GetInterval(int32_t channel) const -> int32_t override;

        //! Unit of intensity - No unit
        auto SetIntensity(int32_t intensity)->void;
        auto SetIntensity(int32_t channel, int32_t intensity) -> void override;
        auto GetIntensity() const->int32_t;
        auto GetIntensity(int32_t channel) const -> int32_t override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class HTXAppEntity_API ImagingConditionFL : public ImagingCondition {
    public:
        typedef std::shared_ptr<ImagingConditionFL> Pointer;

        struct Color {
            int32_t red{ 0 };
            int32_t green{ 0 };
            int32_t blue{ 0 };
        };

    public:
        ImagingConditionFL();
        ImagingConditionFL(const ImagingConditionFL& other);
        ImagingConditionFL(const QJsonObject& object);
        ~ImagingConditionFL() override;

        auto operator=(const ImagingConditionFL& other)->ImagingConditionFL&;
        auto operator==(const ImagingConditionFL& other) const->bool;

        auto Set(const ImagingCondition::Pointer other)->void override;
        auto Clone()->ImagingCondition::Pointer override;
        auto Clear() -> void override;
        auto GetValueMap() const->QMap<QString,QVariant> override;
        auto SetValueMap(const QMap<QString,QVariant>& map)->void override;
        auto ToJsonObject(QJsonObject object) const -> QJsonObject override;

        auto GetImageCount() const -> uint32_t override;
        auto GetImagingType() const -> ImagingType override;

        //! Unit of Exposure - usec, Unit of Intensity - No unit
        auto SetChannel(int32_t channel, int32_t exposure, int32_t interval, int32_t intensity, double gain, 
                        FLFilter exFilter, FLFilter emFilter, const QString& name, const Color color=Color())->void;
        auto GetChannel(int32_t channel) const->std::tuple<int32_t, int32_t, int32_t, double, FLFilter, FLFilter, QString, Color>;
        auto SetExposure(int32_t channel, int32_t exposure) -> void override;
        auto GetExposure(int32_t channel) const->int32_t override;
        auto SetInterval(int32_t channel, int32_t interval) -> void override;
        auto GetInterval(int32_t channel) const->int32_t override;
        auto SetIntensity(int32_t channel, int32_t intensity) -> void override;
        auto GetIntensity(int32_t channel) const->int32_t override;
        auto GetGain(int32_t channel) const->double;
        auto GetExFilter(int32_t channel) const->FLFilter;
        auto GetEmFilter(int32_t channel) const->FLFilter;
        auto GetName(int32_t channel) const->QString;
        auto GetColor(int32_t channel) const->Color;
        auto GetChannels() const->QList<int32_t>;
        auto DeleteChannel(int32_t channel) const->void;

        //Additional information
        auto SetScanMode(FLScanMode mode)->void;
        auto GetScanMode() const->FLScanMode;
        auto SetScanRange(int32_t bottom, int32_t top, int32_t step)->void;
        auto GetScanRangeBottom() const->int32_t;
        auto GetScanRangeTop() const->int32_t;
        auto GetScanRangeStep() const->int32_t;
        auto SetScanFocus(double htFocusInUm, double flFocusOffsetInUm)->void;
        auto GetScanFocusHT() const->double;
        auto GetScanFocusFLOffset() const->double;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

    class HTXAppEntity_API ImagingConditionBF : public ImagingCondition {
    public:
        typedef std::shared_ptr<ImagingConditionBF> Pointer;

    public:
        ImagingConditionBF();
        ImagingConditionBF(const ImagingConditionBF& other);
        ImagingConditionBF(const QJsonObject& object);
        ~ImagingConditionBF() override;

        auto operator=(const ImagingConditionBF& other)->ImagingConditionBF&;
        auto operator==(const ImagingConditionBF& other) const->bool;

        auto Set(const ImagingCondition::Pointer other)->void override;
        auto Clone()->ImagingCondition::Pointer override;
        auto Clear() -> void override;
        auto GetValueMap() const->QMap<QString,QVariant> override;
        auto SetValueMap(const QMap<QString,QVariant>& map)->void override;
        auto ToJsonObject(QJsonObject object) const -> QJsonObject override;

        auto GetImageCount() const -> uint32_t override;
        auto GetImagingType() const -> ImagingType override;

        auto SetColorMode(bool mode)->void;
        auto GetColorMode() const->bool;

        //! Unit of Exposure - usec
        auto SetExposure(int32_t exposure)->void;
        auto SetExposure(int32_t channel, int32_t exposure) -> void override;
        auto GetExposure() const->int32_t;
        auto GetExposure(int32_t channel) const -> int32_t override;

        //! Unit of Interval = usec
        auto SetInterval(int32_t interval)->void;
        auto SetInterval(int32_t channel, int32_t interval) -> void override;
        auto GetInterval() const->int32_t;
        auto GetInterval(int32_t channel) const -> int32_t override;

        //! Unit of intensity - No unit
        auto SetIntensity(int32_t intensity)->void;
        auto SetIntensity(int32_t channel, int32_t intensity) -> void override;
        auto GetIntensity() const->int32_t;
        auto GetIntensity(int32_t channel) const -> int32_t override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}