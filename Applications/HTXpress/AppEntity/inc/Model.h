#pragma once
#include <memory>
#include <QString>
#include <QList>

#include "AppEntityDefines.h"
#include "PatternIndex.h"
#include "IlluminationMapIndex.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API Model {
    public:
        using Pointer = std::shared_ptr<Model>;
        using Axis = AppEntity::Axis;

    public:
        Model(const QString& name=QString());
        Model(const Model& other);
        virtual ~Model();

        auto operator=(const Model& other)->Model&;

        auto Name() const->QString;

        //optical specification
        virtual auto ObjectiveLensMagnification() const->double = 0;
        virtual auto ObjectiveLensNA() const->double = 0;
        virtual auto CondenserLensMagnification() const->double = 0;
        virtual auto CondenserLensNA() const->double = 0;
        virtual auto CameraPixelSize() const->double = 0;
        virtual auto CameraPixelsH() const->uint32_t = 0;
        virtual auto CameraPixelsV() const->uint32_t = 0;
        virtual auto CameraFOVStepH() const->uint32_t = 0;
        virtual auto CameraFOVStepV() const->uint32_t = 0;

        //imaging specification
        virtual auto MinimumIntervalUSec() const->int32_t = 0;
        virtual auto MaximumIntervalUSec() const->int32_t = 0;
        virtual auto MinimumIdleUSec() const->int32_t = 0;      //Idle between the end of exposure and the start of exposure
        virtual auto MinimumExposureUSec() const->int32_t = 0;
        virtual auto MaximumExposureUSec() const->int32_t = 0;
        virtual auto HTLightChannel() const->int32_t = 0;
        virtual auto HTFilterChannel() const->int32_t = 0;
        virtual auto BFLightChannel() const->int32_t = 0;
        virtual auto BFFilterChannel() const->int32_t = 0;
        virtual auto FLMaximumSlices() const->int32_t = 0;
        virtual auto CondenserAFLightChannel() const->int32_t = 0;
        virtual auto SafeXYRangeMM() const->std::tuple<double,double> = 0;

        //motion specification
        virtual auto AxisResolutionPPM(Axis axis) const->uint32_t = 0;
        virtual auto AxisLowerLimitMM(Axis axis) const->double = 0;
        virtual auto AxisUpperLimitMM(Axis axis) const->double = 0;
        auto AxisLowerLimitPulse(Axis axis) const->int32_t;
        auto AxisUpperLimitPulse(Axis axis) const->int32_t;
        virtual auto AxisMotionTimeSPM(Axis axis) const->double = 0;

        //predefined positions
        virtual auto SafeZPositionMM() const->double = 0;
        auto SafeZPositionPulse() const->int32_t;

        virtual auto LoadingXPositionMM() const->double = 0;
        virtual auto LoadingYPositionMM() const->double = 0;
        auto LoadingXPositionPulse() const->int32_t;
        auto LoadingYPositionPulse() const->int32_t;

        virtual auto EmissionFilterPules() const->QList<int32_t> = 0;
        auto EmissionFilterPulse(int32_t channel) const->int32_t;

        virtual auto DripTrayRemovingXPositionMM() const->double = 0;
        virtual auto DripTrayRemovingYPositionMM() const->double = 0;
        virtual auto DripTrayRemovingZPositionMM() const->double = 0;
        virtual auto DripTrayRemovingCPositionMM() const->double = 0;
        virtual auto ParkingXPositionMM() const->double = 0;
        virtual auto ParkingYPositionMM() const->double = 0;
        virtual auto ParkingZPositionMM() const->double = 0;
        virtual auto ParkingCPositionMM() const->double = 0;

        //Time estimation parameters
        virtual auto FilterChangeTime() const->std::tuple<double,double> = 0;
        virtual auto SampleStageMotionTime() const->std::tuple<double,double> = 0;
        virtual auto ZScanMotionTime() const->std::tuple<double,double> = 0;
        virtual auto TriggerWithMotionAccelerationTime() const->double = 0;
        virtual auto TriggerWithMotionDecelerationTime() const->double = 0;
        virtual auto TriggerWithMotionMarginTime() const->double = 0;
        virtual auto FocusToScanReadyMM() const->double = 0;
        virtual auto BFImagingMarginTime() const->double = 0;

        //HT scan parameters
        virtual auto HTTriggerStartOffsetPulse() const->int32_t = 0;
        virtual auto HTTriggerEndOffsetPulse() const->int32_t = 0;
        virtual auto HTTriggerPulseWidthUSec() const->int32_t = 0;
        virtual auto HTTriggerReadOutUSec() const->int32_t = 0;
        virtual auto HTTriggerExposureUSec() const->int32_t = 0;
        virtual auto HTTriggerIntervalMarginUSec() const->int32_t = 0;
        virtual auto HTTriggerAccelerationFactor() const->int32_t = 0;

        //Condenser AF Scan parameters
        virtual auto CondenserAFScanStartPosPulse() const->int32_t = 0;
        virtual auto CondenserAFScanIntervalPulse() const->int32_t = 0;
        virtual auto CondenserAFScanTriggerSlices() const->int32_t = 0;
        virtual auto CondenserAFScanReadOutUSec() const->int32_t = 0;
        virtual auto CondenserAFScanExposureUSec() const->int32_t = 0;
        virtual auto CondenserAFScanIntervalMarginUSec() const->int32_t = 0;
        virtual auto CondenserAFScanPulseWidthUSec() const->int32_t = 0;

        //multi-dish holder
        virtual auto MultiDishHolderThickness() const->double = 0;

        //illumination patterns
        virtual auto GetIlluminationPattern(ImagingType imagingType, TriggerType triggerType, int32_t channel) const -> int32_t = 0;
        virtual auto GetIlluminationPattern(const PatternIndex& index) const -> int32_t = 0;
        virtual auto GetIlluminationPatterns() const->QList<PatternIndex> = 0;

        //FL Illumination Map
        virtual auto GetFLIlluminationMap(int32_t channel) const->IlluminationMapIndex = 0;
        virtual auto GetFLIlluminationMapChannel(IlluminationMapIndex mapIndex) const->int32_t = 0;
        virtual auto GetFLIlluminationMapChannel(const QList<int32_t>& flChannels) const->std::tuple<int32_t, QList<int32_t>> = 0;
        virtual auto GetFLIlluminationMapChannel(int32_t flChannelIndex) const->std::tuple<int32_t, int32_t> = 0;   //<LEDChannel,LED>
        virtual auto GetFLIlluminationMapChannels() const->QList<int32_t> = 0;

        //HT Illumination Intensity Setup Patterns
        virtual auto GetHTIlluminationSetupPattern(double NA) const->int32_t = 0;
        virtual auto GetHTIlluminationSetupPatternNAs() const->QList<double> = 0;


        //Setup references
        virtual auto SetupReference(SetupRef item) const->double = 0;

        //Evaluation references
        virtual auto EvaluationReference(EvaluationRef item) const->double = 0;
        virtual auto EvaluationReference(EvaluationRef item, double NA) const->double = 0;

        //Preview parameters
        virtual auto PreviewParameter(PreviewParam item) const->QVariant = 0;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}