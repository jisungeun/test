﻿#pragma once
#include <memory>
#include <optional>

#include "HTXAppEntityExport.h"
#include "AppEntityDefines.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API SampleTypeRepo {
    protected:
        SampleTypeRepo();

    public:
        using Self = SampleTypeRepo;
        using Pointer = std::shared_ptr<Self>;
        using ModelName = QString;

        static auto GetInstance() -> Pointer;
        ~SampleTypeRepo();

        auto AddSampleType(const ModelName& modelName, const SampleTypeName& sampleTypeName) -> void;

        auto GetSampleTypeNamesByModel(const ModelName& modelName) const -> std::optional<QStringList>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
