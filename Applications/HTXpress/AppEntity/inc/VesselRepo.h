#pragma once
#include <memory>
#include <QList>

#include "Vessel.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API VesselRepo {
    public:
        typedef std::shared_ptr<VesselRepo> Pointer;

    private:
        VesselRepo();

    public:
        ~VesselRepo();

        static auto GetInstance()->Pointer;

        auto AddVessel(const Vessel::Pointer& holder)->void;
        auto SetVessels(const QList<Vessel::Pointer>& vessels)->void;
        auto GetVessels() const->QList<Vessel::Pointer>;
        auto GetVesselByName(const QString& name) const->Vessel::Pointer;
        auto GetVesselByModel(const QString& model) const->Vessel::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}