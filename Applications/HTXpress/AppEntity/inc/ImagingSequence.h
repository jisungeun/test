#pragma once
#include <memory>
#include <QSet>

#include "AppEntityDefines.h"
#include "ImagingCondition.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    class HTXAppEntity_API ImagingSequence {
    public:
        typedef std::shared_ptr<ImagingSequence> Pointer;

    public:
        ImagingSequence();
        ImagingSequence(const ImagingSequence& other);
        virtual ~ImagingSequence();

        auto operator=(const ImagingSequence& other)->ImagingSequence&;
        auto operator==(const ImagingSequence& other) const->bool;

        auto AddImagingCondition(const ImagingCondition::Pointer& condition)->void;
        auto SetImagingCondition(int32_t index, const ImagingCondition::Pointer& condition)->void;
        auto GetImagingCondition(int32_t index)->ImagingCondition::Pointer;
        auto GetImagingCondition()->QList<ImagingCondition::Pointer>;
        auto GetModalityCount() const->int32_t;
        auto RemoveImagingCondition(int32_t index)->void;

        //! Unit of interval - msec
        auto SetTimelapseCondition(int32_t interval, int32_t count)->void;
        auto GetInterval() const->int32_t;
        auto GetTimeCount() const->int32_t;

        auto ToStr() const->QString;

        auto GetImagingTypes()->QSet<ImagingType>;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}