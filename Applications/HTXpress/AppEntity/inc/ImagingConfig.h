#pragma once
#include <memory>

#include <enum.h>

#include "AppEntityDefines.h"
#include "ChannelConfig.h"
#include "ScanConfig.h"
#include "HTXAppEntityExport.h"

namespace HTXpress::AppEntity {
    using ImagingMode = AppEntity::ImagingMode;
    using ChannelConfig = AppEntity::ChannelConfig;

    class HTXAppEntity_API ImagingConfig {
    public:
        using Pointer = std::shared_ptr<ImagingConfig>;

    public:
        ImagingConfig();
        ImagingConfig(const ImagingConfig& other);
        ~ImagingConfig();

        auto operator=(ImagingConfig& other)->ImagingConfig&;

        auto SetChannelConfig(ImagingMode mode, const ChannelConfig& config)->void;
        auto GetChannelConfig(ImagingMode mode)->ChannelConfig::Pointer;
        auto GetChannelConfigAll()->QMap<ImagingMode,ChannelConfig::Pointer>;

        auto SetScanConfig(ImagingMode mode, const ScanConfig& config)->void;
        auto GetScanConfig(ImagingMode mode)->ScanConfig::Pointer;

        auto SetLiveConfig(ImagingMode mode, const ChannelConfig& config)->void;
        auto GetLiveConfig(ImagingMode mode)->ChannelConfig::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}