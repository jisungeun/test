#include <QMap>
#include "ExperimentResultRepo.h"

namespace HTXpress::AppEntity {
    struct ExperimentResultRepo::Impl {
        QMap<QString, QMap<QString, QList<ExperimentResult::Pointer>>> results;
    };

    ExperimentResultRepo::ExperimentResultRepo() : d{new Impl} {
    }

    ExperimentResultRepo::~ExperimentResultRepo() {
    }

    auto ExperimentResultRepo::GetInstance() -> ExperimentResultRepo::Pointer {
        static Pointer theInstance(new ExperimentResultRepo());
        return theInstance;
    }

    auto ExperimentResultRepo::AddResults(const QString& project, const QString& expPrefix,
        const QList<ExperimentResult::Pointer>& results) -> void {
        d->results[project][expPrefix].append(results);
    }

    auto ExperimentResultRepo::GetResults(const QString& project,
        const QString& expPrefix) const -> QList<ExperimentResult::Pointer> {
        return d->results[project][expPrefix];
    }

    auto ExperimentResultRepo::GetExperiments(const QString& project) const -> QStringList {
        return d->results[project].keys();
    }

    auto ExperimentResultRepo::GetProjects() const -> QStringList {
        return d->results.keys();
    }

    auto ExperimentResultRepo::ClearAll() -> void {
        return d->results.clear();
    }

    auto ExperimentResultRepo::ClearExperiment(const QString& project, const QString& expPrefix) -> void {
        return d->results[project][expPrefix].clear();
    }

    auto ExperimentResultRepo::ClearProject(const QString& project) -> void {
        return d->results[project].clear();
    }
}
