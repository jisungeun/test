#include <QMap>

#include "ImagingConfig.h"

namespace HTXpress::AppEntity {
    struct ImagingConfig::Impl {
        QMap<ImagingMode, ChannelConfig::Pointer> channelConfigs = {
            {ImagingMode::HT3D, std::make_shared<ChannelConfig>()},
            {ImagingMode::HT2D, std::make_shared<ChannelConfig>()},
            {ImagingMode::BFGray, std::make_shared<ChannelConfig>()},
            {ImagingMode::BFColor, std::make_shared<ChannelConfig>()},
            {ImagingMode::FLCH0, std::make_shared<ChannelConfig>()},
            {ImagingMode::FLCH1, std::make_shared<ChannelConfig>()},
            {ImagingMode::FLCH2, std::make_shared<ChannelConfig>()}
        };

        QMap<ImagingMode, ScanConfig::Pointer> scanConfigs = {
            {ImagingMode::HT3D, std::make_shared<ScanConfig>()},
            {ImagingMode::HT2D, std::make_shared<ScanConfig>()},
            {ImagingMode::BFGray, std::make_shared<ScanConfig>()},
            {ImagingMode::BFColor, std::make_shared<ScanConfig>()},
            {ImagingMode::FLCH0, std::make_shared<ScanConfig>()},
            {ImagingMode::FLCH1, std::make_shared<ScanConfig>()},
            {ImagingMode::FLCH2, std::make_shared<ScanConfig>()}
        };

        QMap<ImagingMode, ChannelConfig::Pointer> liveConfigs = {
            {ImagingMode::HT3D, std::make_shared<ChannelConfig>()},
            {ImagingMode::HT2D, std::make_shared<ChannelConfig>()},
            {ImagingMode::BFGray, std::make_shared<ChannelConfig>()},
            {ImagingMode::BFColor, std::make_shared<ChannelConfig>()},
            {ImagingMode::FLCH0, std::make_shared<ChannelConfig>()},
            {ImagingMode::FLCH1, std::make_shared<ChannelConfig>()},
            {ImagingMode::FLCH2, std::make_shared<ChannelConfig>()}
        };

        auto operator=(const Impl& other)->Impl& {
            channelConfigs.clear();
            scanConfigs.clear();
            liveConfigs.clear();

            for(auto key : other.channelConfigs.keys()) {
                channelConfigs[key] = std::make_shared<ChannelConfig>(*other.channelConfigs[key]);
            }

            for(auto key : other.scanConfigs.keys()) {
                scanConfigs[key] = std::make_shared<ScanConfig>(*other.scanConfigs[key]);
            }

            for(auto key : other.liveConfigs.keys()) {
                liveConfigs[key] = std::make_shared<ChannelConfig>(*other.liveConfigs[key]);
            }

            return *this;
        }
    };

    ImagingConfig::ImagingConfig() : d{new Impl} {
    }

    ImagingConfig::ImagingConfig(const ImagingConfig& other) {
        *d = *other.d;
    }

    ImagingConfig::~ImagingConfig() {
    }

    auto ImagingConfig::operator=(ImagingConfig& other) -> ImagingConfig& {
        *d = *other.d;
        return *this;
    }

    auto ImagingConfig::SetChannelConfig(ImagingMode mode, const ChannelConfig& config) -> void {
        d->channelConfigs[mode] = std::make_shared<ChannelConfig>(config);
    }

    auto ImagingConfig::GetChannelConfig(ImagingMode mode) -> ChannelConfig::Pointer {
        return d->channelConfigs[mode];
    }

    auto ImagingConfig::GetChannelConfigAll() -> QMap<ImagingMode,ChannelConfig::Pointer> {
        return d->channelConfigs;
    }

    auto ImagingConfig::SetScanConfig(ImagingMode mode, const ScanConfig& config) -> void {
        d->scanConfigs[mode] = std::make_shared<ScanConfig>(config);
        
    }

    auto ImagingConfig::GetScanConfig(ImagingMode mode) -> ScanConfig::Pointer {
        return d->scanConfigs[mode];
    }

    auto ImagingConfig::SetLiveConfig(ImagingMode mode, const ChannelConfig& config) -> void {
        d->liveConfigs[mode] = std::make_shared<ChannelConfig>(config);
    }

    auto ImagingConfig::GetLiveConfig(ImagingMode mode) -> ChannelConfig::Pointer {
        return d->liveConfigs[mode];
    }

}
