#include <QMap>
#include "ImagingCondition.h"

#include <QJsonArray>
#include <QJsonObject>

namespace HTXpress::AppEntity {
    namespace Key {
        constexpr char modality[] = "modality";
        constexpr char fov[] = "fov";
        constexpr char sliceStart[] = "sliceStart";
        constexpr char sliceCount[] = "sliceCount";
        constexpr char sliceStep[] = "sliceStep";
        constexpr char exposure[] = "exposure";
        constexpr char interval[] = "interval";
        constexpr char imagingChannels[] = "channels";
        constexpr char channelIndex[] = "channelIndex";
        constexpr char channelName[] = "name";
        constexpr char color[] = "color";
        constexpr char intensity[] = "intensity";
        constexpr char gain[] = "gain";
        constexpr char exFilterWaveLength[] = "exFilter.wavelength";
        constexpr char exFilterBandWidth[] = "exFilter.bandwidth";
        constexpr char emFilterWaveLength[] = "emFilter.wavelength";
        constexpr char emFilterBandWidth[] = "emFilter.bandwidth";
        constexpr char scanMode[] = "flScanMode";
        constexpr char scanRangeBottom[] = "flScanRangeBottom";
        constexpr char scanRangeTop[] = "flScanRangeTop";
        constexpr char scanRangeStep[] = "flScanRangeStep";
        constexpr char scanFocus[] = "flScanFocus";
        constexpr char scanFLOffset[] = "flScanFLOffset";
        constexpr char colorMode[] = "colorMode";
        constexpr char dimension[] = "dimension";
    };

    struct ImagingCondition::Impl {
        Modality modality{ Modality::HT };
        struct {
            int32_t start;
            int32_t count;
            int32_t step;
        } slice{0, 1, 0};
        int32_t dimension{3};

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other) const->bool;

        auto Clear()->void;
    };

    auto ImagingCondition::Impl::operator=(const Impl& other) -> Impl& {
        modality = other.modality;
        slice = other.slice;
        dimension = other.dimension;
        return *this;
    }

    auto ImagingCondition::Impl::operator==(const Impl& other) const -> bool {
        if(modality != other.modality) return false;
        if(slice.start != other.slice.start) return false;
        if(slice.count != other.slice.count) return false;
        if(slice.step != other.slice.step) return false;
        if(dimension != other.dimension) return false;
        return true;
    }

    auto ImagingCondition::Impl::Clear() -> void {
        slice = {0, 0, 0};
    }

    ImagingCondition::ImagingCondition(Modality modality) : d{new Impl} {
        d->modality = modality;
    }

    ImagingCondition::ImagingCondition(const ImagingCondition& other) : d{new Impl} {
        *this = other;
    }

    ImagingCondition::ImagingCondition(const QJsonObject& object) : d{new Impl} {
        d->modality = Modality::_from_string(object[Key::modality].toString().toLatin1());
        d->slice.start = object[Key::sliceStart].toInt();
        d->slice.count = object[Key::sliceCount].toInt();
        d->slice.step = object[Key::sliceStep].toInt();
        d->dimension = object[Key::dimension].toInt(3);
    }

    ImagingCondition::~ImagingCondition() {
    }

    auto ImagingCondition::operator=(const ImagingCondition& other) -> ImagingCondition& {
        *d = *other.d;
        return *this;
    }

    auto ImagingCondition::operator==(const ImagingCondition& other) const -> bool {
        return *d == *other.d;
    }

    auto ImagingCondition::Set(const ImagingCondition::Pointer other) -> void {
        *this = *other;
    }

    auto ImagingCondition::Clear() -> void {
        d->Clear();
    }

    auto ImagingCondition::GetValueMap() const -> QMap<QString, QVariant> {
        QMap<QString, QVariant> map;
        map["Modality"] = d->modality._to_string();
        map["SliceStart"] = d->slice.start;
        map["SliceCount"] = d->slice.count;
        map["SliceStep"] = d->slice.step;
        map["Dimension"] = d->dimension;
        return map;
    }

    auto ImagingCondition::SetValueMap(const QMap<QString, QVariant>& map) -> void {
        d->modality = AppEntity::Modality::_from_string(map["Modality"].toString().toLatin1());
        d->slice.start = map["SliceStart"].toInt();
        d->slice.count = map["SliceCount"].toInt();
        d->slice.step = map["SliceStep"].toInt();
        d->dimension = map["Dimension"].toInt();
    }

    auto ImagingCondition::ToJsonObject(QJsonObject object) const -> QJsonObject {
        object[Key::modality] = d->modality._to_string();
        object[Key::sliceStart] = d->slice.start;
        object[Key::sliceCount] = d->slice.count;
        object[Key::sliceStep] = d->slice.step;
        object[Key::dimension] = d->dimension;
        return object;
    }

    auto ImagingCondition::GetModality() const -> Modality {
        return d->modality;
    }

    auto ImagingCondition::CheckModality(Modality modality) const -> bool {
        return d->modality == modality;
    }

    auto ImagingCondition::SetDimension(int32_t dimension) -> void {
        d->dimension = dimension;
    }

    auto ImagingCondition::Is2D() const -> bool {
        return d->dimension < 3;

    }

    auto ImagingCondition::Is3D() const -> bool {
        return d->dimension > 2;
    }

    auto ImagingCondition::SetSliceStart(int32_t pos) -> void {
        d->slice.start = pos;
    }

    auto ImagingCondition::GetSliceStart() const -> int32_t {
        return d->slice.start;
    }

    auto ImagingCondition::SetSliceCount(int32_t count) -> void {
        d->slice.count = count;
    }

    auto ImagingCondition::GetSliceCount() const -> int32_t {
        return d->slice.count;
    }

    auto ImagingCondition::SetSliceStep(int32_t step) -> void {
        d->slice.step = step;
    }

    auto ImagingCondition::GetSliceStep() const -> int32_t {
        return d->slice.step;
    }

    struct ImagingConditionHT::Impl {
        int32_t exposure{ 0 };
        int32_t interval{ 0 };
        int32_t intensity{ 0 };

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other) const->bool;

        auto Clear()->void;
    };

    auto ImagingConditionHT::Impl::operator=(const Impl& other) -> Impl& {
        exposure = other.exposure;
        interval = other.interval;
        intensity = other.intensity;
        return *this;
    }

    auto ImagingConditionHT::Impl::operator==(const Impl& other) const -> bool {
        if(exposure != other.exposure) return false;
        if(interval != other.interval) return false;
        if(intensity != other.intensity) return false;
        return true;
    }

    auto ImagingConditionHT::Impl::Clear() -> void {
        exposure = 0;
        interval = 0;
        intensity = 0;
    }

    ImagingConditionHT::ImagingConditionHT() : ImagingCondition(), d{new Impl} {
    }

    ImagingConditionHT::ImagingConditionHT(const ImagingConditionHT& other) : ImagingCondition(other), d{new Impl} {
        *this = other;
    }

    ImagingConditionHT::ImagingConditionHT(const QJsonObject& object) : ImagingCondition(object), d{new Impl} {
        d->exposure = object[Key::exposure].toInt();
        d->interval = object[Key::interval].toInt();
        d->intensity = object[Key::intensity].toInt();
    }

    ImagingConditionHT::~ImagingConditionHT() {
    }

    auto ImagingConditionHT::operator=(const ImagingConditionHT& other) -> ImagingConditionHT& {
        ImagingCondition::operator=(other);
        *d = *other.d;
        return *this;
    }

    auto ImagingConditionHT::operator==(const ImagingConditionHT& other) const -> bool {
        if(!ImagingCondition::operator==(other)) return false;
        return *d == *other.d;
    }

    void ImagingConditionHT::Set(const ImagingCondition::Pointer other) {
        *this = *dynamic_cast<ImagingConditionHT*>(other.get());
    }

    auto ImagingConditionHT::Clone() -> ImagingCondition::Pointer {
        return std::make_shared<ImagingConditionHT>(*this);
    }

    void ImagingConditionHT::Clear() {
        d->Clear();
        ImagingCondition::Clear();
    }

    auto ImagingConditionHT::GetValueMap() const -> QMap<QString, QVariant> {
        auto map = ImagingCondition::GetValueMap();
        map["Exposure"] = d->exposure;
        map["Interval"] = d->interval;
        map["Intensity"] = d->intensity;
        return map;
    }

    void ImagingConditionHT::SetValueMap(const QMap<QString, QVariant>& map) {
        ImagingCondition::SetValueMap(map);
        d->exposure = map["Exposure"].toInt();
        d->interval = map["Interval"].toInt();
        d->intensity = map["Intensity"].toInt();
    }

    QJsonObject ImagingConditionHT::ToJsonObject(QJsonObject object) const {
        object[Key::exposure] = d->exposure;
        object[Key::interval] = d->interval;
        object[Key::intensity] = d->intensity;
        return ImagingCondition::ToJsonObject(object);
    }

    auto ImagingConditionHT::GetImageCount() const -> uint32_t {
        return 4 * GetSliceCount();
    }

    auto ImagingConditionHT::GetImagingType() const -> ImagingType {
        return Is3D() ? ImagingType::HT3D : ImagingType::HT2D;
    }

    auto ImagingConditionHT::SetExposure(int32_t exposure) -> void {
        d->exposure = exposure;
    }

    auto ImagingConditionHT::SetExposure(int32_t channel, int32_t exposure) -> void {
        if(channel != 2) return;
        d->exposure = exposure;
    }

    auto ImagingConditionHT::GetExposure() const -> int32_t {
        return d->exposure;
    }

    auto ImagingConditionHT::GetExposure(int32_t channel) const -> int32_t {
        if(channel != 2) return 0;
        return d->exposure;
    }

    auto ImagingConditionHT::SetInterval(int32_t interval) -> void {
        d->interval = interval;
    }

    auto ImagingConditionHT::SetInterval(int32_t channel, int32_t interval) -> void {
        if(channel != 2) return;
        d->interval = interval;
    }

    auto ImagingConditionHT::GetInterval() const -> int32_t {
        return d->interval;
    }

    auto ImagingConditionHT::GetInterval(int32_t channel) const -> int32_t {
        if(channel != 2) return 0;
        return d->interval;
    }

    auto ImagingConditionHT::SetIntensity(int32_t intensity) -> void {
        d->intensity = intensity;
    }

    auto ImagingConditionHT::SetIntensity(int32_t channel, int32_t intensity) -> void {
        if(channel != 2) return;
        d->intensity = intensity;
    }

    auto ImagingConditionHT::GetIntensity() const -> int32_t {
        return d->intensity;
    }

    auto ImagingConditionHT::GetIntensity(int32_t channel) const -> int32_t {
        if(channel != 2) return 0;
        return d->intensity;
    }

    struct ImagingConditionFL::Impl {
        struct Condition {
            int32_t exposure{ 0 };
            int32_t interval{ 0 };
            int32_t intensity{ 0 };
            double gain{ 0 };
            FLFilter exFilter;
            FLFilter emFilter;
            QString name;
            Color color;

            auto operator=(const Condition& other)->Condition& {
                exposure = other.exposure;
                interval = other.interval;
                intensity = other.intensity;
                gain = other.gain;
                exFilter = other.exFilter;
                emFilter = other.emFilter;
                name = other.name;
                color = other.color;
                return *this;
            }

            auto operator==(const Condition& other) const->bool {
                if(exposure != other.exposure) return false;
                if(interval != other.interval) return false;
                if(intensity != other.intensity) return false;
                if(gain != other.gain) return false;
                if(exFilter != other.exFilter) return false;
                if(emFilter != other.emFilter) return false;
                if(name != other.name) return false;
                if(color.red != other.color.red) return false;
                if(color.green != other.color.green) return false;
                if(color.blue != other.color.blue) return false;
                return true;
            }

            auto operator!=(const Condition& other) const->bool {
                return !(*this == other);
            }
        };

        struct Additional {
            FLScanMode mode{ FLScanMode::Default };
            int32_t bottom{ 0 };
            int32_t top{ 0 };
            int32_t step{ 0 };
            double htFocusInUm{ 0 };
            double flFocusOffsetInUm{ 0 };

            auto operator=(const Additional& other)->Additional& {
                mode = other.mode;
                bottom = other.bottom;
                top = other.top;
                step = other.step;
                htFocusInUm = other.htFocusInUm;
                flFocusOffsetInUm = other.flFocusOffsetInUm;
                return *this;
            }

            auto operator==(const Additional& other) const->bool {
                if(mode != other.mode) return false;
                if(bottom != other.bottom) return false;
                if(top != other.top) return false;
                if(step != other.step) return false;
                if(htFocusInUm != other.htFocusInUm) return false;
                if(flFocusOffsetInUm != other.flFocusOffsetInUm) return false;
                return true;
            }

            auto operator!=(const Additional& other) const->bool {
                return !(*this == other);
            }

        } additional;

        QMap<int32_t, Condition> channels;

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other) const->bool;

        auto Clear()->void;
    };

    auto ImagingConditionFL::Impl::operator=(const Impl& other) -> Impl& {
        channels = other.channels;
        additional = other.additional;
        return *this;
    }

    auto ImagingConditionFL::Impl::operator==(const Impl& other) const -> bool {
        if(channels != other.channels) return false;
        if(additional != other.additional) return false;
        return true;
    }

    auto ImagingConditionFL::Impl::Clear() -> void {
        for(auto& channel : channels) {
            channel = {0, 0, 0};
        }
    }

    ImagingConditionFL::ImagingConditionFL() : ImagingCondition(Modality::FL), d{new Impl} {
    }

    ImagingConditionFL::ImagingConditionFL(const ImagingConditionFL& other) : ImagingCondition(other), d{new Impl} {
        *this = other;
    }

    ImagingConditionFL::ImagingConditionFL(const QJsonObject& object) : ImagingCondition(object), d{new Impl} {
        auto channelArray = object[Key::imagingChannels].toArray();
        for(auto channelValue : channelArray) {
            auto channelObj = channelValue.toObject();

            Color color;
            if(channelObj.contains(Key::color) && channelObj[Key::color].isArray()) {
                auto colorObj = channelObj[Key::color].toArray();
                if(colorObj.size() == 3) {
                    color.red = colorObj[0].toInt();
                    color.green = colorObj[1].toInt();
                    color.blue = colorObj[2].toInt();
                }
            }

            SetChannel(channelObj[Key::channelIndex].toInt(),
                       channelObj[Key::exposure].toInt(),
                       channelObj[Key::interval].toInt(),
                       channelObj[Key::intensity].toInt(),
                       channelObj[Key::gain].toDouble(),
                       {channelObj[Key::exFilterWaveLength].toInt(), channelObj[Key::exFilterBandWidth].toInt() },
                       {channelObj[Key::emFilterWaveLength].toInt(), channelObj[Key::emFilterBandWidth].toInt() },
                       channelObj[Key::channelName].toString(),
                       color);
        }

        SetScanMode(AppEntity::FLScanMode::_from_string(object[Key::scanMode].toString().toLatin1()));
        SetScanRange(object[Key::scanRangeBottom].toDouble(),
                     object[Key::scanRangeTop].toDouble(),
                     object[Key::scanRangeStep].toDouble());
        SetScanFocus(object[Key::scanFocus].toDouble(),
                     object[Key::scanFLOffset].toDouble());
    }

    ImagingConditionFL::~ImagingConditionFL() {
    }

    auto ImagingConditionFL::operator=(const ImagingConditionFL& other) -> ImagingConditionFL& {
        ImagingCondition::operator=(other);
        *d = *other.d;
        return *this;
    }

    auto ImagingConditionFL::operator==(const ImagingConditionFL& other) const -> bool {
        if(!ImagingCondition::operator==(other)) return false;
        return *d == *other.d;
    }

    void ImagingConditionFL::Set(const ImagingCondition::Pointer other) {
        *this = *dynamic_cast<ImagingConditionFL*>(other.get());
    }

    auto ImagingConditionFL::Clone() -> ImagingCondition::Pointer{
        return std::make_shared<ImagingConditionFL>(*this);
    }

    auto ImagingConditionFL::Clear() -> void {
        d->Clear();
        ImagingCondition::Clear();
    }

    auto ImagingConditionFL::GetValueMap() const -> QMap<QString, QVariant> {
        auto map = ImagingCondition::GetValueMap();

        for(auto itr=d->channels.begin(); itr!=d->channels.end(); ++itr) {
            auto strChannel = QString("CH%1").arg(itr.key());
            map[QString("%1.Exposure").arg(strChannel)] = itr->exposure;
            map[QString("%1.Interval").arg(strChannel)] = itr->interval;
            map[QString("%1.Intensity").arg(strChannel)] = itr->intensity;
            map[QString("%1.Gain").arg(strChannel)] = itr->gain;
            map[QString("%1.ExFilterWaveLength").arg(strChannel)] = itr->exFilter.GetWaveLength();
            map[QString("%1.ExFilterBandWidth").arg(strChannel)] = itr->exFilter.GetBandwidth();
            map[QString("%1.EmFilterWaveLength").arg(strChannel)] = itr->emFilter.GetWaveLength();
            map[QString("%1.EmFilterBandWidth").arg(strChannel)] = itr->emFilter.GetBandwidth();
            map[QString("%1.Name").arg(strChannel)] = itr->name;
            map[QString("%1.Color").arg(strChannel)] = QString("%1,%2%3").arg(itr->color.red).arg(itr->color.green).arg(itr->color.blue);
        }

        map[QString("Additional.Mode")] = d->additional.mode._to_string();
        map[QString("Additional.Bottom")] = d->additional.bottom;
        map[QString("Additional.Top")] = d->additional.top;
        map[QString("Additional.Step")] = d->additional.step;
        map[QString("Additional.Focus")] = d->additional.htFocusInUm;
        map[QString("Additional.FLFocusOffset")] = d->additional.flFocusOffsetInUm;

        return map;
    }

    void ImagingConditionFL::SetValueMap(const QMap<QString, QVariant>& map) {
        ImagingCondition::SetValueMap(map);

        for(auto itr=map.begin(); itr!=map.end(); ++itr) {
            const auto key = itr.key();
            if(key.mid(0, 2) == "CH") {
                const auto chIdx = key.at(2).digitValue();
                const auto field = key.mid(4);

                if(field == "Exposure") d->channels[chIdx].exposure = itr.value().toInt();
                if(field == "Interval") d->channels[chIdx].interval = itr.value().toInt();
                if(field == "Intensity") d->channels[chIdx].intensity = itr.value().toInt();
                if(field == "Gain") d->channels[chIdx].gain = itr.value().toDouble();
                if(field == "ExFilterWaveLength") d->channels[chIdx].exFilter.SetWaveLength(itr.value().toInt());
                if(field == "ExFilterBandWidth") d->channels[chIdx].exFilter.SetBandwidth(itr.value().toInt());
                if(field == "EmFilterWaveLength") d->channels[chIdx].emFilter.SetWaveLength(itr.value().toInt());
                if(field == "EmFilterBandWidth") d->channels[chIdx].emFilter.SetBandwidth(itr.value().toInt());
                if(field == "Name") d->channels[chIdx].name = itr.value().toString();
                if(field == "Color") {
                    const auto str = itr.value().toString();
                    const auto toks = str.split(",");
                    if(toks.size()==3) {
                        d->channels[chIdx].color.red = toks.at(0).toInt();
                        d->channels[chIdx].color.green = toks.at(1).toInt();
                        d->channels[chIdx].color.blue = toks.at(2).toInt();
                    }
                }
            } else {
                const auto& field = key;

                if(field == "Additional.Mode") d->additional.mode = FLScanMode::_from_string(itr.value().toString().toStdString().c_str());
                if(field == "Additional.Bottom") d->additional.bottom = itr.value().toInt();
                if(field == "Additional.Top") d->additional.top = itr.value().toInt();
                if(field == "Additional.Step") d->additional.step = itr.value().toInt();
                if(field == "Additional.Focus") d->additional.htFocusInUm = itr.value().toDouble();
                if(field == "Additional.FLFocusOffset") d->additional.flFocusOffsetInUm = itr.value().toDouble();
            }
        }
    }

    QJsonObject ImagingConditionFL::ToJsonObject(QJsonObject object) const {
        QJsonArray jsonArray;

        for(auto itr=d->channels.begin(); itr!=d->channels.end(); ++itr) {
            QJsonObject channelObject;
            channelObject[Key::channelIndex] = itr.key();
            channelObject[Key::channelName] = itr->name;
            channelObject[Key::exposure] = itr->exposure;
            channelObject[Key::interval] = itr->interval;
            channelObject[Key::intensity] = itr->intensity;
            channelObject[Key::gain] = itr->gain;
            channelObject[Key::exFilterWaveLength] = itr->exFilter.GetWaveLength();
            channelObject[Key::exFilterBandWidth] = itr->exFilter.GetBandwidth();
            channelObject[Key::emFilterWaveLength] = itr->emFilter.GetWaveLength();
            channelObject[Key::emFilterBandWidth] = itr->emFilter.GetBandwidth();
            channelObject[Key::color] = QJsonArray({itr->color.red, itr->color.green, itr->color.blue});
            jsonArray << channelObject;
        }

        object[Key::scanMode] = d->additional.mode._to_string();
        object[Key::scanRangeBottom] = d->additional.bottom;
        object[Key::scanRangeTop] = d->additional.top;
        object[Key::scanRangeStep] = d->additional.step;
        object[Key::scanFocus] = d->additional.htFocusInUm;
        object[Key::scanFLOffset] = d->additional.flFocusOffsetInUm;

        object[Key::imagingChannels] = jsonArray;
        return ImagingCondition::ToJsonObject(object);
    }

    auto ImagingConditionFL::GetImageCount() const -> uint32_t {
        return d->channels.count() * GetSliceCount();
    }

    auto ImagingConditionFL::GetImagingType() const -> ImagingType {
        return Is3D() ? ImagingType::FL3D : ImagingType::FL2D;
    }

    auto ImagingConditionFL::SetChannel(int32_t channel, int32_t exposure, int32_t interval,
                                        int32_t intensity, double gain, FLFilter exFilter, 
                                        FLFilter emFilter, const QString& name, const Color color) -> void {
        d->channels[channel].exposure = exposure;
        d->channels[channel].interval = interval;
        d->channels[channel].intensity = intensity;
        d->channels[channel].gain = gain;
        d->channels[channel].exFilter = exFilter;
        d->channels[channel].emFilter = emFilter;
        d->channels[channel].name = name;
        d->channels[channel].color = color;
    }

    auto ImagingConditionFL::GetChannel(int32_t channel) const -> std::tuple<int32_t, int32_t, int32_t, double, FLFilter, FLFilter, QString, Color> {
        auto itr = d->channels.find(channel);
        if(itr == d->channels.end()) return std::tuple(-1, -1, -1, -1, FLFilter(), FLFilter(), "", Color());
        return std::make_tuple(itr->exposure, itr->interval, itr->intensity, itr->gain, itr->exFilter, itr->emFilter, itr->name, itr->color);
    }

    auto ImagingConditionFL::SetExposure(int32_t channel, int32_t exposure) -> void {
        d->channels[channel].exposure = exposure;
    }

    auto ImagingConditionFL::GetExposure(int32_t channel) const -> int32_t {
        auto itr = d->channels.find(channel);
        if(itr == d->channels.end()) return -1;
        return itr->exposure;
    }

    auto ImagingConditionFL::SetInterval(int32_t channel, int32_t interval) -> void {
        d->channels[channel].interval = interval;
    }

    auto ImagingConditionFL::GetInterval(int32_t channel) const -> int32_t {
        auto itr = d->channels.find(channel);
        if(itr == d->channels.end()) return -1;
        return itr->interval;
    }

    auto ImagingConditionFL::SetIntensity(int32_t channel, int32_t intensity) -> void {
        d->channels[channel].intensity = intensity;
    }

    auto ImagingConditionFL::GetIntensity(int32_t channel) const -> int32_t {
        auto itr = d->channels.find(channel);
        if(itr == d->channels.end()) return -1;
        return itr->intensity;
    }

    auto ImagingConditionFL::GetGain(int32_t channel) const -> double {
        auto itr = d->channels.find(channel);
        if(itr == d->channels.end()) return -1;
        return itr->gain;
    }

    auto ImagingConditionFL::GetExFilter(int32_t channel) const -> FLFilter {
        auto itr = d->channels.find(channel);
        if(itr == d->channels.end()) return FLFilter();
        return itr->exFilter;
    }

    auto ImagingConditionFL::GetEmFilter(int32_t channel) const -> FLFilter {
        auto itr = d->channels.find(channel);
        if(itr == d->channels.end()) return FLFilter();
        return itr->emFilter;
    }

    auto ImagingConditionFL::GetName(int32_t channel) const -> QString {
        auto itr = d->channels.find(channel);
        if(itr == d->channels.end()) return "";
        return itr->name;
    }

    auto ImagingConditionFL::GetColor(int32_t channel) const -> Color {
        auto itr = d->channels.find(channel);
        if(itr == d->channels.end()) return Color();
        return itr->color;
    }

    auto ImagingConditionFL::GetChannels() const -> QList<int32_t> {
        return d->channels.keys();
    }

    auto ImagingConditionFL::DeleteChannel(int32_t channel) const -> void {
        auto itr = d->channels.find(channel);
        if (itr == d->channels.end()) return;

        d->channels.erase(itr);
    }

    auto ImagingConditionFL::SetScanMode(FLScanMode mode) -> void {
        d->additional.mode = mode;
    }

    auto ImagingConditionFL::GetScanMode() const -> FLScanMode {
        return d->additional.mode;
    }

    auto ImagingConditionFL::SetScanRange(int32_t bottom, int32_t top, int32_t step) -> void {
        SetSliceStart(bottom);
        d->additional.bottom = bottom;
        d->additional.top = top;
        d->additional.step = step;
    }

    auto ImagingConditionFL::GetScanRangeBottom() const -> int32_t {
        return d->additional.bottom;
    }

    auto ImagingConditionFL::GetScanRangeTop() const -> int32_t {
        return d->additional.top;
    }

    auto ImagingConditionFL::GetScanRangeStep() const -> int32_t {
        return d->additional.step;
    }

    auto ImagingConditionFL::SetScanFocus(double htFocusInUm, double flFocusOffsetInUm) -> void {
        d->additional.htFocusInUm = htFocusInUm;
        d->additional.flFocusOffsetInUm = flFocusOffsetInUm;
    }

    auto ImagingConditionFL::GetScanFocusHT() const -> double {
        return d->additional.htFocusInUm;
    }

    auto ImagingConditionFL::GetScanFocusFLOffset() const -> double {
        return d->additional.flFocusOffsetInUm;
    }

    struct ImagingConditionBF::Impl {
        int32_t exposure{ 0 };
        int32_t interval{ 0 };
        int32_t intensity{ 0 };
        bool colorMode{ false };

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other) const->bool;

        auto Clear()->void;
    };

    auto ImagingConditionBF::Impl::operator=(const Impl& other) -> Impl& {
        exposure = other.exposure;
        interval = other.interval;
        intensity = other.intensity;
        colorMode = other.colorMode;
        return *this;
    }

    auto ImagingConditionBF::Impl::operator==(const Impl& other) const -> bool {
        if(exposure != other.exposure) return false;
        if(interval != other.interval) return false;
        if(intensity != other.intensity) return false;
        if(colorMode != other.colorMode) return false;
        return true;
    }

    auto ImagingConditionBF::Impl::Clear() -> void {
        exposure = 0;
        interval = 0;
        intensity = 0;
        colorMode = false;
    }

    ImagingConditionBF::ImagingConditionBF() : ImagingCondition(Modality::BF), d{new Impl} {
    }

    ImagingConditionBF::ImagingConditionBF(const ImagingConditionBF& other) : ImagingCondition(other), d{new Impl} {
        *this = other;
    }

    ImagingConditionBF::ImagingConditionBF(const QJsonObject& object) : ImagingCondition(object), d{new Impl} {
        d->exposure = object[Key::exposure].toInt();
        d->interval = object[Key::interval].toInt();
        d->intensity = object[Key::intensity].toInt();
        d->colorMode = object[Key::colorMode].toBool();
    }

    ImagingConditionBF::~ImagingConditionBF() {
    }

    auto ImagingConditionBF::operator=(const ImagingConditionBF& other) -> ImagingConditionBF& {
        ImagingCondition::operator=(other);
        *d = *other.d;
        return *this;
    }

    auto ImagingConditionBF::operator==(const ImagingConditionBF& other) const -> bool {
        if(!ImagingCondition::operator==(other)) return false;
        return *d==*other.d;
    }

    void ImagingConditionBF::Set(const ImagingCondition::Pointer other) {
        *this = *dynamic_cast<ImagingConditionBF*>(other.get());
    }

    auto ImagingConditionBF::Clone() -> ImagingCondition::Pointer {
        return std::make_shared<ImagingConditionBF>(*this);
    }

    void ImagingConditionBF::Clear() {
        d->Clear();
        ImagingCondition::Clear();
    }

    auto ImagingConditionBF::GetValueMap() const -> QMap<QString, QVariant> {
        auto map = ImagingCondition::GetValueMap();
        map["ColorMode"] = d->colorMode;
        map["Exposure"] = d->exposure;
        map["Interval"] = d->interval;
        map["Intensity"] = d->intensity;
        return map;
    }

    void ImagingConditionBF::SetValueMap(const QMap<QString, QVariant>& map) {
        ImagingCondition::SetValueMap(map);
        d->colorMode = map["ColorMode"].toBool();
        d->exposure = map["Exposure"].toInt();
        d->interval = map["Interval"].toInt();
        d->intensity = map["Intensity"].toInt();
    }

    QJsonObject ImagingConditionBF::ToJsonObject(QJsonObject object) const {
        object[Key::exposure] = d->exposure;
        object[Key::interval] = d->interval;
        object[Key::intensity] = d->intensity;
        object[Key::colorMode] = d->colorMode;
        return ImagingCondition::ToJsonObject(object);
    }

    auto ImagingConditionBF::GetImageCount() const -> uint32_t {
        return d->colorMode? 3 : 1;
    }

    auto ImagingConditionBF::GetImagingType() const -> ImagingType {
        return GetColorMode() ? ImagingType::BFColor : ImagingType::BFGray;
    }

    auto ImagingConditionBF::SetColorMode(bool mode) -> void {
        d->colorMode = mode;
    }

    auto ImagingConditionBF::GetColorMode() const -> bool {
        return d->colorMode;
    }

    auto ImagingConditionBF::SetExposure(int32_t exposure) -> void {
        d->exposure = exposure;
    }

    auto ImagingConditionBF::SetExposure(int32_t channel, int32_t exposure) -> void {
        if(channel != 2) return;
        d->exposure = exposure;
    }

    auto ImagingConditionBF::GetExposure() const -> int32_t {
        return d->exposure;
    }

    auto ImagingConditionBF::GetExposure(int32_t channel) const -> int32_t {
        if(channel != 2) return 0;
        return d->exposure;
    }

    auto ImagingConditionBF::SetInterval(int32_t interval) -> void {
        d->interval = interval;
    }

    auto ImagingConditionBF::SetInterval(int32_t channel, int32_t interval) -> void {
        if(channel != 2) return;
        d->interval = interval;
    }

    auto ImagingConditionBF::GetInterval() const -> int32_t {
        return d->interval;
    }

    auto ImagingConditionBF::GetInterval(int32_t channel) const -> int32_t {
        if(channel != 2) return 0;
        return d->interval;
    }

    auto ImagingConditionBF::SetIntensity(int32_t intensity) -> void {
        d->intensity = intensity;
    }

    auto ImagingConditionBF::SetIntensity(int32_t channel, int32_t intensity) -> void {
        if(channel != 2) return;
        d->intensity = intensity;
    }

    auto ImagingConditionBF::GetIntensity() const -> int32_t {
        return d->intensity;
    }

    auto ImagingConditionBF::GetIntensity(int32_t channel) const -> int32_t {
        if(channel != 2) return 0;
        return d->intensity;
    }
}
