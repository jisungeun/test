#include <QDebug>
#include <QSettings>
#include <QMap>

#include "SystemConfig.h"

namespace HTXpress::AppEntity {
    struct SystemConfig::Impl {
        bool simulation{ true };
        QString dataDir;
        QString model;
        Position systemCenter;
        uint32_t overlapInUm;
        QString serial;
        double autoFocusTimeInSec;
        int32_t minSpaceInGB;

        struct {
            int32_t min{ 0 };
            int32_t max{ 255 };
        } flOutputRange;

        struct {
            int32_t lightIntensity;
        } bf;

        struct {
            int32_t lightIntensity;
            double gainCoeffA;
            double gainCoeffB;
            double gainCoeffP;
            double gainCoeffQ;
        } preview;

        QMap<Axis, double> axisCompensation;

        QMap<int32_t, FLChannel> flChannels;

        struct {
            int32_t port;
            int32_t baudrate;
        } mcu;

        struct {
            QString serial;
        } imagingCamera, condenserCamera;

        ExcFilter::_enumerated excFilter;
        QMap<int32_t, FLFilter> flInternalExcitations;
        QMap<int32_t, FLFilter> flExternalExcitations;
        QMap<int32_t, FLFilter> flEmissions;
        QMap<double, int32_t> htIlluminationPatterns;

        QMap<AutoFocusParameter, int32_t> afParams;
        double afReadyPos{ 0 };

        struct HTScanParam {
            int32_t step;
            int32_t slices;
        };
        QMap<double, HTScanParam> htScanParams;

        struct CondenserAFParam {
            int32_t patternIndex;
            int32_t intensity;
            double zOffset;
        };
        QMap<double, CondenserAFParam> condenserAFParams;

        struct IlluminationCalParam {
            int32_t intensityStart;
            int32_t intensityStep;
            int32_t threshold;
        };
        QMap<double, IlluminationCalParam> illuminationCalParams;

        struct {
            bool overriden{ false };
            int32_t startPosPulse;
            int32_t intervalPulse;
            int32_t triggerSlices;
        } cafParameter;
    };

    SystemConfig::SystemConfig() : d{ new Impl } {
    }

    SystemConfig::SystemConfig(const SystemConfig& other) : d{ new Impl } {
        *d = *other.d;
    }

    SystemConfig::~SystemConfig() {
    }

    auto SystemConfig::operator=(const SystemConfig& other) -> SystemConfig& {
        *d = *other.d;
        return (*this);
    }

    auto SystemConfig::GetSimulation() const -> bool {
        return QSettings().value("Misc/Simulation").toBool();
    }

    auto SystemConfig::SetDataDir(const QString& dir) -> void {
        d->dataDir = dir;
    }

    auto SystemConfig::GetDataDir() const -> QString {
        return d->dataDir;
    }

    auto SystemConfig::SetModel(const QString& model) -> void {
        d->model = model;
    }

    auto SystemConfig::GetModel() const -> QString {
        return d->model;
    }

    auto SystemConfig::SetSerial(const QString& serial) -> void {
        d->serial = serial;
    }

    auto SystemConfig::GetSerial() const -> QString {
        return d->serial;
    }

    auto SystemConfig::SetMCUComPort(int32_t port, int32_t baudrate) -> void {
        d->mcu.port = port;
        d->mcu.baudrate = baudrate;
    }

    auto SystemConfig::GetMCUPort() const -> int32_t {
        return d->mcu.port;
    }

    auto SystemConfig::GetMCUBaudrate() const -> int32_t {
        return d->mcu.baudrate;
    }

    auto SystemConfig::SetImagingCameraSerial(const QString& serial) -> void {
        d->imagingCamera.serial = serial;
    }

    auto SystemConfig::GetImagingCameraSerial() const -> QString {
        return d->imagingCamera.serial;
    }

    auto SystemConfig::SetCondenserCameraSerial(const QString& serial) -> void {
        d->condenserCamera.serial = serial;
    }

    auto SystemConfig::GetCondenserCameraSerial() const -> QString {
        return d->condenserCamera.serial;
    }

    auto SystemConfig::SetSystemCenter(const Position& position) -> void {
        d->systemCenter = position;
    }

    auto SystemConfig::GetSystemCenter() const -> Position {
        return d->systemCenter;
    }

    auto SystemConfig::SetFlOutputRange(int32_t minVal, int32_t maxVal) -> void {
        d->flOutputRange.min = minVal;
        d->flOutputRange.max = maxVal;
    }

    auto SystemConfig::GetFlOutputRangeMin() const -> int32_t {
        return d->flOutputRange.min;
    }

    auto SystemConfig::GetFlOutputRangeMax() const -> int32_t {
        return d->flOutputRange.max;
    }

    auto SystemConfig::SetBFLightIntensity(int32_t value) -> void {
        d->bf.lightIntensity = value;
    }

    auto SystemConfig::GetBFLightIntensity() const -> int32_t {
        return d->bf.lightIntensity;
    }

    auto SystemConfig::SetPreviewLightIntensity(int32_t value) -> void {
        d->preview.lightIntensity = value;
    }

    auto SystemConfig::GetPreviewLightIntensity() const -> int32_t {
        return d->preview.lightIntensity;
    }

    auto SystemConfig::SetPreviewGainCoefficient(double coeffA, double coeffB, double coeffP, double coeffQ) -> void {
        d->preview.gainCoeffA = coeffA;
        d->preview.gainCoeffB = coeffB;
        d->preview.gainCoeffP = coeffP;
        d->preview.gainCoeffQ = coeffQ;
    }

    auto SystemConfig::GetPreviewGainCoefficient() const -> std::tuple<double, double, double, double> {
        return std::make_tuple(d->preview.gainCoeffA, d->preview.gainCoeffB, d->preview.gainCoeffP, d->preview.gainCoeffQ);
    }

    auto SystemConfig::SetAxisCompensation(Axis axis, double ratio) -> void {
        d->axisCompensation[axis] = ratio;
    }

    auto SystemConfig::GetAxisCompensation(Axis axis) const -> double {
        auto itr = d->axisCompensation.find(axis);
        if(itr == d->axisCompensation.end()) return 1.0;
        return itr.value();
    }

    auto SystemConfig::SetAutofocusParameter(AutoFocusParameter param, int32_t value) -> void {
        d->afParams[param] = value;
    }

    auto SystemConfig::GetAutofocusParameter(AutoFocusParameter param) const -> int32_t {
        return d->afParams[param];
    }

    auto SystemConfig::SetAutofocusReadyPos(double posMm) -> void {
        d->afReadyPos = posMm;
    }

    auto SystemConfig::GetAutofocusReadyPos() const -> double {
        return d->afReadyPos;
    }

    auto SystemConfig::SetTileScanOverlap(const uint32_t overlapUm) -> void {
        d->overlapInUm = overlapUm;
    }

    auto SystemConfig::GetTileScanOverlap() const -> uint32_t {
        return d->overlapInUm;
    }

    auto SystemConfig::SetAutoFocusTime(const double second) -> void {
        d->autoFocusTimeInSec = second;
    }

    auto SystemConfig::GetAutoFocusTime() const -> double {
        return d->autoFocusTimeInSec;
    }

    auto SystemConfig::SetFLChannel(int32_t channelIdx, const FLChannel& channel) -> void {
        d->flChannels[channelIdx] = channel;
    }

    auto SystemConfig::SetFLChannels(const QMap<int32_t, FLChannel>& channels) -> void {
        d->flChannels = channels;
    }

    auto SystemConfig::GetFLChannel(int32_t channelIdx, FLChannel& channel) const -> bool {
        if(!d->flChannels.count(channelIdx)) return false;
        channel = d->flChannels[channelIdx];
        return true;
    }

    auto SystemConfig::GetFLChannel(const QString& name, FLChannel& channel) const -> bool {
        for(auto item : d->flChannels) {
            if(item.GetName() == name) {
                channel = item;
                return true;
            }
        }
        return false;
    }

    auto SystemConfig::GetFLChannels() const -> QList<int32_t> {
        return d->flChannels.keys();
    }

    auto SystemConfig::RemoveFLChannel(int32_t channelIdx) -> void {
        d->flChannels.remove(channelIdx);
    }

    auto SystemConfig::SetExcitationFilter(ExcFilter filter) -> void {
        d->excFilter = filter;
    }

    auto SystemConfig::GetExcitationFilter() const -> ExcFilter {
        return d->excFilter;
    }

    auto SystemConfig::SetFLInternalExcitation(int32_t channel, const FLFilter& excitation) -> void {
        d->flInternalExcitations[channel] = excitation;
    }

    auto SystemConfig::SetFLInternalExcitations(const QMap<int32_t, FLFilter>& excitations) -> void {
        d->flInternalExcitations = excitations;
    }

    auto SystemConfig::GetFLInternalExcitation(int32_t channel, FLFilter& excitation) -> bool {
        if (!d->flInternalExcitations.count(channel)) return false;
        excitation = d->flInternalExcitations[channel];
        return true;
    }

    auto SystemConfig::GetFLInternalExcitations() const -> QList<int32_t> {
        return d->flInternalExcitations.keys();
    }

    auto SystemConfig::RemoveFLInternalExcitation(int32_t channel) -> void {
        d->flInternalExcitations.remove(channel);
    }

    auto SystemConfig::SetFLExternalExcitation(int32_t channel, const FLFilter& excitation) -> void {
        d->flExternalExcitations[channel] = excitation;
    }

    auto SystemConfig::SetFLExternalExcitations(const QMap<int32_t, FLFilter>& excitations) -> void {
        d->flExternalExcitations = excitations;
    }

    auto SystemConfig::GetFLExternalExcitation(int32_t channel, FLFilter& excitation) -> bool {
        if(!d->flExternalExcitations.count(channel)) return false;
        excitation = d->flExternalExcitations[channel];
        return true;
    }

    auto SystemConfig::GetFLExternalExcitations() const -> QList<int32_t> {
        return d->flExternalExcitations.keys();
    }

    auto SystemConfig::RemoveFLExternalExcitation(int32_t channel) -> void {
        d->flExternalExcitations.remove(channel);
    }

    auto SystemConfig::GetFLActiveExcitation(int32_t channel, FLFilter& filter) -> bool {
        if(d->excFilter == ExcFilter::Internal) return GetFLInternalExcitation(channel, filter);
        return GetFLExternalExcitation(channel, filter);
    }

    auto SystemConfig::GetFLActiveExcitations() const -> QList<int32_t> {
        if(d->excFilter == ExcFilter::Internal) return GetFLInternalExcitations();
        return GetFLExternalExcitations();
    }

    auto SystemConfig::SetFLEmission(int32_t channel, const FLFilter& emission) -> void {
        d->flEmissions[channel] = emission;
    }

    auto SystemConfig::SetFLEmissions(const QMap<int32_t, FLFilter>& emissions) -> void {
        d->flEmissions = emissions;
    }

    auto SystemConfig::GetFLEmission(int32_t channel, FLFilter& emission) -> bool {
        if(!d->flEmissions.count(channel)) return false;
        emission = d->flEmissions[channel];
        return true;
    }

    auto SystemConfig::GetFLEmissions() const -> QList<int32_t> {
        return d->flEmissions.keys();
    }

    auto SystemConfig::RemoveFLEmission(int32_t channel) -> void {
        d->flEmissions.remove(channel);
    }

    auto SystemConfig::SetHTIlluminationPattern(double NA, int32_t index) -> void {
        d->htIlluminationPatterns[NA] = index;
    }

    auto SystemConfig::GetHTIlluminationPattern(double NA) const -> int32_t {
        const auto itr = d->htIlluminationPatterns.find(NA);
        if(itr == d->htIlluminationPatterns.end()) return -1;
        return itr.value();
    }

    auto SystemConfig::GetHTIlluminationNAs() const -> QList<double> {
        return d->htIlluminationPatterns.keys();
    }

    auto SystemConfig::SetHTScanParameter(double NA, int32_t step, int32_t slices) -> void {
        d->htScanParams[NA] = {step, slices};
    }

    auto SystemConfig::GetHTScanParameter(double NA) const -> std::tuple<int32_t, int32_t> {
        auto itr = d->htScanParams.find(NA);
        if(itr == d->htScanParams.end()) return std::make_tuple(20, 34);
        return std::make_tuple(itr->step, itr->slices);
    }

    auto SystemConfig::GetHTScanParameterNAs() const -> QList<double> {
        return d->htScanParams.keys();
    }

    auto SystemConfig::SetCondenserAFParameter(double NA, 
                                               int32_t patternIndex, 
                                               int32_t intensity, 
                                               double zOffset) -> void {
        d->condenserAFParams[NA].patternIndex = patternIndex;
        d->condenserAFParams[NA].intensity = intensity;
        d->condenserAFParams[NA].zOffset = zOffset;
    }

    auto SystemConfig::GetCondenserAFParameter(double NA) const -> std::tuple<int32_t, int32_t, double> {
        auto itr = d->condenserAFParams.find(NA);
        if(itr == d->condenserAFParams.end()) return std::make_tuple(23, 50, 0);
        return std::make_tuple(itr->patternIndex, itr->intensity, itr->zOffset);
    }

    auto SystemConfig::GetCondenserAFParameterNAs() const -> QList<double> {
        return d->condenserAFParams.keys();
    }

    auto SystemConfig::SetIlluminationCalibrationParameter(double NA, 
                                                           int32_t intensityStart, 
                                                           int32_t intensityStep,
                                                           int32_t threshold) -> void {
        d->illuminationCalParams[NA].intensityStart = intensityStart;
        d->illuminationCalParams[NA].intensityStep = intensityStep;
        d->illuminationCalParams[NA].threshold = threshold;
    }

    auto SystemConfig::GetIlluminationCalibrationParameter(double NA) const -> std::tuple<int32_t, int32_t, int32_t> {
        auto itr = d->illuminationCalParams.find(NA);
        if(itr == d->illuminationCalParams.end()) return std::make_tuple(95, -10, 120);
        return std::make_tuple(itr->intensityStart, itr->intensityStep, itr->threshold);
    }

    auto SystemConfig::GetIlluminationCalibrationParameterNAs() const -> QList<double> {
        return d->illuminationCalParams.keys();
    }

    auto SystemConfig::SetMinRequiredSpace(int32_t spaceInGB) -> void {
        d->minSpaceInGB = spaceInGB;
    }

    auto SystemConfig::GetMinRequiredSpace() const -> int32_t {
        return d->minSpaceInGB;
    }

    auto SystemConfig::SetCondenserAFScanParameter(int32_t startPosPulse, int32_t intervalPulse, int32_t triggerSlices) -> void {
        d->cafParameter.overriden = true;
        d->cafParameter.startPosPulse = startPosPulse;
        d->cafParameter.intervalPulse = intervalPulse;
        d->cafParameter.triggerSlices = triggerSlices;
    }

    auto SystemConfig::IsCondenserAFScanParameterOverriden() const -> bool {
        return d->cafParameter.overriden;
    }

    auto SystemConfig::GetCondenserAFScanStartPosPulse() const -> int32_t {
        return d->cafParameter.startPosPulse;
    }

    auto SystemConfig::GetCondenserAFScanIntervalPulse() const -> int32_t {
        return d->cafParameter.intervalPulse;
    }

    auto SystemConfig::GetCondenserAFScanTriggerSlices() const -> int32_t {
        return d->cafParameter.triggerSlices;
    }

    auto SystemConfig::ClearCondenserAFScanParameter() -> void {
        d->cafParameter.overriden = false;
        d->cafParameter.startPosPulse = 0;
        d->cafParameter.intervalPulse = 0;
        d->cafParameter.triggerSlices = 0;
    }
}
