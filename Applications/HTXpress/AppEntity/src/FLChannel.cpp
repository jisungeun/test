#include <QColor>

#include "FLChannel.h"

namespace HTXpress::AppEntity {
    struct FLChannel::Impl {
        QString name;
        uint32_t excitation{ 0 };
        uint32_t excitationBandwidth{ 0 };
        uint32_t emission{ 0 };
        uint32_t emissionBandwidth{ 0 };
        QColor color{Qt::gray};
    };

    FLChannel::FLChannel() : d{new Impl} {
    }

    FLChannel::FLChannel(const FLChannel& other) :d {new Impl} {
        d->name = other.d->name;
        d->excitation = other.d->excitation;
        d->excitationBandwidth = other.d->excitationBandwidth;
        d->emission = other.d->emission;
        d->emissionBandwidth = other.d->emissionBandwidth;
        d->color = other.d->color;
    }

    FLChannel::~FLChannel() {
    }

    auto FLChannel::operator=(const FLChannel& other) -> FLChannel& {
        d->name = other.d->name;
        d->excitation = other.d->excitation;
        d->excitationBandwidth = other.d->excitationBandwidth;
        d->emission = other.d->emission;
        d->emissionBandwidth = other.d->emissionBandwidth;
        d->color = other.d->color;

        return *this;
    }

    auto FLChannel::operator==(const FLChannel& other) const -> bool {
        if(d->name != other.d->name) return false;
        if(d->excitation != other.d->excitation) return false;
        if(d->excitationBandwidth != other.d->excitationBandwidth) return false;
        if(d->emission != other.d->emission) return false;
        if(d->emissionBandwidth != other.d->emissionBandwidth) return false;
        if(d->color != other.d->color) return false;

        return true;
    }

    auto FLChannel::operator!=(const FLChannel& other) const -> bool {
        return !(*this == other);
    }

    auto FLChannel::SetName(const QString& name)->void {
        d->name = name;
    }
    
    auto FLChannel::GetName() const->QString {
        return d->name;
    }
    
    auto FLChannel::SetExcitation(uint32_t value)->void {
        d->excitation = value;
    }
    
    auto FLChannel::GetExcitation() const->uint32_t {
        return d->excitation;
    }

    auto FLChannel::SetExcitationBandwidth(uint32_t value) -> void {
        d->excitationBandwidth = value;
    }

    auto FLChannel::GetExcitationBandwidth() const -> uint32_t {
        return d->excitationBandwidth;
    }

    auto FLChannel::SetEmission(uint32_t value)->void {
        d->emission = value;
    }
    
    auto FLChannel::GetEmission() const->uint32_t {
        return d->emission;
    }

    auto FLChannel::SetEmissionBandwidth(uint32_t value) -> void {
        d->emissionBandwidth = value;
    }

    auto FLChannel::GetEmissionBandwidth() const -> uint32_t {
        return d->emissionBandwidth;
    }

    auto FLChannel::SetColor(const QColor& color) -> void {
        d->color = color;
    }

    auto FLChannel::GetColor() const -> QColor {
        return d->color;
    }
}
