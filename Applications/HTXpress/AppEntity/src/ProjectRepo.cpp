#include <QMap>

#include "ProjectRepo.h"

namespace HTXpress::AppEntity {
    struct ProjectRepo::Impl {
        QMap<QString, Project::Pointer> projects;

        auto findKey(const QString& title)->QString;
    };

    auto ProjectRepo::Impl::findKey(const QString& title) -> QString {
        const auto keys = projects.keys();
        for(auto key : keys) {
            if(key.compare(title, Qt::CaseInsensitive) == 0) return key;
        }
        return QString();
    }

    ProjectRepo::ProjectRepo() : d{new Impl} {
    }
    
    ProjectRepo::~ProjectRepo() {
    }

    auto ProjectRepo::GetInstance()->Pointer {
        static Pointer theInstance{ new ProjectRepo() };
        return theInstance;
    }

    auto ProjectRepo::AddProject(const Project::Pointer& project)->bool {
        const auto key = d->findKey(project->GetTitle());
        if(!key.isEmpty()) return false;
        d->projects[project->GetTitle()] = project;
        return true;
    }

    auto ProjectRepo::GetProject(const QString& projectName) const->Project::Pointer {
        const auto key = d->findKey(projectName);
        if (key.isEmpty()) {
            return nullptr;
        }

        return d->projects[key];
    }

    auto ProjectRepo::GetProjectTitles() const->QList<QString> {
        return  d->projects.keys();
    }

    auto ProjectRepo::RemoveProject(const QString& projectTitle) -> void {
        const auto key = d->findKey(projectTitle);
        if(!key.isEmpty()) {
            d->projects.remove(key);
        }
    }

    auto ProjectRepo::ClearAll()->void {
        d->projects.clear();
    }
}