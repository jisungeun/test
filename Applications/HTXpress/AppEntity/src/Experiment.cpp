#include <QList>
#include <QMap>
#include <QCryptographicHash>
#include <QDateTime>

#include "Experiment.h"

namespace HTXpress::AppEntity {
    struct ExpSetting {
        QMap<WellGroupIndex, WellGroup> wellGroups;
        QMap<WellIndex, QString> wellNames;
        QMap<WellIndex, QMap<LocationIndex, Location>> locations;
        QMap<WellIndex, WellGroupIndex> wellGroupIndexes;
        ImagingScenario::Pointer scenario;
        QMap<WellIndex, LocationIndex> lastLocationIndices;
        QMap<WellIndex, QMap<ROIIndex, ROI::Pointer>> roiList;

        auto operator=(const ExpSetting& other)->ExpSetting&;
        auto operator==(const ExpSetting& other) const->bool;
    };

    struct SingleSetting {
        bool enable{ false };
        ImagingCondition::Pointer condition;

        auto operator=(const SingleSetting& other)->SingleSetting&;
        auto operator==(const SingleSetting& other) const->bool;
    };

    struct Experiment::Impl {
        ExperimentID id;
        QString title;
        UserID userID;
        MediumData medium;
        QString createdDate;
        ExperimentProgress progress{ExperimentProgress::NotStarted};
        Vessel::Pointer vessel;
        QMap<VesselIndex, ExpSetting> settings;
        WellGroupIndex wellGroupIndex{ 0 };
        QMap<ImagingType, SingleSetting> singleImaging;
        SampleTypeName sampleTypeName{};
        struct {
            double xInUm;
            double yInUm;
        } fov;

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other) const->bool;

        auto GetNewLocationIndex(VesselIndex vesselIdx, WellIndex wellIdx)->LocationIndex;
        auto GetPositionName(const int32_t& row, const int32_t& col)->QString;
    };

    auto ExpSetting::operator=(const ExpSetting& other) -> ExpSetting& {
        wellGroups = other.wellGroups;
        wellNames = other.wellNames;
        wellGroupIndexes = other.wellGroupIndexes;
        locations = other.locations;
        lastLocationIndices = other.lastLocationIndices;
        if(other.scenario) {
            scenario = std::make_shared<AppEntity::ImagingScenario>(*other.scenario);
        }
        roiList = other.roiList;
        return *this;
    }

    auto ExpSetting::operator==(const ExpSetting& other) const -> bool {
        if(wellGroups != other.wellGroups) return false;
        if(wellNames != other.wellNames) return false;
        if(wellGroupIndexes != other.wellGroupIndexes) return false;
        if(locations != other.locations) return false;
        if(scenario) {
            if(other.scenario) {
                if(!(*scenario == *other.scenario)) return false;
            }
            else {
                return false;
            }
        }
        if(lastLocationIndices != other.lastLocationIndices) return false;
        if(roiList != other.roiList) return false;
        return true;
    }

    auto SingleSetting::operator=(const SingleSetting& other) -> SingleSetting& {
        enable = other.enable;
        condition = other.condition->Clone();
        return *this;
    }

    auto SingleSetting::operator==(const SingleSetting& other) const -> bool {
        if(enable != other.enable) return false;
        if(!(*condition == *other.condition)) return false;
        return true;
    }

    auto Experiment::Impl::operator=(const Impl& other) -> Impl& {
        id = other.id;
        title = other.title;
        userID = other.userID;
        medium = other.medium;
        createdDate = other.createdDate;
        progress = other.progress;
        vessel.reset(new Vessel(*other.vessel));
        sampleTypeName = other.sampleTypeName;
        for(auto itr=other.settings.begin(); itr != other.settings.end(); ++itr) {
            settings[itr.key()] = itr.value();
        }

        wellGroupIndex = other.wellGroupIndex;

        for(auto itr=other.singleImaging.begin(); itr != other.singleImaging.end(); ++itr) {
            singleImaging[itr.key()] = itr.value();
        }

        fov = other.fov;

        return *this;
    }

    auto Experiment::Impl::operator==(const Impl& other) const -> bool {
        if(id != other.id) return false;
        if(title != other.title) return false;
        if(userID != other.userID) return false;
        if(medium != other.medium) return false;
        if(createdDate != other.createdDate) return false;
        if(progress != other.progress) return false;
        if(!(*vessel == *other.vessel)) return false;
        if(settings != other.settings) return false;
        if(wellGroupIndex != other.wellGroupIndex) return false;
        if(singleImaging != other.singleImaging) return false;
        if(fov.xInUm != other.fov.xInUm) return false;
        if(fov.yInUm != other.fov.yInUm) return false;
        if(sampleTypeName != other.sampleTypeName) return false;

        return true;
    }

    auto Experiment::Impl::GetNewLocationIndex(VesselIndex vesselIdx, WellIndex wellIdx) -> LocationIndex {
        const auto newIndex = ++settings[vesselIdx].lastLocationIndices[wellIdx];
        return newIndex;
    }

    auto Experiment::Impl::GetPositionName(const int32_t& row, const int32_t& col) -> QString {
        auto ConvertNumberIndexToTextIndex = [](int32_t numberIndex) -> QString {
            QString text;

            while (numberIndex > 0) {
                const int32_t remainder = numberIndex % 26;
                if (remainder == 0) {
                    text.append('Z');
                    numberIndex = (numberIndex / 26) - 1;
                }
                else {
                    text.append((remainder-1)+'A');
                    numberIndex /= 26;
                }
            }
            
            std::reverse(text.begin(), text.end());
            return text;
        };

        return ConvertNumberIndexToTextIndex(row+1) + QString::number(col+1);
    }

    Experiment::Experiment() : d{new Impl} {
    }

    Experiment::Experiment(const Experiment& other) : d{new Impl} {
        *d = *other.d;
    }

    Experiment::~Experiment() {
    }

    auto Experiment::operator=(const Experiment& other) -> Experiment& {
        *d = *other.d;
        return *this;
    }

    auto Experiment::operator==(const Experiment& other) const -> bool {
        return *d == *other.d;
    }

    auto Experiment::SetID(ExperimentID id) -> void {
        d->id = id;
    }

    auto Experiment::GetID() const -> ExperimentID {
        return d->id;
    }

    auto Experiment::SetTitle(const QString& title) -> void {
        d->title = title;
    }

    auto Experiment::GetTitle() const -> QString {
        return d->title;
    }

    auto Experiment::SetUserID(UserID id) -> void {
        d->userID = id;
    }

    auto Experiment::GetUserID() const -> UserID {
        return d->userID;
    }

    auto Experiment::SetMedium(const MediumData medium) -> void {
        d->medium = medium;
    }

    auto Experiment::GetMedium() const -> MediumData{
        return d->medium;
    }

    auto Experiment::SetCreatedDate(const QString& date) -> void {
        d->createdDate = date;
    }

    auto Experiment::GetCreatedDate() const -> QString {
        return d->createdDate;
    }

    auto Experiment::SetProgress(ExperimentProgress progress) -> void {
        d->progress = progress;
    }

    auto Experiment::GetProgress() const -> ExperimentProgress {
        return d->progress;
    }

    auto Experiment::SetVesselInfo(Vessel::Pointer vessel) -> void {
        d->vessel.reset(new Vessel(*vessel));

        auto wellIndices = d->vessel->GetWellIndices();

        for(int32_t vesselIdx=0; vesselIdx < d->settings.size(); vesselIdx++) {
            d->settings[vesselIdx].wellNames.clear();
            for(auto wellIdx : wellIndices)
                d->settings[vesselIdx].wellNames[wellIdx] = d->vessel->GetWellLabel(wellIdx);
        }
    }

    auto Experiment::GetVessel() const -> Vessel::Pointer {
        return d->vessel;
    }

    auto Experiment::SetFOV(const AppEntity::Area& fov) -> void {
        d->fov.xInUm = fov.toUM().width;
        d->fov.yInUm = fov.toUM().height;
    }

    auto Experiment::GetFOV() const -> AppEntity::Area {
        return AppEntity::Area::fromUM(d->fov.xInUm, d->fov.yInUm);
    }

    auto Experiment::SetWellName(VesselIndex vesselIdx, WellIndex wellIdx, const QString& name) -> void {
        d->settings[vesselIdx].wellNames[wellIdx] = name;
    }

    auto Experiment::GetWellName(VesselIndex vesselIdx, WellIndex wellIdx) const -> QString {
        auto& wellName = d->settings[vesselIdx].wellNames[wellIdx];
        if(wellName.isEmpty()) {
            wellName = d->vessel->GetWellLabel(wellIdx);
        }
        return wellName;
    }

    auto Experiment::GetWellNames(VesselIndex vesselIdx) const -> QMap<WellIndex, QString> {
        if(d->settings[vesselIdx].wellNames.isEmpty()) {
            auto wellIndices = d->vessel->GetWellIndices();
            for(auto wellIdx : wellIndices) {
                const auto label = d->vessel->GetWellLabel(wellIdx);
                d->settings[vesselIdx].wellNames[wellIdx] = label;
            }
        }
        return d->settings[vesselIdx].wellNames;
    }

    auto Experiment::GetWellPositionNames(VesselIndex vesselIdx) const -> QMap<WellIndex, QString> {
        Q_UNUSED(vesselIdx)
        QMap<WellIndex, QString> positionNames;
        auto wellIndices = d->vessel->GetWellIndices();
        for(const auto& wellIdx : wellIndices) {
            const auto posName = d->GetPositionName(d->vessel->GetWellRowIndex(wellIdx),
                                                    d->vessel->GetWellColIndex(wellIdx));
            positionNames[wellIdx] = posName;
        }
        return positionNames;
    }

    auto Experiment::SetWellGroup(VesselIndex vesselIdx, WellGroupIndex groupIdx, const WellGroup& group) -> void {
        d->settings[vesselIdx].wellGroups.insert(groupIdx, group);
        for(auto index : group.GetWells()) {
            auto wellIdx = d->vessel->GetWellIndex(index.rowIdx, index.colIdx);
            d->settings[vesselIdx].wellGroupIndexes[wellIdx] = groupIdx;
        }
    }

    auto Experiment::GetWellGroupCount(VesselIndex vesselIdx) const -> int32_t {
        return d->settings[vesselIdx].wellGroups.size();
    }

    auto Experiment::GetWellGroup(VesselIndex vesselIdx, WellGroupIndex groupIdx) const -> WellGroup {
        if (!d->settings[vesselIdx].wellGroups.contains(groupIdx)) return WellGroup();
        return d->settings[vesselIdx].wellGroups[groupIdx];
    }

    auto Experiment::GetWellGroupIndices(VesselIndex vesselIdx) const -> QList<WellGroupIndex> {
        return d->settings[vesselIdx].wellGroups.keys();
    }

    auto Experiment::SetWellGroupName(VesselIndex vesselIdx, WellGroupIndex groupIdx, const QString& groupName) -> void {
        if (!d->settings[vesselIdx].wellGroups.contains(groupIdx)) return;
        d->settings[vesselIdx].wellGroups[groupIdx].SetTitle(groupName);
    }

    auto Experiment::GetWellGroupName(VesselIndex vesselIdx, WellIndex wellIdx) const -> QString {
        if (!d->settings[vesselIdx].wellGroupIndexes.contains(wellIdx)) {
            return QString();
        }

        const auto groupIdx = d->settings[vesselIdx].wellGroupIndexes[wellIdx];
        if (!d->settings[vesselIdx].wellGroups.contains(groupIdx)) {
            return QString();
        }

        const auto wellGroup = d->settings[vesselIdx].wellGroups[groupIdx];
        return wellGroup.GetTitle();
    }

    auto Experiment::GetWellGroupNames(VesselIndex vesselIdx) const -> QMap<WellIndex, QString> {
        QMap<WellIndex, QString> names;
        for(auto wellIdx : d->vessel->GetWellIndices()) {
            auto name = GetWellGroupName(vesselIdx, wellIdx);
            if (name.isEmpty()) continue;
            names.insert(wellIdx, name);
        }
        return names;
    }

    auto Experiment::DeleteWellGroup(VesselIndex vesselIdx, WellGroupIndex groupIdx) -> void {
        auto& groupMap = d->settings[vesselIdx].wellGroups;
        for (auto it = groupMap.begin(); it != groupMap.end(); ++it) {
            if (it.key() == groupIdx) {
                groupMap.erase(it);
                break;
            }
        }
    }

    auto Experiment::ClearWellGroups(VesselIndex vesselIdx) -> void {
        d->settings[vesselIdx].wellGroups.clear();
    }

    auto Experiment::GetNewWellGroupIndex()->WellGroupIndex {
        return d->wellGroupIndex++;
    }

    auto Experiment::AddWellsToGroup(VesselIndex vesselIdx, WellGroupIndex groupIdx, const QList<QPair<int, int>>& indices) -> void {
        QList<WellGroup::Index> addIndices;
        std::for_each(indices.begin(), indices.end(), [&addIndices](const QPair<int, int>& index) {
            addIndices << WellGroup::Index(index.first, index.second);
        });

        d->settings[vesselIdx].wellGroups[groupIdx].AddWells(addIndices);
    }

    auto Experiment::RemoveWells(VesselIndex vesselIdx, const QList<QPair<int, int>>& indices) -> void {
        QList<WellGroup::Index> removeIndices;
        
        std::for_each(indices.begin(), indices.end(), [&removeIndices](const QPair<int, int>& index) {
            removeIndices << WellGroup::Index(index.first, index.second);
        });

        for (auto& group : d->settings[vesselIdx].wellGroups) {
            group.RemoveWells(removeIndices);
        }
    }

    auto Experiment::SetVesselCount(int32_t count) -> void {
        if (d->settings.size() < count) {
            auto vesselIndex = d->settings.isEmpty() ? 0 : d->settings.lastKey() + 1;
            while (d->settings.size() < count) {
                d->settings.insert(vesselIndex, ExpSetting());

                auto wellIndices = d->vessel->GetWellIndices();
                for(auto wellIdx : wellIndices) {
                    const auto label = d->vessel->GetWellLabel(wellIdx);
                    d->settings[vesselIndex].wellNames[wellIdx] = label;
                }

                vesselIndex++;
            }
        } else if (d->settings.size() > count) {
            while (d->settings.size() != count) {
                d->settings.remove(d->settings.lastKey());
            }
        }
    }

    auto Experiment::GetVesselCount() const -> int32_t {
        return d->settings.size();
    }

    auto Experiment::SetLastLocationIndex(VesselIndex vesselIndex, WellIndex wellIndex, LocationIndex lastLocationIndex) -> void {
        d->settings[vesselIndex].lastLocationIndices[wellIndex] = lastLocationIndex;
    }
    
    auto Experiment::GetLastLocationIndex(VesselIndex vesselIndex, WellIndex wellIndex) const -> VesselIndex {
        return d->settings[vesselIndex].lastLocationIndices[wellIndex];
    }

    auto Experiment::AddLocation(VesselIndex vesselIdx, WellIndex wellIdx, Location location) -> LocationIndex {
        const auto locIndex = d->GetNewLocationIndex(vesselIdx, wellIdx);
        d->settings[vesselIdx].locations[wellIdx][locIndex] = location;
        return locIndex;
    }

    auto Experiment::AddLocation(VesselIndex vesselIdx, WellIndex wellIdx, LocationIndex locIndex, Location location) -> void {
        d->settings[vesselIdx].locations[wellIdx][locIndex] = location;
    }

    auto Experiment::GetLocationCount(VesselIndex vesselIdx, WellIndex wellIdx) const -> int32_t {
        return d->settings[vesselIdx].locations[wellIdx].size();
    }

    auto Experiment::GetLocation(VesselIndex vesselIdx, WellIndex wellIdx, LocationIndex index) const -> Location& {
        return d->settings[vesselIdx].locations[wellIdx][index];
    }

    auto Experiment::GetAllLocations(VesselIndex vesselIndex) const -> QMap<WellIndex, QMap<LocationIndex, Location>>& {
        return d->settings[vesselIndex].locations;
    }

    auto Experiment::GetAllLocationsInVessel(VesselIndex vesselIndex) const -> QList<Location> {
        QList<Location> allLocations;

        const auto& locations = GetAllLocations(vesselIndex);
        auto iter = locations.begin();
        for(; iter!=locations.end(); ++iter) {
            const auto wellIdx = iter.key();
            const auto& locs = iter.value();

            const auto wellCenter = d->vessel->GetWellPosition(wellIdx);

            auto locIter = locs.begin();
            for(; locIter != locs.end(); locIter++) {
                const auto& location = locIter.value();
                const auto center = location.GetCenter() + wellCenter;

                Location locInVessel(location);
                locInVessel.SetCenter(center);
                allLocations.push_back(locInVessel);
            }
        }

        return allLocations;
    }

    auto Experiment::DeleteLocation(VesselIndex vesselIdx, WellIndex wellIdx, LocationIndex index) -> bool {
        if (!d->settings.contains(vesselIdx)) {
            return false;
        }
        if (!d->settings[vesselIdx].locations.contains(wellIdx)) {
            return false;
        }
        if (!d->settings[vesselIdx].locations[wellIdx].contains(index)) {
            return false;
        }

        d->settings[vesselIdx].locations[wellIdx].remove(index);

        if (d->settings[vesselIdx].locations[wellIdx].isEmpty()) {
            d->settings[vesselIdx].locations.remove(wellIdx);
        }

        return true;
    }

    auto Experiment::ClearLocation(VesselIndex vesselIdx) -> void {
        d->settings[vesselIdx].locations.clear();
    }

    auto Experiment::SetScenario(VesselIndex vesselIdx, ImagingScenario::Pointer scenario) -> void {
        d->settings[vesselIdx].scenario = scenario;
    }

    auto Experiment::GetScenario(VesselIndex vesselIdx) const -> ImagingScenario::Pointer {
        return d->settings[vesselIdx].scenario;
    }

    auto Experiment::SetSingleImagingCondition(ImagingType type, ImagingCondition::Pointer condition) -> void {
        d->singleImaging[type].condition = condition;
    }

    auto Experiment::GetSingleImagingCondition(ImagingType type) const -> ImagingCondition::Pointer {
        if(!d->singleImaging.contains(type)) return nullptr;
        return d->singleImaging[type].condition;
    }

    auto Experiment::SetEnableSingleImaging(ImagingType type, bool enable) -> void {
        d->singleImaging[type].enable = enable;
    }

    auto Experiment::GetEnableSingleImaging(ImagingType type) const -> bool {
        if(!d->singleImaging.contains(type)) return false;
        return d->singleImaging[type].enable;
    }

    auto Experiment::GetEnabledImagingTypes() const -> QList<ImagingType> {
        QList<ImagingType> enabledTypes;
        for(auto type : ImagingType::_values()) {
            if(!d->singleImaging.contains(type)) continue;
            if(d->singleImaging[type].enable) enabledTypes.push_back(type);
        }
        return enabledTypes;
    }

    auto Experiment::AddROI(VesselIndex vesselIdx, WellIndex wellIdx, const ROI::Pointer& roi) -> void {
        auto idx = 1;

        if(!d->settings[vesselIdx].roiList.isEmpty()) {
            if(!d->settings[vesselIdx].roiList[wellIdx].isEmpty()) {
                if(!d->settings[vesselIdx].roiList[wellIdx].keys().isEmpty()) {
                    const auto& list = d->settings[vesselIdx].roiList[wellIdx].keys();
                    idx = *std::max_element(list.cbegin(), list.cend()) + 1;
                }
            }
        }

        AddROI(vesselIdx, wellIdx, idx, roi);
    }

    auto Experiment::AddROI(VesselIndex vesselIdx, WellIndex wellIdx, ROIIndex roiIdx, const ROI::Pointer& roi) -> void {
        d->settings[vesselIdx].roiList[wellIdx][roiIdx] = roi;
    }

    auto Experiment::SetROIs(VesselIndex vesselIdx, QMap<WellIndex, QMap<ROIIndex, ROI::Pointer>> rois) -> void {
        d->settings[vesselIdx].roiList = rois;
    }

    auto Experiment::DeleteROI(VesselIndex vesselIdx, WellIndex wellIdx, ROIIndex roiIdx) -> bool {
        if(!d->settings[vesselIdx].roiList[wellIdx].contains(roiIdx)) {
            return false;
        }

        d->settings[vesselIdx].roiList[wellIdx].remove(roiIdx);

        return true;
    }

    auto Experiment::GetROIs(VesselIndex vesselIdx) const -> QMap<WellIndex, QMap<ROIIndex, ROI::Pointer>>& {
         return d->settings[vesselIdx].roiList;
     }

    auto Experiment::GetROIs(VesselIndex vesselIdx, WellIndex wellIdx) const ->  QMap<ROIIndex, ROI::Pointer> {
        if(!d->settings[vesselIdx].roiList.contains(wellIdx)) {
            return QMap<ROIIndex, ROI::Pointer>();
        }

        return d->settings[vesselIdx].roiList[wellIdx];
    }

    auto Experiment::SetSampleTypeName(const SampleTypeName& sampleTypeName) -> void {
        d->sampleTypeName = sampleTypeName;
    }

    auto Experiment::GetSampleTypeName() const -> SampleTypeName {
        return d->sampleTypeName;
    }

    auto Experiment::GenerateID(const QString& userId, const QString &title) -> QString {
        QCryptographicHash cryptoHash(QCryptographicHash::Sha1);
        cryptoHash.addData(userId.toUtf8());
        cryptoHash.addData(title.toUtf8());
        cryptoHash.addData(QDateTime::currentDateTimeUtc().toString("yyyyMMddhhmmss").toUtf8());

        return QString(cryptoHash.result().toHex());
    }
}
