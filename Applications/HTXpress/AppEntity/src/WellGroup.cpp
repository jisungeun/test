#include "WellGroup.h"

namespace HTXpress::AppEntity {
    struct WellGroup::Impl {
        QString title;
        Color color{0, 0, 0, 0};
        QList<Index> wells;

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other) const->bool;
    };

    auto WellGroup::Impl::operator=(const Impl& other) -> Impl& {
        title = other.title;
        color = other.color;
        wells = other.wells;

        return *this;
    }

    auto WellGroup::Impl::operator==(const Impl& other) const -> bool {
        if(title != other.title) return false;
        if(!(color == other.color)) return false;
        if(wells != other.wells) return false;

        return true;
    }

    WellGroup::WellGroup() : d{new Impl} {
    }


    WellGroup::WellGroup(const WellGroup& other) :d {new Impl} {
        *d = *other.d;
    }

    WellGroup::~WellGroup() {
    }

    auto WellGroup::operator=(const WellGroup& other) -> WellGroup& {
        *d = *other.d;
        return *this;
    }

    auto WellGroup::operator==(const WellGroup& other) const -> bool {
        return *d == *other.d;
    }

    auto WellGroup::SetTitle(const QString& title) -> void {
        d->title = title;
    }

    auto WellGroup::GetTitle() const -> QString {
        return d->title;
    }

    auto WellGroup::SetColor(const Color& color) -> void {
        d->color = color;
    }

    auto WellGroup::GetColor() const -> Color {
        return d->color;
    }

    auto WellGroup::AddWell(int32_t rowIdx, int32_t colIdx) -> void {
        d->wells.push_back(Index(rowIdx, colIdx));
    }

    auto WellGroup::AddWell(const Index& index) -> void {
        d->wells.push_back(index);
    }

    auto WellGroup::AddWells(const QList<Index>& indices) -> void {
        d->wells << indices;
    }

    auto WellGroup::RemoveWells(const QList<Index>& indices) -> void {
        for (auto& index : indices) {
            if (d->wells.contains(index)) {
                d->wells.removeOne(index);
            }
        }
    }

    auto WellGroup::GetWells() const -> QList<Index> {
        return d->wells;
    }

    auto WellGroup::ContainsWell(int32_t rowIdx, int32_t colIdx) -> bool {
        return ContainsWell(Index(rowIdx, colIdx));
    }

    auto WellGroup::ContainsWell(const Index& index) -> bool {
        return d->wells.contains(index);
    }
}

