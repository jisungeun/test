#include "ChannelConfig.h"

namespace HTXpress::AppEntity {
    struct ChannelConfig::Impl {
        bool enable{ false };

        struct {
            uint32_t fovX{1024};
            uint32_t fovY{1024};
            uint32_t exposure{ 0 };
            uint32_t interval{ 0 };
            bool internal{ true };
        } camera;

        struct {
            int32_t index{ -1 };        //! excitation channel
            uint32_t intensity{ 0 };
            bool internal{ true };
        } light;

        struct {
            QString name;               //! FL channel name
            int32_t index{ -1 };        //! emission channel
        } filter;

        auto equal(const double lhs, const double rhs, double range) const->bool {
            if(lhs < (rhs - range)) return false;
            if(lhs > (rhs + range)) return false;
            return true;
        }

        auto operator==(const Impl& other) const->bool {
            if(enable != other.enable) return false;
            if(camera.fovX != other.camera.fovX) return false;
            if(camera.fovY != other.camera.fovY) return false;
            if(!equal(camera.exposure, other.camera.exposure, 0.0001)) return false;
            if(camera.interval != other.camera.interval) return false;
            if(camera.internal != other.camera.internal) return false;

            if(light.index != other.light.index) return false;
            if(light.intensity != other.light.intensity) return false;
            if(light.internal != other.light.internal) return false;

            if(filter.name != other.filter.name) return false;
            if(filter.index != other.filter.index) return false;

            return true;
        }
    };

    ChannelConfig::ChannelConfig() : d{new Impl} {
    }

    ChannelConfig::ChannelConfig(const ChannelConfig& other) : d{new Impl} {
        *d = *other.d;
    }

    ChannelConfig::~ChannelConfig() {
    }

    auto ChannelConfig::operator=(const ChannelConfig& other) -> ChannelConfig& {
        *d = *other.d;
        return *this;
    }

    auto ChannelConfig::operator==(const ChannelConfig& other) const -> bool {
        return *d == *other.d;
    }

    auto ChannelConfig::operator!=(const ChannelConfig& other) const -> bool {
        return !(*d == *other.d);
    }

    auto ChannelConfig::SetEnable(bool bEnable) -> void {
        d->enable = bEnable;
    }

    auto ChannelConfig::GetEnable() const -> bool {
        return d->enable;
    }

    auto ChannelConfig::SetCameraFov(uint32_t xPixels, uint32_t yPixels) -> void {
        d->camera.fovX = xPixels;
        d->camera.fovY = yPixels;
    }

    auto ChannelConfig::GetCameraFovX() const -> uint32_t {
        return d->camera.fovX;
    }

    auto ChannelConfig::GetCameraFovY() const -> uint32_t {
        return d->camera.fovY;
    }

    auto ChannelConfig::SetCameraExposureUSec(uint32_t value) -> void {
        d->camera.exposure = value;
    }

    auto ChannelConfig::GetCameraExposureUSec() const -> uint32_t {
        return d->camera.exposure;
    }

    auto ChannelConfig::SetCameraIntervalUSec(uint32_t interval) -> void {
        d->camera.interval = interval;
    }

    auto ChannelConfig::GetCameraIntervalUSec() const -> uint32_t {
        return d->camera.interval;
    }

    auto ChannelConfig::SetCameraInternal(bool internal) -> void {
        d->camera.internal = internal;
    }

    auto ChannelConfig::GetCameraInternal() const -> bool {
        return d->camera.internal;
    }

    auto ChannelConfig::SetLightChannel(int32_t index) -> void {
        d->light.index = index;
    }

    auto ChannelConfig::GetLightChannel() const -> int32_t {
        return d->light.index;
    }

    auto ChannelConfig::SetLightIntensity(uint32_t value) -> void {
        d->light.intensity = value;
    }

    auto ChannelConfig::GetLightIntensity() const -> uint32_t {
        return d->light.intensity;
    }

    auto ChannelConfig::SetLightInternal(bool internal) -> void {
        d->light.internal = internal;
    }

    auto ChannelConfig::GetLightInternal() const -> bool {
        return d->light.internal;
    }

    auto ChannelConfig::SetFilterChannel(int32_t index) -> void {
        d->filter.index = index;
    }

    auto ChannelConfig::GetFilterChannel() const -> int32_t {
        return d->filter.index;
    }

    auto ChannelConfig::SetChannelName(const QString& name) -> void {
        d->filter.name = name;
    }

    auto ChannelConfig::GetChannelName() const -> QString {
        return d->filter.name;
    }

}
