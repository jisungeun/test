#include <QMap>

#include "Vessel.h"

namespace HTXpress::AppEntity {
    struct WellItem {
        double posX{ 0 };
        double posY{ 0 };
        int32_t rowIdx{ -1 };
        int32_t colIdx{ -1 };
        QString label;

        auto operator==(const WellItem& other) const->bool {
            auto doubleEqual = [](double left, double right)->bool {
                return std::fabs(left - right) <= std::numeric_limits<double>::epsilon();
            };

            if(!doubleEqual(posX, other.posX)) return false;
            if(!doubleEqual(posY, other.posY)) return false;
            if(rowIdx != other.rowIdx) return false;
            if(colIdx != other.colIdx) return false;
            if(label != other.label) return false;
            return true;
        }
    };

    struct Vessel::Impl {
        VesselModel vesselModel;
        QString name;

        double na{0.0};
        int32_t afOffset{ 0 };
        bool multiDish{ false };

        struct {
            double x;
            double y;
        } vesselSize{ 0, 0 };

        WellShape wellShape{ WellShape::Circle };
        struct {
            double x;
            double y;
        } wellSize{ 0, 0 };

        struct {
            double horizontal;
            double vertical;
        } wellSpacing{ 0, 0 };

        struct {
            int32_t rows;
            int32_t cols;
        } wellCount{ 0, 0};

        QMap<WellIndex, WellItem> wells;

        ImagingAreaShape imagingAreaShape{ ImagingAreaShape::Circle };
        struct {
            double x;
            double y;
        } imagingAreaSize{ 0, 0 };
        struct {
            double x;
            double y;
        } imagingAreaPosition{ 0, 0 };

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other)->bool;

        auto wellIndex(int32_t rowIdx, int32_t colIdx)->WellIndex;
    };

    auto Vessel::Impl::operator=(const Impl& other) -> Impl& {
        vesselModel = other.vesselModel;
        name = other.name;
        vesselSize = other.vesselSize;
        wellShape = other.wellShape;
        wellSpacing = other.wellSpacing;
        wellSize = other.wellSize;
        wells = other.wells;
        wellCount = other.wellCount;
        imagingAreaShape = other.imagingAreaShape;
        imagingAreaSize = other.imagingAreaSize;
        imagingAreaPosition = other.imagingAreaPosition;
        na = other.na;
        afOffset = other.afOffset;
        multiDish = other.multiDish;

        return (*this);
    }

    auto Vessel::Impl::operator==(const Impl& other) -> bool {
        auto doubleComp = [](double left, double right)->bool {
            return std::fabs(left - right) <= std::numeric_limits<double>::epsilon();
        };

        if(vesselModel != other.vesselModel) return false;
        if(name != other.name) return false;
        if(!doubleComp(vesselSize.x, other.vesselSize.x)) return false;
        if(!doubleComp(vesselSize.y, other.vesselSize.y)) return false;
        if(wellShape != other.wellShape) return false;
        if(!doubleComp(wellSpacing.horizontal, other.wellSpacing.horizontal)) return false;
        if(!doubleComp(wellSpacing.vertical, other.wellSpacing.vertical)) return false;
        if(!doubleComp(wellSize.x, other.wellSize.x)) return false;
        if(!doubleComp(wellSize.y, other.wellSize.y)) return false;
        if(wells != other.wells) return false;
        if(wellCount.rows != other.wellCount.rows) return false;
        if(wellCount.cols != other.wellCount.cols) return false;
        if(imagingAreaShape != other.imagingAreaShape) return false;
        if(!doubleComp(imagingAreaSize.x, other.imagingAreaSize.x)) return false;
        if(!doubleComp(imagingAreaSize.y, other.imagingAreaSize.y)) return false;
        if(!doubleComp(imagingAreaPosition.x, other.imagingAreaPosition.x)) return false;
        if(!doubleComp(imagingAreaPosition.y, other.imagingAreaPosition.y)) return false;
        if(!doubleComp(na, other.na)) return false;
        if(afOffset != other.afOffset) return false;
        if(multiDish != other.multiDish) return false;

        return true;
    }

    auto Vessel::Impl::wellIndex(int32_t rowIdx, int32_t colIdx) -> WellIndex {
        if((colIdx < 0) || (colIdx >= wellCount.cols)) return -1;
        if((rowIdx < 0) || (rowIdx >= wellCount.rows)) return -1;
        return (rowIdx * wellCount.cols) + colIdx;
    }

    Vessel::Vessel() : d{ new Impl } {
    }

    Vessel::Vessel(const Vessel& other) : d{ new Impl } {
        *d = *other.d;
    }

    Vessel::~Vessel() {
    }

    auto Vessel::operator=(const Vessel& other) -> Vessel& {
        *d = *other.d;
        return *this;
    }

    auto Vessel::operator==(const Vessel& other) const -> bool {
        return *d == *other.d;
    }

    auto Vessel::SetModel(VesselModel model) -> void {
        d->vesselModel = model;
    }

    auto Vessel::GetModel() const -> VesselModel {
        return d->vesselModel;
    }

    auto Vessel::SetName(const QString& name) -> void {
        d->name = name;
    }

    auto Vessel::GetName() const -> QString {
        return d->name;
    }

    auto Vessel::SetNA(double na) -> void {
        d->na = na;
    }

    auto Vessel::GetNA() const -> double {
        return d->na;
    }

    auto Vessel::SetAFOffset(int32_t offsetUm) -> void {
        d->afOffset = offsetUm;
    }

    auto Vessel::GetAFOffset() const -> int32_t {
        return d->afOffset;
    }

    auto Vessel::SetMultiDish(bool multiDish) -> void {
        d->multiDish = multiDish;
    }

    auto Vessel::IsMultiDish() const -> bool {
        return d->multiDish;
    }

    auto Vessel::SetSize(double sizeX, double sizeY) -> void {
        d->vesselSize.x = sizeX;
        d->vesselSize.y = sizeY;
    }

    auto Vessel::GetSizeX() const -> double {
        return d->vesselSize.x;
    }

    auto Vessel::GetSizeY() const -> double {
        return d->vesselSize.y;
    }

    auto Vessel::SetWellShape(WellShape shape) -> void {
        d->wellShape = shape;
    }

    auto Vessel::GetWellShape() const -> WellShape {
        return d->wellShape;
    }

    auto Vessel::SetWellSpacing(double hor, double ver) -> void {
        d->wellSpacing.horizontal = hor;
        d->wellSpacing.vertical = ver;
    }

    auto Vessel::GetWellSpacingHorizontal() const -> double {
        return d->wellSpacing.horizontal;
    }

    auto Vessel::GetWellSpacingVertical() const -> double {
        return d->wellSpacing.vertical;
    }

    auto Vessel::SetWellSize(double sizeX, double sizeY) -> void {
        d->wellSize.x = sizeX;
        d->wellSize.y = sizeY;
    }

    auto Vessel::GetWellSizeX() const -> double {
        return d->wellSize.x;
    }

    auto Vessel::GetWellSizeY() const -> double {
        return d->wellSize.y;
    }

    auto Vessel::SetWellCount(int32_t rows, int32_t cols) -> void {
        d->wellCount.rows = rows;
        d->wellCount.cols = cols;
    }

    auto Vessel::GetWellRows() const -> int32_t {
        return d->wellCount.rows;
    }

    auto Vessel::GetWellCols() const -> int32_t {
        return d->wellCount.cols;
    }

    auto Vessel::AddWell(int32_t rowIdx, int32_t colIdx, double posX, double posY, const QString& label) -> void {
        WellItem well;
        well.posX = posX;
        well.posY = posY;
        well.rowIdx = rowIdx;
        well.colIdx = colIdx;
        well.label = label;

        const auto index = d->wellIndex(rowIdx, colIdx);
        d->wells[index] = std::move(well);
    }

    auto Vessel::GetWellCount() const -> int32_t {
        return d->wells.size();
    }

    auto Vessel::GetWellIndices() const -> QList<WellIndex> {
        return d->wells.keys();
    }

    auto Vessel::GetWellIndex(int32_t rowIdx, int32_t colIdx) -> WellIndex {
        return d->wellIndex(rowIdx, colIdx);
    }

    auto Vessel::GetWellRowIndex(WellIndex index) const -> int32_t {
        return d->wells[index].rowIdx;
    }

    auto Vessel::GetWellColIndex(WellIndex index) const -> int32_t {
        return d->wells[index].colIdx;
    }

    auto Vessel::GetWellPositionX(int32_t rowIdx, int32_t colIdx) const -> double {
        const auto index = d->wellIndex(rowIdx, colIdx);
        if(d->wells.keys().indexOf(index) == -1) return 0;
        return GetWellPositionX(index);
    }

    auto Vessel::GetWellPositionY(int32_t rowIdx, int32_t colIdx) const -> double {
        const auto index = d->wellIndex(rowIdx, colIdx);
        if(d->wells.keys().indexOf(index) == -1) return 0;
        return GetWellPositionY(index);
    }

    auto Vessel::GetWellPosition(int32_t rowIdx, int32_t colIdx) const -> Position {
        const auto posX = GetWellPositionX(rowIdx, colIdx);
        const auto posY = GetWellPositionY(rowIdx, colIdx);
        return Position::fromMM(posX, posY, 0);
    }

    auto Vessel::GetWellLabel(int32_t rowIdx, int32_t colIdx) const -> QString {
        const auto index = d->wellIndex(rowIdx, colIdx);
        if(d->wells.keys().indexOf(index) == -1) return "Invalid";
        return GetWellLabel(index);
    }

    auto Vessel::GetWellPositionX(WellIndex index) const -> double {
        return d->wells[index].posX;
    }

    auto Vessel::GetWellPositionY(WellIndex index) const -> double {
        return d->wells[index].posY;
    }

    auto Vessel::GetWellPosition(WellIndex index) const -> Position {
        return Position::fromMM(GetWellPositionX(index), GetWellPositionY(index), 0);
    }

    auto Vessel::GetWellLabel(WellIndex index) const -> QString {
        return d->wells[index].label;
    }

    auto Vessel::SetImagingAreaShape(ImagingAreaShape shape) -> void {
        d->imagingAreaShape = shape;
    }

    auto Vessel::GetImagingAreaShape() const -> ImagingAreaShape {
        return d->imagingAreaShape;
    }

    auto Vessel::SetImagingAreaSize(double sizeX, double sizeY) -> void {
        d->imagingAreaSize.x = sizeX;
        d->imagingAreaSize.y = sizeY;
    }

    auto Vessel::GetImagingAreaSizeX() const -> double {
        return d->imagingAreaSize.x;
    }

    auto Vessel::GetImagingAreaSizeY() const -> double {
        return d->imagingAreaSize.y;
    }

    auto Vessel::SetImagingAreaPosition(double posX, double posY) -> void {
        d->imagingAreaPosition.x = posX;
        d->imagingAreaPosition.y = posY;
    }

    auto Vessel::GetImagingAreaPositionX() const -> double {
        return d->imagingAreaPosition.x;
    }

    auto Vessel::GetImagingAreaPositionY() const -> double {
        return d->imagingAreaPosition.y;
    }

    auto Vessel::IsInImagingArea(const double& posXInWell, const double& posYInWell) const -> bool {
        bool result = false;

        const double relativePosX = posXInWell - d->imagingAreaPosition.x;
        const double relativePosY = posYInWell - d->imagingAreaPosition.y;

        const double halfWidth = d->imagingAreaSize.x / 2;
        const double halfHeight = d->imagingAreaSize.y / 2;

        if (d->imagingAreaShape == +ImagingAreaShape::Rectangle) {
            const auto isXIn = std::abs(relativePosX) <= halfWidth;
            const auto isYIn = std::abs(relativePosY) <= halfHeight;
            result = isXIn && isYIn;
        } 

        if (d->imagingAreaShape == +ImagingAreaShape::Circle) {
            const auto normalizedPosX = std::pow(relativePosX / halfWidth, 2);
            const auto normalizedPosY = std::pow(relativePosY / halfHeight, 2);
            result = normalizedPosX + normalizedPosY <= 1;
        }

        return result;
    }
}
