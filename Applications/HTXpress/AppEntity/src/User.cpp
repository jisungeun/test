#include "User.h"

namespace HTXpress::AppEntity {
    struct User::Impl {
        UserID id;
        QString name;
        Profile profile{ Profile::Operator };

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other)->bool;
    };

    auto User::Impl::operator=(const Impl& other) -> Impl& {
        id = other.id;
        name = other.name;
        profile = other.profile;
        return (*this);
    }

    auto User::Impl::operator==(const Impl& other) -> bool {
        if(id != other.id) return false;
        if(name != other.name) return false;
        if(profile != other.profile) return false;
        return true;
    }

    User::User() : d{ new Impl } {
    }

    User::User(const User& other) :d{ new Impl } {
        *d = *other.d;
    }

    User::~User() {
    }

    auto User::operator=(const User& other) -> User& {
        *d = *other.d;
        return (*this);
    }

    auto User::operator==(const User& other) const -> bool {
        return (*d == *other.d);
    }

    auto User::SetUserID(UserID id) -> void {
        d->id = id;
    }

    auto User::GetUserID() const -> UserID {
        return d->id;
    }

    auto User::SetName(const QString& name) -> void {
        d->name = name;
    }

    auto User::GetName() const -> QString {
        return d->name;
    }

    auto User::SetProfile(Profile profile) -> void {
        d->profile = profile;
    }

    auto User::GetProfile() const -> Profile {
        return d->profile;
    }
}
