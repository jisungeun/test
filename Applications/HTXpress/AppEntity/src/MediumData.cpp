﻿#include "MediumData.h"

namespace HTXpress::AppEntity {
    struct MediumData::Impl {
        QString name;
        double ri{0};
    };

    MediumData::MediumData() : d{std::make_unique<Impl>()} {
    }

    MediumData::MediumData(const MediumData& other) : d{std::make_unique<Impl>()} {
        *d = *other.d;
    }

    MediumData::~MediumData() {
    }

    auto MediumData::operator=(const MediumData& other) -> MediumData& {
        *d = *other.d;
        return *this;
    }

    auto MediumData::operator==(const MediumData& other) const -> bool {
        if (d->name != other.d->name) return false;
        if (fabs(d->ri - other.d->ri) > std::numeric_limits<double>::epsilon()) return false;

        return true;
    }

    auto MediumData::operator!=(const MediumData& other) const -> bool {
        return !(*this == other);
    }

    auto MediumData::SetName(const QString& name) -> void {
        d->name = name;
    }

    auto MediumData::GetName() const -> QString {
        return d->name;
    }

    auto MediumData::SetRI(double ri) -> void {
        d->ri = ri;
    }

    auto MediumData::GetRI() const -> double {
        return d->ri;
    }
}
