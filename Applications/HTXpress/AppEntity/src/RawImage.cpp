#include "RawImage.h"

namespace HTXpress::AppEntity {
    struct RawImage::Impl {
        int32_t sizeX{ 0 };
        int32_t sizeY{ 0 };
        std::shared_ptr<uint8_t[]> buffer;
        QDateTime timestamp;
    };

    RawImage::RawImage() : d{new Impl} {
    }

    RawImage::RawImage(int32_t sizeX, int32_t sizeY, std::shared_ptr<uint8_t[]> buffer) : d{ new Impl} {
        SetImage(sizeX, sizeY, buffer);
    }

    RawImage::~RawImage() {
    }

    auto RawImage::SetImage(int32_t sizeX, int32_t sizeY, std::shared_ptr<uint8_t[]> buffer) -> void {
        d->sizeX = sizeX;
        d->sizeY = sizeY;
        d->buffer = buffer;
    }

    auto RawImage::GetSizeX() const -> int32_t {
        return d->sizeX;
    }

    auto RawImage::GetSizeY() const -> int32_t {
        return d->sizeY;
    }

    auto RawImage::GetBuffer() const -> std::shared_ptr<uint8_t[]> {
        return d->buffer;
    }

    auto RawImage::SetTimestamp(const QDateTime& timestamp) -> void {
        d->timestamp = timestamp;
    }

    auto RawImage::GetTimestamp() const -> QDateTime {
        return d->timestamp;
    }

}
