#include "PatternIndex.h"

namespace HTXpress::AppEntity {
    struct PatternIndex::Impl {
        ImagingType imagingType{ ImagingType::HT3D };
        TriggerType triggerType{ TriggerType::FreeRun };
        int32_t channel{ 0 };
    };
    
    PatternIndex::PatternIndex() : d{new Impl} {
    }
    
    PatternIndex::PatternIndex(const PatternIndex& other) : d{new Impl} {
        d->imagingType = other.d->imagingType;
        d->triggerType = other.d->triggerType;
        d->channel = other.d->channel;
    }
    
    PatternIndex::PatternIndex(ImagingType imagingType, TriggerType triggerType, int32_t channel) : d{new Impl} {
        d->imagingType = imagingType;
        d->triggerType = triggerType;
        d->channel = channel;
    }
    
    PatternIndex::~PatternIndex() {
    }
    
    auto PatternIndex::operator=(const PatternIndex& rhs) -> PatternIndex& {
        d->imagingType = rhs.d->imagingType;
        d->triggerType = rhs.d->triggerType;
        d->channel = rhs.d->channel;
        return *this;
    }
    
    auto PatternIndex::operator<(const PatternIndex& rhs) const -> bool {
        if(d->imagingType > rhs.d->imagingType) return false;
        if(d->imagingType < rhs.d->imagingType) return true;

        if(d->triggerType > rhs.d->triggerType) return false;
        if(d->triggerType < rhs.d->triggerType) return true;

        return (d->channel < rhs.d->channel);
    }

    auto PatternIndex::GetImagingType() const -> ImagingType {
        return d->imagingType;
    }

    auto PatternIndex::GetTriggerType() const -> TriggerType {
        return d->triggerType;
    }

    auto PatternIndex::GetChannel() const -> int32_t {
        return d->channel;
    }
}
