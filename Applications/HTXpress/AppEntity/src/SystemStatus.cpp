#include <QMutex>

#include "SystemStatus.h"

namespace HTXpress::AppEntity {
    struct SystemStatus::Impl {
        struct {
            int32_t x{ 1024 };
            int32_t y{ 1024 };
        } fov;
        Position currentPosition;
        double cAxisPosInMM{ 0 };
        VesselIndex currentVesselIndex{ 0 };
        WellIndex currentWellIndex{ 0 };
        ImagingMode currentLiveConfig{ ImagingMode::BFGray };
        ImagingConfig imagingConfig;
        QString projectTitle;
        Experiment::Pointer experiment;
        QString experimentPath;
        QString dataPath;
        struct {
            Modality modality{Modality::BF};
            int32_t channel{ 0 };
        } liveImage;
        struct {
            bool autofocusEnabled{ true };
            int32_t targetValue{ 0 };
            double zPosition{ 0 };
        } focus;
        struct {
            QMutex mutex;
            bool flag{ false };
        } busy;
        bool timelapseRunning{ false };
        ServiceMode serviceMode{ ServiceMode::None };
        struct {
            bool htIlluminationCalibrated{ false };
        } initialization;

        struct {
            bool sampleStageEncoder{ false };
        } feature;
    };

    SystemStatus::SystemStatus() : d{new Impl} {
    }

    SystemStatus::~SystemStatus() {
    }

    auto SystemStatus::GetInstance() -> Pointer {
        static Pointer theInstance{ new SystemStatus() };
        return theInstance;
    }

    auto SystemStatus::SetCurrentGlobalPosition(const Position& position) -> void {
        d->currentPosition = position;
    }

    auto SystemStatus::GetCurrentGlobalPosition() const -> Position {
        return d->currentPosition;
    }

    auto SystemStatus::SetCurrentCondensorPosition(const double posInMM) -> void {
        d->cAxisPosInMM = posInMM;
    }

    auto SystemStatus::GetCurrentCondensorPosition() const -> double {
        return d->cAxisPosInMM;
    }

    auto SystemStatus::SetCurrentVesselIndex(VesselIndex vesselIndex) -> void {
        d->currentVesselIndex = vesselIndex;
    }

    auto SystemStatus::GetCurrentVesselIndex() const -> VesselIndex {
        return d->currentVesselIndex;
    }

    auto SystemStatus::SetCurrentWell(WellIndex wellIndex) -> void {
        d->currentWellIndex = wellIndex;
    }

    auto SystemStatus::GetCurrentWell() const -> WellIndex {
        return d->currentWellIndex;
    }

    auto SystemStatus::SetChannelConfig(ImagingMode mode, const ChannelConfig& config) -> void {
        d->imagingConfig.SetChannelConfig(mode, config);
    }

    auto SystemStatus::GetChannelConfig(ImagingMode mode) -> ChannelConfig::Pointer {
        return d->imagingConfig.GetChannelConfig(mode);
    }

    auto SystemStatus::GetChannelConfigAll() -> QMap<ImagingMode, ChannelConfig::Pointer> {
        return d->imagingConfig.GetChannelConfigAll();
    }

    auto SystemStatus::SetScanConfig(ImagingMode mode, const ScanConfig& config) ->void {
        d->imagingConfig.SetScanConfig(mode, config);
    }

    auto SystemStatus::GetScanConfig(ImagingMode mode) -> ScanConfig::Pointer {
        return d->imagingConfig.GetScanConfig(mode);
    }

    auto SystemStatus::SetLiveConfig(ImagingMode mode, const ChannelConfig& config) -> void {
        d->currentLiveConfig = mode;
        d->imagingConfig.SetLiveConfig(mode, config);
    }

    auto SystemStatus::GetLiveConfig(ImagingMode mode) -> ChannelConfig::Pointer {
        return d->imagingConfig.GetLiveConfig(mode);
    }

    auto SystemStatus::GetCurrentLiveConfigMode() const -> ImagingMode {
        return d->currentLiveConfig;
    }

    auto SystemStatus::SetProjectTitle(const QString& title) -> void {
        d->projectTitle = title;
    }

    auto SystemStatus::GetProjectTitle() const -> QString {
        return d->projectTitle;
    }

    auto SystemStatus::SetExperiment(const Experiment::Pointer experiment, const QString& path) -> void {
        d->experiment.reset(new Experiment(*experiment));
        d->experimentPath = path;
    }

    auto SystemStatus::GetExperiment() const -> Experiment::Pointer {
        return d->experiment;
    }

    auto SystemStatus::GetExperimentPath() const -> QString {
        return d->experimentPath;
    }

    auto SystemStatus::ResetExperiment() -> void {
        d->experiment = nullptr;
        d->experimentPath.clear();
        d->currentVesselIndex = 0;
        d->currentWellIndex = 0;
    }

    auto SystemStatus::IsExperimentLoaded() const -> bool {
        return (d->experiment != nullptr) && !d->experimentPath.isEmpty();
    }

    auto SystemStatus::SetBusy(bool busy) -> void {
        QMutexLocker locker(&d->busy.mutex);
        d->busy.flag = busy;
    }

    auto SystemStatus::GetBusy() const -> bool {
        QMutexLocker locker(&d->busy.mutex);
        return d->busy.flag;
    }

    auto SystemStatus::SetAutoFocusEnabled(bool enable) -> void {
        d->focus.autofocusEnabled = enable;
    }

    auto SystemStatus::GetAutoFocusEnabled() const -> bool {
        return d->focus.autofocusEnabled;
    }

    auto SystemStatus::SetTimelapseRunning(bool isRunning) -> void {
        d->timelapseRunning = isRunning;
    }

    auto SystemStatus::GetTimelapseRunning() const -> bool {
        return d->timelapseRunning;
    }

    auto SystemStatus::GetNA() const -> double {
        qDebug() << "vessel: " << d->experiment->GetVessel()->GetModel();
        qDebug() << "na: " << d->experiment->GetVessel()->GetNA();
        return d->experiment->GetVessel()->GetNA();
    }

    auto SystemStatus::SetExperimentOutputPath(const QString& path) -> void {
        d->dataPath = path;
    }

    auto SystemStatus::GetExperimentOutputPath() const -> QString {
        return d->dataPath;
    }

    auto SystemStatus::SetCurrentLiveImage(Modality modality, int32_t channel) -> void {
        d->liveImage.modality = modality;
        d->liveImage.channel = channel;
    }

    auto SystemStatus::GetCurrentLiveImage() const -> std::tuple<Modality, int32_t> {
        return std::make_tuple(d->liveImage.modality, d->liveImage.channel);
    }

    auto SystemStatus::SetCurrentFocusTarget(const int32_t value) -> void {
        d->focus.targetValue = value;
    }

    auto SystemStatus::GetCurrentFocusTarget() const -> int32_t {
        return d->focus.targetValue;
    }

    auto SystemStatus::SetFocusZPosition(const double position) -> void {
        d->focus.zPosition = position;
    }

    auto SystemStatus::GetFocusZPosition() const -> double {
        return d->focus.zPosition;
    }

    auto SystemStatus::SetServiceMode(ServiceMode mode) -> void {
        d->serviceMode = mode;
    }

    auto SystemStatus::GetServiceMode() const -> ServiceMode {
        return d->serviceMode;
    }

    auto SystemStatus::SetHTIlluminationCalibrated(const bool calibrated) -> void {
        d->initialization.htIlluminationCalibrated = calibrated;
    }

    auto SystemStatus::GetHTIlluminationCalibrated() const -> bool {
        return d->initialization.htIlluminationCalibrated;
    }

    auto SystemStatus::SetSampleStageEncoderSupported(const bool supported) -> void {
        d->feature.sampleStageEncoder = supported;
    }

    auto SystemStatus::GetSampleStageEncoderSupported() const -> bool {
        return d->feature.sampleStageEncoder;
    }
}
