#include "Position.h"

namespace HTXpress::AppEntity {
    static auto _id = qRegisterMetaType<AppEntity::Position>("AppEntity::Position");

    struct Position::Impl {
        int32_t x{ 0 };
        int32_t y{ 0 };
        int32_t z{ 0 };

        auto operator=(const Impl& other)->Impl& {
            x = other.x;
            y = other.y;
            z = other.z;
            return *this;
        }

        auto operator==(const Impl& other) const->bool {
            if(x != other.x) return false;
            if(y != other.y) return false;
            if(z != other.z) return false;
            return true;
        }

        auto operator+(const Impl& other) const->Impl {
            Impl res;
            res.x = x + other.x;
            res.y = y + other.y;
            res.z = z + other.z;
            return res;
        }

        auto operator-(const Impl& other) const->Impl {
            Impl res;
            res.x = x - other.x;
            res.y = y - other.y;
            res.z = z - other.z;
            return res;
        }
    };

    Position::Position() : d{ new Impl} {
    }

    Position::Position(const Position& other) : d{ new Impl} {
        *d = *other.d;
    }

    Position::Position(int32_t x, int32_t y, int32_t z) : d{ new Impl} {
        d->x = x;
        d->y = y;
        d->z = z;
    }

    Position::~Position() {
    }

    auto Position::operator=(const Position& other) -> Position& {
        *d = *other.d;
        return *this;
    }

    auto Position::operator==(const Position& other) const -> bool {
        return *d == *other.d;
    }

    auto Position::operator!=(const Position& other) const -> bool {
        return !(*d == *other.d);
    }

    auto Position::operator+(const Position& other) const -> Position {
        Position res;
        *res.d = *d + *other.d;
        return res;
    }

    auto Position::operator-(const Position& other) const -> Position {
        Position res;
        *res.d = *d - *other.d;
        return res;
    }

    auto Position::X() const -> int32_t {
        return d->x;
    }

    auto Position::Y() const -> int32_t {
        return d->y;
    }

    auto Position::Z() const -> int32_t {
        return d->z;
    }

    auto Position::toUM() const -> RealPos {
        RealPos pos;
        pos.x = d->x / 1000.0;
        pos.y = d->y / 1000.0;
        pos.z = d->z / 1000.0;
        return pos;
    }

    auto Position::toMM() const -> RealPos {
        RealPos pos;
        pos.x = d->x / 1000000.0;
        pos.y = d->y / 1000000.0;
        pos.z = d->z / 1000000.0;
        return pos;
    }

    auto Position::fromUM(double x, double y, double z) -> Position {
        return Position(static_cast<int32_t>(x*1000), 
                        static_cast<int32_t>(y*1000), 
                        static_cast<int32_t>(z*1000));
    }

    auto Position::fromMM(double x, double y, double z) -> Position {
        return Position(static_cast<int32_t>(x*1000000), 
                        static_cast<int32_t>(y*1000000), 
                        static_cast<int32_t>(z*1000000));
    }
}
