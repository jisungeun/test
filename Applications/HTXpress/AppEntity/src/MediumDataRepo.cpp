﻿#include "MediumDataRepo.h"

namespace HTXpress::AppEntity {
    struct MediumDataRepo::Impl {
        List media;
    };

    MediumDataRepo::MediumDataRepo() : d{std::make_unique<Impl>()} {
    }

    MediumDataRepo::~MediumDataRepo() {
    }

    auto MediumDataRepo::GetInstance() -> Pointer {
        static Pointer theInstance{new MediumDataRepo()};
        return theInstance;
    }

    auto MediumDataRepo::SetMedia(const List& media) -> void {
        d->media = media;
    }

    auto MediumDataRepo::GetMedia() const -> List& {
        return d->media;
    }

    auto MediumDataRepo::GetMedium(const QString& name, MediumData& medium) -> bool {
        auto found = std::find_if(d->media.begin(), d->media.end(), [name](const auto& m) {
           return  m.GetName() == name;
        });

        if(found != d->media.end()) {
            medium = (*found);
            return true;
        }

        return false;
    }

    auto MediumDataRepo::Clear() const -> void {
        d->media.clear();
    }

}
