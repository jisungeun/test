#include <QList>
#include "ImagingSequence.h"

namespace HTXpress::AppEntity {
    struct ImagingSequence::Impl {
        QList<ImagingCondition::Pointer> modalities;
        struct {
            int32_t interval{ 0 };
            int32_t count{ 1 };
        } timeLapse;

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other) const->bool;
    };

    auto ImagingSequence::Impl::operator=(const Impl& other) -> Impl& {
        modalities.clear();
        const auto modalityCount = other.modalities.size();
        for(auto idx=0; idx<modalityCount; ++idx) {
            auto modality = other.modalities.at(idx)->Clone();
            modalities.append(modality);
        }
        timeLapse.interval = other.timeLapse.interval;
        timeLapse.count = other.timeLapse.count;
        return *this;
    }

    auto ImagingSequence::Impl::operator==(const Impl& other) const -> bool {
        if(modalities.size() != other.modalities.size()) return false;

        const auto modalityCount = modalities.size();
        for (auto modalityIndex = 0; modalityIndex < modalityCount; modalityIndex++) {
            auto lhs = modalities.at(modalityIndex);
            auto rhs = other.modalities.at(modalityIndex);

            if (lhs->GetModality() != rhs->GetModality()) return false;

            if (lhs->GetModality() == +Modality::HT) {
                if (!(*dynamic_cast<ImagingConditionHT*>(lhs.get()) == *dynamic_cast<ImagingConditionHT*>(rhs.get()))) return false;
            } else if (lhs->GetModality() == +Modality::FL) {
                if (!(*dynamic_cast<ImagingConditionFL*>(lhs.get()) == *dynamic_cast<ImagingConditionFL*>(rhs.get()))) return false;
            } else if (lhs->GetModality() == +Modality::BF) {
                if (!(*dynamic_cast<ImagingConditionBF*>(lhs.get()) == *dynamic_cast<ImagingConditionBF*>(rhs.get()))) return false;
            }
        }

        if(timeLapse.interval != other.timeLapse.interval) return false;
        if(timeLapse.count != other.timeLapse.count) return false;
        return true;
    }

    ImagingSequence::ImagingSequence() : d{new Impl} {
    }

    ImagingSequence::ImagingSequence(const ImagingSequence& other) : d{new Impl} {
        *this = other;
    }

    ImagingSequence::~ImagingSequence() {
    }

    auto ImagingSequence::operator=(const ImagingSequence& other) -> ImagingSequence& {
        *d = *other.d;
        return *this;
    }

    auto ImagingSequence::operator==(const ImagingSequence& other) const -> bool {
        return *d == *other.d;
    }

    auto ImagingSequence::AddImagingCondition(const ImagingCondition::Pointer& condition) -> void {
        d->modalities.push_back(condition);
    }

    auto ImagingSequence::SetImagingCondition(int32_t index, const ImagingCondition::Pointer& condition) -> void {
        d->modalities[index] = condition;
    }

    auto ImagingSequence::GetImagingCondition(int32_t index) -> ImagingCondition::Pointer {
        return d->modalities[index];
    }

    auto ImagingSequence::GetImagingCondition() -> QList<ImagingCondition::Pointer> {
        return d->modalities;
    }

    auto ImagingSequence::GetModalityCount() const -> int32_t {
        return d->modalities.count();
    }

    auto ImagingSequence::RemoveImagingCondition(int32_t index) -> void {
        d->modalities.removeAt(index);
    }

    auto ImagingSequence::SetTimelapseCondition(int32_t interval, int32_t count) -> void {
        d->timeLapse.interval = interval;
        d->timeLapse.count = count;
    }

    auto ImagingSequence::GetInterval() const -> int32_t {
        return d->timeLapse.interval;
    }

    auto ImagingSequence::GetTimeCount() const -> int32_t {
        return d->timeLapse.count;
    }

    auto ImagingSequence::ToStr() const -> QString {
        QString out;

        out = QString("Modality:%1 Timelapse=[Count:%2 Interval: %3]")
            .arg(d->modalities.count()).arg(d->timeLapse.count).arg(d->timeLapse.interval);

        return out;
    }

    auto ImagingSequence::GetImagingTypes() -> QSet<ImagingType> {
        QSet<ImagingType> types;

        for(auto imgCond : d->modalities) {
            types.insert(imgCond->GetImagingType());
        }

        return types;
    }
}
