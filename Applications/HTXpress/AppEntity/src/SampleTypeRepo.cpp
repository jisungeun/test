﻿#include <QMap>

#include "SampleTypeRepo.h"

namespace HTXpress::AppEntity {
    struct SampleTypeRepo::Impl {
        QMap<ModelName, QList<SampleTypeName>> sampleTypeNameMap{};
    };

    SampleTypeRepo::SampleTypeRepo() : d{std::make_unique<Impl>()} {
    }

    auto SampleTypeRepo::GetInstance() -> Pointer {
        static Pointer theInstance{ new SampleTypeRepo() };
        return theInstance;
    }

    SampleTypeRepo::~SampleTypeRepo() = default;

    auto SampleTypeRepo::AddSampleType(const ModelName& modelName, const SampleTypeName& sampleTypeName) -> void {
        d->sampleTypeNameMap[modelName].push_back(sampleTypeName);
    }

    auto SampleTypeRepo::GetSampleTypeNamesByModel(const ModelName& modelName) const -> std::optional<QStringList> {
        QStringList profileNames;
        if(!d->sampleTypeNameMap.contains(modelName)) {
            return std::nullopt;
        }

        return d->sampleTypeNameMap[modelName];
    }
}
