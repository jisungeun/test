#include <QList>
#include "ImagingScenario.h"

namespace HTXpress::AppEntity {
    struct ImagingScenario::Impl {
        QList<ImagingSequence::Pointer> sequences;
        QList<uint32_t> startTimes;
        QList<QString> titles;

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other) const->bool;
    };

    auto ImagingScenario::Impl::operator=(const Impl& other) -> Impl& {
        sequences.clear();
        const auto seqCount = other.sequences.size();
        for(auto idx=0; idx<seqCount; ++idx) {
            auto sequence = std::make_shared<ImagingSequence>(*other.sequences.at(idx));
            sequences.append(sequence);
        }

        startTimes = other.startTimes;
        titles = other.titles;
        return *this;
    }

    auto ImagingScenario::Impl::operator==(const Impl& other) const -> bool {
        if(sequences.size() != other.sequences.size()) return false;

        const auto sequenceCount = sequences.size();
        for(auto sequenceIndex = 0; sequenceIndex < sequenceCount; sequenceIndex++) {
            if(!(*sequences.at(sequenceIndex) == *other.sequences.at(sequenceIndex))) return false;
        }

        if(startTimes != other.startTimes) return false;
        if(titles != other.titles) return false;
        return true;
    }

    ImagingScenario::ImagingScenario() : d{new Impl} {
    }

    ImagingScenario::ImagingScenario(const ImagingScenario& other) :d{new Impl} {
        *this = other;
    }

    ImagingScenario::~ImagingScenario() {
    }

    auto ImagingScenario::operator=(const ImagingScenario& other) -> ImagingScenario& {
        *d = *other.d;
        return *this;
    }

    auto ImagingScenario::operator==(const ImagingScenario& other) const -> bool {
        return *d == *other.d;
    }

    auto ImagingScenario::AddSequence(uint32_t startTime, const QString& title, const ImagingSequence::Pointer& sequence) -> void {
        d->sequences.push_back(sequence);
        d->startTimes.push_back(startTime);
        d->titles.push_back(title);
    }

    auto ImagingScenario::EditSequence(uint32_t index, uint32_t startTime, const QString& title) -> void {
        d->startTimes[index] = startTime;
        d->titles[index] = title;
    }

    auto ImagingScenario::GetSequence(uint32_t index) const -> ImagingSequence::Pointer {
        if(index >= static_cast<uint32_t>(d->sequences.size())) return nullptr;
        return d->sequences[index];
    }

    auto ImagingScenario::GetStartTime(uint32_t index) -> uint32_t {
        if(index >= static_cast<uint32_t>(d->sequences.size())) return 0;
        return d->startTimes[index];
    }

    auto ImagingScenario::GetTitle(uint32_t index) const -> QString {
        if(index >= static_cast<uint32_t>(d->sequences.size())) return "";
        return d->titles[index];
    }

    auto ImagingScenario::GetCount() const -> int32_t {
        return d->sequences.count();
    }

    auto ImagingScenario::RemoveSequence(int32_t index) -> void {
        d->sequences.removeAt(index);
        d->startTimes.removeAt(index);
    }

    auto ImagingScenario::RemoveAll() -> void {
        d->sequences.clear();
        d->startTimes.clear();
    }

    auto ImagingScenario::GetImagingTypes() -> QSet<ImagingType> {
        QSet<ImagingType> types;

        for(auto sequence : d->sequences) {
            types += sequence->GetImagingTypes();
        }

        return types;
    }
}
