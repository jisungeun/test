#include <QMap>
#include "VesselRepo.h"

namespace HTXpress::AppEntity {
    struct VesselRepo::Impl {
        QMap<QString, Vessel::Pointer> vessels;
        QMap<QString, QString> models;
    };

    VesselRepo::VesselRepo() : d{new Impl} {
    }

    VesselRepo::~VesselRepo() {
    }

    auto VesselRepo::GetInstance()->Pointer {
        static Pointer theInstance{ new VesselRepo() };
        return theInstance;
    }

    auto VesselRepo::AddVessel(const Vessel::Pointer& holder) -> void {
        d->vessels[holder->GetModel()] = holder;
        d->models[holder->GetName()] = holder->GetModel();
    }

    auto VesselRepo::SetVessels(const QList<Vessel::Pointer>& vessels) -> void {
        // clear previous vessel container
        d->vessels.clear();
        d->models.clear();
        for(const auto& vessel : vessels)
        {
            AddVessel(vessel);
        }
    }

    auto VesselRepo::GetVessels() const->QList<Vessel::Pointer> {
        return d->vessels.values();
    }

    auto VesselRepo::GetVesselByName(const QString& name) const -> Vessel::Pointer {
        if(!d->models.contains(name)) {
            return nullptr;
        }
        const auto& model = d->models[name];
        return d->vessels[model];
    }

    auto VesselRepo::GetVesselByModel(const QString& model) const -> Vessel::Pointer {
        if(!d->vessels.contains(model)) {
            return nullptr;
        }
        return d->vessels[model];
    }
}
