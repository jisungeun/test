#include <QMap>
#include "PositionGroup.h"

namespace HTXpress::AppEntity {
    struct PositionGroup::Impl {
        uint32_t rowCount{ 1 };
        uint32_t colCount{ 1 };
        QMap<uint32_t, QMap<uint32_t, Position>> table;
        QString title;
        Position center;
        WellIndex wellIndex{ 0 };
        LocationIndex locationIndex{ 0 };

        auto operator=(const Impl& other)->Impl & = default;
        auto operator==(const Impl& other) const->bool;
    };

    auto PositionGroup::Impl::operator==(const Impl& other) const -> bool {
        if(rowCount != other.rowCount) return false;
        if(colCount != other.colCount) return false;
        if(table != other.table) return false;
        if(title != other.title) return false;
        if(center != other.center) return false;
        if(wellIndex != other.wellIndex) return false;
        if(locationIndex != other.locationIndex) return false;
        return true;
    }

    PositionGroup::PositionGroup(const QString& title) : d{new Impl} {
        d->title = title;
    }

    PositionGroup::PositionGroup(const PositionGroup& other) : d{new Impl} {
        *d = *other.d;
    }

    PositionGroup::PositionGroup(const QString& title, const Position& position) : d{new Impl} {
        d->rowCount = 1;
        d->colCount = 1;
        d->table[0][0] = position;
        d->title = title;
        d->center = position;
    }

    PositionGroup::~PositionGroup() {
    }

    auto PositionGroup::operator=(const PositionGroup& other) -> PositionGroup& {
        *d = *other.d;
        return *this;
    }

    auto PositionGroup::operator==(const PositionGroup& other) const -> bool {
        return *d == *other.d;
    }

    auto PositionGroup::operator!=(const PositionGroup& other) const -> bool {
        return !(*d == *other.d);
    }

    auto PositionGroup::GetTitle() const -> QString {
        return d->title;
    }

    auto PositionGroup::SetSize(uint32_t rowCount, uint32_t colCount) -> void {
        d->rowCount = rowCount;
        d->colCount = colCount;
    }

    auto PositionGroup::GetRows() const -> uint32_t {
        return d->rowCount;
    }

    auto PositionGroup::GetCols() const -> uint32_t {
        return d->colCount;
    }

    auto PositionGroup::GetCount() const -> uint32_t {
        return d->rowCount * d->colCount;
    }

    auto PositionGroup::SetWellIndex(const WellIndex& wellIdx) -> void {
        d->wellIndex = wellIdx;
    }

    auto PositionGroup::GetWellIndex() const -> WellIndex {
        return d->wellIndex;
    }

    auto PositionGroup::SetLocationIndex(const LocationIndex& locationIndex) -> void {
        d->locationIndex = locationIndex;
    }

    auto PositionGroup::GetLocationIndex() const -> LocationIndex {
        return d->locationIndex;
    }

    auto PositionGroup::SetCenter(const Position& center) -> void {
        d->center = center;
    }

    auto PositionGroup::GetCenter() const -> Position {
        return d->center;
    }

    auto PositionGroup::SetPosition(uint32_t rowIdx, uint32_t colIdx, const Position& position) -> bool {
        if(rowIdx >= d->rowCount) return false;
        if(colIdx >= d->colCount) return false;
        d->table[rowIdx][colIdx] = position;
        return true;
    }

    auto PositionGroup::GetPosition(uint32_t rowIdx, uint32_t colIdx) const -> Position {
        return d->table[rowIdx][colIdx];
    }

    auto PositionGroup::GetPositions() const -> QList<Position> {
        QList<Position> positions;

        for(uint32_t rowIdx=0; rowIdx<d->rowCount; rowIdx++) {
            const bool oddLine = (rowIdx%2 == 0);
            for(uint32_t colIdx=0; colIdx<d->colCount; colIdx++) {
                const auto modifiedColIdx = oddLine?colIdx:(d->colCount-colIdx-1);
                positions.push_back(d->table[rowIdx][modifiedColIdx]);
            }
        }

        return positions;
    }
}
