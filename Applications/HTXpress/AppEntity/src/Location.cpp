#include "Location.h"

namespace HTXpress::AppEntity {
    struct Location::Impl {
        Position center;        //! center position
        Area area;              //! size of acquisition area
        bool tile{ false };     //! true if area is larger than FOV
        QString title;          //! title of location

        auto operator==(const Impl& other) const->bool {
            if(center != other.center) return false;
            if(area != other.area) return false;
            if(tile != other.tile) return false;
            if(title != other.title) return false;
            return true;
        }
    };
    
    Location::Location(const QString& title) : d{new Impl} {
        d->title = title;
    }
    
    Location::Location(const Location& other) : d{new Impl} {
        *d = *other.d;
    }

    Location::Location(const Position& position, const Area& area, const bool isTile, const QString& title) : d{new Impl} {
        d->center = position;
        d->area = area;
        d->tile = isTile;
        d->title = title;
    }

    Location::~Location() {
    }

    auto Location::operator==(const Location& other) const->bool {
        return *d == *other.d;
    }

    auto Location::operator=(const Location& other)->Location& {
        *d = *other.d;
        return *this;
    }

    auto Location::SetCenter(const Position& position) -> void {
        d->center = position;
    }

    auto Location::GetCenter() const -> Position {
        return d->center;
    }

    auto Location::SetArea(const Area& area, bool isTile) -> void {
        d->area = area;
        d->tile = isTile;
    }

    auto Location::GetArea() const -> Area {
        return d->area;
    }

    auto Location::IsTile() const -> bool {
        return d->tile;
    }

    auto Location::SetTitle(const QString& title) -> void {
        d->title = title;
    }

    auto Location::GetTitle() const -> QString {
        return d->title;
    }
}
