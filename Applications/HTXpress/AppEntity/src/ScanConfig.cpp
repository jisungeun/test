#include "ScanConfig.h"

namespace HTXpress::AppEntity {
    struct ScanConfig::Impl {
        FLScanMode mode{ FLScanMode::Default };
        int32_t bottomInPulse;
        int32_t topInPulse;
        int32_t stepInPulse;
        int32_t scanSteps;
        double flFocusOffsetInUm;
    };

    ScanConfig::ScanConfig() : d{new Impl} {
    }

    ScanConfig::ScanConfig(const ScanConfig& other) : d{new Impl} {
        *d = *other.d;
    }

    ScanConfig::~ScanConfig() {
    }

    auto ScanConfig::operator=(const ScanConfig& other) -> ScanConfig& {
        *d = *other.d;
        return *this;
    }

    auto ScanConfig::SetScanMode(FLScanMode mode) -> void {
        d->mode = mode;
    }

    auto ScanConfig::GetScanMode() const -> FLScanMode {
        return d->mode;
    }

    auto ScanConfig::SetRange(int32_t bottomInPulse, int32_t topInPulse, int32_t stepInPulse) -> void {
        d->bottomInPulse = bottomInPulse;
        d->topInPulse = topInPulse;
        d->stepInPulse = stepInPulse;
    }

    auto ScanConfig::GetBottom() const -> int32_t {
        return d->bottomInPulse;
    }

    auto ScanConfig::GetTop() const -> int32_t {
        return d->topInPulse;
    }

    auto ScanConfig::GetStep() const -> int32_t {
        return d->stepInPulse;
    }

    auto ScanConfig::SetSteps(int32_t steps) -> void {
        d->scanSteps = steps;
    }

    auto ScanConfig::GetSteps() const -> int32_t {
        return d->scanSteps;
    }

    auto ScanConfig::SetFocusUm(double flFocusOffsetInUm) -> void {
        d->flFocusOffsetInUm = flFocusOffsetInUm;
    }

    auto ScanConfig::GetFocusFLOffsetUm() const -> double {
        return d->flFocusOffsetInUm;
    }
}
