#include "SessionManager.h"

namespace HTXpress::AppEntity {
    struct SessionManager::Impl {
        User::Pointer user;
    };

    SessionManager::SessionManager() : d{new Impl} {
    }

    SessionManager::~SessionManager() {
    }

    auto SessionManager::GetInstance() -> Pointer {
        static Pointer theInstance{ new SessionManager() };
        return theInstance;
    }

    auto SessionManager::Login(const User::Pointer& user) -> void {
        d->user = user;
    }

    auto SessionManager::Logout() -> void {
        d->user = nullptr;
    }

    auto SessionManager::LoggedIn() const -> bool {
        return (d->user != nullptr);
    }

    auto SessionManager::GetID() const -> UserID {
        if(d->user == nullptr) return QString();
        return d->user->GetUserID();
    }

    auto SessionManager::GetName() const -> QString {
        if(d->user == nullptr) return QString();
        return d->user->GetName();
    }

    auto SessionManager::GetProfile() const -> Profile {
        if(d->user == nullptr) return Profile::Operator;
        return d->user->GetProfile();
    }
}
