#include <QMap>
#include "ModelRepo.h"

namespace HTXpress::AppEntity {
    struct ModelRepo::Impl {
        QMap<QString, Model::Pointer> models;
    };

    ModelRepo::ModelRepo() : d{new Impl} {
    }

    ModelRepo::~ModelRepo() {
    }

    auto ModelRepo::GetInstance() -> Pointer {
        static Pointer theInstance{new ModelRepo()};
        return theInstance;
    }

    auto ModelRepo::AddModel(const Model::Pointer model) -> void {
        d->models[model->Name()] = model;
    }

    auto ModelRepo::GetModels() const -> QList<Model::Pointer> {
        return d->models.values();
    }

    auto ModelRepo::GetModel(const QString name) const -> Model::Pointer {
        if (d->models.find(name) != d->models.end()) {
            return d->models[name];
        }
        return nullptr;
    }
}
