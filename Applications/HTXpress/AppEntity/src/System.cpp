#include <QMetaType>

#include "AppEntityDefines.h"
#include "System.h"

namespace HTXpress::AppEntity {
    struct System::Impl {
        Model::Pointer model{ nullptr };
        SystemConfig::Pointer systemConfig{ nullptr };
        Area maximumFOV;
        QString hostID;
        QString swVersion;
        QString backgroundPath;
        QString sampleTypeFolderPath;
    };

    System::System() : d{new Impl} {
    }

    System::~System() {
    }

    auto System::GetInstance() -> Pointer {
        static Pointer theInstance{ new System() };
        return theInstance;
    }

    auto System::SetHostID(const QString& id) -> void {
        GetInstance()->d->hostID = id;
    }

    auto System::GetHostID() -> QString {
        return GetInstance()->d->hostID;
    }

    auto System::SetSoftwareVersion(const QString& version) -> void {
        GetInstance()->d->swVersion = version;
    }

    auto System::GetSoftwareVersion() -> QString {
        return GetInstance()->d->swVersion;
    }

    auto System::SetModel(const Model::Pointer& model) -> void {
        GetInstance()->d->model = model;
    }

    auto System::GetModel() -> Model::Pointer {
        return GetInstance()->d->model;
    }

    auto System::SetSystemConfig(const SystemConfig::Pointer& config)->void {
        GetInstance()->d->systemConfig = config;
    }

    auto System::GetSystemConfig() -> SystemConfig::Pointer {
        if(!GetInstance()->d->systemConfig) {
            GetInstance()->d->systemConfig = std::make_shared<SystemConfig>();
        }

        return GetInstance()->d->systemConfig;
    }

    auto System::GetMaximumFOV() -> Area {
        auto& model = GetInstance()->d->model;
        auto fovH = model->CameraPixelsH() * model->CameraPixelSize() / model->ObjectiveLensMagnification();
        auto fovV = model->CameraPixelsV() * model->CameraPixelSize() / model->ObjectiveLensMagnification();
        return Area::fromUM(fovH, fovV);
    }

    auto System::GetCameraResolutionInUM() -> double {
        auto& model = GetInstance()->d->model;
        return model->CameraPixelSize() / model->ObjectiveLensMagnification();
    }

    auto System::GetSystemCenter() -> Position {
        return GetInstance()->d->systemConfig->GetSystemCenter();
    }

    auto System::GetTileScanOverlap() -> uint32_t {
        return GetInstance()->d->systemConfig->GetTileScanOverlap();
    }

    auto System::SetBackgroundPath(const QString& path) -> void {
        GetInstance()->d->backgroundPath = path;
    }

    auto System::GetBackgroundPath() -> QString {
        return GetInstance()->d->backgroundPath;
    }

    auto System::GetBackgroundPath(double NA) -> QString {
        return QString("%1/%2").arg(GetInstance()->d->backgroundPath).arg(NA);
    }

    auto System::SetSampleTypeFolderPath(const QString& path) -> void {
        GetInstance()->d->sampleTypeFolderPath = path;
    }

    auto System::GetSampleTypeFolderPath() -> QString {
        return GetInstance()->d->sampleTypeFolderPath;
    }

    auto System::GetSampleTypeFolderPath(const QString& model) -> QString {
        return QString("%1/%2").arg(GetSampleTypeFolderPath()).arg(model);
    }
}
