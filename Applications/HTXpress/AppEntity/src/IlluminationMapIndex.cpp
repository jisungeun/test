#include "IlluminationMapIndex.h"

namespace HTXpress::AppEntity {
    struct IlluminationMapIndex::Impl {
        int32_t channels[3]={-1, };
    };

    IlluminationMapIndex::IlluminationMapIndex() : d{new Impl} {
    }

    IlluminationMapIndex::IlluminationMapIndex(const IlluminationMapIndex& other) : d{new Impl} {
        memcpy_s(d->channels, 3*sizeof(int32_t), other.d->channels, 3*sizeof(int32_t));
    }

    IlluminationMapIndex::IlluminationMapIndex(int32_t red, int32_t green, int32_t blue) : d{new Impl} {
        d->channels[0] = red;
        d->channels[1] = green;
        d->channels[2] = blue;
    }

    IlluminationMapIndex::IlluminationMapIndex(const QList<int32_t>& values) : d{new Impl} {
        d->channels[0] = values[0];
        d->channels[1] = values[1];
        d->channels[2] = values[2];
    }

    IlluminationMapIndex::~IlluminationMapIndex() {
    }

    auto IlluminationMapIndex::operator=(const IlluminationMapIndex& rhs) -> IlluminationMapIndex& {
        memcpy_s(d->channels, 3*sizeof(int32_t), rhs.d->channels, 3*sizeof(int32_t));
        return *this;
    }

    auto IlluminationMapIndex::operator==(const IlluminationMapIndex& rhs) const -> bool {
        for(auto idx=0; idx<3; idx++) {
            if(d->channels[idx] != rhs.d->channels[idx]) return false;
        }
        return true;
    }

    auto IlluminationMapIndex::operator<(const IlluminationMapIndex& rhs) const -> bool {
        for(int32_t idx=0; idx<3; idx++) {
            if(d->channels[idx] > rhs.d->channels[idx]) return false;
            if(d->channels[idx] < rhs.d->channels[idx]) return true;
        }
        return false;
    }

    auto IlluminationMapIndex::GetChannelRed() const -> int32_t {
        return d->channels[0];
    }

    auto IlluminationMapIndex::GetChannelGreen() const -> int32_t {
        return d->channels[1];
    }

    auto IlluminationMapIndex::GetChannelBlue() const -> int32_t {
        return d->channels[2];
    }
}
