﻿#include "ROI.h"

namespace HTXpress::AppEntity {
    struct ROI::Impl {
        Position center{};
        Area size{};
        ROIShape::_enumerated shape{};
        QString name{};
    };

    ROI::ROI(const Position& centerPos, const Area& size, const ROIShape& shape, const QString& name) : d{std::make_unique<Impl>()} {
        d->center = centerPos;
        d->size = size;
        d->shape = shape;
        d->name = name;
    }

    ROI::ROI(const ROI& other) : d{std::make_unique<Impl>(*other.d)} {
    }

    ROI::ROI(ROI&& other) noexcept = default;

    ROI::~ROI() = default;

    auto ROI::operator=(const ROI& other) -> ROI& {
        *d = *other.d;
        return *this;
    }

    auto ROI::operator=(ROI&& other) noexcept -> ROI& = default;


    auto ROI::SetCenter(const Position& center) -> void {
        d->center = center;
    }

    auto ROI::GetCenter() const -> Position {
        return d->center;
    }

    auto ROI::SetSize(const Area& size) -> void {
        d->size = size;
    }

    auto ROI::GetSize() const -> Area {
        return d->size;
    }

    auto ROI::SetShape(const ROIShape& shape) -> void {
        d->shape = shape;
    }

    auto ROI::GetShape() const -> ROIShape {
        return d->shape;
    }

    auto ROI::SetName(const QString& name) -> void {
        d->name = name;
    }

    auto ROI::GetName() const -> QString {
        return d->name;
    }
}
