#include "Area.h"

namespace HTXpress::AppEntity {
    struct Area::Impl {
        uint32_t width{ 0 };
        uint32_t height{ 0 };

        auto operator=(const Impl& other)->Impl& {
            width = other.width;
            height = other.height;
            return *this;
        }

        auto operator==(const Impl& other) const->bool {
            if(width != other.width) return false;
            if(height != other.height) return false;
            return true;
        }
    };

    Area::Area() :d{ new Impl} {
    }

    Area::Area(const Area& other) :d{ new Impl} {
        *d = *other.d;
    }

    Area::Area(uint32_t width, uint32_t height) :d{ new Impl} {
        d->width = width;
        d->height = height;
    }

    Area::~Area() {
    }

    auto Area::operator=(const Area& other) -> Area& {
        *d = *other.d;
        return *this;
    }

    auto Area::operator==(const Area& other) const -> bool {
        return *d == *other.d;
    }

    auto Area::operator!=(const Area& other) const -> bool {
        return !(*d == *other.d);
    }

    auto Area::Width() const -> int32_t {
        return d->width;
    }

    auto Area::Height() const -> int32_t {
        return d->height;
    }

    auto Area::Expand(int32_t width, int32_t height) const -> Area {
        Area res;
        res.d->width = d->width + width;
        res.d->height = d->height + height;
        return res;
    }

    auto Area::ExpandInUM(double width, double height) const -> Area {
        Area res;
        res.d->width = d->width + static_cast<int32_t>(width/1000);
        res.d->height = d->height + static_cast<int32_t>(height/1000);
        return res;
    }

    auto Area::ExpandInMM(double width, double height) const -> Area {
        Area res;
        res.d->width = d->width + static_cast<int32_t>(width/1000000);
        res.d->height = d->height + static_cast<int32_t>(height/1000000);
        return res;
    }

    auto Area::toUM() const -> RealArea {
        RealArea area;
        area.width = d->width/1000.0;
        area.height = d->height/1000.0;
        return area;
    }

    auto Area::toMM() const -> RealArea {
        RealArea area;
        area.width = d->width/1000000.0;
        area.height = d->height/1000000.0;
        return area;
    }

    auto Area::fromUM(double width, double height) -> Area {
        Area area;
        area.d->width = static_cast<uint32_t>(width*1000);
        area.d->height = static_cast<uint32_t>(height*1000);
        return area;
    }

    auto Area::fromMM(double width, double height) -> Area {
        Area area;
        area.d->width = static_cast<uint32_t>(width*1000000);
        area.d->height = static_cast<uint32_t>(height*1000000);
        return area;
    }
}
