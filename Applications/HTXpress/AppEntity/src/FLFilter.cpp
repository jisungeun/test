﻿#include <QColor>

#include "FLFilter.h"

namespace HTXpress::AppEntity {
    struct FLFilter::Impl {
        int32_t waveLength{0};
        int32_t bandwidth{0};
    };

    FLFilter::FLFilter() : d {new Impl} {
    }

    FLFilter::FLFilter(const FLFilter& other) : d{new Impl} {
        *d = *other.d;
    }

    FLFilter::FLFilter(int32_t wavelength, int32_t bandwidth) : d{new Impl} {
        d->waveLength = wavelength;
        d->bandwidth = bandwidth;
    }

    FLFilter::~FLFilter() {
    }

    auto FLFilter::operator=(const FLFilter& other) -> FLFilter& {
        *d = *other.d;
        return *this;
    }

    auto FLFilter::operator==(const FLFilter& other) const -> bool {
        if(d->waveLength != other.d->waveLength) return false;
        if(d->bandwidth != other.d->bandwidth) return false;

        return true;
    }

    auto FLFilter::operator!=(const FLFilter& other) const -> bool {
        return !(*this==other);
    }

    auto FLFilter::operator<(const FLFilter& other) const -> bool {
        if(d->waveLength > other.GetWaveLength()) return false;
        if(d->waveLength < other.GetWaveLength()) return true;
        if(d->bandwidth < other.GetBandwidth()) return true;
        return false;
    }

    auto FLFilter::SetWaveLength(int32_t waveLength) -> void {
        d->waveLength = waveLength;
    }

    auto FLFilter::GetWaveLength() const -> int32_t {
        return d->waveLength;
    }

    auto FLFilter::SetBandwidth(int32_t bandwidth) -> void {
        d->bandwidth = bandwidth;
    }

    auto FLFilter::GetBandwidth() const -> int32_t {
        return d->bandwidth;
    }
}
