#include "Model.h"

namespace HTXpress::AppEntity {
    struct Model::Impl {
        QString name;
    };

    Model::Model(const QString& name) : d{ new Impl } {
        d->name = name;
    }

    Model::Model(const Model& other) : d{ new Impl } {
        d->name = other.d->name;
    }

    Model::~Model() {
    }

    auto Model::operator=(const Model& other) -> Model& {
        d->name = other.d->name;
        return (*this);
    }

    auto Model::Name() const -> QString {
        return d->name;
    }

    auto Model::AxisLowerLimitPulse(Axis axis) const -> int32_t {
        const auto ppm = AxisResolutionPPM(axis);
        const auto limit = AxisLowerLimitMM(axis);
        return static_cast<int32_t>(ppm * limit);
    }

    auto Model::AxisUpperLimitPulse(Axis axis) const -> int32_t {
        const auto ppm = AxisResolutionPPM(axis);
        const auto limit = AxisUpperLimitMM(axis);
        return static_cast<int32_t>(ppm * limit);
    }

    auto Model::SafeZPositionPulse() const -> int32_t {
        const auto ppm = AxisResolutionPPM(Axis::Z);
        const auto pos = SafeZPositionMM();
        return static_cast<int32_t>(ppm * pos);
    }

    auto Model::LoadingXPositionPulse() const -> int32_t {
        const auto ppm = AxisResolutionPPM(Axis::X);
        const auto pos = LoadingXPositionMM();
        return static_cast<int32_t>(ppm * pos);
    }

    auto Model::LoadingYPositionPulse() const -> int32_t {
        const auto ppm = AxisResolutionPPM(Axis::Y);
        const auto pos = LoadingYPositionMM();
        return static_cast<int32_t>(ppm * pos);
    }

    auto Model::EmissionFilterPulse(int32_t channel) const -> int32_t {
        const auto positions = EmissionFilterPules();
        if((channel < 0) || (channel >= positions.size())) return -1;
        return positions.at(channel);
    }
}
