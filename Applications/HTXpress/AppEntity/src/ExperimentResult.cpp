#include "ExperimentResult.h"

namespace HTXpress::AppEntity {
    struct ExperimentResult::Impl {
        QString experimentPath;
        QString prefix;
        Project project;
        Experiment experiment;
        QStringList dataList;

        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other) const->bool;
    };

    auto ExperimentResult::Impl::operator=(const Impl& other) -> Impl& {
        experiment = other.experiment;
        prefix = other.prefix;
        project = other.project;
        experiment = other.experiment;
        dataList = other.dataList;
        return *this;
    }

    auto ExperimentResult::Impl::operator==(const Impl& other) const -> bool {
        if(experimentPath != other.experimentPath) return false;
        if(prefix != other.prefix) return false;
        if(!(project == other.project)) return false;
        if(!(experiment == other.experiment)) return false;
        if(dataList != other.dataList) return false;
        return true;
    }

    ExperimentResult::ExperimentResult() : d{new Impl} {
    }

    ExperimentResult::ExperimentResult(const ExperimentResult& other) : d{new Impl} {
        *this = other;
    }

    ExperimentResult::~ExperimentResult() {
    }

    auto ExperimentResult::operator=(const ExperimentResult& other) -> ExperimentResult& {
        *d = *other.d;
        return *this;
    }

    auto ExperimentResult::operator==(const ExperimentResult& other) const -> bool {
        return *d == *other.d;
    }

    auto ExperimentResult::SetExperimentPath(const QString& path) -> void {
        d->experimentPath = path;
    }

    auto ExperimentResult::GetExperimentPath() const -> QString {
        return d->experimentPath;
    }

    auto ExperimentResult::SetPrefix(const QString& prefix) -> void {
        d->prefix = prefix;
    }

    auto ExperimentResult::GetPrefix() const -> QString {
        return d->prefix;
    }

    auto ExperimentResult::SetProject(const Project& project) -> void {
        d->project = project;
    }

    auto ExperimentResult::GetProject() const -> Project {
        return d->project;
    }

    auto ExperimentResult::SetExperiment(const Experiment& experiment) -> void {
        d->experiment = experiment;
    }

    auto ExperimentResult::GetExperiment() const -> Experiment {
        return d->experiment;
    }

    auto ExperimentResult::SetDataPath(const QStringList& list) -> void {
        d->dataList = list;
    }

    auto ExperimentResult::GetDataPath() const -> QStringList {
        return d->dataList;
    }
}
