#pragma once
#include <memory>

namespace HTXpress {
    class MainWindowControl {
    public:
        MainWindowControl();
        ~MainWindowControl();

        auto InitApplication()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
