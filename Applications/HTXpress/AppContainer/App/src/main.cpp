#include <QApplication>
#include <QString>
#include <QTimer>
#include <QSplashScreen>
#include <QElapsedTimer>
#include <QFontDatabase>
#include <QStandardPaths>
#include <QHostInfo>
#include <QDirIterator>
#include <QSettings>

#include <ctkPluginContext.h>
#include <service/event/ctkEvent.h>
#include <BugSplat.h>

#include <QtSingleApplication.h>

#include <LicenseManager.h>
#include <LicenseDialog.h>
#include <MessageDialog.h>
#include <ZipUtility.h>
#include <FileUtility.h>

#include <System.h>
#include <TCLogger.h>

#include "Version.h"
#include "MainWindow.h"
#include "TabUiFramework.h"

#define PRODUCT_DATA L"MjBFMkRGNjVGQzM0QkUyMEVDRTUzMzEwQzc0NkM0REQ=.zopfgOjNZQguckHR4H8c8mjyi9aypppF6p9KUqQADEjVShHpKd0ZzO3hH6Yea72iEL0dqP9El3u8PVmg8R8MWwZKj1IGGpN0KJF3KMbkYjHGJXLBnRmp1OfUw+pDtjNVZ7om6cZjLsndRAEiO+sBgcCqoQoHG5a0sqpJp4mV9cLFNrZpsqIDDUjK85/KSz/P1TGPazID+lB0wIYeNJTye9edDxg37gZdXLDqYvJkCo8D/GitK5Gv+qNsjsonI/rJ2w2zD1yJFbUWRd5v5MZ24athOe/HKcMznOphTElieEe9jmSuMTNDXWw30Ij9FPgG30TmmDKx1b6kbWqLxIi7NjRLptv9LwZYfyH2bEEOJ0TVQ4IdSiWMhfyxigJSC++VWwsQ6pQVqKiH9YH7RYu3bFvlCIojy1pAApU5RCLRlc1PhrUS5l/Fk3FqHx8DmF34goTeiUnzR9enq04RX4mvZwFbXEfJ95IHv7+rQAdEPlEvD/+QtdFXpsbZgwXwWn49xaxKFAslHvjHa70LAh6/xV4cU/7m8/z4wj8VZunN9fKFCqbGRecEOIHawLPlSNVT5YpyzTo4R7HfYU835j9iUO7ScIZM5CeUI7IuJVFF1u804q1utVeI25blHiMRtQj7DzPBhx8CjQt6pDA921ssTNGZHWeTX4uJ3+HDU6HbIKz/ytOW9IPWYk0u/a/PCJAReJixYys4UUM3sdRt+Zt0dXxoaMDIMGRuvHLdmWxuDyQ="
#define PRODUCT_ID "17f43a07-dfb6-43db-8897-a27c5120dc50"

bool CrashCallback(UINT nCode, LPVOID lpVal1, LPVOID lpVal2);
void ArchiveLogs();

MiniDmpSender *mpSender = nullptr;

int main(int argc, char* argv[]) {
	QtSingleApplication app(argc, argv); // instead of using QApplication
	if (app.sendMessage("Wake up!")) return 0;

	QFontDatabase::addApplicationFont(":/font/NotoSansKR-Bold.otf");
	QFontDatabase::addApplicationFont(":/font/NotoSansKR-Regular.otf");

	QFont font;
	font.setFamily("Noto Sans KR");
	font.setPixelSize(12);
	font.setHintingPreference(QFont::HintingPreference::PreferNoHinting);

	QApplication::setFont(font);

	app.setApplicationName("TomoStudioX");
	app.setOrganizationName("Tomocube, Inc.");
	app.setOrganizationDomain("www.tomocube.com");

#ifndef DEBUG
	mpSender = new MiniDmpSender(L"tomostudio_x_v1", 
								 L"TomoStudio X", 
                                 PROJECT_REVISION_W,
								 NULL, 
								 MDSF_USEGUARDMEMORY | MDSF_LOGFILE | MDSF_PREVENTHIJACKING);
	
	SetGlobalCRTExceptionBehavior();
	SetPerThreadCRTExceptionBehavior();

	mpSender->setGuardByteBufferSize(20*1024*1024);
	mpSender->setDefaultUserEmail(L"user@youremail.com");
    mpSender->setDefaultUserName(L"User Name");
	mpSender->setDefaultUserDescription(L"Describe the situation in which the SW crash occurred.");
	mpSender->setCallback(CrashCallback);
#endif

	QString stylesheet;

	QFile qssCommon(":/style/tcdarkcommon.qss");
    if (!qssCommon.exists()) {
		printf("Unable to set stylesheet, file not found\n");
	} else {
		qssCommon.open(QFile::ReadOnly | QFile::Text);
		stylesheet.append(QTextStream(&qssCommon).readAll());
	}

	QFile qssTSX(":/style/tctsx.qss");
    if (!qssTSX.exists()) {
		printf("Unable to set stylesheet, file not found\n");
	} else {
		qssTSX.open(QFile::ReadOnly | QFile::Text);
		stylesheet.append(QTextStream(&qssTSX).readAll());
	}

	if (!stylesheet.isEmpty()) qApp->setStyleSheet(stylesheet);

	auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
	ArchiveLogs();

	TC::Logger::Initialize(QString("%1/log/TomoStudioX.log").arg(appDataPath), QString("%1/log/TomoStudioXError.log").arg(appDataPath));
	TC::Logger::SetTraceLevel();

	QSplashScreen splash(QPixmap(":/app/images/splash.png"), Qt::WindowStaysOnTopHint);
	splash.setEnabled(false);
	splash.showMessage("Loading...");
	splash.show();
	qApp->processEvents();

	QElapsedTimer timer;
	timer.start();

	//License
	LicenseManager::SetLicenseInfo(QString::fromStdWString(PRODUCT_DATA), QString::fromStdString(PRODUCT_ID));
	while (timer.elapsed() < 1000) qApp->processEvents();
		
	if (!LicenseManager::CheckLicense()) {
		splash.hide();
		TC::MessageDialog::warning(0, "License", LicenseManager::GetErrorAsString(), QDialogButtonBox::StandardButton::Ok);

		LicenseDialog dlg;
		dlg.exec();

		splash.show();
	}

	if (!LicenseManager::CheckLicense()) {
        splash.hide();
        TC::MessageDialog::warning(0, "License", LicenseManager::GetErrorAsString(), QDialogButtonBox::StandardButton::Ok);
        return EXIT_SUCCESS;
	}
 
	HTXpress::AppEntity::System::SetHostID(QHostInfo::localHostName());
	HTXpress::AppEntity::System::SetSoftwareVersion(PROJECT_REVISION);

	QLOG_INFO() << "SW Version: " << HTXpress::AppEntity::System::GetSoftwareVersion();
	QLOG_INFO() << "Host Name: " << HTXpress::AppEntity::System::GetHostID();
	QLOG_INFO() << "Edition: " << LicenseManager::GetProductEdition();

	HTXpress::MainWindow* w = new HTXpress::MainWindow();
	w->setAnimated(false);	

	//hide splash window...
	while (timer.elapsed() < 2000) qApp->processEvents();
	splash.hide();
    
	w->showMaximized();

	QObject::connect(&app, &QtSingleApplication::messageReceived, [=](){ w->showMaximized(); });

    app.exec();
    
	qApp->exit();
	delete w;
	
	return EXIT_SUCCESS;
}

bool CrashCallback(UINT nCode, LPVOID lpVal1, LPVOID lpVal2) {
	//destroy logger
	QsLogging::Logger& logger = QsLogging::Logger::instance();
	logger.destroyInstance();
	Sleep(2000);

	wchar_t tempPath[MAX_PATH];
	GetTempPathW(MAX_PATH, tempPath);
	QString strTemp = QString::fromStdWString(tempPath);

	auto BuildTargetLogfilePath = [=](const QString& strPath)->QString {
	    QString strSrc = strPath;
	    
	    strSrc.replace("\\", "/");
	    
	    QStringList toks = strSrc.split("/");
	    const int nLast = toks.size() - 3;
	    for (int ii = 0; ii < nLast; ii++) toks.removeFirst();

	    return toks.join("_");
	};

	const auto strTop = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);

	int idx = 0;
	QDirIterator itr(strTop, QDir::Dirs | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);
	while (itr.hasNext()) {
		itr.next();

		QDir dir(itr.filePath());
		QStringList filter{ "*.log",
		                    "*.log.1",
		                    "*.log.2",
		                    "*.tcxexp*",
		                    "*.ini",
		                    "*.model",
		                    "*.vessel"};

		QStringList entries = dir.entryList(filter);
		foreach(QString entry, entries) {
			const QString strSrcPath = dir.absoluteFilePath(entry);
			const QString strTargetName = BuildTargetLogfilePath(strSrcPath);
			const QString strTargetPath = QString("%1\\%2").arg(strTemp).arg(strTargetName);
			QFile::copy(strSrcPath, strTargetPath);

			mpSender->sendAdditionalFile(strTargetPath.toStdWString().c_str());
		}
	}

	auto reportDir = QString("%1/crashreports").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
	if(!QDir(reportDir).exists()) QDir().mkpath(reportDir);
	auto reportZip = QString("%1/TSX_crash_report_%2.zip").arg(reportDir)
	                                                      .arg(QDateTime::currentDateTime().toString("yyyyMMdd.hhmmss"));
	mpSender->setUserZipPath(reportZip.toStdWString().c_str());
	
	return true;
}

void ArchiveLogs() {
	const auto last = QSettings().value("Misc/LastExecution", "19990101").toInt();
	const auto today = QDate::currentDate().toString("yyyyMMdd").toInt();
	if(today <= last) return;

	const auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation).remove(".");
	const auto& logPath = QString("%1/log").arg(appDataPath);
    const auto& configPath = QString("%1/config").arg(appDataPath);
	const auto& archivePath = QString("%1/log.archive/%2.7z").arg(appDataPath).arg(QDateTime::currentDateTime().toString("yyyyMMdd.hhmmss"));

	if(!TC::ZipUtil::Compress({logPath, configPath}, archivePath)) {
	    TC::ZipUtil::SetBinPath(ZipBin);
		if(!TC::ZipUtil::Compress({logPath, configPath}, archivePath)) {
			return;
		}
	}

	TC::RemoveRecursive(logPath);
	QDir().mkpath(logPath);

	QSettings().setValue("Misc/LastExecution", today);
}