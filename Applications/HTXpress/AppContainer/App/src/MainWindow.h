#pragma once
#include <memory>

#include <QMainWindow>
#include <QVariantList>

#include <service/event/ctkEvent.h>

namespace HTXpress {
    class MainWindow : public QMainWindow
    {
        Q_OBJECT
    
    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();
   
    protected:
        void closeEvent(QCloseEvent* event) override;

    private:
        auto initDefaultAppUi(void)->void;
        auto loadAppUi(const QString& fullName, const QString& appName, const QVariantMap& params = QVariantMap(), const bool closable = true, const bool raisetab = true)->void;
        auto callApp(ctkEvent e)->void;
        auto callAppArg(ctkEvent e)->void;

        auto OnMenuRefreshed(ctkEvent e)->void;
        auto OnTitleBarChanged(ctkEvent e)->void;
        auto OnApplicationCall(ctkEvent e)->void;

    protected slots:
        void onCTKEvent(ctkEvent e);   

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
