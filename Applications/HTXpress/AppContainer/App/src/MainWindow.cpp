#include <iostream>

#include <QFileDialog>
#include <QActionGroup>
#include <QSettings>
#include <QJsonValue>
#include <QCloseEvent>
#include <QMap>

#include <ctkPluginFrameworkLauncher.h>
#include <service/event/ctkEventAdmin.h>
#include <service/event/ctkEvent.h>
#include <LicenseManager.h>
#include <LicenseDialog.h>

#include <ValueEvent.h>
#include <MenuEvent.h>
#include <AppEvent.h>
#include <MessageDialog.h>

#include <AppManagerHTX.h>
#include <AppInterfaceHTX.h>
#include <TabUiFramework.h>
#include <UIUtility.h>
#include <System.h>
#include <SystemStatus.h>

#include "MainWindowControl.h"
#include "ui_MainWindow.h"
#include "MainWindow.h"

namespace HTXpress {
    struct MainWindow::Impl {
        Ui::MainWindow ui;
        MainWindowControl control;

        TC::Framework::EventHandler* handler{ nullptr };
        TC::Framework::EventPublisher* publisher{ nullptr };
        TC::Framework::TabUiFramework* framework{ nullptr };

        int currentLayoutIndex = -1;

        QString windowTitle;
        QStringList menu_names;
    };

    MainWindow::MainWindow(QWidget* parent) : d(new Impl()) {
        setWindowFlag(Qt::WindowStaysOnTopHint);

        d->ui.setupUi(this);                       
        d->ui.menubar->hide();
        d->ui.statusbar->hide();
        setMinimumSize(1024, 768);

        d->windowTitle = QString("TomoStudio X - %1 (%2)").arg(AppEntity::System::GetSoftwareVersion())
                                                          .arg(AppEntity::System::GetHostID());
        setWindowTitle(d->windowTitle);

        auto appManager = AppComponents::Framework::AppManagerHTX::GetInstance();
        appManager->AcquirePackage();

        initDefaultAppUi();

        //Init communication port
        d->publisher = new TC::Framework::EventPublisher;        
        d->handler = new TC::Framework::EventHandler;
        d->publisher->setPluginContext(ctkPluginFrameworkLauncher::getPluginContext());
        d->handler->setPluginContext(ctkPluginFrameworkLauncher::getPluginContext());
        d->handler->subscribeEvent(MenuEvent::Topic());
        d->handler->subscribeEvent(AppEvent::Topic());
        
        connect(d->handler, SIGNAL(sigCtkEvent(ctkEvent)),this,SLOT(onCTKEvent(ctkEvent)));

        if(!d->control.InitApplication()) {
            TC::MessageDialog::warning(this, "System Initialization", tr("It fails to initialize the system"));
        }

        setWindowFlag(Qt::WindowStaysOnTopHint, false);
    }

    MainWindow::~MainWindow() {
        if(d->framework)
            delete d->framework;
        if(d->publisher)
            delete d->publisher;
        if(d->handler)
            delete d->handler;

        auto appManager = AppComponents::Framework::AppManagerHTX::GetInstance();
        appManager->Stop();
    }

    void MainWindow::closeEvent(QCloseEvent* event) {
        auto status = AppEntity::SystemStatus::GetInstance();
        if(status->GetBusy()) {
            TC::MessageDialog::information(this, "TomoStudio X",
                                           tr("You can't close the software because the microscope is busy now"));
            event->ignore();
            return;
        }

        const auto selection = TC::MessageDialog::question(this, "TomoStudio X",
                                                           tr("Would you like to exit the software?"));
        if(selection != TC::MessageDialog::StandardButton::Yes) {
            event->ignore();
            return;
        }

        if(event->spontaneous()) {
            MenuEvent menuEvt(MenuTypeEnum::NONE);
            menuEvt.scriptSet("SuddenClose");
            d->publisher->publishSignal(menuEvt, "MainWindow");
        } else {
            QMainWindow::closeEvent(event);
        }
    }
    
    auto MainWindow::initDefaultAppUi() -> void {
        auto appManager = AppComponents::Framework::AppManagerHTX::GetInstance();
        //initialize TabUiFramework with CTK
        d->framework = new TC::Framework::TabUiFramework(appManager);
        appManager->AddPluginSearchPath(qApp->applicationDirPath() + "/PluginApp");

        d->framework->setTabsVisible(false);
        d->framework->initialize();
        setCentralWidget(d->framework->getMainTab());
                
        //load default app
        loadAppUi("org.tomocube.general.start", "Home", QVariantMap(), false, true);

        d->windowTitle = QString("TomoStudio X - %1 (%2/%3)").arg(AppEntity::System::GetSoftwareVersion())
                                                                     .arg(AppEntity::System::GetSystemConfig()->GetSerial())
                                                                     .arg(AppEntity::System::GetHostID());
        setWindowTitle(d->windowTitle);
    }

    auto MainWindow::loadAppUi(const QString& fullName, const QString& appName, const QVariantMap& params, const bool closable, const bool raisetab) -> void {
        const auto appManager = AppManagerHTX::GetInstance();
        if (!d->framework)
            return;

        if (!appManager->LoadAppPlugin(fullName))
            return;
        if (!appManager->Activate(fullName))
            return;

        TC::MessageDialog loadingMsg(tr("Progress"), tr("Loading, please wait..."));
        loadingMsg.SetStandardButtons(QDialogButtonBox::NoButton);
        loadingMsg.SetClosable(false);
        loadingMsg.setModal(true);
        loadingMsg.show();

        QCoreApplication::processEvents();

        d->framework->addUiFramework(fullName);
        if (!closable)
            d->framework->setTabUnclosable(appName);
        if (raisetab)
            d->framework->raiseTab(appName);

        auto* pluginInterface = dynamic_cast<AppInterfaceHTX*>(appManager->GetAppInterface(fullName));
        pluginInterface->Execute(params);
        loadingMsg.close();
    }
    
    auto MainWindow::callApp(ctkEvent e) -> void {
        auto appEvent = std::make_unique<TC::Framework::AppEvent>(e);
        loadAppUi(appEvent->getFullName(), appEvent->getAppName());
    }

    auto MainWindow::callAppArg(ctkEvent e) -> void {
        auto appEvent = std::make_unique<TC::Framework::AppEvent>(e);

        QVariantMap params;
        auto paramNames = appEvent->getParameterNames();
        for(auto& name : paramNames) {
            params[name] = appEvent->getParameter(name);
        }

        loadAppUi(appEvent->getFullName(), appEvent->getAppName(), params);
    }

    auto MainWindow::OnMenuRefreshed(ctkEvent e) ->void{
        //clear existing menu properties
        d->ui.menubar->clear();
        d->menu_names.clear();

        ////parse event to build menu        
        auto me = new TC::Framework::MenuEvent;
        me->SetEvent(e);
        d-> menu_names = me->constructMenu(d->ui.menubar);        

        connect(d->ui.menubar, SIGNAL(triggered(QAction*)), this, SLOT(OnMenuTriggered(QAction*)), Qt::UniqueConnection);
    }

    auto MainWindow::OnTitleBarChanged(ctkEvent e) -> void {
        auto me = new TC::Framework::MenuEvent;
        me->SetEvent(e);

        const auto& titleInfo = me->getTitleInfo();

        if(titleInfo.isEmpty()) {
            this->setWindowTitle(d->windowTitle);
        }else {
            this->setWindowTitle(d->windowTitle + " - " + titleInfo);            
        }        
    }

    auto MainWindow::OnApplicationCall(ctkEvent e) -> void {
        using AppEnum = TC::Framework::AppTypeEnum;
        auto appEvent = TC::Framework::AppEvent();
        appEvent.SetEvent(e);
        
        auto type = appEvent.getEventType();
        switch(type) {
        case AppEnum::APP_WITHOUT_ARG:
            callApp(e);
            break;
        case AppEnum::APP_WITH_ARGS:
            callAppArg(e);
            break;
        case AppEnum::ARGS_ONLY:            
            break;
        }
    }

    void MainWindow::onCTKEvent(ctkEvent e) {
        const auto& topic = e.getTopic();

        if(topic == AppEvent::Topic()) {
            OnApplicationCall(e);
        } else if(topic == MenuEvent::Topic()) {
            MenuEvent menuEvt(e);
            if(menuEvt.IsType(MenuTypeEnum::MenuScript)) {
                OnMenuRefreshed(e);
            } else if(menuEvt.IsType(MenuTypeEnum::TitleBar)) {
                OnTitleBarChanged(e);
            }
        }
    }
}
