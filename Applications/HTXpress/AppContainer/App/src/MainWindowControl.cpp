#include "MainWindowControl.h"

namespace HTXpress {
    struct MainWindowControl::Impl {
    };

    MainWindowControl::MainWindowControl() : d{new Impl} {
    }

    MainWindowControl::~MainWindowControl() {
    }

    auto MainWindowControl::InitApplication() -> bool {
        //TODO Application 초기화 관련 사항들 구현 e.g. License
        return true;
    }
}
