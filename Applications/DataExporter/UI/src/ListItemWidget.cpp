#include "ListItemWidget.h"

#include <QDateTime>
#include <QDesktopServices>
#include <QFileInfo>
#include <QUrl>

#include "ui_ListItemWidget.h"

namespace DataExporter::UI {
	struct ListItemWidget::Impl {
		Ui::ListItemWidget ui;

		QString tcf;
		QString save;
	};

	ListItemWidget::ListItemWidget(QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);

		d->ui.datetimeLabel->setText(QDateTime::currentDateTime().toString("HH:mm:ss"));

		d->ui.titleLabel->clear();
		d->ui.messageLabel->clear();

		d->ui.datetimeLabel->setVisible(false);
		d->ui.progressBar->setVisible(false);
		d->ui.tcfBtn->setVisible(false);
		d->ui.openBtn->setVisible(false);
		d->ui.cancelBtn->setVisible(false);

		d->ui.okIcon->setVisible(false);
		d->ui.errorIcon->setVisible(false);

		connect(d->ui.tcfBtn, &QPushButton::clicked, this, &ListItemWidget::OnTcfBtnClicked);
		connect(d->ui.openBtn, &QPushButton::clicked, this, &ListItemWidget::OnOpenBtnClicked);
		connect(d->ui.cancelBtn, &QPushButton::clicked, this, &ListItemWidget::OnCancelBtnClicked);
	}

	ListItemWidget::~ListItemWidget() = default;

	auto ListItemWidget::GetTCFPath() const -> QString {
		return d->tcf;
	}

	auto ListItemWidget::GetSavePath() const -> QString {
		return d->save;
	}

	auto ListItemWidget::GetMessage() const -> QString {
		return d->ui.messageLabel->text();
	}

	auto ListItemWidget::GetProgress() const -> int {
		return d->ui.progressBar->value();
	}

	auto ListItemWidget::SetTCFPath(const QString& text) -> void {
		d->tcf = text;

		d->ui.titleLabel->setText(QFileInfo(text).completeBaseName());
	}

	auto ListItemWidget::SetSavePath(const QString& text) -> void {
		d->save = text;
	}

	auto ListItemWidget::SetMessage(const QString& text) -> void {
		d->ui.messageLabel->setVisible(!text.isEmpty());
		d->ui.messageLabel->setText(text);
	}

	auto ListItemWidget::SetProgress(int value) -> void {
		d->ui.progressBar->setVisible(value > 0);
		d->ui.progressBar->setValue(value);
	}

	auto ListItemWidget::ShowTCFIcon() -> void {
		d->ui.tcfIconFrame->setVisible(true);
		d->ui.okIcon->setVisible(false);
		d->ui.errorIcon->setVisible(false);
	}

	auto ListItemWidget::ShowOkIcon() -> void {
		d->ui.tcfIconFrame->setVisible(false);
		d->ui.okIcon->setVisible(true);
		d->ui.errorIcon->setVisible(false);
	}

	auto ListItemWidget::ShowErrorIcon() -> void {
		d->ui.tcfIconFrame->setVisible(false);
		d->ui.okIcon->setVisible(false);
		d->ui.errorIcon->setVisible(true);
	}

	auto ListItemWidget::enterEvent(QEvent* event) -> void {
		QWidget::enterEvent(event);

		if (const QFileInfo info(d->tcf); info.isFile() && info.exists())
			d->ui.tcfBtn->setVisible(true);

		if (const QFileInfo info(d->save); info.isDir() && info.exists())
			d->ui.openBtn->setVisible(true);

		if (d->ui.okIcon->isVisible() || d->ui.errorIcon->isVisible())
			d->ui.cancelBtn->setVisible(true);

		d->ui.datetimeLabel->setVisible(true);
	}

	auto ListItemWidget::leaveEvent(QEvent* event) -> void {
		QWidget::leaveEvent(event);

		d->ui.tcfBtn->setVisible(false);
		d->ui.openBtn->setVisible(false);
		d->ui.cancelBtn->setVisible(false);
		d->ui.datetimeLabel->setVisible(false);
	}

	auto ListItemWidget::OnTcfBtnClicked() -> void {
		if (const QFileInfo info(d->tcf); info.isFile() && info.exists())
			QDesktopServices::openUrl(QUrl::fromLocalFile(info.absolutePath()));
	}

	auto ListItemWidget::OnOpenBtnClicked() -> void {
		if (const QFileInfo info(d->save); info.isDir() && info.exists())
			QDesktopServices::openUrl(QUrl::fromLocalFile(info.absoluteFilePath()));
	}

	auto ListItemWidget::OnCancelBtnClicked() -> void {
		emit Removed();
	}
}
