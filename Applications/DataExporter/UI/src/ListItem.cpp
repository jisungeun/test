#include "ListItem.h"
#include "ListItemWidget.h"

namespace DataExporter::UI {
	struct ListItem::Impl {
		ListItemWidget* widget = nullptr;

		QString tcf;
		QString save;
		QString format;
		QString message;
		TaskStatus status = TaskStatus::Pending;
	};

	ListItem::ListItem(QListWidget* parent) : QListWidgetItem(parent), d(new Impl) {
		d->widget = new ListItemWidget(parent);

		parent->setItemWidget(this, d->widget);

		connect(d->widget, &ListItemWidget::Removed, this, &ListItem::Removed);
	}

	ListItem::ListItem(QListWidget* parent, int index) : QListWidgetItem(), d(new Impl) {
		d->widget = new ListItemWidget(parent);

		parent->insertItem(index, this);
		parent->setItemWidget(this, d->widget);

		connect(d->widget, &ListItemWidget::Removed, this, &ListItem::Removed);
	}

	ListItem::~ListItem() = default;

	auto ListItem::SetTCFPath(const QString& path) -> void {
		d->tcf = path;
		d->widget->SetTCFPath(path);
	}

	auto ListItem::SetSavePath(const QString& path) -> void {
		d->save = path;
		d->widget->SetSavePath(path);
	}

	auto ListItem::SetFormat(const QString& format) -> void {
		d->format = format;
	}

	auto ListItem::SetProgress(int progress) -> void {
		d->widget->SetProgress(progress);

		if (d->message.isEmpty())
			d->widget->SetMessage({});
	}

	auto ListItem::SetStatus(TaskStatus status) -> void {
		d->status = status;

		switch (status) {
			case TaskStatus::Pending:
				if (d->message.isEmpty())
					d->widget->SetMessage("Pending...");

				d->widget->ShowTCFIcon();
				break;
			case TaskStatus::Progressing:
				if (d->message.isEmpty())
					d->widget->SetMessage("Preparing...");

				d->widget->ShowTCFIcon();
				break;
			case TaskStatus::Finished:
				if (d->message.isEmpty())
					d->widget->SetMessage("Finished");

				if (d->message.isEmpty())
					d->widget->ShowOkIcon();
				else
					d->widget->ShowErrorIcon();
				break;
		}
	}

	auto ListItem::SetMessage(const QString& message) -> void {
		d->message = message;

		if (!d->message.isEmpty())
			d->widget->SetMessage(message);
	}

	auto ListItem::GetTCFPath() const -> QString {
		return d->tcf;
	}

	auto ListItem::GetSavePath() const -> QString {
		return d->save;
	}

	auto ListItem::GetFormat() const -> QString {
		return d->format;
	}

	auto ListItem::GetProgress() const -> int {
		return d->widget->GetProgress();
	}

	auto ListItem::GetStatus() const -> TaskStatus {
		return d->status;
	}

	auto ListItem::GetMessage() const -> QString {
		return d->message;
	}
}
