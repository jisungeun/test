#include "AppTrayIcon.h"

#include <QApplication>
#include <QMenu>
#include <QScreen>
#include <QVariantAnimation>

namespace DataExporter::UI {
	namespace {
		constexpr QSize Margin = { 15, 15 };
	}

	struct AppTrayIcon::Impl {
		QVariantAnimation* anime = nullptr;
		QMenu* menu = nullptr;

		bool animating = false;
		bool show = false;
		int height = 600;
		int width = 400;
	};

	AppTrayIcon::AppTrayIcon(QWidget* parent) : QSystemTrayIcon(parent), d(new Impl) {
		d->anime = new QVariantAnimation(this);
		d->menu = new QMenu(parent);

		const auto* show = d->menu->addAction("Show");
		d->menu->addSeparator();
		const auto* quit = d->menu->addAction("Quit");

		setContextMenu(d->menu);
		setIcon(QIcon(":/System/Icon.svg"));
		setToolTip("Data Exporter");
		this->show();

		connect(this, &QSystemTrayIcon::activated, this, &AppTrayIcon::OnTrayActivated);
		connect(show, &QAction::triggered, this, &AppTrayIcon::OnShowClicked);
		connect(quit, &QAction::triggered, this, &AppTrayIcon::OnQuitClicked);
		connect(d->anime, &QVariantAnimation::valueChanged, this, &AppTrayIcon::OnAnimeValueChanged);
		connect(d->anime, &QVariantAnimation::finished, this, &AppTrayIcon::OnAnimeFinished);
	}

	AppTrayIcon::~AppTrayIcon() = default;

	auto AppTrayIcon::SetSize(const QSize& size) -> void {
		d->height = size.height();
		d->width = size.width();

		if (auto* parent = dynamic_cast<QWidget*>(this->parent())) {
			if (const auto* screen = QApplication::primaryScreen()) {
				const auto pos = screen->availableSize() - QSize(d->width, d->height) - Margin;

				parent->setGeometry(pos.width(), pos.height(), d->width, d->height);
			}
		}
	}

	auto AppTrayIcon::ShowWindow() -> void {
		if (d->anime->state() != QAbstractAnimation::Running) {
			if (auto* parent = dynamic_cast<QWidget*>(this->parent())) {
				d->animating = true;
				d->show = true;

				d->anime->setStartValue(parent->minimumHeight());
				d->anime->setEndValue(d->height);
				d->anime->setDuration(300);
				d->anime->setEasingCurve(QEasingCurve::OutCubic);
				d->anime->start();

				parent->show();
			}
		}
	}

	auto AppTrayIcon::HideWindow() -> void {
		if (d->anime->state() != QAbstractAnimation::Running) {
			if (const auto* parent = dynamic_cast<QWidget*>(this->parent())) {
				d->animating = true;
				d->show = false;

				d->anime->setStartValue(d->height);
				d->anime->setEndValue(parent->minimumHeight());
				d->anime->setEasingCurve(QEasingCurve::InCubic);
				d->anime->setDuration(200);
				d->anime->start();
			}
		}
	}

	auto AppTrayIcon::IsAnimating() const -> bool {
		return d->animating;
	}

	auto AppTrayIcon::OnTrayActivated(ActivationReason reason) -> void {
		if (reason == DoubleClick)
			ShowWindow();
	}

	auto AppTrayIcon::OnShowClicked() -> void {
		ShowWindow();
	}

	auto AppTrayIcon::OnQuitClicked() -> void {
		QApplication::exit();
	}

	auto AppTrayIcon::OnAnimeValueChanged(const QVariant& value) -> void {
		if (auto* parent = dynamic_cast<QWidget*>(this->parent())) {
			if (const auto* screen = QApplication::primaryScreen()) {
				const auto pos = screen->availableSize() - QSize(d->width, value.toInt()) - Margin;

				parent->setGeometry(pos.width(), pos.height(), d->width, value.toInt());
			}
		}
	}

	auto AppTrayIcon::OnAnimeFinished() -> void {
		d->animating = false;

		if (auto* parent = dynamic_cast<QWidget*>(this->parent()); parent && !d->show)
			parent->hide();
	}
}
