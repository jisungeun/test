#include "AppMainWindow.h"

#include <QFontDatabase>
#include <QMenu>

#include <QWKWidgets/WidgetWindowAgent.h>

#include "IDataHandler.h"
#include "IScheduler.h"

#include "AppTrayIcon.h"
#include "ListItem.h"

#include "ui_AppMainWindow.h"

namespace DataExporter::UI {
	struct AppMainWindow::Impl {
		Ui::AppMainWindow ui;
		EventContainer events;
		IServiceProvider* provider = nullptr;
		AppTrayIcon* animator = nullptr;
		QMenu* menu = nullptr;

		QAction* showAction = nullptr;
		QAction* clearAction = nullptr;
		QAction* quitAction = nullptr;
		QAction* countAction = nullptr;

		QMap<ITask*, ListItem*> taskMap;
		QMap<IData*, ListItem*> dataMap;

		static auto SetStylesheet(QWidget* parent) -> void {
			if (QFile file(":/Stylesheet/DataExporter.qss", parent); file.open(QIODevice::ReadOnly))
				parent->setStyleSheet(file.readAll());
		}

		static auto SetFrameless(QWidget* parent) -> void {
			if (auto* agent = new QWK::WidgetWindowAgent(parent))
				agent->setup(parent);
		}

		static auto SetFontFamily() -> void {
			for (const auto& i : { "Black", "Bold", "ExtraBold", "ExtraLight", "Light", "Medium", "Regular", "SemiBold", "Thin" }) {
				if (QFile file(QString(":/Font/NotoSansKR-%1.ttf").arg(i)); file.open(QIODevice::ReadOnly))
					QFontDatabase::addApplicationFontFromData(file.readAll());
			}

			auto font = QFontDatabase().font("Noto Sans KR", "Regular", 9);
			font.setHintingPreference(QFont::PreferNoHinting);
			font.setStyleStrategy(QFont::PreferQuality);
			font.setKerning(true);

			QApplication::setFont(font);
		}

		static auto SetListHeight(QListWidget* widget) -> void {
			auto height = 0;

			for (auto i = 0; i < widget->count(); i++)
				height += widget->sizeHintForRow(i);

			widget->setVisible(height > 0);
			widget->setFixedHeight(height);
		}

		auto GetTaskIndex() const -> int {
			auto idx = 0;

			for (auto i = 0; i < ui.pendList->count(); i++) {
				if (const auto* item = dynamic_cast<ListItem*>(ui.pendList->item(i))) {
					if (item->GetStatus() == TaskStatus::Pending)
						idx = i;
					else
						break;
				}
			}

			return idx;
		}
	};

	AppMainWindow::AppMainWindow(IServiceProvider* provider) : IAppWindow(), QMainWindow(nullptr, Qt::Window | Qt::WindowStaysOnTopHint), d(new Impl) {
		d->ui.setupUi(this);
		d->provider = provider;
		d->animator = new AppTrayIcon(this);
		d->menu = new QMenu(this);

		d->showAction = d->menu->addAction("Show Always");
		d->showAction->setCheckable(true);

		d->clearAction = d->menu->addAction("Clear Finished");
		d->countAction = d->menu->addAction("Current Count: 0");
		d->countAction->setEnabled(false);
		d->menu->addSeparator();
		d->quitAction = d->menu->addAction("Quit");

		d->SetStylesheet(this);
		d->SetFrameless(this);
		d->SetFontFamily();
		d->SetListHeight(d->ui.taskList);

		connect(qApp, &QGuiApplication::applicationStateChanged, this, &AppMainWindow::OnAppStateChanged);
		connect(d->clearAction, &QAction::triggered, this, &AppMainWindow::OnClearBtnClicked);
		connect(d->quitAction, &QAction::triggered, this, &AppMainWindow::OnQuitBtnClicked);
		connect(d->ui.optionBtn, &QPushButton::clicked, this, &AppMainWindow::OnOptionBtnClicked);
		connect(d->ui.hideBtn, &QPushButton::clicked, this, &AppMainWindow::OnHideBtnClicked);
	}

	AppMainWindow::~AppMainWindow() = default;

	auto AppMainWindow::Start(const QStringList& args) -> bool {
		if (const auto scheduler = d->provider->GetService<IScheduler>()) {
			scheduler->OnScheduled += { d->events, this, &AppMainWindow::OnScheduled };
			scheduler->OnNextTask += { d->events, this, &AppMainWindow::OnNextTask };
		}

		if (const auto handler = d->provider->GetService<IDataHandler>()) {
			handler->OnStarted += { d->events, this, &AppMainWindow::OnStarted };
			handler->OnUpdated += { d->events, this, &AppMainWindow::OnUpdated };
			handler->OnFinished += { d->events, this, &AppMainWindow::OnFinished };
			handler->OnCleared += { d->events, this, &AppMainWindow::OnCleared };
		}

		d->animator->ShowWindow();

		return true;
	}

	auto AppMainWindow::Stop() -> void {}

	auto AppMainWindow::GetTaskItemList() const -> QList<ITaskItem*> {
		return {};
	}

	auto AppMainWindow::closeEvent(QCloseEvent* event) -> void {
		d->animator->HideWindow();
		event->ignore();
	}

	auto AppMainWindow::resizeEvent(QResizeEvent* event) -> void {
		QMainWindow::resizeEvent(event);

		if (isVisible() && !d->animator->IsAnimating())
			d->animator->SetSize(event->size());
	}

	auto AppMainWindow::OnScheduled(IService* sender, ITask* task) -> void {
		auto* item = new ListItem(d->ui.pendList, d->GetTaskIndex());
		item->SetTCFPath(task->GetTCFPath());
		item->SetSavePath(task->GetSavePath());
		item->SetFormat(task->GetSaveFormat());
		item->SetStatus(TaskStatus::Pending);

		d->taskMap[task] = item;
		d->countAction->setText(QString("Current Count: %1").arg(d->ui.pendList->count() + d->ui.taskList->count()));
	}

	auto AppMainWindow::OnNextTask(IService* sender, ITask* task) -> void {
		if (d->taskMap.contains(task))
			delete d->taskMap.take(task);
	}

	auto AppMainWindow::OnStarted(IService* sender, IData* data) -> void {
		auto* item = new ListItem(d->ui.taskList);
		item->SetTCFPath(data->GetTCFPath());
		item->SetSavePath(data->GetSavePath());
		item->SetFormat(data->GetSaveFormat());
		item->SetStatus(TaskStatus::Progressing);

		d->dataMap[data] = item;
		d->countAction->setText(QString("Current Count: %1").arg(d->ui.pendList->count() + d->ui.taskList->count()));
		d->SetListHeight(d->ui.taskList);
	}

	auto AppMainWindow::OnUpdated(IService* sender, IData* data) -> void {
		if (d->dataMap.contains(data)) {
			auto* item = d->dataMap[data];

			item->SetMessage(data->GetMessage());
			item->SetProgress(data->GetProgress());
		}
	}

	auto AppMainWindow::OnFinished(IService* sender, IData* data) -> void {
		if (d->dataMap.contains(data)) {
			if (const auto row = d->ui.pendList->row(d->dataMap[data]); row > -1)
				d->ui.pendList->takeItem(row);

			delete d->dataMap.take(data);
			d->SetListHeight(d->ui.taskList);
		}

		auto* item = new ListItem(d->ui.pendList);
		item->SetTCFPath(data->GetTCFPath());
		item->SetSavePath(data->GetSavePath());
		item->SetFormat(data->GetSaveFormat());
		item->SetMessage(data->GetError());
		item->SetStatus(TaskStatus::Finished);

		connect(item, &ListItem::Removed, this, &AppMainWindow::OnItemRemoved);
		d->countAction->setText(QString("Current Count: %1").arg(d->ui.pendList->count() + d->ui.taskList->count()));
	}

	auto AppMainWindow::OnCleared(IService* sender) -> void {
		close();
		QApplication::exit();
	}

	auto AppMainWindow::OnAppStateChanged(Qt::ApplicationState state) -> void {
		if (QApplication::activeWindow() == this && !d->showAction->isChecked() && state == Qt::ApplicationInactive)
			OnHideBtnClicked();
	}

	auto AppMainWindow::OnOptionBtnClicked() -> void {
		const auto width = d->ui.optionBtn->width() - d->menu->sizeHint().width();
		const auto height = d->ui.optionBtn->height();

		d->menu->exec(d->ui.optionBtn->mapToGlobal({ width, height }));
	}

	auto AppMainWindow::OnHideBtnClicked() -> void {
		d->animator->HideWindow();
	}

	auto AppMainWindow::OnItemRemoved() -> void {
		if (auto* item = dynamic_cast<ListItem*>(sender())) {
			if (item->GetStatus() == TaskStatus::Finished)
				item->deleteLater();
		}

		Run([this] {
			d->countAction->setText(QString("Current Count: %1").arg(d->ui.pendList->count() + d->ui.taskList->count()));
		}, false);
	}

	auto AppMainWindow::OnClearBtnClicked() -> void {
		for (auto i = 0; i < d->ui.pendList->count(); i++) {
			if (auto* item = dynamic_cast<ListItem*>(d->ui.pendList->item(i))) {
				if (item->GetStatus() == TaskStatus::Finished)
					item->deleteLater();
			}
		}

		Run([this] {
			d->countAction->setText(QString("Current Count: %1").arg(d->ui.pendList->count() + d->ui.taskList->count()));
		}, false);
	}

	auto AppMainWindow::OnQuitBtnClicked() -> void {
		QApplication::exit();
	}
}
