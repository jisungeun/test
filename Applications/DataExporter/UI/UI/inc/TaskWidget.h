#pragma once

#include <QWidget>

#include "ITask.h"

#include "DataExporter.UIExport.h"

namespace DataExporter::UI {
	class DataExporter_UI_API TaskWidget final : public QWidget {
		Q_OBJECT

	public:
		explicit TaskWidget(const TaskPtr& task, QWidget* parent = nullptr);
		~TaskWidget() override;

		auto IsRemovable() const -> bool;
		auto Update() -> void;
		
	protected slots:
		auto OnOptionBtnClicked() -> void;
		auto OnCustomContextMenuRequested(const QPoint& pos) -> void;
		auto OnRemoveActionTriggered() -> void;
		auto OnOpenTcfActionTriggered() -> void;
		auto OnOpenOutputActionTriggered() -> void;

	protected:
		auto enterEvent(QEvent* event) -> void override;
		auto leaveEvent(QEvent* event) -> void override;

	signals:
		auto RemoveRequested() -> void;
		
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
