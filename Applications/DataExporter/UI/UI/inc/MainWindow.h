#pragma once

#include <QMainWindow>
#include <QSystemTrayIcon>

#include "IServiceProvider.h"

#include "IAppModule.h"
#include "ITaskView.h"

#include "DataExporter.UIExport.h"

namespace DataExporter::UI {
	class DataExporter_UI_API MainWindow final : public QMainWindow, public IAppModule, public ITaskView {
	public:
		explicit MainWindow(Tomocube::IServiceProvider * provider);
		~MainWindow() override;

		auto Start() -> void override;
		auto Stop() -> void override;

		auto Show() -> void override;

		auto AddTask(const TaskPtr& task) -> void override;
		auto UpdateTask(const TaskPtr& task) -> void override;
		auto UpdateCount() -> void;

	protected:
		auto closeEvent(QCloseEvent* event) -> void override;

	protected slots:
		auto OnAppStateChanged(Qt::ApplicationState state) -> void;
		auto OnTrayActivated(QSystemTrayIcon::ActivationReason reason) -> void;

		auto OnAnimeValueChanged(const QVariant& value) -> void;
		auto OnAnimeFinished() -> void;

		auto OnShowActionTriggered() -> void;
		auto OnQuitActionTriggered() -> void;
		auto OnAlwaysOnTopActionTriggered(bool checked) -> void;
		auto OnClearActionTriggered() -> void;

		auto OnSettingsBtnClicked() -> void;
		auto OnHideBtnClicked() -> void;
	
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
