#include <QDir>
#include <QProcess>
#include <QMenu>

#include "TaskWidget.h"

#include "ui_TaskWidget.h"

namespace DataExporter::UI {
	struct TaskWidget::Impl {
		Ui::TaskWidget ui;

		TaskPtr task = nullptr;
		static auto ParseTitle(const QString& url)->QString;
	};

	auto TaskWidget::Impl::ParseTitle(const QString& url) -> QString {
		if (const QFileInfo info(url); info.isFile())
			return info.completeBaseName();
		else if (info.isDir())
			return info.fileName();

		return url;
	}
	
	TaskWidget::TaskWidget(const TaskPtr& task, QWidget* parent) : QWidget(parent), d(new Impl) {
		d->ui.setupUi(this);
		d->ui.optionBtn->setVisible(false);
		d->task = task;

		connect(d->ui.optionBtn, &QPushButton::clicked, this, &TaskWidget::OnOptionBtnClicked);
		connect(this, &QWidget::customContextMenuRequested, this, &TaskWidget::OnCustomContextMenuRequested);

		Update();
	}

	TaskWidget::~TaskWidget() = default;

	auto TaskWidget::IsRemovable() const -> bool {
		switch (d->task->GetStatus()) {
			case TaskStatus::Completed:
			case TaskStatus::Failed:
				return true;
			default:
				return false;
		}
	}

	auto TaskWidget::Update() -> void {
		const auto message = d->task->GetErrorMessage().value_or(QString());
		const auto source = d->ParseTitle(d->task->GetSource());
		const auto progress = d->task->GetProgress();

		d->ui.titleLine->setText(source);
		d->ui.titleLine->setCursorPosition(0);

		switch (d->task->GetStatus()) {
			case TaskStatus::Pending:
				d->ui.progressBar->setVisible(false);
				d->ui.messageLabel->setText("Pending...");
				d->ui.icon->setPixmap(QIcon(":/Icon/TCFPending.svg").pixmap(d->ui.icon->size()));
				break;
			case TaskStatus::Running:
				d->ui.progressBar->setVisible(true);
				d->ui.progressBar->setValue(progress);
				d->ui.messageLabel->setText(QString("%1%").arg(progress));
				d->ui.icon->setPixmap(QIcon(":/Icon/TCFPending.svg").pixmap(d->ui.icon->size()));
				break;
			case TaskStatus::Completed:
				d->ui.progressBar->setVisible(false);
				d->ui.messageLabel->setText("Completed");
				d->ui.icon->setPixmap(QIcon(":/Icon/TCFFinished.svg").pixmap(d->ui.icon->size()));
				break;
			case TaskStatus::Failed:
				d->ui.progressBar->setVisible(false);
				d->ui.messageLabel->setText(QString("Failed (%1)").arg(message));
				d->ui.icon->setPixmap(QIcon(":/Icon/TCFError.svg").pixmap(d->ui.icon->size()));
				break;
		}

		update();
	}

	auto TaskWidget::OnOptionBtnClicked() -> void {
		OnCustomContextMenuRequested({});
	}

	auto TaskWidget::OnCustomContextMenuRequested(const QPoint& pos) -> void {
		QMenu menu(this);

		auto* remove = menu.addAction("Remove From List");
		auto* tcf = menu.addAction("Open TCF Folder");
		auto* output = menu.addAction("Open Output Folder");

		remove->setEnabled(IsRemovable());
		tcf->setEnabled(QFileInfo::exists(d->task->GetSource()));
		output->setEnabled(QFileInfo::exists(d->task->GetDestination()));

		connect(remove, &QAction::triggered, this, &TaskWidget::OnRemoveActionTriggered);
		connect(tcf, &QAction::triggered, this, &TaskWidget::OnOpenTcfActionTriggered);
		connect(output, &QAction::triggered, this, &TaskWidget::OnOpenOutputActionTriggered);
		
		menu.exec(d->ui.optionBtn->mapToGlobal({ d->ui.optionBtn->width() - menu.sizeHint().width(), d->ui.optionBtn->height() }));
		update();
	}

	auto TaskWidget::OnRemoveActionTriggered() -> void {
		emit RemoveRequested();
	}

	auto TaskWidget::OnOpenTcfActionTriggered() -> void {
		auto* process = new QProcess(this);
		connect(process, qOverload<int, QProcess::ExitStatus>(&QProcess::finished), this, [process] { delete process; });

		process->start("explorer.exe", { "/select", ",", QDir::toNativeSeparators(d->task->GetSource()) });
	}

	auto TaskWidget::OnOpenOutputActionTriggered() -> void {
		auto* process = new QProcess(this);
		connect(process, qOverload<int, QProcess::ExitStatus>(&QProcess::finished), this, [process] { delete process; });

		process->start("explorer.exe", { QDir::toNativeSeparators(d->task->GetDestination()) });
	}

	auto TaskWidget::enterEvent(QEvent* event) -> void {
		QWidget::enterEvent(event);

		d->ui.optionBtn->setVisible(true);
	}

	auto TaskWidget::leaveEvent(QEvent* event) -> void {
		QWidget::leaveEvent(event);
		
		d->ui.optionBtn->setVisible(false);
		update();
	}
}