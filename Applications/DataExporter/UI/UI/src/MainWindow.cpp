#include <QMenu>
#include <QCloseEvent>
#include <QGraphicsEffect>
#include <QScreen>
#include <QVariantAnimation>

#include "MainWindow.h"

#include "IScheduler.h"

#include "AddTask.h"
#include "UpdateTask.h"
#include "TaskWidget.h"

#include "ui_MainWindow.h"

namespace DataExporter::UI {
	struct MainWindow::Impl {
		Ui::MainWindow ui;
		Tomocube::IServiceProvider* provider = nullptr;

		std::shared_ptr<QSystemTrayIcon> tray = nullptr;
		QMap<TaskPtr, std::shared_ptr<TaskWidget>> taskMap;

		QVariantAnimation anime;
		QGraphicsDropShadowEffect shadow;

		int height = 0;
		bool animating = false;
		bool collapsing = false;
		bool close = false;
		bool alwaysOnTop = false;

		static auto SetStylesheet(QWidget* parent) -> void;
	};

	auto MainWindow::Impl::SetStylesheet(QWidget* parent) -> void {
		if (QFile file(":/Stylesheet/DataExporter.qss", parent); file.open(QIODevice::ReadOnly))
			parent->setStyleSheet(file.readAll());
	}

	MainWindow::MainWindow(Tomocube::IServiceProvider* provider) : QMainWindow(nullptr, Qt::Window | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint), IAppModule(), ITaskView(), d(new Impl) {
		d->ui.setupUi(this);
		d->provider = provider;
	}

	MainWindow::~MainWindow() = default;

	auto MainWindow::Start() -> void {
		const auto scheduler = d->provider->GetService<IScheduler>();
		scheduler->AddEvent(std::make_shared<UI::AddTask>(d->provider));
		scheduler->AddEvent(std::make_shared<UI::UpdateTask>(d->provider));
	}

	auto MainWindow::Stop() -> void {}

	auto MainWindow::Show() -> void {
		this->setAttribute(Qt::WA_TranslucentBackground, true);
		d->SetStylesheet(this);
		d->ui.backFrame->setGraphicsEffect(&d->shadow);
		d->shadow.setBlurRadius(8);
		d->shadow.setColor(Qt::black);
		d->shadow.setOffset(0);
		d->tray = std::make_shared<QSystemTrayIcon>(this);
		d->tray->setContextMenu(new QMenu(this));
		d->tray->setIcon(QIcon(":/Icon/Icon.svg"));
		d->tray->setToolTip("Data Exporter");
		d->tray->show();
		d->height = this->height();

		const auto* show = d->tray->contextMenu()->addAction("Show");
		const auto* quit = d->tray->contextMenu()->addAction("Quit");

		connect(show, &QAction::triggered, this, &MainWindow::OnShowActionTriggered);
		connect(quit, &QAction::triggered, this, &MainWindow::OnQuitActionTriggered);

		connect(qApp, &QGuiApplication::applicationStateChanged, this, &MainWindow::OnAppStateChanged);
		connect(d->tray.get(), &QSystemTrayIcon::activated, this, &MainWindow::OnTrayActivated);
		connect(&d->anime, &QVariantAnimation::valueChanged, this, &MainWindow::OnAnimeValueChanged);
		connect(&d->anime, &QVariantAnimation::finished, this, &MainWindow::OnAnimeFinished);
		connect(d->ui.settingsBtn, &QPushButton::clicked, this, &MainWindow::OnSettingsBtnClicked);
		connect(d->ui.hideBtn, &QPushButton::clicked, this, &MainWindow::OnHideBtnClicked);
	}

	auto MainWindow::AddTask(const TaskPtr& task) -> void {
		const auto widget = std::make_shared<TaskWidget>(task, this);
		d->taskMap[task] = widget;
		d->ui.pendingLayout->addWidget(widget.get());

		connect(widget.get(), &TaskWidget::RemoveRequested, this, [this, task] {
			d->taskMap.remove(task);
		}, Qt::QueuedConnection);

		UpdateCount();
	}

	auto MainWindow::UpdateTask(const TaskPtr& task) -> void {
		if (d->taskMap.contains(task)) {
			d->taskMap[task]->Update();

			switch (task->GetStatus()) {
				case TaskStatus::Pending:
					if (d->taskMap[task]->parent() != d->ui.pendingLayout)
						d->ui.pendingLayout->addWidget(d->taskMap[task].get());
					break;
				case TaskStatus::Running:
					if (d->taskMap[task]->parent() != d->ui.currentLayout)
						d->ui.currentLayout->addWidget(d->taskMap[task].get());
					break;
				case TaskStatus::Completed:
				case TaskStatus::Failed:
					if (d->taskMap[task]->parent() != d->ui.finishedLayout)
						d->ui.finishedLayout->insertWidget(0, d->taskMap[task].get());
					break;
				default: ;
			}

			UpdateCount();
		}
	}

	auto MainWindow::UpdateCount() -> void {
		int finished = 0;
		int error = 0;
		int pending = 0;

		for (const auto& t : d->taskMap.keys()) {
			switch (t->GetStatus()) {
				case TaskStatus::Pending:
				case TaskStatus::Running:
					pending++;
					break;
				case TaskStatus::Completed:
					finished++;
					break;
				case TaskStatus::Failed:
					error++;
					break;
				default: ;
			}
		}

		d->ui.allCount->setText(QString::number(d->taskMap.count()));
		d->ui.finishedCount->setText(QString::number(finished));
		d->ui.errorCount->setText(QString::number(error));
		d->ui.pendingCount->setText(QString::number(pending));
	}

	auto MainWindow::closeEvent(QCloseEvent* event) -> void {
		if (!d->close) {
			OnHideBtnClicked();
			event->ignore();
		}
	}

	auto MainWindow::OnAppStateChanged(Qt::ApplicationState state) -> void {
		if (QApplication::activeWindow() == this && !d->alwaysOnTop && state == Qt::ApplicationInactive)
			OnHideBtnClicked();
	}

	auto MainWindow::OnTrayActivated(QSystemTrayIcon::ActivationReason reason) -> void {
		if (reason == QSystemTrayIcon::DoubleClick)
			OnShowActionTriggered();
	}

	auto MainWindow::OnAnimeValueChanged(const QVariant& value) -> void {
		const auto width = this->size().width();
		const auto pos = QApplication::primaryScreen()->availableSize() - QSize(width, value.toInt()) - QSize(15, 15);

		this->setGeometry(pos.width(), pos.height(), width, value.toInt());
	}

	auto MainWindow::OnAnimeFinished() -> void {
		d->animating = false;

		if (d->collapsing)
			this->hide();
	}

	auto MainWindow::OnShowActionTriggered() -> void {
		if (!d->animating && !this->isVisible()) {
			d->animating = true;
			d->collapsing = false;

			d->anime.setStartValue(1);
			d->anime.setEndValue(d->height);
			d->anime.setDuration(300);
			d->anime.setEasingCurve(QEasingCurve::OutCubic);
			d->anime.start();

			this->show();
			this->raise();
		}
	}

	auto MainWindow::OnQuitActionTriggered() -> void {
		d->close = true;
		this->close();
	}

	auto MainWindow::OnAlwaysOnTopActionTriggered(bool checked) -> void {
		d->alwaysOnTop = checked;
	}

	auto MainWindow::OnClearActionTriggered() -> void {
		QVector<std::shared_ptr<TaskWidget>> removeList;

		for (const auto& key : d->taskMap.keys()) {
			if (d->taskMap[key]->IsRemovable())
				d->taskMap.remove(key);
		}
	}

	auto MainWindow::OnSettingsBtnClicked() -> void {
		QMenu menu(this);
		auto* always = menu.addAction("Always On Top");
		const auto* clear = menu.addAction("Clear Tasks");
		menu.addSeparator();
		const auto* quit = menu.addAction("Quit");

		always->setCheckable(true);
		always->setChecked(d->alwaysOnTop);

		connect(always, &QAction::triggered, this, &MainWindow::OnAlwaysOnTopActionTriggered);
		connect(clear, &QAction::triggered, this, &MainWindow::OnClearActionTriggered);
		connect(quit, &QAction::triggered, this, &MainWindow::OnQuitActionTriggered);

		menu.exec(d->ui.settingsBtn->mapToGlobal({ d->ui.settingsBtn->width() - menu.sizeHint().width(), d->ui.settingsBtn->height() }));
	}

	auto MainWindow::OnHideBtnClicked() -> void {
		if (!d->animating && this->isVisible()) {
			d->animating = true;
			d->collapsing = true;

			d->anime.setStartValue(d->height);
			d->anime.setEndValue(0);
			d->anime.setEasingCurve(QEasingCurve::InCubic);
			d->anime.setDuration(200);
			d->anime.start();
		}
	}
}
