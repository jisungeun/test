#pragma once

#include "IServiceProvider.h"

#include "ISchedulerEvent.h"

#include "DataExporter.UI.UseCaseExport.h"

namespace DataExporter::UI {
	class DataExporter_UI_UseCase_API AddTask final : public ISchedulerEvent {
	public:
		explicit AddTask(Tomocube::IServiceProvider* provider);
		~AddTask() override;

		auto OnTaskSchedulled(const TaskPtr& task) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
