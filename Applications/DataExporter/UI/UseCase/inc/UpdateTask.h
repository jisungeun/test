#pragma once

#include "IServiceProvider.h"

#include "ISchedulerEvent.h"

#include "DataExporter.UI.UseCaseExport.h"

namespace DataExporter::UI {
	class DataExporter_UI_UseCase_API UpdateTask final : public ISchedulerEvent {
	public:
		explicit UpdateTask(Tomocube::IServiceProvider * provider);
		~UpdateTask() override;

		auto OnTaskUpdated(const TaskPtr& task) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
