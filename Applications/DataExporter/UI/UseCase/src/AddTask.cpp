#include "AddTask.h"

#include <QApplication>

#include "ITaskView.h"

namespace DataExporter::UI {
	struct AddTask::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
	};

	AddTask::AddTask(Tomocube::IServiceProvider* provider) : ISchedulerEvent(), d(new Impl) {
		d->provider = provider;
	}

	AddTask::~AddTask() = default;

	auto AddTask::OnTaskSchedulled(const TaskPtr& task) -> void {
		QMetaObject::invokeMethod(qApp, [this, task] {
			const auto view = d->provider->GetService<ITaskView>();
			view->AddTask(task);
		});
	}
}
