#include "UpdateTask.h"

#include <QApplication>

#include "ITaskView.h"

namespace DataExporter::UI {
	struct UpdateTask::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
	};

	UpdateTask::UpdateTask(Tomocube::IServiceProvider* provider) : ISchedulerEvent(), d(new Impl) {
		d->provider = provider;
	}

	UpdateTask::~UpdateTask() = default;

	auto UpdateTask::OnTaskUpdated(const TaskPtr& task) -> void {
		QMetaObject::invokeMethod(qApp, [this, task] {
			const auto view = d->provider->GetService<ITaskView>();
			view->UpdateTask(task);
		});
	}
}
