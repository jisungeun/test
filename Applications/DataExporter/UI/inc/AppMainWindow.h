#pragma once

#include <QListWidgetItem>
#include <QMainWindow>

#include "IAppWindow.h"
#include "IData.h"
#include "IServiceProvider.h"
#include "ITask.h"

#include "DataExporter.UIExport.h"

namespace DataExporter::UI {
	class DataExporter_UI_API AppMainWindow final : public IAppWindow, public QMainWindow {
		Implements(IAppWindow)

	public:
		explicit AppMainWindow(IServiceProvider* provider);
		~AppMainWindow() override;

		auto Start(const QStringList& args) -> bool override;
		auto Stop() -> void override;

		auto GetTaskItemList() const -> QList<ITaskItem*> override;

	protected:
		auto closeEvent(QCloseEvent* event) -> void override;
		auto resizeEvent(QResizeEvent* event) -> void override;

	protected slots:
		auto OnScheduled(IService* sender, ITask* task) -> void;
		auto OnNextTask(IService* sender, ITask* task) -> void;

		auto OnStarted(IService* sender, IData* data) -> void;
		auto OnUpdated(IService* sender, IData* data) -> void;
		auto OnFinished(IService* sender, IData* data) -> void;
		auto OnCleared(IService* sender) -> void;

		auto OnAppStateChanged(Qt::ApplicationState state) -> void;

		auto OnOptionBtnClicked() -> void;
		auto OnHideBtnClicked() -> void;

		auto OnItemRemoved() -> void;
		auto OnClearBtnClicked() -> void;
		auto OnQuitBtnClicked() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
