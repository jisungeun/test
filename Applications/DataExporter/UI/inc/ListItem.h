#pragma once

#include <QListWidgetItem>
#include <QObject>

#include "ITaskItem.h"

#include "DataExporter.UIExport.h"

namespace DataExporter::UI {
	class DataExporter_UI_API ListItem final : public QObject, public ITaskItem, public QListWidgetItem {
		Q_OBJECT

	public:
		explicit ListItem(QListWidget* parent);
		explicit ListItem(QListWidget* parent, int index);
		~ListItem() override;
		
		auto SetTCFPath(const QString& path) -> void;
		auto SetSavePath(const QString& path) -> void;
		auto SetFormat(const QString& format) -> void;
		
		auto SetProgress(int progress) -> void;
		auto SetStatus(TaskStatus status) -> void;
		auto SetMessage(const QString& message) -> void;

		auto GetTCFPath() const -> QString override;
		auto GetSavePath() const -> QString override;
		auto GetFormat() const -> QString override;

		auto GetProgress() const -> int override;
		auto GetStatus() const -> TaskStatus override;
		auto GetMessage() const -> QString override;

	signals:
		auto Removed() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
