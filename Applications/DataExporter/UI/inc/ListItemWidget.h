#pragma once

#include <QWidget>

#include "DataExporter.UIExport.h"

namespace DataExporter::UI {
	class DataExporter_UI_API ListItemWidget final : public QWidget {
		Q_OBJECT

	public:
		explicit ListItemWidget(QWidget* parent = nullptr);
		~ListItemWidget() override;

		auto GetTCFPath() const -> QString;
		auto GetSavePath() const -> QString;
		auto GetMessage() const -> QString;
		auto GetProgress() const -> int;

		auto SetTCFPath(const QString& text) -> void;
		auto SetSavePath(const QString& text) -> void;
		auto SetMessage(const QString& text) -> void;
		auto SetProgress(int value) -> void;

		auto ShowTCFIcon() -> void;
		auto ShowOkIcon() -> void;
		auto ShowErrorIcon() -> void;

	protected:
		auto enterEvent(QEvent* event) -> void override;
		auto leaveEvent(QEvent* event) -> void override;

	protected slots:
		auto OnTcfBtnClicked() -> void;
		auto OnOpenBtnClicked() -> void;
		auto OnCancelBtnClicked() -> void;

	signals:
		auto Removed() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
