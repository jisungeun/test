#pragma once

#include <QSystemTrayIcon>
#include <QWidget>

#include "DataExporter.UIExport.h"

namespace DataExporter::UI {
	class DataExporter_UI_API AppTrayIcon final : public QSystemTrayIcon {
	public:
		explicit AppTrayIcon(QWidget* parent = nullptr);
		~AppTrayIcon() override;

		auto SetSize(const QSize& size) -> void;

		auto ShowWindow() -> void;
		auto HideWindow() -> void;

		auto IsAnimating() const -> bool;

	protected slots:
		auto OnTrayActivated(ActivationReason reason) -> void;
		auto OnShowClicked() -> void;
		auto OnQuitClicked() -> void;
		auto OnAnimeValueChanged(const QVariant& value) -> void;
		auto OnAnimeFinished() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}