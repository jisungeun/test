#include "ServiceProvider.h"

namespace DataExporter::Component {
	struct ServiceProvider::Impl {
		QMap<ServiceId, QList<std::shared_ptr<IServiceBuilder>>> builderMap;
	};

	ServiceProvider::ServiceProvider() : IServiceProvider(), d(new Impl) {
		if (!instance)
			instance = this;
	}

	ServiceProvider::~ServiceProvider() = default;

	auto ServiceProvider::AddBuilder(ServiceId id, const std::shared_ptr<IServiceBuilder>& builder) -> void {
		builder->SetProvider(this);
		d->builderMap[id].push_back(builder);
	}

	auto ServiceProvider::GetBuilder(ServiceId id) -> IServiceBuilder* {
		if (d->builderMap.contains(id) && d->builderMap[id].count() > 0)
			return d->builderMap[id].first().get();

		return nullptr;
	}

	auto ServiceProvider::GetBuilderList(ServiceId id) -> QList<IServiceBuilder*> {
		if (d->builderMap.contains(id)) {
			QList<IServiceBuilder*> list;

			for (const auto& f : d->builderMap[id])
				list.push_back(f.get());

			return list;
		}

		return {};
	}
}
