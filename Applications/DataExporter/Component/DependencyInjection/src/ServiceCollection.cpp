#include "ServiceCollection.h"

namespace DataExporter::Component {
	auto ServiceCollection::Build() -> Service<IServiceProvider> {
		auto provider = std::make_shared<ServiceProvider>();
		QMap<Service<IServiceBuilder>, Service<IServiceBuilder>> copyMap;

		for (const auto& k : builderMap.keys()) {
			for (const auto& f : builderMap[k]) {
				if (!copyMap.contains(f))
					copyMap[f] = f->GetLifeCycle() == LifeCycle::Singleton ? f : f->Copy();

				provider->AddBuilder(k, copyMap[f]);
			}
		}

		return provider;
	}
}
