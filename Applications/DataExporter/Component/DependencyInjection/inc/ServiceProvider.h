#pragma once

#include <QMap>

#include "IServiceProvider.h"

#include "DataExporter.Component.DependencyInjectionExport.h"

namespace DataExporter::Component {
	class DataExporter_Component_DependencyInjection_API ServiceProvider final : public IServiceProvider {
		Implements(IServiceProvider)

	public:
		ServiceProvider();
		~ServiceProvider() override;

		auto AddBuilder(ServiceId id, const std::shared_ptr<IServiceBuilder>& builder) -> void;

	protected:
		auto GetBuilder(ServiceId id) -> IServiceBuilder* override;
		auto GetBuilderList(ServiceId id) -> QList<IServiceBuilder*> override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
