#pragma once

#include "IConfiguration.h"

#include "DataExporter.Component.ConfigurationExport.h"

namespace DataExporter::Component {
	class DataExporter_Component_Configuration_API Configuration final : public IConfiguration {
		Implements(IConfiguration)

	public:
		explicit Configuration(const QVariantMap& map);
		~Configuration() override;

		auto GetProperty(const QString& path) const -> QVariant override;
		auto GetPropertyList() const -> QStringList override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
