#pragma once

#include "IConfigBuilder.h"

#include "Configuration.h"

#include "DataExporter.Component.ConfigurationExport.h"

namespace DataExporter::Component {
	class DataExporter_Component_Configuration_API ConfigBuilder final : public IConfigBuilder {
		Implements(IConfigBuilder)

	public:
		ConfigBuilder();
		~ConfigBuilder() override;

		auto AddConfiguration(const Service<IConfiguration>& config) -> IConfigBuilder* override;
		auto AddJsonFile(const QString& path, bool optional) -> IConfigBuilder* override;
		auto AddRegistry(const QString& organization, const QString& application) -> IConfigBuilder* override;

		auto Build() -> Configuration* override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}