#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSettings>

#include "ConfigBuilder.h"

namespace DataExporter::Component {
	struct ConfigBuilder::Impl {
		QVariantMap map;
	};

	ConfigBuilder::ConfigBuilder() : IConfigBuilder(), d(new Impl) {}

	ConfigBuilder::~ConfigBuilder() = default;

	auto ConfigBuilder::AddConfiguration(const Service<IConfiguration>& config) -> IConfigBuilder* {
		for (const auto& k : config->GetPropertyList())
			d->map[k] = config->GetProperty(k);

		return this;
	}

	auto ConfigBuilder::AddJsonFile(const QString& path, bool optional) -> IConfigBuilder* {
		if (QFile file(path); file.open(QIODevice::ReadOnly)) {
			QJsonParseError error;
			const auto doc = QJsonDocument::fromJson(file.readAll(), &error);

			if (error.error == QJsonParseError::NoError || optional) {
				const auto object = doc.object();
				const auto map = object.toVariantMap();

				for (const auto& k : map.keys())
					d->map[k] = map[k];

				return this;
			}
		} else if (optional)
			return this;

		throw std::exception("Configuration file has not been found.");
	}

	auto ConfigBuilder::AddRegistry(const QString& organization, const QString& application) -> IConfigBuilder* {
		const QSettings settings(organization, application);

		for (const auto& k : settings.allKeys())
			d->map[k] = settings.value(k);

		return this;
	}

	auto ConfigBuilder::Build() -> Configuration* {
		return new Configuration(d->map);
	}
}
