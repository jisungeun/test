#include "Configuration.h"

namespace DataExporter::Component {
	struct Configuration::Impl {
		QVariantMap map;
	};

	Configuration::Configuration(const QVariantMap& map) : IConfiguration(), d(new Impl) {
		d->map = map;
	}

	Configuration::~Configuration() = default;

	auto Configuration::GetProperty(const QString& path) const -> QVariant {
		return d->map[path];
	}

	auto Configuration::GetPropertyList() const -> QStringList {
		return d->map.keys();
	}
}
