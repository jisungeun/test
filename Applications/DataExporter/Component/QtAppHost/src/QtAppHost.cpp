#include <QApplication>

#include "QtAppHost.h"

#include "IHostedService.h"

namespace DataExporter::Component {
	struct QtAppHost::Impl {
		Service<IServiceProvider> provider = nullptr;
	};

	QtAppHost::QtAppHost(const Service<IServiceProvider>& provider) : IHost(), d(new Impl) {
		d->provider = provider;
	}

	QtAppHost::~QtAppHost() = default;

	auto QtAppHost::Start(int argc, char** argv) -> int {
		QApplication app(argc, argv);
		QStringList args;

		for (auto i = 0; i < argc; i++)
			args.push_back(argv[i]);

		const auto services = d->provider->GetServiceList<IHostedService>();

		for (const auto& service : services) {
			if (!service->Start(args))
				return -1;
		}

		const auto result = QApplication::exec();

		std::for_each(services.crbegin(), services.crend(), [] (const Service<IHostedService>& service) {
			service->Stop();
		});

		return result;
	}

	auto QtAppHost::Stop() -> void {
		QApplication::exit();
	}
}
