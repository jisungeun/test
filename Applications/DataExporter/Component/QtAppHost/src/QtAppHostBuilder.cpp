#include "QtAppHostBuilder.h"
#include "QtAppHost.h"

#include "ConfigBuilder.h"
#include "LoggingBuilder.h"
#include "ServiceCollection.h"

namespace DataExporter::Component {
	struct QtAppHostBuilder::Impl {
		LoggingBuilder logging;
		ConfigBuilder config;
		ServiceCollection collection;
	};

	QtAppHostBuilder::QtAppHostBuilder() : IHostBuilder(), d(new Impl) {}

	QtAppHostBuilder::~QtAppHostBuilder() = default;

	auto QtAppHostBuilder::ConfigureAppConfig(std::function<void(IConfigBuilder*)>&& delegate) -> QtAppHostBuilder* {
		delegate(&d->config);
		return this;
	}

	auto QtAppHostBuilder::ConfigureServices(std::function<void(ServiceCollection*)>&& delegate) -> QtAppHostBuilder* {
		delegate(&d->collection);
		return this;
	}

	auto QtAppHostBuilder::ConfigureLogging(std::function<void(ILoggingBuilder*)>&& delegate) -> QtAppHostBuilder* {
		delegate(&d->logging);
		return this;
	}

	auto QtAppHostBuilder::ConfigureServices(std::function<void(IServiceCollection*)>&& delegate) -> IHostBuilder* {
		delegate(&d->collection);
		return this;
	}

	auto QtAppHostBuilder::Build() -> Service<IHost> {
		d->collection.AddSingleton<Configuration>(d->config.Build());
		d->collection.AddSingleton<Logger>(d->logging.Build());
		d->collection.AddSingleton<ServiceCollection>(&d->collection);

		return std::make_shared<QtAppHost>(d->collection.Build());
	}
}
