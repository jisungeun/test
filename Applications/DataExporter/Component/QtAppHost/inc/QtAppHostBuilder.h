#pragma once

#include "IHostBuilder.h"

#include "ServiceCollection.h"

#include "DataExporter.Component.QtAppHostExport.h"

namespace DataExporter::Component {
	class DataExporter_Component_QtAppHost_API QtAppHostBuilder final : public IHostBuilder {
	public:
		QtAppHostBuilder();
		~QtAppHostBuilder() override;

		auto ConfigureAppConfig(std::function<void(IConfigBuilder*)>&& delegate) -> QtAppHostBuilder* override;
		auto ConfigureServices(std::function<void(ServiceCollection*)>&& delegate) -> QtAppHostBuilder*;
		auto ConfigureLogging(std::function<void(ILoggingBuilder*)>&& delegate) -> QtAppHostBuilder* override;

		auto Build() -> Service<IHost> override;

	protected:
		auto ConfigureServices(std::function<void(IServiceCollection*)>&& delegate) -> IHostBuilder* override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
