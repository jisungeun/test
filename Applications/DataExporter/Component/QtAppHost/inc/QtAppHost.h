#pragma once

#include "IHost.h"

#include "IServiceProvider.h"

#include "DataExporter.Component.QtAppHostExport.h"

namespace DataExporter::Component {
	class DataExporter_Component_QtAppHost_API QtAppHost final : public IHost {
	public:
		explicit QtAppHost(const Service<IServiceProvider>& provider);
		~QtAppHost() override;

		auto Start(int argc, char** argv) -> int override;
		auto Stop() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
