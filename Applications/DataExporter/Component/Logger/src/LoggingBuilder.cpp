#include <QDateTime>

#include "LoggingBuilder.h"

namespace DataExporter::Component {
	struct LoggingBuilder::Impl {
		QString filename = QDateTime::currentDateTime().toString("yyyy-MM-dd HH-mm-ss-zzz.log");
		bool writingFile = false;
		bool writingConsole = false;
		uint64_t size = 0;
	};

	LoggingBuilder::LoggingBuilder() : ILoggingBuilder(), d(new Impl) {}

	LoggingBuilder::~LoggingBuilder() = default;

	auto LoggingBuilder::SetWritingOnFile(bool usage) -> ILoggingBuilder* {
		d->writingFile = usage;
		return this;
	}

	auto LoggingBuilder::SetWritingOnConsole(bool usage) -> ILoggingBuilder* {
		d->writingConsole = usage;
		return this;
	}

	auto LoggingBuilder::SetLogFilename(const QString& filename) -> ILoggingBuilder* {
		d->filename = filename;
		return this;
	}

	auto LoggingBuilder::SetLogFileMaxSize(uint64_t size) -> ILoggingBuilder* {
		d->size = size;
		return this;
	}

	auto LoggingBuilder::Build() -> Logger* {
		return new Logger(d->writingFile, d->writingConsole, d->filename, d->size);
	}
}
