#include <iostream>

#include <QDateTime>
#include <QFile>
#include <QMutexLocker>
#include <QQueue>
#include <QTextStream>
#include <QWaitCondition>

#include "Logger.h"

namespace DataExporter::Component {
	struct Logger::Impl {
		QFile file;
		QString filename;
		QMutex mutex;
		QWaitCondition waiter;
		QQueue<QString> queue;
		bool disposed = false;
		bool writingFile = false;
		bool writingConsole = false;
		uint64_t size = 0;

		static auto ToString(LogLevel level) -> QString;
		auto UpdateFilename() -> QFile*;
	};

	auto Logger::Impl::ToString(LogLevel level) -> QString {
		switch (level) {
			case LogLevel::Trace:
				return "TRACE";
			case LogLevel::Debug:
				return "DEBUG";
			case LogLevel::Information:
				return "INFO";
			case LogLevel::Warning:
				return "WARN";
			case LogLevel::Error:
				return "ERROR";
			case LogLevel::Critical:
				return "FATAL";
			default:
				return "NONE";
		}
	}

	auto Logger::Impl::UpdateFilename() -> QFile* {
		file.setFileName(filename);

		for (auto i = 0;; i++) {
			file.setFileName(QString("%1_%2").arg(filename).arg(i));

			if (file.open(QIODevice::WriteOnly | QIODevice::Text))
				break;
		}

		return &file;
	}

	Logger::Logger(bool writeFile, bool writeConsole, const QString& filename, const uint64_t size) : ILogger(), d(new Impl) {
		d->writingFile = writeFile;
		d->writingConsole = writeConsole;
		d->filename = filename;
		d->size = size;
	}

	Logger::~Logger() = default;

	auto Logger::Start(const QStringList& args) -> bool {
		start();
		return true;
	}

	auto Logger::Stop() -> void {
		d->disposed = true;
	}

	auto Logger::Log(LogLevel level, const QString& message) -> void {
		QMutexLocker locker(&d->mutex);
		d->queue.enqueue(QString("[%1 %2] %3").arg(QDateTime::currentDateTime().toString("yyyy/MM/dd HH:mm:ss.zzz")).arg(d->ToString(level), 5).arg(message));
		d->waiter.wakeAll();
	}

	auto Logger::LogTrace(const QString& message) -> void {
		Log(LogLevel::Trace, message);
	}

	auto Logger::LogDebug(const QString& message) -> void {
		Log(LogLevel::Debug, message);
	}

	auto Logger::LogInformation(const QString& message) -> void {
		Log(LogLevel::Information, message);
	}

	auto Logger::LogWarning(const QString& message) -> void {
		Log(LogLevel::Warning, message);
	}

	auto Logger::LogError(const QString& message) -> void {
		Log(LogLevel::Error, message);
	}

	auto Logger::LogCritical(const QString& message) -> void {
		Log(LogLevel::Critical, message);
	}

	auto Logger::run() -> void {
		QTextStream stream;

		if (d->writingFile)
			stream.setDevice(d->UpdateFilename());

		while (!d->disposed) {
			QMutexLocker locker(&d->mutex);

			while (d->queue.isEmpty() && !d->disposed)
				d->waiter.wait(&d->mutex);

			const auto message = d->queue.dequeue();
			locker.unlock();

			if (d->writingFile) {
				stream << message;

				if (d->size > 0 && d->file.size() > d->size)
					stream.setDevice(d->UpdateFilename());
			}

			if (d->writingConsole)
				std::cout << message.toStdString();
		}

		if (d->writingFile)
			d->file.close();
	}
}
