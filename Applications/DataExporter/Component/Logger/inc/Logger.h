#pragma once

#include <QThread>

#include "ILogger.h"

#include "DataExporter.Component.LoggerExport.h"

namespace DataExporter::Component {
	class DataExporter_Component_Logger_API Logger final : public QThread, public ILogger {
		Implements(ILogger)

	public:
		Logger(bool writeFile, bool writeConsole, const QString& filename, const uint64_t size);
		~Logger() override;

		auto Start(const QStringList& args) -> bool override;
		auto Stop() -> void override;

		auto Log(LogLevel level, const QString& message) -> void override;
		auto LogTrace(const QString& message) -> void override;
		auto LogDebug(const QString& message) -> void override;
		auto LogInformation(const QString& message) -> void override;
		auto LogWarning(const QString& message) -> void override;
		auto LogError(const QString& message) -> void override;
		auto LogCritical(const QString& message) -> void override;

	protected:
		auto run() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}