#pragma once

#include "ILoggingBuilder.h"

#include "Logger.h"

#include "DataExporter.Component.LoggerExport.h"

namespace DataExporter::Component {
	class DataExporter_Component_Logger_API LoggingBuilder final : public ILoggingBuilder {
		Implements(ILoggingBuilder)

	public:
		LoggingBuilder();
		~LoggingBuilder() override;

		auto SetWritingOnFile(bool usage) -> ILoggingBuilder* override;
		auto SetWritingOnConsole(bool usage) -> ILoggingBuilder* override;

		auto SetLogFilename(const QString& filename) -> ILoggingBuilder* override;
		auto SetLogFileMaxSize(uint64_t size) -> ILoggingBuilder* override;

		auto Build() -> Logger* override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}