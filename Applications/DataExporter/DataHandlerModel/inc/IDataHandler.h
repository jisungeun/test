#pragma once

#include "EventHandler.h"
#include "IHostedService.h"

#include "IData.h"

#include "DataExporter.DataHandlerModelExport.h"

namespace DataExporter {
	class DataExporter_DataHandlerModel_API IDataHandler : public virtual IHostedService {
	public:
		virtual auto GetCurrentItem() const -> IData* = 0;

		EventHandler<IData*> OnStarted;
		EventHandler<IData*> OnUpdated;
		EventHandler<IData*> OnFinished;
		EventHandler<void> OnCleared;
	};
}
