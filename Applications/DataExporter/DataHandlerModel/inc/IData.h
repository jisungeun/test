#pragma once

#include <QVariantMap>

#include "IService.h"

#include "DataExporter.DataHandlerModelExport.h"

namespace DataExporter {
	class DataExporter_DataHandlerModel_API IData : public virtual IService {
	public:
		virtual auto GetTCFPath() const -> QString = 0;
		virtual auto GetSavePath() const -> QString = 0;
		virtual auto GetSaveFormat() const -> QString = 0;
		virtual auto GetOptionMap() const -> QVariantMap = 0;

		virtual auto GetProgress() const -> int = 0;
		virtual auto GetMessage() const -> QString = 0;
		virtual auto GetError() const -> QString = 0;
	};
}