#include "NamedPipeServer.h"

#include <QLocalServer>
#include <QLocalSocket>

#include "NamedPipeClient.h"

namespace DataExporter::Gateway {
	struct NamedPipeServer::Impl {
		QLocalServer* server = nullptr;
	};

	NamedPipeServer::NamedPipeServer(QObject* parent) : QObject(parent), d(new Impl) {
		d->server = new QLocalServer(this);

		connect(d->server, &QLocalServer::newConnection, this, &NamedPipeServer::OnNewConnection);
	}

	NamedPipeServer::~NamedPipeServer() = default;

	auto NamedPipeServer::Listen() -> bool {
		return d->server->listen(NamedPipeName);
	}

	auto NamedPipeServer::Stop() -> void {
		d->server->close();
	}

	auto NamedPipeServer::OnNewConnection() -> void {
		if (auto* socket = d->server->nextPendingConnection()) {
			if (socket->waitForReadyRead()) {
				const auto bytes = socket->read(NamedPipeBufferSize);
				const auto req = std::make_shared<NamedPipeRequest>();

				if (req->Deserialize(bytes))
					emit Requested(req.get());
			}
		}
	}
}
