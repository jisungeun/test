#include "SingletonProcess.h"

#include <QSharedMemory>

namespace DataExporter::Gateway {
	namespace {
		QSharedMemory* memory = nullptr;
		constexpr const char* SharedMemoryName = "DataExporterSharedMemory";
	}

	auto SingletonProcess::IsSingleton() -> bool {
		static auto ok = false;

		if (!memory) {
			memory = new QSharedMemory(SharedMemoryName);
			ok = memory->create(1, QSharedMemory::ReadWrite);
		}

		return ok;
	}

	auto SingletonProcess::Release() -> void {
		delete memory;
		memory = nullptr;
	}
}
