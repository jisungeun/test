#include "NamedPipeRequest.h"

#include <QJsonDocument>
#include <QJsonObject>

namespace DataExporter::Gateway {
	namespace {
		constexpr const char* RequestMethod = "NamedPipe";
	}

	struct NamedPipeRequest::Impl {
		QDateTime created = QDateTime::currentDateTime();
		QVariantMap content;
	};

	NamedPipeRequest::NamedPipeRequest() : IRequest(), d(new Impl) { }

	NamedPipeRequest::NamedPipeRequest(const QByteArray& bytes) : IRequest(), d(new Impl) {
		Deserialize(bytes);
	}

	NamedPipeRequest::NamedPipeRequest(const QStringList& args) : IRequest(), d(new Impl) {
		Deserialize(args);
	}

	NamedPipeRequest::~NamedPipeRequest() = default;

	auto NamedPipeRequest::Serialize() const -> QByteArray {
		return QJsonDocument::fromVariant(d->content).toJson();
	}

	auto NamedPipeRequest::Deserialize(const QByteArray& bytes) -> bool {
		QJsonParseError error;
		const auto doc = QJsonDocument::fromJson(bytes, &error);

		if (error.error == QJsonParseError::NoError) {
			d->content = doc.object().toVariantMap();
			return true;
		}

		return false;
	}

	auto NamedPipeRequest::Deserialize(const QStringList& args) -> bool {
		for (auto i = 0; i < args.count(); i++) {
			if (args[i].startsWith('-')) {
				const auto key = args[i].right(args[i].count() - 1);

				while (i + 1 < args.count() && !args[i + 1].startsWith('-'))
					d->content[key] = d->content[key].toString() + args[++i] + ' ';

				d->content[key] = d->content[key].toString().trimmed();
			}
		}

		if (d->content.contains("src"))
			d->content["Source"] = d->content.take("src");
		if (d->content.contains("dest"))
			d->content["Destination"] = d->content.take("dest");
		if (d->content.contains("format"))
			d->content["Format"] = d->content.take("format");
		if (d->content.contains("ht"))
			d->content["HT"] = d->content.take("ht");
		if (d->content.contains("ht2d"))
			d->content["HT2D"] = d->content.take("ht2d");
		if (d->content.contains("ht3d"))
			d->content["HT3D"] = d->content.take("ht3d");
		if (d->content.contains("fl"))
			d->content["FL"] = d->content.take("fl");
		if (d->content.contains("fl2d"))
			d->content["FL2D"] = d->content.take("fl2d");
		if (d->content.contains("fl3d"))
			d->content["FL3D"] = d->content.take("fl3d");
		if (d->content.contains("bf"))
			d->content["BF"] = d->content.take("bf");
		if (d->content.contains("z-min")) {
			auto range = d->content["ZRange"].toMap();
			range["Min"] = d->content.take("z-min").toInt();

			d->content["ZRange"] = range;
		}
		if (d->content.contains("z-max")) {
			auto range = d->content["ZRange"].toMap();
			range["Max"] = d->content.take("z-max").toInt();

			d->content["ZRange"] = range;
		}

		return true;
	}

	auto NamedPipeRequest::SetCreated(const QDateTime& created) -> void {
		d->created = created;
	}

	auto NamedPipeRequest::SetContent(const QVariantMap& content) -> void {
		d->content = content;
	}

	auto NamedPipeRequest::GetMethod() const -> QString {
		return RequestMethod;
	}

	auto NamedPipeRequest::GetCreated() const -> QDateTime {
		return d->created;
	}

	auto NamedPipeRequest::GetContent() const -> QVariantMap {
		return d->content;
	}
}
