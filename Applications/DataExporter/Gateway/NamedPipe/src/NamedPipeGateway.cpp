#include "NamedPipeGateway.h"

#include "IDataHandler.h"

#include "NamedPipeServer.h"
#include "SingletonProcess.h"

namespace DataExporter::Gateway {
	struct NamedPipeGateway::Impl {
		EventContainer event;
		NamedPipeServer* server = nullptr;
	};

	NamedPipeGateway::NamedPipeGateway(IServiceProvider* provider) : QObject(), IGateway(), d(new Impl) {
		d->server = new NamedPipeServer(this);

		if (const auto handler = provider->GetService<IDataHandler>())
			handler->OnCleared += { d->event, this, &NamedPipeGateway::OnCleared };

		connect(d->server, &NamedPipeServer::Requested, this, &NamedPipeGateway::OnClientRequested);
	}

	NamedPipeGateway::~NamedPipeGateway() = default;

	auto NamedPipeGateway::Start(const QStringList& args) -> bool {
		if (SingletonProcess::IsSingleton())
			return d->server->Listen();

		return true;
	}

	auto NamedPipeGateway::Stop() -> void {
		d->server->Stop();
	}

	auto NamedPipeGateway::OnCleared(IService* sender) -> void {
		SingletonProcess::Release();
		d->server->Stop();
	}

	auto NamedPipeGateway::OnClientRequested(NamedPipeRequest* request) -> void {
		OnRequested(this, request);
	}
}
