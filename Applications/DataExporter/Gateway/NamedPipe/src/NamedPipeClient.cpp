#include "NamedPipeClient.h"

#include <QLocalSocket>

#include "NamedPipeRequest.h"

namespace DataExporter::Gateway {
	auto NamedPipeClient::Request(const NamedPipeRequest* request) -> bool {
		QLocalSocket client;
		client.connectToServer(NamedPipeName);

		if (client.waitForConnected()) {
			client.write(request->Serialize());
			return client.waitForBytesWritten();
		}

		return false;
	}
}
