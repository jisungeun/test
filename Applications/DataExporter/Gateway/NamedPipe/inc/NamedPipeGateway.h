#pragma once

#include "IGateway.h"
#include "IServiceProvider.h"

#include "NamedPipeRequest.h"

#include "DataExporter.Gateway.NamedPipeExport.h"

namespace DataExporter::Gateway {
	class DataExporter_Gateway_NamedPipe_API NamedPipeGateway final : public QObject, public IGateway {
		Implements(IGateway)

	public:
		explicit NamedPipeGateway(IServiceProvider* provider);
		~NamedPipeGateway() override;

		auto Start(const QStringList& args) -> bool override;
		auto Stop() -> void override;

	protected:
		auto OnCleared(IService* sender) -> void;

	protected slots:
		auto OnClientRequested(NamedPipeRequest* request) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
