#pragma once

#include "DataExporter.Gateway.NamedPipeExport.h"

namespace DataExporter::Gateway {
	class DataExporter_Gateway_NamedPipe_API SingletonProcess {
	public:
		static auto IsSingleton() -> bool;
		static auto Release() -> void;
	};
}
