#pragma once

#include "IRequest.h"

#include "DataExporter.Gateway.NamedPipeExport.h"

namespace DataExporter::Gateway {
	class DataExporter_Gateway_NamedPipe_API NamedPipeRequest final : public IRequest {
	public:
		explicit NamedPipeRequest();
		explicit NamedPipeRequest(const QByteArray& bytes);
		explicit NamedPipeRequest(const QStringList& args);
		~NamedPipeRequest() override;

		auto Serialize() const -> QByteArray;
		auto Deserialize(const QByteArray& bytes) -> bool;
		auto Deserialize(const QStringList& args) -> bool;

		auto SetCreated(const QDateTime& created) -> void;
		auto SetContent(const QVariantMap& content) -> void;

		auto GetMethod() const -> QString override;
		auto GetCreated() const -> QDateTime override;
		auto GetContent() const -> QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
