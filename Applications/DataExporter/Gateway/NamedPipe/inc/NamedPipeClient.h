#pragma once

#include "NamedPipeRequest.h"

#include "DataExporter.Gateway.NamedPipeExport.h"

namespace DataExporter::Gateway {
	class DataExporter_Gateway_NamedPipe_API NamedPipeClient {
	public:
		static auto Request(const NamedPipeRequest* request) -> bool;
	};

	constexpr const char* NamedPipeName = "DataExporterPipeServer";
	constexpr qint64 NamedPipeBufferSize = 4096;
}
