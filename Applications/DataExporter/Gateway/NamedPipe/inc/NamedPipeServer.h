#pragma once

#include <QObject>

#include "NamedPipeRequest.h"

#include "DataExporter.Gateway.NamedPipeExport.h"

namespace DataExporter::Gateway {
	class DataExporter_Gateway_NamedPipe_API NamedPipeServer final : public QObject {
		Q_OBJECT

	public:
		explicit NamedPipeServer(QObject* parent = nullptr);
		~NamedPipeServer() override;

		auto Listen() -> bool;
		auto Stop() -> void;

	protected slots:
		auto OnNewConnection() -> void;

	signals:
		auto Requested(NamedPipeRequest* request) -> void;
	
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
