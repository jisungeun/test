#pragma once

#include "IServiceProvider.h"

#include "IAppModule.h"
#include "IGateway.h"

#include "DataExporter.GatewayExport.h"

namespace DataExporter::Gateway {
	class DataExporter_Gateway_API Gateway final : public IAppModule, public IGateway {
	public:
		explicit Gateway(Tomocube::IServiceProvider * provider);
		~Gateway() override;

		auto Start() -> void override;
		auto Stop() -> void override;

		auto AddEvent(const GatewayEventPtr& event) -> void override;
		auto RemoveEvent(const GatewayEventPtr& event) -> void override;

		auto Request(const RequestPtr& request) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
