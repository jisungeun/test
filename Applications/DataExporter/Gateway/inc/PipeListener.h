#pragma once

#include <memory>

#include <QThread>

#include "IServiceProvider.h"

#include "DataExporter.GatewayExport.h"

namespace DataExporter::Gateway {
	class DataExporter_Gateway_API PipeListener final : public QThread {
	public:
		explicit PipeListener(Tomocube::IServiceProvider* provider, QObject* parent = nullptr);
		~PipeListener() override;

		auto Listen() -> bool;

	protected:
		auto run() -> void override;
	
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
