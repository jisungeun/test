#pragma once

#include "IRequest.h"

#include "DataExporter.GatewayExport.h"

namespace DataExporter::Gateway {
	class DataExporter_Gateway_API Request final : public IRequest {
	public:
		explicit Request(const QString& src, const QString& dest, const QString& format);
		explicit Request(const QString& src, const QString& dest, const QString& format, const QMap<QString, QString>& options);
		explicit Request(const QByteArray& serial);
		explicit Request(int argc, char** argv);
		~Request() override;

		auto GetSource() const -> const QString& override;
		auto GetDestination() const -> const QString& override;
		auto GetFormat() const -> const QString& override;
		auto GetOptions() const -> const QMap<QString, QString>& override;

		static auto Serialize(const RequestPtr& request) -> QByteArray;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
