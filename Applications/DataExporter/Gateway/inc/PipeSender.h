#pragma once

#include <memory>

#include <QObject>

#include "IRequest.h"

#include "DataExporter.GatewayExport.h"

namespace DataExporter::Gateway {
	class DataExporter_Gateway_API PipeSender final : public QObject {
	public:
		PipeSender();
		~PipeSender() override;

		auto Send(const RequestPtr& request) -> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
