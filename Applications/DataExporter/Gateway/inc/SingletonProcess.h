#pragma once

#include <memory>

#include "DataExporter.GatewayExport.h"

namespace DataExporter::Gateway {
	class DataExporter_Gateway_API SingletonProcess {
	public:
		SingletonProcess();
		~SingletonProcess();

		auto IsSingleton() const -> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
