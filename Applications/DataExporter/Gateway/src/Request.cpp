#include "Request.h"

namespace DataExporter::Gateway {
	struct Request::Impl {
		QString src;
		QString dest;
		QString format;
		QMap<QString, QString> options;
	};

	Request::Request(const QString& src, const QString& dest, const QString& format) : IRequest(), d(new Impl) {
		d->src = src;
		d->dest = dest;
		d->format = format;
	}

	Request::Request(const QString& src, const QString& dest, const QString& format, const QMap<QString, QString>& options) : Request(src, dest, format) {
		d->options = options;
	}

	Request::Request(const QByteArray& serial) : IRequest(), d(new Impl) {
		const auto splits = serial.split('\n');
		QMap<QString, QString> series;

		for (const auto& s : splits) {
			const auto idx = s.indexOf('=');
			series[QString::fromLocal8Bit(s.mid(0, idx))] = QString::fromLocal8Bit(s.mid(idx + 1, s.count() - idx));
		}

		d->src = series.take("src");
		d->dest = series.take("dest");
		d->format = series.take("format");
		d->options = series;
	}

	Request::Request(int argc, char** argv) : IRequest(), d(new Impl) {
		QMap<QString, QString> series;

		for (int i = 0; i < argc; i++) {
			if (argv[i][0] == '-') {
				QString key = QString::fromLocal8Bit(argv[i] + 1);
				QString value;

				while (i + 1 < argc && argv[i + 1][0] != '-')
					value.append(QString::fromLocal8Bit(argv[++i]));

				series[key] = value;
			}
		}

		d->src = series.take("src");
		d->dest = series.take("dest");
		d->format = series.take("format");
		d->options = series;
	}

	Request::~Request() = default;

	auto Request::GetSource() const -> const QString& {
		return d->src;
	}

	auto Request::GetDestination() const -> const QString& {
		return d->dest;
	}

	auto Request::GetFormat() const -> const QString& {
		return d->format;
	}

	auto Request::GetOptions() const -> const QMap<QString, QString>& {
		return d->options;
	}

	auto Request::Serialize(const RequestPtr& request) -> QByteArray {
		const auto source = request->GetSource();
		const auto dest = request->GetDestination();
		const auto format = request->GetFormat();
		const auto options = request->GetOptions();

		auto serial = QString("src=%1\ndest=%2\nformat=%3").arg(source).arg(dest).arg(format);

		for (const auto& key : options.keys())
			serial.append(QString("\n%1=%2").arg(key).arg(options[key]));

		return serial.toLocal8Bit();
	}
}
