#include "Gateway.h"

#include "PipeListener.h"
#include "PipeSender.h"
#include "SingletonProcess.h"

namespace DataExporter::Gateway {
	struct Gateway::Impl {
		Tomocube::IServiceProvider* provider = nullptr;

		GatewayEventList events;

		SingletonProcess singleton;
		std::unique_ptr<PipeListener> listener = nullptr;

		QString error;
	};

	Gateway::Gateway(Tomocube::IServiceProvider* provider) : IGateway(), d(new Impl) {
		d->provider = provider;
	}

	Gateway::~Gateway() = default;

	auto Gateway::Start() -> void {
		if (d->singleton.IsSingleton()) {
			d->listener = std::make_unique<PipeListener>(d->provider);

			if (!d->listener->Listen())
				throw ModuleException("Gateway Server could not be started.");
		}
	}

	auto Gateway::Stop() -> void {}

	auto Gateway::AddEvent(const GatewayEventPtr& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto Gateway::RemoveEvent(const GatewayEventPtr& event) -> void {
		d->events.removeOne(event);
	}

	auto Gateway::Request(const RequestPtr& request) -> bool {
		if (d->singleton.IsSingleton()) {
			for (const auto& e : d->events)
				e->OnRequested(request);

			return true;
		}

		PipeSender sender;
		return sender.Send(request);
	}
}
