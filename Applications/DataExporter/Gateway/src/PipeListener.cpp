#include "PipeListener.h"

#include <QDateTime>
#include <QLocalServer>
#include <QLocalSocket>
#include <QMutex>
#include <QThreadPool>
#include <QWaitCondition>

#include "IGateway.h"
#include "Request.h"

namespace DataExporter::Gateway {
	constexpr const char* UniqueID = "DataExporterPipeServer";
	constexpr qint64 BufferSize = 4096;

	struct PipeListener::Impl {
		Tomocube::IServiceProvider* provider = nullptr;

		QThreadPool pool;
		QWaitCondition waiter;
		QMutex mutex;

		bool isListening = false;
		bool isCancelled = false;
	};

	PipeListener::PipeListener(Tomocube::IServiceProvider* provider, QObject* parent) : QThread(parent), d(new Impl) {
		d->provider = provider;
	}

	PipeListener::~PipeListener() {
		if (d->isListening) {
			d->isCancelled = true;
			wait();
		}
	}

	auto PipeListener::Listen() -> bool {
		d->mutex.lock();
		start();
		d->waiter.wait(&d->mutex, 30);
		d->mutex.unlock();
		return d->isListening;
	}

	auto PipeListener::run() -> void {
		QLocalServer server;

		d->mutex.lock();
		d->isListening = server.listen(UniqueID);
		d->waiter.wakeOne();
		d->mutex.unlock();

		QWaitCondition con;
		con.wakeOne();

		while (server.isListening() && !d->isCancelled) {
			if (server.waitForNewConnection(1000)) {
				if (auto* client = server.nextPendingConnection(); client->waitForReadyRead()) {
					d->pool.start([this, client] {
						const auto buffer = std::make_unique<char[]>(BufferSize);
						QByteArray serial;
						qint64 readSize = 0;

						while (readSize < BufferSize) {
							if (const auto size = client->read(buffer.get() + readSize, BufferSize); size > 0) {
								serial.append(buffer.get(), static_cast<int>(size));
								readSize += size;
							} else
								break;
						}

						const auto gateway = d->provider->GetService<IGateway>();
						gateway->Request(std::make_shared<Request>(serial));

						client->deleteLater();
					});
				}
			}
		}

		server.close();
	}
}
