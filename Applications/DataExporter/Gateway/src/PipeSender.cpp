#include "PipeSender.h"

#include <QLocalSocket>

#include "Request.h"

namespace DataExporter::Gateway {
	constexpr const char* UniqueID = "DataExporterPipeServer";
	constexpr qint64 BufferSize = 4096;

	struct PipeSender::Impl {};

	PipeSender::PipeSender() : QObject(), d(new Impl) {}

	PipeSender::~PipeSender() = default;

	auto PipeSender::Send(const RequestPtr& request) -> bool {
		QLocalSocket socket;
		socket.connectToServer(UniqueID, QIODevice::WriteOnly);

		if (socket.waitForConnected() && socket.isValid()) {
			const auto serial = Request::Serialize(request);
			qint64 writtenSize = 0;

			while (writtenSize < serial.size()) {
				if (const auto size = socket.write(serial.data() + writtenSize, std::min(BufferSize, serial.size() - writtenSize)); size > 0)
					writtenSize += size;
				else
					break;
			}

			socket.flush();
			socket.waitForBytesWritten();
			socket.disconnectFromServer();

			return writtenSize == serial.size();
		}

		return false;
	}
}
