#include <optional>

#include <QSharedMemory>
#include <QSystemSemaphore>

#include "SingletonProcess.h"

namespace DataExporter::Gateway {
	constexpr const char* UniqueID = "DataExporterSharedMemory";

	struct SingletonProcess::Impl {
		std::shared_ptr<QSharedMemory> memory = nullptr;
		std::optional<bool> result = std::nullopt;
	};

	SingletonProcess::SingletonProcess() : d(new Impl) { }

	SingletonProcess::~SingletonProcess() = default;

	auto SingletonProcess::IsSingleton() const -> bool {
		if (!d->result) {
			const auto memory = std::make_shared<QSharedMemory>(UniqueID);
			d->result = memory->create(1, QSharedMemory::ReadWrite);

			if (*d->result)
				d->memory = memory;
		}

		return *d->result;
	}
}
