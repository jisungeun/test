#include "PromptGateway.h"

#include "NamedPipeClient.h"
#include "SingletonProcess.h"

namespace DataExporter::Gateway {
	struct PromptGateway::Impl {
		QStringList args;
	};

	PromptGateway::PromptGateway(QObject* parent) : QThread(parent), IGateway(), d(new Impl) {}

	PromptGateway::~PromptGateway() = default;

	auto PromptGateway::Start(const QStringList& args) -> bool {
		d->args = args;

		if (SingletonProcess::IsSingleton()) {
			start();
			return true;
		} else {
			run();
			return false;
		}
	}

	auto PromptGateway::Stop() -> void {}

	auto PromptGateway::run() -> void {
		const auto req = std::make_shared<NamedPipeRequest>(d->args);
		NamedPipeClient::Request(req.get());
	}
}
