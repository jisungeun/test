#pragma once

#include <QThread>

#include "IGateway.h"

#include "DataExporter.Gateway.PromptExport.h"

namespace DataExporter::Gateway {
	class DataExporter_Gateway_Prompt_API PromptGateway final : public QThread, public IGateway {
		Implements(IGateway)

	public:
		explicit PromptGateway(QObject* parent = nullptr);
		~PromptGateway() override;

		auto Start(const QStringList& args) -> bool override;
		auto Stop() -> void override;

	protected:
		auto run() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
