#pragma once

#include <memory>

#include <QList>

#include "DataExporter.ComponentModelExport.h"

#define Implements(...) public: using ServiceParent = std::tuple<__VA_ARGS__>; private:
#define Requires(...) public: using ServiceRequirement = DataExporter::ToPtrTuple<__VA_ARGS__>; using ServiceRequirementType = std::tuple<__VA_ARGS__>; private:

namespace DataExporter {
	class DataExporter_ComponentModel_API IService {
	public:
		virtual ~IService();

		static auto Run(std::function<void()>&& delegate, bool blocking = true) -> void;
		static auto RunAsync(std::function<void()>&& delegate) -> void;
	};

	template <typename T>
	using Service = std::shared_ptr<T>;
	template <typename... T>
	using ToPtrTuple = std::tuple<std::shared_ptr<T>...>;
	using ServicePtr = Service<IService>;
	template <typename T>
	using ServiceList = QList<Service<T>>;
	using ServicePtrList = QList<ServicePtr>;
}
