#pragma once

#include <typeinfo>

#include "IServiceBuilder.h"

#include "DataExporter.ComponentModelExport.h"

namespace DataExporter {
	class DataExporter_ComponentModel_API IServiceProvider : public virtual IService {
	public:
		template <typename T>
		auto GetService() -> Service<T>;

		template <typename T>
		auto GetServiceList() -> ServiceList<T>;

		static auto GetHostProvider() -> IServiceProvider*;

	protected:
		virtual auto GetBuilder(ServiceId id) -> IServiceBuilder* = 0;
		virtual auto GetBuilderList(ServiceId id) -> QList<IServiceBuilder*> = 0;

		inline static IServiceProvider* instance = nullptr;
	};

	template <typename T>
	auto IServiceProvider::GetService() -> Service<T> {
		if (auto* builder = GetBuilder(typeid(T).hash_code())) {
			if (auto service = std::dynamic_pointer_cast<T>(builder->ToService()))
				return service;
		}

		return nullptr;
	}

	template <typename T>
	auto IServiceProvider::GetServiceList() -> ServiceList<T> {
		QList<Service<T>> list;

		for (auto* builder : GetBuilderList(typeid(T).hash_code())) {
			if (auto service = std::dynamic_pointer_cast<T>(builder->ToService()))
				list.push_back(service);
		}

		return list;
	}
}
