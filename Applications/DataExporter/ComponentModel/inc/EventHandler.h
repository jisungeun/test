#pragma once

#include "Event.h"

namespace DataExporter {
	template <typename... Args>
	class EventHandler {
	public:
		using EventBuilderType = EventBuilder<Args...>;
		using EventType = Event<Args...>;
		using EventPtr = std::shared_ptr<IEvent>;
		using EventWeakPtr = std::weak_ptr<EventType>;

		auto AddEvent(EventBuilderType&& event) -> EventPtr {
			eventList.push_back(event.type);
			return event.type;
		}

		auto AddEvent(typename EventType::EventSignature&& event) -> EventPtr {
			auto e = std::make_shared<EventType>(std::move(event));
			eventList.push_back(e);
			return e;
		}

		auto RemoveEvent(const EventPtr& event) -> bool {
			auto removed = false;

			for (auto i = 0; i < eventList.count(); i++) {
				if (const auto e = eventList[i].lock(); !e || e == event) {
					eventList.removeAt(i--);
					removed = true;
				}
			}

			return removed;
		}

		auto ClearEvent() -> void {
			eventList.clear();
		}

		auto Invoke(IService* sender, const Args&... args) -> void {
			for (auto i = 0; i < eventList.count(); i++) {
				if (const auto event = eventList[i].lock())
					event->Invoke(sender, args...);
				else
					eventList.removeAt(i--);
			}
		}

		auto operator+=(EventBuilderType&& event) -> EventPtr {
			return AddEvent(std::move(event));
		}

		auto operator+=(typename EventType::EventSignature&& event) -> EventPtr {
			return AddEvent(std::move(event));
		}

		auto operator-=(const EventPtr& event) -> bool {
			return RemoveEvent(event);
		}

		auto operator()(IService* sender, const Args&... args) -> void {
			Invoke(sender, args...);
		}

	private:
		QList<EventWeakPtr> eventList;
	};

	template <>
	class EventHandler<void> {
	public:
		using EventBuilderType = EventBuilder<void>;
		using EventType = Event<void>;
		using EventPtr = std::shared_ptr<IEvent>;
		using EventWeakPtr = std::weak_ptr<EventType>;

		auto AddEvent(EventBuilderType&& event) -> EventPtr {
			eventList.push_back(event.type);
			return event.type;
		}

		auto AddEvent(EventType::EventSignature&& event) -> EventPtr {
			auto e = std::make_shared<EventType>(std::move(event));
			eventList.push_back(e);
			return e;
		}

		auto RemoveEvent(const EventPtr& event) -> bool {
			auto removed = false;

			for (auto i = 0; i < eventList.count(); i++) {
				if (const auto e = eventList[i].lock(); !e || e == event) {
					eventList.removeAt(i--);
					removed = true;
				}
			}

			return removed;
		}

		auto ClearEvent() -> void {
			eventList.clear();
		}

		auto Invoke(IService* sender) -> void {
			for (auto i = 0; i < eventList.count(); i++) {
				if (const auto event = eventList[i].lock())
					event->Invoke(sender);
				else
					eventList.removeAt(i--);
			}
		}

		auto operator+=(EventBuilderType&& event) -> EventPtr {
			return AddEvent(std::move(event));
		}

		auto operator+=(EventType::EventSignature&& event) -> EventPtr {
			return AddEvent(std::move(event));
		}

		auto operator-=(const EventPtr& event) -> bool {
			return RemoveEvent(event);
		}

		auto operator()(IService* sender) -> void {
			Invoke(sender);
		}

	private:
		QList<EventWeakPtr> eventList;
	};
}
