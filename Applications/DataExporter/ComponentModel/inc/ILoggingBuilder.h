#pragma once

#include "ILogger.h"

#include "DataExporter.ComponentModelExport.h"

namespace DataExporter {
	class DataExporter_ComponentModel_API ILoggingBuilder : public virtual IService {
	public:
		virtual auto SetWritingOnFile(bool usage) -> ILoggingBuilder* = 0;
		virtual auto SetWritingOnConsole(bool usage) -> ILoggingBuilder* = 0;

		virtual auto SetLogFilename(const QString& filename) -> ILoggingBuilder* = 0;
		virtual auto SetLogFileMaxSize(uint64_t size) -> ILoggingBuilder* = 0;

		virtual auto Build() -> ILogger* = 0;
	};
}
