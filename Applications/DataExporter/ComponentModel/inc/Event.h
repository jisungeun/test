#pragma once

#include <functional>

#include "IService.h"

namespace DataExporter {
	using IEvent = IService;
	using EventContainer = ServicePtrList;

	template <typename... Args>
	class Event final : public IEvent {
	public:
		using EventSignature = std::function<void(IService*, const Args&...)>;

		Event(EventSignature&& event) : event(std::move(event)) {}

		auto Invoke(IService* sender, const Args&... args) const -> void {
			event(sender, args...);
		}

	private:
		EventSignature event;
	};

	template <>
	class Event<void> final : public IEvent {
	public:
		using EventSignature = std::function<void(IService*)>;

		Event(EventSignature&& event) : event(std::move(event)) {}

		auto Invoke(IService* sender) const -> void {
			event(sender);
		}

	private:
		EventSignature event;
	};

	template <typename... Args>
	class EventBuilder {
	public:
		using EventType = Event<Args...>;
		using EventSignature = std::function<void(IService*, const Args&...)>;

		EventBuilder(EventSignature&& event) {
			type = std::make_shared<EventType>(std::move(event));
		}

		template <typename Delegate>
		EventBuilder(EventContainer& container, Delegate&& event) {
			type = std::make_shared<EventType>(std::forward<Delegate>(event));
			container.push_back(type);
		}

		template <typename Sender, typename Delegate>
		EventBuilder(Sender sender, Delegate&& delegate) {
			type = std::make_shared<EventType>([sender, delegate](IService* service, auto&... args) {
				(sender->*delegate)(service, args...);
			});
		}

		template <typename Sender, typename Delegate>
		EventBuilder(EventContainer& container, Sender sender, Delegate&& delegate) {
			type = std::make_shared<EventType>([sender, delegate](IService* service, auto&... args) {
				(sender->*delegate)(service, args...);
			});
			container.push_back(type);
		}

		Service<EventType> type;
	};

	template <>
	class EventBuilder<void> final : public IEvent {
	public:
		using EventType = Event<void>;
		using EventSignature = std::function<void(IService*)>;

		EventBuilder(EventSignature&& event) {
			type = std::make_shared<EventType>(std::move(event));
		}

		template <typename Delegate>
		EventBuilder(EventContainer& container, Delegate&& event) {
			type = std::make_shared<EventType>(std::forward<Delegate>(event));
			container.push_back(type);
		}

		template <typename Sender, typename Delegate>
		EventBuilder(Sender sender, Delegate&& delegate) {
			type = std::make_shared<EventType>([sender, delegate](IService* service, auto&... args) {
				(sender->*delegate)(service, args...);
			});
		}

		template <typename Sender, typename Delegate>
		EventBuilder(EventContainer& container, Sender sender, Delegate&& delegate) {
			type = std::make_shared<EventType>([sender, delegate](IService* service, auto&... args) {
				(sender->*delegate)(service, args...);
			});
			container.push_back(type);
		}

		Service<EventType> type;
	};
}
