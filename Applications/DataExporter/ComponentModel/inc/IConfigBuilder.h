#pragma once

#include "IConfiguration.h"

#include "DataExporter.ComponentModelExport.h"

namespace DataExporter {
	class DataExporter_ComponentModel_API IConfigBuilder : public virtual IService {
	public:
		virtual auto AddConfiguration(const Service<IConfiguration>& config) -> IConfigBuilder* = 0;
		virtual auto AddJsonFile(const QString& path, bool optional = false) -> IConfigBuilder* = 0;
		virtual auto AddRegistry(const QString& organization, const QString& application) -> IConfigBuilder* = 0;

		virtual auto Build() -> IConfiguration* = 0;
	};
}
