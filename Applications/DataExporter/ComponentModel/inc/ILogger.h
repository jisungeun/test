#pragma once

#include "IHostedService.h"

#include "DataExporter.ComponentModelExport.h"

namespace DataExporter {
	enum class LogLevel {
        Trace = 0,
        Debug = 1,
        Information = 2,
        Warning = 3,
        Error = 4,
        Critical = 5,
        None = 6
	};

	class DataExporter_ComponentModel_API ILogger : public IHostedService {
	public:
		virtual auto Log(LogLevel level, const QString& message) -> void = 0;
		virtual auto LogTrace(const QString& message) -> void = 0;
		virtual auto LogDebug(const QString& message) -> void = 0;
		virtual auto LogInformation(const QString& message) -> void = 0;
		virtual auto LogWarning(const QString& message) -> void = 0;
		virtual auto LogError(const QString& message) -> void = 0;
		virtual auto LogCritical(const QString& message) -> void = 0;
	};
}
