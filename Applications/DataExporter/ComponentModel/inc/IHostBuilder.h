#pragma once

#include "IHost.h"

#include "IConfigBuilder.h"
#include "ILoggingBuilder.h"
#include "IServiceCollection.h"

#include "DataExporter.ComponentModelExport.h"

namespace DataExporter {
	class DataExporter_ComponentModel_API IHostBuilder : public virtual IService {
	public:
		virtual auto ConfigureAppConfig(std::function<void(IConfigBuilder*)>&& delegate) -> IHostBuilder* = 0;
		virtual auto ConfigureServices(std::function<void(IServiceCollection*)>&& delegate) -> IHostBuilder* = 0;
		virtual auto ConfigureLogging(std::function<void(ILoggingBuilder*)>&& delegate) -> IHostBuilder* = 0;

		virtual auto Build() -> Service<IHost> = 0;
	};
}
