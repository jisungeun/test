#pragma once

#include <QVariant>

#include "IService.h"

#include "DataExporter.ComponentModelExport.h"

namespace DataExporter {
	class DataExporter_ComponentModel_API IConfiguration : public virtual IService {
	public:
		virtual auto GetProperty(const QString& path) const -> QVariant = 0;
		virtual auto GetPropertyList() const -> QStringList = 0;
		
		virtual auto operator [](const QString& path) const -> QVariant {
			return GetProperty(path);
		}
	};
}
