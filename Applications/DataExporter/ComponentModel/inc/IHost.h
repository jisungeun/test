#pragma once

#include "IServiceProvider.h"

#include "DataExporter.ComponentModelExport.h"

namespace DataExporter {
	class DataExporter_ComponentModel_API IHost : public virtual IService {
	public:
		virtual auto Start(int argc, char** argv) -> int = 0;
		virtual auto Stop() -> void = 0;
	};
}
