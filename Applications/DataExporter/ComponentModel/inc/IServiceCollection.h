#pragma once

#include "IServiceProvider.h"

#include "DataExporter.ComponentModelExport.h"

namespace DataExporter {
	class DataExporter_ComponentModel_API IServiceCollection : public virtual IService {
	public:
		virtual auto Build() -> Service<IServiceProvider> = 0;
	};
}
