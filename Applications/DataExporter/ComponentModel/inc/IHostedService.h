#pragma once

#include "IService.h"

#include "DataExporter.ComponentModelExport.h"

namespace DataExporter {
	class DataExporter_ComponentModel_API IHostedService : public virtual IService {
	public:
		virtual auto Start(const QStringList& args) -> bool = 0;
		virtual auto Stop() -> void = 0;
	};
}
