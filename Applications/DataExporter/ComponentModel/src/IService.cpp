#include <QCoreApplication>
#include <QThreadPool>

#include "IService.h"

namespace DataExporter {
	namespace {
		QThreadPool pool;
	}

	IService::~IService() = default;

	auto IService::Run(std::function<void()>&& delegate, bool blocking) -> void {
		Qt::ConnectionType type;

		if (blocking)
			type = QThread::currentThread() == qApp->thread() ? Qt::DirectConnection : Qt::BlockingQueuedConnection;
		else
			type = Qt::QueuedConnection;

		QMetaObject::invokeMethod(qApp, std::move(delegate), type);
	}

	auto IService::RunAsync(std::function<void()>&& delegate) -> void {
		pool.start(std::move(delegate));
	}
}
