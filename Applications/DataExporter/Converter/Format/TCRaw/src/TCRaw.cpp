#include <QDir>
#include <QStack>

#include "H5Cpp.h"

#include "TCRaw.h"

#include "TCRawWriter.h"
#include "TcfH5GroupRoiLdmDataReader.h"

namespace DataExporter::Converter::Format {
	using namespace TC::IO::RawWriter;

	struct TCRaw::Impl {
		mutable int max = 0;
		mutable int cur = 0;

		static auto ReadInt(const H5::Attribute& attr) -> uint64_t;
		static auto ReadDouble(const H5::Attribute& attr) -> double;
		static auto ReadString(const H5::Attribute& attr) -> std::string;

		auto CreateHT2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateHT3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateFL2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateFL3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateBF(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateLDMHT2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateLDMHT3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateLDMFL2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateLDMFL3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateLDMBF(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
	};

	auto TCRaw::Impl::ReadInt(const H5::Attribute& attr) -> uint64_t {
		uint64_t value;
		attr.read(H5::PredType::NATIVE_UINT64, &value);
		return value;
	}

	auto TCRaw::Impl::ReadDouble(const H5::Attribute& attr) -> double {
		double value;
		attr.read(H5::PredType::NATIVE_DOUBLE, &value);
		return value;
	}

	auto TCRaw::Impl::ReadString(const H5::Attribute& attr) -> std::string {
		std::string value;
		attr.read(attr.getDataType(), value);
		return value;
	}

	auto TCRaw::Impl::CreateHT2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("2DMIP");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));

			const auto buffer = std::make_unique<uint16_t[]>(x * y);
			const auto fbuffer = std::make_unique<float_t[]>(x * y);

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto data = group.openDataSet(group.getObjnameByIdx(i));
				const auto output = QDir(dest).filePath(QString("%1_HT2D_%2.raw").arg(QFileInfo(src).completeBaseName()).arg(i));

				data.read(buffer.get(), H5::PredType::NATIVE_UINT16);

				for (int j = 0; j < x * y; j++)
					fbuffer[j] = buffer[j] / 10000.0f;

				if (Writer writer(output, { x, y, 1 }, { rx, ry, 0.0f }, Writer::Type::FLOAT); !writer.Append(reinterpret_cast<const char*>(fbuffer.get()), x * y * 2 * 2, false))
					return false;

				status->SetProgress(cur++, max);
			}

		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TCRaw::Impl::CreateHT3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("3D");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto z = ReadInt(group.openAttribute("SizeZ"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));
			const auto rz = static_cast<float>(ReadDouble(group.openAttribute("ResolutionZ")));

			const hsize_t mcount[] = { y, x };
			const hsize_t fcount[] = { 1ULL, y, x };
			const H5::DataSpace mspace { 2, mcount };

			const auto fbuffer = std::make_unique<float_t[]>(x * y);
			const auto buffer = std::make_unique<uint16_t[]>(x * y);

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto data = group.openDataSet(group.getObjnameByIdx(i));
				const auto output = QDir(dest).filePath(QString("%1_HT3D_%2.raw").arg(QFileInfo(src).completeBaseName()).arg(i));

				Writer writer(output, { x, y, z }, { rx, ry, rz }, Writer::Type::FLOAT);

				for (uint64_t j = 0; j < z; j++) {
					const auto space = data.getSpace();
					const hsize_t start[] = { j, 0ULL, 0ULL };
					space.selectHyperslab(H5S_SELECT_SET, fcount, start);

					data.read(buffer.get(), H5::PredType::NATIVE_UINT16, mspace, space);

					for (int j = 0; j < x * y; j++)
						fbuffer[j] = buffer[j] / 10000.0f;

					if (!writer.Append(reinterpret_cast<const char*>(fbuffer.get()), x * y * 2 * 2, j > 0))
						return false;

					status->SetProgress(cur++, max);
				}
			}

		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TCRaw::Impl::CreateFL2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("2DFLMIP");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));

			const auto buffer = std::make_unique<uint16_t[]>(x * y);

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto channel = group.openGroup(group.getObjnameByIdx(i));

				for (int j = 0; j < channel.getNumObjs(); j++) {
					const auto data = channel.openDataSet(channel.getObjnameByIdx(j));
					const auto output = QDir(dest).filePath(QString("%1_FL2D_%2_%3.raw").arg(QFileInfo(src).completeBaseName()).arg(QString::fromStdString(group.getObjnameByIdx(i))).arg(j));

					data.read(buffer.get(), H5::PredType::NATIVE_UINT16);

					if (Writer writer(output, { x, y, 1 }, { rx, ry, 0.0f }, Writer::Type::UINT16); !writer.Append(reinterpret_cast<const char*>(buffer.get()), x * y * 2, false))
						return false;

					status->SetProgress(cur++, max);
				}
			}

		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TCRaw::Impl::CreateFL3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("3DFL");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto z = ReadInt(group.openAttribute("SizeZ"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));
			const auto rz = static_cast<float>(ReadDouble(group.openAttribute("ResolutionZ")));

			const hsize_t mcount[] = { y, x };
			const hsize_t fcount[] = { 1ULL, y, x };
			const H5::DataSpace mspace { 2, mcount };

			const auto buffer = std::make_unique<uint16_t[]>(x * y);

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto channel = group.openGroup(group.getObjnameByIdx(i));

				for (int j = 0; j < channel.getNumObjs(); j++) {
					const auto data = channel.openDataSet(channel.getObjnameByIdx(j));
					const auto output = QDir(dest).filePath(QString("%1_FL3D_%2_%3.raw").arg(QFileInfo(src).completeBaseName()).arg(QString::fromStdString(group.getObjnameByIdx(i))).arg(j));

					Writer writer(output, { x, y, z }, { rx, ry, rz }, Writer::Type::UINT16);

					for (uint64_t k = 0; k < z; k++) {
						const auto space = data.getSpace();
						const hsize_t start[] = { k, 0ULL, 0ULL };
						space.selectHyperslab(H5S_SELECT_SET, fcount, start);

						data.read(buffer.get(), H5::PredType::NATIVE_UINT16, mspace, space);

						if (!writer.Append(reinterpret_cast<const char*>(buffer.get()), x * y * 2, k > 0))
							return false;

						status->SetProgress(cur++, max);
					}
				}
			}

		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TCRaw::Impl::CreateBF(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("BF");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));

			const auto buffer = std::make_unique<uint8_t[]>(x * y * 3);

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto data = group.openDataSet(group.getObjnameByIdx(i));
				const auto output = QDir(dest).filePath(QString("%1_BF_%2.raw").arg(QFileInfo(src).completeBaseName()).arg(i));

				data.read(buffer.get(), H5::PredType::NATIVE_UINT8);

				if (Writer writer(output, { x, y, 1 }, { rx, ry, 0.0f }, Writer::Type::UINT8); !writer.Append(reinterpret_cast<const char*>(buffer.get()), x * y * 3, false))
					return false;

				status->SetProgress(cur++, max);
			}

		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TCRaw::Impl::CreateLDMHT2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("2DMIP");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));
			const auto buffer = std::make_unique<float_t[]>(x * y);

			for (int i = 0; i < group.getNumObjs(); i++) {
				auto data = group.openGroup(group.getObjnameByIdx(i));
				const auto output = QDir(dest).filePath(QString("%1_HT2D_%2.raw").arg(QFileInfo(src).completeBaseName()).arg(i));
				const auto scalar = ReadInt(data.openAttribute("ScalarType"));
				const auto min = ReadDouble(data.openAttribute("RIMin"));

				TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
				reader.SetLdmDataGroup(data);
				reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0 }, TC::IO::Count { x, y } });
				reader.SetSamplingStep(1);

				const auto result = reader.Read();

				if (scalar == 0) {
					const auto ptr = result->GetDataPointerAs<uint16_t>();

					for (int k = 0; k < x * y; k++)
						buffer[k] = static_cast<float_t>(ptr[k] / 10000.0);
				} else if (scalar == 1) {
					const auto ptr = result->GetDataPointerAs<uint8_t>();

					for (int k = 0; k < x * y; k++)
						buffer[k] = static_cast<float_t>(min + (ptr[k] / 1000.0));
				}

				if (Writer writer(output, { x, y, 1 }, { rx, ry, 0.0f }, Writer::Type::FLOAT); !writer.Append(reinterpret_cast<const char*>(buffer.get()), x * y * 2 * 2, false))
					return false;

				status->SetProgress(cur++, max);
			}

		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TCRaw::Impl::CreateLDMHT3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("3D");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto z = ReadInt(group.openAttribute("SizeZ"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));
			const auto rz = static_cast<float>(ReadDouble(group.openAttribute("ResolutionZ")));

			const auto buffer = std::make_unique<float_t[]>(x * y);

			for (int i = 0; i < group.getNumObjs(); i++) {
				auto data = group.openGroup(group.getObjnameByIdx(i));
				const auto output = QDir(dest).filePath(QString("%1_HT3D_%2.raw").arg(QFileInfo(src).completeBaseName()).arg(i));
				const auto scalar = ReadInt(data.openAttribute("ScalarType"));
				const auto min = ReadDouble(data.openAttribute("RIMin"));

				Writer writer(output, { x, y, z }, { rx, ry, rz }, Writer::Type::FLOAT);

				for (uint64_t j = 0; j < z; j++) {
					TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
					reader.SetLdmDataGroup(data);
					reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0, static_cast<int>(j) }, TC::IO::Count { x, y, 1 } });
					reader.SetSamplingStep(1);

					const auto result = reader.Read();

					if (scalar == 0) {
						const auto ptr = result->GetDataPointerAs<uint16_t>();

						for (int k = 0; k < x * y; k++)
							buffer[k] = static_cast<float_t>(ptr[k] / 10000.0);
					} else if (scalar == 1) {
						const auto ptr = result->GetDataPointerAs<uint8_t>();

						for (int k = 0; k < x * y; k++)
							buffer[k] = static_cast<float_t>(min + (ptr[k] / 1000.0));
					}

					if (!writer.Append(reinterpret_cast<const char*>(buffer.get()), x * y * 2 * 2, j > 0))
						return false;

					status->SetProgress(cur++, max);
				}
			}

		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TCRaw::Impl::CreateLDMFL2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("2DFLMIP");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto channel = group.openGroup(group.getObjnameByIdx(i));

				for (int j = 0; j < channel.getNumObjs(); j++) {
					auto data = channel.openGroup(channel.getObjnameByIdx(j));
					const auto output = QDir(dest).filePath(QString("%1_FL2D_%2_%3.raw").arg(QFileInfo(src).completeBaseName()).arg(QString::fromStdString(group.getObjnameByIdx(i))).arg(j));
					const auto scalar = ReadInt(data.openAttribute("ScalarType"));
					const auto min = static_cast<uint16_t>(ReadDouble(data.openAttribute("MinIntensity")));

					TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
					reader.SetLdmDataGroup(data);
					reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0 }, TC::IO::Count { x, y } });
					reader.SetSamplingStep(1);

					Writer writer(output, { x, y, 1 }, { rx, ry, 0.0f }, Writer::Type::UINT16);

					const auto result = reader.Read();

					if (scalar == 0) {
						const auto buffer = result->GetDataPointerAs<uint16_t>();

						if (!writer.Append(reinterpret_cast<const char*>(buffer), x * y * 2, false))
							return false;
					} else if (scalar == 1) {
						const auto ptr = result->GetDataPointerAs<uint8_t>();
						const auto buffer = std::make_unique<uint16_t[]>(x * y);

						for (int k = 0; k < x * y; k++)
							buffer[k] = min + ptr[k];

						if (!writer.Append(reinterpret_cast<const char*>(buffer.get()), x * y * 2, false))
							return false;
					}

					status->SetProgress(cur++, max);
				}
			}

		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TCRaw::Impl::CreateLDMFL3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("3DFL");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto z = ReadInt(group.openAttribute("SizeZ"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));
			const auto rz = static_cast<float>(ReadDouble(group.openAttribute("ResolutionZ")));

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto channel = group.openGroup(group.getObjnameByIdx(i));

				for (int j = 0; j < channel.getNumObjs(); j++) {
					auto data = channel.openGroup(channel.getObjnameByIdx(j));
					const auto output = QDir(dest).filePath(QString("%1_FL3D_%2_%3.raw").arg(QFileInfo(src).completeBaseName()).arg(QString::fromStdString(group.getObjnameByIdx(i))).arg(j));
					const auto scalar = ReadInt(data.openAttribute("ScalarType"));
					const auto min = static_cast<uint16_t>(ReadDouble(data.openAttribute("MinIntensity")));

					for (uint64_t k = 0; k < z; k++) {
						TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
						reader.SetLdmDataGroup(data);
						reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0, static_cast<int>(k) }, TC::IO::Count { x, y, 1 } });
						reader.SetSamplingStep(1);

						Writer writer(output, { x, y, z }, { rx, ry, rz }, Writer::Type::UINT16);

						const auto result = reader.Read();

						if (scalar == 0) {
							const auto buffer = result->GetDataPointerAs<uint16_t>();

							if (!writer.Append(reinterpret_cast<const char*>(buffer), x * y * 2, k > 0))
								return false;
						} else if (scalar == 1) {
							const auto ptr = result->GetDataPointerAs<uint8_t>();
							const auto buffer = std::make_unique<uint16_t[]>(x * y);

							for (int k = 0; k < x * y; k++)
								buffer[k] = min + ptr[k];

							if (!writer.Append(reinterpret_cast<const char*>(buffer.get()), x * y * 2, k > 0))
								return false;
						}

						status->SetProgress(cur++, max);
					}
				}
			}

		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TCRaw::Impl::CreateLDMBF(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("BF");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));

			for (int i = 0; i < group.getNumObjs(); i++) {
				auto data = group.openGroup(group.getObjnameByIdx(i));
				const auto output = QDir(dest).filePath(QString("%1_BF_%2.raw").arg(QFileInfo(src).completeBaseName()).arg(i));

				TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
				reader.SetLdmDataGroup(data);
				reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0 }, TC::IO::Count { x, y } });
				reader.SetSamplingStep(1);
				reader.SetLdmDataIs2DStack(true);

				const auto result = reader.Read();
				const auto buffer = result->GetDataPointerAs<uint8_t>();

				if (Writer writer(output, { x, y, 1 }, { rx, ry, 0.0f }, Writer::Type::UINT8); !writer.Append(reinterpret_cast<const char*>(buffer), x * y * 3, false))
					return false;

				status->SetProgress(cur++, max);
			}

		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	TCRaw::TCRaw() : IConverter(), d(new Impl) {}

	TCRaw::~TCRaw() = default;

	auto TCRaw::Start(const QString& src, const QString& dest, const ConverterStatusPtr& status) -> void {
		H5::Exception::dontPrint();

		if (const QFileInfo file(src); !file.exists())
			throw ConverterException("TCF Not Found");

		if (const QFileInfo save(dest); save.exists() && !save.isDir())
			throw ConverterException("Output Path Not Accessible");

		if (const QFileInfo save(dest); !save.exists()) {
			if (const QDir dir(dest); !dir.mkpath(dest))
				throw ConverterException("Output Path Not Accessible");
		}

		H5::H5File file;
		bool isLDM = false;

		try {
			const auto euc = src.toUtf8();
			file.openFile(euc.constData(), H5F_ACC_RDONLY);
		} catch (const H5::Exception&) {
			throw ConverterException("TCF Not Accessible");
		}

		try {
			QStack<H5::Group> stack;

			const auto data = file.openGroup("Data");
			stack.push(data);

			while (!stack.isEmpty()) {
				auto group = stack.pop();

				for (int i = 0; i < group.getNumObjs(); i++) {
					const auto id = group.getObjnameByIdx(i);

					if (group.getObjTypeByIdx(i) == H5G_GROUP) {
						const auto elem = group.openGroup(id);

						int z = 1;
						int count = 0;
						int channel = 1;

						if (elem.attrExists("SizeZ"))
							z = d->ReadInt(elem.openAttribute("SizeZ"));
						if (elem.attrExists("DataCount"))
							count = d->ReadInt(elem.openAttribute("DataCount"));
						if (elem.attrExists("Channels"))
							channel = d->ReadInt(elem.openAttribute("Channels"));

						const auto c = z * count * channel;
						d->max += c;

						if (c == 0)
							stack.push(elem);
					}
				}
			}

			if (data.attrExists("StructureTypeName"))
				isLDM = d->ReadString(data.openAttribute("StructureTypeName")) == "LDM";
		} catch (const H5::Exception&) {
			throw ConverterException("Error Counting Data");
		}

		if (isLDM) {
			if (!d->CreateLDMHT3D(file, src, dest, status))
				throw ConverterException("Error in HT3D");

			if (!d->CreateLDMHT2D(file, src, dest, status))
				throw ConverterException("Error in HT2D");

			if (!d->CreateLDMFL3D(file, src, dest, status))
				throw ConverterException("Error in FL3D");

			if (!d->CreateLDMFL2D(file, src, dest, status))
				throw ConverterException("Error in FL2D");

			if (!d->CreateLDMBF(file, src, dest, status))
				throw ConverterException("Error in BF");
		} else {
			if (!d->CreateHT3D(file, src, dest, status))
				throw ConverterException("Error in HT3D");

			if (!d->CreateHT2D(file, src, dest, status))
				throw ConverterException("Error in HT2D");

			if (!d->CreateFL3D(file, src, dest, status))
				throw ConverterException("Error in FL3D");

			if (!d->CreateFL2D(file, src, dest, status))
				throw ConverterException("Error in FL2D");

			if (!d->CreateBF(file, src, dest, status))
				throw ConverterException("Error in BF");
		}
	}

}
