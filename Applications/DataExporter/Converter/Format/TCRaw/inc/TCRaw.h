#pragma once

#include "IConverter.h"

#include "DataExporter.Converter.Format.TCRawExport.h"

namespace DataExporter::Converter::Format {
	class DataExporter_Converter_Format_TCRaw_API TCRaw final : public IConverter {
	public:
		TCRaw();
		~TCRaw() override;

		auto Start(const QString& src, const QString& dest, const ConverterStatusPtr& status) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}