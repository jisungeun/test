#pragma once

#include "IConverter.h"

#include "DataExporter.Converter.Format.LibTiffExport.h"

namespace DataExporter::Converter::Format {
	class DataExporter_Converter_Format_LibTiff_API TiffConverter final : public IConverter {
	public:
		TiffConverter();
		~TiffConverter() override;

		auto Start(const QString& src, const QString& dest, const ConverterStatusPtr& status) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}