#pragma once

#include <memory>
#include <string>

#include "DataExporter.Converter.Format.LibTiffExport.h"

namespace DataExporter::Converter::Format {
	struct TiffMetadata3D {
		uint64_t x;
		uint64_t y;
		uint64_t z;
		int byte;
		bool isRGB;

		std::string filepath;
	};

	struct TiffMetadata2D {
		uint64_t x;
		uint64_t y;
		int byte;
		bool isRGB;

		std::string filepath;
	};

	struct TiffMetadataOption {
		double resolutionX;
		double resolutionY;
		double positionX;
		double positionY;

		std::string desc;
		std::string artist;
		std::string host;
		std::string model;
		std::string software;
		std::string datetime;
	};

	struct Intensity {
		uint16_t min;
		uint16_t max;
	};

	class DataExporter_Converter_Format_LibTiff_API TiffWriter {
	public:
		explicit TiffWriter(const TiffMetadata3D& metadata);
		explicit TiffWriter(const TiffMetadata2D& metadata);
		~TiffWriter();

		auto SetOption(const TiffMetadataOption& option) -> void;
		auto SetIntensity(const Intensity& intensity) -> void;

		auto WriteLine(void* buffer) -> bool;
		
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
