#include <QDir>
#include <QStack>

#include "H5Cpp.h"

#include "TiffConverter.h"

#include "TcfH5GroupRoiLdmDataReader.h"
#include "TiffWriter.h"

namespace DataExporter::Converter::Format {
	struct TiffConverter::Impl {
		mutable int progMax = 0;
		mutable int progCur = 0;

		std::string desc;
		std::string artist;
		std::string host;
		std::string model;
		std::string software;
		std::string datetime;

		static auto ReadInt(const H5::Attribute& attr) -> uint64_t;
		static auto ReadDouble(const H5::Attribute& attr) -> double;
		static auto ReadString(const H5::Attribute& attr) -> std::string;

		auto CreateHT2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateHT3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateFL2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateFL3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateBF(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;

		auto CreateLDMHT2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateLDMHT3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateLDMFL2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateLDMFL3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
		auto CreateLDMBF(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool;
	};

	auto TiffConverter::Impl::ReadInt(const H5::Attribute& attr) -> uint64_t {
		uint64_t value;
		attr.read(H5::PredType::NATIVE_UINT64, &value);
		return value;
	}

	auto TiffConverter::Impl::ReadDouble(const H5::Attribute& attr) -> double {
		double value;
		attr.read(H5::PredType::NATIVE_DOUBLE, &value);
		return value;
	}

	auto TiffConverter::Impl::ReadString(const H5::Attribute& attr) -> std::string {
		std::string value;
		attr.read(attr.getDataType(), value);
		return value;
	}

	auto TiffConverter::Impl::CreateHT2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("2DMIP");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto rx = ReadDouble(group.openAttribute("ResolutionX"));
			const auto ry = ReadDouble(group.openAttribute("ResolutionY"));

			const auto buffer = std::make_unique<uint16_t[]>(x * y);

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto data = group.openDataSet(group.getObjnameByIdx(i));
				const auto output = QDir(dest).filePath(QString("%1_HT2D_%2.tiff").arg(QFileInfo(src).completeBaseName()).arg(i));
				const auto px = ReadDouble(data.openAttribute("PositionX"));
				const auto py = ReadDouble(data.openAttribute("PositionY"));
				const auto min = static_cast<uint16_t>(ReadDouble(data.openAttribute("RIMax")) * 10000.0);
				const auto max = static_cast<uint16_t>(ReadDouble(data.openAttribute("RIMin")) * 10000.0);

				TiffWriter writer(TiffMetadata2D { x, y, 2, false, output.toLocal8Bit() });
				writer.SetOption({ rx, ry, px, py, desc, artist, host, model, software, datetime });
				writer.SetIntensity({ min, max });

				data.read(buffer.get(), H5::PredType::NATIVE_UINT16);

				for (uint64_t j = 0; j < y; j++) {
					if (!writer.WriteLine(buffer.get() + (j * x)))
						return false;

					status->SetProgress(progCur++, progMax);
				}
			}
		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TiffConverter::Impl::CreateHT3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("3D");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto z = ReadInt(group.openAttribute("SizeZ"));
			const auto rx = ReadDouble(group.openAttribute("ResolutionX"));
			const auto ry = ReadDouble(group.openAttribute("ResolutionY"));

			const hsize_t mcount[] = { y, x };
			const hsize_t fcount[] = { 1ULL, y, x };
			const H5::DataSpace mspace { 2, mcount };

			const auto buffer = std::make_unique<uint16_t[]>(x * y);

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto data = group.openDataSet(group.getObjnameByIdx(i));
				const auto output = QDir(dest).filePath(QString("%1_HT3D_%2.tiff").arg(QFileInfo(src).completeBaseName()).arg(i));
				const auto px = ReadDouble(data.openAttribute("PositionX"));
				const auto py = ReadDouble(data.openAttribute("PositionY"));
				const auto min = static_cast<uint16_t>(ReadDouble(data.openAttribute("RIMax")) * 10000.0);
				const auto max = static_cast<uint16_t>(ReadDouble(data.openAttribute("RIMin")) * 10000.0);

				TiffWriter writer(TiffMetadata3D { x, y, z, 2, false, output.toLocal8Bit() });
				writer.SetOption({ rx, ry, px, py, desc, artist, host, model, software, datetime });
				writer.SetIntensity({ min, max });

				for (uint64_t j = 0; j < z; j++) {
					const auto space = data.getSpace();
					const hsize_t start[] = { j, 0ULL, 0ULL };
					space.selectHyperslab(H5S_SELECT_SET, fcount, start);

					data.read(buffer.get(), H5::PredType::NATIVE_UINT16, mspace, space);

					for (uint64_t k = 0; k < y; k++) {
						if (!writer.WriteLine(buffer.get() + (k * x)))
							return false;

						status->SetProgress(progCur++, progMax);
					}
				}
			}
		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TiffConverter::Impl::CreateFL2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("2DFLMIP");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto rx = ReadDouble(group.openAttribute("ResolutionX"));
			const auto ry = ReadDouble(group.openAttribute("ResolutionY"));

			const auto buffer = std::make_unique<uint16_t[]>(x * y);

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto channel = group.openGroup(group.getObjnameByIdx(i));

				for (int j = 0; j < channel.getNumObjs(); j++) {
					const auto data = channel.openDataSet(channel.getObjnameByIdx(j));
					const auto output = QDir(dest).filePath(QString("%1_FL2D_%2_%3.tiff").arg(QFileInfo(src).completeBaseName()).arg(QString::fromStdString(group.getObjnameByIdx(i))).arg(j));
					const auto px = ReadDouble(data.openAttribute("PositionX"));
					const auto py = ReadDouble(data.openAttribute("PositionY"));
					const auto min = static_cast<uint16_t>(ReadDouble(data.openAttribute("MaxIntensity")));
					const auto max = static_cast<uint16_t>(ReadDouble(data.openAttribute("MinIntensity")));

					TiffWriter writer(TiffMetadata2D { x, y, 2, false, output.toLocal8Bit() });
					writer.SetOption({ rx, ry, px, py, desc, artist, host, model, software, datetime });
					writer.SetIntensity({ min, max });

					data.read(buffer.get(), H5::PredType::NATIVE_UINT16);

					for (uint64_t k = 0; k < y; k++) {
						if (!writer.WriteLine(buffer.get() + (k * x)))
							return false;

						status->SetProgress(progCur++, progMax);
					}
				}
			}
		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TiffConverter::Impl::CreateFL3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("3DFL");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto z = ReadInt(group.openAttribute("SizeZ"));
			const auto rx = ReadDouble(group.openAttribute("ResolutionX"));
			const auto ry = ReadDouble(group.openAttribute("ResolutionY"));

			const hsize_t mcount[] { y, x };
			const hsize_t fcount[] { 1ULL, y, x };
			const H5::DataSpace mspace { 2, mcount };

			const auto buffer = std::make_unique<uint16_t[]>(x * y);

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto channel = group.openGroup(group.getObjnameByIdx(i));

				for (int j = 0; j < channel.getNumObjs(); j++) {
					const auto data = channel.openDataSet(channel.getObjnameByIdx(j));
					const auto output = QDir(dest).filePath(QString("%1_FL3D_%2_%3.tiff").arg(QFileInfo(src).completeBaseName()).arg(QString::fromStdString(group.getObjnameByIdx(i))).arg(j));
					const auto px = ReadDouble(data.openAttribute("PositionX"));
					const auto py = ReadDouble(data.openAttribute("PositionY"));
					const auto min = static_cast<uint16_t>(ReadDouble(data.openAttribute("MaxIntensity")));
					const auto max = static_cast<uint16_t>(ReadDouble(data.openAttribute("MinIntensity")));

					TiffWriter writer(TiffMetadata3D { x, y, z, 2, false, output.toLocal8Bit() });
					writer.SetOption({ rx, ry, px, py, desc, artist, host, model, software, datetime });
					writer.SetIntensity({ min, max });

					for (uint64_t k = 0; k < z; k++) {
						const auto fspace = data.getSpace();
						const hsize_t start[] = { k, 0ULL, 0ULL };
						fspace.selectHyperslab(H5S_SELECT_SET, fcount, start);

						data.read(buffer.get(), H5::PredType::NATIVE_UINT16, mspace, fspace);

						for (uint64_t l = 0; l < y; l++) {
							if (!writer.WriteLine(buffer.get() + (l * x)))
								return false;

							status->SetProgress(progCur++, progMax);
						}
					}
				}
			}
		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TiffConverter::Impl::CreateBF(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("BF");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto rx = ReadDouble(group.openAttribute("ResolutionX"));
			const auto ry = ReadDouble(group.openAttribute("ResolutionY"));

			const auto buffer1 = std::make_unique<uint8_t[]>(x * y * 3);
			const auto buffer2 = std::make_unique<uint8_t[]>(x * y * 3);

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto data = group.openDataSet(group.getObjnameByIdx(i));
				const auto output = QDir(dest).filePath(QString("%1_BF_%2.tiff").arg(QFileInfo(src).completeBaseName()).arg(i));
				const auto px = ReadDouble(data.openAttribute("PositionX"));
				const auto py = ReadDouble(data.openAttribute("PositionY"));

				TiffWriter writer(TiffMetadata2D { x, y, 1, true, output.toLocal8Bit() });
				writer.SetOption({ rx, ry, px, py, desc, artist, host, model, software, datetime });

				data.read(buffer1.get(), H5::PredType::NATIVE_UINT8);

				for (uint64_t j = 0; j < x; ++j) {
					for (uint64_t k = 0; k < y; ++k) {
						for (uint64_t z = 0; z < 3; ++z) {
							const auto src = j + (k * x) + (z * x * y);
							const auto dest = k + (j * y) + (z * x * y);

							buffer2[dest] = buffer1[src];
						}
					}
				}

				for (uint64_t j = 0; j < x; ++j) {
					for (uint64_t k = 0; k < y; ++k) {
						const auto srcR = k + j * y + 0 * (x * y);
						const auto srcG = k + j * y + 1 * (x * y);
						const auto srcB = k + j * y + 2 * (x * y);

						const auto destR = 3 * (j + k * x) + 0;
						const auto destG = 3 * (j + k * x) + 1;
						const auto destB = 3 * (j + k * x) + 2;

						buffer1[destR] = buffer2[srcR];
						buffer1[destG] = buffer2[srcG];
						buffer1[destB] = buffer2[srcB];
					}
				}

				for (uint64_t k = 0; k < y; k++) {
					if (!writer.WriteLine(buffer1.get() + (k * x * 3)))
						return false;

					status->SetProgress(progCur++, progMax);
				}
			}
		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TiffConverter::Impl::CreateLDMHT2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("2DMIP");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));

			for (int i = 0; i < group.getNumObjs(); i++) {
				auto data = group.openGroup(group.getObjnameByIdx(i));
				const auto output = QDir(dest).filePath(QString("%1_HT2D_%2.tiff").arg(QFileInfo(src).completeBaseName()).arg(i));
				const auto scalar = ReadInt(data.openAttribute("ScalarType"));
				const auto px = ReadDouble(data.openAttribute("PositionX"));
				const auto py = ReadDouble(data.openAttribute("PositionY"));
				const auto min = static_cast<uint16_t>(ReadDouble(data.openAttribute("RIMin")) * 10000.0);
				const auto max = static_cast<uint16_t>(ReadDouble(data.openAttribute("RIMax")) * 10000.0);

				TiffWriter writer(TiffMetadata2D { x, y, 2, false, output.toLocal8Bit() });
				writer.SetOption({ rx, ry, px, py, desc, artist, host, model, software, datetime });
				writer.SetIntensity({ min, max });

				TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
				reader.SetLdmDataGroup(data);
				reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0 }, TC::IO::Count { x, y } });
				reader.SetSamplingStep(1);

				const auto result = reader.Read();

				if (scalar == 0) {
					const auto buffer = result->GetDataPointerAs<uint16_t>();

					for (uint64_t j = 0; j < y; j++) {
						if (!writer.WriteLine(buffer + (j * x)))
							return false;

						status->SetProgress(progCur++, progMax);
					}
				} else if (scalar == 1) {
					const auto ptr = result->GetDataPointerAs<uint8_t>();
					const auto buffer = std::make_unique<uint16_t[]>(x * y);

					for (int k = 0; k < x * y; k++)
						buffer[k] = static_cast<uint16_t>(min + ptr[k] * 10);

					for (uint64_t j = 0; j < y; j++) {
						if (!writer.WriteLine(buffer.get() + (j * x)))
							return false;

						status->SetProgress(progCur++, progMax);
					}
				}
			}
		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TiffConverter::Impl::CreateLDMHT3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("3D");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto z = ReadInt(group.openAttribute("SizeZ"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));

			for (int i = 0; i < group.getNumObjs(); i++) {
				auto data = group.openGroup(group.getObjnameByIdx(i));
				const auto output = QDir(dest).filePath(QString("%1_HT3D_%2.tiff").arg(QFileInfo(src).completeBaseName()).arg(i));
				const auto scalar = ReadInt(data.openAttribute("ScalarType"));
				const auto px = ReadDouble(data.openAttribute("PositionX"));
				const auto py = ReadDouble(data.openAttribute("PositionY"));
				const auto min = static_cast<uint16_t>(ReadDouble(data.openAttribute("RIMin")) * 10000.0);
				const auto max = static_cast<uint16_t>(ReadDouble(data.openAttribute("RIMax")) * 10000.0);

				TiffWriter writer(TiffMetadata3D { x, y, z, 2, false, output.toLocal8Bit() });
				writer.SetOption({ rx, ry, px, py, desc, artist, host, model, software, datetime });
				writer.SetIntensity({ min, max });

				for (uint64_t j = 0; j < z; j++) {
					TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
					reader.SetLdmDataGroup(data);
					reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0, static_cast<int>(j) }, TC::IO::Count { x, y, 1 } });
					reader.SetSamplingStep(1);

					const auto result = reader.Read();

					if (scalar == 0) {
						const auto buffer = result->GetDataPointerAs<uint16_t>();

						for (uint64_t j = 0; j < y; j++) {
							if (!writer.WriteLine(buffer + (j * x)))
								return false;

							status->SetProgress(progCur++, progMax);
						}
					} else if (scalar == 1) {
						const auto ptr = result->GetDataPointerAs<uint8_t>();
						const auto buffer = std::make_unique<uint16_t[]>(x * y);

						for (int k = 0; k < x * y; k++)
							buffer[k] = static_cast<uint16_t>(min + ptr[k] * 10);

						for (uint64_t j = 0; j < y; j++) {
							if (!writer.WriteLine(buffer.get() + (j * x)))
								return false;

							status->SetProgress(progCur++, progMax);
						}
					}
				}
			}
		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TiffConverter::Impl::CreateLDMFL2D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("2DFLMIP");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto channel = group.openGroup(group.getObjnameByIdx(i));

				for (int j = 0; j < channel.getNumObjs(); j++) {
					auto data = channel.openGroup(channel.getObjnameByIdx(j));
					const auto output = QDir(dest).filePath(QString("%1_FL2D_%2_%3.tiff").arg(QFileInfo(src).completeBaseName()).arg(QString::fromStdString(group.getObjnameByIdx(i))).arg(i));
					const auto scalar = ReadInt(data.openAttribute("ScalarType"));
					const auto px = ReadDouble(data.openAttribute("PositionX"));
					const auto py = ReadDouble(data.openAttribute("PositionY"));
					const auto min = static_cast<uint16_t>(ReadDouble(data.openAttribute("MinIntensity")));
					const auto max = static_cast<uint16_t>(ReadDouble(data.openAttribute("MaxIntensity")));

					TiffWriter writer(TiffMetadata2D { x, y, 2, false, output.toLocal8Bit() });
					writer.SetOption({ rx, ry, px, py, desc, artist, host, model, software, datetime });
					writer.SetIntensity({ min, max });

					TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
					reader.SetLdmDataGroup(data);
					reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0 }, TC::IO::Count { x, y } });
					reader.SetSamplingStep(1);

					const auto result = reader.Read();

					if (scalar == 0) {
						const auto buffer = result->GetDataPointerAs<uint16_t>();

						for (uint64_t k = 0; k < y; k++) {
							if (!writer.WriteLine(buffer + (k * x)))
								return false;

							status->SetProgress(progCur++, progMax);
						}
					} else if (scalar == 1) {
						const auto ptr = result->GetDataPointerAs<uint8_t>();
						const auto buffer = std::make_unique<uint16_t[]>(x * y);

						for (int k = 0; k < x * y; k++)
							buffer[k] = min + ptr[k] * 10;

						for (uint64_t k = 0; k < y; k++) {
							if (!writer.WriteLine(buffer.get() + (k * x)))
								return false;

							status->SetProgress(progCur++, progMax);
						}
					}
				}
			}
		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TiffConverter::Impl::CreateLDMFL3D(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("3DFL");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto z = ReadInt(group.openAttribute("SizeZ"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));

			for (int i = 0; i < group.getNumObjs(); i++) {
				const auto channel = group.openGroup(group.getObjnameByIdx(i));

				for (int j = 0; j < channel.getNumObjs(); j++) {
					auto data = channel.openGroup(channel.getObjnameByIdx(j));
					const auto output = QDir(dest).filePath(QString("%1_FL3D_%2_%3.tiff").arg(QFileInfo(src).completeBaseName()).arg(QString::fromStdString(group.getObjnameByIdx(i))).arg(i));
					const auto scalar = ReadInt(data.openAttribute("ScalarType"));
					const auto px = ReadDouble(data.openAttribute("PositionX"));
					const auto py = ReadDouble(data.openAttribute("PositionY"));
					const auto min = static_cast<uint16_t>(ReadDouble(data.openAttribute("MinIntensity")));
					const auto max = static_cast<uint16_t>(ReadDouble(data.openAttribute("MaxIntensity")));

					TiffWriter writer(TiffMetadata3D { x, y, z, 2, false, output.toLocal8Bit() });
					writer.SetOption({ rx, ry, px, py, desc, artist, host, model, software, datetime });
					writer.SetIntensity({ min, max });

					for (uint64_t k = 0; k < z; k++) {
						TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
						reader.SetLdmDataGroup(data);
						reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0, static_cast<int>(k) }, TC::IO::Count { x, y, 1 } });
						reader.SetSamplingStep(1);

						const auto result = reader.Read();

						if (scalar == 0) {
							const auto buffer = result->GetDataPointerAs<uint16_t>();

							for (uint64_t l = 0; l < y; l++) {
								if (!writer.WriteLine(buffer + (l * x)))
									return false;

								status->SetProgress(progCur++, progMax);
							}
						} else if (scalar == 1) {
							const auto ptr = result->GetDataPointerAs<uint8_t>();
							const auto buffer = std::make_unique<uint16_t[]>(x * y);

							for (int k = 0; k < x * y; k++)
								buffer[k] = min + ptr[k] * 10;

							for (uint64_t l = 0; l < y; l++) {
								if (!writer.WriteLine(buffer.get() + (l * x)))
									return false;

								status->SetProgress(progCur++, progMax);
							}
						}
					}
				}
			}
		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	auto TiffConverter::Impl::CreateLDMBF(const H5::H5File& file, const QString& src, const QString& dest, const ConverterStatusPtr& status) const -> bool {
		H5::Group group;

		try {
			const auto data = file.openGroup("Data");
			group = data.openGroup("BF");
		} catch (const H5::Exception&) {
			return true;
		}

		try {
			const auto x = ReadInt(group.openAttribute("SizeX"));
			const auto y = ReadInt(group.openAttribute("SizeY"));
			const auto rx = static_cast<float>(ReadDouble(group.openAttribute("ResolutionX")));
			const auto ry = static_cast<float>(ReadDouble(group.openAttribute("ResolutionY")));

			for (int i = 0; i < group.getNumObjs(); i++) {
				auto data = group.openGroup(group.getObjnameByIdx(i));
				const auto output = QDir(dest).filePath(QString("%1_BF_%2.tiff").arg(QFileInfo(src).completeBaseName()).arg(i));
				const auto px = ReadDouble(data.openAttribute("PositionX"));
				const auto py = ReadDouble(data.openAttribute("PositionY"));

				TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
				reader.SetLdmDataGroup(data);
				reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0 }, TC::IO::Count { x, y } });
				reader.SetSamplingStep(1);
				reader.SetLdmDataIs2DStack(true);

				TiffWriter writer(TiffMetadata2D { x, y, 1, true, output.toLocal8Bit() });
				writer.SetOption({ rx, ry, px, py, desc, artist, host, model, software, datetime });

				const auto result = reader.Read();
				const auto buffer1 = result->GetDataPointerAs<uint8_t>();
				const auto buffer2 = std::make_unique<uint8_t[]>(x * y * 3);

				for (uint64_t j = 0; j < x; ++j) {
					for (uint64_t k = 0; k < y; ++k) {
						for (uint64_t z = 0; z < 3; ++z) {
							const auto s = j + (k * x) + (z * x * y);
							const auto d = k + (j * y) + (z * x * y);

							buffer2[d] = buffer1[s];
						}
					}
				}

				for (uint64_t j = 0; j < x; ++j) {
					for (uint64_t k = 0; k < y; ++k) {
						const auto srcR = k + j * y + 0 * (x * y);
						const auto srcG = k + j * y + 1 * (x * y);
						const auto srcB = k + j * y + 2 * (x * y);

						const auto destR = 3 * (j + k * x) + 0;
						const auto destG = 3 * (j + k * x) + 1;
						const auto destB = 3 * (j + k * x) + 2;

						buffer1[destR] = buffer2[srcR];
						buffer1[destG] = buffer2[srcG];
						buffer1[destB] = buffer2[srcB];
					}
				}

				for (uint64_t k = 0; k < y; k++) {
					if (!writer.WriteLine(buffer1 + (k * x * 3)))
						return false;

					status->SetProgress(progCur++, progMax);
				}
			}
		} catch (const H5::Exception&) {
			return false;
		}

		return true;
	}

	TiffConverter::TiffConverter() : IConverter(), d(new Impl) {}

	TiffConverter::~TiffConverter() = default;

	auto TiffConverter::Start(const QString& src, const QString& dest, const ConverterStatusPtr& status) -> void {
		H5::Exception::dontPrint();

		if (const QFileInfo file(src); !file.exists())
			throw ConverterException("TCF Not Found");

		if (const QFileInfo save(dest); save.exists() && !save.isDir())
			throw ConverterException("Output Path Not Accessible");

		if (const QFileInfo save(dest); !save.exists()) {
			if (const QDir dir(dest); !dir.mkpath(dest))
				throw ConverterException("Output Path Not Accessible");
		}

		H5::H5File file;
		bool isLDM = false;

		try {
			const auto euc = src.toUtf8();
			file.openFile(euc.constData(), H5F_ACC_RDONLY);

			if (file.attrExists("Description"))
				d->desc = d->ReadString(file.openAttribute("Description"));
			if (file.attrExists("UserID"))
				d->artist = d->ReadString(file.openAttribute("UserID"));
			if (file.attrExists("DeviceHost"))
				d->host = d->ReadString(file.openAttribute("DeviceHost"));
			if (file.attrExists("DeviceModelType"))
				d->model = d->ReadString(file.openAttribute("DeviceModelType"));
			if (file.attrExists("SoftwareVersion"))
				d->software = d->ReadString(file.openAttribute("SoftwareVersion"));
			if (file.attrExists("CreateDate"))
				d->datetime = d->ReadString(file.openAttribute("CreateDate"));
		} catch (const H5::Exception&) {
			throw ConverterException("TCF Not Accessible");
		}

		try {
			QStack<H5::Group> stack;

			const auto data = file.openGroup("Data");
			stack.push(data);

			while (!stack.isEmpty()) {
				auto group = stack.pop();

				for (int i = 0; i < group.getNumObjs(); i++) {
					const auto id = group.getObjnameByIdx(i);

					if (group.getObjTypeByIdx(i) == H5G_GROUP) {
						const auto elem = group.openGroup(id);

						int z = 1;
						int y = 1;
						int count = 0;
						int channel = 1;

						if (elem.attrExists("SizeZ"))
							z = d->ReadInt(elem.openAttribute("SizeZ"));
						if (elem.attrExists("SizeY"))
							y = d->ReadInt(elem.openAttribute("SizeY"));
						if (elem.attrExists("DataCount"))
							count = d->ReadInt(elem.openAttribute("DataCount"));
						if (elem.attrExists("Channels"))
							channel = d->ReadInt(elem.openAttribute("Channels"));

						const auto c = z * y * count * channel;
						d->progMax += c;

						if (c == 0)
							stack.push(elem);
					}
				}
			}

			if (data.attrExists("StructureTypeName"))
				isLDM = d->ReadString(data.openAttribute("StructureTypeName")) == "LDM";
		} catch (const H5::Exception&) {
			throw ConverterException("Error Counting Data");
		}

		if (isLDM) {
			if (!d->CreateLDMHT3D(file, src, dest, status))
				throw ConverterException("Error in HT3D");

			if (!d->CreateLDMHT2D(file, src, dest, status))
				throw ConverterException("Error in HT2D");

			if (!d->CreateLDMFL3D(file, src, dest, status))
				throw ConverterException("Error in FL3D");

			if (!d->CreateLDMFL2D(file, src, dest, status))
				throw ConverterException("Error in FL2D");

			if (!d->CreateLDMBF(file, src, dest, status))
				throw ConverterException("Error in BF");
		} else {
			if (!d->CreateHT3D(file, src, dest, status))
				throw ConverterException("Error in HT3D");

			if (!d->CreateHT2D(file, src, dest, status))
				throw ConverterException("Error in HT2D");

			if (!d->CreateFL3D(file, src, dest, status))
				throw ConverterException("Error in FL3D");

			if (!d->CreateFL2D(file, src, dest, status))
				throw ConverterException("Error in FL2D");

			if (!d->CreateBF(file, src, dest, status))
				throw ConverterException("Error in BF");
		}
	}
}
