#include "ConverterFinder.h"

#include "TCRaw.h"
#include "TiffConverter.h"

namespace DataExporter::Converter {
	struct ConverterFinder::Impl { };

	ConverterFinder::ConverterFinder() : IConverterFinder(), d(new Impl) {}

	ConverterFinder::~ConverterFinder() = default;

	auto ConverterFinder::GetConverter(const QString& format, const QMap<QString, QString>& options) -> ConverterPtr {
		if (const auto f = format.toLower(); f == "raw")
			return std::make_shared<Format::TCRaw>();
		else if (f == "tiff")
			return std::make_shared<Format::TiffConverter>();

		return {};
	}
}
