#pragma once

#include "IConverterFinder.h"

#include "DataExporter.ConverterExport.h"

namespace DataExporter::Converter {
	class DataExporter_Converter_API ConverterFinder final : public IConverterFinder {
	public:
		ConverterFinder();
		~ConverterFinder() override;
		
		auto GetConverter(const QString& format, const QMap<QString, QString>& options) -> ConverterPtr override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
