#pragma once

#include "IService.h"

#include "DataExporter.UIModelExport.h"

namespace DataExporter {
	enum class TaskStatus {
		Pending,
		Progressing,
		Finished
	};

	class DataExporter_UIModel_API ITaskItem : public virtual IService {
	public:
		virtual auto GetTCFPath() const -> QString = 0;
		virtual auto GetSavePath() const -> QString = 0;
		virtual auto GetFormat() const -> QString = 0;

		virtual auto GetProgress() const -> int = 0;
		virtual auto GetStatus() const -> TaskStatus = 0;
		virtual auto GetMessage() const -> QString = 0;
	};
}