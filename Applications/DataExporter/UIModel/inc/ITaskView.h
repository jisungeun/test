#pragma once

#include "IService.h"

#include "ITask.h"

#include "DataExporter.UIModelExport.h"

namespace DataExporter {
	class DataExporter_UIModel_API ITaskView : public virtual Tomocube::IService {
	public:
		virtual auto Show() -> void = 0;

		virtual auto AddTask(const TaskPtr& task) -> void = 0;
		virtual auto UpdateTask(const TaskPtr& task) -> void = 0;
	};
}
