#pragma once

#include "IHostedService.h"

#include "ITaskItem.h"

#include "DataExporter.UIModelExport.h"

namespace DataExporter {
	class DataExporter_UIModel_API IAppWindow : public virtual IHostedService {
	public:
		virtual auto GetTaskItemList() const -> QList<ITaskItem*> = 0;
	};
}
