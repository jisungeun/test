#include "DataHandler.h"

#include <QDebug>
#include <QDir>
#include <QMutexLocker>
#include <QWaitCondition>

#include "IFormat.h"
#include "IScheduler.h"

#include "Data.h"
#include "TCFReader.h"

#include "NpyFormat.h"
#include "RawFormat.h"
#include "TestFormat.h"
#include "TiffFormat.h"

namespace DataExporter::DataHandler {
	struct DataHandler::Impl {
		IServiceProvider* provider = nullptr;
		EventContainer events;
		QWaitCondition waiter;
		QMutex mutex;

		std::shared_ptr<Data> current = nullptr;
		bool disposed = false;

		static auto GetFormat(const QString& name) -> Service<IFormat>;
	};

	auto DataHandler::Impl::GetFormat(const QString& name) -> Service<IFormat> {
		Service<IFormat> format = nullptr;

		if (const auto lower = name.toLower(); lower == "test")
			format = std::make_shared<Format::TestFormat>();
		else if (lower == "raw")
			format = std::make_shared<Format::RawFormat>();
		else if (lower == "tiff")
			format = std::make_shared<Format::TiffFormat>();
		else if (lower == "npy")
			format = std::make_shared<Format::NpyFormat>();

		return format;
	}

	DataHandler::DataHandler(IServiceProvider* provider) : IDataHandler(), d(new Impl) {
		d->provider = provider;
	}

	DataHandler::~DataHandler() = default;

	auto DataHandler::Start(const QStringList& args) -> bool {
		if (const auto scheduler = d->provider->GetService<IScheduler>())
			scheduler->OnScheduled += { d->events, this, &DataHandler::OnScheduled };

		start();

		return true;
	}

	auto DataHandler::Stop() -> void { }

	auto DataHandler::GetCurrentItem() const -> IData* {
		return d->current.get();
	}

	auto DataHandler::OnScheduled([[maybe_unused]] IService* sender, [[maybe_unused]] ITask* task) -> void {
		QMutexLocker locker(&d->mutex);
		d->waiter.wakeAll();
	}

	auto DataHandler::run() -> void {
		if (const auto scheduler = d->provider->GetService<IScheduler>()) {
			{
				QMutexLocker locker(&d->mutex);
				d->waiter.wait(&d->mutex);
			}

			while (scheduler->HasNext()) {
				if (auto* task = scheduler->NextTask()) {
					d->current = std::make_shared<Data>(task);

					Run([this, current = d->current] {
						OnStarted(this, current.get());
					});

					QDir dir(task->GetSavePath());

					if (!dir.exists())
						dir.mkpath(".");

					if (dir.exists()) {
						const auto tcf = std::make_shared<TCFReader>(task->GetTCFPath());
						const auto path = QString("%1/%2")
								.arg(task->GetSavePath())
								.arg(tcf->GetFilename());
						const auto htName = QString("_HT%1_%2.") + task->GetSaveFormat();
						const auto flName = QString("_FL%1_CH%2_%3.") + task->GetSaveFormat();
						const auto bfName = QString("_BF_%2.") + task->GetSaveFormat();

						connect(d->current.get(), &Data::Updated, [this] {
							Run([this, current = d->current] {
								OnUpdated(this, current.get());
							}, false);
						});

						if (const auto format = d->GetFormat(task->GetSaveFormat())) {
							format->SetTCFData(tcf.get());
							format->SetZRange(task->GetZRangeMin(), task->GetZRangeMax());

							if (tcf->ContainsHT()) {
								const auto count = tcf->GetHTDataCount();
								const auto max = count * 2;
								auto cur = 0;

								d->current->SetMessage("HT");
								d->current->SetProgress(0, max);

								for (auto i = 0; i < count; i++) {
									if (!task->IsExclusiveHT2D() || task->ContainsExclusiveHT2D(i)) {
										try {
											if (!format->ExportHT2D(QString("%1%2")
																	.arg(path)
																	.arg(htName
																		.arg("2D")
																		.arg(i)), i))
												qInfo() << "HT2D" << i;
										} catch (const std::exception& ex) {
											d->current->ThrowError(ex.what());
										}
									}

									d->current->SetProgress(cur++, max);

									if (!task->IsExclusiveHT3D() || task->ContainsExclusiveHT3D(i)) {
										try {
											if (!format->ExportHT3D(QString("%1%2")
																	.arg(path)
																	.arg(htName
																		.arg("3D")
																		.arg(i)), i))
												qInfo() << "HT3D" << i;
										} catch (const std::exception& ex) {
											d->current->ThrowError(ex.what());
										}
									}

									d->current->SetProgress(cur++, max);
								}
							}

							if (tcf->ContainsFL()) {
								const auto channel = tcf->GetFLChannelCount();
								auto max = 0;
								auto cur = 0;

								d->current->SetMessage("FL");
								d->current->SetProgress(0, max);

								for (auto i = 0; i < channel; i++)
									max += tcf->GetFLDataCount(i) * 2;

								for (auto i = 0; i < channel; i++) {
									const auto count = tcf->GetFLDataCount(i);

									for (auto j = 0; j < count; j++) {
										if (!task->IsExclusiveFL2D() || task->ContainsExclusiveFL2D(j)) {
											try {
												if (!format->ExportFL2D(QString("%1%2")
																		.arg(path)
																		.arg(flName
																			.arg("2D")
																			.arg(i)
																			.arg(j)), i, j))
													qInfo() << "FL2D CH" << i << j;
											} catch (const std::exception& ex) {
												d->current->ThrowError(ex.what());
											}
										}

										d->current->SetProgress(cur++, max);

										if (!task->IsExclusiveFL3D() || task->ContainsExclusiveFL3D(j)) {
											try {
												if (!format->ExportFL3D(QString("%1%2")
																		.arg(path)
																		.arg(flName
																			.arg("3D")
																			.arg(i)
																			.arg(j)), i, j))
													qInfo() << "FL3D CH" << i << j;
											} catch (const std::exception& ex) {
												d->current->ThrowError(ex.what());
											}
										}

										d->current->SetProgress(cur++, max);
									}
								}
							}

							if (tcf->ContainsBF()) {
								const auto count = tcf->GetBFDataCount();
								const auto max = count;
								auto cur = 0;

								d->current->SetMessage("BF");
								d->current->SetProgress(0, max);

								for (auto i = 0; i < count; i++) {
									if (!task->IsExclusiveBF() || task->ContainsExclusiveBF(i)) {
										try {
											if (!format->ExportHT2D(QString("%1%2")
																	.arg(path)
																	.arg(bfName
																		.arg(i)), i))
												qInfo() << "BF" << i;
										} catch (const std::exception& ex) {
											d->current->ThrowError(ex.what());
										}
									}

									d->current->SetProgress(cur++, max);
								}
							}
						} else
							d->current->ThrowError(QString("Format \'%1\' is not supported.").arg(task->GetSaveFormat()));
					} else
						d->current->ThrowError(QString("Save path is not accessible."));

					Run([this, current = d->current] {
						OnFinished(this, current.get());
					});
				}
			}

			Run([this] {
				OnCleared(this);
			});
		}
	}
}
