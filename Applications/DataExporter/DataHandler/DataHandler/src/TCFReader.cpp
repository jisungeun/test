#include "TCFReader.h"

#include "H5Cpp.h"

#include <QDateTime>
#include <QFileInfo>

#include "TcfH5GroupRoiLdmDataReader.h"
#include "TCFHelper.h"

namespace DataExporter::DataHandler {
	namespace {
		constexpr const char* CreateDate = "CreateDate";
		constexpr const char* DataID = "DataID";
		constexpr const char* Description = "Description";
		constexpr const char* DeviceHost = "DeviceHost";
		constexpr const char* DeviceModelType = "DeviceModelType";
		constexpr const char* DeviceSerial = "DeviceSerial";
		constexpr const char* DeviceSoftwareVersion = "DeviceSoftwareVersion";
		constexpr const char* FormatVersion = "FormatVersion";
		constexpr const char* RecordTime = "RecordTime";
		constexpr const char* SoftwareVersion = "SoftwareVersion";
		constexpr const char* Title = "Title";
		constexpr const char* UniqueID = "UniqueID";
		constexpr const char* UserID = "UserID";

		constexpr const char* SizeX = "SizeX";
		constexpr const char* SizeY = "SizeY";
		constexpr const char* SizeZ = "SizeZ";

		constexpr const char* ResolutionX = "ResolutionX";
		constexpr const char* ResolutionY = "ResolutionY";
		constexpr const char* ResolutionZ = "ResolutionZ";

		constexpr const char* PositionC = "PositionC";
		constexpr const char* PositionX = "PositionX";
		constexpr const char* PositionY = "PositionY";
		constexpr const char* PositionZ = "PositionZ";

		constexpr const char* RIMin = "RIMin";
		constexpr const char* RIMax = "RIMax";

		constexpr const char* MinIntensity = "MinIntensity";
		constexpr const char* MaxIntensity = "MaxIntensity";

		constexpr const char* ScalarType = "ScalarType";
	}

	struct TCFReader::Impl {
		bool ldm = false;
		bool ldmChecked = false;
		QByteArray tcf;
		QVariantMap attrMap;

		auto GetAttrMap() const -> QVariantMap {
			try {
				H5::H5File file;
				file.openFile(tcf, H5F_ACC_RDONLY);

				return ReadAttribute(file);
			} catch (const H5::Exception&) { }

			return {};
		}

		auto GetGroupCount(const QString& path) const -> int {
			try {
				H5::H5File file;
				file.openFile(tcf, H5F_ACC_RDONLY);

				const auto group = file.openGroup(path.toUtf8());
				return static_cast<int>(group.getNumObjs());
			} catch (const H5::Exception&) { }

			return 0;
		}

		auto IsLDM() -> bool {
			if (ldmChecked)
				return ldm;

			try {
				H5::H5File file;
				file.openFile(tcf, H5F_ACC_RDONLY);

				const auto group = file.openGroup("Data");

				if (group.attrExists("StructureType"))
					ldm = ReadInt64(group.openAttribute("StructureType")) == 1LL;
			} catch (const H5::Exception&) { }

			return ldm;
		}

		static auto ToBigScalar(const H5::DataSet& dataset, uint64_t size, const H5::DataSpace* mspace = nullptr, const H5::DataSpace* fspace = nullptr) -> std::shared_ptr<uint16_t[]> {
			if (dataset.attrExists(ScalarType)) {
				if (const auto scalar = ReadInt64(dataset.openAttribute(ScalarType)); scalar == 1) {
					const auto ht = dataset.attrExists(RIMin);
					const auto fl = dataset.attrExists(MinIntensity);
					const auto min = ht ? static_cast<uint16_t>(ReadDouble(dataset.openAttribute(RIMin)) * 10000.0) : static_cast<uint16_t>(ReadDouble(dataset.openAttribute(MinIntensity)));
					const auto data = std::make_unique<uint8_t[]>(size);
					auto buffer = std::make_unique<uint16_t[]>(size);

					if (mspace != nullptr && fspace != nullptr)
						dataset.read(data.get(), H5::PredType::NATIVE_UINT8, *mspace, *fspace);
					else
						dataset.read(data.get(), H5::PredType::NATIVE_UINT8);

					if (ht) {
						for (auto k = 0ULL; k < size; k++)
							buffer[k] = static_cast<uint16_t>(min + data[k] * 10);
					} else if (fl) {
						for (auto k = 0ULL; k < size; k++)
							buffer[k] = static_cast<uint16_t>(min + data[k]);
					}

					return std::move(buffer);
				}
			}

			return nullptr;
		}

		static auto ToBigScalarLdm(const H5::H5Object& dataset, const TC::IO::MemoryChunk::Pointer& result, uint64_t size) -> std::shared_ptr<uint16_t[]> {
			if (dataset.attrExists(ScalarType)) {
				if (const auto scalar = ReadInt64(dataset.openAttribute(ScalarType)); scalar == 1) {
					const auto ht = dataset.attrExists(RIMin);
					const auto fl = dataset.attrExists(MinIntensity);
					const auto min = ht ? static_cast<uint16_t>(ReadDouble(dataset.openAttribute(RIMin)) * 10000.0) : static_cast<uint16_t>(ReadDouble(dataset.openAttribute(MinIntensity)));
					const auto data = std::any_cast<std::shared_ptr<uint8_t[]>>(result->GetData());
					auto buffer = std::make_unique<uint16_t[]>(size);

					for (auto k = 0ULL; k < size; k++)
						buffer[k] = static_cast<uint16_t>(min + data[k]);

					if (ht) {
						for (auto k = 0ULL; k < size; k++)
							buffer[k] = static_cast<uint16_t>(min + data[k] * 10);
					} else if (fl) {
						for (auto k = 0ULL; k < size; k++)
							buffer[k] = static_cast<uint16_t>(min + data[k]);
					}

					return std::move(buffer);
				}
			}

			return nullptr;
		}
	};

	TCFReader::TCFReader(const QString& path) : ITCFData(), d(new Impl) {
		d->tcf = path.toUtf8();

		H5E_auto2_t func = nullptr;
		void* data = nullptr;

		H5::Exception::getAutoPrint(func, &data);
		H5::Exception::dontPrint();
	}

	TCFReader::~TCFReader() = default;

	auto TCFReader::GetFilename() const -> QString {
		return QFileInfo(d->tcf).completeBaseName();
	}

	auto TCFReader::GetCreationDateTime() const -> QDateTime {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(CreateDate))
			return QDateTime::fromString(d->attrMap[CreateDate].toString(), "yyyy-MM-dd HH:mm:ss");

		return {};
	}

	auto TCFReader::GetDataID() const -> QString {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(DataID))
			return d->attrMap[DataID].toString();

		return {};
	}

	auto TCFReader::GetDescription() const -> QString {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(Description))
			return d->attrMap[Description].toString();

		return {};
	}

	auto TCFReader::GetDeviceHost() const -> QString {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(DeviceHost))
			return d->attrMap[DeviceHost].toString();

		return {};
	}

	auto TCFReader::GetDeviceModelType() const -> QString {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(DeviceModelType))
			return d->attrMap[DeviceModelType].toString();

		return {};
	}

	auto TCFReader::GetDeviceSerial() const -> QString {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(DeviceSerial))
			return d->attrMap[DeviceSerial].toString();

		return {};
	}

	auto TCFReader::GetDeviceSoftwareVersion() const -> QString {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(DeviceSoftwareVersion))
			return d->attrMap[DeviceSoftwareVersion].toString();

		return {};
	}

	auto TCFReader::GetFormatVersion() const -> QString {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(FormatVersion))
			return d->attrMap[FormatVersion].toString();

		return {};
	}

	auto TCFReader::GetRecordDateTime() const -> QDateTime {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(RecordTime))
			return QDateTime::fromString(d->attrMap[RecordTime].toString(), "yyyy-MM-dd HH:mm:ss.zzz");

		return {};
	}

	auto TCFReader::GetSoftwareVersion() const -> QString {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(SoftwareVersion))
			return d->attrMap[SoftwareVersion].toString();

		return {};
	}

	auto TCFReader::GetTitle() const -> QString {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(Title))
			return d->attrMap[Title].toString();

		return {};
	}

	auto TCFReader::GetUniqueID() const -> QString {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(UniqueID))
			return d->attrMap[UniqueID].toString();

		return {};
	}

	auto TCFReader::GetUserID() const -> QString {
		if (d->attrMap.isEmpty())
			d->attrMap = d->GetAttrMap();

		if (d->attrMap.contains(UserID))
			return d->attrMap[UserID].toString();

		return {};
	}

	auto TCFReader::ContainsHT() const -> bool {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto path = QString("Data/3D").toUtf8();
			return file.exists(path);
		} catch (const H5::Exception&) { }

		return false;
	}

	auto TCFReader::ContainsFL() const -> bool {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto path = QString("Data/3DFL").toUtf8();
			return file.exists(path);
		} catch (const H5::Exception&) { }

		return false;
	}

	auto TCFReader::ContainsBF() const -> bool {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto path = QString("Data/BF").toUtf8();
			return file.exists(path);
		} catch (const H5::Exception&) { }

		return false;
	}

	auto TCFReader::GetHTDataCount() const -> int {
		return d->GetGroupCount(QString("Data/3D").toUtf8());
	}

	auto TCFReader::GetHTResolution() -> Resolution3D {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto path = QString("Data/3D").toUtf8();
			const auto group = file.openGroup(path);
			const auto attr = ReadAttribute(group);

			return { attr[ResolutionX].toDouble(), attr[ResolutionY].toDouble(), attr[ResolutionZ].toDouble() };
		} catch (const H5::Exception&) { }

		return {};
	}

	auto TCFReader::GetHTSize() -> Size3D {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto path = QString("Data/3D").toUtf8();
			const auto group = file.openGroup(path);
			const auto attr = ReadAttribute(group);

			return { attr[SizeX].toInt(), attr[SizeY].toInt(), attr[SizeZ].toInt() };
		} catch (const H5::Exception&) { }

		return {};
	}

	auto TCFReader::GetHTPosition(int timestep) -> Position {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto index = QString("%1").arg(timestep, 6, 10, QChar('0'));
			const auto path = QString("Data/3D/%1").arg(index).toUtf8();
			const auto group = file.openGroup(path);
			const auto attr = ReadAttribute(group);

			return { attr[PositionC].toDouble(), attr[PositionX].toDouble(), attr[PositionY].toDouble(), attr[PositionZ].toDouble() };
		} catch (const H5::Exception&) { }

		return {};
	}

	auto TCFReader::GetHTRange(int timestep) -> DataRange {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto path = QString("Data/3D/%1").arg(timestep, 6, 10, QChar('0')).toUtf8();

			try {
				const auto group = file.openDataSet(path);
				const auto attr = ReadAttribute(group);

				return { attr[RIMin].toDouble() * 10000.0, attr[RIMax].toDouble() * 10000.0 };
			} catch (const H5::Exception&) { }

			try {
				const auto group = file.openGroup(path);
				const auto attr = ReadAttribute(group);

				return { attr[RIMin].toDouble() * 10000.0, attr[RIMax].toDouble() * 10000.0 };
			} catch (const H5::Exception&) { }
		} catch (const H5::Exception&) { }

		return {};
	}

	auto TCFReader::GetFLChannelCount() const -> int {
		return d->GetGroupCount(QString("Data/3DFL").toUtf8());
	}

	auto TCFReader::GetFLSize() -> Size3D {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto path = QString("Data/3DFL").toUtf8();
			const auto group = file.openGroup(path);
			const auto attr = ReadAttribute(group);

			return { attr[SizeX].toInt(), attr[SizeY].toInt(), attr[SizeZ].toInt() };
		} catch (const H5::Exception&) { }

		return {};
	}

	auto TCFReader::GetFLResolution() -> Resolution3D {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto path = QString("Data/3DFL").toUtf8();
			const auto group = file.openGroup(path);
			const auto attr = ReadAttribute(group);

			return { attr[ResolutionX].toDouble(), attr[ResolutionY].toDouble(), attr[ResolutionZ].toDouble() };
		} catch (const H5::Exception&) { }

		return {};
	}

	auto TCFReader::GetFLDataCount(int channel) const -> int {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto parent = file.openGroup("Data/3DFL");
			const auto name = QString::fromStdString(parent.getObjnameByIdx(channel));
			const auto path = QString("Data/3DFL/%1").arg(name).toUtf8();
			const auto group = file.openGroup(path);
			return static_cast<int>(group.getNumObjs());
		} catch (const H5::Exception&) { }

		return 0;
	}

	auto TCFReader::GetFLPosition(int channel, int timestep) -> Position {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto parent = file.openGroup("Data/3DFL");
			const auto name = QString::fromStdString(parent.getObjnameByIdx(channel));
			const auto index = QString("%1").arg(timestep, 6, 10, QChar('0'));
			const auto path = QString("Data/BF/%1/%2").arg(name).arg(index).toUtf8();
			const auto group = file.openGroup(path);
			const auto attr = ReadAttribute(group);

			return { attr[PositionC].toDouble(), attr[PositionX].toDouble(), attr[PositionY].toDouble(), attr[PositionZ].toDouble() };
		} catch (const H5::Exception&) { }

		return {};
	}

	auto TCFReader::GetFLRange(int channel, int timestep) -> DataRange {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto parent = file.openGroup("Data/3DFL");
			const auto name = QString::fromStdString(parent.getObjnameByIdx(channel));
			const auto path = QString("Data/3DFL/%1/%2").arg(name).arg(timestep, 6, 10, QChar('0')).toUtf8();

			try {
				const auto group = file.openDataSet(path);
				const auto attr = ReadAttribute(group);

				return { attr[MinIntensity].toDouble(), attr[MaxIntensity].toDouble() };
			} catch (const H5::Exception&) { }

			try {
				const auto group = file.openGroup(path);
				const auto attr = ReadAttribute(group);

				return { attr[MinIntensity].toDouble(), attr[MaxIntensity].toDouble() };
			} catch (const H5::Exception&) { }
		} catch (const H5::Exception&) { }

		return {};
	}

	auto TCFReader::GetBFDataCount() const -> int {
		return d->GetGroupCount(QString("Data/BF").toUtf8());
	}

	auto TCFReader::GetBFResolution() const -> Resolution2D {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto path = QString("Data/BF").toUtf8();
			const auto group = file.openGroup(path);
			const auto attr = ReadAttribute(group);

			return { attr[ResolutionX].toDouble(), attr[ResolutionY].toDouble() };
		} catch (const H5::Exception&) { }

		return {};
	}

	auto TCFReader::GetBFSize() const -> Size2D {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto path = QString("Data/BF").toUtf8();
			const auto group = file.openGroup(path);
			const auto attr = ReadAttribute(group);

			Size2D size;
			size.rgb = true;
			size.x = attr[SizeX].toInt();
			size.y = attr[SizeY].toInt();

			return size;
		} catch (const H5::Exception&) { }

		return {};
	}

	auto TCFReader::GetBFPosition(int timestep) -> Position {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto index = QString("%1").arg(timestep, 6, 10, QChar('0'));
			const auto path = QString("Data/BF/%1").arg(index).toUtf8();
			const auto group = file.openGroup(path);
			const auto attr = ReadAttribute(group);

			return { attr[PositionC].toDouble(), attr[PositionX].toDouble(), attr[PositionY].toDouble(), attr[PositionZ].toDouble() };
		} catch (const H5::Exception&) { }

		return {};
	}

	auto TCFReader::LoadHT2D(int timestep) -> std::shared_ptr<uint16_t[]> {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto index = QString("%1").arg(timestep, 6, 10, QChar('0')).toUtf8();
			const auto path = QString("Data/2DMIP").toUtf8();
			const auto group = file.openGroup(path);
			const auto x = ReadInt64(group.openAttribute(SizeX));
			const auto y = ReadInt64(group.openAttribute(SizeY));

			if (d->IsLDM()) {
				const auto dataset = group.openGroup(index);
				TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
				reader.SetLdmDataGroup(const_cast<H5::Group&>(dataset));
				reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0 }, TC::IO::Count { static_cast<size_t>(x), static_cast<size_t>(y) } });
				reader.SetSamplingStep(1);

				if (const auto result = reader.Read()) {
					if (auto data = d->ToBigScalarLdm(dataset, result, x * y))
						return data;

					return std::any_cast<std::shared_ptr<uint16_t[]>>(result->GetData());
				}
			} else {
				const auto dataset = group.openDataSet(index);

				if (auto data = d->ToBigScalar(dataset, x * y))
					return data;

				auto buffer = std::make_unique<uint16_t[]>(x * y);
				dataset.read(buffer.get(), H5::PredType::NATIVE_UINT16);
				return std::move(buffer);
			}
		} catch (const H5::Exception&) { } catch (const std::bad_any_cast&) { }

		return {};
	}

	auto TCFReader::LoadHT3D(int timestep, int zMin, int zMax) -> std::shared_ptr<uint16_t[]> {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto index = QString("%1").arg(timestep, 6, 10, QChar('0')).toUtf8();
			const auto path = QString("Data/3D").toUtf8();
			const auto group = file.openGroup(path);
			const auto x = static_cast<uint64_t>(ReadInt64(group.openAttribute(SizeX)));
			const auto y = static_cast<uint64_t>(ReadInt64(group.openAttribute(SizeY)));
			const auto z = static_cast<uint64_t>(ReadInt64(group.openAttribute(SizeZ)));
			const auto zTo = static_cast<uint64_t>(zMax > -1 ? std::min(static_cast<uint64_t>(zMax), z) : z);
			const auto zFrom = static_cast<uint64_t>(zMin > -1 ? std::min(zMin, static_cast<int>(zTo)) : 0);
			const auto zMid = zTo - zFrom;

			if (d->IsLDM()) {
				const auto dataset = group.openGroup(index);
				TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
				reader.SetLdmDataGroup(const_cast<H5::Group&>(dataset));
				reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0, static_cast<int64_t>(zFrom) }, TC::IO::Count { x, y, zMid } });
				reader.SetSamplingStep(1);

				if (const auto result = reader.Read()) {
					if (auto data = d->ToBigScalarLdm(dataset, result, x * y * zMid))
						return data;

					return std::any_cast<std::shared_ptr<uint16_t[]>>(result->GetData());
				}
			} else {
				const auto dataset = group.openDataSet(index);

				const uint64_t mcount[] = { zMid, y, x };
				const H5::DataSpace mspace { 3, mcount };

				const auto space = dataset.getSpace();
				const uint64_t start[] = { zFrom, 0ULL, 0ULL };
				const uint64_t fcount[] = { zMid, y, x };
				space.selectHyperslab(H5S_SELECT_SET, fcount, start);

				if (auto data = d->ToBigScalar(dataset, x * y * zMid, &mspace, &space))
					return data;

				auto buffer = std::make_unique<uint16_t[]>(x * y * zMid);
				dataset.read(buffer.get(), H5::PredType::NATIVE_UINT16, mspace, space);
				return std::move(buffer);
			}
		} catch (const H5::Exception&) { } catch (const std::bad_any_cast&) { }

		return {};
	}

	auto TCFReader::LoadHT3D(int timestep, int zIndex) -> std::shared_ptr<uint16_t[]> {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto index = QString("%1").arg(timestep, 6, 10, QChar('0')).toUtf8();
			const auto path = QString("Data/3D").toUtf8();
			const auto group = file.openGroup(path);
			const auto x = ReadInt64(group.openAttribute(SizeX));
			const auto y = ReadInt64(group.openAttribute(SizeY));

			if (d->IsLDM()) {
				const auto dataset = group.openGroup(index);
				TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
				reader.SetLdmDataGroup(const_cast<H5::Group&>(dataset));
				reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0, zIndex }, TC::IO::Count { static_cast<uint64_t>(x), static_cast<uint64_t>(y), 1 } });
				reader.SetSamplingStep(1);

				if (const auto result = reader.Read()) {
					if (auto data = d->ToBigScalarLdm(dataset, result, x * y))
						return data;

					return std::any_cast<std::shared_ptr<uint16_t[]>>(result->GetData());
				}
			} else {
				const auto dataset = group.openDataSet(index);
				const hsize_t mcount[] = { static_cast<uint64_t>(y), static_cast<uint64_t>(x) };
				const hsize_t fcount[] = { 1ULL, static_cast<uint64_t>(y), static_cast<uint64_t>(x) };
				const H5::DataSpace mspace { 2, mcount };
				const auto space = dataset.getSpace();
				const hsize_t start[] = { static_cast<uint64_t>(zIndex), 0ULL, 0ULL };
				space.selectHyperslab(H5S_SELECT_SET, fcount, start);

				if (auto data = d->ToBigScalar(dataset, x * y))
					return data;

				auto buffer = std::make_unique<uint16_t[]>(x * y);

				dataset.read(buffer.get(), H5::PredType::NATIVE_UINT16, mspace, space);
				return std::move(buffer);
			}
		} catch (const H5::Exception&) { } catch (const std::bad_any_cast&) { }

		return {};
	}

	auto TCFReader::LoadFL2D(int channel, int timestep) -> std::shared_ptr<uint16_t[]> {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto index = QString("%1").arg(timestep, 6, 10, QChar('0')).toUtf8();
			const auto parent = file.openGroup("Data/2DFLMIP");
			const auto name = QString::fromStdString(parent.getObjnameByIdx(channel));
			const auto path = QString("Data/2DFLMIP/%1").arg(name).toUtf8();
			const auto group = file.openGroup(path);
			const auto x = ReadInt64(parent.openAttribute(SizeX));
			const auto y = ReadInt64(parent.openAttribute(SizeY));

			if (d->IsLDM()) {
				const auto dataset = group.openGroup(index);
				TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
				reader.SetLdmDataGroup(const_cast<H5::Group&>(dataset));
				reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0 }, TC::IO::Count { static_cast<size_t>(x), static_cast<size_t>(y) } });
				reader.SetSamplingStep(1);

				if (const auto result = reader.Read()) {
					if (auto data = d->ToBigScalarLdm(dataset, result, x * y))
						return data;

					return std::any_cast<std::shared_ptr<uint16_t[]>>(result->GetData());
				}
			} else {
				const auto dataset = group.openDataSet(index);

				if (auto data = d->ToBigScalar(dataset, x * y))
					return data;

				auto buffer = std::make_unique<uint16_t[]>(x * y);
				dataset.read(buffer.get(), H5::PredType::NATIVE_UINT16);
				return std::move(buffer);
			}
		} catch (const H5::Exception&) { } catch (const std::bad_any_cast&) { }

		return {};
	}

	auto TCFReader::LoadFL3D(int channel, int timestep, int zMin, int zMax) -> std::shared_ptr<uint16_t[]> {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto index = QString("%1").arg(timestep, 6, 10, QChar('0')).toUtf8();
			const auto parent = file.openGroup("Data/3DFL");
			const auto name = QString::fromStdString(parent.getObjnameByIdx(channel));
			const auto path = QString("Data/3DFL/%1").arg(name).toUtf8();
			const auto group = file.openGroup(path);
			const auto x = static_cast<uint64_t>(ReadInt64(parent.openAttribute(SizeX)));
			const auto y = static_cast<uint64_t>(ReadInt64(parent.openAttribute(SizeY)));
			const auto z = static_cast<uint64_t>(ReadInt64(parent.openAttribute(SizeZ)));
			const auto zMid = static_cast<uint64_t>(zMax - zMin);

			if (d->IsLDM()) {
				const auto dataset = group.openGroup(index);
				TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
				reader.SetLdmDataGroup(const_cast<H5::Group&>(dataset));
				reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0, static_cast<int64_t>(zMin) }, TC::IO::Count { x, y, zMid } });
				reader.SetSamplingStep(1);

				if (const auto result = reader.Read()) {
					if (auto data = d->ToBigScalarLdm(dataset, result, x * y * zMid))
						return data;

					return std::any_cast<std::shared_ptr<uint16_t[]>>(result->GetData());
				}
			} else {
				const auto dataset = group.openDataSet(index);

				const uint64_t mcount[] = { zMid, y, x };
				const H5::DataSpace mspace { 3, mcount };

				const auto space = dataset.getSpace();
				const uint64_t start[] = { static_cast<uint64_t>(zMin), 0ULL, 0ULL };
				const uint64_t fcount[] = { zMid, y, x };
				space.selectHyperslab(H5S_SELECT_SET, fcount, start);

				if (auto data = d->ToBigScalar(dataset, x * y * zMid, &mspace, &space))
					return data;

				auto buffer = std::make_unique<uint16_t[]>(x * y * zMid);
				dataset.read(buffer.get(), H5::PredType::NATIVE_UINT16, mspace, space);

				return std::move(buffer);
			}
		} catch (const H5::Exception&) { } catch (const std::bad_any_cast&) { }

		return {};
	}

	auto TCFReader::LoadFL3D(int channel, int timestep, int zIndex) -> std::shared_ptr<uint16_t[]> {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto index = QString("%1").arg(timestep, 6, 10, QChar('0')).toUtf8();
			const auto parent = file.openGroup("Data/3DFL");
			const auto name = QString::fromStdString(parent.getObjnameByIdx(channel));
			const auto path = QString("Data/3DFL/%1").arg(name).toUtf8();
			const auto group = file.openGroup(path);
			const auto x = ReadInt64(parent.openAttribute(SizeX));
			const auto y = ReadInt64(parent.openAttribute(SizeY));

			if (d->IsLDM()) {
				const auto dataset = group.openGroup(index);
				TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
				reader.SetLdmDataGroup(const_cast<H5::Group&>(dataset));
				reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0, zIndex }, TC::IO::Count { static_cast<uint64_t>(x), static_cast<uint64_t>(y), 1 } });
				reader.SetSamplingStep(1);

				if (const auto result = reader.Read()) {
					if (auto data = d->ToBigScalarLdm(dataset, result, x * y))
						return data;

					return std::any_cast<std::shared_ptr<uint16_t[]>>(result->GetData());
				}
			} else {
				const auto dataset = group.openDataSet(index);
				const hsize_t mcount[] = { static_cast<uint64_t>(y), static_cast<uint64_t>(x) };
				const hsize_t fcount[] = { 1ULL, static_cast<uint64_t>(y), static_cast<uint64_t>(x) };
				const H5::DataSpace mspace { 2, mcount };
				const auto space = dataset.getSpace();
				const hsize_t start[] = { static_cast<uint64_t>(zIndex), 0ULL, 0ULL };
				space.selectHyperslab(H5S_SELECT_SET, fcount, start);

				if (auto data = d->ToBigScalar(dataset, x * y))
					return data;

				auto buffer = std::make_unique<uint16_t[]>(x * y);

				dataset.read(buffer.get(), H5::PredType::NATIVE_UINT16, mspace, space);
				return std::move(buffer);
			}
		} catch (const H5::Exception&) { } catch (const std::bad_any_cast&) { }

		return {};
	}

	auto TCFReader::LoadBF(int timestep) -> std::shared_ptr<uint8_t[]> {
		try {
			H5::H5File file;
			file.openFile(d->tcf, H5F_ACC_RDONLY);

			const auto index = QString("%1").arg(timestep, 6, 10, QChar('0')).toUtf8();
			const auto path = QString("Data/BF").toUtf8();
			const auto group = file.openGroup(path);
			const auto x = ReadInt64(group.openAttribute(SizeX));
			const auto y = ReadInt64(group.openAttribute(SizeY));

			if (d->IsLDM()) {
				const auto dataset = group.openGroup(index);
				TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader reader;
				reader.SetLdmDataGroup(const_cast<H5::Group&>(dataset));
				reader.SetReadingRoi(TC::IO::DataRange { TC::IO::Point { 0, 0 }, TC::IO::Count { static_cast<size_t>(x), static_cast<size_t>(y) } });
				reader.SetSamplingStep(1);

				if (const auto result = reader.Read())
					return std::any_cast<std::shared_ptr<uint8_t[]>>(result->GetData());
			} else {
				const auto dataset = group.openDataSet(index);
				auto buffer1 = std::make_unique<uint8_t[]>(x * y * 3);
				auto buffer2 = std::make_unique<uint8_t[]>(x * y * 3);
				dataset.read(buffer1.get(), H5::PredType::NATIVE_UINT8);

				for (auto j = 0; j < x; ++j) {
					for (auto k = 0; k < y; ++k) {
						for (auto z = 0; z < 3; ++z) {
							const auto ss = j + (k * x) + (z * x * y);
							const auto dd = k + (j * y) + (z * x * y);

							buffer2[dd] = buffer1[ss];
						}
					}
				}

				for (auto j = 0; j < x; ++j) {
					for (auto k = 0; k < y; ++k) {
						const auto srcR = k + j * y + 0 * (x * y);
						const auto srcG = k + j * y + 1 * (x * y);
						const auto srcB = k + j * y + 2 * (x * y);

						const auto destR = 3 * (j + k * x) + 0;
						const auto destG = 3 * (j + k * x) + 1;
						const auto destB = 3 * (j + k * x) + 2;

						buffer1[destR] = buffer2[srcR];
						buffer1[destG] = buffer2[srcG];
						buffer1[destB] = buffer2[srcB];
					}
				}

				return std::move(buffer1);
			}
		} catch (const H5::Exception&) { } catch (const std::bad_any_cast&) { }

		return {};
	}
}
