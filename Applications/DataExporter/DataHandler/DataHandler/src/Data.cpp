#include "Data.h"

namespace DataExporter::DataHandler {
	struct Data::Impl {
		QString tcf;
		QString save;
		QString format;
		QVariantMap option;

		int progress = 0;
		QString message;
		QString error;
	};

	Data::Data(const ITask* task) : QObject(), IData(), d(new Impl) {
		d->tcf = task->GetTCFPath();
		d->save = task->GetSavePath();
		d->format = task->GetSaveFormat();
		d->option = task->GetOptionMap();
	}

	Data::~Data() = default;

	auto Data::SetMessage(const QString& message) -> void {
		if (d->message != message) {
			d->message = message;
			emit Updated();
		}
	}

	auto Data::SetProgress(int value) -> void {
		if (d->progress != value) {
			d->progress = value;
			emit Updated();
		}
	}

	auto Data::SetProgress(int value, int max) -> void {
		SetProgress(static_cast<int>(static_cast<double>(value) / max * 100.0));
	}

	auto Data::ThrowError(const QString& error) -> void {
		if (d->error != error) {
			d->error = error;
			emit Updated();
		}
	}

	auto Data::GetTCFPath() const -> QString {
		return d->tcf;
	}

	auto Data::GetSavePath() const -> QString {
		return d->save;
	}

	auto Data::GetSaveFormat() const -> QString {
		return d->format;
	}

	auto Data::GetOptionMap() const -> QVariantMap {
		return d->option;
	}

	auto Data::GetProgress() const -> int {
		return d->progress;
	}

	auto Data::GetMessage() const -> QString {
		return d->message;
	}

	auto Data::GetError() const -> QString {
		return d->error;
	}
}
