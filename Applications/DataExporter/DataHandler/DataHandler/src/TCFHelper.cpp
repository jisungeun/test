#include "TCFHelper.h"

namespace DataExporter::DataHandler {
	auto ReadUInt64(const H5::Attribute& attr) -> uint64_t {
		uint64_t value;
		attr.read(H5::PredType::NATIVE_UINT64, &value);
		return value;
	}

	auto ReadInt64(const H5::Attribute& attr) -> int64_t {
		int64_t value;
		attr.read(H5::PredType::NATIVE_INT64, &value);
		return value;
	}

	auto ReadInt8(const H5::Attribute& attr) -> int8_t {
		int8_t value;
		attr.read(H5::PredType::NATIVE_INT8, &value);
		return value;
	}

	auto ReadDouble(const H5::Attribute& attr) -> double {
		double value;
		attr.read(H5::PredType::NATIVE_DOUBLE, &value);
		return value;
	}

	auto ReadString(const H5::Attribute& attr) -> std::string {
		std::string value;
		attr.read(attr.getDataType(), value);
		return value;
	}

	auto ReadValue(const H5::Attribute& attr) -> QVariant {
		if (attr.getDataType() == H5::PredType::NATIVE_UINT8)
			return ReadInt8(attr);

		if (attr.getDataType() == H5::PredType::NATIVE_INT64)
			return ReadInt64(attr);

		if (attr.getDataType() == H5::PredType::NATIVE_UINT64)
			return ReadUInt64(attr);

		if (attr.getDataType() == H5::PredType::NATIVE_DOUBLE)
			return ReadDouble(attr);

		return QString::fromStdString(ReadString(attr));
	}

	auto ReadAttribute(const H5::H5Object& obj) -> QVariantMap {
		try {
			QVariantMap map;

			for (auto i = 0; i < obj.getNumAttrs(); i++) {
				const auto attr = obj.openAttribute(i);
				const auto name = QString::fromStdString(attr.getName());

				map[name] = ReadValue(attr);
			}

			return map;
		} catch (const H5::Exception&) { }

		return {};
	}
}
