#pragma once

#include <QThread>

#include "IServiceProvider.h"

#include "IDataHandler.h"
#include "ITask.h"

#include "DataExporter.DataHandlerExport.h"

namespace DataExporter::DataHandler {
	class DataExporter_DataHandler_API DataHandler final : public IDataHandler, public QThread {
		Implements(IDataHandler)

	public:
		explicit DataHandler(IServiceProvider* provider);
		~DataHandler() override;

		auto Start(const QStringList& args) -> bool override;
		auto Stop() -> void override;

		auto GetCurrentItem() const -> IData* override;

	protected slots:
		auto OnScheduled(IService* sender, ITask* task) -> void;

	protected:
		auto run() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
