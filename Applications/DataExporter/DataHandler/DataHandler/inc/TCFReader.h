#pragma once

#include "ITCFData.h"

#include "DataExporter.DataHandlerExport.h"

namespace DataExporter::DataHandler {
	class DataExporter_DataHandler_API TCFReader final : public ITCFData {
	public:
		explicit TCFReader(const QString& path);
		~TCFReader() override;

		auto GetFilename() const -> QString override;
		auto GetCreationDateTime() const -> QDateTime override;
		auto GetDataID() const -> QString override;
		auto GetDescription() const -> QString override;
		auto GetDeviceHost() const -> QString override;
		auto GetDeviceModelType() const -> QString override;
		auto GetDeviceSerial() const -> QString override;
		auto GetDeviceSoftwareVersion() const -> QString override;
		auto GetFormatVersion() const -> QString override;
		auto GetRecordDateTime() const -> QDateTime override;
		auto GetSoftwareVersion() const -> QString override;
		auto GetTitle() const -> QString override;
		auto GetUniqueID() const -> QString override;
		auto GetUserID() const -> QString override;

		auto ContainsHT() const -> bool override;
		auto ContainsFL() const -> bool override;
		auto ContainsBF() const -> bool override;

		auto GetHTDataCount() const -> int override;
		auto GetHTResolution() -> Resolution3D override;
		auto GetHTSize() -> Size3D override;
		auto GetHTPosition(int timestep) -> Position override;
		auto GetHTRange(int timestep) -> DataRange override;

		auto GetFLChannelCount() const -> int override;
		auto GetFLSize() -> Size3D override;
		auto GetFLResolution() -> Resolution3D override;
		auto GetFLDataCount(int channel) const -> int override;
		auto GetFLPosition(int channel, int timestep) -> Position override;
		auto GetFLRange(int channel, int timestep) -> DataRange override;

		auto GetBFDataCount() const -> int override;
		auto GetBFResolution() const -> Resolution2D override;
		auto GetBFSize() const -> Size2D override;
		auto GetBFPosition(int timestep) -> Position override;

		auto LoadHT2D(int timestep) -> std::shared_ptr<uint16_t[]> override;
		auto LoadHT3D(int timestep, int zMin, int zMax) -> std::shared_ptr<uint16_t[]> override;
		auto LoadHT3D(int timestep, int zIndex) -> std::shared_ptr<uint16_t[]> override;
		auto LoadFL2D(int channel, int timestep) -> std::shared_ptr<uint16_t[]> override;
		auto LoadFL3D(int channel, int timestep, int zMin, int zMax) -> std::shared_ptr<uint16_t[]> override;
		auto LoadFL3D(int channel, int timestep, int zIndex) -> std::shared_ptr<uint16_t[]> override;
		auto LoadBF(int timestep) -> std::shared_ptr<uint8_t[]> override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}