#pragma once

#include <QObject>

#include "IData.h"
#include "ITask.h"

#include "DataExporter.DataHandlerExport.h"

namespace DataExporter::DataHandler {
	class DataExporter_DataHandler_API Data final : public QObject, public IData {
		Q_OBJECT

	public:
		explicit Data(const ITask* task);
		~Data() override;

		auto SetMessage(const QString& message) -> void;
		auto SetProgress(int value) -> void;
		auto SetProgress(int value, int max) -> void;

		auto ThrowError(const QString& error) -> void;

		auto GetTCFPath() const -> QString override;
		auto GetSavePath() const -> QString override;
		auto GetSaveFormat() const -> QString override;
		auto GetOptionMap() const -> QVariantMap override;

		auto GetProgress() const -> int override;
		auto GetMessage() const -> QString override;
		auto GetError() const -> QString override;

	signals:
		auto Updated() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
