#pragma once

#include <QVariant>

#include "H5Cpp.h"

#include "DataExporter.DataHandlerExport.h"

namespace DataExporter::DataHandler {
	auto DataExporter_DataHandler_API ReadUInt64(const H5::Attribute& attr) -> uint64_t;
	auto DataExporter_DataHandler_API ReadInt64(const H5::Attribute& attr) -> int64_t;
	auto DataExporter_DataHandler_API ReadInt8(const H5::Attribute& attr) -> int8_t;
	auto DataExporter_DataHandler_API ReadDouble(const H5::Attribute& attr) -> double;
	auto DataExporter_DataHandler_API ReadString(const H5::Attribute& attr) -> std::string;
	auto DataExporter_DataHandler_API ReadValue(const H5::Attribute& attr) -> QVariant;

	auto DataExporter_DataHandler_API ReadAttribute(const H5::H5Object& obj) -> QVariantMap;
}
