#include "TestFormat.h"

#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QThread>

namespace DataExporter::DataHandler::Format {
	struct TestFormat::Impl {
		ITCFData* data = nullptr;
	};

	TestFormat::TestFormat() : IFormat(), d(new Impl) { }

	TestFormat::~TestFormat() = default;

	auto TestFormat::SetTCFData(ITCFData* data) -> void {
		d->data = data;

		qInfo() << "Filename:" << d->data->GetFilename();
		qInfo() << "CreationDateTime: " << d->data->GetCreationDateTime();
		qInfo() << "DataID: " << d->data->GetDataID();
		qInfo() << "Description: " << d->data->GetDescription();
		qInfo() << "DeviceHost: " << d->data->GetDeviceHost();
		qInfo() << "DeviceModelType: " << d->data->GetDeviceModelType();
		qInfo() << "DeviceSerial: " << d->data->GetDeviceSerial();
		qInfo() << "DeviceSoftwareVersion() : " << d->data->GetDeviceSoftwareVersion();
		qInfo() << "FormatVersion: " << d->data->GetFormatVersion();
		qInfo() << "RecordDateTime: " << d->data->GetRecordDateTime();
		qInfo() << "SoftwareVersion: " << d->data->GetSoftwareVersion();
		qInfo() << "Title: " << d->data->GetTitle();
		qInfo() << "UniqueID: " << d->data->GetUniqueID();
		qInfo() << "UserID: " << d->data->GetUserID();
		qInfo() << "";

		{
			qInfo() << "HT Count: " << d->data->GetHTDataCount();
			const auto res = d->data->GetHTResolution();
			qInfo() << "HT Resolution: " << res.x << res.y << res.z;
			const auto size = d->data->GetHTSize();
			qInfo() << "HT Size: " << size.x << size.y << size.z;
			qInfo() << "";
		}

		{
			qInfo() << "FL Channel: " << d->data->GetFLChannelCount();
			const auto res = d->data->GetFLResolution();
			qInfo() << "FL Resolution: " << res.x << res.y << res.z;
			const auto size = d->data->GetFLSize();
			qInfo() << "FL Size: " << size.x << size.y << size.z;
		}

		{
			qInfo() << "BF Count: " << d->data->GetBFDataCount();
			const auto res = d->data->GetBFResolution();
			qInfo() << "BF Resolution: " << res.x << res.y;
			const auto size = d->data->GetBFSize();
			qInfo() << "BF Size: " << size.x << size.y;
		}
	}

	auto TestFormat::SetZRange(int min, int max) -> void {}

	auto TestFormat::ExportHT2D(const QString& path, int timestep) -> bool {
		const auto range = d->data->GetHTRange(timestep);
		qDebug() << "HT2D" << timestep << range.min << range.max;

		return true;
	}

	auto TestFormat::ExportHT3D(const QString& path, int timestep) -> bool {
		return true;
	}

	auto TestFormat::ExportFL2D(const QString& path, int channel, int timestep) -> bool {
		const auto range = d->data->GetFLRange(channel, timestep);
		qDebug() << "FL CH" << channel << timestep << range.min << range.max;

		return true;
	}

	auto TestFormat::ExportFL3D(const QString& path, int channel, int timestep) -> bool {
		return true;
	}

	auto TestFormat::ExportBF(const QString& path, int timestep) -> bool {
		return true;
	}
}
