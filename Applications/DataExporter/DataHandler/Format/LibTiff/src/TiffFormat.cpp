#include "TiffFormat.h"

#include "TiffWriter.h"

namespace DataExporter::DataHandler::Format {
	struct TiffFormat::Impl {
		ITCFData* data = nullptr;
		int min = -1;
		int max = -1;

		auto SetTiffMetadata(TiffWriter& writer) const -> void {
			writer.SetDescription(data->GetDescription());
			writer.SetArtist(data->GetUserID());
			writer.SetHost(data->GetDeviceHost());
			writer.SetModel(data->GetDeviceModelType());
			writer.SetSoftware(data->GetSoftwareVersion());
			writer.SetDateTime(data->GetCreationDateTime());
		}

		auto SetTiffMetadata(TiffWriter& writer, const Size3D& size, const Resolution3D& res, const DataRange& range) const -> void {
			const auto desc = QString("ImageJ=%1\nslices=%2\nunit=%3\nspacing=%4\nloop=%5\nmin=%6\nmax=%7\n")
					.arg("1.54f")
					.arg(size.z)
					.arg("cm")
					.arg(res.z / 10000.0, 0, 'f', 12)
					.arg("false")
					.arg(range.min, 0, 'f', 1)
					.arg(range.max, 0, 'f', 1);

			writer.SetDescription(desc);
			writer.SetArtist(data->GetUserID());
			writer.SetHost(data->GetDeviceHost());
			writer.SetModel(data->GetDeviceModelType());
			writer.SetSoftware(data->GetSoftwareVersion());
			writer.SetDateTime(data->GetCreationDateTime());
		}
	};

	TiffFormat::TiffFormat() : IFormat(), d(new Impl) { }

	TiffFormat::~TiffFormat() = default;

	auto TiffFormat::SetTCFData(ITCFData* data) -> void {
		d->data = data;
	}

	auto TiffFormat::SetZRange(int min, int max) -> void {
		d->min = min;
		d->max = max;
	}

	auto TiffFormat::ExportHT2D(const QString& path, int timestep) -> bool {
		const auto size = d->data->GetHTSize();
		const auto res = d->data->GetHTResolution();
		const auto pos = d->data->GetHTPosition(timestep);
		const auto range = d->data->GetHTRange(timestep);

		TiffWriter writer(path);
		d->SetTiffMetadata(writer);

		writer.SetDataType(sizeof(uint16_t), false);
		writer.SetSize(size.x, size.y);
		writer.SetResolution(res.x, res.y);
		writer.SetPosition(pos.x, pos.y);
		writer.SetIntensity(range.min, range.max);

		if (const auto data = d->data->LoadHT2D(timestep))
			return writer.Write(reinterpret_cast<uint8_t*>(data.get()));

		return false;
	}

	auto TiffFormat::ExportHT3D(const QString& path, int timestep) -> bool {
		const auto size = d->data->GetHTSize();
		const auto res = d->data->GetHTResolution();
		const auto pos = d->data->GetHTPosition(timestep);
		const auto range = d->data->GetHTRange(timestep);
		const auto zMin = d->min > -1 && d->min < size.z ? d->min : 0;
		const auto zMax = d->max > -1 && d->max < size.z ? d->max : size.z;
		const auto zRange = zMax - zMin;

		TiffWriter writer(path);
		d->SetTiffMetadata(writer, size, res, range);

		writer.SetDataType(sizeof(uint16_t), false);
		writer.SetSize(size.x, size.y, zRange);
		writer.SetResolution(res.x, res.y);
		writer.SetPosition(pos.x, pos.y);
		writer.SetIntensity(range.min, range.max);

		if (const auto data = d->data->LoadHT3D(timestep, zMin, zMax))
			return writer.Write(reinterpret_cast<uint8_t*>(data.get()));

		return false;
	}

	auto TiffFormat::ExportFL2D(const QString& path, int channel, int timestep) -> bool {
		const auto size = d->data->GetFLSize();
		const auto res = d->data->GetFLResolution();
		const auto pos = d->data->GetFLPosition(channel, timestep);
		const auto range = d->data->GetFLRange(channel, timestep);

		TiffWriter writer(path);
		d->SetTiffMetadata(writer);

		writer.SetDataType(sizeof(uint16_t), false);
		writer.SetSize(size.x, size.y);
		writer.SetResolution(res.x, res.y);
		writer.SetPosition(pos.x, pos.y);
		writer.SetIntensity(static_cast<uint16_t>(range.min), static_cast<uint16_t>(range.max));

		if (const auto data = d->data->LoadFL2D(channel, timestep))
			return writer.Write(reinterpret_cast<uint8_t*>(data.get()));

		return false;
	}

	auto TiffFormat::ExportFL3D(const QString& path, int channel, int timestep) -> bool {
		const auto size = d->data->GetFLSize();
		const auto res = d->data->GetFLResolution();
		const auto pos = d->data->GetFLPosition(channel, timestep);
		const auto range = d->data->GetFLRange(channel, timestep);
		const auto zMin = d->min > -1 && d->min < size.z ? d->min : 0;
		const auto zMax = d->max > -1 && d->max < size.z ? d->max : size.z;
		const auto zRange = zMax > zMin ? zMax - zMin : 0;

		TiffWriter writer(path);
		d->SetTiffMetadata(writer, size, res, { 0, range.max });

		writer.SetDataType(sizeof(uint16_t), false);
		writer.SetSize(size.x, size.y, zRange);
		writer.SetResolution(res.x, res.y);
		writer.SetPosition(pos.x, pos.y);
		writer.SetIntensity(static_cast<uint16_t>(range.min), static_cast<uint16_t>(range.max));

		if (const auto data = d->data->LoadFL3D(channel, timestep, zMin, zMax))
			return writer.Write(reinterpret_cast<uint8_t*>(data.get()));

		return false;
	}

	auto TiffFormat::ExportBF(const QString& path, int timestep) -> bool {
		const auto size = d->data->GetFLSize();
		const auto res = d->data->GetFLResolution();
		const auto pos = d->data->GetBFPosition(timestep);

		TiffWriter writer(path);
		d->SetTiffMetadata(writer);

		writer.SetDataType(sizeof(uint8_t), true);
		writer.SetSize(size.x, size.y);
		writer.SetResolution(res.x, res.y);
		writer.SetPosition(pos.x, pos.y);

		if (const auto data = d->data->LoadBF(timestep))
			return writer.Write(data.get());

		return false;
	}
}
