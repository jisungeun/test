#include "TiffWriter.h"

#include <tiffio.h>

namespace DataExporter::DataHandler::Format {
	struct TiffWriter::Impl {
		std::wstring filepath;
		std::string desc;
		std::string artist;
		std::string host;
		std::string model;
		std::string sw;
		std::string datetime;

		int x = -1, y = -1, z = -1;
		double rx = 0.0, ry = 0.0;
		double px = 0.0, py = 0.0;

		int size = 0;
		bool big = false;
		bool rgb = false;
		bool floating = false;
		uint16_t min = 0, max = 0;
		double_t dmin = 0.0, dmax = 0.0;

		TIFF* tiff = nullptr;
	};

	TiffWriter::TiffWriter(const QString& filepath) : d(new Impl) {
		d->filepath = filepath.toStdWString();
	}

	TiffWriter::~TiffWriter() {
		Dispose();
	}

	auto TiffWriter::SetRGB(bool rgb) -> void {
		d->rgb = rgb;
	}

	auto TiffWriter::SetDataType(int size, bool floating) -> void {
		d->size = size;
		d->floating = floating;
	}

	auto TiffWriter::SetSize(int x, int y) -> void {
		d->x = x;
		d->y = y;
	}

	auto TiffWriter::SetSize(int x, int y, int z) -> void {
		d->x = x;
		d->y = y;
		d->z = z;
	}

	auto TiffWriter::SetResolution(double x, double y) -> void {
		d->rx = x;
		d->ry = y;
	}

	auto TiffWriter::SetPosition(double x, double y) -> void {
		d->px = x;
		d->py = y;
	}

	auto TiffWriter::SetIntensity(uint16_t min, uint16_t max) -> void {
		d->min = min;
		d->max = max;
	}

	auto TiffWriter::SetIntensity(double_t min, double_t max) -> void {
		d->dmin = min;
		d->dmax = max;
	}

	auto TiffWriter::SetBigTIFF(bool big) -> void {
		d->big = big;
	}

	auto TiffWriter::SetDescription(const QString& desc) -> void {
		d->desc = desc.toStdString();
	}

	auto TiffWriter::SetArtist(const QString& artist) -> void {
		d->artist = artist.toStdString();
	}

	auto TiffWriter::SetHost(const QString& host) -> void {
		d->host = host.toStdString();
	}

	auto TiffWriter::SetModel(const QString& model) -> void {
		d->model = model.toStdString();
	}

	auto TiffWriter::SetSoftware(const QString& sw) -> void {
		d->sw = sw.toStdString();
	}

	auto TiffWriter::SetDateTime(const QDateTime& datetime) -> void {
		d->datetime = datetime.toString("yyyy-MM-dd HH:mm:ss").toStdString();
	}

	auto TiffWriter::Write(uint8_t* data) -> bool {
		d->tiff = TIFFOpenW(d->filepath.c_str(), d->big ? "w8" : "w");

		if (!d->tiff)
			return false;

		auto page = 0;

		do {
			if (d->z > 0)
				TIFFSetDirectory(d->tiff, page);

			TIFFSetField(d->tiff, TIFFTAG_SUBFILETYPE, 0x000000);
			TIFFSetField(d->tiff, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
			TIFFSetField(d->tiff, TIFFTAG_RESOLUTIONUNIT, RESUNIT_CENTIMETER);
			TIFFSetField(d->tiff, TIFFTAG_ROWSPERSTRIP, d->x);
			TIFFSetField(d->tiff, TIFFTAG_IMAGEWIDTH, d->x);
			TIFFSetField(d->tiff, TIFFTAG_IMAGELENGTH, d->y);
			TIFFSetField(d->tiff, TIFFTAG_BITSPERSAMPLE, d->size * 8);
			TIFFSetField(d->tiff, TIFFTAG_PHOTOMETRIC, d->rgb ? PHOTOMETRIC_RGB : PHOTOMETRIC_MINISBLACK);
			TIFFSetField(d->tiff, TIFFTAG_SAMPLESPERPIXEL, d->rgb ? 3 : 1);

			if (d->floating)
				TIFFSetField(d->tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP);
			if (d->rx > 0.0)
				TIFFSetField(d->tiff, TIFFTAG_XRESOLUTION, 10000 / d->rx);
			if (d->ry > 0.0)
				TIFFSetField(d->tiff, TIFFTAG_YRESOLUTION, 10000 / d->ry);
			if (d->px > 0.0)
				TIFFSetField(d->tiff, TIFFTAG_XPOSITION, d->px);
			if (d->py > 0.0)
				TIFFSetField(d->tiff, TIFFTAG_YPOSITION, d->py);
			if (!d->desc.empty())
				TIFFSetField(d->tiff, TIFFTAG_IMAGEDESCRIPTION, d->desc.c_str());
			if (!d->artist.empty())
				TIFFSetField(d->tiff, TIFFTAG_ARTIST, d->artist.c_str());
			if (!d->host.empty())
				TIFFSetField(d->tiff, TIFFTAG_HOSTCOMPUTER, d->host.c_str());
			if (!d->model.empty())
				TIFFSetField(d->tiff, TIFFTAG_MODEL, d->model.c_str());
			if (!d->sw.empty())
				TIFFSetField(d->tiff, TIFFTAG_SOFTWARE, d->sw.c_str());
			if (!d->datetime.empty())
				TIFFSetField(d->tiff, TIFFTAG_DATETIME, d->datetime.c_str());

			for (auto i = 0; i < d->y; i++) {
				if (TIFFWriteScanline(d->tiff, data + static_cast<uint64_t>((i * d->x * d->size) + (page * d->y * d->x * d->size)), i) < 0)
					return false;
			}

			if (d->z > 0)
				TIFFWriteDirectory(d->tiff);
		} while (++page < d->z);

		return true;
	}

	auto TiffWriter::Dispose() -> void {
		if (d->tiff) {
			TIFFClose(d->tiff);
			d->tiff = nullptr;
		}
	}
}
