#pragma once

#include <QDateTime>

#include "DataExporter.DataHandler.Format.TiffExport.h"

namespace DataExporter::DataHandler::Format {
	class DataExporter_DataHandler_Format_Tiff_API TiffWriter {
	public:
		explicit TiffWriter(const QString& filepath);
		~TiffWriter();

		auto SetRGB(bool rgb) -> void;
		auto SetDataType(int size, bool floating) -> void;
		auto SetSize(int x, int y) -> void;
		auto SetSize(int x, int y, int z) -> void;
		auto SetResolution(double x, double y) -> void;
		auto SetPosition(double x, double y) -> void;
		auto SetIntensity(uint16_t min, uint16_t max) -> void;
		auto SetIntensity(double_t min, double_t max) -> void;

		auto SetBigTIFF(bool big) -> void;
		auto SetDescription(const QString& desc) -> void;
		auto SetArtist(const QString& artist) -> void;
		auto SetHost(const QString& host) -> void;
		auto SetModel(const QString& model) -> void;
		auto SetSoftware(const QString& sw) -> void;
		auto SetDateTime(const QDateTime& datetime) -> void;

		auto Write(uint8_t* data) -> bool;
		auto Dispose() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}