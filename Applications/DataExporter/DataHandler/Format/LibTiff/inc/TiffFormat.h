#pragma once

#include "IFormat.h"

#include "DataExporter.DataHandler.Format.TiffExport.h"

namespace DataExporter::DataHandler::Format {
	class DataExporter_DataHandler_Format_Tiff_API TiffFormat final : public IFormat {
	public:
		TiffFormat();
		~TiffFormat() override;

		auto SetTCFData(ITCFData* data) -> void override;
		auto SetZRange(int min, int max) -> void override;

		auto ExportHT2D(const QString& path, int timestep) -> bool override;
		auto ExportHT3D(const QString& path, int timestep) -> bool override;
		auto ExportFL2D(const QString& path, int channel, int timestep) -> bool override;
		auto ExportFL3D(const QString& path, int channel, int timestep) -> bool override;
		auto ExportBF(const QString& path, int timestep) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
