#include "NpyFormat.h"

#include "PythonNumpyIO.h"

namespace DataExporter::DataHandler::Format {
	struct NpyFormat::Impl {
		ITCFData* data = nullptr;

		int min = -1;
		int max = -1;
	};

	NpyFormat::NpyFormat() : IFormat(), d(new Impl) { }

	NpyFormat::~NpyFormat() = default;

	auto NpyFormat::SetTCFData(ITCFData* data) -> void {
		d->data = data;
	}

	auto NpyFormat::SetZRange(int min, int max) -> void {
		d->min = min;
		d->max = max;
	}

	auto NpyFormat::ExportHT2D(const QString& path, int timestep) -> bool {
		const auto size = d->data->GetHTSize();
		const auto data = d->data->LoadHT2D(timestep);
		const auto dataVector = std::vector(data.get(), data.get() + static_cast<uint64_t>(size.x * size.y));
		const auto sizeVector = std::vector<unsigned long> { static_cast<uint32_t>(size.x), static_cast<uint32_t>(size.y) };

		TC::IO::TCNumpyWriter writer;

		return writer.Write(dataVector, path, TC::IO::NpyArrType::arrUSHORT, sizeVector);
	}

	auto NpyFormat::ExportHT3D(const QString& path, int timestep) -> bool {
		const auto size = d->data->GetHTSize();
		const auto zMin = d->min > -1 && d->min < size.z ? d->min : 0;
		const auto zMax = d->max > -1 && d->max < size.z ? d->max : size.z;
		const auto zRange = zMax > zMin ? zMax - zMin : 0;
		const auto data = d->data->LoadHT3D(timestep, zMin, zMax);
		const auto dataVector = std::vector(data.get(), data.get() + static_cast<uint64_t>(size.x * size.y * zRange));
		const auto sizeVector = std::vector<unsigned long> { static_cast<uint32_t>(size.x), static_cast<uint32_t>(size.y), static_cast<uint32_t>(size.z) };

		TC::IO::TCNumpyWriter writer;
		return writer.Write(dataVector, path, TC::IO::NpyArrType::arrUSHORT, sizeVector);
	}

	auto NpyFormat::ExportFL2D(const QString& path, int channel, int timestep) -> bool {
		const auto size = d->data->GetFLSize();
		const auto data = d->data->LoadFL2D(channel, timestep);
		const auto dataVector = std::vector(data.get(), data.get() + static_cast<uint64_t>(size.x * size.y));
		const auto sizeVector = std::vector<unsigned long> { static_cast<uint32_t>(size.x), static_cast<uint32_t>(size.y) };

		TC::IO::TCNumpyWriter writer;
		return writer.Write(dataVector, path, TC::IO::NpyArrType::arrUSHORT, sizeVector);
	}

	auto NpyFormat::ExportFL3D(const QString& path, int channel, int timestep) -> bool {
		const auto size = d->data->GetFLSize();
		const auto zMin = d->min > -1 && d->min < size.z ? d->min : 0;
		const auto zMax = d->max > -1 && d->max < size.z ? d->max : size.z;
		const auto zRange = zMax > zMin ? zMax - zMin : 0;
		const auto data = d->data->LoadFL3D(channel, timestep, zMin, zMax);
		const auto dataVector = std::vector(data.get(), data.get() + static_cast<uint64_t>(size.x * size.y * zRange));
		const auto sizeVector = std::vector<unsigned long> { static_cast<uint32_t>(size.x), static_cast<uint32_t>(size.y), static_cast<uint32_t>(size.z) };

		if (TC::IO::TCNumpyWriter writer; writer.Write(dataVector, path, TC::IO::NpyArrType::arrUSHORT, sizeVector))
			return true;

		return false;
	}

	auto NpyFormat::ExportBF(const QString& path, int timestep) -> bool {
		const auto size = d->data->GetBFSize();
		const auto data = d->data->LoadBF(timestep);
		const auto dataVector = std::vector(data.get(), data.get() + static_cast<uint64_t>(size.x * size.y * 3));
		const auto sizeVector = std::vector<unsigned long> { static_cast<uint32_t>(size.x), static_cast<uint32_t>(size.y) };

		if (TC::IO::TCNumpyWriter writer; writer.Write(dataVector, path, TC::IO::NpyArrType::arrUBYTE, sizeVector))
			return true;

		return false;
	}
}
