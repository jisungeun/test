#include "RawFormat.h"

#include "TCRawWriter.h"

namespace DataExporter::DataHandler::Format {
	using namespace TC::IO::RawWriter;

	struct RawFormat::Impl {
		ITCFData* data = nullptr;

		int min = -1;
		int max = -1;
	};

	RawFormat::RawFormat() : IFormat(), d(new Impl) { }

	RawFormat::~RawFormat() = default;

	auto RawFormat::SetTCFData(ITCFData* data) -> void {
		d->data = data;
	}

	auto RawFormat::SetZRange(int min, int max) -> void {
		d->min = min;
		d->max = max;
	}

	auto RawFormat::ExportHT2D(const QString& path, int timestep) -> bool {
		const auto size = d->data->GetHTSize();
		const auto res = d->data->GetHTResolution();
		const auto sizeVector = std::vector { static_cast<uint64_t>(size.x), static_cast<uint64_t>(size.y), 1ULL };
		const auto resVector = std::vector { static_cast<float_t>(res.x), static_cast<float_t>(res.y), 0.0f };
		const auto data = d->data->LoadHT2D(timestep);

		Writer writer(path, sizeVector, resVector, Writer::Type::UINT16);

		return writer.Append(reinterpret_cast<const char*>(data.get()), size.x * size.y * sizeof(uint16_t), false);
	}

	auto RawFormat::ExportHT3D(const QString& path, int timestep) -> bool {
		const auto size = d->data->GetHTSize();
		const auto res = d->data->GetHTResolution();
		const auto zMin = d->min > -1 && d->min < size.z ? d->min : 0;
		const auto zMax = d->max > -1 && d->max < size.z ? d->max : size.z;
		const auto zRange = zMax > zMin ? zMax - zMin : 0;
		const auto sizeVector = std::vector { static_cast<uint64_t>(size.x), static_cast<uint64_t>(size.y), static_cast<uint64_t>(zRange) };
		const auto resVector = std::vector { static_cast<float_t>(res.x), static_cast<float_t>(res.y), static_cast<float_t>(res.z) };
		const auto data = d->data->LoadHT3D(timestep, zMin, zMax);

		Writer writer(path, sizeVector, resVector, Writer::Type::UINT16);

		return writer.Append(reinterpret_cast<const char*>(data.get()), size.x * size.y * zRange * sizeof(uint16_t), false);
	}

	auto RawFormat::ExportFL2D(const QString& path, int channel, int timestep) -> bool {
		const auto size = d->data->GetFLSize();
		const auto res = d->data->GetFLResolution();
		const auto sizeVector = std::vector { static_cast<uint64_t>(size.x), static_cast<uint64_t>(size.y), 1ULL };
		const auto resVector = std::vector { static_cast<float_t>(res.x), static_cast<float_t>(res.y), 0.0f };
		const auto data = d->data->LoadFL2D(channel, timestep);

		Writer writer(path, sizeVector, resVector, Writer::Type::UINT16);

		return writer.Append(reinterpret_cast<const char*>(data.get()), size.x * size.y * sizeof(uint16_t), false);
	}

	auto RawFormat::ExportFL3D(const QString& path, int channel, int timestep) -> bool {
		const auto size = d->data->GetFLSize();
		const auto res = d->data->GetFLResolution();
		const auto zMin = d->min > -1 && d->min < size.z ? d->min : 0;
		const auto zMax = d->max > -1 && d->max < size.z ? d->max : size.z;
		const auto zRange = zMax > zMin ? zMax - zMin : 0;
		const auto sizeVector = std::vector { static_cast<uint64_t>(size.x), static_cast<uint64_t>(size.y), static_cast<uint64_t>(zRange) };
		const auto resVector = std::vector { static_cast<float_t>(res.x), static_cast<float_t>(res.y), static_cast<float_t>(res.z) };
		const auto data = d->data->LoadFL3D(channel, timestep, zMin, zMax);

		Writer writer(path, sizeVector, resVector, Writer::Type::UINT16);

		return writer.Append(reinterpret_cast<const char*>(data.get()), size.x * size.y * zRange * sizeof(uint16_t), false);
	}

	auto RawFormat::ExportBF(const QString& path, int timestep) -> bool {
		const auto size = d->data->GetBFSize();
		const auto res = d->data->GetBFResolution();
		const auto sizeVector = std::vector { static_cast<uint64_t>(size.x), static_cast<uint64_t>(size.y), 1ULL };
		const auto resVector = std::vector { static_cast<float_t>(res.x), static_cast<float_t>(res.y), 0.0f };
		const auto data = d->data->LoadBF(timestep);

		Writer writer(path, sizeVector, resVector, Writer::Type::UINT8);

		return writer.Append(reinterpret_cast<const char*>(data.get()), size.x * size.y * 3 * sizeof(uint8_t), false);
	}
}
