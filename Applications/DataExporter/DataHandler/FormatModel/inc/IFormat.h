#pragma once

#include "ITCFData.h"

#include "DataExporter.DataHandler.FormatModelExport.h"

namespace DataExporter::DataHandler {
	class DataExporter_DataHandler_FormatModel_API IFormat : public IService {
	public:
		virtual auto SetTCFData(ITCFData* data) -> void = 0;
		virtual auto SetZRange(int min, int max) -> void = 0;

		virtual auto ExportHT2D(const QString& path, int timestep) -> bool = 0;
		virtual auto ExportHT3D(const QString& path, int timestep) -> bool = 0;
		virtual auto ExportFL2D(const QString& path, int channel, int timestep) -> bool = 0;
		virtual auto ExportFL3D(const QString& path, int channel, int timestep) -> bool = 0;
		virtual auto ExportBF(const QString& path, int timestep) -> bool = 0;
	};
}
