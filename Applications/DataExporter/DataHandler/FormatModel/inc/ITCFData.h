#pragma once

#include <QVariantMap>

#include "IService.h"

#include "DataExporter.DataHandler.FormatModelExport.h"

namespace DataExporter::DataHandler {
	struct DataExporter_DataHandler_FormatModel_API Size2D {
		Size2D() = default;

		Size2D(int _x, int _y) : x(_x), y(_y) {}

		bool rgb = false;
		int x = 1;
		int y = 1;

		auto ToSize2D() const -> int64_t {
			return rgb ? x * y * 3 : x * y;
		}
	};

	struct DataExporter_DataHandler_FormatModel_API Size3D : Size2D {
		Size3D() = default;

		Size3D(int _x, int _y, int _z) : Size2D(_x, _y), z(_z) {}

		int z = 1;

		auto ToSize3D() const -> int64_t {
			return rgb ? x * y * z * 3 : x * y * z;
		}
	};

	struct DataExporter_DataHandler_FormatModel_API Resolution2D {
		Resolution2D() = default;

		Resolution2D(double _x, double _y) : x(_x), y(_y) {}

		double x = 0.0;
		double y = 0.0;
	};

	struct DataExporter_DataHandler_FormatModel_API Resolution3D : Resolution2D {
		Resolution3D() = default;

		Resolution3D(double _x, double _y, double _z) : Resolution2D(_x, _y), z(_z) {}

		double z = 0.0;
	};

	struct DataExporter_DataHandler_FormatModel_API Position : Resolution3D {
		Position() = default;

		Position(double _c, double _x, double _y, double _z) : Resolution3D(_x, _y, _z), c(_c) {}

		double c = 0.0;
	};

	struct DataExporter_DataHandler_FormatModel_API DataRange {
		double min = 0.0;
		double max = 0.0;
	};

	struct DataExporter_DataHandler_FormatModel_API Color {
		int red = 0;
		int green = 0;
		int blue = 0;
	};

	class DataExporter_DataHandler_FormatModel_API ITCFData : public IService {
	public:
		virtual auto GetFilename() const -> QString = 0;

		virtual auto GetCreationDateTime() const -> QDateTime = 0;
		virtual auto GetDataID() const -> QString = 0;
		virtual auto GetDescription() const -> QString = 0;
		virtual auto GetDeviceHost() const -> QString = 0;
		virtual auto GetDeviceModelType() const -> QString = 0;
		virtual auto GetDeviceSerial() const -> QString = 0;
		virtual auto GetDeviceSoftwareVersion() const -> QString = 0;
		virtual auto GetFormatVersion() const -> QString = 0;
		virtual auto GetRecordDateTime() const -> QDateTime = 0;
		virtual auto GetSoftwareVersion() const -> QString = 0;
		virtual auto GetTitle() const -> QString = 0;
		virtual auto GetUniqueID() const -> QString = 0;
		virtual auto GetUserID() const -> QString = 0;

		virtual auto ContainsHT() const -> bool = 0;
		virtual auto ContainsFL() const -> bool = 0;
		virtual auto ContainsBF() const -> bool = 0;

		virtual auto GetHTDataCount() const -> int = 0;
		virtual auto GetHTResolution() -> Resolution3D = 0;
		virtual auto GetHTSize() -> Size3D = 0;
		virtual auto GetHTPosition(int timestep) -> Position = 0;
		virtual auto GetHTRange(int timestep) -> DataRange = 0;

		virtual auto GetFLChannelCount() const -> int = 0;
		virtual auto GetFLResolution() -> Resolution3D = 0;
		virtual auto GetFLSize() -> Size3D = 0;
		virtual auto GetFLDataCount(int channel) const -> int = 0;
		virtual auto GetFLPosition(int channel, int timestep) -> Position = 0;
		virtual auto GetFLRange(int channel, int timestep) -> DataRange = 0;

		virtual auto GetBFDataCount() const -> int = 0;
		virtual auto GetBFResolution() const -> Resolution2D = 0;
		virtual auto GetBFSize() const -> Size2D = 0;
		virtual auto GetBFPosition(int timestep) -> Position = 0;

		virtual auto LoadHT2D(int timestep) -> std::shared_ptr<uint16_t[]> = 0;
		virtual auto LoadHT3D(int timestep, int zMin = -1, int zMax = -1) -> std::shared_ptr<uint16_t[]> = 0;
		virtual auto LoadHT3D(int timestep, int zIndex) -> std::shared_ptr<uint16_t[]> = 0;
		virtual auto LoadFL2D(int channel, int timestep) -> std::shared_ptr<uint16_t[]> = 0;
		virtual auto LoadFL3D(int channel, int timestep, int zMin = -1, int zMax = -1) -> std::shared_ptr<uint16_t[]> = 0;
		virtual auto LoadFL3D(int channel, int timestep, int zIndex) -> std::shared_ptr<uint16_t[]> = 0;
		virtual auto LoadBF(int timestep) -> std::shared_ptr<uint8_t[]> = 0;
	};
}
