#include "QtAppHostBuilder.h"

#include "AppMainWindow.h"
#include "DataHandler.h"
#include "NamedPipeGateway.h"
#include "PromptGateway.h"
#include "Scheduler.h"

using namespace DataExporter;

auto main(int argc, char** argv) -> int {
	const auto builder = std::make_shared<Component::QtAppHostBuilder>();
	const auto host = builder
			//->ConfigureAppConfig([] (IConfigBuilder* builder) {
			//	builder
			//			->AddConfiguration(std::make_shared<Component::Configuration>());
			//})
			->ConfigureServices([] (Component::ServiceCollection* collection) {
				collection
						->AddHostedService<Gateway::NamedPipeGateway>()
						->AddHostedService<Gateway::PromptGateway>()
						->AddHostedService<Scheduler::Scheduler>()
						->AddHostedService<DataHandler::DataHandler>()
						->AddHostedService<UI::AppMainWindow>();
			})
			->Build();

	return host->Start(argc, argv);
}
