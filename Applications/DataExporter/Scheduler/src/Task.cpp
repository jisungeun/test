#include "Task.h"

namespace DataExporter::Scheduler {
	struct Task::Impl {
		QString tcf;
		QString save;
		QString format;
		QVariantMap options;

		int min = -1;
		int max = -1;

		bool ht2d = false;
		bool ht3d = false;
		bool fl2d = false;
		bool fl3d = false;
		bool bf = false;

		QList<int> ht2dList;
		QList<int> ht3dList;
		QList<int> fl2dList;
		QList<int> fl3dList;
		QList<int> bfList;
	};

	Task::Task() : ITask(), d(new Impl) { }

	Task::~Task() = default;

	auto Task::SetTCFPath(const QString& path) -> void {
		d->tcf = path;
	}

	auto Task::SetSavePath(const QString& path) -> void {
		d->save = path;
	}

	auto Task::SetSaveFormat(const QString& format) -> void {
		d->format = format;
	}

	auto Task::SetOptionMap(const QVariantMap& options) -> void {
		d->options = options;
	}

	auto Task::SetZRange(int min, int max) -> void {
		d->min = min;
		d->max = max;
	}

	auto Task::SetHT2DExclusive(bool exclusive) -> void {
		d->ht2d = exclusive;
	}

	auto Task::SetHT3DExclusive(bool exclusive) -> void {
		d->ht3d = exclusive;
	}

	auto Task::SetFL2DExclusive(bool exclusive) -> void {
		d->fl2d = exclusive;
	}

	auto Task::SetFL3DExclusive(bool exclusive) -> void {
		d->fl3d = exclusive;
	}

	auto Task::SetBFExclusive(bool exclusive) -> void {
		d->bf = exclusive;
	}

	auto Task::SetHT2DTimeStepList(const QList<int>& list) -> void {
		d->ht2dList = list;
		std::sort(d->ht2dList.begin(), d->ht2dList.end());
	}

	auto Task::SetHT3DTimeStepList(const QList<int>& list) -> void {
		d->ht3dList = list;
		std::sort(d->ht3dList.begin(), d->ht3dList.end());
	}

	auto Task::SetFL2DTimeStepList(const QList<int>& list) -> void {
		d->fl2dList = list;
		std::sort(d->fl2dList.begin(), d->fl2dList.end());
	}

	auto Task::SetFL3DTimeStepList(const QList<int>& list) -> void {
		d->fl3dList = list;
		std::sort(d->fl3dList.begin(), d->fl3dList.end());
	}

	auto Task::SetBFTimeStepList(const QList<int>& list) -> void {
		d->bfList = list;
		std::sort(d->bfList.begin(), d->bfList.end());
	}

	auto Task::GetTCFPath() const -> QString {
		return d->tcf;
	}

	auto Task::GetSavePath() const -> QString {
		return d->save;
	}

	auto Task::GetSaveFormat() const -> QString {
		return d->format;
	}

	auto Task::GetOptionMap() const -> QVariantMap {
		return d->options;
	}

	auto Task::GetZRangeMin() const -> int {
		return d->min;
	}

	auto Task::GetZRangeMax() const -> int {
		return d->max;
	}

	auto Task::IsExclusiveHT2D() const -> bool {
		return d->ht2d;
	}

	auto Task::IsExclusiveHT3D() const -> bool {
		return d->ht3d;
	}

	auto Task::IsExclusiveFL2D() const -> bool {
		return d->fl2d;
	}

	auto Task::IsExclusiveFL3D() const -> bool {
		return d->fl3d;
	}

	auto Task::IsExclusiveBF() const -> bool {
		return d->bf;
	}

	auto Task::ContainsZRange() const -> bool {
		return d->min > -1 || d->max > -1;
	}

	auto Task::ContainsExclusiveHT2D(int timestep) const -> bool {
		return d->ht2dList.contains(timestep);
	}

	auto Task::ContainsExclusiveHT3D(int timestep) const -> bool {
		return d->ht3dList.contains(timestep);
	}

	auto Task::ContainsExclusiveFL2D(int timestep) const -> bool {
		return d->fl2dList.contains(timestep);
	}

	auto Task::ContainsExclusiveFL3D(int timestep) const -> bool {
		return d->fl3dList.contains(timestep);
	}

	auto Task::ContainsExclusiveBF(int timestep) const -> bool {
		return d->bfList.contains(timestep);
	}
}
