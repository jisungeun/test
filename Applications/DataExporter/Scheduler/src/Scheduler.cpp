#include "Scheduler.h"

#include <QDir>
#include <QFileInfo>
#include <QMutexLocker>
#include <QQueue>

#include "IGateway.h"

#include "Task.h"

namespace DataExporter::Scheduler {
	struct Scheduler::Impl {
		IServiceProvider* provider = nullptr;
		EventContainer container;

		Service<ITask> current = nullptr;
		QQueue<Service<ITask>> queue;

		QMutex mutex;
	};

	Scheduler::Scheduler(IServiceProvider* provider) : IScheduler(), d(new Impl) {
		d->provider = provider;
	}

	Scheduler::~Scheduler() = default;

	auto Scheduler::Start(const QStringList& args) -> bool {
		for (const auto& service : d->provider->GetServiceList<IGateway>())
			service->OnRequested += { d->container, this, &Scheduler::OnRequested };

		return true;
	}

	auto Scheduler::Stop() -> void { }

	auto Scheduler::IsEmpty() const -> bool {
		return d->queue.isEmpty();
	}

	auto Scheduler::HasNext() const -> bool {
		return !IsEmpty();
	}

	auto Scheduler::GetTaskCount() const -> int {
		return d->queue.count();
	}

	auto Scheduler::GetCurrentTask() const -> ITask* {
		return d->current.get();
	}

	auto Scheduler::NextTask() -> ITask* {
		QMutexLocker locker(&d->mutex);
		d->current = nullptr;

		if (!d->queue.isEmpty()) {
			if (const auto task = d->queue.dequeue()) {
				d->current = task;

				Run([this, task] {
					OnNextTask(this, task.get());
				}, false);

				return task.get();
			}
		}

		return nullptr;
	}

	auto Scheduler::OnRequested([[maybe_unused]] IService* sender, const IRequest* request) -> void {
		auto content = request->GetContent();
		const auto src = content.take("Source").toString();
		const auto dest = content.take("Destination").toString();
		const auto format = content.take("Format").toString();

		if (const auto info = QFileInfo(src); info.isFile() && info.exists()) {
			QMutexLocker locker(&d->mutex);

			const auto task = std::make_shared<Task>();
			task->SetTCFPath(QDir::fromNativeSeparators(src));
			task->SetSavePath(QDir::fromNativeSeparators(dest));
			task->SetSaveFormat(format);
			task->SetOptionMap(content);

			if (constexpr auto ZRange = "ZRange"; content.contains(ZRange)) {
				const auto [min, max] = ParseRange(content.take(ZRange));

				task->SetZRange(min, max);
			}

			if (constexpr auto Modality = "HT"; content.contains(Modality)) {
				const auto list = ParseIntArray(content.take(Modality));

				task->SetHT2DExclusive(true);
				task->SetHT3DExclusive(true);
				task->SetHT2DTimeStepList(list);
				task->SetHT3DTimeStepList(list);
			}

			if (constexpr auto Modality = "FL"; content.contains(Modality)) {
				const auto list = ParseIntArray(content.take(Modality));

				task->SetFL2DExclusive(true);
				task->SetFL3DExclusive(true);
				task->SetFL2DTimeStepList(list);
				task->SetFL3DTimeStepList(list);
			}

			if (constexpr auto Modality = "HT2D"; content.contains(Modality)) {
				task->SetHT2DExclusive(true);
				task->SetHT2DTimeStepList(ParseIntArray(content.take(Modality)));
			}

			if (constexpr auto Modality = "HT3D"; content.contains(Modality)) {
				task->SetHT3DExclusive(true);
				task->SetHT3DTimeStepList(ParseIntArray(content.take(Modality)));
			}

			if (constexpr auto Modality = "FL2D"; content.contains(Modality)) {
				task->SetFL2DExclusive(true);
				task->SetFL2DTimeStepList(ParseIntArray(content.take(Modality)));
			}

			if (constexpr auto Modality = "FL3D"; content.contains(Modality)) {
				task->SetFL3DExclusive(true);
				task->SetFL3DTimeStepList(ParseIntArray(content.take(Modality)));
			}

			if (constexpr auto Modality = "BF"; content.contains(Modality)) {
				task->SetBFExclusive(true);
				task->SetBFTimeStepList(ParseIntArray(content.take(Modality)));
			}

			d->queue.enqueue(task);

			Run([this, task] {
				OnScheduled(this, task.get());
			}, false);
		}
	}

	auto Scheduler::ParseIntArray(const QVariant& value) -> QList<int> {
		QList<int> list;

		if (value.type() == QVariant::String) {
			const auto splits = value.toString().split(' ');

			for (const auto& s : splits) {
				auto ok = false;

				if (const auto timestep = s.toInt(&ok); ok)
					list.push_back(timestep);
			}
		} else if (value.type() == QVariant::List) {
			for (const auto& i : value.toList()) {
				auto ok = false;

				if (const auto timestep = i.toInt(&ok); ok)
					list.push_back(timestep);
			}
		}

		return list;
	}

	auto Scheduler::ParseRange(const QVariant& value) -> std::tuple<int, int> {
		const auto map = value.toMap();
		const auto min = map.value("Min", -1).toInt();
		const auto max = map.value("Max", -1).toInt();

		return { min, max };
	}
}
