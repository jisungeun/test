#pragma once

#include <QObject>

#include "ITask.h"
#include "IConverterStatus.h"

#include "DataExporter.SchedulerExport.h"

namespace DataExporter::Scheduler {
	class DataExporter_Scheduler_API Task final : public QObject, public ITask, public IConverterStatus {
		Q_OBJECT

	public:
		explicit Task(const QString& src, const QString& dest, const QString& format);
		explicit Task(const QString& src, const QString& dest, const QString& format, const QMap<QString, QString>& options);
		~Task() override;

		auto GetSource() const -> QString override;
		auto GetDestination() const -> QString override;
		auto GetFormat() const -> QString override;
		auto GetOptions() const -> QMap<QString, QString> override;

		auto GetStatus() const -> TaskStatus override;
		auto GetErrorMessage() const -> std::optional<QString> override;
		auto GetProgress() const -> int override;

		auto SetProgress(int progress) -> void override;
		auto SetProgress(int progress, int max) -> void override;
		auto SetError(const QString& message) -> void;
		auto SetStatus(TaskStatus status) -> void;

	signals:
		auto Updated() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
