#pragma once

#include <QThread>

#include "IServiceProvider.h"

#include "IAppModule.h"
#include "IScheduler.h"

#include "DataExporter.SchedulerExport.h"

namespace DataExporter::Scheduler {
	class DataExporter_Scheduler_API Scheduler final : public QThread, public IAppModule, public IScheduler {
	public:
		explicit Scheduler(Tomocube::IServiceProvider* provider);
		~Scheduler() override;

		auto AddEvent(const SchedulerEventPtr& event) -> void override;
		auto RemoveEvent(const SchedulerEventPtr& event) -> void override;

		auto Start() -> void override;
		auto Stop() -> void override;

		auto Run() -> void override;
		auto IsRunning() const -> bool override;

		auto Schedule(const QString& src, const QString& dest, const QString& format, const QMap<QString, QString>& options) -> TaskPtr override;
		auto Unschedule(const TaskPtr& task) -> void override;

		auto GetTaskList() const -> TaskList override;

	protected:
		auto run() -> void override;
	
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
