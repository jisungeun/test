#include "Scheduler.h"

#include <QMutexLocker>
#include <QQueue>
#include <QWaitCondition>

#include "IConverterFinder.h"
#include "IGateway.h"

#include "ScheduleTaskOnGatewayRequested.h"
#include "Task.h"

namespace DataExporter::Scheduler {
	struct Scheduler::Impl {
		Tomocube::IServiceProvider* provider = nullptr;

		SchedulerEventList events;

		QMutex mutex;
		QWaitCondition waiter;

		QQueue<TaskPtr> queue;
		bool isCancelled = false;
	};

	Scheduler::Scheduler(Tomocube::IServiceProvider* provider) : QThread(), IScheduler(), d(new Impl) {
		d->provider = provider;
	}

	Scheduler::~Scheduler() {
		d->mutex.lock();
		d->isCancelled = true;
		d->waiter.wakeOne();
		d->mutex.unlock();

		wait();
	}

	auto Scheduler::AddEvent(const SchedulerEventPtr& event) -> void {
		if (!d->events.contains(event))
			d->events.push_back(event);
	}

	auto Scheduler::RemoveEvent(const SchedulerEventPtr& event) -> void {
		d->events.removeOne(event);
	}

	auto Scheduler::Start() -> void {
		const auto gateway = d->provider->GetService<IGateway>();
		gateway->AddEvent(std::make_shared<ScheduleTaskOnGatewayRequested>(d->provider));
	}

	auto Scheduler::Stop() -> void {}

	auto Scheduler::Run() -> void {
		start();
	}

	auto Scheduler::IsRunning() const -> bool {
		return isRunning();
	}

	auto Scheduler::Schedule(const QString& src, const QString& dest, const QString& format, const QMap<QString, QString>& options) -> TaskPtr {
		QMutexLocker locker(&d->mutex);

		//for (const auto& t : d->queue) {
		//	if (t->GetSource() == src && t->GetDestination() == dest && t->GetFormat() == format && t->GetOptions() == options)
		//		return {};
		//}

		auto task = std::make_shared<Task>(src, dest, format, options);
		connect(task.get(), &Task::Updated, this, [this, task] {
			for (const auto& e : d->events)
				e->OnTaskUpdated(task);
		});

		d->queue.enqueue(task);

		for (const auto& e : d->events)
			e->OnTaskSchedulled(task);

		d->waiter.wakeOne();
		return task;
	}

	auto Scheduler::Unschedule(const TaskPtr& task) -> void {
		QMutexLocker locker(&d->mutex);
		d->queue.removeOne(task);

		for (const auto& e : d->events)
			e->OnTaskCancelled(task);
	}

	auto Scheduler::GetTaskList() const -> TaskList {
		return d->queue.toVector().toList();
	}

	auto Scheduler::run() -> void {
		while (!d->isCancelled) {
			QMutexLocker locker(&d->mutex);

			if (d->queue.isEmpty())
				d->waiter.wait(&d->mutex);

			if (const auto task = std::dynamic_pointer_cast<Task>(d->queue.head())) {
				locker.unlock();

				const auto finder = d->provider->GetService<IConverterFinder>();

				if (const auto converter = finder->GetConverter(task->GetFormat(), task->GetOptions())) {
					try {
						task->SetStatus(TaskStatus::Running);
						converter->Start(task->GetSource(), task->GetDestination(), task);

						task->SetStatus(TaskStatus::Completed);
					} catch (const ConverterException& ex) {
						task->SetError(ex.what());
					}
				} else {
					task->SetError(QString("Converter (%1) Not Found").arg(task->GetFormat()));
				}

				locker.relock();
				d->queue.dequeue();
			}
		}
	}
}