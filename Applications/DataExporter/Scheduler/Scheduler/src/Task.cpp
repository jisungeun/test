#include "Task.h"

namespace DataExporter::Scheduler {
	struct Task::Impl {
		QString src;
		QString dest;
		QString format;
		QMap<QString, QString> options;

		TaskStatus status = TaskStatus::Pending;
		std::optional<QString> message = std::nullopt;
		int progress = 0;
	};

	Task::Task(const QString& src, const QString& dest, const QString& format) : QObject(), ITask(), IConverterStatus(), d(new Impl) {
		d->src = src;
		d->dest = dest;
		d->format = format;
	}

	Task::Task(const QString& src, const QString& dest, const QString& format, const QMap<QString, QString>& options) : Task(src, dest, format) {
		d->options = options;
	}

	Task::~Task() = default;

	auto Task::GetSource() const -> QString {
		return d->src;
	}

	auto Task::GetDestination() const -> QString {
		return d->dest;
	}

	auto Task::GetFormat() const -> QString {
		return d->format;
	}

	auto Task::GetOptions() const -> QMap<QString, QString> {
		return d->options;
	}

	auto Task::GetStatus() const -> TaskStatus {
		return d->status;
	}

	auto Task::GetErrorMessage() const -> std::optional<QString> {
		return d->message;
	}

	auto Task::GetProgress() const -> int {
		return d->progress;
	}

	auto Task::SetProgress(int progress) -> void {
		if (d->progress != progress) {
			d->progress = progress;

			emit Updated();
		}
	}

	auto Task::SetProgress(int progress, int max) -> void {
		if (const auto updated = static_cast<int>(progress / static_cast<double>(max) * 100.0); updated != d->progress) {
			d->progress = updated;

			emit Updated();
		}
	}

	auto Task::SetError(const QString& message) -> void {
		d->message = message;
		d->status = TaskStatus::Failed;

		emit Updated();
	}

	auto Task::SetStatus(TaskStatus status) -> void {
		d->status = status;

		emit Updated();
	}
}
