#include "ScheduleTaskOnGatewayRequested.h"

#include "IScheduler.h"

namespace DataExporter::Scheduler {
	struct ScheduleTaskOnGatewayRequested::Impl {
		Tomocube::IServiceProvider* provider = nullptr;
	};

	ScheduleTaskOnGatewayRequested::ScheduleTaskOnGatewayRequested(Tomocube::IServiceProvider* provider) : IGatewayEvent(), d(new Impl) {
		d->provider = provider;
	}

	ScheduleTaskOnGatewayRequested::~ScheduleTaskOnGatewayRequested() = default;

	auto ScheduleTaskOnGatewayRequested::OnRequested(const RequestPtr& request) -> void {
		const auto scheduler = d->provider->GetService<IScheduler>();

		scheduler->Schedule(request->GetSource(), request->GetDestination(), request->GetFormat(), request->GetOptions());

		if (!scheduler->IsRunning())
			scheduler->Run();
	}
}
