#pragma once

#include "IServiceProvider.h"

#include "IGatewayEvent.h"

#include "DataExporter.Scheduler.UseCaseExport.h"

namespace DataExporter::Scheduler {
	class DataExporter_Scheduler_UseCase_API ScheduleTaskOnGatewayRequested final : public IGatewayEvent {
	public:
		explicit ScheduleTaskOnGatewayRequested(Tomocube::IServiceProvider* provider);
		~ScheduleTaskOnGatewayRequested() override;

		auto OnRequested(const RequestPtr& request) -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
