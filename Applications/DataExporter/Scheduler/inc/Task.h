#pragma once

#include "ITask.h"

#include "DataExporter.SchedulerExport.h"

namespace DataExporter::Scheduler {
	class DataExporter_Scheduler_API Task final : public ITask {
	public:
		explicit Task();
		~Task() override;

		auto SetTCFPath(const QString& path) -> void;
		auto SetSavePath(const QString& path) -> void;
		auto SetSaveFormat(const QString& format) -> void;
		auto SetOptionMap(const QVariantMap& options) -> void;

		auto SetZRange(int min, int max) -> void;
		auto SetHT2DExclusive(bool exclusive) -> void;
		auto SetHT3DExclusive(bool exclusive) -> void;
		auto SetFL2DExclusive(bool exclusive) -> void;
		auto SetFL3DExclusive(bool exclusive) -> void;
		auto SetBFExclusive(bool exclusive) -> void;

		auto SetHT2DTimeStepList(const QList<int>& list) -> void;
		auto SetHT3DTimeStepList(const QList<int>& list) -> void;
		auto SetFL2DTimeStepList(const QList<int>& list) -> void;
		auto SetFL3DTimeStepList(const QList<int>& list) -> void;
		auto SetBFTimeStepList(const QList<int>& list) -> void;

		auto GetTCFPath() const -> QString override;
		auto GetSavePath() const -> QString override;
		auto GetSaveFormat() const -> QString override;
		auto GetOptionMap() const -> QVariantMap override;
		
		auto GetZRangeMin() const -> int override;
		auto GetZRangeMax() const -> int override;

		auto IsExclusiveHT2D() const -> bool override;
		auto IsExclusiveHT3D() const -> bool override;
		auto IsExclusiveFL2D() const -> bool override;
		auto IsExclusiveFL3D() const -> bool override;
		auto IsExclusiveBF() const -> bool override;

		auto ContainsZRange() const -> bool override;
		auto ContainsExclusiveHT2D(int timestep) const -> bool override;
		auto ContainsExclusiveHT3D(int timestep) const -> bool override;
		auto ContainsExclusiveFL2D(int timestep) const -> bool override;
		auto ContainsExclusiveFL3D(int timestep) const -> bool override;
		auto ContainsExclusiveBF(int timestep) const -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
