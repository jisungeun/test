#pragma once

#include "IServiceProvider.h"

#include "IRequest.h"
#include "IScheduler.h"

#include "DataExporter.SchedulerExport.h"

namespace DataExporter::Scheduler {
	class DataExporter_Scheduler_API Scheduler final : public IScheduler {
		Implements(IScheduler)

	public:
		explicit Scheduler(IServiceProvider* provider);
		~Scheduler() override;

		auto Start(const QStringList& args) -> bool override;
		auto Stop() -> void override;

		auto IsEmpty() const -> bool override;
		auto HasNext() const -> bool override;
		auto GetTaskCount() const -> int override;
		auto GetCurrentTask() const -> ITask* override;

		auto NextTask() -> ITask* override;

	protected slots:
		auto OnRequested(IService* sender, const IRequest* request) -> void;

	protected:
		auto ParseIntArray(const QVariant& value) -> QList<int>;
		auto ParseRange(const QVariant& value) -> std::tuple<int, int>;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
