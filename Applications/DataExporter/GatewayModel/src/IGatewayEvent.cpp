#include "IGatewayEvent.h"

namespace DataExporter {
	IGatewayEvent::~IGatewayEvent() = default;

	auto IGatewayEvent::OnRequested(const RequestPtr& request) -> void {}
}
