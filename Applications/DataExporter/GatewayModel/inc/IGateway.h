#pragma once

#include "IHostedService.h"

#include "EventHandler.h"

#include "IRequest.h"

#include "DataExporter.GatewayModelExport.h"

namespace DataExporter {
	class DataExporter_GatewayModel_API IGateway : public virtual IHostedService {
	public:
		EventHandler<IRequest*> OnRequested;
	};
}
