#pragma once

#include <QVariantMap>

#include "IService.h"

#include "DataExporter.GatewayModelExport.h"

namespace DataExporter {
	class DataExporter_GatewayModel_API IRequest : public virtual IService {
	public:
		virtual auto GetMethod() const -> QString = 0;
		virtual auto GetCreated() const -> QDateTime = 0;
		virtual auto GetContent() const -> QVariantMap = 0;
	};
}