#pragma once

#include "IRequest.h"

#include "DataExporter.GatewayModelExport.h"

namespace DataExporter {
	class IGatewayEvent;
	using GatewayEventPtr = std::shared_ptr<IGatewayEvent>;
	using GatewayEventList = QList<GatewayEventPtr>;

	class DataExporter_GatewayModel_API IGatewayEvent {
	public:
		virtual ~IGatewayEvent();
		
		virtual auto OnRequested(const RequestPtr& request) -> void;
	};
}
