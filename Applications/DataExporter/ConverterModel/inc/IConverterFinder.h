#pragma once

#include <memory>

#include <QString>

#include "IService.h"
#include "IConverter.h"

#include "DataExporter.ConverterModelExport.h"

namespace DataExporter {
	class DataExporter_ConverterModel_API IConverterFinder : public virtual Tomocube::IService {
	public:
		virtual auto GetConverter(const QString& format, const QMap<QString, QString>& options) -> ConverterPtr = 0;
	};
}
