#pragma once

#include <memory>

#include <QList>

#include "DataExporter.ConverterModelExport.h"

namespace DataExporter {
	class IConverterStatus;
	using ConverterStatusPtr = std::shared_ptr<IConverterStatus>;
	using ConverterStatusList = QList<ConverterStatusPtr>;

	class DataExporter_ConverterModel_API IConverterStatus {
	public:
		virtual ~IConverterStatus() = default;

		virtual auto SetProgress(int progress) -> void = 0;
		virtual auto SetProgress(int progress, int max) -> void = 0;
	};
}
