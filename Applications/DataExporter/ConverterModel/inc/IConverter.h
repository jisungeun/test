#pragma once

#include <QMap>

#include "IConverterStatus.h"

#include "DataExporter.ConverterModelExport.h"

namespace DataExporter {
	class IConverter;
	using ConverterPtr = std::shared_ptr<IConverter>;
	using ConverterList = QList<ConverterPtr>;

	class DataExporter_ConverterModel_API IConverter {
	public:
		virtual ~IConverter() = default;

		virtual auto Start(const QString& src, const QString& dest, const ConverterStatusPtr& status) -> void = 0;
	};
	
	class DataExporter_ConverterModel_API ConverterException final : public std::exception {
	public:
		explicit ConverterException(const char* message = {});
	};
}
