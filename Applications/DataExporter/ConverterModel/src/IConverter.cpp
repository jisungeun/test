#include "IConverter.h"

namespace DataExporter {
	ConverterException::ConverterException(const char* message) : std::exception(message) {}
}
