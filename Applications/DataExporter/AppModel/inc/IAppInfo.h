#pragma once

#include <QString>

#include "IService.h"

#include "DataExporter.AppModelExport.h"

namespace DataExporter {
	class DataExporter_AppModel_API IAppInfo : public virtual Tomocube::IService {
	public:
		virtual auto GetSoftwareName() const -> QString = 0;
		virtual auto GetVersion() const -> QString = 0;
	};
}
