#pragma once

#include <memory>
#include <stdexcept>

#include <QString>

#include "IService.h"

#include "DataExporter.AppModelExport.h"

namespace DataExporter {
	class DataExporter_AppModel_API IAppModule : public virtual Tomocube::IService {
	public:
		virtual auto Start() -> void = 0;
		virtual auto Stop() -> void = 0;
	};

	class DataExporter_AppModel_API ModuleException final : public std::exception {
	public:
		explicit ModuleException(const char* message = {});
	};
}
