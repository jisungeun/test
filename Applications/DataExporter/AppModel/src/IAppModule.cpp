#include "IAppModule.h"

namespace DataExporter {
	ModuleException::ModuleException(const char* message) : std::exception(message) {}
}
