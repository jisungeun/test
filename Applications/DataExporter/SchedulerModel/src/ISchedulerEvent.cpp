#include "ISchedulerEvent.h"

namespace DataExporter {
	ISchedulerEvent::~ISchedulerEvent() = default;

	auto ISchedulerEvent::OnTaskSchedulled(const TaskPtr& task) -> void {}

	auto ISchedulerEvent::OnTaskCancelled(const TaskPtr& task) -> void {}

	auto ISchedulerEvent::OnTaskUpdated(const TaskPtr& task) -> void {}
}
