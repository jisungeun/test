#pragma once

#include <QVariantMap>

#include "IService.h"

#include "DataExporter.SchedulerModelExport.h"

namespace DataExporter {
	class DataExporter_SchedulerModel_API ITask : public virtual IService {
	public:
		virtual auto GetTCFPath() const -> QString = 0;
		virtual auto GetSavePath() const -> QString = 0;
		virtual auto GetSaveFormat() const -> QString = 0;
		virtual auto GetOptionMap() const -> QVariantMap = 0;

		virtual auto GetZRangeMin() const -> int = 0;
		virtual auto GetZRangeMax() const -> int = 0;

		virtual auto IsExclusiveHT2D() const -> bool = 0;
		virtual auto IsExclusiveHT3D() const -> bool = 0;
		virtual auto IsExclusiveFL2D() const -> bool = 0;
		virtual auto IsExclusiveFL3D() const -> bool = 0;
		virtual auto IsExclusiveBF() const -> bool = 0;

		virtual auto ContainsZRange() const -> bool = 0;
		virtual auto ContainsExclusiveHT2D(int timestep) const -> bool = 0;
		virtual auto ContainsExclusiveHT3D(int timestep) const -> bool = 0;
		virtual auto ContainsExclusiveFL2D(int timestep) const -> bool = 0;
		virtual auto ContainsExclusiveFL3D(int timestep) const -> bool = 0;
		virtual auto ContainsExclusiveBF(int timestep) const -> bool = 0;
	};
}
