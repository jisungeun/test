#pragma once

#include "EventHandler.h"
#include "IHostedService.h"

#include "ITask.h"

#include "DataExporter.SchedulerModelExport.h"

namespace DataExporter {
	class DataExporter_SchedulerModel_API IScheduler : public virtual IHostedService {
	public:
		virtual auto IsEmpty() const -> bool = 0;
		virtual auto HasNext() const -> bool = 0;
		virtual auto GetTaskCount() const -> int = 0;
		virtual auto GetCurrentTask() const -> ITask* = 0;

		virtual auto NextTask() -> ITask* = 0;

		EventHandler<ITask*> OnScheduled;
		EventHandler<ITask*> OnNextTask;
	};
}
