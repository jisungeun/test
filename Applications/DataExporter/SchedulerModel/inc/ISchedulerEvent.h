#pragma once

#include "ITask.h"

#include "DataExporter.SchedulerModelExport.h"

namespace DataExporter {
	class ISchedulerEvent;
	using SchedulerEventPtr = std::shared_ptr<ISchedulerEvent>;
	using SchedulerEventList = QList<SchedulerEventPtr>;

	class DataExporter_SchedulerModel_API ISchedulerEvent {
	public:
		virtual ~ISchedulerEvent();
		
		virtual auto OnTaskSchedulled(const TaskPtr& task) -> void;
		virtual auto OnTaskCancelled(const TaskPtr& task) -> void;
		virtual auto OnTaskUpdated(const TaskPtr& task) -> void;
	};
}
