#include "MetaInfoDefines.h"
#include "TimeStampInfo.h"

namespace TomoAnalysis::AppEntity {    
    /**
    * \brief TimeStamp meta information
    */
    struct TimeStampInfo::Impl {
        double x;
        double y;
        double rgb[3];
        double fontSize;
        bool isVisible;
        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other)const->bool;
    };
    auto TimeStampInfo::Impl::operator=(const Impl& other) -> Impl& {
        x = other.x;
        y = other.y;
        for (auto i = 0; i < 3; i++) {
            rgb[i] = other.rgb[i];
        }
        fontSize = other.fontSize;
        isVisible = other.isVisible;
        return *this;
    }
    auto TimeStampInfo::Impl::operator==(const Impl& other) const -> bool {
        if (false == AreSame(x, other.x)) {
            return false;
        }
        if (false == AreSame(y, other.y)) {
            return false;
        }
        for (auto i = 0; i < 3; i++) {
            if (false == AreSame(rgb[i], other.rgb[i])) {
                return false;
            }
        }
        if (false == AreSame(fontSize, other.fontSize)) {
            return false;
        }
        if (isVisible != other.isVisible) {
            return false;
        }
        return true;
    }
    TimeStampInfo::TimeStampInfo() : d{ new Impl } {

    }
    TimeStampInfo::TimeStampInfo(const TimeStampInfo& other) : d{ new Impl } {
        *this = other;
    }
    TimeStampInfo::~TimeStampInfo() {

    }
    auto TimeStampInfo::SetCoordinate(double x, double y) const -> void {
        d->x = x;
        d->y = y;
    }
    auto TimeStampInfo::GetCoordinate() const -> std::tuple<double, double> {
        return std::make_tuple(d->x, d->y);
    }
    auto TimeStampInfo::SetColor(double r, double g, double b) const -> void {
        d->rgb[0] = r;
        d->rgb[1] = g;
        d->rgb[2] = b;
    }
    auto TimeStampInfo::GetColor() const -> std::tuple<double, double, double> {
        return std::make_tuple(d->rgb[0], d->rgb[1], d->rgb[2]);
    }
    auto TimeStampInfo::SetFontSize(double size) const -> void {
        d->fontSize = size;
    }
    auto TimeStampInfo::GetFontSize() const -> double {
        return d->fontSize;
    }
    auto TimeStampInfo::SetIsVisible(bool isVisible) const -> void {
        d->isVisible = isVisible;
    }
    auto TimeStampInfo::isVisible() const -> bool {
        return d->isVisible;
    }
    auto TimeStampInfo::operator=(const TimeStampInfo& other) -> TimeStampInfo& {
        *d = *other.d;
        return *this;
    }
    auto TimeStampInfo::operator==(const TimeStampInfo& other) const -> bool {
        return *d == *other.d;
    }
}