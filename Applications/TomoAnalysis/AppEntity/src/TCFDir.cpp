#include <iostream>
#include <QDirIterator>

#include "TCFDir.h"

namespace TomoAnalysis {
    struct TCFDir::Impl {
        QString path;
        List dirList;
        QPoint pos;
        QStringList fileList;
        QMap<QString, bool> validityList; // <filename, valid>
    };

    TCFDir::TCFDir() : d{ new Impl } {
        d->pos.setX(100);
        d->pos.setY(100);
    }

    TCFDir::TCFDir(const TCFDir& other) : d{ new Impl } {
        *d = *other.d;
    }

    TCFDir::~TCFDir() {

    }

    auto TCFDir::SetPosition(const QPoint& pos) const -> void {
        d->pos = pos;
    }

    auto TCFDir::GetPosition() const -> QPoint {
        return d->pos;
    }

    auto TCFDir::GetPath() const ->QString {
        return d->path;
    }

    auto TCFDir::SetPath(const QString& path) const ->void {
        d->path = path;
    }

    auto TCFDir::GetName() const->QString {
        return QDir(d->path).dirName();
    }

    auto TCFDir::GetDirList() ->TCFDir::List {
        return d->dirList;
    }

    auto TCFDir::SetDirList(const TCFDir::List& list) const ->void {
        d->dirList = list;
    }

    auto TCFDir::GetFileList()  ->QStringList {
        return d->fileList;
    }

    auto TCFDir::SetFileList(const QStringList& list) const ->void {
        d->fileList = list;
    }

    auto TCFDir::GetValidityList()  ->QMap<QString, bool> {
        return d->validityList;
    }

    auto TCFDir::SetValidityList(const QMap<QString, bool>& list) const ->void {
        d->validityList = list;
    }

    auto TCFDir::ScanDir()->bool {
        if (true == d->path.isEmpty()) {
            return false;
        }

        QDir dir(d->path);
        if (false == dir.exists() || false == dir.isReadable()) {            
            return false;
        }

        dir.setNameFilters(QStringList() << "*.tcf" << "*.TCF");
        dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);

        // scanning
        // file
        auto fileList = dir.entryList();
        for (const auto& file : fileList) {
            d->fileList.push_back(file);
            d->validityList[file] = true;
        }

        // sub directory
        dir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
        auto subDirList = dir.entryList();

        for (const auto& subDir : subDirList) {            
            auto subTCFDir = std::make_shared<TCFDir>();
            auto subPath = QString("%1/%2").arg(dir.absolutePath()).arg(subDir);
            subTCFDir->SetPath(subPath);

            if (false == subTCFDir->ScanDir()) {
                continue;
            }            
            
            if (1 == subTCFDir->GetFileList().size()) {                
                auto split = subTCFDir->GetPath().split("/");
                auto subName = split[split.size() - 1];
                auto file = subTCFDir->GetFileList()[0];
                auto folFile = subName + "/" + subTCFDir->GetFileList()[0];
                if(file.chopped(4).compare(subName)==0) {
                    d->fileList.append(folFile);
                }else {
                    d->dirList.append(subTCFDir);
                }                
            }
            else {
                d->dirList.append(subTCFDir);
            }
        }

        if (d->dirList.size() + d->fileList.size() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    auto TCFDir::GetAllFileFullPathList(const TCFDir::Pointer& dir, QStringList& list) const ->void {
        if(nullptr == dir) {
            return;
        }

        for (const auto& file : dir->GetFileList()) {
            list.append(QString("%1/%2").arg(dir->GetPath()).arg(file));
        }

        for(const auto& sub : dir->GetDirList()) {
            GetAllFileFullPathList(sub, list);
        }
    }
}