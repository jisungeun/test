#include <iostream>
#include <QFileInfo>
#include <QDir>

#include "ProjectInfo.h"

namespace TomoAnalysis {
	struct ProjectInfo::Impl {
		QString path; // absolution full path (include file name, format)
		QString name;
		QString user;
		QDateTime createdTime;
		QDateTime modifiedTime;
		TCFDir::List tcfDirList;
		PlaygroundInfo::List playgroundInfoList;
		PluginAppInfo::List measurementAppInfoList;
		SubProjectInfoList subProjectInfoList;
		QString comment;
		bool root = true;
		QMap<int, QString> worksets;
		QMap<int, TCFDir::List> worksetTcf;

		PlaygroundInfo::Pointer currentPlayground{ nullptr };
		PluginAppInfo::Pointer currentAppInfo{ nullptr };
		HyperCube::Pointer currentHyperCube{ nullptr };
	};

	ProjectInfo::ProjectInfo() : d{ new Impl } {}

	ProjectInfo::~ProjectInfo() {

	}

	auto ProjectInfo::Init() -> void {

	}

	auto ProjectInfo::GetPath(void) const ->QString {
		return d->path;
	}

	auto ProjectInfo::SetPath(const QString& path) const ->void {
		d->path = QFileInfo(path).absoluteFilePath();
	}

	auto ProjectInfo::GetName(void) const ->QString {
		return d->name;
	}

	auto ProjectInfo::SetName(const QString& name) const ->void {
		d->name = name;
	}

	auto ProjectInfo::UpdatePGInfo(const QString& oriName, const QString& newName) const -> void {
		Q_UNUSED(oriName)
			for (auto info : d->playgroundInfoList) {
				auto oriPath = info->GetPath();
				QFileInfo file(oriPath);
				QDir dir = file.dir();
				dir.cdUp();
				dir.cdUp();
				auto parent = dir.absolutePath();
				auto newPath = parent + "/" + newName + "/playground/" + info->GetName() + ".tcpg";

				info->SetPath(newPath);
			}
	}

	auto ProjectInfo::GetUser(void) const ->QString {
		return d->user;
	}

	auto ProjectInfo::SetUser(const QString& user) const ->void {
		d->user = user;
	}

	auto ProjectInfo::GetCreatedTime(void) const ->QDateTime {
		return d->createdTime;
	}

	auto ProjectInfo::SetCreatedTime(const QDateTime& dateTime) const ->void {
		d->createdTime = dateTime;
	}

	auto ProjectInfo::GetModifiedTime(void) const ->QDateTime {
		return d->modifiedTime;
	}

	auto ProjectInfo::SetModifiedTime(const QDateTime& dateTime) const ->void {
		d->modifiedTime = dateTime;
	}

	auto ProjectInfo::GetTCFDirList(void) const ->TCFDir::List {
		return d->tcfDirList;
	}

	auto ProjectInfo::SetTCFDirList(const TCFDir::List& tcfDirList) const ->void {
		d->tcfDirList = tcfDirList;
	}

	auto ProjectInfo::AddTCFDir(const TCFDir::Pointer& tcfDir) const ->void {
		d->tcfDirList.append(tcfDir);
	}

	auto ProjectInfo::GetTCFDir(const QString& dirPath) const ->TCFDir::Pointer {
		for (const auto& dir : d->tcfDirList) {
			if (nullptr == dir) {
				continue;
			}

			if (dirPath == dir->GetPath()) {
				return dir;
			}
		}

		return nullptr;
	}

	auto ProjectInfo::RemoveTCFDir(const QString& dirPath) const ->bool {
		for (const auto& dir : d->tcfDirList) {
			if (nullptr == dir) {
				continue;
			}

			if (dirPath == dir->GetPath()) {
				d->tcfDirList.removeOne(dir);
				return true;
			}
		}

		return false;
	}

	auto ProjectInfo::ExistTCFDir(const QString& dirPath) const ->bool {
		for (const auto& dir : d->tcfDirList) {
			if (nullptr == dir) {
				continue;
			}

			if (dirPath == dir->GetPath()) {
				return true;
			}
		}

		return false;
	}

	auto ProjectInfo::ScanTCFDir(void) const ->void {
		for (const auto& dir : d->tcfDirList) {
			if (nullptr == dir) {
				continue;
			}
			dir->ScanDir();
		}

		for (const auto& playground : d->playgroundInfoList) {
			if (nullptr == playground) {
				continue;
			}

			for (const auto& dir : playground->GetTCFDirList()) {
				if (nullptr == dir) {
					continue;
				}
				dir->ScanDir();
			}
		}
	}

	auto ProjectInfo::AddWorkset(const QString& name, int id) -> void {
		d->worksets[id] = name;
	}

	auto ProjectInfo::RemoveWorkset(int id) -> void {
		d->worksets.remove(id);
	}

	auto ProjectInfo::ExistsWorkset(int id) -> bool {
		return d->worksets.contains(id);
	}

	auto ProjectInfo::GetWorksets() const -> const QMap<int, QString>& {
		return d->worksets;
	}

	auto ProjectInfo::GetWorksetName(int id) const -> const QString& {
		return d->worksets[id];
	}

	auto ProjectInfo::GetWorksetDirs() const -> const QMap<int, TCFDir::List>& {
		return d->worksetTcf;
	}

	auto ProjectInfo::LinkTcfDirToWorkset(int worksetId, const TCFDir::Pointer& tcfDir) -> void {
		if (!d->worksetTcf[worksetId].contains(tcfDir))
			d->worksetTcf[worksetId].push_back(tcfDir);
	}

	auto ProjectInfo::LinkTcfDirsToWorkset(int worksetId, const TCFDir::List& tcfDir) -> void {
		for(const auto& dir : tcfDir) {
			LinkTcfDirToWorkset(worksetId, dir);
		}
	}

	auto ProjectInfo::UnlinkTcfDirToWorkset(int worksetId, const TCFDir::Pointer& tcfDir) -> void {
		d->worksetTcf[worksetId].removeOne(tcfDir);
	}

	auto ProjectInfo::GetWorksetIdOfTcfDir(const TCFDir::Pointer& tcfDir) const -> QVector<int> {
		QVector<int> ids;
		for (const auto key : d->worksetTcf.keys()) {
			for (const auto& tcf : d->worksetTcf[key]) {
				if (tcf->GetName() == tcfDir->GetName()) {
					ids << key;
				}
			}
		}

		return ids;
	}

	auto ProjectInfo::GetCountOfWorkset(int worksetId) const -> int {
		if (d->worksetTcf.contains(worksetId))
			return d->worksetTcf[worksetId].count();

		return 0;
	}

	auto ProjectInfo::GetPlaygroundInfoList(void) const ->PlaygroundInfo::List {
		return d->playgroundInfoList;
	}

	auto ProjectInfo::SetPlaygroundInfoList(const PlaygroundInfo::List& playgroundInfoList) const ->void {
		d->playgroundInfoList = playgroundInfoList;
	}

	auto ProjectInfo::AddPlaygroundInfo(const PlaygroundInfo::Pointer& playgroundInfo) const ->void {
		d->playgroundInfoList.append(playgroundInfo);
	}

	auto ProjectInfo::FindPlaygroundInfo(const QString& path) const ->PlaygroundInfo::Pointer {
		for (auto info : d->playgroundInfoList) {
			if (nullptr == info) {
				continue;
			}

			if (path == info->GetPath()) {
				return info;
			}
		}

		return nullptr;
	}

	auto ProjectInfo::RemovePlaygroundInfo(const QString& path) const->bool {
		const auto foundPlayground = this->FindPlaygroundInfo(path);
		if (nullptr == foundPlayground) {
			return false;
		}

		return d->playgroundInfoList.removeOne(foundPlayground);
	}

	auto ProjectInfo::SetCurrentHyperCube(const HyperCube::Pointer& hyper) -> void {
		d->currentHyperCube = hyper;
	}

	auto ProjectInfo::GetCurrentHyperCube() const -> HyperCube::Pointer {
		return d->currentHyperCube;
	}

	auto ProjectInfo::SetCurrentAppInfo(const PluginAppInfo::Pointer& info) -> void {
		d->currentAppInfo = info;
	}

	auto ProjectInfo::GetCurrentAppInfo() const -> PluginAppInfo::Pointer {
		return d->currentAppInfo;
	}

	auto ProjectInfo::GetCurrentPlayground(void) const ->PlaygroundInfo::Pointer {
		return d->currentPlayground;
	}

	auto ProjectInfo::SetCurrentPlayground(const QString& path) const ->bool {
		const auto foundPlayground = this->FindPlaygroundInfo(path);
		d->currentPlayground = foundPlayground;

		return true;
	}

	auto ProjectInfo::GetMeasurementAppInfoList(void) const ->PluginAppInfo::List {
		return d->measurementAppInfoList;
	}

	auto ProjectInfo::SetMeasurementAppInfoList(const PluginAppInfo::List& measurementAppInfoList) const ->void {
		d->measurementAppInfoList = measurementAppInfoList;
	}

	auto ProjectInfo::GetSubProjectList(void) const ->SubProjectInfoList {
		return d->subProjectInfoList;
	}

	auto ProjectInfo::SetSubProjectList(const SubProjectInfoList& subProjectInfoList) const ->void {
		d->subProjectInfoList = subProjectInfoList;
	}

	auto ProjectInfo::AddSubProject(const ProjectInfo::Pointer& project) const ->void {
		d->subProjectInfoList.append(project);
	}

	auto ProjectInfo::FindSubProject(const QString& path) const ->ProjectInfo::Pointer {
		for (auto sub : d->subProjectInfoList) {
			if (nullptr == sub) {
				continue;
			}

			if (path == sub->GetPath()) {
				return sub;
			}
		}

		return nullptr;
	}

	auto ProjectInfo::GetComment(void) const ->QString {
		return d->comment;
	}

	auto ProjectInfo::SetComment(const QString& comment) const ->void {
		d->comment = comment;
	}

	auto ProjectInfo::IsRoot(void) const -> bool {
		return d->root;
	}

	auto ProjectInfo::SetRoot(const bool& root) const ->void {
		d->root = root;
	}
}