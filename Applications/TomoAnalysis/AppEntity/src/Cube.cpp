#include "Cube.h"

namespace TomoAnalysis {
    struct Cube::Impl {
        ID id;
        QString name;
        QPoint pos;
        TCFDir::List tcfDirList;
        PluginAppInfo::List measurementAppInfoList;
        ResultInfo::List resultInfoList;

    };
    Cube::Cube() : d{ new Impl } {
        d->pos.setX(100);
        d->pos.setY(100);
    }
    Cube::Cube(const Cube::Pointer& other) : d{ new Impl } {
        *d = *other->d;
    }
    Cube::~Cube() {
        
    }
    auto Cube::Init() -> void {
        
    }

    auto Cube::GetID() const ->ID {
        return d->id;
    }

    auto Cube::SetID(const ID& id) const ->void {
        d->id = id;
    }

    auto Cube::SetPosition(const QPoint& pos) const -> void {
        d->pos = pos;
    }

    auto Cube::GetPosition() const -> QPoint {
        return d->pos;
    }

    auto Cube::GetName() const->QString {
        return d->name;
    }

    auto Cube::SetName(const QString& name) const ->void {
        d->name = name;
    }

    auto Cube::GetTCFDirList() const ->TCFDir::List {
        return d->tcfDirList;
    }

    auto Cube::SetTCFDirList(const TCFDir::List& list) const ->void {
        d->tcfDirList = list;
    }

    auto Cube::AddTCFDir(const TCFDir::Pointer& tcfDir) const ->void {
        d->tcfDirList.append(tcfDir);
    }

    auto Cube::FindTCFDir(const QString& dirPath) const -> TCFDir::Pointer {
       for(auto i=0;i<d->tcfDirList.size();i++) {
           if(d->tcfDirList[i]->GetPath().compare(dirPath)==0) {
               return d->tcfDirList[i];
           }
       }

       return nullptr;
    }

    auto Cube::RemoveTCFDir(const QString& dirPath) const -> void {
        for(auto i=0;i<d->tcfDirList.size();i++) {
            if(d->tcfDirList[i]->GetPath().compare(dirPath)==0) {
                d->tcfDirList.removeAt(i);
                return;
            }
        }
    }

    auto Cube::GetMeasurementAppInfoList() const ->PluginAppInfo::List {
        return d->measurementAppInfoList;
    }

    auto Cube::SetMeasurementAppInfoList(const PluginAppInfo::List& list) const ->void {
        d->measurementAppInfoList = list;
    }

    auto Cube::AddApp(const PluginAppInfo::Pointer& app) const -> void {
        d->measurementAppInfoList.append(app);
    }

    auto Cube::FindApp(const QString& appPath) const -> PluginAppInfo::Pointer {
        for(const auto & app: d->measurementAppInfoList) {
            if(app == nullptr) {
                continue;
            }
            if(app->GetPath()==appPath) {
                return app;
            }
        }
        return nullptr;
    }

    auto Cube::RemoveApp(const QString& appPath) const -> void {
        for(auto i=0;i<d->measurementAppInfoList.size();i++) {
            if(d->measurementAppInfoList[i]->GetPath()==appPath) {                
                d->measurementAppInfoList.removeAt(i);
                return;
            }
        }
    }


    auto Cube::GetResultInfoList() const ->ResultInfo::List {
        return d->resultInfoList;
    }

    auto Cube::SetResultInfoList(const ResultInfo::List& list) const ->void {
        d->resultInfoList = list;
    }

}