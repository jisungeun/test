#include <iostream>

#include <QStringList>

#include "PluginAppInfo.h"

namespace TomoAnalysis {
    struct PluginAppInfo::Impl {
        QString tabName;
        QString name;
        QString path;
        QString license;
        QPoint pos;
        bool loaded{ false };
        ProcessorInfo::List procList;
        std::tuple<bool,bool> appType;
    };

    PluginAppInfo::PluginAppInfo() : d{new Impl} {
        d->pos.setX(100);
        d->pos.setY(100);
        d->procList = ProcessorInfo::List();
    }

    PluginAppInfo::PluginAppInfo(const PluginAppInfo& other) : d{new Impl} {
        *d = *other.d;
    }

    PluginAppInfo::~PluginAppInfo() {
        
    }

    auto PluginAppInfo::Init() -> void {
        
    }

    auto PluginAppInfo::GetLoaded() const -> bool {
        return d->loaded;
    }

    auto PluginAppInfo::SetLoaded(const bool& load) const -> void {
        d->loaded = load;
    }

    auto PluginAppInfo::GetTabName() const -> QString {
        return d->tabName;
    }

    auto PluginAppInfo::SetTabName(const QString& name) -> void {
        d->tabName = name;
    }

    auto PluginAppInfo::SetName(const QString& name) const -> void {
        d->name = name;
    }

    auto PluginAppInfo::GetName() const -> QString {
        return d->name;
    }

    auto PluginAppInfo::ScanName() const -> void {
        auto split = d->path.split("liborg_tomocube_");
        auto size = split.size();
        d->name = split[size - 1];
    }

    auto PluginAppInfo::SetLicense(const QString& license) const -> void {
        d->license = license;
    }

    auto PluginAppInfo::GetLicense() const -> QString {
        return d->license;
    }

    auto PluginAppInfo::SetProcessors(const ProcessorInfo::List procList) const -> void {
        d->procList = procList;
    }

    auto PluginAppInfo::SetPath(const QString& path) const -> void {
        d->path = path;
    }
    auto PluginAppInfo::GetAppType() const -> std::tuple<bool, bool> {
        return d->appType;
    }
    auto PluginAppInfo::SetAppType(std::tuple<bool, bool> appType) const -> void {
        d->appType = appType;
    }

    auto PluginAppInfo::GetProcessors() const -> ProcessorInfo::List {        
        return d->procList;
    }

    auto PluginAppInfo::GetPath() const -> QString {
        return d->path;
    }

    auto PluginAppInfo::SetPosition(const QPoint& pos) -> void {
        d->pos = pos;
    }

    auto PluginAppInfo::GetPosition() const -> QPoint {
        return d->pos;
    }
}