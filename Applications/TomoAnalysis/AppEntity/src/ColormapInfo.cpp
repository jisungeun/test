#include "MetaInfoDefines.h"
#include "ColormapInfo.h"

namespace TomoAnalysis::AppEntity {    
    /**
     * \brief Colormap meta information
     */
    struct ColormapInfo::Impl {
        double xRange[2]{0,0};
        double yRange[2]{0,0};
        double rgb[3]{0,0,0};
        double opacity{0};
        double gamma{1};
        bool isGamma{false};
        bool isVisible{false};
        int predefinedColormap{0};
        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other)const->bool;
    };
    auto ColormapInfo::Impl::operator=(const Impl& other) -> Impl& {
        for (auto i = 0; i < 2; i++) {
            xRange[i] = other.xRange[i];
            yRange[i] = other.yRange[i];
        }
        for (auto i = 0; i < 3; i++) {
            rgb[i] = other.rgb[i];
        }
        opacity = other.opacity;
        gamma = other.gamma;
        isVisible = other.isVisible;
        isGamma = other.isGamma;
        predefinedColormap = other.predefinedColormap;
        return *this;
    }
    auto ColormapInfo::Impl::operator==(const Impl& other) const -> bool {
        for (auto i = 0; i < 2; i++) {
            if (false == AreSame(xRange[i], other.xRange[i])) {
                return false;
            }
            if (false == AreSame(yRange[i], other.yRange[i])) {
                return false;
            }
        }
        for (auto i = 0; i < 3; i++) {
            if (false == AreSame(rgb[i], other.rgb[i])) {
                return false;
            }
        }
        if (false == AreSame(opacity, other.opacity)) {
            return false;
        }
        if (false == AreSame(gamma, other.gamma)) {
            return false;
        }
        if (isVisible != other.isVisible) {
            return false;
        }
        if (isGamma != other.isGamma) {
            return false;
        }
        if (predefinedColormap != other.predefinedColormap) {
            return false;
        }
        return true;
    }
    auto ColormapInfo::GetPredefinedColormap() const -> int {
        return d->predefinedColormap;
    }
    auto ColormapInfo::SetPredefinedColormap(int colormap_idx) const -> void {
        d->predefinedColormap = colormap_idx;
    }
    auto ColormapInfo::GetXRange()const -> std::tuple<double, double> {
        return std::make_tuple(d->xRange[0], d->xRange[1]);
    }
    auto ColormapInfo::SetXRange(double xmin, double xmax)const -> void {
        d->xRange[0] = xmin;
        d->xRange[1] = xmax;
    }
    auto ColormapInfo::GetYRange()const -> std::tuple<double, double> {
        return std::make_tuple(d->yRange[0], d->yRange[1]);
    }
    auto ColormapInfo::SetYRange(double ymin, double ymax) const -> void {
        d->yRange[0] = ymin;
        d->yRange[1] = ymax;
    }
    auto ColormapInfo::GetColor()const -> std::tuple<double, double, double> {
        return std::make_tuple(d->rgb[0], d->rgb[1], d->rgb[2]);
    }
    auto ColormapInfo::SetRgb(double r, double g, double b) const -> void {
        d->rgb[0] = r;
        d->rgb[1] = g;
        d->rgb[2] = b;
    }
    auto ColormapInfo::GetOpacity()const -> double {
        return d->opacity;
    }
    auto ColormapInfo::SetOpacity(double opacity) const -> void {
        d->opacity = opacity;
    }
    auto ColormapInfo::GetGamma()const -> double {
        return d->gamma;
    }
    auto ColormapInfo::SetGamma(double gamma) const -> void {
        d->gamma = gamma;
    }
    auto ColormapInfo::isVisible()const -> bool {
        return d->isVisible;
    }
    auto ColormapInfo::SetIsVisible(bool isVisible) const -> void {
        d->isVisible = isVisible;
    }
    auto ColormapInfo::isGamma()const -> bool {
        return d->isGamma;
    }
    auto ColormapInfo::SetIsGamma(bool isGamma) const -> void {
        d->isGamma = isGamma;
    }
    ColormapInfo::ColormapInfo() : d{ new Impl } {

    }
    ColormapInfo::ColormapInfo(const ColormapInfo& other) : d{ new Impl } {
        *this = other;
    }
    ColormapInfo::~ColormapInfo() {

    }
    auto ColormapInfo::operator=(const ColormapInfo& other) -> ColormapInfo& {
        *d = *other.d;
        return *this;
    }
    auto ColormapInfo::operator==(const ColormapInfo& other) const -> bool {
        return *d == *other.d;
    }
}