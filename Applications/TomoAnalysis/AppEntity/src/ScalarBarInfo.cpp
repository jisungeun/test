#include "ScalarBarInfo.h"

namespace TomoAnalysis::AppEntity {    
    /**
     * \brief ScalarBar meta information
     */
    struct ScalarBarInfo::Impl {
        double x;
        double y;
        Orientation orientation;
        bool isVisible;
        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other)const->bool;
    };
    auto ScalarBarInfo::Impl::operator=(const Impl& other) -> Impl& {
        x = other.x;
        y = other.y;
        orientation = other.orientation;
        isVisible = other.isVisible;
        return *this;
    }
    auto ScalarBarInfo::Impl::operator==(const Impl& other) const -> bool {
        if (false == AreSame(x, other.x)) {
            return false;
        }
        if (false == AreSame(y, other.y)) {
            return false;
        }
        if (orientation != other.orientation) {
            return false;
        }
        if (isVisible != other.isVisible) {
            return false;
        }
        return true;
    }
    ScalarBarInfo::ScalarBarInfo() : d{ new Impl } {

    }
    ScalarBarInfo::ScalarBarInfo(const ScalarBarInfo& other) : d{ new Impl } {
        *this = other;
    }
    ScalarBarInfo::~ScalarBarInfo() {

    }
    auto ScalarBarInfo::GetCoordinate() const -> std::tuple<double, double> {
        return std::make_tuple(d->x, d->y);
    }
    auto ScalarBarInfo::GetOrientation() const -> Orientation {
        return d->orientation;
    }
    auto ScalarBarInfo::isVisible() const -> bool {
        return d->isVisible;
    }
    auto ScalarBarInfo::SetCoordinate(double x, double y) const -> void {
        d->x = x;
        d->y = y;
    }
    auto ScalarBarInfo::SetOrientation(Orientation orientation) const -> void {
        d->orientation = orientation;
    }
    auto ScalarBarInfo::SetIsVisible(bool visible) const -> void {
        d->isVisible = visible;
    }

    auto ScalarBarInfo::operator=(const ScalarBarInfo& other) -> ScalarBarInfo& {
        *d = *other.d;
        return *this;
    }
    auto ScalarBarInfo::operator==(const ScalarBarInfo& other) const -> bool {
        return *d == *other.d;
    }
}