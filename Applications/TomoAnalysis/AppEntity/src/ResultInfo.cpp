#include <QMap>

#include "ResultInfo.h"

namespace TomoAnalysis {
    struct ResultInfo::Impl {
        QString path;
        MaskInfo::List maskInfoList;
        PluginAppInfo::List reportAppInfoList;
    };

    ResultInfo::ResultInfo() : d{ new Impl } {
        
    }

    ResultInfo::ResultInfo(const ResultInfo& other) : d{ new Impl } {
        Q_UNUSED(other)
    }

    ResultInfo::~ResultInfo() {
        
    }

    auto ResultInfo::Init() -> void {
        
    }

    auto ResultInfo::GetPath() const ->QString {
        return d->path;
    }

    auto ResultInfo::SetPath(const QString& path) const ->void {
        d->path = path;
    }

    auto ResultInfo::GetMaskInfoList() const ->MaskInfo::List {
        return d->maskInfoList;
    }

    auto ResultInfo::SetMaskInfoList(const MaskInfo::List& list) const ->void {
        d->maskInfoList = list;
    }

    auto ResultInfo::GetReportAppInfoList() const ->PluginAppInfo::List {
        return d->reportAppInfoList;
    }

    auto ResultInfo::SetReportAppInfoList(const PluginAppInfo::List& list) const ->void {
        d->reportAppInfoList = list;
    }

}