#include "MetaInfoDefines.h"
#include "Camera3DInfo.h"

namespace TomoAnalysis::AppEntity {    
    /**
     * \brief Perspective camera meta information
     */
    struct Camera3DInfo::Impl {
        double x;
        double y;
        double z;
        double axis[3];
        double radian;
        double heightAngle;
        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other)const->bool;
    };
    auto Camera3DInfo::Impl::operator=(const Impl& other) -> Impl& {
        x = other.x;
        y = other.y;
        z = other.z;
        for (auto i = 0; i < 3; i++) {
            axis[i] = other.axis[i];
        }
        radian = other.radian;
        heightAngle = other.heightAngle;
        return *this;
    }
    auto Camera3DInfo::Impl::operator==(const Impl& other) const -> bool {
        if (false == AreSame(x, other.x)) {
            return false;
        }
        if (false == AreSame(y, other.y)) {
            return false;
        }
        if (false == AreSame(z, other.z)) {
            return false;
        }
        if (false == AreSame(radian, other.radian)) {
            return false;
        }
        if (false == AreSame(heightAngle, other.heightAngle)) {
            return false;
        }
        for (auto i = 0; i < 3; i++) {
            if (false == AreSame(axis[i], other.axis[i])) {
                return false;
            }
        }
        return true;
    }
    Camera3DInfo::Camera3DInfo() :d{ new Impl } {

    }
    Camera3DInfo::Camera3DInfo(const Camera3DInfo& other) : d{ new Impl } {
        *this = other;
    }
    Camera3DInfo::~Camera3DInfo() {

    }
    auto Camera3DInfo::SetPosition(double x, double y, double z) const -> void {
        d->x = x;
        d->y = y;
        d->z = z;
    }
    auto Camera3DInfo::GetPosition() const -> std::tuple<double, double, double> {
        return std::make_tuple(d->x, d->y, d->z);
    }
    auto Camera3DInfo::SetHeightAngle(double heightAngle) const -> void {
        d->heightAngle = heightAngle;
    }
    auto Camera3DInfo::GetHeightAngle() const -> double {
        return d->heightAngle;
    }
    auto Camera3DInfo::SetDirection(double axisX, double axisY, double axisZ, double radian) const -> void {
        d->axis[0] = axisX;
        d->axis[1] = axisY;
        d->axis[2] = axisZ;
        d->radian = radian;
    }
    auto Camera3DInfo::GetDirection() const -> std::tuple<double, double, double, double> {
        return std::make_tuple(d->axis[0], d->axis[1], d->axis[2], d->radian);
    }
    auto Camera3DInfo::operator=(const Camera3DInfo& other) -> Camera3DInfo& {
        *d = *other.d;
        return *this;
    }
    auto Camera3DInfo::operator==(const Camera3DInfo& other) const -> bool {
        return *d == *other.d;
    }
}