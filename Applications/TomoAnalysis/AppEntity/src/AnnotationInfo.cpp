#include <tuple>

#include "AnnotationInfo.h"

namespace TomoAnalysis::AppEntity {            
    struct AnnotationInfo::Impl {
        bool hasScalarMeta{false};
        ScalarBarInfo scalarMeta;
        bool hasScaleMeta{false};
        ScaleBarInfo scaleMeta;
        bool hasTimeMeta{false};
        TimeStampInfo timeMeta;
        bool hasDeviceMeta{false};
        DeviceInfo deviceMeta;        
        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other)const->bool;
    };
    auto AnnotationInfo::Impl::operator=(const Impl& other) -> Impl& {
        if (other.hasScalarMeta) {
            scalarMeta = other.scalarMeta;
        }
        if (other.hasScaleMeta) {
            scaleMeta = other.scaleMeta;
        }
        if (other.hasTimeMeta) {
            timeMeta = other.timeMeta;
        }
        if (other.hasDeviceMeta) {
            deviceMeta = other.deviceMeta;
        }        
        return *this;
    }
    auto AnnotationInfo::Impl::operator==(const Impl& other) const -> bool {
        if(false == (hasScalarMeta && other.hasScalarMeta)) {
            return false;
        }
        if (false == (scalarMeta == other.scalarMeta)) {
            return false;
        }
        if(false == (hasScaleMeta && other.hasScaleMeta)) {
            return false;
        }
        if(false == (scaleMeta == other.scaleMeta)) {
            return false;
        }
        if(false == (hasTimeMeta && other.hasTimeMeta)) {
            return false;
        }
        if(false == (timeMeta == other.timeMeta)) {
            return false;
        }
        if(false == (hasDeviceMeta && other.hasDeviceMeta)) {
            return false;
        }
        if(false == (deviceMeta == other.deviceMeta)) {
            return false;
        }        
        return true;
    }
    AnnotationInfo::AnnotationInfo() : d{ new Impl } {

    }
    AnnotationInfo::AnnotationInfo(const AnnotationInfo& other) : d{ new Impl } {
        *this = other;
    }
    AnnotationInfo::~AnnotationInfo() {

    }
    auto AnnotationInfo::SetScalarMetaInfo(const ScalarBarInfo& meta) const -> void {
        d->scalarMeta = meta;
        d->hasScalarMeta = true;
    }
    auto AnnotationInfo::hasScalarMetaInfo() const -> bool {
        return d->hasScalarMeta;
    }
    auto AnnotationInfo::GetScalarMetaInfo() const -> ScalarBarInfo {
        return d->scalarMeta;
    }
    auto AnnotationInfo::SetScaleMetaInfo(const ScaleBarInfo& meta) const -> void {
        d->scaleMeta = meta;
        d->hasScaleMeta = true;
    }
    auto AnnotationInfo::hasScaleMetaInfo() const -> bool {
        return d->hasScaleMeta;
    }
    auto AnnotationInfo::GetScaleMetaInfo() const -> ScaleBarInfo {
        return d->scaleMeta;
    }
    auto AnnotationInfo::hasTimeMetaInfo() const -> bool {
        return d->hasTimeMeta;
    }
    auto AnnotationInfo::GetTimeMetaInfo() const -> TimeStampInfo {
        return d->timeMeta;
    }
    auto AnnotationInfo::hasDeviceMetaInfo() const -> bool {
        return d->hasDeviceMeta;
    }
    auto AnnotationInfo::GetDeviceMetaInfo() const -> DeviceInfo {
        return d->deviceMeta;
    }    
    auto AnnotationInfo::SetTimeMetaInfo(const TimeStampInfo& meta) const -> void {
        d->timeMeta = meta;        
        d->hasTimeMeta = true;
    }
    auto AnnotationInfo::SetDeviceMetaInfo(const DeviceInfo& meta) const -> void {
        d->deviceMeta = meta;
        d->hasDeviceMeta = true;
    }
    
    auto AnnotationInfo::operator=(const AnnotationInfo& other) -> AnnotationInfo& {
        *d = *other.d;
        return *this;
    }
    auto AnnotationInfo::operator==(const AnnotationInfo& other) const -> bool {
        return *d == *other.d;
    }
}
