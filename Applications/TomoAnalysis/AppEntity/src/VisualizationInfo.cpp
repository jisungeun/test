#include <QList>

#include "VisualizationInfo.h"

namespace TomoAnalysis::AppEntity {
    struct VisualizationInfo::Impl {
        bool riInfoExist{ false };
        bool flInfoExist[3]{ false,false,false };
        ColormapInfo riInfo;
        
        QList<ColormapInfo> riTF2dList;
        ColormapInfo flInfo[3];

        bool hasCamera2dMeta{ false };
        Camera2DInfo camera2dMeta;
        bool hasCamera3dMeta{ false };
        Camera3DInfo camera3dMeta;
        
        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other)const->bool;
    };
    auto VisualizationInfo::Impl::operator=(const Impl& other) -> Impl& {
        riInfoExist = other.riInfoExist;
        for(auto i=0;i<3;i++) {
            flInfoExist[i] = other.flInfoExist[i];
        }
        if(riTF2dList.count() == other.riTF2dList.count()) {
            for(auto i=0;i<riTF2dList.count();i++) {
                riTF2dList[i] = other.riTF2dList[i];
            }
        }
        if (other.hasCamera2dMeta) {
            camera2dMeta = other.camera2dMeta;
        }
        if (other.hasCamera3dMeta) {
            camera3dMeta = other.camera3dMeta;
        }
        return *this;
    }
    auto VisualizationInfo::Impl::operator==(const Impl& other) const -> bool {
        if(riInfoExist != other.riInfoExist) {
            return false;
        }
        for(auto i=0;i<3;i++) {
            if(flInfoExist[i] != other.flInfoExist[i]) {
                return false;
            }
        }
        if(riTF2dList.count() != other.riTF2dList.count()) {
            return false;
        }
        for(auto i=0;i<riTF2dList.count();i++) {
            if(false == (riTF2dList[i] == other.riTF2dList[i])) {
                return false;
            }
        }
        if (false == (hasCamera2dMeta && other.hasCamera2dMeta)) {
            return false;
        }
        if (false == (camera2dMeta == other.camera2dMeta)) {
            return false;
        }
        if (false == (hasCamera3dMeta && other.hasCamera3dMeta)) {
            return false;
        }
        if (false == (camera3dMeta == other.camera3dMeta)) {
            return false;
        }
        return true;
    }
    VisualizationInfo::VisualizationInfo() : d{ new Impl } {
        
    }
    VisualizationInfo::VisualizationInfo(const VisualizationInfo& other) : d{ new Impl } {
        *this = other;
    }
    VisualizationInfo::~VisualizationInfo() {
        
    }
    auto VisualizationInfo::operator=(const VisualizationInfo& other) -> VisualizationInfo& {
        *d = *other.d;
        return *this;
    }
    auto VisualizationInfo::operator==(const VisualizationInfo& other) const -> bool {
        return *d == *other.d;
    }
    auto VisualizationInfo::GetIsFLInfoExist(int ch) const -> bool {
        return d->flInfoExist[ch];
    }
    auto VisualizationInfo::GetIsRiInfoExist() const -> bool {
        return d->riInfoExist;
    }
    auto VisualizationInfo::GetTF2dList() const -> QList<ColormapInfo> {
        return d->riTF2dList;
    }
    auto VisualizationInfo::SetIsRiInfoExist(bool exist) const -> void {
        d->riInfoExist = exist;
    }
    auto VisualizationInfo::SetIsFLInfoExist(int ch, bool exist) const -> void {
        d->flInfoExist[ch] = exist;
    }
    auto VisualizationInfo::SetTF2dList(const QList<ColormapInfo>& boxList) const -> void {
        d->riTF2dList = boxList;
    }
    auto VisualizationInfo::AppendTF(const ColormapInfo& colormap) const -> void {        
        d->riTF2dList.append(colormap);
    }
    auto VisualizationInfo::SetFLInfo(int ch, const ColormapInfo& flBox) -> void {
        d->flInfoExist[ch] = true;
        d->flInfo[ch] = flBox;
    }
    auto VisualizationInfo::GetFLInfo(int ch) const -> ColormapInfo {
        return d->flInfo[ch];
    }
    auto VisualizationInfo::SetHTInfo(const ColormapInfo& htBox) -> void {
        d->riInfoExist = true;
        d->riInfo = htBox;
    }
    auto VisualizationInfo::GetHTInfo() const -> ColormapInfo {
        return d->riInfo;
    }
    auto VisualizationInfo::hasCamera2dMetaInfo() const -> bool {
        return d->hasCamera2dMeta;
    }
    auto VisualizationInfo::GetCamera2dMetaInfo() const -> Camera2DInfo {
        return d->camera2dMeta;
    }
    auto VisualizationInfo::hasCamera3dMetaInfo() const -> bool {
        return d->hasCamera3dMeta;
    }
    auto VisualizationInfo::GetCamera3dMetaInfo() const -> Camera3DInfo {
        return d->camera3dMeta;
    }
    auto VisualizationInfo::SetCamera2dMetaInfo(const Camera2DInfo& meta) const -> void {
        d->camera2dMeta = meta;
        d->hasCamera2dMeta = true;
    }
    auto VisualizationInfo::SetCamera3dMetaInfo(const Camera3DInfo& meta) const -> void {
        d->camera3dMeta = meta;
        d->hasCamera3dMeta = true;
    }
}