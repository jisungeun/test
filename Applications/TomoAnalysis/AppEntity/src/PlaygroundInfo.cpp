#include <iostream>

#include "PlaygroundInfo.h"

namespace TomoAnalysis {
    struct PlaygroundInfo::Impl {
        QString name;
        QString path;
        HyperCube::List hyperCubeList;
        Cube::List cubeList;
        TCFDir::List tcfDirList;
        PluginAppInfo::List appList;
    };

    PlaygroundInfo::PlaygroundInfo() : d{ new Impl } {
        
    }

    PlaygroundInfo::PlaygroundInfo(const PlaygroundInfo& other) : d{ new Impl } {
        *d = *other.d;
    }

    PlaygroundInfo::~PlaygroundInfo() {
        
    }

    auto PlaygroundInfo::Init() -> void {
        
    }

    auto PlaygroundInfo::GetName() const->QString {
        return d->name;
    }

    auto PlaygroundInfo::SetName(const QString& name) const ->void {
        d->name = name;
    }

    auto PlaygroundInfo::GetPath() const ->QString {
        return d->path;
    }

    auto PlaygroundInfo::SetPath(const QString& path) const ->void {
        d->path = path;
    }

    auto PlaygroundInfo::GetHyperCubeList() const ->HyperCube::List {
        return d->hyperCubeList;
    }

    auto PlaygroundInfo::SetHyperCubeList(const HyperCube::List& list) const ->void {
        d->hyperCubeList = list;
    }

    auto PlaygroundInfo::AddHyperCube(const HyperCube::Pointer& hypercube) const ->void {
        auto newHyper = true;
        for(auto h : d->hyperCubeList) {
            if(h->GetName().compare(hypercube->GetName())==0) {
                newHyper = false;
                break;
            }
        }
        if(newHyper)
            d->hyperCubeList.append(hypercube);
    }

    auto PlaygroundInfo::RemoveHyperCube(const QString& name) const -> void {
        for(auto i =0; i< d->hyperCubeList.size();i++) {
            if(d->hyperCubeList[i]->GetName()==name) {
                d->hyperCubeList.removeAt(i);
                return;
            }
        }
    }


    auto PlaygroundInfo::GetCubeList() const ->Cube::List {
        return d->cubeList;
    }

    auto PlaygroundInfo::SetCubeList(const Cube::List& list) const ->void {
        d->cubeList = list;
    }

    auto PlaygroundInfo::AddCube(const Cube::Pointer& cube) const ->void {
        d->cubeList.append(cube);
    }

    auto PlaygroundInfo::RemoveCube(const QString& name) const -> void {
        for (auto i = 0; i < d->cubeList.size();i++) {
            if(d->cubeList[i]->GetName().compare(name)==0) {
                d->cubeList.removeAt(i);
                return;
            }
        }
    }

    auto PlaygroundInfo::FindCube(const QString& name) const ->Cube::Pointer {
        for(const auto& cube : d->cubeList) {
            if(nullptr == cube) {
                continue;
            }

            if(cube->GetName() == name) {
                return cube;
            }
        }
        for(const auto&hyper : d->hyperCubeList) {
            if(nullptr == hyper) {
                continue;
            }
            for(const auto& cube : hyper->GetCubeList()) {
                if(nullptr == hyper) {
                    continue;
                }
                if(cube->GetName()==name) {
                    return cube;
                }
            }
        }


        return nullptr;
    }

    auto PlaygroundInfo::FindHypercube(const QString& name) const ->HyperCube::Pointer {
        for (const auto& hypercube : d->hyperCubeList) {
            if (nullptr == hypercube) {
                continue;
            }

            if (hypercube->GetName() == name) {
                return hypercube;
            }
        }

        return nullptr;
    }

    auto PlaygroundInfo::GetTCFDirList(void) const ->TCFDir::List {
        return d->tcfDirList;
    }

    auto PlaygroundInfo::SetTCFDirList(const TCFDir::List& tcfDirList) const ->void {
        d->tcfDirList = tcfDirList;
    }

    auto PlaygroundInfo::AddTCFDir(const TCFDir::Pointer& tcfDir) const ->void {
        d->tcfDirList.append(tcfDir);
    }

    auto PlaygroundInfo::RemoveTCFDir(const QString& dirPath) const -> void {
        for(auto i=0;i<d->tcfDirList.size();i++) {            
            if(d->tcfDirList[i]->GetPath().compare(dirPath)==0) {
                d->tcfDirList.removeAt(i);                
                return;
            }
        }
    }

    auto PlaygroundInfo::FindTCFDir(const QString& dirPath) -> TCFDir::Pointer {
        for(const auto& dir : d->tcfDirList) {
            if (dir == nullptr) {
                continue;
            }
            if(dir->GetPath()==dirPath) {
                return dir;
            }
        }
        return nullptr;
    }        

    auto PlaygroundInfo::GetAppList() const -> PluginAppInfo::List {
        return d->appList;
    }

    auto PlaygroundInfo::SetAppList(const PluginAppInfo::List& appList) const -> void {
        d->appList = appList;
    }

    auto PlaygroundInfo::AddApp(const PluginAppInfo::Pointer& app) const -> void {
        d->appList.append(app);
    }

    auto PlaygroundInfo::RemoveApp(const QString& appPath) const -> void {                
        for(auto i=0;i<d->appList.size();i++) {                        
            if(d->appList[i]->GetPath().compare(appPath)==0) {
                d->appList.removeAt(i);
                return;
            }
        }
    }

    
    auto PlaygroundInfo::FindApp(const QString& appPath) const -> PluginAppInfo::Pointer {        
        for(const auto & hyper : d->hyperCubeList) {
            auto app = hyper->FindApp(appPath);
            if(app != nullptr) {
                return app;
            }   
        }
        for(const auto & app : d->appList) {
            if(app == nullptr) {
                continue;;
            }                        

            if(app->GetPath() == appPath) {                
                return app;
            }
        }
        return nullptr;
    }
}
