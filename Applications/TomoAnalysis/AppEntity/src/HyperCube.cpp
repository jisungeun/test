#include <QMap>

#include "HyperCube.h"

namespace TomoAnalysis {
    struct HyperCube::Impl {
        QString name;
        QPoint pos;
        Cube::List cubeList;
        PluginAppInfo::List measurementAppInfoList;
        ResultInfo::List resultInfoList;
    };

    HyperCube::HyperCube() : d{ new Impl } {
        d->pos.setX(100);
        d->pos.setY(100);
    }

    HyperCube::HyperCube(const HyperCube::Pointer other) : d{ new Impl } {
        *d = *other->d;
    }

    HyperCube::~HyperCube() {
        
    }

    auto HyperCube::Init() -> void {
        
    }

    auto HyperCube::GetName() const->QString {
        return d->name;
    }

    auto HyperCube::SetName(const QString& name) const ->void {
        d->name = name;
    }

    auto HyperCube::GetCubeList() const->Cube::List {
        return d->cubeList;
    }

    auto HyperCube::SetCubeList(const Cube::List& list) const ->void {
        d->cubeList = list;
    }

    auto HyperCube::AddCube(const Cube::Pointer& cube) const -> void {
        d->cubeList.append(cube);
    }

    auto HyperCube::RemoveCube(const QString& name) const -> void {
        for(auto i=0;i<d->cubeList.size();i++) {
            if(d->cubeList[i]->GetName().compare(name)==0) {
                d->cubeList.removeAt(i);
                return;
            }
        }
    }

    auto HyperCube::FindCube(const QString& name) const -> Cube::Pointer {
        for (const auto& cube : d->cubeList) {
            if (nullptr == cube) {
                continue;
            }

            if (cube->GetName() == name) {
                return cube;
            }
        }
        return nullptr;
    }

    auto HyperCube::SetPosition(const QPoint& pos) const -> void {
        d->pos = pos;
    }

    auto HyperCube::GetPosition() const -> QPoint {
        return d->pos;
    }

    auto HyperCube::GetMeasurementAppInfoList() const->PluginAppInfo::List {
        return d->measurementAppInfoList;
    }

    auto HyperCube::SetMeasurementAppInfoList(const PluginAppInfo::List& list) const ->void {
        d->measurementAppInfoList = list;
    }

    auto HyperCube::GetResultInfoList() const->ResultInfo::List {
        return d->resultInfoList;
    }

    auto HyperCube::SetResultInfoList(const ResultInfo::List& list) const ->void {
        d->resultInfoList = list;
    }
    auto HyperCube::AddApp(const PluginAppInfo::Pointer& app) const -> void {
        d->measurementAppInfoList.clear();
        d->measurementAppInfoList.append(app);
    }
    auto HyperCube::FindApp(const QString& name) const -> PluginAppInfo::Pointer {
        for(const auto& app:d->measurementAppInfoList) {
            if(app == nullptr) {
                continue;
            }
            if(app->GetName()==name || app->GetPath()==name) {
                return app;
            }
        }
        return nullptr;
    }
    auto HyperCube::RemoveApp(const QString& name) const -> void {
        for(auto i=0;i<d->measurementAppInfoList.size();i++) {
            if(d->measurementAppInfoList[i]->GetName()==name||
                d->measurementAppInfoList[i]->GetPath()==name) {
                d->measurementAppInfoList.removeAt(i);
                return;
            }
        }
    }
}