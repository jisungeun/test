#include "MaskInfo.h"

namespace TomoAnalysis {
    struct MaskInfo::Impl {
        QString referenceTCFPath;
        QString maskTCFPath;
        QString interpretation;
    };

    MaskInfo::MaskInfo() : d{ new Impl } {
        
    }

    MaskInfo::MaskInfo(const MaskInfo& other) : d{ new Impl } {
        *d = *other.d;
    }

    MaskInfo::~MaskInfo() {
        
    }

    auto MaskInfo::Init() -> void {
        
    }

    auto MaskInfo::GetReferenceTCFPath()->QString {
        return d->referenceTCFPath;
    }

    auto MaskInfo::SetReferenceTCFPath(const QString& path)->void {
        d->referenceTCFPath = path;
    }

    auto MaskInfo::GetMaskTCFPath()->QString {
        return  d->maskTCFPath;
    }

    auto MaskInfo::SetMaskTCFPath(const QString& path)->void {
        d->maskTCFPath = path;
    }

    auto MaskInfo::GetInterpretation()->QString {
        return d->interpretation;
    }

    auto MaskInfo::SetInterpretation(const QString& interpretation)->void {
        d->interpretation = interpretation;
    }

}