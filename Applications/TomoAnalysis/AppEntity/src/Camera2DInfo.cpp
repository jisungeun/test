#include "MetaInfoDefines.h"
#include "Camera2DInfo.h"

namespace TomoAnalysis::AppEntity {
    /**
     * \brief Orthographic camera meta information
     */
    struct Camera2DInfo::Impl {
        double x;
        double y;
        double z;
        double height;
        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other)const->bool;
    };
    auto Camera2DInfo::Impl::operator=(const Impl& other) -> Impl& {
        x = other.x;
        y = other.y;
        z = other.z;
        height = other.height;
        return *this;
    }
    auto Camera2DInfo::Impl::operator==(const Impl& other) const -> bool {
        if (false == AreSame(x, other.x)) {
            return false;
        }
        if (false == AreSame(y, other.y)) {
            return false;
        }
        if (false == AreSame(z, other.z)) {
            return false;
        }
        if (false == AreSame(height, other.height)) {
            return false;
        }
        return true;
    }
    Camera2DInfo::Camera2DInfo() : d{ new Impl } {

    }
    Camera2DInfo::Camera2DInfo(const Camera2DInfo& other) : d{ new Impl } {
        *this = other;
    }
    Camera2DInfo::~Camera2DInfo() {

    }
    auto Camera2DInfo::SetPosition(double x, double y, double z) const -> void {
        d->x = x;
        d->y = y;
        d->z = z;
    }
    auto Camera2DInfo::GetPosition() const -> std::tuple<double, double, double> {
        return std::make_tuple(d->x, d->y, d->z);
    }
    auto Camera2DInfo::SetHeight(double height) const -> void {
        d->height = height;
    }
    auto Camera2DInfo::GetHeight() const -> double {
        return d->height;
    }
    auto Camera2DInfo::operator=(const Camera2DInfo& other) -> Camera2DInfo& {
        *d = *other.d;
        return *this;
    }
    auto Camera2DInfo::operator==(const Camera2DInfo& other) const -> bool {
        return *d == *other.d;
    }
}