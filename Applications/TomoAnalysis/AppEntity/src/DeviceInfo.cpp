#include "MetaInfoDefines.h"
#include "DeviceInfo.h"

namespace TomoAnalysis::AppEntity {    
    /**
     * \brief Device Annotation meta information
     */

    struct DeviceInfo::Impl {
        double rgb[3];
        double fontSize;
        bool isVisible;
        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other)const->bool;
    };
    auto DeviceInfo::Impl::operator=(const Impl& other) -> Impl& {
        for (auto i = 0; i < 3; i++) {
            rgb[i] = other.rgb[i];
        }
        fontSize = other.fontSize;
        isVisible = other.isVisible;
        return *this;
    }
    auto DeviceInfo::Impl::operator==(const Impl& other) const -> bool {
        for (auto i = 0; i < 3; i++) {
            if (false == AreSame(rgb[i], other.rgb[i])) {
                return false;
            }
        }
        if (false == AreSame(fontSize, other.fontSize)) {
            return false;
        }
        if (isVisible != other.isVisible) {
            return false;
        }
        return true;
    }
    DeviceInfo::DeviceInfo() : d{ new Impl } {

    }
    DeviceInfo::DeviceInfo(const DeviceInfo& other) : d{ new Impl } {
        *this = other;
    }
    DeviceInfo::~DeviceInfo() {

    }
    auto DeviceInfo::SetColor(double r, double g, double b) const -> void {
        d->rgb[0] = r;
        d->rgb[1] = g;
        d->rgb[2] = b;
    }
    auto DeviceInfo::GetColor() const -> std::tuple<double, double, double> {
        return std::make_tuple(d->rgb[0], d->rgb[1], d->rgb[2]);
    }
    auto DeviceInfo::SetFontSize(double size) const -> void {
        d->fontSize = size;
    }
    auto DeviceInfo::GetFontSize() const -> double {
        return d->fontSize;
    }
    auto DeviceInfo::SetIsVisible(bool isVisible) const -> void {
        d->isVisible = isVisible;
    }
    auto DeviceInfo::isVisible() const -> bool {
        return d->isVisible;
    }
    auto DeviceInfo::operator=(const DeviceInfo& other) -> DeviceInfo& {
        *d = *other.d;
        return *this;
    }
    auto DeviceInfo::operator==(const DeviceInfo& other) const -> bool {
        return *d == *other.d;
    }
}