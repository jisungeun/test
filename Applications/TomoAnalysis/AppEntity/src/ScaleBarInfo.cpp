#include "ScaleBarInfo.h"

namespace TomoAnalysis::AppEntity {    
    /**
     * \brief ScaleBar meta information
     */

    struct ScaleBarInfo::Impl {
        double x;
        double y;
        double rgb[3];
        Orientation orientation;
        double length;
        double tick;
        bool isTextVisible;
        bool isVisible;
        auto operator=(const Impl& other)->Impl&;
        auto operator==(const Impl& other)const->bool;
    };
    auto ScaleBarInfo::Impl::operator=(const Impl& other) -> Impl& {
        x = other.x;
        y = other.y;
        for (auto i = 0; i < 3; i++) {
            rgb[i] = other.rgb[i];
        }
        orientation = other.orientation;
        length = other.length;
        tick = other.tick;
        isTextVisible = other.isTextVisible;
        isVisible = other.isVisible;
        return *this;
    }
    auto ScaleBarInfo::Impl::operator==(const Impl& other) const -> bool {
        if (false == AreSame(x, other.x)) {
            return false;
        }
        if (false == AreSame(y, other.y)) {
            return false;
        }
        for (auto i = 0; i < 3; i++) {
            if (false == AreSame(rgb[i], other.rgb[i])) {
                return false;
            }
        }
        if (orientation != other.orientation) {
            return false;
        }
        if (false == AreSame(length, other.length)) {
            return false;
        }
        if (false == AreSame(tick, other.tick)) {
            return false;
        }
        if (isTextVisible != other.isTextVisible) {
            return false;
        }
        if (isVisible != other.isVisible) {
            return false;
        }
        return true;
    }
    ScaleBarInfo::ScaleBarInfo() : d{ new Impl } {

    }
    ScaleBarInfo::ScaleBarInfo(const ScaleBarInfo& other) : d{ new Impl } {
        *this = other;
    }
    ScaleBarInfo::~ScaleBarInfo() {

    }
    auto ScaleBarInfo::SetCoordinate(double x, double y) const -> void {
        d->x = x;
        d->y = y;
    }
    auto ScaleBarInfo::GetCoordinate() const -> std::tuple<double, double> {
        return std::make_tuple(d->x, d->y);
    }
    auto ScaleBarInfo::SetColor(double r, double g, double b) const -> void {
        d->rgb[0] = r;
        d->rgb[1] = g;
        d->rgb[2] = b;
    }
    auto ScaleBarInfo::GetColor() const -> std::tuple<double, double, double> {
        return std::make_tuple(d->rgb[0], d->rgb[1], d->rgb[2]);
    }
    auto ScaleBarInfo::SetOrientation(Orientation orientation) const -> void {
        d->orientation = orientation;
    }
    auto ScaleBarInfo::GetOrientation() const -> Orientation {
        return d->orientation;
    }
    auto ScaleBarInfo::SetLength(double length) const -> void {
        d->length = length;
    }
    auto ScaleBarInfo::GetLength() const -> double {
        return d->length;
    }
    auto ScaleBarInfo::SetTick(double tick) const -> void {
        d->tick = tick;
    }
    auto ScaleBarInfo::GetTick() const -> double {
        return d->tick;
    }
    auto ScaleBarInfo::SetIsTextVisible(bool isVisible) const -> void {
        d->isTextVisible = isVisible;
    }
    auto ScaleBarInfo::isTextVisible() const -> bool {
        return d->isTextVisible;
    }
    auto ScaleBarInfo::SetIsVisible(bool isVisible) const -> void {
        d->isVisible = isVisible;
    }
    auto ScaleBarInfo::isVisible() const -> bool {
        return d->isVisible;
    }
    auto ScaleBarInfo::operator=(const ScaleBarInfo& other) -> ScaleBarInfo& {
        *d = *other.d;
        return *this;
    }
    auto ScaleBarInfo::operator==(const ScaleBarInfo& other) const -> bool {
        return *d == *other.d;
    }
}