#include <QMap>

#include "ProcessorInfo.h"

namespace TomoAnalysis {
    struct ProcessorInfo::Impl {
        QString name;
        QString path;
        QString parent_plugin_name;
        QString license;
        bool loaded{ false };
        ParameterList parameterList;
    };

    ProcessorInfo::ProcessorInfo() : d{ new Impl } {
        
    }

    ProcessorInfo::ProcessorInfo(const ProcessorInfo& other) : d{ new Impl } {
        *d = *other.d;
    }

    ProcessorInfo::~ProcessorInfo() {
        
    }

    auto ProcessorInfo::Init() -> void {
        
    }

    auto ProcessorInfo::SetLoaded(const bool& load) const -> void {
        d->loaded = load;
    }

    auto ProcessorInfo::GetLoaded() const -> bool {
        return d->loaded;
    }


    auto ProcessorInfo::SetName(const QString& name) const -> void {
        d->name = name;
    }

    auto ProcessorInfo::ScanName() const -> void {
        auto split = d->path.split("Processor_");
        auto size = split.size();
        d->name = split[size - 1];
    }

    auto ProcessorInfo::GetName() const -> QString {
        return d->name;
    }

    auto ProcessorInfo::GetParent() const -> QString {
        return d->parent_plugin_name;
    }

    auto ProcessorInfo::SetParent(const QString& path) const -> void {
        d->parent_plugin_name = path;
    }

    auto ProcessorInfo::GetPath() const -> QString {
        return d->path;
    }

    auto ProcessorInfo::SetPath(const QString& path) const -> void {
        d->path = path;
    }
    
    auto ProcessorInfo::GetLicense() const ->QString {
        return d->license;
    }

    auto ProcessorInfo::SetLicense(const QString& license) const ->void {
        d->license = license;
    }

    auto ProcessorInfo::GetParameterList() const ->ParameterList {
        return d->parameterList;
    }

    auto ProcessorInfo::SetParameterList(const ParameterList& list) const ->void {
        d->parameterList = list;
    }
}