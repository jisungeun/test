#pragma once

#include <memory>
#include <QMap>

#include "TAEntityExport.h"

namespace TomoAnalysis {
    class TAEntity_API TCFDir {
    public:
        typedef TCFDir Self;
        typedef std::shared_ptr<Self> Pointer;
        typedef QList<Pointer> List;

    public:
        TCFDir();
        TCFDir(const TCFDir& other);
        virtual ~TCFDir();

        auto GetPath() const->QString;
        auto SetPath(const QString& path) const ->void;

        auto GetName() const->QString;

        auto GetPosition()const->QPoint;
        auto SetPosition(const QPoint& pos)const->void;

        auto GetDirList()->TCFDir::List;
        auto SetDirList(const TCFDir::List& list) const ->void;

        auto GetFileList()->QStringList;
        auto SetFileList(const QStringList& list) const ->void;

        auto GetValidityList()->QMap<QString, bool>;
        auto SetValidityList(const QMap<QString, bool>& list) const ->void;

        auto ScanDir()->bool;

        auto GetAllFileFullPathList(const TCFDir::Pointer& dir, QStringList& list) const ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
