#pragma once
#include <memory>
#include <tuple>

#include "TAEntityExport.h"

namespace TomoAnalysis::AppEntity {
    class TAEntity_API ColormapInfo {
    public:
        using Pointer = std::shared_ptr<ColormapInfo>;

        ColormapInfo();
        ColormapInfo(const ColormapInfo& other);
        ~ColormapInfo();

        auto operator=(const ColormapInfo& other)->ColormapInfo&;
        auto operator==(const ColormapInfo& other)const->bool;

        [[nodiscard]] auto GetXRange()const->std::tuple<double, double>;
        [[nodiscard]] auto GetYRange()const->std::tuple<double, double>;
        [[nodiscard]] auto GetColor()const->std::tuple<double, double, double>;
        [[nodiscard]] auto GetPredefinedColormap()const->int;
        [[nodiscard]] auto GetOpacity()const->double;
        [[nodiscard]] auto GetGamma()const->double;
        [[nodiscard]] auto isVisible()const->bool;
        [[nodiscard]] auto isGamma()const->bool;

        auto SetXRange(double xmin, double xmax)const->void;
        auto SetYRange(double ymin, double ymax)const->void;
        auto SetRgb(double r, double g, double b)const->void;
        auto SetOpacity(double opacity)const->void;
        auto SetGamma(double gamma)const->void;
        auto SetIsVisible(bool isVisible)const->void;
        auto SetIsGamma(bool isGamma)const->void;
        auto SetPredefinedColormap(int colormap_idx)const->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}