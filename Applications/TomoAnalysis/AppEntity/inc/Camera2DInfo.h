#pragma once
#include <memory>
#include <tuple>

#include "TAEntityExport.h"

namespace TomoAnalysis::AppEntity {
    class TAEntity_API Camera2DInfo {
    public:
        using Pointer = std::shared_ptr<Camera2DInfo>;
        Camera2DInfo();
        Camera2DInfo(const Camera2DInfo& other);
        ~Camera2DInfo();

        auto operator=(const Camera2DInfo& other)->Camera2DInfo&;
        auto operator==(const Camera2DInfo& other)const->bool;

        [[nodiscard]] auto GetPosition()const->std::tuple<double, double, double>;
        [[nodiscard]] auto GetHeight()const->double;

        auto SetPosition(double x, double y, double z)const->void;
        auto SetHeight(double height)const->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}