#pragma once
#include <memory>

namespace TomoAnalysis::AppEntity {
    auto AreSame = [&](double a, double b) {
        return fabs(a - b) < std::numeric_limits<double>::epsilon();
    };
    enum Orientation {
        VERTICAL,
        HORIZONTAL
    };
}