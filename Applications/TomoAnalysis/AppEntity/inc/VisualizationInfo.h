#pragma once
#include <memory>

#include "ColormapInfo.h"
#include "Camera2DInfo.h"
#include "Camera3DInfo.h"
#include "TAEntityExport.h"

namespace TomoAnalysis::AppEntity {    
    class TAEntity_API VisualizationInfo {
    public:
        using Pointer = std::shared_ptr<VisualizationInfo>;

        VisualizationInfo();
        VisualizationInfo(const VisualizationInfo& other);
        ~VisualizationInfo();

        [[nodiscard]] auto GetIsRiInfoExist()const->bool;
        [[nodiscard]] auto GetIsFLInfoExist(int ch)const->bool;
        [[nodiscard]] auto GetTF2dList()const->QList<ColormapInfo>;
        [[nodiscard]] auto GetFLInfo(int ch)const->ColormapInfo;
        [[nodiscard]] auto GetHTInfo()const->ColormapInfo;
        [[nodiscard]] auto hasCamera2dMetaInfo()const->bool;
        [[nodiscard]] auto GetCamera2dMetaInfo()const->Camera2DInfo;
        [[nodiscard]] auto hasCamera3dMetaInfo()const->bool;
        [[nodiscard]] auto GetCamera3dMetaInfo()const->Camera3DInfo;

        auto SetIsRiInfoExist(bool exist)const->void;
        auto SetIsFLInfoExist(int ch, bool exist)const->void;
        auto SetTF2dList(const QList<ColormapInfo>& boxList)const->void;
        auto AppendTF(const ColormapInfo& colormap)const->void;
        auto SetFLInfo(int ch,const ColormapInfo& flBox)->void;
        auto SetHTInfo(const ColormapInfo& htBox)->void;
        auto SetCamera2dMetaInfo(const Camera2DInfo& meta)const->void;
        auto SetCamera3dMetaInfo(const Camera3DInfo& meta)const->void;

        auto operator=(const VisualizationInfo& other)->VisualizationInfo&;
        auto operator==(const VisualizationInfo& other)const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}