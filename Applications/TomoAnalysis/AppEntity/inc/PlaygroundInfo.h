#pragma once

#include <memory>

#include "HyperCube.h"
#include "Cube.h"
#include "PluginAppInfo.h"

#include "TAEntityExport.h"

namespace TomoAnalysis {
    class TAEntity_API PlaygroundInfo {
    public:
        typedef PlaygroundInfo Self;
        typedef std::shared_ptr<Self> Pointer;
        typedef QList<Pointer> List;

    public:
        PlaygroundInfo();
        PlaygroundInfo(const PlaygroundInfo& other);
        virtual ~PlaygroundInfo();

        auto Init()->void;

        auto GetName() const->QString;
        auto SetName(const QString& name) const ->void;

        auto GetPath() const ->QString;
        auto SetPath(const QString& path) const ->void;

        auto GetHyperCubeList() const ->HyperCube::List;
        auto SetHyperCubeList(const HyperCube::List& list) const ->void;
        
        auto GetCubeList() const ->Cube::List;
        auto SetCubeList(const Cube::List& list) const ->void;

        auto AddCube(const Cube::Pointer& cube) const ->void;
        auto RemoveCube(const QString& name) const->void;
        auto FindCube(const QString& name) const ->Cube::Pointer;

        auto AddHyperCube(const HyperCube::Pointer& hyperCube) const ->void;
        auto RemoveHyperCube(const QString& name) const ->void;
        auto FindHypercube(const QString& name) const ->HyperCube::Pointer;

        auto GetTCFDirList(void) const->TCFDir::List;
        auto SetTCFDirList(const TCFDir::List& tcfDirList) const ->void;
        auto AddTCFDir(const TCFDir::Pointer& tcfDir) const ->void;
        auto RemoveTCFDir(const QString& dirPath) const ->void;
        auto FindTCFDir(const QString& dirPath)->TCFDir::Pointer;

        auto GetAppList(void) const->PluginAppInfo::List;
        auto SetAppList(const PluginAppInfo::List& appList) const -> void;
        auto AddApp(const PluginAppInfo::Pointer& app) const -> void;
        auto RemoveApp(const QString& appPath)const-> void;
        auto FindApp(const QString& appPath)const->PluginAppInfo::Pointer;        
        //auto ExistAppDir(const QString& appPath) const ->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}