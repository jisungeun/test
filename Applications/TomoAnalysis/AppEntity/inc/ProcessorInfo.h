#pragma once

#include <memory>
#include <QString>

#include "TAEntityExport.h"
//segmentation method & Quantification method

namespace TomoAnalysis {
    class TAEntity_API ProcessorInfo {
    public:
        typedef ProcessorInfo Self;
        typedef std::shared_ptr<Self> Pointer;
        typedef QList<Pointer> List;

        typedef QMap<QString, QString> ParameterList;

    public:
        ProcessorInfo();
        ProcessorInfo(const ProcessorInfo& other);
        virtual ~ProcessorInfo();

        auto Init()->void;

        auto GetName() const->QString;
        auto SetName(const QString& name) const ->void;
        auto ScanName() const ->void;

        auto GetLoaded()const->bool;
        auto SetLoaded(const bool& load)const->void;

        auto GetPath() const->QString;
        auto SetPath(const QString& path)const ->void;

        auto GetParent() const->QString;
        auto SetParent(const QString& path)const->void;

        auto GetLicense() const ->QString;
        auto SetLicense(const QString& license) const ->void;

        auto GetParameterList() const ->ParameterList;
        auto SetParameterList(const ParameterList& list) const ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}