#pragma once

#include <memory>
#include <QString>
#include <QPoint>

#include "ProcessorInfo.h"

#include "TAEntityExport.h"

namespace TomoAnalysis {
    class TAEntity_API PluginAppInfo {
    public:
        typedef PluginAppInfo Self;
        typedef std::shared_ptr<Self> Pointer;
        typedef QList<Pointer> List;

    public:
        PluginAppInfo();
        PluginAppInfo(const PluginAppInfo& other);
        virtual ~PluginAppInfo();

        auto Init()->void;

        auto GetTabName()const->QString;
        auto SetTabName(const QString& name)->void;

        auto GetName() const->QString;
        auto SetName(const QString& name)const ->void;
        auto ScanName()const->void;

        auto GetPosition()const->QPoint;
        auto SetPosition(const QPoint& pos)->void;

        auto GetPath() const->QString;
        auto SetPath(const QString& path)const ->void;

        auto GetLicense() const->QString;
        auto SetLicense(const QString& license) const-> void;

        auto GetLoaded()const->bool;
        auto SetLoaded(const bool& load) const->void;

        auto GetProcessors()const->ProcessorInfo::List;
        auto SetProcessors(const ProcessorInfo::List procList)const->void;

        auto GetAppType()const->std::tuple<bool,bool>;
        auto SetAppType(std::tuple<bool,bool> appType)const->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}