#pragma once

#include <memory>
#include <QString>

#include "TCFDir.h"
#include "PluginAppInfo.h"
#include "ResultInfo.h"

#include "TAEntityExport.h"

namespace TomoAnalysis {
	class TAEntity_API Cube {
	public:
		typedef Cube Self;
		typedef std::shared_ptr<Self> Pointer;
		typedef QList<Pointer> List;

		typedef int ID;

	public:
		Cube();
		Cube(const Cube::Pointer& other);
		virtual ~Cube();

		auto Init()->void;

		auto GetID() const->ID;
		auto SetID(const ID& id) const ->void;

		auto GetName() const->QString;
		auto SetName(const QString& name) const ->void;

		auto GetPosition() const->QPoint;
		auto SetPosition(const QPoint& pos)const->void;

		auto GetTCFDirList() const->TCFDir::List;
		auto SetTCFDirList(const TCFDir::List& list) const ->void;
		auto AddTCFDir(const TCFDir::Pointer& tcfDir) const ->void;
		auto FindTCFDir(const QString& dirPath)const->TCFDir::Pointer;
		auto RemoveTCFDir(const QString& dirPath)const ->void;

		auto GetMeasurementAppInfoList() const->PluginAppInfo::List;
		auto SetMeasurementAppInfoList(const PluginAppInfo::List& list) const ->void;
		auto AddApp(const PluginAppInfo::Pointer& app)const->void;
		auto FindApp(const QString& appPath)const->PluginAppInfo::Pointer;
		auto RemoveApp(const QString& appPath)const->void;

		auto GetResultInfoList() const->ResultInfo::List;
		auto SetResultInfoList(const ResultInfo::List& list) const ->void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
