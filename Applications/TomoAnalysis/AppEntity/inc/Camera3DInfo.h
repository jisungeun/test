#pragma once
#include <memory>
#include <tuple>

#include "TAEntityExport.h"

namespace TomoAnalysis::AppEntity {
    class TAEntity_API Camera3DInfo {
    public:
        using Pointer = std::shared_ptr<Camera3DInfo>;
        Camera3DInfo();
        Camera3DInfo(const Camera3DInfo& other);
        ~Camera3DInfo();

        auto operator=(const Camera3DInfo& other)->Camera3DInfo&;
        auto operator==(const Camera3DInfo& other)const->bool;

        [[nodiscard]] auto GetPosition()const->std::tuple<double, double, double>;
        [[nodiscard]] auto GetHeightAngle()const->double;
        [[nodiscard]] auto GetDirection()const->std::tuple<double, double, double, double>;

        auto SetPosition(double x, double y, double z)const->void;
        auto SetHeightAngle(double heightAngle)const->void;
        auto SetDirection(double axisX, double axisY, double axisZ, double radian)const->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}