#pragma once

#include <memory>
#include <QString>

#include "TAEntityExport.h"

namespace TomoAnalysis {
    class TAEntity_API MaskInfo {
    public :
        typedef MaskInfo Self;
        typedef std::shared_ptr<Self> Pointer;
        typedef QList<Pointer> List;

    public:
        MaskInfo();
        MaskInfo(const MaskInfo& other);
        virtual ~MaskInfo();

        auto Init()->void;

        auto GetReferenceTCFPath()->QString;
        auto SetReferenceTCFPath(const QString& path)->void;

        auto GetMaskTCFPath()->QString;
        auto SetMaskTCFPath(const QString& path)->void;

        auto GetInterpretation()->QString;
        auto SetInterpretation(const QString& interpretation)->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}