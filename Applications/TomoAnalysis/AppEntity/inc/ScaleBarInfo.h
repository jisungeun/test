#pragma once
#include <memory>
#include <tuple>
#include "MetaInfoDefines.h"
#include "TAEntityExport.h"

namespace TomoAnalysis::AppEntity {    
    class TAEntity_API ScaleBarInfo {
    public:
        using Pointer = std::shared_ptr<ScaleBarInfo>;
        ScaleBarInfo();
        ScaleBarInfo(const ScaleBarInfo& other);
        ~ScaleBarInfo();

        auto operator=(const ScaleBarInfo& other)->ScaleBarInfo&;
        auto operator==(const ScaleBarInfo& other)const->bool;

        [[nodiscard]] auto GetCoordinate()const->std::tuple<double, double>;
        [[nodiscard]] auto GetColor()const->std::tuple<double, double, double>;
        [[nodiscard]] auto GetOrientation()const->Orientation;
        [[nodiscard]] auto GetLength()const->double;
        [[nodiscard]] auto GetTick()const->double;
        [[nodiscard]] auto isTextVisible()const->bool;
        [[nodiscard]] auto isVisible()const->bool;

        auto SetCoordinate(double x, double y)const->void;
        auto SetColor(double r, double g, double b)const->void;
        auto SetOrientation(Orientation orientation)const->void;
        auto SetLength(double length)const->void;
        auto SetTick(double tick)const->void;
        auto SetIsTextVisible(bool isVisible)const->void;
        auto SetIsVisible(bool isVisible)const->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}