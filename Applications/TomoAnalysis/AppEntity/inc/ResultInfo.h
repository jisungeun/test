#pragma once

#include <memory>
#include <QString>

#include "MaskInfo.h"
#include "PluginAppInfo.h"

#include "TAEntityExport.h"

namespace TomoAnalysis {
    class TAEntity_API ResultInfo {
    public:
        typedef ResultInfo Self;
        typedef std::shared_ptr<Self> Pointer;
        typedef QList<Pointer> List;

    public:
        ResultInfo();
        ResultInfo(const ResultInfo& other);
        virtual ~ResultInfo();

        auto Init()->void;

        auto GetPath() const ->QString;
        auto SetPath(const QString& path) const ->void;

        auto GetMaskInfoList() const ->MaskInfo::List;
        auto SetMaskInfoList(const MaskInfo::List& list) const ->void;

        auto GetReportAppInfoList() const ->PluginAppInfo::List;
        auto SetReportAppInfoList(const PluginAppInfo::List& list) const ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}