#pragma once
#include <memory>
#include <tuple>

#include "TAEntityExport.h"

namespace TomoAnalysis::AppEntity {
    class TAEntity_API DeviceInfo {
    public:
        using Pointer = std::shared_ptr<DeviceInfo>;
        DeviceInfo();
        DeviceInfo(const DeviceInfo& other);
        ~DeviceInfo();

        auto operator=(const DeviceInfo& other)->DeviceInfo&;
        auto operator==(const DeviceInfo& other)const->bool;

        [[nodiscard]] auto GetColor()const->std::tuple<double, double, double>;
        [[nodiscard]] auto GetFontSize()const->double;
        [[nodiscard]] auto isVisible()const->bool;

        auto SetColor(double r, double g, double b)const->void;
        auto SetFontSize(double size)const->void;
        auto SetIsVisible(bool isVisible)const->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}