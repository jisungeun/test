#pragma once

#include <memory>

#include <QDateTime>

#include "TCFDir.h"
#include "PlaygroundInfo.h"
#include "PluginAppInfo.h"

#include "TAEntityExport.h"

namespace TomoAnalysis{
    class TAEntity_API ProjectInfo {
    public:
        typedef ProjectInfo Self;
        typedef std::shared_ptr<Self> Pointer;

        typedef QList<Pointer> SubProjectInfoList;

    public:
        ProjectInfo();
        virtual ~ProjectInfo();

        auto Init()->void;

        auto GetPath(void) const ->QString;
        auto SetPath(const QString& path) const ->void;

        auto GetName(void) const ->QString;
        auto SetName(const QString& name) const ->void;

        auto UpdatePGInfo(const QString& oriName,const QString& newName)const->void;

        auto GetUser(void) const ->QString;
        auto SetUser(const QString& user) const ->void;

        auto GetCreatedTime(void) const ->QDateTime;
        auto SetCreatedTime(const QDateTime& dateTime) const ->void;

        auto GetModifiedTime(void) const ->QDateTime;
        auto SetModifiedTime(const QDateTime& dateTime) const ->void;

        auto GetTCFDirList(void) const ->TCFDir::List;
        auto SetTCFDirList(const TCFDir::List& tcfDirList) const ->void;
        auto AddTCFDir(const TCFDir::Pointer& tcfDir) const ->void;
        auto GetTCFDir(const QString& dirPath) const ->TCFDir::Pointer;
        auto RemoveTCFDir(const QString& dirPath) const ->bool;
        auto ExistTCFDir(const QString& dirPath) const ->bool;
        auto ScanTCFDir(void) const ->void;

        auto AddWorkset(const QString& name, int id) -> void;
        auto RemoveWorkset(int id) -> void;
        auto ExistsWorkset(int id) -> bool;
        auto GetWorksets() const->const QMap<int, QString>&;
        auto GetWorksetName(int id) const->const QString&;
        auto GetWorksetDirs() const->const QMap<int, TCFDir::List>&;
        auto LinkTcfDirToWorkset(int worksetId, const TCFDir::Pointer& tcfDir) -> void;
        auto LinkTcfDirsToWorkset(int worksetId, const TCFDir::List& tcfDir) -> void;
        auto UnlinkTcfDirToWorkset(int worksetId, const TCFDir::Pointer& tcfDir) -> void;
        auto GetWorksetIdOfTcfDir(const TCFDir::Pointer& tcfDir) const->QVector<int>;
        auto GetCountOfWorkset(int worksetId) const -> int;

        auto GetPlaygroundInfoList(void) const ->PlaygroundInfo::List;
        auto SetPlaygroundInfoList(const PlaygroundInfo::List& playgroundInfoList) const ->void;
        auto AddPlaygroundInfo(const PlaygroundInfo::Pointer& playgroundInfo) const ->void;
        auto FindPlaygroundInfo(const QString& path) const ->PlaygroundInfo::Pointer;
        auto RemovePlaygroundInfo(const QString& path) const->bool;

        auto SetCurrentHyperCube(const HyperCube::Pointer& hypercube)->void;
        auto GetCurrentHyperCube()const->HyperCube::Pointer;

        auto GetCurrentPlayground(void) const ->PlaygroundInfo::Pointer;
        auto SetCurrentPlayground(const QString& path) const ->bool;

        auto SetCurrentAppInfo(const PluginAppInfo::Pointer& info)->void;
        auto GetCurrentAppInfo()const->PluginAppInfo::Pointer;

        auto GetMeasurementAppInfoList(void) const ->PluginAppInfo::List;
        auto SetMeasurementAppInfoList(const PluginAppInfo::List& measurementAppInfoList) const ->void;

        auto GetSubProjectList(void) const ->SubProjectInfoList;
        auto SetSubProjectList(const SubProjectInfoList& subProjectInfoList) const ->void;
        auto AddSubProject(const ProjectInfo::Pointer& project) const ->void;
        auto FindSubProject(const QString& path) const ->ProjectInfo::Pointer;

        auto GetComment(void) const ->QString;
        auto SetComment(const QString& comment) const ->void;

        auto IsRoot(void) const -> bool;
        auto SetRoot(const bool& root) const ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}