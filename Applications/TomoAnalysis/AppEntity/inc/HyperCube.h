#pragma once

#include <memory>
#include <QString>

#include "Cube.h"
#include "TAEntityExport.h"

namespace TomoAnalysis {
    class TAEntity_API HyperCube {
    public:
        typedef HyperCube Self;
        typedef std::shared_ptr<Self> Pointer;
        typedef QList<Pointer> List;

    public:
        HyperCube();
        HyperCube(const HyperCube::Pointer other);
        virtual ~HyperCube();

        auto Init()->void;

        auto GetName() const->QString;
        auto SetName(const QString& name) const ->void;

        auto GetPosition()const->QPoint;
        auto SetPosition(const QPoint& pos)const->void;

        auto GetCubeList() const->Cube::List;
        auto SetCubeList(const Cube::List& list) const ->void;

        auto AddCube(const Cube::Pointer& cube)const->void;
        auto FindCube(const QString& name)const->Cube::Pointer;
        auto RemoveCube(const QString& name)const ->void;

        auto GetMeasurementAppInfoList() const->PluginAppInfo::List;
        auto SetMeasurementAppInfoList(const PluginAppInfo::List& list) const ->void;
        auto AddApp(const PluginAppInfo::Pointer& app)const->void;
        auto RemoveApp(const QString& name)const->void;
        auto FindApp(const QString& name)const->PluginAppInfo::Pointer;

        auto GetResultInfoList() const->ResultInfo::List;
        auto SetResultInfoList(const ResultInfo::List& list) const ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}

