#pragma once
#include <memory>
#include <tuple>

#include "TAEntityExport.h"

namespace TomoAnalysis::AppEntity{
    class TAEntity_API TimeStampInfo {
    public:
        using Pointer = std::shared_ptr<TimeStampInfo>;
        TimeStampInfo();
        TimeStampInfo(const TimeStampInfo& other);
        ~TimeStampInfo();

        auto operator=(const TimeStampInfo& other)->TimeStampInfo&;
        auto operator==(const TimeStampInfo& other)const->bool;

        [[nodiscard]] auto GetCoordinate()const->std::tuple<double, double>;
        [[nodiscard]] auto GetColor()const->std::tuple<double, double, double>;
        [[nodiscard]] auto GetFontSize()const->double;
        [[nodiscard]] auto isVisible()const->bool;

        auto SetCoordinate(double x, double y)const->void;
        auto SetColor(double r, double g, double b)const->void;
        auto SetFontSize(double size)const->void;
        auto SetIsVisible(bool isVisible)const->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };

}