#pragma once

#include <tuple>
#include <memory>

#include "DeviceInfo.h"
#include "ScalarBarInfo.h"
#include "ScaleBarInfo.h"
#include "TimeStampInfo.h"

#include "TAEntityExport.h"

namespace TomoAnalysis::AppEntity {
    class TAEntity_API AnnotationInfo {
    public:
        using Pointer = std::shared_ptr<AnnotationInfo>;

        AnnotationInfo();
        AnnotationInfo(const AnnotationInfo& other);
        ~AnnotationInfo();

        [[nodiscard]] auto hasScalarMetaInfo()const->bool;
        [[nodiscard]] auto GetScalarMetaInfo()const->ScalarBarInfo;
        [[nodiscard]] auto hasScaleMetaInfo()const->bool;
        [[nodiscard]] auto GetScaleMetaInfo()const->ScaleBarInfo;
        [[nodiscard]] auto hasTimeMetaInfo()const->bool;
        [[nodiscard]] auto GetTimeMetaInfo()const->TimeStampInfo;
        [[nodiscard]] auto hasDeviceMetaInfo()const->bool;
        [[nodiscard]] auto GetDeviceMetaInfo()const->DeviceInfo;
        
        auto SetScalarMetaInfo(const ScalarBarInfo& meta)const->void;
        auto SetScaleMetaInfo(const ScaleBarInfo& meta)const->void;
        auto SetTimeMetaInfo(const TimeStampInfo& meta)const->void;
        auto SetDeviceMetaInfo(const DeviceInfo& meta)const->void;        

        auto operator=(const AnnotationInfo& other)->AnnotationInfo&;
        auto operator==(const AnnotationInfo& other)const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}