#pragma once
#include <memory>
#include <tuple>
#include "MetaInfoDefines.h"
#include "TAEntityExport.h"

namespace TomoAnalysis::AppEntity {
    class TAEntity_API ScalarBarInfo {
    public:
        using Pointer = std::shared_ptr<ScalarBarInfo>;

        ScalarBarInfo();
        ScalarBarInfo(const ScalarBarInfo& other);
        ~ScalarBarInfo();

        auto operator=(const ScalarBarInfo& other)->ScalarBarInfo&;
        auto operator==(const ScalarBarInfo& other)const->bool;

        //follow colormap information based on VisualizationInfo
        [[nodiscard]] auto GetCoordinate()const->std::tuple<double, double>;
        [[nodiscard]] auto GetOrientation()const->Orientation;
        [[nodiscard]] auto isVisible()const->bool;

        auto SetCoordinate(double x, double y)const->void;
        auto SetOrientation(Orientation orientation)const->void;
        auto SetIsVisible(bool visible)const->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}