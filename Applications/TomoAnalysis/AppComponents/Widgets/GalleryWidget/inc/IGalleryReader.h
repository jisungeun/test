#pragma once

#include <QPixmap>
#include <QObject>
#include <QStringList>
#include <QMouseEvent>

#include "TA_Widgets_GalleryWidgetExport.h"

namespace TomoAnalysis::Components {
	class TA_Widgets_GalleryWidget_API IGalleryReader : public QObject {
		Q_OBJECT

	public:
		using QObject::QObject;

		virtual auto GetImageCount() const -> int = 0;
		virtual auto GetLayerCount() const -> int = 0;

		virtual auto IsLoaded(int index) const -> bool = 0;
		virtual auto IsLayerLoaded(int imageIndex, int layerIndex) const -> bool = 0;
		virtual auto GetImage(int index, int size) const -> QPixmap = 0;
		virtual auto GetLayerImage(int imageIndex, int layerIndex, int size) const -> QPixmap = 0;

		virtual auto OnMouseHovered(int index, bool hovered) -> void {}
		virtual auto OnMousePressed(int index, bool pressed, QMouseEvent* event) -> void {}
		virtual auto OnClicked(int index, QMouseEvent* event) -> void {}
		virtual auto OnDoubleClicked(int index) -> void {}
		virtual auto OnVisibleIndexChanged(int from, int to) -> void {}
		virtual auto OnItemResized(int size) -> void {}

	signals:
		auto ImageCountChanged() -> void;
		auto Updated() -> void;
	};
}