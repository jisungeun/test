#pragma once

#include <memory>
#include <QAbstractScrollArea>

#include <QWidget>

#include "IGalleryReader.h"

#include "TA_Widgets_GalleryWidgetExport.h"

namespace TomoAnalysis::Components {
	struct Range {
		int from = -1;
		int to = -1;
	};

	class TA_Widgets_GalleryWidget_API GalleryWidget : public QWidget {
	public:
		explicit GalleryWidget(QWidget* parent = nullptr);
		explicit GalleryWidget(IGalleryReader* reader, QWidget* parent = nullptr);
		~GalleryWidget() override;

		auto SetScrollArea(QAbstractScrollArea* area) -> void;
		auto ScrollTo(int index) -> void;

		auto SetReader(IGalleryReader* reader) -> void;
		auto GetReader() const->IGalleryReader*;

		auto SetSpacing(int space) -> void;
		auto GetSpacing() const -> int;

		auto SetColumnCount(int count) -> void;
		auto GetColumnCount() const->int;

		auto SetAnimative(bool animative) -> void;
		auto IsAnimative() const->bool;

		auto SetRefreshInterval(int msec) -> void;
		auto GetRefreshInterval() const -> int;

	protected:
		auto paintEvent(QPaintEvent*) -> void override;
		auto resizeEvent(QResizeEvent*) -> void override;

		auto mousePressEvent(QMouseEvent* event) -> void override;
		auto mouseReleaseEvent(QMouseEvent* event) -> void override;
		auto mouseDoubleClickEvent(QMouseEvent* event) -> void override;
		auto mouseMoveEvent(QMouseEvent* event) -> void override;
		auto leaveEvent(QEvent* event) -> void override;

		auto minimumSizeHint() const->QSize override;
		auto heightForWidth(int) const -> int override;
		auto hasHeightForWidth() const -> bool override;

	protected slots:
		auto OnScrollAreaValueChanged(int value) -> void;
		auto OnImageCountChanged() -> void;

		auto OnUpdated() -> void;
		auto OnAnimationValueChanged(const QVariant& value) -> void;
		auto OnTimerTimeout() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
