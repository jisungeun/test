#include <QPainter>
#include <QVariantAnimation>
#include <QTimer>
#include <QDateTime>
#include <QScrollBar>
#include <QDebug>

#include "FlowLayout.h"
#include "GalleryWidget.h"

namespace TomoAnalysis::Components {
	struct GalleryWidget::Impl {
		IGalleryReader* reader = nullptr;
		QAbstractScrollArea* area = nullptr;
		QWidget* parent = nullptr;

		int colCount = 5;
		int spacing = 0;

		int lastHoveredIndex = -1;
		int lastPressedIndex = -1;
		mutable int itemSizeCache = 50;
		mutable int heightCache = 0;
		int yPos = 0;
		bool resizing = false;

		QLinearGradient gradient;
		mutable QVariantAnimation anime;
		bool animative = true;

		mutable Range rangeCache;
		mutable double scrollRatio = 0.0;
		mutable double scrollIdx = 0.0;

		QTimer timer;
		uint64_t epoch = 0;

		auto GetIndexOfPosition(int xPos, int yPos) const -> int;
		auto GetItemPosition(int index) const->QPoint;
		auto GetItemSize(int width) const -> int;
		auto GetPreferedHeight(int width) const -> int;
		auto UpdateItemSize(int width) const -> void;
		auto UpdatePreferedHeight() const -> bool;
		auto UpdateVisibleIndex() const-> bool;
		auto UpdateAnime() const  -> void;
	};

	auto GalleryWidget::Impl::GetIndexOfPosition(int xPos, int yPos) const -> int {
		const int size = (parent->width() + spacing) / colCount;

		const auto xIndex = static_cast<int>(xPos / static_cast<double>(size));
		const auto yIndex = static_cast<int>(yPos / static_cast<double>(size));

		if (xPos % size < itemSizeCache && yPos % size < itemSizeCache)
			return yIndex * colCount + xIndex;

		return -1;
	}

	auto GalleryWidget::Impl::GetItemPosition(int index) const -> QPoint {
		const int size = (parent->width() + spacing) / colCount;

		int x = (index % colCount) * size;
		int y = (index / colCount) * size;

		return { x, y };
	}

	auto GalleryWidget::Impl::GetItemSize(int width) const -> int {
		return std::max(1, (width - (spacing * (colCount - 1))) / colCount);
	}

	auto GalleryWidget::Impl::GetPreferedHeight(int width) const -> int {
		if (reader) {
			const auto itemSize = GetItemSize(width);
			const auto itemHeight = itemSize * ((reader->GetImageCount() + colCount - 1) / colCount);
			const auto spaceHeight = spacing * ((reader->GetImageCount() - 1) / colCount);

			return std::max(itemHeight + spaceHeight, 0);
		}

		return heightCache;
	}

	auto GalleryWidget::Impl::UpdateItemSize(int width) const -> void {
		const auto size = GetItemSize(width);

		if (itemSizeCache != size) {
			itemSizeCache = size;

			if (reader)
				reader->OnItemResized(itemSizeCache);
		}
	}

	auto GalleryWidget::Impl::UpdatePreferedHeight() const -> bool {
		UpdateItemSize(parent->width());

		const auto itemCount = (reader) ? reader->GetImageCount() : 0;
		const auto itemHeight = itemSizeCache * ((itemCount + colCount - 1) / colCount);
		const auto spaceHeight = spacing * ((itemCount - 1) / colCount);
		const auto height = std::max(itemHeight + spaceHeight, 0);

		if (heightCache != height) {
			heightCache = height;
			return true;
		}

		return false;
	}

	auto GalleryWidget::Impl::UpdateVisibleIndex() const -> bool {
		const auto height = (area) ? area->height() : heightCache;

		if (height > 0 && reader) {
			const auto itemSize = (parent->width() + spacing) / colCount;
			const auto yFrom = yPos / itemSize;
			const auto yTo = (yPos + height + itemSize - 1) / itemSize;

			const auto from = std::min(yFrom * colCount, reader->GetImageCount());
			const auto to = std::min(yTo * colCount, reader->GetImageCount());

			if (rangeCache.from != from || rangeCache.to != to) {
				rangeCache.from = from;
				rangeCache.to = to;

				reader->OnVisibleIndexChanged(from, to);
				UpdateAnime();
				return true;
			}
		}

		return false;
	}

	auto GalleryWidget::Impl::UpdateAnime() const -> void {
		if (animative && anime.state() == QAbstractAnimation::Stopped && reader->GetImageCount() > 0)
			anime.start();
		else
			anime.stop();
	}

	GalleryWidget::GalleryWidget(QWidget* parent) : QWidget(parent), d(new Impl) {
		d->parent = this;

		auto p = sizePolicy();
		p.setHeightForWidth(true);
		setSizePolicy(p);
		setMouseTracking(true);

		d->timer.setInterval(100);

		d->anime.setDuration(4500);
		d->anime.setStartValue(0.0);
		d->anime.setEndValue(1.0);
		d->anime.setEasingCurve(QEasingCurve::InOutQuint);
		d->anime.setLoopCount(-1);

		d->gradient.setColorAt(0.0, { 144, 144, 144, 30 });
		d->gradient.setColorAt(0.5, { 144, 144, 144, 70 });
		d->gradient.setColorAt(1.0, { 144, 144, 144, 30 });

		connect(&d->anime, &QVariantAnimation::valueChanged, this, &GalleryWidget::OnAnimationValueChanged);
		connect(&d->timer, &QTimer::timeout, this, &GalleryWidget::OnTimerTimeout);
	}

	GalleryWidget::GalleryWidget(IGalleryReader* reader, QWidget* parent) : GalleryWidget(parent) {
		SetReader(reader);
	}

	GalleryWidget::~GalleryWidget() = default;

	auto GalleryWidget::SetScrollArea(QAbstractScrollArea* area) -> void {
		d->area = area;

		connect(area->verticalScrollBar(), &QAbstractSlider::valueChanged, this, &GalleryWidget::OnScrollAreaValueChanged);
	}

	auto GalleryWidget::ScrollTo(int index) -> void {
		if (d->area) {
			const auto pos = d->GetItemPosition(index);

			if (d->yPos > pos.y())
				d->area->verticalScrollBar()->setValue(pos.y());
			else if (d->yPos + d->area->height() - d->itemSizeCache < pos.y())
				d->area->verticalScrollBar()->setValue(pos.y() - d->area->height() + d->itemSizeCache);
		}
	}

	auto GalleryWidget::SetReader(IGalleryReader* reader) -> void {
		d->reader = reader;

		connect(d->reader, &IGalleryReader::ImageCountChanged, this, &GalleryWidget::OnImageCountChanged);
		connect(d->reader, &IGalleryReader::Updated, this, &GalleryWidget::OnUpdated);

		if (d->UpdatePreferedHeight())
			updateGeometry();
		d->UpdateVisibleIndex();

		updateGeometry();
		update();
	}

	auto GalleryWidget::GetReader() const -> IGalleryReader* {
		return d->reader;
	}

	auto GalleryWidget::SetSpacing(int space) -> void {
		d->spacing = space;

		if (d->UpdatePreferedHeight())
			updateGeometry();
		d->UpdateVisibleIndex();

		updateGeometry();
		update();
	}

	auto GalleryWidget::GetSpacing() const -> int {
		return d->spacing;
	}

	auto GalleryWidget::SetColumnCount(int count) -> void {
		d->colCount = count;

		if (d->UpdatePreferedHeight())
			updateGeometry();
		d->UpdateVisibleIndex();

		if (d->area)
			d->area->verticalScrollBar()->setValue(static_cast<int>(d->heightCache * d->scrollRatio));

		update();
	}

	auto GalleryWidget::GetColumnCount() const -> int {
		return d->colCount;
	}

	auto GalleryWidget::SetAnimative(bool animative) -> void {
		d->animative = animative;
		d->UpdateAnime();
	}

	auto GalleryWidget::IsAnimative() const -> bool {
		return d->animative;
	}

	auto GalleryWidget::SetRefreshInterval(int msec) -> void {
		d->timer.setInterval(msec);
	}

	auto GalleryWidget::GetRefreshInterval() const -> int {
		return d->timer.interval();
	}

	auto GalleryWidget::paintEvent(QPaintEvent* paintEvent) -> void {
		QWidget::paintEvent(paintEvent);
		QPainter painter(this);
		painter.setPen(Qt::PenStyle::NoPen);

		int paintedCount = 0;

		if (!d->reader)
			return;

		for (int i = d->rangeCache.from; i < d->rangeCache.to; i++) {
			const auto point = d->GetItemPosition(i);
			auto painted = false;

			if (d->reader->IsLoaded(i)) {
				if (const auto& image = d->reader->GetImage(i, d->itemSizeCache); !image.isNull()) {
					painted = true;
					painter.drawPixmap(point.x(), point.y(), d->itemSizeCache, d->itemSizeCache, image);
					paintedCount++;
				}
			}

			for (int j = 0; j < d->reader->GetLayerCount(); j++) {
				if (d->reader->IsLayerLoaded(i, j)) {
					if (const auto& layer = d->reader->GetLayerImage(i, j, d->itemSizeCache); !layer.isNull())
						painter.drawPixmap(point.x(), point.y(), d->itemSizeCache, d->itemSizeCache, layer);
				}
			}

			if (!painted) {
				painter.setBrush((d->animative) ? QBrush(d->gradient) : QColor(128, 128, 128, 128));
				painter.drawRect(point.x(), point.y(), d->itemSizeCache, d->itemSizeCache);
			}
		}

		if (paintedCount == d->rangeCache.to - d->rangeCache.from && d->anime.state() == QAbstractAnimation::Running)
			d->anime.stop();
		else if (paintedCount != d->rangeCache.to - d->rangeCache.from && d->anime.state() == QAbstractAnimation::Stopped)
			d->anime.start();
	}

	auto GalleryWidget::resizeEvent(QResizeEvent* resizeEvent) -> void {
		QWidget::resizeEvent(resizeEvent);

		if (!d->resizing) {
			d->resizing = true;

			if (resizeEvent->oldSize().width() != resizeEvent->size().width()) {
				if (d->UpdatePreferedHeight())
					updateGeometry();
				if (d->UpdateVisibleIndex())
					update();

				if (d->area)
					d->area->verticalScrollBar()->setValue(static_cast<int>((d->itemSizeCache + d->spacing) * d->scrollIdx));
			}

			d->resizing = false;
		}
	}

	auto GalleryWidget::mousePressEvent(QMouseEvent* event) -> void {
		QWidget::mousePressEvent(event);

		const auto idx = d->GetIndexOfPosition(event->x(), event->y());
		if (!d->reader || idx >= d->reader->GetImageCount())
			return;

		d->reader->OnMousePressed(idx, true, event);
		d->lastPressedIndex = idx;
	}

	auto GalleryWidget::mouseReleaseEvent(QMouseEvent* event) -> void {
		QWidget::mouseReleaseEvent(event);

		if (d->reader && d->lastPressedIndex > -1) {
			d->reader->OnMousePressed(d->lastPressedIndex, false, event);

			const auto idx = d->GetIndexOfPosition(event->x(), event->y());
			if (idx >= d->reader->GetImageCount())
				return;

			if (d->lastPressedIndex == idx)
				d->reader->OnClicked(idx, event);
		}

		d->lastPressedIndex = -1;
	}

	auto GalleryWidget::mouseDoubleClickEvent(QMouseEvent* event) -> void {
		QWidget::mouseDoubleClickEvent(event);

		const auto idx = d->GetIndexOfPosition(event->x(), event->y());
		if (!d->reader || idx >= d->reader->GetImageCount())
			return;

		d->reader->OnDoubleClicked(idx);
	}

	auto GalleryWidget::mouseMoveEvent(QMouseEvent* event) -> void {
		QWidget::mouseMoveEvent(event);

		const auto idx = d->GetIndexOfPosition(event->x(), event->y());
		if (!d->reader || idx >= d->reader->GetImageCount())
			return;

		if (idx != d->lastHoveredIndex) {
			if (d->lastHoveredIndex > -1) {
				d->reader->OnMouseHovered(d->lastHoveredIndex, false);
				d->lastHoveredIndex = -1;
			}

			if (idx > -1) {
				d->reader->OnMouseHovered(idx, true);
				d->lastHoveredIndex = idx;
			}
		}
	}

	auto GalleryWidget::leaveEvent(QEvent* event) -> void {
		QWidget::leaveEvent(event);

		if (d->reader && d->lastHoveredIndex > -1) {
			d->reader->OnMouseHovered(d->lastHoveredIndex, false);
			d->lastHoveredIndex = -1;
		}
	}

	auto GalleryWidget::minimumSizeHint() const -> QSize {
		const auto width = (d->colCount - 1) * d->spacing + d->colCount;
		return { width, 50 };
	}

	auto GalleryWidget::heightForWidth(int i) const -> int {
		return d->GetPreferedHeight(i);
	}

	auto GalleryWidget::hasHeightForWidth() const -> bool {
		return true;
	}

	auto GalleryWidget::OnScrollAreaValueChanged(int value) -> void {
		d->yPos = value;
		d->epoch = QDateTime::currentMSecsSinceEpoch();
		d->timer.start();

		if (!d->resizing && d->heightCache > 0) {
			d->scrollIdx = value / static_cast<double>(d->itemSizeCache);
			d->scrollRatio = value / static_cast<double>(d->heightCache);
		}
	}

	auto GalleryWidget::OnImageCountChanged() -> void {
		if (d->UpdatePreferedHeight())
			updateGeometry();

		if (d->UpdateVisibleIndex())
			update();
	}

	auto GalleryWidget::OnUpdated() -> void {
		update();
	}

	auto GalleryWidget::OnAnimationValueChanged(const QVariant& value) -> void {
		const auto size = std::max(50, width() / 4);
		const auto incline = size / 2;
		const auto height = d->area ? std::min(d->area->height(), d->heightCache) : d->heightCache;

		const auto wp = (width() + (size * 2)) * value.toDouble() - size;
		const auto hp = height * value.toDouble();

		d->gradient.setStart(wp - size, d->yPos + hp - incline);
		d->gradient.setFinalStop(wp, d->yPos + hp);

		update();
	}

	auto GalleryWidget::OnTimerTimeout() -> void {
		if (QDateTime::currentMSecsSinceEpoch() - d->epoch > 100) {
			if (d->UpdateVisibleIndex())
				update();

			d->timer.stop();
		}
	}
}
