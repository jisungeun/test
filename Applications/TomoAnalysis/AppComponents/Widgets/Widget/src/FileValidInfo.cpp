
#include <QCloseEvent>
#include <QAbstractItemView>
#include <QHeaderView>
#include <QFileInfo>
#include <TCFMetaReader.h>

#include "ui_FileValidInfo.h"
#include "FileValidInfo.h"

#include <QDateTime>

namespace TomoAnalysis {
	struct FileValidInfo::Impl {
		Ui::fileValidInfo* ui{ nullptr };
		QStringList horizontalHeaders{ QStringList() };
		QStringList tcfPaths{ QStringList() };
		QStringList tcfCubes{ QStringList() };
		QList<int> tcfTypes;		
	};
	FileValidInfo::FileValidInfo(QWidget* parent) : QWidget(parent), d{ new Impl }{
		d->ui = new Ui::fileValidInfo;
		d->ui->setupUi(this);
		d->ui->tabWidget->setTabEnabled(0, true);
		d->ui->tabWidget->setCurrentIndex(0);

		d->ui->label->setObjectName("h5");

		d->ui->acceptBtn->setObjectName("bt-square-primary");
		d->ui->rejectBtn->setObjectName("bt-square-gray");

		connect(d->ui->acceptBtn, SIGNAL(clicked()), this, SLOT(OnAcceptBtn()));
		connect(d->ui->rejectBtn, SIGNAL(clicked()), this, SLOT(OnRejectBtn()));

		InitTableHeader();
	}
	FileValidInfo::~FileValidInfo() {

	}	
	auto FileValidInfo::SetTcfPaths(const QStringList& tcfs) -> void {
		d->tcfPaths = tcfs;		
    }
	auto FileValidInfo::SetTcfTypes(const QList<int>& tcfTypes) -> void {
		d->tcfTypes = tcfTypes;
    }
	auto FileValidInfo::SetTcfCubes(const QStringList& tcfCubes) -> void {
		d->tcfCubes = tcfCubes;
    }

	auto FileValidInfo::ComposeTable() -> void {
		auto GetSimpleName = [=](const QString& name) {
			auto split = name.split("/");
			auto simple = split[split.size() - 1];

			if (20 > simple.size()) {
				return simple;
			}

			const auto list = simple.split(".");
			if (3 > list.size()) {
				return simple;
			}

			simple = simple.remove(0, 20);
			return simple.chopped(4);
		};
		d->ui->ignoreTable->clearContents();
		d->ui->ignoreTable->setRowCount(d->tcfPaths.count());
		auto reader = TC::IO::TCFMetaReader();		
		for(auto i=0;i<d->tcfPaths.count();i++) {
			const auto meta = reader.Read(d->tcfPaths[i]);						
			auto date = QDateTime::fromString(meta->common.createDate, "yyyy-MM-dd hh:mm:ss").toString("yyyy.MM.dd");
			d->ui->ignoreTable->setItem(i, 0, new QTableWidgetItem(date));
			d->ui->ignoreTable->setItem(i, 1, new QTableWidgetItem(GetSimpleName(d->tcfPaths[i])));
			if(d->tcfTypes[i]==0) {
				d->ui->ignoreTable->setItem(i, 2, new QTableWidgetItem("Not Available"));
			}else if(d->tcfTypes[i]==1){
				d->ui->ignoreTable->setItem(i, 2, new QTableWidgetItem("RI Threshold"));
			}
			d->ui->ignoreTable->setItem(i, 3, new QTableWidgetItem(d->tcfCubes[i]));
		}
    }
	auto FileValidInfo::InitTableHeader() -> void {
		d->horizontalHeaders <<"Date"<< "File Name" << "Available Processor" <<"Cube" ;
		d->ui->ignoreTable->setColumnCount(d->horizontalHeaders.count());
		d->ui->ignoreTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
		d->ui->ignoreTable->setSelectionMode(QAbstractItemView::SingleSelection);
		d->ui->ignoreTable->setHorizontalHeaderLabels(d->horizontalHeaders);
		for(auto i=0;i<d->horizontalHeaders.count();i++) {
			d->ui->ignoreTable->horizontalHeader()->setSectionResizeMode(i, QHeaderView::ResizeMode::Stretch);
		}	    
    }
	auto FileValidInfo::SetInformationText(const QString& txt) -> void {
		d->ui->label->setText(txt);
    }

	void FileValidInfo::closeEvent(QCloseEvent* event) {
        if(event->spontaneous()) {
			emit sigReject();
        }else {
			QWidget::closeEvent(event);
        }
    }

	//slots
	void FileValidInfo::OnAcceptBtn() {
		emit sigAccept();
    }
	void FileValidInfo::OnRejectBtn() {
		emit sigReject();
    }

}
