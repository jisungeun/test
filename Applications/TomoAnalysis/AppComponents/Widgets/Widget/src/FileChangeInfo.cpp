#include "ui_FileChangeInfo.h"

#include <QFileInfo>
#include <QDir>
#include <QCloseEvent>

#include <TCMeasureReader.h>

#include "FileChangeInfo.h"

namespace TomoAnalysis {
    struct FileChangeInfo::Impl {
        Ui::fileChangeForm* ui{ nullptr };
        QStringList horizontalHeaders{QStringList()};

        QString wholeMessage{QString()};
        QString parentFolder{QString()};
        QStringList tcfPaths{QStringList()};
        QList<QList<int>> timePoints{QList<QList<int>>()};
    };
    FileChangeInfo::FileChangeInfo(QWidget* parent) : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::fileChangeForm;
        d->ui->setupUi(this);
        d->ui->tabWidget->setTabEnabled(0, true);
        d->ui->tabWidget->setTabEnabled(1, false);
        d->ui->tabWidget->setCurrentIndex(0);        

                
        d->ui->addLabel->setObjectName("h5");
        d->ui->wholeLabel->setObjectName("h6");
        d->ui->wholeMsg->setObjectName("h6");
        d->ui->wholeFolder->setObjectName("h5");

        d->ui->acceptBtn->setObjectName("bt-square-primary");
        d->ui->rejectBtn->setObjectName("bt-square-gray");

        connect(d->ui->acceptBtn, SIGNAL(clicked()), this, SLOT(OnAcceptBtn()));
        connect(d->ui->rejectBtn, SIGNAL(clicked()), this, SLOT(OnRejectBtn()));

        InitTableHeader();
    }
    FileChangeInfo::~FileChangeInfo() {
        
    }
    auto FileChangeInfo::SetMessage(const QString& msg) -> void {
        d->wholeMessage = msg;
        d->ui->tabWidget->setTabEnabled(0, false);
        d->ui->tabWidget->setTabEnabled(1, true);
        d->ui->tabWidget->setCurrentIndex(1);

        d->ui->wholeMsg->setText(msg);
    }
    auto FileChangeInfo::SetParentPath(const QString& path) -> void {
        d->parentFolder = path;
        d->ui->wholeFolder->setText(path);
    }
    auto FileChangeInfo::SetTcfPaths(const QStringList& tcfs) -> void {
        d->ui->tabWidget->setTabEnabled(0, true);
        d->ui->tabWidget->setTabEnabled(1, false);
        d->ui->tabWidget->setCurrentIndex(0);
        d->tcfPaths = tcfs;
    }
    auto FileChangeInfo::SetTimePoints(QList<QList<int>> tps) -> void {
        d->timePoints = tps;
    }
    void FileChangeInfo::OnAcceptBtn() {
        emit sigAccept();
    }
    void FileChangeInfo::OnRejectBtn() {
        emit sigReject();
    }
    auto FileChangeInfo::InitTableHeader() -> void {
        d->horizontalHeaders << "File Name" << "Time Steps" << "Type";
        d->ui->addTable->setColumnCount(d->horizontalHeaders.count());
        d->ui->addTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
        d->ui->addTable->setSelectionMode(QAbstractItemView::SingleSelection);
        d->ui->addTable->setHorizontalHeaderLabels(d->horizontalHeaders);
        for(auto i=0;i<d->horizontalHeaders.count();i++) {
            d->ui->addTable->horizontalHeader()->setSectionResizeMode(i, QHeaderView::ResizeMode::Stretch);
        }        
    }
    auto FileChangeInfo::ComposeTable() -> void {        
        if(d->timePoints.isEmpty()) {
            ComposeSingle();
        }else {
            ComposeTime();
        }
    }
    auto FileChangeInfo::ComposeSingle()->void {        
        QStringList addMsk;
        QStringList addReps;

        for (auto i = 0; i < d->tcfPaths.count();i++) {
            auto tcf = d->tcfPaths[i];
            QFileInfo fileInfo(tcf);
            auto file_name = fileInfo.fileName();
            //check mask exist
            auto msk_folder = d->parentFolder + "/" + file_name.chopped(4) + "_MultiLayer";
            auto msk_path = msk_folder +  ".msk";
            QDir mask_dir(msk_folder);
            QFileInfo mask_file(msk_path);
            if(!mask_dir.exists() || !mask_file.exists() ) {
                QString real_name = file_name.chopped(4);
                addMsk.push_back(real_name);
                //addMsk.push_back(msk_path);
            }

            auto label_folder = d->parentFolder + "/" + file_name.chopped(4) + "_MultiLabel";
            auto label_path = msk_folder + ".msk";
            QDir label_dir(label_folder);
            QFileInfo label_file(label_path);
            if(!label_dir.exists()|| !label_file.exists()) {
                auto real_name = file_name.chopped(4);
                if(!addMsk.contains(real_name)) {
                    addMsk.push_back(real_name);
                }                
            }

            auto rep_path = d->parentFolder + "/" + file_name.chopped(4) + ".rep";
            QFileInfo rep_file(rep_path);
            if(!rep_file.exists()) {
                //addReps.push_back(rep_path);
                addReps.push_back(file_name.chopped(4));
            }
        }

        d->ui->addTable->clearContents();
        d->ui->addTable->setRowCount(addMsk.count() + addReps.count());
        if(addMsk.count() + addReps.count() < 1) {
            emit sigAccept();            
            return;
        }
        //additional masks
        for(auto i=0;i< addMsk.count();i++) {
            d->ui->addTable->setItem(i, 0, new QTableWidgetItem(addMsk[i]));
            d->ui->addTable->setItem(i, 1, new QTableWidgetItem(QString::number(1)));
            d->ui->addTable->setItem(i, 2, new QTableWidgetItem("Cell Mask"));
        }        
        //additional reports
        for(auto i= addMsk.count();i< addMsk.count() +addReps.count();i++) {
            auto index = i - addMsk.count();
            d->ui->addTable->setItem(i, 0, new QTableWidgetItem(addReps[index]));
            d->ui->addTable->setItem(i, 1, new QTableWidgetItem(QString::number(1)));
            d->ui->addTable->setItem(i, 2, new QTableWidgetItem("Measure"));
        }
    }
    auto FileChangeInfo::ComposeTime()->void {
        QStringList addMskLabel;
        QStringList addMskLayer;
        QStringList addReps;
        QList<QList<int>> addMskLabelTime;
        QList<QList<int>> addMskLayerTime;
        QList<QList<int>> addRepTime;
        for(auto i=0;i<d->tcfPaths.count();i++) {
            auto tcf = d->tcfPaths[i];
            QFileInfo fileInfo(tcf);
            auto file_name = fileInfo.fileName();
            auto msk_folder = d->parentFolder + "/" + file_name.chopped(4) + "_MultiLabel";
            auto msk_path = msk_folder + ".msk";
            QFileInfo mask_file(msk_path);
            QList<int> labelAddTimes;
            if(!mask_file.exists()) {
                addMskLabel.push_back(msk_path);
                labelAddTimes = d->timePoints[i];
            }else {
                QDir maskDir(msk_folder);
                if(!maskDir.exists("Blobs/HT")) {
                    addMskLabel.push_back(msk_path);
                    labelAddTimes = d->timePoints[i];
                }else {
                    maskDir.cd("Blobs");
                    maskDir.cd("HT");
                    auto isFirst = true;
                    for(auto tp : d->timePoints[i]) {
                        const QString time_name = QString("%1").arg(tp, 6, 10, QLatin1Char('0'));
                        QFileInfo timeFile(maskDir.path() + "/" + time_name);
                        if(!timeFile.exists()) {
                            if(isFirst) {
                                addMskLabel.push_back(msk_path);
                                isFirst = false;
                            }
                            labelAddTimes.push_back(tp);
                        }
                    }
                }
            }
            if (labelAddTimes.count() > 0) {
                addMskLabelTime.push_back(labelAddTimes);                
            }

            auto layer_folder = d->parentFolder + "/" + file_name.chopped(4) + "_MultiLayer";
            auto layer_path = layer_folder + ".msk";
            QFileInfo layer_file(layer_path);
            QList<int> layerAddTimes;
            if(!layer_file.exists()) {
                addMskLayer.push_back(layer_path);
                layerAddTimes = d->timePoints[i];
            }else {
                QDir layerDir(layer_folder);
                if(!layerDir.exists("Blobs/HT")) {
                    addMskLayer.push_back(layer_path);
                    labelAddTimes = d->timePoints[i];
                }else {
                    layerDir.cd("Blobs");
                    layerDir.cd("HT");
                    auto isFirst = true;
                    for(auto tp : d->timePoints[i]) {
                        const QString time_name = QString("%1").arg(tp, 6, 10, QLatin1Char('0'));
                        QFileInfo timeFile(layerDir.path() + "/" + time_name);
                        if(!timeFile.exists()) {
                            if(isFirst) {
                                addMskLayer.push_back(layer_path);
                                isFirst = false;
                            }
                            layerAddTimes.push_back(tp);
                        }
                    }
                }
            }
            if (layerAddTimes.count() > 0) {
                addMskLayerTime.push_back(layerAddTimes);
            }

            auto rep_path = d->parentFolder + "/" + file_name.chopped(4) + ".rep";
            QFileInfo rep_file(rep_path);
            QList<int> repAddTimes;
            if(!rep_file.exists()) {
                //addReps.push_back(rep_path);
                addReps.push_back(file_name.chopped(4));
                repAddTimes = d->timePoints[i];
            }else {
                auto reader = new TC::IO::TCMeasureReader(rep_path);
                auto organ = reader->GetOrganNames()[0];
                auto existing_tp = reader->GetTimePoints("Measures/"+organ);
                bool isFirst = true;
                for(auto tp : d->timePoints[i]) {
                    if(!existing_tp.contains(tp)) {
                        if(isFirst) {
                            //addReps.push_back(rep_path);
                            addReps.push_back(file_name.chopped(4));
                            isFirst = false;
                        }
                        repAddTimes.push_back(tp);
                    }
                }
                delete reader;
            }
            addRepTime.push_back(repAddTimes);
        }

        QStringList realMsk;
        QList<QList<int>> realMskTime;
        for(auto i=0;i<addMskLabel.count();i++) {
            QFileInfo filePath(addMskLabel[i]);
            auto absPath = filePath.fileName().chopped(15);
            realMsk.push_back(absPath);
            realMskTime.push_back(addMskLabelTime[i]);
        }

        for(auto i=0;i<addMskLayer.count();i++) {
            QFileInfo filePath(addMskLayer[i]);
            auto absPath = filePath.fileName().chopped(15);
            if(realMsk.contains(absPath)) {
                auto idx = realMsk.indexOf(absPath);
                for (auto j = 0; j < addMskLayerTime[i].count();j++) {
                    auto tp = addMskLayerTime[i][j];
                    if(!realMskTime[idx].contains(tp)) {
                        realMskTime[idx].push_back(tp);
                    }                    
                }                
            }else {
                realMsk.push_back(absPath);
                realMskTime.push_back(addMskLayerTime[i]);
            }
        }

        d->ui->addTable->clearContents();
        //d->ui->addTable->setRowCount(addMskLabel.count() + addMskLayer.count() + addReps.count());
        d->ui->addTable->setRowCount(realMsk.count() + addReps.count());

        //if(addMskLabel.count() + addMskLayer.count() + addReps.count()<1) {
        if (realMsk.count() + addReps.count() < 1) {
            emit sigAccept();            
            return;
        }
        //additional masks
        for(auto i=0;i<realMsk.count();i++) {
            d->ui->addTable->setItem(i, 0, new QTableWidgetItem(realMsk[i]));
            QString times = QString();
            for (auto j = 0; j < realMskTime[i].count() - 1; j++) {
                times += QString::number(realMskTime[i][j] + 1);
                times += ",";
            }
            times += QString::number(realMskTime[i][realMskTime[i].count() - 1] + 1);
            d->ui->addTable->setItem(i, 1, new QTableWidgetItem(times));
            d->ui->addTable->setItem(i, 2, new QTableWidgetItem("Cell Mask"));
        }
        /*for (auto i = 0; i < addMskLabel.count(); i++) {
            d->ui->addTable->setItem(i, 0, new QTableWidgetItem(addMskLabel[i]));
            QString times = QString();
            for(auto j=0;j<addMskLabelTime[i].count() - 1;j++) {
                times += QString::number(addMskLabelTime[i][j]+1);
                times += ",";
            }
            times += QString::number(addMskLabelTime[i][addMskLabelTime[i].count() - 1] +1);
            d->ui->addTable->setItem(i, 1, new QTableWidgetItem(times));
            d->ui->addTable->setItem(i, 2, new QTableWidgetItem("Cell Label Mask"));
        }

        //additional layer
        for(auto i=addMskLabel.count();i<addMskLabel.count()+addMskLayer.count();i++) {
            auto index = i - addMskLabel.count();
            d->ui->addTable->setItem(i, 0, new QTableWidgetItem(addMskLayer[index]));
            QString times = QString();
            for(auto j =0;j<addMskLayerTime[index].count()-1;j++) {
                times += QString::number(addMskLayerTime[index][j] +1);
                times += ",";
            }
            times += QString::number(addMskLayerTime[index][addMskLayerTime[index].count() - 1] +1);
            d->ui->addTable->setItem(i, 1, new QTableWidgetItem(times));
            d->ui->addTable->setItem(i, 2, new QTableWidgetItem("Cell Organ Mask"));
        }*/

        //additional reports
        //for (auto i = addMskLabel.count() + addMskLayer.count(); i < addMskLabel.count() + addMskLayer.count() + addReps.count(); i++) {
        for(auto i= realMsk.count();i<realMsk.count()+addReps.count();i++){
            auto index = i - realMsk.count();            
            d->ui->addTable->setItem(i, 0, new QTableWidgetItem(addReps[index]));
            QString times = QString();
            for (auto j = 0; j < addRepTime[index].count() - 1; j++) {
                times += QString::number(addRepTime[index][j] +1);
                times += ",";
            }
            times += QString::number(addRepTime[index][addRepTime[index].count() - 1] +1);
            d->ui->addTable->setItem(i, 1, new QTableWidgetItem(times));
            d->ui->addTable->setItem(i, 2, new QTableWidgetItem("Measure"));
        }        
    }
    void FileChangeInfo::closeEvent(QCloseEvent* event) {
        if(event->spontaneous()) {
            emit sigReject();
        }else {
            QWidget::closeEvent(event);
        }
    }
}
