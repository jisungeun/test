#pragma once

#include <memory>
#include <QWidget>
#include "TA_Widgets_WidgetExport.h"

namespace TomoAnalysis {
	class TA_Widgets_Widget_API FileChangeInfo : public QWidget {
		Q_OBJECT
	public:
		FileChangeInfo(QWidget* parent = nullptr);
		~FileChangeInfo();

		auto SetMessage(const QString& msg)->void;
		auto SetParentPath(const QString& path)->void;
		auto SetTcfPaths(const QStringList& tcfs)->void;
		auto SetTimePoints(QList<QList<int>> tps)->void;

		auto ComposeTable()->void;

	signals:
		void sigAccept();
		void sigReject();

	protected:
		void closeEvent(QCloseEvent* event) override;

	protected slots:
		void OnAcceptBtn();
		void OnRejectBtn();

	private:
		auto ComposeSingle()->void;
		auto ComposeTime()->void;
		auto InitTableHeader()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}