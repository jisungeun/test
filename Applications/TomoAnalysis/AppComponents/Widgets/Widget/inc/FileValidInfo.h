#pragma once

#include <memory>
#include <QWidget>
#include "TA_Widgets_WidgetExport.h"

namespace TomoAnalysis {
	class TA_Widgets_Widget_API FileValidInfo : public QWidget {
		Q_OBJECT
	public:
		FileValidInfo(QWidget* parent = nullptr);
		~FileValidInfo();

		auto SetTcfPaths(const QStringList& tcfs)->void;
		auto SetTcfCubes(const QStringList& tcfCubes)->void;
		auto SetTcfTypes(const QList<int>& tcfTypes)->void;
		auto ComposeTable()->void;
		auto SetInformationText(const QString& txt)->void;		

	signals:
	    void sigAccept();
		void sigReject();

	protected:
		void closeEvent(QCloseEvent* event) override;

	protected slots:
		void OnAcceptBtn();
		void OnRejectBtn();		

	private:		
		auto InitTableHeader()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}