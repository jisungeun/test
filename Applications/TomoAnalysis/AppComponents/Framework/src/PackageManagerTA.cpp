#include <QMap>

#include <LicenseManager.h>

#include "PackageManagerTA.h"

namespace TomoAnalysis {
    struct PackageManagerTA::Impl {
        QMap<QString, QStringList> package;        
        QStringList current_package;
    };
    PackageManagerTA::PackageManagerTA() : d{ new Impl } {
        BuildBasicPackage();                
    }
    PackageManagerTA::~PackageManagerTA() {
        
    }
    auto PackageManagerTA::AcquirePackage()->bool {
        auto package_name = LicenseManager::GetProductMetaData("Package");
        if(package_name.isEmpty()) {
            package_name = "Basic";
        }
        if(false == d->package.contains(package_name)) {
            return false;
        }
        d->current_package = d->package[package_name];
        return true;
    }
    auto PackageManagerTA::GetActivation(const QString& key) -> bool {
        return d->current_package.contains(key);
    }
    auto PackageManagerTA::BuildBasicPackage() -> void {
        QStringList appList;
        appList.push_back("Basic Analyzer");
        appList.push_back("Basic Analyzer[T]");
        appList.push_back("Mask Editor");
        appList.push_back("2D Image Gallery");
        appList.push_back("Project Manager");
        appList.push_back("FL Mask Generator");
        appList.push_back("3D View");
        appList.push_back("2D View");
        appList.push_back("Project Selector");
        appList.push_back("CILS Project Manager");
        appList.push_back("CILS Executor");
        appList.push_back("2D Mask Editor");
        appList.push_back("ReportFL");
        appList.push_back("Data Manager");
        appList.push_back("Cell Analysis");        
        d->package["Basic"] = appList;
    }
    auto PackageManagerTA::GetPackageAppList() -> QStringList {
        return d->current_package;
    }
}