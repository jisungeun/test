#include "IMainWindowTA.h"

namespace TomoAnalysis {
    struct IMainWindowTA::Impl {
        QString displyTitle;
        QString shortTitle;
    };

    IMainWindowTA::IMainWindowTA(const QString& displayTitle, const QString& shortTitle, QWidget* parent)
        : IMainWindow(parent), d{ new Impl } {
        d->displyTitle = displayTitle;
        d->shortTitle = shortTitle;
    }
    
    IMainWindowTA::~IMainWindowTA() {
    }

    auto IMainWindowTA::GetShortTitle() const -> QString {
        return d->displyTitle;
    }

    auto IMainWindowTA::GetDisplayTitle() const-> QString {
        return d->shortTitle;
    }
}
