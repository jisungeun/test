#include "IMainWindowTA.h"
#include "AppInterfaceTA.h"

namespace TomoAnalysis {
    struct AppInterfaceTA::Impl {
        IMainWindowTA* window{ nullptr };
        bool isProjectManager = false;
    };
    
    AppInterfaceTA::AppInterfaceTA(IMainWindowTA* window) : IAppInterface(), d{ new Impl } {
        d->window = window;
    }

    AppInterfaceTA::~AppInterfaceTA() {
    }        
    
    auto AppInterfaceTA::GetParameter(QString name)->IParameter::Pointer {
        return d->window->GetParameter(name);
    }
    
    auto AppInterfaceTA::SetParameter(QString name,IParameter::Pointer param)->void {
        d->window->SetParameter(name, param);
    }
    
    auto AppInterfaceTA::GetRunType(void) -> std::tuple<bool,bool> {
        return d->window->GetRunType();
    }
    
    auto AppInterfaceTA::GetDisplayTitle() const -> QString {
        return d->window->GetDisplayTitle();
    }

    auto AppInterfaceTA::GetShortTitle() const -> QString {
        return d->window->GetShortTitle();
    }

    auto AppInterfaceTA::GetInterfaceWidget() -> IMainWindow* {
        return d->window;
    }

    auto AppInterfaceTA::Open(const QString& path)->bool {
        Q_UNUSED(path)
        return true;
    }
    
    auto AppInterfaceTA::Execute(const QVariantMap& params)->bool {
        return d->window->Execute(params);        
    }

    auto AppInterfaceTA::IsProjectManager() const -> bool {
        return d->isProjectManager;
    }

    auto AppInterfaceTA::SetProjectManager(bool pm) -> void {
        d->isProjectManager = pm;
    }
}
