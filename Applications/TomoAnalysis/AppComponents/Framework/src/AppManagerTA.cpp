#define LOGGER_TAG "[AppManagerTA]"
#include <TCLogger.h>

#include <iostream>

//ctk
#include <ctkPluginFrameworkLauncher.h>
#include <ctkPluginContext.h>
#include <service/event/ctkEventAdmin.h>

//license
#include <LicenseManager.h>
#include <PackageManagerTA.h>
#include <PluginRegistry.h>

#include "AppInterfaceTA.h"
#include "AppManagerTA.h"

namespace TomoAnalysis {
    struct AppManagerTA::Impl {
        PackageManagerTA::Pointer package;
    };
    AppManagerTA::AppManagerTA() : d{ new Impl } {
        d->package = std::make_shared<PackageManagerTA>();
    }
    AppManagerTA::~AppManagerTA() {
        
    }
    IAppManager::Pointer AppManagerTA::GetInstance() {
        static Pointer theInstance(new AppManagerTA());
        return theInstance;
    }
        
    auto AppManagerTA::PreLoadPlugins() -> void {
        auto psp = GetPluginSearchPaths();        
        for(auto path : psp) {
            auto names = ctkPluginFrameworkLauncher::getPluginSymbolicNames(path);
            for (auto name : names) {                
                /*if (name.contains("experiment")) {
                    continue;
                }
                if (name.contains("general")) {
                    continue;
                }
                if (name.contains("data.")) {
                    continue;
                }
                if(name.contains(".system.")) {
                    continue;
                }                
                //LoadAppPlugin(name);
                LoadAppPlugin(name,true);*/
                if(name.contains(".viewer2dPlugin")) {
                    LoadAppPlugin(name);
                }else if(name.contains(".viewerPlugin")) {
                    LoadAppPlugin(name);
                }else if (name.contains(".datanavigationPlugin")) {
                    LoadAppPlugin(name);
                }
            }            
        }
    }        
    auto AppManagerTA::GetPackageManager() -> IPackageManager::Pointer {
        return d->package;
    }
}
