#pragma once

#include <memory>
#include <IAppInterface.h>

#include "TAFrameworkExport.h"

namespace TomoAnalysis {
	class IMainWindowTA;

	class TAFramework_API AppInterfaceTA : public TC::Framework::IAppInterface {
		Q_OBJECT
			Q_INTERFACES(IAppInterface)

	public:
		AppInterfaceTA(IMainWindowTA* window);
		~AppInterfaceTA();

		auto GetParameter(QString name)->IParameter::Pointer;
		auto SetParameter(QString name, IParameter::Pointer param)->void;
		auto GetRunType(void)->std::tuple<bool, bool>;

		auto GetDisplayTitle() const->QString override;
		auto GetShortTitle() const->QString override;
		auto GetInterfaceWidget()->IMainWindow* override;
		auto Open(const QString& path)->bool override;
		auto Execute(const QVariantMap& params)->bool override;

		auto IsProjectManager() const -> bool;
		auto SetProjectManager(bool pm) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

}
using namespace TomoAnalysis;
Q_DECLARE_INTERFACE(AppInterfaceTA, "AppInterfaceTA");
