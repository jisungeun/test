#pragma once
#include <memory>

#include <IMainWindow.h>

#include <IParameter.h>

#include "TAFrameworkExport.h"

namespace TomoAnalysis {
	class TAFramework_API IMainWindowTA: public TC::Framework::IMainWindow {
		Q_OBJECT

	public:
		IMainWindowTA(const QString& displayTitle, const QString& shortTitle, QWidget* parent = nullptr);
		virtual ~IMainWindowTA();

		auto GetDisplayTitle() const->QString;
		auto GetShortTitle() const->QString;

		virtual auto GetParameter(QString name)->IParameter::Pointer = 0;
		virtual auto SetParameter(QString name,IParameter::Pointer param)->void = 0;		

		virtual auto GetRunType(void)->std::tuple<bool, bool> = 0;
		virtual auto Execute(const QVariantMap& params)->bool = 0;

	    auto TryActivate()->bool override=0;
		auto TryDeactivate()->bool override=0;
		auto IsActivate()->bool override = 0;
		auto GetMetaInfo()->QVariantMap override= 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}