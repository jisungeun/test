#include <IAppManager.h>

#include "TAFrameworkExport.h"

namespace TomoAnalysis {
	class TAFramework_API AppManagerTA : public TC::Framework::IAppManager {
	public:		
		~AppManagerTA();

		static Pointer GetInstance();
				
		auto PreLoadPlugins()->void;		

	protected:
		AppManagerTA();
		auto GetPackageManager()->IPackageManager::Pointer override;

	private:
		struct Impl;
		std::shared_ptr<Impl> d;
	};
}