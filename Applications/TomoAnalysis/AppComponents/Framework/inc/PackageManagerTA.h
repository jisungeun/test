#pragma once

#include <IPackageManager.h>

#include "TAFrameworkExport.h"

namespace TomoAnalysis {
    class TAFramework_API PackageManagerTA : public TC::Framework::IPackageManager{
    public:
        PackageManagerTA();
        ~PackageManagerTA();
        auto GetPackageAppList()->QStringList override;

    protected:
        auto GetActivation(const QString& key) -> bool override;
        auto AcquirePackage()->bool override;
        
    private:
        auto BuildBasicPackage()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}