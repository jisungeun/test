#include "LicenseManager.h"

#include "CryptlexActivator.h"

namespace TomoAnalysis::License::CryptlexLicenseModel {
	CryptlexActivator::CryptlexActivator(const QString& productData, const QString& productId) : ILicenseActivator() {
		LicenseManager::SetLicenseInfo(productData, productId);
	}

	CryptlexActivator::~CryptlexActivator() = default;

	auto CryptlexActivator::Activate(const QString& key) -> bool {
		return LicenseManager::Activate(key);
	}

	auto CryptlexActivator::ActivateOffline(const QString& licenseKey, const QString& filepath) -> bool {
		return LicenseManager::ActivateOfflineLicense(licenseKey, filepath);
	}

	auto CryptlexActivator::Deactivate() -> void {
		LicenseManager::Activate("");
	}

	auto CryptlexActivator::ExportLicense(const QString& key, const QString& filepath) -> bool {
		return LicenseManager::RequestOfflineActivation(key, filepath);
	}

	auto CryptlexActivator::IsActivated() const -> bool {
		return LicenseManager::CheckLicense();
	}

	auto CryptlexActivator::GetEditionName() const -> QString {
		return LicenseManager::GetProductEdition();
	}
}
