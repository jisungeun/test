#include <QDebug>

#include "LicenseManager.h"

#include "CryptlexManager.h"

namespace TomoAnalysis::License::CryptlexLicenseModel {
	struct CryptlexManager::Impl {
		QMap<QString, QString> table;
	};

	CryptlexManager::CryptlexManager() : ILicenseManager(), d(new Impl) {}

	CryptlexManager::~CryptlexManager() = default;

	auto CryptlexManager::SetFeatureTable(const QMap<QString, QString>& table) -> void {
		d->table = table;
	}

	auto CryptlexManager::IsLicensedFeature(const QString& feature) -> bool {
		if (d->table.contains(feature)) {
			if (d->table[feature] == ALWAYS_ACCEPT)
				return true;
			else
				return LicenseManager::Check(d->table[feature]);
		}
		return LicenseManager::Check(feature);
	}
}
