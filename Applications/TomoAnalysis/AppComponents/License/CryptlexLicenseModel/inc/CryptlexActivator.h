#pragma once

#include "ILicenseActivator.h"

#include "TA_License_CryptlexLicenseModelExport.h"

namespace TomoAnalysis::License::CryptlexLicenseModel {
	class TA_License_CryptlexLicenseModel_API CryptlexActivator : public ILicenseActivator {
	public:
		CryptlexActivator(const QString& productData, const QString& productId);
		~CryptlexActivator() override;

		auto Activate(const QString& key) -> bool override;
		auto ActivateOffline(const QString& licenseKey, const QString& filepath) -> bool override;
		auto Deactivate() -> void override;

		auto ExportLicense(const QString& key, const QString& filepath) -> bool override;

		auto IsActivated() const -> bool override;
		auto GetEditionName() const -> QString override;
	};
}
