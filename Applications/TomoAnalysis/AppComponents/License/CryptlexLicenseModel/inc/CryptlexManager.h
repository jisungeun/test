#pragma once

#include "ILicenseManager.h"

#include "TA_License_CryptlexLicenseModelExport.h"

namespace TomoAnalysis::License::CryptlexLicenseModel {
	static const QString ALWAYS_ACCEPT = "{ALWAYS_ACCEPT_LICENSE}";

	class TA_License_CryptlexLicenseModel_API CryptlexManager : public ILicenseManager {
	public:
		CryptlexManager();
		~CryptlexManager() override;

		auto SetFeatureTable(const QMap<QString, QString>& table) -> void;

	protected:
		auto IsLicensedFeature(const QString& feature) -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}