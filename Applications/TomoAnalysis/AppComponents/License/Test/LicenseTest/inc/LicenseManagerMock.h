#pragma once

#include <QMap>

#include "ILicenseManager.h"
#include "LicenseActivatorMock.h"

class LicenseManagerMock : public TomoAnalysis::License::ILicenseManager {
public:
	auto SetFeatureTable(const QMap<QString, QStringList>& featureTable) -> void {
		this->featureTable = featureTable;
	}

	auto IsLicensedFeature(const QString& feature) -> bool override {
		if (activated)
			return featureTable[edition].contains(feature);

		return false;
	}

private:
	QMap<QString, QStringList> featureTable;
};
