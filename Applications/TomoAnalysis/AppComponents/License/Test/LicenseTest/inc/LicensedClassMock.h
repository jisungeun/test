#pragma once
#include "ILicensed.h"

class LicensedClassMock : public TomoAnalysis::License::ILicensed {
public:
	auto SetFeatureName(const QString& name) -> void {
		this->name = name;
	}

	auto GetFeatureName() const -> QString override {
		return name;
	}

	auto OnAccepted() -> void override {
		accepted = true;
	}

	auto OnRejected() -> void override {
		accepted = false;		
	}

	auto IsAccepted() -> bool {
		return accepted;
	}

private:
	bool accepted = false;
	QString name;
};
