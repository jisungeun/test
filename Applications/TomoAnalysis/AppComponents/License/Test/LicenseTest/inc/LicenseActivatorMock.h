#pragma once

#include "ILicenseActivator.h"

// this activator is only for test so that it stores activation in memory.
// commercial activator would rather to use nonvolatile container such as network server, registry...
static bool activated = false;
static QString edition = "";

class LicenseActivatorMock : public TomoAnalysis::License::ILicenseActivator {
public:
	auto SetEditionName(const QString& name) -> void {
		edition = name;
	}

	auto Activate(const QString& key) -> bool override {
		activated = (key == "Pass");

		return activated;
	}
	
	auto Deactivate() -> void override {
		activated = false;
	}

	auto IsActivated() const -> bool override {
		return activated;
	}

	auto GetEditionName() const->QString override {
		return edition;
	}

	auto ActivateOffline(const QString& licenseKey, const QString& filepath) -> bool override { return false; }
	auto ExportLicense(const QString& key, const QString& filepath) -> bool override { return false; }
};
