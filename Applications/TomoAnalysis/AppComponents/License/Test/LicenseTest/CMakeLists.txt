project(TA_License_LicenseTest)

set(INTERFACE_HEADERS
	inc/LicensedClassMock.h
	inc/LicenseActivatorMock.h
	inc/LicenseManagerMock.h
)

set(SOURCES
	src/LicenseTest.cpp
)

add_executable(${PROJECT_NAME}
	${INTERFACE_HEADERS}
	${SOURCES}
)

target_include_directories(${PROJECT_NAME}
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/inc>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<INSTALL_INTERFACE:inc>
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/src
)

target_link_libraries(${PROJECT_NAME}
	PUBLIC
		TomoAnalysis::Components::License::LicenseModel

	PRIVATE
		Catch2::Catch2
)

target_compile_features(${PROJECT_NAME} 
	PUBLIC 
		cxx_std_17
)

target_compile_options(${PROJECT_NAME}
	PUBLIC
		/wd4251
)

set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "Applications/TomoAnalysis/AppComponents/License/Test")

# This makes the project importable from the build directory
export(TARGETS ${PROJECT_NAME} FILE ${PROJECT_NAME}Config.cmake)