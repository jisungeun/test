#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "LicenseActivatorMock.h"
#include "LicensedClassMock.h"
#include "LicenseManagerMock.h"

TEST_CASE("License Pass Scenario") {
	LicenseActivatorMock activator;

	LicenseManagerMock manager;
	manager.SetFeatureTable({ { "Lite", {
										"app1",
										"app2"
									}
								}, { "Advanced", {
										"app1",
										"app3"
								}
							} });

	SECTION("Test Activation Failed") {
		activator.Activate("Fail");
		CHECK(!activator.IsActivated());
	}

	SECTION("Test Deactivated") {
		activator.Activate("Pass");
		activator.Deactivate();
		CHECK(!activator.IsActivated());
	}

	SECTION("Test Activated") {
		activator.Activate("Pass");
		CHECK(activator.IsActivated());
	}

	SECTION("Test Lite Apps") {
		// Commercial Activator usually sets edition name automatically by license key during activation.
		activator.SetEditionName("Lite");

		LicensedClassMock app1;
		LicensedClassMock app2;
		LicensedClassMock app3;
		app1.SetFeatureName("app1");
		app2.SetFeatureName("app2");
		app3.SetFeatureName("app3");
		
		CHECK(manager.Validate(app1) == true);
		CHECK(manager.Validate(app2) == true);
		CHECK(manager.Validate(app3) == false);

		CHECK(app1.IsAccepted() == true);
		CHECK(app2.IsAccepted() == true);
		CHECK(app3.IsAccepted() == false);
	}

	SECTION("Test Advanced Apps") {
		// Commercial Activator usually sets edition name automatically by license key during activation.
		activator.SetEditionName("Advanced");

		LicensedClassMock app1;
		LicensedClassMock app2;
		LicensedClassMock app3;
		app1.SetFeatureName("app1");
		app2.SetFeatureName("app2");
		app3.SetFeatureName("app3");
		
		CHECK(manager.Validate(app1) == true);
		CHECK(manager.Validate(app2) == false);
		CHECK(manager.Validate(app3) == true);

		CHECK(app1.IsAccepted() == true);
		CHECK(app2.IsAccepted() == false);
		CHECK(app3.IsAccepted() == true);
	}
}