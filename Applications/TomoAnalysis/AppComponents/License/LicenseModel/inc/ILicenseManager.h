#pragma once

#include "ILicensed.h"
#include "TA_License_LicenseModelExport.h"

namespace TomoAnalysis::License {
	class TA_License_LicenseModel_API ILicenseManager {
	public:
		ILicenseManager() = default;
		virtual ~ILicenseManager() = default;

		virtual auto Validate(ILicensed& licensed) -> bool;
		virtual auto IsLicensedFeature(const QString& feature) -> bool = 0;
	};
}