#pragma once

#include <QString>

#include "TA_License_LicenseModelExport.h"

namespace TomoAnalysis::License {
	class TA_License_LicenseModel_API ILicensed {
	public:
		ILicensed() = default;
		virtual ~ILicensed() = default;

		virtual auto GetFeatureName() const->QString = 0;

		virtual auto OnAccepted() -> void = 0;
		virtual auto OnRejected() -> void = 0;
	};
}