#pragma once

#include <QString>
#include <QDataStream>

#include "TA_License_LicenseModelExport.h"

namespace TomoAnalysis::License {
	class TA_License_LicenseModel_API ILicenseActivator {
	public:
		ILicenseActivator() = default;
		virtual ~ILicenseActivator() = default;

		virtual auto Activate(const QString& key) -> bool = 0;
		virtual auto ActivateOffline(const QString& licenseKey, const QString& filepath) -> bool = 0;
		virtual auto Deactivate() -> void = 0;

		virtual auto ExportLicense(const QString& key, const QString& filepath) -> bool = 0;

		virtual auto IsActivated() const -> bool = 0;
		virtual auto GetEditionName() const->QString = 0;
	};
}
