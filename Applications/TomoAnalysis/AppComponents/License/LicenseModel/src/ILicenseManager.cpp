#include "ILicenseManager.h"

namespace TomoAnalysis::License {
	auto ILicenseManager::Validate(ILicensed& licensed) -> bool {
		if (IsLicensedFeature(licensed.GetFeatureName())) {
			licensed.OnAccepted();
			return true;
		}

		licensed.OnRejected();
		return false;
	}
}
