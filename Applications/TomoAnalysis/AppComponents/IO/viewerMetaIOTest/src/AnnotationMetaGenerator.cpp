#include "AnnotationMetaGenerator.h"

namespace TomoAnalysis::AppComponents::IO::Test {
    struct AnnotationMetaGenerator::Impl {
        AppEntity::AnnotationInfo::Pointer sparse{ nullptr };
        AppEntity::AnnotationInfo::Pointer full{ nullptr };

        auto Create()->void;
        auto CreateSpare()->void;
        auto CreateFull()->void;
    };
    auto AnnotationMetaGenerator::Impl::Create() -> void {
        CreateSpare();
        CreateFull();
    }
    auto AnnotationMetaGenerator::Impl::CreateSpare() -> void {
        sparse = std::make_shared<AppEntity::AnnotationInfo>();
        AppEntity::ScalarBarInfo scalar;
        scalar.SetCoordinate(0.5, 0.5);
        scalar.SetOrientation(AppEntity::HORIZONTAL);
        scalar.SetIsVisible(true);

        AppEntity::ScaleBarInfo scale;
        scale.SetCoordinate(0.9, 0.9);
        scale.SetOrientation(AppEntity::HORIZONTAL);
        scale.SetLength(10);
        scale.SetColor(1, 0, 0);
        scale.SetIsTextVisible(true);
        scale.SetIsVisible(true);
        scale.SetTick(2);
        
        AppEntity::TimeStampInfo time;
        time.SetIsVisible(false);
        time.SetColor(0, 1, 0);
        time.SetFontSize(10);
        time.SetCoordinate(-0.5, -0.5);

        sparse->SetScalarMetaInfo(scalar);
        sparse->SetScaleMetaInfo(scale);
        sparse->SetTimeMetaInfo(time);
    }
    auto AnnotationMetaGenerator::Impl::CreateFull() -> void {
        full = std::make_shared<AppEntity::AnnotationInfo>();
        AppEntity::ScalarBarInfo scalar;
        scalar.SetCoordinate(0.5, 0.5);
        scalar.SetOrientation(AppEntity::HORIZONTAL);
        scalar.SetIsVisible(true);

        AppEntity::ScaleBarInfo scale;
        scale.SetCoordinate(0.9, 0.9);
        scale.SetOrientation(AppEntity::HORIZONTAL);
        scale.SetLength(10);
        scale.SetColor(1, 0, 0);
        scale.SetIsTextVisible(true);
        scale.SetIsVisible(true);
        scale.SetTick(2);

        AppEntity::TimeStampInfo time;
        time.SetIsVisible(false);
        time.SetColor(0, 1, 0);
        time.SetFontSize(10);
        time.SetCoordinate(-0.5, -0.5);
        
        AppEntity::DeviceInfo device;
        device.SetIsVisible(true);
        device.SetColor(0, 0, 1);
        device.SetFontSize(20);
        
        full->SetScalarMetaInfo(scalar);
        full->SetScaleMetaInfo(scale);
        full->SetTimeMetaInfo(time);
        full->SetDeviceMetaInfo(device);        
    }
    AnnotationMetaGenerator::AnnotationMetaGenerator() : d{ new Impl } {
        d->Create();
    }
    AnnotationMetaGenerator::~AnnotationMetaGenerator() {
        
    }
    auto AnnotationMetaGenerator::SparseInfo() const -> AppEntity::AnnotationInfo::Pointer {
        return d->sparse;
    }    
    auto AnnotationMetaGenerator::FullInfo() const -> AppEntity::AnnotationInfo::Pointer {
        return d->full;
    }
}