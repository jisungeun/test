#pragma once

#include <memory>

#include <QString>

#include <VisualizationInfo.h>

namespace TomoAnalysis::AppComponents::IO::Test {
    class VisualizationMetaGenerator {
    public:
        VisualizationMetaGenerator();
        ~VisualizationMetaGenerator();

        auto SparseInfo()const->AppEntity::VisualizationInfo::Pointer;
        auto FullInfo()const->AppEntity::VisualizationInfo::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}