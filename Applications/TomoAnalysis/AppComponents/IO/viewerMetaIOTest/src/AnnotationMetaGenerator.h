#pragma once

#include <memory>

#include <QString>

#include <AnnotationInfo.h>

namespace TomoAnalysis::AppComponents::IO::Test {
    class AnnotationMetaGenerator {
    public:
        AnnotationMetaGenerator();
        ~AnnotationMetaGenerator();

        auto SparseInfo()const->AppEntity::AnnotationInfo::Pointer;
        auto FullInfo()const->AppEntity::AnnotationInfo::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}