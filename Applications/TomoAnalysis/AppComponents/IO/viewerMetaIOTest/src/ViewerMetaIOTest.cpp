#include <catch2/catch.hpp>

#include <QDir>
#include <QString>

#include <VisualizationInfo.h>
#include <AnnotationInfo.h>
#include <iostream>
#include <ViewerMetaReader.h>
#include <ViewerMetaWriter.h>

#include "AnnotationMetaGenerator.h"
#include "VisualizatoinMetaGenerator.h"

namespace TomoAnalysis::AppComponents::IO::Test {        
    ViewerMetaWriter writer;
    ViewerMetaReader reader;
    auto outputVizPath = QDir::tempPath() + "/ViewerMetaTest/test.tcviz";
    auto outputAnnoPath = QDir::tempPath() + "/ViewerMetaTest/test.tcanno";
    VisualizationMetaGenerator vizGen;
    AnnotationMetaGenerator annoGen;
    TEST_CASE("Visualization Meta File IO") {                   
        SECTION("Write visualization meta failure") {
            REQUIRE_FALSE(writer.WriteVisualizationMeta("", vizGen.FullInfo()));
            REQUIRE_FALSE(writer.WriteVisualizationMeta(outputVizPath,nullptr));
        }
        SECTION("Write visualizatoin meta success") {
            REQUIRE(writer.WriteVisualizationMeta(outputVizPath, vizGen.FullInfo()));
            SECTION("Read failure") {
                auto readResult = reader.ReadVizInfo("/dummypath");
                REQUIRE(nullptr == readResult);
            }
            SECTION("Read success") {                
                auto visualization = reader.ReadVizInfo(outputVizPath);
                REQUIRE(nullptr != visualization);

                SECTION("Compare HT information") {
                    CHECK(visualization->GetHTInfo().operator==(vizGen.FullInfo()->GetHTInfo()));
                }
                SECTION("Compare FL information") {
                    for (auto i = 0; i < 3; i++) {
                        CHECK(visualization->GetFLInfo(i).operator==(vizGen.FullInfo()->GetFLInfo(i)));
                    }
                }
                SECTION("Compare 2D TF information") {
                    CHECK(visualization->GetTF2dList().count() == vizGen.FullInfo()->GetTF2dList().count());
                    for (auto i = 0; i < visualization->GetTF2dList().count();i++) {
                        CHECK(visualization->GetTF2dList()[i].operator==(vizGen.FullInfo()->GetTF2dList()[i]));
                    }
                }
                SECTION("Compare Camera2d information") {
                    CHECK(visualization->GetCamera2dMetaInfo().operator==(vizGen.FullInfo()->GetCamera2dMetaInfo()));
                }
                SECTION("Copmare Camera3d information") {
                    CHECK(visualization->GetCamera3dMetaInfo().operator==(vizGen.FullInfo()->GetCamera3dMetaInfo()));
                }
            }
        }
        if (QFile::exists(outputVizPath)) {
            QFile::remove(outputVizPath);
        }
    }
    TEST_CASE("Visualization Partial Meta File IO") {
        SECTION("Write partial visualization meta success") {
            REQUIRE(writer.WriteVisualizationMeta(outputVizPath, vizGen.SparseInfo()));
            SECTION("Read success") {
                auto partial_visualization = reader.ReadVizInfo(outputVizPath);
                REQUIRE(nullptr != partial_visualization);

                SECTION("Compare HT information") {
                    CHECK(partial_visualization->GetHTInfo().operator==(vizGen.SparseInfo()->GetHTInfo()));
                }
                SECTION("Compare FL information") {
                    for (auto i = 0; i < 3; i++) {
                        CHECK(partial_visualization->GetFLInfo(i).operator==(vizGen.SparseInfo()->GetFLInfo(i)));
                    }
                }
                SECTION("Compare 2D TF information") {
                    CHECK(partial_visualization->GetTF2dList().count() == vizGen.SparseInfo()->GetTF2dList().count());
                    for (auto i = 0; i < partial_visualization->GetTF2dList().count(); i++) {
                        CHECK(partial_visualization->GetTF2dList()[i].operator==(vizGen.SparseInfo()->GetTF2dList()[i]));
                    }
                }
                SECTION("Check Missing Information Flag") {
                    REQUIRE(true == partial_visualization->GetIsRiInfoExist());
                    REQUIRE(true == partial_visualization->GetIsFLInfoExist(0));
                    REQUIRE(false == partial_visualization->GetIsFLInfoExist(1));
                    REQUIRE(false == partial_visualization->GetIsFLInfoExist(2));
                    REQUIRE(0 == partial_visualization->GetTF2dList().count());
                    REQUIRE(false == partial_visualization->hasCamera2dMetaInfo());
                    REQUIRE(false == partial_visualization->hasCamera3dMetaInfo());
                }
            }            
        }
        if (QFile::exists(outputVizPath)) {
            QFile::remove(outputVizPath);
        }
    }
    TEST_CASE("Annotation Meta File IO") {
        SECTION("Write annotation meta failure") {
            REQUIRE_FALSE(writer.WriteAnnotationMeta("", annoGen.FullInfo()));
            REQUIRE_FALSE(writer.WriteAnnotationMeta(outputAnnoPath, nullptr));
        }
        SECTION("Write annotation meta success") {
            REQUIRE(writer.WriteAnnotationMeta(outputAnnoPath, annoGen.FullInfo()));
            SECTION("Read failure") {
                auto readResult = reader.ReadAnnoInfo("/dummypath");
                REQUIRE(nullptr == readResult);
            }
            SECTION("Read success") {
                auto annotation = reader.ReadAnnoInfo(outputAnnoPath);
                REQUIRE(nullptr != annotation);
                SECTION("Compare ScalarBar information") {
                    CHECK(annotation->GetScalarMetaInfo().operator==(annoGen.FullInfo()->GetScalarMetaInfo()));
                }
                SECTION("Compare ScaleBar information") {
                    CHECK(annotation->GetScaleMetaInfo().operator==(annoGen.FullInfo()->GetScaleMetaInfo()));
                }
                SECTION("Compare TimeStamp information") {
                    CHECK(annotation->GetTimeMetaInfo().operator==(annoGen.FullInfo()->GetTimeMetaInfo()));
                }
                SECTION("Compare Device information") {
                    CHECK(annotation->GetDeviceMetaInfo().operator==(annoGen.FullInfo()->GetDeviceMetaInfo()));
                }                
            }
        }
        if (QFile::exists(outputAnnoPath)) {
            QFile::remove(outputAnnoPath);
        }
    }
    TEST_CASE("Annotation Partial Meta File IO") {
        SECTION("Write annotation meta success") {
            REQUIRE(writer.WriteAnnotationMeta(outputAnnoPath, annoGen.SparseInfo()));
            SECTION("Read success") {
                auto annotation = reader.ReadAnnoInfo(outputAnnoPath);
                REQUIRE(nullptr != annotation);
                REQUIRE(true == annotation->hasScalarMetaInfo());
                SECTION("Compare ScalarBar information") {
                    CHECK(annotation->GetScalarMetaInfo().operator==(annoGen.SparseInfo()->GetScalarMetaInfo()));
                }
                REQUIRE(true == annotation->hasScaleMetaInfo());
                SECTION("Compare ScaleBar information") {
                    CHECK(annotation->GetScaleMetaInfo().operator==(annoGen.SparseInfo()->GetScaleMetaInfo()));
                }
                REQUIRE(true == annotation->hasTimeMetaInfo());
                SECTION("Compare TimeStamp information") {
                    CHECK(annotation->GetTimeMetaInfo().operator==(annoGen.SparseInfo()->GetTimeMetaInfo()));
                }
                REQUIRE(false == annotation->hasDeviceMetaInfo());                
            }
        }
        if (QFile::exists(outputAnnoPath)) {
            QFile::remove(outputAnnoPath);
        }
    }
}