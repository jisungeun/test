#include "VisualizatoinMetaGenerator.h"

namespace TomoAnalysis::AppComponents::IO::Test {
    struct VisualizationMetaGenerator::Impl {
        AppEntity::VisualizationInfo::Pointer sparse{ nullptr };
        AppEntity::VisualizationInfo::Pointer full{ nullptr };

        auto Create()->void;
        auto CreateSparse()->void;
        auto CreateFull()->void;
    };
    auto VisualizationMetaGenerator::Impl::Create() -> void {
        CreateSparse();
        CreateFull();
    }
    auto VisualizationMetaGenerator::Impl::CreateSparse() -> void {
        sparse = std::make_shared<AppEntity::VisualizationInfo>();

        AppEntity::ColormapInfo htInfo;
        htInfo.SetIsGamma(true);
        htInfo.SetIsVisible(true);
        htInfo.SetOpacity(0.5);
        htInfo.SetGamma(1.2);
        htInfo.SetPredefinedColormap(2);
        htInfo.SetXRange(13300, 13800);

        AppEntity::ColormapInfo flCh1Info;
        flCh1Info.SetIsGamma(false);
        flCh1Info.SetIsVisible(true);
        flCh1Info.SetOpacity(0.7);
        flCh1Info.SetGamma(1.0);
        flCh1Info.SetRgb(1, 0, 0);
        flCh1Info.SetXRange(10, 400);

        sparse->SetHTInfo(htInfo);
        sparse->SetFLInfo(0,flCh1Info);
    }
    auto VisualizationMetaGenerator::Impl::CreateFull() -> void {
        full = std::make_shared<AppEntity::VisualizationInfo>();

        AppEntity::ColormapInfo htInfo;
        htInfo.SetIsGamma(true);
        htInfo.SetIsVisible(true);
        htInfo.SetOpacity(0.5);
        htInfo.SetGamma(1.2);
        htInfo.SetPredefinedColormap(2);
        htInfo.SetXRange(13300, 13800);

        AppEntity::ColormapInfo flCh1Info;
        flCh1Info.SetIsGamma(false);
        flCh1Info.SetIsVisible(true);
        flCh1Info.SetOpacity(0.7);
        flCh1Info.SetGamma(1.0);
        flCh1Info.SetRgb(1, 0, 0);
        flCh1Info.SetXRange(10, 400);

        AppEntity::ColormapInfo flCh2Info;
        flCh2Info.SetIsGamma(false);
        flCh2Info.SetIsVisible(true);
        flCh2Info.SetOpacity(0.7);
        flCh2Info.SetGamma(1.0);
        flCh2Info.SetRgb(0, 1, 0);
        flCh2Info.SetXRange(10, 400);

        AppEntity::ColormapInfo flCh3Info;
        flCh3Info.SetIsGamma(false);
        flCh3Info.SetIsVisible(true);
        flCh3Info.SetOpacity(0.7);
        flCh3Info.SetGamma(1.0);
        flCh3Info.SetRgb(0, 0, 1);
        flCh3Info.SetXRange(10, 400);

        AppEntity::ColormapInfo tfBox1;
        AppEntity::ColormapInfo tfBox2;

        tfBox1.SetIsGamma(false);
        tfBox1.SetIsVisible(true);
        tfBox1.SetOpacity(0.3);
        tfBox1.SetRgb(0, 1, 1);
        tfBox1.SetGamma(1.0);
        tfBox1.SetXRange(13333, 14444);
        tfBox1.SetYRange(40, 80);

        tfBox2.SetIsGamma(true);
        tfBox2.SetIsVisible(true);
        tfBox2.SetOpacity(0.9);
        tfBox2.SetRgb(1, 1, 0);
        tfBox2.SetGamma(1.5);
        tfBox2.SetXRange(13050, 13850);
        tfBox2.SetYRange(20, 100);

        AppEntity::Camera2DInfo cam2d;
        cam2d.SetHeight(30);
        cam2d.SetPosition(5, 5, 5);

        AppEntity::Camera3DInfo cam3d;
        cam3d.SetPosition(7, 7, 7);
        cam3d.SetDirection(1, 0, 0, 1.57);
        cam3d.SetHeightAngle(20);

        full->SetHTInfo(htInfo);
        full->SetFLInfo(0, flCh1Info);
        full->SetFLInfo(1, flCh2Info);
        full->SetFLInfo(2, flCh3Info);
        full->AppendTF(tfBox1);
        full->AppendTF(tfBox2);
        full->SetCamera2dMetaInfo(cam2d);
        full->SetCamera3dMetaInfo(cam3d);
    }
    VisualizationMetaGenerator::VisualizationMetaGenerator() : d{ new Impl } {
        d->Create();
    }
    VisualizationMetaGenerator::~VisualizationMetaGenerator() {
        
    }
    auto VisualizationMetaGenerator::SparseInfo() const -> AppEntity::VisualizationInfo::Pointer {
        return d->sparse;
    }
    auto VisualizationMetaGenerator::FullInfo() const -> AppEntity::VisualizationInfo::Pointer {
        return d->full;
    }
}