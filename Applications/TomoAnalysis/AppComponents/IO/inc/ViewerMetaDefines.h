#pragma once

namespace TomoAnalysis::AppComponents::IO {
    namespace Key {
        namespace Visualization {
            constexpr char RIMeta[] = "riInfo";
            constexpr char FLCh1Meta[] = "flCh1Info";
            constexpr char FLCh2Meta[] = "flCh2Info";
            constexpr char FLCh3Meta[] = "flCh3Info";
            constexpr char RI2DTF[] = "transferFunction2D";

            //for box items
            constexpr char XRange[] = "intensityRange";
            constexpr char YRange[] = "gradientRange";
            constexpr char TFColor[] = "transferfunctionColor";
            constexpr char TFColorMap[] = "predefinedColorMap";
            constexpr char TFOpacity[] = "transferfunctionOpacity";
            constexpr char TFGamma[] = "transferfunctionGamma";
            constexpr char GammaEnable[] = "isGammaEnabled";
            constexpr char TFVisible[] = "transferfunctionVisiblity";
        }
        namespace Annotation {
            constexpr char ScalarMeta[] = "scalarBarInfo";
            constexpr char ScaleMeta[] = "scaleBarInfo";
            constexpr char TimeMeta[] = "timestampInfo";
            constexpr char DeviceMeta[] = "deviceInfo";
            //camera 2d meta only stores XY plane camera information
            constexpr char Camera2dMeta[] = "orthographicCameraInfo";
            constexpr char Camera3dMeta[] = "perspectiveCamInfo";

            //for annotation details - common
            constexpr char annoColor[] = "annotationColor";
            constexpr char annoPosition[] = "annotationPosition";
            constexpr char annoVisible[] = "annotationVisibility";
            constexpr char annoOrientation[] = "annotationOrientation";
            constexpr char annoFontSize[] = "annotationFontSize";
            constexpr char annoTextVisible[] = "annotationTextVisibility";
            
            //for annotation details - specific item
            constexpr char camHeight[] = "cameraXYHeight";
            constexpr char camHeightAngle[] = "camera3dHeightAngle";
            constexpr char camPos[] = "cameraPosition";
            constexpr char camDir[] = "cameraDirection";
            constexpr char scaleTick[] = "scaleBarTick";
            constexpr char scaleLength[] = "scaleBarLength";
        }
    }
}