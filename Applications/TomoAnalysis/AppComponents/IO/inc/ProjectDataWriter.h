#pragma once

#include <QJsonArray>

#include <Cube.h>
#include <ProjectInfo.h>
#include <PlaygroundInfo.h>

#include "TAIOExport.h"

namespace TomoAnalysis{
	class TAIO_API ProjectDataWriter{
	public:
		ProjectDataWriter();
		~ProjectDataWriter();

		auto WriteProjectData(const QString& path, const ProjectInfo::Pointer& projectInfo)->bool;
		auto WritePlaygroundData(const QString& path, const PlaygroundInfo::Pointer& playgroundInfo)->bool;

	private:
		auto LoadTCFDirList(const TCFDir::List& list)->QJsonArray;
		auto LoadHypercubeInfoList(const HyperCube::List& list)->QJsonArray;
		auto LoadCubeInfoList(const Cube::List& list)->QJsonArray;
		auto LoadMeasurementAppInfoList(const PluginAppInfo::List& list)->QJsonArray;
		auto LoadProcessorInfoList(const ProcessorInfo::List& list)->QJsonArray;		
		auto LoadResultInfoList(const ResultInfo::List& list)->QJsonArray;
		auto LoadMaskInfoList(const MaskInfo::List& list)->QJsonArray;
		auto LoadReportAppInfoList(const PluginAppInfo::List& list)->QJsonArray;
	};
}
