#pragma once

#include <any>
#include <memory>

#include <enum.h>

#include <QStringList>

#include "TAIOExport.h"

namespace TomoAnalysis {
    BETTER_ENUM(ArrType, int,
        arrUSHORT = 0, arrDouble = 1, arrFLOAT = 2,
        arrSHORT = 3, arrBYTE = 4, arrINT = 5,
        arrUBYTE = 6, arrUINT = 7);
    class TAIO_API DataShareStorage final{
    public:
        typedef std::shared_ptr<DataShareStorage> Pointer;
    private:
        DataShareStorage();
    public:
        ~DataShareStorage();
        static auto GetInstance()->Pointer;

        auto FreeArr(const QString& arrName)->bool;
        auto Clear()->void;

        auto CopyImgArrToStorage(void* arr, QList<int> shape,QList<double> resolution, const QString& arrName, int type = ArrType::arrUSHORT)->bool;
        auto CopyImgArrFromStorage(std::any& arr, const QString& arrName)->std::tuple<QList<int>,QList<double>, QString>;
        auto GetArrPtr(const QString& arrName)->std::tuple<QList<int>, QList<double>, QString, std::any*>;
        auto GetDimension(const QString& arrName)->QList<int>;
        auto GetResolution(const QString& arrName)->QList<double>;
        auto GetMinMax(const QString& arrName)->std::tuple<double, double>;
        auto GetArrNameList()const->QStringList;        

    private:
        auto CopyBufferToStorage(void* source,QString arrName, QString typeString, unsigned dimSize)->bool;
        auto CopyBufferFromStorage(std::any& anyPtr, void* source, QString typeString, unsigned dimSize)->bool;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}