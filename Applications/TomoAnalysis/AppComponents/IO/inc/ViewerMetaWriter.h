#pragma once

#include <QString>

#include <VisualizationInfo.h>
#include <AnnotationInfo.h>

#include "TAIOExport.h"

namespace TomoAnalysis::AppComponents::IO {
    class TAIO_API ViewerMetaWriter {
    public:
        ViewerMetaWriter();
        ~ViewerMetaWriter();

        auto WriteVisualizationMeta(const QString& path, const AppEntity::VisualizationInfo::Pointer& meta)const->bool;
        auto WriteAnnotationMeta(const QString& path, const AppEntity::AnnotationInfo::Pointer& meta)const->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}