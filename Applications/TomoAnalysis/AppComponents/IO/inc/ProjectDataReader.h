#pragma once

#include <QJsonArray>

#include <TCFDir.h>
#include <Cube.h>
#include <HyperCube.h>
#include <ProjectInfo.h>
#include <PlaygroundInfo.h>

#include "TAIOExport.h"

namespace TomoAnalysis{
	class TAIO_API ProjectDataReader{
	public:
		ProjectDataReader();
		~ProjectDataReader();

		auto ReadProjectData(const QString& path, ProjectInfo::Pointer& projectInfo)->bool;
		auto ReadPlaygroundData(const QString& path, PlaygroundInfo::Pointer& playgroundInfo)->bool;

	private:
		auto MakeTCFDirList(const QJsonArray& array) const ->TCFDir::List;
		auto MakeHypercubeInfoList(const QJsonArray& array, const Cube::List& cubeList)->HyperCube::List;
		auto MakeCubeInfoList(const QJsonArray& array) const ->Cube::List;
		auto MakeMeasurementAppInfoList(const QJsonArray& array) const ->PluginAppInfo::List;
		auto MakeProcessorInfoList(const QJsonArray& array) const->ProcessorInfo::List;
		auto MakeResultInfoList(const QJsonArray& array) const ->ResultInfo::List;
		auto MakeMaskInfoList(const QJsonArray& array) const ->MaskInfo::List;
		auto MakeReportAppInfoList(const QJsonArray& array) const ->PluginAppInfo::List;
	};
}