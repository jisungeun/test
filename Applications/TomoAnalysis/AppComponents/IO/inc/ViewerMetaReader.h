#pragma once

#include <memory>

#include <QString>
#include <VisualizationInfo.h>
#include <AnnotationInfo.h>

#include "TAIOExport.h"

namespace TomoAnalysis::AppComponents::IO {
    class TAIO_API ViewerMetaReader {
    public:
        ViewerMetaReader();
        ~ViewerMetaReader();

        auto ReadVizInfo(const QString& path)const->AppEntity::VisualizationInfo::Pointer;
        auto ReadAnnoInfo(const QString& path)const->AppEntity::AnnotationInfo::Pointer;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}