#include <QMap>

#include "DataShareStorage.h"

namespace TomoAnalysis {
    struct DataShareStorage::Impl {
        QMap<QString,std::any*> arrList;
        QMap<QString, QList<int>> shapeList;
        QMap<QString, QList<double>> resolutionList;
        QMap<QString, QString> typeList;
    };
    DataShareStorage::DataShareStorage() : d{ new Impl } {
        
    }
    DataShareStorage::~DataShareStorage() {
        
    }
    auto DataShareStorage::GetInstance() -> Pointer {
        static Pointer theInstance{ new DataShareStorage() };
        return theInstance;
    }
    auto DataShareStorage::CopyImgArrFromStorage(std::any& arr, const QString& arrName) -> std::tuple<QList<int>, QList<double>, QString> {
        if (d->arrList.contains(arrName)) {
            return std::make_tuple(QList<int>(),QList<double>(), QString());
        }
        auto dimSize = 1;
        for(auto dim : d->shapeList[arrName]) {
            dimSize *= dim;
        }
        CopyBufferFromStorage(arr, d->arrList[arrName], d->typeList[arrName], dimSize);
        return std::make_tuple(d->shapeList[arrName],d->resolutionList[arrName],d->typeList[arrName]);
    }
    auto DataShareStorage::GetMinMax(const QString& arrName) -> std::tuple<double, double> {
        if(false == d->arrList.contains(arrName)) {
            return std::make_tuple(-1, 1);
        }
        auto typeString = d->typeList[arrName];
        auto dimSize = 1;
        for(auto dim : d->shapeList[arrName]) {
            dimSize *= 1;
        }
        double min = static_cast<double>(INT_MAX);
        double max = static_cast<double>(-INT_MAX);
        if (typeString == "arrUSHORT") {
            auto rawData = std::any_cast<std::shared_ptr<uint16_t[]>>(d->arrList[arrName]);
            for(auto i=0;i<dimSize;i++) {
                auto val = *(rawData->get() + i);
                if (min > val) min = val;
                if (max < val) max = val;
            }
        }
        else if (typeString == "arrUBYTE") {
            auto rawData = std::any_cast<std::shared_ptr<uint8_t[]>>(d->arrList[arrName]);
            for (auto i = 0; i < dimSize; i++) {
                auto val = *(rawData->get() + i);
                if (min > val) min = val;
                if (max < val) max = val;
            }
        }
        else if (typeString == "arrBYTE") {
            auto rawData = std::any_cast<std::shared_ptr<int8_t[]>>(d->arrList[arrName]);
            for (auto i = 0; i < dimSize; i++) {
                auto val = *(rawData->get() + i);
                if (min > val) min = val;
                if (max < val) max = val;
            }
        }
        else if (typeString == "arrDouble") {
            auto rawData = std::any_cast<std::shared_ptr<double[]>>(d->arrList[arrName]);
            for (auto i = 0; i < dimSize; i++) {
                auto val = *(rawData->get() + i);
                if (min > val) min = val;
                if (max < val) max = val;
            }
        }
        else if (typeString == "arrFLOAT") {
            auto rawData = std::any_cast<std::shared_ptr<float[]>>(d->arrList[arrName]);
            for (auto i = 0; i < dimSize; i++) {
                auto val = *(rawData->get() + i);
                if (min > val) min = val;
                if (max < val) max = val;
            }
        }
        else if (typeString == "arrINT") {
            auto rawData = std::any_cast<std::shared_ptr<int[]>>(d->arrList[arrName]);
            for (auto i = 0; i < dimSize; i++) {
                auto val = *(rawData->get() + i);
                if (min > val) min = val;
                if (max < val) max = val;
            }
        }
        else if (typeString == "arrSHORT") {
            auto rawData = std::any_cast<std::shared_ptr<short[]>>(d->arrList[arrName]);
            for (auto i = 0; i < dimSize; i++) {
                auto val = *(rawData->get() + i);
                if (min > val) min = val;
                if (max < val) max = val;
            }
        }
        else if (typeString == "arrUINT") {
            auto rawData = std::any_cast<std::shared_ptr<uint32_t[]>>(d->arrList[arrName]);
            for (auto i = 0; i < dimSize; i++) {
                auto val = *(rawData->get() + i);
                if (min > val) min = val;
                if (max < val) max = val;
            }
        }
        else {
            return std::make_tuple(-1,1);
        }
        return std::make_tuple(min, max);
    }
    auto DataShareStorage::GetDimension(const QString& arrName) -> QList<int> {
        if(false == d->shapeList.contains(arrName)) {
            return QList<int>{};
        }
        return d->shapeList[arrName];
    }
    auto DataShareStorage::GetResolution(const QString& arrName) -> QList<double> {
        if(false == d->resolutionList.contains(arrName)) {
            return QList<double>{};
        }
        return d->resolutionList[arrName];
    }
    auto DataShareStorage::CopyImgArrToStorage(void* arr, QList<int> shape, QList<double> resolution, const QString& arrName, int type) -> bool {
        auto dimSize = 1;
        for(auto dim:shape) {
            dimSize *= dim;
        }
        QString typeString = QString(ArrType::_from_integral(type)._to_string());
        if(false == CopyBufferToStorage(arr, arrName,typeString , dimSize)) {
            return false;
        }
        d->shapeList[arrName] = shape;
        d->typeList[arrName] = typeString;
        d->resolutionList[arrName] = resolution;
        return true;
    }
    auto DataShareStorage::GetArrPtr(const QString& arrName) -> std::tuple<QList<int>,QList<double>, QString, std::any*> {
        if(false == d->arrList.contains(arrName)) {
            return std::make_tuple(QList<int>(), QList<double>(),QString(), nullptr);
        }
        return std::make_tuple(d->shapeList[arrName], d->resolutionList[arrName], d->typeList[arrName],d->arrList[arrName]);
    }
    auto DataShareStorage::FreeArr(const QString& arrName) -> bool {
        if(false == d->arrList.contains(arrName)) {
            return false;
        }
        d->arrList.remove(arrName);
        d->shapeList.remove(arrName);
        d->typeList.remove(arrName);
        d->resolutionList.remove(arrName);
        return true;
    }
    auto DataShareStorage::Clear() -> void {
        d->arrList.clear();
        d->shapeList.clear();
        d->typeList.clear();
        d->resolutionList.clear();
    }

    auto DataShareStorage::GetArrNameList() const -> QStringList {
        return d->arrList.keys();
    }
    auto DataShareStorage::CopyBufferFromStorage(std::any& anyPtr, void* source, QString typeString, unsigned dimSize) -> bool {
        if (typeString == "arrUSHORT") {
            auto rawData = std::shared_ptr<uint16_t[]>(new uint16_t[dimSize]);
            memcpy(rawData.get(), source, sizeof(uint16_t) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrUBYTE") {
            auto rawData = std::shared_ptr<uint8_t[]>(new uint8_t[dimSize]);
            memcpy(rawData.get(), source, sizeof(uint8_t) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrBYTE") {
            auto rawData = std::shared_ptr<int8_t[]>(new int8_t[dimSize]);
            memcpy(rawData.get(), source, sizeof(int8_t) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrDouble") {
            auto rawData = std::shared_ptr<double[]>(new double[dimSize]);
            memcpy(rawData.get(), source, sizeof(double) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrFLOAT") {
            auto rawData = std::shared_ptr<float[]>(new float[dimSize]);
            memcpy(rawData.get(), source, sizeof(float) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrINT") {
            auto rawData = std::shared_ptr<int[]>(new int[dimSize]);
            memcpy(rawData.get(), source, sizeof(int) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrSHORT") {
            auto rawData = std::shared_ptr<short[]>(new short[dimSize]);
            memcpy(rawData.get(), source, sizeof(short) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrUINT") {
            auto rawData = std::shared_ptr<uint32_t[]>(new uint32_t[dimSize]);
            memcpy(rawData.get(), source, sizeof(uint32_t) * dimSize);
            anyPtr = rawData;
        }
        else {
            return false;
        }
        return true;        
    }
    auto DataShareStorage::CopyBufferToStorage(void* source,QString arrName, QString typeString, unsigned dimSize) -> bool {
        std::any anyPtr;
        if (typeString == "arrUSHORT") {
            auto rawData = std::shared_ptr<uint16_t[]>(new uint16_t[dimSize]);
            memcpy(rawData.get(), source, sizeof(uint16_t) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrUBYTE") {
            auto rawData = std::shared_ptr<uint8_t[]>(new uint8_t[dimSize]);
            memcpy(rawData.get(), source, sizeof(uint8_t) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrBYTE") {
            auto rawData = std::shared_ptr<int8_t[]>(new int8_t[dimSize]);
            memcpy(rawData.get(), source, sizeof(int8_t) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrDouble") {
            auto rawData = std::shared_ptr<double[]>(new double[dimSize]);
            memcpy(rawData.get(), source, sizeof(double) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrFLOAT") {
            auto rawData = std::shared_ptr<float[]>(new float[dimSize]);
            memcpy(rawData.get(), source, sizeof(float) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrINT") {
            auto rawData = std::shared_ptr<int[]>(new int[dimSize]);
            memcpy(rawData.get(), source, sizeof(int) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrSHORT") {
            auto rawData = std::shared_ptr<short[]>(new short[dimSize]);
            memcpy(rawData.get(), source, sizeof(short) * dimSize);
            anyPtr = rawData;
        }
        else if (typeString == "arrUINT") {
            auto rawData = std::shared_ptr<uint32_t[]>(new uint32_t[dimSize]);
            memcpy(rawData.get(), source, sizeof(uint32_t) * dimSize);
            anyPtr = rawData;
        }
        else {
            return false;
        }
        d->arrList[arrName] = &anyPtr;
        return true;
    }

}