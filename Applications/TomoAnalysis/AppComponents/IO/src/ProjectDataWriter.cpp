#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>

#include "ProjectDataWriter.h"

namespace TomoAnalysis {
	ProjectDataWriter::ProjectDataWriter() {

	}

	ProjectDataWriter::~ProjectDataWriter() = default;

	auto ProjectDataWriter::WriteProjectData(const QString& path, const ProjectInfo::Pointer& projectInfo)->bool {
		if(true == path.isEmpty()) {
			return false;
		}

		if(nullptr == projectInfo) {
			return false;
		}

		QFile saveFile(path);
		if (false == saveFile.open(QIODevice::WriteOnly)) {
			qWarning("Couldn't open save file.");
			return false;
		}

		QJsonObject root;
		root["name"] = projectInfo->GetName();
		root["user"] = projectInfo->GetUser();
		root["createdDate"] = projectInfo->GetCreatedTime().toString("yyyyMMdd HH:mm:ss");
		root["modifiedDate"] = projectInfo->GetModifiedTime().toString("yyyyMMdd HH:mm:ss");
		root["comment"] = projectInfo->GetComment();
		root["root"] = projectInfo->IsRoot();
		root["tcfDirectorys"] = this->LoadTCFDirList(projectInfo->GetTCFDirList());

		auto worksets = projectInfo->GetWorksets();
		auto dirs = projectInfo->GetWorksetDirs();

		QJsonObject worksetJson;
		QJsonObject dirJson;

		for (const auto id : worksets.keys())
			worksetJson.insert(QString::number(id), worksets[id]);
		for (const auto id : dirs.keys()) {
			QJsonObject dirsJson;
			
			dirJson.insert(QString::number(id), LoadTCFDirList(dirs[id]));
		}

		if(!worksets.isEmpty())
			root["worksets"] = worksetJson;
		if (!dirs.isEmpty())
			root["worksetDirs"] = dirJson;
		
		auto json = QJsonDocument(root).toJson();
		if(-1 == saveFile.write(json)) {
			return false;
		}

	    return true;
	}

	auto ProjectDataWriter::WritePlaygroundData(const QString& path, const PlaygroundInfo::Pointer& playgroundInfo)->bool {
		if (true == path.isEmpty()) {
			return false;
		}

		if (nullptr == playgroundInfo) {
			return false;
		}

		QFile saveFile(path);
		if (false == saveFile.open(QIODevice::WriteOnly)) {
			qWarning("Couldn't open save file.");
			return false;
		}

		QJsonObject root;
		root["name"] = playgroundInfo->GetName();
		root["hypercubes"] = this->LoadHypercubeInfoList(playgroundInfo->GetHyperCubeList());
		root["cubes"] = this->LoadCubeInfoList(playgroundInfo->GetCubeList());
		root["tcfDirectorys"] = this->LoadTCFDirList(playgroundInfo->GetTCFDirList());
		root["applicationPaths"] = this->LoadMeasurementAppInfoList(playgroundInfo->GetAppList());

		if (-1 == saveFile.write(QJsonDocument(root).toJson())) {
			return false;
		}

		return true;
	}

	// private
	auto ProjectDataWriter::LoadTCFDirList(const TCFDir::List& list)->QJsonArray {
		QJsonArray array;
		for (const auto dir : list) {
			if (nullptr == dir) {
				continue;
			}

			QJsonObject obj;

			obj["path"] = dir->GetPath();
			obj["posX"] = dir->GetPosition().x();
			obj["posY"] = dir->GetPosition().y();
			
			array.append(obj);
		}

		return array;
	}

	auto ProjectDataWriter::LoadHypercubeInfoList(const HyperCube::List& list)->QJsonArray {
		auto LoadCubeIdList = [=](const Cube::List& cubeList) {
			QJsonArray array;
			for (auto cube : cubeList) {
			    if(nullptr == cube) {
					continue;
			    }

				array.append(cube->GetID());
			}

		    return array;
		};

	    QJsonArray array;
		for (const auto& hypercube : list) {
			if (nullptr == hypercube) {
				continue;
			}

			QJsonObject obj;

			obj["name"] = hypercube->GetName();
			obj["posX"] = hypercube->GetPosition().x();
			obj["posY"] = hypercube->GetPosition().y();
			obj["cubes"] = this->LoadCubeInfoList(hypercube->GetCubeList());//LoadCubeIdList(hypercube->GetCubeList());
			obj["apps"] = this->LoadMeasurementAppInfoList(hypercube->GetMeasurementAppInfoList());
			obj["results"] = this->LoadResultInfoList(hypercube->GetResultInfoList());

			array.append(obj);
		}

		return array;
	}

	auto ProjectDataWriter::LoadCubeInfoList(const Cube::List& list)->QJsonArray {
		QJsonArray array;
		for (const auto& cube : list) {
			if (nullptr == cube) {
				continue;
			}

			QJsonObject obj;

			obj["id"] = cube->GetID();
			obj["name"] = cube->GetName();
			obj["posX"] = cube->GetPosition().x();
			obj["posY"] = cube->GetPosition().y();
			obj["tcfDirectorys"] = this->LoadTCFDirList(cube->GetTCFDirList());
			obj["apps"] = this->LoadMeasurementAppInfoList(cube->GetMeasurementAppInfoList());
			obj["results"] = this->LoadResultInfoList(cube->GetResultInfoList());

			array.append(obj);
		}

		return array;
	}

	auto ProjectDataWriter::LoadMeasurementAppInfoList(const PluginAppInfo::List& list)->QJsonArray {
		QJsonArray array;
		for (const auto& app : list) {
			if (nullptr == app) {
				continue;
			}

			QJsonObject obj;
			obj["name"] = app->GetName();
			obj["posX"] = app->GetPosition().x();
			obj["posY"] = app->GetPosition().y();
			obj["path"] = app->GetPath();
			obj["singlerun"] = std::get<0>(app->GetAppType());
			obj["batchrun"] = std::get<1>(app->GetAppType());
			obj["processors"] = this->LoadProcessorInfoList(app->GetProcessors());

			array.append(obj);
		}

		return array;
	}

	auto ProjectDataWriter::LoadProcessorInfoList(const ProcessorInfo::List& list) -> QJsonArray {
		QJsonArray array;

		for(const auto& proc : list) {
		    if(nullptr == proc) {
				continue;
		    }
			QJsonObject obj;
			obj["name"] = proc->GetName();
			obj["path"] = proc->GetPath();
			QJsonArray paramArray;
			QMapIterator<QString, QString> iter(proc->GetParameterList());
			while(iter.hasNext()) {
				iter.next();
				QJsonObject paramObject;
				paramObject[iter.key()] = iter.value();
				paramArray.append(paramObject);
			}
			obj["parameters"] = paramArray;
			array.append(obj);
		}
		return array;
    }

    auto ProjectDataWriter::LoadResultInfoList(const ResultInfo::List& list) -> QJsonArray {
		QJsonArray array;

		for (const auto& result : list) {
			if (nullptr == result) {
				continue;
			}

			QJsonObject obj;
			obj["rawDataPath"] = result->GetPath();
			obj["masks"] = this->LoadMaskInfoList(result->GetMaskInfoList());
			obj["reportApps"] = this->LoadReportAppInfoList(result->GetReportAppInfoList());

			array.append(obj);
		}

		return array;
	}

	auto ProjectDataWriter::LoadMaskInfoList(const MaskInfo::List& list)->QJsonArray {
		QJsonArray array;
		for (const auto& mask : list) {
			if (nullptr == mask) {
				continue;
			}

			QJsonObject obj;
			obj["referencePath"] = mask->GetReferenceTCFPath();
			obj["maskPath"] = mask->GetMaskTCFPath();
			obj["interpretation"] = mask->GetInterpretation();

			array.append(obj);
		}

		return array;
	}

	auto ProjectDataWriter::LoadReportAppInfoList(const PluginAppInfo::List& list)->QJsonArray {
		QJsonArray array;
		for (const auto& app : list) {
			if (nullptr == app) {
				continue;
			}

			QJsonObject obj;
			obj["name"] = app->GetName();

			/*QJsonArray paramArray;
			QMapIterator<QString, QString> iter(app->GetProcessors().at(0)->GetParameterList());
			while (iter.hasNext()) {
				iter.next();

				QJsonObject paramObject;
				paramObject[iter.key()] = iter.value();
				paramArray.append(paramObject);
			}
			obj["parameters"] = paramArray;*/

			array.append(obj);
		}

		return array;
	}
}

