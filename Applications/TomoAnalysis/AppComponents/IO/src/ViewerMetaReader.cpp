#define LOGGER_TAG "[ViewerMetaIO]"

#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <TCLogger.h>

#include "ViewerMetaDefines.h"
#include "ViewerMetaReader.h"

namespace TomoAnalysis::AppComponents::IO {
    struct ViewerMetaReader::Impl {
        AppEntity::VisualizationInfo::Pointer visualization{ nullptr };
        AppEntity::AnnotationInfo::Pointer annotation{ nullptr };

        //parsing meta information for visualization
        auto ParseVisualizationMeta(const QJsonObject& json)->bool;

        auto ParseHTInformation(const QJsonObject& htObject)->void;
        auto ParseFLInformation(int chIdx, const QJsonObject& flObject)->void;
        auto Parse2DTransferFunction(const QJsonArray& tfArray)->void;
                
        //parsing meta information for annotation objects
        auto ParseAnnotationMeta(const QJsonObject& json)->bool;

        auto ParseScalarInformation(const QJsonObject& scalarObject)->void;
        auto ParseScaleInformation(const QJsonObject& scaleObject)->void;
        auto ParseTimestampInformation(const QJsonObject& timeObject)->void;
        auto ParseDeviceInformation(const QJsonObject& deviceObject)->void;
        auto ParseCameraXYInformation(const QJsonObject& camera2dObject)->void;
        auto ParseCamera3dInformation(const QJsonObject& camera3dObject)->void;
    };
    auto ViewerMetaReader::Impl::ParseVisualizationMeta(const QJsonObject& json) -> bool {
        auto informationExist = false;
        if(json.contains(Key::Visualization::RIMeta)) {
            ParseHTInformation(json[Key::Visualization::RIMeta].toObject());
            informationExist = true;
        }
        if(json.contains(Key::Visualization::FLCh1Meta)) {
            ParseFLInformation(0,json[Key::Visualization::FLCh1Meta].toObject());
            informationExist = true;
        }
        if (json.contains(Key::Visualization::FLCh2Meta)) {
            ParseFLInformation(1, json[Key::Visualization::FLCh2Meta].toObject());
            informationExist = true;
        }
        if (json.contains(Key::Visualization::FLCh3Meta)) {
            ParseFLInformation(2, json[Key::Visualization::FLCh3Meta].toObject());
            informationExist = true;
        }
        if(json.contains(Key::Visualization::RI2DTF)) {
            Parse2DTransferFunction(json[Key::Visualization::RI2DTF].toArray());
            informationExist = true;
        }
        if (json.contains(Key::Annotation::Camera2dMeta)) {
            ParseCameraXYInformation(json[Key::Annotation::Camera2dMeta].toObject());
            informationExist = true;
        }
        if (json.contains(Key::Annotation::Camera3dMeta)) {
            ParseCamera3dInformation(json[Key::Annotation::Camera3dMeta].toObject());
            informationExist = true;
        }
        return informationExist;
    }
    auto ViewerMetaReader::Impl::ParseHTInformation(const QJsonObject& htObject) -> void {
        AppEntity::ColormapInfo htInfo;        
        if(false == htObject.contains(Key::Visualization::TFColorMap)) {
            return;            
        }
        const auto colormapIdx = htObject[Key::Visualization::TFColorMap].toInt();
        htInfo.SetPredefinedColormap(colormapIdx);        
        if(false == htObject.contains(Key::Visualization::TFOpacity)) {
            return;
        }
        const auto opacity = htObject[Key::Visualization::TFOpacity].toDouble();
        htInfo.SetOpacity(opacity);
        if(false == htObject.contains(Key::Visualization::TFGamma)) {
            return;
        }
        const auto gamma = htObject[Key::Visualization::TFGamma].toDouble();
        htInfo.SetGamma(gamma);
        if(false == htObject.contains(Key::Visualization::GammaEnable)) {
            return;
        }
        const auto isGamma = htObject[Key::Visualization::GammaEnable].toBool();
        htInfo.SetIsGamma(isGamma);
        if(false == htObject.contains(Key::Visualization::TFVisible)) {
            return;            
        }
        const auto isVisible = htObject[Key::Visualization::TFVisible].toBool();
        htInfo.SetIsVisible(isVisible);
        if(false == htObject.contains(Key::Visualization::XRange)) {            
            return;
        }
        const auto xmin = htObject[Key::Visualization::XRange].toArray()[0].toDouble();
        const auto xmax = htObject[Key::Visualization::XRange].toArray()[1].toDouble();
        htInfo.SetXRange(xmin, xmax);

        visualization->SetIsRiInfoExist(true);
        visualization->SetHTInfo(htInfo);
    }
    auto ViewerMetaReader::Impl::ParseFLInformation(int chIdx, const QJsonObject& flObject) -> void {
        AppEntity::ColormapInfo flInfo;        
        if(false == flObject.contains(Key::Visualization::TFColor)) {
            return;            
        }
        const auto color = flObject[Key::Visualization::TFColor].toArray();
        flInfo.SetRgb(color[0].toDouble(), color[1].toDouble(), color[2].toDouble());
        if(false == flObject.contains(Key::Visualization::TFOpacity)) {
            return;
        }
        const auto opacity = flObject[Key::Visualization::TFOpacity].toDouble();
        flInfo.SetOpacity(opacity);
        if(false == flObject.contains(Key::Visualization::TFGamma)) {
            return;
        }
        const auto gamma = flObject[Key::Visualization::TFGamma].toDouble();
        flInfo.SetGamma(gamma);
        if(false == flObject.contains(Key::Visualization::GammaEnable)) {
            return;
        }
        const auto isGamma = flObject[Key::Visualization::GammaEnable].toBool();
        flInfo.SetIsGamma(isGamma);
        if(false == flObject.contains(Key::Visualization::TFVisible)) {
            return;
        }
        const auto isVisible = flObject[Key::Visualization::TFVisible].toBool();
        flInfo.SetIsVisible(isVisible);
        if(false == flObject.contains(Key::Visualization::XRange)) {
            return;
        }
        const auto xmin = flObject[Key::Visualization::XRange].toArray()[0].toDouble();
        const auto xmax = flObject[Key::Visualization::XRange].toArray()[1].toDouble();
        flInfo.SetXRange(xmin, xmax);

        visualization->SetIsFLInfoExist(chIdx, true);
        visualization->SetFLInfo(chIdx, flInfo);
    }
    auto ViewerMetaReader::Impl::Parse2DTransferFunction(const QJsonArray& tfArray) -> void {
        for(auto i=0;i<tfArray.count();i++) {
            auto singleObject = tfArray[i].toObject();
            AppEntity::ColormapInfo tfBox;
            if(false == singleObject.contains(Key::Visualization::XRange)) {
                continue;
            }
            auto xmin = singleObject[Key::Visualization::XRange].toArray()[0].toDouble();
            auto xmax = singleObject[Key::Visualization::XRange].toArray()[1].toDouble();
            tfBox.SetXRange(xmin, xmax);
            if(false == singleObject.contains(Key::Visualization::YRange)) {
                continue;
            }
            auto ymin = singleObject[Key::Visualization::YRange].toArray()[0].toDouble();
            auto ymax = singleObject[Key::Visualization::YRange].toArray()[1].toDouble();
            tfBox.SetYRange(ymin, ymax);
            if(false == singleObject.contains(Key::Visualization::TFColor)) {
                continue;
            }
            auto color = singleObject[Key::Visualization::TFColor].toArray();
            tfBox.SetRgb(color[0].toDouble(), color[1].toDouble(), color[2].toDouble());
            if(false == singleObject.contains(Key::Visualization::TFOpacity)) {
                continue;
            }
            auto opacity = singleObject[Key::Visualization::TFOpacity].toDouble();
            tfBox.SetOpacity(opacity);
            if(false == singleObject.contains(Key::Visualization::TFGamma)) {
                continue;
            }
            auto gamma = singleObject[Key::Visualization::TFGamma].toDouble();
            tfBox.SetGamma(gamma);
            if(false == singleObject.contains(Key::Visualization::GammaEnable)) {
                continue;
            }
            auto isGamma = singleObject[Key::Visualization::GammaEnable].toBool();
            tfBox.SetIsGamma(isGamma);
            if(false == singleObject.contains(Key::Visualization::TFVisible)) {
                continue;
            }
            auto isVisible = singleObject[Key::Visualization::TFVisible].toBool();
            tfBox.SetIsVisible(isVisible);

            visualization->AppendTF(tfBox);
        }        
    }


    auto ViewerMetaReader::Impl::ParseAnnotationMeta(const QJsonObject& json) -> bool {
        auto informationExist = false;
        if(json.contains(Key::Annotation::ScalarMeta)) {
            ParseScalarInformation(json[Key::Annotation::ScalarMeta].toObject());
            informationExist = true;
        }
        if(json.contains(Key::Annotation::ScaleMeta)) {
            ParseScaleInformation(json[Key::Annotation::ScaleMeta].toObject());
            informationExist = true;
        }
        if(json.contains(Key::Annotation::TimeMeta)) {
            ParseTimestampInformation(json[Key::Annotation::TimeMeta].toObject());
            informationExist = true;
        }
        if(json.contains(Key::Annotation::DeviceMeta)) {
            ParseDeviceInformation(json[Key::Annotation::DeviceMeta].toObject());
            informationExist = true;
        }        
        return informationExist;
    }
    auto ViewerMetaReader::Impl::ParseScalarInformation(const QJsonObject& scalarObject) -> void {
        AppEntity::ScalarBarInfo meta;
        if(false == scalarObject.contains(Key::Annotation::annoPosition)) {
            return;
        }
        auto pos = scalarObject[Key::Annotation::annoPosition].toArray();
        meta.SetCoordinate(pos[0].toDouble(), pos[1].toDouble());
        if(false == scalarObject.contains(Key::Annotation::annoOrientation)) {
            return;
        }
        auto orientation = scalarObject[Key::Annotation::annoOrientation].toInt();
        meta.SetOrientation(static_cast<AppEntity::Orientation>(orientation));
        if(false == scalarObject.contains(Key::Annotation::annoVisible)) {
            return;
        }
        auto isVisible = scalarObject[Key::Annotation::annoVisible].toBool();
        meta.SetIsVisible(isVisible);

        annotation->SetScalarMetaInfo(meta);
    }
    auto ViewerMetaReader::Impl::ParseScaleInformation(const QJsonObject& scaleObject) -> void {
        AppEntity::ScaleBarInfo meta;
        if(false == scaleObject.contains(Key::Annotation::annoPosition)) {
            return;
        }
        auto pos = scaleObject[Key::Annotation::annoPosition].toArray();
        meta.SetCoordinate(pos[0].toDouble(), pos[1].toDouble());
        if(false == scaleObject.contains(Key::Annotation::annoColor)) {
            return;
        }
        auto color = scaleObject[Key::Annotation::annoColor].toArray();
        meta.SetColor(color[0].toDouble(), color[1].toDouble(), color[2].toDouble());
        if(false == scaleObject.contains(Key::Annotation::annoOrientation)) {
            return;
        }
        auto orientation = scaleObject[Key::Annotation::annoOrientation].toInt();
        meta.SetOrientation(static_cast<AppEntity::Orientation>(orientation));
        if(false == scaleObject.contains(Key::Annotation::scaleLength)) {
            return;
        }
        auto length = scaleObject[Key::Annotation::scaleLength].toDouble();
        meta.SetLength(length);
        if(false == scaleObject.contains(Key::Annotation::scaleTick)) {
            return;
        }
        auto tick = scaleObject[Key::Annotation::scaleTick].toDouble();
        meta.SetTick(tick);
        if(false == scaleObject.contains(Key::Annotation::annoTextVisible)) {
            return;
        }
        auto isTextVisible = scaleObject[Key::Annotation::annoTextVisible].toBool();
        meta.SetIsTextVisible(isTextVisible);
        if(false == scaleObject.contains(Key::Annotation::annoVisible)) {
            return;
        }
        auto isVisible = scaleObject[Key::Annotation::annoVisible].toBool();
        meta.SetIsVisible(isVisible);

        annotation->SetScaleMetaInfo(meta);
    }
    auto ViewerMetaReader::Impl::ParseTimestampInformation(const QJsonObject& timeObject) -> void {
        AppEntity::TimeStampInfo meta;
        if(false == timeObject.contains(Key::Annotation::annoPosition)) {
            return;
        }
        auto pos = timeObject[Key::Annotation::annoPosition].toArray();
        meta.SetCoordinate(pos[0].toDouble(), pos[1].toDouble());
        if(false == timeObject.contains(Key::Annotation::annoColor)) {
            return;
        }
        auto color = timeObject[Key::Annotation::annoColor].toArray();
        meta.SetColor(color[0].toDouble(), color[1].toDouble(), color[2].toDouble());
        if(false == timeObject.contains(Key::Annotation::annoFontSize)) {
            return;
        }
        auto fontSize = timeObject[Key::Annotation::annoFontSize].toDouble();
        meta.SetFontSize(fontSize);
        if(false == timeObject.contains(Key::Annotation::annoVisible)) {
            return;
        }
        auto isVisible = timeObject[Key::Annotation::annoVisible].toBool();
        meta.SetIsVisible(isVisible);

        annotation->SetTimeMetaInfo(meta);
    }
    auto ViewerMetaReader::Impl::ParseDeviceInformation(const QJsonObject& deviceObject) -> void {
        AppEntity::DeviceInfo meta;
        if(false == deviceObject.contains(Key::Annotation::annoColor)) {
            return;
        }
        auto color = deviceObject[Key::Annotation::annoColor].toArray();
        meta.SetColor(color[0].toDouble(), color[1].toDouble(), color[2].toDouble());
        if(false == deviceObject.contains(Key::Annotation::annoFontSize)) {
            return;
        }
        auto fontSize = deviceObject[Key::Annotation::annoFontSize].toDouble();
        meta.SetFontSize(fontSize);
        if(false == deviceObject.contains(Key::Annotation::annoVisible)) {
            return;
        }
        auto isVisible = deviceObject[Key::Annotation::annoVisible].toBool();
        meta.SetIsVisible(isVisible);

        annotation->SetDeviceMetaInfo(meta);
    }
    auto ViewerMetaReader::Impl::ParseCameraXYInformation(const QJsonObject& camera2dObject) -> void {
        AppEntity::Camera2DInfo meta;
        if(false == camera2dObject.contains(Key::Annotation::camPos)) {
            return;
        }
        auto pos = camera2dObject[Key::Annotation::camPos].toArray();
        meta.SetPosition(pos[0].toDouble(), pos[1].toDouble(), pos[2].toDouble());
        if(false == camera2dObject.contains(Key::Annotation::camHeight)) {
            return;
        }
        auto height = camera2dObject[Key::Annotation::camHeight].toDouble();
        meta.SetHeight(height);

        visualization->SetCamera2dMetaInfo(meta);
    }
    auto ViewerMetaReader::Impl::ParseCamera3dInformation(const QJsonObject& camera3dObject) -> void {
        AppEntity::Camera3DInfo meta;
        if(false == camera3dObject.contains(Key::Annotation::camPos)) {
            return;
        }
        auto pos = camera3dObject[Key::Annotation::camPos].toArray();
        meta.SetPosition(pos[0].toDouble(), pos[1].toDouble(), pos[2].toDouble());
        if(false == camera3dObject.contains(Key::Annotation::camHeightAngle)) {
            return;
        }
        auto heightAngle = camera3dObject[Key::Annotation::camHeightAngle].toDouble();
        meta.SetHeightAngle(heightAngle);
        if(false == camera3dObject.contains(Key::Annotation::camDir)) {
            return;
        }
        auto direction = camera3dObject[Key::Annotation::camDir].toArray();
        meta.SetDirection(direction[0].toDouble(), direction[1].toDouble(), direction[2].toDouble(), direction[3].toDouble());

        visualization->SetCamera3dMetaInfo(meta);
    }
    ViewerMetaReader::ViewerMetaReader() : d{ new Impl } {
        
    }
    ViewerMetaReader::~ViewerMetaReader() {
        
    }
    auto ViewerMetaReader::ReadVizInfo(const QString& path) const -> AppEntity::VisualizationInfo::Pointer {
        if(path.isEmpty()) {
            QLOG_ERROR() << "Visualization meta file path is not specified";
            return nullptr;
        }
        QFile file(path);
        if(false == file.open(QIODevice::ReadOnly)) {
            QLOG_ERROR() << "Not able to open visualization meta file : " << path;
            return nullptr;
        }
        d->visualization = std::make_shared<AppEntity::VisualizationInfo>();

        const auto readData = file.readAll();
        const QJsonDocument jsonDoc(QJsonDocument::fromJson(readData));
        const auto json = jsonDoc.object();

        if(false == d->ParseVisualizationMeta(json)) {
            QLOG_ERROR() << "Failed to parse visualization meta information";
            return nullptr;
        }
        
        return d->visualization;
    }
    auto ViewerMetaReader::ReadAnnoInfo(const QString& path) const -> AppEntity::AnnotationInfo::Pointer {
        if(path.isEmpty()) {
            QLOG_ERROR() << "Annotation meta file path is not specified";
            return nullptr;
        }
        QFile file(path);
        if(false == file.open(QIODevice::ReadOnly)) {
            QLOG_ERROR() << "Not able to open annotation meta file : " << path;
            return nullptr;
        }
        d->annotation = std::make_shared<AppEntity::AnnotationInfo>();

        const auto readData = file.readAll();
        const QJsonDocument jsonDoc(QJsonDocument::fromJson(readData));
        const auto json = jsonDoc.object();

        if(false == d->ParseAnnotationMeta(json)) {
            QLOG_ERROR() << "Filed to parse annotation meta information";
            return nullptr;
        }
        return d->annotation;
    }
}