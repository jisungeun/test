#define LOGGER_TAG "[ViewerMetaIO]"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <TCLogger.h>

#include "ViewerMetaDefines.h"
#include "ViewerMetaWriter.h"

#include <iostream>

namespace TomoAnalysis::AppComponents::IO {
    struct ViewerMetaWriter::Impl {
        AppEntity::VisualizationInfo::Pointer visualization{ nullptr };
        AppEntity::AnnotationInfo::Pointer annotation{ nullptr };

        auto TransformVisualizationIntoJson()const->QJsonObject;
        auto TransformHTintoJson()const->QJsonObject;
        auto TransformFLIntoJson(int chIdx)const->QJsonObject;
        auto TransformTF2dIntoJson()const->QJsonArray;

        auto TransformAnnotationIntoJson()const->QJsonObject;
        auto TransformScalarIntoJson()const->QJsonObject;
        auto TransformScaleIntoJson()const->QJsonObject;
        auto TransformTimestampIntoJson()const->QJsonObject;
        auto TransformDeviceIntoJson()const->QJsonObject;
        auto TransformCameraXYIntoJson()const->QJsonObject;
        auto TransformCamera3dIntoJson()const->QJsonObject;
    };
    auto ViewerMetaWriter::Impl::TransformVisualizationIntoJson() const -> QJsonObject {
        QJsonObject json;
        if (visualization->GetIsRiInfoExist()) {
            json[Key::Visualization::RIMeta] = TransformHTintoJson();
        }
        if (visualization->GetIsFLInfoExist(0)) {
            json[Key::Visualization::FLCh1Meta] = TransformFLIntoJson(0);
        }
        if (visualization->GetIsFLInfoExist(1)) {
            json[Key::Visualization::FLCh2Meta] = TransformFLIntoJson(1);
        }
        if (visualization->GetIsFLInfoExist(2)) {
            json[Key::Visualization::FLCh3Meta] = TransformFLIntoJson(2);
        }
        if (visualization->GetTF2dList().count() > 0) {
            json[Key::Visualization::RI2DTF] = TransformTF2dIntoJson();
        }
        if (visualization->hasCamera2dMetaInfo()) {
            json[Key::Annotation::Camera2dMeta] = TransformCameraXYIntoJson();
        }
        if (visualization->hasCamera3dMetaInfo()) {
            json[Key::Annotation::Camera3dMeta] = TransformCamera3dIntoJson();
        }
        return json;
    }
    auto ViewerMetaWriter::Impl::TransformHTintoJson() const -> QJsonObject {
        QJsonObject jsonObj;
        const auto htInfo = visualization->GetHTInfo();
        jsonObj[Key::Visualization::TFColorMap] = htInfo.GetPredefinedColormap();
        jsonObj[Key::Visualization::TFGamma] = htInfo.GetGamma();
        jsonObj[Key::Visualization::GammaEnable] = htInfo.isGamma();
        jsonObj[Key::Visualization::TFOpacity] = htInfo.GetOpacity();
        jsonObj[Key::Visualization::TFVisible] = htInfo.isVisible();
        const auto xRange = htInfo.GetXRange();
        jsonObj[Key::Visualization::XRange] = QJsonArray{ std::get<0>(xRange),std::get<1>(xRange) };
        return jsonObj;
    }
    auto ViewerMetaWriter::Impl::TransformFLIntoJson(int chIdx)const -> QJsonObject {
        QJsonObject jsonObj;
        const auto flInfo = visualization->GetFLInfo(chIdx);
        const auto color = flInfo.GetColor();
        jsonObj[Key::Visualization::TFColor] = QJsonArray{ std::get<0>(color),std::get<1>(color),std::get<2>(color) };
        jsonObj[Key::Visualization::TFGamma] = flInfo.GetGamma();
        jsonObj[Key::Visualization::GammaEnable] = flInfo.isGamma();
        jsonObj[Key::Visualization::TFOpacity] = flInfo.GetOpacity();
        jsonObj[Key::Visualization::TFVisible] = flInfo.isVisible();
        const auto xRange = flInfo.GetXRange();
        jsonObj[Key::Visualization::XRange] = QJsonArray{ std::get<0>(xRange),std::get<1>(xRange) };
        return jsonObj;
    }
    auto ViewerMetaWriter::Impl::TransformTF2dIntoJson() const -> QJsonArray {
        QJsonArray jsonArr;
        const auto tfList = visualization->GetTF2dList();
        for (auto i = 0; i < tfList.count(); i++) {
            QJsonObject jsonItem;
            const auto tfItem = tfList[i];
            auto color = tfItem.GetColor();
            jsonItem[Key::Visualization::TFColor] = QJsonArray{ std::get<0>(color),std::get<1>(color),std::get<2>(color) };
            jsonItem[Key::Visualization::TFGamma] = tfItem.GetGamma();
            jsonItem[Key::Visualization::GammaEnable] = tfItem.isGamma();
            jsonItem[Key::Visualization::TFOpacity] = tfItem.GetOpacity();
            jsonItem[Key::Visualization::TFVisible] = tfItem.isVisible();
            auto xRange = tfItem.GetXRange();
            jsonItem[Key::Visualization::XRange] = QJsonArray{ std::get<0>(xRange),std::get<1>(xRange) };
            auto yRange = tfItem.GetYRange();
            jsonItem[Key::Visualization::YRange] = QJsonArray{ std::get<0>(yRange),std::get<1>(yRange) };
            jsonArr.append(jsonItem);
        }
        return jsonArr;
    }

    auto ViewerMetaWriter::Impl::TransformAnnotationIntoJson() const -> QJsonObject {
        QJsonObject json;
        if (annotation->hasScalarMetaInfo()) {
            json[Key::Annotation::ScalarMeta] = TransformScalarIntoJson();
        }
        if (annotation->hasScaleMetaInfo()) {
            json[Key::Annotation::ScaleMeta] = TransformScaleIntoJson();
        }
        if (annotation->hasTimeMetaInfo()) {
            json[Key::Annotation::TimeMeta] = TransformTimestampIntoJson();
        }
        if (annotation->hasDeviceMetaInfo()) {
            json[Key::Annotation::DeviceMeta] = TransformDeviceIntoJson();
        }
        return json;
    }
    auto ViewerMetaWriter::Impl::TransformScalarIntoJson() const -> QJsonObject {
        QJsonObject jsonObj;
        const auto meta = annotation->GetScalarMetaInfo();
        const auto pos = meta.GetCoordinate();
        jsonObj[Key::Annotation::annoPosition] = QJsonArray{ std::get<0>(pos),std::get<1>(pos) };
        jsonObj[Key::Annotation::annoOrientation] = meta.GetOrientation();
        jsonObj[Key::Annotation::annoVisible] = meta.isVisible();
        return jsonObj;
    }
    auto ViewerMetaWriter::Impl::TransformScaleIntoJson() const -> QJsonObject {
        QJsonObject jsonObj;
        const auto meta = annotation->GetScaleMetaInfo();
        const auto pos = meta.GetCoordinate();
        jsonObj[Key::Annotation::annoPosition] = QJsonArray{ std::get<0>(pos),std::get<1>(pos) };
        const auto color = meta.GetColor();
        jsonObj[Key::Annotation::annoColor] = QJsonArray{ std::get<0>(color),std::get<1>(color),std::get<2>(color) };
        jsonObj[Key::Annotation::annoOrientation] = meta.GetOrientation();
        jsonObj[Key::Annotation::scaleLength] = meta.GetLength();
        jsonObj[Key::Annotation::scaleTick] = meta.GetTick();
        jsonObj[Key::Annotation::annoTextVisible] = meta.isTextVisible();
        jsonObj[Key::Annotation::annoVisible] = meta.isVisible();
        return jsonObj;
    }
    auto ViewerMetaWriter::Impl::TransformTimestampIntoJson() const -> QJsonObject {
        QJsonObject jsonObj;
        const auto meta = annotation->GetTimeMetaInfo();
        const auto pos = meta.GetCoordinate();
        jsonObj[Key::Annotation::annoPosition] = QJsonArray{ std::get<0>(pos),std::get<1>(pos) };
        const auto color = meta.GetColor();
        jsonObj[Key::Annotation::annoColor] = QJsonArray{ std::get<0>(color),std::get<1>(color),std::get<2>(color) };
        jsonObj[Key::Annotation::annoFontSize] = meta.GetFontSize();
        jsonObj[Key::Annotation::annoVisible] = meta.isVisible();
        return jsonObj;
    }
    auto ViewerMetaWriter::Impl::TransformDeviceIntoJson() const -> QJsonObject {
        QJsonObject jsonObj;
        const auto meta = annotation->GetDeviceMetaInfo();
        const auto color = meta.GetColor();
        jsonObj[Key::Annotation::annoColor] = QJsonArray{ std::get<0>(color),std::get<1>(color),std::get<2>(color) };
        jsonObj[Key::Annotation::annoFontSize] = meta.GetFontSize();
        jsonObj[Key::Annotation::annoVisible] = meta.isVisible();
        return jsonObj;
    }
    auto ViewerMetaWriter::Impl::TransformCameraXYIntoJson() const -> QJsonObject {
        QJsonObject jsonObj;
        const auto meta = visualization->GetCamera2dMetaInfo();
        const auto pos = meta.GetPosition();
        jsonObj[Key::Annotation::camPos] = QJsonArray{ std::get<0>(pos),std::get<1>(pos),std::get<2>(pos) };
        jsonObj[Key::Annotation::camHeight] = meta.GetHeight();
        return jsonObj;
    }
    auto ViewerMetaWriter::Impl::TransformCamera3dIntoJson() const -> QJsonObject {
        QJsonObject jsonObj;
        const auto meta = visualization->GetCamera3dMetaInfo();
        const auto pos = meta.GetPosition();
        jsonObj[Key::Annotation::camPos] = QJsonArray{ std::get<0>(pos),std::get<1>(pos),std::get<2>(pos) };
        jsonObj[Key::Annotation::camHeightAngle] = meta.GetHeightAngle();
        const auto direction = meta.GetDirection();
        jsonObj[Key::Annotation::camDir] = QJsonArray{ std::get<0>(direction),std::get<1>(direction),std::get<2>(direction),std::get<3>(direction) };
        return jsonObj;
    }

    ViewerMetaWriter::ViewerMetaWriter() : d{ new Impl } {

    }
    ViewerMetaWriter::~ViewerMetaWriter() {

    }
    auto ViewerMetaWriter::WriteAnnotationMeta(const QString& path, const AppEntity::AnnotationInfo::Pointer& meta) const -> bool {
        if (path.isEmpty()) {
            QLOG_ERROR() << "Annotation meta output path is empty";
            return false;
        }
        if (nullptr == meta) {
            QLOG_ERROR() << "Annotation meta information is nullptr";
            return false;
        }
        d->annotation = meta;
        auto json = d->TransformAnnotationIntoJson();
        if (json.isEmpty()) {
            QLOG_ERROR() << "Failed to transform annotation meta information into json format";
            return false;
        }
        QFileInfo fileInfo(path);
        QDir fileDir(fileInfo.absolutePath());
        if (false == fileDir.exists()) {
            fileDir.mkpath(fileInfo.absolutePath());
        }

        QFile file(path);
        if (false == file.open(QIODevice::WriteOnly)) {
            QLOG_ERROR() << "Falied to open target file : " << path;
            return false;
        }

        if (file.write(QJsonDocument(json).toJson()) < -1) {
            QLOG_ERROR() << "Failed to write meta information as json format";
            return false;
        }

        return true;
    }
    auto ViewerMetaWriter::WriteVisualizationMeta(const QString& path, const AppEntity::VisualizationInfo::Pointer& meta) const -> bool {
        if (path.isEmpty()) {
            QLOG_ERROR() << "Visualization meta output path is empty";
            return false;
        }
        if (nullptr == meta) {
            QLOG_ERROR() << "Visualization meta information is nullptr";
            return false;
        }
        d->visualization = meta;
        auto json = d->TransformVisualizationIntoJson();
        if (json.isEmpty()) {
            QLOG_ERROR() << "Failed to transform visualization meta information into json format";
            return false;
        }

        QFileInfo fileInfo(path);
        QDir fileDir(fileInfo.absolutePath());
        if (false == fileDir.exists()) {
            fileDir.mkpath(fileInfo.absolutePath());
        }

        QFile file(path);
        if (false == file.open(QIODevice::WriteOnly)) {
            QLOG_ERROR() << "Falied to open target file : " << path;
            return false;
        }

        if (file.write(QJsonDocument(json).toJson()) < -1) {
            QLOG_ERROR() << "Failed to write meta information as json format";
            return false;
        }

        return true;
    }
}