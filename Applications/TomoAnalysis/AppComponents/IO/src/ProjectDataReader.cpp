#include <iostream>

#include <QDirIterator>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>

#include "ProjectDataReader.h"

namespace TomoAnalysis {
	ProjectDataReader::ProjectDataReader() {

	}

	ProjectDataReader::~ProjectDataReader() = default;

	auto ProjectDataReader::ReadProjectData(const QString& path, ProjectInfo::Pointer& projectInfo)->bool {
		if(true == path.isEmpty()) {
			return false;
		}

	    QFile loadFile(path);

		if (false == loadFile.open(QIODevice::ReadOnly)) {
			qWarning("Could not open json file to read");
			return false;
		}

        const QByteArray data = loadFile.readAll();
        const QJsonDocument doc(QJsonDocument::fromJson(data));
		auto object = doc.object();

        projectInfo->SetPath(path);
		projectInfo->SetName(object["name"].toString());
		projectInfo->SetUser(object["user"].toString());
		projectInfo->SetCreatedTime(QDateTime::fromString(object["createdDate"].toString(), "yyyyMMdd HH:mm:ss"));
		projectInfo->SetModifiedTime(QDateTime::fromString(object["modifiedDate"].toString(), "yyyyMMdd HH:mm:ss"));
		projectInfo->SetComment(object["comment"].toString());
        projectInfo->SetRoot(object["root"].toBool());

	    const auto tcfDirectorys = object["tcfDirectorys"].toArray();
		projectInfo->SetTCFDirList(this->MakeTCFDirList(tcfDirectorys));

        if (object.contains("worksets")) {
            const auto worksets = object["worksets"].toObject().toVariantMap();
            for (const auto& id : worksets.keys())
                projectInfo->AddWorkset(worksets[id].toString(), id.toInt());
        }

        if (object.contains("worksetDirs")) {
            const auto worksetDirs = object["worksetDirs"].toObject().toVariantMap();
            for (const auto& id : worksetDirs.keys())
                projectInfo->LinkTcfDirsToWorkset(id.toInt(), this->MakeTCFDirList(worksetDirs[id].toJsonArray()));
        }

		return true;
	}
    
	auto ProjectDataReader::ReadPlaygroundData(const QString& path, PlaygroundInfo::Pointer& playgroundInfo)->bool {
		if (true == path.isEmpty()) {
			return false;
		}

		QFile loadFile(path);

		if (false == loadFile.open(QIODevice::ReadOnly)) {
			qWarning("Could not open json file to read");
			return false;
		}

		const QByteArray data = loadFile.readAll();
        const  QJsonDocument doc(QJsonDocument::fromJson(data));
		auto object = doc.object();

        playgroundInfo->SetPath(path);
        playgroundInfo->SetName(object["name"].toString());
        playgroundInfo->SetCubeList(this->MakeCubeInfoList(object["cubes"].toArray()));
        playgroundInfo->SetHyperCubeList(this->MakeHypercubeInfoList(object["hypercubes"].toArray(), 
                                                                     playgroundInfo->GetCubeList()));

        const auto tcfDirectorys = object["tcfDirectorys"].toArray();
        playgroundInfo->SetTCFDirList(this->MakeTCFDirList(tcfDirectorys));
        const auto appPaths = object["applicationPaths"].toArray();
        playgroundInfo->SetAppList(this->MakeMeasurementAppInfoList(appPaths));

        return true;
	}

    auto ProjectDataReader::MakeTCFDirList(const QJsonArray& array) const ->TCFDir::List {
        TCFDir::List list;        
        for (const auto& item : array) {
            auto path = item["path"];
            QDir dir(path.toString());
            QFileInfo fileInfo(path.toString());
            if (false == dir.exists() && false == fileInfo.exists()) {
                continue;
            }

            auto tcfDir = std::make_shared<TCFDir>();
            tcfDir->SetPath(path.toString());

            auto posX = item["posX"].toInt();
            auto posY = item["posY"].toInt();
            tcfDir->SetPosition(QPoint(posX, posY));

            list.append(tcfDir);
        }

        return list;
	}
    
    auto ProjectDataReader::MakeHypercubeInfoList(const QJsonArray& array, const Cube::List& cubeList)->HyperCube::List {
        Q_UNUSED(cubeList)
        auto FindCube = [=](const Cube::ID& id, const Cube::List& cubes) {
            for (auto cube : cubes) {
                if (id == cube->GetID()) {
                    return cube;
                }
            }

            return std::make_shared<Cube>();
        };

        auto MakeCubeIdList = [=](const QJsonArray& array, const Cube::List& cubes) {
            Cube::List list;

            for (const auto& item : array) {
                auto cube = FindCube(item.toInt(-1), cubes);
                if (nullptr == cube) {
                    continue;
                }

                list.append(cube);
            }
            return list;
        };


	    HyperCube::List list;

        for (const auto& item : array) {
            auto hypercube = std::make_shared<HyperCube>();
            hypercube->SetName(item["name"].toString());
            auto posX = item["posX"].toInt();
            auto posY = item["posY"].toInt();
            hypercube->SetPosition(QPoint(posX, posY));
            //hypercube->SetCubeList(MakeCubeIdList(item["cubes"].toArray(), cubeList));
            hypercube->SetCubeList(this->MakeCubeInfoList(item["cubes"].toArray()));
            hypercube->SetMeasurementAppInfoList(this->MakeMeasurementAppInfoList(item["apps"].toArray()));
            hypercube->SetResultInfoList(this->MakeResultInfoList(item["results"].toArray()));

            list.append(hypercube);
        }

        return list;
    }

    auto ProjectDataReader::MakeCubeInfoList(const QJsonArray& array) const ->Cube::List {
        Cube::List list;

        for(const auto& item : array) {            
            auto cube = std::make_shared<Cube>();
            cube->SetID(item["id"].toInt(-1));
            cube->SetName(item["name"].toString());
            auto posX = item["posX"].toInt();
            auto posY = item["posY"].toInt();
            cube->SetPosition(QPoint(posX, posY));
            cube->SetTCFDirList(this->MakeTCFDirList(item["tcfDirectorys"].toArray()));
            cube->SetMeasurementAppInfoList(this->MakeMeasurementAppInfoList(item["apps"].toArray()));
            cube->SetResultInfoList(this->MakeResultInfoList(item["results"].toArray()));

            list.append(cube);
        }

        return list;
    }

    auto ProjectDataReader::MakeMeasurementAppInfoList(const QJsonArray& array) const ->PluginAppInfo::List {
        PluginAppInfo::List list;

        for(const auto& item : array) {
            auto app = std::make_shared<PluginAppInfo>();
            app->SetName(item["name"].toString());
            app->SetPath(item["path"].toString());
            auto posX = item["posX"].toInt();
            auto posY = item["posY"].toInt();
            app->SetPosition(QPoint(posX, posY));
            auto si = item["singlerun"];            
            auto isSingleRun = true;
            if(si.isBool()) {
                isSingleRun = item["singlerun"].toBool();
            }

            auto ba = item["batchrun"];
            auto isBatchRun = true;
            if(ba.isBool()) {
                isBatchRun = item["batchrun"].toBool();
            }
            app->SetAppType(std::make_tuple(isSingleRun,isBatchRun));

            //processor
            auto procList = this->MakeProcessorInfoList(item["processors"].toArray());
            app->SetProcessors(procList);

            list.append(app);
        }

        return list;
    }

    auto ProjectDataReader::MakeProcessorInfoList(const QJsonArray& array) const -> ProcessorInfo::List {
        ProcessorInfo::List list;
        for(const auto& item : array) {
            auto proc = std::make_shared<ProcessorInfo>();
            proc->SetName(item["name"].toString());
            proc->SetPath(item["path"].toString());

            QMap<QString, QString> paramterList;
            auto parameters = item["paramters"].toObject();

            for(const auto& key : parameters.keys()) {
                paramterList[key] = parameters[key].toString();
            }
            proc->SetParameterList(paramterList);
            list.append(proc);
        }
        return list;
    }

    auto ProjectDataReader::MakeResultInfoList(const QJsonArray& array) const ->ResultInfo::List {
        ResultInfo::List list;

        for(const auto& item : array) {
            auto result = std::make_shared<ResultInfo>();
            result->SetPath(item["rawDataPath"].toString());
            result->SetMaskInfoList(this->MakeMaskInfoList(item["masks"].toArray()));
            result->SetReportAppInfoList(this->MakeReportAppInfoList(item["reportApps"].toArray()));

            list.append(result);
        }

        return list;
    }

    auto ProjectDataReader::MakeMaskInfoList(const QJsonArray& array) const ->MaskInfo::List {
        MaskInfo::List list;

        for(const auto& item : array) {
            auto mask = std::make_shared<MaskInfo>();
            mask->SetReferenceTCFPath(item["referencePath"].toString());
            mask->SetMaskTCFPath(item["maskPath"].toString());
            mask->SetInterpretation(item["interpretation"].toString());

            list.append(mask);
        }

        return list;
    }

    auto ProjectDataReader::MakeReportAppInfoList(const QJsonArray& array) const ->PluginAppInfo::List {
        PluginAppInfo::List list;

        for (auto item : array) {
            auto app = std::make_shared<PluginAppInfo>();
            app->SetName(item["name"].toString());

            // parameter
            QMap<QString, QString> parameterList;
            auto parameters = item["parameters"].toObject();

            for(auto key : parameters.keys()) {
                parameterList[key] = parameters[key].toString();
            }

            //app->GetProcessors().at(0)->SetParameterList(parameterList);
            list.append(app);
        }

        return list;
    }
}
