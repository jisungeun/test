#define LOGGER_TAG "[AppContainer]"
//#include <Python.h>
#include <TCLogger.h>

#include <iostream>

#include <QFileDialog>
#include <QMessageBox>
#include <QActionGroup>
#include <QSettings>
#include <QJsonValue>
#include <QJsonDocument>
#include <QJsonArray>
#include <QCloseEvent>
#include <QTimer>
#include <QElapsedTimer>

#include <ctkPluginFrameworkLauncher.h>
#include <service/event/ctkEventAdmin.h>
#include <service/event/ctkEvent.h>
#include <LicenseManager.h>
#include <LicenseDialog.h>

#include <ValueEvent.h>
#include <MenuEvent.h>
#include <AppEvent.h>
#include <AppInterfaceTA.h>
#include <AppManagerTA.h>
#include <UIUtility.h>
//#include <TCPython.h>

#include <TabUiFramework.h>

#pragma warning( push )
#pragma warning( disable : 4819 )
#include <Inventor/devices/SoGLDevice.h>
#include <VolumeViz/nodes/SoVolumeRendering.h>
#pragma warning( pop )

#include "Version.h"
#include "ui_mainwindow.h"
#include "mainwindow.h"
#include <PluginRegistry.h>

#include <QJsonDocument>
#include <QScreen>
#include <QStandardPaths>

#include "CryptlexManager.h"

namespace TomoAnalysis {
	struct mainwindow::Impl {
		Ui::mainwindow* ui { nullptr };

		const QString senderName = "TomoAnalysis";
		TC::Framework::EventHandler* handler;
		TC::Framework::EventPublisher* publisher;

		TC::Framework::TabUiFramework* framework;

		int currentLayoutIndex = -1;

		QString windowTitle;
		QString tcfPath;
		QStringList menu_names;

		//PyThreadState* pyThState;
	};

	mainwindow::mainwindow(const QStringList& params, QWidget* parent) : QMainWindow(parent), d(new Impl()) {
		//Q_UNUSED(parent)
		//SoQt::init(this);
		d->ui = new Ui::mainwindow();
		d->ui->setupUi(this);
		d->ui->menubar->hide();
		QScreen* screen = QGuiApplication::primaryScreen();
		QRect screenGeometry = screen->availableGeometry();
		const int avwidth = screenGeometry.width();
		const int avheight = screenGeometry.height();

		const auto minWidth = avwidth < 1920 ? avwidth : 1920;
		const auto minHeight = avheight < 1080 ? avheight : 1080;

		setMinimumSize(minWidth, minHeight);

		this->setWindowTitle(QString("TomoAnalysis - %1").arg(PROJECT_REVISION));
		d->windowTitle = QString("TomoAnalysis - %1").arg(PROJECT_REVISION);

		//Init AppManager
		auto appManager = AppManagerTA::GetInstance();
		appManager->AcquirePackage();

		QMap<QString, QStringList> paramMap;

		for (int i = 0; i < params.count(); i++) {
			if (params[i].startsWith("-")) {
				QStringList values;

				for (int j = i + 1; j < params.count(); j++) {
					if (params[j].startsWith("-"))
						break;

					values.push_back(params[j]);
				}

				paramMap[params[i]] = values;
			}
		}

		//Init default AppUi
		initDefaultAppUi(paramMap);

		//Init communication port
		d->publisher = new TC::Framework::EventPublisher;
		d->handler = new TC::Framework::EventHandler;
		d->publisher->setPluginContext(ctkPluginFrameworkLauncher::getPluginContext());
		d->handler->setPluginContext(ctkPluginFrameworkLauncher::getPluginContext());
		d->handler->subscribeEvent(MenuEvent::Topic());
		d->handler->subscribeEvent(AppEvent::Topic());
		connect(d->handler, SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnCTKEvent(ctkEvent)));
		connect(d->framework, SIGNAL(sigTabClose(QString)), this, SLOT(OnTabClosed(QString)));
		showMaximized();

		auto VGANum = SoGLDevice::getDevicesCount();
		QLOG_INFO() << "Total " << VGANum << " Graphics Drivers";
		auto maxmemory = -1;
		SoGLDevice* maxmemoryGPU { nullptr };
		for (auto i = 0; i < VGANum; i++) {
			auto deviceName = SoGLDevice::getDevice(i)->getDeviceName();
			if (maxmemory < SoGLDevice::getDevice(i)->getTotalMemory()) {
				maxmemory = SoGLDevice::getDevice(i)->getTotalMemory();
				maxmemoryGPU = SoGLDevice::getDevice(i);
			}
			QLOG_INFO() << QString("#%1 - %2").arg(i + 1).arg(deviceName.toStdString().c_str());
		}

		if (maxmemoryGPU) {
			const auto devSettings = maxmemoryGPU->getDeviceSettings();
			if (devSettings) {
				devSettings->setSettingName("Largest Memory GPU");
				devSettings->applySettings();
			}
		}

		auto usedVGA = SoGLDevice::findFirstAvailableDevice()->getDeviceName();
		QLOG_INFO() << "Utilized Graphics Driver: " << usedVGA.toStdString().c_str();
	}

	mainwindow::~mainwindow() {
		//PyEval_RestoreThread(d->pyThState);
		//TC::PythonModule::GetInstance()->TurnOffPython();
		if (d->framework)
			delete d->framework;
		/*if (d->publisher)
			delete d->publisher;
		if(d->handler)
			delete d->handler;*/
		auto appManager = AppManagerTA::GetInstance();
		appManager->Stop();
	}

	auto mainwindow::initDefaultAppUi(const QMap<QString, QStringList>& paramMap) -> void {
		//QElapsedTimer timer;
		//timer.start();
		//~3 sec to initialize python engine
		/*TC::PythonModule::GetInstance()->ExecuteString("import sys");
		TC::PythonModule::GetInstance()->ExecuteString("print(sys.version)");
		TC::PythonModule::GetInstance()->ExecuteString("print(\"Changed working directory : {0}\".format(os.getcwd()))");
		TC::PythonModule::GetInstance()->ExecuteString("import matplotlib.pyplot as plt");
		TC::PythonModule::GetInstance()->ExecuteString("from interactive_segmentation_api import InteractiveModel");*/

		auto appManager = std::dynamic_pointer_cast<AppManagerTA>(AppManagerTA::GetInstance());
		License::CryptlexLicenseModel::CryptlexManager licenseManager;
		//initialize TabUiFramework with CTK
		d->framework = new TC::Framework::TabUiFramework(appManager);

		appManager->AddPluginSearchPath(qApp->applicationDirPath() + "/PluginApp");
		appManager->PreLoadPlugins();

		setCentralWidget(d->framework->getMainTab());

		licenseManager.SetFeatureTable({
											{ "org.tomocube.viewer2dPlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT },
											{ "org.tomocube.viewerPlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT },
											{ "org.tomocube.datanavigationPlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT },
											/*{"org.tomocube.projectmanagerPlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT},
											{"org.tomocube.basicanalysisPlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT},
											{"org.tomocube.basicanalysistimePlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT},
											{"org.tomocube.aianalysisPlugin", "App-AI_Mask_Analyzer"},
											{"org.tomocube.aitimePlugin", "App-AI_Mask_Analyzer"},
											{"org.tomocube.flmaskgeneratorPlugin", "App-FL_Mask_Analyzer"},
											{"org.tomocube.intersegPlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT},
											//{"org.tomocube.reportPlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT},
											//{"org.tomocube.reporttimePlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT},
											{"org.tomocube.reportflPlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT},
											{"org.tomocube.multi2dviewerPlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT},
											{"org.tomocube.mask2dviewerPlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT},
											{"org.tomocube.cilsprojectmanagerPlugin", "App-CILS"},
											{"org.tomocube.cilsexecutionPlugin", "App-CILS"},
											{"org.tomocube.maskeditor2dPlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT},
											{"org.tomocube.projectselectorPlugin", License::CryptlexLicenseModel::ALWAYS_ACCEPT},*/
										});

		auto apps = appManager->GetAppList();
		for (const auto& app : apps) {
			appManager->LoadAppPlugin(app);

			if (auto* iapp = appManager->GetAppInterface(app)) {
				if (auto* license = dynamic_cast<TomoAnalysis::License::ILicensed*>(iapp->GetInterfaceWidget())) {
					qDebug() << "License Checked: " << app;

					if (!licenseManager.Validate(*license))
						appManager->RemoveAppPlugin(app);

					continue;
				}
			}

			appManager->RemoveAppPlugin(app);
			qDebug() << "License Check Failed: " << app;
		}

		QLOG_INFO() << "TomoAnalysis AppContainer Initialized";

		if (paramMap.empty()) {
			//load default app (project selector)
			//loadAppUi("org.tomocube.projectselectorPlugin", "Project Selector", QVariantMap(), false);

			//For Viewer Edition
			loadAppUi("org.tomocube.datanavigationPlugin", "Data Manager", QVariantMap(), false);
			//loadAppUi("org.tomocube.projectmanagerPlugin", "Project Manager", QVariantMap(), false);
		}

		if (paramMap.contains("-projectmanager")) {
			loadAppUi("org.tomocube.projectmanagerPlugin", "Project Manager", QVariantMap(), false);
		}

		if (paramMap.contains("-nav")) {
			QVariantMap varMap;
			varMap["RootPaths"] = QStringList { paramMap["-nav"] };

			loadAppUi("org.tomocube.datanavigationPlugin", "Data Manager", varMap, false);
		}

		if (paramMap.contains("-v") || paramMap.contains("-view")) {
			QVariantMap varMap;
			varMap["ExecutionType"] = "OpenTCF";
			varMap["Playground"] = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
			varMap["TCFPath"] = paramMap.contains("-v") ? paramMap["-v"] : paramMap["-view"];

			loadAppUi("org.tomocube.viewer2dPlugin", "2D View", varMap, true);
		}

		if (paramMap.contains("-vl") || paramMap.contains("-viewlist")) {
			auto files = paramMap.contains("-vl") ? paramMap["-vl"] : paramMap["-viewlist"];
			QStringList tcfs;

			for (const auto& f : files) {
				QFile file(f);

				if (file.open(QIODevice::ReadOnly)) {
					auto doc = QJsonDocument::fromJson(file.readAll());
					auto obj = doc.object();

					auto tcfArray = obj["TcfList"].toArray();

					for (const auto& ar : tcfArray) {
						auto tcf = ar.toObject();
						tcfs.push_back(tcf["Path"].toString());
					}
				}

				file.remove();
			}

			if (!tcfs.isEmpty()) {
				QVariantMap varMap;
				varMap["ExecutionType"] = "OpenTCF";
				varMap["Playground"] = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
				varMap["TCFPath"] = tcfs;
				if (appManager->GetAppList().contains("org.tomocube.viewer2dPlugin"))
					loadAppUi("org.tomocube.viewer2dPlugin", "2D View", varMap, true);
			}
		}

		if (paramMap.contains("-t") || paramMap.contains("-title")) {
			d->windowTitle = paramMap.contains("-t") ? paramMap["-t"][0] : paramMap["-title"][0];
			d->windowTitle += QString(" (TA - %1)").arg(PROJECT_REVISION);
		}

		QMessageBox msgBox;
		msgBox.setText("Welcome");
		msgBox.setIcon(QMessageBox::Icon::NoIcon);
		msgBox.show();
		QTimer::singleShot(1000, &msgBox, SLOT(hide()));
	}

	void mainwindow::OnTabClosed(QString name) {
		const auto appManager = AppManagerTA::GetInstance();
		const auto* appInterface = appManager->GetAppInterface(name);

		using MenuType = TC::Framework::MenuTypeEnum;
		MenuEvent menuEvt(MenuType::NONE);		

		menuEvt.AddProperty("TabName", appInterface->GetDisplayTitle());

		d->publisher->publishSignal(menuEvt, "MainWindow");		
	}

	auto mainwindow::loadAppUi(const QString& FullName, const QString& AppName, const QVariantMap& params, const bool closable, const bool raisetab) -> void {
		auto appManager = AppManagerTA::GetInstance();
		if (!appManager->GetAppList().contains(FullName))
			return;

		if (!d->framework)
			return;
		if (!appManager->LoadAppPlugin(FullName))
			return;
		if (!appManager->Activate(FullName))
			return;

		d->framework->addUiFramework(FullName, AppName);
		if (!closable)
			d->framework->setTabUnclosable(AppName);
		if (raisetab)
			d->framework->raiseTab(AppName);

		auto ipluginInterface = appManager->GetAppInterface(FullName);
		auto* pluginInterfaceTA = dynamic_cast<AppInterfaceTA*>(ipluginInterface);

		pluginInterfaceTA->Execute(params);
	}

	void mainwindow::closeEvent(QCloseEvent* event) {
		using MenuType = TC::Framework::MenuTypeEnum;
		MenuEvent menuEvt(MenuType::NONE);
		menuEvt.scriptSet("SuddenClose");
		d->publisher->publishSignal(menuEvt, "MainWindow");

		QMainWindow::closeEvent(event);
	}

	auto mainwindow::findMenuIdx(QAction* menu) -> int {
		auto str = menu->text();
		auto result = -1;
		for (auto i = 0; i < d->menu_names.size(); i++) {
			if (str.compare(d->menu_names[i]) == 0)
				result = i;
		}
		return result;
	}

	auto mainwindow::findMenu(QString menu_name) -> QMenu* {
		QMenu* menu = nullptr;
		foreach(QAction * a, d->ui->menubar->actions()) {
			if (a->menu()) {
				if (a->menu()->title().compare(menu_name) == 0) {
					menu = a->menu();
				}
			}
		}
		return menu;
	}

	auto mainwindow::findAction(QString action_name) -> QAction* {
		QAction* action = nullptr;
		for (auto a : d->ui->menubar->actions()) {
			for (auto aa : a->menu()->actions()) {
				if (!aa->isSeparator()) {
					if (aa->text().compare(action_name) == 0)
						action = aa;
				}
			}
		}
		return action;
	}

	void mainwindow::OnMenuTriggered(QAction* action) {
		auto idx = findMenuIdx(action);
		if (idx > -1) {
			using MenuType = TC::Framework::MenuTypeEnum;
			//send action name as specific signal to current AppUI
			//via ctkEventAdmin port
			if (action->isCheckable()) {
				if (action->isChecked()) {
					MenuEvent menuEvt(MenuType::ActionChecked);
					menuEvt.scriptSet(d->menu_names[idx]);
					d->publisher->publishSignal(menuEvt, "MainWindow");
				} else {
					MenuEvent menuEvt(MenuType::ActionUnchecked);
					menuEvt.scriptSet(d->menu_names[idx]);
					d->publisher->publishSignal(menuEvt, "MainWindow");
				}
			} else {
				//emit simple menu signal                
				MenuEvent menuEvt(MenuType::Action);
				menuEvt.scriptSet(d->menu_names[idx]);
				d->publisher->publishSignal(menuEvt, "MainWindow");
			}
		}
	}
	
	void mainwindow::OnCTKEvent(ctkEvent e) {
		using AppType = TC::Framework::AppTypeEnum;
		using MenuType = TC::Framework::MenuTypeEnum;

		const auto& topic = e.getTopic();
		if (topic == AppEvent::Topic()) {
			OnApplicationCall(e);
		} else if (topic == MenuEvent::Topic()) {
			MenuEvent menuEvt(e);

			if (menuEvt.IsType(MenuTypeEnum::MenuScript)) {
				OnMenuRefreshed(e);
			} else if (menuEvt.IsType(MenuTypeEnum::TitleBar)) {
				OnTitleBarChanged(e);
			} else if (menuEvt.IsType(MenuTypeEnum::ParameterSet)) {
				OnParameterSetting(e);
			} else if (menuEvt.IsType(MenuTypeEnum::BatchFinish)) {
				OnBatchFinished(e);
			} else if (menuEvt.IsType(MenuTypeEnum::AbortTab)) {
				OnBatchAborted(e);
			} else if (menuEvt.IsType(MenuTypeEnum::CloseProgram)) {
				OnCloseProgram();
			}
		}
	}

	auto mainwindow::OnApplicationCall(ctkEvent e) -> void {
		using AppEnum = TC::Framework::AppTypeEnum;
		auto app = new TC::Framework::AppEvent;
		app->SetEvent(e);

		auto type = app->getEventType();
		switch (type) {
			case AppEnum::APP_WITHOUT_ARG:
			case AppEnum::APP_WITH_SHARE:
			case AppEnum::APP_WITH_ARGS:
				callAppArg(e);
				break;
			case AppEnum::APP_UPDATE:
				callAppUpdate(e);
				break;
			case AppEnum::APP_CLOSE:
				callAppClose(e);
				break;
		}
	}

	auto mainwindow::callAppUpdate(ctkEvent e) -> void {
		auto app = new TC::Framework::AppEvent;
		app->SetEvent(e);

		auto appName = app->getFullName();
		auto displayName = app->getAppName();

		auto isAppOpened = d->framework->findTabIdx(appName) > -1;
		if (false == isAppOpened) {
			return;
		}
		QVariantMap params;
		for (auto key : app->getParameterNames()) {
			params[key] = app->getParameter(key);
		}
		loadAppUi(appName, displayName, params, app->isClosable());
	}

	auto mainwindow::callAppArg(ctkEvent e) -> void {
		auto app = new TC::Framework::AppEvent;
		app->SetEvent(e);

		auto appName = app->getFullName();
		auto displayName = app->getAppName();

		QLOG_INFO() << "Load App UI with arguments";
		QLOG_INFO() << "App name: " << appName;
		QLOG_INFO() << "Display name: " << displayName;
		QVariantMap params;
		for (auto key : app->getParameterNames()) {
			params[key] = app->getParameter(key);
		}
		loadAppUi(appName, displayName, params, app->isClosable());
	}

	auto mainwindow::callAppClose(ctkEvent e) -> void {
		auto app = new AppEvent;
		app->SetEvent(e);
		auto appName = app->getFullName();
		auto displayName = app->getAppName();

		auto idx = d->framework->findTabIdx(appName);
		if (idx > -1) {
			d->framework->OnCloseTab(idx);
		}
	}

	auto mainwindow::OnMenuRefreshed(ctkEvent e) -> void {
		//clear existing menu properties
		return;//do not use menu
		d->ui->menubar->clear();
		d->menu_names.clear();

		////parse event to build menu        
		auto me = new TC::Framework::MenuEvent;
		me->SetEvent(e);
		d->menu_names = me->constructMenu(d->ui->menubar);

		connect(d->ui->menubar, SIGNAL(triggered(QAction*)), this, SLOT(OnMenuTriggered(QAction*)), Qt::UniqueConnection);
	}

	auto mainwindow::OnBatchFinished(ctkEvent e) -> void {
		auto appManager = AppManagerTA::GetInstance();
		TC::Framework::MenuTypeEnum batch = TC::Framework::MenuTypeEnum::BatchFinish;

		auto temp = e.getProperty(batch._to_string()).toString();
		auto sp = temp.split("!");
		QString target;
		QString sender;
		QString msg;
		if (sp.size() > 1) {
			sender = sp[0];
			target = sp[1];
			if (sp.size() > 2) {
				msg = sp[2];
			}
			//close existing batch
			if (msg != "NoClose") {
				auto idx = d->framework->findTabIdx(sender);
				if (idx > -1) {
					d->framework->OnCloseTab(idx);
				}
			}
		} else {
			target = temp;
		}
		//Apply history to project manager

		QLOG_INFO() << QString("Batch run finished in %1 app").arg(sender);
		QLOG_INFO() << QString("Open %1 app to present result").arg(target);

		appManager->LoadAppPlugin(target);
		appManager->Activate(target);

		auto appInterface = appManager->GetAppInterface(target);

		if (!appInterface) {
			return;
		}
		auto* targetInterfaceTA = dynamic_cast<AppInterfaceTA*>(appInterface);
		//targetInterfaceTA->OpenScript("BatchFinished");
		QVariantMap params;
		params["ExecutionType"] = "BatchFinished";
		targetInterfaceTA->Execute(params);
	}

	auto mainwindow::OnCloseProgram() -> void {
		dynamic_cast<QWidget*>(this->parent())->close();
	}

	auto mainwindow::OnBatchAborted(ctkEvent e) -> void {
		TC::Framework::MenuTypeEnum batch = TC::Framework::MenuTypeEnum::AbortTab;
		auto temp = e.getProperty(batch._to_string()).toString();
		auto sp = temp.split("!");
		auto sender = sp[0];
		if (sp.count() < 2) {
			auto idx = d->framework->findTabIdx(sender);
			if (idx > -1) {
				d->framework->OnCloseTab(idx);
			}
		}
	}

	auto mainwindow::OnParameterSetting(ctkEvent e) -> void {
		auto appManager = AppManagerTA::GetInstance();
		TC::Framework::MenuTypeEnum tcf = TC::Framework::MenuTypeEnum::ParameterSet;

		auto script = e.getProperty(tcf._to_string()).toString();

		auto setter = script.split("!")[0];
		auto target = script.split("!")[1];
		auto targetApp = target.split("*")[0];
		auto targetProc = target.split("*")[1];

		QLOG_INFO() << QString("Copy parameters in %1 processor from %2 to %3").arg(targetProc).arg(setter).arg(targetApp);

		appManager->LoadAppPlugin(setter);
		appManager->LoadAppPlugin(targetApp);

		appManager->Activate(targetApp);

		auto setterInterface = appManager->GetAppInterface(setter);
		auto targetInterface = appManager->GetAppInterface(targetApp);

		if (!setterInterface || !targetInterface)
			return;

		auto* setterInterfaceTA = dynamic_cast<AppInterfaceTA*>(setterInterface);
		auto* targetInterfaceTA = dynamic_cast<AppInterfaceTA*>(targetInterface);

		auto data = setterInterfaceTA->GetParameter(targetProc);
		targetInterfaceTA->SetParameter(targetProc, std::shared_ptr<IParameter>(data->Clone()));
		d->framework->focusTab(targetApp);
	}

	auto mainwindow::OnTitleBarChanged(ctkEvent e) -> void {
		auto me = new TC::Framework::MenuEvent;
		me->SetEvent(e);

		auto titleInfo = me->getTitleInfo();
		auto sender = me->getSenderName();
		if (sender.compare("3D View") == 0) {
			d->ui->menubar->show();
		} else {
			d->ui->menubar->hide();
		}

		auto parentWidget = static_cast<QWidget*>(parent());

		if (titleInfo.isEmpty()) {
			if (parentWidget) {
				parentWidget->setWindowTitle(d->windowTitle);
			} else {
				this->setWindowTitle(d->windowTitle);
			}
		} else {
			if (parentWidget) {
				parentWidget->setWindowTitle(d->windowTitle + " - " + titleInfo);
			} else {
				this->setWindowTitle(d->windowTitle + " - " + titleInfo);
			}
		}
	}
}
