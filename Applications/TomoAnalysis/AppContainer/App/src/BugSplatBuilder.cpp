#include <BugSplat.h>
#include "QsLog.h"

#include <QStringList>
#include <QFile>

#include "BugSplatBuilder.h"

namespace TomoAnalysis {
	const wchar_t* const DATABASE = L"tomoanalysis_v1";
	const wchar_t* const APPLICATION = L"TomoAnalysis";

	struct BugSplatBuilder::Impl {
		MiniDmpSender* sender = nullptr;

		QString version;
		QStringList logFilenames;

		MiniDmpSenderCallback callback = [](UINT, LPVOID, LPVOID) -> bool {
			QsLogging::Logger::destroyInstance();
			Sleep(2000);

			return true;
		};
	};

	BugSplatBuilder::BugSplatBuilder(const QString& version) : d(new Impl) {
		d->version = version;
	}

	BugSplatBuilder::~BugSplatBuilder() {
		delete d->sender;
	}

	auto BugSplatBuilder::AddLogFile(const QString& filename) -> void {
		d->logFilenames << filename;
	}

	auto BugSplatBuilder::Build() -> void {
		wchar_t pversion[20] = { 0, };
		d->version.toWCharArray(pversion);

		delete d->sender;
		d->sender = new MiniDmpSender(DATABASE, APPLICATION, pversion, nullptr, MDSF_USEGUARDMEMORY | MDSF_LOGFILE | MDSF_PREVENTHIJACKING);
		d->sender->setGuardByteBufferSize(20 * 1024 * 1024);
		d->sender->setDefaultUserEmail(L"user@youremail.com");
		d->sender->setDefaultUserName(L"User Name");
		d->sender->setDefaultUserDescription(L"Describe the situation in which the SW crash occurred.");
		d->sender->setCallback(d->callback);

		for (const auto& filename : d->logFilenames) {
			if (QFile file(filename); file.exists()) {
				auto* pfilename = new wchar_t[filename.size() + sizeof(wchar_t)]{ 0, };
				filename.toWCharArray(pfilename);

				d->sender->sendAdditionalFile(pfilename);
			}
		}
	}
}