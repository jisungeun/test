#include <BugSplat.h>
#include <QApplication>
#include <QString>
#include <QTimer>
#include <QSplashScreen>
#include <QElapsedTimer>
#include <QMessageBox>
#include <QFontDatabase>
#include <QThread>

#include <ctkPluginContext.h>
#include <ImageDev/Initialization.h>
#include <ImageDev/Exception.h>
#include <Inventor/Xt/SoXt.h>
#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>
#include <OivTimer.h>
#include <service/event/ctkEvent.h>

#include <LicenseManager.h>
#include <LicenseDialog.h>
#include <OivActivator.h>
#include <QStandardPaths>
#include <QVBoxLayout>
#include <TCLogger.h>
#include <Version.h>
#include <Inventor/SoSceneManager.h>
#include <Inventor/devices/SoGLDevice.h>
#include <Inventor/fields/SoField.h>

#include "BugSplatBuilder.h"
#include "mainwindow.h"
#include "TabUiFramework.h"
#include "VolumeViz/nodes/SoVolumeRendering.h"
#include "Medical/InventorMedical.h"

#define PRODUCT_DATA L"NTI2MUZCRjY5MkRENDkzNTAxRkY5QTdCMUY2NTcxNEE=.UCOgeuyay9oY4BsGI5pzGM0bGuHbJoXR5Ra09+l2To55bJSvzrmxcR6pVBBAAVT+Ox9DQNdyp5CJGk444Z1j5p0Lmd2Eda4OSVxaIk6aYNCmj4F3yDNmSv+kJWf67R3m97egWVZRO0ZcLtd9KA4tACDMWjAIUfhOq1GJOKlOzuS/8qTfxdpjVhBjyujiFr6wyM8QIhxNuYKwD0gm8axLJ8oFU3kQZ8L+qv7U1Mm618JnNpX0UeEe2slYBYvMoDnCP5tVx7SHKoXaweQckKcRcznCmiOkGdSLvpJlOYPxWYL/z0OdePjAMgvl0pw10F2picR+RIUdpv8xn8OnDrP1+43BWdy+9UIchxaHBl95bQbgTLLEHNoW+IRVZ3wlhEt2W7xjdtc5l2WZmsGPoh5UfHx6/z+GqhwvqP7Y1ML97URNIQSzBQEM3PZhzbHarV7qd0ELXVnOSPMWWblK9WCzPfSq/37zHg7uTUkAjde+Zh0aQKVUfgYyKffjeHRPVpktja7Eg/4kZRUqhGmATFkOtUj3k/GBGzwLospEukB778HEcjINUnhNW66sgYpNqyHPCIlji9SUZ/aJ0Adv6TEnaAp61lDsRG1MpbVPLjkv4MGSSt474HYQQwjrhvMn+72qzlpmoQqVdnBLz3uTVRnWW76QBQxjsvAsoMTXLRdeVqy4jmNq+A1fMtnpjlmbpsJ3bKaW7uAWr51sNLJ81FrPHG2O00k31FPDkZPscHM7sxQ="
#define PRODUCT_ID "8f2e8aba-ce73-4c9e-9a81-948a517a3929"

class DumpWidget : public QWidget {
public:
	DumpWidget(QWidget* parent = nullptr) : QWidget(parent) {};

	~DumpWidget() = default;

	auto SetWidget(QMainWindow* w) -> void {
		auto layout = new QVBoxLayout;
		layout->setContentsMargins(0, 0, 0, 0);
		layout->setSpacing(0);
		setLayout(layout);
		layout->addWidget(w);
		myW = w;
	}

protected:
	void closeEvent(QCloseEvent* event) override {
		if (myW) {
			myW->close();
		}
		QWidget::closeEvent(event);
	}

	QMainWindow* myW { nullptr };
};

extern "C" {
	__declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
	__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
}

int main(int argc, char* argv[]) {
	QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
	QApplication app(argc, argv); // instead of using QApplication				
	auto appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);

	//Remove existing log file
	const auto logFilename = QString("%1/TomoAnalysis.log").arg(appDataPath);
	const auto errorLogFilename = QString("%1/TomoAnalysisErrors.log").arg(appDataPath);

	std::remove(logFilename.toStdString().c_str());
	std::remove(errorLogFilename.toStdString().c_str());
	//Init TC LOGGER
	TC::Logger::Initialize(logFilename, errorLogFilename);
	TC::Logger::SetTraceLevel();

	QFontDatabase::addApplicationFont(":/font/NotoSansKR-Bold.otf");
	QFontDatabase::addApplicationFont(":/font/NotoSansKR-Regular.otf");

	QFont font;
	font.setFamily("Noto Sans KR");
	font.setPixelSize(12);
	font.setHintingPreference(QFont::HintingPreference::PreferNoHinting);

	QApplication::setFont(font);

	//app.setApplicationName("TomoAnalysis");
	//app.setOrganizationName("Tomocube, Inc.");
	//app.setOrganizationDomain("www.tomocube.com");

	//QFile f(":qdarkstyle/style.qss");
	QFile f(":/style/tcdark.qss");

	if (!f.exists()) {
		printf("Unable to set stylesheet, file not found\n");
	} else {
		f.open(QFile::ReadOnly | QFile::Text);
		QTextStream ts(&f);
		qApp->setStyleSheet(ts.readAll());
	}

#ifndef DEBUG
	const auto version = QString("%1.%2.%3%4").arg(PROJECT_VERSION_MAJOR).arg(PROJECT_VERSION_MINOR).arg(PROJECT_VERSION_PATCH).arg(PROJECT_VERSION_RELEASE_TYPE);

	BugSplatBuilder builder(version);
	builder.AddLogFile(logFilename);
	builder.AddLogFile(errorLogFilename);

	builder.Build();
#endif

	QSplashScreen splash(QPixmap(":/app/images/splash.png"), Qt::WindowStaysOnTopHint);
	splash.setEnabled(false);
	splash.showMessage("Loading...");
	splash.show();
	qApp->processEvents();

	QElapsedTimer timer;
	timer.start();

	//License
	LicenseManager::SetLicenseInfo(QString::fromStdWString(PRODUCT_DATA), QString::fromStdString(PRODUCT_ID));
	while (timer.elapsed() < 1000)
		qApp->processEvents();

	if (!LicenseManager::CheckLicense(false)) {
		splash.hide();
		QMessageBox::warning(0, "License", LicenseManager::GetErrorAsString(), QMessageBox::StandardButton::Ok);

		LicenseDialog dlg;
		dlg.exec();

		splash.show();
	}

	const auto isLicensed = LicenseManager::CheckLicense(false);

	QString ImageDevKeys;

	if (isLicensed) {
		QStringList OivKeys;
		OivKeys.append(QString("PACKAGE OpenInventorPackage mcslmd 2024.1 COMPONENTS=\"IvTuneViewer OpenInventor\" SIGN=%1 \nINCREMENT OpenInventorPackage mcslmd 2024.1 permanent uncounted \nVENDOR_STRING=INTERNALKEY HOSTID=DEMO ISSUED=12-mar-2024 \nNOTICE=APP-TOMOANALYSIS SN=20010828-IOIVR START=4-dec-2019 \nTS_OK SIGN=%2").arg(OivActivator::Convert({ "12GEI;;?A?>C" })).arg(OivActivator::Convert({ "FGHI45=J8@C?" })));
		OivKeys.append(QString("PACKAGE VolumeVizLDMPackage mcslmd 2024.1 COMPONENTS=\"VolumeVizLDM VolumeViz LDMWriter\" SIGN=%1 \nINCREMENT VolumeVizLDMPackage mcslmd 2024.1 permanent uncounted \nVENDOR_STRING=INTERNALKEY HOSTID=DEMO ISSUED=12-mar-2024 \nNOTICE=APP-TOMOANALYSIS SN=20010833-IVLDM START=4-dec-2019 \nTS_OK SIGN=%2").arg(OivActivator::Convert({ "72CEFK9J=NBC" })).arg(OivActivator::Convert({ "235E4=J?@;@?" })));

		OivActivator::Activate(OivKeys);

		ImageDevKeys.append("INCREMENT ImageDev asglmd 2023.2 permanent uncounted ");
		ImageDevKeys.append("VENDOR_STRING=INTERNALKEY HOSTID=DEMO ISSUED=26-dec-2023 ");
		ImageDevKeys.append("NOTICE=TomoAnalysis SN=23022968-IMDEV START=14-feb-2023 ");
		QStringList ImageDevPW { "013<", "EC89", "12;I", "E::D", "293I", "EC8G", "8DF8", "A1F9", "E:6E", "CBDF", "7225", "1D2;", "6GG<", "EG3:", "5C65", "F2E7", "4679", "E2:H", "463<", "9FHF", "17E4" };
		ImageDevKeys.append("TS_OK SIGN=\"");
		ImageDevKeys.append(OivActivator::Convert(ImageDevPW));
		ImageDevKeys.append("\"");
	} else {
		splash.hide();
		QMessageBox::warning(0, "License", LicenseManager::GetErrorAsString(), QMessageBox::StandardButton::Ok);
		return EXIT_SUCCESS;
	}

	DumpWidget* topLevelWidget = new DumpWidget;

	//SoDB::init();
	SoPreferences::setBool("OIV_QT_TIMER_THREAD", true);
	SoQt::init(topLevelWidget);
	InventorMedical::init();
	SoVolumeRendering::init();
	try {
		imagedev::init(ImageDevKeys.toStdString().c_str());
	} catch (imagedev::Exception& e) {
		std::cout << e.what() << std::endl;
	}

	QStringList params;

	for (auto i = 0; i < argc; i++) {
		QString param(argv[i]);
		params.push_back(param);
	}

	mainwindow* w = new mainwindow(params);
	w->setAnimated(false);
	topLevelWidget->SetWidget(w);

	if (params.count() <= 1) {
		auto otimer = OivTimer::GetInstance();
		otimer->setDelay(0);
		SoDB::setSystemTimer(otimer);
		otimer->start();
	}

	//hide splash window...
	while (timer.elapsed() < 2000)
		qApp->processEvents();
	splash.hide();

	topLevelWidget->showMaximized();
	SoQt::show(topLevelWidget);
	SoQt::mainLoop();
	topLevelWidget->close();

	//w->showMaximized();
	//qApp->exec();

	delete w;
	delete topLevelWidget;
	if (imagedev::isInitialized()) {
		imagedev::finish();
	}
	SoVolumeRendering::finish();
	InventorMedical::finish();
	SoQt::finish();
	//SoDB::finish();	

	return EXIT_SUCCESS;
}
