#pragma once

#include <QString>

namespace TomoAnalysis {
	class BugSplatBuilder {
	public:
		explicit BugSplatBuilder(const QString& version);
		~BugSplatBuilder();

		auto AddLogFile(const QString& filename) -> void;

		auto Build() -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}