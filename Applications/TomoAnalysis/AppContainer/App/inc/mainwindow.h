#pragma once

#include <QMainWindow>
#include <QVariantMap>

#include <service/event/ctkEvent.h>

#include "AppInterfaceTA.h"

namespace TomoAnalysis {
	class mainwindow : public QMainWindow {
		Q_OBJECT

	public:
		explicit mainwindow(const QStringList& params, QWidget* parent = nullptr);
		~mainwindow();

	protected slots:
		void OnCTKEvent(ctkEvent e);
		void OnMenuTriggered(QAction* action);
		void OnTabClosed(QString);
	protected:
		void closeEvent(QCloseEvent* event) override;

	private:
		auto initDefaultAppUi(const QMap<QString, QStringList>& paramMap) -> void;
		auto loadAppUi(const QString& FullName, const QString& appName, const QVariantMap& params = QVariantMap(), const bool closable = true, const bool raisetab = true) -> void;

		auto callAppArg(ctkEvent e) -> void;
		auto callAppUpdate(ctkEvent e) -> void;
		auto callAppClose(ctkEvent e) -> void;

		auto OnMenuRefreshed(ctkEvent e) -> void;
		auto OnTitleBarChanged(ctkEvent e) -> void;
		auto OnApplicationCall(ctkEvent e) -> void;
		auto OnParameterSetting(ctkEvent e) -> void;
		auto OnBatchFinished(ctkEvent e) -> void;
		auto OnBatchAborted(ctkEvent e) -> void;
		auto OnCloseProgram() -> void;

		auto findAction(QString action_name) -> QAction*;
		auto findMenu(QString menu_name) -> QMenu*;
		auto findMenuIdx(QAction* menu) -> int;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
