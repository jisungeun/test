#pragma once

#include "IAppActivator.h"

#define SYMBOL "org.tomocube.projectselectorPlugin"

namespace TomoAnalysis::ProjectSelector::AppUI {
	class projectselectorPlugin final : public TC::Framework::IAppActivator {
		Q_OBJECT
			Q_PLUGIN_METADATA(IID SYMBOL)

	public:
		explicit projectselectorPlugin();
		~projectselectorPlugin() override;

		auto start(ctkPluginContext* context)->void override;
		auto getPluginContext()->ctkPluginContext* const override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
