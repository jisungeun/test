#include <iostream>

#include <QPushButton>

#include "ui_MainWindow.h"
#include "MainWindow.h"

#include "AppEvent.h"
#include "AppInterfaceTA.h"
#include "AppManagerTA.h"
#include "IAppInterface.h"
#include "IAppManager.h"

#define SYMBOL "org.tomocube.projectselectorPlugin"

using namespace TC::Framework;

namespace TomoAnalysis::ProjectSelector::AppUI {
	struct MainWindow::Impl {
		QVariantMap appProperties;
		Ui::MainWindow* ui = nullptr;

		int index = -1;
	};

	MainWindow::MainWindow() : IMainWindowTA("Project Selector", "Project Selector"), d{ new Impl } {
		d->appProperties["Parent-App"] = "StandAlone";
		d->appProperties["AppKey"] = "Project Selector";
	}

	MainWindow::~MainWindow() = default;

	auto MainWindow::Execute(const QVariantMap& params)->bool {
		if (params.isEmpty()) {
			return true;
		}
		return true;
	}

	auto MainWindow::GetParameter(QString name) -> IParameter::Pointer {
		return {};
	}

	auto MainWindow::SetParameter(QString name, IParameter::Pointer param) -> void {}

	auto MainWindow::GetRunType() -> std::tuple<bool, bool> {
		return {};
	}

	auto MainWindow::TryActivate() -> bool {
		d->ui = new Ui::MainWindow;
		d->ui->setupUi(this);

		const auto metaRegistry = IAppMetaRegistry::GetInstance();
		const auto manager = AppManagerTA::GetInstance();
		const auto appList = manager->GetAppList();

		for (auto i = 0; i < appList.length(); i++) {
			auto metaPath = qApp->applicationDirPath() + "/PluginApp/Meta/" + appList[i].chopped(6) + ".meta";
			auto appMeta = IAppMetaRegistry::GetMeta(metaPath, true);
			if (!appMeta) {
				continue;
			}
			auto meta = appMeta->GetMetaInfo();
			auto* btn = new QPushButton;
			btn->setText(appMeta->GetName());
			btn->setObjectName("bt-square-primary");
			btn->setMinimumWidth(500);
			btn->setMinimumHeight(50);

			QWidget::connect(btn, &QPushButton::clicked,
				[this, appMeta] {
					AppEvent openApp(AppTypeEnum::APP_WITHOUT_ARG);
					openApp.setAppName(appMeta->GetName());
					openApp.setFullName(appMeta->GetFullName());
					openApp.setClosable(false);

					publishSignal(openApp, SYMBOL);

					AppEvent closeThis(AppTypeEnum::APP_CLOSE);
					closeThis.setFullName(SYMBOL);
					closeThis.setAppName(SYMBOL);

					publishSignal(closeThis, SYMBOL);
				}
			);

			d->ui->appLayout->insertWidget(1, btn);

		}
		/*
		for (int i = 0; i < appList.length(); i++) {
			const auto* app = dynamic_cast<AppInterfaceTA*>(manager->GetAppInterface(appList[i]));
			if (app && app->IsProjectManager()) {
				//for ETH
				if (app->GetDisplayTitle() != "Project Manager") {
					continue;
				}
				auto* btn = new QPushButton;
				btn->setText(app->GetDisplayTitle());
				btn->setObjectName("bt-square-primary");
				btn->setMinimumWidth(500);
				btn->setMinimumHeight(50);

				QWidget::connect(btn, &QPushButton::clicked,
					[this, i] {
						const auto manager = AppManagerTA::GetInstance();
						const auto appList = manager->GetAppList();
						const QString& appName = appList[i];
						const auto* app = dynamic_cast<AppInterfaceTA*>(manager->GetAppInterface(appName));

						AppEvent openApp(AppTypeEnum::APP_WITHOUT_ARG);
						openApp.setAppName(app->GetDisplayTitle());
						openApp.setFullName(appName);
						openApp.setClosable(false);

						publishSignal(openApp, SYMBOL);

						AppEvent closeThis(AppTypeEnum::APP_CLOSE);
						closeThis.setFullName(SYMBOL);
						closeThis.setAppName(SYMBOL);

						publishSignal(closeThis, SYMBOL);
					}
				);

				d->ui->appLayout->insertWidget(1, btn);
			}
		}*/

		return true;
	}

	auto MainWindow::TryDeactivate() -> bool {
		return false;
	}

	auto MainWindow::IsActivate() -> bool {
		return false;
	}

	auto MainWindow::GetMetaInfo() -> QVariantMap {
		return d->appProperties;
	}

	auto MainWindow::GetFeatureName() const -> QString {
		return "org.tomocube.projectselectorPlugin";
	}

	auto MainWindow::OnAccepted() -> void {
	}

	auto MainWindow::OnRejected() -> void {
	}
}
