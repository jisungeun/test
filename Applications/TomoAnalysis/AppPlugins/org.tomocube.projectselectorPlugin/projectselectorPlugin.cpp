#include "projectselectorPlugin.h"

#include <AppInterfaceTA.h>

#include "MainWindow.h"

using namespace TC::Framework;

namespace TomoAnalysis::ProjectSelector::AppUI {
	struct projectselectorPlugin::Impl {
		ctkPluginContext* context = nullptr;
		AppInterfaceTA* appInterface = nullptr;
		MainWindow* mainWindow = nullptr;
	};

	projectselectorPlugin::projectselectorPlugin() : TC::Framework::IAppActivator(), d{ new Impl } {
		d->mainWindow = new MainWindow;
		d->appInterface = new AppInterfaceTA(d->mainWindow);
	}

	projectselectorPlugin::~projectselectorPlugin() = default;

	auto projectselectorPlugin::start(ctkPluginContext* context) -> void {
		d->context = context;

		registerService(context, d->appInterface, SYMBOL);
	}

	auto projectselectorPlugin::getPluginContext() -> ctkPluginContext* const {
		return d->context;
	}
}

