#pragma once

#include "ILicensed.h"
#include "IMainWindowTA.h"

namespace TomoAnalysis::ProjectSelector::AppUI {
	class MainWindow final : public IMainWindowTA, public TomoAnalysis::License::ILicensed {
		Q_OBJECT

	public:
		explicit MainWindow();
		~MainWindow() override;

		auto GetParameter(QString name)->IParameter::Pointer override;
		auto SetParameter(QString name, IParameter::Pointer param) -> void override;		

		auto Execute(const QVariantMap& params)->bool override;
		auto GetRunType()->std::tuple<bool, bool> override;		
		auto TryActivate() -> bool override;
		auto TryDeactivate() -> bool override;
		auto IsActivate() -> bool override;
		auto GetMetaInfo()->QVariantMap override;

		auto GetFeatureName() const -> QString override;
		auto OnAccepted() -> void override;
		auto OnRejected() -> void override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
