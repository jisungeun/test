#include <AppInterfaceTA.h>

#include <AppEvent.h>

#include "MainWindow.h"
#include "maskeditor2dPlugin.h"

namespace TomoAnalysis::MaskEditor2d::AppUI {
    struct maskeditor2dPlugin::Impl {
        Impl() = default;
        ~Impl() = default;

        ctkPluginContext* context;
        MainWindow* mainWindow{ nullptr };
        AppInterfaceTA* appInterface{ nullptr };
    };

    maskeditor2dPlugin* maskeditor2dPlugin::instance = 0;

    maskeditor2dPlugin* maskeditor2dPlugin::getInstance() {
        return instance;
    }
    maskeditor2dPlugin::maskeditor2dPlugin() : d{ new Impl } {
        d->mainWindow = new MainWindow;
        d->appInterface = new AppInterfaceTA(d->mainWindow);
    }
    maskeditor2dPlugin::~maskeditor2dPlugin() {
        
    }
    auto maskeditor2dPlugin::start(ctkPluginContext* context) -> void {
        instance = this;
        d->context = context;

        registerService(context, d->appInterface, plugin_symbolic_name);
    }
    auto maskeditor2dPlugin::stop(ctkPluginContext* context) -> void {
        
    }
    auto maskeditor2dPlugin::getPluginContext() -> ctkPluginContext* const {
        return d->context;
    }
}