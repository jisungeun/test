#pragma once

#include <memory>
#include <QWidget>

#include <ISemiAutoPanel.h>

#include "ME2SemiAutoPanelExport.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
    class ME2SemiAutoPanel_API SemiAutoPanel : public QWidget, public Interactor::ISemiAutoPanel {
        Q_OBJECT
    public:
        typedef SemiAutoPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        SemiAutoPanel(QWidget* parent = nullptr);
        ~SemiAutoPanel();

        auto Update() -> bool override;


    signals:        
        void sigLassoDel(bool);
        void sigPickDel(bool);
        void sigSizeFilter(bool);
        void sigErode();
        void sigDilate();
        void sigWatershed();
        void sigSizeIndex(int);        

    protected slots:        
        void OnWatershedClicked();
        void OnErodeClicked();
        void OnDilateClicked();
        void OnPickDeleteClicked();
        void OnLassoDeleteClicked();
        void OnSizeFilterClicked();        

        void OnSizeSliderChanged(int);
        void OnSizeSliderReleased();
        void OnSizeSpinChanged(int);

        void OnCollapse(bool);

        auto SetSizeMax(int max)->void;

    private:
        auto InitIcons()->void;
        auto Init()->void;
        auto InitToolTips()->void;
        auto CleanIcons()->void;
        auto SetIconOn()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}