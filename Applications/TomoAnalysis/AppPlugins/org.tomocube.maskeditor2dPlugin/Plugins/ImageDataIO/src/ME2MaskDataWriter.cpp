#include <TCMaskWriter.h>
#include <TCHMaskWriter.h>
#include <TCUMaskWriter.h>
#include <TCMaskWriterPort.h>

#include "ME2MaskDataWriter.h"

namespace TomoAnalysis::MaskEditor2d::Plugins::ImageDataIO {
    struct MaskDataWriter::Impl {        
    };
    MaskDataWriter::MaskDataWriter() : d{ new Impl } {
        
    }
    MaskDataWriter::~MaskDataWriter() {
        
    }    
    auto MaskDataWriter::Modify(TCMask::Pointer data, const QString& path, const QString& organName,int nameIdx) -> bool {
        TC::IO::TCMaskWriterPort writer;
        return writer.Modify(data, path, "HT", organName, nameIdx);
        /*
        auto time_idx = data->GetTimeStep();
        TC::IO::TCUMaskWriter writer(path,time_idx);        
        const auto indexes = data->GetBlobIndexes();
        writer.ClearBlobCount("HT", organName,time_idx);
        writer.ClearWhole("HT", organName,time_idx);
        auto names = data->GetLayerNames();
        for (auto i = 0; i < indexes.size(); i++) {
            auto index = indexes[i];

            auto blob = data->GetBlob(index);
            auto inBox = blob.GetBoundingBox();
            auto code = blob.GetCode();

            TC::IO::BoundingBox bbox;
            bbox.SetOffset(std::get<0>(inBox), std::get<1>(inBox), std::get<2>(inBox));
            bbox.SetSize(std::get<3>(inBox) - std::get<0>(inBox) + 1,
                std::get<4>(inBox) - std::get<1>(inBox) + 1,
                std::get<5>(inBox) - std::get<2>(inBox) + 1);
            if (!writer.WriteBlob("HT",organName, time_idx, index, bbox, code)) return false;
            //if (!writer.UpdateBlob("HT", time_idx, index, bbox, code)) return false;

            auto maskVolume = blob.GetMaskVolume();
            if (!writer.WriteMask("HT", organName,time_idx, index, bbox, maskVolume)) return false;
            //if (!writer.UpdateMask("HT", time_idx, index, bbox, maskVolume)) return false;
        }
        return true;
        */
    }

    auto MaskDataWriter::Write(TCMask::Pointer data, const QString& path, const QString& organName,int nameIdx) -> bool {
        TC::IO::TCMaskWriterPort writer;
        return writer.Write(data, path,"HT", organName, nameIdx);
        /*auto time_idx = data->GetTimeStep();
        TC::IO::TCUMaskWriter writer(path,time_idx);        
        const auto indexes = data->GetBlobIndexes();        
        writer.SetNumberOfBlob(indexes.count());
        writer.ClearBlobCount("HT",organName, time_idx);

        QString type = "Binary";
        if(data->GetType()._to_integral() == MaskTypeEnum::MultiLabel) {
            type = "Label";
        }
        writer.WriteName("HT", nameIdx, organName,type);                
        for (auto i = 0; i < indexes.count(); i++) {            
            auto index = indexes[i];            

            auto blob = data->GetBlob(index);
            auto inBox = blob.GetBoundingBox();
            auto code = blob.GetCode();

            TC::IO::BoundingBox bbox;
            bbox.SetOffset(std::get<0>(inBox), std::get<1>(inBox), std::get<2>(inBox));
            bbox.SetSize(std::get<3>(inBox) - std::get<0>(inBox) + 1,
                std::get<4>(inBox) - std::get<1>(inBox) + 1,
                std::get<5>(inBox) - std::get<2>(inBox) + 1);
            //if (!writer.WriteBlob("HT", time_idx, real_idx,  bbox, code)) return false;
            if (!writer.WriteBlob("HT", organName, time_idx, index, bbox, code)) {
                return false;
            }

            auto maskVolume = blob.GetMaskVolume();
            //if (!writer.WriteMask("HT", time_idx, real_idx, bbox, maskVolume)) return false;
            if (!writer.WriteMask("HT", organName, time_idx, index, bbox, maskVolume)) {                
                return false;
            }
        }
        const auto size = data->GetSize();
        writer.WriteSize(std::get<0>(size), std::get<1>(size), std::get<2>(size));
        const auto res = data->GetResolution();
        writer.WriteResolution(std::get<0>(res), std::get<1>(res), std::get<2>(res));
        writer.WriteVersion(2, 0, 0);
        return true;
        */
    }
}