#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoVolumeData.h>
#pragma warning(pop)

#include <TCDataConverter.h>
#include <TCMaskReaderPort.h>

#include "ME2MaskDataReader.h"

namespace TomoAnalysis::MaskEditor2d::Plugins::ImageDataIO {
    MaskDataReader::MaskDataReader() {

    }
    MaskDataReader::~MaskDataReader() {

    }   
    auto MaskDataReader::GetMaskList(const QString& path,const QString& modalityName) -> QList<MaskMeta> {
        auto reader = TC::IO::TCMaskReaderPort();
        return reader.GetMaskList(path, modalityName);
    }
    auto MaskDataReader::Read(const QString& path, const QString& modalityName, const QString& organName, int time_idx) const -> TCMask::Pointer {
        auto reader = TC::IO::TCMaskReaderPort();
        return reader.Read(path, modalityName, organName, time_idx);
    }    
    auto MaskDataReader::Create(TCImage::Pointer image, QString mask_name, int typeIdx) -> TCMask::Pointer {
        auto ssize = image->GetSize();
        int size[3]{ std::get<0>(ssize),std::get<1>(ssize),std::get<2>(ssize) };
        double res[3];
        image->GetResolution(res);                

        double rres[3];
        rres[0] = res[0];
        rres[1] = res[1];
        rres[2] = res[2];

        auto resultMask = std::make_shared<TCMask>();
        resultMask->SetResolution(rres);
        resultMask->SetSize(size[0], size[1], size[2]);
        resultMask->SetValid(true);
        resultMask->SetType(MaskTypeEnum::MultiLabel);
        if(typeIdx == 0) {
            resultMask->SetTypeText("Label");
        }else {
            resultMask->SetTypeText("Binary");
        }        

        std::shared_ptr<unsigned char> maskArr(new unsigned char[size[0] * size[1] * size[2]](), std::default_delete<unsigned char[]>());
        MaskBlob blob;
        blob.SetBoundingBox(0, 0, 0, size[0] - 1, size[1] - 1, size[2] - 1);
        blob.SetMaskVolume(maskArr);
                
        resultMask->AppendLayer(mask_name, blob);
        
        return resultMask;
    }        
}
