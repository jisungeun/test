#pragma once

#include <IMaskWriterPort.h>

#include "ME2ImageDataIOExport.h"

namespace TomoAnalysis::MaskEditor2d::Plugins::ImageDataIO {
    class ME2ImageDataIO_API MaskDataWriter : public UseCase::IMaskWriterPort {
    public:
        MaskDataWriter();
        ~MaskDataWriter();
                
        auto Write(TCMask::Pointer data, const QString& path, const QString& organName,int nameIdx) -> bool override;
        auto Modify(TCMask::Pointer data, const QString& path, const QString& organName,int nameIdx) -> bool override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}