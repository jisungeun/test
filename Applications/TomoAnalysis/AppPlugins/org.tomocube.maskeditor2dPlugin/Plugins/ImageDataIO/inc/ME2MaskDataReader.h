#pragma once

#include <IMaskReaderPort.h>

#include "ME2ImageDataIOExport.h"

namespace TomoAnalysis::MaskEditor2d::Plugins::ImageDataIO {
    class ME2ImageDataIO_API MaskDataReader : public UseCase::IMaskReaderPort {
    public:
        MaskDataReader();
        ~MaskDataReader();

        auto Read(const QString& path, const QString& modalityName, const QString& organName, int time_idx) const -> TCMask::Pointer override;
        auto GetMaskList(const QString& path,const QString& modalityName) -> QList<MaskMeta> override;
        auto Create(TCImage::Pointer image, QString mask_name, int typeIdx) -> TCMask::Pointer override;        
    };
}