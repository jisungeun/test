#pragma once

#include <QObject>
#include <IAppMeta.h>

#include "ME2MetaExport.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
	class ME2Meta_API AppMeta
		:public QObject
		, public IAppMeta
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.maskeditor2dMeta")
		Q_INTERFACES(IAppMeta)
	public:
		AppMeta();
		virtual ~AppMeta();

		auto GetName()const->QString override { return "2D Mask Editor"; }
		auto GetFullName()const->QString override { return "org.tomocube.maskeditor2dPlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}