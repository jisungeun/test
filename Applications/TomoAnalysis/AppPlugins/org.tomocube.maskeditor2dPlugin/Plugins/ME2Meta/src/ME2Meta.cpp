#include "ME2Meta.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
	struct AppMeta::Impl {
		QVariantMap appProperties;
	};
	AppMeta::AppMeta() : d{ new Impl } {
		d->appProperties["Parent-App"] = "StandAlone";
		d->appProperties["AppKey"] = "2D Mask Editor";
	}
	AppMeta::~AppMeta() {

	}
	auto AppMeta::GetMetaInfo()const->QVariantMap {
		return d->appProperties;
	}
}