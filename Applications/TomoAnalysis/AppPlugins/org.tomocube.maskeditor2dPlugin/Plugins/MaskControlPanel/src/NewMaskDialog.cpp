#include <QStyle>
#include <QFileDialog>
#include <QMessageBox>
#include <QRegExpValidator>
#include <QSettings>

#include "ui_NewMaskDialog.h"
#include "NewMaskDialog.h"

#include <QLineEdit>

namespace TomoAnalysis::MaskEditor2d::Plugins {
    struct NewMaskDialog::Impl {
        Ui::NewMaskDialog* ui{ nullptr };
        QString rstrip(const QString& str) {
            int n = str.size() - 1;
            for (; n >= 0; --n) {
                if (!str.at(n).isSpace()) {
                    return str.left(n + 1);
                }
            }
            return "";
        }
        //registry keys        
    };

    NewMaskDialog::NewMaskDialog(QWidget* parent)
        : QDialog(parent)
        , d(new Impl()) {

        d->ui = new Ui::NewMaskDialog();
        d->ui->setupUi(this);

                
        QRegExp re("[a-zA-Z\\.\\-\\_0-9][a-zA-Z \\. \\- \\_ 0-9 ]+");
        d->ui->nameLineEdit->setValidator(new QRegExpValidator(re));        
        setWindowTitle("New Mask");
        setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
        d->ui->nameLineEdit->setPlaceholderText("Enter a mask name");
        d->ui->nameLineEdit->setObjectName("input-high");
        d->ui->nameLineEdit->setStyleSheet("QLineEdit[text=\"\"]{ color:rgba(95, 111, 122,255); }");
        connect(d->ui->nameLineEdit, &QLineEdit::textChanged, [=] { d->ui->nameLineEdit->style()->polish(d->ui->nameLineEdit); });

        d->ui->maskTypeCombo->addItem("Label");
        d->ui->maskTypeCombo->addItem("Binary");        

        d->ui->predCombo->addItem("Cell Instance");
        d->ui->predCombo->addItem("Whole cell");
        d->ui->predCombo->addItem("Nucleus");
        d->ui->predCombo->addItem("Nucleolus");
        d->ui->predCombo->addItem("Lipid droplet");

        d->ui->predCombo->hide();

        connect(d->ui->predChk, SIGNAL(clicked()), this, SLOT(OnPredCheckClicked()));
        connect(d->ui->predCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnPredComboChanged(int)));
    }

    NewMaskDialog::~NewMaskDialog() = default;

    auto NewMaskDialog::GetNewMaskName(QWidget* parent)->std::tuple<QString,int> {
        NewMaskDialog dialog(parent);

        if(dialog.exec() != QDialog::Accepted) {
            return std::make_tuple(QString(),-1);
        }

        const auto name = dialog.GetName();
        const auto idx = dialog.GetType();

        return std::make_tuple(name,idx);
    }

    auto NewMaskDialog::GetName(void) const ->QString {
        auto result = d->ui->nameLineEdit->text();
        result = d->rstrip(result);
        return result;
    }        

    auto NewMaskDialog::GetType() const -> int {
        auto type = d->ui->maskTypeCombo->currentIndex();
        return type;
    }

    void NewMaskDialog::on_createButton_clicked() {
        if (true == d->ui->nameLineEdit->text().isEmpty()) {
            QMessageBox::warning(nullptr, "Enter a mask name", "Empty or invalid mask name");
            return;
        }                        
        this->accept();
    }

    void NewMaskDialog::on_cancelButton_clicked() {
        this->reject();
    }

    void NewMaskDialog::OnPredCheckClicked() {
        if(d->ui->predChk->isChecked()) {
            d->ui->maskTypeCombo->setEnabled(false);
            d->ui->predCombo->show();
            auto curTxt = d->ui->predCombo->currentText();
            auto keyTxt = QString();
            d->ui->maskTypeCombo->setCurrentIndex(1);
            if(curTxt == "Cell Instance") {
                keyTxt = "cellInst";
                d->ui->maskTypeCombo->setCurrentIndex(0);
            }else if(curTxt == "Whole cell") {
                keyTxt = "membrane";                
            }else if(curTxt == "Nucleus") {
                keyTxt = "nucleus";
            }else if(curTxt == "Nucleolus") {
                keyTxt = "nucleoli";
            }
            else if (curTxt == "Lipid droplet") {
                keyTxt = "lipid droplet";
            }
            d->ui->nameLineEdit->setText(keyTxt);
        }else {
            d->ui->maskTypeCombo->setEnabled(true);
            d->ui->predCombo->hide();
        }
    }

    void NewMaskDialog::OnPredComboChanged(int idx) {
        if(d->ui->predChk->isChecked()) {
            auto curTxt = d->ui->predCombo->currentText();
            d->ui->maskTypeCombo->setCurrentIndex(1);
            auto keyTxt = QString();
            if (curTxt == "Cell Instance") {
                keyTxt = "cellInst";
                d->ui->maskTypeCombo->setCurrentIndex(0);
            }
            else if (curTxt == "Whole cell") {
                keyTxt = "membrane";                
            }
            else if (curTxt == "Nucleus") {
                keyTxt = "nucleus";
            }
            else if (curTxt == "Nucleolus") {
                keyTxt = "nucleoli";
            }
            else if (curTxt == "Lipid droplet") {
                keyTxt = "lipid droplet";
            }
            d->ui->nameLineEdit->setText(keyTxt);
        }
    }
}
