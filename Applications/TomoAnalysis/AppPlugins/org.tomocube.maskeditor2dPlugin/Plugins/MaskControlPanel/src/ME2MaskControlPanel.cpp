#include <iostream>

#include <QIcon>
#include <QFileDialog>
#include <QSettings>

#include <UIUtility.h>

#include "NewMaskDialog.h"
#include "ui_ME2MaskControlPanel.h"
#include "ME2MaskControlPanel.h"


namespace TomoAnalysis::MaskEditor2d::Plugins {    
    struct MaskControlPanel::Impl {
        Ui::MaskControlForm* ui{ nullptr };
        QIcon confirmIcon;
        QIcon naIcon;//N/A
        QIcon workingIcon;                

        bool isSystem{ false };
    };
    MaskControlPanel::MaskControlPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::MaskControlForm();
        d->ui->setupUi(this);        
        this->Init();
    }
    MaskControlPanel::~MaskControlPanel() = default;
    auto MaskControlPanel::Reset()->void {
        auto ds = GetDS();       
        ds->maskList.clear();        
        Update();
    }
    auto MaskControlPanel::Update() -> bool {
        InternalReset();
        auto ds = GetDS();
        for(auto i=0;i<ds->maskList.count();i++) {
            TC::SilentCall(d->ui->maskCombo)->addItem(ds->maskList[i].name);
        }
        if(-1!= ds->curMaskIdx) {
            TC::SilentCall(d->ui->maskCombo)->setCurrentIndex(ds->curMaskIdx);
        }
        if(-1!=ds->curMaskType) {
            d->ui->typeText->show();
            d->ui->label->setText("Mask type:");
            if(ds->curMaskType == 0) {
                d->ui->typeText->setText("Label");
            }else {
                d->ui->typeText->setText("Binary");
            }
        }else {
            d->ui->typeText->hide();
            d->ui->label->setText("No mask selected");
        }
        return true;
    }        

    void MaskControlPanel::curMaskChanged(int idx) {
        auto ds = GetDS();
        if(ds->curMaskPath.isEmpty()) {
            std::cout << "mask path is empty" << std::endl;
            return;
        }
        emit sigLoadMask(d->ui->maskCombo->itemText(idx),ds->curMaskPath);
    }    

    auto MaskControlPanel::Init() -> void {
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");               

        connect(d->ui->maskCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(curMaskChanged(int)));        
        connect(d->ui->saveBtn, SIGNAL(clicked()), this, SLOT(OnSaveClicked()));
        connect(d->ui->addBtn, SIGNAL(clicked()), this, SLOT(OnNewClicked()));
        connect(d->ui->loadNpyBtn, SIGNAL(clicked()), this, SLOT(OnNumPyLoadClicked()));
        connect(d->ui->saveNpyBtn, SIGNAL(clicked()), this, SLOT(OnNumPySaveClicked()));

        d->ui->label->setObjectName("h7");
        d->ui->typeText->setObjectName("h8");
        d->ui->label->setText("No mask selected");
    }
    
    auto MaskControlPanel::InternalReset() -> void {
        TC::SilentCall(d->ui->maskCombo)->clear();                
    }    
    void MaskControlPanel::OnSaveClicked() {
        if (d->ui->maskCombo->count() < 1) {
            return;
        }
        emit sigSaveMask(d->ui->maskCombo->currentText());
    }
    void MaskControlPanel::OnNewClicked() {        
        auto result = NewMaskDialog::GetNewMaskName(nullptr);
        auto name = std::get<0>(result);
        if(name.isEmpty()) {
            return;
        }
        auto idx = std::get<1>(result);
        emit sigAddMask(name,idx);
    }
    void MaskControlPanel::OnNumPyLoadClicked() {
        QSettings qs("MaskEditor/NumpyIO");
        auto prevPath = qs.value("prevPath",qApp->applicationDirPath()).toString();
        const auto str = QFileDialog::getOpenFileName(nullptr, "Select a npy file", prevPath, "NumPy file (*.npy)");
        if(str.isEmpty()) {
            return;
        }
        qs.setValue("prevPath", str);
        emit sigLoadNumpy(str);
    }
    void MaskControlPanel::OnNumPySaveClicked() {
        QSettings qs("MaskEditor/NumpyIO");
        auto prevPath = qs.value("prevPath", qApp->applicationDirPath()).toString();
        const auto str = QFileDialog::getSaveFileName(nullptr, "Set npy file save path", prevPath, "Numpy file (*.npy)");
        if(str.isEmpty()) {
            return;
        }
        emit sigSaveNumpy(str);
    }
}
