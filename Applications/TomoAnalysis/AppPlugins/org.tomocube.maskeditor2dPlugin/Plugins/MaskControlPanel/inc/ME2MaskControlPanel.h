#pragma once

#include <memory>
#include <QWidget>

#include <IMaskControlPanel.h>

#include "ME2MaskControlPanelExport.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
    class ME2MaskControlPanel_API MaskControlPanel : public QWidget,public Interactor::IMaskControlPanel {
        Q_OBJECT
    public:
        typedef MaskControlPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        MaskControlPanel(QWidget* parent = nullptr);
        ~MaskControlPanel();

        auto Update() -> bool override;
        auto Reset()->void override;
    signals:                        
        void sigLoadMask(QString,QString);
        void sigSaveMask(QString);
        void sigAddMask(QString,int);
        void sigLoadNumpy(QString);
        void sigSaveNumpy(QString);

    protected slots:        
        void curMaskChanged(int);        
        void OnSaveClicked();
        void OnNewClicked();
        void OnNumPyLoadClicked();
        void OnNumPySaveClicked();

    private:        
        auto Init()->void;
        auto InternalReset()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}