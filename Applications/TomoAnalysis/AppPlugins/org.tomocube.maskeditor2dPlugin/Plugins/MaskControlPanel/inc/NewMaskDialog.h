#pragma once

#include <tuple>
#include <memory>
#include <QDialog>

namespace TomoAnalysis::MaskEditor2d::Plugins {
    class NewMaskDialog : public QDialog {
        Q_OBJECT

    public:
        explicit NewMaskDialog(QWidget* parent = nullptr);
        virtual ~NewMaskDialog();

    public:
        static auto GetNewMaskName(QWidget* parent = nullptr)->std::tuple<QString,int>;

        auto GetName(void) const ->QString;
        auto GetType(void)const->int;

    protected slots:
        void on_createButton_clicked();
        void on_cancelButton_clicked();
        void OnPredCheckClicked();
        void OnPredComboChanged(int);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
