#pragma once

#include <memory>
#include <QWidget>

#include <IMaskPathPanel.h>

#include "ME2MaskPathPanelExport.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
    class ME2MaskPathPanel_API MaskPathPanel : public QWidget, public Interactor::IMaskPathPanel {
        Q_OBJECT
    public:
        typedef MaskPathPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        MaskPathPanel(QWidget* parent = nullptr);
        ~MaskPathPanel();

        auto Update() -> bool override;
        auto Reset() -> void override;
    signals:
        void sigMaskPath(QString);

    protected slots:
        void OnChangePathClicked();

    private:        
        struct Impl;
        std::shared_ptr<Impl> d;
    };
}