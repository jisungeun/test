#include <QSettings>
#include <QFileDialog>

#include "ui_ME2MaskPathPanel.h"
#include "ME2MaskPathPanel.h"

#include <iostream>

namespace TomoAnalysis::MaskEditor2d::Plugins {
    struct MaskPathPanel::Impl {
        Ui::MaskPathForm* ui{ nullptr };
    };
    MaskPathPanel::MaskPathPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::MaskPathForm();
        d->ui->setupUi(this);

        d->ui->lineEdit->setReadOnly(true);
        connect(d->ui->changeBtn, SIGNAL(clicked()), this, SLOT(OnChangePathClicked()));
    }
    MaskPathPanel::~MaskPathPanel() {
        
    }
    auto MaskPathPanel::Update() -> bool {
        auto ds = GetDS();
        d->ui->lineEdit->setText(ds->path);
        return true;
    }
    auto MaskPathPanel::Reset() -> void {
        d->ui->lineEdit->clear();
    }

    void MaskPathPanel::OnChangePathClicked() {
        if(false == d->ui->lineEdit->text().isEmpty()) {
            return;
        }
        auto ds = GetDS();
        if(ds->imageName.isEmpty()) {
            return;
        }
        QSettings qs("TA/MaskEditor");
        auto prevPath = qs.value("CurMaskPath", qApp->applicationDirPath()).toString();

        QDir dir(prevPath);        
        dir.makeAbsolute();        
        dir.cdUp();        

        auto final_path = dir.path() + "/" + ds->imageName + ".msk";

        const auto str = QFileDialog::getSaveFileName(this, "Select a mask path", final_path, "Tomocube mask (*.msk)",nullptr);

        if(str.isEmpty()) {
            return;
        }
        qs.setValue("CurMaskPath", str);

        emit sigMaskPath(str);
    }
}
