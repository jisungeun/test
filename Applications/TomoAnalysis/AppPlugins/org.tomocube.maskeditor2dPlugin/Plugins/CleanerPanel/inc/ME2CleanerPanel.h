#pragma once

#include <memory>
#include <QWidget>

#include <ICleanerPanel.h>

#include "ME2CleanerPanelExport.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
    class ME2CleanerPanel_API CleanerPanel : public QWidget, public Interactor::ICleanerPanel {
        Q_OBJECT
    public:
        typedef CleanerPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        CleanerPanel(QWidget* parent = nullptr);
        ~CleanerPanel();;

        auto Update() -> bool override;
        auto Reset() -> void override;

    signals:
        void sigClean(bool);
        void sigCleanSel(bool);
        void sigFlush(bool);
        void sigFlushSel(bool);

    protected slots:
        void OnCleanClicked();
        void OnCleanSelClicked();
        void OnFlushClicked();
        void OnFlushSelClicked();

    private:
        auto InitIcons()->void;
        auto Init()->void;
        auto InitToolTips()->void;
        auto CleanIcons()->void;
        auto SetIconOn()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}