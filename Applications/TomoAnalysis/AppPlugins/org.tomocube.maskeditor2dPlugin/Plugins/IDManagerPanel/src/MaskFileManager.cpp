#include <iostream>
#include <QDirIterator>
#include <QFileInfo>
#include <QString>

#include "MaskFileManager.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
    struct MaskFileManager::Impl {        
        QString inst;
        QString organ;
        QString image;
        QString baseName;
        QStringList scanList;

        QString parentFolder;
    };
    MaskFileManager::MaskFileManager(QObject* parent) : QObject(parent), d{ new Impl } {
        
    }
    MaskFileManager::~MaskFileManager() {
        
    }
    auto MaskFileManager::GetParentFolder() -> QString {
        return d->parentFolder;
    }
    auto MaskFileManager::Clear() -> void {        
        d->inst = QString();
        d->organ = QString();
        d->image = QString();
        d->baseName = QString();
        d->scanList.clear();
        d->parentFolder = QString();
    }

    auto MaskFileManager::SetImagePath(QString img) -> void {
        d->image = img;
    }
    auto MaskFileManager::GetImagePath() -> QString {
        return d->image;
    }
    auto MaskFileManager::SetOrganPath(QString organ) -> void {
        d->organ = organ;
    }
    auto MaskFileManager::GetOrganPath() -> QString {
        return d->organ;
    }
    auto MaskFileManager::SetInstPath(QString inst) -> void {
        d->inst = inst;
    }
    auto MaskFileManager::GetInstPath() -> QString {
        return d->inst;
    }
    auto MaskFileManager::GetScannedList() -> QStringList {
        return d->scanList;
    }
    auto MaskFileManager::ScanDir() -> void {
        //get base image name
        d->scanList.clear();
        d->parentFolder = QString();

        if (d->image.isEmpty()) {
            return;
        }
        QFileInfo imgInfo(d->image);
        if(false == imgInfo.exists()) {
            return;
        }
        QDir imgDir(d->image);
        imgDir.cdUp();
        d->parentFolder = imgDir.path();
        d->baseName = imgInfo.fileName().chopped(4);        

        auto startScan = false;

        //scan user for inst mask
        if(!d->inst.isEmpty()) {
            if (isValid(d->inst)) {
                if(d->inst.contains(d->baseName)) {
                    startScan = true;
                }
            }
        }
        //scan user for organ mask
        if(!d->organ.isEmpty()) {
            if(isValid(d->organ)) {
                if(d->organ.contains(d->baseName)) {
                    startScan = true;
                }
            }
        }
        if (startScan) {
            scanWithBase(d->baseName);
        }
    }
    auto MaskFileManager::scanWithBase(QString base) -> void {
        QString rootdir;
        QDir instPath(d->inst);
        instPath.cdUp();
        QDir organPath(d->organ);
        organPath.cdUp();
        if (instPath.exists()) {
            rootdir = instPath.path();
        }else if(organPath.exists()) {
            rootdir = organPath.path();
        }else {
            return;
        }        
        if(rootdir.isEmpty()) {
            return;
        }
        d->parentFolder = rootdir;
        QDirIterator it(rootdir, QStringList() << "*.msk", QDir::Files);
        while(it.hasNext()) {
            auto file_path = it.next();
            if(file_path.contains(base)) {
                auto tails = file_path.split(base)[1].chopped(4);
                auto split = tails.split("_");
                auto last_word = split[split.count() - 1];
                if(last_word == "MultiLayer" || last_word == "MultiLabel") {
                    //mask from system
                    if(false == d->scanList.contains("System")) {
                        d->scanList.push_back("System");
                    }
                }else if(last_word !="bak") {//ignore backupfile
                    //mask made by some user
                    if(false == d->scanList.contains(last_word)) {
                        d->scanList.push_back(last_word);                        
                    }
                }
            }
        }
    }    
    auto MaskFileManager::isSystem(QString path) -> bool {
        return true;
    }
    auto MaskFileManager::GetInstancePath(QString ID,bool isInst) -> QString {
        QString resultingPath = QString();
        auto isSystem = (ID == "System");
        if(isInst) {
            if (d->inst.isEmpty()) {
                resultingPath += d->image.chopped(4);
                if (isSystem) {
                    resultingPath += "_MultiLabel.msk";
                }else {
                    resultingPath += "_MultiLabel_" + ID + ".msk";
                }
                return resultingPath;
            }                
            auto tails = d->inst.chopped(4);
            auto split = tails.split("_");
            auto last_word = split[split.count() - 1];
            if(last_word == "MultiLabel") {
                if(isSystem){
                    resultingPath = tails + ".msk";
                }else {
                    resultingPath = tails + "_" + ID + ".msk";
                }
            }else if(last_word!="bak") {
                if(isSystem) {
                    resultingPath = tails.chopped(last_word.length() + 1) + ".msk";
                }
                else {
                    resultingPath = tails.chopped(last_word.length() + 1) + "_" + ID + ".msk";
                }
            }
        }else {
            if(d->organ.isEmpty()) {
                resultingPath += d->image.chopped(4);
                if(isSystem) {
                    resultingPath += "_MultiLayer.msk";
                }
                else {
                    resultingPath += "_MultiLayer_" + ID + ".msk";
                }
                return resultingPath;
            }
            auto tails = d->organ.chopped(4);
            auto split = tails.split("_");
            auto last_word = split[split.count() - 1];
            if (last_word == "MultiLayer") {
                if (isSystem) {
                    resultingPath = tails +".msk";
                }else {
                    resultingPath = tails + "_" + ID + ".msk";
                }                
            }else if(last_word != "bak") {
                if(isSystem) {
                    resultingPath = tails.chopped(last_word.length() + 1) + ".msk";
                }else {
                    resultingPath = tails.chopped(last_word.length() + 1) + "_" + ID + ".msk";
                }                
            }
        }
        
        return resultingPath;
    }
    auto MaskFileManager::isValid(QString path) -> bool {
        QFileInfo info(path);
        if(!info.exists()) {
            return false;
        }        
        return (info.suffix() == "msk");
    }
}