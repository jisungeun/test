#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::MaskEditor2d::Plugins {
    class NewIdDialog : public QDialog {
        Q_OBJECT

    public:
        explicit NewIdDialog(QWidget* parent);
        virtual ~NewIdDialog();

    public:
        static auto NewID(QWidget* parent)->QString;

        auto GetNewName(void) const ->QString;

    protected slots:
        void on_okButton_clicked();
        void on_cancelButton_clicked();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
