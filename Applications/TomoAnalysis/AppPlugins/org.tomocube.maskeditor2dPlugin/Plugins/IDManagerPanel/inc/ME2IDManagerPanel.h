#pragma once

#include <memory>
#include <QWidget>

#include <IIDManagerPanel.h>

#include "ME2IDManagerPanelExport.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
    class ME2IDManagerPanel_API IDManagerPanel : public QWidget,public Interactor::IIDManagerPanel {
        Q_OBJECT
    public:
        typedef IDManagerPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        IDManagerPanel(QWidget* parent = nullptr);
        ~IDManagerPanel();

        auto Update() -> bool override;
        auto Reset() -> void override;

    signals:
        void sigIdChanged(QString,QString,QString);
        void sigIdCopy(QString,QString,QString,QString,QString);

    protected slots:
        void OnNewIdClicked();
        void OnCopyIdClicked();
        void OnIdChanged(int);
        void OnOpenWorkingDir();

    private:        
        auto Init()->void;
        auto CopyPath(QString src, QString dst)->void;
        auto IDExist(QStringList prevIDs, QString newID)->bool;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}