#include <iostream>

#include <QFileDialog>
#include <QIcon>
#include <QSettings>

#include <ME2WorkingSet.h>

#include "MaskSelectionDialog.h"

#include "ui_ME2GeneralPanel.h"
#include "ME2GeneralPanel.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
    struct GeneralPanel::Impl {
        Ui::GeneralForm* ui{ nullptr };
        QIcon SaveIcon;
        QIcon SaveLinkIcon;
        QIcon WindowIcon;
        QIcon LoadIcon;

        QString cur_user;
        QString cur_img;

        struct {
            const char* recentMaskFolder = "Recent/MaskFolder";
        } entry;
    };

    GeneralPanel::GeneralPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::GeneralForm;
        d->ui->setupUi(this);
        this->InitIcons();
        this->Init();
        this->InitToolTips();
    }
    GeneralPanel::~GeneralPanel() = default;
    auto GeneralPanel::Reset() -> void {
        auto ds = GetDS();
        ds->user_name = QString();
        ds->file_name = QString();        
        ds->standAlone = true;
        Update();
    }
    auto GeneralPanel::Update() -> bool {
        auto ds = GetDS();        
        d->cur_user = ds->user_name;
        d->cur_img = ds->file_name;
        if(ds->standAlone) {            
            d->ui->SaveLinkBtn->setEnabled(false);
            d->ui->SaveLinkBtn->setToolTip(QString("Apply current mask to application"));
        }else {
            d->ui->SaveLinkBtn->setEnabled(true);
            if (false == ds->linked_app.isEmpty()) {
                d->ui->SaveLinkBtn->setToolTip(QString("Apply current mask to %1").arg(ds->linked_app));
            }
        }        
        return true;
    }
    void GeneralPanel::OnLoadClicked() {
        const auto prevDir = QSettings("Tomocube", "TomoAnalysis")
            .value(d->entry.recentMaskFolder).toString();
        QString file_path = QFileDialog::getOpenFileName(this, "Open mask",
            prevDir,
            tr("Tomocube mask (*.msk)"));
        if(true == file_path.isEmpty()) {
            return;
        }

        const QFileInfo fileInfo(file_path);
        QSettings("Tomocube", "TomoAnalysis").setValue(d->entry.recentMaskFolder,
            fileInfo.absoluteDir().absolutePath());

        emit sigLoad(file_path);
    }
    void GeneralPanel::OnSaveClicked() {
        /*auto ws = Entity::WorkingSet::GetInstance();
        auto instExist = nullptr != ws->GetInstData();
        auto organExist = nullptr != ws->GetOrganData();
        QStringList orgList;
        if(organExist) {
            orgList = ws->GetOrganData()->GetLayerNames();
        }
        auto result = MaskSelectionDialog::MaskSelection(nullptr,instExist,organExist,orgList);

        if(result.nameList.count()<1) {
            return;
        }

        const auto prevDir = QSettings("Tomocube", "TomoAnalysis")
            .value(d->entry.recentMaskFolder).toString();
                
        auto recommandName = prevDir + "/" + d->cur_img + "_" + d->cur_user + "_" + result.type + ".msk";
        if(result.type =="MultiLayer") {            
            if(!ws->GetOrganPath().isEmpty()) {
                recommandName = ws->GetOrganPath();
            }            
        }else if(result.type == "MultiLabel") {
            if(!ws->GetInstPath().isEmpty()) {
                recommandName = ws->GetInstPath();
            }            
        }
        QString file_path = QFileDialog::getSaveFileName(this, "Save mask",
            recommandName,
            tr("Tomocube mask (*.msk)"));
        if(file_path.isEmpty()) {
            return;
        }
        emit sigSave(file_path, result.type, result.nameList);*/
    }
    void GeneralPanel::OnSaveLinkClicked() {
        emit sigSaveLink();
    }
    void GeneralPanel::OnWindowClicked() {
        emit sigWindow();
    }
    auto GeneralPanel::Init() -> void {
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->contentsWidget->setObjectName("panel-contents");
        connect(d->ui->SaveBtn, SIGNAL(clicked()), this, SLOT(OnSaveClicked()));
        connect(d->ui->SaveLinkBtn, SIGNAL(clicked()), this, SLOT(OnSaveLinkClicked()));
        connect(d->ui->WinBtn, SIGNAL(clicked()), this, SLOT(OnWindowClicked()));
        connect(d->ui->LoadBtn, SIGNAL(clicked()), this, SLOT(OnLoadClicked()));

        d->ui->SaveBtn->hide();
        d->ui->WinBtn->hide();
    }
    auto GeneralPanel::InitToolTips() -> void {
        d->ui->SaveBtn->setToolTip("Save current mask (*.msk)");
        d->ui->SaveLinkBtn->setToolTip("Apply current mask to application");
        d->ui->WinBtn->setToolTip("Switch main(big) window");
        d->ui->LoadBtn->setToolTip("Load mask file (*.msk)");
    }

    auto GeneralPanel::InitIcons() -> void {
        d->SaveIcon = QIcon(":/image/images/Save.png");
        d->ui->SaveBtn->setIcon(d->SaveIcon);
        d->ui->SaveBtn->setIconSize(QSize(52, 52));

        d->SaveLinkIcon = QIcon(":/image/images/SaveToLink.png");
        d->ui->SaveLinkBtn->setIcon(d->SaveLinkIcon);
        d->ui->SaveLinkBtn->setIconSize(QSize(52, 52));

        d->WindowIcon = QIcon(":/image/images/Window.png");
        d->ui->WinBtn->setIcon(d->WindowIcon);
        d->ui->WinBtn->setIconSize(QSize(52, 52));

        d->LoadIcon = QIcon(":/image/images/Load.png");
        d->ui->LoadBtn->setIcon(d->LoadIcon);
        d->ui->LoadBtn->setIconSize(QSize(52, 52));
    }
}