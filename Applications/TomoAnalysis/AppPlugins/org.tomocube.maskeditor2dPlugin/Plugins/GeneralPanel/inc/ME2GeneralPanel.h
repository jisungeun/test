#pragma once

#include <memory>
#include <QWidget>

#include <IGeneralPanel.h>

#include "ME2GeneralPanelExport.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
    class ME2GeneralPanel_API GeneralPanel : public QWidget,public Interactor::IGeneralPanel {
        Q_OBJECT
    public:
        typedef GeneralPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        GeneralPanel(QWidget* parent = nullptr);
        ~GeneralPanel();

        auto Update() -> bool override;
        auto Reset() -> void override;
    signals:
        void sigLoad(QString);
        void sigSave(QString,QString,QStringList);
        void sigSaveLink();
        void sigWindow();

    protected slots:
        void OnLoadClicked();
        void OnSaveClicked();
        void OnSaveLinkClicked();
        void OnWindowClicked();

    private:
        auto InitIcons()->void;
        auto Init()->void;
        auto InitToolTips()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}