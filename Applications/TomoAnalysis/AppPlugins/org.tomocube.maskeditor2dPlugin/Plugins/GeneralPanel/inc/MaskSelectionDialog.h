#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::MaskEditor2d::Plugins {
    struct SelectedMask {
        QString type;
        QStringList nameList;
    };
    class MaskSelectionDialog : public QDialog {
        Q_OBJECT

    public:
        explicit MaskSelectionDialog(QWidget* parent);
        virtual ~MaskSelectionDialog();

    public:
        static auto MaskSelection(QWidget* parent,bool isInst = false,bool isOrgan = false,QStringList orgList = QStringList())->SelectedMask;

        auto GetMaskType()const->QString;
        auto GetSelectedMaskList()const->QStringList;

    protected slots:
        void on_okButton_clicked();
        void on_cancelButton_clicked();

        void OnInstRadio();
        void OnOrganRadio();
        void OnAllCheck(int);

    private:
        auto enableInst(bool enable)->void;
        auto enableOrgan(bool enable)->void;
        auto setOrganList(QStringList orgList)->void;
        auto GetDisplayName(QString synName)const->QString;
        auto GetSyntheticName(QString disName)const->QString;
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
