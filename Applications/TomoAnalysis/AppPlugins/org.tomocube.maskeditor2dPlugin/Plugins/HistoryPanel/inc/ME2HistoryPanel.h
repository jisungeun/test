#pragma once

#include <memory>
#include <QWidget>

#include <IHistoryPanel.h>

#include "ME2HistoryPanelExport.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
    class ME2HistoryPanel_API HistoryPanel : public QWidget, public Interactor::IHistoryPanel {
        Q_OBJECT
    public:
        typedef HistoryPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        HistoryPanel(QWidget* parent = nullptr);
        ~HistoryPanel();

        auto Update() -> bool override;
        auto Reset() -> void override;
    signals:
        void sigUndo();
        void sigRedo();
        void sigRefresh();
    protected slots:
        void OnUndoClicked();
        void OnRedoClicked();
        void OnRefreshClicked();

    private:
        auto InitIcons()->void;
        auto Init()->void;
        auto InitToolTips()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}