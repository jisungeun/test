#pragma once

#include <memory>
#include <QWidget>

#include <IFLControlPanel.h>

#include "ME2FLControlPanelExport.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
    class ME2FLControlPanel_API FLControlPanel : public QWidget, public Interactor::IFLControlPanel {
        Q_OBJECT
    public:
        typedef FLControlPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        FLControlPanel(QWidget* parent = nullptr);
        ~FLControlPanel();

        auto Update() -> bool override;
        auto Reset() -> void override;
    signals:
        void flOpacity(int ch, double opa);
        void flChSelected(int ch);
        void flColorChanged(int ch, QColor col);

    protected slots:
        void OnOpacityChanged(double val);
        void OnOpaSliderChanged(int slide);
        void OnRadioClicked();
        void OnColorClicked();
    private:
        auto Init()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}