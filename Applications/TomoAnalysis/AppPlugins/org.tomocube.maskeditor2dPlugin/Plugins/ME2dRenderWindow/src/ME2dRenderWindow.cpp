#include <QLabel>
#include <QMenu>
#include <QVBoxLayout>
#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoInfo.h>
#include <Inventor/nodes/SoImage.h>

#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/nodes/SoEventCallback.h>

#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include <RangeSlider.h>
#include <SpinBoxAction.h>

#include "ME2dRenderWindow.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
    struct MERenderWindow2D::Impl {
        //colormap
        int colorMap{ 0 }; //0: grayscale 1: inverse grayscale 2: hot iron 3: JET 4:Rainbow

        bool leftShiftPressed{ false };
        bool rightShiftPressed{ false };

        TC::SpinBoxAction* levelWindow;
        int minLevelWindow{ 0 };
        int maxLevelWindow{ 100 };

        bool right_mouse_pressed{ false };
        bool middle_mouse_pressed{ false };
        bool left_mouse_pressed{ false };
        double right_stamp{ 0.0 };
        double prev_scale{ 1.0 };
        bool is2D{ false };

        int ID{ -1 };
        bool is_reload{ false };

        bool instantNavi{ false };
    };

    MERenderWindow2D::MERenderWindow2D(QWidget* parent)
        : QOivRenderWindow(parent, true)
        , d{ new Impl } {
        setDefaultWindowType();

        d->levelWindow = new TC::SpinBoxAction("RI Range");
        connect(d->levelWindow->spinBox(), SIGNAL(lowerValueChanged(int)), this, SLOT(lowerLevelChanged(int)));
        connect(d->levelWindow->spinBox(), SIGNAL(upperValueChanged(int)), this, SLOT(upperLevelChanged(int)));
        connect(d->levelWindow, SIGNAL(sigMinChanged(double)), this, SLOT(lowerSpinChanged(double)));
        connect(d->levelWindow, SIGNAL(sigMaxChanged(double)), this, SLOT(upperSpinChanged(double)));

        forceArrowCursor(true);
    }

    MERenderWindow2D::~MERenderWindow2D() {

    }
    auto MERenderWindow2D::setRenderWindowID(int idx) -> void {
        d->ID = idx;
    }

    auto MERenderWindow2D::reset2DView(bool fromFunc) -> void {
        //find which axis
        MedicalHelper::Axis ax;
        auto root = getSceneGraph();

        SoVolumeData* volData = volData = MedicalHelper::find<SoVolumeData>(root, "volData");//find any volume Data

        if (volData) {
            float slack;
            auto dims = volData->getDimension();
            auto spacing = volData->getVoxelSize();

            float x_len = dims[0] * spacing[0];
            float y_len = dims[1] * spacing[1];
            float z_len = dims[2] * spacing[2];
            if (d->ID == 0) {
                ax = MedicalHelper::Axis::AXIAL;
                slack = y_len / x_len;
            }
            else if (d->ID == 1) {
                ax = MedicalHelper::SAGITTAL;
                slack = z_len / y_len;
            }
            else if (d->ID == 2) {
                ax = MedicalHelper::CORONAL;
                slack = z_len / x_len;
            }
            else {
                return;
            }
            auto windowSlack = static_cast<float>(size().height()) / static_cast<float>(size().width());
            if (windowSlack > slack) {
                slack = windowSlack > 1 ? 1.0 : windowSlack;
            }
            MedicalHelper::orientView(ax, getCamera(), volData, slack);
            if (fromFunc) {
                //reset range
                auto range_min = MedicalHelper::find<SoInfo>(root, "HTMin");
                if (range_min) {
                    auto min_val = range_min->string.getValue().toInt();
                    auto range_max = MedicalHelper::find<SoInfo>(root, "HTMax");
                    auto max_val = range_max->string.getValue().toInt();

                    d->levelWindow->setLower(min_val);
                    d->levelWindow->setUpper(max_val);
                }
                //reset colormap
                change2DColorMap(0);
            }
            //change2DColorMap(0);
            //d->levelWindow->setLower(d->minLevelWindow);
            //d->levelWindow->setUpper(d->maxLevelWindow);
        }
    }

    auto MERenderWindow2D::setType(bool type) -> void {
        d->is2D = type;
    }

    void MERenderWindow2D::lowerSpinChanged(double val) {
        auto int_val = static_cast<int>(val * 10000.0);
        auto rootScene = getSceneGraph();
        if (rootScene) {
            auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
            if (range) {
                range->min = int_val;
            }
            auto mipRange = MedicalHelper::find<SoDataRange>(rootScene, "MIPrange2D");
            if (mipRange) {
                mipRange->min = int_val;
            }
        }
    }
    void MERenderWindow2D::lowerLevelChanged(int value) {
        d->levelWindow->setLower(value, false);
        //d->minLevelWindow = value;

        //auto rootScene = getSceneGraph();

        auto dataRangeNode = MedicalHelper::find<SoDataRange>(getSceneGraph(), "HTSliceRange");
        if (dataRangeNode == nullptr) {
            return;
        }

        //dataRangeNode->min = d->minLevelWindow;
        dataRangeNode->min = value;

        auto minInfo = MedicalHelper::find<SoInfo>(getSceneGraph(), "HTMin");
        if (nullptr != minInfo) {
            minInfo->string.setValue(std::to_string(value));
        }
    }

    void MERenderWindow2D::upperSpinChanged(double val) {
        auto int_val = static_cast<int>(val * 10000.0);
        auto rootScene = getSceneGraph();
        if (rootScene) {
            auto range = MedicalHelper::find<SoDataRange>(rootScene, "HTSliceRange");
            if (range) {
                range->max = int_val;
            }
            auto mipRange = MedicalHelper::find<SoDataRange>(rootScene, "MIPrange2D");
            if (mipRange) {
                mipRange->max = int_val;
            }
        }
    }

    void MERenderWindow2D::upperLevelChanged(int value) {
        d->levelWindow->setUpper(value, false);
        //d->maxLevelWindow = value;

        auto dataRangeNode = MedicalHelper::find<SoDataRange>(getSceneGraph(), "HTSliceRange");
        if (dataRangeNode == nullptr) {
            return;
        }

        //dataRangeNode->max = d->maxLevelWindow;
        dataRangeNode->max = value;

        auto maxInfo = MedicalHelper::find<SoInfo>(getSceneGraph(), "HTMax");
        if (nullptr != maxInfo) {
            maxInfo->string.setValue(std::to_string(value));
        }
    }

    auto MERenderWindow2D::MouseMoveEvent(SoEventCallback* node) -> void {
        const SoLocation2Event* moveEvent = (const SoLocation2Event*)node->getEvent();
        if (isNavigation() || d->instantNavi) {
            auto pos = moveEvent->getPositionFloat();
            auto viewport_size = getViewportRegion().getViewportSizePixels();
            auto root = getSceneGraph();
            auto color = MedicalHelper::find<SoImage>(root, "icon_file");
            if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                auto image = new QImage(":/img/ic-setting-s.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            else {
                auto image = new QImage(":/img/ic-setting.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            if (d->right_mouse_pressed) {
                auto diff = moveEvent->getNormalizedPosition(getViewportRegion())[1] - d->right_stamp;
                auto factor = 1.0 - diff * 1.5;
                if (factor > 0) {
                    getCamera()->scaleHeight(factor / d->prev_scale);
                    d->prev_scale = factor;
                }
            }
            if (d->left_mouse_pressed) {
                if (!d->is2D) {
                    auto coord = CalcVolumeCoord(moveEvent->getNormalizedPosition(getViewportRegion()));
                    emit sig2dCoord(coord[0], coord[1], coord[2]);
                }
                node->setHandled();
            }
            if (d->middle_mouse_pressed) {
                if (!d->is2D) {
                    //panCamera(moveEvent->getPositionFloat());
                    panCamera(moveEvent->getNormalizedPosition(getViewportRegion()));
                }
            }
        }
    }

    auto MERenderWindow2D::MouseWheelEvent(SoEventCallback* node) -> void {
        if (!d->is2D) {
            const int MOUSE_WHEEL_DELTA = 120;
            const SoMouseWheelEvent* wheelEvent = (SoMouseWheelEvent*)node->getEvent();
            int delta = wheelEvent->getDelta() / MOUSE_WHEEL_DELTA;
            emit sigWheel(delta);
        }
        //node->setHandled();
    }
    /*void MERenderWindow2D::wheelEvent(QWheelEvent* event) {
        if (!d->is2D) {
            auto delta = (event->angleDelta().y() > 0) ? 1 : -1;
            emit sigWheel(delta);
        }
    }*/

    auto MERenderWindow2D::MouseButtonEvent(SoEventCallback* node) -> void {
        const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)node->getEvent();

        if (!isNavigation()) {//selection mode
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                d->instantNavi = true;
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                d->instantNavi = true;
            }
        }
        if (isNavigation() || d->instantNavi) {//navigation mode
            auto pos = mouseButton->getPositionFloat();
            auto vr = getViewportRegion();
            auto normpos = mouseButton->getNormalizedPosition(vr);
            auto viewport_size = vr.getViewportSizePixels();
            auto mouse_in_setting = (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25);
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                if (!mouse_in_setting) {
                    d->right_mouse_pressed = true;
                    d->right_stamp = normpos[1];
                    d->prev_scale = 1.0;
                }
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                d->right_mouse_pressed = false;
                d->right_stamp = 0.0;
                d->prev_scale = 1.0;
                if (d->instantNavi) {
                    d->instantNavi = false;
                }
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                d->middle_mouse_pressed = true;
                //startPan(mouseButton->getPosition());
                startPan(mouseButton->getNormalizedPosition(getViewportRegion()));
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                d->middle_mouse_pressed = false;
                if (d->instantNavi) {
                    d->instantNavi = false;
                }
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                if (!mouse_in_setting) {
                    d->left_mouse_pressed = true;
                    if (!d->is2D) {
                        auto coord = CalcVolumeCoord(normpos);
                        emit sig2dCoord(coord[0], coord[1], coord[2]);
                    }
                }
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                if (mouse_in_setting) {
                    showContextMenu(pos);
                }
                d->left_mouse_pressed = false;
            }
        }
    }
    auto MERenderWindow2D::GetID() -> int {
        return d->ID;
    }
    auto MERenderWindow2D::KeyboardEvent(SoEventCallback* node) -> void {
        if (SO_KEY_PRESS_EVENT(node->getEvent(), LEFT_SHIFT)) {
            d->leftShiftPressed = true;
        }

        if (SO_KEY_PRESS_EVENT(node->getEvent(), RIGHT_SHIFT)) {
            d->rightShiftPressed = true;
        }

        if (SO_KEY_RELEASE_EVENT(node->getEvent(), LEFT_SHIFT)) {
            d->leftShiftPressed = false;
        }

        if (SO_KEY_RELEASE_EVENT(node->getEvent(), RIGHT_SHIFT)) {
            d->rightShiftPressed = false;
        }

        if (SoKeyboardEvent::isKeyPressEvent(node->getEvent(), SoKeyboardEvent::Key::ESCAPE)) {
            if (isNavigation()) {
                //setInteractionMode(false);
            }
            else {
                //setInteractionMode(true);
            }
        }
    }

    auto MERenderWindow2D::closeEvent(QCloseEvent* unused) -> void {
        Q_UNUSED(unused)
    }

    auto MERenderWindow2D::setDefaultWindowType() -> void {
        // set default gradient background color
        SbVec3f start_color = { 0.0,0.0,0.0 };
        SbVec3f end_color = { 0.0,0.0,0.0 };
        setGradientBackground(start_color, end_color);

        // set default camera type
        setCameraType(true);    // orthographic        

        // set default navigation type
    }

    auto MERenderWindow2D::showContextMenu(SbVec2f pos) -> void {
        auto new_y = getQtRenderArea()->size().height() - pos[1];
        QPoint globalPos = this->mapToGlobal(QPoint(pos[0], new_y));

        QMenu myMenu;

        QMenu* cmMenu = myMenu.addMenu(tr("ColorMap"));
        QAction* cmOptions[5];
        cmOptions[0] = cmMenu->addAction("Grayscale");
        cmOptions[1] = cmMenu->addAction("Inverse grayscale");
        cmOptions[2] = cmMenu->addAction("Hot iron");
        cmOptions[3] = cmMenu->addAction("JET");
        cmOptions[4] = cmMenu->addAction("Rainbow");

        for (int i = 0; i < 5; i++) {
            cmOptions[i]->setCheckable(true);
        }
        cmOptions[d->colorMap]->setChecked(true);

        if (d->is_reload) {
            initRangeSlider();
            d->is_reload = false;
        }

        QMenu* levelMenu = myMenu.addMenu(tr("Level Window"));
        levelMenu->addAction(d->levelWindow);

        myMenu.addAction("Reset View");

        QAction* selectedItem = myMenu.exec(globalPos);

        if (selectedItem) {
            auto text = selectedItem->text();
            if (text.contains("Inverse")) {
                change2DColorMap(1);
            }
            else if (text.contains("Grayscale")) {
                change2DColorMap(0);
            }
            else if (text.contains("Hot")) {
                change2DColorMap(2);
            }
            else if (text.contains("JET")) {
                change2DColorMap(3);
            }
            else if (text.contains("Rainbow")) {
                change2DColorMap(4);
            }
            else if (text.contains("Reset View")) {
                reset2DView(true);
            }
        }
    }
    auto MERenderWindow2D::change2DColorMap(int idx) -> void {
        auto rootScene = getSceneGraph();
        if (rootScene) {
            auto tf = MedicalHelper::find<SoTransferFunction>(rootScene, "colorMapHTSlice");
            if (tf) {
                int pred = 0;
                switch (idx) {
                case 0: // grayscale
                    pred = SoTransferFunction::PredefColorMap::INTENSITY;
                    break;
                case 1: // inversegrayscale
                    pred = SoTransferFunction::PredefColorMap::INTENSITY_REVERSED;
                    break;
                case 2: // Hot Iron - GLOW in OIV
                    pred = SoTransferFunction::PredefColorMap::GLOW;
                    break;
                case 3: // JET - PHYSICS in OIV
                    pred = SoTransferFunction::PredefColorMap::PHYSICS;
                    break;
                case 4: // Rainbow - STANDARD in OIV
                    pred = SoTransferFunction::PredefColorMap::STANDARD;
                    break;
                }
                d->colorMap = idx;
                tf->predefColorMap = pred;

            }
        }
    }
    auto MERenderWindow2D::initRangeSlider() -> void {
        auto newScene = getSceneGraph();
        if (newScene) {
            //auto cur_time = MedicalHelper::find<SoInfo>(newScene, "HT_CUR_TIME");
            //auto int_cur_time = cur_time->string.getValue().toInt();
            //auto buf_idx = int_cur_time % 2;//buffer index is 2 for now
            auto volName = "volData";// +std::to_string(buf_idx);


            auto volData = MedicalHelper::find<SoVolumeData>(newScene, volName);
            if (volData) {
                double min, max;
                volData->getMinMax(min, max);
                d->minLevelWindow = min;
                d->maxLevelWindow = max;
                d->levelWindow->setMinMax(d->minLevelWindow, d->maxLevelWindow);
                d->levelWindow->setLower(d->minLevelWindow);
                d->levelWindow->setUpper(d->maxLevelWindow);
            }
        }
    }
    auto MERenderWindow2D::refreshRangeSlider() -> void {
        d->is_reload = true;
    }
    auto MERenderWindow2D::SetInteractionMode(bool isNavi) -> void {
        if (isNavi) {
            setInteractionMode(false);
        }
        else {
            setInteractionMode(true);
        }
    }
    auto MERenderWindow2D::setHTRange(double min, double max) -> void {
        d->maxLevelWindow = max;
        d->minLevelWindow = min;
        d->levelWindow->blockSignals(true);
        d->levelWindow->setMinMax(d->minLevelWindow, d->maxLevelWindow);
        d->levelWindow->setLower(d->minLevelWindow);
        d->levelWindow->setUpper(d->maxLevelWindow);
        d->levelWindow->blockSignals(false);
    }

    auto MERenderWindow2D::CalcVolumeCoord(SbVec2f norm_point) -> SbVec3f {
        SoRayPickAction rayPick = SoRayPickAction(getViewportRegion());
        SbVec2f normPoint = norm_point;
        //normPoint[0] = (normPoint[0] + 1.0) / 2.0;
        //normPoint[1] = (normPoint[1] + 1.0) / 2.0;

        auto root = getSceneGraph();

        rayPick.setNormalizedPoint(normPoint);
        rayPick.apply(root);

        SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
        if (pickedPoint) {
            SoVolumeData* volData = volData = MedicalHelper::find<SoVolumeData>(root, "volData");//find any volume Data
            if (volData) {
                auto pt = pickedPoint->getPoint();
                auto coord = volData->XYZToVoxel(pt);
                return coord;
            }
        }
        return SbVec3f();
    }

}
