#include <tuple>

#include <QCoreApplication>
#include <QMap>

#include <ME2dRenderWindow.h>

#pragma warning(push)
#pragma warning(disable:4819)
//Open Inventor
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoInfo.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/Axis.h>
#include <Inventor/ViewerComponents/nodes/SoViewingCube.h>

#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeIsosurface.h>

#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoResetImageProcessing.h>
#include <ImageViz/Engines/ImageAnalysis/Statistics/SoMaskedStatisticsQuantification.h>
#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoConvertImageProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoCombineByMaskProcessing.h>
#include <ImageViz/Engines/ImageAnalysis/Statistics/SoIntensityStatisticsQuantification.h>
#include <ImageViz/Engines/ImageSegmentation/Labeling/SoReorderLabelsProcessing.h>

#include <ImageViz/Engines/ArithmeticAndLogic/LogicalOperations/SoLogicalNotProcessing.h>
#include <ImageViz/Engines/MathematicalMorphology/ErosionAndDilation/SoErosionBallProcessing3d.h>
#include <ImageViz/Engines/MathematicalMorphology/ErosionAndDilation/SoDilationBallProcessing3d.h>
#include <ImageViz/Engines/MathematicalMorphology/ErosionAndDilation/SoDilationCubeProcessing.h>
#include <ImageViz/Engines/MathematicalMorphology/ErosionAndDilation/SoErosionCubeProcessing.h>
#include <ImageViz/Engines/ImageSegmentation/Binarization/SoThresholdingProcessing.h>
#include <ImageViz/Engines/ImageSegmentation/RegionGrowing/SoMarkerBasedWatershedProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoArithmeticValueProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoMaskImageProcessing.h>
#include <ImageViz/Engines/ImageAnalysis/IndividualMeasures/SoLabelAnalysisQuantification.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <ImageViz/Nodes/Measures/SoDataMeasurePredefined.h>

#include <Medical/Helpers/MedicalHelper.h>
#include <Medical/helpers/VolumeMarchingCubes.h>
#pragma warning(pop)

#include <TCDataConverter.h>
#include <TCAiInteractiveSeg.h>

#include <Oiv2DDrawer.h>
#include <Oiv3DDrawer.h>
#include <OivFreeLine.h>
#include <OivMaskGenerator.h>
#include <OivScaleBar.h>
#include <PythonNumpyIO.h>
#include "ME2SceneManager.h"

#include <QLayout>

namespace TomoAnalysis::MaskEditor2d::Plugins {
	float color_table[20][3]{
	230,25,75, //red
	60,180,75, //green
	255,255,25, //yellow
	0,130,200, //blue
	245,130,48, //Orange
	145,30,180, //Purple
	70,240,240, //Cyan
	240,50,230, //Magenta
	210,245,60, //Lime
	250,190,212, //Pink
	0,128,128, //Teal
	220,190,255, //Lavender
	170,110,40, //Brown
	255,250,200, //Beige
	128,0,0,//Maroon
	170,255,195, //Mint
	128,128,0, //Olive
	255,215,180, //Apricot
	0,0,128, //Navy
	128,128,128 //Gray
	};

	struct SceneManager::Impl {
		int curTime{ -1 };
		QString curImageName{ QString() };
		SoVolumeData* curImage{ nullptr };
		QString curMaskName{ QString() };
		SoVolumeData* curMask{ nullptr };//working file
		MaskTypeEnum curMaskType{ MaskTypeEnum::None };
		SoVolumeData* mergerTarget{ nullptr };
		SoVolumeData* sizeTarget{ nullptr };
		SoVolumeData* curFL{ nullptr };
		SoSwitch* imageSocket;
		SoSwitch* maskSocket;

		SoInfo* htMinInfo;
		SoInfo* htMaxInfo;

		//SceneGraphs        
		//2D
		SoSeparator* root2d;
		SoSwitch* tempViz;
		SoSeparator* orthoSep;
		SoSeparator* orthoSepMask;
		SoSeparator* orthoSepMerge;
		SoSeparator* orthoSepFL;
		SoOrthoSlice* orthoSlice;
		SoOrthoSlice* orthoSliceMask;
		SoOrthoSlice* orthoSliceMerge;
		SoOrthoSlice* orthoSliceFL;
		SoOrthoSlice* orthoSliceSize;

		SoSwitch* scaleBarSwitch = nullptr;
		
		//2D drawers
		SoSeparator* drawerSep;
		Oiv2DDrawer* drawer{nullptr};
		SoSeparator* drawerSep3d;
		Oiv3DDrawer* drawer3d{ nullptr };
		uint32_t paintSize{ 5 };
		uint32_t wipeSize{ 5 };

		SoGroup* imageGroup;
		SoMaterial* matlImage;
		SoTransferFunction* tfImage;
		SoDataRange* dataRange;

		SoGroup* maskGroup;
		SoMaterial* matlMaskVolume;
		SoMaterial* matlMaskSlice;
		SoMaterial* matlMerge;
		SoMaterial* matlSize;
		SoMaterial* matlFL;
		SoTransferFunction* tfMask;
		SoDataRange* dataRangeMask;
		SoDataRange* dataRangeTemp;

		SoSwitch* mergeSocket;
		SoGroup* mergeGroup;
		SoTransferFunction* tfMerge;
		SoDataRange* dataRangeMerge;

		SoSwitch* sizeSocket;
		SoGroup* sizeGroup;
		SoTransferFunction* tfSize;
		SoDataRange* dataRangeSize;

		SoSwitch* flSocket;
		SoGroup* flGroup;
		SoTransferFunction* tfFL;
		SoDataRange* dataRangeFL;

		SoSeparator* imageSliceSep;
		//slice volume holding socket
		SoSwitch* imageSliceSocket;
		//dataRangeMask
		SoTransferFunction* tfSlice;
		SoOrthoSlice* orthoSliceTemp;

		QList<int> editHistory;
		QList<int> undoHistory;

		//SoSwitch* labelTextSwitch{ nullptr };

		int curLevelMin, curLevelMax;

		int curAxis{ -1 };
		int curIndex{ -1 };
		int curLargeAxis{ 0 };
		int curIndices[3]{ 0, };
		int curLabelValue{ 1 };
		int curLabelMax{ 1 };

		MERenderWindow2D* renWin{ nullptr };
	};
	SceneManager::SceneManager() : d{ new Impl } {
		//d->isIsoSurface = true;
		Init();
		d->renWin = new MERenderWindow2D(nullptr);
		d->renWin->setRenderWindowID(0);
		d->renWin->setSceneGraph(d->root2d);
		//d->renWin->refreshRangeSlider();
		OivFreeLine::initClass();
	}
	SceneManager::~SceneManager() {
		OivFreeLine::exitClass();
	}

	auto SceneManager::SetSceneContainer(QWidget* container) -> void {
		container->layout()->addWidget(d->renWin);
	}

	auto SceneManager::SetSliceIndex(int viewIndex, int sliceIndex) -> void {
		SoOrthoSlice* imageSlice = nullptr;
		SoOrthoSlice* maskSlice = nullptr;
		SoOrthoSlice* mergeSlice = nullptr;
		SoOrthoSlice* flSlice = nullptr;

		imageSlice = d->orthoSlice;
		maskSlice = d->orthoSliceMask;
		mergeSlice = d->orthoSliceMerge;
		flSlice = d->orthoSliceFL;

		if (imageSlice == nullptr || maskSlice == nullptr) {
			return;
		}

		// slice index 범위 확인
		auto count = d->curImage->data.getSize()[viewIndex];
		if (sliceIndex < 0 || sliceIndex >= count) {
			return;
		}

		// 표시할 image & mask slice index 변경
		imageSlice->sliceNumber = maskSlice->sliceNumber = mergeSlice->sliceNumber = flSlice->sliceNumber = sliceIndex;
		d->curIndices[viewIndex] = sliceIndex;
	}
	auto SceneManager::CopyFromNumPy(const QString& path) ->std::tuple<bool,int,int> {
		if(nullptr == d->curMask) {
			return std::make_tuple(false,0,0);
		}
		std::shared_ptr<TC::IO::TCNumpyReader> reader(new TC::IO::TCNumpyReader, std::default_delete<TC::IO::TCNumpyReader>());
		std::any arr;
		auto meta = reader->Read(arr, path, TC::IO::NpyArrType::arrUSHORT);
		auto shape = std::get<1>(meta);

		auto dim = d->curMask->getDimension();

		if(shape[0] != dim[0] ) {
			return std::make_tuple(false, 0, 0);
		}
		if(shape[1] != dim[1]) {
			return std::make_tuple(false, 0, 0);
		}

		if (d->editHistory.count() + d->undoHistory.count() > 0) {
			d->drawer->SaveEditinigs();
			d->curMask->saveEditing();			
			d->editHistory.clear();
			d->undoHistory.clear();
		}
		d->curMask->data.touch();

		auto result = std::any_cast<std::vector<uint16_t>>(arr);		
		d->curMask->data.setValue(SbVec3i32(dim[0], dim[1], 1), SoSFArray::DataType::UNSIGNED_SHORT, 16, result.data(), SoSFArray::COPY);		

		double min, max;
		d->curMask->getMinMax(min, max);		

		d->curLabelMax = static_cast<int>(max);

		ModifyColorTable();

		return std::make_tuple(true,static_cast<int>(min),static_cast<int>(max));
    }
	auto SceneManager::SaveAsNumpy(const QString& path) -> bool {
		if(nullptr == d->curMask) {
			return false;
		}
		if (d->editHistory.count() + d->undoHistory.count() > 0) {
			d->drawer->SaveEditinigs();
			d->curMask->saveEditing();			
			d->editHistory.clear();
			d->undoHistory.clear();
		}
		d->curMask->data.touch();

		double min, max;
		d->curMask->getMinMax(min, max);
		if(max < 1) {
			return false;
		}				

		std::shared_ptr<TC::IO::TCNumpyWriter> writer(new TC::IO::TCNumpyWriter, std::default_delete<TC::IO::TCNumpyWriter>());

		auto dim = d->curMask->getDimension();

		SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(dim[0]-1, dim[1]-1, 0));
		SoLDMDataAccess::DataInfoBox dataInfoBox =
			d->curMask->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
		SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
		dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

		d->curMask->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
		unsigned short* src = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_ONLY);

	    auto n = dim[0] * dim[1];
						
		std::vector<uint16_t> dst(src, src + n);
		
		std::any arr = dst;
		const std::vector<long unsigned> shape{ static_cast<long unsigned>(dim[0]),static_cast<long unsigned>(dim[1])};

		if(false == writer->Write(arr,path,TC::IO::NpyArrType::arrUSHORT,shape)){
			dataBufferObj->unmap();
			return false;
		}
		dataBufferObj->unmap();
		return true;
    }

	auto SceneManager::SetPaintBrushSize(int size)->void {
		d->paintSize = size;
		d->drawer->SetPaintBrushSize(d->paintSize);
	}

	auto SceneManager::SetWipeBrushSize(int size)->void {
		d->wipeSize = size;
		d->drawer->SetWipeBrushSize(d->wipeSize);
	}
	auto SceneManager::SetLabelValue(int value) -> void {		
		d->curLabelValue = value;
		d->drawer->SetCurLabel(value);
	}

	auto SceneManager::AddLabel(int value) -> bool {
		if (nullptr == d->drawer) {
			return false;
		}
		if (d->drawer->GetTool() == DrawerToolType::None) {
			return false;
		}
		if (nullptr == d->curMask) {
			return false;
		}
		if(d->curMaskType._to_integral() != MaskTypeEnum::MultiLabel){
			return false;
		}		
		
		d->curLabelValue = value;
		d->curLabelMax = value;
		d->drawer->SetCurLabel(static_cast<uint32_t>(value));

		ModifyColorTable();
		return true;
	}

	auto SceneManager::SetSizeIndex(int idx) -> void {
		if(nullptr == d->drawer3d) {
			return;
		}
		d->drawer3d->PerformSizeFilter(idx);
	}

	auto SceneManager::InitDataGroups()->void {
		//Init merger volume
		d->mergerTarget = new SoVolumeData;
		d->mergerTarget->setName("Start");

		//Init size volume
		d->sizeTarget = new SoVolumeData;
		d->sizeTarget->setName("sizeStart");
		d->sizeTarget->ldmResourceParameters.getValue()->fixedResolution = TRUE;
		d->sizeTarget->ldmResourceParameters.getValue()->resolution = 0;

		//init sharable group item
		d->imageGroup = new SoGroup;

		auto infoGroup = new SoGroup;
		d->htMinInfo = new SoInfo;
		d->htMinInfo->setName("HTMin");
		d->htMinInfo->string.setValue("");
		d->htMaxInfo = new SoInfo;
		d->htMaxInfo->setName("HTMax");
		d->htMaxInfo->string.setValue("");
		infoGroup->addChild(d->htMinInfo);
		infoGroup->addChild(d->htMaxInfo);
		d->imageGroup->addChild(infoGroup);

		d->imageSocket = new SoSwitch;
		d->imageSocket->whichChild = -1;
		d->imageGroup->addChild(d->imageSocket);

		d->dataRange = new SoDataRange;
		d->dataRange->setName("HTSliceRange");
		d->imageGroup->addChild(d->dataRange);

		d->matlImage = new SoMaterial;
		d->matlImage->ambientColor.setValue(1, 1, 1);
		d->matlImage->diffuseColor.setValue(1, 1, 1);
		d->matlImage->transparency.setValue(0.5);
		d->imageGroup->addChild(d->matlImage);

		d->tfImage = new SoTransferFunction;
		d->tfImage->predefColorMap = SoTransferFunction::INTENSITY;
		d->tfImage->setName("colorMapHTSlice");
		d->imageGroup->addChild(d->tfImage);

		d->mergeGroup = new SoGroup;
		d->mergeSocket = new SoSwitch;
		d->mergeSocket->addChild(d->mergerTarget);
		d->mergeSocket->whichChild = -1;
		d->mergeGroup->addChild(d->mergeSocket);

		d->dataRangeMerge = new SoDataRange;
		d->dataRangeMerge->min = -0.1;
		d->dataRangeMerge->max = 1;
		d->dataRangeMerge->setName("MergeSliceRange");
		d->mergeGroup->addChild(d->dataRangeMerge);

		d->sizeGroup = new SoGroup;
		d->sizeSocket = new SoSwitch;
		d->sizeSocket->addChild(d->sizeTarget);
		d->sizeSocket->whichChild = -1;
		d->sizeGroup->addChild(d->sizeSocket);

		d->dataRangeSize = new SoDataRange;
		d->dataRangeSize->min = -0.1;
		d->dataRangeSize->max = 1;
		d->dataRangeSize->setName("SizeSliceRange");
		d->sizeGroup->addChild(d->dataRangeSize);				

		d->flGroup = new SoGroup;
		d->flSocket = new SoSwitch;
		d->flSocket->addChild(new SoSeparator);
		d->flSocket->whichChild = -1;
		d->flGroup->addChild(d->flSocket);

		d->dataRangeFL = new SoDataRange;
		d->dataRangeFL->min = 50;
		d->dataRangeFL->max = 200;
		d->dataRangeFL->setName("FLSliceRange");
		d->flGroup->addChild(d->dataRangeFL);

		d->maskGroup = new SoGroup;

		d->maskSocket = new SoSwitch;
		d->maskSocket->whichChild = -1;
		d->maskGroup->addChild(d->maskSocket);

		d->dataRangeMask = new SoDataRange;
		d->dataRangeMask->min = -0.5;
		d->dataRangeMask->max = 99;
		d->dataRangeMask->setName("MaskSliceRange");
		d->maskGroup->addChild(d->dataRangeMask);

		d->dataRangeTemp = new SoDataRange;
		d->dataRangeTemp->min = 0;
		d->dataRangeTemp->max = 1;

		d->matlMaskVolume = new SoMaterial;
		d->matlMaskVolume->ambientColor.setValue(1, 1, 1);
		d->matlMaskVolume->diffuseColor.setValue(1, 1, 1);
		d->matlMaskVolume->transparency.setValue(0.5);

		d->matlMaskSlice = new SoMaterial;
		d->matlMaskSlice->ambientColor.setValue(1, 1, 1);
		d->matlMaskSlice->diffuseColor.setValue(1, 1, 1);
		d->matlMaskSlice->transparency.setValue(0.0);

		d->matlMerge = new SoMaterial;
		d->matlMerge->ambientColor.setValue(1, 1, 1);
		d->matlMerge->diffuseColor.setValue(1, 1, 1);
		d->matlMerge->transparency.setValue(0.0f);
		d->mergeGroup->addChild(d->matlMerge);

		d->matlSize = new SoMaterial;
		d->matlSize->ambientColor.setValue(1, 1, 1);
		d->matlSize->diffuseColor.setValue(1, 1, 1);
		d->matlSize->transparency.setValue(0.1f);
		d->sizeGroup->addChild(d->matlSize);

		d->matlFL = new SoMaterial;
		d->matlFL->ambientColor.setValue(1, 1, 1);
		d->matlFL->diffuseColor.setValue(1, 1, 1);
		d->matlFL->transparency.setValue(0.5);
		d->flGroup->addChild(d->matlFL);


		d->tfMask = new SoTransferFunction;
		d->tfMask->predefColorMap = SoTransferFunction::NONE;
		d->tfMask->setName("colormapMask");
		d->tfMask->colorMap.setNum(256 * 4);
		auto max = 100;
		float* p = d->tfMask->colorMap.startEditing();
		for (auto i = 0; i < 256; ++i) {
			int idx = (float)i / 255.0 * max;
			if (idx < 1) {
				*p++ = 0.0;
				*p++ = 0.0;
				*p++ = 0.0;
				*p++ = 0.0;
			}
			else {
				float r = color_table[(idx - 1) % 20][0] / 255.0;
				float g = color_table[(idx - 1) % 20][1] / 255.0;
				float b = color_table[(idx - 1) % 20][2] / 255.0;
				*p++ = r;
				*p++ = g;
				*p++ = b;
				*p++ = 0.7;
			}
		}
		d->tfMask->colorMap.finishEditing();

		d->maskGroup->addChild(d->tfMask);

		d->tfMerge = new SoTransferFunction;
		d->tfMerge->predefColorMap = SoTransferFunction::NONE;
		d->tfMerge->setName("colormapMerge");
		d->tfMerge->colorMap.setNum(256 * 4);
		float* pp = d->tfMerge->colorMap.startEditing();
		for (auto i = 0; i < 256; ++i) {
			if (i > 128) {
				*pp++ = 0.0;
				*pp++ = 0.0;
				*pp++ = 1.0;
				*pp++ = 1.0;
			}
			else {
				*pp++ = 0.0;
				*pp++ = 0.0;
				*pp++ = 0.0;
				*pp++ = 0.0;
			}
		}
		d->tfMerge->colorMap.finishEditing();
		d->mergeGroup->addChild(d->tfMerge);

		d->tfSize = new SoTransferFunction;
		d->tfSize->predefColorMap = SoTransferFunction::NONE;
		d->tfSize->setName("colormapSize");
		d->tfSize->colorMap.setNum(256 * 4);
		float* ppp = d->tfSize->colorMap.startEditing();
		for (auto i = 0; i < 256; ++i) {
			if (i > 128) {
				*ppp++ = 7.0;
				*ppp++ = 5.0;
				*ppp++ = 7.0;
				*ppp++ = 1.0;
			}
			else {
				*ppp++ = 0.0;
				*ppp++ = 0.0;
				*ppp++ = 0.0;
				*ppp++ = 0.0;
			}
		}
		d->tfSize->colorMap.finishEditing();
		d->sizeGroup->addChild(d->tfSize);

		d->orthoSliceSize = new SoOrthoSlice;
		d->orthoSliceSize->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
		d->sizeGroup->addChild(d->orthoSliceSize);

		d->tfFL = new SoTransferFunction;
		d->tfFL->predefColorMap = SoTransferFunction::NONE;
		d->tfFL->setName("colormapFL");
		d->tfFL->colorMap.setNum(256 * 4);
		d->flGroup->addChild(d->tfFL);

		d->tfSlice = new SoTransferFunction;
		d->tfSlice->predefColorMap = SoTransferFunction::NONE;
		d->tfSlice->colorMap.setNum(256 * 4);
		float* f = d->tfSlice->colorMap.startEditing();
		for (auto i = 0; i < 256; ++i) {
			if (i > 128) {
				*f++ = 0.0;
				*f++ = 1.0;
				*f++ = 0.0;
				*f++ = 1.0;
			}
			else {
				*f++ = 0.0;
				*f++ = 0.0;
				*f++ = 0.0;
				*f++ = 0.0;
			}
		}
		d->tfSlice->colorMap.finishEditing();
		d->imageSliceSep = new SoSeparator;
		d->imageSliceSocket = new SoSwitch;
		d->imageSliceSocket->whichChild = -1;
		d->imageSliceSocket->setName("TempSlice0");
		d->imageSliceSep->addChild(d->imageSliceSocket);
		d->imageSliceSep->addChild(d->matlMaskSlice);
		d->imageSliceSep->addChild(d->dataRangeTemp);		
		d->imageSliceSep->addChild(d->tfSlice);
		d->orthoSliceTemp = new SoOrthoSlice;
		d->orthoSliceTemp->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
		d->imageSliceSep->addChild(d->orthoSliceTemp);
	}
	auto SceneManager::ModifyColorTable() -> void {		
		d->tfMask->colorMap.setNum((d->curLabelMax+1) * 4);		
		float* p = d->tfMask->colorMap.startEditing();
		for (auto i = 0; i < d->curLabelMax +1; ++i) {			
			if (i == 0) {
				*p++ = 0.0;
				*p++ = 0.0;
				*p++ = 0.0;
				*p++ = 0.0;
			}
			else {
				float r = color_table[(i - 1) % 20][0] / 255.0;
				float g = color_table[(i - 1) % 20][1] / 255.0;
				float b = color_table[(i - 1) % 20][2] / 255.0;
				*p++ = r;
				*p++ = g;
				*p++ = b;
				*p++ = 0.8;
			}
		}
		d->tfMask->colorMap.finishEditing();

		d->dataRangeMask->min.setValue(0);
		d->dataRangeMask->max.setValue(d->curLabelMax);
    }
	void SceneManager::OnStartSize(int size) {
		d->sizeSocket->whichChild = 0;
		emit sigStartSizeFilter(size);
	}
	auto SceneManager::Init2DScenes()->void {
		//Scenegraphs for 2D renderwindows
		d->root2d = new SoSeparator;

		d->orthoSep = new SoSeparator;
		d->orthoSepMask = new SoSeparator;
		d->orthoSepMask->setName("MaskSeparator");
		d->orthoSepMerge = new SoSeparator;
		d->orthoSepMerge->setName("MergeSeparator");
		d->orthoSepFL = new SoSeparator;
		d->orthoSepFL->setName("FLSeparator");

		d->tempViz = new SoSwitch;
		d->tempViz->setName("TempViz0");
		d->tempViz->whichChild = -1;
		d->tempViz->addChild(d->imageSliceSep);

		d->root2d->addChild(d->sizeGroup);
		d->root2d->addChild(d->tempViz);
		d->root2d->addChild(d->orthoSepMerge);
		d->root2d->addChild(d->orthoSepFL);
		d->root2d->addChild(d->orthoSepMask);
		d->root2d->addChild(d->orthoSep);

		d->orthoSep->addChild(d->imageGroup);

		auto trans = new SoTranslation;
		trans->translation.setValue(0, 0, 0.1f);
		auto transfl = new SoTranslation;
		transfl->translation.setValue(0, 0, 0.0f);
		auto transmerge = new SoTranslation;
		transmerge->translation.setValue(0, 0, 0.0f);
		d->orthoSepMask->addChild(trans);
		d->orthoSepMerge->addChild(transmerge);
		d->orthoSepFL->addChild(transfl);

		d->orthoSepMask->addChild(d->maskGroup);
		d->orthoSepMerge->addChild(d->mergeGroup);
		d->orthoSepFL->addChild(d->flGroup);

		d->orthoSlice = new SoOrthoSlice;
		d->orthoSlice->setName("ImageSlice");
		d->orthoSlice->interpolation = SoSlice::NEAREST;
		d->orthoSlice->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
		d->orthoSep->addChild(d->orthoSlice);

		d->orthoSliceMask = new SoOrthoSlice;
		d->orthoSliceMask->setName("MaskSlice");
		d->orthoSliceMask->interpolation = SoSlice::NEAREST;
		d->orthoSliceMask->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
		d->orthoSepMask->addChild(d->orthoSliceMask);

		d->orthoSliceMerge = new SoOrthoSlice;
		d->orthoSliceMerge->setName("MergeSlice");
		d->orthoSliceMerge->interpolation = SoSlice::NEAREST;
		d->orthoSliceMerge->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
		d->orthoSepMerge->addChild(d->orthoSliceMerge);

		d->orthoSliceFL = new SoOrthoSlice;
		d->orthoSliceFL->setName("FLSlice");
		d->orthoSliceFL->interpolation = SoSlice::NEAREST;
		d->orthoSliceFL->alphaUse = SoSlice::ALPHA_AS_IS;
		d->orthoSepFL->addChild(d->orthoSliceFL);


		d->orthoSlice->axis = MedicalHelper::AXIAL;
		d->orthoSliceMask->axis = MedicalHelper::AXIAL;
		d->orthoSliceTemp->axis = MedicalHelper::AXIAL;
		d->orthoSliceMerge->axis = MedicalHelper::AXIAL;
		d->orthoSliceFL->axis = MedicalHelper::AXIAL;

		if (nullptr == d->scaleBarSwitch) {
			d->scaleBarSwitch = new SoSwitch;
		}
		else {
			d->scaleBarSwitch->removeAllChildren();
		}
		d->scaleBarSwitch->whichChild = -3;
		auto scaleBarSep = new SoSeparator;
		d->scaleBarSwitch->addChild(scaleBarSep);

		
		auto scaleColor = new SoMaterial;
		scaleColor->diffuseColor.setValue(1, 0.25f, 0.25f);
		scaleBarSep->addChild(scaleColor);
		auto scaleStyle = new SoDrawStyle;
		scaleStyle->lineWidth = 2;
		scaleBarSep->addChild(scaleStyle);

		auto scaleBar = new OivScaleBar();
		scaleBar->setName("ScaleBar");
		scaleBar->numTickIntervals = 0;
		scaleBar->position = SbVec2f(0.95f, -0.99f);
		scaleBar->alignment = OivScaleBar::RIGHT;
		//scaleBar->trackedCamera = d->qOiv2DRenderWindow[i]->getCamera();

		scaleBarSep->addChild(scaleBar);

		if (d->root2d->findChild(d->scaleBarSwitch) < 0) {
			d->root2d->addChild(d->scaleBarSwitch);
		}
	}
	auto SceneManager::Init2DDrawers()->void {
		d->drawerSep = new SoSeparator;
		d->drawerSep->setName("DrawerSeparator");
		d->drawerSep->fastEditing = SoSeparator::CLEAR_ZBUFFER;
		d->drawerSep->boundingBoxIgnoring = TRUE;

		d->drawer = new Oiv2DDrawer;
		d->drawer->SetPaintBrushSize(d->paintSize);
		d->drawer->SetWipeBrushSize(d->wipeSize);
		d->drawer->SetMergerVolume(d->mergerTarget);

		d->drawer->SetAxis(MedicalHelper::AXIAL);

		d->drawerSep->addChild(d->drawer->GetSceneGraph());

		d->root2d->addChild(d->drawerSep);

	    connect(d->drawer, qOverload<int>(&Oiv2DDrawer::pickValue), this, &SceneManager::OnLabelPicking);
		connect(d->drawer, SIGNAL(mergeVol(int)), this, SLOT(OnFinishMerge(int)));
		connect(d->drawer, SIGNAL(sigHistory(int)), this, SLOT(OnHistory(int)));
		connect(d->drawer, SIGNAL(pickRemove()), this, SLOT(OnLabelRemove()));

		d->drawerSep3d = new SoSeparator;
		d->drawerSep3d->setName("drawerSep3d");
		d->drawerSep3d->fastEditing = SoSeparator::CLEAR_ZBUFFER;
		d->drawer3d = new Oiv3DDrawer(true);
		connect(d->drawer3d, SIGNAL(sigStartSize(int)), this, SLOT(OnStartSize(int)));

		d->drawerSep3d->addChild(d->drawer3d->GetSceneGraph());
		d->root2d->addChild(d->drawerSep3d);

		d->drawer3d->SetSizeVolume(d->sizeTarget);
	}
	void SceneManager::OnHistory(int id) {
		d->editHistory.push_back(id);
	}

	auto SceneManager::Init() -> void {
		InitDataGroups();
		Init2DScenes();
		Init2DDrawers();
	}

	auto SceneManager::SetMask(TCMask::Pointer& mask, QString name) -> bool {
		if (nullptr != d->curMask) {
			ResetMask();
		}		
		auto converter = new TCDataConverter;
		d->curMaskName = name;
		d->curMask = converter->MaskToSoVolumeData(mask, name);
		//d->curMask->ref();
		double min, max;
		d->curMask->getMinMax(min, max);		
		d->curMaskType = mask->GetType();

		d->matlMaskVolume->transparency = 0.4f;
	    SetMaskToScene();
		return true;
	}
	auto SceneManager::RearrangeLabel() -> void {
		if (nullptr == d->curMask) {
			return;
		}
		if (d->editHistory.count() + d->undoHistory.count() > 0) {
			d->drawer->SaveEditinigs();
		    d->curMask->saveEditing();			
			d->editHistory.clear();
			d->undoHistory.clear();
		}
		d->curMask->data.touch();

		//MedicalHelper::dicomAdjustVolume(d->curMask);
		SoRef<SoMemoryDataAdapter> oriAdapter = MedicalHelper::getImageDataAdapter(d->curMask);
		oriAdapter->interpretation = SoMemoryDataAdapter::Interpretation::LABEL;

		//auto labeling = new SoLabelingProcessing;
		//labeling->inObjectImage = oriAdapter;
		SoRef<SoReorderLabelsProcessing> reorder = new SoReorderLabelsProcessing;
		reorder->inLabelImage = oriAdapter.ptr();

		SoRef<SoConvertImageProcessing> converter = new SoConvertImageProcessing;
		converter->inImage.connectFrom(&reorder->outLabelImage);
		converter->dataType = SoConvertImageProcessing::DataType::UNSIGNED_SHORT;

		SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
		reader->imageData.connectFrom(&converter->outImage);

		d->curMask->setReader(*reader, TRUE);

		double min, max;
		d->curMask->getMinMax(min, max);

		d->curLabelMax = static_cast<int>(max);

		ModifyColorTable();
	}
	auto SceneManager::GetMask()->TCMask::Pointer {
		if (nullptr == d->curMask) {
			return nullptr;
		}
		if (d->editHistory.count() + d->undoHistory.count() > 0) {
			d->drawer->SaveEditinigs();
			d->curMask->saveEditing();			
			d->editHistory.clear();
			d->undoHistory.clear();
		}
		d->curMask->data.touch();

		double min, max;
		d->curMask->getMinMax(min, max);
		if(max < 1) {
			return nullptr;
		}		
		auto converter = new TCDataConverter;
		QList<SoVolumeData*> maskList;
		maskList.push_back(d->curMask);
		
		QStringList names;
		names.push_back(d->curMaskName);
		auto mask = converter->SoVolumeDataToMask(maskList, d->curMaskType, names);
		mask->SetName(d->curMaskName);
		return mask;
	}
	auto SceneManager::TryRefresh() -> void {
		//in outer space mask is save edited before refresh call 
		/*if (nullptr != d->curMask) {
			if (d->editHistory.count() + d->undoHistory.count() > 0) {
				std::cout << "edit history: " << d->editHistory.count() << std::endl;
				std::cout << "undo history: " << d->undoHistory.count() << std::endl;
				std::cout << "SE before refresh" << std::endl;
				d->curMask->saveEditing();
				std::cout << "SE before refresh done" << std::endl;
			}
			d->curMask->data.touch();
		}*/
		d->editHistory.clear();
		d->undoHistory.clear();
	}
	auto SceneManager::TryUndo() -> void {
		if (nullptr == d->curMask) {
			return;
		}
		if (d->editHistory.count() > 0) {
			auto id = d->editHistory.last();
			d->curMask->undoEditing(id);
			d->editHistory.pop_back();
			d->undoHistory.push_back(id);
		}
	}
	auto SceneManager::TryRedo() -> void {
		if (nullptr == d->curMask) {
			return;
		}
		if (d->undoHistory.count() > 0) {
			auto id = d->undoHistory.last();
			d->curMask->redoEditing(id);
			d->editHistory.push_back(id);
			d->undoHistory.pop_back();
		}
	}
	auto SceneManager::SetFLOpacity(double opacity) -> bool {
		d->matlFL->transparency.setValue(1.0 - opacity);
		return true;
	}
	auto SceneManager::SetFLColor(QColor color) -> bool {
		float* f = d->tfFL->colorMap.startEditing();
		for (auto i = 0; i < 256; ++i) {
			if (i > 128) {
				*f++ = color.redF();
				*f++ = color.greenF();
				*f++ = color.blueF();
				*f++ = 1.0;
			}
			else {
				*f++ = 0.0;
				*f++ = 0.0;
				*f++ = 0.0;
				*f++ = 0.0;
			}
		}
		d->tfFL->colorMap.finishEditing();
		return true;
	}

	auto SceneManager::RemoveMask(QString name) -> bool {		
		if (d->curMaskName == name) {
			ResetMask();
			d->curMaskName = QString();
		}
		return true;
	}
	auto SceneManager::SetFLImage(TCImage::Pointer& flImage, int ch, float offset) -> bool {
		if (nullptr != d->curFL) {
			d->flSocket->replaceChild(0, new SoSeparator);
			while (d->curFL->getRefCount() > 0) {
				d->curFL->unref();
			}
			d->curFL = nullptr;
		}
		d->flSocket->whichChild = -1;
		if (ch > -1) {
			auto converter = new TCDataConverter;
			auto original = converter->ImageToSoVolumeData(flImage);
			original->ref();

			d->flSocket->replaceChild(0, d->curFL);
			d->flSocket->whichChild = 0;

			while (original->getRefCount() > 0) {
				original->unref();
			}
			original = nullptr;

			delete converter;
		}
		return true;
	}
	auto SceneManager::SetImage(TCImage::Pointer& image, QString name) -> bool {
		auto time = image->GetTimeStep();
		if (d->curImageName == name && d->curTime == time) {
			return true;
		}
		if (nullptr != d->curImage) {
			ResetAll();
		}
		d->curTime = time;
		auto converter = new TCDataConverter;
		d->curImage = converter->ImageToSoVolumeData(image);
		d->curImage->ref();		
				
		SetImageToScene();

		d->curImageName = name;

		delete converter;

		auto imin = std::get<0>(image->GetMinMax());
		auto imax = std::get<1>(image->GetMinMax());

		d->renWin->setHTRange(imin, imax);

		d->dataRange->min = imin;
		d->dataRange->max = imax;

		return true;
	}
	auto SceneManager::ActivateTool(int idx) -> bool {
		auto type = (DrawerToolType)idx;
		d->drawer->SetTool(type);
		d->drawer3d->SetTool(type);
		d->renWin->SetInteractionMode(true);
		return true;
	}
	auto SceneManager::DeactivateTool(bool isFunc) -> bool {
		auto curtool = d->drawer->GetTool();
		if (curtool == DrawerToolType::MergeLabel) {
			if (isFunc) {
				PerformMergeLabel();
			}
			FlushMergeLabel();
		}
		if (d->sizeSocket->whichChild.getValue() != -1) {
			PerformRemoveSize();
		}
		auto type = DrawerToolType::None;
		d->drawer->SetTool(type);
		d->drawer3d->SetTool(type);
		d->renWin->SetInteractionMode(false);		
		return true;
	}
	auto SceneManager::PerformDilate(int label) -> bool {
		if (d->curMaskName.isEmpty()) {
			return false;
		}
		if (label < 1) {
			std::cout << "label value is lower than 1" << std::endl;
			return false;
		}
		if (nullptr == d->curMask) {
			return false;
		}

		//MedicalHelper::dicomAdjustVolume(d->curMask);
		auto maskAdapter = MedicalHelper::getImageDataAdapter(d->curMask);
		maskAdapter->interpretation = SoMemoryDataAdapter::VALUE;

		auto thresh = new SoThresholdingProcessing;
		thresh->inImage = maskAdapter;
		thresh->thresholdLevel.setValue(label, label + 0.5);

		//auto dilate = new SoDilationBallProcessing3d;
		auto dilate = new SoDilationCubeProcessing;
		dilate->inImage.connectFrom(&thresh->outBinaryImage);
		dilate->elementSize.setValue(3);

		auto binconv = new SoConvertImageProcessing;
		binconv->inImage.connectFrom(&dilate->outImage);
		binconv->dataType = SoConvertImageProcessing::BINARY;

		auto reset = new SoResetImageProcessing;
		reset->inImage = maskAdapter;
		reset->intensityValue = label;

		auto emptyReset = new SoResetImageProcessing;
		emptyReset->inImage = maskAdapter;
		emptyReset->intensityValue = 0;

		auto remover = new SoCombineByMaskProcessing;
		remover->inImage1.connectFrom(&emptyReset->outImage);
		remover->inImage2 = maskAdapter;
		remover->inBinaryImage.connectFrom(&thresh->outBinaryImage);

		auto masking = new SoCombineByMaskProcessing;
		masking->inImage1.connectFrom(&reset->outImage);
		masking->inImage2.connectFrom(&remover->outImage);
		masking->inBinaryImage.connectFrom(&binconv->outImage);

		auto reader = new SoVRImageDataReader;
		reader->imageData.connectFrom(&masking->outImage);

		d->curMask->setReader(*reader, TRUE);
		d->curMask->data.touch();
		return true;
	}
	auto SceneManager::PerformErode(int label) -> bool {
		if (d->curMaskName.isEmpty()) {
			return false;
		}
		if (label < 1) {
			std::cout << "label value is lower than 1" << std::endl;
			return false;
		}
		if (nullptr == d->curMask) {
			return false;
		}

		//MedicalHelper::dicomAdjustVolume(d->curMask);
		auto maskAdapter = MedicalHelper::getImageDataAdapter(d->curMask);
		maskAdapter->interpretation = SoMemoryDataAdapter::VALUE;

		auto thresh = new SoThresholdingProcessing;
		thresh->inImage = maskAdapter;
		thresh->thresholdLevel.setValue(label, label + 0.5);

		//auto erode = new SoErosionBallProcessing3d;
		auto erode = new SoErosionCubeProcessing;		
		erode->inImage.connectFrom(&thresh->outBinaryImage);
		erode->elementSize.setValue(3);

		auto binconv = new SoConvertImageProcessing;
		binconv->inImage.connectFrom(&erode->outImage);
		binconv->dataType = SoConvertImageProcessing::BINARY;

		auto reset = new SoResetImageProcessing;
		reset->inImage = maskAdapter;
		reset->intensityValue = label;

		auto emptyReset = new SoResetImageProcessing;
		emptyReset->inImage = maskAdapter;
		emptyReset->intensityValue = 0;

		auto remover = new SoCombineByMaskProcessing;
		remover->inImage1.connectFrom(&emptyReset->outImage);
		remover->inImage2 = maskAdapter;
		remover->inBinaryImage.connectFrom(&thresh->outBinaryImage);

		auto masking = new SoCombineByMaskProcessing;
		masking->inImage1.connectFrom(&reset->outImage);
		masking->inImage2.connectFrom(&remover->outImage);
		masking->inBinaryImage.connectFrom(&binconv->outImage);

		auto reader = new SoVRImageDataReader;
		reader->imageData.connectFrom(&masking->outImage);

		d->curMask->setReader(*reader, TRUE);
		d->curMask->data.touch();

		return true;
	}
	auto SceneManager::PerformWaterShed(TCMask::Pointer target) -> bool {
		if (d->curMaskName.isEmpty()) {
			return false;
		}
		if (d->curMaskName != "cellInst") {
			return false;
		}
		if (nullptr == target) {
			return false;
		}
		if (nullptr == d->curMask) {
			return false;
		}
		auto converter = new TCDataConverter;
		auto target_volume = converter->MaskToSoVolumeData(target, "membrane");

		//MedicalHelper::dicomAdjustVolume(target_volume);
		auto targetAdapter = MedicalHelper::getImageDataAdapter(target_volume);
		targetAdapter->interpretation = SoMemoryDataAdapter::VALUE;

		//MedicalHelper::dicomAdjustVolume(d->curMask);
		auto maskAdapter = MedicalHelper::getImageDataAdapter(d->curMask);
		maskAdapter->interpretation = SoMemoryDataAdapter::LABEL;

		auto water = new SoMarkerBasedWatershedProcessing;
		water->outputMode = SoMarkerBasedWatershedProcessing::OutputMode::SEPARATED_BASINS;
		water->precisionMode = SoMarkerBasedWatershedProcessing::PrecisionMode::FAST;
		water->inGrayImage = targetAdapter;
		water->inMarkerImage = maskAdapter;

		auto binconv = new SoConvertImageProcessing;
		binconv->inImage = targetAdapter;
		binconv->dataType = SoConvertImageProcessing::BINARY;

		auto masking = new SoMaskImageProcessing;
		masking->inImage.connectFrom(&water->outObjectImage);
		masking->inBinaryImage.connectFrom(&binconv->outImage);

		auto reader = new SoVRImageDataReader;
		reader->imageData.connectFrom(&masking->outImage);

		d->curMask->setReader(*reader, TRUE);

		delete converter;

		return true;
	}

	auto SceneManager::FlushMergeLabel() -> void {		
		d->mergerTarget->setName("Start");		
		d->mergeSocket->whichChild = -1;
	}

	auto SceneManager::PerformRemoveSize() -> void {
		if (nullptr == d->sizeTarget || nullptr == d->curMask) {
			return;
		}
		//MedicalHelper::dicomAdjustVolume(d->curMask);
		auto maskAdapter = MedicalHelper::getImageDataAdapter(d->curMask);
		maskAdapter->interpretation = SoMemoryDataAdapter::VALUE;
		//MedicalHelper::dicomAdjustVolume(d->sizeTarget);
		auto sizeAdapter = MedicalHelper::getImageDataAdapter(d->sizeTarget);
		sizeAdapter->interpretation = SoMemoryDataAdapter::BINARY;

		auto invert = new SoLogicalNotProcessing;
		invert->inImage = sizeAdapter;

		auto masking = new SoMaskImageProcessing;
		masking->inImage = maskAdapter;
		masking->inBinaryImage.connectFrom(&invert->outImage);

		auto reader = new SoVRImageDataReader;
		reader->imageData.connectFrom(&masking->outImage);

		d->curMask->setReader(*reader, TRUE);		

		d->sizeSocket->whichChild = -1;

	    RearrangeLabel();
	}

	auto SceneManager::PerformMergeLabel() -> void {
		if (nullptr != d->mergerTarget && nullptr != d->curMask) {
			if (d->mergerTarget->getName() != "ChunkMask") {
				return;
			}
			if (d->editHistory.count() + d->undoHistory.count() > 0) {
				d->drawer->SaveEditinigs();
				d->curMask->saveEditing();				
				d->editHistory.clear();
				d->undoHistory.clear();
			}			

			double orimin, orimax;
			d->curMask->getMinMax(orimin, orimax);

			//MedicalHelper::dicomAdjustVolume(d->curMask);
			auto maskAdapter = MedicalHelper::getImageDataAdapter(d->curMask);
			maskAdapter->interpretation = SoMemoryDataAdapter::VALUE;
			//MedicalHelper::dicomAdjustVolume(d->mergerTarget);
			auto mergeAdapter = MedicalHelper::getImageDataAdapter(d->mergerTarget);
			mergeAdapter->interpretation = SoMemoryDataAdapter::BINARY;						

			auto reset = new SoResetImageProcessing;
			reset->inImage = maskAdapter;
			reset->intensityValue = orimax + 1;

			auto masking = new SoCombineByMaskProcessing;
			masking->inImage1.connectFrom(&reset->outImage);
			masking->inImage2 = maskAdapter;
			masking->inBinaryImage = mergeAdapter;

			auto conv = new SoConvertImageProcessing;
			conv->inImage.connectFrom(&masking->outImage);
			conv->dataType = SoConvertImageProcessing::UNSIGNED_SHORT;

			auto reader = new SoVRImageDataReader;
			reader->imageData.connectFrom(&conv->outImage);

			d->curMask->setReader(*reader, TRUE);
			//d->curMask->touch();

			//reset volume container name as initial state
			d->mergerTarget->setName("Start");
			d->mergeSocket->whichChild = -1;

			RearrangeLabel();
		}
	}

	auto SceneManager::Reset() -> void {
		d->drawer->SetCurLabel(1);
		DeactivateTool();
		ResetAll();
	}

	auto SceneManager::ResetAll() -> void {
		if (d->imageSocket->getNumChildren() > 0) {
			d->imageSocket->removeChild(0);
		}
		d->imageSocket->whichChild = -1;
		if (nullptr != d->curImage) {
			d->curImage->unref();
		}
		d->curImageName = QString();
		d->maskSocket->whichChild = -1;
		d->flSocket->whichChild = -1;

		d->editHistory.clear();
		d->undoHistory.clear();
		//ResetMask();
	}
	auto SceneManager::ResetMask(bool soft) -> void {
		if (d->maskSocket->getNumChildren() > 0) {
			d->maskSocket->removeChild(0);
			d->maskSocket->whichChild = -1;
			if (!soft) {
				while (d->curMask->getRefCount() > 0) {
					d->curMask->unref();
				}
				d->curMask = nullptr;
			}
		}
		d->drawer->SetTargetVolume(nullptr);
		d->drawer3d->SetTargetVolume(nullptr);
		//d->labelTextSwitch->replaceChild(0, new SoSeparator);
		//d->labelTextSwitch->whichChild = -1;
	}

	void SceneManager::OnLabelRemove() {
		RearrangeLabel();
	}

	void SceneManager::OnFinishMerge(int val) {
		Q_UNUSED(val)
	    double min, max;		
		d->mergerTarget->getMinMax(min, max);		
		if (max > 0) {
			d->mergeSocket->whichChild = 0;
		}
		else {
			d->mergeSocket->whichChild = -1;
		}
	}
	auto SceneManager::TurnOnMergeLasso() -> void {
		d->drawer->ActivateMergeSupport();		
    }

	void SceneManager::OnLabelPicking(int val) {
		emit sigLabel(val);
	}
	auto SceneManager::SetImageToScene() -> void {
		d->curImage->setName("volData");
		d->curImage->dataSetId = 1;
		d->imageSocket->addChild(d->curImage);
		d->imageSocket->whichChild = 0;
		
		MedicalHelper::dicomAdjustDataRange(d->dataRange, d->curImage);
		MedicalHelper::dicomCheckMonochrome1(d->tfImage, d->curImage);

		int intmin = d->dataRange->min.getValue();
		int intmax = d->dataRange->max.getValue();

		auto stmin = QString(d->htMinInfo->string.getValue().toStdString().c_str());
		auto stmax = QString(d->htMaxInfo->string.getValue().toStdString().c_str());
		if (false == stmin.isEmpty()) {
			auto prevMin = stmin.toInt();
			if (prevMin < intmax) {
				d->dataRange->min.setValue(prevMin);
			}
			else {
				d->htMinInfo->string.setValue(std::to_string(intmin));
			}
		}
		else {
			d->htMinInfo->string.setValue(std::to_string(intmin));
		}
		if (false == stmax.isEmpty()) {
			auto prevMax = stmax.toInt();
			if (prevMax > intmin) {
				d->dataRange->max.setValue(prevMax);
			}
			else {
				d->htMaxInfo->string.setValue(std::to_string(intmax));
			}
		}
		else {
			d->htMaxInfo->string.setValue(std::to_string(intmax));
		}

		d->orthoSlice->sliceNumber = d->curImage->data.getSize()[MedicalHelper::AXIAL] / 2;
		d->orthoSliceMask->sliceNumber = d->curImage->data.getSize()[MedicalHelper::AXIAL] / 2;
		d->orthoSliceMerge->sliceNumber = d->curImage->data.getSize()[MedicalHelper::AXIAL] / 2;
		d->orthoSliceFL->sliceNumber = d->curImage->data.getSize()[MedicalHelper::AXIAL] / 2;
		
		d->renWin->reset2DView();
	}
	auto SceneManager::SetMaskToScene() -> void {
		d->curMask->setName("MaskVolume");
		d->curMask->ldmResourceParameters.getValue()->fixedResolution = TRUE;
		d->curMask->ldmResourceParameters.getValue()->resolution = 0;
		d->curMask->usePalettedTexture = TRUE;
		d->drawer->SetTargetVolume(d->curMask);
		d->drawer3d->SetTargetVolume(d->curMask);
		d->maskSocket->addChild(d->curMask);
		d->maskSocket->whichChild = 0;

		double min, max;
		d->curMask->getMinMax(min, max);

		d->curLabelMax = static_cast<int>(max);
		if(d->curLabelMax < 1) {
			d->curLabelMax = 1;
		}

		d->drawer->SetCurLabel(d->curLabelMax);

		ModifyColorTable();
	}
}
