#pragma once

#include <memory>

#include <QColor>

#include <ISceneManagerWidget.h>

#include "ME2SceneManagerExport.h"

class SoVolumeData;
class SoPerspectiveCamera;
class SoSeparator;

namespace TomoAnalysis::MaskEditor2d::Plugins {    
    class ME2SceneManager_API SceneManager : public Interactor::ISceneManagerWidget {
    Q_OBJECT
    public:
        typedef SceneManager Self;
        typedef std::shared_ptr<Self> Pointer;

        SceneManager();
        ~SceneManager();

        auto SetSceneContainer(QWidget* container)->void;

        auto SetImage(TCImage::Pointer& image,QString name) -> bool override;
        auto SetMask(TCMask::Pointer& mask, QString name) -> bool override;        
        auto RemoveMask(QString name) -> bool override;        
        auto GetMask()->TCMask::Pointer override;
        auto RearrangeLabel() -> void override;

        auto ActivateTool(int idx) -> bool override;
        auto DeactivateTool(bool isFunc = false) -> bool override;
        
        auto SetSliceIndex(int viewIndex, int sliceIndex)->void;

        auto SetPaintBrushSize(int size)->void;
        auto SetWipeBrushSize(int size)->void;
        auto SetLabelValue(int value)->void;
        auto AddLabel(int value)->bool;
        auto SetSizeIndex(int idx)->void;

        //FL Controls
        auto SetFLImage(TCImage::Pointer& flImage,int ch,float offset)->bool;
        auto SetFLOpacity(double opacity)->bool;
        auto SetFLColor(QColor color)->bool;

        //Instance processing (no mask type conversion)
        auto PerformDilate(int label = -1) ->bool;
        auto PerformErode(int label = -1)->bool;
        auto PerformWaterShed(TCMask::Pointer target)->bool;

        //Instance History processing
        auto TryUndo()->void;
        auto TryRedo()->void;
        auto TryRefresh()->void;
               
        auto Reset() -> void override;

        //numpy function
        auto CopyFromNumPy(const QString& path)->std::tuple<bool,int,int>;
        auto SaveAsNumpy(const QString& path)->bool;

        auto TurnOnMergeLasso()->void;

    signals:
        void sigLabel(int);        
        void sigStartSizeFilter(int);        

    protected slots:
        void OnLabelPicking(int);
        void OnFinishMerge(int);                        
        void OnHistory(int);
        void OnLabelRemove();
        void OnStartSize(int);                

    private:
        auto Init()->void;
        auto InitDataGroups()->void;        
        auto Init2DScenes()->void;
        auto Init2DDrawers()->void;

        auto ResetAll()->void;
        auto ResetMask(bool soft = false)->void;

        auto SetImageToScene()->void;
        auto SetMaskToScene()->void;

        auto PerformMergeLabel()->void;
        auto FlushMergeLabel()->void;
        auto PerformRemoveSize()->void;

        auto ModifyColorTable()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}