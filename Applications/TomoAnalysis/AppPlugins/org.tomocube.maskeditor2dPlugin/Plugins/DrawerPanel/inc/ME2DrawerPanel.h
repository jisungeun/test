#pragma once

#include <memory>
#include <QWidget>

#include <IDrawingToolPanel.h>

#include "ME2DrawerPanelExport.h"

namespace TomoAnalysis::MaskEditor2d::Plugins {
    class ME2DrawerPanel_API DrawerPanel : public QWidget, public Interactor::IDrawingToolPanel {
        Q_OBJECT
    public:
        typedef DrawerPanel Self;
        typedef  std::shared_ptr<DrawerPanel> Pointer;

        DrawerPanel(QWidget* parent = nullptr);
        ~DrawerPanel();

        auto Update() -> bool override;
        auto Reset() -> void override;
    signals:
        void sigAdd(bool);
        void sigSub(bool);
        void sigPaint(bool);
        void sigWipe(bool);
        void sigFill(bool);
        void sigErase(bool);

        void sigBrushSizeChanged(int);

    protected slots:
        void OnSegButtonClicked(int);        
        void OnBrushSpinChanged(int);
        void OnBrushSliderChanged(int);

    private:
        auto InitIcons()->void;
        auto Init()->void;
        auto InitToolTips()->void;
        auto ResetIcons()->void;
        auto SetIconOn()->void;

        auto SetControlWidget(int id)->void;

        struct Impl;
        std::unique_ptr<Impl> d;

    };
}