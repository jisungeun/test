#include <iostream>
#include <QIcon>
#include <QButtonGroup>

#include <ME2Tool.h>

#include "ui_ME2DrawerPanel.h"
#include "ME2DrawerPanel.h"


enum SegButtonId {
    None = 0,
    Add,
    Subtract,
    Paint,
    Wipe,
    Fill,
    Erase
};

namespace TomoAnalysis::MaskEditor2d::Plugins {
    struct DrawerPanel::Impl {
        Ui::DrawingToolForm* ui{ nullptr };

        QIcon AddIcon;
        QIcon SubIcon;
        QIcon PaintIcon;
        QIcon WipeIcon;
        QIcon FillIcon;
        QIcon EraseIcon;
        QButtonGroup* segButtonGroup{ nullptr };

        int cur_id{ Entity::ToolIdx::None };
        bool isSystem{ false };
    };
    DrawerPanel::DrawerPanel(QWidget* parent): d{ new Impl }, QWidget(parent) {
        d->ui = new Ui::DrawingToolForm();
        d->ui->setupUi(this);        
        this->InitIcons();
        this->Init();
        this->InitToolTips();
    }
    DrawerPanel::~DrawerPanel() {
        
    }
    auto DrawerPanel::Reset() -> void {
        auto ds = GetDS();
        ds->curToolIdx = -1;
        ds->isSystem = false;
        Update();
    }

    auto DrawerPanel::Update() -> bool {
        auto ds = GetDS();
        d->cur_id = ds->curToolIdx;
        d->isSystem = ds->isSystem;
        ResetIcons();
        SetIconOn();        
        return true;
    }
    void DrawerPanel::OnSegButtonClicked(int id) {
        if(d->isSystem) {
            return;
        }
        switch(id) {
        case SegButtonId::Add:
            emit sigAdd(d->cur_id == Entity::ToolIdx::ADD);
            break;
        case SegButtonId::Subtract:
            emit sigSub(d->cur_id == Entity::ToolIdx::SUBTRACT);
            break;
        case SegButtonId::Paint:
            emit sigPaint(d->cur_id == Entity::ToolIdx::PAINT);
            break;
        case SegButtonId::Wipe:
            emit sigWipe(d->cur_id == Entity::ToolIdx::WIPE);
            break;
        case SegButtonId::Fill:
            emit sigFill(d->cur_id == Entity::ToolIdx::Fill);
            break;
        case SegButtonId::Erase:
            emit sigErase(d->cur_id == Entity::ToolIdx::ERASE);
            break;
        default:
            break;
        }        
    }
    void DrawerPanel::OnBrushSpinChanged(int value) {                
        d->ui->BrushSizeSlider->blockSignals(true);
        d->ui->BrushSizeSlider->setValue(value);
        d->ui->BrushSizeSlider->blockSignals(false);
        emit sigBrushSizeChanged(value);
    }
    void DrawerPanel::OnBrushSliderChanged(int value) {        
        d->ui->BrushSizeSpin->blockSignals(true);
        d->ui->BrushSizeSpin->setValue(value);
        d->ui->BrushSizeSpin->blockSignals(false);
        emit sigBrushSizeChanged(value);
    }

    auto DrawerPanel::Init() -> void {
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->contentsWidget->setObjectName("panel-contents");

        d->segButtonGroup = new QButtonGroup;
        d->segButtonGroup->setExclusive(true);

        d->segButtonGroup->addButton(d->ui->AddBtn, SegButtonId::Add);
        d->segButtonGroup->addButton(d->ui->SubBtn, SegButtonId::Subtract);
        d->segButtonGroup->addButton(d->ui->PaintBtn, SegButtonId::Paint);
        d->segButtonGroup->addButton(d->ui->WipeBtn, SegButtonId::Wipe);
        d->segButtonGroup->addButton(d->ui->FillBtn, SegButtonId::Fill);
        d->segButtonGroup->addButton(d->ui->EraseBtn, SegButtonId::Erase);

        d->ui->BrushSizeSlider->setRange(1, 100);
        d->ui->BrushSizeSpin->setRange(1, 100);
        d->ui->BrushSizeSlider->setSingleStep(1);        

        d->ui->BrushSizeSlider->blockSignals(true);
        d->ui->BrushSizeSlider->setValue(5);
        d->ui->BrushSizeSlider->blockSignals(false);
        d->ui->BrushSizeSpin->blockSignals(true);
        d->ui->BrushSizeSpin->setValue(5);
        d->ui->BrushSizeSpin->blockSignals(false);

        connect(d->segButtonGroup, SIGNAL(buttonClicked(int)), this, SLOT(OnSegButtonClicked(int)));
        connect(d->ui->BrushSizeSpin, SIGNAL(valueChanged(int)), this, SLOT(OnBrushSpinChanged(int)));
        connect(d->ui->BrushSizeSlider, SIGNAL(valueChanged(int)), this, SLOT(OnBrushSliderChanged(int)));                        
        
        SetControlWidget(SegButtonId::None);
    }

    auto DrawerPanel::InitToolTips() -> void {
        d->ui->AddBtn->setToolTip("Draw 2D label with lasso tool");
        d->ui->SubBtn->setToolTip("Erase 2D label with lasso tool");
        d->ui->PaintBtn->setToolTip("Draw 2D label with paint tool");
        d->ui->WipeBtn->setToolTip("Erase 2D label with paint tool");
        d->ui->FillBtn->setToolTip("Fill closed 2D hole");
        d->ui->EraseBtn->setToolTip("Remove connected 2D label");
    }

    auto DrawerPanel::ResetIcons() -> void {        
        d->ui->AddBtn->setStyleSheet("");
        d->ui->SubBtn->setStyleSheet("");
        d->ui->PaintBtn->setStyleSheet("");
        d->ui->WipeBtn->setStyleSheet("");
        d->ui->FillBtn->setStyleSheet("");
        d->ui->EraseBtn->setStyleSheet("");
        SetControlWidget(-1);
    }
    auto DrawerPanel::SetIconOn() -> void {
        if (d->cur_id < 0)
            return;
        switch(d->cur_id) {
        case Entity::ToolIdx::ADD:            
            d->ui->AddBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            break;
        case Entity::ToolIdx::SUBTRACT:            
            d->ui->SubBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            break;
        case Entity::ToolIdx::PAINT:
            d->ui->PaintBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            break;
        case Entity::ToolIdx::WIPE:            
            d->ui->WipeBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            break;
        case Entity::ToolIdx::Fill:            
            d->ui->FillBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            break;
        case Entity::ToolIdx::ERASE:            
            d->ui->EraseBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            break;
        default:
            break;
        }
        SetControlWidget(d->cur_id);
    }
    auto DrawerPanel::InitIcons() -> void {
        d->AddIcon = QIcon(":/image/images/Add.png");        
        d->ui->AddBtn->setIcon(d->AddIcon);
        d->ui->AddBtn->setIconSize(QSize(52, 52));
        d->ui->AddBtn->setShortcut(QString("A"));

        d->SubIcon = QIcon(":/image/images/Subtract.png");        
        d->ui->SubBtn->setIcon(d->SubIcon);
        d->ui->SubBtn->setIconSize(QSize(52, 52));
        d->ui->SubBtn->setShortcut(QString("S"));

        d->PaintIcon = QIcon(":/image/images/Paint.png");        
        d->ui->PaintBtn->setIcon(d->PaintIcon);
        d->ui->PaintBtn->setIconSize(QSize(52, 52));
        d->ui->PaintBtn->setShortcut(QString("P"));

        d->WipeIcon = QIcon(":/image/images/Wipe.png");        
        d->ui->WipeBtn->setIcon(d->WipeIcon);
        d->ui->WipeBtn->setIconSize(QSize(52, 52));
        d->ui->WipeBtn->setShortcut(QString("W"));

        d->FillIcon = QIcon(":/image/images/Fill.png");        
        d->ui->FillBtn->setIcon(d->FillIcon);
        d->ui->FillBtn->setIconSize(QSize(52, 52));
        d->ui->FillBtn->setShortcut(QString("F"));

        d->EraseIcon = QIcon(":/image/images/Erase.png");        
        d->ui->EraseBtn->setIcon(d->EraseIcon);
        d->ui->EraseBtn->setIconSize(QSize(52, 52));
        d->ui->EraseBtn->setShortcut(QString("E"));        
    }
    auto DrawerPanel::SetControlWidget(int id) -> void {
        QWidget* controlWidget = nullptr;
        switch(id) {
        case Entity::ToolIdx::PAINT:
        case Entity::ToolIdx::WIPE:
            controlWidget = d->ui->stackedWidget->findChild<QWidget*>("BrushPage");
            break;
        case SegButtonId::None:
        case 5:
        case 6:
        case 3:
        case 4:
        default:
            break;
        }

        if (controlWidget) {
            d->ui->stackedWidget->setCurrentWidget(controlWidget);
            d->ui->stackedWidget->show();
        }else {
            d->ui->stackedWidget->hide();
        }
    }
}