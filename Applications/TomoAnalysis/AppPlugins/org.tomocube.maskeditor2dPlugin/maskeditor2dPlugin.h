#pragma once

#include <IAppActivator.h>

const QString plugin_symbolic_name = "org.tomocube.maskeditor2dPlugin";

namespace TomoAnalysis::MaskEditor2d::AppUI {
    class maskeditor2dPlugin : public TC::Framework::IAppActivator {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.maskeditor2dPlugin")
    public:
        static maskeditor2dPlugin* getInstance();

        maskeditor2dPlugin();
        ~maskeditor2dPlugin();

        auto start(ctkPluginContext* context) -> void override;
        auto stop(ctkPluginContext* context) -> void override;
        auto getPluginContext() -> ctkPluginContext* const override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
        static maskeditor2dPlugin* instance;
    };
}