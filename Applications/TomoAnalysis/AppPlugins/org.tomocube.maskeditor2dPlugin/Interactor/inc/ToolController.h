#pragma once

#include <memory>

#include <ME2Tool.h>
#include <IScenePort.h>
#include <IPanelPort.h>

#include "MaskEditor2dInteractorExport.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {    
    class MaskEditor2dInteractor_API ToolController {
    public:
        ToolController(UseCase::IScenePort* scene, UseCase::IPanelPort* panel);
        virtual ~ToolController();

        auto ActivateTool(Entity::ToolIdx id)->bool;
        auto DeactivateTool()->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}