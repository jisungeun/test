#pragma once

#include <memory>
#include <ME2WorkingSet.h>
#include <QList>
#include <QString>

#include "MaskEditor2dInteractorExport.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    struct MaskEditor2dInteractor_API MaskControlDS {
        typedef std::shared_ptr<MaskControlDS> Pointer;
        //QList<QString> maskList{ QList<QString>() };
        QList<MaskMeta> maskList{ QList<MaskMeta>() };
        QString curMaskPath;
        int curMaskType{ -1 };
        int curMaskIdx{ -1 };
    };
    class MaskEditor2dInteractor_API IMaskControlPanel {
    public:
        IMaskControlPanel();
        virtual ~IMaskControlPanel();

        auto GetDS() const->MaskControlDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}