#pragma once

#include <memory>

#include "MaskEditor2dInteractorExport.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
	struct MaskEditor2dInteractor_API SemiAutoDS {
		typedef std::shared_ptr<SemiAutoDS> Pointer;
		int cur_id{ -1 };
		bool isSystem{ false };
	};
	class MaskEditor2dInteractor_API ISemiAutoPanel {
	public:
		ISemiAutoPanel();
		virtual ~ISemiAutoPanel();

		auto GetDS()const->SemiAutoDS::Pointer;

		virtual auto Update()->bool = 0;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}