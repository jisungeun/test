#pragma once

#include <memory>

#include "IScenePort.h"

#include <MaskEditor2dInteractorExport.h>

namespace TomoAnalysis::MaskEditor2d::Interactor {
	class ISceneManagerWidget;
	class MaskEditor2dInteractor_API ScenePresenter : public UseCase::IScenePort {
	public:
		ScenePresenter(ISceneManagerWidget* widget);
		virtual ~ScenePresenter();

		auto ActivateTool(Entity::ToolIdx id) -> void override;
		auto DeactivateTool(bool isFunc) -> void override;
		auto Update() -> void override;
		auto Reset() -> void override;
		auto SaveCurrent() -> bool override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}