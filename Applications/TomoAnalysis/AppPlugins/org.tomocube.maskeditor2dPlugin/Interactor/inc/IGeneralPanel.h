#pragma once

#include <QString>

#include <memory>

#include "MaskEditor2dInteractorExport.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    struct MaskEditor2dInteractor_API GeneralDS {
        typedef std::shared_ptr<GeneralDS> Pointer;
        bool standAlone{ true };
        QString linked_app{ QString() };
        QString file_name{QString()};
        QString user_name{QString()};
    };
    class MaskEditor2dInteractor_API IGeneralPanel {
    public:
        IGeneralPanel();
        virtual ~IGeneralPanel();

        auto GetDS() const->GeneralDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}