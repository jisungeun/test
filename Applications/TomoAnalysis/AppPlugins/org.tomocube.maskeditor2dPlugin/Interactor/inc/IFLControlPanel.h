#pragma once

#include <memory>

#include "MaskEditor2dInteractorExport.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    struct MaskEditor2dInteractor_API FLControlDS {
        double chOpa[3]{ 0.5,0.5,0.5 };

        int color[3][3]{ {0,0,255},{0,255,0},{255,0,0} };

        int curCh{ 0 };

        bool chExist[3]{ false, };        

        typedef std::shared_ptr<FLControlDS> Pointer;
    };
    class MaskEditor2dInteractor_API IFLControlPanel {
    public:
        IFLControlPanel();
        virtual ~IFLControlPanel();

        auto GetDS() const->FLControlDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}