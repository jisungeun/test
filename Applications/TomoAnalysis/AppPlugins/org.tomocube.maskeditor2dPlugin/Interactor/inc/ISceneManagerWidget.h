#pragma once

#include <memory>

#include <QObject>

#include <TCMask.h>
#include <TCImage.h>

#include "MaskEditor2dInteractorExport.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    class MaskEditor2dInteractor_API ISceneManagerWidget : public QObject {
        Q_OBJECT
    public:
        ISceneManagerWidget();
        virtual ~ISceneManagerWidget();

        virtual auto SetImage(TCImage::Pointer& image,QString name)->bool = 0;
        virtual auto SetMask(TCMask::Pointer& mask,QString name)->bool = 0;
        virtual auto RemoveMask(QString name)->bool = 0;                
        virtual auto GetMask()->TCMask::Pointer = 0;
        virtual auto RearrangeLabel()->void =0;

        virtual auto ActivateTool(int idx)->bool = 0;
        virtual auto DeactivateTool(bool isFunc = false)->bool = 0;

        virtual auto Reset()->void = 0;
    };
}