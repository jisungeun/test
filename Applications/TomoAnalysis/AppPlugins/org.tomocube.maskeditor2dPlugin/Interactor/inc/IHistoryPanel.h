#pragma once

#include <memory>

#include "MaskEditor2dInteractorExport.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    struct MaskEditor2dInteractor_API HistoryDS {
        typedef std::shared_ptr<HistoryDS> Pointer;
    };
    class MaskEditor2dInteractor_API IHistoryPanel {
    public:
        IHistoryPanel();
        virtual ~IHistoryPanel();

        auto GetDS() const->HistoryDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}