#pragma once

#include <memory>


#include <IImageReaderPort.h>
#include <IMaskReaderPort.h>
#include <IMaskWriterPort.h>
#include <IScenePort.h>
#include <IPanelPort.h>

#include "MaskEditor2dInteractorExport.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    class MaskEditor2dInteractor_API ImageDataController {
    public:
        ImageDataController(UseCase::IScenePort* scene,UseCase::IPanelPort* panel,UseCase::IImageReaderPort* ireader,UseCase::IMaskReaderPort* mreader,UseCase::IMaskWriterPort* mwriter);
        virtual ~ImageDataController();

        auto LoadTcfImage(const QString& imgPath,const int& time_step)->bool;
        auto LoadMask(const QString&mskPath,const QString& organName)->bool;
        auto LoadMaskFile(const QString& mskPath)->bool;
        auto UnloadMask()->bool;
        auto AddMask(const QString& organName,const int& maskType)->bool;
        auto DummyMask(const QString& organNAme, const int& maskType)->bool;
        auto SaveMask(const QString& organName)->bool;        
        auto SetMaskPath(const QString& maskPath)->bool;
                
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}