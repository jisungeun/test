#pragma once

#include <memory>
#include <QStringList>
#include "MaskEditor2dInteractorExport.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    struct MaskEditor2dInteractor_API IDManagerDS {
        typedef std::shared_ptr<IDManagerDS> Pointer;
        QStringList idList;
        QString curUser;
        QString imagePath;
        QString curInstPath;
        QString curOrganPath;
        QString workindDir;
    };
    class MaskEditor2dInteractor_API IIDManagerPanel {
    public:
        IIDManagerPanel();
        virtual ~IIDManagerPanel();

        auto GetDS() const->IDManagerDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}