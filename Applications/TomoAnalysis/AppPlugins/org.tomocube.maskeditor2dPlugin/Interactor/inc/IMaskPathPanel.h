#pragma once

#include <memory>

#include <QString>

#include "MaskEditor2dInteractorExport.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    struct MaskEditor2dInteractor_API MaskPathDS {
        typedef std::shared_ptr<MaskPathDS> Pointer;
        QString imageName{ QString() };
        QString path{ QString() };
    };
    class MaskEditor2dInteractor_API IMaskPathPanel {
    public:
        IMaskPathPanel();
        virtual ~IMaskPathPanel();

        auto GetDS()const->MaskPathDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}