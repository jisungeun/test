#pragma once

#include <memory>

#include "MaskEditor2dInteractorExport.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    struct MaskEditor2dInteractor_API CleanerDS {
        typedef std::shared_ptr<CleanerDS> Pointer;
        int cur_idx{ -1 };
        bool isSystem{ false };
    };
    class MaskEditor2dInteractor_API ICleanerPanel {
    public:
        ICleanerPanel();
        virtual ~ICleanerPanel();

        auto GetDS() const->CleanerDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}