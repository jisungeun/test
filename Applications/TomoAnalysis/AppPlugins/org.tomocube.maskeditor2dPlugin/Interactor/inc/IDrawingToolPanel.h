#pragma once

#include <memory>

#include "MaskEditor2dInteractorExport.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    struct MaskEditor2dInteractor_API DrawingToolDS {
        typedef std::shared_ptr<DrawingToolDS> Pointer;
        int curToolIdx{ -1 };
        bool isSystem{ false };
    };
    class MaskEditor2dInteractor_API IDrawingToolPanel {
    public:
        IDrawingToolPanel();
        virtual ~IDrawingToolPanel();

        auto GetDS() const->DrawingToolDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:        

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}