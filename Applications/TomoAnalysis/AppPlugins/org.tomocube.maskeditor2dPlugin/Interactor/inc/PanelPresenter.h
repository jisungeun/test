#pragma once

#include <memory>

#include "IPanelPort.h"

#include <MaskEditor2dInteractorExport.h>

namespace TomoAnalysis::MaskEditor2d::Interactor {
    class ICleanerPanel;
    class IDrawingToolPanel;
    class IFLControlPanel;
    class IGeneralPanel;
    class IHistoryPanel;
    class IIDManagerPanel;
    class ILabelControlPanel;
    class IMaskControlPanel;
    class ISemiAutoPanel;
    class IMaskPathPanel;
    class MaskEditor2dInteractor_API PanelPresenter : public UseCase::IPanelPort {
    public:
        PanelPresenter(ICleanerPanel* cleaner,IDrawingToolPanel* drawing,IFLControlPanel* flControl,IGeneralPanel* general,IHistoryPanel* history,IIDManagerPanel* idmanager,ILabelControlPanel* labelControl,IMaskControlPanel* maskControl,ISemiAutoPanel* semiauto,IMaskPathPanel* maskpath);
        virtual ~PanelPresenter();

        auto Update() -> void override;;
        auto Reset() -> void override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}