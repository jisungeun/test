#include "IHistoryPanel.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
	struct IHistoryPanel::Impl {
		HistoryDS::Pointer ds = std::make_shared<HistoryDS>();
	};
	IHistoryPanel::IHistoryPanel() : d{ new Impl } {

	}
	IHistoryPanel::~IHistoryPanel() {

	}
	auto IHistoryPanel::GetDS() const -> HistoryDS::Pointer {
		return d->ds;
	}
}