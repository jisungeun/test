#include "IIDManagerPanel.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
	struct IIDManagerPanel::Impl {
		IDManagerDS::Pointer ds = std::make_shared<IDManagerDS>();
	};
	IIDManagerPanel::IIDManagerPanel() : d{ new Impl } {

	}
	IIDManagerPanel::~IIDManagerPanel() {

	}
	auto IIDManagerPanel::GetDS() const -> IDManagerDS::Pointer {
		return d->ds;
	}
}