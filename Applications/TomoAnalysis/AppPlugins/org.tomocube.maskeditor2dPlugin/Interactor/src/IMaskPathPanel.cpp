#include "IMaskPathPanel.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    struct IMaskPathPanel::Impl {
        MaskPathDS::Pointer ds = std::make_shared<MaskPathDS>();
    };
    IMaskPathPanel::IMaskPathPanel() : d{ new Impl } {
        
    }
    IMaskPathPanel::~IMaskPathPanel() {
        
    }
    auto IMaskPathPanel::GetDS() const -> MaskPathDS::Pointer {
        return d->ds;
    }
}