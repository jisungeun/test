#include <ManageTool.h>

#include "ToolController.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    using namespace UseCase;
    struct ToolController::Impl {
        IScenePort* scenePort{ nullptr };
        IPanelPort* panelPort{ nullptr };
    };
    ToolController::ToolController(UseCase::IScenePort* scene, UseCase::IPanelPort* panel) : d{ new Impl } {
        d->scenePort = scene;
        d->panelPort = panel;
    }
    ToolController::~ToolController() {
        
    }
    auto ToolController::ActivateTool(Entity::ToolIdx id) -> bool {
        if(nullptr == d->scenePort) {
            return false;
        }
        if(nullptr == d->panelPort) {
            return false;
        }
        UseCase::ManageTool useCase;
        return useCase.ActivateSegmenationTool(id,d->scenePort,d->panelPort);
    }
    auto ToolController::DeactivateTool() -> bool {
        if (nullptr == d->scenePort) {
            return false;
        }
        if (nullptr == d->panelPort) {
            return false;
        }
        UseCase::ManageTool useCase;
        return useCase.FinishSegmentationTool(d->scenePort, d->panelPort);
    }
}
