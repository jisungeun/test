#include "IMaskControlPanel.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
	struct IMaskControlPanel::Impl {
		MaskControlDS::Pointer ds = std::make_shared<MaskControlDS>();
	};
	IMaskControlPanel::IMaskControlPanel() : d{ new Impl } {

	}
	IMaskControlPanel::~IMaskControlPanel() {

	}
	auto IMaskControlPanel::GetDS() const -> MaskControlDS::Pointer {
		return d->ds;
	}
}