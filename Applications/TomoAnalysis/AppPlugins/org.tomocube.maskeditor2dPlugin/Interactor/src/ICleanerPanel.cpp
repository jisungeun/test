#include "ICleanerPanel.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
	struct ICleanerPanel::Impl {
		CleanerDS::Pointer ds = std::make_shared<CleanerDS>();
	};
	ICleanerPanel::ICleanerPanel() : d{ new Impl } {
        
    }
	ICleanerPanel::~ICleanerPanel() {
        
    }
	auto ICleanerPanel::GetDS() const -> CleanerDS::Pointer {
		return d->ds;
    }
}