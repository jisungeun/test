#include <QFileInfo>

#include "ME2WorkingSet.h"

#include "ICleanerPanel.h"
#include "IDrawingToolPanel.h"
#include "IFLControlPanel.h"
#include "IGeneralPanel.h"
#include "IHistoryPanel.h"
#include "IIDManagerPanel.h"
#include "ILabelControlPanel.h"
#include "IMaskControlPanel.h"
#include "IMaskPathPanel.h"
#include "ISemiAutoPanel.h"

#include "PanelPresenter.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    struct PanelPresenter::Impl {
        ICleanerPanel* cleaner{ nullptr };
        IDrawingToolPanel* drawing{ nullptr };
        IFLControlPanel* flcontrol{ nullptr };
        IGeneralPanel* general{ nullptr };
        IHistoryPanel* history{ nullptr };
        IIDManagerPanel* idmanager{ nullptr };
        ILabelControlPanel* labelcontrol{ nullptr };
        IMaskControlPanel* maskcontrol{ nullptr };
        ISemiAutoPanel* semiauto{ nullptr };
        IMaskPathPanel* maskpath{ nullptr };
    };
    PanelPresenter::PanelPresenter(ICleanerPanel* cleaner, IDrawingToolPanel* drawing, IFLControlPanel* flControl, IGeneralPanel* general, IHistoryPanel* history, IIDManagerPanel* idmanager, ILabelControlPanel* labelControl, IMaskControlPanel* maskControl, ISemiAutoPanel* semiauto,IMaskPathPanel* maskpath) : d{ new Impl } {
        d->cleaner = cleaner;
        d->drawing = drawing;
        d->flcontrol = flControl;
        d->general = general;
        d->history = history;
        d->idmanager = idmanager;
        d->labelcontrol = labelControl;
        d->maskcontrol = maskControl;
        d->semiauto = semiauto;
        d->maskpath = maskpath;
    }
    PanelPresenter::~PanelPresenter() {
        
    }
    auto PanelPresenter::Update() -> void {
        auto ws = Entity::WorkingSet::GetInstance();
        if(nullptr != d->cleaner) {
            auto ds = d->cleaner->GetDS();            
            ds->cur_idx = ws->GetCurrentTool();
            d->cleaner->Update();
        }
        if(nullptr != d->drawing) {
            auto ds = d->drawing->GetDS();            
            ds->curToolIdx = ws->GetCurrentTool();
            d->drawing->Update();
        }
        if(nullptr != d->flcontrol) {
            auto ds = d->flcontrol->GetDS();            
            d->flcontrol->Update();
        }
        if(nullptr != d->general) {
            auto ds = d->general->GetDS();            
            d->general->Update();
        }
        if(nullptr != d->idmanager) {
            auto ds = d->idmanager->GetDS();            
            d->idmanager->Update();
        }
        if(nullptr != d->labelcontrol) {
            auto ds = d->labelcontrol->GetDS();
            if(ws->GetCurrentTool()._to_integral() == Entity::ToolIdx::PICK) {
                ds->isEnable = true;
            }else {
                ds->isEnable = false;
            }
            if(ws->GetCurrentTool()._to_integral() == Entity::ToolIdx::MERGE_LABEL) {
                ds->isMerge = true;
            }else {
                ds->isMerge = false;
            }            
            d->labelcontrol->Update();
        }
        if(nullptr != d->maskcontrol) {
            auto ds = d->maskcontrol->GetDS();
            ds->maskList = ws->GetMaskList();
            auto mask = ws->GetMask();
            ds->curMaskType = -1;
            ds->curMaskIdx = -1;
            if(nullptr != mask) {
                /*if (mask->GetType()._to_integral() == MaskTypeEnum::MultiLabel) {
                    ds->curMaskType = 0;
                }else {
                    ds->curMaskType = 1;
                }*/
                if(mask->GetTypeText() == "Label") {
                    ds->curMaskType = 0;
                }else {
                    ds->curMaskType = 1;
                }
                auto idx = -1;
                for (auto i = 0; i < ds->maskList.count();i++) {
                    if(mask->GetName() == ds->maskList[i].name) {
                        idx = i;
                        break;
                    }
                }
                ds->curMaskIdx = idx;
                ds->curMaskPath = ws->GetMaskPath();
            }
            d->maskcontrol->Update();
        }
        if(nullptr != d->semiauto) {
            auto ds = d->semiauto->GetDS();
            ds->cur_id = ws->GetCurrentTool();
            d->semiauto->Update();
        }
        if(nullptr != d->maskpath) {
            auto ds = d->maskpath->GetDS();
            auto img = ws->GetImage();
            if(nullptr != img) {
                auto imgpath = img->GetFilePath();
                QFileInfo info(imgpath);
                ds->imageName = info.fileName().chopped(4);
            }            
            ds->path = ws->GetMaskPath();

            d->maskpath->Update();
        }
    }
    auto PanelPresenter::Reset() -> void {
        if (nullptr == d->cleaner) {
            auto ds = d->cleaner->GetDS();
            ds.reset();
            d->cleaner->Update();
        }
        if (nullptr != d->drawing) {
            auto ds = d->drawing->GetDS();
            ds.reset();
            d->drawing->Update();
        }
        if (nullptr != d->flcontrol) {
            auto ds = d->flcontrol->GetDS();
            ds.reset();
            d->flcontrol->Update();
        }
        if (nullptr != d->general) {
            auto ds = d->general->GetDS();
            ds.reset();
            d->general->Update();
        }
        if (nullptr != d->idmanager) {
            auto ds = d->idmanager->GetDS();
            ds.reset();
            d->idmanager->Update();
        }
        if (nullptr != d->labelcontrol) {
            auto ds = d->labelcontrol->GetDS();
            ds.reset();
            d->labelcontrol->Update();
        }
        if (nullptr != d->maskcontrol) {
            auto ds = d->maskcontrol->GetDS();
            ds.reset();
            d->maskcontrol->Update();
        }
        if (nullptr != d->semiauto) {
            auto ds = d->semiauto->GetDS();
            ds.reset();
            d->semiauto->Update();
        }
        if(nullptr != d->maskpath) {
            auto ds = d->maskpath->GetDS();
            ds.reset();
            d->maskpath->Update();
        }
    }
}
