#include "ILabelControlPanel.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
	struct ILabelControlPanel::Impl {
		LabelControlDS::Pointer ds = std::make_shared<LabelControlDS>();
	};
	ILabelControlPanel::ILabelControlPanel() : d{ new Impl } {

	}
	ILabelControlPanel::~ILabelControlPanel() {

	}
	auto ILabelControlPanel::GetDS() const -> LabelControlDS::Pointer {
		return d->ds;
	}
}