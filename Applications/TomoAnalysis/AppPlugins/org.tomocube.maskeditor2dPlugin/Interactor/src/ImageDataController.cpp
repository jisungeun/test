//UseCase
#include <OpenData.h>
#include <SaveData.h>

#include "ImageDataController.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    using namespace UseCase;
    struct ImageDataController::Impl {
        IScenePort* scenePort{nullptr};
        IPanelPort* panelPort{ nullptr };
        IImageReaderPort* ireader{nullptr};
        IMaskReaderPort* mreader{ nullptr };
        IMaskWriterPort* mwriter{ nullptr };
    };
    ImageDataController::ImageDataController(UseCase::IScenePort* scene,UseCase::IPanelPort* panel, UseCase::IImageReaderPort* ireader, UseCase::IMaskReaderPort* mreader, UseCase::IMaskWriterPort* mwriter) : d{ new Impl } {
        d->scenePort = scene;
        d->panelPort = panel;
        d->ireader = ireader;
        d->mreader = mreader;
        d->mwriter = mwriter;
    }
    ImageDataController::~ImageDataController() {
        
    }
    auto ImageDataController::LoadTcfImage(const QString& imgPath,const int& time_step) -> bool {
        if(nullptr == d->scenePort) {
            return false;
        }
        if(nullptr == d->panelPort) {
            return false;
        }
        if(nullptr == d->ireader) {
            return false;
        }

        UseCase::OpenData useCase;
        return useCase.LoadImage(d->scenePort, d->panelPort, d->ireader, imgPath, time_step);                
    }
    auto ImageDataController::LoadMaskFile(const QString& mskPath) -> bool {
        if (nullptr == d->scenePort) {
            return false;
        }
        if (nullptr == d->panelPort) {
            return false;
        }
        if (nullptr == d->mreader) {
            return false;
        }
        UseCase::OpenData useCase;
        return useCase.LoadMskFile(d->scenePort,d->panelPort,d->mreader,mskPath);
    }
    auto ImageDataController::LoadMask(const QString&mskPath,const QString&organName) -> bool {
        if (nullptr == d->scenePort) {
            return false;
        }
        if (nullptr == d->panelPort) {
            return false;
        }
        if (nullptr == d->mreader) {
            return false;
        }
        UseCase::OpenData useCase;
        return useCase.LoadMask(d->scenePort, d->panelPort, d->mreader,mskPath, organName);
    }
    auto ImageDataController::UnloadMask() -> bool {
        if(nullptr == d->scenePort) {
            return false;
        }        
        UseCase::OpenData useCase;
        return useCase.ResetMask(d->scenePort);
    }
    auto ImageDataController::SaveMask(const QString& organName) -> bool {        
        if (nullptr == d->panelPort) {
            return false;
        }
        if (nullptr == d->mwriter) {
            return false;
        }
        UseCase::SaveData useCase;
        return useCase.SaveMask(d->scenePort,d->panelPort, d->mwriter, organName);
    }  
    auto ImageDataController::SetMaskPath(const QString& maskPath) -> bool {
        if(nullptr ==d->panelPort) {
            return false;
        }
        UseCase::SaveData useCase;
        return useCase.ChangeMaskPath(d->panelPort,maskPath);
    }
    auto ImageDataController::AddMask(const QString& organName,const int& maskType) -> bool {
        if(nullptr == d->scenePort) {
            return false;
        }
        if(nullptr == d->panelPort) {
            return false;
        }
        if(nullptr == d->mreader) {
            return false;
        }
        UseCase::OpenData useCase;
        return useCase.CreateMask(d->scenePort,d->panelPort,d->mreader, organName,maskType);
    }
    auto ImageDataController::DummyMask(const QString& organName, const int& maskType) -> bool {
        if(nullptr == d->scenePort) {
            return false;
        }
        if(nullptr == d->panelPort) {
            return false;
        }
        if(nullptr == d->mreader) {
            return false;
        }
        UseCase::OpenData useCase;
        return useCase.CreateDummyMask(d->scenePort, d->panelPort, d->mreader, organName, maskType);
    }
}