#include "ISceneManagerWidget.h"

#include "ScenePresenter.h"

namespace TomoAnalysis::MaskEditor2d::Interactor {
    struct ScenePresenter::Impl {
        ISceneManagerWidget* widget{ nullptr };
    };
    ScenePresenter::ScenePresenter(ISceneManagerWidget* widget) : d{ new Impl } {
        d->widget = widget;        
    }
    ScenePresenter::~ScenePresenter() {
        
    }
    auto ScenePresenter::ActivateTool(Entity::ToolIdx id) -> void {
        if (nullptr != d->widget) {
            d->widget->ActivateTool(id);
        }
    }
    auto ScenePresenter::DeactivateTool(bool isFunc) -> void {
        if(nullptr != d->widget) {
            d->widget->DeactivateTool(isFunc);
        }
    }
    auto ScenePresenter::Update() -> void {
        auto ws = Entity::WorkingSet::GetInstance();
        if(nullptr != ws->GetImage()) {
            d->widget->SetImage(ws->GetImage(),ws->GetImage()->GetFilePath());
        }        
        if(nullptr != ws->GetMask()) {
            QString prevName = QString();
            if(nullptr != d->widget->GetMask()) {
                prevName = d->widget->GetMask()->GetName();
            }            
            auto curName = ws->GetMask()->GetName();
            if (prevName != curName) {
                d->widget->SetMask(ws->GetMask(), ws->GetMask()->GetName());
            }
        }else {
            auto mask = d->widget->GetMask();
            if(nullptr != mask) {
                auto prevName = mask->GetName();
                d->widget->RemoveMask(prevName);
            }            
        }
    }
    auto ScenePresenter::SaveCurrent() -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        auto mask = d->widget->GetMask();
        if(nullptr == mask) {
            return false;
        }        
        ws->SetMask(mask);
        return true;
    }
    auto ScenePresenter::Reset() -> void {
        if (nullptr != d->widget) {
            d->widget->Reset();
        }
    }    
}