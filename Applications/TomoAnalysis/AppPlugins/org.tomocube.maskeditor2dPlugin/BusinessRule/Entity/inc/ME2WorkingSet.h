#pragma once

#include <memory>

#include "ME2Tool.h"

#include <TCImage.h>
#include <TCMask.h>
#include <TCFMetaReader.h>

#include "MaskEditor2dEntityExport.h"

namespace TomoAnalysis::MaskEditor2d::Entity {
    class MaskEditor2dEntity_API WorkingSet {
    public:
        typedef WorkingSet Self;
        typedef std::shared_ptr<Self> Pointer;
        virtual ~WorkingSet();

        static auto GetInstance()->Pointer;

        auto Reset()->void;

        auto SetImage(TCImage::Pointer img)->void;
        auto GetImage()->TCImage::Pointer;
        auto SetMask(TCMask::Pointer mask)->void;
        auto GetMask()->TCMask::Pointer;
        auto SetMaskPath(const QString& path)->void;
        auto GetMaskPath()->QString;
        auto SetCurrentMaskIdx(int idx)->void;
        auto GetCurrentMaskIdx()->int;
        auto GetMaskIdx(QString name)->int;
        auto SetTimeStep(int time_step)->void;
        auto GetTimeStep()->int;
        auto SetTimePoint(int timePoint)->void;
        auto GetTimePoint()->int;
        auto SetCurrentTool(ToolIdx ID)->void;
        auto GetCurrentTool()->ToolIdx;
        auto AppendMaskMeta(const MaskMeta& name)->bool;
        auto GetMaskList()->QList<MaskMeta>;
        auto ClearMaskList()->void;
        auto SetCurLabel(int val)->void;
        auto GetCurLabel()->int;

    protected:
        WorkingSet();
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}