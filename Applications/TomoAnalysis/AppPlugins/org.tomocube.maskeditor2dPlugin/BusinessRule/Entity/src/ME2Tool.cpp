#include "ME2Tool.h"

namespace TomoAnalysis::MaskEditor2d::Entity {
    struct Tool::Impl {

    };
    Tool::Tool() : d{ new Impl } {

    }
    Tool::Tool(const Tool& other) : d{ new Impl } {
        *d = *other.d;
    }
    Tool::~Tool() {

    }
}