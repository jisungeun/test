#include <iostream>

#include <QMap>

#include "ME2WorkingSet.h"

namespace TomoAnalysis::MaskEditor2d::Entity {
    struct WorkingSet::Impl {
        TCImage::Pointer img;
        TCMask::Pointer mask;
        int time_step{ 0 };
        int time_point{ 0 };
        int mask_idx{ -1 };
        int curLabel{ 1 };
        QString maskPath;
        QList<MaskMeta> maskList;        
        ToolIdx curID{ ToolIdx::None };
    };
    WorkingSet::WorkingSet() : d{ new Impl } {
        
    }
    WorkingSet::~WorkingSet() {
        
    }
    auto WorkingSet::GetInstance() -> Pointer {
        static Pointer theInstance{ new WorkingSet() };
        return theInstance;
    }
    auto WorkingSet::Reset() -> void {
        d->img = nullptr;
        d->mask = nullptr;
        d->time_step = 0;
        d->time_point = 0;
        d->curLabel = 1;
        d->mask_idx = -1;
        d->maskPath = QString();        
        d->maskList.clear();
        d->curID = ToolIdx::None;
    }
    auto WorkingSet::GetCurLabel() -> int {
        return d->curLabel;
    }
    auto WorkingSet::SetCurLabel(int val) -> void {
        d->curLabel = val;
    }

    auto WorkingSet::GetImage() -> TCImage::Pointer {
        return d->img;
    }
    auto WorkingSet::SetImage(TCImage::Pointer img) -> void {
        d->img = img;
    }
    auto WorkingSet::SetMaskPath(const QString& path) -> void {
        d->maskPath = path;
    }
    auto WorkingSet::GetMaskPath() -> QString {
        return d->maskPath;
    }
    auto WorkingSet::GetMask() -> TCMask::Pointer {
        return d->mask;
    }
    auto WorkingSet::SetMask(TCMask::Pointer mask) -> void {
        d->mask = mask;
    }
    auto WorkingSet::GetCurrentMaskIdx() -> int {
        return d->mask_idx;
    }
    auto WorkingSet::SetCurrentMaskIdx(int idx) -> void {
        d->mask_idx = idx;
    }
    auto WorkingSet::GetMaskIdx(QString name) -> int {
        auto idx = -1;
        for (auto i = 0; d->maskList.count();i++){
            if(d->maskList[i].name == name) {
                idx = i;
                break;
            }
        }            
        return idx;
    }
    auto WorkingSet::GetTimePoint() -> int {
        return d->time_point;
    }
    auto WorkingSet::SetTimePoint(int timePoint) -> void {
        d->time_point = timePoint;
    }
    auto WorkingSet::GetTimeStep() -> int {
        return d->time_step;
    }
    auto WorkingSet::SetTimeStep(int time_step) -> void {
        d->time_step = time_step;
    }
    auto WorkingSet::GetCurrentTool() -> ToolIdx {
        return d->curID;
    }
    auto WorkingSet::SetCurrentTool(ToolIdx ID) -> void {
        d->curID = ID;
    }
    auto WorkingSet::GetMaskList() -> QList<MaskMeta> {
        return d->maskList;
    }
    auto WorkingSet::AppendMaskMeta(const MaskMeta& name) -> bool {
        for(auto i=0;i<d->maskList.count();i++) {
            if(d->maskList[i].name == name.name) {
                return false;
            }
        }        
        d->maskList.append(name);
        return true;
    }
    auto WorkingSet::ClearMaskList() -> void {
        d->maskList.clear();
    }
}