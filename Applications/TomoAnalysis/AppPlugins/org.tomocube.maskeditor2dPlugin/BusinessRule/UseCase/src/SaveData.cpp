#include <iostream>

#include "IScenePort.h"
#include "IPanelPort.h"
#include "IMaskWriterPort.h"

#include <TCDataConverter.h>

#include "SaveData.h"

namespace TomoAnalysis::MaskEditor2d::UseCase {
	SaveData::SaveData() {
        
    }
    SaveData::~SaveData() {
        
    }
    auto SaveData::ChangeMaskPath(IPanelPort* panelPort, const QString& path) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        auto mask = ws->GetMask();
        if(nullptr == mask) {
            return false;
        }
        ws->SetMaskPath(path);

        panelPort->Update();
        return true;
    }
    auto SaveData::SaveMask(IScenePort* scenePort,IPanelPort* panelPort, IMaskWriterPort* mwriter, const QString& organName) -> bool {        
        auto ws = Entity::WorkingSet::GetInstance();
        auto maskPath = ws->GetMaskPath();
        if (maskPath.isEmpty()) {
            return false;
        }

        if(false == scenePort->SaveCurrent()) {
            return false;
        }

        auto mask = ws->GetMask();
        if(nullptr == mask) {
            return false;
        }        
        auto image = ws->GetImage();
        auto curTimeStep = mask->GetTimeStep();
        if(image->GetTimeSteps() <= curTimeStep) {
            return false;
        }

        auto maskList = ws->GetMaskList();
        auto idx = -1;
        for(auto i=0;i<maskList.count();i++) {
            if(maskList[i].name == organName) {
                idx = i;
                break;
            }
        }
        if(idx<0) {
            std::cout << "Cannot find mask name match" << std::endl;
            return false;
        }

        mwriter->Write(mask, maskPath, organName,idx);

        ws->SetCurrentTool(Entity::ToolIdx::None);
        panelPort->Update();
        return true;
    }    
}
