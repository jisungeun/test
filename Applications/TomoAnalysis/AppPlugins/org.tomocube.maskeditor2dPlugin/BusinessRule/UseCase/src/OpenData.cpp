#include <iostream>
#include <QFileInfo>

#include "IPanelPort.h"
#include "IScenePort.h"
#include "IMaskReaderPort.h"
#include "IImageReaderPort.h"

#include "OpenData.h"

namespace TomoAnalysis::MaskEditor2d::UseCase {    
    OpenData::OpenData(){
        
    }
    OpenData::~OpenData() {
        
    }
    auto OpenData::LoadImage(IScenePort* scenePort, IPanelPort* panelPort, IImageReaderPort* ireader, const QString& imgPath, const int& time_step) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        auto image = ireader->Read(imgPath, true,time_step);        
        if(nullptr ==image) {
            return false;
        }
        ws->Reset();
        panelPort->Reset();
        scenePort->Reset();

        ws->SetTimeStep(time_step);
        ws->SetImage(image);

        panelPort->Update();
        scenePort->Update();        
        return true;
    }
    auto OpenData::ResetMask(IScenePort* scenePort) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        auto image = ws->GetImage();
        if(nullptr == image) {
            return false;
        }
        auto mask = ws->GetMask();
        if(nullptr == mask) {
            return true; //No need to reset
        }
        ws->SetMask(nullptr);
        ws->SetCurrentMaskIdx(-1);
                
        scenePort->Update();        
    }
    auto OpenData::LoadMskFile(IScenePort* scenePort, IPanelPort* panelPort, IMaskReaderPort* mreader, const QString& mskPath) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        auto image = ws->GetImage();
        auto time_step = ws->GetTimeStep();
        if(nullptr == image) {
            return false;
        }
        if(mskPath.isEmpty()) {
            return false;
        }
        QFileInfo fileInfo(mskPath);
        if(false == fileInfo.exists()) {
            return false;
        }                

        auto maskMetaList = mreader->GetMaskList(mskPath,"HT");

        for(auto i=0;i<maskMetaList.count();i++) {
            std::cout << maskMetaList[i].name.toStdString() << std::endl;
        }


        if(maskMetaList.count()<1) {
            return false;
        }                

        auto organName = maskMetaList[0].name;

        auto mask = mreader->Read(mskPath, "HT", organName, time_step);

        if(nullptr == mask) {
            return false;
        }
        int imageSize[3]{ std::get<0>(image->GetSize()),std::get<1>(image->GetSize()) ,std::get<2>(image->GetSize()) };
        int maskSize[3]{ std::get<0>(mask->GetSize()),std::get<1>(mask->GetSize()) ,std::get<2>(mask->GetSize()) };

        for (auto i = 0; i < 3; i++) {
            if (imageSize[i] != maskSize[i]) {
                return false;
            }
        }

        auto AreSame = [&](double a, double b) {
            return fabs(a - b) < 0.0001;
        };

        double imageRes[3];
        image->GetResolution(imageRes);

        double maskRes[3]{ std::get<0>(mask->GetResolution()),std::get<1>(mask->GetResolution()) ,std::get<2>(mask->GetResolution()) };

        for (auto i = 0; i < 3; i++) {
            if (false == AreSame(imageRes[i], maskRes[i])) {
                return false;
            }
        }

        ws->ClearMaskList();

        for (auto i = 0; i < maskMetaList.count(); i++) {
            auto m = maskMetaList[i];
            ws->AppendMaskMeta(m);
        }

        ws->SetCurrentMaskIdx(0);

        mask->SetName(organName);
        ws->SetMaskPath(mskPath);
        ws->SetMask(mask);

        panelPort->Update();
        scenePort->Update();

        return true;
    }

    auto OpenData::LoadMask(IScenePort* scenePort, IPanelPort* panelPort, IMaskReaderPort* mreader,const QString&mskPath,const QString& organName) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();

        auto time_step = ws->GetTimeStep();

        auto image = ws->GetImage();
        if(nullptr == image) {
            return false;
        }
                
        if(mskPath.isEmpty()) {
            return false;
        }
        auto mask = mreader->Read(mskPath, "HT", organName, time_step);
        if(nullptr == mask) {            
            return false;
        }

        int imageSize[3]{ std::get<0>(image->GetSize()),std::get<1>(image->GetSize()) ,std::get<2>(image->GetSize()) };
        int maskSize[3]{ std::get<0>(mask->GetSize()),std::get<1>(mask->GetSize()) ,std::get<2>(mask->GetSize()) };

        for (auto i = 0; i < 3; i++) {
            if (imageSize[i] != maskSize[i]) {
                std::cout << "size mismatch " << imageSize[i] << " " << maskSize[i] << std::endl;
                return false;
            }
        }

        auto AreSame = [&](double a, double b) {
            return fabs(a - b) < 0.0001;
        };

        double imageRes[3];
        image->GetResolution(imageRes);

        double maskRes[3]{std::get<0>(mask->GetResolution()),std::get<1>(mask->GetResolution()) ,std::get<2>(mask->GetResolution()) };

        for(auto i=0;i<3;i++) {
            if(false == AreSame(imageRes[i], maskRes[i])) {
                std::cout << "res mismatch " << imageRes[i] << " " << maskRes[i] << std::endl;
                return false;
            }
        }

        mask->SetName(organName);
        ws->SetMaskPath(mskPath);
        ws->SetMask(mask);
        MaskMeta meta;
        meta.name = organName;
        /*if (mask->GetType()._to_integral() == MaskTypeEnum::MultiLabel) {
            meta.type = 0;
        }else {
            meta.type = 1;
        }*/
        if(mask->GetTypeText() == "Label") {
            meta.type = 0;
        }else {
            meta.type = 1;
        }
        ws->AppendMaskMeta(meta);
        auto idx = ws->GetMaskIdx(organName);
        if(idx < 0) {
            return false;
        }
        ws->SetCurrentMaskIdx(idx);

        panelPort->Update();
        scenePort->Update();        
        return true;
    }
    auto OpenData::CreateMask(IScenePort* scenePort, IPanelPort* panelPort,IMaskReaderPort* mreader, const QString& maskName, int maskType) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        if(nullptr == ws->GetImage()) {
            return false;
        }
        MaskMeta meta;
        meta.name = maskName;        
        meta.type = maskType;
        if(false == ws->AppendMaskMeta(meta)) {
            return false;
        }

        ws->SetCurrentMaskIdx(ws->GetMaskIdx(maskName));

        auto mask = mreader->Create(ws->GetImage(), maskName, maskType);                
        mask->SetName(maskName);
        if(maskType == 0) {
            mask->SetTypeText("Label");
        }else {
            mask->SetTypeText("Binary");
        }
        ws->SetMask(mask);

        panelPort->Update();
        scenePort->Update();
        return true;
    }
    auto OpenData::CreateDummyMask(IScenePort* scenePort, IPanelPort* panelPort, IMaskReaderPort* mreader, const QString& maskName, int maskType) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        if(nullptr == ws->GetImage()) {
            return false;
        }
        ws->SetCurrentMaskIdx(ws->GetMaskIdx(maskName));

        auto mask = mreader->Create(ws->GetImage(), maskName, maskType);
        mask->SetName(maskName);
        ws->SetMask(mask);

        panelPort->Update();
        scenePort->Update();
        return true;
    }

}
