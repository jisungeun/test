#include "IPanelPort.h"
#include "IScenePort.h"
#include "IMaskWriterPort.h"

#include "ManageTool.h"

namespace TomoAnalysis::MaskEditor2d::UseCase {
    ManageTool::ManageTool() {
        
    }
    ManageTool::~ManageTool() {
        
    }
    auto ManageTool::ActivateSegmenationTool(Entity::ToolIdx id, IScenePort* scenePort, IPanelPort* panelPort) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        if(ws->GetMaskPath().isEmpty()) {            
            return false;
        }
        if (Entity::ToolIdx::None != ws->GetCurrentTool()._to_integral()) {
            scenePort->DeactivateTool(false);
        }
        ws->SetCurrentTool(id);
        scenePort->ActivateTool(id);
        panelPort->Update();

        return true;
    }
    auto ManageTool::FinishSegmentationTool(IScenePort* scenePort, IPanelPort* panelPort) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        if (ws->GetMaskPath().isEmpty()) {            
            return false;
        }
        if(Entity::ToolIdx::MERGE_LABEL == ws->GetCurrentTool()._to_integral()) {
            scenePort->DeactivateTool(true);
        }
        else if (Entity::ToolIdx::None != ws->GetCurrentTool()._to_integral()) {
            scenePort->DeactivateTool(false);
        }
        ws->SetCurrentTool(Entity::ToolIdx::None);
        panelPort->Update();

        return true;
    }
}
