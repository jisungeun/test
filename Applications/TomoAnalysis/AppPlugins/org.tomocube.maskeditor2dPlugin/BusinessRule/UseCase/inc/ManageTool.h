#pragma once

#include <memory>
#include <QString>

#include <ME2Tool.h>

#include "MaskEditor2dUseCaseExport.h"

namespace TomoAnalysis::MaskEditor2d::UseCase {
    class IScenePort;
    class IPanelPort;
    class IMaskWriterPort;
    class MaskEditor2dUseCase_API ManageTool {
    public:
        ManageTool();
        virtual ~ManageTool();

        auto ActivateSegmenationTool(Entity::ToolIdx id, IScenePort* scenePort, IPanelPort* panelPort)->bool;
        auto FinishSegmentationTool(IScenePort* scenePort, IPanelPort* panelPort)->bool;
    };
}