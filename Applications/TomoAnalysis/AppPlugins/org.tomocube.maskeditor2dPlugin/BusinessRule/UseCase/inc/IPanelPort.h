#pragma once

#include "MaskEditor2dUseCaseExport.h"

namespace TomoAnalysis::MaskEditor2d::UseCase {
    class MaskEditor2dUseCase_API IPanelPort {
    public:
        IPanelPort();
        virtual ~IPanelPort();

        virtual auto Update()->void = 0;
        virtual auto Reset()->void = 0;
    };
}