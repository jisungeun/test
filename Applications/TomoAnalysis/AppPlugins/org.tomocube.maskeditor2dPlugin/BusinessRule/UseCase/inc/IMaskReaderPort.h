#pragma once

#include <ME2WorkingSet.h>
#include <TCMask.h>
#include <TCImage.h>

#include "MaskEditor2dUseCaseExport.h"

namespace TomoAnalysis::MaskEditor2d::UseCase {
    class MaskEditor2dUseCase_API IMaskReaderPort {
    public:
        IMaskReaderPort();
        virtual ~IMaskReaderPort();

        virtual auto Read(const QString& path,const QString& modalityName,const QString& organName, int time_idx) const->TCMask::Pointer = 0;
        virtual auto GetMaskList(const QString& path,const QString& modalityName)->QList<MaskMeta> = 0;
        virtual auto Create(TCImage::Pointer image,QString mask_name,int type)->TCMask::Pointer = 0;
    };
}