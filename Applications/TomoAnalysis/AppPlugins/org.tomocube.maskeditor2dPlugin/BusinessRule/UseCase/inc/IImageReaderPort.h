#pragma once

#include <QString>
#include <TCImage.h>
#include <TCFMetaReader.h>

#include "MaskEditor2dUseCaseExport.h"

namespace TomoAnalysis::MaskEditor2d::UseCase {
    class MaskEditor2dUseCase_API IImageReaderPort {
    public:
        IImageReaderPort();
        virtual ~IImageReaderPort();

        virtual auto Read(const QString& path, const bool loadImage = false, int time_step = 0) const->TCImage::Pointer = 0;
        virtual auto ReadFL(const QString& path, int ch, int time_step = 0) const->TCImage::Pointer = 0;
        virtual auto ReadMeta(const QString& path)const->TC::IO::TCFMetaReader::Meta::Pointer = 0;
    };
}