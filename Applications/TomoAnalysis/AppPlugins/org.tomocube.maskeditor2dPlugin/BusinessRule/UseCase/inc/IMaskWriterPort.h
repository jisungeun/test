#pragma once

#include <QString>
#include <TCMask.h>

#include "MaskEditor2dUseCaseExport.h"

namespace TomoAnalysis::MaskEditor2d::UseCase {
    class MaskEditor2dUseCase_API IMaskWriterPort {
    public:
        IMaskWriterPort();
        virtual ~IMaskWriterPort();
                
        virtual auto Write(TCMask::Pointer data, const QString& path,const QString& organName,int nameIdx)->bool = 0;
        virtual auto Modify(TCMask::Pointer data, const QString& path,const QString& organName,int nameIdx)->bool = 0;
    };
}
