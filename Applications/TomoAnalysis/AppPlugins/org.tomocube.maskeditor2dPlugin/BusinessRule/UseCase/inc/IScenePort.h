#pragma once

#include <ME2Tool.h>
#include <ME2WorkingSet.h>
#include <QString>

#include "MaskEditor2dUseCaseExport.h"

namespace TomoAnalysis::MaskEditor2d::UseCase {
    class MaskEditor2dUseCase_API IScenePort {
    public:
        IScenePort();
        virtual ~IScenePort();

        virtual auto ActivateTool(Entity::ToolIdx id)->void = 0;
        virtual auto DeactivateTool(bool isFunc)->void = 0;
        virtual auto Update()->void = 0;        
        virtual auto Reset()->void = 0;
        virtual auto SaveCurrent()->bool = 0;        
    };
}