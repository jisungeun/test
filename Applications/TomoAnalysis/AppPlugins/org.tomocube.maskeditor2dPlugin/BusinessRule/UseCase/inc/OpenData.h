#pragma once

#include <memory>
#include <QString>

#include "MaskEditor2dUseCaseExport.h"

namespace TomoAnalysis::MaskEditor2d::UseCase {
    class IScenePort;
    class IPanelPort;
    class IImageReaderPort;
    class IMaskReaderPort;
    class MaskEditor2dUseCase_API OpenData {
    public:
        OpenData();
        virtual ~OpenData();

        auto LoadImage(IScenePort* scenePort,IPanelPort* panelPort, IImageReaderPort* ireader, const QString& imgPath,const int& time_step)->bool;
        auto LoadMask(IScenePort* scenePort, IPanelPort* panelPort,IMaskReaderPort* mreader,const QString&mskPath,const QString&organName)->bool;
        auto LoadMskFile(IScenePort* scenePort, IPanelPort* panelPort, IMaskReaderPort* mreader, const QString& mskPath)->bool;
        auto ResetMask(IScenePort* scenePort)->bool;
        auto CreateMask(IScenePort* scenePort, IPanelPort* panelPort,IMaskReaderPort* mreader,const QString& maskName,int maskType)->bool;
        auto CreateDummyMask(IScenePort* scenePort, IPanelPort* panelPort, IMaskReaderPort* mreader, const QString& maskName, int maskType)->bool;
    };
}