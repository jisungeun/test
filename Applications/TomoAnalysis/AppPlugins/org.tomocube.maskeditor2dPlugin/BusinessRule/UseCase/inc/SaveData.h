#pragma once

#include <QString>

#include "MaskEditor2dUseCaseExport.h"

namespace TomoAnalysis::MaskEditor2d::UseCase {
    class IScenePort;
    class IPanelPort;
    class IMaskWriterPort;
    class MaskEditor2dUseCase_API SaveData {
    public:
        SaveData();
        ~SaveData();

        auto SaveMask(IScenePort*scenePort,IPanelPort* panelPort, IMaskWriterPort* mwriter, const QString& organName)->bool;
        auto ChangeMaskPath(IPanelPort* panelPort,const QString& path)->bool;
        auto CopyMask(IScenePort* scenePort)->bool;
    };
}