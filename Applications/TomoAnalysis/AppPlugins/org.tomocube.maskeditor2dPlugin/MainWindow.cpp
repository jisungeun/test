#include <iostream>

#include <QMessageBox>

//Interactor
#include <ImageDataController.h>
#include <ToolController.h>

#include <ScenePresenter.h>
#include <PanelPresenter.h>

//UseCase
#include <OpenData.h>

//Plugins
#include <ME2SceneManager.h>
#include <ME2ImageDataReader.h>
#include <ME2MaskDataReader.h>
#include <ME2MaskDataWriter.h>

#include "ui_MainWindow.h"
#include "MainWindow.h"

namespace TomoAnalysis::MaskEditor2d::AppUI {
    using namespace TC::Framework;
    struct MainWindow::Impl {
        QVariantMap appProperties;
        Ui::MainWindow* ui{ nullptr };
        Plugins::SceneManager* manager{ nullptr };
    };
    MainWindow::MainWindow(QWidget* parent) : IMainWindowTA("2D Mask Editor", "2D Mask Editor", parent), d{ new Impl } {
        d->appProperties["Parent-App"] = "StandAlone";
        d->appProperties["AppKey"] = "2D Mask Editor";
    }
    MainWindow::~MainWindow() {
        delete d->manager;
    }
    auto MainWindow::GetRunType() -> std::tuple<bool, bool> {
        auto isSingleRun = true;
        auto isBatchRun = false;
        return std::make_tuple(isSingleRun, isBatchRun);
    }
    auto MainWindow::GetMetaInfo() -> QVariantMap {
        return d->appProperties;
    }
    void MainWindow::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
    }    
    auto MainWindow::Execute(const QVariantMap& params)->bool {
        if (params.isEmpty()) {
            return true;
        }
        if (false == params.contains("ExecutionType")) {
            return false;
        }
        if (params["ExecutionType"] == "OpenTCF") {
            return this->ExecuteOpenTcf(params);
        }
        return true;
    }
    auto MainWindow::ExecuteOpenTcf(const QVariantMap& params)->bool {
        auto real_path = params["TCFPath"].toString();
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, d->ui->FLControlPanel, d->ui->GeneralPanel, d->ui->HistoryPanel, d->ui->IDManagerPanel, d->ui->LabelControlPanel, d->ui->MaskControlPanel, d->ui->SemiAutoPanel, d->ui->maskPathPanel));
        const std::shared_ptr<Plugins::ImageDataIO::ImageDataReader> imageReader(new Plugins::ImageDataIO::ImageDataReader);

        Interactor::ImageDataController imageDataController(spresenter.get(), ppresenter.get(), imageReader.get(), nullptr, nullptr);
        if (false == imageDataController.LoadTcfImage(real_path, 0)) {
            QMessageBox::warning(nullptr, "Error", "Failed to load tcf image");
            return false;
        }
        return true;
    }   
    auto MainWindow::SetParameter(QString name, IParameter::Pointer param) -> void {

    }
    auto MainWindow::GetParameter(QString name) -> IParameter::Pointer {
        return IParameter::Pointer();
    }
    auto MainWindow::IsActivate() -> bool {
        return (nullptr != d->ui);
    }

    auto MainWindow::GetFeatureName() const -> QString {
	    return "org.tomocube.maskeditor2dPlugin";
    }

    auto MainWindow::OnAccepted() -> void {
    }

    auto MainWindow::OnRejected() -> void {
    }

    auto MainWindow::TryActivate() -> bool {
        d->ui = new Ui::MainWindow;
        d->ui->setupUi(this);

        d->manager = new Plugins::SceneManager;

        auto sceneLayout = new QVBoxLayout;
        sceneLayout->setContentsMargins(0, 0, 0, 0);
        sceneLayout->setSpacing(0);

        d->ui->renderContainer->setLayout(sceneLayout);

        d->manager->SetSceneContainer(d->ui->renderContainer);

        InitConnections();

        return true;
    }
    auto MainWindow::TryDeactivate() -> bool {
        return true;
    }
    auto MainWindow::InitConnections() -> void {
        //Mask Control
        connect(d->ui->MaskControlPanel, SIGNAL(sigLoadMask(QString,QString)), this, SLOT(OnLoadMask(QString,QString)));
        connect(d->ui->MaskControlPanel, SIGNAL(sigAddMask(QString,int)), this, SLOT(OnAddMask(QString,int)));
        connect(d->ui->MaskControlPanel, SIGNAL(sigSaveMask(QString)), this, SLOT(OnSaveMask(QString)));
        connect(d->ui->MaskControlPanel, SIGNAL(sigLoadNumpy(QString)), this, SLOT(OnCopyFromNpy(QString)));
        connect(d->ui->MaskControlPanel, SIGNAL(sigSaveNumpy(QString)), this, SLOT(OnSaveAsNpy(QString)));
        connect(d->ui->maskPathPanel, SIGNAL(sigMaskPath(QString)), this, SLOT(OnMaskPath(QString)));

        //Drawer
        connect(d->ui->DrawerPanel, SIGNAL(sigAdd(bool)), this, SLOT(OnAdd(bool)));
        connect(d->ui->DrawerPanel, SIGNAL(sigSub(bool)), this, SLOT(OnSubtract(bool)));
        connect(d->ui->DrawerPanel, SIGNAL(sigPaint(bool)), this, SLOT(OnPaint(bool)));
        connect(d->ui->DrawerPanel, SIGNAL(sigWipe(bool)), this, SLOT(OnWipe(bool)));
        connect(d->ui->DrawerPanel, SIGNAL(sigFill(bool)), this, SLOT(OnFill(bool)));
        connect(d->ui->DrawerPanel, SIGNAL(sigErase(bool)), this, SLOT(OnErase(bool)));
        connect(d->ui->DrawerPanel, SIGNAL(sigBrushSizeChanged(int)), this, SLOT(OnBrushSize(int)));

        //Label
        connect(d->ui->LabelControlPanel, SIGNAL(sigPick(bool)), this, SLOT(OnPick(bool)));
        connect(d->ui->LabelControlPanel, SIGNAL(sigLabelChange(int)), this, SLOT(OnLabelValueChanged(int)));
        connect(d->ui->LabelControlPanel, SIGNAL(sigMergeLabel(bool)), this, SLOT(OnMergeLabel(bool)));
        connect(d->ui->LabelControlPanel, SIGNAL(sigMergeSupport()), this, SLOT(OnMergeSupport()));
        connect(d->ui->LabelControlPanel, SIGNAL(sigAddLabel(int)), this, SLOT(OnAddLabel(int)));

        //Cleaner
        connect(d->ui->CleanerPanel, SIGNAL(sigClean(bool)), this, SLOT(OnClean(bool)));
        connect(d->ui->CleanerPanel, SIGNAL(sigCleanSel(bool)), this, SLOT(OnCleanSel(bool)));

        //Semi-auto
        connect(d->ui->SemiAutoPanel, SIGNAL(sigErode()), this, SLOT(OnErode()));
        connect(d->ui->SemiAutoPanel, SIGNAL(sigDilate()), this, SLOT(OnDilate()));
        connect(d->ui->SemiAutoPanel, SIGNAL(sigPickDel(bool)), this, SLOT(OnPickDel(bool)));
        connect(d->ui->SemiAutoPanel, SIGNAL(sigSizeFilter(bool)), this, SLOT(OnSizeFilter(bool)));
        connect(d->ui->SemiAutoPanel, SIGNAL(sigSizeIndex(int)), this, SLOT(OnSizeIndex(int)));

        //History
        connect(d->ui->HistoryPanel, SIGNAL(sigUndo()), this, SLOT(OnUndo()));
        connect(d->ui->HistoryPanel, SIGNAL(sigRedo()), this, SLOT(OnRedo()));
        connect(d->ui->HistoryPanel, SIGNAL(sigRefresh()), this, SLOT(OnRefresh()));

        //general
        connect(d->ui->GeneralPanel, SIGNAL(sigLoad(QString)), this, SLOT(OnOpenMask(QString)));

        //manager
        connect(d->manager, SIGNAL(sigLabel(int)), d->ui->LabelControlPanel, SLOT
        (SetLabelValue(int)));
        connect(d->manager, SIGNAL(sigStartSizeFilter(int)), d->ui->SemiAutoPanel, SLOT(SetSizeMax(int)));
    }

    //SLOTS
    void MainWindow::OnOpenMask(QString maskPath) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, d->ui->MaskControlPanel, d->ui->SemiAutoPanel, d->ui->maskPathPanel));
        const std::shared_ptr<Plugins::ImageDataIO::MaskDataReader> maskReader(new Plugins::ImageDataIO::MaskDataReader);
        Interactor::ImageDataController dataController(spresenter.get(), ppresenter.get(), nullptr, maskReader.get(), nullptr);
        if(false == dataController.UnloadMask()) {
            QMessageBox::warning(nullptr, "Error", "Failed to unload previous mask");
            return;
        }

        if (false == dataController.LoadMaskFile(maskPath)) {
            QMessageBox::warning(nullptr, "Error", "Failed to load from msk file");
            return;
        }
    }

    void MainWindow::OnLoadMask(QString maskName,QString maskPath) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, d->ui->MaskControlPanel, d->ui->SemiAutoPanel, nullptr));
        const std::shared_ptr<Plugins::ImageDataIO::MaskDataReader> maskReader(new Plugins::ImageDataIO::MaskDataReader);
        Interactor::ImageDataController dataController(spresenter.get(), ppresenter.get(), nullptr, maskReader.get(), nullptr);
        auto ws = Entity::WorkingSet::GetInstance();
        //clean previous mask
        if(false == dataController.UnloadMask()) {
            QMessageBox::warning(nullptr, "Error", "Failed to unload previous mask");
            return;
        }

        //load new mask
        if (dataController.LoadMask(maskPath,maskName)) {
            auto blobCnt = ws->GetMask()->GetBlobIndexes().count();
            auto ds = d->ui->LabelControlPanel->GetDS();
            ds->labelMin = 1;
            ds->labelMax = blobCnt;
            d->ui->LabelControlPanel->Update();
            return;            
        }
        QMessageBox::warning(nullptr, "Error", "Failed to load mask, create empty mask");
        //create empty mask instead
        const std::shared_ptr<Interactor::ScenePresenter> spresenter2(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter2(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, nullptr, d->ui->SemiAutoPanel, nullptr));
        Interactor::ImageDataController dataController2(spresenter.get(), ppresenter.get(), nullptr, maskReader.get(), nullptr);
                
        auto idx = ws->GetMaskIdx(maskName);
        if(idx < 0) {
            return;            
        }
        auto meta = ws->GetMaskList()[idx];
        if (false == dataController2.DummyMask(maskName, meta.type)) {
            QMessageBox::warning(nullptr, "Error", "Failed to create mask");            
        }
        auto blobCnt = ws->GetMask()->GetBlobIndexes().count();
        auto ds = d->ui->LabelControlPanel->GetDS();
        ds->labelMax = blobCnt;
        ds->labelMin = 1;
        ds->curLabel = 1;
        d->ui->LabelControlPanel->Update();
    }
    void MainWindow::OnMaskPath(QString path) {
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, d->ui->maskPathPanel));
        Interactor::ImageDataController dataController(nullptr, ppresenter.get(), nullptr, nullptr, nullptr);
        if(false == dataController.SetMaskPath(path)) { 
            QMessageBox::warning(nullptr, "Error", "Failed to set mask path");
        }
    }
    void MainWindow::OnCopyFromNpy(QString npyPath) {
        auto ws = Entity::WorkingSet::GetInstance();
        auto image = ws->GetImage();
        if(nullptr == image) {
            QMessageBox::warning(nullptr, "Error", "Load image first");
            return;
        }
        auto mask = ws->GetMask();
        if(nullptr == mask) {
            QMessageBox::warning(nullptr, "Error", "Load mask first");
            return;
        }
        if(mask->GetType()._to_integral()!=MaskTypeEnum::MultiLabel) {
            QMessageBox::warning(nullptr, "Error", "Current mask is not a label type");
            return;
        }
        //Non-clean architecture
        auto result = d->manager->CopyFromNumPy(npyPath);
        if(false == std::get<0>(result)) {
            QMessageBox::warning(nullptr, "Error", "Failed to load numpy mask");
            return;
        }
        
        auto labelDS = d->ui->LabelControlPanel->GetDS();
        labelDS->labelMin = 1;
        labelDS->labelMax = std::get<2>(result);        
        d->ui->LabelControlPanel->Update();
    }
    void MainWindow::OnSaveAsNpy(QString npyPath) {
        auto ws = Entity::WorkingSet::GetInstance();
        auto image = ws->GetImage();
        if (nullptr == image) {
            QMessageBox::warning(nullptr, "Error", "Load image first");
            return;
        }
        auto mask = ws->GetMask();
        if (nullptr == mask) {
            QMessageBox::warning(nullptr, "Error", "Load mask first");
            return;
        }
        if (mask->GetType()._to_integral() != MaskTypeEnum::MultiLabel) {
            QMessageBox::warning(nullptr, "Error", "Current mask is not a label type");
            return;
        }        
        //Non-clean architecture
        if(false == d->manager->SaveAsNumpy(npyPath)) {
            QMessageBox::warning(nullptr, "Error", "Failed to save numpy mask");
        }
    }

    void MainWindow::OnSaveMask(QString maskName) {    
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(nullptr, nullptr, nullptr, nullptr, d->ui->HistoryPanel, nullptr, nullptr, nullptr, nullptr,nullptr));
        const std::shared_ptr<Plugins::ImageDataIO::MaskDataWriter> maskWriter(new Plugins::ImageDataIO::MaskDataWriter);
        Interactor::ImageDataController dataController(spresenter.get(), ppresenter.get(), nullptr, nullptr, maskWriter.get());        
        if(false == dataController.SaveMask(maskName)) {
            QMessageBox::warning(nullptr, "Error", "Failed to save mask");
        }
    }        

    void MainWindow::OnAddMask(QString maskName, int maskType) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, d->ui->MaskControlPanel, d->ui->SemiAutoPanel, nullptr));
        const std::shared_ptr<Plugins::ImageDataIO::MaskDataReader> maskReader(new Plugins::ImageDataIO::MaskDataReader);
        Interactor::ImageDataController dataController(spresenter.get(), ppresenter.get(), nullptr, maskReader.get(), nullptr);
        if (false == dataController.AddMask(maskName, maskType)) {
            QMessageBox::warning(nullptr, "Error", "Failed to add new mask");
        }        
        auto ds = d->ui->LabelControlPanel->GetDS();
        ds->labelMax = 1;
        ds->labelMin = 1;
        d->ui->LabelControlPanel->Update();
    }

    void MainWindow::OnLabelValueChanged(int value) {
        auto ws = Entity::WorkingSet::GetInstance();        
        ws->SetCurLabel(value);
        d->manager->SetLabelValue(value);                
    }

    void MainWindow::OnMergeSupport() {
        d->manager->TurnOnMergeLasso();
    }
        
    void MainWindow::OnAddLabel(int curMax) {
        auto ws = Entity::WorkingSet::GetInstance();
        auto mask = ws->GetMask();
        if(nullptr == mask) {
            return;
        }
        if("Label" != mask->GetTypeText()) {
            QMessageBox::warning(nullptr, "Error", "Current mask is binary");
            return;
        }        
        if(false == d->manager->AddLabel(curMax+1)) {
            QMessageBox::warning(nullptr, "Error", "Failed to add label");
            return;
        }
        d->ui->LabelControlPanel->ForceAddLabel();
    }

    void MainWindow::OnMergeLabel(bool deactivate) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, nullptr, d->ui->SemiAutoPanel, nullptr));
        Interactor::ToolController toolController(spresenter.get(), ppresenter.get());
        if (deactivate) {
            if (false == toolController.DeactivateTool()) {
                QMessageBox::warning(nullptr, "Error", "Failed to deactivate add tool");
                return;
            }
        }
        else {
            if (false == toolController.ActivateTool(Entity::ToolIdx::MERGE_LABEL)) {
                QMessageBox::warning(nullptr, "Error", "Failed to activate add tool");
                return;
            }
        }        
    }

    void MainWindow::OnPick(bool deactivate) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, nullptr, d->ui->SemiAutoPanel, nullptr));
        Interactor::ToolController toolController(spresenter.get(), ppresenter.get());
        if(deactivate) {
            if(false == toolController.DeactivateTool()) {
                QMessageBox::warning(nullptr, "Error", "Failed to deactivate add tool");
                return;
            }
        }else {
            if (false == toolController.ActivateTool(Entity::ToolIdx::PICK)) {
                QMessageBox::warning(nullptr, "Error", "Failed to activate add tool");
                return;
            }
        }
    }

    void MainWindow::OnDilate() {
        auto ws = Entity::WorkingSet::GetInstance();
        auto mask = ws->GetMask();
        if(nullptr == mask) {
            return;
        }
        auto label = ws->GetCurLabel();        
        if (false == d->manager->PerformDilate(label)) {
            QMessageBox::warning(nullptr, "Error", QString("Failed to perform dilation"));
            return;
        }
    }

    void MainWindow::OnRedo() {
        d->manager->TryRedo();
    }

    void MainWindow::OnUndo() {
        d->manager->TryUndo();
    }

    void MainWindow::OnRefresh() {
        d->manager->TryRefresh();
    }

    void MainWindow::OnErode() {
        auto ws = Entity::WorkingSet::GetInstance();
        auto mask = ws->GetMask();
        if (nullptr == mask) {
            return;
        }
        auto label = ws->GetCurLabel();
        if (false == d->manager->PerformErode(label)) {
            QMessageBox::warning(nullptr, "Error", QString("Failed to perform erosion"));
            return;
        }
    }

    void MainWindow::OnPickDel(bool deactivate) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, d->ui->MaskControlPanel, d->ui->SemiAutoPanel, nullptr));

        Interactor::ToolController toolController(spresenter.get(), ppresenter.get());
        if (deactivate) {
            if (false == toolController.DeactivateTool()) {
                QMessageBox::warning(nullptr, "Error", "Failed to deactivate previous tool");
                return;
            }
        }
        else {
            if (false == toolController.ActivateTool(Entity::ToolIdx::PICKDELETE)) {
                QMessageBox::warning(nullptr, "Error", "Failed to activate clean tool");
                return;
            }
        }
    }

    void MainWindow::OnSizeFilter(bool deactivate) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, d->ui->MaskControlPanel, d->ui->SemiAutoPanel, nullptr));

        Interactor::ToolController toolController(spresenter.get(), ppresenter.get());
        if (deactivate) {
            if (false == toolController.DeactivateTool()) {
                QMessageBox::warning(nullptr, "Error", "Failed to deactivate clean selection tool");
                return;
            }
        }
        else {
            if (false == toolController.ActivateTool(Entity::ToolIdx::SIZEFILTER)) {
                QMessageBox::warning(nullptr, "Error", "Failed to activate size filter tool");
                return;
            }
        }
    }

    void MainWindow::OnSizeIndex(int size_idx) {
        d->manager->SetSizeIndex(size_idx);
    }

    void MainWindow::OnClean(bool deactivate) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, d->ui->MaskControlPanel, d->ui->SemiAutoPanel, nullptr));

        Interactor::ToolController toolController(spresenter.get(), ppresenter.get());
        if (deactivate) {
            if (false == toolController.DeactivateTool()) {
                QMessageBox::warning(nullptr, "Error", "Failed to deactivate clean tool");
                return;
            }
        }
        else {
            if (false == toolController.ActivateTool(Entity::ToolIdx::CLEAN)) {
                QMessageBox::warning(nullptr, "Error", "Failed to activate clean tool");
                return;
            }
        }
    }

    void MainWindow::OnCleanSel(bool deactivate) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, d->ui->MaskControlPanel, d->ui->SemiAutoPanel, nullptr));

        Interactor::ToolController toolController(spresenter.get(), ppresenter.get());
        if (deactivate) {
            if (false == toolController.DeactivateTool()) {
                QMessageBox::warning(nullptr, "Error", "Failed to deactivate clean selection tool");
                return;
            }
        }
        else {
            if (false == toolController.ActivateTool(Entity::ToolIdx::CLEAN_SEL)) {
                QMessageBox::warning(nullptr, "Error", "Failed to activate clean selection tool");
                return;
            }
        }
    }

    void MainWindow::OnAdd(bool deactivate) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, nullptr, d->ui->SemiAutoPanel,nullptr));
        Interactor::ToolController toolController(spresenter.get(), ppresenter.get());
        if(deactivate) {
            if(false == toolController.DeactivateTool()) {
                QMessageBox::warning(nullptr, "Error", "Failed to deactivate add tool");
                return;
            }
        }else {
            if(false == toolController.ActivateTool(Entity::ToolIdx::ADD)) {
                QMessageBox::warning(nullptr, "Error", "Failed to activate add tool");
                return;
            }
        }
    }
    void MainWindow::OnSubtract(bool deactivate) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, nullptr, d->ui->SemiAutoPanel,nullptr));
        Interactor::ToolController toolController(spresenter.get(), ppresenter.get());
        if (deactivate) {
            if (false == toolController.DeactivateTool()) {
                QMessageBox::warning(nullptr, "Error", "Failed to deactivate subtract tool");
                return;
            }
        }
        else {
            if (false == toolController.ActivateTool(Entity::ToolIdx::SUBTRACT)) {
                QMessageBox::warning(nullptr, "Error", "Failed to activate subtract tool");
                return;
            }
        }
    }
    void MainWindow::OnBrushSize(int size) {
        d->manager->SetPaintBrushSize(size);
        d->manager->SetWipeBrushSize(size);
    }

    void MainWindow::OnPaint(bool deactivate) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, nullptr, d->ui->SemiAutoPanel,nullptr));
        Interactor::ToolController toolController(spresenter.get(), ppresenter.get());
        if (deactivate) {
            if (false == toolController.DeactivateTool()) {
                QMessageBox::warning(nullptr, "Error", "Failed to deactivate paint tool");
                return;
            }
        }
        else {
            if (false == toolController.ActivateTool(Entity::ToolIdx::PAINT)) {
                QMessageBox::warning(nullptr, "Error", "Failed to activate paint tool");
                return;
            }
        }
    }
    void MainWindow::OnWipe(bool deactivate) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, nullptr, d->ui->SemiAutoPanel,nullptr));
        Interactor::ToolController toolController(spresenter.get(), ppresenter.get());
        if (deactivate) {
            if (false == toolController.DeactivateTool()) {
                QMessageBox::warning(nullptr, "Error", "Failed to deactivate wipe tool");
                return;
            }
        }
        else {
            if (false == toolController.ActivateTool(Entity::ToolIdx::WIPE)) {
                QMessageBox::warning(nullptr, "Error", "Failed to activate wipe tool");
                return;
            }
        }
    }
    void MainWindow::OnFill(bool deactivate) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, nullptr, d->ui->SemiAutoPanel,nullptr));
        Interactor::ToolController toolController(spresenter.get(), ppresenter.get());
        if (deactivate) {
            if (false == toolController.DeactivateTool()) {
                QMessageBox::warning(nullptr, "Error", "Failed to deactivate fill tool");
                return;
            }
        }
        else {
            if (false == toolController.ActivateTool(Entity::ToolIdx::Fill)) {
                QMessageBox::warning(nullptr, "Error", "Failed to activate fill tool");
                return;
            }
        }
    }
    void MainWindow::OnErase(bool deactivate) {
        const std::shared_ptr<Interactor::ScenePresenter> spresenter(new Interactor::ScenePresenter(d->manager));
        const std::shared_ptr<Interactor::PanelPresenter> ppresenter(new Interactor::PanelPresenter(d->ui->CleanerPanel, d->ui->DrawerPanel, nullptr, nullptr, d->ui->HistoryPanel, nullptr, d->ui->LabelControlPanel, nullptr, d->ui->SemiAutoPanel,nullptr));
        Interactor::ToolController toolController(spresenter.get(), ppresenter.get());
        if (deactivate) {
            if (false == toolController.DeactivateTool()) {
                QMessageBox::warning(nullptr, "Error", "Failed to deactivate erase tool");
                return;
            }
        }
        else {
            if (false == toolController.ActivateTool(Entity::ToolIdx::ERASE)) {
                QMessageBox::warning(nullptr, "Error", "Failed to activate erase tool");
                return;
            }
        }
    }

}