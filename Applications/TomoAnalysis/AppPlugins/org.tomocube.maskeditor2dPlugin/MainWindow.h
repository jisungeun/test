#pragma once

#include <IMainWindowTA.h>

#include "ILicensed.h"

namespace TomoAnalysis::MaskEditor2d::AppUI {
    class MainWindow : public IMainWindowTA, public TomoAnalysis::License::ILicensed {
        Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = nullptr);
        virtual ~MainWindow();

        auto SetParameter(QString name, IParameter::Pointer param) -> void override;
        auto GetParameter(QString name) -> IParameter::Pointer override;
        
        auto Execute(const QVariantMap& params)->bool override;
        auto GetRunType() -> std::tuple<bool, bool> override;
        auto GetMetaInfo() -> QVariantMap final;
        auto TryActivate() -> bool final;
        auto TryDeactivate() -> bool final;
        auto IsActivate() -> bool final;

        auto GetFeatureName() const->QString override;
        auto OnAccepted() -> void override;
        auto OnRejected() -> void override;

    protected:
        virtual void resizeEvent(QResizeEvent* event);

    protected slots:
        void OnLoadMask(QString,QString);
        void OnAddMask(QString,int);
        void OnSaveMask(QString);
        void OnMaskPath(QString);
        void OnCopyFromNpy(QString);
        void OnSaveAsNpy(QString);

        void OnOpenMask(QString);

        void OnPick(bool);
        void OnLabelValueChanged(int);
        void OnMergeLabel(bool);
        void OnMergeSupport();
        void OnAddLabel(int );

        void OnClean(bool);
        void OnCleanSel(bool);

        void OnErode();
        void OnDilate();
        void OnPickDel(bool);
        void OnSizeFilter(bool);
        void OnSizeIndex(int);

        void OnRedo();
        void OnUndo();
        void OnRefresh();

        void OnAdd(bool);
        void OnSubtract(bool);
        void OnPaint(bool);
        void OnWipe(bool);
        void OnFill(bool);
        void OnErase(bool);
        void OnBrushSize(int);

    private:
        auto InitConnections()->void;    
        auto ExecuteOpenTcf(const QVariantMap& params)->bool;
    
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}