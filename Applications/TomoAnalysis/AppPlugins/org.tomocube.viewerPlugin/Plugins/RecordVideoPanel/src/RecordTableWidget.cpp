#include "ui_VideoEditWidget.h"
#include "VideoEditWidget.h"

#include <QHeaderView>
#include <QPainter>
#include <QScrollArea>
#include <QTime>

#include "UIUtility.h"

namespace  TomoAnalysis::UiModule {
	struct TickMarkWidget::Impl {
		QColor backgroundColor = QColor(32, 32, 32);
		int interval = 50;
	};

	TickMarkWidget::TickMarkWidget(QWidget* parent)
		: QWidget(parent), d{ new Impl } {

		this->setFixedHeight(30);
		this->setMinimumWidth(500);
		this->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
	}

	TickMarkWidget::~TickMarkWidget() {

	}

	void TickMarkWidget::SetInterval(const int& interval) {
		d->interval = interval;
	}

	void TickMarkWidget::paintEvent(QPaintEvent* event) {
		QPainter painter(this);
		QFont font = painter.font();
		font.setPointSize(10);
		painter.setFont(font);
		painter.setRenderHints(QPainter::TextAntialiasing | QPainter::HighQualityAntialiasing);

		QRectF rect = this->rect();

		// paint background color
		painter.fillRect(QRect(rect.left(), rect.top(), this->width(), this->height()), d->backgroundColor);

		DrawTicker(&painter);
	}

	void TickMarkWidget::DrawTicker(QPainter* painter) {
		//int interval = 50;

		int text = 0;
		auto rect = this->rect();

		for (int pos = rect.left(), count = 0; pos < rect.right(); pos += d->interval, ++count) {
			int x1 = pos;
			int y1 = this->rect().top() + 10;
			int x2 = pos;
			int y2 = this->rect().bottom();

			if (0 == count % 2) {
				QPen tickerPen(QColor(61, 61, 61), 2);
				painter->setPen(tickerPen);
				painter->drawLine(QLineF(x1, y1 + 10, x2, y2));

			    // draw text
				if(pos != rect.left() && pos != rect.right()) {
					QPen textPen(QColor(121, 121, 121), 1);
					QTime time(0, count / 2, 0);
					painter->drawText(x1 - 15, y1 + 1, time.toString("mm:ss"));
				}
			}
			else {
				QPen tickerPen(QColor(61, 61, 61), 1);
				painter->setPen(tickerPen);
				painter->drawLine(QLineF(x1, y1 + 12, x2, y2));
			}
		}

		QPen tickerPen(QColor(61, 61, 61), 1);
		painter->setPen(tickerPen);

		int x1 = rect.left();
		int y1 = rect.bottom();
		int x2 = rect.right();
		int y2 = rect.bottom();

		painter->drawLine(QLineF(x1, y1, x2, y2));
    }

	//////////////////////////////////////////////////////////////////////////////////////
    struct VideoEditWidget::Impl {
		TickMarkWidget* tickMarkWidget = nullptr;
		QTableWidget* timestempWidget = nullptr;
		QTableWidget* itemTitleWidget = nullptr;

		QVector<RecordAction> actionList;

		int interval = 50;
    };

    VideoEditWidget::VideoEditWidget(QWidget* parent)
    : QWidget(parent), d{ new Impl } {
		// init ui
		// main layout
		auto layout = new QHBoxLayout;
		layout->setMargin(0);
		layout->setSpacing(0);
		this->setLayout(layout);

		// title widget
		auto titleWidget = new QWidget(this);
		auto titleLayout = new QVBoxLayout;
		titleLayout->setMargin(0);
		titleLayout->setSpacing(0);
		titleWidget->setLayout(titleLayout);

		auto emptyWidget = new QWidget(titleWidget);
		emptyWidget->setFixedHeight(30);
		emptyWidget->setFixedWidth(200);
		titleLayout->addWidget(emptyWidget);

		d->itemTitleWidget = new QTableWidget(titleWidget);
		d->itemTitleWidget->setFixedWidth(200);
		d->itemTitleWidget->setColumnCount(1);
		d->itemTitleWidget->horizontalHeader()->hide();
		d->itemTitleWidget->verticalHeader()->hide();
		titleLayout->addWidget(d->itemTitleWidget);

		// timeline widget
		auto timelineScroll = new QScrollArea(this);
		timelineScroll->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
		timelineScroll->setWidgetResizable(true);
		auto timelineWidget = new QWidget(this);
		auto timelineLayout = new QVBoxLayout;
		timelineLayout->setMargin(0);
		timelineLayout->setSpacing(0);
		timelineWidget->setLayout(timelineLayout);

		d->tickMarkWidget = new TickMarkWidget(timelineWidget);
		d->tickMarkWidget->SetInterval(d->interval);
		timelineLayout->addWidget(d->tickMarkWidget);

		d->timestempWidget = new QTableWidget(timelineWidget);
		d->timestempWidget->horizontalHeader()->hide();
		d->timestempWidget->verticalHeader()->hide();
		d->timestempWidget->setColumnCount(1);
		timelineLayout->addWidget(d->timestempWidget);

		timelineScroll->setWidget(timelineWidget);

		layout->addWidget(titleWidget);
		layout->addWidget(timelineScroll, 1);

		// temp
		RecordAction action;
		action.type = ActionType::Orbit;
        action.start = 0.f;
		action.end = 2.f;

		AddAction(action);
		UpdateAction();

    }

    VideoEditWidget::~VideoEditWidget() {
        
    }

	void VideoEditWidget::AddAction(const RecordAction& action) {
		d->actionList.push_back(action);
    }

	void VideoEditWidget::UpdateAction() {
		if(nullptr == d->itemTitleWidget) {
			return;
		}

		if (nullptr == d->timestempWidget) {
			return;
		}

		// clear ui
		while (d->itemTitleWidget->rowCount() > 0)
		{
			TC::SilentCall(d->itemTitleWidget)->removeRow(0);
		}

		while (d->timestempWidget->rowCount() > 0)
		{
			TC::SilentCall(d->timestempWidget)->removeRow(0);
		}

        for(auto action : d->actionList) {
			const auto row = d->itemTitleWidget->rowCount();

            d->itemTitleWidget->insertRow(row);
			d->timestempWidget->insertRow(row);

            auto item = new QTableWidgetItem(action.type._to_string());
			TC::SilentCall(d->itemTitleWidget)->setItem(row, 0, item);
			auto width = action.end - action.start;

			QLabel* lbl = new QLabel;
			lbl->setAutoFillBackground(true);
			lbl->setFixedWidth(d->interval * width * 2);

			QPalette palette = lbl->palette();
			palette.setColor(lbl->backgroundRole(), Qt::red);
			lbl->setPalette(palette);

			d->timestempWidget->setCellWidget(row, 0, lbl);
			d->timestempWidget->resizeColumnToContents(0);
        }
    }
}
