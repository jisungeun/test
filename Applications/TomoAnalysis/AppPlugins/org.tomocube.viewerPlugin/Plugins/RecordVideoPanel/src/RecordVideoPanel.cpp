#include "ui_RecordVideoPanel.h"
#include "RecordVideoPanel.h"

#include <iostream>
#include <QMenu>
#include <QFileDialog>
#include <qfuture.h>
#include <QMessageBox>
#include <QSettings>
#include <QStandardPaths>
#include <QtConcurrent/qtconcurrentrun.h>

#include "UIUtility.h"
#include "TCVideoWriter.h"

namespace  TomoAnalysis::Viewer::Plugins {
    struct RecordVideoPanel::Impl {
        Ui::RecordVideoPanel* ui{ nullptr };
        QMenu* addMenu;
        QMenu* recordMenu;

        float interval = 40.f;

        Interactor::RecordVideoDS::Pointer ds;
        Entity::Modality prevModality = Entity::Modality::None;

        RecordActionItem* currentItem = nullptr;
        int currentSliceWidth = -1;
        int currentSliceHeight = -1;
        int currentSliceDepth = -1;

        int currentSliceMin = 0;
        int currentSliceMax = -1;

        int currentTimeMax = -1;

        QList<RecordAction*> process;

        bool playing = false;
        bool force2D = false;

        const QList<ActionType> actionList = { ActionType::Orbit ,
                                               ActionType::Slice,
                                               ActionType::Timelapse };

        const QStringList recordTypeList = { "XY Plane",
                                             "YZ Plane",
                                             "XZ Plane",
                                             "3D",
                                             "Multi-view" };
    };

    RecordVideoPanel::RecordVideoPanel(QWidget* parent)
        : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::RecordVideoPanel();
        d->ui->setupUi(this);

        // set object names
        d->ui->contentsWidget->setObjectName("panel-contents");

        d->ui->addButton->setObjectName("bt-round-gray700");
        d->ui->deleteButton->setObjectName("bt-round-gray700");

        d->ui->orbitPageTitleLabel->setObjectName("h8");
        d->ui->angleLabel->setObjectName("h9");
        d->ui->orbitLabel->setObjectName("h9");

        d->ui->slicePageTitleLabel->setObjectName("h8");
        d->ui->timeRangeLabel->setObjectName("h9");
        d->ui->sliceWindowLabel->setObjectName("h9");

        d->ui->timePageTitleLabel->setObjectName("h8");
        d->ui->timeRangeLabel->setObjectName("h9");

        d->ui->recordButton->setObjectName("bt-square-gray700");
        d->ui->playButton->setObjectName("bt-square-gray700");

        d->ui->line->setObjectName("line-separator-section");
        d->ui->line_2->setObjectName("line-separator-section");
        d->ui->line_3->setObjectName("line-separator-section");

        // set icons
        d->ui->recordButton->AddIcon(":/img/ic-record.svg");
        d->ui->recordButton->AddIcon(":/img/ic-record-d.svg", QIcon::Disabled);
        d->ui->recordButton->AddIcon(":/img/ic-record-s.svg", QIcon::Active);
        d->ui->recordButton->AddIcon(":/img/ic-record-s.svg", QIcon::Selected);

        SetPlayingState(false);
    }

    RecordVideoPanel::~RecordVideoPanel() {
        delete d->ui;
    }

    auto RecordVideoPanel::Update()->bool {
        d->ds = GetRecordVideoDS();
        const auto path = QString::fromUtf8(d->ds->tcfName.c_str());
        const QFileInfo fileInfo(path);
        d->ds->tcfName = fileInfo.completeBaseName().toStdString();

        const bool enable = true;
        d->ui->addButton->setEnabled(enable);
        d->ui->deleteButton->setEnabled(enable);
        d->ui->playButton->setEnabled(enable);
        d->ui->recordButton->setEnabled(enable);                

        return true;
    }
    auto RecordVideoPanel::Reset() const -> bool {
        /*for(auto action : d->addMenu->actions()) {
            d->addMenu->removeAction(action);
            action = nullptr;
        }
        d->addMenu = nullptr;
        for(auto action: d->recordMenu->actions()) {
            d->recordMenu->removeAction(action);
            action = nullptr;            
        }
        d->recordMenu = nullptr;*/
        return true;
    }
    auto RecordVideoPanel::Init(void) const ->bool {
        // add menu
        d->ui->addButton->setContextMenuPolicy(Qt::CustomContextMenu);

        d->addMenu = new QMenu;

        for (auto i = 0; i < 3; ++i) {
            const auto action = new QAction(d->actionList.at(i)._to_string());
            connect(action, SIGNAL(triggered(bool)), this, SLOT(onAddAction(bool)));
            d->addMenu->addAction(action);
        }

        // add record menu
        d->ui->recordButton->setContextMenuPolicy(Qt::CustomContextMenu);

        d->recordMenu = new QMenu;

        for (auto i = 0; i < 5; ++i) {
            const auto action = new QAction(d->recordTypeList.at(i));
            connect(action, SIGNAL(triggered(bool)), this, SLOT(onRecord(bool)));
            d->recordMenu->addAction(action);
        }

        connect(d->ui->editWidget, SIGNAL(selectItem(RecordActionItem*)), this, SLOT(onSelectedActionItem(RecordActionItem*)));

        // time stamp
        d->ui->editWidget->SetInterval(d->interval);

        // sub ui
        //d->ui->orbitGroupBox->hide();
        //d->ui->sliceGroupBox->hide();
        //d->ui->timeGroupBox->hide();

        d->ui->stackedWidget->hide();

        TC::SilentCall(d->ui->angleSlider)->setRange(0, 360);
        TC::SilentCall(d->ui->angleSpinBox)->setRange(0, 360);
        TC::SilentCall(d->ui->angleSlider)->setValue(180);
        TC::SilentCall(d->ui->angleSpinBox)->setValue(180);
        TC::SilentCall(d->ui->orbitLineEdit)->setText("360");

        TC::SilentCall(d->ui->sliceWindowComboBox)->addItem("XY Plane");
        TC::SilentCall(d->ui->sliceWindowComboBox)->addItem("YZ Plane");
        TC::SilentCall(d->ui->sliceWindowComboBox)->addItem("XZ Plane");
        TC::SilentCall(d->ui->sliceWindowComboBox)->setCurrentIndex(0);

        const bool enable = true;
        d->ui->addButton->setEnabled(enable);
        d->ui->deleteButton->setEnabled(enable);
        d->ui->playButton->setEnabled(enable);
        d->ui->recordButton->setEnabled(enable);

        return true;
    }

    auto RecordVideoPanel::force2D() -> void {
        if (!d->force2D) {            
            d->force2D = true;
            d->addMenu->clear();
            const auto addAction = new QAction("Timelapse");
            connect(addAction, SIGNAL(triggered(bool)), this, SLOT(onAddAction(bool)));
            d->addMenu->addAction(addAction);

            d->recordMenu->clear();
            const auto recordAction = new QAction("XY Plane");
            connect(recordAction, SIGNAL(triggered(bool)), this, SLOT(onRecord(bool)));
            d->recordMenu->addAction(recordAction);
        }
    }

    auto RecordVideoPanel::restore3D() -> void {
        if (d->force2D) {            
            d->force2D = false;
            d->addMenu->clear();
            for (auto i = 0; i < 3; ++i) {
                const auto action = new QAction(d->actionList.at(i)._to_string());
                connect(action, SIGNAL(triggered(bool)), this, SLOT(onAddAction(bool)));
                d->addMenu->addAction(action);
            }
            d->recordMenu->clear();
            for (auto i = 0; i < 5; ++i) {
                const auto action = new QAction(d->recordTypeList.at(i));
                connect(action, SIGNAL(triggered(bool)), this, SLOT(onRecord(bool)));
                d->recordMenu->addAction(action);
            }
        }
    }

    void RecordVideoPanel::on_addButton_clicked() const {
        QPoint pos = this->mapToGlobal(d->ui->addButton->pos());
        pos.setY(pos.y() + d->ui->addButton->height());
        d->addMenu->exec(pos);
    }

    void RecordVideoPanel::on_deleteButton_clicked() const {
        d->ui->editWidget->DeleteSelectedAction();
    }

    void RecordVideoPanel::on_playButton_clicked() {
        if (false == d->playing) {
            MakeUpProcess();

            SetPlayingState(true);
            emit playVideo(d->process);
        }
        else {
            SetPlayingState(false);
            emit stopVideo();
        }
    }

    void RecordVideoPanel::on_recordButton_clicked() const {        
        //QPoint pos = this->mapToGlobal(d->ui->recordButton->pos());
        QPoint pos = QCursor::pos();
        //pos.setY(pos.y() + d->ui->recordButton->height());        
        d->recordMenu->exec(pos);
    }

    void RecordVideoPanel::on_angleSlider_valueChanged(int value) const {
        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->startAngle = value;

        TC::SilentCall(d->ui->angleSpinBox)->setValue(value);
    }

    void RecordVideoPanel::on_angleSpinBox_valueChanged(int value) const {
        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->startAngle = value;

        TC::SilentCall(d->ui->angleSlider)->setValue(value);
    }

    void RecordVideoPanel::on_orbitLineEdit_textChanged(const QString& text) const {
        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->orbit = text.toInt();
    }

    void RecordVideoPanel::on_orbitCheckBox_toggled(bool checked) const {
        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->reverse = checked;
    }

    void RecordVideoPanel::on_xRadioButton_toggled(bool checked) const {
        if (false == checked) {
            return;
        }

        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->axis = ActionAxis::X;
    }

    void RecordVideoPanel::on_yRadioButton_toggled(bool checked) const {
        if (false == checked) {
            return;
        }

        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->axis = ActionAxis::Y;
    }

    void RecordVideoPanel::on_zRadioButton_toggled(bool checked) const {
        if (false == checked) {
            return;
        }

        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->axis = ActionAxis::Z;
    }

    void RecordVideoPanel::on_tRadioButton_toggled(bool checked) const {
        if (false == checked) {
            return;
        }

        if (ActionType::Orbit != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetOrbitInfo()->axis = ActionAxis::T;
    }

    void RecordVideoPanel::on_sliceMinSpinBox_valueChanged(int value) const {
        if (ActionType::Slice != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetSliceInfo()->min = value;
    }

    void RecordVideoPanel::on_sliceMaxSpinBox_valueChanged(int value) const {
        if (ActionType::Slice != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetSliceInfo()->max = value;
    }

    void RecordVideoPanel::on_sliceCheckBox_toggled(bool checked) const {
        if (ActionType::Slice != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetSliceInfo()->reverse = checked;
    }

    void RecordVideoPanel::on_sliceWindowComboBox_currentIndexChanged(int index) const {
        d->currentItem->GetSliceInfo()->windowType = ActionWindow::_from_integral(index);

        d->currentSliceMax = -1;
        switch (index) {
        case 0: // XY
            d->currentSliceMax = d->currentSliceDepth;
            break;
        case 1: // YZ
            d->currentSliceMax = d->currentSliceHeight;
            break;
        case 2: // XZ
            d->currentSliceMax = d->currentSliceWidth;
            break;
        default:;
        }

        d->ui->sliceMinSpinBox->setRange(0, d->currentSliceMax);
        d->ui->sliceMinSpinBox->setValue(0);

        d->ui->sliceMaxSpinBox->setRange(0, d->currentSliceMax);
        d->ui->sliceMaxSpinBox->setValue(d->currentSliceMax);
    }

    void RecordVideoPanel::on_timeMinSpinBox_valueChanged(int value) const {
        if (ActionType::Timelapse != d->currentItem->GetType()._to_integral()) {
            return;
        }
        //d->currentItem->GetTimeInfo()->min = value;
        d->currentItem->GetTimeInfo()->min = value - 1;
    }

    void RecordVideoPanel::on_timeMaxSpinBox_valueChanged(int value) const {
        if (ActionType::Timelapse != d->currentItem->GetType()._to_integral()) {
            return;
        }
        //d->currentItem->GetTimeInfo()->max = value;
        d->currentItem->GetTimeInfo()->max = value - 1;
    }

    void RecordVideoPanel::on_timeCheckBox_toggled(bool checked) const {
        if (ActionType::Timelapse != d->currentItem->GetType()._to_integral()) {
            return;
        }

        d->currentItem->GetTimeInfo()->reverse = checked;
    }

    void RecordVideoPanel::onAddAction(bool) const {
        const auto action = qobject_cast<QAction*>(sender());
        if (nullptr == action) {
            return;
        }

        auto item = new RecordActionItem(nullptr);
        item->SetType(ActionType::_from_string(action->text().toStdString().c_str()));
        item->SetPosition(0, d->interval * 2.f);

        if (action->text() == d->actionList.at(0)._to_string()) { // orbit
            item->SetCheckOverlap(ActionType::Orbit);
            item->GetOrbitInfo()->axis = ActionAxis::X;
        }
        else if (action->text() == d->actionList.at(1)._to_string()) { // slice
            item->GetSliceInfo()->min = d->currentSliceMin;
            item->GetSliceInfo()->max = d->currentSliceMax;
            item->GetSliceInfo()->windowType = ActionWindow::XY;
        }
        else if (action->text() == d->actionList.at(2)._to_string()) { // timelapse
            //item->GetTimeInfo()->max = d->currentTimeMax;
            item->GetTimeInfo()->max = d->currentTimeMax - 1;
        }
        else {
            // error
            delete item;
            item = nullptr;
        }

        d->ui->editWidget->AddAction(item);        
    }

    void RecordVideoPanel::onRecord(bool) {
        const auto action = qobject_cast<QAction*>(sender());
        if (nullptr == action) {
            return;
        }

        auto text = action->text();
        const auto index = d->recordTypeList.indexOf(QRegExp(text, Qt::CaseInsensitive, QRegExp::RegExp2), 0);
        if (-1 == index) {
            return;
        }

        //const QString dir = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
        const QString name = QString::fromStdString(d->ds->tcfName) + "_" + text.replace(" ", "_") + ".mp4";// ".avi";

        const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recentRecord").toString();
        const auto savePath = QFileDialog::getSaveFileName(nullptr, tr("Save video file"),
            prev + "/" + name, tr("Movie (*.mp4)"));//(*.avi)"));

        QSettings("Tomocube", "TomoAnalysis").setValue("recentRecord", savePath);

        if (true == savePath.isEmpty()) {
            return;
        }

        for (auto i = 0; i < savePath.length(); i++) {
            if (savePath.at(i).unicode() > 127) {
                QMessageBox::warning(nullptr, "Error", QString("Target movie path contains non-english characters"));
                return;
            }
        }

        if (false == MakeUpProcess()) {
            return;
        }

        emit startRecord(savePath, RecordType::_from_integral(index), d->process);
    }

    void RecordVideoPanel::onCurrentModalityChanged(Entity::Modality modality) const {
        if ((d->prevModality & Entity::Modality::None) != Entity::Modality::None) {
            return;
        }

        Interactor::RecordVideoInfo::Pointer info;

        if (((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
            (modality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume)) {
            // volume
            if ((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
                info = d->ds->list[Entity::HT3D];
            }
            else {
                info = d->ds->list[Entity::FL3D];
            }

        }
        else if (((modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP ||
            (modality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP)) {
            // mip
            if ((modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
                info = d->ds->list[Entity::HT2DMIP];
            }
            else {
                info = d->ds->list[Entity::FL2DMIP];
            }
        }

        if (nullptr == info.get()) {
            return;
        }

        d->currentSliceWidth = info->width;
        d->currentSliceHeight = info->height;
        d->currentSliceDepth = info->depth;
        d->currentSliceMax = d->currentSliceDepth; // default XY Plane
        //d->currentTimeMax = info->count - 1;
        d->currentTimeMax = info->count;

        TC::SilentCall(d->ui->sliceMinSpinBox)->setRange(0, d->currentSliceMax);
        TC::SilentCall(d->ui->sliceMaxSpinBox)->setRange(0, d->currentSliceMax);
        TC::SilentCall(d->ui->sliceMaxSpinBox)->setValue(d->currentSliceMax);

        //TC::SilentCall(d->ui->timeMinSpinBox)->setRange(0, d->currentTimeMax);
        TC::SilentCall(d->ui->timeMinSpinBox)->setRange(1, d->currentTimeMax);
        //TC::SilentCall(d->ui->timeMaxSpinBox)->setRange(0, d->currentTimeMax);
        TC::SilentCall(d->ui->timeMaxSpinBox)->setRange(1, d->currentTimeMax);
        TC::SilentCall(d->ui->timeMaxSpinBox)->setValue(d->currentTimeMax);

        if(nullptr != d->currentItem) {
            forceSelectItem(d->currentItem);
        }
    }
    auto RecordVideoPanel::forceSelectItem(RecordActionItem* item) const -> void {
        if (nullptr == item) {
            return;
        }

        auto type = item->GetType();
        switch (type) {
        case ActionType::Orbit: {
            //d->ui->orbitGroupBox->show();

            const auto info = d->currentItem->GetOrbitInfo();
            TC::SilentCall(d->ui->angleSlider)->setValue(info->startAngle);
            TC::SilentCall(d->ui->angleSpinBox)->setValue(info->startAngle);
            TC::SilentCall(d->ui->orbitLineEdit)->setText(QString::number(info->orbit));
            TC::SilentCall(d->ui->orbitCheckBox)->setChecked(info->reverse);

            if (+ActionAxis::X == info->axis) {
                TC::SilentCall(d->ui->xRadioButton)->setChecked(true);
            }
            else if (+ActionAxis::Y == info->axis) {
                TC::SilentCall(d->ui->yRadioButton)->setChecked(true);
            }
            else if (+ActionAxis::Z == info->axis) {
                TC::SilentCall(d->ui->zRadioButton)->setChecked(true);
            }
            else if (+ActionAxis::T == info->axis) {
                TC::SilentCall(d->ui->tRadioButton)->setChecked(true);
            }

            if (d->ui->stackedWidget->isHidden()) {
                d->ui->stackedWidget->show();
            }
            d->ui->stackedWidget->setCurrentIndex(0);

            break;
        }

        case ActionType::Slice: {
            //d->ui->sliceGroupBox->show();

            const auto info = d->currentItem->GetSliceInfo();
            TC::SilentCall(d->ui->sliceMinSpinBox)->setValue(info->min);
            TC::SilentCall(d->ui->sliceMaxSpinBox)->setValue(info->max);
            TC::SilentCall(d->ui->sliceCheckBox)->setChecked(info->reverse);
            TC::SilentCall(d->ui->sliceWindowComboBox)->setCurrentIndex(info->windowType);

            if (d->ui->stackedWidget->isHidden()) {
                d->ui->stackedWidget->show();
            }
            d->ui->stackedWidget->setCurrentIndex(1);

            break;
        }
        case ActionType::Timelapse: {
            //d->ui->timeGroupBox->show();

            const auto info = d->currentItem->GetTimeInfo();
            //TC::SilentCall(d->ui->timeMinSpinBox)->setValue(info->min);
            TC::SilentCall(d->ui->timeMinSpinBox)->setValue(info->min + 1);
            //TC::SilentCall(d->ui->timeMaxSpinBox)->setValue(info->max);
            TC::SilentCall(d->ui->timeMaxSpinBox)->setValue(info->max + 1);
            TC::SilentCall(d->ui->timeCheckBox)->setChecked(info->reverse);

            if (d->ui->stackedWidget->isHidden()) {
                d->ui->stackedWidget->show();
            }
            d->ui->stackedWidget->setCurrentIndex(2);

            break;
        }
        case ActionType::None:
        default:
            d->ui->stackedWidget->hide();
            break;
        }
    }

    void RecordVideoPanel::onSelectedActionItem(RecordActionItem* item) const {
        if (d->currentItem == item) {
            return;
        }

        d->currentItem = item;

        //d->ui->orbitGroupBox->hide();
        //d->ui->sliceGroupBox->hide();
        //d->ui->timeGroupBox->hide();

        if (nullptr == item) {
            return;
        }

        auto type = item->GetType();
        switch (type) {
        case ActionType::Orbit: {
            //d->ui->orbitGroupBox->show();

            const auto info = d->currentItem->GetOrbitInfo();
            TC::SilentCall(d->ui->angleSlider)->setValue(info->startAngle);
            TC::SilentCall(d->ui->angleSpinBox)->setValue(info->startAngle);
            TC::SilentCall(d->ui->orbitLineEdit)->setText(QString::number(info->orbit));
            TC::SilentCall(d->ui->orbitCheckBox)->setChecked(info->reverse);

            if (+ActionAxis::X == info->axis) {
                TC::SilentCall(d->ui->xRadioButton)->setChecked(true);
            }
            else if (+ActionAxis::Y == info->axis) {
                TC::SilentCall(d->ui->yRadioButton)->setChecked(true);
            }
            else if (+ActionAxis::Z == info->axis) {
                TC::SilentCall(d->ui->zRadioButton)->setChecked(true);
            }
            else if (+ActionAxis::T == info->axis) {
                TC::SilentCall(d->ui->tRadioButton)->setChecked(true);
            }

            if (d->ui->stackedWidget->isHidden()) {
                d->ui->stackedWidget->show();
            }
            d->ui->stackedWidget->setCurrentIndex(0);

            break;
        }

        case ActionType::Slice: {
            //d->ui->sliceGroupBox->show();

            const auto info = d->currentItem->GetSliceInfo();
            TC::SilentCall(d->ui->sliceMinSpinBox)->setValue(info->min);
            TC::SilentCall(d->ui->sliceMaxSpinBox)->setValue(info->max);
            TC::SilentCall(d->ui->sliceCheckBox)->setChecked(info->reverse);
            TC::SilentCall(d->ui->sliceWindowComboBox)->setCurrentIndex(info->windowType);

            if (d->ui->stackedWidget->isHidden()) {
                d->ui->stackedWidget->show();
            }
            d->ui->stackedWidget->setCurrentIndex(1);

            break;
        }
        case ActionType::Timelapse: {
            //d->ui->timeGroupBox->show();

            const auto info = d->currentItem->GetTimeInfo();
            //TC::SilentCall(d->ui->timeMinSpinBox)->setValue(info->min);
            TC::SilentCall(d->ui->timeMinSpinBox)->setValue(info->min + 1);
            //TC::SilentCall(d->ui->timeMaxSpinBox)->setValue(info->max);
            TC::SilentCall(d->ui->timeMaxSpinBox)->setValue(info->max + 1);
            TC::SilentCall(d->ui->timeCheckBox)->setChecked(info->reverse);

            if (d->ui->stackedWidget->isHidden()) {
                d->ui->stackedWidget->show();
            }
            d->ui->stackedWidget->setCurrentIndex(2);

            break;
        }
        case ActionType::None:
        default:
            d->ui->stackedWidget->hide();
            break;
        }
    }

    void RecordVideoPanel::onFinishedVideo() {
        SetPlayingState(false);
    }

    auto RecordVideoPanel::keyPressEvent(QKeyEvent* keyPress) -> void {
        switch (keyPress->key()) {
        case Qt::Key_Escape: {
            d->ui->editWidget->ClearSelection();
            break;
        }
        default:
            break;
        }
    }

    auto RecordVideoPanel::MakeUpProcess() const ->bool {
        d->process.clear();

        auto temp = d->ui->editWidget->GetProcess();
        std::sort(temp.begin(), temp.end(),
            [](RecordActionItem* first, RecordActionItem* second) -> bool
            {
                return first->GetEndTime() < second->GetEndTime();
            });
        auto ignore = false;
        for (auto item : temp) {
            const auto type = item->GetType();
            auto action = std::make_shared<RecordAction>();
            switch (type) {
            case ActionType::Orbit: {                
                action->type = ActionType::Orbit;
                action->startTime = item->GetStartTime();
                action->endTime = item->GetEndTime();
                action->begin = item->GetOrbitInfo()->startAngle;
                action->finish = item->GetOrbitInfo()->orbit;
                action->reverse = item->GetOrbitInfo()->reverse;
                action->axis = item->GetOrbitInfo()->axis;
                if (d->force2D)
                    ignore = true;
                break;
            }
            case ActionType::Slice: {
                action->type = ActionType::Slice;
                action->startTime = item->GetStartTime();
                action->endTime = item->GetEndTime();
                action->begin = item->GetSliceInfo()->min;
                action->finish = item->GetSliceInfo()->max;
                action->reverse = item->GetSliceInfo()->reverse;
                action->windowType = item->GetSliceInfo()->windowType;
                if (d->force2D)
                    ignore = true;
                break;
            }
            case ActionType::Timelapse: {
                action->type = ActionType::Timelapse;
                action->startTime = item->GetStartTime();
                action->endTime = item->GetEndTime();
                action->begin = item->GetTimeInfo()->min;
                action->finish = item->GetTimeInfo()->max;                
                action->reverse = item->GetTimeInfo()->reverse;                
                break;
            }
            default:;
            }
            if (!ignore) {
                d->process.push_back(action.get());
            }
        }

        return true;
    }

    auto RecordVideoPanel::SetPlayingState(bool isPlaying) -> void {
        d->playing = isPlaying;

        if (isPlaying) {
            d->ui->playButton->setText("Stop");

            d->ui->playButton->AddIcon(":/img/ic-stop.svg");
            d->ui->playButton->AddIcon(":/img/ic-stop-d.svg", QIcon::Disabled);
            d->ui->playButton->AddIcon(":/img/ic-stop-s.svg", QIcon::Active);
            d->ui->playButton->AddIcon(":/img/ic-stop-s.svg", QIcon::Selected);
        }
        else {
            d->ui->playButton->setText("Play");

            d->ui->playButton->AddIcon(":/img/ic-play.svg");
            d->ui->playButton->AddIcon(":/img/ic-play-d.svg", QIcon::Disabled);
            d->ui->playButton->AddIcon(":/img/ic-play-s.svg", QIcon::Active);
            d->ui->playButton->AddIcon(":/img/ic-play-s.svg", QIcon::Selected);
        }
    }
}


