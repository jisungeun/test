#pragma once

#include <memory>
#include <QWidget>
#include <QKeyEvent>

#include <IRecordVideoPanel.h>
#include "VideoEditWidget.h"
#include "TomoAnalysisRecordVideoPanelExport.h"

namespace TomoAnalysis::Viewer::Plugins {
    BETTER_ENUM(RecordType, int,
        None = -1,
        XY_Plane = 0,
        YZ_Plane = 1,
        XZ_Plane = 2,
        _3D = 3,
        Multi_View = 4)

        struct RecordAction {
        ActionType type = ActionType::None;
        ActionAxis axis = ActionAxis::None;
        ActionWindow windowType = ActionWindow::None;

        float startTime = 0.f;
        float endTime = 0.f;

        int begin = -1;
        int finish = -1;

        bool reverse = false;
    };

    class TomoAnalysisRecordVideoPanel_API RecordVideoPanel : public QWidget, public Interactor::IRecordVideoPanel {
        Q_OBJECT

    public:
        typedef RecordVideoPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        RecordVideoPanel(QWidget* parent = nullptr);
        ~RecordVideoPanel();

        auto Update()->bool override;
        auto Init(void) const ->bool;
        auto Reset(void)const->bool;

        auto force2D()->void;
        auto restore3D()->void;

    signals:
        void playVideo(QList<Plugins::RecordAction*> process);
        void stopVideo();

        void startRecord(QString, Plugins::RecordType type, QList<Plugins::RecordAction*> process);

    protected slots:
        void on_addButton_clicked() const;
        void on_deleteButton_clicked() const;
        void on_playButton_clicked();
        void on_recordButton_clicked() const;

        void on_angleSlider_valueChanged(int value) const;
        void on_angleSpinBox_valueChanged(int value) const;
        void on_orbitLineEdit_textChanged(const QString& text) const;
        void on_orbitCheckBox_toggled(bool checked) const;
        void on_xRadioButton_toggled(bool checked) const;
        void on_yRadioButton_toggled(bool checked) const;
        void on_zRadioButton_toggled(bool checked) const;
        void on_tRadioButton_toggled(bool checked) const;

        void on_sliceMinSpinBox_valueChanged(int value) const;
        void on_sliceMaxSpinBox_valueChanged(int value) const;
        void on_sliceCheckBox_toggled(bool checked) const;
        void on_sliceWindowComboBox_currentIndexChanged(int index) const;

        void on_timeMinSpinBox_valueChanged(int value) const;
        void on_timeMaxSpinBox_valueChanged(int value) const;
        void on_timeCheckBox_toggled(bool checked) const;

        void onAddAction(bool) const;

        void onRecord(bool);

        void onCurrentModalityChanged(Entity::Modality modality) const;
        void onSelectedActionItem(RecordActionItem* item) const;

        void onFinishedVideo();

    protected:
        virtual auto keyPressEvent(QKeyEvent* keyPress) -> void;

    private:
        auto forceSelectItem(RecordActionItem* item)const->void;
        auto MakeUpProcess() const ->bool;
        auto SetPlayingState(bool isPlaying) -> void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}