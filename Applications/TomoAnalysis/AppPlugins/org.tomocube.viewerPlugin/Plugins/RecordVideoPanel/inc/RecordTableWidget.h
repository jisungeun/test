#pragma once

#include <QTableView>
#include "TomoAnalysisRecordVideoPanelExport.h"

namespace TomoAnalysis::UiModule {
    class TomoAnalysisRecordVideoPanel_API RecordVideoPanel : public QWidget {
        Q_OBJECT
        
    public:
        typedef RecordVideoPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        RecordVideoPanel(QWidget* parent=nullptr);
        ~RecordVideoPanel();

        auto Update()->bool override;
        auto Init(void) const ->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}