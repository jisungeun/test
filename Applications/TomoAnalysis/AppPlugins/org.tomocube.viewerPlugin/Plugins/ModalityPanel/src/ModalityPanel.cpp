#include <iostream>

#include <UIUtility.h>

#include "ui_ModalityPanel.h"
#include "ModalityPanel.h"

namespace  TomoAnalysis::Viewer::Plugins {
    struct ModalityPanel::Impl {
        Ui::ModalityPanel* ui{ nullptr };

        int usableModality{ Entity::Modality::None };
        int activatedModality{ Entity::Modality::None };

        Entity::DisplayType currentDisplayType{ Entity::DisplayType::DISPLAY_NONE };
    };

    ModalityPanel::ModalityPanel(QWidget* parent)
    : QWidget(parent)
    , Interactor::IModalityPanel()
    , d{ new Impl } {
        d->ui = new Ui::ModalityPanel();
        d->ui->setupUi(this);

        d->ui->contentsWidget->setObjectName("panel-contents");
    }

    ModalityPanel::~ModalityPanel() {
        delete d->ui;
    }

    auto ModalityPanel::Update()->bool {
        const auto ds = GetModalityDS();
        if (!ds) {
            return false;
        }

        d->usableModality = ds->usableModality;
        
        if (d->usableModality & Entity::Modality::HTVolume ||
            d->usableModality & Entity::Modality::FLVolume) {
            if (d->usableModality & Entity::Modality::HTVolume) {
                d->ui->HTCheckBox->setVisible(true);
            } else {
                d->ui->HTCheckBox->setVisible(false);
            }

            if (d->usableModality & Entity::Modality::FLVolume) {
                d->ui->FLCheckBox->setVisible(true);
            } else {
                d->ui->FLCheckBox->setVisible(false);
            }

            if (d->usableModality & Entity::Modality::HTVolume) {
                TC::SilentCall(d->ui->HTCheckBox)->setChecked(true);
                TC::SilentCall(d->ui->FLCheckBox)->setChecked(false);

                d->activatedModality = Entity::Modality::HTVolume;
            } else if (d->usableModality & Entity::Modality::FLVolume) {
                TC::SilentCall(d->ui->HTCheckBox)->setChecked(false);
                TC::SilentCall(d->ui->FLCheckBox)->setChecked(true);

                d->activatedModality = Entity::Modality::FLVolume;
            }

            emit activatedModalityChanged(static_cast<Entity::Modality>(d->activatedModality));
        }

        if(ds->ForceModality != Entity::Modality::None) {
            if(ds->ForceModality & Entity::Modality::FLVolume && false == d->ui->FLCheckBox->isChecked()) {
                d->ui->FLCheckBox->setChecked(true);
            }
            if(ds->ForceModality & Entity::Modality::HTVolume && false == d->ui->HTCheckBox->isChecked()) {
                d->ui->HTCheckBox->setChecked(true);
            }
        }

        ds->ForceModality = Entity::Modality::None;
        return true;
    }

    auto ModalityPanel::Init(void) const ->bool {
        const auto isEnable = false;
        const auto isChecked = false;

        TC::SilentCall(d->ui->HTCheckBox)->setVisible(isEnable);
        TC::SilentCall(d->ui->FLCheckBox)->setVisible(isEnable);
        TC::SilentCall(d->ui->BFCheckBox)->setVisible(isEnable);

        TC::SilentCall(d->ui->HTCheckBox)->setChecked(isChecked);
        TC::SilentCall(d->ui->FLCheckBox)->setChecked(isChecked);
        TC::SilentCall(d->ui->BFCheckBox)->setChecked(isChecked);

        return true;
    }

    auto ModalityPanel::Reset(void) const ->bool {
        d->usableModality = Entity::Modality::None;
        d->activatedModality = Entity::Modality::None;

        //d->currentDisplayType = Entity::DisplayType::DISPLAY_NONE;//Keep current display
        d->currentDisplayType = Entity::DISPLAY_3D;//reset current display type to 3D

        return true;
    }

    void ModalityPanel::onDisplayTypeChanged(Entity::DisplayType displayType) {        
        d->currentDisplayType = displayType;

        if (d->currentDisplayType == Entity::DisplayType::DISPLAY_3D) {
            d->ui->HTCheckBox->setVisible(d->usableModality & Entity::Modality::HTVolume);
            d->ui->FLCheckBox->setVisible(d->usableModality & Entity::Modality::FLVolume);
            d->ui->BFCheckBox->setVisible(false);
            //d->ui->BFCheckBox->setVisible(d->usableModality & Entity::Modality::BF2D);

            if (d->usableModality & Entity::Modality::HTVolume) {
                TC::SilentCall(d->ui->HTCheckBox)->setChecked(true);
                TC::SilentCall(d->ui->FLCheckBox)->setChecked(false);
                TC::SilentCall(d->ui->BFCheckBox)->setChecked(false);

                d->activatedModality = Entity::Modality::HTVolume;
            } else if (d->usableModality & Entity::Modality::FLVolume) {
                TC::SilentCall(d->ui->HTCheckBox)->setChecked(false);
                TC::SilentCall(d->ui->FLCheckBox)->setChecked(true);
                TC::SilentCall(d->ui->BFCheckBox)->setChecked(false);

                d->activatedModality = Entity::Modality::FLVolume;
            }
        } else if (d->currentDisplayType == Entity::DisplayType::DISPLAY_2D) {
            d->ui->HTCheckBox->setVisible(d->usableModality & Entity::Modality::HTMIP);
            d->ui->FLCheckBox->setVisible(d->usableModality & Entity::Modality::FLMIP);
            d->ui->BFCheckBox->setVisible(d->usableModality & Entity::Modality::BF2D);

            if (d->usableModality & Entity::Modality::HTMIP) {
                TC::SilentCall(d->ui->HTCheckBox)->setChecked(true);
                TC::SilentCall(d->ui->FLCheckBox)->setChecked(false);
                TC::SilentCall(d->ui->BFCheckBox)->setChecked(false);

                d->activatedModality = Entity::Modality::HTMIP;
            } else if (d->usableModality & Entity::Modality::FLMIP) {
                TC::SilentCall(d->ui->HTCheckBox)->setChecked(false);
                TC::SilentCall(d->ui->FLCheckBox)->setChecked(true);
                TC::SilentCall(d->ui->BFCheckBox)->setChecked(false);

                d->activatedModality = Entity::Modality::FLMIP;
            } else if(d->usableModality & Entity::Modality::BF2D) {
                TC::SilentCall(d->ui->HTCheckBox)->setChecked(false);
                TC::SilentCall(d->ui->FLCheckBox)->setChecked(false);
                TC::SilentCall(d->ui->BFCheckBox)->setChecked(true);
            }
        } else if (d->currentDisplayType == Entity::DisplayType::DISPLAY_MIP) {
            d->ui->HTCheckBox->setVisible(d->usableModality & Entity::Modality::HTMIP);
            d->ui->FLCheckBox->setVisible(d->usableModality & Entity::Modality::FLMIP);

            if (d->usableModality & Entity::Modality::HTMIP) {
                TC::SilentCall(d->ui->HTCheckBox)->setChecked(true);
                TC::SilentCall(d->ui->FLCheckBox)->setChecked(false);
                TC::SilentCall(d->ui->BFCheckBox)->setChecked(false);

                d->activatedModality = Entity::Modality::HTMIP;
            } else if (d->usableModality & Entity::Modality::FLMIP) {
                TC::SilentCall(d->ui->HTCheckBox)->setChecked(false);
                TC::SilentCall(d->ui->FLCheckBox)->setChecked(true);
                TC::SilentCall(d->ui->BFCheckBox)->setChecked(false);

                d->activatedModality = Entity::Modality::FLMIP;
            }else if(d->usableModality & Entity::Modality::BF2D) {
                TC::SilentCall(d->ui->HTCheckBox)->setChecked(false);
                TC::SilentCall(d->ui->FLCheckBox)->setChecked(false);
                TC::SilentCall(d->ui->BFCheckBox)->setChecked(true);
            }
        }

        emit activatedModalityChanged(static_cast<Entity::Modality>(d->activatedModality));
    }


    void ModalityPanel::on_HTCheckBox_toggled(bool checked) {
        if (false == checked) {
            if (false == d->ui->FLCheckBox->isChecked() && false == d->ui->BFCheckBox->isChecked()) {
                TC::SilentCall(d->ui->HTCheckBox)->setChecked(true);
                return;
            }
        }
        if (d->currentDisplayType == Entity::DisplayType::DISPLAY_3D) { // volume
            if (checked) {
                d->activatedModality = d->activatedModality | Entity::Modality::HTVolume;
            } else {
                d->activatedModality = d->activatedModality & ~Entity::Modality::HTVolume;
            }
        } else { // mip
            if (checked) {
                d->activatedModality = d->activatedModality | Entity::Modality::HTMIP;
            } else {
                d->activatedModality = d->activatedModality & ~Entity::Modality::HTMIP;
            }
        }

        emit activatedModalityChanged(static_cast<Entity::Modality>(d->activatedModality));
    }

    void ModalityPanel::on_FLCheckBox_toggled(bool checked) {
        if (false == checked) {
            if (false == d->ui->HTCheckBox->isChecked() && false == d->ui->BFCheckBox->isChecked()) {
                TC::SilentCall(d->ui->FLCheckBox)->setChecked(true);
                return;
            }
        }
        if (d->currentDisplayType == Entity::DisplayType::DISPLAY_3D) { // volume
            if (checked) {
                d->activatedModality = d->activatedModality | Entity::Modality::FLVolume;
            } else {
                d->activatedModality = d->activatedModality & ~Entity::Modality::FLVolume;
            }
        } else { // mip
            if (checked) {
                d->activatedModality = d->activatedModality | Entity::Modality::FLMIP;
            } else {
                d->activatedModality = d->activatedModality & ~Entity::Modality::FLMIP;
            }
        }

        emit activatedModalityChanged(static_cast<Entity::Modality>(d->activatedModality));
    }

    void ModalityPanel::on_BFCheckBox_toggled(bool checked) {
        if (false == checked) {
            if (false == d->ui->FLCheckBox->isChecked() && false == d->ui->HTCheckBox->isChecked()) {
                TC::SilentCall(d->ui->BFCheckBox)->setChecked(true);
                return;
            }
        }
        if(d->currentDisplayType != Entity::DISPLAY_3D) {
            if(checked) {
                d->activatedModality = d->activatedModality | Entity::Modality::BF2D;
            }else {
                d->activatedModality = d->activatedModality & ~Entity::Modality::BF2D;
            }
        }
        emit activatedModalityChanged(static_cast<Entity::Modality>(d->activatedModality));
    }
}

