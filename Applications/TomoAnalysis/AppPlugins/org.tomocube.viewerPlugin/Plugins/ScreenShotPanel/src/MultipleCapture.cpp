#include "ui_MultipleCapture.h"
#include "MultipleCapture.h"

namespace TomoAnalysis::Viewer::Plugins {
	struct MultipleCapture::Impl {
        Ui::Dialog* ui{ nullptr };
	};
	MultipleCapture::MultipleCapture(QString labelText, QWidget* parent) : QDialog(parent), d{ new Impl }{
        d->ui = new Ui::Dialog;
        d->ui->setupUi(this);

        d->ui->label->setText(labelText);
        setObjectName("widget-popup");
        d->ui->label->setObjectName("h8");
        d->ui->replaceBtn->setObjectName("bt-square-primary");
        d->ui->cancelBtn->setObjectName("bt-square-gray");
        connect(d->ui->replaceBtn, SIGNAL(clicked()), this, SLOT(OnReplaceBtn()));
        connect(d->ui->cancelBtn, SIGNAL(clicked()), this, SLOT(OnCancelBtn()));
    }
	MultipleCapture::~MultipleCapture() {
        
    }
	void MultipleCapture::OnCancelBtn() {
        reject();
    }
	void MultipleCapture::OnReplaceBtn() {
        accept();
    }
}