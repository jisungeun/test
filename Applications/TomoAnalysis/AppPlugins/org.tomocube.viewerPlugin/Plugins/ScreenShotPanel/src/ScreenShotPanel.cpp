#include <iostream>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QStandardPaths>

#include <MultipleCapture.h>
#include <UIUtility.h>

#include "ui_ScreenShotPanel.h"
#include "ScreenShotPanel.h"

namespace  TomoAnalysis::Viewer::Plugins {
    struct ScreenShotPanel::Impl {
        Ui::ScreenShotPanel* ui{ nullptr };

        QString tcfName;
    };

    ScreenShotPanel::ScreenShotPanel(QWidget* parent)
    : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::ScreenShotPanel();
        d->ui->setupUi(this);

        d->ui->sliceUpsamplingSpinBox->setRange(1, 5);
        d->ui->volumeUpsamplingSpinBox->setRange(1, 5);
        d->ui->multiViewUpsamplingSpinBox->setRange(1, 5);

        d->ui->sliceUpsamplingSpinBox->setValue(3);

        d->ui->multiViewUpsamplingLabel->hide();
        d->ui->multiViewUpsamplingSpinBox->hide();

        // set object names
        d->ui->contentsWidget->setObjectName("panel-contents");
        d->ui->upsampling2DLabel->setObjectName("h9");
        d->ui->sliceCaptureButton->setObjectName("bt-round-gray700");

        d->ui->upsampling3DLabel->setObjectName("h9");
        d->ui->volumeSingleCaptureButton->setObjectName("bt-round-gray700");
        d->ui->volumeAllCaptureButton->setObjectName("bt-round-gray700");

        d->ui->multiViewUpsamplingLabel->setObjectName("h9");
        d->ui->multiViewCaptureButton->setObjectName("bt-round-gray700");
    }

    ScreenShotPanel::~ScreenShotPanel() {
        
    }

    auto ScreenShotPanel::Update()->bool {
        auto ds = GetScreenShotDS();

        auto path = QString::fromUtf8(ds->tcfName.c_str());
        QFileInfo fileInfo(path);
        d->tcfName = fileInfo.completeBaseName();

        this->SetEnableUI(true);

        return true;
    }

    auto ScreenShotPanel::Init(void) const ->bool {
        this->SetEnableUI(false);

        return true;
    }

    auto ScreenShotPanel::Reset() const -> bool {
        return true;
    }

    auto ScreenShotPanel::force2D() -> void {
        d->ui->xyRadioButton->setChecked(true);
        d->ui->yzRadioButton->setEnabled(false);
        d->ui->xzRadioButton->setEnabled(false);
        d->ui->allRadioButton->setEnabled(false);
        d->ui->tabWidget->setCurrentIndex(0);
        d->ui->tabWidget->tabBar()->setEnabled(false);
    }

    auto ScreenShotPanel::restore3D() -> void {
        d->ui->yzRadioButton->setEnabled(true);
        d->ui->xzRadioButton->setEnabled(true);
        d->ui->allRadioButton->setEnabled(true);
        d->ui->tabWidget->tabBar()->setEnabled(true);
    }

    void ScreenShotPanel::on_sliceCaptureButton_clicked(bool clicked) {
        Q_UNUSED(clicked)
        if(true == d->tcfName.isEmpty()) {
            return;
        }

        TC::CaptureType type = TC::CaptureType::XY;
        if (true == d->ui->xyRadioButton->isChecked()) {
            type = TC::CaptureType::XY;
        }
        else if (true == d->ui->yzRadioButton->isChecked()) {
            type = TC::CaptureType::YZ;
        }
        else if (true == d->ui->xzRadioButton->isChecked()) {
            type = TC::CaptureType::XZ;
        }
        else if (true == d->ui->allRadioButton->isChecked()) {
            type = TC::CaptureType::ALL;
        }

        const int upSampling = d->ui->sliceUpsamplingSpinBox->value();

        if(+TC::CaptureType::ALL == type) {
            const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent2DCapture").toString();
            const auto directory = QFileDialog::getExistingDirectory(this, "Select a directory to save screenshot files", prev);
            if (true == directory.isEmpty()) {
                return;
            }            
            QDir dir(directory);
            QString file_name[3];
            QStringList dupFiles;            
            for(auto i=0;i<3;i++) {
                auto index = TC::CaptureType::_from_integral(i);
                file_name[i] = d->tcfName + "_" + index._to_string() + "_plane.png";
                if(dir.exists(file_name[i])) {
                    dupFiles.push_back(file_name[i]);
                }
            }            
            if(dupFiles.size()>0) {
                QString script = QString();
                for(auto i=0;i<dupFiles.size();i++) {
                    auto dup = dupFiles[i];
                    script += dup;
                    if(i<dupFiles.size()-1)
                        script += "\n";
                }                
                MultipleCapture capture(script);
                capture.setWindowTitle("Warning");
                auto result = capture.exec();
                if(result < 1) {
                    return;
                }
            }
            //check directory already exist                                                
            QSettings("Tomocube", "TomoAnalysis").setValue("recent2DCapture", directory);

            for(int i = 0; i < 3; ++i) {
                auto ind = TC::CaptureType::_from_integral(i);
                const auto savePath = directory + "/" + file_name[i];
                emit captureSlice(ind, savePath, upSampling);
            }
        }
        else {
            const QString desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
            const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent2DCapture", desktopPath).toString();

            const QString name = d->tcfName + "_" + type._to_string() + "_plane.png";

            const auto savePath = QFileDialog::getSaveFileName(nullptr, tr("Save screenshot"), prev + "/" + name, tr("Image files (*.png *.jpg *.tiff)"));
            if (true == savePath.isEmpty()) {
                return;
            }

            QSettings("Tomocube", "TomoAnalysis").setValue("recent2DCapture", QFileInfo(savePath).dir().path());

            emit captureSlice(type, savePath, upSampling);
        }
    }

    void ScreenShotPanel::on_volumeSingleCaptureButton_clicked(bool clicked) {
        Q_UNUSED(clicked)
        if (true == d->tcfName.isEmpty()) {
            return;
        }

        const QString desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
        const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent3DSingleCapture", desktopPath).toString();

        const QString name = d->tcfName + "_3D_screenshot.png";

        const auto savePath = QFileDialog::getSaveFileName(nullptr, tr("Save screenshot"), prev + "/" + name, tr("Image files (*.png *.jpg *.tiff)"));
        if (true == savePath.isEmpty()) {
            return;
        }

        QSettings("Tomocube", "TomoAnalysis").setValue("recent3DSingleCapture", QFileInfo(savePath).dir().path());

        const int upsampling = d->ui->volumeUpsamplingSpinBox->value();

        emit captureVolume(savePath, upsampling);
    }

    void ScreenShotPanel::on_volumeAllCaptureButton_clicked(bool clicked) {
        Q_UNUSED(clicked)
        if (true == d->tcfName.isEmpty()) {
            return;
        }

        const int upsampling = d->ui->volumeUpsamplingSpinBox->value();

        const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent3DAllCapture").toString();
        const auto directory = QFileDialog::getExistingDirectory(this, "Select a directory to save screenshot files", prev);
        if (true == directory.isEmpty()) {
            return;
        }
        QSettings("Tomocube", "TomoAnalysis").setValue("recent3DAllCapture", directory);

        for(int i = 0; i < 3; ++i) {
            const auto name = d->tcfName + QString("_3D_View%1.png").arg(i + 1);
            const auto filePath = directory + "/" + name;

            emit directionChanged(TC::DirectionType::_from_index(i));
            emit captureVolume(filePath, upsampling);
        }
    }

    void ScreenShotPanel::on_multiViewCaptureButton_clicked(bool clicked) {
        Q_UNUSED(clicked)
        if (true == d->tcfName.isEmpty()) {
            return;
        }

        const QString desktopPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
        const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recentMultiCapture", desktopPath).toString();
        const QString name = d->tcfName + "_multi-view_screenshot.png";

        const auto savePath = QFileDialog::getSaveFileName(nullptr, tr("Save screenshot"), prev + "/" + name, tr("Image files (*.png *.jpg *.tiff)"));
        if (true == savePath.isEmpty()) {
            return;
        }

        QSettings("Tomocube", "TomoAnalysis").setValue("recentMultiCapture", QFileInfo(savePath).dir().path());

        emit captureMultiView(savePath, 1);
    }

    auto ScreenShotPanel::SetEnableUI(const bool& enable) const ->void {
        // 2D screenshots
        d->ui->xyRadioButton->setEnabled(enable);
        d->ui->yzRadioButton->setEnabled(enable);
        d->ui->xzRadioButton->setEnabled(enable);
        d->ui->allRadioButton->setEnabled(enable);
        d->ui->sliceCaptureButton->setEnabled(enable);
        d->ui->sliceUpsamplingSpinBox->setEnabled(enable);

        // 3D screenshots
        d->ui->volumeSingleCaptureButton->setEnabled(enable);
        d->ui->volumeAllCaptureButton->setEnabled(enable);
        d->ui->volumeUpsamplingSpinBox->setEnabled(enable);

        // multi view screenshots
        d->ui->multiViewUpsamplingSpinBox->setEnabled(enable);
        d->ui->multiViewCaptureButton->setEnabled(enable);
    }
}
