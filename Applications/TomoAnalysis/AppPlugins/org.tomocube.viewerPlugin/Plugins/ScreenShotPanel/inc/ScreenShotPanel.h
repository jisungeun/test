#pragma once

#include <memory>
#include <QWidget>

#include <IScreenShotPanel.h>

#include <ScreenShot.h>

#include "TomoAnalysisScreenShotPanelExport.h"

namespace TomoAnalysis::Viewer::Plugins {
    class TomoAnalysisScreenShotPanel_API ScreenShotPanel : public QWidget, public Interactor::IScreenShotPanel {
        Q_OBJECT
        
    public:
        typedef ScreenShotPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ScreenShotPanel(QWidget* parent=nullptr);
        ~ScreenShotPanel();

        auto Update()->bool override;

        auto Init(void) const ->bool;
        auto Reset(void)const->bool;

        auto force2D()->void;
        auto restore3D()->void;

    signals:
        void captureSlice(TC::CaptureType, QString path, int upsampling);
        void captureVolume(QString path, int upsampling);
        void captureMultiView(QString path, int upsampling);

        void directionChanged(TC::DirectionType type);

    protected slots:
        void on_sliceCaptureButton_clicked(bool clicked);

        void on_volumeSingleCaptureButton_clicked(bool clicked);
        void on_volumeAllCaptureButton_clicked(bool clicked);

        void on_multiViewCaptureButton_clicked(bool clicked);

    private:
        auto SetEnableUI(const bool& enable) const ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}