#pragma once

#include <memory>
#include <QDialog>

namespace TomoAnalysis::Viewer::Plugins {
    class  MultipleCapture : public QDialog {
        Q_OBJECT
    public:
        explicit MultipleCapture(QString labelText, QWidget* parent = nullptr);
        virtual ~MultipleCapture();

    protected slots:
        void OnReplaceBtn();
        void OnCancelBtn();

    private:        

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
