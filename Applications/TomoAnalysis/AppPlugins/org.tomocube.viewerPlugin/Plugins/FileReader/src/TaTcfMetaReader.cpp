#include <iostream>

#include <HDF5Mutex.h>

#include <TCFMetaReader.h>

#include "TaTcfMetaReader.h"

namespace TomoAnalysis::Viewer::Plugins::FileReader {
	TcfMetaReader::TcfMetaReader() {
	    
	}

    TcfMetaReader::TcfMetaReader(const TcfMetaReader& /*other*/) {
		
	}

	TcfMetaReader::~TcfMetaReader() = default;

	
	auto TcfMetaReader::Read(const std::string& path,bool isCrop,int offsetX,int offsetY,int sizeX,int sizeY,int offsetXFL,int offsetYFL,int sizeXFL,int sizeYFL)->Entity::Image::Pointer {
		if (true == path.empty()) {
			return nullptr;
		}

		auto image = std::make_shared<Entity::Image>();
		image->SetPath(path);

		auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();

		auto metaInfo = metaReader->Read(QString(path.c_str()));

		auto ht3D = metaInfo->data.data3D.exist;
		if(ht3D) {
			Entity::Image::Meta meta;
			meta.count = metaInfo->data.data3D.dataCount;
			meta.interval = static_cast<uint32_t>(metaInfo->data.data3D.timeInterval);
			if (isCrop) {
				meta.dimension.X = sizeX;
				meta.dimension.Y = sizeY;
			}else {
				meta.dimension.X = metaInfo->data.data3D.sizeX;
				meta.dimension.Y = metaInfo->data.data3D.sizeY;
			}
			meta.dimension.Z = metaInfo->data.data3D.sizeZ;

			meta.resolution.X = metaInfo->data.data3D.resolutionX;
			meta.resolution.Y = metaInfo->data.data3D.resolutionY;
			meta.resolution.Z = metaInfo->data.data3D.resolutionZ;

			meta.intensity.min = metaInfo->data.data3D.riMin;
			meta.intensity.max = metaInfo->data.data3D.riMax;

			auto qvector = metaInfo->data.data3D.timePoints.toVector();
			meta.timepoints[0] = std::vector(qvector.begin(),qvector.end());

			image->SetMeta(Entity::ImageType::HT3D, meta);
		}		
		
		auto fl3D = metaInfo->data.data3DFL.exist;		
		if (fl3D) {
			for (auto ch = 0; ch < 3; ch++) {
				Entity::Image::Meta meta;
				meta.count = metaInfo->data.data3DFL.dataCount;
				meta.interval = static_cast<uint32_t>(metaInfo->data.data3DFL.timeInterval);

				if (isCrop) {
					meta.dimension.X = sizeXFL;
					meta.dimension.Y = sizeYFL;
				}
				else {
					meta.dimension.X = metaInfo->data.data3DFL.sizeX;
					meta.dimension.Y = metaInfo->data.data3DFL.sizeY;
				}
				meta.dimension.Z = metaInfo->data.data3DFL.sizeZ;

				meta.resolution.X = metaInfo->data.data3DFL.resolutionX;
				meta.resolution.Y = metaInfo->data.data3DFL.resolutionY;
				meta.resolution.Z = metaInfo->data.data3DFL.resolutionZ;

				for (int i = 0; i < 3; ++i) {
					meta.channel[i].isValid = metaInfo->data.data3DFL.valid[i];
					meta.channel[i].min = metaInfo->data.data3DFL.min[i];
					meta.channel[i].max = metaInfo->data.data3DFL.max[i];
					meta.channel[i].name = metaInfo->data.data3DFL.name[i].toStdString();
					meta.channel[i].r = metaInfo->data.data3DFL.r[i];
					meta.channel[i].g = metaInfo->data.data3DFL.g[i];
					meta.channel[i].b = metaInfo->data.data3DFL.b[i];
					auto qvector = metaInfo->data.data3DFL.timePoints[i].toVector();
					meta.timepoints[i] = std::vector(qvector.begin(), qvector.end());
				}				

				image->SetMeta(Entity::ImageType::FL3D, meta);
			}
		}
		image->SetFirstTime(metaInfo->data.total_time.first());
		return image;
	}

    // private
	auto TcfMetaReader::ReadHT3D(const std::string& path, const int& index) -> HTMeta::Pointer {
		if(true == path.empty()) {
			return nullptr;
		}

		if(0 > index) {
			return nullptr;
		}

		TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

		auto meta = std::make_shared<HTMeta>();

		try {
			H5::H5File file(path, H5F_ACC_RDONLY);

			auto gg = file.openGroup("/Data");

			auto isLDM = false;
			if(gg.attrExists("isLDM")) {
			    isLDM = true;
			}

			gg.close();

			H5::Group group = file.openGroup("/Data/3D");
			{
				int count = -1;
			    double res[3] = {-1.0, };
				int dim[3] = {-1, };
				double range[2] = {-1.0, };
				double interval = -1.0;

				bool hasRange = true;				

				if (false == group.exists("000000")) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "DataCount", count)) {
					throw H5::Exception();
				}								

				if (false == ReadAttribute(group, "ResolutionX", res[0])) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "ResolutionY", res[1])) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "ResolutionZ", res[2])) {
					throw H5::Exception();
				}
				
				if (false == ReadAttribute(group, "RIMin", range[0])) {
					hasRange = false;
				}

				if (false == ReadAttribute(group, "RIMax", range[1])) {
					hasRange = false;
				}

				if (false == ReadAttribute(group, "TimeInterval", interval)) {
					throw H5::Exception();
				}

				for(auto i=0;i<count;i++) {
					auto old_string = std::to_string(i);
					std::string new_string = std::string(6 - old_string.length(), '0') + old_string;					
					if(group.exists(new_string)) {
						if(isLDM) {
							H5::Group dataSet = group.openGroup(new_string);
							double time_point;
							if (ReadTimePoint(dataSet, time_point)) {
								meta->time_points.push_back(time_point);
							}
							dataSet.close();
						}
						else {
							H5::DataSet dataSet = group.openDataSet(new_string);
							double time_point;
							if (ReadTimePoint(dataSet, time_point)) {
								meta->time_points.push_back(time_point);
							}
							dataSet.close();
						}
					}					
				}
				if(count != meta->time_points.size()) {
					meta->time_points.clear();
					for(auto i=0;i<count;i++) {
					    meta->time_points.push_back(i*interval);
					}
				}

				if(!isLDM){
				    const auto dimension = GetDimension(group.openDataSet("000000"));

				    if(false == hasRange) {
					    H5::DataSet dataSet = group.openDataSet("000000");
					    {
    						if (false == ReadAttribute(dataSet, "RIMin", range[0])) {
							    //throw H5::Exception();
						    }
    
	    					if (false == ReadAttribute(dataSet, "RIMax", range[1])) {
							    //throw H5::Exception();
						    }
					    }
						dataSet.close();
				    }
					meta->sizeX = dimension.x;
				    meta->sizeY = dimension.y;	
				    meta->sizeZ = dimension.z;
				}else {
				    if (false == ReadAttribute(group, "SizeX", dim[0])) {
				        throw H5::Exception();
				    }
				    if (false == ReadAttribute(group, "SizeY", dim[1])) {
				        throw H5::Exception();
				    }
				    if (false == ReadAttribute(group, "SizeZ", dim[2])) {
				        throw H5::Exception();
				    }
					meta->sizeX = dim[0];
					meta->sizeY = dim[1];
					meta->sizeZ = dim[2];
					auto subGroup = group.openGroup("000000");
					if (false == ReadAttribute(subGroup, "RIMin", range[0])) {
					    throw H5::Exception();
					}

				    if (false == ReadAttribute(subGroup, "RIMax", range[1])) {
				        throw H5::Exception();
				    }
					subGroup.close();
				}

				meta->dataCount = count;

				meta->resolutionX = static_cast<float>(res[0]);
				meta->resolutionY = static_cast<float>(res[1]);
				meta->resolutionZ = static_cast<float>(res[2]);

				meta->riMin = static_cast<float>(range[0]);
				meta->riMax = static_cast<float>(range[1]);

				meta->timeInterval = static_cast<float>(interval);
			}
			group.close();
			file.close();
		}
		catch (H5::Exception& ) {
			return nullptr;
		}

		return meta;
	}
	
	auto TcfMetaReader::ReadHTMIP(const std::string& path, const int& index) -> HTMeta::Pointer {
		if (true == path.empty()) {
			return nullptr;
		}

		if (0 > index) {
			return nullptr;
		}

		TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

		auto meta = std::make_shared<HTMeta>();

		try {
			H5::H5File file(path, H5F_ACC_RDONLY);

			auto gg = file.openGroup("/Data");

			auto isLDM = false;
			if (gg.attrExists("isLDM")) {
				isLDM = true;
			}
			gg.close();

			H5::Group group = file.openGroup("/Data/2DMIP");
			{
				int count = -1;
				double res[2] = { -1.0, };
				int dim[2] = { -1, };
				double range[2] = { -1.0, };
				double interval = -1.0;

				bool hasRange = true;

				if (false == group.exists("000000")) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "DataCount", count)) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "ResolutionX", res[0])) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "ResolutionY", res[1])) {
					throw H5::Exception();
				}
								
				if (false == ReadAttribute(group, "RIMin", range[0])) {
					hasRange = false;
				}

				if (false == ReadAttribute(group, "RIMax", range[1])) {
					hasRange = false;
				}

				if (false == ReadAttribute(group, "TimeInterval", interval)) {
					throw H5::Exception();
				}

				for(auto i=0;i<count;i++) {
					auto old_string = std::to_string(i);
					std::string new_string = std::string(6 - old_string.length(), '0') + old_string;					
					if(group.exists(new_string)) {
						if (isLDM) {
							H5::Group dataSet = group.openGroup(new_string);
							double time_point;
							if (ReadTimePoint(dataSet, time_point)) {
								meta->time_points.push_back(time_point);
							}
							dataSet.close();
						}else {
							H5::DataSet dataSet = group.openDataSet(new_string);
							double time_point;
							if (ReadTimePoint(dataSet, time_point)) {
								meta->time_points.push_back(time_point);
							}
							dataSet.close();
						}
					}					
				}
				if(count != meta->time_points.size()) {
					meta->time_points.clear();
					for(auto i=0;i<count;i++) {
					    meta->time_points.push_back(i*interval);
					}
				}

				if (!isLDM) {
					const auto dimension = GetDimension(group.openDataSet("000000"));

					if (false == hasRange) {
						H5::DataSet dataSet = group.openDataSet("000000");
						{
							if (false == ReadAttribute(dataSet, "RIMin", range[0])) {
								//throw H5::Exception();
							}

							if (false == ReadAttribute(dataSet, "RIMax", range[1])) {
								//throw H5::Exception();
							}
						}
						dataSet.close();
					}
					meta->sizeX = dimension.x;
					meta->sizeY = dimension.y;					
				}
				else {
					if (false == ReadAttribute(group, "SizeX", dim[0])) {
						throw H5::Exception();
					}
					if (false == ReadAttribute(group, "SizeY", dim[1])) {
						throw H5::Exception();
					}
					
					meta->sizeX = dim[0];
					meta->sizeY = dim[1];					
					auto subGroup = group.openGroup("000000");
					if (false == ReadAttribute(subGroup, "RIMin", range[0])) {
						throw H5::Exception();
					}

					if (false == ReadAttribute(subGroup, "RIMax", range[1])) {
						throw H5::Exception();
					}
					subGroup.close();
				}				

				meta->dataCount = count;

				meta->resolutionX = static_cast<float>(res[0]);
				meta->resolutionY = static_cast<float>(res[1]);

				meta->riMin = static_cast<float>(range[0]);
				meta->riMax = static_cast<float>(range[1]);

				meta->timeInterval = static_cast<float>(interval);				
			}
			group.close();
			file.close();
		}
		catch (H5::Exception& ) {
			return nullptr;
		}

		return meta;
	}

	auto TcfMetaReader::ReadFL3D(const std::string& path, const int& index) -> FLMeta::Pointer {
		if (true == path.empty()) {
			return nullptr;
		}

		if (0 > index) {
			return nullptr;
		}

		TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

		auto meta = std::make_shared<FLMeta>();

		try {
			Dimension dimension;

			H5::H5File file(path, H5F_ACC_RDONLY);

			H5::Group group = file.openGroup("/Data/3DFL");
			{
				int count = -1;
				double res[3] = { -1.0, };
				int dim[3] = { -1, };
				bool valid[3] = {false, };
				double minRange[3] = { -1.0, };
				double maxRange[3] = { -1.0, };
				double interval = -1.0;
				double offsetZ;
				double offsetZCompensation;

				bool hasData = false;
				for (int i = 0; i < 3; ++i) {
					std::string channel = "/CH" + std::to_string(i);
					std::string channelGroupName = "/Data/3DFL" + channel;

					if (false == group.nameExists(channelGroupName)) {
						continue;
					}

					H5::Group channelGroup = file.openGroup(channelGroupName);
					if(channelGroup.exists("000000")) {
						hasData = true;
						break;
					}
					channelGroup.close();
				}

				if (false == hasData) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "DataCount", count)) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "ResolutionX", res[0])) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "ResolutionY", res[1])) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "ResolutionZ", res[2])) {
					throw H5::Exception();
				}
				
				if (false == ReadAttribute(group, "TimeInterval", interval)) {
					throw H5::Exception();
				}

				for(int i=0;i<3;++i) {
					std::string channel = "/CH" + std::to_string(i);
					std::string channelGroupName = "/Data/3DFL" + channel;

					if(false == group.nameExists(channelGroupName)) {
					    continue;
					}
					auto chgroup = group.openGroup(channelGroupName);
					for(auto j=0;j<count;j++) {
					    auto old_string = std::to_string(j);
					    std::string new_string = std::string(6 - old_string.length(), '0') + old_string;
					    if(chgroup.exists(new_string)) {
					        H5::DataSet dataSet = chgroup.openDataSet(new_string);
					        double time_point;
					        if(ReadTimePoint(dataSet,time_point)){								
					            meta->time_points.push_back(time_point);
					        }
							dataSet.close();
					    }
					}
					chgroup.close();
				    if(count != meta->time_points.size()) {
				        meta->time_points.clear();
				        for(auto j=0;j<count;j++) {
				            meta->time_points.push_back(j*interval);
						}
				    }
					break;
				}

				if (false == ReadAttribute(group, "OffsetZ", offsetZ)) {
					//throw H5::Exception();
				}

				if (false == ReadAttribute(group, "OffsetZCompensation", offsetZCompensation)) {
					//throw H5::Exception();
				}								

				for(int i = 0; i < 3; ++i) {
					std::string channel = "/CH" + std::to_string(i);
					std::string channelGroupName = "/Data/3DFL" + channel;

				    if(false == group.nameExists(channelGroupName)) {
					    continue;
					}
					valid[i] = false;
					if (ReadAttribute(group, "MinIntensity", minRange[i]) && ReadAttribute(group, "MaxIntensity", maxRange[i])) {
					    //read Intensity from group first						
						valid[i] = true;
					}

				    H5::Group channelGroup = file.openGroup(channelGroupName);
					H5::DataSet dataSet = channelGroup.openDataSet("000000");
					{
						if (ReadAttribute(dataSet, "MinIntensity", minRange[i]) && ReadAttribute(dataSet, "MaxIntensity", maxRange[i])) {
							valid[i] = true;							
						}
					}										

				    dimension = GetDimension(dataSet);

					channelGroup.close();
					dataSet.close();
				}

				meta->dataCount = count;

				meta->resolutionX = static_cast<float>(res[0]);
				meta->resolutionY = static_cast<float>(res[1]);
				meta->resolutionZ = static_cast<float>(res[2]);

				meta->sizeX = dimension.x;
				meta->sizeY = dimension.y;
				meta->sizeZ = dimension.z;				

				for (int i = 0; i < 3; ++i) {
					meta->channelValid[i] = valid[i];
					meta->intensityMin[i] = static_cast<float>(minRange[i]);
					meta->intensityMax[i] = static_cast<float>(maxRange[i]);
				}

				meta->timeInterval = static_cast<float>(interval);

				meta->offsetZ = static_cast<float>(offsetZ);
				meta->offsetZCompensation = static_cast<float>(offsetZCompensation);
			}
			group.close();
			file.close();
		}
		catch (H5::Exception& ) {
			return nullptr;
		}

		return meta;
	}

	auto TcfMetaReader::ReadFLMIP(const std::string& path, const int& index) -> FLMeta::Pointer {
		if (true == path.empty()) {
			return nullptr;
		}

		if (0 > index) {
			return nullptr;
		}
		
		TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());
		H5::Exception::dontPrint();

		auto meta = std::make_shared<FLMeta>();

		try {
			Dimension dimension;

			H5::H5File file(path, H5F_ACC_RDONLY);

			H5::Group group = file.openGroup("/Data/2DFLMIP");
			{
				int count = -1;
				double res[2] = { -1.0, };
				int dim[2] = { -1, };
				bool valid[3] = { false, };
				double minRange[3] = { -1.0, };
				double maxRange[3] = { -1.0, };
				double interval = -1.0;				

				bool hasData = false;
				for (int i = 0; i < 3; ++i) {
					std::string channel = "/CH" + std::to_string(i);
					std::string channelGroupName = "/Data/2DFLMIP" + channel;

					if (false == group.nameExists(channelGroupName)) {
						continue;
					}

					H5::Group channelGroup = file.openGroup(channelGroupName);
					if (channelGroup.exists("000000")) {
						hasData = true;
						break;
					}
					channelGroup.close();
				}

				if (false == ReadAttribute(group, "DataCount", count)) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "ResolutionX", res[0])) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "ResolutionY", res[1])) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "TimeInterval", interval)) {
					throw H5::Exception();
				}

				for(int i=0;i<3;++i) {
					std::string channel = "/CH" + std::to_string(i);
					std::string channelGroupName = "/Data/2DFLMIP" + channel;

					if(false == group.nameExists(channelGroupName)) {
					    continue;
					}
					auto chgroup = group.openGroup(channelGroupName);
					for(auto j=0;j<count;j++) {
					    auto old_string = std::to_string(j);
					    std::string new_string = std::string(6 - old_string.length(), '0') + old_string;
					    if(chgroup.exists(new_string)) {
					        H5::DataSet dataSet = chgroup.openDataSet(new_string);
					        double time_point;
					        if(ReadTimePoint(dataSet,time_point)){								
					            meta->time_points.push_back(time_point);
					        }
							dataSet.close();
					    }
					}
					chgroup.close();
				    if(count != meta->time_points.size()) {				        
				        meta->time_points.clear();
				        for(auto j=0;j<count;j++) {
				            meta->time_points.push_back(j*interval);
						}
				    }
					break;
				}

				for (int i = 0; i < 3; ++i) {
					std::string channel = "/CH" + std::to_string(i);
					std::string channelGroupName = "/Data/2DFLMIP" + channel;

				    if (false == group.nameExists(channelGroupName)) {
						continue;
					}

					H5::Group channelGroup = file.openGroup(channelGroupName);
					H5::DataSet dataSet = channelGroup.openDataSet("000000");
					{
						if (false == ReadAttribute(dataSet, "MinIntensity", minRange[i])) {
							valid[i] = false;
							continue;
						}

						if (false == ReadAttribute(dataSet, "MaxIntensity", maxRange[i])) {
							valid[i] = false;
							continue;
						}

						valid[i] = true;
					}
					dataSet.close();
					channelGroup.close();
					dimension = GetDimension(dataSet);
				}

				meta->dataCount = count;

				meta->resolutionX = static_cast<float>(res[0]);
				meta->resolutionY = static_cast<float>(res[1]);

				meta->sizeX = dimension.x;
				meta->sizeY = dimension.y;

				for (int i = 0; i < 3; ++i) {
					meta->channelValid[i] = valid[i];
					meta->intensityMin[i] = static_cast<float>(minRange[i]);
					meta->intensityMax[i] = static_cast<float>(maxRange[i]);
				}

				meta->timeInterval = static_cast<float>(interval);
			}
			group.close();
			file.close();
		}
		catch (H5::Exception& ) {
			return nullptr;
		}

		return meta;
	}

	auto TcfMetaReader::ReadBF(const std::string& path, const int& index)->BFMeta::Pointer {
		if (true == path.empty()) {
			return nullptr;
		}

		if (0 > index) {
			return nullptr;
		}

		TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());

		auto meta = std::make_shared<BFMeta>();

		try {
			H5::H5File file(path, H5F_ACC_RDONLY);

			H5::Group group = file.openGroup("/Data/BF");
			{
				int count = -1;
				double res[2] = {-1.0, };
				int dim[2] = {-1, };
				double interval = -1.0;
								
				if(false == group.exists("000000")) {
					throw H5::Exception();
				}

			    if (false == ReadAttribute(group, "DataCount", count)) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "ResolutionX", res[0])) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "ResolutionY", res[1])) {
					throw H5::Exception();
				}

				if (false == ReadAttribute(group, "TimeInterval", interval)) {
					throw H5::Exception();
				}

				for(auto i=0;i<count;i++) {
					auto old_string = std::to_string(i);
					std::string new_string = std::string(6 - old_string.length(), '0') + old_string;					
					if(group.exists(new_string)) {
						H5::DataSet dataSet = group.openDataSet(new_string);
					    double time_point;
						if(ReadTimePoint(dataSet,time_point)){							
						    meta->time_points.push_back(time_point);
						}
						dataSet.close();
					}					
				}
				if(count != meta->time_points.size()) {
					meta->time_points.clear();
					for(auto i=0;i<count;i++) {
					    meta->time_points.push_back(i*interval);
					}					
				}

				const auto dimension = GetDimension(group.openDataSet("000000"));

				meta->dataCount = count;

				meta->resolutionX = static_cast<float>(res[0]);
				meta->resolutionY = static_cast<float>(res[1]);

				meta->sizeX = dimension.x;
				meta->sizeY = dimension.y;

				meta->timeInterval = static_cast<float>(interval);
			}
			group.close();
			file.close();
		}
		catch (H5::Exception& ) {
			return nullptr;
		}

		return meta;
	}

	auto TcfMetaReader::ReadAttribute(H5::Group& group, const char* name, double& dValue) const -> bool {
		if (false == group.attrExists(name)) {
			return false;
		}
			
		H5::Attribute attr = group.openAttribute(name);
		const bool bRet = ReadAttribute(attr, dValue);
		attr.close();

		return bRet;
	}

	auto TcfMetaReader::ReadAttribute(H5::Group& group, const char* name, int& nValue) const -> bool {
		if (false == group.attrExists(name)) {
			return false;
		}

		H5::Attribute attr = group.openAttribute(name);
		const bool bRet = ReadAttribute(attr, nValue);
		attr.close();

		return bRet;
	}
	auto TcfMetaReader::ReadTimePoint(H5::Group& grp, double& point) const -> bool {
		std::string attrName = "Time";
		if (false == grp.attrExists(attrName)) {
			return false;
		}

		H5::Attribute attr = grp.openAttribute(attrName);
		const bool bRet = ReadAttribute(attr, point);
		attr.close();

		return bRet;
	}
	auto TcfMetaReader::ReadTimePoint(H5::DataSet& dataset, double& point) const -> bool {
		std::string attrName = "Time";
        if(false == dataset.attrExists(attrName)) {
            return false;
        }

		H5::Attribute attr = dataset.openAttribute(attrName);
		const bool bRet = ReadAttribute(attr,point);
		attr.close();

		return bRet;
    }
	auto TcfMetaReader::ReadAttribute(H5::DataSet& dataset, const char* name, double& dValue) const -> bool {
		if (false == dataset.attrExists(name)) {
			return false;
		}
			
		H5::Attribute attr = dataset.openAttribute(name);
		const bool bRet = ReadAttribute(attr, dValue);
		attr.close();

		return bRet;
	}

	auto TcfMetaReader::ReadAttribute(H5::DataSet& dataset, const char* name, int& nValue) const -> bool {
		if (false == dataset.attrExists(name)) {
			return false;
		}

		H5::Attribute attr = dataset.openAttribute(name);
		const bool bRet = ReadAttribute(attr, nValue);
		attr.close();

		return bRet;
	}

	auto TcfMetaReader::ReadAttribute(H5::Attribute& attr, double& dValue) const -> bool {
		H5::DataType dt = attr.getDataType();
		if (dt == H5::PredType::NATIVE_DOUBLE) {
			attr.read(dt, &dValue);
		}else if(dt == H5::PredType::NATIVE_INT64) {
			int64_t tmpVal;
			attr.read(dt, &tmpVal);
			dValue = tmpVal;
		}

		return true;
	}

	auto TcfMetaReader::ReadAttribute(H5::Attribute& attr, int& nValue) const -> bool {
		//TODO Need to fix TCFWriter::WriteAttributeInteger, but need to support old version TCF files....
		H5Aread(attr.getId(), H5T_NATIVE_INT, &nValue);
		return true;
	}

	auto TcfMetaReader::ConvertStr2ImageType(const std::string& str)->Entity::ImageType {
		auto type = Entity::ImageType::Unknown;

	    if(0 == str.compare("2D")) {
			type = Entity::ImageType::PHASE;
	    }
        else if(0 == str.compare("2DFLMIP")) {
			type = Entity::ImageType::FL2D;
        }
        else if (0 == str.compare("2DMIP")) {
			type = Entity::ImageType::HT2D;
        }
        else if (0 == str.compare("3D")) {
			type = Entity::ImageType::HT3D;
        }
        else if (0 == str.compare("3DFL")) {
			type = Entity::ImageType::FL3D;
        }
		else if (0 == str.compare("BF")) {
			type = Entity::ImageType::BF;
		}
		else {
		    // none
		}

        return type;
	}

	auto TcfMetaReader::GetDimension(const H5::DataSet& dataSet)->Dimension {
		auto dataSpace = dataSet.getSpace();

		const auto dimensionality = dataSpace.getSimpleExtentNdims();
		const std::shared_ptr<hsize_t[]> dataSetSizes(new hsize_t[dimensionality]());
		dataSpace.getSimpleExtentDims(dataSetSizes.get());

		Dimension dimension;
		if (dimensionality == 2) {
			dimension.x = static_cast<int>(dataSetSizes[1]);
			dimension.y = static_cast<int>(dataSetSizes[0]);
		}
		else if (dimensionality == 3) {
			dimension.x = static_cast<int>(dataSetSizes[2]);
			dimension.y = static_cast<int>(dataSetSizes[1]);
			dimension.z = static_cast<int>(dataSetSizes[0]);
		}

		dataSpace.close();
		return dimension;
	}
}

