#pragma once

#include <QObject>
#include <IAppMeta.h>

#include "ViewerMetaExport.h"

namespace TomoAnalysis::Viewer::Plugins {
	class ViewerMeta_API AppMeta
		:public QObject
		, public IAppMeta
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.viewerMeta")
		Q_INTERFACES(IAppMeta)
	public:
		AppMeta();
		virtual ~AppMeta();

		auto GetName()const->QString override { return "3D View"; }
		auto GetFullName()const->QString override { return "org.tomocube.viewerPlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}