#include <UIUtility.h>

#include "ui_DisplayPanel.h"
#include "DisplayPanel.h"

namespace  TomoAnalysis::Viewer::Plugins {
    struct DisplayPanel::Impl {
        Ui::DisplayPanel* ui{ nullptr };

        Entity::DisplayType usableDisplayType{ Entity::DISPLAY_NONE };
    };

    DisplayPanel::DisplayPanel(QWidget* parent)
    : QWidget(parent)
    , Interactor::IDisplayPanel()
    , d{ new Impl } {
        d->ui = new Ui::DisplayPanel();
        d->ui->setupUi(this);

        d->ui->contentsWidget->setObjectName("panel-contents");
    }

    DisplayPanel::~DisplayPanel() {
        delete d->ui;
    }

    auto DisplayPanel::Update()->bool {
        const auto ds = GetDisplayDS();
        if (!ds) {
            return false;
        }

        TC::SilentCall(d->ui->display3DButton)->setChecked(false);
        TC::SilentCall(d->ui->display3DButton)->setEnabled(false);
        TC::SilentCall(d->ui->display3DButton)->hide();
        TC::SilentCall(d->ui->display2DButton)->setChecked(false);
        TC::SilentCall(d->ui->display2DButton)->setEnabled(false);
        TC::SilentCall(d->ui->display2DButton)->hide();

        d->usableDisplayType = ds->usableDisplayType;

        bool hasVolume = false;
        if (d->usableDisplayType & Entity::DisplayType::DISPLAY_3D) {
            TC::SilentCall(d->ui->display3DButton)->show();
            d->ui->display3DButton->setEnabled(true);
            d->ui->display3DButton->setChecked(true);            
            hasVolume = true;
        }

        if (d->usableDisplayType & Entity::DisplayType::DISPLAY_2D) {
            TC::SilentCall(d->ui->display2DButton)->show();
            d->ui->display2DButton->setEnabled(true);            
            if (!hasVolume) {
                d->ui->display2DButton->setChecked(true);
            }
        }

        return true;
    }

    auto DisplayPanel::Init(void) const ->bool {
        for (auto button : d->ui->displayButtonGroup->buttons()) {
            auto radioButton = qobject_cast<QRadioButton*>(button);
            if (radioButton == nullptr) {
                continue;
            }
            
            radioButton->setEnabled(false);
            TC::SilentCall(radioButton)->hide();
            TC::SilentCall(radioButton)->setChecked(false);
        }

        return true;
    }

    auto DisplayPanel::Reset(void) const ->bool {
        d->usableDisplayType = Entity::DisplayType::DISPLAY_NONE;

        return true;
    }

    void DisplayPanel::on_display3DButton_toggled(bool checked) {
        if (!checked) {
            return;
        }

        emit displayTypeChanged(Entity::DisplayType::DISPLAY_3D);
    }

    void DisplayPanel::on_display2DButton_toggled(bool checked) {
        if (!checked) {
            return;
        }

        emit displayTypeChanged(Entity::DisplayType::DISPLAY_2D);
    }
}

