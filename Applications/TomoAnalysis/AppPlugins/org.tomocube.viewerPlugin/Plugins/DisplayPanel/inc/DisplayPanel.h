#pragma once

#include <memory>
#include <QWidget>

#include <IDisplayPanel.h>
#include <Scene.h>

#include "TomoAnalysisDisplayPanelExport.h"

namespace TomoAnalysis::Viewer::Plugins {
    class TomoAnalysisDisplayPanel_API DisplayPanel : public QWidget, public Interactor::IDisplayPanel {
        Q_OBJECT

    public:
        typedef DisplayPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        DisplayPanel(QWidget* parent=nullptr);
        ~DisplayPanel();

        auto Update()->bool override;

        auto Init(void) const ->bool;
        auto Reset(void) const ->bool;

    signals:
        void displayTypeChanged(Entity::DisplayType);

    protected slots:
        void on_display3DButton_toggled(bool);
        void on_display2DButton_toggled(bool);

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}