#pragma once

#include <memory>
#include <QWidget>
#include <QAbstractButton>
#include <Scene.h>
#include <IViewingToolPanel.h>

#include "TomoAnalysisViewingToolPanelExport.h"

namespace TomoAnalysis::Viewer::Plugins {
    class TomoAnalysisViewingToolPanel_API ViewingToolPanel : public QWidget, public Interactor::IViewingToolPanel {
        Q_OBJECT

    public:
        typedef ViewingToolPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        ViewingToolPanel(QWidget* parent=nullptr);
        ~ViewingToolPanel();

        auto Update()->bool override;

        auto Init(void) const ->bool;
        auto Reset(void)->bool;

        void resizeEvent(QResizeEvent* event) override;

        auto force2D()->void;
        auto restore3D()->void;

    signals:
        void layoutChanged(Entity::LayoutType);
        void resetLayoutTriggered();
        void saveLayoutTriggered(const QString&);
        void loadLayoutTriggered(const QString&);
        void orientationMarkerVisibilityChanged(bool);
        void boundaryBoxVisibilityChanged(bool);
        void axisGridVisibilityChanged(bool);
        void timestampVisibilityChanged(bool);
        void timestampColorChanged(QColor);
        void timestampSizeChanged(int);
        void titleVisibilityChanged(bool);
        void sliceResolutionChanged(int);
        void volumeResolutionChanged(int);
        void axisGridFontSize(int);
        void axisGridColor(int, QColor);//0: x 1: y 2: z

    protected slots:
        void onLayoutToolClicked(bool);
        void onLayoutChanged(QAbstractButton*);
        void onResetLayoutTriggered();
        void onSaveLayoutTriggered();
        void onLoadLayoutTriggered();
        void onViewToolClicked(bool);
        void onTimeStampColorClicked(bool);
        void onTimeStampSizeChanged(int);
        void onSliceResolutionChanged(int);
        void onVolumeResolutionChanged(int);
        void onAxisGridSizeChanged(int);
        void onAxisGridColor();

    private:
        auto CreateViewingTool(bool is3D)->void;
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}