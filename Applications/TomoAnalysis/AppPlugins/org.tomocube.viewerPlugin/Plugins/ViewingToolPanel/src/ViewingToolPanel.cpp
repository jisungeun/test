#include <iostream>
#include <windows.h>

#include <QMenu>
#include <QActionGroup>
#include <QMessageBox>
#include <QFileDialog>
#include <QSettings>
#include <QButtonGroup>
#include <QWidgetAction>
#include <QSpinBox>
#include <QColorDialog>

#include <UIUtility.h>

#include "ui_ViewingToolPanel.h"
#include "ViewingToolPanel.h"

namespace  TomoAnalysis::Viewer::Plugins {
    using LayoutMapType = QMap<Entity::LayoutType, QString>;
    static const LayoutMapType layoutMap {
        { Entity::LayoutType::TwoByTwo,     "2x2 layout"  },
        { Entity::LayoutType::HSlicesBy3D,  "2D image left, 3D right" },
        { Entity::LayoutType::VSlicesBy3D,  "2D image top, 3D bottom" },
        { Entity::LayoutType::Only3D,       "Big 3D" },
        { Entity::LayoutType::XYPlane,      "XY Plane" },
        { Entity::LayoutType::YZPlane,      "YZ Plane" },
        { Entity::LayoutType::XZPlane,      "XZ Plane" },
        { Entity::LayoutType::HXY3D,        "XY Plane left, 3D right" },
        { Entity::LayoutType::VXY3D,        "XY Plane top, 3D bottom" }
    };

    struct ViewingToolPanel::Impl {
        Ui::ViewingToolPanel* ui{ nullptr };

        QButtonGroup* layoutButtons{ nullptr };

        QCheckBox* markerCheckBox{ nullptr };
        QCheckBox* boundaryBoxCheckBox{ nullptr };        

        QMenu* timestampSubmenu{ nullptr };
        QCheckBox* timestampCheckBox{ nullptr };
        QAction* timestampSeparator{ nullptr };
        QWidgetAction* timestampSize{ nullptr };
        QWidget* timestampSizeWidget{ nullptr };
        QSpinBox* timestampSizeSpin{ nullptr };
        QAction* timestampColor{ nullptr };

        QMenu* axisGridSubmenu{ nullptr };
        QPixmap axisPixmap[3];
        QAction* axisGridColorX{ nullptr };
        QAction* axisGridColorY{ nullptr };
        QAction* axisGridColorZ{ nullptr };
        QCheckBox* axisGridCheckBox{ nullptr };
        QWidget* axisGridSizeWidget{ nullptr };
        QWidgetAction* axisGridSize{ nullptr };
        QSpinBox* axisGridSizeSpin{ nullptr };

        QMenu* mainMenu{nullptr};

        int color[3] = { 255,255,255 };
        int axisColor[3][3] = {255, };

        QPixmap timePixmap;

        bool is3D{ true };

        Entity::LayoutType cur_3d_type{ Entity::LayoutType::HSlicesBy3D };
    };

    ViewingToolPanel::ViewingToolPanel(QWidget* parent)
    : QWidget(parent)
    , Interactor::IViewingToolPanel()
    , d{ new Impl } {
        d->ui = new Ui::ViewingToolPanel();
        d->ui->setupUi(this);        
        
        d->ui->contentsWidget->setObjectName("panel-contents");        
        d->timePixmap = QPixmap(100, 100);
        d->timePixmap.fill(QColor(255, 255, 255, 255));

        for(auto i=0;i<3;i++) {
            d->axisPixmap[i] = QPixmap(100, 100);            
        }
        d->axisPixmap[0].fill(QColor(0, 255, 0, 255));
        d->axisPixmap[1].fill(QColor(0, 0, 255, 255));
        d->axisPixmap[2].fill(QColor(255, 0, 0, 255));
        // add layout actions
        auto layoutMenu = new QMenu;        
        d->layoutButtons = new QButtonGroup;
        d->layoutButtons->setExclusive(true);

        LayoutMapType::const_iterator layoutIt = layoutMap.constBegin();
        while (layoutIt != layoutMap.constEnd()) {
            auto radioButton = new QRadioButton(layoutMenu);
            radioButton->setText(layoutIt.value());

            auto action = new QWidgetAction(layoutMenu);
            action->setDefaultWidget(radioButton);
            layoutMenu->addAction(action);

            d->layoutButtons->addButton(radioButton, layoutIt.key());

            // set default layout
            //if (layoutIt.key() == +Entity::LayoutType::HXY3D) {
            if (layoutIt.key() == +Entity::LayoutType::HSlicesBy3D) {
                radioButton->setChecked(true);
            }

            ++layoutIt;
        }

        connect(d->layoutButtons, SIGNAL(buttonClicked(QAbstractButton*)), this, SLOT(onLayoutChanged(QAbstractButton*)));

        layoutMenu->addSeparator();
        auto resetLayoutAction = layoutMenu->addAction(QIcon(":/img/ic-reset.svg"), "Reset");
        connect(resetLayoutAction, SIGNAL(triggered()), this, SLOT(onResetLayoutTriggered()));

        auto saveLayoutAction = layoutMenu->addAction(QIcon(":/img/ic-save.svg"), "Save");
        connect(saveLayoutAction, SIGNAL(triggered()), this, SLOT(onSaveLayoutTriggered()));

        auto loadLayoutAction = layoutMenu->addAction(QIcon(":/img/ic-load.svg"), "Load");
        connect(loadLayoutAction, SIGNAL(triggered()), this, SLOT(onLoadLayoutTriggered()));

        d->ui->layoutToolButton->setMenu(layoutMenu);
        
        connect(d->ui->layoutToolButton, SIGNAL(clicked(bool)), this, SLOT(onLayoutToolClicked(bool)));
        connect(layoutMenu, &QMenu::aboutToHide, [=]() {
            d->ui->layoutToolButton->blockSignals(true);
            d->ui->layoutToolButton->setChecked(false);
            d->ui->layoutToolButton->blockSignals(false);
            });
        connect(layoutMenu, &QMenu::aboutToShow, [=]() {
            d->ui->layoutToolButton->blockSignals(true);
            d->ui->layoutToolButton->setChecked(true);
            d->ui->layoutToolButton->blockSignals(false);
            });

        // add view actions
        //auto d->mainMenu = new QMenu;
        d->mainMenu = new QMenu;

        CreateViewingTool(true);

        d->ui->viewToolButton->setMenu(d->mainMenu);                

        connect(d->ui->viewToolButton, SIGNAL(clicked(bool)), this, SLOT(onViewToolClicked(bool)));
        connect(d->mainMenu, &QMenu::aboutToHide, [=]() {
            d->ui->viewToolButton->blockSignals(true);
            d->ui->viewToolButton->setChecked(false);
            d->ui->viewToolButton->blockSignals(false);
        });
        connect(d->mainMenu, &QMenu::aboutToShow, [=]() {
            d->ui->viewToolButton->blockSignals(true);
            d->ui->viewToolButton->setChecked(true);
            d->ui->viewToolButton->blockSignals(false);
        });

        // set object names
        d->ui->layoutToolButton->setObjectName("bt-tool-round");
        d->ui->viewToolButton->setObjectName("bt-tool-round");
        d->ui->sliceResCombo->setObjectName("bt-tool-round");
        d->ui->volumeResCombo->setObjectName("bt-tool-round");

        d->ui->sliceResCombo->addItem("Interactive");
        d->ui->sliceResCombo->addItem("Full");
        d->ui->sliceResCombo->addItem("1/2");
        d->ui->sliceResCombo->addItem("1/4");
        d->ui->sliceResCombo->addItem("1/8");

        d->ui->sliceResCombo->setCurrentIndex(1);

        d->ui->volumeResCombo->addItem("Interactive");
        d->ui->volumeResCombo->addItem("Full");
        d->ui->volumeResCombo->addItem("1/2");
        d->ui->volumeResCombo->addItem("1/4");
        d->ui->volumeResCombo->addItem("1/8");

        d->ui->volumeResCombo->setCurrentIndex(0);

        //connect resolution control
        connect(d->ui->sliceResCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onSliceResolutionChanged(int)));
        connect(d->ui->volumeResCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onVolumeResolutionChanged(int)));

        const auto ds = GetViewingToolDS();
    }

    ViewingToolPanel::~ViewingToolPanel() {
        delete d->ui;
    }

    void ViewingToolPanel::resizeEvent(QResizeEvent* event) {
        QWidget::resizeEvent(event);
        //auto d->mainMenu = d->ui->viewToolButton->menu();
        if (nullptr != d->mainMenu) {
            d->mainMenu->setMinimumWidth(d->ui->viewToolButton->width());
        }
        auto layMenu = d->ui->layoutToolButton->menu();
        if(nullptr != layMenu) {
            layMenu->setMinimumWidth(d->ui->layoutToolButton->width());
        }                
    }

    auto ViewingToolPanel::force2D() -> void {
        CreateViewingTool(false);
        d->is3D = false;
        emit layoutChanged(Entity::LayoutType::XYPlane);
        d->ui->layoutToolButton->setEnabled(false);        
        //d->ui->viewToolButton->setEnabled(false);        
    }

    auto ViewingToolPanel::restore3D()->void {        
        CreateViewingTool(true);
        d->is3D = true;
        emit layoutChanged(d->cur_3d_type);
        d->ui->layoutToolButton->setEnabled(true);        
        d->ui->viewToolButton->setEnabled(true);        
    }

    auto ViewingToolPanel::CreateViewingTool(bool is3D) -> void {
        d->mainMenu->clear();
        if (is3D) {
            // add orientation marker check box
            d->markerCheckBox = new QCheckBox(d->mainMenu);
            d->markerCheckBox->setChecked(false);
            d->markerCheckBox->setText("Show Orientation marker");
            connect(d->markerCheckBox, &QCheckBox::stateChanged,
                [=](int state) { emit orientationMarkerVisibilityChanged(state == Qt::Checked); });

            auto markerAction = new QWidgetAction(d->mainMenu);
            markerAction->setDefaultWidget(d->markerCheckBox);
            d->mainMenu->addAction(markerAction);

            // add boundary box check box
            d->boundaryBoxCheckBox = new QCheckBox(d->mainMenu);
            d->boundaryBoxCheckBox->setText("Show Boundary box");
            connect(d->boundaryBoxCheckBox, &QCheckBox::stateChanged,
                [=](int state) { emit boundaryBoxVisibilityChanged(state == Qt::Checked); });

            auto boundaryBoxAction = new QWidgetAction(d->mainMenu);
            boundaryBoxAction->setDefaultWidget(d->boundaryBoxCheckBox);
            d->mainMenu->addAction(boundaryBoxAction);

            // add axis grid check box
            d->axisGridCheckBox = new QCheckBox(d->mainMenu);
            d->axisGridCheckBox->setText("Show Axis grid");
            connect(d->axisGridCheckBox, &QCheckBox::stateChanged,
                [=](int state) { emit axisGridVisibilityChanged(state == Qt::Checked); });

            auto axisGridAction = new QWidgetAction(d->mainMenu);
            axisGridAction->setDefaultWidget(d->axisGridCheckBox);
            //d->mainMenu->addAction(axisGridAction);

            d->axisGridSubmenu = new QMenu(d->mainMenu);
            d->axisGridSubmenu->setTitle("Axis grid");

            d->axisGridSubmenu->addAction(axisGridAction);

            QIcon axisXIcon(d->axisPixmap[0]);
            QIcon axisYIcon(d->axisPixmap[1]);
            QIcon axisZIcon(d->axisPixmap[2]);
            d->axisGridColorX = new QAction(axisXIcon, "Color (X)");
            d->axisGridColorY = new QAction(axisYIcon, "Color (Y)");
            d->axisGridColorZ = new QAction(axisZIcon, "Color (Z)");

            connect(d->axisGridColorX, SIGNAL(triggered()), this, SLOT(onAxisGridColor()));
            connect(d->axisGridColorY, SIGNAL(triggered()), this, SLOT(onAxisGridColor()));
            connect(d->axisGridColorZ, SIGNAL(triggered()), this, SLOT(onAxisGridColor()));

            d->mainMenu->addMenu(d->axisGridSubmenu);

            //add axis grid control
            d->axisGridSize = new QWidgetAction(d->mainMenu);
            d->axisGridSizeSpin = new QSpinBox(d->mainMenu);
            d->axisGridSizeSpin->setRange(5, 30);
            d->axisGridSizeSpin->setValue(18);
            connect(d->axisGridSizeSpin, SIGNAL(valueChanged(int)), this, SLOT(onAxisGridSizeChanged(int)));

            d->axisGridSizeWidget = new QWidget(d->mainMenu);
            auto aglayout = new QHBoxLayout;
            d->axisGridSizeWidget->setLayout(aglayout);
            auto agLabel = new QLabel("Font size");
            auto agFont = agLabel->font();
            agFont.setBold(true);
            agLabel->setFont(agFont);
            agLabel->setObjectName("h7");
            aglayout->addWidget(agLabel);
            aglayout->addWidget(d->axisGridSizeSpin);

            d->axisGridSize->setDefaultWidget(d->axisGridSizeWidget);
            //d->mainMenu->addAction(d->axisGridSize);                        
        }
        // add timestamp check box
        d->timestampCheckBox = new QCheckBox(d->mainMenu);
        d->timestampCheckBox->setText("Show TimeStamp");
        connect(d->timestampCheckBox, &QCheckBox::stateChanged,
            [=](int state) { emit timestampVisibilityChanged(state == Qt::Checked); });

        auto timestampAction = new QWidgetAction(d->mainMenu);
        timestampAction->setDefaultWidget(d->timestampCheckBox);

        d->timestampSubmenu = new QMenu(d->mainMenu);
        d->timestampSubmenu->setTitle("TimeStamp");

        d->timestampSubmenu->addAction(timestampAction);

        d->timestampSeparator = d->timestampSubmenu->addSeparator();

        //QIcon textIcon()
        QIcon texIcon(d->timePixmap);
        d->timestampColor = new QAction(texIcon, "Change Color");
        d->timestampSubmenu->addAction(d->timestampColor);

        d->timestampSize = new QWidgetAction(d->mainMenu);
        d->timestampSizeSpin = new QSpinBox(d->mainMenu);
        d->timestampSizeSpin->setRange(10, 40);
        d->timestampSizeSpin->setValue(20);

        connect(d->timestampSizeSpin, SIGNAL(valueChanged(int)), this, SLOT(onTimeStampSizeChanged(int)));
        connect(d->timestampColor, SIGNAL(triggered(bool)), this, SLOT(onTimeStampColorClicked(bool)));

        d->timestampSizeWidget = new QWidget(d->mainMenu);
        auto hlayout = new QHBoxLayout;
        d->timestampSizeWidget->setLayout(hlayout);
        auto label = new QLabel("Font Size ");
        auto font = label->font();
        font.setBold(true);
        label->setFont(font);
        label->setObjectName("h7");
        hlayout->addWidget(label);
        hlayout->addWidget(d->timestampSizeSpin);

        d->timestampSize->setDefaultWidget(d->timestampSizeWidget);

        //d->timestampSubmenu->addAction(d->timestampSize);

        //d->timestampSubmenu->removeAction(d->timestampSeparator);
        //d->timestampSubmenu->removeAction(d->timestampColor);
        //d->timestampSubmenu->removeAction(d->timestampSize);

        d->mainMenu->addMenu(d->timestampSubmenu);

        /*//add resolution control
        d->resolutionSize = new QWidgetAction(d->mainMenu);
        d->resolutionSpin = new QSpinBox(d->mainMenu);
        d->resolutionSpin->setRange(-1, 0);
        d->resolutionSpin->setValue(-1);
        connect(d->resolutionSpin, SIGNAL(valueChanged(int)), this, SLOT(onResolutionChanged(int)));*/

        /*d->resolutionWidget = new QWidget(d->mainMenu);
        auto resolayout = new QHBoxLayout;
        d->resolutionWidget->setLayout(resolayout);
        auto resolabel = new QLabel("Resolution");
        auto resofont = resolabel->font();
        resofont.setBold(true);
        resolabel->setFont(resofont);
        resolabel->setObjectName("h7");
        resolayout->addWidget(resolabel);
        resolayout->addWidget(d->resolutionSpin);

        d->resolutionSize->setDefaultWidget(d->resolutionWidget);
        d->mainMenu->addAction(d->resolutionSize);*/                
    }

    auto ViewingToolPanel::Update()->bool {        
        const auto ds = GetViewingToolDS();
        if (!ds) {
            return false;
        }

        blockSignals(true);

        //auto menu = d->ui->viewToolButton->menu();

        auto layoutText = layoutMap[ds->layoutType];        
        for (auto l : d->layoutButtons->buttons()) {
            if(l->text().compare(layoutText)==0) {
                auto r = static_cast<QRadioButton*>(l);
                r->setChecked(true);
            }            
        }

        /*if (ds->changeRes) {
            d->resolutionSpin->setRange(-1, ds->maxRes);
            ds->changeRes = false;
            d->resolutionSpin->setValue(-1);            
        }*/

        if (d->is3D) {
            d->markerCheckBox->setChecked(ds->showOrientationMarker);
            d->boundaryBoxCheckBox->setChecked(ds->showBoundaryBox);

            if (ds->showAxisGrid) {
                d->axisGridCheckBox->setChecked(true);
                //d->mainMenu->insertAction(d->resolutionSize, d->axisGridSize);
                d->axisGridSubmenu->addAction(d->axisGridSize);
                d->axisGridSubmenu->addAction(d->axisGridColorX);
                d->axisGridSubmenu->addAction(d->axisGridColorY);
                d->axisGridSubmenu->addAction(d->axisGridColorZ);
            }
            else {
                d->axisGridCheckBox->setChecked(false);
                //d->mainMenu->removeAction(d->axisGridSize);
                d->axisGridSubmenu->removeAction(d->axisGridSize);
                d->axisGridSubmenu->removeAction(d->axisGridColorX);
                d->axisGridSubmenu->removeAction(d->axisGridColorY);
                d->axisGridSubmenu->removeAction(d->axisGridColorZ);
            }
            for (auto i = 0; i < 3; i++) {
                d->axisColor[0][i] = ds->axisXColor[i];
                d->axisColor[1][i] = ds->axisYColor[i];
                d->axisColor[2][i] = ds->axisZColor[i];
            }
        }
        if(ds->showTimeStamp) {
            d->timestampCheckBox->setChecked(true);
            d->timestampSubmenu->addAction(d->timestampSeparator);
            d->timestampSubmenu->addAction(d->timestampColor);
            d->timestampSubmenu->addAction(d->timestampSize);
        }else {
            d->timestampCheckBox->setChecked(false);
            d->timestampSubmenu->removeAction(d->timestampSeparator);
            d->timestampSubmenu->removeAction(d->timestampColor);
            d->timestampSubmenu->removeAction(d->timestampSize);
        }

        for(auto i=0;i<3;i++) {
            d->color[i] = ds->timestampColor[i];
        }
        
        blockSignals(false);
        return true;
    }

    auto ViewingToolPanel::Init(void) const ->bool {
        return true;
    }

    auto ViewingToolPanel::Reset(void) ->bool {
        blockSignals(true);

        auto ds = GetViewingToolDS();
        ds->Clear();

        if (d->is3D) {
            d->axisGridCheckBox->setChecked(false);
            d->boundaryBoxCheckBox->setChecked(false);
            d->markerCheckBox->setChecked(false);
        }
        d->timestampCheckBox->setChecked(false);
        d->timestampSizeSpin->setValue(20);
        
        for(auto i=0;i<3;i++) {
            d->color[i] = 255;
        }
        QColor white_col = QColor(255, 255, 255);
        /*QColor red_col = QColor(255, 0, 0);
        QColor green_col = QColor(0, 255, 0);
        QColor blue_col = QColor(0, 0, 255);
                
        d->axisColor[0][0] = 0;
        d->axisColor[0][1] = 255;
        d->axisColor[0][2] = 0;
        d->axisColor[1][0] = 0;
        d->axisColor[1][1] = 0;
        d->axisColor[1][2] = 255;
        d->axisColor[2][0] = 255;
        d->axisColor[2][1] = 0;
        d->axisColor[2][2] = 0;

        d->axisPixmap[0].fill(green_col);
        d->axisPixmap[1].fill(blue_col);
        d->axisPixmap[2].fill(red_col);

        d->axisGridColorX->setIcon(QIcon(d->axisPixmap[0]));
        d->axisGridColorY->setIcon(QIcon(d->axisPixmap[1]));
        d->axisGridColorZ->setIcon(QIcon(d->axisPixmap[2]));*/
        d->timePixmap.fill(white_col);
        d->timestampColor->setIcon(QIcon(d->timePixmap));

        auto layoutText = layoutMap[Entity::LayoutType::HSlicesBy3D];
        for (auto l : d->layoutButtons->buttons()) {
            if (l->text().compare(layoutText) == 0) {
                auto r = static_cast<QRadioButton*>(l);// ->clicked(true);
                r->setChecked(true);
            }
        }

        blockSignals(false);
        return true;
    }

    void ViewingToolPanel::onLayoutToolClicked(bool checked) {
        if (checked) {
            d->ui->layoutToolButton->showMenu();
        }
        else {
            auto menu = d->ui->layoutToolButton->menu();
            menu->hide();
        }
    }

    void ViewingToolPanel::onLayoutChanged(QAbstractButton* button) {
        emit layoutChanged(Entity::LayoutType::_from_integral(d->layoutButtons->id(button)));
        d->cur_3d_type = Entity::LayoutType::_from_integral(d->layoutButtons->id(button));
    }

    void ViewingToolPanel::onResetLayoutTriggered() {
        const auto answer = QMessageBox::question(
            nullptr,
            "Reset layout",
            "Are you sure you want to reset layout?",
            QMessageBox::Yes | QMessageBox::No
        );

        if (QMessageBox::No == answer) {
            return;
        }

        emit resetLayoutTriggered();
    }


    void ViewingToolPanel::onSaveLayoutTriggered() {
        const auto prevPath = QSettings("Tomocube", "TomoAnalysis").value("recentSaveLayout").toString();

        const QString fileName = QFileDialog::getSaveFileName(
            nullptr,
            tr("Save layout file"),
            prevPath,
            tr("Layout file (*.layout)")
        );

        if (fileName.isEmpty()) {
            return;
        }

        QSettings("Tomocube", "TomoAnalysis").setValue("recentSaveLayout", fileName);

        emit saveLayoutTriggered(fileName);
    }

    void ViewingToolPanel::onLoadLayoutTriggered() {
        const auto prevPath = QSettings("Tomocube", "TomoAnalysis").value("recentLoadLayout").toString();

        const auto fileName = QFileDialog::getOpenFileName(
            nullptr,
            "Select a layout file.",
            prevPath,
             "*.layout"
        );

        if (fileName.isEmpty()) {
            return;
        }

        QSettings("Tomocube", "TomoAnalysis").setValue("recentLoadLayout", fileName);

        emit loadLayoutTriggered(fileName);
    }

    void ViewingToolPanel::onViewToolClicked(bool checked) {
        if (checked) {
            d->ui->viewToolButton->showMenu();
        } else {
            auto menu = d->ui->viewToolButton->menu();
            menu->hide();
        }
    }

    void ViewingToolPanel::onTimeStampColorClicked(bool not_used) {
        Q_UNUSED(not_used)
        QColor new_col = QColorDialog::getColor(QColor(d->color[0],d->color[1],d->color[2],255), nullptr, "Select Color");
        if(new_col.isValid()) {
            emit timestampColorChanged(new_col);
            d->timePixmap.fill(new_col);
            d->timestampColor->setIcon(QIcon(d->timePixmap));
        }
    }

    void ViewingToolPanel::onTimeStampSizeChanged(int size) {        
        emit timestampSizeChanged(size);
    }

    void ViewingToolPanel::onSliceResolutionChanged(int idx) {
        int res = idx - 1;        
        emit sliceResolutionChanged(res);
    }

    void ViewingToolPanel::onVolumeResolutionChanged(int idx) {
        int res = idx -1;
        emit volumeResolutionChanged(res);
    }

    void ViewingToolPanel::onAxisGridSizeChanged(int size) {
        emit axisGridFontSize(size);
    }
    void ViewingToolPanel::onAxisGridColor() {
        auto sendAction = qobject_cast<QAction*>(sender());        
        if(sendAction) {
            auto axis = -1;
            auto sendTxt = sendAction->text();
            if(sendTxt == "Color (X)") {
                axis = 0;
            }
            else if (sendTxt == "Color (Y)") {
                axis = 1;
            }else if(sendTxt == "Color (Z)") {
                axis = 2;
            }

            if(axis < 0) {
                return;
            }

            QColor new_col = QColorDialog::getColor(QColor(d->axisColor[axis][0], d->axisColor[axis][1], d->axisColor[axis][2], 255), nullptr, QString("Select %1").arg(sendTxt));
            if (new_col.isValid()) {
                d->axisPixmap[axis].fill(new_col);
                if(axis == 0) {
                    d->axisGridColorX->setIcon(QIcon(d->axisPixmap[axis]));
                }else if(axis == 1) {
                    d->axisGridColorY->setIcon(QIcon(d->axisPixmap[axis]));
                }else {
                    d->axisGridColorZ->setIcon(QIcon(d->axisPixmap[axis]));
                }
                d->axisColor[axis][0] = new_col.red();
                d->axisColor[axis][1] = new_col.green();
                d->axisColor[axis][2] = new_col.blue();
                emit axisGridColor(axis, new_col);
            }
        }
    }

}
