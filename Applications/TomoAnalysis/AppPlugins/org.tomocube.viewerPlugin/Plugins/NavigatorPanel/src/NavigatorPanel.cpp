#include <iostream>

#include <UIUtility.h>

#include "ui_NavigatorPanel.h"
#include "NavigatorPanel.h"

namespace  TomoAnalysis::Viewer::Plugins {
    struct NavigatorPanel::Impl {
        Ui::NavigatorPanel* ui{ nullptr };

        Interactor::NavigatorDS::Pointer navigatorDS;

        float resolutionX = 0.f;
        float resolutionY = 0.f;
        float resolutionZ = 0.f;

        Entity::Modality prevModality = Entity::Modality::None;
    };

    NavigatorPanel::NavigatorPanel(QWidget* parent)
    : QWidget(parent), d{ new Impl } {
        d->ui = new Ui::NavigatorPanel();
        d->ui->setupUi(this);

        // set object names
        d->ui->contentsWidget->setObjectName("panel-contents");
        d->ui->locationLabel->setObjectName("h8");
        d->ui->locationUnitlabel->setObjectName("label-unit");
        d->ui->xPhyLabel->setObjectName("h9");
        d->ui->yPhyLabel->setObjectName("h9");
        d->ui->zPhyLabel->setObjectName("h9");
        d->ui->zAxisLabel->setObjectName("h9");
        d->ui->xAxisLabel->setObjectName("h9");
        d->ui->yAxisLabel->setObjectName("h9");

        d->ui->xAxisSpinBox->setStyle(new MyProxyStyle3d);
        d->ui->yAxisSpinBox->setStyle(new MyProxyStyle3d);
        d->ui->zAxisSpinBox->setStyle(new MyProxyStyle3d);

        d->ui->xPhySpinBox->setStyle(new MyProxyStyle3d);
        d->ui->yPhySpinBox->setStyle(new MyProxyStyle3d);
        d->ui->zPhySpinBox->setStyle(new MyProxyStyle3d);                
    }

    NavigatorPanel::~NavigatorPanel() {
        delete d->ui;
    }

    auto NavigatorPanel::Update()->bool {
        auto ds = GetNavigatorDS();

        d->navigatorDS = ds;

        return true;
    }

    auto NavigatorPanel::Refresh()->bool {
        auto ds = GetNavigatorDS();
        d->navigatorDS = ds;

        TC::SilentCall(d->ui->xAxisSlider)->setValue(ds->x);
        TC::SilentCall(d->ui->xAxisSpinBox)->setValue(ds->x);

        TC::SilentCall(d->ui->yAxisSlider)->setValue(ds->y);
        TC::SilentCall(d->ui->yAxisSpinBox)->setValue(ds->y);

        TC::SilentCall(d->ui->zAxisSlider)->setValue(ds->z);
        TC::SilentCall(d->ui->zAxisSpinBox)->setValue(ds->z);

        TC::SilentCall(d->ui->xPhySpinBox)->setValue(ds->phyX);
        TC::SilentCall(d->ui->yPhySpinBox)->setValue(ds->phyY);
        TC::SilentCall(d->ui->zPhySpinBox)->setValue(ds->phyZ);

        return true;
    }

    auto NavigatorPanel::Init(void) const ->bool {
        this->SetEnableUI(false);

        return true;
    }

    auto NavigatorPanel::Reset() const -> bool {
        return true;
    }


    void NavigatorPanel::on_xPhySpinBox_valueChanged(double value) {
        Q_UNUSED(value)
        this->NotifyChangedPhySpinBox();
    }

    void NavigatorPanel::on_yPhySpinBox_valueChanged(double value) {
        Q_UNUSED(value)
        this->NotifyChangedPhySpinBox();
    }

    void NavigatorPanel::on_zPhySpinBox_valueChanged(double value) {
        Q_UNUSED(value)
        this->NotifyChangedPhySpinBox();
    }

    void NavigatorPanel::on_xAxisSlider_valueChanged(int value) {
        Q_UNUSED(value)
        this->NotifyChangedAxisSlider();
    }

    void NavigatorPanel::on_xAxisSpinBox_valueChanged(int value) {
        Q_UNUSED(value)
        this->NotifyChangedAxisSpinBox();
    }

    void NavigatorPanel::on_yAxisSlider_valueChanged(int value) {
        Q_UNUSED(value)
        this->NotifyChangedAxisSlider();
    }

    void NavigatorPanel::on_yAxisSpinBox_valueChanged(int value) {
        Q_UNUSED(value)
        this->NotifyChangedAxisSpinBox();
    }

    void NavigatorPanel::on_zAxisSlider_valueChanged(int value) {
        Q_UNUSED(value)
        this->NotifyChangedAxisSlider();
    }

    void NavigatorPanel::on_zAxisSpinBox_valueChanged(int value) {
        Q_UNUSED(value)
        this->NotifyChangedAxisSpinBox();
    }

    void NavigatorPanel::onCurrentModalityChanged(Entity::Modality modality) {
        if((d->prevModality & Entity::Modality::None) != Entity::Modality::None){
            return;
        }

        Interactor::NavigatorInfo::Pointer info;
        
        if (((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
            (modality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume)) { 
            // volume
            if((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
                info = d->navigatorDS->list[Entity::HT3D];
            }
            else {
                info = d->navigatorDS->list[Entity::FL3D];
            }

        }
        else if (((modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP ||
                (modality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP)) {
            // mip
            if ((modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
                info = d->navigatorDS->list[Entity::HT2DMIP];
            }
            else {
                info = d->navigatorDS->list[Entity::FL2DMIP];
            }
        }

        if(nullptr ==  info.get()) {
            return;
        }

        this->SetEnableUI(true);

        TC::SilentCall(d->ui->xAxisSlider)->setRange(1, info->width);
        TC::SilentCall(d->ui->xAxisSpinBox)->setRange(1, info->width);

        TC::SilentCall(d->ui->xAxisSlider)->setValue(info->width / 2);
        TC::SilentCall(d->ui->xAxisSpinBox)->setValue(info->width / 2);

        TC::SilentCall(d->ui->yAxisSlider)->setRange(1, info->height);
        TC::SilentCall(d->ui->yAxisSpinBox)->setRange(1, info->height);

        TC::SilentCall(d->ui->yAxisSlider)->setValue(info->height / 2);
        TC::SilentCall(d->ui->yAxisSpinBox)->setValue(info->height / 2);

        TC::SilentCall(d->ui->zAxisSlider)->setRange(1, info->depth);
        TC::SilentCall(d->ui->zAxisSpinBox)->setRange(1, info->depth);

        TC::SilentCall(d->ui->zAxisSlider)->setValue(info->depth / 2);
        TC::SilentCall(d->ui->zAxisSpinBox)->setValue(info->depth / 2);

        TC::SilentCall(d->ui->xPhySpinBox)->setRange(info->resolutionX, (float)info->width * info->resolutionX);
        TC::SilentCall(d->ui->yPhySpinBox)->setRange(info->resolutionY, (float)info->height * info->resolutionY);
        TC::SilentCall(d->ui->zPhySpinBox)->setRange(info->resolutionZ, (float)info->depth * info->resolutionZ);

        TC::SilentCall(d->ui->xPhySpinBox)->setSingleStep(info->resolutionX);
        TC::SilentCall(d->ui->yPhySpinBox)->setSingleStep(info->resolutionY);
        TC::SilentCall(d->ui->zPhySpinBox)->setSingleStep(info->resolutionZ);

        TC::SilentCall(d->ui->xPhySpinBox)->setValue((float)info->width * info->resolutionX / 2.f);
        TC::SilentCall(d->ui->yPhySpinBox)->setValue((float)info->height * info->resolutionY / 2.f);
        TC::SilentCall(d->ui->zPhySpinBox)->setValue((float)info->depth * info->resolutionZ / 2.f);

        d->resolutionX = info->resolutionX;
        d->resolutionY = info->resolutionY;
        d->resolutionZ = info->resolutionZ;

        this->NotifyChangedAxisSpinBox();
    }

    void NavigatorPanel::onPositionChanged(int axis, int value) {
        switch (axis) {
        case 0:
            d->ui->zAxisSlider->setValue(value);
            break;
        case 1:
            d->ui->xAxisSlider->setValue(value);
            break;
        case 2:
            d->ui->yAxisSlider->setValue(value);
            break;
        default: ;
        }
    }

    void NavigatorPanel::NotifyChangedAxisSpinBox() {        
        emit positionChanged(d->ui->xAxisSpinBox->value(), 
                             d->ui->yAxisSpinBox->value(), 
                             d->ui->zAxisSpinBox->value());
    }

    void NavigatorPanel::NotifyChangedAxisSlider() {
        emit positionChanged(d->ui->xAxisSlider->value(),
                             d->ui->yAxisSlider->value(),
                             d->ui->zAxisSlider->value());
    }

    void NavigatorPanel::NotifyChangedPhySpinBox() {
        emit phyPositionChanged(d->ui->xPhySpinBox->value(),
                                d->ui->yPhySpinBox->value(),
                                d->ui->zPhySpinBox->value());
    }

    auto NavigatorPanel::SetEnableUI(const bool& enable) const ->void {
        d->ui->xAxisLabel->setEnabled(enable);
        d->ui->xAxisSlider->setEnabled(enable);
        d->ui->xAxisSpinBox->setEnabled(enable);

        d->ui->yAxisLabel->setEnabled(enable);
        d->ui->yAxisSlider->setEnabled(enable);
        d->ui->yAxisSpinBox->setEnabled(enable);

        d->ui->zAxisLabel->setEnabled(enable);
        d->ui->zAxisSlider->setEnabled(enable);
        d->ui->zAxisSpinBox->setEnabled(enable);

        d->ui->xPhySpinBox->setEnabled(enable);
        d->ui->yPhySpinBox->setEnabled(enable);
        d->ui->zPhySpinBox->setEnabled(enable);
    }
}
