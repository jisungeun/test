#pragma once

#include <memory>
#include <QWidget>
#include <QSlider>
#include <QWheelEvent>
#include <QProxyStyle>
#include <INavigatorPanel.h>

#include "TomoAnalysisNavigatorPanelExport.h"

class TcCustomSlider3d : public QSlider
{
public:
    TcCustomSlider3d(QWidget* parent = nullptr) : QSlider(parent) {}

protected:
    void wheelEvent(QWheelEvent* event) override
    {
        int num_degrees = event->delta() / 8;
        int num_steps = num_degrees / 15;  // Each step corresponds to 15 degrees


        setValue(value() + num_steps * singleStep());

        event->accept();
    }
};

class MyProxyStyle3d : public QProxyStyle
{
public:
    int styleHint(StyleHint hint, const QStyleOption* option, const QWidget* widget, QStyleHintReturn* returnData) const override
    {
        if (hint == QStyle::SH_SpinBox_ClickAutoRepeatRate) {
            return 10000;
        }
        if (hint == SH_SpinBox_ClickAutoRepeatRate) {
            return 10000;
        }
        if (hint == SH_SpinBox_ClickAutoRepeatThreshold) {
            return 10000;
        }
        return QProxyStyle::styleHint(hint, option, widget, returnData);
    }
};

namespace TomoAnalysis::Viewer::Plugins {
    class TomoAnalysisNavigatorPanel_API NavigatorPanel : public QWidget, public Interactor::INavigatorPanel {
        Q_OBJECT
        
    public:
        typedef NavigatorPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        NavigatorPanel(QWidget* parent=nullptr);
        ~NavigatorPanel();

        auto Update()->bool override;
        auto Refresh()->bool override;        

        auto Init(void) const ->bool;
        auto Reset(void)const ->bool;

    signals:
        void positionChanged(int x, int y, int z);
        void phyPositionChanged(float x, float y, float z);

    protected slots:
        void on_xPhySpinBox_valueChanged(double value);
        void on_yPhySpinBox_valueChanged(double value);
        void on_zPhySpinBox_valueChanged(double value);

        void on_xAxisSlider_valueChanged(int value);
        void on_xAxisSpinBox_valueChanged(int value);
        void on_yAxisSlider_valueChanged(int value);
        void on_yAxisSpinBox_valueChanged(int value);
        void on_zAxisSlider_valueChanged(int value);
        void on_zAxisSpinBox_valueChanged(int value);

        void onCurrentModalityChanged(Entity::Modality modality);

        void onPositionChanged(int axis, int value);

    private:
        void NotifyChangedAxisSpinBox();
        void NotifyChangedAxisSlider();
        void NotifyChangedPhySpinBox();

        auto SetEnableUI(const bool& enable) const ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}