#include <iostream>

#include <Hdf5Utilities.h>

#include <QColorDialog>
#include <UIUtility.h>

#include <H5Cpp.h>
#include <HDF5Mutex.h>

#include <TCFMetaReader.h>
#include <TcfH5GroupRoiLdmDataReader.h>

#include "RenameChannel.h"
#include "ui_FLChannelPanel.h"
#include "FLChannelPanel.h"

namespace  TomoAnalysis::Viewer::Plugins {
    struct FLChannelPanel::Impl {
        Ui::FLChannelPanel* ui{ nullptr };

        int curMin[3]{0,0,0};
        int curMax[3]{255,255,255};
        float chOpacity[3]{ 0,0,0 };
        float chGamma[3]{ 1,1,1 };
        auto ReadOriginalData(int ch, H5::Group flGroup)->QList<int>;
        auto ReadOriginalDataLdm(int ch, H5::Group flGroup)->QList<int>;
        auto ReadOriginalData3D(int ch, H5::Group flGroup)->QList<int>;
        auto ReadOriginalData3DLdm(int ch, H5::Group flGroup)->QList<int>;
        auto CalculateChHistogram(int steps, QList<int> original_data, int min_value)->QList<int>;
        auto CreateColorMap(QColor base_color, int steps, double opacity, double gamma = 1.0)->QList<QColor>;

        bool isWithHT{ true };
        TC::IO::TCFMetaReader::Meta::Pointer metaInfo{ nullptr };
    };

    auto FLChannelPanel::Impl::CreateColorMap(QColor base_color, int steps, double opacity, double gamma) -> QList<QColor> {
        QList<QColor> result;
        auto r = base_color.redF();
        auto g = base_color.greenF();
        auto b = base_color.blueF();
        for (auto i = 0; i < steps; i++) {
            QColor tmpColor;
            float factor = (float)i / static_cast<float>(steps) * opacity;
            float gammaFactor = pow(static_cast<float>(i) / static_cast<float>(steps), 1.0 / gamma) * opacity;

            auto mod_r = r * gammaFactor;
            auto mod_g = g * gammaFactor;
            auto mod_b = b * gammaFactor;

            if (mod_r > 1) mod_r = 1;
            if (mod_g > 1) mod_g = 1;
            if (mod_b > 1) mod_b = 1;

            tmpColor.setRedF(mod_r);
            tmpColor.setGreenF(mod_g);
            tmpColor.setBlueF(mod_b);

            tmpColor.setAlphaF(factor);

            result.append(tmpColor);
        }
        return result;
    }

    auto FLChannelPanel::Impl::CalculateChHistogram(int steps, QList<int> original_data, int min_value) -> QList<int> {
        auto result = QList<int>();
        for (auto i = 0; i < steps; i++) {
            result.append(0);
        }

        for (auto i = 0; i < original_data.count(); i++) {
            auto value = original_data[i] - min_value;
            if (value > -1 && result.count() > value) {
                result[value]++;
            }
        }

        for (auto i = 0; i < steps; i++) {
            if (result[i] > 0) {
                result[i] = static_cast<int>(log(result[i] + 1.5) / log(1.5));
            }
        }

        return result;
    }

    auto FLChannelPanel::Impl::ReadOriginalDataLdm(int ch, H5::Group flGroup) -> QList<int> {
        auto result = QList<int>();
        auto chName = QString("CH%1/000000").arg(ch).toStdString();
        auto dataGroup = flGroup.openGroup(chName);
        TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
        tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(dataGroup);
        const TC::IO::DataRange roi(TC::IO::Point(0, 0), TC::IO::Count(metaInfo->data.data2DFLMIP.sizeX, metaInfo->data.data2DFLMIP.sizeY));
        tcfH5GroupRoiLdmDataReader.SetReadingRoi(roi);
        auto base = 128;
        auto samplingstep = 0;
        const auto sizeX = metaInfo->data.data2DFLMIP.sizeX;
        while (sizeX > base) {
            base *= 2;
            samplingstep++;
        }
        samplingstep -= 2;
        if (samplingstep < 0) {
            samplingstep = 0;
        }
    	samplingstep = static_cast<int>(pow(2, samplingstep));        
        tcfH5GroupRoiLdmDataReader.SetSamplingStep(samplingstep);
                
        const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();

        if (metaInfo->data.data2DFLMIP.scalarType[ch][0] == 1) {
            auto result1 = memoryChunk->GetDataPointerAs<uint8_t>();

            const auto dimension = memoryChunk->GetDimension();

            for (auto i = 0; i < dimension.GetNumberOfElements(); i++) {
                auto val = static_cast<int>(metaInfo->data.data2DFLMIP.min[ch] + *(result1 + i));
                result.append(val);
            }
        }else {
            auto result1 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();

            for (auto i = 0; i < dimension.GetNumberOfElements(); i++) {
                result.append(*(result1 + i));
            }
        }
        dataGroup.close();
        return result;
    }

    auto FLChannelPanel::Impl::ReadOriginalData(int ch, H5::Group flGroup) -> QList<int> {        
        auto result = QList<int>();
        auto chName = QString("CH%1").arg(ch).toStdString();
        auto chGroup = flGroup.openGroup(chName);
        auto chDSet = chGroup.openDataSet("000000");
        auto datspace = chDSet.getSpace();
        hsize_t dims[3];
        datspace.getSimpleExtentDims(dims);
        unsigned long long numberOfElements;

        const auto dataSizeX = dims[1];
        const auto dataSizeY = dims[0];
        numberOfElements = dataSizeX * dataSizeY;

        if (metaInfo->data.data2DFLMIP.scalarType[ch][0] == 1) {
            const std::shared_ptr<uint8_t[]> rawData{ new uint8_t[numberOfElements] };
            chDSet.read(rawData.get(), chDSet.getDataType());
            for (auto i = 0; i < numberOfElements; i++) {
                result.append(rawData.get()[i]);
            }
        }
        else {
            const std::shared_ptr<uint16_t[]> rawData{ new uint16_t[numberOfElements] };
            chDSet.read(rawData.get(), chDSet.getDataType());
            for (auto i = 0; i < numberOfElements; i++) {
                result.append(rawData.get()[i]);
            }
        }

        datspace.close();
        chDSet.close();
        chGroup.close();
                
        return result;
    }

    auto FLChannelPanel::Impl::ReadOriginalData3DLdm(int ch, H5::Group flGroup) -> QList<int> {
        auto result = QList<int>();
        auto chName = QString("CH%1/000000").arg(ch).toStdString();
        auto dataGroup = flGroup.openGroup(chName);
        TC::IO::LdmReading::TcfH5GroupRoiLdmDataReader tcfH5GroupRoiLdmDataReader;
        tcfH5GroupRoiLdmDataReader.SetLdmDataGroup(dataGroup);
        const TC::IO::DataRange roi(TC::IO::Point(0, 0, metaInfo->data.data3DFL.sizeZ/2), TC::IO::Count(metaInfo->data.data3DFL.sizeX, metaInfo->data.data3DFL.sizeY, 1));
        tcfH5GroupRoiLdmDataReader.SetReadingRoi(roi);

        auto base = 128;
        auto samplingstep = 0;
        const auto sizeX = metaInfo->data.data3DFL.sizeX;
        while (sizeX > base) {
            base *= 2;
            samplingstep++;
        }
        samplingstep -= 2;
        if (samplingstep < 0) {
            samplingstep = 0;
        }
    	samplingstep = static_cast<int>(pow(2, samplingstep));

        tcfH5GroupRoiLdmDataReader.SetSamplingStep(samplingstep);

        const auto memoryChunk = tcfH5GroupRoiLdmDataReader.Read();

        if (metaInfo->data.data3DFL.scalarType[ch][0] == 1) {
            auto result1 = memoryChunk->GetDataPointerAs<uint8_t>();

            const auto dimension = memoryChunk->GetDimension();

            for (auto i = 0; i < dimension.GetNumberOfElements(); i++) {
                auto val = static_cast<int>(metaInfo->data.data3DFL.min[ch] + *(result1 + i));
                result.append(val);
            }
        }
        else {
            auto result1 = memoryChunk->GetDataPointerAs<uint16_t>();

            const auto dimension = memoryChunk->GetDimension();

            for (auto i = 0; i < dimension.GetNumberOfElements(); i++) {
                result.append(*(result1 + i));
            }
        }        
        dataGroup.close();
        return result;
    }

    auto FLChannelPanel::Impl::ReadOriginalData3D(int ch, H5::Group flGroup) -> QList<int> {        
        //read Z-center slice 
        auto result = QList<int>();
        auto chName = QString("CH%1").arg(ch).toStdString();
        auto chGroup = flGroup.openGroup(chName);
        auto chDSet = chGroup.openDataSet("000000");
        auto dataspace = chDSet.getSpace();
        hsize_t dims[3];
        dataspace.getSimpleExtentDims(dims);
        std::cout << dims[0] << " " << dims[1] << " " << dims[2] << std::endl;
        hsize_t tcfOffset[3] = { (hsize_t)(dims[0] / 2), (hsize_t)(0), (hsize_t)0 };
        hsize_t tcfCount[3] = { (hsize_t)1, dims[1], dims[2] };

        dataspace.selectHyperslab(H5S_SELECT_SET, tcfCount, tcfOffset);

        int out_rank = 2;
        const hsize_t out_dim[2] = { (hsize_t)(dims[1]), (hsize_t)(dims[2]) };

        H5::DataSpace memoryspace(out_rank, out_dim);

        hsize_t out_offset[2] = { 0, 0 };
        hsize_t out_count[2] = { (hsize_t)(dims[1]), (hsize_t)(dims[2]) };
        memoryspace.selectHyperslab(H5S_SELECT_SET, out_count, out_offset);

        unsigned long long numberOfElements = dims[1] * dims[2];

        if (metaInfo->data.data3DFL.scalarType[ch][0] == 1) {
            const std::shared_ptr<uint16_t[]> rawData{ new uint16_t[numberOfElements] };
            chDSet.read(rawData.get(), H5::PredType::NATIVE_UINT16, memoryspace, dataspace);
            for (auto i = 0; i < numberOfElements; i++) {
                result.append(rawData.get()[i]);
            }
        }
        else {
            const std::shared_ptr<uint8_t[]> rawData{ new uint8_t[numberOfElements] };
            chDSet.read(rawData.get(), H5::PredType::NATIVE_UINT8, memoryspace, dataspace);
            for (auto i = 0; i < numberOfElements; i++) {
                result.append(rawData.get()[i]);
            }
        }

        memoryspace.close();
        dataspace.close();
        chDSet.close();
        chGroup.close();

        return result;
    }

    FLChannelPanel::FLChannelPanel(QWidget* parent) : d{ new Impl } {        
        Q_UNUSED(parent)
        d->ui = new Ui::FLChannelPanel();
        d->ui->setupUi(this);

        // set object names
        d->ui->contentsWidget->setObjectName("panel-contents");
        d->ui->showButton->setObjectName("bt-square-gray");
        d->ui->hideButton->setObjectName("bt-square-gray");
        d->ui->ch1Label->setObjectName("h9");
        d->ui->ch2Label->setObjectName("h9");
        d->ui->ch3Label->setObjectName("h9");

        d->ui->ch1NameBtn->setObjectName("bt-round-gray700");
        d->ui->ch1ColorBtn->setObjectName("bt-round-gray700");
        d->ui->ch2NameBtn->setObjectName("bt-round-gray700");
        d->ui->ch2ColorBtn->setObjectName("bt-round-gray700");
        d->ui->ch3NameBtn->setObjectName("bt-round-gray700");
        d->ui->ch3ColorBtn->setObjectName("bt-round-gray700");

        d->ui->showButton->AddIcon(":/img/ic-show.svg");
        d->ui->showButton->AddIcon(":/img/ic-show-s.svg", QIcon::Active);
        d->ui->showButton->AddIcon(":/img/ic-show-s.svg", QIcon::Selected);

        d->ui->hideButton->AddIcon(":/img/ic-hide.svg");
        d->ui->hideButton->AddIcon(":/img/ic-hide-s.svg", QIcon::Active);
        d->ui->hideButton->AddIcon(":/img/ic-hide-s.svg", QIcon::Selected);

        d->ui->ch1CheckBox->setIconSize(QSize(10, 10));
        QPixmap ch1Pix(10, 10);
        ch1Pix.fill(QColor(0, 0, 255));
        d->ui->ch1CheckBox->setIcon(QIcon(ch1Pix));
        d->ui->ch2CheckBox->setIconSize(QSize(10, 10));
        QPixmap ch2Pix(10, 10);
        ch2Pix.fill(QColor(0, 255, 0));
        d->ui->ch2CheckBox->setIcon(QIcon(ch2Pix));
        d->ui->ch3CheckBox->setIconSize(QSize(10, 10));
        QPixmap ch3Pix(10, 10);
        ch3Pix.fill(QColor(255, 0, 0));
        d->ui->ch3CheckBox->setIcon(QIcon(ch3Pix));

        connect(d->ui->ch1Control, SIGNAL(sigOpacityChanged(double)), this, SLOT(OnCh1OpacityChanged(double)));
        connect(d->ui->ch2Control, SIGNAL(sigOpacityChanged(double)), this, SLOT(OnCh2OpacityChanged(double)));
        connect(d->ui->ch3Control, SIGNAL(sigOpacityChanged(double)), this, SLOT(OnCh3OpacityChanged(double)));
        connect(d->ui->ch1Control, SIGNAL(sigRangeChanged(double, double)), this, SLOT(OnCh1RangeChanged(double, double)));
        connect(d->ui->ch2Control, SIGNAL(sigRangeChanged(double, double)), this, SLOT(OnCh2RangeChanged(double, double)));
        connect(d->ui->ch3Control, SIGNAL(sigRangeChanged(double, double)), this, SLOT(OnCh3RangeChanged(double, double)));
        connect(d->ui->ch1Control, SIGNAL(sigApplyGamma(bool)), this, SLOT(OnToggleCh1Gamma(bool)));
        connect(d->ui->ch2Control, SIGNAL(sigApplyGamma(bool)), this, SLOT(OnToggleCh2Gamma(bool)));
        connect(d->ui->ch3Control, SIGNAL(sigApplyGamma(bool)), this, SLOT(OnToggleCh3Gamma(bool)));
        connect(d->ui->ch1Control, SIGNAL(sigGammaValue(double)), this, SLOT(OnCh1GammaValue(double)));
        connect(d->ui->ch2Control, SIGNAL(sigGammaValue(double)), this, SLOT(OnCh2GammaValue(double)));
        connect(d->ui->ch3Control, SIGNAL(sigGammaValue(double)), this, SLOT(OnCh3GammaValue(double)));

        connect(d->ui->ch1Control, SIGNAL(sigResetRange()), this, SLOT(OnCh1ResetRange()));
        connect(d->ui->ch2Control, SIGNAL(sigResetRange()), this, SLOT(OnCh2ResetRange()));
        connect(d->ui->ch3Control, SIGNAL(sigResetRange()), this, SLOT(OnCh3ResetRange()));

        connect(d->ui->ch1CheckBox, SIGNAL(stateChanged(int)), this, SLOT(OnCh1CheckBox(int)));
        connect(d->ui->ch2CheckBox, SIGNAL(stateChanged(int)), this, SLOT(OnCh2CheckBox(int)));
        connect(d->ui->ch3CheckBox, SIGNAL(stateChanged(int)), this, SLOT(OnCh3CheckBox(int)));

        connect(d->ui->showButton, SIGNAL(clicked()), this, SLOT(OnShowButton()));
        connect(d->ui->hideButton, SIGNAL(clicked()), this, SLOT(OnHideButton()));

        connect(d->ui->ch1NameBtn, SIGNAL(clicked()), this, SLOT(OnCh1NameBtn()));
        connect(d->ui->ch1ColorBtn, SIGNAL(clicked()), this, SLOT(OnCh1ColorBtn()));
        connect(d->ui->ch2NameBtn, SIGNAL(clicked()), this, SLOT(OnCh2NameBtn()));
        connect(d->ui->ch2ColorBtn, SIGNAL(clicked()), this, SLOT(OnCh2ColorBtn()));
        connect(d->ui->ch3NameBtn, SIGNAL(clicked()), this, SLOT(OnCh3NameBtn()));
        connect(d->ui->ch3ColorBtn, SIGNAL(clicked()), this, SLOT(OnCh3ColorBtn()));

        d->ui->ch1Control->installEventFilter(this);
        d->ui->ch2Control->installEventFilter(this);
        d->ui->ch3Control->installEventFilter(this);
        //this->installEventFilter(this);
    }

    FLChannelPanel::~FLChannelPanel() {
        delete d->ui;
    }    

    auto FLChannelPanel::SetTcfPath(const QString& path) -> void {
        auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
        d->metaInfo = metaReader->Read(path);
        bool chExist[3] = { false, };
        auto isMIP = false;
        QString groupName;
        if(d->metaInfo->data.data2DFLMIP.exist) {
            for(auto i=0;i<3;i++) {
                chExist[i] = d->metaInfo->data.data2DFLMIP.valid[i];
            }
            groupName = "2DFLMIP";
            isMIP = true;
        }else if (d->metaInfo->data.data3DFL.exist) {
            for (auto i = 0; i < 3; i++) {
                chExist[i] = d->metaInfo->data.data3DFL.valid[i];
            }
            groupName = "3DFL";
        }
        else {
            return;
        }

        QList<int> data[3];
        int minSignal[3];
        int maxSignal[3];
        TC::IO::HDF5MutexLocker lock(TC::IO::HDF5Mutex::GetInstance());
        using namespace H5;
        try {
            H5File file(path.toStdString(), H5F_ACC_RDONLY);
            auto dataGroup = file.openGroup("Data");
            auto flGroup = dataGroup.openGroup(groupName.toStdString());
            for (auto i = 0; i < 3; i++) {
                if (false == chExist[i]) {
                    continue;
                }                
                if (isMIP) {
                    if (d->metaInfo->data.isLDM) {
                        data[i] = d->ReadOriginalDataLdm(i, flGroup);
                    }
                    else {
                        data[i] = d->ReadOriginalData(i, flGroup);
                    }
                    minSignal[i] = static_cast<int>(round(d->metaInfo->data.data2DFLMIP.min[i]));
                    maxSignal[i] = static_cast<int>(round(d->metaInfo->data.data2DFLMIP.max[i]));
                }
                else {
                    if (d->metaInfo->data.isLDM) {
                        data[i] = d->ReadOriginalData3DLdm(i, flGroup);
                    }
                    else {
                        data[i] = d->ReadOriginalData3D(i, flGroup);
                    }
                    minSignal[i] = static_cast<int>(round(d->metaInfo->data.data3DFL.min[i]));
                    maxSignal[i] = static_cast<int>(round(d->metaInfo->data.data3DFL.max[i]));
                }                
            }
            flGroup.close();
            dataGroup.close();
            file.close();
        }catch(Exception& e) {
            std::cout << e.getDetailMsg() << std::endl;
            return;
        }
        for(auto i=0;i<3;i++) {
	        if(false == chExist[i]) {
                continue;
	        }
            const auto histogram_step = maxSignal[i] - minSignal[i] + 1;
            const auto histogram = d->CalculateChHistogram(static_cast<int>(histogram_step), data[i], minSignal[i]);
            if (i == 0) {
                d->ui->ch1Control->SetHistogram(QColor(Qt::lightGray), histogram);
            }
            else if (i == 1) {
                d->ui->ch2Control->SetHistogram(QColor(Qt::lightGray), histogram);
            }
            else {
                d->ui->ch3Control->SetHistogram(QColor(Qt::lightGray), histogram);
            }
        }
    }    
    bool FLChannelPanel::eventFilter(QObject* watched, QEvent* event) {
        if (event->type() == QEvent::Leave) {
            emit vizControlFocused(false);
        }
        if (event->type() == QEvent::Enter) {
            emit vizControlFocused(true);
        }
        //return false;//consume event - do not use except special cases
        return QObject::eventFilter(watched, event);
    }
    auto FLChannelPanel::Update()->bool {
        auto ds = GetFLMetaDS();

        //clear ui Text
        if (false == ds->isExistFL) {
            return true;
        }        
        this->SetEnableUI(true);        
                
        const auto ch1 = ds->flIntensity[0];
        this->SetCh1(ch1.isValid, ch1.min, ch1.max, ch1.opacity, ch1.isVisible);
        const auto ch2 = ds->flIntensity[1];
        this->SetCh2(ch2.isValid, ch2.min, ch2.max, ch2.opacity, ch2.isVisible);
        const auto ch3 = ds->flIntensity[2];
        this->SetCh3(ch3.isValid, ch3.min, ch3.max, ch3.opacity,ch3.isVisible);

        d->ui->ch1CheckBox->setText(ds->flName[0].c_str());
        QPixmap ch1Pix(10, 10);
        ch1Pix.fill(QColor(ds->color[0][0], ds->color[0][1], ds->color[0][2]));
        d->ui->ch1CheckBox->setIcon(QIcon(ch1Pix));

        d->ui->ch2CheckBox->setText(ds->flName[1].c_str());
        QPixmap ch2Pix(10, 10);
        ch2Pix.fill(QColor(ds->color[1][0], ds->color[1][1], ds->color[1][2]));
        d->ui->ch2CheckBox->setIcon(QIcon(ch2Pix));

        d->ui->ch3CheckBox->setText(ds->flName[2].c_str());
        QPixmap ch3Pix(10, 10);
        ch3Pix.fill(QColor(ds->color[2][0], ds->color[2][1], ds->color[2][2]));
        d->ui->ch3CheckBox->setIcon(QIcon(ch3Pix));        

        if (ch1.isValid) {
            d->chOpacity[0] = ds->flIntensity[0].opacity / 100.0;
            d->ui->ch1Control->SetColorMap(d->CreateColorMap(QColor(ds->color[0][0], ds->color[0][1], ds->color[0][2]), 256, d->chOpacity[0], d->chGamma[0]));
        }
        if (ch2.isValid) {
            d->chOpacity[1] = ds->flIntensity[1].opacity / 100.0;
            d->ui->ch2Control->SetColorMap(d->CreateColorMap(QColor(ds->color[1][0], ds->color[1][1], ds->color[1][2]), 256, d->chOpacity[1], d->chGamma[1]));
        }
        if (ch3.isValid) {
            d->chOpacity[2] = ds->flIntensity[2].opacity / 100.0;
            d->ui->ch3Control->SetColorMap(d->CreateColorMap(QColor(ds->color[2][0], ds->color[2][1], ds->color[2][2]), 256, d->chOpacity[2], d->chGamma[2]));
        }

        d->ui->ch1Label->setText(ds->flName[0].c_str());
        d->ui->ch2Label->setText(ds->flName[1].c_str());
        d->ui->ch3Label->setText(ds->flName[2].c_str());

        return true;
    }

    auto FLChannelPanel::Init(void) -> bool {	              
        const auto isEnable = false;
        TC::SilentCall(d->ui->ch1CheckBox)->setEnabled(isEnable);
        TC::SilentCall(d->ui->ch2CheckBox)->setEnabled(isEnable);
        TC::SilentCall(d->ui->ch3CheckBox)->setEnabled(isEnable);

        TC::SilentCall(d->ui->ch1ColorBtn)->setEnabled(isEnable);
        TC::SilentCall(d->ui->ch1NameBtn)->setEnabled(isEnable);
        TC::SilentCall(d->ui->ch2ColorBtn)->setEnabled(isEnable);
        TC::SilentCall(d->ui->ch2NameBtn)->setEnabled(isEnable);
        TC::SilentCall(d->ui->ch3ColorBtn)->setEnabled(isEnable);
        TC::SilentCall(d->ui->ch3NameBtn)->setEnabled(isEnable);

        d->ui->ch1Control->hide();
        d->ui->ch2Control->hide();
        d->ui->ch3Control->hide();
        d->ui->ch1Label->hide();
        d->ui->ch2Label->hide();
        d->ui->ch3Label->hide();

        d->ui->ch1Control->SetFixedHistogram(true);
        d->ui->ch1Control->SetFixedColorMap(true);
        d->ui->ch2Control->SetFixedHistogram(true);
        d->ui->ch2Control->SetFixedColorMap(true);
        d->ui->ch3Control->SetFixedHistogram(true);
        d->ui->ch3Control->SetFixedColorMap(true);

        d->ui->ch1Control->SetDecimals(0);
        d->ui->ch1Control->SetSingleStep(1);
        d->ui->ch2Control->SetDecimals(0);
        d->ui->ch2Control->SetSingleStep(1);
        d->ui->ch3Control->SetDecimals(0);
        d->ui->ch3Control->SetSingleStep(1);

        this->SetEnableUI(false);

        return true;
    }

    auto FLChannelPanel::Reset(void) ->bool {
        auto ds = GetFLMetaDS();
        ds.reset();

        for (auto i = 0; i < 3; i++) {
            d->chOpacity[i] = 0;
            d->chGamma[i] = 1;            
        }

        d->ui->ch1Control->setEnabled(false);
        d->ui->ch1Control->hide();
        d->ui->ch2Control->setEnabled(false);
        d->ui->ch2Control->hide();
        d->ui->ch3Control->setEnabled(false);
        d->ui->ch3Control->hide();
        d->ui->ch1Label->hide();
        d->ui->ch2Label->hide();
        d->ui->ch3Label->hide();
        d->ui->ch1Control->Clear();
        d->ui->ch2Control->Clear();
        d->ui->ch3Control->Clear();
                       
        TC::SilentCall(d->ui->ch1CheckBox)->setCheckState(Qt::Unchecked);
        TC::SilentCall(d->ui->ch1CheckBox)->setEnabled(false);        
        TC::SilentCall(d->ui->ch1ColorBtn)->setEnabled(false);
        TC::SilentCall(d->ui->ch1NameBtn)->setEnabled(false);

        TC::SilentCall(d->ui->ch2CheckBox)->setCheckState(Qt::Unchecked);
        TC::SilentCall(d->ui->ch2CheckBox)->setEnabled(false);        
        TC::SilentCall(d->ui->ch2ColorBtn)->setEnabled(false);
        TC::SilentCall(d->ui->ch2NameBtn)->setEnabled(false);
        
        TC::SilentCall(d->ui->ch3CheckBox)->setCheckState(Qt::Unchecked);
        TC::SilentCall(d->ui->ch3CheckBox)->setEnabled(false);        
        TC::SilentCall(d->ui->ch3ColorBtn)->setEnabled(false);
        TC::SilentCall(d->ui->ch3NameBtn)->setEnabled(false);
               
        d->ui->ch1CheckBox->setText("");
        d->ui->ch2CheckBox->setText("");
        d->ui->ch3CheckBox->setText("");

        d->ui->ch1Label->setText("");
        d->ui->ch2Label->setText("");
        d->ui->ch3Label->setText("");

        return true;
    }


    void FLChannelPanel::OnFLRangeChanged(const QString& txt) {
        auto line = qobject_cast<QLineEdit*>(sender());
        int pos = 0;
        QString copy = txt;
        auto state = line->validator()->validate(copy, pos);
        auto idx = line->whatsThis().toInt() % 10;
        auto isMax = line->whatsThis().toInt() / 10 > 2;
        if (state == QIntValidator::Acceptable) {            
            if(isMax) {
                d->curMax[idx] = copy.toInt();
            }else {
                d->curMin[idx] = copy.toInt();
            }
        }else {
            if(isMax) {
                TC::SilentCall(line)->setText(QString::number(d->curMax[idx]));
            }else {
                TC::SilentCall(line)->setText(QString::number(d->curMin[idx]));
            }
        }
    }

    auto FLChannelPanel::SetCh1(const bool& isValid, const int& min, const int& max, const int& opacity, const bool& isVisible)->void {
        auto ds = GetFLMetaDS();
        TC::SilentCall(d->ui->ch1CheckBox)->setEnabled(isValid && isVisible);
        TC::SilentCall(d->ui->ch1ColorBtn)->setEnabled(isValid && isVisible);
        TC::SilentCall(d->ui->ch1NameBtn)->setEnabled(isValid && isVisible);

        d->ui->ch1Control->blockSignals(true);
        d->ui->ch1Control->setEnabled(isValid && isVisible);
        if (true == isValid) {
            d->ui->ch1Control->SetDataRange(ds->flRange[0][0], ds->flRange[0][1], 1);
            d->ui->ch1Control->SetWindowRange(min, max);
            d->ui->ch1Control->SetOpacity(static_cast<double>(opacity) / 100.0);
        }
        d->ui->ch1Control->blockSignals(false);
        if (true == (isValid && isVisible)) {
            TC::SilentCall(d->ui->ch1CheckBox)->setCheckState(Qt::Checked);
        }
        else {
            TC::SilentCall(d->ui->ch1CheckBox)->setCheckState(Qt::Unchecked);
        }
    }

    auto FLChannelPanel::SetCh2(const bool& isValid, const int& min, const int& max, const int& opacity, const bool& isVisible) ->void {
        auto ds = GetFLMetaDS();
        TC::SilentCall(d->ui->ch2CheckBox)->setEnabled(isValid&&isVisible);
        TC::SilentCall(d->ui->ch2ColorBtn)->setEnabled(isValid && isVisible);
        TC::SilentCall(d->ui->ch2NameBtn)->setEnabled(isValid && isVisible);                
        d->ui->ch2Control->blockSignals(true);
        d->ui->ch2Control->setEnabled(isValid && isVisible);
        if (true == isValid) {
            d->ui->ch2Control->SetDataRange(ds->flRange[1][0], ds->flRange[1][1], 1);
            d->ui->ch2Control->SetWindowRange(min, max);
            d->ui->ch2Control->SetOpacity(static_cast<double>(opacity) / 100.0);
        }
        d->ui->ch2Control->blockSignals(false);
        if(true == (isValid && isVisible)) {
            TC::SilentCall(d->ui->ch2CheckBox)->setCheckState(Qt::Checked);
        }else {
            TC::SilentCall(d->ui->ch2CheckBox)->setCheckState(Qt::Unchecked);
        }
    }

    auto FLChannelPanel::SetCh3(const bool& isValid, const int& min, const int& max, const int& opacity, const bool& isVisible) ->void {
        auto ds = GetFLMetaDS();
        TC::SilentCall(d->ui->ch3CheckBox)->setEnabled(isValid && isVisible);
        TC::SilentCall(d->ui->ch3ColorBtn)->setEnabled(isValid && isVisible);
        TC::SilentCall(d->ui->ch3NameBtn)->setEnabled(isValid && isVisible);                
        d->ui->ch3Control->blockSignals(true);
        d->ui->ch3Control->setEnabled(isValid && isVisible);
        if (true == isValid) {
            d->ui->ch3Control->SetDataRange(ds->flRange[2][0], ds->flRange[2][1], 1);
            d->ui->ch3Control->SetWindowRange(min, max);
            d->ui->ch3Control->SetOpacity(static_cast<double>(opacity) / 100.0);
        }
        d->ui->ch3Control->blockSignals(false);
        if (true == (isValid && isVisible)) {
            TC::SilentCall(d->ui->ch3CheckBox)->setCheckState(Qt::Checked);
        }
        else {
            TC::SilentCall(d->ui->ch3CheckBox)->setCheckState(Qt::Unchecked);
        }
    }    

    auto FLChannelPanel::SetOpacity(const int& opacity,const int& ch) const ->void {
        if (ch == 0) {
            d->ui->ch1Control->SetOpacity(static_cast<double>(opacity) / 100.0);
        }
        else if (ch == 1) {
            d->ui->ch2Control->SetOpacity(static_cast<double>(opacity) / 100.0);
        }
        else if (ch == 2) {
            d->ui->ch3Control->SetOpacity(static_cast<double>(opacity) / 100.0);
        }
    }

    void FLChannelPanel::onActivatedModalityChanged(Entity::Modality modality) {
        auto ds = GetFLMetaDS();

        if (false == ds->isExistFL) {
            return;
        }

        auto ch1Col = ds->flIntensity[0];
        auto ch2Col = ds->flIntensity[1];
        auto ch3Col = ds->flIntensity[2];

        d->isWithHT = (modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
            (modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP;

        if ((modality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume ||
            (modality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
            //Turn on check box & enable them
            if(ch1Col.isValid) {                
                d->ui->ch1CheckBox->setCheckState(Qt::CheckState::Checked);
                d->ui->ch1Control->setEnabled(true);
                d->ui->ch1Control->show();
                d->ui->ch1Label->show();
                d->ui->ch1CheckBox->setEnabled(true);
                d->ui->ch1ColorBtn->setEnabled(true);
                d->ui->ch1NameBtn->setEnabled(true);
                ch1Col.isVisible = true;
                d->ui->ch1Control->SetFixedOpacity(!d->isWithHT);
                auto tempOpacity = 1.f;
                if (d->isWithHT) {
                    tempOpacity = d->chOpacity[0];
                }
                d->ui->ch1Control->SetColorMap(d->CreateColorMap(QColor(ds->color[0][0], ds->color[0][1], ds->color[0][2]), 256, tempOpacity, d->chGamma[0]));
                d->ui->ch1Control->SetOpacity(d->chOpacity[0]);
            }
            if(ch2Col.isValid) {
                d->ui->ch2CheckBox->setCheckState(Qt::CheckState::Checked);
                d->ui->ch2Control->setEnabled(true);
                d->ui->ch2Control->show();
                d->ui->ch2Label->show();
                d->ui->ch2CheckBox->setEnabled(true);
                d->ui->ch2ColorBtn->setEnabled(true);
                d->ui->ch2NameBtn->setEnabled(true);
                ch2Col.isVisible = true;
                d->ui->ch2Control->SetFixedOpacity(!d->isWithHT);
                auto tempOpacity = 1.f;
                if (d->isWithHT) {
                    tempOpacity = d->chOpacity[1];
                }
                d->ui->ch2Control->SetColorMap(d->CreateColorMap(QColor(ds->color[1][0], ds->color[1][1], ds->color[1][2]), 256, tempOpacity, d->chGamma[1]));
                d->ui->ch2Control->SetOpacity(d->chOpacity[1]);
            }
            if(ch3Col.isValid) {
                d->ui->ch3CheckBox->setCheckState(Qt::CheckState::Checked);
                d->ui->ch3Control->setEnabled(true);
                d->ui->ch3Control->show();
                d->ui->ch3Label->show();
                d->ui->ch3CheckBox->setEnabled(true);
                d->ui->ch3ColorBtn->setEnabled(true);
                d->ui->ch3NameBtn->setEnabled(true);
                ch3Col.isVisible = true;
                d->ui->ch3Control->SetFixedOpacity(!d->isWithHT);
                auto tempOpacity = 1.f;
                if (d->isWithHT) {
                    tempOpacity = d->chOpacity[2];
                }
                d->ui->ch3Control->SetColorMap(d->CreateColorMap(QColor(ds->color[2][0], ds->color[2][1], ds->color[2][2]), 256, tempOpacity, d->chGamma[2]));
                d->ui->ch3Control->SetOpacity(d->chOpacity[2]);
            }
        }else {
            //Turn off check box & disable them
            if(ch1Col.isValid) {
                d->ui->ch1CheckBox->setCheckState(Qt::CheckState::Unchecked);
                d->ui->ch1CheckBox->setEnabled(false);
                d->ui->ch1Control->setEnabled(false);
                d->ui->ch1Control->hide();
                d->ui->ch1Label->hide();
                d->ui->ch1ColorBtn->setEnabled(false);
                d->ui->ch1NameBtn->setEnabled(false);
                ch1Col.isVisible = false;
            }
            if(ch2Col.isValid) {
                d->ui->ch2CheckBox->setCheckState(Qt::CheckState::Unchecked);
                d->ui->ch2CheckBox->setEnabled(false);
                d->ui->ch2Control->setEnabled(false);
                d->ui->ch2Control->hide();
                d->ui->ch2Label->hide();
                d->ui->ch2ColorBtn->setEnabled(false);
                d->ui->ch2NameBtn->setEnabled(false);
                ch2Col.isVisible = false;
            }
            if(ch3Col.isValid) {
                d->ui->ch3CheckBox->setCheckState(Qt::CheckState::Unchecked);                
                d->ui->ch3CheckBox->setEnabled(false);
                d->ui->ch3Control->setEnabled(false);
                d->ui->ch3Control->hide();
                d->ui->ch3Label->hide();
                d->ui->ch3ColorBtn->setEnabled(false);
                d->ui->ch3NameBtn->setEnabled(false);
                ch3Col.isVisible = false;
            }
        }
    }
       
    void FLChannelPanel::OnCh1CheckBox(int state) {
        bool visible = true;
        if (Qt::Unchecked == state) {
            visible = false;
        }

        auto ds = GetFLMetaDS();
        ds->flIntensity[0].isVisible = visible;

        emit channelVisibleChanged(Entity::Channel::CH1, visible);
    }

    void FLChannelPanel::OnCh2CheckBox(int state) {
        bool visible = true;
        if (Qt::Unchecked == state) {
            visible = false;
        }
        auto ds = GetFLMetaDS();
        ds->flIntensity[1].isVisible = visible;

        emit channelVisibleChanged(Entity::Channel::CH2, visible);
    }

    void FLChannelPanel::OnCh3CheckBox(int state) {
        bool visible = true;
        if (Qt::Unchecked == state) {
            visible = false;
        }

        auto ds = GetFLMetaDS();
        ds->flIntensity[2].isVisible = visible;

        emit channelVisibleChanged(Entity::Channel::CH3, visible);
    }    

    void FLChannelPanel::OnShowButton() {
        if (d->ui->ch1CheckBox->isEnabled()) {
            TC::SilentCall(d->ui->ch1CheckBox)->setCheckState(Qt::Checked);
        }

        if (d->ui->ch2CheckBox->isEnabled()) {
            TC::SilentCall(d->ui->ch2CheckBox)->setCheckState(Qt::Checked);
        }

        if (d->ui->ch3CheckBox->isEnabled()) {
            TC::SilentCall(d->ui->ch3CheckBox)->setCheckState(Qt::Checked);
        }                

        emit channelVisibleChanged(Entity::Channel::All, true);
    }

    void FLChannelPanel::OnHideButton() {
        if(d->ui->ch1CheckBox->isEnabled()) {
            TC::SilentCall(d->ui->ch1CheckBox)->setCheckState(Qt::Unchecked);
        }

        if (d->ui->ch2CheckBox->isEnabled()) {
            TC::SilentCall(d->ui->ch2CheckBox)->setCheckState(Qt::Unchecked);
        }

        if (d->ui->ch3CheckBox->isEnabled()) {
            TC::SilentCall(d->ui->ch3CheckBox)->setCheckState(Qt::Unchecked);
        }

        emit channelVisibleChanged(Entity::Channel::All, false);
    }

    void FLChannelPanel::OnCh1OpacityChanged(double value) {
        emit channelOpacityChanged(Entity::Channel::CH1, static_cast<int>(value * 100.0));
        d->chOpacity[0] = value;
        auto ds = GetFLMetaDS();
        d->ui->ch1Control->SetColorMap(d->CreateColorMap(QColor(ds->color[0][0], ds->color[0][1], ds->color[0][2]), 256, value, d->chGamma[0]));
    }

    void FLChannelPanel::OnCh2OpacityChanged(double value) {
        emit channelOpacityChanged(Entity::Channel::CH2, static_cast<int>(value * 100.0));
        d->chOpacity[1] = value;
        auto ds = GetFLMetaDS();
        d->ui->ch2Control->SetColorMap(d->CreateColorMap(QColor(ds->color[1][0], ds->color[1][1], ds->color[1][2]), 256, value, d->chGamma[1]));
    }

    void FLChannelPanel::OnCh3OpacityChanged(double value) {
        emit channelOpacityChanged(Entity::Channel::CH3, static_cast<int>(value * 100.0));
        d->chOpacity[2] = value;
        auto ds = GetFLMetaDS();
        d->ui->ch3Control->SetColorMap(d->CreateColorMap(QColor(ds->color[2][0], ds->color[2][1], ds->color[2][2]), 256, value, d->chGamma[2]));
    }

    void FLChannelPanel::OnCh1RangeChanged(double min, double max) {
        emit channelRangeChanged(Entity::Channel::CH1, static_cast<int>(min), static_cast<int>(max));
    }
    void FLChannelPanel::OnCh2RangeChanged(double min, double max) {
        emit channelRangeChanged(Entity::Channel::CH2, static_cast<int>(min), static_cast<int>(max));
    }
    void FLChannelPanel::OnCh3RangeChanged(double min, double max) {
        emit channelRangeChanged(Entity::Channel::CH3, static_cast<int>(min), static_cast<int>(max));
    }

    void FLChannelPanel::OnCh1ResetRange() {
        auto ds = GetFLMetaDS();

        d->ui->ch1Control->SetWindowRange(ds->flIntensity[0].min, ds->flIntensity[0].max);
        emit channelRangeChanged(Entity::Channel::CH1, static_cast<int>(ds->flIntensity[0].min), static_cast<int>(ds->flIntensity[0].max));
    }

    void FLChannelPanel::OnCh2ResetRange() {
        auto ds = GetFLMetaDS();

        d->ui->ch2Control->SetWindowRange(ds->flIntensity[1].min, ds->flIntensity[1].max);
        emit channelRangeChanged(Entity::Channel::CH2, static_cast<int>(ds->flIntensity[1].min), static_cast<int>(ds->flIntensity[1].max));
    }

    void FLChannelPanel::OnCh3ResetRange() {
        auto ds = GetFLMetaDS();

        d->ui->ch3Control->SetWindowRange(ds->flIntensity[2].min, ds->flIntensity[2].max);
        emit channelRangeChanged(Entity::Channel::CH3, static_cast<int>(ds->flIntensity[2].min), static_cast<int>(ds->flIntensity[2].max));
    }
    
    void FLChannelPanel::OnCh1GammaValue(double gamma) {
        auto isEnable = d->ui->ch1Control->GetGammaEnabled();
        emit channelGammaChanged(Entity::Channel::CH1, isEnable, gamma);
        if (isEnable) {
            d->chGamma[0] = gamma;
        }
        else {
            d->chGamma[0] = 1;
        }
        auto ds = GetFLMetaDS();
        auto opacity = 1.f;
        if (d->isWithHT) {
            opacity = d->chOpacity[0];
        }
        d->ui->ch1Control->SetColorMap(d->CreateColorMap(QColor(ds->color[0][0], ds->color[0][1], ds->color[0][2]), 256, opacity, d->chGamma[0]));
    }

    void FLChannelPanel::OnCh2GammaValue(double gamma) {
        auto isEnable = d->ui->ch2Control->GetGammaEnabled();
        emit channelGammaChanged(Entity::Channel::CH2, isEnable, gamma);
        if (isEnable) {
            d->chGamma[1] = gamma;
        }
        else {
            d->chGamma[1] = 1;
        }
        auto ds = GetFLMetaDS();
        auto opacity = 1.f;
        if (d->isWithHT) {
            opacity = d->chOpacity[1];
        }
        d->ui->ch2Control->SetColorMap(d->CreateColorMap(QColor(ds->color[1][0], ds->color[1][1], ds->color[1][2]), 256, opacity, d->chGamma[1]));
    }

    void FLChannelPanel::OnCh3GammaValue(double gamma) {
        auto isEnable = d->ui->ch3Control->GetGammaEnabled();
        emit channelGammaChanged(Entity::Channel::CH3, isEnable, gamma);
        if (isEnable) {
            d->chGamma[2] = gamma;
        }
        else {
            d->chGamma[2] = 1;
        }
        auto ds = GetFLMetaDS();
        auto opacity = 1.f;
        if (d->isWithHT) {
            opacity = d->chOpacity[2];
        }
        d->ui->ch3Control->SetColorMap(d->CreateColorMap(QColor(ds->color[2][0], ds->color[2][1], ds->color[2][2]), 256, opacity, d->chGamma[2]));
    }

    void FLChannelPanel::OnToggleCh1Gamma(bool enabled) {
        auto value = d->ui->ch1Control->GetGammaValue();
        if (enabled) {
            d->chGamma[0] = value;
        }
        else {
            d->chGamma[0] = 1;
        }
        emit channelGammaChanged(Entity::Channel::CH1, enabled, value);
        auto ds = GetFLMetaDS();
        d->ui->ch1Control->SetColorMap(d->CreateColorMap(QColor(ds->color[0][0], ds->color[0][1], ds->color[0][2]), 256, d->chOpacity[0], d->chGamma[0]));
    }

    void FLChannelPanel::OnToggleCh2Gamma(bool enabled) {
        auto value = d->ui->ch2Control->GetGammaValue();
        if (enabled) {
            d->chGamma[1] = value;
        }
        else {
            d->chGamma[1] = 1;
        }
        emit channelGammaChanged(Entity::Channel::CH2, enabled, value);
        auto ds = GetFLMetaDS();
        d->ui->ch2Control->SetColorMap(d->CreateColorMap(QColor(ds->color[1][0], ds->color[1][1], ds->color[1][2]), 256, d->chOpacity[1], d->chGamma[1]));
    }

    void FLChannelPanel::OnToggleCh3Gamma(bool enabled) {
        auto value = d->ui->ch3Control->GetGammaValue();
        if (enabled) {
            d->chGamma[2] = value;
        }
        else {
            d->chGamma[2] = 1;
        }
        emit channelGammaChanged(Entity::Channel::CH3, enabled, value);
        auto ds = GetFLMetaDS();
        d->ui->ch3Control->SetColorMap(d->CreateColorMap(QColor(ds->color[2][0], ds->color[2][1], ds->color[2][2]), 256, d->chOpacity[2], d->chGamma[2]));
    }

    void FLChannelPanel::OnCh1NameBtn() {
        auto curName = d->ui->ch1CheckBox->text();
        auto newName = RenameChannel::Rename(nullptr, curName, "Channel",1);
        if (false == newName.isEmpty()) {
            auto ds = GetFLMetaDS();
            ds->flName[0] = newName.toStdString();
            d->ui->ch1CheckBox->setText(newName);
            d->ui->ch1Label->setText(newName);
        }
    }

    void FLChannelPanel::OnCh1ColorBtn() {
        auto ds = GetFLMetaDS();
        auto curR = ds->color[0][0];
        auto curG = ds->color[0][1];
        auto curB = ds->color[0][2];
        QColor new_col = QColorDialog::getColor(QColor(curR,curG,curB), nullptr, QString("Select Color of %1").arg(d->ui->ch1CheckBox->text()));
        if(new_col.isValid()) {
            ds->color[0][0] = new_col.red();
            ds->color[0][1] = new_col.green();
            ds->color[0][2] = new_col.blue();

            QPixmap pixmap(10, 10);
            pixmap.fill(new_col);
            d->ui->ch1CheckBox->setIcon(QIcon(pixmap));

            emit channelColorChanged(Entity::Channel::CH1, new_col);
            auto opacity = 1.f;
            if (d->isWithHT) {
                opacity = d->chOpacity[0];
            }
            d->ui->ch1Control->SetColorMap(d->CreateColorMap(new_col, 256, opacity, d->chGamma[0]));
        }
    }

    void FLChannelPanel::OnCh2NameBtn() {
        auto curName = d->ui->ch2CheckBox->text();
        auto newName = RenameChannel::Rename(nullptr, curName, "Channel", 2);
        if (false == newName.isEmpty()) {
            auto ds = GetFLMetaDS();
            ds->flName[1] = newName.toStdString();
            d->ui->ch2CheckBox->setText(newName);
            d->ui->ch2Label->setText(newName);
        }
    }

    void FLChannelPanel::OnCh2ColorBtn() {
        auto ds = GetFLMetaDS();
        auto curR = ds->color[1][0];
        auto curG = ds->color[1][1];
        auto curB = ds->color[1][2];
        QColor new_col = QColorDialog::getColor(QColor(curR, curG, curB), nullptr, QString("Select Color of %1").arg(d->ui->ch2CheckBox->text()));
        if (new_col.isValid()) {
            ds->color[1][0] = new_col.red();
            ds->color[1][1] = new_col.green();
            ds->color[1][2] = new_col.blue();

            QPixmap pixmap(10, 10);
            pixmap.fill(new_col);
            d->ui->ch2CheckBox->setIcon(QIcon(pixmap));

            emit channelColorChanged(Entity::Channel::CH2, new_col);
            auto opacity = 1.f;
            if (d->isWithHT) {
                opacity = d->chOpacity[1];
            }
            d->ui->ch2Control->SetColorMap(d->CreateColorMap(new_col, 256, opacity, d->chGamma[1]));
        }
    }

    void FLChannelPanel::OnCh3NameBtn() {
        auto curName = d->ui->ch3CheckBox->text();
        auto newName = RenameChannel::Rename(nullptr, curName, "Channel", 3);
        if (false == newName.isEmpty()) {
            auto ds = GetFLMetaDS();
            ds->flName[2] = newName.toStdString();
            d->ui->ch3CheckBox->setText(newName);
            d->ui->ch3Label->setText(newName);
        }
    }

    void FLChannelPanel::OnCh3ColorBtn() {
        auto ds = GetFLMetaDS();
        auto curR = ds->color[2][0];
        auto curG = ds->color[2][1];
        auto curB = ds->color[2][2];
        QColor new_col = QColorDialog::getColor(QColor(curR, curG, curB), nullptr, QString("Select Color of %1").arg(d->ui->ch3CheckBox->text()));
        if (new_col.isValid()) {
            ds->color[2][0] = new_col.red();
            ds->color[2][1] = new_col.green();
            ds->color[2][2] = new_col.blue();

            QPixmap pixmap(10, 10);
            pixmap.fill(new_col);
            d->ui->ch3CheckBox->setIcon(QIcon(pixmap));

            emit channelColorChanged(Entity::Channel::CH3, new_col);
            auto opacity = 1.f;
            if (d->isWithHT) {
                opacity = d->chOpacity[2];
            }
            d->ui->ch3Control->SetColorMap(d->CreateColorMap(new_col, 256, opacity, d->chGamma[2]));
        }
    }
    
    auto FLChannelPanel::SetEnableUI(const bool& enable) const -> void {
        TC::SilentCall(d->ui->ch1CheckBox)->setEnabled(enable);        
        TC::SilentCall(d->ui->ch2CheckBox)->setEnabled(enable);
        TC::SilentCall(d->ui->ch3CheckBox)->setEnabled(enable);

        TC::SilentCall(d->ui->showButton)->setEnabled(enable);
        TC::SilentCall(d->ui->hideButton)->setEnabled(enable);

        d->ui->ch1Control->blockSignals(true);
        d->ui->ch1Control->setEnabled(enable);
        d->ui->ch1Control->blockSignals(false);

        d->ui->ch2Control->blockSignals(true);
        d->ui->ch2Control->setEnabled(enable);
        d->ui->ch2Control->blockSignals(false);

        d->ui->ch3Control->blockSignals(true);
        d->ui->ch3Control->setEnabled(enable);
        d->ui->ch3Control->blockSignals(false);

        TC::SilentCall(d->ui->ch1NameBtn)->setEnabled(enable);
        TC::SilentCall(d->ui->ch1ColorBtn)->setEnabled(enable);
        TC::SilentCall(d->ui->ch2NameBtn)->setEnabled(enable);
        TC::SilentCall(d->ui->ch2ColorBtn)->setEnabled(enable);
        TC::SilentCall(d->ui->ch3NameBtn)->setEnabled(enable);
        TC::SilentCall(d->ui->ch3ColorBtn)->setEnabled(enable);        
    }
}
