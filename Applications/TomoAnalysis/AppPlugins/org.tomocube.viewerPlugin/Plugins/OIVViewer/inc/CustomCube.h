#pragma once

#include <memory>

#include <Inventor/ViewerComponents/nodes/SoViewingCube.h>

class CustomCube : public SoViewingCube {
public:
	CustomCube();

	void handleEvent(SoHandleEventAction* ha) override;

protected:
	~CustomCube() override;

private:
	struct Impl;
	std::unique_ptr<Impl> d;
};
