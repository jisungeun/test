#pragma once

#include <memory>

#include <QWidget>
#pragma warning(push)
#pragma warning(disable:4819)
#include <opencv2/opencv.hpp>
#include <Inventor/nodes/SoSeparator.h>
#pragma warning(pop)

#include <IImageViewer.h>

#include <ScreenShotWidget.h>
#include <ScreenShot.h>


#include <VideoRecorderWidget.h>

#include "TomoAnalysisViewerExport.h"

class OivBufferManager;

namespace TomoAnalysis::Viewer::Plugins {
	class tStepper : public QObject {
		Q_OBJECT
	public:
		explicit tStepper(QObject* parent = nullptr);
		virtual ~tStepper();

		auto SetBuffer(OivBufferManager* buf) -> void;

	public slots:
		void run(int);

	signals:
		void start(int);
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};

	struct tStepper::Impl {
		OivBufferManager* buf;
	};

	class TomoAnalysisViewer_API OIVViewer : public QWidget, public Interactor::IImageViewer {
		Q_OBJECT

	public:
		typedef OIVViewer Self;
		typedef std::shared_ptr<Self> Pointer;

		OIVViewer(SoNode* node, SoSeparator* node2d[3], QWidget* parent = nullptr);
		OIVViewer(QWidget* parent = nullptr);
		~OIVViewer();

		auto Update() -> bool override;
		auto Refresh() -> bool override;
		auto UpdateTransferFunction(const int& index,
									const Entity::TFItem::Pointer& item) -> bool override;
		auto UpdateChannelInfo(const Entity::FLChannelInfo& info) -> bool override;
		auto UpdateVisualizationData(const Entity::TFItemList& tfItemList,
									const Entity::FLChannelInfo& info) -> bool override;
		auto ClearTransferFunction() -> bool override;

		auto Init(void) -> bool;
		auto Reset(void) -> bool;

		auto RequestRender(void) -> void;

		auto SetRootNode(SoNode* node, SoSeparator* node2d[3]) const -> void;
		auto SetSceneID(const int& id) const -> void;
		auto SetTFWidget(QWidget* widget) const -> void;

		auto ResetLayout(const int& type = -1) -> void; // -1 = reset
		auto SaveLayout(const QString& path) const -> bool;
		auto LoadLayout(const QString& path) const -> bool;
		auto GetCurrentLayoutType(void) const -> int;

		//non-clean architecture
		auto SetCurrentProjectPath(const QString& path) -> void;
		auto SetDisplayType(bool is2D) -> void;
		auto SetTimePoints(std::vector<double>) -> void;
		auto ConnectScreenShotWidget(TC::ScreenShotWidget::Pointer widget) -> void;
		auto ConnectRecorderWidget(TC::VideoRecorderWidget::Pointer widget) -> void;

		auto SetCropInformation(int xOffset, int yOffset, int xSize, int ySize) -> void;
		auto SetFLCropInformation(int xOffset, int yOffset, int xSize, int ySize) -> void;

	signals:
		void usableModalityChanged(Entity::Modality modality);

		void gradientRangeLoaded(double min, double max);

		void transferFunctionChanged(Entity::TFItemList list);
		void currentTransferFunctionChanged(int index);

		void sliceIndexChanged(int viewIndex, int sliceIndex);
		void timeStepChanged(double step);

		void finishSimulate();
		void captureScreen(int);

		void moveSlice(int viewIndex, int sliceIndex);

		void timeStepPlayed(double);

	protected slots:
		void onCanvasMouse(bool);
		void onAddTransferFunction();
		void onSelectTFItem(int idx,
							double min, double max,
							double grad_min, double grad_max,
							double opacity);
		void onSelectTransferFunctionItem(int index);
		void onTransferFunctionHistogramChanged(int index) const;
		void onMoveSlice(float value);
		void onNavigateSlice(float x, float y, float z);
		void onSpuitTF(double ri_min, double ri_max,
						double grad_min, double grad_max);
		void onLevelChanged(float ri_min, float ri_max);
		void onColormapChanged(int colormap_idx);

		void onDirectionChanged(TC::DirectionType type);

		void onSimulate(QList<TC::RecordAction*> process);
		void onStopSimulate() const;
		void onFinishedSimulate();
		void onStartRecord(QString path, TC::RecordType type, QList<TC::RecordAction*> process);
		void onFinishedRecord();
		void onCaptureScreen(int type);
		void on3DBackgroundChanged(bool isWhite);
		void onRowRes(bool inProgress);

		void onToggleScalarBar(int id, bool visible);

		void mouseMoveIn3D();
		void mouseMoveIn2D();

	private:
		auto InitUI(void) -> bool;
		auto InitOIV(void) -> bool;
		auto InitRender(void) -> bool;

		auto BuildUp(const int& modality) -> bool;
		auto BuildHiddenScene() const -> bool;
		auto Build3DScene(void) const -> bool;
		auto Build2DScene(void) const -> bool;
		auto BuildHTSlice(void) const -> bool;
		auto BuildFLSlice(void) const -> bool;
		auto BuildHTVolume(void) -> bool;
		auto BuildFLVolume(void) -> bool;
		auto BuildBF(void) -> bool;
		auto BuildTFCanvas(void) -> bool;
		auto BuildCamera(void) const -> bool;
		auto BuildScaleBar(void) -> bool;
		auto BuildScalarBar(void) -> bool;

		auto BuildHTBoundaryBox(SoSeparator* node) -> bool;
		auto BuildFLBoundaryBox(SoSeparator* node) -> bool;

		auto SetMIP(const bool& isMIP) const -> void;
		auto SetHTMIP(const bool& isHTMIP) const -> void;
		auto SetFLMIP(const bool& isFLMIP) const -> void;

		auto Render(const Interactor::SceneDS::Pointer& ds) -> bool;
		auto SetTimeStep(const double& timeIndex) -> void;

		auto AnimateOrbit(const int& axis,
						const float& startTime, const float& endTime,
						const int& startAngle, const int& orbit,
						const bool& reverse, const bool& last = false) -> void;

		auto AnimateSlice(const int& sliceType,
						const float& startTime, const float& endTime,
						const int& begin, const int& finish,
						const bool& reverse, const bool& last = false) -> void;

		auto AnimateTimelapse(const int& modality,
							const float& startTime, const float& endTime,
							const int& begin, const int& finish,
							const bool& reverse, const bool& last = false) -> void;

		auto RecordScreen(QString path, TC::RecordType type, QList<TC::RecordAction*> process) -> bool;

		auto WindowIndex2MedicalHelperAxis(const int& windowIndex) -> int;

		auto ChangeLayout(const Entity::LayoutType& layoutType) const -> void;

		auto Grab() -> QPixmap;

		void Delay(const LARGE_INTEGER& frequency, const float& millisecond);

		auto MergeScreens(std::list<cv::Mat>& cvBuffer) -> void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}
