#include <QSplitter>
#include <QSettings>

#include "LayoutView.h"

namespace TomoAnalysis::Viewer::Plugins {
    struct Plugins::LayoutView::Impl {
        Impl() = default;
        ~Impl() = default;

        Entity::LayoutType layoutType = Entity::LayoutType::UNKNOWN;

        QList<View> views;

        QWidget* activatedWidget = nullptr;
    };

    LayoutView::LayoutView(QWidget* parent)
    : QWidget(parent)
    , d(new Impl) {
        this->installEventFilter(this);
    }

    LayoutView::~LayoutView() = default;

    auto LayoutView::SaveLayout(const QString& path) const ->bool {
        if(true == path.isEmpty()) {
            return false;
        }

        QFile file(path);
        if(false == file.exists()) {
            file.open(QIODevice::WriteOnly);
            file.close();
        }

        QSettings settings(path, QSettings::IniFormat);
        settings.beginGroup("Layout");
        settings.setValue("Mode", d->layoutType._to_integral());

        settings.beginGroup("Geometry");
        for (auto view : d->views) {
            const auto widget = view.second;
            if (nullptr == widget) {
                continue;
            }

            settings.setValue(view.first._to_string(), widget->size());
        }

        settings.endGroup();

        return true;
    }

    auto LayoutView::LoadLayout(const QString& path)->bool {
        if (true == path.isEmpty()) {
            return false;
        }

        QFile file(path);
        if(false == file.exists()) {
            return false;
        }

        if(false == file.open(QIODevice::ReadOnly)) {
            return false;
        }

        QSettings settings(path, QSettings::IniFormat);
        settings.beginGroup("Layout");
        auto type = settings.value("Mode").toInt();
        if(0 >= type) {
            return false;
        }

        settings.beginGroup("Geometry");
        const QStringList childKeys = settings.childKeys();

        QList<ViewGeometry> geometryInfo;
        for(auto key : childKeys) {
            ViewGeometry geometry;
            geometry.first = key;
            geometry.second = settings.value(key).toSize();

            geometryInfo.append(geometry);
        }

        if(true == geometryInfo.isEmpty()) {
            return false;
        }

        settings.endGroup();
        file.close();

        this->SetLayoutType(Entity::LayoutType::_from_integral(type), &geometryInfo);

        return true;
    }

    auto LayoutView::SaveCurrent() const ->void {
        auto settings = new QSettings("Tomocube", "TomoAnalysisTemp");
        settings->beginGroup("Layout");
        settings->setValue("Mode", d->layoutType._to_integral());

        settings->beginGroup("Geometry");
        for (auto view : d->views) {
            const auto widget = view.second;
            if (nullptr == widget) {
                continue;
            }

            settings->setValue(view.first._to_string(), widget->size());
        }

        settings->endGroup();
    }

    auto LayoutView::LoadCurrent()->bool {
        auto settings = new QSettings("Tomocube", "TomoAnalysisTemp");

        settings->beginGroup("Layout");
        auto type = settings->value("Mode").toInt();
        if (0 >= type) {
            return false;
        }

        settings->beginGroup("Geometry");
        const QStringList childKeys = settings->childKeys();

        QList<ViewGeometry> geometryInfo;
        for (auto key : childKeys) {
            ViewGeometry geometry;
            geometry.first = key;
            geometry.second = settings->value(key).toSize();

            geometryInfo.append(geometry);
        }

        if (true == geometryInfo.isEmpty()) {
            return false;
        }

        settings->endGroup();

        this->SetLayoutType(Entity::LayoutType::_from_integral(type), &geometryInfo);

        return true;
    }

    auto LayoutView::GetLayoutType(void) const ->Entity::LayoutType {
        return d->layoutType;
    }

    auto LayoutView::SetLayoutType(const Entity::LayoutType& layoutType, QList<ViewGeometry>* geometryList)->void {
        // layout ����
        if(nullptr != this->layout()) {
            for (auto view : d->views) {
                const auto widget = view.second;
                if (nullptr == widget) {
                    continue;
                }

                this->layout()->removeWidget(widget);
                widget->hide();
            }

            delete this->layout();
        }

        auto layout = new QHBoxLayout(this);
        layout->setSpacing(0);
        layout->setMargin(0);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->installEventFilter(this);

        const auto xyWidget = this->FindWidget(Entity::ViewType::XY2D);
        const auto yzWidget = this->FindWidget(Entity::ViewType::YZ2D);
        const auto xzWidget = this->FindWidget(Entity::ViewType::ZX2D);
        const auto volumeWidget = this->FindWidget(Entity::ViewType::V3D);

        QSize xySize;
        QSize yzSize;
        QSize xzSize;
        QSize volumeSize;

        if(nullptr != geometryList) {
            for(auto geometry : *geometryList) {
                auto type = Entity::ViewType::_from_string(geometry.first.toStdString().c_str());

                if(+Entity::ViewType::XY2D == type) {
                    xySize = geometry.second;
                }
                else if (+Entity::ViewType::YZ2D == type) {
                    yzSize = geometry.second;
                }
                else if (+Entity::ViewType::ZX2D == type) {
                    xzSize = geometry.second;
                }
                else if (+Entity::ViewType::V3D == type) {
                    volumeSize = geometry.second;
                }
            }
        }

        switch (layoutType) {
        case Entity::LayoutType::TwoByTwo: {
            auto splitter1 = new QSplitter(this);
            splitter1->setOrientation(Qt::Orientation::Horizontal);
            splitter1->addWidget(xyWidget);
            splitter1->addWidget(yzWidget);
            

            auto splitter2 = new QSplitter(this);
            splitter2->setOrientation(Qt::Orientation::Horizontal);
            splitter2->addWidget(xzWidget);
            splitter2->addWidget(volumeWidget);
            

            auto splitter3 = new QSplitter(this);
            splitter3->setOrientation(Qt::Orientation::Vertical);
            splitter3->addWidget(splitter1);
            splitter3->addWidget(splitter2);
            
            if (nullptr == geometryList) {
                splitter1->setSizes({ 1, 1 });
                splitter2->setSizes({ 1, 1 });
                splitter3->setSizes({ 1, 1 });
            }
            else {
                splitter1->setSizes({ xySize.width(), yzSize.width() });
                splitter2->setSizes({ xzSize.width(), volumeSize.width() });
                splitter3->setSizes({ xySize.height(), xzSize.height() });
            }
            
            layout->addWidget(splitter3);

            xyWidget->show();
            yzWidget->show();
            xzWidget->show();
            volumeWidget->show();

            break;
        }
        case Entity::LayoutType::HSlicesBy3D: {
            auto splitter1 = new QSplitter(this);
            splitter1->setOrientation(Qt::Orientation::Vertical);
            splitter1->addWidget(xyWidget);
            splitter1->addWidget(yzWidget);
            splitter1->addWidget(xzWidget);

            auto splitter2 = new QSplitter(this);
            splitter2->setOrientation(Qt::Orientation::Horizontal);
            splitter2->addWidget(splitter1);
            splitter2->addWidget(volumeWidget);            

            if (nullptr == geometryList) {
                splitter1->setSizes({ 1, 1, 1 });
                splitter2->setSizes({ 1, 3 });
            }
            else {
                splitter1->setSizes({ xySize.height(), yzSize.height(), xzSize.height() });
                splitter2->setSizes({ xySize.width(), volumeSize.width() });
            }

            layout->addWidget(splitter2);

            xyWidget->show();
            yzWidget->show();
            xzWidget->show();
            volumeWidget->show();

            break;
        }
        case Entity::LayoutType::VSlicesBy3D: {
            auto splitter1 = new QSplitter(this);
            splitter1->setOrientation(Qt::Orientation::Horizontal);
            splitter1->addWidget(xyWidget);
            splitter1->addWidget(yzWidget);
            splitter1->addWidget(xzWidget);

            auto splitter2 = new QSplitter(this);
            splitter2->setOrientation(Qt::Orientation::Vertical);
            splitter2->addWidget(splitter1);
            splitter2->addWidget(volumeWidget);

            if (nullptr == geometryList) {
                splitter1->setSizes({ 1, 1, 1 });
                splitter2->setSizes({ 1, 3 });
            }
            else {
                splitter1->setSizes({ xySize.width(), yzSize.width(), xzSize.width() });
                splitter2->setSizes({ xySize.height(), volumeSize.height() });
            }

            layout->addWidget(splitter2);

            xyWidget->show();
            yzWidget->show();
            xzWidget->show();
            volumeWidget->show();

            break;
        }
        case Entity::LayoutType::Only3D: {
            layout->addWidget(volumeWidget);

            volumeWidget->show();

            break;
        }
        case Entity::LayoutType::XYPlane: {
            layout->addWidget(xyWidget);

            xyWidget->show();

            break;
        }
        case Entity::LayoutType::YZPlane: {
            layout->addWidget(yzWidget);

            yzWidget->show();

            break;
        }
        case Entity::LayoutType::XZPlane: {
            layout->addWidget(xzWidget);

            xzWidget->show();

            break;
        }
        case Entity::LayoutType::HXY3D: {
            auto splitter = new QSplitter(this);
            splitter->setOrientation(Qt::Orientation::Horizontal);
            splitter->addWidget(xyWidget);
            splitter->addWidget(volumeWidget);

            if (nullptr == geometryList) {
                splitter->setSizes({ 1, 1 });
            }
            else {
                splitter->setSizes({ xySize.height(), volumeSize.height() });
            }

            layout->addWidget(splitter);

            xyWidget->show();
            volumeWidget->show();

            break;
        }
        case Entity::LayoutType::VXY3D: {
            auto splitter = new QSplitter(this);
            splitter->setOrientation(Qt::Orientation::Vertical);
            splitter->addWidget(xyWidget);
            splitter->addWidget(volumeWidget);
            
            if (nullptr == geometryList) {
                splitter->setSizes({ 1, 1 });
            }
            else {
                splitter->setSizes({ xySize.width(), volumeSize.width() });
            }

            layout->addWidget(splitter);

            xyWidget->show();
            volumeWidget->show();

            break;
        }
            default: ;
        }

        this->setLayout(layout);

        d->layoutType = layoutType;
    }
    
    auto LayoutView::AddWidget(const Entity::ViewType& viewType, QWidget* widget) ->void {
        d->views.append(View(viewType, widget));
        widget->installEventFilter(this);
    }

    auto LayoutView::GetActivatedWidget(void) const ->QWidget* {
        return d->activatedWidget;
    }

    auto LayoutView::GetActivatedWidgetIndex(void) const ->int {
        int index = -1;

        for(int i = 0; i < d->views.size(); ++i) {
            auto view = d->views.at(i);
            if (d->activatedWidget == view.second) {
                index = i;
                break;
            }
        }

        return index;
    }

    auto LayoutView::GetActivatedWidgetType(void) const ->Entity::ViewType {
        Entity::ViewType type = Entity::ViewType::NONE;

        for (auto view : d->views) {
            if (d->activatedWidget == view.second) {
                type = view.first;
                break;
            }
        }

        return type;
    }

    auto LayoutView::FindWidgetType(QWidget* widget) const ->Entity::ViewType {
        Entity::ViewType type = Entity::ViewType::NONE;

        for (auto view : d->views) {
            if (widget == view.second) {
                type = view.first;
                break;
            }
        }

        return type;
    }

    // protected
    auto LayoutView::eventFilter(QObject* obj, QEvent* event) -> bool {
        if (event->type() == QEvent::HoverEnter) {
            const auto sender = dynamic_cast<QWidget*>(obj);

            for (auto view : d->views) {
                if (sender != view.second) {
                    continue;
                }
            
                d->activatedWidget = sender;
                break;
            }
        }

        return false;
    }

    // private
    auto LayoutView::FindWidget(const Entity::ViewType& viewType) const ->QWidget* {
        QWidget* widget = nullptr;

        for (auto view : d->views) {
            if (viewType == +view.first) {
                widget = view.second;
                break;
            }
        }

        return widget;
    }
}
