#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/events/SoEvent.h>
#include <Inventor/SoPickedPoint.h>
#include "CustomCube.h"

#include <Inventor/events/SoMouseButtonEvent.h>

struct CustomCube::Impl { };

CustomCube::CustomCube() : SoViewingCube(), d { new Impl } { }

CustomCube::~CustomCube() { }

void CustomCube::handleEvent(SoHandleEventAction* ha) {
	//Consume Event
	//SoViewingCube::handleEvent(ha);
}
