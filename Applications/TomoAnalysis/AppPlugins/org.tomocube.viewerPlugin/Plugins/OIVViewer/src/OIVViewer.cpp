#include <utility>

#include <qfuture.h>
#include <QtConcurrent/qtconcurrentrun.h>
#include <QTimer>
#include <QDateTime>
#include <QFutureWatcher>
#include <QColorDialog>
#include <QLabel>
#include <QApplication>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/nodes/SoInfo.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/SoOffscreenRenderArea.h>
#include <Inventor/nodes/SoMultiSwitch.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoScale.h>
#include <Inventor/Axis.h>
#include <Inventor/ViewerComponents/nodes/SoViewingCube.h>
#include "CustomCube.h"
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Medical/nodes/TextBox.h>
#pragma warning(pop)

#include <OivScene3D.h>
#include <OivSceneXY.h>
#include <OivSceneYZ.h>
#include <OivSceneXZ.h>
#include <HiddenScene.h>
#include <OivBufferManager.h>
#include <ViewerRenderWindow2d.h>
#include <ViewerRenderWindow3d.h>
#include <QTransferFunctionCanvas2D.h>
#include <ScalarBarSwitch.h>
#include <ScaleBarSwitch.h>
#include <TimeStampSwitch.h>
#include <OivAxisGrid.h>
#include <Oiv2dBFSlice.h>
#include <LevelWindow.h>

#include <TCFInfoPresenter.h>

#include <ToastMessageBox.h>
#include <TCVideoWriter.h>
#include <TCFMetaReader.h>

#include <QtMath>

#include "LayoutView.h"
#include "OIVViewer.h"

namespace TomoAnalysis::Viewer::Plugins {
#define SAFE_DELETE( p ) { if( p ) { delete ( p ); ( p ) = NULL; } }

#define NUMBER_OF_FL_CHANNEL        3
#define NUMBER_OF_2D_RENDER_WINDOW  3

#define HT_INDEX 0
#define FL_INDEX 1
#define BF_INDEX 2

	tStepper::tStepper(QObject* parent) : d { new Impl } {
		Q_UNUSED(parent)
	}

	tStepper::~tStepper() {
		std::cout << "Thread destroyed" << std::endl;
	}

	auto tStepper::SetBuffer(OivBufferManager* buf) -> void {
		d->buf = buf;
	}

	void tStepper::run(int step) {
		d->buf->prepareTimeHT(step);
	}


	struct OIVViewer::Impl {
		Impl() = default;
		Impl(const Impl& other) = default;

		// Common
		int sceneID = -1;
		QString filePath;

		Entity::LayoutType layoutType = Entity::LayoutType::UNKNOWN;
		int activatedModality = Entity::Modality::None;

		// reader
		TC::IO::TCFMetaReader::Meta::Pointer metaInfo;

		SoRef<SoGroup> infoGroup = nullptr;
		QList<SoRef<SoInfo>> infos;//current metainformation container

		//buffer reader
		OivBufferManager* bufferManager;
		QList<QString> bufferTileName;

		// scene graph
		//OivSliceContainer* sliceContainer = nullptr;
		TC::OivSceneXY* sliceConXY = nullptr;
		TC::OivSceneYZ* sliceConYZ = nullptr;
		TC::OivSceneXZ* sliceConXZ = nullptr;
		//OivVolumeContainer* volumeContainer = nullptr;
		TC::OivScene3D* volumeCon = nullptr;
		HiddenScene* hiddenScene = nullptr;

		Oiv2dBFSlice* bfContainer = nullptr;

		SoRef<SoSeparator> volumeRenderRoot = nullptr;
		SoRef<SoSeparator> volumeRenderGroup = nullptr;

		SoRef<SoSwitch> volumeSwitch = nullptr;
		//SoSwitch* volumeSwitch = nullptr;
		SoRef<SoSwitch> sliceSwitch[NUMBER_OF_2D_RENDER_WINDOW] { nullptr, };

		SoRef<SoSeparator> sliceRenderRoot[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };

		TC::LevelWindow* levelWindow { nullptr };

		bool isBuildHT2D = false;
		bool isBuildFL2D = false;
		bool isBuildHT3D = false;
		bool isBuildFL3D = false;
		bool isBuildBF = false;
		bool isBuildTFCanvas = false;
		bool isBuildCamera = false;
		bool isBuildScaleBar = false;
		bool isBuildScalarBar = false;
		bool isBuildScene = false;

		bool isCrop = false;
		int cropOffset[2] { 0, 0 };
		int cropSize[2] { 0, 0 };
		bool isFLCrop = false;
		int cropOffsetFL[2] { 0, 0 };
		int cropSizeFL[2] { 0, 0 };

		// render window
		LayoutView* layoutView = nullptr;
		ViewerRenderWindow2d* qOiv2DRenderWindow[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };
		ViewerRenderWindow3d* qOiv3DRenderWindow = nullptr;

		QWidget* tfCanvasWidget = nullptr;
		QTransferFunctionCanvas2D* tfCanvas = nullptr;

		bool isOverlay = false;

		SoRef<SoSwitch> boundaryBoxHTSwitch = nullptr;

		SoRef<SoSwitch> boundaryBoxFLSwitch = nullptr;

		SoRef<SoSwitch> orientationMarkerSwitch = nullptr;
		//SoViewingCube* viewCube = nullptr;
		SoRef<CustomCube> viewCube { nullptr };
		SoRef<SoMaterial> annoMatl { nullptr };
		SoRef<SoSwitch> timestampSwitch = nullptr;
		SoRef<SoSwitch> timestampSwitch2d[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };
		SoRef<TextBox> annoText { nullptr };
		SoRef<TextBox> annoText2d[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };

		SoRef<SoSwitch> scaleBarSwitch[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };
		TC::ScaleBarSwitch* scaleBar[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };

		SoRef<SoSeparator> manipulatorRoot[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };

		SoRef<SoSwitch> scalarBarSwitch[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };
		SoRef<SoSeparator> scalarBarSep[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };
		TC::ScalarBarSwitch* scalarBar[NUMBER_OF_2D_RENDER_WINDOW] = { nullptr, };
		OivAxisGrid* axisGridHT { nullptr };
		bool axisHT { false };
		OivAxisGrid* axisGridFL { nullptr };
		bool axisFL { false };

		int volumeChildIndex = 0;
		int sliceChildIndex = 0;
		int sliceTraversalMode = SoMultiSwitch::INCLUDE;
		int childIndex;
		int curSliceRes = 0;
		int curVolumeRes = 0;

		bool simulating = false;
		QMutex simulationMutex;
		QFutureWatcher<bool> simulationWatcher;

		QList<QPixmap> recordBuffer;
		QList<QPixmap> recordBufferXY;
		QList<QPixmap> recordBufferYZ;
		QList<QPixmap> recordBufferXZ;
		QList<QPixmap> recordBuffer3D;
		QString recordSavePath;
		QFutureWatcher<bool> recordWatcher;

		int text_unit { 0 };
		//SoRef<SoSeparator> rootHolder;

		QString curProjPath;

		bool sliceInited { false };
		std::vector<double> time_points;
		bool visibleScalarBar[3] { false, false, false };

		bool isIn3D { true };
		bool curChVisible[3] { true, true, true };

		double htValueDiv { 1 };
		double htValueOffset { 0 };
		double flValueOffset[3] { 0, 0, 0 };
		double curLevelMin { 0 };
		double curLevelMax { 0 };
	};

	OIVViewer::OIVViewer(SoNode* node, SoSeparator* node2d[3], QWidget* parent)
		: OIVViewer(parent) {
		SetRootNode(node, node2d);
	}

	OIVViewer::OIVViewer(QWidget* parent)
		: QWidget(parent)
		, Interactor::IImageViewer()
		, d(new Impl()) { }

	OIVViewer::~OIVViewer() { }

	auto OIVViewer::SetTimePoints(std::vector<double> tps) -> void {
		d->time_points = tps;
	}

	auto OIVViewer::ConnectScreenShotWidget(TC::ScreenShotWidget::Pointer widget) -> void {
		widget->SetRenderWindow(d->qOiv3DRenderWindow, "threeD");
		widget->SetRenderWindow(d->qOiv2DRenderWindow[0], "XY");
		widget->SetRenderWindow(d->qOiv2DRenderWindow[1], "YZ");
		widget->SetRenderWindow(d->qOiv2DRenderWindow[2], "XZ");

		widget->SetMultiLayerType(TC::MultiLayoutType::VSlicesBy3D);
	}

	auto OIVViewer::ConnectRecorderWidget(TC::VideoRecorderWidget::Pointer widget) -> void {
		widget->SetRenderWindow(d->qOiv3DRenderWindow, "threeD");
		widget->SetRenderWindow(d->qOiv2DRenderWindow[0], "XY");
		widget->SetRenderWindow(d->qOiv2DRenderWindow[1], "YZ");
		widget->SetRenderWindow(d->qOiv2DRenderWindow[2], "XZ");

		widget->SetMultiLayerType(TC::MultiLayoutType::VSlicesBy3D);
	}

	auto OIVViewer::SetDisplayType(bool is2D) -> void {
		for (auto i = 0; i < 3; i++) {
			if (nullptr != d->qOiv2DRenderWindow[i]) {
				d->qOiv2DRenderWindow[i]->setType(is2D);
			}
		}
		if (is2D) {
			d->sliceConXY->SetHTOverlay(false);
			d->sliceConYZ->SetHTOverlay(false);
			d->sliceConXZ->SetHTOverlay(false);
			//d->sliceContainer->toggleOverlay(false);
		} else {
			d->sliceConXY->SetHTOverlay(d->isOverlay);
			d->sliceConYZ->SetHTOverlay(d->isOverlay);
			d->sliceConXZ->SetHTOverlay(d->isOverlay);
			//d->sliceContainer->toggleOverlay(d->isOverlay);
		}
	}

	auto OIVViewer::SetCropInformation(int xOffset, int yOffset, int xSize, int ySize) -> void {
		d->isCrop = true;
		d->cropOffset[0] = xOffset;
		d->cropOffset[1] = yOffset;
		d->cropSize[0] = xSize;
		d->cropSize[1] = ySize;
	}

	auto OIVViewer::SetFLCropInformation(int xOffset, int yOffset, int xSize, int ySize) -> void {
		d->isFLCrop = true;
		d->cropOffsetFL[0] = xOffset;
		d->cropOffsetFL[1] = yOffset;
		d->cropSizeFL[0] = xSize;
		d->cropSizeFL[1] = ySize;
	}

	auto OIVViewer::Update() -> bool {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.000001;
		};
		const auto ds = GetFileDS();
		const QString path = QString::fromUtf8(ds->path.c_str());

		if (true == path.isEmpty()) {
			return false;
		}

		d->filePath = path;
		if (nullptr == d->hiddenScene) {
			d->hiddenScene = new HiddenScene;
		}
		d->hiddenScene->SetFilePath(d->filePath);
		// read info
		auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
		d->metaInfo = metaReader->Read(path);
		if (d->metaInfo->data.data3D.scalarType.count() > 0) {
			if (d->metaInfo->data.data3D.scalarType[0] == 1) {
				d->htValueOffset = d->metaInfo->data.data3D.riMinList[0] * 1000.0;
				d->htValueDiv = 10;
			}
		} else if (d->metaInfo->data.data2DMIP.scalarType.count() > 0) {
			if (d->metaInfo->data.data2DMIP.scalarType[0] == 1) {
				d->htValueOffset = d->metaInfo->data.data2DMIP.riMinList[0] * 1000.0;
				d->htValueDiv = 10;
			}
		}
		for (auto i = 0; i < 3; i++) {
			if (d->metaInfo->data.data3DFL.scalarType[i].count() > 0) {
				if (d->metaInfo->data.data3DFL.scalarType[i][0] == 1) {
					d->flValueOffset[i] = d->metaInfo->data.data3DFL.minIntensityList[i][0];
				}
			} else if (d->metaInfo->data.data2DFLMIP.scalarType[i].count() > 0) {
				if (d->metaInfo->data.data2DFLMIP.scalarType[i][0] == 1) {
					d->flValueOffset[i] = d->metaInfo->data.data2DFLMIP.minIntensityList[i][0];
				}
			}
		}
		d->levelWindow->SetMinMax(d->metaInfo->data.data3D.riMin * 10000.0, d->metaInfo->data.data3D.riMax * 10000.0);

		auto isLdm = d->metaInfo->data.isLDM;

		for (auto i = 0; i < 3; i++) {
			d->qOiv2DRenderWindow[i]->setIsLdm(isLdm);
			if (isLdm) {
				d->qOiv2DRenderWindow[i]->setRenderWindowID(i);
			} else {
				d->qOiv2DRenderWindow[i]->setRenderWindowID(0);
			}
		}

		d->bufferManager = new OivBufferManager(true);
		d->bufferManager->setMetaInfo(d->metaInfo);
		d->bufferManager->setNumberOfBuffer(1);
		d->bufferManager->setFilePath(path);
		if (d->isCrop) {
			d->bufferManager->setXYCropOffset(d->cropOffset[0], d->cropOffset[1]);
			d->bufferManager->setXYCropSize(d->cropSize[0], d->cropSize[1]);
		}
		if (d->isFLCrop) {
			d->bufferManager->setXYCropOffsetFL(d->cropOffsetFL[0], d->cropOffsetFL[1]);
			d->bufferManager->setXYCropSizeFL(d->cropSizeFL[0], d->cropSizeFL[1]);
		}
		//existance added						

		if (d->isCrop) {
			if (d->metaInfo->data.data3D.dataCount > 0) {
				d->bufferManager->setNumberOfImage(1);
			} else {
				d->bufferManager->setNumberOfImage(0);
			}
			if (d->metaInfo->data.data3DFL.dataCount > 0) {
				d->bufferManager->setNumberOfFLImage(1);
			} else {
				d->bufferManager->setNumberOfFLImage(0);
			}
			d->bufferManager->setNumberOfBFImage(0);
		} else {
			d->bufferManager->setNumberOfImage(d->metaInfo->data.data3D.dataCount);
			d->bufferManager->setNumberOfFLImage(d->metaInfo->data.data3DFL.dataCount);
			d->bufferManager->setNumberOfBFImage(d->metaInfo->data.dataBF.dataCount);
		}

		d->bufferManager->initialize();
		d->bufferManager->setDefaultReader();

		//set LDM option for buffer manager 20.09.04 Jose T. Kim

		//set default volume resolution = interactive
		d->bufferManager->enableFixedResolution(false, true);		

		//set default slice resolution = full
		d->bufferManager->enableFixedResolution(true, false);
		d->bufferManager->setFixedResolution(0, false);		
		

		d->infoGroup->removeAllChildren();
		d->infos.clear();

		auto HT_TS_info = new SoInfo;
		HT_TS_info->setName("HT_TIME_STEPS");
		if (d->isCrop) {
			HT_TS_info->string.setValue(std::to_string(1));
		} else {
			HT_TS_info->string.setValue(std::to_string(d->metaInfo->data.data3D.dataCount));
		}
		d->infos.push_back(HT_TS_info);
		d->infoGroup->addChild(HT_TS_info);

		auto HT_CURT_info = new SoInfo;
		HT_CURT_info->setName("HT_CUR_TIME");
		HT_CURT_info->string.setValue(std::to_string(0));
		d->infos.push_back(HT_CURT_info);
		d->infoGroup->addChild(HT_CURT_info);

		auto TCF_name_info = new SoInfo;
		TCF_name_info->setName("TCF_NAME");
		TCF_name_info->string.setValue(path.toStdString());
		d->infos.push_back(TCF_name_info);
		d->infoGroup->addChild(TCF_name_info);

		auto HT_RIMin_info = new SoInfo;
		HT_RIMin_info->setName("HTMin");
		HT_RIMin_info->string.setValue(std::to_string(d->metaInfo->data.data3D.riMin * 10000.0));
		d->infos.push_back(HT_RIMin_info);
		d->infoGroup->addChild(HT_RIMin_info);

		auto HT_RIMax_info = new SoInfo;
		HT_RIMax_info->setName("HTMax");
		HT_RIMax_info->string.setValue(std::to_string(d->metaInfo->data.data3D.riMax * 10000.0));
		d->infos.push_back(HT_RIMax_info);
		d->infoGroup->addChild(HT_RIMax_info);

		for (int i = 0; i < 3; i++) {
			if (d->qOiv2DRenderWindow[i]) {
				d->qOiv2DRenderWindow[i]->setHTRange(d->metaInfo->data.data3D.riMin * 10000.0, d->metaInfo->data.data3D.riMax * 10000.0);
				d->qOiv2DRenderWindow[i]->refreshRangeSlider();
			}
		}

		//Check FL & HT uniformity and change shader
		if (nullptr != d->sliceConXY && nullptr != d->sliceConYZ && nullptr != d->sliceConXZ && nullptr != d->volumeCon) {
			//if (nullptr != d->sliceContainer && nullptr != d->volumeContainer) {
			auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
			auto metaInfo = metaReader->Read(path);
			auto dimSame = false;
			auto resSame = false;
			if (metaInfo->data.data3D.exist && metaInfo->data.data3DFL.exist) {
				if (AreSame(metaInfo->data.data3D.resolutionX, metaInfo->data.data3DFL.resolutionX)) {
					if (AreSame(metaInfo->data.data3D.resolutionY, metaInfo->data.data3DFL.resolutionY)) {
						if (AreSame(metaInfo->data.data3D.resolutionZ, metaInfo->data.data3DFL.resolutionZ)) {
							resSame = true;
						}
					}
				}
				if (metaInfo->data.data3D.sizeX == metaInfo->data.data3DFL.sizeX) {
					if (metaInfo->data.data3D.sizeY == metaInfo->data.data3DFL.sizeY) {
						if (metaInfo->data.data3D.sizeZ == metaInfo->data.data3DFL.sizeZ) {
							dimSame = true;
						}
					}
				}
			}
			d->sliceConXY->SetUniform(dimSame && resSame);
			d->sliceConYZ->SetUniform(dimSame && resSame);
			d->sliceConXZ->SetUniform(dimSame && resSame);
			d->volumeCon->SetUniform(dimSame && resSame);
		}

		if (d->metaInfo->data.data3D.exist || d->metaInfo->data.data2DMIP.exist) {
			d->levelWindow->show();
		} else {
			d->levelWindow->hide();
		}
		return true;
	}

	auto OIVViewer::Refresh() -> bool {
		if (nullptr == d->layoutView) {
			return false;
		}

		const auto ds = GetSceneDS();
		if (!ds) {
			return false;
		}
		auto changedBF = false;
		if (d->activatedModality != ds->activatedModality) {
			if (false == this->BuildUp(ds->activatedModality)) {
				return false;
			}

			// MIP
			bool isHTMIP = false;
			if ((ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
				isHTMIP = true;
			}
			this->SetHTMIP(isHTMIP);
			bool isFLMIP = false;
			if ((ds->activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
				isFLMIP = true;
			}
			this->SetFLMIP(isFLMIP);

			this->SetMIP(isHTMIP || isFLMIP);


			if ((ds->activatedModality & Entity::Modality::BF2D) == Entity::Modality::BF2D) {
				changedBF = d->qOiv2DRenderWindow[0]->resetViewBF(true);
			} else {
				changedBF = d->qOiv2DRenderWindow[0]->resetViewBF(false);
			}
			d->activatedModality = ds->activatedModality;
		}

		if (d->layoutType != ds->layoutType) {
			this->ResetLayout(ds->layoutType);
			d->layoutType = ds->layoutType;
		}

		auto levelMin = std::get<0>(d->levelWindow->GetMinMax());
		auto levelMax = std::get<1>(d->levelWindow->GetMinMax());
		onLevelChanged(levelMin, levelMax);

		if (false == this->Render(ds)) {
			return false;
		}

		if (changedBF) {
			d->qOiv2DRenderWindow[0]->reset2DView();
		}

		return true;
	}

	auto OIVViewer::UpdateTransferFunction(const int& index,
											const Entity::TFItem::Pointer& item) -> bool {
		if (false == d->isBuildTFCanvas) {
			return false;
		}

		if (nullptr == d->tfCanvas) {
			return false;
		}

		if (!item) {
			return false;
		}

		d->tfCanvas->ChangeItem(index,
								item->intensityMin, item->intensityMax,
								item->gradientMin, item->gradientMax,
								item->transparency);

		return true;
	}

	auto OIVViewer::UpdateChannelInfo(const Entity::FLChannelInfo& list) -> bool {
		auto ds = GetSceneDS();
		auto offsetZ = d->metaInfo->data.data3DFL.offsetZ - (d->metaInfo->data.data3D.sizeZ * d->metaInfo->data.data3D.resolutionZ / 2);
		auto resZ = d->metaInfo->data.data3DFL.resolutionZ;
		auto phyZ = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex() - 1) - d->metaInfo->data.data3D.sizeZ * d->metaInfo->data.data3D.resolutionZ / 2 - offsetZ + d->metaInfo->data.data3DFL.sizeZ * d->metaInfo->data.data3DFL.resolutionZ / 2;
		auto idxFLz = static_cast<int>(phyZ / resZ);				

		if (resZ < 0.01 || d->metaInfo->data.data3DFL.sizeZ == 1) {//exception for only on slice
			idxFLz = -1;
			auto phyComp = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex()) - d->metaInfo->data.data3D.sizeZ * d->metaInfo->data.data3D.resolutionZ / 2 - offsetZ + d->metaInfo->data.data3DFL.sizeZ * d->metaInfo->data.data3DFL.resolutionZ / 2;
			if (phyComp < d->metaInfo->data.data3D.resolutionZ && phyComp > 0) {
				idxFLz = 0;
			}
		}
		auto flIndexValid = true;
		if ((d->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
			if (idxFLz < 0 || idxFLz > d->metaInfo->data.data3DFL.sizeZ - 1) {
				flIndexValid = false;
			}
		}
		if (d->metaInfo->data.isLDM) {
			flIndexValid = true;
		}
		for (const auto& channelInfo : list) {
			const auto channel = channelInfo.first._to_integral();
			const auto info = channelInfo.second;
			if (!info) {
				continue;
			}

			if (false == info->isValid) {
				continue;
			}

			const float transparency = 1.f - static_cast<float>(info->opacity / 100.f);

			if (nullptr != d->volumeCon && true == d->isBuildFL3D) {
				d->volumeCon->ToggleChannel(channel, info->visible);
				d->volumeCon->SetFLRange(channel, info->min - d->flValueOffset[channel], info->max - d->flValueOffset[channel]);
				d->volumeCon->SetFLColor(info->color[0], info->color[1], info->color[2], channel);
				d->volumeCon->SetFLTransparency(transparency, channel);

				d->sliceConXY->ToggleFLChannel(channel, info->visible && flIndexValid);
				d->sliceConXY->SetFLRange(channel, info->min - d->flValueOffset[channel], info->max - d->flValueOffset[channel]);
				d->sliceConXY->SetFLColor(info->color[0], info->color[1], info->color[2], channel);
				d->sliceConXY->SetFLTrans(transparency, channel);

				d->sliceConYZ->ToggleFLChannel(channel, info->visible);
				d->sliceConYZ->SetFLRange(channel, info->min - d->flValueOffset[channel], info->max - d->flValueOffset[channel]);
				d->sliceConYZ->SetFLColor(info->color[0], info->color[1], info->color[2], channel);
				d->sliceConYZ->SetFLTrans(transparency, channel);

				d->sliceConXZ->ToggleFLChannel(channel, info->visible);
				d->sliceConXZ->SetFLRange(channel, info->min - d->flValueOffset[channel], info->max - d->flValueOffset[channel]);
				d->sliceConXZ->SetFLColor(info->color[0], info->color[1], info->color[2], channel);
				d->sliceConXZ->SetFLTrans(transparency, channel);

				d->volumeCon->SetFLGamma(channel, info->isGamma, static_cast<double>(info->gamma) / 100.0);
				d->sliceConXY->SetFLGamma(channel, info->isGamma, static_cast<double>(info->gamma) / 100.0);
				d->sliceConYZ->SetFLGamma(channel, info->isGamma, static_cast<double>(info->gamma) / 100.0);
				d->sliceConXZ->SetFLGamma(channel, info->isGamma, static_cast<double>(info->gamma) / 100.0);
			} else if (nullptr != d->sliceConXY && true == d->isBuildFL2D) {
				d->sliceConXY->ToggleFLChannel(channel, info->visible);
				d->sliceConXY->SetFLRange(channel, info->min - d->flValueOffset[channel], info->max - d->flValueOffset[channel]);
				d->sliceConXY->SetFLTrans(transparency, channel);
				d->sliceConXY->SetFLGamma(channel, info->isGamma, static_cast<double>(info->gamma) / 100.0);
			}

			d->curChVisible[channel] = info->visible;
		}

		return true;
	}

	auto OIVViewer::UpdateVisualizationData(const Entity::TFItemList& tfItemList,
											const Entity::FLChannelInfo& info) -> bool {
		if (false == d->isBuildTFCanvas) {
			return false;
		}

		if (nullptr == d->tfCanvas) {
			return false;
		}

		d->tfCanvas->DeleteAllItem();

		for (const auto item : tfItemList) {
			d->tfCanvas->AddItem(item->intensityMin, item->intensityMax,
								item->gradientMin, item->gradientMax,
								item->color.red, item->color.green, item->color.blue,
								item->transparency);
		}

		UpdateChannelInfo(info);

		return true;
	}

	auto OIVViewer::ClearTransferFunction() -> bool {
		if (false == d->isBuildTFCanvas) {
			return false;
		}

		if (nullptr == d->tfCanvas) {
			return false;
		}

		d->tfCanvas->DeleteAllItem();

		Entity::TFItemList list;
		emit transferFunctionChanged(list);

		return true;
	}

	auto OIVViewer::Init(void) -> bool {
		if (false == this->InitOIV()) {
			return false;
		}

		if (false == this->InitUI()) {
			return false;
		}

		if (false == this->InitRender()) {
			return false;
		}

		return true;
	}

	auto OIVViewer::Reset(void) -> bool {
		// Common		
		//d->sceneID = -1;
		d->htValueDiv = 1;
		d->htValueOffset = 0;
		for (auto i = 0; i < 3; i++) {
			d->flValueOffset[i] = 0;
			d->visibleScalarBar[i] = false;
			d->curChVisible[i] = true;
		}

		d->isCrop = false;
		d->isFLCrop = false;
		for (auto i = 0; i < 2; i++) {
			d->cropOffset[i] = 0;
			d->cropSize[i] = 0;
			d->cropOffsetFL[i] = 0;
			d->cropSizeFL[i] = 0;
		}

		d->filePath = "";
		d->isBuildScene = false;
		d->activatedModality = Entity::Modality::None;
		d->curVolumeRes = -1;
		d->curSliceRes = -1;

		d->isBuildHT2D = false;;
		d->isBuildFL2D = false;
		d->isBuildHT3D = false;;
		d->isBuildFL3D = false;
		d->isBuildBF = false;
		d->isBuildTFCanvas = false;
		d->isBuildCamera = false;
		d->isBuildScaleBar = false;
		d->isBuildScalarBar = false;

		d->isOverlay = false;

		d->layoutType = Entity::LayoutType::UNKNOWN;
		if (d->volumeCon) {
			d->volumeCon->ClearHTVolume();
			d->volumeCon->ClearFLVolume();
		}
		if (d->sliceConXY) {
			d->sliceConXY->ClearMIPVolume();
			d->sliceConXY->ClearMIPFLVolume();
		}

		d->volumeSwitch->removeAllChildren();
		d->volumeSwitch->addChild(new SoSeparator);
		d->volumeSwitch->whichChild = -1;

		auto ds = GetSceneDS();
		if (d->boundaryBoxHTSwitch) {
			if (d->boundaryBoxHTSwitch->getNumChildren() > 0) {
				d->boundaryBoxHTSwitch->removeAllChildren();
			}
		}
		d->boundaryBoxHTSwitch = new SoSwitch;
		if (d->axisHT) {
			SAFE_DELETE(d->axisGridHT);
			d->axisGridHT = new OivAxisGrid;
			for (auto i = 0; i < 3; i++) {
				d->axisGridHT->SetTextColor(i, ds->axisGridColor[i][0], ds->axisGridColor[i][1], ds->axisGridColor[i][2]);
			}
			d->axisHT = false;
		}
		if (d->boundaryBoxFLSwitch) {
			if (d->boundaryBoxFLSwitch->getNumChildren() > 0) {
				d->boundaryBoxFLSwitch->removeAllChildren();
			}
		}
		d->boundaryBoxFLSwitch = new SoSwitch;
		if (d->axisFL) {
			SAFE_DELETE(d->axisGridFL);
			d->axisGridFL = new OivAxisGrid;
			for (auto i = 0; i < 3; i++) {
				d->axisGridFL->SetTextColor(i, ds->axisGridColor[i][0], ds->axisGridColor[i][1], ds->axisGridColor[i][2]);
			}
			d->axisFL = false;
		}

		for (int i = 0; i < 3; i++) {
			d->scalarBarSwitch[i]->removeAllChildren();
			d->manipulatorRoot[i]->removeAllChildren();
			d->scaleBarSwitch[i]->removeAllChildren();

			d->sliceSwitch[i]->removeAllChildren();
			d->sliceSwitch[i]->addChild(new SoSeparator);
			d->sliceSwitch[i]->addChild(new SoSeparator);
			d->sliceSwitch[i]->whichChild = 0;
		}

		d->metaInfo = nullptr;
		//SAFE_DELETE(d->volumeContainer);
		//SAFE_DELETE(d->sliceContainer);
		SAFE_DELETE(d->volumeCon);
		SAFE_DELETE(d->sliceConXY)
		SAFE_DELETE(d->sliceConYZ)
		SAFE_DELETE(d->sliceConXZ)
		SAFE_DELETE(d->bfContainer);

		SAFE_DELETE(d->bufferManager);
		SAFE_DELETE(d->tfCanvas);

		if (nullptr != d->tfCanvasWidget->layout()) {
			QLayoutItem* child;
			while ((child = d->tfCanvasWidget->layout()->takeAt(0)) != nullptr) {
				child->widget()->hide();
				d->tfCanvasWidget->layout()->removeItem(child);
				SAFE_DELETE(child);
			}
			delete d->tfCanvasWidget->layout();
		}

		if (d->qOiv3DRenderWindow) {
			d->qOiv3DRenderWindow->resetOnLoad();
		}
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			if (d->qOiv2DRenderWindow[i]) {
				d->qOiv2DRenderWindow[i]->resetOnLoad();
			}
		}

		return true;
	}

	auto OIVViewer::SetRootNode(SoNode* node, SoSeparator* node2d[3]) const -> void {
		Q_UNUSED(node)
		Q_UNUSED(node2d)
	}

	auto OIVViewer::SetSceneID(const int& id) const -> void {
		d->sceneID = id;
	}

	auto OIVViewer::SetTFWidget(QWidget* widget) const -> void {
		d->tfCanvasWidget = widget;
	}

	auto OIVViewer::ResetLayout(const int& type) -> void {
		auto layoutType = d->layoutType;

		if (-1 < type) {
			layoutType = Entity::LayoutType::_from_integral(type);
		}

		d->layoutView->SetLayoutType(layoutType);

		if (nullptr != d->qOiv3DRenderWindow) {
			d->qOiv3DRenderWindow->reset3DView();
		}
		for (auto window : d->qOiv2DRenderWindow) {
			if (nullptr != window) {
				window->reset2DView();
			}
		}
	}

	auto OIVViewer::SaveLayout(const QString& path) const -> bool {
		if (nullptr == d->layoutView) {
			return false;
		}

		return d->layoutView->SaveLayout(path);
	}

	auto OIVViewer::LoadLayout(const QString& path) const -> bool {
		if (nullptr == d->layoutView) {
			return false;
		}

		if (false == d->layoutView->LoadLayout(path)) {
			return false;
		}

		d->layoutType = d->layoutView->GetLayoutType();

		return true;
	}

	auto OIVViewer::GetCurrentLayoutType(void) const -> int {
		if (nullptr == d->layoutView) {
			return -1;
		}

		return d->layoutView->GetLayoutType()._to_integral();
	}

	void OIVViewer::onRowRes(bool inProgress) {
		if (d->qOiv3DRenderWindow) {
			d->qOiv3DRenderWindow->InteractiveCount(inProgress);
		}
	}

	void OIVViewer::onAddTransferFunction() {
		if (nullptr == d->tfCanvas) {
			return;
		}

		if (nullptr == d->volumeCon) {
			return;
		}
		/*if (nullptr == d->volumeContainer) {
			return;
		}*/

		Entity::TFItemList list;
		for (auto i = 0; i < d->tfCanvas->getColorList().size(); i++) {
			auto item = std::make_shared<Entity::TFItem>();

			item->intensityMin = d->tfCanvas->getMinList()[i];
			item->intensityMax = d->tfCanvas->getMaxList()[i];
			item->gradientMin = d->tfCanvas->getGradMinList()[i];
			item->gradientMax = d->tfCanvas->getGradMaxList()[i];
			item->color.red = d->tfCanvas->getColorList()[i].redF() * 255;
			item->color.green = d->tfCanvas->getColorList()[i].greenF() * 255;
			item->color.blue = d->tfCanvas->getColorList()[i].blueF() * 255;
			item->color.alpha = d->tfCanvas->getTransparencyList()[i] * 255;;
			item->transparency = d->tfCanvas->getTransparencyList()[i];
			item->visible = d->tfCanvas->getHiddenList()[i];

			list.push_back(item);
		}
		emit transferFunctionChanged(list);
	}

	void OIVViewer::onSelectTFItem(int idx,
									double min, double max,
									double grad_min, double grad_max,
									double opacity) {
		Q_UNUSED(min)
		Q_UNUSED(max)
		Q_UNUSED(grad_min)
		Q_UNUSED(grad_max)
		Q_UNUSED(opacity)
		emit currentTransferFunctionChanged(idx);
	}

	void OIVViewer::onSelectTransferFunctionItem(int index) {
		if (nullptr == d->tfCanvas) {
			return;
		}

		d->tfCanvas->setCurrentItem(index);
	}

	void OIVViewer::onTransferFunctionHistogramChanged(int index) const {
		if (nullptr == d->tfCanvas) {
			return;
		}

		d->tfCanvas->ChangeHistogram(index);
	}

	void OIVViewer::onNavigateSlice(float x, float y, float z) {
		if (nullptr == d->layoutView) {
			return;
		}

		auto sender = QObject::sender();
		auto renderWidget = (QWidget*)sender;


		MedicalHelper::Axis axis;
		int widgetIndex = -1;

		const auto activatedWidgetType = d->layoutView->FindWidgetType(renderWidget);

		int indexX = 0;
		int indexY = 0;
		int indexZ = 0;

		switch (activatedWidgetType) {
			case Entity::ViewType::XY2D:
				axis = MedicalHelper::Axis::AXIAL;
				indexX = x;
				indexY = y;
				widgetIndex = 0;
				break;
			case Entity::ViewType::YZ2D:
				axis = MedicalHelper::Axis::SAGITTAL;
				if (d->metaInfo->data.isLDM) {
					indexY = y;
					indexZ = z;
				} else {
					indexY = x;
					indexZ = y;
				}
				widgetIndex = 1;
				break;
			case Entity::ViewType::ZX2D:
				axis = MedicalHelper::Axis::CORONAL;
				if (d->metaInfo->data.isLDM) {
					indexX = x;
					indexZ = z;
				} else {
					indexX = x;
					indexZ = y;
				}
				widgetIndex = 2;
				break;
			default: ;
		}

		if (widgetIndex != 0) {
			emit sliceIndexChanged(0, indexZ);//XY Plane
		}
		if (widgetIndex != 1) {
			emit sliceIndexChanged(1, indexX);//YZ Plane
		}
		if (widgetIndex != 2) {
			emit sliceIndexChanged(2, indexY);//XZ Plane
		}
	}

	void OIVViewer::onColormapChanged(int colormap_idx) {
		//0: grayscale 1: inverse grayscale 2: hot iron 3: JET 4:Rainbow
		for (auto i = 0; i < 3; i++) {
			if (d->qOiv2DRenderWindow[i]) {
				d->qOiv2DRenderWindow[i]->setColorMapIdx(colormap_idx);
			}
			if (d->scalarBar[i]) {
				//d->scalarBar[i]->setInverse(colormap_idx == 1);
				const auto tf = d->scalarBar[i]->GetTransferFunction();
				if (nullptr == tf)
					return;

				//default color is white
				d->scaleBar[i]->SetColor(1.f, 1.f, 1.f);
				switch (colormap_idx) {
					case 0:
						tf->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
						break;
					case 1:
						tf->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY_REVERSED;
					//change label color as black
						d->scaleBar[i]->SetColor(0.f, 0.f, 0.f);
						break;
					case 2:
						tf->predefColorMap = SoTransferFunction::PredefColorMap::GLOW;
						break;
					case 3:
						tf->predefColorMap = SoTransferFunction::PredefColorMap::PHYSICS;
						break;
					case 4:
						tf->predefColorMap = SoTransferFunction::PredefColorMap::STANDARD;
						break;
				}
			}
		}
	}

	void OIVViewer::onLevelChanged(float ri_min, float ri_max) {
		for (auto i = 0; i < 3; i++) {
			if (nullptr != d->sliceRenderRoot[i]) {
				auto range = MedicalHelper::find<SoDataRange>(d->sliceRenderRoot[i].ptr(), "HTSliceRange");
				if (range) {
					range->min = ri_min / d->htValueDiv - d->htValueOffset;
					range->max = ri_max / d->htValueDiv - d->htValueOffset;
					d->curLevelMin = ri_min;
					d->curLevelMax = ri_max;
				}
			}
			if (nullptr != d->scalarBar[i]) {
				d->scalarBar[i]->SetImageWindow(ri_min / 10000.0, ri_max / 10000.0);
			}
		}
	}

	void OIVViewer::onMoveSlice(float value) {
		if (nullptr == d->layoutView) {
			return;
		}
		auto sender = QObject::sender();
		auto renderWidget = (QWidget*)sender;


		MedicalHelper::Axis axis = MedicalHelper::Axis::AXIAL;
		int widgetIndex = -1;

		const auto activatedWidgetType = d->layoutView->FindWidgetType(renderWidget);

		switch (activatedWidgetType) {
			case Entity::ViewType::XY2D:
				axis = MedicalHelper::Axis::AXIAL;
				widgetIndex = 0;
				break;
			case Entity::ViewType::YZ2D:
				axis = MedicalHelper::Axis::SAGITTAL;
				widgetIndex = 1;
				break;
			case Entity::ViewType::ZX2D:
				axis = MedicalHelper::Axis::CORONAL;
				widgetIndex = 2;
				break;
			default: ;
		}

		int currentSlice = -1;

		if ((d->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
			(d->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
			if (axis == MedicalHelper::Axis::AXIAL) {
				currentSlice = d->bufferManager->getHTSliceIndexZ() + 1;
			} else if (axis == MedicalHelper::Axis::SAGITTAL) {
				currentSlice = d->bufferManager->getHTSliceIndexX() + 1;
			} else {
				currentSlice = d->bufferManager->getHTSliceIndexY() + 1;
			}
		} else if ((d->activatedModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume ||
			(d->activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
			if (axis == MedicalHelper::Axis::AXIAL) {
				currentSlice = d->bufferManager->getFLSliceIndexZ() + 1;
			} else if (axis == MedicalHelper::Axis::SAGITTAL) {
				currentSlice = d->bufferManager->getFLSliceIndexX() + 1;
			} else {
				currentSlice = d->bufferManager->getFLSliceIndexY() + 1;
			}
		}
		if (0 < value) {
			currentSlice++;
		} else if (0 > value) {
			currentSlice--;
		}

		emit sliceIndexChanged(widgetIndex, currentSlice);
	}

	void OIVViewer::onDirectionChanged(TC::DirectionType type) {
		int axis = -1;

		switch (type) {
			case TC::DirectionType::X:
				axis = 0;
				break;
			case TC::DirectionType::Y:
				axis = 1;
				break;
			case TC::DirectionType::Z:
				axis = 2;
				break;
		}

		d->qOiv3DRenderWindow->setViewDirection(axis);
	}

	void OIVViewer::onSimulate(QList<TC::RecordAction*> process) {
		d->simulating = true;

		const QFuture<bool> future = QtConcurrent::run([=] {
			return RecordScreen("", TC::RecordType::None, process);
		});

		d->simulationWatcher.setFuture(future);
	}

	void OIVViewer::onStopSimulate() const {
		d->simulating = false;
	}

	void OIVViewer::onFinishedSimulate() {
		d->simulating = false;
		emit finishSimulate();
	}

	void OIVViewer::onStartRecord(QString path, TC::RecordType type, QList<TC::RecordAction*> process) {
		if (true == path.isEmpty()) {
			return;
		}

		d->qOiv3DRenderWindow->setSettingIconViz(false);
		for (auto i = 0; i < 3; i++) {
			d->qOiv2DRenderWindow[i]->setSettingIconViz(false);
		}

		d->recordBuffer.clear();
		d->recordBuffer3D.clear();
		d->recordBufferXY.clear();
		d->recordBufferYZ.clear();
		d->recordBufferXZ.clear();
		d->recordSavePath = path;

		d->simulating = true;

		const QFuture<bool> future = QtConcurrent::run([=] {
			return RecordScreen(path, type, process);
		});

		d->recordWatcher.setFuture(future);
	}

	void OIVViewer::onFinishedRecord() {
		QString text;
		if (true == d->recordWatcher.result()) {
			text = QString("The recorded video file has been saved successfully.\n%1").arg(d->recordSavePath);
		} else {
			text = QString("Failed to save recorded video file.");
		}

		d->qOiv3DRenderWindow->setSettingIconViz(true);
		for (auto i = 0; i < 3; i++) {
			d->qOiv2DRenderWindow[i]->setSettingIconViz(true);
		}

		auto toastMessageBox = new TC::ToastMessageBox(nullptr);
		toastMessageBox->Show(text);
	}

	void OIVViewer::onCaptureScreen(int type) {
		QMutexLocker locker(&d->simulationMutex);

		switch (type) {
			case 0: {
				d->qOiv2DRenderWindow[0]->reset2DView();
				d->recordBuffer.push_back(d->qOiv2DRenderWindow[0]->getRenderBuffer());
				break;
			}
			case 1: {
				d->qOiv2DRenderWindow[1]->reset2DView();
				d->recordBuffer.push_back(d->qOiv2DRenderWindow[1]->getRenderBuffer());
				break;
			}
			case 2: {
				d->qOiv2DRenderWindow[2]->reset2DView();
				d->recordBuffer.push_back(d->qOiv2DRenderWindow[2]->getRenderBuffer());
				break;
			}
			case 3: {
				d->recordBuffer.push_back(d->qOiv3DRenderWindow->getRenderBuffer());
				break;
			}
			case 4: {
				//d->recordBuffer.push_back(this->Grab());
				d->qOiv2DRenderWindow[0]->reset2DView();
				d->recordBufferXY.push_back(d->qOiv2DRenderWindow[0]->getRenderBuffer(1));
				d->qOiv2DRenderWindow[1]->reset2DView();
				d->recordBufferYZ.push_back(d->qOiv2DRenderWindow[1]->getRenderBuffer(1));
				d->qOiv2DRenderWindow[2]->reset2DView();
				d->recordBufferXZ.push_back(d->qOiv2DRenderWindow[2]->getRenderBuffer(1));
				d->recordBuffer3D.push_back(d->qOiv3DRenderWindow->getRenderBuffer());
				break;
			}
			default: ;
		}
	}

	//private
	auto OIVViewer::InitUI(void) -> bool {
		this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

		// init layout
		d->layoutView = new LayoutView(nullptr);
		d->levelWindow = new TC::LevelWindow;

		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			d->qOiv2DRenderWindow[i] = new ViewerRenderWindow2d(d->layoutView);
			auto n2 = QString("TwoD%1").arg(i);
			d->qOiv2DRenderWindow[i]->setName(n2);
			if (i == 0) {
				d->qOiv2DRenderWindow[i]->setRIPick(true);
			}
			d->qOiv2DRenderWindow[i]->setRenderWindowID(0);
			d->qOiv2DRenderWindow[i]->setAxisID(i);
			d->qOiv2DRenderWindow[i]->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,
																QSizePolicy::Expanding));
			d->qOiv2DRenderWindow[i]->forceArrowCursor(true);
			connect(d->qOiv2DRenderWindow[i], SIGNAL(sigWheel(float)), this, SLOT(onMoveSlice(float)), Qt::UniqueConnection);
			connect(d->qOiv2DRenderWindow[i], SIGNAL(sig2dCoord(float, float, float)), this, SLOT(onNavigateSlice(float, float, float)));
			connect(d->qOiv2DRenderWindow[i], SIGNAL(sigMouseMove()), this, SLOT(mouseMoveIn2D()));
			connect(d->qOiv2DRenderWindow[i], SIGNAL(sigColorMap(int)), this, SLOT(onColormapChanged(int)));
			connect(d->qOiv2DRenderWindow[i], SIGNAL(toggleScalarBar(int, bool)), this, SLOT(onToggleScalarBar(int, bool)));

			d->layoutView->AddWidget(Entity::ViewType::_from_integral(i), d->qOiv2DRenderWindow[i]);
		}
		connect(d->qOiv2DRenderWindow[0], SIGNAL(sigTFBox(double, double, double, double)),
				this, SLOT(onSpuitTF(double, double, double, double)));

		d->qOiv3DRenderWindow = new ViewerRenderWindow3d(d->layoutView);
		d->qOiv3DRenderWindow->forceArrowCursor(true);
		d->qOiv3DRenderWindow->setName("ThreeD");
		d->qOiv3DRenderWindow->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,
														QSizePolicy::Expanding));

		connect(d->qOiv3DRenderWindow, SIGNAL(isWhite(bool)), this, SLOT(on3DBackgroundChanged(bool)));
		connect(d->qOiv3DRenderWindow, SIGNAL(sigMouseMove()), this, SLOT(mouseMoveIn3D()));

		d->layoutView->AddWidget(Entity::ViewType::V3D, d->qOiv3DRenderWindow);

		auto layout = new QHBoxLayout(this);
		layout->addWidget(d->layoutView);

		connect(d->levelWindow, SIGNAL(levelChanged(float, float)), this, SLOT(onLevelChanged(float, float)));
		layout->addWidget(d->levelWindow);
		this->setLayout(layout);

		layout->setStretch(0, 1);
		layout->setStretch(1, 0);

		connect(&d->simulationWatcher, SIGNAL(finished()), this, SLOT(onFinishedSimulate()));

		connect(this, SIGNAL(captureScreen(int)), this, SLOT(onCaptureScreen(int)), Qt::BlockingQueuedConnection);
		connect(&d->recordWatcher, SIGNAL(finished()), this, SLOT(onFinishedRecord()));

		show();

		return true;
	}

	void OIVViewer::mouseMoveIn2D() {
		if (d->isIn3D) {
			d->qOiv3DRenderWindow->InteractiveCount(true);
			d->isIn3D = false;
		}
	}

	void OIVViewer::mouseMoveIn3D() {
		if (false == d->isIn3D) {
			d->qOiv3DRenderWindow->InteractiveCount(false);
			d->isIn3D = true;
		}
	}

	void OIVViewer::onToggleScalarBar(int id, bool visible) {
		auto ds = GetSceneDS();
		d->visibleScalarBar[id] = visible;
		if (visible) {
			if ((ds->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume
				|| (ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
				d->scalarBarSwitch[id]->whichChild = -3;
			}
		} else {
			d->scalarBarSwitch[id]->whichChild = -1;
		}
	}

	void OIVViewer::on3DBackgroundChanged(bool isWhite) { }

	auto OIVViewer::InitOIV(void) -> bool {
		return true;
	}

	auto OIVViewer::InitRender(void) -> bool {
		if (nullptr == d->qOiv3DRenderWindow) {
			return false;
		}

		for (int i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			if (nullptr == d->qOiv2DRenderWindow[i]) {
				return false;
			}
		}

		//init root separators
		d->volumeRenderRoot = new SoSeparator;
		for (auto i = 0; i < 3; i++) {
			d->sliceRenderRoot[i] = new SoSeparator;
		}

		// init reader

		//init Axis Grid
		d->axisGridHT = new OivAxisGrid;
		d->axisGridFL = new OivAxisGrid;

		// init volume scene graph
		d->volumeRenderGroup = new SoSeparator;

		d->volumeSwitch = new SoSwitch;
		d->volumeSwitch->addChild(new SoSeparator);
		d->volumeSwitch->whichChild = -1;

		SoRef<SoScale> swap = new SoScale;
		swap->scaleFactor.setValue(1, 1, -1);
		d->volumeRenderGroup->addChild(swap.ptr());
		d->volumeRenderGroup->addChild(d->volumeSwitch.ptr());

		d->volumeRenderRoot->addChild(d->volumeRenderGroup.ptr());

		d->infoGroup = new SoGroup;
		d->infoGroup->setName("TCF_MetaData");
		d->volumeRenderRoot->addChild(d->infoGroup.ptr());

		d->timestampSwitch = new SoSwitch;

		d->annoMatl = new SoMaterial;
		d->annoMatl->diffuseColor.setValue(1, 1, 1);

		d->annoText = new TextBox;
		d->annoText->setName("TimeStamp");
		d->annoText->position.setValue(0.1f, -0.95f, 0);
		d->annoText->alignmentH = TextBox::RIGHT;
		d->annoText->alignmentV = TextBox::BOTTOM;
		d->annoText->fontSize.setValue(20);
		d->annoText->border = FALSE;

		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			d->annoText2d[i] = new TextBox;
			d->annoText2d[i]->setName("TimeStamp");
			d->annoText2d[i]->position.setValue(0.1f, -0.95f, 0);
			d->annoText2d[i]->alignmentH = TextBox::RIGHT;
			d->annoText2d[i]->alignmentV = TextBox::BOTTOM;
			d->annoText2d[i]->fontSize.setValue(20);
			d->annoText2d[i]->border = FALSE;
		}

		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			d->timestampSwitch2d[i] = new SoSwitch;
			d->timestampSwitch2d[i]->addChild(d->annoMatl.ptr());
			d->timestampSwitch2d[i]->addChild(d->annoText2d[i].ptr());
			d->timestampSwitch2d[i]->whichChild = -1;
		}

		d->timestampSwitch->addChild(d->annoMatl.ptr());
		d->timestampSwitch->addChild(d->annoText.ptr());
		d->timestampSwitch->whichChild = -1;
		d->volumeRenderRoot->addChild(d->timestampSwitch.ptr());

		// set volume scene graph to 3D render window
		d->qOiv3DRenderWindow->setSceneGraph(d->volumeRenderRoot.ptr());
		d->qOiv3DRenderWindow->hide();

		d->orientationMarkerSwitch = new SoSwitch;
		d->orientationMarkerSwitch->setName("OrientationMarkerSwitch");
		//d->viewCube = new SoViewingCube;
		d->viewCube = new CustomCube;
		d->viewCube->ref();
		d->viewCube->position = SoViewingCube::PositionInViewport::BOTTOM_LEFT;
		d->viewCube->sceneCamera = d->qOiv3DRenderWindow->getCamera();
		d->viewCube->upAxis = openinventor::inventor::Axis::Z;
		d->viewCube->facePosX = (qApp->applicationDirPath() + "/img/ViewFrontInv.png").toStdString();
		d->viewCube->faceNegX = (qApp->applicationDirPath() + "/img/ViewBackInv.png").toStdString();
		d->viewCube->facePosY = (qApp->applicationDirPath() + "/img/ViewRightInv.png").toStdString();
		d->viewCube->faceNegY = (qApp->applicationDirPath() + "/img/ViewLeftInv.png").toStdString();
		d->viewCube->facePosZ = (qApp->applicationDirPath() + "/img/ViewBottom.png").toStdString();
		d->viewCube->faceNegZ = (qApp->applicationDirPath() + "/img/ViewTop.png").toStdString();
		d->orientationMarkerSwitch->addChild(d->viewCube.ptr());
		d->orientationMarkerSwitch->whichChild = -1;

		d->volumeRenderRoot->addChild(d->orientationMarkerSwitch.ptr());

		// init slice scene graph		
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			d->sliceSwitch[i] = new SoSwitch;
			d->sliceSwitch[i]->addChild(new SoSeparator);
			d->sliceSwitch[i]->whichChild = 0;

			d->sliceRenderRoot[i]->addChild(d->sliceSwitch[i].ptr());

			d->sliceRenderRoot[i]->addChild(d->infoGroup.ptr());
			d->sliceRenderRoot[i]->addChild(d->timestampSwitch2d[i].ptr());
		}
		//d->sliceContainer = new OivSliceContainer;		
		d->sliceConXY = new TC::OivSceneXY;
		d->sliceConYZ = new TC::OivSceneYZ;
		d->sliceConXZ = new TC::OivSceneXZ;
		d->bfContainer = new Oiv2dBFSlice;

		// set slice scene graph to 2D render windows
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			d->qOiv2DRenderWindow[i]->setSceneGraph(d->sliceRenderRoot[i].ptr());
			d->qOiv2DRenderWindow[i]->refreshRangeSlider();
			d->qOiv2DRenderWindow[i]->hide();
		}

		//set window title info		
		d->qOiv2DRenderWindow[0]->setWindowTitle("XY");
		d->qOiv2DRenderWindow[0]->setWindowTitleColor(0.8f, 0.2f, 0.2f);
		d->qOiv2DRenderWindow[1]->setWindowTitle("YZ");
		d->qOiv2DRenderWindow[1]->setWindowTitleColor(0.2f, 0.8f, 0.2f);
		d->qOiv2DRenderWindow[2]->setWindowTitle("XZ");
		d->qOiv2DRenderWindow[2]->setWindowTitleColor(0.2f, 0.2f, 0.8f);
		d->qOiv3DRenderWindow->setWindowTitle("3D");
		d->qOiv3DRenderWindow->setWindowTitleColor(0.8f, 0.8f, 0.2f);

		d->boundaryBoxHTSwitch = new SoSwitch;

		d->boundaryBoxFLSwitch = new SoSwitch;

		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			d->scaleBarSwitch[i] = new SoSwitch;
			d->scalarBarSwitch[i] = new SoSwitch;
			d->manipulatorRoot[i] = new SoSeparator;
		}

		return true;
	}

	auto OIVViewer::BuildUp(const int& modality) -> bool {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.000001;
		};
		if (!d->isBuildScene) {
			BuildHiddenScene();
			Build3DScene();
			Build2DScene();
			d->isBuildScene = true;
			auto metaReader = std::make_shared<TC::IO::TCFMetaReader>();
			auto metaInfo = metaReader->Read(d->filePath);
			auto dimSame = false;
			auto resSame = false;
			if (metaInfo->data.data3D.exist && metaInfo->data.data3DFL.exist) {
				if (AreSame(metaInfo->data.data3D.resolutionX, metaInfo->data.data3DFL.resolutionX)) {
					if (AreSame(metaInfo->data.data3D.resolutionY, metaInfo->data.data3DFL.resolutionY)) {
						if (AreSame(metaInfo->data.data3D.resolutionZ, metaInfo->data.data3DFL.resolutionZ)) {
							resSame = true;
						}
					}
				}
				if (metaInfo->data.data3D.sizeX == metaInfo->data.data3DFL.sizeX) {
					if (metaInfo->data.data3D.sizeY == metaInfo->data.data3DFL.sizeY) {
						if (metaInfo->data.data3D.sizeZ == metaInfo->data.data3DFL.sizeZ) {
							dimSame = true;
						}
					}
				}
			}
			d->sliceConXY->SetUniform(dimSame && resSame);
			d->sliceConYZ->SetUniform(dimSame && resSame);
			d->sliceConXZ->SetUniform(dimSame && resSame);
			d->volumeCon->SetUniform(dimSame && resSame);
			//auto final_offset = metaInfo->data.data3DFL.offsetZ - (metaInfo->data.data3DFL.sizeZ * metaInfo->data.data3DFL.resolutionZ / 2.0);
			//d->volumeContainer->setFLZOffset(static_cast<float>(final_offset));

			for (auto i = 0; i < 3; i++) {
				if (metaInfo->data.data3DFL.exist) {
					//d->volumeContainer->setFLColor(metaInfo->data.data3DFL.r[i], metaInfo->data.data3DFL.g[i], metaInfo->data.data3DFL.b[i], i);
					d->volumeCon->SetFLColor(metaInfo->data.data3DFL.r[i], metaInfo->data.data3DFL.g[i], metaInfo->data.data3DFL.b[i], i);
				} else if (metaInfo->data.data2DFLMIP.exist) {
					//d->volumeContainer->setFLColor(metaInfo->data.data2DFLMIP.r[i], metaInfo->data.data2DFLMIP.g[i], metaInfo->data.data2DFLMIP.b[i], i);
					d->volumeCon->SetFLColor(metaInfo->data.data2DFLMIP.r[i], metaInfo->data.data2DFLMIP.g[i], metaInfo->data.data2DFLMIP.b[i], i);
				}
			}
		}

		if ((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
			(modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
			if (false == this->BuildHTVolume()) {
				return false;
			}
			if (false == this->BuildHTSlice()) {
				return false;
			}
			if (false == this->BuildTFCanvas()) {
				return false;
			}
		}

		if ((modality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume ||
			(modality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
			if (false == this->BuildFLVolume()) {
				return false;
			}

			if (false == this->BuildFLSlice()) {
				return false;
			}
		}
		//BF MIP modality Deprecated 21.07.28 Jose T. Kim

		if ((modality & Entity::Modality::BF2D) == Entity::Modality::BF2D) {
			if (false == this->BuildBF()) {
				return false;
			}
		}

		if (false == this->BuildCamera()) {
			return false;
		}

		if (false == this->BuildScaleBar()) {
			return false;
		}

		if (false == this->BuildScalarBar()) {
			return false;
		}

		return true;
	}

	auto OIVViewer::Build2DScene() const -> bool {
		/*if (nullptr == d->volumeContainer) {
			return false;
		}*/
		if (nullptr == d->volumeCon) {
			return false;
		}
		/*if (nullptr == d->sliceContainer) {
			d->sliceContainer = new OivSliceContainer;
		}*/
		if (nullptr == d->sliceConXY) {
			d->sliceConXY = new TC::OivSceneXY;
		}
		if (nullptr == d->sliceConYZ) {
			d->sliceConYZ = new TC::OivSceneYZ;
		}
		if (nullptr == d->sliceConXZ) {
			d->sliceConXZ = new TC::OivSceneXZ;
		}
		const auto steps = d->metaInfo->data.data3D.dataCount;

		auto buf_step = steps > 1 ? 2 : 1;

		auto isLDM = d->metaInfo->data.isLDM;
		d->sliceConXY->SetHiddenSep(d->hiddenScene->GetSceneGraph());
		d->sliceConXY->buildSceneGraph();
		d->sliceConXY->SetLDM(isLDM);
		d->sliceConXY->SetMetaInfo(d->metaInfo);
		d->sliceConXY->SetHTRange(d->metaInfo->data.data3D.riMin * 10000.0, d->metaInfo->data.data3D.riMax * 10000.0);

		d->sliceConYZ->SetHTExist(d->metaInfo->data.data3D.exist);
		d->sliceConYZ->SetHiddenSep(d->hiddenScene->GetSceneGraph());
		d->sliceConYZ->buildSceneGraph();
		d->sliceConYZ->SetLDM(isLDM);
		d->sliceConYZ->SetMetaInfo(d->metaInfo);
		d->sliceConYZ->SetHTRange(d->metaInfo->data.data3D.riMin * 10000.0, d->metaInfo->data.data3D.riMax * 10000.0);

		d->sliceConXZ->SetHTExist(d->metaInfo->data.data3D.exist);
		d->sliceConXZ->SetHiddenSep(d->hiddenScene->GetSceneGraph());
		d->sliceConXZ->buildSceneGraph();
		d->sliceConXZ->SetLDM(isLDM);
		d->sliceConXZ->SetMetaInfo(d->metaInfo);
		d->sliceConXZ->SetHTRange(d->metaInfo->data.data3D.riMin * 10000.0, d->metaInfo->data.data3D.riMax * 10000.0);


		d->sliceSwitch[0]->replaceChild(0, d->sliceConXY->GetRenderRoot());
		d->sliceSwitch[1]->replaceChild(0, d->sliceConYZ->GetRenderRoot());
		d->sliceSwitch[2]->replaceChild(0, d->sliceConXZ->GetRenderRoot());

		d->qOiv2DRenderWindow[0]->setXY(isLDM);

		/*for (auto i = 0; i<NUMBER_OF_2D_RENDER_WINDOW; i++) {
			d->sliceSwitch[i]->replaceChild(0, d->sliceContainer->getSliceRenderRoot(i));
		}*/

		return true;
	}

	auto OIVViewer::BuildHTSlice(void) const -> bool {
		if (true == d->isBuildHT2D) {
			return true;
		}

		/*if (nullptr == d->sliceContainer) {
			d->sliceContainer = new OivSliceContainer;
		}*/
		if (nullptr == d->sliceConXY) {
			return false;
		}
		if (nullptr == d->sliceConXZ) {
			return false;
		}
		if (nullptr == d->sliceConYZ) {
			return false;
		}

		const auto steps = d->metaInfo->data.data3D.dataCount;

		auto buf_step = steps > 1 ? 2 : 1;

		d->sliceInited = true;


		/*for (auto i = 0; i< buf_step; i++) {
			if (d->metaInfo->data.data2DMIP.exist && false == d->isCrop) {
				//d->sliceContainer->setHTMIP(d->bufferManager->getHT2dVolume(i), i);
				d->sliceConXY->SetMIPVolume(d->bufferManager->getHT2dVolume(i), true);
			}
		}*/

		auto isLDM = d->metaInfo->data.isLDM;
		d->sliceConXY->SetLDM(isLDM);
		d->sliceConYZ->SetLDM(isLDM);
		d->sliceConXZ->SetLDM(isLDM);

		d->isBuildHT2D = true;

		return true;
	}

	auto OIVViewer::BuildFLSlice(void) const -> bool {
		if (true == d->isBuildFL2D) {
			return true;
		}

		/*if(nullptr == d->volumeContainer) {
			return false;
		}

		if(nullptr == d->sliceContainer) {
			d->sliceContainer = new OivSliceContainer;
		}*/
		if (nullptr == d->sliceConXY) {
			return false;
		}
		if (nullptr == d->sliceConXZ) {
			return false;
		}
		if (nullptr == d->sliceConYZ) {
			return false;
		}

		bool flChExist[3] = { false, };

		if (d->metaInfo->data.data3DFL.exist) {
			for (int i = 0; i < 3; i++) {
				if (d->metaInfo->data.data3DFL.valid[i]) {
					flChExist[i] = true;
				}
			}
		}

		//d->sliceContainer->setFLExist(flChExist[0], flChExist[1], flChExist[2]);
		d->sliceConXY->setFLExist(flChExist[0], flChExist[1], flChExist[2]);
		d->sliceConYZ->setFLExist(flChExist[0], flChExist[1], flChExist[2]);
		d->sliceConXZ->setFLExist(flChExist[0], flChExist[1], flChExist[2]);

		auto isLDM = d->metaInfo->data.isLDM;
		//d->sliceContainer->setLDM(isLDM);
		d->sliceConXY->SetLDM(isLDM);
		d->sliceConYZ->SetLDM(isLDM);
		d->sliceConXZ->SetLDM(isLDM);


		const int steps = d->metaInfo->data.data3DFL.dataCount;

		auto buf_step = (steps > 1) ? 2 : 1;

		/*if (false == d->sliceInited) {
			d->sliceContainer->initSliceInfo(); //if no HT volume exist init slice here
		}*/
		/*
		for(auto i=0;i< buf_step;i++) {
			if (false == d->metaInfo->data.data2DFLMIP.exist) {
				continue;
			}
			for(auto ch = 0 ; ch<3;ch++) {
				if (false == d->metaInfo->data.data2DFLMIP.valid[ch] || d->isCrop) {
					continue;
				}
				//d->sliceContainer->setFLMIP(d->bufferManager->getFL2dVolume(i, ch), ch, i);
				d->sliceConXY->SetMIPFLVolume(ch, d->bufferManager->getFL2dVolume(i, ch), true);
			}
		}*/

		d->isBuildFL2D = true;

		return true;
	}

	auto OIVViewer::BuildHiddenScene() const -> bool {
		if (nullptr == d->hiddenScene) {
			d->hiddenScene = new HiddenScene;
		}
		d->hiddenScene->SetImageParentDir(d->curProjPath);
		return true;
	}

	auto OIVViewer::Build3DScene() const -> bool {
		if (nullptr == d->qOiv3DRenderWindow) {
			return false;
		}
		/*
		if (nullptr == d->volumeContainer) {
			d->volumeContainer = new OivVolumeContainer;
		}*/
		if (nullptr == d->volumeCon) {
			d->volumeCon = new TC::OivScene3D;
		}
		//d->volumeContainer->setCurrentProject(d->curProjPath);

		const int steps = d->metaInfo->data.data3D.dataCount;

		auto buf_cnt = d->bufferManager->getNumberOfBuffer();

		auto buf_step = steps > 1 ? buf_cnt : 1;
		d->volumeCon->SetMetaInfo(d->metaInfo);
		d->volumeCon->SetHTRange(d->metaInfo->data.data3D.riMin * 10000.0, d->metaInfo->data.data3D.riMax * 10000.0);
		//d->volumeCon->set2dTF(d->hiddenScene->GetTF2D());
		d->volumeCon->SetHiddenSep(d->hiddenScene->GetSceneGraph());
		//d->volumeContainer->buildVolumeScene(buf_step);

		d->volumeCon->BuildSceneGraph();
		//d->volumeCon->buildSceneGraph();

		//auto root = d->volumeContainer->getRenderRoot();

		d->volumeSwitch->replaceChild(0, d->volumeCon->GetRenderRoot());

		return true;
	}

	auto OIVViewer::BuildHTVolume(void) -> bool {
		if (true == d->isBuildHT3D) {
			return true;;
		}

		if (nullptr == d->qOiv3DRenderWindow) {
			return false;
		}

		if (nullptr == d->volumeCon) {
			//d->volumeCon = new TC::OivVolumeScene;
			d->volumeCon = new TC::OivScene3D;
		}
		//d->volumeCon->setCurProjPath(d->curProjPath);

		const int steps = d->metaInfo->data.data3D.dataCount;


		auto buf_cnt = d->bufferManager->getNumberOfBuffer();

		auto buf_step = steps > 1 ? buf_cnt : 1;

		auto isLDM = d->metaInfo->data.isLDM;

		auto is8bit = d->metaInfo->data.data3D.scalarType[0] == 1;

		//calculate 2D histogram
		d->hiddenScene->SetIs8Bit(is8bit);
		d->hiddenScene->SetHTMinMax(d->metaInfo->data.data3D.riMin * 10000.0, d->metaInfo->data.data3D.riMax * 10000.0);

		d->hiddenScene->Calc2DHistogram(d->bufferManager->getHT3dVolume(0), isLDM);

		for (auto i = 0; i < buf_step; i++) {
			d->volumeCon->SetHTVolume(d->bufferManager->getHT3dVolume(i), i);
			d->sliceConXY->SetVolume(d->bufferManager->getHTXYSlice(i), true);
			d->sliceConYZ->SetVolume(d->bufferManager->getHTYZSlice(i), true);
			d->sliceConXZ->SetVolume(d->bufferManager->getHTXZSlice(i), true);
		}

		auto root = d->volumeCon->GetRenderRoot();

		if (false == BuildHTBoundaryBox(root)) {
			return false;
		}

		d->isBuildHT3D = true;

		return true;
	}

	auto OIVViewer::SetCurrentProjectPath(const QString& path) -> void {
		d->curProjPath = path;
	}


	auto OIVViewer::BuildFLVolume(void) -> bool {
		if (true == d->isBuildFL3D) {
			return true;;
		}

		if (nullptr == d->qOiv3DRenderWindow) {
			return false;
		}

		if (nullptr == d->volumeCon) {
			//d->volumeCon = new TC::OivVolumeScene;
			d->volumeCon = new TC::OivScene3D;
		}
		//d->volumeCon->setCurProjPath(d->curProjPath);

		bool flChExist[3] = { false, };
		if (d->metaInfo->data.data3DFL.exist) {
			for (auto i = 0; i < 3; i++) {
				if (d->metaInfo->data.data3DFL.valid[i]) {
					flChExist[i] = true;
				}
			}
		}
		const bool isFLExist = flChExist[0] || flChExist[1] || flChExist[2];
		if (false == isFLExist) {
			return false;
		}

		const int flsteps = d->metaInfo->data.data3DFL.dataCount;

		auto buf_cnt = d->bufferManager->getNumberOfBuffer();

		auto buf_step = flsteps > 1 ? buf_cnt : 1;

		d->volumeCon->SetFLExist(flChExist[0], flChExist[1], flChExist[2]);
		//d->volumeCon->SetExist(flChExist[0], flChExist[1], flChExist[2]);

		for (auto i = 0; i < buf_step; i++) {
			for (auto ch = 0; ch < 3; ch++) {
				if (false == flChExist[ch]) {
					continue;
				}
				d->volumeCon->SetFLVolume(d->bufferManager->getFL3dVolume(i, ch), ch, i);
				d->sliceConXY->SetFLVolume(ch, d->bufferManager->getFLXYSlice(i, ch), true);
				d->sliceConYZ->SetFLVolume(ch, d->bufferManager->getFLYZSlice(i, ch), true);
				d->sliceConXZ->SetFLVolume(ch, d->bufferManager->getFLXZSlice(i, ch), true);
			}
		}
		auto root = d->volumeCon->GetRenderRoot();

		if (false == BuildFLBoundaryBox(root)) {
			return false;
		}

		d->isBuildFL3D = true;

		return true;
	}

	auto OIVViewer::BuildBF(void) -> bool {
		//redesign 22.04.26		
		if (true == d->isBuildBF) {
			return true;
		}

		if (nullptr == d->bufferManager) {
			return false;
		}

		if (nullptr == d->bfContainer) {
			d->bfContainer = new Oiv2dBFSlice;
		}

		const int steps = d->metaInfo->data.dataBF.dataCount;
		auto buf_cnt = d->bufferManager->getNumberOfBuffer();
		auto buf_step = steps > 1 ? buf_cnt : 1;

		d->bfContainer->buildBFSlice(steps);

		/*for (auto i = 0; i < buf_step; i++) {
			d->bfContainer->setSingleBF(d->bufferManager->getBF2dVolume(i, 0),
										d->bufferManager->getBF2dVolume(i, 1),
										d->bufferManager->getBF2dVolume(i, 2), i);
		}*/

		d->bfContainer->setTransparency(0.0);
		d->bfContainer->setTimeStep(0);

		d->sliceConXY->setBFRoot(d->bfContainer->getRenderRootNode());

		//for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
		//d->sliceSwitch[i]->replaceChild(1, d->bfContainer->getRenderRootNode());
		//}

		d->isBuildBF = true;

		return true;
	}

	auto OIVViewer::BuildTFCanvas(void) -> bool {
		if (true == d->isBuildTFCanvas) {
			return true;
		}

		if (false == d->isBuildHT2D) {
			return false;
		}

		if (nullptr == d->volumeCon) {
			return false;
		}


		if (nullptr == d->tfCanvas) {
			d->tfCanvas = new QTransferFunctionCanvas2D(nullptr);
			d->tfCanvas->setCurProjPath(d->curProjPath);
			auto layout = new QHBoxLayout();
			layout->setContentsMargins(0, 0, 0, 0);
			layout->addWidget(d->tfCanvas);
			d->tfCanvasWidget->setLayout(layout);

			//d->tfCanvas->setVolCon(d->volumeCon);			
			d->tfCanvas->setHidden(d->hiddenScene);

			connect(d->tfCanvas, SIGNAL(sigRender()), this, SLOT(onAddTransferFunction()));
			connect(d->tfCanvas, SIGNAL(sigSelectItem(int, double, double, double, double, double)),
					this, SLOT(onSelectTFItem(int, double, double, double, double, double)));
			connect(d->tfCanvas, SIGNAL(sigCanvasMouse(bool)), this, SLOT(onCanvasMouse(bool)));
		}

		auto min = -1.0;
		auto max = -1.0;
		auto gmin = -1.0;
		auto gmax = -1.0;

		d->volumeCon->GetHTRange(min, max);
		//d->volumeCon->GetHTMinMax(min, max);
		d->tfCanvas->setDataXdecimation(4);
		d->tfCanvas->setDataXSingleStep(0.0001);
		d->tfCanvas->setDataXdivider(10000.0);
		d->tfCanvas->setDataXname("RI");
		d->tfCanvas->setDataXRange(min, max);

		d->tfCanvas->setDataYdecimation(2);
		d->tfCanvas->setDataYSingleStep(0.05);
		d->tfCanvas->setDataYdivider(1.0);
		d->tfCanvas->setDataYname("Gradient");

		d->hiddenScene->GetGradMinMax(gmin, gmax);
		d->tfCanvas->setDataYRange(gmin, gmax);

		d->isBuildTFCanvas = true;

		// gradient min, max�� TCF meta�� �������� �ʱ⶧���� component���� �о�� ���� �����Ѵ�.
		emit gradientRangeLoaded(gmin, gmax);

		return true;
	}

	auto OIVViewer::BuildCamera(void) const -> bool {
		if (true == d->isBuildCamera) {
			return true;
		}

		if (nullptr == d->bufferManager->getDefaultVolume()) {
			return false;
		}

		if (nullptr == d->sliceConXY) {
			return false;
		}
		if (nullptr == d->sliceConXZ) {
			return false;
		}
		if (nullptr == d->sliceConYZ) {
			return false;
		}

		if (nullptr == d->qOiv3DRenderWindow) {
			return false;
		}

		// init camera
		MedicalHelper::Axis ax[3] = { MedicalHelper::AXIAL,
									MedicalHelper::SAGITTAL,
									MedicalHelper::CORONAL
		};

		const auto volData = d->bufferManager->getDefaultVolume();

		MedicalHelper::orientView(ax[0], d->qOiv3DRenderWindow->getCamera(), volData);
		d->qOiv3DRenderWindow->getCamera()->setName("Camera3D");//_3D_Camera

		auto zoom = d->qOiv3DRenderWindow->getCameraZoom();
		d->qOiv3DRenderWindow->setCameraZoom(zoom * 0.7f);

		if (nullptr != d->sliceSwitch[0]) {
			d->sliceSwitch[0]->replaceChild(0, d->sliceConXY->GetRenderRoot());
			auto vol = MedicalHelper::find<SoVolumeData>(d->sliceConXY->GetRenderRoot());
			MedicalHelper::orientView(ax[0], d->qOiv2DRenderWindow[0]->getCamera(), vol);
			auto name = std::string("Camera2D_0"); // _2D_Camera_i
			d->qOiv2DRenderWindow[0]->getCamera()->setName(name.c_str());
		}

		if (nullptr != d->sliceSwitch[1]) {
			d->sliceSwitch[1]->replaceChild(0, d->sliceConYZ->GetRenderRoot());
			auto vol = MedicalHelper::find<SoVolumeData>(d->sliceConYZ->GetRenderRoot());
			MedicalHelper::orientView(ax[1], d->qOiv2DRenderWindow[1]->getCamera(), vol);
			auto name = std::string("Camera2D_1"); // _2D_Camera_i
			d->qOiv2DRenderWindow[1]->getCamera()->setName(name.c_str());
		}

		if (nullptr != d->sliceSwitch[2]) {
			d->sliceSwitch[2]->replaceChild(0, d->sliceConXZ->GetRenderRoot());
			auto vol = MedicalHelper::find<SoVolumeData>(d->sliceConXZ->GetRenderRoot());
			MedicalHelper::orientView(ax[2], d->qOiv2DRenderWindow[2]->getCamera(), vol);
			auto name = std::string("Camera2D_2"); // _2D_Camera_i
			d->qOiv2DRenderWindow[2]->getCamera()->setName(name.c_str());
		}


		d->isBuildCamera = true;

		return true;
	}

	auto OIVViewer::BuildScaleBar(void) -> bool {
		if (true == d->isBuildScaleBar) {
			return true;
		}

		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			if (nullptr == d->qOiv2DRenderWindow[i]) {
				continue;
			}

			d->scaleBarSwitch[i]->whichChild = -3;

			d->scaleBar[i] = new TC::ScaleBarSwitch(d->qOiv2DRenderWindow[i]->getCamera());
			d->scaleBar[i]->SetPosition(0.95f, -0.95f);
			d->scaleBar[i]->SetColor(1.f, 1.f, 1.f);
			d->scaleBar[i]->SetTick(5);
			d->scaleBarSwitch[i]->addChild(d->scaleBar[i]->GetRoot());


			d->manipulatorRoot[i]->setName(("Manip2D_" + std::to_string(i)).c_str());
			d->manipulatorRoot[i]->addChild(new SoSeparator);

			if (d->sliceRenderRoot[i]->findChild(d->manipulatorRoot[i].ptr()) < 0) {
				d->sliceRenderRoot[i]->addChild(d->manipulatorRoot[i].ptr());
			}
			if (d->sliceRenderRoot[i]->findChild(d->scaleBarSwitch[i].ptr()) < 0) {
				d->sliceRenderRoot[i]->addChild(d->scaleBarSwitch[i].ptr());
			}
		}

		d->isBuildScaleBar = true;

		return true;
	}

	auto OIVViewer::BuildScalarBar(void) -> bool {
		if (true == d->isBuildScalarBar) {
			return true;
		}

		auto ri_min = d->metaInfo->data.data3D.riMin * 10000.0;
		auto ri_max = d->metaInfo->data.data3D.riMax * 10000.0;

		//auto tf = d->sliceContainer->getSliceTransFunc();
		auto tf = d->sliceConXY->getSliceTF();


		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			if (nullptr == d->qOiv2DRenderWindow[i]) {
				continue;;
			}

			auto wsize = d->qOiv2DRenderWindow[i]->getViewportRegion().getWindowSize();
			auto wmin = wsize[0];
			if (wmin > wsize[1])
				wmin = wsize[1];

			d->scalarBarSwitch[i]->whichChild = -1;
			d->scalarBarSwitch[i]->setName(("ScalarBarSwitch" + std::to_string(i)).c_str());
			d->scalarBarSep[i] = new SoSeparator;
			d->scalarBarSep[i]->setName(("ScalarBar" + std::to_string(i)).c_str());
			d->scalarBarSwitch[i]->addChild(d->scalarBarSep[i].ptr());
			d->scalarBar[i] = new TC::ScalarBarSwitch;
			auto tcFunc = new SoTransferFunction;
			tcFunc->predefColorMap = SoTransferFunction::PredefColorMap::INTENSITY;
			d->scalarBar[i]->SetTransferFunction(tcFunc);
			d->scalarBar[i]->SetMajorLength(4);
			d->scalarBar[i]->SetNumberOfAnnotation(2);
			d->scalarBar[i]->SetRatio(12);
			d->scalarBar[i]->SetHorizontal(false);
			d->scalarBar[i]->SetPosition(-0.95f, -0.95f);
			d->scalarBar[i]->SetSize(0.02f, 0.3f);
			d->scalarBar[i]->SetFontSize(13.0f);
			d->scalarBar[i]->SetLastAnnotationMargin(340);
			d->scalarBar[i]->SetPrecision(3);
			d->scalarBar[i]->GetRoot()->whichChild = 0;

			d->scalarBarSep[i]->addChild(d->scalarBar[i]->GetRoot());
			d->scalarBar[i]->SetImageWindow(d->metaInfo->data.data3D.riMin, d->metaInfo->data.data3D.riMax);

			if (d->sliceRenderRoot[i]->findChild(d->scalarBarSwitch[i].ptr()) < 0) {
				d->sliceRenderRoot[i]->addChild(d->scalarBarSwitch[i].ptr());
			}
		}

		d->isBuildScalarBar = true;

		return true;
	}

	auto OIVViewer::BuildHTBoundaryBox(SoSeparator* node) -> bool {
		if (nullptr == node) {
			return false;
		}

		if (nullptr == d->bufferManager) {
			return false;
		}

		if (nullptr == d->volumeRenderGroup) {
			return false;
		}

		const auto volData = d->bufferManager->getDefaultVolume();

		auto cam3d = d->qOiv3DRenderWindow->getCamera();
		d->axisGridHT->GenerateBoundaryBox(volData->extent.getValue(), cam3d);
		d->boundaryBoxHTSwitch->addChild(d->axisGridHT->GetRootSceneGraph());
		d->boundaryBoxHTSwitch->setName("HT_BBox");
		if (node->findChild(d->boundaryBoxHTSwitch.ptr()) < 0) {
			node->addChild(d->boundaryBoxHTSwitch.ptr());
		}
		d->axisHT = true;
		return true;
	}

	auto OIVViewer::BuildFLBoundaryBox(SoSeparator* node) -> bool {
		if (nullptr == node) {
			return false;
		}

		if (nullptr == d->bufferManager) {
			return false;
		}

		if (nullptr == d->volumeRenderGroup) {
			return false;
		}

		//const auto volData = d->bufferManager->getHT3dVolume(0);
		auto ch = -1;
		for (auto i = 0; i < 3; i++) {
			if (d->metaInfo->data.data3DFL.valid[i]) {
				ch = i;
				break;
			}
		}
		if (ch > -1) {
			const auto volData = d->bufferManager->getFL3dVolume(0, ch);

			auto cam3d = d->qOiv3DRenderWindow->getCamera();
			d->axisGridFL->GenerateBoundaryBox(volData->extent.getValue(), cam3d);
			d->boundaryBoxFLSwitch->addChild(d->axisGridFL->GetRootSceneGraph());
			d->boundaryBoxFLSwitch->setName("FL BBox");
			if (node->findChild(d->boundaryBoxFLSwitch.ptr()) < 0) {
				node->addChild(d->boundaryBoxFLSwitch.ptr());
			}
			d->axisFL = true;
		}

		return true;
	}


	auto OIVViewer::SetHTMIP(const bool& isHTMIP) const -> void {
		if (true == d->isBuildHT2D) {
			//d->sliceContainer->toggleHTMIP(isHTMIP);			
			d->sliceConXY->ShowHTMIP(isHTMIP);
			if (isHTMIP) {
				//d->qOiv2DRenderWindow[0]->reset2DView();
			}
		}
	}

	auto OIVViewer::SetFLMIP(const bool& isFLMIP) const -> void {
		if (true == d->isBuildFL2D) {
			//d->sliceContainer->toggleFLMIP(isFLMIP);			
			d->sliceConXY->ShowFLMIP(isFLMIP);
			if (isFLMIP) {
				//d->qOiv2DRenderWindow[0]->reset2DView();
			}
		}
	}

	auto OIVViewer::SetMIP(const bool& isMIP) const -> void {
		d->sliceConXY->ShowMIP(isMIP);
		if (isMIP) {
			//d->qOiv2DRenderWindow[0]->reset2DView();
		}
		/*if (true == d->isBuildHT2D) {
			//d->sliceContainer->toggleHTMIP(isMIP);
			d->sliceConXY->ShowMIP(isMIP);
			d->qOiv2DRenderWindow[0]->reset2DView();
		}

		if (true == d->isBuildFL2D) {
			//d->sliceContainer->toggleFLMIP(isMIP);
			d->sliceConXY->ShowFLMIP(isMIP);
			d->qOiv2DRenderWindow[0]->reset2DView();
		}*/
	}

	auto OIVViewer::Render(const Interactor::SceneDS::Pointer& ds) -> bool {
		/*if(nullptr == d->sliceContainer) {
			return false;
		}*/
		if (nullptr == d->sliceConXY) {
			return false;
		}
		if (nullptr == d->sliceConYZ) {
			return false;
		}
		if (nullptr == d->sliceConXZ) {
			return false;
		}
		/*if (nullptr == d->volumeContainer) {
			return false;
		}*/
		if (nullptr == d->volumeCon) {
			return false;
		}

		if (nullptr == d->bufferManager) {
			return false;
		}

		if (nullptr == d->volumeRenderGroup) {
			return false;
		}

		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; ++i) {
			//if (nullptr == d->sliceRenderSwitch[i]) {
			if (nullptr == d->sliceSwitch[i]) {
				return false;
			}
		}

		// set overlay
		if (d->isOverlay != ds->isOverlay) {
			//d->sliceContainer->toggleOverlay(ds->isOverlay);
			d->sliceConXY->SetHTOverlay(ds->isOverlay);
			d->sliceConYZ->SetHTOverlay(ds->isOverlay);
			d->sliceConXZ->SetHTOverlay(ds->isOverlay);
		}
		d->isOverlay = ds->isOverlay;

		// set SoSwitch node
		d->volumeChildIndex = -1;
		d->sliceChildIndex = -1;
		d->sliceTraversalMode = SoMultiSwitch::INCLUDE;

		d->curSliceRes = ds->slice_resolution;
		if(d->curSliceRes < 0 ) {
			d->bufferManager->enableFixedResolution(false, false);
		}else {
			d->bufferManager->enableFixedResolution(true, false);
			d->bufferManager->setFixedResolution(d->curSliceRes, false);
		}
		d->curVolumeRes = ds->volume_resolution;		
		if (d->curVolumeRes < 0) {
			d->bufferManager->enableFixedResolution(false, true);			
		} else {
			d->bufferManager->enableFixedResolution(true, true);
			d->bufferManager->setFixedResolution(d->curVolumeRes,true);
		}

		auto file = GetFileDS();
		d->volumeSwitch->whichChild = 0;
		//d->sliceContainer->toggleBFMIP(false);
		d->sliceConXY->toggleBF(false);
		d->levelWindow->SetSpaceOnly(false);
		if (((ds->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume &&
				(ds->activatedModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume)
			|| ((ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP &&
				(ds->activatedModality & Entity::Modality::BF2D) == Entity::Modality::BF2D)) {
			d->levelWindow->SetSpaceOnly(false);
			d->volumeChildIndex = SO_SWITCH_ALL;

			d->sliceTraversalMode = SoMultiSwitch::EXCLUDE;
			d->sliceChildIndex = BF_INDEX;

			//d->volumeContainer->toggleHTViz(true);
			d->volumeCon->ToggleHT(true);
			//d->sliceContainer->toggleHTViz(true);
			d->sliceConXY->ToggleHTIntensity(true);
			d->sliceConYZ->ToggleHTIntensity(true);
			d->sliceConXZ->ToggleHTIntensity(true);

			d->childIndex = SO_SWITCH_ALL;

			d->bufferManager->setHTSliceIndexZ(ds->viewConfigList[0]->GetSliceIndex() - 1);
			d->bufferManager->setHTSliceIndexX(ds->viewConfigList[1]->GetSliceIndex() - 1);
			d->bufferManager->setHTSliceIndexY(ds->viewConfigList[2]->GetSliceIndex() - 1);
			auto resX = d->metaInfo->data.data3DFL.resolutionX;
			auto phyX = d->metaInfo->data.data3D.resolutionX * (ds->viewConfigList[1]->GetSliceIndex() - 1);
			auto idxFLx = static_cast<int>(phyX / resX);

			auto resY = d->metaInfo->data.data3DFL.resolutionY;
			auto phyY = d->metaInfo->data.data3D.resolutionY * (ds->viewConfigList[2]->GetSliceIndex() - 1);
			auto idxFLy = static_cast<int>(phyY / resY);

			auto offsetZ = d->metaInfo->data.data3DFL.offsetZ - (d->metaInfo->data.data3D.sizeZ * d->metaInfo->data.data3D.resolutionZ / 2);
			auto resZ = d->metaInfo->data.data3DFL.resolutionZ;
			auto phyZ = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex() - 1) - d->metaInfo->data.data3D.sizeZ * d->metaInfo->data.data3D.resolutionZ / 2 - offsetZ + d->metaInfo->data.data3DFL.sizeZ * d->metaInfo->data.data3DFL.resolutionZ / 2;
			auto idxFLz = static_cast<int>(phyZ / resZ);
			if (resZ < 0.01 || d->metaInfo->data.data3DFL.sizeZ == 1) {//exception for only on slice
				idxFLz = -1;
				auto phyComp = d->metaInfo->data.data3D.resolutionZ * (ds->viewConfigList[0]->GetSliceIndex()) - d->metaInfo->data.data3D.sizeZ * d->metaInfo->data.data3D.resolutionZ / 2 - offsetZ + d->metaInfo->data.data3DFL.sizeZ * d->metaInfo->data.data3DFL.resolutionZ / 2;
				if (phyComp < d->metaInfo->data.data3D.resolutionZ && phyComp > 0) {
					idxFLz = 0;
				}
			}
			if (false == (idxFLz < 0 || idxFLz > d->metaInfo->data.data3DFL.sizeZ - 1)) {
				d->bufferManager->setFLSliceIndexZ(idxFLz);
				for (auto i = 0; i < 3; i++) {
					if (d->metaInfo->data.data3DFL.valid[i]) {
						d->sliceConXY->ToggleFLChannel(i, d->curChVisible[i]);
					}
				}
			} else {
				for (auto i = 0; i < 3; i++) {
					if (d->metaInfo->data.data3DFL.valid[i]) {
						d->sliceConXY->ToggleFLChannel(i, false);
					}
				}
			}			
			if (d->metaInfo->data.isLDM) {
				const auto absoluteSliceIndexZ = ds->viewConfigList[0]->GetSliceIndex() - 1;
				const auto htTileSizeZ = d->metaInfo->data.data3D.tileSizeZ;
				const auto relativeSliceIndexZ = static_cast<int>(ceil(absoluteSliceIndexZ / htTileSizeZ));
				d->sliceConXY->SetSliceIndexHT(relativeSliceIndexZ * htTileSizeZ); // For OIV > 2023.1
				d->sliceConXY->SetSliceIndexHTFL(relativeSliceIndexZ * htTileSizeZ); // For OIV > 2023.1
				//d->sliceConXY->SetSliceIndexHT(relativeSliceIndexZ); // For OIV > 2023.2
				//d->sliceConXY->SetSliceIndexHTFL(relativeSliceIndexZ); // For OIV > 2023.2
				//d->sliceConXY->SetSliceIndexHT(absoluteSliceIndexZ);
				//d->sliceConXY->SetSliceIndexHTFL(absoluteSliceIndexZ);
				//d->sliceConYZ->SetSliceIndex(ds->viewConfigList[1]->GetSliceIndex() - 1;
				d->sliceConYZ->SetSliceIndexHT(ds->viewConfigList[1]->GetSliceIndex() - 1);
				d->sliceConYZ->SetSliceIndexFL(idxFLx);				
				d->sliceConXZ->SetSliceIndexHT(ds->viewConfigList[2]->GetSliceIndex() - 1);
				d->sliceConXZ->SetSliceIndexFL(idxFLy);
				//d->bufferManager->setFLSliceIndexX(idxFLx);
				//d->bufferManager->setFLSliceIndexY(idxFLy);				
			} else {
				//d->sliceConYZ->SetSliceIndex(0);
				d->sliceConYZ->SetSliceIndexHT(0);
				d->sliceConYZ->SetSliceIndexFL(0);
				d->sliceConXZ->SetSliceIndexHT(0);
				d->sliceConXZ->SetSliceIndexFL(0);

				if (false == (idxFLx < 0 || idxFLx > d->metaInfo->data.data3DFL.sizeX - 1)) {
					d->bufferManager->setFLSliceIndexX(idxFLx);
					for (auto i = 0; i < 3; i++) {
						if (d->metaInfo->data.data3DFL.valid[i]) {
							d->sliceConYZ->ToggleFLChannel(i, d->curChVisible[i]);
						}
					}
				} else {
					for (auto i = 0; i < 3; i++) {
						if (d->metaInfo->data.data3DFL.valid[i]) {
							d->sliceConYZ->ToggleFLChannel(i, false);
						}
					}
				}
				if (false == (idxFLy < 0 || idxFLy > d->metaInfo->data.data3DFL.sizeY - 1)) {
					d->bufferManager->setFLSliceIndexY(idxFLy);
					for (auto i = 0; i < 3; i++) {
						if (d->metaInfo->data.data3DFL.valid[i]) {
							d->sliceConXZ->ToggleFLChannel(i, d->curChVisible[i]);
						}
					}
				} else {
					for (auto i = 0; i < 3; i++) {
						if (d->metaInfo->data.data3DFL.valid[i]) {
							d->sliceConXZ->ToggleFLChannel(i, false);
						}
					}
				}
			}
		} else if ((ds->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume
			|| (ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
			d->levelWindow->SetSpaceOnly(false);
			d->volumeChildIndex = HT_INDEX;
			d->sliceChildIndex = HT_INDEX;

			d->volumeCon->ToggleHT(true);
			d->sliceConXY->ToggleHTIntensity(true);
			d->sliceConYZ->ToggleHTIntensity(true);
			d->sliceConXZ->ToggleHTIntensity(true);

			d->bufferManager->setHTSliceIndexZ(ds->viewConfigList[0]->GetSliceIndex() - 1);
			d->bufferManager->setHTSliceIndexX(ds->viewConfigList[1]->GetSliceIndex() - 1);
			d->bufferManager->setHTSliceIndexY(ds->viewConfigList[2]->GetSliceIndex() - 1);
			if (d->metaInfo->data.isLDM) {
				const auto absoluteSliceIndexZ = ds->viewConfigList[0]->GetSliceIndex() - 1;
				const auto htTileSizeZ = d->metaInfo->data.data3D.tileSizeZ;
				const auto relativeSliceIndexZ = static_cast<int>(ceil(absoluteSliceIndexZ / htTileSizeZ));
				d->sliceConXY->SetSliceIndexHT(relativeSliceIndexZ * htTileSizeZ);//For OIV > 2023.1
				d->sliceConXY->SetSliceIndexHTFL(relativeSliceIndexZ * htTileSizeZ);//For OIV > 2023.1
				//d->sliceConXY->SetSliceIndexHT(relativeSliceIndexZ);//For OIV > 2023.2
				//d->sliceConXY->SetSliceIndexHTFL(relativeSliceIndexZ);//For OIV > 2023.2
				//d->sliceConXY->SetSliceIndexHT(absoluteSliceIndexZ);
				//d->sliceConXY->SetSliceIndexHTFL(absoluteSliceIndexZ);
				//d->sliceConYZ->SetSliceIndex(ds->viewConfigList[1]->GetSliceIndex() - 1);
				d->sliceConYZ->SetSliceIndexHT(ds->viewConfigList[1]->GetSliceIndex() - 1);				
				//d->sliceConXZ->SetSliceIndex(ds->viewConfigList[2]->GetSliceIndex() - 1);
				d->sliceConXZ->SetSliceIndexHT(ds->viewConfigList[2]->GetSliceIndex() - 1);
			} else {
				d->sliceConXY->SetSliceIndexHT(0);
				//d->sliceConYZ->SetSliceIndex(0);
				d->sliceConYZ->SetSliceIndexHT(0);				
				//d->sliceConXZ->SetSliceIndex(0);
				d->sliceConXZ->SetSliceIndexHT(0);
			}
		} else if ((ds->activatedModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume
			|| (ds->activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
			d->levelWindow->SetSpaceOnly(true);
			d->volumeChildIndex = FL_INDEX;
			d->sliceChildIndex = FL_INDEX;
			for (auto i = 0; i < 3; i++) {
				if (d->metaInfo->data.data3DFL.valid[i]) {
					d->sliceConXY->ToggleFLChannel(i, d->curChVisible[i]);
					d->sliceConYZ->ToggleFLChannel(i, d->curChVisible[i]);
					d->sliceConXZ->ToggleFLChannel(i, d->curChVisible[i]);
				}
			}

			d->bufferManager->setFLSliceIndexZ(ds->viewConfigList[0]->GetSliceIndex() - 1);
			d->bufferManager->setFLSliceIndexX(ds->viewConfigList[1]->GetSliceIndex() - 1);
			d->bufferManager->setFLSliceIndexY(ds->viewConfigList[2]->GetSliceIndex() - 1);
			if (d->metaInfo->data.isLDM) {
				const auto absoluteSliceIndexZ = ds->viewConfigList[0]->GetSliceIndex() - 1;
				const auto flTileSizeZ = d->metaInfo->data.data3DFL.tileSizeZ;
				const auto relativeSliceIndexZ = static_cast<int>(ceil(absoluteSliceIndexZ / flTileSizeZ));
				d->sliceConXY->SetSliceIndexHTFL(relativeSliceIndexZ * flTileSizeZ); // For OIV > 2023.1
				//d->sliceConXY->SetSliceIndexHTFL(relativeSliceIndexZ); // For OIV > 2023.2
				//d->sliceConXY->SetSliceIndexHTFL(absoluteSliceIndexZ);				
				//d->sliceConYZ->SetSliceIndex(ds->viewConfigList[1]->GetSliceIndex() - 1);
				d->sliceConYZ->SetSliceIndexFL(ds->viewConfigList[1]->GetSliceIndex() - 1);
				//d->sliceConXZ->SetSliceIndex(ds->viewConfigList[2]->GetSliceIndex() - 1);
				d->sliceConXZ->SetSliceIndexFL(ds->viewConfigList[2]->GetSliceIndex() - 1);
			} else {
				d->volumeCon->ToggleHT(false);
				d->sliceConXY->ToggleHTIntensity(false);
				d->sliceConYZ->ToggleHTIntensity(false);
				d->sliceConXZ->ToggleHTIntensity(false);

				d->sliceConXY->SetSliceIndexHTFL(0);
				//d->sliceConYZ->SetSliceIndex(0);				
				d->sliceConYZ->SetSliceIndexFL(0);
				//d->sliceConXZ->SetSliceIndex(0);
				d->sliceConXZ->SetSliceIndexFL(0);
			}
		} else if ((ds->activatedModality & Entity::Modality::BF2D) == Entity::Modality::BF2D) {
			d->levelWindow->SetSpaceOnly(true);
			d->volumeChildIndex = -1;
			d->sliceChildIndex = BF_INDEX;
			d->sliceConXY->toggleBF(true);
		}

		d->boundaryBoxHTSwitch->whichChild = -1;
		d->boundaryBoxFLSwitch->whichChild = -1;
		d->timestampSwitch->whichChild = -1;
		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			d->timestampSwitch2d[i]->whichChild = -1;
		}

		switch (d->volumeChildIndex) {
			case HT_INDEX:
				if (ds->visibleBoundaryBox) {
					d->boundaryBoxHTSwitch->whichChild = -3;
					d->axisGridHT->SetAxisVisibility(false);
				}

				if (ds->visibleAxisGrid) {
					d->boundaryBoxHTSwitch->whichChild = -3;
					d->axisGridHT->SetAxisVisibility(true);
					for (auto i = 0; i < 3; i++) {
						d->axisGridHT->SetTextColor(i, ds->axisGridColor[i][0], ds->axisGridColor[i][1], ds->axisGridColor[i][2]);
					}
				}

				d->boundaryBoxFLSwitch->whichChild = -1;

				break;
			case FL_INDEX:
				if (ds->visibleBoundaryBox) {
					d->boundaryBoxFLSwitch->whichChild = -3;
					d->axisGridFL->SetAxisVisibility(false);
				}

				if (ds->visibleAxisGrid) {
					d->boundaryBoxFLSwitch->whichChild = -3;
					d->axisGridFL->SetAxisVisibility(true);
					for (auto i = 0; i < 3; i++) {
						d->axisGridFL->SetTextColor(i, ds->axisGridColor[i][0], ds->axisGridColor[i][1], ds->axisGridColor[i][2]);
					}
				}

				d->boundaryBoxHTSwitch->whichChild = -1;

				break;
			case SO_SWITCH_ALL:
				if (ds->visibleBoundaryBox) {
					d->boundaryBoxHTSwitch->whichChild = -3;
					d->axisGridHT->SetAxisVisibility(false);
				}

				if (ds->visibleAxisGrid) {
					d->boundaryBoxHTSwitch->whichChild = -3;
					d->axisGridHT->SetAxisVisibility(true);
				}

				d->boundaryBoxFLSwitch->whichChild = -1;

				break;
		}
		SetTimeStep(ds->timelapseTime);

		if (d->axisHT) {
			d->axisGridHT->SetFontSize(ds->axisGridFontSize);
		}
		if (d->axisFL) {
			d->axisGridFL->SetFontSize(ds->axisGridFontSize);
		}

		//Orientation Marker
		if (ds->visibleOrientationMarker) {
			d->orientationMarkerSwitch->whichChild = -3;
		} else {
			d->orientationMarkerSwitch->whichChild = -1;
		}

		for (auto i = 0; i < 3; i++) {
			const auto range = MedicalHelper::find<SoDataRange>(d->sliceRenderRoot[i].ptr(), "HTSliceRange");
			if (range) {
				range->min = d->curLevelMin / d->htValueDiv - d->htValueOffset;
				range->max = d->curLevelMax / d->htValueDiv - d->htValueOffset;
			}
		}

		//Time stamp
		if (ds->visibleTimeStamp) {
			d->timestampSwitch->whichChild = -3;
			d->annoText->fontSize = ds->timestampSize;
			for (auto i = 0; i < 3; i++) {
				d->timestampSwitch2d[i]->whichChild = -3;
				d->annoText2d[i]->fontSize = ds->timestampSize;
			}
			auto rr = ds->timestampR / 255.0;
			auto gg = ds->timestampG / 255.0;
			auto bb = ds->timestampB / 255.0;
			d->annoMatl->diffuseColor.setValue(rr, gg, bb);
		}

		//ScalarBar
		if ((ds->activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume
			|| (ds->activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
			for (auto i = 0; i < 3; i++) {
				if (d->visibleScalarBar[i]) {
					d->scalarBarSwitch[i]->whichChild = -3;
				}
			}
		} else {
			for (auto i = 0; i < 3; i++) {
				d->scalarBarSwitch[i]->whichChild = -1;
			}
		}
		return true;
	}

	//auto OIVViewer::SetTimeStep(const double& timeIndex) const ->void {
	auto OIVViewer::SetTimeStep(const double& timeIndex) -> void {
		auto AreSame = [&](double a, double b) {
			return fabs(a - b) < 0.000001;
		};
		//find index of each modality
		auto htIndex = -1;
		auto ht_time = d->metaInfo->data.data3D.timePoints;
		for (auto i = 0; i < ht_time.size(); i++) {
			if (AreSame(ht_time[i], timeIndex)) {
				htIndex = i;
			}
		}
		int flIndex[3] { -1, -1, -1 };
		for (auto ch = 0; ch < 3; ch++) {
			auto fl_time = d->metaInfo->data.data3DFL.timePoints[ch];
			for (auto i = 0; i < fl_time.size(); i++) {
				if (AreSame(fl_time[i], timeIndex)) {
					flIndex[ch] = i;
				}
			}
		}

		auto bfIndex = -1;
		auto bf_time = d->metaInfo->data.dataBF.timePoints;
		for (auto i = 0; i < bf_time.size(); i++) {
			if (AreSame(bf_time[i], timeIndex)) {
				bfIndex = i;
			}
		}

		auto totalIndex = -1;
		auto total_time = d->metaInfo->data.total_time;
		for (auto i = 0; i < total_time.size(); i++) {
			if (AreSame(total_time[i], timeIndex)) {
				totalIndex = i;
			}
		}
		const int hour = static_cast<int>(timeIndex / 3600.0);
		auto numberTxt = QString::number(hour);
		if (numberTxt.length() == 1) {
			numberTxt = "0" + numberTxt;
		}

		const int minHour = static_cast<int>(total_time.first() / 3600.0);
		auto minTxt = QString::number(minHour);
		if (minTxt.length() == 1) {
			minTxt = "0" + minTxt;
		}
		auto minTck = minTxt + ":" + QDateTime::fromTime_t(total_time.first()).toUTC().toString("mm:ss");
		auto minLength = minTck.length();

		auto tick = numberTxt + ":";
		tick += QDateTime::fromTime_t(timeIndex).toUTC().toString("mm:ss");

		auto stepTxt = QString("#(%1/%2)").arg(totalIndex + 1).arg(total_time.size());
		auto finalTime = QString("%1").arg(tick, minLength + 3, ' ');

		auto time_txt = stepTxt + finalTime;

		d->annoText->deleteAll();
		d->annoText->addLine(time_txt.toStdString());

		for (auto i = 0; i < NUMBER_OF_2D_RENDER_WINDOW; i++) {
			d->annoText2d[i]->deleteAll();
			d->annoText2d[i]->addLine(time_txt.toStdString());
		}

		if (htIndex > -1) {//change precision for HT
			if (d->metaInfo->data.data3D.scalarType.count() > htIndex) {
				if (d->metaInfo->data.data3D.scalarType[htIndex] == 1) {
					d->htValueOffset = d->metaInfo->data.data3D.riMinList[htIndex] * 1000.0;
					d->htValueDiv = 10;
				} else {
					d->htValueOffset = 0;
					d->htValueDiv = 1;
				}
			} else if (d->metaInfo->data.data2DMIP.scalarType.count() > htIndex) {
				if (d->metaInfo->data.data2DMIP.scalarType[htIndex] == 1) {
					d->htValueOffset = d->metaInfo->data.data2DMIP.riMinList[htIndex] * 1000.0;
					d->htValueDiv = 10;
				} else {
					d->htValueOffset = 0;
					d->htValueDiv = 1;
				}
			}
		}
		for (auto i = 0; i < 3; i++) {
			if (flIndex[i] > -1) {//change precision for FL
				if (d->metaInfo->data.data3DFL.scalarType[i].count() > flIndex[i]) {
					if (d->metaInfo->data.data3DFL.scalarType[i][flIndex[i]] == 1) {
						d->flValueOffset[i] = d->metaInfo->data.data3DFL.minIntensityList[i][flIndex[i]];
					} else {
						d->flValueOffset[i] = 0;
					}
				} else if (d->metaInfo->data.data2DFLMIP.scalarType[i].count() > flIndex[i]) {
					if (d->metaInfo->data.data2DFLMIP.scalarType[i][flIndex[i]] == 1) {
						d->flValueOffset[i] = d->metaInfo->data.data2DFLMIP.minIntensityList[i][flIndex[i]];
					} else {
						d->flValueOffset[i] = 0;
					}
				}
			}
		}

		if (nullptr != d->volumeCon || nullptr != d->sliceConXY) {
			if (htIndex > -1) {
				d->bufferManager->setTimeStepHT(htIndex);
				d->infos[1]->string.setValue(std::to_string(htIndex));
			}
			for (auto ch = 0; ch < 3; ch++) {
				if (flIndex[ch] > -1) {
					d->bufferManager->setTimeStepFL(flIndex[ch], ch);
				}
			}
			//d->infos[1]->string.setValue(std::to_string(timeIndex));			
		}

		if (nullptr != d->volumeCon) {
			if (htIndex > -1) {
				d->volumeCon->SetHTTimeStep(htIndex);
			} else {
				d->volumeCon->SetHTTimeStep(-1);
			}
			for (auto ch = 0; ch < 3; ch++) {
				if (flIndex[ch] > -1) {
					d->volumeCon->SetFLTimeStep(flIndex[ch], ch);
				} else {
					d->volumeCon->SetFLTimeStep(-1, ch);
				}
			}
		}

		if (nullptr != d->sliceConXY) {
			if (htIndex > -1) {
				d->sliceConXY->SetHTTimeStep(htIndex);
				d->sliceConYZ->SetHTTimeStep(htIndex);
				d->sliceConXZ->SetHTTimeStep(htIndex);
			} else {
				d->sliceConXY->SetHTTimeStep(-1);
				d->sliceConYZ->SetHTTimeStep(-1);
				d->sliceConXZ->SetHTTimeStep(-1);
			}
			for (auto ch = 0; ch < 3; ch++) {
				if (flIndex[ch] > -1) {
					d->sliceConXY->SetFLTimeStep(flIndex[ch], ch);
					d->sliceConYZ->SetFLTimeStep(flIndex[ch], ch);
					d->sliceConXZ->SetFLTimeStep(flIndex[ch], ch);
				} else {
					d->sliceConXY->SetFLTimeStep(-1, ch);
					d->sliceConYZ->SetFLTimeStep(-1, ch);
					d->sliceConXZ->SetFLTimeStep(-1, ch);
				}
			}
		}

		if (nullptr != d->bfContainer) {
			d->bufferManager->setTimeStepBF(bfIndex);
			d->bfContainer->setTimeStep(bfIndex);
		}

		emit timeStepPlayed(timeIndex);
	}

	auto OIVViewer::AnimateOrbit(const int& axis,
								const float& startTime, const float& endTime,
								const int& startAngle, const int& orbit,
								const bool& reverse, const bool& last) -> void {
		if (nullptr == d->qOiv3DRenderWindow) {
			return;
		}

		const auto fps = 60.f;
		const auto time = endTime - startTime; // seconds
		const auto totalFrame = time * fps;
		const auto interval = 1000.f / fps;

		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);
		Delay(frequency, startTime * 1000.f);

		d->qOiv3DRenderWindow->clearOrbit();

		if (axis != 3) {
			const auto radian = qDegreesToRadians(static_cast<float>(orbit));
			auto tic = radian / totalFrame;
			if (true == reverse) {
				tic *= -1;
			}

			// set starting position
			d->qOiv3DRenderWindow->rotate(axis, qDegreesToRadians(static_cast<float>(startAngle - 180)));

			for (auto i = 0; i < totalFrame; ++i) {
				if (false == d->simulating) {
					return;
				}

				auto tpStart = std::chrono::system_clock::now();
				{
					d->qOiv3DRenderWindow->rotate(axis, tic);
				}
				auto tpEnd = std::chrono::system_clock::now();
				auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
				const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;
				if (0 < delay) {
					Delay(frequency, delay);
				};
			}
		} else { // tilting
			const auto direction = 4;

			// when orbit is 360(default), tilt is 2 degree
			const auto tilt = qDegreesToRadians(static_cast<float>(orbit) / 180.f) * 10.f;
			auto tic = tilt * direction / totalFrame;

			if (true == reverse) {
				tic *= -1;
			}

			const auto repeat = totalFrame / direction / 2;
			for (auto i = 0; i < repeat; ++i) {
				auto tpStart = std::chrono::system_clock::now();
				{
					if (false == d->simulating) {
						return;
					}
					d->qOiv3DRenderWindow->rotate(1.f, -1.f, 0.f, -tic);
				}
				auto tpEnd = std::chrono::system_clock::now();
				auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
				const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;
				if (0 < delay) {
					Delay(frequency, delay);
				}
			}

			for (auto i = 0; i < repeat; ++i) {
				auto tpStart = std::chrono::system_clock::now();
				{
					if (false == d->simulating) {
						return;
					}
					d->qOiv3DRenderWindow->rotate(1.f, -1.f, 0.f, tic);
				}
				auto tpEnd = std::chrono::system_clock::now();
				auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
				const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;
				if (0 < delay) {
					Delay(frequency, delay);
				}
			}

			for (auto i = 0; i < repeat; ++i) {
				auto tpStart = std::chrono::system_clock::now();
				{
					if (false == d->simulating) {
						return;
					}
					d->qOiv3DRenderWindow->rotate(1.f, -1.f, 0.f, tic);
				}
				auto tpEnd = std::chrono::system_clock::now();
				auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
				const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;
				if (0 < delay) {
					Delay(frequency, delay);
				}
			}

			for (auto i = 0; i < repeat; ++i) {
				auto tpStart = std::chrono::system_clock::now();
				{
					if (false == d->simulating) {
						return;
					}
					d->qOiv3DRenderWindow->rotate(1.f, -1.f, 0.f, -tic);
				}
				auto tpEnd = std::chrono::system_clock::now();
				auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
				const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;
				if (0 < delay) {
					Delay(frequency, delay);
				}
			}

			for (auto i = 0; i < repeat; ++i) {
				auto tpStart = std::chrono::system_clock::now();
				{
					if (false == d->simulating) {
						return;
					}
					d->qOiv3DRenderWindow->rotate(1.f, 1.f, 0.f, -tic);
				}
				auto tpEnd = std::chrono::system_clock::now();
				auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
				const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;
				if (0 < delay) {
					Delay(frequency, delay);
				}
			}

			for (auto i = 0; i < repeat; ++i) {
				auto tpStart = std::chrono::system_clock::now();
				{
					if (false == d->simulating) {
						return;
					}
					d->qOiv3DRenderWindow->rotate(1.f, 1.f, 0.f, tic);
				}
				auto tpEnd = std::chrono::system_clock::now();
				auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
				const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;
				if (0 < delay) {
					Delay(frequency, delay);
				}
			}

			for (auto i = 0; i < repeat; ++i) {
				auto tpStart = std::chrono::system_clock::now();
				{
					if (false == d->simulating) {
						return;
					}
					d->qOiv3DRenderWindow->rotate(1.f, 1.f, 0.f, tic);
				}
				auto tpEnd = std::chrono::system_clock::now();
				auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
				const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;
				if (0 < delay) {
					Delay(frequency, delay);
				}
			}

			for (auto i = 0; i < repeat; ++i) {
				auto tpStart = std::chrono::system_clock::now();
				{
					if (false == d->simulating) {
						return;
					}
					d->qOiv3DRenderWindow->rotate(1.f, 1.f, 0.f, -tic);
				}
				auto tpEnd = std::chrono::system_clock::now();
				auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
				const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;
				if (0 < delay) {
					Delay(frequency, delay);
				};
			}
		}

		if (true == last) {
			d->simulating = false;
			emit finishSimulate();
		}
	}

	auto OIVViewer::AnimateSlice(const int& sliceType,
								const float& startTime, const float& endTime,
								const int& begin, const int& finish,
								const bool& reverse, const bool& last) -> void {
		// millisecond
		const float interval = (float(endTime - startTime) / float(finish - begin)) * 1000.f;

		auto index = begin;
		auto endIndex = finish;
		auto iterator = 1;
		if (true == reverse) {
			index = finish;
			endIndex = begin;
			iterator = -1;
		}

		Sleep(startTime * 1000.f);

		while (true) {
			if (false == d->simulating) {
				return;
			}

			auto tpStart = std::chrono::system_clock::now();
			{
				emit moveSlice(sliceType, index);

				if (index == endIndex) {
					break;
				}

				index += iterator;
			}
			auto tpEnd = std::chrono::system_clock::now();
			auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
			const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;

			if (0 < delay) {
				Sleep(delay);
			}
		}

		if (true == last) {
			d->simulating = false;
			emit finishSimulate();
		}
	}

	auto OIVViewer::AnimateTimelapse(const int& modality,
									const float& startTime, const float& endTime,
									const int& begin, const int& finish,
									const bool& reverse, const bool& last) -> void {
		Q_UNUSED(modality)
		if (nullptr == d->bufferManager) {
			return;
		}

		// millisecond
		const float interval = (float(endTime - startTime) / float(finish - begin)) * 1000.f;

		auto index = begin;
		auto endIndex = finish;
		auto iterator = 1;
		if (true == reverse) {
			index = finish;
			endIndex = begin;
			iterator = -1;
		}

		Sleep(startTime * 1000.f);

		while (true) {
			if (false == d->simulating) {
				return;
			}

			auto tpStart = std::chrono::system_clock::now();
			{
				SetTimeStep(index);

				if (index == endIndex) {
					break;
				}

				index += iterator;
			}
			auto tpEnd = std::chrono::system_clock::now();
			auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
			const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;

			if (0 < delay) {
				Sleep(delay);
			}
		}

		if (true == last) {
			d->simulating = false;
			emit finishSimulate();
		}
	}

	auto OIVViewer::RecordScreen(QString path, TC::RecordType type, QList<TC::RecordAction*> process) -> bool {
		auto totalEndTime = 0.f;
		const auto fps = 30.f;

		struct SimulationAction {
			enum ActiveWindow {
				NONE = -1,
				XY = 0,
				YZ = 1,
				XZ = 2,
				VOLUME = 3,
				ALL = 4,
			};

			struct OrbitInfo {
				OrbitInfo(float a1, float a2, float a3, float v)
					: x(a1), y(a2), z(a3), value(v) { }

				float x = 0.f;
				float y = 0.f;
				float z = 0.f;
				float value = 0.f;
			};

			ActiveWindow window = ActiveWindow::NONE;

			QList<OrbitInfo> orbit; // axis x, y, z, orbit
			QList<int> slice; // plane, index,
			QList<int> timelapse; // time step
		};

		// make simulation per frame
		QList<SimulationAction> simulationActionList;
		for (auto item : process) {
			if (nullptr == item) {
				continue;
			}

			if (totalEndTime < item->endTime) {
				totalEndTime = item->endTime;
			}
		}

		const auto totalFrame = totalEndTime * fps;

		for (auto item : process) {
			if (nullptr == item) {
				continue;
			}

			const auto actionFrame = (item->endTime - item->startTime) * fps;

			SimulationAction action;
			if (item->type == +TC::ActionType::Orbit) {
				action.window = SimulationAction::VOLUME;

				float axis[3];

				if (item->axis == +TC::ActionAxis::T) {
					const auto direction = 4;

					// when orbit is 360(default), tilt is 2 degree
					const auto tilt = qDegreesToRadians(static_cast<float>(item->finish) / 180.f) * 10.f;
					auto tic = tilt * direction / totalFrame;

					if (true == item->reverse) {
						tic *= -1;
					}

					// start frame
					for (auto i = 1; i < item->startTime * fps; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(0.f, 0.f, 0.f, 0.f));
					}

					// time frame
					const auto repeat = totalFrame / direction / 2;
					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, -1.f, 0.f, -tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, -1.f, 0.f, tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, -1.f, 0.f, tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, -1.f, 0.f, -tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, 1.f, 0.f, -tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, 1.f, 0.f, tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, 1.f, 0.f, tic));
					}

					for (auto i = 0; i < repeat; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(1.f, 1.f, 0.f, -tic));
					}

					// end frame
					for (auto i = 0; i < totalFrame - actionFrame; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(0.f, 0.f, 0.f, 0.f));
					}
				} else {
					if (item->axis == +TC::ActionAxis::X) {
						axis[0] = 0.f;
						axis[1] = 1.f;
						axis[2] = 0.f;
					} else if (item->axis == +TC::ActionAxis::Y) {
						axis[0] = 1.f;
						axis[1] = 0.f;
						axis[2] = 0.f;
					} else if (item->axis == +TC::ActionAxis::Z) {
						axis[0] = 0.f;
						axis[1] = 0.f;
						axis[2] = 1.f;
					}

					const auto radian = qDegreesToRadians(static_cast<float>(item->finish));
					auto tic = radian / actionFrame;
					if (true == item->reverse) {
						tic *= -1;
					}

					// start frame
					const auto startRadian = qDegreesToRadians(static_cast<float>(item->begin - 180));
					action.orbit.append(SimulationAction::OrbitInfo(axis[0], axis[1], axis[2], startRadian));

					for (auto i = 1; i < item->startTime * fps; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(0.f, 0.f, 0.f, 0.f));
					}

					// time frame
					for (auto i = 0; i < item->endTime * fps; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(axis[0], axis[1], axis[2], tic));
					}

					// end frame
					for (auto i = 0; i < totalFrame - actionFrame; ++i) {
						action.orbit.append(SimulationAction::OrbitInfo(0.f, 0.f, 0.f, 0.f));
					}
				}
			} else if (item->type == +TC::ActionType::Slice) {
				if (item->windowType == +TC::ActionWindow::XY) {
					action.window = SimulationAction::XY;
				} else if (item->windowType == +TC::ActionWindow::YZ) {
					action.window = SimulationAction::YZ;
				} else if (item->windowType == +TC::ActionWindow::XZ) {
					action.window = SimulationAction::XZ;
				}

				// start frame
				for (auto i = 0; i < item->startTime * fps; ++i) {
					action.slice.append(-1);
				}

				// time frame
				auto startIndex = item->begin;
				auto endIndex = item->finish;
				float tic = (endIndex - startIndex + 1) / (actionFrame - 2);

				if (true == item->reverse) {
					startIndex = item->finish;
					endIndex = item->begin;
				}

				// time frame -> first
				action.slice.append(startIndex);

				for (auto i = 1; i < actionFrame - 1; ++i) {
					auto index = startIndex + ceil(i * tic);

					if (true == item->reverse) {
						index = startIndex - ceil(i * tic);
					}

					if (index < item->begin) {
						index = item->begin;
					}

					if (index > item->finish) {
						index = item->finish;
					}

					action.slice.append(index);
				}

				// time frame -> last
				action.slice.append(endIndex);

				// end frame
				for (auto i = 0; i < totalFrame - actionFrame; ++i) {
					action.slice.append(-1);
				}
			} else if (item->type == +TC::ActionType::Timelapse) {
				action.window = SimulationAction::ALL;
				// start frame
				for (auto i = 0; i < item->startTime * fps; ++i) {
					action.timelapse.append(-1);
				}

				auto startIndex = item->begin;
				auto endIndex = item->finish;
				//float tic = (endIndex - startIndex + 1) / (actionFrame - 2);
				float tic = (endIndex - startIndex + 1) / (actionFrame);

				if (true == item->reverse) {
					startIndex = item->finish;
					endIndex = item->begin;
				}

				// time frame -> first
				//action.timelapse.append(startIndex);				
				//for (auto i = 1; i < actionFrame -1; ++i) {
				for (auto i = 0; i < actionFrame; i++) {
					auto index = startIndex + i * tic;

					if (true == item->reverse) {
						index = startIndex - i * tic;
					}

					if (index < item->begin) {
						index = item->begin;
					}

					if (index > item->finish) {
						index = item->finish;
					}
					action.timelapse.append((int)index);
				}


				// time frame -> last
				//action.timelapse.append(endIndex);


				// end frame
				for (auto i = 0; i < totalFrame - actionFrame - 1; ++i) {
					action.timelapse.append(-1);
				}
			}

			simulationActionList.append(action);
		}

		// save buffer
		auto interval = 1000.f / fps;

		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);

		auto prevTimeStep = -1;

		d->qOiv3DRenderWindow->clearOrbit();

		for (auto i = 0; i < totalFrame; ++i) {
			if (false == d->simulating) {
				break;
			}
			auto tpStart = std::chrono::system_clock::now();
			{
				for (const auto& action : simulationActionList) {
					if (false == d->simulating) {
						break;
					}
					if (action.window == SimulationAction::VOLUME) {
						if (0 != action.orbit.at(i).value) {
							d->qOiv3DRenderWindow->rotate(action.orbit.at(i).x,
														action.orbit.at(i).y,
														action.orbit.at(i).z,
														action.orbit.at(i).value);
						}
					} else if (action.window == SimulationAction::ALL) {
						if (0 <= action.timelapse.at(i)) {
							if (prevTimeStep != action.timelapse.at(i)) {
								emit timeStepChanged(d->time_points[action.timelapse.at(i)]);
								prevTimeStep = action.timelapse.at(i);
							}
						}
					} else {
						if (0 <= action.slice.at(i)) {
							emit moveSlice(action.window, action.slice.at(i));
						}
					}
				}
				if (type != +TC::RecordType::None) {
					emit captureScreen(type._to_integral());
				}
				//std::cout << etimer.elapsed() << " ";
			}
			auto tpEnd = std::chrono::system_clock::now();
			auto micro = std::chrono::duration_cast<std::chrono::microseconds>(tpEnd - tpStart);
			const auto delay = ((interval * 1000.f) - micro.count()) / 1000.f;

			if (0 < delay) {
				Sleep(delay);
			}
		}

		d->simulating = false;

		// save file

		if (true == path.isEmpty()) {
			return true;
		}
		std::list<cv::Mat> cvBuffer;
		if (type._to_integral() != 4) {
			for (const auto& frame : d->recordBuffer) {
				QImage image = frame.toImage().convertToFormat(QImage::Format_RGB888);
				cv::Mat mat(image.height(), image.width(),
							CV_8UC3, (void*)image.constBits(),
							image.bytesPerLine());
				cv::Mat matNoAlpha;
				cv::cvtColor(mat, matNoAlpha, cv::COLOR_RGBA2BGR);

				cvBuffer.push_back(matNoAlpha);
			}
		} else {//combine multiple buffers					
			//merge after buffering to avoid mis matching
			MergeScreens(cvBuffer);
		}
		TC::IO::TCVideoWriter writer;
		auto result = writer.Write(path.toStdString(), cvBuffer);

		//std::cout << "Total Elapsed time for OpenCV save: " << etimer.elapsed() << std::endl;

		return result;
	}

	auto OIVViewer::MergeScreens(std::list<cv::Mat>& cvBuffer) -> void {
		if (d->layoutType._to_index() == Entity::LayoutType::TwoByTwo) {//2x2 layout
			for (auto i = 0; i < d->recordBuffer3D.size(); i++) {
				auto frame3D = d->recordBuffer3D[i];
				auto frameXY = d->recordBufferXY[i];
				auto frameYZ = d->recordBufferYZ[i];
				auto frameXZ = d->recordBufferXZ[i];

				auto image3D = frame3D.toImage().convertToFormat(QImage::Format_RGB888);
				auto imageXY = frameXY.toImage().convertToFormat(QImage::Format_RGB888);
				auto imageYZ = frameYZ.toImage().convertToFormat(QImage::Format_RGB888);
				auto imageXZ = frameXZ.toImage().convertToFormat(QImage::Format_RGB888);

				cv::Mat target = cv::Mat::zeros(image3D.height() + imageYZ.height(), imageXY.width() + image3D.width(), CV_8UC3);

				cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
				cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());
				cv::Mat matYZ(imageYZ.height(), imageYZ.width(), CV_8UC3, (void*)imageYZ.constBits(), imageYZ.bytesPerLine());
				cv::Mat matXZ(imageXZ.height(), imageXZ.width(), CV_8UC3, (void*)imageXZ.constBits(), imageXZ.bytesPerLine());

				mat3D.copyTo(target(cv::Rect(imageXY.width(), imageXY.height(), image3D.width(), image3D.height())));
				matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));
				matYZ.copyTo(target(cv::Rect(imageXY.width(), 0, imageYZ.width(), imageYZ.height())));
				matXZ.copyTo(target(cv::Rect(0, imageXY.height(), imageXZ.width(), imageXZ.height())));


				cv::Mat matNoAlpha;
				cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);
				cvBuffer.push_back(matNoAlpha);
			}
		} else if (d->layoutType._to_index() == Entity::LayoutType::HSlicesBy3D) {//2D image left, 3D right			
			for (auto i = 0; i < d->recordBuffer3D.size(); i++) {
				auto frame3D = d->recordBuffer3D[i];
				auto frameXY = d->recordBufferXY[i];
				auto frameYZ = d->recordBufferYZ[i];
				auto frameXZ = d->recordBufferXZ[i];

				auto image3D = frame3D.toImage().convertToFormat(QImage::Format_RGB888);
				auto imageXY = frameXY.toImage().convertToFormat(QImage::Format_RGB888);
				auto imageYZ = frameYZ.toImage().convertToFormat(QImage::Format_RGB888);
				auto imageXZ = frameXZ.toImage().convertToFormat(QImage::Format_RGB888);

				cv::Mat target = cv::Mat::zeros(image3D.height(), imageXY.width() + image3D.width(), CV_8UC3);

				cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
				cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());
				cv::Mat matYZ(imageYZ.height(), imageYZ.width(), CV_8UC3, (void*)imageYZ.constBits(), imageYZ.bytesPerLine());
				cv::Mat matXZ(imageXZ.height(), imageXZ.width(), CV_8UC3, (void*)imageXZ.constBits(), imageXZ.bytesPerLine());

				mat3D.copyTo(target(cv::Rect(imageXY.width(), 0, image3D.width(), image3D.height())));
				matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));
				matYZ.copyTo(target(cv::Rect(0, imageXY.height(), imageYZ.width(), imageYZ.height())));
				matXZ.copyTo(target(cv::Rect(0, imageXY.height() + imageYZ.height(), imageXZ.width(), imageXZ.height())));


				cv::Mat matNoAlpha;
				cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);
				cvBuffer.push_back(matNoAlpha);
			}
		} else if (d->layoutType._to_index() == Entity::LayoutType::VSlicesBy3D) {//2D image top, 3D right
			for (auto i = 0; i < d->recordBuffer3D.size(); i++) {
				auto frame3D = d->recordBuffer3D[i];
				auto frameXY = d->recordBufferXY[i];
				auto frameYZ = d->recordBufferYZ[i];
				auto frameXZ = d->recordBufferXZ[i];

				auto image3D = frame3D.toImage().convertToFormat(QImage::Format_RGB888);
				auto imageXY = frameXY.toImage().convertToFormat(QImage::Format_RGB888);
				auto imageYZ = frameYZ.toImage().convertToFormat(QImage::Format_RGB888);
				auto imageXZ = frameXZ.toImage().convertToFormat(QImage::Format_RGB888);

				cv::Mat target = cv::Mat::zeros(image3D.height() + imageXY.height(), image3D.width(), CV_8UC3);

				cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
				cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());
				cv::Mat matYZ(imageYZ.height(), imageYZ.width(), CV_8UC3, (void*)imageYZ.constBits(), imageYZ.bytesPerLine());
				cv::Mat matXZ(imageXZ.height(), imageXZ.width(), CV_8UC3, (void*)imageXZ.constBits(), imageXZ.bytesPerLine());

				mat3D.copyTo(target(cv::Rect(0, imageXY.height(), image3D.width(), image3D.height())));
				matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));
				matYZ.copyTo(target(cv::Rect(imageXY.width(), 0, imageYZ.width(), imageYZ.height())));
				matXZ.copyTo(target(cv::Rect(imageXY.width() + imageYZ.width(), 0, imageXZ.width(), imageXZ.height())));


				cv::Mat matNoAlpha;
				cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);
				cvBuffer.push_back(matNoAlpha);
			}
		} else if (d->layoutType._to_index() == Entity::LayoutType::HXY3D) {//XY Plane left, 3D right
			for (auto i = 0; i < d->recordBuffer3D.size(); i++) {
				auto frame3D = d->recordBuffer3D[i];
				auto frameXY = d->recordBufferXY[i];

				auto image3D = frame3D.toImage().convertToFormat(QImage::Format_RGB888);
				auto imageXY = frameXY.toImage().convertToFormat(QImage::Format_RGB888);

				cv::Mat target = cv::Mat::zeros(image3D.height(), image3D.width() + imageXY.width(), CV_8UC3);

				cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
				cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());

				mat3D.copyTo(target(cv::Rect(imageXY.width(), 0, image3D.width(), image3D.height())));
				matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));

				cv::Mat matNoAlpha;
				cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);
				cvBuffer.push_back(matNoAlpha);
			}
		} else if (d->layoutType._to_index() == Entity::LayoutType::VXY3D) {//XY Plane top, 3D bottom
			for (auto i = 0; i < d->recordBuffer3D.size(); i++) {
				auto frame3D = d->recordBuffer3D[i];
				auto frameXY = d->recordBufferXY[i];

				auto image3D = frame3D.toImage().convertToFormat(QImage::Format_RGB888);
				auto imageXY = frameXY.toImage().convertToFormat(QImage::Format_RGB888);

				cv::Mat target = cv::Mat::zeros(image3D.height() + imageXY.height(), image3D.width(), CV_8UC3);

				cv::Mat mat3D(image3D.height(), image3D.width(), CV_8UC3, (void*)image3D.constBits(), image3D.bytesPerLine());
				cv::Mat matXY(imageXY.height(), imageXY.width(), CV_8UC3, (void*)imageXY.constBits(), imageXY.bytesPerLine());

				mat3D.copyTo(target(cv::Rect(0, imageXY.height(), image3D.width(), image3D.height())));
				matXY.copyTo(target(cv::Rect(0, 0, imageXY.width(), imageXY.height())));

				cv::Mat matNoAlpha;
				cv::cvtColor(target, matNoAlpha, cv::COLOR_RGBA2BGR);
				cvBuffer.push_back(matNoAlpha);
			}
		} else if (d->layoutType._to_index() == Entity::LayoutType::XYPlane) {
			for (const auto& frame : d->recordBufferXY) {
				QImage image = frame.toImage().convertToFormat(QImage::Format_RGB888);
				cv::Mat mat(image.height(), image.width(),
							CV_8UC3, (void*)image.constBits(),
							image.bytesPerLine());
				cv::Mat matNoAlpha;
				cv::cvtColor(mat, matNoAlpha, cv::COLOR_RGBA2BGR);

				cvBuffer.push_back(matNoAlpha);
			}
		} else if (d->layoutType._to_index() == Entity::LayoutType::YZPlane) {
			for (const auto& frame : d->recordBufferYZ) {
				QImage image = frame.toImage().convertToFormat(QImage::Format_RGB888);
				cv::Mat mat(image.height(), image.width(),
							CV_8UC3, (void*)image.constBits(),
							image.bytesPerLine());
				cv::Mat matNoAlpha;
				cv::cvtColor(mat, matNoAlpha, cv::COLOR_RGBA2BGR);

				cvBuffer.push_back(matNoAlpha);
			}
		} else if (d->layoutType._to_index() == Entity::LayoutType::XZPlane) {
			for (const auto& frame : d->recordBufferXZ) {
				QImage image = frame.toImage().convertToFormat(QImage::Format_RGB888);
				cv::Mat mat(image.height(), image.width(),
							CV_8UC3, (void*)image.constBits(),
							image.bytesPerLine());
				cv::Mat matNoAlpha;
				cv::cvtColor(mat, matNoAlpha, cv::COLOR_RGBA2BGR);

				cvBuffer.push_back(matNoAlpha);
			}
		} else if (d->layoutType._to_index() == Entity::LayoutType::Only3D) {
			for (const auto& frame : d->recordBuffer3D) {
				QImage image = frame.toImage().convertToFormat(QImage::Format_RGB888);
				cv::Mat mat(image.height(), image.width(),
							CV_8UC3, (void*)image.constBits(),
							image.bytesPerLine());
				cv::Mat matNoAlpha;
				cv::cvtColor(mat, matNoAlpha, cv::COLOR_RGBA2BGR);

				cvBuffer.push_back(matNoAlpha);
			}
		}
	}


	auto OIVViewer::WindowIndex2MedicalHelperAxis(const int& windowIndex) -> int {
		MedicalHelper::Axis axis = MedicalHelper::Axis::AXIAL;

		switch (windowIndex) {
			case Entity::ViewType::XY2D:
				axis = MedicalHelper::Axis::AXIAL;
				break;
			case Entity::ViewType::YZ2D:
				axis = MedicalHelper::Axis::SAGITTAL;
				break;
			case Entity::ViewType::ZX2D:
				axis = MedicalHelper::Axis::CORONAL;
				break;
			default: ;
		}

		return axis;
	}

	auto OIVViewer::ChangeLayout(const Entity::LayoutType& layoutType) const -> void {
		d->layoutView->SetLayoutType(layoutType);
		if (nullptr != d->qOiv3DRenderWindow) {
			d->qOiv3DRenderWindow->reset3DView();
		}
		for (auto window : d->qOiv2DRenderWindow) {
			if (nullptr != window) {
				window->reset2DView();
			}
		}

		d->layoutType = layoutType;
	}

	auto OIVViewer::Grab() -> QPixmap {
		auto GetOffWidget = [=](const QPixmap& pixmap, QWidget* parent) {
			auto label = new QLabel;
			label->setPixmap(pixmap);
			label->setScaledContents(true);

			auto layout = new QHBoxLayout;
			layout->setContentsMargins(0, 0, 0, 0);
			layout->setSpacing(0);
			layout->addWidget(label);

			auto widget = new QWidget(parent);
			widget->setContentsMargins(0, 0, 0, 0);
			widget->setLayout(layout);

			return widget;
		};

		if (nullptr == d->layoutView) {
			return QPixmap();
		}

		d->layoutView->SaveCurrent();

		const auto bufferXY = d->qOiv2DRenderWindow[0]->getRenderBuffer(1);
		const auto bufferYZ = d->qOiv2DRenderWindow[1]->getRenderBuffer(1);
		const auto bufferZX = d->qOiv2DRenderWindow[2]->getRenderBuffer(1);
		const auto buffer3D = d->qOiv3DRenderWindow->getRenderBuffer();

		auto offscreenWidget = new QWidget(nullptr);
		offscreenWidget->setFixedSize(this->size());

		auto layoutView = new LayoutView(offscreenWidget);
		layoutView->AddWidget(Entity::ViewType::XY2D, GetOffWidget(bufferXY, layoutView));
		layoutView->AddWidget(Entity::ViewType::YZ2D, GetOffWidget(bufferYZ, layoutView));
		layoutView->AddWidget(Entity::ViewType::ZX2D, GetOffWidget(bufferZX, layoutView));
		layoutView->AddWidget(Entity::ViewType::V3D, GetOffWidget(buffer3D, layoutView));
		layoutView->LoadCurrent();

		auto layout = new QHBoxLayout(offscreenWidget);
		layout->addWidget(layoutView);

		offscreenWidget->setLayout(layout);

		auto pixmap = offscreenWidget->grab();

		delete offscreenWidget;
		offscreenWidget = nullptr;

		return pixmap;
	}

	auto OIVViewer::RequestRender() -> void {
		if (d->qOiv3DRenderWindow)
			d->qOiv3DRenderWindow->immediateRender();
		for (int i = 0; i < 3; i++) {
			if (d->qOiv2DRenderWindow[i])
				d->qOiv2DRenderWindow[i]->immediateRender();
		}
	}

	void OIVViewer::onCanvasMouse(bool pressed) {
		d->qOiv3DRenderWindow->InteractiveCount(pressed);
	}

	void OIVViewer::onSpuitTF(double ri_min, double ri_max, double grad_min, double grad_max) {
		QColor new_col = QColorDialog::getColor(Qt::white, nullptr, "Select Color");
		d->tfCanvas->AddItem(ri_min * 10000.0, ri_max * 10000.0,
							grad_min, grad_max,
							new_col.red(), new_col.green(), new_col.blue(),
							0.5);
	}

	void OIVViewer::Delay(const LARGE_INTEGER& frequency, const float& millisecond) {
		static LARGE_INTEGER BeginTime;
		static LARGE_INTEGER EndTime;
		QueryPerformanceCounter(&BeginTime);

		while (true) {
			QueryPerformanceCounter(&EndTime);
			if ((static_cast<float>(EndTime.QuadPart - BeginTime.QuadPart) /
				static_cast<float>(frequency.QuadPart)) * 1000.0f >= millisecond)
				break;
		}
	}
}
