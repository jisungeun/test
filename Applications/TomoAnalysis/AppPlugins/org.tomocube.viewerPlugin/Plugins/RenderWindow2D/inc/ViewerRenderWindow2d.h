#pragma once

#include <QOivRenderWindow.h>
#include <QDoubleSpinBox>
#include <QWidgetAction>

#include "ViewerRenderWindow2DExport.h"

class SoEventCallback;
class SoVolumeData;

namespace TomoAnalysis::Viewer::Plugins {
    class ViewerRenderWindow2D_API ViewerRenderWindow2d : public QOivRenderWindow {
        Q_OBJECT
    public:
        ViewerRenderWindow2d(QWidget* parent);
        ~ViewerRenderWindow2d();

        auto closeEvent(QCloseEvent* unused) -> void override;

        auto reset2DView(bool fromFunc = false)->void;
        auto resetViewBF(bool isBF)->bool;
        auto setHTRange(double min, double max)->void;
        auto setRenderWindowID(int idx)->void;
        auto setRIPick(bool setRI)->void;
        auto refreshRangeSlider()->void;
        auto setType(bool is2D)->void;
        auto resetOnLoad()->void;
        auto setColorMapIdx(int idx)->void;
        auto setXY(bool isXY)->void;
        auto setIsLdm(bool isLdm)->void;

        auto setAxisID(int id)->void;

    signals:
        void sigWheel(float);
        void sigTFBox(double, double, double, double);
        void sig2dCoord(float, float, float);
        void sigMouseMove();
        void sigColorMap(int);
        void toggleScalarBar(int, bool);

    protected slots:
        void OnFinishSpuit();

    private:
        void resizeEvent(QResizeEvent* event) override;

        auto MouseButtonEvent(SoEventCallback* node) -> void override;
        auto MouseMoveEvent(SoEventCallback* node) -> void override;
        auto KeyboardEvent(SoEventCallback* node) -> void override;
        //void wheelEvent(QWheelEvent* event) override;
        auto MouseWheelEvent(SoEventCallback* node) -> void override;

        auto setDefaultWindowType() -> void override;

        auto showContextMenu(SbVec2f pos)->void;

        auto change2DColorMap(int idx)->void;
        auto initSpuit()->void;

        auto toggleScalarBar()->void;
        auto ScalarBarMove(bool activate)->void;
        auto ScalarBarScale(bool activate)->void;

        auto CalcVolumeCoord(SbVec2f norm_point)->SbVec3f;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}