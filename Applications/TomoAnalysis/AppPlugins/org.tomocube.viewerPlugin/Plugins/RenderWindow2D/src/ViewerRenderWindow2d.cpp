#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QVBoxLayout>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/draggers/SoScale2Dragger.h>
#include <Inventor/draggers/SoScale2UniformDragger.h>
#include <Inventor/draggers/SoTranslate2Dragger.h>
#include <Inventor/engines/SoCompose.h>
#include <Inventor/engines/SoCalculator.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/nodes/SoInfo.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoMultiSwitch.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <Inventor/nodes/SoRotation.h>
#include <Medical/helpers/MedicalHelper.h>
#include <Medical/nodes/TextBox.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/actions/SoGetBoundingBoxAction.h>
#pragma warning(pop)

#include <Oiv2DSpuit.h>
#include <RangeSlider.h>
#include <SpinBoxAction.h>
#include <OivRangeBar.h>
#include <OivScaleBar.h>

#include "ViewerRenderWindow2d.h"

namespace TomoAnalysis::Viewer::Plugins {
    struct ViewerRenderWindow2d::Impl {
        //colormap
        int colorMap{ 0 }; //0: grayscale 1: inverse grayscale 2: hot iron 3: JET 4:Rainbow

        //scalarBar
        bool is_scalarBar{ false };
        bool is_scalarTrans{ false };
        bool is_scalarScale{ false };

        //levelWindow
        int level_min{ 0 };
        int level_max{ 100 };

        //spuit
        Oiv2DSpuit* spuit;
        bool is_spuit{ false };

        bool is_reload{ false };

        bool is_delete{ false };
        int ID{ -1 };
        int AxisID{ -1 };
        bool isRIpick{ false };

        bool right_mouse_pressed{ false };
        bool middle_mouse_pressed{ false };
        bool left_mouse_pressed{ false };
        double right_stamp{ 0.0 };
        double prev_scale{ 1.0 };
        bool is2D{ false };
        bool isBF{ false };
        bool isLDM{ false };

        bool isXY{ false };
        bool isFirstResize{ false };

        bool scalar_pressed{ false };
        float scalar_pressed_pos[2]{ 0,0 };
        bool time_pressed{ false };
        float time_pressed_pos[2]{ 0,0 };
        bool scale_pressed{ false };
        float scale_pressed_pos[2]{ 0,0 };
    };

    ViewerRenderWindow2d::ViewerRenderWindow2d(QWidget* parent) :
        QOivRenderWindow(parent, true), d{ new Impl } {
        setDefaultWindowType();
    }

    ViewerRenderWindow2d::~ViewerRenderWindow2d() {

    }

    auto ViewerRenderWindow2d::setRIPick(bool isRI)->void {
        d->isRIpick = isRI;
    }

    auto ViewerRenderWindow2d::resetOnLoad() -> void {
        d->is_scalarBar = false;
        d->colorMap = 0;
    }

    auto ViewerRenderWindow2d::setType(bool is2D) -> void {
        d->is2D = is2D;
    }

    auto ViewerRenderWindow2d::showContextMenu(SbVec2f pos) -> void {
        //y position is inverted
        //auto new_y = getViewerExaminer()->getRenderArea()->getSize()[1] - pos[1];
        auto new_y = getQtRenderArea()->size().height() - pos[1];
        QPoint globalPos = this->mapToGlobal(QPoint(pos[0], new_y));

        QMenu myMenu;

        QMenu* cmMenu = myMenu.addMenu(tr("ColorMap"));
        QAction* cmOptions[5];
        cmOptions[0] = cmMenu->addAction("Grayscale");
        cmOptions[1] = cmMenu->addAction("Inverse grayscale");
        cmOptions[2] = cmMenu->addAction("Hot iron");
        cmOptions[3] = cmMenu->addAction("JET");
        cmOptions[4] = cmMenu->addAction("Rainbow");

        for (int i = 0; i < 5; i++) {
            cmOptions[i]->setCheckable(true);
        }
        cmOptions[d->colorMap]->setChecked(true);

        //Dynamic addition of color scalarbar
        QMenu* sbMenu = myMenu.addMenu(tr("ScalarBar"));
        auto toggleSB = sbMenu->addAction("Show ScalarBar");
        toggleSB->setCheckable(true);
        toggleSB->setChecked(d->is_scalarBar);

        /*if (d->is_scalarBar) {
            auto moveSB = sbMenu->addAction("Move ScalarBar");
            moveSB->setCheckable(true);
            moveSB->setChecked(d->is_scalarTrans);
            auto scaleSB = sbMenu->addAction("Scale ScalarBar");
            scaleSB->setCheckable(true);
            scaleSB->setChecked(d->is_scalarScale);
        }*/

        if (d->is_reload) {
            d->is_reload = false;
        }

        myMenu.addAction("Reset View");

        //if (d->ID == 0) {
        if (d->isRIpick) {
            myMenu.addAction("RI Picker");
        }

        QAction* selectedItem = myMenu.exec(globalPos);

        if (selectedItem) {
            auto text = selectedItem->text();
            if (text.contains("Inverse")) {
                change2DColorMap(1);
            }
            else if (text.contains("Grayscale")) {
                change2DColorMap(0);
            }
            else if (text.contains("Hot")) {
                change2DColorMap(2);
            }
            else if (text.contains("JET")) {
                change2DColorMap(3);
            }
            else if (text.contains("Rainbow")) {
                change2DColorMap(4);
            }
            else if (text.contains("Reset View")) {
                reset2DView(true);
            }
            else if (text.contains("RI Picker")) {
                initSpuit();
            }
            else if (text.contains("Show ScalarBar")) {
                toggleScalarBar();
            } /*else if (text.contains("Move ScalarBar")) {
                ScalarBarMove(true);
            } else if (text.contains("Scale ScalarBar")) {
                ScalarBarScale(true);
            }*/
        }
    }

    auto ViewerRenderWindow2d::toggleScalarBar() -> void {
        d->is_scalarBar = !d->is_scalarBar;
        emit toggleScalarBar(d->AxisID, d->is_scalarBar);        
    }
    auto ViewerRenderWindow2d::ScalarBarScale(bool activate) -> void {
        return;
        auto sbname = "ScalarBar" + std::to_string(d->ID);
        auto scalarSep = MedicalHelper::find<SoSeparator>(getSceneGraph(), sbname);
        if (scalarSep) {
            if (d->is_scalarBar) {
                if (d->is_scalarScale || !activate) {
                    auto maniname = "Manip2D_" + std::to_string(d->ID);
                    auto manisw = MedicalHelper::find<SoSeparator>(getSceneGraph(), maniname);
                    if (manisw) {
                        manisw->replaceChild(0, new SoSeparator);
                        d->is_scalarScale = false;
                    }
                }
                else {
                    //auto new_scale = new SoScale2Dragger;
                    auto new_scale = new SoScale2UniformDragger;
                    new_scale->setName("tempScaler");
                    new_scale->setMinScale(0.5);
                    auto trans = static_cast<SoTranslation*>(scalarSep->getChild(0));
                    auto scale = static_cast<SoScale*>(scalarSep->getChild(1));

                    auto prev_scale = scale->scaleFactor.getValue();
                    if (d->ID == 0) {
                        new_scale->scaleFactor.setValue(prev_scale[0], prev_scale[1], 1);
                    }
                    else if (d->ID == 1) {
                        new_scale->scaleFactor.setValue(prev_scale[2], prev_scale[1], 1);
                    }
                    else if (d->ID == 2) {
                        new_scale->scaleFactor.setValue(prev_scale[0], prev_scale[2], 1);
                    }

                    auto vp = getViewportRegion();
                    SoGetBoundingBoxAction* action = new SoGetBoundingBoxAction(vp);
                    action->apply(scalarSep->getChild(2));

                    auto bbox = action->getBoundingBox();

                    auto fs = new SoFaceSet;
                    auto vertices = new SoVertexProperty;
                    fs->vertexProperty = vertices;
                    switch (d->ID) {
                    case 0:
                        vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMax()[1] + 0.05, 0));
                        vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMin()[1] - 0.05, 0));
                        vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMin()[1] - 0.05, 0));
                        vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMax()[1] + 0.05, 0));
                        break;
                    case 1:
                        vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[2] - 0.05, bbox.getMax()[1] + 0.05, -1));
                        vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[2] - 0.05, bbox.getMin()[1] - 0.05, -1));
                        vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[2] + 0.05, bbox.getMin()[1] - 0.05, -1));
                        vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[2] + 0.05, bbox.getMax()[1] + 0.05, -1));
                        break;
                    case 2:
                        vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMax()[2] + 0.05, -1));
                        vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMin()[2] - 0.05, -1));
                        vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMin()[2] - 0.05, -1));
                        vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMax()[2] + 0.05, -1));
                        break;
                    }

                    auto handleSep = new SoSeparator;

                    auto handleMatl = new SoMaterial;
                    handleMatl->diffuseColor.setValue(0, 1, 0);
                    handleMatl->transparency.setValue(0.5);

                    handleSep->addChild(handleMatl);
                    handleSep->addChild(fs);

                    new_scale->setPart("scalerActive", handleSep);
                    new_scale->setPart("scaler", handleSep);
                    new_scale->setPart("feedback", new SoSeparator);
                    new_scale->setPart("feedbackActive", new SoSeparator);

                    auto maniname = "Manip2D_" + std::to_string(d->ID);
                    auto manisw = MedicalHelper::find<SoSeparator>(getSceneGraph(), maniname);

                    auto transScale = new SoTranslation;
                    auto prev_trans = trans->translation.getValue();
                    transScale->translation.setValue(trans->translation.getValue());

                    auto hanSep = new SoSeparator;
                    hanSep->addChild(transScale);
                    hanSep->addChild(new_scale);


                    if (manisw) {
                        auto newSep = new SoSeparator;
                        auto itrans = new SoTranslation;
                        auto ttrans = new SoTranslation;

                        itrans->translation.setValue(-prev_trans[0], -prev_trans[1], -prev_trans[2]);
                        ttrans->translation.setValue(prev_trans[0], prev_trans[1], prev_trans[2]);
                        auto rot = new SoRotation;

                        auto decomposer = new SoDecomposeVec3f;
                        decomposer->vector.connectFrom(&new_scale->scaleFactor);
                        auto composer = new SoComposeVec3f;

                        newSep->addChild(ttrans);
                        newSep->addChild(rot);
                        newSep->addChild(itrans);
                        newSep->addChild(hanSep);
                        manisw->replaceChild(0, newSep);

                        if (d->ID == 0) {
                            composer->x.connectFrom(&decomposer->x);
                            composer->y.connectFrom(&decomposer->y);
                            composer->z.connectFrom(&decomposer->z);//fixed value
                        }
                        else if (d->ID == 1) {
                            rot->rotation.setValue(SbVec3f(0, 1, 0), 1.57f);
                            composer->x.connectFrom(&decomposer->z);//fixed value
                            composer->y.connectFrom(&decomposer->y);
                            composer->z.connectFrom(&decomposer->x);
                        }
                        else {
                            rot->rotation.setValue(SbVec3f(1, 0, 0), 1.57f);
                            composer->x.connectFrom(&decomposer->x);
                            composer->y.connectFrom(&decomposer->z);//fixed value
                            composer->z.connectFrom(&decomposer->y);
                        }

                        scale->scaleFactor.connectFrom(&composer->vector);
                        //attach scalar manipulator
                        d->is_scalarScale = true;
                        //setInteractionMode(false);                        
                    }
                }
            }
        }
    }

    auto ViewerRenderWindow2d::ScalarBarMove(bool activate) -> void {
        auto swname = "ScalarBar" + std::to_string(d->AxisID);
        auto scalarSep = MedicalHelper::find<SoSeparator>(getSceneGraph(), swname);

        if (scalarSep) {
            if (d->is_scalarBar) {
                if (d->is_scalarTrans || !activate) {
                    //detach existing manipulator                    
                    auto maniname = "Manip2D_" + std::to_string(d->AxisID);
                    auto manisw = MedicalHelper::find<SoSeparator>(getSceneGraph(), maniname);

                    if (manisw) {
                        manisw->replaceChild(0, new SoSeparator);
                        d->is_scalarTrans = false;
                    }
                }
                else {
                    auto new_trans = new SoTranslate2Dragger;
                    new_trans->setName("tempDragger");

                    auto trans = static_cast<SoTranslation*>(scalarSep->getChild(0));
                    auto scale = static_cast<SoScale*>(scalarSep->getChild(1));

                    auto prev_trans = trans->translation.getValue();

                    new_trans->translation.setValue(prev_trans);

                    auto vp = getViewportRegion();
                    SoGetBoundingBoxAction* action = new SoGetBoundingBoxAction(vp);
                    action->apply(scalarSep->getChild(2));
                    auto bbox = action->getBoundingBox();

                    auto fs = new SoFaceSet;
                    SoVertexProperty* vertices = new SoVertexProperty;
                    fs->vertexProperty = vertices;
                    switch (d->ID) {
                    case 0:
                        vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMax()[1] + 0.05, 0));
                        vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMin()[1] - 0.05, 0));
                        vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMin()[1] - 0.05, 0));
                        vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMax()[1] + 0.05, 0));
                        break;
                    case 1:
                        vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[2] - 0.05, bbox.getMax()[1] + 0.05, -1));
                        vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[2] - 0.05, bbox.getMin()[1] - 0.05, -1));
                        vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[2] + 0.05, bbox.getMin()[1] - 0.05, -1));
                        vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[2] + 0.05, bbox.getMax()[1] + 0.05, -1));
                        break;
                    case 2:
                        vertices->vertex.set1Value(0, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMax()[2] + 0.05, -1));
                        vertices->vertex.set1Value(1, SbVec3f(bbox.getMin()[0] - 0.05, bbox.getMin()[2] - 0.05, -1));
                        vertices->vertex.set1Value(2, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMin()[2] - 0.05, -1));
                        vertices->vertex.set1Value(3, SbVec3f(bbox.getMax()[0] + 0.05, bbox.getMax()[2] + 0.05, -1));
                        break;
                    }

                    auto handleSep = new SoSeparator;

                    auto handleMatl = new SoMaterial;

                    auto transScale = new SoScale;

                    auto val = scale->scaleFactor.getValue();

                    switch (d->ID) {
                    case 0:
                        transScale->scaleFactor.setValue(scale->scaleFactor.getValue());
                        break;
                    case 1:
                        transScale->scaleFactor.setValue(val[2], val[1], 1.0);
                        break;
                    case 2:
                        transScale->scaleFactor.setValue(val[0], val[2], 1.0);
                        break;
                    }
                    handleMatl->diffuseColor.setValue(1.0, 0.0, 0.0);
                    handleMatl->transparency.setValue(0.5);

                    handleSep->addChild(handleMatl);
                    handleSep->addChild(transScale);
                    handleSep->addChild(fs);


                    new_trans->setPart("translatorActive", handleSep);
                    new_trans->setPart("translator", handleSep);
                    new_trans->setPart("xAxisFeedback", new SoSeparator);
                    new_trans->setPart("yAxisFeedback", new SoSeparator);

                    auto maniname = "Manip2D_" + std::to_string(d->AxisID);
                    auto manisw = MedicalHelper::find<SoSeparator>(getSceneGraph(), maniname);

                    if (manisw) {
                        auto newSep = new SoSeparator;
                        auto itrans = new SoTranslation;
                        auto ttrans = new SoTranslation;

                        itrans->translation.setValue(-prev_trans[0], -prev_trans[1], -prev_trans[2]);
                        ttrans->translation.setValue(prev_trans[0], prev_trans[1], prev_trans[2]);
                        auto rot = new SoRotation;

                        newSep->addChild(ttrans);
                        newSep->addChild(rot);
                        newSep->addChild(itrans);
                        newSep->addChild(new_trans);
                        manisw->replaceChild(0, newSep);

                        auto decomposer = new SoDecomposeVec3f;
                        decomposer->vector.connectFrom(&new_trans->translation);
                        auto composer = new SoComposeVec3f;
                        if (d->ID == 0) {
                            composer->x.connectFrom(&decomposer->x);
                            composer->y.connectFrom(&decomposer->y);
                            composer->z.connectFrom(&decomposer->z);
                        }
                        else if (d->ID == 1) {
                            rot->rotation.setValue(SbVec3f(0, 1, 0), 1.57f);
                            auto calc = new SoCalculator;
                            calc->expression = "oc = -a + b";
                            calc->a.connectFrom(&decomposer->x);
                            calc->b.setValue(prev_trans[0] + prev_trans[2]);

                            composer->x.setValue(prev_trans[0]);
                            composer->y.connectFrom(&decomposer->y);
                            composer->z.connectFrom(&calc->oc);
                        }
                        else if (d->ID == 2) {
                            rot->rotation.setValue(SbVec3f(1, 0, 0), 1.57f);
                            auto calc = new SoCalculator;
                            calc->expression = "oc = a + b";
                            calc->a.connectFrom(&decomposer->y);
                            calc->b.setValue(prev_trans[1] + prev_trans[2]);

                            composer->x.connectFrom(&decomposer->x);
                            composer->y.setValue(prev_trans[1]);
                            composer->z.connectFrom(&calc->oc);
                        }

                        trans->translation.connectFrom(&composer->vector);
                        //attach scalar manipulator
                        d->is_scalarTrans = true;
                        //setInteractionMode(false);
                    }

                }
            }
        }
    }

    auto ViewerRenderWindow2d::setRenderWindowID(int idx) -> void {
        d->ID = idx;
    }

    auto ViewerRenderWindow2d::setAxisID(int id) -> void {
        d->AxisID = id;
    }


    auto ViewerRenderWindow2d::initSpuit() -> void {
        d->is_spuit = true;
        auto root2D = MedicalHelper::find<SoSeparator>(getSceneGraph(), "HT2D_XY_root");
        if (root2D) {
            auto existing_spuit = MedicalHelper::find<SoSwitch>(root2D, "SpuitSwitch");
            if (existing_spuit) {
                root2D->removeChild(existing_spuit);
            }
            /*if (!existing_spuit) {
                d->spuit = new Oiv2DSpuit;
                root2D->addChild(d->spuit->getSpuitRoot());
            }*/
            d->spuit = new Oiv2DSpuit;
            root2D->addChild(d->spuit->getSpuitRoot());
            d->spuit->activateSpuit(true);
            setInteractionMode(false);

            connect(d->spuit, SIGNAL(sigFinishSpuit()), this, SLOT(OnFinishSpuit()));
        }
        //d->m_guiAlgo->setViewing(false);
    }
    auto ViewerRenderWindow2d::resetViewBF(bool isBF) -> bool {
        auto changed = false;
        if (d->isBF != isBF) {
            changed = true;
        }
        d->isBF = isBF;
        return changed;
    }

    auto ViewerRenderWindow2d::setIsLdm(bool isLdm) -> void {
        d->isLDM = isLdm;
    }

    auto ViewerRenderWindow2d::reset2DView(bool fromFunc) -> void {
        //find which axis
        MedicalHelper::Axis ax;
        auto root = getSceneGraph();

        //reset scalar bar position
        auto scalarSep = MedicalHelper::find<OivRangeBar>(getSceneGraph());
        if (scalarSep) {
            scalarSep->position.setValue(-0.95f, -0.95f);
        }
        //reset time stamp position
        auto timeSep = MedicalHelper::find<TextBox>(getSceneGraph(), "TimeStamp");
        if (timeSep) {
            timeSep->position.setValue(0.15f, -0.985f, 0);
        }
        //reset scale position
        auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
        if (scaleSep) {
            scaleSep->position.setValue(0.95f, -0.95f);
        }

        SoVolumeData* volData{ nullptr };// = MedicalHelper::find<SoVolumeData>(root);//find any volume Data

        if (d->isBF) {
            volData = MedicalHelper::find<SoVolumeData>(root);//find any volume Data
        }
        else {
            //find volume data except BF
            auto nodes = MedicalHelper::findNodes<SoVolumeData>(root);
            for (auto n : nodes) {
                QString nname(n->getName().getString());
                if (nname.contains("BF_CH")) {
                    continue;
                }
                volData = n;
                break;
            }
        }

        if (volData) {
            float slack;
            auto dims = volData->getDimension();
            auto spacing = volData->getVoxelSize();

            float x_len = dims[0] * spacing[0];
            float y_len = dims[1] * spacing[1];
            float z_len = dims[2] * spacing[2];
            auto wh = static_cast<float>(size().height());
            auto ww = static_cast<float>(size().width());
            auto windowSlack = wh > ww ? ww / wh : wh / ww;
            if (wh < 50) {
                d->isFirstResize = true;
            }
            if (d->ID == 0) {
                ax = MedicalHelper::Axis::AXIAL;
                slack = y_len < x_len ? y_len / x_len : x_len / y_len;
            }
            else if (d->ID == 1) {
                ax = MedicalHelper::SAGITTAL;
                slack = z_len / y_len;
            }
            else if (d->ID == 2) {
                ax = MedicalHelper::CORONAL;
                slack = z_len / x_len;
            }
            else {
                return;
            }

            if (windowSlack > slack && windowSlack > 0) {
                slack = windowSlack > 1 ? 1.0 : windowSlack;
            }
            MedicalHelper::orientView(ax, getCamera(), volData, slack);
            if (false == d->isLDM) {
                if (getName() == "TwoD1") {
                    getCamera()->orientation.setValue(SbVec3f(0, 1, 0), M_PI);
                }
                if (getName() == "TwoD2") {
                    getCamera()->orientation.setValue(SbVec3f(1, 0, 0), M_PI * 2);
                }
            }
            if (fromFunc) {
                //reset colormap
                change2DColorMap(0);
            }
        }
    }

    void ViewerRenderWindow2d::resizeEvent(QResizeEvent* event) {
        if (d->isFirstResize) {
            reset2DView();
            d->isFirstResize = false;
        }
        QWidget::resizeEvent(event);
    }

    auto ViewerRenderWindow2d::change2DColorMap(int idx) -> void {
        auto rootScene = getSceneGraph();
        if (rootScene) {
            auto tf = MedicalHelper::find<SoTransferFunction>(rootScene, "colormap2D_TF");
            if (tf) {
                auto isInverse = false;
                int pred = 0;
                switch (idx) {
                case 0: // grayscale
                    pred = SoTransferFunction::PredefColorMap::INTENSITY;
                    break;
                case 1: // inversegrayscale
                    pred = SoTransferFunction::PredefColorMap::INTENSITY_REVERSED;
                    isInverse = true;
                    break;
                case 2: // Hot Iron - GLOW in OIV
                    pred = SoTransferFunction::PredefColorMap::GLOW;
                    break;
                case 3: // JET - PHYSICS in OIV
                    pred = SoTransferFunction::PredefColorMap::PHYSICS;
                    break;
                case 4: // Rainbow - STANDARD in OIV
                    pred = SoTransferFunction::PredefColorMap::STANDARD;
                    break;
                }
                d->colorMap = idx;
                tf->predefColorMap = pred;

                //change scalar bar text color at inverse grayscale
                emit sigColorMap(d->colorMap);
            }
        }
    }
    auto ViewerRenderWindow2d::setColorMapIdx(int idx) -> void {
        d->colorMap = idx;
    }

    auto ViewerRenderWindow2d::refreshRangeSlider() -> void {
        d->is_reload = true;
    }

    auto ViewerRenderWindow2d::setHTRange(double min, double max) -> void {
        d->level_min = min;
        d->level_max = max;
    }

    auto ViewerRenderWindow2d::MouseMoveEvent(SoEventCallback* node) -> void {
        emit sigMouseMove();
        const SoLocation2Event* moveEvent = (const SoLocation2Event*)node->getEvent();
        if (d->scalar_pressed) {
            auto pos = moveEvent->getNormalizedPosition(getViewportRegion());
            auto move_x = pos[0] * 2.0f - 1.0f;
            auto move_y = pos[1] * 2.0f - 1.0f;
            auto scalarSep = MedicalHelper::find<OivRangeBar>(getSceneGraph());
            if (scalarSep) {
                scalarSep->position.setValue(d->scalar_pressed_pos[0] + move_x, d->scalar_pressed_pos[1] + move_y);
            }
            return;
        }
        if (d->time_pressed) {
            auto pos = moveEvent->getNormalizedPosition(getViewportRegion());
            auto move_x = pos[0] * 2.0f - 1.0f;
            auto move_y = pos[1] * 2.0f - 1.0f;
            auto timeSep = MedicalHelper::find<TextBox>(getSceneGraph(), "TimeStamp");
            if (timeSep) {
                timeSep->position.setValue(move_x, d->time_pressed_pos[1] + move_y, 0);
            }
            return;
        }
        if (d->scale_pressed) {
            auto pos = moveEvent->getNormalizedPosition(getViewportRegion());
            auto move_x = pos[0] * 2.0f - 1.0f;
            auto move_y = pos[1] * 2.0f - 1.0f;
            auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
            if (scaleSep) {
                scaleSep->position.setValue(d->scale_pressed_pos[0] + move_x, -d->scale_pressed_pos[1] + move_y);
            }
            return;
        }
        if (isNavigation()) {
            auto pos = moveEvent->getPositionFloat();
            auto viewport_size = getViewportRegion().getViewportSizePixels();
            auto root = getSceneGraph();
            auto color = MedicalHelper::find<SoImage>(root, "icon_file");
            if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                auto image = new QImage(":/img/ic-setting-s.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            else {
                auto image = new QImage(":/img/ic-setting.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            if (d->right_mouse_pressed) {
                auto diff = moveEvent->getNormalizedPosition(getViewportRegion())[1] - d->right_stamp;
                auto factor = 1.0 - diff * 1.5;
                if (factor > 0) {
                    getCamera()->scaleHeight(factor / d->prev_scale);
                    d->prev_scale = factor;
                }
            }
            if (d->left_mouse_pressed) {
                if (!d->is2D) {
                    auto coord = CalcVolumeCoord(moveEvent->getNormalizedPosition(getViewportRegion()));
                    emit sig2dCoord(coord[0], coord[1], coord[2]);
                }
                node->setHandled();
            }
            if (d->middle_mouse_pressed) {
                if (!d->is2D) {
                    //panCamera(moveEvent->getPositionFloat());
                    panCamera(moveEvent->getNormalizedPosition(getViewportRegion()));
                }
            }
        }
    }

    /*void ViewerRenderWindow2d::wheelEvent(QWheelEvent* event) {
        if (!d->is2D) {
            auto delta = (event->angleDelta().y() > 0) ? 1 : -1;
            emit sigWheel(delta);
        }
    }*/

    auto ViewerRenderWindow2d::MouseWheelEvent(SoEventCallback* node) -> void {
        const SoMouseWheelEvent* mouseWheel = (const SoMouseWheelEvent*)node->getEvent();
        auto delta = mouseWheel->getDelta() > 0 ? 1 : -1;
        emit sigWheel(delta);
    }

    void ViewerRenderWindow2d::OnFinishSpuit() {
        if (d->is_spuit) {
            //d->spuit->setFinalizeAction(node->getAction());
            d->is_spuit = false;
            auto result = d->spuit->getCurrentTFBoxInfo();
            if (nullptr != result) {
                emit sigTFBox(result[0], result[1], result[2], result[3]);
            }
            setInteractionMode(true);
            d->spuit->activateSpuit(false);
        }
    }

    auto ViewerRenderWindow2d::MouseButtonEvent(SoEventCallback* node) -> void {
        const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)node->getEvent();
        if (!isNavigation()) {//selection mode
            //if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {

            //}
        }
        else {//navigation mode
            auto pos = mouseButton->getPositionFloat();
            auto vr = getViewportRegion();
            auto normpos = mouseButton->getNormalizedPosition(vr);
            auto viewport_size = vr.getViewportSizePixels();
            auto mouse_in_setting = (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25);
            if (SoMouseButtonEvent::isButtonDoubleClickEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                reset2DView();
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                if (!mouse_in_setting) {
                    d->right_mouse_pressed = true;
                    d->right_stamp = normpos[1];
                    d->prev_scale = 1.0;
                }
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                d->right_mouse_pressed = false;
                d->right_stamp = 0.0;
                d->prev_scale = 1.0;
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                auto mouse_in_scalar = false;
                auto scalarSep = MedicalHelper::find<OivRangeBar>(getSceneGraph());
                auto vp = getViewportRegion();
                if (scalarSep) {
                    auto geom = scalarSep->getBorderGeometry();
                    auto vertices = dynamic_cast<SoVertexProperty*>(geom->vertexProperty.getValue());
                    auto vvs = vertices->vertex.getValues(0);
                    auto x_min = (vvs[0][0] + 1.0f) / 2.0f;
                    auto x_max = (vvs[2][0] + 1.0f) / 2.0f;
                    auto y_min = (vvs[0][1] + 1.0f) / 2.0f;
                    auto y_max = (vvs[2][1] + 1.0f) / 2.0f;

                    if (normpos[0] > x_min && normpos[0] < x_max && normpos[1] > y_min && normpos[1] < y_max) {
                        mouse_in_scalar = true;
                    }
                }
                auto mouse_in_time = false;
                auto timeSep = MedicalHelper::find<TextBox>(getSceneGraph(), "TimeStamp");
                if (timeSep) {
                    SoGetBoundingBoxAction* action = new SoGetBoundingBoxAction(vp);
                    action->apply(timeSep->getTextNode());
                    auto bbox = action->getBoundingBox();

                    auto size = bbox.getSize();
                    auto tpos = timeSep->position.getValue();
                    auto x_min = (tpos[0] + 1.0f) / 2.0f - size[0] / 2;
                    auto x_max = (tpos[0] + 1.0f) / 2.0f + size[0] / 2;
                    auto y_min = (tpos[1] + 1.0f) / 2.0f;
                    auto y_max = (tpos[1] + 1.0f) / 2.0f + size[1];
                    if (normpos[0] > x_min && normpos[0] < x_max && normpos[1] > y_min && normpos[1] < y_max) {
                        mouse_in_time = true;
                    }
                }
                auto mouse_in_scale = false;
                auto scaleSep = MedicalHelper::find<OivScaleBar>(getSceneGraph());
                if (scaleSep) {
                    auto lineSep = scaleSep->GetLineSep();
                    SoGetBoundingBoxAction* laction = new SoGetBoundingBoxAction(vp);
                    laction->apply(lineSep);
                    auto lbbox = laction->getBoundingBox();
                    auto textSep = scaleSep->GetTextSep();
                    SoGetBoundingBoxAction* taction = new SoGetBoundingBoxAction(vp);
                    taction->apply(textSep);
                    auto tbbox = taction->getBoundingBox();

                    auto x_min = (lbbox.getMin()[0] + 1.0f) / 2.0f;
                    auto x_max = (lbbox.getMax()[0] + 1.0f) / 2.0f;
                    auto y_min = (lbbox.getMin()[1] + 1.0f) / 2.0f;
                    auto y_max = (tbbox.getMax()[1] + 1.0f) / 2.0f;
                    if (normpos[0] > x_min && normpos[0] < x_max && normpos[1] > y_min && normpos[1] < y_max) {
                        mouse_in_scale = true;
                    }
                }
                if (mouse_in_scalar) {
                    auto geom = scalarSep->getBorderGeometry();
                    auto vertices = dynamic_cast<SoVertexProperty*>(geom->vertexProperty.getValue());
                    auto vvs = vertices->vertex.getValues(0);
                    d->scalar_pressed_pos[0] = -(normpos[0] * 2.0f - 1.0f - vvs[0][0]);
                    d->scalar_pressed_pos[1] = -(normpos[1] * 2.0f - 1.0f - vvs[0][1]);
                    d->scalar_pressed = true;
                }
                else if (mouse_in_time) {
                    SoGetBoundingBoxAction* action = new SoGetBoundingBoxAction(vp);
                    action->apply(timeSep->getTextNode());
                    auto bbox = action->getBoundingBox();

                    auto size = bbox.getSize();

                    auto tpos = timeSep->position.getValue();
                    d->time_pressed = true;
                    d->time_pressed_pos[1] = -size[1] / 2;
                }
                else if (mouse_in_scale) {
                    d->scale_pressed = true;
                    auto lineSep = scaleSep->GetLineSep();
                    SoGetBoundingBoxAction* laction = new SoGetBoundingBoxAction(vp);
                    laction->apply(lineSep);
                    auto lbbox = laction->getBoundingBox();
                    auto textSep = scaleSep->GetTextSep();
                    SoGetBoundingBoxAction* taction = new SoGetBoundingBoxAction(vp);
                    taction->apply(textSep);
                    auto tbbox = taction->getBoundingBox();

                    d->scale_pressed_pos[0] = -(normpos[0] * 2.0f - 1.0f - lbbox.getMax()[0]);
                    d->scale_pressed_pos[1] = -(normpos[1] * 2.0f - 1.0f - tbbox.getMax()[1]);
                }
                else {
                    d->middle_mouse_pressed = true;
                    startPan(mouseButton->getNormalizedPosition(getViewportRegion()));
                }
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                d->time_pressed = false;
                d->scalar_pressed = false;
                d->scale_pressed = false;
                d->middle_mouse_pressed = false;
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                if (!mouse_in_setting) {
                    d->left_mouse_pressed = true;
                    if (!d->is2D) {
                        auto coord = CalcVolumeCoord(normpos);
                        emit sig2dCoord(abs(coord[0]), abs(coord[1]), coord[2]);
                    }
                }
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                if (mouse_in_setting) {
                    showContextMenu(pos);
                }
                d->left_mouse_pressed = false;
            }
        }
    }

    auto ViewerRenderWindow2d::CalcVolumeCoord(SbVec2f norm_point) -> SbVec3f {
        SoRayPickAction rayPick = SoRayPickAction(getViewportRegion());
        SbVec2f normPoint = norm_point;

        auto root = getSceneGraph();

        rayPick.setNormalizedPoint(normPoint);
        rayPick.apply(root);

        SoPickedPoint* pickedPoint = rayPick.getPickedPoint(0);
        if (pickedPoint) {
            SoVolumeData* volData = MedicalHelper::find<SoVolumeData>(root);//find any volume Data
            if (volData) {
                auto pt = pickedPoint->getPoint();
                //auto dim = volData->getDimension();
                auto coord = volData->XYZToVoxel(pt);
                /*if (d->isXY) {
                    coord[0] += (static_cast<float>(dim[0]) / 2.f);
                    coord[1] += (static_cast<float>(dim[1]) / 2.f);
                }*/
                return coord;
            }
        }
        return SbVec3f();
    }

    auto ViewerRenderWindow2d::setXY(bool isXY) -> void {
        d->isXY = isXY;
    }

    auto ViewerRenderWindow2d::KeyboardEvent(SoEventCallback* node) -> void {
        const SoEvent* keyEvent = node->getEvent();

        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::S)) {
            //seek mode switching is handled by native viewer event callback                    
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
            //interaction mode
            if (!isNavigation()) {
                setInteractionMode(true);
                setAltPressed(true);
            }
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyReleaseEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
            if (isAltPressed()) {
                setInteractionMode(false);
                setAltPressed(false);
            }
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::ENTER)) {
            if (d->is_scalarTrans) {
                ScalarBarMove(false);
            }
            if (d->is_scalarScale) {
                ScalarBarScale(false);
            }
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::ESCAPE)) {
            /*if (isNavigation()) {
                setInteractionMode(false);
            }else {
                setInteractionMode(true);
            }*/
        }            
    }

    auto ViewerRenderWindow2d::closeEvent(QCloseEvent* unused) -> void {
        Q_UNUSED(unused)
    }

    auto ViewerRenderWindow2d::setDefaultWindowType() -> void {
        //set default gradient background color
        SbVec3f start_color = { 0.0,0.0,0.0 };
        SbVec3f end_color = { 0.0,0.0,0.0 };
        setGradientBackground(start_color, end_color);

        //set default camera type
        setCameraType(true);//orthographic

        //set default navigation type
        //setNavigationMode(false);//plane
    }
}
