#pragma once

#include <IVisualizationDataWritePort.h>

#include "TomoAnalysisVisualizationDataIOExport.h"

namespace TomoAnalysis::Viewer::Plugins::VisualizationDataIO {
	class TomoAnalysisVisualizationDataIO_API VisualizationDataWriter : public UseCase::IVisualizationDataWritePort {
	public:
		VisualizationDataWriter();
		~VisualizationDataWriter();

		auto Write(const std::string& path, Entity::TFItemList tfItemList, Entity::FLChannelInfo channel)->bool override;

	private:
		auto GenerateBackup(const std::string& path)->bool;
	};
}