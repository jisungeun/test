#pragma once

#include <string>
#include <IVisualizationDataReadPort.h>

#include "TomoAnalysisVisualizationDataIOExport.h"

namespace TomoAnalysis::Viewer::Plugins::VisualizationDataIO {
	class TomoAnalysisVisualizationDataIO_API VisualizationDataReader : public UseCase::IVisualizationDataReadPort {
	public:
		VisualizationDataReader();
		~VisualizationDataReader();

		auto Read(const std::string& path, Entity::TFItemList& tfItemList, Entity::FLChannelInfo& channel)->bool override;
	};
}