#include <QDirIterator>
#include <QFile>

#include "tinyxml.h"

#include "VisualizationDataWriter.h"

namespace TomoAnalysis::Viewer::Plugins::VisualizationDataIO {
	VisualizationDataWriter::VisualizationDataWriter() {
	    
	}

	VisualizationDataWriter::~VisualizationDataWriter() = default;

	auto VisualizationDataWriter::Write(const std::string& path, Entity::TFItemList tfItemList, Entity::FLChannelInfo channel)->bool {
		if (true == path.empty()) {
			return false;
		}

		this->GenerateBackup(path);

		TiXmlDocument document;
		auto decl = new TiXmlDeclaration("1.0", "UTF-8", "");
		auto version = new TiXmlElement("Version");
		version->SetDoubleAttribute("TransferfunctionVersion", 2.1);
		auto tfElement = new TiXmlElement("TransferFunction");

		document.LinkEndChild(decl);
		document.LinkEndChild(version);
		document.LinkEndChild(tfElement);

		// TF
		if (false == tfItemList.empty()) {
			auto nodeElement = new TiXmlElement("RI_Tomogram");

			for (auto item : tfItemList) {
				auto data = new TiXmlElement("data");

				data->SetDoubleAttribute("x1", item->intensityMin / 10.0);
				data->SetDoubleAttribute("x2", item->intensityMax / 10.0);
				data->SetDoubleAttribute("y1", item->gradientMin);
				data->SetDoubleAttribute("y2", item->gradientMax);
				data->SetDoubleAttribute("cr", (double)item->color.red / 255.0);
				data->SetDoubleAttribute("cg", (double)item->color.green / 255.0);
				data->SetDoubleAttribute("cb", (double)item->color.blue / 255.0);
				data->SetDoubleAttribute("opacity", item->transparency);
				data->SetDoubleAttribute("softness", 0);
				data->SetDoubleAttribute("gamma", 0);
				data->SetAttribute("name", "defalut");
				data->SetAttribute("v", "1");

				nodeElement->LinkEndChild(data);
			}

			tfElement->LinkEndChild(nodeElement);
		}

		// FL
		if (false == channel.empty()) {
			auto nodeElement = new TiXmlElement("FL_Tomogram");

			for (int i = 0; i < 3; ++i) {
				auto data = new TiXmlElement("data");

				auto item = channel[Entity::Channel::_from_integral(i)];

				data->SetDoubleAttribute("x1", item->min);
				data->SetDoubleAttribute("x2", item->max);
				data->SetDoubleAttribute("y1", 1.0);
				data->SetDoubleAttribute("y2", 255.0);
				data->SetDoubleAttribute("cr", 255.0);
				data->SetDoubleAttribute("cg", 255.0);
				data->SetDoubleAttribute("cb", 255.0);
				data->SetDoubleAttribute("opacity", (double)item->opacity / 100.0);
				data->SetDoubleAttribute("softness", i);
				data->SetDoubleAttribute("gamma", 1);
				data->SetAttribute("name", "");
				if (item->isValid) {
					data->SetAttribute("v", "1");
				}else {
					data->SetAttribute("v", "0");
				}

				nodeElement->LinkEndChild(data);
			}

			tfElement->LinkEndChild(nodeElement);
		}

		if (!document.SaveFile(path.c_str()))
		{
			return false;
		}

		return true;
	}

	auto VisualizationDataWriter::GenerateBackup(const std::string& path)->bool {
		if(true == path.empty()) {
			return false;
		}

	    QFile file(path.c_str());
		if (false == file.exists()) {
			return false;
		}

		QFileInfo fileinfo(file);

		QDir dir = fileinfo.dir();
		QStringList filter(QString("%1.*.bak").arg(fileinfo.fileName()));
		QStringList backups = dir.entryList(filter, QDir::Files);

		const int new_index = backups.size() + 1;

		QString newFilePath = QString("%1/%2.%3.bak").arg(dir.path()).arg(fileinfo.fileName()).arg(new_index, 3, 10, QLatin1Char('0'));
		file.copy(newFilePath);

		return true;
	}
}

