#include <QMenu>
#include <QWheelEvent>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/nodes/SoImage.h>
#include <Medical/helpers/MedicalHelper.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/events/SoKeyboardEvent.h>
#pragma warning(pop)

#include "ViewerRenderWindow3d.h"

namespace TomoAnalysis::Viewer::Plugins {
    struct ViewerRenderWindow3d::Impl {
        float orbit_x = 0.f;
        float orbit_y = 0.f;

        bool is_Black{ false };
        bool is_Gradient{ true };
        bool is_White{ false };

        bool mouseLeftPressed{ false };
        bool mouseMiddlePressed{ false };
        bool mouseRighPressed{ false };
    };

    ViewerRenderWindow3d::ViewerRenderWindow3d(QWidget* parent) :
        QOivRenderWindow(parent), d{ new Impl } {
        setDefaultWindowType();
    }

    ViewerRenderWindow3d::~ViewerRenderWindow3d() {

    }

    auto ViewerRenderWindow3d::resetOnLoad() -> void {

    }

    auto ViewerRenderWindow3d::showContextMenu(SbVec2f pos) -> void {
        //y position is inverted
        auto new_y = getQtRenderArea()->size().height() - pos[1];
        QPoint globalPos = this->mapToGlobal(QPoint(pos[0], new_y));

        QMenu myMenu;

        QMenu* bgMenu = myMenu.addMenu(tr("Background Options"));
        auto gradient = bgMenu->addAction("Gradient");
        auto black = bgMenu->addAction("Black");
        auto white = bgMenu->addAction("White");

        gradient->setCheckable(true);
        black->setCheckable(true);
        white->setCheckable(true);

        if (d->is_Gradient)
            gradient->setChecked(true);
        else if (d->is_Black)
            black->setChecked(true);
        else if (d->is_White)
            white->setChecked(true);

        QMenu* viewMenu = myMenu.addMenu(tr("View"));
        auto view_x = viewMenu->addAction("View X");
        auto view_y = viewMenu->addAction("View Y");
        auto view_z = viewMenu->addAction("View Z");
        auto view_reset = viewMenu->addAction("Reset View");

        view_x->setCheckable(false);
        view_y->setCheckable(false);
        view_z->setCheckable(false);
        view_reset->setCheckable(false);

        QAction* selectedItem = myMenu.exec(globalPos);
        if (selectedItem) {
            auto text = selectedItem->text();
            if (text.contains("Gradient")) {
                d->is_Gradient = true;
                d->is_Black = false;
                d->is_White = false;
                ChangeBackGround(0);
            }
            else if (text.contains("Black")) {
                d->is_Gradient = false;
                d->is_Black = true;
                d->is_White = false;
                ChangeBackGround(1);
            }
            else if (text.contains("White")) {
                d->is_Gradient = false;
                d->is_Black = false;
                d->is_White = true;
                ChangeBackGround(2);
            }
            else if (text.contains("Reset View")) {
                reset3DView(true);
            }
            else if (text.contains("View X")) {
                setViewDirection(0);
            }
            else if (text.contains("View Y")) {
                setViewDirection(1);
            }
            else if (text.contains("View Z")) {
                setViewDirection(2);
            }
        }
    }

    auto ViewerRenderWindow3d::reset3DView(bool fromFunc) -> void {
        //auto root = getViewerExaminer()->getRenderArea()->getSceneGraph();        
        auto root = getSceneGraph();
        auto volData = MedicalHelper::find<SoVolumeData>(root);
        if (volData) {
            resetCameraZoom();
            //22.05
            MedicalHelper::orientView(MedicalHelper::AXIAL, getCamera(), volData);
            auto zoom = getCameraZoom();
            setCameraZoom(zoom * 0.7f);

            if (fromFunc) {
                //pending
                //d->is_Gradient = true;
                //d->is_Black = false;
                //d->is_White = false;
                //ChangeBackGround(0);
            }
        }
    }

    auto ViewerRenderWindow3d::ChangeBackGround(int type) -> void {
        SbVec3f start, end;
        auto isW = false;
        switch (type) {
        case 0://gradient
            start.setValue(0.7f, 0.7f, 0.8f);
            end.setValue(0.0, 0.1f, 0.3f);
            break;
        case 1://black
            start.setValue(0.0, 0.0, 0.0);
            end.setValue(0.0, 0.0, 0.0);
            break;
        case 2://white
            start.setValue(1.0, 1.0, 1.0);
            end.setValue(1.0, 1.0, 1.0);
            isW = true;
            break;
        }
        setGradientBackground(start, end);
        emit isWhite(isW);
    }

    auto ViewerRenderWindow3d::clearOrbit() -> void {
        d->orbit_x = 0.f;
        d->orbit_y = 0.f;

        /*/
        auto fc = getViewerExaminer()->getRenderArea()->getSceneInteractor()->getCameraInteractor()->getViewportCenter();
        getViewerExaminer()->getRenderArea()->getSceneInteractor()->getCameraInteractor()->setRotationCenter(fc);*/
        clearOrbitCenter();
    }

    auto ViewerRenderWindow3d::rotate(int axis, float val) -> void {
        d->orbit_x += val;
        d->orbit_y += val;

        SbVec2f newLocator(d->orbit_x, 0);

        SbVec3f axis_direction;
        if (axis == 0) {
            axis_direction.setValue(0, 1, 0);
            //setOrbitConstraint(false, true, false);
        }
        else if (axis == 1) {
            axis_direction.setValue(1, 0, 0);
            //setOrbitConstraint(true, false, false);
        }
        else if (axis == 2) {
            axis_direction.setValue(0, 0, 1);
            //setOrbitConstraint(false, false, true);
        }
        //getSceneGraph()->getCameraInteractor()->setRotationAxis(axis_direction);
        //getSceneGraph()->getCameraInteractor()->rotate(val);
        rotateTick(axis_direction, val);
    }

    auto ViewerRenderWindow3d::rotate(float x, float y, float z, float radian)->void {
        //auto cameraInteractor = getSceneGraph()->getCameraInteractor();
        //cameraInteractor->setRotationAxis(SbVec3f(x, y, z));
        //cameraInteractor->rotate(radian);
        rotateTick(SbVec3f(x, y, z), radian);
    }

    auto ViewerRenderWindow3d::tilting(int axis, int orbit, float tic, float inter) -> void {
        Q_UNUSED(axis)
            clearOrbit();

        auto val = tic / 5.f;

        for (int i = 0; i < orbit / 8; ++i) {
            d->orbit_x += val;
            d->orbit_y += val;
            SbVec2f newLocator(d->orbit_x, d->orbit_y);
            //getSceneGraph()->getCameraInteractor()->orbit(newLocator);
            spinCamera(newLocator);
            Sleep(inter);
        }
        for (int i = orbit / 8; i < orbit / 8 * 2; ++i) {
            d->orbit_x -= val;
            d->orbit_y -= val;
            SbVec2f newLocator(d->orbit_x, d->orbit_y);
            //getSceneGraph()->getCameraInteractor()->orbit(newLocator);
            spinCamera(newLocator);
            Sleep(inter);
        }
        for (int i = orbit / 8 * 2; i < orbit / 8 * 3; ++i) {
            d->orbit_x -= val;
            d->orbit_y += val;
            SbVec2f newLocator(d->orbit_x, d->orbit_y);
            //getSceneGraph()->getCameraInteractor()->orbit(newLocator);
            spinCamera(newLocator);
            Sleep(inter);
        }
        for (int i = orbit / 8 * 3; i < orbit / 8 * 4; ++i) {
            d->orbit_x += val;
            d->orbit_y -= val;
            SbVec2f newLocator(d->orbit_x, d->orbit_y);
            //getSceneGraph()->getCameraInteractor()->orbit(newLocator);
            spinCamera(newLocator);
            Sleep(inter);
        }
        for (int i = orbit / 8 * 4; i < orbit / 8 * 5; ++i) {
            d->orbit_x -= val;
            d->orbit_y -= val;
            SbVec2f newLocator(d->orbit_x, d->orbit_y);
            //getSceneGraph()->getCameraInteractor()->orbit(newLocator);
            spinCamera(newLocator);
            Sleep(inter);
        }
        for (int i = orbit / 8 * 5; i < orbit / 8 * 6; ++i) {
            d->orbit_x += val;
            d->orbit_y += val;
            SbVec2f newLocator(d->orbit_x, d->orbit_y);
            //getSceneGraph()->getCameraInteractor()->orbit(newLocator);
            spinCamera(newLocator);
            Sleep(inter);
        }
        for (int i = orbit / 8 * 6; i < orbit / 8 * 7; ++i) {
            d->orbit_x += val;
            d->orbit_y -= val;
            SbVec2f newLocator(d->orbit_x, d->orbit_y);
            //getSceneGraph()->getCameraInteractor()->orbit(newLocator);
            spinCamera(newLocator);
            Sleep(inter);
        }
        for (int i = orbit / 8 * 7; i < orbit / 8 * 8; ++i) {
            d->orbit_x -= val;
            d->orbit_y += val;
            SbVec2f newLocator(d->orbit_x, d->orbit_y);
            //getSceneGraph()->getCameraInteractor()->orbit(newLocator);
            spinCamera(newLocator);
            Sleep(inter);
        }
    }

    auto ViewerRenderWindow3d::setDefaultWindowType() -> void {
        //set default gradient background color
        SbVec3f start_color = { 0.7f,0.7f,0.8f };
        SbVec3f end_color = { 0.0,0.1f,0.3f };
        setGradientBackground(start_color, end_color);

        //set default camera type
        setCameraType(false);//perpective

        //set default navigation type
        //setNavigationMode(true);//orbit
    }

    auto ViewerRenderWindow3d::MouseMoveEvent(SoEventCallback* node) -> void {
        emit sigMouseMove();
        const SoLocation2Event* moveEvent = (const SoLocation2Event*)node->getEvent();
        if (isNavigation()) {
            auto pos = moveEvent->getPositionFloat();
            auto viewport_size = getViewportRegion().getViewportSizePixels();
            auto root = getSceneGraph();
            auto color = MedicalHelper::find<SoImage>(root, "icon_file");
            if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                auto image = new QImage(":/img/ic-setting-s.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            else {
                auto image = new QImage(":/img/ic-setting.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            if (d->mouseLeftPressed) {
                //spinCamera(moveEvent->getPositionFloat());
                spinCamera(moveEvent->getNormalizedPosition(getViewportRegion()));
            }
            if (d->mouseMiddlePressed) {
                //panCamera(moveEvent->getPositionFloat());
                panCamera(moveEvent->getNormalizedPosition(getViewportRegion()));
            }
        }
    }

    /*void ViewerRenderWindow3d::wheelEvent(QWheelEvent* event) {
        auto delta = (event->angleDelta().y() > 0) ? 1 : -1;
        auto zoom = getCameraZoom();
        setCameraZoom(zoom + static_cast<float>(delta));
    }*/
    auto ViewerRenderWindow3d::MouseWheelEvent(SoEventCallback* node) -> void {
        const SoMouseWheelEvent* mouseWheel = (const SoMouseWheelEvent*)node->getEvent();
        auto delta = mouseWheel->getDelta() > 0 ? 1 : -1;
        auto zoom = getCameraZoom();
        setCameraZoom(zoom + static_cast<float>(delta));
    }


    auto ViewerRenderWindow3d::MouseButtonEvent(SoEventCallback* node) -> void {
        const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)node->getEvent();

        if (isNavigation()) {
            //Press Event
            if (SoMouseButtonEvent::isButtonDoubleClickEvent(mouseButton, SoMouseButtonEvent::BUTTON3)) {
                reset3DView();
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                auto pos = mouseButton->getPositionFloat();
                auto viewport_size = getViewportRegion().getViewportSizePixels();
                if (!(pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25)) {
                    InteractiveCount(true);
                    //startSpin(mouseButton->getPosition());
                    startSpin(mouseButton->getNormalizedPosition(getViewportRegion()));
                    d->mouseLeftPressed = true;
                }
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                InteractiveCount(true);
                //startPan(mouseButton->getPosition());
                startPan(mouseButton->getNormalizedPosition(getViewportRegion()));
                d->mouseMiddlePressed = true;
            }
            //Release Event
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                auto pos = mouseButton->getPositionFloat();
                auto viewport_size = getViewportRegion().getViewportSizePixels();
                if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                    showContextMenu(pos);
                }
                else {
                    InteractiveCount(false);
                    d->mouseLeftPressed = false;
                }
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                InteractiveCount(false);
                d->mouseMiddlePressed = false;
            }
        }
    }

    auto ViewerRenderWindow3d::setViewDirection(int axis) -> void {
        const auto root = getSceneGraph();
        const auto volData = MedicalHelper::find<SoVolumeData>(root);
        if (nullptr == volData) {
            return;
        }

        switch (axis) {
        case 0:
            MedicalHelper::orientView(MedicalHelper::SAGITTAL, getCamera(), volData);
            break;
        case 1:
            MedicalHelper::orientView(MedicalHelper::CORONAL, getCamera(), volData);
            break;
        case 2:
            MedicalHelper::orientView(MedicalHelper::AXIAL, getCamera(), volData);
            break;
        default:;
        }
        setCameraZoom(getCameraZoom() * 0.7f);
    }

    auto ViewerRenderWindow3d::KeyboardEvent(SoEventCallback* node) -> void {
        const SoEvent* keyEvent = node->getEvent();

        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::S)) {
            //seek mode switching is handled by native viewer event callback        
            //set handled를 통해 event를 scene의 하위 callback으로 전달하지 않고 terminate
            //instance->setSeekMode(true);
            //seek mode는 우선 지원하지 않는것으로 20.10.16 Jose T. Kim
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
            //interaction mode가 selection 일때 alt키를 누르는 동안 임시로 navigation mode로의 전환
            if (!isNavigation()) {
                setInteractionMode(true);
                setAltPressed(true);
            }
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyReleaseEvent(keyEvent, SoKeyboardEvent::Key::LEFT_ALT)) {
            if (isAltPressed()) {
                setInteractionMode(false);
                setAltPressed(false);
            }
            node->setHandled();
        }
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::ESCAPE)) {
            if (isNavigation())
                setInteractionMode(false);
            else
                setInteractionMode(true);
            node->setHandled();
        }
        //Shift + F12는 native viewer event callback이 해결하도록
    }

    auto ViewerRenderWindow3d::closeEvent(QCloseEvent* unused) -> void {
        Q_UNUSED(unused)
    }
}
