#pragma once

#include <IMainWindowTA.h>

#include <Scene.h>

#include "ILicensed.h"

class SoSeparator;

namespace TomoAnalysis::Viewer::AppUI {
    class MainWindow : public IMainWindowTA, public TomoAnalysis::License::ILicensed {
        Q_OBJECT
    public:
        explicit MainWindow(QWidget* parent = nullptr);
        virtual ~MainWindow();

        auto GetCurTCF()const->QString;

        auto GetParameter(QString name) -> IParameter::Pointer override;
        auto SetParameter(QString name, IParameter::Pointer param) -> void override;
        
        auto Execute(const QVariantMap& parmas)->bool override;
        auto GetRunType() -> std::tuple<bool, bool> override;

        auto TryDeactivate() -> bool final;
        auto TryActivate() -> bool final;
        auto IsActivate() -> bool final;
        auto GetMetaInfo()->QVariantMap final;

        auto setOivNodes(SoSeparator* node, SoSeparator* node2d[3])->void;
        auto Init(void)->bool;
        auto Reset()->void;
        auto RequsetRender()->void;
        auto OpenFile(const QString& path)->bool;

        auto SetVisibleAxisGrid(const bool& visible) const ->void;
        auto SetVisibleBoundaryBox(const bool& visible) const ->void;
        auto SetVisibleOrientationMarker(const bool& visible)const->void;
        auto SetVisibleTimeStamp(const bool& visible)const->void;
        auto SetTimeStampColor(QColor color)const->void;
        auto SetTimeStampSize(int size)const->void;
        auto SetVolumeResolution(int res)const->void;
        auto SetSliceResolution(int res)const->void;
        auto SetAxisGridFontSize(int size)const->void;
        auto SetAxisGridColor(int axis, QColor color)const->void;

        auto ChangeLayout(const int& type) const ->void;
        auto ResetLayout(const bool& reset) const ->void;
        auto SaveLayout(const QString& path) const ->void;
        auto LoadLayout(const QString& path) const ->int;                

        auto ForceClosePopup()->void;

        static void* OpenFileThread3d(void* userData);

        auto OnAccepted() -> void override;
        auto OnRejected() -> void override;
        auto GetFeatureName() const -> QString override;

    protected:
        void resizeEvent(QResizeEvent* event) override;

    public slots:
        void onTimelapseIndexChanged(double time);
        void onTimelapseIndexPlayed(double time);

    protected slots:
        void OnSignalPrimitive(const ctkEvent& ctkEvent);

        void onActivatedModalityChanged(Entity::Modality modality);

        void onSliceIndexChanged(int viewIndex, int sliceIndex) const;
        void onPlanePositionChanged(int x, int y, int z);
        void onPhyPlanePositionChanged(float x, float y, float z);

        void onGradientRangeLoaded(double min, double max) const;
        void onTransferFunctionChanged(Entity::TFItemList list) const;
        void onTransferFunctionItemChanged(int index, Entity::TFItem::Pointer item) const;
        void onTransferFunctionOverlayChanged(bool isOverlay) const;
        void onTransferFunctionClear();

        void onChannelVisibleChanged(Entity::Channel channel, bool visible) const;
        void onChannelRangeChanged(Entity::Channel channel, int min, int max) const;
        void onChannelColorChanged(Entity::Channel channel, QColor color)const;
        void onChannelOpacityChanged(Entity::Channel channel, int opacity) const;
        void onChannelGammaChanged(Entity::Channel channel, bool enable, double gamma)const;
        void onTimelapseInfoChanged()const;

        void onDisplayChanged(Entity::DisplayType)const;

        void onLoadPreset(QString) const;
        void onSavePreset(QString, bool) const;
        void onDeletePreset(QString) const;

        void onLoadImage();
        void onSetSlice();
        void onLoadDefaultPreset();
        void onOpenFileFinished();
        void onOpenCanceled();

    signals:
        void sigLoadImage3d();
        void sigSetSlice3d();
        void sigLoadPreset3d();
        void sigOpenThreadFinished3d();
    private:
        auto InitUI(void)->void;
        auto OnMenuAction(const ctkEvent& ctkEvent)->void;
        auto OnMenuActionChecked(const ctkEvent& ctkEvent)->void;
        auto OnMenuActionUnChecked(const ctkEvent& ctkEvent)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}