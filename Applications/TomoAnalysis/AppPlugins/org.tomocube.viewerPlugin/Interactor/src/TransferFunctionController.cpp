#include <TransferFunctionManager.h>

#include "TransferFunctionController.h"

namespace TomoAnalysis::Viewer::Interactor {
	struct TransferFunctionController::Impl {
		Impl() {}
		Impl(UseCase::ITransferFunctionOutputPort* outPort)
	    : outPort(outPort) {}

		UseCase::ITransferFunctionOutputPort* outPort{ nullptr };
	};

	TransferFunctionController::TransferFunctionController()
		: d(new Impl()) {
	}

	TransferFunctionController::TransferFunctionController(UseCase::ITransferFunctionOutputPort* outPort)
		: d(new Impl(outPort)) {
	}

	TransferFunctionController::~TransferFunctionController() {
	}

	auto TransferFunctionController::SetTransFunction(const Entity::Scene::ID& sceneID, const Entity::TFItemList& list) const ->bool {
		auto use_case = UseCase::TransferFunctionManager();
		return use_case.SetList(list, sceneID, d->outPort);
	}

	auto TransferFunctionController::AddTransFunctionItem(const Entity::Scene::ID& sceneID,
		                                                  const double& intensityMin, const double& intensityMax,
		                                                  const double& gradientMin, const double& gradientMax,
		                                                  const float& opacity, const Entity::Color& color, bool visible) const ->bool {
		Entity::TFItem::Pointer item;
		item->intensityMin = intensityMin;
		item->intensityMax = intensityMax;
		item->gradientMin = gradientMin;
		item->gradientMax = gradientMax;
		item->transparency = opacity;
		item->color = color;
		item->visible = visible;

	    auto use_case = UseCase::TransferFunctionManager();
		return use_case.Create(item, sceneID, d->outPort);
	}

	auto TransferFunctionController::ModifyTransFunctionItem(const Entity::Scene::ID& sceneID,
			                         const int& index, const double& intensityMin, const double& intensityMax,
		                             const double& gradientMin, const double& gradientMax, const float& opacity,
			                         const Entity::Color& color, bool visible) const ->bool {
		Entity::TFItem::Pointer item = std::make_shared<Entity::TFItem>();
		item->intensityMin = intensityMin;
		item->intensityMax = intensityMax;
		item->gradientMin = gradientMin;
		item->gradientMax = gradientMax;
		item->transparency = opacity;
		item->color = color;
		item->visible = visible;

		auto use_case = UseCase::TransferFunctionManager();
		return use_case.Modify(index, item, sceneID, d->outPort);
	}

	auto TransferFunctionController::DeleteTransFunctionItem(const Entity::Scene::ID& sceneID, const int& index) const ->bool {
		auto use_case = UseCase::TransferFunctionManager();
		return use_case.Delete(index, sceneID, d->outPort);
	}

	auto TransferFunctionController::ClearTransFunctionItem(const Entity::Scene::ID& sceneID) const ->bool {
		auto use_case = UseCase::TransferFunctionManager();
		return use_case.Clear(sceneID, d->outPort);
	}

	auto TransferFunctionController::SetVisibleTransFunctionItem(const Entity::Scene::ID& sceneID, const int& index, bool visible) const ->bool {
		auto use_case = UseCase::TransferFunctionManager();
		return use_case.SetVisible(index, visible, sceneID, d->outPort);
	}
}