#include "INavigatorPanel.h"

namespace TomoAnalysis::Viewer::Interactor {
    struct INavigatorPanel::Impl {
        NavigatorDS::Pointer navigatorDS = std::make_shared<NavigatorDS>();
    };

    INavigatorPanel::INavigatorPanel() : d{ new Impl } {
    }

    INavigatorPanel::~INavigatorPanel() {
    }

    auto INavigatorPanel::GetNavigatorDS() const ->NavigatorDS::Pointer {
        return d->navigatorDS;
    }
}