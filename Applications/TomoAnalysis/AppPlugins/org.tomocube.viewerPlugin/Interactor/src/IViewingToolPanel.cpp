#include "IViewingToolPanel.h"

namespace TomoAnalysis::Viewer::Interactor {
    struct IViewingToolPanel::Impl {
        ViewingToolDS::Pointer viewingToolDS = std::make_shared<ViewingToolDS>();
    };

    IViewingToolPanel::IViewingToolPanel() : d{ new Impl } {
    }

    IViewingToolPanel::~IViewingToolPanel() {
    }

    auto IViewingToolPanel::GetViewingToolDS() const -> ViewingToolDS::Pointer {
        return d->viewingToolDS;
    }
}