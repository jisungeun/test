#include <iostream>

#include "ScenePresenter.h"

namespace TomoAnalysis::Viewer::Interactor {
    struct ScenePresenter::Impl {
        Impl() : viewer(nullptr),panel(nullptr) {}
        Impl(IImageViewer* viewer, IViewingToolPanel* panel) : viewer(viewer),panel(panel) {}

		IImageViewer* viewer;
		IViewingToolPanel* panel;
    };

    ScenePresenter::ScenePresenter( )
        : d(new Impl()) {
    }

    ScenePresenter::ScenePresenter(IImageViewer* viewer,IViewingToolPanel* panel)
        : d(new Impl(viewer,panel)) {
    }

    ScenePresenter::~ScenePresenter() {
        
    }

    void ScenePresenter::Update(Entity::Scene::Pointer& scene) {
        if(!scene.get()) {
			return;
        }

		if(nullptr == d->viewer) {
		    return;
		}
		
		const auto image = scene->GetImage();
		if (!image) {
			return;
		}

        auto viewerScene = d->viewer->GetSceneDS();
		viewerScene->Clear();

		// get activated modality
		viewerScene->activatedModality = scene->GetActivateModality();

        // get layout info
		viewerScene->layoutType = scene->GetLayoutType();

		// get view config
		viewerScene->viewConfigList = scene->GetViewConfigList();
		
        // get channel info
		viewerScene->flChannelList = scene->GetFLChannelInfoList();
		
		// get visible render tool
		
		viewerScene->visibleBoundaryBox = scene->GetVisibleBoundaryBox();
		viewerScene->visibleAxisGrid = scene->GetVisibleAxisGrid();
		viewerScene->visibleOrientationMarker = scene->GetVisibleOrientationMarker();
		viewerScene->visibleTimeStamp = scene->GetVisibleTimeStamp();				
		for (auto i = 0; i < 3; i++) {
			viewerScene->axisGridColor[i][0] = std::get<0>(scene->GetAxisGridColor(i));
			viewerScene->axisGridColor[i][1] = std::get<1>(scene->GetAxisGridColor(i));
			viewerScene->axisGridColor[i][2] = std::get<2>(scene->GetAxisGridColor(i));
		}
		viewerScene->timestampR = std::get<0>(scene->GetTimeStampColor());
		viewerScene->timestampG = std::get<1>(scene->GetTimeStampColor());
		viewerScene->timestampB = std::get<2>(scene->GetTimeStampColor());
		viewerScene->timestampSize = scene->GetTimeStampSize();
		viewerScene->isOverlay = scene->GetEnableOverlay();		
		//viewerScene->timelapseIndex = scene->GetTimelapseIndex();
	    viewerScene->timelapseTime = scene->GetTimelapseTime();
		viewerScene->slice_resolution = scene->GetResolution(false);
		viewerScene->volume_resolution = scene->GetResolution(true);
		viewerScene->axisGridFontSize = scene->GetAxisGridFontSize();

		d->viewer->Refresh();

		if (nullptr != d->panel) {			
			auto ds = d->panel->GetViewingToolDS();
			ds->timestampColor[0] = viewerScene->timestampR;
			ds->timestampColor[1] = viewerScene->timestampG;
			ds->timestampColor[2] = viewerScene->timestampB;
			ds->timestampSize = static_cast<float>(viewerScene->timestampSize);
			ds->layoutType = viewerScene->layoutType;
			ds->showOrientationMarker = viewerScene->visibleOrientationMarker;
			ds->showAxisGrid = viewerScene->visibleAxisGrid;
			ds->showTimeStamp = viewerScene->visibleTimeStamp;
			ds->showBoundaryBox = viewerScene->visibleBoundaryBox;			
			ds->axisGridFontSize = viewerScene->axisGridFontSize;
			for (auto i = 0; i < 3; i++) {
				ds->axisXColor[i] = viewerScene->axisGridColor[0][i];
				ds->axisYColor[i] = viewerScene->axisGridColor[1][i];
				ds->axisZColor[i] = viewerScene->axisGridColor[2][i];
			}
			d->panel->Update();
		}
    }
}