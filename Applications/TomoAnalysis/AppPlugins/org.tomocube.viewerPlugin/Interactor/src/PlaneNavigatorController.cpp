#include <PlaneNavigator.h>

#include "PlaneNavigatorController.h"

namespace TomoAnalysis::Viewer::Interactor {
	struct PlaneNavigatorController::Impl {
		Impl() {}
		Impl(UseCase::ISliceNavigateOutputPort* outPort)
	    : outPort(outPort) {}

		UseCase::ISliceNavigateOutputPort* outPort{ nullptr };
	};

	PlaneNavigatorController::PlaneNavigatorController()
		: d(new Impl()) {
	}

	PlaneNavigatorController::PlaneNavigatorController(UseCase::ISliceNavigateOutputPort* outPort)
		: d(new Impl(outPort)) {
	}

	PlaneNavigatorController::~PlaneNavigatorController() {
	}

	auto PlaneNavigatorController::MovePlane(const Entity::Scene::ID& sceneID, 
		                                     const int& x, const int& y, const int& z) const -> bool {
		auto use_case = UseCase::PlaneNavigator();
		return use_case.MovePlane(x, y, z, sceneID, d->outPort);
    }

	auto PlaneNavigatorController::MovePhyPlane(const Entity::Scene::ID& sceneID, 
		                                        const float& x, const float& y, const float& z) const ->bool {
		auto use_case = UseCase::PlaneNavigator();
		return use_case.MovePhyPlane(x, y, z, sceneID, d->outPort);
	}

	auto PlaneNavigatorController::MoveSlice(const Entity::Scene::ID& sceneID, const int& viewIndex, const int& sliceIndex) const ->bool {
		auto use_case = UseCase::PlaneNavigator();
		return use_case.MoveSlice(viewIndex, sliceIndex, sceneID, d->outPort);
	}
	auto PlaneNavigatorController::InitSlice(const Entity::Scene::ID& sceneID) const -> bool {
		auto use_case = UseCase::PlaneNavigator();
		return use_case.InitSlice(sceneID, d->outPort);
    }

}