#include <memory>

#include <AllocateScene.h>

#include "AllocateSceneController.h"

namespace TomoAnalysis::Viewer::Interactor {
	AllocateSceneController::AllocateSceneController() {
	}

	AllocateSceneController::~AllocateSceneController() {
	}

	auto AllocateSceneController::RequestSceneAllocation() const -> Entity::Scene::ID {
		auto useCase = std::make_unique<UseCase::AllocateScene>();
		return useCase->Request();
	}
}