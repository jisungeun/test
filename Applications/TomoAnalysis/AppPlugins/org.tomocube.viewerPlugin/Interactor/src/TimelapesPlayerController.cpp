#include <ImageAccessor.h>

#include "TimelapesPlayerController.h"

namespace TomoAnalysis::Viewer::Interactor {
	struct TimelapsePlayerController::Impl {
		Impl() {}
		Impl(UseCase::ISceneOutputPort* outPort)
	    : outPort(outPort) {}

		UseCase::ISceneOutputPort* outPort{ nullptr };
	};

	TimelapsePlayerController::TimelapsePlayerController()
		: d(new Impl()) {
	}

	TimelapsePlayerController::TimelapsePlayerController(UseCase::ISceneOutputPort* outPort)
		: d(new Impl(outPort)) {
	}

	TimelapsePlayerController::~TimelapsePlayerController() {
	}

	auto TimelapsePlayerController::Play(Entity::Scene::ID sceneID, const double& frameTime) const -> bool {
		auto use_case = UseCase::ImageAccessor();
		return use_case.SetTimelapseFrame(frameTime, sceneID, d->outPort);
	}
}
