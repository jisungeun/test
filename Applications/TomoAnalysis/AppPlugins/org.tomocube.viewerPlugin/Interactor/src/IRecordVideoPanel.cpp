#include "IRecordVideoPanel.h"

namespace TomoAnalysis::Viewer::Interactor {
    struct IRecordVideoPanel::Impl {
        RecordVideoDS::Pointer recordVideoDS = std::make_shared<RecordVideoDS>();
    };

    IRecordVideoPanel::IRecordVideoPanel() : d{ new Impl } {
    }

    IRecordVideoPanel::~IRecordVideoPanel() {
    }

    auto IRecordVideoPanel::GetRecordVideoDS() const ->RecordVideoDS::Pointer {
        return d->recordVideoDS;
    }
}