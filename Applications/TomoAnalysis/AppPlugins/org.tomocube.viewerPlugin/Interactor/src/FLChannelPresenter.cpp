#include "FLChannelPresenter.h"

namespace TomoAnalysis::Viewer::Interactor {
    struct FLChannelPresenter::Impl {
        Impl() : viewer(nullptr) {}

		IImageViewer* viewer;
    };

    FLChannelPresenter::FLChannelPresenter()
        : d(new Impl()) {
    }

    FLChannelPresenter::FLChannelPresenter(IImageViewer* viewer)
        : d(new Impl()) {

        d->viewer = viewer;
    }

    FLChannelPresenter::~FLChannelPresenter() {

    }

    void FLChannelPresenter::Update(const Entity::FLChannelInfo& info) {
        if(!d->viewer) {
            return;
        }

        d->viewer->UpdateChannelInfo(info);
    }


}
