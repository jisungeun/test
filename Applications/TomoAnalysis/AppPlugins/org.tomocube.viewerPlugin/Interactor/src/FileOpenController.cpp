#include <iostream>

#include <ImageLoader.h>

#include "FileOpenController.h"

namespace TomoAnalysis::Viewer::Interactor {
	struct FileOpenController::Impl {
		Impl() {}
		Impl(UseCase::ITCFInfoOutputPort* outPort, UseCase::IFileReaderPort* readerPort)
	    : outPort(outPort)
	    , readerPort(readerPort) {}

		UseCase::ITCFInfoOutputPort* outPort{ nullptr };
		UseCase::IFileReaderPort* readerPort{ nullptr };
	};

	FileOpenController::FileOpenController()
		: d(new Impl()) {
	}

	FileOpenController::FileOpenController(UseCase::ITCFInfoOutputPort* outPort, UseCase::IFileReaderPort* readerPort)
		: d(new Impl(outPort, readerPort)) {
	}

	FileOpenController::~FileOpenController() {
	}

	auto FileOpenController::LoadImages(Entity::Scene::ID sceneID, const std::string& path, bool isCrop, int offsetX, int offsetY, int sizeX, int sizeY,int offsetXFL,int offsetYFL,int sizeXFL,int sizeYFL) const -> bool {
		auto use_case = UseCase::ImageLoader();
		if (isCrop) {
			return use_case.Load(path, offsetX,offsetY,sizeX,sizeY,offsetXFL,offsetYFL,sizeXFL,sizeYFL, sceneID, d->outPort, d->readerPort);
		}
		return use_case.Load(path,  sceneID, d->outPort, d->readerPort);
	}
}
