#include "ITransferFunctionPanel.h"

namespace TomoAnalysis::Viewer::Interactor {
    struct ITransferFunctionPanel::Impl {
        HTMetaDS::Pointer htMetaDS = std::make_shared<HTMetaDS>();
        TransferFunctionDS::Pointer transferFunctionDS = std::make_shared<TransferFunctionDS>();
    };

    ITransferFunctionPanel::ITransferFunctionPanel() : d{ new Impl } {
    }

    ITransferFunctionPanel::~ITransferFunctionPanel() {
    }

    auto ITransferFunctionPanel::GetHTMetaDS() const->HTMetaDS::Pointer {
        return d->htMetaDS;
    }

    auto ITransferFunctionPanel::GetTransferFunctionDS() const->TransferFunctionDS::Pointer {
        return d->transferFunctionDS;
    }
}