#include "IModalityPanel.h"

namespace TomoAnalysis::Viewer::Interactor {
    struct IModalityPanel::Impl {
        ModalityDS::Pointer modalityDS = std::make_shared<ModalityDS>();
    };

    IModalityPanel::IModalityPanel() : d{ new Impl } {
    }

    IModalityPanel::~IModalityPanel() {
    }

    auto IModalityPanel::GetModalityDS() const -> ModalityDS::Pointer {
        return d->modalityDS;
    }
}