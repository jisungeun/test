#include "TransferFunctionPresenter.h"

namespace TomoAnalysis::Viewer::Interactor {
    struct TransferFunctionPresenter::Impl {
        Impl() : viewer(nullptr) {}

		IImageViewer* viewer;
    };

	TransferFunctionPresenter::TransferFunctionPresenter()
        : d(new Impl()) {
    }

	TransferFunctionPresenter::TransferFunctionPresenter(IImageViewer* viewer)
        : d(new Impl()) {

        d->viewer = viewer;
    }

	TransferFunctionPresenter::~TransferFunctionPresenter() {

    }

    void TransferFunctionPresenter::Update(const int& index, const Entity::TFItem::Pointer& item) {
	    if(nullptr == d->viewer) {
	        return;
	    }

        d->viewer->UpdateTransferFunction(index, item);
	}

    void TransferFunctionPresenter::Clear(void) {
		if (nullptr == d->viewer) {
			return;
		}

		d->viewer->ClearTransferFunction();
	}
}
