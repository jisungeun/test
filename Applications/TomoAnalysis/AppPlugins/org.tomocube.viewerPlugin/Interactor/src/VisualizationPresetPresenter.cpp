#include "VisualizationPresetPresenter.h"

namespace TomoAnalysis::Viewer::Interactor {
    struct VisualizationPresetPresenter::Impl {
        Impl(){}

        IImageViewer* viewerPanel = nullptr;
        ITransferFunctionPanel* transferFunctionPanel = nullptr;
        IFLChannelPanel* flChannelPanel = nullptr;
        IVisualizationListPanel* visualizationListPanel = nullptr;
        IModalityPanel* modalityPanel = nullptr;
    };

    VisualizationPresetPresenter::VisualizationPresetPresenter()
        : d(new Impl()) {
    }

    VisualizationPresetPresenter::VisualizationPresetPresenter(IImageViewer* viewerPanel, ITransferFunctionPanel* transferFunctionPanel,
                                                               IFLChannelPanel* flChannelPanel, IVisualizationListPanel* visualizationListPanel,IModalityPanel* modalityPanel)
        : d(new Impl()) {

        d->viewerPanel = viewerPanel;
        d->transferFunctionPanel = transferFunctionPanel;
        d->flChannelPanel = flChannelPanel;
        d->visualizationListPanel = visualizationListPanel;
        d->modalityPanel = modalityPanel;
    }

    VisualizationPresetPresenter::~VisualizationPresetPresenter() {

    }

    void VisualizationPresetPresenter::Update(const std::string& tcfFolderPath) {
        if (nullptr == d->visualizationListPanel) {
            return;
        }

        auto ds = d->visualizationListPanel->GetCommonDS();
        ds->tcfPath = tcfFolderPath;

        d->visualizationListPanel->Update();
    }

    void VisualizationPresetPresenter::LoadList(const std::vector<std::string>& list) {
        if(nullptr == d->visualizationListPanel) {
            return;
        }

        auto ds = d->visualizationListPanel->GetListDS();
        ds->list = list;

        d->visualizationListPanel->Refresh();
	}

    void VisualizationPresetPresenter::LoadPreset(const std::string& path, const Entity::TFItemList& tfItemList, Entity::FLChannelInfo& channel) {
        if (nullptr == d->visualizationListPanel) {
            return;
        }

        // VisualizationListPanel
        auto presetDS = d->visualizationListPanel->GetPresetDS();
        presetDS->currentPreset = path;
        presetDS->tfItemList = tfItemList;
        presetDS->channel = channel;
        d->visualizationListPanel->Load();

        // FLChannelPanel
                
		auto flMetaDS = d->flChannelPanel->GetFLMetaDS();
        auto flVisible = false;
		for (uint8_t i = 0; i < channel.size(); ++i) {
			if (channel[Entity::Channel::_from_integral(i)]) {
                //flVisible = channel[Entity::Channel::_from_integral(i)]->visible && channel[Entity::Channel::_from_integral(i)]->isValid;
                flVisible = channel[Entity::Channel::_from_integral(i)]->visible && flMetaDS->flIntensity[i].isValid;
                flMetaDS->flIntensity[i].isVisible = channel[Entity::Channel::_from_integral(i)]->visible;
                //flMetaDS->flIntensity[i].isValid = channel[Entity::Channel::_from_integral(i)]->isValid;
                
				flMetaDS->flIntensity[i].min = static_cast<float>(channel[Entity::Channel::_from_integral(i)]->min);
				flMetaDS->flIntensity[i].max = static_cast<float>(channel[Entity::Channel::_from_integral(i)]->max);
				flMetaDS->flIntensity[i].opacity = static_cast<float>(channel[Entity::Channel::_from_integral(i)]->opacity);                
			}
		}
        d->flChannelPanel->Update();

        if(nullptr != d->modalityPanel) {
            auto modalDS = d->modalityPanel->GetModalityDS();
            int modality = Entity::Modality::None;
            if(false == tfItemList.empty()) {
                //force turn on HT modality
                modality = static_cast<Entity::Modality>(modality | Entity::Modality::HTVolume);
            }
            if(flVisible) {
                //force turn on FL modality
                modality = static_cast<Entity::Modality>(modality | Entity::Modality::FLVolume);
            }            
            modalDS->ForceModality = modality;
            d->modalityPanel->Update();
        }

        // viewer
        d->viewerPanel->UpdateVisualizationData(tfItemList, channel);
    }

    void VisualizationPresetPresenter::SavePreset(const std::string& path, const bool& saveAs) {
        if (nullptr == d->visualizationListPanel) {
            return;
        }

        d->visualizationListPanel->Save(path, saveAs);
    }
}

