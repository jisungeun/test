#pragma once
#include <memory>
#include <string>

#include <Scene.h>
#include <TransferFunction.h>
#include <ITransferFunctionOutputPort.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	class ViewerInteractor_API TransferFunctionController final {
	public:
		TransferFunctionController();
		TransferFunctionController(UseCase::ITransferFunctionOutputPort* outPort);
		TransferFunctionController(const TransferFunctionController& other) = delete;
		~TransferFunctionController();

		auto SetTransFunction(const Entity::Scene::ID& sceneID, const Entity::TFItemList& list) const -> bool;

        auto AddTransFunctionItem(const Entity::Scene::ID& sceneID,
			                      const double& intensityMin, const double& intensityMax,
			                      const double& gradientMin, const double& gradientMax, 
                                  const float& opacity, const Entity::Color& color, bool visible = true) const ->bool;

        auto ModifyTransFunctionItem(const Entity::Scene::ID& sceneID,
			                         const int& index, const double& intensityMin, const double& intensityMax, 
			                         const double& gradientMin, const double& gradientMax, const float& opacity,
			                         const Entity::Color& color, bool visible = true) const ->bool;

        auto DeleteTransFunctionItem(const Entity::Scene::ID& sceneID, const int& index) const ->bool;
		auto ClearTransFunctionItem(const Entity::Scene::ID& sceneID) const ->bool;

		auto SetVisibleTransFunctionItem(const Entity::Scene::ID& sceneID, const int& index, bool visible) const ->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}