#pragma once

#include <memory>
#include <ITCFInfoOutputPort.h>

#include "IImageViewer.h"
#include "IModalityPanel.h"
#include "ITCFPlayerPanel.h"
#include "ITransferFunctionPanel.h"
#include "IFLChannelPanel.h"
#include "IScreenShotPanel.h"
#include "IRecordVideoPanel.h"
#include "INavigatorPanel.h"
#include "IVisualizationListPanel.h"
#include "IDisplayPanel.h"
#include "IViewingToolPanel.h"

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
    class ViewerInteractor_API TCFInfoPresenter : public UseCase::ITCFInfoOutputPort {
    public:
        TCFInfoPresenter();
        TCFInfoPresenter(IImageViewer* viewerPanel,
                         IModalityPanel* modalityPanel, IDisplayPanel* displayPanel,
                         ITransferFunctionPanel* transferFunctionPanel, 
                         IFLChannelPanel* flChannelPanel, ITCFPlayerPanel* tcfPlayerPanel,
                         /*IScreenShotPanel* screenShotPanel,*/ /*IRecordVideoPanel* recordVideoPanel,*/
                         INavigatorPanel* navigatorPanel, IVisualizationListPanel* visualizationListPanel,IViewingToolPanel* viewingToolPanel);
        ~TCFInfoPresenter();

        void Update(Entity::Image::Pointer& image) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
