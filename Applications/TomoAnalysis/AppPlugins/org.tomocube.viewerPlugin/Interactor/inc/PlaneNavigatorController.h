#pragma once
#include <memory>
#include <string>

#include <Scene.h>
#include <ISliceNavigateOutputPort.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	class ViewerInteractor_API PlaneNavigatorController final {
	public:
		PlaneNavigatorController();
		PlaneNavigatorController(UseCase::ISliceNavigateOutputPort* outPort);
		PlaneNavigatorController(const PlaneNavigatorController& other) = delete;
		~PlaneNavigatorController();

		auto InitSlice(const Entity::Scene::ID& sceneID)const ->bool;
		auto MovePlane(const Entity::Scene::ID& sceneID, const int& x, const int& y, const int& z) const ->bool;
		auto MovePhyPlane(const Entity::Scene::ID& sceneID, const float& x, const float& y, const float& z) const ->bool;
        auto MoveSlice(const Entity::Scene::ID& sceneID, const int& viewIndex, const int& sliceIndex) const ->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}