#pragma once

#include <memory>
#include <ISceneOutputPort.h>

#include "IImageViewer.h"
#include "IViewingToolPanel.h"

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
    class ViewerInteractor_API ScenePresenter : public UseCase::ISceneOutputPort {
    public:
		ScenePresenter();
		ScenePresenter(IImageViewer* viewer, IViewingToolPanel* panel=nullptr);
		~ScenePresenter();

		void Update(Entity::Scene::Pointer& scene) override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}