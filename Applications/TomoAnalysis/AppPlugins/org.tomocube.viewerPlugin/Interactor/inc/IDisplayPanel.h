#pragma once

#include <Scene.h>

#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	struct ViewerInteractor_API DisplayDS {
		Entity::DisplayType usableDisplayType = Entity::DisplayType::DISPLAY_NONE;
		typedef std::shared_ptr<DisplayDS> Pointer;
	};

	class ViewerInteractor_API IDisplayPanel {
	public:
		IDisplayPanel();
		virtual ~IDisplayPanel();

		auto GetDisplayDS() const->DisplayDS::Pointer;

		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}