#pragma once

#include <string>
#include <memory>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	struct ViewerInteractor_API ScreenShotDS {
		std::string tcfName;

		typedef std::shared_ptr<ScreenShotDS> Pointer;
	};

	class ViewerInteractor_API IScreenShotPanel {
	public:
		IScreenShotPanel();
		virtual ~IScreenShotPanel();

		auto GetScreenShotDS() const->ScreenShotDS::Pointer;

		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}