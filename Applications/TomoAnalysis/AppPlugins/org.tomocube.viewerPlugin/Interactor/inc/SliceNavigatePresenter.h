#pragma once

#include <memory>
#include <ISliceNavigateOutputPort.h>

#include "IImageViewer.h"
#include "INavigatorPanel.h"

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
    class ViewerInteractor_API SliceNavigatePresenter : public UseCase::ISliceNavigateOutputPort {
    public:
		SliceNavigatePresenter();
		SliceNavigatePresenter(IImageViewer* viewer, INavigatorPanel* navigatorPanel);
		~SliceNavigatePresenter();

		void Update(Entity::Scene::Pointer& scene, const int& posX, const int& posY, const int& posZ) override;
		void UpdatePhy(Entity::Scene::Pointer& scene, const float& x, const float& y, const float& z) override;

    private:
		struct Impl;
		std::unique_ptr<Impl> d;
    };
}