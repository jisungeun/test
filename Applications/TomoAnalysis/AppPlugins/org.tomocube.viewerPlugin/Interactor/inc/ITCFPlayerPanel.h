#pragma once

#include <Scene.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	struct ViewerInteractor_API TimelapseInfo {
		int dataCount = -1;
		float interval = -1.f;
		std::vector<double> time_points[3];
		typedef std::shared_ptr<TimelapseInfo> Pointer;
	};

	struct ViewerInteractor_API TimelapesDS {
		std::map<Entity::ImageType, TimelapseInfo::Pointer> list;

	    typedef std::shared_ptr<TimelapesDS> Pointer;
	};

	class ViewerInteractor_API ITCFPlayerPanel {
	public:
		ITCFPlayerPanel();
		virtual ~ITCFPlayerPanel();

		auto GetTimelapesDS() const ->TimelapesDS::Pointer;

		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}