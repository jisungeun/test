#pragma once

#include <memory>

#include <Scene.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	struct ViewerInteractor_API NavigatorInfo {
		int width = 0;
		int height = 0;
		int depth = 0;

		float resolutionX = 0.f;
		float resolutionY = 0.f;
		float resolutionZ = 0.f;

		typedef std::shared_ptr<NavigatorInfo> Pointer;
	};

	struct ViewerInteractor_API NavigatorDS {
		std::map<Entity::ImageType, NavigatorInfo::Pointer> list;

		int x = 0;
		int y = 0;
		int z = 0;

		float phyX = 0.f;
		float phyY = 0.f;
		float phyZ = 0.f;

		typedef std::shared_ptr<NavigatorDS> Pointer;
	};

	class ViewerInteractor_API INavigatorPanel {
	public:
		INavigatorPanel();
		virtual ~INavigatorPanel();

		auto GetNavigatorDS() const->NavigatorDS::Pointer;

		virtual auto Update()->bool = 0;
		virtual auto Refresh()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}