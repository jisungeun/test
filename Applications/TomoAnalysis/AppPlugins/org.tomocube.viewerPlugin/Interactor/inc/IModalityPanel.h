#pragma once

#include <Scene.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	struct ViewerInteractor_API ModalityDS {
		int usableModality = Entity::Modality::None;
		int ForceModality = Entity::Modality::None;
		typedef std::shared_ptr<ModalityDS> Pointer;
	};

	class ViewerInteractor_API IModalityPanel {
	public:
		IModalityPanel();
		virtual ~IModalityPanel();

		auto GetModalityDS() const->ModalityDS::Pointer;

		virtual auto Update()->bool = 0;		

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}