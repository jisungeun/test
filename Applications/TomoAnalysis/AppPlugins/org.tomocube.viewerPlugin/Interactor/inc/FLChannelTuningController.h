#pragma once
#include <memory>

#include <Scene.h>
#include <IFLChannelOutputPort.h>
#include <FLChannelManager.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	class ViewerInteractor_API FLChannelTuningController final {
	public:
		FLChannelTuningController();
		FLChannelTuningController(UseCase::IFLChannelOutputPort* outPort);
		FLChannelTuningController(const FLChannelTuningController& other) = delete;
		~FLChannelTuningController();

		auto SetRange(Entity::Scene::ID sceneID, const Entity::Channel& channel, const int& min, const int& max) const ->bool;
		auto SetGamma(Entity::Scene::ID sceneID, const Entity::Channel& channel, const int& value, const bool& isGamma) const ->bool;
		auto SetOpacity(Entity::Scene::ID sceneID, const Entity::Channel& channel, const int& value) const ->bool;
		auto SetColor(Entity::Scene::ID sceneID, const Entity::Channel& channel, const int& r, const int& g, const int& b)const->bool;
		auto SetVisible(Entity::Scene::ID sceneID, const Entity::Channel& channel, const bool& visible) const ->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}