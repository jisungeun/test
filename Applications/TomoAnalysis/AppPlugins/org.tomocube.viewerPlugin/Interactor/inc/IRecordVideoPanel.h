#pragma once

#include <string>
#include <memory>

#include <Scene.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	struct ViewerInteractor_API RecordVideoInfo {
		int width = 0;
		int height = 0;
		int depth = 0;

		int count;
		int interval;  //msec

		typedef std::shared_ptr<RecordVideoInfo> Pointer;
	};

	struct ViewerInteractor_API RecordVideoDS {
		std::string tcfName;
	    std::map<Entity::ImageType, RecordVideoInfo::Pointer> list;

		typedef std::shared_ptr<RecordVideoDS> Pointer;
	};

	class ViewerInteractor_API IRecordVideoPanel {
	public:
		IRecordVideoPanel();
		virtual ~IRecordVideoPanel();

		auto GetRecordVideoDS() const->RecordVideoDS::Pointer;

		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}