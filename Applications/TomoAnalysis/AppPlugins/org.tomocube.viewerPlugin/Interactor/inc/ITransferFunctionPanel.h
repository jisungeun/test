#pragma once

//#include "TomoAnalysisInteractorExport.h"

#include <TransferFunction.h>
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	struct ViewerInteractor_API HTMetaDS {
		bool isExistHT = false;

		float intensityMin = -1.f;
		float intensityMax = -1.f;

		float gradientMin = -1.f;
		float gradientMax = -1.f;

		typedef std::shared_ptr<HTMetaDS> Pointer;
	};

	struct ViewerInteractor_API TransferFunctionDS {
		Entity::TFItemList list;

		typedef std::shared_ptr<TransferFunctionDS> Pointer;
	};

	class ViewerInteractor_API ITransferFunctionPanel {
	public:
		ITransferFunctionPanel();
		virtual ~ITransferFunctionPanel();

		auto GetHTMetaDS() const->HTMetaDS::Pointer;
		auto GetTransferFunctionDS() const->TransferFunctionDS::Pointer;

		virtual auto Update()->bool = 0;
		virtual auto Refresh()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}