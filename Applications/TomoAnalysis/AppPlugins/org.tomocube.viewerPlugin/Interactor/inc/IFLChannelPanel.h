#pragma once

#include <memory>
#include <string>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
    struct ViewerInteractor_API FLIntensity {
		bool isVisible = false;
		bool isValid = false;
		float min = -1.f;
		float max = -1.f;
		float opacity = -1.f;
    };

	struct ViewerInteractor_API FLMetaDS {
		bool isExistFL = false;
		FLIntensity flIntensity[3];
		std::string flName[3]{"CH1","CH2","CH3"};
		int flRange[3][2]{ {0,255},{0,255},{0,255} };
		int color[3][3]{ {0,0,255},{0,255,0},{255,0,0} };
		typedef std::shared_ptr<FLMetaDS> Pointer;
	};
	
	class ViewerInteractor_API IFLChannelPanel {
	public:
		IFLChannelPanel();
		virtual ~IFLChannelPanel();

		auto GetFLMetaDS() const->FLMetaDS::Pointer;

		virtual auto Update()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}