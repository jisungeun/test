#pragma once
#include <memory>
#include <string>

#include <Scene.h>
#include <ITCFInfoOutputPort.h>
#include <IFileReaderPort.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	class ViewerInteractor_API FileOpenController final {
	public:
		FileOpenController();
		FileOpenController(UseCase::ITCFInfoOutputPort* outPort, UseCase::IFileReaderPort* readerPort);
		FileOpenController(const FileOpenController& other) = delete;
		~FileOpenController();

		auto LoadImages(Entity::Scene::ID sceneID, const std::string& path,bool isCrop,int offsetX,int offsetY,int sizeX,int sizeY,int offsetXFL,int offsetYFL,int sizeXFL,int sizeYFL) const -> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}