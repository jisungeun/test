#pragma once

#include <memory>
#include <IFLChannelOutputPort.h>

#include "IImageViewer.h"

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
    class ViewerInteractor_API FLChannelPresenter : public UseCase::IFLChannelOutputPort {
    public:
        FLChannelPresenter();
        FLChannelPresenter(IImageViewer* viewer);
        ~FLChannelPresenter();

        void Update(const Entity::FLChannelInfo& info) override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}
