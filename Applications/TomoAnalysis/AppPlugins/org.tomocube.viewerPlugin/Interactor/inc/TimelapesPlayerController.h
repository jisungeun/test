#pragma once
#include <memory>
#include <string>

#include <Scene.h>
#include <ISceneOutputPort.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	class ViewerInteractor_API TimelapsePlayerController final {
	public:
		TimelapsePlayerController();
		TimelapsePlayerController(UseCase::ISceneOutputPort* outPort);
		TimelapsePlayerController(const TimelapsePlayerController& other) = delete;
		~TimelapsePlayerController();

		auto Play(Entity::Scene::ID sceneID, const double& frameTime) const -> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}