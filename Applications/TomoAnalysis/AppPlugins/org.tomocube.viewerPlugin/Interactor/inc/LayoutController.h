#pragma once
#include <memory>

#include <Scene.h>
#include <ISceneOutputPort.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	class ViewerInteractor_API LayoutController final {
	public:
		LayoutController();
		LayoutController(UseCase::ISceneOutputPort* outPort);
		LayoutController(const LayoutController& other) = delete;
		~LayoutController();

		auto SetLayoutType(Entity::Scene::ID sceneID, const Entity::LayoutType& type) const ->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}