#pragma once
#include <Scene.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	class ViewerInteractor_API AllocateSceneController final {
	public:
		AllocateSceneController();
		AllocateSceneController(const AllocateSceneController& other) = delete;
		~AllocateSceneController();
		
		auto RequestSceneAllocation() const -> Entity::Scene::ID;
	};
}