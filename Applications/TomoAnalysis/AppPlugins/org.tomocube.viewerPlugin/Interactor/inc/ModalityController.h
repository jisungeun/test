#pragma once
#include <memory>
#include <string>

#include <Scene.h>
#include <ISceneOutputPort.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	class ViewerInteractor_API ModalityController final {

	public:
		ModalityController();
		ModalityController(UseCase::ISceneOutputPort* outPort);
		ModalityController(const ModalityController& other) = delete;
		~ModalityController();

		auto SelectModality(const Entity::Scene::ID& sceneID, const Entity::Modality& modality) const -> bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}