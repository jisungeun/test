#pragma once
#include <memory>

#include <IVisualizationPresetOutputPort.h>
#include <IVisualizationDataWritePort.h>
#include <IVisualizationDataReadPort.h>

//#include "TomoAnalysisInteractorExport.h"
#include "ViewerInteractorExport.h"

namespace TomoAnalysis::Viewer::Interactor {
	class ViewerInteractor_API VisualizationPresetController final {
	public:
		VisualizationPresetController();
		VisualizationPresetController(UseCase::IVisualizationPresetOutputPort* outPort,
			                          UseCase::IVisualizationDataWritePort* writer,
			                          UseCase::IVisualizationDataReadPort* reader);
		~VisualizationPresetController();

	public:
		auto LoadList(const std::string& presetFolderPath) const ->bool;
		auto Load(Entity::Scene::ID sceneID, const std::string& path) const ->bool;
		auto Save(Entity::Scene::ID sceneID, const std::string& path, const bool& saveAs) const ->bool;
		auto Delete(Entity::Scene::ID sceneID, const std::string& path)const ->bool;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}