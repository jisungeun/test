#define LOGGER_TAG "[3D Visualizer]"
#include <TCLogger.h>

#include <iostream>
#include <QDirIterator>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QLabel>
#include <QStandardPaths>
#include <QObjectUserData>
#include <QProgressDialog>

#include <Inventor/nodes/SoSeparator.h>

#include <AllocateScene.h>
#include <MenuEvent.h>

#include <OivColorReader.h>
#include <OivHdf5Reader.h>
#include <OivLdmReader.h>
#include <OivLdmReaderFL.h>
#include <OivLdmReaderBF.h>
#include <OivXYReader.h>
#include <OivYZReader.h>
#include <OivXZReader.h>
#include <OivScaleBar.h>
#include <SoTransferFunction2D.h>


#include <Medical/nodes/TextBox.h>

//presenters
#include <ScenePresenter.h>
#include <TCFInfoPresenter.h>
#include <TransferFunctionPresenter.h>
#include <FLChannelPresenter.h>
#include <SliceNavigatePresenter.h>
#include <VisualizationPresetPresenter.h>

//controllers
#include <FileOpenController.h>
#include <ModalityController.h>
#include <FLChannelTuningController.h>
#include <TimelapesPlayerController.h>
#include <TransferFunctionController.h>
#include <PlaneNavigatorController.h>
#include <ScreenUtilController.h>
#include <VisualizationPresetController.h>
#include <LayoutController.h>

#include <TaTcfMetaReader.h>
#include <VisualizationDataReader.h>
#include <VisualizationDataWriter.h>
#include <OivColorReader.h>
#include <OivHdf5Reader.h>
#include <OivScaleBar.h>
#include <SoTransferFunction2D.h>

#include <TCDockWidget.h>
#include <AdaptiveTabWidget.h>
#include <OIVViewer.h>
#include <ModalityPanel.h>
#include <NavigatorPanel.h>
#include <TransferFunctionPanel.h>
#include <FLChannelPanel.h>
#include <VisualizationListPanel.h>
#include <TCFPlayerPanel.h>
#include <ScreenShotPanel.h>
#include <RecordVideoPanel.h>
#include <DisplayPanel.h>
#include <ViewingToolPanel.h>

#include <ScreenShotWidget.h>
#include <VideoRecorderWidget.h>
#include <OivRangeBar.h>

#include "ui_MainWindow.h"
#include "MainWindow.h"

#include "AppEvent.h"

namespace TomoAnalysis::Viewer::AppUI {
    using namespace TC::Framework;
    struct MainWindow::Impl {
        Ui::MainWindow* ui{ nullptr };

        SoSeparator* node;
        SoSeparator* node2d[3];

        Entity::Scene::ID sceneID;

        bool opened = false;

        QString cur_tcf;
        QString cur_prj;
        bool isCrop = false;        
        int cropOffset[2]{ 0,0 };
        int cropSize[2]{ 0,0 };
        int cropOffsetFL[2]{ 0,0 };
        int cropSizeFL[2]{ 0,0 };
        QMap<QString, IParameter::Pointer> share_param;

        Plugins::OIVViewer* viewWidget{ nullptr };
        Plugins::ModalityPanel* modalityWidget{ nullptr };
        Plugins::DisplayPanel* displayWidget{ nullptr };
        Plugins::ViewingToolPanel* viewingToolWidget{ nullptr };
        //Plugins::ScreenShotPanel* screenshotWidget{ nullptr };
        TC::ScreenShotWidget::Pointer screenshotComponent{ nullptr };
        Plugins::NavigatorPanel* navigatorWidget{ nullptr };
        Plugins::VisualizationListPanel* visualizationListWidget{ nullptr };
        Plugins::TransferFunctionPanel* transferfunctionWidget{ nullptr };
        Plugins::FLChannelPanel* fluorescenceWidget{ nullptr };
        Plugins::TCFPlayerPanel* playerWidget{ nullptr };
        //Plugins::RecordVideoPanel* movieMakerWidget{ nullptr };
        TC::VideoRecorderWidget::Pointer movieMakerComponent{ nullptr };

        QList<QDockWidget*> leftDockWidgets;
        QList<QDockWidget*> rightDockWidgets;

        QVariantMap appProperties;
        MainWindow* thisPointer{ nullptr };
        SbThread* curOpenThread{ nullptr };
        QString threadPath;
        QString openFailedMsg;
        QProgressDialog* openDialog{ nullptr };
        bool loadFinished{ false };
        bool setSliceFinished{ false };
        bool metaFinished{ false };
        int passedTimeStep{ 1 };
        bool changeTimeByForce{ false };

        QString passedPath;        
    };

    MainWindow::MainWindow(QWidget* parent)
        : IMainWindowTA("3D View", "3D View", parent)
        , d{ new Impl }
    {
        d->appProperties["Parent-App"] = "StandAlone";
        d->appProperties["AppKey"] = "3D View";
    }
    
    MainWindow::~MainWindow() {
        //2D Viewer deactivate Open Inventor classes
    }

    auto MainWindow::OnAccepted() -> void {
        
    }

    auto MainWindow::OnRejected() -> void {
        
    }

    auto MainWindow::GetFeatureName() const -> QString {
        return "org.tomocube.viewerPlugin";
    }


    auto MainWindow::ForceClosePopup() -> void {
        if (nullptr != d->transferfunctionWidget) {
            d->transferfunctionWidget->forceCloseDock();
        }
    }

    auto MainWindow::GetRunType() -> std::tuple<bool, bool> {
        auto singleRun = false;
        auto batchRun = false;
        return std::make_tuple(singleRun,batchRun);
    }    
    auto MainWindow::setOivNodes(SoSeparator* node, SoSeparator* node2d[3]) -> void {
        d->node = node;
        for (int i = 0; i < 3; i++) {
            d->node2d[i] = node2d[i];
        }
    }

    void MainWindow::resizeEvent(QResizeEvent* event) {
        Q_UNUSED(event)
    }

    auto MainWindow::Execute(const QVariantMap& params)->bool {
        if (params.isEmpty()) {
            return true;
        }
        if (false == params.contains("ExecutionType")) {
            return false;
        }
        if(params["ExecutionType"].toString() == "UpdateTime") {
            if(params["timeSender"].toString() == "3D Viewer") {
                return true;
            }
            d->passedPath = params["tcfName"].toString();
            d->passedTimeStep = params["timeStep"].toInt();
            return true;
        }
        if (params["ExecutionType"].toString() == "OpenTCF") {
            if (true == d->opened) {
                this->Reset();
            }
            d->cur_tcf = params["TCFPath"].toString();
            d->cur_prj = params["Playground"].toString();

            d->isCrop = false;
            d->cropOffset[0] = d->cropOffset[1] = 0;
            d->cropSize[0] = d->cropSize[1] = 0;
            d->cropOffsetFL[0] = d->cropOffsetFL[1] = 0;
            d->cropSizeFL[0] = d->cropSizeFL[1] = 0;

            d->passedTimeStep = params["timeStep"].toInt();
            if(params.contains("htROI")) {
                auto roiMap = params["htROI"].toMap();
                if(roiMap["xSize"].toInt() >0) {
                    d->cropOffset[0] = roiMap["xOffset"].toInt();
                    d->cropOffset[1] = roiMap["yOffset"].toInt();
                    d->cropSize[0] = roiMap["xSize"].toInt();
                    d->cropSize[1] = roiMap["ySize"].toInt();
                    d->viewWidget->SetCropInformation(d->cropOffset[0], d->cropOffset[1], d->cropSize[0], d->cropSize[1]);
                    d->isCrop = true;
                }                
            }
            if(params.contains("flROI")) {
                auto roiMap = params["flROI"].toMap();
                if(roiMap["xSize"].toInt()>0) {
                    d->cropOffsetFL[0] = roiMap["xOffset"].toInt();
                    d->cropOffsetFL[1] = roiMap["yOffset"].toInt();
                    d->cropSizeFL[0] = roiMap["xSize"].toInt();
                    d->cropSizeFL[1] = roiMap["ySize"].toInt();
                    d->viewWidget->SetFLCropInformation(d->cropOffsetFL[0], d->cropOffsetFL[1], d->cropSizeFL[0], d->cropSizeFL[1]);
                    d->isCrop = true;
                }                
            }           
            //OpenFile(d->cur_tcf);
            d->openDialog = new QProgressDialog(nullptr);
            d->openDialog->setWindowTitle("File Open");
            d->openDialog->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
            d->openDialog->setMinimumWidth(400);
            d->openDialog->setCancelButton(nullptr);
            d->openDialog->setRange(0, 3);
            d->openDialog->setValue(0);
            d->openDialog->setLabelText(QString("Initializing 3D Visualization ..."));
            connect(d->openDialog, SIGNAL(canceled()), this, SLOT(onOpenCanceled()));

            d->threadPath = d->cur_tcf;
            d->openFailedMsg = QString();
            d->loadFinished = false;
            d->setSliceFinished = false;
            d->metaFinished = false;
            d->curOpenThread = SbThread::create(OpenFileThread3d, (void*)d.get());
            d->openDialog->open();
        }
        return true;
    }        

    auto MainWindow::GetParameter(QString name) -> IParameter::Pointer {
        if(d->share_param.contains(name)) {
            return d->share_param[name];
        }
        return nullptr;
    }

    auto MainWindow::SetParameter(QString name, IParameter::Pointer param) -> void {
        d->share_param[name] = std::make_shared<IParameter>(param->Clone());
    }


    auto MainWindow::GetCurTCF() const -> QString {
        return d->cur_tcf;
    }
        
    auto MainWindow::Init() -> bool {
        UseCase::AllocateScene usecase;
        d->sceneID = usecase.Request();

        this->InitUI();

        return true;
    }
    auto MainWindow::InitUI() -> void{
        if (false == d->modalityWidget->Init()) {
            QMessageBox::warning(nullptr, "Error", "Failed to initialize modality panel.");
            return;
        }

        if (false == d->displayWidget->Init()) {
            QMessageBox::warning(nullptr, "Error", "Failed to initialize display panel.");
            return;
        }

        if (false == d->viewingToolWidget->Init()) {
            QMessageBox::warning(nullptr, "Error", "Failed to initialize viewing tool panel.");
            return;
        }

        if (false == d->visualizationListWidget->Init()) {
            QMessageBox::warning(nullptr, "Error", "Failed to initialize HT visualization panel.");
            return;
        }
        
        if (false == d->fluorescenceWidget->Init()) {
            QMessageBox::warning(nullptr, "Error", "Failed to initialize FL visualization panel.");
            return;
        }

        if (false == d->playerWidget->Init()) {
            QMessageBox::warning(nullptr, "Error", "Failed to initialize player panel.");
            return;
        }

        if(false == d->movieMakerComponent->Init()) {
            QMessageBox::warning(nullptr, "Error", "Failed to initialize record video panel.");
            return;
        }

        if (false == d->navigatorWidget->Init()) {
            QMessageBox::warning(nullptr, "Error", "Failed to initialize navigator panel.");
            return;
        }

        if (false == d->viewWidget->Init()) {
            QMessageBox::warning(nullptr, "Error", "Failed to initialize viewer.");
            return;
        }
        //connect screen shot after generation of render windows
        d->viewWidget->ConnectScreenShotWidget(d->screenshotComponent);
        d->viewWidget->ConnectRecorderWidget(d->movieMakerComponent);

        if (false == d->transferfunctionWidget->Init()) {
            QMessageBox::warning(nullptr, "Error", "Failed to initialize HT visualization panel.");
            return;
        }

        d->viewWidget->SetSceneID(d->sceneID);
        d->viewWidget->SetTFWidget(d->transferfunctionWidget->GetCanvasWidget());

        // event
        connect(d->modalityWidget, SIGNAL(activatedModalityChanged(Entity::Modality)), this, SLOT(onActivatedModalityChanged(Entity::Modality)));
        connect(d->modalityWidget, SIGNAL(activatedModalityChanged(Entity::Modality)), d->fluorescenceWidget, SLOT(onActivatedModalityChanged(Entity::Modality)));
        connect(d->modalityWidget, SIGNAL(activatedModalityChanged(Entity::Modality)), d->playerWidget, SLOT(onTimelapseChanged(Entity::Modality)));
        connect(d->modalityWidget, SIGNAL(activatedModalityChanged(Entity::Modality)), d->navigatorWidget, SLOT(onCurrentModalityChanged(Entity::Modality)));

        connect(d->viewWidget, SIGNAL(sliceIndexChanged(int, int)), this, SLOT(onSliceIndexChanged(int, int)));
        connect(d->navigatorWidget, SIGNAL(positionChanged(int, int, int)), this, SLOT(onPlanePositionChanged(int, int, int)));
        connect(d->navigatorWidget, SIGNAL(phyPositionChanged(float, float, float)), this, SLOT(onPhyPlanePositionChanged(float, float, float)));
        //connect(d->movieMakerComponent.get(), SIGNAL(recMoveSlice(int, int)),d->navigatorWidget, SLOT(onPositionChanged(int, int)));
        connect(d->movieMakerComponent.get(), SIGNAL(recMoveSlice(int, int)), this, SLOT(onSliceIndexChanged(int,int)));

        connect(d->viewWidget, SIGNAL(gradientRangeLoaded(double, double)), this, SLOT(onGradientRangeLoaded(double, double)));
        connect(d->viewWidget, SIGNAL(transferFunctionChanged(Entity::TFItemList)), this, SLOT(onTransferFunctionChanged(Entity::TFItemList)));
        connect(d->viewWidget, SIGNAL(currentTransferFunctionChanged(int)), d->transferfunctionWidget, SLOT(onCurrentTransferFunctionChanged(int)));
        connect(d->transferfunctionWidget, SIGNAL(itemChanged(int, Entity::TFItem::Pointer)), this, SLOT(onTransferFunctionItemChanged(int, Entity::TFItem::Pointer)));
        connect(d->transferfunctionWidget, SIGNAL(histogramChanged(int)), d->viewWidget, SLOT(onTransferFunctionHistogramChanged(int)));
        connect(d->transferfunctionWidget, SIGNAL(overlayChanged(bool)), this, SLOT(onTransferFunctionOverlayChanged(bool)));
        connect(d->transferfunctionWidget, SIGNAL(selectItem(int)), d->viewWidget, SLOT(onSelectTransferFunctionItem(int)));

        connect(d->transferfunctionWidget, SIGNAL(clearItems()), this, SLOT(onTransferFunctionClear()));

        connect(d->fluorescenceWidget, SIGNAL(channelVisibleChanged(Entity::Channel, bool)), this, SLOT(onChannelVisibleChanged(Entity::Channel, bool)));
        connect(d->fluorescenceWidget, SIGNAL(channelRangeChanged(Entity::Channel, int, int)), this, SLOT(onChannelRangeChanged(Entity::Channel, int, int)));
        connect(d->fluorescenceWidget, SIGNAL(channelColorChanged(Entity::Channel, QColor)), this, SLOT(onChannelColorChanged(Entity::Channel, QColor)));
        connect(d->fluorescenceWidget, SIGNAL(channelOpacityChanged(Entity::Channel, int)), this, SLOT(onChannelOpacityChanged(Entity::Channel, int)));
        connect(d->fluorescenceWidget, SIGNAL(channelGammaChanged(Entity::Channel, bool, double)), this, SLOT(onChannelGammaChanged(Entity::Channel, bool, double)));
        connect(d->fluorescenceWidget, SIGNAL(vizControlFocused(bool)), d->viewWidget, SLOT(onRowRes(bool)));
        

        connect(d->playerWidget, SIGNAL(waitTimelapseIndexChanged(double)), this, SLOT(onTimelapseIndexPlayed(double)));
        connect(d->movieMakerComponent.get(), SIGNAL(recTimeStep(double)), this, SLOT(onTimelapseIndexChanged(double)));
        //connect(d->viewWidget, SIGNAL(timeStepPlayed(double)), d->playerWidget, SLOT(onTimeStepPlayed(double)),Qt::QueuedConnection);
        connect(d->playerWidget, SIGNAL(timelapseIndexChanged(double)), this, SLOT(onTimelapseIndexChanged(double)));
        connect(d->playerWidget, SIGNAL(timelapseInfoChanged()), this, SLOT(onTimelapseInfoChanged()));
               
        connect(d->visualizationListWidget, SIGNAL(deletePreset(QString)), this, SLOT(onDeletePreset(QString)));
        connect(d->visualizationListWidget, SIGNAL(loadPreset(QString)), this, SLOT(onLoadPreset(QString)));
        connect(d->visualizationListWidget, SIGNAL(savePreset(QString, bool)), this, SLOT(onSavePreset(QString, bool)));

        connect(d->displayWidget, SIGNAL(displayTypeChanged(Entity::DisplayType)), this, SLOT(onDisplayChanged(Entity::DisplayType)));
        connect(d->displayWidget, SIGNAL(displayTypeChanged(Entity::DisplayType)), d->modalityWidget, SLOT(onDisplayTypeChanged(Entity::DisplayType)));

        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::layoutChanged,
            [=](Entity::LayoutType type) { ChangeLayout(type); });
        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::resetLayoutTriggered,
            [=]() { ResetLayout(true); });
        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::saveLayoutTriggered,
            [=](const QString &path) { SaveLayout(path); });
        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::loadLayoutTriggered,
            [=](const QString& path) { LoadLayout(path); });
        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::boundaryBoxVisibilityChanged,
            [=](bool checked) { SetVisibleBoundaryBox(checked); });
        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::axisGridVisibilityChanged,
            [=](bool checked) { SetVisibleAxisGrid(checked); });
        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::orientationMarkerVisibilityChanged,
            [=](bool checked) {SetVisibleOrientationMarker(checked); });
        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::timestampVisibilityChanged,
            [=](bool checked) {SetVisibleTimeStamp(checked); });
        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::timestampColorChanged,
            [=](QColor col) {SetTimeStampColor(col); });
        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::timestampSizeChanged,
            [=](int size) {SetTimeStampSize(size); });

        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::sliceResolutionChanged, [=](int res) {SetSliceResolution(res); });
        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::volumeResolutionChanged, [=](int res) {SetVolumeResolution(res); });
        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::axisGridFontSize, [=](int size) {SetAxisGridFontSize(size); });
        connect(d->viewingToolWidget, &Plugins::ViewingToolPanel::axisGridColor, [=](int axis, QColor col) {SetAxisGridColor(axis, col); });

        connect(this, SIGNAL(sigOpenThreadFinished3d()), this, SLOT(onOpenFileFinished()));
        connect(this, SIGNAL(sigLoadImage3d()), this, SLOT(onLoadImage()));
        connect(this, SIGNAL(sigSetSlice3d()), this, SLOT(onSetSlice()));
        connect(this, SIGNAL(sigLoadPreset3d()), this, SLOT(onLoadDefaultPreset()));
    }
    void MainWindow::onDisplayChanged(Entity::DisplayType type) const {  
        if(type == Entity::DisplayType::DISPLAY_2D) {
            d->viewWidget->SetDisplayType(true);
            d->viewingToolWidget->force2D();
            //d->screenshotWidget->force2D();
            d->screenshotComponent->force2D();
            d->navigatorWidget->setEnabled(false);
            d->visualizationListWidget->setEnabled(false);
            //d->movieMakerWidget->force2D();
            d->movieMakerComponent->Force2D();
        }else if(type == Entity::DisplayType::DISPLAY_3D) {
            d->viewWidget->SetDisplayType(false);
            d->viewingToolWidget->restore3D();
            //d->screenshotWidget->restore3D();
            d->screenshotComponent->restore3D();            
            d->navigatorWidget->setEnabled(true);
            d->visualizationListWidget->setEnabled(true);
            //d->movieMakerWidget->restore3D();
            d->movieMakerComponent->Restore3D();
        }        
    }

    void MainWindow::onActivatedModalityChanged(Entity::Modality modality) {        
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget) };
        Interactor::ModalityController modalityController(presenter.get());
        const auto result = modalityController.SelectModality(d->sceneID, modality);

        if (false == result) {
            QMessageBox::warning(nullptr, "Select modality", "Failed to select modality.");
            return;
        }
        auto nds = d->navigatorWidget->GetNavigatorDS();
        auto sds = d->viewWidget->GetSceneDS();
        //set slice range to recorder
        Interactor::NavigatorInfo::Pointer info{nullptr};

        auto withHT = (modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
            (modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP;

        if ((modality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume || (modality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP) {
            if (false == withHT) {
                const auto opa1 = sds->flChannelList[Entity::Channel::CH1]->opacity;
                const auto opa2 = sds->flChannelList[Entity::Channel::CH2]->opacity;
                const auto opa3 = sds->flChannelList[Entity::Channel::CH3]->opacity;
                onChannelOpacityChanged(Entity::Channel::All, 100);
                sds->flChannelList[Entity::Channel::CH1]->opacity = opa1;
                sds->flChannelList[Entity::Channel::CH2]->opacity = opa2;
                sds->flChannelList[Entity::Channel::CH3]->opacity = opa3;
            }
            else {
                onChannelOpacityChanged(Entity::Channel::CH1, sds->flChannelList[Entity::Channel::CH1]->opacity);
                onChannelOpacityChanged(Entity::Channel::CH2, sds->flChannelList[Entity::Channel::CH2]->opacity);
                onChannelOpacityChanged(Entity::Channel::CH3, sds->flChannelList[Entity::Channel::CH3]->opacity);
            }
        }

        if (((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
            (modality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume)) {
            // volume
            if ((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
                info = nds->list[Entity::HT3D];
            }
            else {
                info = nds->list[Entity::FL3D];
            }

        }
        else if (((modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP ||
            (modality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP)) {
            // mip
            if ((modality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
                info = nds->list[Entity::HT2DMIP];
            }
            else {
                info = nds->list[Entity::FL2DMIP];
            }
        }
        if (nullptr != info) {
            d->movieMakerComponent->SetSliceMax(info->depth, info->width, info->height);
        }
    }

    void MainWindow::onSliceIndexChanged(int viewIndex, int sliceIndex) const {
        //SoDB::writelock();
        std::shared_ptr<Interactor::SliceNavigatePresenter> presenter{ new Interactor::SliceNavigatePresenter(d->viewWidget, d->navigatorWidget) };
        Interactor::PlaneNavigatorController planeNavigatorController(presenter.get());
        planeNavigatorController.MoveSlice(d->sceneID, viewIndex, sliceIndex);
        //SoDB::writeunlock();
    }

    void MainWindow::onPlanePositionChanged(int x, int y, int z) {
        std::shared_ptr<Interactor::SliceNavigatePresenter> presenter{ new Interactor::SliceNavigatePresenter(d->viewWidget, d->navigatorWidget) };
        Interactor::PlaneNavigatorController planeNavigatorController(presenter.get());
        planeNavigatorController.MovePlane(d->sceneID, x, y, z);
    }

    void MainWindow::onPhyPlanePositionChanged(float x, float y, float z) {
        std::shared_ptr<Interactor::SliceNavigatePresenter> presenter{ new Interactor::SliceNavigatePresenter(d->viewWidget, d->navigatorWidget) };
        Interactor::PlaneNavigatorController planeNavigatorController(presenter.get());
        planeNavigatorController.MovePhyPlane(d->sceneID, x, y, z);
    }

    void MainWindow::onGradientRangeLoaded(double min, double max) const {
        d->transferfunctionWidget->SetGradientRange(min, max);
    }

    void MainWindow::onTransferFunctionChanged(Entity::TFItemList list) const {
        d->transferfunctionWidget->SetTransferFunctionItems(list);

        std::shared_ptr<Interactor::TransferFunctionPresenter> presenter{ new Interactor::TransferFunctionPresenter(d->viewWidget) };
        Interactor::TransferFunctionController transferFunctionController(presenter.get());
        transferFunctionController.SetTransFunction(d->sceneID, list);                
    }

    void MainWindow::onTransferFunctionItemChanged(int index, Entity::TFItem::Pointer item) const {
        std::shared_ptr<Interactor::TransferFunctionPresenter> presenter{ new Interactor::TransferFunctionPresenter(d->viewWidget) };
        Interactor::TransferFunctionController transferFunctionController(presenter.get());
        transferFunctionController.ModifyTransFunctionItem(d->sceneID, index,
            item->intensityMin, item->intensityMax,
            item->gradientMin, item->gradientMax,
            item->transparency, item->color, item->visible);
    }

    void MainWindow::onTransferFunctionOverlayChanged(bool isOverlay) const {
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget) };
        Interactor::ScreenUtilController screenUtilController(presenter.get());
        screenUtilController.SetEnableTransferFunctionOverlay(d->sceneID, isOverlay);
    }

    void MainWindow::onTransferFunctionClear() {
        std::shared_ptr<Interactor::TransferFunctionPresenter> presenter{ new Interactor::TransferFunctionPresenter(d->viewWidget) };
        Interactor::TransferFunctionController transferFunctionController(presenter.get());
        transferFunctionController.ClearTransFunctionItem(d->sceneID);
    }

    void MainWindow::onChannelVisibleChanged(Entity::Channel channel, bool visible) const {
        std::shared_ptr<Interactor::FLChannelPresenter> presenter{ new Interactor::FLChannelPresenter(d->viewWidget) };
        Interactor::FLChannelTuningController channelTuningController(presenter.get());
        channelTuningController.SetVisible(d->sceneID, channel, visible);
    }

    void MainWindow::onChannelRangeChanged(Entity::Channel channel, int min, int max) const {
        std::shared_ptr<Interactor::FLChannelPresenter> presenter{ new Interactor::FLChannelPresenter(d->viewWidget) };
        Interactor::FLChannelTuningController channelTuningController(presenter.get());
        channelTuningController.SetRange(d->sceneID, channel, min, max);
    }

    void MainWindow::onChannelColorChanged(Entity::Channel channel, QColor color) const {
        std::shared_ptr<Interactor::FLChannelPresenter> presenter{ new Interactor::FLChannelPresenter(d->viewWidget) };
        Interactor::FLChannelTuningController channelTuningController(presenter.get());
        channelTuningController.SetColor(d->sceneID,channel, color.red(),color.green(),color.blue());
    }

    void MainWindow::onChannelOpacityChanged(Entity::Channel channel, int opacity) const {
        std::shared_ptr<Interactor::FLChannelPresenter> presenter{ new Interactor::FLChannelPresenter(d->viewWidget) };
        Interactor::FLChannelTuningController channelTuningController(presenter.get());
        channelTuningController.SetOpacity(d->sceneID, channel, opacity);
    }

    void MainWindow::onChannelGammaChanged(Entity::Channel channel, bool enable, double gamma) const {
        std::shared_ptr<Interactor::FLChannelPresenter> presenter{ new Interactor::FLChannelPresenter(d->viewWidget) };
        Interactor::FLChannelTuningController channelTuningController(presenter.get());
        channelTuningController.SetGamma(d->sceneID, channel, static_cast<int>(gamma * 100.0), enable);
    }

    void MainWindow::onTimelapseInfoChanged() const {
        auto tps = d->playerWidget->GetTimePoints();
        d->viewWidget->SetTimePoints(tps);
        d->movieMakerComponent->SetTimePoints(tps);
    }

    void MainWindow::onTimelapseIndexPlayed(double time) {
        //SoDB::writelock();
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget) };
        Interactor::TimelapsePlayerController timelapsePlayerController(presenter.get());
        timelapsePlayerController.Play(d->sceneID, time);
        //SoDB::writeunlock();
        d->playerWidget->onTimeStepPlayed(time);

        if (d->changeTimeByForce) {
            d->changeTimeByForce = false;
        }
        else {
            const auto curTime = d->playerWidget->GetCurTimeStep();
            AppEvent appEvent(AppTypeEnum::APP_UPDATE);
            appEvent.setFullName("org.tomocube.viewerPlugin");
            appEvent.addParameter("timeStep", curTime);
            appEvent.addParameter("tcfName", d->threadPath);
            appEvent.addParameter("timeSender", "3D Viewer");
            appEvent.addParameter("ExecutionType", "UpdateTime");
            publishSignal(appEvent, "3D View");
        }
    }

    void MainWindow::onTimelapseIndexChanged(double time) {
        //SoDB::writelock();
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget) };
        Interactor::TimelapsePlayerController timelapsePlayerController(presenter.get());
        timelapsePlayerController.Play(d->sceneID, time);
        //SoDB::writeunlock();

        if (d->changeTimeByForce) {
            d->changeTimeByForce = false;
        }
        else {
            const auto curTime = d->playerWidget->GetCurTimeStep();
            AppEvent appEvent(AppTypeEnum::APP_UPDATE);
            appEvent.setFullName("org.tomocube.viewer2dPlugin");
            appEvent.addParameter("timeStep", curTime);
            appEvent.addParameter("tcfName", d->threadPath);
            appEvent.addParameter("timeSender", "3D Viewer");
            appEvent.addParameter("ExecutionType", "UpdateTime");
            publishSignal(appEvent, "3D View");
        }
    }

    void MainWindow::onDeletePreset(QString path)const {
        std::shared_ptr<Interactor::VisualizationPresetPresenter> visualizationPresetPresenter{
            new Interactor::VisualizationPresetPresenter(d->viewWidget, d->transferfunctionWidget,
                                                         d->fluorescenceWidget, d->visualizationListWidget,nullptr)
        };
        Interactor::VisualizationPresetController visualizationPresetController(visualizationPresetPresenter.get(), nullptr, nullptr);
        if (false == visualizationPresetController.Delete(d->sceneID, path.toStdString())) {
            QMessageBox::warning(nullptr, "Delete preset", "Failed to delete  preset.");
            return;
        }

        // load visualization preset list
        const auto defaultPath = QString("%1/preset").arg(QApplication::applicationDirPath());
        const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/preset", defaultPath).toString();

        if (false == visualizationPresetController.LoadList(prev.toStdString())) {
            QMessageBox::warning(nullptr, "Load preset list", "Failed to load visualization preset list.");
            return;
        }
    }
    void MainWindow::onLoadPreset(QString path) const {        
        std::shared_ptr<Plugins::VisualizationDataIO::VisualizationDataWriter> writer{ new Plugins::VisualizationDataIO::VisualizationDataWriter };
        std::shared_ptr<Plugins::VisualizationDataIO::VisualizationDataReader> reader{ new Plugins::VisualizationDataIO::VisualizationDataReader };

        std::shared_ptr<Interactor::VisualizationPresetPresenter> visualizationPresetPresenter{
            new Interactor::VisualizationPresetPresenter(d->viewWidget, d->transferfunctionWidget,
                                                         d->fluorescenceWidget, d->visualizationListWidget,d->modalityWidget)
        };

        Interactor::VisualizationPresetController visualizationPresetController(visualizationPresetPresenter.get(), writer.get(), reader.get());
        if (false == visualizationPresetController.Load(d->sceneID, path.toStdString())) {
            QMessageBox::warning(nullptr, "Load preset", "Failed to save visualization preset.");
            return;
        }

        // load visualization preset list
        const auto defaultPath = QString("%1/preset").arg(QApplication::applicationDirPath());
        const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/preset", defaultPath).toString();

        if (false == visualizationPresetController.LoadList(prev.toStdString())) {
            QMessageBox::warning(nullptr, "Load preset list", "Failed to load visualization preset list.");
            return;
        }
    }

    void MainWindow::onSavePreset(QString path, bool saveAs) const {
        std::shared_ptr<Plugins::VisualizationDataIO::VisualizationDataWriter> writer{ new Plugins::VisualizationDataIO::VisualizationDataWriter };
        std::shared_ptr<Plugins::VisualizationDataIO::VisualizationDataReader> reader{ new Plugins::VisualizationDataIO::VisualizationDataReader };

        std::shared_ptr<Interactor::VisualizationPresetPresenter> visualizationPresetPresenter{
            new Interactor::VisualizationPresetPresenter(d->viewWidget, d->transferfunctionWidget,
                                                         d->fluorescenceWidget, d->visualizationListWidget,nullptr)
        };

        Interactor::VisualizationPresetController visualizationPresetController(visualizationPresetPresenter.get(), writer.get(), reader.get());
        if (false == visualizationPresetController.Save(d->sceneID, path.toStdString(), saveAs)) {
            if(!saveAs) {
                QMessageBox::warning(nullptr, "Update preset", "Select preset first!");
            }else {
                QMessageBox::warning(nullptr, "Save preset", "Failed to save visualization preset.");
            }            
            return;
        }

        // load visualization preset list
        const auto defaultPath = QString("%1/preset").arg(QApplication::applicationDirPath());
        const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/preset", defaultPath).toString();

        if (false == visualizationPresetController.LoadList(prev.toStdString())) {
            QMessageBox::warning(nullptr, "Load preset list", "Failed to load visualization preset list.");
            return;
        }
    }

    auto MainWindow::Reset()->void {
        if (false == d->modalityWidget->Reset()) {
            QMessageBox::warning(nullptr, "Error", "Failed to reset modality panel.");
            return;
        }

        if (false == d->displayWidget->Reset()) {
            QMessageBox::warning(nullptr, "Error", "Failed to reset display panel.");
            return;
        }

        if (false == d->viewingToolWidget->Reset()) {
            QMessageBox::warning(nullptr, "Error", "Failed to reset viewing tool panel.");
            return;
        }

        if (false == d->visualizationListWidget->Reset()) {
            QMessageBox::warning(nullptr, "Error", "Failed to reset visualization list panel.");
            return;
        }

        if (false == d->transferfunctionWidget->Reset()) {
            QMessageBox::warning(nullptr, "Error", "Failed to reset HT visualization panel.");
            return;
        }

        if (false == d->fluorescenceWidget->Reset()) {
            QMessageBox::warning(nullptr, "Error", "Failed to reset FL visualization panel.");
            return;
        }

        if (false == d->playerWidget->Reset()) {
            QMessageBox::warning(nullptr, "Error", "Failed to reset player panel.");
            return;
        }

        if(false == d->movieMakerComponent->Reset()) {
            QMessageBox::warning(nullptr, "Error", "Failed to reset movie maker panel");
            return;
        }
        /*
        if(false == d->movieMakerWidget->Reset()) {
            QMessageBox::warning(nullptr, "Error", "Failed to reset movie maker panel");
            return;
        }*/

        if(false == d->navigatorWidget->Reset()) {
            QMessageBox::warning(nullptr, "Error", "Failed to reset navigator panel.");
            return;
        }

        /*if (false == d->screenshotWidget->Reset()) {
            QMessageBox::warning(nullptr, "Error", "Failed to reset screenshot panel.");
            return;
        }*/

        if (false == d->viewWidget->Reset()) {
            QMessageBox::warning(nullptr, "Error", "Failed to reset viewer.");
            return;
        }

        //this->InitUI();
    }

    auto MainWindow::RequsetRender() -> void {
        if (d->viewWidget)
            d->viewWidget->RequestRender();
    }

    void MainWindow::onLoadImage() {
        if(true == d->opened) {
            this->Reset();
        }
        d->viewWidget->SetCurrentProjectPath(d->cur_prj);

        d->screenshotComponent->SetTcfPath(d->threadPath);

        std::shared_ptr<Plugins::FileReader::TcfMetaReader> tcfFileReader{ new Plugins::FileReader::TcfMetaReader };
        std::shared_ptr<Interactor::TCFInfoPresenter> presenter{
            new Interactor::TCFInfoPresenter(d->viewWidget,
                                             d->modalityWidget, d->displayWidget,
                                             d->transferfunctionWidget,
                                             d->fluorescenceWidget, d->playerWidget,
                                             /*d->screenshotWidget,*/ /* d->movieMakerWidget,*/
                                             d->navigatorWidget, d->visualizationListWidget,d->viewingToolWidget)
        };

        Interactor::FileOpenController fileOpenController(presenter.get(), tcfFileReader.get());

        if (false == fileOpenController.LoadImages(d->sceneID, d->threadPath.toLocal8Bit().toStdString(), d->isCrop, d->cropOffset[0], d->cropOffset[1], d->cropSize[0], d->cropSize[1], d->cropOffsetFL[0], d->cropOffsetFL[1], d->cropSizeFL[0], d->cropSizeFL[1])) {
            d->openFailedMsg = "Failed to open TCF file.";
            return;
        }

        d->opened = true;
        d->openDialog->setValue(1);
        d->loadFinished = true;
        
        d->playerWidget->ForceTimeIndex(d->passedTimeStep);
    }

    void MainWindow::onSetSlice() {
        d->fluorescenceWidget->SetTcfPath(d->threadPath);
        //Update MovieMaker Component Meta information after load finished        
        d->movieMakerComponent->SetTcfName(d->threadPath);

        //clear previous transfer function if exist
        std::shared_ptr<Interactor::TransferFunctionPresenter> tfpresenter{ new Interactor::TransferFunctionPresenter(d->viewWidget) };
        Interactor::TransferFunctionController transferFunctionController(tfpresenter.get());
        transferFunctionController.ClearTransFunctionItem(d->sceneID);

        //set slice index to center
        std::shared_ptr<Interactor::SliceNavigatePresenter> slice_presenter{ new Interactor::SliceNavigatePresenter(d->viewWidget, d->navigatorWidget) };
        Interactor::PlaneNavigatorController planeNavigatorController(slice_presenter.get());
        if (false == planeNavigatorController.InitSlice(d->sceneID)) {
            d->openFailedMsg = "Failed to allocate initial slice positions.";
            return;
        }

        d->openDialog->setValue(2);
        d->setSliceFinished = true;                
    }

    void MainWindow::onLoadDefaultPreset() {
        // load visualization preset
        std::shared_ptr<Plugins::VisualizationDataIO::VisualizationDataWriter> writer{ new Plugins::VisualizationDataIO::VisualizationDataWriter };
        std::shared_ptr<Plugins::VisualizationDataIO::VisualizationDataReader> reader{ new Plugins::VisualizationDataIO::VisualizationDataReader };

        std::shared_ptr<Interactor::VisualizationPresetPresenter> visualizationPresetPresenter{
            new Interactor::VisualizationPresetPresenter(d->viewWidget,  d->transferfunctionWidget,
                                                         d->fluorescenceWidget, d->visualizationListWidget,d->modalityWidget)
        };

        Interactor::VisualizationPresetController visualizationPresetController(visualizationPresetPresenter.get(), writer.get(), reader.get());

        const auto defaultPath = QString("%1/preset").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
        auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/preset", defaultPath).toString();
        if (false == QDir().exists(prev)) {
            if (false == QDir().mkpath(prev)) {                
                prev = defaultPath;
            }
        }

        if (false == visualizationPresetController.LoadList(prev.toStdString())) {            
            d->openFailedMsg = "Failed to load visualization preset list.";
            return;
        }

        // load tcf default preset
        QFileInfo fileInfo(d->threadPath);
        const auto fileName = fileInfo.fileName();
        const auto defaultPreset = QString("%1/%2.xml").arg(fileInfo.dir().absolutePath()).arg(fileName.chopped(4));
        if (QFile::exists(defaultPreset)) {
            if (false == visualizationPresetController.Load(d->sceneID, defaultPreset.toStdString())) {
                d->openFailedMsg = "Failed to load visualization preset.";
                return;
            }
        }
        d->openDialog->setValue(3);
        d->metaFinished = true;
    }

    void MainWindow::onOpenFileFinished() {        
        d->openDialog->close();
    }

    void MainWindow::onOpenCanceled() {
        SbThread::destroy(d->curOpenThread);
        if (false == d->openFailedMsg.isEmpty()) {
            QMessageBox::warning(nullptr, "3D View Initialization Failed", d->openFailedMsg);
        }
    }

    void* MainWindow::OpenFileThread3d(void* userData) {
        auto dd = static_cast<Impl*>(userData);

        emit dd->thisPointer->sigLoadImage3d();
        while (false == dd->loadFinished && dd->openFailedMsg.isEmpty()) {
            SbThread::sleep_ms(10);
        }

        if (false == dd->openFailedMsg.isEmpty()) {
            emit dd->thisPointer->sigOpenThreadFinished3d();
            return nullptr;
        }

        emit dd->thisPointer->sigSetSlice3d();
        while (false == dd->setSliceFinished && dd->openFailedMsg.isEmpty()) {
            SbThread::sleep_ms(10);
        }

        if (false == dd->openFailedMsg.isEmpty()) {
            emit dd->thisPointer->sigOpenThreadFinished3d();
            return nullptr;
        }

        emit dd->thisPointer->sigLoadPreset3d();
        while (false == dd->metaFinished) {
            SbThread::sleep_ms(10);
        }

        emit dd->thisPointer->sigOpenThreadFinished3d();
        return nullptr;
    }

    auto MainWindow::OpenFile(const QString& path) -> bool {        
        d->viewWidget->SetCurrentProjectPath(d->cur_prj);
        d->screenshotComponent->SetTcfPath(path);

        std::shared_ptr<Plugins::FileReader::TcfMetaReader> tcfFileReader{ new Plugins::FileReader::TcfMetaReader };
        std::shared_ptr<Interactor::TCFInfoPresenter> presenter{
            new Interactor::TCFInfoPresenter(d->viewWidget,
                                             d->modalityWidget, d->displayWidget,
                                             d->transferfunctionWidget,
                                             d->fluorescenceWidget, d->playerWidget,
                                             /*d->screenshotWidget,*/ /* d->movieMakerWidget,*/
                                             d->navigatorWidget, d->visualizationListWidget,d->viewingToolWidget)
        };
                
        Interactor::FileOpenController fileOpenController(presenter.get(), tcfFileReader.get());
                
        if (false == fileOpenController.LoadImages(d->sceneID, path.toLocal8Bit().toStdString(),d->isCrop,d->cropOffset[0],d->cropOffset[1],d->cropSize[0],d->cropSize[1],d->cropOffsetFL[0],d->cropOffsetFL[1],d->cropSizeFL[0],d->cropSizeFL[1])) {
            QMessageBox::warning(nullptr, "Open TCF file", "Failed to open TCF file.");
            return false;
        }

        d->opened = true;

        d->fluorescenceWidget->SetTcfPath(path);
        //Update MovieMaker Component Meta information after load finished        
        d->movieMakerComponent->SetTcfName(path);

        //clear previous transfer function if exist
        std::shared_ptr<Interactor::TransferFunctionPresenter> tfpresenter{ new Interactor::TransferFunctionPresenter(d->viewWidget) };
        Interactor::TransferFunctionController transferFunctionController(tfpresenter.get());
        transferFunctionController.ClearTransFunctionItem(d->sceneID);

        //set slice index to center
        std::shared_ptr<Interactor::SliceNavigatePresenter> slice_presenter{ new Interactor::SliceNavigatePresenter(d->viewWidget, d->navigatorWidget) };
        Interactor::PlaneNavigatorController planeNavigatorController(slice_presenter.get());
        if (false == planeNavigatorController.InitSlice(d->sceneID)){
            QMessageBox::warning(nullptr, "Error", "Failed to allocate initial slice positions.");
            return false;
        }
                
        // load visualization preset
        std::shared_ptr<Plugins::VisualizationDataIO::VisualizationDataWriter> writer{ new Plugins::VisualizationDataIO::VisualizationDataWriter };
        std::shared_ptr<Plugins::VisualizationDataIO::VisualizationDataReader> reader{ new Plugins::VisualizationDataIO::VisualizationDataReader };

        std::shared_ptr<Interactor::VisualizationPresetPresenter> visualizationPresetPresenter{
            new Interactor::VisualizationPresetPresenter(d->viewWidget,  d->transferfunctionWidget,
                                                         d->fluorescenceWidget, d->visualizationListWidget,d->modalityWidget)
        };

        Interactor::VisualizationPresetController visualizationPresetController(visualizationPresetPresenter.get(), writer.get(), reader.get());

        const auto defaultPath = QString("%1/preset").arg(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));        
        auto prev = QSettings("Tomocube", "TomoAnalysis").value("recent/preset", defaultPath).toString();        
        if (false == QDir().exists(prev)) {
            if (false == QDir().mkpath(prev)) {
                //QMessageBox::warning(nullptr, "Error", "Failed to make preset folder.");
                //return false;
                prev = defaultPath;
            }
        }

        if (false == visualizationPresetController.LoadList(prev.toStdString())) {
            QMessageBox::warning(nullptr, "Load preset list", "Failed to load visualization preset list.");
            return false;
        }
        
        // load tcf default preset
        QFileInfo fileInfo(path);
        const auto fileName = fileInfo.fileName();                
        const auto defaultPreset = QString("%1/%2.xml").arg(fileInfo.dir().absolutePath()).arg(fileName.chopped(4));        
        if (false == QFile::exists(defaultPreset)) {
            return true;
        }

        if (false == visualizationPresetController.Load(d->sceneID, defaultPreset.toStdString())) {
            QMessageBox::warning(nullptr, "Load preset", "Failed to load visualization preset.");
            return false;
        }                
        
        return true;
    }
    auto MainWindow::SetVisibleAxisGrid(const bool& visible) const -> void {
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget,d->viewingToolWidget) };
        Interactor::ScreenUtilController screenUtilController(presenter.get());
        screenUtilController.SetVisibleAxisGrid(d->sceneID, visible);
    }
    auto MainWindow::SetVisibleBoundaryBox(const bool& visible) const -> void {
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget,d->viewingToolWidget) };
        Interactor::ScreenUtilController screenUtilController(presenter.get());
        screenUtilController.SetVisibleBoundaryBox(d->sceneID, visible);
    }
    auto MainWindow::SetVisibleOrientationMarker(const bool& visible) const -> void {
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget,d->viewingToolWidget) };
        Interactor::ScreenUtilController screenUtilController(presenter.get());
        screenUtilController.SetVisibleOrientationMarker(d->sceneID, visible);
    }
    auto MainWindow::SetVisibleTimeStamp(const bool& visible) const -> void {
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget,d->viewingToolWidget) };
        Interactor::ScreenUtilController screenUtilController(presenter.get());
        screenUtilController.SetVisibleTimeStamp(d->sceneID, visible);
    }
    auto MainWindow::SetTimeStampColor(QColor color) const -> void {
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget,d->viewingToolWidget) };
        Interactor::ScreenUtilController screenUtilController(presenter.get());
        screenUtilController.SetTimeStampColor(d->sceneID, color.red(), color.green(), color.blue());
    }
    auto MainWindow::SetTimeStampSize(int size) const -> void {        
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget,d->viewingToolWidget) };
        Interactor::ScreenUtilController screenUtilController(presenter.get());
        screenUtilController.SetTimeStampSize(d->sceneID, size);
    }
    auto MainWindow::SetSliceResolution(int res) const -> void {
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget) };
        Interactor::ScreenUtilController screenUtilController(presenter.get());
        screenUtilController.SetResolution(d->sceneID, res, false);
    }
    auto MainWindow::SetVolumeResolution(int res) const -> void {
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget) };
        Interactor::ScreenUtilController screenUtilController(presenter.get());
        screenUtilController.SetResolution(d->sceneID, res, true);
	}
    auto MainWindow::SetAxisGridFontSize(int size) const -> void {
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget) };
        Interactor::ScreenUtilController screenUtilController(presenter.get());
        screenUtilController.SetAxisGridFontSize(d->sceneID,size);
    }
    auto MainWindow::SetAxisGridColor(int axis, QColor color) const -> void {
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget) };
        Interactor::ScreenUtilController screenUtilController(presenter.get());
        screenUtilController.SetAxisGridColor(d->sceneID, axis, color.red(),color.green(),color.blue());
    }
    auto MainWindow::ChangeLayout(const int& type) const -> void {
        std::shared_ptr<Interactor::ScenePresenter> presenter{ new Interactor::ScenePresenter(d->viewWidget,d->viewingToolWidget) };
        Interactor::LayoutController layoutController(presenter.get());
        layoutController.SetLayoutType(d->sceneID, Entity::LayoutType::_from_integral(type));

        //set layout to screenshot component widget
        switch(Entity::LayoutType::_from_integral(type)) {
        case Entity::LayoutType::VSlicesBy3D:
            d->screenshotComponent->SetMultiLayerType(TC::MultiLayoutType::VSlicesBy3D);
            d->movieMakerComponent->SetMultiLayerType(TC::MultiLayoutType::VSlicesBy3D);
            break;
        case Entity::LayoutType::HSlicesBy3D:
            d->screenshotComponent->SetMultiLayerType(TC::MultiLayoutType::HSlicesBy3D);
            d->movieMakerComponent->SetMultiLayerType(TC::MultiLayoutType::HSlicesBy3D);
            break;
        case Entity::LayoutType::TwoByTwo:
            d->screenshotComponent->SetMultiLayerType(TC::MultiLayoutType::TwoByTwo);
            d->movieMakerComponent->SetMultiLayerType(TC::MultiLayoutType::TwoByTwo);
            break;
        case Entity::LayoutType::VXY3D:
            d->screenshotComponent->SetMultiLayerType(TC::MultiLayoutType::VXY3D);
            d->movieMakerComponent->SetMultiLayerType(TC::MultiLayoutType::VXY3D);
            break;
        case Entity::LayoutType::HXY3D:
            d->screenshotComponent->SetMultiLayerType(TC::MultiLayoutType::HXY3D);
            d->movieMakerComponent->SetMultiLayerType(TC::MultiLayoutType::HXY3D);
            break;
        case Entity::LayoutType::XYPlane:
            d->screenshotComponent->SetMultiLayerType(TC::MultiLayoutType::XYPlane);
            d->movieMakerComponent->SetMultiLayerType(TC::MultiLayoutType::XYPlane);
            break;
        case Entity::LayoutType::YZPlane:
            d->screenshotComponent->SetMultiLayerType(TC::MultiLayoutType::YZPlane);
            d->movieMakerComponent->SetMultiLayerType(TC::MultiLayoutType::YZPlane);
            break;
        case Entity::LayoutType::XZPlane:
            d->screenshotComponent->SetMultiLayerType(TC::MultiLayoutType::XZPlane);
            d->movieMakerComponent->SetMultiLayerType(TC::MultiLayoutType::XZPlane);
            break;
        case Entity::LayoutType::Only3D:
            d->screenshotComponent->SetMultiLayerType(TC::MultiLayoutType::Only3D);
            d->movieMakerComponent->SetMultiLayerType(TC::MultiLayoutType::Only3D);
            break;
        }
    }
    auto MainWindow::ResetLayout(const bool& reset) const -> void {
        if (nullptr == d->viewWidget) {
            return;
        }

        if (false == reset) {
            return;
        }

        d->viewWidget->ResetLayout(-1);
    }
    auto MainWindow::SaveLayout(const QString& path) const -> void {
        if (true == path.isEmpty()) {
            return;
        }

        if (nullptr == d->viewWidget) {
            return;
        }

        if (false == d->viewWidget->SaveLayout(path)) {
            QMessageBox::warning(nullptr, "Save Layout", "Failed to save layout.");
        }
    }
    auto MainWindow::LoadLayout(const QString& path) const -> int {
        if (true == path.isEmpty()) {
            return -1;
        }
        if (nullptr == d->viewWidget) {
            return -1;
        }

        if (false == d->viewWidget->LoadLayout(path)) {
            QMessageBox::warning(nullptr, "Save Layout", "Failed to load layout.");
        }

        return d->viewWidget->GetCurrentLayoutType();
    }
    auto MainWindow::TryDeactivate() -> bool {
        return true;
    }
    auto MainWindow::TryActivate() -> bool {
        //2D Viewer initialize Open Inventor classes

        d->ui = new Ui::MainWindow;
        d->ui->setupUi(this);

        d->viewWidget = new Plugins::OIVViewer;
        d->modalityWidget = new Plugins::ModalityPanel;
        d->displayWidget = new Plugins::DisplayPanel;
        d->viewingToolWidget = new Plugins::ViewingToolPanel;
        d->visualizationListWidget = new Plugins::VisualizationListPanel;
        d->transferfunctionWidget = new Plugins::TransferFunctionPanel;
        d->fluorescenceWidget = new Plugins::FLChannelPanel;
        d->playerWidget = new Plugins::TCFPlayerPanel;
        //d->screenshotWidget = new Plugins::ScreenShotPanel;
        d->screenshotComponent = std::make_shared<TC::ScreenShotWidget>();
        //d->movieMakerWidget = new Plugins::RecordVideoPanel;
        d->movieMakerComponent = std::make_shared<TC::VideoRecorderWidget>();
        d->navigatorWidget = new Plugins::NavigatorPanel;        

        d->leftDockWidgets.clear();
        d->rightDockWidgets.clear();

        // build ui
        auto upperMainWindow = new QMainWindow();
        //upperMainWindow->setProperty("Closing", "TRUE");
        upperMainWindow->setDockOptions(upperMainWindow->dockOptions() & ~QMainWindow::AllowTabbedDocks);
        upperMainWindow->setContentsMargins(0, 0, 0, 0);

        auto modalityDock = new TC::TCDockWidget("Modality", upperMainWindow);
        modalityDock->setAllowedAreas(Qt::LeftDockWidgetArea);
        modalityDock->setFeatures(TC::TCDockWidget::DockWidgetMovable | TC::TCDockWidget::DockWidgetFloatable);
        modalityDock->setWidget(d->modalityWidget);
        d->leftDockWidgets << modalityDock;

        //auto displayDock = new TC::TCDockWidget("Display", upperMainWindow);
        auto displayDock = new TC::TCDockWidget("Data Type", upperMainWindow);
        displayDock->setAllowedAreas(Qt::LeftDockWidgetArea);
        displayDock->setFeatures(TC::TCDockWidget::DockWidgetMovable | TC::TCDockWidget::DockWidgetFloatable);
        displayDock->setWidget(d->displayWidget);
        d->leftDockWidgets << displayDock;

        auto viewingToolDock = new TC::TCDockWidget("Viewing tool", upperMainWindow);
        viewingToolDock->setAllowedAreas(Qt::LeftDockWidgetArea);
        viewingToolDock->setFeatures(TC::TCDockWidget::DockWidgetMovable | TC::TCDockWidget::DockWidgetFloatable);
        viewingToolDock->setWidget(d->viewingToolWidget);
        d->leftDockWidgets << viewingToolDock;

        auto screenshotDock = new TC::TCDockWidget("Screenshot", upperMainWindow);
        screenshotDock->setAllowedAreas(Qt::LeftDockWidgetArea);
        screenshotDock->setFeatures(TC::TCDockWidget::DockWidgetMovable | TC::TCDockWidget::DockWidgetFloatable);
        //screenshotDock->setWidget(d->screenshotWidget);
        screenshotDock->setWidget(d->screenshotComponent.get());
        d->leftDockWidgets << screenshotDock;

        auto navigatorDock = new TC::TCDockWidget("Navigator", upperMainWindow);
        navigatorDock->setAllowedAreas(Qt::LeftDockWidgetArea);
        navigatorDock->setFeatures(TC::TCDockWidget::DockWidgetMovable | TC::TCDockWidget::DockWidgetFloatable);
        navigatorDock->setWidget(d->navigatorWidget);
        d->leftDockWidgets << navigatorDock;

        auto transferFunctionDock = new TC::TCDockWidget("Preset", upperMainWindow);
        transferFunctionDock->setAllowedAreas(Qt::RightDockWidgetArea);
        transferFunctionDock->setFeatures(TC::TCDockWidget::DockWidgetMovable | TC::TCDockWidget::DockWidgetFloatable);
        d->rightDockWidgets << transferFunctionDock;
        {
            auto tabWidget = new QTabWidget;
            tabWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
            tabWidget->addTab(d->transferfunctionWidget, "HT");
            tabWidget->addTab(d->fluorescenceWidget, "FL");

            auto baseLayout = new QVBoxLayout;
            baseLayout->setContentsMargins(0, 0, 0, 0);
            baseLayout->addWidget(d->visualizationListWidget);
            baseLayout->addWidget(tabWidget);            

            auto baseWidget = new QWidget;
            baseWidget->setObjectName("panel-base");
            baseWidget->setLayout(baseLayout);

            transferFunctionDock->setWidget(baseWidget);
        }

        auto viewBaseWidget = new QWidget;
        viewBaseWidget->setObjectName("panel-base");
        viewBaseWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        {
            auto titleLabel = new QLabel("View");
            titleLabel->setObjectName("h6");

            auto titleLayout = new QHBoxLayout;
            titleLayout->setContentsMargins(14, 3, 14, 3);
            titleLayout->addWidget(titleLabel);

            auto contentsLayout = new QVBoxLayout;
            contentsLayout->setContentsMargins(0, 0, 0, 0);
            contentsLayout->addWidget(d->viewWidget);

            auto viewContentsWidget = new QWidget;
            viewContentsWidget->setObjectName("panel-contents");
            viewContentsWidget->setLayout(contentsLayout);

            auto layout = new QVBoxLayout;
            layout->setContentsMargins(0, 0, 0, 0);
            layout->setSpacing(0);
            layout->addLayout(titleLayout);
            layout->addWidget(viewContentsWidget);

            viewBaseWidget->setLayout(layout);
        }

        upperMainWindow->setCentralWidget(viewBaseWidget);

        for (auto dockWidget : d->leftDockWidgets) {
            upperMainWindow->addDockWidget(Qt::LeftDockWidgetArea, dockWidget);
        }
        upperMainWindow->addDockWidget(Qt::RightDockWidgetArea, transferFunctionDock);

        QList<QDockWidget*> rightDocks;
        rightDocks.append(transferFunctionDock);

        upperMainWindow->resizeDocks(rightDocks, QVector<int>(rightDocks.size(), 350).toList(), Qt::Horizontal);

        upperMainWindow->resizeDocks(
            d->leftDockWidgets,
            QVector<int>(d->leftDockWidgets.size(), 350).toList(),
            Qt::Horizontal
        );

        auto mainLayout = new QVBoxLayout;
        mainLayout->setContentsMargins(0, 0, 0, 0);
        mainLayout->setSpacing(7);
        mainLayout->addWidget(upperMainWindow, 1);

        d->ui->mainWidget->setLayout(mainLayout);

        auto videoTabWidget = new TC::AdaptiveTabWidget;
        videoTabWidget->addTab(d->playerWidget, "Player");        
        videoTabWidget->addTab(d->movieMakerComponent.get(), "Movie maker");
        videoTabWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        auto bottomPanelLayout = new QVBoxLayout;
        bottomPanelLayout->setContentsMargins(0, 0, 0, 0);
        bottomPanelLayout->addWidget(videoTabWidget);
        d->ui->bottomPanelWidget->setLayout(bottomPanelLayout);
        d->ui->bottomPanelWidget->setObjectName("panel-base");

        Init();

        //start global communication
        subscribeEvent("TabChange");
        subscribeEvent(TC::Framework::MenuEvent::Topic());
        connectEvent(SIGNAL(sigCtkEvent(ctkEvent)), this, SLOT(OnSignalPrimitive(ctkEvent)));

        d->thisPointer = this;
        return true;
    }
    auto MainWindow::IsActivate() -> bool {
        return (nullptr != d->ui);
    }
    auto MainWindow::GetMetaInfo() -> QVariantMap {
        return d->appProperties;
    }
    void MainWindow::OnSignalPrimitive(const ctkEvent& ctkEvent) {
        using MenuType = TC::Framework::MenuTypeEnum;
        MenuType menuAction = MenuType::Action;
        MenuType menuChecked = MenuType::ActionChecked;
        MenuType menuUnchecked = MenuType::ActionUnchecked;
        MenuType menuSudden = MenuType::NONE;

        if (ctkEvent.getProperty("TabName").isValid()) {
            auto TabName = ctkEvent.getProperty("TabName").toString();
            if (TabName.compare("3D View") == 0) {
                if (ctkEvent.getProperty(menuSudden._to_string()).isValid()) {
                    for (auto* ldock : d->leftDockWidgets) {
                        if (ldock->isFloating()) {
                            ldock->setFloating(false);
                        }
                    }
                    for (auto* rdock : d->rightDockWidgets) {
                        if (rdock->isFloating()) {
                            rdock->setFloating(false);
                        }
                    }
                }
                else {
                    RequsetRender();
                    //Emit tcf info script
                    MenuEvent menuEvt(MenuTypeEnum::TitleBar);
                    menuEvt.scriptSet(GetCurTCF());
                    publishSignal(menuEvt, "3D View");
                    if (d->passedPath == d->threadPath) {
                        d->changeTimeByForce = true;
                        d->playerWidget->ForceTimeIndex(d->passedTimeStep);
                    }
                }
            }            
        }
        else if (ctkEvent.getProperty(menuAction._to_string()).isValid()) {
            OnMenuAction(ctkEvent);
        }
        else if (ctkEvent.getProperty(menuChecked._to_string()).isValid()) {
            OnMenuActionChecked(ctkEvent);
        }
        else if (ctkEvent.getProperty(menuUnchecked._to_string()).isValid()) {
            OnMenuActionUnChecked(ctkEvent);
        }
        else if (ctkEvent.getProperty(menuSudden._to_string()).isValid()) {
            ForceClosePopup();
        }
    }
    auto MainWindow::OnMenuAction(const ctkEvent& ctkEvent) -> void {
        using MenuType = TC::Framework::MenuTypeEnum;
        MenuType menuAction = MenuType::Action;
        auto actionName = ctkEvent.getProperty(menuAction._to_string()).toString();
        if (actionName.contains("Open")) {
            const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recentOpenTCF").toString();

            const QString fileName = QFileDialog::getOpenFileName(nullptr, "Select TCF", prev, "TCF (*.tcf)");
            if (true == fileName.isEmpty()) {
                QLOG_ERROR() << "file name is empty";
                return;
            }

            QSettings("Tomocube", "TomoAnalysis").setValue("recentOpenTCF", fileName);

            if (true == d->opened) {
                this->Reset();
            }

            //this->OpenFile(fileName);
            d->openDialog = new QProgressDialog(nullptr);
            d->openDialog->setWindowTitle("File Open");
            d->openDialog->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
            d->openDialog->setMinimumWidth(400);
            d->openDialog->setCancelButton(nullptr);
            d->openDialog->setRange(0, 3);
            d->openDialog->setValue(0);
            d->openDialog->setLabelText(QString("Initializing 3D Visualization ..."));
            connect(d->openDialog, SIGNAL(canceled()), this, SLOT(onOpenCanceled()));

            d->threadPath = fileName;
            d->openFailedMsg = QString();
            d->loadFinished = false;
            d->setSliceFinished = false;
            d->metaFinished = false;
            d->curOpenThread = SbThread::create(OpenFileThread3d, (void*)d.get());
            d->openDialog->open();
        }
        else if (actionName.contains("Reset")) {
            //Reset layout
            const auto answer = QMessageBox::question(nullptr, "Reset layout",
                "Are you sure you want to reset layout?",
                QMessageBox::Yes | QMessageBox::No);
            if (QMessageBox::No == answer) {
                return;
            }
            this->ResetLayout(true);
        }
        else if (actionName.contains("Save")) {
            //Save layout
            const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recentSaveLayout").toString();

            const QString fileName = QFileDialog::getSaveFileName(nullptr, tr("Save layout file"),
                prev, tr("Layout file (*.layout)"));
            if (true == fileName.isEmpty()) {
                return;
            }

            QSettings("Tomocube", "TomoAnalysis").setValue("recentSaveLayout", fileName);


            this->SaveLayout(fileName);
        }
        else if (actionName.contains("Load")) {
            //Load layout
            const auto prev = QSettings("Tomocube", "TomoAnalysis").value("recentLoadLayout").toString();

            const auto fileName = QFileDialog::getOpenFileName(nullptr, "Select a layout file.", prev, "*.layout");
            if (true == fileName.isEmpty()) {
                return;
            }

            QSettings("Tomocube", "TomoAnalysis").setValue("recentLoadLayout", fileName);

            this->LoadLayout(fileName);
        }
    }
    auto MainWindow::OnMenuActionChecked(const ctkEvent& ctkEvent) -> void {
        using MenuType = TC::Framework::MenuTypeEnum;
        MenuType menuGroup = MenuType::ActionChecked;
        auto groupName = ctkEvent.getProperty(menuGroup._to_string()).toString();
        if (groupName.contains("Show/Hide")) {
            if (groupName.contains("OrientationMarker")) {
                //Not Implemented yet
            }
            else if (groupName.contains("Boundary box")) {
                this->SetVisibleBoundaryBox(true);
            }
            else if (groupName.contains("Axis grid")) {
                this->SetVisibleAxisGrid(true);
            }
            else if (groupName.contains("Timestamp")) {
                //Not Implemented yet
            }
            else if (groupName.contains("Title")) {
                //Not Implemented yet
            }
        }
        else {
            if (groupName.contains("2X2 layout")) {
                this->ChangeLayout(1);
            }
            else if (groupName.contains("2D image left, 3D right")) {
                this->ChangeLayout(2);
            }
            else if (groupName.contains("2D image top, 3D bottom")) {
                this->ChangeLayout(3);
            }
            else if (groupName.contains("Big 3D")) {
                this->ChangeLayout(4);
            }
            else if (groupName.contains("XY Plane")) {
                this->ChangeLayout(5);
            }
            else if (groupName.contains("YZ Plane")) {
                this->ChangeLayout(6);
            }
            else if (groupName.contains("XZ Plane")) {
                this->ChangeLayout(7);
            }
            else if (groupName.contains("XY plane left, 3D right")) {
                this->ChangeLayout(8);
            }
            else if (groupName.contains("XY plane top, 3D bottom")) {
                this->ChangeLayout(9);
            }
        }
    }
    auto MainWindow::OnMenuActionUnChecked(const ctkEvent& ctkEvent) -> void {
        using MenuType = TC::Framework::MenuTypeEnum;
        MenuType menuGroup = MenuType::ActionUnchecked;
        auto groupName = ctkEvent.getProperty(menuGroup._to_string()).toString();
        if (groupName.contains("Show/Hide")) {
            if (groupName.contains("OrientationMarker")) {
                //Not implemented yet
            }
            else if (groupName.contains("Boundary box")) {
                this->SetVisibleBoundaryBox(false);
            }
            else if (groupName.contains("Axis grid")) {
                this->SetVisibleAxisGrid(false);
            }
            else if (groupName.contains("Timestamp")) {
                //Not implemented yet
            }
            else if (groupName.contains("Title")) {
                //Not implemented yet
            }
        }
    }	
}
