#pragma once

#include <memory>
#include "Scene.h"
//#include "TomoAnalysisEntityExport.h"

namespace TomoAnalysis::Viewer::Entity {
	class ViewerEntity_API SceneStorage final {
	public:
		typedef SceneStorage Self;
		typedef std::shared_ptr<Self> Pointer;

	public:
		static auto GetInstance() -> Pointer;
		explicit SceneStorage(const SceneStorage& other) = delete;
		~SceneStorage();
			
		auto CreateScene() -> Scene::ID;
		auto ReleseScene(const Scene::ID& sceneID)->void;

		auto GetScene(const Scene::ID& sceneID)->Scene::Pointer;
			
	private:
		SceneStorage();
			
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}

