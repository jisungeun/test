#pragma once

#include <memory>
#include <map>
#include <vector>

#include "Image.h"

//#include "TomoAnalysisEntityExport.h"

namespace TomoAnalysis::Viewer::Entity {
    struct Color {
		int red = 0;
		int green = 0;
		int blue = 0;
		int alpha = 0;

		Color() { red = 0; green = 0; blue = 0; alpha = 0; }
        Color(int r, int g, int b, int a) { red = r; green =g; blue = b; alpha = a; }
    };

	struct TFItem {
		double intensityMin = 0;
		double intensityMax = 0;
	    double gradientMin = 0;
		double gradientMax = 0;
		float transparency = 0.f;
		Color color;

		bool visible = true;

		typedef TFItem Self;
		typedef std::shared_ptr<Self> Pointer;
	};

	typedef std::vector<TFItem::Pointer> TFItemList;

	class ViewerEntity_API TransferFunction final {
	public:
		typedef TransferFunction Self;
		typedef std::shared_ptr<Self> Pointer;

	public:
		TransferFunction();
		explicit TransferFunction(const TransferFunction& other);
		~TransferFunction();

		auto AddItem(const TFItem::Pointer& item) const ->bool;
        auto ModifyItem(const int& index, const TFItem::Pointer& item) const ->bool;
		auto DeleteItem(const int& index) const ->bool;
        auto SetVisibleItem(const int& index, bool visible) const ->bool;
		auto GetItem(const int& index)->TFItem::Pointer;
		auto GetItemList(void)->TFItemList;
		auto SetItemList(const TFItemList& list)->void;

		auto Clear(void)->void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}