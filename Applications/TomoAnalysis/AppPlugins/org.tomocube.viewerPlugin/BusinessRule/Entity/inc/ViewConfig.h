#pragma once

#include <memory>
#include <enum.h>

#include "Image.h"
//#include "TomoAnalysisEntityExport.h"

namespace TomoAnalysis::Viewer::Entity {
	BETTER_ENUM(ViewType, int8_t, NONE = -1, XY2D = 0, YZ2D, ZX2D, V3D)
	BETTER_ENUM(AXIS, uint8_t, X, Y, Z)

    class ViewerEntity_API ViewConfig final {
	public:
		struct Color {
			uint8_t Red = 0;
			uint8_t Green = 0;
			uint8_t Blue = 0;
			uint8_t Alpha = 0;

			Color() { Red = 0; Green = 0; Blue = 0; Alpha = 0; }
			Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a) { Red = r; Green = g; Blue = b; Alpha = a; }
		};

		typedef int Index;
		typedef ViewConfig Self;
		typedef std::shared_ptr<Self> Pointer;
		typedef ViewType Type;

	public:
		ViewConfig();
		explicit ViewConfig(const ViewConfig& other);
		~ViewConfig();

		auto GetIndex(void) const ->Index;
		auto SetIndex(const Index& index) const ->void;

		auto GetViewType(void) const ->ViewType;
		auto SetViewType(const ViewType& type) const ->void;

		auto GetImageType(void) const->ImageType;
		auto SetImageType(const ImageType& type) const ->void;

		auto GetSliceIndex(void) const->int;
		auto SetSliceIndex(const int& index) const ->void;

		auto GetBackgroundColor(void) const ->Color;
		auto SetBackgroundColor(const Color& color) const ->void;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}