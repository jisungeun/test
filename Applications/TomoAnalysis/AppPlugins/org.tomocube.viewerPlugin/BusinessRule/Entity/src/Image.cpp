#include <map>

#include "Image.h"

namespace TomoAnalysis::Viewer::Entity {
    struct Image::Impl {
        Impl() = default;
        Impl(const Impl& other)
            : filePath(other.filePath)
            , metaList(other.metaList) {
        }
		~Impl() = default;

        double first_time{ 0 };

		std::string filePath;
		std::map<Type, Meta> metaList;
    };

	Image::Image()
        : d(new Impl()) {
	}

    Image::Image(const Image& other)
        : d(new Impl(*other.d)) {
    }

	Image::~Image() = default;

    auto Image::GetPath() const -> std::string {
		return d->filePath;
    }

    auto Image::HasType(ImageType type) const -> bool {
		return (d->metaList.find(type) != d->metaList.end());
    }

    auto Image::GetDimension(ImageType type) const -> Dimension {
		Dimension dim;
        try {
			dim = d->metaList[type].dimension;
        } catch(...) {
        }
		return dim;
    }
    auto Image::GetTimePoints(ImageType type,int ch) const -> std::vector<double> {
        std::vector<double> points;
        try {
            points = d->metaList[type].timepoints[ch];
        }catch(...) {
            
        }
        return points;
    }

    auto Image::GetResolution(ImageType type) const -> Resolution {
		Resolution res;
        try {
			res = d->metaList[type].resolution;
        } catch(...) {
        }
		return res;
    }

    auto Image::GetCount(ImageType type) const -> uint32_t {
		uint32_t count = 0;
        try {
			count = d->metaList[type].count;
        } catch(...) {
        }
		return count;
    }

    auto Image::GetTimeInterval(ImageType type) const -> uint32_t {
		uint32_t interval = 0;
        try {
			interval = d->metaList[type].interval;
        } catch(...) {
        }
		return interval;
    }

    auto Image::GetChannel(ImageType type) const->Channel* {
        Channel* channel{nullptr};
        try {
            channel = d->metaList[type].channel;
        }
        catch (...) {
        }
        return channel;
    }

    auto Image::GetIntensity(ImageType type) const->Intensity {
        Intensity intensity;
        try {
            intensity = d->metaList[type].intensity;
        }
        catch (...) {
        }
        return intensity;
    }

    auto Image::SetPath(const std::string& path) -> void {
		d->filePath = path;
    }

    auto Image::SetMeta(Type type, Meta meta) -> void {
		d->metaList[type] = meta;
    }

    auto Image::SetFirstTime(const double& ft) -> void {
        d->first_time = ft;
    }

    auto Image::GetFirstTime() const -> double {
        return d->first_time;
	}
}
