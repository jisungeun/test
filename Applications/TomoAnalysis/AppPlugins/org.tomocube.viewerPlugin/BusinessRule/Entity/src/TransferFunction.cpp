#include "TransferFunction.h"

namespace TomoAnalysis::Viewer::Entity {
	struct TransferFunction::Impl {
		Impl() = default;
		Impl(const Impl& other) = default;
		~Impl() = default;

		TFItemList itemList;
	};

	TransferFunction::TransferFunction()
		: d(new Impl()) {

	}

	TransferFunction::TransferFunction(const TransferFunction& other)
		: d(new Impl(*other.d)) {
	}

	TransferFunction::~TransferFunction() = default;

	auto TransferFunction::AddItem(const TFItem::Pointer& item) const ->bool {
		d->itemList.push_back(item);
		return true;
	}

	auto TransferFunction::ModifyItem(const int& index, const TFItem::Pointer& item) const ->bool {
		try {
			d->itemList[index] = item;
		}
		catch (...) {
			return false;
		}

		return true;
	}

	auto TransferFunction::DeleteItem(const int& index) const ->bool {
		try {
			d->itemList.erase(d->itemList.begin() + index);
		}
		catch (...) {
			return false;
		}

		return true;
	}

	auto TransferFunction::SetVisibleItem(const int& index, bool visible) const ->bool {
		try {
			d->itemList[index]->visible = visible;
		}
		catch (...) {
			return false;
		}

		return true;
	}

	auto TransferFunction::GetItem(const int& index)->TFItem::Pointer {
		TFItem::Pointer item;

		try {
			item = d->itemList[index];
		}
		catch (...) {
		}

		return item;
	}

	auto TransferFunction::GetItemList(void)->TFItemList {
		return d->itemList;
	}

	auto TransferFunction::SetItemList(const TFItemList& list)->void {
		d->itemList = list;
	}

	auto TransferFunction::Clear(void)->void {
		d->itemList.clear();
	}
}
