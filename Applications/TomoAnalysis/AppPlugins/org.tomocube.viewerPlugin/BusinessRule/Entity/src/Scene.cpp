#include <iostream>

#include "Scene.h"

namespace TomoAnalysis::Viewer::Entity {
    struct Scene::Impl {
        Impl() = default;
        Impl(const Impl& other) = default;
	
		ID id = -1;

		Image::Pointer image;
		LayoutType layoutType = LayoutType::UNKNOWN;
		
		ViewConfigList viewConfigList;

		FLChannelInfo flChannelInfo;
		TransferFunction::Pointer transferFunction = std::make_shared<TransferFunction>();

		bool visibleBoundaryBox = false;
		bool visibleAxisGrid = false;
		bool visibleOrientationMarker = false;
		bool visibleTimeStamp = false;
		int axisGridFontSize{ 18 };
		int axisGridColor[3][3] = { {0,255,0},{0,0,255},{255,0,0} };
		int timestampR{ 255 };
		int timestampG{ 255 };
		int timestampB{ 255 };
		int timestampSize{ 20 };

		bool enableOverlay = false;		

		int timelapesIndex = 0;
		double timelapseTime = 0.0;

		int slice_resolution = 0;
		int volume_resolution = -1;

		Modality activatedModality = Modality::None;
    };

	Scene::Scene()
        : d(new Impl()) {		
	}

	Scene::Scene(const Scene& other)
        : d(new Impl(*other.d)) {
	}

	Scene::~Scene() = default;

	auto Scene::GetImage(void) const ->Image::Pointer {
		return d->image;
	}

    auto Scene::SetImage(Image::Pointer& image) const -> void {
		d->image = image;
    }

	auto Scene::SetResolution(int res,bool forVolume) const -> void {
		if(forVolume) {
			d->volume_resolution = res;
		}else {
			d->slice_resolution = res;
		}
    }
	auto Scene::GetResolution(bool forVolume) const -> int {
		if(forVolume) {
			return d->volume_resolution;
		}
		return d->slice_resolution;
    }

	auto Scene::GetLayoutType() const ->LayoutType {
		return d->layoutType;
	}

	auto Scene::SetLayoutType(LayoutType type) const ->void {
		d->layoutType = type;
    }

	auto Scene::SetDefaultLayout(const Entity::Modality& modality) const->bool {		
		if(d->layoutType._to_index() != LayoutType::UNKNOWN) {
			return true;
		}

	    Entity::LayoutType layout = Entity::LayoutType::UNKNOWN;

		if(((modality & Modality::HTVolume) == Modality::HTVolume) ||
		   ((modality & Modality::FLVolume) == Modality::FLVolume)) {
			layout = Entity::LayoutType::HSlicesBy3D;
		}
		else {
			layout = Entity::LayoutType::XYPlane;
		}

		d->layoutType = layout;

		return true;
	}

	auto Scene::GetViewConfigList(void) const ->ViewConfigList {
		return d->viewConfigList;
	}

	auto Scene::GetViewConfig(const int& index) const ->ViewConfig::Pointer {
		ViewConfig::Pointer viewConfig;

		try {
			viewConfig = d->viewConfigList[index];
		}
		catch (...) {
		}

		return viewConfig;
	}

    auto Scene::AddViewCofig(const ViewConfig::Pointer& config) const -> void {
		d->viewConfigList.push_back(config);
    }

	auto Scene::SetViewConfig(const int& index, const ViewConfig::Pointer& config) const ->bool {
        if(index >= d->viewConfigList.size()) {
			return false;
        }

	    auto result = true;

	    try {
			d->viewConfigList[index] = config;
		}
		catch (...) {
			result = false;
		}
		return result;
	}

	auto Scene::SetViewConfigList(const ViewConfigList& viewConfigList) const ->void {
		d->viewConfigList = viewConfigList;
	}

	auto Scene::GenerateViewConfigList()const->bool {
		auto config0 = std::make_shared<Entity::ViewConfig>();
		config0->SetIndex(0);
		config0->SetViewType(Entity::ViewType::XY2D);
		d->viewConfigList.push_back(config0);

		auto config1 = std::make_shared<Entity::ViewConfig>();
		config1->SetIndex(1);
		config1->SetViewType(Entity::ViewType::YZ2D);
		d->viewConfigList.push_back(config1);

		auto config2 = std::make_shared<Entity::ViewConfig>();
		config2->SetIndex(2);
		config2->SetViewType(Entity::ViewType::ZX2D);
		d->viewConfigList.push_back(config2);

		auto config3 = std::make_shared<Entity::ViewConfig>();
		config3->SetIndex(3);
		config3->SetViewType(Entity::ViewType::V3D);
		d->viewConfigList.push_back(config3);
		
		return true;
	}

	auto Scene::GetFLChannelInfoList(void)->FLChannelInfo {
		return d->flChannelInfo;
	}

	auto Scene::GetFLChannelInfo(const Channel& channel) const->ChannelInfo::Pointer {
		ChannelInfo::Pointer info;
		try {
			info = d->flChannelInfo[channel];
		}
		catch (...) {
		}
		return info;
	}

	auto Scene::SetFLChannelInfo(const Channel& channel, const ChannelInfo::Pointer& info) const ->void {
		d->flChannelInfo[channel] = info;
	}

	auto Scene::SetFLChannelInfoList(const FLChannelInfo& list) const ->void {
		d->flChannelInfo = list;
	}

	auto Scene::GetTransferFunctionList() const ->TFItemList {
		return d->transferFunction.get()->GetItemList();
	}

	auto Scene::SetTransferFunctionList(const TFItemList& list) const ->void {
		d->transferFunction.get()->SetItemList(list);
	}

	auto Scene::AddTransferFunctionItem(const TFItem::Pointer& item) const ->void {
		d->transferFunction.get()->AddItem(item);
	}

	auto Scene::DeleteTransferFunctionItem(const int& index) const ->void {
		d->transferFunction.get()->DeleteItem(index);
	}

	auto Scene::SetTransferFunctionItem(const int& index, const TFItem::Pointer& item) const ->void {
		d->transferFunction.get()->ModifyItem(index, item);
	}
	auto Scene::GetTransferFunctionItem(const int& index) const->TFItem::Pointer {
		return d->transferFunction.get()->GetItem(index);
	}

	auto Scene::GetVisibleBoundaryBox(void) const ->bool {
		return d->visibleBoundaryBox;
	}

	auto Scene::SetAxisGridFontSize(int size) const -> void {
		d->axisGridFontSize = size;
    }

	auto Scene::SetAxisGridColor(int axis, int r, int g, int b) const -> void {
		d->axisGridColor[axis][0] = r;
		d->axisGridColor[axis][1] = g;
		d->axisGridColor[axis][2] = b;
    }

	auto Scene::GetAxisGridColor(int axis) const -> std::tuple<int, int, int> {
		return std::make_tuple(d->axisGridColor[axis][0], d->axisGridColor[axis][1], d->axisGridColor[axis][2]);
    }

	auto Scene::GetAxisGridFontSize() const -> int {
		return d->axisGridFontSize;
    }

	auto Scene::SetVisibleTimeStamp(bool visible) const -> void {
		d->visibleTimeStamp = visible;
    }

	auto Scene::GetVisibleTimeStamp() const -> bool {
		return d->visibleTimeStamp;
    }

	auto Scene::GetTimeStampColor() -> std::tuple<int, int, int> {
		return std::make_tuple(d->timestampR, d->timestampG, d->timestampB);
    }

	auto Scene::SetTimeStampColor(int r, int g, int b) -> void {
		d->timestampR = r;
		d->timestampG = g;
		d->timestampB = b;
    }

	auto Scene::GetTimeStampSize() -> int {
		return d->timestampSize;
    }

	auto Scene::SetTimeStampSize(int size) -> void {
		d->timestampSize = size;
    }

	auto Scene::SetVisibleBoundaryBox(bool visible) const ->void {
		d->visibleBoundaryBox = visible;
	}

	auto Scene::GetVisibleAxisGrid(void) const ->bool {
		return d->visibleAxisGrid;
	}

	auto Scene::SetVisibleAxisGrid(bool visible) const ->void {
		d->visibleAxisGrid = visible;
	}

	auto Scene::SetVisibleOrientationMarker(bool visible) const -> void {
		d->visibleOrientationMarker = visible;
    }

	auto Scene::GetVisibleOrientationMarker() const -> bool {
		return d->visibleOrientationMarker;
    }

	auto Scene::GetEnableOverlay(void) const ->bool {
		return d->enableOverlay;
	}

	auto Scene::SetEnableOverlay(bool enable) const ->void {
		d->enableOverlay = enable;
	}

	auto Scene::GetActivateModality() const->Entity::Modality {
		return d->activatedModality;
	}

	auto Scene::SetActivateModality(const Entity::Modality& modality) const -> void {
		d->activatedModality = modality;
	}

	auto Scene::GetTimelapseIndex() const -> int {
		return d->timelapesIndex;
	}

    auto Scene::SetTimelapseIndex(const int index)->void {
		d->timelapesIndex = index;
	}

	auto Scene::GetTimelapseTime() const->double {
	    return d->timelapseTime;
	}

	auto Scene::SetTimelapseTime(const double time)->void {
	    d->timelapseTime = time;
	}

	auto Scene::Clear(void)->void {
		int prev_axis_col[3][3];
		for(auto i=0;i<3;i++) {
		    for(auto j=0;j<3;j++) {
				prev_axis_col[i][j] = d->axisGridColor[i][j];
		    }
		}
		d.reset(new Impl);
		for (auto i = 0; i < 3; i++) {
			for (auto j = 0; j < 3; j++) {
				d->axisGridColor[i][j] = prev_axis_col[i][j];
			}
		}
		//preserve axis grid color
	}
}
