#pragma once

#include <TransferFunction.h>
#include <ITransferFunctionOutputPort.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "ViewerUseCaseExport.h"

namespace TomoAnalysis::Viewer::UseCase {
	class ViewerUseCase_API TransferFunctionManager {
	public:
		TransferFunctionManager();
		~TransferFunctionManager();
		auto SetList(const Entity::TFItemList& list, const Entity::Scene::ID& sceneID, ITransferFunctionOutputPort* port)->bool;
		auto Create(const Entity::TFItem::Pointer& item, const Entity::Scene::ID& sceneID, ITransferFunctionOutputPort* port)->bool;
		auto Modify(const int& itemIndex, const Entity::TFItem::Pointer& item,
			        const Entity::Scene::ID& sceneID, ITransferFunctionOutputPort* port)->bool;
		auto Delete(const int& itemIndex, Entity::Scene::ID sceneID, ITransferFunctionOutputPort* port)->bool;
		auto Clear(Entity::Scene::ID sceneID, ITransferFunctionOutputPort* port)->bool;
		auto SetVisible(const int& itemIndex, bool visible,
			            Entity::Scene::ID sceneID, ITransferFunctionOutputPort* port)->bool;

		
	};
}