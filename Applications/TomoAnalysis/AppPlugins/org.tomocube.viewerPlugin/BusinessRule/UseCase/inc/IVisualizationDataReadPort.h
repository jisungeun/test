#pragma once

#include <TransferFunction.h>

#include <Scene.h>
//#include "TomoAnalysisUseCaseExport.h"
#include "ViewerUseCaseExport.h"

namespace TomoAnalysis::Viewer::UseCase {
	class ViewerUseCase_API IVisualizationDataReadPort {
	public:
		IVisualizationDataReadPort();
		virtual ~IVisualizationDataReadPort();

		virtual auto Read(const std::string& path, Entity::TFItemList& tfItemList, Entity::FLChannelInfo& channel)->bool = 0;
	};
}