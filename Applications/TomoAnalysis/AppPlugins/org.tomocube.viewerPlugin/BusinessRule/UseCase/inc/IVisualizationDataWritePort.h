#pragma once

#include <TransferFunction.h>

#include <Scene.h>
//#include "TomoAnalysisUseCaseExport.h"
#include "ViewerUseCaseExport.h"

namespace TomoAnalysis::Viewer::UseCase {
	class ViewerUseCase_API IVisualizationDataWritePort {
	public:
		IVisualizationDataWritePort();
		virtual ~IVisualizationDataWritePort();

		virtual auto Write(const std::string& path, Entity::TFItemList tfItemList, Entity::FLChannelInfo channel)->bool = 0;
	};
}