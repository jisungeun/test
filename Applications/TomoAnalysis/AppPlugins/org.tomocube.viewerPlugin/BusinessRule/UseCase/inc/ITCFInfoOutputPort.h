#pragma once

#include <Image.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "ViewerUseCaseExport.h"

namespace TomoAnalysis::Viewer::UseCase {
	class ViewerUseCase_API ITCFInfoOutputPort {
	public:
		ITCFInfoOutputPort();
		virtual ~ITCFInfoOutputPort();

		virtual void Update(Entity::Image::Pointer& image) = 0;
	};
}