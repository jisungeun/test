#pragma once

#include <IFLChannelOutputPort.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "ViewerUseCaseExport.h"

namespace TomoAnalysis::Viewer::UseCase {
	class ViewerUseCase_API FLChannelManager {
	public:
		FLChannelManager();
		~FLChannelManager();

		auto SetFLChannelInfo(const Entity::Channel& channel, const Entity::ChannelInfo::Pointer& info,
			                  Entity::Scene::ID sceneID, IFLChannelOutputPort* port)->bool;

		auto SetRange(const Entity::Channel& channel, const int& min, const int& max,
			          Entity::Scene::ID sceneID, IFLChannelOutputPort* port)->bool;

		auto SetGamma(const Entity::Channel& channel, const int& value, const bool& isGamma,
			Entity::Scene::ID sceneID, IFLChannelOutputPort* port)->bool;

		auto SetOpacity(const Entity::Channel& channel, const int& value,
			            Entity::Scene::ID sceneID, IFLChannelOutputPort* port)->bool;

		auto SetColor(const Entity::Channel& channel, const int& r, const int& g, const int& b, Entity::Scene::ID sceneID, IFLChannelOutputPort* port)->bool;

        auto SetVisible(const Entity::Channel& channel, const bool& visible,
			            Entity::Scene::ID sceneID, IFLChannelOutputPort* port)->bool;
	};
}
