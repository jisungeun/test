#pragma once

//#include "TomoAnalysisUseCaseExport.h"
#include <Scene.h>

#include "ViewerUseCaseExport.h"

namespace TomoAnalysis::Viewer::UseCase {
	class ViewerUseCase_API AllocateScene {
	public:
		AllocateScene();
		~AllocateScene();

		auto Request()->Entity::Scene::ID;
	};
}