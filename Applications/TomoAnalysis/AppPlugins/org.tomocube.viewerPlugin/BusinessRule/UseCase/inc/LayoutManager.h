#pragma once
#include <Scene.h>

#include <ISceneOutputPort.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "ViewerUseCaseExport.h"

namespace TomoAnalysis::Viewer::UseCase {
	class ViewerUseCase_API LayoutManager {
	public:
		LayoutManager();
		~LayoutManager();

		auto SetLayoutType(const Entity::LayoutType& type, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool;
	};
}