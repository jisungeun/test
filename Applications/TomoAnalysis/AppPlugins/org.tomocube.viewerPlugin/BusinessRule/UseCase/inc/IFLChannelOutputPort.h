#pragma once

#include <Scene.h>

#include "ViewerUseCaseExport.h"

namespace TomoAnalysis::Viewer::UseCase {
	class ViewerUseCase_API IFLChannelOutputPort {
	public:
		IFLChannelOutputPort();
		virtual ~IFLChannelOutputPort();

		virtual void Update(const Entity::FLChannelInfo& info) = 0;
	};
}