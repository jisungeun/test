#pragma once

#include <Image.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "ViewerUseCaseExport.h"

namespace TomoAnalysis::Viewer::UseCase {
	class ViewerUseCase_API IFileReaderPort {
	public:
		IFileReaderPort();
		virtual ~IFileReaderPort();
		
		virtual auto Read(const std::string& path,bool isCrop = false,int offsetX = 0, int offsetY = 0, int sizeX = 0,int sizeY =0,int offsetFLX =0,int offsetFLY =0,int sizeFLX=0,int sizeFLY=0)->Entity::Image::Pointer = 0;
	};
}