#pragma once

#include <Scene.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "ViewerUseCaseExport.h"

namespace TomoAnalysis::Viewer::UseCase {

	class ViewerUseCase_API ISceneOutputPort {
	public:
		ISceneOutputPort();
		virtual ~ISceneOutputPort();
	
	    virtual void Update(Entity::Scene::Pointer& scene) = 0;
	};
}
