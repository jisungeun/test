#pragma once

#include <string>

#include <Scene.h>

#include <ITCFInfoOutputPort.h>
#include <IFileReaderPort.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "ViewerUseCaseExport.h"

namespace TomoAnalysis::Viewer::UseCase {
	class ViewerUseCase_API ImageLoader {
	public:
		ImageLoader();
		~ImageLoader();

		auto Load(const std::string& path, Entity::Scene::ID sceneId, 
			      ITCFInfoOutputPort* port, IFileReaderPort* reader) const ->bool;
		auto Load(const std::string& path, int cropOffsetX,int cropOffsetY,int cropSizeX,int cropSizeY,int cropOffsetXFL,int cropOffsetYFL,int cropSizeXFL,int cropSizeYFL, Entity::Scene::ID sceneId, ITCFInfoOutputPort* port, IFileReaderPort* reader) const ->bool;
	};
}