#pragma once

#include <Scene.h>

//#include "TomoAnalysisUseCaseExport.h"
#include "ViewerUseCaseExport.h"

namespace TomoAnalysis::Viewer::UseCase {
	class ViewerUseCase_API ITransferFunctionOutputPort {
	public:
		ITransferFunctionOutputPort();
		virtual ~ITransferFunctionOutputPort();

		virtual void Update(const int& index, const Entity::TFItem::Pointer& item) = 0;
		virtual void Clear(void) = 0;
	};
}