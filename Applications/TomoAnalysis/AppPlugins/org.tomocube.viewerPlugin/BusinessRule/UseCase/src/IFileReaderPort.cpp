#include "IFileReaderPort.h"

namespace TomoAnalysis::Viewer::UseCase {
	IFileReaderPort::IFileReaderPort() = default;
	IFileReaderPort::~IFileReaderPort() = default;
}