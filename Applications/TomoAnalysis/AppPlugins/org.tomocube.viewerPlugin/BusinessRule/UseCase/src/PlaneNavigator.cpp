#include <iostream>

#include <SceneStorage.h>

#include "PlaneNavigator.h"

namespace TomoAnalysis::Viewer::UseCase {
	PlaneNavigator::PlaneNavigator() = default;
	PlaneNavigator::~PlaneNavigator() = default;

	auto PlaneNavigator::MovePlane(const int&x, const int& y, const int& z, 
		                           Entity::Scene::ID sceneId, ISliceNavigateOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if (!scene.get()) {
			return false;
		}

		auto viewConfigList = scene->GetViewConfigList();
        for (const auto& viewConfig : viewConfigList) {
            const auto type = viewConfig->GetViewType();

            switch (type) {
			case Entity::ViewType::XY2D:
				viewConfig.get()->SetSliceIndex(z);
				break;
            case Entity::ViewType::YZ2D:
				viewConfig.get()->SetSliceIndex(x);
				break;
			case Entity::ViewType::ZX2D:
				viewConfig.get()->SetSliceIndex(y);
				break;
			default:
				break;
            }
        }
		port->Update(scene, x, y, z);

		return true;
	}

	auto PlaneNavigator::MovePhyPlane(const float& x, const float& y, const float& z, 
		                             Entity::Scene::ID sceneId, ISliceNavigateOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if (!scene.get()) {
			return false;
		}

		const auto image = scene->GetImage();
		if (nullptr == image) {
			return false;
		}

		auto activatedModality = scene->GetActivateModality();
		auto imageType = Entity::Unknown;

		if (activatedModality == Entity::Modality::None) {
			return false;
		}

		if (((activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
			(activatedModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume)) {
			// volume
			if ((activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
				imageType = Entity::HT3D;
			}
			else {
				imageType = Entity::FL3D;
			}

		}
		else if (((activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP ||
			(activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP)) {

			// mip
			if ((activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
				imageType = Entity::HT2DMIP;
			}
			else {
				imageType = Entity::FL2DMIP;
			}
		}

		auto posX = x / image->GetResolution(imageType).X;
		auto posY = y / image->GetResolution(imageType).Y;
		auto posZ = z / image->GetResolution(imageType).Z;

		auto viewConfigList = scene->GetViewConfigList();
		for (const auto& viewConfig : viewConfigList) {
			const auto type = viewConfig->GetViewType();

			switch (type) {
			case Entity::ViewType::XY2D:
				viewConfig.get()->SetSliceIndex(static_cast<int>(posZ));
				break;
			case Entity::ViewType::YZ2D:
				viewConfig.get()->SetSliceIndex(static_cast<int>(posX));
				break;
			case Entity::ViewType::ZX2D:
				viewConfig.get()->SetSliceIndex(static_cast<int>(posY));
				break;
			default:
				break;
			}
		}

		port->UpdatePhy(scene, x, y, z);

		return true;
	}
	auto PlaneNavigator::InitSlice(Entity::Scene::ID sceneId, ISliceNavigateOutputPort* port) -> bool {
		if(nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if(!scene.get()) {
			return false;
		}

		const auto image = scene->GetImage();
		if (nullptr == image) {
			return false;
		}

		auto activatedModality = scene->GetActivateModality();
		auto imageType = Entity::Unknown;

		if (activatedModality == Entity::Modality::None) {
			return false;
		}

		if (((activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
			(activatedModality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume)) {
			// volume
			if ((activatedModality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
				imageType = Entity::HT3D;
			}
			else {
				imageType = Entity::FL3D;
			}

		}
		else if (((activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP ||
			(activatedModality & Entity::Modality::FLMIP) == Entity::Modality::FLMIP)) {

			// mip
			if ((activatedModality & Entity::Modality::HTMIP) == Entity::Modality::HTMIP) {
				imageType = Entity::HT2DMIP;
			}
			else {
				imageType = Entity::FL2DMIP;
			}
		}

		auto posX = image->GetDimension(imageType).X / 2;
		auto posY = image->GetDimension(imageType).Y / 2;
		auto posZ = image->GetDimension(imageType).Z / 2;

		auto viewConfigList = scene->GetViewConfigList();
		for (const auto& viewConfig : viewConfigList) {
			const auto type = viewConfig->GetViewType();

			switch (type) {
			case Entity::ViewType::XY2D:
				viewConfig.get()->SetSliceIndex(posZ);
				break;
			case Entity::ViewType::YZ2D:
				viewConfig.get()->SetSliceIndex(posX);
				break;
			case Entity::ViewType::ZX2D:
				viewConfig.get()->SetSliceIndex(posY);
				break;
			default:
				break;
			}
		}

		port->Update(scene, posX, posY, posZ);

		return true;
    }
	auto PlaneNavigator::MoveSlice(const int& viewIndex, const int& sliceIndex, Entity::Scene::ID sceneId, ISliceNavigateOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}
				
		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);		
		if (!scene.get()) {
			return false;
		}				

		auto modality = scene->GetActivateModality();
		auto maxSlice = -1;
		if (((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume ||
			(modality & Entity::Modality::FLVolume) == Entity::Modality::FLVolume)) {
			// volume
			if ((modality & Entity::Modality::HTVolume) == Entity::Modality::HTVolume) {
				//HT range
				if (viewIndex == 0) {
					maxSlice = scene->GetImage()->GetDimension(Entity::ImageType::HT3D).Z;
				}
				else if (viewIndex == 1) {
					maxSlice = scene->GetImage()->GetDimension(Entity::ImageType::HT3D).X;
				}
				else {
					maxSlice = scene->GetImage()->GetDimension(Entity::ImageType::HT3D).Y;
				}
			}
			else {				
				//FL range
				if (viewIndex == 0) {
					maxSlice = scene->GetImage()->GetDimension(Entity::ImageType::FL3D).Z;
				}
				else if (viewIndex == 1) {
					maxSlice = scene->GetImage()->GetDimension(Entity::ImageType::FL3D).X;
				}
				else {
					maxSlice = scene->GetImage()->GetDimension(Entity::ImageType::FL3D).Y;
				}
			}
		}
		else {
			return true;
		}				
		auto real_idx = sliceIndex;
		if (sliceIndex < 1) {
			real_idx = 1;
		}
		else if (sliceIndex > maxSlice) {
			real_idx = maxSlice;
		}

		auto viewConfig = scene->GetViewConfig(viewIndex);
        if(!viewConfig.get()) {
			return false;;
        }		
		viewConfig.get()->SetSliceIndex(real_idx);

		int x = -1;
		int y = -1;
		int z = -1;

		const auto type = viewConfig->GetViewType();
		switch (type) {
		case Entity::ViewType::XY2D:
			z = real_idx;
			break;
		case Entity::ViewType::YZ2D:
			x = real_idx;
			break;
		case Entity::ViewType::ZX2D:
			y = real_idx;
			break;
		default:
			break;
		}

		port->Update(scene, x, y, z);

		return true;
	}
}
