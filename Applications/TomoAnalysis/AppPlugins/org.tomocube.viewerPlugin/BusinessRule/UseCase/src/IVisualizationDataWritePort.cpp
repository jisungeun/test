#include "IVisualizationDataWritePort.h"

namespace TomoAnalysis::Viewer::UseCase {
	IVisualizationDataWritePort::IVisualizationDataWritePort() = default;
	IVisualizationDataWritePort::~IVisualizationDataWritePort() = default;
}