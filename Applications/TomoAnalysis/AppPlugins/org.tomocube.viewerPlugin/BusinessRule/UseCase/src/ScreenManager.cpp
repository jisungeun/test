#include <SceneStorage.h>
#include <ImageLoader.h>

#include "ScreenManager.h"

namespace TomoAnalysis::Viewer::UseCase {
	ScreenManager::ScreenManager() = default;
	ScreenManager::~ScreenManager() = default;

	auto ScreenManager::SetVisibleBoundaryBox(bool visible, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

	    auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if (!scene.get()) {
			return false;
		}

		scene->SetVisibleBoundaryBox(visible);
		scene->SetVisibleAxisGrid(false);

		port->Update(scene);

		return true;
	}
	auto ScreenManager::SetVisibleOrientationMarker(bool visible, const Entity::Scene::ID& sceneId, ISceneOutputPort* port) -> bool {
		if(nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if(!scene.get()) {
			return false;
		}
		scene->SetVisibleOrientationMarker(visible);

		port->Update(scene);

		return true;
    }
	auto ScreenManager::SetVisibleTimeStamp(bool visible, const Entity::Scene::ID& sceneId, ISceneOutputPort* port) -> bool {
		if (nullptr == port) {
			return false;
		}
		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if(!scene.get()) {
			return false;
		}
		scene->SetVisibleTimeStamp(visible);

		port->Update(scene);					

	    return true;
    }

	auto ScreenManager::SetTimeStampColor(int r, int g, int b, const Entity::Scene::ID& sceneId, ISceneOutputPort* port) -> bool {
		if(nullptr == port) {
			return false;
		}
		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if(!scene.get()) {
			return false;
		}
		scene->SetTimeStampColor(r, g, b);

		port->Update(scene);

		return true;
    }

	auto ScreenManager::SetTimeStampSize(int size, const Entity::Scene::ID& sceneId, ISceneOutputPort* port) -> bool {
		if(nullptr == port) {
			return false;
		}
		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if(!scene.get()) {
			return false;
		}
		scene->SetTimeStampSize(size);

		port->Update(scene);

		return true;
    }


	auto ScreenManager::SetVisibleAxisGrid(bool visible, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

	    auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if (!scene.get()) {
			return false;
		}

		scene->SetVisibleAxisGrid(visible);
		scene->SetVisibleBoundaryBox(false);

		port->Update(scene);

		return true;
	}

	auto ScreenManager::SetAxisGridFontSize(int size, const Entity::Scene::ID& sceneId, ISceneOutputPort* port) -> bool {
        if(nullptr == port) {
			return false;
        }
		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if(!scene.get()) {
			return false;
		}

		scene->SetAxisGridFontSize(size);

		port->Update(scene);

		return true;
    }

	auto ScreenManager::SetAxisGridColor(int axis, int r, int g, int b, const Entity::Scene::ID& sceneId, ISceneOutputPort* port) -> bool {
        if(nullptr == port) {
			return false;
        }
		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if(!scene.get()) {
			return false;
		}

		scene->SetAxisGridColor(axis,r,g,b);

		port->Update(scene);

		return true;
    }

	auto ScreenManager::SetResolution(int resolution,bool forVolume, const Entity::Scene::ID& sceneId, ISceneOutputPort* port) -> bool {
        if(nullptr == port) {
			return false;
        }

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if(!scene.get()) {
			return false;
		}
		scene->SetResolution(resolution,forVolume);

		port->Update(scene);

		return true;
    }

	auto ScreenManager::SetEnableOverlay(bool enable, const Entity::Scene::ID& sceneId, ISceneOutputPort* port)->bool {
		if (nullptr == port) {
			return false;
		}

		auto storage = Entity::SceneStorage::GetInstance();
		auto scene = storage->GetScene(sceneId);
		if (!scene.get()) {
			return false;
		}

		scene->SetEnableOverlay(enable);

		port->Update(scene);

		return true;
	}
}
