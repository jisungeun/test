#include "IVisualizationDataReadPort.h"

namespace TomoAnalysis::Viewer::UseCase {
	IVisualizationDataReadPort::IVisualizationDataReadPort() = default;
	IVisualizationDataReadPort::~IVisualizationDataReadPort() = default;
}