#pragma once

#include <memory>
#include <QWidget>

#include <INavigatorPanel.h>
#include "InterSegNavigatorPanelExport.h"

namespace TomoAnalysis::InterSeg::Plugins {
    class InterSegNavigatorPanel_API NavigatorPanel : public QWidget, public Interactor::INavigatorPanel {
        Q_OBJECT
        
    public:
        typedef NavigatorPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        NavigatorPanel(QWidget* parent=nullptr);
        ~NavigatorPanel();

        auto Update()->bool override;
        auto Refresh()->bool override;
        auto RefreshWithCall()->void;

        auto Init(void) const ->bool;
        auto Reset(void) ->bool;

    signals:
        void positionChanged(int x, int y, int z);
        void phyPositionChanged(float x, float y, float z);

    protected slots:
        void on_xPhySpinBox_valueChanged(double value);
        void on_yPhySpinBox_valueChanged(double value);
        void on_zPhySpinBox_valueChanged(double value);

        void on_xAxisSlider_valueChanged(int value);
        void on_xAxisSpinBox_valueChanged(int value);
        void on_yAxisSlider_valueChanged(int value);
        void on_yAxisSpinBox_valueChanged(int value);
        void on_zAxisSlider_valueChanged(int value);
        void on_zAxisSpinBox_valueChanged(int value);

        //void onCurrentModalityChanged(Entity::Modality modality);

        void onPositionChanged(int axis, int value);

    private:
        void NotifyChangedAxisSpinBox();
        void NotifyChangedAxisSlider();
        void NotifyChangedPhySpinBox();

        auto SetEnableUI(const bool& enable) const ->void;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}