#pragma once

#include <memory>

#include <QWidget>

#include <IRenderWindowWidget.h>
#include <ScreenShotWidget.h>

#include "InterSegRenderWindowExport.h"

class SoPerspectiveCamera;
class SoSeparator;

namespace TomoAnalysis::InterSeg::Plugins {
    class InterSegRenderWindow_API RenderWindow : public QWidget, public Interactor::IRenderWindowWidget {
        Q_OBJECT
    public:
        typedef RenderWindow Self;
        typedef std::shared_ptr<Self> Pointer;

        RenderWindow(QWidget* parent = nullptr);
        ~RenderWindow();

        auto ChangeLayout() -> void override;
        auto Update() -> void override;

        //Non-Clean Architecture
        auto Clear()->void;
        auto Reset()->void;
        auto Set3DSceneGraph(SoSeparator* root)->void;
        auto Set2DSceneGraph(SoSeparator* root2d,int idx)->void;
        auto Get3dCam()->SoPerspectiveCamera*;
        auto ConnectScreenShotWidget(TC::ScreenShotWidget::Pointer widget)->void;
        auto GetCurrentLayout()->int;
        auto SetNavigation(bool isNavi) -> void override;
        auto SetHTMinMax(double min, double max)->void;

    signals:
        void sliceIndexChanged(int viewIndex, int sliceIndex);
        void slicePicking(float x, float y, float z,int sliceIndex);

    protected slots:
        void onMoveSlice(float delta);
        void onPicking(float x, float y, float z);
        void onStartDrawing();
        void onFinishDrawing();

    private:
        auto Init()->void;        

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}