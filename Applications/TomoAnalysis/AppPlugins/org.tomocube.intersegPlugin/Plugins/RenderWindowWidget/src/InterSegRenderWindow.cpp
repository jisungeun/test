#include <QSplitter>
#include <QHBoxLayout>
#include <QLayout>

#pragma warning(push)
#pragma warning(disable:4819)
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/ViewerComponents/nodes/SoViewingCube.h>
#include <Medical/helpers/MedicalHelper.h>
#pragma warning(pop)

#include <InterSegRenderWindow2D.h>
#include <InterSegRenderWindow3D.h>

#include "InterSegRenderWindow.h"



namespace TomoAnalysis::InterSeg::Plugins {
    struct RenderWindow::Impl {
        // ui
        QSplitter* mainSplitter;
        QSplitter* subSplitter;    // sub render window 그룹간 splitter
        QWidget* subContainer;  // sub render window와 splitter를 포함하는 widget

        int order_idx{ 0 };
        QWidget* winContainer[4];
        QWidget* winSocket[4];

        InterSegRenderWindow2D* renderwindow2d[3];
        InterSegRenderWindow3D* renderwindow3d;
        
        // render
        SoVolumeData* curImage{nullptr};
        SoVolumeData* curMask{nullptr};        
    };

    RenderWindow::RenderWindow(QWidget* parent)
    : QWidget(parent)
    , d{ new Impl } {
        Init();
    }

    RenderWindow::~RenderWindow() {        
    }  

    auto RenderWindow::ConnectScreenShotWidget(TC::ScreenShotWidget::Pointer widget) -> void {       
        widget->SetRenderWindow(d->renderwindow3d, "threeD");
        widget->SetRenderWindow(d->renderwindow2d[0], "XY");
        widget->SetRenderWindow(d->renderwindow2d[1], "YZ");
        widget->SetRenderWindow(d->renderwindow2d[2], "XZ");

        widget->SetMultiLayerType(TC::MultiLayoutType::BigXY);
    }
    auto RenderWindow::GetCurrentLayout() -> int {
        return d->order_idx;
    }

    auto RenderWindow::ChangeLayout() -> void {        
        //get order
        d->order_idx++;
        d->order_idx %= 4;              

        const auto new_order = WinOrder[d->order_idx];        
        for(auto i=0;i<4;i++) {            
            //lay->addWidget(d->winContainer[new_order[i]]);
            auto lay = d->winSocket[i]->layout();
            lay->removeItem(lay->takeAt(0));

            lay->addWidget(d->winContainer[new_order[i]]);
        }
    }

    void RenderWindow::onStartDrawing() {        
        d->renderwindow3d->setNavi(false);
    }

    void RenderWindow::onFinishDrawing() {        
        d->renderwindow3d->setNavi(true);
    }
    auto RenderWindow::SetNavigation(bool isNavi) -> void {
        for(auto i=0;i<3;i++) {
            if(d->renderwindow2d[i]) {
                d->renderwindow2d[i]->SetInteractionMode(isNavi);
            }
        }
        if(d->renderwindow3d) {
            d->renderwindow3d->setNavi(!isNavi);
        }
    }
    void RenderWindow::onPicking(float x, float y, float z) {
        auto eventSender = dynamic_cast<InterSegRenderWindow2D*>(sender());
        auto id = eventSender->GetID();
        emit slicePicking(x, y, z,id);
    }

    void RenderWindow::onMoveSlice(float delta) {
        auto eventSender = dynamic_cast<InterSegRenderWindow2D*>(sender());

        // event 발생한 render window의 slice 정보 획득
        auto orthoSlice = MedicalHelper::find<SoOrthoSlice>(eventSender->getSceneGraph(), "ImageSlice");
        if (orthoSlice == nullptr) {
            return;
        }

        int sliceIndex, viewIndex;
        sliceIndex = viewIndex = -1;

        viewIndex = orthoSlice->axis.getValue();
        sliceIndex = orthoSlice->sliceNumber.getValue();

        if (delta < 0) {
            sliceIndex--;
        }
        else if (delta > 0) {
            sliceIndex++;
        }
        else {
            return;
        }

        emit sliceIndexChanged(viewIndex, sliceIndex);
    }

    auto RenderWindow::Init() -> void {
        //init render windows
        for (auto i = 0; i < 4; i++) {
            d->winContainer[i] = new QWidget(this);
            auto lay = new QVBoxLayout;
            lay->setContentsMargins(0, 0, 0, 0);
            lay->setSpacing(0);
            d->winContainer[i]->setLayout(lay);

            d->winSocket[i] = new QWidget;
            auto slay = new QVBoxLayout;
            slay->setContentsMargins(0, 0, 0, 0);
            slay->setSpacing(0);
            d->winSocket[i]->setLayout(slay);
        }

        d->renderwindow3d = new InterSegRenderWindow3D(nullptr);        
        for (auto i = 0; i < 3; i++) {
            d->renderwindow2d[i] = new InterSegRenderWindow2D(nullptr);            
            auto n2 = QString("TwoD%1").arg(i);
            d->renderwindow2d[i]->setName(n2);            
            connect(d->renderwindow2d[i], SIGNAL(sigWheel(float)), this, SLOT(onMoveSlice(float)));
            connect(d->renderwindow2d[i], SIGNAL(sig2dCoord(float,float,float)), this, SLOT(onPicking(float,float,float)));
        }

        d->renderwindow2d[0]->setRenderWindowID(0);
        d->renderwindow2d[1]->setRenderWindowID(1);
        d->renderwindow2d[2]->setRenderWindowID(2);
        d->winContainer[0]->layout()->addWidget(d->renderwindow2d[0]);
        d->winContainer[1]->layout()->addWidget(d->renderwindow3d);
        d->winContainer[2]->layout()->addWidget(d->renderwindow2d[1]);
        d->winContainer[3]->layout()->addWidget(d->renderwindow2d[2]);

        d->renderwindow2d[0]->setWindowTitle("XY");
        d->renderwindow2d[0]->setWindowTitleColor(0.8f, 0.2f, 0.2f);
        d->renderwindow2d[1]->setWindowTitle("YZ");
        d->renderwindow2d[1]->setWindowTitleColor(0.2f, 0.8f, 0.2f);
        d->renderwindow2d[2]->setWindowTitle("XZ");
        d->renderwindow2d[2]->setWindowTitleColor(0.2f, 0.2f, 0.8f);
        d->renderwindow3d->setWindowTitle("3D");
        d->renderwindow3d->setWindowTitleColor(0.8f, 0.8f, 0.2f);

        for (auto i = 0; i < 4; i++) {
            d->winSocket[i]->layout()->addWidget(d->winContainer[i]);
        }

        //set layout
        auto layout = new QHBoxLayout(nullptr);
        d->mainSplitter = new QSplitter;
        d->mainSplitter->setOrientation(Qt::Orientation::Horizontal);

        d->subSplitter = new QSplitter;
        d->subSplitter->setOrientation(Qt::Orientation::Vertical);
        for (auto i = 0; i < 3; i++) {
            d->subSplitter->addWidget(d->winSocket[i + 1]);
        }

        d->subSplitter->setStretchFactor(0, 1);
        d->subSplitter->setStretchFactor(1, 1);
        d->subSplitter->setStretchFactor(2, 1);

        d->subContainer = new QWidget;
        auto subLayout = new QVBoxLayout;
        subLayout->setContentsMargins(0, 0, 0, 0);
        subLayout->setSpacing(0);
        subLayout->addWidget(d->subSplitter);
        d->subContainer->setLayout(subLayout);

        d->mainSplitter->addWidget(d->winSocket[0]);
        d->mainSplitter->addWidget(d->subContainer);

        QList<int> mainSizes;
        mainSizes << 600 << 200;

        d->mainSplitter->setSizes(mainSizes);
        //d->mainSplit->setStretchFactor(0,0);
        //d->mainSplit->setStretchFactor(1, 1);

        layout->addWidget(d->mainSplitter);

        this->setLayout(layout);
    }

    //Non-Clean Architecture
    auto RenderWindow::Reset() -> void {
        for (auto i = 0; i < 3; i++) {
            if (d->renderwindow2d[i]) {
                d->renderwindow2d[i]->reset2DView();
                d->renderwindow2d[i]->viewall();
            }
        }

        if (d->renderwindow3d) {
            d->renderwindow3d->reset3DView();
            d->renderwindow3d->viewall();
        }
    }

    auto RenderWindow::Set2DSceneGraph(SoSeparator* root2d, int idx) -> void {
        if(nullptr!= d->renderwindow2d[idx]) {            
            d->renderwindow2d[idx]->setSceneGraph(root2d);            
            //d->renderwindow2d[idx]->refreshRangeSlider();
        }        
    }
    auto RenderWindow::SetHTMinMax(double min, double max) -> void {
        for(auto i=0;i<3;i++) {
            if(nullptr != d->renderwindow2d[i]) {
                d->renderwindow2d[i]->setHTRange(min, max);
            }
        }
    }
    auto RenderWindow::Set3DSceneGraph(SoSeparator* root) -> void {
        if (nullptr != d->renderwindow3d) {            
            d->renderwindow3d->setSceneGraph(root);
            auto cube = MedicalHelper::find<SoViewingCube>(root, "ViewCube");
            if(nullptr != cube) {
                cube->sceneCamera = d->renderwindow3d->getCamera();
            }
        }
    }
    auto RenderWindow::Get3dCam() -> SoPerspectiveCamera* {
        return (SoPerspectiveCamera*)d->renderwindow3d->getCamera();
    }

    auto RenderWindow::Clear() -> void {
        for(auto i=0;i<3;i++) {
            if(d->renderwindow2d[i]) {
                d->renderwindow2d[i]->setSceneGraph(new SoSeparator);
            }
        }
        if(d->renderwindow3d) {
            d->renderwindow3d->setSceneGraph(new SoSeparator);
        }
    }
    auto RenderWindow::Update() -> void {
        for (auto i = 0; i < 3; i++) {
            d->renderwindow2d[i]->reset2DView();            
        }
        d->renderwindow3d->reset3DView();        
    }
}