#include <TCMaskWriter.h>
#include <TCHMaskWriter.h>
#include <TCMaskWriterPort.h>

#include "MaskDataWriter.h"

namespace TomoAnalysis::InterSeg::Plugins::ImageDataIO {
    struct MaskDataWriter::Impl {
        QStringList layer_names;
    };
    MaskDataWriter::MaskDataWriter() : d{ new Impl } {
        
    }
    MaskDataWriter::~MaskDataWriter() {
        
    }
    auto MaskDataWriter::SetLayerNames(QStringList name) -> void {        
        d->layer_names = name;
    }
    auto MaskDataWriter::AppendLayer(TCMask::Pointer src, TCMask::Pointer dst) -> void {
        //append src information to dst
        auto srcLayers = src->GetLayerNames();
        auto dstLayers = dst->GetLayerNames();
        for (auto sl : srcLayers) {
            if (dstLayers.contains(sl)) {
                auto srcVolume = src->GetLayerBlob(sl);                
                dst->AppendLayerVolume(sl, srcVolume.GetMaskVolume());
                break;
            }
        }
    }
    auto MaskDataWriter::Modify(TCMask::Pointer data, const QString& path) -> bool {
        TC::IO::TCMaskWriterPort writer;

        auto tcmask = std::dynamic_pointer_cast<TCMask>(data);
        auto names = tcmask->GetLayerNames();

        auto success = true;

        for (auto i = 0; i < names.count(); i++) {
            success &= writer.Write(tcmask, path, "HT", names[i], 0);//TODO
        }

        return success;
        
        /*TC::IO::TCHMaskWriter writer(path);
        auto time_idx = data->GetTimeStep();
        const auto indexes = data->GetBlobIndexes();        
        writer.ClearBlobCount("HT", time_idx);        
        writer.ClearWhole("HT", time_idx);        
        auto names = data->GetLayerNames();
        for (auto i = 0; i < indexes.size(); i++) {            
            auto index = indexes[i];            
            
            auto blob = data->GetBlob(index);
            auto inBox = blob.GetBoundingBox();
            auto code = blob.GetCode();

            TC::IO::BoundingBox bbox;
            bbox.SetOffset(std::get<0>(inBox), std::get<1>(inBox), std::get<2>(inBox));
            bbox.SetSize(std::get<3>(inBox) - std::get<0>(inBox) + 1,
                std::get<4>(inBox) - std::get<1>(inBox) + 1,
                std::get<5>(inBox) - std::get<2>(inBox) + 1);
            if (!writer.WriteBlob("HT", time_idx, index,  bbox, code)) return false;
            //if (!writer.UpdateBlob("HT", time_idx, index, bbox, code)) return false;

            auto maskVolume = blob.GetMaskVolume();            
            if (!writer.WriteMask("HT", time_idx, index, bbox, maskVolume)) return false;
            //if (!writer.UpdateMask("HT", time_idx, index, bbox, maskVolume)) return false;
        }
        return true;*/
    }

    auto MaskDataWriter::Write(TCMask::Pointer data, const QString& path) -> bool {        
        TC::IO::TCMaskWriterPort writer;

        auto tcmask = std::dynamic_pointer_cast<TCMask>(data);
        auto names = tcmask->GetLayerNames();

        auto success = true;

        for (auto i = 0; i < names.count(); i++) {
            success &= writer.Write(tcmask, path, "HT", names[i], 0);//TODO
        }

        return success;

        /*
        TC::IO::TCHMaskWriter writer(path);
        auto time_idx = data->GetTimeStep();   
        const auto indexes = data->GetBlobIndexes();        
        writer.ClearBlobCount("HT", time_idx);

        auto names = data->GetLayerNames();

        if (data->GetType()._to_integral() == MaskTypeEnum::MultiLabel) {
            for (auto i = 0; i < d->layer_names.size(); i++) {
                writer.WriteName("HT", i, d->layer_names[i]);
            }
        }

        for(auto i=0;i<indexes.size();i++){
            auto index = indexes[i];                        
            if (data->GetType()._to_integral() == MaskTypeEnum::MultiLayer) {
                auto skip = true;
                for (auto r = 0; r < d->layer_names.size(); r++) {
                    if (d->layer_names[r].compare(names[index]) == 0) {
                        skip = false;
                        writer.WriteName("HT", index, d->layer_names[r]);
                        break;
                    }
                }
                if (skip) {
                    continue;
                }
            }            

            auto blob = data->GetBlob(index);
            auto inBox = blob.GetBoundingBox();
            auto code = blob.GetCode();

            TC::IO::BoundingBox bbox;
            bbox.SetOffset(std::get<0>(inBox), std::get<1>(inBox), std::get<2>(inBox));
            bbox.SetSize(std::get<3>(inBox) - std::get<0>(inBox) + 1,
                std::get<4>(inBox) - std::get<1>(inBox) + 1,
                std::get<5>(inBox) - std::get<2>(inBox) + 1);            
            //if (!writer.WriteBlob("HT", time_idx, real_idx,  bbox, code)) return false;
            if (!writer.WriteBlob("HT", time_idx, index, bbox, code)) return false;

            auto maskVolume = blob.GetMaskVolume();            
            //if (!writer.WriteMask("HT", time_idx, real_idx, bbox, maskVolume)) return false;
            if (!writer.WriteMask("HT", time_idx, index, bbox, maskVolume)) return false;
        }
        const auto size = data->GetSize();
        writer.WriteSize(std::get<0>(size), std::get<1>(size), std::get<2>(size));
        const auto res = data->GetResolution();
        writer.WriteResolution(std::get<0>(res), std::get<1>(res), std::get<2>(res));
        writer.WriteVersion(1, 0, 0);
        return true;
        */
    }
}