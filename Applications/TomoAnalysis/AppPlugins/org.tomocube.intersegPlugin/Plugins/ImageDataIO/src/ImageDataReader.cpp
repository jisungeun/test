#include <QStringList>

#include <OivHdf5Reader.h>
#include <TCFMetaReader.h>
#include <TCFSimpleReader.h>
#include <OivLdmReader.h>

#include "ImageDataReader.h"

#include "TCImageReader.h"
using namespace TC::IO;

namespace TomoAnalysis::InterSeg::Plugins::ImageDataIO {
    
    ImageDataReader::ImageDataReader() {

    }
    ImageDataReader::~ImageDataReader() {
        
    }
    auto ImageDataReader::Read(const QString& path, const bool loadImage,int time_step) const -> TCImage::Pointer {
        if (loadImage) {
            auto metaReader = new TCFMetaReader;
            auto meta = metaReader->Read(path);
            auto vers = meta->common.formatVersion.split(".");
            auto minor_ver = vers[1].toInt();
            auto isFloat = (minor_ver < 3);
            auto isLDM = meta->data.isLDM;

            std::shared_ptr<TCImage> img(new TCImage, std::default_delete<TCImage>());

            if(isLDM) {
                OivLdmReader* reader = new OivLdmReader;
                reader->ref();
                reader->setDataGroupPath("/Data/3D");
                reader->setTileDimension(meta->data.data3D.tileSizeX, meta->data.data3D.tileSizeY, meta->data.data3D.tileSizeZ);
                QString tileName = QString("%1").arg(time_step, 6, 10, QLatin1Char('0'));
                reader->setFilename(path.toStdString());
                reader->setTileName(tileName.toStdString());
                SoVolumeData* vol = new SoVolumeData;
                vol->ref();
                vol->texturePrecision = 16;
                vol->ldmResourceParameters.getValue()->tileDimension.setValue(meta->data.data3D.tileSizeX, meta->data.data3D.tileSizeY, meta->data.data3D.tileSizeZ);
                vol->setReader(*reader, TRUE);
                reader->touch();

                auto dim = vol->getDimension();
                auto res = vol->getVoxelSize();

                auto cnt = dim[0] * dim[1] * dim[2];

                std::shared_ptr<unsigned short> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());

                SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));
                SoLDMDataAccess::DataInfoBox dataInfoBox =
                    vol->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);

                SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
                dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

                vol->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());

                unsigned short* ptr = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_ONLY);

                memcpy(arr.get(), ptr, cnt * sizeof(unsigned short));

                dataBufferObj->unmap();

                img->SetImageVolume(arr);

                double rres[3];
                for (auto i = 0; i < 3; i++) {
                    rres[i] = res[i];
                }

                img->SetBoundingBox(0, 0, 0, dim[0] - 1, dim[1] - 1, dim[2] - 1);
                img->SetResolution(rres);

                while (vol->getRefCount() > 0) {
                    vol->unref();
                }
                vol = nullptr;
                while (reader->getRefCount() > 0) {
                    reader->unref();
                }
                reader = nullptr;
            }else {
                auto reader = new TCFSimpleReader;
                reader->SetFilePath(path);
                auto result = reader->ReadHT3D(time_step, isFloat);
                auto dim = result.GetDimension();
                auto res = result.GetResolution();
                auto cnt = std::get<0>(dim) * std::get<1>(dim) * std::get<2>(dim);

                std::shared_ptr<unsigned short> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());
                if (isFloat) {
                    auto base = result.GetDataAsDouble();
                    for (unsigned i = 0; i < cnt; i++) {
                        arr.get()[i] = base.get()[i] * 10000.0;
                    }
                }
                else {
                    auto base = result.GetDataAsUInt16();
                    memcpy(arr.get(), base.get(), cnt * sizeof(unsigned short));
                }

                img->SetImageVolume(arr);

                double rres[3];
                rres[0] = std::get<0>(res); rres[1] = std::get<1>(res); rres[2] = std::get<2>(res);
                img->SetBoundingBox(0, 0, 0, std::get<0>(dim) - 1, std::get<1>(dim) - 1, std::get<2>(dim) - 1);
                img->SetResolution(rres);
                img->SetTimeStep(time_step);
                auto range = result.GetRange();
                int min = std::get<0>(range) * 10000;
                int max = std::get<1>(range) * 10000;
                img->SetMinMax(min, max);
                delete reader;
            }

            
            delete metaReader;            
            return img;
        }
        return nullptr;
    }
    auto ImageDataReader::ReadFL(const QString& path, int ch, int time_step) const -> TCImage::Pointer {
        //auto metaReader = new TCFMetaReader;
        //auto meta = metaReader->Read(path);

        //std::shared_ptr<TCImage> img(new TCImage, std::default_delete<TCImage>());

        //auto reader = new TCFSimpleReader;
        auto reader = new TCImageReader;
        auto img = reader->ReadFL(path, ch, time_step);
        /*auto fimg = reader->ReadFL(path, ch, time_step);
        //reader->SetFilePath(path);
        //auto result = reader->ReadFL3D(ch, time_step);
        auto dim = img->GetSize();// result.GetDimension();
        double rres[3];
        img->GetResolution(rres);//result.GetResolution();
        auto cnt = std::get<0>(dim) * std::get<1>(dim) * std::get<2>(dim);

        std::shared_ptr<unsigned short> arr(new unsigned short[cnt], std::default_delete<unsigned short[]>());
        //auto base = result.GetDataAsUInt16();
        auto base = static_cast<unsigned short*>(img->GetBuffer());
        memcpy(arr.get(), base, cnt * sizeof(unsigned short));

        img->SetImageVolume(arr);

        //double rres[3];
        //rres[0] = std::get<0>(res); rres[1] = std::get<1>(res); rres[2] = std::get<2>(res);
        img->SetBoundingBox(0, 0, 0, std::get<0>(dim) - 1, std::get<1>(dim) - 1, std::get<2>(dim) - 1);
        img->SetResolution(rres);
        img->SetTimeStep(time_step);

        //auto range = result.GetRange();

        int min = meta->data.data3DFL.minIntensity;// std::get<0>(range);
        int max = meta->data.data3DFL.maxIntensity;// std::get<1>(range);
        img->SetMinMax(min, max);
        delete metaReader;
        delete reader;*/
        return img;
    }
    auto ImageDataReader::ReadMeta(const QString& path) const -> TC::IO::TCFMetaReader::Meta::Pointer {
        auto metaReader = new TCFMetaReader;
        auto meta = metaReader->Read(path);
        return meta;
    }

}