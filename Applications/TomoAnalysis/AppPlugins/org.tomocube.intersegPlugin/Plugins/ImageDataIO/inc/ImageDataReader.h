#pragma once

#include <IImageReaderPort.h>

#include "ImageDataIOExport.h"

namespace TomoAnalysis::InterSeg::Plugins::ImageDataIO {
    class ImageDataIO_API ImageDataReader : public UseCase::IImageReaderPort {
    public:
        ImageDataReader();
        ~ImageDataReader();

        auto Read(const QString& path, const bool loadImage = false,int time_step =0) const -> TCImage::Pointer override;
        auto ReadFL(const QString& path, int ch, int time_step) const -> TCImage::Pointer override;
        auto ReadMeta(const QString& path) const -> TC::IO::TCFMetaReader::Meta::Pointer override;
    private:

    };
}