#pragma once

#include <IMaskWriterPort.h>

#include "ImageDataIOExport.h"

namespace TomoAnalysis::InterSeg::Plugins::ImageDataIO {
    class ImageDataIO_API MaskDataWriter : public UseCase::IMaskWriterPort {
    public:
        MaskDataWriter();
        ~MaskDataWriter();

        auto SetLayerNames(QStringList name) -> void override;
        auto Write(TCMask::Pointer data, const QString& path) -> bool override;
        auto Modify(TCMask::Pointer data, const QString& path) -> bool override;
        auto AppendLayer(TCMask::Pointer src, TCMask::Pointer dst) -> void override;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}