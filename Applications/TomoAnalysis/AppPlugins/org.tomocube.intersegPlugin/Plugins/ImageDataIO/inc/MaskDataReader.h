#pragma once

#include <IMaskReaderPort.h>

#include "ImageDataIOExport.h"

namespace TomoAnalysis::InterSeg::Plugins::ImageDataIO {
    class ImageDataIO_API MaskDataReader : public UseCase::IMaskReaderPort {
    public:
        MaskDataReader();
        ~MaskDataReader();

        auto Read(const QString& path, int time_idx, const bool loadMaskVolume, const bool islabel) const -> TCMask::Pointer override;
        auto Create(int x0, int y0, int z0, int x1, int y1, int z1) -> TCMask::Pointer override;
        auto Create(TCImage::Pointer image, QString mask_name) -> TCMask::Pointer override;        
        auto CreateLayer(TCMask::Pointer targetMask, QString mask_name) -> void override;

    private:
        auto isBinaryMask(QString name) ->bool;
        auto ReadOld(const QString& path, int time_idx, const bool loadMaskVolume, const bool islabel) const->TCMask::Pointer;
    };
}