//!oiv_include <VolumeViz/vvizGetData_frag.h>
//!oiv_include <VolumeViz/vvizfnc_frag.h>
//!oiv_include <VolumeViz/vvizTransferFunction_frag.h>

uniform VVizDataSetId data1;
uniform int transfer;

vec4 VVizComputeFragmentColor(VVizDataSetId data, vec3 rayDir, inout VVizVoxelInfo voxelInfoFront, in VVizVoxelInfo voxelInfoBack, int maskId)
{
    vec3 texCoord = voxelInfoFront.texCoord;
    vec3 texCoordBack = voxelInfoBack.texCoord;

    VVIZ_DATATYPE index1 = VVizGetData(data1, texCoord);
    VVIZ_DATATYPE index_back = VVizGetData(data1, texCoordBack);    
    vec3 normal, gradient;
    VVizComputeGradientCommon(data1, voxelInfoFront.texCoord, voxelInfoBack.texCoord, gradient, normal);

    float gradientValue = length(gradient);

    vec4 color = vec4(0, 0, 0, 0);
    if (gradientValue > 0) {
        if (index1 > index_back) {            
            color = VVizTransferFunction(index1, transfer);            
        }
        else {// # for option 1 layered gardient
            color = VVizTransferFunction(index_back, transfer);
        }
    }
    color = VVizComputeVolumeRenderingLighting(color, normal, gradientValue);
    return color;
}
