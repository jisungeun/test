#include <QCoreApplication>
#include <QMap>

#pragma warning(push)
#pragma warning(disable:4819)
//Open Inventor
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoInfo.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/Axis.h>
#include <Inventor/ViewerComponents/nodes/SoViewingCube.h>

#include <VolumeViz/nodes/SoVolumeData.h>
#include <VolumeViz/nodes/SoOrthoSlice.h>
#include <VolumeViz/nodes/SoVolumeRender.h>
#include <VolumeViz/nodes/SoVolumeRenderingQuality.h>
#include <VolumeViz/nodes/SoDataRange.h>
#include <VolumeViz/nodes/SoVolumeIsosurface.h>
#include <Inventor/nodes/SoShape.h>
#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoResetImageProcessing.h>
#include <ImageViz/Engines/ImageAnalysis/Statistics/SoMaskedStatisticsQuantification.h>
#include <ImageViz/Engines/ImageManipulation/ImageEditing/SoConvertImageProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoCombineByMaskProcessing.h>
#include <ImageViz/Engines/ImageAnalysis/Statistics/SoIntensityStatisticsQuantification.h>
#include <ImageViz/Engines/ImageSegmentation/Labeling/SoReorderLabelsProcessing.h>
#include <ImageViz/Engines/MathematicalMorphology/ErosionAndDilation/SoErosionCubeProcessing.h>
#include <ImageViz/Engines/ImageSegmentation/SeparatingAndFilling/SoExpandLabelsProcessing.h>

#include <ImageViz/Engines/ArithmeticAndLogic/LogicalOperations/SoLogicalNotProcessing.h>
#include <ImageViz/Engines/MathematicalMorphology/ErosionAndDilation/SoErosionBallProcessing3d.h>
#include <ImageViz/Engines/MathematicalMorphology/ErosionAndDilation/SoDilationBallProcessing3d.h>
#include <ImageViz/Engines/ImageSegmentation/Binarization/SoThresholdingProcessing.h>
#include <ImageViz/Engines/ImageSegmentation/RegionGrowing/SoMarkerBasedWatershedProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoArithmeticValueProcessing.h>
#include <ImageViz/Engines/ArithmeticAndLogic/ArithmeticOperations/SoMaskImageProcessing.h>
#include <ImageViz/Engines/ImageAnalysis/IndividualMeasures/SoLabelAnalysisQuantification.h>
#include <ImageViz/Nodes/Images/SoMemoryDataAdapter.h>
#include <ImageViz/Nodes/Images/SoVRImageDataReader.h>
#include <ImageViz/Nodes/Measures/SoDataMeasurePredefined.h>

#include <Medical/Helpers/MedicalHelper.h>
#include <Medical/helpers/VolumeMarchingCubes.h>
#pragma warning(pop)

#include <TCDataConverter.h>
#include <TCAiInteractiveSeg.h>

#include <Oiv2DDrawer.h>
#include <Oiv3DDrawer.h>
#include <OivFreeLine.h>
#include <OivMaskGenerator.h>
#include <OivScaleBar.h>

#include "SceneManager.h"

namespace TomoAnalysis::InterSeg::Plugins {
    float color_table[20][3]{
	230,25,75, //red
	60,180,75, //green
	255,255,25, //yellow
	0,130,200, //blue
	245,130,48, //Orange
	145,30,180, //Purple
	70,240,240, //Cyan
	240,50,230, //Magenta
	210,245,60, //Lime
	250,190,212, //Pink
	0,128,128, //Teal
	220,190,255, //Lavender
	170,110,40, //Brown
	255,250,200, //Beige
	128,0,0,//Maroon
	170,255,195, //Mint
	128,128,0, //Olive
	255,215,180, //Apricot
	0,0,128, //Navy
	128,128,128 //Gray
	};

    struct SceneManager::Impl {
        int curTime{ -1 };
        QString curImageName{ QString() };
        SoVolumeData* curImage{ nullptr };
        QString curMaskName{ QString() };
        SoVolumeData* curMask{ nullptr };//working file
        SoVolumeData* mergerTarget{nullptr};
        SoVolumeData* sizeTarget{ nullptr };
        SoVolumeData* curFL{ nullptr };        
        SoSwitch* imageSocket;
        SoSwitch* maskSocket;                

        SoInfo* htMinInfo;
        SoInfo* htMaxInfo;

        //SceneGraphs
        //3D
        SoSeparator* root3d;
        SoSeparator* volRenSep;
        SoSeparator* volRenSepMask;
        SoSeparator* modelSepMask;
        SoSeparator* volRenSepMerge;
        SoSeparator* volRenSepSize;

        SoVolumeRenderingQuality* volQual;
        SoVolumeRender* volRender;
        SoVolumeRenderingQuality* volQualMask;
        SoFragmentShader* maskFrag{ nullptr };
        SoVolumeRender* volRenderMask;
        SoMaterial* matlIsoMask;
        SoVolumeIsosurface* volIsoMask;        
        SoVolumeRenderingQuality* volQualMerge;
        SoVolumeRender* volRenderMerge;

        SoVolumeRenderingQuality* volQualSize;
        SoVolumeRender* volRenderSize;

        VolumeMarchingCubes* marching;

        //2Ds
        SoSeparator* root2d[3];
        SoSwitch* tempViz[3];
        SoSeparator* orthoSep[3];
        SoSeparator* orthoSepMask[3];
        SoSeparator* orthoSepMerge[3];
        SoSeparator* orthoSepFL[3];
        SoOrthoSlice* orthoSlice[3];
        SoOrthoSlice* orthoSliceMask[3];
        SoOrthoSlice* orthoSliceMerge[3];
        SoOrthoSlice* orthoSliceFL[3];

        SoSwitch* scaleBarSwitch[3] = { nullptr, };

        //2D drawers
        SoSeparator* drawerSep[3];
        Oiv2DDrawer* drawer[3];
        uint32_t paintSize { 5 };
        uint32_t wipeSize { 5 };

        //2D drawer on 3d
        SoSeparator* drawerSep3d{ nullptr };
        Oiv3DDrawer* drawer3d{ nullptr };
        //Sharable        

        SoGroup* imageGroup;
        SoMaterial* matlImage;
        SoTransferFunction* tfImage;
        SoDataRange* dataRange;        

        SoGroup* maskGroup;
        SoMaterial* matlMaskVolume;
        SoMaterial* matlMaskSlice;
        SoMaterial* matlMerge;
        SoMaterial* matlSize;
        SoMaterial* matlFL;
        SoTransferFunction* tfMask;
        SoDataRange* dataRangeMask;
        SoDataRange* dataRangeTemp;

        SoSwitch* mergeSocket;
        SoGroup* mergeGroup;
        SoTransferFunction* tfMerge;
        SoDataRange* dataRangeMerge;

        SoSwitch* sizeSocket;
        SoGroup* sizeGroup;
        SoTransferFunction* tfSize;
        SoDataRange* dataRangeSize;

        SoSwitch* flSocket;
        SoGroup* flGroup;
        SoTransferFunction* tfFL;
        SoDataRange* dataRangeFL;
                
        SoSeparator* imageSliceSep[3];
        //slice volume holding socket
        SoSwitch* imageSliceSocket[3];
        //dataRangeMask
        SoTransferFunction* tfSlice;
        SoOrthoSlice* orthoSliceTemp[3];

        QList<int> editHistory;
        QList<int> undoHistory;

        SoSwitch* labelTextSwitch{ nullptr };

        bool isIsoSurface{ false };

        OivMaskGenerator* maskGenerator{ nullptr };
        TC::AI::TCAiInteractiveSeg* aiSegEngine{ nullptr };

        int curLevelMin, curLevelMax;

        bool isAddingPoint{ false };
        bool is2DPoint{ true };
        bool isPositive{ true };
        bool isAiInProgress{ false };
        int curAxis{ -1 };
        int curIndex{ -1 };
        int curLargeAxis{ 0 };
        int curIndices[3]{ 0, };
        int curLabelValue{ 1 };
    };
    SceneManager::SceneManager() : d{ new Impl } {
        //d->isIsoSurface = true;
        Init();        
        OivFreeLine::initClass();
    }
    SceneManager::~SceneManager() {
        delete d->maskGenerator;
        OivFreeLine::exitClass();
    }
    auto SceneManager::Get3DSceneGraph() -> SoSeparator* {
        return d->root3d;
    }
    auto SceneManager::Get2DSceneGraph(int idx) -> SoSeparator* {
        return d->root2d[idx];
    }

    auto SceneManager::SetSliceIndex(int viewIndex, int sliceIndex) -> void {
        SoOrthoSlice* imageSlice = nullptr;
        SoOrthoSlice* maskSlice = nullptr;
        SoOrthoSlice* mergeSlice = nullptr;
        SoOrthoSlice* flSlice = nullptr;                

        // view index(axis)에 해당하는 SoOrthoSlice 획득
        for (auto i = 0; i < 3; i++) {
            if (d->orthoSlice[i]->axis.getValue() == viewIndex) {
                imageSlice = d->orthoSlice[i];
                maskSlice = d->orthoSliceMask[i];
                mergeSlice = d->orthoSliceMerge[i];
                flSlice = d->orthoSliceFL[i];
                break;
            }
        }

        if (imageSlice == nullptr || maskSlice == nullptr) {
            return;
        }

        // slice index 범위 확인
        auto count = d->curImage->data.getSize()[viewIndex];
        if (sliceIndex < 0 || sliceIndex >= count) {
            return;
        }

        // 표시할 image & mask slice index 변경
        imageSlice->sliceNumber = maskSlice->sliceNumber = mergeSlice->sliceNumber = flSlice->sliceNumber= sliceIndex;
        d->curIndices[viewIndex] = sliceIndex;
    }
    auto SceneManager::convertFL(SoVolumeData* oriFL,float offset) -> void {
        Q_UNUSED(offset)
        d->curFL = new SoVolumeData;
        d->curFL->ref();

        auto dim = d->curImage->getDimension();        
        std::shared_ptr<unsigned short> volume(new unsigned short[dim[0] * dim[1] * dim[2]](), std::default_delete<unsigned short[]>());
        auto fl_dim = oriFL->getDimension();
        SbBox3i32 bBox(SbVec3i32(0, 0, 0), SbVec3i32(fl_dim[0] - 1, fl_dim[1] - 1, fl_dim[2] - 1));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            oriFL->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
        SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

        oriFL->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        unsigned short* fl_pointer = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_ONLY);

        for (auto k = 0; k < dim[2]; k++) {
            for (auto j = 0; j < dim[1]; j++) {
                for (auto i = 0; i < dim[0]; i++) {
                    SbVec3f index;
                    index[0] = i;
                    index[1] = j;
                    index[2] = k;
                    auto key = k * dim[0] * dim[1] + j * dim[0] + i;
                    auto phys = d->curImage->voxelToXYZ(index);
                    //phys[2] -= offset;
                    auto voxel = oriFL->XYZToVoxel(phys);
                    int vv[3] = { static_cast<int>(voxel[0]),static_cast<int>(voxel[1]),static_cast<int>(voxel[2]) };
                    if (vv[0] >= 0 && vv[0] < fl_dim[0]
                        && vv[1] >= 0 && vv[1] < fl_dim[1]
                        && vv[2] >= 0 && vv[2] < fl_dim[2]) {
                        int vKey = vv[2] * fl_dim[0] * fl_dim[1] + vv[1] * fl_dim[0] + vv[0];
                        auto val = *(fl_pointer + vKey);
                        *(volume.get() + key) = val;
                    }
                }
            }
        }
        /*
        for (auto k = 0; k < dim[2]; k++) {
            for (auto i = 0; i < dim[0]; i++) {
                for (auto j = 0; j < dim[1]; j++) {
                    SbVec3f index;
                    index[0] = i;
                    index[1] = j;
                    index[2] = k;
                    auto key = k * dim[0] * dim[1] + i * dim[1] + j;
                    auto phys = d->curImage->voxelToXYZ(index);
                    //phys[2] -= offset;
                    auto voxel = oriFL->XYZToVoxel(phys);
                    int vv[3] = { static_cast<int>(voxel[0]),static_cast<int>(voxel[1]),static_cast<int>(voxel[2]) };
                    if (vv[0] >= 0 && vv[0] < fl_dim[0]
                        && vv[1] >= 0 && vv[1] < fl_dim[1]
                        && vv[2] >= 0 && vv[2] < fl_dim[2]) {                                        
                        int vKey = vv[2] * fl_dim[0] * fl_dim[1] + vv[0] * fl_dim[1] + vv[1];
                        auto val = *(fl_pointer + vKey);
                        *(volume.get() + key) = val;
                    }
                }
            }
        }*/
        dataBufferObj->unmap();
        SbDataType type = SbDataType::UNSIGNED_SHORT;
        d->curFL->data.setValue(dim, type, 0, (void*)volume.get(), SoSFArray::COPY);
        volume = nullptr;
        d->curFL->extent.setValue(d->curImage->extent.getValue());                
    }

    auto SceneManager::SetPaintBrushSize(int size)->void {
        d->paintSize = size;
        for (int i = 0; i < 3; i++) {
            d->drawer[i]->SetPaintBrushSize(d->paintSize);
        }
    }

    auto SceneManager::SetWipeBrushSize(int size)->void {
        d->wipeSize = size;
        for (int i = 0; i < 3; i++) {
            d->drawer[i]->SetWipeBrushSize(d->wipeSize);
        }
    }
    auto SceneManager::SetLabelValue(int value) -> void {        
        d->curLabelValue = value;
        std::cout << "curLabel value: " << d->curLabelValue << std::endl;
        for(auto i=0;i<3;i++) {
            d->drawer[i]->SetCurLabel(value);
        }
        if (d->isAiInProgress) {
            AiResetImage();
        }
    }

    auto SceneManager::AddLabel(int value) -> bool {
        if(nullptr == d->drawer[0]) {            
            return false;
        }        
        if(d->drawer[0]->GetTool()==DrawerToolType::None && false == d->isAiInProgress) {
            return false;
        }
        if(nullptr == d->curMask) {            
            return false;
        }        
        if(d->curMaskName != "cellInst") {            
            return false;
        }        
        d->curLabelValue = value;
        std::cout << "curLabel value: " << d->curLabelValue << std::endl;
        for(auto i=0;i<3;i++) {
            d->drawer[i]->SetCurLabel(static_cast<uint32_t>(value));
        }

        if(d->isAiInProgress) {
            AiResetImage();
        }

        return true;
    }

    auto SceneManager::SetSizeIndex(int idx) -> void {
        if(nullptr == d->drawer3d) {
            return;
        }
        d->drawer3d->PerformSizeFilter(idx);
    }

    auto SceneManager::InitDataGroups()->void {
        //Init merger volume
        d->mergerTarget = new SoVolumeData;
        d->mergerTarget->setName("Start");

        //Init size volume
        d->sizeTarget = new SoVolumeData;
        d->sizeTarget->setName("sizeStart");

        //init sharable group item
        d->imageGroup = new SoGroup;

        auto infoGroup = new SoGroup;
        d->htMinInfo = new SoInfo;
        d->htMinInfo->setName("HTMin");
        d->htMinInfo->string.setValue("");
        d->htMaxInfo = new SoInfo;
        d->htMaxInfo->setName("HTMax");
        d->htMaxInfo->string.setValue("");
        infoGroup->addChild(d->htMinInfo);
        infoGroup->addChild(d->htMaxInfo);
        d->imageGroup->addChild(infoGroup);

        d->imageSocket = new SoSwitch;
        d->imageSocket->whichChild = -1;
        d->imageGroup->addChild(d->imageSocket);

        d->dataRange = new SoDataRange;
        d->dataRange->setName("HTSliceRange");
        d->imageGroup->addChild(d->dataRange);

        d->matlImage = new SoMaterial;
        d->matlImage->ambientColor.setValue(1, 1, 1);
        d->matlImage->diffuseColor.setValue(1, 1, 1);
        d->matlImage->transparency.setValue(0.5);
        d->imageGroup->addChild(d->matlImage);

        d->tfImage = new SoTransferFunction;
        d->tfImage->predefColorMap = SoTransferFunction::INTENSITY;
        d->tfImage->setName("colorMapHTSlice");
        d->imageGroup->addChild(d->tfImage);

        d->mergeGroup = new SoGroup;
        d->mergeSocket = new SoSwitch;        
        d->mergeSocket->addChild(d->mergerTarget);
        d->mergeSocket->whichChild = -1;
        d->mergeGroup->addChild(d->mergeSocket);

        d->dataRangeMerge = new SoDataRange;
        d->dataRangeMerge->min = -0.1;
        d->dataRangeMerge->max = 1;
        d->dataRangeMerge->setName("MergeSliceRange");
        d->mergeGroup->addChild(d->dataRangeMerge);

        d->sizeGroup = new SoGroup;
        d->sizeSocket = new SoSwitch;
        d->sizeSocket->addChild(d->sizeTarget);
        d->sizeSocket->whichChild = -1;
        d->sizeGroup->addChild(d->sizeSocket);

        d->dataRangeSize = new SoDataRange;
        d->dataRangeSize->min = -0.1;
        d->dataRangeSize->max = 1;
        d->dataRangeSize->setName("SizeSliceRange");
        d->sizeGroup->addChild(d->dataRangeSize);

        d->flGroup = new SoGroup;
        d->flSocket = new SoSwitch;
        d->flSocket->addChild(new SoSeparator);
        d->flSocket->whichChild = -1;
        d->flGroup->addChild(d->flSocket);

        d->dataRangeFL = new SoDataRange;
        d->dataRangeFL->min = 50;
        d->dataRangeFL->max = 200;
        d->dataRangeFL->setName("FLSliceRange");
        d->flGroup->addChild(d->dataRangeFL);

        d->maskGroup = new SoGroup;

        d->maskSocket = new SoSwitch;
        d->maskSocket->whichChild = -1;
        d->maskGroup->addChild(d->maskSocket);

        d->dataRangeMask = new SoDataRange;
        d->dataRangeMask->min = -0.5;
        d->dataRangeMask->max = 99;
        d->dataRangeMask->setName("MaskSliceRange");
        if (!d->isIsoSurface) {
            d->maskGroup->addChild(d->dataRangeMask);
        }

        d->dataRangeTemp = new SoDataRange;
        d->dataRangeTemp->min =0;
        d->dataRangeTemp->max =1;

        d->matlMaskVolume = new SoMaterial;
        d->matlMaskVolume->ambientColor.setValue(1, 1, 1);
        d->matlMaskVolume->diffuseColor.setValue(1, 1, 1);
        d->matlMaskVolume->transparency.setValue(0.5);

        d->matlMaskSlice = new SoMaterial;
        d->matlMaskSlice->ambientColor.setValue(1, 1, 1);
        d->matlMaskSlice->diffuseColor.setValue(1, 1, 1);
        d->matlMaskSlice->transparency.setValue(0.0);
                

        //d->matlMask->transparency.setValue(0.9);//for option #1
        //if(!d->isIsoSurface)
            //d->maskGroup->addChild(d->matlMask);

        d->matlMerge = new SoMaterial;
        d->matlMerge->ambientColor.setValue(1, 1, 1);
        d->matlMerge->diffuseColor.setValue(1, 1, 1);
        d->matlMerge->transparency.setValue(0.1f);
        d->mergeGroup->addChild(d->matlMerge);

        d->matlSize = new SoMaterial;
        d->matlSize->ambientColor.setValue(1, 1, 1);
        d->matlSize->diffuseColor.setValue(1, 1, 1);
        d->matlSize->transparency.setValue(0.1f);
        d->sizeGroup->addChild(d->matlSize);

        d->matlFL = new SoMaterial;
        d->matlFL->ambientColor.setValue(1, 1, 1);
        d->matlFL->diffuseColor.setValue(1, 1, 1);
        d->matlFL->transparency.setValue(0.5);
        d->flGroup->addChild(d->matlFL);


        d->tfMask = new SoTransferFunction;
        d->tfMask->predefColorMap = SoTransferFunction::NONE;
        d->tfMask->setName("colormapMask");
        d->tfMask->colorMap.setNum(256 * 4);        
        auto max = 100;
        float* p = d->tfMask->colorMap.startEditing();
        for (auto i = 0; i < 256; ++i) {            
            int idx = (float)i / 255.0 * max;
            if (idx < 1) {
				*p++ = 0.0;
				*p++ = 0.0;
				*p++ = 0.0;
				*p++ = 0.0;
			}else {
				float r = color_table[(idx-1) % 20][0] / 255.0;
				float g = color_table[(idx-1) % 20][1] / 255.0;
				float b = color_table[(idx-1) % 20][2] / 255.0;
				*p++ = r;
				*p++ = g;
				*p++ = b;
                *p++ = 0.7;
			}
        }
        d->tfMask->colorMap.finishEditing();
        if (!d->isIsoSurface) {
            d->maskGroup->addChild(d->tfMask);
        }

        d->tfMerge = new SoTransferFunction;
        d->tfMerge->predefColorMap = SoTransferFunction::NONE;
        d->tfMerge->setName("colormapMerge");
        d->tfMerge->colorMap.setNum(256 * 4);
        float* pp = d->tfMerge->colorMap.startEditing();
        for(auto i=0;i<256;++i) {
            if (i > 128) {
                *pp++ = 0.0;
                *pp++ = 0.0;
                *pp++ = 1.0;
                *pp++ = 1.0;
            }
            else {
                *pp++ = 0.0;
                *pp++ = 0.0;
                *pp++ = 0.0;
                *pp++ = 0.0;
            }
        }
        d->tfMerge->colorMap.finishEditing();
        d->mergeGroup->addChild(d->tfMerge);

        d->tfSize = new SoTransferFunction;
        d->tfSize->predefColorMap = SoTransferFunction::NONE;
        d->tfSize->setName("colormapSize");
        d->tfSize->colorMap.setNum(256 * 4);
        float* ppp = d->tfSize->colorMap.startEditing();
        for(auto i=0;i<256;++i) {
            if(i>128) {
                *ppp++ = 7.0;
                *ppp++ = 5.0;
                *ppp++ = 7.0;
                *ppp++ = 1.0;
            }else {
                *ppp++ = 0.0;
                *ppp++ = 0.0;
                *ppp++ = 0.0;
                *ppp++ = 0.0;
            }
        }
        d->tfSize->colorMap.finishEditing();
        d->sizeGroup->addChild(d->tfSize);
        
        d->tfFL = new SoTransferFunction;
        d->tfFL->predefColorMap = SoTransferFunction::NONE;
        d->tfFL->setName("colormapFL");
        d->tfFL->colorMap.setNum(256 * 4);
        d->flGroup->addChild(d->tfFL);

        d->tfSlice = new SoTransferFunction;
        d->tfSlice->predefColorMap = SoTransferFunction::NONE;
        d->tfSlice->colorMap.setNum(256 * 4);
        float* f = d->tfSlice->colorMap.startEditing();
        for(auto i=0;i<256;++i) {
            if(i>128) {
                *f++ = 0.0;
                *f++ = 1.0;
                *f++ = 0.0;
                *f++ = 1.0;
            }
            else {
                *f++ = 0.0;
                *f++ = 0.0;
                *f++ = 0.0;
                *f++ = 0.0;
            }
        }
        d->tfSlice->colorMap.finishEditing();
        for(auto i=0;i<3;i++) {
            d->imageSliceSep[i] = new SoSeparator;
            d->imageSliceSocket[i] = new SoSwitch;
            d->imageSliceSocket[i]->whichChild = -1;
            d->imageSliceSocket[i]->setName((std::string("TempSlice") + std::to_string(i)).c_str());
            d->imageSliceSep[i]->addChild(d->imageSliceSocket[i]);
            d->imageSliceSep[i]->addChild(d->matlMaskSlice);       
            //d->imageSliceSep[i]->addChild(d->dataRangeMask);
            d->imageSliceSep[i]->addChild(d->dataRangeTemp);
            d->imageSliceSep[i]->addChild(d->tfSlice);
            d->orthoSliceTemp[i] = new SoOrthoSlice;
            d->orthoSliceTemp[i]->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
            d->imageSliceSep[i]->addChild(d->orthoSliceTemp[i]);
        }

        d->maskGenerator = new OivMaskGenerator;
        d->aiSegEngine = new TC::AI::TCAiInteractiveSeg;
        d->aiSegEngine->InitModel();
    }
    auto SceneManager::Init3DScene()->void {
        //Scenegraph for 3D renderwindow
        d->root3d = new SoSeparator;
        d->volRenSep = new SoSeparator;
        d->volRenSepMask = new SoSeparator;
        d->modelSepMask = new SoSeparator;
        d->volRenSepMerge = new SoSeparator;
        d->volRenSepSize = new SoSeparator;

        SoShapeHints* hints = new SoShapeHints();
        hints->vertexOrdering = SoShapeHints::COUNTERCLOCKWISE;
        hints->creaseAngle = (float)M_PI;
        d->modelSepMask->addChild(hints);

        d->marching = new VolumeMarchingCubes;
                
        d->volRenSepMask->fastEditing.setValue(SoSeparator::FastEditing::CLEAR_ZBUFFER);

        d->root3d->addChild(d->volRenSepSize);
        d->root3d->addChild(d->volRenSepMerge);
        d->root3d->addChild(d->volRenSepMask);
        //d->root3d->addChild(d->modelSepMask);
        d->root3d->addChild(d->volRenSep); //no volume rendering for image required for now
        
        //scene graph for image volume render
        d->volRenSep->addChild(d->imageGroup);
        auto mat = new SoMaterial;
        mat->transparency = 1.0;
        d->volRenSep->addChild(mat);
        d->volQual = new SoVolumeRenderingQuality;                
        d->volQual->ambientOcclusion = FALSE;
        d->volQual->deferredLighting = FALSE;
        d->volRenSep->addChild(d->volQual);        

        d->volRender = new SoVolumeRender;        
        d->volRenSep->addChild(d->volRender);

        d->volRenSepMask->addChild(d->matlMaskVolume);
        d->volRenSepMask->addChild(d->maskGroup);

        d->matlIsoMask = new SoMaterial;        
        d->volIsoMask = new SoVolumeIsosurface;        
        if (d->isIsoSurface) {
            d->maskGroup->addChild(d->matlIsoMask);
            d->maskGroup->addChild(d->volIsoMask);
        }

        d->maskFrag = new SoFragmentShader;
        const auto shaderPath = QString("%1/shader/SurfaceRender.glsl").arg(qApp->applicationDirPath());
        d->maskFrag->sourceProgram.setValue(shaderPath.toStdString());
        d->maskFrag->addShaderParameter1i("transfer", 0);
        d->maskFrag->addShaderParameter1i("data1", 1);

        d->volQualMask = new SoVolumeRenderingQuality;
        d->volQualMask->shaderObject.set1Value(SoVolumeShader::FRAGMENT_COMPUTE_COLOR, d->maskFrag);
        d->volQualMask->lighting = TRUE;
        d->volQualMask->preIntegrated = TRUE;
        d->volQualMask->ambientOcclusion = FALSE;
        d->volQualMask->deferredLighting = FALSE;
        d->volQualMask->colorInterpolation = FALSE;
        d->volQualMask->segmentedInterpolation = FALSE;
        d->volQualMask->gradientQuality = SoVolumeRenderingQuality::GradientQuality::MEDIUM;
        d->volRenSepMask->addChild(d->volQualMask);
        
        d->volRenderMask = new SoVolumeRender;
        d->volRenderMask->samplingAlignment = SoVolumeRender::SamplingAlignment::VIEW_ALIGNED;
        d->volRenderMask->subdivideTile.setValue(TRUE);
        if (d->isIsoSurface) {
            d->volRenderMask->interpolation = SoVolumeRender::Interpolation::LINEAR;
        }
        else {
            d->volRenderMask->interpolation = SoVolumeRender::Interpolation::NEAREST;
        }
        d->volRenSepMask->addChild(d->volRenderMask);        

        d->volRenSepMerge->addChild(d->mergeGroup);
        d->volQualMerge = new SoVolumeRenderingQuality;
        d->volQualMerge->lighting = TRUE;
        d->volQualMerge->preIntegrated = FALSE;
        d->volQualMerge->ambientOcclusion = FALSE;
        d->volQualMerge->deferredLighting = FALSE;
        d->volQualMerge->colorInterpolation = FALSE;
        d->volQualMerge->voxelizedRendering = TRUE;
        d->volRenSepMerge->addChild(d->volQualMerge);

        d->volRenderMerge = new SoVolumeRender;
        d->volRenderMerge->samplingAlignment = SoVolumeRender::SamplingAlignment::DATA_ALIGNED;
        d->volRenderMerge->interpolation = SoVolumeRender::NEAREST;
        d->volRenSepMerge->addChild(d->volRenderMerge);

        d->volRenSepSize->addChild(d->sizeGroup);
        d->volQualSize = new SoVolumeRenderingQuality;
        d->volQualSize->lighting = TRUE;
        d->volQualSize->preIntegrated = FALSE;
        d->volQualSize->ambientOcclusion = FALSE;
        d->volQualSize->deferredLighting = FALSE;
        d->volQualSize->colorInterpolation = FALSE;
        d->volQualSize->voxelizedRendering = TRUE;
        d->volRenSepSize->addChild(d->volQualSize);

        d->volRenderSize = new SoVolumeRender;
        d->volRenderSize->samplingAlignment = SoVolumeRender::SamplingAlignment::DATA_ALIGNED;
        d->volRenderSize->interpolation = SoVolumeRender::NEAREST;
        d->volRenSepSize->addChild(d->volRenderSize);

        d->labelTextSwitch = new SoSwitch;
        d->labelTextSwitch->addChild(new SoSeparator);
        d->labelTextSwitch->whichChild = -1;

        d->root3d->addChild(d->labelTextSwitch);

        auto cube = new SoViewingCube;
        cube->setName("ViewCube");
        cube->position = SoViewingCube::PositionInViewport::BOTTOM_LEFT;
        //cube->sceneCamera = d->qOiv3DRenderWindow->getCamera();
        //cube->upAxis = SoViewingCube::Axis::Z;
        cube->upAxis = openinventor::inventor::Axis::Z;
        cube->facePosX = (qApp->applicationDirPath() + "/img/ViewFront.png").toStdString();
        cube->faceNegX = (qApp->applicationDirPath() + "/img/ViewBack.png").toStdString();
        cube->facePosY = (qApp->applicationDirPath() + "/img/ViewRight.png").toStdString();
        cube->faceNegY = (qApp->applicationDirPath() + "/img/ViewLeft.png").toStdString();
        cube->facePosZ = (qApp->applicationDirPath() + "/img/ViewTop.png").toStdString();
        cube->faceNegZ = (qApp->applicationDirPath() + "/img/ViewBottom.png").toStdString();
        //d->root3d->addChild(cube);
        d->volRenSepMask->addChild(cube);
        //d->root3d->addChild(new OivAxisMarker);

        //init 3D drawer
        d->drawerSep3d = new SoSeparator;
        d->drawerSep3d->setName("drawerSep3d");
        //d->drawerSep3d->fastEditing = SoSeparator::CLEAR_ZBUFFER;        
        d->drawerSep3d->boundingBoxIgnoring = TRUE;
        d->drawer3d = new Oiv3DDrawer;
        connect(d->drawer3d, (&Oiv3DDrawer::sigStartDrawing), this, &SceneManager::OnStart3dDrawing);
        connect(d->drawer3d, (&Oiv3DDrawer::sigFinishDrawing), this, &SceneManager::OnFinish3dDrawing);
        connect(d->drawer3d, (&Oiv3DDrawer::sigStartBranch), this, &SceneManager::OnStart3dBranching);
        connect(d->drawer3d, (&Oiv3DDrawer::sigFinishBranch), this, &SceneManager::OnFinish3dDrawing);
        connect(d->drawer3d, SIGNAL(sigStartSize(int)), this, SLOT(OnStartSize(int)));
        d->drawerSep3d->addChild(d->drawer3d->GetSceneGraph());        
        d->root3d->addChild(d->drawerSep3d);
    }
    void SceneManager::OnStartSize(int size) {
        d->sizeSocket->whichChild = 0;
        emit sigStartSizeFilter(size);
    }
    void SceneManager::OnStart3dDrawing() {
        emit sigStart3dDrawing();
    }
    void SceneManager::OnFinish3dDrawing() {        
        PerformSeparate();
        emit sigFinish3dDrawing();
    }
    void SceneManager::OnStart3dBranching() {
        //Make mask transparent
        /*auto max = 20;
        float* p = d->tfMask->colorMap.startEditing();
        for (auto i = 0; i < 256; ++i) {
            int idx = (float)i / 255.0 * max;
            if (idx < 1) {
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
            }
            else {
                float r = color_table[(idx - 1) % 20][0] / 255.0;
                float g = color_table[(idx - 1) % 20][1] / 255.0;
                float b = color_table[(idx - 1) % 20][2] / 255.0;
                *p++ = r;
                *p++ = g;
                *p++ = b;
                *p++ = 0.3;
            }
        }
        d->tfMask->colorMap.finishEditing();*/
    }
    auto SceneManager::Set3DCamera(SoPerspectiveCamera* cam) -> void {
        d->drawer3d->SetCamera(cam);
    }
    void SceneManager::OnFinish3dBranching() {
        //Mask mask opaque
        /*auto max = 20;
        float* p = d->tfMask->colorMap.startEditing();
        for (auto i = 0; i < 256; ++i) {
            int idx = (float)i / 255.0 * max;
            if (idx < 1) {
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
            }
            else {
                float r = color_table[(idx - 1) % 20][0] / 255.0;
                float g = color_table[(idx - 1) % 20][1] / 255.0;
                float b = color_table[(idx - 1) % 20][2] / 255.0;
                *p++ = r;
                *p++ = g;
                *p++ = b;
                *p++ = 1.0;
            }
        }
        d->tfMask->colorMap.finishEditing();
        */
    }
    auto SceneManager::Init2DScenes()->void {
        //Scenegraphs for 2D renderwindows
        for (auto i = 0; i < 3; i++) {
            d->root2d[i] = new SoSeparator;

            d->orthoSep[i] = new SoSeparator;            
            d->orthoSepMask[i] = new SoSeparator;
            d->orthoSepMask[i]->setName("MaskSeparator");
            d->orthoSepMerge[i] = new SoSeparator;
            d->orthoSepMerge[i]->setName("MergeSeparator");
            d->orthoSepFL[i] = new SoSeparator;
            d->orthoSepFL[i]->setName("FLSeparator");

            d->tempViz[i] = new SoSwitch;
            d->tempViz[i]->setName(("TempViz" + std::to_string(i)).c_str());
            d->tempViz[i]->whichChild = -1;
            d->tempViz[i]->addChild((d->imageSliceSep[i]));
            
            d->root2d[i]->addChild(d->tempViz[i]);
            d->root2d[i]->addChild(d->orthoSepMerge[i]);
            d->root2d[i]->addChild(d->orthoSepFL[i]);
            d->root2d[i]->addChild(d->orthoSepMask[i]);            
            d->root2d[i]->addChild(d->orthoSep[i]);            

            d->orthoSep[i]->addChild(d->imageGroup);
            if(i==0) {
                auto trans = new SoTranslation;
                trans->translation.setValue(0, 0, 0.1f);
                auto transfl = new SoTranslation;
                transfl->translation.setValue(0, 0, 0.2f);
                auto transmerge = new SoTranslation;
                transmerge->translation.setValue(0, 0, 0.3f);
                d->orthoSepMask[i]->addChild(trans);
                d->orthoSepMerge[i]->addChild(transmerge);
                d->orthoSepFL[i]->addChild(transfl);
            }
            d->orthoSepMask[i]->addChild(d->maskGroup);
            d->orthoSepMerge[i]->addChild(d->mergeGroup);
            d->orthoSepFL[i]->addChild(d->flGroup);

            d->orthoSlice[i] = new SoOrthoSlice;
            d->orthoSlice[i]->setName("ImageSlice");
            d->orthoSlice[i]->interpolation = SoSlice::NEAREST;
            d->orthoSlice[i]->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
            d->orthoSep[i]->addChild(d->orthoSlice[i]);

            d->orthoSliceMask[i] = new SoOrthoSlice;            
            d->orthoSliceMask[i]->setName("MaskSlice");
            d->orthoSliceMask[i]->interpolation = SoSlice::NEAREST;
            d->orthoSliceMask[i]->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
            d->orthoSepMask[i]->addChild(d->orthoSliceMask[i]);

            d->orthoSliceMerge[i] = new SoOrthoSlice;
            d->orthoSliceMerge[i]->setName("MergeSlice");
            d->orthoSliceMerge[i]->interpolation = SoSlice::NEAREST;
            d->orthoSliceMerge[i]->alphaUse = SoOrthoSlice::ALPHA_AS_IS;
            d->orthoSepMerge[i]->addChild(d->orthoSliceMerge[i]);

            d->orthoSliceFL[i] = new SoOrthoSlice;
            d->orthoSliceFL[i]->setName("FLSlice");
            d->orthoSliceFL[i]->interpolation = SoSlice::NEAREST;
            d->orthoSliceFL[i]->alphaUse = SoSlice::ALPHA_AS_IS;
            d->orthoSepFL[i]->addChild(d->orthoSliceFL[i]);

            if (i == 0) {
                d->orthoSlice[i]->axis = MedicalHelper::AXIAL;
                d->orthoSliceMask[i]->axis = MedicalHelper::AXIAL;
                d->orthoSliceTemp[i]->axis = MedicalHelper::AXIAL;
                d->orthoSliceMerge[i]->axis = MedicalHelper::AXIAL;
                d->orthoSliceFL[i]->axis = MedicalHelper::AXIAL;
            }
            else if (i == 1) {
                d->orthoSlice[i]->axis = MedicalHelper::SAGITTAL;
                d->orthoSliceMask[i]->axis = MedicalHelper::SAGITTAL;
                d->orthoSliceTemp[i]->axis = MedicalHelper::SAGITTAL;
                d->orthoSliceMerge[i]->axis = MedicalHelper::SAGITTAL;
                d->orthoSliceFL[i]->axis = MedicalHelper::SAGITTAL;
            }
            else {
                d->orthoSlice[i]->axis = MedicalHelper::CORONAL;
                d->orthoSliceMask[i]->axis = MedicalHelper::CORONAL;
                d->orthoSliceTemp[i]->axis = MedicalHelper::CORONAL;
                d->orthoSliceMerge[i]->axis = MedicalHelper::CORONAL;
                d->orthoSliceFL[i]->axis = MedicalHelper::CORONAL;
            }
            if (nullptr == d->scaleBarSwitch[i]) {
                d->scaleBarSwitch[i] = new SoSwitch;
            }
            else {
                d->scaleBarSwitch[i]->removeAllChildren();
            }
            d->scaleBarSwitch[i]->whichChild = -3;
            auto scaleBarSep = new SoSeparator;
            d->scaleBarSwitch[i]->addChild(scaleBarSep);

            auto scaleColor = new SoMaterial;
            scaleColor->diffuseColor.setValue(1, 0.25f, 0.25f);
            scaleBarSep->addChild(scaleColor);
            auto scaleStyle = new SoDrawStyle;
            scaleStyle->lineWidth = 2;
            scaleBarSep->addChild(scaleStyle);

            auto scaleBar = new OivScaleBar();
            scaleBar->setName("ScaleBar");
            scaleBar->numTickIntervals = 0;
            scaleBar->position = SbVec2f(0.95f, -0.99f);
            scaleBar->alignment = OivScaleBar::RIGHT;
            //scaleBar->trackedCamera = d->qOiv2DRenderWindow[i]->getCamera();

            scaleBarSep->addChild(scaleBar);

            if (d->root2d[i]->findChild(d->scaleBarSwitch[i]) < 0) {
                d->root2d[i]->addChild(d->scaleBarSwitch[i]);
            }
        }
    }
    auto SceneManager::Init2DDrawers()->void {
        for(auto i = 0; i < 3; i++) {
            d->drawerSep[i] = new SoSeparator;
            d->drawerSep[i]->setName("DrawerSeparator");
            //d->drawerSep[i]->fastEditing = SoSeparator::CLEAR_ZBUFFER;
            d->drawerSep[i]->boundingBoxIgnoring = TRUE;

            d->drawer[i] = new Oiv2DDrawer;
            d->drawer[i]->SetPaintBrushSize(d->paintSize);
            d->drawer[i]->SetWipeBrushSize(d->wipeSize);
            d->drawer[i]->SetMergerVolume(d->mergerTarget);            

            if (i == 0) {
                d->drawer[i]->SetAxis(MedicalHelper::AXIAL);
            }
            else if (i == 1) {
                d->drawer[i]->SetAxis(MedicalHelper::SAGITTAL);
            }
            else {
                d->drawer[i]->SetAxis(MedicalHelper::CORONAL);
            }

            d->drawerSep[i]->addChild(d->drawer[i]->GetSceneGraph());

            d->root2d[i]->addChild(d->drawerSep[i]);
            connect(d->drawer[i],qOverload<int>(&Oiv2DDrawer::pickValue),this,&SceneManager::OnLabelPicking);
            connect(d->drawer[i], SIGNAL(mergeVol(int)), this, SLOT(OnFinishMerge(int)));
            connect(d->drawer[i], SIGNAL(sigHistory(int)), this, SLOT(OnHistory(int)));
            connect(d->drawer[i], SIGNAL(pickRemove()), this, SLOT(OnLabelRemove()));
        }
        d->drawer3d->SetSizeVolume(d->sizeTarget);
    }
    void SceneManager::OnHistory(int id) {
        d->editHistory.push_back(id);
    }

    auto SceneManager::Init() -> void {        
        InitDataGroups();
        Init3DScene();
        Init2DScenes();
        Init2DDrawers();        
    }
    
    auto SceneManager::SetMask(TCMask::Pointer& mask,QString name) -> bool {
        if (nullptr != d->curMask) {
            ResetMask();
        }
        
        auto converter = new TCDataConverter;
        d->curMaskName = name;                
        d->curMask = converter->MaskToSoVolumeData(mask,name);
        d->curMask->ref();
        std::cout << "Dim: " << d->curMask->getDimension() << std::endl;
        d->matlMaskVolume->transparency = 0.4f;
        SetMaskToScene();

        if(name == "cellInst") {
            CreateLabelTextMap();
        }
        //CreateMarchingCubes();
        if (d->isIsoSurface)
            CreateIsoSurface();

        delete converter;

        return true;
    }
    auto SceneManager::CreateIsoSurface() -> void {
        double min, max;
        d->curMask->getMinMax(min, max);        
        int int_max = max;        
        MedicalHelper::dicomAdjustDataRange(d->dataRangeMask, d->curMask);

        for(auto i=0;i<int_max;i++) {
            d->volIsoMask->isovalues.set1Value(i, i + 1);
            auto color_idx = i % 20;
            d->matlIsoMask->diffuseColor.set1Value(i, color_table[color_idx][0] / 255.0, color_table[color_idx][1] / 255.0, color_table[color_idx][2] / 255.0);
            d->matlIsoMask->transparency.set1Value(i, 0.85f);
        }
    }
    auto SceneManager::CreateMarchingCubes() -> void {
        double min, max;
        d->curMask->getMinMax(min, max);
        int int_max = max;
        for(auto i=0;i<int_max;i++) {
            auto sep = new SoSeparator;
            sep->setName(QString("Model%1Sep").arg(i+1).toStdString().c_str());
            auto modelMatl = new SoMaterial;
            modelMatl->setName(QString("Model%1Matl").arg(i + 1).toStdString().c_str());
            auto color_idx = i % 20;
            modelMatl->diffuseColor.setValue(color_table[color_idx][0] / 255.0, color_table[color_idx][1] / 255.0, color_table[color_idx][2] / 255.0);
            sep->addChild(modelMatl);            
            SoTriangleSet* model = d->marching->getIsosurface(*d->curMask,i + 0.5);
            
            sep->addChild(model);
            d->modelSepMask->addChild(sep);
        }
    }
    auto SceneManager::CreateLabelTextMap() -> void {
        if(nullptr == d->curMask) {
            return;
        }
        double min, max;
        d->curMask->getMinMax(min, max);
        d->labelTextSwitch->removeAllChildren();
        auto labelRoot = new SoSeparator;
        d->labelTextSwitch->replaceChild(0, labelRoot);
        d->labelTextSwitch->whichChild = 0;
        if(max<1) {
            return;
        }        

        auto font = new SoFont;
        font->size = 25;
        font->name = "NOTO SANS KR : BOLD";
        labelRoot->addChild(font);

        SoRef<SoVolumeData> mask = d->curMask;
        SoRef<SoLabelAnalysisQuantification> analysis = new SoLabelAnalysisQuantification;
        analysis->measureList.set1Value(0, new SoDataMeasurePredefined(SoDataMeasurePredefined::BOUNDING_BOX_OX));
        analysis->measureList.set1Value(1, new SoDataMeasurePredefined(SoDataMeasurePredefined::BOUNDING_BOX_OY));
        analysis->measureList.set1Value(2, new SoDataMeasurePredefined(SoDataMeasurePredefined::BOUNDING_BOX_OZ));
        analysis->measureList.set1Value(3, new SoDataMeasurePredefined(SoDataMeasurePredefined::BOUNDING_BOX_DY));
        analysis->measureList.set1Value(4, new SoDataMeasurePredefined(SoDataMeasurePredefined::BOUNDING_BOX_DZ));
        //MedicalHelper::dicomAdjustVolume(mask.ptr());
        SoRef<SoMemoryDataAdapter> labelAdapter = MedicalHelper::getImageDataAdapter(mask.ptr());
        labelAdapter->interpretation = SoImageDataAdapter::Interpretation::LABEL;
        analysis->inLabelImage = labelAdapter.ptr();
        analysis->inIntensityImage = labelAdapter.ptr();

        SoRef<SoLabelAnalysisResult> analysisOutput = analysis->outAnalysis.getValue();
        auto numLabel = analysisOutput->getNumLabels();

        for (auto label = 0; label < numLabel; label++) {
            //retrive label position
            auto x = analysisOutput->getValueAsDouble(label, 0, 0);
            auto y = analysisOutput->getValueAsDouble(label, 1, 0);
            auto z = analysisOutput->getValueAsDouble(label, 2, 0);
            auto ySize = analysisOutput->getValueAsDouble(label, 3, 0);
            auto zSize = analysisOutput->getValueAsDouble(label, 4, 0);

            auto textSep = new SoSeparator;

            auto textMatl = new SoMaterial;
            auto color_idx = label % 20;
            textMatl->diffuseColor.setValue(color_table[color_idx][0] / 255.0, color_table[color_idx][1] / 255.0, color_table[color_idx][2] / 255.0);

            textSep->addChild(textMatl);

            auto trans = new SoTranslation;
            trans->translation.setValue(x, y + ySize, z + zSize);

            textSep->addChild(trans);

            auto text = new SoText2;
            text->string = std::to_string(label + 1);

            textSep->addChild(text);

            labelRoot->addChild(textSep);
        }
    }

    auto SceneManager::RearrangeLabel() -> void {
        if(nullptr == d->curMask) {
            return;
        }
        if (d->editHistory.count() + d->undoHistory.count() > 0) {
            d->curMask->saveEditing();
            d->editHistory.clear();
            d->undoHistory.clear();            
            d->curMask->data.touch();
        }        

        //MedicalHelper::dicomAdjustVolume(d->curMask);
        SoRef<SoMemoryDataAdapter> oriAdapter = MedicalHelper::getImageDataAdapter(d->curMask);        
        oriAdapter->interpretation = SoMemoryDataAdapter::Interpretation::LABEL;

        //auto labeling = new SoLabelingProcessing;
        //labeling->inObjectImage = oriAdapter;
        SoRef<SoReorderLabelsProcessing> reorder = new SoReorderLabelsProcessing;
        reorder->inLabelImage = oriAdapter.ptr();

        SoRef<SoConvertImageProcessing> converter = new SoConvertImageProcessing;
        converter->inImage.connectFrom(&reorder->outLabelImage);        
        converter->dataType = SoConvertImageProcessing::DataType::UNSIGNED_SHORT;

        //SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;        
        auto reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&converter->outImage);

        SoVolumeData* newVol = new SoVolumeData;
        newVol->ref();
        newVol->setReader(*reader, TRUE);
        newVol->setName("MaskVolume");
        ResetMask();

        d->curMask = newVol;
        
        std::cout << "Rea " << d->curMask->getDimension() << std::endl;

        SetMaskToScene();

        CreateLabelTextMap();
    }
    auto SceneManager::GetMask()->TCMask::Pointer {
        if(nullptr == d->curMask) {
            return nullptr; 
        }
        if (d->editHistory.count() + d->undoHistory.count() > 0) {
            d->curMask->saveEditing();
            d->editHistory.clear();
            d->undoHistory.clear();            
        }        
        d->curMask->data.touch();        

        auto converter = new TCDataConverter;
        QList<SoVolumeData*> maskList;
        maskList.push_back(d->curMask);                                

        MaskTypeEnum type = MaskTypeEnum::None;
        if(d->curMaskName == "cellInst") {
            type = MaskTypeEnum::MultiLabel;
        }else {
            type = MaskTypeEnum::MultiLayer;
        }
        QStringList names;
        names.push_back(d->curMaskName);
        auto mask = converter->SoVolumeDataToMask(maskList,type,names);
    
        delete converter;
        return mask;
    }
    auto SceneManager::TryRefresh() -> void {
        //in outer space mask is save edited before refresh call 
        /*if (nullptr != d->curMask) {
            if (d->editHistory.count() + d->undoHistory.count() > 0) {
                std::cout << "edit history: " << d->editHistory.count() << std::endl;
                std::cout << "undo history: " << d->undoHistory.count() << std::endl;
                std::cout << "SE before refresh" << std::endl;                
                d->curMask->saveEditing();
                std::cout << "SE before refresh done" << std::endl;
            }
            d->curMask->data.touch();
        }*/
        d->editHistory.clear();
        d->undoHistory.clear();        
    }
    auto SceneManager::TryUndo() -> void {        
        if(nullptr == d->curMask) {
            return;
        }
        if (d->editHistory.count() > 0) {
            auto id = d->editHistory.last();
            d->curMask->undoEditing(id);
            d->editHistory.pop_back();
            d->undoHistory.push_back(id);
        }
    }
    auto SceneManager::TryRedo() -> void {
        if(nullptr == d->curMask) {
            return;
        }
        if(d->undoHistory.count()>0) {
            auto id = d->undoHistory.last();
            d->curMask->redoEditing(id);
            d->editHistory.push_back(id);
            d->undoHistory.pop_back();
        }
    }
    auto SceneManager::SetFLOpacity(double opacity) -> bool {
        d->matlFL->transparency.setValue(1.0 - opacity);
        return true;
    }
    auto SceneManager::SetFLColor(QColor color) -> bool {
        float* f = d->tfFL->colorMap.startEditing();
        for (auto i = 0; i < 256; ++i) {
            if (i > 128) {
                *f++ = color.redF();
                *f++ = color.greenF();
                *f++ = color.blueF();
                *f++ = 1.0;
            }
            else {
                *f++ = 0.0;
                *f++ = 0.0;
                *f++ = 0.0;
                *f++ = 0.0;
            }
        }
        d->tfFL->colorMap.finishEditing();
        return true;
    }

    auto SceneManager::RemoveMask(QString name) -> bool {
        if(d->curMaskName == name) {
            ResetMask();
            d->curMaskName = QString();
        }
        return true;
    }
    auto SceneManager::ClearVizMask() -> bool {        
        ResetMask();
        d->curMaskName = QString();
        d->maskGenerator->Clear(true);
        return true;
    }
    auto SceneManager::AddVizMask(TCMask::Pointer& mask, QString name) -> bool {
        auto converter = new TCDataConverter;
        auto m = converter->MaskToSoVolumeData(mask, name);        
        d->maskGenerator->AppendMask(name, m);        
        delete converter;
        return true;
    }
    auto SceneManager::RemoveVizMask(QString name) -> bool {        
        //d->maskGenerator->DeleteMask(name);
        //d->maskGenerator->SetMaskOrder();
        return true;
    }

    auto SceneManager::GenerateVizMask() -> bool {        
        d->matlMaskVolume->transparency = 0.9f;
        d->maskGenerator->SetMaskOrder();
        d->curMask = d->maskGenerator->GetCurrentResult();        

        std::cout << "GVIZ " << d->curMask->getDimension() << std::endl;

        SetMaskToScene();
        return true;
    }
    auto SceneManager::SetFLImage(TCImage::Pointer& flImage, int ch,float offset) -> bool {        
        if(nullptr != d->curFL) {
            d->flSocket->replaceChild(0, new SoSeparator);
            while(d->curFL->getRefCount()>0) {
                d->curFL->unref();
            }
            d->curFL = nullptr;            
        }
        d->flSocket->whichChild = -1;
        if (ch > -1) {
            auto converter = new TCDataConverter;
            auto original = converter->ImageToSoVolumeData(flImage);
            original->ref();
            convertFL(original, offset);

            d->flSocket->replaceChild(0, d->curFL);
            d->flSocket->whichChild = 0;

            while (original->getRefCount() > 0) {
                original->unref();
            }
            original = nullptr;

            delete converter;
        }
        return true;
    }
    auto SceneManager::SetImage(TCImage::Pointer& image,QString name) -> bool {
        auto time = image->GetTimeStep();
        if(d->curImageName == name && d->curTime == time) {
            return true;
        }
        if (nullptr != d->curImage) {     
            ResetAll();                        
        }
        d->curTime = time;
        auto converter = new TCDataConverter;        
        d->curImage = converter->ImageToSoVolumeData(image);        
        d->curImage->ref();
        //double min, max;
        //d->curImage->getMinMax(min, max);        
        std::cout << "Set image dimension: " << d->curImage->getDimension() << std::endl;
        SetImageToScene();

        d->curImageName = name;

        delete converter;

        auto imin = std::get<0>(image->GetMinMax());
        auto imax = std::get<1>(image->GetMinMax());
        d->dataRange->min = imin;
        d->dataRange->max = imax;

        return true;
    }
    auto SceneManager::ActivateTool(int idx) -> bool {        
        auto type = (DrawerToolType)idx;
        for(auto i=0;i<3;i++) {
            d->drawer[i]->SetTool(type);
            //d->drawer[i]->SetCurLabel(1);
        }
        d->drawer3d->SetTool(type);
        if(type != DrawerToolType::None) {
            emit sigAiPointOff();
        }
        return true;
    }
    auto SceneManager::DeactivateTool(bool isFunc) -> bool {
        auto curtool = d->drawer[0]->GetTool();
        if(curtool == DrawerToolType::MergeLabel) {
            if (isFunc) {
                PerformMergeLabel();
            }else {
                FlushMergeLabel();
            }
        }
        if(d->sizeSocket->whichChild.getValue() != -1) {
            PerformRemoveSize();            
        }
        auto type = DrawerToolType::None;
        for(auto i=0;i<3;i++) {
            d->drawer[i]->SetTool(type);            
        }
        d->drawer3d->SetTool(type);        
        //TryRefresh();
        return true;
    }    
    auto SceneManager::PerformDilate(int label) -> bool {
        if (d->curMaskName.isEmpty()) {
            return false;
        }
        if (label < 1) {
            std::cout << "label value is lower than 1" << std::endl;
            return false;
        }
        if (nullptr == d->curMask) {
            return false;
        }

        //MedicalHelper::dicomAdjustVolume(d->curMask);
        auto maskAdapter = MedicalHelper::getImageDataAdapter(d->curMask);
        maskAdapter->interpretation = SoMemoryDataAdapter::VALUE;

        auto thresh = new SoThresholdingProcessing;
        thresh->inImage = maskAdapter;
        thresh->thresholdLevel.setValue(label, label + 0.5);

        auto dilate = new SoDilationBallProcessing3d;
        dilate->inImage.connectFrom(&thresh->outBinaryImage);
        dilate->elementSize.setValue(3);

        auto binconv = new SoConvertImageProcessing;
        binconv->inImage.connectFrom(&dilate->outImage);
        binconv->dataType = SoConvertImageProcessing::BINARY;

        auto reset = new SoResetImageProcessing;
        reset->inImage = maskAdapter;
        reset->intensityValue = label;

        auto emptyReset = new SoResetImageProcessing;
        emptyReset->inImage = maskAdapter;
        emptyReset->intensityValue = 0;

        auto remover = new SoCombineByMaskProcessing;
        remover->inImage1.connectFrom(&emptyReset->outImage);
        remover->inImage2 = maskAdapter;
        remover->inBinaryImage.connectFrom(&thresh->outBinaryImage);

        auto masking = new SoCombineByMaskProcessing;
        masking->inImage1.connectFrom(&reset->outImage);
        masking->inImage2.connectFrom(&remover->outImage);
        masking->inBinaryImage.connectFrom(&binconv->outImage);

        auto reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&masking->outImage);

        d->curMask->setReader(*reader, TRUE);
        d->curMask->data.touch();
        return true;
    }
    auto SceneManager::PerformErode(int label) -> bool {        
        if (d->curMaskName.isEmpty()) {
            return false;
        }
        if (label < 1) {
            std::cout << "label value is lower than 1" << std::endl;
            return false;
        }
        if(nullptr ==d->curMask) {
            return false;
        }

        //MedicalHelper::dicomAdjustVolume(d->curMask);
        auto maskAdapter = MedicalHelper::getImageDataAdapter(d->curMask);
        maskAdapter->interpretation = SoMemoryDataAdapter::VALUE;

        auto thresh = new SoThresholdingProcessing;
        thresh->inImage = maskAdapter;
        thresh->thresholdLevel.setValue(label, label +0.5);

        auto erode = new SoErosionBallProcessing3d;
        erode->inImage.connectFrom(&thresh->outBinaryImage);
        erode->elementSize.setValue(3);

        auto binconv = new SoConvertImageProcessing;
        binconv->inImage.connectFrom(&erode->outImage);
        binconv->dataType = SoConvertImageProcessing::BINARY;

        auto reset = new SoResetImageProcessing;
        reset->inImage = maskAdapter;
        reset->intensityValue = label;

        auto emptyReset = new SoResetImageProcessing;
        emptyReset->inImage = maskAdapter;
        emptyReset->intensityValue = 0;

        auto remover = new SoCombineByMaskProcessing;
        remover->inImage1.connectFrom(&emptyReset->outImage);
        remover->inImage2 = maskAdapter;
        remover->inBinaryImage.connectFrom(&thresh->outBinaryImage);

        auto masking = new SoCombineByMaskProcessing;
        masking->inImage1.connectFrom(&reset->outImage);
        masking->inImage2.connectFrom(&remover->outImage);
        masking->inBinaryImage.connectFrom(&binconv->outImage);

        auto reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&masking->outImage);

        d->curMask->setReader(*reader, TRUE);
        d->curMask->data.touch();

        return true;
    }
    auto SceneManager::PerformWaterShed(TCMask::Pointer target) -> bool {
        if(d->curMaskName.isEmpty()) {
            return false;
        }
        if(d->curMaskName != "cellInst") {
            return false;
        }
        if(nullptr == target) {
            return false;
        }
        if(nullptr == d->curMask) {
            return false;
        }
        auto converter = new TCDataConverter;
        auto target_volume = converter->MaskToSoVolumeData(target,"membrane");                

        //MedicalHelper::dicomAdjustVolume(target_volume);
        auto targetAdapter = MedicalHelper::getImageDataAdapter(target_volume);
        targetAdapter->interpretation = SoMemoryDataAdapter::VALUE;

        //MedicalHelper::dicomAdjustVolume(d->curMask);
        auto maskAdapter = MedicalHelper::getImageDataAdapter(d->curMask);
        maskAdapter->interpretation = SoMemoryDataAdapter::LABEL;

        auto water = new SoMarkerBasedWatershedProcessing;
        water->outputMode = SoMarkerBasedWatershedProcessing::OutputMode::SEPARATED_BASINS;
        water->precisionMode = SoMarkerBasedWatershedProcessing::PrecisionMode::FAST;
        water->inGrayImage = targetAdapter;
        water->inMarkerImage = maskAdapter;

        auto binconv = new SoConvertImageProcessing;
        binconv->inImage = targetAdapter;
        binconv->dataType = SoConvertImageProcessing::BINARY;

        auto masking = new SoMaskImageProcessing;
        masking->inImage.connectFrom(&water->outObjectImage);
        masking->inBinaryImage.connectFrom(&binconv->outImage);        

        auto reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&masking->outImage);

        d->curMask->setReader(*reader, TRUE);

        delete converter;

        return true;
    }

    auto SceneManager::FlushMergeLabel() -> void {
        d->mergerTarget->setName("Start");
        d->mergeSocket->whichChild = -1;
    }

    auto SceneManager::PerformSeparate()->void {                
        if (nullptr == d->curMask) {
            return;
        }
        auto guideShape = MedicalHelper::find<SoShape>(d->root3d,"GuideFrustum");
        if (nullptr == guideShape) {
            return;
        }
        auto size = d->curMask->getDimension();

        //copy original image and get threshold first
        //MedicalHelper::dicomAdjustVolume(d->targetVolume);
        SoRef<SoMemoryDataAdapter> oriAdap = MedicalHelper::getImageDataAdapter(d->curMask);
        SoRef<SoThresholdingProcessing> oriThresh = new SoThresholdingProcessing;
        oriThresh->inImage = oriAdap.ptr();
        oriThresh->thresholdLevel.setValue(1, static_cast<float>(INT_MAX));

        //perform erosion for threshold imaged
        SoRef<SoErosionCubeProcessing> ero = new SoErosionCubeProcessing;
        ero->inImage.connectFrom(&oriThresh->outBinaryImage);
        ero->elementSize = 10;


        double mmin, mmax;
        d->curMask->getMinMax(mmin, mmax);
        int new_label = mmax + 1;

        int editionId;
        d->curMask->startEditing(editionId);
        d->curMask->editSolidShape(guideShape, new_label);
        d->curMask->finishEditing(editionId);
        d->curMask->saveEditing();

        /*SoRef<SoVolumeData> copyVol = new SoVolumeData;
        copyVol->data.setValue(size, SbDataType::UNSIGNED_SHORT, d->curMask->data.getValue(), SoSFArray::COPY);
        copyVol->extent.setValue(d->curMask->extent.getValue());
        copyVol->touch();

        double mmin, mmax;
        copyVol->getMinMax(mmin, mmax);
        int new_label = mmax + 1;

        int editionId;
        copyVol->startEditing(editionId);
        copyVol->editSolidShape(guideShape, new_label);
        copyVol->finishEditing(editionId);
        copyVol->saveEditing();

        copyVol->data.touch();*/               
        

        //get modified adapter
        //MedicalHelper::dicomAdjustVolume(copyVol);
        //SoRef<SoMemoryDataAdapter> modiAdap = MedicalHelper::getImageDataAdapter(copyVol.ptr());
        SoRef<SoMemoryDataAdapter> modiAdap = MedicalHelper::getImageDataAdapter(d->curMask);
        SoRef<SoMaskImageProcessing> masking = new SoMaskImageProcessing;
        masking->inImage = modiAdap.ptr();
        masking->inBinaryImage.connectFrom(&ero->outImage);

        SoRef<SoConvertImageProcessing> conv = new SoConvertImageProcessing;
        conv->inImage.connectFrom(&masking->outImage);
        conv->dataType = SoConvertImageProcessing::DataType::LABEL;

        SoRef<SoExpandLabelsProcessing> expand = new SoExpandLabelsProcessing;
        expand->inLabelImage.connectFrom(&conv->outImage);
        expand->expandMode = SoExpandLabelsProcessing::ExpandMode::REGIONS;

        SoRef< SoConvertImageProcessing> iconv = new SoConvertImageProcessing;
        iconv->inImage.connectFrom(&expand->outLabelImage);
        iconv->dataType = SoConvertImageProcessing::DataType::UNSIGNED_SHORT;

        SoRef<SoMaskImageProcessing> finalMask = new SoMaskImageProcessing;
        finalMask->inImage.connectFrom(&iconv->outImage);
        finalMask->inBinaryImage.connectFrom(&oriThresh->outBinaryImage);

        SoRef<SoConvertImageProcessing> labelConv = new SoConvertImageProcessing;
        labelConv->dataType = SoConvertImageProcessing::DataType::LABEL;
        labelConv->inImage.connectFrom(&finalMask->outImage);

        SoRef<SoConvertImageProcessing> thConv = new SoConvertImageProcessing;
        thConv->inImage.connectFrom(&oriThresh->outBinaryImage);
        thConv->dataType = SoConvertImageProcessing::DataType::UNSIGNED_SHORT;

        SoRef<SoMarkerBasedWatershedProcessing> water = new SoMarkerBasedWatershedProcessing;
        water->outputMode = SoMarkerBasedWatershedProcessing::OutputMode::SEPARATED_BASINS;
        water->precisionMode = SoMarkerBasedWatershedProcessing::PrecisionMode::FAST;
        water->inGrayImage.connectFrom(&thConv->outImage);
        water->inMarkerImage.connectFrom(&labelConv->outImage);

        SoRef<SoMaskImageProcessing> fmasking = new SoMaskImageProcessing;
        fmasking->inImage.connectFrom(&water->outObjectImage);
        fmasking->inBinaryImage.connectFrom(&oriThresh->outBinaryImage);

        SoRef<SoVRImageDataReader> outSingleReader = new SoVRImageDataReader;
        outSingleReader->imageData.connectFrom(&fmasking->outImage);

        d->curMask->setReader(*outSingleReader, TRUE);
        //d->curMask->data.touch();              

        auto frustumSep = MedicalHelper::find<SoSeparator>(d->root3d, "FrustumSep");
        frustumSep->replaceChild(1, new SoSeparator);
    }

    auto SceneManager::PerformRemoveSize() -> void {
        if(nullptr == d->sizeTarget || nullptr == d->curMask) {
            return;
        }
        //MedicalHelper::dicomAdjustVolume(d->curMask);
        auto maskAdapter = MedicalHelper::getImageDataAdapter(d->curMask);
        maskAdapter->interpretation = SoMemoryDataAdapter::VALUE;
        //MedicalHelper::dicomAdjustVolume(d->sizeTarget);
        auto sizeAdapter = MedicalHelper::getImageDataAdapter(d->sizeTarget);
        sizeAdapter->interpretation = SoMemoryDataAdapter::BINARY;

        auto invert = new SoLogicalNotProcessing;        
        invert->inImage = sizeAdapter;

        auto masking = new SoMaskImageProcessing;        
        masking->inImage = maskAdapter;
        masking->inBinaryImage.connectFrom(&invert->outImage);

        auto reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&masking->outImage);

        d->curMask->setReader(*reader, TRUE);
        //d->curMask->touch();

        d->sizeSocket->whichChild = -1;
    }

    auto SceneManager::PerformMergeLabel() -> void {        
        if(nullptr != d->mergerTarget && nullptr != d->curMask) {            
            if(d->mergerTarget->getName() != "ChunkMask") {
                return;
            }
            //MedicalHelper::dicomAdjustVolume(d->curMask);
            auto maskAdapter = MedicalHelper::getImageDataAdapter(d->curMask);
            maskAdapter->interpretation = SoMemoryDataAdapter::VALUE;            
            //MedicalHelper::dicomAdjustVolume(d->mergerTarget);
            auto mergeAdapter = MedicalHelper::getImageDataAdapter(d->mergerTarget);
            mergeAdapter->interpretation = SoMemoryDataAdapter::BINARY;

            auto quan = new  SoMaskedStatisticsQuantification;
            quan->inImage = maskAdapter;
            quan->inMaskImage = mergeAdapter;

            auto min = quan->outResult.getDetail().getMinimum();            

            auto reset = new SoResetImageProcessing;
            reset->inImage = maskAdapter;
            reset->intensityValue = min;

            auto masking = new SoCombineByMaskProcessing;
            masking->inImage1.connectFrom(&reset->outImage);
            masking->inImage2 = maskAdapter;
            masking->inBinaryImage = mergeAdapter;            

            auto conv = new SoConvertImageProcessing;
            conv->inImage.connectFrom(&masking->outImage);
            conv->dataType = SoConvertImageProcessing::UNSIGNED_SHORT;

            auto reader = new SoVRImageDataReader;
            reader->imageData.connectFrom(&conv->outImage);
            
            d->curMask->setReader(*reader, TRUE);            
            //d->curMask->touch();
            
            //reset volume container name as initial state
            d->mergerTarget->setName("Start");
            d->mergeSocket->whichChild = -1;
        }
    }

    auto SceneManager::Reset() -> void {
        for (auto i = 0; i < 3; i++) {
            d->drawer[i]->SetCurLabel(1);
        }
        DeactivateTool();
        ResetAll();
    }

    auto SceneManager::ResetAll() -> void {
        if (d->imageSocket->getNumChildren() > 0) {
            d->imageSocket->removeChild(0);
        }
        d->imageSocket->whichChild = -1;
        if (nullptr != d->curImage) {
            d->curImage->unref();
        }
        d->curImageName = QString();        
        d->flSocket->whichChild = -1;

        d->editHistory.clear();
        d->undoHistory.clear();
        ResetMask();
    }
    auto SceneManager::ResetMask(bool soft) -> void {        
        if (d->maskSocket->getNumChildren() > 0) {
            d->maskSocket->removeChild(0);
            d->maskSocket->whichChild = -1;
            if (!soft) {
                while (d->curMask->getRefCount() > 0) {
                    d->curMask->unref();
                }
                d->curMask = nullptr;
            }
        }
        for(int i=0;i<3;i++) {
            d->drawer[i]->SetTargetVolume(nullptr);
        }
        d->drawer3d->SetTargetVolume(nullptr);
        d->labelTextSwitch->replaceChild(0, new SoSeparator);
        d->labelTextSwitch->whichChild = -1;

        d->modelSepMask->removeAllChildren();
        SoShapeHints* hints = new SoShapeHints();
        hints->vertexOrdering = SoShapeHints::COUNTERCLOCKWISE;
        hints->creaseAngle = (float)M_PI;
        d->modelSepMask->addChild(hints);
    }

    void SceneManager::OnLabelRemove() {
        //RearrangeLabel();
    }

    void SceneManager::OnFinishMerge(int val) {
        Q_UNUSED(val)
        double min, max;        
        d->mergerTarget->getMinMax(min, max);        
        if(max>0) {
            d->mergeSocket->whichChild = 0;
        }else {
            d->mergeSocket->whichChild = -1;
        }
    }
    void SceneManager::OnLabelPicking(int val) {        
        emit sigLabel(val);
    }
    auto SceneManager::SetImageToScene() -> void {
        d->curImage->setName("volData");
        d->curImage->dataSetId = 1;
        d->curImage->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::ALWAYS ;
        d->imageSocket->addChild(d->curImage);
        d->imageSocket->whichChild = 0;
        
        //MedicalHelper::dicomAdjustDataRange(d->dataRange, d->curImage);
        MedicalHelper::dicomCheckMonochrome1(d->tfImage, d->curImage);        

        int intmin = d->dataRange->min.getValue();
        int intmax = d->dataRange->max.getValue();

        auto stmin = QString(d->htMinInfo->string.getValue().toStdString().c_str());
        auto stmax = QString(d->htMaxInfo->string.getValue().toStdString().c_str());        
        if(false == stmin.isEmpty()) {            
            auto prevMin = stmin.toInt();
            if(prevMin < intmax) {                
                d->dataRange->min.setValue(prevMin);
            }else {                
                d->htMinInfo->string.setValue(std::to_string(intmin));
            }
        }else {            
            d->htMinInfo->string.setValue(std::to_string(intmin));
        }
        if(false == stmax.isEmpty()) {            
            auto prevMax = stmax.toInt();
            if(prevMax > intmin) {                
                d->dataRange->max.setValue(prevMax);
            }else {                
                d->htMaxInfo->string.setValue(std::to_string(intmax));
            }
        }else {            
            d->htMaxInfo->string.setValue(std::to_string(intmax));
        }

        d->orthoSlice[0]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::AXIAL] / 2;
        d->orthoSlice[1]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::SAGITTAL] / 2;
        d->orthoSlice[2]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::CORONAL] / 2;
        d->orthoSliceMask[0]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::AXIAL] / 2;
        d->orthoSliceMask[1]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::SAGITTAL] / 2;
        d->orthoSliceMask[2]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::CORONAL] / 2;
        d->orthoSliceMerge[0]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::AXIAL] / 2;
        d->orthoSliceMerge[1]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::SAGITTAL] / 2;
        d->orthoSliceMerge[2]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::CORONAL] / 2;
        d->orthoSliceFL[0]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::AXIAL] / 2;
        d->orthoSliceFL[1]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::SAGITTAL] / 2;
        d->orthoSliceFL[2]->sliceNumber = d->curImage->data.getSize()[MedicalHelper::CORONAL] / 2;
    }
    auto SceneManager::SetMaskToScene() -> void {
        d->curMask->setName("MaskVolume");
        d->curMask->ldmResourceParameters.getValue()->loadPolicy = SoLDMResourceParameters::ALWAYS;
        d->curMask->ldmResourceParameters.getValue()->fixedResolution = TRUE;
        d->curMask->ldmResourceParameters.getValue()->resolution = 0;
        d->curMask->usePalettedTexture = TRUE;
        for(auto i=0;i<3;i++) {
            d->drawer[i]->SetTargetVolume(d->curMask);
        }
        d->drawer3d->SetTargetVolume(d->curMask);
        d->maskSocket->addChild(d->curMask);
        d->maskSocket->whichChild = 0;

        //update tf
        /*double min, max;
        d->curMask->getMinMax(min, max);

        d->tfMask->colorMap.setNum(256 * 4);

        float* p = d->tfMask->colorMap.startEditing();
        for (int i = 0; i < 256; ++i) {
            int idx = (float)i / 255.0 * max;
            if (idx < 1) {
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
                *p++ = 0.0;
            }
            else {
                float r = color_table[(idx - 1) % 20][0] / 255.0;
                float g = color_table[(idx - 1) % 20][1] / 255.0;
                float b = color_table[(idx - 1) % 20][2] / 255.0;
                *p++ = r;
                *p++ = g;
                *p++ = b;
                *p++ = 0.5;
            }
        }
        d->tfMask->colorMap.finishEditing();
        MedicalHelper::dicomAdjustDataRange(d->dataRangeMask, d->curMask);*/
    }
    //AI interactions
    auto SceneManager::AiFinishImage() -> void {
        if(d->isAiInProgress) {
            d->aiSegEngine->ReleaseMemory();
        }
        d->isAiInProgress = false;
    }
    auto SceneManager::AiResetImage() -> void {
        if (nullptr == d->curImage) {
            std::cout << "image is null" << std::endl;
            return;
        }
        if (nullptr == d->curMask) {
            std::cout << "mask is null" << std::endl;
            return;
        }
        if(false == d->isAiInProgress) {
            std::cout << "AI segmentation is not in progress" << std::endl;
            return;
        }
        auto dim = d->curImage->getDimension();
        unsigned dims[3] = { dim[0],dim[1],dim[2] };

        //get current mask with current label

        //MedicalHelper::dicomAdjustVolume(d->curMask);
        SoRef<SoMemoryDataAdapter> maskAdap = MedicalHelper::getImageDataAdapter(d->curMask);

        SoRef<SoThresholdingProcessing> threshold = new SoThresholdingProcessing;
        threshold->inImage = maskAdap.ptr();
        threshold->thresholdLevel.setValue(d->curLabelValue-0.5f, d->curLabelValue + 0.5f);

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&threshold->outBinaryImage);

        SoRef<SoVolumeData> resultMask = new SoVolumeData;
        resultMask->setReader(*reader, TRUE);

        auto bBox = SbBox3i32(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            resultMask->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);

        SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

        resultMask->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        unsigned char* mask_buffer = static_cast<unsigned char*>(dataBufferObj->map(SoBufferObject::READ_WRITE));

        unsigned char* mask_holder = new unsigned char[dims[0] * dims[1] * dims[2]];

        memcpy(mask_holder, mask_buffer, dataInfoBox.bufferSize);

        dataBufferObj->unmap();

        d->aiSegEngine->SetInputImage(static_cast<unsigned short*>(d->curImage->data.getValue()), dims, mask_holder);
    }
    auto SceneManager::AiSetImage() -> void {
        if(nullptr == d->curImage) {
            std::cout << "image is null" << std::endl;
            return;
        }
        if(nullptr == d->curMask) {
            std::cout << "mask is null" << std::endl;
            return;
        }
        auto dim = d->curImage->getDimension();
        unsigned dims[3] = { dim[0],dim[1],dim[2] };

        //get current mask with current label

        //MedicalHelper::dicomAdjustVolume(d->curMask);
        SoRef<SoMemoryDataAdapter> maskAdap = MedicalHelper::getImageDataAdapter(d->curMask);

        SoRef<SoThresholdingProcessing> threshold = new SoThresholdingProcessing;
        threshold->inImage = maskAdap.ptr();
        threshold->thresholdLevel.setValue(d->curLabelValue, d->curLabelValue + 0.5);

        SoRef<SoVRImageDataReader> reader = new SoVRImageDataReader;
        reader->imageData.connectFrom(&threshold->outBinaryImage);

        SoRef<SoVolumeData> resultMask = new SoVolumeData;
        resultMask->setReader(*reader, TRUE);

        double rmin, rmax;
        resultMask->getMinMax(rmin, rmax);

        std::cout << "resulting mask minmax" << rmin << " " << rmax << std::endl;

        auto bBox = SbBox3i32(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2] - 1));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            resultMask->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);

        SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

        resultMask->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        unsigned char* mask_buffer = static_cast<unsigned char*>(dataBufferObj->map(SoBufferObject::READ_WRITE));

        unsigned char* mask_holder = new unsigned char[dims[0] * dims[1] * dims[2]];

        memcpy(mask_holder, mask_buffer, dataInfoBox.bufferSize);

        dataBufferObj->unmap();

        d->aiSegEngine->SetInputImage(static_cast<unsigned short*>(d->curImage->data.getValue()),dims,mask_holder);
        d->isAiInProgress = true;
    }
    auto SceneManager::ToggleAiPoint(bool activate) -> void {        
        if (nullptr == d->curMask) {
            std::cout << "mask is null" << std::endl;
            return;
        }
        d->isAddingPoint = activate;
    }
    auto SceneManager::isInAiAddPoint() -> bool {
        return d->isAddingPoint;
    }
    void SceneManager::OnPointPick(float x, float y,float z, int axis) {
        if(false == d->isAddingPoint) {
            return;
        }
        auto intX = static_cast<int>(abs(x));
        auto intY = static_cast<int>(abs(y));
        auto intZ = static_cast<int>(abs(z));                
        
        if (d->is2DPoint) {
            if (axis == 0) {
                if (d->curAxis != 0 || d->curIndex != intZ) {
                    d->aiSegEngine->SelectSlice(0, intZ);
                    d->curAxis = 0;
                    d->curIndex = intZ;
                }
                d->aiSegEngine->Add2dPoint(intY, intX, d->isPositive);
            }else if(axis == 1) {
                if (d->curAxis != 1 || d->curIndex != intX) {//2-YZ
                    d->aiSegEngine->SelectSlice(2, intX);
                    d->curAxis = 1;
                    d->curIndex = intX;
                }
                d->aiSegEngine->Add2dPoint(intZ,intY , d->isPositive);
            }else {
                if (d->curAxis != 2 || d->curIndex != intY) {//1-XZ
                    d->aiSegEngine->SelectSlice(1, intY);
                    d->curAxis = 2;
                    d->curIndex = intY;
                }                
                d->aiSegEngine->Add2dPoint(intZ, intX, d->isPositive);
            }            
        }else {
            d->aiSegEngine->Add3dPoint(intX, intY, intZ, d->isPositive);            
        }
        PostProcAi();
    }
    auto SceneManager::PostProcAi() -> void {
        if (nullptr == d->curMask) {
            std::cout << "mask is null" << std::endl;
            return;
        }
        //d->aiSegEngine->SetValue(1);
        d->aiSegEngine->SetValue(d->curLabelValue);
        auto dim = d->curMask->getDimension();
        long long size = 1;
        SbBox3i32 bBox;
        size = dim[0] * dim[1] * dim[2];
        bBox = SbBox3i32(SbVec3i32(0, 0, 0), SbVec3i32(dim[0] - 1, dim[1] - 1, dim[2]-1));
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            d->curMask->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);

        SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

        d->curMask->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        unsigned short* buffer = static_cast<unsigned short*>(dataBufferObj->map(SoBufferObject::READ_WRITE));

        unsigned short* cur_buf = new unsigned short[size];

        //d->aiSegEngine->GetOutputMask(buffer, size);
        d->aiSegEngine->GetOutputMask(cur_buf, size);

        for(auto i=0;i<size;i++) {
            if (cur_buf[i] > 0 || buffer[i] == d->curLabelValue) {
                buffer[i] = cur_buf[i];
            }
        }

        dataBufferObj->unmap();

        int transaction;
        d->curMask->startEditing(transaction);
        d->curMask->editSubVolume(bBox, dataBufferObj.ptr());
        d->curMask->finishEditing(transaction);
        //d->curMask->saveEditing();

        //d->curMask->touch();

        delete[] cur_buf;
    }    
    auto SceneManager::ToggleAi2D(bool is2D) -> void {
        d->is2DPoint = is2D;
    }
    auto SceneManager::ToggleAiPositive(bool isPositive) -> void {
        d->isPositive = isPositive;
    }
    auto SceneManager::AiSliceRecommendation() -> void {
        auto recom_info = d->aiSegEngine->GetSliceRecommendation();
        auto recom_axis = std::get<0>(recom_info);
        auto recom_index = std::get<1>(recom_info);

        std::cout << "recom_axis: " << recom_axis << std::endl;
        std::cout << "recom_index: " << recom_index << std::endl;

        auto real_axis = recom_axis;
        if(real_axis == 1) {
            real_axis = 2;
        }else if(real_axis==2) {
            real_axis = 1;
        }                

        emit sigAiSlide(real_axis, recom_index);
    }
    auto SceneManager::AiGenerateVolume() -> void {
        d->aiSegEngine->Generate3dMask();
        PostProcAi();
    }
    auto SceneManager::AiAddSlice()->void {
        if(nullptr == d->curMask) {
            return;
        }
        auto curVal = 0;
        auto curAxis = 0;
        unsigned dim[3];
        dim[2] = 1;
        auto dims = d->curMask->getDimension();
        SbBox3i32 bBox;
        if(d->curLargeAxis==0) {
            curVal = d->curIndices[2];
            dim[0] = dims[1];
            dim[1] = dims[0];
            bBox = SbBox3i32(SbVec3i32(0, 0, curVal), SbVec3i32(dims[0] - 1, dims[1] - 1, curVal));
        }else if(d->curLargeAxis==1) {
            curVal = d->curIndices[0];
            curAxis = 2;
            dim[0] = dims[2];
            dim[0] = dims[1];
            bBox = SbBox3i32(SbVec3i32(curVal, 0, 0), SbVec3i32(curVal, dims[1] - 1, dims[2]-1));
        }else {
            curVal = d->curIndices[1];
            curAxis = 1;
            dim[0] = dims[2];
            dim[0] = dims[0];
            bBox = SbBox3i32(SbVec3i32(0, curVal, 0), SbVec3i32(dims[0] - 1, curVal, dims[2] - 1));
        }

        std::shared_ptr<unsigned short> volume(new unsigned short[dim[0] * dim[1] * dim[2]](), std::default_delete<unsigned short[]>());                
        SoLDMDataAccess::DataInfoBox dataInfoBox =
            d->curMask->getLdmDataAccess().getData(0, bBox, (SoBufferObject*)NULL);
        SoRef<SoCpuBufferObject> dataBufferObj = new SoCpuBufferObject;
        dataBufferObj->setSize((size_t)dataInfoBox.bufferSize);

        d->curMask->getLdmDataAccess().getData(0, bBox, dataBufferObj.ptr());
        unsigned short* mask_buffer = (unsigned short*)dataBufferObj->map(SoBufferObject::READ_ONLY);

        d->aiSegEngine->SelectSlice(curAxis, curVal);
        d->curAxis = d->curLargeAxis;
        d->curIndex = curVal;

        //get current mask slice
        d->aiSegEngine->SetCustomSlice(mask_buffer, dim);

        dataBufferObj->unmap();
    }
    auto SceneManager::SetCurLarge(int idx) -> void {
        d->curLargeAxis = idx;        
    }

}