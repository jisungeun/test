#pragma once

#include <memory>

namespace TomoAnalysis::InterSeg::Plugins {
    class ToolManager {
    public:
        typedef ToolManager Self;
        typedef std::shared_ptr<Self> Pointer;

        ToolManager();
        ~ToolManager();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}