#pragma once

#include <memory>

#include <QColor>

#include <ISceneManagerWidget.h>

#include "SceneManagerExport.h"

class SoVolumeData;
class SoPerspectiveCamera;
class SoSeparator;

namespace TomoAnalysis::InterSeg::Plugins {    
    class SceneManager_API SceneManager : public Interactor::ISceneManagerWidget {
    Q_OBJECT
    public:
        typedef SceneManager Self;
        typedef std::shared_ptr<Self> Pointer;

        SceneManager();
        ~SceneManager();

        auto SetImage(TCImage::Pointer& image,QString name) -> bool override;
        auto SetMask(TCMask::Pointer& mask, QString name) -> bool override;
        auto RemoveMask(QString name) -> bool override;
        auto AddVizMask(TCMask::Pointer& mask, QString name) -> bool override;
        auto RemoveVizMask(QString name) -> bool override;
        auto ClearVizMask() -> bool override;
        auto GenerateVizMask() -> bool override;
        auto GetMask()->TCMask::Pointer override;
        auto RearrangeLabel() -> void override;

        auto ActivateTool(int idx) -> bool override;
        auto DeactivateTool(bool isFunc = false) -> bool override;

        //None-Clean Architecture
        auto Get2DSceneGraph(int idx)->SoSeparator*;
        auto Get3DSceneGraph()->SoSeparator*;
        auto Set3DCamera(SoPerspectiveCamera* cam)->void;

        auto SetSliceIndex(int viewIndex, int sliceIndex)->void;

        auto SetPaintBrushSize(int size)->void;
        auto SetWipeBrushSize(int size)->void;
        auto SetLabelValue(int value)->void;
        auto AddLabel(int value)->bool;
        auto SetSizeIndex(int idx)->void;

        //FL Controls
        auto SetFLImage(TCImage::Pointer& flImage,int ch,float offset)->bool;
        auto SetFLOpacity(double opacity)->bool;
        auto SetFLColor(QColor color)->bool;

        //Instance processing (no mask type conversion)
        auto PerformDilate(int label = -1) ->bool;
        auto PerformErode(int label = -1)->bool;
        auto PerformWaterShed(TCMask::Pointer target)->bool;

        //Instance History processing
        auto TryUndo()->void;
        auto TryRedo()->void;
        auto TryRefresh()->void;

        //Instance AI mask generation processing
        auto AiSetImage()->void;
        auto AiResetImage()->void;
        auto AiFinishImage()->void;
        auto ToggleAiPoint(bool activate)->void;
        auto ToggleAi2D(bool is2D)->void;
        auto ToggleAiPositive(bool isPositive)->void;
        auto AiSliceRecommendation()->void;
        auto AiGenerateVolume()->void;
        auto AiAddSlice()->void;
        auto SetCurLarge(int idx)->void;
        auto isInAiAddPoint()->bool;
        void OnPointPick(float x, float y, float z, int axis);

        auto Reset() -> void override;

    signals:
        void sigLabel(int);
        void sigStart3dDrawing();
        void sigFinish3dDrawing();
        void sigStartSizeFilter(int);
        void sigAiSlide(int, int);
        void sigAiPointOff();

    protected slots:
        void OnLabelPicking(int);
        void OnFinishMerge(int);
        void OnStart3dDrawing();
        void OnFinish3dDrawing();
        void OnStart3dBranching();
        void OnFinish3dBranching();
        void OnHistory(int);
        void OnLabelRemove();
        void OnStartSize(int);                

    private:
        auto Init()->void;
        auto InitDataGroups()->void;
        auto Init3DScene()->void;
        auto Init2DScenes()->void;
        auto Init2DDrawers()->void;

        auto ResetAll()->void;
        auto ResetMask(bool soft = false)->void;

        auto SetImageToScene()->void;
        auto SetMaskToScene()->void;

        auto PerformMergeLabel()->void;
        auto FlushMergeLabel()->void;
        auto PerformRemoveSize()->void;
        auto PerformSeparate()->void;

        auto convertFL(SoVolumeData* oriFL,float offset)->void;
        auto CreateLabelTextMap()->void;
        auto CreateIsoSurface()->void;
        auto CreateMarchingCubes()->void;

        auto PostProcAi()->void;        

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}