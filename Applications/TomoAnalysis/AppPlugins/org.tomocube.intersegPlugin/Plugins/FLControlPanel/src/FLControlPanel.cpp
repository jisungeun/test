#include <iostream>
#include <QColorDialog>

#include "ui_FLControlPanel.h"
#include "FLControlPanel.h"

namespace TomoAnalysis::InterSeg::Plugins {
    struct FLControlPanel::Impl {
        Ui::FLControlForm* ui{ nullptr };
    };
    FLControlPanel::FLControlPanel(QWidget* parent) : d{ new Impl }, QWidget(parent) {
        d->ui = new Ui::FLControlForm();
        d->ui->setupUi(this);

        this->Init();
    }
    FLControlPanel::~FLControlPanel() {
        
    }
    auto FLControlPanel::Reset() -> void {
        auto ds = GetDS();
        for(auto i=0;i<3;i++) {
            ds->chOpa[i] = 0.5;
            ds->chExist[i] = false;            
        }
        ds->color[0][0] = 0;
        ds->color[0][1] = 0;
        ds->color[0][2] = 255;
        ds->color[1][0] = 0;
        ds->color[1][1] = 255;
        ds->color[1][2] = 0;
        ds->color[2][0] = 255;
        ds->color[2][1] = 0;
        ds->color[2][2] = 0;
        ds->curCh = -1;
        Update();
    }

    auto FLControlPanel::Update() -> bool {
        auto ds = GetDS();
        d->ui->ch0Rad->setEnabled(ds->chExist[0]);
        d->ui->ch1Rad->setEnabled(ds->chExist[1]);
        d->ui->ch2Rad->setEnabled(ds->chExist[2]);

        if(ds->curCh < 0) {
            d->ui->opaSpin->setEnabled(false);
            d->ui->opaSlider->setEnabled(false);
            d->ui->opaSpin->blockSignals(true);
            d->ui->opaSpin->setValue(0.0);
            d->ui->opaSpin->blockSignals(false);
            d->ui->opaSlider->blockSignals(true);
            d->ui->opaSlider->setValue(0);
            d->ui->opaSlider->blockSignals(false);
            d->ui->noneRad->blockSignals(true);
            d->ui->noneRad->setChecked(true);
            d->ui->noneRad->blockSignals(false);
        }
        else {
            d->ui->opaSpin->setEnabled(ds->chExist[ds->curCh]);
            d->ui->opaSlider->setEnabled(ds->chExist[ds->curCh]);
            d->ui->opaSpin->setValue(ds->chOpa[ds->curCh]);
        }

        return true;
    }
    auto FLControlPanel::Init() -> void {
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->contentsWidget->setObjectName("panel-contents");

        d->ui->opaSlider->setRange(0, 100);
        d->ui->opaSpin->setRange(0.0, 1.0);
        d->ui->opaSpin->setSingleStep(0.01);

        d->ui->opaSlider->setEnabled(false);
        d->ui->opaSpin->setEnabled(false);

        d->ui->ch0Rad->setEnabled(false);
        d->ui->ch1Rad->setEnabled(false);
        d->ui->ch2Rad->setEnabled(false);
        d->ui->noneRad->setChecked(true);

        d->ui->colorBtn->setObjectName("bt-round-gray500");
        d->ui->label->setObjectName("h8");

        connect(d->ui->opaSlider, SIGNAL(valueChanged(int)), this, SLOT(OnOpaSliderChanged(int)));
        connect(d->ui->opaSpin, SIGNAL(valueChanged(double)), this, SLOT(OnOpacityChanged(double)));
        connect(d->ui->ch0Rad, SIGNAL(clicked()), this, SLOT(OnRadioClicked()));
        connect(d->ui->ch1Rad, SIGNAL(clicked()), this, SLOT(OnRadioClicked()));
        connect(d->ui->ch2Rad, SIGNAL(clicked()), this, SLOT(OnRadioClicked()));
        connect(d->ui->noneRad, SIGNAL(clicked()), this, SLOT(OnRadioClicked()));
        connect(d->ui->colorBtn, SIGNAL(clicked()), this, SLOT(OnColorClicked()));
    }
    void FLControlPanel::OnRadioClicked() {        
        auto ch = -1;
        if(sender() == d->ui->ch0Rad) {
            ch = 0;
        }else if(sender() == d->ui->ch1Rad) {
            ch = 1;
        }else if(sender() == d->ui->ch2Rad) {
            ch = 2;
        }
        emit flChSelected(ch);
        if (ch > -1) {
            auto ds = GetDS();
            QColor color;
            color.setRed(ds->color[ch][0]);
            color.setGreen(ds->color[ch][1]);
            color.setBlue(ds->color[ch][2]);
            emit flColorChanged(ch, color);
            emit flOpacity(ch, ds->chOpa[ch]);
        }
    }
    void FLControlPanel::OnOpaSliderChanged(int slide) {
        auto dval = static_cast<double>(slide)/ 100.0;        
        d->ui->opaSpin->setValue(dval);
    }
    void FLControlPanel::OnColorClicked() {
        auto ds = GetDS();        
        auto ch = ds->curCh;        
        if (ch < 0) {
            return;
        }
        QColor new_col = QColorDialog::getColor(QColor(ds->color[ch][0], ds->color[ch][1], ds->color[ch][2], 255), nullptr, "Select Color");
        if(new_col.isValid()) {
            ds->color[ch][0] = new_col.red();
            ds->color[ch][1] = new_col.green();
            ds->color[ch][2] = new_col.blue();
            emit flColorChanged(ch, new_col);
        }
    }
    void FLControlPanel::OnOpacityChanged(double val) {
        auto intVal = static_cast<int>(val * 100.0);
        d->ui->opaSlider->blockSignals(true);
        d->ui->opaSlider->setValue(intVal);
        d->ui->opaSlider->blockSignals(false);                        

        auto ch = -1;
        if(d->ui->ch0Rad->isChecked()) {
            ch = 0;//blue
        }else if(d->ui->ch1Rad->isChecked()) {
            ch = 1;//green
        }else if(d->ui->ch2Rad->isChecked()) {
            ch = 2;//red
        }

        emit flOpacity(ch, val);
    }
}
