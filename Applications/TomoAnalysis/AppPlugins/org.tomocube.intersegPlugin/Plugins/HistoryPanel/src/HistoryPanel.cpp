#include <QIcon>

#include "ui_HistoryPanel.h"
#include "HistoryPanel.h"

namespace TomoAnalysis::InterSeg::Plugins {
    struct HistoryPanel::Impl {
        Ui::HistoryForm* ui{ nullptr };
        QIcon UndoIcon;
        QIcon RedoIcon;
        QIcon RefreshIcon;
    };
    HistoryPanel::HistoryPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::HistoryForm;
        d->ui->setupUi(this);
        this->InitIcons();
        this->Init();
        this->InitToolTips();
    }
    HistoryPanel::~HistoryPanel() = default;
    auto HistoryPanel::Reset() -> void {
        Update();
    }

    auto HistoryPanel::Update() -> bool {
        return true;
    }
    void HistoryPanel::OnRedoClicked() {
        emit sigRedo();
    }
    void HistoryPanel::OnUndoClicked() {
        emit sigUndo();
    }
    void HistoryPanel::OnRefreshClicked() {
        emit sigRefresh();//clear history
    }    
    auto HistoryPanel::Init() -> void {
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->contentsWidget->setObjectName("panel-contents");
        connect(d->ui->UndoBtn, SIGNAL(clicked()), this, SLOT(OnUndoClicked()));
        connect(d->ui->RedoBtn, SIGNAL(clicked()), this, SLOT(OnRedoClicked()));
        connect(d->ui->RefreshBtn, SIGNAL(clicked()), this, SLOT(OnRefreshClicked()));
    }
    auto HistoryPanel::InitToolTips() -> void {
        d->ui->UndoBtn->setToolTip("Undo");
        d->ui->RedoBtn->setToolTip("Redo");
        d->ui->RefreshBtn->setToolTip("Flush undo/redo history");
    }

    auto HistoryPanel::InitIcons() -> void {
        d->UndoIcon = QIcon(":/image/images/Undo.png");
        d->ui->UndoBtn->setIcon(d->UndoIcon);
        d->ui->UndoBtn->setIconSize(QSize(52, 52));

        d->RedoIcon = QIcon(":/image/images/Redo.png");
        d->ui->RedoBtn->setIcon(d->RedoIcon);
        d->ui->RedoBtn->setIconSize(QSize(52, 52));

        d->RefreshIcon = QIcon(":/image/images/Refresh.png");
        d->ui->RefreshBtn->setIcon(d->RefreshIcon);
        d->ui->RefreshBtn->setIconSize(QSize(52, 52));
    }
}