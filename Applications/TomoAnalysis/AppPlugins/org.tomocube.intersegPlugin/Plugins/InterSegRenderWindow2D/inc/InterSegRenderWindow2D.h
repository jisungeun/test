#pragma once

#include <QOivRenderWindow.h>

#include "InterSegRenderWindow2DExport.h"

class SoEventCallback;

namespace TomoAnalysis::InterSeg::Plugins {
    class InterSegRenderWindow2D_API InterSegRenderWindow2D : public QOivRenderWindow {
        Q_OBJECT
    public:
        InterSegRenderWindow2D(QWidget* parent);
        ~InterSegRenderWindow2D();

        auto closeEvent(QCloseEvent* unused) -> void override;

        auto reset2DView(bool fromFunc = false)->void;
        auto setRenderWindowID(int idx)->void;
        auto setType(bool type)->void;
        auto refreshRangeSlider()->void;
        auto setHTRange(double min, double max)->void;
        auto GetID()->int;
        auto SetInteractionMode(bool isNavi)->void;

    signals:
        void sigWheel(float);
        void sig2dCoord(float, float, float);

    protected slots:
        void lowerLevelChanged(int);
        void upperLevelChanged(int);
        void lowerSpinChanged(double);
        void upperSpinChanged(double);

    private:
        auto MouseButtonEvent(SoEventCallback* node) -> void override;
        auto MouseMoveEvent(SoEventCallback* node) -> void override;
        auto MouseWheelEvent(SoEventCallback* node) -> void override;
        auto KeyboardEvent(SoEventCallback* node) -> void override;

        auto setDefaultWindowType() -> void override;

        auto showContextMenu(SbVec2f pos)->void;

        auto change2DColorMap(int idx)->void;
        auto initRangeSlider()->void;

        auto CalcVolumeCoord(SbVec2f norm_point)->SbVec3f;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}