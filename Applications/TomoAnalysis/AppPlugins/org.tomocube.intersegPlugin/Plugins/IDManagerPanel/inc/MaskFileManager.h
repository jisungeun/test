#pragma once

#include <memory>
#include <QObject>

namespace TomoAnalysis::InterSeg::Plugins {
    class MaskFileManager : public QObject {
        Q_OBJECT
    public:
        typedef MaskFileManager Self;
        typedef std::shared_ptr<Self> Pointer;

        MaskFileManager(QObject* parent = nullptr);
        ~MaskFileManager();

        auto SetImagePath(QString img)->void;
        auto GetImagePath()->QString;
        auto SetMaskPath(const QString& path)->void;
        auto GetMaskPath()->QString;
        auto GetParentFolder()->QString;

        auto ScanDir()->void;
        auto GetScannedList()->QStringList;

        auto GetInstancePath(QString ID,bool isInst)->QString;

        auto Clear()->void;

    private:
        auto isValid(QString path)->bool;
        auto isSystem(QString path)->bool;
        auto scanWithBase(QString base)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}