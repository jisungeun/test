#include <QMessageBox>
#include <QRegExpValidator>

#include "ui_NewIdDialog.h"
#include "NewIdDialog.h"

namespace TomoAnalysis::InterSeg::Plugins {
    struct NewIdDialog::Impl {
        Ui::NewIdDialog* ui{ nullptr };
    };

    NewIdDialog::NewIdDialog(QWidget* parent)
        : QDialog(parent)
        , d(new Impl()) {

        d->ui = new Ui::NewIdDialog();
        d->ui->setupUi(this);        

        // set object names
        d->ui->titleLabel->setObjectName("label-title-dialog");                
        d->ui->userIDName->setObjectName("input-high");
        d->ui->okButton->setObjectName("bt-square-primary");
        d->ui->cancelButton->setObjectName("bt-square-line");

        QRegExp re("[a-zA-Z\\.\\-\\_0-9]+");
        d->ui->userIDName->setValidator(new QRegExpValidator(re));
    }

    NewIdDialog::~NewIdDialog() = default;

    auto NewIdDialog::NewID(QWidget* parent)->QString {
        NewIdDialog dialog(parent);

        if(dialog.exec() != QDialog::Accepted) {
            return QString();
        }

        const auto newName = dialog.GetNewName();

        if(newName == "System") {
            return QString();
        }

        if(newName == "copy") {
            return QString();
        }

        return newName;
    }

    auto NewIdDialog::GetNewName(void) const ->QString {
        return d->ui->userIDName->text();
    }

    void NewIdDialog::on_okButton_clicked() {
        if (true == d->ui->userIDName->text().isEmpty()) {
            QMessageBox::warning(nullptr, "Enter a new playground name", "Empty or invalid playground name");
            return;
        }

        this->accept();
    }

    void NewIdDialog::on_cancelButton_clicked() {
        this->reject();
    }
}
