#include <iostream>

#include <QRegExpValidator>
#include <QUrl>
#include <QDesktopServices>
#include <QDir>
#include <QMessageBox>

#include "MaskFileManager.h"
#include "NewIdDialog.h"

#include "ui_IDManagerPanel.h"
#include "IDManagerPanel.h"

namespace TomoAnalysis::InterSeg::Plugins {
    struct IDManagerPanel::Impl {
        Ui::IDForm* ui{ nullptr };
        MaskFileManager* manager{nullptr};
        QIcon FolderIcon;
    };
    IDManagerPanel::IDManagerPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::IDForm();
        d->ui->setupUi(this);

        this->Init();
        d->manager = new MaskFileManager;
    }
    IDManagerPanel::~IDManagerPanel() = default;
    auto IDManagerPanel::Reset() -> void {
        auto ds = GetDS();
        ds->idList.clear();
        ds->curMaskPath = QString();
        ds->curUser = QString();

        Update();
    }
    auto IDManagerPanel::Update() -> bool {
        //ID list update code
        auto ds = GetDS();

        d->manager->Clear();

        //enabler
        d->manager->SetImagePath(ds->imagePath);
        d->manager->SetMaskPath(ds->curMaskPath);

        d->manager->ScanDir();
        auto scan = d->manager->GetScannedList();
        if(scan.count()>0) {
            ds->idList = scan;            
        }
        d->ui->IDCombo->blockSignals(true);
        d->ui->IDCombo->clear();
        for (auto n : ds->idList) {
            d->ui->IDCombo->addItem(n);
        }
        d->ui->IDCombo->setCurrentIndex(0);
        if (!ds->curUser.isEmpty()) {            
            auto idx = d->ui->IDCombo->findText(ds->curUser);
            if (idx > 0) {
                d->ui->IDCombo->setCurrentIndex(idx);
            }
        }
        if (d->ui->IDCombo->count() > 0) {
            d->ui->copyIdBtn->setEnabled(true);
        }
        else {
            d->ui->copyIdBtn->setEnabled(false);
        }
        d->ui->IDCombo->blockSignals(false);
        ds->workindDir = d->manager->GetParentFolder();

        if (d->ui->IDCombo->currentText() == "System") {
            OnIdChanged(d->ui->IDCombo->currentIndex());
        }else {
            ds->curUser = d->ui->IDCombo->currentText();
        }

        return true;
    }    
    auto IDManagerPanel::IDExist(QStringList prevIDs, QString newID)->bool {
        for (auto prev : prevIDs) {
            if (prev.toLower() == newID.toLower()) {
                return true;
            }
        }
        return false;
    }
    void IDManagerPanel::OnNewIdClicked() {        
        auto ds = GetDS();
        QStringList curList = ds->idList;
        const auto newID = NewIdDialog::NewID(this);

        if(newID.isEmpty()) {
            QMessageBox::warning(nullptr, "Error", "ID is invalid!");
            return;
        }
        if(IDExist(curList,newID)){
            QMessageBox::warning(nullptr, "Error", "ID already exist!");
            return;            
        }

        d->ui->IDCombo->blockSignals(true);        
        d->ui->IDCombo->addItem(newID);
        d->ui->IDCombo->setCurrentIndex(d->ui->IDCombo->count() - 1);
        ds->idList.push_back(newID);
        ds->curUser = newID;
        d->ui->IDCombo->blockSignals(false);                

        auto mask = d->manager->GetInstancePath(newID, true);

        emit sigIdChanged(newID,mask);
    }
    void IDManagerPanel::OnCopyIdClicked() {
        auto ds = GetDS();
        QStringList curList = ds->idList;
        const auto newID = NewIdDialog::NewID(this);

        if (newID.isEmpty()) {
            QMessageBox::warning(nullptr, "Error", "ID is invalid!");
            return;
        }
        if (IDExist(curList,newID)) {
            QMessageBox::warning(nullptr, "Error", "ID already exist!");
            return;
        }

        auto prevUsr = ds->curUser;        

        d->ui->IDCombo->blockSignals(true);
        d->ui->IDCombo->addItem(newID);
        d->ui->IDCombo->setCurrentIndex(d->ui->IDCombo->count() - 1);
        ds->idList.push_back(newID);
        ds->curUser = newID;
        d->ui->IDCombo->blockSignals(false);                        
                
        auto prevPath = d->manager->GetInstancePath(prevUsr, true);
        std::cout <<"prevpath: "<< prevPath.toStdString() << std::endl;
        auto newPath = d->manager->GetInstancePath(newID, true);
        std::cout << "newpath: " << newPath.toStdString() << std::endl;

        QFileInfo pi(prevPath);
        if(pi.exists()) {
            QFile::copy(prevPath, newPath);
            CopyPath(prevPath.chopped(4), newPath.chopped(4));
        }        

        emit sigIdChanged(newID, newPath);
    }
    auto IDManagerPanel::CopyPath(QString src, QString dst) -> void {
        QDir dir(src);
        if (!dir.exists())
            return;

        foreach(QString dd, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
            QString dst_path = dst + QDir::separator() + dd;
            dir.mkpath(dst_path);
            CopyPath(src + QDir::separator() + dd, dst_path);
        }

        foreach(QString f, dir.entryList(QDir::Files)) {
            QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
        }
    }

    void IDManagerPanel::OnIdChanged(int idx) {
        auto candidate = d->ui->IDCombo->itemText(idx);
        auto ds = GetDS();
        ds->curUser = candidate;

        auto path = d->manager->GetInstancePath(candidate, true);

        emit sigIdChanged(candidate,path);
    }
    void IDManagerPanel::OnOpenWorkingDir() {
        auto ds = GetDS();
        auto dir = ds->workindDir;
        if(dir.isEmpty()) {
            QMessageBox::warning(nullptr, "Error", "No working directory!");
            return;
        }
        QDir qdir(dir);
        if(!qdir.exists()) {
            QMessageBox::warning(nullptr, "Error", "Invalid working directory!");
            return;
        }        
        QDesktopServices::openUrl(QUrl::fromLocalFile(dir));
    }
    auto IDManagerPanel::Init() -> void {
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->contentsWidget->setObjectName("panel-contents");
        connect(d->ui->newIdBtn, SIGNAL(clicked()), this, SLOT(OnNewIdClicked()));
        connect(d->ui->copyIdBtn, SIGNAL(clicked()), this, SLOT(OnCopyIdClicked()));
        connect(d->ui->IDCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(OnIdChanged(int)));
        connect(d->ui->workDirBtn, SIGNAL(clicked()), this, SLOT(OnOpenWorkingDir()));
        d->ui->copyIdBtn->setEnabled(false);
        d->FolderIcon = QIcon(":/image/images/Folder.png");        

        d->ui->workDirBtn->setObjectName("bt-round-tool");
        d->ui->copyIdBtn->setObjectName("bt-round-gray500");
        d->ui->newIdBtn->setObjectName("bt-round-gray500");

        d->ui->workDirBtn->setIcon(d->FolderIcon);
        d->ui->workDirBtn->setIconSize(QSize(25, 25));
    }
}