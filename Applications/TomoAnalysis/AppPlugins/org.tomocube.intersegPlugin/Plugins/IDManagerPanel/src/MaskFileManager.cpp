#include <iostream>
#include <QDirIterator>
#include <QFileInfo>
#include <QString>

#include "MaskFileManager.h"

namespace TomoAnalysis::InterSeg::Plugins {
    struct MaskFileManager::Impl {        
        QString mask;
        QString image;
        QString baseName;
        QStringList scanList;

        QString parentFolder;
    };
    MaskFileManager::MaskFileManager(QObject* parent) : QObject(parent), d{ new Impl } {
        
    }
    MaskFileManager::~MaskFileManager() {
        
    }
    auto MaskFileManager::GetParentFolder() -> QString {
        return d->parentFolder;
    }
    auto MaskFileManager::Clear() -> void {        
        d->image = QString();
        d->baseName = QString();
        d->scanList.clear();
        d->parentFolder = QString();
    }

    auto MaskFileManager::SetImagePath(QString img) -> void {
        d->image = img;
    }
    auto MaskFileManager::GetImagePath() -> QString {
        return d->image;
    }
    auto MaskFileManager::SetMaskPath(const QString& path)->void {
        d->mask = path;
    }
    auto MaskFileManager::GetMaskPath()->QString {
        return d->mask;
    }
    auto MaskFileManager::GetScannedList() -> QStringList {
        return d->scanList;
    }
    auto MaskFileManager::ScanDir() -> void {
        //get base image name
        d->scanList.clear();
        d->parentFolder = QString();

        if (d->image.isEmpty()) {
            return;
        }
        QFileInfo imgInfo(d->image);
        if(false == imgInfo.exists()) {
            return;
        }
        QDir imgDir(d->image);
        imgDir.cdUp();
        d->parentFolder = imgDir.path();
        d->baseName = imgInfo.fileName().chopped(4);        

        auto startScan = false;

        //scan user for inst mask

        if (false == d->mask.isEmpty()) {
            if (isValid(d->mask)) {
                if (d->mask.contains(d->baseName)) {
                    startScan = true;
                }
            }
        }              
        if (startScan) {
            scanWithBase(d->baseName);
        }
    }
    auto MaskFileManager::scanWithBase(QString base) -> void {
        QString rootdir;
        QDir maskPath(d->mask);
        maskPath.cdUp();
        if (maskPath.exists()) {
            rootdir = maskPath.path();
        }
        else {
            return;
        }
        if (rootdir.isEmpty()) {
            return;
        }
        d->parentFolder = rootdir;
        QDirIterator it(rootdir, QStringList() << "*.msk", QDir::Files);
        while(it.hasNext()) {
            auto file_path = it.next();
            if(file_path.contains(base)) {
                auto tails = file_path.split(base)[1].chopped(4);
                auto split = tails.split("_");
                auto last_word = split[split.count() - 1];
                if (tails.isEmpty()) {
                    if (false == d->scanList.contains("System")) {
                        d->scanList.push_back("System");
                    }
                }
                else if(last_word !="bak") {//ignore backupfile
                    //mask made by some user
                    if(false == d->scanList.contains(last_word)) {
                        d->scanList.push_back(last_word);                        
                    }
                }                
            }
        }
    }    
    auto MaskFileManager::isSystem(QString path) -> bool {
        return true;
    }
    auto MaskFileManager::GetInstancePath(QString ID,bool isInst) -> QString {
        QString resultingPath = QString();
        auto isSystem = (ID == "System");
        if (d->mask.isEmpty()) {
            resultingPath += d->image.chopped(4);
            if (isSystem) {
                resultingPath += ".msk";
            }
            else {
                resultingPath += "_" + ID + ".msk";
            }
            return resultingPath;
        }
        QFileInfo fileInfo(d->mask);
        auto filename = fileInfo.fileName();
        auto filedir = fileInfo.dir().path();
        //auto tails = d->mask.chopped(4);
        auto tails = filename.chopped(4);
        auto split = tails.split("_");
        auto last_word = split[split.count() - 1];
        if (split.count() == 1) {
            if (isSystem) {
                resultingPath = filedir + "/" + tails + ".msk";
            }
            else {
                resultingPath = filedir + "/" + tails + "_" + ID + ".msk";
            }
        }
        else if (last_word != "bak") {
            if (isSystem) {
                resultingPath = filedir + "/" + tails.chopped(last_word.length() + 1) + ".msk";
            }
            else {
                resultingPath = filedir + "/" + tails.chopped(last_word.length() + 1) + "_" + ID + ".msk";
            }
        }
        else {
            if (isSystem) {
                resultingPath = filedir + "/" + tails.chopped(4) + ".msk";
            }
            else {
                resultingPath = filedir + "/" + tails.chopped(4) + "_" + ID + ".msk";
            }
        }
        return resultingPath;
    }
    auto MaskFileManager::isValid(QString path) -> bool {
        QFileInfo info(path);
        if(!info.exists()) {
            return false;
        }        
        return (info.suffix() == "msk");
    }
}