#pragma once

#include <memory>
#include <QWidget>

#include <IDrawingToolPanel.h>

#include "DrawingToolPanelExport.h"

namespace TomoAnalysis::InterSeg::Plugins {
    class DrawingToolPanel_API DrawingToolPanel : public QWidget, public Interactor::IDrawingToolPanel {
        Q_OBJECT
    public:
        typedef DrawingToolPanel Self;
        typedef  std::shared_ptr<DrawingToolPanel> Pointer;

        DrawingToolPanel(QWidget* parent = nullptr);
        ~DrawingToolPanel();

        auto Update() -> bool override;
        auto Reset() -> void override;
    signals:
        void sigAdd(bool);
        void sigSub(bool);
        void sigPaint(bool);
        void sigWipe(bool);
        void sigFill(bool);
        void sigErase(bool);

        void sigBrushSizeChanged(int);

    protected slots:
        void OnSegButtonClicked(int);        
        void OnBrushSpinChanged(int);
        void OnBrushSliderChanged(int);

    private:
        auto InitIcons()->void;
        auto Init()->void;
        auto InitToolTips()->void;
        auto ResetIcons()->void;
        auto SetIconOn()->void;

        auto SetControlWidget(int id)->void;

        struct Impl;
        std::unique_ptr<Impl> d;

    };
}