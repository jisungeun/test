#include <iostream>

#include <QtConcurrent/QtConcurrent>

#include <UIUtility.h>

#include "ui_InterSegPlayerPanel.h"
#include "InterSegPlayerPanel.h"


namespace  TomoAnalysis::InterSeg::Plugins {
    struct PlayerPanel::Impl {
        Ui::InterSegPlayerPanel* ui{ nullptr };        

        bool isProgress = false;
        int range = 0;
        int interval = 0;        
    };

    PlayerPanel::PlayerPanel(QWidget* parent)
    : QWidget(parent)    
    , d{ new Impl } {
        d->ui = new Ui::InterSegPlayerPanel();
        d->ui->setupUi(this);
        

        Init();
    }

    PlayerPanel::~PlayerPanel() {
        delete d->ui;
    }
    auto PlayerPanel::Reset(void)  ->bool {
        d->isProgress = false;
        d->range = 1;
        d->interval = 0;

        auto ds = GetDS();
        ds->range = 1;
        ds->cur_step = 0;

        Update();
        return true;
    }
    auto PlayerPanel::Update() -> bool {
        auto ds = GetDS();
        auto steps = ds->range;
        TC::SilentCall(d->ui->progressSlider)->setEnabled(false);
        TC::SilentCall(d->ui->progressSpinBox)->setEnabled(false);
        d->range = 1;
        TC::SilentCall(d->ui->progressSlider)->setValue(1);
        TC::SilentCall(d->ui->progressSpinBox)->setValue(1);
        if(1<steps) {
            d->range = steps;
            TC::SilentCall(d->ui->progressSlider)->setEnabled(true);
            TC::SilentCall(d->ui->progressSpinBox)->setEnabled(true);
            TC::SilentCall(d->ui->progressSlider)->setRange(1, d->range);
            TC::SilentCall(d->ui->progressSpinBox)->setRange(1, d->range);
        }
        if(ds->cur_step < steps) {
            TC::SilentCall(d->ui->progressSlider)->setValue(ds->cur_step+1);
            TC::SilentCall(d->ui->progressSpinBox)->setValue(ds->cur_step+1);
        }
        return true;
    }    

    auto PlayerPanel::isEnabled() -> bool {
        auto enable = d->ui->progressSlider->isEnabled();
        return enable;
    }

    auto PlayerPanel::Init(void) const ->bool {
        const auto isEnabled = false;
                
        TC::SilentCall(d->ui->progressSlider)->setEnabled(isEnabled);
        TC::SilentCall(d->ui->progressSpinBox)->setEnabled(isEnabled);                

        return true;
    }    

    auto PlayerPanel::SetSliceIndex(const int& index) const ->void {
        TC::SilentCall(d->ui->progressSpinBox)->setValue(index);
        TC::SilentCall(d->ui->progressSlider)->setValue(index);
    }   

    void PlayerPanel::on_progressSlider_valueChanged(int value) {
        if (true == d->isProgress) {
            d->isProgress = false;     
        }

        TC::SilentCall(d->ui->progressSpinBox)->setValue(value);        
        emit timelapseIndexChanged(value-1);
    }

    void PlayerPanel::on_progressSpinBox_valueChanged(int value) {
        if (true == d->isProgress) {
            d->isProgress = false;            
        }

        TC::SilentCall(d->ui->progressSlider)->setValue(value);

        emit timelapseIndexChanged(value-1);
    }    
    
}
