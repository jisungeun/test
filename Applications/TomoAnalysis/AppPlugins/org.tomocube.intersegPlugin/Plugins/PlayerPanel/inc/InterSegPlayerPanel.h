#pragma once

#include <memory>
#include <QWidget>

#include <IPlayerPanel.h>

#include "InterSegPlayerPanelExport.h"

namespace TomoAnalysis::InterSeg::Plugins {
    class InterSegPlayerPanel_API PlayerPanel : public QWidget, public Interactor::IPlayerPanel {
        Q_OBJECT
    public:
        typedef PlayerPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        PlayerPanel(QWidget* parent=nullptr);
        ~PlayerPanel();

        auto Update() -> bool override;        

        auto Init(void) const ->bool;
        auto Reset(void)  ->bool;
                
        auto SetSliceIndex(const int& index) const ->void;

        auto isEnabled(void)->bool;
    signals:
        void timelapseIndexChanged(int index);

    protected slots:
        void on_progressSlider_valueChanged(int value);
        void on_progressSpinBox_valueChanged(int value);
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}