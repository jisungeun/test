#include <iostream>
#include <QFileDialog>
#include <QIcon>
#include <QSettings>

#include <Tool.h>

#include "ui_CleanerPanel.h"
#include "CleanerPanel.h"

namespace TomoAnalysis::InterSeg::Plugins {
    struct CleanerPanel::Impl {
        Ui::CleanerForm* ui{ nullptr };
        QIcon CleanIcon;        
        QIcon CleanSelIcon;        
        QIcon FlushIcon;        
        QIcon FlushSelIcon;        

        int cur_idx{ -1 };
        bool isSystem{false};
    };
    CleanerPanel::CleanerPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::CleanerForm();
        d->ui->setupUi(this);
        this->InitIcons();
        this->Init();
        this->InitToolTips();
    }
    CleanerPanel::~CleanerPanel() {
        
    }
    auto CleanerPanel::Reset() -> void {
        auto ds = GetDS();
        ds->cur_idx = -1;
        ds->isSystem = false;
        Update();
    }
    auto CleanerPanel::Update() -> bool {
        auto ds = GetDS();
        d->cur_idx = ds->cur_idx;
        d->isSystem = ds->isSystem;
        CleanIcons();
        SetIconOn();
        return true;
    }
    void CleanerPanel::OnCleanClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigClean(d->cur_idx == Entity::ToolIdx::CLEAN);
    }
    void CleanerPanel::OnCleanSelClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigCleanSel(d->cur_idx == Entity::ToolIdx::CLEAN_SEL);
    }
    void CleanerPanel::OnFlushClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigFlush(d->cur_idx == Entity::ToolIdx::FLUSH);
    }
    void CleanerPanel::OnFlushSelClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigFlushSel(d->cur_idx == Entity::ToolIdx::FLUSH_SEL);
    }
    auto CleanerPanel::Init() -> void {
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->contentsWidget->setObjectName("panel-contents");
        connect(d->ui->CleanBtn, SIGNAL(clicked()), this, SLOT(OnCleanClicked()));
        connect(d->ui->CleanSelBtn, SIGNAL(clicked()), this, SLOT(OnCleanSelClicked()));
        connect(d->ui->FlushBtn, SIGNAL(clicked()), this, SLOT(OnFlushClicked()));
        connect(d->ui->FlushSelBtn, SIGNAL(clicked()), this, SLOT(OnFlushSelClicked()));
    }
    auto CleanerPanel::InitToolTips() -> void {
        d->ui->CleanBtn->setToolTip("Remove current slice");
        d->ui->CleanSelBtn->setToolTip("Remove selected label from current slice");
        d->ui->FlushBtn->setToolTip("Remove whole volume");
        d->ui->FlushSelBtn->setToolTip("Remove selected label");
    }

    auto CleanerPanel::InitIcons() -> void {
        d->CleanIcon = QIcon(":/image/images/Clean.png");        
        d->ui->CleanBtn->setIcon(d->CleanIcon);
        d->ui->CleanBtn->setIconSize(QSize(52, 52));

        d->CleanSelIcon = QIcon(":/image/images/CleanSelection.png");        
        d->ui->CleanSelBtn->setIcon(d->CleanSelIcon);
        d->ui->CleanSelBtn->setIconSize(QSize(52, 52));

        d->FlushIcon = QIcon(":/image/images/Flush.png");        
        d->ui->FlushBtn->setIcon(d->FlushIcon);
        d->ui->FlushBtn->setIconSize(QSize(52, 52));

        d->FlushSelIcon = QIcon(":/image/images/FlushSelection.png");        
        d->ui->FlushSelBtn->setIcon(d->FlushSelIcon);
        d->ui->FlushSelBtn->setIconSize(QSize(52, 52));
    }
    auto CleanerPanel::CleanIcons() -> void {
        d->ui->CleanSelBtn->setStyleSheet("");
        d->ui->FlushSelBtn->setStyleSheet("");
        d->ui->FlushBtn->setStyleSheet("");
        d->ui->CleanBtn->setStyleSheet("");
    }
    auto CleanerPanel::SetIconOn() -> void {        
        if(d->cur_idx == 8 ) {
            d->ui->CleanBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }
        else if (d->cur_idx == 10 ) {
            d->ui->FlushBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }
        else if(d->cur_idx == 11) {
            d->ui->FlushSelBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }else if(d->cur_idx == 9) {
            d->ui->CleanSelBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }
    }
}