#pragma once

#include <QOivRenderWindow.h>

#include "InterSegRenderWindow3DExport.h"

class SoEventCallback;

namespace TomoAnalysis::InterSeg::Plugins {
    class InterSegRenderWindow3D_API InterSegRenderWindow3D : public QOivRenderWindow {
        Q_OBJECT
    public:
        InterSegRenderWindow3D(QWidget* parent);
        ~InterSegRenderWindow3D();

        auto closeEvent(QCloseEvent* unused) -> void override;

        auto reset3DView()->void;
        auto setViewDirectionIndex(int axis)->void; // 0: x, 1: y, 2: z        
        auto setNavi(bool isNavi)->void;

    private:
        auto MouseButtonEvent(SoEventCallback* node) -> void override;
        auto MouseMoveEvent(SoEventCallback* node) -> void override;
        auto MouseWheelEvent(SoEventCallback* node) -> void override;
        auto KeyboardEvent(SoEventCallback* node) -> void override;

        auto setDefaultWindowType() -> void override;

        auto showContextMenu(SbVec2f pos)->void;
        auto ChangeBackGround(int type)->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}