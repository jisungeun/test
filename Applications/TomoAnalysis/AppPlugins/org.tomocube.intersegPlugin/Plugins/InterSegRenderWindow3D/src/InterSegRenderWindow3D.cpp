#include <QMenu>

#pragma warning(push)
#pragma warning(disable:4819)
#include <Medical/helpers/MedicalHelper.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoImage.h>
#include <Inventor/events/SoMouseWheelEvent.h>
#include <VolumeViz/nodes/SoVolumeData.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#pragma warning(pop)

#include "InterSegRenderWindow3D.h"

namespace TomoAnalysis::InterSeg::Plugins {
    struct InterSegRenderWindow3D::Impl {
        bool is_Black{ false };
        bool is_Gradient{ true };
        bool is_White{ false };

        bool mouseLeftPressed{ false };
        bool mouseMiddlePressed{ false };

        bool instantNavi{ false };
    };

    InterSegRenderWindow3D::InterSegRenderWindow3D(QWidget* parent) :
        QOivRenderWindow(parent), d{ new Impl } {
        setDefaultWindowType();
        forceArrowCursor(true);
    }

    InterSegRenderWindow3D::~InterSegRenderWindow3D() {

    }

    auto InterSegRenderWindow3D::showContextMenu(SbVec2f pos) -> void {
        //y position is inverted
        auto new_y = getQtRenderArea()->size().height() - pos[1];
        QPoint globalPos = this->mapToGlobal(QPoint(pos[0], new_y));

        QMenu myMenu;

        QMenu* bgMenu = myMenu.addMenu(tr("Background Options"));
        auto gradient = bgMenu->addAction("Gradient");
        auto black = bgMenu->addAction("Black");
        auto white = bgMenu->addAction("White");

        gradient->setCheckable(true);
        black->setCheckable(true);
        white->setCheckable(true);

        if (d->is_Gradient)
            gradient->setChecked(true);
        else if (d->is_Black)
            black->setChecked(true);
        else if (d->is_White)
            white->setChecked(true);

        QMenu* viewMenu = myMenu.addMenu(tr("View"));
        auto view_x = viewMenu->addAction("View X");
        auto view_y = viewMenu->addAction("View Y");
        auto view_z = viewMenu->addAction("View Z");
        auto view_reset = viewMenu->addAction("Reset View");

        view_x->setCheckable(false);
        view_y->setCheckable(false);
        view_z->setCheckable(false);
        view_reset->setCheckable(false);

        QAction* selectedItem = myMenu.exec(globalPos);
        if (selectedItem) {
            auto text = selectedItem->text();
            if (text.contains("Gradient")) {
                d->is_Gradient = true;
                d->is_Black = false;
                d->is_White = false;
                ChangeBackGround(0);
            }
            else if (text.contains("Black")) {
                d->is_Gradient = false;
                d->is_Black = true;
                d->is_White = false;
                ChangeBackGround(1);
            }
            else if (text.contains("White")) {
                d->is_Gradient = false;
                d->is_Black = false;
                d->is_White = false;
                ChangeBackGround(2);
            }
            else if (text.contains("Reset View")) {
                reset3DView();
            }
            else if (text.contains("View X")) {
                setViewDirectionIndex(0);
            }
            else if (text.contains("View Y")) {
                setViewDirectionIndex(1);
            }
            else if (text.contains("View Z")) {
                setViewDirectionIndex(2);
            }
        }
    }
    auto InterSegRenderWindow3D::ChangeBackGround(int type) -> void {
        SbVec3f start, end;
        switch (type) {
        case 0://gradient
            start.setValue(0.7f, 0.7f, 0.8f);
            end.setValue(0.0, 0.1f, 0.3f);
            break;
        case 1://black
            start.setValue(0.0, 0.0, 0.0);
            end.setValue(0.0, 0.0, 0.0);
            break;
        case 2://white
            start.setValue(1.0, 1.0, 1.0);
            end.setValue(1.0, 1.0, 1.0);
            break;
        }
        setGradientBackground(start, end);
    }

    auto InterSegRenderWindow3D::reset3DView() -> void {
        auto root = getSceneGraph();
        auto volData = MedicalHelper::find<SoVolumeData>(root, "volData");
        if (volData) {
            resetCameraZoom();
            MedicalHelper::orientView(MedicalHelper::AXIAL, getCamera(), volData);
            auto zoom = getCameraZoom();
            setCameraZoom(zoom * 0.7f);
        }
    }

    auto InterSegRenderWindow3D::setDefaultWindowType() -> void {
        // set default gradient background color
        setGradientBackground(
            { 0.7f, 0.7f, 0.8f },
            { 0.0f, 0.1f, 0.3f }
        );

        //set default camera type
        setCameraType(false);   // false: perspective

        //set default navigation type
    }

    auto InterSegRenderWindow3D::MouseMoveEvent(SoEventCallback* node) -> void {
        const SoLocation2Event* moveEvent = (const SoLocation2Event*)node->getEvent();
        if (isNavigation() || d->instantNavi) {
            auto pos = moveEvent->getPositionFloat();
            auto viewport_size = getViewportRegion().getViewportSizePixels();
            auto root = getSceneGraph();
            auto color = MedicalHelper::find<SoImage>(root, "icon_file");
            if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                auto image = new QImage(":/img/ic-setting-s.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            else {
                auto image = new QImage(":/img/ic-setting.svg");
                image->convertTo(QImage::Format_RGBA8888);
                color->image.setValue(SbVec2s(16, 16), 4, image->bits());
                delete image;
            }
            if (d->mouseLeftPressed) {
                //spinCamera(moveEvent->getPositionFloat());
                spinCamera(moveEvent->getNormalizedPosition(getViewportRegion()));
            }
            if (d->mouseMiddlePressed) {
                //panCamera(moveEvent->getPositionFloat());
                panCamera(moveEvent->getNormalizedPosition(getViewportRegion()));
            }
            //node->setHandled();
        }
    }

    /*void InterSegRenderWindow3D::wheelEvent(QWheelEvent* event) {
        auto delta = (event->angleDelta().y() > 0) ? 1 : -1;
        auto zoom = getCameraZoom();
        setCameraZoom(zoom + static_cast<float>(delta));
    }*/
    auto InterSegRenderWindow3D::MouseWheelEvent(SoEventCallback* node) -> void {
        const SoMouseWheelEvent* mouseWheel = (const SoMouseWheelEvent*)node->getEvent();
        auto delta = mouseWheel->getDelta() > 0 ? 1 : -1;
        auto zoom = getCameraZoom();
        setCameraZoom(zoom + static_cast<float>(delta));
    }

    auto InterSegRenderWindow3D::MouseButtonEvent(SoEventCallback* node) -> void {
        const SoMouseButtonEvent* mouseButton = (const SoMouseButtonEvent*)node->getEvent();
        //Press Event

        //Release Event
        if (!isNavigation()) {
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                d->instantNavi = true;
            }
        }
        if (isNavigation() || d->instantNavi) {
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                auto pos = mouseButton->getPositionFloat();
                auto viewport_size = getViewportRegion().getViewportSizePixels();
                if (!(pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25)) {
                    //startSpin(mouseButton->getPosition());
                    startSpin(mouseButton->getNormalizedPosition(getViewportRegion()));
                    d->mouseLeftPressed = true;
                }
            }
            if (SoMouseButtonEvent::isButtonPressEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                //startPan(mouseButton->getPosition());
                startPan(mouseButton->getNormalizedPosition(getViewportRegion()));
                d->mouseMiddlePressed = true;
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON1)) {
                auto pos = mouseButton->getPositionFloat();
                auto viewport_size = getViewportRegion().getViewportSizePixels();
                if (pos[0] > viewport_size[0] - 25 && pos[1] > viewport_size[1] - 25) {
                    showContextMenu(pos);
                }
                else {
                    d->mouseLeftPressed = false;
                }
            }
            if (SoMouseButtonEvent::isButtonReleaseEvent(mouseButton, SoMouseButtonEvent::BUTTON2)) {
                d->mouseMiddlePressed = false;
                if (d->instantNavi) {
                    d->instantNavi = false;
                }
            }
            //node->setHandled();
        }
    }

    auto InterSegRenderWindow3D::setViewDirectionIndex(int axis) -> void {
        const auto root = getSceneGraph();
        const auto volData = MedicalHelper::find<SoVolumeData>(root, "volData");
        if (nullptr == volData) {
            return;
        }
        switch (axis) {
        case 0:
            MedicalHelper::orientView(MedicalHelper::SAGITTAL, getCamera(), volData);
            break;
        case 1:
            MedicalHelper::orientView(MedicalHelper::CORONAL, getCamera(), volData);
            break;
        case 2:
            MedicalHelper::orientView(MedicalHelper::AXIAL, getCamera(), volData);
            break;
        }

        setCameraZoom(getCameraZoom() * 0.7f);
    }


    auto InterSegRenderWindow3D::KeyboardEvent(SoEventCallback* node) -> void {
        const SoEvent* keyEvent = node->getEvent();
        if (SoKeyboardEvent::isKeyPressEvent(keyEvent, SoKeyboardEvent::Key::ESCAPE)) {
            if (isNavigation()) {
                //setInteractionMode(false);
            }
            else {
                //setInteractionMode(true);
            }
            node->setHandled();
        }
    }

    auto InterSegRenderWindow3D::closeEvent(QCloseEvent* unused) -> void {
        Q_UNUSED(unused)
    }

    auto InterSegRenderWindow3D::setNavi(bool isNavi) -> void {
        setInteractionMode(isNavi);
    }

}
