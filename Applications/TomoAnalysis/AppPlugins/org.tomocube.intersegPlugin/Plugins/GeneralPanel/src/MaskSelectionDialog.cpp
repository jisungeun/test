#include <QCheckBox>
#include <QMessageBox>
#include <QRegExpValidator>

#include "ui_MaskSelectionDialog.h"
#include "MaskSelectionDialog.h"

namespace TomoAnalysis::InterSeg::Plugins {
    struct MaskSelectionDialog::Impl {
        Ui::MaskSelectionDialog* ui{ nullptr };

        QMap<QString,QCheckBox*> chkBoxList;
    };

    MaskSelectionDialog::MaskSelectionDialog(QWidget* parent)
        : QDialog(parent)
        , d(new Impl()) {

        d->ui = new Ui::MaskSelectionDialog();
        d->ui->setupUi(this);        
        setWindowTitle("Save Target Mask Selection");
        // set object names
        d->ui->titleLabel->setObjectName("label-title-dialog");                        
        d->ui->labelList->setObjectName("input-high");
        d->ui->okButton->setObjectName("bt-square-primary");
        d->ui->cancelButton->setObjectName("bt-square-line");

        QRegExp re("^(([1-9]\\d*-[1-9]\\d*)|([1-9]\\d*))(, *(([1-9]\\d*-[1-9]\\d*)|([1-9]\\d*)))*$");
        d->ui->labelList->setValidator(new QRegExpValidator(re));
        d->ui->labelList->hide();

        d->ui->InstRadio->setEnabled(false);
        d->ui->OrganRadio->setEnabled(false);

        d->ui->InstPage->setEnabled(false);
        d->ui->OrganPage->setEnabled(false);

        connect(d->ui->InstRadio, SIGNAL(clicked()), this, SLOT(OnInstRadio()));
        connect(d->ui->OrganRadio, SIGNAL(clicked()), this, SLOT(OnOrganRadio()));
        connect(d->ui->allBox, SIGNAL(stateChanged(int)), this, SLOT(OnAllCheck(int)));
    }

    MaskSelectionDialog::~MaskSelectionDialog() = default;

    auto MaskSelectionDialog::MaskSelection(QWidget* parent, bool isInst, bool isOrgan, QStringList orgList) -> SelectedMask {
        SelectedMask selection;
        MaskSelectionDialog dialog(parent);

        dialog.setOrganList(orgList);
        dialog.enableInst(isInst);
        dialog.enableOrgan(isOrgan);        

        if(dialog.exec() != QDialog::Accepted) {
            return selection;
        }
                
        const auto type = dialog.GetMaskType();
        const auto nameList = dialog.GetSelectedMaskList();

        selection.type = type;
        selection.nameList = nameList;

        return selection;
    }
    void MaskSelectionDialog::OnOrganRadio() {
        d->ui->stackedWidget->setCurrentIndex(1);
    }

    void MaskSelectionDialog::OnInstRadio() {
        d->ui->stackedWidget->setCurrentIndex(0);
    }

    void MaskSelectionDialog::OnAllCheck(int state) {
        Q_UNUSED(state)
        auto isChk = d->ui->allBox->isChecked();
        for(auto c : d->chkBoxList) {
            c->setChecked(isChk);
        }
    }

    auto MaskSelectionDialog::GetMaskType() const -> QString {
        QString type;
        if(d->ui->InstRadio->isChecked()) {
            type = "MultiLabel"; 
        }else {
            type = "MultiLayer";
        }
        return type;;
    }

    auto MaskSelectionDialog::GetSelectedMaskList() const -> QStringList {
        QStringList result;
        if(d->ui->InstRadio->isChecked()) {
            result.push_back("cellInst");
        }
        else {
            for (auto c : d->chkBoxList) {
                if (c->isChecked() && c->text()!="All") {
                    result.push_back(GetSyntheticName(c->text()));
                }
            }
        }
        return result;
    }
    auto MaskSelectionDialog::GetDisplayName(QString synName)const -> QString {
        if (synName == "cellInst") {
            return "Cell instance";
        }
        if (synName == "membrane") {
            return "Whole cell";
        }
        if (synName == "nucleus") {
            return "Nucleus";
        }
        if (synName == "nucleoli") {
            return "Nucleolus";
        }
        if (synName == "lipid droplet") {
            return "Lipid droplet";
        }
        return synName;
    }
    auto MaskSelectionDialog::GetSyntheticName(QString disName)const -> QString {
        if (disName == "Cell instance") {
            return "cellInst";
        }
        if (disName == "Whole cell") {
            return "membrane";
        }
        if (disName == "Nucleus") {
            return "nucleus";
        }
        if (disName == "Nucleolus") {
            return "nucleoli";
        }
        if (disName == "Lipid droplet") {
            return "lipid droplet";
        }
        return disName;
    }


    auto MaskSelectionDialog::enableInst(bool enable) -> void {
        d->ui->InstRadio->setEnabled(enable);
        d->ui->InstPage->setEnabled(enable);        
    }

    auto MaskSelectionDialog::enableOrgan(bool enable) -> void {
        d->ui->OrganRadio->setEnabled(enable);
        d->ui->OrganPage->setEnabled(enable);
    }

    auto MaskSelectionDialog::setOrganList(QStringList orgList) -> void {
        for(auto i=0;i<orgList.size();i++) {
            auto chk = new QCheckBox;            
            chk->setText(GetDisplayName(orgList[i]));
            d->chkBoxList[orgList[i]] = chk;
            d->ui->orgChkGroup->layout()->addWidget(chk);
        }
    }

    void MaskSelectionDialog::on_okButton_clicked() {        
        /*if (d->ui->InstRadio->isChecked()) {
            if (d->ui->labelList->text().isEmpty()) {
                QMessageBox::warning(nullptr, "Select proper labels", "Empty or invalid label list");
                return;
            }    
        }*/
        if(d->ui->OrganRadio->isChecked()) {
            if(GetSelectedMaskList().count()<1) {
                QMessageBox::warning(nullptr, "Select proper mask", "Empty or invalid mask type");
                return;
            }
        }
        this->accept();
    }

    void MaskSelectionDialog::on_cancelButton_clicked() {
        this->reject();
    }
}
