#include <iostream>

#include <QIcon>

#include <UIUtility.h>

#include "ui_MaskControlPanel.h"
#include "MaskControlPanel.h"

namespace TomoAnalysis::InterSeg::Plugins {
    enum Status {
        NA,
        WORKING,
        CONFIRM
    };
    struct MaskControlPanel::Impl {
        Ui::MaskControlForm* ui{ nullptr };
        QIcon confirmIcon;
        QIcon naIcon;//N/A
        QIcon workingIcon;

        Status instStat{NA};
        Status memStat{NA};
        Status nucleStat{NA};
        Status nucliStat{NA};
        Status lipStat{NA};
        Status maskStat{NA};

        bool isSystem{ false };
    };
    MaskControlPanel::MaskControlPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::MaskControlForm();
        d->ui->setupUi(this);
        this->InitIcons();
        this->Init();
    }
    MaskControlPanel::~MaskControlPanel() = default;
    auto MaskControlPanel::Reset()->void {
        auto ds = GetDS();
        ds->instExist = false;
        ds->memExist = false;
        ds->lipExist = false;
        ds->nucleExist = false;
        ds->nucliExist = false;
        ds->maskList.clear();
        ds->isSystem = false;
        Update();
    }
    auto MaskControlPanel::Update() -> bool {
        InternalReset();
        auto ds = GetDS();
        if (ds->instExist) {
            d->ui->instChk->setEnabled(true);
            d->instStat = WORKING;
            d->ui->instStatus->setIcon(d->workingIcon);
            TC::SilentCall(d->ui->instChk)->setChecked(ds->instCheck);
        }
        if (ds->memExist) {
            d->ui->memChk->setEnabled(true);
            d->memStat = WORKING;
            d->ui->memStatus->setIcon(d->workingIcon);
            TC::SilentCall(d->ui->memChk)->setChecked(ds->memCheck);
        }
        if(ds->lipExist) {
            d->ui->lipChk->setEnabled(true);
            d->lipStat = WORKING;
            d->ui->lipStatus->setIcon(d->workingIcon);
            TC::SilentCall(d->ui->lipChk)->setChecked(ds->lipCheck);
        }
        if (ds->nucleExist) {
            d->ui->nucleChk->setEnabled(true);
            d->nucleStat = WORKING;
            d->ui->nucleStatus->setIcon(d->workingIcon);
            TC::SilentCall(d->ui->nucleChk)->setChecked(ds->nucleCheck);
        }
        if (ds->nucliExist) {
            d->ui->nucliChk->setEnabled(true);
            d->nucliStat = WORKING;
            d->ui->nucliStatus->setIcon(d->workingIcon);
            TC::SilentCall(d->ui->nucliChk)->setChecked(ds->nucliCheck);
        }
        if(ds->maskList.count()>0){
            d->ui->maskChk->setEnabled(true);
            d->maskStat = WORKING;
            d->ui->maskStatus->setIcon(d->workingIcon);
            for (auto n : ds->maskList) {
                TC::SilentCall(d->ui->maskCombo)->addItem(n);
            }
            TC::SilentCall(d->ui->maskChk)->setChecked(ds->maskCheck);
        }        
        d->isSystem = ds->isSystem;
        return true;
    }

    void MaskControlPanel::vizStateChanged(int state) {
        Q_UNUSED(state)
        auto s = sender();
        if(s == d->ui->instChk) {
            emit sigViz(d->ui->instChk->isChecked(),"cellInst");
            return;
        }
        if(s == d->ui->memChk) {
            emit sigViz(d->ui->memChk->isChecked(),"membrane");
            return;
        }
        if(s == d->ui->nucleChk) {
            emit sigViz(d->ui->nucleChk->isChecked(),"nucleus");
            return;
        }
        if(s == d->ui->nucliChk) {
            emit sigViz(d->ui->nucliChk->isChecked(),"nucleoli");
            return;
        }
        if(s == d->ui->lipChk) {
            emit sigViz(d->ui->lipChk->isChecked(),"lipid droplet");
            return;
        }
        if(s== d->ui->maskChk) {
            emit sigViz(d->ui->maskChk->isChecked(), d->ui->maskCombo->currentText());
        }
    }
    void MaskControlPanel::statusClicked() {
        if(d->isSystem) {
            return;
        }
        auto s = sender();
        if (s == d->ui->instStatus) {
            emit sigState(d->instStat, "cellInst");
            return;
        }
        if (s == d->ui->memStatus) {            
            emit sigState(d->memStat, "membrane");
            return;
        }
        if (s == d->ui->nucleStatus) {
            emit sigState(d->nucleStat, "nucleus");
            return;
        }
        if (s == d->ui->nucliStatus) {
            emit sigState(d->nucliStat, "nucleoli");
            return;
        }
        if (s == d->ui->lipStatus) {
            emit sigState(d->lipStat, "lipid droplet");
            return;
        }
        if (s == d->ui->maskStatus) {
            emit sigState(d->maskStat, d->ui->maskCombo->currentText());
        }
    }

    void MaskControlPanel::curMaskChanged(int idx) {
        emit sigChangeMask(d->ui->maskCombo->itemText(idx));
    }    

    auto MaskControlPanel::Init() -> void {
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->contentsWidget->setObjectName("panel-contents");

        d->ui->instStatus->setStyleSheet("border: none;");
        d->ui->lipStatus->setStyleSheet("border: none;");
        d->ui->maskStatus->setStyleSheet("border: none;");
        d->ui->memStatus->setStyleSheet("border: none;");
        d->ui->nucleStatus->setStyleSheet("border: none;");
        d->ui->nucliStatus->setStyleSheet("border: none;");

        connect(d->ui->lipChk, SIGNAL(stateChanged(int)), this, SLOT(vizStateChanged(int)));
        connect(d->ui->instChk, SIGNAL(stateChanged(int)), this, SLOT(vizStateChanged(int)));
        connect(d->ui->maskChk, SIGNAL(stateChanged(int)), this, SLOT(vizStateChanged(int)));
        connect(d->ui->memChk, SIGNAL(stateChanged(int)), this, SLOT(vizStateChanged(int)));
        connect(d->ui->nucleChk, SIGNAL(stateChanged(int)), this, SLOT(vizStateChanged(int)));
        connect(d->ui->nucliChk, SIGNAL(stateChanged(int)), this, SLOT(vizStateChanged(int)));

        connect(d->ui->instStatus, SIGNAL(clicked()), this, SLOT(statusClicked()));
        connect(d->ui->lipStatus, SIGNAL(clicked()), this, SLOT(statusClicked()));
        connect(d->ui->maskStatus, SIGNAL(clicked()), this, SLOT(statusClicked()));
        connect(d->ui->memStatus, SIGNAL(clicked()), this, SLOT(statusClicked()));
        connect(d->ui->nucleStatus, SIGNAL(clicked()), this, SLOT(statusClicked()));
        connect(d->ui->nucliStatus, SIGNAL(clicked()), this, SLOT(statusClicked()));

        d->ui->lipChk->setEnabled(false);
        d->ui->instChk->setEnabled(false);
        d->ui->maskChk->setEnabled(false);
        d->ui->memChk->setEnabled(false);
        d->ui->nucleChk->setEnabled(false);
        d->ui->nucliChk->setEnabled(false);

        d->ui->maskChk->hide();
        d->ui->maskCombo->hide();
        d->ui->maskStatus->hide();

        connect(d->ui->maskCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(curMaskChanged(int)));
    }
    auto MaskControlPanel::InitIcons() -> void {
        d->confirmIcon = QIcon(":/image/images/Confirm.png");
        d->naIcon = QIcon(":/image/images/NotA.png");
        d->workingIcon = QIcon(":/image/images/Working.png");
        d->ui->instStatus->setIcon(d->naIcon);
        d->ui->lipStatus->setIcon(d->naIcon);
        d->ui->maskStatus->setIcon(d->naIcon);
        d->ui->memStatus->setIcon(d->naIcon);
        d->ui->nucleStatus->setIcon(d->naIcon);
        d->ui->nucliStatus->setIcon(d->naIcon);                
    }
    auto MaskControlPanel::InternalReset() -> void {
        TC::SilentCall(d->ui->maskCombo)->clear();
        TC::SilentCall(d->ui->instChk)->setCheckState(Qt::CheckState::Unchecked);
        TC::SilentCall(d->ui->memChk)->setCheckState(Qt::CheckState::Unchecked);
        TC::SilentCall(d->ui->lipChk)->setCheckState(Qt::CheckState::Unchecked);
        TC::SilentCall(d->ui->nucleChk)->setCheckState(Qt::CheckState::Unchecked);
        TC::SilentCall(d->ui->nucliChk)->setCheckState(Qt::CheckState::Unchecked);
        TC::SilentCall(d->ui->maskChk)->setCheckState(Qt::CheckState::Unchecked);

        d->instStat = NA;
        d->lipStat = NA;
        d->maskStat = NA;
        d->memStat = NA;
        d->nucleStat = NA;
        d->nucliStat = NA;

        d->ui->instStatus->setIcon(d->naIcon);
        d->ui->lipStatus->setIcon(d->naIcon);
        d->ui->maskStatus->setIcon(d->naIcon);
        d->ui->memStatus->setIcon(d->naIcon);
        d->ui->nucleStatus->setIcon(d->naIcon);
        d->ui->nucliStatus->setIcon(d->naIcon);
    }
}