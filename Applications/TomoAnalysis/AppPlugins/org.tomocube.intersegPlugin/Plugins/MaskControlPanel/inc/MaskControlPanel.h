#pragma once

#include <memory>
#include <QWidget>

#include <IMaskControlPanel.h>

#include "MaskControlPanelExport.h"

namespace TomoAnalysis::InterSeg::Plugins {
    class MaskControlPanel_API MaskControlPanel : public QWidget,public Interactor::IMaskControlPanel {
        Q_OBJECT
    public:
        typedef MaskControlPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        MaskControlPanel(QWidget* parent = nullptr);
        ~MaskControlPanel();

        auto Update() -> bool override;
        auto Reset()->void override;
    signals:        
        void sigViz(bool, QString);
        void sigState(int, QString);
        void sigChangeMask(QString);        

    protected slots:
        void statusClicked();
        void vizStateChanged(int);
        void curMaskChanged(int);

    private:
        auto InitIcons()->void;
        auto Init()->void;
        auto InternalReset()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}