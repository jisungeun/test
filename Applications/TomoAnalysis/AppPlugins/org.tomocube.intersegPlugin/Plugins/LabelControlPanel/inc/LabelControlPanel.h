#pragma once

#include <memory>
#include <QWidget>

#include <ILabelControlPanel.h>

#include "LabelControlPanelExport.h"

namespace TomoAnalysis::InterSeg::Plugins {
    class LabelControlPanel_API LabelControlPanel : public QWidget, public Interactor::ILabelControlPanel {
        Q_OBJECT
    public:
        typedef LabelControlPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        LabelControlPanel(QWidget* parent = nullptr);
        ~LabelControlPanel();

        auto Update() -> bool override;
        auto Reset() -> void override;

        //non-clean architecture
        auto ForceAddLabel()->void;
    signals:
        void sigLabelChange(int);
        void sigPick(bool);
        void sigMergeLabel(bool);
        void sigAddLabel();

    protected slots:
        void OnLabelValueChanged(int);
        void OnPickClicked();
        void OnMergeLabelClicked();
        void OnAddLabelClicked();
        void SetLabelValue(int);
        void OnAnnoClicked();

    private:
        auto InitIcons()->void;
        auto Init()->void;
        auto InitToolTips()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}