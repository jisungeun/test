#pragma once

#include <memory>

#include <QWidget>

#include "LabelControlPanelExport.h"

namespace TomoAnalysis::InterSeg::Plugins {
    class LabelControlPanel_API AnnoWidget : public QWidget {
        Q_OBJECT
    public:
        typedef AnnoWidget Self;
        typedef std::shared_ptr<Self> Pointer;

        AnnoWidget(QWidget* parent = nullptr);
        ~AnnoWidget();

    signals:
         void sigCloseAnnoPanel();

    protected slots:
        

    protected:
        void closeEvent(QCloseEvent* event) override;

    private:
        auto InitUI()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}