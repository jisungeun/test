#include <iostream>
#include <QIcon>

#include "AnnoWidget.h"

#include "ui_LabelControlPanel.h"
#include "LabelControlPanel.h"

namespace TomoAnalysis::InterSeg::Plugins {
    struct LabelControlPanel::Impl {
        Ui::LabelForm* ui{ nullptr };
        QIcon pickIcon;
        QIcon mergeIcon;
        QIcon annoIcon;
        bool isActivate{ false };
        bool isMerge{ false };
        bool isSystem{ false };
        bool isAnno{ false };

        AnnoWidget::Pointer annoControl{ nullptr };
    };
    LabelControlPanel::LabelControlPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::LabelForm();
        d->ui->setupUi(this);

        d->annoControl = std::make_shared<AnnoWidget>();

        this->InitIcons();
        this->Init();
        this->InitToolTips();                
    }
    LabelControlPanel::~LabelControlPanel() = default;
    auto LabelControlPanel::Reset() -> void {
        auto ds = GetDS();
        ds->labelMin = 1;
        ds->labelMax = 1;
        ds->isEnable = false;
        ds->isMerge = false;
        ds->isSystem = false;

        if(d->isAnno) {
            OnAnnoClicked();
        }

        Update();
        
    }
    auto LabelControlPanel::Update() -> bool {
        auto ds = GetDS();
        d->ui->LabelSpin->blockSignals(true);
        d->ui->LabelSpin->setRange(ds->labelMin, ds->labelMax);        
        d->ui->LabelSpin->blockSignals(false);
        d->isActivate = ds->isEnable;
        d->isMerge = ds->isMerge;
        d->isSystem = ds->isSystem;
        if(d->isActivate) {
            d->ui->PickBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }else {
            d->ui->PickBtn->setStyleSheet("");
        }
        if(d->isMerge) {
            d->ui->MergeLabelBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }else {
            d->ui->MergeLabelBtn->setStyleSheet("");
        }
        return true;
    }
    void LabelControlPanel::OnLabelValueChanged(int val) {        
        emit sigLabelChange(val);
    }
    void LabelControlPanel::SetLabelValue(int val) {
        d->ui->LabelSpin->setValue(val);
    }

    void LabelControlPanel::OnPickClicked() {
        emit sigPick(d->isActivate);
    }
    void LabelControlPanel::OnMergeLabelClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigMergeLabel(d->isMerge);
    }
    void LabelControlPanel::OnAddLabelClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigAddLabel();
    }

    void LabelControlPanel::OnAnnoClicked() {
        if(false == d->isAnno) {
            d->ui->AnnoBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            d->annoControl->show();
        }else {
            d->ui->AnnoBtn->setStyleSheet("");
            d->annoControl->hide();
        }
        d->isAnno = !d->isAnno;
    }

    auto LabelControlPanel::ForceAddLabel() -> void {
        d->ui->LabelSpin->blockSignals(true);
        auto new_max = d->ui->LabelSpin->maximum() + 1;
        d->ui->LabelSpin->setMaximum(new_max);
        d->ui->LabelSpin->setValue(new_max);
        d->ui->LabelSpin->blockSignals(false);
        auto ds = GetDS();
        ds->labelMax = new_max;
    }
    auto LabelControlPanel::Init() -> void {
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->contentsWidget->setObjectName("panel-contents");
        connect(d->ui->PickBtn, SIGNAL(clicked()), this, SLOT(OnPickClicked()));
        connect(d->ui->MergeLabelBtn, SIGNAL(clicked()), this, SLOT(OnMergeLabelClicked()));
        connect(d->ui->LabelSpin, SIGNAL(valueChanged(int)), this, SLOT(OnLabelValueChanged(int)));
        connect(d->ui->addLabelBtn, SIGNAL(clicked()), this, SLOT(OnAddLabelClicked()));
        connect(d->ui->AnnoBtn, SIGNAL(clicked()), this, SLOT(OnAnnoClicked()));

        d->ui->addLabelBtn->setObjectName("bt-round-gray500");
        d->ui->AnnoBtn->hide();
    }
    auto LabelControlPanel::InitToolTips() -> void {
        d->ui->PickBtn->setToolTip("Pick label value");
        d->ui->MergeLabelBtn->setToolTip("Merge labels");
        d->ui->MergeLabelBtn->setToolTip("Add annotation");
    }

    auto LabelControlPanel::InitIcons() -> void {
        d->pickIcon = QIcon(":/image/images/Pick.png");        
        d->ui->PickBtn->setIcon(d->pickIcon);
        d->ui->PickBtn->setIconSize(QSize(52, 52));
        d->mergeIcon = QIcon(":/image/images/Merge.png");        
        d->ui->MergeLabelBtn->setIcon(d->mergeIcon);
        d->ui->MergeLabelBtn->setIconSize(QSize(52, 52));
        d->annoIcon = QIcon(":/image/images/Annotation.png");
        d->ui->AnnoBtn->setIcon(d->annoIcon);
        d->ui->AnnoBtn->setIconSize(QSize(52, 52));
    }
}