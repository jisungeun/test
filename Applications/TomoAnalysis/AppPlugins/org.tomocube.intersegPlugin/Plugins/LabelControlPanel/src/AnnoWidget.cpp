#include <iostream>

#include "ui_AnnoWidget.h"

#include "AnnoWidget.h"

#include <QCloseEvent>

namespace TomoAnalysis::InterSeg::Plugins {
    struct AnnoWidget::Impl {
        Ui::Anno* ui{ nullptr };
    };
    AnnoWidget::AnnoWidget(QWidget* parent) : d{ new Impl } {
        d->ui = new Ui::Anno;
        d->ui->setupUi(this);
        InitUI();
    }
    AnnoWidget::~AnnoWidget() {
        
    }
    auto AnnoWidget::InitUI() -> void {
        
    }
    void AnnoWidget::closeEvent(QCloseEvent* event) {
        if(event->spontaneous()) {
            emit sigCloseAnnoPanel();
        }else {
            QWidget::closeEvent(event);
        }
    }
}