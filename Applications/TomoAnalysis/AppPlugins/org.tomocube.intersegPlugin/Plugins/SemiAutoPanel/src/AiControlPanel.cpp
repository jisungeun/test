#include <iostream>

#include "ui_AiControlPanel.h"

#include "AiControlPanel.h"

#include <QCloseEvent>

namespace TomoAnalysis::InterSeg::Plugins {
    struct AiControlPanel::Impl {
        Ui::AiControl* ui{ nullptr };
        bool isAdding{ false };
        bool addSilent{ false };
    };
    AiControlPanel::AiControlPanel(QWidget* parent) : d{ new Impl } {
        d->ui = new Ui::AiControl;
        d->ui->setupUi(this);
        InitUI();
    }
    AiControlPanel::~AiControlPanel() {
        
    }

    auto AiControlPanel::InitUI() -> void {
        connect(d->ui->newBtn, SIGNAL(clicked()), this, SLOT(OnNewBtn()));
        connect(d->ui->endBtn, SIGNAL(clicked()), this, SLOT(OnEndBtn()));
        connect(d->ui->addPointBtn, SIGNAL(clicked()), this, SLOT(OnAddPointBtn()));
        connect(d->ui->volumeBtn, SIGNAL(clicked()), this, SLOT(OnUpdateVolumeBtn()));
        connect(d->ui->sliceBtn, SIGNAL(clicked()), this, SLOT(OnSliceRecommendation()));
        connect(d->ui->posRad, SIGNAL(clicked()), this, SLOT(OnPosNegChanged()));
        connect(d->ui->negRad, SIGNAL(clicked()), this, SLOT(OnPosNegChanged()));
        connect(d->ui->rad2D, SIGNAL(clicked()), this, SLOT(OnDimChanged()));
        connect(d->ui->rad3D, SIGNAL(clicked()), this, SLOT(OnDimChanged()));
        connect(d->ui->addSliceBtn, SIGNAL(clicked()), this, SLOT(OnAddSlice()));

        setWindowTitle("Ai Control");
        setWindowFlags(Qt::WindowStaysOnTopHint);
        d->ui->posRad->setChecked(true);
        d->ui->rad2D->setChecked(true);
    }

    void AiControlPanel::closeEvent(QCloseEvent* event) {
        d->ui->addPointBtn->setStyleSheet("");
        d->isAdding = false;
        if(event->spontaneous()) {
            emit sigCloseAiPanel();
        }else {
            QWidget::closeEvent(event);
        }
    }

    auto AiControlPanel::SetAddPtDisabled() -> void {
        if (d->isAdding) {
            OnAddPointBtn();
        }
    }
    //SLOTS
    void AiControlPanel::OnAddSlice() {
        emit sigSetSlice();
    }
    void AiControlPanel::OnDimChanged() {
        auto is2D = d->ui->rad2D->isChecked();
        emit sigIs2D(is2D);
    }
    void AiControlPanel::OnPosNegChanged() {
        //emit sigIsPositive(d->ui->positiveChk->isChecked());
        emit sigIsPositive(d->ui->posRad->isChecked());
    }
    void AiControlPanel::OnSliceRecommendation() {
        emit sigSliceRecom();
    }
    void AiControlPanel::OnAddPointBtn() {
        if(d->isAdding) {
            d->ui->addPointBtn->setStyleSheet("");
            emit sigAddPoint(false,d->addSilent);
        }else {
            d->ui->addPointBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            emit sigAddPoint(true,d->addSilent);            
        }
        d->addSilent = false;
        d->isAdding = !d->isAdding;
    }
    void AiControlPanel::OnEndBtn() {
        if (d->isAdding) {
            OnAddPointBtn();
        }
        emit sigFinishImage();
    }
    void AiControlPanel::OnNewBtn() {
        emit sigSetImage();
    }
    void AiControlPanel::OnUpdateVolumeBtn() {
        emit sigUpdateVol();
    }
    void AiControlPanel::OnAiPointOff() {
        if(d->isAdding) {
            d->addSilent = true;
            OnAddPointBtn();
        }
    }
}
