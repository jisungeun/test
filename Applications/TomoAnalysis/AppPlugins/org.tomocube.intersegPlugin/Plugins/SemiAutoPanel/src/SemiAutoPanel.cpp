#include <QIcon>

#include <Tool.h>

#include "ui_SemiAutoPanel.h"
#include "SemiAutoPanel.h"

namespace TomoAnalysis::InterSeg::Plugins {
    struct SemiAutoPanel::Impl {
        Ui::SemiAutoForm* ui{ nullptr };
        QIcon DivideIcon;
        QIcon BranchIcon;
        QIcon WaterIcon;
        QIcon ErodeIcon;
        QIcon DilateIcon;
        QIcon SizeFilterIcon;
        QIcon LassoDelIcon;
        QIcon PickDelIcon;
        QIcon AiIcon;

        int cur_size_idx{ 1 };
        int cur_idx{ -1 };
        bool isSystem{ false };
        bool collapsed{ true };
        bool isAi{ false };
    };
    SemiAutoPanel::SemiAutoPanel(QWidget* parent):d{new Impl},QWidget(parent) {
        d->ui = new Ui::SemiAutoForm();
        d->ui->setupUi(this);
        this->InitIcons();
        this->Init();
        this->InitToolTips();
    }
    SemiAutoPanel::~SemiAutoPanel() {
        
    }
    auto SemiAutoPanel::Update() -> bool {
        auto ds = GetDS();
        d->cur_idx = ds->cur_id;
        d->isSystem = ds->isSystem;
        CleanIcons();
        SetIconOn();
        return true;
    }
    auto SemiAutoPanel::Init() -> void {
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->contentsWidget->setObjectName("panel-contents");

        d->ui->BranchBtn->hide();
        //d->ui->PickEraseBtn->hide();
        d->ui->LassoDelBtn->hide();
        d->ui->SizeFilterBtn->hide();
        d->ui->AiGuideBtn->hide();

        connect(d->ui->DividerBtn, SIGNAL(clicked()), this, SLOT(OnDivideClicked()));
        connect(d->ui->BranchBtn, SIGNAL(clicked()), this, SLOT(OnBranchClicked()));
        connect(d->ui->WaterShedBtn, SIGNAL(clicked()), this, SLOT(OnWatershedClicked()));
        connect(d->ui->DilateBtn, SIGNAL(clicked()), this, SLOT(OnDilateClicked()));
        connect(d->ui->ErodeBtn, SIGNAL(clicked()), this, SLOT(OnErodeClicked()));
        connect(d->ui->LassoDelBtn, SIGNAL(clicked()), this, SLOT(OnLassoDeleteClicked()));
        connect(d->ui->PickEraseBtn, SIGNAL(clicked()), this, SLOT(OnPickDeleteClicked()));
        connect(d->ui->SizeFilterBtn, SIGNAL(clicked()), this, SLOT(OnSizeFilterClicked()));
        //d->ui->BranchBtn->setEnabled(false);
        //d->ui->BranchBtn->hide();
        d->ui->arrowBtn->setChecked(false);
        d->ui->arrowBtn->setObjectName("bt-collapse");
        connect(d->ui->arrowBtn, SIGNAL(clicked(bool)), this, SLOT(OnCollapse(bool)));

        d->ui->sizeWidget->hide();
        d->ui->sizeSlider->setRange(1, 10);
        d->ui->sizeSlider->setValue(1);
        d->ui->sizeSpin->setRange(1, 10);
        d->ui->sizeSpin->setValue(1);
        connect(d->ui->sizeSpin, SIGNAL(valueChanged(int)), this, SLOT(OnSizeSpinChanged(int)));
        connect(d->ui->sizeSlider, SIGNAL(valueChanged(int)), this, SLOT(OnSizeSliderChanged(int)));
        connect(d->ui->sizeSlider, SIGNAL(sliderReleased()), this, SLOT(OnSizeSliderReleased()));

        connect(d->ui->AiGuideBtn, SIGNAL(clicked()), this, SLOT(OnAiClicked()));
    }
    void SemiAutoPanel::OnSizeSpinChanged(int value) {
        d->ui->sizeSlider->blockSignals(true);
        d->ui->sizeSlider->setValue(value);
        d->ui->sizeSlider->blockSignals(false);
        emit sigSizeIndex(value);
    }
    void SemiAutoPanel::OnSizeSliderChanged(int value) {        
        d->ui->sizeSpin->blockSignals(true);
        d->ui->sizeSpin->setValue(value);
        d->ui->sizeSpin->blockSignals(false);
    }
    void SemiAutoPanel::OnSizeSliderReleased() {
        auto value = d->ui->sizeSlider->value();
        if(d->cur_size_idx == value) {
            return;
        }
        d->cur_size_idx = value;
        //perform size based threshold
        //send signal
        emit sigSizeIndex(value);
    }
    auto SemiAutoPanel::SetSizeMax(int max) -> void {
        d->ui->sizeSlider->blockSignals(true);
        d->ui->sizeSlider->setRange(1, max);
        d->ui->sizeSlider->setValue(1);
        d->ui->sizeSlider->blockSignals(false);
        d->ui->sizeSpin->blockSignals(true);
        d->ui->sizeSpin->setRange(1, max);
        d->ui->sizeSpin->setValue(1);
        d->ui->sizeSpin->blockSignals(false);
    }

    auto SemiAutoPanel::InitToolTips() -> void {
        d->ui->DividerBtn->setToolTip("Divide single volume into two volumes");
        d->ui->BranchBtn->setToolTip("Manipulate branch-structure of the volume");
        d->ui->WaterShedBtn->setToolTip("Expand cell label based on cell boundary mask");
        d->ui->DilateBtn->setToolTip("Dilate current label");
        d->ui->ErodeBtn->setToolTip("Erode current label");
        d->ui->PickEraseBtn->setToolTip("Delete chunk by picking");
        d->ui->LassoDelBtn->setToolTip("Delete Frustum by lasso drawing");
        d->ui->SizeFilterBtn->setToolTip("Delete chunks by size filtering");
    }
    auto SemiAutoPanel::InitIcons() -> void {
        d->DivideIcon = QIcon(":/image/images/Divide.png");        
        d->ui->DividerBtn->setIcon(d->DivideIcon);
        d->ui->DividerBtn->setIconSize(QSize(52, 52));

        d->BranchIcon = QIcon(":/image/images/Branch.png");        
        d->ui->BranchBtn->setIcon(d->BranchIcon);
        d->ui->BranchBtn->setIconSize(QSize(52, 52));

        d->ErodeIcon = QIcon(":/image/images/Erode.png");
        d->ui->ErodeBtn->setIcon(d->ErodeIcon);
        d->ui->ErodeBtn->setIconSize(QSize(52, 52));

        d->DilateIcon = QIcon(":/image/images/Dilate.png");
        d->ui->DilateBtn->setIcon(d->DilateIcon);
        d->ui->DilateBtn->setIconSize(QSize(52, 52));

        d->WaterIcon = QIcon(":/image/images/Watershed.png");
        d->ui->WaterShedBtn->setIcon(d->WaterIcon);
        d->ui->WaterShedBtn->setIconSize(QSize(52, 52));

        d->SizeFilterIcon = QIcon(":/image/images/SizeFilter.png");
        d->ui->SizeFilterBtn->setIcon(d->SizeFilterIcon);
        d->ui->SizeFilterBtn->setIconSize(QSize(52, 52));

        d->PickDelIcon = QIcon(":/image/images/PickErase.png");
        d->ui->PickEraseBtn->setIcon(d->PickDelIcon);
        d->ui->PickEraseBtn->setIconSize(QSize(52, 52));

        d->LassoDelIcon = QIcon(":/image/images/LassoDelete.png");
        d->ui->LassoDelBtn->setIcon(d->LassoDelIcon);
        d->ui->LassoDelBtn->setIconSize(QSize(52, 52));

        d->AiIcon = QIcon(":/image/images/AiGuide.png");
        d->ui->AiGuideBtn->setIcon(d->AiIcon);
        d->ui->AiGuideBtn->setIconSize(QSize(52, 52));                
    }
    auto SemiAutoPanel::CleanIcons() -> void {
        d->ui->BranchBtn->setStyleSheet("");
        d->ui->DividerBtn->setStyleSheet("");
        d->ui->PickEraseBtn->setStyleSheet("");
        d->ui->LassoDelBtn->setStyleSheet("");
        d->ui->SizeFilterBtn->setStyleSheet("");

        d->ui->sizeWidget->hide();
    }
    auto SemiAutoPanel::SetIconOn() -> void {
        if(d->cur_idx == Entity::ToolIdx::DIVIDE) {
            d->ui->DividerBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }else if(d->cur_idx == Entity::ToolIdx::BRANCH){
            d->ui->BranchBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }else if(d->cur_idx == Entity::ToolIdx::PICKDELETE) {
            d->ui->PickEraseBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }else if(d->cur_idx == Entity::ToolIdx::LASSODELETE) {
            d->ui->LassoDelBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");            
        }else if(d->cur_idx == Entity::ToolIdx::SIZEFILTER) {
            d->ui->SizeFilterBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            d->ui->sizeWidget->show();
        }
    }
    void SemiAutoPanel::OnCollapse(bool isClose) {
        d->collapsed = isClose;

        if (d->collapsed) {
            d->ui->LassoDelBtn->show();
            //d->ui->PickEraseBtn->show();
            d->ui->SizeFilterBtn->show();
            d->ui->AiGuideBtn->show();
        }
        else {
            d->ui->LassoDelBtn->hide();
            //d->ui->PickEraseBtn->hide();
            d->ui->SizeFilterBtn->hide();
            d->ui->AiGuideBtn->hide();
        }
    }
    void SemiAutoPanel::OnLassoDeleteClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigLassoDel(d->cur_idx == Entity::ToolIdx::LASSODELETE);
    }
    void SemiAutoPanel::OnPickDeleteClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigPickDel(d->cur_idx == Entity::ToolIdx::PICKDELETE);
    }
    void SemiAutoPanel::OnSizeFilterClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigSizeFilter(d->cur_idx == Entity::ToolIdx::SIZEFILTER);
    }
    void SemiAutoPanel::OnAiClicked() {
        if(d->isAi) {
            d->ui->AiGuideBtn->setStyleSheet("");
            emit sigAiPanel(false);
        }else {
            d->ui->AiGuideBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            emit sigAiPanel(true);
        }
        d->isAi = !d->isAi;
    }
    auto SemiAutoPanel::AiPanelHidden() -> void {
        d->ui->AiGuideBtn->setStyleSheet("");
        d->isAi = false;
    }

    void SemiAutoPanel::OnDivideClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigDivide(d->cur_idx==Entity::ToolIdx::DIVIDE);
    }
    void SemiAutoPanel::OnBranchClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigBranch(d->cur_idx==Entity::ToolIdx::BRANCH);
    }
    void SemiAutoPanel::OnDilateClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigDilate();
    }
    void SemiAutoPanel::OnErodeClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigErode();
    }
    void SemiAutoPanel::OnWatershedClicked() {
        if(d->isSystem) {
            return;
        }
        emit sigWatershed();
    }
}