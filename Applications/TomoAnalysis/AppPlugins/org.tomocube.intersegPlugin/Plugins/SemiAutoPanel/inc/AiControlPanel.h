#pragma once

#include <memory>

#include <QWidget>

#include "SemiAutoPanelExport.h"

namespace TomoAnalysis::InterSeg::Plugins {
	class SemiAutoPanel_API AiControlPanel : public QWidget{
		Q_OBJECT
	public:
		typedef AiControlPanel Self;
		typedef std::shared_ptr<Self> Pointer;

		AiControlPanel(QWidget* parent = nullptr);
		~AiControlPanel();

		auto Is3D()->bool;
		auto IsPositive()->bool;
		auto SetAddPtDisabled()->void;

	signals:
		void sigSetImage();
		void sigFinishImage();
		void sigAddPoint(bool,bool);
		void sigUpdateVol();
		void sigCloseAiPanel();
		void sigSliceRecom();
		void sigIs2D(bool);
		void sigIsPositive(bool);
		void sigSetSlice();

	protected slots:
		void OnNewBtn();
		void OnEndBtn();
		void OnAddPointBtn();
		void OnUpdateVolumeBtn();
		void OnSliceRecommendation();
		void OnDimChanged();
		void OnPosNegChanged();
		void OnAddSlice();
		void OnAiPointOff();

	protected:
		void closeEvent(QCloseEvent* event) override;

	private:
		auto InitUI()->void;

		struct Impl;
		std::unique_ptr<Impl> d;
	};
}