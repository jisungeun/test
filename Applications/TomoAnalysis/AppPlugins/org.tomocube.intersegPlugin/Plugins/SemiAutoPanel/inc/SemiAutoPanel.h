#pragma once

#include <memory>
#include <QWidget>

#include <ISemiAutoPanel.h>

#include "SemiAutoPanelExport.h"

namespace TomoAnalysis::InterSeg::Plugins {
    class SemiAutoPanel_API SemiAutoPanel : public QWidget, public Interactor::ISemiAutoPanel {
        Q_OBJECT
    public:
        typedef SemiAutoPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        SemiAutoPanel(QWidget* parent = nullptr);
        ~SemiAutoPanel();

        auto Update() -> bool override;

        //Non-clean architecture
        auto AiPanelHidden()->void;

    signals:
        void sigDivide(bool);
        void sigBranch(bool);
        void sigLassoDel(bool);
        void sigPickDel(bool);
        void sigSizeFilter(bool);
        void sigErode();
        void sigDilate();
        void sigWatershed();
        void sigSizeIndex(int);
        void sigAiPanel(bool);

    protected slots:
        void OnDivideClicked();
        void OnBranchClicked();
        void OnWatershedClicked();
        void OnErodeClicked();
        void OnDilateClicked();
        void OnPickDeleteClicked();
        void OnLassoDeleteClicked();
        void OnSizeFilterClicked();
        void OnAiClicked();

        void OnSizeSliderChanged(int);
        void OnSizeSliderReleased();
        void OnSizeSpinChanged(int);

        void OnCollapse(bool);

        auto SetSizeMax(int max)->void;

    private:
        auto InitIcons()->void;
        auto Init()->void;
        auto InitToolTips()->void;
        auto CleanIcons()->void;
        auto SetIconOn()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}