#pragma once

#include <QObject>
#include <IAppMeta.h>

#include "InterSegMetaExport.h"

namespace TomoAnalysis::InterSeg::Plugins {
	class InterSegMeta_API AppMeta
		:public QObject
		, public IAppMeta
	{
		Q_OBJECT
		Q_PLUGIN_METADATA(IID "org.tomocube.intersegMeta")
		Q_INTERFACES(IAppMeta)
	public:
		AppMeta();
		virtual ~AppMeta();

		auto GetName()const->QString override { return "Mask Editor"; }
		auto GetFullName()const->QString override { return "org.tomocube.intersegPlugin"; }
		auto clone()const->IAppMeta* override { return new AppMeta(); }
		auto GetMetaInfo()const->QVariantMap override;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}