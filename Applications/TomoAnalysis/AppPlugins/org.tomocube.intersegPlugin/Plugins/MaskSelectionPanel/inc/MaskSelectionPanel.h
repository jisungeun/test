#pragma once

#include <memory>
#include <QWidget>

#include <IMaskSelectionPanel.h>
#include "MaskSelectionPanelExport.h"

namespace TomoAnalysis::InterSeg::Plugins {
    class MaskSelectionPanel_API MaskSelectionPanel : public QWidget, public Interactor::IMaskSelectionPanel {
        Q_OBJECT
    public:
        typedef MaskSelectionPanel Self;
        typedef std::shared_ptr<Self> Pointer;

        MaskSelectionPanel(QWidget* parent = nullptr);
        ~MaskSelectionPanel();

        auto Update() -> bool override;
        auto Reset() -> void override;
    signals:
        void sigActivate(QString);
        void sigDeactivate(QString);

    protected slots:
        void OnInstClicked();
        void OnMemClicked();
        void OnNucleClicked();
        void OnNucliClicked();
        void OnLipClicked();
        void OnMaskClicked();
    private:
        auto InitIcons()->void;
        auto Init()->void;
        auto InitToolTips()->void;
        auto ResetIcons()->void;
        auto ResetOthers()->void;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}