#include <QIcon>

#include "ui_MaskSelectionPanel.h"
#include "MaskSelectionPanel.h"

namespace TomoAnalysis::InterSeg::Plugins {
    struct MaskSelectionPanel::Impl {
        Ui::MaskSelForm* ui{ nullptr };
        QIcon instIcon;
        QIcon memIcon;
        QIcon nucleIcon;
        QIcon nucliIcon;
        QIcon lipIcon;
        QIcon maskIcon;
        bool activeInst{ false };
        bool activeMem{ false };
        bool activeNucle{ false };
        bool activeNucli{ false };
        bool activeLip{ false };
        bool activeCustom{ false };
    };
    MaskSelectionPanel::MaskSelectionPanel(QWidget* parent) : d{ new Impl },QWidget(parent) {
        d->ui = new Ui::MaskSelForm();
        d->ui->setupUi(this);

        this->InitIcons();
        this->Init();
        this->InitToolTips();
    }
    MaskSelectionPanel::~MaskSelectionPanel() = default;
    auto MaskSelectionPanel::Reset() -> void {
        auto ds = GetDS();
        ds->memExist = false;
        ds->nucleExist = false;
        ds->nucliExist = false;
        ds->instExist = false;
        ds->lipExist = false;
        ds->maskList.clear();
        ds->curSelection = QString();
        d->activeInst = false;
        d->activeMem = false;
        d->activeNucle = false;
        d->activeNucli = false;
        d->activeCustom = false;

        Update();
    }
    auto MaskSelectionPanel::Update() -> bool {
        auto ds = GetDS();
        d->ui->memBtn->setEnabled(ds->memExist);
        d->ui->nucleBtn->setEnabled(ds->nucleExist);
        d->ui->nucliBtn->setEnabled(ds->nucliExist);
        d->ui->lipBtn->setEnabled(ds->lipExist);
        d->ui->instBtn->setEnabled(ds->instExist);
        d->ui->maskBtn->setEnabled(ds->maskList.count() > 0);

        //ResetOthers();
        ResetIcons();
        if(ds->curSelection == "cellInst") {
            d->ui->instBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }else if(ds->curSelection == "membrane") {
            d->ui->memBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }else if(ds->curSelection == "nucleus") {
            d->ui->nucleBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }else if(ds->curSelection == "nucleoli") {
            d->ui->nucliBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }else if(ds->curSelection == "lipid droplet") {
            d->ui->lipBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }else if(!ds->curSelection.isEmpty()) {
            d->ui->maskBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
        }
        return true;
    }

    void MaskSelectionPanel::OnInstClicked() {
        ResetIcons();
        if(d->activeInst) {
            d->ui->instBtn->setStyleSheet("");
            emit sigDeactivate("cellInst");
        }else {
            d->ui->instBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            emit sigActivate("cellInst");
            ResetOthers();
        }
        d->activeInst = !d->activeInst;
    }

    void MaskSelectionPanel::OnMemClicked() {
        ResetIcons();
        if(d->activeMem) {
            d->ui->memBtn->setStyleSheet("");
            emit sigDeactivate("membrane");
        }else {
            d->ui->memBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            emit sigActivate("membrane");
            ResetOthers();
        }
        d->activeMem = !d->activeMem;
    }

    void MaskSelectionPanel::OnNucleClicked() {
        ResetIcons();
        if(d->activeNucle) {
            d->ui->nucleBtn->setStyleSheet("");
            emit sigDeactivate("nucleus");
        }else {
            d->ui->nucleBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            emit sigActivate("nucleus");
            ResetOthers();
        }
        d->activeNucle = !d->activeNucle;
    }

    void MaskSelectionPanel::OnNucliClicked() {
        ResetIcons();
        if(d->activeNucli) {
            d->ui->nucliBtn->setStyleSheet("");
            emit sigDeactivate("nucleoli");
        }else {
            d->ui->nucliBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            emit sigActivate("nucleoli");
            ResetOthers();
        }
        d->activeNucli = !d->activeNucli;
    }

    void MaskSelectionPanel::OnLipClicked() {
        ResetIcons();
        if(d->activeLip) {
            d->ui->lipBtn->setStyleSheet("");
            emit sigDeactivate("lipid droplet");
        }else {
            d->ui->lipBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            emit sigActivate("lipid droplet");
            ResetOthers();
        }
        d->activeLip = !d->activeLip;
    }

    void MaskSelectionPanel::OnMaskClicked() {
        ResetIcons();
        if(d->activeCustom) {
            d->ui->maskBtn->setStyleSheet("");
            emit sigDeactivate("custom");
        }else {
            d->ui->maskBtn->setStyleSheet("background-color: rgba(0, 194, 211, 128); border: 2px solid rgb(0, 194, 211); ");
            emit sigActivate("custom");
            ResetOthers();
        }
        d->activeCustom = !d->activeCustom;
    }

    auto MaskSelectionPanel::ResetOthers() -> void {
        d->activeInst = false;
        d->activeMem = false;
        d->activeNucle = false;
        d->activeNucli = false;
        d->activeLip = false;
        d->activeCustom = false;        
    }


    auto MaskSelectionPanel::ResetIcons() -> void {        
        d->ui->instBtn->setStyleSheet("");
        d->ui->memBtn->setStyleSheet("");
        d->ui->nucleBtn->setStyleSheet("");
        d->ui->nucliBtn->setStyleSheet("");
        d->ui->lipBtn->setStyleSheet("");
        d->ui->maskBtn->setStyleSheet("");
    }

    auto MaskSelectionPanel::Init() -> void {
        d->ui->frame->setObjectName("panel-base");
        d->ui->titleLabel->setObjectName("h6");
        d->ui->contentsWidget->setObjectName("panel-contents");
        connect(d->ui->instBtn, SIGNAL(clicked()), this, SLOT(OnInstClicked()));
        connect(d->ui->memBtn, SIGNAL(clicked()), this, SLOT(OnMemClicked()));
        connect(d->ui->nucleBtn, SIGNAL(clicked()), this, SLOT(OnNucleClicked()));
        connect(d->ui->nucliBtn, SIGNAL(clicked()), this, SLOT(OnNucliClicked()));
        connect(d->ui->lipBtn, SIGNAL(clicked()), this, SLOT(OnLipClicked()));
        connect(d->ui->maskBtn, SIGNAL(clicked()), this, SLOT(OnMaskClicked()));

        d->ui->instBtn->setEnabled(false);
        d->ui->lipBtn->setEnabled(false);
        d->ui->maskBtn->setEnabled(false);
        d->ui->nucleBtn->setEnabled(false);
        d->ui->nucliBtn->setEnabled(false);
        d->ui->memBtn->setEnabled(false);

        d->ui->maskBtn->hide();
    }
    auto MaskSelectionPanel::InitToolTips() -> void {
        d->ui->instBtn->setToolTip("Cell label");
        d->ui->memBtn->setToolTip("Whole cell");
        d->ui->nucleBtn->setToolTip("Nucleus");
        d->ui->nucliBtn->setToolTip("Nucleolus");
        d->ui->lipBtn->setToolTip("Lipid droplet");
    }

    auto MaskSelectionPanel::InitIcons() -> void {
        d->instIcon = QIcon(":/image/images/Instance.png");        
        d->memIcon = QIcon(":/image/images/Membrane.png");        
        d->nucleIcon = QIcon(":/image/images/Nuclear.png");        
        d->nucliIcon = QIcon(":/image/images/Nucleoli.png");        
        d->lipIcon = QIcon(":/image/images/Lipid.png");        
        d->maskIcon = QIcon(":/image/images/Mask.png");        

        d->ui->instBtn->setIcon(d->instIcon);
        d->ui->instBtn->setIconSize(QSize(52, 52));
        d->ui->memBtn->setIcon(d->memIcon);
        d->ui->memBtn->setIconSize(QSize(52, 52));
        d->ui->nucleBtn->setIcon(d->nucleIcon);
        d->ui->nucleBtn->setIconSize(QSize(52, 52));
        d->ui->nucliBtn->setIcon(d->nucliIcon);
        d->ui->nucliBtn->setIconSize(QSize(52, 52));
        d->ui->lipBtn->setIcon(d->lipIcon);
        d->ui->lipBtn->setIconSize(QSize(52, 52));
        d->ui->maskBtn->setIcon(d->maskIcon);
        d->ui->maskBtn->setIconSize(QSize(52, 52));                
    }
}