#pragma once

#include <memory>

#include "IMaskReaderPort.h"
#include "IWorkBenchPort.h"
#include "IImageReaderPort.h"
#include "ISceneManagerPort.h"

#include "InterSegUseCaseExport.h"

namespace TomoAnalysis::InterSeg::UseCase {
    class InterSegUseCase_API OpenData {
    public:
        OpenData(IImageReaderPort* ireader,IMaskReaderPort* mreader=nullptr,ISceneManagerPort* port=nullptr,IWorkBenchPort* workport = nullptr);
        virtual ~OpenData();
                
        auto LoadImage(const QString& imgPath)->bool;
        auto LoadMask(const QString& mskPath,bool fromLink = false)->bool;
        auto LoadImageNMask(const QString& imgPath,const QString& maskPath)->bool;        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}