#pragma once

#include <memory>

#include "ISceneManagerPort.h"
#include "IWorkBenchPort.h"

#include "InterSegUseCaseExport.h"

namespace TomoAnalysis::InterSeg::UseCase {
    class InterSegUseCase_API ActivateTool {
    public:
        ActivateTool(ISceneManagerPort* port,IWorkBenchPort* workPort);
        virtual ~ActivateTool();

        auto Request(const int &toolID)->bool;
        auto ImmediateUpdate(const int &functionID)->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}