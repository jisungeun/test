#pragma once

#include <QString>
#include <TCMask.h>

#include "InterSegUseCaseExport.h"

namespace TomoAnalysis::InterSeg::UseCase {
    class InterSegUseCase_API IMaskWriterPort {
    public:
        IMaskWriterPort();
        virtual ~IMaskWriterPort();

        virtual auto SetLayerNames(QStringList name)->void =0;
        virtual auto Write(TCMask::Pointer data, const QString& path)->bool = 0;
        virtual auto Modify(TCMask::Pointer data, const QString& path)->bool = 0;
        virtual auto AppendLayer(TCMask::Pointer src, TCMask::Pointer dst)->void = 0;
    };
}
