#pragma once

#include <WorkingSet.h>

#include "InterSegUseCaseExport.h"

namespace TomoAnalysis::InterSeg::UseCase {
	class InterSegUseCase_API IWorkBenchPort {
	public:
		IWorkBenchPort();
		virtual ~IWorkBenchPort();
				
		virtual auto Update() ->void = 0;
		virtual auto Update(Entity::WorkingSet::Pointer workingset)->void = 0;
	};
}