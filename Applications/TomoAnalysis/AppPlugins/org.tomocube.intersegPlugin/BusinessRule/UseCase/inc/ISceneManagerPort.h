#pragma once

#include <Tool.h>
#include <WorkingSet.h>

#include "InterSegUseCaseExport.h"

namespace TomoAnalysis::InterSeg::UseCase {
    class InterSegUseCase_API ISceneManagerPort {        
    public:
        typedef unsigned short ToolID;
        ISceneManagerPort();
        virtual ~ISceneManagerPort();

        virtual auto ActivateTool(ToolID id)->void = 0;
        virtual auto DeactivateTool(bool isFunc)->void = 0;        
        virtual auto Update(Entity::WorkingSet::Pointer workingset) ->void = 0;
        virtual auto UpdateFL(Entity::WorkingSet::Pointer workingset)->void = 0;
        virtual auto UpdateMultiViz(Entity::WorkingSet::Pointer workingset)->void = 0;
    };
}