#pragma once

#include <memory>

#include "IWorkBenchPort.h"
#include "ISceneManagerPort.h"

#include "InterSegUseCaseExport.h"

namespace TomoAnalysis::InterSeg::UseCase {
    class InterSegUseCase_API PerformTool {
    public:
        PerformTool(ISceneManagerPort* sport = nullptr,IWorkBenchPort* wport = nullptr);
        virtual ~PerformTool();

        auto PerformAlgorithm(QString algo_name)->bool;

    private:
        auto PerformDilate()->bool;
        auto PerformErode()->bool;
        auto PerformWaterShed()->bool;

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}