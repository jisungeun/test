#pragma once

#include <memory>

#include "IWorkBenchPort.h"

#include "InterSegUseCaseExport.h"

namespace TomoAnalysis::InterSeg::UseCase {
    class InterSegUseCase_API ModifyWorkBench {
    public:
        ModifyWorkBench();
        virtual ~ModifyWorkBench();

        auto ChangeLayout(IWorkBenchPort* port)const ->bool;
        auto RefreshInterface(Entity::WorkingSet::Pointer workingset,IWorkBenchPort* port)const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}