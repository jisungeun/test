#pragma once

#include <QString>
#include <TCMask.h>
#include <TCImage.h>

#include "InterSegUseCaseExport.h"

namespace TomoAnalysis::InterSeg::UseCase {
    class InterSegUseCase_API IMaskReaderPort {
    public:
        IMaskReaderPort();
        virtual ~IMaskReaderPort();

        virtual auto Read(const QString& path,int time_idx, const bool loadMaskVolume = false,const bool islabel = true) const->TCMask::Pointer = 0;
        virtual auto Create(int x0,int y0,int z0,int x1,int y1,int z1)->TCMask::Pointer = 0;
        virtual auto Create(TCImage::Pointer image, QString mask_name)->TCMask::Pointer = 0;
        virtual auto CreateLayer(TCMask::Pointer targetMask, QString mask_name)->void = 0;
    };
}
