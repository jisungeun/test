#pragma once

#include <memory>

#include "ISceneManagerPort.h"
#include "IWorkBenchPort.h"

#include "InterSegUseCaseExport.h"

namespace TomoAnalysis::InterSeg::UseCase {
    class InterSegUseCase_API FinishTool {
    public:
        FinishTool(ISceneManagerPort* port,IWorkBenchPort* workPort);
        virtual ~FinishTool();

        auto Request(bool isFunc)->bool;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}