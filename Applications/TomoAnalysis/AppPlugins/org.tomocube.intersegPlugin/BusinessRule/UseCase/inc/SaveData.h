#pragma once

#include <memory>
#include <QString>

#include "IMaskWriterPort.h"

#include "InterSegUseCaseExport.h"

namespace TomoAnalysis::InterSeg::UseCase {
    class InterSegUseCase_API SaveData {
    public:
        SaveData();
        virtual ~SaveData();

        auto Request(const QString& path,const QString& type,const QStringList& organList,IMaskWriterPort* writer)->bool;
        auto SaveAll(IMaskWriterPort* writer)->bool;
    private:        
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}