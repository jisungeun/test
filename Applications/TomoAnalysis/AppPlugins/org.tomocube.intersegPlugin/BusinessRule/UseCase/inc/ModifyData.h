#pragma once

#include <memory>

#include "IWorkBenchPort.h"
#include "ISceneManagerPort.h"
#include "IImageReaderPort.h"
#include "IMaskReaderPort.h"
#include "IMaskWriterPort.h"

#include "InterSegUseCaseExport.h"

namespace TomoAnalysis::InterSeg::UseCase {
    class InterSegUseCase_API ModifyData {
    public:
        ModifyData(ISceneManagerPort* sport = nullptr, IWorkBenchPort* wport = nullptr,IImageReaderPort* ireader = nullptr, IMaskReaderPort* mreader = nullptr,IMaskWriterPort* =nullptr);
        virtual ~ModifyData();

        auto MakeMask(const QString& maskName)->bool;
        auto ConfirmMask(const QString& maskName)->bool;
        auto WorkingMask(const QString& maskName,bool unset = false)->bool;
        auto ChangeTimeStep(const int& step)->bool;
        auto ChangeSlice(int axis, int index)->bool;
        auto ChangeSlice(int x, int y, int z)->bool;
        auto ChangeSlice(float phyX, float phyY, float phyZ)->bool;
        auto ChangeUser(QString ID, QString maskPath)->bool;
        auto ShowFLSlice(int ch)->bool;
        auto SetFLOpacity(int ch, double opa)->bool;

        auto SetMultiViz(const QString& maskName, bool viz)->bool;

    private:
        auto SetWorkingMask(const QString& mask_name)->bool;
        auto UnsetWorkingMask(const QString& mask_name)->bool;
        auto isMaskExist(const QString& mask_name)->bool;
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}