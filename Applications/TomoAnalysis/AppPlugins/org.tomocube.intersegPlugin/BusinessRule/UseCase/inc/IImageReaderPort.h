#pragma once

#include <QString>
#include <TCImage.h>
#include <TCFMetaReader.h>

#include "InterSegUseCaseExport.h"

namespace TomoAnalysis::InterSeg::UseCase {
    class InterSegUseCase_API IImageReaderPort {
    public:
        IImageReaderPort();
        virtual ~IImageReaderPort();

        virtual auto Read(const QString& path, const bool loadImage = false,int time_step=0) const->TCImage::Pointer=0;
        virtual auto ReadFL(const QString& path, int ch, int time_step = 0) const->TCImage::Pointer = 0;
        virtual auto ReadMeta(const QString& path)const->TC::IO::TCFMetaReader::Meta::Pointer = 0;
    };
}