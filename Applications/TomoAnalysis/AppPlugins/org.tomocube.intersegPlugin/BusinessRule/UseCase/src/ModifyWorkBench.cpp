#include "ModifyWorkBench.h"

namespace TomoAnalysis::InterSeg::UseCase {
    struct ModifyWorkBench::Impl {
        
    };

    ModifyWorkBench::ModifyWorkBench() :d{ new Impl } {
        
    }
    ModifyWorkBench::~ModifyWorkBench() {
        
    }
    auto ModifyWorkBench::ChangeLayout(IWorkBenchPort* port) const -> bool {
        if(nullptr == port) {
            return false;
        }
        port->Update();
        return true;
    }
    auto ModifyWorkBench::RefreshInterface(Entity::WorkingSet::Pointer workingset,IWorkBenchPort* port) const -> bool {
        if(nullptr == port) {
            return false;
        }
        port->Update(workingset);
        return true;
    }

}