#include "FinishTool.h"

namespace TomoAnalysis::InterSeg::UseCase {
    struct FinishTool::Impl {
        ISceneManagerPort* outPort;
        IWorkBenchPort* workPort;
    };
    FinishTool::FinishTool(ISceneManagerPort* port, IWorkBenchPort* workPort) : d{ new Impl } {
        d->outPort = port;
        d->workPort = workPort;
    }

    FinishTool::~FinishTool() {

    }
    auto FinishTool::Request(bool isFunc) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        ws->SetCurToolIdx(-1);
        d->outPort->DeactivateTool(isFunc);
        if(nullptr != d->workPort) {
            d->workPort->Update(ws);
        }
        return true;
    }
}