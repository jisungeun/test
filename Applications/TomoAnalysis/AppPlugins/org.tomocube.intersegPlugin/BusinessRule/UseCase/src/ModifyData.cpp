#include <iostream>
#include <QFileInfo>

#include <WorkingSet.h>

#include "ModifyData.h"

namespace TomoAnalysis::InterSeg::UseCase {
    struct ModifyData::Impl {
        IWorkBenchPort* wport;
        ISceneManagerPort* sport;        
        IImageReaderPort* ireader;
        IMaskReaderPort* mreader;
        IMaskWriterPort* mwriter;
    };
    ModifyData::ModifyData(ISceneManagerPort* sport, IWorkBenchPort* wport, IImageReaderPort* ireader, IMaskReaderPort* mreader,IMaskWriterPort* mwriter) :d{ new Impl } {
        d->wport = wport;
        d->sport = sport;
        d->ireader = ireader;
        d->mreader = mreader;
        d->mwriter = mwriter;
    }
    ModifyData::~ModifyData() {
        
    }
    auto ModifyData::ConfirmMask(const QString& maskName) -> bool {
        Q_UNUSED(maskName)
        return true;
    }
    auto ModifyData::ChangeSlice(int x, int y, int z) -> bool {
        if(nullptr == d->wport) {
            return false;
        }
        auto ws = Entity::WorkingSet::GetInstance();
        ws->SetChangeSlice(true);
        ws->SetCurrentSlice(x, y, z);
        d->wport->Update(ws);
        return true;
    }
    auto ModifyData::ChangeSlice(float phyX, float phyY, float phyZ) -> bool {
        if(nullptr == d->wport) {
            return false;
        }
        auto ws = Entity::WorkingSet::GetInstance();
        ws->SetChangeSlice(true);
        double res[3];
        ws->GetImageData()->GetResolution(res);
        ws->SetCurrentSlice(phyX / res[0], phyY / res[1], phyZ / res[2]);
        d->wport->Update(ws);
        return true;
    }
    auto ModifyData::ShowFLSlice(int ch) -> bool {
        if(nullptr == d->wport) {
            return false;
        }        
        auto ws = Entity::WorkingSet::GetInstance();
        ws->SetCurFLCh(ch);        

        d->wport->Update(ws);

        return true;
    }
    auto ModifyData::SetFLOpacity(int ch, double opa) -> bool {
        Q_UNUSED(ch)
        Q_UNUSED(opa)
        return true;
    }
    auto ModifyData::ChangeSlice(int axis, int index) -> bool {
        if(nullptr == d->wport) {
            return false;
        }        
        auto ws = Entity::WorkingSet::GetInstance();
        ws->SetChangeSlice(true);

        int prev[3];
        ws->GetCurrentSlice(prev);
        int x = 0;
        int y = 0;
        int z = 0;
        if(axis == 0) {
            x = index;
            y = prev[1];
            z = prev[2];
        }else if(axis == 1) {
            x = prev[0];
            y = index;
            z = prev[2];
        }else if (axis == 2) {
            x = prev[0];
            y = prev[1];
            z = index;
        }
        ws->SetCurrentSlice(x,y,z);

        d->wport->Update(ws);
        return true;
    }
    auto ModifyData::SetMultiViz(const QString& maskName, bool viz) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        if(nullptr == d->wport) {
            return false;
        }
        if(nullptr == d->sport) {
            return false;
        }

        ws->SetMultiViz(maskName, viz);

        d->sport->UpdateMultiViz(ws);
        d->wport->Update(ws);
        return true;
    }

    auto ModifyData::ChangeTimeStep(const int& step) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        if(nullptr == d->wport) {
            return false;
        }
        if(nullptr == d->sport) {
            return false;
        }
        if(nullptr == d->ireader) {
            return false;
        }
        if(nullptr == d->mreader) {
            return false;
        }
        if(nullptr == d->mwriter) {
            return false;
        }
        auto img_path = ws->GetImagePath();
        if(img_path.isEmpty()) {            
            return false;
        }
        //change image data in workingset        
        auto img = d->ireader->Read(img_path, true, step);        
        if(nullptr == img) {
            return false;
        }
        ws->SetCurTimeStep(step);
        ws->SetImageData(img);                
        auto maskChanged = false;
        //try to load mask data        
        
        auto mask_path = ws->GetMaskPath();        
        if (false == mask_path.isEmpty()) {
            auto instData = ws->GetInstData();
            if (nullptr != instData && ws->GetCurUser() != "System") {
                d->mwriter->SetLayerNames(instData->GetLayerNames());
                if (ws->GetImageData()->GetTimeSteps() > 1) {
                    d->mwriter->Modify(instData, mask_path);
                }
                else {
                    d->mwriter->Write(instData, mask_path);
                }
            }
            ws->SetInstData(nullptr);
            auto inst_mask = d->mreader->Read(mask_path, step, true);
            if (nullptr != inst_mask) {
                ws->SetInstData(inst_mask);
                maskChanged = true;
            }

            auto organData = ws->GetOrganData();
            if (nullptr != organData && ws->GetCurUser() != "System") {
                d->mwriter->SetLayerNames(organData->GetLayerNames());
                if (ws->GetImageData()->GetTimeSteps() > 1) {
                    d->mwriter->Modify(organData, mask_path);
                }
                else {
                    d->mwriter->Write(organData, mask_path);
                }
            }
            ws->SetOrganData(nullptr);
            auto organ_mask = d->mreader->Read(mask_path, step, true,false);
            if (nullptr != organ_mask) {
                ws->SetOrganData(organ_mask);
                maskChanged = true;
            }
        }        


        /*auto inst_path = ws->GetInstPath();
        if(!inst_path.isEmpty()) {
            //save previous inst mask
            auto prev_inst = ws->GetInstData();
            if(nullptr != prev_inst && ws->GetCurUser() != "System") {
                d->mwriter->SetLayerNames(prev_inst->GetLayerNames());
                if(ws->GetImageData()->GetTimeSteps() >1) {
                    d->mwriter->Modify(prev_inst, inst_path);
                }
                else {
                    if (QFile::exists(inst_path)) {
                        QFile::remove(inst_path);
                    }
                    d->mwriter->Write(prev_inst, inst_path);
                }
            }
            ws->SetInstData(nullptr);
            auto inst_mask = d->mreader->Read(inst_path, step, true);
            if(nullptr!= inst_mask) {
                ws->SetInstData(inst_mask);
                maskChanged = true;
            }
        }*/
        /*auto organ_path = ws->GetOrganPath();
        if(!organ_path.isEmpty()) {
            //save previous organ mask
            auto prev_organ = ws->GetOrganData();
            if(nullptr != prev_organ && ws->GetCurUser()!="System") {
                d->mwriter->SetLayerNames(prev_organ->GetLayerNames());
                if (ws->GetImageData()->GetTimeSteps() > 1) {
                    d->mwriter->Modify(prev_organ, organ_path);
                }
                else {
                    if (QFile::exists(organ_path)) {
                        QFile::remove(organ_path);
                    }
                    d->mwriter->Write(prev_organ, organ_path);
                }                                
            }
            ws->SetOrganData(nullptr);
            auto organ_mask = d->mreader->Read(organ_path, step, true);
            if(nullptr!=organ_mask) {                
                ws->SetOrganData(organ_mask);
                maskChanged = true;
            }
        }*/

        //reset working data & working tool
        ws->SetCurMaskName(QString());
        ws->SetCurToolIdx(-1);

        auto meta = ws->GetMetaInfo();

        if(meta->data.data3DFL.exist) {
            auto cur_ch = ws->GetCurFLCh();
            if(cur_ch > -1) {
                auto flImage = d->ireader->ReadFL(img_path, cur_ch, step);
                if(nullptr != flImage) {
                    ws->SetImageFLData(flImage,cur_ch);
                }
            }
        }

        d->sport->Update(ws);

        d->wport->Update(ws);

        return true;
    }
    auto ModifyData::ChangeUser(QString ID, QString maskPath) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        if(ws->GetCurUser() == ID) {
            //std::cout << "User is same" << std::endl;
            return true;
        }
        auto prev_user = ws->GetCurUser();
        ws->SetCurUser(ID);
        //ws->SetCurTimeStep(0);
        auto mpath = ws->GetMaskPath();

        auto instData = ws->GetInstData();
        if(nullptr != instData && prev_user != "System") {
            d->mwriter->SetLayerNames(instData->GetLayerNames());
            d->mwriter->Write(instData, mpath);
        }
        auto organData = ws->GetOrganData();
        if(nullptr != organData && prev_user != "System") {
            d->mwriter->SetLayerNames(organData->GetLayerNames());
            d->mwriter->Write(organData, mpath);
        }
        ws->SetMaskPath(maskPath);
        auto ts = ws->GetCurTimeStep();
        if (ts < 0)
            ts = 0;
        QFileInfo mask(maskPath);
        if (mask.exists()) {
            auto instMask = d->mreader->Read(maskPath, ts, true, true);
            if (nullptr != instMask) {
                ws->SetInstData(instMask);
            }
            else {
                ws->SetInstData(nullptr);
            }
            auto organMask = d->mreader->Read(maskPath, ts, true, false);
            if (nullptr != organMask) {
                ws->SetOrganData(organMask);
            }
            else {
                ws->SetOrganData(nullptr);
            }
        }        
        ws->ClearMultiViz();

        d->sport->Update(ws);
        d->wport->Update(ws);

        return true;
    }
    auto ModifyData::MakeMask(const QString& maskName) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();        
        auto img = ws->GetImageData();
        if(nullptr == img) {
            return false;
        }

        if(maskName == "cellInst") {
            if(nullptr == ws->GetInstData()) {
                auto mask = d->mreader->Create(img, maskName);
                if (nullptr == mask) {
                    return false;
                }
                mask->SetTimeStep(ws->GetCurTimeStep());
                ws->SetInstData(mask);
            }
        }else {
            if(nullptr == ws->GetOrganData()) {
                auto mask = d->mreader->Create(img, maskName);                
                if (nullptr == mask) {
                    return false;
                }
                mask->SetTimeStep(ws->GetCurTimeStep());
                ws->SetOrganData(mask);
            }else {
                auto existingMask = ws->GetOrganData();
                d->mreader->CreateLayer(existingMask, maskName);
            }
        }                
        //d->sport->Update(ws);
        d->wport->Update(ws);

        return true;
    }
    auto ModifyData::WorkingMask(const QString& maskName,bool unset) -> bool {
        if(unset) {
            return UnsetWorkingMask(maskName);
        }else {
            return SetWorkingMask(maskName);
        }
    }
    auto ModifyData::SetWorkingMask(const QString& mask_name) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        auto img = ws->GetImageData();
        if(nullptr == img) {
            return false;
        }
        if(false == isMaskExist(mask_name)) {
            return false;
        }
        ws->SetCurMaskName(mask_name);
        ws->ClearMultiViz();

        d->wport->Update(ws);        
        d->sport->Update(ws);
        

        return true;
    }
    auto ModifyData::UnsetWorkingMask(const QString& mask_name) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        auto img = ws->GetImageData();
        if (nullptr == img) {
            return false;
        }
       
        if(ws->GetCurMaskName() == mask_name) {            
            ws->SetCurMaskName(QString());
            ws->SetCurToolIdx(-1);
            d->sport->Update(ws);            
            d->wport->Update(ws);
        }
        return true;
    }
    auto ModifyData::isMaskExist(const QString& mask_name) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();
        if(mask_name =="cellInst") {
            auto instMask = ws->GetInstData();
            if(nullptr == instMask) {
                return false;
            }
        }else {
            auto organMask = ws->GetOrganData();
            if(nullptr == organMask) {
                return false;
            }
            auto maskVolume = organMask->GetMaskLayer(mask_name);
            if(nullptr == maskVolume) {
                return false;
            }
        }
        return true;
    }
}