#include <iostream>

#include <WorkingSet.h>
#include <TCDataConverter.h>

#include "PerformTool.h"

namespace TomoAnalysis::InterSeg::UseCase {
    struct PerformTool::Impl {
        IWorkBenchPort* wport;
        ISceneManagerPort* sport;
    };
    PerformTool::PerformTool(ISceneManagerPort* sport, IWorkBenchPort* wport) : d{ new Impl } {
        d->wport = wport;
        d->sport = sport;
    }
    PerformTool::~PerformTool() {
        
    }
    auto PerformTool::PerformAlgorithm(QString algo_name) -> bool {
        if(algo_name == "Dilate") {
            return PerformDilate();
        }
        if(algo_name == "Erode") {
            return PerformErode();
        }
        if(algo_name == "Watershed") {
            return PerformWaterShed();
        }
        return false;
    }
    auto PerformTool::PerformDilate() -> bool {
        return true;
    }
    auto PerformTool::PerformErode() -> bool {
        return true;
    }
    auto PerformTool::PerformWaterShed() -> bool {
        return true;
    }
}