#include "ActivateTool.h"

namespace TomoAnalysis::InterSeg::UseCase {
    struct ActivateTool::Impl {
        ISceneManagerPort* outPort;
        IWorkBenchPort* workPort;
    };
    ActivateTool::ActivateTool(ISceneManagerPort* port,IWorkBenchPort* workPort) : d{ new Impl } {
        d->outPort = port;
        d->workPort = workPort;
    }
    ActivateTool::~ActivateTool() {
        
    }
    auto ActivateTool::Request(const int& toolID) -> bool {
        auto ws = Entity::WorkingSet::GetInstance();        
        ws->SetCurToolIdx(toolID);
        d->outPort->ActivateTool(toolID);
        if(nullptr != d->workPort) {            
            d->workPort->Update(ws);
        }
        return true;
    }
    auto ActivateTool::ImmediateUpdate(const int& functionID) -> bool {
        Q_UNUSED(functionID)
        return true;
    }

}