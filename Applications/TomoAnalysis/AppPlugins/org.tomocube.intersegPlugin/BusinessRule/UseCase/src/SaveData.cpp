#include <iostream>
#include <QFile>

#include <WorkingSet.h>

#include "SaveData.h"

namespace TomoAnalysis::InterSeg::UseCase {
    struct SaveData::Impl {
        QString path;
    };
    SaveData::SaveData() : d{ new Impl } {
        
    }
    SaveData::~SaveData() {
        
    }
    auto SaveData::Request(const QString& path, const QString& type, const QStringList& organList, IMaskWriterPort* writer) -> bool {
        if(nullptr == writer) {
            return false;
        }
        auto ws = Entity::WorkingSet::GetInstance();
        if(type == "MultiLabel") {
            if(nullptr == ws->GetInstData()) {
                return false;
            }
            auto mask = ws->GetInstData();            
            writer->SetLayerNames(organList);
            writer->Write(mask, path);
        }else if(type =="MultiLayer") {
            if(nullptr == ws->GetOrganData()) {
                return false;
            }
            auto mask = ws->GetOrganData();            
            writer->SetLayerNames(organList);
            writer->Write(mask, path);

        }else {
            return false;
        }
        return true;
    }
    auto SaveData::SaveAll(IMaskWriterPort* writer) -> bool {
        if (nullptr == writer) {
            return false;
        }
        auto ws = Entity::WorkingSet::GetInstance();

        if (ws->GetMaskPath().isEmpty()) {
            return false;
        }

        if(nullptr == ws->GetInstData() && nullptr == ws->GetOrganData()) {
            return false;
        }        
        auto maskPath = ws->GetMaskPath();
        if(nullptr != ws->GetInstData()) {            
            auto mask = ws->GetInstData();
            QStringList layername;
            layername.push_back("cellInst");
            writer->SetLayerNames(layername);
            if(ws->GetImageData()->GetTimeSteps()>1) {
                writer->Modify(mask, maskPath);
            }else {
                writer->Write(mask, maskPath);
            }                                            
        }
        
        if(nullptr != ws->GetOrganData()){
            auto mask = ws->GetOrganData();
            writer->SetLayerNames(mask->GetLayerNames());
            if (ws->GetImageData()->GetTimeSteps() > 1) {
                writer->Modify(mask, maskPath);
            }else {
                writer->Write(mask, maskPath);
            }                                                            
        }
        return true;
    }
}
