﻿#pragma once

#include <iostream>

#include <WorkingSet.h>

#include "OpenData.h"

namespace TomoAnalysis::InterSeg::UseCase {
    struct OpenData::Impl {
        IImageReaderPort* ireader;
        IMaskReaderPort* mreader;
        ISceneManagerPort* port;
        IWorkBenchPort* wport;
    };
    OpenData::OpenData(IImageReaderPort* ireader,IMaskReaderPort* mreader,ISceneManagerPort* port,IWorkBenchPort* workport) : d{ new Impl } {
        d->ireader = ireader;
        d->mreader = mreader;
        d->port = port;
        d->wport = workport;
    }
    OpenData::~OpenData() {

    }
    auto OpenData::LoadImageNMask(const QString& imgPath,const QString& maskPath) -> bool {
        if(nullptr == d->ireader) {
            return false;
        }
        if (nullptr == d->mreader) {
            return false;            
        }
        if(nullptr == d->port) {
            return false;
        }
        if(nullptr == d->wport) {
            return false;
        }
        auto workingset = Entity::WorkingSet::GetInstance();
        workingset->Reset();

        workingset->SetImagePath(imgPath);        

        auto image = d->ireader->Read(imgPath,true);
        if (nullptr == image.get()) return false;
        workingset->SetImageData(image);        

        workingset->SetMaskPath(maskPath);

        auto isMask = false;
        if (false == maskPath.isEmpty()) {
            auto instMask = d->mreader->Read(maskPath, 0, true, true);
            if (nullptr != instMask) {
                isMask = true;
                workingset->SetInstData(instMask);
            }
            auto organMask = d->mreader->Read(maskPath, 0, true, false);
            if (nullptr != organMask) {
                isMask = true;
                workingset->SetOrganData(organMask);
            }
        }        
        
        if (false == isMask) {
            return false;
        }

        d->port->Update(workingset);
        d->wport->Update(workingset);

        return true;
    }
    auto OpenData::LoadImage(const QString& imgPath) -> bool {
        if(nullptr == d->ireader) {
            return false;
        }
        if(nullptr == d->port) {
            return false;
        }
        if(nullptr == d->wport) {
            return false;
        }
        auto workingset = Entity::WorkingSet::GetInstance();
        //reset working set first
        workingset->Reset();

        workingset->SetImagePath(imgPath);

        //set default mask save path
        auto image_base = imgPath.chopped(4);
        auto default_mask_path = image_base + ".msk";

        workingset->SetMaskPath(default_mask_path);

        auto meta = d->ireader->ReadMeta(imgPath);        
        workingset->SetMetaInfo(meta);

        auto image = d->ireader->Read(imgPath, true);
        if (nullptr == image.get()) return false;
        workingset->SetImageData(image);
        workingset->SetCurTimeStep(0);

        if(meta->data.data3DFL.exist) {
            for(auto i=0;i<3;i++){
                if(meta->data.data3DFL.valid[i]) {                    
                    auto flImage = d->ireader->ReadFL(imgPath, i, 0);
                    workingset->SetImageFLData(flImage, i);
                }
            }
        }

        d->port->Update(workingset);
        d->wport->Update(workingset);

        return true;
    }
    auto OpenData::LoadMask(const QString& mskPath,bool fromLink) -> bool {
        if(nullptr == d->mreader) {
            return false;
        }
        if(nullptr == d->port) {
            return false;
        }
        if(nullptr == d->wport) {
            return false;
        }
        auto workingset = Entity::WorkingSet::GetInstance();
        auto img = workingset->GetImageData();
        if(nullptr == img) {
            return false;
        }        

        auto AreSame = [&](double a, double b) {
            return fabs(a - b) < 0.00001;
        };

        auto cur_time = workingset->GetCurTimeStep();        

        double res[3];
        img->GetResolution(res);
        int imgSize[3];
        imgSize[0] = std::get<0>(img->GetSize());
        imgSize[1] = std::get<1>(img->GetSize());
        imgSize[2] = std::get<2>(img->GetSize());        

        auto instMask = d->mreader->Read(mskPath, cur_time, true);        
        if (nullptr != instMask) {
            auto mres = instMask->GetResolution();
            double maskRes[3]{ std::get<0>(mres),std::get<1>(mres),std::get<2>(mres) };
            auto msize = instMask->GetSize();
            int maskSize[3]{ std::get<0>(msize),std::get<1>(msize),std::get<2>(msize) };
            for (auto i = 0; i < 3; i++) {
                if (false == AreSame(maskRes[i], res[i])) {
                    std::cout << "mask :" << maskRes[i] << " image: " << res[i] << "are not same" << std::endl;
                    return false;
                }
                if (imgSize[i] != maskSize[i]) {
                    std::cout << "mask :" << maskSize[i] << " image: " << imgSize[i] << "are not same" << std::endl;
                    return false;
                }
            }
            if (fromLink) {
                workingset->SetMaskPath(mskPath);
            }
            else if (workingset->GetMaskPath().isEmpty()) {
                return false;
            }
            workingset->SetInstData(instMask);
        }
        auto organMask = d->mreader->Read(mskPath, cur_time, true, false);
        if (nullptr != organMask) {            
            auto mres = organMask->GetResolution();
            double maskRes[3]{ std::get<0>(mres),std::get<1>(mres),std::get<2>(mres) };
            auto msize = organMask->GetSize();
            int maskSize[3]{ std::get<0>(msize),std::get<1>(msize),std::get<2>(msize) };
            for (auto i = 0; i < 3; i++) {
                if (false == AreSame(maskRes[i], res[i])) {
                    return false;
                }
                if (imgSize[i] != maskSize[i]) {
                    return false;
                }
            }
            if (fromLink) {
                workingset->SetMaskPath(mskPath);
            }
            else if (workingset->GetMaskPath().isEmpty()) {
                return false;
            }
            workingset->SetOrganData(organMask);
        }        
        /*
        auto mask = d->mreader->Read(mskPath,cur_time, true);       
        
        mask->SetResolution(res);

        int maskSize[3];        
        
        if (nullptr == mask.get()) return false;

        maskSize[0] = std::get<0>(mask->GetSize());
        maskSize[1] = std::get<1>(mask->GetSize());
        maskSize[2] = std::get<2>(mask->GetSize());

        if(maskSize[0] != imgSize[0] || maskSize[1] != imgSize[1] || maskSize[2] != imgSize[2]) {
            return false;
        }

        if(mask->GetType()._to_integral() == MaskTypeEnum::MultiLabel) {
            if (fromLink) {
                workingset->SetInstPath(mskPath);
            }else if(workingset->GetInstPath().isEmpty()) {
                return false;
            }
            workingset->SetInstData(mask);
        }else if(mask->GetType()._to_integral() == MaskTypeEnum::MultiLayer) {
            if (fromLink) {
                workingset->SetOrganPath(mskPath);
            }else if(workingset->GetOrganPath().isEmpty()) {
                return false;
            }
            workingset->SetOrganData(mask);
        }*/
                
        d->port->Update(workingset);
        d->wport->Update(workingset);

        return true;
    }

}