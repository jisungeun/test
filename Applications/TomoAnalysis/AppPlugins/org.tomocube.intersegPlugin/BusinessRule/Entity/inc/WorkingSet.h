#pragma once

#include <memory>
#include <QString>

#include <TCImage.h>
#include <TCMask.h>
#include <TCFMetaReader.h>

#include "History.h"

#include "InterSegEntityExport.h"

namespace TomoAnalysis::InterSeg::Entity {
    class InterSegEntity_API WorkingSet {
    public:
        typedef WorkingSet Self;
        typedef std::shared_ptr<Self> Pointer;            
        virtual ~WorkingSet();

        static auto GetInstance(void)->Pointer;

        auto SetWorkingDirectory(const QString& path)->void;
        auto GetWorkingDirectory()const->QString;

        auto SetCurUser(const QString& name)->void;
        auto GetCurUser()const->QString;

        auto SetCurMaskName(const QString& name)->void;
        auto GetCurMaskName()const->QString;

        auto SetMetaInfo(const TC::IO::TCFMetaReader::Meta::Pointer meta)->void;
        auto GetMetaInfo()const->TC::IO::TCFMetaReader::Meta::Pointer;

        auto SetImagePath(const QString& path)->void;
        auto GetImagePath()const->QString;
        auto SetImageData(TCImage::Pointer image)->void;
        auto GetImageData()->TCImage::Pointer;
        auto SetImageFLData(TCImage::Pointer fl, int ch)->void;
        auto GetImageFLData(int ch)->TCImage::Pointer;
        auto SetCurFLCh(int ch)->void;
        auto GetCurFLCh()->int;
        auto SetFLOpacity(int ch, double opa)->void;
        auto GetFLOpacity(int ch)->double;

        auto SetMaskPath(const QString& path)->void;
        auto GetMaskPath()const->QString;

        auto SetInstData(TCMask::Pointer mask)->void;
        auto GetInstData()->TCMask::Pointer;

        auto SetOrganData(TCMask::Pointer mask)->void;
        auto GetOrganData()->TCMask::Pointer;

        auto SetHistoryPath(const QString& path)->void;
        auto GetHistoryPath()const->QString;

        auto SetHistoryData(History::Pointer history)->void;
        auto GetHistoryData()->History::Pointer;

        auto SetCurToolIdx(int idx)->void;
        auto GetCurToolIdx()->int;

        auto SetCurLabel(int label)->void;
        auto GetCurLabel()->int;

        auto SetCurTimeStep(int step)->void;
        auto GetCurTimeStep()->int;

        auto SetChangeSlice(bool change)->void;
        auto GetChangeSlice()->bool;

        auto SetCurrentSlice(int x, int y, int z)->void;
        auto GetCurrentSlice(int index[3])->void;

        auto SetMultiViz(QString name, bool viz)->void;
        auto GetMultiViz(QString name)->bool;
        auto GetMultiViz()->QMap<QString, bool>;
        auto ClearMultiViz()->void;

        auto SetResetView(bool reset)->void;
        auto GetResetView()->bool;        

        auto Reset()->void;

    protected:
        WorkingSet();                

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}