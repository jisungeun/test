#pragma once

#include <memory>

#include <enum.h>

#include "InterSegEntityExport.h"

namespace TomoAnalysis::InterSeg::Entity {
    BETTER_ENUM(ToolIdx, int,
        None = -1,
        PAINT = 1, 
        WIPE =2,
        Fill =3,
        ERASE = 4,
        ADD = 5,
        SUBTRACT =6,
        PICK = 7,
        CLEAN = 8,
        CLEAN_SEL = 9,
        FLUSH = 10,
        FLUSH_SEL = 11,
        DIVIDE = 112,
        BRANCH = 13,
        MERGE_LABEL = 114,
        ERODE = 15,
        DILATE = 16,
        WATERSHED = 17,
        PICKDELETE = 18,
        LASSODELETE = 19,
        SIZEFILTER = 20
        );
    class InterSegEntity_API Tool {
    public:
        typedef Tool Self;
        typedef std::shared_ptr<Self> Pointer;

        Tool();
        Tool(const Tool& other);
        virtual ~Tool();

    private:
        struct Impl;
        std::unique_ptr<Impl> d;        
    };
}