#pragma once

#include <memory>

#include "InterSegEntityExport.h"

namespace TomoAnalysis::InterSeg::Entity {
    class InterSegEntity_API History {
    public:
        typedef History Self;
        typedef std::shared_ptr<Self> Pointer;

        History();
        History(const History& other);
        virtual ~History();;
        
        auto Init()->void;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}