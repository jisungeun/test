#include "History.h"

namespace TomoAnalysis::InterSeg::Entity {
    struct History::Impl {
        
    };
    History::History() : d{ new Impl } {
        
    }
    History::History(const History& other) : d{ new Impl } {
        *d = *other.d;
    }
    History::~History() {
        
    }
    auto History::Init() -> void {
        
    }
}