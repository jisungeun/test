#include "Tool.h"

namespace TomoAnalysis::InterSeg::Entity {
    struct Tool::Impl {
        
    };
    Tool::Tool() : d{ new Impl } {
        
    }
    Tool::Tool(const Tool& other) : d{ new Impl } {
        *d = *other.d;
    }
    Tool::~Tool() {

    }
}