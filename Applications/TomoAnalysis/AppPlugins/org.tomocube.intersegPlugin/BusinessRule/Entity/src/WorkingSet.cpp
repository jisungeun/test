#include <iostream>
#include <QFileInfo>
#include <QMap>

#include "WorkingSet.h"

namespace TomoAnalysis::InterSeg::Entity {
    struct WorkingSet::Impl {
        QString cur_mask_name;
        QString cur_user_name;

        QString work_dir;

        QString tcfPath;
        QString mskPath;
        QString customPath;
        QString historyPath;

        TCImage::Pointer imageData;
        TCImage::Pointer imageFLData[3];
        double flOpacity[3]{ 0.5, 0.5,0.5};
        int curCh{ -1 };
        TCMask::Pointer instMask;
        TCMask::Pointer organMask;
        TCMask::Pointer customMask;
        History::Pointer historyData;
        TC::IO::TCFMetaReader::Meta::Pointer meta;

        //multi-layer variables
        QMap<QString, bool> multiViz;
                
        bool sliceChange{ false };
        int curSlice[3];

        int cur_tool_idx{ -1 };
        int cur_label{ 1 };
        int cur_time_step{ 0 };

        bool resetView = false;
    };
    WorkingSet::WorkingSet():d{new Impl} {
        
    }
    WorkingSet::~WorkingSet() {
        
    }
    auto WorkingSet::GetResetView() -> bool {
        return d->resetView;
    }
    auto WorkingSet::SetResetView(bool reset) -> void {
        d->resetView = reset;
    }

    auto WorkingSet::GetChangeSlice() -> bool {
        return d->sliceChange;
    }

    auto WorkingSet::SetChangeSlice(bool change) -> void {
        d->sliceChange = change;
    }

    auto WorkingSet::GetCurrentSlice(int index[3])->void{
        for(auto i=0;i<3;i++) {
            index[i] = d->curSlice[i];
        }
    }

    auto WorkingSet::SetCurrentSlice(int x, int y, int z) -> void {
        d->curSlice[0] = x;
        d->curSlice[1] = y;
        d->curSlice[2] = z;
    }

    auto WorkingSet::GetCurTimeStep() -> int {
        return d->cur_time_step;
    }

    auto WorkingSet::SetCurTimeStep(int step) -> void {
        d->cur_time_step = step;
    }

    auto WorkingSet::GetCurLabel() -> int {
        return d->cur_label;
    }

    auto WorkingSet::SetCurLabel(int label) -> void {
        d->cur_label = label;
    }

    auto WorkingSet::GetCurToolIdx() -> int {
        return d->cur_tool_idx;
    }
    auto WorkingSet::SetCurToolIdx(int idx) -> void {
        d->cur_tool_idx = idx;
    }

    auto WorkingSet::GetCurUser() const -> QString {
        return d->cur_user_name;
    }

    auto WorkingSet::SetCurUser(const QString& name) -> void {
        d->cur_user_name = name;
    }
    
    auto WorkingSet::GetCurMaskName() const -> QString {
        return d->cur_mask_name;
    }

    auto WorkingSet::SetCurMaskName(const QString& name) -> void {
        d->cur_mask_name = name;
    }

    auto WorkingSet::Reset() -> void {
        d->imageData = nullptr;
        d->instMask = nullptr;
        d->organMask = nullptr;
        d->customMask = nullptr;
        d->historyData = nullptr;
        d->meta = nullptr;
        for(auto i=0;i<3;i++) {
            d->imageFLData[i] = nullptr;
            d->flOpacity[i] = 0.5;            
        }
        d->curCh = -1;
        d->cur_label = 1;
        d->cur_tool_idx = -1;
        d->cur_time_step = 0;
        
        d->work_dir = QString();
        d->tcfPath = QString();
        d->mskPath = QString();
        d->customPath = QString();
        d->historyPath = QString();

        d->cur_mask_name = QString();

        d->multiViz.clear();

        d->resetView = true;
    }

    auto WorkingSet::ClearMultiViz() -> void {
        d->multiViz.clear();
    }

    auto WorkingSet::GetMultiViz(QString name) -> bool {
        if(d->multiViz.contains(name)) {
            return d->multiViz[name];
        }
        return false;
    }

    auto WorkingSet::GetMultiViz() -> QMap<QString, bool> {
        return d->multiViz;
    }

    auto WorkingSet::SetMultiViz(QString name, bool viz) -> void {
        d->multiViz[name] = viz;
    }

    auto WorkingSet::GetInstance() -> Pointer {
        static Pointer theInstance{ new WorkingSet() };
        return theInstance;
    }
    auto WorkingSet::SetMetaInfo(const TC::IO::TCFMetaReader::Meta::Pointer meta) -> void {
        d->meta = meta;
    }
    auto WorkingSet::GetMetaInfo() const -> TC::IO::TCFMetaReader::Meta::Pointer {
        return d->meta;
    }
    auto WorkingSet::SetImagePath(const QString& path) -> void {
        d->tcfPath = path;
    }
    auto WorkingSet::GetImagePath() const -> QString {
        return d->tcfPath;
    }
    auto WorkingSet::SetImageData(TCImage::Pointer image) -> void {
        d->imageData = image;
        auto mm = d->imageData->GetMinMax();
    }    
    auto WorkingSet::GetImageData() -> TCImage::Pointer {
        return d->imageData;
    }
    auto WorkingSet::SetImageFLData(TCImage::Pointer fl, int ch) -> void {
        d->imageFLData[ch] = fl;
    }
    auto WorkingSet::GetImageFLData(int ch) -> TCImage::Pointer {
        return d->imageFLData[ch];
    }
    auto WorkingSet::GetFLOpacity(int ch) -> double {
        return d->flOpacity[ch];
    }
    auto WorkingSet::SetFLOpacity(int ch, double opa) -> void {
        d->flOpacity[ch] = opa;
    }
    auto WorkingSet::GetCurFLCh() -> int {
        return d->curCh;
    }
    auto WorkingSet::SetCurFLCh(int ch) -> void {
        d->curCh = ch;
    }    
    auto WorkingSet::GetInstData() -> TCMask::Pointer {
        return d->instMask;
    }
    auto WorkingSet::SetInstData(TCMask::Pointer mask) -> void {
        d->instMask = mask;
    }
    auto WorkingSet::SetMaskPath(const QString& path)->void {
        d->mskPath = path;
    }
    auto WorkingSet::GetMaskPath()const->QString {
        return d->mskPath;
    }
    auto WorkingSet::GetOrganData() -> TCMask::Pointer {
        return d->organMask;
    }
    auto WorkingSet::SetOrganData(TCMask::Pointer mask) -> void {
        d->organMask = mask;
    }

    auto WorkingSet::SetHistoryPath(const QString& path) -> void {
        d->historyPath = path;
    }
    auto WorkingSet::GetHistoryPath() const -> QString {
        return d->historyPath;
    }
    auto WorkingSet::SetHistoryData(History::Pointer history) -> void {
        d->historyData = history;
    }
    auto WorkingSet::GetHistoryData() -> History::Pointer {
        return d->historyData;
    }
    auto WorkingSet::SetWorkingDirectory(const QString& path) -> void {
        d->work_dir = path;
    }
    auto WorkingSet::GetWorkingDirectory() const -> QString {
        return d->work_dir;
    }    
}