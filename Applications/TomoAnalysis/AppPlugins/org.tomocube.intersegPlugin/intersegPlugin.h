#pragma once

#include <IAppActivator.h>

const QString plugin_symbolic_name = "org.tomocube.intersegPlugin";

namespace TomoAnalysis::InterSeg::AppUI {
    class intersegPlugin : public TC::Framework::IAppActivator {
        Q_OBJECT
        Q_PLUGIN_METADATA(IID "org.tomocube.intersegPlugin")
            
    public:
        static intersegPlugin* getInstance();

        intersegPlugin();
        ~intersegPlugin();

        auto start(ctkPluginContext* context)->void override;
        //auto stop(ctkPluginContext* context)->void override;
        auto getPluginContext()->ctkPluginContext* const override;                
        
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
        static intersegPlugin* instance;
    };
}