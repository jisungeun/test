#pragma once

#include <memory>

#include <QList>
#include <QString>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
    struct InterSegInteractor_API MaskControlDS {
        typedef std::shared_ptr<MaskControlDS> Pointer;
        bool instExist{ false };
        bool memExist{ false };
        bool nucleExist{ false };
        bool nucliExist{ false };
        bool lipExist{ false };
        bool isSystem{ false };
        bool instCheck{ false };
        bool memCheck{ false };
        bool nucleCheck{ false };
        bool nucliCheck{ false };
        bool lipCheck{ false };
        QList<QString> maskList{ QList<QString>() };
        bool maskCheck{ false };
    };
    class InterSegInteractor_API IMaskControlPanel {
    public:
        IMaskControlPanel();
        virtual ~IMaskControlPanel();

        auto GetDS() const->MaskControlDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}