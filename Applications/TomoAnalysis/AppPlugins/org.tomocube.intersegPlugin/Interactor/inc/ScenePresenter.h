#pragma once

#include <memory>
#include <ISceneManagerPort.h>
#include <IMaskWriterPort.h>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
    class ISceneManagerWidget;
    class InterSegInteractor_API ScenePresenter : public UseCase::ISceneManagerPort {
    public:
        ScenePresenter(ISceneManagerWidget* widget = nullptr);
        virtual ~ScenePresenter();
                
        auto Update(Entity::WorkingSet::Pointer workingset) -> void override;
        auto UpdateFL(Entity::WorkingSet::Pointer workingset) -> void override;
        auto UpdateMultiViz(Entity::WorkingSet::Pointer workingset)->void override;
        auto ActivateTool(ToolID id) -> void override;
        auto DeactivateTool(bool isFunc) -> void override;
    private:                
        auto SetImage(TCImage::Pointer img)->void;
        auto SetWorkingData(TCMask::Pointer mask,QString name)->void;
        auto RemoveWorkingData()->void;        

        auto AppendLayer(TCMask::Pointer src, TCMask::Pointer dst)->void;
        auto RefreshWorkingData()->void;                

        struct Impl;
        std::unique_ptr<Impl> d;
    };
}