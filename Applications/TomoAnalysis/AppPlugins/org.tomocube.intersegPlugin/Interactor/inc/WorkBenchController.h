#pragma once

#include <memory>
#include <QString>

#include <IWorkBenchPort.h>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
    class InterSegInteractor_API WorkBenchController {
    public:
        WorkBenchController(UseCase::IWorkBenchPort* outPort);
        virtual ~WorkBenchController();

        auto ChangeLayout()const ->bool;
        auto UpdateInterface(Entity::WorkingSet::Pointer workingset)const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}