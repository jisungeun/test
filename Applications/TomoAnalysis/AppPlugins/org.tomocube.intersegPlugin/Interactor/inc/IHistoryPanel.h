#pragma once

#include <memory>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
    struct InterSegInteractor_API HistoryDS {
        typedef std::shared_ptr<HistoryDS> Pointer;
    };
    class InterSegInteractor_API IHistoryPanel {
    public:
        IHistoryPanel();
        virtual ~IHistoryPanel();

        auto GetDS() const->HistoryDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}