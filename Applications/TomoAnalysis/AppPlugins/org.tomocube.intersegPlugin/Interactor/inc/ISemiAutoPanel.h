#pragma once

#include <memory>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
	struct InterSegInteractor_API SemiAutoDS {
		typedef std::shared_ptr<SemiAutoDS> Pointer;
		int cur_id{ -1 };
		bool isSystem{ false };
	};
	class InterSegInteractor_API ISemiAutoPanel {
	public:
		ISemiAutoPanel();
		virtual ~ISemiAutoPanel();

		auto GetDS()const->SemiAutoDS::Pointer;

		virtual auto Update()->bool = 0;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}