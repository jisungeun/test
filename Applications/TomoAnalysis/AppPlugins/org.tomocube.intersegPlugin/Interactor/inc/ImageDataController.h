#pragma once

#include <memory>
#include <QString>

#include <ISceneManagerPort.h>
#include <IImageReaderPort.h>
#include <IWorkBenchPort.h>
#include <IMaskReaderPort.h>
#include <IMaskWriterPort.h>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
    class InterSegInteractor_API ImageDataController {
    public:
        ImageDataController(UseCase::ISceneManagerPort* outPort,
            UseCase::IWorkBenchPort* workPort,
            UseCase::IImageReaderPort* imgPort,
            UseCase::IMaskReaderPort* mskPort,
            UseCase::IMaskWriterPort* writerPort);
        virtual ~ImageDataController();                

        auto LoadTcfImage(const QString& imgPath)const->bool;//load image only and create empty mask
        auto LoadMask(const QString& mskPath,bool fromLink = false)const->bool;
        auto SaveMask(const QString& mskPath,const QString& type,const QStringList& organs)const->bool;
        auto SaveMasks()const->bool;
        auto LoadLinkedData(const QString& script)->bool;//load image and related mask using links
        auto HandleUserChange(const QString& userId,const QString& maskPath)->bool;

        auto HandleStateChanges(const QString& mask_name, int state)->bool;
        auto SetWorkingData(const QString& mask_name)->bool;
        auto UnsetWorkingData(const QString& mask_name)->bool;

        auto TimeStepChanged(int step)->bool;
        auto ImageSliceChanged(int axis, int idx)->bool;
        auto ImageSliceChanged(int x, int y, int z)->bool;

        auto ActivateFL(int ch)->bool;
        auto SetFLOpacity(int ch, double)->bool;

        auto ChangeMultiVizInfo(const QString& mask_name, bool viz)->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}