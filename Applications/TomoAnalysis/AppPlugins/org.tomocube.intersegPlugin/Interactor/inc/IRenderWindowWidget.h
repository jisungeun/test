#pragma once

#include <memory>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {        
    class InterSegInteractor_API IRenderWindowWidget {
        enum WindowTitle {
            XY,
            threeD,
            YZ,
            XZ,
        };
    public:
        WindowTitle WinOrder[4][4]{
        {XY,threeD,YZ,XZ},
        {YZ,threeD,XY,XZ},
        {XZ,threeD,XY,YZ},
        {threeD,XY,YZ,XZ}
        };    
        IRenderWindowWidget();
        virtual ~IRenderWindowWidget();

        virtual auto Update()->void = 0;
        virtual auto ChangeLayout()->void = 0;
        virtual auto SetNavigation(bool isNavi)->void = 0;
    };
}