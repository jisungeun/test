#pragma once

#include <memory>
#include <WorkingSet.h>
#include <IWorkBenchPort.h>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
    class IRenderWindowWidget;
    class ICleanerPanel;
    class IDrawingToolPanel;
    class IGeneralPanel;
    class IHistoryPanel;
    class IIDManagerPanel;
    class ILabelControlPanel;
    class IMaskControlPanel;
    class INavigatorPanel;
    class IFLControlPanel;
    class IMaskSelectionPanel;
    class ISemiAutoPanel;
    class IPlayerPanel;

    class InterSegInteractor_API WorkBenchPresenter : public UseCase::IWorkBenchPort{
    public:
        WorkBenchPresenter(IRenderWindowWidget* widget = nullptr,
            ICleanerPanel* cleaner = nullptr,
            IDrawingToolPanel* drawingtool = nullptr,
            IGeneralPanel* general = nullptr,
            IHistoryPanel* history = nullptr,
            IIDManagerPanel* idmanager = nullptr,
            ILabelControlPanel* labelcontrol = nullptr,
            IMaskControlPanel* maskcontrol = nullptr,
            INavigatorPanel* navigation = nullptr,
            IFLControlPanel* flcontrol = nullptr,
            IMaskSelectionPanel* maskselection = nullptr,
            ISemiAutoPanel* semiauto=nullptr,
            IPlayerPanel* player=nullptr);
        virtual ~WorkBenchPresenter();
                
        auto Update() -> void override;
        auto Update(Entity::WorkingSet::Pointer workingset)->void override;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}