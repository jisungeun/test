#pragma once

#include <memory>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
	struct InterSegInteractor_API PlayerDS {
		typedef std::shared_ptr<PlayerDS> Pointer;
		int range{ 1 };
		int cur_step{ 0 };
	};
	class InterSegInteractor_API IPlayerPanel {
	public:
		IPlayerPanel();
		virtual ~IPlayerPanel();

		auto GetDS() const->PlayerDS::Pointer;

		virtual auto Update()->bool = 0;
	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}