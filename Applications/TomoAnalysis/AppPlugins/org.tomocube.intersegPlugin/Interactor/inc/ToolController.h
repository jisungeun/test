#pragma once

#include <memory>

#include <ISceneManagerPort.h>
#include <IWorkBenchPort.h>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
    class InterSegInteractor_API ToolController {
    public:
        ToolController(UseCase::ISceneManagerPort* outPort,UseCase::IWorkBenchPort* workPort = nullptr);
        virtual ~ToolController();

        auto ActivateTool(const int& toolID)const->bool;
        auto DeactivateTool(bool isFunc = false)const->bool;
        auto PerformInstanceAlgorithm(QString algo_name)const->bool;

    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}