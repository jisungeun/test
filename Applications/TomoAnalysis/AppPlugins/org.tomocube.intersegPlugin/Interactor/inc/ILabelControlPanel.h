#pragma once

#include <memory>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
    struct InterSegInteractor_API LabelControlDS {
        typedef std::shared_ptr<LabelControlDS> Pointer;
        int labelMin{ 1 };
        int labelMax{ 1 };        
        bool isEnable{ false };
        bool isMerge{ false };
        bool isSystem{ false };
    };
    class InterSegInteractor_API ILabelControlPanel {
    public:
        ILabelControlPanel();
        virtual ~ILabelControlPanel();

        auto GetDS() const->LabelControlDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}