#pragma once

#include <memory>

#include <ISceneManagerPort.h>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
	class InterSegInteractor_API SceneController {
	public:
		SceneController(UseCase::ISceneManagerPort* outPort);
		virtual ~SceneController();

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}