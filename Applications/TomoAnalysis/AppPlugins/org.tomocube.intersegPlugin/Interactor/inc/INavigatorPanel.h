#pragma once

#include <map>
#include <memory>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
	enum ImageType {
		Unknown = 0,
		HT3D = 1,
		FL3D,
		HT2D,
		FL2D,
		PHASE,
		HT2DMIP,
		FL2DMIP,
		BF,
		Count,
	};
	struct InterSegInteractor_API NavigatorInfo {
		int width = 0;
		int height = 0;
		int depth = 0;

		float resolutionX = 0.f;
		float resolutionY = 0.f;
		float resolutionZ = 0.f;

		typedef std::shared_ptr<NavigatorInfo> Pointer;
	};

	struct InterSegInteractor_API NavigatorDS {
		int rangeX = 1;
		int rangeY = 1;
		int rangeZ = 1;

		int x = 0;
		int y = 0;
		int z = 0;

		float resX = 0.f;
		float resY = 0.f;
		float resZ = 0.f;

		typedef std::shared_ptr<NavigatorDS> Pointer;
	};

	class InterSegInteractor_API INavigatorPanel {
	public:
		INavigatorPanel();
		virtual ~INavigatorPanel();

		auto GetNavigatorDS() const->NavigatorDS::Pointer;

		virtual auto Update()->bool = 0;
		virtual auto Refresh()->bool = 0;

	private:
		struct Impl;
		std::unique_ptr<Impl> d;
	};
}