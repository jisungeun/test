#pragma once

#include <QString>

#include <memory>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
    struct InterSegInteractor_API GeneralDS {
        typedef std::shared_ptr<GeneralDS> Pointer;
        bool standAlone{ true };
        QString linked_app{ QString() };
        QString file_name{QString()};
        QString user_name{QString()};
    };
    class InterSegInteractor_API IGeneralPanel {
    public:
        IGeneralPanel();
        virtual ~IGeneralPanel();

        auto GetDS() const->GeneralDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}