#pragma once

#include <memory>
#include <QStringList>
#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
    struct InterSegInteractor_API IDManagerDS {
        typedef std::shared_ptr<IDManagerDS> Pointer;
        QStringList idList;
        QString curUser;
        QString imagePath;
        QString curMaskPath;
        QString workindDir;
    };
    class InterSegInteractor_API IIDManagerPanel {
    public:
        IIDManagerPanel();
        virtual ~IIDManagerPanel();

        auto GetDS() const->IDManagerDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}