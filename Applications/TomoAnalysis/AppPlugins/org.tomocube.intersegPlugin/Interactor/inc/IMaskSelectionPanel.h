#pragma once

#include <memory>

#include <QList>
#include <QString>

#include "InterSegInteractorExport.h"

namespace TomoAnalysis::InterSeg::Interactor {
    struct InterSegInteractor_API MaskSelectionDS {
        typedef std::shared_ptr<MaskSelectionDS> Pointer;
        bool instExist{ false };
        bool memExist{ false };
        bool nucleExist{ false };
        bool nucliExist{ false };
        bool lipExist{ false };
        QList<QString> maskList{ QList<QString>() };
        QString curSelection{ QString() };
    };
    class InterSegInteractor_API IMaskSelectionPanel {
    public:
        IMaskSelectionPanel();
        virtual ~IMaskSelectionPanel();

        auto GetDS() const->MaskSelectionDS::Pointer;

        virtual auto Update()->bool = 0;
        virtual auto Reset()->void = 0;
    private:
        struct Impl;
        std::unique_ptr<Impl> d;
    };
}