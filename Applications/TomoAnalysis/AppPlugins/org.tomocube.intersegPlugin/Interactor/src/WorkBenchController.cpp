//UseCases
#include <ModifyWorkBench.h>

#include "WorkBenchController.h"


namespace TomoAnalysis::InterSeg::Interactor {
    struct WorkBenchController::Impl {
        UseCase::IWorkBenchPort* outPort{ nullptr };
    };

    WorkBenchController::WorkBenchController(UseCase::IWorkBenchPort* outPort) :d{ new Impl } {
        d->outPort = outPort;
    }

    WorkBenchController::~WorkBenchController() {
        
    }
    auto WorkBenchController::ChangeLayout() const -> bool {
        auto useCase = UseCase::ModifyWorkBench();

        return useCase.ChangeLayout(d->outPort);
    }
    auto WorkBenchController::UpdateInterface(Entity::WorkingSet::Pointer workingset) const -> bool {
        auto useCase = UseCase::ModifyWorkBench();
        return useCase.RefreshInterface(workingset,d->outPort);
    }

}