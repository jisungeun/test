#include <iostream>
#include <QFileInfo>

#include <Tool.h>

#include "IRenderWindowWidget.h"
#include "ICleanerPanel.h"
#include "IDrawingToolPanel.h"
#include "IGeneralPanel.h"
#include "IHistoryPanel.h"
#include "IIDManagerPanel.h"
#include "ILabelControlPanel.h"
#include "IMaskControlPanel.h"
#include "IMaskSelectionPanel.h"
#include "INavigatorPanel.h"
#include "IFLControlPanel.h"
#include "ISemiAutoPanel.h"
#include "IPlayerPanel.h"

#include "WorkBenchPresenter.h"

namespace TomoAnalysis::InterSeg::Interactor {
	struct WorkBenchPresenter::Impl {
		IRenderWindowWidget* widget{ nullptr };
		ICleanerPanel* cleaner{ nullptr };
		IDrawingToolPanel* drawingtool{ nullptr };
		IGeneralPanel* general{ nullptr };
		IHistoryPanel* history{ nullptr };
		IIDManagerPanel* idmanager{ nullptr };
		ILabelControlPanel* labelcontrol{ nullptr };
		IMaskControlPanel* maskcontrol{ nullptr };
		INavigatorPanel* navigation{ nullptr };
		IFLControlPanel* flcontrol{ nullptr };
		IMaskSelectionPanel* maskselection{ nullptr };
		ISemiAutoPanel* semiauto{ nullptr };
		IPlayerPanel* player{ nullptr };
	};
	WorkBenchPresenter::WorkBenchPresenter(IRenderWindowWidget* widget, ICleanerPanel* cleaner, IDrawingToolPanel* drawingtool, IGeneralPanel* general, IHistoryPanel* history, IIDManagerPanel* idmanager, ILabelControlPanel* labelcontrol, IMaskControlPanel* maskcontrol, INavigatorPanel* navigation, IFLControlPanel* flpanel, IMaskSelectionPanel* maskselection,ISemiAutoPanel* semiauto,IPlayerPanel* player) :d{ new Impl } {
		d->widget = widget;
		d->cleaner = cleaner;
		d->drawingtool = drawingtool;
		d->general = general;
		d->history = history;
		d->idmanager = idmanager;
		d->labelcontrol = labelcontrol;
		d->maskcontrol = maskcontrol;
		d->navigation = navigation;
		d->flcontrol = flpanel;
		d->maskselection = maskselection;
		d->semiauto = semiauto;
		d->player = player;
	}

	WorkBenchPresenter::~WorkBenchPresenter() {

	}
	auto WorkBenchPresenter::Update() -> void {
		//simple update for change widget layout
		if (nullptr != d->widget) {
			d->widget->ChangeLayout();
		}
	}
	auto WorkBenchPresenter::Update(Entity::WorkingSet::Pointer workingset) -> void {
		//update linked components due to image meta data
		auto meta = workingset->GetMetaInfo();
		auto instExist = nullptr != workingset->GetInstData();
		auto organExist = nullptr != workingset->GetOrganData();

		if(!instExist && !organExist) {
			workingset->SetCurMaskName("");
		}

		if (nullptr == meta) {
			return;
		}
		if (nullptr != d->widget) {			
			//update navigation mode
		    d->widget->SetNavigation(-1 != workingset->GetCurToolIdx());
			if (workingset->GetResetView()) {
				d->widget->Update();
				workingset->SetResetView(false);
			}
		}		
		if (nullptr != d->cleaner) {
			auto cleanDS = d->cleaner->GetDS();
			cleanDS->cur_idx = workingset->GetCurToolIdx();
			cleanDS->isSystem = (workingset->GetCurUser() == "System");
			d->cleaner->Update();
		}
		if (nullptr != d->history) {
			//Nothing
			d->history->Update();
		}
		if (nullptr != d->idmanager) {
			//nothing
			auto ds = d->idmanager->GetDS();
			ds->imagePath = workingset->GetImagePath();
			ds->curMaskPath = workingset->GetMaskPath();
			d->idmanager->Update();
		}
		if (nullptr != d->labelcontrol) {
			auto labelDS = d->labelcontrol->GetDS();
			labelDS->labelMin = 1;
			labelDS->labelMax = 1;
			labelDS->isEnable = workingset->GetCurToolIdx() == Entity::ToolIdx::PICK;
			labelDS->isMerge = workingset->GetCurToolIdx() == Entity::ToolIdx::MERGE_LABEL;
			labelDS->isSystem = (workingset->GetCurUser() == "System");
			if (instExist && workingset->GetCurMaskName() == "cellInst") {
				auto mask = workingset->GetInstData();
				labelDS->labelMax = mask->GetBlobIndexes().size();
			}
			d->labelcontrol->Update();
		}
		if (nullptr != d->maskselection) {
			auto msDS = d->maskselection->GetDS();
			msDS->instExist = false;
			msDS->lipExist = false;
			msDS->memExist = false;
			msDS->nucleExist = false;
			msDS->nucliExist = false;
			msDS->maskList = QList<QString>();
			if(instExist) {
				msDS->instExist = true;
			}			
			if(organExist) {
				auto mask = workingset->GetOrganData();
				auto names = mask->GetLayerNames();
				for(auto n : names) {
				    if(n == "membrane") {
						msDS->memExist = true;
				    }else if(n == "nucleus") {
						msDS->nucleExist = true;
				    }else if(n == "nucleoli") {
						msDS->nucliExist = true;
				    }else if(n == "lipid droplet") {
						msDS->lipExist = true;
				    }else {
						msDS->maskList.push_back(n);
				    }
				}				
			}
			msDS->curSelection = workingset->GetCurMaskName();
			d->maskselection->Update();
		}
		if (nullptr != d->maskcontrol) {
			auto mcDS = d->maskcontrol->GetDS();			
			mcDS->memExist = false;
			mcDS->nucleExist = false;
			mcDS->nucliExist = false;
			mcDS->lipExist = false;
			mcDS->instExist = false;
			mcDS->maskList = QList<QString>();
			mcDS->isSystem = (workingset->GetCurUser() == "System");			

			if(instExist) {
				mcDS->instExist = true;
				mcDS->instCheck = workingset->GetMultiViz("cellInst");
			}
			if(organExist) {
				auto mask = workingset->GetOrganData();
				auto names = mask->GetLayerNames();
				for(auto n : names) {
					if (n == "membrane") {
						mcDS->memExist = true;
						mcDS->memCheck = workingset->GetMultiViz("membrane");
					}
					else if (n == "nucleus") {
						mcDS->nucleExist = true;
						mcDS->nucleCheck = workingset->GetMultiViz("nucleus");
					}
					else if (n == "nucleoli") {
						mcDS->nucliExist = true;
						mcDS->nucliCheck = workingset->GetMultiViz("nucleoli");
					}
					else if (n == "lipid droplet") {
						mcDS->lipExist = true;
						mcDS->lipCheck = workingset->GetMultiViz("lipid droplet");
					}
					else {
						mcDS->maskList.push_back(n);
						mcDS->maskCheck = workingset->GetMultiViz(n);
					}
				}				
			}
			d->maskcontrol->Update();
		}
		if (nullptr != d->navigation) {
			auto nds = d->navigation->GetNavigatorDS();
			if(workingset->GetChangeSlice()) {
				int curSlice[3];
				workingset->GetCurrentSlice(curSlice);
				nds->x = curSlice[0];
				nds->y = curSlice[1];
				nds->z = curSlice[2];
				d->navigation->Refresh();
				workingset->SetChangeSlice(false);
			}
			else {				
				auto size = workingset->GetImageData()->GetSize();
				double res[3];
				workingset->GetImageData()->GetResolution(res);
				nds->rangeX = std::get<0>(size);
				nds->rangeY = std::get<1>(size);
				nds->rangeZ = std::get<2>(size);

				nds->x = std::get<0>(size) / 2;
				nds->y = std::get<1>(size) / 2;
				nds->z = std::get<2>(size) / 2;

				workingset->SetCurrentSlice(nds->x, nds->y, nds->z);

				nds->resX = res[0];
				nds->resY = res[1];
				nds->resZ = res[2];

				d->navigation->Update();
			}
		}
		if (nullptr != d->drawingtool) {
			auto ds = d->drawingtool->GetDS();
			ds->curToolIdx = workingset->GetCurToolIdx();
			ds->isSystem = (workingset->GetCurUser() == "System");
			d->drawingtool->Update();
		}
		if (nullptr != d->general) {
			auto ds = d->general->GetDS();
			ds->user_name = workingset->GetCurUser();
			QFileInfo info(workingset->GetImagePath());
			ds->file_name = info.completeBaseName();
			d->general->Update();
		}
		if(nullptr != d->semiauto) {
			auto ds = d->semiauto->GetDS();
			ds->cur_id = workingset->GetCurToolIdx();
			ds->isSystem = (workingset->GetCurUser() == "System");
			d->semiauto->Update();
		}
		if(nullptr !=d->player) {
			auto ds = d->player->GetDS();
			ds->range = meta->data.data3D.dataCount;
			ds->cur_step = workingset->GetCurTimeStep();
			d->player->Update();
		}
		if(nullptr !=d->flcontrol) {
			auto ds = d->flcontrol->GetDS();
			auto fmeta = workingset->GetMetaInfo();
			if(fmeta->data.data3DFL.exist) {
			    for(auto i=0;i<3;i++) {
			        if(fmeta->data.data3DFL.valid[i]) {
						ds->chExist[i] = true;
						ds->chOpa[i] = workingset->GetFLOpacity(i);
			        }
			    }
			}
			ds->curCh = workingset->GetCurFLCh();
			d->flcontrol->Update();
		}
	}

}
