#include "IDrawingToolPanel.h"

namespace TomoAnalysis::InterSeg::Interactor {
	struct IDrawingToolPanel::Impl {
		DrawingToolDS::Pointer ds = std::make_shared<DrawingToolDS>();
	};
	IDrawingToolPanel::IDrawingToolPanel() : d{ new Impl } {

	}
	IDrawingToolPanel::~IDrawingToolPanel() {

	}
	auto IDrawingToolPanel::GetDS() const -> DrawingToolDS::Pointer {
		return d->ds;
	}
}