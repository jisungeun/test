//UseCase
#include <ActivateTool.h>
#include <FinishTool.h>

#include "ToolController.h"

namespace TomoAnalysis::InterSeg::Interactor {
    struct ToolController::Impl {
        UseCase::ISceneManagerPort* outPort{ nullptr };
        UseCase::IWorkBenchPort* workPort{ nullptr };
    };
    ToolController::ToolController(UseCase::ISceneManagerPort* outPort,UseCase::IWorkBenchPort* workPort) :d{ new Impl } {
        d->outPort = outPort;
        d->workPort = workPort;
    }
    ToolController::~ToolController() {
        
    }
    auto ToolController::ActivateTool(const int& toolID) const -> bool {
        auto working_mask = Entity::WorkingSet::GetInstance()->GetCurMaskName();
        if(working_mask.isEmpty()) {
            return false;
        }
        if((toolID  / 100 > 0)&& working_mask != "cellInst") {
            return false;
        }        
        if(1 > toolID) {
            return false;
        }
        
        auto useCase = UseCase::ActivateTool(d->outPort,d->workPort);
        return useCase.Request(toolID);        
    }
    auto ToolController::DeactivateTool(bool isFunc) const -> bool {
        auto working_mask = Entity::WorkingSet::GetInstance()->GetCurMaskName();
        if(working_mask.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::FinishTool(d->outPort,d->workPort);
        return useCase.Request(isFunc);
    }
    auto ToolController::PerformInstanceAlgorithm(QString algo_name) const -> bool {
        if(algo_name.isEmpty()) {
            return false;
        }
        return true;
    }

}