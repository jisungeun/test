#include "ISemiAutoPanel.h"

namespace TomoAnalysis::InterSeg::Interactor {
    struct ISemiAutoPanel::Impl {
        SemiAutoDS::Pointer ds = std::make_shared<SemiAutoDS>();
    };
    ISemiAutoPanel::ISemiAutoPanel() :d{ new Impl } {
        
    }
    ISemiAutoPanel::~ISemiAutoPanel() {
        
    }
    auto ISemiAutoPanel::GetDS() const -> SemiAutoDS::Pointer {
        return d->ds;
    }
}