#include "IMaskSelectionPanel.h"

namespace TomoAnalysis::InterSeg::Interactor {
	struct IMaskSelectionPanel::Impl {
		MaskSelectionDS::Pointer ds = std::make_shared<MaskSelectionDS>();
	};
	IMaskSelectionPanel::IMaskSelectionPanel() : d{ new Impl } {

	}
	IMaskSelectionPanel::~IMaskSelectionPanel() {

	}
	auto IMaskSelectionPanel::GetDS() const -> MaskSelectionDS::Pointer {
		return d->ds;
	}
}