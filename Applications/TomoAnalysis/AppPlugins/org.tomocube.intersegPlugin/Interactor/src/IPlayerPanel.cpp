#include "IPlayerPanel.h"

namespace TomoAnalysis::InterSeg::Interactor {
	struct IPlayerPanel::Impl {
		PlayerDS::Pointer ds = std::make_shared<PlayerDS>();
	};

	IPlayerPanel::IPlayerPanel() :d{ new Impl } {

	}
	IPlayerPanel::~IPlayerPanel() {

	}
	auto IPlayerPanel::GetDS() const -> PlayerDS::Pointer {
		return d->ds;
    }
}