#include "SceneController.h"

//UseCases

namespace TomoAnalysis::InterSeg::Interactor {
    struct SceneController::Impl {
        UseCase::ISceneManagerPort* outPort{ nullptr };
    };
    SceneController::SceneController(UseCase::ISceneManagerPort* outPort) {
        d->outPort = outPort;
    }
    SceneController::~SceneController() {
        
    }    
}