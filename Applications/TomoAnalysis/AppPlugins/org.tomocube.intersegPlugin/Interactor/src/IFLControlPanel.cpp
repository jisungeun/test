#include "IFLControlPanel.h"

namespace TomoAnalysis::InterSeg::Interactor {
	struct IFLControlPanel::Impl {
		FLControlDS::Pointer ds = std::make_shared<FLControlDS>();
	};
	IFLControlPanel::IFLControlPanel() : d{ new Impl } {

	}
	IFLControlPanel::~IFLControlPanel() {

	}
	auto IFLControlPanel::GetDS() const -> FLControlDS::Pointer {
		return d->ds;
	}
}