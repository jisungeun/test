#include "IGeneralPanel.h"

namespace TomoAnalysis::InterSeg::Interactor {
	struct IGeneralPanel::Impl {
		GeneralDS::Pointer ds = std::make_shared<GeneralDS>();
	};
	IGeneralPanel::IGeneralPanel() : d{ new Impl } {

	}
	IGeneralPanel::~IGeneralPanel() {

	}
	auto IGeneralPanel::GetDS() const -> GeneralDS::Pointer {
		return d->ds;
	}
}