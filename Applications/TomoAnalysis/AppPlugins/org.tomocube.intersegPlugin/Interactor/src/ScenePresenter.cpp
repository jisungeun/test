#include <iostream>

#include <QMap>

#include "ISceneManagerWidget.h"

#include "ScenePresenter.h"

namespace TomoAnalysis::InterSeg::Interactor {
    struct ScenePresenter::Impl {
        ISceneManagerWidget* widget{nullptr};        
    };
    ScenePresenter::ScenePresenter(ISceneManagerWidget* widget) : d{ new Impl } {
        d->widget = widget;        
    }
    ScenePresenter::~ScenePresenter() {


    }
    auto ScenePresenter::Update(Entity::WorkingSet::Pointer workingset) -> void {
        d->widget->ClearVizMask();
        auto image = workingset->GetImageData();        
        d->widget->SetImage(image,workingset->GetImagePath());
        auto toolidx = workingset->GetCurToolIdx();
        if(toolidx>-1) {
            d->widget->DeactivateTool();
        }
        auto curMaskName = workingset->GetCurMaskName();        
        if(curMaskName.isEmpty()) {
            RemoveWorkingData();
        }else {
            if(curMaskName=="cellInst") {
                SetWorkingData(workingset->GetInstData(), curMaskName);
            }else {
                SetWorkingData(workingset->GetOrganData(), curMaskName);
            }            
        }
    }    
    auto ScenePresenter::UpdateFL(Entity::WorkingSet::Pointer workingset) -> void {
        auto curCh = workingset->GetCurFLCh();
        if(curCh<0) {            
            return;
        }
        auto flImage = workingset->GetImageFLData(curCh);
        if(nullptr == flImage) {
            return;
        }
    }
    auto ScenePresenter::UpdateMultiViz(Entity::WorkingSet::Pointer workingset) -> void {
        d->widget->ClearVizMask();
        auto mv = workingset->GetMultiViz();
        auto viz_cnt = 0;
        for(auto mkey : mv.keys()) {
            TCMask::Pointer mask;            
            if (mkey == "cellInst") {
                mask = workingset->GetInstData();                
            }else {
                mask = workingset->GetOrganData();                
            }
            if (mv[mkey]) {
                d->widget->AddVizMask(mask, mkey);
                viz_cnt++;
            }
            else {
                d->widget->RemoveVizMask(mkey);
            }
        }
        if (viz_cnt > 0) {
            d->widget->GenerateVizMask();
        }
    }
    auto ScenePresenter::ActivateTool(ToolID id) -> void {
        d->widget->ActivateTool(id);
    }
    auto ScenePresenter::DeactivateTool(bool isFunc) -> void {
        d->widget->DeactivateTool(isFunc);
        RefreshWorkingData();
    }
    auto ScenePresenter::SetImage(TCImage::Pointer img) -> void {
        if(nullptr != d->widget) {
            d->widget->SetImage(img,QString());
        }
    }
    auto ScenePresenter::RefreshWorkingData() -> void {
        auto ws = Entity::WorkingSet::GetInstance();
        auto curName = ws->GetCurMaskName();
        if(!curName.isEmpty()) {
            if(curName == "cellInst") {
                d->widget->RearrangeLabel();
                auto mask = d->widget->GetMask();
                mask->SetTimeStep(ws->GetCurTimeStep());
                ws->SetInstData(mask);
            }else {
                auto mask = d->widget->GetMask();
                auto oriMask = ws->GetOrganData();
                if (nullptr != oriMask) {
                    AppendLayer(mask, oriMask);
                }
                //mask->SetTimeStep(ws->GetCurTimeStep());
                //ws->SetOrganData(mask);                
            }
        }
    }
    auto ScenePresenter::SetWorkingData(TCMask::Pointer mask, QString name) -> void {
        if(nullptr !=d->widget->GetMask()) {
            RemoveWorkingData();
        }
        if (nullptr != mask) {
            d->widget->SetMask(mask, name);
        }
    } 
    auto ScenePresenter::RemoveWorkingData() -> void {
        auto ws = Entity::WorkingSet::GetInstance();
        if(ws->GetCurUser() == "System") {
            return;
        }
        auto mask = d->widget->GetMask();
        if(nullptr == mask) {
            return;
        }
        auto mask_names = mask->GetLayerNames();
        if(mask_names.count()<1) {
            return;
        }
        auto mask_name = mask->GetLayerNames()[0];
        if(mask_name.isEmpty()) {
            return;
        }        
        TCMask::Pointer oriMask;
        if(mask_name == "cellInst") {
            mask->SetTimeStep(ws->GetCurTimeStep());
            ws->SetInstData(mask);
        }else {
            oriMask = ws->GetOrganData();
            if (nullptr != oriMask) {
                AppendLayer(mask, oriMask);
            }
        }

        d->widget->RemoveMask(mask_name);
    }    

    auto ScenePresenter::AppendLayer(TCMask::Pointer src, TCMask::Pointer dst) -> void {
        auto srcLayers = src->GetLayerNames();
        auto dstLayers = dst->GetLayerNames();
        for (auto sl : srcLayers) {
            if (dstLayers.contains(sl)) {
                auto srcBlob = src->GetLayerBlob(sl);
                dst->AppendLayer(sl, srcBlob);

                break;
            }
        }
    }

}