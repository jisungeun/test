#include "ILabelControlPanel.h"

namespace TomoAnalysis::InterSeg::Interactor {
	struct ILabelControlPanel::Impl {
		LabelControlDS::Pointer ds = std::make_shared<LabelControlDS>();
	};
	ILabelControlPanel::ILabelControlPanel() : d{ new Impl } {

	}
	ILabelControlPanel::~ILabelControlPanel() {

	}
	auto ILabelControlPanel::GetDS() const -> LabelControlDS::Pointer {
		return d->ds;
	}
}