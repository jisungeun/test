#include <iostream>
#include <QFileInfo>

//UseCases
#include <OpenData.h>
#include <SaveData.h>
#include <ModifyData.h>

#include "ImageDataController.h"

namespace TomoAnalysis::InterSeg::Interactor {
    struct ImageDataController::Impl {
        UseCase::ISceneManagerPort* outPort{ nullptr };
        UseCase::IWorkBenchPort* workPort{ nullptr };
        UseCase::IImageReaderPort* imagePort{ nullptr };
        UseCase::IMaskReaderPort* maskPort{ nullptr };
        UseCase::IMaskWriterPort* writerPort{ nullptr };
    };    
    ImageDataController::ImageDataController(UseCase::ISceneManagerPort* outPort, UseCase::IWorkBenchPort* workPort, UseCase::IImageReaderPort* imgPort, UseCase::IMaskReaderPort* mskPort, UseCase::IMaskWriterPort* writerPort) :d{ new Impl } {
        d->outPort = outPort;
        d->workPort = workPort;
        d->imagePort = imgPort;
        d->maskPort = mskPort;
        d->writerPort = writerPort;
    }
    ImageDataController::~ImageDataController() {
        
    }    
    auto ImageDataController::LoadTcfImage(const QString& imgPath) const -> bool {
        if(imgPath.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::OpenData(d->imagePort,d->maskPort,d->outPort,d->workPort);
        return useCase.LoadImage(imgPath);
    }
    auto ImageDataController::LoadMask(const QString& mskPath,bool fromLink) const -> bool {
        if(mskPath.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::OpenData(nullptr, d->maskPort, d->outPort, d->workPort);
        return useCase.LoadMask(mskPath,fromLink);
    }
    auto ImageDataController::SaveMask(const QString& mskPath, const QString& type, const QStringList& organs) const -> bool {
        if(mskPath.isEmpty()) {
            return false;
        }
        if(type.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::SaveData();
        return useCase.Request(mskPath,type,organs,d->writerPort);
    }    
    auto ImageDataController::SaveMasks() const -> bool {
        auto useCase = UseCase::SaveData();
        return useCase.SaveAll(d->writerPort);
    }

    auto ImageDataController::LoadLinkedData(const QString& script) -> bool {
        Q_UNUSED(script)
        return true;
    }
    auto ImageDataController::HandleUserChange(const QString& userId, const QString& maskPath) -> bool {        
        if(userId.isEmpty()) {
            return false;
        }
        if(maskPath.isEmpty()) {
            return false;
        }        
        auto useCase = UseCase::ModifyData(d->outPort, d->workPort, nullptr, d->maskPort,d->writerPort);
        return useCase.ChangeUser(userId, maskPath);
    }

    auto ImageDataController::HandleStateChanges(const QString& mask_name, int state) -> bool {
        if(mask_name.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyData(d->outPort, d->workPort,nullptr,d->maskPort);
        if(state == 0) {//empty to working            
            return useCase.MakeMask(mask_name);
        }
        if(state == 1) {//working to confirm
            return useCase.ConfirmMask(mask_name);
        }
        if(state == 2) {//confirm to working
            return useCase.WorkingMask(mask_name);
        }
        return false;
    }

    auto ImageDataController::ImageSliceChanged(int x, int y, int z) -> bool {
        auto useCase = UseCase::ModifyData(d->outPort, d->workPort);
        return useCase.ChangeSlice(x, y, z);
    }

    auto ImageDataController::ImageSliceChanged(int axis, int idx) -> bool {
        auto useCase = UseCase::ModifyData(d->outPort, d->workPort);
        return useCase.ChangeSlice(axis,idx);
    }
    auto ImageDataController::TimeStepChanged(int step) -> bool {
        auto useCase = UseCase::ModifyData(d->outPort,d->workPort,d->imagePort,d->maskPort,d->writerPort);
        return useCase.ChangeTimeStep(step);
    }
    auto ImageDataController::SetWorkingData(const QString& mask_name) -> bool {
        if(mask_name.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyData(d->outPort, d->workPort);
        return useCase.WorkingMask(mask_name);
    }
    auto ImageDataController::UnsetWorkingData(const QString& mask_name) -> bool {
        if(mask_name.isEmpty()) {
            return false;
        }
        auto useCase = UseCase::ModifyData(d->outPort, d->workPort,nullptr,d->maskPort,d->writerPort);
        return useCase.WorkingMask(mask_name, true);
    }
    auto ImageDataController::ActivateFL(int ch) -> bool {
        auto useCase = UseCase::ModifyData(nullptr, d->workPort,nullptr,nullptr,d->writerPort);
        return useCase.ShowFLSlice(ch);
    }
    auto ImageDataController::SetFLOpacity(int ch, double opa) -> bool {
        auto useCase = UseCase::ModifyData(d->outPort);
        return useCase.SetFLOpacity(ch, opa);
    }
    auto ImageDataController::ChangeMultiVizInfo(const QString& mask_name, bool viz) -> bool {
        auto useCase = UseCase::ModifyData(d->outPort, d->workPort);
        return useCase.SetMultiViz(mask_name, viz);
    }

}